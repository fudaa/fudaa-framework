image: registry.gitlab.com/fudaa/fudaa-scripts:21

variables:
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "-U --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

# Cache downloaded dependencies and plugins between builds.
cache:
  paths:
  - .m2/repository
  - .sonar/cache

# we define the stages. By default, the first three are defined. We added the release stage to deliver the artifacts and tag the sources.
stages:
- build
- deploy
#to launch Pipelines of dependant project
- trigger_dependant_projects_and_quality
#we added a manual stage to release version
- release

build:
  stage: build
  interruptible: true
  except:
  - tags
  script:
  - mvn $MAVEN_CLI_OPTS -Pcoverage clean verify install
  artifacts:
    #to have the junit results
    when: always
    paths:
      - "**/target/"
    reports:
      junit:
        - "*/target/surefire-reports/TEST-*.xml"
        - "*/target/failsafe-reports/TEST-*.xml"

generate_sonar_report:
  stage: deploy
  allow_failure: true
  dependencies:
    - build
  interruptible: true
  before_script:
  - bash /ci_scripts/prevent_maven_recompile.sh
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
  - mvn sonar:sonar -Dsonar.projectKey=fudaa-framework -Dsonar.organization=fudaa -Dsonar.host.url=https://sonarcloud.io -Dsonar.token=$SONAR_TOKEN
  only:
  - master

deploy:
  stage: deploy
  dependencies:
    - build
  interruptible: true
  except:
  - tags
  before_script:
  - bash /ci_scripts/prevent_maven_recompile.sh
  script:
  #skipTests: already done in the test stage
  - mvn $MAVEN_CLI_OPTS -Psources -DskipTests deploy
  only:
    - master

# Launch a Fudaa-Prepro build
trigger_fudaa_prepro:
  stage: trigger_dependant_projects_and_quality
  except: 
  - tags
  only: 
  - master
  script:
  - curl --request POST --form "token=$CI_JOB_TOKEN" --form description="Launched from fudaa-framework project" --form ref=master https://gitlab.com/api/v4/projects/8893336/trigger/pipeline

# Launch a Fudaa-Crue build
trigger_fudaa_crue:
  stage: trigger_dependant_projects_and_quality
  except:
  - tags
  only:
  - master
  script:
  - curl --request POST --form "token=$CI_JOB_TOKEN" --form description="Launched from fudaa-framework project" --form ref=master https://gitlab.com/api/v4/projects/8895820/trigger/pipeline

# Launch a Fudaa-LSPIV build
trigger_fudaa_lspiv:
  stage: trigger_dependant_projects_and_quality
  except:
  - tags
  only:
  - master
  script:
  - curl --request POST --form "token=$CI_JOB_TOKEN" --form description="Launched from fudaa-framework project" --form ref=master https://gitlab.com/api/v4/projects/8894465/trigger/pipeline

  # only launched if a tag release-* is done. In general this tag is created by the job release
deploy_release:
  stage: deploy
  # use regexp
  only:
  - /^release-.*$/
  #do not depend on previous job
  dependencies: []
  except:
  - branches
  script:
  - bash /ci_scripts/install_gpg.sh
  - mvn $MAVEN_CLI_OPTS -Prelease deploy

#The release job, create a tag the next release version and update the current branch (MASTER) with the next SNAPSHOT
release:
  stage: release
  except:
  - tags
  when: manual
  # no dependencies on previous jobs as this job can be launched at any time.
  dependencies: []
  script:
  - bash  /ci_scripts/maven_tag_new_version_and_next_snapshot.sh
