Documentation: https://fudaa-project.atlassian.net/wiki/spaces/FUDAA

pour le déploiement voir https://fudaa-project.atlassian.net/wiki/spaces/FUDAA/pages/236781569/Pipelines+Fudaa+pour+cr+er+une+release

# Maven et image Docker
Une image Maven spécifique à Fudaa est utilisée.
Elle est construite via la projet https://gitlab.com/fudaa/fudaa-scripts

# Trigger

le déploiement depuis la master lance les pipelines de Fudaa-Prepro et Fudaa-Crue

# Old Version 1.0
**Source managed on Sourceforge**: 
* https://sourceforge.net/p/fudaa/svn/HEAD/tree/trunk/archives/dodico-corba/
* https://sourceforge.net/p/fudaa/svn/HEAD/tree/trunk/archives/fudaa-common-corba/

**branchs**
* https://sourceforge.net/p/fudaa/svn/HEAD/tree/branches/fudaa-framework-1.0/
* https://sourceforge.net/p/fudaa/svn/HEAD/tree/branches/dodico-corba_1.0/
* https://sourceforge.net/p/fudaa/svn/HEAD/tree/branches/fudaa-common-corba_1.0/

