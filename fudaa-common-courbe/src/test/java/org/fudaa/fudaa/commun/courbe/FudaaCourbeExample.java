/**
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2007-01-19 13:14:33 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGFilleSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.ebli.courbe.EGTableGraphePanel;

import org.fudaa.fudaa.commun.courbe.FudaaGrapheSimpleModel;
import org.fudaa.fudaa.commun.impl.Fudaa;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeExample.java,v 1.3 2007-01-19 13:14:33 deniger Exp $ Un exemple pour l'utilisation des
 *          courbes.
 */
public final class FudaaCourbeExample {

  private FudaaCourbeExample() {}

  public static void main(final String[] _args) {
    final Fudaa f = new Fudaa();
    final BuInformationsSoftware s = new BuInformationsSoftware();
    s.name = "fudaa-test";
    f.launch(_args, s, true, false);
    final FudaaCommonImplementation imp = new FudaaCommonImplementation() {

      @Override
      public BuPreferences getApplicationPreferences() {
        return null;
      }

      @Override
      public BuInformationsSoftware getInformationsSoftware() {
        // TODO Auto-generated method stub
        return null;
      }
    };
    f.startApp(imp);
    final EGAxeHorizontal h = new EGAxeHorizontal();
    h.setTitre("X");
    final FudaaGrapheSimpleModel model = new FudaaGrapheSimpleModel();
    final EGFilleSimple fille = new EGFilleSimple(model, EbliLib.getS("Courbes"), imp, null, new EGTableGraphePanel());
    final EGGraphe g = fille.getGraphe();
    model.addModelListener(new EGGrapheModelListener() {

      @Override
      public void axeAspectChanged(final EGAxe _c) {}

      @Override
      public void axeContentChanged(final EGAxe _c) {}

      @Override
      public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {}

      @Override
      public void courbeContentChanged(final EGObject _c, boolean _mustRestore) {
        g.restore();
      }

      @Override
      public void structureChanged() {
        g.restore();
      }

    });
    g.setXAxe(h);
    model.addMutableCourbe("test", new double[] { 1, 10, 100 }, new double[] { 1, 10, 100 });
    fille.setClosable(false);
    final Runnable r = new Runnable() {

      @Override
      public void run() {
        JComponent c = fille.createPanelComponent();
        fille.majComponent(c);
        BuScrollPane sp = new BuScrollPane(c);
        sp.setPreferredWidth(150);
        imp.getMainPanel().getRightColumn().addToggledComponent(fille.getComponentTitle(), "TOGGLE_SPEC",
            BuResource.BU.getToolIcon("arbre"), sp, true, imp);
        imp.getMainPanel().revalidate();
        imp.getMainPanel().updateSplits();
        imp.addInternalFrame(fille);
        fille.setBounds(0, 0, 600, 500);
        fille.getGraphe().restore();
      }
    };
    SwingUtilities.invokeLater(r);

  }
}
