/*
 * @creation 25 mars 2005 @modification $Date: 2007-04-02 08:56:22 $ @license GNU General Public License 2 @copyright
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.courbe;

import java.awt.event.ActionEvent;

import javax.swing.JMenu;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMenuItem;

import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorTime;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGFilleSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaDurationChooserPanel;

/**
 * @author Fred Deniger
 * @version $Id: FudaaGrapheSimpleTimeFille.java,v 1.10 2007-04-02 08:56:22 deniger Exp $
 */
public class FudaaGrapheSimpleTimeFille extends EGFilleSimple {

  protected final FudaaCommonImplementation impl_;

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   * @param _t
   */
  public FudaaGrapheSimpleTimeFille(final EGFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _t) {
    super(_g, _titre, _appli, _id, _t);
    this.impl_ = _appli;
  }

  @Override
  protected void fillSpecificMenu(final JMenu _m) {
    super.fillSpecificMenu(_m);
    _m.addSeparator();
    final BuMenuItem it = new BuMenuItem(EbliLib.getS("Formater le temps..."));
    it.setActionCommand("TIME_FORMAT");
    it.addActionListener(this);
    _m.add(it);
  }

  CtuluNumberFormatI timeFmt_;

  /**
   * @param _timeFmt The timeFmt to set.
   */
  public void setTimeFmt(final CtuluNumberFormatI _timeFmt) {
    if (timeFmt_ != _timeFmt) {
      timeFmt_ = _timeFmt;
      updateGraphe(getGraphe(), timeFmt_);
    }
  }

  public static void updateGraphe(final EGGraphe _g, final CtuluNumberFormatI _timeFmt) {
    final EGAxeHorizontal h = _g.getTransformer().getXAxe();
    if (_timeFmt == null) {
      h.setUnite("s");
      h.setSpecificFormat(CtuluDurationFormatter.DEFAULT_TIME);

    } else {
      h.setSpecificFormat(_timeFmt);
      h.setUnite(null);
    }
    ((CtuluValueEditorTime) h.getValueEditor()).setFmt(_timeFmt);
    _g.getModel().fireAxeContentChanged(h);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if ("TIME_FORMAT".equals(_evt.getActionCommand())) {
      setTimeFmt(FudaaDurationChooserPanel.chooseFormatter(BuLib.HELPER, timeFmt_));

    } else {
      super.actionPerformed(_evt);
    }
  }

}
