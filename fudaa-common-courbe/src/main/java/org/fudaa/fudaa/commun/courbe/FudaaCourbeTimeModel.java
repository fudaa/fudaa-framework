/*
 * @creation 19 oct. 2004
 *
 * @modification $Date: 2007-05-22 14:20:38 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.courbe;

import org.fudaa.ctulu.*;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeTimeModel.java,v 1.3 2007-05-22 14:20:38 deniger Exp $
 */
public class FudaaCourbeTimeModel implements EGModel {
  protected String nom_;
  protected double[] time_;
  protected CtuluRange yRange_;
  protected double[] y_;

  public FudaaCourbeTimeModel(final double[] _timeIdx) {
    super();
    time_ = _timeIdx;
  }

  /**
   * ACHTUNG!!!; constructeur utilis� uniquement pour la persistance des donnees!!!
   */
  public FudaaCourbeTimeModel() {
  }

  public double[] getTime() {
    return time_;
  }

  protected void setTime(final double[] _time) {
    time_ = _time;
    y_ = null;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
  }

  protected void clearCache() {
    y_ = null;
    yRange_ = null;
  }

  protected void computeYRAnge() {
    yRange_ = new CtuluRange();
    if (getNbValues() == 0) {
      return;
    }
    yRange_.max_ = getY(0);
    yRange_.min_ = yRange_.max_;
    for (int i = getNbValues() - 1; i > 0; i--) {
      final double v = getY(i);
      if (v > yRange_.max_) {
        yRange_.max_ = v;
      }
      if (v < yRange_.min_) {
        yRange_.min_ = v;
      }
    }
  }

  public double[] getY() {
    return y_;
  }

  public void setY(final double[] _y) {
    y_ = _y;
    yRange_ = null;
  }

  @Override
  public boolean addValue(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean addValue(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean deplace(final int[] _selectIdx, final double _deltaX, final double _deltaY, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public String getPointLabel(int _i) {
    return null;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public int getNbValues() {
    if (y_ == null) {
      return 0;
    }
    return time_.length;
  }

  @Override
  public String getTitle() {
    return nom_;
  }

  @Override
  public double getX(final int _idx) {
    return CtuluLibArray.isEmpty(time_) ? 0 : time_[_idx];
  }

  @Override
  public double getXMax() {
    return CtuluLibArray.isEmpty(time_) ? 0 : time_[time_.length - 1];
  }

  @Override
  public double getXMin() {
    return CtuluLibArray.isEmpty(time_) ? 0 : time_[0];
  }

  @Override
  public double getY(final int _idx) {
    return y_ == null ? 0 : y_[_idx];
  }

  @Override
  public double getYMax() {
    if (yRange_ == null) {
      computeYRAnge();
    }
    return yRange_.max_;
  }

  @Override
  public double getYMin() {
    if (yRange_ == null) {
      computeYRAnge();
    }
    return yRange_.min_;
  }

  public final boolean isCalculating() {
    return y_ == null;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isPointDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return true;
  }

  @Override
  public boolean isSegmentDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  public boolean isVisibleLong() {
    return y_ == null;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  public boolean isYNull() {
    return y_ == null;
  }

  @Override
  public boolean removeValue(final int _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(final int[] _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  public void setTidx(final double[] _timeIdx) {
    time_ = _timeIdx;
  }

  @Override
  public boolean setTitle(final String _s) {
    if (_s != null && !_s.equals(nom_)) {
      nom_ = _s;
      return true;
    }
    return false;
  }

  @Override
  public boolean setValue(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    time_ = _x;
    y_ = _y;
    return true;
  }

  @Override
  public EGModel duplicate() {
    FudaaCourbeTimeModel duplic = new FudaaCourbeTimeModel(this.time_);
    if (this.yRange_ != null) {
      duplic.yRange_ = new CtuluRange(this.yRange_);
    }
    if (this.nom_ != null) {
      duplic.nom_ = nom_;
    }
    if (this.y_ != null) {
      duplic.y_ = CtuluLibArray.copy(y_);
    }

    return duplic;
  }

  @Override
  public Object savePersistSpecificDatas() {
    return nom_;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    if (data instanceof String) {
      nom_ = (String) data;
    }
    infos.put(FudaaCourbeTimePersistBuilder.TIME_MODEL, new FudaaCourbeTimeListModelDefault(time_, null));
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    //nothing to do here
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    //nothing to do here
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
