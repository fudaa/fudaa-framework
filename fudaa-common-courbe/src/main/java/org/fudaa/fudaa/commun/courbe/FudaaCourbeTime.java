/*
 * @creation 8 mars 07
 * 
 * @modification $Date: 2007-03-09 08:39:04 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.courbe;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;

/**
 * @author fred deniger
 * @version $Id: FudaaCourbeTime.java,v 1.1 2007-03-09 08:39:04 deniger Exp $
 */
public class FudaaCourbeTime extends EGCourbeChild implements ListSelectionListener, BSelecteurListTimeTarget,
    EbliAnimationAdapterInterface {

  final ListSelectionModel selection_;

  final FudaaCourbeTimeListModel timeModel_;

  public FudaaCourbeTime(EGGroup _m, EGModel _model, FudaaCourbeTimeListModel _timeModel) {
    super(_m, _model);
    timeModel_ = _timeModel;
    selection_ = new DefaultListSelectionModel();
    selection_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    selection_.setSelectionInterval(_timeModel.getSize() - 1, _timeModel.getSize() - 1);
    selection_.addListSelectionListener(this);
  }

  @Override
  protected EGCourbePersistBuilder<FudaaCourbeTime> createPersistBuilder() {
    return new FudaaCourbeTimePersistBuilder();
  }

  @Override
  public int getActiveTimeIdx() {
    return selection_.getMaxSelectionIndex();
  }

  @Override
  public double getTimeStepValueSec(int _idx) {
    return timeModel_.getTimeInSec(_idx);
  }

  @Override
  public ListModel getTimeListModel() {
    return timeModel_;
  }

  @Override
  public int getNbTimeStep() {
    return timeModel_.getSize();
  }

  @Override
  public String getTimeStep(int _idx) {
    return (String) timeModel_.getElementAt(_idx);
  }

  // public double getTimeStepValueSec(int _idx) {
  // return timeModel_.;
  // }

  @Override
  public void setTimeStep(int _idx) {
    selection_.setSelectionInterval(_idx, _idx);

  }

  @Override
  public ListSelectionModel getTimeListSelectionModel() {
    return selection_;
  }

  @Override
  public boolean isActiveTimeEnable() {
    return true;
  }

  @Override
  public void valueChanged(ListSelectionEvent _e) {
    fireCourbeAspectChanged(false);
  }

  /**
   * @return the timeModel_
   */
  public FudaaCourbeTimeListModel getTimeModel() {
    return timeModel_;
  }

}
