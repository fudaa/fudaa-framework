/*
 * @creation 8 mars 07
 * @modification $Date: 2007-03-09 08:39:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.courbe;

import com.memoire.bu.BuInformationsDocument;

import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author fred deniger
 * @version $Id: FudaaGrapheTimeAnimatedFille.java,v 1.1 2007-03-09 08:39:03 deniger Exp $
 */
public class FudaaGrapheTimeAnimatedFille extends FudaaGrapheTimeTreeFille implements EbliAnimatedInterface {

  public FudaaGrapheTimeAnimatedFille(FudaaGrapheTimeAnimatedVisuPanel _g, String _titre,
      FudaaCommonImplementation _appli, BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id);
  }

  @Override
  public EbliAnimationSourceInterface getAnimationSrc() {
    return ((FudaaGrapheTimeAnimatedVisuPanel) p_).getAnimationSrc();
  }

}
