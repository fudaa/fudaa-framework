/**
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2006-09-19 15:02:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import java.awt.Component;

import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcherDefault;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereFixe;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author Fred Deniger
 * @version $Id: FudaaGrapheSimpleModel.java,v 1.6 2006-09-19 15:02:22 deniger Exp $
 */
public class FudaaGrapheSimpleModel extends EGGrapheSimpleModel implements
    EvolutionListener {

   transient EvolutionListenerDispatcherDefault listener_;

  public FudaaGrapheSimpleModel() {}

  private void buildEvolListener(){
    if (listener_ == null) {
      listener_ = new EvolutionListenerDispatcherDefault();
      listener_.addEvolutionListener(this);
    }
  }

  private EGObject getCourbeWithEvol(final EvolutionReguliereInterface _e){
    if (element_ != null) {
      for (int i = element_.size() - 1; i >= 0; i--) {
        final EGCourbe c = getCourbe(i);
        final FudaaCourbeDefautModel model = (FudaaCourbeDefautModel) c.getModel();
        if (model.getEvolutionInterface() == _e) {
          return c;
        }
      }
    }
    return null;
  }

  @Override
  public void addNewCourbe(final CtuluCommandManager _cmd, final Component _p, final EGGraphe _graphe){
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    final BuTextField f = pn.addLabelStringText(FudaaLib.getS("Titre"));
    f.setColumns(10);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_p))) {
      addMutableCourbe(f.getText(), new double[] { 0, 1, 3}, new double[] { 0, 1, 3});
    }
  }

  @Override
  protected EGCourbeSimple doDuplicateCourbe(final EGCourbe _c){
    final FudaaCourbeDefautModel model = (FudaaCourbeDefautModel) _c.getModel();
    final EvolutionReguliereInterface e = model.getEvolutionInterface();
    return addMutableCourbe(_c.getTitle() + " new", e.getArrayX(), e.getArrayY());

  }

  public EGCourbeSimple addMutableCourbe(final String _nom,final double[] _x,final double[] _y){
    final EvolutionReguliere e = new EvolutionReguliere();
    buildEvolListener();
    e.setListener(listener_);
    e.setNom(_nom);
    e.setNom(_nom);
    e.add(_x, _y, null);
    final FudaaCourbeModel model = new FudaaCourbeModel(e);
    final EGAxeVertical v = new EGAxeVertical();
    v.setTitre(_nom);
    final EGCourbeSimple courbe = new EGCourbeSimple(v, model);
    super.addCourbe(courbe, null);
    return courbe;
  }

  public EGCourbeSimple addConstantCourbe(final String _nom,final double[] _x,final double[] _y){
    final EvolutionReguliereFixe e = new EvolutionReguliereFixe(_x, _y);
    buildEvolListener();
    e.setListener(listener_);
    e.setNom(_nom);
    e.setNom(_nom);
    final FudaaCourbeDefautModel model = new FudaaCourbeDefautModel(e);
    final EGAxeVertical v = new EGAxeVertical();
    v.setTitre(_nom);
    final EGCourbeSimple courbe = new EGCourbeSimple(v, model);
    super.addCourbe(courbe, null);
    return courbe;
  }

  /*  *//**
         * Ne fait rien
         */
  /*
   * public synchronized void addCourbe(EGCourbeSimple _obj,CtuluCommandManager _mng,int _idx){}
   *//**
      * Ne fait rien
      */
  /*
   * public synchronized void addCourbe(EGCourbeSimple _obj,CtuluCommandManager _mng){}
   */
  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e){
    final EGObject o = getCourbeWithEvol(_e);
    if (o != null) {
      super.fireObjectChanged(o);
    }
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new,
                                   final boolean _isAdjusting){}

}
