package org.fudaa.fudaa.commun.courbe;

import java.io.File;
import java.util.Map;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGCourbePersistBuilder;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.fudaa.commun.FudaaLib;

public class FudaaCourbeTimePersistBuilder extends EGCourbePersistBuilder<FudaaCourbeTime> {

  public final static String TIME_MODEL = "TIME_MODEL";

  @Override
  protected FudaaCourbeTime createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    EGGroup parent = getGroup(params);
    EGModel createModel = createModel(target, params);
    FudaaCourbeTimeListModel timeModel = (FudaaCourbeTimeListModel) params.get(TIME_MODEL);
    if (timeModel == null) {
      log.addError(FudaaLib.getS("Le temps n'a pas �t� trouv� pour la courbe {0}", target.getTitle()));
      return null;
    }
    return new FudaaCourbeTime(parent, createModel, timeModel);
  }

  @Override
  protected void postRestore(FudaaCourbeTime egObject, EGCourbePersist persist, Map params, CtuluAnalyze log) {
    super.postRestore(egObject, persist, params, log);
    int idx = persist.getSpecificIntValue("timeIdx", 0);
    egObject.selection_.setSelectionInterval(idx, idx);

  }

  @Override
  protected void postCreatePersist(EGCourbePersist res, FudaaCourbeTime courbe, EGGraphe graphe,File binFile) {
    super.postCreatePersist(res, courbe, graphe,binFile);
    res.saveSpecificData("timeIdx", courbe.selection_.getLeadSelectionIndex());
  }

}
