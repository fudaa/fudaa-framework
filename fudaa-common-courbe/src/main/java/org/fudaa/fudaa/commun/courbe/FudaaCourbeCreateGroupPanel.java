/*
 *  @creation     9 mars 2005
 *  @modification $Date: 2006-09-19 15:02:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

import com.memoire.bu.BuLabel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeCreateGroupPanel.java,v 1.9 2006-09-19 15:02:23 deniger Exp $
 */
public class FudaaCourbeCreateGroupPanel extends FudaaCourbeCreatorPanel {

  /**
   * @author fred deniger
   * @version $Id: FudaaCourbeCreateGroupPanel.java,v 1.9 2006-09-19 15:02:23 deniger Exp $
   */
  static final class CbCellTextRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value){
      if (_value == null) {
        setText(CtuluLibString.EMPTY_STRING);
      } else {
        setText(((EGGroup) _value).getTitle());
      }
    
    }
  }

  ComboBoxModel groupModel_;

  /**
   * @param _defaultNom le nom propose par defaut
   * @param _proposeXmin la valeur min proposee par defaut
   * @param _proposeXmax la valeur max proposee par defaut
   * @param _groupModel le model des groupes
   * @param _xValidator le validateur pour les x
   * @param _yValidator le validateur pour les y
   */
  public FudaaCourbeCreateGroupPanel(final String _defaultNom, final double _proposeXmin, final double _proposeXmax,
      final ComboBoxModel _groupModel, final EGGraphe _xValidator, final CtuluValueValidator _yValidator) {
    super(_defaultNom, _proposeXmin, _proposeXmax, _xValidator, _yValidator);
    groupModel_ = _groupModel;
    final JComboBox cb = new JComboBox();
    cb.setRenderer(new CbCellTextRenderer());
    cb.setModel(_groupModel);
    pnNorth_.add(cb, 0);
    pnNorth_.add(new BuLabel(FudaaLib.getS("Groupe:")), 0);
  }

  /**
   * @return le groupe sélectionné.
   */
  public EGGroup getSelectedGroup(){
    return (EGGroup) groupModel_.getSelectedItem();
  }

  @Override
  public boolean isDataValid(){
    final boolean r = super.isDataValid();
    boolean groupeValid = (groupModel_.getSelectedItem() != null);
    if (!groupeValid) {
      final String txt = FudaaLib.getS("Sélectionner le groupe de destination");
      if (getErrorText() != null && getErrorText().length() > 0) {
        setErrorText(getErrorText() + CtuluLibString.LINE_SEP + txt);
      } else {
        setErrorText(txt);
      }
    }
    return r && groupeValid ;
  }
}
