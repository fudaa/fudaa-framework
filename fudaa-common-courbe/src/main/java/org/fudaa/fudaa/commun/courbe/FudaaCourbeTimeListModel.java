package org.fudaa.fudaa.commun.courbe;

import javax.swing.ListModel;

import org.fudaa.ctulu.CtuluNumberFormatI;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeTimeListModel.java,v 1.1 2007-03-09 08:39:04 deniger Exp $
 */
public interface FudaaCourbeTimeListModel extends ListModel {

  double getTimeInSec(int _i);

  void setTimeFmt(CtuluNumberFormatI _fmt);
  
  CtuluNumberFormatI getTimeFmt();
  
  double[] getTimesInSec();
  
  /**
   * Appelee si des pas de temps ont �t� ajoutes ou enleves.
   */
  void fireStructureChanged();
  /**
   * Appelee si le contenu a ete modifie.
   */
  void fireContentChanged();
}