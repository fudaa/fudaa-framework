/**
 * @creation 24 juin 2004
 * @modification $Date: 2007-05-22 14:20:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.courbe;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeModel.java,v 1.8 2007-05-22 14:20:38 deniger Exp $
 */
public class FudaaCourbeModel extends FudaaCourbeDefautModel {

  /**
   * @param _e l'evolution support
   */
  public FudaaCourbeModel(final EvolutionReguliere _e) {
    super(_e);
  }
  /**
   * Achtung: utiliser uniqument pour la persistance des donnees.
   */
  public FudaaCourbeModel() {
	    super();
	  }

  @Override
  public boolean addValue(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return getEvolution().add(_x, _y, _cmd);
  }

  @Override
  public boolean addValue(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return getEvolution().add(_x, _y, _cmd);
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public boolean deplace(final int[] _select, final double _deltaX, final double _deltaY,
                         final CtuluCommandContainer _cmd) {
    return EGCourbeModelDefault.deplace(this, _select, _deltaX, _deltaY, _cmd);
  }

  /**
   * @return l'evolution servant de support a ce modele
   */
  public final EvolutionReguliere getEvolution() {
    return (EvolutionReguliere) e_;
  }

  @Override
  public int getNbValues() {
    return e_.getNbValues();
  }

  @Override
  public String getTitle() {
    return e_.getNom();
  }

  @Override
  public double getX(final int _idx) {
    return e_.getX(_idx);
  }

  @Override
  public double getXMax() {
    return e_.getMaxX();
  }

  /**
   * @see org.fudaa.ebli.courbe.EGModel#getXMin()
   */
  @Override
  public double getXMin() {
    return e_.getMinX();
  }

  /**
   * @see org.fudaa.ebli.courbe.EGModel#getY(int)
   */
  @Override
  public double getY(final int _idx) {
    return e_.getY(_idx);
  }

  /**
   * @see org.fudaa.ebli.courbe.EGModel#getYMax()
   */
  @Override
  public double getYMax() {
    return e_.getMaxY();
  }

  /**
   * @see org.fudaa.ebli.courbe.EGModel#getYMin()
   */
  @Override
  public double getYMin() {
    return e_.getMinY();
  }

  @Override
  public boolean isModifiable() {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return !e_.isUsed();
  }

  @Override
  public boolean isXModifiable() {
    return true;
  }

  @Override
  public boolean removeValue(final int _i, final CtuluCommandContainer _cmd) {
    return getEvolution().remove(_i, _cmd);
  }

  @Override
  public boolean removeValue(final int[] _i, final CtuluCommandContainer _cmd) {
    return getEvolution().remove(_i, _cmd);
  }

  @Override
  public boolean setTitle(final String _nom) {
    if (_nom != null && !_nom.equals(e_.getNom())) {
      e_.setNom(_nom);
      return true;
    }
    return false;
  }

  @Override
  public boolean setValue(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return getEvolution().setValue(_i, _x, _y, _cmd);
  }

  @Override
  public boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return getEvolution().setValues(_idx, _x, _y, _cmd);
  }

}
