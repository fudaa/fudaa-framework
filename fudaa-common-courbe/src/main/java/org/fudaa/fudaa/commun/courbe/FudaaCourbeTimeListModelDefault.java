/**
 * 
 */
package org.fudaa.fudaa.commun.courbe;

import javax.swing.event.ListDataListener;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluNumberFormatI;

public class FudaaCourbeTimeListModelDefault implements FudaaCourbeTimeListModel {
  final double[] times_;
  CtuluNumberFormatI timeFmt;

  public FudaaCourbeTimeListModelDefault(final double[] _times, CtuluNumberFormatI timeFmt) {
    super();
    times_ = _times;
    this.timeFmt = timeFmt;
  }

  @Override
  public Object getElementAt(final int _index) {
    return getTimeFmt() == null ? Double.toString(times_[_index]) : getTimeFmt().format(times_[_index]);
  }

  @Override
  public int getSize() {
    return times_ == null ? 0 : times_.length;
  }

  @Override
  public void fireContentChanged() {}

  @Override
  public void fireStructureChanged() {}

  @Override
  public CtuluNumberFormatI getTimeFmt() {
    return timeFmt;
  }

  @Override
  public double getTimeInSec(final int _i) {
    return times_[_i];
  }

  @Override
  public double[] getTimesInSec() {
    return CtuluLibArray.copy(times_);
  }

  @Override
  public void setTimeFmt(final CtuluNumberFormatI _fmt) {
    this.timeFmt = _fmt;
  }

  @Override
  public void addListDataListener(final ListDataListener _l) {}

  @Override
  public void removeListDataListener(final ListDataListener _l) {}

}
