/**
 * @creation 20 janv. 2005 @modification $Date: 2007-05-22 14:20:38 $ @license GNU General Public License 2 @copyright
 * (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import org.fudaa.ctulu.*;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.commun.FudaaLib;

import java.awt.*;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeDefautModel.java,v 1.9 2007-05-22 14:20:38 deniger Exp $
 */
public class FudaaCourbeDefautModel implements EGModel {

  protected EvolutionReguliereInterface e_;

  public final EvolutionReguliereInterface getEvolutionInterface() {
    return e_;
  }

  /**
   * @param _e
   */
  public FudaaCourbeDefautModel(final EvolutionReguliereInterface _e) {
    super();
    e_ = _e;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  /**
   * Achtung: utiliser uniqument pour la persistance des donnees.
   */
  public FudaaCourbeDefautModel() {
    super();
  }

  public int getActiveTimeIdx() {
    return 0;
  }

  public boolean isActiveTimeEnable() {
    return false;
  }

  public static void defaultFill(EvolutionReguliereInterface _e, InfoData _table,
          CtuluListSelectionInterface _selectedPt) {
    if (_e.isUsed()) {
      _table.put(FudaaLib.getS("Courbe utilis�e"), CtuluLibString.getString(_e.getUsed()) + CtuluLibString.ESPACE
              + CtuluLib.getS("fois"));
    } else {
      _table.put(FudaaLib.getS("Courbe non utilis�e"), CtuluLibString.ZERO);
    }

  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    defaultFill(e_, _table, _selectedPt);

  }

  @Override
  public boolean isPointDrawn(final int _i) {
    return true;
  }

  @Override
  public boolean isSegmentDrawn(final int _i) {
    return true;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public boolean addValue(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean addValue(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean deplace(final int[] _selectIdx, final double _deltaX, final double _deltaY,
                         final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public String getPointLabel(int _i) {
    return null;
  }

  @Override
  public int getNbValues() {
    return e_.getNbValues();
  }

  @Override
  public String getTitle() {
    return e_.getNom();
  }

  @Override
  public double getX(final int _idx) {
    return e_.getX(_idx);
  }

  @Override
  public double getXMax() {
    return e_.getMaxX();
  }

  @Override
  public double getXMin() {
    return e_.getMinX();
  }

  @Override
  public double getY(final int _idx) {
    return e_.getY(_idx);
  }

  @Override
  public double getYMax() {
    return e_.getMaxY();
  }

  @Override
  public double getYMin() {
    return e_.getMinY();
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isRemovable() {
    return false;
  }

  @Override
  public boolean isDuplicatable() {
    return true;
  }

  public boolean isVisibleLong() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean removeValue(final int _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(final int[] _i, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setTitle(final String _newName) {
    return false;
  }

  @Override
  public boolean setValue(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public EGModel duplicate() {
    return new FudaaCourbeDefautModel(this.e_.getCopy(this.e_.getListener()));
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
  }
  
  @Override
  public boolean isGenerationSourceVisible() {
  return false;
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

    @Override
    public int[] getInitRows() {
        return null;
    }
}
