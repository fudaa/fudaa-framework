/*
 *  @creation     25 mars 2005
 *  @modification $Date: 2006-09-19 15:02:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeDefaultModelName.java,v 1.3 2006-09-19 15:02:23 deniger Exp $
 */
public class FudaaCourbeDefaultModelName extends FudaaCourbeDefautModel {

  String name_;

  /**
   * @param _e
   */
  public FudaaCourbeDefaultModelName(final EvolutionReguliereInterface _e) {
    super(_e);
    name_=_e.getNom();
  }

  @Override
  public boolean isTitleModifiable(){
    return true;
  }

  @Override
  public String getTitle(){
    return name_;
  }

  @Override
  public boolean setTitle(final String _newName){
    if (_newName != null && !_newName.equals(name_)) {
      name_ = _newName;
      return true;
    }
    return false;
  }
}
