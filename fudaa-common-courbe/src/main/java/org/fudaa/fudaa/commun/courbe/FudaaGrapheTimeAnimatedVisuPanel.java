/*
 * @creation 8 mars 07
 * @modification $Date: 2007-03-09 08:39:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.courbe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.animation.ActionAnimationTreeSelection;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.calque.action.EbliCalqueActionTimeChooser;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGFillePanelAnimAdapter;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;

/**
 * @author fred deniger
 * @version $Id: FudaaGrapheTimeAnimatedVisuPanel.java,v 1.1 2007-03-09 08:39:03 deniger Exp $
 */
public class FudaaGrapheTimeAnimatedVisuPanel extends EGFillePanel implements EbliAnimatedInterface {
  ActionAnimationTreeSelection video_;

  public FudaaGrapheTimeAnimatedVisuPanel(EGGraphe _a) {
    this(_a, (EbliActionInterface[]) null);
  }

  public FudaaGrapheTimeAnimatedVisuPanel(EGGraphe _a, CtuluUI ui) {
    this(_a, (EbliActionInterface[]) null);
  }

  public FudaaGrapheTimeAnimatedVisuPanel(EGGraphe _a, EbliActionInterface[] _userAction) {
    super(_a, _userAction);
    List acts = new ArrayList();
    if (_userAction != null) acts.addAll(Arrays.asList(_userAction));
    acts.add(new EbliCalqueActionTimeChooser(getTreeModel().getSelectionModel(),true));
    createVideo();
    acts.add(getVideo());
    setPersonnalAction((EbliActionInterface[]) acts.toArray(new EbliActionInterface[acts.size()]));
  }
  
  @Override
  public EGFillePanel duplicate() {
    return new FudaaGrapheTimeAnimatedVisuPanel(vue_.getGraphe().duplicate());
  }

  protected void createVideo() {
    if (video_ == null) {
      video_ = new ActionAnimationTreeSelection(new EGFillePanelAnimAdapter(this));
      getTreeModel().getSelectionModel().addTreeSelectionListener(video_);
      video_.updateFromModel(getTreeModel().getSelectionModel());
      video_.updateStateBeforeShow();
    }
  }

  protected ActionAnimationTreeSelection getVideo() {
    return video_;
  }

  @Override
  public EbliAnimationSourceInterface getAnimationSrc() {
    createVideo();
    return video_.getSrc();
  }

  public EGGrapheTreeModel getTreeModel() {
    return (EGGrapheTreeModel) getGraphe().getModel();
  }

}
