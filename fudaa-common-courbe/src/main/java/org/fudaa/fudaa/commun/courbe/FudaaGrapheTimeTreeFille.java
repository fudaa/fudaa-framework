/*
 * @creation 14 mars 2005
 * 
 * @modification $Date: 2007-04-02 08:56:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JMenu;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuMenuItem;

import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGFilleTree;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaDurationChooserPanel;

/**
 * @author Fred Deniger
 * @version $Id: FudaaGrapheTimeTreeFille.java,v 1.12 2007-04-02 08:56:22 deniger Exp $
 */
public class FudaaGrapheTimeTreeFille extends EGFilleTree {

  protected FudaaCommonImplementation impl_;

  protected CtuluNumberFormatI timeFmt_;

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public FudaaGrapheTimeTreeFille(final EGFillePanel _g, final String _titre, final FudaaCommonImplementation _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id, new EGTableGraphePanel());
    setFrameIcon(EbliResource.EBLI.getFrameIcon("curves"));
    impl_ = _appli;
  }

  @Override
  protected void fillSpecificMenu(final JMenu _m) {
    super.fillSpecificMenu(_m);
    addTimeMenuItem(_m);
  }

  protected void addTimeMenuItem(final JMenu _m) {
    _m.addSeparator();
    final BuMenuItem it = new BuMenuItem(FudaaLib.getS("Formater le temps..."));
    it.setActionCommand("TIME_FORMAT");
    it.addActionListener(this);
    _m.add(it);
  }

  @Override
  protected String getMenuTitle() {
    return EbliLib.getS("Evolutions");
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if ("TIME_FORMAT".equals(_evt.getActionCommand())) {
      setTimeFmt(FudaaDurationChooserPanel.chooseFormatter(impl_.getFrame(), timeFmt_));

    } else {
      super.actionPerformed(_evt);
    }
  }

  /**
   * @param _timeFmt The timeFmt to set.
   */
  public void setTimeFmt(final CtuluNumberFormatI _timeFmt) {
    if (timeFmt_ != _timeFmt) {
      timeFmt_ = _timeFmt;
      List<EGCourbeChild> allCourbesChild = getGrapheTree().getAllCourbesChild();
      for (EGCourbeChild egCourbeChild : allCourbesChild) {
        if (egCourbeChild instanceof FudaaCourbeTime) {
          ((FudaaCourbeTime) egCourbeChild).getTimeModel().setTimeFmt(_timeFmt);
        }
      }
      FudaaGrapheSimpleTimeFille.updateGraphe(getGraphe(), timeFmt_);
    }
  }

  public CtuluNumberFormatI getTimeFmt() {
    return timeFmt_;
  }
}
