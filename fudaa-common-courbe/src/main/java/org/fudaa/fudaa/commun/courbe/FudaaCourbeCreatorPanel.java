/*
 *  @creation     9 mars 2005
 *  @modification $Date: 2007-04-16 16:35:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.courbe;

import gnu.trove.TDoubleArrayList;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author Fred Deniger
 * @version $Id: FudaaCourbeCreatorPanel.java,v 1.12 2007-04-16 16:35:34 deniger Exp $
 */
public class FudaaCourbeCreatorPanel extends CtuluDialogPanel implements ItemListener {

  private interface LineEdition {

    JComponent getComponent();

    EvolutionReguliere getEvol();

    boolean isValueValid();
  }

  class XYTableModel extends AbstractTableModel {

    TDoubleArrayList x_ = new TDoubleArrayList();
    TDoubleArrayList y_ = new TDoubleArrayList();
    JTable t_;

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public int getRowCount() {
      final int xNb = x_.size();
      final int yNb = y_.size();
      if (xNb == yNb) {
        return xNb + 1;
      }
      return xNb > yNb ? xNb : yNb;
    }

    protected String getX(final double _d) {
      final EGAxeHorizontal h = dest_.getTransformer().getXAxe();
      if (h != null && h.isSpecificFormat()) {
        return h.getSpecificFormat().format(_d);
      }
      return Double.toString(_d);
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return CtuluLib.getS("Abscisse");
      }
      return CtuluLib.getS("Ordonn�e");
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return true;
    }

    public boolean isValueValid() {
      boolean r = x_.size() > 0 && y_.size() > 0;
      if (!r) {
        setErrorText(EbliLib.getS("D�finir au moins un point"));
      }
      return r;
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_value == null) {
        return;
      }
      boolean r = false;
      boolean modif = false;
      try {
        final double d = Double.parseDouble(_value.toString());
        if (_columnIndex == 0 && (xEditeur_.isValid(_value.toString()))) {
          if (_rowIndex == x_.size()) {
            x_.add(d);
            r = true;
          } else {
            x_.set(_rowIndex, d);
          }
          modif = true;
        } else if (_columnIndex == 1 && (yVal_ == null || yVal_.isValueValid(d))) {
          if (_rowIndex == y_.size()) {
            y_.add(d);
            r = true;
          } else {
            y_.set(_rowIndex, d);
          }
          modif = true;
        }
      } catch (final NumberFormatException e) {
        r = false;
      }
      if (modif) {
        fireTableCellUpdated(_rowIndex, _columnIndex);
      }
      if (r && x_.size() == y_.size()) {
        fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
      }
    }

    public EvolutionReguliere createEvol() {
      final EvolutionReguliere r = new EvolutionReguliere();
      final int min = Math.min(x_.size(), y_.size());
      for (int i = 0; i < min; i++) {
        r.add(x_.get(i), y_.get(i));
      }
      return r;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0 && _rowIndex < x_.size()) {
        return getX(x_.get(_rowIndex));
      } else if (_columnIndex == 1 && _rowIndex < y_.size()) {
        return CtuluLib.getDouble(y_.get(_rowIndex));
      }
      return CtuluLibString.EMPTY_STRING;
    }
  }

  private class ListPoint extends BuPanel implements LineEdition {

    XYTableModel model_;

    ListPoint() {
      setLayout(new BuBorderLayout());
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(
          3, 3, 3, 3)));
      model_ = new XYTableModel();
      final JTable t = new EGTableGraphePanel.EvolTable(model_);
      t.setCellSelectionEnabled(true);
      final TableCellEditor text = xEditeur_.createTableEditorComponent();
      if (text != null) {
        t.getColumnModel().getColumn(0).setCellEditor(text);
      }
      final BuTextField txt = BuTextField.createDoubleField();
      if (yVal_ != null) {
        txt.setToolTipText(yVal_.getDescription());
      }
      txt.setValueValidator(yVal_);
      t.getColumnModel().getColumn(1).setCellEditor(new BuTableCellEditor(txt));
      EGTableGraphePanel.setDefaultEnterAction(t);
      add(new BuScrollPane(t));
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public EvolutionReguliere getEvol() {
      return model_.createEvol();
    }

    @Override
    public boolean isValueValid() {
      return model_.isValueValid();
    }
  }

  /**
   * @author fred deniger
   * @version $Id: FudaaCourbeCreatorPanel.java,v 1.12 2007-04-16 16:35:34 deniger Exp $
   */
  private final class StraightLine extends BuPanel implements LineEdition {

    BuTextField nb_;
    JComponent xmax_;

    JComponent xmin_;
    BuTextField ymax_;
    BuTextField ymin_;

    StraightLine(final double _xmin, final double _xmax) {
      setLayout(new BuGridLayout(2, 5, 5));
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(
          3, 3, 3, 3)));
      xmin_ = xEditeur_.createEditorComponent();
      xEditeur_.setValue(CtuluLib.getDouble(_xmin), xmin_);
      xmax_ = xEditeur_.createEditorComponent();
      xEditeur_.setValue(CtuluLib.getDouble(_xmax), xmax_);
      nb_ = FudaaCourbeCreatorPanel.this.addLabelIntegerText(this, EbliLib.getS("Nombre de points"));
      nb_.setText(CtuluLibString.DEUX);
      nb_.setValueValidator(BuValueValidator.MIN(1));
      ymin_ = BuTextField.createDoubleField();
      ymin_.setText(CtuluLibString.UN);
      ymax_ = BuTextField.createDoubleField();
      ymax_.setText(CtuluLibString.UN);
      BuPanel minPn = new BuPanel();
      minPn.setLayout(new BuGridLayout(2, 5, 0));
      FudaaCourbeCreatorPanel.this.addLabel(minPn, CtuluLib.getS("abscisse"));
      FudaaCourbeCreatorPanel.this.addLabel(minPn, CtuluLib.getS("ordonn�e"));
      FudaaCourbeCreatorPanel.this.addLabel(this, CtuluLibString.EMPTY_STRING);
      add(minPn);
      minPn = new BuPanel();
      minPn.setLayout(new BuGridLayout(2, 5, 0));
      minPn.add(xmin_);
      minPn.add(ymin_);
      FudaaCourbeCreatorPanel.this.addLabel(this, FudaaLib.getS("Premier point"));
      add(minPn);
      FudaaCourbeCreatorPanel.this.addLabel(this, FudaaLib.getS("Dernier point"));
      minPn = new BuPanel();
      minPn.setLayout(new BuGridLayout(2, 5, 0));
      minPn.add(xmax_);
      minPn.add(ymax_);
      ymax_.setColumns(8);
      ymin_.setColumns(8);
      add(minPn);
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public EvolutionReguliere getEvol() {

      final EvolutionReguliere ne = new EvolutionReguliere();
      EvolutionReguliere.createEvolution(ne, Double.parseDouble(xEditeur_.getStringValue(xmin_)), Double
          .parseDouble(xEditeur_.getStringValue(xmax_)), Double.parseDouble(ymin_.getText()), Double.parseDouble(ymax_
          .getText()), Integer.parseInt(nb_.getText()));
      return ne;
    }

    @Override
    public boolean isValueValid() {
      boolean xValid = xEditeur_.isValueValidFromComponent(xmin_);
      if (!xValid) {
        setErrorText(FudaaLib.getS("L'abscisse pour le premier point n'est pas valide"));
        return false;
      }
      xValid = xEditeur_.isValueValidFromComponent(xmax_);
      if (!xValid) {
        setErrorText(FudaaLib.getS("L'abscisse pour le dernier point n'est pas valide"));
        return false;
      }
      xValid = yVal_ == null || yVal_.isValueValid(ymin_.getValue());
      if (!xValid) {
        setErrorText(FudaaLib.getS("L'ordonn�e pour le premier point n'est pas valide"));
        return false;
      }
      xValid = (yVal_ == null || yVal_.isValueValid(ymax_.getValue()));
      if (!xValid) {
        setErrorText(FudaaLib.getS("L'ordonn�e pour le dernier point n'est pas valide"));
        return false;
      }
      xValid = nb_.getValueValidator().isValueValid(nb_.getValue());
      if (!xValid) {
        setErrorText(FudaaLib.getS("Le nombre de points n'est pas valide"));
        return false;
      }
      return true;
    }

  }

  BuCheckBox cbLine_;
  BuCheckBox cbNuage_;
  LineEdition current_;
  LineEdition line_;
  LineEdition list_;

  BuTextField txtTitle_;
  CtuluValueValidator yVal_;
  CtuluValueEditorI xEditeur_;
  BuPanel pnNorth_;
  EGGraphe dest_;

  public FudaaCourbeCreatorPanel(final String _defaultNom, final double _proposeXmin, final double _proposeXmax,
      final EGGraphe _g, final CtuluValueValidator _yValidator) {
    setLayout(new BuBorderLayout());
    dest_ = _g;
    if (dest_ == null) {
      throw new IllegalArgumentException("le graphe est null");
    }
    if (dest_.getTransformer() == null) {
      throw new IllegalArgumentException("le transformeur de graphe est null");
    }
    xEditeur_ = dest_.getTransformer().getXAxe().getValueEditor();
    yVal_ = _yValidator;
    pnNorth_ = new BuPanel();
    pnNorth_.setLayout(new BuGridLayout(2, 5, 5));
    cbLine_ = new BuCheckBox(FudaaLib.getS("Cr�er � partir d'une ligne"));
    cbNuage_ = new BuCheckBox(FudaaLib.getS("Cr�er � partir d'une liste de coordonn�es"));
    final ButtonGroup g = new ButtonGroup();
    g.add(cbLine_);
    g.add(cbNuage_);
    cbLine_.setSelected(true);
    txtTitle_ = super.addLabelStringText(pnNorth_, FudaaLib.getS("Nom"));
    if (_defaultNom != null) {
      txtTitle_.setText(_defaultNom);
    }
    txtTitle_.setColumns(10);
    pnNorth_.add(new BuLabel(FudaaLib.getS("Choisir le mode de cr�ation:")));
    pnNorth_.add(cbLine_);
    pnNorth_.add(new BuLabel());
    pnNorth_.add(cbNuage_);
    current_ = new StraightLine(_proposeXmin, _proposeXmax);
    line_ = current_;
    add(pnNorth_, BuBorderLayout.NORTH);
    add(current_.getComponent(), BuBorderLayout.CENTER);
    cbLine_.addItemListener(this);
    cbNuage_.addItemListener(this);
  }

  public EvolutionReguliere getEvol() {
    final EvolutionReguliere r = current_.getEvol();
    if (r == null) {
      return null;
    }
    r.setNom(txtTitle_.getText());
    return r;
  }

  private void changeEditor() {
    remove(current_.getComponent());
    if (cbLine_.isSelected()) {
      current_ = line_;
    } else {
      if (list_ == null) {
        list_ = new ListPoint();
      }
      current_ = list_;
    }
    add(current_.getComponent(), BuBorderLayout.CENTER);
    revalidate();
    repaint();
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.SELECTED) {
      changeEditor();
    }
  }

  @Override
  public boolean isDataValid() {
    setErrorText(null);
    final boolean r = current_.isValueValid();
    boolean v = txtTitle_.getText().trim().length() > 0;
    if (!v) {
      final String txt = FudaaLib.getS("Pr�ciser un nom pour la courbe");
      if (getErrorText() != null && getErrorText().length() > 0) {
        setErrorText(getErrorText() + CtuluLibString.LINE_SEP + txt);
      } else {
        setErrorText(txt);
      }
    }
    return r && v;
  }
}
