/*
 * @creation 8 ao�t 06
 * @modification $Date: 2007-01-17 10:45:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import com.memoire.fu.FuLog;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibImage;

/**
 * @author fred deniger
 * @version $Id: TestCtuluImage.java,v 1.1 2007-01-17 10:45:27 deniger Exp $
 */
public final class TestCtuluImage {
  private TestCtuluImage() {}

  private static BufferedImage readWithJava(ImageReader _read, int _size, int _w, int _h, float _ratio) {
    ImageReadParam params = _read.getDefaultReadParam();
    params.setSourceRegion(new Rectangle(_w - _size, _h - _size, _size, _size));
    try {
      BufferedImage res = _read.read(0, params);
      BufferedImage dest = new BufferedImage((int) Math.ceil(res.getWidth() * _ratio), (int) Math.ceil(res.getHeight()
          * _ratio), BufferedImage.TYPE_INT_RGB);
      Graphics2D g2d = dest.createGraphics();
      g2d.getRenderingHints().put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      g2d.drawImage(res, 0, 0, dest.getWidth(), dest.getHeight(), null);
      g2d.dispose();
      res.flush();
      return dest;
    } catch (IOException _evt) {
      FuLog.error(_evt);

    }
    return null;
  }

  public static void main(String[] _args) {
    System.out.println(CtuluImageExport.FORMAT_LIST);
    if (CtuluLibArray.isEmpty(_args)) {
      System.err.println("preciser un fichier");
      System.exit(1);
    }
    File f = new File(_args[0]);
    if (!f.exists()) {
      System.err.println("le fichier " + f.getAbsolutePath() + " n'existe pas");
      System.exit(1);
    }
    System.out.println("lecture de l'image " + f.getAbsolutePath());
    ImageReader reader = CtuluLibImage.getImageReader(f, null);
    if (reader == null) {
      System.err.println("Impossible de lire l'image");
      System.exit(1);
    }
    try {
      int h = reader.getHeight(0);
      int w = reader.getWidth(0);
      System.out.println("image w=" + w + ", h=" + h);
      int size = 512;
      if (h < size || w < size) {
        System.err.println("l'image est petite et le test n'aura pas d'importance");
        System.exit(1);
      }
      float ratio = 0.5f;
      int time = 10;
      for (int i = 0; i < time; i++) {
        readWithJava(reader, size, w, h, ratio);
      }
      long t0 = System.currentTimeMillis();

      t0 = System.currentTimeMillis();
      for (int i = 0; i < time; i++) {
        readWithJava(reader, size, w, h, ratio);
      }
      System.out.println("java " + (System.currentTimeMillis() - t0));
      System.out.println("fin");

    } catch (IOException _evt) {
      FuLog.error(_evt);
      System.exit(1);

    }

  }
}
