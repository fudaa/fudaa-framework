package org.fudaa.ctulu.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluDurationDateFormatter;

public class CtuluDurationDateFormatterComponentExample {

  public static void main(String[] args) {
    final CtuluDurationDateFormatterComponent cmp = new CtuluDurationDateFormatterComponent();
    cmp.buildComponents();
    
    
    JPanel pn = new JPanel(new BorderLayout());
    pn.add(cmp.buildPanel());
    JButton bt = new JButton("test value");
    pn.add(bt, BorderLayout.NORTH);
    final JLabel res = new JLabel();
    pn.add(res, BorderLayout.SOUTH);
    JFrame fr = new JFrame();
    fr.setContentPane(pn);
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        CtuluDurationDateFormatter buildFormatter = cmp.buildFormatter();
        res.setText(new Date(buildFormatter.getReferenceDate()) + " fmt=" + buildFormatter.getDatePattern());
        String res = buildFormatter.toLocalizedPattern();
        buildFormatter = (CtuluDurationDateFormatter) CtuluDurationDateFormatter.buildFromPattern(res);
        cmp.setInitFormatter(buildFormatter);

      }
    });
    fr.setSize(400, 400);
    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.setVisible(true);

  }

}
