/*
 * @creation 26 juil. 06
 * @modification $Date: 2007-01-17 10:45:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import javax.swing.JFrame;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.editor.CtuluExprGUI;

/**
 * @author fred deniger
 * @version $Id: TestExprGui.java,v 1.1 2007-01-17 10:45:27 deniger Exp $
 */
public final class TestExprGui {

  private TestExprGui() {}

  public static void main(final String[] _args) {
    final CtuluExpr expr = new CtuluExpr();
    expr.addVar("t", "time");
    final CtuluExprGUI gui = new CtuluExprGUI(expr);
    final JFrame f = new JFrame();
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.getContentPane().add(gui);
    f.pack();
    f.setVisible(true);

  }

}
