/*
 * @creation 23 juin 2003
 * @modification $Date: 2007-06-14 11:58:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;

import javax.swing.*;
import java.awt.*;

/**
 * @author deniger
 * @version $Id: CtuluFileChooser.java,v 1.7 2007-06-14 11:58:18 deniger Exp $
 */
public class CtuluFileChooser extends BuFileChooser {
  transient CtuluFileChooserFileTester tester_;
  private String text;

  public final CtuluFileChooserFileTester getTester() {
    return tester_;
  }

  public final void setTester(final CtuluFileChooserFileTester _tester) {
    tester_ = _tester;
  }

  public CtuluFileChooser() {
    this(true);
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  /**
   * Si _lastDir, le fileChooser est ouvert dans le dernier repertoire ouvert.
   *
   * @param _lastDir si true le rep courant est le dernier rep ouvert
   */
  public CtuluFileChooser(final boolean _lastDir) {
    super();
    if (_lastDir) {
      setCurrentDirectory(CtuluFavoriteFiles.getLastDir());
    }
  }

  /**
   * Permet d'enregistrer le dernier chemin utilise.
   */
  @Override
  public void approveSelection() {
    if (tester_ != null) {
      if (!tester_.isFileOk(getSelectedFile(), this)) {
        return;
      }
    }
    BuPreferences.BU.putStringProperty(CtuluFavoriteFiles.PREF_LAST, getCurrentDirectory()
        .getAbsolutePath());
    super.approveSelection();
  }

  protected void addOtherPanel(final JPanel _dest) {
  }

  @Override
  protected JDialog createDialog(final Component _parent) {
    final Frame frame = _parent instanceof Frame ? (Frame) _parent : (Frame) SwingUtilities
        .getAncestorOfClass(Frame.class, _parent);

    final String title = getUI().getDialogTitle(this);
    getAccessibleContext().setAccessibleDescription(title);
    final JDialog dialog = new JDialog(frame, title, true);

    if (JDialog.isDefaultLookAndFeelDecorated()) {
      final boolean supportsWindowDecorations = UIManager.getLookAndFeel().getSupportsWindowDecorations();
      if (supportsWindowDecorations) {
        dialog.getRootPane().setWindowDecorationStyle(JRootPane.FILE_CHOOSER_DIALOG);
      }
    }

    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuVerticalLayout(0, true, true));
    pn.setBorder(BuBorders.EMPTY2222);
    final JComponent createComponent = CtuluFavoriteFiles.INSTANCE.createComponent(this, dialog);
    createComponent.setBorder(BuBorders.EMPTY0505);
    pn.add(createComponent);
    pn.add(this);
    if (text != null) {
      final JLabel jLabel = new JLabel(text);
      jLabel.setBorder(BuBorders.EMPTY5555);
      pn.add(jLabel);
    }
    addOtherPanel(pn);
    pn.doLayout();
    dialog.setContentPane(pn);
    dialog.pack();
    if (FuLib.isWindows() && UIManager.getLookAndFeel().isNativeLookAndFeel()) {
      dialog.setMinimumSize(new Dimension(800, 500));
    }
    dialog.setLocationRelativeTo(_parent);

    return dialog;
  }
}
