/*
 * MySwing: Advanced Swing Utilites
 * Copyright (C) 2005  Santhosh Kumar T
 * <p/>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * <p/>
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

package org.fudaa.ctulu.gui;

import com.memoire.bu.BuInsets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.fudaa.ctulu.CtuluResource;

/**
 * Modifie pour ctulu.
 * 
 * @author santhosh kumar - santhosh@in.fiorano.com
 * @version $Id: CtuluButtonForPopup.java,v 1.3 2007-05-04 13:43:23 deniger Exp $
 */
public abstract class CtuluButtonForPopup extends JToggleButton implements PropertyChangeListener, ChangeListener,
    PopupMenuListener, ActionListener {

  final JButton mainButton_;

  JPopupMenu popup_;

  public CtuluButtonForPopup() {
    this(null);
  }

  public CtuluButtonForPopup(final JButton _main) {
    super(CtuluResource.CTULU.getIcon("popup.png"));
    setMargin(BuInsets.INSETS0000);

    getModel().addChangeListener(this);
    addActionListener(this);
    mainButton_ = _main;
    if (mainButton_ != null) {
      mainButton_.addPropertyChangeListener("enabled", this);
      mainButton_.getModel().addChangeListener(this);
    }
  }

  private void hidePopup() {
    if (mainButton_ != null) {
      mainButton_.getModel().setRollover(false);
    }
    getModel().setSelected(false);
    popup_.removePopupMenuListener(this); // act as good programmer :)
    popup_ = null;
  }

  protected abstract JPopupMenu buildPopup(JPopupMenu _popup);

  protected JPopupMenu createPopupMenu() {
    return buildPopup(new JPopupMenu());
  }

  @Override
  protected void processMouseEvent(final MouseEvent _e) {
    super.processMouseEvent(_e);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if (popup_ == null) {
      popup_ = createPopupMenu();
      popup_.addPopupMenuListener(this);
      popup_.show(this, 0, getHeight());
    } else {
      hidePopup();
    }
  }

  @Override
  public void popupMenuCanceled(final PopupMenuEvent _e) {}

  @Override
  public void popupMenuWillBecomeInvisible(final PopupMenuEvent _e) {
    doClick();
    setSelected(false);
  }

  @Override
  public void popupMenuWillBecomeVisible(final PopupMenuEvent _e) {
    if (mainButton_ != null) {
      mainButton_.getModel().setRollover(true);
    }
    getModel().setSelected(true);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (mainButton_ != null) {
      setEnabled(mainButton_.isEnabled());
    }
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    if (mainButton_ != null && _e.getSource() == mainButton_.getModel()) {
      if (popup_ != null && !mainButton_.getModel().isRollover()) {
        mainButton_.getModel().setRollover(true);
        return;
      }
      getModel().setRollover(mainButton_.getModel().isRollover());
      setSelected(mainButton_.getModel().isArmed() && mainButton_.getModel().isPressed());
    } else {
      if (popup_ != null && !getModel().isSelected()) {
        getModel().setSelected(true);
        return;
      }
      if (mainButton_ != null) {
        mainButton_.getModel().setRollover(getModel().isRollover());
      }
    }
  }

}
