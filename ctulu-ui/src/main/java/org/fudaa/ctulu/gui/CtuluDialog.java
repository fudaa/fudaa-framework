/*
 * @file EbliSimpleDialog.java
 * 
 * @creation 23 juin 2003
 * 
 * @modification $Date: 2007-06-05 08:57:46 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuToolButton;
import com.memoire.fu.FuLog;

/**
 * @author deniger
 * @version $Id: CtuluDialog.java,v 1.18 2007-06-05 08:57:46 deniger Exp $
 */
public class CtuluDialog extends CtuluEnhancedDialog implements ActionListener {

  /**
   * Bouton appliquer, annuler.
   */
  public static final int APPLY_CANCEL_OPTION = 5;

  /**
   * Option bouton cancel uniquement.
   */
  public static final int CANCEL_OPTION = 4;

  /**
   * Option boutons apply,ok.
   */
  public static final int OK_APPLY_OPTION = 2;

  /**
   * Option boutons cancel,apply,ok.
   */
  public static final int OK_CANCEL_APPLY_OPTION = 3;

  /**
   * Option boutons cancel,ok.
   */
  public static final int OK_CANCEL_OPTION = 1;

  /**
   * Option boutons cancel,ok.
   */
  public static final int QUIT_OPTION = 6;

  /**
   * Option : uniquement un bouton ok sera affiche. Reponse donnee par le dialogue si le bouton "ok" utilise.
   */
  public static final int OK_OPTION = 0;
  
  public static final int APPLY_OPTION = 7;
  
  public static final int ZERO_OPTION = -1;

  CtuluDialogPanel dial_;
  private BuPanel pnAction_;
    
  JButton btOk_;
  JButton btApply_;
  JButton btCancel_;
  JButton btQuit_;
  
  int option_;

  int response_;

  public CtuluDialog(final Dialog _c, final CtuluDialogPanel _dial) {
    super(_c);
    init(_dial);

  }

  public void setValue(final Object _o) {
    dial_.setValue(_o);
  }

  public Object getValue() {
    return dial_.getValue();
  }

  public CtuluDialog(final CtuluDialogPanel _dial) {
    super(BuLib.HELPER);
    init(_dial);
  }

  public CtuluDialog(final Frame _parent, final CtuluDialogPanel _dial) {
    super(_parent);
    init(_dial);
  }

  Component initParent_;

  public Component getInitParent() {
    return initParent_;
  }

  public void setInitParent(Component _initParent) {
    initParent_ = _initParent;
  }

  /**
   * Affiche le dialogue et le positionne par rapport a <code>_parent</code>.
   * 
   * @return le choix de l'utilisateur JOptionPane.DEFAULT_OPTION,JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION
   */
  public int afficheDialogModal() {
    setModal(true);
    afficheDialog();

    return response_;
  }

  public boolean afficheAndIsOk() {
    return CtuluDialogPanel.isOkResponse(afficheDialogModal());
  }

  /**
   * @param _r une action a lancer apres la construction de la boite de dialogue. *
   * @return le choix de l'utilisateur JOptionPane.DEFAULT_OPTION,JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION
   */
  public int afficheDialogModal(final Runnable _r) {
    if (_r == null) { return afficheDialogModal(); }
    setModal(true);
    afficheDialog(true, _r);
    return response_;
  }
  public int afficheDialogModal(final boolean modify) {
    setModal(true);
    afficheDialog(modify, null);
    return response_;
  }

  /**
   * @param _loc le point en haut a gauche de la nouvelle fenetre
   * @param _d la dimension de la nouvelle fenetre
   * @return le choix de l'utilisateur JOptionPane.DEFAULT_OPTION,JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION
   */
  public int afficheDialogModal(final Point _loc, final Dimension _d) {

    return afficheDialog(_loc, _d, true);
  }

  public int afficheDialog(final Point _loc, final Dimension _d, final boolean _modal) {
    setModal(_modal);
    setLocation(_loc);
    if (_d != null) setSize(_d);
    doLayout();
    afficheDialog(false);

    return response_;
  }

  /**
   * Affiche en mode non modale.
   */
  public void afficheDialog() {
    afficheDialog(true);
  }

  public void afficheDialog(final boolean _modify) {
    afficheDialog(_modify, null);
  }

  /**
   * @param _txt le textfield qui sera scrolle a la fin
   * @return le runnable a lancer
   */
  public static Runnable createRunnableToScroll(final JTextField _txt) {
    return new Runnable() {

      @Override
      public void run() {
        Rectangle rect = null;
        try {
          rect = _txt.modelToView(_txt.getText().length());
        } catch (final BadLocationException e) {
          FuLog.warning(e);
        }
        if (rect != null) {
          _txt.scrollRectToVisible(rect);
        }

      }
    };
  }

  /**
   * @param _modify true si la fenetre doit etre modifiee pour etre adapte au parent
   */
  public void afficheDialog(final boolean _modify, final Runnable _r) {
    final BuPanel r = new BuPanel();
    final JButton d = construitDialogPanel(r);
    setContentPane(r);
    if (_modify) {
      pack();
      if (getParent() == null && initParent_ == null) {
        final Dimension dialogSize = getSize();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        boolean set = false;
        if (dialogSize.width > screenSize.width * 3 / 4) {
          dialogSize.width = screenSize.width * 3 / 4;
          set = true;
        }
        if (dialogSize.height > screenSize.height * 3 / 4) {
          set = true;
          dialogSize.height = screenSize.height * 3 / 4;
        }
        if (set) {
          setSize(dialogSize);
        }
        setLocation((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2);
      } else {
        if (initParent_ != null) {
          setLocationRelativeTo(initParent_);
        } else setLocationRelativeTo(getParent());
      }
    }

    pack();
    // le bouton qui aura le focus par defaut
    if (d != null) {
      d.setFocusable(true);
      d.requestFocus();
      d.setDefaultCapable(true);
    }
    if (_r != null) {
      _r.run();
    }
    setVisible(true);

    if (isModal()) {
      dispose();
    }
  }

  protected JButton construireApply() {
    if (btApply_ == null) {
      btApply_ = construireBuButton(BuResource.BU.getString("Appliquer"), "APPLY");
      btApply_.setIcon(BuResource.BU.getIcon("appliquer"));
      btApply_.setMnemonic(KeyEvent.VK_A);
      btApply_.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          if (dial_ != null)
            dial_.setErrorText(null);
        }
      });
    }
    return btApply_;
  }

  /**
   * Construit le panel de la boite de dialogue.
   */
  private JButton construitDialogPanel(final BuPanel _princ) {
    _princ.setLayout(new BuBorderLayout(0, 0, true, true));
    final JLabel error = dial_.getError();
    if (error != null) {
      final BuScrollPane sc = new BuScrollPane(error);

      sc.setBorder(null);
      final FontMetrics fm = getFontMetrics(error.getFont());
      sc.setPreferredHeight(fm.getHeight() * 2);
      _princ.add(sc, BuBorderLayout.NORTH);
    }

    _princ.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _princ.add(dial_, BuBorderLayout.CENTER);
    pnAction_ = new BuPanel();
    pnAction_.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 0));
    pnAction_.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
//    JButton r = null;
//    switch (option_) {
//    case ZERO_OPTION:
//      break;
//    case OK_CANCEL_APPLY_OPTION:
//      r = construireOk();
//      pnAction_.add(r);
//      pnAction_.add(construireApply());
//      pnAction_.add(construireCancel());
//      break;
//    case OK_CANCEL_OPTION:
//      r = construireOk();
//      pnAction_.add(r);
//      pnAction_.add(construireCancel());
//      break;
//    case OK_OPTION:
//      r = construireOk();
//      pnAction_.add(r);
//      break;
//    case OK_APPLY_OPTION:
//      r = construireOk();
//      pnAction_.add(construireApply());
//      pnAction_.add(r);
//
//      break;
//    case CANCEL_OPTION:
//      r = construireCancel();
//      pnAction_.add(r);
//      break;
//    case APPLY_CANCEL_OPTION:
//      r = construireApply();
//      pnAction_.add(r);
//      pnAction_.add(construireCancel());
//      break;
//    case QUIT_OPTION:
//      r = construireQuit();
//      pnAction_.add(r);
//      break;
//    default:
//    }
    JButton r=rebuildButtons();
    
    BuPanel pnSouth=new BuPanel();
    pnSouth.setLayout(new BuBorderLayout(0, 0));
    pnSouth.add(pnAction_, BuBorderLayout.EAST);
    _princ.add(pnSouth, BuBorderLayout.SOUTH);
    
    if (dial_.getHelpText() != null) {
      final JButton b = new BuToolButton(BuResource.BU.getToolIcon("aide"));
      b.setToolTipText(BuResource.BU.getString("aide"));
      b.setActionCommand("HELP");
      b.addActionListener(this);
      pnSouth.add(b, BuBorderLayout.WEST);
    }
    return r;
  }

  private void init(final CtuluDialogPanel _dial) {
    construireApply();
    construireOk();
    construireQuit();
    construireCancel();
    
    dial_ = _dial;
    dial_.setDialog(this);
    // addWindowListener(this);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    response_ = JOptionPane.DEFAULT_OPTION;
    option_ = OK_CANCEL_OPTION;
  }

  /**
   * Construit un bouton ayant comme label <code>_text</code> et comme "ActionCommand" <code>_action</code>.
   */
  protected JButton construireBuButton(final String _text, final String _action) {
    final BuButton r = new BuButton(_text);
    r.setActionCommand(_action);
    r.addActionListener(this);
    return r;
  }

  protected JButton construireCancel() {
    if (btCancel_ == null) {
      btCancel_ = construireBuButton(BuResource.BU.getString("Annuler"), "CANCEL");
      btCancel_.setIcon(BuResource.BU.getIcon("annuler"));
      btCancel_.setMnemonic(KeyEvent.VK_N);
      btCancel_.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          if (dial_ != null)
            dial_.setErrorText(null);
        }
      });
    }
    return btCancel_;
  }

  protected JButton construireOk() {
    if (btOk_ == null) {
      btOk_ = construireBuButton(BuResource.BU.getString("Valider"), "OK");
      btOk_.setIcon(BuResource.BU.getIcon("valider"));
      final String text = btOk_.getText();
      if(text!=null){
        btOk_.setMnemonic(text.charAt(0));
      }
      btOk_.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          if (dial_ != null)
            dial_.setErrorText(null);
        }
      });
    }
    return btOk_;
  }

  protected JButton construireQuit() {
    if (btQuit_ == null) {
      btQuit_ = construireBuButton(BuResource.BU.getString("Fermer"), "QUIT");
      btQuit_.setIcon(BuResource.BU.getIcon("fermer"));
      final String text = btQuit_.getText();
      if(text!=null){
        btQuit_.setMnemonic(text.charAt(0));
      }
      btQuit_.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          if (dial_ != null)
            dial_.setErrorText(null);
        }
      });
    }
    return btQuit_;
  }

  /**
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String com = _e.getActionCommand();
    if ("OK".equals(com)) {
      ok();
    } else if ("CANCEL".equals(com) || "QUIT".equals(com)) {
      cancel();
    } else if ("APPLY".equals(com)) {
      apply();
    } else if ("HELP".equals(com)) {
      final JOptionPane p = new JOptionPane(dial_.getHelpText(), JOptionPane.PLAIN_MESSAGE);
      CtuluLibSwing.showDialog(this, BuResource.BU.getString("Aide"), p);
    }
  }

  /**
   * Appelle la methode apply du panneau.
   */
  public void apply() {
    if (dial_.isDataValid()) {
      dial_.apply();
    }
  }

  @Override
  public void cancel() {
    response_ = JOptionPane.CANCEL_OPTION;
    if (dial_.cancel()) {
    // B.M. L'appel a setVisible(false) empeche de réafficher le panel au travers d'un autre CtuluDialog.
    // dial_.setVisible(false);
      dispose();
    }
  }

  /**
   * Renvoie true si la reponse est egale a JOptionPane.DEFAULT_OPTION. Cela veut dire que l'utilisateur n'a pas valide
   * ou annule son choix.
   * 
   * @return si la reponse n'est pas ok ou cancel
   */
  public boolean isResponseUndefinied() {
    return response_ == JOptionPane.DEFAULT_OPTION;
  }

  public void ok() {
    if (dial_.isDataValid()) {
      response_ = JOptionPane.OK_OPTION;
      if (dial_.ok()) {
        // B.M. L'appel a setVisible(false) empeche de réafficher le panel au travers d'un autre CtuluDialog.
        // dial_.setVisible(false);
        dispose();
      }
    }
  }

  /**
   * @return Returns the option.
   */
  public int getOption() {
    return option_;
  }

  /**
   * @param _option The option to set.
   */
  public void setOption(final int _option) {
    option_ = _option;
    if (pnAction_!=null) {
      rebuildButtons();
    }
  }
  
  public void setButtonText(int _option, String _text) {
    switch (_option) {
    case OK_OPTION:
      btOk_.setText(_text);
      break;
    case APPLY_OPTION:
      btApply_.setText(_text);
      break;
    case QUIT_OPTION:
      btQuit_.setText(_text);
      break;
    case CANCEL_OPTION:
      btCancel_.setText(_text);
      break;
    default:
    }
  }
  
  public void setButtonIcon(int _option, Icon _icon) {
    switch (_option) {
    case OK_OPTION:
      btOk_.setIcon(_icon);
      break;
    case APPLY_OPTION:
      btApply_.setIcon(_icon);
      break;
    case QUIT_OPTION:
      btQuit_.setIcon(_icon);
      break;
    case CANCEL_OPTION:
      btCancel_.setIcon(_icon);
      break;
    default:
    }
  }
  
  /**
   * Rend actif ou inactif un bouton.
   * @param _button Le bouton
   * @param _b
   */
  public void setButtonEnabled(int _button, boolean _b) {
    switch (_button) {
    case OK_OPTION:
      btOk_.setEnabled(_b);
      break;
    case APPLY_OPTION:
      btApply_.setEnabled(_b);
      break;
    case QUIT_OPTION:
      btQuit_.setEnabled(_b);
      break;
    case CANCEL_OPTION:
      btCancel_.setEnabled(_b);
      break;
    default:
    }
  }
  
  /**
   * Reconstruit les boutons en cas de modification des options par exemple.
   */
  public JButton rebuildButtons() {
    pnAction_.removeAll();
    JButton r = null;
    switch (option_) {
    case ZERO_OPTION:
      break;
    case OK_CANCEL_APPLY_OPTION:
      r = construireOk();
      pnAction_.add(r);
      pnAction_.add(construireApply());
      pnAction_.add(construireCancel());
      break;
    case OK_CANCEL_OPTION:
      r = construireOk();
      pnAction_.add(r);
      pnAction_.add(construireCancel());
      break;
    case OK_OPTION:
      r = construireOk();
      pnAction_.add(r);
      break;
    case OK_APPLY_OPTION:
      r = construireOk();
      pnAction_.add(construireApply());
      pnAction_.add(r);

      break;
    case CANCEL_OPTION:
      r = construireCancel();
      pnAction_.add(r);
      break;
    case APPLY_CANCEL_OPTION:
      r = construireApply();
      pnAction_.add(r);
      pnAction_.add(construireCancel());
      break;
    case QUIT_OPTION:
      r = construireQuit();
      pnAction_.add(r);
      break;
    default:
    }
    pnAction_.doLayout();
    return r;
  }
}
