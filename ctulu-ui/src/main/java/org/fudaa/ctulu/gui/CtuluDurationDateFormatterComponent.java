package org.fudaa.ctulu.gui;

import com.memoire.bu.BuGridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import org.fudaa.ctulu.CtuluDurationDateFormatter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.jdesktop.swingx.JXDatePicker;

public class CtuluDurationDateFormatterComponent {

  private static final String DEFAULT = "DEFAULT";
  private JButton buttonInit;
  private JComboBox datePatternChooser;
  private JXDatePicker datePicker;

  private long defaultReferenceTime;
  private String defaultValuei18n;
  private JSpinner hourSpinner;
  private String initPattern;
  private long referenceDate;
  private JLabel lbResultat;
  private JSpinner minSpinner;

  public CtuluDurationDateFormatterComponent() {

  }

  @SuppressWarnings("serial")
  public void buildComponents() {
    if (lbResultat != null) {
      return;
    }
    if (defaultReferenceTime > 0) {
      buttonInit = new JButton(CtuluLib.getS("Utiliser la valeur par d�faut"));
      buttonInit.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          setReferenceDate(defaultReferenceTime);
        }
      });
    }
    lbResultat = new JLabel();
    datePicker = new JXDatePicker();
    SpinnerNumberModel hourModel = new SpinnerNumberModel();
    hourModel.setMaximum(23);
    hourModel.setMinimum(0);
    hourModel.setStepSize(1);
    hourSpinner = new JSpinner(hourModel);
    SpinnerNumberModel minModel = new SpinnerNumberModel();
    minModel.setMaximum(59);
    minModel.setMinimum(0);
    minModel.setStepSize(1);
    minSpinner = new JSpinner(minModel);
    defaultValuei18n = CtuluResource.CTULU.getString("Formatteur par d�faut");
    if (referenceDate > 0) {
      updateTime();
    }

    datePatternChooser = new JComboBox();
    datePatternChooser.setEditable(true);

    List<String> patterns = new ArrayList<String>();
    if (initPattern != null) {
      patterns.add(initPattern);
    }
    patterns.add("dd/MM/yy HH:mm:ss");
    patterns.add("dd/MM/yyyy HH:mm:ss");
    patterns.add("yyyy-MM-dd HH:mm:ss");
    patterns.add(DEFAULT);
    datePatternChooser.setRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(Object _value) {
        if (_value == DEFAULT) {
          super.setValue(defaultValuei18n);
        } else {
          super.setValue(_value);
        }
      }
    });

    datePatternChooser.setModel(new DefaultComboBoxModel(patterns.toArray()));
    if (initPattern != null) {
      datePatternChooser.setSelectedItem(initPattern);
    } else {
      datePatternChooser.setSelectedItem(DEFAULT);
    }
    updateLabelResultat();
    final ComboBoxEditor editor = new BasicComboBoxEditor() {
      @Override
      public void setItem(Object anObject) {
        if (anObject == DEFAULT) {
          super.setItem(defaultValuei18n);
        } else {
          super.setItem(anObject);
        }
      }
    };
    ((JTextField) editor.getEditorComponent()).getDocument().addDocumentListener(new DocumentListener() {

      @Override
      public void changedUpdate(DocumentEvent e) {
        updateLabelResultat(((JTextField) editor.getEditorComponent()).getText());

      }

      @Override
      public void insertUpdate(DocumentEvent e) {
        updateLabelResultat(((JTextField) editor.getEditorComponent()).getText());

      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        updateLabelResultat(((JTextField) editor.getEditorComponent()).getText());

      }
    });
    datePatternChooser.setEditor(editor);
    datePatternChooser.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        updateLabelResultat();
      }
    });

  }

  public CtuluDurationDateFormatter buildFormatter() {
    CtuluDurationDateFormatter res = new CtuluDurationDateFormatter();
    Calendar c = Calendar.getInstance();
    Date date = datePicker.getDate();
    c.setTime(date == null ? new Date() : date);
    c.set(Calendar.MILLISECOND, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MINUTE, ((Number) minSpinner.getValue()).intValue());
    c.set(Calendar.HOUR_OF_DAY, ((Number) hourSpinner.getValue()).intValue());
    res.setReferenceDate(c.getTimeInMillis());
    String selectedItem = (String) datePatternChooser.getSelectedItem();
    if (selectedItem == DEFAULT) {
      res.setDatePattern(null);
    } else {
      res.setDatePattern(selectedItem);
    }
    return res;

  }

  public JPanel buildPanel() {
    buildComponents();
    JPanel res = new JPanel();
    res.setLayout(new BuGridLayout(2, 10, 15, true, true, true, true));
    if (buttonInit != null) {
      res.add(new JLabel());
      res.add(buttonInit);
    }
    res.add(new JLabel(CtuluLib.getS("Date de r�f�rence")));
    JPanel pnDate = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
    pnDate.add(datePicker);
    pnDate.add(new JLabel("hh:"));
    pnDate.add(hourSpinner);
    pnDate.add(new JLabel("min:"));
    pnDate.add(minSpinner);
    res.add(pnDate);
    String sep = ": ";
    res.add(new JLabel(CtuluLib.getS("Formattage de la date") + sep));
    res.add(datePatternChooser);
    res.add(new JLabel(CtuluLib.getS("Aper�u formattage") + sep));
    res.add(lbResultat);
    res.add(new JLabel(CtuluLib.getS("Aide pour le formattage") + sep));
    JLabel lbHelp = new JLabel(
        "<html><body><table border=1><tr><td>yy ou yyyy</td><td>Ann�e</td></tr><tr><td>MM ou MMM</td><td>Mois</td></tr><tr><td>dd</td><td>Jour</td></tr><tr><td>hh ou HH </td><td>Heures ou Heures 24</td></tr><tr><td>mm</td><td>minutes</td></tr><tr><td>ss</td><td>secondes</td></tr>");
    res.add(lbHelp);

    return res;
  }

  public JButton getButtonInit() {
    return buttonInit;
  }

  public JComboBox getDatePatternChooser() {
    return datePatternChooser;
  }

  public JXDatePicker getDatePicker() {
    return datePicker;
  }

  public String getInitPattern() {
    return initPattern;
  }

  public long getInitTime() {
    return referenceDate;
  }

  public JLabel getLbResultat() {
    return lbResultat;
  }

  public void setButtonInitDefaultText(String txt) {
    if (buttonInit != null)
      buttonInit.setText(txt);
  }

  public void setDefaultReferenceTime(long defaultTime) {
    this.defaultReferenceTime = defaultTime;
  }

  public void setInitFormatter(CtuluDurationDateFormatter formatter) {
    if (formatter != null) {
      setReferenceDate(formatter.getReferenceDate());
      String pattern = formatter.getDatePattern();
      if (CtuluLibString.isEmpty(pattern)) {
        pattern = DEFAULT;
      }
      setInitPattern(pattern);
    }
  }

  public void setInitPattern(String initPattern) {
    this.initPattern = initPattern;
    if (lbResultat != null) {
      updateLabelResultat();
    }
  }

  public void setReferenceDate(long defaultTime) {
    this.referenceDate = defaultTime;
    if (datePicker != null) {
      updateTime();
    }
  }

  private void updateLabelResultat() {
    String pattern = (String) datePatternChooser.getSelectedItem();
    updateLabelResultat(pattern);
  }

  private void updateLabelResultat(String pattern) {

    SimpleDateFormat simpleDateFormat = null;
    if (pattern != DEFAULT) {
      try {
        simpleDateFormat = new SimpleDateFormat(pattern);
      } catch (Exception e) {
      }
    }
    if (simpleDateFormat == null) {
      simpleDateFormat = CtuluDurationDateFormatter.createDefaultSimpleDateFormat();
    }
    lbResultat.setText(simpleDateFormat.format(new Date()));
  }

  private void updateTime() {
    Date date = new Date(referenceDate);
    datePicker.setDate(date);
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    hourSpinner.setValue(c.get(Calendar.HOUR_OF_DAY));
    minSpinner.setValue(c.get(Calendar.MINUTE));
  }

}
