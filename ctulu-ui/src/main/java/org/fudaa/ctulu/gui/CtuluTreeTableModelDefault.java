/*
 * @creation 30 janv. 2006
 * 
 * @modification $Date: 2007-04-16 16:33:53 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.util.List;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.jdesktop.swingx.JXTreeTable;

/**
 * @author fred deniger
 * @version $Id: CtuluTableModelDefault.java,v 1.4 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluTreeTableModelDefault implements CtuluTableModelInterface {

  int[] column;
  final JXTreeTable t_;
  List<String[]> values;
  boolean commentSetByUser;
  List<String> comments;

  public CtuluTreeTableModelDefault(final JXTreeTable _t, final int[] _column, final int[] row) {
    super();
    column = _column;
    t_ = _t;
    values = CopyAllTableToClipboardRunnable.getValues(_t, row);
  }

  public void setComments(List<String> comments) {
    commentSetByUser = true;
    this.comments = comments;
  }

  @Override
  public List<String> getComments() {
    if (commentSetByUser) {
      return comments;
    }
    return (List<String>) t_.getClientProperty(CtuluTableModelInterface.EXPORT_COMMENT_PROPERTY);
  }

  private int getRealColumn(int ask) {
    if (column == null) {
      return ask;
    }
    return column[ask];

  }

  @Override
  public String getColumnName(final int _i) {
    return t_.getColumnName(getRealColumn(_i));
  }

  @Override
  public WritableCell getExcelWritable(int _row, int _col, int _rowInXls, int _colInXls) {

    int r = _row;
    int c = getRealColumn(_col);
    String s = getValue(r, c);
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    if (CtuluLibString.isNumeric(s)) {
      try {
        return new Number(_colInXls, _rowInXls, Double.parseDouble(s));
      } catch (final NumberFormatException e) {
      }
    }
    return new Label(_colInXls, _rowInXls, s);
  }

  @Override
  public int getMaxCol() {
    return column == null ? t_.getColumnCount() : column.length;
  }

  @Override
  public int getMaxRow() {
    return values.size();
  }

  @Override
  public String getValue(final int _row, final int _col) {
    return values.get(_row)[getRealColumn(_col)];
  }

    @Override
    public int[] getSelectedRows() {
       return null;
    }
}