/*
 * @creation 23 juin 2003
 * @modification $Date: 2007-02-02 11:20:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Observer;
import javax.swing.*;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Permet de gerer les dossiers favoris et le dernier repertoire favori.
 * 
 * @author deniger
 * @version $Id: CtuluFavoriteFiles.java,v 1.7 2007-02-02 11:20:10 deniger Exp $
 */
public final class CtuluFavoriteFiles {

  /**
   * Le prefix pour les favoris.
   */
  public static final String PREF_ID = "explorer.favorite.";

  /**
   * Le suffixe pardi.
   */
  public static final String SUF_ID = ".path";

  /**
   * La cle de la pref pour le nombre de dossier favoris.
   */
  public static final String PREF_NB = "explorer.favorite.nb";

  /**
   * La cle pour le dernier rep ouvert.
   */
  public static final String PREF_LAST = "explorer.last.dir";

  /**
   * Le singleton.
   */
  public static transient final CtuluFavoriteFiles INSTANCE = new CtuluFavoriteFiles();

  transient List list_;

  private transient Observer observer_;

  private CtuluFavoriteFiles() {
    loadFavorites();
  }

  /**
   * @return le dernier dossier visite selon la pref PREF_LAST
   */
  public static File getLastDir() {
    return new File(getLastDirPath());
  }

  /**
   * @return le dernier dossier visite selon la pref PREF_LAST
   */
  public static String getLastDirPath() {
    return BuPreferences.BU.getStringProperty(CtuluFavoriteFiles.PREF_LAST, FuLib.getUserHome());
  }

  /**
   * L'observer recevra un update avec des arguments null a chaque modif des favoris.
   * 
   * @param _o le nouvel Observer
   */
  public void setObserver(final Observer _o) {
    observer_ = _o;
  }

  /**
   * @param _f un nouveau rep favoris
   */
  public void addFile(final File _f) {
    if (CtuluLibFile.exists(_f) && (_f.isDirectory())) {
      final String s = _f.getAbsolutePath();
      if (!list_.contains(s)) {
        list_.add(s);
        fireChange();
      }
    }
  }

  public int getNbFavoritesFile() {
    return list_ == null ? 0 : list_.size();
  }

  /**
   * @return l'observeur principal
   */
  public Observer getObserver() {
    return observer_;
  }

  private void loadFavorites() {
    list_ = new ArrayList(20);
    final int nb = BuPreferences.BU.getIntegerProperty(PREF_NB, -1);
    if (nb < 0) {
      for (final Enumeration e = BuPreferences.BU.keys(); e.hasMoreElements();) {
        final String k = (String) e.nextElement();
        if (k.startsWith(PREF_ID)) {
          String val = BuPreferences.BU.getStringProperty(k, null);
          if (val != null) {
            val = val.trim();
            final File f = new File(val);
            if ((val.length() == 0) || (!f.exists()) || (!f.isDirectory())) {
              BuPreferences.BU.removeProperty(k);
            } else {
              list_.add(val);
            }
          }
        }
      }
    }
    for (int i = 1; i <= nb; i++) {
      final String k = PREF_ID + i + SUF_ID;
      String val = BuPreferences.BU.getStringProperty(k, null);
      if (val != null) {
        val = val.trim();
        final File f = new File(val);
        if ((val.length() == 0) || (!f.exists()) || (!f.isDirectory())) {
          BuPreferences.BU.removeProperty(k);
        } else {
          list_.add(val);
        }
      }
    }
    Collections.sort(list_);
  }

  public void fillMenu(final BuMenu _m, final ActionListener _l) {
    final String pref = "DIR_";
    for (int i = 0; i < list_.size(); i++) {
      final String s = (String) list_.get(i);
      final BuMenuItem it = _m.addMenuItem(FuLib.reducedPath(s), pref + s);
      it.addActionListener(_l);
      it.setToolTipText(s);
    }
  }

  public static boolean isFavAction(final String _s) {
    return (_s != null) && (_s.startsWith("DIR_"));
  }

  public String getPathFromMenuCommand(final String _init) {
    if ((_init != null) && (_init.length() > 4)) {
      return _init.substring(4);
    }
    return null;
  }

  class FavoriteModel extends AbstractListModel implements ComboBoxModel {

    transient Object selected_;

    /**
     * @return l'obj selectionne
     */
    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if ((_anItem != null) && (!_anItem.equals(selected_)) || ((_anItem == null) && selected_ != null)) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    /**
     * @param _o l'objet a tester
     * @return true si l'objet est contenu dans la liste
     */
    public boolean contains(final Object _o) {
      return list_.contains(_o);
    }

    public void addElement(final Object _obj) {
      if ((_obj instanceof String) && (((String) _obj).trim().length() > 0)) {
        list_.add(_obj);
        Collections.sort(list_);
        fireContentsChanged(this, 0, FavoriteModel.this.getSize());
      }
    }

    public void insertElementAt(final Object _obj, final int _index) {
      addElement(_obj);
    }

    /**
     * Removes an item at a specific index.
     * 
     * @param _i l'index a supprimer et pan!
     */
    public void removeElementAt(final int _i) {
      if (_i > 0) {
        list_.remove(_i - 1);
        fireIntervalRemoved(this, _i, _i);
      }
    }

    protected void update() {
      if (list_.contains(getSelectedItem())) {
        setSelectedItem(null);
      }
      fireContentsChanged(this, 0, list_.size());
    }

    @Override
    public Object getElementAt(final int _arg) {
      if (_arg == 0) {
        return CtuluLibString.EMPTY_STRING;
      }
      return list_.get(_arg - 1);
    }

    @Override
    public int getSize() {
      return list_.size() + 1;
    }

    public void removeElement(final Object _obj) {
      removeElementAt(list_.indexOf(_obj));
    }
  }

  /**
   * @param _f le file chooser dans lequel le composant sera affiche
   * @return un composant a ajouter au JFileChooser _f
   */
  public JComponent createComponent(final JFileChooser _f, final Dialog _d) {
    final BuPanel pnMain = new BuPanel();
    pnMain.setLayout(new BuBorderLayout(2, 2));
    final JLabel lb = new JLabel(CtuluLib.getS("Favoris") + ':');
    pnMain.add(lb, BuBorderLayout.WEST);
    final FavoriteModel model = new FavoriteModel();
    final JComboBox j = new JComboBox(model);
    final CtuluCellTextRenderer cellRenderer = new CtuluCellTextRenderer() {

      @Override
      protected void setValue(Object _v) {
        final String s = (String) _v;
        if (s != null) {
          setToolTipText(s);
          setText(FuLib.reducedPath(s));
        }
      }
    };
    cellRenderer.setIcon(BuResource.BU.getToolIcon("ouvrir"));
    j.setRenderer(cellRenderer);
    j.setSelectedItem(CtuluLibString.EMPTY_STRING);
    j.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _evt) {
        if (_evt.getStateChange() == ItemEvent.SELECTED) {
          if (model.getSelectedItem() != null) {
            final String s = (String) model.getSelectedItem();
            if (s.length() == 0) {
              return;
            }
            _f.setCurrentDirectory(new File(FuLib.expandedPath(s)));
          }
        }
      }
    });
    pnMain.add(j, BuBorderLayout.CENTER);
    BuToolButton bt = new BuToolButton(BuResource.BU.loadMenuCommandIcon("oui"));
    bt.setToolTipText(CtuluLib.getS("Ajouter le répertoire courant aux favoris"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _ae) {
        final String s = _f.getCurrentDirectory().getAbsolutePath();
        if (!list_.contains(s)) {
          model.addElement(s);
          CtuluFavoriteFiles.this.fireChange();
        }
      }
    });
    final BuPanel pnBt = new BuPanel();
    pnBt.setLayout(new BuButtonLayout(1, SwingConstants.CENTER));
    pnBt.add(bt);
    bt = new BuToolButton(BuResource.BU.loadMenuCommandIcon("editer"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _ae) {
        editFavorite(j);
        model.update();
      }
    });
    bt.setToolTipText(CtuluLib.getS("Editer les favoris"));
    pnBt.add(bt);
    pnMain.add(pnBt, BuBorderLayout.EAST);
    pnMain.revalidate();
    final Dimension d = pnMain.getPreferredSize();
    _d.doLayout();
    d.width = _d.getPreferredSize().width;
    pnMain.setPreferredSize(d);
    return pnMain;
  }

  /**
   * Affiche une boite de dialogue editant les favoris.
   * 
   * @param _parent le composant parent
   */
  public void editFavorite(final JComponent _parent) {
    final CtuluListEditorModel listmodel = new CtuluListEditorModel(list_, false);
    final CtuluListEditorPanel editor = new CtuluListEditorPanel(listmodel, false, true, false) {

      @Override
      public void initCellRendererEditor() {
        final CtuluCellTextRenderer txt = new CtuluCellTextRenderer();
        txt.setIcon(BuResource.BU.getToolIcon("ouvrir"));
        table_.getColumnModel().getColumn(0).setCellRenderer(txt);
      }
    };
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    pn.add(editor, BuBorderLayout.CENTER);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_parent, CtuluLib.getS("Edition des favoris")))) {
      list_ = listmodel.getValuesInList();
      fireChange();
    }
  }

  /**
   * Enregistre les preferences dans FudaaPreferences mais n'ecrit pas le fichier de pref.
   */
  public void saveFavorites() {
    final int nb = BuPreferences.BU.getIntegerProperty(PREF_NB, -1);
    if (nb < 0) {
      for (final Enumeration e = BuPreferences.BU.keys(); e.hasMoreElements();) {
        final String k = (String) e.nextElement();
        if (k.startsWith(PREF_ID)) {
          BuPreferences.BU.removeProperty(k);
        }
      }
    } else {
      for (int i = 1; i <= nb; i++) {
        BuPreferences.BU.removeProperty(PREF_ID + i + SUF_ID);
      }
    }
    final int n = list_.size();
    int idxN = 0;
    for (int i = 0; i < n; i++) {
      final String key = PREF_ID + CtuluLibString.getString(idxN + 1) + SUF_ID;
      final String val = (String) list_.get(i);
      final File f = new File(val);
      if (f.exists() && f.isDirectory()) {
        BuPreferences.BU.putStringProperty(key, val);
        idxN++;
      }
    }
    BuPreferences.BU.putIntegerProperty(PREF_NB, idxN);
  }

  protected void fireChange() {
    saveFavorites();
    if (observer_ != null) {
      observer_.update(null, null);
    }
  }
}