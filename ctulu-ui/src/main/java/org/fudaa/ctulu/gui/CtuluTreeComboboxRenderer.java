/*
 *  @creation     16 juin 2005
 *  @modification $Date: 2006-12-05 10:12:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuGridLayout;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTreeComboboxRenderer.java,v 1.4 2006-12-05 10:12:51 deniger Exp $
 */
public class CtuluTreeComboboxRenderer extends JPanel implements ListCellRenderer {

  private final JTree tree_;
  final TreeModel treeModel_;
  final TreeCellRenderer treeRenderer_;
  IndentBorder indentBorder_ = new IndentBorder();

  public CtuluTreeComboboxRenderer(final TreeModel _treeModel, final TreeCellRenderer _treeRenderer) {
    tree_ = new JTree();
    this.treeModel_ = _treeModel;
    this.treeRenderer_ = _treeRenderer;
    setLayout(new BuGridLayout(1, 0, 0, true, true));
    setBorder(indentBorder_);
    setOpaque(true);
  }

  private static class IndentBorder extends EmptyBorder {

    final int indent_ = UIManager.getInt("Tree.leftChildIndent");

    public IndentBorder() {
      super(1, 0, 0, 0);
    }

    public void setDepth(final int _depth) {
      left = indent_ * _depth;
    }
  }

  protected void update(final JList _l, final JComponent _treeRender, final boolean _isSelected, final Object _value) {
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
          final boolean _isSelected, final boolean _cellHasFocus) {
    if (_value == null) { // if selected value is null
      removeAll();
      return this;
    }

    final boolean leaf = treeModel_.isLeaf(_value);
    final boolean isSelectable = ((CtuluTreeComboboxModel) _list.getModel()).isSelectable(_value);
    final boolean hasFocus = isSelectable && _cellHasFocus;
    final boolean isSelected = isSelectable && _isSelected;
    final Component comp = treeRenderer_.getTreeCellRendererComponent(tree_, _value, isSelected, true, leaf, _index,
            hasFocus);
    removeAll();
    ((JComponent) comp).setAlignmentY(0.5f);
    ((JComponent) comp).setOpaque(false);
    ((JComponent) comp).setFont(_list.getFont());
    add(comp);
    revalidate();
    if (isSelected && _index >= 0) {
      setBackground(_list.getSelectionBackground());
      ((JComponent) comp).setForeground(_list.getSelectionForeground());
    } else {
      setBackground(_list.getBackground());
      ((JComponent) comp).setForeground(_list.getForeground());
    }
    update(_list, (JComponent) comp, isSelected, _value);
    final CtuluTreeComboboxModel.PreorderEnumeration enumer = new CtuluTreeComboboxModel.PreorderEnumeration(treeModel_);
    for (int i = 0; i <= _index; i++) {
      enumer.nextElement();
    }
    indentBorder_.setDepth(enumer.getDepth());

    return this;
  }
}