/*
 * @creation 21 juin 07
 * @modification $Date: 2007-06-28 09:24:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Cette classe permet de g�rer correctement les evenements pour les popups. En effet, il faut redefinr les methodes
 * mousePressed et mouseReleased pour que le tout fonctionne correctement sur toutes les plateformes ( linux, windows,
 * mac).
 * 
 * @author fred deniger
 * @version $Id: CtuluPopupListener.java,v 1.1 2007-06-28 09:24:23 deniger Exp $
 */
public class CtuluPopupListener extends MouseAdapter {

  /**
   * Interface que doit implanter le receveur d'evt popup.
   * 
   * @author fred deniger
   * @version $Id: CtuluPopupListener.java,v 1.1 2007-06-28 09:24:23 deniger Exp $
   */
  public interface PopupReceiver {

    void popup(MouseEvent _evt);
  }

  final PopupReceiver receiver_;

  /**
   * @param _receiver ne doit pas etre null: exception sinon.
   */
  public CtuluPopupListener(final PopupReceiver _receiver) {
    this(_receiver, null);
  }

  /**
   * @param _receiver ne doit pas etre null: exception sinon.
   * @param _sender peut etre null. si non null, ce listener s'enregistre automatiquement sur ce component
   */
  public CtuluPopupListener(final PopupReceiver _receiver, Component _sender) {
    super();
    receiver_ = _receiver;
    if (receiver_ == null) throw new IllegalArgumentException("_receiver should not be null");
    if (_sender != null) _sender.addMouseListener(this);
  }

  @Override
  public void mousePressed(MouseEvent _e) {
    if (_e.isPopupTrigger()) receiver_.popup(_e);
  }

  @Override
  public void mouseReleased(MouseEvent _e) {
    if (_e.isPopupTrigger()) receiver_.popup(_e);
  }

}
