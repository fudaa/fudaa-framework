package org.fudaa.ctulu.gui;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;

/**
 * Constructeur de popup menu.
 *
 * @author deniger
 */
public class PopupMenuReceiver implements CtuluPopupListener.PopupReceiver {

  private JMenuItem copyItem;
  private JMenuItem copyAllItem;
  private JMenuItem cutItem;
  private JComponent component;
  CtuluUI ui;
  private JMenuItem pasteItem;
  protected CtuluPopupMenu popupMenu;
  private boolean isBuild;

  @SuppressWarnings("serial")
  public static Action createCopyAllAction(final JTable table) {
    Action res = new AbstractAction(CtuluResource.CTULU.getString("Copier tout"), CtuluResource.CTULU
            .getIcon("copie-tout")) {
      @Override
      public void actionPerformed(ActionEvent e) {
        new CopyAllTableToClipboardRunnable(table).run();

      }
    };
    res.putValue(Action.SHORT_DESCRIPTION, CtuluLib.getS("Tout copier dans le presse-papier"));
    res.putValue(Action.ACTION_COMMAND_KEY, "copyAll");
    return res;

  }

  public final void build() {
    if (isBuild) {
      return;
    }
    isBuild = true;
    popupMenu = new CtuluPopupMenu();
    copyItem = new JMenuItem(new ActionTransferSource.CopyAction(component));
    cutItem = new JMenuItem(new ActionTransferSource.CutAction(component));
    pasteItem = new JMenuItem(new ActionTransferSource.PasteAction(component));
    if (isTable()) {
      copyAllItem = new JMenuItem(createCopyAllAction((JTable) component));
    }
    addItems();
  }

  private boolean isTable() {
    return component instanceof JTable;
  }
  protected CtuluTableExportAction ctuluTableExportAction;

  protected void addExportItems() {
    if (isTable()) {
      popupMenu.addSeparator();
      ctuluTableExportAction = new CtuluTableExportAction(ui, (JTable) component);
      ctuluTableExportAction.setSeparator(';');
      popupMenu.add(ctuluTableExportAction);
    }
  }

  protected void addItems() {
    addCopyPasteItems();
    addExportItems();
  }

  private void addCopyPasteItems() {
    popupMenu.add(cutItem);
    popupMenu.add(copyItem);
    if (copyAllItem != null) {
      popupMenu.add(copyAllItem);
    }
    popupMenu.add(pasteItem);
  }

  public void install(JComponent component, CtuluUI ui) {
    install(component, component, ui);
  }

  public void install(JComponent component, JComponent source, CtuluUI ui) {
    this.ui = ui;
    this.component = source;
    assert this.component != null;
    new CtuluPopupListener(this, component);
  }

  public static void installDefault(JComponent jc, CtuluUI ui) {
    new PopupMenuReceiver().install(jc, ui);

  }

  @Override
  public void popup(MouseEvent evt) {
    build();
    updateItemStateBeforeShow();
    popupMenu.show(evt.getComponent(), evt.getX(), evt.getY());

  }

  protected void updateItemStateBeforeShow() {
    ((ActionTransferSource) copyItem.getAction()).updateState();
    ((ActionTransferSource) cutItem.getAction()).updateState();
    ((ActionTransferSource) pasteItem.getAction()).updateState();
  }
}
