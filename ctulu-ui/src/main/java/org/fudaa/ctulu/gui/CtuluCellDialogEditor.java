/*
 *  @creation     13 mai 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.gui;


/**
 * @author deniger
 * @version $Id$
 */
public class CtuluCellDialogEditor extends CtuluCellButtonEditor {
  protected CtuluDialog dialog_;

  public CtuluCellDialogEditor(final CtuluDialog _dialog) {
    this(_dialog, null);
  }

  public CtuluCellDialogEditor(final CtuluDialog _dialog, final CtuluCellDecorator _deco) {
    super(_deco);
    dialog_ = _dialog;
  }

  @Override
  protected void doAction() {
    dialog_.setValue(value_);
    dialog_.doLayout();
    if (dialog_.afficheAndIsOk()) {
      value_ = dialog_.getValue();
      setValue(value_);

    } else {
      cancelCellEditing();
    }
    stopCellEditing();
    if (getParent() != null) {
      getParent().requestFocus();
    }
  }

  public CtuluDialog getDialog() {
    return dialog_;
  }

}
