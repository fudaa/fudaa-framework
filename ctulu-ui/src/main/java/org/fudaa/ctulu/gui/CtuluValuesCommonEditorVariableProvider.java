package org.fudaa.ctulu.gui;

import java.util.Collection;
import java.util.Map;
import org.nfunk.jep.Variable;

public interface CtuluValuesCommonEditorVariableProvider {
  
  
  Collection<String> getVariables();
  
  /**
   * @param idx the real idx.
   */
  void modifyVariablesFor(int idx,Map<String,Variable> variables);

}
