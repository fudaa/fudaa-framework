/*
 *  @creation     27 mai 2005
 *  @modification $Date: 2007-05-04 13:43:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.*;

/**
 * Un panel permettant de sp�cifier le nombre de chiffres significatifs � utiliser.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluDecimalFormatEditPanel.java,v 1.2 2007-05-04 13:43:23 deniger Exp $
 */
public class CtuluDecimalFormatEditPanel extends CtuluDialogPanel implements BuBorders {

  public static CtuluNumberFormatI chooseNumberFormat(CtuluNumberFormatI _init) {
    CtuluDecimalFormatEditPanel pn = new CtuluDecimalFormatEditPanel(_init);
    if (pn.afficheModaleOk(CtuluLibSwing.getActiveWindow(), CtuluLib.getS("Choisir le format"))) { 
      return pn.getCurrentFmt(); }
    return _init;

  }

  JPanel pnDecimalFormat_;
  JPanel pnFixedFormat_;

  BuCheckBox cbISExp_;
  final BuComboBox cbChoose_;
  DecimalFormat decimalFormat_;
  CtuluNumberFormatDefault currentDecimalFmt_;
  CtuluNumberFormatFixedFigure currentFixedFmt_;

  BuTextField tfFraction_;
  BuTextField tfFractionMin_;
  BuTextField tfNbDigits_;

  final BigDecimal[] values_ = new BigDecimal[] { new BigDecimal("0.00000134"), new BigDecimal("10.2345"),
      new BigDecimal("100"), new BigDecimal("1234567890") };
  final BuLabel[] label_ = new BuLabel[values_.length];

  /**
   * @param _fmt le format initial
   */
  public CtuluDecimalFormatEditPanel(final CtuluNumberFormatI _fmt) {
    super(true);
    setLayout(new BuBorderLayout());
    boolean isFixed = true;
    if (_fmt instanceof CtuluNumberFormatDefault) {
      currentDecimalFmt_ = (CtuluNumberFormatDefault) _fmt.getCopy();
      decimalFormat_ = currentDecimalFmt_.cloneDecimalFormat();
      currentDecimalFmt_.setFmt(decimalFormat_);
      isFixed = false;
    } else if (_fmt instanceof CtuluNumberFormatFixedFigure) {
      currentFixedFmt_ = (CtuluNumberFormatFixedFigure) _fmt;
    } else {
      currentFixedFmt_ = CtuluLib.DEFAULT_NUMBER_FORMAT;
    }
    if (currentFixedFmt_ == null) {
      currentFixedFmt_ = CtuluLib.DEFAULT_NUMBER_FORMAT;
    }
    if (currentDecimalFmt_ == null) {
      decimalFormat_ = CtuluLib.getDecimalFormat();
      decimalFormat_.setMaximumFractionDigits(3);
      decimalFormat_.setMinimumFractionDigits(0);
      currentDecimalFmt_ = new CtuluNumberFormatDefault(decimalFormat_);
    }
    cbChoose_ = new BuComboBox(new String[] { CtuluLib.getS("Formater avec un nombre de chiffres signicatifs"),
        CtuluLib.getS("Formater avec un nombre de d�cimales / �criture exponentielle") });
    setLayout(new BuBorderLayout());
    if (!isFixed) {
      cbChoose_.setSelectedIndex(1);
    }

    add(createPreviewPanel(), BuBorderLayout.SOUTH);
    updatePanel();
    cbChoose_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        updatePanel();
        final JDialog d = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, CtuluDecimalFormatEditPanel.this);
        if (d != null) {
          d.pack();
        }

      }

    });
    add(cbChoose_, BuBorderLayout.NORTH);

  }

  protected void updatePanel() {
    if (pnDecimalFormat_ != null) {
      remove(pnDecimalFormat_);
    }
    if (pnFixedFormat_ != null) {
      remove(pnFixedFormat_);
    }
    add(isFixedSelected() ? createFixedPanel() : createDecimalPanel(), BuBorderLayout.CENTER);
    fillRes();
  }

  boolean isFixedSelected() {
    return cbChoose_.getSelectedIndex() == 0;
  }

  private JPanel createFixedPanel() {
    if (pnFixedFormat_ != null) { return pnFixedFormat_; }
    pnFixedFormat_ = new BuPanel(new BuGridLayout(2, 5, 5));
    final CtuluValueValidator min = new CtuluValueValidator.IntMin(1);
    tfNbDigits_ = BuTextField.createIntegerField();
    tfNbDigits_.setValueValidator(min);
    tfNbDigits_.setToolTipText(min.getDescription());
    tfNbDigits_.setValue(new Integer(currentFixedFmt_.getMaxChar()));
    final BuLabel buLabel = new BuLabel(CtuluLib.getS("Nombre de chiffres significatifs"));
    pnFixedFormat_.add(buLabel);
    pnFixedFormat_.add(tfNbDigits_);
    buLabel.setLabelFor(tfNbDigits_);
    tfNbDigits_.getDocument().addDocumentListener(new DocumentListener() {

      @Override
      public void removeUpdate(final DocumentEvent _e) {
        updateFromFixed();

      }

      @Override
      public void insertUpdate(final DocumentEvent _e) {
        updateFromFixed();

      }

      @Override
      public void changedUpdate(final DocumentEvent _e) {

        updateFromFixed();
      }

    });
    return pnFixedFormat_;

  }

  private JPanel createDecimalPanel() {
    if (pnDecimalFormat_ != null) { return pnDecimalFormat_; }
    pnDecimalFormat_ = new BuPanel(new BuGridLayout(2, 5, 5));
    cbISExp_ = new BuCheckBox();
    tfFraction_ = BuTextField.createIntegerField();
    final CtuluValueValidator min = new CtuluValueValidator.IntMin(0);
    tfFraction_.setText(CtuluLibString.getString(decimalFormat_.getMaximumFractionDigits()));

    tfFractionMin_ = BuTextField.createIntegerField();
    tfFractionMin_.setText(CtuluLibString.getString(decimalFormat_.getMinimumFractionDigits()));
    tfFraction_.setValueValidator(min);
    tfFractionMin_.setValueValidator(min);
    tfFraction_.setToolTipText(min.getDescription());
    tfFractionMin_.setToolTipText(min.getDescription());
    cbISExp_.setSelected(decimalFormat_.toPattern().indexOf('E') > 0);
    BuLabel buLabel = new BuLabel(CtuluLib.getS("Nombre de d�cimales max"));
    pnDecimalFormat_.add(buLabel);
    pnDecimalFormat_.add(tfFraction_);
    buLabel.setLabelFor(tfFraction_);
    buLabel = new BuLabel(CtuluLib.getS("Nombre de d�cimales min"));
    pnDecimalFormat_.add(buLabel);
    pnDecimalFormat_.add(tfFractionMin_);
    buLabel.setLabelFor(tfFractionMin_);
    pnDecimalFormat_.add(new BuLabel(CtuluLib.getS("notation scientifique")));
    pnDecimalFormat_.add(cbISExp_);
    final DocumentListener fraction = new DocumentListener() {

      @Override
      public void removeUpdate(DocumentEvent _e) {
        updateFromDecimal();
      }

      @Override
      public void insertUpdate(DocumentEvent _e) {
        updateFromDecimal();
      }

      @Override
      public void changedUpdate(DocumentEvent _e) {
        updateFromDecimal();
      }

    };
    tfFraction_.getDocument().addDocumentListener(fraction);
    tfFractionMin_.getDocument().addDocumentListener(fraction);
    cbISExp_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        updateFromDecimal();

      }

    });
    return pnDecimalFormat_;
  }

  private BuPanel createPreviewPanel() {
    final BuPanel center = new BuPanel(new BuGridLayout(2, 2, 2));
    center.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Exemples")));
    for (int i = 0; i < values_.length; i++) {
      final BuLabel lb = new BuLabel(values_[i].toString());
      center.add(lb);
      label_[i] = new BuLabel();
      center.add(label_[i]);
      label_[i].setBackground(Color.WHITE);
      lb.setBackground(Color.WHITE);
      lb.setHorizontalTextPosition(SwingConstants.RIGHT);
      lb.setHorizontalAlignment(SwingConstants.RIGHT);
      lb.setOpaque(true);
      label_[i].setOpaque(true);
      label_[i].setHorizontalTextPosition(SwingConstants.CENTER);
      label_[i].setHorizontalAlignment(SwingConstants.RIGHT);
    }
    return center;
  }

  private void buildDecimalFormat() {
    final boolean isGroup = decimalFormat_.isGroupingUsed();
    if (cbISExp_.isSelected()) {
      decimalFormat_.applyPattern("0.#####E0");
    } else {
      decimalFormat_.applyPattern("0.##");

    }
    int i = 0;
    if (tfFraction_.getText() != null && tfFraction_.getText().trim().length() > 0) {
      i = ((Integer) tfFraction_.getValue()).intValue();
    }
    decimalFormat_.setMaximumFractionDigits(i);
    i = 0;
    if (tfFractionMin_.getText() != null && tfFractionMin_.getText().trim().length() > 0) {
      i = ((Integer) tfFractionMin_.getValue()).intValue();
    }
    decimalFormat_.setMinimumFractionDigits(i);
    decimalFormat_.setGroupingUsed(isGroup);
    this.currentDecimalFmt_.setFmt(decimalFormat_);
  }

  private void fillRes() {
    final CtuluNumberFormat fmt = getCurrentFmt();
    for (int i = values_.length - 1; i >= 0; i--) {
      label_[i].setText(fmt.format(values_[i]));
    }
  }

  void updateFromDecimal() {
    buildDecimalFormat();
    fillRes();

  }

  void updateFromFixed() {
    final Integer value = (Integer) tfNbDigits_.getValue();
    if (value != null) {
      currentFixedFmt_ = new CtuluNumberFormatFixedFigure(value.intValue());
    }
    fillRes();

  }

  @Override
  public boolean isDataValid() {
    if (isFixedSelected()) {
      final Object intValue = tfNbDigits_.getValue();
      CtuluLibSwing.setErrorInLabel(tfNbDigits_);
      if (intValue == null) {
        setErrorText(((CtuluValueValidator) tfNbDigits_.getValueValidator()).getDescription());
        return false;
      }
      return true;
    }
    final Object i1 = tfFraction_.getValue();
    boolean valide = true;
    if (i1 == null) {
      CtuluLibSwing.setErrorInLabel(tfFraction_);
      valide = false;
    }
    final Object i2 = tfFractionMin_.getValue();
    if (i2 == null) {
      CtuluLibSwing.setErrorInLabel(tfFractionMin_);
      valide = false;
    }
    if (!valide) {
      setErrorText(((CtuluValueValidator) tfFractionMin_.getValueValidator()).getDescription());
    }
    return valide;

  }

  public final CtuluNumberFormat getCurrentFmt() {
    return cbChoose_.getSelectedIndex() == 0 ? (CtuluNumberFormat) currentFixedFmt_ : currentDecimalFmt_;
  }

}
