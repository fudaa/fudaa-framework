/*
 * @creation 19 janv. 2006
 * 
 * @modification $Date: 2007-01-10 08:58:48 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * Une interface permettant d'initialiser facilement les editeurs. TOUS LES tableaux doivent avoir la meme taille
 * 
 * @author Fred Deniger
 * @version $Id: CtuluValuesParameters.java,v 1.4 2007-01-10 08:58:48 deniger Exp $
 */
public interface CtuluValuesParameters {
  Object[] getNames();

  int[] getIdx();

  CtuluValueEditorI[] getEditors();

  CtuluCollection[] getValues();

  CtuluValuesCommonEditorVariableProvider getVariableProvider();

}
