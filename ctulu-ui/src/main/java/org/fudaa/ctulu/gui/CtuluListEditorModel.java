/*
 * @creation 28 mai 2003
 * @modification $Date: 2007-05-04 13:43:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Le model attache au panel FudaaPanelListEditor.
 * 
 * @author deniger
 * @version $Id: CtuluListEditorModel.java,v 1.7 2007-05-04 13:43:23 deniger Exp $
 */
public class CtuluListEditorModel extends AbstractTableModel {

  /**
   * @author Fred Deniger
   * @version $Id: CtuluListEditorModel.java,v 1.7 2007-05-04 13:43:23 deniger Exp $
   */
  public class ListEditorComboBoxModel extends AbstractListModel implements ComboBoxModel {

    private final transient Object first_;

    private transient Object selected_;

    public ListEditorComboBoxModel() {
      this(null);

    }

    public void fireChanged() {
      super.fireIntervalAdded(this, 0, getSize());

    }

    public ListEditorComboBoxModel(final Object _first) {
      first_ = _first;
    }

    @Override
    public Object getElementAt(final int _i) {
      if (first_ != null) {
        if (_i == 0) {
          return first_;
        }
        return v_.get(_i - 1);
      }
      return v_.get(_i);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return first_ == null ? v_.size() : v_.size() + 1;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if ((selected_ == null && _anItem != null) || (selected_ != null && !selected_.equals(_anItem))) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    public void update() {
      fireContentsChanged(this, 0, getSize());
    }
  }

  private int maxValueNb_;

  private boolean showNumber_;

  protected ArrayList v_;

  transient Object defaultValue_;

  /**
   * On utilise une arrayList car on veut appeler la m�thode ensureCapacity.
   */
  ArrayList proposedValues_;

  public CtuluListEditorModel(final List _v, final boolean _showNumber, final int _maxValueNb) {
    maxValueNb_ = _maxValueNb;
    if ((maxValueNb_ > 0) && (_v.size() > _maxValueNb)) {
      v_ = new ArrayList(_v.subList(0, _maxValueNb));
    } else {
      v_ = new ArrayList(_v);
    }
    showNumber_ = _showNumber;
    defaultValue_ = CtuluLibString.EMPTY_STRING;
  }

  public CtuluListEditorModel() {
    this(-1);
  }

  public CtuluListEditorModel(final boolean _showNumber) {
    this(Collections.EMPTY_LIST, _showNumber, -1);
  }

  public CtuluListEditorModel(final int _maxNumber) {
    this(new ArrayList(_maxNumber > 0 ? _maxNumber : 10), false, _maxNumber);
  }

  /**
   * @param _l la liste des elements initiaux
   * @param _showNumber si true la premiere colonne affiche le num de ligne
   */
  public CtuluListEditorModel(final List _l, final boolean _showNumber) {
    this(_l, _showNumber, -1);
  }

  /**
   * @param _o tableau des elements initiaux
   * @param _showNumber si true la premiere colonne affiche le num de ligne
   */
  public CtuluListEditorModel(final Object[] _o, final boolean _showNumber) {
    this(Arrays.asList(_o), _showNumber, -1);
  }

  public CtuluListEditorModel(final Object[] _o, final boolean _showNumber, final int _maxNbValue) {
    this(new Vector(Arrays.asList(_o)), _showNumber, _maxNbValue);
  }

  public boolean actionAdd() {
    final int n = v_.size();
    if ((maxValueNb_ > 0) && (n == maxValueNb_)) {
      return false;
    }
    if ((proposedValues_ != null) && (n < proposedValues_.size())) {
      addElement(proposedValues_.get(n));
    } else {
      final Object o = createNewObject();
      if (o == null) {
        return false;
      }
      addElement(o);
    }
    return true;
  }
  
  public boolean actionCopy(final int _r) {
    final int n = v_.size();
    if ((maxValueNb_ > 0) && (n == maxValueNb_)) {
      return false;
    }
    final Object o=copyObject(_r);
    if (o == null) {
      return false;
    }

    addElement(o);
    return true;
  }

  public boolean actionInserer(final int _r) {
    final int n = v_.size();
    if ((maxValueNb_ > 0) && (n == maxValueNb_)) {
      return false;
    }
    if ((proposedValues_ != null) && (_r < proposedValues_.size())) {
      add(_r, proposedValues_.get(_r));
    } else {
      final Object o = createNewObject();
      if (o == null) {
        return false;
      }
      add(_r, o);
    }
    return true;
  }

  public void add(final int _i, final Object _o) {
    if (canAdd()) {
      v_.add(_i, _o);
      fireTableRowsInserted(_i, _i);
    }
  }

  public void addElement(final Object _o) {
    if (canAdd()) {
      v_.add(_o);
      fireTableRowsInserted(v_.size() - 1, v_.size() - 1);
    }
  }

  public void addProposedValue(final Object _o) {
    if (_o == null) {
      return;
    }
    if (proposedValues_ == null) {
      setProposedValue(_o);
    } else {
      proposedValues_.add(_o);
    }
  }

  public void addProposedValues(final Object[] _o) {
    if (_o == null) {
      return;
    }
    if (proposedValues_ == null) {
      setProposedValues(_o);
    } else {
      final int n = _o.length;
      for (int i = 0; i < n; i++) {
        if (_o[i] != null) {
          proposedValues_.add(_o[i]);
        }
      }
    }
  }

  public void addProposedValuesIfNotPresent(final Object[] _o) {
    if (_o == null) {
      return;
    }
    if (proposedValues_ == null) {
      setProposedValues(_o);
    } else {
      final int n = _o.length;
      for (int i = 0; i < n; i++) {
        if ((_o[i] != null) && (!proposedValues_.contains(_o[i]))) {
          proposedValues_.add(_o[i]);
        }
      }
    }
  }

  public boolean canAdd() {
    return (maxValueNb_ < 0) || (v_.size() < maxValueNb_);
  }

  public boolean canRemove() {
    return v_.size() > 0;
  }

  /**
   * Renvoie une model correspondant. Attention ce model n'envoie pas d'evenements si le model englobant est modifie;
   */
  public ComboBoxModel createComboBoxModel() {
    return new ListEditorComboBoxModel();
  }

  public ComboBoxModel createComboBoxModel(final Object _first) {
    return new ListEditorComboBoxModel(_first);
  }

  public Object createNewObject() {
    return defaultValue_;
  }

  /**
   * Must be overriden
   */
  public Object copyObject(int _r) {
    throw new UnsupportedOperationException("This method must be overridden");
  }

  public void edit(final int _r) {

  }

  public void emptyProposedValue() {
    if (proposedValues_ != null) {
      proposedValues_.clear();
    }
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return String.class;
  }

  @Override
  public int getColumnCount() {
    return showNumber_ ? 2 : 1;
  }

  @Override
  public String getColumnName(final int _column) {
    return CtuluLibString.EMPTY_STRING;
  }

  public int getIndexOf(final Object _o) {
    return v_.indexOf(_o);
  }

  public int getMaxValueNb() {
    return maxValueNb_;
  }

  @Override
  public int getRowCount() {
    return v_.size();
  }

  public Object getValueAt(final int _i) {
    if ((_i >= 0) && (_i < v_.size())) {
      return v_.get(_i);
    }
    return null;
  }

  @Override
  public Object getValueAt(final int _row, final int _col) {
    if (showNumber_) {
      if (_col == 0) {
        return CtuluLibString.getString(_row + 1);
      } else if (_col == 1) {
        return getValueAt(_row);
      }
    } else if (_col == 0) {
      return getValueAt(_row);
    }
    return null;
  }

  public int getValueNb() {
    return v_.size();
  }

  public Object[] getValues() {
    return v_.toArray();
  }

  public void getValues(final Object[] _o) {
    v_.toArray(_o);
  }

  public List getValuesInList() {
    return new Vector(v_);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return showNumber_ ? (_columnIndex == 1 ? true : false) : true;
  }

  public boolean isEmpty() {
    return v_.isEmpty();
  }

  public boolean isProposedValueEmpty() {
    return proposedValues_ == null ? true : proposedValues_.isEmpty();
  }

  public boolean isShowNumber() {
    return showNumber_;
  }

  protected void actionBeforeRemove(final int _i) {

  }

  public void removeAll() {
    final int min = 0;
    final int max = Math.max(min,v_.size() - 1);
    v_.clear();
    fireTableRowsDeleted(min, max);
  }

  public void remove(final ListSelectionModel _m) {
    if ((_m.isSelectionEmpty()) || (!canRemove())) {
      return;
    }
    final int min = _m.getMinSelectionIndex();
    int max = _m.getMaxSelectionIndex();
    max = max >= v_.size() ? v_.size() - 1 : max;
    for (int i = max; i >= min; i--) {
      if (_m.isSelectedIndex(i)) {
        actionBeforeRemove(i);
        v_.remove(i);
      }
    }
    fireTableRowsDeleted(min, max);
  }
  
  public void remove(int _ind) {
    actionBeforeRemove(_ind);
    v_.remove(_ind);
    fireTableRowsDeleted(_ind, _ind);
  }

  public void setData(final Object[] _o) {
    v_.clear();
    if (_o != null) {
      int n = _o.length;
      if (maxValueNb_ > 0) {
        n = n > maxValueNb_ ? maxValueNb_ : n;
      }
      v_.ensureCapacity(n);
      for (int i = 0; i < n; i++) {
        v_.add(_o[i]);
      }
    }
    fireTableDataChanged();
  }

  public void setData(final List _o) {
    v_.clear();
    if (_o != null) {
      int n = _o.size();
      if (maxValueNb_ > 0) {
        n = n > maxValueNb_ ? maxValueNb_ : n;
      }
      v_.ensureCapacity(n);
      for (int i = 0; i < n; i++) {
        v_.add(_o.get(i));
      }
    }
    fireTableDataChanged();
  }

  public void setDefaultValue(final Object _o) {
    defaultValue_ = _o;
  }

  public void setMaxValueNb(final int _i) {
    if (_i != maxValueNb_) {
      maxValueNb_ = _i;
      final int n = v_.size();
      if ((maxValueNb_ > 0) && (v_.size() > maxValueNb_)) {
        for (int i = v_.size() - 1; i >= maxValueNb_; i--) {
          v_.remove(i);
        }
        fireTableRowsDeleted(maxValueNb_, n - 1);
      }
    }
  }

  public void setProposedValue(final Object _o) {
    if (_o == null) {
      return;
    }
    if (proposedValues_ == null) {
      proposedValues_ = new ArrayList(10);
    } else {
      proposedValues_.clear();
      proposedValues_.ensureCapacity(10);
    }
    proposedValues_.add(_o);
  }

  public void setProposedValues(final Object[] _o) {
    if (_o == null) {
      return;
    }
    if (proposedValues_ == null) {
      proposedValues_ = new ArrayList(_o.length);
    } else {
      proposedValues_.clear();
      proposedValues_.ensureCapacity(_o.length);
    }
    final int n = _o.length;
    for (int i = 0; i < n; i++) {
      proposedValues_.add(_o[i]);
    }
  }

  @Override
  public void setValueAt(final Object _aValue, final int _rowIndex, final int _columnIndex) {
    if (_rowIndex >= v_.size()) {
      return;
    }
    v_.set(_rowIndex, _aValue);
  };
}