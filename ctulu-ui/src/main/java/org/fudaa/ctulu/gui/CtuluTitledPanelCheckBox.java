/*
 * @creation 25 janv. 07
 * @modification $Date: 2007-02-07 09:54:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuTitledPanel;
import java.awt.LayoutManager;
import javax.swing.JCheckBox;
import javax.swing.border.Border;

/**
 * @author fred deniger
 * @version $Id: CtuluTitledPanelCheckBox.java,v 1.1 2007-02-07 09:54:11 deniger Exp $
 */
public class CtuluTitledPanelCheckBox extends BuTitledPanel {

  public CtuluTitledPanelCheckBox(final JCheckBox _title, final LayoutManager _layout, final Border _inside) {
    super(_title, _layout, _inside);
    initListener();
  }

  public CtuluTitledPanelCheckBox(final JCheckBox _title, final LayoutManager _layout) {
    super(_title, _layout);
    initListener();
  }

  public CtuluTitledPanelCheckBox(final JCheckBox _title) {
    super(_title);
    initListener();
  }

  private void initListener() {
    CtuluLibSwing.addActionListenerForCheckBoxTitle(this, (JCheckBox) getTitle());
    ((JCheckBox) getTitle()).setSelected(true);
    
  }
  
  public JCheckBox getTitleCb(){
    return (JCheckBox)super.getTitle();
  }

}
