/**
 *  @creation     21 sept. 2004
 *  @modification $Date: 2007-05-04 13:43:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gui;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * @author Fred Deniger
 * @version $Id: CtuluListModelEmpty.java,v 1.2 2007-05-04 13:43:23 deniger Exp $
 */
public final class CtuluListModelEmpty implements ComboBoxModel,TableModel {

  public final static CtuluListModelEmpty EMPTY = new CtuluListModelEmpty();

  private CtuluListModelEmpty() {

  }

  @Override
  public void addListDataListener(final ListDataListener _l) {}

  @Override
  public void addTableModelListener(TableModelListener _l) {}

  @Override
  public Class<?> getColumnClass(int _columnIndex) {
    return null;
  }

  @Override
  public int getColumnCount() {
    return 0;
  }

  @Override
  public String getColumnName(int _columnIndex) {
    return null;
  }

  @Override
  public Object getElementAt(final int _index) {
    return null;
  }

  @Override
  public int getRowCount() {
    return 0;
  }

  @Override
  public Object getSelectedItem() {
    return null;
  }

  @Override
  public int getSize() {
    return 0;
  }

  @Override
  public Object getValueAt(int _rowIndex, int _columnIndex) {
    return null;
  }

  @Override
  public boolean isCellEditable(int _rowIndex, int _columnIndex) {
    return false;
  }

  @Override
  public void removeListDataListener(final ListDataListener _l) {}

  @Override
  public void removeTableModelListener(TableModelListener _l) {}

  @Override
  public void setSelectedItem(final Object _anItem) {}

  @Override
  public void setValueAt(Object _value, int _rowIndex, int _columnIndex) {}
}
