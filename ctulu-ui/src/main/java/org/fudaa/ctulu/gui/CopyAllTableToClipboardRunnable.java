/**
 *
 */
package org.fudaa.ctulu.gui;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import org.fudaa.ctulu.CtuluLibString;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.TreeTableModel;

/**
 * @author deniger
 */
public class CopyAllTableToClipboardRunnable implements Runnable {

  private JTable table;

  public CopyAllTableToClipboardRunnable(final JTable table) {
    super();
    this.table = table;
  }

  /**
   * @return the table
   */
  public JTable getTable() {
    return table;
  }

  /**
   * @param table the table to set
   */
  public void setTable(final JTable table) {
    this.table = table;
  }

  private static void recursiveFillWithTeeNodeString(final StringBuilder dest, final Object parent,
          final TreeTableModel model, int level) {
    final int nbCol = model.getColumnCount();
    final int childCount = model.getChildCount(parent);
    for (int i = 0; i < childCount; i++) {
      final Object child = model.getChild(parent, i);
      for (int col = 0; col < nbCol; col++) {
        if (col == 0 && level > 0) {
          for (int pad = 0; pad < level; pad++) {
            dest.append(' ').append(' ');
          }
        }
        final Object object = model.getValueAt(child, col);
        final boolean isLast = col == nbCol - 1;
        // a la fin, ce n'est pas la peine d'ajouter des espaces
        dest.append(toString(object)).append(isLast ? '\n' : "\t");
      }
      recursiveFillWithTeeNodeString(dest, child, model, level + 1);
    }

  }

  public static List<String[]> getValues(final TreeTableModel model) {
    List<String[]> res = new ArrayList<String[]>();
    recursiveFillWithTeeNodeString(res, model.getRoot(), model, 0);
    return res;
  }

  public static List<String[]> getValues(final JXTreeTable table, int[] rows) {
    if (rows == null) {
      List<String[]> values = getValues(table.getTreeTableModel());
      return values;
    }
    List<String[]> res = new ArrayList<String[]>(rows.length);
    int colCount = table.getColumnCount();
    for (int i = 0; i < rows.length; i++) {
      int row = rows[i];
      String[] vals = new String[colCount];
      int pathCount = table.getPathForRow(row).getPathCount();
      vals[0] = appendSpaceBefore(pathCount, toString(table.getValueAt(row, 0)));
      for (int col = 1; col < colCount; col++) {
        vals[col] = toString(table.getValueAt(row, col));
      }
      res.add(vals);
    }
    return res;
  }

  public static void recursiveFillWithTeeNodeString(final List<String[]> values, final Object parent,
          final TreeTableModel model, int level) {
    final int nbCol = model.getColumnCount();

    final int childCount = model.getChildCount(parent);
    for (int i = 0; i < childCount; i++) {
      String[] colsValues = new String[nbCol];
      final Object child = model.getChild(parent, i);
      for (int col = 0; col < nbCol; col++) {
        String value = toString(model.getValueAt(child, col));
        if (col == 0 && level > 0) {
          value = appendSpaceBefore(level, value);
        }
        colsValues[col] = value;
      }
      values.add(colsValues);
      recursiveFillWithTeeNodeString(values, child, model, level + 1);
    }

  }

  private static String appendSpaceBefore(int level, String value) {
    StringBuilder dest = new StringBuilder(value.length() + level);
    for (int pad = 0; pad < level; pad++) {
      dest.append(' ').append(' ');
    }
    value = dest.append(value).toString();
    return value;
  }

  public String createString() {
    final StringBuilder plainBuf = new StringBuilder(256);
    if (table instanceof JXTreeTable) {
      fillWithJTreeTable(plainBuf);
    } else {
      fillWithTable(plainBuf);
    }
    if (plainBuf.length() == 0) {
      return CtuluLibString.EMPTY_STRING;
    }

    // remove the last newline
    plainBuf.deleteCharAt(plainBuf.length() - 1);
    return plainBuf.toString();
  }

  private void fillWithTable(final StringBuilder plainBuf) {
    final int nbCol = table.getColumnCount();
    final int nbRow = table.getRowCount();
    for (int row = 0; row < nbRow; row++) {
      for (int col = 0; col < nbCol; col++) {
        final Object obj = table.getValueAt(row, col);
        final boolean isLast = col == nbCol - 1;
        // a la fin, ce n'est pas la peine d'ajouter des espaces
        plainBuf.append(toString(obj)).append(isLast ? '\n' : "\t");
      }
    }
  }

  private void fillWithJTreeTable(final StringBuilder plainBuf) {
    final JXTreeTable jxTreeTable = (JXTreeTable) table;
    recursiveFillWithTeeNodeString(plainBuf, jxTreeTable.getTreeTableModel().getRoot(),
            jxTreeTable.getTreeTableModel(), 0);
  }

  private static String toString(final Object obj) {
    return ((obj == null) ? "" : obj.toString());
  }

  protected Transferable createTransferable() {
    return new StringSelection(createString());
  }

  @Override
  public void run() {
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(createTransferable(), null);
  }
}