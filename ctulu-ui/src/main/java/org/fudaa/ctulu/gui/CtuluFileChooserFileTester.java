/*
 * @creation 13 oct. 2004
 * @modification $Date: 2007-06-14 11:58:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: CtuluFileChooserFileTester.java,v 1.3 2007-06-14 11:58:18 deniger Exp $
 */
public interface CtuluFileChooserFileTester {

  /**
   * @param _selectedFile le fichier selectionne par le file chooser
   * @return null si ok
   */
  boolean isFileOk(File _selectedFile, CtuluFileChooser _fileChooser);

}