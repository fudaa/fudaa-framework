/*
 *  @creation     15 mai 2003
 *  @modification $Date: 2007-04-30 14:21:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.TreeCellRenderer;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: CtuluCellTextRenderer.java,v 1.14 2007-04-30 14:21:16 deniger Exp $
 */
@SuppressWarnings("serial")
public class CtuluCellTextRenderer extends JLabel implements CtuluCellRenderer, TreeCellRenderer {

  private CtuluCellDecorator decorator_;

  public CtuluCellTextRenderer() {
    this(null);
  }

  public CtuluCellTextRenderer(final CtuluCellDecorator _deco) {
    super();
    setDecorator(_deco);
    setOpaque(true);
    setBorder(BORDER_NO_FOCUS);
  }

  @Override
  public void updateUI() {
    super.updateUI();
    setForeground(null);
    setBackground(null);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
          final boolean _hasFocus, final int _row, final int _column) {
    CtuluCellBooleanRenderer.updateRenderer(this, _table, _isSelected, _hasFocus, _row, _column);
    if (!_isSelected) {
      super.setForeground(_table.getForeground());
      super.setBackground(_table.getBackground());
    }
    setValue(_value);
    if (decorator_ != null) {
      decorator_.decore(this, _table, _value, _row, _column);
    }
    return this;
  }

  @Override
  public void validate() {
  }

  /**
   * Overridden for performance reasons. See the <a href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void revalidate() {
  }

  /**
   * Overridden for performance reasons. See the <a href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void repaint(final long _tm, final int _x, final int _y, final int _width, final int _height) {
  }

  @Override
  public void repaint(final Rectangle _r) {
  }

  @Override
  protected void firePropertyChange(final String _propertyName, final Object _oldValue, final Object _newValue) {
    if ("text".equals(_propertyName)) {
      super.firePropertyChange(_propertyName, _oldValue, _newValue);
    }
  }

  @Override
  public void firePropertyChange(final String _propertyName, final boolean _oldValue, final boolean _newValue) {
  }

  protected void setValue(final Object _value) {
    setText((_value == null) ? CtuluLibString.EMPTY_STRING : _value.toString());
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
          final boolean _isSelected, final boolean _cellHasFocus) {
    CtuluCellBooleanRenderer.updateListCellRenderer(this, _list, _index, _isSelected, _cellHasFocus);
    setValue(_value);
    if (decorator_ != null) {
      decorator_.decore(this, _list, _value, _index);
    }
    return this;
  }
  Color treeSelectionForeground_;
  Color treeSelectionBackground_;

  @Override
  public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
          final boolean _expanded, final boolean _leaf, final int _row, final boolean _hasFocus) {
    if (treeSelectionForeground_ == null) {
      treeSelectionForeground_ = UIManager.getColor("Tree.selectionForeground");
    }
    if (treeSelectionBackground_ == null) {
      treeSelectionBackground_ = UIManager.getColor("Tree.selectionBackground");
    }
    if (_selected) {
      setBackground(treeSelectionBackground_);
      setForeground(treeSelectionForeground_);
    } else {
      setBackground(_tree.getBackground());
      setForeground(_tree.getForeground());
    }
    setValue(_value);
    return this;
  }

  @Override
  public final void setDecorator(final CtuluCellDecorator _c) {
    decorator_ = _c;
  }
}