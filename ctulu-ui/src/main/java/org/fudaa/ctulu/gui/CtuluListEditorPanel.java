/*
 *  @file         FudaaListEditor.java
 *  @creation     28 mai 2003
 *  @modification $Date: 2007-03-30 15:35:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluResource;

/**
 * @author deniger
 * @version $Id: CtuluListEditorPanel.java,v 1.9 2007-03-30 15:35:20 deniger Exp $
 */
public class CtuluListEditorPanel extends BuPanel implements ListSelectionListener, ActionListener, MouseListener,
    TableModelListener {

  protected JTable table_;

  CtuluListEditorModel model_;

  protected JButton btAjouter_;

  protected JButton btSupprimer_;

  JButton btInserer_;

  protected JButton btModifier_;
  protected JButton btCopy_;
  
  boolean doubleClickEdit_;
  /** Pour rendre le panneau enable ou non */
  private boolean allEnabled_=true;

  public CtuluListEditorPanel(final CtuluListEditorModel _m) {
    this(_m, true, true, true, true, true);
  }

  public void setModel(final CtuluListEditorModel _m) {
    table_.setModel(_m);
    model_ = _m;
  }

  public CtuluListEditorModel getTableModel() {
    return model_;
  }

  public CtuluListEditorPanel(final CtuluListEditorModel _m, final boolean _ajouterAction,
      final boolean _supprimerAction, final boolean _insererAction) {
    this(_m, _ajouterAction, _supprimerAction, _insererAction, false, true);
  }

  public CtuluListEditorPanel(final CtuluListEditorModel _m, final boolean _ajouterAction,
      final boolean _supprimerAction, final boolean _insererAction, final boolean _modifierAction,
      final boolean _monterDesc) {
    this(_m, _ajouterAction, _supprimerAction, _insererAction, _modifierAction, _monterDesc, false);
  }

  public CtuluListEditorPanel(final CtuluListEditorModel _m, final boolean _ajouterAction,
      final boolean _supprimerAction, final boolean _insererAction, final boolean _modifierAction,
      final boolean _monterDesc, final boolean _dupliquerAction) {
    model_ = _m;
    model_.addTableModelListener(this);
    table_ = new BuTable(model_);
    table_.setAutoscrolls(true);
    table_.getSelectionModel().addListSelectionListener(this);
    initCellRendererEditor();
    setLayout(new BuBorderLayout(5, 5));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    final BuScrollPane scroll = new BuScrollPane(table_);
    int height = 0;
    if ((model_ != null) && (model_.getRowCount() > 0)) {
      height = (model_.getRowCount() + 3) * table_.getRowHeight();
    } else {
      height = 3 * table_.getRowHeight();
    }
    scroll.setPreferredHeight(height);
    scroll.setName("scroll");
    add(scroll, BuBorderLayout.CENTER);
    boolean action = false;
    if (_ajouterAction) {
      action = true;
      buildAjouterButton();
    }
    if (_supprimerAction) {
      action = true;
      buildSupprimerButton();
    }
    if (_insererAction) {
      action = true;
      buildInsererButton();
    }
    if (_modifierAction) {
      action = true;
      buildModifierButton();
    }
    if (_dupliquerAction) {
      action = true;
      buildCopyButton();
    }
    if (_monterDesc) {
      action = true;
      buildMonterButton();
    }
    if (action) {
      pnBt_=new BuPanel();
      pnBt_.setName("pnBt");
      pnBt_.setLayout(new BuVerticalLayout(5, true, false));
      boolean sep = false;
      if (btAjouter_ != null) {
        sep = true;
        addBtAdd(pnBt_);
      }
      if (btInserer_ != null) {
        sep = true;
        pnBt_.add(btInserer_);
      }
      if (btSupprimer_ != null) {
        sep = true;
        pnBt_.add(btSupprimer_);
      }
      if (btCopy_ != null) {
        sep = true;
        pnBt_.add(btCopy_);
      }
      if (btMonter_ != null) {
        sep = true;
        pnBt_.add(btMonter_);
      }
      if (btDescendre_ != null) {
        sep = true;
        pnBt_.add(btDescendre_);
      }
      if (btModifier_ != null) {
        if (sep) {
          pnBt_.add(new BuSeparator());
        }
        pnBt_.add(btModifier_);
      }
      table_.addMouseListener(this);
      updateActionsState();
      add(pnBt_, BuBorderLayout.EAST);
    }
  }

  protected void addBtAdd(final BuPanel _pn) {
    if (btAjouter_ != null) {
      _pn.add(btAjouter_);
    }
  }
  
  /**
   * Pour ajouter un bouton au panneau liste.
   * @param _bt Le bouton.
   */
  public void addButton(AbstractButton _bt) {
    pnBt_.add(_bt);
  }

  public void initCellRendererEditor() {
    final TableColumnModel colModel = table_.getColumnModel();
    int valueCol = 0;
    if (model_.isShowNumber()) {
      colModel.getColumn(0).setCellRenderer(new CtuluCellRendererRowHeader());
      colModel.getColumn(0).setMaxWidth(50);
      valueCol = 1;
    }
    colModel.getColumn(valueCol).setCellRenderer(new CtuluCellTextRenderer());
  }

  public TableColumnModel getTableColumnModel() {
    return table_.getColumnModel();
  }

  public JTable getTable() {
    return table_;
  }

  public void stopCellEditing() {
    final CellEditor edit = table_.getCellEditor();
    if (edit != null) {
      edit.stopCellEditing();
    }
  }

  public void setState(final boolean _ajouterAction, final boolean _supprimerAction, final boolean _insererAction) {
    if (btAjouter_ != null) {
      btAjouter_.setEnabled(_ajouterAction);
    }
    if (btSupprimer_ != null) {
      btSupprimer_.setEnabled(_supprimerAction);
    }
    if (btInserer_ != null) {
      btInserer_.setEnabled(_insererAction);
    }
  }

  public void setState(final boolean _ajouterAction, final boolean _supprimerAction, final boolean _insererAction,
      final boolean _modifyAction) {
    setState(_ajouterAction, _supprimerAction, _insererAction);
    if (btModifier_ != null) {
      btModifier_.setEnabled(_modifyAction);
    }
  }

  public void setState(final boolean _ajouterAction, final boolean _supprimerAction, final boolean _insererAction,
      final boolean _modifyAction, final boolean _dupliquerAction) {
    setState(_ajouterAction, _supprimerAction, _insererAction, _modifyAction);
    
    if (btCopy_ != null) {
      btCopy_.setEnabled(_modifyAction);
    }
  }

  public void setValueListCellRenderer(final TableCellRenderer _l) {
    table_.getColumnModel().getColumn(model_.isShowNumber() ? 1 : 0).setCellRenderer(_l);
  }

  public void setValueListCellEditor(final TableCellEditor _l) {
    table_.getColumnModel().getColumn(model_.isShowNumber() ? 1 : 0).setCellEditor(_l);
  }

  protected JButton buildButton(final BuIcon _ic, final String _lb, final String _com, final String _toolTip,
      final int _mnemonic) {
    final JButton r = new BuButton(_ic);
    r.setText(_lb);
    r.setMnemonic(_mnemonic);
    r.setActionCommand(_com);
    r.setToolTipText(_toolTip);
    r.addActionListener(this);
    return r;
  }

  protected void buildAjouterButton() {
    btAjouter_ = buildButton(BuResource.BU.getIcon("ajouter"), BuResource.BU.getString("Ajouter"), "AJOUTER", CtuluLib
        .getS("Ajouter un nouvel �l�ment"), KeyEvent.VK_A);
  }

  protected void buildInsererButton() {
    btInserer_ = buildButton(CtuluResource.CTULU.getIcon("insert"), BuResource.BU.getString("Ins�rer"), "INSERER",
        CtuluLib.getS("Ins�rer un nouveau �l�ment"), KeyEvent.VK_I);
  }

  protected void buildSupprimerButton() {
    btSupprimer_ = buildButton(BuResource.BU.getIcon("enlever"), BuResource.BU.getString("Enlever"), "SUPPRIMER",
        CtuluLib.getS("Supprimer les �l�ments s�lectionn�s"), KeyEvent.VK_E);
  }

  protected void buildModifierButton() {
    btModifier_ = buildButton(BuResource.BU.getIcon("editer"), CtuluLib.getS("Modifier"), "MODIFIER", CtuluLib
        .getS("Modifier l'�l�ment s�lectionn�"), KeyEvent.VK_M);
  }

  protected void buildCopyButton() {
    btCopy_ = buildButton(BuResource.BU.getIcon("copier"), CtuluLib.getS("Copier"), "DUPLIQUER", CtuluLib
        .getS("Dupliquer l'�l�ment s�lectionn�"), KeyEvent.VK_M);
  }

  JButton btMonter_;

  JButton btDescendre_;

  protected BuPanel pnBt_;

  protected void buildMonterButton() {
    btMonter_ = buildButton(CtuluResource.CTULU.getIcon("monter"), CtuluLib.getS("Monter"), "MONTER", CtuluLib
        .getS("Monter l'objet s�lecionn�"), 0);
    btDescendre_ = buildButton(CtuluResource.CTULU.getIcon("descendre"), CtuluLib.getS("Descendre"), "DESCENDRE",
        CtuluLib.getS("Descendre l'objet s�lecionn�"), 0);
  }

  public final ListSelectionModel getListSelectionModel() {
    return table_.getSelectionModel();
  }

  public void actionAdd() {
    final int n = model_.getRowCount();
    if (model_.actionAdd()) {
      table_.getSelectionModel().setSelectionInterval(n, n);
      table_.scrollRectToVisible(table_.getCellRect(n, 0, true));
    }
  }

  public void actionInserer() {
    if (table_.getSelectedRowCount() == 1) {
      final int n = table_.getSelectionModel().getMinSelectionIndex();
      if (model_.actionInserer(n)) {
        table_.getSelectionModel().setSelectionInterval(n + 1, n + 1);
      }
    }
  }

  public void actionRemove() {
    model_.remove(table_.getSelectionModel());
  }

  public void actionModify() {
    if (table_.getSelectedRowCount() == 1) {
      model_.edit(table_.getSelectedRow());
    }
  }

  public void actionCopy() {
    if (table_.getSelectedRowCount() == 1) {
      final int n = model_.getRowCount();
      if (model_.actionCopy(table_.getSelectedRow())) {
        table_.getSelectionModel().setSelectionInterval(n, n);
        table_.scrollRectToVisible(table_.getCellRect(n, 0, true));
      }
    }
  }

  @Override
  public void valueChanged(final ListSelectionEvent _evt) {
    updateActionsState();
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String com = _evt.getActionCommand();
    if ("AJOUTER".equals(com)) {
      if (table_.isEditing()) {
        table_.getCellEditor().stopCellEditing();
      }
      actionAdd();
    } else if ("SUPPRIMER".equals(com)) {
      if (table_.isEditing()) {
        table_.getCellEditor().stopCellEditing();
      }
      actionRemove();
    } else if ("INSERER".equals(com)) {
      if (table_.isEditing()) {
        table_.getCellEditor().stopCellEditing();
      }
      actionInserer();
    } else if ("MODIFIER".equals(com)) {
      actionModify();
    } else if ("MONTER".equals(com)) {
      actionUp();
    } else if ("DESCENDRE".equals(com)) {
      actionDown();
    } else if ("DUPLIQUER".equals(com)) {
      actionCopy();
    }
  }

  protected void actionUp() {
    if (table_.getRowCount() <= 1) return;
    final int[] idx = table_.getSelectedRows();
    if (!CtuluLibArray.isEmpty(idx)) {
      for (int i = 0; i < idx.length; i++) {
        swap(idx[i] - 1, idx[i]);
        idx[i]--;
      }
      model_.fireTableRowsUpdated(idx[0] + 1, idx[idx.length - 1]);
      table_.clearSelection();
      for (int i = idx.length - 1; i >= 0; i--) {
        table_.getSelectionModel().addSelectionInterval(idx[i], idx[i]);
      }

    }
  }

  private boolean isIdxOk(final int _i) {
    return _i >= 0 && _i < model_.getRowCount();
  }

  protected boolean swap(final int _i, final int _i1) {
    if (isIdxOk(_i) && isIdxOk(_i1)) {
      model_.v_.set(_i, model_.v_.set(_i1, model_.v_.get(_i)));
      return true;
    }
    return false;

  }

  protected void actionDown() {
    if (table_.getRowCount() <= 1) return;
    final int[] idx = table_.getSelectedRows();
    if (!CtuluLibArray.isEmpty(idx)) {
      for (int i = idx.length - 1; i >= 0; i--) {
        swap(idx[i] + 1, idx[i]);
        idx[i]++;
      }
      model_.fireTableRowsUpdated(idx[0] - 1, idx[idx.length - 1]);
      table_.clearSelection();
      for (int i = idx.length - 1; i >= 0; i--) {
        table_.getSelectionModel().addSelectionInterval(idx[i], idx[i]);
      }

    }

  }

  private JMenuItem initFrom(final JButton _bt) {
    if (_bt == null) {
      return null;
    }
    final JMenuItem it = new JMenuItem();
    it.setIcon(_bt.getIcon());
    it.setText(_bt.getText());
    it.setToolTipText(_bt.getToolTipText());
    it.setEnabled(_bt.isEnabled());
    it.setActionCommand(_bt.getActionCommand());
    it.addActionListener(this);
    return it;
  }

  private JPopupMenu buildPopupMenu() {
    final CtuluPopupMenu menu = new CtuluPopupMenu();
    if (btAjouter_ != null) {
      menu.add(initFrom(btAjouter_));
    }
    if (btInserer_ != null) {
      menu.add(initFrom(btInserer_));
    }
    if (btSupprimer_ != null) {
      menu.add(initFrom(btSupprimer_));
    }
    if (btMonter_ != null) {
      menu.add(initFrom(btMonter_));
    }
    if (btDescendre_ != null) {
      menu.add(initFrom(btDescendre_));
    }
    if (btCopy_ != null) {
      menu.add(initFrom(btCopy_));
    }
    if (btModifier_ != null) {
      if (menu.getComponentCount() > 0) {
        menu.addSeparator();
      }
      menu.add(initFrom(btModifier_));
    }
    updateActionsState();
    return menu;
  }

  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (_evt.isPopupTrigger()) {
      popupMenu(_evt);
    }
  }
  
  /**
   * Rend actif ou inactif tous les sous composants.
   * @param _b True : Le composant est actif.
   */
  public void setAllEnabled(boolean _b) {
    allEnabled_=_b;
    updateActionsState();
  }

  void popupMenu(final MouseEvent _evt) {
    buildPopupMenu().show((JComponent) _evt.getSource(), _evt.getX(), _evt.getY());
  }

  protected void updateActionsState() {
    if (btAjouter_ != null) {
      btAjouter_.setEnabled(model_.canAdd() && allEnabled_);
    }
    if (btInserer_ != null) {
      btInserer_.setEnabled((model_.canAdd()) && (table_.getSelectedRowCount() == 1) && allEnabled_);
    }
    if (btSupprimer_ != null) {
      btSupprimer_.setEnabled((model_.canRemove()) && (table_.getSelectedRowCount() != 0) && allEnabled_);
    }
    if (btModifier_ != null) {
      btModifier_.setEnabled(table_.getSelectedRowCount() == 1 && allEnabled_);
    }
    if (btCopy_ != null) {
      btCopy_.setEnabled(table_.getSelectedRowCount() == 1 && allEnabled_);
    }

    if (btMonter_ != null) {
      final boolean isOnlyOne = table_.getRowCount() > 1 && table_.getSelectedRowCount() >= 1
          && table_.getSelectedRow() >= 0 && allEnabled_;
      btMonter_.setEnabled(isOnlyOne && table_.getSelectionModel().getMinSelectionIndex() > 0 && allEnabled_);
      btDescendre_
          .setEnabled(isOnlyOne && table_.getSelectionModel().getMaxSelectionIndex() < table_.getRowCount() - 1 && allEnabled_);
    }
    
    table_.setEnabled(allEnabled_);
  }

  public void setDoubleClickEdit(final boolean _b) {
    doubleClickEdit_ = _b;
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {
    if (doubleClickEdit_ && _evt.getClickCount() == 2 && (table_.getSelectedRowCount() == 1)) {
      final int r = table_.getSelectedRow();
      final Rectangle rec = table_.getCellRect(r, 0, true);
      for (int i = table_.getColumnCount() - 1; i > 0; i--) {
        rec.width += table_.getCellRect(r, i, true).width;
      }
      if (rec.contains(_evt.getX(), _evt.getY())) {
        model_.edit(table_.getSelectedRow());
      }
    }
  }

  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  @Override
  public void mouseExited(final MouseEvent _evt) {}

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (_evt.isPopupTrigger()) {
      popupMenu(_evt);
    }
  }

  @Override
  public void tableChanged(final TableModelEvent _evt) {
    this.updateActionsState();
  }
}