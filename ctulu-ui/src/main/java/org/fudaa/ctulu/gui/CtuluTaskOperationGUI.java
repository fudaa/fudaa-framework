/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2007-01-17 10:45:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuMainPanel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskOperationAbstract;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Utiliser car BuTaskOperation recupere les exceptions qui pourraient etre utilisees par d' autres m�thodes.
 */
public class CtuluTaskOperationGUI extends CtuluTaskOperationAbstract implements ProgressionInterface {

  private final BuCommonInterface app_;

  public CtuluTaskOperationGUI(final BuCommonInterface _app, final String _nom) {
    super(_nom);
    app_ = _app;
    if (app_ != null) {
      setTaskView(app_.getMainPanel().getTaskView());
    }
  }

  @Override
  public ProgressionInterface getStateReceiver() {
    return this;
  }

  public static class MainProgression implements ProgressionInterface {

    final BuMainPanel pnMain_;
    boolean isDone_;

    protected final boolean isDone() {
      return isDone_;
    }

    /**
     * @param _main
     */
    public MainProgression(final BuMainPanel _main) {
      super();
      pnMain_ = _main;
    }

    @Override
    public void reset() {
      pnMain_.setMessage(CtuluLibString.EMPTY_STRING);
      pnMain_.setProgression(0);
    }

    @Override
    public void setDesc(final String _s) {
      isDone_ = true;
      pnMain_.setMessage(_s);
    }

    @Override
    public void setProgression(final int _v) {
      isDone_ = true;
      pnMain_.setProgression(_v);
    }
  }

  MainProgression main_;

  @Override
  public ProgressionInterface getMainStateReceiver() {
    if (main_ == null) {
      main_ = new MainProgression(app_.getMainPanel());
    }
    return main_;

  }

  @Override
  public void reset() {
    if (main_ != null && main_.isDone()) {
      main_.reset();
    }
    setName(CtuluLibString.EMPTY_STRING);
    setProgression(0);
  }

}
