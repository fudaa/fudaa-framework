package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuResource;
import org.fudaa.ctulu.*;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class CtuluAnalyzeGUI {
  /**
   * Affiche la dialog avec le resultata du ctuluAnalyse.
   *
   * @param ui
   * @param title
   */
  public static void showDialog(final CtuluAnalyzeGroup errors, final CtuluUI ui, final String title) {
    showDialogErrorFiltre(errors, ui, title, false);
  }

  private static void addLines(final CtuluLog message, final DefaultMutableTreeTableNode rootFic, final boolean onlyError) {
    final List<CtuluLogRecord> all = new ArrayList<CtuluLogRecord>();
    final List<CtuluLogRecord> warn = new ArrayList<CtuluLogRecord>();
    final List<CtuluLogRecord> info = new ArrayList<CtuluLogRecord>();
    for (final CtuluLogRecord log : message.getRecords()) {
      if (log.getLevel() == CtuluLogLevel.SEVERE || log.getLevel() == CtuluLogLevel.FATAL || log.getLevel() == CtuluLogLevel.ERROR) {
        all.add(log);
      } else if (!onlyError) {
        if (log.getLevel() == CtuluLogLevel.WARNING) {
          warn.add(log);
        } else if (log.getLevel() == CtuluLogLevel.INFO) {
          info.add(log);
        }
      }
    }
    all.addAll(warn);
    all.addAll(info);
    for (final CtuluLogRecord log : all) {
      if (log != null) {
        if (log.getLocalizedMessage() == null) {
          log.setLocalizedMessage(log.getMsg());
        }
        rootFic.add(new MessageTreeTableNode(log.getLocalizedMessage(), log.getLevel()));
      }
    }
  }

  private static DefaultMutableTreeTableNode constructArborescence(final CtuluLogGroup listemessage, final DefaultMutableTreeTableNode parent,
                                                                   final boolean onlyError) {
    final DefaultMutableTreeTableNode root = parent == null ? new MessageTreeTableNode(listemessage.getDesci18n(), "") : parent;

    for (final CtuluLog message : listemessage.getLogs()) {
      if (onlyError && !message.containsErrors()) {
        continue;
      }
      final DefaultMutableTreeTableNode rootFic = new MessageTreeTableNode(message.getDesci18n(),
          "");
      if (root != rootFic) {
        root.add(rootFic);
      }
      addLines(message, rootFic, onlyError);
    }
    for (final CtuluLogGroup err : listemessage.getGroups()) {
      final MessageTreeTableNode rootFic = new MessageTreeTableNode(err.getDesci18n(), "");
      rootFic.setGroup(true);
      root.add(rootFic);
      constructArborescence(err, rootFic, onlyError);
    }
    return root;
  }

  protected static void loadTablePreferencesLater(final JXTable table) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        UserPreferencesSaver.loadTablePreferences(table);
      }
    });
  }

  public static void showDialogErrorFiltre(final CtuluAnalyze errors, final CtuluUI ui, final String title, final boolean onlyError) {
    CtuluAnalyzeGroup gr = new CtuluAnalyzeGroup(null);
    gr.addAnalyzer(errors);
    showDialogErrorFiltre(gr, ui, title, onlyError);
  }

  public static void showDialogErrorFiltre(final CtuluLog errors, final CtuluUI ui, final String title, final boolean onlyError) {
    CtuluLogGroup gr = new CtuluLogGroup(null);
    gr.addLog(errors);
    showDialogErrorFiltre(gr, ui, title, onlyError);
  }

  @SuppressWarnings("serial")
  public static void showDialogErrorFiltre(final CtuluAnalyzeGroup errors, final CtuluUI ui, final String title, final boolean onlyError) {
    showDialogErrorFiltre(LogConverter.convert(errors), ui, title, onlyError);
  }

  @SuppressWarnings("serial")
  public static void showDialogErrorFiltre(final CtuluLogGroup errors, final CtuluUI ui, final String title, final boolean onlyError) {
    if (!errors.containsSomething() && ui != null) {
      return;
    }

    final JXTreeTable table = createLogTable(errors, ui, onlyError);
    final Frame f;
    if (ui != null) {
      f = CtuluLibSwing.getFrameAncestorHelper(ui.getParentComponent());
    } else {
      f = null;
    }
    final JDialog dialog = new JDialog(f);
    dialog.setModal(true);
    dialog.setTitle(title);
    final JPanel container = new JPanel(new BorderLayout(2, 10));
    container.setBorder(BuBorders.EMPTY2222);
    final JScrollPane comp = new JScrollPane(table);

    comp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    final JCheckBox cbOnlyError = new JCheckBox(CtuluResource.CTULU.getString("Afficher uniquement les erreurs"));
    cbOnlyError.setSelected(onlyError);

    container.add(comp, BorderLayout.CENTER);
    final JButton btContinue = new JButton(BuResource.BU.getString("Continuer"), BuResource.BU.getIcon("crystal_valider"));

    table.expandAll();
    btContinue.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        dialog.dispose();
      }
    });
    cbOnlyError.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        UserPreferencesSaver.saveTablePreferences(table);
        UserPreferencesSaver.saveLocationAndDimension(dialog);
        dialog.dispose();
        showDialogErrorFiltre(errors, ui, title, cbOnlyError.isSelected());
      }
    });
    JPanel pnNorth = new JPanel(new BuBorderLayout());

    boolean containsError = errors.containsError();
    JLabel lb = new JLabel(CtuluResource.CTULU.getString(containsError ? "Erreurs rencontrées" : "Pas d'erreur"));
    lb.setForeground(containsError ? Color.RED : Color.GREEN.darker());
    pnNorth.add(cbOnlyError, BorderLayout.EAST);
    pnNorth.add(lb, BorderLayout.WEST);
    container.add(pnNorth, BorderLayout.NORTH);
    final JPanel pnBt = new JPanel();
    pnBt.setLayout(new FlowLayout(FlowLayout.CENTER));
    pnBt.add(btContinue);
    container.add(pnBt, BorderLayout.SOUTH);

    dialog.setContentPane(container);
    table.packAll();
    dialog.pack();
    dialog.setLocationRelativeTo(f);
    UserPreferencesSaver.loadDialogLocationAndDimension(dialog);
    loadTablePreferencesLater(table);
    dialog.setVisible(true);
    UserPreferencesSaver.saveTablePreferences(table);
    UserPreferencesSaver.saveLocationAndDimension(dialog);
  }

  public static JXTreeTable createLogTable(CtuluLogGroup errors, CtuluUI ui, boolean onlyError) {
    final DefaultTreeTableModel modeleJX = new DefaultTreeTableModel(constructArborescence(errors, null, onlyError));
    final List<String> colonnes = new ArrayList<String>();
    colonnes.add(CtuluResource.CTULU.getString("Message"));
    colonnes.add(CtuluResource.CTULU.getString("Niveau"));

    modeleJX.setColumnIdentifiers(colonnes);

    final JXTreeTable table = new JXTreeTable(modeleJX);
    PopupMenuReceiver.installDefault(table, ui);
    table.setTreeCellRenderer(new DefaultTreeCellRenderer() {
      Icon err = CtuluLibImage.resize(UIManager.getIcon("OptionPane.errorIcon"), 16, 16);
      Icon warn = CtuluLibImage.resize(UIManager.getIcon("OptionPane.warningIcon"), 16, 16);
      Icon info = CtuluLibImage.resize(UIManager.getIcon("OptionPane.informationIcon"), 16, 16);
      Font bdFont;

      @Override
      public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean sel, final boolean expanded,
                                                    final boolean leaf, final int row, final boolean hasFocus) {
        final MessageTreeTableNode node = (MessageTreeTableNode) value;
        final Component res = super.getTreeCellRendererComponent(tree, node.getValueAt(0), sel, expanded, leaf, row, hasFocus);
        if (node.level == CtuluLogLevel.SEVERE || node.level == CtuluLogLevel.FATAL || node.level == CtuluLogLevel.ERROR) {
          setIcon(err);
        } else if (node.level == CtuluLogLevel.WARNING) {
          setIcon(warn);
        } else if (node.level == CtuluLogLevel.INFO) {
          setIcon(info);
        }
        if (node.isGroup()) {
          final Font ft = tree.getFont();
          if (bdFont == null) {
            bdFont = new Font(ft.getFontName(), Font.BOLD, ft.getSize());
          }
          setFont(bdFont);
        } else {
          setFont(tree.getFont());
        }
        return res;
      }
    });
    table.setSortable(true);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    table.setEditable(false);
    return table;
  }

  private static class MessageTreeTableNode extends DefaultMutableTreeTableNode {
    private final CtuluLogLevel level;
    private final String replacer;
    boolean group;

    /**
     * @return the group
     */
    public boolean isGroup() {
      return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(boolean group) {
      this.group = group;
    }

    public MessageTreeTableNode(final String msg, final Level level) {
      super(msg);
      this.level = LogConverter.getLevel(level);
      replacer = CtuluLibString.EMPTY_STRING;
    }

    public MessageTreeTableNode(final String msg, final CtuluLogLevel level) {
      super(msg);
      this.level = level;
      replacer = CtuluLibString.EMPTY_STRING;
    }

    public MessageTreeTableNode(final String msg, final String replace) {
      super(msg);
      this.level = null;
      this.replacer = replace == null ? CtuluLibString.EMPTY_STRING : replace;
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public Object getValueAt(final int column) {
      if (column == 0) {
        return super.userObject;
      }
      if (level == null) {
        return replacer;
      }
      if (level == CtuluLogLevel.SEVERE || level == CtuluLogLevel.FATAL || level == CtuluLogLevel.ERROR) {
        return CtuluLib.getS("Erreur");
      }
      if (level == CtuluLogLevel.WARNING) {
        return CtuluLib.getS("Avertissement");
      }
      return CtuluLib.getS("Information");
    }

    @Override
    public boolean isEditable(final int column) {
      return false;
    }
  }
}
