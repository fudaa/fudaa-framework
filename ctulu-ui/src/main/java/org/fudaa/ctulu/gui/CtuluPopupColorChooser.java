/*
 * @creation 13 f�vr. 07
 * @modification $Date: 2007-03-23 17:16:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * @author fred deniger
 * @version $Id: CtuluPopupColorChooser.java,v 1.2 2007-03-23 17:16:16 deniger Exp $
 */
public class CtuluPopupColorChooser extends JComponent {

  final Color bakgroundColor_ = Color.WHITE;
  final Color borderColor_ = Color.LIGHT_GRAY;
  final Color[] colors_;
  final int colWidth_ = 22;
  int delta_ = 2;
  Font font_ = CtuluLibSwing.getMiniFont();
  int idxSelectedColor_ = -2;
  String inv_ = CtuluLib.getS("Invisible");
  final boolean invisible_;
  final int nbCol_ = 4;
  final int nbRow_;
  JPopupMenu parent_;

  final int rowWidth_ = 22;

  final Color selectedColor_ = UIManager.getColor("Tree.selectionBackground");

  public CtuluPopupColorChooser(final boolean _invisible) {
    this(_invisible, new Color[] { Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.CYAN,
        Color.ORANGE, Color.MAGENTA, Color.PINK, Color.GRAY, Color.LIGHT_GRAY });
  }

  public CtuluPopupColorChooser(final boolean _invisible, final Color[] _colors) {
    super();
    colors_ = _colors;
    invisible_ = _invisible;
    nbRow_ = (int) Math.ceil(((double) _colors.length) / nbCol_) + (invisible_ ? 1 : 0);

    setPreferredSize(new Dimension(nbCol_ * colWidth_, nbRow_ * rowWidth_));
    idxSelectedColor_ = -1;
    addMouseListener(new MouseAdapter() {

      @Override
      public void mouseReleased(final MouseEvent _e) {
        doClick(_e);
      }

    });

  }

  protected void doClick(final MouseEvent _e) {
    final int x = _e.getX();
    final int y = _e.getY();
    final int col = x / colWidth_;
    final int row = y / rowWidth_;
    int idx = row * nbCol_ + col;
    if (invisible_) {
      if (row == 0) {
        idx = -1;
      } else {
        idx -= nbCol_;
      }
    }
    if (idx >= colors_.length) {
      return;
    }
    final int xStart = delta_ + col * colWidth_;
    int xEnd = xStart + colWidth_ - delta_ * 2;
    if (row == 0 && invisible_) {
      xEnd = nbCol_ * colWidth_ - delta_;
    }
    if (x >= xStart && x < xEnd) {
      final int yStart = delta_ + row * rowWidth_;
      if (y >= yStart && y < yStart + rowWidth_ - delta_ * 2) {
        if (invisible_ && idx < 0) {
          fireColorSelected(null);
        } else if (idx >= 0 && idx < colors_.length) {
          fireColorSelected(colors_[idx]);
        }

      }

    }

  }

  public void addPropertyChangeListenerForSelectedColor(final PropertyChangeListener _l) {
    addPropertyChangeListener("selectedColor", _l);
  }

  public void removePropertyChangeListenerForSelectedColor(final PropertyChangeListener _l) {
    removePropertyChangeListener("selectedColor", _l);
  }

  protected void fireColorSelected(final Color _c) {
    if (parent_ != null) {
      parent_.setVisible(false);
    }
    firePropertyChange("selectedColor", initColor_, _c);

  }

  @Override
  protected void paintComponent(final Graphics _g) {

    super.paintComponent(_g);
    _g.setColor(bakgroundColor_);
    final int width = getWidth();
    final int height = getHeight();
    _g.fillRect(0, 0, width, height);

    int x = 0;
    int y = 0;
    final int cellWidth = rowWidth_ - delta_ * 2;
    final int cellHeight = colWidth_ - delta_ * 2;
    int idxColor = 0;
    for (int i = 0; i < nbRow_; i++) {
      if (i == 0 && invisible_) {
        final Color border = idxSelectedColor_ == -1 ? selectedColor_ : borderColor_;
        _g.setColor(border);
        final int wTrans = nbCol_ * colWidth_ - 2 * delta_;
        _g.drawRect(x + delta_, y + delta_, wTrans - 1, cellWidth - 1);
        if (idxSelectedColor_ == -1) {
          _g.drawRect(x + delta_ + 1, y + delta_ + 1, wTrans - 3, cellWidth - 3);
        }
        _g.setFont(font_);
        _g.setColor(Color.BLACK);
        final FontMetrics fm = _g.getFontMetrics();
        _g.drawString(inv_, x + delta_ + (wTrans - fm.stringWidth(inv_)) / 2, y + delta_ / 2 + cellHeight / 2
            + fm.getAscent() / 2);
      } else {
        for (int col = 0; col < nbCol_ && idxColor < colors_.length; col++) {
          if (idxSelectedColor_ == idxColor) {
            _g.setColor(selectedColor_);
            _g.fillRect(x, y, colWidth_ - 1, rowWidth_ - 1);
          }
          _g.setColor(colors_[idxColor]);
          _g.fillRect(x + delta_, y + delta_, cellWidth - delta_, cellHeight - delta_);
          if (colors_[idxColor] == Color.WHITE) {
            _g.setColor(Color.LIGHT_GRAY);
            _g.drawRect(x + delta_, y + delta_, cellWidth - delta_ - 1, cellHeight - delta_ - 1);

          }

          idxColor++;
          x += colWidth_;

        }
      }
      y += rowWidth_;
      x = 0;
    }

  }

  public int getIdxSelectedColor() {
    return idxSelectedColor_;
  }

  Color initColor_;

  public void setIdxSelectedColor(final Color _c) {
    idxSelectedColor_ = -2;
    if (_c == null && invisible_) {
      idxSelectedColor_ = -1;
      initColor_ = null;
    } else {
      final int idx = CtuluLibArray.findObject(colors_, _c);
      if (idx >= 0) {
        idxSelectedColor_ = idx;
        initColor_ = colors_[idxSelectedColor_];
      }

    }
  }

  public void setParentPopup(final JPopupMenu _parent) {
    parent_ = _parent;
  }
}
