/*
 *  @creation     13 mai 2005
 *  @modification $Date: 2007-02-02 11:20:11 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLog;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class CtuluFileChooserPanel extends BuPanel implements ActionListener, DocumentListener {

  final BuButton bt_;

  String ext_;

  FileFilter[] filter_;

  File initDir_;

  protected JTextField tf_;

  boolean writeMode_ = true;
  
  /** Le filtre selectionn�. */
  protected FileFilter selectedFilter_;

  protected final int getFileSelectMode() {
    return fileSelectMode_;
  }

  public final void setFileSelectMode(final int _fileSelectMode) {
    if (fileSelectMode_ == JFileChooser.FILES_AND_DIRECTORIES || fileSelectMode_ == JFileChooser.FILES_ONLY
        || fileSelectMode_ == JFileChooser.DIRECTORIES_ONLY) {
      fileSelectMode_ = _fileSelectMode;
    }
  }

  public CtuluFileChooserPanel() {
    this(null, null);
  }

  public CtuluFileChooserPanel(final File _dir) {
    this(_dir, null);
  }

  public static BuPanel buildPanel(final CtuluFileChooserPanel _pn, final String _label) {
    final BuPanel res = new BuPanel();
    res.setLayout(new BuBorderLayout(3,1));
    res.add(new BuLabel(_label), BuBorderLayout.WEST);
    res.add(_pn, BuBorderLayout.CENTER);
    return res;
  }

  public CtuluFileChooserPanel(final File _initDir, final String _title) {
    super();
    initDir_ = _initDir;
    tf_ = new BuTextField();
    tf_.setColumns(30);
    tf_.getDocument().addDocumentListener(this);
    setLayout(new BuBorderLayout());
    add(tf_, BuBorderLayout.CENTER);
    bt_ = new BuButton();
    bt_.setHorizontalAlignment(SwingConstants.RIGHT);
    bt_.setName(_title);
    bt_.setHorizontalTextPosition(SwingConstants.CENTER);
    bt_.setText("...");
    bt_.setToolTipText(CtuluLib.getS("Modifier le chemin du fichier"));
    bt_.addActionListener(this);
    add(bt_, BuBorderLayout.EAST);
  }

  public CtuluFileChooserPanel(final String _title) {
    this(null, _title);
  }

  private void update() {
    String txt = tf_.getText();
    if (writeMode_) {
      File f = getFile();
      if (f != null && ext_ != null) {
        f = CtuluLibFile.appendExtensionIfNeeded(f, ext_);
      }
      final String err = CtuluLibFile.canWrite(f);
      if (err != null) {
        txt = "<html><body>" + txt + "<br><u>" + err + "</u></body></html>";
      }
    }
    tf_.setToolTipText(txt);
  }

  protected final boolean isChooseDir() {
    return fileSelectMode_ == JFileChooser.DIRECTORIES_ONLY;
  }

  int fileSelectMode_ = JFileChooser.FILES_ONLY;

  protected final void setChooseDir(final boolean _chooseDir) {
    if (_chooseDir) {
      fileSelectMode_ = JFileChooser.DIRECTORIES_ONLY;
    }
  }
  
  boolean isAllFileFilter_=true;

  @Override
  public void actionPerformed(final ActionEvent _e) {
    File dir = null;
    File initFile = null;
    final CtuluFileChooser fileChooser = createFileChooser();
    String title = bt_.getName();
    if (title == null) {
      final JLabel l = (JLabel) tf_.getClientProperty("labeledBy");
      if (l != null) {
        title = l.getText();
      }
    }
    if (title != null) {
      fileChooser.setDialogTitle(title);
    }
    final String txtText = tf_.getText();
    if ((txtText != null) && (txtText.trim().length() > 0)) {
      // Si le fichier est donn� dans le textField, on r�ouvre sur ce fichier.
      initFile=new File(tf_.getText());
      fileChooser.setSelectedFile(initFile);
    }
    else if (initDir_ != null) {
      if (initDir_.isDirectory()) {
        dir=initDir_;
      }
      else {
        dir=initDir_.getParentFile();
      }
    }
    if (dir != null && dir.isDirectory()) {
      fileChooser.setCurrentDirectory(dir);
    }
    fileChooser.setMultiSelectionEnabled(false);
    final int r = writeMode_ ? fileChooser.showSaveDialog(this) : fileChooser.showOpenDialog(this);
    if (r == JFileChooser.APPROVE_OPTION) {
      final File out = fileChooser.getSelectedFile();
      selectedFilter_=fileChooser.getFileFilter();
      if(!selectedFilter_.accept(out)&&(selectedFilter_ instanceof BuFileFilter))
        tf_.setText(out.getAbsolutePath()+"."+((BuFileFilter)selectedFilter_).getFirstExt());
      else
        tf_.setText(out.getAbsolutePath());
      fileChooserAccepted(fileChooser);
    }

  }
  
  /**
   * Retourne le filtre selectionn�.
   */
  public FileFilter getSelectedFilter() {
    return selectedFilter_;
  }
  
  protected void fileChooserAccepted(final CtuluFileChooser _fc){
  }

  protected CtuluFileChooser createFileChooser() {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    fileChooser.setAcceptAllFileFilterUsed(isAllFileFilter_);
    fileChooser.setFileSelectionMode(fileSelectMode_);
    if (filter_ != null) {
      for (int i = 0; i < filter_.length; i++) {
        fileChooser.addChoosableFileFilter(filter_[i]);
      }
    }
    if (writeMode_) {
      fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
    }
    return fileChooser;
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    update();
  }

  public final String getExt() {
    return ext_;
  }

  public File getFile() {
    return (tf_.getText() == null || tf_.getText().trim().length() == 0) ? null : new File(tf_.getText());
  }

  /**
   * @return le fichier sans extension
   */
  public File getFileSansExtension() {
    File r = getFile();
    if (r != null && CtuluLibFile.containsExtension(r.getName())) {
      r = new File(r.getParentFile(), CtuluLibFile.getSansExtension(r.getName()));
    }
    return r;
  }

  public File getFileWithExt() {
    final File f = getFile();
    if (ext_ != null) {
      return CtuluLibFile.appendExtensionIfNeeded(f, ext_);
    }
    return f;
  }

  public final JTextField getTf() {
    return tf_;
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    update();
  }

  public final boolean isWriteMode() {
    return writeMode_;
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    update();
  }

  @Override
  public void setEnabled(final boolean _enabled) {
    for (int i = getComponentCount() - 1; i >= 0; i--) {
      getComponent(i).setEnabled(_enabled);
    }
  }

  public final void setExt(final String _ext) {
    ext_ = _ext;
  }

  public void setFile(final File _f) {
    if (_f == null) {
      return;
    }
    tf_.setText(_f.getAbsolutePath());
    try {
      final Rectangle rect = tf_.modelToView(tf_.getText().length());
      if (rect != null) {
        tf_.scrollRectToVisible(rect);
      }
    } catch (final BadLocationException _e) {
      FuLog.error(_e);
    }
  }

  public final void setFilter(final FileFilter[] _filter) {
    filter_ = _filter;
  }

  @Override
  public void setToolTipText(final String _text) {
    super.setToolTipText(_text);
    bt_.setToolTipText(_text);
    tf_.setToolTipText(_text);

  }

  public final void setWriteMode(final boolean _writeMode) {
    writeMode_ = _writeMode;
  }

  public File getInitDir() {
    return initDir_;
  }

  public void setInitDir(final File _initDir) {
    if(_initDir!=null){
      if(_initDir.isDirectory()) {
        initDir_ = _initDir;
      } else {
        initDir_=_initDir.getParentFile();
      }
    }
  }

  public boolean isAllFileFilter() {
    return isAllFileFilter_;
  }

  public void setAllFileFilter(final boolean _isAllFileFilter) {
    isAllFileFilter_ = _isAllFileFilter;
  }

}
