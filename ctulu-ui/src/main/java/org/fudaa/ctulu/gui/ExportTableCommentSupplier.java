/*
 GPL 2
 */
package org.fudaa.ctulu.gui;

import java.util.List;

/**
 *
 * @author Frederic Deniger
 */
public interface ExportTableCommentSupplier {

  List<String> getComments();
}
