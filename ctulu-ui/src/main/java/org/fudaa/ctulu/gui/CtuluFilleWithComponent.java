/**
 *  @creation     24 juin 2004
 *  @modification $Date: 2006-08-17 16:28:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gui;

import javax.swing.JComponent;


/**
 * Cette classe decrit un internal frame possedant un composant attache: 
 * la vue calque avec l'arbre calque. En implementant cette interface, la colonne de droite sera
 * automatiquement mise a jour en fonction du composant attach�.
 * @author Fred Deniger
 * @version $Id: CtuluFilleWithComponent.java,v 1.3 2006-08-17 16:28:37 deniger Exp $
 */
public interface CtuluFilleWithComponent {
  
  /**
   * @return la classe du composant attache.
   */
  Class getComponentClass();
  /**
   * @return le titre du composant attache.
   */
  String getComponentTitle();
  /**
   * La classe de l'objet doit etre la meme que getComponentClass.
   * @return le composant a l'etat initiale. Appele la premiere fois pour construire le composant.
   */
  JComponent createPanelComponent();
  /**
   * La classe de l'objet doit etre la meme que getComponentClass.
   * @param o le composant a mettre a jour
   */
  void majComponent(Object _o);
  

}
