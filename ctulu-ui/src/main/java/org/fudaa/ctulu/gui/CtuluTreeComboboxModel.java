/*
 *  @creation     16 juin 2005
 *  @modification $Date: 2007-02-02 11:20:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.util.Enumeration;
import java.util.Stack;
import java.util.Vector;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.tree.TreeModel;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTreeComboboxModel.java,v 1.6 2007-02-02 11:20:10 deniger Exp $
 */
public class CtuluTreeComboboxModel extends AbstractListModel implements ComboBoxModel {

  private final TreeModel treeModel_;
  private Object selectedObject_;

  /**
   * @param _treeModel le modele d'arbre a transformer en combobox.
   */
  public CtuluTreeComboboxModel(final TreeModel _treeModel) {
    this.treeModel_ = _treeModel;
  }

  @Override
  public int getSize() {
    int count = 0;
    final Enumeration enumer = new PreorderEnumeration(treeModel_);
    while (enumer.hasMoreElements()) {
      enumer.nextElement();
      count++;
    }
    return count;
  }

  public boolean isSelectable(final Object _o) {
    return true;
  }

  @Override
  public Object getElementAt(final int _index) {
    final Enumeration enumer = new PreorderEnumeration(treeModel_);
    for (int i = 0; i < _index; i++) {
      enumer.nextElement();
    }
    return enumer.nextElement();
  }

  @Override
  public void setSelectedItem(final Object _anObject) {
    if (!isSelectable(_anObject)) {
      return;
    }
    if ((selectedObject_ != null && !selectedObject_.equals(_anObject)) || selectedObject_ == null && _anObject != null) {
      selectedObject_ = _anObject;
      fireContentsChanged(this, -1, -1);
    }
  }

  @Override
  public Object getSelectedItem() {
    return selectedObject_;
  }

  static class ChildrenEnumeration implements Enumeration {

    private final TreeModel cTreeModel_;
    final Object node_;
    int index_ = -1;
    int depth_;

    public ChildrenEnumeration(final TreeModel _treeModel, final Object _node) {
      cTreeModel_ = _treeModel;
      this.node_ = _node;
    }

    @Override
    public boolean hasMoreElements() {
      return index_ < cTreeModel_.getChildCount(node_) - 1;
    }

    @Override
    public Object nextElement() {
      return cTreeModel_.getChild(node_, ++index_);
    }
  }

  static class PreorderEnumeration implements Enumeration {

    final protected Stack stack_;
    private int depth_;
    private final TreeModel ptreeModel_;

    public PreorderEnumeration(final TreeModel _treeModel) {
      ptreeModel_ = _treeModel;
      final Vector v = new Vector(1);
      final Object root = ptreeModel_.getRoot();
      final int nb = ptreeModel_.getChildCount(root);
      for (int i = 0; i < nb; i++) {
        v.addElement(ptreeModel_.getChild(root, i));
      }
      stack_ = new Stack();
      stack_.push(v.elements());
    }

    @Override
    public boolean hasMoreElements() {
      return !stack_.empty() && ((Enumeration) stack_.peek()).hasMoreElements();
    }

    @Override
    public Object nextElement() {
      final Enumeration enumer = (Enumeration) stack_.peek();
      final Object node = enumer.nextElement();
      depth_ = enumer instanceof ChildrenEnumeration ? ((ChildrenEnumeration) enumer).depth_ : 0;
      if (!enumer.hasMoreElements()) {
        stack_.pop();
      }
      final ChildrenEnumeration children = new ChildrenEnumeration(ptreeModel_, node);
      children.depth_ = depth_ + 1;
      if (children.hasMoreElements()) {
        stack_.push(children);
      }
      return node;
    }

    public int getDepth() {
      return depth_;
    }
  }
}
