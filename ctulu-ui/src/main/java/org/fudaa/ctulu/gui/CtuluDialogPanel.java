/*
 * @creation 2002-09-03
 * @modification $Date: 2007-06-05 08:57:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.io.File;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * Un panneau qui peut etre facilement affichable dans une boite de dialogue. Cette classe reprend le mode de
 * fonctionnement de JOptionPane. Les methodes valide(), actionOk(), actionApply() et actionCancel() peuvent �tre
 * 
 * @version $Id: CtuluDialogPanel.java,v 1.11 2007-06-05 08:57:45 deniger Exp $
 * @author Bertrand Marchand,Fred Deniger
 */
public class CtuluDialogPanel extends BuPanel {

  private String helpText_;
  /** Le dialogue cr�� lors de l'appel a afficheDialog. null si la methode n'a pas �t� appel�e. */
  protected CtuluDialog dialog_;

  protected String getHelpActionCommand() {
    return "COM_HELP";
  }

  public String getHelpText() {
    return helpText_;
  }

  /**
   * @param _s le nouveau texte d'aide
   */
  public final void setHelpText(final String _s) {
    helpText_ = _s;
  }

  public static BuPanel getInfosPanel(final BuInformationsSoftware _isoft) {
    final BuPanel mpi = new BuPanel();
    mpi.setLayout(new BuVerticalLayout(5));
    mpi.setBorder(new EmptyBorder(5, 5, 5, 5));
    mpi.setBackground(mpi.getBackground().darker());
    mpi.setForeground(Color.white);
    BuPicture mllogo = null;
    if (_isoft.logo != null) {
      mllogo = new BuPicture(_isoft.logo);
    }
    final BuLabelMultiLine mlisoft = new BuLabelMultiLine(_isoft.name + "\nversion: " + _isoft.version + "\ndate: "
        + _isoft.date);
    mlisoft.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
    mlisoft.setForeground(Color.white);
    if (mllogo != null) {
      mpi.add(mllogo);
    }
    mpi.add(mlisoft);
    return mpi;
  }

  public static final boolean isCancelResponse(final int _res) {
    return _res == JOptionPane.CANCEL_OPTION;
  }

  public static final boolean isOkResponse(final int _res) {
    return _res == JOptionPane.OK_OPTION;
  }

  private BuLabelMultiLine lbError_;
  protected boolean modale_;

  /**
   * @param _errorText si true, un label est cree pour contenir les messages d'erreur eventuels.
   */
  public CtuluDialogPanel(final boolean _errorText) {
    if (_errorText) {
      setErrorTextEnable();
    }

  }

  /**
   * Appelle le constructeur en validant l'usage des messages d'erreur.
   */
  public CtuluDialogPanel() {
    this(true);
  }

  public JLabel getError() {
    return lbError_;
  }

  protected BuTextField addDoubleText() {
    return addDoubleText(this);
  }

  protected BuTextField addDoubleText(final Container _c) {
    final BuTextField r = createDoubleTextField();
    _c.add(r);
    return r;
  }

  protected BuTextField createDoubleTextField() {
    final BuTextField r = new BuTextField(20);
    r.setCharValidator(BuCharValidator.DOUBLE);
    r.setStringValidator(BuStringValidator.DOUBLE);
    r.setValueValidator(BuValueValidator.DOUBLE);
    return r;
  }

  protected BuTextField addDoubleText(final Container _c, final double _d) {
    final BuTextField r = addDoubleText(_c);
    r.setText(_d + "");
    return r;
  }

  protected BuTextField addDoubleText(final Container _c, final String _d) {
    final BuTextField r = addDoubleText(_c);
    r.setText(_d);
    return r;
  }

  protected BuTextField addDoubleText(final String _d) {
    return addDoubleText(this, _d);
  }

  protected BuTextField addIntegerText() {
    return addIntegerText(this);
  }

  protected BuTextField addIntegerText(final Container _c, final int _d) {
    final BuTextField r = addIntegerText(_c);
    r.setText(_d + "");
    return r;
  }

  protected BuTextField addIntegerText(final Container _c, final String _d) {
    final BuTextField r = addIntegerText(_c);
    r.setText(_d);
    return r;
  }

  protected BuTextField addIntegerText(final int _d) {
    return addIntegerText(this, _d);
  }

  protected BuTextField addIntegerText(final String _d) {
    return addIntegerText(this, _d);
  }

  /*
   * protected JTextField addLabelFileChooserPanel(final Container _c, final String _label) { return
   * addLabelFileChooserPanel(_c, _label, null, false); }
   */

  protected BuTextField addLongText(final Container _c, final long _d) {
    return addLongText(_c, Long.toString(_d));
  }

  protected BuTextField addLongText(final Container _c, final String _d) {
    final BuTextField r = addLongText(_c);
    r.setText(_d);
    return r;
  }

  protected BuTextField addLongText(final long _d) {
    return addLongText(this, _d);
  }

  protected BuTextField addLongText(final String _d) {
    return addLongText(this, _d);
  }

  protected JComboBox addComboBox(Object[] _values, Container _c) {
    JComboBox cb = new BuComboBox(_values);;
    _c.add(cb);
    return cb;
  }

  /**
   * ajoute au panel un label dont la couleur de texte est rouge.
   */
  protected BuLabel addRedLabel() {
    return addRedLabel(this);
  }

  protected BuLabel addRedLabel(final Container _c) {
    final BuLabel r = new BuLabel();
    r.setForeground(Color.red);
    _c.add(r);
    return r;
  }

  public BuTextField addStringText() {
    return addStringText(this);
  }

  protected BuTextField addStringText(final Container _c) {
    final BuTextField txt = new BuTextField();
    _c.add(txt);
    return txt;
  }

  protected BuTextField addStringText(final Container _c, final String _s) {
    final BuTextField txt = new BuTextField();
    _c.add(txt);
    txt.setText(_s);
    return txt;
  }

  protected BuTextField addStringText(final String _s) {
    return addStringText(this, _s);
  }

  public void addEmptyBorder(final int _b) {
    addEmptyBorder(this, _b);
  }

  public void addEmptyBorder(final JComponent _c, final int _b) {
    _c.setBorder(BorderFactory.createEmptyBorder(_b, _b, _b, _b));
  }

  public BuTextField addIntegerText(final Container _c) {
    final BuTextField r = new BuTextField(20);
    r.setCharValidator(BuCharValidator.INTEGER);
    r.setStringValidator(BuStringValidator.INTEGER);
    r.setValueValidator(BuValueValidator.INTEGER);
    _c.add(r);
    return r;
  }

  public BuLabel addLabel(final Container _c, final String _l) {
    final BuLabel r = new BuLabel(_l);
    r.setVerticalAlignment(SwingConstants.CENTER);
    _c.add(r);
    return r;
  }

  public BuLabel addLabel(final String _l) {
    return addLabel(this, _l);
  }

  public BuTextField addLabelDoubleText(final Container _c, final String _label) {
    addLabel(_c, _label);
    return addDoubleText(_c);
  }

  public BuTextField addLabelDoubleText(final String _label) {
    return addLabelDoubleText(this, _label);
  }

  /**
   * Permet de construire un panel avec un jtextfield et un bouton (...) permettant de selectionner un fichier. Le
   * textfield est automatiquement mis a jour. Le fileChooser est cree a partir de la methode
   * {@link #createFileChooser() createFileChooser}et il a pour titre <code>_label</code> (si non nul).
   * 
   * @return JtextField contenant le chemin du fichier selectionne
   * @param _c le container dans lequel on veut ajoute le panel (et le label)
   * @param _label le texte du label mis devant le textfield. Si nul, aucun label n'est ajoute.
   * @param _initValue la valeur initiale du textfield
   * @param _chooseDir si true, le filechooser utilise sera en mode repertoire
   * @param _saveMode si true, le filechooser sera ouvert pour enregistrer un fichier
   */
  public JTextField addLabelFileChooserPanel(final Container _c, final String _label, final File _initValue,
      final boolean _chooseDir, final boolean _saveMode) {
    final CtuluFileChooserPanel pn = new CtuluFileChooserPanel(_initValue == null ? null : _initValue, _label);
    pn.setWriteMode(_saveMode);
    pn.setChooseDir(_chooseDir);
    pn.setFile(_initValue);
    if (_label != null) {
      final JLabel lb = addLabel(_c, _label);
      lb.setVerticalTextPosition(SwingConstants.CENTER);
      lb.setLabelFor(pn.tf_);
    }
    _c.add(pn);
    return pn.tf_;
  }

  /*
   * public CtuluFileChooserPanel addFileChooserPanel(final String _label, final boolean _saveMode) { return
   * addFileChooserPanel(this, _label, _saveMode); }
   */

  public CtuluFileChooserPanel addFileChooserPanel(final Container _c, final String _label, final boolean _chooseDir,
      final boolean _saveMode) {
    final CtuluFileChooserPanel pn = new CtuluFileChooserPanel(_label);
    pn.setWriteMode(_saveMode);
    pn.setChooseDir(_chooseDir);
    if (_label != null) {
      final JLabel lb = addLabel(_c, _label);
      lb.setVerticalTextPosition(SwingConstants.CENTER);
      lb.setLabelFor(pn.tf_);
    }
    _c.add(pn);
    return pn;
  }

  /**
   * Cree un panel de selection de fichier avec comme label <code>_label</code>.
   */
  /*
   * public JTextField addLabelFileChooserPanel(final String _label, final boolean _saveMode) { return
   * addLabelFileChooserPanel(this, _label, null, false, _saveMode); }
   */

  /*
   * public JTextField addLabelFileChooserPanel(final String _label, final File _initValue, final boolean _saveMode) {
   * return addLabelFileChooserPanel(this, _label, _initValue, false, _saveMode); }
   */

  /*
   * public JTextField addLabelFileChooserPanel(final Container _cont, final String _label, final File _initValue, final
   * boolean _saveMode) { return addLabelFileChooserPanel(_cont, _label, _initValue, false, _saveMode); }
   */

  public JTextField addLabelFileChooserPanel(final String _label, final File _initValue, final boolean _chooseDir,
      final boolean _saveMode) {
    return addLabelFileChooserPanel(this, _label, _initValue, _chooseDir, _saveMode);
  }

  public BuTextField addLabelIntegerText(final Container _c, final String _label) {
    addLabel(_c, _label);
    return addIntegerText(_c);
  }

  public BuTextField addLabelIntegerText(final String _label) {
    return addLabelIntegerText(this, _label);
  }

  public BuTextField addLabelStringText(final Container _c, final String _label) {
    addLabel(_c, _label);
    return addStringText(_c);
  }

  public BuTextField addLabelStringText(final String _label) {
    addLabel(_label);
    return addStringText();
  }

  public BuTextField addLongText() {
    return addLongText(this);
  }

  public BuTextField addLongText(final Container _c) {
    final BuTextField r = new BuTextField(20);
    r.setCharValidator(BuCharValidator.LONG);
    r.setStringValidator(BuStringValidator.LONG);
    r.setValueValidator(BuValueValidator.LONG);
    _c.add(r);
    return r;
  }

  /**
   * Affiche le panel dans une boite de dialogue.
   * 
   * @param _parent la boite de dialogue parent
   */
  public void affiche(final Dialog _parent) {
    new CtuluDialog(_parent, this).afficheDialog();
  }

  /**
   * Affiche le panel dans une boite de dialogue.
   * 
   * @param _parent la fenetre parent
   */
  public void affiche(final Frame _parent) {
    new CtuluDialog(_parent, this).afficheDialog();
  }

  public void affiche(final Frame _parent, final String _title, final int _option) {
    final CtuluDialog ctuluDialog = new CtuluDialog(_parent, this);
    ctuluDialog.setOption(_option);
    ctuluDialog.setTitle(_title);
    ctuluDialog.afficheDialog();
  }
  
  public void affiche(final Component _parent) {
    affiche(_parent, null);
  }
  
  public void affiche(final Component _parent, final int _option) {
    affiche(_parent, null, _option);
  }
  
  public void affiche(final Component _parent, final String _title) {
    affiche(_parent, _title, CtuluDialog.OK_CANCEL_OPTION);
  }
  
  public void affiche(final Component _parent, final String _t, final int _option) {
    final CtuluDialog s = createDialog(_parent);
    s.setInitParent(_parent);
    s.setOption(_option);
    if (_t != null) {
      s.setTitle(_t);
    }
    s.afficheDialog();
  }

  public int afficheModale(final Component _parent) {
    return afficheModale(_parent, null, CtuluDialog.OK_CANCEL_OPTION);
  }

  public boolean afficheModaleOk(final Component _parent) {
    return isOkResponse(afficheModale(_parent, null, CtuluDialog.OK_CANCEL_OPTION));
  }

  public boolean afficheModaleOk(final Component _parent, final String _titre) {
    return isOkResponse(afficheModale(_parent, _titre, CtuluDialog.OK_CANCEL_OPTION));
  }

  public boolean afficheModaleOk(final Component _parent, final String _titre, final Runnable _run) {
    return isOkResponse(afficheModale(_parent, _titre, CtuluDialog.OK_CANCEL_OPTION, _run));
  }

  public boolean afficheModaleOk(final Component _parent, final String _titre, final int _option) {
    return isOkResponse(afficheModale(_parent, _titre, _option));
  }

  /**
   * Affiche le panel dans une boite de dialogue modale.
   * 
   * @param _parent la fenetre parent
   * @param _title le titre du dialogue
   * @return la reponse
   */
  public int afficheModale(final Component _parent, final String _title) {
    return afficheModale(_parent, _title, CtuluDialog.OK_CANCEL_OPTION);
  }

  public int afficheModale(final Component _parent, final int _option) {
    return afficheModale(_parent, null, _option);
  }

  public CtuluDialog createDialog(final Component _parent) {
    return createDialog(_parent, this);

  }
  
  /**
   * Le dialogue parent, quand le panel est associ� au dialogue.
   * @return Le dialogue, ou null.
   */
  public CtuluDialog getDialog() {
    return dialog_;
  }
  
  public void setDialog(CtuluDialog _dial) {
    dialog_ = _dial;
  }

  public static CtuluDialog createDialog(final Component _parent, final CtuluDialogPanel _panel) {
    if (_parent instanceof JDialog) {
      return new CtuluDialog((JDialog) _parent, _panel);
    }
    else if (_parent instanceof JFrame) {
      return new CtuluDialog((JFrame) _parent, _panel);
    }
    final BuApplication app = (BuApplication) SwingUtilities.getAncestorOfClass(BuApplication.class, _parent);
    if (app != null) {
      return new CtuluDialog(app, _panel);
    }
    return new CtuluDialog(_panel);
  }

  public int afficheModale(final Component _parent, final String _t, final int _option) {
    final CtuluDialog s = createDialog(_parent);
    s.setInitParent(_parent);
    s.setOption(_option);
    if (_t != null) {
      s.setTitle(_t);
    }
    return s.afficheDialogModal();
  }

  /**
   * Affiche le panel dans une boite de dialogue modale.
   * 
   * @param _parent la boite de dialogue parent
   */
  public int afficheModale(final Dialog _parent) {
    return afficheModale(_parent, null);
  }

  public int afficheModale(final Dialog _parent, final String _title) {
    final CtuluDialog d = new CtuluDialog(_parent, this);
    if (_title != null) {
      d.setTitle(_title);
    }
    return d.afficheDialogModal();
  }

  /**
   * Affiche le panel dans une boite de dialogue modale.
   * 
   * @param _parent la fenetre parent
   */
  public int afficheModale(final Frame _parent) {
    return afficheModale(_parent, null, CtuluDialog.OK_CANCEL_OPTION, null);
  }

  public int afficheModale(final Frame _parent, final String _title) {
    return afficheModale(_parent, _title, CtuluDialog.OK_CANCEL_OPTION, null);
  }

  public int afficheModale(final Frame _parent, final String _title, final Runnable _run) {
    return afficheModale(_parent, _title, CtuluDialog.OK_CANCEL_OPTION, _run);
  }

  public int afficheModale(final Component _parent, final String _title, final Runnable _run) {
    return afficheModale(_parent, _title, CtuluDialog.OK_CANCEL_OPTION, _run);
  }

  public int afficheModale(final Component _parent, final String _title, final int _option, final Runnable _run) {
    final CtuluDialog d = createDialog(_parent);
    d.setOption(_option);
    if (_title != null) {
      d.setTitle(_title);
    }
    return d.afficheDialogModal(_run);
  }

  public int afficheModale(final Frame _parent, final String _title, final int _option, final Runnable _run) {
    final CtuluDialog d = new CtuluDialog(_parent, this);
    d.setOption(_option);
    if (_title != null) {
      d.setTitle(_title);
    }
    return d.afficheDialogModal(_run);
  }

  /**
   * M�thode � surcharger : elle est appelee si le bouton APPLY est active.
   * @return true : La boite de dialogue peut �tre ferm�e. false : Elle reste ouverte.
   */
  public boolean apply() {
    return true;
  }

  /**
   * @return true : La boite de dialogue peut �tre ferm�e. false : Elle reste ouverte.
   */
  public boolean cancel() {
    return true;
  }

  public void cancelErrorText() {
    if (lbError_.isVisible()) {
      lbError_.setVisible(false);
      lbError_.setText("");
      doLayout();
    }
  }

  /**
   * Appeler avant la fermeture effective du dialogue.
   */
  public void closeDialog() {}

  public JFileChooser createFileChooser() {
    return new CtuluFileChooser();
  }

  /**
   * Renvoie la chaine caracterisant le contenu de ce dialogue.
   */
  public Object getValue() {
    throw new NoSuchMethodError("Methode non implantee");
  }

  /**
   * @return true : La boite de dialogue peut �tre ferm�e. false : Elle reste ouverte.
   */
  public boolean ok() {
    return apply();
  }

  public void setErrorText(final String _error) {
    if (lbError_ == null) { return; }
    lbError_.setVisible(true);
    lbError_.setText(_error);
    lbError_.revalidate();
    lbError_.repaint();
  }

  public String getErrorText() {
    return lbError_ == null ? null : lbError_.getText();
  }

  public final void setErrorTextEnable() {
    if (lbError_ == null) {
      lbError_ = new BuLabelMultiLine();
      lbError_.setWrapMode(BuLabelMultiLine.WORD);
      lbError_.setForeground(Color.red);
      lbError_.revalidate();
    }
  }

  public void setErrorTextUnable() {
    lbError_ = null;
  }

  public void setModale(final boolean _modale) {
    modale_ = _modale;
  }

  /**
   * Permet d'initialiser ce dialogue a l'aide d'un objet : envoie une exception par defaut.
   */
  public void setValue(final Object _f) {
    throw new NoSuchMethodError("Methode non implantee");
  }

  public boolean isDataValid() {
    return true;
  }

}
