/*
 *  @creation     11 f�vr. 2005
 *  @modification $Date: 2007-06-14 11:58:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;

/**
 * @author Fred Deniger
 * @version $Id: CtuluFileChooserCsvExcel.java,v 1.6 2007-06-14 11:58:18 deniger Exp $
 */
public class CtuluFileChooserCsvExcel extends CtuluFileChooser {

  public enum TypeChoosen {

    CSV,
    XLS,
    XLSX;
  }

  final BuFileFilter ftCsv_;
  final BuFileFilter ftXsl_;
  final BuFileFilter ftXslx_;
  final Component parentComponent;

  public static CtuluFileChooserCsvExcel createSaveFileChooser() {
    CtuluFileChooserCsvExcel fileChooser = new CtuluFileChooserCsvExcel((CtuluUI) null);
    fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
    return fileChooser;
  }

  public static CtuluFileChooserCsvExcel createSaveFileChooser(CtuluUI ui) {
    CtuluFileChooserCsvExcel fileChooser = new CtuluFileChooserCsvExcel(ui);
    fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
    return fileChooser;
  }

  public static CtuluFileChooserCsvExcel createOpenFileChooser() {
    CtuluFileChooserCsvExcel fileChooser = new CtuluFileChooserCsvExcel((CtuluUI) null);
    fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    fileChooser.setTester(null);
    return fileChooser;
  }

  public static CtuluFileChooserCsvExcel createOpenFileChooser(Component parentComponent) {
    CtuluFileChooserCsvExcel fileChooser = new CtuluFileChooserCsvExcel(parentComponent);
    fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    fileChooser.setTester(null);
    return fileChooser;
  }

  public CtuluFileChooserCsvExcel() {
    this((Component) null);

  }

  public CtuluFileChooserCsvExcel(CtuluUI _ui) {
    this(_ui == null ? null : _ui.getParentComponent());
  }

  public CtuluFileChooserCsvExcel(Component parentComponent) {
    super(true);
    this.parentComponent = parentComponent;
    ftCsv_ = new BuFileFilter(new String[]{"csv", "txt"}, CtuluResource.CTULU.getString("Texte CSV"));
    ftCsv_.setExtensionListInDescription(true);
    ftXsl_ = new BuFileFilter(new String[]{"xls"}, CtuluResource.CTULU.getString("Fichier Excel 97-2003"));
    ftXsl_.setExtensionListInDescription(true);
    ftXslx_ = new BuFileFilter(new String[]{"xlsx"}, CtuluResource.CTULU.getString("Fichier Excel"));
    ftXslx_.setExtensionListInDescription(true);
    addChoosableFileFilter(ftXsl_);
    addChoosableFileFilter(ftXslx_);
    addChoosableFileFilter(ftCsv_);
    setAcceptAllFileFilterUsed(false);
    setMultiSelectionEnabled(false);
    setDialogType(JFileChooser.SAVE_DIALOG);
    setApproveButtonToolTipText(CtuluLib.getS("L'extension du fichier sera automatiquement compl�t�e"));
    CtuluFileChooserTestWritable tester = new CtuluFileChooserTestWritable(parentComponent);
    tester.setAppendExtension(true, null);
    tester.setAppendStrictExtension(true);
    setFileFilter(ftXslx_);
    setDialogTitle(CtuluLib.getS("Enregistrer sous"));
    setTester(tester);
  }

  public BuFileFilter getFtCsv() {
    return ftCsv_;
  }

  public BuFileFilter getFtXsl() {
    return ftXsl_;
  }

  public BuFileFilter getFtXslx() {
    return ftXslx_;
  }

  /**
   * @return true si le format excel est choisi
   */
  public TypeChoosen getTypeChoosen() {
    FileFilter fileFilter = getFileFilter();
    if (fileFilter == ftXsl_) {
      return TypeChoosen.XLS;
    }
    if (fileFilter == ftXslx_) {
      return TypeChoosen.XLSX;
    }
    return TypeChoosen.CSV;
  }

  public File getDestFile() {
    final int res = showDialog(parentComponent, BuResource.BU.getString("Continuer"));
    if (res != JFileChooser.APPROVE_OPTION) {
      return null;
    }
    File f = getSelectedFile();
    if (tester_ instanceof CtuluFileChooserTestWritable) {
      return ((CtuluFileChooserTestWritable) tester_).getDestFile(f, this);
    }
    return completeExtension(f);
  }

  private File completeExtension(File f) {
    final BuFileFilter filter = (BuFileFilter) getFileFilter();
    final String name = f.getName();
    if (name.indexOf('.') < 0) {
      return new File(f.getAbsolutePath() + "." + filter.getFirstExt());
    }
    return f;
  }
}
