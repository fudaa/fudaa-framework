/*
 *  @creation     19 janv. 2006
 *  @modification $Date: 2007-01-10 08:58:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * @author Fred Deniger
 * @version $Id: CtuluValuesParametersDefault.java,v 1.4 2007-01-10 08:58:49 deniger Exp $
 */
public class CtuluValuesParametersDefault implements CtuluValuesParameters {

  public CtuluValueEditorI[] editors_;

  public int[] idx_;

  public Object[] names_;

  public CtuluCollection[] values_;
  
  private CtuluValuesCommonEditorVariableProvider variableProvider;

  @Override
  public CtuluValuesCommonEditorVariableProvider getVariableProvider() {
    return variableProvider;
  }

  public void setVariableProvider(CtuluValuesCommonEditorVariableProvider variableProvider) {
    this.variableProvider = variableProvider;
  }

  @Override
  public Object[] getNames() {
    return names_;
  }

  @Override
  public int[] getIdx() {
    return idx_;
  }

  @Override
  public CtuluValueEditorI[] getEditors() {
    return editors_;
  }

  @Override
  public CtuluCollection[] getValues() {
    return values_;
  }

}
