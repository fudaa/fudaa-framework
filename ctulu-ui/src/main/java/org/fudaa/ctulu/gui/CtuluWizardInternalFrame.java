/*
 * @creation 26 juil. 06
 * @modification $Date: 2007-05-04 13:43:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.JTextComponent;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: CtuluWizardInternalFrame.java,v 1.2 2007-05-04 13:43:23 deniger Exp $
 */
public class CtuluWizardInternalFrame extends BuInternalFrame {
  protected JComponent content_;
  protected BuHeaderPanel header_;
  protected BuPanel view_;
  protected BuButtonPanel buttons_;
  protected BuWizardTask task_;

  private final Updater updater_ = new Updater();

  public CtuluWizardInternalFrame(final BuWizardTask _task) {
    super(_task == null ? CtuluLibString.EMPTY_STRING : _task.getTaskTitle(), true, false, true, false);
    if (_task == null) {
      task_ = new BuWizardTaskSample();
    } else {
      task_ = _task;
    }

    // addWindowListener(this);

    setTitle(task_.getTaskTitle());

    content_ = (JComponent) getContentPane();
    content_.setLayout(new BorderLayout(5, 5));

    header_ = new BuHeaderPanel();

    view_ = new BuPanel();
    view_.setLayout(new BorderLayout());
    view_.setBorder(new EmptyBorder(20, 30, 20, 30));

    buttons_ = new BuButtonPanel(BuButtonPanel.TERMINER, this);

    content_.add(header_, BorderLayout.NORTH);
    content_.add(view_, BorderLayout.CENTER);
    content_.add(buttons_, BorderLayout.SOUTH);

    installStep();
  }

  class Updater implements ActionListener, ListSelectionListener, TreeSelectionListener, DocumentListener {
    @Override
    public void actionPerformed(final ActionEvent _evt) {
      update();
    }

    @Override
    public void valueChanged(final ListSelectionEvent _evt) {
      update();
    }

    @Override
    public void valueChanged(final TreeSelectionEvent _evt) {
      update();
    }

    @Override
    public void changedUpdate(final DocumentEvent _evt) {
      update();
    }

    @Override
    public void insertUpdate(final DocumentEvent _evt) {
      update();
    }

    @Override
    public void removeUpdate(final DocumentEvent _evt) {
      update();
    }

    private void update() {
      buttons_.setDisabledButtons(task_.getStepDisabledButtons());
    }
  }

  private void addListeners() {
    final Enumeration e = BuLib.getAllSubComponents(view_).elements();
    while (e.hasMoreElements()) {
      final Object c = e.nextElement();
      if (c instanceof AbstractButton) {
        ((AbstractButton) c).addActionListener(updater_);
      } else if (c instanceof JList) {
        ((JList) c).addListSelectionListener(updater_);
      } else if (c instanceof JTree) {
        ((JTree) c).addTreeSelectionListener(updater_);
      } else if (c instanceof JTextComponent) {
        ((JTextComponent) c).getDocument().addDocumentListener(updater_);
      } else if (c instanceof JTable) {
        ((JTable) c).getSelectionModel().addListSelectionListener(updater_);
      }
    }
  }

  private void removeListeners() {
    final Enumeration e = BuLib.getAllSubComponents(view_).elements();
    while (e.hasMoreElements()) {
      final Object c = e.nextElement();
      if (c instanceof AbstractButton) {
        ((AbstractButton) c).removeActionListener(updater_);
      } else if (c instanceof JList) {
        ((JList) c).removeListSelectionListener(updater_);
      } else if (c instanceof JTree) {
        ((JTree) c).removeTreeSelectionListener(updater_);
      } else if (c instanceof JTextComponent) {
        ((JTextComponent) c).getDocument().removeDocumentListener(updater_);
      } else if (c instanceof JTable) {
        ((JTable) c).getSelectionModel().removeListSelectionListener(updater_);
      }
    }
  }

  protected void installStep() {
    if (task_.isFinished()) {
      return;
    }

    content_.setVisible(false);

    header_.setTitle(task_.getStepTitle());
    header_.setText(task_.getStepText());
    header_.setIcon(new BuFixedSizeIcon(task_.getStepIcon(), 64, 64));

    removeListeners();

    view_.removeAll();
    view_.add(task_.getStepComponent(), BorderLayout.CENTER);

    buttons_.setButtons(task_.getStepButtons(),// |BuButtonPanel.DETRUIRE,
        this, task_.getStepDisabledButtons());
    final BuButton db = buttons_.getDefaultButton();
    getRootPane().setDefaultButton(db);
    // if(db!=null) db.requestFocus();

    addListeners();

    content_.revalidate();
    content_.setVisible(true);
    view_.requestFocus();
    view_.transferFocus();
  }

  protected void nextStep() {
    task_.nextStep();
    if (!task_.isFinished()) {
      installStep();
    }
  }

  protected void previousStep() {
    task_.previousStep();
    installStep();
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();

    if ("ABANDONNER".equals(action)) {
      task_.cancelTask();
    } else if ("PRECEDENT".equals(action)) {
      previousStep();
    } else if ("SUIVANT".equals(action)) {
      nextStep();
    } else if ("TERMINER".equals(action)) {
      task_.doTask();
    }

    if (task_.isFinished()) {
      hide();
      dispose();
    }
  }

  public void windowClosing(final WindowEvent _evt) {
    if (!task_.isFinished()) {
      task_.cancelTask();
    }
  }

  public BuWizardTask getTask() {
    return task_;
  }

}
