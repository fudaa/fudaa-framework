/*
 GPL 2
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuDesktop;
import com.memoire.fu.FuPreferences;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.table.TableColumn;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;

/**
 *
 * @author Frederic Deniger
 */
public class CtuluDialogPreferences {

  public static boolean isWindowMaximized(JFrame main) {
    int extendedState = main.getExtendedState();
    return isExtendedStateMaximized(extendedState);
  }

  public static boolean isExtendedStateMaximized(int extendedState) {
    return (extendedState & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH;
  }

  public static void saveComponentLocationAndDimension(Component main, final Preferences preferences,
          final String prefix) {
    saveIn(main.getSize(), prefix, preferences);
    saveIn(main.getLocation(), prefix, preferences);
  }
  public static void saveComponentLocationAndDimension(Component main, final FuPreferences preferences,
          final String prefix) {
    saveIn(main.getSize(), prefix, preferences);
    saveIn(main.getLocation(), prefix, preferences);
  }

  public static void ensureJInternalFrameWillBeVisible(JInternalFrame main, BuDesktop parent) {
    Point init = main.getLocation();
    Dimension initDim = main.getSize();
    int maxW = parent.getWidth();
    int maxH = parent.getHeight();
    int ifW = initDim.width;
    int ifH = initDim.height;
    ifW = Math.min(maxW, ifW);
    ifH = Math.min(maxH, ifH);
    int locX = Math.max(0, Math.min(init.x, maxW - ifW));
    int locY = Math.max(0, Math.min(init.y, maxH - ifH));
    Point newPt = new Point(locX, locY);
    Dimension newDimension = new Dimension(ifW, ifH);
    if (!newPt.equals(init) || newDimension.equals(initDim)) {
      main.setLocation(newPt);
      main.setSize(newDimension);
    }
  }

  public static Point loadComponentLocationAndDimension(final Component window, final Preferences preferences,
          final String mainWindow) {

    final Dimension dim = loadDim(mainWindow, preferences);
    if (dim != null) {
      window.setPreferredSize(dim);
      window.setSize(dim);
    }
    final Point loc = loadLocation(mainWindow, preferences);
    if (loc != null) {
      window.setLocation(loc);
    }
    return loc;
  }
  public static Point loadComponentLocationAndDimension(final Component window, final FuPreferences preferences,
          final String mainWindow) {

    final Dimension dim = loadDim(mainWindow, preferences);
    if (dim != null) {
      window.setPreferredSize(dim);
      window.setSize(dim);
    }
    final Point loc = loadLocation(mainWindow, preferences);
    if (loc != null) {
      window.setLocation(loc);
    }
    return loc;
  }

  /**
   * @param prefix le prefixe a utilser pour les cles
   * @param parentDim
   * @param pref les preferences a utiliser
   * @return la dim lue ou null si non trouvee
   */
  public static Dimension loadRatioDim(final String prefix, final Dimension parentDim, final Preferences pref) {
    final double w = pref.getDouble(prefix + ".ratio.w", -1d);
    if (w <= 0) {
      return null;
    }
    final double h = pref.getDouble(prefix + ".ratio.h", -1d);
    if (h <= 0) {
      return null;
    }
    return new Dimension((int) (w * parentDim.width), (int) (h * parentDim.height));
  }

  /**
   * @param prefix le prefixe a utilser pour les cles
   * @param pref les preferences a utiliser
   * @return le point lu ou null si non trouve
   */
  public static Point loadLocation(final String prefix, final Preferences pref) {
    final int x = pref.getInt(prefix + ".x", -1);
    if (x < 0) {
      return null;
    }
    final int y = pref.getInt(prefix + ".y", -1);
    if (y < 0) {
      return null;
    }
    return new Point(x, y);
  }
  public static Point loadLocation(final String prefix, final FuPreferences pref) {
    final int x = pref.getIntegerProperty(prefix + ".x", -1);
    if (x < 0) {
      return null;
    }
    final int y = pref.getIntegerProperty(prefix + ".y", -1);
    if (y < 0) {
      return null;
    }
    return new Point(x, y);
  }

  /**
   * @param pref les pref a modifier
   * @param table la table a sauver: colonne + visibilite
   */
  public static void saveTablePreferences(final Preferences pref, final JXTable table) {
    final List<TableColumn> columns = ((TableColumnModelExt) table.getColumnModel()).getColumns(true);
    final String tablePrefix = getComponentPreferencesPrefix(table);
    saveIn(table.getSize(), tablePrefix + "dimension.", pref);
    final String wId = tablePrefix + "column.width.";
    final String visibleId = tablePrefix + "column.visible.";
    for (final TableColumn tableColumn : columns) {
      final String colName = table.getModel().getColumnName(tableColumn.getModelIndex());
      pref.putInt(wId + colName, tableColumn.getWidth());
      pref.putBoolean(visibleId + colName, ((TableColumnExt) tableColumn).isVisible());
    }
  }

  public static GraphicsDevice getDevice(final Component window) {
    GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
    Rectangle boundsOfWindow = window.getBounds();
    if (screenDevices != null && screenDevices.length > 1) {
      for (GraphicsDevice graphicsDevice : screenDevices) {
        Rectangle bounds = graphicsDevice.getDefaultConfiguration().getBounds();
        Point p = new Point(boundsOfWindow.x, bounds.y);
        if (bounds.contains(p)) {
          p.x = boundsOfWindow.x + boundsOfWindow.width;
          if (bounds.contains(p)) {
            return graphicsDevice;
          }
        }
      }
    }
    return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
  }

  public static void ensureComponentWillBeVisible(final Component window, final Point initLocation) {

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    ensureComponentWillBeVisible(window, initLocation, screenSize);
    GraphicsDevice[] screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
    if (screenDevices != null && screenDevices.length > 1) {
      GraphicsDevice device = getDevice(window);
      if (device != null) {
        ensureComponentWillBeVisible(window, initLocation, device.getDefaultConfiguration().getBounds().getSize(),
                false);
      }
    }
  }

  private static void ensureComponentWillBeVisible(final Component window, final Point initLocation,
          final Dimension screenSize) {
    ensureComponentWillBeVisible(window, initLocation, screenSize, true);
  }

  private static void ensureComponentWillBeVisible(final Component window, final Point initLocation,
          final Dimension screenSize, boolean modifyX) {
    final Dimension dim = window.getSize();

    boolean size = false;
    if (dim.width > screenSize.width) {
      size = true;
      dim.width = screenSize.width;
    }
    if (dim.height > screenSize.height) {
      size = true;
      dim.height = screenSize.height;
    }
    if (size) {
      window.setSize(dim);
      window.setPreferredSize(dim);
    }
    if (initLocation == null) {
      return;
    }
    if (modifyX) {
      initLocation.x = Math.max(0, Math.min(initLocation.x, screenSize.width - dim.width));
    }
    initLocation.y = Math.max(0, Math.min(initLocation.y, screenSize.height - dim.height));
    window.setLocation(initLocation);
  }

  /**
   * @param d le point a sauvegarder
   * @param prefix le prefixe a utilser pour les cles
   * @param pref les preferences a modifier
   */
  public static void saveIn(final Point d, final String prefix, final Preferences pref) {
    // on s'assure d'avoir au moins 0: car si la fenetre est maximise, la location
    // renvoie -4 ,-4
    pref.putInt(prefix + ".x", Math.max(0, d.x));
    pref.putInt(prefix + ".y", Math.max(0, d.y));
  }
  public static void saveIn(final Point d, final String prefix, final FuPreferences pref) {
    // on s'assure d'avoir au moins 0: car si la fenetre est maximise, la location
    // renvoie -4 ,-4
    pref.putIntegerProperty(prefix + ".x", Math.max(0, d.x));
    pref.putIntegerProperty(prefix + ".y", Math.max(0, d.y));
  }

  /**
   * @param prefix le prefixe a utilser pour les cles
   * @param pref les preferences a utiliser
   * @return la dim lue ou null si non trouvee
   */
  public static Dimension loadDim(final String prefix, final Preferences pref) {
    final int w = pref.getInt(prefix + ".w", -1);
    if (w <= 0) {
      return null;
    }
    final int h = pref.getInt(prefix + ".h", -1);
    if (h <= 0) {
      return null;
    }
    return new Dimension(w, h);
  }
  public static Dimension loadDim(final String prefix, final FuPreferences pref) {
    final int w = pref.getIntegerProperty(prefix + ".w", -1);
    if (w <= 0) {
      return null;
    }
    final int h = pref.getIntegerProperty(prefix + ".h", -1);
    if (h <= 0) {
      return null;
    }
    return new Dimension(w, h);
  }

  public static boolean loadExtendedState(final String prefix, final Preferences pref) {
    return pref.getBoolean(prefix + ".isExtended", false);
  }

  public static void saveExtendedState(final String prefix, final Preferences pref, boolean val) {
    pref.putBoolean(prefix + ".isExtended", val);
  }

  public static void saveIn(final Dimension d, final String prefix, final Preferences pref) {
    pref.putInt(prefix + ".w", d.width);
    pref.putInt(prefix + ".h", d.height);
  }
  public static void saveIn(final Dimension d, final String prefix, final FuPreferences pref) {
    pref.putIntegerProperty(prefix + ".w", d.width);
    pref.putIntegerProperty(prefix + ".h", d.height);
  }

  /**
   * @param d les dimension du composant a suaver
   * @param parentDim les dimension parentes utilisees pour le ratio.
   * @param prefix prefix a utiliser pour les preferences
   * @param pref lles preferences a modifier
   */
  public static void saveRatioIn(final Dimension d, final Dimension parentDim, final String prefix,
          final Preferences pref) {
    pref.putDouble(prefix + ".ratio.w", ((double) d.width) / (double) parentDim.width);
    pref.putDouble(prefix + ".ratio.h", ((double) d.height) / (double) parentDim.height);
  }

  /**
   * @param pref les preferences a utiliser
   * @param table la table a modifier
   * @param defaultWidths les largeurs a utiliser par default
   */
  public static void loadTablePreferences(final Preferences pref, final JXTable table,
          final Map<String, Integer> defaultWidths) {
    final String tablePrefix = getComponentPreferencesPrefix(table);
    loadTableSizePreferences(pref, table, tablePrefix);
    final List<TableColumn> columns = ((TableColumnModelExt) table.getColumnModel()).getColumns(true);

    final String wId = tablePrefix + "column.width.";
    final String visibleId = tablePrefix + "column.visible.";
    for (final TableColumn tableColumn : columns) {
      final String colName = table.getModel().getColumnName(tableColumn.getModelIndex());
      final Integer defaultWidth = defaultWidths == null ? null : defaultWidths.get(colName);
      final int w = pref.getInt(wId + colName, defaultWidth == null ? -1 : defaultWidth.intValue());
      if (w > 0) {
        tableColumn.setPreferredWidth(w);
        tableColumn.setWidth(w);
      }
      // par defaut les colonnes sont visibles: donc on ne traite que le cas cache.
      if (!pref.getBoolean(visibleId + colName, true)) {
        ((TableColumnExt) tableColumn).setVisible(false);
      }
    }
  }

  /**
   * Dimensionne la table selon les preferences sauvegardees.
   *
   * @param pref les preferences a utiliser
   * @param table la table a modifier
   * @param defaultWidths les largeurs a utiliser par default
   */
  private static void loadTableSizePreferences(final Preferences pref, final JXTable table, final String tablePrefix) {
    final Dimension d = loadDim(tablePrefix + "dimension.", pref);
    if (d != null) {
      table.setSize(d);
    }
  }

  /**
   * @param t
   * @return
   */
  private static String getComponentPreferencesPrefix(final JComponent t) {
    return StringUtils.defaultString(t.getName(), "noName") + ".";
  }

}
