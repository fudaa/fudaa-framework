package org.fudaa.ctulu.gui;

import com.memoire.bu.BuResource;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.text.JTextComponent;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public class ActionTransferSource extends AbstractAction {

  final JComponent source;
  final Action destAction;

  /**
   * @param name
   * @param icon
   */
  public ActionTransferSource(final JComponent source, final String destAction, final String name, Icon icon) {
    super(name, icon);
    this.source = source;
    this.destAction = source.getActionMap().get(destAction);
    super.putValue(ACTION_COMMAND_KEY, destAction);
    super.putValue(SHORT_DESCRIPTION, name);
  }

  protected boolean isStateEnable() {
    return true;
  }

  public void updateState() {
    setEnabled(isStateEnable());

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    e.setSource(source);
    destAction.actionPerformed(e);
  }

  public static boolean isSomethingToCopy(JComponent jc) {
    if (jc instanceof JTextComponent) {
      return !CtuluLibString.isEmpty(((JTextComponent) jc).getSelectedText());
    }
    if (jc instanceof JTable) {
      return !((JTable) jc).getSelectionModel().isSelectionEmpty();
    }
    if (jc instanceof JTree) {
      return !((JTree) jc).getSelectionModel().isSelectionEmpty();
    }
    return false;
  }

  public static class CopyAction extends ActionTransferSource {

    /**
     * @param source
     * @param destAction
     */
    public CopyAction(JComponent source) {
      super(source, getActionName(), BuResource.BU.getString("Copier"), BuResource.BU.getIcon("crystal_copier"));
      final KeyStroke keyStroke = getKeyStroke();
      putValue(ACCELERATOR_KEY, keyStroke);
      source.getInputMap().put(getKeyStroke(), "newCopy");
      source.getActionMap().put("newCopy", this);
    }

    public static String getActionName() {
      return "copy";
    }

    public static KeyStroke getKeyStroke() {
      return KeyStroke.getKeyStroke("control C");
    }

    @Override
    public boolean isStateEnable() {
      return isSomethingToCopy(source);
    }
  }

  /**
   * Paste from the system clipboard.
   *
   * @author deniger
   */
  public static class PasteAction extends ActionTransferSource {

    /**
     * @param source
     * @param destAction
     */
    public PasteAction(JComponent source) {
      super(source, getActionName(), BuResource.BU.getString("Coller"), BuResource.BU.getIcon("crystal_coller"));
      final KeyStroke keyStroke = getKeyStroke();
      putValue(ACCELERATOR_KEY, keyStroke);
      source.getInputMap().put(getKeyStroke(), "newPaste");
      source.getActionMap().put("newPaste", this);
    }

    public static String getActionName() {
      return "paste";
    }

    public static KeyStroke getKeyStroke() {
      return KeyStroke.getKeyStroke("control V");
    }

    @Override
    public boolean isStateEnable() {
      TransferHandler transferHandler = source.getTransferHandler();
      if (transferHandler == null) {
        return false;
      }
      Transferable contents = source.getToolkit().getSystemClipboard().getContents(null);
      return transferHandler.canImport(new TransferSupport(source, contents));
    }
  }

  public static class CutAction extends ActionTransferSource {

    /**
     * @param source
     * @param destAction
     */
    public CutAction(JComponent source) {
      super(source, "cut", BuResource.BU.getString("Couper"), BuResource.BU.getIcon("crystal_couper"));
      final KeyStroke keyStroke = getKeyStroke();
      putValue(ACCELERATOR_KEY, keyStroke);
      source.getInputMap().put(getKeyStroke(), "newCut");
      source.getActionMap().put("newCut", this);
    }

    public static String getActionName() {
      return "paste";
    }

    public static KeyStroke getKeyStroke() {
      return KeyStroke.getKeyStroke("control X");
    }

    @Override
    public boolean isStateEnable() {
      TransferHandler transferHandler = source.getTransferHandler();
      int mode = transferHandler.getSourceActions(source);
      if (mode != TransferHandler.COPY_OR_MOVE && mode != TransferHandler.MOVE) {
        return false;
      }
      return isSomethingToCopy(source);
    }
  }
}
