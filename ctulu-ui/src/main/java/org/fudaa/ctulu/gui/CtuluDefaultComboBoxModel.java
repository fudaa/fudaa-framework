/**
 *  @creation     2 mars 2005
 *  @modification $Date: 2006-07-13 13:34:40 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gui;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * @author Fred Deniger
 * @version $Id: CtuluDefaultComboBoxModel.java,v 1.2 2006-07-13 13:34:40 deniger Exp $
 */
public class CtuluDefaultComboBoxModel extends AbstractListModel implements ComboBoxModel {

  Object[] o_;
  Object selected_;
  /**
   * @param _o
   */
  public CtuluDefaultComboBoxModel(final Object[] _o) {
    super();
    o_ = _o;
  }

  @Override
  public Object getElementAt(final int _index){
    return o_ == null ? null : o_[_index];
  }

  @Override
  public int getSize(){
    return o_ == null ? 0 : o_.length;
  }

  @Override
  public Object getSelectedItem(){
    return selected_;
  }

  @Override
  public void setSelectedItem(final Object _anItem){
    if (_anItem != selected_) {
      selected_ = _anItem;
      fireContentsChanged(this, -1, -1);
    }
  }

}