/*
 GPL 2
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuPreferences;
import java.io.File;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
public class CtuluExportTableChooseFile {

  private final CtuluFileChooserCsvExcel choose;

  public CtuluExportTableChooseFile(CtuluFileChooserCsvExcel choose) {
    this.choose = choose;
  }

  public File chooseExportFile() {
    String defaultDir = BuPreferences.BU.getStringProperty("export.excel.lastPath", null);
    if (StringUtils.isNotBlank(defaultDir)) {
      File dir = new File(defaultDir);
      if (dir.exists() && dir.isDirectory()) {
        choose.setCurrentDirectory(dir);

      }
    }
    final File f = choose.getDestFile();
    if (f != null) {
      File dir = f.getParentFile();
      if (dir != null) {
        BuPreferences.BU.putStringProperty("export.excel.lastPath", dir.getAbsolutePath());
        BuPreferences.BU.writeIniFile();
      }
    }
    return f;
  }

}
