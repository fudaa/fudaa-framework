/*
 * @creation 20 avr. 2005
 *
 * @modification $Date: 2007-03-09 08:37:37 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuScrollPane;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.table.CtuluTable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Un editeur de collections de valeurs.
 *
 * @author Fred Deniger, marchand@deltacad.fr
 * @version $Id: CtuluValuesMultiEditorPanel.java,v 1.12 2007-03-09 08:37:37 deniger Exp $
 */
public class CtuluValuesMultiEditorPanel extends CtuluDialogPanel {
  /**
   * Une ligne du tableau, sans la colonne des indices.
   */
  protected class Row {
    /**
     * Les valeurs initiales des cellules correspondantes aux attributs
     */
    private Object[] cells_;
    /**
     * Les valeurs modifi�es, null si pas modifi�e
     */
    private Object[] modCells_;

    public Row(Object[] _values) {
      cells_ = _values;
    }

    public Object getValue(int _col) {
      if (modCells_ != null && modCells_[_col] != null) {
        return modCells_[_col];
      } else {
        return cells_[_col];
      }
    }

    public boolean isModified(int _col) {
      return modCells_ != null && modCells_[_col] != null;
    }

    public void setValue(Object _obj, int _col) {
      if (modCells_ == null) {
        modCells_ = new Object[cells_.length];
      }

      if (cells_[_col].equals(_obj)) {
        modCells_[_col] = null;
      } else {
        modCells_[_col] = _obj;
      }
    }
  }

  /**
   * Le modele du tableau.
   */
  class ValueTableModel extends AbstractTableModel {
    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex == 0) {
        return String.class;
      }
      return editors_[_columnIndex - 1].getDataClass();
    }

    @Override
    public int getColumnCount() {
      return 1 + values_.length;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return CtuluLib.getS("Indice");
      }
      return title_[_column - 1].toString();
    }

    private int getRealIdx(final int _rowIdx) {
      return convertViewToMdlIdx(_rowIdx);
    }

    @Override
    public int getRowCount() {
      return idxMdlSel_.length;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      final int realIdx = getRealIdx(_rowIndex);
      if (_columnIndex == 0) {
        return CtuluLibString.getString(idxMdlSel_[_rowIndex] + 1);
      }
      return rows_.get(realIdx).getValue(_columnIndex - 1);
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex > 0) {
        return editors_[_columnIndex - 1].isEditable();
      }
      return false;
    }

    public boolean isModified(final int _rowIndex, final int _columnIndex) {
      final int realIdx = getRealIdx(_rowIndex);
      return rows_.get(realIdx).isModified(_columnIndex - 1);
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0 || _value == getValueAt(_rowIndex, _columnIndex)) {
        return;
      }
      final int var = _columnIndex - 1;
      if (!editors_[var].isValid(_value)) {
        return;
      }
      final int realIdx = getRealIdx(_rowIndex);
      rows_.get(realIdx).setValue(_value, var);
    }
  }

  protected CtuluCommandContainer cmd_;
  protected final CtuluValueEditorI[] editors_;
  /**
   * Les seuls indices de mod�le selectionn�s � l'origine
   */
  protected int[] idxMdlSel_;
  protected ValueTableModel model_;
  /**
   * Les lignes, dans l'ordre
   */
  protected List<Row> rows_;
  protected JTable table_;
  protected final Object[] title_;
  protected final CtuluCollection[] values_;
  private CtuluValuesCommonEditorVariableProvider variablesProvider;

  public CtuluValuesMultiEditorPanel(final CtuluValuesParameters _params) {
    this(_params.getNames(), _params.getIdx(), _params.getEditors(), _params.getValues());
    this.variablesProvider = _params.getVariableProvider();
  }

  /**
   * @param _names les noms (non null)
   * @param _idxMdlSel les indices s�lectionn�s.
   * @param _editors les editeurs
   * @param _values les valeurs (meme taille que les editeurs)
   */
  public CtuluValuesMultiEditorPanel(final Object[] _names, final int[] _idxMdlSel, final CtuluValueEditorI[] _editors,
                                     final CtuluCollection[] _values) {
    editors_ = _editors;
    values_ = _values;
    title_ = _names;
    idxMdlSel_ = _idxMdlSel;

    init();
    buildRows();
    CtuluLibSwing.packJTable(table_);
  }

  protected void addRow(Object[] _cells) {
    rows_.add(new Row(_cells));
  }

  @Override
  public boolean apply() {
    // on arrete la saisie avant d'appliquer les modifs
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }

    // Mise a jour des champs.
    final CtuluCommandComposite cmp = new CtuluCommandComposite(CtuluLib.getS("Edition des g�om�tries"));
    for (int irow = 0; irow < rows_.size(); irow++) {
      for (int icol = 0; icol < values_.length; icol++) {
        if (rows_.get(irow).isModified(icol)) {
          values_[icol].setObject(irow, rows_.get(irow).getValue(icol), cmp);
        }
      }
    }
    if (cmd_ != null) {
      cmd_.addCmd(cmp.getSimplify());
    }
    
    return true;
  }

  /**
   * Construit les lignes.
   */
  private void buildRows() {
    rows_ = new ArrayList<Row>();
    if (values_.length > 0) {
      int size = values_[0].getSize();
      for (int i = 0; i < size; i++) {
        Object[] cells = new Object[values_.length];
        for (int j = 0; j < cells.length; j++) {
          cells[j] = values_[j].getObjectValueAt(i);
        }
        addRow(cells);
      }
    }
  }

  protected int convertViewToMdlIdx(int _idx) {
    return idxMdlSel_[_idx];
  }

  public CtuluValuesCommonEditorPanel createCommon() {
    final CtuluValuesCommonEditorPanel res = new CtuluValuesCommonEditorPanel(title_, idxMdlSel_, editors_, values_, variablesProvider);
    res.setCmd(cmd_);
    return res;
  }

  /**
   * @return le receveur de commandes
   */
  public final CtuluCommandContainer getCmd() {
    return cmd_;
  }

  /**
   * @return La selection initiale
   */
  public int[] getSelectedIndexes() {
    return idxMdlSel_;
  }

  protected void init() {
    if (editors_.length != values_.length) {
      throw new IllegalArgumentException("same size required");
    }
    setLayout(new BuBorderLayout());

    model_ = new ValueTableModel();
    table_ = new CtuluTable();
    // Pour permettre le copy/paste facilement surle tableau.
    table_.setCellSelectionEnabled(true);
    table_.setModel(model_);

    TableColumnModel model = table_.getColumnModel();
    // on construit les labels et les editeurs, et les renderer
    for (int i = 0; i < title_.length; i++) {
      model.getColumn(i + 1).setCellEditor(editors_[i].createTableEditorComponent());
      model.getColumn(i + 1).setCellRenderer(editors_[i].createTableRenderer());
    }

    table_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    add(new BuScrollPane(table_), BuBorderLayout.CENTER);

  }

  /**
   * @param _cmd le receveur de commande
   */
  public final void setCmd(final CtuluCommandContainer _cmd) {
    cmd_ = _cmd;
  }
}
