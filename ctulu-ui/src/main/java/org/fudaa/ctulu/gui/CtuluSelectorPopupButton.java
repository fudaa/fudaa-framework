/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ctulu.gui;

import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * @author deniger
 */
public abstract class CtuluSelectorPopupButton {

  JButton jb;
  private JPopupMenu menu_;

  public ActionListener createActionActionListener() {
    return new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent _e) {
        JPopupMenu menu = createPopup();
        JComponent jc = (JComponent) _e.getSource();
        final Dimension pref = menu.getPreferredSize();
        final Point popupLocation = new Point((jc.getWidth() - pref.width) / 2, (jc.getHeight() - pref.height) / 2);
        try {
          final Point center = new Point(jc.getWidth() / 2, jc.getHeight() / 2);
          SwingUtilities.convertPointToScreen(center, jc);
          new Robot().mouseMove(center.x, center.y);
        } catch (Exception ex) {
          FuLog.error(ex);
        }
        menu.show(jc, popupLocation.x, popupLocation.y);
      }
    };
  }

  public abstract JComponent createComponent();

  private JPopupMenu createPopup() {
    if (menu_ == null) {
      menu_ = new JPopupMenu();
      menu_.setLayout(new BorderLayout());
      menu_.add(createComponent());
    }
    return menu_;
  }

  public JButton getButton() {
    if (jb == null) {
      jb = new JButton(ScrollPaneSelector.LAUNCH_SELECTOR_ICON);
      jb.setFocusable(false);
      jb.setPreferredSize(new Dimension(16, 16));
      jb.setMaximumSize(jb.getPreferredSize());
      jb.addActionListener(createActionActionListener());
    }
    return jb;

  }

  public void installButton(JScrollPane _jc) {

    try {
      _jc.setCorner(ScrollPaneConstants.LOWER_TRAILING_CORNER, getButton());
    } catch (final IllegalArgumentException _evt) {
      _jc.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, getButton());
    }
  }
}
