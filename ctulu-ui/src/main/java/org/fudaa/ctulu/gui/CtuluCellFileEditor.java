/*
 *  @creation     3 juin 2003
 *  @modification $Date: 2007-06-05 08:57:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuFileChooser;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.EventObject;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.CtuluLibFile;

/**
 * @author deniger
 * @version $Id: CtuluCellFileEditor.java,v 1.7 2007-06-05 08:57:45 deniger Exp $
 */
public class CtuluCellFileEditor extends JButton implements TableCellEditor, ActionListener {

  //protected transient ChangeEvent changeEvent;
  BuFileChooser bfc_;
  int fileChooserSelectionMode_;
  boolean multipleSelection_;
  private File baseDir_;
  boolean onlyName_;
  CtuluCellDecorator decorator_;
  String value_;
  boolean doubleClick_;

  //  int maxParentForRelativePath_= 3;
  public CtuluCellFileEditor() {
    this(null);
  }

  public CtuluCellFileEditor(final CtuluCellDecorator _deco) {
    setOpaque(false);
    setEnabled(true);
    addActionListener(this);
    decorator_ = _deco;
    setBorder(UIManager.getBorder("Label.border"));
    setFont(UIManager.getFont("Table.font"));
    setHorizontalAlignment(0);
    setAlignmentX(0);
  }

  @Override
  public Component getTableCellEditorComponent(final JTable _table,final Object _value,final boolean _isSelected,
    final int _row,final int _column){
    if (_value instanceof File) {
      setValue(((File) _value).getAbsolutePath());
    } else {
      setValue((String) _value);
    }
    if (decorator_ != null) {
      decorator_.decore(this, _table, _value, _row, _column);
    }
    return this;
  }

  public void setValue(final String _o){
    value_ = _o;
    setText(value_);
  }

  @Override
  public Object getCellEditorValue(){
    return value_;
  }

  @Override
  public boolean isCellEditable(final EventObject _anEvent){
    if (!doubleClick_) {
      return true;
    }
    if (_anEvent instanceof MouseEvent) {
      return ((MouseEvent) _anEvent).getClickCount() >= 2;
    }
    return true;
  }

  @Override
  public boolean shouldSelectCell(final EventObject _anEvent){
    return true;
  }

  @Override
  public void cancelCellEditing(){
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == CellEditorListener.class) {
        if (changeEvent == null) {
          changeEvent = new ChangeEvent(this);
        }
        ((CellEditorListener) listeners[i + 1]).editingCanceled(changeEvent);
      }
    }
  }

  @Override
  public void addCellEditorListener(final CellEditorListener _l){
    listenerList.add(CellEditorListener.class, _l);
  }

  @Override
  public void removeCellEditorListener(final CellEditorListener _l){
    listenerList.remove(CellEditorListener.class, _l);
  }

  @Override
  public boolean stopCellEditing(){
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == CellEditorListener.class) {
        if (changeEvent == null) {
          changeEvent = new ChangeEvent(this);
        }
        ((CellEditorListener) listeners[i + 1]).editingStopped(changeEvent);
      }
    }
    return false;
  }

  /**
   *
   */
  @Override
  public void actionPerformed(final ActionEvent _ae){
    if (bfc_ == null) {
      bfc_ = new CtuluFileChooser(false);
    }
    bfc_.setFileSelectionMode(fileChooserSelectionMode_);
    bfc_.setMultiSelectionEnabled(multipleSelection_);
    if ((baseDir_ == null) || (value_ == null)) {
      if (value_ != null) {
        final File f = new File(value_);
        if (f.isAbsolute()) {
          bfc_.setSelectedFile(f);
        }
      }
    } else {
      bfc_.setCurrentDirectory(CtuluLibFile.getAbsolutePath(baseDir_, value_).getParentFile());
    }
    final int r = bfc_.showOpenDialog(CtuluCellFileEditor.this);
    if (r == JFileChooser.APPROVE_OPTION) {
      if (onlyName_) {
        setValue(bfc_.getSelectedFile().getName());
      } else if (baseDir_ == null) {
        setValue(bfc_.getSelectedFile().getAbsolutePath());
      }
      else {
        setValue(CtuluLibFile.getRelativeFile(bfc_.getSelectedFile(), baseDir_, 3));
      }
    }
    stopCellEditing();
  }

  /**
   *
   */
  public CtuluCellDecorator getDecorator(){
    return decorator_;
  }

  public void setDecorator(final CtuluCellDecorator _decorator){
    decorator_ = _decorator;
  }

  /**
   *
   */
  public boolean isDoubleClick(){
    return doubleClick_;
  }

  /**
   *
   */
  public void setDoubleClick(final boolean _b){
    doubleClick_ = _b;
  }

  /**
   *
   */
  public int getFileChooserSelectionMode(){
    return fileChooserSelectionMode_;
  }

  /**
   *
   */
  public File getBaseDir(){
    return baseDir_;
  }

  /**
   *
   */
  public boolean isMultipleSelection(){
    return multipleSelection_;
  }

  /**
   *
   */
  public boolean isOnlyName(){
    return onlyName_;
  }

  /**
   *
   */
  public void setFileChooserSelectionMode(final int _i){
    fileChooserSelectionMode_ = _i;
  }

  /**
   *
   */
  public void setBaseDir(final File _file){
    baseDir_ = _file;
  }

  /**
   *
   */
  public void setMultipleSelection(final boolean _b){
    multipleSelection_ = _b;
  }

  /**
   *
   */
  public void setOnlyName(final boolean _b){
    onlyName_ = _b;
  }

  /**
   *
   */
  public String getValue(){
    return value_;
  }
}
