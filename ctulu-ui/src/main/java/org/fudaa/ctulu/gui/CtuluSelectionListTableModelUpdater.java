/*
 * @creation 26 juil. 06
 * @modification $Date: 2007-01-17 10:45:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionEvent;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluListSelectionListener;

/**
 * @author fred deniger
 * @version $Id: CtuluSelectionListTableModelUpdater.java,v 1.1 2007-01-17 10:45:24 deniger Exp $
 */
public class CtuluSelectionListTableModelUpdater implements CtuluListSelectionListener, ListSelectionListener {

  protected final JTable t_;
  protected boolean isInternalUpdate_;
  protected final CtuluListSelection selection_;

  public CtuluSelectionListTableModelUpdater(final JTable _table, final CtuluListSelection _selection) {
    super();
    t_ = _table;
    t_.getSelectionModel().addListSelectionListener(this);
    selection_ = _selection;
    selection_.addListeSelectionListener(this);
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (isInternalUpdate_ || _e.getValueIsAdjusting() || selection_ == null) {
      return;
    }
    isInternalUpdate_ = true;
    selection_.setSelection(t_.getSelectionModel());
    isInternalUpdate_ = false;
    doAfterTableSectionChanged();

  }

  protected void doAfterCtuluListSectionChanged() {}

  protected void doAfterTableSectionChanged() {}

  @Override
  public void listeSelectionChanged(final CtuluListSelectionEvent _e) {
    if (isInternalUpdate_) {
      return;
    }
    isInternalUpdate_ = true;
    final CtuluListSelectionInterface s = (CtuluListSelectionInterface) _e.getSource();
    if (s.isEmpty()) {
      t_.clearSelection();
    } else {
      t_.requestFocusInWindow();
      t_.requestFocus();
      final ListSelectionModel m = t_.getSelectionModel();
      final ListSelectionModel col = t_.getColumnModel().getSelectionModel();
      m.setValueIsAdjusting(true);
      m.clearSelection();
      final int max = s.getMaxIndex();
      if (s.isOnlyOnIndexSelected()) {
        m.addSelectionInterval(max, max);
      } else {
        for (int i = s.getMinIndex(); i <= max; i++) {
          if (s.isSelected(i)) {
            m.addSelectionInterval(i, i);
          }
        }
      }
      m.setValueIsAdjusting(false);
      if (!m.isSelectionEmpty()) {
        col.setSelectionInterval(0, 1);
        t_.scrollRectToVisible(t_.getCellRect(max, 0, true));
      }
    }
    isInternalUpdate_ = false;
    doAfterCtuluListSectionChanged();

  }

}
