/*
 * EnhancedDialog.java - Handles OK/Cancel for you
 * Copyright (C) 1998, 1999, 2001 Slava Pestov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.ref.WeakReference;
import javax.swing.JDialog;
/**
 * A dialog box that handles window closing, the ENTER key and the ESCAPE
 * key for you. All you have to do is implement ok() (called when
 * Enter is pressed) and cancel() (called when Escape is pressed, or window
 * is closed).
 * @author Slava Pestov
 * @version $Id: CtuluEnhancedDialog.java,v 1.6 2006-10-19 14:12:03 deniger Exp $
 */
public class CtuluEnhancedDialog extends JDialog {
  public CtuluEnhancedDialog(final Frame _parent) {
    super(_parent);
    init();
  }
  public CtuluEnhancedDialog(final Dialog _c) {
    super(_c);
    init();
  }
  public CtuluEnhancedDialog() {
    super();
    init();
  }
  private void init() {
    addWindowListener(new WindowHandler(this));
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
  }
  public void cancel() {}
  // protected members
  static class WindowHandler extends WindowAdapter {
    final WeakReference ref_;
    
    public WindowHandler(final CtuluEnhancedDialog _dial){
      ref_=new WeakReference(_dial);
    }
    @Override
    public void windowClosing(final WindowEvent _evt) {
      final CtuluEnhancedDialog dest=(CtuluEnhancedDialog)ref_.get();

      // Quand le bouton close est d�sactiv�, cancel() n'est pas appel�e.
      if (dest.getDefaultCloseOperation()==DO_NOTHING_ON_CLOSE)
        return;

      if(dest!=null){
      dest.cancel();
      }
    }
  }
}
