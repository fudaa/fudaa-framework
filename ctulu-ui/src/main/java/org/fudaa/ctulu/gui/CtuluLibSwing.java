/*
 * @creation 13 d�c. 06
 * @modification $Date: 2007-05-04 13:43:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLib;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author fred deniger
 * @version $Id: CtuluLibSwing.java,v 1.8 2007-05-04 13:43:23 deniger Exp $
 */
public final class CtuluLibSwing {
  private static Font minFont;

  private CtuluLibSwing() {
  }

  public static Frame getFrameAncestorHelper(final Component _c) {
    final Frame res = getFrameAncestor(_c);
    return res == null ? BuLib.HELPER : res;
  }

  public static Icon getWarningIcon() {
    return UIManager.getIcon("OptionPane.warningIcon");
  }

  public static CtuluTitledPanelCheckBox createTitleCheckBox(final String _text) {
    return new CtuluTitledPanelCheckBox(new BuCheckBox(_text));
  }

  /**
   * @param _parent le composant � partir duquel la recherche commence
   * @param _name le nom du composant fils � chercher
   * @return le fils direct de _parent ayant le nom donne ou null si non trouve
   */
  public static JComponent findChildByName(JComponent _parent, String _name) {
    if (_parent == null || _name == null) {
      return null;
    }
    for (int i = _parent.getComponentCount() - 1; i >= 0; i--) {
      if (_name.equals(_parent.getComponent(i).getName())) {
        return (JComponent) _parent.getComponent(i);
      }
    }
    return null;
  }

  public static JComponent findChildByClass(JComponent _parent, Class _name) {
    if (_parent == null || _name == null) {
      return null;
    }
    for (int i = _parent.getComponentCount() - 1; i >= 0; i--) {
      if (_name.equals(_parent.getComponent(i).getClass())) {
        return (JComponent) _parent.getComponent(i);
      }
    }
    return null;
  }

  /**
   * Ajouter un actionListener a la checkbox _res afin d'activer ou non un panneau (et tous ces composants).
   *
   * @param _target le panneau dont les composants seront activ�es ou non, selon l'�tat de la checkbox _res
   * @param _res la checkbox permettant de activer ou non un panneau
   */
  public static void addActionListenerForCheckBoxTitle(final JPanel _target, final JCheckBox _res) {
    _res.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        final boolean enable = _res.isSelected();
        setEnable(_target, _res, enable);
      }
    });
  }

  public static String buildLinkToHelp(final JComponent _c) {
    String help = getHelpUrl(_c);
    if (help != null) {
      help = "<a href=\"" + help + "\">" + CtuluLib.getS("Voir la documentation") + "</a>";
    }
    return help;
  }

  public static String getHelpUrl(final JComponent _c) {
    if (_c == null) {
      return null;
    }
    String r = (String) _c.getClientProperty(CtuluLib.getHelpProperty());
    if (r == null) {
      final Container parent = _c.getParent();
      if (parent instanceof JComponent) {
        r = (String) ((JComponent) parent).getClientProperty(CtuluLib.getHelpProperty());
      }
    }
    return r;
  }

  public static Frame getFrameAncestor(final Component _c) {
    if (_c instanceof Frame) {
      return (Frame) _c;
    }
    Frame fr = (Frame) SwingUtilities.getAncestorOfClass(JFrame.class, _c);
    if (fr != null) {
      return fr;
    }
    final BuApplication app = (BuApplication) SwingUtilities.getAncestorOfClass(BuApplication.class, _c);
    if (app != null) {
      return app;
    }
    return null;
  }

  /**
   * @return la window active
   */
  public static Window getActiveWindow() {
    return KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
  }

  public static JDialog createDialogOnActiveWindow(String _title) {
    Window w = getActiveWindow();
    if (w instanceof Frame) {
      return new JDialog((Frame) w, _title);
    }
    if (w instanceof Dialog) {
      return new JDialog((Dialog) w, _title);
    }
    return new JDialog(BuLib.HELPER, _title);
  }

  public static BuList createBuList(final Object[] _o) {
    if (_o == null) {
      return new BuList();
    }
    final BuList l = new BuList();
    l.setModel(CtuluLibSwing.createListModel(_o));
    return l;
  }

  public static BuList createBuList(final java.util.List _o) {
    if (_o == null) {
      return new BuList();
    }
    final BuList l = new BuList();
    l.setModel(CtuluLibSwing.createListModel(_o));
    return l;
  }

  /**
   * @return font "SansSerif", Font.PLAIN, 10
   */
  public static Font getMiniFont() {
    if (minFont == null) {
      minFont = new Font("SansSerif", Font.PLAIN, 10);
    }
    return minFont;
  }

  public static Color getDefaultLabelForegroundColor() {
    return UIManager.getColor("Label.foreground");
  }

  public static Font getDefaultLabelFont() {
    final Font font = UIManager.getFont("Label.font");
    return new Font(font.getName(), font.getStyle(), font.getSize());
  }

  public static Color getDefaultTitleBorderColor() {
    return UIManager.getColor("TitledBorder.titleColor");
  }

  public static Color getDefaultCheckboxForegroundColor() {
    return UIManager.getColor("CheckBox.foreground");
  }

  public static Color getDefaultTextFieldForegroundColor() {
    return UIManager.getColor("TextField.foreground");
  }

  public static Color getDefaultTextAreaForegroundColor(final JTextField _txt) {
    return UIManager.getColor("TextArea.foreground");
  }

  /**
   * Permet d'afficher un dialogue simplement.
   *
   * @param _parent
   * @param _title
   * @param _optionPane return JOptionPane constante
   */
  public static int showDialog(final Component _parent, final String _title, final JOptionPane _optionPane) {
    final JDialog d = _optionPane.createDialog(_parent, _title);
    if (d == null) {
      return JOptionPane.CLOSED_OPTION;
    }
    // en fait c'est juste pour avoir le joli icone en haut a gauche
    d.setResizable(true);
    d.setModal(true);
    d.pack();
    d.setVisible(true);

    d.dispose();
    final Object selectedValue = _optionPane.getValue();
    if (selectedValue == null) {
      return JOptionPane.CLOSED_OPTION;
    }
    if (selectedValue instanceof Integer) {
      return ((Integer) selectedValue).intValue();
    }
    Frame f = getFrameAncestor(_parent);
    if (f != null) {
      f.toFront();
    }
    return JOptionPane.CLOSED_OPTION;
  }

  public static Object showDialogOptions(final Component _parent, final String _title, final JOptionPane _optionPane) {
    final JDialog d = _optionPane.createDialog(_parent, _title);
    // en fait c'est juste pour avoir le joli icone en haut a gauche
    d.setResizable(true);
    d.setModal(true);
    d.pack();
    d.setVisible(true);
    d.dispose();
    return _optionPane.getValue();
  }

  public static void setEnable(final Container _pn, final boolean _b) {
    setEnable(_pn, null, _b);
  }

  public static Border createTitleBorder(final String _title) {
    return createTitleBorder(_title, BuBorders.EMPTY2222);
  }

  /**
   * @param _values les valeurs parmi lesquelles on veut choisir
   * @param _parent le composant parent
   * @param _title le titre du dialog
   * @param _lb le label
   * @param _vert true si on veut que le label est le com soit place verticalement
   * @return l'indice de l'objet s�lectionne au -1 si l'utilisateur a refuse
   */
  public static int chooseValue(Object[] _values, Component _parent, String _title, String _lb, boolean _vert) {
    CtuluDialogPanel pn = new CtuluDialogPanel();
    if (_vert) {
      pn.setLayout(new BuVerticalLayout(5));
    } else {
      pn.setLayout(new BuGridLayout(2));
    }
    pn.addLabel(_lb);
    JComboBox res = pn.addComboBox(_values, pn);
    if (pn.afficheModaleOk(_parent, _title)) {
      return res.getSelectedIndex();
    }
    return -1;
  }

  public static Border createTitleBorder(final String _title, final Border _inBorder) {
    return BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(_title), _inBorder);
  }

  public static AbstractListModel createListModel(final Object[] _o) {
    return new AbstractListModel() {
      @Override
      public int getSize() {
        return _o.length;
      }

      @Override
      public Object getElementAt(final int _i) {
        return _o[_i];
      }
    };
  }

  public static AbstractListModel createListModel(final java.util.List _o) {
    return new AbstractListModel() {
      @Override
      public int getSize() {
        return _o.size();
      }

      @Override
      public Object getElementAt(final int _i) {
        return _o.get(_i);
      }
    };
  }

  public static void griserPanel(final Container _target, final boolean _enable) {
    setEnable(_target, _enable);
  }

  public static void setEnable(final Container _target, final JComponent _resToAvoid, final boolean _enable) {
    for (int i = _target.getComponentCount() - 1; i >= 0; i--) {
      final Component c = _target.getComponent(i);
      if (c != _resToAvoid) {
        c.setEnabled(_enable);
        if (c instanceof Container) {
          setEnable((Container) c, _resToAvoid, _enable);
        }
      }
    }
  }

  public static void setErrorInLabel(final JComponent _cp) {
    final Component c = (Component) _cp.getClientProperty("labeledBy");
    if (c != null) {
      c.setForeground(Color.RED);
    }
  }

  public static void packJTable(JTable table) {
    for (int i = 0; i < table.getColumnCount(); i++) {
      packJTable(table, i);
    }
  }

  public static void packJTable(JTable table, int myColumn) {
    int width = 0;
    for (int row = 0; row < table.getRowCount(); row++) {
      TableCellRenderer renderer = table.getCellRenderer(row, myColumn);
      Component comp = table.prepareRenderer(renderer, row, myColumn);
      width = Math.max(comp.getPreferredSize().width, width);
    }
    TableCellRenderer headerRenderer = table.getTableHeader().getColumnModel().getColumn(myColumn).getHeaderRenderer();
    if (headerRenderer == null) {
      headerRenderer = table.getTableHeader().getDefaultRenderer();
    }
    Component comp = headerRenderer.getTableCellRendererComponent(table, table.getColumnModel().getColumn(myColumn).getHeaderValue(), true, true, 0, myColumn);
    width = Math.max(comp.getPreferredSize().width, width);
    width += 10;
    table.getTableHeader().getColumnModel().getColumn(myColumn).setPreferredWidth(width);
  }
}
