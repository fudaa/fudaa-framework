/**
 *  @creation     9 juil. 2004
 *  @modification $Date: 2007-03-30 15:35:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBrowserControl;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;


/**
 * @author Fred Deniger
 * @version $Id: CtuluOptionPane.java,v 1.2 2007-03-30 15:35:20 deniger Exp $
 */
public class CtuluOptionPane extends JOptionPane {

  public CtuluOptionPane() {
    super();
  }

  /**
   * @param _message le message a afficher
   */
  public CtuluOptionPane(final Object _message) {
    super(_message);
  }

  /**
   * @param _message
   * @param _messageType
   */
  public CtuluOptionPane(final Object _message, final int _messageType) {
    super(_message, _messageType);
    updateHtml();
  }

  /**
   * @param _message
   * @param _messageType
   * @param _optionType
   */
  public CtuluOptionPane(final Object _message, final int _messageType, final int _optionType) {
    super(_message, _messageType, _optionType);
    updateHtml();
  }
  

  /**
   * @param _message
   * @param _messageType
   * @param _optionType
   * @param _icon
   */
  public CtuluOptionPane(final Object _message, final int _messageType, final int _optionType, final Icon _icon) {
    super(_message, _messageType, _optionType, _icon);
    updateHtml();
  }

  /**
   * @param _message
   * @param _messageType
   * @param _optionType
   * @param _icon
   * @param _options
   */
  public CtuluOptionPane(final Object _message, final int _messageType, final int _optionType, final Icon _icon,
      final Object[] _options) {
    super(_message, _messageType, _optionType, _icon, _options);
    updateHtml();
  }

  /**
   * @param _message
   * @param _messageType
   * @param _optionType
   * @param _icon
   * @param _options
   * @param _initialValue
   */
  public CtuluOptionPane(final Object _message, final int _messageType, final int _optionType, final Icon _icon,
      final Object[] _options, final Object _initialValue) {
    super(_message, _messageType, _optionType, _icon, _options, _initialValue);
    updateHtml();
  }

  protected void linkActivated(final String _url) {
    BuBrowserControl.displayURL(_url);
  }

  private void updateHtml() {
    if (message instanceof String && ((String) message).startsWith("<html>")) {
      final JEditorPane pane = new JEditorPane();
      pane.setOpaque(false);
      pane.setEditable(false);
      pane.setEditorKit(new HTMLEditorKit());
      pane.setText((String) message);
      pane.addHyperlinkListener(new HyperlinkListener() {

        @Override
        public void hyperlinkUpdate(final HyperlinkEvent _evt) {
          if (_evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            linkActivated(_evt.getURL() == null ? null : _evt.getURL().toString());
          }

        }

      });
      setMessage(pane);
    }
  }

  public static int showDialog(final Component _parent, final String _title, final JOptionPane _p) {
    return CtuluLibSwing.showDialog(_parent, _title, _p);
  }
  
  
}
