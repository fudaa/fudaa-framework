/*
 * @creation 10 mars 2005
 * 
 * @modification $Date: 2007-06-14 11:58:18 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import gnu.trove.TIntArrayList;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.AbstractListModel;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluFileChooserCsvExcel.TypeChoosen;
import org.fudaa.ctulu.table.CtuluTableExportInterface;

/**
 * @author DENIGER
 */
@SuppressWarnings("serial")
public final class CtuluTableExportPanel extends CtuluDialogPanel implements ItemListener {

  private static final char DEFAULT_SEPARATOR = '\t';
  JList cbModel_;
  BuRadioButton cbOnlySelected_;

  final CtuluTableExportTask exportTask;
  JDialog parentDial_;

  public static void setShowColumnsToExport(final BuTable _b, final boolean _show) {
    _b.putClientProperty("TABLE_SHOW_COLUMNS_TO_EXPORT", _show);
  }

  public static boolean isshowColumnToExport(final JTable _b) {
    final Boolean res = (Boolean) _b.getClientProperty("TABLE_SHOW_COLUMNS_TO_EXPORT");
    return res == null ? true : res.booleanValue();
  }

  public static void showExportDialog(final char separator, final JTable _table, final File _dest, final TypeChoosen type,
          final CtuluUI _ui) {
    showExportDialog(separator, _table, _dest, type, _ui, CtuluLibSwing.getFrameAncestor(_ui.getParentComponent()));
  }

  public static void showExportDialog(final JTable _table, final File _dest, final TypeChoosen type, final CtuluUI _ui,
          final Window windowParent) {
    showExportDialog('\t', _table, _dest, type, _ui, windowParent);
  }

  public static void showExportDialog(final char separator, final JTable _table, final File _dest, final TypeChoosen type,
          final CtuluUI _ui, final Window windowParent) {
    if (!isshowColumnToExport(_table)) {
      CtuluTableExportTask task = new CtuluTableExportTask(_table, _ui, type, _dest);
      task.setSeparator(separator);
      task.export(null, null);
      return;
    }
    final CtuluTableExportPanel pn = new CtuluTableExportPanel(_table, _dest, type, _ui);
    pn.setSeparator(separator);
    final CtuluDialog dial = CtuluDialogPanel.createDialog(windowParent, pn);
    pn.setParentDial(dial);
    dial.setTitle(CtuluLib.getS("S�lection des donn�es � exporter"));
    Point isSet = CtuluDialogPreferences.loadComponentLocationAndDimension(dial, BuPreferences.BU, "exportExcelDialog");
    CtuluDialogPreferences.ensureComponentWillBeVisible(dial, dial.getLocation());
    dial.afficheDialogModal(isSet == null);
    CtuluDialogPreferences.saveComponentLocationAndDimension(dial, BuPreferences.BU, "exportExcelDialog");
    BuPreferences.BU.writeIniFile();
  }

  public void setSeparator(char separator) {
    exportTask.setSeparator(separator);
  }

  public static void doExport(final JTable _t, final CtuluUI _ui, final Window _f) {
    doExport(DEFAULT_SEPARATOR, _t, _ui, _f);
  }

  public static void doExport(final char separator, final JTable _t, final CtuluUI _ui, final Window _f) {
    final CtuluFileChooserCsvExcel choose = new CtuluFileChooserCsvExcel(_ui);
    final File f = new CtuluExportTableChooseFile(choose).chooseExportFile();
    if (f != null) {
      CtuluTableExportPanel.showExportDialog(separator, _t, f, choose.getTypeChoosen(), _ui);
    }
  }

  public static void doExport(char separator, final JTable _t, final CtuluUI _ui) {
    doExport(separator, _t, _ui, CtuluLibSwing.getFrameAncestor(_ui.getParentComponent()));
  }

  public static void doExport(final JTable _t, final CtuluUI _ui) {
    doExport('\t', _t, _ui, CtuluLibSwing.getFrameAncestor(_ui.getParentComponent()));
  }

  final TIntArrayList exportableColumns = new TIntArrayList();

  @SuppressWarnings("serial")
  private CtuluTableExportPanel(final JTable _table, final File _dest, final TypeChoosen type, final CtuluUI _ui) {
    super(false);
    setLayout(new BuBorderLayout(5, 5));
    exportTask = new CtuluTableExportTask(_table, _ui, type, _dest);
    cbModel_ = new BuList();
    CtuluTableExportInterface exportInterface = null;
    if (_table.getModel() instanceof CtuluTableExportInterface) {
      exportInterface = (CtuluTableExportInterface) _table.getModel();
    }

    for (int i = 0; i < _table.getColumnCount(); i++) {
      final TableColumn column = _table.getColumnModel().getColumn(i);
      boolean toAdd = true;
      if (exportInterface != null) {
        toAdd = exportInterface.isColumnExportable(column.getModelIndex());
      }
      if (toAdd) {
        exportableColumns.add(i);
      }
    }
    cbModel_.setModel(new AbstractListModel() {
      @Override
      public Object getElementAt(final int _index) {
        return exportTask.getTable().getColumnName(exportableColumns.get(_index));
      }

      @Override
      public int getSize() {
        return exportableColumns.size();
      }
    });
    final ButtonGroup bg = new ButtonGroup();
    final BuRadioButton col = new BuRadioButton(CtuluLib.getS("Exporter les colonnes suivantes:"));
    bg.add(col);
    add(col, BuBorderLayout.NORTH);
    final boolean isCellSelected = !exportTask.getTable().getSelectionModel().isSelectionEmpty();
    add(new BuScrollPane(cbModel_), BuBorderLayout.CENTER);
    cbModel_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    cbModel_.getSelectionModel().setSelectionInterval(0, cbModel_.getModel().getSize() - 1);
    cbModel_.getSelectionModel().setValueIsAdjusting(false);
    cbOnlySelected_ = new BuRadioButton(CtuluLib.getS("Exporter seulement les cellules s�lectionn�es"));
    bg.add(cbOnlySelected_);
    cbOnlySelected_.setEnabled(isCellSelected);
    if (isCellSelected) {
      cbOnlySelected_.addItemListener(this);
    }
    col.setSelected(true);
    add(cbOnlySelected_, BuBorderLayout.SOUTH);
  }

  @Override
  public String getHelpText() {
    String msg = CtuluLib.getS("Vous pouvez s�lectionner les colonnes � exporter");
    if (cbOnlySelected_.isEnabled()) {
      msg += "<br>" + CtuluLib.getS("ou exporter uniquement les cellules s�lectionn�es.");
    } else {
      msg += CtuluLibString.DOT;
    }
    msg += "<br>" + CtuluLib.getS("Par d�faut, toutes les colonnes sont s�lectionn�es pour l'exportation.");
    return "<html><body>" + msg + "</body></html>";
  }

  protected void nothingToExport() {
    exportTask.getUi().warn(CtuluResource.CTULU.getString("Exportation annul�e"), CtuluResource.CTULU.getString("Ancune donn�e n'a �t� s�lectionn�e"),
            false);
  }

  @Override
  public boolean apply() {
    int[] col = null;
    int[] row = null;
    if (cbOnlySelected_.isSelected()) {
      col = exportTask.getTable().getSelectedColumns();
      if (!exportTask.getTable().getColumnSelectionAllowed()) {
        col = new int[exportTask.getTable().getColumnCount()];
        for (int i = 0; i < col.length; i++) {
          col[i] = i;
        }
      }
      row = exportTask.getTable().getSelectedRows();
      if ((col == null || row == null) || (row.length == 0 || col.length == 0)) {
        nothingToExport();
        return true;
      }
    } else {
      final int[] selectedIndices = cbModel_.getSelectedIndices();
      col = new int[selectedIndices.length];
      for (int i = 0; i < selectedIndices.length; i++) {
        col[i] = this.exportableColumns.get(selectedIndices[i]);
      }
      // pas de selection -> pas de fichier
      if (col == null) {
        nothingToExport();
        return true;
      }
      // tout est selectionne -> null
      if (col.length == cbModel_.getModel().getSize()) {
        col = null;
      }
    }
    exportTask.export(col, row);
    return true;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    cbModel_.setEnabled(!cbOnlySelected_.isSelected());
  }

  public JDialog getParentDial() {
    return parentDial_;
  }

  public void setParentDial(final JDialog _parentDial) {
    parentDial_ = _parentDial;
  }
}
