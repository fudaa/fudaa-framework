/*
 *  @creation     13 mai 2003
 *  @modification $Date: 2006-09-19 14:36:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

/**
 * @author deniger
 * @version $Id: CtuluCellButtonEditor.java,v 1.6 2006-09-19 14:36:55 deniger Exp $
 */
public abstract class CtuluCellButtonEditor extends JButton implements TableCellEditor, ActionListener {
  private transient ChangeEvent changeEvent_;

  private CtuluCellDecorator decorator_;

  protected Object value_;

  private boolean doubleClick_;

  public CtuluCellButtonEditor(final CtuluCellDecorator _deco) {
    setOpaque(false);
    setEnabled(true);
    addActionListener(this);
    decorator_ = _deco;
    setBorder(UIManager.getBorder("Label.border"));
    setFont(UIManager.getFont("Table.font"));
    setHorizontalAlignment(0);
  }

  @Override
  public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected, final int _row, final int _column) {
    value_ = _value;
    setValue(_value);
    if (decorator_ != null) {
      decorator_.decore(this, _table, _value, _row, _column);
    }
    return this;
  }

  public void setValue(final Object _o) {
    setText(_o.toString());
  }

  @Override
  public Object getCellEditorValue() {
    return value_;
  }

  @Override
  public boolean isCellEditable(final EventObject _anEvent) {
    if (!doubleClick_) {
      return true;
    }
    if (_anEvent instanceof MouseEvent) {
      return ((MouseEvent) _anEvent).getClickCount() >= 2;
    }
    return true;
  }

  @Override
  public boolean shouldSelectCell(final EventObject _anEvent) {
    return true;
  }

  @Override
  public void cancelCellEditing() {
final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == CellEditorListener.class) {
        if (changeEvent_ == null) {
          changeEvent_ = new ChangeEvent(this);
        }
        ((CellEditorListener) listeners[i + 1]).editingCanceled(changeEvent_);
      }
    }
  }

  @Override
  public void addCellEditorListener(final CellEditorListener _listener) {
    listenerList.add(CellEditorListener.class, _listener);
  }

  @Override
  public void removeCellEditorListener(final CellEditorListener _l) {
    listenerList.remove(CellEditorListener.class, _l);
  }

  @Override
  public boolean stopCellEditing() {
    final  Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == CellEditorListener.class) {
        if (changeEvent_ == null) {
          changeEvent_ = new ChangeEvent(this);
        }
        ((CellEditorListener) listeners[i + 1]).editingStopped(changeEvent_);
      }
    }
    return false;
  }

  protected abstract void doAction();

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    doAction();
    stopCellEditing();
  }

  /**
   *
   */
  public CtuluCellDecorator getDecorator() {
    return decorator_;
  }

  /**
   *
   */
  public void setDecorator(final CtuluCellDecorator _decorator) {
    decorator_ = _decorator;
  }

  /**
   *
   */
  public boolean isDoubleClick() {
    return doubleClick_;
  }

  /**
   *
   */
  public void setDoubleClick(final boolean _b) {
    doubleClick_ = _b;
  }
}
