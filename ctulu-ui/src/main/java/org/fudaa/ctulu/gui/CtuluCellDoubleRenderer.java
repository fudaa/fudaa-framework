/*
 * @creation 7 mars 2006
 * @modification $Date: 2007-03-30 15:35:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;

/**
 * @author fred deniger
 * @version $Id: CtuluCellDoubleRenderer.java,v 1.5 2007-03-30 15:35:20 deniger Exp $
 */
public class CtuluCellDoubleRenderer extends CtuluCellTextRenderer {

  final CtuluNumberFormatI fmt_;

  public CtuluCellDoubleRenderer(final CtuluNumberFormatI _fmt) {
    super();
    fmt_ = _fmt;
/*    setHorizontalTextPosition(SwingConstants.TRIGHT);
    setHorizontalAlignment(SwingConstants.RIGHT);*/

  }

  @Override
  protected void setValue(final Object _value) {
    if (_value == null) {
      setText(CtuluLibString.EMPTY_STRING);
    } else if (fmt_ == null) {
      setText(_value.toString());
    } else {
      setText(fmt_.format(((Double) _value).doubleValue()));
    }
  }
}
