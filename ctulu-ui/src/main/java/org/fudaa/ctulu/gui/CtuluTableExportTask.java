/*
 GPL 2
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPreferences;
import gnu.trove.TIntArrayList;
import java.awt.Cursor;
import java.io.File;
import java.text.DecimalFormat;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import org.fudaa.ctulu.CsvWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.table.CtuluTableCsvWriter;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluTableExportInterface;
import org.fudaa.ctulu.table.CtuluTableModelDefault;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.ctulu.table.CtuluTableXlsxWriter;
import org.jdesktop.swingx.JXTreeTable;

/**
 *
 * @author Frederic Deniger
 */
public class CtuluTableExportTask {

  private final JTable table_;
  private final CtuluUI ui_;
  private final CtuluFileChooserCsvExcel.TypeChoosen choosenType_;
  private final File dest_;
  private JDialog parentDial_;
  private char separator = '\t';

  public CtuluTableExportTask(JTable table_, CtuluUI ui_, CtuluFileChooserCsvExcel.TypeChoosen choosenType_, File dest_) {
    this.table_ = table_;
    this.ui_ = ui_;
    this.choosenType_ = choosenType_;
    this.dest_ = dest_;
  }

  public JTable getTable() {
    return table_;
  }

  public CtuluUI getUi() {
    return ui_;
  }

  public CtuluFileChooserCsvExcel.TypeChoosen getChoosenType() {
    return choosenType_;
  }

  public File getDest() {
    return dest_;
  }

  public JDialog getParentDial() {
    return parentDial_;
  }

  public char getSeparator() {
    return separator;
  }

  public void setParentDial(JDialog parentDial_) {
    this.parentDial_ = parentDial_;
  }

  public void setSeparator(char separator) {
    this.separator = separator;
  }

  private CtuluTableModelInterface createDefaultModele(final int[] colToWrite, final int[] rowToWrite) {
    if (table_ instanceof JXTreeTable) {
      return new CtuluTreeTableModelDefault((JXTreeTable) table_, colToWrite,
              rowToWrite);
    }
    return new CtuluTableModelDefault(table_, colToWrite, rowToWrite);
  }

  public void export(int[] col, int[] row) {
    final BuGlassPaneStop s = new BuGlassPaneStop();
    //on enleve les colonnes non exportables
    if (table_.getModel() instanceof CtuluTableExportInterface) {
      CtuluTableExportInterface exportInterface = (CtuluTableExportInterface) table_.getModel();
      final TIntArrayList toExport = new TIntArrayList();
      if (col == null) {
        for (int i = 0; i < table_.getColumnCount(); i++) {
          final TableColumn column = table_.getColumnModel().getColumn(i);
          boolean toAdd = exportInterface.isColumnExportable(column.getModelIndex());
          if (toAdd) {
            toExport.add(i);
          }
        }
      } else {
        for (int i = 0; i < col.length; i++) {
          final TableColumn column = table_.getColumnModel().getColumn(col[i]);
          boolean toAdd = exportInterface.isColumnExportable(column.getModelIndex());
          if (toAdd) {
            toExport.add(col[i]);
          }
        }
      }
      col = toExport.toNativeArray();
    }
    final int[] colToWrite = col;
    final int[] rowToWrite = row;
    if (parentDial_ != null) {
      parentDial_.setGlassPane(s);
      parentDial_.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }
    s.setVisible(true);
    final CtuluTaskDelegate task = ui_.createTask(CtuluLib.getS("Export format texte"));
    task.start(new Runnable() {
      @Override
      public void run() {
        final CtuluTableModelInterface m = createDefaultModele(colToWrite, rowToWrite);
        boolean ok = true;
        try {
          if (CtuluFileChooserCsvExcel.TypeChoosen.XLS.equals(choosenType_)) {
            final CtuluTableExcelWriter w = new CtuluTableExcelWriter(m, dest_);
            w.write(task.getStateReceiver());
          } else if (CtuluFileChooserCsvExcel.TypeChoosen.XLSX.equals(choosenType_)) {
            final CtuluTableXlsxWriter w = new CtuluTableXlsxWriter(m, dest_);
            w.write(task.getStateReceiver());
          } else {
            int nbDigits = BuPreferences.BU.getIntegerProperty(CtuluTablePreferencesComponent.COORDS_EXPORT_NB_DIGITS, -1);
            DecimalFormat fmt;
            if (nbDigits == -1) {
              fmt = null;
            } else {
              fmt = CtuluLib.getDecimalFormat();
              fmt.setMaximumFractionDigits(nbDigits);
            }
            final CtuluTableCsvWriter w = new CtuluTableCsvWriter(new CsvWriter(dest_), m, separator, fmt);
            w.write(task.getStateReceiver());
          }
        } catch (final Exception e) {
          ok = false;
          ui_.error(dest_.getName(), e.getLocalizedMessage(), false);
        }
        if (ok) {
          BuLib.invokeLater(new Runnable() {
            @Override
            public void run() {
              s.setVisible(false);
              if (parentDial_ != null) {
                parentDial_.getRootPane().remove(s);
                parentDial_.setCursor(Cursor.getDefaultCursor());
              }
              ui_.message(CtuluResource.CTULU.getString("Export termin�"), CtuluResource.CTULU.getString("Donn�es export�es dans le fichier\n {0}",
                      dest_.
                      getAbsolutePath()), true);
            }
          });
        }
      }
    });
  }
}
