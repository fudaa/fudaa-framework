/*
 *  @creation     30 mai 2003
 *  @modification $Date: 2006-07-13 13:34:40 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.gui;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
/**
 * @author fred deniger
 * @version $Id: CtuluCellRendererRowHeader.java,v 1.4 2006-07-13 13:34:40 deniger Exp $
 *
 */
public class CtuluCellRendererRowHeader extends CtuluCellTextRenderer {
  public CtuluCellRendererRowHeader() {
    super();
    setOpaque(true);
    setBorder(UIManager.getBorder("TableHeader.cellBorder"));
    setHorizontalAlignment(SwingConstants.CENTER);
    setForeground(UIManager.getColor("TableHeader.foreground"));
    setBackground(UIManager.getColor("TableHeader.background"));
    setFont(UIManager.getFont("TableHeader.font"));
  }
  @Override
  protected void setValue(final Object _value) {
    setText((String)_value);
  }
  @Override
  public Component getTableCellRendererComponent(
    final JTable _table,
    final Object _value,
    final boolean _isSelected,
    final boolean _hasFocus,
    final int _row,
    final int _column) {
    setValue(_value);
    return this;
  }
}
