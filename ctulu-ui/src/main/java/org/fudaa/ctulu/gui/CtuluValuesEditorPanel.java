/*
 * @creation 30 sept. 2005
 * 
 * @modification $Date: 2007-01-10 08:58:49 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * @author Fred Deniger
 * @version $Id: CtuluValuesEditorPanel.java,v 1.7 2007-01-10 08:58:49 deniger Exp $
 */
public class CtuluValuesEditorPanel extends CtuluDialogPanel {

  protected CtuluValuesCommonEditorPanel common_;

  CtuluDialogPanel current_;

  protected CtuluValuesMultiEditorPanel multi_;

  JDialog parent_;

  BuRadioButton rdCommon_;
  protected JPanel pnNorth_;

  public CtuluValuesEditorPanel(final CtuluValuesParameters _params, final CtuluCommandContainer _cmd) {
    this(new CtuluValuesMultiEditorPanel(_params), _cmd);
  }

  /**
   * @param _names les noms (non null)
   * @param _idx les indices a modifier.
   * @param _editors les editeurs
   * @param _values les valeurs (meme taille que les editeurs)
   */
  public CtuluValuesEditorPanel(final Object[] _names, final int[] _idx, final CtuluValueEditorI[] _editors, final CtuluCollection[] _values,
      final CtuluCommandContainer _cmd) {
    this(new CtuluValuesMultiEditorPanel(_names, _idx, _editors, _values), _cmd);
  }

  /**
   * Constructeur, avec le panneau des valeurs d�ja construit.
   * 
   * @param _pn Le panneau des valeurs.
   * @param _cmd Le manager undo/redo
   */
  public CtuluValuesEditorPanel(final CtuluValuesMultiEditorPanel _pn, final CtuluCommandContainer _cmd) {
    multi_ = _pn;
    init(_cmd);
  }

  private void init(final CtuluCommandContainer _cmd) {
    multi_.setCmd(_cmd);
    current_ = multi_;
    setLayout(new BuBorderLayout(4, 4));
    multi_.setBorder(BorderFactory.createTitledBorder(CtuluLib.getS("Editeur")));
    add(multi_, BuBorderLayout.CENTER);
    final BuPanel pn = new BuPanel(new BuHorizontalLayout(10));
    final String txt = "<br><u>" + CtuluLib.getS("Avertissement") + "</u>: "
        + CtuluLib.getS("Les modifications ne sont pas actualis�es lors du changement du mode d'�dition.");
    final BuRadioButton rd = new BuRadioButton(CtuluLib.getS("Table des valeurs"));
    rd.setToolTipText("<html><body>" + CtuluLib.getS("Edition des valeurs sous forme de table.") + txt + "</body></html>");
    rdCommon_ = new BuRadioButton(CtuluLib.getS("Valeurs agr�g�es"));
    rdCommon_.setToolTipText("<html><body>" + CtuluLib.getS("Edition des valeurs sous forme agr�g�e") + "<br>"
        + CtuluLib.getS("Ce mode permet d'effectuer des modifications globales et la variable 'old' peut �tre utilis�e.") + txt + "</body></html>");

    final ButtonGroup group = new ButtonGroup();
    group.add(rd);
    group.add(rdCommon_);
    rd.setSelected(true);
    pn.add(rd);
    pn.add(rdCommon_);
    rdCommon_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        changeEditor();
      }

    });
    pn.setBorder(BorderFactory.createTitledBorder(CtuluLib.getS("Mode d'�dition")));
    pnNorth_ = new JPanel();
    pnNorth_.setLayout(new BuVerticalLayout());
    pnNorth_.add(pn);
    add(pnNorth_, BuBorderLayout.NORTH);
  }

  public void displayOnlyCommonPanel() {
    rdCommon_.setSelected(true);
    pnNorth_.setVisible(false);

  }

  protected void changeEditor() {
    final CtuluDialogPanel panelCurrent = current_;
    if (rdCommon_.isSelected()) {
      if (common_ == null) {
        common_ = multi_.createCommon();
      }
      current_ = common_;
    } else {
      current_ = multi_;
    }
    remove(panelCurrent);
    add(current_, BuBorderLayout.CENTER);
    revalidate();
    if (parent_ != null) {
      parent_.pack();
    }
    repaint();

  }

  protected final JDialog getParentDialog() {
    return parent_;
  }

  protected final void setParentDialog(final JDialog _parent) {
    parent_ = _parent;
  }

  @Override
  public boolean apply() {
    return current_.apply();
  }

  @Override
  public boolean isDataValid() {
    return current_.isDataValid();
  }

}
