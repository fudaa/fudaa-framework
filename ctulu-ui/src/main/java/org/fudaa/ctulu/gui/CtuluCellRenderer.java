/*
 *  @creation     15 mai 2003
 *  @modification $Date: 2006-11-24 09:30:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuBorders;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

/**
 * @author deniger
 * @version $Id: CtuluCellRenderer.java,v 1.4 2006-11-24 09:30:26 deniger Exp $
 */
public interface CtuluCellRenderer extends TableCellRenderer, ListCellRenderer {
  Border BORDER_NO_FOCUS = BuBorders.EMPTY1111;
  Border BORDER_FOCUS = UIManager.getBorder("Table.focusCellHighlightBorder");

  void setDecorator(CtuluCellDecorator _c);
}
