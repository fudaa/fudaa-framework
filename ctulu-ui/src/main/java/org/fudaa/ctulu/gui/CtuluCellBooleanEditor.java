/*
 *  @creation     13 mai 2003
 *  @modification $Date: 2007-03-27 16:09:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.awt.Rectangle;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author deniger
 * @version $Id: CtuluCellBooleanEditor.java,v 1.6 2007-03-27 16:09:42 deniger Exp $
 */
public class CtuluCellBooleanEditor extends DefaultCellEditor {
  public CtuluCellBooleanEditor() {
    super(new JCheckBox() {
      // un bogue avec le lnk JGoodies look XP
      @Override
      public void validate() {}

      @Override
      public void revalidate() {}

      @Override
      public void repaint(final long _tm, final int _x, final int _y, final int _width, final int _height) {}

      @Override
      public void repaint(final Rectangle _r) {}
    });
    final JCheckBox ch = (JCheckBox) editorComponent;
    ch.setText(CtuluLibString.EMPTY_STRING);
    ch.revalidate();
    delegate = new EditorDelegate() {
      @Override
      public void setValue(final Object _value) {
        updateCheckBox(ch, _value);
      }

      @Override
      public Object getCellEditorValue() {
        return getObjectValue(ch.isSelected());
      }
    };
  }

  protected void updateCheckBox(final JCheckBox _ch, final Object _value) {
    _ch.setSelected(getBooleanValue(_value));
  }

  public boolean getBooleanValue(final Object _o) {
    return getDefaultBooleanValue(_o);
  }

  public static boolean getDefaultBooleanValue(final Object _o) {
    if (_o == null) return false;
    if (_o instanceof Boolean) {
      return ((Boolean) _o).booleanValue();
    } else if (_o instanceof String) {
      return _o.equals("true");
    }
    return false;
  }

  public Object getObjectValue(final boolean _o) {
    return _o ? Boolean.TRUE : Boolean.FALSE;
  }
}