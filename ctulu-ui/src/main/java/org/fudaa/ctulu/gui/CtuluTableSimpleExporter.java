/*
 * @creation 10 mars 2005
 * 
 * @modification $Date: 2007-06-14 11:58:18 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuPreferences;
import java.awt.Window;
import java.io.File;
import java.text.DecimalFormat;
import org.fudaa.ctulu.CsvWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluFileChooserCsvExcel.TypeChoosen;
import org.fudaa.ctulu.table.CtuluTableCsvWriter;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.ctulu.table.CtuluTableXlsxWriter;

/**
 * @author DENIGER
 */
@SuppressWarnings("serial")
public final class CtuluTableSimpleExporter {

  private static final char DEFAULT_SEPARATOR = '\t';
  File dest_;
  CtuluFileChooserCsvExcel.TypeChoosen typeChoosen;
  final CtuluUI ui_;
  char separator = '\t';
  private CtuluTableModelInterface model;

  public static void showExportDialog(final char separator, final CtuluTableModelInterface _table, final File _dest,
          final CtuluFileChooserCsvExcel.TypeChoosen _xsl,
          final CtuluUI _ui) {
    final CtuluTableSimpleExporter pn = new CtuluTableSimpleExporter(_dest, _xsl, _ui, _table);
    pn.separator = separator;
    pn.apply();
  }

  public static void doExport(final CtuluTableModelInterface _t, final CtuluUI _ui, final Window _f) {
    doExport(DEFAULT_SEPARATOR, _t, _ui, _f);
  }

  public static void doExport(final char separator, final CtuluTableModelInterface _t, final CtuluUI _ui, final Window _f) {
    final CtuluFileChooserCsvExcel choose = CtuluFileChooserCsvExcel.createSaveFileChooser(_ui);
    final File f = choose.getDestFile();
    if (f == null) {
      return;
    }
    CtuluTableSimpleExporter.showExportDialog(separator, _t, f, choose.getTypeChoosen(), _ui);
  }

  public static void doExport(char separator, final CtuluTableModelInterface _t, final CtuluUI _ui) {
    doExport(separator, _t, _ui, CtuluLibSwing.getFrameAncestor(_ui.getParentComponent()));
  }

  public static void doExport(final CtuluTableModelInterface _t, final CtuluUI _ui) {
    doExport('\t', _t, _ui, CtuluLibSwing.getFrameAncestor(_ui.getParentComponent()));
  }

  public CtuluTableSimpleExporter(File dest_, CtuluFileChooserCsvExcel.TypeChoosen _typeChoosen, CtuluUI ui_, CtuluTableModelInterface model) {
    this.dest_ = dest_;
    this.typeChoosen = _typeChoosen;
    this.ui_ = ui_;
    this.model = model;
  }

  public void apply() {

    final CtuluTaskDelegate task = ui_.createTask(CtuluLib.getS("Export format texte"));
    task.start(new Runnable() {
      @Override
      public void run() {
        final CtuluTableModelInterface m = model;
        boolean ok = true;
        try {
          if (TypeChoosen.XLSX.equals(typeChoosen)) {
            final CtuluTableXlsxWriter w = new CtuluTableXlsxWriter(m, dest_);
            w.write(task.getStateReceiver());
          } else if (TypeChoosen.XLS.equals(typeChoosen)) {
            final CtuluTableExcelWriter w = new CtuluTableExcelWriter(m, dest_);
            w.write(task.getStateReceiver());
          } else {
            int nbDigits = BuPreferences.BU.getIntegerProperty(CtuluTablePreferencesComponent.COORDS_EXPORT_NB_DIGITS, -1);
            DecimalFormat fmt;
            if (nbDigits == -1) {
              fmt = null;
            } else {
              fmt = CtuluLib.getDecimalFormat();
              fmt.setMaximumFractionDigits(nbDigits);
            }
            final CtuluTableCsvWriter w = new CtuluTableCsvWriter(new CsvWriter(dest_), m, separator, fmt);
            w.write(task.getStateReceiver());
          }
        } catch (final Exception e) {
          ok = false;
          ui_.error(dest_.getName(), e.getMessage(), false);
        }
        if (ok) {
          BuLib.invokeLater(new Runnable() {
            @Override
            public void run() {
              ui_.message(CtuluResource.CTULU.getString("Export termin�"), CtuluResource.CTULU.getString("Donn�es export�es dans le fichier\n {0}",
                      dest_.
                      getAbsolutePath()), true);
            }
          });
        }

      }
    });

  }
}
