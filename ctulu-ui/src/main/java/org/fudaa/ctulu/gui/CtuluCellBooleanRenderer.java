/*
 *  @creation     13 mai 2003
 *  @modification $Date: 2007-03-27 16:09:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuCheckBox;
import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.DefaultButtonModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.UIManager;

/**
 * @author deniger
 * @version $Id: CtuluCellBooleanRenderer.java,v 1.10 2007-03-27 16:09:42 deniger Exp $
 */
public class CtuluCellBooleanRenderer extends BuCheckBox implements CtuluCellRenderer {
  private CtuluCellDecorator decorator_;

  public CtuluCellBooleanRenderer() {
    setModel(new DefaultButtonModel());
  }

  public CtuluCellBooleanRenderer(final CtuluCellDecorator _deco) {
    setDecorator(_deco);
  }

  @Override
  public boolean isOpaque() {
    return true;
  }

  @Override
  public void validate() {}

  @Override
  public void revalidate() {}

  @Override
  public void repaint(final long _tm, final int _x, final int _y, final int _width, final int _height) {}

  @Override
  public void repaint(final Rectangle _r) {}

  public static void updateRenderer(final JComponent _target, final JTable _table, final boolean _isSelected,
      final boolean _hasFocus, final int _row, final int _column) {
    if (_isSelected) {
      _target.setForeground(_table.getSelectionForeground());
      _target.setBackground(_table.getSelectionBackground());
    } else {
      _target.setForeground(_table.getForeground());
      _target.setBackground(_table.getBackground());
    }
    if (_hasFocus) {
      _target.setBorder(BORDER_FOCUS);
      if (_table.isCellEditable(_row, _column)) {
        _target.setForeground(UIManager.getColor("Table.focusCellForeground"));
        _target.setBackground(UIManager.getColor("Table.focusCellBackground"));
      }
    } else {
      _target.setBorder(BORDER_NO_FOCUS);
    }
    _target.setFont(_table.getFont());
  }

  @Override
  public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
      final boolean _hasFocus, final int _row, final int _column) {
    updateRenderer(this, _table, _isSelected, _hasFocus, _row, _column);
    setValue(_value);
    if (decorator_ != null) {
      decorator_.decore(this, _table, _value, _row, _column);
    }
    return this;
  }

  public static void updateListCellRenderer(final JComponent _target, final JList _list, final int _index,
      final boolean _isSelected, final boolean _cellHasFocus) {
    _target.setComponentOrientation(_list.getComponentOrientation());
    if (_isSelected) {
      _target.setBackground(_list.getSelectionBackground());
      _target.setForeground(_list.getSelectionForeground());
    } else {
      _target.setBackground(_list.getBackground());
      _target.setForeground(_list.getForeground());
    }
    _target.setEnabled(_list.isEnabled());
    _target.setFont(_list.getFont());
    _target.setBorder(_cellHasFocus ? UIManager.getBorder("List.focusCellHighlightBorder") : BORDER_NO_FOCUS);
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
      final boolean _isSelected, final boolean _cellHasFocus) {
    updateListCellRenderer(this, _list, _index, _isSelected, _cellHasFocus);
    setValue(_value);
    if (decorator_ != null) {
      decorator_.decore(this, _list, _value, _index);
    }
    return this;
  }

  public void setValue(final Object _value) {
    setSelected(CtuluCellBooleanEditor.getDefaultBooleanValue(_value));
  }

  @Override
  public final void setDecorator(final CtuluCellDecorator _c) {
    decorator_ = _c;
  }
}
