/*
 *  @creation     31 ao�t 2005
 *  @modification $Date: 2007-06-14 11:58:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.io.File;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;

/**
 * @author Fred Deniger
 * @version $Id: CtuluFileChooserTestWritable.java,v 1.5 2007-06-14 11:58:18 deniger Exp $
 */
public class CtuluFileChooserTestWritable implements CtuluFileChooserFileTester {

  final Component parentComponent;
  boolean appendExtension_;
  boolean appendStrictExt_;
  String defaultExt_;

  /**
   * @param _impl
   */
  public CtuluFileChooserTestWritable(final CtuluUI _impl) {
    super();
    parentComponent = _impl == null ? null : _impl.getParentComponent();
  }

  public CtuluFileChooserTestWritable(final Component _impl) {
    super();
    parentComponent = _impl;
  }

  public File getDestFile(final File _initFile, CtuluFileChooser _chooser) {
    if (_initFile == null) {
      return null;
    }
    String ext = defaultExt_;
    if (ext == null) {
      FileFilter filter = _chooser.getFileFilter();
      if (filter instanceof BuFileFilter) {
        ext = ((BuFileFilter) filter).getFirstExt();
      }
    }
    if (appendExtension_ && ext != null && CtuluLibFile.getExtension(_initFile.getName()) == null) {
      return CtuluLibFile.appendExtensionIfNeeded(_initFile, ext);

    }
    else if (appendStrictExt_ && ext!=null) {
      return CtuluLibFile.changeExtension(_initFile, ext);
    }
    return _initFile;
  }

  public final boolean isFileOk(final File _selectedFile) {
    return false;
  }

  @Override
  public boolean isFileOk(final File _selectedFile, CtuluFileChooser _fileChooser) {
    final File destFile = getDestFile(_selectedFile, _fileChooser);
    final String err = CtuluLibFile.canWrite(destFile);
    if (err != null) {
      CtuluLibDialog.showError(parentComponent, BuResource.BU.getString("Fichier"), err);
      return false;
    }

    if (destFile.exists()) {
      return CtuluLibDialog.confirmeOverwriteFile(parentComponent, destFile.getName());
    }
    return true;
  }

  public boolean isAppendExtension() {
    return appendExtension_;
  }

  /**
   * Le fichier doit se terminer par l'extension du filtre choisi, sinon cette extension est ajout�e m�me si
   * le nom du fichier contient un '.'
   * @param _strictExt True Ajout de l'extension
   */
  public void setAppendStrictExtension(boolean _strictExt) {
    setAppendStrictExtension(_strictExt, null);
  }
  
  /**
   * Le fichier doit se terminer par l'extension par d�faut, sinon cette extension est ajout�e m�me si
   * le nom du fichier contient un '.'
   * @param _strictExt True Ajout de l'extension
   * @param _defaultExt L'extension par d�faut
   */
  public void setAppendStrictExtension(boolean _strictExt, String _defaultExt) {
    appendStrictExt_ = _strictExt;
    defaultExt_ = _defaultExt;
    
    if (appendStrictExt_) appendExtension_=false;
  }
  
  public void setAppendExtension(boolean _appendExtension) {
    setAppendExtension(_appendExtension, null);
  }

  public void setAppendExtension(boolean _appendExtension, String _defaultExt) {
    appendExtension_ = _appendExtension;
    defaultExt_ = _defaultExt;
    
    if (appendExtension_) appendStrictExt_=false;
  }
}
