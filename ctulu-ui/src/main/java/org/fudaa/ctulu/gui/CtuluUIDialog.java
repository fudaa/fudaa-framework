/*
 * @creation 24 juil. 06
 * @modification $Date: 2007-02-02 11:20:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.memoire.bu.BuProgressBar;
import java.awt.Component;
import org.fudaa.ctulu.CtuluProgressionBarAdapter;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluTaskOperationDefault;
import org.fudaa.ctulu.CtuluUIAbstract;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author fred deniger
 * @version $Id: CtuluUIDialog.java,v 1.2 2007-02-02 11:20:11 deniger Exp $
 */
public class CtuluUIDialog extends CtuluUIAbstract {

  final Component dialog_;

  CtuluProgressionBarAdapter task_;
  BuProgressBar bar_ = new BuProgressBar();

  @Override
  public void clearMainProgression() {
    if (task_ != null) {
      task_.reset();
    }

  }

  @Override
  public CtuluTaskDelegate createTask(final String _name) {
    return new CtuluTaskOperationDefault(_name) {
      @Override
      public ProgressionInterface getMainStateReceiver() {
        return task_;
      }

      @Override
      public ProgressionInterface getStateReceiver() {
        return task_;
      }
    };
  }

  @Override
  public ProgressionInterface getMainProgression() {
    if (task_ == null) {
      task_ = new CtuluProgressionBarAdapter(bar_);
    }
    return task_;
  }

  public CtuluUIDialog(final Component _dialog) {
    super();
    dialog_ = _dialog;
  }

  @Override
  public void error(final String _titre, final String _msg, final boolean _tempo) {
    CtuluLibDialog.showError(dialog_, _titre, _msg);

  }

  @Override
  public Component getParentComponent() {
    return dialog_;
  }

  @Override
  public void message(final String _titre, final String _msg, final boolean _tempo) {
    CtuluLibDialog.showMessage(dialog_, _titre, _msg);

  }

  @Override
  public boolean question(final String _titre, final String _text) {
    return CtuluLibDialog.showConfirmation(dialog_, _titre, _text);
  }

  @Override
  public void warn(final String _titre, final String _msg, final boolean _tempo) {
    CtuluLibDialog.showWarn(dialog_, _titre, _msg);

  }

  public BuProgressBar getBar() {
    return bar_;
  }

}
