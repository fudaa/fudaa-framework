/*
 * @creation 31 janv. 07
 * @modification $Date: 2007-02-02 11:20:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Shape;
import javax.swing.border.EtchedBorder;

/**
 * @author fred deniger
 * @version $Id: CtuluRoundEtchBorder.java,v 1.2 2007-02-02 11:20:11 deniger Exp $
 */
public class CtuluRoundEtchBorder extends EtchedBorder {

  private int round_ = 10;

  public CtuluRoundEtchBorder(final int _round) {
    super();
    round_ = _round;
  }

  @Override
  public Insets getBorderInsets(final Component _c) {
    final Insets res = super.getBorderInsets(_c);
    enlargeInsets(res);
    return res;
  }

  @Override
  public Insets getBorderInsets(final Component _c, final Insets _insets) {
    final Insets res = super.getBorderInsets(_c, _insets);
    enlargeInsets(res);
    return res;
  }

  private void enlargeInsets(final Insets _res) {
    _res.bottom += 2;
    _res.top += 2;
    _res.left += 2;
    _res.right += 2;
  }

  @Override
  public void paintBorder(final Component _c, final Graphics _g, int _x, int _y, final int _width, final int _height) {
    final int w = _width;
    final int h = _height;

    _g.translate(_x, _y);
    final Shape old = _g.getClip();
    _g.setClip(0, 0, _width, _height / 2);

    _g.setColor(etchType == LOWERED ? getHighlightColor(_c) : getShadowColor(_c));
    _g.drawRoundRect(1, 1, w - 4, h - 4, round_, round_);
    _g.setClip(old);
    _g.setColor(etchType == LOWERED ? getShadowColor(_c) : getHighlightColor(_c));
    _g.drawRoundRect(0, 0, w - 2, h - 2, round_, round_);
    /*
     * int arc=round_/2; _g.drawLine(1, h - 3-arc, 1, 1+arc); _g.drawLine(1+arc, 1, w - 3-arc, 1); _g.drawLine(arc, h -
     * 1, w - 1-arc, h - 1); _g.drawLine(w - 1, h - 1-arc, w - 1, arc);
     */
    _g.translate(-_x, -_y);
  }

}
