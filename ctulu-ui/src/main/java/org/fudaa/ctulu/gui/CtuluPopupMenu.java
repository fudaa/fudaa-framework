/**
 * @modification $Date: 2007-05-04 13:43:23 $
 * @statut unstable
 * @file BuPopupMenu.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */

package org.fudaa.ctulu.gui;

import com.memoire.bu.*;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import javax.swing.*;

/**
 * A popup menu which follows action events and has simplified adding methods.
 */

public class CtuluPopupMenu extends JPopupMenu {

  public CtuluPopupMenu() {}

  /**
   * @param _s titre du menu.
   */
  public CtuluPopupMenu(final String _s) {
    super(_s);
  }

  /**
   * @param _text le texte du separateur
   * @return le separateur ajoute.
   */
  public BuSeparator addSeparator(final String _text) {
    final BuSeparator r = new BuSeparator(_text);
    this.add(r);
    return r;
  }

  /**
   * Ajoute un sous-menu.
   * 
   * @param _m le sous-menu a ajouter
   * @param _enabled true si active
   */
  public void addSubMenu(final JMenu _m, final boolean _enabled) {
    _m.setEnabled(_enabled);
    this.add(_m);
  }

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _enabled true si activee
   * @param _checked true si cochee
   * @param _l le listener
   * @return la checkbox ajoutee
   */
  public BuCheckBoxMenuItem addCheckBox(final String _s, final String _cmd, final boolean _enabled,
      final boolean _checked, final ActionListener _l) {
    final BuCheckBoxMenuItem r = addCheckBox(_s, _cmd, _enabled, _checked);
    if (_l != null) {
      r.addActionListener(_l);
    }
    return r;

  }

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _enabled true si activee
   * @param _checked true si cochee
   * @return la checkbox ajoutee
   */
  public BuCheckBoxMenuItem addCheckBox(final String _s, final String _cmd, final boolean _enabled,
      final boolean _checked) {
    return addCheckBox(_s, _cmd, null, _enabled, _checked);
  }

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _enabled true si activee
   * @param _checked true si cochee
   * @param _icon l'icone
   * @return la checkbox ajoutee
   */
  public BuCheckBoxMenuItem addCheckBox(final String _s, final String _cmd, final Icon _icon, final boolean _enabled,
      final boolean _checked) {
    Icon icon = _icon;
    if (_icon == null) {
      icon = BuResource.BU.loadMenuCommandIcon(_cmd);
    }
    if ((_icon instanceof BuIcon) && ((BuIcon) _icon).isDefault()) {
      icon = null;
    }

    final BuCheckBoxMenuItem r = new BuCheckBoxMenuItem();
    r.setText(_s);
    r.setName("cbmi" + _cmd);
    r.setActionCommand(_cmd);
    if (icon instanceof BuIcon) {
      r.setIcon((BuIcon) icon);
    } else {
      r.setIcon(icon);
    }
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.setEnabled(_enabled);
    r.setSelected(_checked);
    this.add(r);
    return r;
  }

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _enabled true si active
   * @param _checked true si coche
   * @return le radio bouton ajoute
   */
  public BuRadioButtonMenuItem addRadioButton(final String _s, final String _cmd, final boolean _enabled,
      final boolean _checked) {
    return addRadioButton(_s, _cmd, null, _enabled, _checked);
  }

  private ButtonGroup group_;

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _icon l'icone
   * @param _enabled true si active
   * @param _checked true si coche
   * @return le radio bouton ajoute
   */
  public BuRadioButtonMenuItem addRadioButton(final String _s, final String _cmd, final Icon _icon,
      final boolean _enabled, final boolean _checked) {
    if (group_ == null) {
      group_ = new ButtonGroup();
    }
    Icon ic = _icon;
    if (ic == null) {
      ic = BuResource.BU.loadMenuCommandIcon(_cmd);
    }
    if ((ic instanceof BuIcon) && ((BuIcon) ic).isDefault()) {
      ic = null;
    }

    final BuRadioButtonMenuItem r = new BuRadioButtonMenuItem();
    r.setText(_s);
    r.setName("rbmi" + _cmd);
    r.setActionCommand(_cmd);
    if (ic instanceof BuIcon) {
      r.setIcon((BuIcon) ic);
    } else {
      r.setIcon(ic);
    }
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.setEnabled(_enabled);
    r.setSelected(_checked);
    group_.add(r);
    this.add(r);
    return r;
  }

  // MenuItem

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _enabled true si active
   * @return l'iteme ajoute
   */
  public BuMenuItem addMenuItem(final String _s, final String _cmd, final boolean _enabled) {
    return addMenuItem(_s, _cmd, null, _enabled, 0);
  }

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _enabled true si active
   * @param _l le listener
   * @return le menu item ajoute
   */
  public BuMenuItem addMenuItem(final String _s, final String _cmd, final boolean _enabled, final ActionListener _l) {
    final BuMenuItem i = addMenuItem(_s, _cmd, null, _enabled, 0);
    if (_l != null) {
      i.addActionListener(_l);
    }
    return i;
  }

  /**
   * @param _s le texte du menu
   * @param _cmd la commande
   * @param _icon l'icone
   * @param _enabled true si active
   * @return le menu item ajoute
   */
  public BuMenuItem addMenuItem(final String _s, final String _cmd, final Icon _icon, final boolean _enabled) {
    return addMenuItem(_s, _cmd, _icon, _enabled, 0);
  }

  /**
   * @param _s le texte du menu
   * @param _cmd la commande
   * @param _icon l'icone
   * @param _enabled true si active
   * @param _l le listener
   * @return le menu item ajoute
   */
  public BuMenuItem addMenuItem(final String _s, final String _cmd, final Icon _icon, final boolean _enabled,
      final ActionListener _l) {
    final BuMenuItem r = addMenuItem(_s, _cmd, _icon, _enabled, 0);
    if (_l != null) {
      r.addActionListener(_l);
    }
    return r;
  }

  /**
   * @param _s le texte
   * @param _cmd la commande
   * @param _icon l'icone
   * @param _enabled true si active
   * @param _key keystroke l'accelerateur
   * @return le menu item ajoute
   */
  public BuMenuItem addMenuItem(final String _s, final String _cmd, final Icon _icon, final boolean _enabled,
      final int _key) {
    Icon ic = _icon;
    if (ic == null) {
      ic = BuResource.BU.loadMenuCommandIcon(_cmd);
    }
    if ((ic instanceof BuIcon) && ((BuIcon) ic).isDefault()) {
      ic = null;
    }

    final BuMenuItem r = new BuMenuItem();
    r.setName("mi" + _cmd);
    r.setActionCommand(_cmd);
    r.setText(_s);
    if (ic instanceof BuIcon) {
      r.setIcon((BuIcon) ic);
    } else {
      r.setIcon(ic);
    }
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.setEnabled(_enabled);
    if (_key != 0) {
      if ((_key >= KeyEvent.VK_F1) && (_key <= KeyEvent.VK_F12)) {
        r.setAccelerator(KeyStroke.getKeyStroke(_key,
            InputEvent.ALT_MASK));
      } else {
        r.setAccelerator(KeyStroke.getKeyStroke(_key, InputEvent.CTRL_MASK));
      }
    }
    this.add(r);
    return r;
  }

  // Mnemonics (copied fromBuMenu)

  /**
   * Parcourt les items pour mettre a jour les mnemonics.
   */
  public void computeMnemonics() {
    Component[] c = null;
    Hashtable t = null;

    try {
      c = getComponents();
    } /* Menu */
    catch (final Exception ex) {} // swing 1.03
    if (c == null) {
      return;
    }
    for (int i = 0; i < c.length; i++) {
      if (c[i] instanceof JMenuItem) {
        final JMenuItem mi = (JMenuItem) c[i];
        int mn = mi.getMnemonic();
        if (mn <= 0) {
          final String tx = mi.getText() == null ? null : BuLib.candidateMnemonics(mi.getText());
          if (tx != null) {
            for (int j = 0; j < tx.length(); j++) {
              mn = tx.charAt(j);
              if (t == null) {
                t = new Hashtable();
              }
              if (t.get(new Integer(mn)) == null) {
                t.put(new Integer(mn), mi);
                mi.setMnemonic(mn); 
                break;
              }
            }
          }
        }
        if (mi instanceof BuMenu) {
          ((BuMenu) mi).computeMnemonics();
        }
      }
    }
  }
}