package org.fudaa.ctulu.gui;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;

/**
 * Une classe d'utilitaires inclassables.
 *
 * @version $Revision: 1.5 $ $Date: 2007-06-28 09:24:23 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public final class CtuluLibDialog {

  private CtuluLibDialog() {
  }

  /**
   * Affiche une message d'avertissement contenant les lignes de
   * <code>_message
   * </code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showWarn(final BuCommonInterface _impl, final String _titre, final String[] _message) {
    showWarn(_impl, _titre, CtuluLibString.arrayToString(_message));
  }

  public static GraphicsConfiguration getConfiguration(Point location) {
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] screenDevices = ge.getScreenDevices();
    if (screenDevices.length == 1) {
      return screenDevices[0].getDefaultConfiguration();
    }
    for (GraphicsDevice graphicsDevice : screenDevices) {
      GraphicsConfiguration defaultConfiguration = graphicsDevice.getDefaultConfiguration();
      Rectangle bounds = defaultConfiguration.getBounds();
      if (bounds.contains(location)) {
        return defaultConfiguration;
      }
    }
    return null;
  }

  public static Rectangle getMaximizedWindowBounds(Rectangle initBounds) {
    BuLib.HELPER.setExtendedState(Frame.NORMAL);
    BuLib.HELPER.setLocation(initBounds.getLocation());
    BuLib.HELPER.setSize(initBounds.getSize());
    BuLib.HELPER.setVisible(true);
    BuLib.HELPER.setExtendedState(Frame.MAXIMIZED_BOTH);
    BuLib.HELPER.setVisible(false);
    final Point location = BuLib.HELPER.getLocation();
    Dimension size = BuLib.HELPER.getSize();
    double centerX = BuLib.HELPER.getBounds().getCenterX();
    double centerY = BuLib.HELPER.getBounds().getCenterY();
    GraphicsConfiguration configuration = getConfiguration(new Point((int) centerX, (int) centerY));
    if (configuration != null) {
      final Rectangle bounds = configuration.getBounds();
      if (bounds.x > location.x) {
        size.width -= (bounds.x - location.x);
      }
      if (bounds.y >= location.y) {
        size.height -= (bounds.y - location.y);
      }
      location.x = bounds.x;
      location.y = bounds.y;
      size.height = Math.min(size.height, bounds.height);
      size.width = Math.min(size.width, bounds.width);
    } else {
      location.x = Math.max(0, location.x);
      location.y = Math.max(0, location.y);
    }
    return new Rectangle(location, size);
  }

  public static boolean confirmeOverwriteFile(final Component _impl, final String _nameFile) {
    String yesText = CtuluResource.CTULU.getString("Ecraser le fichier");
    String noText = BuResource.BU.getString("Annuler");
    return showConfirmation(_impl, CtuluResource.CTULU.getString("Attention:"), CtuluResource.CTULU.getString(
            "Le fichier {0} existe d�j�. Voulez-vous l'�craser?", _nameFile), yesText, noText);
  }

  public static boolean confirmeOverwriteFiles(final Component _impl, final String _nameFile) {
    String yesText = CtuluResource.CTULU.getString("Ecraser les fichiers");
    String noText = BuResource.BU.getString("Annuler");
    return showConfirmation(_impl, CtuluResource.CTULU.getString("Attention:"), CtuluResource.CTULU.getString(
            "Les fichiers suivants seront �cras�s:{0}\nVoulez vous continuer ?", _nameFile), yesText, noText);
  }

  /**
   * Affiche une message d'erreur contenant les lignes de
   * <code>_message
   * </code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showError(final BuCommonInterface _impl, final String _titre, final String[] _message) {
    showError(_impl, _titre, CtuluLibString.arrayToString(_message));
  }

  /**
   * Affiche une message d'erreur contenant les lignes de
   * <code>_message
   * </code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showError(final Component _impl, final String _titre, final String _message) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.ERROR_MESSAGE);
    CtuluOptionPane.showDialog(_impl, _titre, p);
  }

  /**
   * Affiche une message d'erreur contenant les lignes de
   * <code>_message
   * </code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showError(final Component _impl, final String _titre, final String[] _message) {
    showError(_impl, _titre, CtuluLibString.arrayToString(_message));
  }

  /**
   * Affiche une message contenant les lignes de
   * <code>_message</code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showMessage(final BuCommonInterface _impl, final String _titre, final String[] _message) {
    showMessage(_impl, _titre, CtuluLibString.arrayToString(_message));
  }

  /**
   * Affiche une boite de dialogue d'information ayant comme titre
   * <code>_titre</code> et comme message
   * <code>_message</code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showMessage(final BuCommonInterface _impl, final String _titre, final String _message) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.INFORMATION_MESSAGE);
    CtuluOptionPane.showDialog(_impl == null ? BuLib.HELPER : _impl.getFrame(), _titre, p);
  }

  public static void showMessage(final Component _impl, final String _titre, final String _message) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.INFORMATION_MESSAGE);
    CtuluOptionPane.showDialog(_impl == null ? BuLib.HELPER : _impl, _titre, p);
  }

  /**
   * Affiche une boite de dialogue d'avertissement ayant comme titre
   * <code>_titre</code> et comme message
   * <code>_message</code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showWarn(final BuCommonInterface _impl, final String _titre, final String _message) {
    JOptionPane.showMessageDialog(_impl == null ? BuLib.HELPER : _impl.getFrame(), _message, _titre,
            JOptionPane.WARNING_MESSAGE);
  }

  public static void showWarn(final Component _impl, final String _titre, final String _message) {
    JOptionPane.showMessageDialog(_impl, _message, _titre, JOptionPane.WARNING_MESSAGE);
  }

  /**
   * Affiche une boite de dialogue d'erreur ayant comme titre
   * <code>_titre</code> et comme message
   * <code>_message</code>.
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   */
  public static void showError(final BuCommonInterface _impl, final String _titre, final String _message) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.ERROR_MESSAGE);
    if (_impl == null) {
      CtuluOptionPane.showDialog(BuLib.HELPER, _titre, p);
    } else {
      CtuluOptionPane.showDialog(_impl.getFrame(), _titre, p);
      /*
       * BuDialogError erreur = _impl == null ? new BuDialogError(null, null, _message) : new
       * BuDialogError(_impl.getImplementation(), _impl.getInformationsSoftware(), _message); erreur.setTitle(_titre);
       * erreur.setModal(true); erreur.activate(); erreur.dispose();
       */
    }
  }

  /**
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   * @return true si l'utilisateur a accepte
   */
  public static boolean showConfirmation(final Component _impl, final String _titre, final String _message) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
    return isOkResponse(CtuluOptionPane.showDialog(_impl, _titre, p));
  }

  public static boolean showConfirmation(final Component _impl, final String _titre, final String _message,
          final String _yesText, final String _noText) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
    p.setOptions(new String[]{_yesText, _noText});
    final JDialog d = p.createDialog(_impl, _titre);
    // en fait c'est juste pour avoir le joli icone en haut a gauche
    d.setResizable(true);
    d.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    d.setModal(true);
    d.pack();
    d.setVisible(true);
    d.dispose();
    return p.getValue() == _yesText;
  }

  /**
   * Une question avec l'icone "erreur".
   *
   * @param _impl l'implementation parente
   * @param _titre le titre du message
   * @param _message le contenu
   * @return true si l'utilisateur a accepte
   */
  public static boolean showErrorConfirmation(final Component _impl, final String _titre, final String _message) {
    final CtuluOptionPane p = new CtuluOptionPane(_message, JOptionPane.ERROR_MESSAGE, JOptionPane.YES_NO_OPTION);
    return isOkResponse(CtuluOptionPane.showDialog(_impl, _titre, p));
  }

  /**
   * @param _impl l'impl parente
   * @return JOptionPane.CANCEL_OPTION si l'utilisateur annule l'action JOptionPane.CANCEL_NO si l'utilisateur ne veut pas sauvegarder le projet
   * JOptionPane.CANCEL_YES si l'utilisateur veut sauvegarder le projet
   */
  public static int confirmExitIfProjectisModified(final Component _impl) {
    final String title = CtuluResource.CTULU.getString("Fermer le projet");
    final String message = CtuluResource.CTULU.getString("Le projet courant a �t� modifi�") + ".\n"
            + CtuluResource.CTULU.getString("Voulez-vous enregistrer les modifications") + "?\n"
            + CtuluResource.CTULU.getString("Si vous r�pondez non, toutes vos modifications seront perdues.");
    final CtuluOptionPane p = new CtuluOptionPane(message, JOptionPane.QUESTION_MESSAGE);
    p.setOptionType(JOptionPane.YES_NO_CANCEL_OPTION);
    return CtuluOptionPane.showDialog(_impl, title, p);

  }

  /**
   * @return true si _r correspond a <code>JOptionPane.YES_OPTION</code> ou <code>JOptionPane.OK_OPTION</code>.
   * @param _r l'entier a tester
   */
  public static boolean isOkResponse(final int _r) {
    return (_r == JOptionPane.YES_OPTION) || (_r == JOptionPane.OK_OPTION);
  }

  /**
   * @return true si _r correspond a <code>JOptionPane.CANCEL_OPTION</code> ou <code>JOptionPane.CLOSED_OPTION</code>.
   * @param _r l'entier a tester
   */
  public static boolean isCancelResponse(final int _r) {
    return (_r == JOptionPane.CANCEL_OPTION) || (_r == JOptionPane.CLOSED_OPTION);
  }
}
