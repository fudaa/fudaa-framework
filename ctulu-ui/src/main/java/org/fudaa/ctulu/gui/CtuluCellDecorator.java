/*
 *  @creation     15 mai 2003
 *  @modification $Date: 2006-02-09 18:50:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.gui;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;

/**
 * @author deniger
 * @version $Id: CtuluCellDecorator.java,v 1.3 2006-02-09 18:50:26 deniger Exp $
 */
public interface CtuluCellDecorator {
  void decore(JComponent _c, JTable _table, Object _value, int _row, int _col);

  void decore(JComponent _c, JList _table, Object _value, int _index);
}
