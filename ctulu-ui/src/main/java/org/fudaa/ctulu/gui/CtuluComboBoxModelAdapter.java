/*
 * @creation 14 d�c. 2005
 * 
 * @modification $Date: 2007-01-17 10:45:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gui;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * @author Fred Deniger
 * @version $Id: CtuluComboBoxModelAdapter.java,v 1.1 2007-01-17 10:45:24 deniger Exp $
 */
public class CtuluComboBoxModelAdapter extends AbstractListModel implements ComboBoxModel, ListDataListener {

  ListModel model_;
  Object selectedObject_;

  /**
   * @param _model
   */
  public CtuluComboBoxModelAdapter(final ListModel _model) {
    super();
    model_ = _model;
    model_.addListDataListener(this);
  }

  @Override
  public void setSelectedItem(final Object _anObject) {
    if (selectedObject_ != _anObject) {
      selectedObject_ = _anObject;
      fireContentsChanged(this, -1, -1);
    }
  }

  @Override
  public Object getSelectedItem() {
    return selectedObject_;
  }

  @Override
  public Object getElementAt(final int _index) {
    return model_.getElementAt(_index);
  }

  @Override
  public int getSize() {
    return model_.getSize();
  }

  public ListModel getModel() {
    return model_;
  }

  public final void setModel(final ListModel _model) {
    if (_model != model_) {
      if (model_ != null) {
        model_.removeListDataListener(this);
      }
      model_ = _model;
      model_.addListDataListener(this);
      fireIntervalAdded(this, 0, model_.getSize());
    }
  }

  /**
   * use to propagate the event from the listModel to the listeners of this model
   */
  @Override
  public void contentsChanged(ListDataEvent e) {
    fireContentsChanged(this, e.getIndex0(), e.getIndex1());
  }
  /**
   * use to propagate the event from the listModel to the listeners of this model
   */
  @Override
  public void intervalAdded(ListDataEvent e) {
    fireIntervalAdded(this, e.getIndex0(), e.getIndex1());

  }
  /**
   * use to propagate the event from the listModel to the listeners of this model
   */
  @Override
  public void intervalRemoved(ListDataEvent e) {
    fireIntervalRemoved(this, e.getIndex0(), e.getIndex1());
  }

}
