/**
 *  @creation     28 janv. 2005
 *  @modification $Date: 2007-01-17 10:45:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gui;


/**
 * @author Fred Deniger
 * @version $Id: CtuluHelpComponent.java,v 1.1 2007-01-17 10:45:24 deniger Exp $
 */
public interface CtuluHelpComponent {
  
  /**
   * @return une aide html (sans les balises html,body) decrivant le composant
   */
  String getShortHtmlHelp();

}
