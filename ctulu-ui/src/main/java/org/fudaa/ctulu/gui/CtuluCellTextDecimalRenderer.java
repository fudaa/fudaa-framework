/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ctulu.gui;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.table.CtuluTableCellDoubleValue;
/**
 * @author deniger
 */
public class CtuluCellTextDecimalRenderer extends CtuluCellTextRenderer {

  CtuluNumberFormatI formatter = CtuluNumberFormatDefault.DEFAULT_FMT;

  /**
   * @return the formatter
   */
  public CtuluNumberFormatI getFormatter() {
    return formatter;
  }

  /**
   * @param _formatter the formatter to set
   */
  public void setFormatter(CtuluNumberFormatI _formatter) {
    formatter = _formatter;
  }

  @Override
  protected void setValue(Object _value) {
    setHorizontalTextPosition(RIGHT);
    setHorizontalAlignment(RIGHT);
    if (_value == null) {
      super.setValue(CtuluLibString.EMPTY_STRING);
      return;
    }
    if (String.class.equals(_value.getClass())) {
      super.setValue(_value);
    } else if(CtuluTableCellDoubleValue.class.equals(_value.getClass())){
    	super.setValue(formatter.format(((CtuluTableCellDoubleValue) _value).getValue()));    	
    }else
      super.setValue(formatter.format(((Number) _value).doubleValue()));
    }
  
}
