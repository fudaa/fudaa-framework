package org.fudaa.ctulu.gui;

import com.memoire.bu.BuResource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.table.CtuluTableCsvWriter;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluTableXlsxWriter;
import org.fudaa.ctulu.table.CtuluWriter;

/**
 * Chooser Générique qui complete tous les formats en ajoutant les interfaces CtuluWriter appropriées.
 *
 * @author Adrien Hadoux
 *
 */
public class CtuluFileChooserWriter extends CtuluFileChooser {
  
  final List<CtuluWriter> listWriter_;
  final CtuluUI ui_;
  
  public CtuluFileChooserWriter(CtuluUI _ui, final List<CtuluWriter> _listWriter) {
    
    ui_ = _ui;
    
    if (_listWriter != null) {
      listWriter_ = _listWriter;
    } else {
      // -- on ajoute par defaut les import export csv et excel --//
      listWriter_ = new ArrayList<CtuluWriter>();
      listWriter_.add(new CtuluTableXlsxWriter());
      listWriter_.add(new CtuluTableExcelWriter());
      listWriter_.add(new CtuluTableCsvWriter());
    }
    for (CtuluWriter writer : listWriter_) {
      writer.getFilter().setExtensionListInDescription(true);
      addChoosableFileFilter(writer.getFilter());
    }
    setDialogType(JFileChooser.SAVE_DIALOG);
    setApproveButtonToolTipText(CtuluLib.getS("L'extension du fichier sera automatiquement complétée"));
    CtuluFileChooserTestWritable tester = new CtuluFileChooserTestWritable(_ui);
    tester.setAppendExtension(true, null);
    tester.setAppendStrictExtension(true);
    setTester(tester);
    
  }

  /**
   * retourne le writter selectionné
   *
   * @return
   */
  public CtuluWriter getSelectedWriter() {
    for (CtuluWriter writer : listWriter_) {
      if (writer.getFilter() == getFileFilter()) {
        return writer;
      }
    }
    return null;
  }
  
  public File getDestFile() {
    final int res = showDialog(ui_.getParentComponent(), BuResource.BU.getString("Continuer"));
    if (res != JFileChooser.APPROVE_OPTION) {
      return null;
    }
    File f = getSelectedFile();
    
    return completeExtension(f);
  }
  
  private File completeExtension(File f) {
    if (f == null) {
      return null;
    }
    f = new File(f.getAbsolutePath() + getSelectedWriter().getMostPopularExtension());
    return f;
  }
  
}
