/*
 * @creation 14 f�vr. 07
 * @modification $Date: 2007-03-23 17:16:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gui;

import com.hexidec.ekit.EkitCore;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: CtuluHtmlEditorPanel.java,v 1.2 2007-03-23 17:16:16 deniger Exp $
 */
public class CtuluHtmlEditorPanel extends EkitCore {

  public static String showMinimalHtmlDialog(final String _txt, final Component _parent, final boolean _modal) {

    final CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout());
    final CtuluHtmlEditorPanel ctuluHtmlEditorPanel = new CtuluHtmlEditorPanel(_txt);
    pn.add(ctuluHtmlEditorPanel);
    pn.add(ctuluHtmlEditorPanel.getToolBar(true), BuBorderLayout.NORTH);
    final CtuluDialog f = new CtuluDialog(CtuluLibSwing.getFrameAncestorHelper(_parent), pn);
    f.setTitle(BuResource.BU.getString("Edition"));
    ctuluHtmlEditorPanel.setDialogHandler(f);
    f.setModal(_modal);
    f.setJMenuBar(ctuluHtmlEditorPanel.getMenuBar(true, false));
    f.pack();
    if (f.afficheAndIsOk()) {
      return ctuluHtmlEditorPanel.getDocumentText();
    }
    return null;

  }

  public CtuluHtmlEditorPanel() {
    this(null, getMinimalActions());
  }

  public static String getMinimalActions() {
    return "SR|SP|BL|IT|UD|SP|SK|SU|SB|SP|AL|AC|AR|AJ|SP|UM|SP|UL|OL|SP|*|FO";
  }

  public CtuluHtmlEditorPanel(final String _htmlText) {
    this(_htmlText, getMinimalActions());
  }

  public CtuluHtmlEditorPanel(final String _htmlText, final String _actions) {
    super(null, null, null, null, null, true, false, true, true, null, null, false, false, false, false, _actions);

    if (CtuluLibString.isEmpty(_htmlText)) {
      setDocumentText("<html><head><style type=\"text/css\"><!--p{margin-top:0px;margin-bottom:0px;}--></style></head><body color=\"#00000\"><p></p></body></html>");
    } else {
      setDocumentText(_htmlText);
    }
  }

  @Override
  public void setDocumentText(final String _text) {
    super.setDocumentText(_text);
    /*
     * Reader r = new StringReader(_text); try { htmlKit.read(r, htmlDoc, 0); } catch (Throwable e) {} ((HTMLEditorKit)
     * (jtpMain.getEditorKit())).setDefaultCursor(new Cursor(Cursor.TEXT_CURSOR)); jtpSource.setText(jtpMain.getText());
     */
  }

  public JMenuBar getMenuBar(final boolean _addFileMenu, final boolean _addFormMenu) {
    final List r = new ArrayList();
    if (_addFileMenu) {
      r.add(EkitCore.KEY_MENU_FILE);
    }
    r.add(EkitCore.KEY_MENU_EDIT);
    r.add(EkitCore.KEY_MENU_FONT);
    r.add(EkitCore.KEY_MENU_FORMAT);
    r.add(EkitCore.KEY_MENU_INSERT);
    r.add(EkitCore.KEY_MENU_TABLE);
    r.add(EkitCore.KEY_MENU_SEARCH);
    if (_addFormMenu) {
      r.add(EkitCore.KEY_MENU_FORMS);
    }
    return getCustomMenuBar(r);
  }

  public static void main(final String[] _args) {
    final JFrame f = new JFrame();
    final JPanel pn = new BuPanel(new BuBorderLayout());
    final CtuluHtmlEditorPanel ctuluHtmlEditorPanel = new CtuluHtmlEditorPanel();
    pn.add(ctuluHtmlEditorPanel);
    pn.add(ctuluHtmlEditorPanel.getToolBar(true), BuBorderLayout.NORTH);
    ctuluHtmlEditorPanel.setFrame(f);
    f.setJMenuBar(ctuluHtmlEditorPanel.getMenuBar());
    
    f.setContentPane(pn);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.pack();
    f.setVisible(true);

  }

}
