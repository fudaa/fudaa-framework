/*
 GPL 2
 */
package org.fudaa.ctulu.action;

import com.memoire.bu.BuResource;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluLibDialog;

/**
 *
 * @author Frederic Deniger
 */
public class MinimizeDialogAction extends AbstractAction {

  private final JComponent jc;
  private SwitchMinMaxDialogAction switcher;

  public MinimizeDialogAction(JComponent jc) {
    super(CtuluLib.getS("Restaurer la fen�tre parente"));
    this.jc = jc;
    putValue(Action.ACTION_COMMAND_KEY, getClass().getSimpleName());
    final KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.ALT_DOWN_MASK);
    putValue(Action.ACCELERATOR_KEY, keyStroke);
    putValue(Action.SMALL_ICON, BuResource.BU.getIcon("down_left"));
    putValue(Action.SHORT_DESCRIPTION, getValue(NAME) + " :  " + ActionsInstaller.getAcceleratorText(keyStroke));
  }

  public SwitchMinMaxDialogAction getSwitcher() {
    return switcher;
  }

  public void setSwitcher(SwitchMinMaxDialogAction switcher) {
    this.switcher = switcher;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, jc);
    if (dialog != null) {
      Rectangle initBounds = dialog.getBounds();
      Rectangle maximizedWindowBounds = CtuluLibDialog.getMaximizedWindowBounds(initBounds);
      if (initBounds.equals(maximizedWindowBounds)) {
        minimizeDialog(dialog, initBounds);
        if (switcher != null) {
          switcher.switchedToMin();
        }
      }
    } else {
      JFrame parent = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, jc);
      if (parent != null) {
        parent.setExtendedState(Frame.NORMAL);
        if (switcher != null) {
          switcher.switchedToMin();
        }
      }
    }
  }

  public void minimizeDialog(JDialog dialog, Rectangle initBounds) {
    Rectangle old = (Rectangle) dialog.getRootPane().getClientProperty(MaximizeDialogAction.PROP_OLD_BOUNDS);
    if (old != null) {
      dialog.setLocation(old.getLocation());
      dialog.setSize(old.getSize());
    } else {
      initBounds.x = initBounds.x + initBounds.width / 4;
      initBounds.y = initBounds.y + initBounds.height / 4;
      initBounds.height = initBounds.height / 2;
      initBounds.width = initBounds.width / 2;
      dialog.setLocation(initBounds.getLocation());
      dialog.setSize(initBounds.getSize());
    }
  }
}
