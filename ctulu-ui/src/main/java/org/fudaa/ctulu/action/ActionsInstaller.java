/*
 GPL 2
 */
package org.fudaa.ctulu.action;

import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

/**
 *
 * @author Frederic Deniger
 */
public class ActionsInstaller {

  public static final String SECOND_KEYSTROKE = "SecondKeyStroke";

  public static void install(JComponent jc) {
    updateMapKeyStrokeForButton(jc, new MaximizeDialogAction(jc), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    updateMapKeyStrokeForButton(jc, new MinimizeDialogAction(jc), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }

  public static String getAcceleratorText(final KeyStroke _accelerator) {
    String acceleratorDelimiter = UIManager.getString("MenuItem.acceleratorDelimiter");
    if (acceleratorDelimiter == null) {
      acceleratorDelimiter = "+";
    }

    final StringBuffer acceleratorText = new StringBuffer(50);
    if (_accelerator != null) {
      final int modifiers = _accelerator.getModifiers();
      if (modifiers > 0) {
        acceleratorText.append(KeyEvent.getKeyModifiersText(modifiers)).append(acceleratorDelimiter);
      }

      final int keyCode = _accelerator.getKeyCode();
      if (keyCode == 0) {
        acceleratorText.append(_accelerator.getKeyChar());
      } else {
        acceleratorText.append(KeyEvent.getKeyText(keyCode));
      }
    }
    return acceleratorText.toString();
  }

  public static SwitchMinMaxDialogAction installAndCreateSwitchAction(JComponent jc) {
    final MaximizeDialogAction maximizeDialogAction = new MaximizeDialogAction(jc);
    updateMapKeyStrokeForButton(jc, maximizeDialogAction, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    final MinimizeDialogAction minimizeDialogAction = new MinimizeDialogAction(jc);
    updateMapKeyStrokeForButton(jc, minimizeDialogAction, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    return new SwitchMinMaxDialogAction(jc, maximizeDialogAction, minimizeDialogAction);
  }

  public MaximizeDialogAction getRegisteredMaximizeAction(JComponent jc) {
    return (MaximizeDialogAction) jc.getActionMap().get(MaximizeDialogAction.class);
  }

  public MinimizeDialogAction getRegisteredMinimizeAction(JComponent jc) {
    return (MinimizeDialogAction) jc.getActionMap().get(MinimizeDialogAction.class);
  }

  public static void install(JDialog dialog) {
    install(dialog.getRootPane());
  }

  public static void updateMapKeyStrokeForButton(final JComponent _c, final Action _ac, final int _when) {
    if (_ac != null) {
      final InputMap thisMap = _c.getInputMap(_when);
      final ActionMap m = _c.getActionMap();
      KeyStroke k = (KeyStroke) _ac.getValue(Action.ACCELERATOR_KEY);
      if (k != null) {
        final Object o = _ac.getValue(Action.ACTION_COMMAND_KEY);
        thisMap.put(k, o);
        m.put(o, _ac);
        k = (KeyStroke) _ac.getValue(SECOND_KEYSTROKE);
        if (k != null) {
          thisMap.put(k, o);
        }
      }
    }
  }
}
