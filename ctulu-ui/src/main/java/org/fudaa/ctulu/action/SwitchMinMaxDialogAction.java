/*
 GPL 2
 */
package org.fudaa.ctulu.action;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.gui.CtuluLibDialog;

/**
 *
 * @author Frederic Deniger
 */
public class SwitchMinMaxDialogAction extends AbstractAction {

  private final JComponent jc;
  private final MaximizeDialogAction maxAction;
  private final MinimizeDialogAction minAction;

  public SwitchMinMaxDialogAction(JComponent jc, MaximizeDialogAction maxAction, MinimizeDialogAction minAction) {
    this.jc = jc;
    this.maxAction = maxAction;
    this.minAction = minAction;
    maxAction.setSwitcher(this);
    minAction.setSwitcher(this);
    putValue(Action.ACTION_COMMAND_KEY, getClass().getSimpleName());
    putValue(Action.SMALL_ICON, maxAction.getValue(Action.SMALL_ICON));
    putValue(Action.SHORT_DESCRIPTION, maxAction.getValue(Action.SHORT_DESCRIPTION));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, jc);
    if (dialog != null) {

      Rectangle initBounds = dialog.getBounds();
      Rectangle maximizedWindowBounds = CtuluLibDialog.getMaximizedWindowBounds(initBounds);
      if (!maximizedWindowBounds.equals(initBounds)) {
        maxAction.maximizeDialog(dialog, maximizedWindowBounds);
        switchedToMax();
        EventQueue.invokeLater(new Runnable() {
          @Override
          public void run() {
            new DialogListener(dialog);
          }
        });
      } else {
        minAction.minimizeDialog(dialog, initBounds);
        switchedToMin();
      }
    } else {
      final JFrame parent = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, jc);
      if (parent != null) {
        if (parent.getExtendedState() == Frame.NORMAL) {
          parent.setExtendedState(Frame.MAXIMIZED_BOTH);
          switchedToMax();
          EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
              new DialogListener(parent);
            }
          });
        } else {
          parent.setExtendedState(Frame.NORMAL);
          switchedToMin();
        }
      }
    }
  }

  public void switchedToMax() {
    putValue(Action.SMALL_ICON, minAction.getValue(Action.SMALL_ICON));
    putValue(Action.SHORT_DESCRIPTION, minAction.getValue(Action.SHORT_DESCRIPTION));
  }

  public void switchedToMin() {
    putValue(Action.SMALL_ICON, maxAction.getValue(Action.SMALL_ICON));
    putValue(Action.SHORT_DESCRIPTION, maxAction.getValue(Action.SHORT_DESCRIPTION));
  }

  private class DialogListener implements ComponentListener, WindowListener {

    private final Window dialog;

    public DialogListener(Window dialog) {
      this.dialog = dialog;
      dialog.addComponentListener(this);
      dialog.addWindowListener(this);
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
      remove();
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void componentResized(ComponentEvent e) {
      remove();
    }

    private void remove() {
      switchedToMin();
      dialog.removeComponentListener(this);
    }

    @Override
    public void componentMoved(ComponentEvent e) {
      remove();
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }
  }
}
