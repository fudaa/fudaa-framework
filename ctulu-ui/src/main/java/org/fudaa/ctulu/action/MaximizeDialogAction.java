/*
 GPL 2
 */
package org.fudaa.ctulu.action;

import com.memoire.bu.BuResource;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import static javax.swing.Action.NAME;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluLibDialog;

/**
 *
 * @author Frederic Deniger
 */
public class MaximizeDialogAction extends AbstractAction {

  public static final String PROP_OLD_BOUNDS = "OLD_BOUNDS";
  private final JComponent jc;
  private SwitchMinMaxDialogAction switcher;

  public MaximizeDialogAction(JComponent jc) {
    super(CtuluLib.getS("Maximiser la fen�tre parente"));
    this.jc = jc;
    putValue(Action.ACTION_COMMAND_KEY, getClass().getSimpleName());
    final KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.ALT_DOWN_MASK);
    putValue(Action.ACCELERATOR_KEY, keyStroke);
    putValue(Action.SMALL_ICON, BuResource.BU.getIcon("up_right"));
    putValue(Action.SHORT_DESCRIPTION, getValue(NAME) + " :  " + ActionsInstaller.getAcceleratorText(keyStroke));
  }

  public SwitchMinMaxDialogAction getSwitcher() {
    return switcher;
  }

  public void setSwitcher(SwitchMinMaxDialogAction switcher) {
    this.switcher = switcher;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final JDialog dialog = (JDialog) SwingUtilities.getAncestorOfClass(JDialog.class, jc);
    if (dialog != null) {

      Rectangle initBounds = dialog.getBounds();
      Rectangle maximizedWindowBounds = CtuluLibDialog.getMaximizedWindowBounds(initBounds);
      if (!maximizedWindowBounds.equals(initBounds)) {
        maximizeDialog(dialog, maximizedWindowBounds);
        if (switcher != null) {
          switcher.switchedToMax();
        }
      }
    } else {
      JFrame parent = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, jc);
      if (parent != null) {
        parent.setExtendedState(Frame.MAXIMIZED_BOTH);
        if (switcher != null) {
          switcher.switchedToMax();
        }
      }
    }
  }

  public void maximizeDialog(final JDialog dialog, Rectangle maximizedWindowBounds) {
    dialog.getRootPane().putClientProperty(PROP_OLD_BOUNDS, dialog.getBounds());
    dialog.setLocation(maximizedWindowBounds.getLocation());
    dialog.setSize(maximizedWindowBounds.getSize());
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        new DialogListener(dialog);
      }
    });
  }

  private static class DialogListener implements ComponentListener, WindowListener {

    private final JDialog dialog;

    public DialogListener(JDialog dialog) {
      this.dialog = dialog;
      dialog.addComponentListener(this);
      dialog.addWindowListener(this);
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
      remove();
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void componentResized(ComponentEvent e) {
      remove();
    }

    private void remove() {
      dialog.getRootPane().putClientProperty(PROP_OLD_BOUNDS, null);
      dialog.removeComponentListener(this);
    }

    @Override
    public void componentMoved(ComponentEvent e) {
      remove();
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }
  }
}
