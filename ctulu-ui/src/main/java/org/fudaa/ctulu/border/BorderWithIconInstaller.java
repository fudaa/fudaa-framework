/*
 GPL 2
 */
package org.fudaa.ctulu.border;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import static javax.swing.Action.SHORT_DESCRIPTION;
import javax.swing.Icon;
import javax.swing.JComponent;

/**
 *
 * @author Frederic Deniger
 */
public class BorderWithIconInstaller {

  /**
   * install a border with the icon of the action. if user clicked on icon, the action is performed.
   *
   * @param jc
   * @param action
   */
  public static void install(JComponent jc, Action action, boolean onlyInDialog) {
    BorderWithIcon border = new BorderWithIcon((Icon) action.getValue(Action.SMALL_ICON));
    border.setPaintInDialogOnly(onlyInDialog);
    jc.setBorder(border);
    jc.addMouseMotionListener(new IconBorderMouseListener(jc, border, action));
    jc.addMouseListener(new IconBorderMouseListener(jc, border, action));
  }
  
  private static class IconBorderMouseListener extends MouseAdapter implements PropertyChangeListener {
    
    private final JComponent jc;
    private final Action action;
    private final BorderWithIcon border;
    private String oldTooltip;
    private boolean tooltipChanged;
    
    public IconBorderMouseListener(JComponent jc, BorderWithIcon border, Action action) {
      this.jc = jc;
      this.border = border;
      this.action = action;
      action.addPropertyChangeListener(this);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (Action.SMALL_ICON.equals(evt.getPropertyName())) {
        border.setIcon((Icon) action.getValue(Action.SMALL_ICON));
        jc.repaint();
      }
      if (Action.SHORT_DESCRIPTION.equals(evt.getPropertyName()) && tooltipChanged) {
        jc.setToolTipText(getTooltipFromAction());
      }
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
      boolean old = border.paintFocus;
      boolean isSelected = border.isSelected(jc, e.getX(), e.getY());
      
      if (old != isSelected) {
        border.paintFocus = isSelected;
        jc.repaint();
        if (isSelected && action.getValue(SHORT_DESCRIPTION) != null) {
          oldTooltip = jc.getToolTipText();
          tooltipChanged = true;
          jc.setToolTipText(getTooltipFromAction());
        } else if (!isSelected && tooltipChanged) {
          restoreOldTooltip();
        }
      }
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
      if (border.paintFocus) {
        border.paintFocus = false;
        if (tooltipChanged) {
          restoreOldTooltip();
        }
        jc.repaint();
      }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
      if (border.paintFocus && action != null) {
        action.actionPerformed(null);
      }
    }
    
    public void restoreOldTooltip() {
      jc.setToolTipText(oldTooltip);
      tooltipChanged = false;
      oldTooltip = null;
    }

    public String getTooltipFromAction() {
      return (String) action.getValue(SHORT_DESCRIPTION);
    }
  }
}
