/*
 GPL 2
 */
package org.fudaa.ctulu.border;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 * Border that display an icon and focus if set.
 *
 * @author Frederic Deniger
 */
public class BorderWithIcon extends EmptyBorder {

  protected Icon icon;
  protected boolean paintFocus;
  private boolean paintInDialogOnly;
  protected Color focus = UIManager.getColor("activeCaptionBorder");

  public BorderWithIcon(int top, int left, int bottom, int right, Icon tileIcon) {
    super(top, left, bottom, right);
    this.icon = tileIcon;
  }

  public boolean isPaintInDialogOnly() {
    return paintInDialogOnly;
  }

  public void setPaintInDialogOnly(boolean paintInDialogOnly) {
    this.paintInDialogOnly = paintInDialogOnly;
  }

  public BorderWithIcon(Insets borderInsets, Icon tileIcon) {
    super(borderInsets);
    this.icon = tileIcon;
  }

  public BorderWithIcon(Icon tileIcon) {
    this(-1, -1, -1, -1, tileIcon);
  }

  public boolean isSelected(Component c, int x, int y) {
    int width = c.getWidth();
    int minx = width - icon.getIconWidth() - 1;
    int maxx = width;
    if (x >= minx && x <= maxx) {
      int miny = 0;
      int maxy = icon.getIconHeight();
      return y >= miny && y <= maxy;
    }
    return false;
  }

  /**
   * Paints the matte border.
   */
  @Override
  public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
    if (paintInDialogOnly) {
      if (SwingUtilities.getAncestorOfClass(JDialog.class, c) == null) {
        return;
      }
    }
    if (icon != null) {
      icon.paintIcon(c, g, x + width - icon.getIconWidth() - 1, y + 1);
      if (paintFocus) {
        Color old = g.getColor();
        g.setColor(focus);
        g.drawRect(x + width - icon.getIconWidth() - 1, y, icon.getIconWidth(), icon.getIconHeight());
        g.setColor(old);
      }
    }
  }

  @Override
  public Insets getBorderInsets(Component c, Insets insets) {
    if (paintInDialogOnly) {
      if (SwingUtilities.getAncestorOfClass(JDialog.class, c) == null) {
        return super.getBorderInsets(c, insets);
      }
    }
    return computeInsets(insets);
  }

  public void setIcon(Icon icon) {
    this.icon = icon;
  }

  /**
   * Returns the insets of the border.
   *
   * @since 1.3
   */
  @Override
  public Insets getBorderInsets() {
    return computeInsets(new Insets(0, 0, 0, 0));
  }

  /* should be protected once api changes area allowed */
  private Insets computeInsets(Insets insets) {
    int w = icon == null ? 0 : icon.getIconWidth();
    int h = icon == null ? 0 : icon.getIconHeight();
    insets.left = left;
    insets.top = top + h + 2;
    insets.right = right;
    insets.bottom = bottom;
    return insets;
  }

  /**
   * Returns the icon used for tiling the border or null if a solid color is being used.
   *
   * @since 1.3
   */
  public Icon getTileIcon() {
    return icon;
  }

  /**
   * Returns whether or not the border is opaque.
   */
  @Override
  public boolean isBorderOpaque() {
    return false;
  }
}
