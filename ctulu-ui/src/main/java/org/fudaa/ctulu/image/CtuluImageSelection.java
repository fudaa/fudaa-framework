/*
 * @creation 6 d�c. 06
 * @modification $Date: 2007-02-07 09:56:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import java.awt.Image;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.TransferHandler;


/**
 * @author fred deniger
 * @version $Id: FudaaImageSelection.java,v 1.3 2007-02-07 09:56:20 deniger Exp $
 */
public class CtuluImageSelection extends TransferHandler implements Transferable, ClipboardOwner {

  public CtuluImageSelection(final Image _image) {
    super();
    image_ = _image;
  }
  public CtuluImageSelection() {
    super();
  }

  @Override
  public void lostOwnership(final Clipboard _clipboard, final Transferable _contents) {}

  private static final DataFlavor[] FLAVORS = { DataFlavor.imageFlavor };

  private Image image_;

  @Override
  public int getSourceActions(final JComponent _c) {
    return TransferHandler.COPY;
  }

  @Override
  public boolean canImport(final JComponent _comp, final DataFlavor[] _flavor) {
    if (!(_comp instanceof CtuluImageProducer)) {
      return false;
    }
    for (int i = 0, n = _flavor.length; i < n; i++) {
      if (_flavor[i].equals(FLAVORS[0])) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Transferable createTransferable(final JComponent _comp) {
    // Clear
    image_ = null;

    if (_comp instanceof CtuluImageProducer) {
      image_ = ((CtuluImageProducer) _comp).produceImage(null);
      return this;
    }
    return null;
  }

  @Override
  public boolean importData(final JComponent _comp, final Transferable _t) {
    return false;
  }

  // Transferable
  @Override
  public Object getTransferData(final DataFlavor _flavor) {
    if (isDataFlavorSupported(_flavor)) {
      return image_;
    }
    return null;
  }

  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(final DataFlavor _flavor) {
    return _flavor.equals(FLAVORS[0]);
  }

}
