/*
 * @creation 8 sept. 06
 * @modification $Date: 2006-12-20 16:05:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

/**
 * @author fred deniger
 * @version $Id: CtuluImageImporter.java,v 1.1 2006-12-20 16:05:19 deniger Exp $
 */
public interface CtuluImageImporter {
  
  void importImage();

}
