/**
 *  @creation     16 juin 2004
 *  @modification $Date: 2007-03-30 15:35:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.image;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.Map;

/**
 * Interface a implementer pour faciliter l'export sous forme d'images.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluImageProducer.java,v 1.5 2007-03-30 15:35:34 deniger Exp $
 */
public interface CtuluImageProducer {

  

  /**
   * @param _params permet d'indiquer des options pour la g�n�ration: remplir le fond pour l'instant
   * @return l'image produite dans la taille courant du composant.
   */
  BufferedImage produceImage(Map _params);

  /**
   * Methode optionnelle: peut renvoyer nulle si non support�e. Voir la classe {@link CtuluLibImage} pour des exemples
   * d'utilisations.
   * 
   * @see CtuluLibImage#produceImageForComponent(java.awt.Component, Map)
   * @param _w la largeur voulue
   * @param _h la hauteur voulue
   * @param _params contient des options
   * @return une image de la taille _w,_h.
   */
  BufferedImage produceImage(int _w, int _h, Map _params);

  /**
   * @return les dimensions par d�faut de l'image.
   */
  Dimension getDefaultImageDimension();

}
