/*
 * @creation 26 janv. 07
 * @modification $Date: 2007-03-23 17:16:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuVerticalLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.RenderedImage;
import java.io.File;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;

/**
 * @author fred deniger
 * @version $Id: CtuluImageExportPanel.java,v 1.4 2007-03-23 17:16:17 deniger Exp $
 */
public class CtuluImageExportPanel extends CtuluDialogPanel {
  CtuluImageDimensionChooserPanel pnDim_;
  CtuluImageProducer producer_;
  CtuluFileChooserPanel pnFile_;
  final CtuluUI ui_;
  String ext_;

  public CtuluImageExportPanel(final CtuluImageProducer _prod, final CtuluUI _ui) {
    producer_ = _prod;
    ui_ = _ui;
    setLayout(new BuVerticalLayout(4, true, false));
    pnFile_ = new CtuluFileChooserPanel(CtuluLib.getS("Fichier")) {
      @Override
      protected CtuluFileChooser createFileChooser() {
        return new CtuluImageFileChooser(producer_);
      }

      @Override
      protected void fileChooserAccepted(final CtuluFileChooser _fc) {
        ext_ = ((CtuluImageFileChooser) _fc).getSelectedFormat();
        final File f = _fc.getSelectedFile();
        if (f != null) {
          final String fName = f.getName();
          final String ext = CtuluLibFile.getExtension(fName);
          // si l'utilisateur a entre une extension valide dans le nom de fichier, on l'utilise
          // malgr� le filtre selectionne.
          if (ext != null && !ext.equals(ext_) && CtuluImageExport.FORMAT_LIST.contains(ext)) {
            ext_ = ext;
          } else {
            pnFile_.getTf().setText(CtuluLibFile.changeExtension(f.getAbsoluteFile(), ext_).getAbsolutePath());
          }
        }

      }
    };
    pnFile_.setWriteMode(true);
    add(CtuluFileChooserPanel.buildPanel(pnFile_, CtuluLib.getS("Fichier")));
    pnDim_ = new CtuluImageDimensionChooserPanel(producer_.getDefaultImageDimension());
    pnDim_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Dimension de l'image cr��e")));
    add(pnDim_);
  }

  @Override
  public boolean isDataValid() {
    cancelErrorText();
    final File dest = pnFile_.getFile();
    final String err = CtuluLibFile.canWrite(dest);
    if (err != null) {
      setErrorText(err);
      return false;
    }
    if (CtuluLibFile.exists(dest) && !CtuluLibDialog.confirmeOverwriteFile(this, dest.getName())) {
      return false;
    }
    if (!pnDim_.isDataValid()) {
      setErrorText(pnDim_.getErrorText());
      return false;
    }
    return super.isDataValid();
  }

  /**
   * Applique la creation des images producer a partir des dimensions pass�es en parametres.
   */
  @Override
  public boolean apply() {
    final File dest = pnFile_.getFile();
    final RenderedImage imageResult ;
    if(pnDim_.isPortrait()){
    	//-- cas classique portrait --//
    	int w=pnDim_.getImageWidth();
    	int h=pnDim_.getImageHeight();
    	imageResult = producer_.produceImage(w, h, null);
    }
    	else{
    	//-- cas paysage:  --//
    	int h=pnDim_.getImageHeight();
    	int w=pnDim_.getImageWidth();
    	BufferedImage img= producer_.produceImage(h, w, null);
//    	//-- on dessinne l'image avec la bonne forme --//
//    	BufferedImage imageRotation = new BufferedImage(w, h, img.getType());
//    	Graphics2D graphicsImageRotation = (Graphics2D)imageRotation.getGraphics();
//    	int centreX=w/2;
//    	int centreY=h/2;
//    	graphicsImageRotation.rotate(3*Math.PI/2,centreX,centreY);
//    	graphicsImageRotation.drawImage(img,0,0,null);
    	imageResult =getRotateImage(img);//imageRotation;
    }
    final CtuluTaskDelegate delegate = ui_.createTask(dest.getName());
    delegate.start(new Runnable() {
      @Override
      public void run() {

        if (CtuluImageExport.export(imageResult, dest, ext_, ui_)) {
          BuLib.invokeLater(new Runnable() {
            @Override
            public void run() {
              ui_.message(dest.getName(), CtuluLib.getS("Enregistrement image r�ussi"), true);
            }
          });
        }
      }

    });
    
    return true;
  }
  
  
  private BufferedImage getRotateImage(BufferedImage img){
	  AffineTransform at = new AffineTransform();
	    // scale image
	    at.scale(1.0, 1.0);
	    // -- rotation 90 deg --//
	    at.rotate( Math.PI / 2.0, img.getWidth() / 2.0, img
	        .getHeight() / 2.0);

	    /*
	     * translate to make sure the rotation doesn't cut off any image data
	     */
	    AffineTransform translationTransform;
	    translationTransform = findTranslation(at, img);
	    at.preConcatenate(translationTransform);

	    // instantiate and apply affine transformation filter
	    BufferedImageOp bio;
	    bio = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);

	    BufferedImage  res = bio.filter(img, null);
	    
	    return res;
  }
  
  
  private AffineTransform findTranslation(AffineTransform at, BufferedImage bi) {
	    Point2D p2din, p2dout;

	    p2din = new Point2D.Double(0.0, 0.0);
	    p2dout = at.transform(p2din, null);
	    double ytrans = p2dout.getY();

	    p2din = new Point2D.Double(0, bi.getHeight());
	    p2dout = at.transform(p2din, null);
	    double xtrans = p2dout.getX();

	    AffineTransform tat = new AffineTransform();
	    tat.translate(-xtrans, -ytrans);
	    return tat;
	  }
}
