/*
 * @creation 24 juil. 06
 * @modification $Date: 2007-01-26 14:57:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluLibSwing;

/**
 * @author fred deniger
 * @version $Id: CtuluImageIconImageFixeSize.java,v 1.1 2007-01-26 14:57:25 deniger Exp $
 */
public class CtuluImageIconImageFixeSize implements Icon {

  final int maxSize_;
  Image img_;
  int w_;
  int h_;

  public CtuluImageIconImageFixeSize(final Image _img) {
    super();
    img_ = _img;
    h_ = _img.getHeight(null);
    w_ = _img.getWidth(null);
    maxSize_ = Math.max(h_, w_);
  }

  @Override
  public int getIconHeight() {
    return maxSize_;
  }

  @Override
  public int getIconWidth() {
    return maxSize_;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    if (img_ == null) {
      _g.setFont(CtuluLibSwing.getMiniFont());
      final String s = CtuluLib.getS("Pas d'image");
      int wString = _g.getFontMetrics().stringWidth(s);
      if (wString < getIconWidth()) {
        wString = (getIconWidth() - wString) / 2;
      } else {
        wString = 0;
      }

      ((Graphics2D) _g).drawString(s, _x + wString, _y + getIconHeight() / 2);
    } else {
      ((Graphics2D) _g).drawImage(img_, (maxSize_ - w_) / 2, (maxSize_ - h_) / 2, w_, h_, null);
    }

  }

}
