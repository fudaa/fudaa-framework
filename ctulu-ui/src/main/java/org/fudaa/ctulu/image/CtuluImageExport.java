/**
 * @creation 16 juin 2004
 * @modification $Date: 2007-02-15 10:14:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.imageio.ImageIO;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.pdf.CtuluPdfPsExport;

/**
 * @author Fred Deniger
 * @version $Id: CtuluImageExport.java,v 1.5 2007-02-15 10:14:53 deniger Exp $
 */
public final class CtuluImageExport {

  /**
   * La liste des formats images supportes � l'export (enregistrables). Liste modifiable.
   */
  public final static List<String> FORMAT_LIST = new ArrayList<String>(Arrays.asList(getAvailableFormat()));

  private CtuluImageExport() {
    super();
  }

  public static void exportImageFor(final CtuluUI _ui, final CtuluImageProducer _producer) {
    new CtuluImageExportPanel(_producer, _ui).afficheModale(_ui.getParentComponent(), CtuluLib
        .getS("Enregistrement image"));
  }

  private static String[] getAvailableFormat() {
    String[] w = ImageIO.getWriterFormatNames();
    final Set<String> r = new HashSet<String>(w.length);
    for (int i = w.length - 1; i >= 0; i--) {
      final String s = w[i].toLowerCase();
      // Les formats contenant des blanc sont supprim�s (se retrouvent ds l'extension) + 
      // format jpeg prete a confusion
      if (!s.contains(" ") && !"jpeg".equals(s)) {
        r.add(s);
      }
    }
    
    //-- ajout des formats pdf et ps --//
    r.add(CtuluPdfPsExport.FORMATPdf);
    r.add(CtuluPdfPsExport.FORMATPS);
    
    
    w = new String[r.size()];
    r.toArray(w);
    Arrays.sort(w);
    return w;
  }
  
  /**
   * Supprime les formats de la liste des formats enregistrables par l'application.
   * @param _formats Les format � supprimer.
   */
  public static void removeWritableFormats(String[] _formats) {
    for (String fmt : _formats) {
      FORMAT_LIST.remove(fmt);
    }
  }

  public static void exportImageFor(final CtuluUI _ui, File _f, String _fmt, final CtuluImageProducer _producer) {
    export(_producer.produceImage(null), _f, _fmt, _ui);
  }

  public static void exportImageFor(final CtuluUI _ui, File _f, final CtuluImageProducer _producer) {
    export(_producer.produceImage(null), _f, CtuluLibFile.getExtension(_f.getName()), _ui);
  }

  /**
   * @param _image l'image a exporter
   * @param _targetFile le fichier cible
   * @param _format le format voulu ( doit appartenir a la liste FORMAT_LIST
   * @param _ui l'interface utilisateur
   * @return true si ok
   * @see #FORMAT_LIST
   */
  public static boolean export(final RenderedImage _image, final File _targetFile, final String _format,
      final CtuluUI _ui) {
    try {
    	if(CtuluPdfPsExport.isApdfPsFormat(_format)){
    		CtuluPdfPsExport.write(_image, _targetFile, _format);
    	}
    	else
    		ImageIO.write(_image, _format, _targetFile);
    } catch (final IllegalArgumentException e) {
      return false;
    } catch (final Exception _e) {
      if (_ui == null) {
        FuLog.error("save image", _e);
      } else {
        _ui.error(BuResource.BU.getString("Enregistrement image..."), _e.getLocalizedMessage(), false);
      }

      return false;
    }
  
    return true;
  }

  public static void exportImageInClipboard(final CtuluImageProducer frame, final CtuluUI _ui) {
    final BufferedImage produceImage = frame.produceImage(null);
    final CtuluTaskDelegate task = _ui.createTask(CtuluLib.getS("Enregistrement dans le presse-papier"));
    task.start(new Runnable() {
  
      @Override
      public void run() {
        final ProgressionInterface prog = task.getStateReceiver();
  
        final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        prog.setProgression(10);
        final CtuluImageSelection selection = new CtuluImageSelection(produceImage);
        prog.setProgression(30);
        clipboard.setContents(selection, selection);
        prog.setProgression(90);
        _ui.message("Image", CtuluLib.getS("Image enregistr�e"), true);
      }
  
    });
  }
  
  
  
  
  

}