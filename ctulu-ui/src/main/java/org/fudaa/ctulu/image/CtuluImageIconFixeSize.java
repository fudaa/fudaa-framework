/*
 * @creation 24 juil. 06
 * @modification $Date: 2006-12-20 16:05:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluLibSwing;

/**
 * @author fred deniger
 * @version $Id: CtuluImageIconFixeSize.java,v 1.1 2006-12-20 16:05:19 deniger Exp $
 */
public class CtuluImageIconFixeSize implements Icon {

  final int maxSize_;
  CtuluImageContainer img_;

  public CtuluImageIconFixeSize(final int _maxSize, final CtuluImageContainer _img) {
    super();
    img_ = _img;
    maxSize_ = _maxSize;
  }

  @Override
  public int getIconHeight() {
    return maxSize_;
  }

  @Override
  public int getIconWidth() {
    return maxSize_;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    if (img_ == null) {
      _g.setFont(CtuluLibSwing.getMiniFont());
      final String s = CtuluLib.getS("Pas d'image");
      int wString = _g.getFontMetrics().stringWidth(s);
      if (wString < getIconWidth()) {
        wString = (getIconWidth() - wString) / 2;
      } else {
        wString = 0;
      }

      ((Graphics2D) _g).drawString(s, _x + wString, _y + getIconHeight() / 2);
    } else {
      img_.paintSnapshot((Graphics2D) _g, getIconWidth(), getIconHeight());
    }

  }

  public CtuluImageContainer getImg() {
    return img_;
  }

  public void setImg(final CtuluImageContainer _img) {
    img_ = _img;
  }

}
