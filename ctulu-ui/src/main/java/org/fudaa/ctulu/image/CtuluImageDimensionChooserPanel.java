/*
 * @creation 25 janv. 07
 * @modification $Date: 2007-02-02 11:20:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import com.memoire.bu.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.editor.CtuluValueEditorInteger;
import org.fudaa.ctulu.gui.CtuluDialogPanel;

/**
 * Un composant permettant de s�lectionner la largeur/ hauteur d'un fichier de sortie.
 * 
 * @author fred deniger
 * @version $Id: CtuluImageDimensionChooserPanel.java,v 1.2 2007-02-02 11:20:06 deniger Exp $
 */
public class CtuluImageDimensionChooserPanel extends CtuluDialogPanel implements DocumentListener {

	final JComponent tfWidth_;
	final JComponent tfHeight_;
	Dimension initDimension_;
	final CtuluValueEditorInteger editor_;
	final BuTransparentToggleButton cbLinked_;
	float initRatio_;
	final BuTransparentButton btDefaireWidth_;
	final BuTransparentButton btDefaireHeight_;
	final JComboBox comboFormatStd_;
	
	private static String PERSONNALISE=CtuluResource.CTULU.getString("Personalis�");
	private static String ISO_A4="A4";
	private static String ISO_A3="A3";
	private static String ISO_A2="A2";
	
	private static String PORTRAIT=CtuluResource.CTULU.getString("Portrait");
	private static String LANDSCAPE=CtuluResource.CTULU.getString("Paysage");
	
	JRadioButton typePortrait_=new JRadioButton(PORTRAIT,true);
	JRadioButton typePaysage_=new JRadioButton(LANDSCAPE,false);
	
	
	
	String[] listeFormatSupported_={PERSONNALISE,ISO_A4,ISO_A3,ISO_A2};


	public CtuluImageDimensionChooserPanel() {
		this(null);
	}

	public CtuluImageDimensionChooserPanel(final Dimension _init) {
		super();

		editor_ = new CtuluValueEditorInteger(false);
		editor_.setEditable(true);
		editor_.setVal(new CtuluValueValidator.IntMin(0));
		tfHeight_ = editor_.createEditorComponent();
		tfWidth_ = editor_.createEditorComponent();
		setLayout(new BuBorderLayout(0, 0, true, false));
		final BuPanel pn = new BuPanel(new BuGridLayout(2, 3, 3));
		addLabel(pn, CtuluLib.getS("Format"));




		comboFormatStd_=new JComboBox(listeFormatSupported_);
		comboFormatStd_.addActionListener(new ActionListener(){
      @Override
			public void actionPerformed(ActionEvent e) {
				selectionFormatStandard(comboFormatStd_.getSelectedIndex());
			}
		});
		pn.add(comboFormatStd_);

		ButtonGroup group=new ButtonGroup();
		group.add(typePaysage_);
		group.add(typePortrait_);
//		typePaysage_.addActionListener(new ActionListener(){
//			public void actionPerformed(ActionEvent e) {
//				invertDimension();
//			}
//		});
//		typePortrait_.addActionListener(new ActionListener(){
//			public void actionPerformed(ActionEvent e) {
//				invertDimension();
//			}
//		});
		addLabel(pn, CtuluLib.getS("Largeur"));
		pn.add(tfWidth_);
		addLabel(pn, CtuluLib.getS("Hauteur"));
		pn.add(tfHeight_);
		
		pn.add(typePortrait_);
		pn.add(typePaysage_);
		
		add(pn, BuBorderLayout.CENTER);
		cbLinked_ = new BuTransparentToggleButton(CtuluResource.CTULU.getIcon("non-lie.png"), CtuluResource.CTULU
				.getIcon("lie.png"));
		cbLinked_.addActionListener(new ActionListener() {

      @Override
			public void actionPerformed(final ActionEvent _e) {
				if (cbLinked_.isSelected()) {
					widthValueChanged();
				}

			}

		});
		cbLinked_.setSelected(true);
		add(cbLinked_, BuBorderLayout.EAST);
		final BuIcon toolIcon = BuResource.BU.getToolIcon("defaire");
		btDefaireWidth_ = new BuTransparentButton(toolIcon);
		btDefaireHeight_ = new BuTransparentButton(toolIcon);
		final String tooltip = CtuluLib.getS("Reinitialiser avec la valeur initiale");
		btDefaireHeight_.setEnabled(false);
		btDefaireWidth_.setEnabled(false);
		tfWidth_.setLayout(new BuBorderLayout());
		tfWidth_.add(btDefaireWidth_, BuBorderLayout.EAST);
		tfHeight_.setLayout(new BuBorderLayout());
		tfHeight_.add(btDefaireHeight_, BuBorderLayout.EAST);
		btDefaireHeight_.addActionListener(new ActionListener() {
      @Override
			public void actionPerformed(final ActionEvent _e) {
				refreshHeight();
			}
		});
		btDefaireWidth_.addActionListener(new ActionListener() {
      @Override
			public void actionPerformed(final ActionEvent _e) {
				refreshWidth();
			}
		});
		((BuTextField) tfHeight_).getDocument().addDocumentListener(this);
		((BuTextField) tfWidth_).getDocument().addDocumentListener(this);
		setInitDimension(_init, true);
		editor_.setValue(new Integer(initDimension_.width), tfWidth_);
		editor_.setValue(new Integer(initDimension_.height), tfHeight_);
		btDefaireWidth_.setToolTipText(tooltip + " (" + initDimension_.width + " )");
		btDefaireWidth_.setToolTipText(tooltip + " (" + initDimension_.height + " )");

	}

	public final void setInitDimension(final Dimension _init, final boolean _force) {
		initDimension_ = (_init == null || _init.width == 0 || _init.height == 0) ? new Dimension(100, 100)
		: new Dimension(_init);
		initRatio_ = CtuluLibImage.getRatio(initDimension_.width, initDimension_.height);
		if (btDefaireHeight_ != null) {
			updateBtHeight();
		}
		if (btDefaireWidth_ != null) {
			updateBtWidth();
		}
		if (_force || !isWidthChanged() || !isHeigthChanged()) {
			editor_.setValue(new Integer(initDimension_.width), tfWidth_);
			editor_.setValue(new Integer(initDimension_.height), tfHeight_);
		}
	}

  @Override
	public void changedUpdate(final DocumentEvent _e) {
		update(_e.getDocument());

	}

  @Override
	public void insertUpdate(final DocumentEvent _e) {
		update(_e.getDocument());
	}

  @Override
	public void removeUpdate(final DocumentEvent _e) {
		update(_e.getDocument());
	}

	boolean update_;

	private void update(final Document _src) {
		if (_src == ((JTextField) tfHeight_).getDocument()) {
			heightValueChanged();
		} else {
			widthValueChanged();
		}

	}

	void widthValueChanged() {
		updateBtWidth();
		if (!update_ && cbLinked_.isSelected()) {
			update_ = true;
			final Object integer = editor_.getValue(tfWidth_);
			if (integer != null) {
				editor_.setValue(new Integer((int) Math.floor(((Integer) integer).intValue() / initRatio_)), tfHeight_);
			}
			update_ = false;

		}
	}

	void heightValueChanged() {
		updateBtHeight();
		if (!update_ && cbLinked_.isSelected()) {
			update_ = true;
			final Object integer = editor_.getValue(tfHeight_);
			if (integer != null) {
				editor_.setValue(new Integer((int) Math.floor(initRatio_ * ((Integer) integer).intValue())), tfWidth_);
			}
			update_ = false;
		}
	}

	final void updateBtWidth() {
		btDefaireWidth_.setEnabled(isWidthChanged());
	}

	private boolean isWidthChanged() {
		return initDimension_.width != getImageWidth();
	}

	final void updateBtHeight() {

		btDefaireHeight_.setEnabled(isHeigthChanged());
	}

	private boolean isHeigthChanged() {
		return initDimension_.height != getImageHeight();
	}

	public int getImageHeight() {
		final Object integer = editor_.getValue(tfHeight_);
		return integer == null ? 0 : ((Integer) integer).intValue();
	}

	public int getImageWidth() {
		final Object integer = editor_.getValue(tfWidth_);
		return integer == null ? 0 : ((Integer) integer).intValue();
	}

	void refreshWidth() {
		editor_.setValue(new Integer(initDimension_.width), tfWidth_);

	}

	void refreshHeight() {
		editor_.setValue(new Integer(initDimension_.height), tfHeight_);
	}

	
	
	public boolean isPaysage(){
		return typePaysage_.isSelected();
	}
	public boolean isPortrait(){
		return typePortrait_.isSelected();
	}
	/**
	 * Classe qui remplit les param�tres en fonction du format standard.
	 * @param selection
	 * @author Adrien Hadoux
	 */
	 public void selectionFormatStandard(int selection) {

		 if(selection==-1 || listeFormatSupported_.length<=selection)
			 return;

		 //-- text bloqu� si on choisit un format standard --//
		 tfWidth_.setEnabled((selection==0));
		 tfHeight_.setEnabled((selection==0));
		 //-- cas personnalis� on ne fait plus rien, on a deja lib�r� les textfields --//
		 if(selection==0){
			 cbLinked_.setSelected(true);
			 return;
		 }
		 boolean lienProportionMemoire=cbLinked_.isSelected();
		//-- il faut enleever les proportions pour modifier proprement--//
		 cbLinked_.setSelected(false);
		 //-- recuperation des formats --//
		 MediaSize media=null;
		 
		 String format=listeFormatSupported_[selection];
		 
		 if(ISO_A4.equals(format)){
			 media=MediaSize.getMediaSizeForName(MediaSizeName.ISO_A4);
		 }else
			 if(ISO_A3.equals(format)){
				 media=MediaSize.getMediaSizeForName(MediaSizeName.ISO_A3);
			 }else
				 if(ISO_A2.equals(format)){
					 media=MediaSize.getMediaSizeForName(MediaSizeName.ISO_A2);
				 }
			 
		 //-- mettre a jour les valeurs --//
		 float newWidth=media.getX(MediaSize.MM);
		 float nw=media.getX(MediaSize.INCH);
		 
		 float newHeight=media.getY(MediaSize.MM);
		 float nh=media.getY(MediaSize.INCH);
		 
		 //-- on a la r�solution dpi de l'ecran --//
		int pixelParpouce= Toolkit.getDefaultToolkit().getScreenResolution();
		 
		//-- [taille en pixels] / [r�solution en dpi] * 2,54 = [taille en centim�tres]  --//
		//-- [taille en pixels]=[taille en centim�tres]*[r�solution en dpi]/2.54 --//
		
		int pixelW= (int) (newWidth/10*pixelParpouce/2.54);
		int pixelH= (int) (newHeight/10*pixelParpouce/2.54);
		
		
		 editor_.setValue(new Integer((int)pixelH),tfHeight_);
		 editor_.setValue(new Integer((int)pixelW),tfWidth_);
		 
		 
		 //-- on remet les proportions comme c'�tait avant --//
		 cbLinked_.setSelected(lienProportionMemoire);

	 }

	 /**
	  * appel� pour inverser les dimensions dans le cas switch paysage/portrait
	  */
	 private void invertDimension(){
		 boolean lienProportionMemoire=cbLinked_.isSelected();
			//-- il faut enleever les proportions pour modifier proprement--//
			 cbLinked_.setSelected(false);
		 int h=getImageHeight();
		 int w=getImageWidth();
		 editor_.setValue(new Integer(w),tfHeight_);
		 editor_.setValue(new Integer(h),tfWidth_);
		 //-- on remet les proportions comme c'�tait avant --//
		 cbLinked_.setSelected(lienProportionMemoire);
	 }
	 
}
