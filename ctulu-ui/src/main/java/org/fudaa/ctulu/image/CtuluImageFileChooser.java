/*
 *  @creation     16 juin 2004
 *  @modification $Date: 2007-02-02 11:20:07 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.image;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFileChooser;

/**
 * @author Fred Deniger
 * @version $Id: CtuluImageFileChooser.java,v 1.3 2007-02-02 11:20:07 deniger Exp $
 */
public class CtuluImageFileChooser extends CtuluFileChooser {

  transient Map filterFormat_;
  Dimension init_;
  CtuluImageProducer producer_;

  public CtuluImageFileChooser(final CtuluImageProducer _init) {
    this(false, _init);
  }

  public CtuluImageFileChooser(final boolean _open, final CtuluImageProducer _init) {
    super(true);
    init_ = _init == null ? null : _init.getDefaultImageDimension();
    producer_ = _init;
    setFileHidingEnabled(false);
    setAcceptAllFileFilterUsed(false);
    final BuFileFilter[] f = new BuFileFilter[CtuluImageExport.FORMAT_LIST.size()];
    filterFormat_ = new HashMap(f.length);
    int pngIDx = 0;
    BuFileFilter all = null;

    if (_open) {

      final String[] filter = (String[]) CtuluImageExport.FORMAT_LIST.toArray(new String[CtuluImageExport.FORMAT_LIST
          .size()]);
      all = new BuFileFilter(filter, BuResource.BU.getString("Image"));
      addChoosableFileFilter(all);

    }
    for (int i = 0; i < f.length; i++) {
      final String fmt = (String) CtuluImageExport.FORMAT_LIST.get(i);
      if ("png".equals(fmt)) {
        pngIDx = i;
      }
      f[i] = new BuFileFilter(fmt, BuResource.BU.getString("Image") + CtuluLibString.ESPACE + fmt);
      filterFormat_.put(f[i], fmt);
      addChoosableFileFilter(f[i]);
    }
    if (_open) {
      setFileFilter(all);
    } else {
      setFileFilter(f[pngIDx]);
    }
    setDialogType(_open ? JFileChooser.OPEN_DIALOG : JFileChooser.SAVE_DIALOG);
  }

  public String getSelectedFormat() {
    return (String) filterFormat_.get(getFileFilter());
  }

  @Override
  protected void addOtherPanel(final JPanel _dest) {

  }

  @Override
  protected JDialog createDialog(final Component _parent) {
    return super.createDialog(_parent);
  }

}