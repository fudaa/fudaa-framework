/*
 * $Id: ScrollPaneSelector.java,v 1.4 2007-05-04 13:43:26 deniger Exp $
 *
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 */

package org.jdesktop.swingx;

import java.awt.*;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

/**
 * ScrollPaneSelector is a little utility class that provides a means to quickly scroll both vertically and horizontally
 * on a single mouse click, by dragging a selection rectangle over a "thumbnail" of the scrollPane's viewport view.
 * <p>
 * Once the selector is installed on a given JScrollPane instance, a little button appears as soon as at least one of
 * its scroll bars is made visible.
 * 
 * @author weebib
 */
public final class ScrollPaneSelector extends JComponent {
  // static final fields
  public static final double MAX_SIZE = 200;
  public static final Icon LAUNCH_SELECTOR_ICON = new Icon() {
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
      Color tmpColor = g.getColor();
      g.setColor(Color.BLACK);
      g.drawRect(2, 2, 10, 10);
      g.drawRect(4, 5, 6, 4);
      g.setColor(tmpColor);
    }

    @Override
    public int getIconWidth() {
      return 15;
    }

    @Override
    public int getIconHeight() {
      return 15;
    }
  };
  static final String COMPONENT_ORIENTATION = "componentOrientation";

  // member fields
  LayoutManager theFormerLayoutManager;
  JScrollPane theScrollPane;

  JComponent theComponent;
  JPopupMenu thePopupMenu;
  JComponent theButton;
  BufferedImage theImage;
  Rectangle theStartRectangle;
  Rectangle theRectangle;
  Point theStartPoint;
  double theScale;
  // PropertyChangeListener theComponentOrientationListener;
  ContainerAdapter theViewPortViewListener;

  /**
   * Installs a ScrollPaneSelector to the given JScrollPane instance.
   * 
   * @param aScrollPane
   */
  public synchronized static void installScrollPaneSelector(final JScrollPane aScrollPane) {
    if (aScrollPane == null) {
      return;
    }
    final ScrollPaneSelector scrollPaneSelector = new ScrollPaneSelector();
    scrollPaneSelector.installOnScrollPane(aScrollPane);

  }

  // -- Constructor ------
  private ScrollPaneSelector() {
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    theScrollPane = null;
    theImage = null;
    theStartRectangle = null;
    theRectangle = null;
    theStartPoint = null;
    theScale = 0.0;
    theButton = new JLabel(LAUNCH_SELECTOR_ICON);
    final MouseInputListener mil = new MouseInputAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        Point p = e.getPoint();
        SwingUtilities.convertPointToScreen(p, theButton);
        display(p);
      }

      @Override
      public void mouseReleased(MouseEvent e) {
        if (theStartPoint != null) {
          Point newPoint = e.getPoint();
          SwingUtilities.convertPointToScreen(newPoint, (Component) e.getSource());
          int deltaX = (int) ((newPoint.x - theStartPoint.x) / theScale);
          int deltaY = (int) ((newPoint.y - theStartPoint.y) / theScale);
          scroll(deltaX, deltaY, true);
        }
        theStartPoint = null;
        theStartRectangle = theRectangle;
      }

      @Override
      public void mouseDragged(MouseEvent e) {
        if (theStartPoint == null) {
          return;
        }
        Point newPoint = e.getPoint();
        SwingUtilities.convertPointToScreen(newPoint, (Component) e.getSource());
        moveRectangle(newPoint.x - theStartPoint.x, newPoint.y - theStartPoint.y);
        int deltaX = (int) ((newPoint.x - theStartPoint.x) / theScale);
        int deltaY = (int) ((newPoint.y - theStartPoint.y) / theScale);
        scroll(deltaX, deltaY, false);
      }
    };
    theButton.addMouseListener(mil);
    theButton.addMouseMotionListener(mil);
    setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
    thePopupMenu = new JPopupMenu();
    thePopupMenu.setLayout(new BorderLayout());
    thePopupMenu.add(this, BorderLayout.CENTER);
    /*
     * theComponentOrientationListener = new PropertyChangeListener() { public void propertyChange(PropertyChangeEvent
     * evt) { if (theScrollPane == null) return; theScrollPane.setCorner(ScrollPaneConstants.LOWER_LEADING_CORNER,
     * null); setCornerInScrollPane(theButton); } };
     */
    theViewPortViewListener = new ContainerAdapter() {
      @Override
      public void componentAdded(final ContainerEvent e) {
        if (thePopupMenu.isVisible()) {
          thePopupMenu.setVisible(false);
        }
        final Component comp = theScrollPane.getViewport().getView();
        theComponent = (comp instanceof JComponent) ? (JComponent) comp : null;
      }
    };
  }

  // -- JComponent overriden methods ------
  @Override
  public Dimension getPreferredSize() {
    if (theImage == null || theRectangle == null) {
      return new Dimension();
    }
    final Insets insets = getInsets();
    return new Dimension(theImage.getWidth(null) + insets.left + insets.right, theImage.getHeight(null) + insets.top
        + insets.bottom);
  }

  @Override
  protected void paintComponent(final Graphics g1D) {
    if (theImage == null || theRectangle == null) {
      return;
    }
    final Graphics2D g = (Graphics2D) g1D;
    final Insets insets = getInsets();
    final int xOffset = insets.left;
    final int yOffset = insets.top;
    final int availableWidth = getWidth() - insets.left - insets.right;
    final int availableHeight = getHeight() - insets.top - insets.bottom;
    g.drawImage(theImage, xOffset, yOffset, null);
    final Color tmpColor = g.getColor();
    final Area area = new Area(new Rectangle(xOffset, yOffset, availableWidth, availableHeight));
    area.subtract(new Area(theRectangle));
    g.setColor(new Color(200, 200, 200, 128));
    g.fill(area);
    g.setColor(Color.BLACK);
    g.draw(theRectangle);
    g.setColor(tmpColor);
  }

  // -- Private methods ------
  private void installOnScrollPane(final JScrollPane aScrollPane) {
    if (theScrollPane != null) {
      uninstallFromScrollPane();
    }
    theScrollPane = aScrollPane;
    theFormerLayoutManager = theScrollPane.getLayout();
    theScrollPane.setLayout(new TweakedScrollPaneLayout());
    // theScrollPane.addPropertyChangeListener(COMPONENT_ORIENTATION, theComponentOrientationListener);
    theScrollPane.getViewport().addContainerListener(theViewPortViewListener);
    setCornerInScrollPane(theButton);
    final Component comp = theScrollPane.getViewport().getView();
    theComponent = (comp instanceof JComponent) ? (JComponent) comp : null;
  }

  private void uninstallFromScrollPane() {
    if (theScrollPane == null) {
      return;
    }
    if (thePopupMenu.isVisible()) {
      thePopupMenu.setVisible(false);
    }
    setCornerInScrollPane(null);
    // theScrollPane.removePropertyChangeListener(COMPONENT_ORIENTATION, theComponentOrientationListener);
    theScrollPane.getViewport().removeContainerListener(theViewPortViewListener);
    theScrollPane.setLayout(theFormerLayoutManager);
    theScrollPane = null;
  }

  void display(final Point aPointOnScreen) {
    if (theComponent == null) {
      return;
    }
    final double compWidth = theComponent.getWidth();
    final double compHeight = theComponent.getHeight();
    final double scaleX = MAX_SIZE / compWidth;
    final double scaleY = MAX_SIZE / compHeight;
    theScale = Math.min(scaleX, scaleY);
    theImage = new BufferedImage((int) (theComponent.getWidth() * theScale),
        (int) (theComponent.getHeight() * theScale), BufferedImage.TYPE_INT_RGB);

    final Graphics2D g = theImage.createGraphics();
    g.scale(theScale, theScale);
    theComponent.paint(g);
    theStartRectangle = theComponent.getVisibleRect();
    final Insets insets = getInsets();
    theStartRectangle.x = (int) (theScale * theStartRectangle.x + insets.left);
    theStartRectangle.y = (int) (theScale * theStartRectangle.y + insets.right);
    theStartRectangle.width *= theScale;
    theStartRectangle.height *= theScale;
    theRectangle = theStartRectangle;

    final Dimension pref = thePopupMenu.getPreferredSize();
    final Point buttonLocation = theButton.getLocationOnScreen();
    final Point popupLocation = new Point((theButton.getWidth() - pref.width) / 2, (theButton.getHeight() - pref.height) / 2);
    final Point centerPoint = new Point(buttonLocation.x + popupLocation.x + theRectangle.x + theRectangle.width / 2,
        buttonLocation.y + popupLocation.y + theRectangle.y + theRectangle.height / 2);
    try {
      // Attempt to move the mouse pointer to the center of the selector's rectangle.
      new Robot().mouseMove(centerPoint.x, centerPoint.y);
      theStartPoint = centerPoint;
    } catch (final Exception e) {
      // Since we cannot move the cursor, we'll move the popup instead.
      theStartPoint = aPointOnScreen;
      popupLocation.x += theStartPoint.x - centerPoint.x;
      popupLocation.y += theStartPoint.y - centerPoint.y;
    }
    thePopupMenu.show(theButton, popupLocation.x, popupLocation.y);
  }

  void moveRectangle(final int aDeltaX, final int aDeltaY) {
    if (theStartRectangle == null) {
      return;
    }

    final Insets insets = getInsets();
    final Rectangle newRect = new Rectangle(theStartRectangle);
    newRect.x += aDeltaX;
    newRect.y += aDeltaY;
    newRect.x = Math.min(Math.max(newRect.x, insets.left), getWidth() - insets.right - newRect.width);
    newRect.y = Math.min(Math.max(newRect.y, insets.right), getHeight() - insets.bottom - newRect.height);
    final Rectangle clip = new Rectangle();
    Rectangle2D.union(theRectangle, newRect, clip);
    clip.grow(2, 2);
    theRectangle = newRect;
    paintImmediately(clip);
  }

  void scroll(final int aDeltaX, final int aDeltaY, final boolean _end) {
    if (theComponent == null) {
      return;
    }
    final Rectangle rect = theComponent.getVisibleRect();
    rect.x += aDeltaX;
    rect.y += aDeltaY;
    theComponent.scrollRectToVisible(rect);
    if (_end) {
      thePopupMenu.setVisible(false);
    }
  }

  public void setCornerInScrollPane(final Component _c) {

    try {
      theScrollPane.setCorner(ScrollPaneConstants.LOWER_TRAILING_CORNER, _c);
    } catch (final IllegalArgumentException _evt) {
      theScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, _c);
    }
  }
}