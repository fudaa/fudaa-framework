/*
 * $Id: TweakedScrollPaneLayout.java,v 1.3 2007-05-04 13:43:26 deniger Exp $
 *
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 */

package org.jdesktop.swingx;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.Scrollable;
import javax.swing.border.Border;

/**
 * A hideous hack to allow the display of the selector's button even when only one scrollBar is visible.
 *
 * @author weebib
 */
class TweakedScrollPaneLayout extends ScrollPaneLayout {
	private void superlayoutContainer(final Container parent) {
		final JScrollPane scrollPane = (JScrollPane)parent;
		vsbPolicy = scrollPane.getVerticalScrollBarPolicy();
		hsbPolicy = scrollPane.getHorizontalScrollBarPolicy();

		final Rectangle availR = scrollPane.getBounds();
		final Insets insets = parent.getInsets();
		availR.x = insets.left;
		availR.y = insets.top;
		availR.width -= insets.left + insets.right;
		availR.height -= insets.top + insets.bottom;

		boolean leftToRight = scrollPane.getComponentOrientation().isLeftToRight();

		final Rectangle colHeadR = new Rectangle(0, availR.y, 0, 0);

		if ((colHead != null) && (colHead.isVisible())) {
			final int colHeadHeight = Math.min(availR.height, colHead.getPreferredSize().height);
			colHeadR.height = colHeadHeight;
			availR.y += colHeadHeight;
			availR.height -= colHeadHeight;
		}

		final Rectangle rowHeadR = new Rectangle(0, 0, 0, 0);

		if ((rowHead != null) && (rowHead.isVisible())) {
			final int rowHeadWidth = Math.min(availR.width, rowHead.getPreferredSize().width);
			rowHeadR.width = rowHeadWidth;
			availR.width -= rowHeadWidth;
			if (leftToRight) {
				rowHeadR.x = availR.x;
				availR.x += rowHeadWidth;
			} else {
				rowHeadR.x = availR.x + availR.width;
			}
		}

		final Border viewportBorder = scrollPane.getViewportBorder();
		Insets vpbInsets;
		if (viewportBorder == null) {
			vpbInsets = new Insets(0, 0, 0, 0);
		} else {
			vpbInsets = viewportBorder.getBorderInsets(parent);
			availR.x += vpbInsets.left;
			availR.y += vpbInsets.top;
			availR.width -= vpbInsets.left + vpbInsets.right;
			availR.height -= vpbInsets.top + vpbInsets.bottom;
		}

		final Component view = (viewport == null) ? null : viewport.getView();
		final Dimension viewPrefSize = (view == null) ? new Dimension(0, 0) : view.getPreferredSize();

		Dimension extentSize = (viewport == null) ? new Dimension(0, 0) : viewport.toViewCoordinates(availR.getSize());

		boolean viewTracksViewportWidth = false;
		boolean viewTracksViewportHeight = false;
		boolean isEmpty = (availR.width < 0 || availR.height < 0);
		Scrollable sv;
		if (!isEmpty && view instanceof Scrollable) {
			sv = (Scrollable)view;
			viewTracksViewportWidth = sv.getScrollableTracksViewportWidth();
			viewTracksViewportHeight = sv.getScrollableTracksViewportHeight();
		} else {
			sv = null;
		}

		final Rectangle vsbR = new Rectangle(0, availR.y - vpbInsets.top, 0, 0);

		boolean vsbNeeded;
		if (isEmpty) {
			vsbNeeded = false;
		} else if (vsbPolicy == VERTICAL_SCROLLBAR_ALWAYS) {
			vsbNeeded = true;
		} else if (vsbPolicy == VERTICAL_SCROLLBAR_NEVER) {
			vsbNeeded = false;
		} else {  // vsbPolicy == VERTICAL_SCROLLBAR_AS_NEEDED
			vsbNeeded = !viewTracksViewportHeight && (viewPrefSize.height > extentSize.height);
		}

		if ((vsb != null) && vsbNeeded) {
			adjustForVSB(true, availR, vsbR, vpbInsets, leftToRight);
			extentSize = viewport.toViewCoordinates(availR.getSize());
		}

		final Rectangle hsbR = new Rectangle(availR.x - vpbInsets.left, 0, 0, 0);
		boolean hsbNeeded;
		if (isEmpty) {
			hsbNeeded = false;
		} else if (hsbPolicy == HORIZONTAL_SCROLLBAR_ALWAYS) {
			hsbNeeded = true;
		} else if (hsbPolicy == HORIZONTAL_SCROLLBAR_NEVER) {
			hsbNeeded = false;
		} else {  // hsbPolicy == HORIZONTAL_SCROLLBAR_AS_NEEDED
			hsbNeeded = !viewTracksViewportWidth && (viewPrefSize.width > extentSize.width);
		}
		if ((hsb != null) && hsbNeeded) {
			adjustForHSB(true, availR, hsbR, vpbInsets);
			if ((vsb != null) && !vsbNeeded && (vsbPolicy != VERTICAL_SCROLLBAR_NEVER)) {
				extentSize = viewport.toViewCoordinates(availR.getSize());
				vsbNeeded = viewPrefSize.height > extentSize.height;
				if (vsbNeeded) {
					adjustForVSB(true, availR, vsbR, vpbInsets, leftToRight);

					// Hack to correct Bug 6411225
					// I don't actually measure the consequences of this bug "fix"

					if (!leftToRight) {
						hsbR.x += vsbR.width;
					}

					// end of hack
				}
			}
		}

		if (viewport != null) {
			viewport.setBounds(availR);

			if (sv != null) {
				extentSize = viewport.toViewCoordinates(availR.getSize());

				final boolean oldHSBNeeded = hsbNeeded;
				final boolean oldVSBNeeded = vsbNeeded;
				viewTracksViewportWidth = sv.
						getScrollableTracksViewportWidth();
				viewTracksViewportHeight = sv.
						getScrollableTracksViewportHeight();
				if (vsb != null && vsbPolicy == VERTICAL_SCROLLBAR_AS_NEEDED) {
					final boolean newVSBNeeded = !viewTracksViewportHeight && (viewPrefSize.height > extentSize.height);
					if (newVSBNeeded != vsbNeeded) {
						vsbNeeded = newVSBNeeded;
						adjustForVSB(vsbNeeded, availR, vsbR, vpbInsets, leftToRight);
						extentSize = viewport.toViewCoordinates(availR.getSize());
					}
				}
				if (hsb != null && hsbPolicy == HORIZONTAL_SCROLLBAR_AS_NEEDED) {
					final boolean newHSBbNeeded = !viewTracksViewportWidth && (viewPrefSize.width > extentSize.width);
					if (newHSBbNeeded != hsbNeeded) {
						hsbNeeded = newHSBbNeeded;
						adjustForHSB(hsbNeeded, availR, hsbR, vpbInsets);
						if ((vsb != null) && !vsbNeeded && (vsbPolicy != VERTICAL_SCROLLBAR_NEVER)) {
							extentSize = viewport.toViewCoordinates(availR.getSize());
							vsbNeeded = viewPrefSize.height > extentSize.height;
							if (vsbNeeded) {
								adjustForVSB(true, availR, vsbR, vpbInsets, leftToRight);
							}
						}
					}
				}
				if (oldHSBNeeded != hsbNeeded || oldVSBNeeded != vsbNeeded) {
					viewport.setBounds(availR);
				}
			}
		}
		vsbR.height = availR.height + vpbInsets.top + vpbInsets.bottom;
		hsbR.width = availR.width + vpbInsets.left + vpbInsets.right;
		rowHeadR.height = availR.height + vpbInsets.top + vpbInsets.bottom;
		rowHeadR.y = availR.y - vpbInsets.top;
		colHeadR.width = availR.width + vpbInsets.left + vpbInsets.right;
		colHeadR.x = availR.x - vpbInsets.left;

		if (rowHead != null) {
			rowHead.setBounds(rowHeadR);
		}

		if (colHead != null) {
			colHead.setBounds(colHeadR);
		}

		if (vsb != null) {
			if (vsbNeeded) {
				vsb.setVisible(true);
				vsb.setBounds(vsbR);
			} else {
				vsb.setVisible(false);
			}
		}

		if (hsb != null) {
			if (hsbNeeded) {
				hsb.setVisible(true);
				hsb.setBounds(hsbR);
			} else {
				hsb.setVisible(false);
			}
		}

		//bug 6411225 ????????

		if (lowerLeft != null) {
			lowerLeft.setBounds(leftToRight ? rowHeadR.x : vsbR.x,
								hsbR.y,
								leftToRight ? rowHeadR.width : vsbR.width,
								hsbR.height);
		}

		if (lowerRight != null) {
			lowerRight.setBounds(leftToRight ? vsbR.x : rowHeadR.x,
								 hsbR.y,
								 leftToRight ? vsbR.width : rowHeadR.width,
								 hsbR.height);
		}

		if (upperLeft != null) {
			upperLeft.setBounds(leftToRight ? rowHeadR.x : vsbR.x,
								colHeadR.y,
								leftToRight ? rowHeadR.width : vsbR.width,
								colHeadR.height);
		}

		if (upperRight != null) {
			upperRight.setBounds(leftToRight ? vsbR.x : rowHeadR.x,
								 colHeadR.y,
								 leftToRight ? vsbR.width : rowHeadR.width,
								 colHeadR.height);
		}
	}

	private void adjustForVSB(final boolean wantsVSB, final Rectangle available, final Rectangle vsbR,
							  final Insets vpbInsets, final boolean leftToRight) {
		final int oldWidth = vsbR.width;
		if (wantsVSB) {
			final int vsbWidth = Math.max(0, Math.min(vsb.getPreferredSize().width,
												available.width));

			available.width -= vsbWidth;
			vsbR.width = vsbWidth;

			if (leftToRight) {
				vsbR.x = available.x + available.width + vpbInsets.right;
			} else {
				vsbR.x = available.x - vpbInsets.left;
				available.x += vsbWidth;
			}
		} else {
			available.width += oldWidth;
		}
	}

	private void adjustForHSB(final boolean wantsHSB, final Rectangle available,
							  final Rectangle hsbR, final Insets vpbInsets) {
		final int oldHeight = hsbR.height;
		if (wantsHSB) {
			final int hsbHeight = Math.max(0, Math.min(available.height,
												 hsb.getPreferredSize().height));

			available.height -= hsbHeight;
			hsbR.y = available.y + available.height + vpbInsets.bottom;
			hsbR.height = hsbHeight;
		} else {
			available.height += oldHeight;
		}
	}

  @Override
	public void layoutContainer(final Container parent) {
		superlayoutContainer(parent);
		boolean isLeftToRight = parent.getComponentOrientation().isLeftToRight();
		if (isLeftToRight && lowerRight == null) {
      return;
    }
		if (!isLeftToRight && lowerLeft == null) {
      return;
    }
		if (vsb.isVisible() && !hsb.isVisible()) {
			final Rectangle vsbBounds = vsb.getBounds();
			final Dimension hsbPref = hsb.getPreferredSize();
			final int delta = hsbPref.height;
			vsbBounds.height -= delta;
			vsb.setBounds(vsbBounds);
			if (isLeftToRight) {
				lowerRight.setBounds(vsbBounds.x, vsbBounds.y + vsbBounds.height, vsbBounds.width, delta);
				lowerRight.setVisible(true);
			} else {
				lowerLeft.setBounds(vsbBounds.x, vsbBounds.y + vsbBounds.height, vsbBounds.width, delta);
				lowerLeft.setVisible(true);
			}
		}
		if (hsb.isVisible() && !vsb.isVisible()) {
			final Rectangle hsbBounds = hsb.getBounds();
			final Dimension vsbPref = vsb.getPreferredSize();
			final int delta = vsbPref.width;
			hsbBounds.width -= delta;
			if (!isLeftToRight) {
				hsbBounds.x += delta;
			}
			hsb.setBounds(hsbBounds);
			if (isLeftToRight) {
				lowerRight.setBounds(hsbBounds.x + hsbBounds.width, hsbBounds.y, delta, hsbBounds.height);
				lowerRight.setVisible(true);
			} else {
				lowerLeft.setBounds(hsbBounds.x - delta, hsbBounds.y, delta, hsbBounds.height);
				lowerLeft.setVisible(true);
			}
		}
	}
}