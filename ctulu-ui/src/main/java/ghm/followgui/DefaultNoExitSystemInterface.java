/**
 *  @file         DefaultNoExitSystemInterface.java
 *  @creation     2 d�c. 2003
 *  @modification $Date: 2006-11-16 15:04:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package ghm.followgui;

import com.memoire.bu.BuRegistry;
import javax.swing.MenuSelectionManager;

/**
 * @author deniger
 * @version $Id: DefaultNoExitSystemInterface.java,v 1.8 2006-11-16 15:04:55 deniger Exp $
 */
public class DefaultNoExitSystemInterface extends DefaultSystemInterface {

  /**
   * @param app
   */
  public DefaultNoExitSystemInterface(final FollowApp app) {
    super(app);
  }

  /**
   * @see ghm.followgui.SystemInterface#exit(int)
   */
  @Override
  public void exit(final int code) {
    app_.popupMenu_.removeAll();
    MenuSelectionManager.defaultManager().clearSelectedPath();
    if (BuRegistry.getModel().contains(app_)) {
      BuRegistry.unregister(app_);
    }
  }
}