/* 
Copyright (C) 2000-2003 Greg Merrill (greghmerrill@yahoo.com)

This file is part of Follow (http://follow.sf.net).

Follow is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public
License as published by the Free Software Foundation.

Follow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Follow; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package ghm.followgui;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author <a href="mailto:greghmerrill@yahoo.com">Greg Merrill</a>
 */
class FollowAppAttributes {
  FollowAppAttributes(final FollowApp app) throws IOException {
    if (!(propertyFile.exists())) {
      // If the property file doesn't exist, we create a default property 
      // file using a prototype property file stored somewhere on the classpath
      System.out.println(
          "No property file for the Follow application is present; creating " +
              propertyFile.getAbsolutePath() + " (with default values) ..."
      );
      properties_ = (EnumeratedProperties) getDefaultProperties().clone();
      System.out.println("... property file created successfully.");
    } else {
      properties_ = new EnumeratedProperties();
      final FileInputStream fis = new FileInputStream(propertyFile);
      properties_.load(fis);
      switch (getAttributesVersion()) {
        case UNVERSIONED:
          // Migrate unversioned attributes to 1.1 attributes
          System.out.println("Migrating pre-v1.1 properties to v1.1.");
          setAttributesVersion(v1_1);
          setTabPlacement(getDefaultAttributes().getTabPlacement());
        case v1_1:
          // Migrate 1.1 attributes to 1.2 attributes
          System.out.println("Migrating v1.1 properties to v1.2.");
          setAttributesVersion(v1_2);
          setFont(getDefaultAttributes().getFont());
        case v1_2:
          // Migrate 1.2 attributes to 1.3 attributes
          System.out.println("Migrating v1.2 properties to v1.3.");
          setAttributesVersion(v1_3);
          setConfirmDelete(true);
          setConfirmDeleteAll(true);
          // Additionally, it is necessary to warn the user about the changes to 
          // Clear and ClearAll and the introduction of Delete and DeleteAll
          JOptionPane.showMessageDialog(
              null,
              app.resBundle_.getString("v1.3.warning.text"),
              app.resBundle_.getString("v1.3.warning.title"),
              JOptionPane.WARNING_MESSAGE
          );
        case v1_3:
        case v1_3_2:
          // Migrate 1.3 attributes to 1.4 attributes
          System.out.println("Migrating v1.3 properties to v1.4.");
          setAttributesVersion(v1_4);
          setAutoScroll(true);
          // Inform the user of the new AutoScroll feature
          JOptionPane.showMessageDialog(
              null,
              app.resBundle_.getString("v1.4.info.text"),
              app.resBundle_.getString("v1.4.info.title"),
              JOptionPane.INFORMATION_MESSAGE
          );
      }
      fis.close();
    }
  }

  private FollowAppAttributes(final EnumeratedProperties props) throws IOException {
    properties_ = props;
  }

  int getHeight() {
    return getInt(heightKey);
  }

  void setHeight(final int height) {
    setInt(heightKey, height);
  }

  int getWidth() {
    return getInt(widthKey);
  }

  void setWidth(final int width) {
    setInt(widthKey, width);
  }

  int getX() {
    return getInt(xKey);
  }

  void setX(final int x) {
    setInt(xKey, x);
  }

  int getY() {
    return getInt(yKey);
  }

  void setY(final int y) {
    setInt(yKey, y);
  }

  Iterator getFollowedFiles() {
    final List fileNames = properties_.getEnumeratedProperty(followedFilesKey);
    final List files = new ArrayList();
    final Iterator i = fileNames.iterator();
    while (i.hasNext()) {
      files.add(new File((String) i.next()));
    }
    return files.iterator();
  }

  /**
   * @return true iff any File in the List of followed Files
   *     (getFollowedFiles()) has the same Canonical Path as the supplied File
   */
  boolean followedFileListContains(final File file) throws IOException {
    final Iterator i = getFollowedFiles();
    while (i.hasNext()) {
      final File nextFile = (File) i.next();
      if (nextFile.getCanonicalPath().equals(file.getCanonicalPath())) {
        return true;
      }
    }
    return false;
  }

  void addFollowedFile(final File file) {
    final List fileNames = properties_.getEnumeratedProperty(followedFilesKey);
    fileNames.add(file.getAbsolutePath());
    properties_.setEnumeratedProperty(
        followedFilesKey,
        fileNames
    );
  }

  void removeFollowedFile(final File file) {
    final List fileNames = properties_.getEnumeratedProperty(followedFilesKey);
    fileNames.remove(file.getAbsolutePath());
    properties_.setEnumeratedProperty(
        followedFilesKey,
        fileNames
    );
  }

  int getTabPlacement() {
    return getInt(tabPlacementKey);
  }

  void setTabPlacement(final int tabPlacement) {
    setInt(tabPlacementKey, tabPlacement);
  }

  int getSelectedTabIndex() {
    try {
      return getInt(selectedTabIndexKey);
    } catch (final NumberFormatException e) {
      setSelectedTabIndex(0);
      return 0;
    }
  }

  void setSelectedTabIndex(final int selectedTabIndex) {
    setInt(selectedTabIndexKey, selectedTabIndex);
  }

  File getLastFileChooserDirectory() {
    return new File(properties_.getProperty(lastFileChooserDirKey, userHome));
  }

  void setLastFileChooserDirectory(final File file) {
    properties_.setProperty(lastFileChooserDirKey, file.getAbsolutePath());
  }

  int getBufferSize() {
    return getInt(bufferSizeKey);
  }

  void setBufferSize(final int _bufferSize) {
    setInt(bufferSizeKey, _bufferSize);
  }

  void setBufferSize(final String _bufferSize) {
    setBufferSize(Integer.parseInt(_bufferSize));
  }

  int getLatency() {
    return getInt(latencyKey);
  }

  void setLatency(final int latency) {
    setInt(latencyKey, latency);
  }

  void setLatency(final String latency) {
    setLatency(Integer.parseInt(latency));
  }

  int getAttributesVersion() {
    if (properties_.get(attributesVersionKey) == null) {
      // Supporting v1.0 & v1.0.1, which had no notion of attributes version
      return UNVERSIONED;
    }
    return getInt(attributesVersionKey);
  }

  void setAttributesVersion(final int attributesVersion) {
    setInt(attributesVersionKey, attributesVersion);
  }

  Font getFont() {
    final Font font = new Font(
        properties_.getProperty(fontFamilyKey),
        getInt(fontStyleKey),
        getInt(fontSizeKey)
    );
    return font;
  }

  void setFont(final Font font) {
    properties_.setProperty(fontFamilyKey, font.getFontName());
    setInt(fontStyleKey, font.getStyle());
    setInt(fontSizeKey, font.getSize());
  }

  public boolean confirmDelete() {
    return getBoolean(confirmDeleteKey);
  }

  public void setConfirmDelete(final boolean value) {
    setBoolean(confirmDeleteKey, value);
  }

  public boolean confirmDeleteAll() {
    return getBoolean(confirmDeleteAllKey);
  }

  public void setConfirmDeleteAll(final boolean value) {
    setBoolean(confirmDeleteAllKey, value);
  }

  public boolean autoScroll() {
    return getBoolean(autoScrollKey);
  }

  public void setAutoScroll(final boolean value) {
    setBoolean(autoScrollKey, value);
  }

  public String getEditor() {
    String result = properties_.getProperty(editorKey);
    if (result == null) {
      result = "";
    }

    return (result);
  }

  public void setEditor(final String value) {
    properties_.setProperty(editorKey, value);
  }

  void store() throws IOException {
    final BufferedOutputStream bos =
        new BufferedOutputStream(new FileOutputStream(propertyFileName));
    properties_.store(bos, null);
    bos.close();
  }

  private int getInt(final String key) {
    return Integer.parseInt(properties_.getProperty(key));
  }

  private void setInt(final String key, final int value) {
    properties_.setProperty(key, String.valueOf(value));
  }

  private boolean getBoolean(final String key) {
    return "true".equals(properties_.getProperty(key));
  }

  private void setBoolean(final String key, final boolean value) {
    properties_.setProperty(key, String.valueOf(value));
  }

  FollowAppAttributes getDefaultAttributes() throws IOException {
    if (defaultAttributes_ == null) {
      defaultAttributes_ = new FollowAppAttributes(getDefaultProperties());
      // Check for the unlikely possibility that the default font is
      // unavailable
      final Font defaultFont = defaultAttributes_.getFont();
      final String[] availableFontFamilyNames =
          GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
      boolean defaultFontIsAvailable = false;
      for (int i = 0; i < availableFontFamilyNames.length; i++) {
        if (defaultFont.getFamily().equals(availableFontFamilyNames[i])) {
          defaultFontIsAvailable = true;
          break;
        }
      }
      if (!defaultFontIsAvailable) {
        System.out.println(
            "Font family " + defaultFont.getFamily() + " is unavailable; using " +
                availableFontFamilyNames[0] + " instead."
        );
        defaultAttributes_.setFont(new Font(
            availableFontFamilyNames[0],
            defaultFont.getStyle(),
            defaultFont.getSize()
        ));
      }
    }
    return defaultAttributes_;
  }

  private EnumeratedProperties getDefaultProperties() throws IOException {
    if (defaultProperties_ == null) {
      try (final BufferedInputStream bis = new BufferedInputStream(
          this.getClass().getResourceAsStream(propertyPrototypeFileName)
      )) {
        try (final BufferedOutputStream bos = new BufferedOutputStream(
            new FileOutputStream(propertyFile)
        )) {
          final byte[] byteArray = new byte[bufferSize];
          int len;
          while ((len = bis.read(byteArray, 0, bufferSize)) > 0) {
            bos.write(byteArray, 0, len);
          }
          bos.flush();
        }
      }
      defaultProperties_ = new EnumeratedProperties();
      defaultProperties_.load(new BufferedInputStream(
          new FileInputStream(propertyFile)
      ));
    }
    return defaultProperties_;
  }

  EnumeratedProperties properties_;
  private EnumeratedProperties defaultProperties_;
  private FollowAppAttributes defaultAttributes_;
  static final String userHome = System.getProperty("user.home");
  static final String propertyFileName =
      userHome + FollowApp.fileSeparator + ".followApp.properties";
  static final File propertyFile = new File(propertyFileName);
  static final String propertyPrototypeFileName =
      "followApp.properties.prototype";
  static final int bufferSize = 32768;
  static final String heightKey = "height";
  static final String widthKey = "width";
  static final String xKey = "x";
  static final String yKey = "y";
  static final String followedFilesKey = "followedFiles";
  static final String tabPlacementKey = "tabs.placement";
  static final String selectedTabIndexKey = "tabs.selectedIndex";
  static final String lastFileChooserDirKey = "fileChooser.lastDir";
  static final String bufferSizeKey = "bufferSize";
  static final String latencyKey = "latency";
  static final String attributesVersionKey = "attributesVersion";
  static final String fontFamilyKey = "fontFamily";
  static final String fontStyleKey = "fontStyle";
  static final String fontSizeKey = "fontSize";
  static final String confirmDeleteKey = "confirmDelete";
  static final String confirmDeleteAllKey = "confirmDeleteAll";
  static final String autoScrollKey = "autoScroll";
  static final String editorKey = "editor";
  // Versions
  static final int UNVERSIONED = 0;
  static final int v1_1 = 1;
  static final int v1_2 = 2;
  static final int v1_3 = 3;
  static final int v1_3_2 = 4;
  static final int v1_4 = 5;
}

