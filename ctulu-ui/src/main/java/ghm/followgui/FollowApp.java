/* 
Copyright (C) 2000-2003 Greg Merrill (greghmerrill@yahoo.com)

This file is part of Follow (http://follow.sf.net).

Follow is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public
License as published by the Free Software Foundation.

Follow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Follow; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package ghm.followgui;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
/**
This class' main() method is the entry point into the Follow 
application.
@see #main(String[])
@author <a href="mailto:greghmerrill@yahoo.com">Greg Merrill</a>
*/
public class FollowApp {
  static final boolean DEBUG= Boolean.getBoolean("follow.debug");
  static final String fileSeparator= System.getProperty("file.separator");
  static boolean HAS_SOLARIS_BUG= false;
  static FollowApp instance_;
  static final String messageLineSeparator= "\n";
  // We should remove this hack once JDK 1.4 gets wide adoption on Solaris.
  static {
    final boolean isSolaris= "SunOS".equals(System.getProperty("os.name"));
    if (isSolaris) {
      final String version= System.getProperty("java.version");
      if ((version != null) && version.startsWith("1.")) {
        final String substring= version.substring(2, 3);
        try {
          final int minor= Integer.parseInt(substring);
          if (minor < 4) {
            HAS_SOLARIS_BUG= true;
          }
        } catch (final NumberFormatException nfe) {
          // Nothing else to do.
        }
      }
    }
  }
  static void centerWindowInScreen(final Window window) {
    final Dimension screenSize= window.getToolkit().getScreenSize();
    final Dimension windowSize= window.getPreferredSize();
    window.setLocation(
      (int) (screenSize.getWidth() / 2 - windowSize.getWidth() / 2),
      (int) (screenSize.getHeight() / 2 - windowSize.getHeight() / 2));
  }
  /**
  Invoke this method to start the Follow application.  If any command-line
  arguments are passed in, they are assume to be filenames and are opened
  in the Follow application
  @param args files to be opened
  */
  public static void main(final String[] args)
    throws IOException, InterruptedException, InvocationTargetException {
    instance_= new FollowApp(args);
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        // ensure all widgets inited before opening files
        instance_.show();
        instance_.startupStatus_.markDone(
          instance_.startupStatus_.CREATE_WIDGETS);
      }
    });
    instance_.startupStatus_.dispose();
    for (int i= 0; i < instance_.tabbedPane_.getTabCount(); i++) {
      ((FileFollowingPane)instance_.tabbedPane_.getComponentAt(i))
        .startFollowing();
    }
  }
  public static FollowApp openFollowApp(final String[] args,final boolean _show)
    throws IOException, InterruptedException, InvocationTargetException {
    final FollowApp instance= new FollowApp(args);
    if(_show) {
      instance.show();
    }
    instance.startupStatus_.markDone(instance.startupStatus_.CREATE_WIDGETS);
    instance.startupStatus_.dispose();
    for (int i= 0; i < instance.tabbedPane_.getTabCount(); i++) {
      ((FileFollowingPane)instance.tabbedPane_.getComponentAt(i))
        .startFollowing();
    }
    return instance;
  }
  About about_;
  FollowAppAttributes attributes_;
  Bottom bottom_;
  Clear clear_;
  ClearAll clearAll_;
  Close close_;
  Configure configure_;
  int currentCursor_= Cursor.DEFAULT_CURSOR;
  Debug debug_;
  Cursor defaultCursor_;
  Delete delete_;
  DeleteAll deleteAll_;
  Edit edit_;
  Exit exit_;
  Map fileToFollowingPaneMap_= new HashMap();
  JFrame frame_;
  // Actions
  Open open_;
  PopupMenu popupMenu_;
  Reload reload_;
  ReloadFromBeginning reloadStart_;
  ResourceBundle resBundle_=
    ResourceBundle.getBundle("ghm.followgui.FollowAppResourceBundle");
  private MouseListener rightClickListener_;
  StartupStatus startupStatus_;
  SystemInterface systemInterface_;
  JTabbedPane tabbedPane_;
  ToolBar toolBar_;
  Top top_;
  Cursor waitCursor_;
  /**
   * @param fileNames names of files to be opened
   */
  FollowApp(final String[] fileNames)
    throws IOException, InterruptedException, InvocationTargetException {
    // Create & show startup status window
    startupStatus_= new StartupStatus(resBundle_);
    centerWindowInScreen(startupStatus_);
    startupStatus_.pack();
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        startupStatus_.setVisible(true);
      }
    });
    // Ghastly workaround for bug in Font construction, in review by
    // Sun with review id 108683.
    GraphicsEnvironment
      .getLocalGraphicsEnvironment()
      .getAvailableFontFamilyNames();
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        startupStatus_.markDone(startupStatus_.LOAD_SYSTEM_FONTS);
      }
    });
    // create frame first
    frame_= new JFrame(resBundle_.getString("frame.title"));
    // initialize attributes
    attributes_= new FollowAppAttributes(this);
    for (int i= 0; i < fileNames.length; i++) {
      final File file= new File(fileNames[i]);
      if (!file.exists()) {
       /* final String msg=
          MessageFormat.format(
            resBundle_.getString("message.cmdLineFileNotFound.text"),
            new Object[] { file });*/
      } else if (!attributes_.followedFileListContains(file)) {
        attributes_.addFollowedFile(file);
      }
    }
    // initialize actions
    open_= new Open(this);
    close_= new Close(this);
    reload_= new Reload(this);
    reloadStart_= new ReloadFromBeginning(this);
    edit_= new Edit(this);
    exit_= new Exit(this);
    top_= new Top(this);
    bottom_= new Bottom(this);
    clear_= new Clear(this);
    clearAll_= new ClearAll(this);
    delete_= new Delete(this);
    deleteAll_= new DeleteAll(this);
    configure_= new Configure(this);
    about_= new About(this);
    if (DEBUG) {
      debug_= new Debug(this);
    }
    // initialize SystemInterface
    systemInterface_= new DefaultSystemInterface(this);
    // initialize menubar
    final Menu fileMenu=
      new Menu(
        resBundle_.getString("menu.File.name"),
        resBundle_.getString("menu.File.mnemonic"));
    fileMenu.addFollowAppAction(open_);
    fileMenu.addFollowAppAction(close_);
    fileMenu.addFollowAppAction(reload_);
    fileMenu.addFollowAppAction(reloadStart_);
    fileMenu.addSeparator();
    fileMenu.addFollowAppAction(exit_);
    final Menu toolsMenu=
      new Menu(
        resBundle_.getString("menu.Tools.name"),
        resBundle_.getString("menu.Tools.mnemonic"));
    toolsMenu.addFollowAppAction(top_);
    toolsMenu.addFollowAppAction(bottom_);
    toolsMenu.addSeparator();
    toolsMenu.addFollowAppAction(clear_);
    toolsMenu.addFollowAppAction(clearAll_);
//    toolsMenu.addFollowAppAction(delete_);
//    toolsMenu.addFollowAppAction(deleteAll_);
    toolsMenu.addSeparator();
    toolsMenu.addFollowAppAction(configure_);
    toolsMenu.addFollowAppAction(edit_);
    final Menu helpMenu=
      new Menu(
        resBundle_.getString("menu.Help.name"),
        resBundle_.getString("menu.Help.mnemonic"));
    helpMenu.addFollowAppAction(about_);
    if (DEBUG) {
      helpMenu.addSeparator();
      helpMenu.addFollowAppAction(debug_);
    }
    final JMenuBar jMenuBar= new JMenuBar();
    jMenuBar.add(fileMenu);
    jMenuBar.add(toolsMenu);
    jMenuBar.add(new CharsetMenu(this));
    jMenuBar.add(helpMenu);
    // initialize popupMenu
    popupMenu_= new PopupMenu();
    popupMenu_.setInvoker(frame_);
    popupMenu_.addFollowAppAction(open_);
    popupMenu_.addFollowAppAction(close_);
    popupMenu_.addFollowAppAction(reload_);
    popupMenu_.addFollowAppAction(reloadStart_);
    popupMenu_.addSeparator();
    popupMenu_.addFollowAppAction(top_);
    popupMenu_.addFollowAppAction(bottom_);
    popupMenu_.addSeparator();
    popupMenu_.addFollowAppAction(clear_);
    popupMenu_.addFollowAppAction(clearAll_);
    //popupMenu_.addFollowAppAction(delete_);
    //popupMenu_.addFollowAppAction(deleteAll_);
    popupMenu_.addSeparator();
    popupMenu_.addFollowAppAction(configure_);
    popupMenu_.addFollowAppAction(edit_);
    // initialize toolbar
    toolBar_= new ToolBar();
    toolBar_.addFollowAppAction(open_);
    toolBar_.addSeparator();
    toolBar_.addFollowAppAction(top_);
    toolBar_.addFollowAppAction(bottom_);
    toolBar_.addSeparator();
    toolBar_.addFollowAppAction(clear_);
    toolBar_.addFollowAppAction(clearAll_);
    //toolBar_.addFollowAppAction(delete_);
    //toolBar_.addFollowAppAction(deleteAll_);
    toolBar_.addSeparator();
    toolBar_.addFollowAppAction(configure_);
    // initialize tabbedPane, but wait to open files until after frame
    // initialization
    tabbedPane_= new TabbedPane(attributes_.getTabPlacement());
    enableDragAndDrop(tabbedPane_);
    // initialize frame
    frame_.setJMenuBar(jMenuBar);
    frame_.getContentPane().add(toolBar_, BorderLayout.NORTH);
    frame_.getContentPane().add(tabbedPane_, BorderLayout.CENTER);
    frame_.setSize(attributes_.getWidth(), attributes_.getHeight());
    // This is an ugly hack.  It seems like JFrame.setLocation() is buggy
    // on Solaris jdk versions before 1.4
    if (HAS_SOLARIS_BUG) {
      frame_.setLocation(50, 50);
    } else {
      frame_.setLocation(attributes_.getX(), attributes_.getY());
    }
    frame_.addWindowListener(new WindowTracker(attributes_));
    frame_.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent e) {
        try {
          attributes_.store();
        } catch (final IOException ioe) {
          System.err.println("Error encountered while storing properties...");
          ioe.printStackTrace(System.err);
        } finally {
          //on arrete les threads
          for (int i= 0; i < tabbedPane_.getTabCount(); i++) {
            ((FileFollowingPane)tabbedPane_.getComponentAt(i))
              .stopFollowing();
          }
          systemInterface_.exit(0);
        }
      }
      @Override
      public void windowClosing(final WindowEvent e) {
        if (tabbedPane_.getTabCount() > 0) {
          attributes_.setSelectedTabIndex(tabbedPane_.getSelectedIndex());
        }
        ((Window)e.getSource()).dispose();
      }
    });
    enableDragAndDrop(frame_);
    // Open files from attributes; this is done after the frame is complete
    // and all components have been added to it to make sure that the frame
    // can be shown absolutely as soon as possible. If we put this code
    // before frame creation (as in v1.0), frame creation may take longer
    // because there are more threads (spawned in the course of open())
    // contending for processor time.
    final Iterator i= attributes_.getFollowedFiles();
    StringBuffer nonexistentFilesBuffer= null;
    int nonexistentFileCount= 0;
    File file;
    while (i.hasNext()) {
      file= (File)i.next();
      if (file.exists()) {
        open(file, false, false);
      } else {
        // This file has been deleted since the previous execution. Remove it
        // from the list of followed files
        attributes_.removeFollowedFile(file);
        nonexistentFileCount++;
        if (nonexistentFilesBuffer == null) {
          nonexistentFilesBuffer= new StringBuffer(file.getAbsolutePath());
        } else {
          nonexistentFilesBuffer.append(file.getAbsolutePath());
        }
        nonexistentFilesBuffer.append(messageLineSeparator);
      }
    }
    if (nonexistentFileCount > 0) {
      // Alert the user of the fact that one or more files have been
      // deleted since the previous execution
      final String message=
        MessageFormat.format(
          resBundle_.getString("message.filesDeletedSinceLastExecution.text"),
          new Object[] {
            new Long(nonexistentFileCount),
            nonexistentFilesBuffer.toString()});
      JOptionPane.showMessageDialog(
        frame_,
        message,
        resBundle_.getString("message.filesDeletedSinceLastExecution.title"),
        JOptionPane.WARNING_MESSAGE);
    }
    if (tabbedPane_.getTabCount() > 0) {
      if (tabbedPane_.getTabCount() > attributes_.getSelectedTabIndex()) {
        tabbedPane_.setSelectedIndex(attributes_.getSelectedTabIndex());
      } else {
        tabbedPane_.setSelectedIndex(0);
      }
    } else {
      close_.setEnabled(false);
      reload_.setEnabled(false);
      edit_.setEnabled(false);
      top_.setEnabled(false);
      bottom_.setEnabled(false);
      clear_.setEnabled(false);
      clearAll_.setEnabled(false);
      delete_.setEnabled(false);
      deleteAll_.setEnabled(false);
    }
  }
  void disableDragAndDrop(final Component c) {
    c.setDropTarget(null);
  }
  void enableDragAndDrop(final Component c) {
    // Invoking this constructor automatically sets the component's drop target
    new DropTarget(c, new DndFileOpener(this));
  }
  List getAllFileFollowingPanes() {
    final int tabCount= tabbedPane_.getTabCount();
    final List allFileFollowingPanes= new ArrayList();
    for (int i= 0; i < tabCount; i++) {
      allFileFollowingPanes.add(tabbedPane_.getComponentAt(i));
    }
    return allFileFollowingPanes;
  }
  public JFrame getFrame() {
    return frame_;
  }
  // Lazy initializer for the right-click listener which invokes a popup menu
  private MouseListener getRightClickListener() {
    if (rightClickListener_ == null) {
      rightClickListener_= new MouseAdapter() {
        @Override
        public void mouseReleased(final MouseEvent e) {
          if (SwingUtilities.isRightMouseButton(e)) {
            final Component source= e.getComponent();
            popupMenu_.show(source, e.getX(), e.getY());
          }
        }
      };
    }
    return rightClickListener_;
  }
  FileFollowingPane getSelectedFileFollowingPane() {
    return (FileFollowingPane)tabbedPane_.getSelectedComponent();
  }
  void open(final File file, final boolean addFileToAttributes) {
    open(file, addFileToAttributes, true);
  }
  /* 
  Warning: This method should be called only from (1) the FollowApp 
  initializer (before any components are realized) or (2) from the event 
  dispatching thread. 
  */
  void open(final File file, final boolean addFileToAttributes, final boolean startFollowing) {
    FileFollowingPane fileFollowingPane=
      (FileFollowingPane)fileToFollowingPaneMap_.get(file);
    if (fileFollowingPane != null) {
      // File is already open; merely select its tab
      tabbedPane_.setSelectedComponent(fileFollowingPane);
    } else {
      fileFollowingPane=
        new FileFollowingPane(
          file,
          attributes_.getBufferSize(),
          attributes_.getLatency(),
          attributes_.autoScroll());
      final JTextArea ffpTextArea= fileFollowingPane.getTextArea();
      enableDragAndDrop(ffpTextArea);
      ffpTextArea.setFont(attributes_.getFont());
      ffpTextArea.addMouseListener(getRightClickListener());
      fileToFollowingPaneMap_.put(file, fileFollowingPane);
      if (startFollowing) {
        fileFollowingPane.startFollowing();
      }
      tabbedPane_.addTab(
        file.getName(),
        null,
        fileFollowingPane,
        file.getAbsolutePath());
      tabbedPane_.setSelectedIndex(tabbedPane_.getTabCount() - 1);
      if (!close_.isEnabled()) {
        close_.setEnabled(true);
        reload_.setEnabled(true);
        edit_.setEnabled(true);
        top_.setEnabled(true);
        bottom_.setEnabled(true);
        clear_.setEnabled(true);
        clearAll_.setEnabled(true);
        delete_.setEnabled(true);
        deleteAll_.setEnabled(true);
      }
      if (addFileToAttributes) {
        attributes_.addFollowedFile(file);
      }
    }
  }
  public void openFile(
    final File file,
    final boolean addFileToAttributes,
    final boolean startFollowing) {
    if (SwingUtilities.isEventDispatchThread()) {
      open(file, addFileToAttributes, startFollowing);
    } else {
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          @Override
          public void run() {
            open(file, addFileToAttributes, startFollowing);
          }
        });
      } catch (final InterruptedException e) {
        e.printStackTrace();
      } catch (final InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
  public void setCloseFrameExitJvm(final boolean _b) {
    if (_b) {
      systemInterface_= new DefaultSystemInterface(this);
    } else {
      systemInterface_= new DefaultNoExitSystemInterface(this);
    }
  }
  /**
  Warning: This method should be called only from the event 
  dispatching thread. 
  @param cursorType may be Cursor.DEFAULT_CURSOR or Cursor.WAIT_CURSOR
  */
  void setCursor(final int cursorType) {
    if (cursorType == currentCursor_) {
      return;
    }
    switch (cursorType) {
      case Cursor.DEFAULT_CURSOR :
        if (defaultCursor_ == null) {
          defaultCursor_= Cursor.getDefaultCursor();
        }
        frame_.setCursor(defaultCursor_);
        break;
      case Cursor.WAIT_CURSOR :
        if (waitCursor_ == null) {
          waitCursor_= Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
        }
        frame_.setCursor(waitCursor_);
        break;
      default :
        throw new IllegalArgumentException("Supported cursors are Cursor.DEFAULT_CURSOR and Cursor.WAIT_CURSOR");
    }
    currentCursor_= cursorType;
  }
  public void show() {
    frame_.setVisible(true);
  }
}
