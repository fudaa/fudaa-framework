/* 
Copyright (C) 2000-2003 Greg Merrill (greghmerrill@yahoo.com)

This file is part of Follow (http://follow.sf.net).

Follow is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public
License as published by the Free Software Foundation.

Follow is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Follow; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package ghm.followgui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
Implementation of <code>java.awt.dnd.DropTargetListener</code> which
opens files dropped on the Follow application's tabbed pane.

@author <a href="mailto:greghmerrill@yahoo.com">Greg Merrill</a>
*/
class DndFileOpener implements DropTargetListener {

  DndFileOpener (final FollowApp app) {
    app_ = app;
  }
  
  /**
  If the DropTargetDropEvent's DataFlavor is javaFileListFlavor, it opens
  the List of dropped files in the Follow application. No other DataFlavors 
  are supported.
  @param e "drop" event
  @see java.awt.dnd.DropTargetListener#drop(DropTargetDropEvent)
  */
  @Override
  public void drop (final DropTargetDropEvent e) {
    final DataFlavor[] flavors = e.getCurrentDataFlavors();
    final int numFlavors = (flavors != null) ? flavors.length : 0;
    for (int i=0; i < numFlavors; i++) {
      // Ignore all flavors except javaFileListType
      if (flavors[i].isFlavorJavaFileListType()) {
        e.acceptDrop(DnDConstants.ACTION_COPY);
        boolean dropCompleted = false;
        final Transferable transferable = e.getTransferable();
        try {
          final List fileList = (List)transferable.getTransferData(flavors[i]);
          final Iterator iterator = fileList.iterator();
          while (iterator.hasNext()) {
            app_.open((File)iterator.next(), true);
          }
          dropCompleted = true;
        }
        catch (final UnsupportedFlavorException ufException) { /* do nothing */ }
        catch (final IOException ioException) { /* do nothing */ }
        finally { e.dropComplete(dropCompleted); }
      }
    }
  }

  /** Does nothing. */
  @Override
  public void dragEnter (final DropTargetDragEvent e) {}

  /** Does nothing. */
  @Override
  public void dragOver (final DropTargetDragEvent e) {}

  /** Does nothing. */
  @Override
  public void dragExit (final DropTargetEvent e) {}

  /** Does nothing. */
  public void dragScroll (final DropTargetDragEvent e) {}

  /** Does nothing. */
  @Override
  public void dropActionChanged (final DropTargetDragEvent e) {}

  FollowApp app_;
  
}

