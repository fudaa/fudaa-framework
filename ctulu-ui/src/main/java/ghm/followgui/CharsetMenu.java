/**
 * @creation 30 sept. 2004
 * @modification $Date: 2006-09-19 14:38:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package ghm.followgui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.SortedMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * @author Fred Deniger
 * @version $Id: CharsetMenu.java,v 1.4 2006-09-19 14:38:43 deniger Exp $
 */
public class CharsetMenu extends JMenu implements ItemListener, ActionListener {

  @Override
  public void actionPerformed(final ActionEvent _e){
    if (app_.getSelectedFileFollowingPane() != null) {
      app_.getSelectedFileFollowingPane().getFileFollower().setEncoding(_e.getActionCommand());
      //      if (app_.getSelectedFileFollowingPane() != null)
    }

  }

  FollowApp app_;

  @Override
  public void itemStateChanged(final ItemEvent _e){
    if (_e.getSource() == this) {
      if (getMenuComponentCount() == 0) {
        build();
      } else {
        update();
      }
    }
  }

  public CharsetMenu(final FollowApp _app) {
    final ResourceBundle b=ResourceBundle.getBundle("ghm.followgui.FollowAppResourceBundle");
    setText(b.getString(
        "menu.Charset.name"));
    setMnemonic(b.getString("menu.Charset.mnemonic").charAt(0));
    addItemListener(this);
    app_ = _app;
  }

  private void update(){
    String c = null;
    if (app_.getSelectedFileFollowingPane() != null) {
      c = app_.getSelectedFileFollowingPane().getFileFollower().getEncoding().name();
    }
    for (int i = getMenuComponentCount() - 1; i >= 0; i--) {
      if (getMenuComponent(i) instanceof JMenu) {
        updateMenu((JMenu) getMenuComponent(i), c);
      }
      else if (getMenuComponent(i) instanceof JMenuItem) {
        final JMenuItem it = (JMenuItem) getMenuComponent(i);
        it.setSelected(it.getActionCommand().equals(c));
      }

    }
  }

  private static void updateMenu(final JMenu _m,final String actionCommand){
    for (int i = _m.getMenuComponentCount() - 1; i >= 0; i--) {
      if (_m.getMenuComponent(i) instanceof JMenuItem) {
        final JMenuItem it = (JMenuItem) _m.getMenuComponent(i);
        it.setSelected(it.getActionCommand().equals(actionCommand));
      }
    }

  }

  private void build(){
    removeAll();
    Charset c = null;
    if (app_.getSelectedFileFollowingPane() != null) {
      c = app_.getSelectedFileFollowingPane().getFileFollower().getEncoding();
    }
    final SortedMap m = Charset.availableCharsets();
    final String[] startS = new String[] { "ISO", "x-", "windows"};
    final JMenu[] startMenu = new JMenu[3];
    final Charset defCharset = Charset.forName(System.getProperty("file.encoding"));
    final JMenuItem itemDefault = new JCheckBoxMenuItem(ResourceBundle.getBundle(
        "ghm.followgui.FollowAppResourceBundle").getString("menu.Charset.default")
      + " (" + defCharset.displayName() + ")");
    itemDefault.setActionCommand(defCharset.name());
    add(itemDefault);
    itemDefault.setSelected(defCharset == c);
    itemDefault.addActionListener(this);
    addSeparator();
    for (final Iterator it = m.values().iterator(); it.hasNext();) {

      final Charset e = (Charset) it.next();
      final String s = e.displayName();
      final JMenuItem item = new JCheckBoxMenuItem(s);
      item.setActionCommand(e.name());
      if (e == c) {
        item.setSelected(true);
      }
      item.addActionListener(this);
      boolean add = false;
      for (int i = startS.length - 1; i >= 0; i--) {
        final String deb = startS[i];
        if (s.startsWith(deb)) {
          if (startMenu[i] == null) {
            startMenu[i] = new JMenu(deb + " ...");
            startMenu[i].setActionCommand("menu" + deb);
            add(startMenu[i]);
          }
          startMenu[i].add(item);
          add = true;

        }
      }
      if (!add) {
        add(item);
      //      if(e.getValue()==c) item.setSelected(true);
      }
    }
  }

}