/* 
 Copyright (C) 2000-2003 Greg Merrill (greghmerrill@yahoo.com)

 This file is part of Follow (http://follow.sf.net).

 Follow is free software; you can redistribute it and/or modify
 it under the terms of version 2 of the GNU General Public
 License as published by the Free Software Foundation.

 Follow is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Follow; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package ghm.follow;

import java.util.ResourceBundle;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Implementation of {@link OutputDestination} which appends Strings to a {@link JTextArea}.
 * 
 * @see OutputDestination
 * @see JTextArea
 * @author <a href="mailto:greghmerrill@yahoo.com">Greg Merrill</a>
 */
public class JTextAreaDestination implements OutputDestination {

  /**
   * Construct a new JTextAreaDestination.
   * 
   * @param jTextArea text will be appended to this text area
   * @param autoPositionCaret if true, caret will be automatically moved to the bottom of the text area when text is
   *          appended
   */
  public JTextAreaDestination(final JTextArea jTextArea, final boolean autoPositionCaret) {
    jTextArea_ = jTextArea;
    autoPositionCaret_ = autoPositionCaret;
  }

  public JTextArea getJTextArea() {
    return jTextArea_;
  }

  public void setJTextArea(final JTextArea jTextArea) {
    jTextArea_ = jTextArea;
  }

  /**
   * @return whether caret will be automatically moved to the bottom of the text area when text is appended
   */
  public boolean autoPositionCaret() {
    return autoPositionCaret_;
  }

  /**
   * @param autoPositionCaret if true, caret will be automatically moved to the bottom of the text area when text is
   *          appended
   */
  public void setAutoPositionCaret(final boolean autoPositionCaret) {
    autoPositionCaret_ = autoPositionCaret;
  }
  // 200 lignes de 80 caractere: pour �viter les outofMemory
  int nbMaxChar_ = 200 * 80;
  boolean isTruncated_ = false;

  @Override
  public void print(final String s) {
    if (s == null) return;
    final Document doc = jTextArea_.getDocument();
    int docLength = doc.getLength();
    int sLength = s.length();
    // si le contenu est trop grand
    if (docLength + sLength >= nbMaxChar_) {
      // on tronque le contenu
      isTruncated_ = true;
      int nbCharToBeLeft = nbMaxChar_ - sLength;
      // on peut laisser des caract�res: la nouvelle chaine n'est pas trop grande
      // dans ce cas on est sur qu'il y a du texte d
      if (nbCharToBeLeft >= 0) {
        // s'il y a du texte dans le jtextarea et si on peut laisser des caract�res
        StringBuffer buf = new StringBuffer(nbMaxChar_);
        // si s a la meme taille que le max autoris�: ce n'est pas la peine de reprendre l'ancien
        // texte
        if (nbCharToBeLeft > 0) {
          int min = Math.max(0, docLength - nbCharToBeLeft);
          try {
            String newText = getTextFromFirstLineSep(doc.getText(min, docLength - min));
            buf.append(newText);
          } catch (BadLocationException _evt) {
            _evt.printStackTrace();

          }
        }
        buf.append(s);
        clear();
        jTextArea_.append(buf.toString());

        // s est trop grand: on efface et on ajoute ce qui est autoris�
        // dans ce cas nbCharToBeLeft est strictement n�gatif et correspond a ce qu'il faut enlever
      } else {
        clear();
        jTextArea_.append(getTextFromFirstLineSep(s.substring(-nbCharToBeLeft, sLength - 1)));

      }
    } else {
      jTextArea_.append(s);

    }
    doAutoPosition();
  }

  private String getTextFromFirstLineSep(final String newText) {
    final int idx = newText.indexOf('\n');
    if (idx > 0 && idx < newText.length() - 5) {
      return newText.substring(idx+1);
    }
    return newText;
  }

  private void doAutoPosition() {
    if (autoPositionCaret_) {
      jTextArea_.setCaretPosition(jTextArea_.getDocument().getLength());
    }
  }
  ResourceBundle bundle_;

  protected ResourceBundle getBundle() {
    if (bundle_ == null) {
      bundle_ = ResourceBundle.getBundle("ghm.followgui.FollowAppResourceBundle");
    }
    return bundle_;
  }

  @Override
  public void clear() {
    jTextArea_.setText(isTruncated_ ? ("!!!" + getBundle().getString("message.file.begin.skip") + "\n") : "");
    doAutoPosition();
  }
  protected JTextArea jTextArea_;
  protected boolean autoPositionCaret_;

}
