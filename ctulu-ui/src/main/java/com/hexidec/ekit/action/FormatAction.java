/*
 GNU Lesser General Public License

 FormatAction
 Copyright (C) 2000 Howard Kistler

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package com.hexidec.ekit.action;

import com.hexidec.ekit.EkitCore;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledEditorKit;

/**
 * Class for implementing HTML format actions (NOTE : Does not toggle. User must use the "Clear Format" option to remove
 * formatting correctly.)
 */
public class FormatAction extends StyledEditorKit.StyledTextAction {
  protected EkitCore parentEkit;
  Object htmlTag;

  public FormatAction(EkitCore ekit, String actionName, Object _style) {
    super(actionName);
    parentEkit = ekit;
    htmlTag = _style;
  }

  @Override
  public void actionPerformed(ActionEvent ae) {
    JTextPane parentTextPane = parentEkit.getTextPane();
    String selText = parentTextPane.getSelectedText();
    int textLength = -1;
    if (selText != null) {
      textLength = selText.length();
    }
    if (selText == null || textLength < 1) {
      return;
    }
    JEditorPane editor = getEditor(ae);
    if (editor != null) {
      StyledEditorKit kit = getStyledEditorKit(editor);
      MutableAttributeSet attr = kit.getInputAttributes();
      boolean newValue = (attr.getAttribute(htmlTag) == Boolean.TRUE) ? false : true;
      SimpleAttributeSet sas = new SimpleAttributeSet();
      sas.addAttribute(htmlTag, Boolean.valueOf(newValue));
      //StyleConstants.setUnderline(sas, newValue);
      setCharacterAttributes(editor, sas, false);
    }
  }
}
