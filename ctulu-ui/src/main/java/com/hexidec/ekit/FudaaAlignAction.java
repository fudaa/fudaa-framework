/*
 * @creation 14 f�vr. 07
 * @modification $Date: 2007-03-23 17:16:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package com.hexidec.ekit;

import java.awt.event.ActionEvent;
import java.util.Enumeration;
import javax.swing.AbstractAction;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTML.Tag;

/**
 * @author fred deniger
 * @version $Id: FudaaAlignAction.java,v 1.2 2007-03-23 17:16:17 deniger Exp $
 */
public class FudaaAlignAction extends AbstractAction {

  String a_;
  final EkitCore core_;

  Tag p_ = HTML.Tag.P;

  public FudaaAlignAction(EkitCore _core, String _nm, String _a) {
    super(_nm);
    a_ = _a;
    core_ = _core;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JTextPane parentTextPane = core_.getTextPane();
    SimpleAttributeSet sasText = new SimpleAttributeSet(parentTextPane.getCharacterAttributes());
    SimpleAttributeSet sasPara = new SimpleAttributeSet(parentTextPane.getParagraphAttributes());
    String selText = parentTextPane.getSelectedText();
    int textLength = -1;

    if (selText != null) {
      textLength = selText.length();
    }

    /*
     * if (selText == null || textLength < 1) { return; }
     */

    boolean alignFound = false;
    for (Enumeration en = sasPara.getAttributeNames(); en.hasMoreElements();) {
      Object elm = en.nextElement();
      if (elm.equals(HTML.Attribute.ALIGN)) {
        alignFound = true;
      }
    }
    int caretOffset = parentTextPane.getSelectionStart();
    int internalTextLength = selText == null ? 0 : selText.length();
    if (alignFound) {
      sasPara.addAttribute(HTML.Attribute.ALIGN, a_);
      if (textLength > 0) parentTextPane.select(caretOffset, caretOffset + textLength);
      parentTextPane.setParagraphAttributes(sasPara, true);
    } else if (textLength < 0) {
      if (sasPara.getAttribute(StyleConstants.NameAttribute) == Tag.IMPLIED) sasPara.addAttribute(
          StyleConstants.NameAttribute, Tag.P);
      sasPara.addAttribute(HTML.Attribute.ALIGN, a_);
      parentTextPane.setParagraphAttributes(sasPara, true);
    } else {

      sasText.removeAttribute(HTML.Tag.P);
      SimpleAttributeSet attribs = new SimpleAttributeSet();
      attribs.addAttribute(HTML.Attribute.ALIGN, a_);
      parentTextPane.setParagraphAttributes(sasPara, true);
      sasText.addAttribute(p_, attribs);
      if (textLength > 0) parentTextPane.select(caretOffset, caretOffset + textLength);
      parentTextPane.setCharacterAttributes(sasText, true);
    }

    core_.refreshOnUpdate();
    if (textLength > 0) parentTextPane.select(caretOffset, caretOffset + internalTextLength);
  }
}
