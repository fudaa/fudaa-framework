/**
 * @creation 21 juin 2004
 * @modification $Date: 2007-01-17 10:45:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.visuallibrary.example;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.iterator.LogarithmicNumberIterator;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionAlign;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionColorBackground;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionColorForeground;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionConfigure;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionDuplicate;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionFont;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionImageChooser;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionMoveToBack;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionMoveToFirst;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionRetaillageHorizontal;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionRetaillageVertical;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorArrow;
import org.fudaa.ebli.visuallibrary.tree.EbliWidgetJXTreeTableModel;
import org.jdesktop.swingx.JXTreeTable;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.TwoStateHoverProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.router.RouterFactory;
import org.netbeans.api.visual.widget.ComponentWidget;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

/**
 * @author Fred Deniger
 * @version $Id: TestGraphe2.java,v 1.8 2007-01-17 10:45:17 deniger Exp $
 */
public final class VisualLibraryExample {

  private VisualLibraryExample() {
    super();
  }

  private static EGGraphe createGraphe() {
    final EGGrapheTreeModel grapheModel = new EGGrapheTreeModel();
    final EGGraphe g = new EGGraphe(grapheModel);
    final EGAxeHorizontal x = new EGAxeHorizontal(false);
    x.setTitre("temps");
    x.setUnite("sec");
    x.setBounds(0.0001, 1000000);
    x.setAxisIterator(new LogarithmicNumberIterator());
    x.setGraduations(true);
    g.setXAxe(x);
    EGGroup gr = new EGGroup();
    gr.setTitle("gr 1");
    EGCourbeModelDefault m = new EGCourbeModelDefault(new double[] { 0.0001, 3, 4, 1000000 }, new double[] { 10000, 4, 5, 3 });
    EGCourbeChild c = new EGCourbeChild(gr);
    m.setTitle("toto bleue");
    c.setModel(m);
    c.setAspectContour(Color.cyan);

    gr.addEGComponent(c);

    m = new EGCourbeModelDefault(new double[] { 0.0002, 5, 7, 900000 }, new double[] { 0.001, 1, 3, 4 });
    c = new EGCourbeChild(gr);
    c.setAspectContour(Color.RED);
    m.setTitle("toto rouge");
    c.setModel(m);

    gr.addEGComponent(c);

    EGAxeVertical y = new EGAxeVertical();
    y.setGraduations(true);
    y.setTraceGraduations(new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    y.setDroite(true);
    y.setAxisIterator(new LogarithmicNumberIterator());
    y.setBounds(0.0001, 10000);
    y.setLineColor(Color.blue);
    y.setTitre("essai 2");
    gr.setAxeY(y);
    grapheModel.add(gr);
    gr = new EGGroup();
    gr.setTitle("gr 2");
    m = new EGCourbeModelDefault(new double[] { 1, 8, 9, 10 }, new double[] { 10, 4, 2, 24 });
    c = new EGCourbeChild(gr);
    c.setAspectContour(Color.yellow);
    m.setTitle("toto jaune");
    c.setModel(m);
    gr.addEGComponent(c);
    y = new EGAxeVertical();
    y.setGraduations(true);
    y.setBounds(0, 55);
    y.setTitre("essai 1");
    gr.setAxeY(y);
    gr.addEGComponent(c);
    m = new EGCourbeModelDefault(new double[] { 1, 3, 4, 5 }, new double[] { 14, 54, 25, 43 });
    c = new EGCourbeChild(gr);
    m.setTitle("toto vert");
    c.setAspectContour(Color.green);
    c.setModel(m);
    gr.addEGComponent(c);
    grapheModel.add(gr);
    return g;
  }

  /**
   * @param _args non utilise
   */
  public static void main(final String[] _args) {

    // creation de la scene EBLI
    final EbliScene scene = new EbliScene();

    // -- construction du jtree
    ;

    // -- ajout du rectangle --//
    //    final EbliNode nodeRect = new EbliNodeDefault();
    //    nodeRect.setTitle("Rectangle");
    //    nodeRect.setCreator(new EbliWidgetCreatorTextLabel("Test recangle"));
    //    nodeRect.getCreator().setPreferredSize(new Dimension(200, 100));
    //    nodeRect.getCreator().setPreferredLocation(new Point(350, 125));
    //    scene.addNode(nodeRect);
    //    
    //
    //    EbliNodeDefault nodeFleche = new EbliNodeDefault();
    //    nodeFleche.setTitle("CONNECTOR");
    //    nodeFleche.setCreator(new EbliWidgetCreatorTest());
    //    scene.addNode(nodeFleche);
    //
    EbliNodeDefault nodeFleche = new EbliNodeDefault();
    nodeFleche.setTitle("CONNECTOR_BIS");
    nodeFleche.setCreator(new EbliWidgetCreatorArrow());
    scene.addNode(nodeFleche);
    scene.refresh();

//    LayerWidget mainLayer = new LayerWidget(scene);
//    scene.addChild(mainLayer);
//    LayerWidget connLayer = new LayerWidget(scene);
//    scene.addChild(connLayer);
//
//    addLabel(scene, mainLayer, "Double-click on the connection to create a control point", 10, 30);
//    addLabel(scene, mainLayer, "Drag a control point to move it", 10, 60);
//    addLabel(scene, mainLayer, "Double-click on a control point to delete it", 10, 90);
//
//    LabelWidget hello1 = addLabel(scene, mainLayer, "Hello", 100, 150);
//    LabelWidget hello2 = addLabel(scene, mainLayer, "NetBeans", 300, 250);
//
//    ConnectionWidget conn = new ConnectionWidget(scene);
//    conn.setPaintControlPoints(true);
//    conn.setControlPointShape(PointShape.SQUARE_FILLED_BIG);
//    conn.setRouter(RouterFactory.createFreeRouter());
//    conn.setSourceAnchor(AnchorFactory.createFreeRectangularAnchor(hello1, true));
//    conn.setTargetAnchor(AnchorFactory.createFreeRectangularAnchor(hello2, true));
//    connLayer.addChild(conn);
    LayerWidget mainLayer = new LayerWidget (scene);
    scene.addChild (mainLayer);
    LayerWidget connectionLayer = new LayerWidget (scene);
    scene.addChild (connectionLayer);
    WidgetAction action = ActionFactory.createHoverAction (new MyHoverProvider (scene));
    scene.getActions ().addAction (action);

    LabelWidget sourceNode = new LabelWidget (scene, "Source");
    sourceNode.setBorder (BorderFactory.createLineBorder ());
    sourceNode.setOpaque (true);
    mainLayer.addChild (sourceNode);
    sourceNode.getActions ().addAction (action);
    sourceNode.getActions ().addAction (ActionFactory.createMoveAction ());
    sourceNode.setPreferredLocation (new Point (50, 100));

    LabelWidget targetNode = new LabelWidget (scene, "Target");
    targetNode.setBorder (BorderFactory.createLineBorder ());
    targetNode.setOpaque (true);
    mainLayer.addChild (targetNode);
    targetNode.getActions ().addAction (action);
    targetNode.getActions ().addAction (ActionFactory.createMoveAction ());
    targetNode.setPreferredLocation (new Point (350, 200));

    ConnectionWidget edge = new ConnectionWidget (scene);
    edge.setSourceAnchor (AnchorFactory.createDirectionalAnchor (sourceNode, AnchorFactory.DirectionalAnchorKind.HORIZONTAL));
    edge.setTargetAnchor (AnchorFactory.createDirectionalAnchor (targetNode, AnchorFactory.DirectionalAnchorKind.HORIZONTAL));
    edge.setRouter (RouterFactory.createOrthogonalSearchRouter (mainLayer));
    connectionLayer.addChild (edge);

    LabelWidget label1 = new LabelWidget (scene, "Source Top Label");
    label1.setOpaque (true);
    edge.addChild (label1);
    edge.setConstraint (label1, LayoutFactory.ConnectionWidgetLayoutAlignment.TOP_RIGHT, 10);
    label1.getActions ().addAction (action);

    LabelWidget label2 = new LabelWidget (scene, "Movable Edge Center Label");
    label2.setOpaque (true);
    label2.getActions ().addAction (ActionFactory.createMoveAction ());
    edge.addChild (label2);
    edge.setConstraint (label2, LayoutFactory.ConnectionWidgetLayoutAlignment.CENTER_RIGHT, 0.5f);
    label2.getActions ().addAction (action);

    LabelWidget label3 = new LabelWidget (scene, "Target Bottom Label");
    label3.setOpaque (true);
    edge.addChild (label3);
    edge.setConstraint (label3, LayoutFactory.ConnectionWidgetLayoutAlignment.BOTTOM_LEFT, -10);
    label3.getActions ().addAction (action);
//
//    conn.setRoutingPolicy(RoutingPolicy.ALWAYS_ROUTE);
//    conn.getActions().addAction(ActionFactory.createAddRemoveControlPointAction());
//    conn.getActions().addAction(ActionFactory.createFreeMoveControlPointAction());
//    conn.getActions().addAction(ActionFactory.createMoveAction());

    JTable table = new JTable();
    table.setModel(new DefaultTableModel(new Object[][] { { "11", "12" }, { "21", "22" } }, new Object[] { "First", "Second" }));
    ComponentWidget widgetJtable = new ComponentWidget(scene, new JScrollPane(table)) {
      @Override
      public void notifyStateChanged(ObjectState previousState, ObjectState newState) {

        setBorder(BorderFactory.createResizeBorder(10));

      }
    };
    widgetJtable.setBorder(BorderFactory.createResizeBorder(10));
    widgetJtable.setPreferredLocation(new Point(500, 300));
    widgetJtable.getActions().addAction(ActionFactory.createResizeAction());
    // scene.addChild(widgetJtable);

    final JFrame f = new JFrame();
    f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    f.setSize(new Dimension(1024, 768));
    final BuPanel p = new BuPanel();
    p.setLayout(new BuBorderLayout());
    EbliWidgetJXTreeTableModel tree = new EbliWidgetJXTreeTableModel(scene);

    JXTreeTable view = new JXTreeTable(tree);

    Box boxEast = Box.createVerticalBox();
    p.add(new JScrollPane(boxEast), BuBorderLayout.EAST);
    f.setLayout(new BorderLayout());

    f.add(p, BorderLayout.CENTER);
    // fin
    p.add(new JScrollPane(scene.createView()), BuBorderLayout.CENTER);

    // ajout de la vue sattelite en bas � gauche
    boxEast.add(scene.createSatelliteView());

    final JMenu menu = new JMenu();
    menu.setName("essai");
    menu.setText("essai");
    final JMenuBar b = new JMenuBar();
    b.add(menu);
    f.setJMenuBar(b);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // f.pack();
    f.setVisible(true);
    scene.setFont(CtuluLibSwing.getMiniFont());

    // -- ajout par la suite --/
    /*
     * node = new EbliNodeDefault(); node.setTitle("BOUDOUM"); node.setCreator(new
     * EbliWidgetCreatorRectangle("BOUDOUM"));
     * 
     * node.setD(new Dimension(100, 100)); node.setP(new Point(450,20)); scene.addNode(node);
     */

    JToolBar bar = new JToolBar();
    f.add(bar, BorderLayout.PAGE_START);
    // om met en place le undo/redo
    scene.setCmdMng(new CtuluCommandManager());
    bar.add(new EbliActionSimple("undo", BuResource.BU.getToolIcon("defaire"), "UNDO") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        if (scene.getCmdMng().canUndo())
          scene.getCmdMng().undo();
      }
    });
    bar.add(new EbliActionSimple("redo", BuResource.BU.getToolIcon("refaire"), "REDO") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        if (scene.getCmdMng().canRedo())
          scene.getCmdMng().redo();
      }
    });
    bar.addSeparator();
    bar.add(new EbliWidgetActionAlign.Left(scene));
    bar.add(new EbliWidgetActionAlign.Right(scene));
    bar.add(new EbliWidgetActionAlign.Middle(scene));
    bar.add(new EbliWidgetActionAlign.Center(scene));
    bar.add(new EbliWidgetActionAlign.Top(scene));
    bar.add(new EbliWidgetActionAlign.Bottom(scene));

    bar.add(new EbliWidgetActionConfigure(scene));

    /**
     * Action qui recupere les noeuds de l arbre selectionnes et indique la widget comme target.
     */
    /*
     * BConfigurePaletteAction action = new BConfigurePaletteAction(view.getTreeSelectionModel()) {
     * 
     * protected Object getTarget(final TreeSelectionModel _m) { final TreePath[] p = _m.getSelectionPaths();
     * 
     * if (p != null) { final Object[] targets = new Object[p.length]; // p est non null for (int i = p.length - 1; i >=
     * 0; i--) { targets[i] = p[i] == null ? null : ((EbliNode) ((EbliWidgetTreeTableNode) p[i].getLastPathComponent())
     * .getUserObject()).getWidget();
     * 
     * 
     * FuLog.warning("je suis la target selectionne title: " + targets[i].toString()); }
     * 
     * 
     * 
     * return targets; } return null; }
     * 
     * }; bar.add(action);
     */
    bar = new JToolBar();
    f.add(bar, BorderLayout.PAGE_END);

    bar.add(new EbliWidgetActionDuplicate(scene));

    bar.add(new EbliWidgetActionMoveToFirst(scene));

    bar.add(new EbliWidgetActionMoveToBack(scene));

    bar.add(new EbliWidgetActionColorForeground(scene));

    bar.add(new EbliWidgetActionColorBackground.ForScene(scene));

    bar.add(new EbliWidgetActionRetaillageHorizontal(scene, EbliWidgetActionRetaillageHorizontal.RETAIILLAGE_MIN));
    bar.add(new EbliWidgetActionRetaillageHorizontal(scene, EbliWidgetActionRetaillageHorizontal.RETAIILLAGE_MAX));
    bar.add(new EbliWidgetActionRetaillageVertical(scene, EbliWidgetActionRetaillageVertical.RETAIILLAGE_MIN));
    bar.add(new EbliWidgetActionRetaillageVertical(scene, EbliWidgetActionRetaillageVertical.RETAIILLAGE_MAX));

    bar.add(new EbliWidgetActionImageChooser(scene));

    // bar.add(new EbliWidgetActiontextEditor(scene));

    p.doLayout();

    // -- creation de l action pour les fonts --//
    EbliWidgetActionFont actionFont = new EbliWidgetActionFont(scene);

    // -- ajout de la combo des font dans la toolbar --//
    bar.add(actionFont.getFonts());

    bar.add(actionFont.getSizeFonts());

    boxEast.add(new BCalqueLegende());

  }
  
  private static class MyHoverProvider implements TwoStateHoverProvider {

    private Scene scene;

    public MyHoverProvider (Scene scene) {
        this.scene = scene;
    }

    @Override
    public void unsetHovering (Widget widget) {
        if (widget != null) {
            widget.setBackground (scene.getLookFeel ().getBackground (ObjectState.createNormal ()));
            widget.setForeground (scene.getLookFeel ().getForeground (ObjectState.createNormal ()));
        }
    }

    @Override
    public void setHovering (Widget widget) {
        if (widget != null) {
            ObjectState state = ObjectState.createNormal ().deriveSelected (true);
            widget.setBackground (scene.getLookFeel ().getBackground (state));
            widget.setForeground (scene.getLookFeel ().getForeground (state));
        }
    }
  }

  private static LabelWidget addLabel(Scene scene, LayerWidget mainLayer, String text, int x, int y) {
    LabelWidget widget = new LabelWidget(scene, text);

    widget.setFont(scene.getDefaultFont().deriveFont(24.0f));
    widget.setOpaque(true);
    widget.setPreferredLocation(new Point(x, y));

    widget.getActions().addAction(ActionFactory.createMoveAction());

    mainLayer.addChild(widget);

    return widget;
  }

}