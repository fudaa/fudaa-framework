package org.fudaa.ebli.visuallibrary.example;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.border.Border;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

public class TestWidgetHtml {

    /**
     * Empty border
     */
    public static final Border EMPTY_BORDER = BorderFactory.createEmptyBorder(8);
    /**
     * Resize border
     */
    public static final Border RESIZE_BORDER = BorderFactory.createResizeBorder(8, Color.BLACK, true);

    private static class HtmlViewWidget extends Widget {

        View view;

        protected HtmlViewWidget(Scene scene, String html) {
            super(scene);
            //TODO JLabel must be change to another component ...
            view = BasicHTML.createHTMLView(new JLabel(), html);
            super.setCheckClipping(true);
            getActions().addAction(scene.createWidgetHoverAction());
            getActions().addAction(ActionFactory.createResizeAction());
            getActions().addAction(ActionFactory.createMoveAction());

        }

        /**
         *
         */
        @Override
        protected Rectangle calculateClientArea() {
            Rectangle rec = new Rectangle();
            //the preferred size is here
            rec.height = (int) view.getPreferredSpan(View.Y_AXIS);
            rec.width = (int) view.getPreferredSpan(View.X_AXIS);
            return rec;
        }

        /**
         *
         */
        @Override
        protected void paintWidget() {
            Rectangle clientArea = getClientArea();
            view.paint(getGraphics(), clientArea);
        }

        @Override
        public void notifyStateChanged(ObjectState previousState, ObjectState newState) {
            setBorder(newState.isHovered() ? RESIZE_BORDER : EMPTY_BORDER);
        }

    }

    /**
     * @author deniger
     * @creation 12 sept. 2008
     * @version
     *
     */
    public static final class DefaultScene extends GraphScene.StringGraph {
        private int y = 10;
        final LayerWidget layer;

        /**
         * build the default layer
         */
        public DefaultScene() {
            layer = new LayerWidget(this);
            addChild(layer);
        }

        @Override
        protected void attachEdgeSourceAnchor(String edge, String oldSourceNode, String sourceNode) {
        }

        @Override
        protected void attachEdgeTargetAnchor(String edge, String oldTargetNode, String targetNode) {
        }

        @Override
        protected Widget attachEdgeWidget(String edge) {
            return null;
        }

        @Override
        protected Widget attachNodeWidget(String node) {
            Widget res = new HtmlViewWidget(this, node);
            res.setPreferredLocation(new Point(10, y));
            y += 100;
            layer.addChild(res);
            return res;
        }
    }

    /**
     * @param view the view
     * @return a panel with the view and a satellite view
     */
    private static JPanel buildContentPane(GraphScene.StringGraph view) {
        JPanel pn = new JPanel(new BorderLayout());
        pn.add(view.createView());
        pn.add(view.createSatelliteView(), BorderLayout.EAST);
        pn.setPreferredSize(new Dimension(600, 400));
        return pn;
    }

    /**
     * @param pn
     */
    private static void showFrameWithContentPane(JPanel pn) {
        JFrame test = new JFrame("Genesis");
        test.setContentPane(pn);
        test.pack();
        test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        test.setVisible(true);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        GraphScene.StringGraph view = new DefaultScene();
        //ici on se defoule, let's go
        view
                .addNode("<html><body>Bonjour<br>Au revoir<table border=\"1\"><tr><td>One</td><td>Two</td></tr><tr><td>Three</td><td>Four</td>");
        view
                .addNode("<html><body><p color=\"blue\">Hi</p><br><b color=\"red\">Red</b><br><u>Underline with long long long long blabla</u>");
        view.addNode("It's not html text but we can paint it and resize ");

        //        view.getActions().

        JPanel pn = buildContentPane(view);
        showFrameWithContentPane(pn);

    }
}

