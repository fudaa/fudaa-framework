package org.fudaa.ebli.visuallibrary.creator;

/**
 * Classe qui se charge de de loader la bonne classes en fonction du string passer en param
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorClassLoader {

  /**
   * Retourne une classe qui implemente EbliWidgetCreator sinon retourne null
   * 
   * @param className
   * @return
   * @throws ClassNotFoundException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  public static EbliWidgetCreator forName(String className) throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
//    final EbliWidgetCreator creator = null;
    if (className.startsWith("class ")) ;
    className = className.substring("class ".length());
    final Class myclass = Class.forName(className, true, Thread.currentThread().getContextClassLoader());
    final Object myCreator = myclass.newInstance();

    if (myCreator instanceof EbliWidgetCreator) return (EbliWidgetCreator) myCreator;
    else return null;
  }

}
