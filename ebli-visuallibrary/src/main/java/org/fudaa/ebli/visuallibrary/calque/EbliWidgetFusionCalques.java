package org.fudaa.ebli.visuallibrary.calque;

import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetGroup;
import org.fudaa.ebli.visuallibrary.EbliWidgetImageProducer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;

/**
 * Widget qui resulte de la fusion de calques
 *
 * @author Adrien Hadoux
 */
public final class EbliWidgetFusionCalques extends EbliWidgetGroup implements PropertyChangeListener, EbliWidgetImageProducer {
  public final ArrayList<EbliWidgetVueCalque> listeWidgetCalque_ = new ArrayList<EbliWidgetVueCalque>();
  boolean noCyclePropertieChange = true;

  public EbliWidgetFusionCalques(final EbliScene _scene) {
    super(_scene);
  }

  public void initZoom(GrBoite boite) {
    listeWidgetCalque_.forEach(layer -> layer.setZoom(new GrBoite(boite)));
  }

  public void addChildCalque(final EbliWidget child) {
    addChild(child);
    final EbliWidgetVueCalque widgetCalque = (EbliWidgetVueCalque) child.getIntern();
    registerVueListener(widgetCalque);
    registerGrillesListener(widgetCalque);
    listeWidgetCalque_.add(widgetCalque);
  }

  public void restaurer() {
    GrBoite boite = new GrBoite();
    listeWidgetCalque_.forEach(widget -> {
      final GrBoite domaine = widget.getCalquePanel().getVueCalque().getCalque().getDomaine();
      boite.ajuste(domaine);
    });
    getMainPanel().getVueCalque().setViewBoite(boite);
    synchronyseZoom();
  }

  @Override
  public BufferedImage produceImage(int width, int height, Map params) {
    final BufferedImage image = CtuluLibImage.createImage(width, height, params);
    for (EbliWidgetVueCalque vc : listeWidgetCalque_) {
      vc.getCalquePanel().paintOnImage(width, height, params, image);
    }
    return image;
  }

  /**
   * Methode a appelee a la fin des ajout de widget pour synchroniser les zooms de tous les calques du groupe. Il faut
   * le faire dans l'ordre inverse afin de préserver le zoom de la widget initiale.
   */
  public void synchronyseZoom() {

    // -- on met a jour le modele de zoom pour toutes les filles en prenant le
    // dernier widget (= la widget temoin)
    final ZEbliCalquesPanel pn = getMainPanel();
    // le zoom
    changeAllZoomFrom(pn.getVueCalque());
    // les espaces
    changeAllInsetsFrom(pn.getVueCalque());
    // les grilles
    changeAllGrilleFrom(pn.getVueCalque());

    getEbliScene().refresh();
  }

  public ZEbliCalquesPanel getMainPanel() {
    return getMainLayerWidget().calquePanel_;
  }

  public EbliWidgetVueCalque getMainLayerWidget() {
    return listeWidgetCalque_.get(listeWidgetCalque_.size() - 1);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    final boolean isRepere = "repere".equals(_evt.getPropertyName());
    // pour eviter de boucler indefiniment
    if (!noCyclePropertieChange) {
      return;
    }
    noCyclePropertieChange = false;
    final BVueCalque vueCalque = (BVueCalque) _evt.getSource();
    if (getVueInEdition() != vueCalque) {
      noCyclePropertieChange = true;
      return;
    }
    if (isRepere) {
      changeAllZoomFrom(vueCalque);
    } else {
      changeAllInsetsFrom(vueCalque);
    }
    noCyclePropertieChange = true;
    getEbliScene().refresh();
  }

  private PropertyChangeListener grillePropertyChangeListener = _evt -> propertyChangeFromGrille(_evt);

  public void propertyChangeFromGrille(final PropertyChangeEvent _evt) {
    final BVueCalque vueCalque = getVueCalque((ZCalqueGrille) _evt.getSource());
    if (getVueInEdition() != vueCalque) {
      return;
    }
    changeAllGrilleFrom(vueCalque);
    getEbliScene().refresh();
  }

  private BVueCalque getVueInEdition() {
    for (final Iterator<EbliWidgetVueCalque> it = listeWidgetCalque_.iterator(); it.hasNext(); ) {
      final EbliWidgetVueCalque widget = it.next();
      if (widget.isInEditMode()) {
        return widget.calquePanel_.getVueCalque();
      }
    }
    return null;
  }

  ZCalqueGrille getGrille(final ZEbliCalquesPanel _panel) {
    final BCalque calqueParNom = _panel.getCqInfos().getCalqueParNom("cqGrille");
    return (ZCalqueGrille) (calqueParNom instanceof ZCalqueGrille ? calqueParNom : null);
  }

  Map<BVueCalque, ZCalqueGrille> grilles_ = new HashMap<BVueCalque, ZCalqueGrille>();

  private BVueCalque getVueCalque(ZCalqueGrille _grille) {
    for (Map.Entry<BVueCalque, ZCalqueGrille> entry : grilles_.entrySet()) {
      if (entry.getValue() == _grille) {
        return entry.getKey();
      }
    }
    return null;
  }

  private void registerGrillesListener(EbliWidgetVueCalque vue) {
    final ZCalqueGrille grille = getGrille(vue.calquePanel_);
    if (grille != null) {
      grilles_.put(vue.calquePanel_.getVueCalque(), grille);
      grille.addPropertyChangeListener("grille", grillePropertyChangeListener);
    }
  }

  private void registerVueListener(EbliWidgetVueCalque vue) {
    vue.calquePanel_.getVueCalque().addPropertyChangeListener("repere", this);
    vue.calquePanel_.getVueCalque().addPropertyChangeListener("insets", this);
  }

  private void unregisterVueListener() {
    for (EbliWidgetVueCalque vue : listeWidgetCalque_) {
      vue.calquePanel_.getVueCalque().removePropertyChangeListener("repere", this);
      vue.calquePanel_.getVueCalque().removePropertyChangeListener("insets", this);
    }
  }

  private void unregisterGrillesListener() {
    for (EbliWidgetVueCalque vue : listeWidgetCalque_) {
      final ZCalqueGrille grille = getGrille(vue.calquePanel_);
      if (grille != null) {
        grille.removePropertyChangeListener("grille", grillePropertyChangeListener);
      }
    }
  }

  private void changeAllGrilleFrom(final BVueCalque vueCalque) {
    ZCalqueGrille grille = grilles_.get(vueCalque);
    if (grille == null) {
      return;
    }
    final EbliUIProperties ui = grille.saveUIProperties();
    for (Map.Entry<BVueCalque, ZCalqueGrille> entry : grilles_.entrySet()) {
      if (entry.getKey() != vueCalque) {
        entry.getValue().initFrom(ui);
      }
    }
  }

  public void resynchronizeAfterEdition(EbliWidgetVueCalque ebliWidgetVueCalque) {
    for (EbliWidgetVueCalque vue : this.listeWidgetCalque_) {
      vue.getParentWidget().bringToFront();
    }
    final GrBoite zoomSaved = new GrBoite(ebliWidgetVueCalque.zoom_);
    changeAllInsetsFrom(ebliWidgetVueCalque.getCalquePanel().getVueCalque());
    changeAllBounds(ebliWidgetVueCalque);
    changeAllZoomFrom(zoomSaved);
  }

  private void changeAllBounds(final EbliWidgetVueCalque vueCalque) {
    final Rectangle preferredSize = vueCalque.getPreferredBounds();
    final Point location = vueCalque.getLocation();
    for (final Iterator<EbliWidgetVueCalque> it = listeWidgetCalque_.iterator(); it.hasNext(); ) {
      final EbliWidgetVueCalque widget = it.next();
      widget.setPreferredBounds(preferredSize);
      widget.resolveBounds(location, vueCalque.getBounds());
      widget.clearCacheImage();
    }
  }

  private void changeAllZoomFrom(final BVueCalque vueCalque) {
    final GrBoite viewBoite = vueCalque.getViewBoite();
    for (final Iterator<EbliWidgetVueCalque> it = listeWidgetCalque_.iterator(); it.hasNext(); ) {
      final EbliWidgetVueCalque widget = it.next();
      if (widget.calquePanel_.getVueCalque() != vueCalque) {
        widget.setZoom(viewBoite);
      }
    }
  }

  private void changeAllZoomFrom(final GrBoite viewBoite) {
    for (final Iterator<EbliWidgetVueCalque> it = listeWidgetCalque_.iterator(); it.hasNext(); ) {
      final EbliWidgetVueCalque widget = it.next();
      widget.setZoom(new GrBoite(viewBoite));
      widget.repaintAll(false);
    }
  }

  private void changeAllInsetsFrom(final BVueCalque vueCalque) {
    final Insets init = vueCalque.getUserInsets();
    for (final Iterator<EbliWidgetVueCalque> it = listeWidgetCalque_.iterator(); it.hasNext(); ) {
      final EbliWidgetVueCalque widget = it.next();
//      if (widget.calquePanel_.getVueCalque() != vueCalque) {
      widget.calquePanel_.getVueCalque().setUserInsets(init);
      widget.clearCacheImage();
//      }
    }
  }

  private ZCalqueSondeSynchroniserFusion managerSondesFusion_;

  /**
   * Methode a appeler a la fin de la creation de la sonde fusion pour ajouter l action sonde fusion dans chaque
   * calques.
   */
  private void synchroniseSondeFusion() {

    // -- creation du manager de sonde fusion qui synchronise toute ses sondes
    // --//
    managerSondesFusion_ = new ZCalqueSondeSynchroniserFusion();

    for (final EbliWidgetVueCalque widget : listeWidgetCalque_) {
      final ZEbliCalquesPanel panel = widget.calquePanel_;

      // -- il faut initialiser les actions specifiques si pas deja fait pour
      // activer les interactions--//
      panel.getController().initSpecificActions();

      // --recuperation de la sonde du calque --//
      final ZCalqueSondeInteraction sonde = panel.getController().getCalqueSondeInteraction();

      // -- ajout de la sonde dans le manager
      managerSondesFusion_.addZCalqueSondeInteraction(sonde, widget);
    }
  }

  @Override
  protected void notifyAdded() {
    super.notifyAdded();
    synchronyseZoom();
    synchroniseSondeFusion();
  }

  @Override
  protected void notifyRemoved() {
    unregisterGrillesListener();
    unregisterVueListener();
    super.notifyRemoved();
    desynchroniseSondeFusion();
  }

  /**
   * Methode a appeler au ungroup ou undo sur la fusion. Desynchronise les sondes des calques.
   */
  private void desynchroniseSondeFusion() {
    managerSondesFusion_.removeAllListenningSonde();
  }

  @Override
  public boolean hasSattelite() {
    return true;
  }

  @Override
  public Set<EbliWidget> getSatellites() {
    final Set<EbliWidget> liste = new HashSet<EbliWidget>();
    for (EbliWidget vue : listeWidgetCalque_) {
      if (vue.hasSattelite()) {
        liste.addAll(vue.getSatellites());
      }
    }
    return liste;
  }

  @Override
  protected Rectangle calculateClientArea() {
    return super.calculateClientArea();
  }
}
