package org.fudaa.ebli.visuallibrary.actions;

import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * Classe qui permet de faire un undo redo sur le front/back
 * 
 * @author Adrien Hadoux
 */
public class CommandBringToFront implements CtuluCommand {

  List<EbliWidget> widgets_;

  public CommandBringToFront(final List<EbliWidget> widgets_) {
    super();
    this.widgets_ = widgets_;
  }

  @Override
  public void redo() {
    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).bringToFront();

    }
    refreshScene();
  }

  private void refreshScene() {
    EbliScene.refreshScene(widgets_.get(0).getScene());
  }

  @Override
  public void undo() {
    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).bringToBack();

    }
    refreshScene();
  }

}
