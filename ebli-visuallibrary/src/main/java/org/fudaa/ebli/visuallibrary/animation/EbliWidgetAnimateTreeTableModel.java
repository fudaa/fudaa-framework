/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.animation;

import java.util.List;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

/**
 * @author deniger
 */
public class EbliWidgetAnimateTreeTableModel extends DefaultTreeTableModel {

  public EbliWidgetAnimateTreeTableModel() {
    super();
  }

  public EbliWidgetAnimateTreeTableModel(final TreeTableNode _root) {
    super(_root);
  }

  public EbliWidgetAnimateTreeTableModel(final TreeTableNode _root, final List<?> _columnNames) {
    super(_root, _columnNames);
  }

  @Override
  public Class<?> getColumnClass(final int _column) {
    if (_column == 1) return Boolean.class;
    return super.getColumnClass(_column);
  }

  @Override
  public void setValueAt(final Object _value, final Object _node, final int _column) {
    super.setValueAt(_value, _node, _column);
    final TreeTableNode ttn = (TreeTableNode) _node;

    if (_column == 1 && !ttn.isLeaf()) {
      final int n = ttn.getChildCount();
      for (int i = 0; i < n; i++) {
        final TreeTableNode child = ttn.getChildAt(i);
        child.setValueAt(_value, _column);
        modelSupport.firePathChanged(new TreePath(getPathToRoot(child)));
      }
    }
  }

}
