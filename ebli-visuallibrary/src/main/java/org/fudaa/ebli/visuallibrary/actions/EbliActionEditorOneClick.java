package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.action.InplaceEditorProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.util.GeomUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EnumSet;

/**
 * Classe qui permet d'ouvrir l editor en 1 seul clic.
 *
 * @author David Kaspar
 * @pompeur Adrien Hadoux
 */
public final class EbliActionEditorOneClick<C extends JComponent> extends WidgetAction.LockedAdapter implements
    InplaceEditorProvider.EditorController {
  private final InplaceEditorProvider<C> provider;
  private C editor = null;
  private Widget widget = null;
  private Rectangle rectangle = null;

  public EbliActionEditorOneClick(final InplaceEditorProvider<C> provider) {
    this.provider = provider;
  }

  @Override
  protected boolean isLocked() {
    return editor != null;
  }

  /**
   * Modification ici: pour activer l editor, il suffit d appuyer une seule fois sur l editeur
   */
  @Override
  public State mouseClicked(final Widget _widget, final WidgetMouseEvent _event) {

    if (_event.getButton() == MouseEvent.BUTTON1) {
      final Point p = _event.getPoint();
      EbliScene scene = (EbliScene) _widget.getScene();

      if (_widget.getClientArea().contains(p)) {
        if (scene.getToEdit() != null && scene.getToEdit() != _widget) {
          return State.REJECTED;
        }
        if (openEditor(_widget)) {
          return State.createLocked(_widget, this);
        }
      }
    }
    return State.REJECTED;
  }

  @Override
  public State mousePressed(final Widget _widget, final WidgetMouseEvent _event) {
    if (editor != null) {
      closeEditor(true);
      return State.CONSUMED;
    }
    return State.REJECTED;
  }

  @Override
  public State mouseReleased(final Widget _widget, final WidgetAction.WidgetMouseEvent _event) {
    if (editor != null) {
      closeEditor(true);
      return State.CONSUMED;
    }
    return State.REJECTED;
  }

  @Override
  public State keyPressed(final Widget _widget, final WidgetKeyEvent _event) {
    if (_event.getKeyChar() == KeyEvent.VK_ENTER) {
      if (openEditor(_widget)) {
        return State.createLocked(_widget, this);
      }
    }
    return State.REJECTED;
  }

  @Override
  public final boolean isEditorVisible() {
    return editor != null;
  }

  public final boolean openEditorFromMenu(final Widget _widget) {
    EbliWidget ebliWidget = ((EbliWidget) _widget);
    final EbliScene ebliScene = ebliWidget.getEbliScene();
    ebliScene.getView().requestFocusInWindow();
    ebliScene.setWidgetToEdit(ebliWidget);

    Point location = ebliWidget.getParentBordure().getPreferredLocationOnScreen();
    final EbliWidget group = ebliWidget.getGroup();
    if (group != null) {
      location = group.getPreferredLocationOnScreen();
    }
    _widget.getScene().setFocusedWidget(_widget);
    final Rectangle bounds = _widget.getBounds();
    MouseEvent event = new MouseEvent(ebliScene.getView(), MouseEvent.MOUSE_CLICKED, System.currentTimeMillis() - 4, 0, location.x + bounds.width / 2,
        location.y + bounds.height / 2, 1, false, MouseEvent.BUTTON1);
    ebliScene.mouseClicked(event);

    return true;
  }

  @Override
  public final boolean openEditor(final Widget _widget) {
    final Scene scene = _widget.getScene();
    if (editor != null) {
      return false;
    }
    ((EbliScene) scene).clearEditContext();

    final JComponent component = scene.getView();
    if (component == null) {
      return false;
    }

    editor = provider.createEditorComponent(this, _widget);
    if (editor == null) {
      return false;
    }
    this.widget = _widget;

    component.add(editor);
    provider.notifyOpened(this, _widget, editor);

    Rectangle sceneRect = _widget.getScene().convertSceneToView(_widget.convertLocalToScene(_widget.getBounds()));

    final Point center = GeomUtil.center(sceneRect);
    final Dimension size = editor.getMinimumSize();
    if (sceneRect.width > size.width) {
      size.width = sceneRect.width;
    }
    if (sceneRect.height > size.height) {
      size.height = sceneRect.height;
    }
    final int x = center.x - size.width / 2;
    final int y = center.y - size.height / 2;
    sceneRect = new Rectangle(x, y, size.width, size.height);
    updateRectangleToFitToView(sceneRect);

    final Rectangle r = provider.getInitialEditorComponentBounds(this, _widget, editor, sceneRect);
    this.rectangle = r != null ? r : sceneRect;

    editor.setBounds(x, y, size.width, size.height);
    notifyEditorComponentBoundsChanged();
    editor.requestFocusInWindow();

    return true;
  }

  private void updateRectangleToFitToView(final Rectangle _rectangle) {
    final JComponent component = widget.getScene().getView();
    if (_rectangle.x + _rectangle.width > component.getWidth()) {
      _rectangle.x = component.getWidth() - _rectangle.width;
    }
    if (_rectangle.y + _rectangle.height > component.getHeight()) {
      _rectangle.y = component.getHeight()
          - _rectangle.height;
    }
    if (_rectangle.x < 0) {
      _rectangle.x = 0;
    }
    if (_rectangle.y < 0) {
      _rectangle.y = 0;
    }
  }

  @Override
  public final void closeEditor(final boolean commit) {
    if (editor == null) {
      return;
    }
    final Container parent = editor.getParent();
    final Rectangle bounds = parent != null ? editor.getBounds() : null;
    provider.notifyClosing(this, widget, editor, commit);
    final boolean hasFocus = editor.hasFocus();
    if (bounds != null) {
      parent.remove(editor);
      parent.repaint(bounds.x, bounds.y, bounds.width, bounds.height);
      parent.repaint(0);
    }
    editor = null;
    widget = null;
    rectangle = null;
    if (hasFocus) {
      if (parent != null) {
        parent.requestFocusInWindow();
      }
    }
  }

  @Override
  public void notifyEditorComponentBoundsChanged() {
    EnumSet<InplaceEditorProvider.ExpansionDirection> directions = provider
        .getExpansionDirections(this, widget, editor);
    if (directions == null) {
      directions = EnumSet.noneOf(InplaceEditorProvider.ExpansionDirection.class);
    }
    // final Rectangle rectangle = this.rectangle;
    final Dimension size = editor.getPreferredSize();
    final Dimension minimumSize = editor.getMinimumSize();
    if (minimumSize != null) {
      if (size.width < minimumSize.width) {
        size.width = minimumSize.width;
      }
      if (size.height < minimumSize.height) {
        size.height = minimumSize.height;
      }
    }

    final int heightDiff = rectangle.height - size.height;
    final int widthDiff = rectangle.width - size.width;

    final boolean top = directions.contains(InplaceEditorProvider.ExpansionDirection.TOP);
    final boolean bottom = directions.contains(InplaceEditorProvider.ExpansionDirection.BOTTOM);

    if (top) {
      if (bottom) {
        rectangle.y += heightDiff / 2;
        rectangle.height = size.height;
      } else {
        rectangle.y += heightDiff;
        rectangle.height = size.height;
      }
    } else {
      if (bottom) {
        rectangle.height = size.height;
      } else {
      }
    }

    final boolean left = directions.contains(InplaceEditorProvider.ExpansionDirection.LEFT);
    final boolean right = directions.contains(InplaceEditorProvider.ExpansionDirection.RIGHT);

    if (left) {
      if (right) {
        rectangle.x += widthDiff / 2;
        rectangle.width = size.width;
      } else {
        rectangle.x += widthDiff;
        rectangle.width = size.width;
      }
    } else {
      if (right) {
        rectangle.width = size.width;
      } else {
      }
    }

    updateRectangleToFitToView(rectangle);

    editor.setBounds(rectangle);
    editor.repaint();
  }
}
