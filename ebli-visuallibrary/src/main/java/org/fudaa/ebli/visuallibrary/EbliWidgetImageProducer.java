package org.fudaa.ebli.visuallibrary;

import java.awt.image.BufferedImage;
import java.util.Map;

/**
 * Interface to be implemented by widget that can be painted in a image
 */
public interface EbliWidgetImageProducer {
  BufferedImage produceImage(int width, int height, Map params);
}
