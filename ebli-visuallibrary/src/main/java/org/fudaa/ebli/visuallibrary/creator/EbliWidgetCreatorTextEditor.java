package org.fudaa.ebli.visuallibrary.creator;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluHtmlEditorPanel;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.EbliWidgetTextEditor;

import java.io.*;
import java.util.Map;

/**
 * creator Editeur de texte widget.
 *
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorTextEditor extends EbliWidgetCreator {
  CtuluHtmlEditorPanel editorPane_;
  static int Idcontenu = 1;

  public EbliWidgetCreatorTextEditor(final CtuluHtmlEditorPanel g) {
    super();
    this.editorPane_ = g;
  }

  public EbliWidgetCreatorTextEditor(final EbliWidgetCreatorTextEditor toCopy) {
    super(toCopy);
    this.editorPane_ = new CtuluHtmlEditorPanel();
    editorPane_.setDocumentText(toCopy.getHtmlEditorPanel().getDocumentText());
  }

  public EbliWidgetCreatorTextEditor() {

  }

  public CtuluHtmlEditorPanel getHtmlEditorPanel() {
    return editorPane_;
  }

  public void setG(final CtuluHtmlEditorPanel g) {
    this.editorPane_ = g;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    return new EbliWidgetBordureSingle(new EbliWidgetTextEditor(_scene, getHtmlEditorPanel()));
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorTextEditor(this);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    // ecriture du contenu html dans ce fichier
    try {
      final String path = (String) parameters.get("pathTexte");
      if (path != null) {
        final File contenu = new File(path + File.separator + "contenu" + parameters.get("nodeName")
            + getWidget().getId() + ".html");
        try (FileWriter writer = new FileWriter(contenu)) {
          writer.write(editorPane_.getDocumentText());
        }

        String pathRelative = CtuluLibFile.getRelativeFile(contenu, new File((String) parameters.get("pathLayout")), 4);

        return pathRelative;
      }
      return null;
    } catch (final IOException e) {
      FuLog.error(e);
      return null;
    }
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    editorPane_ = new CtuluHtmlEditorPanel();

    if (data == null) {
      return false;
    }
    final File contenu = new File(parameters.get("pathLayout") + File.separator + data);
    try (final InputStream ips = new FileInputStream(contenu);
        final InputStreamReader ipsr = new InputStreamReader(ips);
        final BufferedReader br = new BufferedReader(ipsr)
    ) {
      final StringBuffer contentHTML = new StringBuffer();
      String ligne;
      while ((ligne = br.readLine()) != null)
        contentHTML.append(ligne);
      br.close();
      editorPane_.setDocumentText(contentHTML.toString());
    } catch (final IOException e) {
      FuLog.error(e);
    }
    return true;
  }
}
