package org.fudaa.ebli.visuallibrary.persist;

import org.fudaa.ebli.visuallibrary.EbliNode;

import java.awt.*;
import java.util.Map;

/**
 * Classe utilisation pour la serialization des donn�es widget groupes
 *
 * @author Adrien Hadoux
 */
public class EbliWidgetGroupSerializeXml extends EbliWidgetSerializeXml {
  /**
   * Indique si le groupe est une fusion ou pas
   */
  boolean isFusion;

  public EbliWidgetGroupSerializeXml(EbliNode node, Map parameters, boolean isFusion) {
    super(node, parameters);
    // TODO Auto-generated constructor stub
    this.isFusion = isFusion;
  }

  /**
   * Methode qui met a jour les infos du groupe.
   * Pour des raisons de conception, ce groupe ne se cr�e pas � ce niveau, car il est sp�cialis� (peut etre groupe classique, ou groupe de calque ou autre.
   *
   * @param nodeGroup
   */
  public void updateGroup(EbliNode nodeGroup, Map parameters) {
    nodeGroup.getWidget().setId(this.Id);
    String title = (String) parameters.get("nodeName");
    if (title != null) {
      nodeGroup.setTitle((String) parameters.get("nodeName"));
    } else {
      nodeGroup.setTitle("");
    }
    nodeGroup.getWidget().setPreferredLocation(new Point(getX(), getY()));
    nodeGroup.getWidget().setPreferredSize(new Dimension(getWidth(), getHeight()));
  }

  public boolean isFusion() {
    return isFusion;
  }

  public void setFusion(boolean isFusion) {
    this.isFusion = isFusion;
  }
}
