package org.fudaa.ebli.visuallibrary.tree;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.Icon;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.visuallibrary.DefaultObjectSceneListener;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.netbeans.api.visual.model.ObjectSceneEvent;
import org.netbeans.api.visual.model.ObjectSceneEventType;
import org.netbeans.api.visual.model.ObjectSceneListener;

/**
 * Model du treetable. contient 3 colonnes: colonne 1:icone colonne 2:nom colonne 3:checkbox associee
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetJXTreeTableModel extends DefaultTreeTableModel {

  /**
   * titres des colonnes.
   */
  private static final String[] title = { "", EbliLib.getS("Nom"), "V", EbliLib.getS("Bloqu�") };

  final EbliScene scene_;
  final ObjectSceneListener sceneListener_;

  public void firePathChanged(TreeTableNode ttn) {
    modelSupport.firePathChanged(new TreePath(getPathToRoot(ttn)));
  }

  /**
   * constructeur.
   * 
   * @param numLayer
   * @param scene_
   */
  public EbliWidgetJXTreeTableModel(final EbliScene _scene) {
    super(EbliWidgetTreeTableNode.build(_scene));
    scene_ = _scene;
    sceneListener_ = new DefaultObjectSceneListener() {
      @Override
      public void objectAdded(final ObjectSceneEvent _event, final Object _addedObject) {
        nodeAdded((EbliNode) _addedObject);
      }

      @Override
      public void objectRemoved(final ObjectSceneEvent _event, final Object _removedObject) {
        nodeRemoved((EbliNode) _removedObject);
      }

    };
    scene_.addObjectSceneListener(sceneListener_, ObjectSceneEventType.OBJECT_ADDED,
        ObjectSceneEventType.OBJECT_REMOVED);
    scene_.addPropertyChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(final PropertyChangeEvent _evt) {
        if ("title".equals(_evt.getPropertyName()) || "visible".equals(_evt.getPropertyName())) {
          final EbliWidget widget = (EbliWidget) _evt.getNewValue();
          final EbliNode n = (EbliNode) scene_.findObject(widget);
          fireNodeChanged(n);
        } else if (_evt.getPropagationId() instanceof EbliNode) {
          fireNodeChanged((EbliNode) _evt.getPropagationId());
        }

      }
    });
  }

  public void removeListener() {
    scene_.removeObjectSceneListener(sceneListener_, ObjectSceneEventType.OBJECT_ADDED,
        ObjectSceneEventType.OBJECT_REMOVED);
  }

  public EbliScene getScene() {
    return scene_;
  }

  public void nodeAdded(final EbliNode _node) {
    refresh();
  }

  public void nodeMoved(final EbliWidget widget) {
    refresh();
  }

  public void nodeRemoved(final EbliNode _node) {
    final MutableTreeTableNode aSuppr = findTreeTableNode(_node);

    // -- suppression du node de ses parents
    if (aSuppr != null) removeNodeFromParent(aSuppr);

    // -- rafraichissement du modele --//
    // modeleTree.reload();
    // reload();

    // setResizeColumn();

  }

  public MutableTreeTableNode findTreeTableNode(final EbliNode _node) {
    // -- recuperation des nodes --//
    final TreeTableNode parent = getRoot();
    final Enumeration<? extends TreeTableNode> childrens = parent.children();
    while (childrens.hasMoreElements()) {
      final MutableTreeTableNode suspect = findTreeTableNode(_node, (MutableTreeTableNode) childrens.nextElement());
      if (suspect != null) return suspect;

    }
    return null;
  }

  public MutableTreeTableNode findTreeTableNode(final EbliNode _node, final MutableTreeTableNode parent) {
    if (parent.getUserObject() == _node) return parent;
    final Enumeration<? extends TreeTableNode> childrens = parent.children();
    while (childrens.hasMoreElements()) {
      final MutableTreeTableNode suspect = (MutableTreeTableNode) childrens.nextElement();
      if (suspect.getUserObject() == _node) return suspect;

    }
    return null;

  }

  private void refresh() {
    final EbliWidgetTreeTableNode rootNode = (EbliWidgetTreeTableNode) getRoot();

    // final Enumeration<? extends MutableTreeTableNode> children = root.children();
    final int childCount = rootNode.getChildCount();
    final List<TreeTableNode> res = new ArrayList(childCount);
    for (int i = 0; i < childCount; i++) {
      res.add(rootNode.getChildAt(i));
    }
    for (final TreeTableNode treeTableNode : res) {
      removeNodeFromParent((MutableTreeTableNode) treeTableNode);
    }
    EbliWidgetTreeTableNode.fillRoot(rootNode, this);
    modelSupport.fireNewRoot();
  }

  @Override
  public Class<?> getColumnClass(final int arg0) {
    switch (arg0) {
    case 0:
      return Icon.class;
    case 1:
      return String.class;
    case 2:
      return Boolean.class;
    case 3:
      return Boolean.class;
    default:
      return null;
    }

  }

  @Override
  public String getColumnName(final int arg0) {
    if (arg0 < title.length) return title[arg0];
    return CtuluLibString.EMPTY_STRING;
  }

  private void fireNodeChanged(final EbliNode n) {
    if (n != null) {
      final MutableTreeTableNode aNode = findTreeTableNode(n);
      if (aNode != null) modelSupport.firePathChanged(new TreePath(getPathToRoot(aNode)));
    }
  }

  // /**
  // * Raffraichissement maison du jxtreetable
  // */
  // private void reload() {
  //
  // this.modelSupport.fireTreeStructureChanged(new
  // TreePath(getPathToRoot(getRoot())));
  // }

}
