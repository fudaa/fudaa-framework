package org.fudaa.ebli.visuallibrary.graphe;

import java.awt.Font;
import java.util.Map;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.EbliWidgetControllerMenuOnly;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;

public class EbliWidgetCreatorLegende extends EbliWidgetCreator {

  EGGraphe g;

  public EbliWidgetCreatorLegende(EbliWidgetCreatorLegende toCopy, final EGGraphe g, final EbliWidgetCreatorGraphe id) {
    super(toCopy);
    super.owner = id;
    this.g = g;
  }

  public EbliWidgetCreatorLegende(final EGGraphe g, final EbliWidgetCreatorGraphe id) {
    super();
    super.owner = id;
    this.g = g;
  }

  public EbliWidgetCreatorLegende() {

  }

  public EGGraphe getG() {
    return g;
  }

  public void setG(final EGGraphe g) {
    this.g = g;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    if (g == null) return null;
    EbliWidget legende = new EbliWidgetGrapheLegende(g, _scene, false);
    EbliWidgetBordureSingle res = new EbliWidgetBordureSingle(legende, true, true);
    legende.setController(new EbliWidgetControllerMenuOnly(res, false));
    if (owner != null) owner.getWidget().addSatellite(res);
    legende.setFormeFont(new Font("Helvetica", Font.PLAIN, 12));
    return res;
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    EbliWidgetCreatorGraphe ow = (EbliWidgetCreatorGraphe) satteliteOwnerDuplicated;
    return new EbliWidgetCreatorLegende(this, ow.getGraphe(), ow);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return super.owner.getWidget().getId();
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    if (data == null) {
      g = new EGGraphe(new EGGrapheTreeModel());
    } else if (data instanceof String) {
      // --recuperation de l'id du graphe parent--//
      // -- recuperation du node graphe--//
      final EbliNode nodeGraphe = ((EbliScene) parameters.get("scene")).findNodeById((String) data);
      if (nodeGraphe != null && nodeGraphe.getCreator() instanceof EbliWidgetCreatorGraphe) {
        owner = nodeGraphe.getCreator();
        // -- on recupere le graphe --//
        g = ((EbliWidgetCreatorGraphe) nodeGraphe.getCreator()).getGraphe();
        // -- mise a jour des sattelites --//

      } else {
        return false;
      }
    } else {
      g = new EGGraphe(new EGGrapheTreeModel());
    }
    return true;

  }

}
