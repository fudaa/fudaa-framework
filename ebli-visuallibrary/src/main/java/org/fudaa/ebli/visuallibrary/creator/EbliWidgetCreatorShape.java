package org.fudaa.ebli.visuallibrary.creator;

import java.util.Map;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetShape;

/**
 * Creator pour les objets graphiques de type shape.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorShape extends EbliWidgetCreator {

  /**
   * Le type de l objet a creer.
   */
  ShapeCreator typeObject_;

  public ShapeCreator getTypeObject() {
    return typeObject_;
  }

  public void setTypeObject(final ShapeCreator typeObject_) {
    this.typeObject_ = typeObject_;
  }

  public EbliWidgetCreatorShape(final ShapeCreator _typeObject) {
    super();
    typeObject_ = _typeObject;
  }

  public EbliWidgetCreatorShape(final EbliWidgetCreatorShape toCopy) {
    super(toCopy);
    typeObject_ = toCopy.typeObject_;
  }

  public EbliWidgetCreatorShape() {

  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {

    return new EbliWidgetShape(_scene, typeObject_, null);
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorShape(this);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return typeObject_;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    if (data == null) typeObject_ = new ShapeCreatorCircle();
    else {
      typeObject_ = (ShapeCreator) data;
    }
    return true;
  }

}
