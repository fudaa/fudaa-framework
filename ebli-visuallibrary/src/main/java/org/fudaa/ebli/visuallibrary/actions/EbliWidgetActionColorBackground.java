package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Color;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;
import javax.swing.JColorChooser;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.WidgetResource;

/**
 * Classe qui permet de changer la couleur des contours des widgets .
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public abstract class EbliWidgetActionColorBackground extends EbliWidgetActionAbstract {

  public static class ForWidget extends EbliWidgetActionColorBackground {

    public ForWidget(final EbliScene _scene) {
      super(_scene);

    }

    @Override
    public CtuluCommand changeCouleur(final Color color) {
      // on met a jour les couleurs pour toutes les widgets selectionnees --//

      // -- recupere les anciennes graphical properties --//
      final List<Map<String, Object>> oldGraphicalProperties = new ArrayList<Map<String, Object>>();

      // -- liste des nouvelles graphical properties --//
      final List<Map<String, Object>> newGraphicalProperties = new ArrayList<Map<String, Object>>();

      // -- nouvelle couleur --//

      final Color newColor = color;

      // -- liste des widget selectionnees --//
      final java.util.List<EbliWidget> listeWidget = new ArrayList<EbliWidget>();

      // -- liste des nodes selectionnes --//
      final Set<EbliNode> listeNode = (Set<EbliNode>) scene_.getSelectedObjects();// scene_

      for (final Iterator<EbliNode> it = listeNode.iterator(); it.hasNext();) {

        final EbliNode currentNode = it.next();

        if (currentNode.hasWidget()) {

          final EbliWidget widget = currentNode.getWidget();

          // -- test pour savoir si le widget gere le colorcontour --//
          if (widget.getColorFond() != null) {

            // -- ajout pour les undo redo
            listeWidget.add(widget);
            oldGraphicalProperties.add(widget.duplicateGraphicalProperties());

            // -- mise a jour de la nouvelle couleur --//
            widget.setColorFond(newColor);

            // -- ajout de la nouvelle graphicalProperty --//
            newGraphicalProperties.add(widget.getPropGraphique());

          }

        }
      }

      return new CommandeUndoRedoGraphicalProperties(newGraphicalProperties, oldGraphicalProperties, listeWidget);

    }

  }

  public static class ForScene extends EbliWidgetActionColorBackground {

    public ForScene(final EbliScene _scene) {
      super(_scene);
      putValue(Action.NAME, WidgetResource.getS("Couleur de fond Layout"));

    }

    @Override
    public CtuluCommand changeCouleur(final Color color) {

      final Paint oldColor = scene_.getBackground();

      final Paint newColor = color;

      scene_.setBackground(newColor);

      return new CtuluCommand() {

        @Override
        public void redo() {
          scene_.setBackground(newColor);
          scene_.refresh();
        }

        @Override
        public void undo() {
          scene_.setBackground(oldColor);
          scene_.refresh();
        }

      };

    }

  }

  public EbliWidgetActionColorBackground(final EbliScene _scene) {
    super(_scene, EbliResource.EBLI.getString("Couleur de fond"), EbliResource.EBLI.getToolIcon("couleur"), "CONTOUR");

    cmd_ = _scene.getCmdMng();

  }

  public abstract CtuluCommand changeCouleur(Color color);

  @Override
  public void actionPerformed(final ActionEvent e) {

    // -- choix de la couleur par l utilisateur --//
    final Color couleur = JColorChooser.showDialog(null, EbliResource.EBLI.getString("Couleur de fond"), Color.black);
    if (couleur != null) {
      if (cmd_ != null) cmd_.addCmd(changeCouleur(couleur));

      scene_.refresh();

    }

  }

}
