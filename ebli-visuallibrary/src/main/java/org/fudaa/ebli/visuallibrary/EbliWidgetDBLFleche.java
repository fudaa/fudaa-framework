package org.fudaa.ebli.visuallibrary;

import java.awt.Insets;
import java.awt.Point;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.anchor.AnchorShapeFactory;
import org.netbeans.api.visual.widget.ConnectionWidget;

/**
 * Legende Widget qui permet de construire une fleche double
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetDBLFleche extends EbliWidget {

  Insets inGaps_;
  int largeurFleche = 15;

  public final static int ORIENTATION_NORTH = 0;
  public final static int ORIENTATION_SOUTH = 1;
  public final static int ORIENTATION_EAST = 2;
  public final static int ORIENTATION_WEST = 3;
  public final static int ORIENTATION_DIAG1 = 4;
  public final static int ORIENTATION_DIAG2 = 5;
  public final static int ORIENTATION_DIAG3 = 6;
  public final static int ORIENTATION_DIAG4 = 7;

  Point positionInit;
  Point positionDest;

  ConnectionWidget shapeWidget;
  AnchorShape shape;

  /**
   * @param _scene
   */
  public EbliWidgetDBLFleche(final EbliScene _scene, final int orientation) {
    super(_scene, true);

    // -- creation de la fleche --//
    shapeWidget = new ConnectionWidget(_scene);
    shape = AnchorShapeFactory.createTriangleAnchorShape(largeurFleche, true, false, 0);
    // shape = AnchorShapeFactory.createArrowAnchorShape(45, 17);

    determinePosition(orientation);

    shapeWidget.setSourceAnchor(AnchorFactory.createFixedAnchor(positionInit));
    shapeWidget.setTargetAnchor(AnchorFactory.createFixedAnchor(positionDest));
    shapeWidget.setSourceAnchorShape(shape);
    shapeWidget.setTargetAnchorShape(shape);
    addChild(shapeWidget);

  }

  public void determinePosition(final int orientation) {
    positionInit = new Point();
    positionDest = new Point();

    switch (orientation) {
    case ORIENTATION_NORTH:
      positionInit.x = 25;
      positionInit.y = 0;
      positionDest.x = 25;
      positionDest.y = 50;
      break;
    case ORIENTATION_SOUTH:
      positionInit.x = 25;
      positionInit.y = 0;
      positionDest.x = 25;
      positionDest.y = 50;
      break;
    case ORIENTATION_WEST:
      positionInit.x = 0;
      positionInit.y = 25;
      positionDest.x = 50;
      positionDest.y = 25;
      break;
    case ORIENTATION_EAST:
      positionInit.x = 0;
      positionInit.y = 25;
      positionDest.x = 50;
      positionDest.y = 25;
      break;
    case ORIENTATION_DIAG1:
      positionInit.x = 0;
      positionInit.y = 0;
      positionDest.x = 50;
      positionDest.y = 50;
      break;
    case ORIENTATION_DIAG2:
      positionInit.x = 0;
      positionInit.y = 0;
      positionDest.x = 50;
      positionDest.y = 50;
      break;
    case ORIENTATION_DIAG3:
      positionInit.x = 0;
      positionInit.y = 50;
      positionDest.x = 50;
      positionDest.y = 50;
      break;
    case ORIENTATION_DIAG4:
      positionInit.x = 0;
      positionInit.y = 50;
      positionDest.x = 50;
      positionDest.y = 0;
      break;

    }
  }

}
