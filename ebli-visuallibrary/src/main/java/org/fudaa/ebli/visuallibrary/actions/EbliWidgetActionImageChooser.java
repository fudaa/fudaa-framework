package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuResource;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.JFileChooser;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorImage;

/**
 * Action qui permet de creer une widget image.
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class EbliWidgetActionImageChooser extends EbliWidgetActionAbstract {

  public EbliWidgetActionImageChooser(final EbliScene _widget) {
    super(_widget, EbliLib.getS("Ins�rer une image"), BuResource.BU.getIcon("crystal_image"), "INSERTIONIMAGE");

  }

  @Override
  public void actionPerformed(final ActionEvent e) {

    final BuFileChooser chooser = new BuFileChooser(/* System.getProperty("user.dir") */);

    final int rep = chooser.showOpenDialog(scene_.getView());

    if (rep == JFileChooser.APPROVE_OPTION) {
      final Image img = Toolkit.getDefaultToolkit().getImage(chooser.getSelectedFile().getAbsolutePath());

      // -- creation de la widget et ajout dans la scene --//
      final EbliNode nodeImage = new EbliNodeDefault();
      nodeImage.setTitle(" " + chooser.getSelectedFile());
      EbliWidgetCreatorImage creator = new EbliWidgetCreatorImage(img, chooser.getSelectedFile().getAbsolutePath());
      nodeImage.setCreator(creator);
      creator.setPreferredSize(new Dimension(200, 200));
      creator.setPreferredLocation(new Point(270, 225));
      scene_.addNode(nodeImage);

      // -- rafraichissement de la scene --//
      scene_.refresh();

    }
  }

}
