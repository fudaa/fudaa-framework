/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import org.netbeans.api.visual.border.Border;
import org.netbeans.modules.visual.util.GeomUtil;

/**
 * @author deniger
 */
public class ProportionnalResizeBorder implements Border {

  private static final BasicStroke STROKE = new BasicStroke(1.0f, BasicStroke.JOIN_BEVEL, BasicStroke.CAP_BUTT, 5.0f,
      new float[] { 6.0f, 3.0f }, 0.0f);

  private int thickness;
  private Color color;
  private boolean outer;

  public ProportionnalResizeBorder(int thickness, Color color, boolean outer) {
    this.thickness = thickness;
    this.color = color;
    this.outer = outer;
  }

  @Override
  public Insets getInsets() {
    return new Insets(thickness, thickness, thickness, thickness);
  }

  public boolean isOuter() {
    return outer;
  }

  @Override
  public void paint(Graphics2D gr, Rectangle bounds) {
    gr.setColor(color);

    Stroke stroke = gr.getStroke();
    gr.setStroke(STROKE);
    if (outer) gr.draw(new Rectangle2D.Double(bounds.x + 0.5, bounds.y + 0.5, bounds.width - 1.0, bounds.height - 1.0));
    else gr.draw(new Rectangle2D.Double(bounds.x + thickness + 0.5, bounds.y + thickness + 0.5, bounds.width
        - thickness - thickness - 1.0, bounds.height - thickness - thickness - 1.0));
    gr.setStroke(stroke);

    Point center = GeomUtil.center(bounds);
    if (bounds.width >= thickness * 5) {
      gr.fillRect(center.x - thickness / 2, bounds.y, thickness, thickness);
      gr.fillRect(center.x - thickness / 2, bounds.y + bounds.height - thickness, thickness, thickness);
    }
    if (bounds.height >= thickness * 5) {
      gr.fillRect(bounds.x, center.y - thickness / 2, thickness, thickness);
      gr.fillRect(bounds.x + bounds.width - thickness, center.y - thickness / 2, thickness, thickness);
    }
  }

  @Override
  public boolean isOpaque() {
    return outer;
  }

}
