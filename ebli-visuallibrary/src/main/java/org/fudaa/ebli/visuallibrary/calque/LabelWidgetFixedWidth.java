package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Rectangle;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.LabelWidget.VerticalAlignment;
import org.netbeans.api.visual.widget.Scene;

public class LabelWidgetFixedWidth extends LabelWidget {

  private int width;

  public LabelWidgetFixedWidth(final Scene scene) {
    super(scene);
    setVerticalAlignment(VerticalAlignment.CENTER);
    setAlignment(Alignment.RIGHT);
    setEnabled(true);
  }

  @Override
  protected Rectangle calculateClientArea() {
    Rectangle res = super.calculateClientArea();
    if (getWidth() > 0) {
      res.width = Math.max(getWidth(), res.width);
    }
    return res;
  }

  /**
   * @return the width
   */
  public int getWidth() {
    return width;
  }

  /**
   * @param width the width to set
   */
  public void setWidth(int width) {
    this.width = width;
  }

  public static int calculateNormalWidth(LabelWidgetFixedWidth w) {
    if (w == null)
      return 0;
    int save = w.getWidth();
    w.setWidth(0);
    int res = w.calculateClientArea().width;
    w.setWidth(save);
    return res + 3;
  }

}