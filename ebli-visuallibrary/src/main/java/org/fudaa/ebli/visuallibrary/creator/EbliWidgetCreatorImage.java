package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Map;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.EbliWidgetImage;

/**
 * Creator de la widget image.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorImage extends EbliWidgetCreator {

  Image image_;
  String pathImage_;

  public EbliWidgetCreatorImage(final Image g, final String path) {
    super();
    pathImage_ = path;
    this.image_ = g;
  }

  public EbliWidgetCreatorImage(final EbliWidgetCreatorImage toCopy) {
    super(toCopy);
    pathImage_ = toCopy.pathImage_;
    this.image_ = toCopy.image_;
  }

  public EbliWidgetCreatorImage() {

  }

  public Image getImage() {
    return image_;
  }

  public void setG(final Image g) {
    this.image_ = g;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    return new EbliWidgetBordureSingle(new EbliWidgetImage(_scene, getImage()));
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorImage(this);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return pathImage_;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    if (data == null) return false;
    pathImage_ = (String) data;

    // chargement de l'image
    if (pathImage_ != null) {
      image_ = Toolkit.getDefaultToolkit().getImage(pathImage_);
    }
    return true;
  }

}
