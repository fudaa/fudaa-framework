package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JColorChooser;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * Classe qui permet de changer la couleur des contours des widgets .
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class EbliWidgetActionColorForeground extends EbliWidgetActionAbstract {

//  CtuluCommandContainer cmd_;

  public EbliWidgetActionColorForeground(final EbliScene _scene) {
    super(_scene, EbliResource.EBLI.getString("Couleur Contour"), EbliResource.EBLI.getToolIcon("palettecouleur"),
        "CONTOUR");

//    cmd_ = _scene.getCmdMng();
  }

  @Override
  public void actionPerformed(final ActionEvent e) {

    // -- choix de la couleur par l utilisateur --//
    final Color couleur = JColorChooser
        .showDialog(null, EbliResource.EBLI.getString("Couleur du contour"), Color.black);
    if (couleur != null) {

      // on met a joru les couleurs pour toutes les widgets selectionnees --//

      // -- recupere les anciennes graphical properties --//
      final List<Map<String, Object>> oldGraphicalProperties = new ArrayList<Map<String, Object>>();;

      // -- liste des nouvelles graphical properties --//
      final List<Map<String, Object>> newGraphicalProperties = new ArrayList<Map<String, Object>>();;

      // -- nouvelle couleur --//

      final Color newColor = couleur;

      // -- liste des widget selectionnees --//
      final java.util.List<EbliWidget> listeWidget = new ArrayList<EbliWidget>();

      // -- liste des nodes selectionnes --//
      final Set<EbliNode> listeNode = (Set<EbliNode>) scene_.getSelectedObjects();// scene_

      for (final Iterator<EbliNode> it = listeNode.iterator(); it.hasNext();) {

        final EbliNode currentNode = it.next();

        if (currentNode.hasWidget()) {

          final EbliWidget widget = currentNode.getWidget();

          // -- test pour savoir si le widget gere le colorcontour --//
          if (widget.getColorContour() != null) {

            // -- ajout pour les undo redo
            listeWidget.add(widget);
            oldGraphicalProperties.add(widget.duplicateGraphicalProperties());

            // -- mise a jour de la nouvelle couleur --//
            widget.setColorContour(newColor);

            // -- ajout de la nouvelle graphicalProperty --//
            newGraphicalProperties.add(widget.getPropGraphique());

          }

        }
      }
      // -- rafraichissement de la scene --//
      scene_.refresh();

      if (cmd_ != null) {
        cmd_
            .addCmd(new CommandeUndoRedoGraphicalProperties(newGraphicalProperties, oldGraphicalProperties, listeWidget));

      }

    }

  }

}
