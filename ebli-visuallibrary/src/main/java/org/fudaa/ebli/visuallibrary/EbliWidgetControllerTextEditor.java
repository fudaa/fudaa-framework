package org.fudaa.ebli.visuallibrary;

import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluHtmlEditorPanel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.actions.EbliActionEditorOneClick;
import org.netbeans.api.visual.action.InplaceEditorProvider;

/**
 * Controller de l'editeur de texte. Permet entre autre l'action edition pour
 * pouvoir y acc�der dans certains cas.
 * 
 * @author Adrien Hadoux
 * 
 */
public class EbliWidgetControllerTextEditor extends EbliWidgetControllerMenuOnly {

  EbliActionEditorOneClick<BuPanel> editorAction_;
  CtuluHtmlEditorPanel editor_;
  
  public EbliWidgetControllerTextEditor(EbliWidgetTextEditor _widget) {
    super(_widget);
    addActionSpecifiques();
    editor_ = _widget.editorPane_;
  }

  
  public void MenuEditer() {
    // -- executer l action d edition --//
    editorAction_.openEditorFromMenu(getWidget());
  }
  
  public void addActionSpecifiques() {
  
    editorAction_ = new EbliActionEditorOneClick<BuPanel>((InplaceEditorProvider) widget_);
    // -- ajout de l action au widget correspondant --//
    widget_.getActions().addAction(editorAction_);

  }
  
  @Override
  protected void buildPopupMenu(final JPopupMenu _menu) {
    constructPopupMenuSpecifique(_menu);
    // -- creation du menu commun a tous les widgets
    constructPopupMenuBase(_menu);
  }
  
  
  private void constructPopupMenuSpecifique(final JPopupMenu _popup) {
    JMenuItem menuItem = new JMenuItem(EbliResource.EBLI.getString("Editer le texte"));
    _popup.add(menuItem, 0);
    menuItem.setIcon(CtuluResource.CTULU.getIcon("crystal_editer"));
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        MenuEditer();
      }
    });
  }
  
  
  @Override
  public JToolBar getToolbarComponent() {
    if (editor_ != null)
      return editor_.getToolBar(true);
    else
      return new JToolBar();
  }
  
  JMenuBar menuBar_;
  
  @Override
  public JMenuBar getMenubarComponent() {
    if (editor_ == null)
      return new BuMenuBar();
    
    if (menuBar_ == null)
      menuBar_ = editor_.getMenuBar(true, false);
    return menuBar_;
     
  }
  
}
