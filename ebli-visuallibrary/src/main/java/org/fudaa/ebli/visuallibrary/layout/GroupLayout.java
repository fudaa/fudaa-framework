/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.layout;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.widget.Widget;

/**
 * Layout qui permet de gerer un groupe de widget. Le ratio initial est respect�: il doit etre sauve dans le prop
 * getChildConstraint
 * 
 * @author deniger
 */
public class GroupLayout implements Layout {

  @Override
  public void layout(final Widget widget) {
    final Collection<Widget> children = widget.getChildren();
    for (final Widget child : children) {
      if (child.isVisible()) {
        child.resolveBounds(child.getPreferredLocation(), null);
      } else {
        child.resolveBounds(null, new Rectangle());
      }
    }
  }

  /**
   * Recupere la prop ChildConstraint de chaque Widget afin de la placer dans le groupe
   */
  @Override
  public void justify(final Widget widget) {
    final Collection<Widget> children = widget.getChildren();
    final Rectangle r = widget.getClientArea();
    final float wParent = r.width;
    final float hParent = r.height;
    for (final Widget child : children) {
      final Rectangle2D.Float ratios = (Rectangle2D.Float) widget.getChildConstraint(child);
      final float minx = ratios.x * wParent;
      final float miny = ratios.y * hParent;
      final float maxx = wParent - ratios.width * wParent;
      final float maxy = hParent - ratios.height * hParent;
      final Rectangle rec = child.getBounds();
      rec.height = (int) Math.ceil(maxy - miny);
      rec.width = (int) Math.ceil(maxx - minx);
      child.resolveBounds(new Point(r.x + (int) (minx - rec.x), r.y + (int) (miny - rec.y)), rec);
    }

  }

  /**
       *
       */
  @Override
  public boolean requiresJustification(final Widget widget) {
    return true;
  }

}