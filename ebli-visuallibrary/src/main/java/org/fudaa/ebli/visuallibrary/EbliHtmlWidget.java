/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

/**
 * @author deniger
 */
public class EbliHtmlWidget extends Widget {

  View view;
  JLabel c = new JLabel();

  protected EbliHtmlWidget(Scene scene, String html) {
    super(scene);
    view = BasicHTML.createHTMLView(c, html);
    super.setCheckClipping(true);
  }

  public void setHtml(String html) {
    view = BasicHTML.createHTMLView(c, html);
  }

  @Override
  protected Rectangle calculateClientArea() {
     Rectangle rec = new Rectangle();
     // the preferred size is here
     rec.height = (int) view.getPreferredSpan(View.Y_AXIS);
     rec.width = (int) view.getPreferredSpan(View.X_AXIS);
     return rec;
  }

  /**
   *
   */
  @Override
  protected void paintWidget() {
    Rectangle clientArea = getClientArea();
    c.setPreferredSize(new Dimension(clientArea.width, clientArea.height));
    view.paint(getGraphics(), clientArea);
  }

}
