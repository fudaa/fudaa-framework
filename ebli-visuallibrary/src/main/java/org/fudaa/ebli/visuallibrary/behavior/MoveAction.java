package org.fudaa.ebli.visuallibrary.behavior;

import java.awt.Point;
import java.awt.event.MouseEvent;
import org.netbeans.api.visual.action.MoveProvider;
import org.netbeans.api.visual.action.MoveStrategy;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;

/**
 * Classe moveAction specifique aux widget. (pour certaines widget sattellites,
 * leur deplacement doit etre aussi r�alis�)
 * 
 * @author Adrien Hadoux
 * 
 */
public class MoveAction extends WidgetAction.LockedAdapter {

  
 
  
  private MoveStrategy strategy;
  private MoveProvider provider;

  private Widget movingWidget = null;
  private Point dragSceneLocation = null;
  private Point originalSceneLocation = null;
  private Point initialMouseLocation = null;

  public MoveAction(MoveStrategy strategy, MoveProvider provider) {
    this.strategy = strategy;
    this.provider = provider;
  }

  @Override
  protected boolean isLocked() {
    return movingWidget != null;
  }

  @Override
  public State mousePressed(Widget widget, WidgetMouseEvent event) {
    if (isLocked())
      return State.createLocked(widget, this);
    if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 1) {
      movingWidget = widget;
      initialMouseLocation = event.getPoint();
      originalSceneLocation = provider.getOriginalLocation(widget);
      if (originalSceneLocation == null)
        originalSceneLocation = new Point();
      dragSceneLocation = widget.convertLocalToScene(event.getPoint());
      provider.movementStarted(widget);
      return State.createLocked(widget, this);
    }
    return State.REJECTED;
  }

  @Override
  public State mouseReleased(Widget widget, WidgetMouseEvent event) {
    boolean state;
    if (initialMouseLocation != null && initialMouseLocation.equals(event.getPoint()))
      state = true;
    else
      state = move(widget, event.getPoint());
    if (state) {
      movingWidget = null;
      dragSceneLocation = null;
      originalSceneLocation = null;
      initialMouseLocation = null;
      provider.movementFinished(widget);
    }
    return state ? State.CONSUMED : State.REJECTED;
  }

  @Override
  public State mouseDragged(Widget widget, WidgetMouseEvent event) {
    return move(widget, event.getPoint()) ? State.createLocked(widget, this) : State.REJECTED;
  }

  private boolean move(Widget widget, Point newLocation) {
    if (movingWidget != widget)
      return false;
    initialMouseLocation = null;
    newLocation = widget.convertLocalToScene(newLocation);
    Point location = new Point(originalSceneLocation.x + newLocation.x - dragSceneLocation.x, originalSceneLocation.y
        + newLocation.y - dragSceneLocation.y);
    provider.setNewLocation(widget, strategy.locationSuggested(widget, originalSceneLocation, location));
    
   
    
    return true;
  }

}
