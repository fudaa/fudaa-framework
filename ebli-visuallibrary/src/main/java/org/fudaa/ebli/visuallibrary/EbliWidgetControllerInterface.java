/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

/**
 * @author deniger
 */
public interface EbliWidgetControllerInterface {

  JMenuBar getMenubarComponent();

  JComponent getOverviewComponent();

  JToolBar getToolbarComponent();

  JComponent getTracableComponent();

  boolean isEditable();
  
  /**
   * @return true if the widget can export datas
   */
  boolean isDataExportable();

}