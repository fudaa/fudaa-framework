/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetGroup;
import org.netbeans.api.visual.widget.Widget;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public class EbliWidgetActionUngroup extends EbliWidgetActionAbstract {
  EbliNode target_;

  public EbliWidgetActionUngroup(final EbliScene _scene, final EbliNode _target) {
    super(_scene, EbliLib.getS("D�grouper"), EbliResource.EBLI.getToolIcon("formatungroup_16.png"), "DEGROUP_WIDGET");
    target_ = _target;
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    degroupObjects(scene_, target_);
  }

  public static void degroupObjects(final EbliScene _scene, final EbliNode _nodeToDegroup) {
    if (_nodeToDegroup == null) {
      return;
    }
    EbliWidget widgetOfNodeToDegroup = (EbliWidget) _scene.findWidget(_nodeToDegroup);
    if (widgetOfNodeToDegroup == null) {
      widgetOfNodeToDegroup = _nodeToDegroup.getCreator().getWidget();
    }
    if (widgetOfNodeToDegroup == null || !widgetOfNodeToDegroup.isGroup()) {
      return;
    }
    final List<Widget> children = new ArrayList<Widget>(widgetOfNodeToDegroup.getChildren());
    for (final Widget widget : children) {
      final Point p = widget.getLocation();
      final Point toScene = widgetOfNodeToDegroup.convertLocalToScene(p);
      final EbliNode n = (EbliNode) _scene.findObject(widget);
      Set<EbliWidget> listeSattellites = ((EbliWidget) widget).getSatellites();
      // warn must do it to avoid further delete
      if (listeSattellites != null) {
        listeSattellites = new HashSet<EbliWidget>(listeSattellites);
      }

      widget.removeFromParent();

      _nodeToDegroup.getCreator().setPreferredLocation(toScene);

      // -- on essaie de recuperere les proportions --//
      // getPropDimension
      final Dimension sizeProp = ((EbliWidgetGroup) _nodeToDegroup.getWidget().getIntern())
          .getPropDimension((EbliWidget) widget);
      if (sizeProp != null) {
        _nodeToDegroup.getCreator().setPreferredSize(sizeProp);
      } else {
        _nodeToDegroup.getCreator().setPreferredSize(widget.getPreferredSize());
      }

      _scene.addNode(n).setPreferredLocation(toScene);
      n.getWidget().getController().activeActionsResizeAndMove();
      if (listeSattellites != null) {
        n.getWidget().setSatellite(listeSattellites);
      }
    }
    _scene.removeNode(_nodeToDegroup);
    _scene.validate();
  }
}
