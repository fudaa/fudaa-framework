package org.fudaa.ebli.visuallibrary.creator;

import java.util.Map;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetFlecheSimple;

/**
 * Creator de fleche simple sans contenu.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorFlecheSimple extends EbliWidgetCreator {

  int orientation;

  public EbliWidgetCreatorFlecheSimple(final int orientation) {
    super();
    this.orientation = orientation;

  }

  public EbliWidgetCreatorFlecheSimple() {

  }

  public EbliWidgetCreatorFlecheSimple(EbliWidgetCreatorFlecheSimple copy) {
    super(copy);
    orientation = copy.orientation;

  }

  public int getOrientation() {
    return orientation;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    return new EbliWidgetFlecheSimple(_scene, orientation);
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorFlecheSimple(this);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return null;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    return true;
  }

}
