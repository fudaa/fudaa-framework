package org.fudaa.ebli.visuallibrary;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.beans.PropertyChangeListener;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;

public final class EbliPrintOverviewLayer extends LayerWidget implements BSelecteurTargetInterface {

  private MediaSizeName currentMediaSizeName = MediaSizeName.ISO_A4;

  private int orientation = PageFormat.PORTRAIT;

  private Dimension pageFormat;

  private TraceLigne traceLigne;

  public static String PROP_ORIENTATION_LANDSCAPE = "ORIENTATION_LANDSCAPE";
  public static String PROP_MEDIANAME = "MEDIANAME";

  EbliPrintOverviewLayer(Scene scene) {
    super(scene);
    traceLigne = new TraceLigne();
    traceLigne.setEpaisseur(2f);
    traceLigne.setCouleur(Color.RED);
    setVisible(false);
  }

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {

  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {

  }

  @Override
  public Object getMin(String _key) {
    return null;
  }

  @Override
  public Object getMoy(String _key) {
    return null;
  }

  @Override
  public Object getProperty(String _key) {
    if (EbliWidget.VISIBLE.equals(_key)) {
      return Boolean.valueOf(isVisible());
    }
    if (EbliWidget.LINEMODEL.equals(_key)) {
      return traceLigne.getModel();
    }
    if (PROP_ORIENTATION_LANDSCAPE.equals(_key)) {
      return Boolean.valueOf(orientation == PageFormat.LANDSCAPE);
    }
    
    if (PROP_MEDIANAME.equals(_key)) {
      return getCurrentMediaSizeName();
    }

    return null;
  }

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    if (EbliWidget.VISIBLE.equals(_key)) {
      if (_newProp != null) {
        setVisible(((Boolean) _newProp).booleanValue());
      }

    }
    if (EbliWidget.LINEMODEL.equals(_key)) {
      traceLigne.getModel().updateData((TraceLigneModel) _newProp);
    }
    if (PROP_ORIENTATION_LANDSCAPE.equals(_key)) {
      setOrientation(Boolean.TRUE.equals(_newProp) ? PageFormat.LANDSCAPE : PageFormat.PORTRAIT);
    }
    if (PROP_MEDIANAME.equals(_key)) {
      setCurrentMediaSizeName((MediaSizeName) _newProp);
    }
    EbliScene.refreshScene(getScene());
    return true;
  }

  public MediaSizeName getCurrentMediaSizeName() {
    return currentMediaSizeName;
  }

  public int getOrientation() {
    return orientation;
  }

  public TraceLigne getTraceLigne() {
    return traceLigne;
  }

  @Override
  protected void paintWidget() {
    if (!isVisible()) {
      return;
    }
    final Graphics2D g = getGraphics();
    Dimension pg = retrievePageFormat();
    int h = (int) pg.getHeight();
    int w = (int) pg.getWidth();
    if (orientation == PageFormat.LANDSCAPE) {
      int tmp = h;
      h = w;
      w = tmp;
    }
    traceLigne.dessineRectangle(g, 0, 0, w, h);
    traceLigne.dessineTrait(g, 0, 0, w, h);
    traceLigne.dessineTrait(g, w, 0, 0, h);

  }

  private Dimension retrievePageFormat() {
    if (pageFormat == null) {
      MediaSize mediaSizeForName = MediaSize.getMediaSizeForName(currentMediaSizeName);
      int w = (int) EbliPageFormat.cmVersPixel((mediaSizeForName.getX(MediaSize.MM)-30) / 10d);
      int h = (int) EbliPageFormat.cmVersPixel((mediaSizeForName.getY(MediaSize.MM)-30) / 10d);
      pageFormat = new Dimension(w, h);
    }
    return pageFormat;
  }

  public void setCurrentMediaSizeName(MediaSizeName currentMediaSizeName) {
    this.currentMediaSizeName = currentMediaSizeName;
    pageFormat = null;
  }

  public void setOrientation(int orientation) {
    this.orientation = orientation;
    pageFormat = null;
  }

  public void setTraceLigne(TraceLigne traceLigne) {
    this.traceLigne = traceLigne;
  }

  public void refresh() {
    EbliScene.refreshScene(getScene());
  }
}