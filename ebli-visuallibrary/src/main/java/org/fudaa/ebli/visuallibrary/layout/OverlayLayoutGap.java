package org.fudaa.ebli.visuallibrary.layout;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.widget.Widget;

public class OverlayLayoutGap implements Layout {

  Insets inGaps_;

  public OverlayLayoutGap(final Insets _inGaps) {
    super();
    inGaps_ = _inGaps;
  }

  @Override
  public void layout(final Widget widget) {
    final Dimension total = new Dimension();
    for (final Widget child : widget.getChildren()) {
      if (!child.isVisible()) continue;
      final Dimension size = child.getPreferredBounds().getSize();
      if (size.width > total.width) total.width = size.width;
      if (size.height > total.height) total.height = size.height;
    }
    total.width += inGaps_.left + inGaps_.right;
    total.height += inGaps_.top + inGaps_.bottom;
    for (final Widget child : widget.getChildren()) {
      final Point location = child.getPreferredBounds().getLocation();
      child.resolveBounds(new Point(-location.x + inGaps_.left, -location.y + inGaps_.top), new Rectangle(location,
          total));
    }
  }

  @Override
  public boolean requiresJustification(final Widget widget) {
    return true;
  }

  @Override
  public void justify(final Widget widget) {
    final Rectangle clientArea = widget.getClientArea();
    clientArea.x += inGaps_.left;
    clientArea.y += inGaps_.top;
    clientArea.width -= inGaps_.left + inGaps_.right;
    clientArea.height -= inGaps_.top + inGaps_.bottom;
    for (final Widget child : widget.getChildren()) {
      if (child.isVisible()) {
        final Point location = child.getPreferredBounds().getLocation();
        child.resolveBounds(new Point(clientArea.x - location.x, clientArea.y - location.y), new Rectangle(location,
            clientArea.getSize()));
      } else {
        child.resolveBounds(clientArea.getLocation(), new Rectangle());
      }
    }
  }

  /**
   * @param inGaps the inGaps_ to set
   */
  public void setInGaps(Insets inGaps) {
    inGaps_ = inGaps;
  }

}
