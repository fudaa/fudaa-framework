package org.fudaa.ebli.visuallibrary.graphe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import org.fudaa.ebli.visuallibrary.calque.ZCalqueSondeSynchroniserFusion.SondeCouple;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

/**
 * Tree cell Renderer classique pour afficher les icones mignature a cot� des objets
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class GrapheTreeCellRenderer extends JLabel implements TreeCellRenderer {
  private final Color HIGHLIGHT_COLOR = UIManager.getColor("Tree.selectionBackground");
  private final DefaultTreeCellRenderer rendererClassique_ = new DefaultTreeCellRenderer();
  JLabel defaultZone_ = new JLabel("");

  public GrapheTreeCellRenderer() {
    setOpaque(true);
    setIconTextGap(12);
    setSize(150, 100);
    setMinimumSize(new Dimension(150, 100));

  }

  @Override
  public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
      final boolean _expanded, final boolean _leaf, final int _row, final boolean _hasFocus) {

    final DefaultMutableTreeTableNode node = (DefaultMutableTreeTableNode) _value;

    if (node.getUserObject() instanceof JLabel) {
      final JLabel entry = (JLabel) node.getUserObject();

      setText(entry.getText());
      setIcon(entry.getIcon());
      if (_selected) {
        setBackground(HIGHLIGHT_COLOR);
        setForeground(Color.white);
      } else {
        setBackground(Color.white);
        setForeground(Color.black);
      }
      return this;
    } else if (node.getUserObject() instanceof SondeCouple) {
      final SondeCouple entry = (SondeCouple) node.getUserObject();
      defaultZone_.setText(entry.name);
      return defaultZone_;
    } else return rendererClassique_.getTreeCellRendererComponent(_tree, _value, _selected, _expanded, _leaf, _row,
        _hasFocus);

  }

}
