package org.fudaa.ebli.visuallibrary;

import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuToolBar;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import org.fudaa.ebli.visuallibrary.behavior.EbliAlignWithMoveStrategyProvider;
import org.fudaa.ebli.visuallibrary.behavior.EbliWidgetActionFactory;
import org.fudaa.ebli.visuallibrary.tree.EbliJXTreeTableCellRenderer;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.model.ObjectSceneEventType;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.action.SingleLayerAlignWithWidgetCollector;

public class EbliSceneController implements EbliWidgetControllerInterface {
  final private WidgetAction alignWithMoveAction;

  final private WidgetAction alignWithResizeAction;

  // final private WidgetAction alignWithResizeActionProportionnal;

  Set<EbliWidgetEditModeListener> listeners_ = new HashSet<EbliWidgetEditModeListener>();
  BuMenuBar menuCalque_ = null;

  public final WidgetAction rectangularSelection_;

  JScrollPane sc_;
  EbliScene scene_;

  BuToolBar toolbarCalque_ = null;

  /**
   * Tree qui contient la hierarchie des widget de la scene.
   */
  JXTreeTable treeScene_ = null;

  public EbliSceneController(final EbliScene _scene) {

    scene_ = _scene;
    final SingleLayerAlignWithWidgetCollector widgetCollector = new SingleLayerAlignWithWidgetCollector(scene_
        .getVisu(), false);
    final EbliAlignWithMoveStrategyProvider sp = new EbliAlignWithMoveStrategyProvider(widgetCollector, scene_
        .getInteractionLayer(), ActionFactory.createDefaultAlignWithMoveDecorator(), false);
    alignWithMoveAction = EbliWidgetActionFactory.createMoveAction(sp, sp);


    alignWithResizeAction = EbliWidgetActionFactory.createAlignWithProportionnalResizeAction(scene_.getVisu(), scene_
        .getInteractionLayer(), ActionFactory.createDefaultAlignWithMoveDecorator(), false);

    rectangularSelection_ = ActionFactory.createRectangularSelectAction(scene_, scene_.getInteractionLayer());
    // -- ajoute l'action du zoom ( ctrl + clic)
    // suppression pour eviter les bugs sioux
    scene_.getActions().addAction(rectangularSelection_);

    // -- ajouter le menu popup de base
    setMenuBase();

  }
  

  @Override
  public boolean isEditable() {
    return false;
  }

  public void addEditListener(final EbliWidgetEditModeListener _l) {
    listeners_.add(_l);
  }

  public void fireEditStart(final Widget _w, JComponent _editor) {
    for (final EbliWidgetEditModeListener l : listeners_) {
      l.editStart(_w, _editor);
    }
  }

  public void fireEditStop(final Widget w, JComponent editor) {
    for (final EbliWidgetEditModeListener l : listeners_) {
      l.editStop(w, editor);
    }
  }

  @Override
  public BuMenuBar getMenubarComponent() {
    return null;
  }

  public WidgetAction getMoveAction() {
    return alignWithMoveAction;
  }

  /**
   * Methode qui retourne ou cree le tree associe a la scene
   * 
   * @return
   */
  @Override
  public JComponent getOverviewComponent() {

    if (sc_ != null) {

      return sc_;
    }

    treeScene_ = new JXTreeTable(scene_.getTreeModel());

    // -- ajout du tree au model pour les dimensionnements --//

    // --vision du noeud root --//
    treeScene_.setRootVisible(false);

    // --ajout du renderer pour les images-//
    treeScene_.setTreeCellRenderer(new EbliJXTreeTableCellRenderer());

    // -- ouvert par defaut --//
    treeScene_.expandAll();

    // -- ajout des highlighters customs --//
    treeScene_.setHighlighters(HighlighterFactory.createSimpleStriping());
    treeScene_.setShowsRootHandles(true);
    // -- selection unique --//
    treeScene_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    treeScene_.setEditable(true);

    // --dimensions des colonnes --//
    treeScene_.getColumn(0).setPreferredWidth(60);
    treeScene_.getColumn(1).setPreferredWidth(50);
    treeScene_.getColumn(2).setPreferredWidth(5);
    // FIXME Adrien: si on edite du texte dans le jxtreetable els colonnes se
    // redimensionnents
    treeScene_.getColumn(0).setResizable(false);
    treeScene_.getColumn(1).setResizable(false);
    treeScene_.getColumn(2).setResizable(false);
    treeScene_.validate();

    // -- ajout du synchroniser du tree/scene --//

    scene_.addObjectSceneListener(new EbliWidgetSynchroniser(scene_.getTreeModel(), treeScene_),
        ObjectSceneEventType.OBJECT_SELECTION_CHANGED);
    sc_ = new JScrollPane(treeScene_);

    return sc_;

  }

  /**
   * @return the rectangularSelection
   */
  public WidgetAction getRectangularSelection() {
    return rectangularSelection_;
  }

  public WidgetAction getResizeAction() {
    return alignWithResizeAction;
  }


  @Override
  public BuToolBar getToolbarComponent() {
    return null;
  }

  @Override
  public JComponent getTracableComponent() {
    return null;
  }

  public void removeEditListener(final EbliWidgetEditModeListener _l) {
    listeners_.remove(_l);
  }

  public void setMenuBase() {
  }



  @Override
  public boolean isDataExportable() {
    return false;
  }

}
