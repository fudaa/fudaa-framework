package org.fudaa.ebli.visuallibrary.calque;

import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliWidgetControllerMenuOnly;
import org.fudaa.ebli.visuallibrary.EbliWidgetWithBordure;

/**
 * controller specifique aux legendes. gere
 * 
 * @author genesis
 */
public class EbliWidgetControllerLegendeTime extends EbliWidgetControllerMenuOnly {

  public EbliWidgetControllerLegendeTime(final EbliWidgetWithBordure widget) {
    super(widget);
  }

  @Override
  protected void buildPopupMenu(final JPopupMenu _menu) {
    constructPopupMenuSpecifique(_menu);
    // -- creation du menu commun a tous les widgets
    constructPopupMenuBase(_menu);
  }

  private void constructPopupMenuSpecifique(final JPopupMenu _popup) {
    final EbliActionChangeState displayTitleTime = new EbliActionChangeState(EbliResource.EBLI.getString("Afficher le titre"),
        EbliResource.EBLI.getIcon("crystal_voir"), "VIEW_TITLE") {

      @Override
      public void changeAction() {
        if (isSelected()) {
          getTimeLegendWidget().setTitleVisible(true);
        } else {
          getTimeLegendWidget().setTitleVisible(false);
        }

      }

    };
    _popup.add(EbliComponentFactory.INSTANCE.buildMenuItem(displayTitleTime));
    _popup.addPopupMenuListener(new PopupMenuListener() {

      @Override
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        displayTitleTime.setSelected(isTitleVisible());
      }

      @Override
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

      }

      @Override
      public void popupMenuCanceled(PopupMenuEvent e) {

      }
    });

  }

  protected boolean isTitleVisible() {
    EbliWidgetTimeLegende intern = getTimeLegendWidget();
    return intern.isTitleVisible();
  }

  private EbliWidgetTimeLegende getTimeLegendWidget() {
    return (EbliWidgetTimeLegende) getWidget().getIntern();
  }

  @Override
  public boolean isEditable() {
    return true;
  }


}
