/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import javax.swing.JLabel;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import org.fudaa.ctulu.CtuluLibString;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXLabel.MultiLineSupport;
import org.netbeans.api.visual.widget.Scene;

/**
 * @author deniger
 */
public class EbliWidgetWrappedLine extends EbliWidgetWithView {

  JXLabel label = new JXLabel();
  View view;

  // private static Basic

  public EbliWidgetWrappedLine(Scene scene) {
    this(scene, CtuluLibString.EMPTY_STRING);
  }

  protected EbliWidgetWrappedLine(Scene scene, String txt) {
    super(scene);
    setText(txt);
  }

  public void setText(String txt) {
    label.setText(txt);
    view = MultiLineSupport.createView(label);
  }

  public String getText() {
    return label.getText();
  }

  @Override
  protected View refreshView() {
    view = BasicHTML.createHTMLView(label, label.getText());
    return view;
  }

  /**
   * @param textPosition
   * @see javax.swing.JLabel#setVerticalTextPosition(int)
   */
  public void setVerticalTextPosition(int textPosition) {
    label.setVerticalTextPosition(textPosition);
  }

  @Override
  protected JLabel getLabel() {
    return label;
  }

  @Override
  protected View getView() {
    return view;
  }

}
