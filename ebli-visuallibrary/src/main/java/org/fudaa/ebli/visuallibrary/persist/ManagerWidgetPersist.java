package org.fudaa.ebli.visuallibrary.persist;

import com.memoire.fu.FuLib;
import java.io.File;
import org.fudaa.ebli.visuallibrary.EbliNode;

/**
 * Manager qui g�re la sauvegarde de plusieurs layout au sein d'un fichier xml. Gere le repertoire contenant l'ensemble
 * des donn�es.
 * 
 * @author Adrien Hadoux
 */
public class ManagerWidgetPersist {

  /**
   * Methode qui genere le path vers le graphe.
   * 
   * @return
   */
  public static String generateGraphPath(final String path, final EbliNode nodeGraphe) {

    return path + File.separator + File.separator + FuLib.clean(nodeGraphe.getTitle()) + nodeGraphe.getWidget().getId();

  }

  /**
   * Genere un path vers l objetc calque
   * 
   * @param path
   * @param nodeCalque
   * @return
   */
  public static String generateCalquePath(final String path, final EbliNode nodeCalque) {

    return path + File.separator + File.separator + FuLib.clean(nodeCalque.getTitle()) + nodeCalque.getWidget().getId();

  }

}
