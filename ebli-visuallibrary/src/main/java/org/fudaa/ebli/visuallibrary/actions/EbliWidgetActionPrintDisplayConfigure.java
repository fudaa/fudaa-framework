package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuResource;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.JDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurePalette;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * Action qui permet de configurer les composants en ouvrant les interfaces qui vont bien. Ouvre la dialog de choix des
 * composants
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class EbliWidgetActionPrintDisplayConfigure extends EbliWidgetActionAbstract {

  public EbliWidgetActionPrintDisplayConfigure(final EbliScene _scene) {
    super(_scene, EbliLib.getS("Impression"), BuResource.BU.getIcon("miseenpage"), "PRINT_CONFIGURE");

  }

  /**
     *
     */
  @Override
  public void actionPerformed(final ActionEvent e) {
    final EbliScene found = getScene();
    configure(found);
  }

  public static void configure(final EbliScene found) {
    if (found != null) {
      final BConfigurePalette palette = new BConfigurePalette(false, null, null);
      PrintWidgetConfigure conf = new PrintWidgetConfigure(found);
      BConfigurableComposite comp = new BConfigurableComposite(conf, EbliLib.getS("Zone d'impression"));
      palette.setTargetConf(comp);
      palette.setPanelTitleVisible(false);

      final JDialog d = CtuluLibSwing.createDialogOnActiveWindow(EbliLib.getS("Zone d'impression"));
      d.setModal(true);
      d.setContentPane(palette);
      Dimension dim = palette.getPreferredSize();
      dim.height = Math.max(dim.height, 550);
      dim.width = Math.max(dim.width, 500);
      palette.setPreferredSize(dim);
      d.pack();
      d.setLocationRelativeTo(d.getParent());
      d.setVisible(true);
    }
  }

}
