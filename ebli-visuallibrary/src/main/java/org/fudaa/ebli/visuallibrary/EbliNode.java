package org.fudaa.ebli.visuallibrary;

import java.util.List;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;

public interface EbliNode {

  String getTitle();

  void setTitle(String title);

  EbliWidgetCreator getCreator();

  EbliWidgetController getController();

  void setCreator(EbliWidgetCreator creator);

  /**
   * Permet de dupliquer toutes les donnees du noeud. Si le noeud possede des satellites, le r�sultat comprend les
   * noeuds des satellites dupliques dans la liste.
   * Le premier de la liste est le noeud duplique.
   * 
   * @return les noeuds dupliques avec en premier le duplicata de ce noeud
   */
  List<EbliNode> duplicate();

  /**
   * Methode qui verifie que le node a une widget
   * 
   * @see EbliWidgetCreator#isWidgetCreated()
   * @return true si la widget associ�e a �t� cr��
   */
  public boolean hasWidget();

  /**
   * obtient la widget associee
   * 
   * @return la widget du creator.
   */
  public EbliWidget getWidget();

  public List<EbliNode> duplicateInSameScene();

  /**
   * @return the description
   */
  public String getDescription();

  public void setDescription(final String _description);

}
