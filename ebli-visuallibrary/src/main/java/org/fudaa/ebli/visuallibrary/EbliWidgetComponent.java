/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.AffineTransform;
import javax.swing.JComponent;
import org.netbeans.api.visual.widget.Scene;

/**
 * @author deniger
 */
public class EbliWidgetComponent extends EbliWidget {

  private final Component component;
  private boolean componentAdded;
  private boolean widgetAdded;
  private double zoomFactor = Double.MIN_VALUE;
  private ComponentSceneListener validateListener;
  private final ComponentComponentListener componentListener;
  private boolean componentVisible = false;

  /**
   * Creates a component widget.
   * 
   * @param scene the scene
   * @param component the AWT/Swing component
   */
  public EbliWidgetComponent(final EbliScene scene, final Component component) {
    super(scene);
    this.component = component;
    validateListener = null;
    componentListener = new ComponentComponentListener();
    setComponentVisible(true);
  }

  /**
   * Returns a AWT/Swing component.
   * 
   * @return the AWT/Swing component
   */
  public final Component getComponent() {
    return component;
  }

  /**
   * Returns whether the component should be visible.
   * 
   * @return true if the component is visible
   */
  public final boolean isComponentVisible() {
    return componentVisible;
  }

  /**
   * Sets whether the component should be visible.
   * 
   * @param componentVisible if true, then the component is visible
   */
  public final void setComponentVisible(final boolean componentVisible) {
    if (this.componentVisible == componentVisible) return;
    this.componentVisible = componentVisible;
    attach();
    revalidate();
  }

  @Override
  protected final void notifyAdded() {
    widgetAdded = true;
    attach();
  }

  @Override
  protected final void notifyRemoved() {
    widgetAdded = false;
  }

  private void attach() {
    if (validateListener != null) return;
    validateListener = new ComponentSceneListener();
    getScene().addSceneListener(validateListener);
  }

  private void detach() {
    if (validateListener == null) return;
    getScene().removeSceneListener(validateListener);
    validateListener = null;
  }

  /**
   * Calculates a client area from the preferred size of the component.
   * 
   * @return the calculated client area
   */
  @Override
  protected final Rectangle calculateClientArea() {
    final Dimension preferredSize = component.getPreferredSize();
    zoomFactor = getScene().getZoomFactor();
    preferredSize.width = (int) Math.floor(preferredSize.width / zoomFactor);
    preferredSize.height = (int) Math.floor(preferredSize.height / zoomFactor);
    return new Rectangle(preferredSize);
  }

  private void addComponent() {
    final Scene scene = getScene();
    if (!componentAdded) {
      scene.getView().add(component);
      scene.getView().revalidate();
      component.addComponentListener(componentListener);
      componentAdded = true;
    }
    component.removeComponentListener(componentListener);
    component.setBounds(scene.convertSceneToView(convertLocalToScene(getClientArea())));
    component.addComponentListener(componentListener);
    component.repaint();
  }

  private void removeComponent() {
    final Scene scene = getScene();
    if (componentAdded) {
      component.removeComponentListener(componentListener);
      scene.getView().remove(component);
      scene.getView().revalidate();
      componentAdded = false;
    }
  }

  /**
   * Paints the component widget.
   */
  @Override
  protected final void paintWidget() {
    if (!componentVisible) {
      final boolean isDoubleBuffered = component instanceof JComponent && component.isDoubleBuffered();
      if (isDoubleBuffered) ((JComponent) component).setDoubleBuffered(false);
      final Graphics2D graphics = getGraphics();
      final Rectangle bounds = getClientArea();
      final AffineTransform previousTransform = graphics.getTransform();
      graphics.translate(bounds.x, bounds.y);
      final double factor = getScene().getZoomFactor();
      graphics.scale(1 / factor, 1 / factor);
      component.print(graphics);
      graphics.setTransform(previousTransform);
      if (isDoubleBuffered) ((JComponent) component).setDoubleBuffered(isDoubleBuffered);
    }
  }

  private final class ComponentSceneListener implements Scene.SceneListener {

    @Override
    public void sceneRepaint() {}

    @Override
    public void sceneValidating() {
      final double newZoomFactor = getScene().getZoomFactor();
      if (Math.abs(newZoomFactor - zoomFactor) != 0.0) {
        revalidate();
        zoomFactor = newZoomFactor;
      }
    }

    @Override
    public void sceneValidated() {
      if (widgetAdded && componentVisible) addComponent();
      else {
        removeComponent();
        detach();
      }
    }
  }

  private final class ComponentComponentListener implements ComponentListener {

    @Override
    public void componentResized(final ComponentEvent e) {
      revalidate();
    }

    @Override
    public void componentMoved(final ComponentEvent e) {
      revalidate();
    }

    @Override
    public void componentShown(final ComponentEvent e) {}

    @Override
    public void componentHidden(final ComponentEvent e) {}

  }

}
