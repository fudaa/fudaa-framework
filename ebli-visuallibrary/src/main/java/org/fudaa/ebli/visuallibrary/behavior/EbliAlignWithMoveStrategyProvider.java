/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.behavior;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.WidgetForwarder;
import org.fudaa.ebli.visuallibrary.actions.CommandMove;
import org.netbeans.api.visual.action.AlignWithMoveDecorator;
import org.netbeans.api.visual.action.AlignWithWidgetCollector;
import org.netbeans.api.visual.action.MoveProvider;
import org.netbeans.api.visual.action.MoveStrategy;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.action.AlignWithMoveStrategyProvider;

/**
 * @author deniger
 */
public class EbliAlignWithMoveStrategyProvider implements MoveStrategy, MoveProvider {

  /**
   * @param widget
   */
  public static Widget getWidgetToMove(final Widget widget) {
    if (widget instanceof WidgetForwarder) {
      final WidgetForwarder w = (WidgetForwarder) widget;
      final Widget group = w.getWidgetToMove();
      if (group != null) {
        return group;
      }
    }
    return widget;
  }

  final AlignWithMoveStrategyProvider alignProvider_;

  List<Widget> movedWidgets;

  List<Point> oldLocations;

  public EbliAlignWithMoveStrategyProvider(final AlignWithWidgetCollector collector, final LayerWidget interractionLayer,
      final AlignWithMoveDecorator decorator, final boolean outerBounds) {
    alignProvider_ = new AlignWithMoveStrategyProvider(collector, interractionLayer, decorator, outerBounds);
  }

  /**
   * @param _widget
   * @return
   * @see org.netbeans.modules.visual.action.AlignWithMoveStrategyProvider#getOriginalLocation(org.netbeans.api.visual.widget.Widget)
   */
  @Override
  public Point getOriginalLocation(final Widget _widget) {
    return getWidgetToMove(_widget).getPreferredLocation();
  }

  /**
   * {@inheritDoc}
   */
  public void hide() {
    alignProvider_.hide();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Point locationSuggested(final Widget _widget, final Point _originalLocation, final Point _suggestedLocation) {
    return alignProvider_.locationSuggested(getWidgetToMove(_widget), _originalLocation, _suggestedLocation);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void movementFinished(final Widget _widget) {
    Widget widgetToMove = getWidgetToMove(_widget);
    alignProvider_.movementFinished(widgetToMove);
    if (_widget instanceof EbliWidget && !oldLocations.get(0).equals(widgetToMove.getPreferredLocation())) {
      List<Point> newLocations = new ArrayList<Point>();
      newLocations.add(widgetToMove.getPreferredLocation());
      if (widgetToMove instanceof EbliWidget) {
        EbliWidget ebliWidget = (EbliWidget) widgetToMove;
        if (ebliWidget.hasSattelite()) {
          for (EbliWidget satt : ebliWidget.getSatellites()) {
            newLocations.add(satt.getPreferredLocation());
          }
        }
      }
      Scene scene = widgetToMove.getScene();
      if (scene instanceof EbliScene) {
        ((EbliScene) scene).getCmdMng().addCmd(new CommandMove(movedWidgets, oldLocations, newLocations));
      }

      oldLocations = null;
      movedWidgets = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void movementStarted(final Widget _widget) {
    Widget widgetToMove = getWidgetToMove(_widget);
    alignProvider_.movementStarted(widgetToMove);
    oldLocations = new ArrayList<Point>();
    oldLocations.add(widgetToMove.getLocation());
    movedWidgets = new ArrayList<Widget>();
    movedWidgets.add(widgetToMove);
    if (widgetToMove instanceof EbliWidget) {
      EbliWidget ebliWidget = (EbliWidget) widgetToMove;
      if (ebliWidget.hasSattelite()) {
        for (EbliWidget satt : ebliWidget.getSatellites()) {
          movedWidgets.add(satt);
          oldLocations.add(satt.getLocation());
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setNewLocation(final Widget _widget, final Point _location) {
    Widget widgetToMove = getWidgetToMove(_widget);
    Point oldLocation = widgetToMove.getLocation();
    widgetToMove.setPreferredLocation(_location);
    if (widgetToMove instanceof EbliWidget) {
      // -- cas particulier pour les widgets sattelites --//
      EbliWidget ebliwidget = (EbliWidget) widgetToMove;
      if (ebliwidget.hasSattelite()) {
        // -- on deplace ses widget sattelite a la location relative a leur
        // location d'origine --//
        for (Widget satt : ebliwidget.getSatellites()) {
          int translationX = _location.x - oldLocation.x;
          int translationY = _location.y - oldLocation.y;
          Point newLocationSatt = new Point(satt.getLocation().x + translationX, satt.getLocation().y + translationY);
          satt.setPreferredLocation(newLocationSatt);
        }
      }
      ebliwidget.notifyWidgetMoved(_location);
    }
  }

  public void show() {
    alignProvider_.show();
  }

}
