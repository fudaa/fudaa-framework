package org.fudaa.ebli.visuallibrary;

/**
 * Classe qui herite du controller. Gere uniquement les menus. Pas d actions gerees par ce controlleur.
 * 
 * @author Adrien Hadoux.
 */
public class EbliWidgetControllerMenuOnly extends EbliWidgetController {

  public EbliWidgetControllerMenuOnly(final EbliWidget widget) {
    this(widget, true);
  }

  public EbliWidgetControllerMenuOnly(final EbliWidget widget, final boolean canDuplicate) {
    super(widget, true, true, canDuplicate);

  }


}
