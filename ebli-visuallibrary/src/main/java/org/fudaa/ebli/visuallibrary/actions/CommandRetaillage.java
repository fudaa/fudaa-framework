package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Rectangle;
import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * Commande pour le undo redo sur les retaillages.
 * 
 * @author Adrien Hadoux
 */
public class CommandRetaillage implements CtuluCommand {

  List<Rectangle> oldRectangles;
  List<Rectangle> newRectangle;
  java.util.List<EbliWidget> widgets_;

  public CommandRetaillage(final List<EbliWidget> listeWidget, final List<Rectangle> oldRectangle,
      final List<Rectangle> newRectangle) {
    super();
    this.widgets_ = listeWidget;
    this.newRectangle = newRectangle;
    this.oldRectangles = oldRectangle;
    if (widgets_.size() != oldRectangle.size()) throw new IllegalArgumentException("list must ahava the same size");

  }

  @Override
  public void undo() {
    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).setPreferredBounds(oldRectangles.get(i));

    }
    refreshScene();
  }

  private void refreshScene() {
    EbliScene.refreshScene(widgets_.get(0).getScene());
  }

  @Override
  public void redo() {
    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).setPreferredBounds(newRectangle.get(i));

    }
    refreshScene();
  }

}