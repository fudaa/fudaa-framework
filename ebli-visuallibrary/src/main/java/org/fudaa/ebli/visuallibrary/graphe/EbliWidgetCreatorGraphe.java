package org.fudaa.ebli.visuallibrary.graphe;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGGraphePersist;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;
import org.fudaa.ebli.visuallibrary.persist.ManagerWidgetPersist;

/**
 * interface qui permet de creer un widget
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorGraphe extends EbliWidgetCreator {

  EGFillePanel pn_;

  public EbliWidgetCreatorGraphe() {

  }

  public EbliWidgetCreatorGraphe(EbliWidgetCreatorGraphe toCopy, final EGFillePanel panel) {
    super(toCopy);
    this.pn_ = panel;
    this.pn_.setPreferredSize(toCopy.pn_.getPreferredSize());

  }

  public EbliWidgetCreatorGraphe(final EGFillePanel _pn) {
    super();
    this.pn_ = _pn;
  }

  public EbliWidgetCreatorGraphe(final EGGraphe _g) {
    this(new EGFillePanel(_g));

  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    // creation de la widget correspondante
    // -- encapsulation dans la widget bordure
    EbliWidgetGraphe ebliWidgetGraphe = new EbliWidgetGraphe(_scene, null, pn_);
    EbliWidgetBordureSingle res = new EbliWidgetBordureSingle(ebliWidgetGraphe);
    ebliWidgetGraphe.setController(new EbliWidgetControllerGraphe(this, (EbliWidgetGraphe) res.getIntern(), res));
    // ajout des listener d ecoute du graphe
    pn_.getGraphe().getModel().addModelListener(new EbliWidgetGrapheModelListener(res));
    return res;
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorGraphe(this, pn_.duplicate());
  }

  public EGGraphe getGraphe() {
    return pn_.getGraphe();
  }

  public EGFillePanel getGraphePanel() {
    return pn_;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    EGGrapheModel modele = null;
    if (data == null) modele = new EGGrapheTreeModel();// ADRIEN: on en fait quoi de cela ??
    else {
      final String pathGraphe = (String) parameters.get("pathLayout") + File.separator + (String) data;

      try {
        pn_ = new EGGraphePersist(pathGraphe).loadFillePanel(parameters);
      } catch (final Exception e) {
        e.printStackTrace();
        FuLog.error(e);
      }
    }
    return true;
  }

  @Override
  public Object savePersistData(final Map parameters) {
    // -- generation d'un identifiant unique pour le repertoire du graphe --//
    // String
    final String pathUnique = ManagerWidgetPersist.generateGraphPath((String) parameters.get("pathGraphes"),
        getWidget().getEbliScene().findNodeById(getWidget().getId()));
    // - sauvegarde du contenu de l eggraphe dans un repertoire a part --//
    try {
      final File createRep = new File(pathUnique);
      if (createRep.mkdir() || createRep.isDirectory()) new EGGraphePersist(pathUnique).savePersitGrapheXml(this.pn_
          .getClass().getName(), getGraphe(), parameters);
    } catch (final IOException e) {
      e.printStackTrace();
    }
    String pathRelative = CtuluLibFile.getRelativeFile(new File(pathUnique), new File((String) parameters
        .get("pathLayout")), 4);
    return pathRelative;
  }

}
