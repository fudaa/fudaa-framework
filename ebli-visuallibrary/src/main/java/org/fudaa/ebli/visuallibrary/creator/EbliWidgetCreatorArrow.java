package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetArrow;
import org.fudaa.ebli.visuallibrary.EbliWidgetArrowAnchor;
import org.fudaa.ebli.visuallibrary.EbliWidgetController;
import org.fudaa.ebli.visuallibrary.EbliWidgetControllerArrow;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetTriangleAnchorShape;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.widget.ConnectionWidget;

/**
 * NE plus utiliser pour l'instant a revoir Creator pour un widget creator connection.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorArrow extends EbliWidgetCreator {

  /**
   * les dimensions des widgets extremites qui permettent le deplacement de l'objet.
   */
  private int defaultDimensions = 30;
  private TraceLigneModel initTraceLigne = new TraceLigneModel(TraceLigne.LISSE, 2f, Color.BLACK);
  AnchorShape sourceShape = EbliWidgetTriangleAnchorShape.DEFAULT_NONE;
  // -- positions par defaut --//
  int sourceX = 10;
  int sourceY = 10;

  /**
   * forme des contours des fleches.
   */
  AnchorShape targetShape = EbliWidgetTriangleAnchorShape.TRIANGLE_FILLED;
  int targetX = 100;
  int targetY = 100;

  public EbliWidgetCreatorArrow() {
    super();

  }

  /**
   * Creator qui initialise l'emplacement de la widget.
   * 
   * @param positionSource
   * @param positionTarget
   */
  public EbliWidgetCreatorArrow(Point positionSource, Point positionTarget, AnchorShape targetShape, AnchorShape sourceShape) {
    super();
    targetX = positionSource.x;
    targetY = positionSource.y;
    sourceX = positionTarget.x;
    sourceY = positionTarget.y;
    this.targetShape = targetShape;
    this.sourceShape = sourceShape;
  }

  private void configureAnchor(EbliScene _scene, EbliWidgetArrow conteneurFleche, EbliWidgetArrowAnchor source) {
    source.setPreferredSize(new Dimension(defaultDimensions, defaultDimensions));
    source.setController(createAnchorController(source));
    source.setTraceLigneModel(conteneurFleche.getTraceLigneModel());
    source.getActions().addAction(_scene.createSelectAction());
    source.getActions().addAction(_scene.createWidgetHoverAction());
  }

  protected EbliWidgetController createAnchorController(EbliWidget w) {
    EbliWidgetController control = new EbliWidgetController(w, true, false);
    control.setMenuDisplayed(false);
    return control;
  }

  @Override
  public EbliWidget createWidget(EbliScene _scene) {
    /**
     * Il s'agit de la widget principal vide qui d�place tout le contenu (les widgets connections).
     */
    EbliWidgetArrow conteneurFleche = new EbliWidgetArrow(_scene, false);
    conteneurFleche.setController(new EbliWidgetControllerArrow(conteneurFleche));

    ConnectionWidget connection = conteneurFleche.getConnectionWidget();
    EbliWidgetArrowAnchor source = new EbliWidgetArrowAnchor(conteneurFleche, false);
    EbliWidgetArrowAnchor target = new EbliWidgetArrowAnchor(conteneurFleche, false);
    conteneurFleche.addSatellite(source);
    conteneurFleche.addSatellite(target);

    // -- ajout de l'info de l'autre extremit� pour chaque extr�mit� --//

    source.setPreferredSize(new Dimension(defaultDimensions, defaultDimensions));
    target.setPreferredSize(new Dimension(defaultDimensions, defaultDimensions));

    source.setPreferredLocation(new Point(sourceX, sourceY));
    target.setPreferredLocation(new Point(targetX, targetY));

    conteneurFleche.setPreferredSize(new Dimension(defaultDimensions, defaultDimensions));

    // -- creation de la connection --//

    connection.setTargetAnchorShape(targetShape);
    connection.setSourceAnchorShape(sourceShape);
    configureAnchor(_scene, conteneurFleche, source);
    configureAnchor(_scene, conteneurFleche, target);
    //edit actions:
    final WidgetAction editorAction = ActionFactory.createEditAction(conteneurFleche);
    target.getActions().addAction(editorAction);
    source.getActions().addAction(editorAction);
    connection.getActions().addAction(editorAction);
    conteneurFleche.getActions().addAction(editorAction);
    conteneurFleche.setTraceLigneModel(initTraceLigne);
    connection.setSourceAnchor(AnchorFactory.createCenterAnchor(source));
    connection.setTargetAnchor(AnchorFactory.createCenterAnchor(target));
    return conteneurFleche;
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    EbliWidgetArrow arrow = (EbliWidgetArrow) widget;
    EbliWidgetCreatorArrow duplicated = new EbliWidgetCreatorArrow(arrow.getSourceLocation(), arrow.getTargetLocation(),
        arrow.getSourceAnchorShape(), arrow.getTargetAnchorShape());
    duplicated.initTraceLigne = new TraceLigneModel(arrow.getTraceLigneModel());
    return duplicated;
  }

  @Override
  public boolean satelliteMustBeDuplicated() {
    return false;
  }

  @Override
  public boolean loadPersistData(Object data, Map parameters) {
    if (data == null || !(data instanceof List))
      return false;
    //
    List persist = (List) data;
    //
    if (persist.size() != 7)
      return false;
    //
    //    // -- recuperation des parametres --//
    try {
      int idx = 0;
      Point source = (Point) persist.get(idx++);
      Point target = (Point) persist.get(idx++);
      String sourceTypeKey = (String) persist.get(idx++);
      Integer sourceSize = (Integer) persist.get(idx++);
      String targetTypeKey = (String) persist.get(idx++);
      Integer targetSize = (Integer) persist.get(idx++);
      this.initTraceLigne = (TraceLigneModel) persist.get(idx++);
      this.sourceX = source.x;
      this.sourceY = source.y;
      this.targetX = target.x;
      this.targetY = target.y;
      sourceShape = new EbliWidgetTriangleAnchorShape(EbliWidgetTriangleAnchorShape.Type.valueOf(sourceTypeKey), sourceSize);
      targetShape = new EbliWidgetTriangleAnchorShape(EbliWidgetTriangleAnchorShape.Type.valueOf(targetTypeKey), targetSize);
      if (getWidget() != null)
        getWidget().getEbliScene().refresh();
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  @Override
  public Object savePersistData(Map parameters) {

    List paramFleches = new ArrayList();
    EbliWidgetArrow arrow = (EbliWidgetArrow) widget;
    EbliWidgetTriangleAnchorShape sourceAnchorShape = arrow.getSourceAnchorShape();
    EbliWidgetTriangleAnchorShape targetAnchorShape = arrow.getTargetAnchorShape();
    paramFleches.add(arrow.getSourceLocation());
    paramFleches.add(arrow.getTargetLocation());
    paramFleches.add(sourceAnchorShape.getType().toString());
    paramFleches.add(sourceAnchorShape.getSize());
    paramFleches.add(targetAnchorShape.getType().toString());
    paramFleches.add(targetAnchorShape.getSize());
    paramFleches.add(arrow.getTraceLigneModel());
    return paramFleches;
  }

  @Override
  public void widgetAttached() {
    super.widgetAttached();

    EbliScene scene = getWidget().getEbliScene();
    EbliWidgetArrow conteneurFleche = (EbliWidgetArrow) getWidget();
    scene.getVisu().addChild(conteneurFleche.getSource());
    scene.getVisu().addChild(conteneurFleche.getTarget());
    //we have to add this action here because it's built after widget has been added to the scene.
    WidgetAction popupAction = conteneurFleche.getController().getPopupAction();
    conteneurFleche.installSelectMoveAction();
    conteneurFleche.getConnectionWidget().getActions().addAction(popupAction);
    conteneurFleche.getSource().getActions().addAction(popupAction);
    conteneurFleche.getTarget().getActions().addAction(popupAction);
    conteneurFleche.getConnectionWidget().getActions().addAction(scene.createWidgetHoverAction());
    scene.getConnexion().addChild(conteneurFleche.getConnectionWidget());
    scene.getConnexion().revalidate();

  }

  @Override
  public void widgetAddedInView() {
    super.widgetAddedInView();
    EbliWidgetArrow conteneurFleche = (EbliWidgetArrow) getWidget();
    conteneurFleche.updatePositionFromAnchor();
    EbliScene scene = getWidget().getEbliScene();

  }

  @Override
  public void widgetDetached() {
    super.widgetDetached();
    EbliScene scene = getWidget().getEbliScene();
    EbliWidgetArrow conteneurFleche = (EbliWidgetArrow) getWidget();
    scene.getVisu().removeChild(conteneurFleche.getSource());
    scene.getVisu().removeChild(conteneurFleche.getTarget());
    scene.getConnexion().removeChild(conteneurFleche.getConnectionWidget());
  }
}
