package org.fudaa.ebli.visuallibrary.actions;

import java.util.Iterator;
import java.util.Set;
import org.apache.commons.collections4.Predicate;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.PredicateFactory;

/**
 * classe qui permet de placer les widgets selectionnes en arriere plan.
 * 
 * @author genesis
 */
public class EbliWidgetActionMoveToBack extends EbliWidgetActionFilteredAbstract {

  public EbliWidgetActionMoveToBack(final EbliScene _scene) {
    super(_scene, EbliResource.EBLI.getString("Arri�re plan"), CtuluResource.CTULU.getIcon("crystal_disposerderriere"),
        "BACKGROUND");
  }

  public EbliWidgetActionMoveToBack(final EbliNode _node) {
    super(_node, EbliResource.EBLI.getString("Arri�re plan"), CtuluResource.CTULU.getIcon("crystal_disposerderriere"),
        "BACKGROUND");
  }

  private static final long serialVersionUID = 1L;

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {
    // -- liste des widget selectionnees --//
    // -- parcours des nodes
    CtuluCommandComposite cmp = new CtuluCommandComposite();
    for (final Iterator<EbliNode> it = filteredNode.iterator(); it.hasNext();) {

      final EbliNode currentNode = it.next();
      scene_.bringToLast(currentNode.getWidget(), cmp);

    }

    return cmp;
  }

  @Override
  protected Predicate getAcceptPredicate() {
    return PredicateFactory.geMovablePredicate();
  }

}
