/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.BCalqueLegendePanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * La classe de gestion des legendes pour le calque.
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class CalqueLegendeWidgetAdapter extends BCalqueLegende {

  private Map<BCalque, BCalqueLegendePanel> legendePanel_ = new HashMap<BCalque, BCalqueLegendePanel>();

  EbliScene scene_;

  public EbliScene getScene() {
    return scene_;
  }

  public void setScene(final EbliScene _scene) {
    scene_ = _scene;
  }

  public CalqueLegendeWidgetAdapter(final EbliScene _scene) {
    super();
    scene_ = _scene;

  }
  
  
  public BCalqueLegendePanel getLegendPanel(BCalque cq){
    BCalqueLegendePanel res=legendePanel_.get(cq);
    return res;
  }

  /**
   * override de calqueLegende qui recupere le panel legende associe
   */
  @Override
  protected void addLegendToPanel(final BCalqueLegendePanel _pnLeg) {
    boolean newOne=!legendePanel_.containsKey(_pnLeg.getCalque());
    legendePanel_.put(_pnLeg.getCalque(), _pnLeg);
    if(newOne){
      firePropertyChange("newLegende", null, _pnLeg);
    }
  }

  @Override
  public void enleve(BCalqueAffichage _cq) {
    legendePanel_.remove(_cq);
  }

  /**
   * cree la legende en widget.
   * 
   * @return EbliNode cree ajoute a la scene.
   */
  public EbliNode createLegende(final Point _location, final EbliScene _scene, final EbliWidgetCreatorVueCalque _id,
      BCalque _calqueCible) {
    setScene(_scene);
    final EbliNode def = new EbliNodeDefault();

    BCalqueLegendePanel pane = legendePanel_.get(_calqueCible);

    if (pane == null) return null;

    def.setCreator(new EbliWidgetCreatorLegende(pane, _id, _calqueCible));
    def.getCreator().setPreferredLocation(_location);
    def.setTitle(EbliLib.getS("L�gende calque") + " " + _calqueCible.getTitle());
    _scene.addNode(def);
    _scene.refresh();

    return def;
  }

}
