package org.fudaa.ebli.visuallibrary.calque;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluSelectorPopupButton;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.action.EbliCalqueActionTimeChooser;
import org.fudaa.ebli.commun.*;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurListComboBox;
import org.fudaa.ebli.palette.PaletteEditAction;
import org.fudaa.ebli.repere.ZTransformationDomaine;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.*;
import org.fudaa.ebli.visuallibrary.actions.EbliActionEditorOneClick;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.List;

/**
 * controller specifique pour les actions de la widget du calque. le calque ne cree aucune action par default. De plus surcharge de la methode de
 * masquage: la widget se masque avec sa widget legende.
 *
 * @author Adrien Hadoux.
 */
public class EbliWidgetControllerCalque extends EbliWidgetControllerMenuOnly implements CtuluExportDataInterface, PropertyChangeListener {
  JComponent labelTrace_;
  public boolean hasAlreadyFusion = false;
  BuMenuBar menuCalque_;
  JComponent panelTreeCalque_;
  BuToolBar toolbarCalque_;
  EbliWidgetVueCalque widgetCalque_;
  EbliWidgetWithBordure widgetParent_;
  EbliActionEditorOneClick<BuPanel> editorAction_;
  private boolean alreadyCreate_;

  public EbliWidgetControllerCalque(final EbliWidgetVueCalque _widget, final EbliWidgetWithBordure _parent) {
    super(_parent);
    widgetCalque_ = _widget;
    widgetParent_ = _parent;
    addActionSpecifiques();
    setProportional(true);
    registerListenerToaddLegendAutomatically();
  }

  /**
   * If the layer is made visible, its legend will be displayed automatically
   *
   * @param layer the layer to activate
   */
  public static void setLegendAddedIfVisible(BCalque layer) {
    layer.putClientProperty("legendAutomaticallyAdded", Boolean.TRUE);
  }

  public static void unsetLegendAddedIfVisible(BCalque layer) {
    layer.putClientProperty("legendAutomaticallyAdded", null);
  }

  /**
   * @param layer
   * @return true if the legend of the layer will be displayed if layer made visible
   */
  public static boolean isLegendAddedIfVisible(BCalque layer) {
    return layer != null && Boolean.TRUE.equals(layer.getClientProperty("legendAutomaticallyAdded"));
  }

  /**
   * listen to "visible" property change to display legend automically if requested.
   */
  private void registerListenerToaddLegendAutomatically() {
    getVisuPanel().getArbreCalqueModel().getObservable().addObserver(new Observer() {
      @Override
      public void update(Observable o, Object arg) {
        if (BSelecteurCheckBox.PROP_VISIBLE.equals(arg)) {
          final BCalque[] allLayers = getVisuPanel().getArbreCalqueModel().getRootCalque().getCalques();
          for (BCalque layer : allLayers) {
            if (layer.isVisible() && isLegendAddedIfVisible(layer) && !hasLegende(layer)) {
              ajoutLegende(layer);
            }
          }
        }
      }
    });
  }

  public void refreshCalqueWidget() {
    widgetCalque_.clearCacheImage();
    widgetCalque_.repaint();
  }

  /**
   *
   */
  @Override
  public boolean isEditable() {
    return true;
  }

  public void addActionSpecifiques() {

    editorAction_ = new EbliActionEditorOneClick<BuPanel>(widgetCalque_);
    // -- ajout de l action au widget correspondant --//
    widgetCalque_.getActions().addAction(editorAction_);
  }

  /**
   * ajoute des toolsbuton specifiques si pas deja ajoutes.
   */
  @Override
  public void addFonctionsSpecific(final ArrayList<EbliActionSimple> _listeActions) {

    if (!fonctionsAlreadySpecified()) {
      alreadyCreate_ = true;

      for (final Iterator<EbliActionSimple> it = _listeActions.iterator(); it.hasNext(); ) {
        final EbliActionSimple action = it.next();
        if (action == null) {
          toolbarCalque_.addSeparator();
          menuCalque_.getMenu(1).addSeparator();
        } else {
          toolbarCalque_.add(action.buildToolButton(EbliComponentFactory.INSTANCE));
          menuCalque_.getMenu(1).add(action.buildMenuItem(EbliComponentFactory.INSTANCE));
        }
      }
    }
  }

  public boolean hasLegende(BCalque cq) {
    return getNodeLegende(cq) != null;
  }

  public boolean hasLegendeForActifCalque() {
    return hasLegende(getCalqueActif());
  }

  public EbliWidgetCalqueLegende getWidgetLegendeForActifCalque() {
    return getWidgetLegende(getCalqueActif());
  }

  private BCalque getCalqueActif() {
    return widgetCalque_.getCalquePanel().getCalqueActif();
  }

  public EbliWidgetCalqueLegende getWidgetLegende(BCalque cq) {
    return widgetCalque_.getLegendFor(cq);
  }

  public EbliNode getNodeLegende(BCalque cq) {
    EbliWidgetCalqueLegende legendFor = widgetCalque_.getLegendFor(cq);
    if (legendFor == null) {
      return null;
    }
    return (EbliNode) widget_.getEbliScene().findObject(legendFor.getParentBordure());
  }

  public boolean hasTimeLegend() {
    return widgetCalque_.getTimeLegend() != null;
  }

  public EbliNode getNodeLegendeForActifCalque() {
    return getNodeLegende(getCalqueActif());
  }

  public void cacheLegende(BCalque cq) {
    EbliWidgetCalqueLegende w = getWidgetLegende(cq);
    if (w != null) {
      w.setWidgetVisible(false);
    }
  }

  public void cacheTimeLegend() {
    if (hasTimeLegend()) {
      widgetCalque_.getTimeLegend().setVisible(false);
    }
  }

  public void cacheLegendeForActifCalque() {
    cacheLegende(getCalqueActif());
  }

  /**
   * add a legend for active layer.
   */
  public void ajoutLegende() {
    ajoutLegende(getCalqueActif());
  }

  boolean registerDone;

  public void registerListenerForNewLegend() {
    if (registerDone) {
      return;
    }
    registerDone = true;
    final CalqueLegendeWidgetAdapter legendeAdapter = (CalqueLegendeWidgetAdapter) widgetCalque_.calquePanel_.getCqLegend();
    legendeAdapter.addPropertyChangeListener("newLegende", this);
  }

  public void ajoutLegende(BCalque cq) {
    if (hasLegende(cq)) {
      getWidgetLegende(cq).setWidgetVisible(true);
      return;
    }
    // -- creation de la l�gende --//
    if (widget_.getEbliScene() != null) {

      if (cq != null && cq instanceof BCalqueAffichage) {
        final CalqueLegendeWidgetAdapter legendeAdapter = (CalqueLegendeWidgetAdapter) widgetCalque_.calquePanel_.getCqLegend();
        // -- creation de la legende
        final Point positionLegende = new Point();
        if (widget_.getPreferredSize() != null && widget_.getPreferredLocationOnScreen() != null) {

          positionLegende.x = widget_.getPreferredLocationOnScreen().x + widget_.getPreferredSize().width + 20;
          positionLegende.y = widget_.getPreferredLocationOnScreen().y;
          Set<EbliWidget> satellites = widget_.getSatellites();
          updatePositionLegende(positionLegende, satellites);
          legendeAdapter.createLegende(positionLegende, widget_.getEbliScene(), (EbliWidgetCreatorVueCalque) getNode().getCreator(), cq);
        }
        widget_.getEbliScene().refresh();
      }
    }
  }

  private void updatePositionLegende(Point positionLegende, Set<EbliWidget> satellites) {
    if (satellites != null && !satellites.isEmpty()) {
      EbliWidget next = satellites.iterator().next();
      if (next.getLocation() != null && next.getBounds() != null) {
        Point toScene = next.getParentWidget().convertLocalToScene(next.getLocation());
        positionLegende.x = toScene.x + next.getBounds().width + 20;
      }
    }
  }

  @SuppressWarnings("serial")
  private void constructPopupMenuSpecifique(final JPopupMenu _popup) {

    JMenuItem menuItem = new JMenuItem(EbliResource.EBLI.getString("Editer le calque"));
    _popup.add(menuItem);
    menuItem.setIcon(CtuluResource.CTULU.getIcon("crystal_editer"));
    menuItem.addActionListener(e -> editWidget());
    _popup.addSeparator();
    final EbliActionChangeState displayTime = new EbliActionChangeState(EbliResource.EBLI.getString("Afficher le temps"),
        EbliResource.EBLI.getIcon("time"), "DISPLAY_TIME") {
      @Override
      public void changeAction() {
        if (isSelected()) {
          addTimeLegende();
        } else {
          cacheTimeLegend();
        }
      }
    };
    final EbliActionChangeState displayLegende = new EbliActionChangeState(EbliResource.EBLI.getString("Afficher la l�gende du calque actif"),
        CtuluResource.CTULU.getIcon("crystal_commentaire"), "DISPLAY_LEGEND") {
      @Override
      public void changeAction() {
        if (isSelected()) {
          ajoutLegende(getCalqueActif());
        } else {
          cacheLegendeForActifCalque();
        }
      }
    };
    displayLegende.setSelected(isLegendVisible());
    displayTime.setSelected(isTimeVisible());
    _popup.add(EbliComponentFactory.INSTANCE.buildMenuItem(displayLegende));
    _popup.add(EbliComponentFactory.INSTANCE.buildMenuItem(displayTime));
    _popup.addPopupMenuListener(new PopupMenuListener() {
      @Override
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        displayLegende.setSelected(isLegendVisible());
        displayTime.setSelected(isTimeVisible());
      }

      @Override
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
      }

      @Override
      public void popupMenuCanceled(PopupMenuEvent e) {
      }
    });
    _popup.addSeparator();
  }

  private boolean isTimeVisible() {
    return widgetCalque_.isTimeLegendVisible();
  }

  protected void addTimeLegende() {
    if (hasTimeLegend()) {
      widgetCalque_.getTimeLegend().setVisible(true);
      return;
    }

    final Point positionLegende = new Point();
    if (widget_.getPreferredSize() != null && widget_.getPreferredLocationOnScreen() != null) {

      positionLegende.x = widget_.getPreferredLocationOnScreen().x + widget_.getPreferredSize().width + 20;
      positionLegende.y = widget_.getPreferredLocationOnScreen().y;
      Set<EbliWidget> satellites = widget_.getSatellites();
      updatePositionLegende(positionLegende, satellites);
      EbliWidgetCreatorTimeLegend.create(positionLegende, widget_.getEbliScene(), (EbliWidgetCreatorVueCalque) getNode().getCreator());
    }
    widget_.getEbliScene().refresh();
  }

  /**
   * Mise au premier plan de la widget et edition. Sert beaucoup pour la fusion
   */
  public void editWidget() {
    // -- premier plan pour la visualiser dans le groupe --//
//    if (this.widgetCalque_.getParentWidget() != null && this.widgetCalque_.getParentWidget() instanceof EbliWidgetBordureSingle) {
//      this.widgetCalque_.getParentWidget().bringToFront();
//    } else {
//      this.widgetCalque_.bringToFront();
//    }
    // -- executer l action d edition --//
//    editorAction_.openeEditorFromMenu(widgetCalque_);

    editorAction_.openEditorFromMenu(widgetCalque_);
  }

  public void fermerEditeur() {
    editorAction_.closeEditor(true);
  }

  public ZEbliCalquesPanel getVisuPanel() {
    return widgetCalque_.calquePanel_;
  }

  /**
   * retourne le menuBar associee au calque
   */
  @Override
  public JMenuBar getMenubarComponent() {

    if (menuCalque_ == null) {

      menuCalque_ = new BuMenuBar();
      final List<JMenu> ms = new ArrayList<JMenu>(3);
      JMenu menuSelectionPath = getVisuPanel().getMenuSelectionPath();
      menuSelectionPath.setIcon(null);
      ms.add(menuSelectionPath);
      ms.addAll(Arrays.asList(getVisuPanel().getSpecificMenus(EbliLib.getS("Vue 2D"))));
      ms.add(createMenuExport());

      for (final JMenu menu : ms) {
        menuCalque_.add(menu);
      }
    }

    return menuCalque_;
  }

  private JMenu createMenuExport() {
    return createMenuExport(getVisuPanel(), getVisuPanel().getCtuluUI());
  }

  /**
   * Recupere les objets graphique tree et panel infos associe au calque.
   */
  @Override
  public JComponent getOverviewComponent() {

    if (panelTreeCalque_ == null) {
      final JScrollPane scrollPane = new JScrollPane(new BArbreCalque(getVisuPanel().getArbreCalqueModel()));
      final Collection<EbliActionPaletteAbstract> acts = getVisuPanel().getController().getTabPaletteAction();

      if (!CtuluLibArray.isEmpty(acts)) {
        final BuTabbedPane tb = new BuTabbedPane();
        for (final EbliActionPaletteAbstract palette : acts) {
          final JComponent component = palette.getPaletteContent();
          if (component instanceof BCalquePaletteInfo) {
            ((BCalquePaletteInfo) component).setAvailable(true);
            ((BCalquePaletteInfo) component).updateState();
          }
          palette.setEnabled(true);
          palette.updateBeforeShow();
          tb.addTab(palette.getTitle(), palette.getIcon(), component);
        }

        // -- split avec infos + arbre des calques --//
        final JSplitPane splitInfosArbre = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

        final BuPanel scrollInfos = getVisuPanel().buildInfosCreationComposant();
        scrollInfos.setMinimumSize(new Dimension((int) scrollInfos.getSize().getWidth(), 100));

        splitInfosArbre.setTopComponent(scrollInfos);
        splitInfosArbre.setBottomComponent(scrollPane);
        splitInfosArbre.setDividerLocation(0.3D);

        final JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setTopComponent(tb);
        splitPane.setBottomComponent(splitInfosArbre);
        splitPane.setDividerLocation(0.3D);

        panelTreeCalque_ = splitPane;
      } else {
        panelTreeCalque_ = scrollPane;
      }
    }

    return panelTreeCalque_;
  }

  BSelecteurListComboBox comboVar_;

  public void majComboVar() {
    // -- on retire le listener si il existe --//
    if (comboVar_ == null) {
      return;
    }
    getVisuPanel().getArbreCalqueModel().getTreeSelectionModel().removeTreeSelectionListener(comboVar_);
    getVisuPanel().getArbreCalqueModel().getTreeSelectionModel().addTreeSelectionListener(comboVar_);
    comboVar_.setPalettePanelTarget(getVisuPanel().getArbreCalqueModel().getSelectedCalque());
  }

  /**
   * Obtient la toolbar specifique au calque
   */
  @Override
  public JToolBar getToolbarComponent() {

    if (toolbarCalque_ == null) {
      toolbarCalque_ = new BuToolBar();
      // ajout des tool specifiques du calque
      final List actions = getVisuPanel().getController().getActions();
      for (final Iterator iterator = actions.iterator(); iterator.hasNext(); ) {
        final EbliActionInterface object = (EbliActionInterface) iterator.next();
        if (object == null) {
          toolbarCalque_.addSeparator();
        } else {
          toolbarCalque_.add(object.buildToolButton(EbliComponentFactory.INSTANCE));
        }
      }
      toolbarCalque_.add(new PaletteEditAction(getVisuPanel().getArbreCalqueModel().getTreeSelectionModel())
          .buildToolButton(EbliComponentFactory.INSTANCE));

      // -- ajout des comboBox des variables --//
      comboVar_ = new BSelecteurListComboBox();
      // pour mettre � jour la combo d�s que la s�lection de l'arbre change
      majComboVar();

      // ajout combobox
      toolbarCalque_.add(comboVar_);
      BSelecteurListComboBox.updateComboPreferredSize(comboVar_);
      comboVar_.revalidate();
      toolbarCalque_.revalidate();

      // -- ajout des combo des pas de temps --//
      final EbliCalqueActionTimeChooser chooserT = new EbliCalqueActionTimeChooser(getVisuPanel().getArbreCalqueModel().getTreeSelectionModel(), true);
      // pour activer l'action
      chooserT.setSelected(true);
      final BSelecteurListComboBox combo = (BSelecteurListComboBox) chooserT.getPaletteContent();
      chooserT.updateBeforeShow();

      // ajout combobox
      toolbarCalque_.add(combo);
      BSelecteurListComboBox.updateComboPreferredSize(combo);
      toolbarCalque_.revalidate();
    }
    // -- mise a jour de la combo de variables pour le cas ou l on ajoute des
    // var dans editer les var
    majComboVar();
    return toolbarCalque_;
  }

  /**
   * affiche le label des coordonnees
   */
  @Override
  public JComponent getTracableComponent() {

    if (labelTrace_ == null) {
      labelTrace_ = new JPanel(new BuBorderLayout(0, 2, true, false));
      JComponent suiviPanel = getVisuPanel().getSuiviPanel();
      suiviPanel.setBorder(null);
      labelTrace_.add(suiviPanel);
      CtuluSelectorPopupButton bt = new CtuluSelectorPopupButton() {
        @Override
        public JComponent createComponent() {
          return new ZTransformationDomaine(getVisuPanel().getVueCalque());
        }
      };
      labelTrace_.add(bt.getButton(), BuBorderLayout.WEST);
    }

    return labelTrace_;
  }

  @Override
  protected void buildPopupMenu(final JPopupMenu _menu) {
    constructPopupMenuSpecifique(_menu);
    // -- creation du menu commun a tous les widgets
    constructPopupMenuBase(_menu);
  }

  @Override
  public boolean fonctionsAlreadySpecified() {
    return alreadyCreate_;
  }

  private boolean isLegendVisible() {
    EbliWidget w = getWidgetLegendeForActifCalque();
    return w != null && w.getParentBordure().isVisible();
  }

  @Override
  public boolean isDataExportable() {
    return (getVisuPanel() instanceof CtuluExportDataInterface);
  }

  @Override
  public String getTitle() {
    return getNode().getTitle() + ": " + getNode().getDescription();
  }

  @Override
  public void startExport(CtuluUI ui) {
    if (isDataExportable()) {
      ((CtuluExportDataInterface) getVisuPanel()).startExport(ui);
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    BCalqueLegendePanel pn = (BCalqueLegendePanel) evt.getNewValue();
    BCalqueAffichage cq = pn.getCalque();
    EbliWidgetCalqueLegende w = getWidgetLegende(cq);
    if (w == null) {
      ajoutLegende(cq);
    }
  }

  public void addMenuSpecifique(JMenu createImportMenu) {
    getMenubarComponent().add(createImportMenu);
  }
}
