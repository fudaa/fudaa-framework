package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.Predicate;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.PredicateFactory;

/**
 * classe qui permet de redimensionnner les widgets selectionnes au min ou max selon choix du type dans le constructeur.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetActionRetaillageHorizontal extends EbliWidgetActionFilteredAbstract {

  public final static int RETAIILLAGE_MAX = 0;
  public final static int RETAIILLAGE_MIN = 1;

  public int typeRetaillage_;

  // CtuluCommandContainer cmd_;

  public EbliWidgetActionRetaillageHorizontal(final EbliScene _scene, final int _typeRetaillage) {
    super(_scene, EbliResource.EBLI.getString("Premier plan"), CtuluResource.CTULU.getIcon("crystal_rangericones"),
        "FORGROUND");

    // -- type retailage --//
    typeRetaillage_ = _typeRetaillage;

    // cmd_ = _scene.getCmdMng();
    if (_typeRetaillage == RETAIILLAGE_MAX) {
      putValue(NAME, "Resize Horizontal Max");
      setIcon(EbliResource.EBLI.getToolIcon("aowidest"));
    } else {
      putValue(NAME, "Resize Horizontal Min");
      setIcon(CtuluResource.CTULU.getIcon("crystal_rangericones"));

    }
  }

  @Override
  protected Predicate getAcceptPredicate() {
    return PredicateFactory.geMovablePredicate();
  }
  
  /**
   * @return le nombre d'objet minimal pour activer la selection
   */
  @Override
  public int getMinObjectSelectedToEnableAction() {
    return 2;
  }

  private static final long serialVersionUID = 1L;

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {

    // -- recherche du Min/Max selon choix constructeur --//
    final Iterator<EbliNode> it = filteredNode.iterator();
    Rectangle tailleAresize = null;
    if (it.hasNext()) {
      tailleAresize = it.next().getWidget().getPreferredBounds();
      for (; it.hasNext();) {

        final EbliNode currentNode = it.next();

        if (typeRetaillage_ == RETAIILLAGE_MAX && currentNode.hasWidget()) {
          if (tailleAresize.width < currentNode.getWidget().getPreferredBounds().width) tailleAresize = currentNode
              .getWidget().getPreferredBounds();
        } else if (currentNode.hasWidget()) {
          if (tailleAresize.width > currentNode.getWidget().getPreferredBounds().width) tailleAresize = currentNode
              .getWidget().getPreferredBounds();
        }
      }
    }

    // -- si rien n est selectionne --//
    if (tailleAresize == null) return null;

    // -- recupere les anciennes tailles --//
    final List<Rectangle> oldRectangle = new ArrayList<Rectangle>();
    final List<Rectangle> newRectangle = new ArrayList<Rectangle>();
    // -- liste des widget selectionnees --//
    final java.util.List<EbliWidget> listeWidget = new ArrayList<EbliWidget>();

    // -- parcours des nodes
    for (final Iterator<EbliNode> it2 = filteredNode.iterator(); it2.hasNext();) {

      final EbliNode currentNode = it2.next();
      if (currentNode != null && currentNode.hasWidget()) {

        listeWidget.add(currentNode.getWidget());
        oldRectangle.add(currentNode.getWidget().getPreferredBounds());

        // -- retaillage --//
        currentNode.getWidget().setPreferredBounds(
            new Rectangle(new Dimension(tailleAresize.width, currentNode.getWidget().getPreferredBounds().height)));

        newRectangle.add(currentNode.getWidget().getPreferredBounds());

      }

    }
    // -- ajout de l action undo redo --//
    return new CommandRetaillage(listeWidget, oldRectangle, newRectangle);

  }
}
