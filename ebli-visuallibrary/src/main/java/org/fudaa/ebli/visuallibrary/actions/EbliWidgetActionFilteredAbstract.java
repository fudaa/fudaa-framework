/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.swing.Icon;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.netbeans.api.visual.model.ObjectSceneEvent;
import org.netbeans.api.visual.model.ObjectSceneEventType;
import org.netbeans.api.visual.model.ObjectSceneListener;
import org.netbeans.api.visual.model.ObjectState;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public abstract class EbliWidgetActionFilteredAbstract extends EbliWidgetActionAbstract {

  Set<EbliNode> selectedNode;

  /**
   * @param selectedNode le widget selectionne
   * @param name
   * @param ic
   * @param ac
   */
  public EbliWidgetActionFilteredAbstract(EbliNode selectedNode, String name, Icon ic, String ac) {
    super(selectedNode.getWidget().getEbliScene(), name, ic, ac);
    this.selectedNode = new HashSet<EbliNode>(Arrays.asList(selectedNode));
  }

  public EbliWidgetActionFilteredAbstract(HashSet<EbliNode> selectedNode, String name, Icon ic, String ac) {
    super(selectedNode.iterator().next().getWidget().getEbliScene(), name, ic, ac);
    this.selectedNode = new HashSet<EbliNode>(selectedNode);
  }

  /**
   * Constructeur pour l'action bas�e sur les objet s�lectionn�s
   * 
   * @param scene
   * @param name
   * @param ic
   * @param ac
   */
  public EbliWidgetActionFilteredAbstract(EbliScene scene, String name, Icon ic, String ac) {
    super(scene, name, ic, ac);
    setEnabled(false);
    scene.addObjectSceneListener(new ObjectSceneListener() {

      @Override
      public void selectionChanged(ObjectSceneEvent event, Set<Object> previousSelection, Set<Object> newSelection) {
        selectionChangedInScene(previousSelection, newSelection);

      }

      @Override
      public void objectStateChanged(ObjectSceneEvent event, Object changedObject, ObjectState previousState,
          ObjectState newState) {}

      @Override
      public void objectRemoved(ObjectSceneEvent event, Object removedObject) {}

      @Override
      public void objectAdded(ObjectSceneEvent event, Object addedObject) {}

      @Override
      public void hoverChanged(ObjectSceneEvent event, Object previousHoveredObject, Object newHoveredObject) {}

      @Override
      public void highlightingChanged(ObjectSceneEvent event, Set<Object> previousHighlighting,
          Set<Object> newHighlighting) {}

      @Override
      public void focusChanged(ObjectSceneEvent event, Object previousFocusedObject, Object newFocusedObject) {}
    }, ObjectSceneEventType.OBJECT_SELECTION_CHANGED);
    // scene.addSceneListener(listener)
  }

  /**
   * Appele a chaque modif de selection dans la scene.Utilise le predicate pour savoir si au moins un widget correspond
   * a ce qui est attendu.
   * 
   * @param previousSelection
   * @param newSelection
   */
  protected void selectionChangedInScene(Set<Object> previousSelection, Set<Object> newSelection) {
    if (CollectionUtils.isEmpty(newSelection)) {
      setEnabled(false);
    }
    Predicate predicate = getAcceptPredicate();
    if (predicate != null) {
      Set<EbliNode> newSelected = getScene().getSelectedNodes();
      boolean enable = false;
      if (getMinObjectSelectedToEnableAction() == 1) enable = CollectionUtils.exists(newSelected, predicate);
      else {
        enable = CollectionUtils.countMatches(newSelected, predicate) >= getMinObjectSelectedToEnableAction();
      }
      setEnabled(enable);
    } else {
      setEnabled(newSelection.size() >= getMinObjectSelectedToEnableAction());
    }

  }

  /**
   * @return le nombre d'objet minimal pour activer la selection
   */
  public int getMinObjectSelectedToEnableAction() {
    return 1;
  }

  /**
   * @return true si l'action porte toujours sur le meme node.
   */
  protected boolean isOnCurrentSelection() {
    return selectedNode == null;
  }

  protected Set<EbliNode> getSelectedWidgets() {
    if (selectedNode != null) { return new HashSet<EbliNode>(selectedNode); }
    return getScene().getSelectedNodes();
  }

  /**
   * @param filteredNode les noeuds filtres sur lesquels portent l'action. C'est une nouvelle Set.
   * @return la commande
   */
  protected abstract CtuluCommand act(Set<EbliNode> filteredNode);

  protected boolean clearSelectionBefore() {
    return false;
  }

  /**
   * @return le predicate permet de savoir si un node est accepte par l'action.
   */
  protected Predicate getAcceptPredicate() {
    return null;
  }

  /**
   * @param init le set a filter
   */
  protected void filterNodes(Set<EbliNode> init) {
    Predicate predicate = getAcceptPredicate();
    if (predicate != null) {
      CollectionUtils.filter(init, predicate);
    }

  }

  @Override
  public final void actionPerformed(ActionEvent e) {
    CtuluCommandComposite cmp = new CommandCompositeScene(getScene());
    Set<EbliNode> selectedWidgets = getSelectedWidgets();
    if (isOnCurrentSelection() && clearSelectionBefore()) {
      scene_.clearSelection();
      cmp.addCmd(new CommandSelect(new HashSet(selectedWidgets)));
    }
    filterNodes(selectedWidgets);
    cmp.addCmd(act(selectedWidgets));
    scene_.refresh();
    super.cmd_.addCmd(cmp);
  }
}
