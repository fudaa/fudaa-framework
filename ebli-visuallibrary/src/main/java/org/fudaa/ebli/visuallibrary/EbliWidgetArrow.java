package org.fudaa.ebli.visuallibrary;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Stroke;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionConfigure;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetArrowConfigure;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetTriangleAnchorShape;
import org.fudaa.ebli.visuallibrary.behavior.EbliWidgetCursorProvider;
import org.netbeans.api.visual.action.EditProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.action.WidgetAction.Chain;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

/**
 * classe r�serv�e pour la widget principale. Gere ses sattelites en s'assurant d'etre en arrier plan par rapport �
 * elles.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetArrow extends EbliWidget implements EditProvider {

  private class ArrowConnectionWidget extends ConnectionWidget implements WidgetForwarder {

    public ArrowConnectionWidget(Scene scene) {
      super(scene);
    }

    @Override
    public Widget getWidgetToMove() {
      return EbliWidgetArrow.this;
    }

    @Override
    protected Cursor getCursorAt(final Point _localLocation) {
      return EbliWidgetCursorProvider.getCursor(EbliWidgetArrow.this, _localLocation);
    }

  }

  private final ArrowConnectionWidget connectionAnchor_;

  public EbliWidgetArrow(EbliScene _scene, boolean resize) {
    super(_scene, resize);
    connectionAnchor_ = new ArrowConnectionWidget(_scene);
  }

  public ConnectionWidget getConnectionWidget() {
    return connectionAnchor_;
  }

  public EbliWidgetTriangleAnchorShape getSourceAnchorShape() {
    return (EbliWidgetTriangleAnchorShape) connectionAnchor_.getSourceAnchorShape();
  }

  public EbliWidgetTriangleAnchorShape getTargetAnchorShape() {
    return (EbliWidgetTriangleAnchorShape) connectionAnchor_.getTargetAnchorShape();
  }

  public Widget getSource() {
    return connectionAnchor_.getSourceAnchor().getRelatedWidget();
  }

  @Override
  public void edit(Widget widget) {
    EbliWidgetActionConfigure.configure(this);

  }

  public Point getSourceLocation() {
    return getSource().getPreferredLocation();
  }

  public Dimension getSourceSize() {
    return getSource().getPreferredSize();
  }

  public Dimension getTargetSize() {
    return getTarget().getPreferredSize();
  }

  public Point getTargetLocation() {
    return getTarget().getPreferredLocation();
  }

  public void installSelectMoveAction() {
    reInstallMoveActions();
  }

  public Widget getTarget() {
    return connectionAnchor_.getTargetAnchor().getRelatedWidget();
  }

  /**
   * Surcharge de notifyNodeAdded apr�s insertion pour forcer les d�placements.
   */
  @Override
  public void notifyNodeAdded(EbliNode node) {
    bringToFirst();
    getEbliScene().addObject(node, this, getConnectionWidget(), getTarget(), getSource());
  }

  @Override
  public BConfigurableInterface[] getConfigureInterfaces(boolean lineModel_, boolean colorsContours_, boolean colorFonds_, boolean rotations_,
      boolean police_, boolean transparence) {
    BConfigurableInterface display = getSingleConfigureInterface(lineModel_, colorsContours_, colorFonds_, rotations_, police_, transparence);
    return new BConfigurableInterface[] { new BConfigurableComposite(display, new EbliWidgetArrowConfigure(this), EbliLib.getS("Affichage")) };
  }

  @Override
  public void setWidgetVisible(boolean _visible) {
    super.setWidgetVisible(_visible);
    connectionAnchor_.setVisible(_visible);
    getSource().setVisible(_visible);
    getTarget().setVisible(_visible);
    refreshMyProperties();
  }

  /**
   * ne replace rien. la widget centrale se contente de son mode sattelite qui lui permet de d�placer ses extr�mit�s
   * automatiquement dans efforts.
   */
  @Override
  public void notifyWidgetMoved(final Point _location) {
    bringToFirst();
  }

  @Override
  protected void setPropertyCmd(String _property, Object _newValue, CtuluCommandContainer _cmd) {
    super.setPropertyCmd(_property, _newValue, _cmd);
    if (_property.equals(EbliWidgetArrowConfigure.ANCHOR_SOURCE)) {
      connectionAnchor_.setVisible(false);
      connectionAnchor_.getSourceAnchorEntry().revalidateEntry();
      connectionAnchor_.setSourceAnchorShape((EbliWidgetTriangleAnchorShape) _newValue);
      connectionAnchor_.setVisible(isVisible());

    } else if (_property.equals(EbliWidgetArrowConfigure.ANCHOR_TARGET)) {
      connectionAnchor_.setVisible(false);
      connectionAnchor_.setTargetAnchorShape((EbliWidgetTriangleAnchorShape) _newValue);
      connectionAnchor_.getTargetAnchorEntry().revalidateEntry();
      connectionAnchor_.setVisible(isVisible());

    } else if (_property.equals(LINEMODEL)) {
      setTraceLigneModel(getTraceLigneModel());
    } else if (_property.equals(COLORCONTOUR)) {
      getTraceLigneModel().setColor((Color) _newValue);
      setTraceLigneModel(getTraceLigneModel());
    }
    refreshMyProperties();
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canTranparency() {
    return false;
  }

  @Override
  public boolean canColorBackground() {
    return false;
  }

  @Override
  public boolean canFont() {
    return false;
  }

  @Override
  public void setTraceLigneModel(TraceLigneModel model) {
    super.setTraceLigneModel(model);
    // -- on modifie le contenu du connecteur --//
    TraceLigne traceligne = new TraceLigne(model);
    Stroke stroke = traceligne.getStroke();
    connectionAnchor_.setForeground(getTraceLigneModel().getCouleur());
    connectionAnchor_.setLineColor(getTraceLigneModel().getCouleur());
    if (stroke != null)
      connectionAnchor_.setStroke(stroke);
  }

  /**
   * repositionne les widget extremites sattelites en haut de la scene (on ne voit rien dans le tree car il n'y a pas de
   * node enegistr�e pouer cette widget).
   */
  private void bringToFirst() {
    Set<EbliWidget> set = getSatellites();
    if (set == null)
      return;
    for (EbliWidget satt : set) {
      // -- amene en debut lse frames--//
      getEbliScene().bringToFirst(satt);
    }
  }

  public void updatePositionFromAnchor() {
    Dimension sourceDimension = getSourceSize();
    Point sourceLocation = getSourceLocation();
    sourceLocation.x += sourceDimension.width / 2;
    sourceLocation.y += sourceDimension.height / 2;
    Point targetLocation = getTargetLocation();
    Dimension targetDimension = getTargetSize();
    targetLocation.x += targetDimension.width / 2;
    targetLocation.y += targetDimension.height / 2;

    // -- position du nouveau point --//
    int x = (sourceLocation.x + targetLocation.x) / 2;
    int y = (sourceLocation.y + targetLocation.y) / 2;
    Dimension dimension = getPreferredSize();
    Point newPosition = new Point(x - dimension.width / 2, y - dimension.height / 2);

    setPreferredLocation(newPosition);
  }

  @Override
  public Object getProperty(String _key) {
    if (_key.equals(EbliWidgetArrowConfigure.ANCHOR_SOURCE)) {
      return getSourceAnchorShape();
    }
    if (_key.equals(EbliWidgetArrowConfigure.ANCHOR_TARGET)) {
      return getTargetAnchorShape();
    }
    return super.getProperty(_key);
  }

  public void reInstallMoveActions() {
    WidgetAction moveAction = getEbliScene().getController().getMoveAction();
    WidgetAction selectAction = getEbliScene().createSelectAction();
    reinstallMoveActions(moveAction, selectAction, connectionAnchor_.getActions());
    reinstallMoveActions(moveAction, selectAction, getSource().getActions());
    reinstallMoveActions(moveAction, selectAction, getTarget().getActions());
  }

  private void reinstallMoveActions(WidgetAction moveAction, WidgetAction selectAction, Chain actions) {
    actions.removeAction(moveAction);
    actions.removeAction(selectAction);
    if (!actions.getActions().contains(selectAction)) {
      actions.addAction(selectAction);
    }
    if (getController().canMove_ && !actions.getActions().contains(moveAction)) {
      actions.addAction(moveAction);
    }
  }

  public void removeActionResizeAndMove() {
    WidgetAction moveAction = getEbliScene().getController().getMoveAction();
    connectionAnchor_.getActions().removeAction(moveAction);
    getSource().getActions().removeAction(moveAction);
    getTarget().getActions().removeAction(moveAction);

  }
}