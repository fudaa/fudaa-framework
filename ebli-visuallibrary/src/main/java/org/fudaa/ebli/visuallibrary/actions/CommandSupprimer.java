package org.fudaa.ebli.visuallibrary.actions;

import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * commande qui effectue un undo/redo sur la suppression.
 * 
 * @author Adrien Hadoux
 */
public class CommandSupprimer implements CtuluCommand {

  List<EbliNode> nodeSupprime_;

  List<EbliScene> scene_;

  public CommandSupprimer(final EbliNode nodeSupprime, final EbliScene scene) {
    super();
    nodeSupprime_ = Arrays.asList(nodeSupprime);
    this.scene_ = Arrays.asList(scene);
  }

  public CommandSupprimer(final List<EbliNode> listeNode, final List<EbliScene> scene) {
    super();
    nodeSupprime_ = listeNode;
    this.scene_ = scene;
  }

  @Override
  public void redo() {
    for (int i = 0; i < nodeSupprime_.size(); i++) {
      EbliNode node = nodeSupprime_.get(i);
      node.getWidget().getEbliScene().removeNode(node);
    }
  }

  @Override
  public void undo() {
    int size = nodeSupprime_.size();
    for (int i = 0; i < size; i++) {
      EbliNode node = nodeSupprime_.get(i);
      // -- de nouveau suppression du node dans la scene --//
      scene_.get(i).addNode(node);
    }
  }

}
