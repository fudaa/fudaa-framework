package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.netbeans.api.visual.widget.Widget;

/**
 * classe qui permet de faire du undo redo de positions de une ou plusieurs widgets
 * 
 * @author genesis
 */
public class CommandMove implements CtuluCommand {

  List<Widget> widgets_;
  List<Point> oldPos_;
  List<Point> newPos_;

  public CommandMove(final List<Widget> _widgets, final List<Point> _oldPos, final List<Point> _newPos) {
    super();
    this.newPos_ = _newPos;
    this.oldPos_ = _oldPos;
    this.widgets_ = _widgets;
    if (_newPos.size() != _oldPos.size()) throw new IllegalArgumentException("list must ahava the same size");
    if (_widgets.size() != _oldPos.size()) throw new IllegalArgumentException("list must ahava the same size");
  }

  public CommandMove(final Widget _widget, final Point _oldPos, final Point _newPos) {
    super();
    this.newPos_ = Arrays.asList(_newPos);
    this.oldPos_ = Arrays.asList(_oldPos);
    this.widgets_ = Arrays.asList(_widget);
  }

  @Override
  public void redo() {
    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).setPreferredLocation(newPos_.get(i));

    }
    refreshScene();
  }

  private void refreshScene() {
    EbliScene.refreshScene(widgets_.get(0).getScene());
  }

  @Override
  public void undo() {
    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).setPreferredLocation(oldPos_.get(i));

    }
    refreshScene();
  }

}
