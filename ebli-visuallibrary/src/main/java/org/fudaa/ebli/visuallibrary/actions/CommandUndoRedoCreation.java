package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;

public class CommandUndoRedoCreation implements CtuluCommand {

  EbliNode nodeCree_;

  EbliScene scene_;

  public CommandUndoRedoCreation(final EbliNode _nodeCree, final EbliScene scene_) {
    super();
    this.nodeCree_ = _nodeCree;
    this.scene_ = scene_;
  }

  @Override
  public void redo() {
    // --rattachement du node dans la scene --//
    scene_.addNode(nodeCree_);
    // -- raffraichissement de la scene --//
    scene_.refresh();

  }

  @Override
  public void undo() {

    // -- de nouveau suppression du node dans la scene --//
    scene_.removeNode(nodeCree_);
    // -- raffraichissement de la scene --//
    scene_.refresh();
  }

}