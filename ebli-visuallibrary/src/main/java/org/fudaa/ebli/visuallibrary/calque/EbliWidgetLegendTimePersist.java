package org.fudaa.ebli.visuallibrary.calque;

public class EbliWidgetLegendTimePersist {
  
  boolean titleVisible=true;
  String idOwnerWidget;

  public String getIdOwnerWidget() {
    return idOwnerWidget;
  }

  public void setIdOwnerWidget(String idOwnerWidget) {
    this.idOwnerWidget = idOwnerWidget;
  }

  public boolean isTitleVisible() {
    return titleVisible;
  }

  public void setTitleVisible(boolean titleVisible) {
    this.titleVisible = titleVisible;
  }

}
