package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.modules.visual.layout.FlowLayout;

/**
 * Legende Widget qui contient le titre de la plage du calque
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetPlage extends EbliWidget {

  public class InternWidget extends LabelWidgetFixedWidth {

    public InternWidget(final Scene scene) {
      super(scene);
    }

    /**
     * Methode utilisee pour mettre a jour la plage.
     */
    public void majText(String txt) {
      this.setLabel(txt);
      setToolTipText(plage_.getLegende());
    }

  }

  // LabelWidget intern_;

  InternWidget single_;
  InternWidget left_;
  InternWidget middle_;
  InternWidget right_;

  EbliWidget logo_;

  BPlageInterface plage_;

  /**
   * @param _scene
   */
  public EbliWidgetPlage(final EbliScene _scene, final BPlageInterface _cb) {
    super(_scene);
    plage_ = _cb;

    setLayout(new FlowLayout(false, SerialAlignment.RIGHT_BOTTOM, 2));
    setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 3));
    // setEnabled(false);
    setEnabled(true);
    logo_ = new EbliWidget(_scene) {
      @Override
      protected void paintWidget() {
        final Graphics2D g = getGraphics();
        g.setColor(plage_.getCouleur());
        final Rectangle newBounds = getBounds();
        g.fillRect(0, 0, newBounds.width, newBounds.height);
      }

      @Override
      protected Rectangle calculateClientArea() {
        final Rectangle rec = new Rectangle();
        InternWidget widget = single_ == null ? left_ : single_;
        // rec.width = (int) intern_.getPreferredBounds().getHeight();
        final Rectangle bounds = widget.getBounds();
        if (bounds != null) {
          rec.width = (int) bounds.getHeight();
        } else {
          rec.width = (int) widget.getPreferredBounds().getHeight();
        }
        rec.width++;
        rec.height = rec.width;
        return rec;
      }

    };

    logo_.setEnabled(true);
    addChild(logo_);
    if (plage_.isLegendCustomized()) {
      // -- creation de la legende --//
      single_ = new InternWidget(_scene);
      single_.setAlignment(Alignment.RIGHT);
      single_.majText(plage_.getLegende());
      addChild(single_);
    } else {
      String min = plage_.getMinString();
      String middle = plage_.getSep();
      String max = plage_.getMaxString();
      if (min == null) {
        String[] parseString = CtuluLibString.parseString(plage_.getLegende(), " ");
        if (parseString != null) {
          if (parseString.length > 0) {
            min = parseString[0];
            if (parseString.length > 1) {
              middle = parseString[1];
            }
            if (parseString.length > 2) {
              max = CtuluLibString.arrayToString(parseString, 2, " ");
            }
          }
        }
      }
      left_ = create(min, _scene);
      middle_ = create(middle, _scene);
      middle_.setAlignment(Alignment.CENTER);
      right_ = create(max, _scene);
      addChild(left_);
      addChild(middle_);
      addChild(right_);

    }
    // intern_.setForeground(_cb.getCouleur());
    setToolTipText(_cb.getLegende());

  }

  protected InternWidget create(String txt, EbliScene _scene) {
    InternWidget res = new InternWidget(_scene);
    res.majText(txt);
    return res;
  }

  public boolean isThreePart() {
    return left_ != null;
  }

  public int calculateNormalWidth(int pos) {
    return LabelWidgetFixedWidth.calculateNormalWidth(getWidget(pos));
  }

  public int getSingleWidth() {
    return LabelWidgetFixedWidth.calculateNormalWidth(single_);
  }

  public void setWidth(int pos, int newWidth) {
    InternWidget widget = getWidget(pos);
    widget.setWidth(newWidth);
    widget.revalidate();
  }

  public void setSingleLabelWidth(int newWidth) {
    if (single_ != null) {
      single_.setWidth(newWidth);
      single_.revalidate();
    }

  }

  public static int getGapBetweenParts() {
    return 4;
  }

  public InternWidget getWidget(int position) {
    if (position == SwingConstants.LEFT)
      return left_;
    if (position == SwingConstants.RIGHT)
      return right_;
    if (position == SwingConstants.CENTER)
      return middle_;
    return null;

  }

  public void updateFont(LabelWidget w, Font ft) {
    if (w != null) {
      w.setFont(ft);
      w.revalidate();
    }

  }

  public void updateFont(Font ft) {
    updateFont(ft, false);
  }

  public void updateFont(Font ft, boolean refreshScene) {
    super.setFont(ft);
    updateFont(single_, ft);
    updateFont(left_, ft);
    updateFont(right_, ft);
    updateFont(middle_, ft);
    logo_.revalidate();
    if (refreshScene) {
      logo_.getEbliScene().refresh();
      logo_.revalidate();
      logo_.getEbliScene().refresh();
    }

  }

}
