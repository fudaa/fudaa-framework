package org.fudaa.ebli.visuallibrary.calque;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliWidgetControllerMenuOnly;
import org.fudaa.ebli.visuallibrary.EbliWidgetWithBordure;

/**
 * controller specifique aux legendes. gere
 * 
 * @author genesis
 */
public class EbliWidgetControllerLegendeCalque extends EbliWidgetControllerMenuOnly {

  public EbliWidgetControllerLegendeCalque(final EbliWidgetWithBordure widget) {
    super(widget);
  }

  @Override
  protected void buildPopupMenu(final JPopupMenu _menu) {
    constructPopupMenuSpecifique(_menu);
    // -- creation du menu commun a tous les widgets
    constructPopupMenuBase(_menu);
  }

  private void constructPopupMenuSpecifique(final JPopupMenu _popup) {

    final JMenuItem menuItem = new JMenuItem(EbliResource.EBLI.getString("Editer la l�gende"));
    _popup.add(menuItem, 0);
    menuItem.setIcon(CtuluResource.CTULU.getIcon("crystal_editer"));
    menuItem.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {

        MenuEditer();

      }
    });
    final EbliWidgetCalqueLegende widgetCalqueLegende = (EbliWidgetCalqueLegende) getWidget().getIntern();
    String label = widgetCalqueLegende.getSubTitleLabel();
    if (label != null) {
      final JCheckBox subTitleCheckBox = new JCheckBox(EbliLib.getS("Afficher") + ": " + label);
      subTitleCheckBox.setSelected(widgetCalqueLegende.isSubTitleVisible());
      _popup.add(subTitleCheckBox);
      subTitleCheckBox.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          widgetCalqueLegende.setSubTitleVisible(subTitleCheckBox.isSelected());

        }
      });
      _popup.addPopupMenuListener(new PopupMenuListener() {

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          subTitleCheckBox.setSelected(widgetCalqueLegende.isSubTitleVisible());

        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {

        }
      });
    }

  }

  @Override
  public boolean isEditable() {
    return true;
  }

  /**
   * Mise au premier plan de la widget et edition. Sert beaucoup pour la fusion
   */
  public void MenuEditer() {
    // -- executer l action d edition --//
    EbliWidgetCalqueLegende widgetCalqueLegende = (EbliWidgetCalqueLegende) getWidget().getIntern();
    widgetCalqueLegende.edit(getWidget());

  }

}
