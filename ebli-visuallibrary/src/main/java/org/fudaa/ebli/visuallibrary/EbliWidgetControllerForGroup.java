/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionRestoreVueCalqueForFusion;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionUngroup;

import javax.swing.*;
import java.util.ArrayList;

/**
 * @author deniger
 */
public class EbliWidgetControllerForGroup extends EbliWidgetController {
  public boolean hasAlreadyFusion = false;
  public boolean isFusionCalque = false;

  public EbliWidgetControllerForGroup(final EbliWidget _widget, final boolean isFusionClaques) {
    super(_widget, true, true, false);
    isFusionCalque = isFusionClaques;
  }

  public EbliWidgetControllerForGroup(final EbliWidget _widget) {
    this(_widget, false);
  }

  public void addFonctionsSpecific(final ArrayList<EbliActionSimple> _listeActions) {
    if (isFusionCalque) {
      _listeActions.add(new EbliWidgetActionRestoreVueCalqueForFusion(getWidget().getEbliScene(), node_));
    }
  }

  @Override
  protected void buildPopupMenu(final JPopupMenu _menu) {
    super.buildPopupMenu(_menu);

    _menu.add(new EbliWidgetActionUngroup(getWidget().getEbliScene(), node_));
    if (isFusionCalque) {
      _menu.addSeparator();
      _menu.add(new EbliWidgetActionRestoreVueCalqueForFusion(getWidget().getEbliScene(), node_));
    }
  }
}
