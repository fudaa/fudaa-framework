/*
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import java.util.Set;
import org.netbeans.api.visual.model.ObjectSceneEvent;
import org.netbeans.api.visual.model.ObjectSceneListener;
import org.netbeans.api.visual.model.ObjectState;

/**
 * Classe implementant � vide les m�thodes de ObjectSceneListener.
 * 
 * @author deniger
 */
public class DefaultObjectSceneListener implements ObjectSceneListener {

  @Override
  public void focusChanged(final ObjectSceneEvent _event, final Object _previousFocusedObject,
      final Object _newFocusedObject) {}

  @Override
  public void highlightingChanged(final ObjectSceneEvent _event, final Set<Object> _previousHighlighting,
      final Set<Object> _newHighlighting) {}

  @Override
  public void hoverChanged(final ObjectSceneEvent _event, final Object _previousHoveredObject,
      final Object _newHoveredObject) {}

  @Override
  public void objectAdded(final ObjectSceneEvent _event, final Object _addedObject) {}

  @Override
  public void objectRemoved(final ObjectSceneEvent _event, final Object _removedObject) {}

  @Override
  public void objectStateChanged(final ObjectSceneEvent _event, final Object _changedObject,
      final ObjectState _previousState, final ObjectState _newState) {}

  @Override
  public void selectionChanged(final ObjectSceneEvent _event, final Set<Object> _previousSelection,
      final Set<Object> _newSelection) {}

}
