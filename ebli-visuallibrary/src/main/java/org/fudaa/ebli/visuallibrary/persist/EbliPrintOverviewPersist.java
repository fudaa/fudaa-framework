package org.fudaa.ebli.visuallibrary.persist;

import javax.print.attribute.EnumSyntax;
import javax.print.attribute.standard.MediaSizeName;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.EbliPrintOverviewLayer;

public class EbliPrintOverviewPersist {

  private static class FakeMediaSizeName extends MediaSizeName {

    public FakeMediaSizeName() {
      super(0);
    }

    @Override
    public EnumSyntax[] getEnumValueTable() {
      return super.getEnumValueTable();
    }

    @Override
    public String[] getStringTable() {
      return super.getStringTable();
    }
  }
  
  
  public static String[] getNames(){
    return new FakeMediaSizeName().getStringTable();
  }

  String mediaSizeName;
  int orientation;
  boolean visible;
  TraceLigneModel ligneModel;

  public void initFrom(EbliPrintOverviewLayer layer) {
    mediaSizeName = layer.getCurrentMediaSizeName().toString();
    orientation = layer.getOrientation();
    visible = layer.isVisible();
    ligneModel = layer.getTraceLigne().getModel();
  }

  public static MediaSizeName findMediaSizeName(String mediaSizeName) {
    FakeMediaSizeName fakeMediaSizeName = new FakeMediaSizeName();
    EnumSyntax[] fake = fakeMediaSizeName.getEnumValueTable();
    String[] names = fakeMediaSizeName.getStringTable();
    for (int i = 0; i < names.length; i++) {
      String string = names[i];
      if (string.equals(mediaSizeName)) {
        return (MediaSizeName) fake[i];
      }
    }
    return null;
  }

  public void applyTo(EbliPrintOverviewLayer layer) {

    MediaSizeName media = findMediaSizeName(mediaSizeName);
    if (media != null) {
      layer.setCurrentMediaSizeName(media);
    }
    layer.setVisible(visible);
    layer.setOrientation(orientation);
    layer.getTraceLigne().getModel().updateData(ligneModel);
    layer.refresh();

  }
}
