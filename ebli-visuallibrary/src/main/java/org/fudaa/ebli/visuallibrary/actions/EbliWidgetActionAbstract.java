package org.fudaa.ebli.visuallibrary.actions;

import javax.swing.Icon;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * Action reservee aux widgets. Uniformise la gestion de la scene dans l action. permet de changer de scene pour la meme
 * action.
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public abstract class EbliWidgetActionAbstract extends EbliActionSimple {

  protected final EbliScene scene_;
  protected CtuluCommandContainer cmd_;

  public EbliWidgetActionAbstract(final EbliScene _scene, final String _name, final Icon _ic, final String _ac) {
    super(_name, _ic, _ac);
    scene_ = _scene;
    cmd_ = _scene.getCmdMng();
  }

  public EbliScene getScene() {
    return scene_;
  }

}
