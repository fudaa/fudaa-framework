package org.fudaa.ebli.visuallibrary.behavior;

import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.action.AlignWithMoveDecorator;
import org.netbeans.api.visual.action.AlignWithWidgetCollector;
import org.netbeans.api.visual.action.ResizeProvider;
import org.netbeans.api.visual.action.ResizeStrategy;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.action.AlignWithSupport;

/**
 * Stratégie poru le resize proportionnel
 * 
 * @author Adrien Hadoux
 */

public class AlignWithResizeProportionalStrategy extends AlignWithSupport implements ResizeStrategy, ResizeProvider {

  private boolean outerBounds;

  public AlignWithResizeProportionalStrategy(AlignWithWidgetCollector collector, LayerWidget interractionLayer,
      AlignWithMoveDecorator decorator, boolean outerBounds) {
    super(collector, interractionLayer, decorator);
    this.outerBounds = outerBounds;
  }

  @Override
  public Rectangle boundsSuggested(Widget widget, Rectangle originalBounds, Rectangle suggestedBounds,
      ControlPoint controlPoint) {
    final double wDivParH = CtuluLibImage.getRatio(originalBounds.width, originalBounds.height);
    final double hDivParW = CtuluLibImage.getRatio(originalBounds.height, originalBounds.width);
    Insets insets = widget.getBorder().getInsets();
    int minx = insets.left + insets.right;
    int miny = insets.top + insets.bottom;

    suggestedBounds = widget.convertLocalToScene(suggestedBounds);

    Point suggestedLocation, point;
    int tempx, tempy;

    switch (controlPoint) {
    case BOTTOM_CENTER:
      suggestedLocation = new Point(suggestedBounds.x + suggestedBounds.width / 2, suggestedBounds.y
          + suggestedBounds.height);
      if (!outerBounds) suggestedLocation.y -= insets.bottom;

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, false, true, false,
          false);

      if (!outerBounds) point.y += insets.bottom;

      suggestedBounds.height = Math.max(miny, point.y - suggestedBounds.y);

      break;
    case BOTTOM_LEFT:
      suggestedLocation = new Point(suggestedBounds.x, suggestedBounds.y + suggestedBounds.height);
      if (!outerBounds) {
        suggestedLocation.y -= insets.bottom;
        suggestedLocation.x += insets.left;
      }

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, true, true, false,
          false);

      if (!outerBounds) {
        point.y += insets.bottom;
        point.x -= insets.left;
      }

      suggestedBounds.height = Math.max(miny, point.y - suggestedBounds.y);

      tempx = Math.min(point.x, suggestedBounds.x + suggestedBounds.width - minx);
      suggestedBounds.width = suggestedBounds.x + suggestedBounds.width - tempx;
      suggestedBounds.x = tempx;
      break;
    case BOTTOM_RIGHT:
      suggestedLocation = new Point(suggestedBounds.x + suggestedBounds.width, suggestedBounds.y
          + suggestedBounds.height);
      if (!outerBounds) {
        suggestedLocation.y -= insets.bottom;
        suggestedLocation.x -= insets.right;
      }

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, true, true, false,
          false);

      if (!outerBounds) {
        point.y += insets.bottom;
        point.x += insets.right;
      }

      suggestedBounds.height = Math.max(miny, point.y - suggestedBounds.y);

      suggestedBounds.width = Math.max(minx, point.x - suggestedBounds.x);
      break;
    case CENTER_LEFT:
      suggestedLocation = new Point(suggestedBounds.x, suggestedBounds.y + suggestedBounds.height / 2);
      if (!outerBounds) suggestedLocation.x += insets.left;

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, true, false, false,
          false);

      if (!outerBounds) point.x -= insets.left;

      tempx = Math.min(point.x, suggestedBounds.x + suggestedBounds.width - minx);
      suggestedBounds.width = suggestedBounds.x + suggestedBounds.width - tempx;
      suggestedBounds.x = tempx;
      break;
    case CENTER_RIGHT:
      suggestedLocation = new Point(suggestedBounds.x + suggestedBounds.width, suggestedBounds.y
          + suggestedBounds.height / 2);
      if (!outerBounds) suggestedLocation.x -= insets.right;

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, true, false, false,
          false);

      if (!outerBounds) point.x += insets.right;

      suggestedBounds.width = Math.max(minx, point.x - suggestedBounds.x);
      break;
    case TOP_CENTER:
      suggestedLocation = new Point(suggestedBounds.x + suggestedBounds.width / 2, suggestedBounds.y);
      if (!outerBounds) suggestedLocation.y += insets.top;

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, false, true, false,
          false);

      if (!outerBounds) point.y -= insets.top;

      tempy = Math.min(point.y, suggestedBounds.y + suggestedBounds.height - miny);
      suggestedBounds.height = suggestedBounds.y + suggestedBounds.height - tempy;
      suggestedBounds.y = tempy;
      break;
    case TOP_LEFT:
      suggestedLocation = new Point(suggestedBounds.x, suggestedBounds.y);
      if (!outerBounds) {
        suggestedLocation.y += insets.top;
        suggestedLocation.x += insets.left;
      }

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, true, true, false,
          false);

      if (!outerBounds) {
        point.y -= insets.top;
        point.x -= insets.left;
      }

      tempy = Math.min(point.y, suggestedBounds.y + suggestedBounds.height - miny);
      suggestedBounds.height = suggestedBounds.y + suggestedBounds.height - tempy;
      suggestedBounds.y = tempy;

      tempx = Math.min(point.x, suggestedBounds.x + suggestedBounds.width - minx);
      suggestedBounds.width = suggestedBounds.x + suggestedBounds.width - tempx;
      suggestedBounds.x = tempx;
      break;
    case TOP_RIGHT:
      suggestedLocation = new Point(suggestedBounds.x + suggestedBounds.width, suggestedBounds.y);
      if (!outerBounds) {
        suggestedLocation.y += insets.top;
        suggestedLocation.x -= insets.right;
      }

      point = super.locationSuggested(widget, new Rectangle(suggestedLocation), suggestedLocation, true, true, false,
          false);

      if (!outerBounds) {
        point.y -= insets.top;
        point.x += insets.right;
      }

      tempy = Math.min(point.y, suggestedBounds.y + suggestedBounds.height - miny);
      suggestedBounds.height = suggestedBounds.y + suggestedBounds.height - tempy;
      suggestedBounds.y = tempy;

      suggestedBounds.width = Math.max(minx, point.x - suggestedBounds.x);
      break;
    }

    if (widget instanceof EbliWidget) {
      EbliWidget widgetToResize = (EbliWidget) widget;
      EbliWidget group = widgetToResize.getGroup();
      if (group != null) {
        widgetToResize = group;
      }
      if (widgetToResize.getController().isProportional()) {
        // -- proportions respectees --//
        // double coeff = ((double) suggestedBounds.height) / ((double) oldHeight);
        if (suggestedBounds.height != originalBounds.height) {
          suggestedBounds.width = (int) (wDivParH * suggestedBounds.height);
        } else if (suggestedBounds.width != originalBounds.width) {
          suggestedBounds.height = (int) (hDivParW * suggestedBounds.width);
        }
      }

      return widgetToResize.convertSceneToLocal(suggestedBounds);

    } else {
      return widget.convertSceneToLocal(suggestedBounds);
    }
  }

  @Override
  public void resizingStarted(Widget widget) {
    show();
  }

  @Override
  public void resizingFinished(Widget widget) {
    hide();
  }

}
