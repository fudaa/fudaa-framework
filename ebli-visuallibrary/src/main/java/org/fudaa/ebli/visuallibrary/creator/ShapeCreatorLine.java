package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D.Float;
import java.util.Map;

/**
 * ShapeCreator pour les lignes classiques.
 * 
 * @author Adrien Hadoux
 * 
 */
public class ShapeCreatorLine implements ShapeCreator {

  @Override
  public Shape createShapeFor(Float _rec, Map _options, float _largeurTrait) {
    // final GeneralPath path = new GeneralPath();
    // path.moveTo(_rec.x, _rec.y + _rec.height / 2f);
    // path.lineTo(_rec.x + _rec.width, _rec.y + _rec.height / 2f);
    //    
    // path.closePath();
    // return path;
    return new Line2D.Float(_rec.x, _rec.y + _rec.height / 2f, _rec.x + _rec.width, _rec.y + _rec.height / 2f);
  }

}
