package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.modules.visual.layout.FlowLayout;

/**
 * Legende Widget qui contient le titre de la plage du calque
 */
public class EbliWidgetTwoPart extends EbliWidget {

  // LabelWidget intern_;

  LabelWidgetFixedWidth left_;
  LabelWidgetFixedWidth middle_;

  EbliWidget logo_;

  /**
   * @param _scene
   */
  public EbliWidgetTwoPart(final EbliScene _scene) {
    super(_scene);

    setLayout(new FlowLayout(false, SerialAlignment.RIGHT_BOTTOM, 2));
    setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    setEnabled(true);
    left_ = new LabelWidgetFixedWidth(getScene());
    middle_ = new LabelWidgetFixedWidth(getScene());
    addChild(left_);
    addChild(middle_);
  }

  @Override
  public void setFormeFont(Font newFont) {
    super.setFormeFont(newFont);
    left_.setFont(newFont);
    middle_.setFont(newFont);
  }

  public void setText(int pos, String txt) {
    LabelWidgetFixedWidth widget = getWidget(pos);
    widget.setLabel(txt);
  }

  protected LabelWidgetFixedWidth create(String txt, EbliScene _scene) {
    LabelWidgetFixedWidth res = new LabelWidgetFixedWidth(_scene);
    res.setLabel(txt);
    return res;
  }

  public static int getWidth(LabelWidgetFixedWidth w) {
    if (w == null)
      return 0;
    int save = w.getWidth();
    w.setWidth(0);
    int res = w.calculateClientArea().width;
    w.setWidth(save);
    return res + 3;
  }

  public int calculateNormalWidth(int pos) {
    return LabelWidgetFixedWidth.calculateNormalWidth(getWidget(pos));
  }

  public void setWidth(int pos, int newWidth) {
    LabelWidgetFixedWidth widget = getWidget(pos);
    widget.setWidth(newWidth);
    widget.revalidate();
  }

  public void resetFixedWidth() {
    left_.setWidth(0);
    middle_.setWidth(0);
  }

  public static int getGapBetweenParts() {
    return 4;
  }

  public LabelWidgetFixedWidth getWidget(int position) {
    if (position == SwingConstants.LEFT)
      return left_;
    if (position == SwingConstants.CENTER)
      return middle_;
    return null;

  }

  public void updateFont(LabelWidget w, Font ft) {
    if (w != null) {
      w.setFont(ft);
      w.revalidate();
    }

  }

  public void updateFont(Font ft) {
    updateFont(ft, false);
  }

  public void updateFont(Font ft, boolean refreshScene) {
    super.setFont(ft);
    updateFont(left_, ft);
    updateFont(middle_, ft);
    logo_.revalidate();
    if (refreshScene) {
      logo_.getEbliScene().refresh();
      logo_.revalidate();
      logo_.getEbliScene().refresh();
    }

  }

}
