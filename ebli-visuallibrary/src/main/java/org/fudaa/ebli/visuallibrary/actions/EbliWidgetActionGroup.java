/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetGroupCreator;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public class EbliWidgetActionGroup extends EbliWidgetActionFilteredAbstract {

  public EbliWidgetActionGroup(final EbliScene _scene) {
    super(_scene, EbliLib.getS("Grouper"), EbliResource.EBLI.getToolIcon("formatgroup_16.png"), "GROUP_WIDGETS");
  }

  /**
   * @return le nombre d'objet minimal pour activer la selection
   */
  @Override
  public int getMinObjectSelectedToEnableAction() {
    return 2;
  }

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {
    return performGroup(filteredNode);

  }

  /**
   * Methode qui realise le groupage des ebliNodes.
   * 
   * @param _selectedObjects
   * @return
   */
  public CtuluCommand performGroup(final Set _selectedObjects) {
    final EbliNode n = groupWidgets(_selectedObjects);
    return new CtuluCommand() {

      @Override
      public void undo() {
        EbliWidgetActionUngroup.degroupObjects(getScene(), n);

      }

      @Override
      public void redo() {
        groupWidgets(_selectedObjects);
      }
    };
  }

  public EbliNode groupWidgets(final Collection _selectedObjects) {
    List<EbliNode> nodes = new ArrayList<EbliNode>(_selectedObjects.size());
    for (Object object : _selectedObjects) {
      nodes.add((EbliNode) object);
    }
    EbliWidgetGroupCreator creator = new EbliWidgetGroupCreator(_selectedObjects);
    final EbliNodeDefault node = new EbliNodeDefault();
    node.setTitle(EbliLib.getS("Groupe") + " " + nodes.iterator().next().getTitle());
    node.setCreator(creator);
    scene_.addNode(node);
    scene_.setSelectedObjects(new HashSet(Arrays.asList(node)));
    return node;
  }
}