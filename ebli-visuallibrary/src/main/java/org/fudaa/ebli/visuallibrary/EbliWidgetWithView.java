/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.text.View;
import org.fudaa.ctulu.CtuluLib;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

/**
 * @author deniger
 */
abstract class EbliWidgetWithView extends Widget {

  EbliWidgetWithView(Scene scene) {
    super(scene);
    // WrappedPlainView
    // this.view = view;
    super.setCheckClipping(true);
  }

  protected abstract View getView();

  protected abstract View refreshView();

  protected abstract JLabel getLabel();

  @Override
  protected Rectangle calculateClientArea() {
    updateViewIfNeeded();
    Rectangle rec = new Rectangle();
    // the preferred size is here
    rec.height = (int) getView().getPreferredSpan(View.Y_AXIS);
    rec.width = (int) getView().getPreferredSpan(View.X_AXIS);
    return rec;
  }

  public boolean mustUpdate() {
    return !CtuluLib.isEquals(getFont(), getLabel().getFont())
        || !CtuluLib.isEquals(getForeground(), getLabel().getForeground());
  }

  /**
   *
   */
  @Override
  protected void paintWidget() {
    try {
      updateViewIfNeeded();
      Rectangle clientArea = getClientArea();
      getLabel().setFont(getFont());
      Graphics2D graphics = getGraphics();
      getView().paint(graphics, clientArea);
    } catch (Exception e) {
      System.out.println("Erreur paintWidget: " + e);
    }
  }

  private void updateViewIfNeeded() {
    if (mustUpdate()) {
      getLabel().setFont(getFont());
      getLabel().setForeground(getForeground());
      refreshView();
    }
  }

}
