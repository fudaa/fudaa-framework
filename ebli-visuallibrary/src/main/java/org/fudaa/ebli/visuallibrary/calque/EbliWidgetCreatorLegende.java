package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Font;
import java.util.Map;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.BCalqueLegendePanel;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;

/**
 * Creator pour les legendes des calques
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorLegende extends EbliWidgetCreator {

  BCalqueLegendePanel g;

  /**
   * La cible de la legende. Ce calque appartient au panel de calque. Cela permet de g�n�rer une legende par calque
   */
  BCalque calqueCible_;

  public EbliWidgetCreatorLegende(final BCalqueLegendePanel g, final EbliWidgetCreatorVueCalque satelliteCreatorOwner, BCalque calqueCible) {
    this(null, g, satelliteCreatorOwner, calqueCible);
  }

  protected EbliWidgetCreatorLegende(EbliWidgetCreatorLegende toCopy, final BCalqueLegendePanel g,
      final EbliWidgetCreatorVueCalque satelliteCreatorOwner, BCalque calqueCible) {
    super(toCopy);
    this.g = g;
    super.owner = satelliteCreatorOwner;
    calqueCible_ = calqueCible;
  }

  public EbliWidgetCreatorLegende() {

  }

  @Override
  public void widgetAttached() {
    super.widgetAttached();
    ((EbliWidgetCalqueLegende) getWidget().getIntern()).addListeners();
  }

  @Override
  public void widgetDetached() {
    super.widgetDetached();
    ((EbliWidgetCalqueLegende) getWidget().getIntern()).removeListeners();

  }

  public BCalqueLegendePanel getLegendPanel() {
    return g;
  }

  public void setLegendPanel(final BCalqueLegendePanel g) {
    this.g = g;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    EbliWidgetCalqueLegende legende = new EbliWidgetCalqueLegende(_scene, false, g);
    legende.updateLegendeWidget();
    EbliWidgetBordureSingle res = new EbliWidgetBordureSingle(legende, true, true);
    legende.setFormeFont(new Font("Helvetica", Font.PLAIN, 10));
    legende.setController(new EbliWidgetControllerLegendeCalque(res));
    return res;
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    EbliWidgetCreatorVueCalque duplicatedOwner = (EbliWidgetCreatorVueCalque) satteliteOwnerDuplicated;
    BCalque newCible = duplicatedOwner.getCalquesPanel().getDonneesCalqueByName(calqueCible_.getName());
    BCalqueLegende cqLegend = duplicatedOwner.getCalquesPanel().getCqLegend();
    BCalqueLegendePanel legendePanel = ((CalqueLegendeWidgetAdapter) cqLegend).getLegendPanel(newCible);
    return new EbliWidgetCreatorLegende(this, legendePanel, duplicatedOwner, newCible);
  }

  public static class CoupleData {
    public String idPosessor;
    public int indiceCalqueCible = 0;
    public String layerName;
  }

  @Override
  public Object savePersistData(final Map parameters) {
    CoupleData data = new CoupleData();
    // -- on recupere l'indice du calque de la legende --//
    data.idPosessor = owner.getWidget().getId();

    // -- recuperation du calque --//
    EbliWidget widget = owner.getWidget();
    if (widget != null && widget.getController() instanceof EbliWidgetControllerCalque) {
      ZEbliCalquesPanel calque = ((EbliWidgetControllerCalque) widget.getController()).getVisuPanel();

      data.indiceCalqueCible = calque.getArbreCalqueModel().getIndexOfChild(calque.getArbreCalqueModel().getRoot(), calqueCible_);
      if (data.indiceCalqueCible == -1)
        data.indiceCalqueCible = 0;
      data.layerName = calqueCible_.getName();
    }

    return data;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    if (data == null) {
      g = new BCalqueLegendePanel(new BCalqueLegende(), "empty");
      return true;
    } else {
      CoupleData datas = (CoupleData) data;
      String idCalque = datas.idPosessor;
      int indexCalque = datas.indiceCalqueCible;
      String nomCalque = datas.layerName;

      if (idCalque != null) {
        final EbliScene scene = (EbliScene) parameters.get("scene");
        // -- recherche de la widget qui contient l'id et rejouer l'action ajouter legende --//
        // -- recuperation du calque --//
        EbliNode nodeCalque = scene.findNodeById(idCalque);
        if (nodeCalque != null && nodeCalque.getController() instanceof EbliWidgetControllerCalque) {
          ZEbliCalquesPanel calque = ((EbliWidgetControllerCalque) nodeCalque.getController()).getVisuPanel();
          // -- recuperation du bon calque --//
          if (nomCalque != null) {
            calqueCible_ = (BCalque) calque.getArbreCalqueModel().getRootCalque().getCalque(nomCalque);
          }
          if(calqueCible_==null){
            calqueCible_ = (BCalque) calque.getArbreCalqueModel().getChild(calque.getArbreCalqueModel().getRoot(), indexCalque);
          }
          owner = nodeCalque.getCreator();
          BCalqueLegendePanel legendePanel = ((CalqueLegendeWidgetAdapter) calque.getCqLegend()).getLegendPanel(calqueCible_);
          g = legendePanel;
          // -- mise a jour des sattelites --//
          return g != null;
        }

      }
      return false;
    }

  }

}
