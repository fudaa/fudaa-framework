package org.fudaa.ebli.visuallibrary.creator;

import java.util.Map;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetDBLFleche;
import org.fudaa.ebli.visuallibrary.EbliWidgetWithBordure;

public class EbliWidgetCreatorDblFleche extends EbliWidgetCreator {

  int orientation;

  public EbliWidgetCreatorDblFleche(final int orientation) {
    super();
    this.orientation = orientation;

  }

  public EbliWidgetCreatorDblFleche() {

  }

  public EbliWidgetCreatorDblFleche(EbliWidgetCreatorDblFleche copy) {
    super(copy);
    orientation = copy.orientation;

  }

  public int getOrientation() {
    return orientation;
  }

  public void setG(final int orientation) {
    this.orientation = orientation;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    return new EbliWidgetDBLFleche(_scene, orientation);
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorDblFleche(this);
  }

  public EbliWidgetWithBordure getBordure() {
    return null;
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return null;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    return true;
  }

}
