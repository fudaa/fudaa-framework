package org.fudaa.ebli.visuallibrary.calque;

import com.memoire.bu.BuLib;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.SwingConstants;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluPair;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.LabelWidget.VerticalAlignment;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.layout.FlowLayout;

/**
 * Creation de la classe widget specifique au calque. Permet de gerer le resize via le changement de font
 */
public class EbliWidgetTimeLegende extends EbliWidget implements PropertyChangeListener {

  private class TimeListener implements ListDataListener, ListSelectionListener, PropertyChangeListener {

    @Override
    public void contentsChanged(ListDataEvent e) {
      reconstructWidget();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      reconstructWidget();
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
      reconstructWidget();

    }
  }

  private class TreeListener implements TreeModelListener {

    @Override
    public void treeNodesChanged(TreeModelEvent e) {

    }

    @Override
    public void treeNodesInserted(TreeModelEvent e) {
      calqueChanged();

    }

    private void calqueChanged() {
      removeListenerTimeTargets();
      updateTargetLayers();
      addListenersTimeTargets();
      reconstructWidget();
    }

    @Override
    public void treeNodesRemoved(TreeModelEvent e) {
      calqueChanged();

    }

    @Override
    public void treeStructureChanged(TreeModelEvent e) {
      calqueChanged();

    }

  }

  private class NodeTitleChangeListener implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == getParentWidget()) {
        updateTitle();
      }

    }

  }

  Font boldFont_;
  private ZEbliCalquesPanel calquesPanel;

  private boolean mustUpdateFont_;

  TimeListener timeListener = new TimeListener();
  TreeListener treeListener = new TreeListener();

  Set<BSelecteurListTimeTarget> timesTarget = new HashSet<BSelecteurListTimeTarget>();
  private PropertyChangeListener nodeTitleChangeListener = new NodeTitleChangeListener();

  public EbliWidgetTimeLegende(final EbliScene _scene, final boolean _controller) {
    super(_scene, _controller);
    setCheckClipping(true);
    setEnabled(true);
  }

  public void addListeners() {
    calquesPanel.getArbreCalqueModel().addTreeModelListener(treeListener);
    addListenersTimeTargets();
    getEbliScene().addPropertyChangeListener(EbliWidget.TITLE, nodeTitleChangeListener);
    updateTitle();
  }

  private void updateTitle() {
    final EbliNode n = (EbliNode) getEbliScene().findObject(getParentWidget());
    if (titre != null && n != null) {
      setTitle(n.getTitle());
    }
  }

  public boolean isTitleVisible() {
    return titre.isVisible();
  }

  private void addListenersTimeTargets() {
    for (BSelecteurListTimeTarget targets : timesTarget) {
      addListener(targets);
    }
  }

  private void addListener(BSelecteurListTimeTarget targets) {
    targets.getTimeListModel().addListDataListener(timeListener);
    targets.getTimeListSelectionModel().addListSelectionListener(timeListener);
    ((BCalque) targets).addPropertyChangeListener("visible", timeListener);
  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  @Override
  public boolean canColorForeground() {
    return false;
  }

  @Override
  public boolean canFont() {
    return true;
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  protected void notifyRemoved() {
  }

  @Override
  public void notifyStateChanged(final ObjectState _previousState, final ObjectState _newState) {
  }

  @Override
  protected void paintWidget() {
    if (mustUpdateFont_) {
      updateFont(getFormeFont());
    }
    super.paintWidget();
  }

  /**
   * Si la palette est modifiee on change la widget
   */
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    rebuildWidget();

  }

  private void rebuildWidget() {
    Font oldFond = super.getFormeFont();
    this.removeChildren();
    reconstructWidget();
    updateLegendView();
    setFormeFont(oldFond);
  }

  public void reconstructWidget() {
    updateTimesWidget();
    getParentWidget().revalidate();

  }

  public void removeListeners() {
    calquesPanel.getArbreCalqueModel().removeTreeModelListener(treeListener);
    removeListenerTimeTargets();
    getEbliScene().removePropertyChangeListener(EbliWidget.TITLE, nodeTitleChangeListener);

  }

  private void removeListenerTimeTargets() {
    for (BSelecteurListTimeTarget targets : timesTarget) {
      removeListener(targets);
    }
  }

  private void removeListener(BSelecteurListTimeTarget targets) {
    targets.getTimeListModel().removeListDataListener(timeListener);
    targets.getTimeListSelectionModel().removeListSelectionListener(timeListener);
    ((BCalque) targets).removePropertyChangeListener("visible", timeListener);
  }

  @Override
  public void setFormeFont(final Font _newFont) {
    super.setFormeFont(_newFont);
    updateFont(_newFont);
  }

  @Override
  public void setPropertyCmd(final String _key, final Object _prop, final CtuluCommandContainer _cmd) {
    super.setPropertyCmd(_key, _prop, _cmd);
    if ("font".equals(_key)) {
      final Font ft = (Font) _prop;
      updateFont(ft);
    }
  }

  public void setVisuPanel(EbliWidgetCreatorVueCalque owner) {
    calquesPanel = owner.getCalquesPanel();
    updateTargetLayers();

  }

  private void updateTargetLayers() {
    timesTarget.clear();
    BCalque[] tousCalques = calquesPanel.getArbreCalqueModel().getRootCalque().getTousCalques();
    for (BCalque bCalque : tousCalques) {
      if (bCalque instanceof BSelecteurListTimeTarget) {
        timesTarget.add((BSelecteurListTimeTarget) bCalque);
      }
    }
  }

  protected void updateFont(final Font ft) {
    if (getGraphics() == null) {
      mustUpdateFont_ = true;
      return;
    }
    mustUpdateFont_ = false;
    boldFont_ = BuLib.deriveFont(ft, Font.BOLD, 1);

    // -- mise a jour de la widget avec la nouvelle font
    updateWidgets(getChildren(), boldFont_);
    if (timeWidget != null) {
      updateWidgets(timeWidget.getChildren(), ft);
      recomputeSize();
    }
    updateLegendView();
  }

  private void updateWidgets(final List<Widget> listePlage, Font font) {
    for (final Iterator<Widget> it = listePlage.iterator(); it.hasNext();) {
      final Widget widget = it.next();
      if (widget instanceof LabelWidget && (((LabelWidget) widget).getLabel() != null)) {
        final LabelWidget lbWidget = (LabelWidget) widget;
        lbWidget.setFont(font);
      }
      if (widget instanceof EbliWidgetTwoPart) {
        ((EbliWidgetTwoPart) widget).setFormeFont(font);
      }
    }

  }

  private void updateLegendView() {
    revalidate();
    getParentWidget().revalidate();
    getEbliScene().refresh();
  }

  private Map<String, List<String>> getLayersByValues() {
    Map<String, List<String>> res = new TreeMap<String, List<String>>();
    for (BSelecteurListTimeTarget time : this.timesTarget) {
      BCalque cq = (BCalque) time;
      if (cq.isVisible() && !time.getTimeListSelectionModel().isSelectionEmpty()) {
        Object elementAt = time.getTimeListModel().getElementAt(time.getTimeListSelectionModel().getMaxSelectionIndex());
        if (elementAt != null) {
          String timeSelected = elementAt.toString();
          List<String> layers = res.get(timeSelected);
          if (layers == null) {
            layers = new ArrayList<String>();
            res.put(timeSelected, layers);
          }
          layers.add(((BCalque) time).getTitle());
        }
      }
    }
    return res;
  }

  Widget timeWidget;
  private LabelWidget titre;

  /**
   * Mananger statique qui se charge de creer �a widget associee au calque.
   */
  protected void updateTimesWidget() {
    final EbliScene _scene = getEbliScene();
    if (timeWidget == null) {
      setLayout(new FlowLayout(true, SerialAlignment.CENTER, 0));
//      setBorder(BorderFactory.createEmptyBorder(2));
      titre = new LabelWidget(_scene);
      titre.setBorder(BorderFactory.createEmptyBorder(3, 0, 0, 0));
      titre.setFont(boldFont_);
      titre.setAlignment(Alignment.CENTER);
      titre.setVerticalAlignment(VerticalAlignment.CENTER);
      titre.setLabel(EbliLib.getS("Temps"));
      titre.setEnabled(true);
      addChild(titre);
      timeWidget = new Widget(_scene);
      timeWidget.setLayout(new FlowLayout(true, SerialAlignment.LEFT_TOP, 0));
      addChild(timeWidget);
    }

    Map<String, List<String>> layersByValues = getLayersByValues();
    int nbWidgetNeeded = 0;
    if (layersByValues.size() == 1) {
      nbWidgetNeeded = 1;
    } else {
      Collection<List<String>> values = layersByValues.values();
      for (List<String> list : values) {
        nbWidgetNeeded += list.size();
      }
    }
    if (nbWidgetNeeded == 0) {
      timeWidget.removeChildren();
    } else {
      List<Widget> existantChildren = timeWidget.getChildren();
      List<EbliWidgetTwoPart> targetWidgets = new ArrayList<EbliWidgetTwoPart>(nbWidgetNeeded);
      int targetSize = nbWidgetNeeded;
      int existantSize = existantChildren.size();
      for (int i = targetSize; i < existantSize; i++) {
        timeWidget.removeChild(existantChildren.get(i));
      }
      for (int i = 0; i < Math.min(targetSize, existantSize); i++) {
        targetWidgets.add((EbliWidgetTwoPart) existantChildren.get(i));
      }
      for (int i = existantSize; i < targetSize; i++) {
        EbliWidgetTwoPart widgetTwoPart = new EbliWidgetTwoPart(getEbliScene());
        widgetTwoPart.setFont(getFormeFont());
        widgetTwoPart.setFormeFont(getFormeFont());
        timeWidget.addChild(widgetTwoPart);
        targetWidgets.add(widgetTwoPart);
      }
      if (nbWidgetNeeded == 1) {
        EbliWidgetTwoPart part = targetWidgets.get(0);
        part.setText(SwingConstants.LEFT, null);
        part.setWidth(SwingConstants.LEFT, 0);
        part.setText(SwingConstants.CENTER, layersByValues.keySet().iterator().next());
        part.setWidth(SwingConstants.CENTER, part.calculateNormalWidth(SwingConstants.CENTER));
      } else {

        List<CtuluPair<String>> pairs = new ArrayList<CtuluPair<String>>();
        for (Entry<String, List<String>> entry : layersByValues.entrySet()) {
          for (String calqueName : entry.getValue()) {
            pairs.add(new CtuluPair<String>(calqueName, entry.getKey()));
          }
        }
        Collections.sort(pairs);
        for (int i = 0; i < targetWidgets.size(); i++) {
          EbliWidgetTwoPart part = targetWidgets.get(i);
          part.setText(SwingConstants.LEFT, pairs.get(i).getKey());
          part.setText(SwingConstants.CENTER, pairs.get(i).getObject());
        }
        recomputeSize();
      }
    }
    getEbliScene().refresh();
  }

  private void recomputeSize() {
    int maxLeft = 0;
    int maxCenter = 0;
    List<Widget> children = timeWidget.getChildren();
    for (Widget widget : children) {
      EbliWidgetTwoPart part = (EbliWidgetTwoPart) widget;
      maxLeft = Math.max(maxLeft, part.calculateNormalWidth(SwingConstants.LEFT));
      maxCenter = Math.max(maxCenter, part.calculateNormalWidth(SwingConstants.CENTER));
    }
    for (Widget widget : children) {
      EbliWidgetTwoPart part = (EbliWidgetTwoPart) widget;
      part.setWidth(SwingConstants.LEFT, maxLeft);
      part.setWidth(SwingConstants.CENTER, maxCenter);
    }
  }

  public void setTitle(String title) {
    titre.setLabel(title);
    updateLegendView();
  }

  public void setTitleVisible(boolean b) {
    if (b != titre.isVisible()) {
      titre.setVisible(b);
      updateLegendView();
    }

  }
}