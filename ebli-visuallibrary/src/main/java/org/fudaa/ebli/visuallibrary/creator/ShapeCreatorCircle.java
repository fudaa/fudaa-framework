package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public class ShapeCreatorCircle implements ShapeCreator {

  @Override
  public Shape createShapeFor(final Rectangle2D.Float rec, final Map options, final float largeurTrait) {
    final float ray = Math.min(rec.width, rec.height);
    return new Ellipse2D.Float(rec.x, rec.y, ray, ray);
  }
}
