package org.fudaa.ebli.visuallibrary;

import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.visuallibrary.animation.EbliWidgetAnimatedItem;
import org.fudaa.ebli.visuallibrary.layout.OverlayLayoutGap;

/**
 * classe qui genere une bordure pour contenir une widget classique.
 * 
 * @author genesis
 */
public class EbliWidgetBordureSingle extends EbliWidgetWithBordure {

  /**
   * Widget contenu en intern dans la widget bordure
   */
  EbliWidget intern_;

  OverlayLayoutGap layout_;

  TraceLigne oldTraceligne_ = new TraceLigne(getTraceLigneModel().buildCopy());

  /**
   * constructeur qui gere un controller uniquement pour les actions.
   * 
   * @param scene
   */
  public EbliWidgetBordureSingle(final EbliWidget _intern) {
    this(_intern, true, true);
  }

  public EbliWidgetBordureSingle(final EbliWidget _intern, final boolean canMove, final boolean canResize) {
    super(_intern.getEbliScene(), false);

    intern_ = _intern;

    insets_ = createInset(getTraceLigneModel().getEpaisseur());
    layout_ = new OverlayLayoutGap(insets_);
    setLayout(layout_);

    // -- ajout du child --//
    addChild(intern_);
  }

  @Override
  public void addSatellite(EbliWidget w) {
    intern_.addSatellite(w);
  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  @Override
  public boolean canColorForeground() {
    return true;
  }

  @Override
  public boolean canFont() {
    return intern_.canFont();
  }

  @Override
  public boolean canRotate() {
    return intern_.canRotate();
  }

  @Override
  public boolean canTraceLigneModel() {
    return true;
  }

  public Insets createInset(final float epaisseur) {
    int intEpaisseur = (int) Math.ceil(epaisseur);
    return new Insets(intEpaisseur, intEpaisseur, intEpaisseur, intEpaisseur);
  }

  @Override
  public EbliAnimatedInterface getAnimatedInterface() {
    return getIntern().getAnimatedInterface();
  }

  @Override
  public List<EbliWidgetAnimatedItem> getAnimatedItems() {
    return getIntern().getAnimatedItems();
  }

  /**
   * Retourne le controlleur de son objet contenu. Le controlleur de la bordure n'a pas besoin d'etre recup�r�.
   */
  @Override
  public EbliWidgetController getController() {
    return getIntern().getController();
  }

  @Override
  public String getId() {

    return intern_.getId();
  }

  @Override
  public EbliWidget getIntern() {
    return intern_;
  }

  @Override
  public EbliWidget getParentBordure() {
    return this;
  }

  /**
   * surcharge des methodes getProperty pour recuperer les proprietes graphiques et les dipatcher a son conteneur
   */
  @Override
  public Object getProperty(final String _key) {

    if (_key.equals(FONT)) {
      return intern_.propGraphique.get(_key);
    } else
    // if (_key.equals(COLORFOND)) { return intern_.propGraphique.get(_key); }
    return super.getProperty(_key);
  }

  /**
   * surcharge des getter et setter graphical properties pour rediriger les infos vers les widgets contenu
   */
  @Override
  public Map<String, Object> getPropGraphique() {

    final Map<String, Object> mapMeltingPot = new HashMap<String, Object>();
    mapMeltingPot.put(COLORCONTOUR, getColorContour());
    mapMeltingPot.put(LINEMODEL, getTraceLigneModel());

    // -- les autres valeurs conernent le contenu --//
    mapMeltingPot.put(ROTATION, intern_.getRotation());
    mapMeltingPot.put(FONT, intern_.getFormeFont());
    mapMeltingPot.put(COLORFOND, intern_.getColorFond());
    mapMeltingPot.put(TRANSPARENCE, intern_.getTransparent());

    return mapMeltingPot;
  }

  @Override
  public Set<EbliWidget> getSatellites() {
    return intern_.getSatellites();
  }

  @Override
  public boolean hasSattelite() {
    return intern_.hasSattelite();
  }

  @Override
  public boolean isAnimatedItemAlive(final String _id) {
    return getIntern().isAnimatedItemAlive(_id);
  }

  @Override
  public boolean isInEditMode() {
    return intern_.isInEditMode_;
  }

  @Override
  protected void paintWidget() {

    // -- mise a jour de la fonte que si il y a eu une modification --//

    final Graphics2D g = getGraphics();

    final Rectangle rec = getClientArea();

    g.translate(rec.x, rec.y);

    final TraceLigne l = new TraceLigne(getTraceLigneModel());

    // // --test on autorise que si son widget fils le permet --//
    if (this.getIntern().canColorBackground() && !this.getTransparent()) {
      getGraphics().setColor(getColorFond());
      getGraphics().fillRect(0, 0, (int) (rec.width - l.getEpaisseur()), (int) (rec.height - l.getEpaisseur()));
    }

    l.setCouleur(getColorContour());
    l.dessineRectangle(g, (int) Math.floor(l.getEpaisseur() / 2), (int) Math.floor(l.getEpaisseur() / 2),
        (int) (rec.width - l.getEpaisseur() + 1), (int) (rec.height - l.getEpaisseur() + 1)/*
                                                                                            * , rec.width
                                                                                            */);

    g.translate(-rec.x, -rec.y);

    // -- on redimensionne la frame correctement --//
    if (oldTraceligne_.getEpaisseur() != l.getEpaisseur()) {
      insets_ = createInset(getTraceLigneModel().getEpaisseur());
      layout_.setInGaps(insets_);
      // setLayout(layout_);
      // -- on ajoute le delta d'epaisseur --//
      float delta = l.getEpaisseur() - oldTraceligne_.getEpaisseur();
      Rectangle sizeWidget = this.getPreferredBounds();
      // -- on rajoute le double de delta pour l'�paisseur des 2 cot�s --//
      sizeWidget.height += 2 * delta;
      // sizeWidget.width += 2 * delta+insets_.right+insets_.left;
      sizeWidget.width += 2 * delta;
      this.setPreferredBounds(sizeWidget);
      oldTraceligne_ = new TraceLigne(l);
    }

    revalidate();

  }

  @Override
  public void removeSatellite(EbliWidget w) {
    intern_.removeSatellite(w);
  }

  @Override
  public void setId(final String id_) {
    intern_.setId(id_);
  }

  public void setIntern(final EbliWidget _intern) {
    intern_ = _intern;
  }

  @Override
  protected void setPropertyCmd(final String _key, final Object _prop, final CtuluCommandContainer _cmd) {
    if (_key.equals(LINEMODEL) || _key.equals(COLORCONTOUR) || _key.equals(COLORFOND) || _key.equals(TRANSPARENCE)) {
      super.setPropertyCmd(_key, _prop, _cmd);
    } else {
      intern_.setPropertyCmd(_key, _prop, _cmd);
    }
  }

  @Override
  public void setSatellite(Collection<EbliWidget> liste) {
    intern_.setSatellite(liste);
  }

}
