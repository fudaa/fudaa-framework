package org.fudaa.ebli.visuallibrary.graphe;

import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuToolBar;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.action.EbliCalqueActionTimeChooser;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurListComboBox;
import org.fudaa.ebli.courbe.EGActionAfficheOrigineCourbe;
import org.fudaa.ebli.courbe.EGActionReplayDataCourbe;
import org.fudaa.ebli.courbe.EGExporter;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.courbe.EGListSimple;
import org.fudaa.ebli.courbe.EGPaletteInfo;
import org.fudaa.ebli.courbe.EGSpecificActions;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.ebli.courbe.EGTree;
import org.fudaa.ebli.courbe.EGVue;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetControllerMenuOnly;
import org.fudaa.ebli.visuallibrary.EbliWidgetWithBordure;
import org.fudaa.ebli.visuallibrary.actions.EbliActionEditorOneClick;

/**
 * controller specifique pour les actions de la widget du graphe. Ne gere pas les actions de deplacement. Ces actions seront gerees par la bordure.
 *
 * @author Adrien Hadoux
 */
public class EbliWidgetControllerGraphe extends EbliWidgetControllerMenuOnly implements CtuluExportDataInterface {

  private static int indiceLegende = 1;
  public boolean alreadyCreate_;
  public boolean hasAlreadyFusion = false;
  JLabel labelTrace_;
  BuMenuBar menuGraphe_;
  JComponent panelTreeGraphe_;
  BuToolBar toolbarGraphe_;
  final EbliWidgetGraphe widgetGraphe_;
  final EbliWidgetCreatorGraphe creatorGraphe_;
  EbliActionEditorOneClick<EGVue> editorAction_;

  public void triggerDestroy() {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
      }

      {
        editorAction_.closeEditor(true);
        getWidget().getEbliScene().refresh();
        actionSupprimer(getCmdManager());
        getWidget().getEbliScene().refresh();

      }
    });
  }

  public EbliWidgetControllerGraphe(final EbliWidgetCreatorGraphe creatorGraphe, final EbliWidgetGraphe grapheWidget,
          final EbliWidgetWithBordure _parent) {
    super(_parent);

    widgetGraphe_ = grapheWidget;
    creatorGraphe_ = creatorGraphe;

    labelTrace_ = (JLabel) CtuluLibSwing.findChildByName(getGraphePanel(), "lbTools");
    // getGraphePanel().remove(labelTrace_);

    addActionSpecifiques();

    setProportional(true);

  }

  @Override
  public boolean isEditable() {
    return true;

  }

  public void addActionSpecifiques() {
    editorAction_ = new EbliActionEditorOneClick<EGVue>(widgetGraphe_);
    // -- ajout de l action au widget correspondant --//
    widgetGraphe_.getActions().addAction(editorAction_);

  }

  /**
   * ajoute des toolsbuton specifiques si pas deja ajoutes.
   */
  @Override
  public void addFonctionsSpecific(final ArrayList<EbliActionSimple> _listeActions) {

    if (!fonctionsAlreadySpecified()) {
      alreadyCreate_ = true;

      for (final Iterator<EbliActionSimple> it = _listeActions.iterator(); it.hasNext();) {
        final EbliActionSimple action = it.next();
        toolbarGraphe_.add(action.buildToolButton(EbliComponentFactory.INSTANCE));
        menuGraphe_.getMenu(0).add(new JMenuItem(action));
      }

    }

  }

  public boolean hasLegende() {
    return widgetGraphe_.hasSattelite();
  }

  public void ajoutLegende() {
    // -- creation de la l�gende --//
    if (widget_.getEbliScene() != null) {

      if (!hasLegende()) {

        // -- creation d'un node legende --//

        // -- nom de la legende --//
        EbliNode nodeLegende = new EbliNodeDefault();
        nodeLegende.setTitle(EbliLib.getS("L�gende")+" " + (indiceLegende++));

        final EbliWidgetCreatorLegende creator = new EbliWidgetCreatorLegende(widgetGraphe_.getGraphe(), creatorGraphe_);

        nodeLegende.setCreator(creator);

        final Point positionLegende = new Point();
        if (widget_.getParentBordure().getBounds() != null && widget_.getParentBordure().getLocation() != null) {
          positionLegende.x = widget_.getParentBordure().getLocation().x + widget_.getParentBordure().getBounds().width + 20;
          positionLegende.y = widget_.getParentBordure().getLocation().y;
          creator.setPreferredLocation(positionLegende);
        }
        widget_.getEbliScene().addNode(nodeLegende);
        widgetGraphe_.addSatellite(nodeLegende.getWidget());
        widget_.getEbliScene().refresh();

        // -- reinitialisation des titres de la legendes --//
        widgetGraphe_.panelGraphe_.getGraphe().reinitTitlesOrigins();
      } else {
        Set<EbliWidget> satellites = widgetGraphe_.getSatellites();
        for (EbliWidget ebliWidget : satellites) {
          ebliWidget.setVisible(true);
        }
      }

    }
  }

  /**
   * ajoutant la possibilit� de configurer la courbe ainsi que la l�gende
   *
   * @param _popup
   */
  private void constructPopupMenuSpecifique(final JPopupMenu _popup) {
    JMenuItem menuItem = _popup.add(EbliResource.EBLI.getString("Ajouter la l�gende"));
    menuItem.setIcon(CtuluResource.CTULU.getIcon("crystal_commentaire"));
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {

        ajoutLegende();
      }
    });

    menuItem = new JMenuItem(EbliResource.EBLI.getString("Editer le graphe"));
    _popup.add(menuItem, 0);
    menuItem.setIcon(CtuluResource.CTULU.getIcon("crystal_editer"));
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {

        MenuEditer();

      }
    });

    // -- creation du menu config de la courbe

  }

  /**
   * Mise au premier plan de la widget et edition. Sert beaucoup pour la fusion
   */
  public void MenuEditer() {
    // -- executer l action d edition --//
    editorAction_.openEditorFromMenu(getWidget());

  }

  @Override
  protected void buildPopupMenu(final JPopupMenu _menu) {
    constructPopupMenuSpecifique(_menu);

    // -- creation du menu commun a tous les widgets
    constructPopupMenuBase(_menu);

  }

  @Override
  public boolean fonctionsAlreadySpecified() {
    return alreadyCreate_;
  }

  public EGGraphe getGraphe() {
    return widgetGraphe_.getGraphe();
  }

  public EGFillePanel getGraphePanel() {
    return widgetGraphe_.panelGraphe_;
  }

  /**
   * retourne le menuBar associee au calque
   *
   * @return
   */
  @Override
  public JMenuBar getMenubarComponent() {

    if (menuGraphe_ == null) {
      menuGraphe_ = new BuMenuBar();
      final JMenu menu = new JMenu(EbliResource.EBLI.getString("Menu graphe"));
      getGraphePanel().fillSpecificMenu(menu);
      menuGraphe_.add(menu);
      menuGraphe_.add(createMenuExport(getGraphePanel(), getWidget().getEbliScene().getCtuluUi()));

    }

    return menuGraphe_;

  }

  /**
   * Recupere les objets graphique tree et panel infos associe au calque.
   *
   * @return
   */
  @Override
  public JComponent getOverviewComponent() {

    if (panelTreeGraphe_ == null) {
      final JComponent tree = createComponent();

      final EGTableGraphePanel tablePanel = new EGTableGraphePanel(false);
      tablePanel.setGraphe(getGraphe());
      tablePanel.addPanelAction(getGraphePanel());
      final JSplitPane pane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

      final JScrollPane compTop = new JScrollPane(tree);
      compTop.setPreferredSize(new Dimension(150, 200));
      pane.setTopComponent(compTop);

      // -- panel global qui contient la paletet d'infos + tableau de valeurs --//
      final JSplitPane panelBottom = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
      // TODO BM Voir si la palette info doit gerer le undo.
      EGPaletteInfo paletteInfos = new EGPaletteInfo(getGraphePanel(), null);
      paletteInfos.setAvailable(true);
      paletteInfos.setPreferredSize(new Dimension(150, 120));

      // -- ajout des actions origine et replayData pour les graphes--//
      BuPanel panelBoutons = new BuPanel(new FlowLayout(FlowLayout.LEFT));
      EGActionAfficheOrigineCourbe origine = new EGActionAfficheOrigineCourbe(getGraphePanel(), getWidget().getEbliScene().getCtuluUi());
      panelBoutons.add(origine.buildButton(EbliComponentFactory.INSTANCE));
      EGActionReplayDataCourbe replayData = new EGActionReplayDataCourbe(getGraphePanel(), getWidget().getEbliScene().getCtuluUi());
      panelBoutons.add(replayData.buildButton(EbliComponentFactory.INSTANCE));

      BuPanel miseEnPage = new BuPanel(new BorderLayout());
      miseEnPage.add(paletteInfos, BorderLayout.CENTER);
      miseEnPage.add(panelBoutons, BorderLayout.SOUTH);
      miseEnPage.setPreferredSize(new Dimension(150, 150));
      panelBottom.setTopComponent(miseEnPage);

      final JScrollPane comp = new JScrollPane(tablePanel);
      comp.setPreferredSize(new Dimension(150, 150));

      panelBottom.setBottomComponent(comp);
      pane.setBottomComponent(panelBottom);

      pane.setDividerLocation(0.5D);
      pane.resetToPreferredSizes();
      getGraphePanel().majSelectionListener(tablePanel);

      panelTreeGraphe_ = pane;// paneGlobal;
    }

    return panelTreeGraphe_;
  }

  private JComponent createComponent() {
    EGGrapheModel model = getGraphe().getModel();
    if (model instanceof EGGrapheSimpleModel) {
      EGListSimple list = new EGListSimple();
      list.setModel(((EGGrapheSimpleModel) model).createTableTreeModel());
      EGSpecificActions listAction = new EGSpecificActions(getGraphe());
      listAction.setCmd(getCmdManager());
      return list;

    }
    final EGTree tree_ = new EGTree();
    tree_.setActions(new EGSpecificActions(getGraphe(), getWidget().getEbliScene().getCtuluUi()));

    final EGGrapheTreeModel treeModel = (EGGrapheTreeModel) model;
    tree_.setModel(treeModel);
    tree_.setSelectionModel(treeModel.getSelectionModel());
    return tree_;
  }

  /**
   * Obtient la toolbar specifique au calque
   *
   * @return
   */
  @Override
  public JToolBar getToolbarComponent() {

    if (toolbarGraphe_ == null) {
      toolbarGraphe_ = new BuToolBar();
      // ajout des toolbar specifiques du calque
      final EbliActionInterface[] specificInterfaces = getGraphePanel().getSpecificActions();
      for (int i = 0; i < specificInterfaces.length; i++) {
        final EbliActionInterface object = specificInterfaces[i];
        if (object == null) {
          toolbarGraphe_.addSeparator();
        } else if (object instanceof EbliCalqueActionTimeChooser) {
          // -- ajout des combo des pas de temps --//
          final EbliCalqueActionTimeChooser chooserT = (EbliCalqueActionTimeChooser) object;
          // pour activer l'action
          chooserT.setSelected(true);
          final JComponent paletteContent = chooserT.getPaletteContent();
          chooserT.updateBeforeShow();
          // ajout combobox
          toolbarGraphe_.add(paletteContent);
          if (paletteContent instanceof BSelecteurListComboBox) {
            BSelecteurListComboBox.updateComboPreferredSize((BSelecteurListComboBox) paletteContent);
          }
        } else {
          toolbarGraphe_.add(object.buildToolButton(EbliComponentFactory.INSTANCE));
        }
      }
    }

    return toolbarGraphe_;
  }

  /**
   * affiche le label des coordonnees
   */
  @Override
  public JComponent getTracableComponent() {
    return labelTrace_;
  }

  @Override
  public boolean isDataExportable() {
    return true;
  }

  @Override
  public String getTitle() {
    return getNode().getTitle();
  }

  @Override
  public void startExport(CtuluUI ui) {
    EGExporter.startExport(getGraphe(), ui, null);
  }
}
