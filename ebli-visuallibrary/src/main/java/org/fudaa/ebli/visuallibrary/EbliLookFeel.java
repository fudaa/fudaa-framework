package org.fudaa.ebli.visuallibrary;

import org.netbeans.api.visual.border.Border;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.modules.visual.laf.DefaultLookFeel;

import java.awt.*;

public class EbliLookFeel extends DefaultLookFeel {
  private static final Color COLOR_HOVERED = new Color(0x447BCD);
  private static final Color COLOR_SELECTED = COLOR_HOVERED.brighter();
  /**
   *
   */
  private static final Border ROUNDED_BORDER = BorderFactory.createRoundedBorder(0, 0, 0, 0, COLOR_SELECTED,
      COLOR_HOVERED);
  private static final Border EMPTY_BORDER = BorderFactory.createEmptyBorder();
  private static final int MINI_THICKNESS = 8;
  private static final Border MINI_BORDER_SELECTED = BorderFactory.createRoundedBorder(MINI_THICKNESS, MINI_THICKNESS,
      MINI_THICKNESS, MINI_THICKNESS, COLOR_SELECTED, COLOR_SELECTED.darker());
  private static final Border RESIZE_BORDER = BorderFactory.createResizeBorder(MINI_THICKNESS, COLOR_HOVERED, true);
  private static final Border RESIZE_BORDER_SELECTED = BorderFactory.createCompositeBorder(ROUNDED_BORDER,
      RESIZE_BORDER);
  private static final Border RESIZE_BORDER_PROP = new ProportionnalResizeBorder(MINI_THICKNESS, COLOR_HOVERED, true);
  //  private static final Border RESIZE_BORDER_SELECTED_PROP = BorderFactory.createCompositeBorder(ROUNDED_BORDER,
//      RESIZE_BORDER_PROP);
  private static final Border DEFAULT_BORDER = BorderFactory.createEmptyBorder(MINI_THICKNESS);
  private static final Border NON_RESIZE_BORDER_SELECTED = BorderFactory.createCompositeBorder(ROUNDED_BORDER,
      DEFAULT_BORDER);
  private static final Border NON_RESIZE_BORDER = BorderFactory.createRoundedBorder(MINI_THICKNESS, MINI_THICKNESS,
      MINI_THICKNESS, MINI_THICKNESS, null, COLOR_SELECTED.darker());
  private static final Border SELECTED_IN_GROUP= BorderFactory.createRoundedBorder(MINI_THICKNESS, MINI_THICKNESS,
      MINI_THICKNESS, MINI_THICKNESS, null, COLOR_SELECTED);

  @Override
  public Border getBorder(final ObjectState _state) {
    return getBorder(_state, true, false);
  }

  public static int getBorderThickness() {
    return MINI_THICKNESS;
  }

  public Border getBorderInGroup(final ObjectState _state) {
    if (_state.isSelected()) {
      return SELECTED_IN_GROUP;
    }
    return getBorder(_state, false, false);
  }

  public Border getBorder(final ObjectState _state, final boolean resizable, final boolean prop) {
    if (resizable) {
      if (_state.isHovered()) {
        if (_state.isSelected()) {
          return RESIZE_BORDER_SELECTED;
        }
        return prop ? RESIZE_BORDER_PROP : RESIZE_BORDER;
      }
      if (_state.isSelected()) {
        return MINI_BORDER_SELECTED;
      }
    } else {
      if (_state.isSelected()) {
        return NON_RESIZE_BORDER_SELECTED;
      } else if (_state.isHovered()) {
        return NON_RESIZE_BORDER;
      }
    }

    return DEFAULT_BORDER;
  }

  /**
   * @return the defaultBorder
   */
  public static Border getDefaultBorder() {
    return DEFAULT_BORDER;
  }
}
