/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.animation;

import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * @author deniger
 */
public class EbliWidgetAnimationComposition implements EbliAnimationAdapterInterface {

  final List<EbliWidgetAnimatedItem> anims_;
  final List<TIntArrayList> tsIdx_ = new ArrayList<TIntArrayList>();
  final EbliScene scene_;
  final double eps = 1E-3;

  public EbliWidgetAnimationComposition(final EbliScene _scene, final List<EbliWidgetAnimatedItem> _items) {
    super();
    scene_ = _scene;
    anims_ = _items;
    buildTs();
  }

  private void buildTs() {
    tsIdx_.clear();
    if (anims_.isEmpty()) return;
    final int size = anims_.size();
    if (size == 1) {
      tsIdx_.add(null);
      return;
    }
    final EbliWidgetAnimatedItem it = anims_.get(0);
    final int nbTime = it.getAnimated().getNbTimeStep();
    for (int i = size - 1; i >= 0; i--) {
      tsIdx_.add(new TIntArrayList(nbTime));
    }

    final int[] indexes = new int[size - 1];
    for (int i = 0; i < nbTime; i++) {
      final boolean ok = findNext(indexes, it.getAnimated().getTimeStepValueSec(i));
      if (ok) {
        tsIdx_.get(0).add(i);
        for (int k = size - 1; k > 0; k--) {
          tsIdx_.get(k).add(indexes[k - 1]);
        }
      }
    }

  }

  private boolean findNext(final int[] idx, final double _v) {
    Arrays.fill(idx, -1);
    final int nb = anims_.size();
    boolean ok = true;
    for (int i = 1; i < nb; i++) {
      final int res = findIn(anims_.get(i).getAnimated(), _v);
      idx[i - 1] = res;
      if (res < 0) ok = false;
    }
    return ok;
  }

  private int findIn(final EbliAnimationAdapterInterface _src, final double _val) {
    for (int i = 0; i < _src.getNbTimeStep(); i++) {
      if (CtuluLib.isZero(_val - _src.getTimeStepValueSec(i), eps)) return i;
    }
    return -1;
  }

  public int getNbItems() {
    return anims_.size();
  }

  public boolean contains(final EbliAnimationAdapterInterface _anim) {
    for (final EbliWidgetAnimatedItem it : anims_) {
      if (CtuluLib.isEquals(_anim, it.getAnimated())) return true;
    }
    return false;
  }

  protected boolean removeKilledItems() {
    boolean removed = false;
    for (int i = anims_.size() - 1; i >= 0; i--) {
      if (!anims_.get(i).isAlive(scene_)) {
        anims_.remove(i);
        tsIdx_.remove(i);
        removed = true;
      }

    }
    if (removed) buildTs();
    return removed;
  }

  protected int getTsFor(final int _animsIdx, final int _tidx) {
    final TIntArrayList list = tsIdx_.get(_animsIdx);
    return list == null ? _tidx : list.get(_tidx);
  }

  @Override
  public int getNbTimeStep() {
    final TIntArrayList list = tsIdx_.get(0);
    return list == null ? anims_.get(0).getAnimated().getNbTimeStep() : tsIdx_.get(0).size();
  }

  @Override
  public String getTimeStep(final int _idx) {
    return anims_.get(0).getAnimated().getTimeStep(getTsFor(0, _idx));
  }

  @Override
  public double getTimeStepValueSec(final int _idx) {
    return anims_.get(0).getAnimated().getTimeStepValueSec(getTsFor(0, _idx));
  }

  @Override
  public String getTitle() {
    return EbliLib.getS("Sources muliples");
  }

  @Override
  public void setTimeStep(final int _idx) {
    int idx = 0;
    for (final EbliWidgetAnimatedItem it : anims_) {
      it.getAnimated().setTimeStep(getTsFor(idx++, _idx));
    }
  }

}
