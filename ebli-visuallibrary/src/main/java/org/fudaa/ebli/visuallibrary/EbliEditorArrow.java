package org.fudaa.ebli.visuallibrary;

import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;


/**
 * Editeur de fleche.
 * Gere 4 points (2 pour le segment) et 2 pour les extr�mit�s 
 * Gere 2 type: plein ou vide
 * 
 * Permet � l'utilisateur de modifier sa fleche en cliquant dessus
 * 
 * @author Adrien Hadoux
 *
 */
@SuppressWarnings("serial")
public class EbliEditorArrow extends BuPanel implements CtuluImageProducer,  MouseListener, MouseMotionListener {

	public final static int ExtremiteTete=0;
	public final static int ExtremiteQueue=1;
	public final static int ExtremiteGauche=2;
	public final static int ExtremiteDroite=3;


	public EbliEditorArrow(){
		this(new Point[ExtremiteDroite+1]);
		//-- on initialise --//

		creationFlecheClassique();
		repaint();
	}

	/**
	 * Constructeur appel� pour une cr�ation de base.
	 * @param points
	 */
	public EbliEditorArrow(Point[] points){
		points_=points;
		setSize(200, 150);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		repaint();
	}


	private Point[] points_;




	/**
	 * Model traceligne appel� a chaque edition de la fleche pour mettre a jour le trac�.
	 */
	public TraceLigneModel model_;
	public Color background_=Color.white;
	public Color foreground_=Color.black;

	public Point getExtremiteTete(){
		return points_[ExtremiteTete];
	}

	public Point getExtremiteQueue(){
		return points_[ExtremiteQueue];
	}


	public Point getExtremiteGauche(){
		return points_[ExtremiteGauche];
	}

	public Point getExtremiteDroite(){
		return points_[ExtremiteDroite];
	}

	public Point[] getPoints() {
		return points_;
	}

	public void setPoints_(Point[] points_) {
		this.points_ = points_;
	}

	public void modifieExtremite(Point newPoint, int position){
		if(position>ExtremiteDroite || points_==null)return;
		//-- cas particulier si on deplace la queue --//
		if(position==ExtremiteQueue && points_[position]!=null && points_[ExtremiteGauche]!=null && points_[ExtremiteDroite]!=null && points_[position]!=points_[ExtremiteQueue]){
			//-- on deplace les arcs selon la meme translation
			int translationX=points_[ExtremiteQueue].x-newPoint.x;
			int translationY=points_[ExtremiteQueue].y-newPoint.y;
			
			//-- on applique la translation aux arretes --//
			points_[ExtremiteGauche].x+=translationX;
			points_[ExtremiteGauche].y+=translationY;
			points_[ExtremiteDroite].x+=translationX;
			points_[ExtremiteDroite].y+=translationY;
			
			
		}
		
		
		points_[position]=newPoint;
		repaint();
	}

	public void clearArrow(){
		points_=new Point[ExtremiteDroite+1];
		repaint();
	}



	int previousWidth_=0;
	int previousHeight_=0;


	public void recalculatePosition(int w,int h){
		if(points_==null)return;
		if(previousWidth_==0)previousWidth_=getWidth();
		if(previousHeight_==0)previousHeight_=getHeight();
		if(previousWidth_==0)return;
		if(previousHeight_==0)return;
		
		double ratioW=1.0*w/previousWidth_;
		double ratioH=1.0*h/previousHeight_;


		for(int i=0;i<points_.length;i++){
			if(points_[i]!=null){
				points_[i].x=(int)(points_[i].x*ratioW);
				points_[i].y=(int)(points_[i].y*ratioH);
			}
		}
		previousWidth_=w;
		previousHeight_=h;
	}

	/**
	 * Methode qui dessine la fleche.
	 */
	public void print(Graphics _g,int w,int h) {


		//recalculatePosition(w,h);

		final BufferedImage image = CtuluLibImage.createImage(w, h, new HashMap());
		final Graphics2D g2d = image.createGraphics();
		image.flush();




		if(points_==null)return;

		if(existeAucunPoint()){
			g2d.setColor(Color.black);
			g2d.drawString("Veuillez cliquer sur les 4 points pour cr�er la fl�che", 10, 10);
			g2d.drawString("Modifier: faire des glisser-d�pos� pr�s des extr�mit�s", 10, 20);

		}

		// -- creation du traceligne --//
		final TraceLigne traceligne = new TraceLigne(model_);
		traceligne.setCouleur(foreground_);
		
		//-- on trace le segment initial --//
		if(getExtremiteTete()!=null && getExtremiteQueue()!=null){
			traceligne.dessineTrait(g2d, getExtremiteTete().x, getExtremiteTete().y, getExtremiteQueue().x,getExtremiteQueue().y);
		}else{
			// pas de segment complet, on trace juste les points existants
			if(getExtremiteTete()!=null)
				traceligne.dessineRectangle(g2d,getExtremiteTete().x-traceligne.getEpaisseur()/2,getExtremiteTete().y-traceligne.getEpaisseur()/2,traceligne.getEpaisseur(), traceligne.getEpaisseur());
			if(getExtremiteQueue()!=null)
				traceligne.dessineRectangle(g2d,getExtremiteQueue().x-traceligne.getEpaisseur()/2,getExtremiteQueue().y-traceligne.getEpaisseur()/2,traceligne.getEpaisseur(), traceligne.getEpaisseur());

		}
		//-- on trace le segment tete/gauche --//
		if(getExtremiteQueue()!=null && getExtremiteGauche()!=null){
			traceligne.dessineTrait(g2d, getExtremiteQueue().x, getExtremiteQueue().y, getExtremiteGauche().x,getExtremiteGauche().y);
		}else{
			// pas de segment complet, on trace juste les points existants
			if(getExtremiteGauche()!=null)
				traceligne.dessineRectangle(g2d,getExtremiteGauche().x-traceligne.getEpaisseur()/2,getExtremiteGauche().y-traceligne.getEpaisseur()/2,traceligne.getEpaisseur(), traceligne.getEpaisseur());

		} 
		//-- on trace le segment tete/droite --//
		if(getExtremiteQueue()!=null && getExtremiteDroite()!=null){
			traceligne.dessineTrait(g2d, getExtremiteQueue().x, getExtremiteQueue().y, getExtremiteDroite().x,getExtremiteDroite().y);
		}else{
			// pas de segment complet, on trace juste les points existants
			if(getExtremiteDroite()!=null)
				traceligne.dessineRectangle(g2d,getExtremiteDroite().x-traceligne.getEpaisseur()/2,getExtremiteDroite().y-traceligne.getEpaisseur()/2,traceligne.getEpaisseur(), traceligne.getEpaisseur());

		} 

		//-- on dessine l'image --//
		_g.drawImage(image, 0,0, null);

	}

	@Override
	public void paintComponent(Graphics _g) {

		//-- empecher de voir d'autres dessins par dessus --//
		_g.setColor(background_);
		_g.fillRect(0, 0, getWidth(), getHeight());
		_g.setColor(foreground_);
		print(_g,getWidth(),getHeight() );

		//-- on affiche les marqueurs drag and drop --//
		if(marqueurDeplacement_ && positionPointToMove_!=-1){
			_g.setColor(Color.RED);
			//-- position initiale --//
			_g.fillRect(points_[positionPointToMove_].x-3,points_[positionPointToMove_].y-3,6,6);

			//-- position deplacement --//
			if(positionDeplacement_!=null){
				_g.setColor(Color.GREEN);

				_g.fillRect(positionDeplacement_.x-3,positionDeplacement_.y-3,6,6);
				//-- on dessine la droite qui va etre deplac�e
				int positionDroite=-1;
				if(positionPointToMove_==ExtremiteTete)
					positionDroite=ExtremiteQueue;
				if(positionPointToMove_==ExtremiteQueue)
					positionDroite=ExtremiteTete;
				if(positionPointToMove_==ExtremiteDroite)
					positionDroite=ExtremiteQueue;
				if(positionPointToMove_==ExtremiteGauche)
					positionDroite=ExtremiteQueue;
				
				
				_g.drawLine(points_[positionDroite].x, points_[positionDroite].y, positionDeplacement_.x, positionDeplacement_.y);
				
				
			}


		}

	}

  @Override
	public BufferedImage produceImage(final int _w, final int _h, final Map _params) {

		recalculatePosition(_w,_h);

		final BufferedImage i = CtuluLibImage.createImage(_w, _h, _params);
		final Graphics2D g2d = i.createGraphics();
		CtuluLibImage.setBestQuality(g2d);
		if (CtuluLibImage.mustFillBackground(_params)) {
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, _w, _h);
		}

		if (getWidth() != _w || getHeight() != _h) {
						
			//			this.setPreferredSize(new Dimension(_w, _h));

			g2d.scale(CtuluLibImage.getRatio(_w, getWidth()), CtuluLibImage.getRatio(_h, getHeight()));
			setSize(_w, _h);
		}

		print(g2d,_w,_h);
		g2d.dispose();
		i.flush();
		return i;
	}

  @Override
	public Dimension getDefaultImageDimension() {
		return getSize();
	}

  @Override
	public BufferedImage produceImage(Map _params) {
		final BufferedImage i = CtuluLibImage.createImage(getWidth(), getHeight(), _params);
		final Graphics2D g2d = i.createGraphics();
		CtuluLibImage.setBestQuality(g2d);
		if (CtuluLibImage.mustFillBackground(_params)) {
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, getWidth(), getHeight());
		}


		print(g2d);
		g2d.dispose();
		i.flush();
		return i;
	}

	/**
	 * Methode qui est appel�e apr�s un clic et qui d�termine le point manquant � ajouter:
	 * @param p
	 */
	public void determinePointAajouter(Point p){
		if(p==null)return;

		if(getExtremiteTete()==null)
			modifieExtremite(p, ExtremiteTete);
		else
			if(getExtremiteQueue()==null)
				modifieExtremite(p, ExtremiteQueue);
			else
				if(getExtremiteGauche()==null)
					modifieExtremite(p, ExtremiteGauche);
				else
					if(getExtremiteDroite()==null)
						modifieExtremite(p, ExtremiteDroite);
					else return;
	}		


	/**
	 * Retourne true si le point est a ajouter ou false si il s'agit d'un d�placement
	 * @return
	 */
	public boolean ajout(){
		if(getExtremiteTete()==null)
			return true;
		if(getExtremiteQueue()==null)
			return true;
		if(getExtremiteDroite()==null)
			return true;
		if(getExtremiteGauche()==null)
			return true;
		return false;
	}

	public boolean existeAucunPoint(){
		if(getExtremiteTete()!=null)
			return false;
		if(getExtremiteQueue()!=null)
			return false;
		if(getExtremiteDroite()!=null)
			return false;
		if(getExtremiteGauche()!=null)
			return false;
		return true;
	}

	/**
	 * Methode qui determine le point a modifier de la fleche, c'est a dire le point qui se trouve le plus proche du clic utilisateur au point p.
	 * @param p
	 * @return
	 */
	public int determinePointAmodifier(Point p){

		double distanceMin=p.distance(getExtremiteTete());
		int position=ExtremiteTete;

		if(distanceMin>p.distance(getExtremiteQueue())){
			distanceMin=p.distance(getExtremiteQueue());
			position=ExtremiteQueue;
		}

		if(distanceMin>p.distance(getExtremiteDroite())){
			distanceMin=p.distance(getExtremiteDroite());
			position=ExtremiteDroite;
		}

		if(distanceMin>p.distance(getExtremiteGauche())){
			distanceMin=p.distance(getExtremiteGauche());
			position=ExtremiteGauche;
		}

		return position;
	}


  @Override
	public void mouseClicked(MouseEvent e) {

		if(ajout())
			determinePointAajouter(new Point(e.getX(),e.getY()));
		else{

			//-- mode modification du point --//
			//	modifieExtremite(new Point(e.getX(),e.getY()), determinePointAmodifier(new Point(e.getX(),e.getY())));
			//RIEN FAIRE
		}
		repaint();
	}

  @Override
	public void mouseEntered(MouseEvent e) {

	}

  @Override
	public void mouseExited(MouseEvent e) {

	}
	/**
	 * lors d'un drag and drop, on retient la position du point qu'on veut deplacer
	 * et lors du relachement souris ou remplace les infos
	 */
	int positionPointToMove_=-1;

	/**
	 * marqueur qui s'active dans le cas du drag and drop et qui marque le point initial ainsi que le point destination jusqu'au lacher.
	 */
	boolean marqueurDeplacement_=false;
  @Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if(!ajout()){
			positionPointToMove_=determinePointAmodifier(new Point(e.getX(),e.getY()));
			marqueurDeplacement_=true;
//			System.out.println("Souris pressed: positionToMove:"+positionPointToMove_);
			repaint();
		}
		else
			positionPointToMove_=-1;


	}

  @Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if(!ajout() && positionPointToMove_!=-1){
			modifieExtremite(new Point(e.getX(),e.getY()), positionPointToMove_);
			positionPointToMove_=-1;
//			System.out.println("Souris released: positionToMove:"+positionPointToMove_);
			marqueurDeplacement_=false;
			positionDeplacement_=null;
			repaint();
		}
	}

	Point positionDeplacement_;
  @Override
	public void mouseDragged(MouseEvent e) {
		//System.out.println("Souris dragged: "+positionPointToMove_);
		if(marqueurDeplacement_){
			positionDeplacement_=new Point(e.getX(),e.getY());
			repaint();
		}
	}

  @Override
	public void mouseMoved(MouseEvent e) {
//		System.out.println("Souris MOVED: "+positionPointToMove_);
	}


	public EbliEditorArrow duplicate(){
		EbliEditorArrow duplic=new EbliEditorArrow(points_.clone());
		duplic.model_=new TraceLigneModel(model_);
		return duplic;
	}


	public void creationFlecheClassique(){
		clearArrow();
		if(model_==null)
			model_=new TraceLigneModel();
		modifieExtremite(new Point((int)(getWidth()-model_.getEpaisseur()),(int)(getHeight()/2.0-model_.getEpaisseur()/2.0)), ExtremiteTete);
		modifieExtremite(new Point((int)(0+model_.getEpaisseur()),(int)(getHeight()/2.0-model_.getEpaisseur()/2.0)), ExtremiteQueue);
		modifieExtremite(new Point((int)(1.0/3.0*getWidth()),(int)(model_.getEpaisseur())), ExtremiteDroite);
		modifieExtremite(new Point((int)(1.0/3.0*getWidth()),(int)(getHeight()-model_.getEpaisseur())), ExtremiteGauche);
		repaint();
	}

	public void creationDoubleFlecheClassique(){
		clearArrow();
		if(model_==null)
			model_=new TraceLigneModel();
		modifieExtremite(new Point((int)(getWidth()-model_.getEpaisseur()),(int)(getHeight()/2.0-model_.getEpaisseur()/2.0)), ExtremiteTete);
		modifieExtremite(new Point((int)(0+model_.getEpaisseur()),(int)(getHeight()/2.0-model_.getEpaisseur()/2.0)), ExtremiteQueue);
		modifieExtremite(new Point((int)(2.0/3.0*getWidth()),(int)(model_.getEpaisseur())), ExtremiteDroite);
		modifieExtremite(new Point((int)(2.0/3.0*getWidth()),(int)(getHeight()-model_.getEpaisseur())), ExtremiteGauche);
		repaint();
	}

	JToolBar toolbar_;
	JMenuBar menuBar_;
	public JToolBar getToolBar( ) {
		if(toolbar_==null){
			toolbar_=new JToolBar();
			for(EbliActionSimple action:getActions())
				toolbar_.add(action);
		}
		return  toolbar_;
	}

	public JMenuBar getMenuBar( ) {
		if(menuBar_==null){
			menuBar_=new JMenuBar();
			JMenu principal=new JMenu("Menu");
			for(EbliActionSimple action:getActions())
				principal.add(action);
			menuBar_.add(principal);
		}
		return  menuBar_;
	}


	List<EbliActionSimple> getActions(){
		List<EbliActionSimple> liste=new ArrayList<EbliActionSimple>();

		EbliActionSimple action=new EbliActionSimple(EbliResource.EBLI.getString("Initialiser"),EbliResource.EBLI
				.getIcon("restore"),"Restaurer"){
      @Override
			public void actionPerformed(final ActionEvent _evt) {
				clearArrow();
			}
		};
		liste.add(action);
		action=new EbliActionSimple(EbliResource.EBLI.getString("Fl�che"),EbliResource.EBLI.getToolIcon("crystal_bu_link"),"Fl�che"){
      @Override
			public void actionPerformed(final ActionEvent _evt) {
				creationFlecheClassique();
			}
		};
		liste.add(action);
//		action=new EbliActionSimple(EbliResource.EBLI.getString("Double Fl�che"),EbliResource.EBLI.getToolIcon("crystal_bu_scrollpane_corner"),"Double Fl�che"){
//			public void actionPerformed(final ActionEvent _evt) {
//				creationDoubleFlecheClassique();
//			}
//		};
//		liste.add(action);
		return liste;
	}




}
