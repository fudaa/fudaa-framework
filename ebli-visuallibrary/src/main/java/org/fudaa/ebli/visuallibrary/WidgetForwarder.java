package org.fudaa.ebli.visuallibrary;

import org.netbeans.api.visual.widget.Widget;

public interface WidgetForwarder {
  
  
  Widget getWidgetToMove();

}
