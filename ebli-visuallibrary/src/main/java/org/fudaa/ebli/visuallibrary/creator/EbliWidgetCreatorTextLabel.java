package org.fudaa.ebli.visuallibrary.creator;

import java.util.Map;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.EbliWidgetTextLabel;

public class EbliWidgetCreatorTextLabel extends EbliWidgetCreator {

  String label_;

  public EbliWidgetCreatorTextLabel(final String g) {
    super();
    this.label_ = g;
  }

  public EbliWidgetCreatorTextLabel(EbliWidgetCreatorTextLabel toCopy) {
    super(toCopy);
    label_ = toCopy.label_;

  }

  public String getLabel() {
    return label_;
  }

  public void setLabel(final String g) {
    this.label_ = g;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    return new EbliWidgetBordureSingle(new EbliWidgetTextLabel(_scene, getLabel()));
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    return new EbliWidgetCreatorTextLabel(this);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return label_;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    if (data == null) label_ = "";
    label_ = (String) data;
    return true;
  }

}
