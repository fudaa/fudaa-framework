package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZebliCalquePersist;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.visuallibrary.*;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;
import org.fudaa.ebli.visuallibrary.persist.ManagerWidgetPersist;

public class EbliWidgetCreatorVueCalque extends EbliWidgetCreator implements EbliWidgetImageProducer {

  ZEbliCalquesPanel calque_;

  GrBoite initZoom_;

  public EbliWidgetCreatorVueCalque(final ZEbliCalquesPanel calque) {
    this(calque, null);
  }

  public EbliWidgetCreatorVueCalque() {

  }

  public EbliWidgetCreatorVueCalque(final ZEbliCalquesPanel calque, final GrBoite _initZoom) {
    super();
    this.calque_ = calque;
    initZoom_ = _initZoom;
  }

  public GrBoite getInitZoom() {
    return initZoom_;
  }

  protected EbliWidgetCreatorVueCalque(final EbliWidgetCreatorVueCalque toCopy, final ZEbliCalquesPanel calque,
                                       final GrBoite _initZoom) {
    super(toCopy);
    this.calque_ = calque;
    initZoom_ = _initZoom;
    initSize(toCopy.calque_.getVueCalque().getCalque().getSize());
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    if (calque_ == null) { return null; }
    final EbliWidgetVueCalque widgetCalque = new EbliWidgetVueCalque(_scene, calque_, initZoom_);
    calque_.getController().buildActions();
    
    final EbliWidgetWithBordure res = new EbliWidgetBordureSingle(widgetCalque);
    res.setPreferredSize(new Dimension(200, 200));
    widgetCalque.setController(new EbliWidgetControllerCalque(widgetCalque, res));
    return res;
  }

  @Override
  public EbliWidgetCreator duplicate(final EbliWidgetCreator satteliteOwnerDuplicated) {

    final Map duplicOptions = new HashMap();
    duplicOptions.put("scene", getWidget().getEbliScene());
    final EbliWidgetCreatorVueCalque creator = new EbliWidgetCreatorVueCalque(this, getCalquesPanel().duplicate(
        duplicOptions), getCalquesPanel().getVueCalque().getViewBoite());
    return creator;
  }

  public void initSize(final Dimension rec) {
    final BCalque[] tousCalques = calque_.getVueCalque().getCalque().getTousCalques();
    calque_.getVueCalque().setSize(rec.width, rec.height);
    calque_.getVueCalque().getCalque().setSize(rec.width, rec.height);
    for (int i = 0; i < tousCalques.length; i++) {
      tousCalques[i].setSize(rec.width, rec.height);
    }
  }

  public ZEbliCalquesPanel getCalquesPanel() {
    return calque_;
  }

  public void setCalque(final ZEbliCalquesPanel calque) {
    this.calque_ = calque;
  }

  @Override
  public BufferedImage produceImage(int width, int height, Map params) {
    return calque_.produceImage(width,height,params);
  }

  @Override
  public Object savePersistData(final Map parameters) {
    // -- generation d'un identifiant unique pour le repertoire du graphe --//
    // String
    final String pathUnique = ManagerWidgetPersist.generateCalquePath((String) parameters.get("pathCalques"),
        getWidget().getEbliScene().findNodeById(getWidget().getId()));
    // - sauvegarde du contenu de l eggraphe dans un repertoire a part --//

    final File createRep = new File(pathUnique);
    if (createRep.mkdir() || createRep.isDirectory()) {
      (new ZebliCalquePersist(pathUnique)).savePersitCalqueXml(getCalquesPanel(), parameters);

    }
    final String pathRelative = CtuluLibFile.getRelativeFile(new File(pathUnique), new File((String) parameters
        .get("pathLayout")), 4);
    return pathRelative;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    // comment on recupere le dossier ?
    // il faut reconstruire le PersistenceManager par reflexion et c'est lui qui produire le ZEbliCalquesPanel
    if (data == null) {
      calque_ = new ZEbliCalquesPanel(null);
    } else {
      final String pathCalque = (String) parameters.get("pathLayout") + File.separator + (String) data;

      final ZebliCalquePersist persistance = new ZebliCalquePersist(pathCalque);

      calque_ = persistance.loadPersitCalqueXml(parameters);
      if (calque_ != null && calque_.getVueCalque() != null) {
        initZoom_ = persistance.getZoom();
      }
    }
    return true;
  }

}
