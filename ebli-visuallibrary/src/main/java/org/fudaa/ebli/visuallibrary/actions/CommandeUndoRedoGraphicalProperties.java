package org.fudaa.ebli.visuallibrary.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * Command undo/redo sur toutes les proprietes graphiques des widgets.
 * 
 * @author Adrien Hadoux
 */
public class CommandeUndoRedoGraphicalProperties implements CtuluCommand {

  List<Map<String, Object>> oldGraphicalProperties;
  List<Map<String, Object>> newGraphicalProperties;

  java.util.List<EbliWidget> widgets_;

  public CommandeUndoRedoGraphicalProperties(final List<Map<String, Object>> newGraphicalProperties,
      final List<Map<String, Object>> oldGraphicalProperties, final List<EbliWidget> widgets_) {
    super();
    this.newGraphicalProperties = newGraphicalProperties;
    this.oldGraphicalProperties = oldGraphicalProperties;
    this.widgets_ = widgets_;
  }

  /**
   * constructeur sans parametre. il faudra par la suite ajouter des old et new graphical properties
   */
  public CommandeUndoRedoGraphicalProperties() {
    widgets_ = new ArrayList<EbliWidget>();
    oldGraphicalProperties = new ArrayList<Map<String, Object>>();
    newGraphicalProperties = new ArrayList<Map<String, Object>>();
  }

  public void addWidget(final EbliWidget w) {
    widgets_.add(w);
  }

  public void addOldPropertie(final Map oldProperty) {
    oldGraphicalProperties.add(oldProperty);
  }

  public void addNewPropertie(final Map newProperty) {
    newGraphicalProperties.add(newProperty);
  }

  @Override
  public void undo() {

    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).setPropGraphique(oldGraphicalProperties.get(i));

    }
    refreshScene();

  }

  @Override
  public void redo() {

    final int nb = widgets_.size();
    if (nb == 0) return;
    for (int i = 0; i < nb; i++) {
      widgets_.get(i).setPropGraphique(newGraphicalProperties.get(i));

    }
    refreshScene();
  }

  private void refreshScene() {
    EbliScene.refreshScene(widgets_.get(0).getScene());
  }
}
