package org.fudaa.ebli.visuallibrary.calque;

import com.memoire.bu.BuComboBox;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZCalqueSondeInteraction;
import org.fudaa.ebli.calque.ZCalqueSondeInterface;
import org.fudaa.ebli.calque.ZCalqueSondeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPaletteInfo.ModifyPropertyInfo;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.graphe.GrapheTreeCellRenderer;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

/**
 * Manager sp�ciale fusion. Elle contient plusieurs sonde ( de tous les calques de la fusion). Lors de sa mise a jour,
 * met a jour les sonde de tous les calques.
 * 
 * @author Adrien Hadoux
 */
public class ZCalqueSondeSynchroniserFusion implements ZCalqueSondeListener {

  final List<ZCalqueSondeInteraction> listeSondeInteractions_ = new ArrayList<ZCalqueSondeInteraction>();

  ArrayList<EbliWidgetVueCalque> listeWidgetCalque_ = new ArrayList<EbliWidgetVueCalque>();

  /**
   * Liste des panelTableModel qui contiennent les infos des calques.
   */
  ArrayList<DataSonde> listInfoData_ = new ArrayList<DataSonde>();

  /**
   * HahMap qui contient les infos mises a jour des positions des sondes.
   */

  public ZCalqueSondeSynchroniserFusion() {

  }

  int cpt = 1;

  public void addZCalqueSondeInteraction(ZCalqueSondeInteraction si, EbliWidgetVueCalque _widget) {
    // -- ajout de la sonde dans le manager --//
    listeSondeInteractions_.add(si);

    // -- ajout de la widget --//
    listeWidgetCalque_.add(_widget);

    // -- init des datas --//
    DataSonde data = new DataSonde();
    si.fillWithInfo(data);
    listInfoData_.add(data);

    // -- enregistrement aupres de la sonde du manager comme listener --//
    si.addZCalqueSondeListener(this);

  }

  /**
   * Methode a appeler a la fin de la fusion pour retirer tous les listener qui lient les sondes entre elles.
   */
  public void removeAllListenningSonde() {
    for (ZCalqueSondeInteraction sondeInteraction : listeSondeInteractions_)
      sondeInteraction.removeZCalqueSondeListener(this);
  }

  /**
   * lorsque c'est appel�, met a jour les sondes pour tous les autre zcalquesondeInteraction.
   */
  @Override
  public void notifySondeMoved(ZCalqueSondeInteraction _activator, GrPoint _e) {

    // if (structureModel_ == null)
    // structureModel_ = constructStructureModel();

    for (int i = 0; i < listeSondeInteractions_.size(); i++) {

      ZCalqueSondeInteraction sondeInteraction = listeSondeInteractions_.get(i);
      if (/* sondeInteraction != _activator && */sondeInteraction.getTarget() != null) {
        // -- on recupere le point et on met a jour les sondes --//
        final GrPoint pt = new GrPoint(_e);// new GrPoint(_e.getX(), _e.getY(),
        // 0);
        FuLog.debug("Le point du notify synchroniser est " + pt);
        // pt.autoApplique(((BCalque)
        // sondeInteraction.getTarget()).getVersReel());
        // -- on replace la sonde des autres calques --//
        ZCalqueSondeInterface objet = sondeInteraction.getTarget();
        objet.setSondeEnable(true);

        objet.changeSonde(pt, false);

        // --remplissage des infos specifiques a la sonde --//
        ZCalquePoint isoLayer = listeWidgetCalque_.get(i).getCalqueController().getVisuPanel().getIsoLayer();
        isoLayer.fillWithInterpolateInfo(listInfoData_.get(i));

        // -- mise a jour du model avec les nouvelles infos
        // structureModel_.fillWithInfos(i, listInfoData_.get(i));
      }
    }

    fillAllVariables();

    buildDialog();
    structureModel_ = constructStructureModel();
    treeTableNodes_.setTreeTableModel(structureModel_);
    treeTableNodes_.expandAll();
    structureModel_.reload();
  }

  BuComboBox comboChoixVar_ = new BuComboBox();
  ArrayList<String> listeVar_ = new ArrayList<String>();
  final static String ALLVAR = EbliResource.EBLI.getString("tout");

  /**
   * Remplit la combo avec toutes les variables.
   */
  private void fillAllVariables() {
    HashSet<String> listeVar = new HashSet<String>();
    for (DataSonde sonde : listInfoData_)
      for (String key : sonde.mapValueSonde.keySet())
        if (!key.equals("X") && !key.equals("Y") && !key.equals("El�ment englobant"))
          listeVar.add(key);
    listeVar_ = new ArrayList<String>();
    listeVar_.add(ALLVAR);
    listeVar_.addAll(listeVar);

  }

  JDialog dialog_;

  public void buildDialog() {
    if (dialog_ != null) {
      dialog_.setVisible(true);
      return;
    }

    CtuluUIDialog ui = new CtuluUIDialog(listeWidgetCalque_.get(0).getEbliScene().getView());
    final Frame f = CtuluLibSwing.getFrameAncestorHelper(ui.getParentComponent());
    dialog_ = new JDialog(f);

    dialog_.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent _e) {
        // on desactive l action de suivi sur le graphe
        dialog_ = null;
      }

      @Override
      public void windowClosing(WindowEvent _e) {
        dialog_ = null;
      }
    });

    dialog_.setModal(false);
    dialog_.pack();
    dialog_.setTitle(getTitle());
    JPanel container = new JPanel(new BorderLayout());
    container.add(buildComponent(), BorderLayout.CENTER);

    comboChoixVar_ = new BuComboBox(listeVar_.toArray(new String[listeVar_.size()]));
    comboChoixVar_.setMinimumSize(new Dimension(150, (int) comboChoixVar_.getSize().getHeight()));
    comboChoixVar_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        filtreVariables();

      }

    });
    JPanel panelRadio = new JPanel();
    panelRadio.add(new JLabel("Variable:"));
    panelRadio.add(comboChoixVar_);

    container.add(panelRadio, BorderLayout.NORTH);

    JPanel content = new JPanel(new BorderLayout());
    content.add(container, BorderLayout.CENTER);
    // content.add(new JLabel("<html> <body><b><center>" + getTitle() +
    // "</center></b> </body></html>"),BorderLayout.NORTH);
    dialog_.setContentPane(content);
    dialog_.setSize(400, 250);
    dialog_.setLocationRelativeTo(CtuluLibSwing.getFrameAncestor(ui.getParentComponent()));
    dialog_.setVisible(true);

  }

  public String getTitle() {
    return EbliResource.EBLI.getString("-- Interpolation r�sultats -- ");
  }

  JXTreeTable treeTableNodes_;

  public JComponent buildComponent() {
    JPanel container = new JPanel(new BorderLayout());
    if (structureModel_ == null)
      structureModel_ = constructStructureModel();
    treeTableNodes_ = new JXTreeTable(structureModel_);

    // treeTableNodes.setRowHeight(60);
    treeTableNodes_.getColumn(0).setMinWidth(100);
    treeTableNodes_.setTreeCellRenderer(new GrapheTreeCellRenderer());
    JPanel containerTree = new JPanel(new BorderLayout());
    containerTree.add(treeTableNodes_, BorderLayout.CENTER);
    container.add(new JScrollPane(containerTree), BorderLayout.CENTER);
    container.add(treeTableNodes_.getTableHeader(), BorderLayout.NORTH);
    return container;
  }

  TreeTableModelGraphe structureModel_;

  public TreeTableModelGraphe constructStructureModel() {
    DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode("Sonde pour la fusion des calques");

    for (int i = 0; i < listeWidgetCalque_.size(); i++) {
      EbliWidgetVueCalque widget = listeWidgetCalque_.get(i);
      JLabel label = new JLabel();
      label.setText(getTitleCalque(widget));
      label.setIcon(getIconCalque(widget));
      DefaultMutableTreeTableNode nodeCalque = new DefaultMutableTreeTableNode(label);
      root.add(nodeCalque);

      fillWithInfos(i, nodeCalque, listInfoData_.get(i));

      // -- parcours des donn�es

    }
    TreeTableModelGraphe model = new TreeTableModelGraphe();
    model.setRoot(root);

    return model;
  }

  public String getTitleCalque(EbliWidgetVueCalque _widget) {
    return ((EbliNode) _widget.getEbliScene().findObject(_widget)).getTitle();
  }

  public Icon getIconCalque(EbliWidgetVueCalque _widget) {
    return new ImageIcon(_widget.getCalqueController().getVisuPanel().produceImage(60, 30, new HashMap()));
  }

  public class DataSonde implements InfoData {
    private Map<String, Object> mapValueSonde = new HashMap<String, Object>();
    public ArrayList<String> listOrdonneetitres_ = new ArrayList<String>();
    private String valueEmpty = "N/A";

    public DataSonde() {
      // -- init de la map sonde avec les valeurs par defaut --//

      listOrdonneetitres_.add("X");
      listOrdonneetitres_.add("Y");
      listOrdonneetitres_.add("El�ment englobant");
      // listOrdonneetitres_.add("Bathym�trie");
      // listOrdonneetitres_.add("Cote eau");
      // listOrdonneetitres_.add("Hauteur d'eau");
      // listOrdonneetitres_.add("Nombre de Froude");
      // listOrdonneetitres_.add("Vitesse");
      // listOrdonneetitres_.add("Vitesse u");
      // listOrdonneetitres_.add("Vitesse v");

      for (String key : listOrdonneetitres_) {
        mapValueSonde.put(key, valueEmpty);

      }

    }

    @Override
    public boolean isTitleSet() {
      return false;
    }

    @Override
    public void put(String _key, String _value) {
      mapValueSonde.put(_key, _value);
      // if (!listOrdonneetitres_.contains(_key))
      // listOrdonneetitres_.add(_key);
    }

    @Override
    public void setTitle(String _title) {}

    @Override
    public void put(String _key, Object _value, CtuluValueEditorI _editor, TableCellRenderer _renderer,
        ModifyPropertyInfo _owner, int[] _data) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setOnlyOneGeometry(boolean _value) {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  public class SondeCouple {
    public String name;
    public int indiceData_;

    public SondeCouple(String _name, int indiceData) {
      super();
      name = _name;
      indiceData_ = indiceData;
    }

    public Object getValue() {
      return listInfoData_.get(indiceData_).mapValueSonde.get(name);
    }

  }

  public class TreeTableModelGraphe extends DefaultTreeTableModel {
    String[] titre_;

    // -- data correspondant au x donn� --//
    double[] dataY_ = new double[0];

    public TreeTableModelGraphe() {
      String[] val = { "Nom", "Valeur" };
      titre_ = val;
    }

    @Override
    public int getColumnCount() {

      return titre_.length;
    }

    @Override
    public boolean isCellEditable(Object _node, int _column) {
      return false;
    }

    @Override
    public String getColumnName(int _columnIndex) {
      return titre_[_columnIndex];
    }

    public int getRowCount() {
      return listeWidgetCalque_.size();

    }

    @Override
    public Object getValueAt(Object node, int column) {
      Object res = new DefaultMutableTreeTableNode("n/a");
      if (node instanceof DefaultMutableTreeTableNode) {
        DefaultMutableTreeTableNode defNode = (DefaultMutableTreeTableNode) node;
        if (defNode.getUserObject() instanceof JLabel) {
          if (column == 0) return defNode.getUserObject();
          else return "";
        } else if (defNode.getUserObject() instanceof SondeCouple) {
          SondeCouple couple = (SondeCouple) defNode.getUserObject();
          if (column == 0) return couple.name;
          else if (column == 1) return couple.getValue();
          else return "";
        } else return "";
      }
      return res;
    }

    public DefaultMutableTreeTableNode getTreeNodeCalque(int i) {
      return (DefaultMutableTreeTableNode) getRoot().getChildAt(i);

    }

    public void reload() {

      this.modelSupport.fireTreeStructureChanged(treeTableNodes_.getPathForRow(0));
      // treeTableNodes_.collapseAll();
      // treeTableNodes_.expandAll();
    }

    /**
     * met a jour les nodes childre ndu node calque pour le jtree.
     * 
     * @param _treenode
     */

  }

  /**
   * Remplit les infos de la structure avec les infos des sondes.
   * 
   * @param indice
   * @param treenode
   * @param data_
   */
  public void fillWithInfos(int indice, DefaultMutableTreeTableNode treenode, DataSonde data_) {
    treenode.add(new DefaultMutableTreeTableNode(new SondeCouple("X", indice)));
    treenode.add(new DefaultMutableTreeTableNode(new SondeCouple("Y", indice)));
    treenode.add(new DefaultMutableTreeTableNode(new SondeCouple("El�ment englobant", indice)));

    if (comboChoixVar_ != null && comboChoixVar_.getSelectedIndex() == 0) {
      for (Iterator<String> it = data_.mapValueSonde.keySet().iterator(); it.hasNext();) {
        String key = it.next();
        // Object value = data_.mapValueSonde.get(key);
        if (!key.equals("X") && !key.equals("Y") && !key.equals("El�ment englobant")) {
          DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(new SondeCouple(key, indice));
          treenode.add(node);
        }
      }
    } else {
      // -- ajout juste du noeud correspondant au sorter --//
      String value = (String) comboChoixVar_.getSelectedItem();
      treenode.add(new DefaultMutableTreeTableNode(new SondeCouple(value, indice)));
    }

  }

  /**
   * Est appel� lors d'un choix de la combo. Affiche uniquement les variables voulues. C'est dans la methode
   * fillWithInfos que l'on va prendre en compte le filtre de la variable.
   */
  public void filtreVariables() {
    structureModel_ = constructStructureModel();
    treeTableNodes_.setTreeTableModel(structureModel_);
    treeTableNodes_.expandAll();
    structureModel_.reload();
  }

}
