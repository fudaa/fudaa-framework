package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliWidgetController;

/**
 * Commande undo/redo sur le blocage des widgets.
 * 
 * @author Adrien Hadoux
 */

public class CommandUndoRedoBloque implements CtuluCommand {

  EbliWidgetController widget_;
  boolean bloque_ = true;

  public CommandUndoRedoBloque(final EbliWidgetController widget, final boolean bloque) {
    super();
    this.widget_ = widget;
    bloque_ = bloque;
  }

  @Override
  public void undo() {
    widget_.changePositionAndSizeBlockedState();
  }

  @Override
  public void redo() {
    widget_.changePositionAndSizeBlockedState();
  }

}