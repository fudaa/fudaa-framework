package org.fudaa.ebli.visuallibrary;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;

public class EbliWidgetImage extends EbliWidget implements ImageObserver {

  Image image;

  public EbliWidgetImage(final EbliScene scene, final Image _img) {
    super(scene, false);
    image = _img;

    // -- controller menu seulement, la bordure englobante contient le
    // controller action --//
    setController(new EbliWidgetControllerMenuOnly(this));
  }

  @Override
  protected void paintWidget() {
    final Graphics2D g = getGraphics();

    final Rectangle rec = getClientArea();

    // -- dessin de l'image --//
    g.drawImage(image, rec.x, rec.y, rec.width - 1, rec.height - 1, this);
  }

  @Override
  public boolean imageUpdate(final Image img, final int infoflags, final int x, final int y, final int width,
      final int height) {
    repaint();
    return true;
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canColorForeground() {
    return false;
  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  public boolean canFont() {
    return false;
  }

}
