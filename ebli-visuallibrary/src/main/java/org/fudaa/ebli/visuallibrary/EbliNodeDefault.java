package org.fudaa.ebli.visuallibrary;

import java.util.List;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;

public class EbliNodeDefault implements EbliNode {

  EbliWidgetCreator creator;

  String title;

  String description;

  /**
   * @return the description
   */
  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public void setDescription(final String _description) {
    this.description = _description;
    if (hasWidget()) getWidget().setToolTipText(_description);
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public EbliWidgetController getController() {
    return hasWidget() ? creator.getWidget().getController() : null;
  }

  /**
   * methode qui permet de dupliquer un node
   */
  @Override
  public List<EbliNode> duplicate() {
    return getCreator().duplicate(this, null);
  }

  @Override
  public List<EbliNode> duplicateInSameScene() {
    return getCreator().duplicateInSameScene(this, null);
  }

  @Override
  public void setTitle(final String title) {
    this.title = title;
    if (CtuluLibString.isEmpty(description)) {
      setDescription(title);
    }
  }

  @Override
  public void setCreator(final EbliWidgetCreator creator) {
    this.creator = creator;
  }

  @Override
  public EbliWidgetCreator getCreator() {
    return creator;
  }

  @Override
  public boolean hasWidget() {
    return (getCreator() != null && getCreator().isWidgetCreated());

  }

  /**
   * @return EbliWidget
   */
  @Override
  public EbliWidget getWidget() {
    return creator.getWidget();
  }

}
