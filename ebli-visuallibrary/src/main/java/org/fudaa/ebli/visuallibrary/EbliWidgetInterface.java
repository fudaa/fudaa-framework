/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

/**
 * @author deniger
 */
public interface EbliWidgetInterface<T extends EbliWidgetControllerInterface> {

  T getController();

}
