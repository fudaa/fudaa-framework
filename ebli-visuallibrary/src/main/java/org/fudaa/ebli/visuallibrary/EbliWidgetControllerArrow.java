package org.fudaa.ebli.visuallibrary;

/**
 * Controlleur r�serv� au conteneur principale des fleches. Gere une duplication, suppression et autre particuliers.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetControllerArrow extends EbliWidgetController {

  public EbliWidgetControllerArrow(EbliWidgetArrow widget) {
    super(widget, true, false);
  }

  @Override
  protected void activateResizeMoveState() {
    super.activateResizeMoveState();
    canResize_ = false;
  }

  @Override
  protected void removeActionResizeAndMove() {
    super.removeActionResizeAndMove();
    EbliWidgetArrow arrow = (EbliWidgetArrow) getWidget();
    arrow.removeActionResizeAndMove();
  }

  @Override
  protected void reInstallSelectResizeMoveActions() {
    super.reInstallSelectResizeMoveActions();
    EbliWidgetArrow arrow = (EbliWidgetArrow) getWidget();
    arrow.reInstallMoveActions();
  }

}