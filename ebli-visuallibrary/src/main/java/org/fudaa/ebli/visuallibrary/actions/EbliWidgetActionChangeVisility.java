/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.WidgetResource;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public abstract class EbliWidgetActionChangeVisility extends EbliWidgetActionFilteredAbstract {

  boolean newVisibilty = false;

  public EbliWidgetActionChangeVisility(EbliNode selectedNode, String name, Icon ic, String ac) {
    super(selectedNode, name, ic, ac);
  }

  public EbliWidgetActionChangeVisility(EbliScene scene, String name, Icon ic, String ac) {
    super(scene, name, ic, ac);
  }

  public EbliWidgetActionChangeVisility(HashSet<EbliNode> selectedNode, String name, Icon ic, String ac) {
    super(selectedNode, name, ic, ac);
  }

  public static class Hide extends EbliWidgetActionChangeVisility {
    public Hide(EbliNode selectedNode) {
      super(selectedNode, WidgetResource.getS("Masquer l'objet"), CtuluResource.CTULU.getIcon("crystal_cacher"), "HIDE");
    }

    public Hide(EbliScene scene) {
      super(scene, WidgetResource.getS("Masquer les objets sélectionnés"), CtuluResource.CTULU
          .getIcon("crystal_cacher"), "HIDE");
    }

  }

  public static class Show extends EbliWidgetActionChangeVisility {
    public Show(EbliNode selectedNode) {
      super(selectedNode, WidgetResource.getS("Afficher l'objet"), CtuluResource.CTULU.getIcon("crystal_visibilite"),
          "HIDE");
      newVisibilty = true;
    }

    public Show(EbliScene scene) {
      super(scene, WidgetResource.getS("Afficher les objets sélectionnés"), CtuluResource.CTULU
          .getIcon("crystal_visibilite"), "SHOW");
      newVisibilty = true;
    }

  }

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {
    return changeVisibility(filteredNode, newVisibilty);
  }

  public static CtuluCommand changeVisibility(EbliNode filteredNode, boolean newValue) {
    return changeVisibility(new HashSet<EbliNode>(Arrays.asList(filteredNode)), newValue);
  }

  public static CtuluCommand changeVisibility(Set<EbliNode> filteredNode, boolean newValue) {
    List<EbliNode> all = new ArrayList<EbliNode>(filteredNode);
    for (EbliNode ebliNode : filteredNode) {
      ebliNode.getController().fillWithSatelliteNodes(all);
    }
    Map<EbliWidget, Boolean> oldValues = new HashMap<EbliWidget, Boolean>();
    for (EbliNode ebliNode : all) {
      EbliWidget widget = ebliNode.getWidget().getParentBordure();
      oldValues.put(widget, ebliNode.getWidget().isVisible());
      widget.setVisible(newValue);
      widget.getEbliScene().firePropertyChange(widget, BSelecteurCheckBox.PROP_VISIBLE);
    }
    return new CommandChangeVisibility(oldValues, newValue);
  }

}
