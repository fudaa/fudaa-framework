package org.fudaa.ebli.visuallibrary;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuResource;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.actions.*;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.action.WidgetAction.Chain;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.*;

/**
 * Classe qui g�re les actions de contr�le de la widget
 *
 * @author genesis
 */
public class EbliWidgetController implements EbliWidgetControllerInterface {
  private final class PropertyChangeListenerDispatcher implements PropertyChangeListener {
    private final EbliWidget widget;

    private PropertyChangeListenerDispatcher(final EbliWidget widget) {
      this.widget = widget;
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      evt.setPropagationId(EbliWidgetController.this.getNode());
      widget.getEbliScene().firePropertyChange(evt);
    }
  }

  protected static JMenu createMenuExport(final CtuluImageProducer _imageDataProducer, final CtuluUI _ui) {
    final BuMenu menuExport = new BuMenu(CtuluLib.getS("Exporter"), "EXPORT");

    if (_imageDataProducer instanceof CtuluExportDataInterface) {

      menuExport.addMenuItem(CtuluLib.getS("Exporter donn�es"), "EXPORT_DATA", BuResource.BU.getIcon("exporter"), new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          ((CtuluExportDataInterface) _imageDataProducer).startExport(_ui);
        }
      });
    }
    menuExport.addMenuItem(CtuluLib.getS("Exporter image"), CtuluLibImage.SNAPSHOT_COMMAND, BuResource.BU.getIcon("photographie"),
        new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _e) {
            CtuluImageExport.exportImageFor(_ui, _imageDataProducer);
          }
        }).setToolTipText(CtuluLib.getS("Cr�er une image � partir de la fen�tre active"));
    menuExport.addMenuItem(CtuluLib.getS("Copier l'image dans le presse-papier"), CtuluLibImage.SNAPSHOT_CLIPBOARD_COMMAND,
        CtuluResource.CTULU.getIcon("copie-image"), new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _e) {
            CtuluImageExport.exportImageInClipboard(_imageDataProducer, _ui);
          }
        }).setToolTipText(CtuluLib.getS("Placer l'image de la fen�tre active dans le presse-papier"));
    menuExport.setIcon(null);
    return menuExport;
  }

  /**
   * action de la widget recuperable et eventuellement supprimable
   */
  WidgetAction actionMove_;
  WidgetAction actionResize_;
  WidgetAction actionSelect_;
  public AbstractButton boutonProportion = null;
  PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
  protected boolean canDuplicate_ = true;
  protected boolean canMove_ = true;
  protected boolean canResize_ = true;
  protected EbliWidgetActionDelete deleteAction;
  private boolean isBlocked = false;
  boolean isEditable;
  boolean isMenuDisplayed = true;
  private boolean isProportional = false;
  protected EbliNode node_;
  private JPopupMenu popupMenu_;
  protected PopupMenuProvider providerPopup_ = null;
  protected EbliWidget widget_;
  public static String BLOCK_PROPERTY = "blocked";

  public EbliWidgetController(final EbliWidget widget_) {
    this(widget_, true, true, true);
  }

  public EbliWidgetController(final EbliWidget _widget, final boolean _canMove, final boolean _canResize) {
    this(_widget, _canMove, _canResize, true);
  }

  public void addPropertyChangeListener(final String property, final PropertyChangeListener listener) {
    changeSupport.addPropertyChangeListener(property, listener);
  }

  public void removePropertyChangeListener(final String property, final PropertyChangeListener listener) {
    changeSupport.removePropertyChangeListener(property, listener);
  }

  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    changeSupport.addPropertyChangeListener(listener);
  }

  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    changeSupport.removePropertyChangeListener(listener);
  }

  public EbliWidgetController(final EbliWidget widget_, final boolean canMove, final boolean canResize, final boolean canDuplicate) {

    this.widget_ = widget_;
    canDuplicate_ = canDuplicate;
    canMove_ = canMove;
    canResize_ = canResize;
    changeSupport.addPropertyChangeListener(new PropertyChangeListenerDispatcher(widget_));
  }

  protected final void actionSupprimer(final CtuluCommandContainer cmd_) {
    cmd_.addCmd(deleteWidget());
  }

  public void addFonctionsSpecific(final ArrayList<EbliActionSimple> _listeActions) {

  }

  protected void buildPopupMenu(final JPopupMenu _menu) {
    constructPopupMenuBase(_menu);
  }

  public void constructMenuBloquer(final JPopupMenu _popup, final CtuluCommandContainer cmd_) {
    final EbliWidgetActionAbstract actionBloque = new EbliWidgetActionBlock(widget_);
    _popup.add(actionBloque.buildMenuItem(EbliComponentFactory.INSTANCE));
  }

  public void constructMenuDeplacerDansArbre(final JPopupMenu menu) {

    // EbliActionSimple action=null;

    menu.add(new EbliActionSimple(EbliResource.EBLI.getString("En premier"), EbliResource.EBLI.getIcon("enpremier"), "ENPREMIER") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        widget_.getEbliScene().bringToFirst(widget_);
      }
    });
    menu.add(new EbliActionSimple(EbliResource.EBLI.getString("Monter"), EbliResource.EBLI.getIcon("monter"), "MONTER") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        widget_.getEbliScene().bringUp(widget_);
      }
    });
    menu.add(new EbliActionSimple(EbliResource.EBLI.getString("Descendre"), EbliResource.EBLI.getIcon("descendre"), "DESCENTRE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        widget_.getEbliScene().bringDown(widget_);
      }
    });
    menu.add(new EbliActionSimple(EbliResource.EBLI.getString("En dernier"), EbliResource.EBLI.getIcon("endernier"), "ENPREMIER") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        widget_.getEbliScene().bringToLast(widget_);
      }
    });
  }

  protected void constructMenuDupliquer(final JPopupMenu _popup, final CtuluCommandContainer cmd_) {
    _popup.add(new EbliWidgetActionDuplicate(node_));
  }

  protected final void constructMenuMasquer(final JPopupMenu _popup, final CtuluCommandContainer cmd_) {
    _popup.add(new EbliWidgetActionChangeVisility.Hide(getNode()));
  }

  public void constructMenuResizeProportional(final JPopupMenu _popup, final CtuluCommandContainer cmd_) {
    if (!isProportional()) {
      final EbliWidgetActionAbstract actionBloque = new EbliWidgetActionRatio.Proportional(node_);
      boutonProportion = actionBloque.buildMenuItem(EbliComponentFactory.INSTANCE);
    } else {
      final EbliWidgetActionAbstract actionBloque = new EbliWidgetActionRatio.Classical(node_);
      boutonProportion = actionBloque.buildMenuItem(EbliComponentFactory.INSTANCE);
    }
    _popup.add(boutonProportion);
  }

  protected void constructMenuSupprimer(final JPopupMenu _popup, final CtuluCommandContainer cmd_) {
    final JMenuItem menuItem = _popup.add(EbliResource.EBLI.getString("Supprimer la frame"));
    menuItem.setIcon(CtuluResource.CTULU.getIcon("crystal_non"));
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        cmd_.addCmd(deleteWidget());
      }
    });
  }

  /**
   * Methode de construction des menus de base
   *
   * @param _popup
   */
  public void constructPopupMenuBase(final JPopupMenu _popup) {

    // -- reference vers le gestionnaire de commandeundo/redo --//
    final CtuluCommandContainer cmd_ = getCmdManager();

    constructPopupMenuBaseGraphique(_popup);
    _popup.addSeparator();
    constructMenuMasquer(_popup, cmd_);
    constructMenuSupprimer(_popup, cmd_);
    _popup.addSeparator();
    constructMenuBloquer(_popup, cmd_);
    constructMenuResizeProportional(_popup, cmd_);
    _popup.addSeparator();
    constructMenuDeplacerDansArbre(_popup);
    if (isDuplicatable()) {
      _popup.addSeparator();
      constructMenuDupliquer(_popup, cmd_);
    }
  }

  /**
   * @param _popup
   */
  private void constructPopupMenuBaseGraphique(final JPopupMenu _popup) {

    final JMenuItem itemConfigGraphique = _popup.add(EbliResource.EBLI.getString("Configuration graphique"));
    itemConfigGraphique.setIcon(CtuluResource.CTULU.getIcon("crystal_configurer"));
    // BuResource.BU.getIcon("configurer")
    itemConfigGraphique.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        EbliWidget found = null;// widget_;

        if (widget_.getParentWidget() != null && widget_.getParentWidget() instanceof EbliWidgetBordureSingle) {
          found = (EbliWidget) widget_.getParentWidget();
        } else {
          found = widget_;
        }
        EbliWidgetActionConfigure.configure(found);
      }
    });
  }

  /**
   * methode qui cree les actions par defaut pour la widget.
   */
  public final void installDefaultAction() {

    installSelectResizeMoveActions();
    // to avoid having to times the same action:
    widget_.getActions().removeAction(widget_.getEbliScene().createWidgetHoverAction());
    widget_.getActions().addAction(widget_.getEbliScene().createWidgetHoverAction());
  }

  protected void reInstallSelectResizeMoveActions() {
    final Chain actions = getChainForMoveResize();
    actions.removeAction(actionSelect_);
    actions.removeAction(actionResize_);
    actions.removeAction(actionMove_);
    installSelectResizeMoveActions();
  }

  private void installSelectResizeMoveActions() {
    createActions();
    final Chain actions = getChainForMoveResize();
    List<WidgetAction> alreadySet = actions.getActions();
    if (!alreadySet.contains(actionSelect_)) {
      actions.addAction(actionSelect_);
    }

    if (canResize_ && !alreadySet.contains(actionResize_)) {
      actions.addAction(actionResize_);
    }

    if (canMove_ && !alreadySet.contains(actionMove_)) {
      actions.addAction(actionMove_);
    }
  }

  private Chain getChainForMoveResize() {
    return widget_.getParentBordure().getActions();
  }

  private void createActions() {
    if (actionSelect_ == null) {
      actionSelect_ = widget_.getEbliScene().createSelectAction();
    }
    if (actionResize_ == null) {
      actionResize_ = widget_.getEbliScene().getController().getResizeAction();
    }
    if (actionMove_ == null) {
      actionMove_ = widget_.getEbliScene().getController().getMoveAction();
    }
  }

  void createPopup() {
    if (popupMenu_ == null) {
      popupMenu_ = new JPopupMenu();
      buildPopupMenu(popupMenu_);
    }
    // on met � jour l'�tat des menus si possible.
    final int nb = popupMenu_.getComponentCount();
    for (int i = 0; i < nb; i++) {
      final Component component = popupMenu_.getComponent(i);
      if ((component instanceof AbstractButton) && (((AbstractButton) component).getAction() instanceof EbliActionSimple)) {
        ((EbliActionSimple) ((AbstractButton) component).getAction()).updateStateBeforeShow();
      }
    }
  }

  public CtuluCommand deleteWidget() {
    return EbliWidgetActionDelete.deleteNodes(Arrays.asList(getNode()));
  }

  public void changePositionAndSizeBlockedState() {
    setPositionAndSizeBlocked(!isBlocked);
  }

  public void setPositionAndSizeBlocked(final boolean blocked) {
    setPositionAndSizeBlocked(blocked, true);
  }

  public void setPositionAndSizeBlocked(final boolean blocked, boolean registerCmd) {
    if (blocked == isBlocked) {
      return;
    }
    if (blocked) {
      removeActionResizeAndMove();
    } else {
      activeActionsResizeAndMove();
    }
    isBlocked = blocked;
    // -- rafraichissement de la scene --//
    widget_.getEbliScene().refresh();

    // -- enregistrement de la commande undoRedo --//
    if (registerCmd && widget_.getEbliScene().getCmdMng() != null) {
      widget_.getEbliScene().getCmdMng().addCmd(new CommandUndoRedoBloque(this, blocked));
    }
    changeSupport.firePropertyChange(BLOCK_PROPERTY, isBlocked, !isBlocked);
  }

  /**
   * @param toFill liste rempli avec les EbliNode satellites.
   * @return toFill
   */
  public Collection<EbliNode> fillWithSatelliteNodes(final Collection<EbliNode> toFill) {
    final Set<EbliWidget> satellites = widget_.getSatellites();
    if (satellites == null) {
      return toFill;
    }
    for (final EbliWidget ebliWidget : satellites) {
      final EbliWidgetController controller = ebliWidget.getController();
      if (controller.node_ != null) {
        toFill.add(controller.node_);
      }
      controller.fillWithSatelliteNodes(toFill);
    }
    return toFill;
  }

  public boolean fonctionsAlreadySpecified() {
    return false;
  }

  public CtuluCommandManager getCmdManager() {
    return getWidget().getEbliScene().getCmdMng();
  }

  @Override
  public JMenuBar getMenubarComponent() {
    return null;
  }

  /**
   * @return the node_
   */
  public EbliNode getNode() {
    return node_;
  }

  @Override
  public JComponent getOverviewComponent() {

    return null;
  }

  JMenuItem ungroup;
  JMenuItem restoreAllVueCalque;
  private WidgetAction popupAction;

  public WidgetAction getPopupAction() {
    return popupAction;
  }

  public JPopupMenu getPopup() {
    createPopup();

    EbliWidget group = widget_.getGroup();
    final boolean isFusionCalque = group != null && group.getController() instanceof EbliWidgetControllerForGroup && ((EbliWidgetControllerForGroup) group
        .getController()).isFusionCalque;
    if (restoreAllVueCalque == null && isFusionCalque) {
      final EbliWidgetActionRestoreVueCalqueForFusion restoreVueCalque = new EbliWidgetActionRestoreVueCalqueForFusion(group.getEbliScene(), (EbliNode) group.getEbliScene().findObject(group));
      restoreAllVueCalque = popupMenu_.add(restoreVueCalque);
    }
    if (restoreAllVueCalque != null && !isFusionCalque) {
      popupMenu_.remove(restoreAllVueCalque);
      restoreAllVueCalque = null;
    }
    if (group != null && ungroup == null) {
      ungroup = popupMenu_.add(new EbliWidgetActionUngroup(group.getEbliScene(), (EbliNode) group.getEbliScene().findObject(group)));
    } else if (group == null && ungroup != null) {
      popupMenu_.remove(ungroup);
      ungroup = null;
    }
    return popupMenu_;
  }

  public Collection<EbliNode> getSatelliteNodes() {
    final Set<EbliWidget> satellites = widget_.getSatellites();
    if (satellites == null) {
      return Collections.emptyList();
    }
    return fillWithSatelliteNodes(new ArrayList<EbliNode>(satellites.size()));
  }

  @Override
  public JToolBar getToolbarComponent() {
    return null;
  }

  @Override
  public JComponent getTracableComponent() {

    return new JLabel();
  }

  public EbliWidget getWidget() {
    return widget_;
  }

  /**
   * Methode generique qui fait apparaitre un menuItem propre au composant et offre les op�rations de base on ouvre le
   * menu via le clic droit ou le raccourcis shift+F10
   */
  protected final void initMenu() {
    if (!isMenuDisplayed()) {
      return;
    }
    providerPopup_ = new PopupMenuProvider() {
      @Override
      public JPopupMenu getPopupMenu(final Widget widget, final Point localLocation) {
        return getPopup();
      }
    };
    if (popupAction == null) {
      popupAction = ActionFactory.createPopupMenuAction(providerPopup_);
    }
    if (!widget_.getActions().getActions().contains(popupAction)) {
      widget_.getActions().addAction(popupAction);
    }
  }

  @Override
  public boolean isDataExportable() {
    return false;
  }

  public boolean isDuplicatable() {
    return !widget_.isSatellite() && canDuplicate_;
  }

  @Override
  public boolean isEditable() {
    return isEditable;
  }

  ;

  /**
   * @return true par defaut: le popup menu sera cree
   */
  protected final boolean isMenuDisplayed() {
    return isMenuDisplayed;
  }

  public boolean isMovable() {
    return canMove_;
  }

  /**
   * @return the isProportional
   */
  public final boolean isProportional() {
    return isProportional;
  }

  public boolean isResizable() {
    return canResize_;
  }

  public void removeActionResize() {
    getChainForMoveResize().removeAction(actionResize_);
    canResize_ = false;
  }

  /**
   * Methode appelee pour bloquer une widget: l'empecher de se deplacer.
   */
  protected void removeActionResizeAndMove() {
    removeActionResize();
    getChainForMoveResize().removeAction(actionMove_);
    canMove_ = false;
  }

  public void installActionResize() {
    canResize_ = true;
    reInstallSelectResizeMoveActions();
  }

  /**
   * fonction inverse de la precedente.
   */
  public void activeActionsResizeAndMove() {
    activateResizeMoveState();
    reInstallSelectResizeMoveActions();
    if (isBlocked) {
      isBlocked = false;
      changeSupport.firePropertyChange(BLOCK_PROPERTY, isBlocked, !isBlocked);
    }
  }

  protected void activateResizeMoveState() {
    canResize_ = true;
    canMove_ = true;
  }

  /**
   * @param _isEditable the isEditable to set
   */
  public void setEditable(final boolean _isEditable) {
    isEditable = _isEditable;
  }

  /**
   * @param isMenuDisplayed the isMenuDisplayed to set
   */
  public void setMenuDisplayed(final boolean isMenuDisplayed) {
    this.isMenuDisplayed = isMenuDisplayed;
  }

  /**
   * Appele lors de l'ajout a la scene.
   *
   * @param node the node_ to set
   */
  protected void setNode(final EbliNode node) {

    node_ = node;
    if (node != null && isMenuDisplayed && providerPopup_ == null) {
      initMenu();
    }
    installDefaultAction();
  }

  /**
   * @param _isProportional the isProportional to set
   */
  public final void setProportional(final boolean _isProportional) {
    isProportional = _isProportional;
  }

  public void setWidget(final EbliWidget _widget) {
    this.widget_ = _widget;
  }

  /**
   * @return the isBlocked
   */
  public boolean isBlocked() {
    return isBlocked;
  }
}
