package org.fudaa.ebli.visuallibrary.actions;

import java.util.ArrayList;
import java.util.Iterator;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * commande qui gere le undo/redo en cas de masquage des widgets
 * 
 * @author Adrien Hadoux
 */
public class CommandMasquer implements CtuluCommand {

  ArrayList<EbliWidget> widget_;
  private boolean visible_;

  public CommandMasquer(final EbliWidget widget) {

    widget_ = new ArrayList<EbliWidget>();
    widget_.add(widget);

  }

  public CommandMasquer(final ArrayList<EbliWidget> widget) {
    this(widget, false);

  }

  public CommandMasquer(final ArrayList<EbliWidget> widget, final boolean visible) {
    super();
    this.widget_ = widget;
    visible_ = visible;
  }

  @Override
  public void undo() {

    for (final Iterator<EbliWidget> it = widget_.iterator(); it.hasNext();) {
      it.next().setVisible(!visible_);
    }

    widget_.get(0).getEbliScene().refresh();
  }

  @Override
  public void redo() {
    for (final Iterator<EbliWidget> it = widget_.iterator(); it.hasNext();) {
      it.next().setVisible(visible_);
    }
    widget_.get(0).getEbliScene().refresh();
  }

}
