/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import javax.swing.JComponent;
import org.netbeans.api.visual.widget.Widget;

/**
 * Un Listener pour le mode edition
 * 
 * @author deniger
 */
public interface EbliWidgetEditModeListener {

  void editStart(Widget w, JComponent _editor);

  void editStop(Widget w, JComponent _editor);

}
