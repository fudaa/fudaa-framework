/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * Une commande qui lance un refresh apr�s les undo/redo
 * @author deniger
 */
public class CommandCompositeScene extends CtuluCommandComposite {
  private final EbliScene scene;

  public CommandCompositeScene(EbliScene scene) {
    super();
    this.scene = scene;
  }

  @Override
  protected void actionToDoAfterUndoOrRedo() {
    scene.refresh();
  }

  @Override
  protected boolean isActionToDoAfterUndoOrRedo() {
    return true;
  }

}
