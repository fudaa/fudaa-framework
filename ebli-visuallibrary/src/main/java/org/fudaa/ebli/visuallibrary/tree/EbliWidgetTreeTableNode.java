package org.fudaa.ebli.visuallibrary.tree;

import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionChangeVisility;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.netbeans.api.visual.model.ObjectScene;
import org.netbeans.api.visual.widget.Widget;

public class EbliWidgetTreeTableNode extends DefaultMutableTreeTableNode {

  public static EbliWidgetTreeTableNode build(final EbliScene _sc) {
    final Widget w = _sc.getVisu();
    final EbliWidgetTreeTableNode root = new EbliWidgetTreeTableNode(w);
    addChilds(_sc, w, root, null);
    return root;
  }

  static void fillRoot(final EbliWidgetTreeTableNode _root, EbliWidgetJXTreeTableModel support) {
    final Widget w = (Widget) _root.getUserObject();
    addChilds((ObjectScene) w.getScene(), w, _root, null);
  }

  /**
   * Ajoute les childs depuis la liste de la scene
   * 
   * @param _sc
   * @param w
   * @param parent
   * @param parentNode
   */
  static void addChilds(final ObjectScene _sc, final Widget w, final EbliWidgetTreeTableNode parent, final EbliNode parentNode) {
    List<Widget> children = w.getChildren();
    for (int i = children.size() - 1; i >= 0; i--) {
      Widget child = children.get(i);
      final EbliNode n = (EbliNode) _sc.findObject(child);
      
      if (n != null && n != parentNode&& _sc.findWidget(n)==child) {//the last test is used to be sure that it's a main widget.
        final EbliWidgetTreeTableNode treeNode = new EbliWidgetTreeTableNode(n);
        parent.add(treeNode);
        addChilds(_sc, child, treeNode, n);
      }
    }
  }

  boolean root;

  EbliWidgetTreeTableNode(final Widget w) {
    super(w);
    root = true;

  }

  EbliWidgetTreeTableNode(final EbliNode _userObject) {
    super(_userObject, true);

  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public Object getValueAt(final int _column) {
    if (root)
      return "root";
    if (_column == 0) {
      return getUserObject();
    }
    if (_column == 1) {
      return ((EbliNode) getUserObject()).getTitle();
    }
    if (_column == 2)
      return Boolean.valueOf(((EbliNode) getUserObject()).getWidget().isVisible());

    EbliNode node = (EbliNode) getUserObject();
    EbliWidget widget = node.getWidget();
    return Boolean.valueOf(widget.getController().isBlocked());

  }

  @Override
  public boolean isEditable(final int _column) {
    return _column > 0;
  }

  @Override
  public void setValueAt(final Object _value, final int _column) {
    if (!root) {
      EbliNode selectedNode = (EbliNode) getUserObject();
      if (_column == 1) {
        selectedNode.setTitle((String) _value);
        selectedNode.getWidget().getEbliScene().firePropertyChange(selectedNode.getWidget(), EbliWidget.TITLE);
      } else if (_column == 2) {
        final CtuluCommandContainer cmd_ = selectedNode.getWidget().getEbliScene().getCmdMng();
        cmd_.addCmd(EbliWidgetActionChangeVisility.changeVisibility(selectedNode, ((Boolean) _value).booleanValue()));
        selectedNode.getWidget().getEbliScene().refresh();
      } else if (_column == 3) {
        selectedNode.getWidget().getController().changePositionAndSizeBlockedState();
      }
    }
  }

}
