package org.fudaa.ebli.visuallibrary.tree;

import com.memoire.bu.BuLabel;
import java.awt.Color;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.tree.TreeCellRenderer;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

/**
 * Treecell renderer pour l arbre des calques de visualllibrary. gere uniquement l affichage des icones optimises.
 * 
 * @author genesis
 */
public class EbliJXTreeTableCellRenderer implements TreeCellRenderer {

  // -- panel renderer --//
  private final BuLabel returnLabel = new BuLabel();

  Color selectionBorderColor, selectionForeground, selectionBackground, textForeground, textBackground;

  public Icon iconLeafEnabled = CtuluResource.CTULU.getIcon("crystal_visibilite");
  public Icon iconLeafDisabled = CtuluResource.CTULU.getIcon("crystal_cacher");
  public Icon iconSubDirectorie = CtuluResource.CTULU.getIcon("reouvrir");
  public Icon iconRoot = CtuluResource.CTULU.getIcon("crystal_maison");
  public Icon iconBloque = CtuluResource.CTULU.getIcon("crystal_tuer");
  public Icon iconDeBloque = CtuluResource.CTULU.getIcon("crystal_valider");

  public EbliJXTreeTableCellRenderer() {
    returnLabel.setOpaque(false);
    returnLabel.setHorizontalAlignment(SwingConstants.LEFT);
  }

  @Override
  public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean selected,
      final boolean expanded, final boolean leaf, final int row, final boolean hasFocus) {

    // cas particulier renderer: on met un checkbox que si on est feuille et pqs root (= cqs ou lqyer depourvu d objet
    // graphique)
    if (leaf && ((DefaultMutableTreeTableNode) value).getParent() != null) {

      // -- recuperation du node --//
      final Object userObject = ((DefaultMutableTreeTableNode) value).getUserObject();

      EbliNode node = null;
      if (userObject instanceof EbliNode) {
        node = (EbliNode) (((DefaultMutableTreeTableNode) value).getUserObject());

        if (node.getWidget().isVisible()) returnLabel.setIcon(iconLeafEnabled);// =new BuLabel(this.iconLeafEnabled);
        else returnLabel.setIcon(iconLeafDisabled);// returnLabel=new BuLabel(this.iconLeafDisabled);

        returnLabel.setEnabled(node.getWidget().isVisible());
      }

    } /*
       * else if (!leaf && !((DefaultMutableTreeTableNode) value).isRoot()) {
       * 
       * returnLabel=new BuLabel(this.iconSubDirectorie); returnValue =
       * nonLeafRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
       * 
       * }
       */else {
      // returnLabel=new BuLabel(((DefaultMutableTreeTableNode) value).toString(),this.iconRoot);
      returnLabel.setIcon(this.iconRoot);
      returnLabel.setEnabled(true);
    }

    return returnLabel;
  }

}
