package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public interface ShapeCreator {

  Shape createShapeFor(Rectangle2D.Float rec, Map options, float largeurTrait);
}
