package org.fudaa.ebli.visuallibrary.calque;

import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.LabelWidget.VerticalAlignment;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.layout.FlowLayout;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class EbliWidgetFlechePlage extends EbliWidget implements PropertyChangeListener {
  private static class FlecheWidget extends Widget {
    private final ZCalqueFleche support;

    /**
     */
    public FlecheWidget(Scene scene, ZCalqueFleche support) {
      super(scene);
      this.support = support;
    }

    @Override
    protected Rectangle calculateClientArea() {
      Rectangle res = super.calculateClientArea();
      Rectangle parent = getParentWidget().getClientArea();
      res.height = 10;
      if (parent == null) {
        res.width = 40;
      } else {
        res.width = parent.width;
      }
      return res;
    }

    @Override
    protected void paintWidget() {
      final Rectangle rec = getClientArea();
      final Graphics2D g2d = getGraphics();
      int height = (int) rec.getHeight();
      int width = (int) rec.getWidth();
      support.getFlecheLegend().getFleche().setIconHeight(height);
      support.getFlecheLegend().getFleche().setIconWidth(width);
      support.getFlecheLegend().getFleche().paintIcon(null, g2d, rec.x, rec.y);
    }
  }

  private final ZCalqueFleche support;
  LabelWidget legend;
  FlecheWidget flecheWidget;

  /**
   * @param scene
   */
  public EbliWidgetFlechePlage(EbliScene scene, ZCalqueFleche fleche) {
    super(scene);
    this.support = fleche;
    this.support.getFlecheLegend();
    setBorder(BorderFactory.createEmptyBorder(4, 1, 3, 1));
    setLayout(new FlowLayout(true, SerialAlignment.CENTER, 2));
    flecheWidget = new FlecheWidget(scene, fleche);
    legend = new LabelWidget(scene);
    legend.setVerticalAlignment(VerticalAlignment.CENTER);
    legend.setAlignment(Alignment.RIGHT);
    legend.setEnabled(true);
    legend.setLabel(support.getFlecheLegend().getLbInfo().getText());
    support.getFlecheLegend().addPropertyChangeListener(this);
    addChild(flecheWidget);
    addChild(legend);
    revalidate();
    setEnabled(true);
  }

  @Override
  public void propertyChange(PropertyChangeEvent arg0) {
    boolean legendVisible = support.getFlecheLegend().isLegendVisible();
    setVisible(legendVisible);
    legend.setVisible(legendVisible);
    flecheWidget.setVisible(legendVisible);
    flecheWidget.repaint();
    flecheWidget.revalidate();
    legend.setLabel(support.getFlecheLegend().getLbInfo().getText());
    legend.repaint();
    repaintAll(true);
    revalidate();
    getEbliScene().refresh();
  }

  public void removeListeners() {
    support.getFlecheLegend().removePropertyChangeListener(this);
  }
}
