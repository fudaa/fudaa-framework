package org.fudaa.ebli.visuallibrary.graphe;

import java.awt.Point;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidgetHtml;
import org.netbeans.api.visual.action.TextFieldInplaceEditor;
import org.netbeans.api.visual.widget.Widget;

/**
 * Legende Widget qui contient le titre de la ligne de la l�gende courbe
 * 
 * @author genesis
 */
public class EbliWidgetTitle extends EbliWidgetHtml implements TextFieldInplaceEditor {

  EGCourbe cb_;

  /**
   * @param _scene
   */
  public EbliWidgetTitle(final EbliScene _scene, final EGCourbe _cb, final Point preferedLocation) {
    super(_scene, "");
    setCourbe(_cb);
  }

  public void majLabel() {
    if (cb_ != null) {
      setText(cb_.getTitle());
      setToolTipText(cb_.getTitle());
      repaint();
    }
  }

  /**
   * @return the cb_
   */
  public EGCourbe getCourbe() {
    return cb_;
  }

  /**
   * @param _cb the cb_ to set
   */
  public void setCourbe(final EGCourbe _cb) {
    this.cb_ = _cb;
    majLabel();
  }

  @Override
  public String getText(final Widget widget) {
    return ((EbliWidgetTitle) widget).getText();
  }

  @Override
  public boolean isEnabled(final Widget widget) {
    return true;
  }

  @Override
  public void setText(final Widget widget, final String text) {
    final String texte = formatte(text);
    EbliWidgetTitle ebliWidgetHtml = (EbliWidgetTitle) widget;
    ebliWidgetHtml.cb_.setTitle(texte);
    ebliWidgetHtml.majLabel();
  }

  /**
   * methode qui reformatte le texte avec la meme tailel que le libell� pr�c�dent pour bien afficher les modifs
   * 
   * @param texte
   * @return
   */
  public String formatte(final String texte) {
    if (texte.length() < cb_.getTitle().length()) {

      return texte;
    } else return texte.substring(0, cb_.getTitle().length());

  }

}