package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuComboBox;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * Classe qui gere un petit panel permettant de changer la police. Style logiciel windows.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetActionFont implements ActionListener {

  CtuluCommandContainer cmd_;

  /**
   * liste de tous les fontes
   */
  BuComboBox fonts;

  BuComboBox sizeFonts;

  public BuComboBox getSizeFonts() {
    return sizeFonts;
  }

  /**
   * scene.
   */
  EbliScene scene_;

  public EbliWidgetActionFont(final EbliScene _scene) {

    scene_ = _scene;
    cmd_ = _scene.getCmdMng();

    // -- creation de la combobox --//
    final GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();

    fonts = new BuComboBox(gEnv.getAvailableFontFamilyNames());
    fonts.addActionListener(this);

    sizeFonts = new BuComboBox();
    int cpt = 8;
    while (cpt < 100)
      sizeFonts.addItem("" + cpt++);
    sizeFonts.addActionListener(this);
  }

  public BuComboBox getFonts() {
    return fonts;
  }

  public void setFonts(final BuComboBox fonts) {
    this.fonts = fonts;
  }

  /**
   * appelee dans le cas d event du combo.
   */
  @Override
  public void actionPerformed(final ActionEvent e) {

    if (e.getSource().equals(fonts)) {

      // -- choix de la fontpar l utilisateur --//

      // -- recuperation de la fonte choisie --//
      final Font newFont = new Font((String) fonts.getSelectedItem(), Font.BOLD, Integer.parseInt((String) sizeFonts
          .getSelectedItem()));

      // on met a jour les couleurs pour toutes les widgets selectionnees --//

      // -- recupere les anciennes graphical properties --//
      final List<Map<String, Object>> oldGraphicalProperties = new ArrayList<Map<String, Object>>();

      // -- liste des nouvelles graphical properties --//
      final List<Map<String, Object>> newGraphicalProperties = new ArrayList<Map<String, Object>>();

      // -- liste des widget selectionnees --//
      final java.util.List<EbliWidget> listeWidget = new ArrayList<EbliWidget>();

      // -- liste des nodes selectionnes --//
      final Set<EbliNode> listeNode = (Set<EbliNode>) scene_.getSelectedObjects();// scene_

      for (final Iterator<EbliNode> it = listeNode.iterator(); it.hasNext();) {

        final EbliNode currentNode = it.next();

        if (currentNode.hasWidget()) {

          final EbliWidget widget = currentNode.getWidget();

          // -- test pour savoir si le widget gere le colorcontour --//
          if (widget.getFormeFont() != null) {

            // -- ajout pour les undo redo
            listeWidget.add(widget);
            oldGraphicalProperties.add(widget.duplicateGraphicalProperties());

            // -- mise a jour de la nouvelle font --//
            widget.setFormeFont(newFont);

            // -- ajout de la nouvelle graphicalProperty --//
            newGraphicalProperties.add(widget.getPropGraphique());

          }

        }
      }
      // -- rafraichissement de la scene --//
      scene_.refresh();

      if (cmd_ != null) {
        cmd_
            .addCmd(new CommandeUndoRedoGraphicalProperties(newGraphicalProperties, oldGraphicalProperties, listeWidget));

      }

    }

  }

}
