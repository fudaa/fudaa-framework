package org.fudaa.ebli.visuallibrary.creator;


import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.util.Map;

/**
 * Ctreator qui se charge de g�n�rer une fleche simple.
 * @author Adrien Hadoux
 *
 */
public class ShapeCreatorFlecheSimple implements ShapeCreator {

  @Override
  public Shape createShapeFor(final Rectangle2D.Float rec, final Map options, final float largeurTrait) {
    final Float ratioHeadW = (Float) options.get("arrow.ratioHeadW");
//    final Float ratioBaseH = (Float) options.get("arrow.ratioBaseH");
//    final float rb = ratioBaseH == null ? 0.3f : ratioBaseH.floatValue();
    final float rh = ratioHeadW == null ? 0.3f : ratioHeadW.floatValue();
    final float middleH = rec.y + rec.height / 2f;
//    final float hauteurBase = rec.height * rb;
    final float largeurHead = rec.width * rh;
    final GeneralPath path = new GeneralPath();
    path.moveTo(rec.x, middleH );
    path.lineTo(rec.x + rec.width - largeurTrait, middleH);//-
    path.lineTo(rec.x + rec.width - largeurHead, rec.y + largeurTrait / 2);//  \
    path.lineTo(rec.x + rec.width - largeurTrait , middleH);//-
    path.lineTo(rec.x + rec.width - largeurHead, rec.y + rec.height - largeurTrait / 2);// /
    path.lineTo(rec.x + rec.width - largeurTrait , middleH);//-
    path.lineTo(rec.x, middleH );
    //path.lineTo(rec.x + rec.width - largeurHead, middleH + hauteurBase / 2);
    //path.lineTo(rec.x, middleH + hauteurBase / 2);
    path.closePath();
    return path;
  }
}
