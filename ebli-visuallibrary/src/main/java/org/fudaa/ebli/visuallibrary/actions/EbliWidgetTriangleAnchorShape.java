package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ebli.ressource.EbliResource;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.modules.visual.anchor.TriangleAnchorShape;

public class EbliWidgetTriangleAnchorShape extends TriangleAnchorShape {

  public static enum Type {
    NONE(EbliResource.EBLI.getString("Aucun"), false, false, false),
    FILLED(EbliResource.EBLI.getString("Plein"), true, false, false),
    HOLLOW(EbliResource.EBLI.getString("Contour"), false, false, true),
    OUT(EbliResource.EBLI.getString("Invers�"), true, true, false);

    private boolean filled;
    private boolean output;
    private boolean hollow;
    private final String name;

    private Type(String name, boolean filled, boolean output, boolean hollow) {
      this.name = name;
      this.filled = filled;
      this.output = output;
      this.hollow = hollow;
    }

    public String getName() {
      return name;
    }

    public boolean isFilled() {
      return filled;
    }

    public boolean isOutput() {
      return output;
    }

    public boolean isHollow() {
      return hollow;
    }

  }

  /**
   * The empty anchor shape.
   */
  public static final EbliWidgetTriangleAnchorShape DEFAULT_NONE = new EbliWidgetTriangleAnchorShape(Type.NONE, 0);

  /**
   * The hollow-triangle anchor shape.
   */
  public static final AnchorShape TRIANGLE_HOLLOW = new EbliWidgetTriangleAnchorShape(Type.HOLLOW, 12);

  /**
   * The filled-triangle anchor shape.
   */
  public static final AnchorShape TRIANGLE_FILLED = new EbliWidgetTriangleAnchorShape(Type.FILLED, 12);

  /**
   * The output-triangle anchor shape.
   */
  public static final AnchorShape TRIANGLE_OUT = new EbliWidgetTriangleAnchorShape(Type.OUT, 12);
  private static final Map<Type, AnchorShape> DEFAULT_ANCHOR = new HashMap<Type, AnchorShape>();
  static {
    DEFAULT_ANCHOR.put(Type.NONE, DEFAULT_NONE);
    DEFAULT_ANCHOR.put(Type.HOLLOW, TRIANGLE_HOLLOW);
    DEFAULT_ANCHOR.put(Type.OUT, TRIANGLE_OUT);
    DEFAULT_ANCHOR.put(Type.FILLED, TRIANGLE_FILLED);
  }

  public static AnchorShape getDefault(Type type) {
    return DEFAULT_ANCHOR.get(type);
  }
  
  

  private final int size;
  private final Type type;

  public EbliWidgetTriangleAnchorShape(Type type, int size) {
    super(size, type.isFilled(), type.isOutput(), type.isHollow(), size - 1);
    this.size = size;
    this.type = type;
  }

  @Override
  public int getRadius() {
    return super.getRadius();
  }

  @Override
  public double getCutDistance() {
    return super.getCutDistance();
  }

  @Override
  public void paint(Graphics2D graphics, boolean source) {
    if (Type.NONE.equals(type)) {
      return;
    }
    super.paint(graphics, source);
  }

  public Type getType() {
    return type;
  }

  public int getSize() {
    return size;
  }

}
