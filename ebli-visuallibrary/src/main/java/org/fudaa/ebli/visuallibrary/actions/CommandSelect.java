/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import java.util.Set;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.visuallibrary.EbliNode;

/**
 * Command de selection/deselection.
 * 
 * @author deniger
 */
public class CommandSelect implements CtuluCommand {

  final Set<EbliNode> oldSelected;
  final Set<EbliNode> newSelected;

  public CommandSelect(Set<EbliNode> oldSelected) {
    this(oldSelected, null);
  }

  public CommandSelect(Set<EbliNode> oldSelected, Set<EbliNode> newSelected) {
    super();
    this.oldSelected = oldSelected;
    this.newSelected = newSelected;
  }

  @Override
  public void redo() {
    select(newSelected);
  }

  @Override
  public void undo() {
    select(oldSelected);

  }

  private void select(Set<EbliNode> selected) {
    if (CtuluLibArray.isNotEmpty(selected)) {
      EbliNode first = selected.iterator().next();
      first.getWidget().getEbliScene().setSelectedObjects(selected);
    }
  }

}
