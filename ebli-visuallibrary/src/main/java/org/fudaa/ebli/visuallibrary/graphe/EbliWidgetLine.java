package org.fudaa.ebli.visuallibrary.graphe;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * Legende Widget qui contient le trac� de la ligne de la l�gende courbe
 * 
 * @author genesis
 */
public class EbliWidgetLine extends EbliWidget {

  EGCourbe cb_;

  /**
   * @param _scene
   */
  public EbliWidgetLine(final EbliScene _scene, final EGCourbe _cb, final Point preferedLocation) {
    super(_scene, true);
    cb_ = _cb;

    setEnabled(false);

  }

  /**
     *
     */
  @Override
  protected void paintWidget() {
    final Graphics2D g = getGraphics();
    final Rectangle rec = getClientArea();

    g.translate(rec.x, rec.y);
    // la ligne
    final TraceLigne l = new TraceLigne(cb_.getLigneModel());
    l.dessineTrait(g, 0, rec.height / 2, rec.width, rec.height / 2);
    // l'icone
    final TraceIconModel icm = new TraceIconModel(cb_.getIconModel());
    icm.setTaille(Math.min(rec.width / 2, rec.height));// a voir
    final TraceIcon ic = new TraceIcon(icm);
    ic.paintIconCentre(g, rec.width / 2, rec.height / 2);
    g.translate(-rec.x, -rec.y);
  }

}
