package org.fudaa.ebli.visuallibrary.calque;

import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.ebli.calque.BCalqueLegendePanel;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.LabelWidget.VerticalAlignment;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.layout.FlowLayout;

/**
 * Creation de la classe widget specifique au calque. Permet de gerer le resize via le changement de font
 */
public class EbliWidgetCalqueLegendeItems extends EbliWidget {



  List<EbliWidgetPlage> plagesWithTreeParts;
  List<EbliWidgetPlage> plagesWithSingleParts;

  public EbliWidgetCalqueLegendeItems(final EbliScene _scene) {
    super(_scene);

    setCheckClipping(true);
    setEnabled(true);
  }


  /**
   * @return the plages
   */
  public List<EbliWidgetPlage> getPlagesWithTreeParts() {
    return plagesWithTreeParts;
  }

  @Override
  public Rectangle calculateClientArea() {
    Rectangle res = super.calculateClientArea();
    updatePlagesWidth();
    return res;
  }

  public void updatePlagesWidth() {
    int maxSingle = 0;
    if (plagesWithSingleParts != null) {
      for (EbliWidgetPlage widget : plagesWithSingleParts) {
        maxSingle = Math.max(maxSingle, widget.getSingleWidth());
      }
    }
    if (plagesWithTreeParts != null) {
      int maxLeft = 0;
      int maxMiddle = 0;
      int maxRight = 0;
      for (EbliWidgetPlage widget : plagesWithTreeParts) {
        maxLeft = Math.max(maxLeft, widget.calculateNormalWidth(SwingConstants.LEFT));
        maxMiddle = Math.max(maxMiddle, widget.calculateNormalWidth(SwingConstants.CENTER));
        maxRight = Math.max(maxRight, widget.calculateNormalWidth(SwingConstants.RIGHT));
      }
      int totalWidth = maxLeft + maxMiddle + maxRight + EbliWidgetPlage.getGapBetweenParts();
      if (totalWidth > maxSingle) {
        maxSingle = totalWidth;
      } else {
        maxLeft += maxSingle - totalWidth;
      }
      for (EbliWidgetPlage widget : plagesWithTreeParts) {
        widget.setWidth(SwingConstants.LEFT, maxLeft);
        widget.setWidth(SwingConstants.CENTER, maxMiddle);
        widget.setWidth(SwingConstants.RIGHT, maxRight);
        widget.revalidate();

      }
    }
    if (plagesWithSingleParts != null) {
      for (EbliWidgetPlage widget : plagesWithSingleParts) {
        widget.setSingleLabelWidth(maxSingle);
        widget.revalidate();
        widget.repaint();
      }
    }
  }


  protected void updateFont(final Font ft) {
    final List<Widget> listePlage = getChildren();
    for (final Iterator<Widget> it = listePlage.iterator(); it.hasNext();) {
      final Widget widget = it.next();
      // -- test si il s agit d un widget plage --//
      if (widget instanceof EbliWidgetPlage) {
        final EbliWidgetPlage widgetPlage = (EbliWidgetPlage) widget;
        widgetPlage.updateFont(ft, false);
      }
    }

    updateLegendView();

  }

  public void updateLegendView() {
    updatePlagesWidth();
    revalidate();
  }


  /**
   * Mananger statique qui se charge de creer �a widget associee au calque.
   */
  public void recreateLegendeWidget(BCalqueLegendePanel legendePanel_) {
    final EbliScene _scene = getEbliScene();
    // --creation de la widget conteneur --//
    BPalettePlageInterface listeplages = null;
    removeChildren();
    setLayout(new FlowLayout(true, SerialAlignment.CENTER, 5));
    setBorder(BorderFactory.createEmptyBorder(5));
    plagesWithTreeParts = null;
    plagesWithSingleParts = null;
    if (legendePanel_ != null) {
      final Component[] listeC = legendePanel_.getComponents();

      for (int i = 0; i < listeC.length; i++) {
        final Component c = listeC[i];
        if (c instanceof BPalettePlageLegende) {
          final BPalettePlageLegende pal = (BPalettePlageLegende) c;
          // --recuperation de la liste des plages --//
          listeplages = pal.getModel();
          if (listeplages != null) {
            setLayout(new FlowLayout(true, SerialAlignment.LEFT_TOP, 2));
            int nbPlages = listeplages.getNbPlages();
            if (nbPlages != 0) {
              // pour l'alignement des labels
              plagesWithTreeParts = new ArrayList<EbliWidgetPlage>(nbPlages);
              plagesWithSingleParts = new ArrayList<EbliWidgetPlage>(nbPlages);
              for (int j = nbPlages - 1; j >= 0; j--) {

                final BPlageInterface plage = listeplages.getPlageInterface(j);
                // -- ajout d'une widget plage --//
                final EbliWidgetPlage lw = new EbliWidgetPlage(_scene, plage);
                lw.updateFont(pal.getFont(), false);

                lw.setUseBorder(false);
                lw.setEnabled(true);
                if (lw.isThreePart()) {
                  plagesWithTreeParts.add(lw);
                } else {
                  plagesWithSingleParts.add(lw);
                }
                addChild(lw);

              }
            } else {
              // liste des plages est vide il s agit d une autre legende
              final Component[] lcomp = pal.getComponents();
              for (int j = 0; j < lcomp.length; j++) {
                final Component co = lcomp[j];

                if (co instanceof JPanel) {
                  final JPanel panel = (JPanel) co;

                  for (int k = 0; k < panel.getComponents().length; k++) {

                    if (panel.getComponents()[k] instanceof JLabel) {
                      LabelWidget titre = new LabelWidget(_scene);
                      titre.setAlignment(Alignment.CENTER);
                      titre.setVerticalAlignment(VerticalAlignment.CENTER);
                      titre.setLabel(((JLabel) panel.getComponents()[k]).getText());
                      titre.setEnabled(true);
                      addChild(titre);
                    } else if (panel.getComponents()[k] instanceof JList) {
                      final JList jlst = (JList) panel.getComponents()[k];
                      for (int g = 0; g < jlst.getModel().getSize(); g++) {
                        LabelWidget titre = new LabelWidget(_scene);
                        titre.setAlignment(Alignment.CENTER);
                        titre.setVerticalAlignment(VerticalAlignment.CENTER);
                        titre.setLabel((String) jlst.getModel().getElementAt(g));
                        titre.setEnabled(true);
                        addChild(titre);
                      }

                    }

                  }

                }

              }

            }

            setPreferredBounds(null);
            revalidate();
          }// fin du if listeplage==null
        }
      }
      revalidate();
      // -- prendre en compte le resize automatique --//
      getEbliScene().refresh();
    } else {
      FuLog.warning("createLegendeWidget retourne widget null");
    }
  }

}