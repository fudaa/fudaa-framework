package org.fudaa.ebli.visuallibrary.actions;

import java.util.ArrayList;
import java.util.Iterator;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * Command undo/redo pour le duplicate de widget.
 * 
 * @author Adrien Hadoux
 */
public class CommandeDuplicate implements CtuluCommand {

  ArrayList<EbliNode> nodeDuplique;

  EbliScene destScene_;

  public CommandeDuplicate(final ArrayList<EbliNode> nodeDuplique, final EbliScene destinationScene) {
    super();
    this.nodeDuplique = nodeDuplique;
    this.destScene_ = destinationScene;

  }

  @Override
  public void redo() {
    for (final Iterator<EbliNode> it = nodeDuplique.iterator(); it.hasNext();) {
      final EbliNode node = it.next();
      destScene_.addNode(node);
    }
    destScene_.refresh();
  }

  @Override
  public void undo() {
    EbliWidgetActionDelete.deleteNodes(nodeDuplique);
    destScene_.refresh();
  }

}
