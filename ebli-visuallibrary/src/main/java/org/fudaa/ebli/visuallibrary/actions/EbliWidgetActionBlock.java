package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuIcon;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetController;

/**
 * Action qui empeche les widgets de se resize et d'etre deplacee
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetActionBlock extends EbliWidgetActionAbstract {

  private static final long serialVersionUID = 1L;

  EbliWidget widget;

  public EbliWidgetActionBlock(final EbliWidget _widget) {
    super(_widget.getEbliScene(), "", null, "");
    widget = _widget;
    widget.getController().addPropertyChangeListener(EbliWidgetController.BLOCK_PROPERTY, new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateActionState();
      }
    });
    updateActionState();
  }

  private void updateActionState() {
    setDefaultToolTip(getBlockTooltip());
    setIcon(getBlockIcon());
    putValue(Action.NAME, getBlockName());
  }

  private boolean isBlocked() {
    return widget.getController().isBlocked();
  }

  protected String getBlockName() {
    if (isBlocked()) { return EbliResource.EBLI.getString("Debloquer la frame"); }
    return EbliResource.EBLI.getString("Bloquer la frame");
  }

  protected String getBlockTooltip() {
    if (isBlocked()) { return EbliResource.EBLI.getString("D�bloquer la taille et la position de la frame"); }
    return EbliResource.EBLI.getString("Bloquer la taille et la position de la frame");
  }

  protected BuIcon getBlockIcon() {
    if (isBlocked()) { return EbliResource.EBLI.getIcon("unlock_16.png"); }
    return EbliResource.EBLI.getIcon("lock_16");
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    widget.getController().changePositionAndSizeBlockedState();
  }

}
