package org.fudaa.ebli.visuallibrary.graphe;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.ebli.visuallibrary.EbliWidgetWithBordure;

/**
 * Controller specifique au graphe.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetGrapheModelListener implements EGGrapheModelListener {
  EbliWidgetWithBordure w;

  public EbliWidgetGrapheModelListener(final EbliWidgetWithBordure w) {
    super();
    this.w = w;
  }

  @Override
  public void structureChanged() {
    repaintScene();
    EbliWidgetGraphe g = (EbliWidgetGraphe) w.getIntern();
    if(g.getGraphe().getModel().isUpdating()) return;
    EGCourbe[] courbes = g.getGraphe().getModel().getMainModel().getCourbes();
    // il suffit de faire le test ici:
    if (CtuluLibArray.isEmpty(courbes)) {
      w.getEbliScene().getController().fireEditStop(w, null);
      w.getController().getCmdManager().addCmd(w.getController().deleteWidget());
    }

  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean restore) {
    repaintScene();

  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
    repaintScene();

  }

  private void repaintScene() {
    w.repaintAll(true);
    w.getEbliScene().refresh();
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
    repaintScene();

  }

  @Override
  public void axeAspectChanged(final EGAxe _c) {

  }

}
