package org.fudaa.ebli.visuallibrary.behavior;

import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.actions.CommandRetaillage;
import org.netbeans.api.visual.action.ResizeControlPointResolver;
import org.netbeans.api.visual.action.ResizeProvider;
import org.netbeans.api.visual.action.ResizeStrategy;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;

public class EbliResizeAction extends WidgetAction.LockedAdapter {

  private ResizeStrategy strategy;
  private ResizeControlPointResolver resolver;
  private ResizeProvider provider;

  private Widget resizingWidget = null;
  private ResizeProvider.ControlPoint controlPoint;
  private Rectangle originalSceneRectangle = null;
  private Insets insets;
  private Point dragSceneLocation = null;
  private Rectangle initBounds;

  public EbliResizeAction(ResizeStrategy strategy, ResizeControlPointResolver resolver, ResizeProvider provider) {
    this.strategy = strategy;
    this.resolver = resolver;
    this.provider = provider;
  }

  @Override
  protected boolean isLocked() {
    return resizingWidget != null;
  }

  @Override
  public State mousePressed(Widget widget, WidgetMouseEvent event) {
    Widget currentWidget = widget;
    if (widget instanceof EbliWidget) {
      EbliWidget group = ((EbliWidget) widget).getGroup();
      if (group != null) {
        currentWidget = group;
      }

    }
    initBounds = currentWidget.getPreferredBounds();
    if (isLocked()) return State.createLocked(currentWidget, this);
    if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 1) {
      insets = currentWidget.getBorder().getInsets();
      controlPoint = resolver.resolveControlPoint(widget, event.getPoint());
      if (controlPoint != null) {
        resizingWidget = currentWidget;
        originalSceneRectangle = null;
        if (currentWidget.isPreferredBoundsSet()) originalSceneRectangle = currentWidget.getPreferredBounds();
        if (originalSceneRectangle == null) originalSceneRectangle = currentWidget.getBounds();
        if (originalSceneRectangle == null) originalSceneRectangle = currentWidget.getPreferredBounds();
        dragSceneLocation = currentWidget.convertLocalToScene(event.getPoint());
        provider.resizingStarted(currentWidget);
        return State.createLocked(currentWidget, this);
      }
    }
    return State.REJECTED;
  }

  @Override
  public State mouseReleased(Widget widget, WidgetMouseEvent event) {
    boolean state = resize(widget, event.getPoint());
    if (state) {
      resizingWidget = null;
      provider.resizingFinished(widget);
      CtuluCommandManager cmdMng = ((EbliWidget) widget).getEbliScene().getCmdMng();
      if (cmdMng != null) {
        cmdMng.addCmd(new CommandRetaillage(Arrays.asList((EbliWidget) widget), Arrays.asList(initBounds), Arrays
            .asList(widget.getPreferredBounds())));
      }
    }
    initBounds = null;
    return state ? State.CONSUMED : State.REJECTED;
  }

  @Override
  public State mouseDragged(Widget widget, WidgetMouseEvent event) {
    return resize(widget, event.getPoint()) ? State.createLocked(widget, this) : State.REJECTED;
  }

  private boolean resize(Widget widget, Point newLocation) {
    if (resizingWidget != widget) return false;

    newLocation = widget.convertLocalToScene(newLocation);
    int dx = newLocation.x - dragSceneLocation.x;
    int dy = newLocation.y - dragSceneLocation.y;
    int minx = insets.left + insets.right;
    int miny = insets.top + insets.bottom;

    Rectangle rectangle = new Rectangle(originalSceneRectangle);
    switch (controlPoint) {
    case BOTTOM_CENTER:
      resizeToBottom(miny, rectangle, dy);
      break;
    case BOTTOM_LEFT:
      resizeToLeft(minx, rectangle, dx);
      resizeToBottom(miny, rectangle, dy);
      break;
    case BOTTOM_RIGHT:
      resizeToRight(minx, rectangle, dx);
      resizeToBottom(miny, rectangle, dy);
      break;
    case CENTER_LEFT:
      resizeToLeft(minx, rectangle, dx);
      break;
    case CENTER_RIGHT:
      resizeToRight(minx, rectangle, dx);
      break;
    case TOP_CENTER:
      resizeToTop(miny, rectangle, dy);
      break;
    case TOP_LEFT:
      resizeToLeft(minx, rectangle, dx);
      resizeToTop(miny, rectangle, dy);
      break;
    case TOP_RIGHT:
      resizeToRight(minx, rectangle, dx);
      resizeToTop(miny, rectangle, dy);
      break;
    }

    widget.setPreferredBounds(strategy.boundsSuggested(widget, originalSceneRectangle, rectangle, controlPoint));
    return true;
  }

  private static void resizeToTop(int miny, Rectangle rectangle, int dy) {
    if (rectangle.height - dy < miny) dy = rectangle.height - miny;
    rectangle.y += dy;
    rectangle.height -= dy;
  }

  private static void resizeToBottom(int miny, Rectangle rectangle, int dy) {
    if (rectangle.height + dy < miny) dy = miny - rectangle.height;
    rectangle.height += dy;
  }

  private static void resizeToLeft(int minx, Rectangle rectangle, int dx) {
    if (rectangle.width - dx < minx) dx = rectangle.width - minx;
    rectangle.x += dx;
    rectangle.width -= dx;
  }

  private static void resizeToRight(int minx, Rectangle rectangle, int dx) {
    if (rectangle.width + dx < minx) dx = minx - rectangle.width;
    rectangle.width += dx;
  }

}
