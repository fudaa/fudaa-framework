package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.Map;

public class ShapeCreatorRectangle implements ShapeCreator {

  @Override
  public Shape createShapeFor(final Rectangle2D.Float rec, final Map options, final float largeurTrait) {
    final Float arcWidth = (Float) options.get("arc.width");
    final Float arcHeight = (Float) options.get("arc.height");
    final float arcw = arcWidth == null ? 0 : arcWidth.floatValue();
    final float arch = arcHeight == null ? 0 : arcHeight.floatValue();
    return new RoundRectangle2D.Float(rec.x, rec.y, rec.width, rec.height, arcw, arch);
  }
}
