package org.fudaa.ebli.visuallibrary.behavior;

import java.awt.BasicStroke;
import java.awt.Color;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.AlignWithMoveDecorator;
import org.netbeans.api.visual.action.AlignWithWidgetCollector;
import org.netbeans.api.visual.action.MoveProvider;
import org.netbeans.api.visual.action.MoveStrategy;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.modules.visual.action.MoveAction;
import org.netbeans.modules.visual.action.ResizeCornersControlPointResolver;
import org.netbeans.modules.visual.action.SingleLayerAlignWithWidgetCollector;

/**
 * Customisation apr rapport a l'usine ActionFactory de visulalibrary de creation des actions des widgets. Rajout des
 * resize proportionnels
 * 
 * @author Adrien Hadoux
 */

public class EbliWidgetActionFactory {

  // private static final MoveProvider MOVE_PROVIDER_SATTELITTE = new MoveProvider() {
  // public void movementStarted(Widget widget) {}
  //
  // public void movementFinished(Widget widget) {}
  //
  // public Point getOriginalLocation(Widget widget) {
  // return widget.getPreferredLocation();
  // }
  //
  // public void setNewLocation(Widget widget, Point location) {
  // widget.setPreferredLocation(location);
  // // -- cas particulier pour les widgets sattelites --//
  // EbliWidget ebliwidget = (EbliWidget) widget;
  // if (ebliwidget.hasSattelite()) {
  // // -- on deplace ses widget sattelite a la location relative a leur
  // // location d'origine --//
  // for (EbliWidget satt : ebliwidget.getSatellites()) {
  // satt.setPreferredLocation(location);
  // }
  // }
  //
  // }
  // };

  public static ResizeCornersControlPointResolver CONTROL_POINT_RESOLVER = new ResizeCornersControlPointResolver();

  private static final BasicStroke STROKE = new BasicStroke(1.0f, BasicStroke.JOIN_BEVEL, BasicStroke.CAP_BUTT, 5.0f,
      new float[] { 6.0f, 3.0f }, 0.0f);

  private static final AlignWithMoveDecorator ALIGN_WITH_MOVE_DECORATOR_DEFAULT = new AlignWithMoveDecorator() {
    @Override
    public ConnectionWidget createLineWidget(Scene scene) {
      ConnectionWidget widget = new ConnectionWidget(scene);
      widget.setStroke(STROKE);
      widget.setForeground(Color.BLUE);
      return widget;
    }
  };

  public static WidgetAction createAlignWithProportionnalResizeAction(LayerWidget collectionLayer,
      LayerWidget interractionLayer, AlignWithMoveDecorator decorator, boolean outerBounds) {
    assert collectionLayer != null;
    return createAlignWithProportionnalResizeAction(new SingleLayerAlignWithWidgetCollector(collectionLayer,
        outerBounds), interractionLayer, decorator != null ? decorator : ALIGN_WITH_MOVE_DECORATOR_DEFAULT, outerBounds);
  }

  public static WidgetAction createAlignWithProportionnalResizeAction(AlignWithWidgetCollector collector,
      LayerWidget interractionLayer, AlignWithMoveDecorator decorator, boolean outerBounds) {
    assert collector != null && interractionLayer != null && decorator != null;
    AlignWithResizeProportionalStrategy sp = new AlignWithResizeProportionalStrategy(collector, interractionLayer,
        decorator, outerBounds);
    return new EbliResizeAction(sp, CONTROL_POINT_RESOLVER, sp);
  }

  public static WidgetAction createMoveAction(MoveStrategy strategy, MoveProvider provider) {
    return new MoveAction(strategy != null ? strategy : ActionFactory.createFreeMoveStrategy(), provider);
  }

}
