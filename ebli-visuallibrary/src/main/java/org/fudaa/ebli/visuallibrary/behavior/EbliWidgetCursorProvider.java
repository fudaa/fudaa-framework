/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.behavior;

import java.awt.Cursor;
import java.awt.Point;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.action.ResizeProvider.ControlPoint;
import org.netbeans.api.visual.widget.Widget;

/**
 * @author deniger
 */
public class EbliWidgetCursorProvider {

  private static ControlPoint getControlPoint(final Widget _widget, final Point _localLocation) {
    return EbliWidgetActionFactory.CONTROL_POINT_RESOLVER.resolveControlPoint(_widget, _localLocation);
  }

  public static Cursor getCursor(final EbliWidget _widget, final Point _localLocation) {
    if (!_widget.isEnabled() || _widget.getController() == null) return Cursor.getDefaultCursor();
    if (_widget.getController().isResizable()) {
      final ControlPoint controlPoint = getControlPoint(_widget, _localLocation);
      if (controlPoint != null) {
        int type = Cursor.SE_RESIZE_CURSOR;
        if (controlPoint == ControlPoint.TOP_CENTER) type = Cursor.N_RESIZE_CURSOR;
        else if (controlPoint == ControlPoint.TOP_LEFT) type = Cursor.NW_RESIZE_CURSOR;
        else if (controlPoint == ControlPoint.TOP_RIGHT) type = Cursor.NE_RESIZE_CURSOR;
        else if (controlPoint == ControlPoint.CENTER_LEFT) type = Cursor.W_RESIZE_CURSOR;
        else if (controlPoint == ControlPoint.CENTER_RIGHT) type = Cursor.E_RESIZE_CURSOR;
        else if (controlPoint == ControlPoint.BOTTOM_CENTER) type = Cursor.S_RESIZE_CURSOR;
        else if (controlPoint == ControlPoint.BOTTOM_LEFT) type = Cursor.SW_RESIZE_CURSOR;
        return Cursor.getPredefinedCursor(type);
      }
    }
    if (_widget.getController().isEditable() && _widget.getClientArea().contains(_localLocation)) { return Cursor
        .getPredefinedCursor(Cursor.TEXT_CURSOR); }
    if (_widget.getController().isMovable()) return Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
    return Cursor.getDefaultCursor();

  }

}
