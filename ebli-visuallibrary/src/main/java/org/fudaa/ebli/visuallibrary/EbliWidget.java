package org.fudaa.ebli.visuallibrary;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.actions.CommandeUndoRedoGraphicalProperties;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetActionChangeVisility;
import org.fudaa.ebli.visuallibrary.actions.WidgetConfigure;
import org.fudaa.ebli.visuallibrary.animation.EbliWidgetAnimatedItem;
import org.fudaa.ebli.visuallibrary.behavior.EbliWidgetCursorProvider;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorArrow;
import org.netbeans.api.visual.border.Border;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.*;

/**
 * Widget version EBLI
 *
 * @author Adrien Hadoux
 */
public class EbliWidget extends Widget implements BSelecteurTargetInterface, EbliWidgetInterface<EbliWidgetController>, WidgetForwarder {
  public static final String TITLE = "title";
  public final static String COLORCONTOUR = "ColorContour";
  public final static String COLORFOND = "colorFond";
  public final static String FONT = "font";
  /**
   * Liste des clef utilisees pour les composants graphiques
   */
  public final static String LINEMODEL = "lineModel";
  public final static String ROTATION = "rotation";
  public final static String TRANSPARENCE = "Transpa";
  public final static String VISIBLE = "visible";
  protected EbliWidgetController controller_;
  /**
   * L'identifiant unique du widget
   */
  private String id_;
  boolean isGroup;
  boolean isInEditMode_ = false;
  /**
   * Map contenant tous les objets graphiques de la widget generique. key: lineModel => le lignemodel du ebliWidget. key: color => couleur de fond.
   */
  protected Map<String, Object> propGraphique;
  private EbliScene scene_;
  private boolean useBorder_ = true;

  /**
   * @param scene
   */
  public EbliWidget(final EbliScene scene) {
    super(scene);
    id_ = scene.generateId();
    setScene(scene);

    // -- remplisage de la map de propriete grahiques --//
    propGraphique = new HashMap<String, Object>();

    // -- ajout du traceligneModel utilise par les filles--//
    propGraphique.put(LINEMODEL, new TraceLigneModel());

    // -- ajout de la couleur de fond par default blanc--//
    propGraphique.put(COLORFOND, Color.WHITE);

    // -- ajout de la couleur de contour par default black--//
    propGraphique.put(COLORCONTOUR, Color.BLACK);

    // -- ajout de la font par default--//
    propGraphique.put(FONT, new Font("Helvetica.Italic", Font.PLAIN, 12));

    // -- ajout de l angle de rotation par default PI --//
    propGraphique.put(ROTATION, Math.PI);

    // -- ajout de la trnaparence a false --//
    propGraphique.put(EbliWidget.TRANSPARENCE, false);
  }

  /**
   * Utilise par la plupart des composant sauf calque et graphe qui necessitent un controller particulier.
   *
   * @param scene
   * @param controllerDefaut
   */
  public EbliWidget(final EbliScene scene, final boolean controllerDefaut) {
    this(scene);

    // -- creation du controller par defaut --//
    if (controllerDefaut) {
      controller_ = new EbliWidgetController(this);
    }
  }

  @Override
  public final void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    // voir EGObject
  }

  public boolean getTransparent() {
    return (Boolean) propGraphique.get(TRANSPARENCE);
  }

  public void setTransparent(boolean transparent) {
    propGraphique.put(TRANSPARENCE, Boolean.valueOf(transparent));
  }

  public boolean canColorBackground() {
    return true && !getTransparent();
  }

  public boolean canColorForeground() {
    return true;
  }

  public boolean canFont() {
    return true;
  }

  public boolean canRotate() {
    return true;
  }

  public boolean canTraceLigneModel() {
    return true;
  }

  private boolean changeTitle(final String _title, final boolean _cmd) {
    final String old = getTitle();
    if (_title != null && !_title.equals(old)) {
      final EbliNode n = (EbliNode) getEbliScene().findObject(this);
      if (n == null) {
        return false;
      }
      n.setTitle(_title);
      getEbliScene().firePropertyChange(EbliWidget.this, TITLE);
      if (_cmd) {

        getEbliScene().getCmdMng().addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            changeTitle(_title, false);
          }

          @Override
          public void undo() {
            changeTitle(old, false);
          }
        });
      }
      postNamedChanged(_title);
      return true;
    }
    return false;
  }

  protected void postNamedChanged(String _newTitle) {
  }

  /**
   * reschedule le repaint pour ce widget, le widget interne si present et des satellites si demande Attention, dans la librairie visuallibrary, il
   * est bien pr�cise que les methodes repaint ne font que dire que le widget devra etre repaint au prochain tour ( appele par EbliScene.refresh).
   *
   * @param includeSatellite true si on repaint les satellites
   */
  public void repaintAll(boolean includeSatellite) {
    repaint();
    if (getIntern() != null) {
      getIntern().repaint();
    }
    if (satellites != null) {
      for (EbliWidget w : satellites) {
        w.repaintAll(includeSatellite);
      }
    }
  }




  /**
   * reschedule le revalidate de ce widget, le widget interne si present et des satellites si demande Attention, dans la librairie visuallibrary, il
   * est bien pr�cise que les methodes revalidate ne font que dire que le widget devra etre revalidatee au prochain tour ( appele par
   * EbliScene.refresh).
   *
   * @param includeSatellite true si on revalidate les satellites
   */
  public void revalidateAll(boolean includeSatellite) {
    revalidate();
    if (getIntern() != null) {
      getIntern().revalidate();
    }
    if (satellites != null) {
      for (EbliWidget w : satellites) {
        w.revalidateAll(includeSatellite);
      }
    }
  }

  public Map<String, Object> duplicateGraphicalProperties() {
    final Map<String, Object> mapDupliquee = new HashMap<String, Object>();
    mapDupliquee.put(LINEMODEL, new TraceLigneModel(getTraceLigneModel()));
    mapDupliquee.put(COLORCONTOUR, getColorContour());
    mapDupliquee.put(COLORFOND, getColorFond());
    mapDupliquee.put(ROTATION, getRotation());
    mapDupliquee.put(FONT, getFormeFont());
    mapDupliquee.put(TRANSPARENCE, getTransparent());
    return mapDupliquee;
  }

  protected void editingStart(JComponent _editor) {
    isInEditMode_ = true;
    getEbliScene().getController().fireEditStart(this, _editor);
  }

  protected void editingStop(JComponent _editor) {
    isInEditMode_ = false;
    getEbliScene().getController().fireEditStop(this, _editor);
  }

  /**
   * @return une interface non null si la widget peut etre animee
   */
  public EbliAnimatedInterface getAnimatedInterface() {
    return null;
  }

  public List<EbliWidgetAnimatedItem> getAnimatedItems() {
    return null;
  }

  public Color getColorContour() {
    return (Color) propGraphique.get(COLORCONTOUR);
  }

  public Color getColorFond() {
    return (Color) propGraphique.get(COLORFOND);
  }

  /**
   * retourne la liste des interfaces configurees.
   */
  public BConfigurableInterface[] getConfigureInterfaces(final boolean lineModel_, final boolean colorsContours_, final boolean colorFonds_,
                                                         final boolean rotations_, final boolean police_, final boolean transparence) {
    return new BConfigurableInterface[]{new BConfigurableComposite(getSingleConfigureInterface(lineModel_, colorsContours_, colorFonds_,
        rotations_, police_, transparence), EbliLib.getS("Affichage"))};
  }

  @Override
  public EbliWidgetController getController() {
    return controller_;
  }

  @Override
  protected Cursor getCursorAt(final Point _localLocation) {
    return EbliWidgetCursorProvider.getCursor(this, _localLocation);
  }

  public EbliScene getEbliScene() {
    return scene_;
  }

  public Font getFormeFont() {
    return (Font) getProperty(FONT);
  }

  /**
   * @return le PreferredLocation dans les coordonn�es de la scene
   */
  public Point getPreferredLocationOnScreen() {
    Widget parentWidget = getParentWidget();
    return parentWidget == null ? getPreferredLocation() : parentWidget.convertLocalToScene(getPreferredLocation());
  }

  /**
   * @param p le PreferredLocation dans les coordonn�es de la scene
   */
  public void setPreferredLocationFromScreen(Point p) {
    Widget parentWidget = getParentWidget();
    setPreferredLocation(parentWidget == null ? p : parentWidget.convertLocalToScene(p));
  }

  @Override
  public Widget getWidgetToMove() {
    return getGroup();
  }

  public EbliWidget getGroup() {
    Widget parent = getParentWidget();
    while (parent != getScene() && parent != null) {
      if (parent instanceof EbliWidget && ((EbliWidget) parent).isGroup) {
        return ((EbliWidget) parent);
      }
      parent = parent.getParentWidget();
    }
    return null;
  }

  /**
   * Retourne systematiquement l id de la widget qui contient les infos. La bordure redirige son id, il n'est pas necessaire de retnir l id pour cette
   * widget sans importance.
   *
   * @return the idst
   */
  public String getId() {
    return id_;
  }

  /**
   * Renvoie l'id du group ou l'id NOGROUP si la widget ne fais pas partie d un groupe.
   */
  public String getIdOfGroup() {
    if (isGrouped()) {
      return ((EbliWidget) this.getParentBordure().getParentWidget()).getId();
    } else {
      return EbliWidgetGroup.NOGROUP;
    }
  }

  public EbliWidget getIntern() {
    return this;
  }

  // useless
  @Override
  public Object getMin(final String _key) {
    return null;
  }

  // useless
  @Override
  public Object getMoy(final String _key) {
    return null;
  }

  /**
   * @return le parent direct si de type bordure, sinon this.
   */
  public EbliWidget getParentBordure() {
    final Widget parent = getParentWidget();
    if (parent instanceof EbliWidgetBordureSingle) {
      return (EbliWidget) parent;
    }
    return this;
  }

  /**
   * Retourne l objet correspondant a la clef: retourne l objet graphique correspondant depuis la map d objets graphiques de la widget.
   */
  @Override
  public Object getProperty(final String _key) {
    if (TITLE.equals(_key)) {
      return getTitle();
    }
    if (BSelecteurCheckBox.PROP_VISIBLE.equals(_key)) {
      return Boolean.valueOf(isVisible());
    }

    // -- cas particulier si il s agit de la rotation --//
    // -- il faut transformer les degres en radian --//
    if (_key.equals(ROTATION)) {
      return Integer.valueOf((int) Math.toDegrees(getRotation()));
    }

    return propGraphique.get(_key);
  }

  public Map<String, Object> getPropGraphique() {
    return propGraphique;
  }

  public double getRotation() {
    if (propGraphique.get(ROTATION) instanceof Integer) {
      propGraphique.put(ROTATION, new Double((Integer) propGraphique.get(ROTATION)));
    }
    return (Double) propGraphique.get(ROTATION);
  }

  Set<EbliWidget> satellites;
  Set<EbliWidget> unModifiableSatellites;
  EbliWidget satelliteOwner;

  public Set<EbliWidget> getSatellites() {
    return unModifiableSatellites;
  }

  public void removeAllSatellites() {
    List<EbliNode> nds = getNodeSattelites();
    if (nds != null) {
      for (EbliNode ebliNode : nds) {
        scene_.removeNode(ebliNode);
      }
    }
  }

  public List<EbliNode> getNodeSattelites() {
    Set<EbliWidget> satellites = getSatellites();
    if (satellites == null) {
      return Collections.emptyList();
    }
    List<EbliNode> nodes = new ArrayList<EbliNode>(satellites.size());
    for (EbliWidget widget : satellites) {
      EbliNode findNodeById = getEbliScene().findNodeById(widget.getId());
      if (findNodeById != null) {
        nodes.add(findNodeById);
      }
    }
    return nodes;
  }

  public void removeSatellite(EbliWidget w) {
    if (satellites != null) {
      satellites.remove(w);
    }
  }

  public void addSatellite(EbliWidget w) {
    createSatelliteList();
    satellites.add(w);
    w.satelliteOwner = this;
  }

  public boolean isSatellite() {
    return satelliteOwner != null;
  }

  private void createSatelliteList() {
    if (satellites == null) {
      satellites = new HashSet<EbliWidget>();
      unModifiableSatellites = Collections.unmodifiableSet(satellites);
    }
  }

  /**
   * Retourne une interface widgetConfigure qui permet de gerer les interfaces a creer
   */
  protected BConfigurableInterface getSingleConfigureInterface(final boolean lineModel_, final boolean colorsContours_, final boolean colorFonds_,
                                                               final boolean rotations_, final boolean transparence, final boolean police_) {

    // -- retourne l interface qui va bien qui permet de creer les composants
    // graphiques correspondant --//
    return new WidgetConfigure(lineModel_, colorsContours_, colorFonds_, rotations_, police_, transparence, this);
  }

  protected String getTitle() {
    final EbliNode n = (EbliNode) getEbliScene().findObject(this);
    return n == null ? CtuluLibString.EMPTY_STRING : n.getTitle();
  }

  /**
   * Retourne les proprietes de la map.
   */
  public TraceLigneModel getTraceLigneModel() {
    return (TraceLigneModel) propGraphique.get(LINEMODEL);
  }

  /**
   * Methode qui indique si lq widget a des sattelites, ie des widgets qui doivent etre deplacees avec.
   *
   * @return true si poss�de des satellites
   */
  public boolean hasSattelite() {
    return CtuluLibArray.isNotEmpty(satellites);
  }

  public boolean isAnimatedItemAlive(final String _id) {
    return false;
  }

  /**
   * @return the isGroup: true si le widget represente un groupe de widget comme un groupe par exemple
   */
  public boolean isGroup() {
    return isGroup;
  }

  /**
   * Methode qui retourne vrai si la widget est group�e.
   */
  public boolean isGrouped() {

    if (this.getParentBordure().getParentWidget() instanceof EbliWidgetGroup) {
      return true;
    }
    return false;
  }

  @Override
  public boolean isHitAt(final Point localLocation) {

    if (localLocation == null) {
      return false;
    }

    if (getBounds() != null) {
      return isVisible() && getBounds().contains(localLocation);
    } else // return isVisible() && getPreferredBounds().contains(localLocation);
    {
      return false;
    }
  }

  public boolean isInEditMode() {
    return isInEditMode_;
  }

  public boolean isTitleModifiable() {
    return true;
  }

  public boolean isUseBorder() {
    return useBorder_;
  }

  @Override
  public void notifyStateChanged(final ObjectState _previousState, final ObjectState _newState) {
    if (!useBorder_) {
      return;
    }

    final EbliLookFeel lookFeel = (EbliLookFeel) getScene().getLookFeel();
    final EbliWidget parent = getGroup();
    if (parent != null) {
      final Border borderInGroup = lookFeel.getBorderInGroup(_newState);
      setBorder(borderInGroup);
      parent.setBorder(lookFeel.getBorder(_newState, parent.getController().canResize_, parent.getController().isProportional()));
    } else {
      if (lookFeel != null && getController() != null) {
        final Border border = lookFeel.getBorder(_newState, getController().canResize_, getController().isProportional());
        setBorder(border);
      }
    }
  }

  /**
   * Methode qui raffraichis les proprietes de la widget.
   */
  public final void refreshMyProperties() {
    getEbliScene().refresh();
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
  }

  public void setColorContour(final Color newColor) {
    propGraphique.put(COLORCONTOUR, newColor);
  }

  public void setColorFond(final Color newColor) {
    propGraphique.put(COLORFOND, newColor);
  }

  public void setController(final EbliWidgetController _controller) {
    this.controller_ = _controller;
  }

  public void setFormeFont(final Font newFont) {
    setPropertyCmd(FONT, newFont, null);
  }

  /**
   * @param isGroup the isGroup to set
   */
  public void setGroup(final boolean isGroup) {
    this.isGroup = isGroup;
  }

  public void setId(final String _id) {
    this.id_ = _id;
  }

  /**
   * Methode � utiliser permettant de prendre en compte des undo/redo
   *
   * @param _visible
   */
  public void setWidgetVisible(boolean _visible) {
    if (getParentBordure() != this) {
      getParentBordure().setWidgetVisible(_visible);
    }
    if (isVisible() == _visible) {
      return;
    }
    EbliNode node = (EbliNode) getEbliScene().findObject(this);
    if (node != null) {
      getEbliScene().getCmdMng().addCmd(EbliWidgetActionChangeVisility.changeVisibility(node, _visible));
    }
  }

  /**
   * Methode directement appelee apres modification des parametres renvoye par le getproperty. Ajoute les anciens parametres dans la commande
   * undo/redo.
   */
  @Override
  public final boolean setProperty(final String _property, final Object _newVlaue) {
    if (TITLE.equals(_property)) {
      return changeTitle((String) _newVlaue, true);
    }
    if (BSelecteurCheckBox.PROP_VISIBLE.equals(_property)) {
      setWidgetVisible(((Boolean) _newVlaue).booleanValue());
      return true;
    }

    // --cas particulier si il s agit du tracelignemodel: il faut mettre a jour
    // l ancien lignemodel --//
    setPropertyCmd(_property, _newVlaue, getEbliScene().getCmdMng());

    return true;
  }

  protected void setPropertyCmd(final String _property, Object _newValue, final CtuluCommandContainer _cmd) {
    final Object old = propGraphique.get(_property);
    if (old == _newValue || (old != null && old.equals(_newValue))) {
      return;
    }

    CommandeUndoRedoGraphicalProperties undoRedo = null;
    if (_cmd != null) {
      undoRedo = new CommandeUndoRedoGraphicalProperties();
      // -- ajout de la widget en question ainsi que la oldpropertie --//
      undoRedo.addWidget(this);
      // inutile de tout dupliquer
      undoRedo.addOldPropertie(this.duplicateGraphicalProperties());
      _cmd.addCmd(undoRedo);
    }
    if (_property.equals(LINEMODEL)) {
      getTraceLigneModel().updateData((TraceLigneModel) _newValue);
      _newValue = getTraceLigneModel();
    } else if (_property.equals(ROTATION) && _newValue instanceof Integer) {
      _newValue = new Double(Math.toRadians(((Integer) _newValue).intValue()));
    }

    // --mise a jout de la map de proprietes --//
    propGraphique.put(_property, _newValue);
    getEbliScene().firePropertyChange(this, _property);
    if (undoRedo != null) {
      // -- ajout de la nouvelle propertie --//
      undoRedo.addNewPropertie(propGraphique);
    }

    // -- mise a jour des parametres de la map --//
    refreshMyProperties();
  }

  public final void setPropGraphique(final Map<String, Object> propGraphique) {
    for (final Map.Entry<String, Object> it : propGraphique.entrySet()) {
      setPropertyCmd(it.getKey(), it.getValue(), null);
    }
  }

  public void setRotation(final double rotation) {
    propGraphique.put(ROTATION, rotation);
  }

  public void setSatellite(Collection<EbliWidget> liste) {
    createSatelliteList();
    satellites.clear();
    satellites.addAll(liste);
    for (EbliWidget ebliWidget : liste) {
      ebliWidget.satelliteOwner = this;
    }
  }

  public void setScene(final EbliScene _scene) {
    this.scene_ = _scene;
  }

  public void setTraceLigneModel(final TraceLigneModel _l) {
    propGraphique.put(LINEMODEL, _l);
  }

  public void setUseBorder(final boolean _useBorder) {
    useBorder_ = _useBorder;
  }

  /**
   * Methode appelee par le notifyAdded de la EbliScene apres ajout du node. Dans cette classe on peut appliquer des traitement particuliers tout
   * juste apres insertion dans la scene.
   *
   * @param node
   * @see EbliWidgetCreatorArrow
   */
  public void notifyNodeAdded(EbliNode node) {
  }

  /**
   * Est appel� des que la widget a �t� d�plac�.
   *
   * @param _location
   */
  public void notifyWidgetMoved(final Point _location) {
  }

  public final void bringToPosition(int position) {
    if (getParentWidget() == null) {
      return;
    }
    List<Widget> children = getParentWidget().getChildren();
    int i = children.indexOf(this);
    if (i < 0) {
      return;
    }
    children.remove(i);
    children.add(position, this);
    revalidate();
    getParentWidget().revalidate();
  }

  public boolean canTranparency() {
    return true;
  }
}
