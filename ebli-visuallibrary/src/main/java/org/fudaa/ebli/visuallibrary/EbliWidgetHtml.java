/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import javax.swing.JLabel;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import org.netbeans.api.visual.widget.Scene;

/**
 * @author deniger
 */
public class EbliWidgetHtml extends EbliWidgetWithView {

  JLabel c = new JLabel();
  View view;

  public EbliWidgetHtml(Scene scene, String html) {
    super(scene);
    setHtml(html);
  }

  public final void setHtml(String html) {
    view = BasicHTML.createHTMLView(c, html);
    c.setText(html);
  }

  public final void setText(String html) {
    setHtml(html);
  }

  public final String getText() {
    return c.getText();
  }

  @Override
  protected JLabel getLabel() {
    return c;
  }

  @Override
  protected View getView() {
    return view;
  }

  @Override
  protected View refreshView() {
    view = BasicHTML.createHTMLView(c, c.getText());
    return view;
  }

}
