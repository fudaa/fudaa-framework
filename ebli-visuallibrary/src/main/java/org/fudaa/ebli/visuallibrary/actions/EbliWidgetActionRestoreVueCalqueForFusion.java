/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetFusionCalques;

import java.awt.event.ActionEvent;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public class EbliWidgetActionRestoreVueCalqueForFusion extends EbliWidgetActionAbstract {
  EbliNode target_;

  public EbliWidgetActionRestoreVueCalqueForFusion(final EbliScene _scene, final EbliNode _target) {
    super(_scene, EbliLib.getS("Restaurer toutes les vues"), EbliResource.EBLI.getToolIcon("restore.png"), "RESTORE_ALL");
    target_ = _target;
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    EbliWidgetFusionCalques widget = (EbliWidgetFusionCalques) target_.getCreator().getWidget();
    widget.restaurer();
    scene_.validate();
  }
}
