package org.fudaa.ebli.visuallibrary.graphe;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.courbe.*;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.EditProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.border.EmptyBorder;
import org.netbeans.modules.visual.layout.FlowLayout;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Creation de la classe widget specifique au calque. Permet de gerer le resize via le changement de font
 */
public class EbliWidgetGrapheLegende extends EbliWidget implements EditProvider, EGGrapheModelListener {
  EGGraphe graphe_;

  public EbliWidgetGrapheLegende(EGGraphe graphe, final EbliScene scene, final boolean _controller) {
    super(scene, _controller);
    graphe_ = graphe;
    setCheckClipping(true);
    updateFromLegend();
    // -- creation de l action --//
    final WidgetAction editorAction = ActionFactory.createEditAction(this);

    // -- ajout de l action au label correspondant --//
    this.getActions().addAction(editorAction);
    graphe_.getModel().addModelListener(this);
  }

  @Override
  public void setPropertyCmd(final String _key, final Object _prop, final CtuluCommandContainer _cmd) {
    super.setPropertyCmd(_key, _prop, _cmd);
    if ("font".equals(_key)) {
      final Font ft = (Font) _prop;
      updateFont(ft);
    }
  }

  public void majSuppression() {
    repaint();
  }

  @Override
  public void setFormeFont(final Font _newFont) {
    super.setFormeFont(_newFont);
    updateFont(_newFont);
  }

  private void updateFont(Font newFont) {
    updateFromLegend();
    revalidate();
    getEbliScene().refresh();
  }

  @Override
  protected void paintWidget() {
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canColorForeground() {
    return false;
  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  public boolean canFont() {
    return true;
  }

  @Override
  public void edit(Widget arg0) {
    EGPaletteLegendeGraphe palette = new EGPaletteLegendeGraphe(this.graphe_);
    palette.setPreferredSize(new Dimension(400, 600));
    JDialog d = new JDialog(CtuluLibSwing.getActiveWindow());
    d.setContentPane(palette);
    d.setModal(true);
    d.pack();
    d.setLocationRelativeTo(getScene().getView());
    d.setVisible(true);
  }

  Map<EGCourbe, EbliWidgetLine> lines = new HashMap<EGCourbe, EbliWidgetLine>();
  Map<EGCourbe, EbliWidgetTitle> titles = new HashMap<EGCourbe, EbliWidgetTitle>();

  private void updateFromLegend() {
    removeChildren();
    lines.clear();
    titles.clear();
    setLayout(new FlowLayout(true, SerialAlignment.CENTER, 5));// a
    Widget w = new Widget(getScene());
    w.setLayout(new FlowLayout(true, SerialAlignment.LEFT_TOP, 5));// a
    w.setBorder(new EmptyBorder(5, 5, 5, 5, false));
    final EGCourbe[] cs = graphe_.getModel().getCourbes();
    addChild(w);
    for (int i = 0; i < cs.length; i++) {
      final EGCourbe egCourbe = cs[i];
      if (egCourbe == null || egCourbe.getTitle() == null) {
        continue;
      }
      if (!egCourbe.isDisplayed()) {
        continue;
      }
      if (egCourbe.getTitle().equals("X") || egCourbe.getTitle().equals("Y")) {
        // -- on n'affiche pas les l�gendes X et Y
      } else {
        final EbliWidget line = new EbliWidget(getEbliScene());
        line.setLayout(new FlowLayout(false, SerialAlignment.CENTER, 5));
        // creation de la widget titre
        final EbliWidgetTitle textWidget = new EbliWidgetTitle(getEbliScene(), egCourbe, null);
        textWidget.setFont(getFormeFont());
        // creation de la widget ligne
        final EbliWidgetLine lineWidget = new EbliWidgetLine(getEbliScene(), egCourbe, null);
        lines.put(egCourbe, lineWidget);
        titles.put(egCourbe, textWidget);
        lineWidget.setMinimumSize(new Dimension(30, 5));
        line.setUseBorder(false);
        lineWidget.setUseBorder(false);
        // textWidget.setUseBorder(false);

        lineWidget.setEnabled(false);
        line.addChild(lineWidget);
        line.addChild(textWidget);
        w.addChild(line);
      }
    }
    getEbliScene().refresh();
  }

  @Override
  public void structureChanged() {
    updateFromLegend();
    revalidate();
    getEbliScene().revalidate();
  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean restore) {
    courbeAspectChanged(_c, false);
  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
    EbliWidgetTitle title = titles.get(_c);
    if (title != null) {
      title.majLabel();
      if (_visibil) {
        title.setVisible(_c.isVisible());
      }
      title.revalidate();
      title.repaint();
    }
    EbliWidgetLine line = lines.get(_c);
    if (line != null) {
      line.revalidate();
      if (_visibil) {
        line.setVisible(_c.isVisible());
      }
    } else {
      updateFromLegend();
    }

    getEbliScene().refresh();
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
  }

  @Override
  public void axeAspectChanged(final EGAxe _c) {
  }
}
