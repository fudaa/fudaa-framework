package org.fudaa.ebli.visuallibrary;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.widget.Widget;

/**
 * Widget qui a pour but de grouper plusieurs sous widget.
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetGroup extends EbliWidget {

  public final static String NOGROUP = "N/A";

  /**
   * Enregistre les proportions des widgets initialement par rapport a la taille de la widget
   */
  public Map<EbliWidget, Couple> listeProportionsWidgets = new HashMap<EbliWidget, Couple>();

  public EbliWidgetGroup(final EbliScene scene) {
    super(scene);
  }

  private static class Couple {
    public double propX;
    public double propY;

    public Couple(double propX, double propY) {
      super();
      this.propX = propX;
      this.propY = propY;
    }
  }

  public void setProportions() {
    if (getPreferredSize() == null) return;
    double width = getPreferredSize().width;
    double height = getPreferredSize().height;

    for (Widget widget : getChildren()) {
      if (widget instanceof EbliWidget && widget.getPreferredSize() != null) {
        double w = widget.getPreferredSize().width;
        double h = widget.getPreferredSize().height;

        Couple prop = new Couple(w / width, h / height);
        listeProportionsWidgets.put((EbliWidget) widget, prop);
      }

    }
  }

  @Override
  public boolean isHitAt(Point localLocation) {
    boolean res=super.isHitAt(localLocation);
    return  res;
  }

  public Dimension getPropDimension(EbliWidget widget) {

    Couple proportions = listeProportionsWidgets.get(widget);
    if (proportions == null) return null;

    if (getBounds() == null) return null;
    int width = getBounds().width;
    int height = getBounds().height;

    double w = proportions.propX * width;
    double h = proportions.propY * height;

    return new Dimension((int) w, (int) h);
  }

}
