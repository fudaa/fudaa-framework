package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.AffineTransform;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ebli.controle.BSelecteurAbstract;
import org.fudaa.ebli.visuallibrary.actions.EbliWidgetTriangleAnchorShape.Type;
import org.netbeans.api.visual.anchor.AnchorShape;

public class EbliWidgetArrowSelector extends BSelecteurAbstract implements ItemListener, ChangeListener {

  public static class AnchorIcon implements Icon {

    private final boolean inverse;

    private AnchorIcon(boolean inverse) {
      super();
      this.inverse = inverse;
    }

    private AnchorShape shape;

    @Override
    public int getIconHeight() {
      return 12;
    }

    @Override
    public int getIconWidth() {
      return 12;
    }

    public AnchorShape getShape() {
      return shape;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
      Graphics2D g2d = (Graphics2D) g;
      AffineTransform transform = g2d.getTransform();
      g.setColor(Color.BLACK);
      int ty = getIconHeight() / 2 + y;
      g.translate(x, ty);
      if(!inverse){
      g2d.rotate(Math.PI, getIconWidth()/2,0);
      }
      shape.paint(g2d, false);
      g2d.setTransform(transform);

    }

    public void setShape(AnchorShape shape) {
      this.shape = shape;
    }

  }

  private static class CellArrowRenderer extends CtuluCellTextRenderer {

    private final AnchorIcon icon;

    public CellArrowRenderer(boolean inverse) {
      icon = new AnchorIcon(inverse);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (!(value instanceof EbliWidgetTriangleAnchorShape.Type))
        return this;
      Type type = (Type) value;
      icon.setShape(EbliWidgetTriangleAnchorShape.getDefault(type));
      setIcon(icon);
      setText(type.getName());
      return this;
    }
  }

  private final boolean inverse;

  private void configureSourceAnchor(JComboBox cb) {
    DefaultComboBoxModel modele = new DefaultComboBoxModel();
    modele.addElement(EbliWidgetTriangleAnchorShape.Type.NONE);
    modele.addElement(EbliWidgetTriangleAnchorShape.Type.FILLED);
    modele.addElement(EbliWidgetTriangleAnchorShape.Type.HOLLOW);
    modele.addElement(EbliWidgetTriangleAnchorShape.Type.OUT);
    cb.setModel(modele);
    cb.setRenderer(new CellArrowRenderer(inverse));
  }

  public static final String PROPERTY = "arrow";

  BuComboBox cbAnchorType;

  JSpinner anchorTaille;

  SpinnerNumberModel tailleModel_;

  public EbliWidgetArrowSelector(final boolean inverse) {
    this(PROPERTY, inverse, null);
  }

  public EbliWidgetArrowSelector(final String _prop, final boolean inverse) {
    this(_prop, inverse, null);
  }

  public EbliWidgetArrowSelector(final String _prop, final boolean inverse, final EbliWidgetTriangleAnchorShape _data) {
    super(_prop);
    this.inverse = inverse;
    cbAnchorType = new BuComboBox();

    configureSourceAnchor(cbAnchorType);
    cbAnchorType.addItemListener(this);
    int val = 12;
    if (_data != null) {
      val = _data.getSize();
      cbAnchorType.setSelectedItem(_data.getType());
    }
    tailleModel_ = new SpinnerNumberModel(val, 10, 150, 1);
    anchorTaille = new JSpinner(tailleModel_);
    tailleModel_.addChangeListener(this);
    anchorTaille.setModel(tailleModel_);
  }

  public EbliWidgetArrowSelector(final EbliWidgetTriangleAnchorShape _data, final boolean inverse) {
    this(PROPERTY, inverse, _data);
  }

  public JPanel buildPanel() {
    final JPanel r = new BuPanel();
    buildPanel(r, true);
    return r;
  }

  public void buildPanel(final JPanel _pn, final boolean _layout) {
    if (_layout) {
      _pn.setLayout(new BuGridLayout(3, 3, 3, true, true));
    }
    _pn.add(cbAnchorType);
    _pn.add(anchorTaille);

  }

  protected void fireDataChanged(final boolean _type, final boolean _epaisseur, final boolean _color) {
    if (isUpdating_) {
      return;
    }
    firePropertyChange(getProperty(), getNewData());
  }

  public JComponent getComponent() {
    return buildPanel();
  }

  @Override
  public JComponent[] getComponents() {
    return new JComponent[] { cbAnchorType, anchorTaille };
  }

  public EbliWidgetTriangleAnchorShape getNewData() {
    int size = this.tailleModel_.getNumber().intValue();
    Type type = (Type) cbAnchorType.getSelectedItem();
    return new EbliWidgetTriangleAnchorShape(type, size);
  }

  public final SpinnerNumberModel getTailleModel() {
    return tailleModel_;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.DESELECTED) {
      return;
    }
    updateEnableStates();
    fireDataChanged(true, false, false);
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    fireDataChanged(false, true, false);
  }

  private void updateEnableStates() {
    final boolean enable = cbAnchorType.getSelectedItem() != Type.NONE;
    anchorTaille.setEnabled(enable);
  }

  @Override
  public void updateFromTarget() {
    final EbliWidgetTriangleAnchorShape ic = (EbliWidgetTriangleAnchorShape) getTargetValue();
    final Type type = ic == null ? null : ic.getType();
    cbAnchorType.setSelectedItem(type);
    tailleModel_.setValue(CtuluLib.getDouble(Math.max(10, ic == null ? 10 : ic.getSize())));
    updateEnableStates();

  }
}