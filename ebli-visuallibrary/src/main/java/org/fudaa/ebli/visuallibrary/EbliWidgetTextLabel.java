package org.fudaa.ebli.visuallibrary;

import java.awt.Font;
import java.awt.Insets;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.TextFieldInplaceEditor;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.LabelWidget.VerticalAlignment;
import org.netbeans.api.visual.widget.Widget;

/**
 * Legende Widget qui permet de construire des rectangles avec du texte
 * 
 * @author Adrien Hadoux TODO a generaliser avec autre chose qu'un LabelWidget
 */
public class EbliWidgetTextLabel extends EbliWidget implements TextFieldInplaceEditor {

  LabelWidget intern;
  Insets inGaps_;
  int largeurBorder = 15;
  int espaceInterieur = 5;

  public Font fontForme;

  /**
   * @param _scene
   */
  public EbliWidgetTextLabel(final EbliScene _scene, final String label) {
    super(_scene, false);

    intern = new LabelWidget(_scene);
    intern.setVerticalAlignment(VerticalAlignment.CENTER);
    intern.setAlignment(Alignment.CENTER);
    intern.setFont(getFormeFont());

    intern.setLabel(label);

    // -- layout du texte --//
    addChild(intern);

    // -- ajouter l option de remplacer le text dans le label --//

    // -- creation de l action --//
    final WidgetAction editorAction = ActionFactory.createInplaceEditorAction(this);

    // -- ajout de l action au label correspondant --//
    this.getActions().addAction(editorAction);

    // intern.getActions().addAction(ActionFactory.createResizeAction());

    // setEnabled(false);

    // -- ajout du controller special sans actions, juste le menu --/
    final EbliWidgetControllerMenuOnly controller = new EbliWidgetControllerMenuOnly(this);
    controller.setEditable(true);
    setController(controller);

  }

  public void majLabel(final String label) {
    // intern.setForeground();
    intern.setLabel(label);
    revalidate();
  }


  @Override
  protected void paintWidget() {

    // -- mise a jour de la fonte que si il y a eu une modification --//
    if (intern.getFont() != getFormeFont()) intern.setFont(getFormeFont());

    if (intern.getForeground() != getColorContour()) intern.setForeground(getColorContour());

  }

  @Override
  public String getText(final Widget widget) {

    return this.intern.getLabel();
  }

  @Override
  public boolean isEnabled(final Widget widget) {
    return true;
  }

  @Override
  public void setText(final Widget widget, final String text) {

    this.intern.setLabel(text);
    revalidate();

  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canColorForeground() {
    return true;
  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  public boolean canFont() {
    return true;
  }

}