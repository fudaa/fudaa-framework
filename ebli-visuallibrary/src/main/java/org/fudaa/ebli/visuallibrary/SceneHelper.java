package org.fudaa.ebli.visuallibrary;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collection;

public class SceneHelper {

  public static Point getOptimalePosition(EbliScene scene) {
    EbliWidget widget = findLowerNode(scene);
    if (widget == null) {
      return new Point(20, 20);
    }
    Rectangle convertLocalToScene = widget.convertLocalToScene(widget.getBounds());
    return new Point(convertLocalToScene.x, convertLocalToScene.y + convertLocalToScene.height + 20);

  }

  /**
   * Warn: the name lower is confusing: y axis is inversed
   * 
   * @param scene
   * @return
   */
  private static EbliWidget findLowerNode(EbliScene scene) {
    Collection<EbliNode> nodes = scene.getNodes();
    //the lower widget has the higher y...
    int higher = 0;
    EbliWidget res = null;
    for (EbliNode ebliNode : nodes) {
      EbliWidget widget = ebliNode.getWidget().getIntern();
      Rectangle convertLocalToScene = widget.convertLocalToScene(widget.getBounds());
      int y = convertLocalToScene.y + convertLocalToScene.height;
      if (y > higher) {
        higher = y;
        res = widget;
      }
    }
    return res;

  }

}
