/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.animation;

import java.lang.ref.WeakReference;
import javax.swing.Icon;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * @author deniger
 */
public class EbliWidgetAnimatedItem {

  /**
   * L'interface d'animation. On utilise une weakreference car l'inteface peut etre supprime de la scene et toujours
   * reference dans la liste des anim�es
   */
  private final WeakReference<EbliAnimationAdapterInterface> animated_;
  /**
   * L'icone. On utilise une weakreference car l'inteface peut etre supprime de la scene et toujours reference dans la
   * liste des anim�es
   */
  private final WeakReference<Icon> icon_;

  /**
   * L'identifiant
   */
  private final String id_;
  private final String widgetId_;
  private final String title_;

  public EbliWidgetAnimatedItem(final EbliAnimationAdapterInterface _animated, final Icon _icon,
      final String _widgetId, final String _id, final String _title) {
    super();
    animated_ = new WeakReference<EbliAnimationAdapterInterface>(_animated);
    icon_ = new WeakReference<Icon>(_icon);
    id_ = _id;
    widgetId_ = _widgetId;
    title_ = _title;
  }

  /**
   * @return the widgetId
   */
  public String getWidgetId() {
    return widgetId_;
  }

  public boolean isAlive(final EbliScene _scene) {
    if (animated_.get() == null) return false;
    final EbliWidget w = _scene.findById(getWidgetId());
    if (w == null) return false;
    return w.isAnimatedItemAlive(getId());

  }

  /**
   * @return the animated
   */
  public EbliAnimationAdapterInterface getAnimated() {
    return animated_.get();
  }

  /**
   * @return the icon
   */
  public Icon getIcon() {
    return icon_.get();
  }

  /**
   * @return the id
   */
  public String getId() {
    return id_;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title_;
  }

}
