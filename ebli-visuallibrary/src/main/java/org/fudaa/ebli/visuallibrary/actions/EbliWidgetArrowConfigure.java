package org.fudaa.ebli.visuallibrary.actions;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.visuallibrary.EbliWidgetArrow;

public class EbliWidgetArrowConfigure implements BConfigurableInterface {

  public static final String ANCHOR_TARGET = "ANCHOR_TARGET";
  public static final String ANCHOR_SOURCE = "ANCHOR_SOURCE";
  EbliWidgetArrow arrow;

  public EbliWidgetArrowConfigure(EbliWidgetArrow arrow) {
    super();
    this.arrow = arrow;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {

    EbliWidgetArrowSelector cbSource = new EbliWidgetArrowSelector(ANCHOR_SOURCE,true);
    cbSource.setTitle(EbliLib.getS("D�but"));
    EbliWidgetArrowSelector cbTarget = new EbliWidgetArrowSelector(ANCHOR_TARGET,false);
    cbTarget.setTitle(EbliLib.getS("Fin"));
    return new BSelecteurInterface[] { cbSource, cbTarget };
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return arrow;
  }

  @Override
  public void stopConfiguration() {

  }

  @Override
  public String getTitle() {
    return EbliLib.getS("Fl�che");
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

}
