package org.fudaa.ebli.visuallibrary.calque;

import java.awt.Font;
import java.awt.Point;
import java.util.Map;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;

/**
 * Creator pour les legendes des calques
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetCreatorTimeLegend extends EbliWidgetCreator {

  public EbliWidgetCreatorTimeLegend(final EbliWidgetCreatorVueCalque satelliteCreatorOwner) {
    this(null, satelliteCreatorOwner);
  }

  protected EbliWidgetCreatorTimeLegend(EbliWidgetCreatorTimeLegend toCopy, final EbliWidgetCreatorVueCalque satelliteCreatorOwner) {
    super(toCopy);
    super.owner = satelliteCreatorOwner;
  }

  public EbliWidgetCreatorTimeLegend() {

  }

  public static EbliNode create(final Point _location, final EbliScene _scene, final EbliWidgetCreatorVueCalque _id) {
    final EbliNode def = new EbliNodeDefault();

    def.setCreator(new EbliWidgetCreatorTimeLegend(_id));
    def.getCreator().setPreferredLocation(_location);
    def.setTitle(EbliLib.getS("Temps"));
    _scene.addNode(def);
    ((EbliWidgetTimeLegende) def.getWidget().getIntern()).setTitle(def.getTitle());
    _scene.refresh();

    return def;
  }

  @Override
  public void widgetAttached() {
    super.widgetAttached();
    ((EbliWidgetTimeLegende) getWidget().getIntern()).addListeners();
  }

  @Override
  public void widgetDetached() {
    super.widgetDetached();
    ((EbliWidgetTimeLegende) getWidget().getIntern()).removeListeners();

  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    EbliWidgetTimeLegende legende = new EbliWidgetTimeLegende(_scene, false);
    legende.setVisuPanel((EbliWidgetCreatorVueCalque) owner);
    EbliWidgetBordureSingle res = new EbliWidgetBordureSingle(legende, true, true);
    legende.setFormeFont(new Font("Helvetica", Font.PLAIN, 10));
    legende.setController(new EbliWidgetControllerLegendeTime(res));
    legende.updateTimesWidget();
    legende.setTitleVisible(titleVisible);
    return res;
  }

  private boolean titleVisible = true;

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    EbliWidgetCreatorVueCalque duplicatedOwner = (EbliWidgetCreatorVueCalque) satteliteOwnerDuplicated;
    EbliWidgetCreatorTimeLegend legend = new EbliWidgetCreatorTimeLegend(this, duplicatedOwner);
    legend.titleVisible = titleVisible;
    return legend;
  }

  @Override
  public Object savePersistData(final Map parameters) {
    EbliWidgetLegendTimePersist data = new EbliWidgetLegendTimePersist();
    data.setIdOwnerWidget(owner.getWidget().getId());
    data.setTitleVisible(((EbliWidgetTimeLegende) getWidget().getIntern()).isTitleVisible());
    return data;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    if (data != null) {
      EbliWidgetLegendTimePersist persist = (EbliWidgetLegendTimePersist) data;
      String id = persist.getIdOwnerWidget();
      titleVisible = persist.titleVisible;
      if (id != null) {
        final EbliScene scene = (EbliScene) parameters.get("scene");
        EbliNode nodeCalque = scene.findNodeById(id);
        if (nodeCalque != null && nodeCalque.getController() instanceof EbliWidgetControllerCalque) {
          super.owner = nodeCalque.getCreator();
          return owner != null;
        }

      }
    }
    return false;

  }

}
