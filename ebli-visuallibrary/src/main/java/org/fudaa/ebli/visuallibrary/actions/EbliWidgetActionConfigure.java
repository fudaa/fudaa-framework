package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuResource;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Set;
import javax.swing.JDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurePalette;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.widget.Widget;

/**
 * Action qui permet de configurer les composants en ouvrant les interfaces qui vont bien. Ouvre la dialog de choix des
 * composants
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class EbliWidgetActionConfigure extends EbliWidgetActionAbstract {

  public EbliWidgetActionConfigure(final EbliScene _scene) {
    super(_scene, EbliLib.getS("Configure"), BuResource.BU.getIcon("configurer"), "CONFIGURE");

  }

  /**
     *
     */
  @Override
  public void actionPerformed(final ActionEvent e) {
    final EbliWidget found = findWidget();
    configure(found);
  }

  public static void configure(final EbliWidget found) {
    if (found != null) {
      final BConfigurePalette palette = new BConfigurePalette(true, null, null);

      final BConfigurableComposite cmp = new BConfigurableComposite(found.getConfigureInterfaces(found.canTraceLigneModel(), found.canColorForeground(),
      found.canColorBackground(), found.canRotate(), found.canFont(), found.canTranparency()), "palette");
      palette.setTargetConf(cmp);
      palette.setTitleVisibleTarget(found, true);

      final JDialog d = CtuluLibSwing.createDialogOnActiveWindow(EbliLib.getS("Configuration graphique"));
      d.setModal(true);
      d.setContentPane(palette);
      Dimension dim = palette.getPreferredSize();
      dim.height = Math.max(dim.height, 550);
      dim.width = Math.max(dim.width, 500);
      palette.setPreferredSize(dim);
      d.pack();
      d.setLocationRelativeTo(d.getParent());
      d.setVisible(true);
    }
  }

  EbliWidget findWidget() {
    final Set objs = scene_.getSelectedObjects();
    for (final Iterator iterator = objs.iterator(); iterator.hasNext();) {
      final Object object = iterator.next();
      final Widget w = scene_.findWidget(object);
      if (w instanceof EbliWidget) { return (EbliWidget) w; }
    }
    return null;

  }
}
