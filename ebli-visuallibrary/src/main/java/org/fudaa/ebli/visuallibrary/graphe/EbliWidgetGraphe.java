package org.fudaa.ebli.visuallibrary.graphe;

import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.courbe.EGAxe;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModelListener;
import org.fudaa.ebli.courbe.EGObject;
import org.fudaa.ebli.courbe.EGVue;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.animation.EbliWidgetAnimatedItem;
import org.netbeans.api.visual.action.InplaceEditorProvider;
import org.netbeans.api.visual.widget.Widget;

/**
 * widget qui contient un objet graphe
 * 
 * @author adrien hadoux
 */
public final class EbliWidgetGraphe extends EbliWidget implements InplaceEditorProvider<EGVue>, EGGrapheModelListener {

  // private EGGraphe graphe_;
  // Window frame_;
  EGFillePanel panelGraphe_;

  public EGFillePanel getPanelGraphe() {
    return panelGraphe_;
  }

  BuPanel conteneurEditor;

  @Override
  public EbliAnimatedInterface getAnimatedInterface() {
    if (panelGraphe_ instanceof EbliAnimatedInterface) return (EbliAnimatedInterface) panelGraphe_;
    return null;
  }

  @Override
  public List<EbliWidgetAnimatedItem> getAnimatedItems() {
    final EGCourbe[] cqs = panelGraphe_.getGraphe().getModel().getCourbes();
    final List<EbliWidgetAnimatedItem> res = new ArrayList<EbliWidgetAnimatedItem>(20);
    for (int i = 0; i < cqs.length; i++) {
      EGCourbe courbe = cqs[i];
      if (courbe.isVisible() && courbe instanceof EbliAnimationAdapterInterface) {
        res.add(new EbliWidgetAnimatedItem((EbliAnimationAdapterInterface) courbe, courbe, getId(), getAnimId(courbe),
            courbe.getTitle()));
      }
    }
    return res;
  }

  private String getAnimId(EGCourbe courbe) {
    return courbe.hashCode() + courbe.getTitle();
  }

  @Override
  public boolean isAnimatedItemAlive(final String _id) {
    if (_id == null) return false;
    final EGCourbe[] cqs = panelGraphe_.getGraphe().getModel().getCourbes();
    for (int i = 0; i < cqs.length; i++) {
      EGCourbe courbe = cqs[i];
      if (_id.equals(getAnimId(courbe))) return true;
    }
    return false;
  }

  public Color couleurContour = Color.black;
  public Color couleurFond = Color.white;

  public EGGraphe getGraphe() {
    return panelGraphe_.getGraphe();
  }

  public EbliWidgetGraphe(final EbliScene scene, final Point preferredLocation, final EGFillePanel _pn) {
    super(scene);
    panelGraphe_ = _pn;
    panelGraphe_.setBorder(null);
    panelGraphe_.getView().setBorder(null);

    panelGraphe_.getGraphe().getModel().addModelListener(this);

  }

  public EbliWidgetGraphe(final EbliScene scene, final Point preferredLocation, final EGGraphe _graphe) {
    this(scene, preferredLocation, new EGFillePanel(_graphe));

  }

  BufferedImage imageGraphe;

  public BufferedImage getImageGraphe() {
    return imageGraphe;
  }

  @Override
  protected void notifyAdded() {
    super.notifyAdded();
  }

  @Override
  protected void notifyRemoved() {
    super.notifyRemoved();
  }

  @Override
  protected void paintWidget() {
    //in edition mode we do nothing: the component is rendering.
    if (isInEditMode()) { return; }
    final Rectangle rec = getClientArea();
    final Graphics2D g = getGraphics();
    rec.width -= 1;
    rec.height -= 1;
    // g.translate(rec.x, rec.y);
    if (rec.width > 0 && rec.height > 0) {
      panelGraphe_.getGraphe().setSize(rec.width, rec.height);
      panelGraphe_.setSize(rec.width, rec.height);
      panelGraphe_.getView().setSize(rec.width, rec.height);

      // mode edition
      if (imageGraphe == null || imageGraphe.getWidth() != (rec.width) || imageGraphe.getHeight() != (rec.height)) {
        FuLog.debug("EWI: recreate image");
        final Map params = new HashMap();
        CtuluLibImage.setCompatibleImageAsked(params);

        imageGraphe = getGraphe().produceImage(rec.width, rec.height, params);

      }
      g.drawImage(imageGraphe, rec.x, rec.y, rec.width, rec.height, null);
    }
  }

  @Override
  public EGVue createEditorComponent(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget) {
    return panelGraphe_.getView();
  }

  @Override
  public void notifyClosing(final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller,
      final Widget widget, final EGVue editor, final boolean commit) {
    this.imageGraphe = null;
    editingStop(editor);
    getEbliScene().refresh();
  }

  @Override
  public EnumSet<org.netbeans.api.visual.action.InplaceEditorProvider.ExpansionDirection> getExpansionDirections(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget,
      final EGVue editor) {
    return null;
  }

  @Override
  public Rectangle getInitialEditorComponentBounds(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget,
      final EGVue editor, final Rectangle viewBounds) {
    // final Rectangle rec = convertLocalToScene(getClientArea());
    // rec.width -= 1;
    // rec.height -= 1;
    // return rec;
    return null;
  }

  @Override
  public void notifyOpened(final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller,
      final Widget widget, final EGVue editor) {
    editingStart(editor);
  }
  
  protected void repaintScene() {
    // il faut indiquer que l image a ete modifiee donc la forcer a se repaindre
    this.imageGraphe = null;
    if (getEbliScene().getView() != null) {
      repaint();
      // w.revalidate(true);
      // w.getScene_().revalidate(true);
      getEbliScene().getView().repaint(convertLocalToScene(getBounds()));
    }
  }

  @Override
  public void axeAspectChanged(final EGAxe _c) {
    repaintScene();
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
    repaintScene();
  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
    repaintScene();
  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean restore) {

    repaintScene();
  }

  @Override
  public void structureChanged() {
    repaintScene();
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canColorForeground() {
    return false;
  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  public boolean canFont() {
    return false;
  }

}
