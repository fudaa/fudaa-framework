package org.fudaa.ebli.visuallibrary;

import java.awt.Point;

/**
 * @author Adrien Hadoux
 */
public class EbliWidgetArrowAnchor extends EbliWidget {

  private final EbliWidgetArrow main;

  public EbliWidgetArrowAnchor(final EbliWidgetArrow connection, final boolean resize) {
    super(connection.getEbliScene(), resize);
    this.main = connection;
    setBorder(EbliLookFeel.getDefaultBorder());

  }

  /**
   * repositionne la widget centrale des qu'il y a un deplacement.
   */
  @Override
  public void notifyWidgetMoved(final Point _location) {
    getEbliScene().bringToFirst(this);
    main.updatePositionFromAnchor();
  }

}