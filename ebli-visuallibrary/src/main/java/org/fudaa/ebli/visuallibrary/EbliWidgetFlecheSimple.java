package org.fudaa.ebli.visuallibrary;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.visuallibrary.layout.OverlayLayoutGap;

/**
 * Legende Widget qui permet de construire une fleche simple sans contenu
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetFlecheSimple extends EbliWidget {

  Insets inGaps_;
  int largeurBorder = 15;
  int espaceInterieur = 5;
  public static int ORIENTATION_NORTH = 0;
  public static int ORIENTATION_SOUTH = 1;
  public static int ORIENTATION_EAST = 2;
  public static int ORIENTATION_WEST = 3;

  public Color couleurContour = Color.black;
  public Color couleurFond = Color.white;

  /**
   * @param _scene
   */
  public EbliWidgetFlecheSimple(final EbliScene _scene, final int orientation) {
    super(_scene, true);
    inGaps_ = new Insets(largeurBorder + espaceInterieur, largeurBorder + espaceInterieur, largeurBorder
        + espaceInterieur, largeurBorder + espaceInterieur);
    // Ce layout permet de placer le texte

    setLayout(new OverlayLayoutGap(inGaps_));

  }

  /*
   * protected Rectangle calculateClientArea() { Rectangle res = this.getPreferredBounds(); // TODO Auto-generated
   * method stub return new Rectangle(0, 0, 2 (largeurBorder + espaceInterieur) + res.width, 2 (largeurBorder +
   * espaceInterieur) + res.height); }
   */

  @Override
  protected void paintWidget() {
    final Graphics2D g = getGraphics();

    final Rectangle rec = getClientArea();

    // -- couleur de fond --//
    g.setColor(couleurFond);
    g.fillRect(0, 0, rec.width, rec.height);

    g.translate(rec.x, rec.y);
    // la ligne

    final TraceLigne l = new TraceLigne();

    l.setEpaisseur(largeurBorder);

    l.setCouleur(couleurContour);

    l.dessineFleche(g, (int) (l.getEpaisseur() / 2), (rec.height / 2), (int) (rec.width - l.getEpaisseur() / 2),
        (rec.height / 2));

    /*
     * Shape shape; l.dessineShape(g, shape);
     */

    g.translate(-rec.x, -rec.y);

  }

  @Override
  public void setColorContour(final Color newColor) {
    couleurContour = newColor;
    repaint();
  }

  @Override
  public Color getColorContour() {
    return couleurContour;
  }

  @Override
  public void setColorFond(final Color newColor) {
    couleurFond = newColor;
    repaint();
  }

  @Override
  public Color getColorFond() {
    return couleurFond;
  }

}
