/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.animation;

import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

public class EbliWidgetAnimTreeNode extends DefaultMutableTreeTableNode {
  boolean selected_;

  public EbliWidgetAnimTreeNode() {}

  public EbliWidgetAnimTreeNode(final EbliWidgetAnimatedItem _userObject) {
    super(_userObject);
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public Object getValueAt(final int _column) {
    if (_column == 1) return Boolean.valueOf(selected_);
    return super.getValueAt(_column);
  }

  @Override
  public boolean isEditable(final int _column) {
    return _column == 1;
  }

  /**
   * @return the selected
   */
  public boolean isSelected() {
    return selected_;
  }

  @Override
  public void setValueAt(final Object _value, final int _column) {
    final Boolean b = (Boolean) _value;
    selected_ = b.booleanValue();
  }
}