/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import org.apache.commons.collections4.Predicate;

/**
 * @author deniger
 */
public class PredicateFactory {

  private static class MovablePredicate implements Predicate {

    @Override
    public boolean evaluate(Object arg0) {
      return arg0 != null && ((EbliNode) arg0).getController().isMovable();
    }
  }

  private static class DuplicatablePredicate implements Predicate {

    @Override
    public boolean evaluate(Object arg0) {
      return arg0 != null && ((EbliNode) arg0).getController().isDuplicatable();
    }
  }

  private static class EditablePredicate implements Predicate {

    @Override
    public boolean evaluate(Object arg0) {
      return arg0 != null && ((EbliNode) arg0).getController().isEditable();
    }
  }

  private final static MovablePredicate MOVABLE = new MovablePredicate();
  private final static DuplicatablePredicate DUPLICATABLE = new DuplicatablePredicate();
  private final static EditablePredicate EDITABLE = new EditablePredicate();

  public static Predicate geMovablePredicate() {
    return MOVABLE;
  }

  public static Predicate geDuplicatablePredicate() {
    return DUPLICATABLE;
  }

  public static Predicate geEditablePredicate() {
    return EDITABLE;
  }
}
