package org.fudaa.ebli.visuallibrary;

import org.fudaa.ctulu.*;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetVueCalque;
import org.fudaa.ebli.visuallibrary.tree.EbliWidgetJXTreeTableModel;
import org.netbeans.api.visual.action.InplaceEditorProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.action.WidgetAction.Chain;
import org.netbeans.api.visual.graph.GraphScene;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.*;

/**
 * classe de gestion de la scene principale
 *
 * @author Adrien Hadoux
 */
public class EbliScene extends GraphScene<EbliNode, EbliEdge> implements CtuluImageProducer, EbliWidgetInterface<EbliSceneController> {
  
  int idx_ = 0;
  CtuluUI ctuluUi_;
  private EbliWidget  toEdit;

  /**
   * @return the ctuluUi
   */
  public CtuluUI getCtuluUi() {
    return ctuluUi_;
  }
  
  public void mouseClicked(MouseEvent e) {
    getView().requestFocusInWindow();
    ((MouseListener) getView()).mouseClicked(e);
  }

  public void mouseReleased(MouseEvent e) {
    ((MouseListener) getView()).mouseReleased(e);
  }

  /**
   * @param _ctuluUi the ctuluUi to set
   */
  public final void setCtuluUi(final CtuluUI _ctuluUi) {
    ctuluUi_ = _ctuluUi;
  }

  public void setWidgetToEdit(EbliWidget  toEdit) {
    this.toEdit=toEdit;
  }

  public EbliWidget getToEdit() {
    return toEdit;
  }

  public void clearEditContext(){
    toEdit=null;
  }

  private static class DelegateMouseListener implements MouseListener {

    private final MouseListener delegate;

    /**
     * @param delegate
     */
    private DelegateMouseListener(MouseListener delegate) {
      super();
      this.delegate = delegate;
    }

    /**
     * @param e
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent e) {
      delegate.mouseClicked(e);
    }

    /**
     * @param e
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseEntered(MouseEvent e) {
      delegate.mouseEntered(e);
    }

    /**
     * @param e
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }

    /**
     * @param e
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent e) {
      delegate.mousePressed(e);
    }

    /**
     * @param e
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e) {
      delegate.mouseReleased(e);
    }
  }
  
  @Override
  public JComponent createView() {
    JComponent res = super.createView();
    //to avoid add bugs with visual library: scrolling while the user exits the mouse from the scene
    MouseListener mouseListener = (MouseListener) res;
    res.removeMouseListener(mouseListener);
    res.addMouseListener(new DelegateMouseListener(mouseListener));
    return res;
  }
  
  public void clearSelection() {
    setSelectedObjects(Collections.EMPTY_SET);
  }
  
  public final Set<EbliNode> getSelectedNodes() {
    final Set init = getSelectedObjects();
    if (init == null) {
      return null;
    }
    final Set<EbliNode> res = new HashSet<EbliNode>(init.size());
    for (final Iterator it = init.iterator(); it.hasNext();) {
      res.add((EbliNode) it.next());
    }
    return res;
  }
  
  public void stopEditing() {
    final Set<?> objects2 = getSelectedObjects();
    if (objects2 != null) {
      for (final Iterator iterator = objects2.iterator(); iterator.hasNext();) {
        final Object object = iterator.next();
        stopEditing(findWidget(object));
        
      }
    }
  }
  
  protected void stopEditing(final Widget w) {
    if (w == null) {
      return;
    }
    final Chain actions = w.getActions();
    for (final WidgetAction widgetAction : actions.getActions()) {
      if (widgetAction instanceof InplaceEditorProvider.EditorController && ((InplaceEditorProvider.EditorController) widgetAction).isEditorVisible()) {
        ((InplaceEditorProvider.EditorController) widgetAction).closeEditor(true);
      }
    }
    for (final Widget widget : w.getChildren()) {
      stopEditing(widget);
    }
  }
  
  protected String generateId() {
    // return CtuluLibString.getString(idx_++);
    return CtuluLibGenerator.getInstance().deliverUniqueStringId();
  }
  
  public EbliWidget findById(final String _id) {
    final Set obj = getObjects();
    for (final Iterator it = obj.iterator(); it.hasNext();) {
      final EbliWidget w = (EbliWidget) findWidget(it.next());
      if (w.getIntern().getId().equals(_id)) {
        return w;
      }
      
    }
    return null;
  }
  
  public EbliNode findNodeById(final String _id) {
    final Set obj = getObjects();
    for (final Iterator it = obj.iterator(); it.hasNext();) {
      final EbliNode w = (EbliNode) it.next();
      if (w.getWidget() != null && w.getWidget().getIntern().getId().equals(_id)) {
        return w;
      }
    }
    return null;
  }
  /**
   * model de la scene pour l affichage de ses composants
   */
  EbliWidgetJXTreeTableModel treeModel_;
  final PropertyChangeSupport propertyChangeSupport_ = new PropertyChangeSupport(this);

  /**
   * @param _listener le listener: a chaque fois le parametre new correspond au widget modifie
   */
  public void addPropertyChangeListener(final PropertyChangeListener _listener) {
    propertyChangeSupport_.addPropertyChangeListener(_listener);
  }

  /**
   * @param _propertyName
   * @param _listener
   * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(java.lang.String, java.beans.PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String _propertyName, final PropertyChangeListener _listener) {
    propertyChangeSupport_.addPropertyChangeListener(_propertyName, _listener);
  }

  /**
   * @param _listener
   * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(java.beans.PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener _listener) {
    propertyChangeSupport_.removePropertyChangeListener(_listener);
  }
  
  public void firePropertyChange(final Object _nodeOrEdge, final String _property) {
    propertyChangeSupport_.firePropertyChange(_property, null, _nodeOrEdge);
  }
  
  public void firePropertyChange(final PropertyChangeEvent evt) {
    propertyChangeSupport_.firePropertyChange(evt);
  }
  
  public void firePropertyChange(final Object _nodeOrEdge, final String propertyName, final boolean newValue) {
    firePropertyChange(_nodeOrEdge, propertyName, Boolean.valueOf(!newValue), Boolean.valueOf(newValue));
    
  }
  
  public void firePropertyChange(final Object _nodeOrEdge, final String propertyName, final Object oldValue, final Object newValue) {
    PropertyChangeEvent event = new PropertyChangeEvent(this, propertyName, oldValue, newValue);
    event.setPropagationId(_nodeOrEdge);
    propertyChangeSupport_.firePropertyChange(event);
  }

  /**
   * @param _propertyName
   * @param _listener
   * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(java.lang.String, java.beans.PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String _propertyName, final PropertyChangeListener _listener) {
    propertyChangeSupport_.removePropertyChangeListener(_propertyName, _listener);
  }
  /**
   * controller de la scene qui gere les actions et les constructions graphiques
   */
  private final EbliSceneController controller_;
  
  public static void refreshScene(final Scene _sc) {
    _sc.validate();
    _sc.repaint();
    if (_sc.getView() != null) {
      _sc.getView().repaint();
    }
    
  }
  /**
   * Le gestionnaire de commande associe � la scene
   */
  private CtuluCommandManager cmdMng_;
  private final LayerWidget interactionLayer_;
  private final EbliPrintOverviewLayer printOverviewLayer;
  
  public EbliPrintOverviewLayer getPrintOverviewLayer() {
    return printOverviewLayer;
  }
  /**
   * la visu du graphscene
   */
  private LayerWidget visu_;
  private LayerWidget connexion;

  /**
   * la liste des listener associes a la scene
   */
  public EbliScene() {
    super();
    setLookFeel(new EbliLookFeel());
    // creation du layer de mise en page
    interactionLayer_ = new LayerWidget(this);
    connexion = new LayerWidget(this);
    printOverviewLayer = new EbliPrintOverviewLayer(this);
    addChild(interactionLayer_);
    addChild(printOverviewLayer);
    visu_ = new LayerWidget(this);
    addChild(visu_);
    addChild(connexion);

    // -- creation de son model tree --//
    treeModel_ = new EbliWidgetJXTreeTableModel(this);

    // -- creation de son controlleur --//
    controller_ = new EbliSceneController(this);
    
  }
  
  public LayerWidget getConnexion() {
    return connexion;
  }
  
  @Override
  public Dimension getDefaultImageDimension() {
    final Rectangle rec = visu_.getBounds();
    final Dimension d = new Dimension();
    d.height = rec.height;
    d.width = rec.width;
    return d;
  }
  
  @Override
  public BufferedImage produceImage(final Map _params) {
    final Dimension d = getDefaultImageDimension();
    return produceImage(d.width, d.height, _params);
  }
  private boolean isProducingImage;
  
  public void produceImage(Graphics2D g, final int _w, final int _h, final Map _params) {
    isProducingImage = true;
    boolean printOverviewVisible = printOverviewLayer.isVisible();
    if (printOverviewVisible) {
      printOverviewLayer.setVisible(false);
      refreshScene(this);
    }
    repaint();
    final Dimension d = getDefaultImageDimension();
    CtuluLibImage.setBestQuality(g);
    AffineTransform old = g.getTransform();
    if (_w != d.width || _h != d.height) {
      g.scale(CtuluLibImage.getRatio(_w, d.width), CtuluLibImage.getRatio(_h, d.height));
    }
    if (CtuluLibImage.mustFillBackground(_params)) {
      g.setPaint(getBackground());
      g.fillRect(0, 0, d.width, d.height);
    }
    stopEditing();
    Set<?> selectedObjects = getSelectedObjects();
    
    if (!CtuluLibArray.isEmpty(selectedObjects)) {
      selectedObjects = new HashSet(selectedObjects);
      for (Object object : selectedObjects) {
        Widget widget = findWidget(object);
        if (widget != null) {
          widget.setState(ObjectState.createNormal());
        }
      }
    }
    setSelectedObjects(Collections.EMPTY_SET);
    getView().printAll(g);
    // paint(g);
    isProducingImage = false;
    setSelectedObjects(selectedObjects);
    g.setTransform(old);
    printOverviewLayer.setVisible(printOverviewVisible);
    refreshScene(this);
    
  }
  
  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    isProducingImage = true;
    boolean printOverviewVisible = printOverviewLayer.isVisible();
    if (printOverviewVisible) {
      printOverviewLayer.setVisible(false);
      refreshScene(this);
    }
    final Dimension d = getDefaultImageDimension();
    final BufferedImage i = CtuluLibImage.createImage(_w, _h, _params);
    final Graphics2D g = i.createGraphics();
    CtuluLibImage.setBestQuality(g);
    if (_w != d.width || _h != d.height) {
      g.scale(CtuluLibImage.getRatio(_w, d.width), CtuluLibImage.getRatio(_h, d.height));
    }
    if (CtuluLibImage.mustFillBackground(_params)) {
      g.setPaint(getBackground());
      g.fillRect(0, 0, d.width, d.height);
    }
    stopEditing();
    Set<?> selectedObjects = getSelectedObjects();
    
    if (!CtuluLibArray.isEmpty(selectedObjects)) {
      selectedObjects = new HashSet(selectedObjects);
      for (Object object : selectedObjects) {
        Widget widget = findWidget(object);
        if (widget != null) {
          widget.setState(ObjectState.createNormal());
        }
      }
    }
    setSelectedObjects(Collections.EMPTY_SET);
    getView().printAll(g);
    // paint(g);
    isProducingImage = false;
    setSelectedObjects(selectedObjects);
    g.dispose();
    i.flush();
    printOverviewLayer.setVisible(printOverviewVisible);
    refreshScene(this);
    return i;
    
  }
  
  @Override
  protected void attachEdgeSourceAnchor(final EbliEdge edge, final EbliNode oldSourceNode, final EbliNode sourceNode) {
    
  }
  
  @Override
  protected void attachEdgeTargetAnchor(final EbliEdge edge, final EbliNode oldTargetNode, final EbliNode targetNode) {
    
  }
  
  @Override
  protected Widget attachEdgeWidget(final EbliEdge edge) {
    return null;
  }
  
  @Override
  protected void detachNodeWidget(final EbliNode node, final Widget widget) {
    super.detachNodeWidget(node, widget);
    node.getCreator().widgetDetached();
  }
  
  @Override
  protected Widget attachNodeWidget(final EbliNode node) {
    EbliWidget widget = null;
    // -- creation de la widget ici --//
    if (!node.getCreator().isWidgetCreated()) {
      // -- on ne cree la widget que si necessaire
      node.getCreator().create(this);
      widget = node.getWidget();
      if (widget == null) {
        return null;
      }
      if (node.getCreator().getInitPreferredLocation() != null) {
        widget.setPreferredLocation(node.getCreator().getInitPreferredLocation());
      }
      if (node.getCreator().getCurrentPreferredSize() != null) {
        widget.setPreferredSize(node.getCreator().getCurrentPreferredSize());
      }
    } else {
      widget = node.getCreator().getWidget();
    }
    
    if (widget == null) {
      return null;
    }
    widget.getController().setNode(node);
    node.getCreator().widgetAttached();
    // -- ajout dans la vue layer --//
    visu_.addChild(node.getWidget());
    node.getCreator().widgetAddedInView();
    node.getWidget().setToolTipText(node.getDescription());
    return node.getWidget();
  }

  /**
   * @return the cmdMng
   */
  public CtuluCommandManager getCmdMng() {
    return cmdMng_;
  }
  
  public LayerWidget getLayerVisu() {
    return visu_;
  }
  
  public LayerWidget getVisu() {
    return visu_;
  }
  
  @Override
  protected void notifyNodeAdded(final EbliNode node, final Widget widget) {
    if (widget instanceof EbliWidget) {
      ((EbliWidget) widget).notifyNodeAdded(node);
    }
  }

  /**
   * Methode qui permet de raffraichir les elements de la scene
   */
  public void refresh() {
    refreshScene(this);
  }

  /**
   * @param _cmdMng the cmdMng to set
   */
  public void setCmdMng(final CtuluCommandManager _cmdMng) {
    cmdMng_ = _cmdMng;
  }
  
  public void setVisuLayer(final LayerWidget visu_) {
    this.visu_ = visu_;
  }
  
  public LayerWidget getInteractionLayer() {
    return interactionLayer_;
  }
  
  @Override
  public EbliSceneController getController() {
    return controller_;
  }
  
  public EbliWidgetJXTreeTableModel getTreeModel() {
    return treeModel_;
  }
  
  public List<EbliWidget> getAllVue2d() {
    
    final List<EbliWidget> liste = new ArrayList<EbliWidget>();
    
    for (final Object objet : this.getObjects()) {
      if (objet instanceof EbliNode) {
        final EbliNode cible = (EbliNode) objet;
        
        if (cible.getWidget().getIntern() instanceof EbliWidgetVueCalque) {
          liste.add(cible.getWidget().getIntern());
        }
        
      }
    }
    return liste;
    
  }

  /**
   * Methode qui redimensionne proportionellement les widget � la scene
   *
   */
  public void resizeInnerWidget(final Dimension newSize, final Dimension oldSize) {
    if (getNodes().size() == 0) {
      return;
    }
    for (final Iterator<EbliNode> it2 = getNodes().iterator(); it2.hasNext();) {
      
      final EbliNode currentNode = it2.next();
      if (currentNode != null && currentNode.hasWidget()) {
        
        final Rectangle oldRectangle = currentNode.getWidget().getPreferredBounds();
        
        final double proportionW = newSize.width * oldRectangle.width / oldSize.width;
        final double proportionH = newSize.width * oldRectangle.height / oldSize.height;

        // -- retaillage --//
        currentNode.getWidget().setPreferredBounds(new Rectangle(new Dimension((int) proportionW, (int) proportionH)));

        // -- rafraichissement de la scene --//
        this.refresh();
        
      }
    }

    // this.setPreferredSize(newSize);

  }
  
  public void moveWidgetTo(final EbliWidget widget, final int position) {

    // -- ne marche pas.......................... --//

    //	  
    // if (widget.getParentWidget() == null)
    // return;
    // List<Widget> children = getChildren();
    // int i = children.indexOf (widget);
    // if (i < 0)
    // return;
    // children.remove (i);
    // children.add (position, widget);
    // widget.revalidate ();
    // widget.getParentWidget().revalidate ();
    // refresh();

  }

  /**
   * Place la widget en fin de liste: le premier plan:la fin de la liste children l'arriere plan: le debut de la liste children
   *
   * @param widget
   */
  public void bringToFirst(final EbliWidget widget) {
    bringToLast(widget, cmdMng_);
  }
  
  public void bringToLast(final EbliWidget widget, final CtuluCommandContainer container) {
    final Widget layer;
    final EbliWidget parent = widget.getParentBordure();
    // -- cas particulier pour les groupes--//
    if (parent.getParentWidget() instanceof EbliWidgetGroup) {
      layer = parent.getParentWidget();
    } else {
      layer = visu_;
    }
    
    final List<Widget> children = layer.getChildren();
    final int position = children.indexOf(parent);
    layer.removeChild(parent);
    layer.addChild(children.size(), parent);
    widget.revalidate();
    this.refresh();
    // -- notify le modele: redessine toi ! --//
    treeModel_.nodeMoved(parent);
    if (container != null) {
      
      container.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          layer.removeChild(parent);
          layer.addChild(children.size(), parent);
          widget.revalidate();
          refresh();
          treeModel_.nodeMoved(parent);
        }
        
        @Override
        public void undo() {
          layer.removeChild(parent);
          layer.addChild(position, parent);
          widget.revalidate();
          refresh();
          treeModel_.nodeMoved(parent);
        }
      });
    }
  }
  
  public void bringUp(final EbliWidget widget) {
    bringUp(widget, cmdMng_);
  }

  /**
   * Augmente la position de la widget d'un cran: le premier plan:la fin de la liste children l'arriere plan: le debut de la liste children
   *
   * @param widget
   */
  public void bringUp(final EbliWidget widget, final CtuluCommandContainer container) {
    
    Widget layer = visu_;
    final EbliWidget parent = widget.getParentBordure();
    // -- cas particulier pour les groupes--//
    if (parent.getParentWidget() instanceof EbliWidgetGroup) {
      layer = parent.getParentWidget();
    }
    final List<Widget> children = layer.getChildren();
    
    final int i = children.indexOf(parent);
    if (i < 0 || i == children.size() - 1) {
      return;
    }
    layer.removeChild(parent);
    layer.addChild(i + 1, parent);
    widget.revalidate();
    this.refresh();
    // -- notify le modele: redessine toi ! --//
    treeModel_.nodeMoved(parent);
    
    container.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        bringUp(widget);
      }
      
      @Override
      public void undo() {
        
        bringDown(widget);
      }
    });
    
  }
  
  public void bringDown(final EbliWidget widget) {
    bringDown(widget, cmdMng_);
  }

  /**
   * Diminue la position de la widget d'un cran: le premier plan:la fin de la liste children l'arriere plan: le debut de la liste children
   *
   * @param widget
   */
  public void bringDown(final EbliWidget widget, final CtuluCommandContainer container) {
    Widget layer = visu_;
    final EbliWidget parent = widget.getParentBordure();
    // -- cas particulier pour les groupes--//
    if (parent.getParentWidget() instanceof EbliWidgetGroup) {
      layer = parent.getParentWidget();
    }
    final List<Widget> children = layer.getChildren();
    
    final int i = children.indexOf(parent);
    if (i <= 0) {
      return;
    }
    layer.removeChild(parent);
    layer.addChild(i - 1, parent);
    widget.revalidate();
    this.refresh();
    // -- notify le modele: redessine toi ! --//
    treeModel_.nodeMoved(parent);
    
    container.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        bringDown(widget);
      }
      
      @Override
      public void undo() {
        bringUp(widget);
      }
    });
    
  }
  
  public void bringToLast(final EbliWidget widget) {
    bringToFirst(widget, cmdMng_);
  }

  /**
   * Place la widget en debut de liste: le premier plan:la fin de la liste children l'arriere plan: le debut de la liste children
   *
   * @param widget
   */
  public void bringToFirst(final EbliWidget widget, final CtuluCommandContainer container) {
    final Widget layer;
    final EbliWidget parent = widget.getParentBordure();
    // -- cas particulier pour les groupes--//
    if (parent.getParentWidget() instanceof EbliWidgetGroup) {
      layer = parent.getParentWidget();
    } else {
      layer = visu_;
    }
    final List<Widget> children = layer.getChildren();
    final int position = children.indexOf(parent);
    
    layer.removeChild(parent);
    layer.addChild(0, parent);
    widget.revalidate();
    this.refresh();
    // -- notify le modele: redessine toi ! --//
    treeModel_.nodeMoved(parent);
    
    container.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        layer.removeChild(parent);
        layer.addChild(0, parent);
        widget.revalidate();
        refresh();
        treeModel_.nodeMoved(parent);
      }
      
      @Override
      public void undo() {
        layer.removeChild(parent);
        layer.addChild(position, parent);
        widget.revalidate();
        refresh();
        treeModel_.nodeMoved(parent);
      }
    });
    
  }

  /**
   * @return the isProducingImage
   */
  public boolean isProducingImage() {
    return isProducingImage;
  }
}
