package org.fudaa.ebli.visuallibrary.calque;

import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationAdapterInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.ZCalqueGrille;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetImageProducer;
import org.fudaa.ebli.visuallibrary.animation.EbliWidgetAnimatedItem;
import org.netbeans.api.visual.action.InplaceEditorProvider;
import org.netbeans.api.visual.widget.Widget;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.*;

/**
 * Classe permettant d'afficher un calque en tant que Widget
 *
 * @author deniger
 */
public class EbliWidgetVueCalque extends EbliWidget implements
/* EditProvider */InplaceEditorProvider<BuPanel>, EbliWidgetImageProducer {
  public static final String CLEAR_CACHE = "clearCache";
  ZEbliCalquesPanel calquePanel_;
  Window frame_;
  BufferedImage image_;
  BuPanel conteneurEditor;
  GrBoite zoom_;

  public EbliWidgetVueCalque(final EbliScene _scene, final ZEbliCalquesPanel _vue) {
    this(_scene, _vue, null);
  }

  public EbliWidgetVueCalque(final EbliScene _scene, final ZEbliCalquesPanel _vue, final GrBoite _initZoom) {
    super(_scene);
    calquePanel_ = _vue;
    calquePanel_.getVueCalque().setCheckBoite(false);
    zoom_ = _initZoom;
    calquePanel_.setBorder(null);

    setPreferredSize(new Dimension(500, 400));
    initSize(new Rectangle(0, 0, 500, 400));
    calquePanel_.getVueCalque().addPropertyChangeListener("repere", arg0 -> {
      final boolean mustRefresh = (image_ != null);
      updateZoomFromPanel();
      if (mustRefresh) {
        getEbliScene().refresh();
      }
    });
    calquePanel_.getArbreCalqueModel().getObservable().addObserver((_o, _arg) -> {
      if (!isInEditMode() && ("t".equals(_arg) || CLEAR_CACHE.equals(_arg))) {
        clearCacheImage();
      }
    });
  }

  @Override
  public BufferedImage produceImage(int width, int height, Map params) {
    return calquePanel_.produceImage(width, height, params);
  }

  public ZEbliCalquesPanel getCalquePanel() {
    return calquePanel_;
  }

  @Override
  public BuPanel createEditorComponent(final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget) {
    return calquePanel_;
  }

  /**
   * appel� lors de la fermeture de l editeur. Il faut redessiner la widget.
   */
  @Override
  public void notifyClosing(final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget,
                            final BuPanel editor, final boolean commit) {
    editingStop(editor);
    updateZoomFromPanel();
    // le zoom n'a pas chang�.
    zoomChanged_ = false;
    Widget parentWidget = getGroup();

    if (parentWidget instanceof EbliWidgetFusionCalques) {
      ((EbliWidgetFusionCalques) parentWidget).resynchronizeAfterEdition(this);
      ((EbliWidgetFusionCalques) parentWidget).repaintAll(false);
    }
    getEbliScene().refresh();
    getEbliScene().repaint();
  }

  private void updateZoomFromPanel() {
    setZoom(calquePanel_.getVueCalque().getViewBoite());
  }

  /**
   * @return une interface non null si la widget peut etre animee
   */
  @Override
  public EbliAnimatedInterface getAnimatedInterface() {
    if (calquePanel_ instanceof EbliAnimatedInterface) {
      return (EbliAnimatedInterface) calquePanel_;
    }
    return null;
  }

  @Override
  public Color getColorFond() {
    return null;
  }

  private void initSize(final Rectangle rec) {
    calquePanel_.setSize(rec.width, rec.height);
    calquePanel_.setPreferredSize(new Dimension(rec.width, rec.height));
    final BCalque[] tousCalques = calquePanel_.getVueCalque().getCalque().getTousCalques();
    calquePanel_.getVueCalque().setSize(rec.width, rec.height);
    // calquePanel_.setPreferredSize(new Dimension(rec.width, rec.height));
    calquePanel_.getVueCalque().getCalque().setSize(rec.width, rec.height);
    for (int i = 0; i < tousCalques.length; i++) {
      tousCalques[i].setSize(rec.width, rec.height);
    }
  }

  public GrBoite getViewBoite() {
    return calquePanel_.getVueCalque().getViewBoite();
  }

  public EbliWidgetControllerCalque getCalqueController() {
    return (EbliWidgetControllerCalque) getController();
  }

  boolean zoomChanged_ = true;
  boolean first = true;

  @Override
  protected void paintWidget() {
    //in edition mode we do nothing: the component is rendering.
    if (isInEditMode()) {
      return;
    }

    final Rectangle rec = getClientArea();
    if (rec.width > 0 && rec.height > 0) {
      final Graphics2D g = getGraphics();
      initSize(rec);
      // mode edition
      final boolean sizeChanged = image_ != null && (image_.getWidth() != rec.width || image_.getHeight() != rec.height);
      if (image_ == null || sizeChanged) {
        FuLog.debug("EWI: recreate image");

        // on doit retailler la vue changement de taille.
        if (sizeChanged || zoomChanged_) {
          zoomChanged_ = false;
          applyZoom();
        }

        final Map params = new HashMap();
        CtuluLibImage.setCompatibleImageAsked(params);
        // -- on redessinne --//
        // if we didnt saved the insets we have to paint two times the view 2d...
        if (first && calquePanel_.getVueCalque().getUserInsets() == null) {
          first = false;
          final ZCalqueGrille grille = (ZCalqueGrille) calquePanel_.getCqInfos().getCalqueParNom("cqGrille");
          if (grille != null && grille.isAxisPainted()) {
            calquePanel_.produceImage(rec.width, rec.height, params);
          }
        }
        image_ = calquePanel_.produceImage(rec.width, rec.height, params);
      }

      g.drawImage(image_, rec.x, rec.y, rec.width, rec.height, null);
    }
  }

  private void applyZoom() {
    if (!isInEditMode() && zoom_ != null && zoom_.getDeltaX() > 0 && zoom_.getDeltaY() > 0) {
      calquePanel_.getVueCalque().setViewBoite(zoom_);
    } else {
      calquePanel_.restaurer();
    }
  }

  @Override
  public EnumSet<org.netbeans.api.visual.action.InplaceEditorProvider.ExpansionDirection> getExpansionDirections(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget, final BuPanel editor) {
    return null;
  }

  @Override
  public Rectangle getInitialEditorComponentBounds(final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller,
                                                   final Widget widget, final BuPanel editor, final Rectangle viewBounds) {
    return null;// convertLocalToScene(getClientArea());
  }

  @Override
  public void notifyOpened(final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget,
                           final BuPanel editor) {
    editingStart(editor);
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canColorForeground() {
    return false;
  }

  @Override
  public boolean canColorBackground() {
    return false;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  public boolean canFont() {
    return false;
  }

  @Override
  public List<EbliWidgetAnimatedItem> getAnimatedItems() {
    final BCalque[] cqs = calquePanel_.getVueCalque().getCalque().getTousCalques();
    final List<EbliWidgetAnimatedItem> res = new ArrayList<EbliWidgetAnimatedItem>(20);
    for (int i = 0; i < cqs.length; i++) {
      if (cqs[i].isVisible() && cqs[i] instanceof EbliAnimationAdapterInterface) {
        res.add(new EbliWidgetAnimatedItem((EbliAnimationAdapterInterface) cqs[i], cqs[i], getId(), cqs[i].getName(), cqs[i].getTitle()));
      }
    }
    return res;
  }

  @Override
  public boolean isAnimatedItemAlive(final String _id) {
    final BCalque cq = calquePanel_.getVueCalque().getCalque().getCalqueParNom(_id);
    return cq != null && cq.isVisible();
  }

  Map<BCalqueAffichage, EbliWidgetCalqueLegende> legends = new HashMap<BCalqueAffichage, EbliWidgetCalqueLegende>();

  @Override
  public void addSatellite(final EbliWidget w) {
    super.addSatellite(w);
    EbliWidget intern = w.getIntern();
    if (intern instanceof EbliWidgetCalqueLegende) {
      legends.put(((EbliWidgetCalqueLegende) intern).getCalque(), (EbliWidgetCalqueLegende) intern);
    } else if (intern instanceof EbliWidgetTimeLegende) {
      timeLegende = (EbliWidgetTimeLegende) intern;
    }
  }

  EbliWidgetCalqueLegende getLegendFor(final BCalque cq) {
    return legends.get(cq);
  }

  EbliWidgetTimeLegende timeLegende;

  EbliWidgetTimeLegende getTimeLegend() {
    return timeLegende;
  }

  public boolean isTimeLegendVisible() {
    return timeLegende != null && timeLegende.isVisible();
  }

  @Override
  public void removeSatellite(final EbliWidget w) {
    super.removeSatellite(w);
    EbliWidget intern = w.getIntern();
    if (intern instanceof EbliWidgetCalqueLegende) {
      final BCalqueAffichage layer = ((EbliWidgetCalqueLegende) intern).getCalque();
      legends.remove(layer);
      //if the user remove explicitely a legend, it won't be automatically added next time.
      EbliWidgetControllerCalque.unsetLegendAddedIfVisible(layer);
    } else if (intern instanceof EbliWidgetTimeLegende) {
      timeLegende = null;
    }
  }

  protected void setZoom(final GrBoite _zoom) {
    if (CtuluLib.isEquals(_zoom, zoom_)) {
      return;
    }
    clearCacheImage();
    this.zoom_ = _zoom;
    zoomChanged_ = true;
  }

  /**
   * Nettoie l'image utilise pour sauvegarder l'affichage
   */
  public void clearCacheImage() {
    image_ = null;
  }
}
