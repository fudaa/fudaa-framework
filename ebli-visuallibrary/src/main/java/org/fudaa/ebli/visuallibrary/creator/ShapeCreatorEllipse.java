package org.fudaa.ebli.visuallibrary.creator;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public class ShapeCreatorEllipse implements ShapeCreator {

  @Override
  public Shape createShapeFor(final Rectangle2D.Float rec, final Map options, final float largeurTrait) {
    return new Ellipse2D.Float(rec.x, rec.y, rec.width, rec.height);
  }
}
