package org.fudaa.ebli.visuallibrary.actions;

import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

/**
 * commande qui gere le undo/redo en cas de masquage des widgets
 * 
 * @author Adrien Hadoux
 */
public class CommandChangeVisibility implements CtuluCommand {

  private boolean newValue_;
  private Map<EbliWidget, Boolean> oldValues;

  public CommandChangeVisibility(Map<EbliWidget, Boolean> oldValues, final boolean visible) {
    super();
    this.oldValues = oldValues;
    newValue_ = visible;
  }

  @Override
  public void undo() {
    EbliScene scene = null;
    for (Map.Entry<EbliWidget, Boolean> entry : oldValues.entrySet()) {
      entry.getKey().setVisible(entry.getValue());
      entry.getKey().getEbliScene().firePropertyChange(entry.getKey(), BSelecteurCheckBox.PROP_VISIBLE);
      scene = entry.getKey().getEbliScene();
    }
    if (scene != null) {
      scene.refresh();
    }
  }

  @Override
  public void redo() {
    EbliScene scene = null;
    for (EbliWidget widget : oldValues.keySet()) {
      widget.setVisible(newValue_);
      widget.getEbliScene().firePropertyChange(widget, BSelecteurCheckBox.PROP_VISIBLE);
      scene = widget.getEbliScene();
    }
    if (scene != null) {
      scene.refresh();
    }
  }

}
