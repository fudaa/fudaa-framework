package org.fudaa.ebli.visuallibrary.calque;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BCalqueLegendePanel;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZCalqueFlecheScaleSection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.ebli.palette.PaletteSelecteurCouleurPlage;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.EditProvider;
import org.netbeans.api.visual.action.InplaceEditorProvider.EditorController;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.layout.LayoutFactory.SerialAlignment;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.ComponentWidget;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.LabelWidget.Alignment;
import org.netbeans.api.visual.widget.LabelWidget.VerticalAlignment;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.modules.visual.layout.FlowLayout;

/**
 * Creation de la classe widget specifique au calque. Permet de gerer le resize via le changement de font
 */
public class EbliWidgetCalqueLegende extends EbliWidget implements EditProvider, PropertyChangeListener {

  Font boldFont_;
  BCalqueLegendePanel legendePanel_;

  Widget legendsWidget_;

  boolean maj = true;

  boolean mustUpdateFont_ = false;

  List<EbliWidgetPlage> plagesWithTreeParts;
  List<EbliWidgetPlage> plagesWithSingleParts;

  @SuppressWarnings("unchecked")
  public EbliWidgetCalqueLegende(final EbliScene _scene, final boolean _controller,
      final BCalqueLegendePanel _legendePanel) {
    super(_scene, _controller);

    setCheckClipping(true);
    // -- la legende est un listener des modifs de son calque --//
    legendePanel_ = _legendePanel;
    setEnabled(true);
    // -- creation de l action edition des labels --//

    // -- ajout de l action au label correspondant --//
    this.getActions().addAction(ActionFactory.createEditAction(this));
  }

  EbliWidgetFlechePlage fleche;

  protected void addListeners() {
    BCalqueAffichage calque = legendePanel_.getCalque();
    calque.addPropertyChangeListener("paletteCouleur", this);
    calque.addPropertyChangeListener("paletteTitle", this);
  }

  protected void removeListeners() {
    legendePanel_.getCalque().removePropertyChangeListener("paletteCouleur", this);
    legendePanel_.getCalque().removePropertyChangeListener("paletteTitle", this);
    if (fleche != null) {
      fleche.removeListeners();
    }

  }

  @Override
  public boolean canColorBackground() {
    return true;
  }

  public BCalqueAffichage getCalque() {
    return legendePanel_.getCalque();
  }

  @Override
  public boolean canColorForeground() {
    return false;
  }

  @Override
  public boolean canFont() {
    return true;
  }

  @Override
  public boolean canRotate() {
    return false;
  }

  @Override
  public boolean canTraceLigneModel() {
    return false;
  }

  @Override
  public void edit(Widget widget) {
    if (PaletteSelecteurCouleurPlage.isTargetValid(legendePanel_.getCalque())) {
      JDialog dialog = new JDialog(CtuluLibSwing.getActiveWindow());
      JComponent createPanel = createPanel(dialog);
      dialog.setContentPane(createPanel);
      dialog.setModal(true);
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.pack();
      dialog.setLocationRelativeTo(CtuluLibSwing.getActiveWindow());
      dialog.setVisible(true);
      dialog.dispose();
    }

  }

  private JComponent createPanel(final JDialog parent) {
    final PaletteSelecteurCouleurPlage palette = new PaletteSelecteurCouleurPlage();
    BuButton bt = new BuButton(BuResource.BU.getIcon("crystal_fermer"), EbliLib.getS("Fermer"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.setVisible(false);
        parent.dispose();
      }
    });
    palette.addButton(bt);
    palette.setPalettePanelTarget(legendePanel_.getCalque());
    if (legendePanel_.getCalque() instanceof ZCalqueFleche) {
      ZCalqueFleche calqueFleche = (ZCalqueFleche) legendePanel_.getCalque();
      ZCalqueFlecheScaleSection section = new ZCalqueFlecheScaleSection(calqueFleche);
      BSelecteurInterface createLegendSelecteur = section.createLegendSelecteur(false);
      JComponent[] components = createLegendSelecteur.getComponents();
      createLegendSelecteur.setSelecteurTarget(section);
      BuPanel pn = new BuPanel(new BorderLayout());
      JComponent jComponent = components[0];
      JPanel leg = new JPanel(new BorderLayout());
      leg.setBorder(javax.swing.BorderFactory.createTitledBorder(EbliLib.getS("Echelle des vecteurs")));
      leg.add(jComponent);
      pn.add(leg, BorderLayout.NORTH);
      pn.add(palette);
      return pn;

    }
    return palette;
  }


  public void notifyClosing(final EditorController _controller, final Widget _widget, final JComponent _editor,
      final boolean _commit) {
    editingStop(_editor);

  }

  public void notifyOpened(final EditorController _controller, final Widget _widget, final JComponent _editor) {
    editingStart(_editor);

  }

  @Override
  protected void notifyRemoved() {
    legendePanel_.getCalque().removePropertyChangeListener("paletteCouleur", this);
    legendePanel_.getCalque().removePropertyChangeListener("paletteTitle", this);
  }

  @Override
  public void notifyStateChanged(final ObjectState _previousState, final ObjectState _newState) {
    // super.notifyStateChanged(_previousState, _newState);
  }

  @Override
  protected Rectangle calculateClientArea() {
    updatePlagesWidth();
    return super.calculateClientArea();
  }

  private void updatePlagesWidth() {
    int maxSingle = 0;
    if (plagesWithSingleParts != null) {
      for (EbliWidgetPlage widget : plagesWithSingleParts) {
        maxSingle = Math.max(maxSingle, widget.getSingleWidth());
      }
    }
    if (plagesWithTreeParts != null) {
      int maxLeft = 0;
      int maxMiddle = 0;
      int maxRight = 0;
      for (EbliWidgetPlage widget : plagesWithTreeParts) {
        maxLeft = Math.max(maxLeft, widget.calculateNormalWidth(SwingConstants.LEFT));
        maxMiddle = Math.max(maxMiddle, widget.calculateNormalWidth(SwingConstants.CENTER));
        maxRight = Math.max(maxRight, widget.calculateNormalWidth(SwingConstants.RIGHT));
      }
      int totalWidth = maxLeft + maxMiddle + maxRight + EbliWidgetPlage.getGapBetweenParts();
      if (totalWidth > maxSingle) {
        maxSingle = totalWidth;
      } else {
        maxLeft += maxSingle - totalWidth;
      }
      for (EbliWidgetPlage widget : plagesWithTreeParts) {
        widget.setWidth(SwingConstants.LEFT, maxLeft);
        widget.setWidth(SwingConstants.CENTER, maxMiddle);
        widget.setWidth(SwingConstants.RIGHT, maxRight);
        widget.revalidate();

      }
    }
    if (plagesWithSingleParts != null) {
      for (EbliWidgetPlage widget : plagesWithSingleParts) {
        widget.setSingleLabelWidth(maxSingle);
      }
    }
  }

  @Override
  protected void paintWidget() {
    if (mustUpdateFont_) {
      updateFont(getFormeFont());
    }
    super.paintWidget();
  }

  /**
   * Si la palette est modifiee on change la widget
   */
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    rebuildWidget();

  }

  private void rebuildWidget() {
    Font oldFond = super.getFormeFont();
    this.removeChildren();
    plagesWithSingleParts = null;
    plagesWithTreeParts = null;
    reconstructWidget();
    updateLegendView();
    setFormeFont(oldFond);
  }

  @Override
  public void setFormeFont(final Font _newFont) {
    super.setFormeFont(_newFont);
    updateFont(_newFont);
  }

  /**
   * @param _legendsWidget the legendsWidget to set
   */
  public void setLegendsWidget(final Widget _legendsWidget) {
    legendsWidget_ = _legendsWidget;
  }

  @Override
  public void setPropertyCmd(final String _key, final Object _prop, final CtuluCommandContainer _cmd) {
    super.setPropertyCmd(_key, _prop, _cmd);
    if ("font".equals(_key)) {
      final Font ft = (Font) _prop;
      updateFont(ft);
    }

  }

  protected void updateFont(final Font ft) {
    if (getGraphics() == null) {
      mustUpdateFont_ = true;
      return;
    }
    mustUpdateFont_ = false;
    boldFont_ = BuLib.deriveFont(ft, Font.BOLD, 1);
    if (legendsWidget_ != null) {
      final List<Widget> listePlage = legendsWidget_.getChildren();
      for (final Iterator<Widget> it = listePlage.iterator(); it.hasNext();) {
        final Widget widget = it.next();
        // -- test si il s agit d un widget plage --//
        if (widget instanceof EbliWidgetPlage) {
          final EbliWidgetPlage widgetPlage = (EbliWidgetPlage) widget;
          widgetPlage.updateFont(ft);
        }
      }
    }

    // -- mise a jour de la widget avec la nouvelle font
    final List<Widget> listePlage = getChildren();
    for (final Iterator<Widget> it = listePlage.iterator(); it.hasNext();) {
      final Widget widget = it.next();
      if (widget instanceof LabelWidget && (((LabelWidget) widget).getLabel() != null)) {
        final LabelWidget lbWidget = (LabelWidget) widget;
        lbWidget.setFont(boldFont_);
      }

    }
    updateLegendView();
  }

  private void updateLegendView() {
    // pour ne plus prendre en compte la taille sett�e par l'utilisateur.
    //    getParentWidget().setPreferredBounds(null);
    updatePlagesWidth();
    revalidate();
    getEbliScene().refresh();
  }

  public void reconstructWidget() {
    updateLegendeWidget();
    getParentWidget().revalidate();

  }

  /**
   * Mananger statique qui se charge de creer �a widget associee au calque.
   */
  public void updateLegendeWidget() {
    final EbliScene _scene = getEbliScene();
    // --creation de la widget conteneur --//
    BPalettePlageInterface listeplages = null;
    removeChildren();
    setLayout(new FlowLayout(true, SerialAlignment.CENTER, 5));
    setBorder(BorderFactory.createEmptyBorder(5));
    plagesWithTreeParts = null;
    plagesWithSingleParts = null;
    if (legendePanel_ != null) {
      boolean isFleche = legendePanel_.getCalque() instanceof ZCalqueFleche;
      final Component[] listeC = legendePanel_.getComponents();

      for (int i = 0; i < listeC.length; i++) {
        final Component c = listeC[i];
        if (c instanceof JLabel && !"TITLE".equals(((JComponent) c).getClientProperty("legend.type"))) {

          final LabelWidget titre = new LabelWidget(_scene);
          titre.setAlignment(Alignment.CENTER);
          titre.setVerticalAlignment(VerticalAlignment.CENTER);
          titre.setLabel(((JLabel) c).getText());
          titre.setEnabled(true);
          titre.setFont(c.getFont());
          addChild(titre);
        }

        if (c instanceof BPalettePlageLegende) {

          final BPalettePlageLegende pal = (BPalettePlageLegende) c;

          // --recuperation de la liste des plages --//
          listeplages = pal.getModel();
          if (listeplages != null) {
            // --creation du titre --//
            LabelWidget titre = new LabelWidget(_scene);
            titre.setAlignment(Alignment.CENTER);
            titre.setVerticalAlignment(VerticalAlignment.CENTER);
            titre.setLabel(listeplages.getTitre());
            titre.setFont(pal.getFont());
            titre.setEnabled(true);
            addChild(titre);
            final LabelWidget sstitre = new LabelWidget(_scene);
            sstitre.setAlignment(Alignment.CENTER);
            titre.setFont(pal.getFont());
            sstitre.setVerticalAlignment(VerticalAlignment.CENTER);
            sstitre.setLabel(listeplages.getSousTitre());
            sstitre.setEnabled(true);
            sstitre.setVisible(listeplages.isSubTitleVisible());
            setSubTitleWidget(sstitre, listeplages.getSubTitleLabel());
            addChild(sstitre);
            if (isFleche) {
              if (fleche == null) {
                fleche = new EbliWidgetFlechePlage(_scene, (ZCalqueFleche) legendePanel_.getCalque());
              }
              addChild(fleche);
            }
            final Widget ws = new Widget(_scene);
            ws.setLayout(new FlowLayout(true, SerialAlignment.LEFT_TOP, 2));
            int nbPlages = listeplages.getNbPlages();

            if (nbPlages != 0) {
              plagesWithTreeParts = new ArrayList<EbliWidgetPlage>(nbPlages);
              plagesWithSingleParts = new ArrayList<EbliWidgetPlage>(nbPlages);
              if (listeplages.isReduit()) {
                JComponent createReduit = listeplages.createReduit();
                ws.addChild(new ComponentWidget(_scene, createReduit));

              } else {
                // pour l'alignement des labels
              
                for (int j = nbPlages - 1; j >= 0; j--) {

                  final BPlageInterface plage = listeplages.getPlageInterface(j);

                  // -- ajout d'une widget plage --//
                  final EbliWidgetPlage lw = new EbliWidgetPlage(_scene, plage);
                  lw.updateFont(pal.getFont());

                  lw.setUseBorder(false);
                  lw.setEnabled(true);
                  if (lw.isThreePart()) {
                    plagesWithTreeParts.add(lw);
                  } else {
                    plagesWithSingleParts.add(lw);
                  }
                  ws.addChild(lw);
                }

              }
            } else if (!isFleche) {
              // liste des plages est vide il s agit d une autre legende
              final Component[] lcomp = pal.getComponents();
              for (int j = 0; j < lcomp.length; j++) {
                final Component co = lcomp[j];

                if (co instanceof JPanel) {
                  final JPanel panel = (JPanel) co;

                  for (int k = 0; k < panel.getComponents().length; k++) {

                    if (panel.getComponents()[k] instanceof JLabel) {
                      titre = new LabelWidget(_scene);
                      titre.setAlignment(Alignment.CENTER);
                      titre.setVerticalAlignment(VerticalAlignment.CENTER);
                      titre.setLabel(((JLabel) panel.getComponents()[k]).getText());
                      titre.setEnabled(true);
                      ws.addChild(titre);
                    } else if (panel.getComponents()[k] instanceof JList) {
                      final JList jlst = (JList) panel.getComponents()[k];
                      for (int g = 0; g < jlst.getModel().getSize(); g++) {
                        titre = new LabelWidget(_scene);
                        titre.setAlignment(Alignment.CENTER);
                        titre.setVerticalAlignment(VerticalAlignment.CENTER);
                        titre.setLabel((String) jlst.getModel().getElementAt(g));
                        titre.setEnabled(true);
                        ws.addChild(titre);
                      }

                    }

                  }

                }

              }

            }
            addChild(ws);
            setLegendsWidget(ws);
          }// fin du if listeplage==null
        }
      }
      // -- prendre en compte le resize automatique --//
      getEbliScene().refresh();
    } else {
      FuLog.warning("createLegendeWidget retourne widget null");
    }
  }

  LabelWidget subTitleWidget;
  String subTitleLabel;

  public String getSubTitleLabel() {
    return subTitleLabel;
  }

  private void setSubTitleWidget(LabelWidget sstitre, String label) {
    this.subTitleWidget = sstitre;
    subTitleLabel = label;

  }

  public boolean isSubTitleVisible() {
    return subTitleWidget != null && subTitleWidget.isVisible();
  }

  public void setSubTitleVisible(boolean visible) {
    if (subTitleWidget == null || visible == isSubTitleVisible()) { return; }
    if (PaletteSelecteurCouleurPlage.isTargetValid(legendePanel_.getCalque())) {
      BPalettePlageTarget target = (BPalettePlageTarget) legendePanel_.getCalque();
      target.getPaletteCouleur().setSubTitleVisible(visible);
      subTitleWidget.setVisible(visible);
      updateLegendView();
    }

  }

}
