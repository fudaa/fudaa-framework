/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary.creator;

import com.memoire.bu.BuInsets;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetControllerForGroup;
import org.fudaa.ebli.visuallibrary.EbliWidgetGroup;
import org.fudaa.ebli.visuallibrary.layout.GroupLayout;
import org.netbeans.api.visual.widget.Widget;

/**
 * Creator permettant de cr�er un groupe de widget simple.
 * 
 * @author deniger
 */
public class EbliWidgetGroupCreator extends EbliWidgetCreator {

  Collection<EbliNode> nodesToGroup;

  public EbliWidgetGroupCreator() {

  }

  public EbliWidgetGroupCreator(Collection<EbliNode> nodesToGroup) {
    this.nodesToGroup = new ArrayList<EbliNode>(nodesToGroup);
  }

  /**
   * Ajoute automatiquement les legendes dans la liste a grouper.
   * 
   * @param selectedObjects
   * @return
   */
  private Set<EbliNode> addLegendeAuto(final Collection<EbliNode> selectedObjects) {
    Set<EbliNode> newList = new HashSet<EbliNode>(selectedObjects);

    // -- check automatiquement les l�gendes si il doit en ajouter --//
    for (EbliNode objet : selectedObjects) {
      newList.add(objet);
      objet.getController().fillWithSatelliteNodes(newList);
    }

    return newList;
  }

  @Override
  public EbliWidget createWidget(final EbliScene _scene) {
    if (nodesToGroup != null) {
      Set<EbliNode> allNodesToGroup = addLegendeAuto(nodesToGroup);
      final EbliWidgetGroup parent = new EbliWidgetGroup(_scene);
      // parent.setController(new EbliWidgetControllerMenuOnly(parent));
      parent.setLayout(new GroupLayout());
      final Point min = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
      final Point max = new Point(-Integer.MAX_VALUE, -Integer.MAX_VALUE);
      for (final EbliNode object : allNodesToGroup) {
        if (!_scene.isObject(object)) {
          _scene.addNode(object);
        }
        final Widget findWidget = _scene.findWidget(object);
        // Rectangle rec = findWidget.getBounds();
        final Rectangle rec = findWidget.convertLocalToScene(findWidget.getBounds());
        min.x = Math.min(min.x, rec.x);
        min.y = Math.min(min.y, rec.y);
        max.x = Math.max(max.x, rec.x + rec.width);
        max.y = Math.max(max.y, rec.y + rec.height);
      }
      final int w = max.x - min.x;
      final int h = max.y - min.y;
      final Insets b = BuInsets.INSETS0000;// parent.getBorder().getInsets();

      final Rectangle bounds = new Rectangle(-b.left, -b.top, w + b.left + b.right, h + b.top + b.bottom);
      parent.setPreferredBounds(bounds);
      parent.setPreferredLocation(new Point(min.x - bounds.x - b.left, min.y - bounds.y - b.top));
      parent.setPreferredSize(new Dimension(w + b.left + b.right, h + b.top + b.bottom));

      for (final Object object : allNodesToGroup) {
        final Widget findWidget = _scene.findWidget(object);
        final Rectangle widgetBounds = findWidget.getBounds();
        final Rectangle rec = findWidget.convertLocalToScene(widgetBounds);
        final EbliWidget ew = (EbliWidget) findWidget;
        ew.getController().removeActionResize();

        findWidget.removeFromParent();
        final int dx = rec.x - min.x;
        final int dy = rec.y - min.y;

        final Point pt = new Point(dx - widgetBounds.x, dy - widgetBounds.y);
        findWidget.setPreferredLocation(pt);
        parent.addChild(findWidget);
        final float rMinX = ((float) dx) / w;
        final float rMinY = ((float) dy) / h;
        final float rMaxX = ((float) (w - widgetBounds.width - dx)) / w;
        final float rMaxY = ((float) (h - widgetBounds.height - dy)) / h;
        parent.setChildConstraint(findWidget, new Rectangle2D.Float(rMinX, rMinY, rMaxX, rMaxY));

      }
      // -- sauvegarde des proportions pour le ungroup --//
      parent.setProportions();
      parent.setGroup(true);
      setPreferredLocation(min);
      parent.setController(new EbliWidgetControllerForGroup(parent));
      return parent;

    }
    return getWidget();
  }

  @Override
  public EbliWidgetCreator duplicate(EbliWidgetCreator satteliteOwnerDuplicated) {
    final ArrayList<EbliNode> nodeToGroup = new ArrayList<EbliNode>();
    EbliWidget groupeWidget = getWidget();
    final List<Widget> children = new ArrayList<Widget>(groupeWidget.getIntern().getChildren());
    for (final Widget child : children) {
      final EbliNode n = (EbliNode) groupeWidget.getEbliScene().findObject(child);
      List<EbliNode> duplic = n.duplicate();
      nodeToGroup.addAll(duplic);
    }
    return new EbliWidgetGroupCreator(nodesToGroup);
  }

  /**
   * @param w the w to set
   */
  public void setWidget(final EbliWidget w) {
    super.widget = w;
  }

  @Override
  public Object savePersistData(final Map parameters) {
    return null;
  }

  @Override
  public boolean loadPersistData(final Object data, final Map parameters) {
    return true;
  }
}