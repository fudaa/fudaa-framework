package org.fudaa.ebli.visuallibrary.actions;

import java.util.Set;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;

/**
 * Action qui resize les widgets proportionnellement
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class EbliWidgetActionRatio extends EbliWidgetActionFilteredAbstract {

  // private static final long serialVersionUID = 1L;

  // final CtuluCommandContainer cmd_;
  final boolean resizeProportionnel_;

  // EbliWidget widget_;

  public EbliWidgetActionRatio(final String name, final Icon ic, final String id, final EbliNode _widget,
      final boolean proportionnel) {
    super(_widget, name, ic, id);

    // cmd_ = _widget.getEbliScene().getCmdMng();
    resizeProportionnel_ = proportionnel;
  }

  public static class Proportional extends EbliWidgetActionRatio {

    public Proportional(final EbliNode _widget) {
      super(EbliResource.EBLI.getString("Conserver le ratio hauteur/largeur"), CtuluResource.CTULU.getIcon("lie.png"),
          "RESIZEPEWIDGET", _widget, true);
      setDefaultToolTip(EbliResource.EBLI.getString("Redimensionner proportionnellement la taille de la frame"));

    }

  }

  public static class Classical extends EbliWidgetActionRatio {

    public Classical(final EbliNode _widget) {
      super(EbliResource.EBLI.getString("Ne pas conserver le ratio hauteur/largeur"), CtuluResource.CTULU
          .getIcon("non-lie.png"), "RESIZEEWIDGET", _widget, false);
      setDefaultToolTip(EbliResource.EBLI.getString("Redimensionnement classique de la taille de la frame"));

    }

  }

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {
    if (CtuluLibArray.isEmpty(filteredNode)) return null;
    final EbliNode node = filteredNode.iterator().next();

    // EbliWidgetController controll;
    //
    // if (widget_ instanceof EbliWidgetBordureSingle) {
    //
    // controll = ((EbliWidgetBordureSingle) widget_).getBordureController();
    // } else if (widget_.getParentWidget() instanceof EbliWidgetBordureSingle) {
    // controll = ((EbliWidgetBordureSingle) widget_.getParentWidget()).getBordureController();
    // } else controll = widget_.getController();

    // final EbliWidgetController controller = controll;

    if (resizeProportionnel_) {
      // controller.switchToResizeProportionnal();
      node.getController().boutonProportion.setAction(new EbliWidgetActionRatio.Classical(node));
      node.getController().setProportional(true);
      // -- enregistrement de la commande undoRedo --//
      return new CtuluCommand() {

        @Override
        public void redo() {
          // controller.switchToResizeProportionnal();
          node.getController().boutonProportion.setAction(new EbliWidgetActionRatio.Classical(node));
          node.getController().setProportional(true);
        }

        @Override
        public void undo() {
          // controller.switchToResizeClassical();
          node.getController().boutonProportion.setAction(new EbliWidgetActionRatio.Proportional(node));
          node.getController().setProportional(false);
        }

      };

    } else {
      // controller.switchToResizeClassical();
      node.getController().boutonProportion.setAction(new EbliWidgetActionRatio.Proportional(node));
      node.getController().setProportional(false);
      // -- enregistrement de la commande undoRedo --//
      return new CtuluCommand() {

        @Override
        public void redo() {
          // controller.switchToResizeClassical();
          node.getController().boutonProportion.setAction(new EbliWidgetActionRatio.Proportional(node));
          node.getController().setProportional(false);
        }

        @Override
        public void undo() {
          // controller.switchToResizeProportionnal();
          node.getController().boutonProportion.setAction(new EbliWidgetActionRatio.Classical(node));
          node.getController().setProportional(true);
        }

      };
    }

  }

}
