package org.fudaa.ebli.visuallibrary.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.Predicate;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.PredicateFactory;
import org.fudaa.ebli.visuallibrary.WidgetResource;

/**
 * classe qui permet de dupliquer l'objet selectionne pour connaître les objet selectionnee, on interroge le state des
 * widget isSelection() on duplique via la methode du node duplicate() duplication s effectue en bas par default
 * 
 * @author genesis
 */
public class EbliWidgetActionDuplicate extends EbliWidgetActionFilteredAbstract {

  private static final long serialVersionUID = 1L;

  public EbliWidgetActionDuplicate(final EbliScene _scene) {
    super(_scene, EbliResource.EBLI.getString("Dupliquer les objets sélectionnés"), CtuluResource.CTULU
        .getIcon("crystal_copier"), "DUPLICATE");

  }

  public EbliWidgetActionDuplicate(final EbliNode _scene) {
    super(_scene, WidgetResource.getS("Dupliquer l'objet"), CtuluResource.CTULU.getIcon("crystal_copier"), "DUPLICATE");

  }

  @Override
  protected Predicate getAcceptPredicate() {
    return PredicateFactory.geDuplicatablePredicate();
  }

  @Override
  protected CtuluCommand act(Set<EbliNode> filteredNode) {
    final ArrayList<EbliNode> listeNodeADupliquer = new ArrayList<EbliNode>(filteredNode);

    EbliScene sceneDestination = selectionneSceneNode();
    // -- duplication des noeuds saisis --//
    return duplicate(listeNodeADupliquer, sceneDestination);
  }

  public static CtuluCommand duplicate(final Collection<EbliNode> listeNodeADupliquer, EbliScene sceneDestination) {
    ArrayList<EbliNode> listeNodeUndo = duplicateInScene(listeNodeADupliquer, sceneDestination);
    return new CommandeDuplicate(listeNodeUndo, sceneDestination);
  }

  public static ArrayList<EbliNode> duplicateInScene(final Collection<EbliNode> listeNodeADupliquer,
      EbliScene sceneDestination) {
    ArrayList<EbliNode> listeNodeUndo = new ArrayList<EbliNode>();
    for (final Iterator<EbliNode> it = listeNodeADupliquer.iterator(); it.hasNext();) {
      final EbliNode currentNode = it.next();
      // duplication du node en question
      final List<EbliNode> duplique = (sceneDestination == currentNode.getWidget().getEbliScene()) ? currentNode
          .duplicateInSameScene() : currentNode.duplicate();
      // nouvelle position a cote de son predecesseur
      // -- ajout dans la scene --//
      if (duplique != null && sceneDestination != null) {
        listeNodeUndo.addAll(duplique);
        for (EbliNode ebliNode : duplique) {
          sceneDestination.addNode(ebliNode);
        }
        sceneDestination.refresh();
      }
    }
    return listeNodeUndo;
  }

  /**
   * methode qui doit etre surchargee par les filles pour selectionner la scene de destination
   */
  protected EbliScene selectionneSceneNode() {
    return scene_;
  }

}
