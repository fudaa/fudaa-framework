/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.visuallibrary;

import com.memoire.bu.BuResource;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author deniger
 */
public class WidgetResource extends EbliResource {

  public final static WidgetResource WIDGET = new WidgetResource(EbliResource.EBLI);

  public WidgetResource(final BuResource _parent) {
    super(_parent);
  }

  public static String getS(String _msg) {
    return WIDGET.getString(_msg);
  }
}
