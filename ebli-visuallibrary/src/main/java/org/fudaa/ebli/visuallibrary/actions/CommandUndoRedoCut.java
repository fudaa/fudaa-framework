package org.fudaa.ebli.visuallibrary.actions;

import java.util.Iterator;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliNode;

/**
 * classe de gestion des undo cut. Gere les undo/redo pour l'action ctrl+x. Pour l action paste, un autre undo/redo est
 * g�r� et permet de g�rer les 2 cas copy/cut.
 * 
 * @author Adrien Hadoux
 */
public class CommandUndoRedoCut implements CtuluCommand {

  Set<EbliNode> nodesCutted_;

  public CommandUndoRedoCut(final Set<EbliNode> _nodesCutted) {
    super();
    nodesCutted_ = _nodesCutted;
  }

  @Override
  public void redo() {
    for (final Iterator<EbliNode> it = nodesCutted_.iterator(); it.hasNext();) {
      final EbliNode node = it.next();

      // enlever les nodes de la scene
      // le node existe toujours apres l'avoir enleve
      node.getWidget().getEbliScene().removeNode(node);
    }
    nodesCutted_.iterator().next().getWidget().getEbliScene().refresh();

  }

  @Override
  public void undo() {
    for (final Iterator<EbliNode> it = nodesCutted_.iterator(); it.hasNext();) {
      final EbliNode node = it.next();

      // enlever les nodes de la scene
      // le node existe toujours apres l'avoir enleve
      node.getWidget().getEbliScene().addNode(node);
    }
    nodesCutted_.iterator().next().getWidget().getEbliScene().refresh();

  }

}
