package org.fudaa.ebli.visuallibrary;

import com.memoire.bu.BuPanel;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.EnumSet;
import javax.swing.JFrame;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluHtmlEditorPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.InplaceEditorProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;

public class EbliWidgetTextEditor extends EbliWidget implements InplaceEditorProvider<CtuluHtmlEditorPanel> {

  // editeur de texte
  CtuluHtmlEditorPanel editorPane_ = new CtuluHtmlEditorPanel();
  BuPanel conteneur = null;
  View view;

  public EbliWidgetTextEditor(final EbliScene scene, final CtuluHtmlEditorPanel content) {
    super(scene, false);

    super.setCheckClipping(true);
    editorPane_ = content;
    
    view = BasicHTML.createHTMLView(content, content.getDocumentText());
    // -- creation de l action pour editor --//
    final WidgetAction editorAction = ActionFactory.createInplaceEditorAction(this);

    // -- ajout de l action au widget correspondant --//
    this.getActions().addAction(editorAction);

    final EbliWidgetControllerTextEditor controller = new EbliWidgetControllerTextEditor(this);
    controller.setEditable(true);
    setController(controller);

  }


  @Override
  public boolean canRotate() {
    return false;
  }




  public void setHtml(String html) {
    view = BasicHTML.createHTMLView(editorPane_.getJtpMain(), html);
  }

  @Override
  protected Rectangle calculateClientArea() {
    Rectangle rec = new Rectangle();
    // the preferred size is here
    rec.height = (int) view.getPreferredSpan(View.Y_AXIS);
    rec.width = (int) view.getPreferredSpan(View.X_AXIS);
    editorPane_.setPreferredSize(new Dimension(rec.width,rec.height));
    return rec;
  }

  /**
   *
   */
  @Override
  protected void paintWidget() {
    Rectangle clientArea = getClientArea();
    view.setSize(clientArea.width, clientArea.height);
    view.paint(getGraphics(), clientArea);
  }

  @Override
  public CtuluHtmlEditorPanel createEditorComponent(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget) {
    return editorPane_;

  }

  @Override
  public EnumSet<org.netbeans.api.visual.action.InplaceEditorProvider.ExpansionDirection> getExpansionDirections(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget,
      final CtuluHtmlEditorPanel editor) {
    return null;
  }

  @Override
  public Rectangle getInitialEditorComponentBounds(
      final org.netbeans.api.visual.action.InplaceEditorProvider.EditorController controller, final Widget widget,
      final CtuluHtmlEditorPanel editor, final Rectangle viewBounds) {
    return null;
  }

  @Override
  public void notifyClosing(final EditorController _controller, final Widget _widget,
      final CtuluHtmlEditorPanel _editor, final boolean _commit) {
    setHtml(editorPane_.getDocumentText());
    this.repaint();
    getEbliScene().refresh();
    editingStop(_editor);

  }

  @Override
  public void notifyOpened(final EditorController _controller, final Widget _widget, final CtuluHtmlEditorPanel _editor) {
    _editor.setFrame((JFrame) CtuluLibSwing.getActiveWindow());
    setHtml(CtuluLibString.EMPTY_STRING);
    editingStart(_editor);

  }

}
