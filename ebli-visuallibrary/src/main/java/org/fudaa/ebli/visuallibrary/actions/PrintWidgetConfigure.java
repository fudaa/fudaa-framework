package org.fudaa.ebli.visuallibrary.actions;

import com.memoire.bu.BuComboBox;
import java.util.ArrayList;
import java.util.List;
import javax.print.attribute.standard.MediaSizeName;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurComboBox;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.visuallibrary.EbliPrintOverviewLayer;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;

public class PrintWidgetConfigure implements BConfigurableInterface {

  private EbliScene scene;

  public PrintWidgetConfigure(EbliScene scene) {

    this.scene = scene;
  }

  @SuppressWarnings("serial")
  private static class MediaSizeCellRenderer extends CtuluCellTextRenderer {

    @Override
    protected void setValue(Object _value) {
      if (_value == null) {
        super.setValue(_value);
        return;
      }
      MediaSizeName media = (MediaSizeName) _value;

      String isoName = media.toString();
      String string = "iso-";
      if (isoName.startsWith(string))
        isoName = isoName.substring(string.length());
      super.setValue(isoName.toUpperCase());
    }

  }

  /**
   * Methode qui creer les selecteurs avec les actions qui vont bien
   */
  @Override
  public BSelecteurInterface[] createSelecteurs() {

    final ArrayList<BSelecteurInterface> listeComposants = new ArrayList<BSelecteurInterface>();
    BSelecteurCheckBox selectionVisible = new BSelecteurCheckBox(EbliWidget.VISIBLE, EbliLib.getS("Visible"));
    listeComposants.add(selectionVisible);

    BSelecteurCheckBox selectionLandscape = new BSelecteurCheckBox(EbliPrintOverviewLayer.PROP_ORIENTATION_LANDSCAPE, EbliLib.getS("Paysage"));
    listeComposants.add(selectionLandscape);

    List<MediaSizeName> medias = new ArrayList<MediaSizeName>();
    medias.add(MediaSizeName.ISO_A0);
    medias.add(MediaSizeName.ISO_A1);
    medias.add(MediaSizeName.ISO_A2);
    medias.add(MediaSizeName.ISO_A3);
    medias.add(MediaSizeName.ISO_A4);
    medias.add(MediaSizeName.ISO_A5);
    BuComboBox cb = new BuComboBox((MediaSizeName[]) medias.toArray(new MediaSizeName[medias.size()]));
    cb.setRenderer(new MediaSizeCellRenderer());
    BSelecteurComboBox selecteurCb = new BSelecteurComboBox(EbliPrintOverviewLayer.PROP_MEDIANAME, cb);
    listeComposants.add(selecteurCb);
    selecteurCb.setTitle(EbliLib.getS("Format papier"));

    final BSelecteurLineModel modelLigne = new BSelecteurLineModel(EbliWidget.LINEMODEL);
    modelLigne.setAddColor(true);
    listeComposants.add(modelLigne);

    final BSelecteurInterface[] tableau = new BSelecteurInterface[listeComposants.size()];
    return listeComposants.toArray(tableau);
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return scene.getPrintOverviewLayer();
  }

  @Override
  public String getTitle() {
    return EbliLib.getS("Zone d'impression");
  }

  @Override
  public void stopConfiguration() {
  }
}
