package org.fudaa.ebli.visuallibrary.actions;

import java.util.Collection;
import java.util.Iterator;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliNode;

public class CommandUndoRedoPaste implements CtuluCommand {

  Collection<EbliNode> nodesPasted_;

  public CommandUndoRedoPaste(final Collection<EbliNode> _nodesPasted) {
    super();
    nodesPasted_ = _nodesPasted;
  }

  @Override
  public void redo() {
    for (final Iterator<EbliNode> it = nodesPasted_.iterator(); it.hasNext();) {
      final EbliNode node = it.next();

      // enlever les nodes de la scene
      // le node existe toujours apres l'avoir enleve
      node.getWidget().getEbliScene().addNode(node);
    }
    if (nodesPasted_.iterator().hasNext()) nodesPasted_.iterator().next().getWidget().getEbliScene().refresh();

  }

  @Override
  public void undo() {
    for (final Iterator<EbliNode> it = nodesPasted_.iterator(); it.hasNext();) {
      final EbliNode node = it.next();

      // enlever les nodes de la scene
      // le node existe toujours apres l'avoir enleve
      node.getWidget().getEbliScene().removeNode(node);
    }
    if (nodesPasted_.iterator().hasNext()) nodesPasted_.iterator().next().getWidget().getEbliScene().refresh();

  }

}
