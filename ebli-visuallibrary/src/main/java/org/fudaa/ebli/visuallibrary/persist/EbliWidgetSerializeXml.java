package org.fudaa.ebli.visuallibrary.persist;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.io.Serializable;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibGenerator;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliNodeDefault;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidget;
import org.fudaa.ebli.visuallibrary.EbliWidgetBordureSingle;
import org.fudaa.ebli.visuallibrary.EbliWidgetGroup;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetCreatorLegende;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreator;
import org.fudaa.ebli.visuallibrary.creator.EbliWidgetCreatorClassLoader;
import org.netbeans.api.visual.widget.Widget;

/**
 * Classe utilisation pour la serialization des donn�es widget
 * 
 * @author Adrien Hadoux
 */
public class EbliWidgetSerializeXml implements Serializable{

  public static class CoupleNomId implements Serializable {
    public String id;
    public String nom;

    public CoupleNomId(final String _nom, final String _id) {
      super();
      nom = _nom;
      id = _id;
    }

  }

  @XStreamAlias("warning")
  String warning = "ID unique";

  /**
   * Important: l'id de la widget pour avoir des references .
   */
  String Id;

  // String title;
  /**
   * L'instance de la classe creator
   */
  String type;

  Color bacckground;
  Color foreground;
  Color colorModel;
  Font font;
  double rotation;
  boolean transparent = false;
  /**
   * Donn�es du traceligneModel
   */
  float epaisseurTrait;
  int typeTrait;

  // dimensions
  int width;
  int height;

  // location
  int x;
  int y;

  boolean bordure;

  boolean isVisible;

  boolean isSelected;

  /**
   * L'id de la widget groupe si il y a lieu.
   */
  String IdGroup = EbliWidgetGroup.NOGROUP;

  // -- datas : objet g�r� par le creator, inconnu pour cette classe --//
  Object data;

  public EbliWidgetSerializeXml(final EbliNode node, final Map parameters) {

    fillInfoWith(node, parameters);

  }

  protected void fillInfoWith(final EbliNode node, final Map parameters) {
    EbliWidget widget;
    if (node.getWidget() instanceof EbliWidgetBordureSingle) {
      widget = (EbliWidget) node.getWidget().getChildren().get(0);
      this.bordure = true;
    } else {
      widget = node.getWidget();
      this.bordure = false;
    }
    this.Id = widget.getId();
    // this.title = node.getTitle();
    this.type = node.getCreator().getClass().toString();
    if (widget.getParentBordure().getColorFond() != null) this.bacckground = node.getWidget().getParentBordure()
        .getColorFond();
    if (widget.getParentBordure().getColorContour() != null) this.foreground = widget.getParentBordure()
        .getColorContour();
    if (widget.getParentBordure().getFormeFont() != null) this.font = widget.getParentBordure().getFormeFont();
    this.rotation = widget.getParentBordure().getRotation();
    if (widget.getParentBordure().getTraceLigneModel() != null) {
      this.epaisseurTrait = widget.getParentBordure().getTraceLigneModel().getEpaisseur();
      this.typeTrait = widget.getParentBordure().getTraceLigneModel().getTypeTrait();
      this.colorModel = widget.getParentBordure().getTraceLigneModel().getCouleur();
    }
    this.transparent = widget.getParentBordure().getTransparent();
    // --cas particulier, si widget non visible les dimension bonnes sont celles du parent preferredbounds--//
    if (!widget.getParentBordure().isVisible()) {
      if (widget.getParentBordure().getPreferredBounds() != null) {
        this.width = widget.getParentBordure().getPreferredBounds().width;
        this.height = widget.getParentBordure().getPreferredBounds().height;
      }
    } else if (widget.getParentBordure().getBounds() != null) {
      this.width = widget.getParentBordure().getBounds().width;
      this.height = widget.getParentBordure().getBounds().height;
    }

    if (widget.getParentBordure().getPreferredLocation() != null) {
      this.x = widget.getParentBordure().getPreferredLocation().x;
      this.y = widget.getParentBordure().getPreferredLocation().y;
    }

    this.isVisible = widget.getParentBordure().isVisible();
    this.isSelected = widget.getEbliScene().getSelectedObjects().contains(node);
    this.IdGroup = widget.getIdOfGroup();

    // -- datas --//
    parameters.put("nodeName", node.getTitle());
    this.data = node.getCreator().savePersistData(parameters);

  }

  /**
   * Genere la widget, le creator et le node correspondant ajoute le tout dans la scene et regle les parametres
   * graphiques.
   * 
   * @return null si echec au moment de la creation
   * @throws ClassNotFoundException
   * @throws IllegalAccessException
   * @throws InstantiationException
   */
  public EbliNode generateWidgetInScene(final Map parameters, final EbliScene scene) throws Exception {
    final EbliNode newNode = new EbliNodeDefault();
    newNode.setTitle((String) parameters.get("nodeName"));

    // -- creation du creator specicalis� --//
    final EbliWidgetCreator creator = EbliWidgetCreatorClassLoader.forName(type);
    creator.setPreferredLocation(new Point(x, y));
    if (width > 0 && height > 0) creator.setPreferredSize(new Dimension(width, height));
    else creator.setPreferredSize(new Dimension(200, 200));
    parameters.put("node", newNode);
    newNode.setCreator(creator);
    // -- creation des datas --//
    if (creator.loadPersistData(data, parameters)) {

      // -- ajout du node dans la scene --//
      Widget addNode = scene.addNode(newNode);
      if(addNode==null) scene.removeNode(newNode);
      
      // -- recuperation du precedent Id, tres important poru les references --//
      if (newNode.getWidget() != null) {
        CtuluLibGenerator.getInstance().removeConstraint(newNode.getWidget().getId());
        newNode.getWidget().setId(this.Id);
        CtuluLibGenerator.getInstance().addConstraint(this.Id);

        // --remplissage des proprietes graphiques --//
        newNode.getWidget().setColorFond(bacckground);
        newNode.getWidget().setFormeFont(font);
        newNode.getWidget().setColorContour(foreground);
        newNode.getWidget().setRotation(rotation);
        newNode.getWidget().setTransparent(transparent);
        final TraceLigneModel model = new TraceLigneModel(typeTrait, epaisseurTrait, colorModel);
        newNode.getWidget().setTraceLigneModel(model);
        newNode.getWidget().getParentBordure().setVisible(isVisible);
      }
      return newNode;
    }
    return null;

  }

  public boolean isLinked() {
    String className = type;
    if (type == null) return true;
    if (className.startsWith("class ")) ;
    className = className.substring("class ".length());

    if (className.equals(EbliWidgetCreatorLegende.class.getName())) return true;
    else if (className.equals(org.fudaa.ebli.visuallibrary.graphe.EbliWidgetCreatorLegende.class.getName())) return true;
    return false;

  }

  // public String getTitle() {
  // return title;
  // }
  //
  // public void setTitle(final String title) {
  // this.title = title;
  // }

  public String getType() {
    return type;
  }

  public void setType(final String type) {
    this.type = type;
  }

  public Color getBacckground() {
    return bacckground;
  }

  public void setBacckground(final Color bacckground) {
    this.bacckground = bacckground;
  }

  public Color getForeground() {
    return foreground;
  }

  public void setForeground(final Color foreground) {
    this.foreground = foreground;
  }

  public Font getFont() {
    return font;
  }

  public void setFont(final Font font) {
    this.font = font;
  }

  public double getRotation() {
    return rotation;
  }

  public void setRotation(final double rotation) {
    this.rotation = rotation;
  }

  public float getEpaisseurTrait() {
    return epaisseurTrait;
  }

  public void setEpaisseurTrait(final float epaisseurTrait) {
    this.epaisseurTrait = epaisseurTrait;
  }

  public int getTypeTrait() {
    return typeTrait;
  }

  public void setTypeTrait(final int typeTrait) {
    this.typeTrait = typeTrait;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(final int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(final int height) {
    this.height = height;
  }

  public int getX() {
    return x;
  }

  public void setX(final int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(final int y) {
    this.y = y;
  }

  public boolean isBordure() {
    return bordure;
  }

  public void setBordure(final boolean bordure) {
    this.bordure = bordure;
  }

  public Object getData() {
    return data;
  }

  public void setData(final Object data) {
    this.data = data;
  }

  public Color getColorModel() {
    return colorModel;
  }

  public void setColorModel(final Color colorModel) {
    this.colorModel = colorModel;
  }

  public boolean isVisible() {
    return isVisible;
  }

  public void setVisible(final boolean isVisible) {
    this.isVisible = isVisible;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(final boolean isSelected) {
    this.isSelected = isSelected;
  }

  public String getId() {
    return Id;
  }

  public void setId(final String id) {
    Id = id;
  }

  public String getIdGroup() {
    return IdGroup;
  }

  public void setIdGroup(final String idGroup) {
    IdGroup = idGroup;
  }

  public boolean isTransparent() {
    return transparent;
  }

  public void setTransparent(boolean transparent) {
    this.transparent = transparent;
  }

}
