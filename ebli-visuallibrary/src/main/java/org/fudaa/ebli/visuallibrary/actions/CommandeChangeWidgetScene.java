package org.fudaa.ebli.visuallibrary.actions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;

/**
 * action qui permet de faire le undo/redo sur le changement de scene du node.
 * 
 * @author genesis
 */
public class CommandeChangeWidgetScene implements CtuluCommand {

  ArrayList<EbliNode> nodeDuplique_;
  ArrayList<Point> previsouLocation_;
  ArrayList<Point> newLocation_;

  EbliScene sceneDepart_;
  EbliScene sceneDestination_;

  public CommandeChangeWidgetScene(final ArrayList<EbliNode> nodeDuplique, final EbliScene sceneDep,
      final EbliScene sceneDest, ArrayList<Point> previsouLocation) {
    super();
    this.nodeDuplique_ = nodeDuplique;
    this.sceneDepart_ = sceneDep;
    sceneDestination_ = sceneDest;
    previsouLocation_ = previsouLocation;

    newLocation_ = new ArrayList<Point>();
    for (EbliNode node : nodeDuplique_) {
      newLocation_.add(node.getWidget().getLocation());
    }

  }

  @Override
  public void redo() {
    // -- redo unitaire execute pour chaque node dupliques --//
    for (final Iterator<EbliNode> it = nodeDuplique_.iterator(); it.hasNext();) {
      final EbliNode node = it.next();
      sceneDepart_.removeNode(node);
      sceneDepart_.refresh();
      sceneDestination_.addNode(node);
      sceneDestination_.refresh();
    }
  }

  @Override
  public void undo() {
    // -- redo unitaire execute pour chaque node dupliques --//
    int cpt = 0;
    for (final Iterator<EbliNode> it = nodeDuplique_.iterator(); it.hasNext();) {
      final EbliNode node = it.next();
      sceneDestination_.removeNode(node);
      sceneDestination_.refresh();
      sceneDepart_.addNode(node);
      if (previsouLocation_.size() > cpt) node.getWidget().setPreferredLocation(
          node.getWidget().convertSceneToLocal(previsouLocation_.get(cpt++)));
      sceneDepart_.refresh();
      // -- gerer cas particulier des legendes, il faut les rattacher a leur
      // widget origine --//
    }
  }

}
