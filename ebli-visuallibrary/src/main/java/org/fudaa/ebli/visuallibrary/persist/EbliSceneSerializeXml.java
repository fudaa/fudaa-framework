package org.fudaa.ebli.visuallibrary.persist;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.io.Serializable;

import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.EbliWidgetGroup;
import org.fudaa.ebli.visuallibrary.calque.EbliWidgetFusionCalques;

/**
 * Classe utilis�e par d�faut pour la serialization xml avec xstream
 * 
 * @author Adrien Hadoux
 */
public class EbliSceneSerializeXml implements Serializable {

  String title;
  Color background;
  int nbFrames;

  // dimensiosn de la frame
  int width;
  int height;

  // position de la frame ou autre
  int x;
  int y;

  EbliPrintOverviewPersist printOverviewPersist;

  public EbliSceneSerializeXml(final EbliScene scene, final String title, final Dimension dimensionFrame, final Point location) {

    fillInfoWith(scene, title, dimensionFrame, location);

  }

  private void fillInfoWith(final EbliScene _scene, final String _title, final Dimension _dimensionFrame, final Point _location) {
    this.title = _title;
    this.background = (Color) _scene.getBackground();
    this.nbFrames = countFrameToSave(_scene);
    if (_scene.getView() != null) {
      this.width = _dimensionFrame.width;
      this.height = _dimensionFrame.height;
      this.x = _location.x;
      this.y = _location.y;
    }
    printOverviewPersist = new EbliPrintOverviewPersist();
    printOverviewPersist.initFrom(_scene.getPrintOverviewLayer());
  }

  private int countFrameToSave(final EbliScene scene) {
    int cpt = 0;
    for (final Object node : scene.getObjects()) {
      if (node instanceof EbliNode) {
        if (!(((EbliNode) node).getWidget() instanceof EbliWidgetGroup) && !(((EbliNode) node).getWidget() instanceof EbliWidgetFusionCalques))
          cpt++;
      }
    }
    return cpt;
  }

  /**
   * Methode qui configurer la scene avec les infos r�cup�r�es
   * 
   * @param scene
   */
  public void configureSceneWith(final EbliScene scene) {
    scene.setToolTipText(this.title);
    scene.setPreferredSize(new Dimension(width, height));
    scene.setBackground(background);
    if (printOverviewPersist != null) {
      printOverviewPersist.applyTo(scene.getPrintOverviewLayer());
    }
  }

  public String getTitle() {
    return title;
  }
  
  
  public void setTitle(final String title) {
    this.title = title;
  }

  public Color getBakground() {
    return background;
  }

  public void setBakground(final Color bakground) {
    this.background = bakground;
  }

  public int getNbFrames() {
    return nbFrames;
  }

  public void setNbFrames(final int nbFrames) {
    this.nbFrames = nbFrames;
  }

  public Color getBackground() {
    return background;
  }

  public void setBackground(final Color background) {
    this.background = background;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(final int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(final int height) {
    this.height = height;
  }

  public int getX() {
    return x;
  }

  public void setX(final int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(final int y) {
    this.y = y;
  }

}
