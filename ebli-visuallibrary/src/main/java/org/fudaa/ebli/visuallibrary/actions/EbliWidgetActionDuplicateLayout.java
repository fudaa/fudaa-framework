package org.fudaa.ebli.visuallibrary.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.visuallibrary.EbliNode;
import org.fudaa.ebli.visuallibrary.EbliScene;
import org.fudaa.ebli.visuallibrary.PredicateFactory;

/**
 * Action qui duplique l'ensemble des widget d'un layout vers un autre
 * 
 * @author Adrien Hadoux
 */
@SuppressWarnings("serial")
public class EbliWidgetActionDuplicateLayout extends EbliWidgetActionAbstract {

  /**
		 * 
		 */

  /**
   * Possibilité de changer la scene de destination
   */
  EbliScene sceneDestination_ = null;

  public EbliWidgetActionDuplicateLayout(final EbliScene _scene) {
    super(_scene, EbliResource.EBLI.getString("Dupliquer le layout"), CtuluResource.CTULU
        .getIcon("crystal_cascade.png"), "DUPLICATE");
    putValue(NAME, EbliResource.EBLI.getString("Dupliquer le layout"));
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    // -- recuperation de la liste des widgets selectionnees --//

    final Set<EbliNode> listeNode = (Set<EbliNode>) scene_.getNodes();

    final ArrayList<EbliNode> listeNodeADupliquer = new ArrayList<EbliNode>(listeNode.size());
    CollectionUtils.select(listeNode, PredicateFactory.geDuplicatablePredicate(), listeNodeADupliquer);
    sceneDestination_ = selectionneSceneNode();

    // -- duplication des noeuds saisis --//
    for (final Iterator<EbliNode> it = listeNodeADupliquer.iterator(); it.hasNext();) {
      final EbliNode currentNode = it.next();
      List<EbliNode> node = currentNode.duplicate();
      for (EbliNode ebliNode : node) {
        sceneDestination_.addNode(ebliNode);
      }
      sceneDestination_.refresh();

    }

  }

  /**
   * methode qui doit etre surchargee par les filles pour selectionner la scene de destination
   */
  protected EbliScene selectionneSceneNode() {

    return scene_;
  }

}
