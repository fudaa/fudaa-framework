/*
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2006-09-19 14:55:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;

import javax.swing.JLabel;

import org.fudaa.ctulu.CtuluLibString;

/**
 * @author deniger
 * @version $Id: GrapheInteractionSuiviSouris.java,v 1.5 2006-09-19 14:55:45 deniger Exp $
 */
public class GrapheInteractionSuiviSouris extends MouseMotionAdapter {
  BGraphe g_;
  Point2D p_;
  JLabel l_;
  DecimalFormat xFormat_;
  DecimalFormat yFormat_;

  public GrapheInteractionSuiviSouris(final BGraphe _g) {
    p_ = new Point2D.Double();
    g_ = _g;
  }

  public JLabel getLabel() {
    if (l_ == null) {
      l_ = new JLabel();
      l_.setText(xFormat_ == null ? CtuluLibString.getString(0)
          : xFormat_.format(0) + " ; " + yFormat_ == null ? CtuluLibString.getString(0) : yFormat_.format(0));
    }

    return l_;
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {

    g_.versReel(_e.getX(), _e.getY(), p_);
    if (l_ != null) {
      l_.setText((xFormat_ == null ? CtuluLibString.getString((int) p_.getX()) : xFormat_.format(p_.getX())) + " ; "
          + (yFormat_ == null ? CtuluLibString.getString((int) p_.getY()) : yFormat_.format(p_.getY())));

    }

  }

  public DecimalFormat getXFormat() {
    return xFormat_;
  }

  public void setFormat(final DecimalFormat _f) {
    setXFormat(_f);
    setYFormat(_f);
  }

  public void setFormatInteger() {
    yFormat_ = null;
    xFormat_ = null;
  }

  public void setXFormatInteger() {
    xFormat_ = null;
  }

  public void setYFormatInteger() {
    yFormat_ = null;
  }

  public DecimalFormat getYFormat() {
    return yFormat_;
  }

  public void setXFormat(final DecimalFormat _format) {
    xFormat_ = _format;
  }

  public void setYFormat(final DecimalFormat _format) {
    yFormat_ = _format;
  }

}
