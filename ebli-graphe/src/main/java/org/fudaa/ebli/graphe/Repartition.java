/*
 * @creation     1999-12-22
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Vector;

/**
 * Repartition.
 *
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class Repartition implements GrapheComponent {
  public String titre_;
  public String type_;
  public String trace_;
  public boolean marqueurs_;
  public Aspect aspect_;
  public Vector valeurs_;
  public boolean visible_;

  public Repartition() {
    titre_ = "";
    type_ = "courbe";
    trace_ = "lineaire";
    marqueurs_ = true;
    aspect_ = new Aspect();
    aspect_.contour_ = Color.red;
    valeurs_ = new Vector(0, 1);
    visible_ = true;
  }

  public static Repartition parse(final Lecteur _lin) {
    final Repartition r = new Repartition();
    String t = _lin.get();
    if (t.equals("repartition")) {
      t = _lin.get();
    }
    if (!t.equals("{")) {
      System.err.println("Erreur de syntaxe: " + t);
    }
    t = _lin.get();
    while (!t.equals("") && !t.equals("}")) {
      if (t.equals("titre")) {
        r.titre_ = _lin.get();
      } else if (t.equals("type")) {
        r.type_ = _lin.get();
      } else if (t.equals("trace")) {
        r.trace_ = _lin.get();
      } else if (t.equals("marqueurs")) {
        r.marqueurs_ = _lin.get().equals("oui");
      } else if (t.equals("aspect")) {
        r.aspect_ = Aspect.parse(_lin);
      } else if (t.equals("valeurs")) {
        r.valeurs_ = Valeur.parse(_lin);
      } else if (t.equals("visible")) {
        r.visible_ = _lin.get().equals("oui");
      } else {
        System.err.println("Erreur de syntaxe: " + t);
      }
      t = _lin.get();
    }
    return r;
  }

  @Override
  public void dessine(final Graphics2D _g, final int _x, final int _y, final int _w, final int _h, final Axe _ax, final Axe _ay, final boolean _b) {
    if (!visible_) {
      return;
    }
    double angleP = 0;
    final Rectangle rect = new Rectangle(_x, _y, Math.min(_w, _h), Math.min(_w, _h));
    _g.setColor(_b ? aspect_.contour_ : Color.gray);
    double total = 0.;
    for (int i = 0; i < valeurs_.size(); i++) {
      total += ((Valeur) valeurs_.get(i)).s_;
    }
    for (int i = 0; i < valeurs_.size(); i++) {
      final Valeur o = (Valeur) valeurs_.get(i);
      if (type_.equals("camembert")) {
        final Aspect a = o.aspect_;
        final double angleE = o.s_ / total * 360;
        System.err.println((int) angleP + " -> " + ((int) angleE + (int) angleP));
        if ((a != null) && (a.surface_ != null)) {
          _g.setColor(a.surface_);
          _g.fillArc(rect.x, rect.y, rect.width, rect.height, (int) angleP, (int) angleE + 1);
        } else if (aspect_.surface_ != null) {
          _g.setColor(aspect_.surface_);
          _g.fillArc(rect.x, rect.y, rect.width, rect.height, (int) angleP, (int) angleE + 1);
        }
        _g.setColor(_b ? (a != null ? a.contour_ : aspect_.contour_) : Color.gray);
        _g.drawArc(rect.x, rect.y, rect.width, rect.height, (int) angleP, (int) angleE + 1);
        angleP += angleE;
      }
    }
  }

  /**
   *
   */
  @Override
  public Color getAspectContour() {
    return aspect_.contour_;
  }

  /**
   *
   */
  @Override
  public Color getAspectSurface() {
    return aspect_.surface_;
  }

  /**
   *
   */
  @Override
  public Color getAspectTexte() {
    return aspect_.texte_;
  }

  /**
   *
   */
  @Override
  public String getTitre() {
    return titre_;
  }

  /**
   *
   */
  @Override
  public boolean isVisible() {
    return visible_;
  }

  public final boolean isCourbe() {
    return false;
  }

  /**
   *
   */
  @Override
  public int getAxeId() {
    return -1;
  }

  /**
   *
   */
  @Override
  public final boolean isLegendAvailable() {
    return true;
  }

  @Override
  public boolean setVisible(final boolean _r) {
    if (visible_ != _r) {
      visible_ = _r;
      return true;
    }
    return false;
  }

  @Override
  public boolean setAspectContour(final Color _c) {
    if (_c == null) {
      return false;
    }
    final Color init = aspect_.contour_;
    if ((init == _c) || (_c.equals(init))) {
      return false;
    }
    aspect_.contour_ = _c;
    return true;
  }

  @Override
  public boolean getMinMax(final double[] _r) {
    final int n = valeurs_.size() - 1;
    if (n < 0) {
      return false;
    }
    Valeur v = (Valeur) valeurs_.get(n);
    _r[0] = v.s_;
    _r[1] = v.s_;
    _r[2] = v.v_;
    _r[3] = v.v_;
    for (int i = n - 1; i >= 0; i--) {
      v = (Valeur) valeurs_.get(i);
      if (v.s_ < _r[0]) {
        _r[0] = v.s_;
      }
      if (v.s_ > _r[1]) {
        _r[1] = v.s_;
      }
      if (v.v_ < _r[2]) {
        _r[2] = v.v_;
      }
      if (v.v_ > _r[3]) {
        _r[3] = v.v_;
      }
    }
    return true;
  }

}
