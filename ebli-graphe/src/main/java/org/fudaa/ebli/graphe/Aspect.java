/**
 * @creation     1999-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;

import com.memoire.fu.FuLog;

/**
 * Aspect.
 *
 * @version $Revision: 1.12 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class Aspect {
  public Color contour_;
  public Color surface_;
  public Color texte_;
  public int largeur_;

  public Aspect() {
    contour_ = Color.black;
    surface_ = Color.white;
    texte_ = Color.black;
    largeur_ = 7;
  }

  public static Aspect parse(final Lecteur _lin) {
    final Aspect r = new Aspect();
    String t = _lin.get();
    if (t.equals("aspect")) {
      t = _lin.get();
    }
    if (!t.equals("{")) {
      FuLog.error("Erreur de syntaxe: " + t);
    }
    t = _lin.get();
    while (!t.equals("") && !t.equals("}")) {
      if (t.equals("contour.couleur")) {
        r.contour_ = new Color(Integer.valueOf(_lin.get(), 16).intValue());
      } else if (t.equals("surface.couleur")) {
        r.surface_ = new Color(Integer.valueOf(_lin.get(), 16).intValue());
      } else if (t.equals("texte.couleur")) {
        r.texte_ = new Color(Integer.valueOf(_lin.get(), 16).intValue());
      } else if (t.equals("contour.largeur")) {
        r.largeur_ = Integer.valueOf(_lin.get()).intValue();
        if (r.largeur_ % 2 == 0) {
          r.largeur_--;
        }
      } 
      t = _lin.get();
    }
    return r;
  }

  protected Color getContour() {
    return contour_;
  }

  protected void setContour(final Color _contour) {
    contour_ = _contour;
  }

  protected int getLargeur() {
    return largeur_;
  }

  protected void setLargeur(final int _largeur) {
    largeur_ = _largeur;
  }

  protected Color getSurface() {
    return surface_;
  }

  protected void setSurface(final Color _surface) {
    surface_ = _surface;
  }

  protected Color getTexte() {
    return texte_;
  }

  protected void setTexte(final Color _texte) {
    texte_ = _texte;
  }
}
