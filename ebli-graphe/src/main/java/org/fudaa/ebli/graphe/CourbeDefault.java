/*
 * @creation     1999-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

/**
 * Courbe.
 *
 * @version $Id: CourbeDefault.java,v 1.8 2006-09-19 14:55:45 deniger Exp $
 * @author Guillaume Desnoix
 */
public class CourbeDefault extends Courbe {

  public List<Valeur> valeurs_;

  /**
   * Constructeur avec x et y donn�s. Les tableaux doivent avoir m�me taille.
   * @param _x Les coordonn�es x
   * @param _y Les coordonn�es y
   */
  public CourbeDefault(double[] _x, double[] _y) {
    if (_x==null || _y==null || _x.length!=_y.length)
      throw new IllegalArgumentException("_x et _y doivent avoir la m�me taille");
    
    valeurs_=new ArrayList<Valeur>(_x.length);
    for (int i=0; i<_x.length; i++) {
      valeurs_.add(new Valeur(_x[i],_y[i]));
    }
  }
  
  public CourbeDefault() {
    valeurs_ = new ArrayList<Valeur>(1);
  }

  public static CourbeDefault parse(final Lecteur _lin) {
    final CourbeDefault r = new CourbeDefault();
    String t = _lin.get();
    if (t.equals("courbe")) {
      t = _lin.get();
    }
    if (!t.equals("{")) {
      System.err.println("Erreur de syntaxe: " + t);
    }
    t = _lin.get();
    while (!t.equals("") && !t.equals("}")) {
      if (t.equals("titre")) {
        r.titre_ = _lin.get();
      } else if (t.equals("type")) {
        r.type_ = _lin.get();
      } else if (t.equals("trace")) {
        r.trace_ = _lin.get();
      } else if (t.equals("axe")) {
        try {
          r.axe_ = Integer.parseInt(_lin.get());
        } catch (final NumberFormatException e) {}
      } else if (t.equals("marqueurs")) {
        r.marqueurs_ = _lin.get().equals("oui");
      } else if (t.equals("aspect")) {
        r.aspect_ = Aspect.parse(_lin);
      } else if (t.equals("valeurs")) {
        r.valeurs_ = Valeur.parse(_lin);
      } else if (t.equals("visible")) {
        r.visible_ = _lin.get().equals("oui");
      } else {
        System.err.println("Erreur de syntaxe: " + t);
      }
      t = _lin.get();
    }
    return r;
  }

  public static CourbeDefault parseXY(final Lecteur _lin) {
    final CourbeDefault r = new CourbeDefault();
    r.valeurs_ = Valeur.parseXY(_lin);
    return r;
  }

  void trieX() {
    boolean b = true;
    while (b) {
      b = false;
      for (int i = 0; i < valeurs_.size() - 1; i++) {
        final Valeur o1 = (Valeur) valeurs_.get(i);
        final Valeur o2 = (Valeur) valeurs_.get(i + 1);
        if (o1.s_ > o2.s_) {
          b = true;
          valeurs_.set(i + 1,o1);
          valeurs_.set(i,o2);
          if (i >= 0) {
            i--;
          }
          if (i >= 0) {
            i--;
          }
        }
      }
    }
  }

  void trieY() {
    boolean b = true;
    while (b) {
      b = false;
      for (int i = 0; i < valeurs_.size() - 1; i++) {
        final Valeur o1 = (Valeur) valeurs_.get(i);
        final Valeur o2 = (Valeur) valeurs_.get(i + 1);
        if (o1.v_ > o2.v_) {
          b = true;
          valeurs_.set(i + 1, o1);
          valeurs_.set(i, o2);
          if (i >= 0) {
            i--;
          }
          if (i >= 0) {
            i--;
          }
        }
      }
    }
  }

  double getMinX() {
    final int n = valeurs_.size() - 1;
    double r = Double.MAX_VALUE;
    double v;
    for (int i = n; i >= 0; i--) {
      v = ((Valeur) valeurs_.get(i)).s_;
      if (v < r) {
        r = v;
      }
    }
    if (r >= Double.MAX_VALUE) {
      r = 0;
    }
    return r;
  }

  double getMinY() {
    final int n = valeurs_.size() - 1;
    double r = Double.MAX_VALUE;
    double v;
    for (int i = n; i >= 0; i--) {
      v = ((Valeur) valeurs_.get(i)).v_;
      if (v < r) {
        r = v;
      }
    }
    if (r >= Double.MAX_VALUE) {
      r = 0;
    }
    return r;
  }

  double getMaxX() {
    final int n = valeurs_.size() - 1;
    double r = Double.NEGATIVE_INFINITY;
    double v;
    for (int i = n; i >= 0; i--) {
      v = ((Valeur) valeurs_.get(i)).s_;
      if (v > r) {
        r = v;
      }
    }
    if (r <= Double.NEGATIVE_INFINITY) {
      r = 0;
    }
    return r;
  }

  double getMaxY() {
    final int n = valeurs_.size() - 1;
    double r = Double.NEGATIVE_INFINITY;
    double v;
    for (int i = n; i >= 0; i--) {
      v = ((Valeur) valeurs_.get(i)).v_;
      if (v > r) {
        r = v;
      }
    }
    if (r <= Double.NEGATIVE_INFINITY) {
      r = 0;
    }
    return r;
  }

  @Override
  public void dessine(final Graphics2D _g, final int _x, final int _y, final int _w, final int _h, final Axe _ax, final Axe _ay, final boolean _couleurAspect) {
    if (!visible_) {
      return;
    }
    if (valeurs_.size() == 0) {
      return;
    }
    int xp = 0;
    int yp = 0;
    if (type_.equals("enveloppe")) {
      trieX();
      type_ = "enveloppex";
      dessine(_g, _x, _y, _w, _h, _ax, _ay, _couleurAspect);
      trieY();
      type_ = "enveloppey";
      dessine(_g, _x, _y, _w, _h, _ax, _ay, _couleurAspect);
      return;
    }
    final Valeur o0 = (Valeur) valeurs_.get(0);
    final int x0 = _x + (int) (_w * ((o0.s_ - _ax.minimum_) / (_ax.maximum_ - _ax.minimum_)));
    final int y0 = _y + _h - (int) (_h * ((o0.v_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
    final Valeur o1 = (Valeur) valeurs_.get(valeurs_.size() - 1);
    final int x1 = _x + (int) (_w * ((o1.s_ - _ax.minimum_) / (_ax.maximum_ - _ax.minimum_)));
    final int y1 = _y + _h - (int) (_h * ((o1.v_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
    final Polygon polygon = new Polygon();
    _g.setColor(_couleurAspect ? aspect_.contour_ : Color.gray);
    if (trace_.equals("lineaire")) {
      for (int i = 0; i < valeurs_.size(); i++) {
        final Valeur o = (Valeur) valeurs_.get(i);
        final int xe = _x + (int) (_w * ((o.s_ - _ax.minimum_) / (_ax.maximum_ - _ax.minimum_)));
        final int ye = _y + _h - (int) (_h * ((o.v_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
        if (type_.equals("nuage")) {
          _g.setColor(_couleurAspect ? aspect_.contour_ : Color.gray);
          _g.drawLine(xe, ye, xe, ye);
        } else if (type_.equals("enveloppex")) {
          final int[] xg = { x0, xe, x1 };
          final int[] yg = { y0, ye, y1 };
          _g.setColor(_couleurAspect ? aspect_.surface_ : Color.lightGray);
          _g.fillPolygon(xg, yg, 3);
        } else if (type_.equals("enveloppey")) {
          final int[] xg = { x0, xe, x1 };
          final int[] yg = { y0, ye, y1 };
          _g.setColor(_couleurAspect ? aspect_.surface_ : Color.lightGray);
          _g.fillPolygon(xg, yg, 3);
        } else if (type_.equals("histogramme")) {
          int l = aspect_.largeur_;
          if (l < 0) {
            l = xe - xp;
          } else {
            l = (int) (_w * ((aspect_.largeur_ - _ax.minimum_) / (_ax.maximum_ - _ax.minimum_)));
          }
          _g.setColor(_couleurAspect ? aspect_.surface_ : Color.lightGray);
          _g.fillRect(xe - l / 2, ye, l, _y + _h - ye - 1);
          _g.setColor(_couleurAspect ? aspect_.contour_ : Color.gray);
          _g.drawLine(xe - l / 2, _y + _h - 1, xe - l / 2, ye);
          _g.drawLine(xe - l / 2, ye, xe + l / 2, ye);
          _g.drawLine(xe + l / 2, ye, xe + l / 2, _y + _h - 1);
        } else if (type_.equals("fuseau")) {
          final int yeinf = _y + _h - (int) (_h * ((o.vinf_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
          final int yesup = _y + _h - (int) (_h * ((o.vsup_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
          _g.setColor(_couleurAspect ? aspect_.surface_ : Color.lightGray);
          _g.drawLine(xe, yeinf, xe, yesup);
          _g.drawLine(xe - 2, yeinf, xe + 2, yeinf);
          _g.drawLine(xe - 2, yesup, xe + 2, yesup);
          _g.setColor(_couleurAspect ? aspect_.contour_ : Color.gray);
          _g.drawLine(xe - 2, ye, xe + 2, ye);
        } else if (type_.equals("polygone")) {
          polygon.addPoint(xe, ye);
        } else if (i > 0) {
          if (type_.equals("aire")) {
            final int[] xg = { xp, xe, xe, xp };
            final int[] yg = { yp, ye, _y + _h - 1, _y + _h - 1 };
            _g.setColor(_couleurAspect ? aspect_.surface_ : Color.lightGray);
            _g.fillPolygon(xg, yg, 4);
            _g.setColor(_couleurAspect ? aspect_.contour_ : Color.gray);
          }
          _g.drawLine(xp, yp, xe, ye);
        }
        xp = xe;
        yp = ye;
      }
      if (type_.equals("polygone")) {
        _g.setColor(_couleurAspect ? aspect_.surface_ : Color.lightGray);
        _g.fillPolygon(polygon);
        _g.setColor(_couleurAspect ? aspect_.contour_ : Color.gray);
        _g.drawPolygon(polygon);
      }
    }
    _g.setColor(aspect_.texte_);
    if (marqueurs_) {
      for (int i = 0; i < valeurs_.size(); i++) {
        final Valeur o = (Valeur) valeurs_.get(i);
        final int xe = _x + (int) (_w * ((o.s_ - _ax.minimum_) / (_ax.maximum_ - _ax.minimum_)));
        final int ye = _y + _h - (int) (_h * ((o.v_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
        _g.drawRect(xe - 1, ye - 1, 2, 2);
      }
    }
    _g.setColor(aspect_.texte_);
    final FontMetrics fm = _g.getFontMetrics();
    for (int i = 0; i < valeurs_.size(); i++) {
      final Valeur o = (Valeur) valeurs_.get(i);
      final int xe = _x + (int) (_w * ((o.s_ - _ax.minimum_) / (_ax.maximum_ - _ax.minimum_)));
      final int ye = _y + _h - (int) (_h * ((o.v_ - _ay.minimum_) / (_ay.maximum_ - _ay.minimum_)));
      _g.drawString(o.titre_, xe - fm.stringWidth(o.titre_) / 2, ye - 3);
    }
    /*
     * if((valeurs.size()>0)&&(!titre.equals(""))) { g.setColor(b ? aspect.texte : Color.gray); Valeur
     * o=(Valeur)valeurs.elementAt(0); int xe=x+(int)((double)w*((o.s-ax.minimum)/(ax.maximum-ax.minimum))); int
     * ye=y+h-(int)((double)h*((o.v-ay.minimum)/(ay.maximum-ay.minimum)));
     * g.drawString(titre,xe-3-fm.stringWidth(titre),ye+4); }
     */
  }

  @Override
  public boolean getMinMax(final double[] _r) {
    final int n = valeurs_.size() - 1;
    if (n < 0) {
      return false;
    }
    Valeur v = (Valeur) valeurs_.get(n);
    _r[0] = v.s_;
    _r[1] = v.s_;
    _r[2] = v.v_;
    _r[3] = v.v_;
    for (int i = n - 1; i >= 0; i--) {
      v = (Valeur) valeurs_.get(i);
      if (v.s_ < _r[0]) {
        _r[0] = v.s_;
      }
      if (v.s_ > _r[1]) {
        _r[1] = v.s_;
      }
      if (v.v_ < _r[2]) {
        _r[2] = v.v_;
      }
      if (v.v_ > _r[3]) {
        _r[3] = v.v_;
      }
    }
    return true;
  }

  @Override
  public int getNbPoint() {
    return valeurs_.size();
  }

  @Override
  public double getX(final int _idx) {
    return ((Valeur) valeurs_.get(_idx)).s_;
  }

  @Override
  public double getY(final int _idx) {
    return ((Valeur) valeurs_.get(_idx)).v_;
  }

}
