/*
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2006-09-19 14:55:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;


import javax.swing.BorderFactory;
import javax.swing.JTable;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;

/**
 * @author deniger
 * @version $Id: BGrapheComponentListPanel.java,v 1.4 2006-09-19 14:55:45 deniger Exp $
 */
public class BGrapheComponentListPanel extends BuPanel {
  BGrapheController grapheController_;
  /**
   *
   */
  public BGrapheComponentListPanel(final BGrapheController _c) {
    grapheController_=_c;
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setLayout(new BuBorderLayout());
    final JTable t=grapheController_.createTable();
    t.setShowGrid(false);
    add(new BuScrollPane(t),BuBorderLayout.CENTER);
  }

}