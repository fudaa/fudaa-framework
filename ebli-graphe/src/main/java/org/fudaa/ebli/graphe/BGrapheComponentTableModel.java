/*
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2006-09-19 14:55:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;


import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author deniger
 * @version $Id: BGrapheComponentTableModel.java,v 1.6 2006-09-19 14:55:45 deniger Exp $
 */
public class BGrapheComponentTableModel extends AbstractTableModel {

  BGraphe g_;
  /**
   *
   */
  public BGrapheComponentTableModel(final BGraphe _g) {
    super();
    g_= _g;
  }

  public void setAllVisible(final boolean _b){
    final Graphe g=g_.getGraphe();
    if(g.setAllVisible(_b)) {
      g_.fullRepaint();
    }
  }

  /**
   *
   */
  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public int getRowCount() {
    return g_.getGraphe().getNbGrapheComponent();
  }

  @Override
  public Object getValueAt(final int _row, final int _col) {
    return g_.getGraphe().getGrapheComponent(_row);
  }



  /**
   * Permet d'initialiser les renderer,editor pour ce model.
   */
  public void setRendererAndEditor(final TableColumnModel _m) {
    TableColumn col= _m.getColumn(0);
    col.setCellRenderer(BGrapheController.createNameContourRenderer());
    col= _m.getColumn(1);
    col.setCellEditor(BGrapheController.createVisibleCellEditor());
    col.setCellRenderer(BGrapheController.createVisibleCellRenderer());
    col.setWidth(5);


  }

  @Override
  public void setValueAt(final Object _aValue, final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 1) {
      final GrapheComponent c=
        g_.getGraphe().getGrapheComponent(_rowIndex);
      if (c.setVisible(_aValue == Boolean.TRUE)){
        g_.fullRepaint();
      }
    }
  }

  @Override
  public String getColumnName(final int _column) {
    if(_column==0) {
      return EbliLib.getS("Titre");
    } else if(_column==1) {
      return "v";
    }
    return CtuluLibString.EMPTY_STRING;


  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex>0;
  }

}
