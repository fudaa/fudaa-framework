/*
 * @creation     1999-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Arrays;

/**
 * Courbe.
 *
 * @version $Revision: 1.6 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public abstract class CourbeModelAbstract extends Courbe {

  @Override
  public void dessine(final Graphics2D _g, final int _x, final int _y, final int _w, final int _h, final Axe _ax, final Axe _ay, final boolean _b) {

    if (!visible_) {
      return;
    }
    final int nb = getNbPoint();
    if (nb == 0) {
      return;
    }
    int xp = 0;
    int yp = 0;
    final int xmin = _x;
    final int xmax = _x + _w;
    final int ymin = _y;
    final int ymax = _y + _h;
    final double xCoef = _w / (_ax.maximum_ - _ax.minimum_);
    final double yCoef = -((double) _h) / (_ay.maximum_ - _ay.minimum_);
    final double yBase = (double) _y + _h;
    final double xBase = _x;
    final Color old = _g.getColor();
    if (marqueurs_) {
      _g.setColor(getAspectTexte());
      for (int i = nb - 1; i >= 0; i--) {
        xp = (int) (xBase + xCoef * (getX(i) - _ax.minimum_));
        yp = (int) (yBase + yCoef * (getY(i) - _ay.minimum_));
        if ((xp >= xmin) && (xp <= xmax) && (yp >= ymin) && (yp <= ymax)) {
          _g.drawRect(xp - 1, yp - 1, 2, 2);
        }
      }
    }
    int x0 = (int) (xBase + xCoef * (getX(nb - 1) - _ax.minimum_));
    int y0 = (int) (yBase + yCoef * (getY(nb - 1) - _ay.minimum_));
    boolean x0InClip = (x0 >= xmin) && (x0 <= xmax) && (y0 >= ymin) && (y0 <= ymax);
    boolean xpInClip = false;
    _g.setColor(getAspectContour());
    for (int i = nb - 2; i >= 0; i--) {
      xp = (int) (xBase + xCoef * (getX(i) - _ax.minimum_));
      yp = (int) (yBase + yCoef * (getY(i) - _ay.minimum_));
      xpInClip = (xp >= xmin) && (xp <= xmax) && (yp >= ymin) && (yp <= ymax);
      if (x0InClip || xpInClip) {
        _g.drawLine(x0, y0, xp, yp);
      }
      x0InClip = xpInClip;
      x0 = xp;
      y0 = yp;

    }
    _g.setColor(old);
  }

  /**
   * @param _r tableau de taille 4
   */
  @Override
  public boolean getMinMax(final double[] _r) {
    Arrays.fill(_r, 0);
    final int n = getNbPoint() - 1;
    if (n < 0) {
      return false;
    }
    _r[0] = getX(n);
    _r[1] = _r[0];
    _r[2] = getY(n);
    _r[3] = _r[2];
    double t;
    for (int i = n - 2; i >= 0; i--) {
      t = getX(i);
      if (t < _r[0]) {
        _r[0] = t;
      }
      if (t > _r[1]) {
        _r[1] = t;
      }
      t = getY(i);
      if (t < _r[2]) {
        _r[2] = t;
      }
      if (t > _r[3]) {
        _r[3] = t;
      }
    }
    return true;
  }

}
