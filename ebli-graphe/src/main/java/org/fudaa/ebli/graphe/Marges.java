/*
 * @creation     1999-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

/**
 * Marges.
 *
 * @version $Revision: 1.7 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class Marges {
  public static Marges parse(final Lecteur _lin) {
    final Marges r = new Marges();
    String t = _lin.get();
    if (t.equals("marges")) {
      t = _lin.get();
    }
    if (!t.equals("{")) {
      System.err.println("Erreur de syntaxe: " + t);
    }
    t = _lin.get();
    while (!t.equals("") && !t.equals("}")) {
      if (t.equals("gauche")) {
        r.gauche_ = Integer.valueOf(_lin.get()).intValue();
      } else if (t.equals("droite")) {
        r.droite_ = Integer.valueOf(_lin.get()).intValue();
      } else if (t.equals("haut")) {
        r.haut_ = Integer.valueOf(_lin.get()).intValue();
      } else if (t.equals("bas")) {
        r.bas_ = Integer.valueOf(_lin.get()).intValue();
      } else {
        System.err.println("Erreur de syntaxe: " + t);
      }
      t = _lin.get();
    }
    return r;
  }
  public int bas_;
  public int droite_;
  public int gauche_;
  public int haut_;

  public Marges() {
    gauche_ = 60;
    droite_ = 20;
    haut_ = 40;
    bas_ = 30;
  }

  public Marges(final int _gauche, final int _haut, final int _droite, final int _bas) {
    super();
    gauche_ = _gauche;
    haut_ = _haut;
    droite_ = _droite;
    bas_ = _bas;
  }

  protected int getBas() {
    return bas_;
  }

  protected void setBas(final int _bas) {
    bas_ = _bas;
  }

  protected int getDroite() {
    return droite_;
  }

  protected void setDroite(final int _droite) {
    droite_ = _droite;
  }

  protected int getGauche() {
    return gauche_;
  }

  protected void setGauche(final int _gauche) {
    gauche_ = _gauche;
  }

  protected int getHaut() {
    return haut_;
  }

  protected void setHaut(final int _haut) {
    haut_ = _haut;
  }
}
