/*
 * @creation     1999-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Etiquette.
 *
 * @version $Revision: 1.7 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Guilaume Desnoix
 */
public class Etiquette {
  public String titre_;
  public boolean visible_;
  public double s_;

  public Etiquette() {
    titre_ = CtuluLibString.EMPTY_STRING;
    s_ = 0.;
    visible_ = true;
  }

  public static Vector parse(final Lecteur _lin) {
    final Vector r = new Vector(0, 1);
    String t = _lin.get();
    if (t.equals("etiquettes")) {
      t = _lin.get();
    }
    if (!t.equals("{")) {
      System.err.println("Erreur de syntaxe: " + t);
    }
    t = _lin.get();
    while (!t.equals("") && !t.equals("}")) {
      final Etiquette e = new Etiquette();
      e.s_ = Double.valueOf(t).doubleValue();
      e.titre_ = _lin.get();
      r.add(e);
      t = _lin.get();
    }
    return r;
  }

  protected double getS() {
    return s_;
  }

  protected void setS(final double _s) {
    s_ = _s;
  }

  protected String getTitre() {
    return titre_;
  }

  protected void setTitre(final String _titre) {
    titre_ = _titre;
  }

  protected boolean isVisible() {
    return visible_;
  }

  protected void setVisible(final boolean _visible) {
    visible_ = _visible;
  }
}
