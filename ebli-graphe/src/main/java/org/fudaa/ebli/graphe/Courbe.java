/*
 *  @creation     11 d�c. 2003
 *  @modification $Date: 2006-09-19 14:55:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibString;

/**
 * @author deniger
 * @version $Id: Courbe.java,v 1.12 2006-09-19 14:55:45 deniger Exp $
 */
public abstract class Courbe implements GrapheComponent {

  public String titre_;
  public boolean visible_;
  public String type_;
  public String trace_;
  public int axe_;
  public boolean marqueurs_;
  public Aspect aspect_;

  public Courbe() {
    titre_= CtuluLibString.EMPTY_STRING;
    visible_= true;
    type_= "courbe";
    trace_= "lineaire";
    axe_= 0;
    marqueurs_= true;
    aspect_= new Aspect();
    aspect_.contour_= Color.red;
  }
  public final boolean isCourbe() {
    return true;
  }
  @Override
  public int getAxeId() {
    return axe_;
  }

  @Override
  public Color getAspectContour() {
    return aspect_.contour_;
  }

  @Override
  public Color getAspectSurface() {
    return aspect_.surface_;
  }

  @Override
  public Color getAspectTexte() {
    return aspect_.texte_;
  }

  @Override
  public String getTitre() {
    return titre_;
  }

  @Override
  public boolean isVisible() {
    return visible_;
  }

  @Override
  public final boolean isLegendAvailable() {
    return true;
  }

  public String toString() {
    return titre_;
  }

  @Override
  public boolean setVisible(final boolean _r) {
    if(visible_!= _r){
      visible_= _r;
      return true;
    }
    return false;
  }

  @Override
  public boolean setAspectContour(final Color _c) {
    if(_c==null) {
      return false;
    }
    final Color init=aspect_.contour_;
    if( (init==_c) || (_c.equals(init))) {
      return false;
    }
    aspect_.contour_=_c;
    return true;
  }

  public abstract int getNbPoint();
  public abstract double getX(int _idx);
  public abstract double getY(int _idx);

  @Override
  public boolean getMinMax(final double[] _r) {
    Arrays.fill(_r, 0);
    final int n= getNbPoint() - 1;
    if (n < 0) {
      return false;
    }
    _r[0]= getX(n);
    _r[1]= _r[0];
    _r[2]= getY(n);
    _r[3]= _r[2];
    double t;
    for (int i= n - 1; i >= 0; i--) {
      t= getX(i);
      if (t < _r[0]) {
        _r[0]= t;
      }
      if (t > _r[1]) {
        _r[1]= t;
      }
      t= getY(i);
      if (t < _r[2]) {
        _r[2]= t;
      }
      if (t > _r[3]) {
        _r[3]= t;
      }
    }
    return true;
  }

}
