/*
 * @creation     2001-10-18
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;
import java.awt.Graphics;
import java.awt.print.PageFormat;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;

import org.fudaa.ebli.impression.EbliFilleImprimable;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author       Fred Deniger
 */
public abstract class EbliFilleGraphe extends EbliFilleImprimable {
  protected BGraphe graphe_;
  public EbliFilleGraphe(
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico,
    final BuCommonInterface _appli) {
    this(_titre, _re, _clo, _max, _ico, _appli, null);
  }
  public EbliFilleGraphe(
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico,
    final BuCommonInterface _appli,
    final BuInformationsDocument _id) {
    super(_titre, _re, _clo, _max, _ico, _appli, _id);
  }
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    return graphe_.print(_g, _format, _numPage);
  }
  @Override
  public int getNumberOfPages() {
    return 1;
  }
}
