/*
 * @creation     1997-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
/**
 * Valeur.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author       Guillaume Desnoix , Axel von Arnim
 */
public final class Valeur {
  public String titre_;
  public double s_;
  public double v_;
  public double vinf_;
  public double vsup_;
  public Aspect aspect_;
  
  public Valeur() {
    titre_= CtuluLibString.EMPTY_STRING;
  }
  
  public Valeur(double _s, double _v) {
    s_=_s;
    v_=_v;
    titre_= CtuluLibString.EMPTY_STRING;
  }
  
  public static Vector<Valeur> parse(final Lecteur _lin) {
    final Vector<Valeur> r= new Vector<Valeur>(0, 1);
    String t= _lin.get();
    if ("valeurs".equals(t)) {
      t= _lin.get();
    }
    if (!"{".equals(t)) {
      FuLog.error("Erreur de syntaxe: " + t);
    }
    t= _lin.get();
    while (!"".equals(t) && !"}".equals(t)) {
      if ("etiquette".equals(t)) {
        final Valeur e= (Valeur)r.get(r.size() - 1);
        e.titre_= _lin.get();
      } else if (t.equals("inf")) {
        final Valeur e= (Valeur)r.get(r.size() - 1);
        e.vinf_= Double.valueOf(_lin.get()).doubleValue();
      } else if (t.equals("sup")) {
        final Valeur e= (Valeur)r.get(r.size() - 1);
        e.vsup_= Double.valueOf(_lin.get()).doubleValue();
      } else if ("aspect".equals(t)) {
        final Valeur e= (Valeur)r.get(r.size() - 1);
        e.aspect_= Aspect.parse(_lin);
      } else {
        final Valeur e= new Valeur();
        e.s_= Double.valueOf(t).doubleValue();
        e.v_= Double.valueOf(_lin.get()).doubleValue();
        r.addElement(e);
      }
      t= _lin.get();
    }
    return r;
  }
  public static List<Valeur> parseXY(final Lecteur _lin) {
    final List<Valeur> r= new ArrayList<Valeur>(20);
    String t= _lin.get();
    while (!t.equals("")) {
      final Valeur e= new Valeur();
      e.s_= Double.valueOf(t).doubleValue();
      e.v_= Double.valueOf(_lin.get()).doubleValue();
      r.add(e);
      t= _lin.get();
    }
    return r;
  }
  public Aspect getAspect() {
    return aspect_;
  }
  public void setAspect(final Aspect _aspect) {
    aspect_ = _aspect;
  }
  public double getS() {
    return s_;
  }
  public void setS(final double _s) {
    s_ = _s;
  }
  public String getTitre() {
    return titre_;
  }
  public void setTitre(final String _titre) {
    titre_ = _titre;
  }
  public double getV() {
    return v_;
  }
  public void setV(final double _v) {
    v_ = _v;
  }
  public double getVinf() {
    return vinf_;
  }
  public void setVinf(final double _vinf) {
    vinf_ = _vinf;
  }
  public double getVsup() {
    return vsup_;
  }
  public void setVsup(final double _vsup) {
    vsup_ = _vsup;
  }
}
