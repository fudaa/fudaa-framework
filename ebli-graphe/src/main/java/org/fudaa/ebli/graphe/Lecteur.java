/*
 * @creation     1999-07-29
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.Stack;
/**
 * GDF Lecteur.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class Lecteur {
  private StreamTokenizer st_;
  private Stack pile_;
  /**
   * @deprecated use Lecteur(Reader) instead.
   */
  public Lecteur(final InputStream _is) {
    this(new InputStreamReader(_is));
  }
  public Lecteur(final Reader _reader) {
    st_= new StreamTokenizer(new BufferedReader(_reader));
    st_.resetSyntax();
    st_.eolIsSignificant(false);
    st_.quoteChar('"');
    st_.quoteChar('\'');
    st_.slashSlashComments(true);
    st_.slashStarComments(true);
    st_.wordChars('a', 'z');
    st_.wordChars('A', 'Z');
    st_.wordChars('0', '9');
    st_.wordChars('_', '_');
    st_.wordChars('.', '.');
    st_.wordChars('+', '+');
    st_.wordChars('-', '-');
    st_.wordChars('[', '[');
    st_.wordChars(']', ']');
    st_.whitespaceChars(' ', ' ');
    st_.whitespaceChars('\t', '\t');
    st_.whitespaceChars('\n', '\n');
    st_.whitespaceChars('\r', '\r');
    pile_= new Stack();
  }
  public String get() {
    String r= "";
    if (pile_.empty()) {
      try {
        st_.nextToken();
        // System.err.println(">>>"+st_.ttype+" "+st_.sval);
        switch (st_.ttype) {
          case StreamTokenizer.TT_EOF :
            r= "";
            break;
          case StreamTokenizer.TT_WORD :
            r= st_.sval;
            break;
          case StreamTokenizer.TT_NUMBER :
            r= "" + st_.nval;
            break;
          case '"' :
            r= st_.sval;
            break;
          case '\'' :
            r= "" + st_.sval;
            break;
          default :
            r= "" + (char)st_.ttype;
            break;
        }
      } catch (final Exception e) {
        System.err.println("Erreur: " + e);
      }
    } else {
      r= (String)pile_.pop();
    }
    return r;
  }
  void put(final String _s) {
    pile_.push(_s);
  }
}
