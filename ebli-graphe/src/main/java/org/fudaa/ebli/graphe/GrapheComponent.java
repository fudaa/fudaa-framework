/*
 *  @creation     11 d�c. 2003
 *  @modification $Date: 2006-04-12 15:28:01 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * @author deniger
 * @version $Id: GrapheComponent.java,v 1.4 2006-04-12 15:28:01 deniger Exp $
 */
public interface GrapheComponent {

  void dessine(Graphics2D _g, int _x, int _y, int _w, int _h, Axe _ax, Axe _ay, boolean _b);

  String getTitre();

  boolean isVisible();

  boolean setVisible(boolean _r);

  /**
   * Renvoie true, si la couleur a ete change.
   */
  boolean setAspectContour(Color _c);

  /**
   * Renvoie l'index de l'axe requis.
   * 
   * @return -1 si pas d'axe necessaire
   */
  int getAxeId();

  boolean isLegendAvailable();

  Color getAspectSurface();

  Color getAspectContour();

  Color getAspectTexte();

  /**
   * Initalise le tableau avec les donnees xmin,xmax,ymin,ymax.
   * 
   * @return true si les min max ont ete initialises
   */
  boolean getMinMax(double[] _xminXmaxYminYmax);

}
