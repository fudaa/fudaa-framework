/*
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2006-09-19 14:55:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.text.DecimalFormat;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuSplit2Pane;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author deniger
 * @version $Id: EbliFilleGrapheController.java,v 1.7 2006-09-19 14:55:45 deniger Exp $
 */
public class EbliFilleGrapheController extends EbliFilleImprimable {

  protected BGrapheController controller_;
  EbliActionInterface restoreAction_;
  JComponent[] specificTools_;

  /**
   *
   */
  public EbliFilleGrapheController() {
    this(true, true);
  }

  public EbliFilleGrapheController(final boolean _legend, final boolean _copyright) {
    super();
    controller_= new BGrapheController(_legend, _copyright);
    init();
  }

  /**
   * @param _titre
   * @param _re
   * @param _clo
   * @param _max
   * @param _ico
   */
  public EbliFilleGrapheController(
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico) {
    this(true, true, _titre, _re, _clo, _max, _ico);
  }

  public EbliFilleGrapheController(
    final boolean _legend,
    final boolean _copyright,
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico) {
    super(_titre, _re, _clo, _max, _ico);
    controller_= new BGrapheController(_legend, _copyright);
    init();
  }

  public EbliFilleGrapheController(
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico,
    final BuCommonInterface _appli,
    final BuInformationsDocument _id) {
    this(true, true, _titre, _re, _clo, _max, _ico, _appli, _id);
  }

  /**
   * @param _titre
   * @param _re
   * @param _clo
   * @param _max
   * @param _ico
   * @param _appli
   * @param _id
   */
  public EbliFilleGrapheController(
    final boolean _legend,
    final boolean _copyright,
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico,
    final BuCommonInterface _appli,
    final BuInformationsDocument _id) {
    super(_titre, _re, _clo, _max, _ico, _appli, _id);
    controller_= new BGrapheController(_legend, _copyright);
    init();
  }

  private void init() {
    final BuSplit2Pane split2pane= new BuSplit2Pane();
    split2pane.setOneTouchExpandable(true);
    final JComponent left= new BGrapheComponentListPanel(controller_);
    split2pane.setLeftComponent(left);
    left.setPreferredSize(new Dimension(100, 300));
    final BuPanel middle= new BuPanel();
    middle.setLayout(new BuBorderLayout());
    middle.setBorder(
        BorderFactory.createEtchedBorder());
    middle.add(controller_.createGrapheComponent());
    middle.setPreferredSize(new Dimension(500, 500));
    split2pane.setMiddleComponent(middle);
    split2pane.resetToPreferredSizes();
    final BuPanel container=new BuPanel();
    container.setLayout(new BuBorderLayout(5,5));
    container.add(split2pane,BuBorderLayout.CENTER);
    final GrapheInteractionSuiviSouris inter=controller_.activeSuiviSouris();
    inter.setFormat(new DecimalFormat("0.00"));
    container.add(inter.getLabel(),BuBorderLayout.SOUTH);
    container.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    container.doLayout();
    setContentPane(container);

  }

  public EbliActionInterface getRestaurerAction() {
    if (restoreAction_ == null) {
      restoreAction_=
        new EbliActionSimple(
          EbliResource.EBLI.getString("Restaurer"),
          EbliResource.EBLI.getIcon("loupe-etendue"),
          "RESTORE") {
        @Override
        public void actionPerformed(final ActionEvent _evt) {
          controller_.restaurer();
        }
      };
      restoreAction_.putValue(
        Action.SHORT_DESCRIPTION,
        EbliResource.EBLI.getString("Restaurer la vue globale"));
      restoreAction_.putValue(
        Action.ACCELERATOR_KEY,
        KeyStroke.getKeyStroke('r'));
    }
    return restoreAction_;
  }

  /**
   *
   */
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    return controller_.print(_g, _format, _page);
  }

  /**
   *
   */
  @Override
  public int getNumberOfPages() {
    return 1;
  }

  /**
   *
   */
  @Override
  public JComponent[] getSpecificTools() {
    if (specificTools_ == null) {
      specificTools_= new JComponent[2];
      specificTools_[0]=
        getRestaurerAction().buildToolButton(EbliComponentFactory.INSTANCE);
      specificTools_[1]=
        controller_.createChangeCouleurAction().buildToolButton(
          EbliComponentFactory.INSTANCE);
    }
    return specificTools_;
  }

}
