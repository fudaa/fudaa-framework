/*
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2006-09-19 14:55:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.graphe;

import com.memoire.fu.FuComparator;

/**
 * @author deniger
 * @version $Id: GrapheComponentComparator.java,v 1.4 2006-09-19 14:55:45 deniger Exp $
 */
public class GrapheComponentComparator implements FuComparator {

  public GrapheComponentComparator() {
    super();
  }

  @Override
  public int compare(final Object _a, final Object _b) {
    if(!(_a instanceof GrapheComponent)) {
      new IllegalArgumentException();
      return 1;
    }
    else if(!(_b instanceof GrapheComponent)) {
          new IllegalArgumentException();
          return -1;
        }
        else{
          return ((GrapheComponent)_a).getTitre().compareTo(((GrapheComponent)_b).getTitre());
        }
  }

}
