/*
 * @creation     2000-06-26
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Le gestionnaire d'interaction de BGraphe.
 *
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Axel von Arnim
 */
final class BGrapheInteraction implements MouseListener, MouseMotionListener {
  BGraphe bgraphe_;

  NumberFormat nf_;

  ContexteCoordonnees coordContext_, coordContextP_;

  public BGrapheInteraction(final BGraphe _b) {
    bgraphe_ = _b;
    coordContext_ = null;
    coordContextP_ = null;
    nf_ = NumberFormat.getInstance(Locale.US);
    nf_.setMaximumFractionDigits(2);
    nf_.setGroupingUsed(false);
  }

  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (coordContext_ == null) {
      coordContext_ = new ContexteCoordonnees();
      coordContextP_ = new ContexteCoordonnees();
      final Graphe g = bgraphe_.getGraphe();
      /*coordContextP_.g_ = g;
      coordContext_.g_ = g;*/
      coordContextP_.courbes_ = g.getElementsParType(new CourbeDefault().getClass());
      coordContext_.courbes_ = coordContextP_.courbes_;
      coordContextP_.axes_ = g.getElementsParType(new Axe().getClass());
      coordContext_.axes_ = coordContextP_.axes_;
      coordContextP_.x_ = g.marges_.gauche_;
      coordContext_.x_ = coordContextP_.x_;
      coordContext_.y_ = g.marges_.haut_;
      coordContextP_.y_ = g.marges_.haut_;
      coordContextP_.w_ = bgraphe_.getWidth() - g.marges_.gauche_ - g.marges_.droite_;
      coordContext_.w_ = coordContextP_.w_;
      coordContextP_.h_ = bgraphe_.getHeight() - g.marges_.haut_ - g.marges_.bas_;
      coordContext_.h_ = coordContextP_.h_;
      // recherche des axes
      for (int ia = 0; ia < coordContext_.axes_.length; ia++) {
        final Axe a = (Axe) coordContext_.axes_[ia];
        if (a.vertical_) {
          coordContextP_.ay_ = a;
          coordContext_.ay_ = coordContextP_.ay_;
        } else {
          coordContextP_.ax_ = a;
          coordContext_.ax_ = coordContextP_.ax_;
        }
      }
    }
    // recherche du point et de la courbe les plus proches
    final Point p = _evt.getPoint();
    coordContextP_.vs_ = coordContext_.vs_;
    coordContextP_.vrs_ = coordContext_.vrs_;
    coordContextP_.ivs_ = coordContext_.ivs_;
    coordContextP_.cs_ = coordContext_.cs_;
    cherchePoint(coordContext_, p);
    if ((coordContext_.cs_ == null) || (coordContext_.vs_ == null)) {
      return;
    }
    final Graphics gr = bgraphe_.getGraphics();
    gr.setXORMode(bgraphe_.getBackground());
    gr.setColor(new Color(255, 0, 0));
    if (coordContextP_.vrs_ != null) {
      dessinePointCoord(coordContextP_, gr);
    }
    dessinePointCoord(coordContext_, gr);
  }

  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  @Override
  public void mouseExited(final MouseEvent _evt) {}

  @Override
  public void mouseClicked(final MouseEvent _evt) {}

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if ((coordContext_ != null) && (coordContext_.cs_ != null) && (coordContext_.vs_ != null)) {
      final Graphics gr = bgraphe_.getGraphics();
      gr.setXORMode(bgraphe_.getBackground());
      gr.setColor(new Color(255, 0, 0));
      dessinePointCoord(coordContext_, gr);
    }
    coordContext_ = null;
  }

  @Override
  public void mouseDragged(final MouseEvent _evt) {
    // recherche du point et de la courbe les plus proches
    final Point p = _evt.getPoint();
    coordContextP_.vs_ = coordContext_.vs_;
    coordContextP_.vrs_ = coordContext_.vrs_;
    coordContextP_.ivs_ = coordContext_.ivs_;
    coordContextP_.cs_ = coordContext_.cs_;
    cherchePoint(coordContext_, p);
    if (coordContext_.ivs_ == coordContextP_.ivs_) {
      return;
    }
    if ((coordContext_.cs_ == null) || (coordContext_.vs_ == null)) {
      return;
    }
    final Graphics gr = bgraphe_.getGraphics();
    gr.setXORMode(bgraphe_.getBackground());
    gr.setColor(new Color(255, 0, 0));
    dessinePointCoord(coordContextP_, gr);
    dessinePointCoord(coordContext_, gr);
  }

  @Override
  public void mouseMoved(final MouseEvent _evt) {}

  private void cherchePoint(final ContexteCoordonnees _cx, final Point _p) {
    double d = Double.POSITIVE_INFINITY, dtmp = 0.;
    if (_cx.cs_ == null) {
      for (int i = 0; i < _cx.courbes_.length; i++) {
        final CourbeDefault c = (CourbeDefault) _cx.courbes_[i];
        if (!c.visible_) {
          continue;
        }
        for (int j = 0; j < c.valeurs_.size(); j++) {
          final Valeur v = (Valeur) c.valeurs_.get(j);
          final Point vr = versEcran(v, _cx);
          dtmp = _p.distanceSq(vr.x, vr.y);
          if (dtmp < d) {
            d = dtmp;
            _cx.cs_ = c;
            _cx.vs_ = v;
            _cx.ivs_ = j;
            _cx.vrs_ = vr;
          }
        }
      }
    } else {
      for (int j = 0; j < _cx.cs_.valeurs_.size(); j++) {
        final Valeur v = (Valeur) _cx.cs_.valeurs_.get(j);
        final Point vr = versEcran(v, _cx);
        dtmp = _p.distanceSq(vr.x, vr.y);
        if (dtmp < d) {
          d = dtmp;
          _cx.vs_ = v;
          _cx.ivs_ = j;
          _cx.vrs_ = vr;
        }
      }
    }
  }

  private Point versEcran(final Valeur _v, final ContexteCoordonnees _cx) {
    final int yv = _cx.y_ + _cx.h_ - (int) (_cx.h_ * ((_v.v_ - _cx.ay_.minimum_) / (_cx.ay_.maximum_ - _cx.ay_.minimum_)));
    final int xv = _cx.x_ + (int) (_cx.w_ * ((_v.s_ - _cx.ax_.minimum_) / (_cx.ax_.maximum_ - _cx.ax_.minimum_)));
    return new Point(xv, yv);
  }

  private void dessinePointCoord(final ContexteCoordonnees _cx, final Graphics _gr) {
    Font f = new Font("Dialog", Font.BOLD, 9);
    _gr.setFont(f);
    FontMetrics fm = _gr.getFontMetrics();
    final int dl = 3;
    _gr.drawLine(_cx.x_ - 2, _cx.vrs_.y, _cx.vrs_.x - dl, _cx.vrs_.y);
    String t = nf_.format(_cx.vs_.v_);
    _gr.drawString(t, _cx.x_ - 2 - fm.stringWidth(t), _cx.vrs_.y);
    _gr.drawLine(_cx.vrs_.x, _cx.y_ + _cx.h_ + 2, _cx.vrs_.x, _cx.vrs_.y + dl);
    t = nf_.format(_cx.vs_.s_);
    _gr.drawString(t, _cx.vrs_.x - fm.stringWidth(t) / 2, _cx.y_ + _cx.h_ + 2 + 2 * 10);
    _gr.drawArc(_cx.vrs_.x - dl, _cx.vrs_.y - dl, 2 * dl + 1, 2 * dl + 1, 0, 360);
    t = "" + (_cx.ivs_ + 1);
    f = new Font(f.getName(), f.getStyle(), 14);
    _gr.setFont(f);
    fm = _gr.getFontMetrics();
    _gr.drawString(t, _cx.vrs_.x - fm.stringWidth(t) / 2, _cx.vrs_.y - dl - 1);
  }

  static class ContexteCoordonnees {
   // Graphe g_;
    Object[] axes_;
    Object[] courbes_;
    int x_;
    int y_;
    int w_;
    int h_;
    Axe ax_;
    Axe ay_;
    Valeur vs_;
    int ivs_;
    Point vrs_;
    CourbeDefault cs_;
  }
}
