/*
 * @creation     1999-02-24
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe.example;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileReader;
import java.io.Reader;

import javax.swing.JFrame;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;

/**
 * Graphe Definition File.
 *
 * @version $Revision: 1.6 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class GrapheExample extends JFrame implements Runnable {
  String erreur_;
  int temps_;
  int w_, h_;
  Thread animation_;
  Image tampon_;
  public Graphe graphe_;
  Lecteur lin_;
  String file_;

  public GrapheExample(final String _f) {
    super();
    file_ = _f;
    setSize(new Dimension(640, 480));
    setVisible(true);
    init();
  }

  public void error(final String _m) {
    erreur_ = "Erreur: " + getClass() + CtuluLibString.DOT + _m;
    System.err.println(erreur_);
  }

  public void init() {
    erreur_ = "";
    temps_ = 0;
    w_ = getSize().width;
    h_ = getSize().height;
    tampon_ = createImage(w_, h_);
    try {
      final Reader is = new FileReader(file_);
      lin_ = new Lecteur(is);
      if (file_.endsWith(".gdf")) {
        parse();
      } else {
        parseXY();
      }
    } catch (final Exception e) {
      e.printStackTrace();
    }
    if ((graphe_ != null) && !graphe_.animation_) {
      maj();
    }

    final BGraphe bgraphe = new BGraphe();
    bgraphe.setSize(w_, h_);
    bgraphe.setSize(getSize());
    graphe_.titre_ = "Courbe de de dzergfset sfdsg g fdsf sdf sdfsd sg qgq ";
    bgraphe.setGraphe(graphe_);
    bgraphe.setInteractif(true);
    setContentPane(bgraphe);
  }

  public void parse() {
    String t = lin_.get();
    while (!t.equals("")) {
      if (t.equals("graphe")) {
        graphe_ = Graphe.parse(lin_, null);
      } else {
        System.err.println("Erreur de syntaxe: " + t);
      }
      t = lin_.get();
    }
  }

  public void parseXY() {
    graphe_ = Graphe.parseXY(lin_);
  }

  public void maj() {
    final Graphics g = tampon_.getGraphics();
    if (graphe_ != null) {
      graphe_.dessine(g, 0, 0, w_, h_, temps_, null);
    }
  }

  @Override
  public void update(final Graphics _g) {
    paint(_g);
  }

  public void start() {
    if ((graphe_ != null) && graphe_.animation_) {
      animation_ = new Thread(this);
      animation_.start();
    }
  }

  public void stop() {
    if ((graphe_ != null) && graphe_.animation_) {
      graphe_.animation_ = false;
    }
  }

  @Override
  public void run() {
    if ((graphe_ != null) && graphe_.animation_) {
      while (graphe_.animation_) {
        final long avant = System.currentTimeMillis();
        maj();
        repaint();
        temps_++;
        final long apres = System.currentTimeMillis();
        System.err.println("Temps = " + (apres - avant));
        if (apres - avant < graphe_.vitesse_) {
          try {
            Thread.sleep(graphe_.vitesse_ + avant - apres);
          } catch (final InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }
  }

  public static void main(final String[] _args) {
    if (_args.length < 1) {
      System.err.println("Usage: java TestGraphe <fichier>");
      System.exit(1);
    }
    final GrapheExample frame = new GrapheExample(_args[0]);
    final JFrame dl = new JFrame();
    final BGraphePersonnaliseur p = new BGraphePersonnaliseur(frame.graphe_);
    p.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(final PropertyChangeEvent _evt) {
        frame.maj();
        frame.repaint();
      }
    });
    dl.setContentPane(p);
    dl.pack();
    dl.setVisible(true);
    frame.start();
  }
}
