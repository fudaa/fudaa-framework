/*
 * @file         TestFrameImpression.java
 * @creation     2001-06-28
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe.example;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;
/**
 * @version      $Id: TestFrameImpression.java,v 1.2 2006-10-19 14:13:24 deniger Exp $
 * @author       Fred Deniger
 */
public class FrameImpressionExample
  extends JFrame
  implements ActionListener, Printable {
  /**
   * GrapheFrame
   */
  public FrameImpressionExample() {
    super();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    final JMenuBar menu= new JMenuBar();
    final JMenuItem imprime= new JMenuItem("Imprimer");
    imprime.addActionListener(this);
    final JMenuItem change= new JMenuItem("Changer le mode");
    change.addActionListener(this);
    menu.add(imprime);
    menu.add(change);
    setJMenuBar(menu);
    setSize(new Dimension(640, 480));
    setVisible(true);
  }
  /**
   * ....
   *
   * @param _comp
   */
  private void imprimer(final Printable _comp) {
    final PrinterJob printJob= PrinterJob.getPrinterJob();
    printJob.setPrintable(_comp);
    if (printJob.printDialog()) {
      try {
        printJob.print();
        System.out.println("fini");
      } catch (final Exception PrintException) {
        PrintException.printStackTrace();
      }
    }
  }
  /**
   * ....
   *
   * @param _fichier
   */
  public void loadFichier(final String _fichier) {
    try {
      final Graphe g=
        Graphe.parse(new Lecteur(new java.io.FileReader(_fichier)), null);
      final BGraphe bg= new BGraphe();
      bg.setGraphe(g);
      setContentPane(bg);
      bg.revalidate();
    } catch (final IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }
  /**
   * ....
   */
  public void chargerTableau() {
    final JTable table= new JTable(10, 10);
    for (int i= 0; i < table.getColumnCount(); i++) {
      for (int j= 0; j < table.getRowCount(); j++) {
        table.setValueAt("Essai col" + i + " ligne" + j, i, j);
      }
    }
    setContentPane(table);
    table.revalidate();
  }
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    System.err.println("passage" + _numPage);
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    System.out.println(_g);
    ((BGraphe)getContentPane()).print(_g, _format, _numPage);
    return Printable.PAGE_EXISTS;
  }
  /**
   * ....
   *
   * @param _e
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("Imprimer".equals(_e.getActionCommand())) {
      System.out.println("Impression demand�e");
      imprimer(this);
      /*
      Component cont = getContentPane();
      if(cont instanceof Printable)
      {
        System.out.println("\nImprimable");
        //imprimer((Printable)cont);
        imprimer(this)
      }
      else
      {
        System.out.println("\nNon imprimable");
      }
      }
      else if("Changer le mode".equals(_e.getActionCommand()))
      {
      //      if(getContentPane() instanceof BGraphe)
      //      {
      //        System.out.println("changement de mode");
      //        ((BGrapheEssai)getContentPane()).changeMode();
      //      }
      }*/
    }
  }
  /**
   * main de TestFrameImpression class
   *
   * @param args  The command line arguments
   */
  public static void main(final String[] args) {
    if (args.length == 0) {
      System.out.println("Usage: TestFrameImpression <fichier>");
      System.exit(1);
    }
    final FrameImpressionExample frame= new FrameImpressionExample();
    //frame.chargerTableau();
    frame.loadFichier(args[0]);
  }
}
