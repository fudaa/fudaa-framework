/*
 * @file         TestGraphe.java
 * @creation     1999-02-24
 * @modification $Date: 2006-12-05 10:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.graphe.example;

import java.applet.Applet;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;

import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenuBar;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuResource;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Lecteur;
import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.impression.EbliPageableDelegate;

/**
 * Graphe Definition File.
 *
 * @version $Revision: 1.7 $ $Date: 2006-12-05 10:14:36 $ by $Author: deniger $
 * @author Axel von Arnim
 * @see Applet
 */
public class BGrapheExample extends JFrame implements ActionListener {
  File defaultHome_ = new File(System.getProperty("user.home"));
  BuInformationsSoftware is_;
  JDesktopPane desk_;

  public BGrapheExample() {
    this(null);
  }

  public BGrapheExample(final String _fichier) {
    super("Visualisation graphe");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    desk_ = new JDesktopPane();
    setSize(new Dimension(640, 480));
    setContentPane(desk_);
    final BuButton open = new BuButton("ouvrir");
    open.setActionCommand("OPEN");
    open.addActionListener(this);
    final JMenuBar menu = new JMenuBar();
    menu.add(open);
    setJMenuBar(menu);
    if (_fichier != null) {
      litFichier(_fichier);
    }
    setVisible(true);
  }

  public void addInternal(final JInternalFrame _g) {
    _g.setVisible(true);
    desk_.add(_g);
  }
  class IFrameGraphe extends BuInternalFrame implements EbliPageable {
    EbliFillePrevisualisation previsu_;
    public BGraphe graphe_;
    EbliPageableDelegate delegueImpression_;

    public IFrameGraphe(final BGraphe _graphe) {
      super();
      graphe_ = _graphe;
      delegueImpression_ = new EbliPageableDelegate(this);
      this.setSize(new Dimension(640, 480));
      graphe_.setInteractif(true);
      this.setContentPane(graphe_);
      final BuButton open = new BuButton("ouvrir");
      open.setActionCommand("OPEN");
      open.addActionListener(this);
      final BuButton pers = new BuButton("perso");
      pers.setActionCommand("PERSO");
      pers.addActionListener(this);
      final BuButton impr = new BuButton("imprimer");
      impr.setActionCommand("IMPRIMER");
      impr.addActionListener(this);
      final BuButton prev = new BuButton("previsualiser");
      prev.setActionCommand("PREVISUALISER");
      prev.addActionListener(this);
      final BuButton fermer = new BuButton("fermer");
      fermer.setActionCommand("FERMER");
      fermer.addActionListener(this);
      final JMenuBar menu = new JMenuBar();
      menu.add(open);
      menu.add(pers);
      menu.add(impr);
      menu.add(prev);
      menu.add(fermer);
      this.setJMenuBar(menu);
    }

    private void openPersonnaliseur() {
      final Graphe gr = graphe_.getGraphe();
      if (gr == null) {
        return;
      }
      final JDialog dl = new JDialog(BGrapheExample.this);
      final BGraphePersonnaliseur p = new BGraphePersonnaliseur(gr);
      p.addPropertyChangeListener(new PropertyChangeListener() {
        @Override
        public void propertyChange(final PropertyChangeEvent _evt) {
          graphe_.fullRepaint();
        }
      });
      dl.setContentPane(p);
      dl.pack();
      dl.setVisible(true);
    }

    @Override
    public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
      return graphe_.print(_g, _format, _numPage);
    }

    @Override
    public int getNumberOfPages() {
      return 1;
    }

    @Override
    public Printable getPrintable(final int _i) {
      if (delegueImpression_ == null) {
        delegueImpression_ = new EbliPageableDelegate(this);
      }
      return delegueImpression_.getPrintable(_i);
    }

    @Override
    public PageFormat getPageFormat(final int _i) {
      if (delegueImpression_ == null) {
        delegueImpression_ = new EbliPageableDelegate(this);
      }
      return delegueImpression_.getPageFormat(_i);
    }

    @Override
    public EbliPageFormat getDefaultEbliPageFormat() {
      if (delegueImpression_ == null) {
        delegueImpression_ = new EbliPageableDelegate(this);
      }
      return delegueImpression_.getDefaultEbliPageFormat();
    }

    @Override
    public BuInformationsDocument getInformationsDocument() {
      return graphe_.getInformationsDocument();
    }

    @Override
    public BuInformationsSoftware getInformationsSoftware() {
      return BGrapheExample.this.is_;
    }

    private void gestionnaireImpression(final String _commande) {
      if ("IMPRIMER".equals(_commande)) {
        cmdImprimer(this);
      } else if ("MISEENPAGE".equals(_commande)) {
        cmdMiseEnPage(this);
      } else if ("PREVISUALISER".equals(_commande)) {
        cmdPrevisualisation(this);
      }
    }

    /**
     * Impression de la page <code>_target</code> dans un nouveau thread.
     */
    public void cmdImprimer(final EbliPageable _target) {
      final PrinterJob printJob = PrinterJob.getPrinterJob();
      printJob.setPageable(_target);
      if (printJob.printDialog()) {
        new Thread(BuResource.BU.getString("Impression")) {
          @Override
          public void run() {
            try {
              printJob.print();
            } catch (final Exception _printException) {
              _printException.printStackTrace();
            }
          }
        }.start();
      }
    }

    public void cmdMiseEnPage(final EbliPageable _target) {
      new EbliMiseEnPageDialog(_target, null, null).activate();
    }

    public void cmdPrevisualisation(final EbliPageable _target) {
      if (previsu_ == null) {
        previsu_ = new EbliFillePrevisualisation(null, _target);
        final JMenuBar me = new JMenuBar();
        final JComponent[] meI = previsu_.getSpecificTools();
        for (int i = 0; i < meI.length; i++) {
          me.add(meI[i]);
        }
        previsu_.setJMenuBar(me);
      }
      BGrapheExample.this.addInternal(previsu_);
      previsu_.moveToFront();
    }

    @Override
    public void actionPerformed(final ActionEvent _ae) {
      final String com = _ae.getActionCommand();
      if ("PERSO".equals(com)) {
        openPersonnaliseur();
      } else if (("PREVISUALISER".equals(com)) || ("MISEENPAGE".equals(com)) || ("IMPRIMER".equals(com))) {
        gestionnaireImpression(com);
      } else if ("FERMER".equals(com)) {
        this.dispose();
      }
    }
  }

  private void litFichier(final String _fic) {
    Graphe g = null;
    try {
      final Reader is = new FileReader(_fic);
      final Lecteur lin = new Lecteur(is);
      if (_fic.endsWith(".gdf")) {
        g = parse(lin);
      } else {
        g = parseXY(lin);
      }
    } catch (final Exception e) {
      e.printStackTrace();
    }
    if (g != null) {
      g.copyright_ = false;
      final BGraphe bf = new BGraphe();
      bf.setGraphe(g);
      final IFrameGraphe f = new IFrameGraphe(bf);
      f.setSize(new Dimension(540, 380));
      addInternal(f);
    } else {
      new BuDialogError(null, null, "Erreur de lecture").activate();
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    final String com = _ae.getActionCommand();
    if ("OPEN".equals(com)) {
      open();
    }
  }

  public static Graphe parse(final Lecteur _l) {
    Graphe r = null;
    String t = _l.get();
    while (!t.equals("")) {
      if (t.equals("graphe")) {
        r = Graphe.parse(_l, null);
      } else {
        System.err.println("Erreur de syntaxe: " + t);
      }
      t = _l.get();
    }
    return r;
  }

  public static Graphe parseXY(final Lecteur _l) {
    return Graphe.parseXY(_l);
  }

  private void open() {
    final BuFileChooser f = new BuFileChooser();
    f.setDialogType(JFileChooser.OPEN_DIALOG);
    f.setCurrentDirectory(defaultHome_);
    f.setFileSelectionMode(JFileChooser.FILES_ONLY);
    final int i = f.showOpenDialog(this);
    if (i == JFileChooser.APPROVE_OPTION) {
      final File file = f.getSelectedFile();
      final File parent = file.getParentFile();
      if (parent != null) {
        defaultHome_ = parent;
      }
      litFichier(file.getAbsolutePath());
    }
  }

  public BuInformationsSoftware getInformationsSoftware() {
    if (is_ == null) {
      is_ = new BuInformationsSoftware();
    }
    return is_;
  }

  public static void main(final String[] _args) {
    if (_args.length < 1) {
      new BGrapheExample();
    } else {
      new BGrapheExample(_args[0]);
    }
  }
}
