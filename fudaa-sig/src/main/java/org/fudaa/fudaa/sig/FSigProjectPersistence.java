/*
 * @creation 7 juin 07
 * @modification $Date: 2008-01-09 15:53:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig;

import com.db4o.ObjectContainer;
import com.memoire.fu.FuLog;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JInternalFrame;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluSavable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.save.FudaaSavable;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.commun.save.FudaaSaveZipWriter;

/**
 * G�re la persistence du projet. Permet la sauvegarde ou la restitution du projet
 * dans un format zipp�, suivant une structure g�n�rique propos�e par F.Deniger.
 * 
 * @author fred deniger
 * @version $Id: FSigProjectPersistence.java,v 1.2 2008-01-09 15:53:17 bmarchan Exp $
 */
public class FSigProjectPersistence {

  /**
   * Sauvegarde du projet dans un fichier.
   * @param _impl La classe d'impl�mentation de l'application.
   * @param _proj Le projet.
   * @param _destFile Le fichier destination, avec l'extension.
   * @param _prog Pour le suivi de la tache de sauvegarde.
   * @return true si la sauvegarde s'est bien d�roul�e.
   */
  public static boolean saveProject(final FudaaCommonImplementation _impl, final FSigProjet _proj,
      final File _destFile, final ProgressionInterface _prog) {
    if (_proj == null) {
      return false;
    }
    final File zipFile = _destFile;
    // on verifie si on peut ecrire
    final String err = CtuluLibFile.canWrite(zipFile);
    if (err != null) {
      _impl.error(FudaaSaveLib.getActionSaveTitle(), err, false);
      return false;
    }
    if (zipFile.exists()) {
      zipFile.delete();
    }
    FudaaSaveZipWriter writer = null;
    // final ObjectContainer cont = Db4o.openFile(dbFile.getAbsolutePath());
    try {
      writer = new FudaaSaveZipWriter(zipFile);
      FSigLib.addReadmeFile(writer);
      FudaaSaveProject.saveIn(writer, _proj.getSystemVersion());
      FudaaSaveProject.saveIn(writer, _proj.getInformationsDocument(), _impl.getInformationsSoftware());
      final ObjectContainer cont = writer.getDb();
      // final FudaaSaveMainData data = new FudaaSaveMainData();
      // data.infoDoc_ = _proj.getInformationsDocument();
      // data.system_ = _proj.getSystemVersion();
      if (cont != null) {
        cont.set(_impl.getInformationsSoftware());
      }
      // pour ne pas sauvegarder deux fois les memes donn�es
      final Set compSaved = new HashSet();
      final JInternalFrame[] iframes = _impl.getAllInternalFrames();
      if (iframes != null) {
        for (int i = iframes.length - 1; i >= 0; i--) {
          if (iframes[i] instanceof FudaaSavable && !compSaved.contains(iframes[i])) {
            compSaved.add(iframes[i]);
            ((CtuluSavable) iframes[i]).saveIn(writer, _prog);
          }
        }
      }
      final CtuluSavable[] compToSave = _proj.getSavableComponent();
      if (compToSave != null) {
        for (int i = compToSave.length - 1; i >= 0; i--) {
          if (compToSave[i] != null && !compSaved.contains(compToSave[i])) {
            compSaved.add(compToSave[i]);
            compToSave[i].saveIn(writer, _prog);
          }
        }
      }
      // ancien format a effacer
      final File db = FSigProjectPersistence.getPreSaveFileOld(_destFile);
      if (db.exists()) {
        db.delete();
      }
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      String ioerr = _evt.getLocalizedMessage();
      if (CtuluLibString.isEmpty(ioerr)) {
        ioerr = _evt.getMessage();
      }
      _impl.error(FudaaSaveLib.getActionSaveTitle(), ioerr, false);

    } finally {
      if (writer != null) {
        try {
          writer.close();
        } catch (final IOException _evt) {
          FuLog.error(_evt);

        }
      }
    }
    return true;
  }

  /**
   * Chargement du projet depuis un fichier.
   * @param _impl La classe d'impl�mentation de l'application.
   * @param _proj Le projet.
   * @param _srcFile Le fichier source, avec l'extension.
   * @param _prog Pour le suivi de la tache de chargement.
   * @return true si le chargement s'est bien d�roul�.
   */
  public static boolean loadProject(final FudaaCommonImplementation _impl, final FSigProjet _proj, final File _srcFile,
      final ProgressionInterface _prog) {
    /** @todo A implementer pour la symetrie avec la sauvegarde. */
    return false;
    
  }

  public static File getPreSaveFileOld(final File _file) {
    if (_file == null) { return null; }
    return new File(_file.getAbsolutePath() + ".pre.fdb");
  }
}
