/*
 *  @creation     26 ao�t 2005
 *  @modification $Date: 2007-01-19 13:14:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.persistence;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;

/**
 * @author Fred Deniger
 * @version $Id: FSigLayerLinePersistence.java,v 1.5 2007-01-19 13:14:23 deniger Exp $
 */
public class FSigLayerLinePersistence extends FSigLayerPointPersistence {

  public FSigLayerLinePersistence() {}


  @Override
  protected GISZoneCollection createInitCollection() {
    return new GISZoneCollectionLigneBrisee();
  }

  
  

  @Override
  protected BCalque addInParent(final FSigLayerGroup _gr, final BCalqueSaverInterface _cqName, final GISZoneCollection _collection) {
    return _gr.addLigneBriseeLayerAct(_cqName.getUI().getTitle(),(GISZoneCollectionLigneBrisee) _collection, null);
  }

  

}
