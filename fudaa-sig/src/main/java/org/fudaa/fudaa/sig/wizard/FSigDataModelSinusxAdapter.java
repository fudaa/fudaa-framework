/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2008-04-01 17:11:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelMultiAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZone;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPolygone;
import org.fudaa.ctulu.gis.GISZoneCollectionPolyligne;

/**
 * @author Fred Deniger
 * @version $Id: FSigDataModelSinusxAdapter.java,v 1.2.6.1 2008-04-01 17:11:49 bmarchan Exp $
 */
public class FSigDataModelSinusxAdapter /**implements GISDataModel**/ {

  private Geometry[] geoms_;
  private GISAttributeModel[] model_;

  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {}

  public Envelope getEnvelopeInternal() {
    return GISLib.computeEnveloppe(geoms_);
  }

  /**
   * Cr�ation de mod�le a partir des donn�es lues. Un attribut est ajout� (ATT_NUM_BLOC) pour regrouper
   * les points par bloc.
   * 
   * @param _zi les zones sources
   * @param _isPointAccepted true si les points sont acceptes dans le modele
   * @param _isPolyligneAccepted true si les polylignes sont acceptees dans le modele
   * @param _isPolygoneAccepted true si les polygones sont acceptees dans le modele
   * @param _progress la barre de progression
   * @param _res le resultat peut etre null
   * @return le model, null si aucune g�om�trie.
   */
  public static GISDataModel analyseSxFile(final GISZone _zi, final boolean _isPointAccepted,
      final boolean _isPolyligneAccepted, final boolean _isPolygoneAccepted, final ProgressionInterface _progress,
      final FSigFileLoadResult _res) {
    final GISZoneCollectionPolygone[] polygones = _zi.getPolygoneCollections();
    // on parcourt les polygones
    if (polygones != null && _isPolygoneAccepted) {
      if (polygones.length==0) return null;
      final int nbPolyg = polygones.length;
      for (int j = 0; j < nbPolyg; j++) {
        _res.nbPolygones_+=polygones[j].getNumGeometries();
        _res.nbPointTotal_+=polygones[j].getNumPoints();
      }
      GISDataModel mdl=new GISDataModelMultiAdapter(polygones);
      return mdl;
    }
    final GISZoneCollectionPolyligne[] polyligne = _zi.getPolyligneCollections();
    // on parcourt les polygones
    if (polyligne != null && _isPolyligneAccepted) {
      if (polyligne.length==0) return null;
      final int nbPolyg = polyligne.length;
      for (int j = 0; j < nbPolyg; j++) {
        _res.nbPolylignes_+=polyligne[j].getNumGeometries();
        _res.nbPointTotal_+=polyligne[j].getNumPoints();
      }
      GISDataModel mdl=new GISDataModelMultiAdapter(polyligne);
      return mdl;
    }
    final GISZoneCollectionPoint[] pts = _zi.getPointCollections();
    if (pts != null && _isPointAccepted) {
      final int nbPts = pts.length;
      if (pts.length==0) return null;
      for (int j = 0; j < nbPts; j++) {
        _res.nbPoint_+=pts[j].getNumPoints();
        _res.nbPointTotal_+=pts[j].getNumPoints();
      }
      GISDataModel mdl=new GISDataModelMultiAdapter(pts);
      return mdl;
    }
        
    return null;
  }


  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return model_ == null ? null : model_[_idxAtt].getAttribute();
  }

  public Object[] getDataModel(final int _idxAtt) {
    return model_ == null ? null : GISLib.getValues(model_[_idxAtt]);
  }

  public double[] getDataModelDouble(final int _idxAtt) {
    return null;

  }

  public Object[] getDataModelForGeom(final int _idxGeom) {
    final Object[] r = new Object[1];
    r[0] = model_[0].getObjectValueAt(_idxGeom);
    return r;

  }

  public Geometry getGeometry(final int _idxGeom) {
    return geoms_[_idxGeom];
  }

  public int getIndiceOf(final GISAttributeInterface _att) {
    for (int i=0; i<getNbAttributes(); i++) {
      if (_att.equals(model_[i].getAttribute())) return i;
    }
    return -1;
  }

  public int getNbAttributes() {
    return model_ == null ? 0 : model_.length;
  }

  public int getNumGeometries() {
    return geoms_.length;
  }

  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return model_[_idxAtt].getObjectValueAt(_idxGeom);
  }

  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return ((CtuluCollectionDouble) model_[_idxAtt]).getValue(_idxGeom);
  }
}
