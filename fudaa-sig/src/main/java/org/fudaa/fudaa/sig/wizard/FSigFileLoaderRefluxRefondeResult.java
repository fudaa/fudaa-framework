/*
 * @creation 19 juin 07
 * @modification $Date: 2007-06-20 12:23:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.dodico.reflux.io.RefluxRefondeResultatGisAdapter;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionFileFormat;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionNewReader;
import org.fudaa.dodico.reflux.io.RefluxRefondeSolutionSequentielResult;
import org.fudaa.dodico.reflux.io.RefluxSolutionSequentielReader;
import org.fudaa.dodico.refonde.io.RefondeINPResult;
import org.fudaa.dodico.refonde.io.RefondeQuickINPReader;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author fred deniger
 * @version $Id: FSigFileLoaderRefluxRefondeResult.java,v 1.1 2007-06-20 12:23:41 deniger Exp $
 */
public class FSigFileLoaderRefluxRefondeResult implements FSigFileLoaderI {

  final BuFileFilter ft_;

  public FSigFileLoaderRefluxRefondeResult(final BuFileFilter _ft) {
    super();
    ft_ = _ft;
  }

  public FSigFileLoaderRefluxRefondeResult() {
    ft_ = RefluxRefondeSolutionFileFormat.getInstance().createFileFilter();

  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderRefluxRefondeResult(ft_);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  @Override
  public void setInResult(FSigFileLoadResult _r, File _f, String _fileOrigine, ProgressionInterface _prog, CtuluAnalyze _analyze) {
    final File inp = CtuluLibFile.changeExtension(_f, "inp");
    if (!CtuluLibFile.exists(inp)) {
      _analyze.addFatalError(FSigLib.getS("Le fichier inp est requis pour la lecture des résultats."));
      return;
    }
    boolean isSeiche = false;
    H2dVariableType firstVar = H2dVariableType.PHASE;
    EfGridInterface g = null;
    if (RefluxRefondeSolutionSequentielResult.isRefonde(_f)) {
      CtuluIOOperationSynthese s = new RefondeQuickINPReader().read(inp, _prog);
      if (s.containsMessages()) _analyze.merge(s.getAnalyze());
      if (s.containsSevereError()) return;
      final RefondeINPResult res = (RefondeINPResult) s.getSource();
      g = res.getGrid();
      isSeiche = res.isModuleSeiche();
      firstVar = res.getVariableForFirstCol();
    } else {

      CtuluIOOperationSynthese s = INPFileFormat.getInstance().getLastINPVersionImpl().readGrid(inp, _prog);
      if (s.containsMessages()) _analyze.merge(s.getAnalyze());
      if (s.containsSevereError()) return;
      g = ((EfGridSource) s.getSource()).getGrid();
    }
    final RefluxRefondeSolutionNewReader r = new RefluxRefondeSolutionNewReader(RefluxRefondeSolutionFileFormat
        .getInstance());
    r.setFile(_f);
    CtuluIOOperationSynthese s = r.read();
    final RefluxRefondeSolutionSequentielResult sol = (RefluxRefondeSolutionSequentielResult) s.getSource();
    // pour le cas refonde
    if (sol.isRefonde()) {
      sol.setRefondeSeiche(isSeiche, firstVar);
    }
    // final boolean isSot = RefluxRefondeSolutionFileFormat.isSot(sovFile);
    // support temperature
    final H2dVariableType[] variable = sol.getVars();
    final int[] placement = sol.getPosition();
    final int nbVar = variable == null ? 0 : variable.length;
    final GISAttributeInterface[] attributes = new GISAttributeInterface[nbVar];
    if (variable != null) {
      for (int i = 0; i < nbVar; i++) {
        attributes[i] = _r.findOrCreateAttribute(variable[i], true);
      }
      _r.addUsedAttributes(attributes);
    }
    RefluxSolutionSequentielReader reader = new RefluxSolutionSequentielReader(sol, _f);
    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(new RefluxRefondeResultatGisAdapter(reader, g, attributes, placement, sol.getTimeStepNb() - 1));
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    // 
    _r.pointModel_.add(adapter);
    _r.nbPoint_ += g.getPtsNb();
    _r.nbPointTotal_ += g.getPtsNb();

  }
}
