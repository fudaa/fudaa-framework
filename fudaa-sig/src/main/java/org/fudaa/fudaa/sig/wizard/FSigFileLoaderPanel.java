/*
 *  @creation     8 juin 2005
 *  @modification $Date: 2007-01-19 13:14:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeNameComparator;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gui.*;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.FSigLoaderPreviewer;
import org.fudaa.fudaa.sig.FSigVarAttrMapperTableModel;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderPanel.java,v 1.1 2007-01-19 13:14:10 deniger Exp $
 */
public class FSigFileLoaderPanel extends FSigWizardDefaultPanel {

  /**
   * @author fred deniger
   * @version $Id: FSigFileLoaderPanel.java,v 1.1 2007-01-19 13:14:10 deniger Exp $
   */
  protected static final class FileListCellRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      File f = null;
      if (_value instanceof File) {
        f = (File) _value;
      } else if (_value instanceof String) {
        f = new File((String) _value);
      }
      if (f == null) {
        super.setValue(_value);
      } else {
        super.setValue(f.getName());
        super.setToolTipText(f.getAbsolutePath());
      }
    }
  }

  /**
   * @author fred deniger
   * @version $Id: FSigFileLoaderPanel.java,v 1.1 2007-01-19 13:14:10 deniger Exp $
   */
  protected static final class FileListCellEditor extends CtuluCellFileEditor {
    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final int _row, final int _column) {
      final File f = (File) _value;
      setValue(f.getAbsolutePath());
      return this;
    }

    @Override
    public Object getCellEditorValue() {
      final Object r = super.getCellEditorValue();
      if (r instanceof File) {
        return r;
      }
      return new File(r.toString());
    }
  }

  /**
   * @author fred deniger
   * @version $Id: FSigFileLoaderPanel.java,v 1.1 2007-01-19 13:14:10 deniger Exp $
   */
  protected final class FileListPanel extends CtuluListEditorPanel {
    /**
     * @param _m
     */
    protected FileListPanel(final CtuluListEditorModel _m) {
      super(_m, true, true, true, false, true);
    }

    @Override
    public void actionAdd() {
      ((FSigWizardFileModel) super.getTableModel()).addFiles(fileMng_.startEditingFile(null, CtuluLibSwing
          .getFrameAncestorHelper(impl_.getParentComponent())), -1);
    }

    @Override
    public void actionInserer() {
      final int n = table_.getSelectionModel().getMinSelectionIndex();
      ((FSigWizardFileModel) super.getTableModel()).addFiles(fileMng_.startEditingFile(null, CtuluLibSwing
          .getFrameAncestorHelper(impl_.getParentComponent())), n);
      table_.getSelectionModel().setSelectionInterval(n, n);
    }
  }

  private boolean isAttributeRequired_ = true;
  final DefaultListModel attributeList_;

  final FSigWizardFileMng fileMng_ = new FSigWizardFileMng();
  final GISPolygone[] grid_;
  final CtuluUI impl_;
  private boolean useNameToFindKnownAtt=true;

  CtuluListEditorPanel pnFiles_;

  /**
   * @param _attributeList la liste stockant les attribute
   * @param _impl l'impl
   * @param _grid l'enveloppe a utiliser pour la previsu
   */
  public FSigFileLoaderPanel(final DefaultListModel _attributeList, final CtuluUI _impl, final GISPolygone[] _grid) {
    attributeList_ = _attributeList;
    impl_ = _impl;
    grid_ = _grid;
    pnFiles_ = new FileListPanel(fileMng_.getModel());
    pnFiles_.setValueListCellEditor(new FileListCellEditor());
    pnFiles_.setValueListCellRenderer(new FileListCellRenderer());
    pnFiles_.getTableColumnModel().getColumn(2).setCellEditor(fileMng_.getFmtRowEditor());
    pnFiles_.getTableColumnModel().getColumn(2).setCellRenderer(fileMng_.getFmtRowRenderer());
    pnFiles_.getTableColumnModel().getColumn(3).setCellEditor(fileMng_.getFmtRowOrigineEditor());
    pnFiles_.getTableColumnModel().getColumn(3).setCellRenderer(fileMng_.getFmtRowOrigineRenderer());
    final BuButton btPreview = new BuButton(FSigLib.getS("Prévisualiser"));
    btPreview.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        buildPreview();
      }
    });
    final BuButton btLoad = new BuButton(FSigLib.getS("Charger"));
    btLoad.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {

        buildPreview(false);
      }
    });
    final BuList l = new BuList();
    l.setCellRenderer(FSigVarAttrMapperTableModel.getAttRenderer());
    l.setModel(attributeList_);
    final BuScrollPane sc = new BuScrollPane(l);
    sc.setPreferredWidth(95);
    sc.setPreferredHeight(120);
    JComponent c = (JComponent) BuLib.findNamedComponent(pnFiles_, "scroll");
    pnFiles_.remove(c);
    final BuBorderLayout lay = new BuBorderLayout(2, 2, true, true);
    final JPanel pn = new BuPanel(lay);
    pn.setBorder(BorderFactory.createTitledBorder(FSigLib.getS("Attributs disponibles")));
    pn.add(sc);
    final BuPanel center = new BuPanel(lay);
    center.add(c);
    center.add(pn, BuBorderLayout.SOUTH);
    pnFiles_.add(center, BuBorderLayout.CENTER);
    c = (JComponent) BuLib.findNamedComponent(pnFiles_, "pnBt");
    c.add(new BuSeparator());
    c.add(btLoad);
    fileMng_.addListenerToFile(new TableModelListener() {

      @Override
      public void tableChanged(final TableModelEvent _e) {
        attributeList_.removeAllElements();
        final boolean enable = fileMng_.getModel().getRowCount() > 0;
        btLoad.setEnabled(enable);
        btPreview.setEnabled(enable);
      }
    });
    c.add(btPreview);
    add(pnFiles_);
    super.valideData();
  }

  protected void buildPreview() {
    buildPreview(true);

  }

  public void buildPreview(final boolean _showPreview) {
    final FSigLoaderPreviewer prev = new FSigLoaderPreviewer() {

      @Override
      public FSigGeomSrcData loadData(final ProgressionInterface _prog) {
        final CtuluAnalyze analyse = new CtuluAnalyze();
        FSigGeomSrcData res = loadAll(_prog, analyse);
        valideData();
        impl_.manageAnalyzeAndIsFatal(analyse);
        return res;
      }
    };
    prev.preview(impl_, grid_, _showPreview);
  }

  public void setUseNameToFindKnownAtt(boolean useNameToFindKnownAtt) {
    this.useNameToFindKnownAtt = useNameToFindKnownAtt;
  }

  public boolean isUseNameToFindKnownAtt() {
    return useNameToFindKnownAtt;
  }

  @Override
  public String valideAndGetError() {
    String message = null;
    if (isAttributeRequired_ && attributeList_.getSize() == 0) {
      message = FSigLib.getS("Aucun attribut disponible");
    }
    if (fileMng_.isEmpty()) {
      final String emptyMes = FSigLib.getS("Aucune donnée géographique disponible");
      if (message == null) {
        message = emptyMes;
      } else {
        message = message + ", " + emptyMes;
      }
    }
    if (message == null) {
      message = CtuluLibString.ESPACE;
    } else {

      message = message + ":<br>&nbsp;&nbsp;" + FSigLib.getS("ajouter des fichiers et utiliser le bouton 'Charger'");
    }
    return message;
  }

  public boolean isOk() {
    return !fileMng_.isEmpty() && (!isAttributeRequired_ || attributeList_.getSize() > 0);
  }

  public final FSigWizardFileMng getFileMng() {
    return fileMng_;
  }

  /**
   * @return true si des attributs doivent etre presents
   */
  public final boolean isAttributeRequired() {
    return isAttributeRequired_;
  }



  public FSigGeomSrcData loadAll(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    final FSigFileLoadResult res = fileMng_.loadAll(_prog, _analyze);
    attributeList_.removeAllElements();
    if (res != null) {
      final Object[] att = res.attribute_.toArray();
      if (att != null && att.length > 0) {
        Arrays.sort(att, new GISAttributeNameComparator());
        final int nb = att.length;
        for (int i = 0; i < nb; i++) {
          attributeList_.addElement(att[i]);
        }
      }
    }
    super.valideData();
    return res == null ? null : res.createDataWithAtomic(useNameToFindKnownAtt);
  }

  /**
   * @param _isAttributeRequired true si des attributs doivent etre presents
   */
  public final void setAttributeRequired(final boolean _isAttributeRequired) {
    isAttributeRequired_ = _isAttributeRequired;
    super.valideData();
  }
}
