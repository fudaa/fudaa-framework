/*
 * @creation 24 juil. 06
 * @modification $Date: 2008-02-22 16:28:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuWizardTask;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JRootPane;
import org.fudaa.ctulu.RasterDataInterface;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZModeleImageRaster;
import org.fudaa.ebli.calque.ZModeleStatiqueImageRaster;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * Un wizard pour l'import d'une image. Le wizard gere plusieurs �tapes.
 * 
 * @author fred deniger
 * @version $Id: FSigImageWizardTask.java,v 1.2.6.1 2008-02-22 16:28:58 bmarchan Exp $
 */
public class FSigImageWizardTask extends BuWizardTask {

  private BArbreCalqueModel tree_;
  
  public FSigImageWizardTask(){
    
  }
  
  public FSigImageWizardTask(BArbreCalqueModel _tree){
    tree_=_tree;
  }
  
  public interface CalqueFactory {
    ZCalqueImageRaster createCalque(ZModeleImageRaster _model);
  }

  final FSigImageWizardStepImage img_ = new FSigImageWizardStepImage();
  FSigImageWizardStepCalage calage_;
  FSigWizardStepInterface currentStep_ = img_;
  ZEbliCalquesPanel cqDest_;
  CalqueFactory fact_;

  public void setCqDest(final ZEbliCalquesPanel _cqDest) {
    cqDest_ = _cqDest;
    if (calage_ != null) {
      calage_.setCqDest(cqDest_);
    }
  }

  @Override
  public JComponent getStepComponent() {
    final JComponent c = currentStep_.getComponent();
    if (currentStep_ == calage_) {
      BuLib.invokeLater(new Runnable() {

        @Override
        public void run() {
          calage_.restaurer();
        }

      });
    }
    return c;

  }

  public void setParentRootPane(final JDialog _d) {
    setParentRootPane(_d.getRootPane());
  }

  public void setParentRootPane(final JFrame _d) {
    setParentRootPane(_d.getRootPane());

  }

  public void setParentRootPane(final JRootPane _d) {
    img_.setParentRootPane(_d);
    if (calage_ != null) {
      calage_.setParentRootPane(_d);
    }
  }

  @Override
  public int getStepCount() {
    return 2;
  }

  @Override
  public void cancelTask() {
    if (calage_ != null) {
      calage_.end();
    }
    super.cancelTask();
  }

  BGroupeCalque dest_;
  ZCalqueImageRaster calqueToModify_;

  @Override
  public void doTask() {
    if (calage_ == null) {
      return;
    }
    if (!calage_.valideData()) {
      return;
    }
    calage_.end();

    super.doTask();
    if (dest_ == null && calqueToModify_ == null) {
      return;
    }
    final FSigImageWizardTableModel.Result res = calage_.getResult();
    ZModeleImageRaster model;
    if (calqueToModify_ == null) {
      model = new ZModeleStatiqueImageRaster(img_.getImg());
    } else {
      calqueToModify_.setTitle(img_.getCalqueTitle());
      model = calqueToModify_.getModelImage();
      if(model!=null)
        model.setImage(img_.getImg());
      else {
        calqueToModify_.setModele(new ZModeleStatiqueImageRaster(img_.getImg()));
        model = calqueToModify_.getModelImage();
      }
    }
    boolean old=model.getGeomData().isGeomModifiable();
    model.getGeomData().setGeomModifiable(true);
    model.setProj(res.ptImg_, res.ptReel_, res.transform_, res.error_);
    model.getGeomData().setGeomModifiable(old);
    if (calqueToModify_ != null) {
      calqueToModify_.imgChanged();
      if(tree_!=null)
        tree_.fireObservableChanged();
      return;
    }
    ZCalqueImageRaster calque = null;
    if (fact_ != null) {
      calque = fact_.createCalque(model);
    }
    if (calque == null) {
      calque = new ZCalqueImageRaster(model);
      calque.setDestructible(true);
    }
    calque.setName(BGroupeCalque.findUniqueChildName(dest_));
    calque.setTitle(img_.getCalqueTitle());
    dest_.enDernier(calque);
    calqueToModify_ = calque;
  }

  @Override
  public String getStepText() {
    return currentStep_.getStepText();
  }

  @Override
  public String getStepTitle() {
    return currentStep_.getStepTitle();
  }

  @Override
  public String getTaskTitle() {
    return FSigLib.getS("Importer et g�or�f�rencer une image");
  }

  @Override
  public void previousStep() {
    if (currentStep_ == calage_) {
      currentStep_ = img_;
    }
    super.previousStep();
  }

  @Override
  public void nextStep() {
    if (currentStep_ == img_) {
      if (img_.getImg() == null||!img_.getImg().isImageLoaded()) {
        img_.loadImage(this, false,false);
        return;
      }
      if (calage_ == null) {
        calage_ = new FSigImageWizardStepCalage(img_.getDialog());
        if (cqDest_ != null) {
          calage_.setCqDest(cqDest_);
        }
      }
      calage_.setImage(img_.getImg());
      if (img_.data_ != null) {
        calage_.setRasterData(img_.data_);
      }
      currentStep_ = calage_;
      calage_.valideData();
    }
    super.nextStep();
  }

  public BGroupeCalque getDest() {
    return dest_;
  }

  public void setDest(final BGroupeCalque _dest) {
    dest_ = _dest;
  }

  public ZCalqueImageRaster getCalqueToModiy() {
    return calqueToModify_;
  }

  public void setModelToModiy(final ZCalqueImageRaster _modelToModiy) {
    if (_modelToModiy!=null) {
      calqueToModify_=_modelToModiy;
      if (_modelToModiy.getModelImage()!=null) {
        final RasterDataInterface data=_modelToModiy.getModelImage().getData();
        img_.setData(data, _modelToModiy.getTitle());
        if(_modelToModiy.getModelImage().isImageLoaded())
          nextStep();
      }
    }
  }

  public CalqueFactory getFact() {
    return fact_;
  }

  public void setFactory(final CalqueFactory _fact) {
    fact_ = _fact;
  }

}
