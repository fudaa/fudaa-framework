/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2007-01-19 13:14:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISGeometry;
import org.fudaa.ctulu.gis.GISVisitorDefault;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.fudaa.sig.FSigLib;
import org.locationtech.jts.geom.*;

import java.io.File;
import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderGIS.java,v 1.1 2007-01-19 13:14:09 deniger Exp $
 */
public class FSigFileLoaderGIS implements FSigFileLoaderI {
  /**
   * Un loader pour un fichier Shape.
   */
  public static class Shape extends FSigFileLoaderGIS {
    public Shape() {
      super(GISFileFormat.SHP);
    }
  }

  /**
   * Un loader pour un fichier GML.
   */
  public static class GML extends FSigFileLoaderGIS {
    public GML() {
      super(GISFileFormat.GML_V2);
    }
  }

  final BuFileFilter ft_;
  final GISFileFormat gisFileFormat;
  transient AbstractFSigDataModelSIGAdapter src_;
  transient int nbPoint_;
  transient int nbPointTotal_;
  transient int nbPolygone_;
  transient int nbPolyligne_;

  public FSigFileLoaderGIS(final BuFileFilter _ft, final GISFileFormat gisFileFormat) {
    ft_ = _ft;
    this.gisFileFormat = gisFileFormat;
  }

  /**
   * Cr�ation a partir de la seule fabrique
   *
   * @param fileFormat La fabrique.
   */
  public FSigFileLoaderGIS(final GISFileFormat fileFormat) {
    gisFileFormat = fileFormat;
    ft_ = GISExportDataStoreFactory.buildFileFilterFor(fileFormat);
  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderGIS(ft_, gisFileFormat);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  private class GiSCount extends GISVisitorDefault {
    int idx_;
    final CtuluAnalyze analyze_;
    boolean isPolygoneWarn_;

    public GiSCount(final CtuluAnalyze _analyze) {
      super();
      analyze_ = _analyze;
    }

    @Override
    public boolean visitPoint(final Point _p) {
      nbPoint_++;
      return true;
    }

    @Override
    public boolean visitPolygone(final LinearRing _p) {
      nbPolygone_++;
      nbPointTotal_ += _p.getNumPoints();
      return true;
    }

    @Override
    public boolean visitPolygoneWithHole(final Polygon _p) {
      nbPolygone_++;
      nbPointTotal_ += _p.getExteriorRing().getNumPoints();
      src_.replacePolygonWithLine(idx_);
      if (!isPolygoneWarn_ && analyze_ != null && _p.getNumInteriorRing() > 0) {
        analyze_.addWarn(FSigLib.getS("Des lignes int�rieures � des polygones ont �t� ignor�es"), idx_);
        isPolygoneWarn_ = true;
      }
      return true;
    }

    @Override
    public boolean visitPolyligne(final LineString _p) {
      nbPolyligne_++;
      nbPointTotal_ += _p.getNumPoints();
      return true;
    }
  }

  @Override
  public void setInResult(final FSigFileLoadResult fSigFileLoadResult, final File file, String fileOrigine, final ProgressionInterface prog,
                          final CtuluAnalyze log) {
    if (src_ == null) {
      try {
        src_ = AbstractFSigDataModelSIGAdapter.create(gisFileFormat, file, prog);
        final GiSCount chooser = new GiSCount(log);
        if (src_.getNumGeometries() > 0) {
          for (int i = src_.getNumGeometries() - 1; i >= 0; i--) {
            final Geometry geom = src_.getGeometry(i);
            chooser.idx_ = i;
            ((GISGeometry) geom).accept(chooser);
          }
        }
      } catch (final IOException _e) {
        if (log != null) {
          log.addFatalError(_e.getMessage());
        }
        FuLog.warning(_e);
      }
    }
    if (src_ != null && src_.getNumGeometries() > 0) {
      src_.atts_ = null;
      src_.buildAttributes(fSigFileLoadResult);
      // Ajout de l'attribut ETAT_GEOM
      fSigFileLoadResult.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
      GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(src_);
      adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, fileOrigine);
      // 
      fSigFileLoadResult.addUsedAttributes(src_.atts_);
      if (nbPoint_ > 0) {
        fSigFileLoadResult.pointModel_.add(adapter);
      }
      if (nbPolygone_ > 0) {
        fSigFileLoadResult.polygoneModel_.add(adapter);
      }
      if (nbPolyligne_ > 0) {
        fSigFileLoadResult.ligneModel_.add(adapter);
      }
      fSigFileLoadResult.nbPoint_ += nbPoint_;
      fSigFileLoadResult.nbPointTotal_ += nbPointTotal_;
      fSigFileLoadResult.nbPolygones_ += nbPolygone_;
      fSigFileLoadResult.nbPolylignes_ += nbPolyligne_;
    }
  }
}
