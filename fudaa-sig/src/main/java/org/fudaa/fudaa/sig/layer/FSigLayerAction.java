/*
 * @creation 30 mars 2006
 * @modification $Date: 2008-01-17 11:38:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.layer;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeNameComparator;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.sig.FSigAttributeEditorPanel;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * Une classe d�finissant des actions portant sur les calques ou groupes de calques SIG.
 * 
 * @author fred deniger
 * @version $Id: FSigLayerAction.java,v 1.3.4.1 2008-01-17 11:38:58 bmarchan Exp $
 */
public final class FSigLayerAction {
  private FSigLayerAction() {}

  /**
   * @author Fred Deniger
   * @version $Id: FSigLayerAction.java,v 1.3.4.1 2008-01-17 11:38:58 bmarchan Exp $
   */
  public static class AddGroupAction extends EbliActionSimple {

    final BArbreCalqueModel model_;

    public AddGroupAction(final BArbreCalqueModel _model) {
      super(FSigLib.getS("Ajouter un groupe"), BuResource.BU.getToolIcon("ajouter"), "GIS_ADD_GROUP");
      model_ = _model;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final BCalque c = model_.getSelectedCalque();
      if (c instanceof ZCalqueEditionGroup) {
        addGroupLayer(((FSigLayerGroup) c));
      }
    }

  }

  /**
   * @author Fred Deniger
   * @version $Id: FSigLayerAction.java,v 1.3.4.1 2008-01-17 11:38:58 bmarchan Exp $
   */
  public static class AddPointAction extends EbliActionSimple {

    final BArbreCalqueModel model_;

    public AddPointAction(final BArbreCalqueModel _model) {
      super(FSigLib.getS("Ajouter un calque de points"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "GIS_ADD_POINT");
      model_ = _model;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final BCalque c = model_.getSelectedCalque();
      if (c instanceof ZCalqueEditionGroup) {
        addPointLayer(((FSigLayerGroup) c));
      }
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: FSigLayerAction.java,v 1.3.4.1 2008-01-17 11:38:58 bmarchan Exp $
   */
  public static class AddPolyAction extends EbliActionSimple {

    final BArbreCalqueModel model_;

    public AddPolyAction(final BArbreCalqueModel _model) {
      super(FSigLib.getS("Ajouter un calque polylignes"), EbliResource.EBLI.getToolIcon("draw-polyline"),
          "GIS_ADD_POLY");
      model_ = _model;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final BCalque c = model_.getSelectedCalque();
      if (c instanceof ZCalqueEditionGroup) {
        addPolyligneLayer((FSigLayerGroup) c);
      }
    }

  }

  /**
   * @author Fred Deniger
   * @version $Id: FSigLayerAction.java,v 1.3.4.1 2008-01-17 11:38:58 bmarchan Exp $
   */
  public static class ModifyAttributeAction extends EbliActionSimple {

    final BArbreCalqueModel model_;

    public ModifyAttributeAction(final BArbreCalqueModel _model) {
      super(FSigLib.getS("Modifier les attributs"), null, "MODIFY_ATTRIBUTES");
      model_ = _model;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final BCalque c = model_.getSelectedCalque();
      if (c instanceof ZCalqueEditionGroup) {
        FSigLayerGroup.editFor((FSigLayerGroup) c);
      }
    }

  }

  protected static void addPointLayer(final FSigLayerGroup _gr) {
    final String name = _gr.askForName(FSigLib.getS("Nouveau calque de points"), false);
    if (name != null) {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          _gr.addPointLayerAct(name, _gr.createPointZone());
        }
      });

    }
  }

  protected static void addPolyligneLayer(final FSigLayerGroup _gr) {
    final String name = _gr.askForName(FSigLib.getS("Nouveau calque de polylignes"), true);
    if (name != null) {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          _gr.addLigneBriseeLayerAct(name, _gr.createLigneBriseeZone());
        }
      });

    }
  }

  protected static void addGroupLayer(final FSigLayerGroup _gr) {
    final BuTextField ft = new BuTextField("gr " + (++_gr.grIdx_));
    ft.setColumns(12);
    final FSigAttributeEditorPanel pn = new FSigAttributeEditorPanel(null, _gr.visu_.getAttributeMng(), null, 
        FSigLib.getS("Variables � utiliser")) {

      @Override
      public boolean isDataValid() {
        final String txt = ft.getText();
        if (txt.length() == 0) {
          setErrorText(CtuluLib.getS("Le champ est vide") + '!');
          return false;
        }
        return true;
      }
    };
    final BuPanel pnTop = new BuPanel(new BuGridLayout(2, 3, 3));
    pnTop.add(new BuLabel(FSigLib.getS("Nom du nouveau groupe:")));
    pnTop.add(ft);
    pnTop.setBorder(BuBorders.EMPTY4444);
    pn.add(pnTop, BuBorderLayout.NORTH);

    if (!CtuluDialogPanel.isOkResponse(pn.afficheModale(_gr.visu_.getFrame(), FSigLib.getS("Ajouter un groupe")))) {
      return;
    }
    final String name = ft.getText();
    final Set removed = new HashSet();
    final Set added = new HashSet();
    pn.fillWithAddedRemovedAttribute(added, removed);
    for (final Iterator it = removed.iterator(); it.hasNext();) {
      final GISAttributeInterface att = (GISAttributeInterface) it.next();
      _gr.att_.setUnUsed(att);
    }
    for (final Iterator it = added.iterator(); it.hasNext();) {
      _gr.att_.setUsed((GISAttributeInterface) it.next());

    }
    if (name != null) {
      final FSigLayerGroup gr = _gr.addGroupAct(name, _gr, true);
      gr.attributes_ = new GISAttributeInterface[added.size()];
      added.toArray(gr.attributes_);
      Arrays.sort(gr.attributes_, new GISAttributeNameComparator());
    }
  }

}
