
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import org.locationtech.jts.geom.Coordinate;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.mesure.DodicoCsvReader;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * Manage format Z ESRI
 * @author Adrien Hadoux
 */
public class FSigFileLoaderZESRI implements FSigFileLoaderI {
  
  final BuFileFilter ft_;
  private GISAttributeDouble firstColonne;
  
  public final Map getOptions() {
    return options_;
  }
  
  public final void setOptions(final Map _options) {
    options_ = _options;
  }
  
  void setFirstColonneAttribute(GISAttributeDouble firstCol) {
    this.firstColonne = firstCol;
  }

  /**
   * File format Z ESRI
   * @author Adrien Hadoux
   */
  static final class ZESRIFileFilter extends BuFileFilter {

    /**
     * @param _filters
     * @param _description
     */
    ZESRIFileFilter(String[] _filters, String _description) {
      super(_filters, _description);
    }
    
    @Override
    public boolean accept(final File _f) {
      return _f != null;
    }
    
    @Override
    public boolean accept(final File _d, final String _fn) {
      return !new File(_fn).isDirectory();
    }
  }

  /**
   * Reader format Z ESRI
   * @author Adrien Hadoux   * 
   */
  public static class ZESRIFileReader extends DodicoCsvReader {
    
    public ZESRIFileReader() {
      setNumeric(false);
      setBlankValid(true);
      // setSepChar(",");
      super.minValueByLine_ = 2;
    }
    
    @Override
    public String[] getName() {
  
      String[] names = reader_.getName();
      if (names == null) {
        names = new String[3];
       
        names[0] = "X";
        names[1] = "Y";
        names[2] = "Z";
        
        reader_.setName(names);
      }
      return reader_.getName();
    }
    
    public String containsError() {
      final String[][] r = getPreview();
      if (r == null || r.length == 0 || !r[1][0].contains(",")) {
        return FSigLib.getS("Le fichier doit contenir 3 champs (X,Y,Z) au minimum encadr� par la valeur du profil et un marqueur END");
      }
     
      
      return null;
    }
    
    @Override
    protected String[][] internalRead() {
      analyze_.clear();
      final String[][] r = (String[][]) super.internalRead();
     
      
      String error = containsError();
      if (error != null) {
        analyze_.addFatalError(error);
        return null;
      }
      return r;
    }
  }
  
  public FSigFileLoaderZESRI() {
    ft_ = new ZESRIFileFilter(new String[]{"txt", "dat", "_polyligne.gen"}, FudaaLib.getS("Fichier Z ESRI"));
  }
  Map options_;
  
  private FSigFileLoaderZESRI(final BuFileFilter _ft) {
    ft_ = _ft;
  }
  
  @Override
  public FSigFileLoaderI createNew() {
    final FSigFileLoaderZESRI fSigFileLoader = new FSigFileLoaderZESRI(ft_);
    fSigFileLoader.setFirstColonneAttribute(firstColonne);
    return fSigFileLoader;
  }
  
  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }
  
  
  
  public  List<List<GISPoint>> mergeGridByProfils(final String[][] data) {
      String separator=",";
      List<List<GISPoint>> profilList = new ArrayList<List<GISPoint>>();
      List<GISPoint> currentProfil=new ArrayList<GISPoint>();
       for(int i=0;i<data[0].length;i++) {
          String[] line = data[0][i].replace("SEP","").replace(" ","").split(",");
          if(line !=null && line.length <=1) {         
              if("END".equals(line[0])) {
                  profilList.add(currentProfil);
                  currentProfil = new ArrayList<GISPoint>();
              }
          }else {
                try{
                    double x = Double.parseDouble(line[0]);
                    double y = Double.parseDouble(line[1]);
                    double z = Double.parseDouble(line[2]);

                    GISPoint pt = new GISPoint(x, y, z);
                    currentProfil.add(pt);
                } catch (NumberFormatException _exc) {
                    
                }
          }
      }
      
          
       return profilList;   
  }
  
  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
          final CtuluAnalyze _analyze) {

    // Lecture du fichier
    final ZESRIFileReader reader = new ZESRIFileReader();
    final CtuluDoubleParser parser = new CtuluDoubleParser();
    parser.setEmptyIsNull(true);
    reader.initFromOption(options_);
    reader.setAddCommentInResult(true);
    final CtuluIOOperationSynthese op = reader.read(_f, _prog);
    if (op.containsMessages() && op.getAnalyze().containsErrors()) {
      _analyze.merge(op.getAnalyze());
      return;
    }

    // Suppression des lignes invalides (X ou Y non Double) et conversion des
    // String en Double pour les colonnes de ce type.
    String[][] data = (String[][]) op.getSource();
    if (data==null || data.length == 0) {
      return;
    }
    
    List<GISZoneCollectionLigneBrisee> zones = new ArrayList<GISZoneCollectionLigneBrisee>();
    
     List<List<GISPoint>> listeProfils = mergeGridByProfils(data);
      for (List<GISPoint> vals : listeProfils) {
        
          // Cr�ation des points
        //  final GISPoint[] pt = new GISPoint[vals.size()];
          final Coordinate[] coordinates = new  Coordinate[vals.size()];
          List<Object[]> attVals = new ArrayList<Object[]>();
          Double[] zValues = new Double[vals.size()];
          
          for (int i = 0; i < coordinates.length; i++) {
              zValues[i]=vals.get(i).getZ();
             //  pt[i] = new GISPoint(vals.get(i).getX(),vals.get(i).getY(),0);
              coordinates[i] = new Coordinate(vals.get(i).getX(),vals.get(i).getY(),vals.get(i).getZ());
          }
          attVals.add(zValues);
        GISAttributeInterface att = _r.findOrCreateAttribute("Z", Double.class, true);
          
          GISAttributeInterface[] atts = new GISAttributeInterface[1];
          atts[0] = att;
          // Cr�ation des attributs
          GISZoneCollectionLigneBrisee zone = new GISZoneCollectionLigneBrisee();
          //zone.addAll(pt, attVals, null);
          final GISCoordinateSequence seq = new GISCoordinateSequence(coordinates);
           zone.setAttributes(atts, null);
          zone.addPolyligne(seq, attVals.toArray(), null);
         
          //-- finally add the zone tto the zone collection --//
          zones.add(zone);
      }
      
       
       

      final String[] name = reader.getName();
     GISAttributeInterface att = _r.findOrCreateAttribute("Z", Double.class, true);
     GISAttributeInterface[] atts = new GISAttributeInterface[1];
          atts[0] = att;
      for (GISZoneCollectionLigneBrisee zl : zones) {
           //zl.setAttributes(atts, null);
          _r.nbPointTotal_ += zl.getNumPoints();
          _r.nbPolylignes_ += 1;
          // Ajout de l'attribut ETAT_GEOM
          _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
          GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zl);
          adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, "ORIG");
         // _r.pointModel_.add(adapter);
          _r.ligneModel_.add(adapter);
      }
      
      _r.nbPoint_ = _r.nbPointTotal_;
   
  }
}
