/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GisDataModelTranslated;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.wfs.GML;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import org.geotools.wfs.GML.Version;

/**
 * @author Fred Deniger
 * @version $Id: FSigDataModelStoreAdapter.java,v 1.3 2007-04-30 14:22:38 deniger Exp $
 */
public class FSigDataModelGMLAdapter extends AbstractFSigDataModelSIGAdapter {
  final SimpleFeatureCollection featureCollection;

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GisDataModelTranslated(this, xyToAdd);
  }

  public static FSigDataModelGMLAdapter create(final File _f, GML.Version version,
                                               final ProgressionInterface _progress) throws IOException {
    SimpleFeatureCollection toUse = new DefaultFeatureCollection();
    try (FileInputStream in = new FileInputStream(_f)) {
      final FSigGML fSigGML = new FSigGML(version);
      fSigGML.setLegacy(true);
      toUse = new FSigGML(version).decodeFeatureCollection(in);
    } catch (SAXException e) {
      FuLog.error(e);
    } catch (ParserConfigurationException e) {
      FuLog.error(e);
    }
    return new FSigDataModelGMLAdapter(toUse, loadGeometries(toUse, _progress));
  }

  public FSigDataModelGMLAdapter(SimpleFeatureCollection featureCollection, List<Geometry> geometries) throws IOException {
    super(geometries, featureCollection.getSchema());
    this.featureCollection = featureCollection;
  }

  @Override
  protected FeatureIterator<SimpleFeature> createIterator() {
    return featureCollection.features();
  }

  private Object getCache(final int _idxAtt, final int _geom) {
    return cacheAtt_[_idxAtt][_geom];
  }
}
