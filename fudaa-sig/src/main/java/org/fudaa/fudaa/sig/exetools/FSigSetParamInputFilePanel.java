/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sig.exetools;

import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * Un panneau pour selectionner les calques qui servent � cr�er les fichiers
 * d'entr�e.
 * @author marchand@deltacad.fr
 */
public class FSigSetParamInputFilePanel extends CtuluDialogPanel {
  private CtuluUI ui_;
  private FudaaExeTool.ParamI param_;
  private BArbreCalqueModel mdLayers;
  private JTree trLayers;
  private BGroupeCalque cqRoot_;

  /**
   * Modele de selection de l'arbre, gerant la non selection des groupes de calques.
   * @author Bertrand Marchand
   */
  private static class OnlyGeomLayerTreeSelectionModel extends DefaultTreeSelectionModel {

    @Override
    public void setSelectionPaths(final TreePath[] _paths) {
      super.setSelectionPaths(getCorrectPaths(_paths));
    }

    @Override
    public void addSelectionPaths(final TreePath[] _paths) {
      super.addSelectionPaths(getCorrectPaths(_paths));
    }

    private TreePath[] getCorrectPaths(final TreePath[] _paths) {
      TreePath[] paths = _paths;
      if (!CtuluLibArray.isEmpty(_paths)) {
        final List<TreePath> correctPath = new ArrayList<TreePath>(paths.length);
        for (int i = 0; i < paths.length; i++) {
          if (paths[i] != null && paths[i].getLastPathComponent() != null
              && (paths[i].getLastPathComponent()) instanceof ZCalqueGeometry) {
            correctPath.add(paths[i]);
          }
        }
        if (correctPath.size() != paths.length) {
          paths=correctPath.toArray(new TreePath[correctPath.size()]);
        }
      }
      return paths;
    }

  }

  public FSigSetParamInputFilePanel(CtuluUI _ui, BGroupeCalque _cqRoot) {
    ui_=_ui;
    cqRoot_=_cqRoot;
    setLayout(new BorderLayout());
    mdLayers=new BArbreCalqueModel(_cqRoot);
    trLayers=new JTree();
    trLayers.setModel(mdLayers);
    trLayers.setExpandsSelectedPaths(true);
    trLayers.setSelectionModel(new OnlyGeomLayerTreeSelectionModel());
    JScrollPane spLayers=new JScrollPane();
    spLayers.setPreferredSize(new Dimension(250, 300));
    spLayers.setViewportView(trLayers);
    add(spLayers, BorderLayout.CENTER);
  }

  /**
   * Selectionne les calques depuis le param�tre.
   * @param _obj Le parametre.
   */
  @Override
  public void setValue(Object _obj) {
    param_=(FudaaExeTool.ParamI) _obj;
    String[] names=param_.getValSet() == null ? new String[0] : param_.getValSet().split("\\|");
    TreePath[] pths=new TreePath[names.length];
    for (int i=0; i < names.length; i++) {
      BCalque cq=cqRoot_.getCalqueParNom(names[i]);
      List<BCalque> lcqs=new ArrayList<BCalque>();
      lcqs.add(cq);
      while ((cq=(BCalque) cq.getParent()) != cqRoot_) {
        lcqs.add(0, cq);
      }
      lcqs.add(0, cqRoot_);
      pths[i]=new TreePath(lcqs.toArray());
    }
    trLayers.setSelectionPaths(pths);
  }

  @Override
  public Object getValue() {
    TreePath[] cqs=trLayers.getSelectionPaths();
    StringBuffer sb=new StringBuffer();
    for (TreePath cq : cqs) {
      if (sb.length() != 0) {
        sb.append("|");
      }
      sb.append(((BCalque) cq.getLastPathComponent()).getName());
    }
    param_.setValSet(sb.toString());
    FuLog.debug(param_.getValSet());
    return param_;
  }

  @Override
  public boolean isDataValid() {
    if (trLayers.getSelectionCount() == 0) {
      setErrorText(FSigLib.getS("Aucun calque n'a �t� s�lectionn�"));
      return false;
    }
    return true;
  }
  
}
