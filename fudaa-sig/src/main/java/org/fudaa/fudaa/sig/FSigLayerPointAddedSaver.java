/*
 *  @creation     26 ao�t 2005
 *  @modification $Date: 2006-09-01 14:56:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.BCalqueSaverSingle;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.sig.persistence.FSigLayerPointPersistence;

/**
 * Laisse pour les anciennes sauvegardes.
 * 
 * @deprecated tout est dans FSigLayerLinePersistence
 * @author Fred Deniger
 * @version $Id: FSigLayerPointAddedSaver.java,v 1.4 2006-09-01 14:56:42 deniger Exp $
 */
public class FSigLayerPointAddedSaver extends BCalqueSaverSingle {

  GISZoneCollectionPoint zone_;

  /**
   * Laisse pour les anciennes sauvegardes.
   * 
   * @deprecated tout est dans FSigLayerLinePersistence
   */

  public FSigLayerPointAddedSaver() {
    setPersistenceClass(FSigLayerPointPersistence.class.getName());
  }

  @Override
  public EbliUIProperties getUI() {

    final EbliUIProperties res = super.getUI();
    if (zone_ != null) {
      res.put(FSigLayerPointPersistence.getGeomId(), zone_);
      zone_ = null;
    }
    return res;
  }

}
