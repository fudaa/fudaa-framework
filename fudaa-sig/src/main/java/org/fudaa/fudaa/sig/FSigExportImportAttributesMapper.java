/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2007-05-04 14:00:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Import de donn�es spatiales pour double, String le reste est ignor�...
 * 
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigExportImportAttributesMapper {

  // la source
  // Les attributs de destination
  final GISAttributeInterface[] src_;
  final AttributesTableModel model_;
  JTable t_;
  /**
   * Donne pour chaque classe, les attributs de destination qui serait valides.
   */
  Map<Class, GISAttributeInterface[]> srcClassDestValid_;

  class AttributesTableModel extends AbstractTableModel {

    private GISAttributeInterface[] selectedAttributes_;

    public AttributesTableModel() {
      selectedAttributes_=new GISAttributeInterface[src_==null ? 0:src_.length];
      // G�n�ration du choix par defaut pour chaque attributs source
      for (int i=0; i<selectedAttributes_.length; i++) {
        // Attribut source
        GISAttributeInterface attributSource=src_[i];
        if (src_!=null) {
          // Attributs correspondant pour cet attribut source
          GISAttributeInterface[] attributeCorrespondant=srcClassDestValid_.get(attributSource.getDataClass());
          if (attributeCorrespondant!=null) {
            // Suppression des attributs d�j� mapp�s
            ArrayList<GISAttributeInterface> attributesNonMappes=new ArrayList<GISAttributeInterface>();
            for (int j=0; j<attributeCorrespondant.length; j++)
              if (CtuluLibArray.findObjectEgalEgal(selectedAttributes_, attributeCorrespondant[j])==-1)
                attributesNonMappes.add(attributeCorrespondant[j]);
            // Selection de l'attribut dont le nom correspond � celui de
            // l'attribut source
            boolean found=false;
            int j=-1;
            while (!found&&++j<attributesNonMappes.size())
              found=attributesNonMappes.get(j).getName()==attributSource.getName();
            if (found)
              selectedAttributes_[i]=attributesNonMappes.get(j);
          }
        }
      }
    }
    
    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return FSigLib.getS("Variable source (lue)");
      }
      return FSigLib.getS("Variable destination");
    }

    @Override
    public int getRowCount() {
      return src_.length;
    }

    Map<GISAttributeInterface, GISAttributeInterface> getReadDestMap() {
      final Map<GISAttributeInterface, GISAttributeInterface> r = new HashMap<GISAttributeInterface, GISAttributeInterface>();
      for (int i = src_.length - 1; i >= 0; i--) {
        r.put(src_[i], selectedAttributes_[i]);
      }
      return r;
    }

    Map<GISAttributeInterface, GISAttributeInterface> getDestReadMap() {
      final Map<GISAttributeInterface, GISAttributeInterface> r = new HashMap<GISAttributeInterface, GISAttributeInterface>();
      for (int i = src_.length - 1; i >= 0; i--) {
        if (selectedAttributes_[i] != null) {
          r.put(selectedAttributes_[i], src_[i]);
        }
      }
      return r;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return src_[_rowIndex];
      }
      return selectedAttributes_[_rowIndex];
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 1 && CtuluLibArray.findObjectEgalEgal(selectedAttributes_, _value) < 0) {
        selectedAttributes_[_rowIndex] = (GISAttributeInterface) _value;
        fireTableCellUpdated(_rowIndex, _columnIndex);
      }
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return (_columnIndex == 1 && srcClassDestValid_.get(src_[_rowIndex].getDataClass()) != null);
    }
  }

  /**
   * @param _src les attributs sources : lus dans un fichier
   * @param _dest les attributs de destination
   * @param _filterAtomic true si on doit prendre en compte l'aspect atomique
   */
  public FSigExportImportAttributesMapper(final GISAttributeInterface[] _src, final GISAttributeInterface[] _dest,
      final boolean _filterAtomic) {
    super();
    src_ = _src == null ? new GISAttributeInterface[0] : _src;
    final int nb = src_.length;
    srcClassDestValid_ = new HashMap<Class, GISAttributeInterface[]>(nb);
    if (_src != null) {
      for (int i = nb - 1; i >= 0; i--) {
        if (!srcClassDestValid_.containsKey(_src[i].getDataClass())) {
          srcClassDestValid_.put(_src[i].getDataClass(), filtreAttributes(_dest, _src[i].getDataClass(),
              _filterAtomic ? Boolean.valueOf(_src[i].isAtomicValue()) : null));
        }
      }
    }
    model_ = new AttributesTableModel();
    t_ = new JTable(model_);
    final CtuluCellTextRenderer renderer = new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value == null) {
          super.setText(ignoreObject_);
        } else if (_value instanceof String) {
          setText((String) _value);
        } else if (_value == GISAttributeConstants.BATHY) {
          setText(GISAttributeConstants.BATHY.getLongName() + " :" + H2dVariableType.BATHYMETRIE);
        } else {
          setText(((GISAttributeInterface) _value).getName());
        }
      }
    };
    t_.getColumnModel().getColumn(0).setCellRenderer(renderer);
    t_.getColumnModel().getColumn(1).setCellRenderer(renderer);
    t_.getColumnModel().getColumn(1).setCellEditor(new ComboCellEditor(renderer));
  }

  final String ignoreObject_ = FSigLib.getS("< Ignorer >");

  /**
   * @return la table utilisee pour editer les correspondance source->destination
   */
  public final JTable getTable() {
    return t_;
  }

  /**
   * @return la table de correpondance attribut source (lu) -> attribut de destination
   */
  public Map<GISAttributeInterface, GISAttributeInterface> getReadDestMap() {
    return model_.getReadDestMap();
  }

  /**
   * @return la table de correpondance attribut de destination -> attribut source (lu)
   */
  public Map<GISAttributeInterface, GISAttributeInterface> getDestReadMap() {
    return model_.getDestReadMap();
  }

  private class ComboCellEditor extends DefaultCellEditor {

    DefaultComboBoxModel cbModel_;

    protected ComboCellEditor(final ListCellRenderer _renderer) {
      super(new JComboBox(new DefaultComboBoxModel()));
      cbModel_ = (DefaultComboBoxModel) ((JComboBox) super.editorComponent).getModel();
      ((JComboBox) super.editorComponent).setRenderer(_renderer);
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final int _row, final int _column) {
      final GISAttributeInterface[] attributes = (GISAttributeInterface[]) srcClassDestValid_.get(src_[_row]
          .getDataClass());
      cbModel_.removeAllElements();
      cbModel_.addElement(ignoreObject_);
      if (_value != null && !_value.equals(ignoreObject_)) {
        cbModel_.addElement(_value);
      }
      if (attributes != null) {
        for (int i = 0; i < attributes.length; i++) {
          if (CtuluLibArray.findObjectEgalEgal(FSigExportImportAttributesMapper.this.model_.selectedAttributes_,
              attributes[i]) < 0) {
            cbModel_.addElement(attributes[i]);
          }
        }
      }
      return super.getTableCellEditorComponent(_table, _value == null ? ignoreObject_ : _value, _isSelected, _row,
          _column);
    }

    @Override
    public Object getCellEditorValue() {
      // on renvoie null si c'est l'objet a ignorer
      if (cbModel_.getSelectedItem() == ignoreObject_) {
        return null;
      }
      return super.getCellEditorValue();
    }

  }

  /**
   * @param _src les attributs a filtrer
   * @param _c les classes demandees
   * @param _isAtomic si non null, un filtre est fait sur le caractere atomique
   * @return les attributs du style demande. Renvoie null si aucun.
   */
  public static GISAttributeInterface[] filtreAttributes(final GISAttributeInterface[] _src, final Class[] _c,
      final Boolean _isAtomic) {
    if (_src == null || _src.length == 0 || _c == null || _c.length == 0) {
      return null;
    }
    final List<GISAttributeInterface> resList = new ArrayList<GISAttributeInterface>();

    for (int i = 0; i < _src.length; i++) {
      if ((_isAtomic == null || _src[i].isAtomicValue() == _isAtomic.booleanValue())
          && CtuluLibArray.findObject(_c, _src[i].getDataClass()) >= 0) {
        resList.add(_src[i]);
      }
    }

    final GISAttributeInterface[] res = new GISAttributeInterface[resList.size()];
    resList.toArray(res);
    return res;
  }

  /**
   * @param _src les attributs a filtrer
   * @param _c les classes demandees
   * @param _isAtomic si non null, un filtre est fait sur le caractere atomique
   * @return les attributs du style demande. Renvoie null si aucun.
   */
  public static GISAttributeInterface[] filtreAttributes(final GISDataModel _src, final Class[] _c,
      final Boolean _isAtomic) {
    if (_src == null || _src.getNbAttributes() == 0 || _c == null || _c.length == 0) {
      return null;
    }
    final List<GISAttributeInterface> resList = new ArrayList<GISAttributeInterface>();

    for (int i = 0; i < _src.getNbAttributes(); i++) {
      if ((_isAtomic == null || _src.getAttribute(i).isAtomicValue() == _isAtomic.booleanValue())
          && CtuluLibArray.findObject(_c, _src.getAttribute(i).getDataClass()) >= 0) {
        resList.add(_src.getAttribute(i));
      }
    }

    final GISAttributeInterface[] res = new GISAttributeInterface[resList.size()];
    resList.toArray(res);
    return res;
  }

  /**
   * @param _src la source des attribut
   * @param _c les classes demandees
   * @param _isAtomic si non null, un filtre est fait sur le caractere atomique
   * @return les attributs du style demande. Renvoie null si aucun.
   */
  public static GISAttributeInterface[] filtreAttributes(final GISDataModel _src, final Class _c,
      final Boolean _isAtomic) {
    if (_src == null || _src.getNbAttributes() == 0) {
      return null;
    }
    final List<GISAttributeInterface> resList = new ArrayList<GISAttributeInterface>();

    for (int i = 0; i < _src.getNbAttributes(); i++) {
      if ((_isAtomic == null || _src.getAttribute(i).isAtomicValue() == _isAtomic.booleanValue())
          && _src.getAttribute(i).getDataClass().equals(_c)) {
        resList.add(_src.getAttribute(i));
      }
    }
    if (resList.size() == 0) {
      return null;
    }
    final GISAttributeInterface[] res = new GISAttributeInterface[resList.size()];
    resList.toArray(res);
    return res;
  }

  /**
   * @param _src la source des attribut
   * @param _c les classes demandees
   * @param _isAtomic si non null, un filtre est fait sur le caractere atomique
   * @return les attributs du style demande. Renvoie null si aucun.
   */
  public static GISAttributeInterface[] filtreAttributes(final GISAttributeInterface[] _src, final Class _c,
      final Boolean _isAtomic) {
    if (_src == null || _src.length == 0) {
      return null;
    }
    final List<GISAttributeInterface> resList = new ArrayList<GISAttributeInterface>();
    for (int i = 0; i < _src.length; i++) {
      if ((_isAtomic == null || _src[i].isAtomicValue() == _isAtomic.booleanValue())
          && _src[i].getDataClass().equals(_c)) {
        resList.add(_src[i]);
      }
    }
    if (resList.size() == 0) {
      return null;
    }
    final GISAttributeInterface[] res = new GISAttributeInterface[resList.size()];
    resList.toArray(res);
    return res;
  }
}
