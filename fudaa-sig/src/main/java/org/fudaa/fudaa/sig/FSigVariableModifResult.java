/*
 *  @creation     26 mai 2005
 *  @modification $Date: 2007-06-05 09:01:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: FSigVariableModifResult.java,v 1.6 2007-06-05 09:01:12 deniger Exp $
 */
public class FSigVariableModifResult {

  private final TIntArrayList idx_;
  private final FastBitSet idxSet_;
  private int maxIdx_;
  private int minIdx_;
  final int nbVar_;
  private final TDoubleArrayList[] varRes_;
  final H2dVariableProviderInterface target_;

  public int getNbVar() {
    return nbVar_;
  }

  public EfGridInterface getGrid() {
    return target_.getGrid();
  }

  public final boolean isTargetElement() {
    return target_.isElementVar();
  }

  /**
   * @param _nbVar le nombre de variables attendues
   * @param _set les indices a true sont consideres comme deja faits
   * @param _target la cible
   */
  public FSigVariableModifResult(final int _nbVar, final FastBitSet _set, final H2dVariableProviderInterface _target) {
    super();
    idxSet_ = _set;
    target_ = _target;
    maxIdx_ = target_.isElementVar() ? target_.getGrid().getEltNb() - 1 : target_.getGrid().getPtsNb() - 1;
    nbVar_ = _nbVar;
    varRes_ = new TDoubleArrayList[nbVar_];
    for (int i = varRes_.length - 1; i >= 0; i--) {
      varRes_[i] = new TDoubleArrayList();
    }
    idx_ = new TIntArrayList();
  }

  public int[] getUnsetIndex() {
    final TIntArrayList res = new TIntArrayList();
    for (int i = minIdx_; i <= maxIdx_; i++) {
      if (!idxSet_.get(i)) {
        res.add(i);
      }
    }
    return res.toNativeArray();
  }

  public void go(final CtuluCommandContainer _cmd, final H2dVariableType[] _var,
          final H2dVariableProviderInterface _target) {
    if (_var.length != nbVar_) {
      throw new IllegalArgumentException("bad size for var array");
    }
    final int[] idx = idx_.toNativeArray();
    for (int i = 0; i < nbVar_; i++) {
      _target.getModifiableModel(_var[i]).set(idx, this.varRes_[i].toNativeArray(), _cmd);
    }
  }

  public final int getMaxIdx() {
    return maxIdx_;
  }

  public final int getMinIdx() {
    return minIdx_;
  }

  public boolean isDone(final int _idx) {
    return idxSet_.get(_idx);
  }

  /**
   * Recalcul les bornes min max des points/elements non initialises.
   */
  public void computeMinMax() {
    for (int i = minIdx_ + 1; i <= maxIdx_; i++) {
      if (idxSet_.get(i)) {
        minIdx_ = i;
      } else {
        break;
      }
    }
    if (maxIdx_ == 0) {
      return;
    }
    for (int i = maxIdx_ - 1; i >= minIdx_; i--) {
      if (idxSet_.get(i)) {
        maxIdx_ = i;
      } else {
        break;
      }
    }
  }

  /**
   * @return le nombre de point actuellement modifie
   */
  public int getNbPtDone() {
    return idx_.size();
  }

  /**
   * @param _idx l'indice a modifier
   * @param _values les valeurs pour chaque variable
   */
  public void setDone(final int _idx, final double[] _values) {
    if (_values.length != nbVar_) {
      throw new IllegalArgumentException();
    }
    idxSet_.set(_idx);
    idx_.add(_idx);
    for (int i = _values.length - 1; i >= 0; i--) {
      varRes_[i].add(_values[i]);
    }
  }
}
