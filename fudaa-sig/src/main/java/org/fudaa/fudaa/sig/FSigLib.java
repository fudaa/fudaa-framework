/*
 *  @creation     22 sept. 2005
 *  @modification $Date: 2008-03-18 11:05:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuMenu;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.fudaa.commun.save.FudaaSaveZipWriter;


/**
 * Classe fourre tout d'utilitaires.
 * @author Fred Deniger
 * @version $Id: FSigLib.java,v 1.16.2.1 2008-03-18 11:05:45 bmarchan Exp $
 */
public final class FSigLib {

  /**
   * Pas d'instanciation possible de la classe.
   */
  private FSigLib() {}

  /**
   * Ajout des fichiers readme.txt dans le fichier projet zippe.
   * @param _loader Le fichier projet zippe.
   * @throws IOException En cas de pb d'�criture.
   */
  public static void addReadmeFile(final FudaaSaveZipWriter _loader) throws IOException {
    final String lisezmoi = "lisez-moi.txt";
    final String readme = "readme.txt";
    _loader.startEntry(lisezmoi);
    CtuluLibFile
        .copyStream(FSigResource.FSIG.getStream("persistence/" + lisezmoi), _loader.getOutStream(), true, false);
    _loader.startEntry(readme);
    CtuluLibFile.copyStream(FSigResource.FSIG.getStream("persistence/" + readme), _loader.getOutStream(), true, false);
  }

  /**
   * Retourne une chaine traduite dans le langage courant pour la chaine r�f�rence.
   * @param _s La chaine r�f�rence
   * @return La chaine traduite.
   */
  public static String getS(final String _s) {
    return FSigResource.FSIG.getString(_s);
  }

  /**
   * Retourne une chaine traduite dans le langage courant pour la chaine r�f�rence avec insertion d'une valeur.
   * @param _s La chaine r�f�rence.
   * @param _v0 La valeur � ins�rer � l'emplacement {0} de la chaine r�f�rence.
   * @return La chaine traduite.
   */
  public static String getS(final String _s, final String _v0) {
    return FSigResource.FSIG.getString(_s, _v0);
  }

  /**
   * Retourne une chaine traduite dans le langage courant pour la chaine r�f�rence avec insertion de 2 valeurs.
   * @param _s La chaine r�f�rence.
   * @param _v0 La valeur � ins�rer � l'emplacement {0} de la chaine r�f�rence.
   * @param _v1 La valeur � ins�rer � l'emplacement {1} de la chaine r�f�rence.
   * @return La chaine traduite.
   */
  public static String getS(final String _s, final String _v0, final String _v1) {
    return FSigResource.FSIG.getString(_s, _v0, _v1);
  }

  /**
   * Ajoute les actions de selection au menu pass�
   * @param _menu Le menu auquel ajouter les actions de s�lection.
   */
  public static void addSelectionAction(final BuMenu _menu) {
    ZCalqueSelectionInteractionAbstract.addSelectionAction(_menu,false);
  }
}
