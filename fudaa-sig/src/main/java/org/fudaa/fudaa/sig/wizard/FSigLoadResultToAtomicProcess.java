/*
 GPL 2
 */
package org.fudaa.fudaa.sig.wizard;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelAttributeToAtomicSubstitutionAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

/**
 * Permet de transformer les attributs d'un chargement en atomique.
 *
 * @author Frederic Deniger
 */
public class FSigLoadResultToAtomicProcess {
  private final boolean useNameToFindKnownAtt;

  public FSigLoadResultToAtomicProcess(boolean useNameToFindKnownAtt) {
    this.useNameToFindKnownAtt = useNameToFindKnownAtt;
  }

  private GISDataModel transform(GISDataModel gisDataModel) {
    GISDataModel res = gisDataModel;
    int nbAttributes = gisDataModel.getNbAttributes();
    for (int i = 0; i < nbAttributes; i++) {
      GISAttributeInterface attribute = gisDataModel.getAttribute(i);
      if (!attribute.isAtomicValue()) {
        GISAttributeInterface target = GISLib.createCommonAttributeFrom(attribute.getName(), attribute.getDataClass(), useNameToFindKnownAtt);
        if (target.isAtomicValue()) {
          res = new GISDataModelAttributeToAtomicSubstitutionAdapter(res, attribute, target);
        }
      }
    }
    return res;
  }

  public FSigGeomSrcData transform(FSigGeomSrcData init) {
    FSigGeomSrcData res = new FSigGeomSrcData();
    res.setNbPoints(init.getNbPoints());
    res.setNbPointsTotal(init.getNbPointsTotal());
    res.setNbPolygones(init.getNbPolygones());
    res.setNbPolylignes(init.getNbPolylignes());
    //doit on transformer les points
    res.setPoints(init.getPoints());
    res.setPolygones(transformPolygons(init));
    res.setPolylignes(transformPolylines(init));

    return res;
  }

  public GISDataModel[] transformPolygons(FSigGeomSrcData init) {
    GISDataModel[] polygones = init.getPolygones();
    GISDataModel[] targetPolygones = new GISDataModel[polygones.length];
    for (int i = 0; i < targetPolygones.length; i++) {
      targetPolygones[i] = transform(polygones[i]);
    }
    return targetPolygones;
  }

  public GISDataModel[] transformPolylines(FSigGeomSrcData init) {
    GISDataModel[] polylines = init.getPolylignes();
    GISDataModel[] targetPolylines = new GISDataModel[polylines.length];
    for (int i = 0; i < targetPolylines.length; i++) {
      targetPolylines[i] = transform(polylines[i]);
    }
    return targetPolylines;
  }
}
