/*
 * @creation 27 juil. 06
 * @modification $Date: 2007-05-04 14:00:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.AbstractButton;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.RasterDataInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPointMutable;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditableInterface;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author fred deniger
 * @version $Id: FSigImageWizardStepCalageLogic.java,v 1.3 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigImageWizardStepCalageLogic implements ActionListener {

  /**
   * @author fred deniger
   * @version $Id: FSigImageWizardStepCalageLogic.java,v 1.3 2007-05-04 14:00:26 deniger Exp $
   */
  static final class SpecificListener implements GISZoneListener {

    final FSigImageWizardStepCalageLogic logic_;

    public SpecificListener(final FSigImageWizardStepCalageLogic _logic) {
      super();
      logic_ = _logic;
    }

    @Override
    public void attributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
    }

    @Override
    public void attributeValueChangeAction(Object _source, int att, GISAttributeInterface _att, int geom, Object value) {
      logic_.tableModel_.updateError();
      logic_.tableModel_.fireTableRowsUpdated(0, logic_.tableModel_.getRowCount() - 1);
      logic_.majTextFields();
    }

    @Override
    public void objectAction(Object _source, int geom, Object _obj, int _action) {
      logic_.tableModel_.updateError();
      if (_action == OBJECT_ACTION_MODIFY) {
        logic_.tableModel_.fireTableRowsUpdated(0, logic_.tableModel_.getRowCount() - 1);
        logic_.majTextFields();
      } else if (_action == OBJECT_ACTION_REMOVE && logic_.tableModel_.getRowCount() > 0) {
        logic_.ui_.getTable().clearSelection();
        logic_.tableModel_.fireTableRowsDeleted(0, logic_.tableModel_.getRowCount() - 1);
      } else if (_action == OBJECT_ACTION_ADD) {
        logic_.tableModel_.fireTableRowsInserted(0, logic_.tableModel_.getRowCount() - 1);
      }
    }
  }
  CtuluCommandManager mng_;
  ZCalquePointEditable ptCalque_;
  FSIgImageWizartStepCalageUI ui_;
  final FSigImageWizardTableModel tableModel_;

  public FSigImageWizardStepCalageLogic(final CtuluCommandManager _mng, final CtuluUI _ui, final int _hImg) {
    mng_ = _mng;
    ui_ = new FSIgImageWizartStepCalageUI(_ui);
    final GISZoneListenerDispatcher gisZoneListenerDispatcher = new GISZoneListenerDispatcher();
    final GISZoneCollectionPoint pt = new GISZoneCollectionPoint(gisZoneListenerDispatcher);
    gisZoneListenerDispatcher.addListener(new SpecificListener(this));
    final GISAttributeDouble[] realXY = new GISAttributeDouble[]{
      new GISAttributeDouble(FSigLib.getS("X r�el"), true), new GISAttributeDouble(FSigLib.getS("Y r�el"), true)};
    pt.setAttributes(realXY, null);
    final ZModelePointEditable modele = new ZModelePointEditable(pt);
    ptCalque_ = new ZCalquePointEditable(modele, null);
    tableModel_ = new FSigImageWizardTableModel(modele, _hImg);

  }

  protected int[] majTextFields() {
    final int[] idx = ui_.getTable().getSelectedRows();
    final ZModelePointEditableInterface ptModel = getPtModel();
    final GISZoneCollection geomData = ptModel.getGeomData();
    if (idx != null && idx.length == 1) {
      final int i = idx[0];
      ui_.setTfXPixel(new Integer((int) ptModel.getX(i)));
      ui_.setTfYPixel(new Integer((int) ptModel.getY(i)));
      ui_.setTfXReel((Number) (geomData.getModel(0).getObjectValueAt(i)));
      ui_.setTfYReel((Number) (geomData.getModel(1).getObjectValueAt(i)));
    } else {
      ui_.clearTextField();
    }
    return idx;
  }

  public void setRasterData(final RasterDataInterface _data) {
    if (getPtModel().getNombre() > 0) {
      final int[] idx = new int[getPtModel().getNombre()];
      for (int i = idx.length - 1; i >= 0; i--) {
        idx[i] = i;
      }
      getPtModel().removePoint(idx, null);
    }
    final int h = tableModel_.getHImage();
    for (int i = 0; i < _data.getNbPt(); i++) {
      ((GISZoneCollectionPoint) getPtModel().getGeomData()).add(_data.getPtImgX(i), h - _data.getPtImgY(i), 0D,
                                                                Arrays.asList(new Double[]{CtuluLib.getDouble(_data.getPtX(i)), CtuluLib.getDouble(_data.getPtY(
                i))}), null);
    }
    tableModel_.updateError();
  }

  ZModelePointEditableInterface getPtModel() {
    return (ZModelePointEditableInterface) ptCalque_.getModelEditable();
  }

  private boolean isModifying() {
    return ui_.getModifiedRow() >= 0;
  }

  public FSigImageWizardTableModel.Result getResult() {
    return tableModel_.getResult();
  }

  public void setHImg(final int _hImg) {
    tableModel_.setHImage(_hImg);
  }

  protected JPanel createEditorPanel(final AbstractButton _btAddPt, final AbstractButton _btMovePoint) {
    final JPanel res = ui_.createEditorPanel(ptCalque_, tableModel_, _btAddPt, _btMovePoint);
    ui_.getBtAddMod().addActionListener(this);
    return res;
  }

  void addTmpLayer(final ZEbliCalquesPanel _pn) {
    final ZModelePoint modele = ui_.getTmpModel();
    final ZCalquePoint pt = new ZCalquePoint(modele);
    pt.setIconModel(0, new TraceIconModel(TraceIcon.PLUS_DOUBLE, 4, Color.BLUE));
    ui_.addListenerToTfPixel(createDocListener(pt));
    final BCalque cq = _pn.getCalqueActif();
    _pn.addCalque(pt, true);
    _pn.setCalqueActif(cq);
  }

  static DocumentListener createDocListener(final ZCalquePoint _pt) {
    return new DocumentListener() {

      @Override
      public void removeUpdate(DocumentEvent _e) {
        _pt.repaint();
      }

      @Override
      public void insertUpdate(DocumentEvent _e) {
        _pt.repaint();
      }

      @Override
      public void changedUpdate(DocumentEvent _e) {
        _pt.repaint();
      }
    };
  }

  public String valideAndGetError() {
    return tableModel_.getError();
  }

  protected void modifySelectionRow() {
    if (!ui_.isDataValid()) {
      return;
    }
    final Number xReel = ui_.getTfXReel();
    final Number yReel = ui_.getTfYReel();
    final Number xPixel = ui_.getTfXPixel();
    final Number yPixel = ui_.getTfYPixel();
    final int rowToModify = ui_.getModifiedRow();
    // on modifie un point existant
    if (rowToModify >= 0) {
      final CtuluCommandComposite cmp = new CtuluCommandComposite();
      getPtModel().setPoint(rowToModify, xPixel.intValue(), yPixel.intValue(), cmp);
      ((CtuluCollectionDoubleEdit) getPtModel().getGeomData().getModel(0)).set(rowToModify, xReel.doubleValue(), cmp);
      ((CtuluCollectionDoubleEdit) getPtModel().getGeomData().getModel(1)).set(rowToModify, yReel.doubleValue(), cmp);
      mng_.addCmd(cmp.getSimplify());
      ui_.getTable().getSelectionModel().setSelectionInterval(rowToModify, rowToModify);
    }
  }

  public void setNewPointActivated() {
    ui_.setNewPointActivated();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (isModifying()) {
      modifySelectionRow();
      return;
    }
    if (!ui_.isDataValid()) {
      return;
    }
    // on ajoute un point
    final Number xReel = ui_.getTfXReel();
    final Number yReel = ui_.getTfYReel();
    final Number xPixel = ui_.getTfXPixel();
    final Number yPixel = ui_.getTfYPixel();
    // on ajoute un point
    final GISPoint pt = new GISPoint(xPixel.intValue(), yPixel.intValue(), 0);
    final GISZoneCollectionPoint geomData = (GISZoneCollectionPoint) getPtModel().getGeomData();
    final GISPointMutable ptTemp = new GISPointMutable();
    for (int i = geomData.getNumGeometries() - 1; i >= 0; i--) {
      geomData.get(i, ptTemp);
      if (CtuluLibGeometrie.getD2(pt, ptTemp) < 1) {
        if (ui_.question(FSigLib.getS("Ajouter un point de calage"), FSigLib.getS("Le point de calage {0} existe d�j�",
                                                                                  '(' + ((int) pt.getX()) + CtuluLibString.VIR + ((int) pt.getY() + ')'))
                                                                     + CtuluLibString.LINE_SEP + FSigLib.getS(
                "Voulez-vous le modifier ?"))) {
          ui_.getTable().getSelectionModel().setSelectionInterval(i, i);
          modifySelectionRow();
        }

        return;
      }
    }

    geomData.add(pt.getX(), pt.getY(), 0, Arrays.asList(new Double[]{((Double) xReel), ((Double) yReel)}), mng_);
    ptCalque_.repaint();
    ui_.clearTextField();
    ui_.getTable().getSelectionModel().clearSelection();

  }

  public ZCalquePointEditable getPtCalque() {
    return ptCalque_;
  }

  public void ptClickedInImg(final int _xImg, final int _yImg) {
    ui_.setTfXPixel(new Integer(_xImg));
    ui_.setTfYPixel(new Integer(_yImg));
  }

  public void ptClickedInReel(final double _xreel, final double _yreel) {
    ui_.setTfXReel(CtuluLib.getDouble(_xreel));
    ui_.setTfYReel(CtuluLib.getDouble(_yreel));
  }

  protected CtuluCommandManager getMng() {
    return mng_;
  }

  protected CtuluUI getUi() {
    return ui_.getUI();
  }
}
