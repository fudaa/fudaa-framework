/*
 * @creation 22 juin 2006
 * @modification $Date: 2007-01-19 13:14:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.layer;

import javax.swing.Action;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZCalqueGrille;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author fred deniger
 * @version $Id: FSigVisuPanelController.java,v 1.1 2007-01-19 13:14:29 deniger Exp $
 */
public class FSigVisuPanelController extends ZEbliCalquePanelController {

  public FSigVisuPanelController(final CtuluUI _ui) {
    super(_ui);
  }

  protected ZCalqueGrille getGrille() {
    return getVisuPanel().grille_;
  }

  public FSigVisuPanel getVisuPanel() {
    return ((FSigVisuPanel) pn_);
  }

  @Override
  protected EbliActionInterface createRepereAction() {
    final EbliActionPaletteAbstract plAction = new FSigGrillePalette(getGrille(), getVisuPanel());
    plAction.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Transformations du rep�re"));
    return plAction;
  }
}
