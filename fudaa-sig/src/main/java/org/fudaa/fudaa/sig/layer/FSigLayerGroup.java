/*
 *  @creation     18 mai 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sig.layer;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneAttributeFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.fudaa.sig.FSigAttibuteTypeManager;
import org.fudaa.fudaa.sig.FSigAttributeTableModel;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.persistence.FSigLayerGroupPersistence;

/**
 * Une implementation d'un groupe de calques GIS. Il peut contenir des calques SIG points ou polylignes, avec attributs.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigLayerGroup extends ZCalqueEditionGroup {

  /**
   * @author fred deniger
   * @version $Id$
   */
  static final class CommandForLayerCreation implements CtuluCommand {

    private final BCalque parent_;
    private final ZCalqueAffichageDonnees la_;

    /**
     * @param _parent le calque parent
     * @param _la le calque ajout�
     */
    CommandForLayerCreation(BCalque _parent, ZCalqueAffichageDonnees _la) {
      parent_ = _parent;
      la_ = _la;
    }

    @Override
    public void redo() {
      parent_.enPremier(la_);
    }

    @Override
    public void undo() {
      parent_.remove(la_);
    }
  }

  /**
   * @author fred deniger
   * @version $Id$
   */
  public static final class NewNamePanel extends CtuluDialogPanel {

    /**
     *
     */
    private final BuTextField ft_;

    /**
     * @param _ft
     */
    public NewNamePanel(final BuTextField _ft) {
      ft_ = _ft;
    }

    @Override
    public boolean isDataValid() {
      final String txt = ft_.getText();
      if (txt.length() == 0) {
        setErrorText(CtuluLib.getS("Le champ est vide") + '!');
        return false;
      }
      return true;
    }
  }
  int grIdx_;
  EbliActionInterface actionAddGroup_;
  protected EbliActionInterface actionAddPoint_;
  protected EbliActionInterface actionAddPoly_;
  EbliActionInterface actionModifyAtt_;
  transient FSigAttibuteTypeManager att_;
  GISAttributeInterface[] attributes_;
  // final boolean canAddGroup_;
  protected transient final FSigVisuPanel visu_;

  FSigLayerGroup(final FSigVisuPanel _impl, final EbliActionInterface _actPoint, final EbliActionInterface _actPoly,
          final EbliActionInterface _modifyAttributes) {
    // canAddGroup_ = false;
    visu_ = _impl;
    att_ = visu_.getAttributeMng();
    actionAddPoly_ = _actPoly;
    actionAddPoint_ = _actPoint;
    actionModifyAtt_ = _modifyAttributes;
    setActions(new EbliActionInterface[]{actionAddPoly_, actionAddPoint_, null, actionModifyAtt_, null,
      visu_.getEditor().getExportAction(), visu_.getEditor().getImportAction()});
  }

  /**
   * @param _impl l'implementation parent
   */
  public FSigLayerGroup(final FSigVisuPanel _impl) {
    this(_impl, null);
  }

  public FSigLayerGroup(final FSigVisuPanel _impl, GISAttributeInterface[] _att) {
    super();
    visu_ = _impl;
    // canAddGroup_ = true;
    buildAddGroup();
    buildAddPoint();
    buildAddPoly();
    buildModifyAtt();
    setActions(new EbliActionInterface[]{actionAddGroup_, null, visu_.getEditor().getExportAction()});
    att_ = visu_.getAttributeMng();
    attributes_ = _att;
  }

  public FSigLayerGroup addGroupAct(final String _title, final BCalque _parent, final boolean _enPremier) {
    return addGroupAct(_title, _parent, _enPremier, null);
  }

  public FSigLayerGroup addGroupAct(final String _title, final BCalque _parent, final boolean _enPremier,
          final GISAttributeInterface[] _att) {
    /*
     * if (!canAddGroup_) { return null; }
     */
    final FSigLayerGroup la = new FSigLayerGroup(visu_, actionAddPoint_, actionAddPoly_, actionModifyAtt_);
    la.setDestructible(true);
    la.setName(BGroupeCalque.findUniqueChildName(_parent, "gr"));
    la.setTitle(_title);
    if (_enPremier) {
      _parent.enPremier(la);
    } else {
      _parent.enDernier(la);
    }
    if (_att != null) {
      la.attributes_ = new GISAttributeInterface[_att.length];
      System.arraycopy(_att, 0, la.attributes_, 0, _att.length);
    }
    return la;
  }

  protected Frame getParentFrame() {
    return CtuluLibSwing.getFrameAncestorHelper(visu_.getCtuluUI().getParentComponent());
  }

  protected void updateEditor() {
    visu_.getEditor().updatePalette();

  }
  /**
   * Pour le titre propose par d�faut d'un calque de lignes bris�es.
   */
  int idxPoly_;
  /**
   * Pour le titre propose par d�faut d'un calque de points.
   */
  int idxPt_;

  protected String askForName(final String _title, final boolean _poly) {
    final BuTextField ft = new BuTextField(CtuluLib.getS("Nouveau")
            + CtuluLibString.getEspaceString(_poly ? idxPoly_ : idxPt_));
    ft.setColumns(12);
    final CtuluDialogPanel pn = new NewNamePanel(ft);
    pn.addLabel(BuResource.BU.getString("Nom"));
    pn.add(ft);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(visu_.getFrame(), _title))) {
      if (_poly) {
        idxPoly_++;
      }
      idxPt_++;
      return ft.getText();
    }
    return null;
  }

  protected void buildAddGroup() {
    if (actionAddGroup_ == null) {
      actionAddGroup_ = new FSigLayerAction.AddGroupAction(visu_.getArbreCalqueModel());
    }
  }

  protected void buildAddPoint() {
    if (actionAddPoint_ == null) {
      actionAddPoint_ = new FSigLayerAction.AddPointAction(visu_.getArbreCalqueModel());
    }
  }

  protected void buildAddPoly() {
    if (actionAddPoly_ == null) {
      actionAddPoly_ = new FSigLayerAction.AddPolyAction(visu_.getArbreCalqueModel());
    }
  }

  protected void setAttributes(GISAttributeInterface[] attributes_) {
    this.attributes_ = attributes_;
  }

  protected void buildModifyAtt() {
    if (actionModifyAtt_ == null) {
      actionModifyAtt_ = new FSigLayerAction.ModifyAttributeAction(visu_.getArbreCalqueModel());
    }
  }

  protected void finishActionCreation(final BCalque _parent, final CtuluCommandContainer _cmd,
          final ZCalqueAffichageDonnees _la, final boolean _addCancel) {
    final List action = new ArrayList(5);
    action.addAll(Arrays.asList(visu_.getEditor().getEditAction()));
    action.add(visu_.getEditor().getActionDelete());
    if (_addCancel) {
      action.add(visu_.getEditor().getActionCancel());
    }
    action.add(null);
    action.add(visu_.getEditor().getExportAction());
    action.add(visu_.getEditor().getImportAction());
    final EbliActionInterface[] act = new EbliActionInterface[action.size()];
    action.toArray(act);
    _la.setActions(act);
    _la.setDestructible(true);
    _parent.enPremier(_la);
    if (_cmd != null) {
      _cmd.addCmd(new CommandForLayerCreation(_parent, _la));
    }
  }

  public ZCalqueLigneBrisee addLigneBriseeLayerAct(final String _title, final GISZoneCollectionLigneBrisee _zone) {
    return addLigneBriseeLayerAct(_title, _zone, visu_.getCmdMng());
  }

  @Override
  public ZCalqueLigneBrisee addLigneBriseeLayerAct(final String _title, final GISZoneCollectionLigneBrisee _zone,
          final CtuluCommandContainer _cmd) {
    return addLigneBriseeLayerAct(_title, new ZModeleLigneBriseeEditable(_zone), _cmd);
  }

  public ZCalqueLigneBrisee addLigneBriseeLayerAct(final String _title, final ZModeleLigneBriseeEditable _model,
          final CtuluCommandContainer _cmd) {
    final FSigLayerLineEditable la = new FSigLayerLineEditable(_model, visu_.getEditor());
    la.setName(BGroupeCalque.findUniqueChildName(this, "poly"));
    la.setTitle(_title);
    la.getDisplayer().feedVariable(this);
    finishActionCreation(this, _cmd, la, true);
    return la;
  }

  public ZCalquePointEditable addPointLayerAct(final String _title, final GISZoneCollectionPoint _zone) {
    return addPointLayerAct(_title, _zone, visu_.getCmdMng());
  }

  @Override
  public ZCalquePointEditable addPointLayerAct(final String _title, final GISZoneCollectionPoint _zone,
          final CtuluCommandContainer _cmd) {
    return addPointLayerAct(_title, new ZModelePointEditable(_zone), _cmd);
  }

  public ZCalquePointEditable addPointLayerAct(final String _title, final ZModelePointEditable _modele,
          final CtuluCommandContainer _cmd) {
    final FSigLayerPointEditable la = new FSigLayerPointEditable(_modele, visu_.getEditor());
    la.setName(BGroupeCalque.findUniqueChildName(this, "point"));
    la.setTitle(_title);
    la.getDisplayer().feedVariable(this);
    finishActionCreation(this, _cmd, la, false);
    return la;
  }

  @Override
  public GISZoneCollectionLigneBrisee createLigneBriseeZone() {
    return GISZoneAttributeFactory.createPolyligne(visu_.getEditor(), attributes_);
  }

  @Override
  public GISZoneCollectionPoint createPointZone() {
    return GISZoneAttributeFactory.createPoint(visu_.getEditor(), attributes_);
  }

  public FSigEditor getGisEditor() {
    return visu_.getEditor();
  }

  @Override
  public final GISAttributeInterface[] getAttributes() {
    return attributes_;
  }

  protected void basicDetruire(final BCalque _c) {
    super.detruire(_c);
  }

  @Override
  public void detruire(final BCalque _c) {
    final int idx = CtuluLibArray.findObjectEgalEgal(getComponents(), _c);
    basicDetruire(_c);
    if (idx >= 0 && getCmdMng() != null) {
      getCmdMng().addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          FSigLayerGroup.this.basicDetruire(_c);
        }

        @Override
        public void undo() {
          FSigLayerGroup.this.add(_c, idx);

        }
      });

    }
  }

  @Override
  public CtuluCommandContainer getCmdMng() {
    return visu_.getCmdMng();
  }

  /**
   * @return true si des groupes peuvent etre ajoutes
   */
  /*
   * public final boolean isCanAddGroup() { return canAddGroup_; }
   */
  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new FSigLayerGroupPersistence();
  }

  public static void editFor(final FSigLayerGroup _layer) {
    if (_layer == null) {
      return;
    }
    final FSigAttributeTableModel model = new FSigAttributeTableModel(_layer.getAttributes(), true);
    final CtuluListEditorPanel panel = model.buildEditor();
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    pn.add(new BuLabel(FSigLib.getS("Modifier les attributs du groupe {0}", _layer.getTitle())), BuBorderLayout.NORTH);
    pn.add(panel, BuBorderLayout.CENTER);
    if (pn.afficheModaleOk(_layer.getParentFrame(), _layer.getTitle())) {
      final BCalque[] cq = _layer.getCalques();
      final GISAttributeInterface[] att = model.getAttributes();
      final Map newName = model.getRenamedAttribute();
      if (Arrays.equals(att, _layer.attributes_) && newName.size() == 0) {
        return;
      }
      final CtuluCommandComposite cmp = new CtuluCommandComposite() {
        @Override
        protected void actionToDoAfterUndoOrRedo() {
          _layer.visu_.getArbreCalqueModel().fireObservableChanged();
          _layer.updateEditor();
        }

        @Override
        protected boolean isActionToDoAfterUndoOrRedo() {
          return true;
        }
      };
      for (int i = 0; i < cq.length; i++) {
        if (cq[i] instanceof ZCalqueAffichageDonneesInterface
                && ((ZCalqueAffichageDonneesInterface) cq[i]).modeleDonnees() instanceof ZModeleGeometry) {
          final ZModeleGeometry donnees = (ZModeleGeometry) ((ZCalqueAffichageDonneesInterface) cq[i]).modeleDonnees();
          donnees.getGeomData().setAttributes(att, cmp);
        }
      }
      final GISAttributeInterface[] old = _layer.attributes_;
      _layer.attributes_ = att;
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          _layer.attributes_ = att;

        }

        @Override
        public void undo() {
          _layer.attributes_ = old;

        }
      });

      final boolean changed = FSigAttributeTableModel.rename(newName);
      if (changed) {

        cmp.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            FSigAttributeTableModel.rename(newName);

          }

          @Override
          public void undo() {
            FSigAttributeTableModel.rename(newName);
          }
        });
      }
      if (cmp.getNbCmd() > 0) {
        _layer.visu_.getArbreCalqueModel().fireObservableChanged();
        _layer.updateEditor();
        _layer.getCmdMng().addCmd(cmp);
      }

    }
  }

  public FSigAttibuteTypeManager getAtt() {
    return att_;
  }

  /**
   * On autorise la modification du manager d'attribut
   *
   * @param _att the att to set
   */
  public void setAtt(FSigAttibuteTypeManager _att) {
    att_ = _att;
  }
}
