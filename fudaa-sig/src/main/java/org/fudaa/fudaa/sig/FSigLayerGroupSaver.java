/*
 *  @creation     25 ao�t 2005
 *  @modification $Date: 2007-01-19 13:14:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ebli.calque.BCalqueSaverGroup;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.sig.persistence.FSigLayerGroupPersistence;

/**
 * Une classe permettant la sauvegarde des calques SIG.
 * 
 * @deprecated ne plus utiliser. Tout est dans MvSigLayerGroupPersistence
 * @author Fred Deniger
 * @version $Id: FSigLayerGroupSaver.java,v 1.11 2007-01-19 13:14:21 deniger Exp $
 */
public class FSigLayerGroupSaver extends BCalqueSaverGroup {

  GISAttributeInterface[] attributes_;

  // boolean isMain_;

  /**
   * @deprecated ne doit plus etre utilisee
   */
  protected FSigLayerGroupSaver() {
    setPersistenceClass(FSigLayerGroupPersistence.class.getName());

  }

  @Override
  public EbliUIProperties getUI() {
    final EbliUIProperties res = super.getUI();
    // res.put(MvSigLayerGroupPersistence.getMainId(), isMain_);
    if (attributes_ != null) {
      res.put(FSigLayerGroupPersistence.getAttributeId(), attributes_);
      attributes_ = null;
    }
    return res;
  }

}
