/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2008/02/04 17:14:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.reflux.io.ReflucadSEMFileFormat;

/**
 * Un lecteur de fichier Reflucad au format SEM (semis) contenant des objets g�om�triques
 * de type multipoint.
 * 
 * @author Bertrand Marchand
 * @version $Id: FSigFileLoaderReflucadSEM.java,v 1.1.2.2 2008/02/04 17:14:10 bmarchan Exp $
 */
public class FSigFileLoaderReflucadSEM implements FSigFileLoaderI {

  /** Le filtre sp�cifique aux fichiers Reflucad Sem */
  protected final BuFileFilter ft_;
  /** Les points contenus dans le fichier */
  protected GISZoneCollectionPoint pts_;

  /**
   * Construction d'un chargeur avec filtre de fichier par d�faut.
   */
  public FSigFileLoaderReflucadSEM() {
    super();
    ft_ = ReflucadSEMFileFormat.getInstance().createFileFilter();
  }

  public FSigFileLoaderReflucadSEM(final BuFileFilter _ft) {
    ft_ = _ft;
  }

  /* @override */
  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderReflucadSEM(ft_);
  }

  /* @override */
  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  /* @override */
  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final CtuluIOOperationSynthese op=ReflucadSEMFileFormat.getInstance().read(_f, _prog);
    // En cas d'erreur, fin d'import.
    if (op.containsMessages()&&op.getAnalyze().containsErrors()) {
      _analyze.merge(op.getAnalyze());
      return;
    }

    pts_=(GISZoneCollectionPoint)op.getSource();
    _r.nbPointTotal_+=pts_.getNumGeometries();
    _r.nbPoint_+=pts_.getNumGeometries();
    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(pts_);
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    // 
    _r.pointModel_.add(adapter);
    _r.addUsedAttributes(GISLib.getAttributeFrom(pts_));
  }
}
