package org.fudaa.fudaa.sig.layer;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuTitledPanel;
import com.memoire.bu.BuValueValidator;
import com.memoire.bu.BuVerticalLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesLigneConfigure;
import org.fudaa.ebli.calque.ZCalqueGrille;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BControleVisible;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurReduitFonteNewVersion;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.controle.BSelecteurTextField;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author fred deniger
 * @version $Id: FSigGrillePalette.java,v 1.2 2007-04-16 16:35:35 deniger Exp $
 */
@SuppressWarnings("serial")
public final class FSigGrillePalette extends EbliActionPaletteAbstract implements BSelecteurTargetInterface {

  ZEbliCalquesPanel panel_;
  final ZCalqueGrille target_;

  /**
   * @param _name
   * @param _icon
   * @param _name2
   */
  FSigGrillePalette(final ZCalqueGrille _target, final ZEbliCalquesPanel _panel) {
    super(EbliResource.EBLI.getString("Repere"), EbliResource.EBLI.getIcon("repere"),
            "CHANGE_REFERENCE");
    target_ = _target;
    panel_ = _panel;
  }

  protected BuTextField addDoubleText(final Container _c) {
    final BuTextField r = new BuTextField(10);
    r.setCharValidator(BuCharValidator.DOUBLE);
    r.setStringValidator(BuStringValidator.DOUBLE);
    r.setValueValidator(BuValueValidator.DOUBLE);
    _c.add(r);
    return r;
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.addPropertyChangeListener(_key, _l);

  }

  @Override
  public JComponent buildContentPane() {
    final BuPanel content = new BuPanel(new BuVerticalLayout(2));
    content.add(createMainProperties());
    content.add(new AxisView().createPanel());
    content.add(new AxisView().initForY().createPanel());
    content.add(createZoom());
    return content;
  }

  BuComboBox createAxeComboBox() {
    return new BuComboBox(new String[]{EbliLib.getS("Graduations manuelles: nombre"),
      EbliLib.getS("Graduations manuelles: longueur"), EbliLib.getS("Graduations automatiques: Nombre")});
  }

  /**
   * Identifiant pour le tf definissant le nombre de graduation
   */
  public static String CHOOSE_GRADUATION_NB = "CHOOSE_GRADUATION_NB";

  /**
   * Identifiant pour le tf definissant le nombre de graduation
   */
  public static String CHOOSE_GRADUATION_LENGTH = "CHOOSE_GRADUATION_LENGTH";

  protected class AxisView {

    protected String propAxeVisible_ = ZCalqueGrille.AXIS_X_VISIBLE;
    protected String propMainGraduationModeAuto_ = ZCalqueGrille.GRADUATION_X_MODE_AUTO;
    protected String propMainGraduationModeLength_ = ZCalqueGrille.GRADUATION_X_MODE_LENGTH;
    protected String propMainGraduationLength_ = ZCalqueGrille.GRADUATION_X_LENGTH;
    protected String propMainGraduationNb_ = ZCalqueGrille.GRADUATION_X_NB;
    protected String propMinorGraduationVisible_ = ZCalqueGrille.MINOR_GRADUATION_X_VISIBLE;
    protected String propMinorGraduationNb_ = ZCalqueGrille.MINOR_GRADUATION_X_NB;
    protected String titleCbShowAxe_ = EbliLib.getS("Afficher l'axe X");
    JToggleButton cbShowAxe_;
    BuComboBox mainGraduationType_;
    JToggleButton cbShowMinorGraduation_;
    BuTextField tfMinorGraduation_;
    BuPanel pnMainGraduation_;
    CardLayout layoutCardX_;
    BuComboBox cbGraduationMode_;

    protected AxisView initForY() {
      propAxeVisible_ = ZCalqueGrille.AXIS_Y_VISIBLE;
      propMainGraduationModeAuto_ = ZCalqueGrille.GRADUATION_Y_MODE_AUTO;
      propMainGraduationModeLength_ = ZCalqueGrille.GRADUATION_Y_MODE_LENGTH;
      propMainGraduationLength_ = ZCalqueGrille.GRADUATION_Y_LENGTH;
      propMainGraduationNb_ = ZCalqueGrille.GRADUATION_Y_NB;
      propMinorGraduationVisible_ = ZCalqueGrille.MINOR_GRADUATION_Y_VISIBLE;
      propMinorGraduationNb_ = ZCalqueGrille.MINOR_GRADUATION_Y_NB;
      titleCbShowAxe_ = EbliLib.getS("Afficher l'axe Y");
      return this;
    }

    void updateMainGraduationType() {
      final int selectedIndex = cbGraduationMode_.getSelectedIndex();
      setProperty(propMainGraduationModeAuto_, selectedIndex >= 2);
      setProperty(propMainGraduationModeLength_, Boolean.valueOf(selectedIndex == 1
              || selectedIndex == 3));
      if (selectedIndex == 0 || selectedIndex == 2) {
        layoutCardX_.show(pnMainGraduation_, CHOOSE_GRADUATION_NB);
      } else if (selectedIndex == 1 || selectedIndex == 3) {
        layoutCardX_.show(pnMainGraduation_, CHOOSE_GRADUATION_LENGTH);
      }
      updateSousGraduation();
    }

    void updateSousGraduation() {
      tfMinorGraduation_.setEnabled(cbShowMinorGraduation_.isSelected() && target_.isDrawX()
              && !target_.isModeAutomatiqueX());
    }

    protected JPanel createPanel() {
      BSelecteurCheckBox selectCb = new BSelecteurCheckBox(propAxeVisible_);
      selectCb.setSelecteurTarget(FSigGrillePalette.this);
      cbShowAxe_ = selectCb.getCb();
      cbShowAxe_.setText(titleCbShowAxe_);

      cbGraduationMode_ = createAxeComboBox();
      final boolean auto = ((Boolean) target_.getProperty(propMainGraduationModeAuto_)).booleanValue();
      final boolean length = ((Boolean) target_.getProperty(propMainGraduationModeLength_)).booleanValue();
      if (auto) {
        cbGraduationMode_.setSelectedIndex(2);
      } else {
        cbGraduationMode_.setSelectedIndex(length ? 1 : 0);
      }
      cbGraduationMode_.addItemListener(new ItemListener() {
        /**
         *
         */
        @Override
        public void itemStateChanged(final ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED) {
            updateMainGraduationType();
          }
        }
      });

      layoutCardX_ = new CardLayout();
      pnMainGraduation_ = new BuPanel(layoutCardX_);

      final BuPanel mainPanel = new BuTitledPanel(cbShowAxe_, new BuGridLayout(2, 5, 5, true,
              false, true, true, true));
      cbShowAxe_.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          CtuluLibSwing.setEnable(mainPanel, cbShowAxe_, cbShowAxe_.isSelected());
          updateSousGraduation();
        }

      });
      mainPanel.add(cbGraduationMode_/* , BorderLayout.NORTH */);
      mainPanel.add(pnMainGraduation_/* , BorderLayout.CENTER */);
      BSelecteurTextField selecteurTextField = new BSelecteurTextField(propMainGraduationNb_, BuValueValidator.INTEGER);
      selecteurTextField.setSelecteurTarget(FSigGrillePalette.this);
      final BuTextField tfGraduationNb = (BuTextField) selecteurTextField.getComponents()[0];
      tfGraduationNb.setStringValidator(BuStringValidator.INTEGER);
      tfGraduationNb.setValueValidator(BuValueValidator.MULTI(BuValueValidator.INTEGER,
              BuValueValidator.MINMAX(0, 5000)));
      selecteurTextField.updateFromTarget();
      // tfGraduationNb.setColumns(5);
      pnMainGraduation_.add(tfGraduationNb, CHOOSE_GRADUATION_NB);
      selecteurTextField = new BSelecteurTextField(propMainGraduationLength_,
              BuValueValidator.DOUBLE);
      selecteurTextField.setSelecteurTarget(FSigGrillePalette.this);
      final BuTextField tfMainGraduationLength = (BuTextField) selecteurTextField.getComponents()[0];
      selecteurTextField.updateFromTarget();
      pnMainGraduation_.add(tfMainGraduationLength, CHOOSE_GRADUATION_LENGTH);
      // --on affiche le manuel 1 par defaut --//
      layoutCardX_.show(pnMainGraduation_, CHOOSE_GRADUATION_NB);// TODO a verifier

      selectCb = new BSelecteurCheckBox(propMinorGraduationVisible_);
      selectCb.setSelecteurTarget(FSigGrillePalette.this);
      cbShowMinorGraduation_ = selectCb.getCb();
      cbShowMinorGraduation_.setText(EbliLib.getS("Dessiner les sous-graduations"));
      selecteurTextField = new BSelecteurTextField(propMinorGraduationNb_,
              BuValueValidator.INTEGER);

      selecteurTextField.setSelecteurTarget(FSigGrillePalette.this);
      tfMinorGraduation_ = (BuTextField) selecteurTextField.getComponents()[0];
      tfMinorGraduation_.setStringValidator(BuStringValidator.INTEGER);
      selecteurTextField.updateFromTarget();
      // tfMinorGraduation_.setColumns(5);
      mainPanel.add(cbShowMinorGraduation_);
      cbShowMinorGraduation_.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(final ItemEvent e) {
          updateSousGraduation();
        }
      });
      mainPanel.add(tfMinorGraduation_/* , BorderLayout.CENTER */);
      return mainPanel;
    }
  }

  private BuPanel createMainProperties() {
    final BuPanel pn = new BuPanel();
    pn.setBorder(CtuluLibSwing.createTitleBorder(super.getTitle()));
    pn.setLayout(new BuGridLayout(2, 3, 3));
    final BControleVisible vi = new BControleVisible();
    vi.setTarget(target_);
    pn.add(new BuLabel(EbliLib.getS("Visible")));
    pn.add(vi);
    pn.add(new BuLabel(EbliLib.getS("Grille")));
    final BuCheckBox cb = new BuCheckBox();
    cb.setSelected(target_.isDrawGrid());
    cb.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        target_.setDrawGrid(cb.isSelected());
      }

    });
    pn.add(cb);
    installTraceLigne(pn, new ZCalqueAffichageDonneesLigneConfigure(target_, 0));
    installTraceLigne(pn, new ZCalqueAffichageDonneesLigneConfigure(target_, 1));

    pn.add(new BuLabel(EbliLib.getS("Fonte")));
    final BuButton btFont = new BuButton("1234.567");
    pn.add(btFont);
    btFont.setFont(target_.getFont());
    btFont.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        final JDialog d = new JDialog(CtuluLibSwing.getFrameAncestorHelper(target_));
        final BSelecteurReduitFonteNewVersion ft = new BSelecteurReduitFonteNewVersion(btFont.getFont(), d);
        ft.setTarget(target_);
        d.setContentPane(ft.getComponent());
        d.setModal(true);
        d.pack();
        d.setLocationRelativeTo(target_);
        d.setVisible(true);
        btFont.setFont(target_.getFont());

      }

    });
    return pn;
  }

  /**
   * @param pn
   * @param lineConf
   */
  private void installTraceLigne(final BuPanel pn, final ZCalqueAffichageDonneesLigneConfigure lineConf) {
    final BSelecteurInterface[] jc = lineConf.createSelecteurs();
    pn.add(new JLabel(lineConf.getTitle()));
    final JComponent[] components = jc[0].getComponents();
    jc[0].setSelecteurTarget(lineConf);
    final JPanel pnComps = new JPanel(new BuGridLayout(components.length));
    for (int i = 0; i < components.length; i++) {
      pnComps.add(components[i]);
    }
    pn.add(pnComps);
  }

  private class ZoomContainer {

    private final BuTextField tfMinX;
    private final BuTextField tfMaxX;
    private final BuTextField tfMinY;
    private final BuTextField tfMaxY;

    /**
     * @param tfMinX
     * @param tfMaxX
     * @param tfMinY
     * @param tfMaxY
     */
    public ZoomContainer(final BuTextField tfMinX, final BuTextField tfMaxX, final BuTextField tfMinY,
            final BuTextField tfMaxY) {
      super();
      this.tfMinX = tfMinX;
      this.tfMaxX = tfMaxX;
      this.tfMinY = tfMinY;
      this.tfMaxY = tfMaxY;
    }

    protected void updateFromView() {
      final GrBoite viewBoite = panel_.getVueCalque().getViewBoite();
      tfMinX.setText(CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS.format(viewBoite.getMinX()));
      tfMaxX.setText(CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS.format(viewBoite.getMaxX()));
      tfMinY.setText(CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS.format(viewBoite.getMinY()));
      tfMaxY.setText(CtuluNumberFormatDefault.DEFAULT_FMT_3DIGITS.format(viewBoite.getMaxY()));
    }

    protected boolean isAllSet() {
      final Double maxX = (Double) tfMaxX.getValue();
      final Double maxY = (Double) tfMaxY.getValue();
      final Double minX = (Double) tfMinX.getValue();
      final Double minY = (Double) tfMinY.getValue();
      return (maxX != null && maxY != null && minX != null && minY != null);
    }

    protected void applyToPanel(boolean zoomX) {
      if (isAllSet()) {
        final Double minx = (Double) tfMinX.getValue();
        final Double miny = (Double) tfMinY.getValue();
        Double maxx = (Double) tfMaxX.getValue();
        Double maxy = (Double) tfMaxY.getValue();
        final double deltax = Math.abs(maxx - minx);
        final double deltay = Math.abs(maxy - miny);
        /*
         if (deltay > deltax) {
         maxx = Double.valueOf(minx.doubleValue() + deltay);
         } else if (deltax > deltay) {
         maxy = Double.valueOf(miny.doubleValue() + deltax);
         }
         */
        target_.getVue().changeRepereZoom(target_, new GrBoite(new GrPoint(minx, miny, 0), new GrPoint(maxx, maxy, 0)), 0, 0, 0, false, false, zoomX);
        //target_.changeZoom(minx, miny, maxx, maxy);
      }

    }
  }

  private BuPanel createZoom() {
    final BuPanel select = new BuPanel(new BuGridLayout(4, 3, 3, false, false, false, false, false));

    select.add(new BuLabel(EbliLib.getS("")));
    select.add(new BuLabel(EbliLib.getS("Min")));
    select.add(new BuLabel(EbliLib.getS("Max")));
    select.add(new BuLabel(EbliLib.getS("")));

    select.add(new BuLabel(EbliLib.getS("X")));
    final BuTextField tfMinX = addDoubleText(select);
    final BuTextField tfMaxX = addDoubleText(select);

    final BuButton btZoomX = new BuButton(EbliLib.getS("Appliquer le zoom sur X"));
    btZoomX.setIcon(EbliResource.EBLI.getToolIcon("zoom-selection"));
    btZoomX.setEnabled(true);
    select.add(btZoomX);

    select.add(new BuLabel(EbliLib.getS("Y")));
    final BuTextField tfMinY = addDoubleText(select);
    final BuTextField tfMaxY = addDoubleText(select);

    final BuButton btZoomY = new BuButton(EbliLib.getS("Appliquer le zoom sur Y"));
    btZoomY.setIcon(EbliResource.EBLI.getToolIcon("zoom-selection"));
    btZoomY.setEnabled(true);
    select.add(btZoomY);

    final ZoomContainer container = new ZoomContainer(tfMinX, tfMaxX, tfMinY, tfMaxY);
    final PropertyChangeListener listener = new PropertyChangeListener() {

      @Override
      public void propertyChange(final PropertyChangeEvent evt) {
        container.updateFromView();
      }
    };

    btZoomX.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        container.applyToPanel(true);
      }
    });

    btZoomY.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        container.applyToPanel(false);
      }
    });

    panel_.getVueCalque().addPropertyChangeListener("repere", listener);
    // to update the textfields at construction
    container.updateFromView();

    final BuPanel zoom = new BuPanel(new BuVerticalLayout(3, false, false));
    zoom.setBorder(CtuluLibSwing.createTitleBorder(EbliLib.getS("Changer le zoom")));
    zoom.add(select);

    final BuButton btRestaurer = new BuButton(EbliResource.EBLI.getIcon("restore"),
            EbliResource.EBLI.getString("Restaurer"));
    btRestaurer.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        panel_.restaurer();
      }
    });
    zoom.add(btRestaurer);
    return zoom;
  }

  @Override
  public Object getMin(final String _key) {
    return null;
  }

  @Override
  public Object getMoy(final String _key) {
    return null;
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.addPropertyChangeListener(_key, _l);

  }

  /**
   *
   */
  @Override
  public Object getProperty(final String _key) {
    return target_.getProperty(_key);
  }

  /**
   *
   */
  @Override
  public boolean setProperty(final String _key, final Object prop) {
    return target_.setProperty(_key, prop);
  }
}
