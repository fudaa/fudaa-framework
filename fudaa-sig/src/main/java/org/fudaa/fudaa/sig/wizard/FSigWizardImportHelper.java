/*
 * @creation 30 mars 2006
 * @modification $Date: 2007-01-19 13:14:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigGeomSrcDataUtils;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author fred deniger
 * @version $Id$
 */
public final class FSigWizardImportHelper {

  private FSigWizardImportHelper() {
  }

  public static class InfoPanel extends BuPanel {

    JLabel lbNbLines_;
    JLabel lbNbGones_;
    JLabel lbNbPts_;
    JLabel lbNbPtsTotal_;

    public InfoPanel() {
      setLayout(new BuHorizontalLayout(20, true, false));
      final BuGridLayout lay = new BuGridLayout(2, 5, 5, true, false);
      lay.setColumnXAlign(new float[]{0f, 0f});
      BuPanel line = new BuPanel();
      line.setLayout(lay);
      line.add(FSigGeomSrcDataUtils.buildLabel(FSigLib.getS("Nombre de lignes ouvertes:")));
      lbNbLines_ = FSigGeomSrcDataUtils.buildLabel("");
      line.add(lbNbLines_);
      line.add(FSigGeomSrcDataUtils.buildLabel(FSigLib.getS("Nombre de lignes ferm�es:")));
      lbNbGones_ = FSigGeomSrcDataUtils.buildLabel("");
      line.add(lbNbGones_);
      add(line);
      line = new BuPanel();
      line.setLayout(lay);
      line.add(FSigGeomSrcDataUtils.buildLabel(FSigLib.getS("Nombre de points:")));
      lbNbPts_ = FSigGeomSrcDataUtils.buildLabel("");
      line.add(lbNbPts_);
      line.add(FSigGeomSrcDataUtils.buildLabel(FSigLib.getS("Nombre de points total:")));
      lbNbPtsTotal_ = FSigGeomSrcDataUtils.buildLabel("");
      line.add(lbNbPtsTotal_);
      add(line);
    }

    public void setSrc(FSigGeomSrcData _data) {
      if (_data == null) {
        return;
      }
      lbNbLines_.setText(CtuluLibString.getString(_data.getNbPolylignes()));
      lbNbGones_.setText(CtuluLibString.getString(_data.getNbPolygones()));
      lbNbPts_.setText(CtuluLibString.getString(_data.getNbPoints()));
      lbNbPtsTotal_.setText(CtuluLibString.getString(_data.getNbPointsTotal()));
    }
  }

  /**
   * @return un panneau d'information sur le nombre d'objet
   */
  public JPanel getInfoPanel() {
    final BuPanel top = new BuPanel();
    return top;
  }

  public static class CalqueCellRender extends CtuluCellTextRenderer {

    private String nullValue;

    public void setNullValue(String nullValue) {
      this.nullValue = nullValue;
    }

    @Override
    protected void setValue(final Object _value) {
      if (_value == null && nullValue != null) {
        super.setText(nullValue);
      } else if (_value instanceof ZCalqueAffichageDonneesInterface) {
        super.setText(((ZCalqueAffichageDonneesInterface) _value).getTitle());
      } else {
        super.setValue(_value);
      }
    }
  }

  public static class CalqueModel extends AbstractListModel implements ComboBoxModel {

    final ZCalqueAffichageDonneesInterface[] calques_;
    String newItem_;
    Object selected_;

    /**
     * @param _calques
     */
    public CalqueModel(final ZCalqueAffichageDonneesInterface[] _calques, final String _newItem) {
      super();
      newItem_ = _newItem;
      calques_ = _calques;
      selected_ = newItem_;
    }

    @Override
    public Object getElementAt(final int _index) {
      if (_index == 0) {
        return newItem_;
      }
      if (!CtuluLibArray.isEmpty(calques_)) {
        return calques_[_index - 1];
      }
      return null;
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return 1 + (calques_ == null ? 0 : calques_.length);
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  public static class SpecCombo extends BuComboBox {

    @Override
    public void setEnabled(final boolean _b) {
      super.setEnabled(_b && getModel().getSize() > 0);
    }
  }

  public static class PanelItemListener implements ItemListener {

    final Container dest_;
    final BuCheckBox cb_;

    public boolean isOk() {
      return FSigWizardImportHelper.isOk(cb_);
    }

    /**
     * @param _dest
     * @param _cb
     */
    public PanelItemListener(final Container _dest, final BuCheckBox _cb) {
      super();
      dest_ = _dest;
      cb_ = _cb;
      cb_.addItemListener(this);
    }

    public void updateState() {
      final boolean ok = isOk();
      for (int i = dest_.getComponentCount() - 1; i >= 0; i--) {
        if (dest_.getComponent(i) != cb_) {
          dest_.getComponent(i).setEnabled(ok);
        }
      }
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      updateState();
    }
  }

  public static void setTitleBorder(final JPanel _pn, final String _s) {
    _pn.setBorder(BorderFactory.createTitledBorder(_s));
  }

  public static boolean isOk(final JCheckBox _cb) {
    return _cb != null && _cb.isEnabled() && _cb.isSelected();
  }

  public static String getFormatExtHelp() {
    return FSigLib.getS("Les formats des fichiers sont choisis en fonction des extensions. Il est possible de les modifier dans la derni�re colonne");
  }

  public static String getInsertOrderHelp() {
    return FSigLib.getS("L'ordre d'insertion des fichiers permet de d�finir les priorit�s pour les zones");
  }
}
