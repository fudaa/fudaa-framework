/*
 *  @creation     18 janv. 2006
 *  @modification $Date: 2008-01-11 12:20:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuComboBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;

/**
 * Un mod�le pour les attributs d'un groupe SIG.
 * 
 * @author Fred Deniger
 * @version $Id: FSigAttributeTableModel.java,v 1.10 2008-01-11 12:20:37 bmarchan Exp $
 */
public final class FSigAttributeTableModel extends CtuluListEditorModel {

  Map attributeNewName_ = new HashMap();

  @Override
  public boolean canAdd() {
    return true;
  }

  public Map getRenamedAttribute() {
    return new HashMap(attributeNewName_);
  }

  protected Object createAttribute(final int _i) {
    final Object o = v_.get(_i);
    if (o instanceof GISAttributeInterface) {
      return o;
    }
    final String s = ((String) o).trim();
    if (s.length() > 0) {
      return new GISAttributeDouble(s, false);
    }
    return null;
  }

  /**
   * @return les attributs non renomme
   */
  public GISAttributeInterface[] getAttributes() {
    final int nb = getRowCount();
    final List r = new ArrayList(nb);
    for (int i = 0; i < nb; i++) {
      final Object o = createAttribute(i);
      if (o != null) {
        r.add(o);
      }
    }
    return (GISAttributeInterface[]) r.toArray(new GISAttributeInterface[r.size()]);
  }

  /**
   *
   */
  public FSigAttributeTableModel() {
    super();
    buildProposedValue();

  }

  /**
   * @param _showNumber
   */
  public FSigAttributeTableModel(final boolean _showNumber) {
    super(_showNumber);
    buildProposedValue();
  }

  /**
   * @param _maxNumber
   */
  public FSigAttributeTableModel(final int _maxNumber) {
    super(_maxNumber);
    buildProposedValue();
  }

  /**
   * @param _l
   * @param _showNumber
   * @param _maxNbValue
   */
  public FSigAttributeTableModel(final List _l, final boolean _showNumber, final int _maxNbValue) {
    super(_l, _showNumber, _maxNbValue);
    buildProposedValue();
  }

  /**
   * @param _l
   * @param _showNumber
   */
  public FSigAttributeTableModel(final List _l, final boolean _showNumber) {
    super(_l, _showNumber);
    buildProposedValue();
  }

  /**
   * @param _o
   * @param _showNumber
   * @param _maxNbValue
   */
  public FSigAttributeTableModel(final Object[] _o, final boolean _showNumber, final int _maxNbValue) {
    super(_o, _showNumber, _maxNbValue);
    buildProposedValue();
  }

  /**
   * @param _o
   * @param _showNumber
   */
  public FSigAttributeTableModel(final Object[] _o, final boolean _showNumber) {
    super(_o, _showNumber);
    buildProposedValue();
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return String.class;
  }

  @Override
  public boolean canRemove() {
    return true;
  }

  @Override
  public void edit(final int _r) {}

  protected void buildProposedValue() {

    final List defaultAtt = GISAttributeConstants.getDefaults();
    defaultAtt.removeAll(super.v_);
    if (model_ != null) {
      model_.removeAllElements();
      final int nb = defaultAtt.size();
      for (int i = 0; i < nb; i++) {
        model_.addElement(defaultAtt.get(i));
      }
    }
  }

  DefaultComboBoxModel model_;

  public ComboBoxModel getProposedValueModel() {
    if (model_ == null) {
      model_ = new DefaultComboBoxModel();
    }
    buildProposedValue();
    return model_;
  }

  @Override
  public Object getValueAt(final int _i) {

    final Object o = super.getValueAt(_i);
    if (attributeNewName_.containsKey(o)) {
      return attributeNewName_.get(o);
    } else if (o instanceof GISAttributeInterface) {
      return ((GISAttributeInterface) o).getName();
    }
    return o.toString();
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    if (super.isShowNumber() && _columnIndex == 0) {
      return false;
    }
    final Object o = super.getValueAt(_rowIndex);
    if (o instanceof GISAttributeInterface) {
      return !GISAttributeConstants.isConstant((GISAttributeInterface) o);
    }
    return super.isCellEditable(_rowIndex, _columnIndex);
  }

  public CtuluListEditorPanel buildEditor() {
    final CtuluListEditorPanel ed = new CtuluListEditorPanel(this);
    final ComboBoxModel cbModel = getProposedValueModel();
    final BuComboBox cb = new BuComboBox();
    createRendererFor(cb);
    cb.setModel(cbModel);
    cb.setEditable(true);
    final DefaultCellEditor cellEd = new DefaultCellEditor(cb);
    ed.getTable().getColumnModel().getColumn(1).setCellEditor(cellEd);
    return ed;
  }

  static void createRendererFor(final BuComboBox _cb) {
    _cb.setRenderer(new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value instanceof GISAttributeInterface) {
          super.setValue(((GISAttributeInterface) _value).getName());
        } else {
          super.setValue(_value);
        }
      }

    });
  }

  public static boolean rename(final Map _attNewName) {
    boolean changed = false;
    if (_attNewName.size() > 0) {
      for (final Iterator it = _attNewName.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        final GISAttributeInterface attChanged = (GISAttributeInterface) e.getKey();
        final String oldName = attChanged.getName();
        final boolean add = attChanged.setName((String) e.getValue());
        if (add) {
          e.setValue(oldName);
        } else {
          it.remove();
        }
        changed |= add;
      }
    }
    return changed;

  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_value == null || _value.toString().trim().length() == 0) {
      return;
    }
    final Object o = super.getValueAt(_rowIndex);
    if (o instanceof GISAttributeInterface) {
      attributeNewName_.put(o, _value);
    } else {
      super.setValueAt(_value, _rowIndex, _columnIndex);
    }
  }

  @Override
  public void addElement(final Object _o) {
    super.addElement(_o);
    buildProposedValue();
  }

  @Override
  public void remove(final ListSelectionModel _m) {
    super.remove(_m);
    buildProposedValue();
  }

  public String getChanged() {
    final StringBuffer buf = new StringBuffer(100);
    final GISAttributeInterface[] att = getAttributes();
    if (att == null) {
      buf.append("No change");
    } else {
      for (int i = 0; i < att.length; i++) {
        buf.append(att[i].getName());
        if (attributeNewName_.containsKey(att[i])) {
          buf.append(" non chang� = ").append(attributeNewName_.get(att[i]));
        }
        buf.append('\n');
      }
    }
    return buf.toString();

  }

}
