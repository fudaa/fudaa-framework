/*
 *  @creation     1 juin 2005
 *  @modification $Date: 2007-06-11 12:53:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.fu.FuLog;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;

/**
 * @author Fred Deniger
 * @version $Id: FSigVariableModifResultSub.java,v 1.4 2007-06-11 12:53:36 deniger Exp $
 */
public class FSigVariableModifResultSub implements InterpolationResultsHolderI {

  final FSigVariableModifResult res_;
  final int[] idx_;

  /**
   * @param _res le resultat a modifier
   * @param _idx les indices reelles
   */
  public FSigVariableModifResultSub(final FSigVariableModifResult _res, final int[] _idx) {
    res_ = _res;
    idx_ = _idx;
  }

  @Override
  public int getNbPtResultSet() {
    return idxValues_.size();
  }

  @Override
  public double[] getValuesForPt(int _ptIdx) {
    return (double[]) idxValues_.get(_ptIdx);
  }

  TIntObjectHashMap idxValues_ = new TIntObjectHashMap();

  @Override
  public void setResult(int _idx, int _alreadSetIdx) {
    setResult(_idx, (double[]) idxValues_.get(_alreadSetIdx));
  }

  @Override
  public boolean isDone(int _idxPt) {
    if (_idxPt < 0 || _idxPt >= idx_.length) return false;
    final int realIdx = idx_[_idxPt];
    return res_.isDone(realIdx);
  }

  @Override
  public double[] getValueForVariable(CtuluVariable _idxVariable) {
    return null;
  }

  @Override
  public void setResult(final int _idx, final double[] _values) {
    final int realIdx = idx_[_idx];
    if (res_.isDone(realIdx)) {
      FuLog.warning("FTR: this index is already done");
    } else {
      idxValues_.put(_idx, CtuluLibArray.copy(_values));
      res_.setDone(realIdx, _values);
    }

  }
}
