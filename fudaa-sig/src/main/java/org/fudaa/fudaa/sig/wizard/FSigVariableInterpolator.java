/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2007-06-05 09:01:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISDataModelLignePointAdapter;
import org.fudaa.ctulu.gis.GISDataModelMultiAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationSupportGISAdapter;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfInterpolationTargetGridAdapter;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.FSigVarAttrMapperTableModel;
import org.fudaa.fudaa.sig.FSigVarAttrMapperTableModel.MapperResult;
import org.fudaa.fudaa.sig.FSigVarPolygoneModifierActivity;
import org.fudaa.fudaa.sig.FSigVariableModifResult;
import org.fudaa.fudaa.sig.FSigVariableModifResultSub;

/**
 * @author Fred Deniger
 * @version $Id: FSigVariableInterpolator.java,v 1.4 2007-06-05 09:01:16 deniger Exp $
 */
public class FSigVariableInterpolator implements CtuluActivity {

  private final DefaultListModel attributes_;
  private final CtuluCommandContainer cmd_;
  private CtuluActivity currentAct_;
  private FSigGeomSrcData data_;
  private FSigVarAttrMapperTableModel tableModel_;

  private final H2dVariableProviderInterface target_;

  boolean stop_;
  /**
   * True si on utilise l'interpolation.
   */
  boolean useInterpolation_ = true;
  /**
   * true si seuls les point a l'interieur de la zone d'interpolation doivent etre modifier.
   */
  boolean interpolationInZone_;
  /**
   * La distance max pour les point a modifier par la zone.
   */
  double maxDistanceIntZone_;
  /**
   * True si les polylignes doivent etre utilis�es pour l'interpolation.
   */
  boolean useLinesInInterpolation_ = true;

  boolean ignorePtExtern_;
  double maxDistanceToIgnore_;

  /**
   * True si on initialise par zones.
   */
  boolean useZone_ = true;

  /**
   * @param _target
   * @param _cmd
   */
  public FSigVariableInterpolator(final H2dVariableProviderInterface _target, final CtuluCommandContainer _cmd) {
    super();
    target_ = _target;
    cmd_ = _cmd;
    attributes_ = new DefaultListModel();
    attributes_.addListDataListener(new ListDataListener() {

      @Override
      public void contentsChanged(final ListDataEvent _e) {
        attributeChanged();
      }

      @Override
      public void intervalAdded(final ListDataEvent _e) {
        attributeChanged();
      }

      @Override
      public void intervalRemoved(final ListDataEvent _e) {
        attributeChanged();
      }
    });
  }

  private GISAttributeInterface[] getSrcAttributes() {
    final GISAttributeInterface[] r = new GISAttributeInterface[attributes_.getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = (GISAttributeInterface) attributes_.getElementAt(i);
    }
    return r;
  }

  protected void attributeChanged() {
    tableModel_ = null;
  }

  int getNbPoints() {
    return data_ == null ? 0 : data_.getNbPoints();
  }

  int getNbPolygones() {
    return data_ == null ? 0 : data_.getNbPolygones();
  }

  int getNbPolylignes() {
    return data_ == null ? 0 : data_.getNbPolylignes();
  }

  public FSigVarAttrMapperTableModel getAttModel() {
    if (tableModel_ == null) {
      tableModel_ = new FSigVarAttrMapperTableModel(getTarget().getVarToModify(), getSrcAttributes());
    }
    return tableModel_;
  }

  public final DefaultListModel getAttributes() {
    return attributes_;
  }

  public final CtuluCommandContainer getCmd() {
    return cmd_;
  }

  public final FSigGeomSrcData getData() {
    return data_;
  }

  public final H2dVariableProviderInterface getTarget() {
    return target_;
  }
  
  MapperResult userMappingDefined;

  public void process(final CtuluAnalyze _analyse, final FastBitSet _set, final ProgressionInterface _prog) {
    stop_ = false;
    final FSigVarAttrMapperTableModel.MapperResult res = getMapperResult();
    final boolean isElt = target_.isElementVar();
    final int nbObj = isElt ? target_.getGrid().getEltNb() : target_.getGrid().getPtsNb();
    final FastBitSet objectSet = _set == null ? new FastBitSet(nbObj) : _set;
    int nbPtZoneMod = 0;
    if (res.att_.length == 0) {
      _analyse.addWarn(FSigLib.getS("Les fichiers en entr�e ne comporte pas de donn�es, l'interpolation n'a pas �t� effectu�e"));
      return;
    }
    final FSigVariableModifResult resultat = new FSigVariableModifResult(res.att_.length, objectSet, target_);
    if (useZone_ && data_.getNbPolygones() > 0) {
      final GISDataModel[] from = data_.getPolygones();
      final GISDataModel[] src = new GISDataModel[from.length];

      for (int i = src.length - 1; i >= 0; i--) {
        src[i] = GISDataModelFilterAdapter.buildAdapter(from[i], res.att_);
      }
      final FSigVarPolygoneModifierActivity act = new FSigVarPolygoneModifierActivity();
      currentAct_ = act;
      act.process(resultat, new GISDataModelMultiAdapter(src), _prog, true);
      nbPtZoneMod = resultat.getNbPtDone();
      _analyse.addInfo(isElt ? FSigLib.getS("Nombre d'�l�ments modifi�s par les zones:")
          : FSigLib.getS("Nombre de noeuds modifi�s par les zones:")
          + CtuluLibString.getEspaceString(nbPtZoneMod));
      currentAct_ = null;
    }
    if (stop_) {
      return;
    }
    if (useInterpolation_) {
      int[] ptRestant = resultat.getUnsetIndex();
      // s'il reste des points � modifier
      if (ptRestant != null && ptRestant.length > 0) {
        // les sources de l'interpolation
        final List srcInterp = new ArrayList(data_.getNbPoints() + data_.getNbPolygones() + data_.getNbPolylignes());
        GISDataModel[] pts = data_.getPoints();
        if (pts != null) {
          final int nb = pts.length;
          for (int i = 0; i < nb; i++) {
            srcInterp.add(GISDataModelFilterAdapter.buildAdapter(pts[i], res.att_));
          }
        }
        if (useLinesInInterpolation_) {
          pts = data_.getPolygones();
          if (pts != null) {
            final int nb = pts.length;
            for (int i = 0; i < nb; i++) {
              GISDataModelLignePointAdapter.buildAdapters(srcInterp, pts[i], res.att_);
            }
          }
          pts = data_.getPolylignes();
          if (pts != null) {
            final int nb = pts.length;
            for (int i = 0; i < nb; i++) {
              GISDataModelLignePointAdapter.buildAdapters(srcInterp, pts[i], res.att_);
            }

          }
        }
        if (stop_) {
          return;
        }
        if (srcInterp.size() > 0) {
          final GISDataModel[] models = new GISDataModel[srcInterp.size()];
          srcInterp.toArray(models);
          // la reference pour l'interpolation
          final GISDataModelMultiAdapter srcAll = new GISDataModelMultiAdapter(models);
          // si interpolation dans la zone, on enleve les points qui ne sont pas dans la zone.
          if (interpolationInZone_) {
            final Envelope ring = GISLib.computeEnveloppe(models, _prog);
            if (ring != null) {
              final TIntArrayList ptRestFinal = new TIntArrayList(ptRestant.length);
              final int nb = ptRestant.length;
              final EfGridInterface grid = target_.getGrid();
              final Coordinate tmp = new Coordinate();
              for (int i = 0; i < nb; i++) {
                final int idx = ptRestant[i];
                if (isElt) {
                  tmp.x = grid.getCentreXElement(idx);
                  tmp.y = grid.getCentreYElement(idx);
                } else {
                  tmp.x = grid.getPtX(idx);
                  tmp.y = grid.getPtY(idx);
                }
                if ((ring.contains(tmp))
                    || (maxDistanceIntZone_ > 1E-10 && GISLib.getDistance(ring, tmp.x, tmp.y) < maxDistanceIntZone_)) {
                  ptRestFinal.add(idx);
                }
              }
              ptRestant = ptRestFinal.toNativeArray();

            }

          }
          final InterpolationSupportGISAdapter reference = new InterpolationSupportGISAdapter(srcAll);
          // Interpolation cible
          InterpolationParameters params = new InterpolationParameters(new CtuluPermanentList(GISLib
              .getAttributeFrom(srcAll)), new EfInterpolationTargetGridAdapter(target_.getGrid(), ptRestant, isElt),
              reference);
          params.setProg(_prog);
          params.setResults(new FSigVariableModifResultSub(resultat, ptRestant));

          // on construit la source optimisee
          final InterpolatorBilinear act = new InterpolatorBilinear(reference);
          if (ignorePtExtern_) {
            act.setDistance(maxDistanceToIgnore_);
          }
          currentAct_ = act;

          act.interpolate(params);
          _analyse.merge(params.getAnalyze());
          _analyse.addInfo(FSigLib.getS(isElt ? "Nombre d'�l�ments modifi�s par interpolation:"
              : "Nombre de noeuds modifi�s par interpolation:")
              + CtuluLibString.getEspaceString(resultat.getNbPtDone() - nbPtZoneMod));
          currentAct_ = null;

        }

      }
    }
    if (stop_ || _analyse.containsErrors()) {
      return;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    resultat.go(cmp, res.vars_, target_);
    if (cmd_ != null) {
      cmd_.addCmd(cmp.getSimplify());
    }

  }

  private MapperResult getMapperResult() {
    return userMappingDefined==null?getAttModel().getResult():userMappingDefined;
  }

  public final void setData(final FSigGeomSrcData _data) {
    data_ = _data;
  }

  @Override
  public void stop() {
    stop_ = true;
    if (currentAct_ != null) {
      currentAct_.stop();
    }
  }

  public boolean isIgnorePtExtern() {
    return ignorePtExtern_;
  }

  public void setIgnorePtExtern(final boolean _ignorePtExtern) {
    ignorePtExtern_ = _ignorePtExtern;
  }

  public boolean isInterpolationInZone() {
    return interpolationInZone_;
  }

  public void setInterpolationInZone(final boolean _interpolationInZone) {
    interpolationInZone_ = _interpolationInZone;
  }

  public double getMaxDistanceIntZone() {
    return maxDistanceIntZone_;
  }

  public void setMaxDistanceIntZone(final double _maxDistanceIntZone) {
    maxDistanceIntZone_ = _maxDistanceIntZone;
  }

  public double getMaxDistanceToIgnore() {
    return maxDistanceToIgnore_;
  }

  public void setMaxDistanceToIgnore(final double _maxDistanceToIgnore) {
    maxDistanceToIgnore_ = _maxDistanceToIgnore;
  }

  public boolean isUseInterpolation() {
    return useInterpolation_;
  }

  public void setUseInterpolation(final boolean _useInterpolation) {
    useInterpolation_ = _useInterpolation;
  }

  public boolean isUseLinesInInterpolation() {
    return useLinesInInterpolation_;
  }

  public void setUseLinesInInterpolation(final boolean _useLinesInInterpolation) {
    useLinesInInterpolation_ = _useLinesInInterpolation;
  }

  public boolean isUseZone() {
    return useZone_;
  }

  public void setUseZone(final boolean _useZone) {
    useZone_ = _useZone;
  }

  /**
   * @return the userMappingDefined
   */
  public MapperResult getUserMappingDefined() {
    return userMappingDefined;
  }

  /**
   * @param userMappingDefined the userMappingDefined to set
   */
  public void setUserMappingDefined(MapperResult userMappingDefined) {
    this.userMappingDefined = userMappingDefined;
  }

}
