/*
 *  @creation     18 mai 2005
 *  @modification $Date: 2006-09-19 15:10:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import gnu.trove.TObjectIntHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.GISAttributeBoolean;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInteger;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeString;

/**
 * @author Fred Deniger
 * @version $Id: FSigAttibuteTypeManager.java,v 1.4 2006-09-19 15:10:21 deniger Exp $
 */
public class FSigAttibuteTypeManager {

  final Map atts_ = new TreeMap();
  final TObjectIntHashMap attUsed_ = new TObjectIntHashMap();

  public GISAttributeInterface getAttribute(final String _name) {
    return (GISAttributeInterface) atts_.get(_name);
  }

  public boolean contains(final String _name) {
    return atts_.containsKey(_name);
  }

  public int getNbUsed(final GISAttributeInterface _att) {
    return attUsed_.get(_att);
  }

  public GISAttributeDouble addDoubleAttribute(final String _name) {
    return addDoubleAttribute(_name, false);
  }

  public void fillListWithAtt(final List _dest) {
    _dest.addAll(atts_.values());
  }

  public GISAttributeDouble addDoubleAttribute(final CtuluVariable _var, final boolean _atomic) {
    final GISAttributeDouble r = addDoubleAttribute(_var.getName(), _atomic);
    if (r != null) {
      r.setUserData(_var.getID());
    }
    return r;
  }

  public GISAttributeDouble addDoubleAttribute(final String _name, final boolean _atomic) {
    if (contains(_name)) { return null; }
    final GISAttributeDouble r = new GISAttributeDouble(_name, _atomic);
    atts_.put(_name, r);
    return r;
  }

  public GISAttributeInteger addIntegerAttribute(final String _name) {
    return addIntegerAttribute(_name, false);

  }

  public GISAttributeInteger addIntegerAttribute(final String _name, final boolean _atomic) {
    if (contains(_name)) { return null; }
    final GISAttributeInteger r = new GISAttributeInteger(_name, _atomic);
    atts_.put(_name, r);
    return r;
  }

  public GISAttributeBoolean addBooleanAttribute(final String _name, final boolean _atomic) {
    if (contains(_name)) { return null; }
    final GISAttributeBoolean r = new GISAttributeBoolean(_name, _atomic);
    atts_.put(_name, r);
    return r;
  }

  public GISAttributeString addTextAttribute(final String _name) {
    if (contains(_name)) { return null; }
    final GISAttributeString r = new GISAttributeString(_name);
    atts_.put(_name, r);
    return r;
  }

  /**
   * 
   */
  public FSigAttibuteTypeManager() {
    super();
    for (GISAttributeInterface att: GISAttributeConstants.getDefaults()) {
      atts_.put(att.getName(), att);
    }
  }

  /**
   * @param _att attribut utilisee par un groupe de calque SIG
   */
  public void setUsed(final GISAttributeInterface _att) {
    attUsed_.put(_att, getNbUsed(_att) + 1);
  }

  /**
   * @param _att attribut qui n'est plus utilisr par un groupe de calque SIG
   */
  public void setUnUsed(final GISAttributeInterface _att) {
    final int newUsed = getNbUsed(_att) - 1;
    if (newUsed <= 0) {
      attUsed_.remove(_att);
      atts_.remove(_att);
    } else {
      attUsed_.put(_att, newUsed);
    }
  }

}
