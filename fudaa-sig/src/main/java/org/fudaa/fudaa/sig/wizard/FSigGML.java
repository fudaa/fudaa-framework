package org.fudaa.fudaa.sig.wizard;

import org.fudaa.ctulu.gis.GISDataModelToFeatureCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.exporter.GMLMapDecoder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.store.ReTypingFeatureCollection;
import org.geotools.wfs.GML.Version;
import org.geotools.xsd.Configuration;
import org.geotools.xsd.Parser;
import org.opengis.feature.simple.SimpleFeatureType;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class FSigGML extends org.fudaa.fudaa.sig.wizard.GML {
  /**
   * Construct a GML utility class to work with the indicated version of GML.
   * <p>Note that when working with GML2 you need to supply additional information prior to use
   * (in order to indicate where for XSD file is located).
   *
   * @param version Version of GML to use
   */
  public FSigGML(Version version) {
    super(version);
  }

  public SimpleFeatureCollection decodeFeatureCollection(
    InputStream in, boolean computeFullFeatureType)
    throws IOException, SAXException, ParserConfigurationException {
    if (Version.GML2 == version
      || Version.WFS1_0 == version
      || Version.GML2 == version
      || Version.GML3 == version
      || Version.WFS1_0 == version
      || Version.WFS1_1 == version) {
      Configuration cfg = gmlConfiguration;
      Parser parser = new Parser(cfg);
      DynamicFeatureTypeCacheCustomizer customizer = null;
      if (computeFullFeatureType) {
        customizer = new DynamicFeatureTypeCacheCustomizer();
        parser.setContextCustomizer(customizer);
      }
      Object obj = parser.parse(in);
      if (obj instanceof Map) {
        return legacyLoad((Map) obj);
      }
      SimpleFeatureCollection collection = toFeatureCollection(obj);
      // have we figured out the schema feature by feature? If so, harmonize
      if (computeFullFeatureType && customizer.isDynamicTypeFound()) {
        SimpleFeatureType harmonizedType = getCompleteFeatureType(collection);
        collection = new ReTypingFeatureCollection(collection, harmonizedType);
      }

      return collection;
    }
    return null;
  }

  private SimpleFeatureCollection legacyLoad(Map obj) {
    GMLMapDecoder mapDecoder = new GMLMapDecoder(obj, false);
    final GISZoneCollectionGeometry collectionGeometry = mapDecoder.decode();
    GISDataModelToFeatureCollection toFeatureCollection = new GISDataModelToFeatureCollection();
    return toFeatureCollection.processGML(null, collectionGeometry);
  }
}
