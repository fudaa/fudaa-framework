package org.fudaa.fudaa.sig.exetools;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gis.GISZoneCollection;

/**
 * Un exporter d'un ou plusieurs layers vers un fichier de type donn�.
 * @author marchand@deltacad.fr
 */
public interface FSigFileExporterI {
  
  /** 
   * @return Le format de fichier associ� au writer 
   */
  public FileFormat getFileFormat();
  
  /**
   * Export vers le fichier d�sign� des collections, qui peuvent �tre avoir une
   * nature.
   * 
   * @param _cols Les collections
   * @param _impl L'impl�emntation
   * @param _file Le fichier � ecrire
   * @param _prog Le suivi de la progression
   * @return La synth�se. Peut comporter des erreurs lors de l'�criture.
   */
  CtuluIOOperationSynthese exportTo(GISZoneCollection[] _cols, final CtuluUI _impl, final File _file, final ProgressionInterface _prog);
}
