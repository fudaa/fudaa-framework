/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2008-04-01 17:11:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.sig.FSigGeomSrcData;

import java.util.*;

/**
 * Une classe contenant des objets GIS import�s depuis un ou plusieurs fichiers contenant des informations GIS. Cette classe est charg�e par
 * l'interface de chargement {@link FSigFileLoaderI}, qui peut �tre appel�e plusieurs fois de facon iterative.
 *
 * @author Fred Deniger
 * @version $Id: FSigFileLoadResult.java,v 1.1.6.2 2008-04-01 17:11:49 bmarchan Exp $
 */
public class FSigFileLoadResult {
  private boolean first_ = true;
  public Set<GISAttributeInterface> allAttribute_ = new HashSet<GISAttributeInterface>();
  public Set<GISAttributeInterface> attribute_ = new HashSet<GISAttributeInterface>();
  /**
   * La somme des points contenus dans tous les mod�les de points
   */
  public int nbPoint_;
  /**
   * La somme des points contenus dans tous les mod�les (points, polylignes, polygones, etc.)
   */
  public int nbPointTotal_;
  /**
   * La somme des polygones contenus dans tous les mod�les de polygones
   */
  public int nbPolygones_;
  /**
   * La somme des poylignes contenus dans tous les mod�les de polylignes
   */
  public int nbPolylignes_;
  /**
   * La liste des mod�les {@link GISDataModel} de polylignes
   */
  public List<GISDataModel> ligneModel_ = new ArrayList<GISDataModel>();
  /**
   * La liste des mod�les {@link GISDataModel} de points
   */
  public List<GISDataModel> pointModel_ = new ArrayList<GISDataModel>();
  /**
   * La liste des mod�les {@link GISDataModel} de polygones
   */
  public List<GISDataModel> polygoneModel_ = new ArrayList<>();

  public void addUsedAttributes(GISAttributeInterface[] _att) {
    addUsedAttributes(Arrays.asList(_att));
  }

  /**
   * La liste des mod�les {@link GISDataModel} de multipoints
   */
  public void addUsedAttributes(final List<GISAttributeInterface> _att) {
    if (_att == null) {
      return;
    }
    allAttribute_.addAll(_att);
    if (first_) {
      for (int i = 0; i < _att.size(); i++) {
        if (_att.get(i).getDataClass().equals(Double.class)) {
          attribute_.add(_att.get(i));
        }
      }
      first_ = false;
    } else {
      attribute_.retainAll(_att);
    }
  }

  public FSigGeomSrcData createData() {
    if (isEmpty()) {
      return null;
    }
    final FSigGeomSrcData res = new FSigGeomSrcData();
    res.setNbPoints(nbPoint_);
    res.setNbPointsTotal(nbPointTotal_);
    res.setNbPolygones(nbPolygones_);
    res.setNbPolylignes(nbPolylignes_);
    res.setPoints(pointModel_.toArray(new GISDataModel[pointModel_.size()]));
    res.setPolygones(polygoneModel_.toArray(new GISDataModel[polygoneModel_.size()]));
    res.setPolylignes(ligneModel_.toArray(new GISDataModel[ligneModel_.size()]));
    return res;
  }


  public FSigGeomSrcData createDataWithAtomic() {
    return createDataWithAtomic(true);
  }

  /**
   * permet de prendre en compte les attributs atomiques stock�s comme tableau dans les attributs globaux.
   */
  public FSigGeomSrcData createDataWithAtomic(boolean useNameToFindKnownAtt) {
    if (isEmpty()) {
      return null;
    }
    final FSigGeomSrcData res = createData();
    FSigLoadResultToAtomicProcess process = new FSigLoadResultToAtomicProcess(useNameToFindKnownAtt);
    return process.transform(res);
  }

  public GISAttributeInterface findOrCreateAttribute(final H2dVariableType _t, final boolean _isAtomic) {
    if (_t == H2dVariableType.BATHYMETRIE && _isAtomic) {
      allAttribute_.add(GISAttributeConstants.BATHY);
      return GISAttributeConstants.BATHY;
    }
    return findOrCreateAttribute(_t.getName(), Double.class, _isAtomic);
  }

  /**
   * @param _name le nom de l'attribute
   * @param _isAtomic true si valeur sur sommets
   * @return l'attribute cree ou retrouv�
   */
  public GISAttributeInterface findOrCreateAttribute(final String _name, final Class _data, final boolean _isAtomic) {
    GISAttributeInterface res = findAttributes(_name, _data, _isAtomic);
    if (res == null) {
      res = GISLib.createAttributeFrom(_name, _data, _isAtomic);
      if (res != null) {
        allAttribute_.add(res);
      }
    }
    return res;
  }

  private GISAttributeInterface findAttributes(final String _name, final Class _clazz, final boolean _isAtomic) {
    for (final Iterator<GISAttributeInterface> it = allAttribute_.iterator(); it.hasNext(); ) {
      final GISAttributeInterface att = (GISAttributeInterface) it.next();
      if (att.isAtomicValue() == _isAtomic && att.getName().equals(_name) && att.getDataClass().equals(_clazz)) {
        return att;
      }
    }
    return null;
  }

  public boolean isEmpty() {
    return pointModel_.size() == 0 && polygoneModel_.size() == 0 && ligneModel_.size() == 0;
  }
}
