/*
 * @creation 27 juil. 06
 * @modification $Date: 2007-03-30 15:38:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.ZModeleStatiqueImageRaster;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.fudaa.sig.FSigLib;

class FSigImageWizardTableModel extends AbstractTableModel {

  public static class Result {
    public Result() {}

    public Result(final Result _r) {
      if (_r != null) {
        error_ = CtuluLibArray.copy(_r.error_);
        ptImg_ = ZModeleStatiqueImageRaster.copy(_r.ptImg_);
        ptReel_ = ZModeleStatiqueImageRaster.copy(_r.ptReel_);
        transform_ = new AffineTransform(_r.transform_);
      }
    }
    public double[] error_;
    public Point2D.Double[] ptImg_;
    public Point2D.Double[] ptReel_;
    public AffineTransform transform_;

  }

  boolean isErrorComputed_;
  private final Result res_ = new Result();

  public String getError() {
    if (ptCalage_.getNombre() < 2) {
      return FSigLib.getS("Vous devez d�finir au moins 2 points diff�rents pour caler une image.");
    }
    if (res_.transform_ == null) {
      return FSigLib.getS("Vos points ne permettent pas de caler l'image (ils sont s�rement align�s).");
    }
    return null;

  }

  final ZModelePointEditable ptCalage_;

  public Result getResult() {
    if (!isErrorComputed_) {
      return null;
    }
    return new Result(res_);
  }

  public boolean isOk() {
    return isErrorComputed_;
  }

  void updateError() {
    final int nbPt = ptCalage_.getNombre();
    if (nbPt < 2) {
      isErrorComputed_ = false;
      return;
    }
    isErrorComputed_ = true;
    if (res_.ptReel_ == null || res_.ptReel_.length != nbPt) {
      res_.ptReel_ = new Point2D.Double[nbPt];
      res_.ptImg_ = new Point2D.Double[nbPt];
      for (int i = 0; i < nbPt; i++) {
        res_.ptReel_[i] = new Point2D.Double();
        res_.ptImg_[i] = new Point2D.Double();
      }
    }
    final GISZoneCollectionPoint pt = (GISZoneCollectionPoint) ptCalage_.getGeomData();
    final CtuluCollectionDouble x = (CtuluCollectionDouble) pt.getModel(0);
    final CtuluCollectionDouble y = (CtuluCollectionDouble) pt.getModel(1);
    for (int i = 0; i < nbPt; i++) {
      res_.ptImg_[i].setLocation((int) pt.getX(i), hImage_ - (int) pt.getY(i));
      res_.ptReel_[i].setLocation(x.getValue(i), y.getValue(i));
    }
    res_.transform_ = CtuluLibGeometrie.projection(res_.ptImg_, res_.ptReel_);
    isErrorComputed_ = res_.transform_ != null;
    if (isErrorComputed_) {
      res_.error_ = ZModeleStatiqueImageRaster.getErreurs(res_.ptImg_, res_.ptReel_, res_.transform_, res_.error_);
    }

  }

  int hImage_;

  public FSigImageWizardTableModel(final ZModelePointEditable _ptCalage, final int _hImage) {
    super();
    ptCalage_ = _ptCalage;
    hImage_ = _hImage;
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return _columnIndex < 2 ? Integer.class : String.class;
  }

  @Override
  public int getColumnCount() {
    return 3;
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) {
      return "X";
    }
    if (_column == 1) {
      return "Y";
    }
    return CtuluLib.getS("Erreurs");
  }

  @Override
  public int getRowCount() {
    return ptCalage_.getNombre();
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      return new Integer((int) ptCalage_.getX(_rowIndex));
    }
    if (_columnIndex == 1) {
      return new Integer((int) ptCalage_.getY(_rowIndex));
    }
    if (!isErrorComputed_ || res_.error_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    return CtuluLib.DEFAULT_NUMBER_FORMAT.format(res_.error_[_rowIndex]);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return false;
  }

  public int getHImage() {
    return hImage_;
  }

  public void setHImage(final int _image) {
    if (_image != hImage_) {
      hImage_ = _image;
      if (res_ != null && res_.ptImg_ != null) {
        res_.ptImg_ = null;
        res_.ptReel_ = null;
        res_.error_ = null;
        res_.transform_ = null;
        updateError();
      }
    }
  }

}