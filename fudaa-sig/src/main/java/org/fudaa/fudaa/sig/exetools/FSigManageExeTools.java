/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sig.exetools;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.fudaa.commun.exetools.FudaaManageExeTools;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * Une surchage du manager de Fudaa pour introduire des types fichiers.
 * @author marchand@deltacad.fr
 */
public class FSigManageExeTools extends FudaaManageExeTools {

  public FSigManageExeTools(FudaaCommonImplementation _impl) {
    FSigInpFileParam.setCtuluUI(_impl);
    FSigOutFileParam.setCtuluUI(_impl);
  }
  
  @Override
  public void initParamTypes() {
    super.initParamTypes();
    paramTypes.add(new FSigInpFileParam());
    paramTypes.add(new FSigOutFileParam());
  }
  
  public void setRootLayer(BGroupeCalque _cq) {
    FSigInpFileParam.setRoot(_cq);
    FSigOutFileParam.setRoot(_cq);
  }
  
  public void setCommandManager(CtuluCommandManager _mng) {
    FSigOutFileParam.setCommandManager(_mng);
  }
}
