/*
 * @creation     13 nov. 2008
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.mascaret.io.MascaretGEO2dFileFormat;

/**
 * Loader des fichiers mascaret (extension : georef).
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class FSigFileLoaderMascaret implements FSigFileLoaderI {
  
  private BuFileFilter filter_;

  public FSigFileLoaderMascaret(){
    filter_=MascaretGEO2dFileFormat.getInstance().createFileFilter();
  }
  
  private FSigFileLoaderMascaret(final BuFileFilter _filter){
    filter_=_filter;
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.fudaa.sig.wizard.FSigFileLoaderI#createNew()
   */
  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderMascaret(filter_);
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.sig.wizard.FSigFileLoaderI#getFileFilter()
   */
  @Override
  public BuFileFilter getFileFilter() {
    return filter_;
  }

  /* (non-Javadoc)
   * @see org.fudaa.fudaa.sig.wizard.FSigFileLoaderI#setInResult(org.fudaa.fudaa.sig.wizard.FSigFileLoadResult, java.io.File, java.lang.String, org.fudaa.ctulu.ProgressionInterface, org.fudaa.ctulu.CtuluAnalyze)
   */
  @Override
  public void setInResult(FSigFileLoadResult _r, File _f, String _origine, ProgressionInterface _prog, CtuluAnalyze _analyze) {
    final CtuluIOOperationSynthese op=MascaretGEO2dFileFormat.getInstance().read(_f, _prog);
    // En cas d'erreur, fin d'import.
    if(op.containsMessages()&&op.getAnalyze().containsFatalError()||op.getAnalyze().containsErrors()||op.getAnalyze().containsInfos()||op.getAnalyze().containsWarnings())
      _analyze.merge(op.getAnalyze());
    if(op.containsMessages()&&op.getAnalyze().containsFatalError())
      return;

    GISZoneCollectionLigneBrisee[] lines=(GISZoneCollectionLigneBrisee[])((Object[])op.getSource())[1];
    for (GISZoneCollectionLigneBrisee zl : lines) {
      _r.nbPointTotal_+=zl.getNumPoints();
      _r.nbPolylignes_+=zl.getNumGeometries();
      // Ajout de l'attribut ETAT_GEOM
      _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
      GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zl);
      adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _origine);
      _r.ligneModel_.add(adapter);
    }
  }

}
