/*
 *  @creation     25 mai 2005
 *  @modification $Date: 2007-05-04 14:00:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntIntHashMap;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: FSigFilterZoneActivity.java,v 1.5 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigFilterZoneActivity implements CtuluActivity {

  boolean stop_;
  EfGridInterface grid_;
  GISZoneCollectionLigneBrisee lines_;

  public final GISZoneCollectionLigneBrisee getLines() {
    return lines_;
  }

  @Override
  public void stop() {}

  /**
   * @param _src le maillage a parcourir
   * @param _lignes les listes utilisees pour la selection
   */
  public FSigFilterZoneActivity(final EfGridInterface _src, final GISZoneCollectionLigneBrisee _lignes) {
    super();
    grid_ = _src;
    lines_ = _lignes;
  }

  EfFilter[] filters_;

  /**
   * @param _progression la barre de progression
   * @param _busyPts les indices des points deja pris
   * @return la table indice du polygone -> liste (TintArraylist) dans l'ordre des indices des points contenus par le
   *         polygone
   */
  public TIntIntHashMap findPoint(final ProgressionInterface _progression, final BitSet _busyPts,
      final boolean _onElement, final boolean _strict) {
    stop_ = false;
    final int nbGeomInit = lines_.getNumGeometries();
    final TIntIntHashMap idxList = new TIntIntHashMap(nbGeomInit);
    // construction des filtres
    if (filters_ == null) {
      final List listFiltre = new ArrayList(nbGeomInit);
      for (int i = 0; i < nbGeomInit; i++) {
        if (lines_.getGeometry(i) instanceof LinearRing) {
          listFiltre.add(new FSigFilterZone(grid_, (LinearRing) lines_.getGeometry(i)));
        }
      }
      filters_ = new EfFilter[listFiltre.size()];
      listFiltre.toArray(filters_);
    }
    if (stop_) {
      return null;
    }
    // fin construction des filtres
    final ProgressionUpdater updater = new ProgressionUpdater(_progression);
    final int nbObject = _onElement ? grid_.getEltNb() : grid_.getPtsNb();
    updater.setValue(9, nbObject);
    final int nbFiltre = filters_.length;
    if (nbFiltre == 0) {
      return null;
    }
    updater.majProgessionStateOnly(FSigLib.getS("s�lection des points"));

    for (int i = 0; i < nbObject; i++) {
      if (stop_) {
        return null;
      }
      if (_busyPts == null || !_busyPts.get(i)) {
        boolean set = false;
        for (int k = nbFiltre - 1; k >= 0 && !set; k--) {
          if ((_onElement && filters_[k].isActivatedElt(i))
              || (!_onElement && filters_[k].isActivated(i))) {
            set = true;
            idxList.put(i, k);
            if (set && _busyPts != null) {
              _busyPts.set(i);
            }
          }
        }

      }
      updater.majAvancement();
    }
    return idxList;
  }
}
