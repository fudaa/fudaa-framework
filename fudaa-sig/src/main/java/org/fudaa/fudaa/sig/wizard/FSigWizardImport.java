/*
 * @creation 9 juin 2005
 * 
 * @modification $Date: 2008-02-22 16:27:43 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuButtonPanel;
import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuWizardTask;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeNameComparator;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISDataModelPointPolyAdapter;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.sig.FSigExportImportAttributesMapper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * Un wizard pour l'import de fichiers.
 *
 * @author Fred Deniger
 * @version $Id: FSigWizardImport.java,v 1.2.6.1 2008-02-22 16:27:43 bmarchan Exp $
 */
public class FSigWizardImport extends BuWizardTask {

  protected final ZCalqueEditionGroup dest_;

  protected FSigExportImportAttributesMapper mapper_;

  protected final GISPolygone[] previewZone_;

  protected final ZCalqueEditable selectedLayer_;

  protected FSigGeomSrcData data_;

  JDialog dialog_;

  protected FSigFileLoaderPanel fileLoader_;
  protected FSigWizardImportStepDestination pnDest_;

  protected final CtuluUI impl_;

  protected GISAttributeInterface[] src_;

  protected final DefaultListModel srcAtt_ = new DefaultListModel();

  /**
   * @param _dest le groupe de destination: ne doit pas etre le calque de base
   * @param _previewZone la zone a utiliser pour la pr�visualisation
   * @param _selectedLayer le calque en cours de selection: peut-etre null
   * @param _impl l'impl parent
   */
  public FSigWizardImport(final ZCalqueEditionGroup _dest, final GISPolygone[] _previewZone, final ZCalqueEditable _selectedLayer, final CtuluUI _impl) {
    dest_ = _dest;
    impl_ = _impl;
    previewZone_ = _previewZone;
    selectedLayer_ = _selectedLayer;
  }

  protected void rebuildAtt() {
    GISAttributeInterface[] newatt = new GISAttributeInterface[srcAtt_.size()];
    for (int i = newatt.length - 1; i >= 0; i--) {
      newatt[i] = (GISAttributeInterface) srcAtt_.get(i);
    }
    // pour l'instant on filtre le tout
    newatt = FSigExportImportAttributesMapper.filtreAttributes(newatt, new Class[]{String.class, Double.class}, null);
    if (newatt != null) {
      Arrays.sort(newatt, new GISAttributeNameComparator());
      // il n'y a pas de changement
      if (src_ != null && Arrays.equals(newatt, src_)) {
        return;
      }
    }
    src_ = newatt;
    mapper_ = null;

  }

  @Override
  public void doTask() {
    done_ = getStepCount() == 1 || !dest_.canAddNewLayer() || (pnDest_ != null && pnDest_.isImportLineValid());
    if (done_) {
      final CtuluTaskDelegate task = impl_.createTask(FudaaLib.getS("Importation"));
      task.start(new Runnable() {
        @Override
        public void run() {
          importData(task.getStateReceiver());
        }
      });
      super.doTask();
    } else {
      done_ = impl_.question(CtuluLib.getS("Attention"), FSigLib.getS("Aucune importation ne sera effectu�e.\nVoulez-vous fermer ce dialogue ?"));
    }
  }

  protected void importData(final ProgressionInterface _prog) {
    if (data_ == null) {
      return;
    }
    GISPoint translate = pnDest_.getTranslation();
    final Map r = mapper_ == null ? new HashMap() : mapper_.getDestReadMap();
    r.put(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ETAT_GEOM);
    final GISAttributeInterface[] dest = dest_.getAttributes();
    final GISAttributeInterface[] attChosen = new GISAttributeInterface[dest == null ? 0 : dest.length];
    boolean doPost = true;
    for (int i = attChosen.length - 1; i >= 0; i--) {
      // dest n'est pas null ici
      attChosen[i] = (GISAttributeInterface) r.get(dest[i]);
      if (dest[i] == GISAttributeConstants.BATHY && attChosen[i] != null) {
        doPost = false;
      }
    }
    data_.preload(attChosen, _prog);
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    if (!this.dest_.canAddNewLayer()) {
      GISZoneCollection geomData = selectedLayer_.getModelEditable().getGeomData();
      final GISDataModel[] pts = data_.getPoints();
      final int nb = pts.length;
      for (int i = 0; i < nb; i++) {
        final GISDataModel model = GISDataModelFilterAdapter.buildPointAdapter(pts[i].createTranslate(translate), attChosen);
        geomData.addAll(model, cmp, doPost);
      }
      return;
    }
    if (pnDest_.importPoint_.isOk() && data_.getNbPoints() > 0) {
      // creation du calque de dest.
      final GISZoneCollectionPoint pt = createPointLayer(cmp);
      final GISAttributeDouble attributeIsZ = pt.getAttributeIsZ();
      final GISDataModel[] pts = data_.getPoints();
      final int nb = pts.length;
      for (int i = 0; i < nb; i++) {
        final GISDataModel model = GISDataModelFilterAdapter.buildPointAdapter(pts[i].createTranslate(translate), attChosen);
        pt.addAll(model, cmp, false);
      }
    }
    if (pnDest_.importLignes_.isOk()) {
      GISZoneCollectionLigneBrisee line = null;
      if (data_.getNbPolygones() > 0 || data_.getNbPolylignes() > 0) {
        // creation du calque de dest.
        line = createLineLayer(cmp);
        GISDataModel[] lines = data_.getPolylignes();
        if (line != null) {
          final int nb = lines.length;
          for (int i = 0; i < nb; i++) {
            final GISDataModel model = GISDataModelFilterAdapter.buildLigneAdapter(lines[i].createTranslate(translate), attChosen);
            line.addAll(model, cmp, doPost);
          }
        }
        lines = data_.getPolygones();
        if (lines != null && line != null) {
          final int nb = lines.length;
          for (int i = 0; i < nb; i++) {
            final GISDataModel model = GISDataModelFilterAdapter.buildLigneAdapter(lines[i].createTranslate(translate), attChosen);
            line.addAll(model, cmp, doPost);
          }
        }
      }
      if (pnDest_.cbImportLignesFromPt_.isSelected() && data_.getNbPoints() > 0) {
        final GISDataModel[] pts = data_.getPoints();
        final int nb = pts.length;
        final GISDataModelPointPolyAdapter.PolyOption option = new GISDataModelPointPolyAdapter.PolyOption();

        if (pnDest_.cmbImpLinesFromPt_.getSelectedIndex() == 1) {
          option.setPoly();
        } else if (pnDest_.cmbImpLinesFromPt_.getSelectedIndex() == 2) {
          option.setCheck();
        }
        for (int i = 0; i < nb; i++) {
          if (pts[i].getNumGeometries() > 2) {
            if (line == null) {
              line = createLineLayer(cmp);
            }
            line.addAll(new GISDataModelPointPolyAdapter(pts[i].createTranslate(translate), attChosen, option), cmp, doPost);
          }
        }
      }
    }

    dest_.getCmdMng().addCmd(cmp.getSimplify());
  }

  private GISZoneCollectionPoint createPointLayer(final CtuluCommandComposite _cmp) {
    if (!dest_.canAddNewLayer()) {
      return null;
    }
    GISZoneCollectionPoint pt = null;
    if (pnDest_.cmbDestPoint_.getSelectedIndex() > 0) {
      pt = (GISZoneCollectionPoint) ((ZCalqueEditable) pnDest_.cmbDestPoint_.getSelectedItem()).getModelEditable().getGeomData();
    } else {
      pt = dest_.createPointZone();
      dest_.addPointLayerAct(pnDest_.tfNewPointLayer_.getText(), pt, _cmp);
    }
    return pt;
  }

  private GISZoneCollectionLigneBrisee createLineLayer(final CtuluCommandComposite _cmp) {
    if (!dest_.canAddNewLayer()) {
      return null;
    }
    GISZoneCollectionLigneBrisee line;
    if (pnDest_.cmbDestLignes_.getSelectedIndex() > 0) {
      line = (GISZoneCollectionLigneBrisee) ((ZCalqueEditable) pnDest_.cmbDestLignes_.getSelectedItem()).getModelEditable().getGeomData();
    } else {
      line = dest_.createLigneBriseeZone();
      dest_.addLigneBriseeLayerAct(pnDest_.tfNewLigneLayer_.getText(), line, _cmp);
    }
    return line;
  }

  public final JDialog getDialog() {
    return dialog_;
  }

  @Override
  public JComponent getStepComponent() {
    if (current_ == 0) {
      if (fileLoader_ == null) {
        srcAtt_.addListDataListener(new ListDataListener() {

          @Override
          public void contentsChanged(final ListDataEvent _e) {
            rebuildAtt();
          }

          @Override
          public void intervalAdded(final ListDataEvent _e) {
            rebuildAtt();
          }

          @Override
          public void intervalRemoved(final ListDataEvent _e) {
            rebuildAtt();
          }
        });
        fileLoader_ = new FSigFileLoaderPanel(srcAtt_, impl_, previewZone_);
        fileLoader_.setPreferredSize(new Dimension(450, 400));
        fileLoader_.setAttributeRequired(false);
        fileLoader_.getFileMng().getModel().addTableModelListener(new TableModelListener() {

          @Override
          public void tableChanged(final TableModelEvent _e) {
            data_ = null;
          }
        });
      }
      return fileLoader_;
    } else if (current_ == 1) {
      if (mapper_ == null) {
        if (dest_ != null && dest_.getAttributes() != null) {
          mapper_ = new FSigExportImportAttributesMapper(src_, dest_.getAttributes(), false);
        }
      }
      if (pnDest_ == null) {
        pnDest_ = new FSigWizardImportStepDestination(dest_, this.dest_.canAddNewLayer());
      }
      pnDest_.setMapper(mapper_);
      pnDest_.setSrc(data_);
      pnDest_.setSelectedLayer(selectedLayer_);
      pnDest_.update();
      return pnDest_;
    }
    return null;
  }

  @Override
  public int getStepCount() {
    return 2;
  }

  @Override
  public int getStepDisabledButtons() {
    return super.getStepDisabledButtons() | BuButtonPanel.DETRUIRE;
  }

  @Override
  public String getStepText() {
    if (super.current_ == 0) {
      final StringBuffer r = new StringBuffer();
      r.append(CtuluLibString.LINE_SEP).append(FSigWizardImportHelper.getFormatExtHelp());
      r.append(CtuluLibString.LINE_SEP).append(FSigWizardImportHelper.getInsertOrderHelp());
      return r.toString();
    }
    return FSigLib.getS("Pr�ciser les calques et les attributs � modifier");
  }

  @Override
  public String getStepTitle() {
    return super.current_ == 0 ? FSigLib.getS("S�lection des fichiers") : FSigLib.getS("D�finir les calques cibles");
  }

  @Override
  public String getTaskTitle() {
    return FudaaLib.getS("Importation");
  }

  @Override
  public void nextStep() {
    if (current_ == 0 && data_ == null) {
      if (dialog_ != null) {
        dialog_.setGlassPane(new BuGlassPaneStop());
      }
      fileLoader_.buildPreview(false);
      data_ = fileLoader_.loadAll(null, new CtuluAnalyze());
      if (dialog_ != null) {
        final Component c = dialog_.getGlassPane();
        c.setVisible(false);
        dialog_.remove(c);
      }
      if (data_ == null) {
        return;
      }
    }
    super.nextStep();
  }

  public void setStepDestination(FSigWizardImportStepDestination _pn) {
    pnDest_ = _pn;
  }

  public final void setDialog(final JDialog _dialog) {
    dialog_ = _dialog;
  }
}
