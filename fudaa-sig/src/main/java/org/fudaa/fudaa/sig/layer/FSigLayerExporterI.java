/*
 * @creation 26 f�vr. 07
 *
 * @modification $Date: 2007-06-14 12:01:22 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.layer;

import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gis.exporter.GISDataStoreZoneExporter;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.ctulu.gis.exporter.GISGMLZoneExporter;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.io.dunes.DunesGEOFileFormat;
import org.fudaa.dodico.rubar.io.RubarSEMFileFormat;
import org.fudaa.dodico.rubar.io.RubarSEMWriterGISAdapter;
import org.fudaa.dodico.rubar.io.RubarStCnFileFormat;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;
import org.fudaa.dodico.zesri.io.ZESRiFileFormat;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.fudaa.commun.FudaaLib;
import org.geotools.wfs.GML;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author fred deniger
 * @version $Id$
 */
public interface FSigLayerExporterI {
  /**
   * Export vers le fichier d�sign� des collections, qui peuvent �tre avoir une nature.
   *
   * @param _filter     Le filtre contenant les collections/
   * @param _impl       L'impl�mentation
   * @param _file       Le fichier � ecrire
   * @param _prog       Le suivi de la progression
   * @param translateXY si non null, une translation des donnees sera effectu�e
   * @return La synth�se. Peut comporter des erreurs lors de l'�criture.
   */
  CtuluIOOperationSynthese exportTo(FSigLayerFilterAbstract _filter, final CtuluUI _impl, final File _file, GISPoint translateXY,
                                    final ProgressionInterface _prog);

  /**
   * @return Le filtre de fichier associ� au writer
   */
  BuFileFilter getFileFilter();

  class ToGML extends DataStore {
    public ToGML() {
      super(GISFileFormat.GML_V2);
    }
  }

  class ToShape extends DataStore {
    public ToShape() {
      super(GISFileFormat.SHP);
    }
  }

  class DataStore implements FSigLayerExporterI {
    final GISFileFormat gisFileFormat;

    public DataStore(final GISFileFormat gisFileFormat) {
      this.gisFileFormat = gisFileFormat;
    }

    @Override
    public CtuluIOOperationSynthese exportTo(final FSigLayerFilterAbstract _filter, final CtuluUI _impl, final File _file, GISPoint translateXY,
                                             final ProgressionInterface _prog) {

      try {
        final boolean one = _filter.getNbZone() == 1;

        final Set nameUsed = new HashSet();
        final File dir = _file.getParentFile();
        final String name = _file.getName();
        int idx = 0;
        final List total = new ArrayList(_filter.pointCq_);
        total.addAll(_filter.polyCq_);
        total.addAll(_filter.mlptsCq_);
        final int nb = total.size();
        for (Object o : total) {
          final ZCalqueAffichageDonneesInterface oi = (ZCalqueAffichageDonneesInterface) o;
          String ni = one ? name : name + '-' + _filter.getName(oi);
          if (nameUsed.contains(ni)) {
            ni += '-' + CtuluLibString.getString(++idx);
          }
          if (nameUsed.contains(ni)) {
            ni = File.createTempFile(new File(dir, name + _filter.getName(oi)).getAbsolutePath(), "").getName();
          }
          nameUsed.add(ni);
          File file = new File(dir, ni);
          final String suffix = "." + gisFileFormat.getExtension();
          if (!file.getName().endsWith(suffix)) {
            file = new File(file.getAbsolutePath() + suffix);
          }
          GISDataModel collect = _filter.getCollect(oi);
          Class geomDataClass = ((ZModeleGeometry) oi.modeleDonnees()).getGeomData().getDataStoreClass();
          if (translateXY != null) {
            collect = collect.createTranslate(translateXY);
          }
          GISAttributeDouble attIsZ = _filter.getAttributeIsZ(oi);
          if (gisFileFormat.isGML()) {
            final GISGMLZoneExporter exporter = new GISGMLZoneExporter(GML.Version.GML3);
            exporter.setUseIdAsName(_filter.isExportWithId());
            OutputStream output = null;
            try {
              output = new FileOutputStream(file);
              exporter.processGML(_prog, collect, output, geomDataClass);
            } catch (Exception exception) {
              FuLog.error(exception);
            } finally {
              CtuluLibFile.close(output);
            }
          } else {
            final GISDataStoreZoneExporter exporter = new GISDataStoreZoneExporter();
            exporter.process(_prog, collect, attIsZ,
                GISExportDataStoreFactory.createDataStore(gisFileFormat.getDataStoreFactorySpi(), file.toURL(), collect.getEnvelopeInternal(), true), geomDataClass);
          }
        }
      } catch (final Exception _e) {
        _impl.error(FudaaLib.getS("Export") + ' ' + _file.getName(), _e.getMessage(), false);
        FuLog.warning(_e);
      }
      return null;
    }

    @Override
    public BuFileFilter getFileFilter() {
      return gisFileFormat.createFileFilter();
    }
  }

  class ToSinusX implements FSigLayerExporterI {
    @Override
    public CtuluIOOperationSynthese exportTo(final FSigLayerFilterAbstract _filter, final CtuluUI _impl, final File _f, GISPoint translateXY,
                                             final ProgressionInterface _prog) {
      File f = CtuluLibFile.appendExtensionIfNeeded(_f, "sx");
      GISDataModel[][] z = new GISDataModel[3][];
      int nb = _filter.pointCq_.size();
      z[0] = new GISDataModel[_filter.pointCq_.size()];
      for (int i = 0; i < nb; i++) {
        final GISDataModel collec = _filter.getCollect(_filter.pointCq_.get(i));
        z[0][i] = collec;
      }
      nb = _filter.polyCq_.size();
      z[1] = new GISDataModel[_filter.polyCq_.size()];
      for (int i = 0; i < nb; i++) {
        final GISDataModel collec = _filter.getCollect(_filter.polyCq_.get(i));
        z[1][i] = collec;
      }
      nb = _filter.mlptsCq_.size();
      z[2] = new GISDataModel[_filter.mlptsCq_.size()];
      for (int i = 0; i < nb; i++) {
        final GISDataModel collec = _filter.getCollect(_filter.mlptsCq_.get(i));
        z[2][i] = collec;
      }
      if (translateXY != null) {
        GISDataModel[][] zTranslated = new GISDataModel[z.length][];
        for (int i = 0; i < zTranslated.length; i++) {
          zTranslated[i] = new GISDataModel[z[i].length];
          for (int j = 0; j < zTranslated[i].length; j++) {
            zTranslated[i][j] = z[i][j].createTranslate(translateXY);
          }
        }
        z = zTranslated;
      }
      return SinusxFileFormat.getInstance().write(f, z, _prog);
    }

    @Override
    public BuFileFilter getFileFilter() {
      return SinusxFileFormat.getInstance().createFileFilter();
    }
  }

  public static class ToDunes implements FSigLayerExporterI {
    @Override
    public CtuluIOOperationSynthese exportTo(final FSigLayerFilterAbstract _filter, final CtuluUI _impl, final File _f, GISPoint translateXY,
                                             final ProgressionInterface _prog) {
      File f = CtuluLibFile.appendExtensionIfNeeded(_f, DunesGEOFileFormat.getInstance().getExtensions()[0]);

      List<GISDataModel> mdls = new ArrayList<GISDataModel>();
      for (int i = 0; i < _filter.pointCq_.size(); i++) {
        mdls.add(new GISDataModelPointToMultiPointAdapter(GISDataModelFilterAdapter.buildAdapter(_filter.getCollect(_filter.pointCq_.get(i)), null)));
      }
      for (int i = 0; i < _filter.mlptsCq_.size(); i++) {
        mdls.add(_filter.getCollect(_filter.mlptsCq_.get(i)));
      }
      for (int i = 0; i < _filter.polyCq_.size(); i++) {
        final GISDataModel collec = _filter.getCollect(_filter.polyCq_.get(i));
        mdls.add(collec);
      }

      return DunesGEOFileFormat.getInstance().write(f, mdls.toArray(new GISDataModel[0]), _prog);
    }

    @Override
    public BuFileFilter getFileFilter() {
      return DunesGEOFileFormat.getInstance().createFileFilter();
    }
  }

  /**
   * Export des g�om�tries vers les fichiers Rubar (ST, SEM, CN).
   *
   * @author Bertrand Marchand
   * @version $Id$
   */
  public static class ToRubar implements FSigLayerExporterI {
    @Override
    public CtuluIOOperationSynthese exportTo(final FSigLayerFilterAbstract _filter, final CtuluUI _impl, final File _f, GISPoint translateXY,
                                             final ProgressionInterface _prog) {
      CtuluIOOperationSynthese synt = new CtuluIOOperationSynthese();
      File f = CtuluLibFile.getSansExtension(_f);
      CtuluAnalyze ana = new CtuluAnalyze();
      synt.setAnalyze(ana);

      // Regroupement des mod�les suivant leur nature.
      List<GISDataModel> mdlprofs = new ArrayList<GISDataModel>();
      List<GISDataModel> mdlniv = new ArrayList<GISDataModel>();
      List<GISDataModel> mdlligdir = new ArrayList<GISDataModel>();
      List<GISDataModel> mdlsemis = new ArrayList<GISDataModel>();
      List<GISDataModel> mdlautres = new ArrayList<GISDataModel>();

      for (int i = 0; i < _filter.pointCq_.size(); i++) {
        mdlsemis.add(new GISDataModelPointToMultiPointAdapter(GISDataModelFilterAdapter.buildAdapter(_filter.getCollect(_filter.pointCq_.get(i)),
            null)));
      }
      for (int i = 0; i < _filter.mlptsCq_.size(); i++) {
        mdlsemis.add(GISDataModelFilterAdapter.buildAdapter(_filter.getCollect(_filter.mlptsCq_.get(i)), null));
      }
      for (int i = 0; i < _filter.polyCq_.size(); i++) {
        GISDataModel col = _filter.getCollect(_filter.polyCq_.get(i));
        if (col.getNumGeometries() == 0) {
          continue;
        }
        int idxAtt = col.getIndiceOf(GISAttributeConstants.NATURE);
        if (idxAtt == -1) {
          mdlautres.add(GISDataModelFilterAdapter.buildAdapter(_filter.getCollect(_filter.polyCq_.get(i)), new GISAttributeInterface[]{
              GISAttributeConstants.TITRE, GISAttributeConstants.COMMENTAIRE_HYDRO}));
        } else {
          GISDataModel mdl = GISDataModelFilterAdapter.buildAdapter(_filter.getCollect(_filter.polyCq_.get(i)), new GISAttributeInterface[]{
              GISAttributeConstants.TITRE, GISAttributeConstants.NATURE, GISAttributeConstants.COMMENTAIRE_HYDRO});
          if (GISAttributeConstants.ATT_NATURE_CN.equals(col.getValue(idxAtt, 0))) {
            mdlniv.add(mdl);
          } else if (GISAttributeConstants.ATT_NATURE_PF.equals(col.getValue(idxAtt, 0))) {
            mdlprofs.add(mdl);
          } else if (GISAttributeConstants.ATT_NATURE_LD.equals(col.getValue(idxAtt, 0))) {
            mdlligdir.add(mdl);
          } else {
            mdlautres.add(mdl);
          }
        }
      }
      if (translateXY != null) {
        mdlprofs = translate(mdlprofs, translateXY);
        mdlniv = translate(mdlniv, translateXY);
        mdlligdir = translate(mdlligdir, translateXY);
        mdlsemis = translate(mdlsemis, translateXY);
        mdlautres = translate(mdlautres, translateXY);
      }

      FileFormatUnique ff;

      // Les semis
      if (mdlsemis.size() > 0) {
        ff = new RubarSEMFileFormat();
        GISDataModel mdl = new GISDataModelMultiAdapter(mdlsemis.toArray(new GISDataModel[0]));
        ana = ff.write(CtuluLibFile.appendExtensionIfNeeded(f, ff.getExtensions()[0]), new RubarSEMWriterGISAdapter(mdl), _prog).getAnalyze();

        int nbsemis = mdl.getNumGeometries();
        ana.addInfo(DodicoLib.getS("{0} semis sur {1} ont �t� export�s", "" + nbsemis, "" + nbsemis), -1);

        synt.getAnalyze().merge(ana);
        if (synt.containsSevereError()) {
          return synt;
        }
      }

      // Les profils et lignes directrices
      boolean profsOrLigneDirectrice = mdlprofs.size() > 0 || mdlligdir.size() > 0;
      if (profsOrLigneDirectrice) {
        ff = RubarStCnFileFormat.getInstance();
        GISDataModel[] mdl = new GISDataModel[2];
        mdl[0] = mdlprofs.size() == 0 ? null : new GISDataModelMultiAdapter(mdlprofs.toArray(new GISDataModel[0]));
        mdl[1] = mdlligdir.size() == 0 ? null : new GISDataModelMultiAdapter(mdlligdir.toArray(new GISDataModel[0]));
        ana = ff.write(CtuluLibFile.appendExtensionIfNeeded(f, ff.getExtensions()[0]), mdl, _prog).getAnalyze();
        synt.getAnalyze().merge(ana);
        if (synt.containsSevereError()) {
          return synt;
        }
      }

      // Les courbes de niveau
      if (mdlniv.size() > 0) {
        ff = RubarStCnFileFormat.getInstance();
        GISDataModel[] mdl = new GISDataModel[2];
        mdl[0] = new GISDataModelMultiAdapter(mdlniv.toArray(new GISDataModel[0]));
        mdl[1] = null;
        ana = ff.write(CtuluLibFile.appendExtensionIfNeeded(f, ff.getExtensions()[1]), mdl, _prog).getAnalyze();
        synt.getAnalyze().merge(ana);
        if (synt.containsSevereError()) {
          return synt;
        }
      }

      // Les autres polylignes => dans un fichier _autres.st
      if (mdlautres.size() > 0) {
        ff = RubarStCnFileFormat.getInstance();
        GISDataModel[] mdl = new GISDataModel[2];
        mdl[0] = new GISDataModelMultiAdapter(mdlautres.toArray(new GISDataModel[0]));
        mdl[1] = null;
        String pathname = profsOrLigneDirectrice ? f.getPath() + "_" + CtuluLib.getS("autres") : f.getPath();
        ana = ff.write(CtuluLibFile.appendExtensionIfNeeded(new File(pathname), ff.getExtensions()[0]), mdl, _prog).getAnalyze();
        synt.getAnalyze().merge(ana);
        if (synt.containsSevereError()) {
          return synt;
        }
      }

      return synt;
    }

    private List<GISDataModel> translate(List<GISDataModel> in, GISPoint translateXY) {
      // Regroupement des mod�les suivant leur nature.
      List<GISDataModel> out = new ArrayList<GISDataModel>(in.size());
      for (GISDataModel gisDataModel : in) {
        out.add(gisDataModel.createTranslate(translateXY));
      }
      return out;
    }

    @Override
    public BuFileFilter getFileFilter() {
      return RubarStCnFileFormat.getInstance().createFileFilter();
    }
  }

  /**
   * Export to format ESRI Z
   *
   * @author Adrien Hadoux
   */
  public static class ToSZESRI implements FSigLayerExporterI {
    @Override
    public CtuluIOOperationSynthese exportTo(final FSigLayerFilterAbstract _filter, final CtuluUI _impl, final File _f, GISPoint translateXY,
                                             final ProgressionInterface _prog) {
      File f = CtuluLibFile.appendExtensionIfNeeded(_f, "_polyligne.gen");
      List<List<GISPoint>> listeProfils = new ArrayList<List<GISPoint>>();

      for (int i = 0; i < _filter.polyCq_.size(); i++) {
        final GISDataModel model = _filter.getCollect(_filter.polyCq_.get(i));

        for (int geom = 0; geom < model.getNumGeometries(); geom++) {
          List<GISPoint> profil = new ArrayList<GISPoint>();
          listeProfils.add(profil);
          Geometry geometry = model.getGeometry(geom);
          int indexAtt = 0;
          for (Coordinate c : geometry.getCoordinates()) {
            if (translateXY != null) {
              c.x += translateXY.getX();
              c.y += translateXY.getY();
            }
            GISPoint point = new GISPoint(c);
            if (model.getValue(indexAtt, geom) != null && model.getValue(indexAtt, geom) instanceof Double) {
              point.setZ(model.getDoubleValue(indexAtt, geom));
            }

            profil.add(point);
            indexAtt++;
          }
        }
      }

      return ZESRiFileFormat.getInstance().write(f, listeProfils, _prog);
    }

    @Override
    public BuFileFilter getFileFilter() {
      return SinusxFileFormat.getInstance().createFileFilter();
    }
  }
}
