/*
 *  @creation     3 juin 2005
 *  @modification $Date: 2008-03-26 16:46:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleGeometryDefault;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * Un mod�le de donn�es pour un calque de lignes bris�es. Ce mod�le ne peut contenir qu'une seule ligne, de type GIS.
 *
 * @see org.fudaa.ebli.calque.ZModeleLigneBrisee
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigLineSingleModel extends ZModeleGeometryDefault implements ZModeleLigneBrisee {

  LineString r_;
  boolean isClosed_;
//  final GISZoneCollectionLigneBrisee geom_ = new GISZoneCollectionLigneBrisee();

  public FSigLineSingleModel() {
    super(new GISZoneCollectionLigneBrisee());
    r_ = null;
  }

  @Override
  public GISZoneCollectionLigneBrisee getGeomData() {
    return (GISZoneCollectionLigneBrisee) geometries_;
  }

  /**
   * @param _r
   */
  public FSigLineSingleModel(final LineString _r) {
    super(new GISZoneCollectionLigneBrisee());
    setLine(_r);
  }

  public final void setLine(final LineString _r) {
    if (geometries_.getNumGeometries() > 0) {
      geometries_.removeGeometries(new int[]{0}, null);
    }
    r_ = _r;
    if (r_ != null) {
      geometries_.addCoordinateSequence(r_.getCoordinateSequence(), null, null);
      isClosed_ = r_.isClosed();
    }
  }

  @Override
  public boolean containsPolygone() {
    return isClosed_;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }

  @Override
  public int getNbPolyligne() {
    return isClosed_ ? 0 : 1;
  }

  @Override
  public int getNbPointForGeometry(final int _idxLigne) {
    return r_ == null ? 0 : r_.getNumPoints();
  }

  @Override
  public int getNbPolygone() {
    return isClosed_ ? 1 : 0;
  }

  @Override
  public boolean isGeometryFermee(final int _idxLigne) {
    return isClosed_;
  }

  @Override
  public void prepareExport() {
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
  }

  @Override
  public boolean isGeometryReliee(int geom) {
    return true;
  }

  @Override
  public boolean isGeometryVisible(int geom) {
    return true;
  }
}
