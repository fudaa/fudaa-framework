/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2007-01-19 13:14:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.sig.persistence.FSigLayerPointPersistence;

/**
 * @author Fred Deniger
 * @version $Id: FSigLayerPointEditable.java,v 1.1 2007-01-19 13:14:30 deniger Exp $
 */
public class FSigLayerPointEditable extends ZCalquePointEditable implements FSigLayerVariableDisplayAble {

  final private FSigLayerVariableDisplay varDisplayer = new FSigLayerVariableDisplay();
    
  public FSigLayerPointEditable() {
    super();
  }

  /**
   * @param _modele
   */
  public FSigLayerPointEditable(final ZModelePointEditable _modele, final FSigEditor _editor) {
    super(_modele, _editor);
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new FSigLayerPointPersistence();
  }

    @Override
    public void paintValueOfPoint(final Graphics2D _g, final GrPoint p, final  int indicePoint, final GrMorphisme _versEcran, final GrBoite _clipReel) {
       varDisplayer.paintValueOfPoint(getModelEditable().getGeomData(), _g, p, indicePoint, _versEcran, _clipReel);
    }
  @Override
  public boolean isShowValues() {
      return varDisplayer.isShowValues();
  }
   @Override
   public void showValues(boolean showValues) {
    varDisplayer.showValues(showValues,this);
   }
  
  /**
   * display variable value for Idx.
   * @param idX
   * @return 
   */
   public String getValueForIdx(final int idX) {       
          return varDisplayer.getValueForIdx(idX, getModelEditable().getGeomData());
  }

    @Override
    public ListModel getListModel() {
       return varDisplayer.getListModel();
    }

    @Override
    public ListSelectionModel getListSelectionModel() {
        return varDisplayer.getListSelectionModel();
    }
    
    @Override
    public FSigLayerVariableDisplay getDisplayer(){
        return varDisplayer;
    }
    
    @Override
    public void setForeground(Color fg) {
        super.setForeground(fg);
        getDisplayer().changeColor(fg);
    }

    @Override
    public void setBackground(Color fg) {
        super.setBackground(fg);
        getDisplayer().changeColorBackGround(fg);
    }

}
