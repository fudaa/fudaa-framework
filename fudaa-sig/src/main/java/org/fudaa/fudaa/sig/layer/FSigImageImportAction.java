/*
 * @creation 27 juil. 06
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.layer;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuWizardDialog;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.WindowConstants;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.calque.ZModeleImageRaster;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.wizard.FSigImageWizardTask;

/**
 * Une action "import d'image".
 *
 * @author fred deniger
 * @version $Id$
 */
public class FSigImageImportAction extends EbliActionSimple implements FSigImageWizardTask.CalqueFactory,
        ContainerListener {

  private boolean addEditGeo = true;

  public boolean isAddEditGeo() {
    return addEditGeo;
  }

  public void setAddEditGeo(boolean addEditGeo) {
    this.addEditGeo = addEditGeo;
  }

  private class EditAction extends EbliActionSimple {

    public EditAction() {
      super(FSigLib.getS("Editer le fond de plan"), BuResource.BU.getIcon("image"), "EDIT_CALAGE_IMAGE");
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final ZCalqueImageRaster img = (ZCalqueImageRaster) visu_.getArbreCalqueModel().getSelectedCalque();
      if (img.getModelImage().getGeomData().isGeomModifiable()) {
        final FSigImageWizardTask task = new FSigImageWizardTask(visu_.getEditor().getPanel().getArbreCalqueModel());
        task.setModelToModiy(img);
        afficheWizard(task);
      }
    }

    @Override
    public void updateStateBeforeShow() {
      final BCalque selectedCalque = visu_.getArbreCalqueModel().getSelectedCalque();
      boolean enable = selectedCalque instanceof ZCalqueImageRaster;
      if (enable) {
        enable = ((ZCalqueImageRaster) selectedCalque).getModelEditable().getGeomData().isGeomModifiable();
      }
      super.setEnabled(enable);
    }
  }

  public static BuIcon getCommonImage() {
    return BuResource.BU.getIcon("image");
  }

  public static String getCommonTitle() {
    return FSigLib.getS("Importer et g�or�f�rencer une image");
  }
  BuWizardDialog current_;
  BGroupeCalque dest_;
  EditAction editAction_;
  final FSigVisuPanel visu_;

  public FSigImageImportAction(final FSigVisuPanel _visu, final BGroupeCalque _dest) {
    super(getCommonTitle(), getCommonImage(), "IMPORT_CALAGE_IMAGE");
    visu_ = _visu;
    setDest(_dest);

  }

  private void initImgLayer(final ZCalqueImageRaster _res) {
    _res.setDestructible(true);
    if (editAction_ == null) {
      editAction_ = new EditAction();
    }
    if (isAddEditGeo()) {
      _res.setActions(new EbliActionInterface[]{visu_.getEditor().getEditAction()[0], editAction_});
    } else {
      _res.setActions(new EbliActionInterface[]{editAction_});
    }
  }

  void afficheWizard(final FSigImageWizardTask _task) {
    _task.setCqDest(visu_);
    current_ = new BuWizardDialog(CtuluLibSwing.getFrameAncestorHelper(visu_), _task);
    current_.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    _task.setParentRootPane(current_.getRootPane());
    current_.addWindowListener(new WindowListener() {
      @Override
      public void windowActivated(final WindowEvent _e) {
      }

      @Override
      public void windowClosed(final WindowEvent _e) {
        current_ = null;
      }

      @Override
      public void windowClosing(final WindowEvent _e) {
        current_ = null;
      }

      @Override
      public void windowDeactivated(final WindowEvent _e) {
      }

      @Override
      public void windowDeiconified(final WindowEvent _e) {
      }

      @Override
      public void windowIconified(final WindowEvent _e) {
      }

      @Override
      public void windowOpened(final WindowEvent _e) {
      }
    });

    current_.pack();
    current_.setModal(false);
    final Dimension dim = current_.getSize();
    dim.height = dim.height + 100;
    dim.width = dim.height + 100;
    current_.setSize(dim);
    current_.setLocationRelativeTo(visu_.getFrame());
    current_.setVisible(true);

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    importTo();
  }

  /**
   * Cette m�thode est appel�e a chaque fois q'une image est ajout�e dans le groupe de destination. Elle permet de cr�er une seule action d'�dition
   * pour plusieurs calques.
   */
  @Override
  public void componentAdded(final ContainerEvent _e) {
    final Object dest = _e.getChild();
    if (dest instanceof ZCalqueImageRaster) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("FSI: calque ajout� " + ((ZCalqueImageRaster) dest).getTitle());
      }
      initImgLayer((ZCalqueImageRaster) dest);
    }

  }

  @Override
  public void componentRemoved(final ContainerEvent _e) {
  }

  @Override
  public ZCalqueImageRaster createCalque(final ZModeleImageRaster _model) {
    // initImgLayer(res);
    final ZCalqueImageRaster res = new ZCalqueImageRaster(_model);
    res.setEditor(visu_.getEditor());
    return res;
  }

  public BGroupeCalque getDest() {
    return dest_;
  }

  public void importTo() {
    importTo(dest_);
  }

  public void importTo(final BGroupeCalque _dest) {
    if (current_ != null) {
      current_.setVisible(true);
      current_.toFront();
      return;
    }
    final FSigImageWizardTask task = new FSigImageWizardTask();
    task.setFactory(this);
    task.setDest(_dest);
    afficheWizard(task);

  }

  public final void setDest(final BGroupeCalque _dest) {
    if (dest_ != null) {
      dest_.removeContainerListener(this);
    }
    dest_ = _dest;
    if (dest_ != null) {
      dest_.addContainerListener(this);
    }
  }
}
