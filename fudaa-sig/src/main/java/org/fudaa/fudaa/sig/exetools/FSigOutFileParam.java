/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sig.exetools;

import java.awt.BorderLayout;
import java.io.File;
import java.util.Arrays;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionDetailedInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool.ParamI;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.wizard.FSigFileLoadResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderCsv;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderGIS;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderI;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderMascaret;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderRefluxRefondeResult;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderRubarCox;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderRubarInx;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderRubarSem;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderRubarSt;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderSerafin;
import org.fudaa.fudaa.sig.wizard.FSigFileLoaderSinusX;
import org.fudaa.fudaa.sig.wizard.FSigWizardFileModel;

/**
 * Un param�tre fichier de sortie construit a partir de calques s�lectionn�s.
 * @author marchand@deltacad.fr
 */
public class FSigOutFileParam extends ParamI {
  /** Les exporters pour ce type de parametre */
  private static FSigFileLoaderI[] loaders_={
    new FSigFileLoaderRubarSt(),
    new FSigFileLoaderGIS.GML(),
    new FSigFileLoaderGIS.Shape(),
    new FSigFileLoaderSinusX(),
    new FSigFileLoaderMascaret(),
    new FSigFileLoaderSerafin(),
    new FSigFileLoaderRubarCox(),
    new FSigFileLoaderRubarInx(),
    new FSigFileLoaderRubarSem(),
    new FSigFileLoaderRefluxRefondeResult(),
    new FSigFileLoaderCsv()
  };
  /** Les noms associ�es. Ne pas modifier ces noms, ils sont sauv�s dans les pr�f�rences */
  private static String[] loadNames_={
    "RubarSt",
    "GML",
    "Shape",
    "SinusX",
    "Mascaret2d",
    "Serafin",
    "RubarCox",
    "RubarInx",
    "RubarSem",
    "SOV",
    "CSV"
  };
  
  static {
    
  }

  /** Le root calque pour les fichiers de sortie. */
  private static BGroupeCalque root_;
  private static CtuluUI ui_;
  private static CtuluCommandManager mng_;
  private String value;
  private int numLoader;
  
  private ParamOutFileDefinitionPanel pnDef_=new ParamOutFileDefinitionPanel();

  @Override
  public FSigOutFileParam clone() {
    FSigOutFileParam o=new FSigOutFileParam();
    o.name=name;
    o.exeTool=exeTool;
    o.pnDef_=pnDef_;
    o.value=value;
    o.numLoader=numLoader;

    return o;
  }

  @Override
  public void setValue(String _val) {
    value=_val;
    for (int i=0; i < loadNames_.length; i++) {
      if (loadNames_[i].equalsIgnoreCase(value)) {
        numLoader=i;
        break;
      }
    }
  }

  @Override
  public String getValue() {
    return value;
  }

  /** Non utilis� */
  @Override
  public void setValSet(String _val) {
  }

  /** Non utilis� */
  @Override
  public String getValSet() {
    return null;
  }

  @Override
  public String getValParam() {
    // R�cup�ration de l'indice du param dans l'utilitaire
    int num=Arrays.asList(exeTool.getParams()).indexOf(this)+1;
    return "fout"+num+"."+loaders_[numLoader].getFileFilter().getFirstExt();
  }

  @Override
  public boolean postLaunch(ProgressionDetailedInterface _prog) {
    File fout=new File(exeTool.exeDirPath, getValParam());
    if (!fout.exists()) {
      _prog.appendDetailln(FSigLib.getS("Le fichier {0} n'existe pas", fout.getPath()));
      return false;
    }
    FSigFileLoadResult res=new FSigFileLoadResult();
    CtuluAnalyze ana=new CtuluAnalyze();
    
    // Sp�cial CSV/TXT : chargement d'options de lecture
    if (getValue().equals("CSV")) {
      Map options = FSigWizardFileModel.loadCsv(fout);
      if (options==null) return false;
      ((FSigFileLoaderCsv)loaders_[numLoader]).setOptions(options);
    }
    
    loaders_[numLoader].setInResult(res, fout, "ORIG", _prog, ana);
    if (ana.containsFatalError()) {
      _prog.appendDetailln(ana.getFatalError());
      return false;
    }
    
    // Chargement par la fenetre de destination
    ParamOutFilePostPanel pn=new ParamOutFilePostPanel(res.createData());
    if (pn.afficheModaleOk(ui_.getParentComponent(),FSigLib.getS("Destination des donn�es du fichier {0}",getValParam()))) {
      pn.doAtEnd(_prog, mng_);
    }
    
    
    return false;
  }
    
  public class ParamOutFilePostPanel extends CtuluDialogPanel {
    private FSigImportDestinationPanel pnDest_;
    
    public ParamOutFilePostPanel(FSigGeomSrcData _data) {
      setLayout(new BorderLayout(5, 0));
      pnDest_=new FSigImportDestinationPanel(root_, _data, null);
      add(pnDest_, BorderLayout.CENTER);
    }
    
    public void doAtEnd(ProgressionInterface _prog, CtuluCommandManager _mng) {
      pnDest_.doAtEnd(_prog, _mng);
    }
  }
  
  public class ParamOutFileDefinitionPanel extends CtuluDialogPanel {

    JComboBox coOutFileValue_;

    public ParamOutFileDefinitionPanel() {
      coOutFileValue_=new JComboBox(new String[]{""});
      for (FSigFileLoaderI loader : loaders_) {
        coOutFileValue_.addItem(loader.getFileFilter().getShortDescription());
      }
      setLayout(new BorderLayout(5, 0));
      add(new JLabel(FSigLib.getS("Format")), BorderLayout.WEST);
      add(coOutFileValue_, BorderLayout.CENTER);
    }

    @Override
    public boolean isDataValid() {
      if (coOutFileValue_.getSelectedIndex() == 0) {
        setErrorText(FSigLib.getS("Un format doit �tre choisi"));
        return false;
      }
      return true;
    }

    @Override
    public void setValue(Object _o) {
      FudaaExeTool.ParamI param=(FudaaExeTool.ParamI) _o;
      for (int i=0; i<loadNames_.length; i++) {
        if (loadNames_[i].equalsIgnoreCase(param.getValue()))
          coOutFileValue_.setSelectedIndex(i+1);
      }
//      coOutFileValue_.setSelectedItem(param.getValue());
    }

    @Override
    public FudaaExeTool.ParamI getValue() {
      FudaaExeTool.ParamI param=new FSigOutFileParam();
      param.setValue(loadNames_[coOutFileValue_.getSelectedIndex()-1]);
      return param;
    }
  }
  
  public static void setRoot(BGroupeCalque _root) {
    root_=_root;
  }

  public static void setCtuluUI(CtuluUI _ui) {
    ui_=_ui;
  }

  public static void setCommandManager(CtuluCommandManager _mng) {
    mng_=_mng;
  }
  
  @Override
  public String getDefinitionCellText() {
    String nm="";
    if (!"".equals(name))
      nm=FudaaLib.getS("Nom")+"="+name+" ; ";

    return nm+getLabel() + "/" + FSigLib.getS("Type={0}", value);
  }

  /** Not used */
  @Override
  public String getSetCellText() {
    return null;
  }

  /** Not used */
  @Override
  public TableCellEditor getSetCellEditor() {
    return null;
  }

  @Override
  public boolean prepareLaunch(ProgressionDetailedInterface _prog) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public String getType() {
    return "OUTFILE";
  }

  @Override
  public boolean mustBeSet() {
    return false;
  }

  @Override
  public String getLabel() {
    return FSigLib.getS("Fichier de sortie");
  }

  @Override
  public CtuluDialogPanel getDefinitionPanel() {
    return pnDef_;
  }
}
