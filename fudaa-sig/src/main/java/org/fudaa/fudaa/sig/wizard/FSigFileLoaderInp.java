/*
 * @creation 7 juin 2005
 * 
 * @modification $Date: 2007-05-22 14:20:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementProperty;
import org.fudaa.dodico.h2d.reflux.H2dRefluxElementPropertyMngAbstract;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.dodico.reflux.io.INPInterface;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderInp.java,v 1.4 2007-05-22 14:20:38 deniger Exp $
 */
public class FSigFileLoaderInp implements FSigFileLoaderI {

  GISPoint[] pt_;

  private class INPAdapter implements GISDataModel {

    final GISAttributeInterface[] att_;
    double xOffset;
    double yOffset;

    @Override
    public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
    }

    INPAdapter(final GISAttributeInterface[] _att) {
      att_ = _att;
    }

    @Override
    public GISDataModel createTranslate(GISPoint xyToAdd) {
      if (xyToAdd == null) {
        return this;
      }
      INPAdapter res = new INPAdapter(att_);
      res.xOffset = xyToAdd.getX();
      res.yOffset = xyToAdd.getY();
      return res;
    }

    @Override
    public GISAttributeInterface getAttribute(final int _idxAtt) {
      return att_[_idxAtt];
    }

    @Override
    public Envelope getEnvelopeInternal() {
      Envelope envelope = inp_.getGrid().getEnvelope(null);
      envelope.translate(xOffset, yOffset);
      return envelope;
    }

    @Override
    public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
      final H2dRefluxElementProperty prop = mng_.getEltProp((H2dVariableType) mng_.getVarList().get(_idxAtt));
      final H2dBcType type = prop.getTypeForEltIdx(_idxGeom);
      if (type == H2dBcType.PERMANENT) {
        return prop.getPermanentValueFor(_idxGeom);
      } else if (type == H2dBcType.TRANSITOIRE) {
        final EvolutionReguliereAbstract evol = prop.getTransitoireEvol(_idxGeom);
        // on prend la premiere valeur: a voir !
        if (evol.getNbValues() > 0) {
          return evol.getY(0);
        }
      }
      return 0;
    }

    @Override
    public Geometry getGeometry(final int _idxGeom) {
      if (pt_ == null) {
        final EfGridInterface g = inp_.getGrid();
        pt_ = new GISPoint[g.getEltNb()];
        final Coordinate c = new Coordinate();
        for (int i = pt_.length - 1; i >= 0; i--) {
          g.getCentreElement(i, c);
          c.x = c.x + xOffset;
          c.y = c.y + yOffset;
          pt_[i] = new GISPoint(c);
        }
      }
      return pt_[_idxGeom];
    }

    @Override
    public int getIndiceOf(final GISAttributeInterface _att) {
      return CtuluLibArray.findObjectEgalEgal(att_, _att);
    }

    @Override
    public int getNbAttributes() {
      return att_.length;
    }

    @Override
    public int getNumGeometries() {
      return inp_.getGrid().getEltNb();
    }

    @Override
    public Object getValue(final int _idxAtt, final int _idxGeom) {
      return CtuluLib.getDouble(getDoubleValue(_idxAtt, _idxGeom));
    }

  }

  final BuFileFilter ft_;

  INPInterface inp_;
  H2dRefluxElementPropertyMngAbstract mng_;

  /**
   * @param _ft
   */
  private FSigFileLoaderInp(final BuFileFilter _ft) {
    super();
    ft_ = _ft;
  }

  /**
   * @param _ft
   */
  public FSigFileLoaderInp() {
    this(INPFileFormat.getInstance().createFileFilter());
  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderInp(ft_);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    if (inp_ == null) {
      final CtuluIOOperationSynthese op = INPFileFormat.getInstance().getLastINPVersionImpl().read(_f, _prog);
      if (op.containsMessages() && op.getAnalyze().containsErrors()) {
        _analyze.merge(op.getAnalyze());
        return;
      }
      inp_ = (INPInterface) op.getSource();
      inp_.getGrid().computeBord(_prog, _analyze);
      mng_ = H2dRefluxElementPropertyMngAbstract.createElementPropMng(inp_.getTypeProjet(), inp_.getPropElementaires(), inp_.getGrid().getEltNb(),
          _analyze);
      if (_analyze.containsFatalError()) {
        return;
      }
    }
    if (inp_ == null || mng_ == null) {
      return;
    }
    final GISAttributeInterface[] att = new GISAttributeInterface[mng_.getVarList().size()];
    for (int i = 0; i < att.length; i++) {
      att[i] = _r.findOrCreateAttribute(((H2dVariableType) mng_.getVarList().get(i)), true);

    }
    _r.nbPoint_ += inp_.getGrid().getEltNb();
    _r.nbPointTotal_ += inp_.getGrid().getEltNb();
    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(new INPAdapter(att));
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    // 
    _r.pointModel_.add(adapter);
    _r.addUsedAttributes(att);
  }
}
