/*
 *  @creation     25 mai 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuButtonPanel;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.bu.BuWizardDialog;
import com.memoire.bu.BuWizardTask;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluUIAbstract;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeNameComparator;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.FSigLoaderPreviewer;
import org.fudaa.fudaa.sig.FSigVarAttrMapperTableModel;

/**
 * Un wizard pour la modification d'une variable.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigWizardVariableModifier extends BuWizardTask implements TreeSelectionListener {

  BuTextField ftInterInRing_;
  BuTextField ftInterFromProject_;
  BuCheckBox cbInterp_;
  BuCheckBox cbInterpLines_;
  BuCheckBox cbInterInRing_;
  BuCheckBox cbModifySelected_;
  BuCheckBox cbUseFiles_;
  BuCheckBox cbInterIgnorePt_;
  BuCheckBox cbUseLayer_;
  BuCheckBox cbZone_;
  BuWizardDialog dialog_;
  /**
   * Le groupe contenant les calques de polylignes.
   */
  final ZCalqueEditionGroup gisLayer_;
  final CtuluUI impl_;
  FSigVariableInterpolator interpolator_;
  BuLabel lbCibleError_;
  JLabel lbLayersError_;
  BArbreCalqueModel model_;
  GISAttributeInterface[] nullAtt_ = new GISAttributeInterface[0];
  JPanel pnCible_;
  FSigFileLoaderPanel pnFiles_;
  JPanel pnInterInfo_;
  BuPanel pnInterMethode_;
  JPanel pnInterp_;
  JPanel pnLayers_;
  JPanel pnSelectType_;
  DefaultListModel selectedLayer_;
  boolean selectedLayerEmpty_;
  FastBitSet selectedPt_;
  JTable table_;

  /**
   * @param _layer le groupe contenant les calques a utiliser pour modifier la cible
   * @param _target la cible.
   * @param _impl l'impl parente
   * @param _cmd le receveur de commande
   * @param _selectedLay le calque selectionne
   */
  public FSigWizardVariableModifier(final ZCalqueEditionGroup _layer, final H2dVariableProviderInterface _target,
          final CtuluUI _impl, final CtuluCommandContainer _cmd, final BCalque _selectedLay) {
    super();
    interpolator_ = new FSigVariableInterpolator(_target, _cmd);
    gisLayer_ = _layer;
    impl_ = _impl;
    model_ = new BArbreCalqueModel(_layer, false);
    buildBitset(_selectedLay);
  }

  protected InterpolationPanel buildInterpolationSrcPanel() {

    cbInterIgnorePt_ = new BuCheckBox(FSigLib.getS("Donn�es sources: ignorer les points ext�rieurs au projet"));
    cbInterIgnorePt_.setToolTipText(FSigLib.getS("Si activ�, les points situ�s � l'ext�rieur de l'enveloppe du projet seront ignor�s"));
    final BuLabel lb = new BuLabel(FSigLib.getS("Distance maximale:"));
    lb
            .setToolTipText(createHtmlString("Permet de pr�ciser la distance maximale<br>entre l'enveloppe du projet  et les points sources � utiliser"));

    ftInterFromProject_ = BuTextField.createDoubleField();
    ftInterFromProject_.setColumns(8);
    ftInterFromProject_.setValue(CtuluLib.getDouble(1E-3));
    return new InterpolationPanel(cbInterIgnorePt_, lb, ftInterFromProject_);
  }

  /**
   * @author fred deniger
   * @version $Id$
   */
  static final class VariableCellTextRenderer extends CtuluCellTextRenderer {

    @Override
    protected void setValue(Object _value) {
      if (_value instanceof GISAttributeInterface) {
        setText(((GISAttributeInterface) _value).getLongName());
      } else {
        super.setValue(_value);
      }
    }
  }

  protected class InterpolationPanel extends BuPanel {

    final BuLabel lb_;
    final BuCheckBox cb_;
    final BuTextField tf_;

    /**
     * @param _cb
     * @param _lb
     */
    public InterpolationPanel(final BuCheckBox _cb, final BuLabel _lb, final BuTextField _tf) {
      super(new BuVerticalLayout());
      cb_ = _cb;
      lb_ = _lb;
      tf_ = _tf;
      add(cb_);
      final BuPanel pn = new BuPanel(new BuGridLayout(2));
      pn.add(lb_);
      if (tf_ != null) {
        pn.add(tf_);
      }
      pn.setBorder(BuBorders.EMPTY2500);
      add(pn);
      cb_.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(final ItemEvent _e) {
          updateIsEnable();
        }
      });
    }

    protected void updateIsEnable() {
      final boolean enable = cbInterp_.isEnabled() && cbInterp_.isSelected() && cb_.isEnabled() && cb_.isSelected();
      if (tf_ != null) {
        tf_.setEnabled(enable);
      }
      lb_.setEnabled(enable);
    }

    @Override
    public void setEnabled(final boolean _enabled) {
      if (_enabled) {
        cb_.setEnabled(true);
        updateIsEnable();
      } else {
        lb_.setEnabled(_enabled);
        cb_.setEnabled(_enabled);
        if (tf_ != null) {
          tf_.setEnabled(_enabled);
        }
      }
    }
  }

  protected InterpolationPanel buildInterpolationInZonePanel() {

    cbInterInRing_ = new BuCheckBox(FSigLib
            .getS("Ne modifier que les points situ�s � l'int�rieur de l'enveloppe des donn�es sources"));
    ftInterInRing_ = BuTextField.createDoubleField();
    ftInterInRing_.setValue(CtuluLib.getDouble(1E-3));
    ftInterInRing_.setColumns(8);
    ftInterInRing_.setEnabled(cbInterInRing_.isSelected());
    ftInterInRing_.setToolTipText(FSigLib.getS("Entrer la distance en m�tres"));
    final BuLabel lb = new BuLabel(FSigLib.getS("Distance maximale:"));
    lb.setToolTipText(createHtmlString("Permet de pr�ciser la distance maximale<br>"
            + "entre l'enveloppe et les points � modifier par l'interpolation"));
    return new InterpolationPanel(cbInterInRing_, lb, ftInterInRing_);

  }

  private String createHtmlString(final String _s) {
    return "<html><body>" + FSigLib.getS(_s) + "</body></html>";
  }

  private String createHtmlString(final String _s, final String _s2) {
    return "<html><body>" + FSigLib.getS(_s) + "<br>" + FSigLib.getS(_s2) + "</body></html>";
  }

  private void buildBitset(final BCalque _selectedLay) {
    if (_selectedLay != null) {
      final ZCalqueAffichageDonneesInterface lay = (ZCalqueAffichageDonneesInterface) _selectedLay;
      int nbObj = -1;
      if (!lay.isSelectionEmpty()) {
        nbObj = lay.modeleDonnees().getNombre();
        final CtuluListSelection selection = new CtuluListSelection(lay.getLayerSelection());
        selection.inverse(nbObj);
        selectedPt_ = selection.cloneBitSet();
      }
    }
  }

  private void removeAllListener() {
    model_.getRootCalque().removeContainerListener(model_);
    model_.removeTreeSelectionListener(this);
  }

  protected void buildLayerPanel() {
    if (pnLayers_ == null) {
      pnLayers_ = new BuPanel();
      lbLayersError_ = new BuLabel();
      lbLayersError_.setForeground(Color.RED);
      lbLayersError_.setText("<html><body><br></body></html>");
      pnLayers_.setLayout(new BuBorderLayout());
      pnLayers_.add(lbLayersError_, BuBorderLayout.NORTH);
      final BArbreCalque arbre = new BArbreCalque(model_);
      arbre.setEditable(false);
      arbre.addTreeSelectionListener(this);
      BuScrollPane p = new BuScrollPane(arbre);
      p.setPreferredHeight(110);
      p.setPreferredWidth(90);
      BuPanel se = new BuPanel(new BuBorderLayout());
      se.setBorder(BorderFactory.createTitledBorder(FSigLib.getS("S�lectionner les calques")));
      se.add(p);
      pnLayers_.add(se, BuBorderLayout.CENTER);
      // le panneau est
      final BuPanel east = new BuPanel(new BuBorderLayout());
      final BuList lays = new BuList();
      selectedLayer_ = new DefaultListModel();
      lays.setModel(selectedLayer_);
      se = new BuPanel(new BuBorderLayout());
      se.setBorder(BorderFactory.createTitledBorder(FSigLib.getS("Calques s�lectionn�s:")));
      p = new BuScrollPane(lays);
      p.setPreferredWidth(100);
      se.add(p, BuBorderLayout.CENTER);
      east.add(se, BuBorderLayout.CENTER);
      se = new BuPanel(new BuBorderLayout());
      final BuList l = new BuList();
      se.setBorder(BorderFactory.createTitledBorder(FSigLib.getS("Attributs disponibles:")));
      l.setCellRenderer(new VariableCellTextRenderer());
      l.setModel(interpolator_.getAttributes());
      p = new BuScrollPane(l);
      se.add(p, BuBorderLayout.CENTER);
      east.add(se, BuBorderLayout.SOUTH);
      p.setPreferredHeight(75);
      p.setPreferredWidth(100);
      final BuPanel pnBut = new BuPanel();
      pnBut.setLayout(new BuButtonLayout(0, SwingConstants.RIGHT));
      final BuButton btPrev = new BuButton(FSigLib.getS("Pr�visualiser"));
      btPrev.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          buildPreview();
        }
      });
      pnBut.add(btPrev);
      pnLayers_.add(pnBut, BuBorderLayout.SOUTH);
      pnLayers_.add(east, BuBorderLayout.EAST);
      pnLayers_.doLayout();
      filterSelectedLayer();
    }
  }

  protected void buildPnCible() {
    if (pnCible_ == null) {
      pnCible_ = new BuPanel(new BuVerticalLayout());
      lbCibleError_ = new BuLabel();
      table_ = new CtuluTable();
      lbCibleError_.setForeground(Color.RED);

      cbModifySelected_ = new BuCheckBox(FSigLib.getS("Modifier les objets s�lectionn�es uniquement"));
      cbModifySelected_.setEnabled(selectedPt_ != null);
      pnCible_.add(lbCibleError_);
      pnCible_.add(cbModifySelected_);
      pnCible_.add(new BuScrollPane(table_));
      updateCibleState();
    }
    modifyTable();
  }

  protected void buildPnFiles() {
    if (pnFiles_ == null) {
      pnFiles_ = new FSigFileLoaderPanel(interpolator_.getAttributes(), impl_, interpolator_.getTarget().getGrid()
              .getExtRings());
    }
  }
  BuLabel warn_;

  protected void buildPnInterpol() {
    if (pnInterp_ == null) {
      pnInterp_ = new BuPanel();
//      pnInterInfo_ = interpolator_.getData().getInfoPanel();
      pnInterInfo_ = new FSigWizardImportHelper.InfoPanel();
      ((FSigWizardImportHelper.InfoPanel) pnInterInfo_).setSrc(interpolator_.getData());
      final BuPanel bottom = new BuPanel();
      bottom.setLayout(new BuVerticalLayout());
      cbZone_ = new BuCheckBox(FSigLib.getS("Initialiser � partir des zones"));
      cbZone_.setToolTipText(createHtmlString("Si activ�, tous les points appartenant � une zone (ligne ferm�e) "
              + "seront intialis�s avec les valeurs de cette zone", "Cette action est prioritaire sur l'interpolation"));
      cbInterp_ = new BuCheckBox(FSigLib.getS("Interpoler � partir des points"));
      cbInterp_.setToolTipText(createHtmlString(
              "Si activ�, les valeurs seront interpol�es � partir des donn�es g�ographiques",
              "Les donn�es d�j� modifi�es par les zones ne seront pas prise en compte"));
      cbInterpLines_ = new BuCheckBox(FSigLib.getS("Utiliser les sommets des lignes pour l'interpolation"));
      cbInterpLines_.setToolTipText(FSigLib
              .getS("Si activ�, les sommets des lignes (ferm�es ou non) seront utilis�es pour l'interpolation"));
      cbZone_.setSelected(true);
      cbInterp_.setSelected(false);
      cbInterpLines_.setSelected(false);
      cbInterpLines_.setEnabled(false);
      final Icon ic = UIManager.getIcon("OptionPane.warningIcon");
      warn_ = new BuLabel(ic);
      final BuPanel pn = new BuPanel(new BuGridLayout(2));
      pn.add(cbZone_);
      pn.add(warn_);
      warn_.setVisible(false);
      bottom.add(pn);
      bottom.add(cbInterp_);

      pnInterMethode_ = new BuPanel();
      pnInterMethode_.setBorder(BorderFactory.createCompoundBorder(BuBorders.EMPTY0505, BorderFactory
              .createTitledBorder(FSigLib.getS("Interpolation"))));
      pnInterMethode_.setLayout(new BuVerticalLayout());
      pnInterMethode_.add(cbInterpLines_);
      pnInterMethode_.add(buildInterpolationInZonePanel());
      pnInterMethode_.add(buildInterpolationSrcPanel());
      // pnInterMethode_.add(new BuLabel("interpolation methode"));
      bottom.add(pnInterMethode_);
      pnInterp_.setLayout(new BuBorderLayout());
      pnInterp_.add(bottom, BuBorderLayout.CENTER);
      pnInterp_.add(pnInterInfo_, BuBorderLayout.NORTH);
      cbInterp_.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(final ItemEvent _e) {
          setInterpolationEnable(cbInterp_.isEnabled() && cbInterp_.isSelected());
        }
      });
      cbInterp_.addPropertyChangeListener("enable", new PropertyChangeListener() {
        @Override
        public void propertyChange(final PropertyChangeEvent _evt) {
          setInterpolationEnable(cbInterp_.isEnabled() && cbInterp_.isSelected());
        }
      });
    } else {
      pnInterp_.remove(pnInterInfo_);
//      pnInterInfo_ = interpolator_.getData().getInfoPanel();
      pnInterInfo_ = new FSigWizardImportHelper.InfoPanel();
      ((FSigWizardImportHelper.InfoPanel) pnInterInfo_).setSrc(interpolator_.getData());
      pnInterp_.add(pnInterInfo_, BuBorderLayout.NORTH);
    }
    final FSigGeomSrcData data = interpolator_.getData();
    cbZone_.setEnabled(data.getNbPolygones() > 0);
    warn_.setVisible(false);
    if (cbZone_.isEnabled() && interpolator_.getAttModel().isAtomicSrcUsed()) {
      if (warn_.getToolTipText() == null) {
        final StringBuffer buf = new StringBuffer(120);
        buf.append("<html><body><u>").append(BuResource.BU.getString("Avertissement:"));
        buf.append("</u><br>");
        buf.append(FSigLib.getS("Des donn�es sont d�finies sur chaque sommets des zones"));
        buf.append("<br>");
        buf.append(FSigLib.getS("Pour ces donn�es, la valeur moyenne sera utilis�e pour<br>modifier les points contenus dans les zones"));
        warn_.setToolTipText(buf.toString());
      }
      warn_.setVisible(true);
      cbZone_.setSelected(false);
    }
    cbInterp_.setEnabled(data.getNbPoints() > 0 || data.getNbPolylignes() > 0 || data.getNbPolygones() > 0);
    if (cbInterp_.isEnabled() && !cbInterp_.isSelected()) {
      cbInterp_.setSelected(!cbZone_.isEnabled() || !cbZone_.isSelected());
    }
    if (cbInterp_.isEnabled() && cbInterp_.isSelected() && (data.getNbPolylignes() > 0 || data.getNbPolygones() > 0)) {
      cbInterpLines_.setEnabled(true);
      cbInterpLines_.setSelected(true);
    }
    setInterpolationEnable(cbInterp_.isEnabled() && cbInterp_.isSelected());
    cbInterpLines_.setEnabled(data.getNbPolylignes() > 0 || data.getNbPolygones() > 0);
  }

  protected void buildPnSrc() {
    if (pnSelectType_ == null) {
      pnSelectType_ = new BuPanel(new BuVerticalLayout(5, true, false));
      cbUseLayer_ = new BuCheckBox(FSigLib.getS("Modifier � partir de calques"));
      cbUseFiles_ = new BuCheckBox(FSigLib.getS("Modifier � partir de fichiers"));
      final ItemListener listener = new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent _e) {
          interpolator_.setData(null);
          interpolator_.getAttributes().removeAllElements();
        }
      };
      cbUseFiles_.addItemListener(listener);
      pnSelectType_.add(cbUseLayer_);
      pnSelectType_.add(cbUseFiles_);
      final ButtonGroup bt = new ButtonGroup();
      bt.add(cbUseFiles_);
      bt.add(cbUseLayer_);
      cbUseLayer_.setSelected(true);
      pnSelectType_.setPreferredSize(new Dimension(450, 350));
    }
  }

  protected void buildPreview() {
    buildPreview(true);

  }

  protected void buildPreview(final boolean _showPreview) {
    final FSigLoaderPreviewer prev = new FSigLoaderPreviewer() {
      @Override
      public FSigGeomSrcData loadData(final ProgressionInterface _prog) {
        buildMapperResult(_prog);
        return interpolator_.getData();
      }
    };
    prev.preview(impl_, interpolator_.getTarget().getGrid().getExtRings(), _showPreview);
  }

  protected void filterSelectedLayer() {
    selectedLayerEmpty_ = true;
    interpolator_.setData(null);
    final Set att = new HashSet();
    BCalque[] cq = model_.getSelection();
    if (model_.getSelectedCalque() == gisLayer_) {
      cq = gisLayer_.getCalques();
    }
    if (cq == null || cq.length == 0) {
      selectedLayer_.clear();
      interpolator_.getAttributes().removeAllElements();
      testSrc(lbLayersError_);
      return;
    }
    GISAttributeInterface[] attTmp = getAtt(cq[0]);
    final List selected = new ArrayList();
    if (cq[0].isGroupeCalque()) {
      final BCalque[] childs = cq[0].getCalques();
      for (int i = 0; i < childs.length; i++) {
        if (!selected.contains(childs[i])) {
          selected.add(childs[i]);
        }
      }
    } else {
      selected.add(cq[0]);
    }
    for (int i = attTmp.length - 1; i >= 0; i--) {
      if (isAccepted(attTmp[i])) {
        att.add(attTmp[i]);
      }
    }
    for (int i = cq.length - 1; i > 0; i--) {
      attTmp = getAtt(cq[i]);
      att.retainAll(Arrays.asList(attTmp));
      if (cq[0].isGroupeCalque()) {
        final BCalque[] childs = cq[i].getCalques();
        for (int j = 0; j < childs.length; j++) {
          if (!selected.contains(childs[j])) {
            selected.add(childs[j]);
          }
        }
      } else {
        selected.add(cq[i]);
      }
    }
    final Object[] o = att.toArray();
    Arrays.sort(o, new GISAttributeNameComparator());
    // on verifie que les attributes on ete modifiees
    boolean modify = false;
    final int nb = o.length;
    if (nb == interpolator_.getAttributes().getSize()) {
      for (int i = nb - 1; i >= 0 && !modify; i--) {
        if (!o[i].equals(interpolator_.getAttributes().elementAt(i))) {
          modify = true;
        }
      }
    } else {
      modify = true;
    }
    if (modify) {
      interpolator_.getAttributes().clear();

      for (int i = 0; i < nb; i++) {
        interpolator_.getAttributes().addElement(o[i]);
      }
      modifyTable();
    }
    selectedLayer_.clear();
    final int nbS = selected.size();
    for (int i = 0; i < nbS; i++) {
      if (selectedLayerEmpty_ && ((ZCalqueAffichageDonneesInterface) selected.get(i)).modeleDonnees().getNombre() > 0) {
        selectedLayerEmpty_ = false;
      }
      selectedLayer_.addElement(((BCalque) selected.get(i)).getTitle());
    }
    testSrc(lbLayersError_);
  }

  protected GISAttributeInterface[] getAtt(final BCalque _cqToScan) {
    GISAttributeInterface[] res = null;
    if (_cqToScan instanceof ZCalqueEditionGroup) {
      res = ((ZCalqueEditionGroup) _cqToScan).getAttributes();
    } else if (_cqToScan instanceof ZCalqueAffichageDonneesInterface
            && ((ZCalqueAffichageDonneesInterface) _cqToScan).modeleDonnees() instanceof ZModeleGeometry) {
      res = ((ZModeleGeometry) ((ZCalqueAffichageDonneesInterface) _cqToScan).modeleDonnees()).getGeomData()
              .getAttributes();
    }
    return res == null ? nullAtt_ : res;
  }

  protected boolean isAccepted(final GISAttributeInterface _att) {
    return _att.getDataClass().equals(Double.class);
  }

  protected boolean isLayerType() {
    return cbUseLayer_.isSelected();
  }

  protected void modifyTable() {
    if (table_ == null) {
      return;
    }
    final FSigVarAttrMapperTableModel model = interpolator_.getAttModel();
    if (!model.equals(table_.getModel())) {
      model.addTableModelListener(new TableModelListener() {
        @Override
        public void tableChanged(final TableModelEvent _e) {
          updateCibleState();
        }
      });
      table_.setModel(model);
      table_.getColumnModel().getColumn(1).setCellRenderer(model.getSrcCellRenderer());
      table_.getColumnModel().getColumn(1).setCellEditor(model.getSrcCellEditor());
    }
  }

  protected void setInterpolationEnable(final boolean _b) {
    if (pnInterMethode_ == null) {
      return;
    }
    cbInterpLines_.setEnabled(_b && interpolator_.getData().getNbPolygones() > 0
            || interpolator_.getData().getNbPolylignes() > 0);
    for (int i = pnInterMethode_.getComponentCount() - 1; i >= 0; i--) {
      pnInterMethode_.getComponent(i).setEnabled(_b);
    }
  }

  protected void testSrc(final JLabel _lb) {
    String message = null;
    if (interpolator_.getAttributes().getSize() == 0) {
      message = FSigLib.getS("Aucun attribut disponible");
    }
    if ((isLayerType() && selectedLayerEmpty_) || (!isLayerType() && interpolator_.getData() == null)) {
      final String emptyMes = FSigLib.getS("Aucune donn�e g�ographique disponible");
      if (message == null) {
        message = emptyMes;
      } else {
        message = message + ", " + emptyMes;
      }
    }
    if (message == null) {
      message = CtuluLibString.ESPACE;
    }
    _lb.setText(message);
    _lb.getParent().doLayout();
  }

  protected void updateCibleState() {
    if (lbCibleError_ != null) {
      lbCibleError_.setText(interpolator_.getAttModel().isAttributeUsed() ? CtuluLibString.EMPTY_STRING : FSigLib
              .getS("Aucune variable cible ne sera modifi�e"));
    }
  }

  /**
   * @param _progr
   * @return true si les resultats on pu etre construit
   */
  boolean buildMapperResult(final ProgressionInterface _progr) {
    if (isLayerType()) {
      final FSigGeomSrcData data = new FSigGeomSrcData();
      data.fill(model_.getSelection());
      interpolator_.setData(data);
      return true;
    }
    // fichier
    final CtuluAnalyze analyze = new CtuluAnalyze();
    interpolator_.setData(pnFiles_.loadAll(_progr, analyze));
    return !impl_.manageAnalyzeAndIsFatal(analyze);
  }

  @Override
  public void cancelTask() {
    removeAllListener();
    super.cancelTask();
  }

  @Override
  public void doTask() {
    removeAllListener();
    interpolator_.useZone_ = cbZone_.isEnabled() && cbZone_.isSelected();
    interpolator_.useInterpolation_ = cbInterp_.isEnabled() && cbInterp_.isSelected();
    interpolator_.useLinesInInterpolation_ = cbInterpLines_.isEnabled() && cbInterpLines_.isSelected();
    interpolator_.interpolationInZone_ = cbInterInRing_.isEnabled() && cbInterInRing_.isSelected();
    interpolator_.ignorePtExtern_ = cbInterIgnorePt_.isEnabled() && cbInterIgnorePt_.isSelected();
    Object v = ftInterInRing_.getValue();
    interpolator_.maxDistanceIntZone_ = v == null ? 1E-10 : ((Double) v).doubleValue();
    v = ftInterFromProject_.getValue();
    interpolator_.maxDistanceToIgnore_ = v == null ? 1E-10 : ((Double) v).doubleValue();
    if (FuLog.isTrace()) {
      FuLog.trace(getClass().getName() + " max distance zone " + interpolator_.interpolationInZone_ + ""
              + interpolator_.maxDistanceIntZone_);
      FuLog.trace(getClass().getName() + " max distance ignore" + interpolator_.ignorePtExtern_ + ": "
              + interpolator_.maxDistanceIntZone_);
    }
    final CtuluTaskDelegate task = impl_.createTask(FSigLib.getS("Interpolation"));
    task.start(new Runnable() {
      @Override
      public void run() {
        final CtuluAnalyze analyze = new CtuluAnalyze();
        FastBitSet set = null;
        if (cbModifySelected_.isEnabled() && cbModifySelected_.isSelected()) {
          set = selectedPt_;
        }
        interpolator_.process(analyze, set, task.getStateReceiver());

        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            if (analyze.containsErrors()) {
              impl_.manageAnalyzeAndIsFatal(analyze);
            } else if (!analyze.isEmpty()) {
              CtuluUIAbstract.showAnalyzeWarnAndInfo(analyze, impl_);
            }
          }
        });
      }
    });

    super.doTask();
  }

  public final BuWizardDialog getDialog() {
    return dialog_;
  }

  @Override
  public JComponent getStepComponent() {
    if (current_ == 0) {
      buildPnSrc();
      return this.pnSelectType_;
    } else if (current_ == 1) {
      if (cbUseLayer_.isSelected()) {
        buildLayerPanel();
        return pnLayers_;
      } else if (cbUseFiles_.isSelected()) {
        buildPnFiles();
        return pnFiles_;
      }
    } else if (current_ == 2) {
      if (dialog_ != null) {
        dialog_.setGlassPane(new BuGlassPaneStop());
      }
      buildMapperResult(null);
      buildPnCible();
      if (dialog_ != null) {
        final Component c = dialog_.getGlassPane();
        c.setVisible(false);
        dialog_.remove(c);
      }
      return pnCible_;
    } else if (current_ == 3) {
      buildPnInterpol();
      return pnInterp_;
    }
    return null;
  }

  @Override
  public int getStepCount() {
    return 4;
  }

  @Override
  public int getStepDisabledButtons() {
    return super.getStepDisabledButtons() | BuButtonPanel.DETRUIRE;
  }

  @Override
  public String getStepText() {
    final StringBuffer r = new StringBuffer();
    switch (current_) {
      case 0:
        r.append(FSigLib.getS("Choisir le type de la source: fichiers ou calques g�ographiques."));
        break;
      case 1:
        if (cbUseLayer_.isSelected()) {
          r.append(FSigLib.getS("Choisir les calques g�ographiques."));
          r.append(CtuluLibString.LINE_SEP).append(
                  FSigLib.getS("Utiliser les touches 'Ctrl' et/ou 'Maj' pour s�lectionner plusieurs calques."));
        } else {
          r.append(FSigLib.getS("Choisir les fichiers � utiliser."));
          r.append(CtuluLibString.LINE_SEP).append(FSigWizardImportHelper.getFormatExtHelp()).append(
                  CtuluLibString.LINE_SEP).append(FSigWizardImportHelper.getInsertOrderHelp());

        }
        break;
      case 2:
        r.append(FSigLib.getS("D�finir les variables � modifier"));
        r.append(CtuluLibString.LINE_SEP).append(
                FSigLib.getS("Si la s�lection du calque actif est non vide, il est possible de ne modifier que les objets s�lectionn�s"));
        break;
      case 3:
        r.append(FSigLib.getS("Pr�ciser la m�thode d'interpolation"));
        break;
      default:
        break;
    }
    return r.toString();
  }

  @Override
  public String getStepTitle() {
    switch (current_) {
      case 0:
        return FSigLib.getS("Choix du type de source");
      case 1:
        return FSigLib.getS("Source: s�lection");
      case 2:
        return FSigLib.getS("D�finition de la cible");
      case 3:
        return FSigLib.getS("M�thodes d'interpolation");
      default:
        return CtuluLibString.EMPTY_STRING;
    }

  }

  @Override
  public String getTaskTitle() {
    return FSigLib.getS("Modifier � partir de donn�es g�ographiques");
  }

  @Override
  public void nextStep() {
    if (current_ == 1 && (interpolator_.getData() == null || interpolator_.getAttributes().size() == 0)) {
      if (cbUseFiles_.isSelected()) {
        if (dialog_ != null) {
          dialog_.setGlassPane(new BuGlassPaneStop());
        }
        pnFiles_.buildPreview(false);
        if (!pnFiles_.isOk()) {
          return;
        }
        buildMapperResult(null);
        if (dialog_ != null) {
          final Component c = dialog_.getGlassPane();
          c.setVisible(false);
          dialog_.remove(c);
        }
      }

    } else if (current_ == 2 && !interpolator_.getAttModel().isAttributeUsed()) {
      return;
    }
    super.nextStep();
  }

  public final void setDialog(final BuWizardDialog _dialog) {
    dialog_ = _dialog;
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    filterSelectedLayer();
  }
}
