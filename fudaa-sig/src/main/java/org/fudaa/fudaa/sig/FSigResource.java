/**
 * @file         PrertResource.java
 * @creation     2002-09-25
 * @modification $Date: 2006-09-19 15:10:20 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @version $Id: FSigResource.java,v 1.4 2006-09-19 15:10:20 deniger Exp $
 * @author Fred Deniger
 */
public final class FSigResource extends FudaaResource {
  
  public final static FSigResource FSIG = new FSigResource(FudaaResource.FUDAA);

  private FSigResource(final BuResource _b) {
    super(_b);
  }

  /**
   * Traduit et retourne la chaine traduite, avec ou sans valeurs � ins�rer.
   *
   * @param _s La chaine � traduire.
   * @param _vals Les valeurs, de n'importe quelle type.
   * @return La chaine traduite.
   */
  public static String getS(String _s, Object ... _vals) {
    String r = FSIG.getString(_s);
    if (r == null) {
      return r;
    }

    for (int i=0; i<_vals.length; i++) {
      r = FuLib.replace(r, "{"+i+"}", _vals[i].toString());
    }
    return r;
  }
}