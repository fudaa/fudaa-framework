/*
 * @creation 5 avr. 2005
 *
 * @modification $Date$
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.layer;

import com.memoire.bu.*;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserTestWritable;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.rubar.io.RubarStCnFileFormat;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;
import org.fudaa.dodico.zesri.io.ZESRiFileFormat;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.edition.*;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.sig.FSigGraphEditorPanel;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.wizard.FSigWizardImport;
import org.locationtech.jts.geom.Geometry;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigEditor extends ZEditorDefault implements GISZoneListener, ZModelGeometryListener {
  private boolean exportWithId = false;

  /**
   * @param _panel
   */
  public FSigEditor(final ZEbliCalquesPanel _panel, final ZSceneEditor _editor) {
    super(_panel, _editor);
  }

  /**
   * @return true si les exports doivent utiliser les id � la place des noms.
   */
  public boolean isExportWithId() {
    return exportWithId;
  }

  public void setExportWithId(boolean exportWithId) {
    this.exportWithId = exportWithId;
  }

  /**
   * @param _panel
   */
  public FSigEditor(final ZEbliCalquesPanel _panel) {
    super(_panel);
    //    setMng(_cmd);
  }

  public ZEditorLigneBriseePanel getEditorPanel() {
    if (editorPanel_ == null) {
      super.getToolPanel();
    }
    return (ZEditorLigneBriseePanel) editorPanel_;
  }

  @Override
  public boolean changeState(String s) {
    return super.changeState(s);
  }

  /**
   * Contient l'index de l'onglet selectionn� dans le panel d'edition d'objet seul. C'est pour que d'une �dition � l'autre le m�me onglet soit
   * automatiquement selectionn�.
   */
  private int selectedTabInSingleObjectEditorPanel_ = 0;

  /**
   * L'�dition pour un objet selectionn� unique.
   *
   * @param _target Le calque cible editable.
   */
  @Override
  protected void editSingleObject(final ZCalqueEditable _target) {
    final int idxSelected = ((ZCalqueAffichageDonnees) _target).getLayerSelection().getMaxIndex();
    final EbliSingleObjectEditorPanel ed = new EbliSingleObjectEditorPanel(_target.getModelEditable(), idxSelected, true, true,
        getCoordinateDefinitions(), editorValidator_);
    configureEditorPanel(ed);
    if (isDeferredModificationAsDefault()) {
      ed.setDeferredModifications(true);
    }
    ed.setCmd(getMng());
    GISZoneCollection zone = _target.getModelEditable().getGeomData();
    GISAttributeDouble zAttr = zone.getAttributeIsZ();
    // Ajout de l'onglet "courbe"
    if (!(zone.getGeometry(idxSelected) instanceof GISPoint) && zAttr != null && zAttr.isAtomicValue()) {
      FSigGraphEditorPanel pnCurve = new FSigGraphEditorPanel(ed.getTableModel());
      ed.addNewTab(FSigLib.getS("Courbe"), pnCurve);
    }
    ed.setSelectedTab(selectedTabInSingleObjectEditorPanel_);
    ed.afficheModale(getFrame(), _target.getTitle());
    selectedTabInSingleObjectEditorPanel_ = ed.getSelectedTab();
  }

  /**
   * L'import peut �tre fait si le calque selectionn� est un groupe de calques SIG ou un calque poss�dant une geometrie modifiable
   *
   * @return true : L'import est possible, false : impossible.
   */
  public boolean canImportInSelectedLayer() {
    final ZCalqueEditable ed = super.getTarget();
    if (ed == null && ((getPanel().getArbreCalqueModel().getSelectedCalque() instanceof ZCalqueEditionGroup))) {
      return true;
    }
    if (ed == null || ed.getModelEditable().getGeomData() == null || !ed.getModelEditable().getGeomData().isGeomModifiable()) {
      return false;
    }
    return true;
  }

  public GISPolygone[] getEnglobPolygone() {
    final GrBoite b = getPanel().getDonneesCalque().getDomaine();
    return new GISPolygone[]{GISGeometryFactory.INSTANCE.createLinearRing(b.o_.x_, b.e_.x_, b.o_.y_, b.e_.y_)};
  }

  public static boolean isDeferredModificationAsDefault() {
    return "true".equals(System.getProperty("USED_DEFERRED_AS_DEFAUT"));
  }

  public static void activeDeferredModificationAsDefault() {
    System.setProperty("USED_DEFERRED_AS_DEFAUT", "true");
  }

  private static class ZCalqueEditionGroupFromGeomLayer extends ZCalqueEditionGroup {
    private final ZCalqueEditable calque;
    private final CtuluCommandContainer cmd;

    public ZCalqueEditionGroupFromGeomLayer(ZCalqueEditable calque, CtuluCommandContainer cmd) {
      this.calque = calque;
      this.cmd = cmd;
    }

    @Override
    public GISAttributeInterface[] getAttributes() {
      return calque.getModelEditable().getGeomData().getAttributes();
    }

    @Override
    public boolean canAddNewLayer() {
      return false;
    }

    @Override
    public CtuluCommandContainer getCmdMng() {
      return cmd;
    }

    @Override
    public GISZoneCollectionLigneBrisee createLigneBriseeZone() {
      return null;
    }

    @Override
    public GISZoneCollectionPoint createPointZone() {
      return null;
    }

    @Override
    public ZCalqueLigneBrisee addLigneBriseeLayerAct(String _title, GISZoneCollectionLigneBrisee _zone, CtuluCommandContainer _cmd) {
      return null;
    }

    @Override
    public ZCalquePointEditable addPointLayerAct(String _title, GISZoneCollectionPoint _zone, CtuluCommandContainer _cmd) {
      return null;
    }
  }

  public void importSelectedLayer() {
    final ZCalqueEditable ed = super.getTarget();
    ZCalqueEditionGroup dest = null;
    BCalque parent = null;
    if (ed == null) {
      parent = super.getPanel().getArbreCalqueModel().getSelectedCalque();
    } else {
      parent = (BCalque) ((BCalque) ed).getParent();
    }
    if (parent instanceof ZCalqueEditionGroup) {
      dest = (FSigLayerGroup) parent;
    } else {
      dest = new ZCalqueEditionGroupFromGeomLayer(ed, this.getMng());
    }

    final FSigWizardImport importWizard = new FSigWizardImport(dest, getEnglobPolygone(), ed, ((FSigVisuPanel) getPanel()).getCtuluUI());
    final BuWizardDialog dialog = new BuWizardDialog(CtuluLibSwing.getFrameAncestorHelper(super.getPanel()), importWizard);
    importWizard.setDialog(dialog);
    dialog.pack();
    dialog.setModal(true);
    dialog.setLocationRelativeTo(getPanel());
    dialog.setVisible(true);
  }

  public void exportSelectedLayer() {
    exportSelectedLayer(isExportWithId());
  }

  public void exportSelectedLayer(boolean useIdAsAttributeName) {

    final BCalque[] parent = super.getPanel().getArbreCalqueModel().getSelection();
    if (parent == null) {
      return;
    }
    final FSigLayerFilter filter = new FSigLayerFilter();
    filter.setExportWithId(useIdAsAttributeName);
    final int nb = parent.length;
    for (int i = 0; i < nb; i++) {
      parent[i].apply(filter);
    }

    if (filter.isEmpty()) {
      return;
    }

    final Map filterExporter = generateSupportedFileFilter();
    if (filterExporter == null) return;
    final BuFileFilter[] filters = new BuFileFilter[filterExporter.size()];
    filterExporter.keySet().toArray(filters);
    final CtuluFileChooser fileChooser = FudaaGuiLib.getFileChooser(FudaaLib.getS("Export"), filters, null);
    fileChooser.setAcceptAllFileFilterUsed(false);
    CtuluFileChooserTestWritable tester = new CtuluFileChooserTestWritable(getUi());
    tester.setAppendExtension(true);
    fileChooser.setTester(tester);
    final File f = FudaaGuiLib.chooseFile(CtuluLibSwing.getFrameAncestorHelper(getPanel()), true, fileChooser);
    if (f == null) {
      return;
    }
    final FSigLayerExporterI fileExporter = (FSigLayerExporterI) filterExporter.get(fileChooser.getFileFilter());
    if (fileExporter == null) {
      getPanel().getCtuluUI().error(FSigLib.getS("Le format d'export n'a pas �t� trouv�"));
      return;
    }
    CtuluDialogPanel pnExporterTranslate = new CtuluDialogPanel();
    pnExporterTranslate.setLayout(new BuGridLayout(2));
    BuTextField translateX = pnExporterTranslate.addLabelDoubleText(FudaaLib.getS("En X:"));
    BuTextField translateY = pnExporterTranslate.addLabelDoubleText(FudaaLib.getS("En Y:"));
    translateX.setText("0");
    translateY.setText("0");
    double x = 0;
    double y = 0;
    boolean isTranslate = false;
    if (pnExporterTranslate.afficheModaleOk(getPanel(), FSigLib.getS("Translater les donn�es"))) {
      if (!CtuluLibString.isEmpty(translateX.getText())) {
        try {
          x = Double.parseDouble(translateX.getText());
        } catch (NumberFormatException e) {
        }
      }
      if (!CtuluLibString.isEmpty(translateY.getText())) {
        try {
          y = Double.parseDouble(translateY.getText());
        } catch (NumberFormatException e) {
        }
      }
      isTranslate = !CtuluLib.isZero(x) || !CtuluLib.isZero(y);
    }
    final GISPoint translate = isTranslate ? new GISPoint(x, y, 0) : null;
    new CtuluRunnable(FudaaLib.getS("Export") + ' ' + parent[0].getTitle(), getUi()) {
      @Override
      public boolean run(ProgressionInterface _proj) {
        final CtuluIOOperationSynthese op = filter.exportTo(fileExporter, getUi(), f,
            translate, _proj);
        if (op != null) {
          getUi().manageErrorOperationAndIsFatal(op);
        }
        return true;
      }
    }.run();
  }

 protected Map<BuFileFilter, GISFileFormat> generateSupportedFileFilter() {
    final BuFileFilter sx = SinusxFileFormat.getInstance().createFileFilter();
    final BuFileFilter st = RubarStCnFileFormat.createStFilter();
    final BuFileFilter gen = ZESRiFileFormat.getInstance().createFileFilter();

    final Map filterExporter = new HashMap(5);// pour l'instant .....

    filterExporter.put(sx, new FSigLayerExporterI.ToSinusX());
    filterExporter.put(st, new FSigLayerExporterI.ToRubar());
    filterExporter.put(gen, new FSigLayerExporterI.ToSZESRI());
    final Map<BuFileFilter, GISFileFormat> dataStores = GISExportDataStoreFactory.buildFileFilterMapExcludeGML();
    if (dataStores.size() == 0) {
      if (FuLog.isTrace()) {
        FuLog.trace("no gis format to export");
      }
      return null;
    }
    for (final Map.Entry<BuFileFilter, GISFileFormat> e : dataStores.entrySet()) {
      filterExporter.put(e.getKey(), new FSigLayerExporterI.DataStore(e.getValue()));
    }
    return filterExporter;
  }

  EbliActionSimple[] editAction_;
  EbliActionSimple exportAction_;
  EbliActionSimple visuAction_;
  EbliActionSimple importAction_;
  EbliActionSimple joinAction_;

  /**
   * @return les actions d'edition pour les calque editables
   */
  public EbliActionSimple[] getEditAction() {
    if (editAction_ == null) {
      editAction_ = new EbliActionSimple[]{new EditAction()};
    }
    return editAction_;
  }

  public EbliActionSimple getExportAction() {
    if (exportAction_ == null) {
      exportAction_ = new ExportAction();
    }
    return exportAction_;
  }

  public EbliActionSimple getVisuAction() {
    if (visuAction_ == null) {
      visuAction_ = new VisuAction();
    }
    return visuAction_;
  }

  public EbliActionSimple getImportAction() {
    if (importAction_ == null) {
      importAction_ = new ImportAction();
    }
    return importAction_;
  }

  private class VisuAction extends EbliActionSimple {
    protected VisuAction() {
      super(FudaaLib.getS("Visualiser"), null, "GIS_EDIT");
      setDefaultToolTip(FudaaLib.getS("Visualise les objets s�lectionn�s"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      visuSelectedLayer();
    }

    @Override
    public void updateStateBeforeShow() {
      final BCalque cq = FSigEditor.this.getPanel().getArbreCalqueModel().getSelectedCalque();
      super.setEnabled((cq instanceof ZCalqueAffichageDonnees) && (!((ZCalqueAffichageDonnees) cq).isSelectionEmpty())
          && ((((ZCalqueAffichageDonnees) cq).modeleDonnees() instanceof ZModeleGeometry)));
    }

    @Override
    public String getEnableCondition() {
      return FudaaLib.getS("S�lectionner au moins un objet");
    }
  }

  private class ExportAction extends EbliActionSimple {
    protected ExportAction() {
      super(FudaaLib.getS("Exporter"), BuResource.BU.getIcon("exporter"), "GIS_EXPORT");
      setDefaultToolTip(FudaaLib.getS("Exporter les donn�es"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      exportSelectedLayer();
    }

    @Override
    public void updateStateBeforeShow() {
      boolean canExport = canExportSelectedLayer();
      //action activ�e que si des donn�es sont pr�sentes
      if (canExport) {
        canExport = false;
        final BCalque[] parent = getPanel().getArbreCalqueModel().getSelection();
        if (parent != null) {
          for (BCalque bCalque : parent) {
            if (bCalque instanceof ZCalqueAffichageDonneesInterface
                && ((ZCalqueAffichageDonneesInterface) bCalque).modeleDonnees() instanceof ZModeleGeometry) {
              ZModeleGeometry modeleGeometry = (ZModeleGeometry) ((ZCalqueAffichageDonneesInterface) bCalque).modeleDonnees();
              if (modeleGeometry.getNombre() > 0) {
                canExport = true;
              }
            }
          }
        }
      }
      super.setEnabled(canExport);
    }

    @Override
    public String getEnableCondition() {
      return FudaaLib.getS("S�lectionner un calque non vide");
    }
  }

  private class ImportAction extends EbliActionSimple {
    protected ImportAction() {
      super(BuResource.BU.getString("Importer"), BuResource.BU.getIcon("importer"), "GIS_IMPORT");
      setDefaultToolTip(FudaaLib.getS("Importer les donn�es"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      importSelectedLayer();
    }

    @Override
    public void updateStateBeforeShow() {
      super.setEnabled(canImportInSelectedLayer());
    }

    @Override
    public String getEnableCondition() {
      return FudaaLib.getS("S�lectionner un calque non vide et �ditable");
    }
  }

  private class EditAction extends EbliActionSimple {
    protected EditAction() {
      super(BuResource.BU.getString("Editer"), BuResource.BU.getIcon("editer"), "GIS_EDIT");
      setDefaultToolTip(FudaaLib.getS("Editer les objets s�lectionn�s"));
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      edit();
    }

    @Override
    public void updateStateBeforeShow() {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("FTR gis target= " + FSigEditor.this.getTarget());
      }
      // R�cup�ration des calques contenant des g�om�tries selectionn�es \\
      ZCalqueAffichageDonneesInterface[] calques = getSupport().getAllLayers();
      ArrayList<ZCalqueAffichageDonneesInterface> claquesWithSelectedObjects = new ArrayList<ZCalqueAffichageDonneesInterface>();
      for (int i = 0; i < calques.length; i++) {
        if (!calques[i].isSelectionEmpty() && calques[i] instanceof ZCalqueEditable) {
          claquesWithSelectedObjects.add(calques[i]);
        }
      }
      super.setEnabled(claquesWithSelectedObjects.size() > 0);
    }

    @Override
    public String getEnableCondition() {
      return FudaaLib.getS("S�lectionner au moins un objet");
    }
  }

  protected boolean canExportSelectedLayer() {
    final BCalque[] parent = super.getPanel().getArbreCalqueModel().getSelection();
    if (parent == null) {
      return false;
    }
    final FSigLayerFilter filter = new FSigLayerFilter();
    final int nb = parent.length;
    for (int i = 0; i < nb; i++) {
      parent[i].apply(filter);
    }
    return !(filter.isEmpty());
  }

  @Override
  public final Frame getFrame() {
    return CtuluLibSwing.getFrameAncestorHelper(getPanel().getCtuluUI().getParentComponent());
  }

  @Override
  public void attributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int att, GISAttributeInterface _att, int geom, Object value) {
    getPanel().getArbreCalqueModel().fireObservableChanged();
  }

  @Override
  public void objectAction(Object _source, int geom, Object _obj, int _action) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FTR: gis editor geometry changed " + _obj.getClass());
    }
    getPanel().getArbreCalqueModel().fireObservableChanged();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZModelListener#geometryAction(java.lang.Object, int, com.vividsolutions.jts.geom.Geometry, int)
   */
  @Override
  public void geometryAction(Object _source, int geom, Geometry _geom, int _action) {
    objectAction(_source, geom, _geom, _action);
  }
}
