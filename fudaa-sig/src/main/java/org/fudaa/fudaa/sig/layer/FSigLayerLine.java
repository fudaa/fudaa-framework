/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2007-01-19 13:14:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.layer;

import org.fudaa.ebli.calque.BCalquePersistenceInterface;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;

/**
 * @author Fred Deniger
 * @version $Id: FSigLayerLine.java,v 1.1 2007-01-19 13:14:29 deniger Exp $
 */
@SuppressWarnings("serial")
public class FSigLayerLine extends ZCalqueLigneBrisee {

  /**
   * @param _modele
   */
  public FSigLayerLine(final ZModeleLigneBrisee _modele) {
    super(_modele);
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new FSigLayerLinePersistence();
  }
}
