/*
 *  @creation     25 mai 2005
 *  @modification $Date: 2007-05-04 14:00:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import java.util.BitSet;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleArray;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometry;
import org.fudaa.ctulu.gis.GISVisitorDefault;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: FSigVarPolygoneModifierActivity.java,v 1.9 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigVarPolygoneModifierActivity implements CtuluActivity {

  boolean stop_;

  public FSigVarPolygoneModifierActivity() {
    super();
  }
  /**
   * @author Fred Deniger
   * @version $Id: FSigVarPolygoneModifierActivity.java,v 1.9 2007-05-04 14:00:26 deniger Exp $
   */
  static class PolygoneFinder extends GISVisitorDefault {

    boolean res_;

    protected void init() {
      res_ = false;
    }

    @Override
    public boolean visitPolygoneWithHole(final Polygon _p) {
      res_ = false;
      return false;
    }

    @Override
    public boolean visitPoint(final Point _p) {
      res_ = false;
      return false;
    }

    @Override
    public boolean visitPolygone(final LinearRing _p) {
      res_ = true;
      return false;
    }

    @Override
    public boolean visitPolyligne(final LineString _p) {
      res_ = false;
      return false;
    }
  }

  /**
   * @param _res le conteneur des resultats
   * @param _src la source : il est suppose que les attributs sont dans le bon ordre
   * @param _progress la barre de progression
   * @param _strict strict si le filtre doit etre stict.
   */
  public void process(final FSigVariableModifResult _res, final GISDataModel _src,
      final ProgressionInterface _progress, final boolean _strict) {
    final int nbGeom = _src.getNumGeometries();
    if (nbGeom == 0) {
      return;
    }
    final FSigFilterZone[] lineFilter = new FSigFilterZone[nbGeom];
    // true si la geometrie a deja ete teste
    final BitSet isGeomTested = new BitSet(lineFilter.length);
    _res.computeMinMax();
    final int min = _res.getMinIdx();
    final int max = _res.getMaxIdx();
    final EfGridInterface grid = _res.getGrid();
    final boolean isEltVar = _res.isTargetElement();
    final double[] values = new double[_src.getNbAttributes()];
    FSigFilterZone current;
    final PolygoneFinder isPolygone = new PolygoneFinder();
    for (int i = min; i <= max; i++) {
      // l'indice est deja calcul ou si on ne veut qu'il le soit
      if (_res.isDone(i)) {
        continue;
      }
      if (stop_) {
        return;
      }
      for (int k = 0; k < nbGeom; k++) {
        if (stop_) {
          return;
        }
        current = lineFilter[k];
        // le filtre n'est pas initialisť et la geometrie n'a pas ete testee.
        // dans ce cas si c'est une ligne ferme on ajoute le filtre
        if (current == null && !isGeomTested.get(k)) {
          isGeomTested.set(k);
          isPolygone.init();
          ((GISGeometry) _src.getGeometry(k)).accept(isPolygone);
          if (isPolygone.res_) {
            current = new FSigFilterZone(grid, (LinearRing) _src.getGeometry(k));
            lineFilter[k] = current;
          }
        }
        if (current != null
            && ((isEltVar && current.isActivatedElt(i)) || (!isEltVar && current.isActivated(i)))) {
          for (int var = _src.getNbAttributes() - 1; var >= 0; var--) {
            if (_src.getAttribute(var).isAtomicValue()) {
              values[var] = ((GISAttributeModelDoubleArray) _src.getValue(var, k)).getDoubleAverage();
            } else {
              values[var] = _src.getDoubleValue(var, k);
            }
          }
          _res.setDone(i, values);
        }

      }

    }
    if (stop_) {
      return;
    }

  }

  @Override
  public void stop() {
    stop_ = true;
  }
}
