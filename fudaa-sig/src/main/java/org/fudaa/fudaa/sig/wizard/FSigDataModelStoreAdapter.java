/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureSource;
import org.geotools.data.FileDataStoreFactorySpi;
import org.geotools.data.store.ContentDataStore;
import org.geotools.feature.FeatureIterator;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: FSigDataModelStoreAdapter.java,v 1.3 2007-04-30 14:22:38 deniger Exp $
 */
public class FSigDataModelStoreAdapter extends AbstractFSigDataModelSIGAdapter {
  private final FeatureSource<SimpleFeatureType, SimpleFeature> featureSource;

  public static FSigDataModelStoreAdapter create(final FileDataStoreFactorySpi _factory, final File _f,
                                                 final ProgressionInterface _progress) throws IOException {
    final DataStore dataStore = _factory.createDataStore(_f.toURI().toURL());
    if (dataStore instanceof ContentDataStore) {
      ((ContentDataStore) dataStore).setGeometryFactory(GISGeometryFactory.INSTANCE);
    }
    final String[] name = dataStore.getTypeNames();
    if (name == null || name.length == 0) {
      throw new IOException("name not found");
    }
    FeatureSource<SimpleFeatureType, SimpleFeature> source = dataStore.getFeatureSource(name[0]);
    return new FSigDataModelStoreAdapter(loadGeometries(source, _progress), source.getSchema(), source);
  }

  public FSigDataModelStoreAdapter(List<Geometry> geometries, SimpleFeatureType simpleFeatureType, FeatureSource<SimpleFeatureType, SimpleFeature> source) {
    super(geometries, simpleFeatureType);
    featureSource = source;
  }

  @Override
  protected FeatureIterator<SimpleFeature> createIterator() throws IOException {
    return featureSource.getFeatures().features();
  }
}
