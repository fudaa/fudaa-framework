/*
 *  @creation     10 juin 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.layer;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.Point;
import gnu.trove.TIntArrayList;
import java.util.HashSet;
import java.util.Set;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleGeometry;

/**
 * Un filtre sur les calques a exporter. Les calques a exporter peuvent être uniquement les calques visibles.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigLayerFilter extends FSigLayerFilterAbstract {

  protected boolean bonlyVisible_ = false;
  protected boolean onlySelectedGeometries_ = false;
  protected boolean bonlyOnVisibleGeometries_ = true;
  final Set<ZCalqueAffichageDonneesInterface> cqs_ = new HashSet<ZCalqueAffichageDonneesInterface>();
  

  /**
   *
   */
  public FSigLayerFilter() {
    super();
  }

  @Override
  protected GISDataModel getCollect(final ZCalqueAffichageDonneesInterface _o) {
    GISZoneCollection zone = ((ZModeleGeometry) _o.modeleDonnees()).getGeomData();
    zone.prepareExport();

    // On ne retient que les géométries sélectionnées.
    if (onlySelectedGeometries_) {
      return new GISDataModelFilterAdapter(zone, null, _o.getSelectedObjectInTable());
    } // On ne retient que les géométries visibles du calque.
    else if (bonlyOnVisibleGeometries_) {
      ZModeleGeometry md = (ZModeleGeometry) _o.modeleDonnees();
      TIntArrayList idx = new TIntArrayList(md.getNombre());
      for (int id = 0; id < md.getNombre(); id++) {
        if (md.isGeometryVisible(id)) {
          idx.add(id);
        }
      }
      return new GISDataModelFilterAdapter(zone, null, idx.toNativeArray());
    } else {
      return zone;
    }
  }

  /**
   * Le traitement ne s'applique qu'aux seuls calques visibles.
   *
   * @param _b True : Uniquement les calques visibles sont considérés.
   */
  public void setTreatmentOnlyOnVisible(boolean _b) {
    bonlyVisible_ = _b;
  }

  /**
   * Le traitement ne se fait que sur les géométries visibles.
   *
   * @param _b True : Uniquement les géométries visibles des calques sélectionnés sont considérées.
   */
  public void setTreatmentOnlyOnVisibleGeometries(boolean _b) {
    bonlyOnVisibleGeometries_ = _b;
  }

  /**
   * Le traitement s'applique uniquement aux géométries selectionnées.
   */
  public void setTreatmentOnlySelectedGeometries(boolean _b) {
    onlySelectedGeometries_ = _b;
  }

  @Override
  public boolean visit(final BCalque _cq) {
    // Calque non traité si invisible.
    if (bonlyVisible_ && !_cq.isVisible()) {
      return true;
    }

    if (!cqs_.contains(_cq) && _cq instanceof ZCalqueAffichageDonneesInterface
            && ((ZCalqueAffichageDonneesInterface) _cq).modeleDonnees() instanceof ZModeleGeometry) {

      ZCalqueAffichageDonneesInterface calque = (ZCalqueAffichageDonneesInterface) _cq;
      ZModeleGeometry geom = (ZModeleGeometry) calque.modeleDonnees();
      cqs_.add(calque);
      if (!onlySelectedGeometries_ || !calque.isSelectionEmpty()) {
        if (geom.getGeomData().getDataStoreClass().equals(Point.class)) {
          pointCq_.add(calque);
        } else if (LineString.class.isAssignableFrom(geom.getGeomData().getDataStoreClass())) {
          polyCq_.add(calque);
        } else if (MultiPoint.class.isAssignableFrom(geom.getGeomData().getDataStoreClass())) {
          mlptsCq_.add(calque);
        }
      }
    }
    return true;
  }
}
