/*
 *  @creation     8 juin 2005
 *  @modification $Date: 2007-04-30 14:22:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISDataModelListPtAdapter;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.dodico.fortran.FortranDoubleReaderResult;
import org.fudaa.dodico.rubar.io.RubarSEMFileFormat;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderRubarSem.java,v 1.2 2007-04-30 14:22:39 deniger Exp $
 */
public class FSigFileLoaderRubarSem implements FSigFileLoaderI {

  final BuFileFilter ft_;

  public FSigFileLoaderRubarSem() {
    ft_ = new RubarSEMFileFormat().createFileFilter();
  }

  private FSigFileLoaderRubarSem(final BuFileFilter _ft) {
    ft_ = _ft;
  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderRubarSem(ft_);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final CtuluIOOperationSynthese op = new RubarSEMFileFormat().read(_f, _prog);
    if (op.containsMessages() && op.getAnalyze().containsErrors()) {
      _analyze.merge(op.getAnalyze());
      return;
    }
    final FortranDoubleReaderResult res = (FortranDoubleReaderResult) op.getSource();
    if (res.getNbLigne() == 0) {
      return;
    }
    final int nbPt = res.getNbLigne();
    final GISPoint[] pts = new GISPoint[nbPt];
    final double[][] z = new double[1][nbPt];
    final GISAttributeInterface att = GISAttributeConstants.BATHY;
    _r.addUsedAttributes(new GISAttributeInterface[] { att });
    for (int i = 0; i < nbPt; i++) {
      pts[i] = (GISPoint) GISGeometryFactory.INSTANCE.createPoint(res.getValue(i, 0), res.getValue(i, 1), res.getValue(
          i, 2));
      z[0][i] = pts[i].getZ();
    }
    _r.nbPoint_ += nbPt;
    _r.nbPointTotal_ += nbPt;
    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(new GISDataModelListPtAdapter(pts, new GISAttributeInterface[] { att }, z));
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    // 
    _r.pointModel_.add(adapter);
  }
}
