/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2008-02-01 14:42:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Une interface repr�sentant un lecteur de fichier contenant des objets g�om�triques.<p>
 * Cette interface poss�de une methode de lecture du fichier et stockage des objets
 * lus dans un conteneur {@link FSigFileLoadResult}
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderI.java,v 1.1.6.2 2008-02-01 14:42:42 bmarchan Exp $
 */
public interface FSigFileLoaderI {

  /**
   * Retourne un nouveau loader de la m�me classe que celle de l'instance courante.
   * @return Le loader
   */
  FSigFileLoaderI createNew();

  /**
   * Lit le fichier _f et stocke les objets g�om�triques dans _r. Des informations/erreurs peuvent
   * �tre produites et g�r�es par _analyse.
   * @param _r le conteneur de resultat
   * @param _f le fichier a lire si necessaire
   * @param _fileOrigine un string indiquant � la valeur � mettre dans l'attribut ETAT_GEOM
   * @param _prog la barre de progression
   * @param _analyze L'analyseur d'informations.
   */
  void setInResult(FSigFileLoadResult _r, File _f, String _fileOrigine, ProgressionInterface _prog, CtuluAnalyze _analyze);

  /**
   * Le filtre pour les fichiers accept�s par ce lecteur.
   * @return Le filtre.
   */
  BuFileFilter getFileFilter();
}