/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sig.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.text.DecimalFormat;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleArray;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceBox;

/**
 * Decorator to FsigLayer that allow to display values of variable on FsigLayer for any variable declared on its parent group.
 *
 * @author Adrien Hadoux
 */
public class FSigLayerVariableDisplay implements BSelecteurListTarget {

  private boolean showValues = false;
  private TraceBox paintBox = new TraceBox();
  ListSelectionModel selectionModel = new DefaultListSelectionModel();
  DefaultListModel listModel = new DefaultListModel();

  public TraceBox getTraceBox() {
    return paintBox;
  }

  public void changeColor(Color color) {
    paintBox.setColorText(color);
  }

  public void changeColorBackGround(Color color) {
    paintBox.setColorFond(color);
  }

  public void changeBorderColor(Color color) {
    paintBox.setColorBoite(color);
  }

  public void changePosition() {
    paintBox.setVertical(!paintBox.isVertical());
  }

  @Override
  public ListModel getListModel() {
    return listModel;
  }

  @Override
  public ListSelectionModel getListSelectionModel() {
    return selectionModel;
  }

  public void paintValueOfPoint(GISDataModel model, final Graphics2D _g, final GrPoint p, final int indicePoint,
          final GrMorphisme _versEcran, final GrBoite _clipReel) {
    if (showValues) {
      String value = getValueForIdx(indicePoint, model);
      if (StringUtils.isNotBlank(value)) {
        paintBox.paintBox(_g, (int) p.x_, (int) p.y_, value);
      }
    }
  }

  public boolean isShowValues() {
    return this.showValues;
  }

  public void showValues(final boolean showValues, final BCalque contener) {
    this.showValues = showValues;
    contener.repaint();
  }

  public void feedVariable(final FSigLayerGroup parent) {
    if (listModel == null) {
      listModel = new DefaultListModel();
    } else {
      listModel.removeAllElements();
    }
    if (parent != null && parent.getAttributes() != null) {
      for (GISAttributeInterface att : parent.getAttributes()) {
        listModel.addElement(att);
      }
    }

  }

  /**
   * display variable value for Idx.
   *
   * @param idX
   * @return
   */
  public String getValueForIdx(final int idX, GISDataModel dataModel) {
    int index = selectionModel.getMinSelectionIndex() == -1 ? 0 : selectionModel.getMinSelectionIndex();

    if (dataModel != null) {
      DecimalFormat decFormat = new DecimalFormat("##.####");
      if (dataModel.getNbAttributes() > index && idX < dataModel.getNumGeometries()
              && dataModel.getValue(index, idX) != null && dataModel.getValue(index, idX) instanceof Double) {

        return decFormat.format(dataModel.getDoubleValue(0, idX));
      } else if (dataModel.getNbAttributes() > index && dataModel.getValue(index, 0) instanceof GISAttributeModelDoubleArray) {
        GISAttributeModelDoubleArray array = (GISAttributeModelDoubleArray) dataModel.getValue(index, 0);
        if (idX < array.getSize()) {
          return decFormat.format(array.getValue(idX));
        }
      } else if (dataModel.getNbAttributes() == 0 && dataModel instanceof GISZoneCollection) {
        GISZoneCollection collection = (GISZoneCollection) dataModel;
        if (idX < collection.getCoordinates().length) {
          return decFormat.format(collection.getCoordinates()[idX].z);
        }
      }
    }
    return "";
  }

}
