/*
 * @creation 2 juin 08
 * 
 * @modification $Date:$
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuFormLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMultiFormLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuTitledPanel;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZModelClassLayerFilter;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.sig.FSigExportImportAttributesMapper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author Bertrand Marchand
 * @version $Id:$
 */
public class FSigWizardImportStepDestination extends JPanel {
  private ZCalqueEditable selectedLayer_;
  FSigWizardImportHelper.PanelItemListener importLignes_;
  BuCheckBox cbImportLignesFromPt_;
  BuComboBox cmbImpLinesFromPt_;
  FSigWizardImportHelper.PanelItemListener importPoint_;
  FSigWizardImportHelper.SpecCombo cmbDestLignes_;
  FSigWizardImportHelper.SpecCombo cmbDestPoint_;
  BuTextField tfNewLigneLayer_;
  BuTextField tfNewPointLayer_;
  private BGroupeCalque dest_;
  FSigGeomSrcData data_;
  BuCheckBox cbImportPoint_;
  BuCheckBox cbImportLignes_;
  FSigWizardImportHelper.InfoPanel pnInfo_;
  private FSigExportImportAttributesMapper mapper_;
  BuScrollPane scAttrs_;
  private CtuluValueEditorDouble translate_;
  private JComponent tfTranslateX_;
  private JComponent tfTranslateY_;
  private JCheckBox jcTranslate;

  protected FSigWizardImportStepDestination() {
  }

  public FSigWizardImportStepDestination(BCalque _dest, boolean canAddLayer) {
    dest_ = (BGroupeCalque) _dest;
    setLayout(new BuVerticalLayout(2, true, true));
    cbImportLignes_ = new BuCheckBox(FSigLib.getS("Importer les lignes"));
    cbImportLignesFromPt_ = new BuCheckBox() {
      @Override
      public void setEnabled(final boolean _b) {
        super.setEnabled(_b && importLignes_.isOk() && data_ != null && data_.getNbPoints() > 0);
      }
    };
    cbImportLignesFromPt_.setToolTipText(FSigLib.getS("Chaque fichier contenant des points sera utilis� pour cr�er une polyligne ou un polygone"));
    cmbImpLinesFromPt_ = new BuComboBox() {
      @Override
      public void setEnabled(final boolean _b) {
        super.setEnabled(_b && importLignes_.isOk() && cbImportLignesFromPt_.isSelected());
      }
    };
    cmbImpLinesFromPt_.setModel(new DefaultComboBoxModel(new String[] { FSigLib.getS("Cr�er des polylignes pour les points import�s"),
        FSigLib.getS("Cr�er des polygones pour les points import�s"),
        FSigLib.getS("Reconna�tre les polygones: le premier et le dernier point sont identiques") }));
    cbImportLignesFromPt_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        cmbImpLinesFromPt_.setEnabled(cbImportLignesFromPt_.isSelected() && cbImportLignesFromPt_.isEnabled());
      }
    });
    cbImportPoint_ = new BuCheckBox(FSigLib.getS("Importer les points"));
    final BuFormLayout formLayout = new BuMultiFormLayout(5, 5);
    final BuPanel pnPoints = new BuTitledPanel(cbImportPoint_, formLayout);
    importPoint_ = new FSigWizardImportHelper.PanelItemListener(pnPoints, cbImportPoint_);
    ZModelClassLayerFilter filter = new ZModelClassLayerFilter(GISZoneCollectionPoint.class);
    dest_.apply(filter);
    ZCalqueAffichageDonneesInterface[] res = filter.getCalques();
    ComboBoxModel model = new FSigWizardImportHelper.CalqueModel(res, CtuluLib.getS("Nouveau"));
    cmbDestPoint_ = new FSigWizardImportHelper.SpecCombo();
    final FSigWizardImportHelper.CalqueCellRender render = new FSigWizardImportHelper.CalqueCellRender();
    cmbDestPoint_.setRenderer(render);
    model.addListDataListener(new ListDataListener() {

      @Override
      public void contentsChanged(final ListDataEvent _e) {
        tfNewPointLayer_.setEnabled(cmbDestPoint_.getSelectedIndex() == 0);
      }

      @Override
      public void intervalAdded(final ListDataEvent _e) {
      }

      @Override
      public void intervalRemoved(final ListDataEvent _e) {
      }
    });
    cmbDestPoint_.setModel(model);
    cmbDestPoint_.setEnabled(res.length > 0);
    pnPoints.add(new BuLabel(FSigLib.getS("Le calque de destination")), BuFormLayout.constraint(0, 0));
    pnPoints.add(cmbDestPoint_, BuFormLayout.constraint(1, 0));
    tfNewPointLayer_ = new BuTextField(10) {
      @Override
      public void setEnabled(final boolean _enabled) {
        super.setEnabled(_enabled && cmbDestPoint_.getSelectedIndex() == 0 && importPoint_.isOk());
      }
    };
    tfNewPointLayer_.setEnabled(true);
    tfNewPointLayer_.setText(FSigLib.getS("Points"));
    pnPoints.add(tfNewPointLayer_, BuFormLayout.constraint(2, 0, true, 1f));
    if (canAddLayer) {
      add(pnPoints);
    }
    final BuPanel pnLines = new BuTitledPanel(cbImportLignes_, formLayout);
    importLignes_ = new FSigWizardImportHelper.PanelItemListener(pnLines, cbImportLignes_);
    final BuPanel pnLineFromPt = new BuPanel();
    pnLineFromPt.setLayout(new BuHorizontalLayout(0));
    pnLineFromPt.add(cbImportLignesFromPt_);
    pnLineFromPt.add(cmbImpLinesFromPt_);
    pnLines.add(pnLineFromPt, BuFormLayout.constraint(0, 1, 3, 1, true, false, 0f, 0f));
    pnLines.add(new BuLabel(FSigLib.getS("Le calque de destination")), BuFormLayout.constraint(0, 2));

    filter = new ZModelClassLayerFilter(GISZoneCollectionLigneBrisee.class);
    dest_.apply(filter);
    res = filter.getCalques();
    model = new FSigWizardImportHelper.CalqueModel(res, CtuluLib.getS("Nouveau"));
    cmbDestLignes_ = new FSigWizardImportHelper.SpecCombo();
    cmbDestLignes_.setRenderer(render);
    model.addListDataListener(new ListDataListener() {

      @Override
      public void contentsChanged(final ListDataEvent _e) {
        tfNewLigneLayer_.setEnabled(cmbDestLignes_.getSelectedIndex() == 0);
      }

      @Override
      public void intervalAdded(final ListDataEvent _e) {
      }

      @Override
      public void intervalRemoved(final ListDataEvent _e) {
      }
    });
    cmbDestLignes_.setModel(model);
    // cmbDestLignes_.setEnabled(res.length > 0);
    pnLines.add(cmbDestLignes_, BuFormLayout.constraint(1, 2));
    tfNewLigneLayer_ = new BuTextField(10) {
      @Override
      public void setEnabled(final boolean _enabled) {
        super.setEnabled(_enabled && cmbDestLignes_.getSelectedIndex() == 0 && importLignes_.isOk());

      }
    };
    tfNewLigneLayer_.setEnabled(true);
    tfNewLigneLayer_.setText(EbliLib.getS("Lignes"));
    pnLines.add(tfNewLigneLayer_, BuFormLayout.constraint(2, 2, true, 1f));

    pnInfo_ = new FSigWizardImportHelper.InfoPanel();

    FSigWizardImportHelper.setTitleBorder(pnInfo_, BuResource.BU.getString("Informations"));
    add(pnInfo_);
    if (canAddLayer) {
      add(pnPoints);
      add(pnLines);
    }
    jcTranslate = new BuCheckBox(FSigLib.getS("Translater les donn�es"));
    final JPanel pnTranslate = new BuTitledPanel(jcTranslate, new BuGridLayout(2, 3, 3));
    CtuluLibSwing.addActionListenerForCheckBoxTitle(pnTranslate, jcTranslate);
    translate_ = new CtuluValueEditorDouble(false);
    tfTranslateX_ = translate_.createEditorComponent();
    translate_.setValue(getOldXValues(), tfTranslateX_);
    tfTranslateY_ = translate_.createEditorComponent();
    translate_.setValue(getOldYValues(), tfTranslateY_);
    jcTranslate.setSelected(false);
    pnTranslate.add(new BuLabel(FudaaLib.getS("En X:"))).setEnabled(false);
    pnTranslate.add(tfTranslateX_).setEnabled(false);
    pnTranslate.add(new BuLabel(FudaaLib.getS("En Y:"))).setEnabled(false);
    pnTranslate.add(tfTranslateY_).setEnabled(false);
    add(pnTranslate);
    JPanel pn = new BuPanel();
    FSigWizardImportHelper.setTitleBorder(pn, FSigLib.getS("Attributs"));
    pn.setLayout(new BuBorderLayout());
    scAttrs_ = new BuScrollPane();
    scAttrs_.setPreferredWidth(120);
    pn.add(scAttrs_);
    add(pn);
    cbImportLignes_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        updateLineFromPt();
      }

    });
  }

  protected boolean isTranslate() {
    return jcTranslate.isSelected();
  }

  protected GISPoint getTranslation() {
    if (isTranslate()) {
      final Object tx = translate_.getValue(tfTranslateX_);
      final Object ty = translate_.getValue(tfTranslateY_);
      if (tx != null && ty != null) {
        return new GISPoint(((Number) tx).doubleValue(), ((Number) ty).doubleValue(), 0);
      }
    }
    return null;
  }

  public double getOldXValues() {
    return FudaaPreferences.FUDAA.getDoubleProperty("export.translate.x", 0);
  }

  public void setOldXValues(final double _tx) {
    FudaaPreferences.FUDAA.putDoubleProperty("export.translate.x", _tx);
  }

  public void setOldYValues(final double _ty) {
    FudaaPreferences.FUDAA.putDoubleProperty("export.translate.y", _ty);
  }

  public double getOldYValues() {
    return FudaaPreferences.FUDAA.getDoubleProperty("export.translate.y", 0);
  }

  void updateLineFromPt() {
    final boolean first = importLignes_.isOk() && data_ != null && data_.getNbPoints() > 0;
    cbImportLignesFromPt_.setEnabled(first);
    if (first && data_.getNbPolygones() == 0 && data_ != null && data_.getNbPolylignes() == 0) {
      cbImportLignesFromPt_.setSelected(true);
      cbImportLignesFromPt_.setEnabled(false);
      cmbImpLinesFromPt_.setEnabled(true);

    } else {
      cmbImpLinesFromPt_.setEnabled(cbImportLignesFromPt_.isEnabled());
      cbImportLignesFromPt_.setSelected(false);
    }

  }

  public void setSrc(FSigGeomSrcData _data) {
    data_ = _data;
  }

  public void setMapper(FSigExportImportAttributesMapper _mapper) {
    mapper_ = _mapper;
  }

  public void setSelectedLayer(ZCalqueEditable _cq) {
    selectedLayer_ = _cq;
  }

  public void update() {
    if (data_ == null)
      return;

    pnInfo_.setSrc(data_);
    cbImportLignes_.setEnabled(data_.getNbPolygones() > 0 || data_.getNbPolylignes() > 0 || data_.getNbPoints() > 0);
    cbImportLignes_.setSelected(data_.getNbPolygones() > 0 || data_.getNbPolylignes() > 0);
    cbImportPoint_.setSelected(data_.getNbPoints() > 0);
    cbImportPoint_.setEnabled(data_.getNbPoints() > 0);
    importLignes_.updateState();
    importPoint_.updateState();

    if (mapper_ != null)
      scAttrs_.getViewport().add(mapper_.getTable());

    if (selectedLayer_ != null && cmbDestPoint_ != null) {
      int isel = 0;
      for (int i = 0; i < cmbDestPoint_.getItemCount(); i++)
        if (cmbDestPoint_.getItemAt(i).equals(selectedLayer_)) {
          isel = i;
          break;
        }
      //    } else {
      cmbDestPoint_.setSelectedIndex(isel);
    }
    if (selectedLayer_ != null && cmbDestLignes_ != null) {
      int isel = 0;
      for (int i = 0; i < cmbDestLignes_.getItemCount(); i++)
        if (cmbDestLignes_.getItemAt(i).equals(selectedLayer_)) {
          isel = i;
          break;
        }
      cmbDestLignes_.setSelectedIndex(isel);
    }

    updateLineFromPt();
  }

  public boolean isImportLineValid() {
    return importLignes_.isOk() || importPoint_.isOk();
  }

}
