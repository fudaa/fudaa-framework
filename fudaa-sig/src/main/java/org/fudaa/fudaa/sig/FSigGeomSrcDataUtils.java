/*
 * @creation 5 d�c. 06
 * @modification $Date: 2006-12-20 16:13:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuSplit2Pane;
import com.memoire.bu.BuToolBar;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * @author fred deniger
 * @version $Id: FSigGeomSrcDataUtils.java,v 1.2 2006-12-20 16:13:17 deniger Exp $
 */
public final class FSigGeomSrcDataUtils {

  private FSigGeomSrcDataUtils() {}

  public static JLabel buildLabel(final String _s) {
    final BuLabel r = new BuLabel(_s);
    r.setHorizontalAlignment(SwingConstants.LEFT);
    r.setHorizontalTextPosition(SwingConstants.LEFT);
    return r;
  }

  static Runnable buildRunnableForPrevisu(final Dialog _d, final CtuluUI _parent, final CtuluDialogPanel _dest,
      final BGroupeCalque _gc) {
    return new Runnable() {

      @Override
      public void run() {
        final BuPanel pnTop = new BuPanel(new BuBorderLayout());
        final ZEbliCalquesPanel pn = new ZEbliCalquesPanel(_gc, _parent);
        final EbliActionInterface[] act = pn.getController().getNavigationActions();
        final BuToolBar tb = new BuToolBar();
        if (act != null) {
          for (int i = 0; i < act.length; i++) {
            if (act[i] != null && !(act[i] instanceof EbliActionPaletteAbstract)) {
              tb.add(act[i].buildToolButton(EbliComponentFactory.INSTANCE));
            }
          }
        }
        pnTop.add(tb, BuBorderLayout.NORTH);
        final BArbreCalque arbre = new BArbreCalque(pn.getArbreCalqueModel(), true);
        arbre.setRootVisible(false);
        final BuScrollPane sp = new BuScrollPane(arbre);
        sp.setPreferredWidth(160);
        sp.setPreferredHeight(150);
        final BuSplit2Pane split = new BuSplit2Pane(pn, sp);
        pn.setPreferredSize(new Dimension(170, 200));
        split.resetToPreferredSizes();
        pnTop.add(split, BuBorderLayout.CENTER);
        _dest.add(pnTop);
        if (_d != null) {
          _d.dispose();
        }

        _dest.afficheModale(_parent.getParentComponent(), FSigLib.getS("Pr�visualisation"), CtuluDialog.OK_OPTION,
            new Runnable() {

          @Override
              public void run() {
                pn.restaurer();
              }
            });
      }
    };
  }

  static ZCalqueLigneBrisee buildPrevisuDomaineTotal(final Envelope _envConvex) {
    final ZCalqueLigneBrisee cq = new ZCalqueLigneBrisee(new FSigLineSingleModel(
        (LineString) GISGeometryFactory.INSTANCE.toGeometry(_envConvex)));
    cq.setTitle(EbliLib.getS("Emprise des donn�es import�es"));
    cq.setIconModel(0, null);
    cq.setForeground(Color.RED);
    return cq;
  }

  static ZCalqueLigneBrisee buildPrevisuDomaines(final Envelope _ptRing) {
    final ZCalqueLigneBrisee cq = new ZCalqueLigneBrisee(new FSigLineSingleModel(
        (LineString) GISGeometryFactory.INSTANCE.toGeometry(_ptRing)));
    final Color color = Color.GREEN;
    cq.setForeground(new Color(color.getRed(), color.getGreen(), color.getBlue(), 160));
    cq.getLineModel(0).setTypeTrait(TraceLigne.POINTILLE);
    cq.setTitle(EbliLib.getS("Emprise des points"));
    cq.setIconModel(0, null);
    return cq;
  }

  static ZCalqueLigneBrisee buildPrevisuCq(final GISZoneCollectionLigneBrisee _dest) {
    final ZCalqueLigneBrisee cq = new ZCalqueLigneBrisee(new ZModeleLigneBriseeDefault(_dest));
    cq.setForeground(new Color(Color.ORANGE.getRed(), Color.ORANGE.getGreen(), Color.ORANGE.getBlue(), 160));
    cq.getLineModel(0).setColor(cq.getForeground());
    cq.getLineModel(0).setTypeTrait(TraceLigne.POINTILLE);
    cq.getIconModel(0).setType(TraceIcon.RIEN);
    cq.setTitle(EbliLib.getS("Lignes"));
    return cq;
  }

}
