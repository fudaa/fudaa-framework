/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sig.exetools;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionDetailedInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gui.CtuluCellDialogEditor;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool.ParamI;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.layer.FSigLayerExporterI;
import org.fudaa.fudaa.sig.layer.FSigLayerFilterAbstract;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.Point;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Un param�tre fichier d'entr�e construit a partir de calques s�lectionn�s.
 *
 * @author marchand@deltacad.fr
 */
public class FSigInpFileParam extends ParamI {
  /**
   * Les exporters pour ce type de parametre
   */
  private static FSigLayerExporterI[] exporters_ = {
      new FSigLayerExporterI.ToRubar(),
      new FSigLayerExporterI.ToGML(),
      new FSigLayerExporterI.ToShape(),
      new FSigLayerExporterI.ToSinusX()
  };
  /**
   * Les noms associ�es. Ne pas modifier ces noms, ils sont sauv�s dans les pr�f�rences
   */
  private final static String[] EXP_NAMES = {
      "RubarSt",
      "GML",
      "Shape",
      "SinusX"
  };
  /**
   * Le root calque pour les fichiers d'entr�e.
   */
  private static BGroupeCalque bGroupeCalque;
  private static CtuluUI ctuluUI;
  private static CtuluCellDialogEditor editor;
  private ParamInpFileDefinitionPanel panel = new ParamInpFileDefinitionPanel();
  private String value;
  private String valSet;
  private int numExporter;

  @Override
  public FSigInpFileParam clone() {
    FSigInpFileParam o = new FSigInpFileParam();
    o.name = name;
    o.exeTool = exeTool;
    o.panel = panel;
    o.value = value;
    o.valSet = valSet;
    o.numExporter = numExporter;

    return o;
  }

  @Override
  public void setValue(String value) {
    this.value = value;
    for (int i = 0; i < EXP_NAMES.length; i++) {
      if (EXP_NAMES[i].equalsIgnoreCase(this.value)) {
        numExporter = i;
        break;
      }
    }
  }

  @Override
  public String getValue() {
    return value;
  }

  @Override
  public void setValSet(String val) {
    valSet = val;
  }

  @Override
  public String getValSet() {
    return valSet;
  }

  @Override
  public String getValParam() {
    // R�cup�ration de l'indice du param dans l'utilitaire
    int num = Arrays.asList(exeTool.getParams()).indexOf(this) + 1;
    return "finp" + num + "." + exporters_[numExporter].getFileFilter().getFirstExt();
  }

  @Override
  public boolean postLaunch(ProgressionDetailedInterface _prog) {
    return true;
  }

  public class ParamInpFileDefinitionPanel extends CtuluDialogPanel {
    JComboBox coInpFileValue_;

    public ParamInpFileDefinitionPanel() {
      coInpFileValue_ = new JComboBox(new String[]{""});
      for (FSigLayerExporterI exp : exporters_) {
        coInpFileValue_.addItem(exp.getFileFilter().getShortDescription());
      }
      setLayout(new BorderLayout(5, 0));
      add(new JLabel(FSigLib.getS("Format")), BorderLayout.WEST);
      add(coInpFileValue_, BorderLayout.CENTER);
    }

    @Override
    public boolean isDataValid() {
      if (coInpFileValue_.getSelectedIndex() == 0) {
        setErrorText(FSigLib.getS("Un format doit �tre choisi"));
        return false;
      }
      return true;
    }

    @Override
    public void setValue(Object _o) {
      FudaaExeTool.ParamI param = (FudaaExeTool.ParamI) _o;
      for (int i = 0; i < EXP_NAMES.length; i++) {
        if (EXP_NAMES[i].equalsIgnoreCase(param.getValue())) {
          coInpFileValue_.setSelectedIndex(i + 1);
        }
      }
    }

    @Override
    public FudaaExeTool.ParamI getValue() {
      FudaaExeTool.ParamI param = new FSigInpFileParam();
      param.setValue(EXP_NAMES[coInpFileValue_.getSelectedIndex() - 1]);
      return param;
    }
  }

  private class CalqueZoneGeometryVisitor extends FSigLayerFilterAbstract {
    private List<String> layerNames = new ArrayList<String>();

    public CalqueZoneGeometryVisitor(String[] _names) {
      layerNames.addAll(Arrays.asList(_names));
    }

    @Override
    public boolean visit(BCalque _cq) {
      if (_cq instanceof ZCalqueGeometry) {
        ZCalqueGeometry cq = (ZCalqueGeometry) _cq;
        if (layerNames.contains(_cq.getName())) {
          if (cq.modeleDonnees().getGeomData().getDataStoreClass().equals(Point.class)) {
            pointCq_.add(cq);
          } else if (LineString.class.isAssignableFrom(cq.modeleDonnees().getGeomData().getDataStoreClass())) {
            polyCq_.add(cq);
          } else if (MultiPoint.class.isAssignableFrom(cq.modeleDonnees().getGeomData().getDataStoreClass())) {
            mlptsCq_.add(cq);
          }
        }
      }
      return true;
    }

    @Override
    protected GISDataModel getCollect(ZCalqueAffichageDonneesInterface _cq) {
      return ((ZCalqueGeometry) _cq).modeleDonnees().getGeomData();
    }
  }

  public static void setCtuluUI(CtuluUI _ui) {
    ctuluUI = _ui;
    editor = null;
  }

  public static void setRoot(BGroupeCalque _root) {
    bGroupeCalque = _root;
    editor = null;
  }

  @Override
  public String getDefinitionCellText() {
    String nm = "";
    if (!"".equals(name)) {
      nm = FudaaLib.getS("Nom") + "=" + name + " ; ";
    }

    return nm + getLabel() + "/" + FSigLib.getS("Type={0}", value);
  }

  @Override
  public String getSetCellText() {
    return valSet == null ? "" : "[ " + FSigLib.getS("{0} calque(s) s�lectionn�(s)", "" + valSet.split("\\|").length) + " ]";
  }

  @Override
  public TableCellEditor getSetCellEditor() {
    if (editor == null) {
      CtuluDialog di = new FSigSetParamInputFilePanel(ctuluUI, bGroupeCalque).createDialog(ctuluUI.getParentComponent());
      di.setTitle(FSigLib.getS("Calques"));
      editor = new CtuluCellDialogEditor(di);
    }
    return editor;
  }

  /**
   * Transfert le contenu des calques s�lectionn�s vers le format de fichier attendu
   */
  @Override
  public boolean prepareLaunch(ProgressionDetailedInterface _prog) {
    // R�cup�ration de l'indice du param dans l'utilitaire
    int num = Arrays.asList(exeTool.getParams()).indexOf(this) + 1;

    // Recup�ration des zones g�om�tries
    String[] selLayers = valSet.split("\\|");
    CalqueZoneGeometryVisitor v = new CalqueZoneGeometryVisitor(selLayers);
    bGroupeCalque.apply(v);

    File outFile = new File(exeTool.exeDirPath, getValParam());
    if (_prog != null) {
      _prog.appendDetailln(FSigLib.getS("Ecriture du fichier {0}", outFile.getPath()));
    }

    CtuluIOOperationSynthese ret = exporters_[numExporter].exportTo(v, ctuluUI, outFile, null, null);
    if (ret.containsSevereError() && _prog != null) {
      _prog.appendDetailln(FSigLib.getS("Erreur d'�criture"));
      return false;
    }
    return true;
  }

  @Override
  public String getType() {
    return "INPFILE";
  }

  @Override
  public boolean mustBeSet() {
    return true;
  }

  @Override
  public String getLabel() {
    return FSigLib.getS("Fichier d'entr�e");
  }

  @Override
  public CtuluDialogPanel getDefinitionPanel() {
    return panel;
  }
}
