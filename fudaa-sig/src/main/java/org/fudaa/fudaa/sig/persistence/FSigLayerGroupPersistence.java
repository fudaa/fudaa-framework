/*
 *  @creation     25 ao�t 2005
 *  @modification $Date: 2007-06-11 12:53:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.persistence;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalquePersistenceGroupe;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Une classe permettant la sauvegarde des calques SIG.
 * 
 * @author Fred Deniger
 * @version $Id: FSigLayerGroupPersistence.java,v 1.5 2007-06-11 12:53:36 deniger Exp $
 */
public class FSigLayerGroupPersistence extends BCalquePersistenceGroupe {

  public static String getAttributeId() {
    return "sig.attributes";
  }

  /*
   * public static String getMainId() { return "siglayer.isMain"; }
   */

  public FSigLayerGroupPersistence() {

  }

  @Override
  public String restoreFrom(final CtuluArkLoader _loader, final BCalqueSaverTargetInterface _parentPanel,
      final BCalque _parentCalque, final String _parentDirEntry, final String _entryName,
      final ProgressionInterface _proj) {
    if (FSigLayerPointPersistence.isNoAddSigLayer(_loader)) {
      return null;
    }
    return super.restoreFrom(_loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
  }

  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FSI: " + getClass().getName() + " restore group " + _saver.getLayerName());
    }
    if (!(_pn instanceof FSigVisuPanel)) {
      FuLog.warning("FSI: the save data for gis group is not valid" + _pn.getClass());
      return null;
    }
    final FSigVisuPanel mvVisuPanel = ((FSigVisuPanel) _pn);
    FSigLayerGroup grGis = mvVisuPanel.getGroupGIS();
    final boolean isMain = _parent == mvVisuPanel.getDonneesCalque();
    if (isMain) {
      return grGis;
    } else if (_parent instanceof FSigLayerGroup) {
      grGis = (FSigLayerGroup) _parent;
    }
    // on cree le calque, et on restore ces attributs
    final ZCalqueEditionGroup thisGroup = grGis.addGroupAct(_saver.getUI().getTitle(), grGis, false,
        (GISAttributeInterface[]) _saver.getUI().get(getAttributeId()));
    if (thisGroup == null) {
      FuLog.warning("FSI: new group layer can't be built");
      return null;
    }
    return thisGroup;
  }

  @Override
  public BCalqueSaverInterface save(final BCalque _cq, final ProgressionInterface _prog) {
    if (_cq == null) {
      return null;
    }
    final BCalqueSaverInterface res = super.save(_cq, _prog);
    final ZCalqueEditionGroup sigLayer = (ZCalqueEditionGroup) _cq;
    res.getUI().put(getAttributeId(), sigLayer.getAttributes());
    return res;
  }

  @Override
  protected void writeHeaderData(final CtuluXmlWriter _out, final BCalque _cqToSave) throws IOException {
    final ZCalqueEditionGroup sigLayer = (ZCalqueEditionGroup) _cqToSave;
    final GISAttributeInterface[] att = sigLayer.getAttributes();
    if (att != null && att.length > 0) {
      _out.write('<');
      _out.write(getAttributeId());
      _out.write('>');
      final int nb = att.length;
      for (int i = 0; i < nb; i++) {
        _out.writeEntry("attribute", GISAttributeConstants.toString(att[i]));
      }
      _out.write("</");
      _out.write(getAttributeId());
      _out.write('>');
    }
  }

  @Override
  public void readXmlSpecific(final CtuluXmlReaderHelper _helper, final BCalqueSaverInterface _target) {
    final NodeList list = _helper.getDoc().getElementsByTagName("attribute");
    if (list != null && list.getLength() > 0) {
      final int nb = list.getLength();
      final List att = new ArrayList(nb);
      for (int i = 0; i < nb; i++) {
        final Node nodei = list.item(i);
        // il possede du texte
        if (nodei.getChildNodes().getLength() > 0) {
          final Node item = nodei.getFirstChild();
          String nodeValue = item.getNodeValue();
          if (nodeValue != null) {
            nodeValue = nodeValue.trim();
          }
          final GISAttributeInterface atti = GISAttributeConstants.restoreFrom(nodeValue);
          if (atti != null) {
            att.add(atti);
          }
        }
      }
      _target.getUI().put(getAttributeId(), att.toArray(new GISAttributeInterface[att.size()]));
    }
    /*
     * String isMain = _helper.getTextFor(getMainId()); _target.getUI().put(getMainId(),
     * CtuluLibString.toBoolean(isMain));
     */

  }
}
