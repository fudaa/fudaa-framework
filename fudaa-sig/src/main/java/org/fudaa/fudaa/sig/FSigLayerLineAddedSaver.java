/*
 *  @creation     26 ao�t 2005
 *  @modification $Date: 2006-09-19 15:10:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.BCalqueSaverSingle;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.sig.persistence.FSigLayerLinePersistence;
import org.fudaa.fudaa.sig.persistence.FSigLayerPointPersistence;

/**
 * Laisse pour les anciennes sauvegardes.
 * 
 * @deprecated tout est dans FSigLayerLinePersistence
 * @author Fred Deniger
 * @version $Id: FSigLayerLineAddedSaver.java,v 1.7 2006-09-19 15:10:21 deniger Exp $
 */
public class FSigLayerLineAddedSaver extends BCalqueSaverSingle {

  GISZoneCollectionLigneBrisee zone_;

  /**
   * @deprecated tout est dans FSigLayerLinePersistence
   */
  protected FSigLayerLineAddedSaver() {
    setPersistenceClass(FSigLayerLinePersistence.class.getName());
  }

  @Override
  public EbliUIProperties getUI() {
    final EbliUIProperties ui = super.getUI();
    if (zone_ != null) {
      ui.put(FSigLayerPointPersistence.getGeomId(), zone_);
      zone_ = null;
    }
    return ui;

  }

}
