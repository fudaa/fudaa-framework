/*
 *  @creation     26 ao�t 2005
 *  @modification $Date: 2008-03-26 16:46:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.persistence;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;

/**
 * Une presistence pour les multipoints.
 * @author Bertrnad Marchand
 * @version $Id: FSigLayerMultiPointPersistence.java,v 1.1.2.1 2008-03-26 16:46:44 bmarchan Exp $
 */
public class FSigLayerMultiPointPersistence extends FSigLayerPointPersistence {

  public FSigLayerMultiPointPersistence() {}


  @Override
  protected GISZoneCollection createInitCollection() {
    return new GISZoneCollectionMultiPoint();
  }

  @Override
  protected BCalque addInParent(final FSigLayerGroup _gr, final BCalqueSaverInterface _cqName, final GISZoneCollection _collection) {
    throw new UnsupportedOperationException("This methode is not implemented for Multipoint");
  }

  

}
