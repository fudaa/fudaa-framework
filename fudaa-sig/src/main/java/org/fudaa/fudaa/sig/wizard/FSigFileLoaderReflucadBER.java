/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2007/04/30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.reflux.io.ReflucadBERFileFormat;

/**
 * Un lecteur de fichier Reflucad au format BER (berges) contenant des objets g�om�triques
 * de type lignes.
 * 
 * @author Bertrand Marchand
 * @version $Id: FSigFileLoaderSinusX.java,v 1.3 2007/04/30 14:22:38 deniger Exp $
 */
public class FSigFileLoaderReflucadBER implements FSigFileLoaderI {

  /** Le filtre sp�cifique aux fichiers Reflucad BER */
  protected final BuFileFilter ft_;
  /** Les lignes contenues dans le fichier */
  protected GISZoneCollectionLigneBrisee lines_;

  /**
   * Construction d'un chargeur avec filtre de fichier par d�faut.
   */
  public FSigFileLoaderReflucadBER() {
    super();
    ft_ = ReflucadBERFileFormat.getInstance().createFileFilter();
  }

  public FSigFileLoaderReflucadBER(final BuFileFilter _ft) {
    ft_ = _ft;
  }

  /* @override */
  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderReflucadBER(ft_);
  }

  /* @override */
  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  /* @override */
  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final CtuluIOOperationSynthese op=ReflucadBERFileFormat.getInstance().read(_f, _prog);
    // En cas d'erreur, fin d'import.
    if (op.containsMessages()&&op.getAnalyze().containsErrors()) {
      _analyze.merge(op.getAnalyze());
      return;
    }

    lines_=(GISZoneCollectionLigneBrisee)op.getSource();
    _r.nbPointTotal_+=lines_.getNumPoints();
    _r.nbPolylignes_+=lines_.getNumGeometries();
    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(lines_);
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    // 
    _r.ligneModel_.add(adapter);
    _r.addUsedAttributes(GISLib.getAttributeFrom(lines_));
  }
}
