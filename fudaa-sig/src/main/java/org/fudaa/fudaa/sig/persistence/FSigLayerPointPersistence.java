/*
 *  @creation     26 ao�t 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.persistence;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gis.exporter.GISDataStoreZoneExporter;
import org.fudaa.ctulu.gis.exporter.GMLMapDecoder;
import org.fudaa.ctulu.gis.exporter.GMLReader;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.fudaa.commun.save.FudaaSaveLib;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureSource;
import org.geotools.data.geojson.store.GeoJSONDataStore;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.store.ContentDataStore;
import org.geotools.feature.FeatureCollection;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Fred Deniger
 */
public class FSigLayerPointPersistence extends BCalquePersistenceSingle {
  private static final String EXTENSION_GEOJSON = ".geojson";
  private static final String EXTENSION_GML = ".gml";
  private static final String EXTENSION_SHP = ".shp";
  private boolean prepareExportBeforeSave;

  public FSigLayerPointPersistence() {
  }

  @Override
  public BCalqueSaverInterface save(final BCalque calque, final ProgressionInterface progressionInterface) {
    final BCalqueSaverInterface save = super.save(calque, progressionInterface);
    final GISZoneCollection geomData = getGeomData(calque);
    geomData.prepareExport();
    save.getUI().put(getGeomId(), geomData);
    return save;
  }

  /**
   * Sauvegarde des entites g�ometriques sur entry .gml.
   */
  @Override
  public BCalqueSaverInterface saveIn(final BCalque cqToSave, final CtuluArkSaver ctuluArkSaver,
                                      final String parentDirEntry, final String parentDirIndice) {
    final BCalqueSaverSingle res = (BCalqueSaverSingle) super.saveIn(cqToSave, ctuluArkSaver, parentDirEntry,
        parentDirIndice);

    Path directory = null;
    try {
      directory = Files.createTempDirectory("export");
      File geoJson = new File(directory.toFile(), "export.geojson");
      DataStore store = new GeoJSONDataStore(geoJson.toURI().toURL());
      final GISZoneCollection geomData = getGeomData(cqToSave);
      if (prepareExportBeforeSave) {
        geomData.prepareExport();
      }
      createExporter().process(null, geomData, null, store, geomData.getDataStoreClass());
      final String entry = getEntryBase(cqToSave, parentDirEntry, res.getId()) + EXTENSION_GEOJSON;
      ctuluArkSaver.startEntry(entry);
      try (final FileInputStream inputStream = new FileInputStream(geoJson)) {
        CtuluLibFile.copyStream(inputStream, ctuluArkSaver.getOutStream(), true, false);
        ctuluArkSaver.getOutStream().flush();
      }
    } catch (Exception ex) {
      Logger.getLogger(FSigLayerPointPersistence.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      if (directory != null) {
        CtuluLibFile.deleteDir(directory.toFile());
      }
    }

    return res;
  }

  protected GISDataStoreZoneExporter createExporter() {
    return new GISDataStoreZoneExporter();
  }

  private GISZoneCollection getGeomData(final BCalque cqToSave) {
    return ((ZModeleGeometry) ((ZCalqueAffichageDonneesInterface) cqToSave).modeleDonnees()).getGeomData();
  }

  protected GISZoneCollection createInitCollection() {
    return new GISZoneCollectionPoint();
  }

  protected final GISZoneCollection createCollection(final GISDataModel dataModel) {
    final GISZoneCollection pt = createInitCollection();
    pt.setAttributes(GISLib.getAttributeFrom(dataModel), null);
    pt.addAll(dataModel, null, false);
    return pt;
  }

  @Override
  public String restoreFrom(final CtuluArkLoader ctuluArkLoader, final BCalqueSaverTargetInterface parentPanel,
                            final BCalque parentCalque, final String _parentDirEntry, final String entryname,
                            final ProgressionInterface progressionInterface) {
    if (isNoAddSigLayer(ctuluArkLoader)) {
      return null;
    }
    return super.restoreFrom(ctuluArkLoader, parentPanel, parentCalque, _parentDirEntry, entryname, progressionInterface);
  }

  private boolean readGML(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader, final String _parentDirEntry, final String _entryName) {
    boolean r = false;
    try (final InputStream inputStream = getGMLInputStream(_loader, _parentDirEntry, _entryName)) {
      if (inputStream == null) {
        return true;
      }
      final Map decode = new GMLReader().decode(getGMLInputStream(_loader, _parentDirEntry, _entryName));
      if (decode != null) {
        _saver.getUI().put(getGeomId(), new GMLMapDecoder(decode, true).decode());
      }
      r = true;
    } catch (final Exception _evt) {
      FuLog.error(_evt);
    }
    return r;
  }

  private boolean readShapefile(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader, final String _parentDirEntry, final String _entryName) throws IOException {
    File shp = null;
    boolean r = false;
    try (InputStream shapeInputStream = getShapeInputStream(_loader, _parentDirEntry, _entryName)) {
      if (shapeInputStream != null) {
        shp = File.createTempFile("tmp", EXTENSION_SHP);
        CtuluLibFile.copyStream(shapeInputStream, new FileOutputStream(shp), false, true);
        ShapefileDataStore store = new ShapefileDataStore(shp.toURI().toURL());
        readContentDataStore(_saver, store);

        r = true;
      }
    } finally {
      if (shp != null) {
        shp.delete();
      }
    }
    return r;
  }

  private boolean readGeoJson(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader, final String _parentDirEntry, final String _entryName) {
    File geoJson = null;
    boolean r = false;
    try (InputStream geoJsonInputStream = getGeoJsonInputStream(_loader, _parentDirEntry, _entryName)) {
      if (geoJsonInputStream != null) {
        geoJson = File.createTempFile("tmp", EXTENSION_GEOJSON);
        CtuluLibFile.copyStream(geoJsonInputStream, new FileOutputStream(geoJson), false, true);
        if (geoJson.length() == 0) {
          return false;
        }
        GeoJSONDataStore store = new GeoJSONDataStore(geoJson.toURI().toURL());
        store.setGeometryFactory(GISGeometryFactory.INSTANCE);
        readContentDataStore(_saver, store);
        r = true;
      }
    } catch (IOException ex) {
      FuLog.error(ex);
    } finally {
      if (geoJson != null) {
        geoJson.delete();
      }
    }
    return r;
  }

  private void readContentDataStore(BCalqueSaverInterface _saver, ContentDataStore store) throws IOException {
    store.setGeometryFactory(GISGeometryFactory.INSTANCE);
    SimpleFeatureType schema = store.getSchema(store.getTypeNames()[0]);
    final String[] name = store.getTypeNames();
    if (name == null || name.length == 0) {
      throw new IOException("name not found");
    }
    final String typeName = schema.getTypeName();
    FeatureSource featureSource = store.getFeatureSource(typeName);
    final FeatureCollection feature = featureSource.getFeatures();
    _saver.getUI().put(getGeomId(), createCollection(GISDataModelFeatureAdapter.load(feature)));
  }

  @Override
  protected boolean restoreFromSpecific(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader,
                                        final BCalqueSaverTargetInterface _parentPanel, final BCalque _parentCalque, final String _parentDirEntry,
                                        final String _entryName, final ProgressionInterface _proj) {
    if (isShapeInputStream(_loader, _parentDirEntry, _entryName)) {
      try {
        return readShapefile(_saver, _loader, _parentDirEntry, _entryName);
      } catch (Exception e) {
        FuLog.error(e);
        return false;
      }
    } else if (isGMLInputStream(_loader, _parentDirEntry, _entryName)) {
      return readGML(_saver, _loader, _parentDirEntry, _entryName);
    }
    if (isGeoJsonInputStream(_loader, _parentDirEntry, _entryName)) {
      try {
        return readGeoJson(_saver, _loader, _parentDirEntry, _entryName);
      } catch (Exception e) {
        FuLog.error(e);
      }
    }
    return false;
  }

  private InputStream getGMLInputStream(final CtuluArkLoader _loader, final String _parentDirEntry,
                                        final String _entryName) {
    return _loader.getReader(_parentDirEntry, _entryName + EXTENSION_GML);
  }

  private boolean isGMLInputStream(final CtuluArkLoader _loader, final String _parentDirEntry,
                                   final String _entryName) {
    return _loader.isEntryFound(_parentDirEntry, _entryName + EXTENSION_GML);
  }

  private InputStream getShapeInputStream(final CtuluArkLoader _loader, final String _parentDirEntry,
                                          final String _entryName) {
    return _loader.getReader(_parentDirEntry, _entryName + EXTENSION_SHP);
  }

  private boolean isShapeInputStream(final CtuluArkLoader _loader, final String _parentDirEntry,
                                     final String _entryName) {
    return _loader.isEntryFound(_parentDirEntry, _entryName + EXTENSION_SHP);
  }

  private InputStream getGeoJsonInputStream(final CtuluArkLoader _loader, final String _parentDirEntry,
                                            final String _entryName) {
    return _loader.getReader(_parentDirEntry, _entryName + EXTENSION_GEOJSON);
  }

  private boolean isGeoJsonInputStream(final CtuluArkLoader _loader, final String _parentDirEntry,
                                       final String _entryName) {
    return _loader.isEntryFound(_parentDirEntry, _entryName + EXTENSION_GEOJSON);
  }

  protected BCalque addInParent(final FSigLayerGroup _gr, final BCalqueSaverInterface _cqName,
                                final GISZoneCollection _collection) {
    return _gr.addPointLayerAct(_cqName.getUI().getTitle(), (GISZoneCollectionPoint) _collection, null);
  }

  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn,
                               final BCalque _parent) {
    if (!(_parent instanceof ZCalqueEditionGroup)) {
      return super.findCalque(_cqName, _pn, _parent);
    }
    final FSigLayerGroup grGis = (FSigLayerGroup) _parent;
    final GISZoneCollection zone = (GISZoneCollection) _cqName.getUI().get(FSigLayerPointPersistence.getGeomId());
    verifyGISCollection(grGis, zone);
    return addInParent(grGis, _cqName, zone);
  }

  public static boolean isNoAddSigLayer(final CtuluArkLoader _loader) {
    if (BCalquePersistenceGroupe.isNoAddLayer(_loader)) {
      return true;
    }
    return CtuluLibString.toBoolean(_loader.getOption(FudaaSaveLib.getNoGeogLayerOpt()));
  }

  private static void verifyGISCollection(final FSigLayerGroup _gr, final GISZoneCollection _collection) {
    _collection.initListeners(_gr.getGisEditor());
    if (Fu.DEBUG && FuLog.isDebug()) {
      final GISAttributeInterface[] att = _gr.getAttributes();
      if (att == null && _collection.getNbAttributes() != 0) {
        FuLog.error("FTR: attribute are not saved correcty", new Throwable());
      }
      if (!Arrays.equals(att, _collection.getAttributes())) {
        _collection.setAttributes(att, null);
      }
      _collection.postImport(0);
    }
  }

  public static String getGeomId() {
    return "sigLayer.geom";
  }

  public boolean isPrepareExportBeforeSave() {
    return prepareExportBeforeSave;
  }

  public void setPrepareExportBeforeSave(boolean prepareExportBeforeSave) {
    this.prepareExportBeforeSave = prepareExportBeforeSave;
  }
}
