/*
 * @creation     8 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.exetools;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISDataModelAttributeSubstitutionAdapter;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISDataModelMultiPointToPolyligneAdapter;
import org.fudaa.ctulu.gis.GISDataModelPointToMultiPointAdapter;
import org.fudaa.ctulu.gis.GISDataModelPolygoneToMultiPointAdapter;
import org.fudaa.ctulu.gis.GISDataModelPolyligneToMultiPointAdapter;
import org.fudaa.ctulu.gis.GISDataModelPolyligneToPolygoneAdapter;
import org.fudaa.ctulu.gis.GISDataModelSelectionAdapter;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.CalqueSelectorVisitor;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesAbstract;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.fudaa.sig.FSigExportImportAttributesMapper;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.wizard.FSigWizardImportHelper;

/**
 * L'�tape de choix des calques destinations lors de l'import de fichiers de donn�es.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class FSigImportDestinationPanel extends JPanel implements CellEditorListener, ListSelectionListener {
  
  /**
   * Modele utilis� par la table d'import.
   * @author Emmanuel MARTIN
   * @version $Id$
   */
  protected class ModelTableImport extends AbstractTableModel{
        
    private final String[] titreColonnes_;
    private final Object[][] data_;
    
    public ModelTableImport(String[] _columnTitles, Object[][] _data){
      if(_columnTitles==null||_data==null)
        throw new IllegalArgumentException("Erreur prog : Les arguments ne doivent pas �tre null.");
      if(_columnTitles.length!=4)
        throw new IllegalArgumentException("Erreur prog : Il doit y avoir 4 colonnes.");
      // Verification de la doh�rence des donn�es \\
      int rowSize=_columnTitles.length;
      // Parcours ligne par ligne
      for (int i=0; i<_data.length; i++) {
        if(_data[i]==null)
          throw new IllegalArgumentException("Erreur prog : _data ne doit pas contenir de case null.");
        if(rowSize!=_data[i].length)
          throw new IllegalArgumentException("Erreur prog : Toutes les lignes doivent avoir la m�me taille.");
        // Verification de la coh�rence des calques
        if(!(_data[i][_data[i].length-1] instanceof BCalque))
          throw new IllegalArgumentException("Erreur prog : La derni�re colonne doit contenir un calques pour l'importation.");
      }
      
      titreColonnes_ = _columnTitles;
      data_ = _data;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      switch(columnIndex){
        case 0: return String.class;
        case 1: return String.class;
        case 2: return Boolean.class;
        case 3: return BCalque.class;
        default: return null;
      }
    }

    @Override
    public int getColumnCount() {
      return titreColonnes_.length;
    }
    
    @Override
    public String getColumnName(int _columnIndex) {
      if(_columnIndex>=0 && _columnIndex<titreColonnes_.length)
        return titreColonnes_[_columnIndex];
      else
        return new String();
    }

    @Override
    public int getRowCount() {
      return data_.length;
    }

    @Override
    public Object getValueAt(int _rowIndex, int _columnIndex) {
      if (_rowIndex>=0&&_rowIndex<data_.length&&_columnIndex>=0&&_columnIndex<titreColonnes_.length) 
        return data_[_rowIndex][_columnIndex];
      else
        return new String();
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      switch(columnIndex){
        case 0:  return false;
        case 1:  return false;
        case 2:  return true;
        case 3:  return true;
        default: return false;
      }
    }

    @Override
    public void setValueAt(Object value, int _rowIndex, int _columnIndex) {
      if (_rowIndex>=0&&_rowIndex<data_.length&&_columnIndex>=0&&_columnIndex<titreColonnes_.length) 
          data_[_rowIndex][_columnIndex] = value;
    }
  }
  
  /**
   * Classe utilis� pour le rendu des cellules.
   * @author Emmanuel MARTIN
   * @version $Id$
   */
  public class ImportTableRenderer implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable _table, Object _value, boolean _isSelected, boolean _hasFocus, int _row,
        int _column) {
      Color selectionBackground = UIManager.getColor("Table.selectionBackground");
      JPanel pn=new JPanel();
      pn.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
      if(_isSelected)
        pn.setBackground(selectionBackground);
      else
        pn.setBackground(Color.WHITE);
      
      BuLabel lbl;
      switch (_column) {
      case 1:
        lbl =new BuLabel(_value.toString());
        lbl.setOpaque(true);
        lbl.setHorizontalAlignment(BuLabel.CENTER);
        if(_isSelected)
          lbl.setBackground(selectionBackground);
        pn.add(lbl);
        return pn;

      case 2:
        BuCheckBox cb=new BuCheckBox("", (Boolean)_value);
        if(_isSelected)
          cb.setBackground(selectionBackground);
        else
          cb.setBackground(Color.WHITE);
        pn.add(cb);
        return pn;
        
      case 3:
        String value;
        if (_value==null) 
          value=FSigLib.getS("< Nouveau >");
        else
          value=((BCalque)_value).getTitle();
        lbl =new BuLabel(value);
        lbl.setHorizontalAlignment(BuLabel.CENTER);
        if(_isSelected)
          lbl.setBackground(selectionBackground);
        pn.add(lbl);
        return pn;

      default:
        lbl =new BuLabel(_value.toString());
        lbl.setHorizontalAlignment(BuLabel.CENTER);
        if(_isSelected)
          lbl.setBackground(selectionBackground);
        pn.add(lbl);
        return pn;
      }
    }
  }
  
  /**
   * Classe utilis�e pour l'edition avec des comboBox.
   * @author Emmanuel MARTIN
   * @version $Id$
   */
  public class ImportTableEditorForBCalqueSelection extends AbstractCellEditor implements TableCellEditor, ActionListener {
   
    private JComboBox cb_;
    
    public ImportTableEditorForBCalqueSelection(){
      cb_ = new JComboBox();
      cb_.addActionListener(this);
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable _table, Object _value, boolean _isSelected, int _row, int _column) {
      cb_.removeAllItems();
      for (int i=0; i<allDestLayers_.length; i++) {
        // On ajoute le num�ro d'ordre:
        // 1. pour que l'utilisateur s'y retrouve 
        // 2. Car sinon en cas de String identique, le setSelectedIndex ne fonctionne pas ??!!
        cb_.addItem((i+1)+". "+allDestLayers_[i]);
        if (allDestLayers_[i]==_value) {
          cb_.setSelectedIndex(i);
        }
      }
      return cb_;
    }
    
    @Override
    public Object getCellEditorValue() {
      return allDestLayers_[cb_.getSelectedIndex()];
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      stopCellEditing();
    }
  }
  
  // Les calques
  private ZCalqueEditable selectedLayer_;
  private BGroupeCalque racine_;
  /** Tous les calques destination possibles */
  private ZCalqueAffichageDonneesAbstract[] allDestLayers_;
  
  // Les donn�es fournies par le panel de chargement des fichiers
  private FSigGeomSrcData data_;
  
  // Donn�es g�n�r�es
  /** Association entre la ligne du tableau => le calque et la map. */
  private HashMap<Integer, Object[]> mappers_;
  
  // Les frames internes
  private FSigWizardImportHelper.InfoPanel pnInfo_;
  private CtuluTable table_;
  private BuScrollPane scAttrs_;
  
  public FSigImportDestinationPanel(BCalque _racine, FSigGeomSrcData _data, ZCalqueEditable _selectedLayer) {
    mappers_=new HashMap<Integer, Object[]>();
    data_=_data;
    selectedLayer_=_selectedLayer;
    racine_=(BGroupeCalque)_racine;

    // R�cup�ration des calques \\
    CalqueSelectorVisitor visitor = new CalqueSelectorVisitor(ZModeleGeometry.class);
    racine_.apply(visitor);
    allDestLayers_= visitor.getSelectedCalques();

    setLayout(new BuVerticalLayout(2, true, true));
    // Frame d'information \\
    pnInfo_ = new FSigWizardImportHelper.InfoPanel();
    FSigWizardImportHelper.setTitleBorder(pnInfo_, FSigLib.getS("Informations"));
    add(pnInfo_);
    
    // Frame d'importation \\
    // Le tableau
    table_ = new CtuluTable(new ModelTableImport(generateTitleColumnTable(), generateDataTable()));
    // Cr�ation du panel
    JPanel pnImport = new BuPanel();
    pnImport.setLayout(new BuBorderLayout());
    FSigWizardImportHelper.setTitleBorder(pnImport, FSigLib.getS("Importation"));
    pnImport.add(new BuScrollPane(table_), BuBorderLayout.CENTER);
    pnImport.setPreferredSize(new Dimension(10, 200));
    add(pnImport);
    
    // Frame des attributs \\
    JPanel pn = new BuPanel();
    FSigWizardImportHelper.setTitleBorder(pn, FSigLib.getS("Attributs"));
    pn.setLayout(new BuBorderLayout());
    scAttrs_=new BuScrollPane();
    scAttrs_.setPreferredSize(new Dimension(400, 150));
    pn.add(scAttrs_);
    add(pn);
    
    update();
  }
  
  protected String[] generateTitleColumnTable(){
    return new String[]{
        FSigLib.getS("G�om�trie"),
        FSigLib.getS("Type"),
        FSigLib.getS("Import"),
        FSigLib.getS("Calque")
    };
  }
  
  /**
   * Genere les datas de la table de mapping des g�om�tries, contenant entre autres les calques de destination.
   * @return Les datas de la table.
   */
  protected Object[][] generateDataTable() {
    if (data_==null)
      return new Object[0][];
    // Extraction des Informations sur les g�om�tries \\
    // Conteneur des informations extraites
    ArrayList<String[]> geometrieAndType=new ArrayList<String[]>();
    // Variables utiles seulement pendant l'extraction
    HashSet<String> natures=new HashSet<String>();
    int indexAttr;
    // Analyse des points
    if (data_.getNbPoints()>0) {
      natures.clear();
      // Recherche des natures des points
      for (int i=0; i<data_.getPoints().length; i++) {
        indexAttr=data_.getPoints()[i].getIndiceOf(GISAttributeConstants.NATURE);
        if (indexAttr==-1)
          natures.add("");
        else {
          for (int j=0; j<data_.getPoints()[i].getNumGeometries(); j++)
            natures.add((String)data_.getPoints()[i].getValue(indexAttr, j));
        }
      }
      // Enregistrement des natures trouv�es
      for (Iterator<String> it=natures.iterator(); it.hasNext();)
        geometrieAndType.add(new String[]{FSigLib.getS("MultiPoint"), it.next()});
    }
    // Analyse des polylignes
    if (data_.getNbPolylignes()>0) {
      natures.clear();
      // Recherche des natures des polylignes
      for (int i=0; i<data_.getPolylignes().length; i++) {
        indexAttr=data_.getPolylignes()[i].getIndiceOf(GISAttributeConstants.NATURE);
        if (indexAttr==-1)
          natures.add("");
        else {
          for (int j=0; j<data_.getPolylignes()[i].getNumGeometries(); j++)
            natures.add((String)data_.getPolylignes()[i].getValue(indexAttr, j));
        }
      }
      // Enregistrement des natures trouv�es
      for (Iterator<String> it=natures.iterator(); it.hasNext();)
        geometrieAndType.add(new String[]{FSigLib.getS("Polyligne"), it.next()});
    }
    // Analyse des polygones
    if (data_.getNbPolygones()>0) {
      natures.clear();
      // Recherche des natures des polygones
      for (int i=0; i<data_.getPolygones().length; i++) {
        indexAttr=data_.getPolygones()[i].getIndiceOf(GISAttributeConstants.NATURE);
        if (indexAttr==-1)
          natures.add("");
        else {
          for (int j=0; j<data_.getPolygones()[i].getNumGeometries(); j++)
            natures.add((String)data_.getPolygones()[i].getValue(indexAttr, j));
        }
      }
      // Enregistrement des natures trouv�es
      for (Iterator<String> it=natures.iterator(); it.hasNext();)
        geometrieAndType.add(new String[]{FSigLib.getS("Polygone"), it.next()});
    }

    // G�n�ration du model de donn� du tableau \\
    Object[][] dataTable=new Object[geometrieAndType.size()][];
    for (int i=0; i<geometrieAndType.size(); i++) {
      dataTable[i]=new Object[]{geometrieAndType.get(i)[0], // Nature
          geometrieAndType.get(i)[1], // Type g�om�trie
          true, // Import selectionn�
          getDefaultCalque(geometrieAndType.get(i)[1],geometrieAndType.get(i)[0]), // Calque par defaut
      };
    }

    return dataTable;
  }

  /**
   * @param _nature
   *          La nature que doit g�rer le calque.
   * @param _geometrie
   *          La g�om�trie que doit contenir le calque.
   * @return le claque par defaut pour la nature et la g�om�trie donn�es.
   */
  protected BCalque getDefaultCalque(String _nature, String _geometrie){
    // Tentative de d�termination avec la nature
    if(_nature!=null&&_nature.length()!=0){
      ArrayList<BCalque> found = new ArrayList<BCalque>();
      for(int i=0;i<allDestLayers_.length;i++)
        if(((ZModeleGeometry)allDestLayers_[i].modeleDonnees()).getGeomData().getFixedAttributValue(GISAttributeConstants.NATURE)== _nature)
          found.add(allDestLayers_[i]);
      if(found.size()>0)
        if(found.contains(selectedLayer_))
          return (BCalque) selectedLayer_;
        else
          return found.get(0);
    }
    // Tentative de d�termination avec la g�om�trie
    if (_geometrie!=null&&_geometrie.length()!=0) {
      ArrayList<BCalque> found=new ArrayList<BCalque>();
      for (int i=0; i<allDestLayers_.length; i++) {
        if (FSigLib.getS("MultiPoint").equals(_geometrie)) {
          if (((ZCalqueEditable)allDestLayers_[i]).canAddForme(DeForme.MULTI_POINT))
            found.add(allDestLayers_[i]);
        }
        else if (FSigLib.getS("Polyligne").equals(_geometrie)) {
          if (((ZCalqueEditable)allDestLayers_[i]).canAddForme(DeForme.LIGNE_BRISEE))
            found.add(allDestLayers_[i]);
        }
        else if (FSigLib.getS("Polygone").equals(_geometrie)) {
          if (((ZCalqueEditable)allDestLayers_[i]).canAddForme(DeForme.POLYGONE))
            found.add(allDestLayers_[i]);
        }
      }
      if (found.size()>0)
        if(found.contains(selectedLayer_))
          return (BCalque) selectedLayer_;
        else
          return found.get(0);
    }
    // A defaut retourne le calque selectionn� si il est dans la liste
    for(int i=0;i<allDestLayers_.length;i++)
      if(allDestLayers_[i]==selectedLayer_)
        return (BCalque) selectedLayer_;
    // Il reste plus qu'a retourner un claque arbitrairement si il y en a un qui existe
    if(allDestLayers_.length>0)
      return allDestLayers_[0];
    else
      return null;
  }
  
  public void setSrc(FSigGeomSrcData _data) {
    data_=_data;
  }
  
  public void setSelectedLayer(ZCalqueEditable _cq) {
    selectedLayer_=_cq;
  }
  
  public void update() {
    if (data_==null) return;
    // Update du panel d'info
    pnInfo_.setSrc(data_);
    // Update du panel d'import
    table_.setModel(new ModelTableImport(generateTitleColumnTable(), generateDataTable()));
    table_.getColumnModel().getColumn(0).setCellRenderer(new ImportTableRenderer());
    table_.getColumnModel().getColumn(0).setPreferredWidth(80);
    table_.getColumnModel().getColumn(1).setCellRenderer(new ImportTableRenderer());
    table_.getColumnModel().getColumn(1).setPreferredWidth(40);
    table_.getColumnModel().getColumn(2).setCellRenderer(new ImportTableRenderer());
    table_.getColumnModel().getColumn(2).setPreferredWidth(40);
    table_.getColumnModel().getColumn(3).setCellRenderer(new ImportTableRenderer());
    table_.getColumnModel().getColumn(3).setPreferredWidth(230);
    ImportTableEditorForBCalqueSelection editor=new ImportTableEditorForBCalqueSelection();
    editor.addCellEditorListener(this);
    table_.getSelectionModel().addListSelectionListener(this);
    table_.getColumnModel().getColumn(3).setCellEditor(editor);
    // Mise a jour du panel d'attribut
    fillAttributsPanel();
  }

  /**
   * Cette fonction g�n�re les attributs a mapper pour la ligne donn�e. Ce
   * mapping est affich� dans le panel pr�vu � cet effet.
   * 
   * @param _indexLigne index de la ligne a traiter
   * @return vrai si la ligne poss�de des attributs � mapper, faux sinon.
   */
  private boolean generateMappingAttribute(int _indexLigne){
    if(_indexLigne<0||_indexLigne>=table_.getRowCount())
      return false;
    // Calque concern�
    ZCalqueAffichageDonneesAbstract calque=(ZCalqueAffichageDonneesAbstract)table_.getValueAt(_indexLigne, table_.getColumnCount()-1);
    if (!mappers_.containsKey(_indexLigne)||(mappers_.containsKey(_indexLigne)&&((BCalque)mappers_.get(_indexLigne)[0])!=calque)) {
      // Cr�ation d'une liste reliante les attributs lus aux attributs du calque. \\
      // R�cup�ration des informations sur les attributs du calques
      String geometrie=(String)table_.getValueAt(_indexLigne, 0);
      String nature=(String)table_.getValueAt(_indexLigne, 1);
      // 1 : r�cup�ration des attributs attendus par le calque
      ArrayList<GISAttributeInterface> calqueAttr=new ArrayList<GISAttributeInterface>();
      GISAttributeInterface[] calqueAttTmp=((ZModeleGeometry)calque.modeleDonnees()).getGeomData().getAttributes();
      for (int j=0; j<calqueAttTmp.length; j++)
        calqueAttr.add(calqueAttTmp[j]);
      // 2 : selection des g�om�tries concern�es par le calque
      LinkedList<GISDataModel> concernedGeom=new LinkedList<GISDataModel>();
      GISDataModel[] source;
      // Prise en compte du type de g�om�trie utilis�
      if (FSigLib.getS("MultiPoint").equals(geometrie))
        source=data_.getPoints();
      else if (FSigLib.getS("Polyligne").equals(geometrie))
        source=data_.getPolylignes();
      else
        source=data_.getPolygones();
      // Prise en compte du type de nature utilis�
      for (int j=0; j<source.length; j++) {
        int indexNature=source[j].getIndiceOf(GISAttributeConstants.NATURE);
        if ("".equals(nature)&&indexNature==-1)
          concernedGeom.add(source[j]);
        else if (indexNature!=-1) {
          boolean correctNature=false;
          int k=-1;
          while (!correctNature&&++k<source[j].getNumGeometries())
            correctNature=source[j].getValue(indexNature, k).equals(nature);
          if (correctNature)
            concernedGeom.add(source[j]);
        }
      }
      // 3 : r�cup�ration des attributs disponibles dans les g�om�tries
      HashSet<GISAttributeInterface> geomAttr=new HashSet<GISAttributeInterface>();
      for (int j=0; j<concernedGeom.size(); j++) {
        for (int k=0; k<concernedGeom.get(j).getNbAttributes(); k++)
          geomAttr.add(concernedGeom.get(j).getAttribute(k));
      }
      // 4 : Ajout des attributs au mapper
      mappers_.put(_indexLigne, new Object[]{calque, new FSigExportImportAttributesMapper(geomAttr.toArray(new GISAttributeInterface[0]), calqueAttr.toArray(new GISAttributeInterface[0]), false)});
    }
    // G�n�ration d'une table affichant les liens propos�s \\
    scAttrs_.getViewport().add(((FSigExportImportAttributesMapper)mappers_.get(_indexLigne)[1]).getTable());
    if(((FSigExportImportAttributesMapper)mappers_.get(_indexLigne)[1]).getReadDestMap().size()==0)
      return false;
    else
      return true;
  }
  
  protected void fillAttributsPanel(){
    // Si rien n'est selectionn�, on iter sur les lignes jusqu'� en trouver une ayant des truc a mapper
    if(table_.getSelectedRow()!=-1)
      generateMappingAttribute(table_.getSelectedRow());
    else{
      int indexRow = -1;
      boolean found = false;
      while(!found&&++indexRow<table_.getRowCount())
        found = generateMappingAttribute(indexRow);
      if(found)
        table_.setRowSelectionInterval(indexRow, indexRow);
    }
  }
  
  /**
   * Retourne les donn�es n�c�ssaires � l'importation des g�om�tries.
   * 
   * @return un tableau de tableau.
   *         Chaque ligne contient dans l'ordre :
   *         le g�om�trie d'origine ; la nature d'origine ; la g�om�trie de destination ;
   *         la nature de destination ; le mapper ; le calque de destination.
   *         Ce qui donne en type :
   *         int ; string ; int ; string ; FSigExportImportAttributesMapper ;
   *         ZCalqueAffichageDonneesAbstract (avec un ZModeleGeometry a l'int�rieur)
   *         -les codes de g�om�tries sont ceux de DeForme.
   *         -La nature d'origine est �gale � "" en cas de nature absente dans les g�om�tries.
   *         -Le mapper est null si il n'y a rien a mapper.
   */
  public Object[][] getImportData(){
    ArrayList<Object[]> result = new ArrayList<Object[]>();
    // Les informations sont extraites du tableau
    for(int i=0;i<table_.getRowCount();i++){
      // Si la case import est coch� dans la ligne
      if((Boolean) table_.getValueAt(i, 2)){
        Object[] row = new Object[6];
        // G�om�trie d'origine
        row[0] = convertNomForme((String)table_.getValueAt(i, 0));
        // Nature d'origine
        row[1] = table_.getValueAt(i, 1);
        // Calque de destination
        row[5] = table_.getValueAt(i, 3);
        // G�om�trie de destination
        if(((ZCalqueEditable)row[5]).canAddForme((Integer) row[0]))
          row[2]=(Integer) row[0];
        else if(((ZCalqueEditable)row[5]).canAddForme(DeForme.MULTI_POINT))
          row[2]=DeForme.MULTI_POINT;
        else if(((ZCalqueEditable)row[5]).canAddForme(DeForme.LIGNE_BRISEE))
          row[2]=DeForme.LIGNE_BRISEE;
        else
          row[2]=DeForme.POLYGONE;
        // Nature de destination
        row[3] = ((ZModeleGeometry)((ZCalqueAffichageDonneesAbstract) row[5]).modeleDonnees()).getGeomData().getFixedAttributValue(GISAttributeConstants.NATURE);
        // Mapper
        if(mappers_.containsKey(i))
          row[4] = mappers_.get(i)[1];
        result.add(row);
      }
    }
    return (Object[][])result.toArray(new Object[0][]);
  }
  
  private int convertNomForme(String _nomForme){
    if(FSigLib.getS("MultiPoint").equals(_nomForme))
      return DeForme.MULTI_POINT;
    if(FSigLib.getS("Polyligne").equals(_nomForme))
      return DeForme.LIGNE_BRISEE;
    if(FSigLib.getS("Polygone").equals(_nomForme))
      return DeForme.POLYGONE;
    return -1;
  }
  
  public void doAtEnd(ProgressionInterface _prog, CtuluCommandManager _mng) {
    final Object[][] instructionData = getImportData();
    final CtuluCommandComposite cmp = new CtuluCommandComposite(FSigLib.getS("Importer g�om�tries"));
    // Conversion et importation des g�om�tries \\
    for(int i=0;i<instructionData.length;i++){
      int formeOrigine = (Integer) instructionData[i][0];
      int formeDestination = (Integer) instructionData[i][2];
      String natureOrigine = (String) instructionData[i][1];
      String natureDestination = (String) instructionData[i][3];
      FSigExportImportAttributesMapper attributesMapper = (FSigExportImportAttributesMapper) instructionData[i][4];
      ZCalqueAffichageDonneesAbstract calqueDestination = (ZCalqueAffichageDonneesAbstract) instructionData[i][5];
      
      GISDataModel[] models = new GISDataModel[0];
      boolean isPoint=false;
      if(formeOrigine==DeForme.MULTI_POINT){
        isPoint = true;
        models=data_.getPoints();
      }
      if(formeOrigine==DeForme.LIGNE_BRISEE)
        models=data_.getPolylignes();
      if(formeOrigine==DeForme.POLYGONE)
        models=data_.getPolygones();
      
      for(int j=0;j<models.length;j++)
        // Si le model est fait de point, on les convertis automatiquement en multipoints
        if(isPoint)
          processGeometry(new GISDataModelPointToMultiPointAdapter(models[j]), formeOrigine, formeDestination, natureOrigine, natureDestination, attributesMapper, calqueDestination, _prog, cmp);
        else
          processGeometry(models[j], formeOrigine, formeDestination, natureOrigine, natureDestination, attributesMapper, calqueDestination, _prog, cmp);
    }
    if (_mng!=null) _mng.addCmd(cmp.getSimplify());
  }

  /**
   * Converti toutes les g�om�tries correspondant au propri�t� d'origine donn�es
   * en prori�t� d'origine de destination, mappe les attributs et ajout les
   * g�om�tries r�sultantes au calque._natureOrigine
   * Il doit �tre garantie que les g�om�tries sont de la forme d'origine donn�e.
   * Seul les g�om�tries de nature d'origine seront trait�s par cette fonction.
   * Les autres seront ignor�es.
   * 
   * @param _modelDonnees
   *          le model de donn�es contenant la g�om�trie � importer
   * @param _formeOrigine
   *          la forme d'origine de la g�om�trie
   * @param _formeDestination
   *          la forme dans lequelle convertir la g�om�trie
   * @param _natureOrigine
   *          la nature d'origine de la forme
   * @param _natureDestination
   *          la nature dans laquelle convertir la g�om�trie
   * @param _attributesMapper
   *          le mapper d'attribut
   * @param _calqueDestination
   *          le calque dans lequel ajouter la g�om�trie
   * @param _prog L'interface de progression
   */
  private void processGeometry(GISDataModel _modelDonnees, int _formeOrigine, int _formeDestination, String _natureOrigine, String _natureDestination,
      FSigExportImportAttributesMapper _attributesMapper, ZCalqueAffichageDonneesAbstract _calqueDestination, ProgressionInterface _prog, CtuluCommandComposite _cmd) {

//    for (int i=0; i<_modelDonnees.getNbAttributes(); i++) FuLog.trace("Att "+(i+1)+":"+_modelDonnees.getAttribute(i).getLongName());
    if (_attributesMapper!=null) {

      // Chargement en m�moire des seuls attributs sources utilis�s (necessaire pour des sources d'origine SIG). \\
      
      GISAttributeInterface[] attrs=new GISAttributeInterface[_attributesMapper.getDestReadMap().size()];
      int i=0;
      for (Entry<GISAttributeInterface,GISAttributeInterface> att: _attributesMapper.getDestReadMap().entrySet())
        attrs[i++]=att.getValue();
      
      _modelDonnees.preload(attrs,_prog);
      
      // On cache les attributs ignor�s par le mapping.
      _modelDonnees = GISDataModelFilterAdapter.buildAdapter(_modelDonnees, attrs);
    }
//  for (int i=0; i<_modelDonnees.getNbAttributes(); i++) FuLog.trace("Att "+(i+1)+":"+_modelDonnees.getAttribute(i).getLongName());
    
    // Selection des g�om�tries avec la nature \\
    
    if(_natureOrigine.isEmpty())
      _modelDonnees = new GISDataModelSelectionAdapter(_modelDonnees, new Object[][]{{GISAttributeConstants.NATURE, ""}, {GISAttributeConstants.NATURE, GISDataModelSelectionAdapter.ATTRIBUT_NOT_EXIST}});
    else
      _modelDonnees = new GISDataModelSelectionAdapter(_modelDonnees, new Object[][]{{GISAttributeConstants.NATURE, _natureOrigine}});
    
    // Conversion de la forme \\
    
    if(_formeOrigine!=_formeDestination){
      if(_formeOrigine==DeForme.LIGNE_BRISEE&&_formeDestination==DeForme.MULTI_POINT)
        // Conversion de polyligne en multiPoint
        _modelDonnees=new GISDataModelPolyligneToMultiPointAdapter(_modelDonnees);
      else if(_formeOrigine==DeForme.POLYGONE){
        // Conversion de polygone en multipoint
        _modelDonnees=new GISDataModelPolygoneToMultiPointAdapter(_modelDonnees);
        // Conversion de polygone en polyligne
        if(_formeDestination==DeForme.LIGNE_BRISEE)
          _modelDonnees=new GISDataModelMultiPointToPolyligneAdapter(_modelDonnees);
      }
      else{
        if(_formeOrigine==DeForme.MULTI_POINT)
          // Conversion de multiPoint en polyligne
          _modelDonnees=new GISDataModelMultiPointToPolyligneAdapter(_modelDonnees);
        if(_formeDestination==DeForme.POLYGONE)
          // Conversion de multipoint en polygone
          _modelDonnees=new GISDataModelPolyligneToPolygoneAdapter(_modelDonnees);
      }
    }
    
    // Mapping des attributs \\
    
    // Extraction des attributs du calque de destination
    GISZoneCollection zoneCollection = ((ZModeleGeometry)_calqueDestination.modeleDonnees()).getGeomData();
    
    // Ajout des attributs manquant (ceux du calque de destination)
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(_modelDonnees);
    if (_attributesMapper!=null) {
      Map<GISAttributeInterface, GISAttributeInterface> attributsMap=_attributesMapper.getDestReadMap();
      for (Map.Entry<GISAttributeInterface, GISAttributeInterface> entry : attributsMap.entrySet())
        adapter.addAttribut(entry.getKey(), entry.getKey().getDefaultValue());
    }
    // Remplacement de l'ancien model par le nouveau
    _modelDonnees = adapter;
//  for (int i=0; i<_modelDonnees.getNbAttributes(); i++) FuLog.trace("Att "+(i+1)+":"+_modelDonnees.getAttribute(i).getLongName());

    if (_attributesMapper!=null)
      // Substitution des attributs
      _modelDonnees=new GISDataModelAttributeSubstitutionAdapter(_modelDonnees, _attributesMapper.getDestReadMap());

//  for (int i=0; i<_modelDonnees.getNbAttributes(); i++) FuLog.trace("Att "+(i+1)+":"+_modelDonnees.getAttribute(i).getLongName());
    
    // Mise en conformit� du mod�le pour qu'il contienne les m�mes attributs que le calque de destination.
    // Les attributs en trop sont cach�s, les attributs manquants sont ajout�s avec des valeurs nulles.
    _modelDonnees = GISDataModelFilterAdapter.buildAdapter(_modelDonnees, zoneCollection.getAttributes());
//  for (int i=0; i<_modelDonnees.getNbAttributes(); i++) if (_modelDonnees.getAttribute(i)!=null) FuLog.trace("Att "+(i+1)+":"+_modelDonnees.getAttribute(i).getLongName());
    
    // Ajout dans le calque \\
    
    // Pb : addAll() initialise l'attribut Z avec les coordonn�es Z, m�me si on souhaite ignorer le Z. On met donc l'attribut
    // isZ=null avant de le restituer.
    GISAttributeDouble isZ=zoneCollection.getAttributeIsZ();
    if (isZ!=null && _attributesMapper!=null && !_attributesMapper.getDestReadMap().containsKey(isZ)) {
      zoneCollection.setAttributeIsZ(null);
    }
    zoneCollection.addAll(_modelDonnees, _cmd, false);
    zoneCollection.setAttributeIsZ(isZ);
  }
  
//  public boolean isValid() {
//    return true;
//  }

  /* (non-Javadoc)
   * @see javax.swing.event.CellEditorListener#editingCanceled(javax.swing.event.ChangeEvent)
   */
  @Override
  public void editingCanceled(ChangeEvent e) {
    fillAttributsPanel();
  }

  /* (non-Javadoc)
   * @see javax.swing.event.CellEditorListener#editingStopped(javax.swing.event.ChangeEvent)
   */
  @Override
  public void editingStopped(ChangeEvent e) {
    fillAttributsPanel();
  }

  /* (non-Javadoc)
   * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
   */
  @Override
  public void valueChanged(ListSelectionEvent e) {
    fillAttributsPanel();
  }
}
