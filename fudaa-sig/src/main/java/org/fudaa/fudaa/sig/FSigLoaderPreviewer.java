/*
 *  @creation     8 juin 2005
 *  @modification $Date: 2008-01-11 12:20:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuTaskView;
import java.awt.Dimension;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionBuAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Chargement et/ou prévisualisation lors de l'import de données dans un calque SIG.
 * 
 * @author Fred Deniger
 * @version $Id: FSigLoaderPreviewer.java,v 1.8 2008-01-11 12:20:37 bmarchan Exp $
 */
public abstract class FSigLoaderPreviewer {

  public FSigLoaderPreviewer() {
    super();
  }

  public void preview(final CtuluUI _parent, final GISPolygone[] _zoneCible, final boolean _showPreview) {
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuHorizontalLayout());
    final BuTaskView tv = new BuTaskView();
    tv.setPreferredSize(new Dimension(150, 30));
    pn.add(tv);
    pn.add(new BuButton("stop"));
    final CtuluDialog d = pn.createDialog(_parent.getParentComponent());

    final CtuluTaskOperationGUI r = new CtuluTaskOperationGUI(null, FudaaLib.getS("Chargement")) {

      @Override
      public void act() {
        final ProgressionBuAdapter prog = new ProgressionBuAdapter(this);
        final FSigGeomSrcData src = loadData(prog);
        if (src != null && _showPreview) {
          src.showPreview(d, prog, _zoneCible, _parent);
        } else {
          d.dispose();
        }
      }
    };
    r.setTaskView(tv);
    r.start();
    d.afficheDialogModal();
  }

  public abstract FSigGeomSrcData loadData(ProgressionInterface _prog);

}
