package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFileChooser;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.fudaa.commun.impl.FudaaImportCsvPanel;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * Le modele gerant le fichier.
 *
 * @author Fred Deniger
 * @version $Id: FSigWizardFileModel.java,v 1.2 2007-04-30 14:22:39 deniger Exp $
 */
public class FSigWizardFileModel extends CtuluListEditorModel {

  /**
   * @author Fred Deniger
   * @version $Id: FSigWizardFileModel.java,v 1.2 2007-04-30 14:22:39 deniger Exp $
   */
  protected final static class FileSelectResult {

    File[] f_;
    BuFileFilter ft_;
  }
  final Map fileData_;
  final Map fmtLoader_;
  final BuFileFilter[] filters_;
  final BuFileFilter csv_;
  // Association entre le nom du fichier et son �tat (d'origine ou non)
  private HashMap<String, String> origines_;

  /**
   * @return Une HashMap Associant le nom du fichier et son �tat (d'origine ou non)
   */
  public HashMap<String, String> getOrigines() {
    return origines_;
  }

  protected FSigFileLoaderI createModel(final BuFileFilter _f) {
    if (_f == null) {
      return null;
    }
    return ((FSigFileLoaderI) fmtLoader_.get(_f)).createNew();
  }

  BuFileFilter guessFmt(final File _f) {
    for (int i = 0; i < filters_.length; i++) {
      if (csv_ != filters_[i] && filters_[i].accept(_f)) {
        return filters_[i];
      }
    }
    if (_f != null && (_f.getName().endsWith(".txt") || _f.getName().endsWith(".csv"))) {
      return csv_;
    }
    return null;
  }

  public FSigWizardFileModel(final Map _fmtLoader, final BuFileFilter[] _filters, final BuFileFilter _csv) {
    super(true);
    fmtLoader_ = _fmtLoader;
    fileData_ = new HashMap();
    filters_ = _filters;
    csv_ = _csv;
    origines_ = new HashMap<String, String>();
  }

  @Override
  protected void actionBeforeRemove(final int _i) {
    fileData_.remove(getValueAt(_i));
  }

  @Override
  public void addElement(final Object _o) {
    if (!fileData_.containsKey(_o)) {
      // ajouter le format
      super.addElement(_o);
    }
  }
  private GISAttributeDouble csvFirstColonne;

  public void setCsvFirstColonne(GISAttributeDouble csvFirstColonne) {
    this.csvFirstColonne = csvFirstColonne;
  }

  public GISAttributeDouble getCsvFirstColonne() {
    return csvFirstColonne;
  }


  public void addFiles(final FSigWizardFileModel.FileSelectResult _r, final int _i) {
    if (_r.f_ != null && _r.f_.length > 0) {
      if (_r.ft_ == null) {
        _r.ft_ = guessFmt(_r.f_[0]);
      }
      boolean isCsv = false;
      Map options = null;
      if (csv_ == _r.ft_) {
        isCsv = true;
        options = FSigWizardFileModel.loadCsv(_r.f_[0]);
      }
      for (int i = 0; i < _r.f_.length; i++) {
        // on ajoute un fichier que s'il n'est pas pr�sent
        boolean modifyData = false;
        if (!fileData_.containsKey(_r.f_[i])) {
          modifyData = true;
          if (_i >= 0) {
            super.add(_i, _r.f_[i]);
          } else {
            super.addElement(_r.f_[i]);
            super.getRowCount();
          }
          origines_.put(_r.f_[i].getName(), GISAttributeConstants.ATT_VAL_ETAT_ORIG);
        }
        // on recharge le model si le format est diff�rent
        if (modifyData || !((FSigFileLoaderI) fileData_.get(_r.f_[i])).getFileFilter().equals(_r.ft_)) {
          if (isCsv) {
            final FSigFileLoaderCsv csv = (FSigFileLoaderCsv) createModel(csv_);
            csv.setFirstColonneAttribute(csvFirstColonne);
            csv.setOptions(options);
            fileData_.put(_r.f_[i], csv);
          } else {
            fileData_.put(_r.f_[i], createModel(_r.ft_));
          }
        }
      }
    }
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public String getColumnName(final int _column) {
    switch (_column) {
      case 0:
        return FSigLib.getS("N�");
      case 1:
        return FSigLib.getS("Fichier");
      case 2:
        return FSigLib.getS("Format");
      case 3:
        return FSigLib.getS(GISAttributeConstants.ETAT_GEOM.getName());
      default:
        return CtuluLibString.EMPTY_STRING;
    }
  }

  @Override
  public Object getValueAt(final int _row, final int _col) {
    if (_col == 3) {
      return origines_.get(((File) super.getValueAt(_row, 1)).getName());
    } else if (_col == 2) {
      final FSigFileLoaderI loader = (FSigFileLoaderI) fileData_.get(getValueAt(_row));
      return loader == null ? null : loader.getFileFilter();
    }
    return super.getValueAt(_row, _col);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex > 0;
  }
  CtuluFileChooser fileChooser;

  @Override
  public Object createNewObject() {
    if (fileChooser == null) {
      fileChooser = new CtuluFileChooser(false);
    }
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setMultiSelectionEnabled(false);
    final int r = fileChooser.showOpenDialog(CtuluLibSwing.getActiveWindow());
    if (r == JFileChooser.APPROVE_OPTION) {
      return fileChooser.getSelectedFile();
    }
    return null;
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 3) {
      origines_.put(((File) super.getValueAt(_rowIndex, 1)).getName(), (String) _value);
    }
    if (_columnIndex == 2) {
      final FSigFileLoaderI loader = (FSigFileLoaderI) fileData_.get(getValueAt(_rowIndex));
      final BuFileFilter old = loader == null ? null : loader.getFileFilter();
      if (old != _value) {
        final BuFileFilter ft = (BuFileFilter) _value;
        final FSigFileLoaderI newLoader = createModel(ft);
        if (csv_ == ft) {
          final Map m = FSigWizardFileModel.loadCsv((File) getValueAt(_rowIndex));
          if (m != null) {
            ((FSigFileLoaderCsv) newLoader).setOptions(m);
          }
        }
        fileData_.put(getValueAt(_rowIndex), newLoader);
        fireTableCellUpdated(_rowIndex, _columnIndex);
      }
    } else if (_columnIndex == 1) {
      File f = null;
      if (_value instanceof File) {
        f = (File) _value;
      } else if (_value instanceof String) {
        f = new File((String) _value);
      }
      if (getValueAt(_rowIndex) instanceof File) {
        final File old = (File) getValueAt(_rowIndex);
        if (!old.equals(f)) {
          fileData_.remove(old);

        }
      }
      super.setValueAt(f, _rowIndex, _columnIndex);
      setValueAt(guessFmt(f), _rowIndex, _columnIndex + 1);
    }
  }

  public static Map loadCsv(final File _f) {
    final FSigFileLoaderCsv.XYCsvReader reader = new FSigFileLoaderCsv.XYCsvReader();
    final FudaaImportCsvPanel pn = new FudaaImportCsvPanel(_f, false) {
      @Override
      public boolean isDataValid() {
        reader.initFromOption(buildOption());
        final String res = reader.containsError();
        if (res != null) {
          setErrorText(res);
        }
        return res == null;
      }
    };

    reader.setNumeric(false);
    pn.setReader(reader);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(CtuluLibSwing.getActiveWindow()))) {
      return pn.buildOption();
    }
    return null;
  }
}