/*
 *  @creation     25 mai 2005
 *  @modification $Date: 2007-05-04 14:00:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.ListCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: FSigVarAttrMapperTableModel.java,v 1.11 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigVarAttrMapperTableModel extends AbstractTableModel {

  public static final CtuluCellTextRenderer getAttRenderer() {
    return new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value instanceof String) {
          setText((String) _value);
        } else if (_value == GISAttributeConstants.BATHY) {
          super.setText(GISAttributeConstants.BATHY.getName() + " (" + H2dVariableType.BATHYMETRIE + ')');
        } else {
          super.setText(((GISAttributeInterface) _value).getName());
        }
      }
    };
  }

  /**
   * @author Fred Deniger
   * @version $Id: FSigVarAttrMapperTableModel.java,v 1.11 2007-05-04 14:00:26 deniger Exp $
   */
  public final static class MapperResult {

    public H2dVariableType[] vars_;
    public GISAttributeInterface[] att_;

    public boolean isDestDefined() {
      return att_ != null && att_.length > 0;
    }
  }

  final H2dVariableType[] target_;
  final GISAttributeInterface[] src_;
  /**
   * de la taille de target_. Pour chaque target donne l'indice de la source
   */
  final GISAttributeInterface[] idx_;

  public final boolean isAttributeUsed() {
    if (idx_ != null && idx_.length > 0) {
      for (int i = idx_.length - 1; i >= 0; i--) {
        if (idx_[i] != null) {
          return true;
        }
      }
    }
    return false;

  }

  /**
   * @param _target les variables cibles
   * @param _src les attributs a utilises pour l'intialisation
   */
  public FSigVarAttrMapperTableModel(final H2dVariableType[] _target, final GISAttributeInterface[] _src) {
    super();
    src_ = _src;
    target_ = _target;
    idx_ = new GISAttributeInterface[target_.length];
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public int getRowCount() {
    return target_.length;
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return _columnIndex == 1 && src_ != null && src_.length > 0;
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    idx_[_rowIndex] = (_value instanceof GISAttributeInterface) ? (GISAttributeInterface) _value : null;
    fireTableCellUpdated(_rowIndex, _columnIndex);
  }

  @Override
  public String getColumnName(final int _column) {
    if (_column == 0) {
      return FSigLib.getS("Variables � modifier");
    }
    return FSigLib.getS("Depuis:");
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      return target_[_rowIndex].getName();
    }
    if (src_ == null || src_.length == 0) {
      return FSigLib.getS("Pas de source");
    }
    return idx_[_rowIndex];
  }

  /**
   * @return true si une source s�lectionn�e est atomique
   */
  public boolean isAtomicSrcUsed() {
    for (int i = idx_.length - 1; i >= 0; i--) {
      if (idx_[i] != null && idx_[i].isAtomicValue()) {
        return true;
      }
    }
    return false;
  }

  class SrcComboboxModel extends AbstractListModel implements ComboBoxModel {

    Object selected_;

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    @Override
    public Object getElementAt(final int _index) {
      return _index == 0 ? null : src_[_index - 1];
    }

    @Override
    public int getSize() {
      return src_ == null ? 0 : src_.length + 1;
    }
  }

  /**
   * @return renvoie la correspondance variable a modifier -> attribut a utiliser.
   */
  public Map getTargetAtt() {
    final Map res = new HashMap(target_.length);
    for (int i = idx_.length - 1; i >= 0; i--) {
      final GISAttributeInterface att = idx_[i];
      if (att != null) {
        res.put(target_[i], att);
      }
    }
    return res;

  }

  /**
   * @return les resultats dans l'ordre d'entree.
   */
  public MapperResult getResult() {
    final ArrayList<H2dVariableType> listVar = new ArrayList<H2dVariableType>(target_.length);
    final ArrayList<GISAttributeInterface> listAtt = new ArrayList<GISAttributeInterface>(target_.length);
    final int nb = idx_.length;
    for (int i = 0; i < nb; i++) {
      final GISAttributeInterface att = idx_[i];
      if (att != null) {
        listAtt.add(att);
        listVar.add(target_[i]);
      }
    }
    final MapperResult res = new MapperResult();
    res.att_ = new GISAttributeInterface[listAtt.size()];
    listAtt.toArray(res.att_);
    res.vars_ = new H2dVariableType[res.att_.length];
    listVar.toArray(res.vars_);
    return res;
  }

  public CtuluCellTextRenderer getSrcCellRenderer() {
    return cellRender_;
  }

  public TableCellEditor getSrcCellEditor() {
    return new SrcCellEditor(cellRender_, new SrcComboboxModel());
  }

  CtuluCellTextRenderer cellRender_ = new CtuluCellTextRenderer() {

    @Override
    protected void setValue(final Object _value) {
      if (_value == null) {
        setText(FSigLib.getS("Ignorer"));
      } else if (_value instanceof String) {
        setText((String) _value);
      } else {
        setText(((GISAttributeInterface) _value).getLongName());
      }
    }
  };

  private static class SrcCellEditor extends DefaultCellEditor {

    public SrcCellEditor(ListCellRenderer _r, ComboBoxModel _model) {
      super(new JComboBox(_model));
      ((JComboBox) super.editorComponent).setRenderer(_r);
    }
  }
}
