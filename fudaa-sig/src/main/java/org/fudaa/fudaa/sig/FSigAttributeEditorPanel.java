/*
 *  @creation     18 mai 2005
 *  @modification $Date: 2008-01-11 12:20:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Un panneau d'�dition des attributs d'un groupe de calque SIG utilis� lors de la
 * cr�ation du groupe.
 * 
 * @author Fred Deniger
 * @version $Id: FSigAttributeEditorPanel.java,v 1.12 2008-01-11 12:20:39 bmarchan Exp $
 */
public class FSigAttributeEditorPanel extends CtuluDialogPanel {

  final GISZoneCollection collect_;
  final FSigAttibuteTypeManager mng_;
  SpecModel model_;

  private class SpecModel extends CtuluListEditorModel {

    SpecModel() {
      super(collect_ == null ? FuEmptyArrays.OBJECT0 : collect_.getAttributes(), true);
    }

    @Override
    public int getRowCount() {
      return super.getRowCount() + 1;
    }

    @Override
    public boolean actionAdd() {
      return false;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return _columnIndex == 1 && _rowIndex == getRowCount() - 1;
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_value == null || _value.toString().trim().length() == 0) {
        return;
      }
      Object newValue = mng_.getAttribute(_value.toString());
      if (newValue == null) {
        newValue = _value;
      }
      v_.add(newValue);
      final int n = v_.size();
      fireTableRowsInserted(n, n);
    }

    @Override
    public Object getValueAt(final int _row, final int _col) {
      if (_col == 1 && _row == getRowCount() - 1) {
        return null;
      }
      return super.getValueAt(_row, _col);
    }

    @Override
    public boolean actionInserer(final int _r) {
      return false;
    }

  }

  private class SpecCbModel extends AbstractListModel implements ComboBoxModel {

    Object select_;
    List att_ = new ArrayList();

    SpecCbModel() {
      fillModel();
    }

    @Override
    public Object getSelectedItem() {
      return select_;
    }

    final void fillModel() {
      att_.clear();
      mng_.fillListWithAtt(att_);
      if (proposedVar_ != null) {
        att_.addAll(Arrays.asList(proposedVar_));
        fireIntervalAdded(this, 0, att_.size());
      }
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != select_) {
        select_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    @Override
    public Object getElementAt(final int _index) {
      if (_index == 0) {
        return null;
      }
      return att_.get(_index - 1);
    }

    @Override
    public int getSize() {
      return 1 + att_.size();
    }
  }

  H2dVariableType[] proposedVar_;

  Set initAtt_;

  @Override
  public boolean isDataValid() {
    if (t_.isEditing()) {
      t_.getCellEditor().stopCellEditing();
    }
    return true;
  }

  public void fillWithAddedRemovedAttribute(final Set _add, final Set _removed) {
    final Object[] o = model_.getValues();
    _removed.addAll(initAtt_);
    for (int i = o.length - 1; i >= 0; i--) {
      final Object oi = o[i];
      if (oi instanceof GISAttributeInterface) {
        if (initAtt_.contains(oi)) {
          _removed.remove(oi);
        } else {
          _add.add(oi);
        }
      } else if (oi instanceof H2dVariableType) {
        final GISAttributeDouble d = mng_.addDoubleAttribute((H2dVariableType) oi, false);
        if (d == null) {
          FuLog.warning(new Throwable());
        } else {
          _add.add(d);
        }
      } else if (oi instanceof String) {
        final GISAttributeDouble d = mng_.addDoubleAttribute((String) oi, false);
        if (d == null) {
          FuLog.warning(new Throwable());
        } else {
          _add.add(d);
        }
      }

    }
  }

  JTable t_;

  /**
   * @param _collect
   * @param _mng
   * @param _proposedVar
   */
  public FSigAttributeEditorPanel(final GISZoneCollection _collect, final FSigAttibuteTypeManager _mng,
      final H2dVariableType[] _proposedVar, final String _titleForEditor) {
    super();
    collect_ = _collect;
    initAtt_ = new HashSet();

    if (collect_ != null) {
      initAtt_.addAll(Arrays.asList(collect_.getAttributes()));
    }
    mng_ = _mng;
    model_ = new SpecModel();
    proposedVar_ = _proposedVar;
    final CtuluListEditorPanel editor = new CtuluListEditorPanel(model_, false, true, false);
    t_ = editor.getTable();
    final JComboBox cb = new JComboBox(new SpecCbModel());
    cb.setEditable(true);
    final DefaultCellEditor cellEditor = new DefaultCellEditor(cb);
    final CtuluCellTextRenderer cbRenderer = createCbCellRenderer();
    final CtuluCellTextRenderer cellRenderer = createCellRenderer();
    cb.setRenderer(cbRenderer);
    editor.setValueListCellEditor(cellEditor);
    editor.setValueListCellRenderer(cellRenderer);
    setLayout(new BuBorderLayout());
    if (_titleForEditor != null) {
      editor.setBorder(BorderFactory.createTitledBorder(_titleForEditor));
    }
    add(editor, BuBorderLayout.CENTER);
  }

  private static CtuluCellTextRenderer createCellRenderer() {
    return new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value == null) {
          setText(BuResource.BU.getString("Ajouter..."));
        } else if (_value instanceof GISAttributeInterface) {
          setText(((GISAttributeInterface) _value).getLongName());
        } else {
          super.setValue(_value);
        }
      }
    };
  }

  private static CtuluCellTextRenderer createCbCellRenderer() {
    return new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value == null) {
          setText(CtuluLibString.EMPTY_STRING);
        } else if (_value instanceof GISAttributeInterface) {
          final GISAttributeInterface att = (GISAttributeInterface) _value;
          setText(att.getLongName());
        } else {
          super.setValue(_value);
        }
      }
    };
  }
}
