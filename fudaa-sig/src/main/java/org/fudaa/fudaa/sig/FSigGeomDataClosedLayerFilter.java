/*
 *  @creation     26 mai 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelZoneAdapter;
import org.fudaa.ctulu.gis.GISVisitorDefault;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleGeometry;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigGeomDataClosedLayerFilter implements BCalqueVisitor {

  final List r_ = new ArrayList();
  final List polyligne_ = new ArrayList();
  final PolyCounter counter_ = new PolyCounter();

  final static class PolyCounter extends GISVisitorDefault {

    int nbPolyligne_;
    int nbPolygones_;
    int nbPoint_;

    void init() {
      nbPolygones_ = 0;
      nbPolyligne_ = 0;
    }

    @Override
    public boolean visitPoint(final Point _p) {
      return true;
    }

    @Override
    public boolean visitPolygone(final LinearRing _p) {
      nbPolygones_++;
      nbPoint_ += _p.getNumPoints();
      return true;
    }

    @Override
    public boolean visitPolygoneWithHole(final Polygon _p) {
      return true;
    }

    @Override
    public boolean visitPolyligne(final LineString _p) {
      nbPolyligne_++;
      nbPoint_ += _p.getNumPoints();
      return true;
    }
  }

  public final int getNbPointTotal() {
    return nbPointTotal_;
  }

  public GISZoneCollectionLigneBrisee[] getGeomClosedData() {
    final GISZoneCollectionLigneBrisee[] res = new GISZoneCollectionLigneBrisee[r_.size()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = (GISZoneCollectionLigneBrisee) ((ZCalqueLigneBrisee) r_.get(i)).modeleDonnees().getGeomData();
    }
    return res;
  }

  public GISDataModel[] getGeomClosedDataModel() {
    final GISDataModel[] res = new GISDataModel[r_.size()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new GISDataModelZoneAdapter(((ZCalqueLigneBrisee) r_.get(i)).modeleDonnees().getGeomData(), null);
    }
    return res;
  }

  public GISZoneCollectionLigneBrisee[] getGeomData() {
    final GISZoneCollectionLigneBrisee[] res = new GISZoneCollectionLigneBrisee[polyligne_.size()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = (GISZoneCollectionLigneBrisee) ((ZCalqueLigneBrisee) polyligne_.get(i)).modeleDonnees().getGeomData();
    }
    return res;
  }

  public GISDataModel[] getGeomDataModel() {
    final GISDataModel[] res = new GISDataModel[polyligne_.size()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new GISDataModelZoneAdapter(((ZCalqueLigneBrisee) polyligne_.get(i)).modeleDonnees().getGeomData(), null);
    }
    return res;
  }

  public final int getNbPolylignes() {
    return nbPolylignesTotal_;
  }

  public final int getNbPolygones() {
    return nbPolygonesTotal_;
  }

  int nbPolygonesTotal_;
  int nbPolylignesTotal_;
  int nbPointTotal_;

  Set visited_ = new HashSet();

  @Override
  public boolean visit(final BCalque _cq) {
    if (visited_.contains(_cq)) {
      return true;
    }
    if (_cq instanceof ZCalqueLigneBrisee) {
      counter_.init();
      final ZModeleGeometry geom = ((ZCalqueLigneBrisee) _cq).modeleDonnees();
      geom.getGeomData().accept(counter_);
      nbPointTotal_ += counter_.nbPoint_;
      if (counter_.nbPolygones_ > 0) {
        r_.add(_cq);
        nbPolygonesTotal_ += counter_.nbPolygones_;
      }
      if (counter_.nbPolyligne_ > 0) {
        polyligne_.add(_cq);
        nbPolylignesTotal_ += counter_.nbPolyligne_;
      }
    }
    return true;
  }
}
