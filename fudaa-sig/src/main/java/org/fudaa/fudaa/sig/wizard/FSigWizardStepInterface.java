/*
 * @creation 24 juil. 06
 * @modification $Date: 2008-02-22 16:27:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import javax.swing.JComponent;
import javax.swing.JRootPane;

/**
 * Une interface pour une etape de Wizard.
 * 
 * @author fred deniger
 * @version $Id: FSigWizardStepInterface.java,v 1.1.6.1 2008-02-22 16:27:43 bmarchan Exp $
 */
public interface FSigWizardStepInterface {

  /**
   * Le texte d'info pour l'�tape.
   * @return Le texte.
   */
  String getStepText();

  /**
   * Le titre pour cette etape.
   * @return Le titre.
   */
  String getStepTitle();

  void setParentRootPane(JRootPane _dialog);

  /**
   * Le composant correspondant a l'�tape.
   * @return Le composant.
   */
  JComponent getComponent();

}