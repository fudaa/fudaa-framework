/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2008-04-01 17:11:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZone;
import org.fudaa.dodico.telemac.io.SinusxFileFormat;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderSinusX.java,v 1.3.6.1 2008-04-01 17:11:49 bmarchan Exp $
 */
public class FSigFileLoaderSinusX implements FSigFileLoaderI {

  final BuFileFilter ft_;

  /**
   * @param _ft
   */
  public FSigFileLoaderSinusX() {
    super();
    ft_ = SinusxFileFormat.getInstance().createFileFilter();
  }

  public FSigFileLoaderSinusX(final BuFileFilter _ft) {
    ft_ = _ft;
  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderSinusX(ft_);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }

  GISZone zones_;

  // L'appel � cette m�thode stocke au plus un modele pour les points, un mod�le pour les lignes, un mod�le pour les polygones.
  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    if (zones_ == null) {
      CtuluIOOperationSynthese op=null;
      try {
        op = SinusxFileFormat.getInstance().read(_f, _prog);
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      _analyze.merge(op.getAnalyze());
      if (op.containsSevereError()) {
        return;
      }
      zones_ = (GISZone) op.getSource();
    }
    if (zones_ != null) {
      GISDataModel data=null;
      data = FSigDataModelSinusxAdapter.analyseSxFile(zones_, true, false, false, _prog, _r);
      if (data!=null && data.getNumGeometries() > 0) {
        // Ajout de l'attribut ETAT_GEOM
        _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
        GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(data);
        adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
        // 
        _r.pointModel_.add(adapter);
        _r.addUsedAttributes(GISLib.getAttributeFrom(data));
      }
      data = FSigDataModelSinusxAdapter.analyseSxFile(zones_, false, true, false, _prog, _r);
      if (data!=null && data.getNumGeometries() > 0) {
        // Ajout de l'attribut ETAT_GEOM
        _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
        GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(data);
        adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
        // 
        _r.ligneModel_.add(adapter);
        _r.addUsedAttributes(GISLib.getAttributeFrom(data));
      }
      data = FSigDataModelSinusxAdapter.analyseSxFile(zones_, false, false, true, _prog, _r);
      if (data!=null && data.getNumGeometries() > 0) {
        // Ajout de l'attribut ETAT_GEOM
        _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
        GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(data);
        adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
        // 
        _r.polygoneModel_.add(adapter);
        _r.addUsedAttributes(GISLib.getAttributeFrom(data));
      }
    }

  }
}
