/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2007-05-04 14:00:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinGisAdapter;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderSerafin.java,v 1.2 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigFileLoaderSerafin implements FSigFileLoaderI {

  final BuFileFilter filter_;

  private FSigFileLoaderSerafin(final BuFileFilter _ft) {
    filter_ = _ft;
  }

  /**
   * 
   */
  public FSigFileLoaderSerafin() {
    filter_ = SerafinFileFormat.getInstance().createFileFilter();
  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderSerafin(filter_);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return filter_;
  }

  transient SerafinInterface result_;

  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    if (result_ == null) {
      final CtuluIOOperationSynthese op = SerafinFileFormat.getInstance().readLast(_f, _prog);
      if (!op.containsSevereError()) {
        result_ = (SerafinInterface) op.getSource();
        result_.getGrid().computeBord(_prog, _analyze);
      }
    }
    final TelemacVariableMapper mapper = new TelemacVariableMapper();
    final int nbVar = result_ == null ? 0 : result_.getValueNb();
    final GISAttributeInterface[] attributes = new GISAttributeInterface[nbVar];
    for (int i = 0; i < nbVar; i++) {
      final H2dVariableType t = mapper.getUsedKnownVar(result_.getValueId(i));
      attributes[i] = t == null ? _r.findOrCreateAttribute(result_.getValueId(i), Double.class, true) : _r
          .findOrCreateAttribute(t, true);
    }
    _r.addUsedAttributes(attributes);
    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(new SerafinGisAdapter(result_, attributes));
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    // 
    _r.pointModel_.add(adapter);
    _r.nbPoint_ += result_.getGrid().getPtsNb();
    _r.nbPointTotal_ += result_.getGrid().getPtsNb();

  }
}
