/*
 * @creation     18 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * Cette class peut �tre vue comme un adapteur entre les donn�es de la courbe et la vue qui va les traiter.
 *
 * @author Emmanuel Martin
 * @version $Id$
 */
public class FSigCurveModelProfile implements EGModel {
  private String name_; // Nom de la courbe
  private FSigGraphEditorPanel pnCurve_; // Le model � adapter

  public FSigCurveModelProfile(FSigGraphEditorPanel _pnCurve, String _name) {
    name_ = _name;
    pnCurve_ = _pnCurve;
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return false;
  }

  @Override
  public boolean addValue(double _x, double _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean addValue(double[] _x, double[] _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean deplace(int[] _idx, double _deltax, double _deltay, CtuluCommandContainer _cmd) {
    for (int i = 0; i < _idx.length; i++) {
      pnCurve_.setY(_idx[i], pnCurve_.getY(_idx[i]) + _deltay, _cmd);
    }
    return true;
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface pt) {
  }

  @Override
  public int getNbValues() {
    return pnCurve_.getNbValues();
  }

  @Override
  public String getTitle() {
    return name_;
  }

  @Override
  public String getPointLabel(int _i) {
    return null;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return null;
  }

  @Override
  public double getX(int _idx) {
    return pnCurve_.getX(_idx);
  }

  @Override
  public double getXMax() {
    double xMax = 0;
    for (int i = 0; i < pnCurve_.getNbValues(); i++) {
      xMax = xMax < pnCurve_.getX(i) ? pnCurve_.getX(i) : xMax;
    }
    return xMax;
  }

  @Override
  public double getXMin() {
    if (pnCurve_.getNbValues() == 0) {
      return 0;
    }
    double xMin = pnCurve_.getX(0);
    for (int i = 1; i < pnCurve_.getNbValues(); i++) {
      xMin = xMin > pnCurve_.getX(i) ? pnCurve_.getX(i) : xMin;
    }
    return xMin;
  }

  @Override
  public double getY(int _idx) {
    return pnCurve_.getY(_idx);
  }

  @Override
  public double getYMax() {
    double yMax = 0;
    for (int i = 0; i < pnCurve_.getNbValues(); i++) {
      yMax = yMax < pnCurve_.getY(i) ? pnCurve_.getY(i) : yMax;
    }
    return yMax;
  }

  @Override
  public double getYMin() {
    if (pnCurve_.getNbValues() == 0) {
      return 0;
    }
    double yMin = pnCurve_.getY(0);
    for (int i = 1; i < pnCurve_.getNbValues(); i++) {
      yMin = yMin > pnCurve_.getY(i) ? pnCurve_.getY(i) : yMin;
    }
    return yMin;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public boolean isModifiable() {
    return true;
  }

  @Override
  public boolean isPointDrawn(int _i) {
    return true;
  }

  @Override
  public boolean isRemovable() {
    return false;
  }

  @Override
  public boolean isSegmentDrawn(int _i) {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean removeValue(int _i, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(int[] _i, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setTitle(String name) {
    return false;
  }

  @Override
  public boolean setValue(int _i, double _x, double _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setValues(int[] _idx, double[] _x, double[] _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public EGModel duplicate() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public Object savePersistSpecificDatas() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int[] getInitRows() {
    return null;
  }
}
