/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2007-01-19 13:14:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.impl.EfGridGisAdapter;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderGrid.java,v 1.1 2007-01-19 13:14:09 deniger Exp $
 */
public class FSigFileLoaderGrid implements FSigFileLoaderI {

  final BuFileFilter filter_;
  final FileFormatGridVersion version_;

  /**
   * @param _filter
   * @param _version
   */
  public FSigFileLoaderGrid(final BuFileFilter _filter, final FileFormatGridVersion _version) {
    super();
    filter_ = _filter;
    version_ = _version;
  }

  @Override
  public FSigFileLoaderI createNew() {
    return new FSigFileLoaderGrid(filter_, version_);
  }

  @Override
  public BuFileFilter getFileFilter() {
    return filter_;
  }

  EfGridGisAdapter adapter_;

  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    if (adapter_ == null) {
      final CtuluIOOperationSynthese op = version_.readGrid(_f, _prog);
      if (op.containsMessages() && op.getAnalyze().containsErrors()) {
        if (_analyze != null) {
          _analyze.merge(op.getAnalyze());
        }
        return;
      }
      if (op.getSource() == null) {
        return;
      }
      final EfGridInterface src = ((EfGridSource) op.getSource()).getGrid();
      adapter_ = new EfGridGisAdapter(src, GISAttributeConstants.BATHY);
      src.computeBord(_prog, _analyze);
    }

    if (adapter_ != null) {
      _r.addUsedAttributes(new GISAttributeInterface[] { GISAttributeConstants.BATHY });
      _r.allAttribute_.add(GISAttributeConstants.BATHY);
      // Ajout de l'attribut ETAT_GEOM
      _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
      GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(adapter_);
      adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
      // 
      _r.pointModel_.add(adapter);
      _r.nbPoint_ += adapter_.getNumGeometries();
      _r.nbPointTotal_ += adapter_.getNumGeometries();
    }

  }
}
