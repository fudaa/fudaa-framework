/*
 * @creation 24 juil. 06
 * @modification $Date: 2008-05-13 12:11:00 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.RasterDataInterface;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.action.CalqueActionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueDeplacementInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueDeplacementTargetInterface;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.sig.FSigLib;

import javax.measure.quantity.Dimensionless;
import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * L'�tape de calage de l'image pour le wizard d'import d'image.
 *
 * @author fred deniger
 * @version $Id: FSigImageWizardStepCalage.java,v 1.2.6.3 2008-05-13 12:11:00 bmarchan Exp $
 */
public class FSigImageWizardStepCalage extends FSigWizardDefaultPanel implements FSigWizardStepInterface,
    ZCalqueClikInteractionListener {
  ZCalqueClickInteraction click_ = new ZCalqueClickInteraction() {
    @Override
    public void setGele(final boolean _gele) {
      if (!_gele) {
        calageLogic.setNewPointActivated();
      }
      super.setGele(_gele);
    }
  };
  private ZCalqueClickInteraction zCalqueClickInteraction;
  private final FSigImageWizardStepCalageLogic calageLogic;

  @Override
  public String valideAndGetError() {
    return calageLogic.valideAndGetError();
  }

  final ZCalqueImageRaster image_;
  final ZEbliCalquesPanel pn_;
  private final JLabel lbHeight;
  private final JLabel lbWidth;

  public FSigImageWizardTableModel.Result getResult() {
    return calageLogic.getResult();
  }

  public FSigImageWizardStepCalage(final JRootPane _d) {
    click_.setListener(this);

    final FSigImageWizardPanelController controller = new FSigImageWizardPanelController(new CtuluUIDialog(_d));

    final CalqueActionInteraction acClick = new FSigImageWizardPanelController.SpecAction(FSigLib
        .getS("Ajouter un point"), EbliResource.EBLI.getToolIcon("draw-add-pt"), "SAISIR", click_);
    zCalqueClickInteraction = new ZCalqueClickInteraction();
    zCalqueClickInteraction.setListener(new ZCalqueClikInteractionListener() {
      @Override
      public void pointClicked(final GrPoint _ptReel) {
        mouseClickedOnReal(_ptReel);
      }
    });
    // on ajoute un calque
    final CalqueActionInteraction acClickonTarget = new FSigImageWizardPanelController.SpecAction(FSigLib
        .getS("Saisir un point r�el"), null, "SAISI_REAL", zCalqueClickInteraction);
    pn_ = controller.buildCalquePanel();
    calageLogic = controller.getControl();
    pn_.addCalqueInteraction(click_);
    pn_.getController().setCalqueInteractionActif(click_);
    image_ = controller.getImage();
    final NumberFormat fmtInteger = new DecimalFormat();
    // fmtInteger.setParseIntegerOnly(true);
    fmtInteger.setMaximumFractionDigits(0);
    final EbliFormatter fmt = new EbliFormatter();
    fmt.setFmt(fmtInteger);
    fmt.setUnit(new tech.units.indriya.unit.BaseUnit<Dimensionless>("pixel"));
    controller.setEbliFormatter(fmt);
    pn_.setPreferredSize(new Dimension(450, 400));
    pn_.getVueCalque().addMouseListener(click_);
    final BuPanel center = new BuPanel();
    center.setLayout(new BuBorderLayout());
    center.add(pn_, BuBorderLayout.CENTER);
    final List l = pn_.getController().getActions();
    final BuPanel tb = new BuPanel();
    tb.setOpaque(true);
    center.setOpaque(true);
    tb.setLayout(new BuVerticalLayout(0, false, false));
    BuPanel tbTool = new BuPanel();
    tbTool.setLayout(new BuButtonLayout(0, SwingConstants.LEFT));
    // -2 pour ne pas mettre les 2 dernieres action "acClick" et "acClickonTarget"
    for (int i = 0; i < l.size() - 2; i++) {
      final EbliActionInterface ac = (EbliActionInterface) l.get(i);
      if (ac != null) {
        tbTool.add(ac.buildToolButton(EbliComponentFactory.INSTANCE));
      }
    }
    tb.add(tbTool);
    tbTool = new BuPanel();
    tbTool.setLayout(new BuButtonLayout(0, SwingConstants.LEFT));
    final AbstractButton btNewPoint = acClick.buildButton(EbliComponentFactory.INSTANCE);
    tbTool = new BuPanel();
    tbTool.setLayout(new BuButtonLayout(0, SwingConstants.LEFT));
    zCalqueClickInteraction.setGele(true);
    tbTool.add(acClickonTarget.buildButton(EbliComponentFactory.INSTANCE));
    tb.add(tbTool);
    center.add(tb, BuBorderLayout.NORTH);
    add(center, BuBorderLayout.CENTER);
    final ZCalqueDeplacementInteraction move = new ZCalqueDeplacementInteraction(pn_.getCqSelectionI());
    move.setGele(true);
    pn_.addCalqueInteraction(move);
    move.setTarget(new ZCalqueDeplacementTargetInterface() {
      @Override
      public void paintMove(final Graphics2D _g, final int _dxEcran, final int _dyEcran, final TraceIcon _ic) {
        controller.getCalageCalque().paintDeplacement(_g, _dxEcran, _dyEcran, _ic);
      }

      @Override
      public void moved(final double _dx, final double _dy, double _dz, boolean _confirmOnZ) {
        controller.getCalageCalque().moveSelectedObjects(_dx, _dy, _dz, calageLogic.getMng(), calageLogic.getUi());
      }

      @Override
      public ZScene getSupport() {
        return controller.getPn().getScene();
      }
    });
    final CalqueActionInteraction moveAction = new CalqueActionInteraction(FSigLib.getS("D�placer un point"),
        EbliResource.EBLI.getIcon("draw-move"), "MOVE", move);
    add(calageLogic.createEditorPanel(btNewPoint, moveAction.buildButton(EbliComponentFactory.INSTANCE)),
        BuBorderLayout.EAST);
    lbHeight = new JLabel();
    lbWidth = new JLabel();
    JPanel pnSize = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 10));
    pnSize.add(lbWidth);
    pnSize.add(lbHeight);
    add(pnSize, BuBorderLayout.SOUTH);

    btNewPoint.setSelected(false);
    acClickonTarget.setSelected(false);
    calageLogic.addTmpLayer(pn_);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  ZEbliCalquesPanel cqDest_;

  public void end() {
    if (cqDest_ != null) {
      cqDest_.removeCalqueInteraction(zCalqueClickInteraction);
    }
  }

  @Override
  public String getStepText() {
    return FSigLib.getS("Editer les correspondances entre les coordonn�es en pixels et coordonn�es r�elles.")
        + CtuluLibString.LINE_SEP_SIMPLE
        + FSigLib
        .getS("Activer le bouton 'Ajouter un point' et cliquer sur l'image pour r�cup�rer les coordonn�es en pixels")
        + CtuluLibString.LINE_SEP_SIMPLE
        + FSigLib.getS("Utiliser le bouton 'Saisir un point r�el' r�cup�rer les coordonn�es r�elles");
  }

  @Override
  public String getStepTitle() {
    return FSigLib.getS("G�or�f�rencer l'image");
  }

  @Override
  public void pointClicked(final GrPoint _ptReel) {
    if (click_.isGele()) {
      return;
    }
    calageLogic.ptClickedInImg((int) _ptReel.x_, (int) _ptReel.y_);
  }

  public void restaurer() {
    pn_.restaurer();
  }

  public void setImage(final CtuluImageContainer _img) {
    final ZModeleImageRaster img = new ZModeleStatiqueImageRaster(_img);
    // img.setProjeMaxTaille(400);
    image_.setModele(img);
    calageLogic.setHImg(img.getHImg());
    image_.repaint();
    lbHeight.setText(FSigLib.getS("Hauteur: {0} px", Integer.toString(_img.getImageHeight())));
    lbHeight.setToolTipText(lbHeight.getText());
    lbWidth.setText(FSigLib.getS("Largeur: {0} px", Integer.toString(_img.getImageWidth())));
    lbWidth.setToolTipText(lbHeight.getText());
  }

  public BVueCalque getCqDest() {
    return cqDest_.getVueCalque();
  }

  public void setCqDest(final ZEbliCalquesPanel _cqDest) {
    if (cqDest_ == _cqDest) {
      return;
    }
    end();
    cqDest_ = _cqDest;
    cqDest_.addCalqueInteraction(zCalqueClickInteraction);
  }

  public void mouseClickedOnReal(final GrPoint _ptReel) {
    calageLogic.ptClickedInReel(_ptReel.x_, _ptReel.y_);
  }

  public void setRasterData(final RasterDataInterface _data) {
    calageLogic.setRasterData(_data);
  }
}
