/*
 * @creation 8 juin 2005
 * 
 * @modification $Date: 2007-05-04 14:00:26 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModelAttributeAdapter;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.mesure.DodicoCsvReader;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author Fred Deniger
 * @version $Id: FSigFileLoaderCsv.java,v 1.2 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigFileLoaderCsv implements FSigFileLoaderI {
  
  final BuFileFilter ft_;
  private GISAttributeDouble firstColonne;
  
  public final Map getOptions() {
    return options_;
  }
  
  public final void setOptions(final Map _options) {
    options_ = _options;
  }
  
  void setFirstColonneAttribute(GISAttributeDouble csvFirstColonne) {
    this.firstColonne = csvFirstColonne;
  }

  /**
   * @author fred deniger
   * @version $Id: FSigFileLoaderCsv.java,v 1.2 2007-05-04 14:00:26 deniger Exp $
   */
  static final class CsvFileFilter extends BuFileFilter {

    /**
     * @param _filters
     * @param _description
     */
    CsvFileFilter(String[] _filters, String _description) {
      super(_filters, _description);
    }
    
    @Override
    public boolean accept(final File _f) {
      return _f != null;
    }
    
    @Override
    public boolean accept(final File _d, final String _fn) {
      return !new File(_fn).isDirectory();
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: FSigFileLoaderCsv.java,v 1.2 2007-05-04 14:00:26 deniger Exp $
   */
  public static class XYCsvReader extends DodicoCsvReader {
    
    public XYCsvReader() {
      setNumeric(false);
      setBlankValid(true);
      super.minValueByLine_ = 2;
    }
    
    @Override
    public String[] getName() {
      if (reader_ == null) {
        return null;
      }
      String[] names = reader_.getName();
      if (names == null) {
        names = new String[reader_.getNbValues()];
        if (names.length < 2) {
          return null;
        }
        names[0] = "X";
        names[1] = "Y";
        for (int i = 2; i < names.length; i++) {
          names[i] = "V_" + CtuluLibString.getString(i - 1);
        }
        reader_.setName(names);
      }
      return reader_.getName();
    }
    
    public String containsError() {
      final String[][] r = getPreview();
      if (r == null || r.length == 0 || r[0].length < 2) {
        return FSigLib.getS("Le fichier doit contenir 2 champs (X,Y) au minimum");
      }
      if (colTypes_ == null || colTypes_[0] == DodicoCsvReader.COL_TYPE_STRING
              || colTypes_[1] == DodicoCsvReader.COL_TYPE_STRING) {
        return FSigLib.getS("Les champs (X,Y) doivent �tre de type num�rique");
      }
      
      return null;
    }
    
    @Override
    protected String[][] internalRead() {
      analyze_.clear();
      final String[][] r = (String[][]) super.internalRead();
      String error = containsError();
      if (error != null) {
        analyze_.addFatalError(error);
        return null;
      }
      return r;
    }
  }
  
  public FSigFileLoaderCsv() {
    ft_ = new CsvFileFilter(new String[]{"txt", "csv", "*"}, FudaaLib.getS("Fichier csv"));
  }
  Map options_;
  
  private FSigFileLoaderCsv(final BuFileFilter _ft) {
    ft_ = _ft;
  }
  
  @Override
  public FSigFileLoaderI createNew() {
    final FSigFileLoaderCsv fSigFileLoaderCsv = new FSigFileLoaderCsv(ft_);
    fSigFileLoaderCsv.setFirstColonneAttribute(firstColonne);
    return fSigFileLoaderCsv;
  }
  
  @Override
  public BuFileFilter getFileFilter() {
    return ft_;
  }
  
  @Override
  public void setInResult(final FSigFileLoadResult _r, final File _f, String _fileOrigine, final ProgressionInterface _prog,
          final CtuluAnalyze _analyze) {

    // Lecture du fichier
    final XYCsvReader reader = new XYCsvReader();
    final CtuluDoubleParser parser = new CtuluDoubleParser();
    parser.setEmptyIsNull(true);
    reader.initFromOption(options_);
    reader.setAddCommentInResult(true);
    final CtuluIOOperationSynthese op = reader.read(_f, _prog);
    if (op.containsMessages() && op.getAnalyze().containsErrors()) {
      _analyze.merge(op.getAnalyze());
      return;
    }

    // Suppression des lignes invalides (X ou Y non Double) et conversion des
    // String en Double pour les colonnes de ce type.
    String[][] vals = (String[][]) op.getSource();
    if (vals.length == 0) {
      return;
    }
    
    ArrayList[] lvals = new ArrayList[vals.length];
    for (int i = 0; i < lvals.length; i++) {
      lvals[i] = new ArrayList(Arrays.asList(vals[i]));
    }
    
    int[] colTypes = reader.getColTypes();
    for (int i = 0; i < lvals.length; i++) {
      if (colTypes[i] == DodicoCsvReader.COL_TYPE_STRING) {
        continue;
      }
      
      for (int j = 0; j < lvals[i].size();) {
        try {
          lvals[i].set(j, Double.parseDouble((String) lvals[i].get(j)));
          j++;
        } catch (NumberFormatException _exc) {
          for (int k = 0; k < lvals.length; k++) {
            lvals[k].remove(j);
          }
        }
      }
    }

    // Cr�ation des points
    final GISPoint[] pt = new GISPoint[lvals[0].size()];
    for (int i = 0; i < pt.length; i++) {
      pt[i] = new GISPoint((Double) lvals[0].get(i), (Double) lvals[1].get(i), 0);
    }

    // Cr�ation des attributs
    List<Object[]> attVals = new ArrayList<Object[]>();
    
    final String[] name = reader.getName();
    GISAttributeInterface[] att = new GISAttributeInterface[lvals.length - 2];
    if (lvals.length > 2) { // On ne prend pas le x et le y
      for (int i = 0; i < att.length; i++) {
        final int real = i + 2;
        if (i == 0 && firstColonne != null) {
          att[i] = firstColonne;
          attVals.add(lvals[real].toArray(new Double[0]));
          continue;
        }
        if (colTypes[real] == DodicoCsvReader.COL_TYPE_STRING) {
          att[i] = _r.findOrCreateAttribute(name.length > real ? name[real]
                  : "S_" + (i + 1), String.class, true);
          attVals.add(lvals[real].toArray(new String[0]));
        } else {
          att[i] = _r.findOrCreateAttribute(name.length > real ? name[real]
                  : "V_" + (i + 1), Double.class, true);
          attVals.add(lvals[real].toArray(new Double[0]));
        }
      }
      _r.addUsedAttributes(att);
    }
    
    GISZoneCollectionPoint zone = new GISZoneCollectionPoint();
    zone.setAttributes(att, null);
    zone.addAll(pt, attVals, null);

    // Ajout de l'attribut ETAT_GEOM
    _r.findOrCreateAttribute(GISAttributeConstants.ETAT_GEOM.getID(), String.class, false);
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zone);
    adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, _fileOrigine);
    
    _r.nbPoint_ += pt.length;
    _r.nbPointTotal_ += pt.length;
    _r.pointModel_.add(adapter);
  }
}
