/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2007-04-30 14:22:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.geotools.data.FeatureSource;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.feature.FeatureIterator;
import org.geotools.wfs.GML;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Fred Deniger
 * @version $Id: FSigDataModelStoreAdapter.java,v 1.3 2007-04-30 14:22:38 deniger Exp $
 */
public abstract class AbstractFSigDataModelSIGAdapter implements GISDataModel {
  List<GISAttributeInterface> atts_;
  int[] idxInSrc_;
  final List<Geometry> geometries;
  final SimpleFeatureType features;
  Object[][] cacheAtt_;

  public static AbstractFSigDataModelSIGAdapter create(GISFileFormat fileFormat, final File file, final ProgressionInterface progressionInterface) throws IOException {
    if (fileFormat.isGML()) {
      return FSigDataModelGMLAdapter.create( file, GML.Version.GML2 ,progressionInterface);
    }
    return FSigDataModelStoreAdapter.create(fileFormat.getDataStoreFactorySpi(), file, progressionInterface);
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GisDataModelTranslated(this, xyToAdd);
  }

  protected abstract FeatureIterator<SimpleFeature> createIterator() throws IOException;

  public AbstractFSigDataModelSIGAdapter(List<Geometry> geometries, SimpleFeatureType features_) {
    this.geometries = geometries;
    this.features = features_;
  }

  public void cache(final int[] _idx, final ProgressionInterface _prog) {
    buildAttributes();
    if (CtuluLibArray.isEmpty(atts_) || CtuluLibArray.isEmpty(_idx) || idxInSrc_ == null) {
      return;
    }
    if (cacheAtt_ == null) {
      cacheAtt_ = new Object[atts_.size()][];
    }

    boolean allDone = true;
    for (int i = _idx.length - 1; i >= 0; i--) {
      final int attIdx = _idx[i];
      // pas d'attribut trouv�
      if (attIdx < 0) {
        continue;
      }
      if (cacheAtt_[attIdx] == null) {
        allDone = false;
        cacheAtt_[attIdx] = new Object[geometries.size()];
      }
    }
    if (allDone) {
      return;
    }

    try {
      try (FeatureIterator<SimpleFeature> reader = createIterator()) {
        int idx = 0;
        final ProgressionUpdater up = new ProgressionUpdater(_prog);
        up.setValue(10, getNumGeometries());
        while (reader.hasNext()) {
          final SimpleFeature feature = reader.next();
          final Geometry defaultGeometry = (Geometry) feature.getDefaultGeometry();
          if (defaultGeometry.getNumPoints() <= 0) {
            continue;
          }
          final int nbGeom = defaultGeometry.getNumGeometries();
          // pour cette geometrie on parcourt tous les attributs � mettre en cache
          for (int i = _idx.length - 1; i >= 0; i--) {
            final int attIdx = _idx[i];
            if (attIdx < 0 || attIdx >= idxInSrc_.length) {
              continue;
            }
            final int srcIdx = idxInSrc_[attIdx];
            // la geometrie comporte d'autre g�ometrie
            if (nbGeom > 0) {
              // on les met � jour
              for (int k = 0; k < nbGeom; k++) {
                cacheAtt_[attIdx][idx + k] = feature.getAttribute(srcIdx);
                up.majAvancement();
              }
            } else {
              cacheAtt_[attIdx][idx] = feature.getAttribute(srcIdx);
              up.majAvancement();
            }
          }
          // on augmente le compteur
          // si plusieurs geo on �t� lues,on les ajouters
          if (nbGeom > 0) {
            idx += nbGeom;
          } else {
            // si incr�ment simple
            idx++;
          }
        }
      } catch (IOException e) {
        FuLog.error(e);
      }
    } catch (final NoSuchElementException _evt) {
      FuLog.error(_evt);
    }
  }

  protected static List<Geometry> read(FeatureIterator<SimpleFeature> iterator, final ProgressionInterface _prog) {
    List<Geometry> readGeometries = new ArrayList<>();
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(4, 1000);
    while (iterator.hasNext()) {
      final SimpleFeature feature = iterator.next();
      final Geometry g = (Geometry) feature.getDefaultGeometry();
      if (g.getNumPoints() <= 0) {
        continue;
      }
      final int numGeometries = g.getNumGeometries();
      if (numGeometries > 0) {
        for (int i = 0; i < numGeometries; i++) {
          readGeometries.add(GISGeometryFactory.INSTANCE.transformToCustom(g.getGeometryN(i)));
        }
      } else {
        readGeometries.add(GISGeometryFactory.INSTANCE.transformToCustom(g));
      }
      up.majAvancement();
    }
    return readGeometries;
  }

  protected void buildAttributes() {

    if (atts_ == null) {
      final int nb = features.getAttributeCount();
      atts_ = new ArrayList<>(nb);
      final TIntArrayList idx = new TIntArrayList(nb);
      for (int i = 0; i < nb; i++) {
        final AttributeDescriptor ti = features.getDescriptor(i);
        final GISAttributeInterface att = GISLib.createAttributeFrom(ti);
        final boolean ok = att != null;
        if (att != null) {
          atts_.add(att);
        }
        if (ok) {
          idx.add(i);
        }
      }
      idxInSrc_ = idx.toNativeArray();
    }
  }

  protected void buildAttributes(final FSigFileLoadResult _r) {
    if (atts_ == null) {
      final int nb = features.getAttributeCount();
      atts_ = new ArrayList<>(nb);
      final TIntArrayList idx = new TIntArrayList(nb);
      for (int i = 0; i < nb; i++) {
        final AttributeDescriptor ti = features.getDescriptor(i);
        final GISAttributeInterface att = _r.findOrCreateAttribute(ti.getLocalName(), ti.getType().getBinding(), false);
        if (att != null) {
          atts_.add(att);
          idx.add(i);
        }
      }
      idxInSrc_ = idx.toNativeArray();
    }
  }

  protected void replacePolygonWithLine(final int _i) {
    geometries.set(_i, ((Polygon) geometries.get(_i)).getExteriorRing());
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    buildAttributes();
    return atts_.get(_idxAtt);
  }

  public Object[] getDataModel(final int _idxAtt) {
    buildAttributes();
    if (idxInSrc_ == null) {
      return null;
    }
    if (isCache(_idxAtt)) {
      return CtuluLibArray.copy(cacheAtt_[_idxAtt]);
    }
    final int realIdx = idxInSrc_[_idxAtt];
    final Object[] res = new Object[getNumGeometries()];
    try {
      try (FeatureIterator<SimpleFeature> it = createIterator()) {
        int idx = 0;
        while (it.hasNext()) {
          final SimpleFeature feature = it.next();
          final Geometry defaultGeometry = (Geometry) feature.getDefaultGeometry();
          // geometrie null, on oublie
          if (defaultGeometry.getNumPoints() <= 0) {
            continue;
          }
          final int nbGeom = defaultGeometry.getNumGeometries();
          final Object attribute = feature.getAttribute(realIdx);
          if (nbGeom > 0) {
            for (int i = 0; i < nbGeom; i++) {
              res[idx++] = attribute;
            }
          } else {
            res[idx++] = attribute;
          }
        }
      }
    } catch (final Exception e) {
      if (FuLog.isTrace()) {
        FuLog.warning(e);
      }
    }
    return res;
  }

  protected static List<Geometry> loadGeometries(SimpleFeatureCollection featureCollection, final ProgressionInterface _prog) {
    List<Geometry> geometries = new ArrayList<>();
    try (SimpleFeatureIterator iterator = featureCollection.features()) {
      geometries = read(iterator, _prog);
    } catch (final Exception _e) {
      if (FuLog.isTrace()) {
        FuLog.warning(_e);
      }
    }
    return geometries;
  }

  protected static List<Geometry> loadGeometries(FeatureSource<SimpleFeatureType, SimpleFeature> source, final ProgressionInterface _prog) {
    List<Geometry> geometries = new ArrayList<>();
    try (FeatureIterator<SimpleFeature> iterator = source.getFeatures().features()) {
      geometries = read(iterator, _prog);
    } catch (final Exception _e) {
      if (FuLog.isTrace()) {
        FuLog.warning(_e);
      }
    }
    return geometries;
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    final Object o = getValue(_idxAtt, _idxGeom);
    if (o != null) {
      return ((Number) o).doubleValue();
    }
    return 0;
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return GISLib.computeEnveloppe(geometries);
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return geometries.get(_idxGeom);
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    if (_att == null) {
      return -1;
    }
    buildAttributes();
    return atts_.indexOf(_att);
  }

  @Override
  public int getNbAttributes() {
    buildAttributes();
    return atts_ == null ? 0 : atts_.size();
  }

  @Override
  public int getNumGeometries() {
    return geometries.size();
  }

  private Object getCache(final int _idxAtt, final int _geom) {
    return cacheAtt_[_idxAtt][_geom];
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    if (isCache(_idxAtt)) {
      return getCache(_idxAtt, _idxGeom);
    }
    buildAttributes();
    final int realIdx = idxInSrc_[_idxAtt];
    try {
      SimpleFeature feature = null;
      try (FeatureIterator<SimpleFeature> reader = createIterator()) {
        int idx = 0;

        while (reader.hasNext() && idx <= _idxGeom) {
          feature = reader.next();
          final Geometry g = (Geometry) feature.getDefaultGeometry();
          if (g.getNumPoints() <= 0) {
            continue;
          }

          if (g.getNumGeometries() > 0) {
            idx += g.getNumGeometries();
          } else {
            idx++;
          }
        }
      }
      if (feature != null) {
        return feature.getAttribute(realIdx);
      }
    } catch (final Exception _e) {
      if (FuLog.isTrace()) {
        FuLog.warning(_e);
      }
    }
    return null;
  }

  public boolean isCache(final int _idx) {
    return cacheAtt_ != null && cacheAtt_[_idx] != null;
  }

  @Override
  public void preload(final GISAttributeInterface[] _idx, final ProgressionInterface _prog) {
    if (CtuluLibArray.isEmpty(_idx)) {
      return;
    }
    final int[] res = new int[_idx.length];
    // true si au moins un attribut est trouv�
    boolean ok = false;
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getIndiceOf(_idx[i]);
      if (res[i] >= 0) {
        ok = true;
      }
    }
    if (ok) {
      cache(res, _prog);
    }
  }
}
