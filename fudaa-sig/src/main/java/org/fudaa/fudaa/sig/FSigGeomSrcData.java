/*
 * @creation 31 mai 2005
 * 
 * @modification $Date: 2007-05-04 14:00:26 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuBorderLayout;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Polygon;
import java.awt.Color;
import java.awt.Dialog;
import java.util.HashSet;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometry;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISVisitorChooser;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPolygone;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModelClassLayerFilter;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.sig.wizard.FSigWizardImportHelper;

/**
 * @author Fred Deniger
 * @version $Id: FSigGeomSrcData.java,v 1.14 2007-05-04 14:00:26 deniger Exp $
 */
public class FSigGeomSrcData {

  int nbPoints_;

  int nbPointsTotal_;

  int nbPolygones_;

  int nbPolylignes_;

  GISDataModel[] points_;

  GISDataModel[] polygones_;

  GISDataModel[] polylignes_;

  public boolean containsDataFor(final GISAttributeInterface toFind) {
    return containsDataFor(toFind, points_) || containsDataFor(toFind, polygones_)
        || containsDataFor(toFind, polylignes_);

  }

  public static boolean containsDataFor(final GISAttributeInterface toFind, final GISDataModel[] models) {
    if (models == null) { return false; }
    for (final GISDataModel gisDataModel : models) {
      if (gisDataModel.getIndiceOf(toFind) >= 0) { return true; }
    }
    return false;

  }

  private void preload(final GISAttributeInterface[] _att, final GISDataModel[] _models,
      final ProgressionInterface _prog) {

    if (_models != null) {
      for (int i = _models.length - 1; i >= 0; i--) {
        _models[i].preload(_att, _prog);
      }
    }

  }

  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
    preload(_att, points_, _prog);
    preload(_att, polygones_, _prog);
    preload(_att, polylignes_, _prog);
  }

  private void previsu(final Dialog _d, final CtuluUI _parent, final CtuluDialogPanel _dest,
      final GISPolygone[] _zoneCible) {
    final BGroupeCalque gc = new BGroupeCalque();
    final Set doneLigneBrisee = new HashSet();
    final GISZoneCollectionPolygone polygone = new GISZoneCollectionPolygone(null);
    final int nb = _zoneCible.length;

    for (int i = 0; i < nb; i++) {
      polygone.addPolygone(_zoneCible[i], null);
    }

    final ZModeleLigneBriseeDefault model = new ZModeleLigneBriseeDefault(polygone);
    final ZCalqueLigneBrisee lay = new ZCalqueLigneBrisee(model);
    lay.setIconModel(0, null);
    final GISZoneCollectionLigneBrisee dest = new GISZoneCollectionLigneBrisee(null);
    final GISVisitorChooser chooser = new GISVisitorChooser();
    if (polygones_ != null) {
      previsuForPolygones(doneLigneBrisee, dest, chooser);
    }
    if (polylignes_ != null) {

      previsuForPolylignes(doneLigneBrisee, dest, chooser);
    }
    final Envelope envConvex = new Envelope();
    // List convex = new ArrayList();
    if (dest.getNumGeometries() > 0) {
      final ZCalqueLigneBrisee cq = FSigGeomSrcDataUtils.buildPrevisuCq(dest);
      gc.add(cq);
      envConvex.expandToInclude(dest.getEnvelopeInternal());
      // convex.add(dest);
    }
    // final Envelope ptRing = new Envelope();
    // if (points_ != null) {
    // // List geoms = new ArrayList();
    // for (int i = points_.length - 1; i >= 0; i--) {
    // for (int k = points_[i].getNumGeometries() - 1; k >= 0; k--) {
    // ptRing.expandToInclude(points_[i].getGeometry(k).getEnvelopeInternal());
    // }
    // }
    // if (!ptRing.isNull()) {
    // envConvex.expandToInclude(ptRing);
    // final ZCalqueLigneBrisee cq = FSigGeomSrcDataUtils.buildPrevisuDomaines(ptRing);
    // gc.add(cq);
    // }
    // }
    if (!envConvex.isNull()) {
      final ZCalqueLigneBrisee cq = FSigGeomSrcDataUtils.buildPrevisuDomaineTotal(envConvex);
      gc.add(cq);
    }
    lay.setForeground(Color.BLUE.darker());
    lay.getLineModel(0).setTypeTrait(TraceLigne.LISSE);
    lay.setTitle(FudaaLib.getS("Projet"));
    gc.add(lay);
    SwingUtilities.invokeLater(FSigGeomSrcDataUtils.buildRunnableForPrevisu(_d, _parent, _dest, gc));
  }

  protected void previsuForPolylignes(final Set _doneLigneBrisee, final GISZoneCollectionLigneBrisee _dest,
      final GISVisitorChooser _chooser) {
    for (int i = 0; i < polylignes_.length; i++) {
      if (!_doneLigneBrisee.contains(polylignes_[i])) {
        _doneLigneBrisee.add(polylignes_[i]);
        final int nbPol = polylignes_[i].getNumGeometries();
        for (int j = 0; j < nbPol; j++) {
          final Geometry geom = polylignes_[i].getGeometry(j);
          ((GISGeometry) geom).accept(_chooser);
          if (_chooser.isPolyligne()) {
            _dest.addCoordinateSequence(((LineString) (geom)).getCoordinateSequence(), null, null);
          }
        }
      }
    }
  }

  protected void previsuForPolygones(final Set _doneLigneBrisee, final GISZoneCollectionLigneBrisee _dest,
      final GISVisitorChooser _chooser) {
    for (int i = 0; i < polygones_.length; i++) {
      if (!_doneLigneBrisee.contains(polygones_[i])) {
        _doneLigneBrisee.add(polygones_[i]);
        final int nbPol = polygones_[i].getNumGeometries();
        for (int j = 0; j < nbPol; j++) {
          final Geometry geom = polygones_[i].getGeometry(j);
          ((GISGeometry) geom).accept(_chooser);
          if (_chooser.isPolygoneWithHole()) {
            _dest.addCoordinateSequence((((Polygon) (geom))).getExteriorRing().getCoordinateSequence(), null, null);

          } else if (_chooser.isPolygone()) {
            _dest.addCoordinateSequence(((LineString) (geom)).getCoordinateSequence(), null, null);
          }
        }
      }
    }
  }

  public final int getNbPoints() {
    return nbPoints_;
  }

  public final int getNbPointsTotal() {
    return nbPointsTotal_;
  }

  public final int getNbPolygones() {
    return nbPolygones_;
  }

  public final int getNbPolylignes() {
    return nbPolylignes_;
  }

  public final GISDataModel[] getPoints() {
    return points_;
  }

  public final GISDataModel[] getPolygones() {
    return polygones_;
  }

  public final GISDataModel[] getPolylignes() {
    return polylignes_;
  }

  public final void setNbPoints(final int _nbPoints) {
    nbPoints_ = _nbPoints;
  }

  public final void setNbPointsTotal(final int _nbPointsTotal) {
    nbPointsTotal_ = _nbPointsTotal;
  }

  public final void setNbPolygones(final int _nbPolygones) {
    nbPolygones_ = _nbPolygones;
  }

  public final void setNbPolylignes(final int _nbPolylignes) {
    nbPolylignes_ = _nbPolylignes;
  }

  public final void setPoints(final GISDataModel[] _points) {
    points_ = _points;
  }

  public final void setPolygones(final GISDataModel[] _polygones) {
    polygones_ = _polygones;
  }

  public final void setPolylignes(final GISDataModel[] _polylignes) {
    polylignes_ = _polylignes;
  }

  public void fill(final BCalque[] _cq) {
    final FSigGeomDataClosedLayerFilter filter = new FSigGeomDataClosedLayerFilter();
    final ZModelClassLayerFilter classfilter = new ZModelClassLayerFilter(GISZoneCollectionPoint.class);
    final int nb = _cq.length;
    for (int i = 0; i < nb; i++) {
      _cq[i].apply(filter);
      _cq[i].apply(classfilter);
    }
    // les geometries selectionnées qui vont bien
    polygones_ = filter.getGeomClosedDataModel();
    polylignes_ = filter.getGeomDataModel();
    nbPolygones_ = filter.getNbPolygones();
    nbPolylignes_ = filter.getNbPolylignes();
    points_ = new GISDataModel[classfilter.getNbLayerFound()];
    classfilter.fillArrayModel(points_);
    nbPoints_ = GISLib.getNbGeometries(points_);
    nbPointsTotal_ = nbPoints_ + filter.getNbPointTotal();
  }

  public String getHtmlDesc() {
    final StringBuffer r = new StringBuffer(
        "<html><body><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td>");
    final String sepLine = "</td><tr><tr><td>";
    final String sepCell = "</td><td>";
    r.append(EbliLib.getS("Nombre de points:")).append(sepCell).append(CtuluLibString.getString(nbPoints_));
    r.append(sepLine);
    r.append(EbliLib.getS("Nombre de lignes:")).append(sepCell).append(CtuluLibString.getString(nbPolylignes_));
    r.append(sepLine);
    r.append(EbliLib.getS("Nombre de lignes fermées:")).append(sepCell).append(CtuluLibString.getString(nbPolygones_));
    r.append(sepLine);
    r.append(EbliLib.getS("Nombre de points total:")).append(sepCell).append(CtuluLibString.getString(nbPointsTotal_));
    return r.append("</td></tr></table></body></html>").toString();
  }

  public boolean containsLignes() {
    return nbPolygones_ > 0 || nbPolylignes_ > 0;
  }

  public void showPreview(final Dialog _d, final ProgressionInterface _prog, final GISPolygone[] _zoneCible,
      final CtuluUI _parent) {
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout(0, 10, true, true));
    final FSigWizardImportHelper.InfoPanel pnInfo = new FSigWizardImportHelper.InfoPanel();
    pnInfo.setSrc(this);
    pn.add(pnInfo, BuBorderLayout.NORTH);
    previsu(_d, _parent, pn, _zoneCible);
  }
}
