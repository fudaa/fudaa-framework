/*
 * @creation 24 juil. 06
 * @modification $Date: 2008-02-22 16:28:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.*;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.image.CtuluImageIconFixeSize;
import org.fudaa.fudaa.sig.FSigLib;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * L'etape de chargement de l'image pour le wizard d'import d'image.
 *
 * @author fred deniger
 * @version $Id: FSigImageWizardStepImage.java,v 1.3.6.1 2008-02-22 16:28:58 bmarchan Exp $
 */
public class FSigImageWizardStepImage extends FSigWizardDefaultPanel implements DocumentListener,
    FSigWizardStepInterface {
  static int idxForImageName;
  BuButton btPrevisu_;
  final BuCheckBox cbAuto_;
  final CtuluImageIconFixeSize icon_ = new CtuluImageIconFixeSize(200, null);
  CtuluImageContainer imgReader_;
  boolean isLoaded_;
  File lastFile_;
  File tabFile_;
  final BuLabel lbFile_;
  final BuLabel lbTabFile_;
  final BuLabel lbHeight_;
  final BuLabel lbWidth_;
  final BuLabel lbIcone_;
  final BuLabel lbName_;
  final CtuluFileChooserPanel txtFile_;
  final CtuluFileChooserPanel tabFileChooser_;
  final BuTextField txtName_;

  public final void setData(final RasterDataInterface _data, final String _calqueName) {
    isUpdatingFromModel_ = true;
    if (_data.getImgFile() != null) {
      txtFile_.getTf().setText(_data.getImgFile().getAbsolutePath());
    }
    imgReader_ = _data.getImg();
    isLoaded_ = true;
    lastFile_ = _data.getImgFile();
    data_ = _data;
    icon_.setImg(_data.getImg());
    txtName_.setText(_calqueName);
    lbIcone_.repaint();
    updateImageInfos();
  }

  public FSigImageWizardStepImage() {
    lbName_ = new BuLabel(FSigLib.getS("Le nom du calque"));
    String txt = BuResource.BU.getString("Image");
    if (idxForImageName > 0) {
      txt += CtuluLibString.getEspaceString(idxForImageName);
    }
    txtName_ = new BuTextField(txt);
    txtName_.getDocument().addDocumentListener(this);
    final BuPanel data = new BuPanel();
    data.setLayout(new BuGridLayout(2, 2, 2));
    data.add(lbName_);
    data.add(txtName_);
    txtFile_ = new CtuluFileChooserPanel();
    tabFileChooser_ = new CtuluFileChooserPanel();
    txtFile_.setFileSelectMode(JFileChooser.FILES_ONLY);
    tabFileChooser_.setFileSelectMode(JFileChooser.FILES_ONLY);
    final BuFileFilter imgFilter = new BuFileFilter((String[]) CtuluImageExport.FORMAT_LIST
        .toArray(new String[CtuluImageExport.FORMAT_LIST.size()]), BuResource.BU.getString("Images"));
    imgFilter.setExtensionListInDescription(false);
    final BuFileFilter datFilter = new BuFileFilter("tab", FSigLib.getS("Fichier TAB Mapinfo"));
    txtFile_.setFilter(new BuFileFilter[]{imgFilter});
    tabFileChooser_.setFilter(new BuFileFilter[]{datFilter});
    tabFileChooser_.setAllFileFilter(false);
    lbFile_ = new BuLabel(FSigLib.getS("L'image � charger"));
    lbTabFile_ = new BuLabel(FSigLib.getS("Le fichier Tab � charger"));
    data.add(lbFile_);
    data.add(txtFile_);
    data.add(lbTabFile_);
    data.add(tabFileChooser_);
    txtFile_.getTf().getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void changedUpdate(final DocumentEvent _e) {
        fileChanged();
      }

      @Override
      public void insertUpdate(final DocumentEvent _e) {
        fileChanged();
      }

      @Override
      public void removeUpdate(final DocumentEvent _e) {
        fileChanged();
      }
    });

    tabFileChooser_.getTf().getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void changedUpdate(final DocumentEvent _e) {
        fileChanged(true);
      }

      @Override
      public void insertUpdate(final DocumentEvent _e) {
        fileChanged(true);
      }

      @Override
      public void removeUpdate(final DocumentEvent _e) {
        fileChanged(true);
      }
    });
    tabFileChooser_.setEnabled(false);
    add(data, BuBorderLayout.CENTER);
    lbIcone_ = new BuLabel(icon_);
    lbHeight_ = new BuLabel();
    lbWidth_ = new BuLabel();
    final BuPanel pnPrevisu = new BuPanel();
    pnPrevisu.setLayout(new BuBorderLayout(1, 1, false, false));
    pnPrevisu.setBorder(CtuluLibSwing.createTitleBorder(FSigLib.getS("Pr�visualisation")));
    pnPrevisu.add(lbIcone_, BuBorderLayout.CENTER);
    btPrevisu_ = new BuButton(BuResource.BU.getIcon("rafraichir"));
    btPrevisu_.setText(FSigLib.getS("Pr�visualiser"));
    btPrevisu_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        loadImage(null, true, false);
      }
    });

    btPrevisu_.setEnabled(false);
    cbAuto_ = new BuCheckBox(FSigLib.getS("Pr�visualiser automatiquement"));
    cbAuto_.setSelected(true);
    lbIcone_.setEnabled(true);
    lbIcone_.setBorder(BorderFactory.createEtchedBorder());
    final BuPanel east = new BuPanel();
    east.setLayout(new BuVerticalLayout(2, true, false));
    east.add(btPrevisu_);
    east.add(cbAuto_);
    east.add(new JSeparator());
    east.add(lbWidth_);
    east.add(lbHeight_);
    pnPrevisu.add(east, BuBorderLayout.EAST);
    cbAuto_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        lbIcone_.setEnabled(cbAuto_.isSelected());
        if (cbAuto_.isSelected() && lastFile_ != null && lastFile_.exists()) {
          loadImage(null, true, false);
        }
      }
    });
    add(pnPrevisu, BuBorderLayout.SOUTH);

    idxForImageName++;
  }

  String getErrorFileNotFound() {
    if (lastFile_ == null) {
      return FSigLib.getS("Pr�ciser un chemin vers l'image � charger.");
    } else if (!lastFile_.exists()) {
      return CtuluLib.getS("Le fichier {0} n'existe pas.", lastFile_.getName());
    }
    return null;
  }

  void loadFile(boolean isTabFile) {

    File file = isTabFile ? tabFile_ : lastFile_;
    if (file == null || !file.exists()) {
      /*
       * String name = lastFile_ == null ? "?" : lastFile_.getName(); CtuluLibDialog.showError(dialog_,
       * FSigLib.getS("Chargement image"), CtuluLib.getS("Le fichier {0} n'existe pas", name));
       */
      return;
    }
    final String name = file.getName();
    final CtuluAnalyze an = new CtuluAnalyze();

    imgReader_ = new CtuluImageContainer(file);
    if (isError(an)) {
      return;
    }
    if (data_ == null) {
      data_ = new RasterData();
    }
    ((RasterData) data_).setImg(imgReader_);
    isLoaded_ = true;
  }

  private boolean isError(final CtuluAnalyze _an) {
    if (_an.containsFatalError()) {
      imgReader_ = null;
      isLoaded_ = true;
      CtuluLibDialog.showError(dialog_, getDialogTitle(), _an.getResume());
      return true;
    }
    return false;
  }

  public static String getDialogTitle() {
    return FSigLib.getS("Chargement image");
  }

  RasterDataInterface data_;

  void loadImage(final BuWizardTask _wizard, final boolean _afficheInIcon, final boolean isTabFile) {
    if (dialog_ != null) {
      final BuGlassPaneStop buGlassPaneStop = new BuGlassPaneStop();
      buGlassPaneStop.add(new BuLabel(getDialogTitle()));
      dialog_.setGlassPane(buGlassPaneStop);
    }
    final Runnable finish = new Runnable() {
      @Override
      public void run() {
        setErrorDefaultText();
        lbHeight_.setText(StringUtils.EMPTY);
        lbHeight_.setToolTipText(StringUtils.EMPTY);
        lbWidth_.setText(StringUtils.EMPTY);
        lbWidth_.setToolTipText(StringUtils.EMPTY);
        if (imgReader_ == null || (!imgReader_.isImageLoaded() && !isTabFile)) {
          String error = getErrorFileNotFound();
          if (error == null) {
            error = FSigLib.getS("Le fichier {0} n'est pas valide.", lastFile_ == null ? "?" : lastFile_.getName());
          }
          CtuluLibDialog.showError(dialog_, getDialogTitle(), error);
          valideData();
        } else {
          if (!isTabFile) {
            updateImageInfos();
          }
        }
        final Component c = dialog_.getGlassPane();
        c.setVisible(false);
        dialog_.remove(c);
        if (_wizard != null && imgReader_ != null && imgReader_.isImageLoaded()) {
          _wizard.nextStep();
        }
        if (_afficheInIcon && !isTabFile) {
          icon_.setImg(imgReader_);
          lbIcone_.repaint();
        }

        if (!isTabFile) {
          enableTabFileChooser();
        }
      }
    };
    final Runnable r = new Runnable() {
      @Override
      public void run() {
        loadFile(isTabFile);
        BuLib.invokeLater(finish);
      }
    };
    new Thread(r).start();
  }

  boolean isUpdatingFromModel_;

  protected void fileChanged() {
    fileChanged(false);
  }

  public void enableTabFileChooser() {
    tabFileChooser_.setEnabled(true);

    //-- try to auto launch the tab file --//
    if (tabFile_ == null && lastFile_ != null) {
      int extension = lastFile_.getAbsolutePath().lastIndexOf(".");
      if (extension != -1) {
        String pathTab = lastFile_.getAbsolutePath().substring(0, extension) + ".tab";
        File fileTab = new File(pathTab);
        if (fileTab.exists()) {
          tabFileChooser_.setFile(fileTab);
          fileChanged(true);
        }
      }
    }
  }

  protected void fileChanged(boolean isTabFile) {
    final File f = isTabFile ? tabFileChooser_.getFile() : txtFile_.getFile();
    if (f != null && f.equals(lastFile_)) {
      return;
    }

    if (isTabFile) {
      tabFile_ = f;
    } else {
      lastFile_ = f;
    }
    imgReader_ = null;
    isLoaded_ = false;
    final boolean isAlreaydError = Color.RED.equals(lbFile_.getForeground());
    String error = getErrorFileNotFound();
    final boolean isError = error != null;
    btPrevisu_.setEnabled(!isError);
    if (isAlreaydError != isError) {
      lbFile_.setForeground(isError ? Color.RED : CtuluLibSwing.getDefaultLabelForegroundColor());
      if (!isError) {
        if (isTabFile) {
          error = FSigLib.getS("Entrer le chemin vers le fichier tab");
        } else {
          error = FSigLib.getS("Entrer le chemin vers le fichier image");
        }
      }
      lbFile_.setToolTipText(error);
      txtFile_.setToolTipText(error);
    }
    if (isError || isUpdatingFromModel_) {
      isUpdatingFromModel_ = false;
      return;
    }

    if (cbAuto_.isSelected()) {
      loadImage(null, true, isTabFile);
    } else {
      icon_.setImg(null);
    }
  }

  public void updateImageInfos() {
    lbHeight_.setText(FSigLib.getS("Hauteur: {0} px", Integer.toString(imgReader_.getImageHeight())));
    lbHeight_.setToolTipText(lbHeight_.getText());
    lbWidth_.setText(FSigLib.getS("Largeur: {0} px", Integer.toString(imgReader_.getImageWidth())));
    lbWidth_.setToolTipText(lbHeight_.getText());
  }

  protected String getErrorEmptyName() {
    final String name = txtName_.getText().trim();
    lbName_.setForeground(name.length() == 0 ? Color.RED : CtuluLibSwing.getDefaultLabelForegroundColor());
    if (name.length() == 0) {
      return FSigLib.getS("Le nom du calque de destination est vide");
    }
    return null;
  }

  protected void nameChanged(final DocumentEvent _e) {
    final boolean isAlreadyError = Color.RED.equals(lbName_.getForeground());
    String error = getErrorEmptyName();
    final boolean isError = error != null;
    if (isError != isAlreadyError) {
      lbName_.setForeground(error == null ? CtuluLibSwing.getDefaultLabelForegroundColor() : Color.RED);
      if (!isError) {
        error = FSigLib.getS("Entrez le nom du calque de destination");
      }
      txtName_.setToolTipText(error);
      lbName_.setToolTipText(error);
    }
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    if (_e.getDocument() == txtName_.getDocument()) {
      nameChanged(_e);
    }
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  public CtuluImageContainer getImg() {
    return imgReader_;
  }

  public String getImgError(final BuWizardTask _wizard) {
    final String notFound = getErrorFileNotFound();
    if (notFound != null) {
      return notFound;
    }
    if (isLoaded_) {
      if (imgReader_ == null) {
        return FSigLib.getS("Le fichier {0} n'est pas valide.", lastFile_.getName());
      }
    } else {
      // on doit charger le tout
      loadImage(_wizard, false, false);
      return FSigLib.getS("Chargement image...");
    }
    return null;
  }

  public File getLastFile() {
    return lastFile_;
  }

  @Override
  public String getStepText() {
    return FSigLib.getS("Choisir le nom du calque � cr�er et l'image � utiliser.");
  }

  @Override
  public String getStepTitle() {
    return FSigLib.getS("Choisir le calque et l'image");
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    if (_e.getDocument() == txtName_.getDocument()) {
      nameChanged(_e);
    }
  }

  public String getCalqueTitle() {
    return txtName_.getText();
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    if (_e.getDocument() == txtName_.getDocument()) {
      nameChanged(_e);
    }
  }

  @Override
  public String valideAndGetError() {
    final String error = getErrorEmptyName();
    final String img = getImgError(null);
    if (error == null) {
      return img;
    }
    if (img == null) {
      return error;
    }
    return error + "<br>" + img;
  }
}
