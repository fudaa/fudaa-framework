/*
 *  @creation     6 juin 2005
 *  @modification $Date: 2008-02-04 17:12:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.ef.io.dunes.DunesMAIFileFormat;
import org.fudaa.dodico.ef.io.trigrid.TrigridFileFormat;
import org.fudaa.dodico.rubar.io.RubarDATFileFormat;
import org.fudaa.dodico.rubar.io.RubarMAIFileFormat;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.sig.FSigLib;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class FSigWizardFileMng {
  /**
   * @author fred deniger
   * @version $Id$
   */
  static final class FilterComparator implements Comparator {
    @Override
    public int compare(Object _o1, Object _o2) {
      return ((BuFileFilter) _o1).getDescription().compareTo(((BuFileFilter) _o2).getDescription());
    }
  }

  /**
   * @author fred deniger
   * @version $Id$
   */
  static final class FmtRowRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      setForeground(_value == null ? Color.RED : Color.BLACK);
      if (_value == null) {
        setText(FSigLib.getS("Non d�fini"));
      } else {
        setText(((BuFileFilter) _value).getDescription());
      }
    }
  }

  static final class FmtRowOrigineRenderer extends CtuluCellTextRenderer {
    @Override
    protected void setValue(final Object _value) {
      setForeground(_value == null ? Color.RED : Color.BLACK);
      if (_value == null) {
        setText(FSigLib.getS("Non d�fini"));
      } else {
        String val = FSigLib.getS("Inconnu");
        if ((String) _value == GISAttributeConstants.ATT_VAL_ETAT_ORIG) {
          val = FSigLib.getS("Origine");
        } else if ((String) _value == GISAttributeConstants.ATT_VAL_ETAT_MODI) {
          val = FSigLib.getS("Modifi�");
        }
        setText(val);
      }
    }
  }

  FSigWizardFileModel model_;
  FSigFileLoadResult res_;
  /**
   * Le file chooser pour tous les fichiers. En variable globale pour conserver la derniere op�ration
   */
  CtuluFileChooser fileChooser_;

  public FSigWizardFileMng() {
    model_ = buildFmt();
    model_.addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(final TableModelEvent _e) {
        res_ = null;
      }
    });
  }

  private void addGridList(final List _filter, final Map _dest) {
    final List r = getGridVerions();
    for (int i = r.size() - 1; i >= 0; i--) {
      final FileFormatGridVersion vers = (FileFormatGridVersion) r.get(i);
      final BuFileFilter ft = vers.getFileFormat().createFileFilter();
      if (ft != null) {
        _filter.add(ft);
        _dest.put(ft, new FSigFileLoaderGrid(ft, vers));
      }
    }
  }

  private FSigWizardFileModel buildFmt() {
    final List r = new ArrayList();
    final Map fmtLoader = new HashMap();
    // les format sig
    final Map<BuFileFilter, GISFileFormat> gisFmt = GISExportDataStoreFactory.buildFileFilterMap();
    for (final Iterator<Map.Entry<BuFileFilter, GISFileFormat>> it = gisFmt.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry<BuFileFilter, GISFileFormat> e = it.next();
      final GISFileFormat spi = e.getValue();
      fmtLoader.put(e.getKey(), new FSigFileLoaderGIS(e.getKey(), spi));
    }
    r.addAll(gisFmt.keySet());
    // le format sinusx
    addLoader(r, fmtLoader, new FSigFileLoaderSinusX());
    // le format serafin
    addLoader(r, fmtLoader, new FSigFileLoaderSerafin());
    // le format inp
    addLoader(r, fmtLoader, new FSigFileLoaderInp());
    // le format csv a part le s...
    final FSigFileLoaderCsv csv = new FSigFileLoaderCsv();
    final BuFileFilter csvFileFilter = csv.getFileFilter();
    r.add(csvFileFilter);
    fmtLoader.put(csvFileFilter, csv);

    //-- le format Z ESRI --// 
    addLoader(r, fmtLoader, new FSigFileLoaderZESRI());
    // le format rubar sem
    addLoader(r, fmtLoader, new FSigFileLoaderRubarSem());
    // le format st,cn
    addLoader(r, fmtLoader, new FSigFileLoaderRubarSt());
    // format cox
    addLoader(r, fmtLoader, new FSigFileLoaderRubarCox());
    // format inx
    addLoader(r, fmtLoader, new FSigFileLoaderRubarInx());
    // reflucad semis, berges, profils
    addLoader(r, fmtLoader, new FSigFileLoaderReflucadBER());
    addLoader(r, fmtLoader, new FSigFileLoaderReflucadSEM());
    addLoader(r, fmtLoader, new FSigFileLoaderReflucadPRO());
    // reflux solution
    addLoader(r, fmtLoader, new FSigFileLoaderRefluxRefondeResult());
    // Mascaret
    addLoader(r, fmtLoader, new FSigFileLoaderMascaret());

    // les maillages
    addGridList(r, fmtLoader);
    final BuFileFilter[] filters = new BuFileFilter[r.size()];
    r.toArray(filters);
    final Comparator c = new FilterComparator();
    Arrays.sort(filters, c);
    return new FSigWizardFileModel(fmtLoader, filters, csvFileFilter);
  }

  private void addLoader(final List _r, final Map _fmtLoader, final FSigFileLoaderI _loader) {
    final BuFileFilter ft = _loader.getFileFilter();
    _r.add(ft);
    _fmtLoader.put(ft, _loader);
  }

  private List getGridVerions() {
    final List r = new ArrayList();
    r.add(CorEleBthFileFormat.getInstance());
    r.add(RubarMAIFileFormat.getInstance());
    r.add(new RubarMAIFileFormat(false));
    r.add(TrigridFileFormat.getInstance());
    r.add(DunesMAIFileFormat.getInstance());
    r.add(RubarDATFileFormat.getInstance());
    r.add(new RubarDATFileFormat(false));
    return r;
  }

  /**
   * Choisit un fichier a importer.
   *
   * @param _f Le nom du fichier en cas de modification de selection. Peut �tre null.
   * @param _frame Le frame parent.
   * @return Le filtre et les fichiers selectionn�s (null si pas de fichiers selectionn�s).
   */
  FSigWizardFileModel.FileSelectResult startEditingFile(final File _f, final Frame _frame) {
    if (fileChooser_ == null) {
      // Recup�ration du dernier format de fichier sauvegard�.
      String fltImport = FudaaPreferences.FUDAA.getStringProperty("filter.import");
      BuFileFilter lastFlt = null;
      fileChooser_ = new CtuluFileChooser(true);
      for (int i = 0; i < model_.filters_.length; i++) {
        if (model_.filters_[i] == null) {
          continue;
        }
        fileChooser_.addChoosableFileFilter(model_.filters_[i]);
        if (fltImport.equals(model_.filters_[i].getShortDescription())) {
          fileChooser_.setFileFilter(model_.filters_[i]);
          lastFlt = model_.filters_[i];
        }
      }
      fileChooser_.setDialogType(JFileChooser.OPEN_DIALOG);
      fileChooser_.setFileFilter(lastFlt != null ? lastFlt : fileChooser_.getAcceptAllFileFilter());
    }
    if (_f != null) {
      fileChooser_.setSelectedFile(_f);
    }
    fileChooser_.setMultiSelectionEnabled(_f == null ? true : false);
    final FSigWizardFileModel.FileSelectResult r = new FSigWizardFileModel.FileSelectResult();
    if (fileChooser_.showOpenDialog(_frame) == JFileChooser.APPROVE_OPTION) {
      r.f_ = fileChooser_.getSelectedFiles();
      String fltImport = "";
      if (r.f_ != null && (fileChooser_.getFileFilter() instanceof BuFileFilter)) {
        r.ft_ = (BuFileFilter) fileChooser_.getFileFilter();
        fltImport = ((BuFileFilter) fileChooser_.getFileFilter()).getShortDescription();
      }
      // Sauvegarde du format de fichier pour une prochaine ouverture.
      FudaaPreferences.FUDAA.putStringProperty("filter.import", fltImport);
      FudaaPreferences.FUDAA.writeIniFile();
    }
    return r;
  }

  public void addListenerToFile(final TableModelListener _listener) {
    model_.addTableModelListener(_listener);
  }

  public final TableCellEditor getFmtRowEditor() {
    final BuComboBox cb = new BuComboBox();
    cb.setModel(new DefaultComboBoxModel(model_.filters_));
    cb.setRenderer(getFmtRowRenderer());
    return new DefaultCellEditor(cb);
  }

  public final CtuluCellTextRenderer getFmtRowRenderer() {
    return new FmtRowRenderer();
  }

  public final TableCellEditor getFmtRowOrigineEditor() {
    return GISAttributeConstants.ETAT_GEOM.getEditor().createTableEditorComponent();
  }

  public final CtuluCellTextRenderer getFmtRowOrigineRenderer() {
    return new FmtRowOrigineRenderer();
  }

  public final FSigWizardFileModel getModel() {
    return model_;
  }

  public boolean isEmpty() {
    return res_ == null || res_.isEmpty();
  }

  public boolean isLoaded() {
    return res_ != null;
  }

  public FSigFileLoadResult loadAll(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    if (res_ == null) {
      HashMap<String, String> origines = model_.getOrigines();
      res_ = new FSigFileLoadResult();
      for (final Iterator it = model_.fileData_.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        final File file = (File) e.getKey();
        final FSigFileLoaderI model = (FSigFileLoaderI) e.getValue();
        if (model == null) {
          _analyze.addFatalError(FSigLib.getS("Un format n'est pas sp�cifi�"));
          return null;
        }
        String fileOrigine = origines.get(file.getName());
        try {
          if (model instanceof FSigFileLoaderCsv) {
            ((FSigFileLoaderCsv) model).setFirstColonneAttribute(getModel().getCsvFirstColonne());
          }
          model.setInResult(res_, file, fileOrigine, _prog, _analyze);
        } catch (final Exception _e) {
          _analyze.addError(_e.getMessage(), -1);
          FuLog.warning(_e);
        }
      }
    }
    return res_;
  }
}
