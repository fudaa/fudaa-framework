/*
 * @creation 5 oct. 06
 * @modification $Date: 2007-05-04 14:00:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Component;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorInteger;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.calque.ZSelectionTableUpdater;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZModelePointEditableInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author fred deniger
 * @version $Id$
 */
public class FSIgImageWizartStepCalageUI {

  BuButton btAddMod_;
  CtuluTable table_;
  final BuTextField tfXPixel_;
  final BuTextField tfXReel_ = (BuTextField) CtuluValueEditorDefaults.DOUBLE_EDITOR.createCommonEditorComponent();

  final BuTextField tfYPixel_;
  final BuTextField tfYReel_ = (BuTextField) CtuluValueEditorDefaults.DOUBLE_EDITOR.createCommonEditorComponent();
  final CtuluUI ui_;
  final CtuluValueEditorInteger editor_;

  AbstractButton btAddPt_;
  AbstractButton btMovePt_;

  public FSIgImageWizartStepCalageUI(final CtuluUI _ui) {
    super();
    ui_ = _ui;
    editor_ = new CtuluValueEditorInteger(false);
    editor_.setVal(new CtuluValueValidator.IntMin(0));
    editor_.setEditable(true);
    tfXPixel_ = (BuTextField) editor_.createEditorComponent();
    tfYPixel_ = (BuTextField) editor_.createEditorComponent();
  }

  public void error(final String _titre, final String _msg, final boolean _tempo) {
    ui_.error(_titre, _msg, _tempo);
  }

  void addListenerToTfPixel(final DocumentListener _l) {
    tfXPixel_.getDocument().addDocumentListener(_l);
    tfYPixel_.getDocument().addDocumentListener(_l);
  }

  public Component getParentComponent() {
    return ui_.getParentComponent();
  }

  public void message(final String _titre, final String _msg, final boolean _tempo) {
    ui_.message(_titre, _msg, _tempo);
  }

  public boolean question(final String _titre, final String _text) {
    return ui_.question(_titre, _text);
  }

  public void warn(final String _titre, final String _msg, final boolean _tempo) {
    ui_.warn(_titre, _msg, _tempo);
  }

  public Number getTfXReel() {
    return (Number) tfXReel_.getValue();
  }

  public Number getTfYReel() {
    return (Number) tfYReel_.getValue();
  }

  public Number getTfXPixel() {
    return (Number) tfXPixel_.getValue();
  }

  public Number getTfYPixel() {
    return (Number) tfYPixel_.getValue();
  }

  public void setTfXReel(final Number _n) {
    tfXReel_.setValue(CtuluLib.DEFAULT_NUMBER_FORMAT.format(_n));
  }

  public void setTfYReel(final Number _n) {
    tfYReel_.setValue(CtuluLib.DEFAULT_NUMBER_FORMAT.format(_n));
  }

  public void setTfXPixel(final Number _n) {
    tfXPixel_.setValue(_n);
  }

  protected void clearTextField() {
    tfXPixel_.setText(CtuluLibString.EMPTY_STRING);
    tfYPixel_.setText(CtuluLibString.EMPTY_STRING);
    tfXReel_.setText(CtuluLibString.EMPTY_STRING);
    tfYReel_.setText(CtuluLibString.EMPTY_STRING);
  }

  public void setTfYPixel(final Number _n) {
    tfYPixel_.setValue(_n);
  }

  int getModifiedRow() {
    return table_.getSelectedRowCount() == 1 ? table_.getSelectedRow() : -1;
  }

  protected CtuluTable getTable() {
    return table_;
  }

  public boolean isDataValid() {
    return tfXReel_.getValue() != null && tfYReel_.getValue() != null && tfXPixel_.getValue() != null
        && tfYPixel_.getValue() != null;
  }

  protected CtuluUI getUI() {
    return ui_;
  }

  public void setNewPointActivated() {
    btMovePt_.setSelected(false);
    getTable().clearSelection();
  }

  protected void updateMoveBt() {
    btMovePt_.setEnabled(table_.getRowCount() > 0);
  }

  protected int[] majTextFields(final ZModelePointEditableInterface _pt) {
    final int[] idx = getTable().getSelectedRows();

    if (idx != null && idx.length == 1) {
      final int i = idx[0];
      setTfXPixel(new Integer((int) _pt.getX(i)));
      setTfYPixel(new Integer((int) _pt.getY(i)));
      setTfXReel((Number) (_pt.getGeomData().getModel(0).getObjectValueAt(i)));
      setTfYReel((Number) (_pt.getGeomData().getModel(1).getObjectValueAt(i)));
    } else {
      clearTextField();
    }
    return idx;
  }

  class TempModel implements ZModelePoint {

    @Override
    public double getX(final int _i) {
      return getTfXPixel().doubleValue();
    }

    @Override
    public double getY(final int _i) {
      return getTfYPixel().doubleValue();
    }

    @Override
    public boolean point(final GrPoint _p, final int _i, final boolean _force) {
      _p.x_ = getX(_i);
      _p.y_ = getY(_i);
      return true;
    }

    @Override
    public GrPoint getVertexForObject(int _ind, int vertex) {
      GrPoint p = new GrPoint();
      point(p, _ind, true);
      return p;
    }

    @Override
    public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
      return null;
    }

    @Override
    public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {}

    @Override
    public GrBoite getDomaine() {
      return null;
    }

    @Override
    public int getNombre() {
      return (!table_.getSelectionModel().isSelectionEmpty() || getTfXPixel() == null || getTfYPixel() == null) ? 0 : 1;
    }

    @Override
    public Object getObject(final int _ind) {
      return null;
    }

    @Override
    public boolean isValuesTableAvailable() {
      return false;
    }

    
  }

  public ZModelePoint getTmpModel() {
    return new TempModel();
  }

  protected JPanel createEditorPanel(final ZCalquePointEditable _cq, final TableModel _model,
      final AbstractButton _btAddPt, final AbstractButton _btMovePoint) {
    btAddPt_ = _btAddPt;
    btMovePt_ = _btMovePoint;

    final BuPanel pnEst = new BuPanel();
    pnEst.setLayout(new BuVerticalLayout(3, true, true));
    table_ = new CtuluTable(_model);
    table_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(final ListSelectionEvent _e) {
        final int[] idx = majTextFields((ZModelePointEditableInterface) _cq.getModelEditable());
        final boolean add = CtuluLibArray.isEmpty(idx);
        if (btAddPt_.isSelected() && !add) {
          btAddPt_.setSelected(false);
        }
        btAddMod_.setText(BuResource.BU.getString(add ? "Ajouter" : "Modifier"));
        btAddMod_.setEnabled(!(!add && idx != null && idx.length > 1));

      }

    });
    _model.addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(final TableModelEvent _e) {
        updateMoveBt();
      }
    });
    table_.setRowSelectionAllowed(true);
    // table_.setCellSelectionEnabled(false);
    table_.setColumnSelectionAllowed(false);
    final BuScrollPane buScrollPane = new BuScrollPane(table_);
    buScrollPane.setPreferredWidth(50);
    buScrollPane.setPreferredHeight(90);
    final BuPanel pnEditor = new BuPanel();
    pnEditor.setLayout(new BuGridLayout(2));
    pnEditor.add(new BuLabel(EbliLib.getS("X image")));
    pnEditor.add(tfXPixel_);
    pnEditor.add(new BuLabel(EbliLib.getS("Y image")));
    pnEditor.add(tfYPixel_);
    pnEditor.add(new BuLabel(EbliLib.getS("X r�el")));
    pnEditor.add(tfXReel_);
    pnEditor.add(new BuLabel(EbliLib.getS("Y r�el")));
    pnEditor.add(tfYReel_);
    pnEditor.add(tfYReel_);
    pnEditor.add(new BuLabel(CtuluLibString.EMPTY_STRING));
    btAddMod_ = new BuButton(BuResource.BU.getToolIcon("ajouter"), BuResource.BU.getString("Ajouter"));
    pnEditor.add(btAddMod_);
    new ZSelectionTableUpdater(table_, _cq);
    final BuPanel top = new BuPanel();
    top.setLayout(new BuVerticalLayout(1, true, false));
    top.add(btAddPt_);
    btAddPt_.setSelected(false);
    top.add(btMovePt_);
    pnEst.add(top);
    pnEst.add(pnEditor);
    pnEst.add(buScrollPane);
    pnEst.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BuBorders.EMPTY0505));
    updateMoveBt();
    return pnEst;
  }

  protected BuButton getBtAddMod() {
    return btAddMod_;
  }

}
