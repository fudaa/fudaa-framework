/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sig.layer;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import org.fudaa.ebli.controle.BSelecteurListTarget;

/**
 * Layer Able to display variable.
 *
 * @author Adrien Hadoux
 */
public interface FSigLayerVariableDisplayAble extends BSelecteurListTarget {

    public FSigLayerVariableDisplay getDisplayer();

    @Override
    public ListModel getListModel();

    @Override
    public ListSelectionModel getListSelectionModel();

    public void showValues(boolean showValues);

    public boolean isShowValues();
}
