/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.fudaa.sig;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuSplit2Pane;
import org.locationtech.jts.geom.CoordinateSequence;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionEvent;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluListSelectionListener;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluCellBooleanRenderer;
import org.fudaa.ctulu.gui.CtuluCellDoubleRenderer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.edition.EbliSingleObjectTableModel;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;


  /**
   * Cette classe repr�sente l'information z sous forme d'une courbe.
   * Cette information peut-�tre �dit�e via la courbe.
   *
   * @author MARTIN Emmanuel
   * @version $Id: EbliSingleObjectEditorPanel.java,v 1.10.6.2 2008-04-01
   *          07:28:15 emartin Exp $
   */
  public class FSigGraphEditorPanel extends BuPanel implements TableModelListener, CtuluListSelectionListener, ListSelectionListener {

    /**
     * Cette classe donne un acc�s restraint au model 'model_'. Les X et Y sont
     * remplac�s par un abscisse curviligne en travers.
     *
     * @author Emmanuel MARTIN
     * @version $Id$
     */
    protected class LimitedTableModel extends AbstractTableModel implements TableModelListener {

      /**
       * Le renderer des cellules du tableau.
       */
//      protected class Renderer extends DefaultTableCellRenderer {
//        @Override
//        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
//            int column) {
//
//          if(value instanceof Double)
//            return super.getTableCellRendererComponent(table, coordDefs_[0].getFormatter().getXYFormatter().format((Double) value), isSelected, hasFocus, row, column);
//          if(value instanceof Boolean)
//            return table.getDefaultRenderer(Boolean.class).getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//          else
//            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//        }
//      }

      private EbliCoordinateDefinition[] coordDefs_;

      /**
       * @param _defs les d�finitions pour la colonne des abscisses en travers
       */
      public LimitedTableModel(EbliCoordinateDefinition[] _defs){
        coordDefs_=_defs;
        modelData_.addTableModelListener(this);
      }

    @Override
      public int getColumnCount() {
        return modelData_.getColumnCount()-1;
      }

    @Override
      public int getRowCount() {
        return modelData_.getRowCount();
      }

    @Override
      public Object getValueAt(int _rowIndex, int _columnIndex) {
        switch (_columnIndex) {
        case 0:
          return _rowIndex+1;
        case 1:
          return getX(_rowIndex);
        default:
          return modelData_.getValueAt(_rowIndex, _columnIndex+1);
        }
      }

      @Override
      public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
        if (_columnIndex<2)
          return false;

        return modelData_.isCellEditable(_rowIndex, _columnIndex+1);
      }

      @Override
      public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
        if(_columnIndex >= 2)
          modelData_.setValueAt(_value, _rowIndex, _columnIndex+1);
      }

      @Override
      public String getColumnName(int _column) {
        switch (_column) {
        case 0:
          return FSigLib.getS("Index");
        case 1:
          return FSigLib.getS("Abscisse");
        default:
          return modelData_.getColumnName(_column+1);
        }
      }

      @Override
      public Class<?> getColumnClass(int _columnIndex) {
        switch (_columnIndex) {
        case 0:
          return Integer.class;
        case 1:
          return Double.class;
        default:
          return modelData_.getColumnClass(_columnIndex);
        }
      }

    @Override
      public void tableChanged(TableModelEvent _e) {
        int col=_e.getColumn();
        if (_e.getColumn()==zCol_)
          col=2;
        fireTableChanged(new TableModelEvent((TableModel)_e.getSource(), _e.getFirstRow(), _e.getLastRow(), col, _e.getType()));
      }

      public void updateEditorAndRenderer(JTable _table) {
        TableColumnModel cols=_table.getColumnModel();

        // Renderer \\
//        Renderer renderer=new Renderer();
//        for(int i=0;i<cols.getColumnCount();i++)
//          cols.getColumn(i).setCellRenderer(renderer);

        // Colonne Abscisse
        TableCellEditor editorAbs=CtuluValueEditorDefaults.DOUBLE_EDITOR.createTableEditorComponent();
        cols.getColumn(1).setCellEditor(editorAbs);
        cols.getColumn(1).setCellRenderer(new CtuluCellDoubleRenderer(coordDefs_[0].getFormatter().getXYFormatter()));
        
        // Colonnes des attributs
        if (modelData_!=null) {
          for (int i=3; i<modelData_.getColumnCount(); i++) {
            CtuluValueEditorI editor=modelData_.getAttribute(i).getEditor();
            if (editor!=null) {
              cols.getColumn(i-1).setCellEditor(editor.createTableEditorComponent());
              cols.getColumn(i-1).setCellRenderer(editor.createTableRenderer());
            }
            else {
              // Le renderer pour les Double
              if (modelData_.getAttribute(i).getDataClass().equals(Double.class)) {
                cols.getColumn(i - 1)
                    .setCellRenderer(new CtuluCellDoubleRenderer(coordDefs_[2].getFormatter().getXYFormatter()));
              }
              // Pour un boolean
              else if (modelData_.getAttribute(i).getDataClass().equals(Boolean.class)) {
                cols.getColumn(i - 1).setCellRenderer(new CtuluCellBooleanRenderer());
              }
            }
          }
        }
      }
    }

    // Taille de la polyligne, utile pour l'axe des abscisses
    private double lengthPolyligne_;
    // Association entre l'index du point et sa coordnn�e x
    private HashMap<Integer, Double> courbeModelIndCoordX_;
    // Valeur actuelle maximal de z
    private double maxZ_;
    // Le conteneur de la courbe
    private EGGraphe grapheVue_;
    // Axes du la courbe
    private EGAxeHorizontal axeX_;
    private EGAxeVertical axeY_;
    // Vue de la courbe
    private EGCourbeSimple courbeVue_;
    // Gestion de la selection
    private CtuluListSelection listSelection_;
    // Model du tableau
    private CtuluTable tableauVue_;
    // Selection update
    private boolean disable;
    /** Indice de la colonne z sur le modele data global. */
    protected int zCol_;
    /** Le modele de donn�es � utiliser comme model de tabelau */
    protected EbliSingleObjectTableModel modelData_;

    public FSigGraphEditorPanel(EbliSingleObjectTableModel _modelData) {
      disable = false;
      zCol_=/*zone_.getIndiceOf(zone_.getAttributeIsZ())+*/3; // l'index + les colonnes index, x et y
      modelData_=_modelData;
      modelData_.addTableModelListener(this);
      // Construction de la courbe \\
      setPreferredSize(new Dimension(650,450));
      EGGrapheSimpleModel grapheModel=new EGGrapheSimpleModel();
      grapheVue_=new EGGraphe(grapheModel);
      // Axe des X
      axeX_=new EGAxeHorizontal(false);
      axeX_.setTitre(FSigLib.getS("Abscisse curviligne"));
      axeX_.setUnite("m");
      axeX_.setGraduations(true);
      grapheVue_.setXAxe(axeX_);
      // Axe des Y
      axeY_=new EGAxeVertical();
      axeY_.setGraduations(true);
      axeY_.setTitre(FSigLib.getS("Ordonn�e") + " : " + modelData_.getColumnName(zCol_));
      axeY_.setUnite("m");
      DecimalFormat df=CtuluLib.getDecimalFormat();
      df.setMaximumFractionDigits(2);
      axeY_.setSpecificFormat(new CtuluNumberFormatDefault(df));
      // Calcule des x
      generateCoordX();
      axeX_.setBounds(0, lengthPolyligne_);
      axeY_.setBounds(0, maxZ_);
      // La courbe
      FSigCurveModelProfile courbeModel=new FSigCurveModelProfile(this, "");
      courbeVue_=new EGCourbeSimple(axeY_, courbeModel);
      courbeVue_.setAspectContour(Color.BLUE);
      courbeVue_.setIconeModel(new TraceIconModel(TraceIcon.CARRE_PLEIN,2,Color.BLUE));
      grapheModel.addCourbe(courbeVue_, new CtuluCommandManager());
      // Conteneur de graphe
      EGFillePanel pnD=new EGFillePanel(grapheVue_);
      listSelection_ = pnD.getSelection();
      listSelection_.addListeSelectionListener(this);
      // Construction des boutons \\
      EbliActionInterface[] actions=pnD.getSpecificActions();
      Container buttons=new Container();
      buttons.setLayout(new GridLayout(1, 6));
      EbliLib.updateMapKeyStroke(this, actions);
      String[] tmpAction = {"SELECT", "RESTORE", "AUTO_REST", "ZOOM", "SUIVI", "MOVE_POINT"};
      HashSet<String> actionsKept = new HashSet<String>();
      for(int i=0;i<tmpAction.length;i++)
        actionsKept.add(tmpAction[i]);
      for (int i=0; i<actions.length; i++) {
        if(actions[i] != null && actionsKept.contains(actions[i].getValue(Action.ACTION_COMMAND_KEY))){
          AbstractButton button = actions[i].buildButton(EbliComponentFactory.INSTANCE);
          button.setText("");
          buttons.add(button);
        }
      }
      // Construction du tableau \\
      LimitedTableModel tableau = new LimitedTableModel(modelData_.getCoordinateDefs());
      tableauVue_ = new CtuluTable();
      tableauVue_.setModel(tableau);
      tableauVue_.getSelectionModel().addListSelectionListener(this);
      tableau.updateEditorAndRenderer(tableauVue_);

      // Ajout au panel \\
      BuScrollPane scrollPane = new BuScrollPane(tableauVue_);
      BuPanel left = new BuPanel();
      left.setLayout(new BuBorderLayout());
      scrollPane.setPreferredSize(new Dimension(130, 335));
      left.add(buttons, BuBorderLayout.NORTH);
      left.add(scrollPane, BuBorderLayout.CENTER);
      BuSplit2Pane splitPane = new BuSplit2Pane(left, pnD, BuSplit2Pane.HORIZONTAL);
      setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
      setLayout(new BorderLayout(0,0));
      add(splitPane,BorderLayout.CENTER);
    }

    public int getNbValues() {
      return modelData_.getRowCount();
    }

    public double getX(int _id) {
      return courbeModelIndCoordX_.get(_id);
    }

    public double getY(int _id) {
      return (Double)modelData_.getValueAt(_id, zCol_);
    }

    public void setY(int _id, double _value, CtuluCommandContainer _cmd) {
      modelData_.setValueAt(new Double(_value), _id, zCol_);
    }

    /**
     * @return la s�quence des coordonn�es modifi�es. Toujours null puisque ni x
     *         ni y ne sont modifi�s (seulement z).
     */
    protected CoordinateSequence getNewCoordinate() {
      return null;
    }

    /**
     * Build or rebuild 'courbeModelIndCoordX_' with data in model_.
     */
    protected void generateCoordX() {
      double oldLengthPolyligne = lengthPolyligne_;
      double oldMaxZ = maxZ_;
      lengthPolyligne_=0;
      courbeModelIndCoordX_=new HashMap<Integer, Double>(modelData_.getRowCount());
      if (modelData_.getRowCount()>0) {
        double lastX=(Double)modelData_.getValueAt(0, 1);
        double lastY=(Double)modelData_.getValueAt(0, 2);
        double currentX, currentY;
        // Remplissage et construction des attributs
        courbeModelIndCoordX_.put(0, lengthPolyligne_);
        maxZ_=getY(0); // Le Y de getY (y de la courbe) est le Z du model
        for (int i=1; i<modelData_.getRowCount(); i++) {
          currentX=(Double)modelData_.getValueAt(i, 1);
          currentY=(Double)modelData_.getValueAt(i, 2);
          lengthPolyligne_+=Math.sqrt(Math.pow(lastX-currentX, 2)+Math.pow(lastY-currentY, 2));
          courbeModelIndCoordX_.put(i, lengthPolyligne_);
          maxZ_=Math.max(maxZ_, (Double)modelData_.getValueAt(i, 3));
          lastX=currentX;
          lastY=currentY;
        }
        if (oldLengthPolyligne!=lengthPolyligne_)
          axeX_.setBounds(0, lengthPolyligne_);
        if (grapheVue_.isAutoRestore()) {
          if (oldMaxZ!=maxZ_)
            axeY_.setBounds(0, maxZ_);
        }
      }
    }

  @Override
    public void tableChanged(TableModelEvent e) {
      generateCoordX();
      if(grapheVue_.isAutoRestore())
        grapheVue_.restore();
      else
        grapheVue_.fullRepaint();
    }

  @Override
    public void listeSelectionChanged(CtuluListSelectionEvent _e) {
      // Un changement de selection a �t� effectu� dans la courbe \\
      if (!disable) {
        // Suppression de l'�coute des changement de selection de le tableau
        disable=true;
        // Changement de selection dans le tableau
        int[] selectedIndex=((CtuluListSelectionInterface)_e.getSource()).getSelectedIndex();
        ListSelectionModel lstTableauSelect=tableauVue_.getSelectionModel();
        lstTableauSelect.clearSelection();
        if (selectedIndex!=null && selectedIndex.length>0) {
          for (int i=0; i<selectedIndex.length; i++)
            lstTableauSelect.addSelectionInterval(selectedIndex[i], selectedIndex[i]);
          tableauVue_.scrollRectToVisible(tableauVue_.getCellRect(selectedIndex[selectedIndex.length-1], 0, false));
        }
        // R�activation de l'�coute de changement de selection de le tableau
        disable=false;
      }
    }

  @Override
    public void valueChanged(ListSelectionEvent _e) {
      // Un changement de selection a �t� effectu� dans le tableau \\
      if (!disable) {
        // Suppression de l'�coute des changement de selection de la courbe
        disable=true;
        // Changement de selection de la courbe
        CtuluListSelection lst=new CtuluListSelection();
        ListSelectionModel lstSelect=tableauVue_.getSelectionModel();
        listSelection_.clear();
        for (int i=0; i<tableauVue_.getRowCount(); i++)
          if (lstSelect.isSelectedIndex(i))
            lst.add(i);
        if (!lst.isEmpty())
          listSelection_.setSelection(lst);
        // R�activation de l'�coute de changement de selection de la courbe
        disable=false;
      }
    }
  }
