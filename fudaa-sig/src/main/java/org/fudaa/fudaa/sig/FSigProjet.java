/*
 * @creation 7 juin 07
 * @modification $Date: 2007-06-11 12:53:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuInformationsDocument;
import org.fudaa.ctulu.CtuluSavable;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.fudaa.commun.FudaaProjectInterface;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;

/**
 * @author fred deniger
 * @version $Id: FSigProjet.java,v 1.1 2007-06-11 12:53:36 deniger Exp $
 */
public interface FSigProjet extends FudaaProjectInterface {

  /**
   * @return les composants qui doivent etre sauvegardés
   */
  CtuluSavable[] getSavableComponent();

  /**
   * @return la version soft, version et langage utilise
   */
  FileFormatSoftware getSystemVersion();

  BuInformationsDocument getInformationsDocument();

  void close();

  FudaaProjetStateInterface getProjectState();



}
