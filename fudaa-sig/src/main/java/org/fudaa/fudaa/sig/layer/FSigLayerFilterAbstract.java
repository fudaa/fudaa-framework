/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package org.fudaa.fudaa.sig.layer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleGeometry;

/**
 * Un filtre g�n�rique sur des calques � exporter, rempli par un visitor a implementer.
 *
 * @author marchand@deltacad.fr
 */
public abstract class FSigLayerFilterAbstract implements BCalqueVisitor {

  protected List<ZCalqueAffichageDonneesInterface> mlptsCq_ = new ArrayList<ZCalqueAffichageDonneesInterface>();
  protected List<ZCalqueAffichageDonneesInterface> pointCq_ = new ArrayList<ZCalqueAffichageDonneesInterface>();
  protected List<ZCalqueAffichageDonneesInterface> polyCq_ = new ArrayList<ZCalqueAffichageDonneesInterface>();
  private boolean exportWithId = false;

  public FSigLayerFilterAbstract() {
    super();
  }

  public boolean isExportWithId() {
    return exportWithId;
  }

  public void setExportWithId(boolean exportWithId) {
    this.exportWithId = exportWithId;
  }

  public CtuluIOOperationSynthese exportTo(final FSigLayerExporterI _exporter, final CtuluUI _impl, final File _file, GISPoint translateXY,
          final ProgressionInterface _prog) {
    return _exporter.exportTo(this, _impl, _file, translateXY, _prog);
  }

  public CtuluIOOperationSynthese exportTo(final FSigLayerExporterI _exporter, final CtuluUI _impl, final File _file, final ProgressionInterface _prog) {
    return _exporter.exportTo(this, _impl, _file, null, _prog);
  }

  /**
   * Retourne l'attribut pris pour Z dans la zone.
   *
   * @param _o Le calque
   * @return L'attribut pris pour Z. Peut �tre null.
   */
  final GISAttributeDouble getAttributeIsZ(final ZCalqueAffichageDonneesInterface _o) {
    return ((ZModeleGeometry) _o.modeleDonnees()).getGeomData().getAttributeIsZ();
  }

  String getName(final ZCalqueAffichageDonneesInterface _o) {
    return _o.getTitle();
  }

  public int getNbZone() {
    return pointCq_.size() + polyCq_.size() + mlptsCq_.size();
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return pointCq_.size() == 0 && polyCq_.size() == 0 && mlptsCq_.size() == 0;
  }

  /**
   * Retourne la collection pour le calque donn�. Peut eventuellement �tre filtr�e.
   *
   * @param _cq Le calque
   * @return La collection
   */
  protected abstract GISDataModel getCollect(final ZCalqueAffichageDonneesInterface _cq);
}
