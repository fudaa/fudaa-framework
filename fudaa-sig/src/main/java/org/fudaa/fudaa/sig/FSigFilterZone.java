/*
 * @creation 25 mai 2005
 *
 * @modification $Date: 2006-09-19 15:10:20 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

/**
 * @author Fred Deniger
 * @version $Id: FSigFilterZone.java,v 1.3 2006-09-19 15:10:20 deniger Exp $
 */
public class FSigFilterZone implements EfFilter {
  final EfGridInterface efGridInterface;
  final LinearRing linearRing;
  final Envelope envelop_;
  IndexedPointInAreaLocator areaLocator;
  private final Coordinate coordinate = new Coordinate();

  /**
   * @param _grid
   * @param _poly
   */
  public FSigFilterZone(final EfGridInterface _grid, final LinearRing _poly) {
    super();
    efGridInterface = _grid;
    envelop_ = _poly.getEnvelopeInternal();
    linearRing = _poly;
  }

  @Override
  public boolean isActivated(final int _idxPt) {
    coordinate.x = efGridInterface.getPtX(_idxPt);
    coordinate.y = efGridInterface.getPtY(_idxPt);
    if (envelop_.contains(coordinate.x, coordinate.y)) {
      if (areaLocator == null) {
        areaLocator = new IndexedPointInAreaLocator(linearRing);
      }
      return GISLib.isInside(areaLocator, coordinate);
    }
    return false;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt) {
    final EfElement el = efGridInterface.getElement(_idxElt);
    for (int i = el.getPtNb() - 1; i >= 0; i--) {
      final boolean isContained = isActivated(el.getPtIndex(i));
      if (isContained) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt, boolean strict) {
    if (!strict) {
      return isActivatedElt(_idxElt);
    }
    final EfElement el = efGridInterface.getElement(_idxElt);
    for (int i = el.getPtNb() - 1; i >= 0; i--) {
      final boolean isContained = isActivated(el.getPtIndex(i));
      if (!isContained) {
        return false;
      }
    }
    return true;
  }
}
