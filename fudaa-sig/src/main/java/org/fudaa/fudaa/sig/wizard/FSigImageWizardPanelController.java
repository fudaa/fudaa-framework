/*
 * @creation 26 sept. 06
 * @modification $Date: 2008-02-22 16:28:57 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.sig.wizard;

import com.memoire.bu.BuPopupMenu;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.action.CalqueActionInteraction;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.controle.BConfigurePaletteAction;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 * Un controleur des actions du panneau de calques 2D pour le Wizard d'import des images.
 * @author fred deniger
 * @version $Id: FSigImageWizardPanelController.java,v 1.2.6.1 2008-02-22 16:28:57 bmarchan Exp $
 */
public class FSigImageWizardPanelController extends ZEbliCalquePanelController {

  /**
   * @author fred deniger
   * @version $Id: FSigImageWizardPanelController.java,v 1.2.6.1 2008-02-22 16:28:57 bmarchan Exp $
   */
  static final class WizardCalquesPanel extends ZEbliCalquesPanel {
    /**
     * 
     */
    private final EbliActionSimple delete_;

    /**
     * @param _init
     * @param _controller
     * @param _delete
     */
    WizardCalquesPanel(BCalque _init, ZEbliCalquePanelController _controller, EbliActionSimple _delete) {
      super(_init, _controller);
      delete_ = _delete;
    }

    @Override
    protected void fillCmdContextuelles(final BuPopupMenu _menu) {
      delete_.updateStateBeforeShow();
      // _menu.add(acClick);
      _menu.add(delete_);
      _menu.addSeparator();
      getController().fillMenuWithToolsActions(_menu);
    }
  }

  public static class SpecAction extends CalqueActionInteraction {

    public SpecAction(final String _n, final Icon _ic, final String _actionCommand, final BCalqueInteraction _bc) {
      super(_n, _ic, _actionCommand, _bc);
    }

    @Override
    public AbstractButton buildToolButton(final EbliComponentFactory _f) {
      final AbstractButton r = super.buildToolButton(_f);
      r.setText(getTitle());
      return r;

    }

  }

  public FSigImageWizardPanelController(final boolean _useInfo, final CtuluUI _ui) {
    super(_useInfo, _ui);
  }

  public FSigImageWizardPanelController(final CtuluUI _ui) {
    super(_ui);
  }

  private FSigImageWizardStepCalageLogic control_;

  private final ZCalqueImageRaster image_ = new ZCalqueImageRaster();
  ZCalquePointEditable calage_;

  public ZEbliCalquesPanel buildCalquePanel( /* final EbliActionInterface[] _acs */) {
    final CtuluCommandManager mng = new CtuluCommandManager();
    control_ = new FSigImageWizardStepCalageLogic(mng, ui_, 0);
    calage_ = control_.getPtCalque();
    calage_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS_DOUBLE, 5, Color.RED));
    final EbliActionSimple actionDelete;
    actionDelete = new EbliActionSimple(CtuluLib.getS("Supprimer"), null, "DeleteSelectedAction") {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        calage_.removeSelectedObjects(mng, getUI());
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(!calage_.isSelectionEmpty());
      }
    };

    actionDelete.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
    final ZEbliCalquesPanel res = new WizardCalquesPanel(null, this, actionDelete);
    res.addCalque(calage_);

    pn_.addCalque(image_);
    res.getArbreCalqueModel().setSelectionCalque(calage_);
    return res;
  }

  @Override
  protected void buildButtonGroupStandard() {
    standardActionGroup_ = new EbliActionInterface[] { new BConfigurePaletteAction(getView().getArbreCalqueModel()
        .getTreeSelectionModel()) };

  }

  @Override
  protected EbliActionInterface createRepereAction() {
    return null;
  }

  public FSigImageWizardStepCalageLogic getControl() {
    return control_;
  }

  public ZCalqueImageRaster getImage() {
    return image_;
  }

  protected ZCalquePointEditable getCalageCalque() {
    return calage_;
  }

}