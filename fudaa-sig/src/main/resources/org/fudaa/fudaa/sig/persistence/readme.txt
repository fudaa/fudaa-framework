This file contains all data for your application

*project.db*
The file project.db is not editable and contains formatting informations and intermediate results
it cant be deleted

*data*
The folder data contains the data for the layers of the view 2D.
The files are organized in order to respect the layers structure.

You can edit the the jpg files ,the gml files, and the "desc.xml" files.
Please, use the encoding UTF-8 for the text files (extensions: xml and gml)
The files with the extension ".props" are editable but edition is not recommended. 
If a "props file" is deleted, the corresponding data will be ignored.

You can find more information for the gml files on the website

http://www.opengeospatial.org/

The contents of the xml file are easy to understand.