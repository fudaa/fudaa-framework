Ce fichier contient toutes les donn�es n�cessaires � votre �tude.

*project.db*
Ce fichier n'est pas �ditable et contient toutes les donn�es de mise en forme de votre projet ainsi que
des r�sultats interm�diaires.
Ce fichier peut �tre effac�.

*data*
Ce dossier contient toutes les donn�es de la vue 2D.
Les fichiers sont organis�s et nomm�s afin de respect�s la structure des calques.

Tous les fichiers sont �ditables.
Pour les fichiers textes ( .gml et .xml), vous devez utiliser l'encodage UTF-8 (ou �viter les accents si 
vous ne disposez pas d'un �diteur r�cent).
Les fichiers avec l'extension ".props" sont �galement �ditables mais il est fortement 
d�conseill� de les modifier: ils contiennent des informations uniquement utilisable par le 
logiciel.

Pour les fichiers gml et leur structure, vous trouverez plus d'informations sur le site

http://www.opengeospatial.org/

Le contenu des fichiers xml est normalement simple � comprendre...