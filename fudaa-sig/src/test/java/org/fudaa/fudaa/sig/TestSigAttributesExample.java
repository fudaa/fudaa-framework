/*
 *  @creation     18 janv. 2006
 *  @modification $Date: 2006-10-19 14:15:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sig;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuPanel;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JFrame;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;


/**
 * @author Fred Deniger
 * @version $Id: TestSigAttributes.java,v 1.6 2006-10-19 14:15:26 deniger Exp $
 */
public final class TestSigAttributesExample {

  private TestSigAttributesExample() {}

  public static void main(final String[] _args) {
    final FSigAttributeTableModel model = new FSigAttributeTableModel(new GISAttributeInterface[] {
        GISAttributeConstants.BATHY, GISAttributeConstants.TITRE, new GISAttributeDouble("toto", true) }, true);
    final CtuluListEditorPanel ed = new CtuluListEditorPanel(model);
    final ComboBoxModel cbModel = model.getProposedValueModel();
    final BuComboBox cbox = new BuComboBox();
    cbox.setRenderer(new CtuluCellTextRenderer() {

      @Override
      protected void setValue(final Object _value) {
        if (_value instanceof GISAttributeInterface) {
          super.setValue(((GISAttributeInterface) _value).getName());
        } else {
          super.setValue(_value);
        }
      }

    });
    cbox.setModel(cbModel);
    cbox.setEditable(true);
    final DefaultCellEditor cellEd = new DefaultCellEditor(cbox);
    ed.getTable().getColumnModel().getColumn(1).setCellEditor(cellEd);
    final JFrame f = new JFrame();
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuBorderLayout());
    pn.add(ed, BuBorderLayout.CENTER);
    final BuButton bt = new BuButton("printData");
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final java.awt.event.ActionEvent _evt) {
        System.out.println(model.getChanged());
      }
    });
    pn.add(bt, BuBorderLayout.SOUTH);
    f.setContentPane(pn);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.pack();
    f.setVisible(true);

  }

}
