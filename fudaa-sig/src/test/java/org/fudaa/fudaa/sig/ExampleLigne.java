package org.fudaa.fudaa.sig;

import org.locationtech.jts.geom.Coordinate;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.sig.layer.FSigLayerGroup;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;

public class ExampleLigne
{

  private static final class FSigVisuPanelExtension extends FSigVisuPanel
  {
    FSigLayerGroup gr;

    public FSigVisuPanelExtension(BGroupeCalque gc, CtuluUI ui)
    {
      super(gc, ui);
      gr = new FSigLayerGroup(this);
      gr.setName("groupGIS");
      getDonneesCalque().add(gr);
    }

    @Override
    public FSigLayerGroup getGroupGIS()
    {
      return gr;
    }

    @Override
    public void setActive(boolean _active) {}
  }

  public static void main(String[] args)
  {
    JPanel main = new JPanel();
    CtuluUIDialog ui = new CtuluUIDialog(main);
    BGroupeCalque gc = new BGroupeCalque();
    gc.setTitle("Test");
    gc.setName("TeSt");
    FSigVisuPanel pn = new FSigVisuPanelExtension(gc, ui);
//    GISZoneCollectionPoint testModel = new GISZoneCollectionPoint();
    GISZoneCollectionLigneBrisee testModel = new GISZoneCollectionLigneBrisee();
    GISPolyligne createLineString = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] { new Coordinate(12, 13), new Coordinate(32, 33), new Coordinate(57, 24), new Coordinate(86, 42), new Coordinate(112, 13) });
    testModel.addPolyligne((GISCoordinateSequence) createLineString.getCoordinateSequence(), null);
    ZCalqueLigneBrisee layerAct = pn.getGroupGIS().addLigneBriseeLayerAct("Test", testModel);
//    ZCalquePoint layerAct = pn.getGroupGIS().addPointLayerAct("Toto", testModel);
    main.setLayout(new BorderLayout());
    main.add(pn);
    pn.setPreferredSize(new Dimension(300, 300));
    pn.restaurer();
    JFrame fr = new JFrame();
    fr.setPreferredSize(new Dimension(500, 400));
    fr.setContentPane(main);
    fr.pack();
    BArbreCalque arbre = new BArbreCalque(pn.getArbreCalqueModel());
    main.add(new JScrollPane(arbre), BorderLayout.EAST);
    List<EbliActionInterface> actions = pn.getController().getActions();
    JToolBar tb = new JToolBar();
    for (Iterator iterator = actions.iterator(); iterator.hasNext();)
    {
      EbliActionInterface ebliActionInterface = (EbliActionInterface) iterator.next();
      if (ebliActionInterface != null)
        tb.add(ebliActionInterface.buildToolButton(EbliComponentFactory.INSTANCE));
    }
    BPaletteEdition palette = new BPaletteEdition(pn.getScene());
/*    
    CutPolylineAction act1 = new CutPolylineAction(arbre.getArbreModel(), null);
    JButton button1 = new JButton(act1.getIcon());
    button1.addActionListener(act1);
    palette.add(button1);

    JoinPolylineAction act2 = new JoinPolylineAction(arbre.getArbreModel(), null);
    JButton button2 = new JButton(act2.getIcon());
    button2.addActionListener(act2);
    palette.add(button2);

    ClearPointsAction act3 = new ClearPointsAction(arbre.getArbreModel(), null);
    JButton button3 = new JButton(act3.getIcon());
    button3.addActionListener(act3);
    palette.add(button3);

    DetectIntersectionAction act4 = new DetectIntersectionAction(arbre.getArbreModel());
    JButton button4 = new JButton(act4.getIcon());
    button4.addActionListener(act4);
    palette.add(button4);
*/
    palette.setPalettePanelTarget(pn.getEditor());
    pn.getEditor().setActivated(layerAct, palette);
    pn.restaurer();

    main.add(tb, BorderLayout.NORTH);
    pn.getScene().setCalqueActif(layerAct);
    main.add(palette.getComponent(), BorderLayout.WEST);
    fr.setVisible(true);

    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
}
