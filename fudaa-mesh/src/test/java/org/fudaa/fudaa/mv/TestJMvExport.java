/*
 *  @creation     19 d�c. 2005
 *  @modification $Date: 2007-06-11 13:08:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.mv;

import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.fudaa.meshviewer.export.MvExportT6Activity;
import org.fudaa.fudaa.meshviewer.export.MvExportT6T3Activity;

/**
 * @author Fred Deniger
 * @version $Id: TestJMvExport.java,v 1.3 2007-06-11 13:08:17 deniger Exp $
 */
public class TestJMvExport extends TestCase {

  /**
   * @return maillage T3.
   */
  public static EfGrid createMaillage() {
    final EfNode[] pts = new EfNode[9];
    pts[0] = new EfNode(0, 0, 15);
    pts[1] = new EfNode(2, -1, -15);
    pts[2] = new EfNode(1, 2, -5);
    pts[3] = new EfNode(2, 2, 30);
    pts[4] = new EfNode(0, 2, 30);
    pts[5] = new EfNode(1, 4, 30);
    pts[6] = new EfNode(10, 11, 30);
    pts[7] = new EfNode(0, 4, 30);
    pts[8] = new EfNode(2, 20, 30);
    final EfElement[] elt = new EfElement[9];
    elt[0] = new EfElement(new int[] { 0, 2, 1 });
    elt[1] = new EfElement(new int[] { 1, 2, 3 });
    elt[2] = new EfElement(new int[] { 0, 4, 2 });
    elt[3] = new EfElement(new int[] { 4, 5, 2 });
    elt[4] = new EfElement(new int[] { 5, 6, 3 });
    elt[5] = new EfElement(new int[] { 5, 4, 7 });
    elt[6] = new EfElement(new int[] { 1, 3, 6 });
    elt[7] = new EfElement(new int[] { 7, 8, 5 });
    elt[8] = new EfElement(new int[] { 6, 5, 8 });
    return new EfGrid(pts, elt);
  }

  public void testT3T6() {
    final EfGridInterface t3 = createMaillage();
    final double[] values = new double[t3.getPtsNb()];
    for (int i = values.length - 1; i >= 0; i--) {
      values[i] = i * 123;
    }
    final EfDataNode nodeData = new EfDataNode(values);
    final EfGridData data = new EfGridData() {

      @Override
      public EfGridInterface getGrid() {
        return t3;
      }

      @Override
      public boolean isDefined(CtuluVariable _var) {
        return true;
      }

      @Override
      public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
        return nodeData.getValue(_idxObjet);
      }

      @Override
      public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
        return nodeData;
      }

      @Override
      public boolean isElementVar(CtuluVariable _idxVar) {
        return false;
      }

    };
    final EfGridData t6 = new MvExportT6Activity(data).process(null, new CtuluAnalyze());
    assertNotNull(t6);
    final EfGridData newt3 = new MvExportT6T3Activity(t6).process(null, new CtuluAnalyze());
    assertTrue(newt3.getGrid().isEquivalent(t3, true, 1E-15));
    EfData d;
    try {
      d = newt3.getData(null, 0);
      assertNotNull(d);
      assertEquals(d.getSize(), values.length);
      for (int i = d.getSize() - 1; i >= 0; i--) {
        assertEquals(d.getValue(i), nodeData.getValue(i), 1E-15);
      }
    } catch (final IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }

  }

}
