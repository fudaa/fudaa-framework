/*
 * @creation 5 f?vr. 07
 * @modification $Date: 2007-06-13 12:58:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.mv;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleUnique;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataAdapter;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.EfLineFlowrateResult;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsMng;
import org.fudaa.dodico.ef.operation.EfLineIntersectorActivity;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.export.MvExportT6Activity;

/**
 * @author fred deniger
 * @version $Id: TestJDebit.java,v 1.5 2007-06-13 12:58:12 deniger Exp $
 */
public class TestJDebit extends TestCase {
  public void testDebit() {
    // on construit un maillage de quadrilatere tout bete
    /**
     * <pre>
     *                    7    8     9      13
     *                    +----+-----+      +
     *                    | 0  |     |    5 |
     *                    +----+-----+      +
     *                    0    1     2      6
     * </pre>
     */
    // 14 noeuds
    double dx = 5;
    double dy = 20;

    int nbPointParLigne = 7;
    int nbLignes = 2;
    int nbPt = nbLignes * nbPointParLigne;
    int nbEltParLigne = nbPointParLigne - 1;
    // le nombre d'?lement (nombre d'intervalle * nombre de lignes)
    int nbElt = (nbLignes - 1) * nbEltParLigne;
    EfNode[] nodes = new EfNode[nbPt];
    for (int i = 0; i < nbPt; i++) {
      int indiceSurLaligne = i % nbPointParLigne;
      int ligne = (i - indiceSurLaligne) / nbPointParLigne;
      nodes[i] = new EfNode(indiceSurLaligne * dx, ligne * dy, 0);
    }
    // Les elements
    EfElement[] elt = new EfElement[nbElt];

    for (int i = 0; i < elt.length; i++) {
      int idxSurLaLigne = i % nbEltParLigne;
      int ligne = (i - idxSurLaLigne) / nbEltParLigne;
      int[] idx = new int[4];
      idx[0] = idxSurLaLigne + (ligne * nbPointParLigne);
      idx[1] = idx[0] + 1;
      idx[2] = idx[1] + nbPointParLigne;
      idx[3] = idx[0] + nbPointParLigne;
      elt[i] = new EfElement(idx);
    }
    EfGridInterface grid = new EfGrid(nodes, elt);
    final double eps = 1E-5;
    // test a changer si on changer les nombres de noeuds e
    assertEquals(grid.getPtX(9), dx * 2, eps);
    assertEquals(grid.getPtY(9), dy, eps);
    EfElement elt1 = grid.getElement(1);
    assertEquals(1, elt1.getPtIndex(0));
    assertEquals(2, elt1.getPtIndex(1));
    assertEquals(9, elt1.getPtIndex(2));
    assertEquals(8, elt1.getPtIndex(3));
    // on construit une ligne qui passe au milieu de la premiere ligne
    double y = dy / 2;
    LineString string = GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] { new Coordinate(0, y),
        new Coordinate(dx * (nbPointParLigne - 1), y) });
    InterpolationVectorContainer vectContainer = new InterpolationVectorContainer();
    /**
     * <pre>
     * 
     *           100   ------             ------
     *                       \           /
     *                        \_________/
     *           50
     * </pre>
     */
    double[] bathyValues = new double[nbElt];
    double[] hValues = new double[nbElt];
    double zHaut = 100;
    double zBas = 50;
    double h = 50;
    for (int i = 0; i < elt.length; i++) {
      int idxSurLaLigne = i % nbEltParLigne;
      if (idxSurLaLigne < 2 || idxSurLaLigne >= nbEltParLigne - 2) {
        bathyValues[i] = zHaut;
        hValues[i] = 0;
      } else {
        bathyValues[i] = zBas;
        hValues[i] = h;
      }
    }
    final EfData bathy = new EfDataElement(bathyValues);
    final EfData hauteurEau = new EfDataElement(hValues);
    // on prend des vitesses constantes.
    // le vx ne doit avoir aucune incidence sur le d?bit
    final EfData vx = new EfDataAdapter(new CtuluCollectionDoubleUnique(-54), true);
    double vyValue = -5;

    final EfData vy = new EfDataAdapter(new CtuluCollectionDoubleUnique(vyValue), true);
    EfGridDataDefault datas = getDataAdapter(grid, bathy, hauteurEau, vx, vy);
    // vectContainer.addVxVy(H2dVariableType.VITESSE_U, H2dVariableType.VITESSE_V);
    EfGridDataInterpolator support = new EfGridDataInterpolator(datas, vectContainer);
    EfLineIntersectorActivity act = new EfLineIntersectorActivity(support);
    EfLineIntersectionsResultsMng resMng = act.computeForMeshes(string, null);
    EfLineIntersectionsResultsI res = resMng.getDefaultRes();
    assertNotNull(res);
    // +2 pour ajouter le point de d?but et le point de fin
    assertEquals(nbEltParLigne + 2, res.getNbIntersect());
    for (int i = 0; i < res.getNbIntersect(); i++) {
      // pour le y, il n'y a pas de surprise
      assertEquals(res.getIntersect(i).getY(), y, eps);
      if (i == 0) assertEquals(res.getIntersect(i).getX(), 0, eps);
      else if (i < res.getNbIntersect() - 1) {
        assertEquals(res.getIntersect(i).getX(), dx / 2 + dx * (i - 1), eps);
      } else {
        // -2 car le dernier est un demi segment
        assertEquals(res.getIntersect(i).getX(), dx / 2 + dx * (i - 2) + dx / 2, eps);
      }

    }

    EfLineFlowrateResult resDebit = new EfLineFlowrateResult(resMng.createBuilder(null), H2dVariableType.VITESSE_U,
        H2dVariableType.VITESSE_V, H2dVariableType.HAUTEUR_EAU);
    // on va calculer la surface
    int nbEltTriangle = 2;
    // le -1 correspond ? la perte de 2 demi-elements au debut et a la fin
    int nbEltPlein = nbEltParLigne - nbEltTriangle - 2 - 1;

    double surface = nbEltPlein * (dx * h) + nbEltTriangle * (dx * h / 2);
    assertEquals(resDebit.getQ(datas, 0, null), -surface * vyValue, eps);

    // on recommence la calcul avec une ligne sur les noeuds
    string = GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] { new Coordinate(0, 0),
        new Coordinate(dx * (nbPointParLigne - 1), 0) });
    act = new EfLineIntersectorActivity(support);
    resMng = act.computeForMeshes(string, null);
    res = resMng.getDefaultRes();
    // assertEquals(nbEltParLigne + 2, res.getNbIntersect());
    resDebit = new EfLineFlowrateResult(resMng.createBuilder(null), H2dVariableType.VITESSE_U,
        H2dVariableType.VITESSE_V, H2dVariableType.HAUTEUR_EAU);
    assertEquals(resDebit.getQ(datas, 0, null), -surface * vyValue, eps);

    // pour les noeuds
    bathyValues = new double[nbPt];
    hValues = new double[nbPt];

    for (int i = 0; i < nbPt; i++) {
      int idxSurLaLigne = i % nbPointParLigne;
      if (idxSurLaLigne < 2 || idxSurLaLigne >= nbPointParLigne - 2) {
        bathyValues[i] = zHaut;
        hValues[i] = 0;
      } else {
        bathyValues[i] = zBas;
        hValues[i] = h;
      }
    }
    datas = getDataAdapter(grid, new EfDataNode(bathyValues), new EfDataNode(hValues), vx, vy);
    resMng = act.computeForNodes(string, null);
    res = resMng.getDefaultRes();
    assertEquals(nbPointParLigne, res.getNbIntersect());
    resDebit = new EfLineFlowrateResult(resMng.createBuilder(null), H2dVariableType.VITESSE_U,
        H2dVariableType.VITESSE_V, H2dVariableType.HAUTEUR_EAU);
    nbEltPlein = nbEltParLigne - nbEltTriangle - 2;
    surface = nbEltPlein * (dx * h) + nbEltTriangle * (dx * h / 2);
    assertEquals(resDebit.getQ(datas, 0, null), -surface * vyValue, eps);
    // pour un maillage T6
    MvExportT6Activity toT6 = new MvExportT6Activity(datas);
    EfGridData t6 = toT6.process(null, new CtuluAnalyze());
    assertTrue(t6.getGrid().getEltType() == EfElementType.T6);
    support =  new EfGridDataInterpolator(t6, vectContainer);
    act = new EfLineIntersectorActivity(support);
    resMng = act.computeForNodes(string, null);
    res = resMng.getDefaultRes();
    resDebit = new EfLineFlowrateResult(resMng.createBuilder(null), H2dVariableType.VITESSE_U,
        H2dVariableType.VITESSE_V, H2dVariableType.HAUTEUR_EAU);
    // le d?bit doit etre le meme
    assertEquals(resDebit.getQ(datas, 0, null), -surface * vyValue, eps);

  }

  private EfGridDataDefault getDataAdapter(EfGridInterface _grid, final EfData _bathy, final EfData _hauteurEau,
      final EfData _vx, final EfData _vy) {
    final EfGridDataDefault datas = new EfGridDataDefault(_grid) {
      @Override
      public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
        if (_o == H2dVariableType.VITESSE_U) return _vx;
        else if (_o == H2dVariableType.VITESSE_V) return _vy;
        else if (_o == H2dVariableType.BATHYMETRIE) return _bathy;
        else if (_o == H2dVariableType.HAUTEUR_EAU) return _hauteurEau;
        return null;
      }

      @Override
      public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
        return getData(_o, _timeIdx).getValue(_idxObjet);
      }
    };
    return datas;
  }
}
