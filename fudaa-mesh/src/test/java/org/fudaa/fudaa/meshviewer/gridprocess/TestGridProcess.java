package org.fudaa.fudaa.meshviewer.gridprocess;

import javax.swing.JFrame;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.dodico.ef.io.adcirc.AdcircFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.supertab.SuperTabFileFormat;
import org.fudaa.dodico.ef.io.trigrid.TrigridFileFormat;
import org.fudaa.fudaa.commun.impl.FudaaPanelTask;

public class TestGridProcess {

  /**
   * @param args
   */
  public static void main(String[] args) {

//    // Locale.setDefault(new Locale("fr", "ch"));
//    Locale.setDefault(new Locale("en"));
//    String s = MvResource.getS("Afficher les");
//    System.err.println(s);
//    s = MvResource.getS("A");
//    System.err.println(s);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    CtuluUIDialog dialog = new CtuluUIDialog(frame);
    dialog.getMainProgression();

    GridProcessTaskModel model = new GridProcessTaskModel(new FileFormat[] { TrigridFileFormat.getInstance(),
        AdcircFileFormat.getInstance(), SuperTabFileFormat.getInstance(), SerafinFileFormat.getInstance() }, null);
    FudaaPanelTask task = new FudaaPanelTask(dialog, model);

    task.afficheDialog();
  }
}
