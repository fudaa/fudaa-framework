/*
 * @creation 17 mai 2005
 * 
 * @modification $Date: 2007-06-11 13:08:16 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridDataInterpolationValuesAdapter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;

/**
 * Transforme des valeurs de type noeuds en valeur de type elements.
 * 
 * @author Fred Deniger
 * @version $Id: MvExportDecoratorNodeData.java,v 1.6 2007-06-11 13:08:16 deniger Exp $
 */
public class MvExportDecoratorNodeData implements EfGridData {

  final EfGridData src_;

  final InterpolationVectorContainer vects_;

  public MvExportDecoratorNodeData(final EfGridData _src, final InterpolationVectorContainer _vects) {
    src_ = _src;
    vects_ = _vects;
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return src_.isDefined(_var);
  }

  @Override
  public EfData getData(final CtuluVariable _var, final int _timeIdx) throws IOException {
    final EfData init = src_.getData(_var, _timeIdx);
    if (init == null || init.isElementData()) { return init; }
    return EfLib.getElementData(_var, init, src_.getGrid(), new EfGridDataInterpolationValuesAdapter(src_, _timeIdx),
        vects_);
  }

  @Override
  public double getData(final CtuluVariable _var, final int _timeIdx, final int _idxObjet) throws IOException {
    final EfData init = src_.getData(_var, _timeIdx);
    if (init == null) { return 0; }
    if (init.isElementData()) { return init.getValue(_idxObjet); }
    return getGrid().getElement(_idxObjet).getAverage(_var, _timeIdx, init, src_, vects_);
  }

  @Override
  public EfGridInterface getGrid() {
    return src_.getGrid();
  }

  /**
   * @return true
   */
  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return true;
  }

}
