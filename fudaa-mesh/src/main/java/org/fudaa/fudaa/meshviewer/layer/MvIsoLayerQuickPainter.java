/*
 GPL 2
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.Envelope;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceIsoInterface;
import org.fudaa.ebli.trace.TraceIsoLignesAvecPlages;
import org.fudaa.ebli.trace.TraceIsoPlageInterface;
import org.fudaa.ebli.trace.TraceIsoSurfacesAvecPlages;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class MvIsoLayerQuickPainter {

  private boolean traceIsoLine;
  private double[] value;
  private MvIsoModelInterface model;
  private TraceIsoPlageInterface paletteCouleur;
  private TraceLigneModel ligneModel;
  private int alpha;
  private boolean isAntaliasing;

  public MvIsoModelInterface getModel() {
    return model;
  }

  public void setModel(MvIsoModelInterface model) {
    this.model = model;
  }

  public boolean isTraceIsoLine() {
    return traceIsoLine;
  }

  public void setTraceIsoLine(boolean traceIsoLine) {
    this.traceIsoLine = traceIsoLine;
  }

  public TraceIsoPlageInterface getPaletteCouleur() {
    return paletteCouleur;
  }

  public void setPaletteCouleur(TraceIsoPlageInterface paletteCouleur) {
    this.paletteCouleur = paletteCouleur;
  }

  public TraceLigneModel getLigneModel() {
    return ligneModel;
  }

  public void setLigneModel(TraceLigneModel ligneModel) {
    this.ligneModel = ligneModel;
  }

  public int getAlpha() {
    return alpha;
  }

  public void setAlpha(int alpha) {
    this.alpha = alpha;
  }

  public boolean isIsAntaliasing() {
    return isAntaliasing;
  }

  public void setIsAntaliasing(boolean isAntaliasing) {
    this.isAntaliasing = isAntaliasing;
  }

  /**
   * Contructeur du calque.
   *
   * @param _m le modele de donnees
   */
  public MvIsoLayerQuickPainter(final MvIsoModelInterface _m) {
    this.model = _m;
  }

  private boolean isLineAllPaintedByNode(int minX, int maxX, int minY, int maxY, int[] x, int[] y, int nbPt) {
    final int deltaX = maxX - minX;
    final int deltaY = maxY - minY;
    if (deltaX <= 1 && deltaY <= 1) {
      return true;
    }
    if (deltaX == 0 && deltaY < nbPt) {
      boolean[] dots = new boolean[deltaY + 1];
      for (int i = 0; i < nbPt; i++) {
        dots[y[i] - minY] = true;
      }
      return isAllTrue(dots);

    }
    if (deltaX < nbPt && deltaY == 0) {
      boolean[] dots = new boolean[deltaX + 1];
      for (int i = 0; i < nbPt; i++) {
        dots[x[i] - minX] = true;
      }
      return isAllTrue(dots);
    }
    return false;
  }

  private boolean isAllTrue(boolean[] dots) {
    for (int i = 0; i < dots.length; i++) {
      if (!dots[i]) {
        return false;
      }
    }
    return true;
  }

  public void paintDonnees(int width, int height, final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    int idxElt;
    TraceIsoInterface isos = null;
    MvIsoModelInterface modelToUse = model;
    if (traceIsoLine) {
      isos = new TraceIsoLignesAvecPlages(paletteCouleur, alpha, ligneModel);
    } else {
      isos = new TraceIsoSurfacesAvecPlages(paletteCouleur, alpha);
    }
    GrBoite clipEcran = _clipReel.applique(_versEcran);
    isos.setRapide(rapide);
    int w = width + 1;
    CtuluListSelection memory = new CtuluListSelection(w * height);
    isos.setDimension(width, height, memory);
    isos.setClipEcran(clipEcran);
    // pour les surfaces, on voudrait eviter d'utiliser l'anticrenelage
    final RenderingHints renderingHints = _g.getRenderingHints();
    renderingHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    renderingHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
    _g.setRenderingHints(renderingHints);
//    }

    final int n = modelToUse.getNbElt();
    int[] x = null;
    int[] y = null;
    GrPoint pt = new GrPoint();

    Envelope envReel = _clipReel.getEnv();
    for (idxElt = 0; idxElt < n; idxElt++) {
      if (!modelToUse.isPainted(idxElt)) {
        continue;
      }

      final int nbPt = modelToUse.getNbPt(idxElt);
      Envelope envelopeElement = modelToUse.getEnvelopeForElement(idxElt);

      if (!envelopeElement.intersects(envReel)) {
        continue;
      }
      if (x == null || x.length < nbPt) {
        x = new int[nbPt];
      }
      if (y == null || y.length < nbPt) {
        y = new int[nbPt];
      }

      int maxX = -1;
      int minX = Integer.MAX_VALUE;
      int maxY = -1;
      int minY = Integer.MAX_VALUE;
      for (int iPt = 0; iPt < nbPt; iPt++) {
        pt.x_ = modelToUse.getX(idxElt, iPt);
        pt.y_ = modelToUse.getY(idxElt, iPt);
        pt.z_ = 0;
        pt.autoApplique(_versEcran);
        final int xi = (int) pt.x_;
        final int yi = (int) pt.y_;
        x[iPt] = xi;
        y[iPt] = yi;
        maxX = Math.max(maxX, xi);
        maxY = Math.max(maxY, yi);
        minX = Math.min(minX, xi);
        minY = Math.min(minY, yi);
      }
      final int deltaX = maxX - minX;
      final int deltaY = maxY - minY;
      boolean isOnlyOnePoint = deltaX == 0 && deltaY == 0;
      // Trace des isosurfaces / Isolignes (dans la couleur du fond si les isocouleurs sont
      // tracees).
      if (isos != null) {
        if (!traceIsoLine && isOnlyOnePoint) {
          if (clipEcran.contientXY(x[0], y[0])) {
            int idxDone = x[0] + y[0] * w;
            if (idxDone >= 0 && !memory.isSelected(idxDone)) {
              memory.add(idxDone);
              double ptValue = modelToUse.getDatatFor(idxElt, 0);
              final Color c = paletteCouleur == null ? null : ((BPalettePlage) paletteCouleur).getColorFor(ptValue);
              if (c != null) {
                _g.setColor(rapide ? c : EbliLib.getAlphaColor(c, alpha));
                _g.drawLine(x[0], y[0], x[0], y[0]);
              }
            }
          }
          //l'element est un element plat(ligne) ou est contenu dans un carre 2*2
        } else if (!traceIsoLine && isLineAllPaintedByNode(minX, maxX, minY, maxY, x, y, nbPt)) {

          boolean painted = false;
          if (deltaX == 0 || deltaY == 0) {
            double valueToUse = modelToUse.getDatatFor(idxElt, 0);
            boolean constant = true;
            //pour les points sur 2 pixels on ne fait pas le tests pour savoir si meme valeur.
            if (deltaX >= 1 || deltaY >= 1) {
              for (int iPt = 1; iPt < nbPt; iPt++) {
                double val = modelToUse.getDatatFor(idxElt, iPt);
                if (constant) {
                  constant = CtuluLib.isEquals(val, valueToUse, 1e-10);
                  if (!constant) {
                    break;
                  }
                }
              }
            }
            if (constant) {
              painted = true;
              final Color color = paletteCouleur == null ? null : ((BPalettePlage) paletteCouleur).getColorFor(
                      valueToUse);
              final Color c = rapide ? color : EbliLib.getAlphaColor(color, alpha);
              if (c != null) {
                _g.setColor(c);
                _g.drawLine(minX, minY, maxX, maxY);
              }
              for (int iPt = 0; iPt < nbPt; iPt++) {
                int idxDone = x[iPt] + y[iPt] * w;
                if (idxDone >= 0) {
                  memory.add(idxDone);
                }
              }
            }
          }
          if (!painted) {
            for (int iPt = 0; iPt < nbPt; iPt++) {
              if (clipEcran.contientXY(x[iPt], y[iPt])) {
                int idxDone = x[iPt] + y[iPt] * w;
                if (idxDone >= 0 && !memory.isSelected(idxDone)) {
                  memory.add(idxDone);
                  final Color color = paletteCouleur == null ? null : ((BPalettePlage) paletteCouleur).getColorFor(
                          modelToUse.getDatatFor(idxElt, iPt));
                  final Color c = rapide ? color : EbliLib.getAlphaColor(color, alpha);
                  if (c != null) {
                    _g.setColor(c);
                    _g.drawLine(x[iPt], y[iPt], x[iPt], y[iPt]);
                  }
                }
              }
            }
          }
        } else {
          value = modelToUse.fillWithData(idxElt, value);
          if (value != null) {
            if (rapide && !traceIsoLine && deltaX <= 5 && deltaY <= 5) {
              double moyenne = CtuluLibArray.getMoyenne(value);
              final Color c = paletteCouleur == null ? null : ((BPalettePlage) paletteCouleur).getColorFor(
                      moyenne);
              _g.setColor(c);
              _g.fillRect(minX, minY, deltaX + 1, deltaY + 1);
            } else {
              final Polygon p = new Polygon(x, y, nbPt);
              isos.draw(_g, p, value);
            }
          }
        }
      }
    }

  }
  boolean rapide;

  public void setRapide(boolean rapide) {
    this.rapide = rapide;
  }
}
