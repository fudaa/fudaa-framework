/**
 *  @creation     4 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.model;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIterator;
import java.util.Arrays;

/**
 * @author deniger
 * @version $Id: MvNumberDelegate.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
 */
public final class MvNumberDelegate {

  private boolean all_;
  private TIntHashSet selectedEdge_;
  final int nbItem_;

  /**
   * @param _item
   */
  public MvNumberDelegate(final int _item) {
    super();
    nbItem_ = _item;
  }

  public boolean addSelectedPoint(final int[] _idx) {
    if (!all_) {
      if (selectedEdge_ == null) {
        selectedEdge_ = new TIntHashSet(_idx);
        return true;
      }
      final boolean change = selectedEdge_.addAll(_idx);
      if (change && selectedEdge_.size() == nbItem_) {
        selectedEdge_.clear();
        all_ = true;
      }
      return change;
    }
    return false;
  }

  public boolean clearIdxShow() {
    if (all_) {
      all_ = false;
      return true;
    }
    if (selectedEdge_ != null && selectedEdge_.size() > 0) {
      selectedEdge_.clear();
      return true;
    }
    return false;
  }

  public boolean isAll() {
    return all_;
  }

  public boolean isSomethingToView() {
    return all_ || (selectedEdge_ != null && selectedEdge_.size() > 0);
  }

  public boolean setAll() {
    if (all_) { return false; }
    all_ = true;
    if (selectedEdge_ != null) {
      selectedEdge_.clear();
    }
    return true;
  }

  public TIntIterator getIdxIterator() {
    return selectedEdge_ == null ? null : selectedEdge_.iterator();
  }

  public int getNbIdxSelected() {
    return selectedEdge_ == null ? 0 : selectedEdge_.size();
  }

  public boolean setSelectedPoint(final int[] _idx) {
    if (_idx == null || _idx.length == 0) { return clearIdxShow(); }
    if (_idx.length == nbItem_) {
      if (all_) { return false; }
      all_ = true;
      if (selectedEdge_ != null) {
        selectedEdge_.clear();
      }
      return true;
    }
    all_ = false;
    if (selectedEdge_ == null) {
      selectedEdge_ = new TIntHashSet(_idx);
      return true;
    }
    final int[] old = selectedEdge_.toArray();
    Arrays.sort(old);
    if (!Arrays.equals(_idx, old)) {
      selectedEdge_.clear();
      selectedEdge_.ensureCapacity(_idx.length);
      selectedEdge_.addAll(_idx);
      return true;
    }
    return false;
  }
}