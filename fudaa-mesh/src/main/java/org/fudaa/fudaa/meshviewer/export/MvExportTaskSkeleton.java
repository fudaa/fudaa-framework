/*
 * @creation 8 sept. 2005
 * 
 * @modification $Date: 2007-06-05 09:01:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.decorator.EfGridDataTranslateDecorator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author Fred Deniger
 * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
 */
public class MvExportTaskSkeleton implements FudaaPanelTaskModel {

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public interface ExportPanelCustom extends ExportPanel {

    void update(MvExportActInterface _target);
  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public interface ExportPanelFileChooser extends ExportPanel {

    File[] getSelectedFiles();

  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public interface ExportPanel {

    JComponent getComponent();

    String getErrorMessage();

    /**
     * ferme les resources prises par le panel
     */
    void close();
  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public static class ExportPanelVarTimeChooser extends MvExportChooseVarAndTime implements ExportPanelVarTime {

    /**
     * @param _varModel
     * @param _timeModel
     */
    public ExportPanelVarTimeChooser(final ListModel _varModel, final FudaaCourbeTimeListModel _timeModel) {
      super(_varModel, _timeModel, null);
    }

    @Override
    public EfGridData getTimeSrc(final EfGridData _init) {
      final ListSelectionModel timeSel = getTimeSelectionModel();
      if (timeSel == null || timeSel.isSelectionEmpty()
          || CtuluLibArray.getSelectedIdxNb(timeSel) == getTimeModel().getSize()) { return _init; }
      return new MvExportSrcTimeAdapter(_init, CtuluLibArray.getSelectedIdx(timeSel));
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public String getErrorMessage() {
      return super.getErrorText();
    }

    @Override
    public void close() {}

  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public interface ExportPanelVarTime extends ExportPanel {

    double[] getSelectedTimeStep();

    ListSelectionModel getTimeSelectionModel();

    /**
     * @param _init la source initiale
     * @return la source modifiee pour respecter les pas de temps selectionnes
     */
    EfGridData getTimeSrc(EfGridData _init);

    CtuluVariable[] getSelectedVar();

  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public static class ExportPanelFileChooserSingle extends BuPanel implements ExportPanelFileChooser {

    final BuLabel lb_;

    final CtuluFileChooserPanel pnFile_;

    String[] extensionsCreated_;

    public final String[] getExtensionsCreated() {
      return extensionsCreated_;
    }

    @Override
    public void close() {}

    public final void setExtensionsCreated(final String[] _extensionsCreated) {
      extensionsCreated_ = _extensionsCreated;
    }

    public ExportPanelFileChooserSingle(final String _cbTitres, final File _file, final String _ext,
        final FileFilter[] _filters) {
      setLayout(new BuGridLayout(2, 5, 5));

      final File dir = (_file != null) ? _file.getParentFile() : new File(System.getProperty("user.home"));
      lb_ = new BuLabel(_cbTitres);
      add(lb_);
      pnFile_ = new CtuluFileChooserPanel(dir);
      pnFile_.setFilter(_filters);
      pnFile_.setExt(_ext);
      pnFile_.setFile(_file);
      add(pnFile_);
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public String getErrorMessage() {
      Color okColor = CtuluLibSwing.getDefaultLabelForegroundColor();
      if (okColor == null) {
        okColor = Color.BLACK;
      }
      final String r = CtuluLibFile.canWrite(pnFile_.getFileWithExt());
      lb_.setForeground(r == null ? okColor : Color.RED);
      return r;
    }

    @Override
    public File[] getSelectedFiles() {
      final File r = pnFile_.getFileWithExt();
      if (r == null) { return null; }
      if (CtuluLibArray.isEmpty(extensionsCreated_)) { return new File[] { pnFile_.getFileWithExt() }; }
      final List res = new ArrayList(1 + extensionsCreated_.length);
      res.add(r);
      for (int i = 0; i < extensionsCreated_.length; i++) {
        if (!extensionsCreated_[i].equals(pnFile_.getExt())) {
          res.add(CtuluLibFile.changeExtension(r, extensionsCreated_[i]));
        }
      }
      return (File[]) res.toArray(new File[res.size()]);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public static class ExportPanelFileChooserMulti extends BuPanel implements ExportPanelFileChooser {

    /**
     * @author fred deniger
     * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
     */
    static final class CbActionListener implements ActionListener {
      /**
       * 
       */
      private final CtuluFileChooserPanel file_;
      /**
       * 
       */
      private final BuCheckBox cb_;

      /**
       * @param _file
       * @param _cb
       */
      CbActionListener(final CtuluFileChooserPanel _file, final BuCheckBox _cb) {
        file_ = _file;
        cb_ = _cb;
      }

      @Override
      public void actionPerformed(final ActionEvent _e) {
        file_.setEnabled(cb_.isSelected());
      }
    }

    final JCheckBox[] cb_;

    final CtuluFileChooserPanel[] pns_;

    public ExportPanelFileChooserMulti(final String[] _cbTitres, final String[] _suffixe, final File _file,
        final String _ext, final FileFilter[] _filters) {
      final int nb = _cbTitres.length;
      setLayout(new BuGridLayout(2, 5, 5));
      pns_ = new CtuluFileChooserPanel[nb];
      cb_ = new JCheckBox[nb];
      final String ext = CtuluLibFile.getCorrectExtension(_ext);
      final String pref = CtuluLibFile.getSansExtension(_file.getName());
      final File dir = _file.getParentFile();
      for (int i = 0; i < nb; i++) {
        final BuCheckBox cb = new BuCheckBox(_cbTitres[i]);
        cb.setSelected(true);
        final CtuluFileChooserPanel pnFile = new CtuluFileChooserPanel(dir);
        pnFile.setFilter(_filters);
        pnFile.setExt(ext);
        pnFile.setFile(new File(dir, pref + (_suffixe == null ? CtuluLibString.EMPTY_STRING : _suffixe[i])));
        add(cb);
        add(pnFile);
        cb_[i] = cb;
        pns_[i] = pnFile;
        cb.addActionListener(new CbActionListener(pnFile, cb));
      }
    }

    @Override
    public void close() {}

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public String getErrorMessage() {
      Color okColor = CtuluLibSwing.getDefaultCheckboxForegroundColor();
      if (okColor == null) {
        okColor = Color.BLACK;
      }
      final Color wrong = Color.RED;
      final StringBuffer buffer = new StringBuffer();
      for (int i = cb_.length - 1; i >= 0; i--) {
        boolean ok = true;
        if (cb_[i].isSelected()) {
          final String r = CtuluLibFile.canWrite(pns_[i].getFileWithExt());
          if (r != null) {
            buffer.append(CtuluLibString.LINE_SEP).append(r);
            ok = false;
          }
        }
        cb_[i].setForeground(ok ? okColor : wrong);
      }
      return buffer.length() == 0 ? null : buffer.toString();
    }

    @Override
    public File[] getSelectedFiles() {
      final File[] res = new File[cb_.length];
      for (int i = res.length - 1; i >= 0; i--) {
        if (cb_[i].isSelected()) {
          res[i] = pns_[i].getFileWithExt();
        }
      }
      return res;
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportTaskSkeleton.java,v 1.22 2007-06-05 09:01:12 deniger Exp $
   */
  public static class ExportPaneVarTimelSingleChooser implements ExportPanelVarTime {

    FudaaCourbeTimeListModel time_;

    BuComboBox cbTime_;

    JPanel pn_;

    DefaultListSelectionModel select_;

    @Override
    public void close() {}

    /**
     * @param _timeModel
     */
    public ExportPaneVarTimelSingleChooser(final FudaaCourbeTimeListModel _timeModel) {
      time_ = _timeModel;
      cbTime_ = new BuComboBox();
      cbTime_.setModel(new CtuluComboBoxModelAdapter(time_));
      select_ = new DefaultListSelectionModel();
      select_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      cbTime_.getModel().addListDataListener(new ListDataListener() {

        @Override
        public void intervalRemoved(final ListDataEvent _e) {}

        @Override
        public void intervalAdded(final ListDataEvent _e) {}

        @Override
        public void contentsChanged(final ListDataEvent _e) {
          select_.setSelectionInterval(cbTime_.getSelectedIndex(), cbTime_.getSelectedIndex());
        }

      });
      cbTime_.setSelectedIndex(time_.getSize() - 1);
    }

    @Override
    public ListSelectionModel getTimeSelectionModel() {
      return select_;
    }

    @Override
    public String getErrorMessage() {
      return null;
    }

    @Override
    public double[] getSelectedTimeStep() {
      return new double[] { (cbTime_ == null || cbTime_.getSelectedIndex() < 0) ? 0 : time_.getTimeInSec(cbTime_
          .getSelectedIndex()) };
    }

    @Override
    public CtuluVariable[] getSelectedVar() {
      return new CtuluVariable[] { H2dVariableType.BATHYMETRIE };
    }

    @Override
    public EfGridData getTimeSrc(final EfGridData _init) {
      return new MvExportSrcTimeAdapter(_init, new int[] { (cbTime_ == null || cbTime_.getSelectedIndex() < 0) ? 0
          : cbTime_.getSelectedIndex() });
    }

    @Override
    public JComponent getComponent() {
      if (pn_ == null) {
        pn_ = new BuPanel();
        pn_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Pas de temps")));
        pn_.setLayout(new BuBorderLayout());
        pn_.add(cbTime_);
      }
      return pn_;
    }
  }

  protected final ExportPanelFileChooser getPnFile() {
    return pnFile_;
  }

  public final void setPnFile(final ExportPanelFileChooser _pnFile) {
    pnFile_ = _pnFile;
  }

  protected final MvExportPanelFilter getPnFilter() {
    return pnOperationOnGrid;
  }

  public final void setPnFilter(final MvExportPanelFilter _pnFilter) {
    pnOperationOnGrid = _pnFilter;
  }

  protected final ExportPanelVarTime getPnVar() {
    return pnVar_;
  }

  public final void setPnVar(final ExportPanelVarTime _pnVar) {
    pnVar_ = _pnVar;
  }

  public static Border getFilterBorder() {
    return BorderFactory.createTitledBorder(CtuluLib.getS("Filtres"));
  }

  public static Border getFileBorder(final boolean _pluriel) {
    return BorderFactory.createTitledBorder(CtuluLib.getS(_pluriel ? "Fichiers" : "Fichier"));
  }

  protected CtuluActivity currentActivity_;

  protected boolean stop_;

  ExportPanelCustom[] custom_;

  BuPanel pn_;

  ExportPanelFileChooser pnFile_;

  CtuluValueEditorDouble translate_;
  MvExportPanelFilter pnOperationOnGrid;
  JComponent tfTranslateX_;
  JComponent tfTranslateY_;

  ExportPanelVarTime pnVar_;

  final MvExportActInterface act_;

  final EfGridData src_;
  final InterpolationVectorContainer vect_;

  public MvExportTaskSkeleton(final EfGridData _src, final InterpolationVectorContainer _vect,
      final MvExportActInterface _act) {
    act_ = _act;
    src_ = _src;
    vect_ = _vect;

  }

  public MvExportActInterface getActInterface() {
    return act_;
  }

  protected void initActivity() {
    stop_ = false;
    currentActivity_ = null;
  }

  @Override
  public void actTask(final ProgressionInterface _prog, final CtuluAnalyzeGroup _analyze, final String[] _messages) {
    initActivity();
    stop_ = false;
    act_.setOperation(null);
    EfGridData src = src_;
    if (tfTranslateX_.isEnabled()) {
      final Object tx = translate_.getValue(tfTranslateX_);
      final Object ty = translate_.getValue(tfTranslateY_);
      if (tx != null && ty != null) {
        final double txDouble = ((Number) tx).doubleValue();
        final double tyDouble = ((Number) ty).doubleValue();
        setOldXValues(txDouble);
        setOldYValues(tyDouble);
        src = new EfGridDataTranslateDecorator(txDouble, tyDouble, src);
      }
    }
    if (pnVar_ != null) {
      act_.setSelectedTimeStep(pnVar_.getSelectedTimeStep());
      act_.setSelectedVar(pnVar_.getSelectedVar());
      src = pnVar_.getTimeSrc(src);
    }
    if (pnOperationOnGrid != null) {
      final MvExportOperationBuilderInterface builder = pnOperationOnGrid.getOperation(src);
      if (builder != null) {
        currentActivity_ = builder;
        builder.buildOperation(_prog);
        act_.setOperation(builder.getOperation(-1));
      }

    }
    act_.setSrc(src, vect_);
    if (!CtuluLibArray.isEmpty(custom_)) {
      for (int i = 0; i < custom_.length; i++) {
        custom_[i].update(act_);
      }
    }
    if (!stop_) {
      act_.actExport(_prog, this.getDestFiles(), _analyze, _messages);
    }
    _prog.reset();
    initActivity();

  }

  @Override
  public void decoreBtApply(final JButton _bt) {
    _bt.setText(FudaaLib.getS("Exporter"));
    _bt.setIcon(BuResource.BU.getIcon("Exporter"));

  }

  @Override
  public int getNbMessageMax() {
    return act_.getNbLabelInfoNeeded();
  }

  public double getOldXValues() {
    return FudaaPreferences.FUDAA.getDoubleProperty("export.translate.x", 0);
  }

  public void setOldXValues(final double _tx) {
    FudaaPreferences.FUDAA.putDoubleProperty("export.translate.x", _tx);
  }

  public void setOldYValues(final double _ty) {
    FudaaPreferences.FUDAA.putDoubleProperty("export.translate.y", _ty);
  }

  public double getOldYValues() {
    return FudaaPreferences.FUDAA.getDoubleProperty("export.translate.y", 0);
  }

  protected void constructPanelFiles(JComponent comp) {
    comp.add(pnFile_.getComponent());
  }

  protected final void constructFilterPanel(JComponent comp) {
    if (pnOperationOnGrid != null) {
      pn_.add(pnOperationOnGrid.getComponent());
    }
  }

  @Override
  public JComponent getPanel() {
    if (pn_ == null) {
      pn_ = new BuPanel(new BuVerticalLayout(2, true, false));

      constructPanelFiles(pn_);// .add(pnFile_.getComponent());
      if (pnVar_ != null) {
        pn_.add(pnVar_.getComponent());
      }
      constructFilterPanel(pn_);
      final JCheckBox jc = new BuCheckBox(MvResource.getS("Translater les donn�es"));
      final JPanel pnTranslate = new BuTitledPanel(jc, new BuGridLayout(2, 3, 3));
      CtuluLibSwing.addActionListenerForCheckBoxTitle(pnTranslate, jc);
      translate_ = new CtuluValueEditorDouble(false);
      tfTranslateX_ = translate_.createEditorComponent();
      translate_.setValue(getOldXValues(), tfTranslateX_);
      tfTranslateY_ = translate_.createEditorComponent();
      translate_.setValue(getOldYValues(), tfTranslateY_);
      jc.setSelected(false);
      pnTranslate.add(new BuLabel(FudaaLib.getS("En X:"))).setEnabled(false);
      pnTranslate.add(tfTranslateX_).setEnabled(false);
      pnTranslate.add(new BuLabel(FudaaLib.getS("En Y:"))).setEnabled(false);
      pnTranslate.add(tfTranslateY_).setEnabled(false);
      pn_.add(pnTranslate);
      if (custom_ != null) {
        for (int i = 0; i < custom_.length; i++) {
          pn_.add(custom_[i].getComponent());
        }
      }
    }
    return pn_;
  }

  @Override
  public String getTitre() {
    return FudaaLib.getS("Export:") + CtuluLibString.ESPACE + act_.getTitre();
  }

  public File[] getDestFiles() {
    return pnFile_.getSelectedFiles();
  }

  protected String isValidPanelFile() {
    return pnFile_.getErrorMessage();
  }

  @Override
  public String isTakDataValid() {
    final StringBuffer buffer = new StringBuffer();
    String mess = isValidPanelFile();
    if (mess != null) {
      buffer.append(mess);
    }
    if (pnVar_ != null) {
      mess = pnVar_.getErrorMessage();
      if (mess != null) {
        addLineSepIfNeeded(buffer);
        buffer.append(mess);
      }
    }
    if (pnOperationOnGrid != null) {
      mess = pnOperationOnGrid.getErrorMessage();
      if (mess != null) {
        addLineSepIfNeeded(buffer);
        buffer.append(mess);
      }
    }
    if (tfTranslateX_.isEnabled()) {
      Object o = translate_.getValue(tfTranslateX_);
      if (o == null) {
        addLineSepIfNeeded(buffer);
        buffer.append(MvResource.getS("La valeur de translation en {0} est erron�e", "X"));
      }
      o = translate_.getValue(tfTranslateY_);
      if (o == null) {
        addLineSepIfNeeded(buffer);
        buffer.append(MvResource.getS("La valeur de translation en {0} est erron�e", "Y"));
      }

    }

    if (custom_ != null) {
      for (int i = 0; i < custom_.length; i++) {
        mess = custom_[i].getErrorMessage();
        if (mess != null) {
          addLineSepIfNeeded(buffer);
          buffer.append(mess);
        }
      }
    }
    if (buffer.length() > 0) { return buffer.toString(); }
    final File[] fs = getDestFiles();
    final StringBuffer name = new StringBuffer();
    for (int i = 0; i < fs.length; i++) {
      if (fs[i] != null && fs[i].exists()) {
        name.append(CtuluLibString.LINE_SEP);
        name.append(fs[i].getName());
      }
    }
    if (name.length() > 0) {
      if (!CtuluLibDialog.confirmeOverwriteFiles(pn_, name.toString())) { return CtuluLib
          .getS("Modifier les fichiers cibles"); }
    }
    return null;
  }

  private void addLineSepIfNeeded(final StringBuffer _buffer) {
    if (_buffer.length() > 0) {
      _buffer.append(CtuluLibString.LINE_SEP);
    }
  }

  public final void setCustom(final ExportPanelCustom[] _custom) {
    custom_ = _custom;
  }

  @Override
  public void stopTask() {
    stop_ = true;
    if (currentActivity_ != null) {
      currentActivity_.stop();
    }
    if (act_ != null) {
      act_.stop();
    }
  }

  @Override
  public void dialogClosed() {
    if (custom_ != null) {
      for (ExportPanelCustom pns : custom_) {
        pns.close();
      }
      if (pnVar_ != null) pnVar_.close();
      if (pnFile_ != null) pnFile_.close();
      if (pnOperationOnGrid != null) pnOperationOnGrid.close();
    }

  }
}
