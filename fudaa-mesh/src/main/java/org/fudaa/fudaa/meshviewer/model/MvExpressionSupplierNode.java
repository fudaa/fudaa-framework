/*
 *  @creation     17 juin 2005
 *  @modification $Date: 2007-04-16 16:35:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.calque.find.CalqueFindExpression;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: MvExpressionSupplierNode.java,v 1.2 2007-04-16 16:35:34 deniger Exp $
 */
public class MvExpressionSupplierNode extends MvExpressionSupplierDefault {

  Variable x_;
  Variable y_;
  Variable frIdx_;
  Variable idx_;
  Variable idxGlobalOnFr_;
  Variable idxInFr_;

  /**
   * @param _nodes le calque des noeuds
   */
  public MvExpressionSupplierNode(final MvNodeModel _nodes) {
    super(_nodes);
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    final MvNodeModel nodes = (MvNodeModel) data_;
    if (nodes.getGrid() != null && nodes.getGrid().getFrontiers() != null) {
      frIdx_ = buildFrIdx(_expr);
      idxGlobalOnFr_ = buildidxGlobalOnFr(_expr);
      idxInFr_ = buildidxInFr(_expr);
    }
    idx_ = CalqueFindExpression.buildIdxNode(_expr);
    x_ = CalqueFindExpression.buildXNode(_expr);
    y_ = CalqueFindExpression.buildYNode(_expr);

  }
  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    idx_.setValue(CtuluLib.getDouble(_idx + 1));
    final MvNodeModel nodes = (MvNodeModel) data_;
    x_.setValue(CtuluLib.getDouble(nodes.getX(_idx)));
    y_.setValue(CtuluLib.getDouble(nodes.getY(_idx)));
    if (frIdx_ != null && nodes.getGrid().getFrontiers() != null) {
      final int idxGlobalOnFr = CtuluLibArray.findObjectEgalEgal(_varToUpdate, idxGlobalOnFr_);
      if (idxGlobalOnFr >= 0) {
        _varToUpdate[idxGlobalOnFr].setValue(CtuluLib
            .getDouble(nodes.getGrid().getFrontiers().getIdxOnFrontier(_idx) + 1));
      }
      final int idx = CtuluLibArray.findObjectEgalEgal(_varToUpdate, frIdx_);
      final int idxOnFr = CtuluLibArray.findObjectEgalEgal(_varToUpdate, idxInFr_);
      if (idx >= 0 || idxOnFr >= 0) {
        int[] res = nodes.getGrid().getFrontiers().getIdxFrontierIdxPtInFrontierFromGlobal(_idx);
        if (res == null) {
          res = new int[] { -1, -1 };
        }
        if (idx >= 0) {
          frIdx_.setValue(CtuluLib.getDouble(res[0] + 1));
        }
        if (idxOnFr >= 0) {
          idxInFr_.setValue(CtuluLib.getDouble(res[1] + 1));
        }

      }

    }
  }
}
