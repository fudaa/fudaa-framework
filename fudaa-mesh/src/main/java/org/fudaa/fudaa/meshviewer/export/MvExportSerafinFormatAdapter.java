/*
 * @creation 13 mai 2005
 * 
 * @modification $Date: 2007-06-29 15:09:42 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import java.io.IOException;
import java.util.HashMap;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormatVersionInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinMaillageBuilderAdapterAbstract;
import org.fudaa.dodico.telemac.io.TelemacVariableMapper;

import com.memoire.fu.FuEmptyArrays;

/**
 * @author Fred Deniger
 * @version $Id: MvExportSerafinFormatAdapter.java,v 1.8 2007-06-29 15:09:42 deniger Exp $
 */
public class MvExportSerafinFormatAdapter extends SerafinMaillageBuilderAdapterAbstract {

  final EfGridData data_;
  int[] initIpobo_;
  final boolean isVolumique_;
  // pour eviter de recalculer a chaque fois les donn�es.
  EfData old_;
  int oldTime_ = -1;
  int oldVar_ = -1;
  int[] boundaryConditions;
  double[] timeStep_;
  private boolean xYdoublePrecision = true;
  final CtuluVariable[] var_;

  /**
   * @param _var
   * @param _data
   * @param _vars2Names Un mapping entre les variables selectionn�es et le nom correspondant � Telemac.
   * Si null, on fait le mapping a partir du mapper Telemac standard.
   * standard.
   */
  public MvExportSerafinFormatAdapter(final CtuluVariable[] _var, final EfGridData _data, final double[] _ts,
          final int _language, boolean _volumique, HashMap<CtuluVariable, String> _vars2Names) {
    super((SerafinFileFormatVersionInterface) (SerafinFileFormat.getInstance().getLastVersionInstance(null)), _data
            .getGrid());
    isVolumique_ = _volumique;
    var_ = _var == null ? new CtuluVariable[0] : _var;
    data_ = _data;
    timeStep_ = var_.length == 0 ? FuEmptyArrays.DOUBLE0 : _ts;
    if (timeStep_ == null) {
      timeStep_ = new double[]{0};
    }
    valuesName_ = new String[var_.length];
    final String[] units = new String[var_.length];
    final TelemacVariableMapper varMapper = new TelemacVariableMapper();
    for (int i = valuesName_.length - 1; i >= 0; i--) {
      if (_vars2Names == null)
        valuesName_[i]=varMapper.getSerafinKnownVar(var_[i], _language);
      else
        valuesName_[i]=_vars2Names.get(var_[i]);
      
      if (valuesName_[i] == null) {
        valuesName_[i] = var_[i].getName();
      }
      units[i] = TelemacVariableMapper.getUnits(valuesName_[i]);
    }
    setUnits(units);
  }

  @Override
  public boolean isXYdoublePrecision() {
    return xYdoublePrecision;
  }

  public void setxYdoublePrecision(boolean xYdoublePrecision) {
    this.xYdoublePrecision = xYdoublePrecision;
  }

  @Override
  public int[] getIpoboInitial() {
    if (initIpobo_ != null) {
      return initIpobo_;
    }
    return super.getIpoboInitial();
  }

  @Override
  public double getTimeStep(final int _i) {
    return timeStep_[_i];
  }

  @Override
  public int getTimeStepNb() {
    return timeStep_.length;
  }

  @Override
  public double getValue(final int _numVariable, final int _pasTemps, final int _ptIdx) throws IOException {
    if (_numVariable != oldVar_ || _pasTemps != oldTime_ || old_ == null) {
      oldTime_ = _pasTemps;
      oldVar_ = _numVariable;
      old_ = data_.getData(var_[oldVar_], oldTime_);
    }
    return old_.getValue(_ptIdx);
  }

  @Override
  public String getValueId(final int _i) {
    return valuesName_[_i];
  }

  @Override
  public int getValueNb() {
    return valuesName_.length;
  }

  @Override
  public boolean isVolumique() {
    return isVolumique_;
  }

  public void setInitIpobo(final int[] _initIpobo) {
    initIpobo_ = _initIpobo;
  }

  @Override
  public ConditionLimiteEnum[] getBoundaryConditions() {
    return ConditionLimiteHelper.EMPTY_ARRAY;
  }
}