/*
 *  @creation     17 mai 2005
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.dodico.ef.operation.type.EfOperationToT3;

/**
 * PErmet de transformer un maillage en T3.
 * 
 * @author Fred Deniger
 * @version $Id: MvExportToT3Activity.java,v 1.9 2007-05-04 13:59:50 deniger Exp $
 */
public class MvExportToT3Activity implements MvExportAdaptI {

  boolean stop_;
  final EfGridData src_;

  @Override
  public void stop() {
    stop_ = true;
  }

  public MvExportToT3Activity(final EfGridData _src) {
    src_ = _src;
  }

  @Override
  public EfGridData process(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    EfOperationToT3 toT3 = new EfOperationToT3();
    toT3.setInitGridData(src_);
    EfOperationResult process = toT3.process(_prog);
    _analyze.merge(process.getAnalyze());
    return process.getGridData();
  }
}
