/*
 *  @file         MvCheckNodeModelDefault.java
 *  @creation     10 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import java.util.Arrays;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.model.MvNodeModelDefault;

/**
 * @author deniger
 * @version $Id: MvCheckNodeModelDefault.java,v 1.6 2007-01-19 13:14:15 deniger Exp $
 */
public class MvCheckNodeModelDefault extends MvNodeModelDefault implements MvCheckNodeModel {

  private int[] r_;
  private String desc_;

  /**
   * @param _g
   */
  public MvCheckNodeModelDefault(final EfGridInterface _g) {
    super(_g);
  }

  @Override
  public boolean isErroneous(final int _ptIDx) {
    return r_ == null ? false : (Arrays.binarySearch(r_, _ptIDx) >= 0);
  }

  @Override
  public String getDesc() {
    return desc_ == null ? "?" : desc_;
  }

  @Override
  public void updateCheckMessage(final MvControlNodeResult _r) {
    r_ = _r.getIdx();
    if (r_ != null) {
      Arrays.sort(r_);
    }
    desc_ = _r.getDesc();
  }

  @Override
  public int getNbPtWithError() {
    return r_ == null ? 0 : r_.length;
  }

  @Override
  public int getPtIdxWithError(final int _i) {
    return r_[_i];
  }

}
