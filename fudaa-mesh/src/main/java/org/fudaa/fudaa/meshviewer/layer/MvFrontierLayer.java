/*
 * @creation 2 oct. 06
 * @modification $Date: 2007-01-19 13:14:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.fudaa.fudaa.meshviewer.model.Mv3DFrontierData;

/**
 * @author fred deniger
 * @version $Id: MvFrontierLayer.java,v 1.2 2007-01-19 13:14:08 deniger Exp $
 */
public interface MvFrontierLayer {

  int[] getSelectedFrontier();

  boolean isSelectionEmpty();

  /**
   * @param _idxFr
   * @return true si la frontiere est interne
   */
  boolean isInternFr(int _idxFr);

  Mv3DFrontierData createData();

}
