/*
 *  @creation     13 mai 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.filter;

import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.ef.EfFilterSelectedNode;
import org.fudaa.dodico.ef.EfFilterTime;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class MvFilterSelectedNode extends EfFilterSelectedNode implements EfFilterTime {

  /**
   * @param _selectedPt
   * @param _g
   */
  public MvFilterSelectedNode(final CtuluListSelectionInterface _selectedPt, final EfGridInterface _g) {
    super(_selectedPt, _g);
  }

  @Override
  public void updateTimeStep(final int _t, final EfGridData _src) {}

}
