/*
 *  @file         GVParentInterface.java
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2006-09-19 15:11:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer;

import com.memoire.bu.BuTask;
import java.awt.Frame;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author deniger
 * @version $Id: MvParentInterface.java,v 1.6 2006-09-19 15:11:18 deniger Exp $
 */
public interface MvParentInterface {

  Frame getMvParentComponent();

  void setActionRunning(boolean _b);

  ProgressionInterface createProgression(BuTask _t);

  void setMessage(String _s);

  void setProgression(int _v);

}
