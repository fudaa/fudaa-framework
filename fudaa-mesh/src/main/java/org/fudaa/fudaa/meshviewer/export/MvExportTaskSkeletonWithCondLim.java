package org.fudaa.fudaa.meshviewer.export;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.JComponent;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Panel de task pour les exports avec conditions limites.
 * 
 * @author Christophe CANEL (Genesis)
 */
public class MvExportTaskSkeletonWithCondLim extends MvExportTaskSkeleton implements ItemListener {

  ExportPanelFileChooser pnFileCondLimites_;

  public MvExportTaskSkeletonWithCondLim(EfGridData src, InterpolationVectorContainer vect, MvExportActInterface act) {
    super(src, vect, act);

  }

  public File[] getDestFilesCondLim() {
    return pnFileCondLimites_.getSelectedFiles();
  }

  public final void setPnFileCondLimites(final ExportPanelFileChooser _pnFile) {
    pnFileCondLimites_ = _pnFile;
  }

  @Override
  public File[] getDestFiles() {
    File[] exportedFile = super.getDestFiles();
    File[] condLimFile = this.pnFileCondLimites_.getSelectedFiles();

    if (condLimFile != null) {
      int nbExportedFile = exportedFile.length;
      int nbCondLimFile = condLimFile.length;
      File[] filesToExport = new File[nbExportedFile + nbCondLimFile];

      for (int i = 0; i < nbExportedFile; i++) {
        filesToExport[i] = exportedFile[i];
      }

      for (int i = 0; i < nbCondLimFile; i++) {
        filesToExport[i + nbExportedFile] = condLimFile[i];
      }

      return filesToExport;
    }

    return exportedFile;
  }

  @Override
  protected String isValidPanelFile() {
    String mess = super.isValidPanelFile();

    // Valide si le fichier de conditions limites est correct seulement si l'utilisateur en a entr� un.
    if (this.pnFileCondLimites_.getSelectedFiles() != null) {
      String tmpMsg = this.pnFileCondLimites_.getErrorMessage();

      if (mess == null) {
        mess = tmpMsg;
      } else {
        mess += tmpMsg;
      }
    }

    return mess;
  }

  /**
   * Surcharge de constructPanelFiles. {@inheritDoc}
   */
  @Override
  protected void constructPanelFiles(JComponent comp) {
    super.constructPanelFiles(comp);
    comp.add(pnFileCondLimites_.getComponent());
  }

  @Override
  public void itemStateChanged(ItemEvent arg0) {

  }

}
