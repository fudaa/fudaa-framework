/**
 * @creation 21 d�c. 2004
 * @modification $Date: 2007-06-05 09:01:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.Envelope;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZModelePolygone;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoModelInterface.java,v 1.4 2007-06-05 09:01:14 deniger Exp $
 */
public interface MvIsoModelInterface extends ZModelePolygone {

  /**
   * @return le nombre d'element
   */
  int getNbElt();

  EfGridInterface getGrid();

  public Envelope getEnvelopeForElement(int idxElt);

  /**
   *
   * @param idxElt
   * @return
   */
  int getNbPt(int idxElt);

  /**
   *
   * @param idxElt
   * @param idxPtInElt
   * @return x value
   */
  double getX(int idxElt, int idxPtInElt);

  /**
   *
   * @param idxElt
   * @param idxPtInElt
   * @return
   */
  double getY(int idxElt, int idxPtInElt);

  /**
   *
   * @param idxElt
   * @return true if the element must be painted
   */
  @Override
  boolean isPainted(int idxElt);

  double[] fillWithData(final int _idxElement, final double[] _l);

  /**
   *
   * @param idxElt
   * @param idxPtOnElt the index in the mesh.
   * @return the value
   */
  public double getDatatFor(int idxElt, int idxPtOnElt);
}
