/**
 * @creation 10 f�vr. 2004
 * @modification $Date: 2006-11-14 09:08:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.fudaa.meshviewer.layer.MvElementLayer;

/**
 * @author deniger
 * @version $Id: MvCheckElementLayer.java,v 1.16 2006-11-14 09:08:07 deniger Exp $
 */
public class MvCheckElementLayer extends MvElementLayer {

  TraceSurface ts_;

  /**
   * @param _m le modele correspondant
   */
  public MvCheckElementLayer(final MvCheckElementModel _m) {
    super(_m);
    setBackground(Color.red);
    setIconModel(0, new TraceIconModel(TraceIcon.CROIX, 5, Color.RED));
  }

  private MvCheckElementModel getM() {
    return (MvCheckElementModel) modele_;
  }

  /**
   * @param _g
   */
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) { return; }
    final MvCheckElementModel m = getM();
    final int nbElt = m.getNbEltWithError();
    if ((nbElt <= 0) && (isSelectionEmpty())) { return; }
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final boolean attenue = isAttenue();
    Color background = getBackground();
    if (attenue) {
      background = attenueCouleur(background);
    }
    final GrPolygone p = new GrPolygone();
    final GrBoite bPoly = new GrBoite();
    // utilse uniquement pour dessiner la croix afin de voir
    // facilement les elements detectes.
    final GrPoint centre = new GrPoint();
    if (ts_ == null) {
      ts_ = new TraceSurface();
    }
    ts_.setCouleurFond(background);
    final TraceIcon icone = new TraceIcon(new TraceIconModel(iconModel_));
    icone.setCouleur(background);
    for (int i = nbElt - 1; i >= 0; i--) {
      modele_.polygone(p, m.getEltIdxWithError(i), true);
      p.boite(bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }
      final int nbPoints = p.sommets_.nombre();
      if (nbPoints <= 0) {
        continue;
      }
      p.autoApplique(versEcran);
      p.centre(centre);
      icone.paintIconCentre(this, _g, centre.x_, centre.y_);
      ts_.remplitPolygone(_g, p.polygon());
    }
  }

  @Override
  public String getTitle() {
    return getM().getDesc();
  }

}