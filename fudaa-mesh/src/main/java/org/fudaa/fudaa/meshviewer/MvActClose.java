/*
 *  @file         GVClose.java
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer;

/**
 * @author deniger
 * @version $Id: MvActClose.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public interface MvActClose extends MvAct {

  /**
   * @return true si l'utilisateur a accepte de fermer les sources en cours
   */
  boolean close(MvControllerSrc _s, MvActSave _saver);

}
