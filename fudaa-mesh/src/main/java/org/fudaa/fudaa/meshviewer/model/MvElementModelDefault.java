/**
 * @creation 6 f�vr. 2004
 * @modification $Date: 2007-01-19 13:14:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntHashSet;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnesAbstract;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author Fred Deniger
 * @version $Id: MvElementModelDefault.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
 */
public class MvElementModelDefault extends ZModeleDonnesAbstract implements MvElementModel {

  /**
   * @author Fred Deniger
   * @version $Id: MvElementModelDefault.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
   */
  final class ElementTableModel extends AbstractTableModel {

    int maxRow_;

    ElementTableModel() {
      maxRow_ = g_.getMaxPointInElements();
    }

    @Override
    public int getColumnCount() {
      return maxRow_ + 1;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return MvResource.getS("Indice");
      }
      return MvResource.getS("Indice du noeud {0}", CtuluLibString.getString(_column));
    }

    @Override
    public int getRowCount() {
      return g_.getEltNb();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _column) {
      if (_column == 0) {
        return new Integer(_rowIndex + 1);
      }
      final EfElement e = g_.getElement(_rowIndex);
      final int idx = _column - 1;
      if (idx >= 0 && idx < e.getPtNb()) {
        return new Integer(e.getPtIndex(idx) + 1);
      }
      return CtuluLibString.EMPTY_STRING;
    }
  }

  public static boolean polygone(final EfGridInterface _g, final GrPolygone _p, final int _i) {
    final EfElement e = _g.getElement(_i);
    MvElementModelDefault.initGrPolygone(_g, e, _p);
    return true;
  }

  @Override
  public boolean getCentre(final GrPoint pt, final int i, final boolean force) {
    return cente(g_, pt, i);
  }

  public static boolean cente(final EfGridInterface grid, final GrPoint pt, final int idx) {
    final EfElement e = grid.getElement(idx);
    pt.x_ = e.getMilieuX(grid);
    pt.y_ = e.getMilieuY(grid);
    return true;
  }
  protected MvInfoDelegate delegate_;
  protected EfGridInterface g_;

  public EfGridInterface getG() {
    return g_;
  }

  public MvElementModelDefault(final EfGridInterface _g) {
    this(_g, null);
  }

  @Override
  public boolean isPainted(int idxElt) {
    return true;
  }

  public MvElementModelDefault(final EfGridInterface _g, final MvInfoDelegate _d) {
    g_ = _g;
    delegate_ = _d;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    if (delegate_ != null) {
      delegate_.fillWithElementInfo(_d, _layer.getLayerSelection(), _layer.getTitle());
    }

  }

  @Override
  public void fillWithPtIdx(final int _i, final TIntHashSet _list) {
    g_.getElement(_i).fillList(_list);
  }

  public MvInfoDelegate getDelegate() {
    return delegate_;
  }

  public final int getNbElt() {
    return g_.getEltNb();
  }

  @Override
  public final double getX(int idxElt, int idxPtInElt) {
    return g_.getPtX(g_.getElement(idxElt).getPtIndex(idxPtInElt));
  }

  @Override
  public final Envelope getEnvelopeForElement(int idxElt) {
    return g_.getEnvelopeForElement(idxElt);
  }

  @Override
  public final double getY(int idxElt, int idxPtInElt) {
    return g_.getPtY(g_.getElement(idxElt).getPtIndex(idxPtInElt));
  }

  @Override
  public final int getNbPt(int idxElt) {
    return g_.getElement(idxElt).getPtNb();
  }

  @Override
  public GrBoite getDomaine() {
    if (getNbPoint()==0 && getNbElt()==0)
      return null;
    
    final GrBoite b = new GrBoite();
    b.o_ = new GrPoint(g_.getMinX(), g_.getMinY(), 0);
    b.e_ = new GrPoint(g_.getMaxX(), g_.getMaxY(), 0);
    return b;
  }

  @Override
  public int getNbPoint() {
    return g_.getPtsNb();
  }

  @Override
  public int getNombre() {
    return g_.getEltNb();
  }

  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public void polygone(final GrPolygone _p, final int _i, final boolean _force) {
    polygone(g_, _p, _i);
  }

  public void setDelegate(final MvInfoDelegate _delegate) {
    delegate_ = _delegate;
  }

  @Override
  public void setGrid(final EfGrid _g) {
    g_ = _g;
  }

  @Override
  public int getFrNbFrontier() {
    return g_.getFrontiers() == null ? 0 : g_.getFrontiers().getNbFrontier();
  }

  @Override
  public int getFrNbPointInFrontier(final int _i) {
    return g_.getFrontiers().getNbPt(_i);
  }

  @Override
  public final boolean point(final int _idxPt, final GrPoint _pt) {
    _pt.setCoordonnees(g_.getPtX(_idxPt), g_.getPtY(_idxPt), 0);
    return true;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable r = new CtuluTable();
    r.setModel(new ElementTableModel());
    EbliTableInfoPanel.setTitle(r, _layer.getTitle());
    return r;
  }

  @Override
  public EfGridInterface getGrid() {
    return g_;
  }

  @Override
  public int getFrGlobalIdx(final int _idxFrontier, final int _idxPtOnFrontier) {
    return g_.getFrontiers().getIdxGlobal(_idxFrontier, _idxPtOnFrontier);
  }

  @Override
  public int getIndice(final int _idxElt, final int _idxPtInElt) {
    return g_.getElement(_idxElt).getPtIndex(_idxPtInElt);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  public static void initGrPolygone(final EfGridInterface _ma, final EfElement _pt, final GrPolygone _ptToinit) {
    final int n = _pt.getPtNb();
    if (_ptToinit.nombre() != n) {
      _ptToinit.sommets_.setSize(n);
    }
    GrPoint grpt;
    for (int i = 0; i < n; i++) {
      grpt = _ptToinit.sommets_.renvoie(i);
      final int idx = _pt.getPtIndex(i);
      if (grpt == null) {
        grpt = new GrPoint(_ma.getPtX(idx), _ma.getPtY(idx), 0);
        _ptToinit.sommets_.remplace(grpt, i);
      } else {
        grpt.setCoordonnees(_ma.getPtX(idx), _ma.getPtY(idx), 0);
      }
    }
  }
}
