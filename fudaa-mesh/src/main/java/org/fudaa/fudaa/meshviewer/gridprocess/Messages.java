package org.fudaa.fudaa.meshviewer.gridprocess;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages
{
    private static final String         BUNDLE_NAME     = "org.fudaa.fudaa.meshviewer.gridprocess.gridprocess"; //$NON-NLS-1$

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private Messages()
    {
    }

    public static String getString(String key)
    {
        try
        {
            return RESOURCE_BUNDLE.getString(key);
        }
        catch (MissingResourceException e)
        {
            return '!' + key + '!';
        }
    }
    
    public static String getString(String key,Object... args)
    {
        try
        {
            String string = RESOURCE_BUNDLE.getString(key);
            return MessageFormat.format(string, args);
        }
        catch (MissingResourceException e)
        {
            return '!' + key + '!';
        }
    }
}
