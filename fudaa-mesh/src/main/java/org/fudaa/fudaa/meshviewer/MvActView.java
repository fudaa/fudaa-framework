/**
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer;

import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.impl.EfGridSourceMutable;

/**
 * @author deniger
 * @version $Id: MvActView.java,v 1.8 2007-01-19 13:14:16 deniger Exp $
 */
public interface MvActView extends MvAct {

  void init(EfGridSource _source);

  void refresh(EfGridSource _source, EfGridSourceMutable _enCours);

  void updateCheck(MvControlResult[] _a);
}
