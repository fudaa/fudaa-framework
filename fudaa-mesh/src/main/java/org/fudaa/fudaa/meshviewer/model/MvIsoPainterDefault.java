/**
 * @creation 12 juil. 2004
 * @modification $Date: 2007-05-04 13:59:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.PaletteManager;
import org.fudaa.ebli.trace.TraceIsoSurfacesAvecPlages;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPainterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public abstract class MvIsoPainterDefault implements MvIsoPainter {

  protected EfGridInterface g_;
  protected BPalettePlageAbstract pal_;

  public MvIsoPainterDefault(final EfGridInterface _g) {
    g_ = _g;
  }

  @Override
  public boolean isPaletteInitialized() {
    return pal_ != null;
  }

  protected void initPal() {
    if (pal_ == null) {
      pal_ = new BPalettePlage();
      ((BPalettePlage) pal_).initPlages(10, getMin(), getMax());
      pal_.setTitre(getNom());
      pal_.setSousTitre(CtuluLibString.EMPTY_STRING);
      ((BPalettePlage) pal_).initCouleurs(PaletteManager.INSTANCE);
    }
  }

  public double[] fillWithData(final int _idxElt, final double[] _v) {
    final EfElement ef = g_.getElement(_idxElt);
    double[] r = _v;
    final int nb = ef.getPtNb();
    if ((_v == null) || (_v.length != nb)) {
      r = new double[nb];
    }
    for (int i = nb - 1; i >= 0; i--) {
      r[i] = getValue(ef.getPtIndex(i));
    }
    return r;
  }

  @Override
  public BPalettePlageAbstract getPalette() {
    if (pal_ == null) {
      initPal();
    }
    return pal_;
  }

  @Override
  public void setPalette(final BPalettePlageAbstract _palette) {
    pal_ = _palette;
  }

  public abstract double getValue(int _idxPoint);

  @Override
  public boolean isDiscrete() {
    return false;
  }

  @Override
  public void paint(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel, final int _alpha) {
    int i;
    final GrBoite boite = new GrBoite();
    boite.o_ = new GrPoint(g_.getMinX(), g_.getMinY(), 0);
    boite.e_ = new GrPoint(g_.getMaxX(), g_.getMaxY(), 0);
    if (!boite.intersectXY(_clipReel)) { return; }
    final GrMorphisme versEcran = _versEcran;
    initPal();
    final TraceIsoSurfacesAvecPlages isos = new TraceIsoSurfacesAvecPlages(pal_, _alpha);
    final GrPolygone poly = new GrPolygone();
    final int nb = g_.getEltNb();
    final GrBoite bTemp = new GrBoite();
    double[] v = null;

    for (i = 0; i < nb; i++) {
      MvElementModelDefault.polygone(g_, poly, i);
      poly.boite(bTemp);
      if (!_clipReel.intersectXY(bTemp)) {
        continue;
      }
      v = fillWithData(i, v);
      poly.autoApplique(versEcran);
      isos.draw(_g, poly.polygon(), v);
    }
  }

  @Override
  public String toString() {
    return getNom();
  }
}