/*
 *  @creation     19 d�c. 2005
 *  @modification $Date: 2006-09-19 15:11:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;

/**
 * @author Fred Deniger
 * @version $Id: MvExportAdaptI.java,v 1.2 2006-09-19 15:11:18 deniger Exp $
 */
public interface MvExportAdaptI extends CtuluActivity {

  /**
   * @param _prog la barre de progression
   * @param _analyze l'analyze
   * @return la source transform�e
   */
  EfGridData process(ProgressionInterface _prog, CtuluAnalyze _analyze);
}
