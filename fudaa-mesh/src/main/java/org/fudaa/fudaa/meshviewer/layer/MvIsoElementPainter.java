/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.layer;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;
import org.fudaa.fudaa.meshviewer.model.MvIsoPainter;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoElementPainter.java,v 1.14 2007-05-04 13:59:50 deniger Exp $
 */
public abstract class MvIsoElementPainter implements MvIsoPainter {

  EfGridInterface g_;

  BPalettePlage pal_;

  public MvIsoElementPainter(final EfGridInterface _g) {
    g_ = _g;
    if (g_ == null) {
      FuLog.warning(new Throwable());
    }

  }

  @Override
  public void setPalette(final BPalettePlageAbstract _palette) {
    if (pal_ == null) {
      initPal();
    }
    pal_.initFrom(_palette);
  }

  protected void initPal() {
    if (pal_ == null) {
      pal_ = new BPalettePlage();
      pal_.initPlages(10, getMin(), getMax());
      pal_.setTitre(getNom());
      pal_.setSousTitre(CtuluLibString.EMPTY_STRING);
    }
  }

  @Override
  public BPalettePlageAbstract getPalette() {
    initPal();
    return pal_;
  }

  public abstract double getValue(int _eltIdx);

  @Override
  public boolean isPaletteInitialized() {
    return pal_ != null;
  }

  @Override
  public void paint(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel, final int _alpha) {
    final GrBoite d = MvGridLayerGroup.getDomaine(g_);
    if (!d.intersectXY(_clipReel)) { return; }
    final GrPolygone p = new GrPolygone();
    final GrBoite pBoite = new GrBoite();
    final Color old = _g.getColor();
    initPal();
    for (int i = g_.getEltNb() - 1; i >= 0; i--) {
      if (paintValue(i)) {
        MvElementModelDefault.initGrPolygone(g_, g_.getElement(i), p);
        p.boite(pBoite);
        final Color c = EbliLib.getAlphaColor(pal_.getColorFor(getValue(i)), _alpha);
        if (c != null && pBoite.intersectXY(_clipReel)) {
          _g.setColor(c);
          p.autoApplique(_versEcran);
          _g.fill(p.polygon());
        }
      }
    }
    _g.setColor(old);
  }

  public boolean paintValue(final int _eltIdx) {
    return true;
  }

  @Override
  public String toString() {
    return getNom();
  }

}