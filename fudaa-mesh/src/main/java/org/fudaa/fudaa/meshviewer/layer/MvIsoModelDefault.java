/*
 GPL 2
 */
package org.fudaa.fudaa.meshviewer.layer;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;

/**
 * une implementation par defaut.
 *
 * @author Frederic Deniger
 */
public class MvIsoModelDefault extends MvElementModelDefault implements MvIsoModelInterface {

  private EfGridData data;
  private CtuluVariable var;
  private int tIdx;

  public MvIsoModelDefault(EfGridData data) {
    super(data.getGrid());
    this.data = data;
  }

  @Override
  public boolean isPainted(int idxElt) {
    return super.isPainted(idxElt);
  }
  
  public EfGridData getData() {
    return data;
  }

  public void setData(EfGridData data) {
    this.data = data;
  }

  public CtuluVariable getVar() {
    return var;
  }

  public void setVar(CtuluVariable var) {
    this.var = var;
  }

  public int gettIdx() {
    return tIdx;
  }

  public void settIdx(int tIdx) {
    this.tIdx = tIdx;
  }

  @Override
  public double[] fillWithData(int _idxElement, double[] _l) {
    final EfElement el = data.getGrid().getElement(_idxElement);
    final int n = el.getPtNb();
    double[] r = _l;
    if ((r == null) || (r.length != n)) {
      r = new double[n];
    }
    try {
      for (int i = 0; i < n; i++) {
        r[i] = data.getData(var, tIdx, el.getPtIndex(i));
      }
    } catch (IOException iOException) {
    }
    return r;
  }

  @Override
  public double getDatatFor(int idxElt, int idxPtOnElt) {
    try {
      return data.getData(var, tIdx, data.getGrid().getElement(idxElt).getPtIndex(idxPtOnElt));
    } catch (IOException iOException) {
    }
    return 0;
  }
}
