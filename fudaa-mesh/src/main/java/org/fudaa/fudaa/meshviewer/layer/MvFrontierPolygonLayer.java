/**
 *  @creation     25 nov. 2004
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.Envelope;
import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.meshviewer.model.MvFrontierModel;

/**
 * @author Fred Deniger
 * @version $Id: MvFrontierPolygonLayer.java,v 1.14 2007-01-19 13:14:08 deniger Exp $
 */
public class MvFrontierPolygonLayer extends MvFrontierLayerAbstract {

  /**
   * @param _cl
   */
  public MvFrontierPolygonLayer(final MvFrontierModel _cl) {
    super(_cl);

    ligneModel_ = new TraceLigneModel();
    iconModel_ = new TraceIconModel(TraceIcon.RIEN, 1, Color.BLACK);
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return null;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if ((m_ == null) || (m_.getNombre() <= 0)) { return; }
    final boolean attenue = isAttenue();
    // BPaletteCouleur paletteCouleur = getPaletteCouleur();
    // BPaletteIcone paletteIcone = getPaletteIcone();
    final Color old = _g.getColor();
    final int nombre = m_.getNbFrontier();
    Color foreground = getForeground();
    if (attenue) {
      foreground = EbliLib.getAlphaColor(attenueCouleur(foreground), alpha_);
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      foreground = EbliLib.getAlphaColor(foreground, alpha_);
    }
    // boolean rapide = isRapide();
    final TraceIcon icone = iconModel_ == null ? null : iconModel_.buildCopy();
    final TraceLigne ligne = ligneModel_.buildCopy();
    ligne.setCouleur(foreground);
    if (icone != null) {
      icone.setCouleur(foreground);
    }
    final GrBoite bPoly = new GrBoite();
    bPoly.o_ = new GrPoint();
    bPoly.e_ = new GrPoint();
    final Envelope e = new Envelope();
    for (int i = 0; i < nombre; i++) {
      e.setToNull();
      m_.getDomaineForPolygoneIdx(i, e);
      bPoly.o_.setCoordonnees(e.getMinX(), e.getMinY(), 0);
      bPoly.e_.setCoordonnees(e.getMaxX(), e.getMaxY(), 0);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(_clipReel) == null) {
        continue;
      }
      final int nbPoints = m_.getNbElementIn(i);
      if (nbPoints <= 0) {
        continue;
      }
      final GrPoint ptOri = new GrPoint();
      m_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(_versEcran);
      if (icone != null) {
        icone.paintIconCentre(this, _g, ptOri.x_, ptOri.y_);
      }
      final GrPoint ptDest = new GrPoint();
      for (int j = 0; j < nbPoints; j++) {
        // le point de dest est initialise
        m_.point(ptDest, i, j);
        ptDest.autoApplique(_versEcran);
        if (icone != null) {
          icone.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
        }
        ligne.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        ptOri.initialiseAvec(ptDest);
      }
    }
    _g.setColor(old);
  }
}
