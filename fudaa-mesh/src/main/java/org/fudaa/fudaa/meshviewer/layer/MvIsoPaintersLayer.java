/**
 * @creation 12 juil. 2004 @modification $Date: 2008-02-20 10:11:51 $ @license GNU General Public License 2 @copyright
 * (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.calque.BCalqueLegende;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZModeleDonneesVide;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.palette.BPalettePlageProperties;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.model.MvIsoPainter;
import org.fudaa.fudaa.meshviewer.model.MvIsoPainterComparator;
import org.fudaa.fudaa.meshviewer.model.MvIsoPaintersSource;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPaintersLayer.java,v 1.32.6.1 2008-02-20 10:11:51 bmarchan Exp $
 */
public final class MvIsoPaintersLayer extends ZCalqueAffichageDonnees implements MvLayerGrid, ListSelectionListener,
        BSelecteurListTarget {

  public static final String ISOPAINTER_ID = "isoPainter.id";
  public static final String ISOPAINTER_PALETTE = "isoPainter.palette";

  public JComponent getTargetComponent() {
    return this;
  }

  @Override
  public int[] getSelectedEdgeIdx() {
    return null;
  }

  @Override
  public boolean isSelectionEdgeEmpty() {
    return false;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return null;
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return null;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public int getNbSet() {
    return 0;
  }

  @Override
  public boolean isAntialiasSupported() {
    return false;
  }
  Icon iconeCalque_;

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    _g.setColor(Color.RED);
    final int h = getIconHeight();
    final int w = getIconWidth();
    _g.fillRect(_x, _y, w - 1, h / 2);
    _g.setColor(Color.GREEN);
    _g.fillRect(_x, _y + h / 2, w - 1, h / 2);
    _g.setColor(Color.YELLOW);
    _g.fillRect(_x + w * 3 / 4, _y + h / 2 - h / 8, w / 4, h / 4);
  }

  /**
   * Ne fait rien : non adpate.
   */
  @Override
  public void setForeground(final Color _v) {
    // super.setForeground(_v);
  }

  @Override
  public String getDataDescription() {
    final MvIsoPainter p = getSelectedIsoPainter();
    if (p != null) {
      return p.getNom();
    }
    return null;
  }

  @Override
  public BPalettePlageInterface getPaletteCouleur() {
    final MvIsoPainter p = getSelectedIsoPainter();
    return p == null ? null : p.getPalette();
  }

  public boolean isForegroundColorModifiable() {
    return false;
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (!_e.getValueIsAdjusting()) {
      final MvIsoPainter p = getSelectedIsoPainter();
      construitLegende();
      if (p != null) {
        firePropertyChange("paletteCouleur", null, p.getPalette());
      }
      repaint();
    }
  }

  public MvIsoPainter getSelectedIsoPainter() {
    if (selectedPainter_ == null || selectedPainter_.getMinSelectionIndex() < 0) {
      return null;
    }
    final int i = selectedPainter_.getMinSelectionIndex();
    if ((i < 0) || (i >= isoPainters_.length)) {
      return null;
    }
    return isoPainters_[i];
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    _b.setToNill();
    final MvIsoPainter p = getSelectedIsoPainter();
    if (p != null) {
      _b.expandTo(p.getMax());
      _b.expandTo(p.getMin());
      return true;
    }
    return false;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return false;
  }

  @Override
  protected void construitLegende() {
    final BCalqueLegende l = getLegende();
    if (isoPainters_ == null) {
      return;
    }
    if (l == null) {
      return;
    }
    if (getSelectedIsoPainter() == null || getSelectedIsoPainter().getPalette() == null) {
      if (paletteLegende_ != null) {
        l.enleve(this);
      }
      return;
    }
    final boolean updateTitle = !getSelectedIsoPainter().isPaletteInitialized();
    final BPalettePlageAbstract plage = getSelectedIsoPainter().getPalette();
    if (updateTitle && plage != null) {
      plage.setTitre(getTitle());
      plage.setSousTitre(getSelectedIsoPainter().getNom());
    }
    if (paletteLegende_ == null) {
      paletteLegende_ = new BPalettePlageLegende(getSelectedIsoPainter().getPalette());
      l.ajoute(this, paletteLegende_, getTitle());
    } else {
      if (paletteLegende_.getModel() == plage) {
        paletteLegende_.allPaletteModified(getSelectedIsoPainter().getPalette());
      } else {
        paletteLegende_.setModel(getSelectedIsoPainter().getPalette());
      }
      if (!l.containsLegend(this)) {
        l.ajoute(this, paletteLegende_, getTitle());
      }

    }
    if (l.isVisible()) {
      l.revalidate();
    }
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    // il faut que les isopainter soient non vide et que le iso painter selectionne
    // puisse etre modifie (une palette non nulle).
    return isoPainters_ != null && isoPainters_.length > 0 && getSelectedIsoPainter() != null
            && getSelectedIsoPainter().getPalette() != null;
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }

  /*
   * public void setPaletteCouleur(BPalettePlage _paletteCouleur){ new Throwable().printStackTrace(); }
   */
  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    final MvIsoPainter p = getSelectedIsoPainter();
    if (p != null) {
      p.getPalette().initFrom(_newPlage);
      if (paletteLegende_ != null) {
        paletteLegende_.allPaletteModified(p.getPalette());
      }
      firePropertyChange("palette", null, _newPlage);
      repaint();
    }
  }

  @Override
  public boolean isDiscrete() {
    return getSelectedIsoPainter() == null ? false : getSelectedIsoPainter().isDiscrete();
  }

  class IsoListModel extends AbstractListModel {

    protected void update(final int _oldSize, final int _newSize) {
      fireIntervalRemoved(this, 0, _oldSize);
      fireIntervalAdded(this, 0, _newSize);
    }

    @Override
    public Object getElementAt(final int _index) {
      if ((isoPainters_ == null)) {
        return null;
      }
      return isoPainters_[_index];
    }

    @Override
    public int getSize() {
      return isoPainters_ == null ? 0 : isoPainters_.length;
    }
  }
  /**
   * Painter utilise pour ne rien dessiner.
   */
  // public static final MvIsoPainter ISO_AUCUN = new MvIsoPainterNone();
  public static final ZModeleDonnees MODELE_VIDE = new ZModeleDonneesVide();

  public static MvIsoPaintersLayer buildIsoLayer(final MvIsoPaintersSource _s, final GrBoite _domaine) {
    return new MvIsoPaintersLayer(_s, _domaine);
  }
  private final GrBoite domaine_;
  MvIsoPainter[] isoPainters_;
  private IsoListModel listModel_;
  private ListSelectionModel selectedPainter_;
  private final MvIsoPaintersSource s_;

  private MvIsoPaintersLayer(final MvIsoPaintersSource _s, final GrBoite _b) {
    s_ = _s;
    domaine_ = _b;
  }

  public void updateList() {
    int oldSize = 0;
    if (listModel_ != null) {
      oldSize = listModel_.getSize();
    }
    // on essaie de recupere l'ancien painter
    final String oldSelected = getSelectedIsoPainter() == null ? null : getSelectedIsoPainter().getNom();
    final Map oldPainter = new HashMap();
    if (isoPainters_ != null) {
      for (int i = isoPainters_.length - 1; i >= 0; i--) {
        oldPainter.put(isoPainters_[i].getNom(), isoPainters_[i].getPalette());
      }
    }
    isoPainters_ = null;

    if (selectedPainter_ != null) {
      selectedPainter_.clearSelection();
    }
    buildModels(oldPainter);
    if (listModel_ != null) {
      listModel_.update(oldSize, listModel_.getSize());
      // on reselectionne l'ancien painter si disp
      if (oldSelected != null) {
        for (int i = listModel_.getSize() - 1; i >= 0; i--) {
          if (((MvIsoPainter) listModel_.getElementAt(i)).getNom().equals(oldSelected)) {
            selectedPainter_.setSelectionInterval(i, i);
            break;
          }
        }

      }
    }

  }

  private void buildModels(final Map _oldPalettes) {
    if (isoPainters_ != null) {
      return;
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build iso models");
    }

    final Set painters = new HashSet();
    s_.getIsoPainters(painters);
    isoPainters_ = new MvIsoPainter[painters.size()];
    painters.toArray(isoPainters_);
    Arrays.sort(isoPainters_, new MvIsoPainterComparator());
    if (_oldPalettes != null) {
      for (int i = isoPainters_.length - 1; i >= 0; i--) {
        final BPalettePlageAbstract old = (BPalettePlageAbstract) _oldPalettes.get(isoPainters_[i].getNom());
        if (old != null) {
          isoPainters_[i].setPalette(old);
        }
      }
    }
    if (selectedPainter_ == null) {
      selectedPainter_ = new DefaultListSelectionModel();
      selectedPainter_.addListSelectionListener(this);
      selectedPainter_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
  }

  @Override
  public void fillWithInfo(final InfoData _m) {
  }

  @Override
  public GrBoite getDomaine() {
    return domaine_;
  }

  @Override
  public ListModel getListModel() {
    buildModels(null);
    if (listModel_ == null) {
      listModel_ = new IsoListModel();
    }
    return listModel_;
  }

  @Override
  public ListSelectionModel getListSelectionModel() {
    buildModels(null);
    return selectedPainter_;
  }

  @Override
  public int[] getSelectedElementIdx() {
    return null;
  }

  @Override
  public int[] getSelectedPtIdx() {
    return null;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    return false;
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return false;
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return MODELE_VIDE;
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties saveUIProperties = super.saveUIProperties();

    MvIsoPainter selectedIsoPainter = getSelectedIsoPainter();
    if (selectedIsoPainter != null) {
      BPalettePlageAbstract palette = selectedIsoPainter.getPalette();
      if (palette != null) {
        saveUIProperties.put(ISOPAINTER_PALETTE, palette.save());
      }
      String id = selectedIsoPainter.getNom();
      saveUIProperties.put(ISOPAINTER_ID, id);

    }
    return saveUIProperties;
  }

  @Override
  public void initFrom(EbliUIProperties _p) {
    super.initFrom(_p);
    String id = _p.getString(ISOPAINTER_ID);
    if (id != null) {
      int selected = getIsoPainter(id);
      if (selected >= 0) {
        getListSelectionModel().setSelectionInterval(selected, selected);
        BPalettePlageProperties savedPalette = (BPalettePlageProperties) _p.get(ISOPAINTER_PALETTE);
        if (savedPalette != null) {
          final BPalettePlageInterface newPalette = createPaletteCouleur();
          newPalette.load(savedPalette);
          setPaletteCouleurPlages(newPalette);
        }
      }
    }
  }

  public int getIsoPainter(String id) {
    if (id == null) {
      return -1;
    }
    ListModel listModel = getListModel();
    int size = listModel.getSize();
    for (int i = 0; i < size; i++) {
      MvIsoPainter isoPainter = (MvIsoPainter) listModel.getElementAt(i);
      if (id.equals(isoPainter.getNom())) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    if ((selectedPainter_ == null) || !isVisible() || isRapide()) {
      return;
    }
    final MvIsoPainter p = getSelectedIsoPainter();
    if (p != null) {
      p.paint(_g, _versEcran, _clipReel, getAlpha());
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
          final GrBoite _clipReel) {
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    return null;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    return null;
  }
}
