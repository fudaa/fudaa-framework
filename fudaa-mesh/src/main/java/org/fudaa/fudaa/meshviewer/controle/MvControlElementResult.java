/*
 *  @file         MvControlElementResult.java
 *  @creation     10 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.fudaa.meshviewer.MvControlResult;

/**
 * @author deniger
 * @version $Id: MvControlElementResult.java,v 1.7 2007-01-19 13:14:16 deniger Exp $
 */
public class MvControlElementResult extends MvControlResult {

  @Override
  public ZCalqueAffichageDonnees buildLayer(final EfGridInterface _g) {
    final MvCheckElementModelDefault m = new MvCheckElementModelDefault(_g);
    m.updateCheckMessage(this);
    return new MvCheckElementLayer(m);
  }

}
