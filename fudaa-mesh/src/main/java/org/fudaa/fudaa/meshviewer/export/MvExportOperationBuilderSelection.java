package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluListSelectionInterface;

/**
 * @author deniger
 */
public interface MvExportOperationBuilderSelection extends MvExportOperationBuilderInterface {

  CtuluListSelectionInterface getSelectedMeshes();
  
}
