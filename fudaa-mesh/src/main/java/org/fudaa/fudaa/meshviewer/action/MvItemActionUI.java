/*
 *  @file         MvControlUI.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2006-09-19 15:11:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.action;

import javax.swing.JComponent;

/**
 * @author deniger
 * @version $Id: MvItemActionUI.java,v 1.4 2006-09-19 15:11:19 deniger Exp $
 */
public interface MvItemActionUI {

  JComponent getComponent();
}
