package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.fudaa.meshviewer.export.MvExportTaskSkeleton.ExportPanel;

/**
 * @author Fred Deniger
 * @version $Id: MvExportPanelFilter.java,v 1.1 2007-04-26 14:36:47 deniger Exp $
 */
public interface MvExportPanelFilter extends ExportPanel{

  MvExportOperationBuilderInterface getOperation(EfGridData _src);
}