/*
 * @creation 16 mai 2005
 * 
 * @modification $Date: 2007-05-04 13:59:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;


import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.FastBitSet;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfFilterSelectedElement;
import org.fudaa.dodico.ef.EfFilterTime;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfOperation;

/**
 * @author Fred Deniger
 * @version $Id: MvExportFilterBuilderTime.java,v 1.7 2007-05-04 13:59:50 deniger Exp $
 */
public class MvExportFilterBuilderTime implements MvExportOperationBuilderSelection {

  EfFilterTime filter_;
  boolean and_;
  EfFilter res_;
  final EfGridData src_;

  boolean stop_;
  boolean strict_;
  int nbTimeStep_;
  private CtuluListSelection selectedElement;

  public final boolean isStrict() {
    return strict_;
  }

  /**
   * @param _src la source avec les pas de temps dans l'ordre
   * @param _filter le filtre a utiliser
   * @param _nbTimeStep le nombre de pas de temps a tester
   * @param _and true s'il faut que le filtre soit accepte pour tous les pas de temps
   * @param _strict true si le filtre est strict
   */
  public MvExportFilterBuilderTime(final EfGridData _src, final EfFilterTime _filter, final int _nbTimeStep,
      final boolean _and, final boolean _strict) {
    super();
    and_ = _and;
    strict_ = _strict;
    src_ = _src;
    filter_ = _filter;
    nbTimeStep_ = _nbTimeStep;
  }

  protected void findFilteredElement(final ProgressionInterface _prog) {
    stop_ = false;
    // le filtre est null tout est ok
    if (filter_ == null) {
      res_ = null;
      return;
    }
    final EfGridInterface g = src_.getGrid();
    final FastBitSet element = new FastBitSet();
    final int nbTime = nbTimeStep_;
    final int nbElt = g.getEltNb();
    // on parcourt les pas de temps
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(4, nbTime * nbElt);
    // pour le and on met tous les bits a true et on les enleve
    if (and_) {
      element.set(0, nbElt);
    }
    if (_prog != null) {
      up.majProgessionStateOnly(DodicoLib.getS("Filtre"));
    }
    for (int i = 0; i < nbTime; i++) {
      if (stop_) { return; }
      final int ti = i;
      // intialisation du filtre pour le pas temps i
      filter_.updateTimeStep(ti, src_);
      // on parcourt tous les elements
      for (int ielt = nbElt - 1; ielt >= 0; ielt--) {
        if (stop_) { return; }
        // si au moins un pas de temps doit etre positif
        if (!and_) {
          // si l'element n'est toujours pas filtre positivement
          // puis si le test est positif on active l'element
          if (!element.get(ielt) && filter_.isActivatedElt(ielt)) {
            element.set(ielt);
          }
        }
        // tous les pas de temps doivent etre positifs
        else if (element.get(ielt) && !filter_.isActivatedElt(ielt)) {
          element.clear(ielt);
        }
        up.majAvancement();
      }
    }
    selectedElement = new CtuluListSelection(element);
    res_ = new EfFilterSelectedElement(selectedElement, src_.getGrid());
  }

  @Override
  public CtuluListSelectionInterface getSelectedMeshes() {
    return selectedElement;
  }

  @Override
  public void buildOperation(final ProgressionInterface _progression) {
    findFilteredElement(_progression);
  }

  @Override
  public EfOperation getOperation(final int _tidx) {
    return new MvExportSourceFilterActivity(src_, res_);
  }

  @Override
  public void stop() {
    stop_ = true;
  }
}
