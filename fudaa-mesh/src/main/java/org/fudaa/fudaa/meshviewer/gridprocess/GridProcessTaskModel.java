package org.fudaa.fudaa.meshviewer.gridprocess;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuNumericValueValidator;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResult;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluCellFileEditor;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.ctulu.gui.CtuluOptionPane;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfDataAdapter;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.frontier.EfFrontierMapper;
import org.fudaa.dodico.ef.frontier.EfFrontierMapperActivity;
import org.fudaa.dodico.ef.impl.EfGridBathyAdapter;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinVolumeFileFormat;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.dodico.ef.operation.clean.EfOperationCleanGrid;
import org.fudaa.dodico.ef.operation.overstressed.EfOperationOverstressed;
import org.fudaa.dodico.ef.operation.refine.EfOperationRefineEdge;
import org.fudaa.dodico.ef.operation.renum.EfOperationBackElimination;
import org.fudaa.dodico.ef.operation.renum.EfOperationRenumeration;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.telemac.io.TelemacCLAdapter;
import org.fudaa.dodico.telemac.io.TelemacCLFileFormat;
import org.fudaa.dodico.telemac.io.TelemacCLLine;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.sig.FSigGeomSrcData;
import org.fudaa.fudaa.sig.FSigVarAttrMapperTableModel.MapperResult;
import org.fudaa.fudaa.sig.wizard.FSigVariableInterpolator;
import org.fudaa.fudaa.sig.wizard.FSigWizardFileMng;

/**
 * @author christophe Canel
 */
public class GridProcessTaskModel implements FudaaPanelTaskModel, ItemListener {
  
  public final static Color OK_COLOR = getOkColor();
  public final static Color ERROR_COLOR = Color.RED;
  public final static String COMBO_IN = "COMBO_IN";
  public final static String COMBO_OUT = "COMBO_OUT";
  private BuPanel panel;
  private FileChooserPanel fileInGrid;
  private FileChooserPanel fileInCL;
  private FileChooserPanel fileOutGrid;
  private FileChooserPanel fileOutCL;
  private ChooseProcessPanel chooser;
  private FileFormatUniquePanel formatInGrid;
  private FileFormatUniquePanel formatOutGrid;
  private boolean stop;
  private CtuluActivity currentActivity;
  private List<FileFormatGridVersion> formatsToWrite;
  private List<FileFormatGridVersion> formatsToRead;
  private File fileIn;
  private InterpolationPanel interpolationPanel;
  
  private static Color getOkColor() {
    Color okColor = CtuluLibSwing.getDefaultLabelForegroundColor();
    
    if (okColor == null) {
      okColor = Color.BLACK;
    }
    return okColor;
  }
  
  private static BuPanel createSubpanel(JComponent component1, JComponent component2) {
    BuPanel panel = new BuPanel(new BuGridLayout(2, 5, 5));
    
    panel.add(component1);
    panel.add(component2);
    
    return panel;
  }
  
  private static BuPanel createPanelWithHelpButton(JComponent component, String helpMsg) {
    BuBorderLayout layout = new BuBorderLayout(5, 5);
    BuPanel panel = new BuPanel(layout);
    HelpButton helpButton = new HelpButton();
    
    helpButton.setHtmlMessage(helpMsg);
    
    panel.add(component);
    panel.add(helpButton, BuBorderLayout.EAST);
    
    return panel;
  }

  /**
   * @param originalMsg
   * @param newMsg
   * @return
   */
  private static String concatMessage(String originalMsg, String newMsg) {
    if (newMsg != null) {
      String resultMsg;
      if (originalMsg == null) {
        resultMsg = new String(newMsg);
      } else {
        resultMsg = originalMsg + "\n" + newMsg;
      }
      return resultMsg;
    }
    return originalMsg;
  }
  
  public GridProcessTaskModel(FileFormat[] fileFormats, File fileIn) {
    Arrays.sort(fileFormats);
    this.formatsToWrite = new ArrayList<FileFormatGridVersion>(fileFormats.length);
    this.formatsToRead = new ArrayList<FileFormatGridVersion>(fileFormats.length);
    this.fileIn = fileIn;
    
    for (int i = 0; i < fileFormats.length; i++) {
      if (fileFormats[i] instanceof FileFormatGridVersion) {
        FileFormatGridVersion format = (FileFormatGridVersion) fileFormats[i];
        
        if (format.canWriteGrid()) {
          this.formatsToWrite.add(format);
        }
        
        if (format.canReadGrid()) {
          this.formatsToRead.add(format);
        }
      }
    }
  }
  
  private void updateDoublePrecisionVisibility() {
    FileFormat fileFormat = formatOutGrid.getSelectedFormat().getFileFormat();
    cbXyDoublePrecision.setVisible(fileFormat != null
            && ((SerafinFileFormat.getInstance() == fileFormat)
            || (SerafinVolumeFileFormat.getInstance() == fileFormat)));
  }
  
  @SuppressWarnings("serial")
  private static class ProcessException extends Exception {
    
    public ProcessException(String arg0) {
      super(arg0);
    }
  }
  
  public static interface Validator {
    
    public String getErrorMessage();
  }
  
  @SuppressWarnings("serial")
  private static class FileFormatUniquePanel extends BuPanel {
    
    private BuComboBox combo;
    private BuLabel label;
    private FileFormatUnique[] formats;
    
    public FileFormatUniquePanel(String title, FileFormatUnique[] formats) {
      this.label = new BuLabel(title);
      this.formats = formats;
      this.combo = new BuComboBox();
      
      for (int i = 0; i < this.formats.length; i++) {
        this.combo.addItem(this.formats[i]);
      }
      combo.setRenderer(new CtuluCellTextRenderer() {
        @Override
        protected void setValue(Object value) {
          super.setValue(((FileFormat) value).getName());
        }
      });
      
      this.setLayout(new BuGridLayout(2, 5, 5));
      this.add(this.label);
      this.add(this.combo);
    }
    
    public BuComboBox getCombo() {
      return combo;
    }
    
    public void setHtmlToolTip(String message) {
      this.combo.setToolTipText("<HTML>" + message + "</HTML>");
    }
    
    public FileFormatGridVersion getSelectedFormat() {
      return (FileFormatGridVersion) this.combo.getSelectedItem();
    }
    
    public void setItemListener(ItemListener listener, String command) {
      this.combo.addItemListener(listener);
      this.combo.setActionCommand(command);
    }
    
    public void selectFormatForFile(File file) {
      for (int i = 0; i < this.formats.length; i++) {
        if (this.formats[i].createFileFilter().accept(file)) {
          this.combo.setSelectedIndex(i);
          break;
        }
      }
    }
  }
  
  @SuppressWarnings("serial")
  private static class FileChooserPanel extends BuPanel implements Validator {
    
    private CtuluFileChooserPanel filePanel;
    private BuLabel fileLabel;
    private boolean isMandatory;
    
    public FileChooserPanel(String title, FileFormat format) {
      this(title, format, false);
    }

    /**
     * La valeur par defaut du champ isMandatory est false.
     *
     * @param title
     * @param isMandatory
     */
    public FileChooserPanel(String title, FileFormat format, boolean isMandatory) {
      this.isMandatory = isMandatory;
      this.fileLabel = new BuLabel(title);
      this.filePanel = new CtuluFileChooserPanel();
      
      this.setFormat(format);
      
      this.setLayout(new BuGridLayout(2, 5, 5));
      this.add(this.fileLabel);
      this.add(this.filePanel);
    }
    
    @Override
    public void setEnabled(boolean isEnable) {
      this.filePanel.setEnabled(isEnable);
    }
    
    public void setFile(File file) {
      this.filePanel.setFile(file);
    }
    
    public void setFormat(FileFormat format) {
      if (format != null) {
        this.filePanel.setExt(format.getExtensions()[0]);
        this.filePanel.setFilter(new FileFilter[]{format.createFileFilter()});
      }
    }
    
    @Override
    public String getErrorMessage() {
      String result = null;
      
      if ((this.isMandatory) || (this.filePanel.getFileWithExt() != null)) {
        // TODO Pour les fichier en entr�e test si il peut-�tre lu et non �crit.
        result = CtuluLibFile.canWrite(this.filePanel.getFileWithExt());
        
        this.fileLabel.setForeground(result == null ? OK_COLOR : ERROR_COLOR);
      }
      
      return result;
    }
    
    public File getFileWithExt() {
      return this.filePanel.getFileWithExt();
    }
    
    public File getFile() {
      return this.filePanel.getFile();
    }
    
    public void setHtmlToolTip(String message) {
      this.filePanel.setToolTipText("<HTML>" + message + "</HTML>");
    }
  }
  
  @SuppressWarnings("serial")
  private static class HelpButton extends BuButton implements ActionListener {
    
    public String message;
    
    public HelpButton() {
      this.setIcon(BuResource.BU.getIcon("crystal_aide"));
      
      this.addActionListener(this);
    }
    
    public void setHtmlMessage(String message) {
      if ((message == null) || (message.length() == 0)) {
        this.message = null;
      } else {
        this.message = "<HTML>" + message + "</HTML>";
      }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      if (this.message != null) {
        CtuluOptionPane.showDialog(this, Messages.getString("gridprocess.helpDialog.title.txt"), new JOptionPane(
                message, JOptionPane.INFORMATION_MESSAGE));
      }
    }
  }
  
  @SuppressWarnings("serial")
  private static class ChooseProcessPanel extends BuPanel implements ActionListener, Validator {
    
    private static final String MIN_DIST_CMD = "MIN_DIST_CMD";
    private static final String BACK_DEP_CMD = "BACK_DEP_CMD";
    private BuCheckBox cleanGridCheckBox;
    private BuCheckBox refineEdgeCheckBox;
    private BuCheckBox overstressedCheckBox;
    private BuCheckBox renumerationCheckBox;
    private BuCheckBox backEliminationCheckBox;
    private BuTextField minDistanceValue;
    private BuTextField vectorLengthValue;
    
    public ChooseProcessPanel() {
      this.initComponent();
      
      this.createUI();
      
      this.cleanGridCheckBox.setActionCommand(MIN_DIST_CMD);
      this.backEliminationCheckBox.setActionCommand(BACK_DEP_CMD);
      
      this.cleanGridCheckBox.addActionListener(this);
      this.backEliminationCheckBox.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
      String cmd = arg0.getActionCommand();
      
      if (cmd == MIN_DIST_CMD) {
        this.minDistanceValue.setEnabled(this.cleanGridCheckBox.isSelected());
      } else if (cmd == BACK_DEP_CMD) {
        this.vectorLengthValue.setEnabled(this.backEliminationCheckBox.isSelected());
      }
    }
    
    private void initComponent() {
      this.cleanGridCheckBox = new BuCheckBox(Messages.getString("gridprocess.distMini.txt"));
      this.refineEdgeCheckBox = new BuCheckBox(Messages.getString("gridprocess.refine.txt"));
      this.overstressedCheckBox = new BuCheckBox(Messages.getString("gridprocess.overstressed.txt"));
      this.renumerationCheckBox = new BuCheckBox(Messages.getString("gridprocess.renum.txt"));
      this.backEliminationCheckBox = new BuCheckBox(Messages.getString("gridprocess.backDep.txt"));
      
      this.minDistanceValue = new BuTextField();
      minDistanceValue.setToolTipText(Messages.getString("gridprocess.distMini.tooltip"));
      this.vectorLengthValue = new BuTextField();
      vectorLengthValue.setToolTipText(Messages.getString("gridprocess.vectorLength.tooltip"));
      
      this.minDistanceValue.setStringValidator(BuStringValidator.DOUBLE);
      this.minDistanceValue.setValueValidator(new BuNumericValueValidator(false, 0d, Double.MAX_VALUE));
      this.vectorLengthValue.setStringValidator(BuStringValidator.INTEGER);
      this.vectorLengthValue.setValueValidator(new BuNumericValueValidator(true, 0d, Integer.MAX_VALUE));
    }
    
    private void createUI() {
      this.minDistanceValue.setEnabled(false);
      this.vectorLengthValue.setEnabled(false);
      
      this.setBorder(BorderFactory.createTitledBorder(Messages.getString("gridprocess.process.txt")));
      
      this.setLayout(new BuVerticalLayout(2, true, false));
      
      this.add(createPanelWithHelpButton(createSubpanel(this.cleanGridCheckBox, this.minDistanceValue), Messages
              .getString("gridprocess.distMini.help.txt")));
      this.add(createPanelWithHelpButton(this.refineEdgeCheckBox, Messages.getString("gridprocess.refine.help.txt")));
      this.add(createPanelWithHelpButton(this.overstressedCheckBox, Messages
              .getString("gridprocess.overstressed.help.txt")));
      this.add(createPanelWithHelpButton(this.renumerationCheckBox, Messages
              .getString("gridprocess.renumeration.help.txt")));
      this.add(createPanelWithHelpButton(createSubpanel(this.backEliminationCheckBox, this.vectorLengthValue), Messages
              .getString("gridprocess.backDep.help.txt")));
    }
    
    @Override
    public String getErrorMessage() {
      String message = null;
      
      initOkColorOnProcessComponent();
      // non on peut se servir du dialogue pour exporter simplement:
      // if ((!this.cleanGridCheckBox.isSelected()) && (!this.refineEdgeCheckBox.isSelected())
      // && (!this.overstressedCheckBox.isSelected()) && (!this.renumerationCheckBox.isSelected())
      // && (!this.backEliminationCheckBox.isSelected())) {
      // message = getProcessErrorMessage();
      // } else {
      message = getMidDistanceErrorMessage(message);
      message = getVectorLengthErrorMessage(message);
      // }
      this.updateUI();
      return message;
    }
    
    private void initOkColorOnProcessComponent() {
      ((TitledBorder) this.getBorder()).setTitleColor(OK_COLOR);
      this.cleanGridCheckBox.setForeground(OK_COLOR);
      this.backEliminationCheckBox.setForeground(OK_COLOR);
    }
    
    private String getVectorLengthErrorMessage(String message) {
      if (this.backEliminationCheckBox.isSelected() && (this.vectorLengthValue.getValue() == null)) {
        this.backEliminationCheckBox.setForeground(ERROR_COLOR);
        message = concatMessage(message, Messages.getString("gridprocess.backDepRule.msg"));
      }
      return message;
    }
    
    private String getMidDistanceErrorMessage(String message) {
      if (this.cleanGridCheckBox.isSelected() && (this.minDistanceValue.getValue() == null)) {
        this.cleanGridCheckBox.setForeground(ERROR_COLOR);
        message = concatMessage(message, Messages.getString("gridprocess.distMinRule.msg"));
      }
      return message;
    }
    
    public boolean mustDoCleanGrid() {
      return this.cleanGridCheckBox.isSelected();
    }
    
    public boolean mustDoRefineEdge() {
      return this.refineEdgeCheckBox.isSelected();
    }
    
    public boolean mustDoOverstressed() {
      return this.overstressedCheckBox.isSelected();
    }
    
    public boolean mustDoRenumeration() {
      return this.renumerationCheckBox.isSelected();
    }
    
    public boolean mustDoBackElimination() {
      return this.backEliminationCheckBox.isSelected();
    }
    
    public double getMinDistance() {
      Double value = (Double) this.minDistanceValue.getValue();
      
      if (value == null) {
        return 0d;
      }
      
      return value.doubleValue();
    }
    
    public int getVectorLength() {
      Integer value = (Integer) this.vectorLengthValue.getValue();
      
      if (value == null) {
        return 0;
      }
      
      return value.intValue();
    }
  }
  
  @SuppressWarnings("serial")
  private static class InterpolationPanel extends BuPanel implements ActionListener, Validator {

    /**
     * @author fred deniger
     * @version $Id: FSigFileLoaderPanel.java,v 1.1 2007-01-19 13:14:10 deniger Exp $
     */
    protected static final class FileListCellEditor extends CtuluCellFileEditor {
      
      @Override
      public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
              final int _row, final int _column) {
        if ("".equals(_value)) {
          setValue("");
        } else {
          final File f = (File) _value;
          setValue(f.getAbsolutePath());
        }
        return this;
      }
      
      @Override
      public Object getCellEditorValue() {
        final Object r = super.getCellEditorValue();
        if (r instanceof File) {
          return r;
        }
        return new File(r.toString());
      }
    }
    
    protected static final class FileListCellRenderer extends CtuluCellTextRenderer {
      
      @Override
      protected void setValue(final Object _value) {
        File f = null;
        if (_value instanceof File) {
          f = (File) _value;
        } else if (_value instanceof String) {
          f = new File((String) _value);
        }
        if (f == null) {
          super.setValue(_value);
        } else {
          super.setValue(f.getName());
          super.setToolTipText(f.getAbsolutePath());
        }
      }
    }
    private BuCheckBox useInterpolation;
    // private BuCheckBox interpolationInZone;
    private BuCheckBox ignorePtExtern;
    // private BuCheckBox useZone;
    // private BuTextField maxDistanceIntZone;
    private BuTextField maxDistanceToIgnore;
    private BuPanel subPanel;
    private BuScrollPane scrollPane;
    private FSigWizardFileMng fileMng;
    private CtuluListEditorPanel panel;
    
    public InterpolationPanel() {
      this.initComponent();
      
      this.createUI();
      
      this.useInterpolation.addActionListener(this);
      this.actionPerformed(null);
    }
    
    public static Dialog getDialogAncestor(final Component _c) {
      if (_c instanceof Frame) {
        return (Dialog) _c;
      }
      Dialog dialog = (Dialog) SwingUtilities.getAncestorOfClass(Dialog.class, _c);
      if (dialog != null) {
        return dialog;
      }
      return dialog;
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
      subPanel.setVisible(useInterpolation());
      Dialog frameAncestor = getDialogAncestor(this);
      if (frameAncestor != null) {
        frameAncestor.pack();
      }
    }
    
    private void initComponent() {
      this.useInterpolation = new BuCheckBox(Messages.getString("gridprocess.useInterpolation.txt"));
      // this.interpolationInZone = new BuCheckBox(Messages.getString("gridprocess.interpolationInZone.txt"));
      this.ignorePtExtern = new BuCheckBox(Messages.getString("gridprocess.ignorePtExtern.txt"));
      // this.useZone = new BuCheckBox(Messages.getString("gridprocess.useZone.txt"));

      // this.maxDistanceIntZone = new BuTextField();
      // this.maxDistanceIntZone.setToolTipText(Messages.getString("gridprocess.maxDistanceIntZone.tooltip"));
      this.maxDistanceToIgnore = new BuTextField();
      this.maxDistanceToIgnore.setToolTipText(Messages.getString("gridprocess.maxDistanceToIgnore.tooltip"));
      
      this.scrollPane = new BuScrollPane();
      this.scrollPane.setPreferredHeight(80);
      this.subPanel = new BuPanel();

      // this.maxDistanceIntZone.setStringValidator(BuStringValidator.DOUBLE);
      // this.maxDistanceIntZone.setValueValidator(new BuNumericValueValidator(false, 0d, Double.MAX_VALUE));
      this.maxDistanceToIgnore.setStringValidator(BuStringValidator.DOUBLE);
      this.maxDistanceToIgnore.setValue(0.1D);
      this.maxDistanceToIgnore.setValueValidator(new BuNumericValueValidator(false, 0d, Double.MAX_VALUE));
      ignorePtExtern.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          maxDistanceToIgnore.setEnabled(ignorePtExtern.isSelected());
        }
      });
      maxDistanceToIgnore.setEnabled(false);
      
      this.fileMng = new FSigWizardFileMng();
      this.fileMng.getModel().setCsvFirstColonne(GISAttributeConstants.BATHY);
      panel = new CtuluListEditorPanel(fileMng.getModel());
      panel.setValueListCellEditor(new FileListCellEditor());
      panel.setValueListCellRenderer(new FileListCellRenderer());
      panel.getTableColumnModel().getColumn(2).setCellEditor(fileMng.getFmtRowEditor());
      panel.getTableColumnModel().getColumn(2).setCellRenderer(fileMng.getFmtRowRenderer());
    }
    
    private void createUI() {
      this.setLayout(new BuVerticalLayout(1, true, false));
      this.add(this.useInterpolation);
      subPanel.setVisible(false);
      this.subPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(2, 15, 2, 0),
              BorderFactory.createTitledBorder(Messages.getString("gridprocess.interpolation.txt"))));
      this.subPanel.setLayout(new BuVerticalLayout(2, true, false));
      // this.subPanel.add(createPanelWithHelpButton(createSubpanel(new BuLabel(Messages
      // .getString("gridprocess.maxDistanceIntZone.txt")), this.maxDistanceIntZone), Messages
      // .getString("gridprocess.maxDistanceIntZone.help.txt")));
      // this.subPanel.add(createPanelWithHelpButton(this.interpolationInZone, Messages
      // .getString("gridprocess.interpolationInZone.help.txt")));
      this.subPanel.add(createPanelWithHelpButton(createSubpanel(ignorePtExtern, maxDistanceToIgnore), Messages
              .getString("gridprocess.ignorePtExtern.help.txt")));
      // this.subPanel.add(createPanelWithHelpButton(createSubpanel(new BuLabel(Messages
      // .getString("gridprocess.maxDistanceToIgnore.txt")), this.maxDistanceToIgnore), Messages
      // .getString("gridprocess.maxDistanceToIgnore.help.txt")));

      // this.subPanel.add(createPanelWithHelpButton(this.useZone, Messages.getString("gridprocess.useZone.help.txt")));

      this.scrollPane.getViewport().add(panel);
      
      this.subPanel.add(this.scrollPane);
      this.add(this.subPanel);
    }
    
    @Override
    public String getErrorMessage() {
      String message = null;
      
      initOkColorOnProcessComponent();
      
      if (this.useInterpolation()) {
        // message = getMaxDistanceIntZoneErrorMessage(message);
        message = getMaxDistanceToIgnoreErrorMessage(message);
        int rowCount = fileMng.getModel().getRowCount();
        if (rowCount == 0) {
          message = concatMessage(message, Messages.getString("interpolation.noInterpolationFileDefined"));
        }
      }
      
      this.updateUI();
      return message;
    }
    
    private void initOkColorOnProcessComponent() {
      // this.maxDistanceIntZone.setBackground(OK_COLOR);
      this.maxDistanceToIgnore.setBackground(OK_COLOR);
    }

    // private String getMaxDistanceIntZoneErrorMessage(String message) {
    // if (this.maxDistanceIntZone.getValue() == null) {
    // this.maxDistanceIntZone.setBackground(ERROR_COLOR);
    // message = concatMessage(message, Messages.getString("gridprocess.maxDistanceIntZoneRule.msg"));
    // }
    // return message;
    // }
    private String getMaxDistanceToIgnoreErrorMessage(String message) {
      if (this.maxDistanceToIgnore.getValue() == null) {
        this.maxDistanceToIgnore.setBackground(ERROR_COLOR);
        message = concatMessage(message, Messages.getString("gridprocess.maxDistanceToIgnoreRule.msg"));
      }
      return message;
    }
    
    public boolean useInterpolation() {
      return this.useInterpolation.isSelected();
    }

    // public boolean interpolationInZone() {
    // return this.interpolationInZone.isSelected();
    // }
    public boolean ignorePtExtern() {
      return this.ignorePtExtern.isSelected();
    }

    // public boolean useZone() {
    // return this.useZone.isSelected();
    // }
    // public double getMaxDistanceIntZone() {
    // Double value = (Double) this.maxDistanceIntZone.getValue();
    //
    // if (value == null) { return 0d; }
    //
    // return value.doubleValue();
    // }
    public double getMaxDistanceToIgnore() {
      Double value = (Double) this.maxDistanceToIgnore.getValue();
      
      if (value == null) {
        return 0d;
      }
      
      return value.doubleValue();
    }
  }
  
  @Override
  public void actTask(ProgressionInterface prog, CtuluAnalyzeGroup analyze, String[] messages) {
    this.stop = false;
    this.currentActivity = null;
    
    EfGridData resGridData = null;
    CtuluAnalyzeGroup logManager = new CtuluAnalyzeGroup(null);
    try {
      resGridData = this.doProcess(prog, logManager);
      if (resGridData != null) {
        messages[0] = Messages.getString("gridprocess.result1.msg");
        messages[2] = Messages.getString("gridprocess.result2.msg", resGridData.getGrid().getPtsNb());
        messages[4] = Messages.getString("gridprocess.result3.msg", resGridData.getGrid().getEltNb());
      }
      
    } catch (ProcessException e) {
      FuLog.error(e);
    } finally {
      prog.reset();
    }
    CtuluAnalyzeGUI.showDialog(logManager, null, Messages.getString("log.title"));
  }
  
  private EfGridData doProcess(ProgressionInterface prog, CtuluAnalyzeGroup analyze) throws ProcessException {
    ReadData initGridData = this.readGrid(prog, analyze.getNewAnalyser(MvResource.getS("Lecture maillage")));
    // en cas d'erreur dans la lecture.
    if (initGridData == null) {
      return null;
    }
    // on lit si non null:
    if (this.fileInCL.getFileWithExt() != null) {
      initGridData.cl = this.readConditionLimit(prog, analyze.getNewAnalyser(MvResource
              .getS("Lecture fichier conditions limites")));
    }
    EfGridData resGridData = this.doOperations(prog, analyze, initGridData.gridData);
    
    if (this.interpolationPanel.useInterpolation()) {
      BathyVariable variable = new BathyVariable(resGridData.getGrid());
      FSigVariableInterpolator interpolator = new FSigVariableInterpolator(variable, null);
      interpolator.setUseLinesInInterpolation(true);
      interpolator.setIgnorePtExtern(this.interpolationPanel.ignorePtExtern());
      interpolator.setMaxDistanceToIgnore(this.interpolationPanel.getMaxDistanceToIgnore());
      CtuluAnalyzeGroup createGroup = analyze.createGroup(Messages.getString("grid.process.interpolation.log.group"));
      CtuluAnalyze readFile = createGroup.getNewAnalyser(Messages.getString("reading.sigFile"));
      MapperResult mapping = new MapperResult();
      mapping.att_ = new GISAttribute[]{GISAttributeConstants.BATHY};
      mapping.vars_ = new H2dVariableType[]{H2dVariableType.BATHYMETRIE};
      interpolator.setUserMappingDefined(mapping);
      FSigGeomSrcData inputs = interpolationPanel.fileMng.loadAll(prog, readFile).createData();
      if (inputs == null) {
        readFile.addWarn(Messages.getString("grid.process.interpolation.stop.noBathy"));
        return null;
      }
      CtuluAnalyze interpolationLog = createGroup.getNewAnalyser(Messages.getString("grid.process.interpolation"));
      if (!inputs.containsDataFor(GISAttributeConstants.BATHY)) {
        interpolationLog.addWarn(Messages.getString("grid.process.interpolation.stop.noBathy"));
        
      } else {
        interpolator.setData(inputs);
        interpolator.process(interpolationLog, null, prog);
        
        resGridData = new EfGridDataDefault(new EfGridBathyAdapter(new EfDataAdapter(variable.getValues(null), false),
                variable.getGrid()));
      }
    }
    
    this.writeGrid(prog, analyze, resGridData);
    this.writeCL(prog, analyze, initGridData, resGridData.getGrid());
    
    return resGridData;
  }
  
  private void writeGrid(ProgressionInterface prog, CtuluAnalyzeGroup analyze, EfGridData resGridData)
          throws ProcessException {
    final EfGridSourceDefaut efGridSourceDefaut = new EfGridSourceDefaut(resGridData.getGrid(), this.formatOutGrid.getSelectedFormat().getFileFormat());
    if (cbXyDoublePrecision.isVisible()) {
      SerafinFileFormat.setDoubleFormatOption(efGridSourceDefaut.getOptions(), cbXyDoublePrecision.isSelected());
    }
    CtuluIOOperationSynthese synthWrite = this.formatOutGrid.getSelectedFormat().writeGrid(
            this.fileOutGrid.getFileWithExt(), efGridSourceDefaut, prog);
    
    analyze.addAnalyzer(synthWrite.getAnalyze());
    
    this.manageStop(synthWrite.getAnalyze());
  }
  
  private void manageStop(CtuluAnalyze currentAnalyze) throws ProcessException {
    if (currentAnalyze.containsErrors() || currentAnalyze.containsFatalError() || this.stop) {
      String message;
      if (this.stop) {
        // TODO Utiliser la classe des messages
        // TODO Y mettre dans messages et pas dans analyse.
        message = Messages.getString("gridprocess.stop.msg");

        // globalAnalyze.addFatalError(message);
      } else {
        message = currentAnalyze.getResume();
      }
      
      throw new ProcessException(message);
    }
  }
  
  private class ReadData {
    
    EfGridData gridData;
    List<TelemacCLLine> cl;
  }
  
  private ReadData readGrid(ProgressionInterface prog, CtuluAnalyze analyze) throws ProcessException {
    ReadData res = new ReadData();
    File file = this.fileInGrid.getFileWithExt();
    
    if (!file.exists()) {
      file = this.fileInGrid.getFile();
    }
    CtuluIOOperationSynthese synthRead = this.formatInGrid.getSelectedFormat().readGrid(file, prog);
    analyze.merge(synthRead.getAnalyze());
    this.manageStop(synthRead.getAnalyze());
    if (analyze.containsFatalError()) {
      return null;
    }
    EfGridSource source = (EfGridSource) synthRead.getSource();
    EfLib.orienteGrid(source.getGrid(), prog, true, analyze);
    EfGridInterface grid = source.getGrid();
    if (source.getBoundaryConditions() != null && source.getBoundaryConditions().length > 0) {
      CtuluResult<ConditionLimiteEnum[]> extractConditionLimiteOnFrontier = ConditionLimiteHelper
              .extractConditionLimiteOnFrontier(source.getBoundaryConditions(), grid, prog);
      if (extractConditionLimiteOnFrontier != null && extractConditionLimiteOnFrontier.getResultat() != null) {
        ConditionLimiteEnum[] limiteEnums = extractConditionLimiteOnFrontier.getResultat();
        res.cl = new ArrayList<TelemacCLLine>(limiteEnums.length);
        for (ConditionLimiteEnum conditionLimiteEnum : limiteEnums) {
          res.cl.add(new TelemacCLLine(conditionLimiteEnum));
        }
      }
    }
    res.gridData = new EfGridDataDefault(grid);
    
    return res;
  }
  
  @Override
  public void decoreBtApply(JButton bt) {
    bt.setText(Messages.getString("gridprocess.execute.txt"));
    bt.setIcon(BuResource.BU.getIcon("crystal_executer"));
  }
  
  @Override
  public void dialogClosed() {
    // TODO Voir si mettre quelque chose.
  }
  
  @Override
  public int getNbMessageMax() {
    return 3;
  }

  // FileFormatGridVersion guessFmt(final File _f, Collection<FileFormatGridVersion> fts) {
  // for (FileFormatGridVersion fileFormat : fts) {
  // if (fileFormat.getFileFormat().createFileFilter().accept(_f)) return fileFormat;
  // }
  // return fts.iterator().next();
  //
  // }
  private void initComponent() {
    this.panel = new BuPanel(new BuVerticalLayout(20, true, false));
    // TODO Traduire label
    this.formatInGrid = new FileFormatUniquePanel(MvResource.getS("Format du fichier de maillage en entr�e"), this.formatsToRead
            .toArray(new FileFormatUnique[0]));
    // FileFormatGridVersion guessFmt = guessFmt(this.fileIn, formatsToRead);
    // formatInGrid.combo.setSelectedItem(guessFmt);
    this.formatOutGrid = new FileFormatUniquePanel(MvResource.getS("Format du fichier de maillage en sortie"), this.formatsToWrite
            .toArray(new FileFormatUnique[0]));
    FileFormatGridVersion selectedFormat = this.formatInGrid.getSelectedFormat();
    this.fileInGrid = new FileChooserPanel(Messages.getString("gridprocess.gridFileIn.txt"),
            selectedFormat == null ? null : selectedFormat.getFileFormat(), true);
    this.fileInCL = new FileChooserPanel(Messages.getString("gridprocess.clFileIn.txt"), TelemacCLFileFormat
            .getInstance());
    this.fileOutGrid = new FileChooserPanel(Messages.getString("gridprocess.gridFileOut.txt"), this.formatOutGrid
            .getSelectedFormat().getFileFormat(), true);
    this.fileOutCL = new FileChooserPanel(Messages.getString("gridprocess.clFileOut.txt"), TelemacCLFileFormat
            .getInstance());
    this.chooser = new ChooseProcessPanel();
    this.interpolationPanel = new InterpolationPanel();
    
    this.fileInCL.setEnabled(!selectedFormat.hasBoundaryConditons());
    // this.fileOutCL.setEnabled(!this.formatOutGrid.getSelectedFormat().hasBoundaryConditons());

    this.fileInGrid.setHtmlToolTip(Messages.getString("gridprocess.gridFileIn.help.txt"));
    this.fileInCL.setHtmlToolTip(Messages.getString("gridprocess.clFileIn.help.txt"));
    this.fileOutGrid.setHtmlToolTip(Messages.getString("gridprocess.gridFileOut.help.txt"));
    this.fileOutCL.setHtmlToolTip(Messages.getString("gridprocess.clFileOut.help.txt"));
    this.formatInGrid.setHtmlToolTip(Messages.getString("gridprocess.formatInGrid.help.txt"));
    this.formatOutGrid.setHtmlToolTip(Messages.getString("gridprocess.formatOutGrid.help.txt"));
    
    this.formatInGrid.setItemListener(this, COMBO_IN);
    this.formatOutGrid.setItemListener(this, COMBO_OUT);
    
    if (this.fileIn != null) {
      this.formatInGrid.selectFormatForFile(this.fileIn);
      this.formatOutGrid.selectFormatForFile(this.fileIn);
      this.fileInGrid.setFile(this.fileIn);
    }
  }
  JCheckBox cbXyDoublePrecision = new JCheckBox(MvResource.getS("Export serafin au format double pr�cision"));
  
  private void createUI() {
    BuPanel pnIn = new BuPanel(new BuVerticalLayout(2));
    
    pnIn.add(this.fileInGrid);
    pnIn.add(this.formatInGrid);
    pnIn.add(this.fileInCL);
    
    BuPanel pnOut = new BuPanel(new BuVerticalLayout(2));
    pnOut.add(this.fileOutGrid);
    pnOut.add(this.formatOutGrid);
    pnOut.add(cbXyDoublePrecision);
    updateDoublePrecisionVisibility();
    formatOutGrid.getCombo().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateDoublePrecisionVisibility();
      }
    });
    
    pnOut.add(this.fileOutCL);
    
    panel.add(pnIn);
    // panel.add(new JSeparator());
    panel.add(this.chooser);
    this.panel.add(this.interpolationPanel);
    // panel.add(new JSeparator());
    this.panel.add(pnOut);
    
  }
  
  @Override
  public JComponent getPanel() {
    if (this.panel == null) {
      this.initComponent();
      
      this.createUI();
    }
    
    return this.panel;
  }
  
  @Override
  public String getTitre() {
    return Messages.getString("gridprocess.title.txt");
  }
  
  @Override
  public String isTakDataValid() {
    String message = null;
    
    message = this.appendMessage(message, this.fileInGrid.getErrorMessage());
    message = this.appendMessage(message, this.fileInCL.getErrorMessage());
    message = this.appendMessage(message, this.chooser.getErrorMessage());
    message = this.appendMessage(message, this.interpolationPanel.getErrorMessage());
    message = this.appendMessage(message, this.fileOutGrid.getErrorMessage());
    message = this.appendMessage(message, this.fileOutCL.getErrorMessage());
    
    if (message == null) {
      StringBuffer name = new StringBuffer();
      
      for (int i = 0; i < 2; i++) {
        File fileToTest = (i == 0) ? this.fileOutGrid.getFileWithExt() : this.fileOutCL.getFileWithExt();
        
        if ((fileToTest != null) && fileToTest.exists()) {
          // TODO Voir si tjs utiliser CtuluLibString.LINE_SEP
          name.append(CtuluLibString.LINE_SEP);
          name.append(fileToTest.getName());
        }
      }
      
      if (name.length() > 0) {
        if (!CtuluLibDialog.confirmeOverwriteFiles(this.panel, name.toString())) {
          // TODO Voir si tjs utiliser CtuluLib.getS
          return Messages.getString("gridprocess.modifyTargetFiles.msg");
        }
      }
    }
    
    return message;
  }
  
  private String appendMessage(String originalMsg, String newMsg) {
    if (newMsg != null) {
      if (originalMsg != null) {
        return new String(originalMsg + "\n" + newMsg);
      } else {
        return newMsg;
      }
    }
    
    return originalMsg;
  }
  
  @Override
  public void stopTask() {
    this.stop = true;
    
    if (this.currentActivity != null) {
      this.currentActivity.stop();
    }
  }
  
  private List<TelemacCLLine> readConditionLimit(final ProgressionInterface prog, final CtuluAnalyze analyze)
          throws ProcessException {
    List<TelemacCLLine> cLAdapter = Collections.emptyList();
    
    if (this.fileInCL.getFileWithExt() != null) {
      CtuluIOOperationSynthese cLReaderSyth = TelemacCLFileFormat.getInstance().getLastVersionImpl().readDataBrutes(
              this.fileInCL.getFileWithExt(), prog);
      cLAdapter = (List<TelemacCLLine>) cLReaderSyth.getSource();
      analyze.merge(cLReaderSyth.getAnalyze());
      this.manageStop(cLReaderSyth.getAnalyze());
    }
    
    return cLAdapter;
  }
  
  private void writeCL(final ProgressionInterface prog, final CtuluAnalyzeGroup analyze, ReadData initGrid,
          EfGridInterface resGrid) throws ProcessException {
    if (this.fileOutCL.getFileWithExt() != null) {
      // peut renvoyer null si pas de conditions limites dans les donnees initiales.
      EfFrontierMapper mapper = getMapper(prog, analyze, initGrid, resGrid);
      resGrid.computeBord(prog, analyze.getNewAnalyser());
      // on construit la liste des TelemacCLLine a utiliser
      int[] ipoboNewGrid = resGrid.getFrontiers().getArray();
      List<TelemacCLLine> cls = new ArrayList<TelemacCLLine>(ipoboNewGrid.length);
      ProgressionUpdater updater = new ProgressionUpdater(prog);
      updater.majProgessionStateOnly(Messages.getString("gridprocess.calculatecl.msg"));
      updater.setValue(10, ipoboNewGrid.length);
      for (int i = 0; i < ipoboNewGrid.length; i++) {
        TelemacCLLine line = new TelemacCLLine(ConditionLimiteEnum.SOLID);
        line.k_ = i;
        line.n_ = ipoboNewGrid[i];
        TelemacCLLine initial = getInitalCl(initGrid, mapper, i);
        // nouveau point: on regarde si est au milieu de cl connus: si oui on affecte la meme cl.
        if (initial == null) {
          TelemacCLLine next = getInitalCl(initGrid, mapper, (i + 1) % ipoboNewGrid.length);
          TelemacCLLine before = getInitalCl(initGrid, mapper, i == 0 ? ipoboNewGrid.length - 1 : (i - 1));
          if (next != null && before != null && next.sameHUVCode(before)) {
            initial = before;
          }
        }
        if (initial != null) {
          line.initCodesFrom(initial);
        }
        cls.add(line);
        updater.majAvancement();
      }
      checkConditionsLimites(resGrid, cls);
      CtuluIOOperationSynthese cLWriterSyth = TelemacCLFileFormat.getInstance().getLastVersionImpl().writeDataBrutes(
              this.fileOutCL.getFileWithExt(), cls, prog);
      
      analyze.addAnalyzer(cLWriterSyth.getAnalyze());
      
      this.manageStop(cLWriterSyth.getAnalyze());
    }
  }

  /**
   * Check to liquid cl is not isolated.
   *
   * @param resGrid
   * @param cls
   */
  private void checkConditionsLimites(EfGridInterface resGrid, List<TelemacCLLine> cls) {
    EfFrontierInterface frontiers = resGrid.getFrontiers();
    int nbFrontier = frontiers.getNbFrontier();
    for (int i = 0; i < nbFrontier; i++) {
      int nbPt = frontiers.getNbPt(i);
      for (int j = 0; j < nbPt; j++) {
        int frIdx = frontiers.getFrontiereIndice(i, j);
        // un point liquid ne doit pas etre isole
        if (cls.get(frIdx).isLiquid()) {
          int next = frontiers.getFrontiereIndice(i, (j + 1) % nbPt);
          int prec = frontiers.getFrontiereIndice(i, j == 0 ? nbPt - 1 : j - 1);
          if (cls.get(next).isSolid() && cls.get(prec).isSolid()) {
            cls.get(next).initCodesFrom(cls.get(prec));
          }
        }
        
      }
    }
  }
  
  private TelemacCLLine getInitalCl(ReadData initGrid, EfFrontierMapper mapper, int i) {
    TelemacCLLine initial = null;
    if (mapper != null) {
      int oldFrtIdx = mapper.getOldFrtIdx(i);
      if (oldFrtIdx >= 0 && initGrid.cl.size() > oldFrtIdx) {
        initial = initGrid.cl.get(oldFrtIdx);
      }
    }
    return initial;
  }
  
  private EfFrontierMapper getMapper(final ProgressionInterface prog, final CtuluAnalyzeGroup analyze,
          ReadData initGrid, EfGridInterface resGrid) {
    if (initGrid.cl == null || initGrid.cl.isEmpty()) {
      return null;
    }
    EfFrontierMapperActivity mapperActivity = new EfFrontierMapperActivity();
    mapperActivity.setOldGrid(initGrid.gridData.getGrid());
    mapperActivity.setNewGrid(resGrid);
    CtuluResult<EfFrontierMapper> res = mapperActivity.compute(prog);
    analyze.addAnalyzer(res.getAnalyze());
    EfFrontierMapper mapper = res.getResultat();
    return mapper;
  }
  
  private H2dTelemacCLElementSource getCLElt(int idx, TelemacCLAdapter cLAdapter) {
    if (cLAdapter != null) {
      int nbLine = cLAdapter.getNbLines();
      H2dTelemacCLElementSource eltSrc = new H2dTelemacCLElementSource();
      
      for (int i = 0; i < nbLine; i++) {
        cLAdapter.getLine(i, eltSrc);
        
        if (eltSrc.ptGlobalIdx_ == idx) {
          return eltSrc;
        }
      }
    }
    
    return H2dTelemacCLElementSource.createDefault();
  }
  
  protected EfGridData doOperations(final ProgressionInterface prog, final CtuluAnalyzeGroup logManager,
          EfGridData initGridData) throws ProcessException {
    EfGridData resGridData = initGridData;
    
    if (this.chooser.mustDoCleanGrid()) {
      resGridData = this.doCleanGrid(prog, logManager, resGridData);
    }
    
    if (this.chooser.mustDoRefineEdge()) {
      resGridData = this.doRefineEdge(prog, logManager, resGridData);
    }
    
    if (this.chooser.mustDoOverstressed()) {
      resGridData = this.doOverstressed(prog, logManager, resGridData);
    }
    if (this.chooser.mustDoBackElimination()) {
      resGridData = this.doBackElimination(prog, logManager, resGridData);
    }
    
    if (this.chooser.mustDoRenumeration()) {
      resGridData = this.doRenumeration(prog, logManager, resGridData);
    }
    
    return resGridData;
  }
  
  private EfGridData doOperation(final ProgressionInterface prog, final CtuluAnalyzeGroup analyse,
          EfOperation operation, EfGridData gridData) throws ProcessException {
    this.currentActivity = operation;
    
    operation.setInitGridData(gridData);
    
    EfOperationResult result = operation.process(prog);
    
    this.currentActivity = null;
    analyse.addAnalyzer(result.getAnalyze());

    // analyse.merge(result.getAnalyze());

    this.manageStop(result.getAnalyze());
    
    return result.getGridData();
  }
  
  private EfGridData doCleanGrid(final ProgressionInterface prog, final CtuluAnalyzeGroup analyse, EfGridData gridData)
          throws ProcessException {
    EfOperationCleanGrid cleanGrid = new EfOperationCleanGrid();
    
    cleanGrid.setMinDistance(this.chooser.getMinDistance());
    
    return this.doOperation(prog, analyse, cleanGrid, gridData);
  }
  
  private EfGridData doRefineEdge(final ProgressionInterface prog, final CtuluAnalyzeGroup analyse, EfGridData gridData)
          throws ProcessException {
    
    EfOperationRefineEdge refineEdge = new EfOperationRefineEdge();
    
    return this.doOperation(prog, analyse, refineEdge, gridData);
  }
  
  private EfGridData doOverstressed(final ProgressionInterface prog, final CtuluAnalyzeGroup analyse,
          EfGridData gridData) throws ProcessException {
    EfOperationOverstressed overstressed = new EfOperationOverstressed();
    
    return this.doOperation(prog, analyse, overstressed, gridData);
  }
  
  private EfGridData doRenumeration(final ProgressionInterface prog, final CtuluAnalyzeGroup analyse,
          EfGridData gridData) throws ProcessException {
    EfOperationRenumeration renumeration = new EfOperationRenumeration();
    
    return this.doOperation(prog, analyse, renumeration, gridData);
  }
  
  private EfGridData doBackElimination(final ProgressionInterface prog, final CtuluAnalyzeGroup analyse,
          EfGridData gridData) throws ProcessException {
    EfOperationBackElimination backElimination = new EfOperationBackElimination();
    
    backElimination.setVectorLength(this.chooser.getVectorLength());
    
    return this.doOperation(prog, analyse, backElimination, gridData);
  }
  
  @Override
  public void itemStateChanged(ItemEvent arg0) {
    String command = ((BuComboBox) arg0.getSource()).getActionCommand();
    
    if (command.equals(COMBO_IN)) {
      this.fileInGrid.setFormat(this.formatInGrid.getSelectedFormat().getFileFormat());
      
      this.fileInCL.setEnabled(!this.formatInGrid.getSelectedFormat().hasBoundaryConditons());
    } else if (command.equals(COMBO_OUT)) {
      this.fileOutGrid.setFormat(this.formatOutGrid.getSelectedFormat().getFileFormat());

      // this.fileOutCL.setEnabled(!this.formatOutGrid.getSelectedFormat().hasBoundaryConditons());
    }
  }
  
  public static void main(String[] args) {
    System.err.println(5 % 5);
  }
}
