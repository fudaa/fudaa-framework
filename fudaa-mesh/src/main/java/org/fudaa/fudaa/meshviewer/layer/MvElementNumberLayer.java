/**
 * @creation 5 d�c. 2003
 * @modification $Date: 2008-02-20 10:11:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntIterator;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZModelePolygone;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvNumberDelegate;
import org.fudaa.fudaa.meshviewer.model.MvNumberLayerI;

/**
 * @author deniger
 * @version $Id: MvElementNumberLayer.java,v 1.27.6.1 2008-02-20 10:11:50 bmarchan Exp $
 */
public class MvElementNumberLayer extends ZCalqueAffichageDonnees implements MvNumberLayerI {

  private final ZModelePolygone model_;
  final MvNumberDelegate delegate_;

  public MvElementNumberLayer(final ZModelePolygone _m) {
    model_ = _m;
    delegate_ = new MvNumberDelegate(_m.getNombre());
    setTitle(MvResource.getS("Num�ros des �l�ments"));
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    return null;
  }

  @Override
  public void setItem(final int[] _idx) {
    if (delegate_.setSelectedPoint(_idx)) {
      quickRepaint();
    }
  }

  @Override
  public void clearIdxShow() {
    if (delegate_.clearIdxShow()) {
      quickRepaint();
    }
  }

  @Override
  public void setAll() {
    if (delegate_.setAll()) {
      quickRepaint();
    }
  }

  @Override
  public boolean isAll() {
    return delegate_.isAll();
  }

  @Override
  public boolean isSomethingToView() {
    return delegate_.isSomethingToView();
  }

  public ZModelePolygone getModel() {
    return model_;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return null;
  }


  @Override
  public ZModeleDonnees modeleDonnees() {
    return model_;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if (isRapide()) { return; }
    if (!isSomethingToView()) { return; }
    final GrBoite clip = _clipReel;
    final GrPolygone p = new GrPolygone();
    final GrPoint pt = new GrPoint();
    final GrMorphisme versEcran = _versEcran;
    final Font old = _g.getFont();
    final Color oldC = _g.getColor();
    _g.setFont(getFont());
    if (isAttenue()) {
      _g.setColor(attenueCouleur(getForeground()));
    } else {
      _g.setColor(getForeground());
    }
    double x, y;
    // Color foreground= g.getColor();
    if (isAll()) {
      final FontMetrics fm = _g.getFontMetrics();
      // Color backColor=getBackground();
      for (int i = model_.getNombre() - 1; i >= 0; i--) {
        model_.polygone(p, i, true);
        p.centre(pt);
        if (clip.contientXY(pt)) {
          final String s = CtuluLibString.getString(i + 1);
          pt.autoApplique(versEcran);
          final Rectangle2D rec = fm.getStringBounds(s, _g);
          x = pt.x_ - rec.getWidth() / 2;
          y = pt.y_ + rec.getHeight() / 2;
          _g.drawString(s, (int) x, (int) y);
        }
      }
    } else {
      final TIntIterator it = delegate_.getIdxIterator();
      final FontMetrics fm = _g.getFontMetrics();
      // Color backColor=getBackground();
      for (int i = delegate_.getNbIdxSelected(); i-- > 0;) {
        final int idx = it.next();
        model_.polygone(p, idx, true);
        p.centre(pt);
        if (clip.contientXY(pt)) {
          pt.autoApplique(versEcran);
          final String s = CtuluLibString.getString(idx + 1);
          final Rectangle2D rec = fm.getStringBounds(s, _g);
          x = pt.x_ - rec.getWidth() / 2;
          y = pt.y_ + rec.getHeight() / 2;
          // rec.setFrame(x, y - fm.getAscent(), rec.getWidth(), rec.getHeight());
          // g.setColor(backColor);
          // ((Graphics2D)g).fill(rec);
          // g.setColor(foreground);
          _g.drawString(s, (int) x, (int) y);
        }
      }
    }
    _g.setFont(old);
    _g.setColor(oldC);
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
      final GrBoite _clipReel) {}

  @Override
  public GrBoite getDomaineOnSelected() {
    return null;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    return null;
  }

  @Override
  public int[] getSelectedIdx(final MvLayerGrid _layer) {
    return _layer == null ? null : _layer.getSelectedElementIdx();
  }

  @Override
  public boolean isSelectionOk(final MvLayerGrid _layer) {
    return _layer == null ? false : !_layer.isSelectionElementEmpty();
  }

}
