/*
 *  @creation     9 sept. 2005
 *  @modification $Date: 2007-06-11 13:08:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: MvExportSrcTimeAdapter.java,v 1.5 2007-06-11 13:08:16 deniger Exp $
 */
public class MvExportSrcTimeAdapter implements EfGridData {

  final int[] time_;
  final EfGridData src_;

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return src_.isDefined(_var);
  }

  /**
   * @param _src
   * @param _time
   */
  public MvExportSrcTimeAdapter(final EfGridData _src, final int[] _time) {
    super();
    src_ = _src;
    time_ = _time;
  }

  @Override
  public double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    return src_.getData(_o, time_[_timeIdx], _idxObjet);
  }

  @Override
  public EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    return src_.getData(_o, time_[_timeIdx]);
  }

  @Override
  public EfGridInterface getGrid() {
    return src_.getGrid();
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return src_.isElementVar(_idxVar);
  }

}
