/*
 *  @file         GVSourceModifier.java
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer;

import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.impl.EfGridSourceMutable;

/**
 * @author deniger
 * @version $Id: MvActModify.java,v 1.4 2007-01-19 13:14:17 deniger Exp $
 */
public interface MvActModify extends MvAct {

  boolean modify(EfGridSource _src, EfGridSourceMutable _currentGrid);

}
