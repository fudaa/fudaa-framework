package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;

/**
 * @author Fred Deniger
 * @version $Id: MvExportPanelFilterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public class MvExportPanelFilterDefault extends BuPanel implements MvExportPanelFilter {

  /**
   * @author fred deniger
   * @version $Id: MvExportPanelFilterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
   */
  static final class FilterItemObserver implements Observer {
    /**
     * 
     */
    private final MvExportOperationItem filter_;
    /**
     * 
     */
    private final BuRadioButton bi_;

    /**
     * @param _filter
     * @param _bi
     */
    FilterItemObserver(final MvExportOperationItem _filter, final BuRadioButton _bi) {
      filter_ = _filter;
      bi_ = _bi;
    }

    @Override
    public void update(final Observable _o, final Object _arg) {
      bi_.setEnabled(filter_.isEnable());
    }
  }

  /**
   * @author fred deniger
   * @version $Id: MvExportPanelFilterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
   */
  static final class FilterItemListener implements ItemListener {
    /**
     * 
     */
    private final MvExportOperationItem filter_;
    /**
     * 
     */
    private final BuRadioButton bi_;

    /**
     * @param _filter
     * @param _bi
     */
    FilterItemListener(final MvExportOperationItem _filter, final BuRadioButton _bi) {
      filter_ = _filter;
      bi_ = _bi;
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      filter_.getConfigureComponent().setEnabled(bi_.isSelected());
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportPanelFilterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
   */
  public static class ExportFilterItemSelection extends MvExportOperationSelectionItem {

    final boolean isElt_;

    MvExportOperationBuilderSelection save_;

    final int[] selectedIdx_;

    /**
     * @param _elt
     * @param _idx
     */
    public ExportFilterItemSelection(final MvSelectionNodeOrEltData _selection) {
      super();
      if (_selection == null) {
        isElt_ = false;
        selectedIdx_ = null;
      } else {
        isElt_ = _selection.isElement_;
        selectedIdx_ = _selection.idxSelected_;
      }
    }

    @Override
    public MvExportOperationBuilderSelection getBuilder(final EfGridData _src) {
      if (save_ == null) {
        if (isElt_) {
          save_ = new MvExportFilterBuilderSelectedElt(new CtuluListSelection(selectedIdx_), _src);
        } else {
          save_ = new MvExportFilterBuilderSelectedPoint(new CtuluListSelection(selectedIdx_), _src);
        }
      }
      return save_;
    }

    @Override
    public JComponent getConfigureComponent() {
      return null;
    }

    @Override
    public String getTitle() {
      return MvResource.getS("Exporter uniquement les objets sélectionnés");
    }

    @Override
    public boolean isEnable() {
      return selectedIdx_ != null;
    }

    @Override
    public String getErrorMessage() {
      return null;
    }

  }

  /**
   * @author Fred Deniger
   * @version $Id: MvExportPanelFilterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
   */
  public static class ExportFilterItemNone extends MvExportOperationItem {

    @Override
    public MvExportOperationBuilderInterface getBuilder(final EfGridData _src) {
      return null;
    }

    @Override
    public JComponent getConfigureComponent() {
      return null;
    }

    @Override
    public String getTitle() {
      return MvResource.getS("Tout exporter");
    }

    @Override
    public boolean isEnable() {
      return true;
    }

    @Override
    public String getErrorMessage() {
      return null;
    }

  }

  BuComboBox cb;
  JComponent currentComp;

  // BuRadioButton[] bt_;

  // final List<MvExportPanelFilterDefault.ExportFilterItem> filters_;
  Map<MvExportOperationItem, JComponent> cachedComp;

  /**
   * @param _filters les opération a ajouter
   */
  public MvExportPanelFilterDefault(final MvExportOperationItem[] _filters) {
    List<MvExportOperationItem> filters = new ArrayList<MvExportOperationItem>();
    filters.add(new ExportFilterItemNone());
    if (_filters != null) filters.addAll(Arrays.asList(_filters));
    setLayout(new BuBorderLayout());
    cb = new BuComboBox(filters.toArray());
    cb.setRenderer(new DefaultListCellRenderer() {

      @Override
      public java.awt.Component getListCellRendererComponent(javax.swing.JList list, Object value, int index,
          boolean isSelected, boolean cellHasFocus) {
        Component res = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        setText(((MvExportOperationItem) value).getTitle());
        return res;
      };
    });
    cb.setSelectedIndex(0);
    cb.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          operationChanged();
        }
      }
    });
    add(cb, BorderLayout.NORTH);

  }

  protected void operationChanged() {
    if (currentComp != null) {
      remove(currentComp);
    }

    MvExportOperationItem selectedOperationBuilder = getSelectedOperationBuilder();
    if (cachedComp == null) {
      cachedComp = new HashMap<MvExportOperationItem, JComponent>();
    }
    currentComp = cachedComp.get(cb.getSelectedIndex());
    if (currentComp == null) {
      currentComp = selectedOperationBuilder.getConfigureComponent();
      cachedComp.put(selectedOperationBuilder, currentComp);
    }
    if (currentComp != null) add(currentComp, BorderLayout.CENTER);
    Window parent = (Window) SwingUtilities.getAncestorOfClass(Window.class, this);
    if (parent != null) {
      parent.pack();
    }

  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public String getErrorMessage() {
    return getSelectedOperationBuilder().getErrorMessage();
  }

  @Override
  public MvExportOperationBuilderInterface getOperation(final EfGridData _src) {
    return (getSelectedOperationBuilder()).getBuilder(_src);
  }

  @Override
  public void close() {
    ComboBoxModel model = cb.getModel();
    int nb = model.getSize();
    for (int i = 0; i < nb; i++) {
      ((MvExportOperationItem) model.getElementAt(i)).close();
    }

  }

  private MvExportOperationItem getSelectedOperationBuilder() {
    return (MvExportOperationItem) cb.getSelectedItem();
  }

}