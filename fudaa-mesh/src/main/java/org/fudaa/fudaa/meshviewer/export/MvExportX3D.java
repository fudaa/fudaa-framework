/*
 *  @creation     5 sept. 2005
 *  @modification $Date: 2007-06-05 09:01:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuLog;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Arrays;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;

/**
 * Export au format X3D.
 * 
 * @author Fred Deniger
 * @version $Id: MvExportX3D.java,v 1.11 2007-06-05 09:01:12 deniger Exp $
 */
public class MvExportX3D {

  final EfGridInterface grid_;
  EfData bathy_;
  EfData bathy2_;
  EfData h_;

  CtuluListSelectionInterface selectedFaces_;

  public final EfData getBathy() {
    return bathy_;
  }

  public final void setBathy(final EfData _bathy) {
    bathy_ = _bathy;
  }

  public final EfData getBathy2() {
    return bathy2_;
  }

  public final void setBathy2(final EfData _bathy2) {
    bathy2_ = _bathy2;
  }

  public final EfData getH() {
    return h_;
  }

  public final void setH(final EfData _h) {
    h_ = _h;
  }

  public final CtuluListSelectionInterface getSelectedFaces() {
    return selectedFaces_;
  }

  public final void setSelectedFaces(final CtuluListSelectionInterface _selectedFaces) {
    selectedFaces_ = _selectedFaces;
  }

  public static FudaaPanelTaskModel createX3dTaskModel(final MvExportFactory _fact, final File _initFile,
      final MvSelectionNodeOrEltData _selection) {
    final String ext = "x3d";
    final MvExportActX3D ser = new MvExportActX3D();
    final MvExportTaskSkeleton skeleton = new MvExportTaskSkeleton(_fact.getDatas(), _fact.vects_, ser);
    final MvExportTaskSkeleton.ExportPanelFileChooserSingle file = new MvExportTaskSkeleton.ExportPanelFileChooserSingle(
        CtuluLib.getS("Fichier:"), _initFile, ext, new FileFilter[] { new BuFileFilter(ext) });
    file.setBorder(BuBorders.EMPTY2222);
    skeleton.setPnFile(file);
    final MvExportTaskSkeleton.ExportPanelVarTime selection = MvExportActX3D.createChooser(_fact.getDatas(), _fact
        .getTime());
    skeleton.setPnVar(selection);
    skeleton.setPnFilter(_fact.buildFilter( _selection, selection.getTimeSelectionModel()));
    return skeleton;

  }

  /**
   * @param _grid
   */
  public MvExportX3D(final EfGridInterface _grid) {
    super();
    grid_ = _grid;
  }

  public void doExport(final File _dest, final ProgressionInterface _prog) throws IOException {
    BufferedWriter writer = null;
    try {
      writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(_dest), "UTF8"));
      writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
      writer.newLine();
      writer.write("<X3D profile=\"Immersive\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema-instance\" "
          + "xsd:noNamespaceSchemaLocation=\"http://www.web3d.org/specifications/x3d-3.0.xsd\">");
      writer.newLine();
      writer.write(" <head>");
      writer.newLine();
      writer.write("      <meta content=\"" + _dest.getName() + "\" name=\"filename\"/>");
      writer.newLine();
      writer.write("      <meta content=\"Fudaa\" name=\"generator\"/>");
      writer.newLine();
      writer.write(" </head>");
      writer.newLine();
      writer.write("<Scene>");
      writer.newLine();
      final double dx = grid_.getMaxX() - grid_.getMinX();
      final double dy = grid_.getMaxY() - grid_.getMinY();
      final double dz = grid_.getMaxZ() - grid_.getMinZ();
      int speed = (int) (dx / 10);
      if ((dy / 10) > speed) {
        speed = (int) (dy / 10);
      }
      // la scene
      writer.write("      <NavigationInfo type='\"FLY\"  \"WALK\" \"EXAMINE\" \"ANY\"' speed='" + speed + getEndXml());

      writer.write("<DirectionalLight direction='0.2 -1 0.2' on='true'/>");
      final double middleX = (grid_.getMaxX() + grid_.getMinX()) / 2;
      double x = middleX;
      // double y = grid_.getMaxY();
      final CtuluRange range = new CtuluRange();
      if (h_ != null) {
        h_.expandTo(range);
      }
      if (bathy2_ != null) {
        bathy2_.expandTo(range);
      }
      if (bathy_ != null) {
        bathy_.expandTo(range);
      }
      final double maxZ = range.max_;
      final double minZ = range.min_;

      final DecimalFormat fmt = CtuluLib.getDecimalFormat();
      fmt.setMaximumFractionDigits(3);

      final double angleDY = Math.toRadians(10);
      final double angleView = Math.toRadians(100);
      final double k1 = Math.tan(angleDY);
      final double k2 = Math.tan(angleView - angleDY);
      final double ecartMax = dy * 1.1;
      double h = ecartMax / (k1 + k2);
      final double angleFromV = angleView - angleDY - angleView / 2;
      final double distanceForX = Math.abs(Math.cos(angleFromV) * 0.5 * dx * 1.1 / Math.tan(angleView / 2));
      if (distanceForX > h) {
        h = distanceForX;
      }
      final double d1 = Math.abs(h * k1);
      // final double alpha = Math.PI / 2 - angleView - angleDY;
      double z = -grid_.getMinY() - d1;
      double y = maxZ + h;
      writer.newLine();
      final double angleRot = Math.PI / 2 - angleFromV;
      writer.write("      <Viewpoint description=\"middle\" position=\"" + fmt.format(x) + CtuluLibString.ESPACE
          + fmt.format(y) + CtuluLibString.ESPACE + fmt.format(z) + "\" orientation='1 0 0 -" + fmt.format(angleRot)
          + "' fieldOfView='" + fmt.format(angleView) + "' centerOfRotation='" + fmt.format(middleX)
          + CtuluLibString.ESPACE + fmt.format((maxZ + minZ) / 2) + CtuluLibString.ESPACE
          + fmt.format((grid_.getMaxY() + grid_.getMinY()) / 2) + getEndXml());
      writer.newLine();
      x = grid_.getMinX();
      y = minZ + dz * 0.1;
      z = -(grid_.getMaxY() + grid_.getMinY()) / 2;
      writer.write("      <Viewpoint description=\"left\" position=\"" + fmt.format(x) + CtuluLibString.ESPACE
          + fmt.format(y) + CtuluLibString.ESPACE + fmt.format(z) + "\" orientation='0 1 0 -1.5708' fieldOfView='"
          + fmt.format(angleView) + getEndXml());
      writer.newLine();
      z = -grid_.getMinY();
      writer.write("      <Viewpoint description=\"corner-left\" position=\"" + fmt.format(x) + CtuluLibString.ESPACE
          + fmt.format(y) + CtuluLibString.ESPACE + fmt.format(z) + "\" orientation='0 1 0 -0.75' fieldOfView='"
          + fmt.format(angleView) + getEndXml());
      writer.newLine();
      x = grid_.getMaxX();
      z = -(grid_.getMaxY() + grid_.getMinY()) / 2;
      writer.write("      <Viewpoint description=\"right\" position=\"" + fmt.format(x) + CtuluLibString.ESPACE
          + fmt.format(y) + CtuluLibString.ESPACE + fmt.format(z) + "\" orientation='0 1 0 1.5708' fieldOfView='"
          + fmt.format(angleView) + getEndXml());
      writer.newLine();
      z = -grid_.getMinY();
      writer.write("      <Viewpoint description=\"corner-right\" position=\"" + fmt.format(x) + CtuluLibString.ESPACE
          + fmt.format(y) + CtuluLibString.ESPACE + fmt.format(z) + "\" orientation='0 1 0 0.75' fieldOfView='"
          + fmt.format(angleView) + getEndXml());
      writer.newLine();
      writer.write("<Transform DEF='Column' rotation='1 0 0 -1.5708'>");

      writer.write("<Group>");
      writer.newLine();
      if (h_ != null) {
        final CtuluListSelection selection = new CtuluListSelection(grid_.getEltNb());
        for (int i = grid_.getEltNb() - 1; i >= 0; i--) {
          if (selectedFaces_ == null || selectedFaces_.isSelected(i)) {
            boolean add = false;
            final EfElement elt = grid_.getElement(i);
            for (int k = elt.getPtNb() - 1; k >= 0; k--) {
              final int pt = elt.getPtIndex(k);
              if (h_.getValue(pt) > bathy_.getValue(pt)) {
                add = true;
                break;
              }
            }
            if (add) {
              selection.add(i);
            }
          }
        }
        if (!selection.isEmpty()) {
          writer.write("<Shape><Appearance><Material diffuseColor='0.3 0.3 1'/></Appearance>");
          writeaceShapes(writer, h_, selection, false);
          writer.write(getEndShape());

        }
      }
      writer.write("<Shape><Appearance><Material diffuseColor='0.51 0.5 0.4'/></Appearance>");
      writeaceShapes(writer, bathy_, selectedFaces_, true);
      writer.write(getEndShape());
      writeAxes(writer, grid_.getMinX() - 0.5, grid_.getMaxX() + 0.5, grid_.getMinY() - 0.5, grid_.getMaxY() + 0.5,
          minZ - 0.5, maxZ + 0.5);
      writer.write("</Group></Transform>");
      writer.newLine();
      writer.write("</Scene></X3D>");

    } catch (final UnsupportedEncodingException _e) {
      FuLog.warning(_e);
    } catch (final FileNotFoundException _e) {
      throw _e;
    } finally {
      if (writer != null) {
        writer.close();
      }
    }

  }

  private String getEndXml() {
    return "'/>";
  }

  private String getEndShape() {
    return "</Shape>";
  }

  private void writeCoordinate(final BufferedWriter _out, final EfData _data) throws IOException {
    _out.write("<Coordinate point=\"");
    final int nbPt = grid_.getPtsNb();
    for (int i = 0; i < nbPt; i++) {
      if (i % 100 == 1) {
        _out.newLine();
      }
      if (i > 0) {
        _out.write(CtuluLibString.VIR);
      }
      _out.write(grid_.getPtX(i) + CtuluLibString.ESPACE + grid_.getPtY(i) + CtuluLibString.ESPACE + _data.getValue(i));

    }
    _out.write("\"/>");
  }

  private void writeaceShapes(final BufferedWriter _writer, final EfData _data,
      final CtuluListSelectionInterface _selected, final boolean _solid) throws IOException {
    _writer.write("<IndexedFaceSet solid=\"" + Boolean.toString(_solid) + "\" ccw=\"true\"");
    _writer.newLine();
    _writer.write(" coordIndex=\"");
    _writer.newLine();
    // les index
    boolean isFirstElement = true;
    final int nbElt = grid_.getEltNb();
    int nbWrite = 0;
    for (int i = 0; i < nbElt; i++) {
      if (_selected == null || _selected.isSelected(i)) {
        if (nbWrite > 100) {
          _writer.newLine();
          nbWrite = 0;
        }
        if (!isFirstElement) {
          _writer.write(",-1");
        }
        final EfElement el = grid_.getElement(i);
        final int nbPt = el.getPtNb();
        for (int j = 0; j < nbPt; j++) {
          // c'est le tout premier piont
          if (j == 0 && isFirstElement) {
            _writer.write("" + el.getPtIndex(j));
          } else {
            _writer.write(CtuluLibString.VIR + el.getPtIndex(j));
          }
        }
        nbWrite++;
        isFirstElement = false;

      }

    }
    _writer.write("\">");
    _writer.newLine();
    writeCoordinate(_writer, _data);
    _writer.newLine();
    _writer.write("</IndexedFaceSet>");
    _writer.newLine();
  }

  private void writeLines(final BufferedWriter _out, final double[] _x, final double[] _y, final double[] _z)
      throws IOException {
    _out.newLine();
    _out.write("<IndexedLineSet colorPerVertex='false' coordIndex='");
    for (int i = 0; i < _x.length; i++) {
      _out.write(CtuluLibString.ESPACE + i);
      if (i % 2 == 1) {
        _out.write(" -1,");
      }
    }
    _out.write("        '>");
    _out.newLine();
    _out.write("<Coordinate DEF='EndPoints' point=' ");
    for (int i = 0; i < _x.length; i++) {
      _out.write(CtuluLibString.ESPACE + _x[i]);
      _out.write(CtuluLibString.ESPACE + _y[i]);
      _out.write(CtuluLibString.ESPACE + _z[i]);
    }
    _out.write(getEndXml());
    _out.newLine();
    _out.write("<Color color='0.4 0.4 0.4'/>");
    _out.write("</IndexedLineSet>");
    _out.newLine();
  }

  private void writeAxes(final BufferedWriter _out, final double _minX, final double _maxX, final double _minY,
      final double _maxY, final double _minZ, final double _maxZ) throws IOException {
    _out.write("<Group><Text DEF='CenterText' string='origin'>");
    _out.newLine();
    _out.write("<FontStyle DEF='LABEL_FONT' family='SANS' justify='\"MIDDLE\" \"MIDDLE\"' size='.4'/>");
    _out.newLine();
    _out.write("</Text><Shape DEF='LinesForX'>");
    int nb = 10;
    double[] x = new double[2 * (nb + 1)];
    double[] y = new double[x.length];
    double[] z = new double[x.length];
    Arrays.fill(z, _minZ);
    final double dx = (_maxX - _minX) / nb;
    for (int i = 0; i < y.length; i++) {
      if (i % 2 == 0) {
        y[i] = _minY;
        x[i] = (i / 2D) * dx + _minX;
      } else {
        y[i] = _maxY;
        x[i] = ((i - 1) / 2D) * dx + _minX;
      }
    }
    x[0] = _minX;
    x[1] = _minX;
    x[x.length - 1] = _maxX;
    x[x.length - 2] = _maxX;
    writeLines(_out, x, y, z);
    _out.write(getEndShape());
    _out.write("<Shape DEF='LinesForY'>");
    final double dy = (_maxY - _minY) / nb;
    for (int i = 0; i < y.length; i++) {
      if (i % 2 == 0) {
        x[i] = _minX;
        y[i] = (i / 2D) * dy + _minY;
      } else {
        x[i] = _maxX;
        y[i] = ((i - 1) / 2D) * dy + _minY;
      }
    }
    y[0] = _minY;
    y[1] = _minY;
    y[x.length - 1] = _maxY;
    y[x.length - 2] = _maxY;
    writeLines(_out, x, y, z);
    _out.write(getEndShape());
    _out.write("<Shape DEF='LinesForZ'>");
    nb = 2;
    x = new double[2 * (nb + 1)];
    y = new double[x.length];
    z = new double[x.length];
    final double dz = (_maxZ - _minZ) / nb;
    Arrays.fill(y, _maxY);
    for (int i = 0; i < y.length; i++) {
      if (i % 2 == 0) {
        x[i] = _minX;
        z[i] = (i / 2D) * dz + _minZ;
      } else {
        x[i] = _maxX;
        z[i] = ((i - 1) / 2D) * dz + _minZ;
      }
    }
    z[0] = _minZ;
    z[1] = _minZ;
    z[x.length - 1] = _maxZ;
    z[x.length - 2] = _maxZ;
    writeLines(_out, x, y, z);
    _out.newLine();
    _out.write(getEndShape());
    _out.write("</Group>");
  }

}
