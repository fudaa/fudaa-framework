/*
 * @file MvVisuPanel.java
 *
 * @creation 6 f�vr. 2004
 *
 * @modification $Date: 2008-01-17 11:42:49 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.layer;

import com.memoire.bu.BuDynamicMenu;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPopupMenu;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueSondeInteraction;
import org.fudaa.ebli.calque.ZCalqueSondeInterface;
import org.fudaa.ebli.calque.action.SceneRotationAction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionGroup;
import org.fudaa.ebli.controle.BSelecteurReduitFonteNewVersion;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.BConfigureTraceBox;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.meshviewer.model.*;
import org.fudaa.fudaa.sig.FSigLib;
import org.fudaa.fudaa.sig.layer.*;
import org.locationtech.jts.geom.LineString;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.List;

/**
 * Le panneau de visualisation 2D g�rant les calques. Cette classe g�re les groupes de calques SIG (par h�ritage. Elle
 * ajoute la gestion des calques de maillage (noeuds, element, etc.) et des services associ�s.
 *
 * @author deniger
 * @version $Id: MvVisuPanel.java,v 1.5.6.1 2008-01-17 11:42:49 bmarchan Exp $
 */
public class MvVisuPanel extends FSigVisuPanel implements MvIsoPaintersSource {
  private static class DisplayNumberMenu extends BuDynamicMenu {
    JMenuItem addEdge_;
    JMenuItem frontierEdge_;
    JMenuItem allEdge_;
    JMenuItem clearEdge_;
    final MvNumberLayerI support_;
    final MvVisuPanel pn_;
    final String lb_;
    final boolean displayFrontier_;

    public DisplayNumberMenu(final MvVisuPanel _pn, final String _title, final MvNumberLayerI _layer, final String _addLabel) {
      this(_pn, _title, _layer, _addLabel, false);
    }

    public DisplayNumberMenu(final MvVisuPanel _pn, final String _title, final MvNumberLayerI _layer, final String _addLabel,
                             final boolean _displayFrontier) {
      super(_title, _title.toUpperCase());
      support_ = _layer;
      lb_ = _addLabel;
      pn_ = _pn;
      displayFrontier_ = _displayFrontier;
    }

    @Override
    protected final boolean isActive() {
      return true;
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      final String com = _evt.getActionCommand();
      if ("DISPLAY_INDEX_FONT".equals(com)) {
        pn_.fontChange(support_.getTitle(), (JComponent) support_);
      } else if ("DISPLAY_INDEX_COLOR".equals(com)) {
        pn_.colorChange(support_.getTitle(), (JComponent) support_);
      } else if ("DISPLAY_INDEX_SELECTED".equals(com)) {
        support_.setItem(support_.getSelectedIdx((MvLayerGrid) pn_.getCalqueActif()));
        ((MvNodeNumberLayer) support_).setValueIdxShown(false);
      } else if ("DISPLAY_INDEX_ALL".equals(com)) {
        support_.setAll();
      } else if ("DISPLAY_INDEX_FRONTIER".equals(com)) {
        if (support_ instanceof TrNumberFrontierLayerI) {
          ((TrNumberFrontierLayerI) support_).setFrontiersOnly();
        }
      } else if ("DISPLAY_NONE_INDEX".equals(com)) {
        support_.clearIdxShow();
      } else {
        super.actionPerformed(_evt);
      }
    }

    protected void addSpecificItem() {

    }

    @Override
    protected void build() {
      if (addEdge_ == null) {
        addEdge_ = addMenuItem(lb_, "DISPLAY_INDEX_SELECTED");
        if (displayFrontier_) {
          frontierEdge_ = addMenuItem(MvResource.getS("Afficher les num�ros aux fronti�res"), "DISPLAY_INDEX_FRONTIER");
        }
        allEdge_ = addMenuItem(MvResource.getS("Afficher tous les num�ros"), "DISPLAY_INDEX_ALL");
        clearEdge_ = addMenuItem(MvResource.getS("Ne plus afficher les num�ros"), "DISPLAY_NONE_INDEX");
        addSpecificItem();
        addSeparator();
        addMenuItem(MvResource.getS("Modifier la fonte"), "DISPLAY_INDEX_FONT");
        addMenuItem(MvResource.getS("Modifier la couleur"), "DISPLAY_INDEX_COLOR");
      }
      updateState();
    }

    protected void updateState() {
      final boolean isAll = support_ != null && support_.isAll();
      allEdge_.setEnabled(!isAll);
      clearEdge_.setEnabled(support_.isSomethingToView());
      addEdge_.setEnabled((pn_.getCalqueActif() instanceof MvLayerGrid) && support_.isSelectionOk((MvLayerGrid) pn_.getCalqueActif()));
    }
  }

  private static class DisplayNodeNumberMenu extends DisplayNumberMenu {
    JCheckBoxMenuItem boundaryIdxPt_;

    /**
     * Numero des points.
     */
    public DisplayNodeNumberMenu(final MvVisuPanel _pn, final MvNumberLayerI _layerNode) {
      super(_pn, MvResource.getS("Indices des noeuds"), _layerNode, MvResource.getS("Afficher les num�ros des noeuds s�lectionn�s"));
      setIcon(MvResource.MV.getIcon("numnoeuds"));
    }

    @Override
    protected void addSpecificItem() {
      boundaryIdxPt_ = addCheckBox(MvResource.getS("Afficher les num�ros sur la fronti�re"), "DISPLAY_BOUNDARY_INDEX_POINT", true, false);
    }

    @Override
    public void actionPerformed(final ActionEvent _arg) {
      if ("DISPLAY_BOUNDARY_INDEX_POINT".equals(_arg.getActionCommand())) {
        ((MvNodeNumberLayer) support_).changeFrontierIdxShownState();
      } else {
        super.actionPerformed(_arg);
      }
    }
  }

  /**
   * display value of nodes.
   *
   * @author Adrien Hadoux
   */
  private static class DisplayNodeValueMenu extends BuDynamicMenu {
    JCheckBoxMenuItem boundaryIdxPt_;
    JMenuItem addEdge_;
    JMenuItem frontierEdge_;
    JMenuItem allEdge_;
    JMenuItem clearEdge_;
    BConfigureTraceBox configTraceBox = new BConfigureTraceBox();
    final MvNumberLayerI support_;
    final MvVisuPanel pn_;
    final String lb_;
    final boolean displayFrontier_;

    @Override
    protected final boolean isActive() {
      return true;
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      final String com = _evt.getActionCommand();
      if ("CONFIG_TRACEBOX".equals(com)) {
        if (pn_.getCalqueActif() instanceof FSigLayerVariableDisplayAble) {
          FSigLayerVariableDisplayAble layer = ((FSigLayerVariableDisplayAble) pn_.getCalqueActif());
          configTraceBox.setTarget(layer.getDisplayer().getTraceBox(), pn_.getCalqueActif());
        } else if (support_ instanceof MvNodeNumberLayer) {
          configTraceBox.setTarget(((MvNodeNumberLayer) support_).getTraceBox(), (JComponent) support_);
        }

        configTraceBox.buildUi();
      } else if ("DISPLAY_INDEX_SELECTED".equals(com)) {
        if ((pn_.getCalqueActif() instanceof FSigLayerVariableDisplayAble)) {
          FSigLayerVariableDisplayAble layer = (FSigLayerVariableDisplayAble) pn_.getCalqueActif();
          layer.showValues(true);
        } else {
          support_.setItem(support_.getSelectedIdx((MvLayerGrid) pn_.getCalqueActif()));
        }
        ((MvNodeNumberLayer) support_).setValueIdxShown(true);
      } else if ("DISPLAY_INDEX_ALL".equals(com)) {
        //support_.setAll();
      } else if ("DISPLAY_INDEX_FRONTIER".equals(com)) {
        if (support_ instanceof TrNumberFrontierLayerI) {
          //((TrNumberFrontierLayerI) support_).setFrontiersOnly();
        }
      } else if ("DISPLAY_NONE_INDEX".equals(com)) {
        if ((pn_.getCalqueActif() instanceof FSigLayerVariableDisplayAble)) {
          FSigLayerVariableDisplayAble layer = (FSigLayerVariableDisplayAble) pn_.getCalqueActif();
          layer.showValues(false);
        } else {
          support_.clearIdxShow();
        }
      } else if ("DISPLAY_VALUE_INDEX_POINT".equals(_evt.getActionCommand())) {
        ((MvNodeNumberLayer) support_).changeFrontierIdxValueShownState();
      } else {
        super.actionPerformed(_evt);
      }
    }

    /**
     * valeur des points.
     */
    public DisplayNodeValueMenu(final MvVisuPanel _pn, final MvNumberLayerI _layerNode) {
      super(MvResource.getS("Valeurs aux noeuds"), MvResource.getS("valeurs aux noeuds").toUpperCase());
      support_ = _layerNode;
      lb_ = MvResource.getS("Afficher les valeurs des noeuds s�lectionn�s");
      pn_ = _pn;
      displayFrontier_ = false;
      setIcon(MvResource.MV.getIcon("numnoeuds"));
    }

    protected void addSpecificItem() {
      // boundaryIdxPt_ = addCheckBox(MvResource.getS("Afficher les valeurs des noeuds"), "DISPLAY_VALUE_INDEX_POINT", true, false);
    }

    @Override
    protected void build() {
      if (addEdge_ == null) {
        addEdge_ = addMenuItem(lb_, "DISPLAY_INDEX_SELECTED");
//        if (displayFrontier_) {
//          frontierEdge_ = addMenuItem(MvResource.getS("Afficher les num�ros aux fronti�res"), "DISPLAY_INDEX_FRONTIER");
//        }
//        allEdge_ = addMenuItem(MvResource.getS("Afficher tous les num�ros"), "DISPLAY_INDEX_ALL");
        clearEdge_ = addMenuItem(MvResource.getS("Ne plus afficher les valeurs"), "DISPLAY_NONE_INDEX");
        // addSpecificItem();
        addSeparator();
        addMenuItem(MvResource.getS("Configurer le rendu"), "CONFIG_TRACEBOX");
//        addMenuItem(MvResource.getS("Modifier la couleur"), "DISPLAY_INDEX_COLOR");
//     
//       addMenuItem(MvResource.getS("Modifier la couleur de fond"), "DISPLAY_INDEX_COLOR_BACKGROUND");
//        addMenuItem(MvResource.getS("Modifier la couleur du cadre"), "DISPLAY_INDEX_COLOR_BORDER");
//        addMenuItem(MvResource.getS("Modifier la position (verticale/horizontale)"), "DISPLAY_INDEX_HPOSITION");

      }
      updateState();
    }

    protected void updateState() {
      final boolean isAll = support_ != null && support_.isAll();
      //allEdge_.setEnabled(!isAll);
      if ((pn_.getCalqueActif() instanceof FSigLayerVariableDisplayAble)) {
        FSigLayerVariableDisplayAble layer = (FSigLayerVariableDisplayAble) pn_.getCalqueActif();
        clearEdge_.setEnabled(layer.isShowValues());
        addEdge_.setEnabled(!layer.isShowValues());
      } else {
        clearEdge_.setEnabled(support_.isSomethingToView());
        addEdge_.setEnabled((pn_.getCalqueActif() instanceof MvLayerGrid) && support_.isSelectionOk((MvLayerGrid) pn_.getCalqueActif()));
      }
    }
  }

  @Override
  public void getIsoPainters(final Collection _setToFill) {
  }

  private BuMenu ptDisplayTools_;
  private BuMenu ptValueDisplayTools_;
  private BuMenu eltDisplayTools_;
  private BuMenu edgeDisplayTools_;

  /**
   * @param _controller l'implementation parente
   */
  public MvVisuPanel(final FSigVisuPanelController _controller) {
    super(null, _controller);
    buildGroupGIS();
    getScene().setRestrictedToCalqueActif(true);
  }

  /**
   * @param _impl l'implementation parente
   */
  public MvVisuPanel(final FudaaCommonImplementation _impl) {
    this(new FSigVisuPanelController(_impl));
    getScene().setRestrictedToCalqueActif(true);
  }

  /**
   * @param _impl l'implementation parente
   */
  public MvVisuPanel(final CtuluUI _impl) {
    this(new FSigVisuPanelController(_impl));
    getScene().setRestrictedToCalqueActif(true);
  }

  public FudaaCommonImplementation getImpl() {
    return (FudaaCommonImplementation) super.getController().getUI();
  }

  @Override
  public void fillMenuWithToolsActions(final JMenu _m) {
    super.fillMenuWithToolsActions(_m);
    _m.addSeparator();
    _m.add(getPointNumberDisplayMenu());
    _m.add(getElementNumberDisplayMenu());
    if (getCqEdgeNumber() != null) {
      _m.add(getEdgeNumberDisplayMenu());
    }
  }

  /**
   * Ajoute le groupe de calque maillage.
   *
   * @param _m le maillage
   * @param _d le delegue pour la mise a jour des infos
   */
  public void addCqMaillage(final EfGridInterface _m, final MvInfoDelegate _d) {
    final MvNodeModelDefault pt = new MvNodeModelDefault(_m);
    pt.setDelegate(_d);
    final MvElementModelDefault elt = new MvElementModelDefault(_m);
    elt.setDelegate(_d);
    addCqMaillage(_m, pt, elt);
  }

  @Override
  protected FSigEditor createGisEditor() {
    FSigEditor fSigEditor = new FSigEditor(this) {
      @Override
      public GISPolygone[] getEnglobPolygone() {
        final EfFrontierInterface frontiers = MvVisuPanel.this.getGridGroup().getGrid().getFrontiers();
        if (frontiers == null) {
          return super.getEnglobPolygone();
        }
        return frontiers.getExternRing(MvVisuPanel.this.getGridGroup().getGrid());
      }
    };
    mng_ = fSigEditor.getMng();
    return fSigEditor;
  }

  /**
   * @param _m le maillage
   * @param _ptModel le modele a utiliser pour les points
   * @param _eltModel le modele a utiliser pour les elements
   */
  protected void addCqMaillage(final EfGridInterface _m, final MvNodeModel _ptModel, final MvElementModel _eltModel) {
    if (getGridGroup() != null) {
      return;
    }
    addCqGroupeMaillage(new MvGridLayerGroup(_m, _ptModel, _eltModel));
  }

  protected final void addCqGroupeMaillage(final MvGridLayerGroup _g) {
    _g.setName("gpGrid");
    addCalque(_g);
  }

  /**
   * Action appelee pour editer un point du maillage.
   */
  public String editGridPoint() {
    return null;
  }

  /**
   * Action appelee pour editer un polygone.
   */
  public String editGridPoly() {
    return null;
  }

  /**
   * Doit renvoyer true si les points du maillage sont editables.
   *
   * @return false par defaut
   */
  public boolean isGridPointEditable() {
    return false;
  }

  @Override
  protected SceneRotationAction createRotationAction() {
    return new SceneRotationAction(getArbreCalqueModel().getTreeSelectionModel(), gisEditor_, gisEditor_.getSceneEditor(), getEbliFormatter()) {
      @Override
      protected boolean isTargetValid(Object _o) {
        boolean res = super.isTargetValid(_o);
        if (res && _o instanceof MvNodeLayer) {
          res = false;
        }
        return res;
      }
    };
  }

  /**
   * Doit renvoyer true si les elements sont editables.
   *
   * @return false par defaut
   */
  public boolean isGridElementEditable() {
    return false;
  }

  /**
   * Ajoute les calques d'informations : numero des points et des elements.
   *
   * @param _g le maillage associe
   */
  public final void addCqInfos(final EfGridInterface _g) {
    if (getCqInfos() != null) {
      if (getCqNodeNumber() != null && getCqElementNumber() != null) {
        if (_g instanceof EfGridVolumeInterface) {
          if (getCqEdgeNumber() != null) {
            return;
          }
        } else {
          return;
        }
      }
    }
    addCqInfos();
    final BGroupeCalque gr = getCqInfos();
    addCqNumber(_g, gr);
    addCqElementNumber(_g, gr);
    if (_g instanceof EfGridVolumeInterface) {
      addCqEdgeNumber((EfGridVolumeInterface) _g, gr);
    }
    vc_.getCalque().enPremier(gr);
  }

  /**
   * @param _g le maillage associee
   * @param _c le groupe de calque de dest
   */
  public void addCqNumber(final EfGridInterface _g, final BGroupeCalque _c) {
    final MvNodeNumberLayer pt = new MvNodeNumberLayer(new MvNodeModelDefault(_g));
    pt.setName("cqPtNumber");
    _c.add(pt);
  }

  private void addCqElementNumber(final EfGridInterface _g, final BGroupeCalque _c) {
    final MvElementNumberLayer pt = new MvElementNumberLayer(new MvElementModelDefault(_g));
    pt.setName("cqEltNumber");
    _c.add(pt);
  }

  protected void addCqEdgeNumber(final EfGridVolumeInterface _g, final BGroupeCalque _c) {
    final MvEdgeNumberLayer pt = new MvEdgeNumberLayer(new MvEdgeModelDefault(_g));
    pt.setName("cqEdgeNumber");
    _c.add(pt);
  }

  /**
   * @return le calque affichant les numeros des points
   */
  public final MvNodeNumberLayer getCqNodeNumber() {
    return (MvNodeNumberLayer) getCqInfos().getCalqueParNom("cqPtNumber");
  }

  public final MvEdgeNumberLayer getCqEdgeNumber() {
    return (MvEdgeNumberLayer) getCqInfos().getCalqueParNom("cqEdgeNumber");
  }

  /**
   * @return le calque affichant les numeros des elements
   */
  public final MvElementNumberLayer getCqElementNumber() {
    return (MvElementNumberLayer) getCqInfos().getCalqueParNom("cqEltNumber");
  }

  /**
   * @return le groupe maillage
   */
  public final MvGridLayerGroup getGridGroup() {
    return (MvGridLayerGroup) getDonneesCalqueByName("gpGrid");
  }

  @Override
  protected final void fillCmdContextuelles(final BuPopupMenu _menu) {
    super.fillCmdContextuelles(_menu);
    if (ptDisplayTools_ == null) {
      ptDisplayTools_ = getPointNumberDisplayMenu();
    }
    _menu.add(ptDisplayTools_);
    if (eltDisplayTools_ == null) {
      eltDisplayTools_ = getElementNumberDisplayMenu();
    }
    _menu.add(eltDisplayTools_);
    if (getCqEdgeNumber() != null) {
      if (edgeDisplayTools_ == null) {
        edgeDisplayTools_ = getEdgeNumberDisplayMenu();
      }
      _menu.add(edgeDisplayTools_);
    }

    if (ptValueDisplayTools_ == null) {
      ptValueDisplayTools_ = getPointValueDisplayMenu();
    }
    _menu.add(ptValueDisplayTools_);
  }

  /**
   * @return le menu pour l'affichage des numero
   */
  public final BuMenu getPointNumberDisplayMenu() {
    return new DisplayNodeNumberMenu(this, getCqNodeNumber());
  }

  /**
   * @return le menu pour l'affichage des valeurs des points
   */
  public final BuMenu getPointValueDisplayMenu() {
    return new DisplayNodeValueMenu(this, getCqNodeNumber());
  }

  public MvSelectionNodeOrEltData getCurrentSelection() {
    final MvSelectionNodeOrEltData res = new MvSelectionNodeOrEltData();
    if (getCalqueActif() instanceof MvLayerGrid) {
      final MvLayerGrid mvLay = (MvLayerGrid) getCalqueActif();
      res.isElement_ = !mvLay.isSelectionElementEmpty();
      res.idxSelected_ = res.isElement_ ? mvLay.getSelectedElementIdx() : mvLay.getSelectedPtIdx();
    }
    return res;
  }

  private synchronized ZCalqueEditionGroup buildGroupGIS() {
    ZCalqueEditionGroup cq = getGroupGIS();
    if (cq == null) {
      cq = new FSigLayerGroup(this);
      cq.setName(MvVisuPanel.getGisGroupCalqueName());
      String title = FSigLib.getS("Donn�es g�og");
      if (CtuluLib.isFrenchLanguageSelected()) {
        title += CtuluLibString.DOT;
      }
      cq.setTitle(title);
      configureGISGroup(cq);
      cq.setDestructible(false);
      getDonneesCalque().enPremier(cq);
    }
    return cq;
  }

  @Override
  public final FSigLayerGroup getGroupGIS() {
    return (FSigLayerGroup) getDonneesCalqueByName(MvVisuPanel.getGisGroupCalqueName());
  }

  /**
   * @return le menu pour l'affichage des numero
   */
  public final BuMenu getElementNumberDisplayMenu() {
    final BuMenu r = new DisplayNumberMenu(this, MvResource.getS("Indices des �l�ments"), getCqElementNumber(),
        MvResource.getS("Afficher les num�ros des �l�ments s�lectionn�s"));
    r.setIcon(MvResource.MV.getToolIcon("numelements"));
    return r;
  }

  public final BuMenu getEdgeNumberDisplayMenu() {
    final BuMenu r = new DisplayNumberMenu(this, MvResource.getS("Indices des ar�tes"), getCqEdgeNumber(),
        MvResource.getS("Afficher les num�ros des ar�tes s�lectionn�s"), true);
    r.setIcon(MvResource.MV.getToolIcon("numaretes"));
    return r;
  }

  void fontChange(final String _titre, final JComponent _c) {

    final JDialog m = new JDialog(getFrame(), _titre);
    final BSelecteurReduitFonteNewVersion s = new BSelecteurReduitFonteNewVersion(_c.getFont(), m);
    s.setTarget(_c);
    m.setResizable(true);
    m.setContentPane(s.getComponent());
    m.setModal(true);
    m.pack();
    m.setVisible(true);
    m.dispose();
  }

  void colorChange(final String _titre, final JComponent _c) {
    colorChange(_titre, _c, true);
  }

  void colorChange(final String _titre, final JComponent _c, final boolean foreground) {
    final JColorChooser ch = new JColorChooser(_c.getForeground());
    final CtuluDialogPanel p = new CtuluDialogPanel(false) {
      @Override
      public boolean apply() {
        if (foreground) {
          _c.setForeground(ch.getColor());
        } else {
          _c.setBackground(ch.getColor());
        }
        return true;
      }
    };
    p.add(ch);
    p.afficheModale(this, _titre, CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }

  public void colorChangeTracebox(final TraceBox box, final boolean border) {
    final JColorChooser ch = new JColorChooser(box.getColorBoite());
    final CtuluDialogPanel p = new CtuluDialogPanel(false) {
      @Override
      public boolean apply() {
        if (border) {
          box.setColorBoite(ch.getColor());
        } else {
          box.setColorFond(ch.getColor());
        }
        return true;
      }
    };
    p.add(ch);
    p.afficheModale(this, "", CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }

  public String getTitle() {
    return getName();
  }

  /**
   * @return la ligne selectionnee dans le calque selectionne ou null si aucune
   */
  public LineString getSelectedLine() {
    LineString selection = null;
    // pas de calque s�lectionn�
    if ((getArbreCalqueModel().getSelectedCalque() instanceof ZCalqueAffichageDonneesInterface)) {
      // -- on essaie de recuperer la ligne selectionnee:--//
      selection = ((ZCalqueAffichageDonneesInterface) getArbreCalqueModel().getSelectedCalque()).getSelectedLine();
    }
    // -- si il n'y a pas de ligne selectionne dans une donne geometrique, alors on tente de renvoyer la ligne formee
    // des sondes --//
    if (selection == null) {
      selection = getSelectedLineFromSonde();
    }

    return selection;
  }

  public boolean isSelectionOkForEvolutionSonde() {
    return (getCalqueActif() instanceof ZCalqueSondeInterface) && ((ZCalqueSondeInterface) getCalqueActif()).isSondeActive();
  }

  /**
   * methode qui permet d'ajouter une sonde dans le calque donn�.
   *
   * @param pointToAdd
   */
  public void addSondeToCalque(GrPoint pointToAdd) {
    ZCalqueSondeInteraction interactionSonde = getController().addCalqueSondeInteraction();
    interactionSonde.setGele(false);
    interactionSonde.moveEvent(pointToAdd, true, false);
  }

  /**
   * Retourne la ligne brisee formee par les sondes. On peut creer plusieurs sondes en maintenant shift appuy�.
   */
  public List<GrPoint> getLigneBriseeFromSonde() {
    if (isSelectionOkForEvolutionSonde()) {
      final ZCalqueSondeInterface s = (ZCalqueSondeInterface) getCalqueActif();
      if (s != null) {
        List<GrPoint> ligneBriseeFromSondes = s.getLigneBriseeFromSondes();
        if (ligneBriseeFromSondes != null && ligneBriseeFromSondes.size() > 1) {
          return ligneBriseeFromSondes;
        }
      }
    }

    return null;
  }

  /**
   * Retourne la ligne cr�e a partir des objets sondes (en maintenant le crtl+shift)
   */
  public LineString getSelectedLineFromSonde() {
    List<GrPoint> listeSondes = getLigneBriseeFromSonde();
    if (listeSondes == null) {
      return null;
    }
    GISZoneCollectionPoint listePoints = new GISZoneCollectionPoint();
    for (GrPoint point : listeSondes) {
      listePoints.add(point.x_, point.y_, point.z_);
    }
    return new LineString(listePoints.getAttachedSequence(), GISGeometryFactory.INSTANCE);
  }

  @Override
  public void setActive(final boolean _b) {
  }

  /**
   * @return le nom utilise pour le groupe de calque SIG.
   */
  public static String getGisGroupCalqueName() {
    return "gcGIS";
  }
}
