/**
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;

/**
 * @author deniger
 * @version $Id: MvNodeModel.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
 */
public interface MvNodeModel extends ZModelePoint {

  void setGrid(EfGrid _g);

  EfGridInterface getGrid();

  EbliFindExpressionContainerInterface getExpressionContainer();

  boolean getDataRange(CtuluRange _b);

  double getZ(int _i);

}