/**
 *  @creation     5 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer;


/**
 * Une interface decrivant les calques contenant des selection en reference a des noeuds ou elements.
 * @author deniger
 * @version $Id: MvLayerGrid.java,v 1.1 2007-01-19 13:14:16 deniger Exp $
 */
public interface MvLayerGrid {

  /**
   * @return true si aucun point selectionne
   */
  boolean isSelectionPointEmpty();

  /**
   * @return les indices des points selectionnes
   */
  int[] getSelectedPtIdx();

  /**
   * @return true si aucun point selectionne
   */
  boolean isSelectionEdgeEmpty();

  /**
   * @return les indices des points selectionnes
   */
  int[] getSelectedEdgeIdx();

  /**
   * @return true si acun element selectionne
   */
  boolean isSelectionElementEmpty();

  /**
   * @return les indices des elements selectionnes
   */
  int[] getSelectedElementIdx();

}