/*
 * @creation 12 avr. 2006
 *
 * @modification $Date: 2007-05-04 14:01:54 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.impl.EfGridBathyAdapter;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.rubar.io.*;
import org.fudaa.fudaa.meshviewer.MvResource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author fred deniger
 * @version $Id: TrExportRubarInxCoxAct.java,v 1.18 2007-05-04 14:01:54 deniger Exp $
 */
public class MvExportRubarMAI extends MvExportActAbstract {
  private H2dRubarGridModifier h2dRubarGridModifier;

  static boolean isCoxEnable(final EfGridData _src) {
    return _src.isDefined(H2dVariableType.BATHYMETRIE);
  }

  static boolean isFrxEnable(final EfGridData _src) {
    return _src.isDefined(H2dVariableType.COEF_FROTTEMENT_FOND);
  }

  public static boolean isConcentrationAvailable(EfGridData data) {
    return data.isDefined(H2dVariableTransType.CONCENTRATION);
  }

  static boolean isInxEnable(final EfGridData _src) {
    final boolean debitOk = _src.isDefined(H2dVariableType.DEBIT_X) && _src.isDefined(H2dVariableType.DEBIT_Y);
    return debitOk && (_src.isDefined(H2dVariableType.COTE_EAU) || _src.isDefined(H2dVariableType.HAUTEUR_EAU));
  }

  private boolean cofDurSed = true;
  private boolean cox = true;
  private boolean frtAndDif = true;
  private boolean frx = true;
  private boolean imaGenerated = true;
  private boolean datGenerated = true;
  private boolean inx_ = true;
  private boolean inxUseH;
  private boolean maiGenerated = true;
  boolean toTriangle;
  private int nbBlockConcentration = -1;
  private H2dRubarFileType fileType = H2dRubarFileType.NEW_FORMAT_9_DIGITS;

  public MvExportRubarMAI() {
    super();
  }

  @Override
  protected void doExport(final ProgressionInterface progressionInterface, final File[] files, final CtuluAnalyzeGroup logs, final String[] message) {
    if (!isSomethingGenerated()) {

      super.addInfo(message, 0);
      message[0] = MvResource.MV.getString("Aucune donn�e n'a �t� s�lectionn�e");
      return;
    }
    initActivity();
    EfGridData last = super.processOperations(progressionInterface, logs);

    if (!isInternOk(logs, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: stop after filter");
      }
      return;
    }
    if (toTriangle) {
      final MvExportToT3Activity toT3 = new MvExportToT3Activity(last);
      setCurrentActivity(toT3);
      CtuluAnalyze toT3log = new CtuluAnalyze();
      last = toT3.process(progressionInterface, toT3log);
      if (toT3log.containsErrorOrFatalError()) {
        logs.addAnalyzer(toT3log);
      }
    }
    final File file = files[0];
    final File parent = file.getParentFile();
    if (parent == null) {
      return;
    }

    final String path = new File(parent, CtuluLibFile.getSansExtension(file.getName())).getAbsolutePath();
    //data on nodes:
    final EfGridData nodeData = filterToNodes(progressionInterface, last, new CtuluAnalyze());
    CtuluAnalyze log = new CtuluAnalyze();
    log.setDesc(MvResource.getS("Adaptation des donn�es"));
    logs.addAnalyzer(log);
    if (isCofDurSed()) {
      writeCofDurSed(progressionInterface, logs, file, nodeData);
    }
    if (isFrtAndDif()) {
      writeFrtAndDif(progressionInterface, logs, file, last);
    }
    if (isInx()) {
      exportINX(path, progressionInterface, log, last, nodeData);
    }
    if (isCox()) {
      // dans le post traitement, les donn�es sont donn�es sur les �l�ments. Or, le fichier cox doit etre d�fini sur les
      // noeuds
      // demande A.Paquier F�vrier 2007.
      final H2dRubarDonneesBrutesAdapter ad = new H2dRubarDonneesBrutesAdapter(nodeData, H2dVariableType.BATHYMETRIE, fileType);
      RubarCOXFileFormat.getFormat(isNewFormat()).write(CtuluLibFile.getFile(path, RubarCOXFileFormat.getInstance().getExtensions()[0]), ad, progressionInterface);
    }
    if (isFrx()) {
      // export Frx
      final H2dRubarDonneesBrutesAdapter ad = new H2dRubarDonneesBrutesAdapter(last, H2dVariableType.COEF_FROTTEMENT_FOND, fileType);

      RubarFRXFileFormat.getFormat(isNewFormat()).write(CtuluLibFile.getFile(path, RubarFRXFileFormat.getInstance().getExtensions()[0]), ad, progressionInterface);
    }

    if (!goOn_) {
      return;
    }

    //grids:
    if (isImaGenerated()) {
      final File ima = CtuluLibFile.changeExtension(file, "ima");
      RubarIMAFileFormat.getInstance().writeGrid(ima, new EfGridSourceDefaut(last.getGrid(), RubarIMAFileFormat.getInstance()), progressionInterface);
    }
    if (isMaiGenerated()) {
      try {
        EfGridData data = last;
        if (last.isElementVar(H2dVariableType.BATHYMETRIE)) {
          data = nodeData;
        }
        final EfGridInterface grid = new EfGridBathyAdapter(data.getData(H2dVariableType.BATHYMETRIE, 0), data.getGrid());
        RubarMAIFileFormat.getFormat(isNewFormat()).writeGrid(CtuluLibFile.getFile(path, RubarMAIFileFormat.getInstance().getExtensions()[0]),
            new EfGridSourceDefaut(grid, RubarMAIFileFormat.getInstance()), progressionInterface);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }
    if (isDatGenerated()) {
      try {
        EfGridData data = last;
        if (last.isElementVar(H2dVariableType.BATHYMETRIE)) {
          data = nodeData;
        }
        final EfGridInterface grid = new EfGridBathyAdapter(data.getData(H2dVariableType.BATHYMETRIE, 0), data.getGrid());
        final File datFile = CtuluLibFile.changeExtension(file, "dat");
        final H2dRubarGridAreteSource rubarGridAreteSource = H2dRubarGrid.createCompleteGridFrom(grid, progressionInterface, log, 3);
        if (h2dRubarGridModifier != null) {
          h2dRubarGridModifier.modify(rubarGridAreteSource, progressionInterface);
        }
        new RubarDATFileFormat(isNewFormat()).write(datFile, rubarGridAreteSource, progressionInterface);
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }
    super.addInfo(message, last.getGrid().getPtsNb(), last.getGrid().getEltNb());
  }

  /**
   * @param _path le chemin du fichier a ecrire (sans extension)
   * @param _prog la progression
   * @param _analyze analyse pour ajouter des erreurs
   * @param _last les derni�res donn�es utilis�es
   */
  private void exportINX(final String _path, final ProgressionInterface _prog, final CtuluAnalyze _analyze, final EfGridData _last,
                         final EfGridData nodeData) {

    H2dVariableType[] var = new H2dVariableType[]{
        H2dVariableType.HAUTEUR_EAU, H2dVariableType.DEBIT_X, H2dVariableType.DEBIT_Y, H2dVariableType.COTE_EAU};
    if (nbBlockConcentration > 0) {
      List<H2dVariableType> variableTypeList = new ArrayList<>();
      variableTypeList.addAll(Arrays.asList(var));
      variableTypeList.addAll(H2dRubarBcMng.getTransportVariables(nbBlockConcentration));

      var = variableTypeList.toArray(new H2dVariableType[variableTypeList.size()]);
    }
    boolean isElt = false;
    // Permet de savoir si une variable a deja ete trouvee
    boolean isStarted = false;
    boolean isAllSame = true;
    for (int i = var.length - 1; i >= 0 && isAllSame; i--) {
      if (_last.isDefined(var[i])) {
        if (isStarted) {
          isAllSame = (isElt == _last.isElementVar(var[i]));
        } else {
          isElt = _last.isElementVar(var[i]);
          isStarted = true;
        }
      }
    }
    EfGridData data = _last;
    if (!goOn_) {
      return;
    }
    if (!isAllSame) {
      data = nodeData;
    }
    if (!goOn_) {
      return;
    }
    final H2dRubarDonneesBrutesAdapter ad = new H2dRubarDonneesBrutesAdapter(data, var, isElt, fileType);
    ad.setBadValueFor(inxUseH ? 3 : 0);
    RubarINXFileFormat.getFormat(isNewFormat()).write(CtuluLibFile.getFile(_path, RubarINXFileFormat.getInstance().getExtensions()[0]), ad, _prog);
  }

  private boolean isNewFormat() {
    return fileType == H2dRubarFileType.NEW_FORMAT_9_DIGITS;
  }

  @Override
  public int getNbLabelInfoNeeded() {
    return 2;
  }

  @Override
  public String getTitre() {
    return "Rubar dat, mai, ima, cox, inx,...";
  }

  public boolean isCofDurSed() {
    return cofDurSed;
  }

  protected boolean isCox() {
    return cox;
  }

  public boolean isFrtAndDif() {
    return frtAndDif;
  }

  protected boolean isFrx() {
    return frx;
  }

  public boolean isImaGenerated() {
    return imaGenerated;
  }

  protected boolean isInx() {
    return inx_;
  }

  protected boolean isInxUseH() {
    return inxUseH;
  }

  protected boolean isMaiGenerated() {
    return maiGenerated;
  }

  public boolean isSomethingGenerated() {
    return isCofDurSed() || isCox() || isFrtAndDif() || isFrx() || isImaGenerated() || isDatGenerated() || isMaiGenerated() || isInx();
  }

  public boolean isDatGenerated() {
    return datGenerated;
  }

  public void setCofDurSedGenerated(boolean cofDurSed) {
    this.cofDurSed = cofDurSed;
  }

  protected void setCoxGenerated(final boolean _cox) {
    cox = _cox;
  }

  public void setFrtAndDifGenerated(boolean frtAndDifGenerated) {
    this.frtAndDif = frtAndDifGenerated;
  }

  protected void setFrxGenerated(final boolean _frx) {
    frx = _frx;
  }

  public void setImaGenerated(boolean imaGenerated) {
    this.imaGenerated = imaGenerated;
  }

  void setDatGenerated(boolean datGenerated) {
    this.datGenerated = datGenerated;
  }

  void setInxGenerated(final boolean _inx) {
    inx_ = _inx;
  }

  void setInxUseH(final boolean _inxUseH) {
    inxUseH = _inxUseH;
  }

  void setMaiGenerated(final boolean _mai) {
    maiGenerated = _mai;
  }

  void setFileType(H2dRubarFileType fileType) {
    this.fileType = fileType;
  }

  void setToTriangle(boolean selected) {
    this.toTriangle = selected;
  }

  public int getNbBlockConcentration() {
    return nbBlockConcentration;
  }

  void setNbBlockConcentration(int nbBlockConcentration) {
    this.nbBlockConcentration = nbBlockConcentration;
  }

  private void writeCofDurSed(final ProgressionInterface progressionInterface, final CtuluAnalyzeGroup logs, final File file, final EfGridData nodeData) {
    CtuluAnalyze cofDurSed = new CtuluAnalyze();
    try {
      final File cof = CtuluLibFile.changeExtension(file, "cof");
      final EfData data = nodeData.getData(H2dVariableType.BATHYMETRIE, 0);
      if (data != null) {
        final CtuluIOOperationSynthese operationSynthese = RubarFRTFileFormat.getInstance().write(cof, data, progressionInterface);
        if (operationSynthese != null) {
          logs.addAnalyzer(operationSynthese.getAnalyze());
        }
      } else {
        cofDurSed.addWarn(MvResource.getS("Fichier {0} non export�", "COF"));
      }
    } catch (IOException e) {
      FuLog.error(e);
    }
    try {
      final File durFile = CtuluLibFile.changeExtension(file, "dur");
      final EfData data = nodeData.getData(H2dVariableTransType.FOND_INERODABLE, 0);
      if (data != null) {
        final CtuluIOOperationSynthese operationSynthese = RubarDURFileFormat.getInstance().write(durFile, new RubarDURResult(data), progressionInterface);
        if (operationSynthese != null) {
          logs.addAnalyzer(operationSynthese.getAnalyze());
        }
      } else {
        cofDurSed.addWarn(MvResource.getS("Fichier {0} non export�", "DUR"));
      }
    } catch (IOException e) {
      FuLog.error(e);
    }
    final List<H2dRubarSedimentVariableType> variables = H2dRubarSedimentMng.getVariables();
    final H2dVariableType first = variables.get(0);
    final H2dVariableType last = variables.get(variables.size() - 1);
    try {
      if (nodeData.getData(first, 0) != null && nodeData.getData(last, 0) != null) {
        final File sedFile = CtuluLibFile.changeExtension(file, "sed");
        final CtuluIOOperationSynthese operationSynthese = new RubarSEDFileFormat().write(sedFile, new MvSedDataAdapter(nodeData), progressionInterface);
        if (operationSynthese != null) {
          logs.addAnalyzer(operationSynthese.getAnalyze());
        }
      } else {
        cofDurSed.addWarn(MvResource.getS("Fichier {0} non export�", "SED"));
      }
    } catch (IOException e) {
      FuLog.error(e);
    }
    if (cofDurSed.isNotEmpty()) {
      cofDurSed.setDesc(MvResource.getS("G�n�rer les fichiers COF, DUR et SED"));
      logs.addAnalyzer(cofDurSed);
    }
  }

  private void writeFrtAndDif(ProgressionInterface progressionInterface, CtuluAnalyzeGroup logs, File file, EfGridData efGridData) {
    final File frt = CtuluLibFile.changeExtension(file, "frt");
    try {
      EfData data = efGridData.getData(H2dVariableType.COEF_FROTTEMENT_FOND, 0);
      if (!data.isElementData()) {
        // on est sur que le frottement de fond n'est pas un vecteur ...
        data = EfLib.getElementDataDanger(data, efGridData.getGrid());
      }
      if (data != null) {
        final CtuluIOOperationSynthese newR = RubarFRTFileFormat.getInstance().write(frt, data, progressionInterface);
        if (newR != null) {
          logs.addAnalyzer(newR.getAnalyze());
        }
      }
    } catch (IOException e) {
      FuLog.error(e);
    }
    final File difFile = CtuluLibFile.changeExtension(file, "dif");
    try {
      EfData data = efGridData.getData(H2dVariableType.COEF_DIFFUSION, 0);
      if (data != null) {
        if (!data.isElementData()) {
          // on est sur que le frottement de fond n'est pas un vecteur ...
          data = EfLib.getElementDataDanger(data, efGridData.getGrid());
        }
        final CtuluIOOperationSynthese newR = RubarDIFFileFormat.getInstance().write(difFile, data, progressionInterface);
        if (newR != null) {
          logs.addAnalyzer(newR.getAnalyze());
        }
      }
    } catch (IOException e) {
      FuLog.error(e);
    }
  }

  public void setRubarGridModifier(H2dRubarGridModifier h2dRubarGridModifier) {
    this.h2dRubarGridModifier = h2dRubarGridModifier;
  }
}
