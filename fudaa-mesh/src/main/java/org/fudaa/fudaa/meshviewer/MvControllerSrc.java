/**
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.impl.EfGridSourceMutable;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author deniger
 * @version $Id: MvControllerSrc.java,v 1.10 2007-05-04 13:59:50 deniger Exp $
 */
public class MvControllerSrc {

  EfGridSource source_;
  EfGridSourceMutable gridModified_;
  MvActCheck checker_;
  MvActModify modifier_;
  MvActView viewer_;
  MvParentInterface parent_;

  public void setDefaultComponent() {}

  public EfGridSource getSource() {
    return source_;
  }

  private void refreshViewer() {
    if (viewer_ != null) {
      viewer_.refresh(source_, gridModified_);
    }
  }

  public MvParentInterface getParent() {
    return parent_;
  }

  public void setParent(final MvParentInterface _c) {
    parent_ = _c;
  }

  public void setLoaded(final CtuluIOOperationSynthese _s) {
    if (_s == null) {
      return;
    }
    source_ = (EfGridSource) _s.getSource();
    gridModified_ = new EfGridSourceMutable();
    if (viewer_ != null) {
      viewer_.init(source_);
    }
  }

  // public void load() {
  // load(null);
  // }
  //  
  // public void load(File f) {
  // if (loader_ != null) {
  // if ((close_ != null) && (!close_.close(this, saver_)))
  // return;
  // loader_.load(f,this);
  // }
  // }

  public void modify() {
    if (modifier_ != null && modifier_.modify(source_, gridModified_)) {
      refreshViewer();
    }
  }

  public void check() {
    if (source_ == null) {
      return;
    }
    if (checker_ != null) {
      checker_.check(this, (FudaaCommonImplementation) parent_);
    }
  }

  public void updateCheckResult(final MvControlResult[] _r) {
    viewer_.updateCheck(_r);
  }

  /**
   *
   */
  public MvActCheck getChecker() {
    return checker_;
  }

  /**
   *
   */
  public EfGridSourceMutable getGridModified() {
    return gridModified_;
  }

  /**
   *
   */
  public MvActModify getModifier() {
    return modifier_;
  }

  /**
   *
   */
  public MvActView getViewer() {
    return viewer_;
  }

  /**
   *
   */
  public void setChecker(final MvActCheck _checker) {
    checker_ = _checker;
  }

  /**
   *
   */
  public void setModifier(final MvActModify _modifier) {
    modifier_ = _modifier;
  }

  /**
   *
   */
  public void setViewer(final MvActView _viewer) {
    viewer_ = _viewer;
    if (source_ != null) {
      viewer_.init(source_);
    }
  }

}