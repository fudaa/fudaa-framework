/*
 *  @creation     13 mai 2005
 *  @modification $Date: 2007-01-19 13:14:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.IOException;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.impl.EfGridBathyAdapter;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author Fred Deniger
 * @version $Id: MvExportActGrid.java,v 1.8 2007-01-19 13:14:23 deniger Exp $
 */
public class MvExportActGrid extends MvExportActAbstract {

  /**
   * Activit� permettant de transformer en T6.
   * 
   * @author Fred Deniger
   * @version $Id: MvExportActGrid.java,v 1.8 2007-01-19 13:14:23 deniger Exp $
   */
  public static class ToT6 extends MvExportActGrid {

    /**
     * @param _ft
     */
    public ToT6(final FileFormatGridVersion _ft) {
      super(_ft);
    }

    @Override
    public EfGridData toDestType(final ProgressionInterface _prog, final EfGridData _grid, final CtuluAnalyze _analyze) {
      if (_grid.getGrid().getEltType() == EfElementType.T6) { return _grid; }
      final MvExportT6Activity toT6 = new MvExportT6Activity(_grid);
      setCurrentActivity(toT6);
      return toT6.process(_prog, _analyze);
    }

  }

  final FileFormatGridVersion ft_;

  public MvExportActGrid(final FileFormatGridVersion _ft) {
    ft_ = _ft;

  }

  protected CtuluIOOperationSynthese writeExport(final File[] _dest, final EfGridData _last,
      final ProgressionInterface _prog) {
    try {
      final EfGridInterface g = new EfGridBathyAdapter(_last.getData(H2dVariableType.BATHYMETRIE, 0), _last.getGrid());
      return ft_.writeGrid(_dest[0], new EfGridSourceDefaut(g, ft_.getFileFormat()), _prog);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return null;

  }

  @Override
  public final void doExport(final ProgressionInterface _prog, final File[] _dest, final CtuluAnalyzeGroup _analyze,
      final String[] _messages) {

    if (FuLog.isTrace()) {
      FuLog.trace("TRE: start export");
    }
    EfGridData last = super.processOperations(_prog, _analyze);
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: stop after filter");
      }
      return;
    }
    CtuluAnalyze logAdapt=new CtuluAnalyze();
    logAdapt.setDesc(MvResource.getS("Adaptation des donn�es"));
    _analyze.addAnalyzer(logAdapt);
    // s'il y a des variables definis sur les elements on les transforme en vari sur les noeuds
    last = toNodesData(_prog, last, logAdapt);
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: top after transofrm to node data");
      }
      return;
    }
    last = toDestType(_prog, last, logAdapt);
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: top after transform in T3");
      }
      return;
    }
    if (FuLog.isTrace()) {
      FuLog.trace("TRE: write grid file " + _dest[0].getAbsolutePath());
    }
    // on ecrit
    final CtuluIOOperationSynthese io = writeExport(_dest, last, _prog);
    if (io != null) {
      _analyze.addAnalyzer(io.getAnalyze());
      addInfo(_messages, last.getGrid().getPtsNb(), last.getGrid().getEltNb());
    }
  }

  @Override
  public int getNbLabelInfoNeeded() {
    return 2;
  }

  @Override
  public String getTitre() {
    return ft_.getName();
  }

  public EfGridData toDestType(final ProgressionInterface _prog, final EfGridData _data, final CtuluAnalyze _analyze) {
    final EfElementType type = _data.getGrid().getEltType();
    MvExportAdaptI act = null;
    // le cas simple
    if (type == EfElementType.T3) { return _data; }
    // le cas connu
    if (type == EfElementType.T6) {
      act = new MvExportT6T3Activity(_data);
    } else {
      act = new MvExportToT3Activity(_data);
    }
    setCurrentActivity(act);
    return act.process(_prog, _analyze);

  }

}
