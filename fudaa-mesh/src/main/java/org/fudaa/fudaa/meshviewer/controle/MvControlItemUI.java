/*
 *  @file         MvControlUI.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.action.MvItemActionUI;

/**
 * @author deniger
 * @version $Id: MvControlItemUI.java,v 1.6 2007-01-19 13:14:15 deniger Exp $
 */
public interface MvControlItemUI extends MvItemActionUI {

  MvControlResult execute(EfGridInterface _g, ProgressionInterface _progress);

}
