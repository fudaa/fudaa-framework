/*
 *  @file         MvControlArea.java
 *  @creation     10 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.action.MvItemAction;
import org.fudaa.fudaa.meshviewer.action.MvItemActionUI;

/**
 * @author deniger
 * @version $Id: MvControlArea.java,v 1.9 2007-01-19 13:14:16 deniger Exp $
 */
public class MvControlArea implements MvItemAction {

  @Override
  public String getDesc() {
    return DodicoLib.getS("Petits �l�ments");
  }

  @Override
  public MvItemActionUI buildUI() {
    return new ControlUI();

  }

  final static class ControlUI extends JPanel implements MvControlItemUI {

    BuTextField ftArea_;

    ControlUI() {
      setLayout(new BuGridLayout(2, 10, 5));
      add(new BuLabel(DodicoLib.getS("El�ments ayant une aire inf�rieure �")));
      ftArea_ = BuTextField.createDoubleField();
      add(ftArea_);
    }

    @Override
    public MvControlResult execute(final EfGridInterface _g, final ProgressionInterface _progress) {
      final String t = ftArea_.getText().trim();
      if (t.length() == 0) { return null; }
      final MvControlResult rf = new MvControlElementResult();
      rf.idx_ = _g.getSmallElement(_progress, Double.parseDouble(t));
      rf.description_ = DodicoLib.getS("Aire inf�rieure � {0}", t);
      return rf;
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

  }

}
