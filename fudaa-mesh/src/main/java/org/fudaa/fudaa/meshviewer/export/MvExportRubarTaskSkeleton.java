/*
 * @creation 19 juin 2006
 *
 * @modification $Date: 2007-06-05 09:01:12 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class MvExportRubarTaskSkeleton extends MvExportTaskSkeleton {
  public static FudaaPanelTaskModel createRubarMaiTaskModel(final MvExportFactory _fact, final boolean _isT6, final FileFormatGridVersion _fmt,
                                                            final File _initFile, final MvSelectionNodeOrEltData _selection, int nbBlockConcentration) {
    final MvExportRubarTaskSkeleton skeleton = new MvExportRubarTaskSkeleton(_fact.getDatas(), _fact.vects_, _fmt, nbBlockConcentration);
    _fact.updateGridTaskModel(skeleton, _fmt.getFileFormat(), _initFile, _selection);
    skeleton.setCustom(new MvExportTaskSkeleton.ExportPanelCustom[]{skeleton.getCustomPanel()});
    return skeleton;
  }

  private final MvExportRubarMAICustomPanel customPanel;

  public MvExportRubarTaskSkeleton(final EfGridData _src, final InterpolationVectorContainer _vects, final FileFormatGridVersion _fmt, int nbBlockConcentration) {
    super(_src, _vects, new MvExportRubarMAI());
    customPanel = new MvExportRubarMAICustomPanel(_src, nbBlockConcentration);
  }

  public MvExportTaskSkeleton.ExportPanelCustom getCustomPanel() {
    return customPanel;
  }

  @Override
  public File[] getDestFiles() {
    final File[] init = super.getDestFiles();
    if (init == null) {
      return null;
    }
    final List l = new ArrayList(Arrays.asList(init));
    if (customPanel.isCofDurSedGenerated()) {
      l.add(CtuluLibFile.changeExtension(init[0], "cof"));
      l.add(CtuluLibFile.changeExtension(init[0], "dur"));
      l.add(CtuluLibFile.changeExtension(init[0], "sed"));
    }
    if (customPanel.isCox()) {
      l.add(CtuluLibFile.changeExtension(init[0], "cox"));
    }
    if (customPanel.isFrtAndDif()) {
      l.add(CtuluLibFile.changeExtension(init[0], "frt"));
      l.add(CtuluLibFile.changeExtension(init[0], "dif"));
    }
    if (customPanel.isInx()) {
      l.add(CtuluLibFile.changeExtension(init[0], "inx"));
    }
    if (customPanel.isFrx()) {
      l.add(CtuluLibFile.changeExtension(init[0], "frx"));
    }
    if (customPanel.isIma()) {
      l.add(CtuluLibFile.changeExtension(init[0], "ima"));
    }
    if (customPanel.isDat()) {
      l.add(CtuluLibFile.changeExtension(init[0], "dat"));
    }
    if (customPanel.isMai()) {
      l.add(CtuluLibFile.changeExtension(init[0], "ima"));
    }
    return (File[]) l.toArray(new File[l.size()]);
  }
}
