/**
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer;

import java.io.File;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * Une interface qui sera appelee pour choisir un fichier et le charger
 * 
 * @author deniger
 * @version $Id: MvActLoad.java,v 1.9 2007-05-04 13:59:50 deniger Exp $
 */
public interface MvActLoad extends MvAct {

  /**
   * Si _f est null, ouvre un file chooser pour choisir le fichier. Sinon, essaie de charger le fichier demander
   * 
   * @return le fichier reellement ouvert
   */
  File load(File _f, MvControllerSrc _s, FudaaCommonImplementation _impl);
}
