package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;


/**
 * Exportation en transformant en �l�ments.
 * @author Adrien Hadoux
 *
 */
public class MvExportToElementDataActivity implements CtuluActivity {

	  final EfGridData src_;
	  final InterpolationVectorContainer vects_;

	  /**
	   * @param _src
	   */
	  public MvExportToElementDataActivity(final EfGridData _src, final InterpolationVectorContainer _vects) {
	    super();
	    src_ = _src;
	    vects_ = _vects;
	  }

	  boolean stop_;

  @Override
	  public void stop() {
	    stop_ = true;
	  }

	  public EfGridData process(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
	    
	    final MvExportDecoratorNodeData res = new MvExportDecoratorNodeData(src_, vects_);
	    if (stop_) { return null; }
	    return res;

	  }
	}
