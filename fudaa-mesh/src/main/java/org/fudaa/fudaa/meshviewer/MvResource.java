/**
 *  @creation     27 mai 2004
 *  @modification $Date: 2006-09-19 15:11:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer;

import com.memoire.bu.BuResource;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sig.FSigResource;

/**
 * @author Fred Deniger
 * @version $Id: MvResource.java,v 1.6 2006-09-19 15:11:18 deniger Exp $
 */
public final class MvResource extends FudaaResource {

  public final static MvResource MV = new MvResource(FSigResource.FSIG);

  private MvResource(final BuResource _parent) {
    super(_parent);
  }

  /**
   * A "shortcut" to get i18n String (MV.getString).
   */
  public static String getS(final String _s) {
    return MV.getString(_s);
  }

  /**
   * A "shortcut" to get i18n String (MV.getString).
   */
  public static String getS(final String _s, final String _v0) {
    return MV.getString(_s, _v0);
  }

  /**
   * A "shortcut" to get i18n String (MV.getString).
   */
  public static String getS(final String _s, final String _v0, final String _v1) {
    return MV.getString(_s, _v0, _v1);
  }

}
