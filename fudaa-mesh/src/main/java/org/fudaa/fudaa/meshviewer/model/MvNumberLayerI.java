/*
 *  @creation     12 oct. 2005
 *  @modification $Date: 2007-01-19 13:14:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.fudaa.meshviewer.MvLayerGrid;

/**
 * @author Fred Deniger
 * @version $Id: MvNumberLayerI.java,v 1.1 2007-01-19 13:14:17 deniger Exp $
 */
public interface MvNumberLayerI {

  void setAll();

  boolean isAll();

  void clearIdxShow();

  boolean isSomethingToView();

  void setItem(int[] _idx);

  String getTitle();

  boolean isSelectionOk(MvLayerGrid _layer);

  int[] getSelectedIdx(MvLayerGrid _layer);

}
