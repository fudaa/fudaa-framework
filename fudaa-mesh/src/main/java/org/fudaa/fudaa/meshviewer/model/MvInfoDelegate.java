/**
 * @creation 6 f�vr. 2004
 * @modification $Date: 2007-01-19 13:14:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.palette.BPaletteInfo;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author deniger
 * @version $Id: MvInfoDelegate.java,v 1.1 2007-01-19 13:14:17 deniger Exp $
 */
public interface MvInfoDelegate {

  /**
   * @param _m
   * @param _selection
   */
  void fillWithPointInfo(BPaletteInfo.InfoData _m, CtuluListSelectionInterface _selection, String _title);

  EbliFormatterInterface getXYFormatter();

  /**
   * @param _m
   * @param _selection
   */
  void fillWithElementInfo(BPaletteInfo.InfoData _m, CtuluListSelectionInterface _selection, String _title);

  void fillWithBcPointInfo(InfoData _receiver, EbliListeSelectionMultiInterface _s, String _title);

  BuTable createValuesTableForNodeLayer();

  boolean isValuesTableAvailableForNodeLayer();

  void fillWithFlecheInfo(BPaletteInfo.InfoData _m, CtuluListSelectionInterface _selection, ZCalqueFleche _src);

}