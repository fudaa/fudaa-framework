/**
 *  @file         TrFindComponent.java
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.fudaa.ebli.calque.find.CalqueFindActionDefault;
import org.fudaa.ebli.find.EbliFindable;

/**
 * @author deniger
 * @version $Id: MvFindActionNodeElt.java,v 1.1 2007-01-19 13:14:08 deniger Exp $
 */
public class MvFindActionNodeElt extends CalqueFindActionDefault {

  final boolean point_;

  /**
   *
   */
  public MvFindActionNodeElt(final MvNodeLayer _layer) {
    super(_layer);
    point_ = true;
  }

  public MvFindActionNodeElt(final MvElementLayer _layer) {
    super(_layer);
    point_ = false;
  }

  @Override
  public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
    return point_ ? ((MvVisuPanel) _parent).isGridPointEditable() : ((MvVisuPanel) _parent).isGridElementEditable();
  }

  @Override
  public String editSelected(final EbliFindable _parent) {
    if (point_) { return ((MvVisuPanel) _parent).editGridPoint(); }
    return ((MvVisuPanel) _parent).editGridPoly();
  }

}