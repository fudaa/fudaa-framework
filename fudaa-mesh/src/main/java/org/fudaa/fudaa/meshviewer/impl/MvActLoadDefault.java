/**
 * @creation 5 f�vr. 2004 @modification $Date: 2007-06-29 15:09:43 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer.impl;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.fu.FuLog;
import java.io.File;
import java.util.List;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.adcirc.AdcircFileFormat;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.ef.io.dunes.DunesMAIFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.supertab.SuperTabFileFormat;
import org.fudaa.dodico.ef.io.trigrid.TrigridFileFormat;
import org.fudaa.dodico.reflux.io.INPFileFormat;
import org.fudaa.dodico.rubar.io.RubarDATFileFormat;
import org.fudaa.dodico.rubar.io.RubarIMAFileFormat;
import org.fudaa.dodico.rubar.io.RubarMAIFileFormat;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvActLoad;
import org.fudaa.fudaa.meshviewer.MvControllerSrc;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author deniger
 * @version $Id: MvActLoadDefault.java,v 1.4 2007-06-29 15:09:43 deniger Exp $
 */
public class MvActLoadDefault implements MvActLoad {

  /**
   * @author fred deniger
   * @version $Id: MvActLoadDefault.java,v 1.4 2007-06-29 15:09:43 deniger Exp $
   */
  static final class CbDocumentListener implements DocumentListener {

    private final BuComboBox cb_;

    /**
     * @param _cb
     */
    CbDocumentListener(final BuComboBox _cb) {
      cb_ = _cb;
    }

    @Override
    public void insertUpdate(final DocumentEvent _e) {
      updateCB(_e.getDocument());
    }

    @Override
    public void removeUpdate(final DocumentEvent _e) {
      updateCB(_e.getDocument());
    }

    @Override
    public void changedUpdate(final DocumentEvent _e) {
      updateCB(_e.getDocument());
    }

    private void updateCB(final Document _d) {
      try {
        cb_.setSelectedItem(guessFormat(new File(_d.getText(0, _d.getLength())).getName(), FT_LIST));
      } catch (final BadLocationException e) {
        // ne fait rien car on s'en fout
      }
    }
  }
  /**
   *
   */
  public final static CtuluPermanentList FT_LIST = new CtuluPermanentList(
          CtuluLibArray.sort(
          new FileFormat[]{
            INPFileFormat.getInstance(), DunesMAIFileFormat.getInstance(), CorEleBthFileFormat.getInstance(),
            SerafinFileFormat.getInstance(), TrigridFileFormat.getInstance(), RubarDATFileFormat.getInstance(),
            RubarMAIFileFormat.getInstance(), RubarIMAFileFormat.getInstance(), SuperTabFileFormat.getInstance(), AdcircFileFormat.getInstance()}));

  @Override
  public File load(final File _fileToLoad, final MvControllerSrc _s, final FudaaCommonImplementation _impl) {
    File file = _fileToLoad;
    FileFormat fileFormatUsed = null;
    if (_fileToLoad != null) {
      fileFormatUsed = guessFormat(file.getName(), FT_LIST);
    }
    final CtuluDialogPanel dialogPanel = new CtuluDialogPanel();
    dialogPanel.addEmptyBorder(5);
    dialogPanel.setLayout(new BuGridLayout(1, 5, 5, false, false));
    final BuComboBox fileFormatCb = new BuComboBox(FT_LIST.toArray());
    fileFormatCb.setSelectedItem(fileFormatUsed);
    final JTextField tfFile = dialogPanel.addLabelFileChooserPanel(FudaaLib.getS("Fichier"), null, false, false);
    if (_fileToLoad != null) {
      tfFile.setText(_fileToLoad.getAbsolutePath());
      tfFile.setToolTipText(_fileToLoad.getAbsolutePath());
    }

    tfFile.getDocument().addDocumentListener(new CbDocumentListener(fileFormatCb));
    dialogPanel.add(fileFormatCb);
    final int res = dialogPanel.afficheModale(_s.getParent().getMvParentComponent(), CtuluLib.getS("Ouvrir"),
                                              CtuluDialog.createRunnableToScroll(tfFile));
    if (CtuluDialogPanel.isOkResponse(res)) {
      final String filename = tfFile.getText();
      if (filename != null) {
        file = new File(filename);
        fileFormatUsed = (FileFormat) fileFormatCb.getSelectedItem();
      }
      if ((file != null) && (fileFormatUsed != null)) {
        loadFileAct(_s, file, (FileFormatGridVersion) fileFormatUsed.getLastVersionInstance(file), _impl);
        return file;
      }
    }
    return null;
  }

  public static EfGridSource load(final CtuluUI _ui, final File _file, final FileFormatGridVersion _grid,
                                  final ProgressionInterface _prog) {
    if (_file == null || _grid == null) {
      return null;
    }
    final CtuluIOOperationSynthese s = _grid.readGrid(_file, _prog);
    if (!_ui.manageAnalyzeAndIsFatal(s.getAnalyze())) {
      return (EfGridSource) s.getSource();
    }
    return null;

  }

  private void loadFileAct(final MvControllerSrc _s, final File _file, final FileFormatGridVersion _v,
                           final FudaaCommonImplementation _impl) {
    if (_v == null) {
      if (FuLog.isWarning()) {
        FuLog.warning("pas de lecture");
      }
      return;
    }
    if (_file.exists()) {
      final CtuluTaskOperationGUI op = new CtuluTaskOperationGUI(_impl, MvResource.getS("Lecture")) {

        @Override
        public void act() {
          final ProgressionInterface prog = _impl.createProgressionInterface(this);
          final CtuluIOOperationSynthese s = _v.readGrid(_file, prog);
          if (s.containsMessages()) {
            s.printAnalyze();
          }
          _impl.manageErrorOperationAndIsFatal(s);
          if (s.containsSevereError()) {
            return;
          }

          final EfGridSource src = (EfGridSource) s.getSource();
          if (src != null) {
            final CtuluAnalyzeGroup group = new CtuluAnalyzeGroup(null);
            final CtuluAnalyze reorienteAnalyze = new CtuluAnalyze();
            EfLib.orienteGrid(src.getGrid(), prog, true, reorienteAnalyze);
            if (reorienteAnalyze.isNotEmpty()) {
              reorienteAnalyze.setDesc(MvResource.getS("Orientation des �l�ments"));
              group.addAnalyzer(reorienteAnalyze);
            }
            try {
              final CtuluAnalyze log = new CtuluAnalyze();
              src.getGrid().computeBord(prog, log);
              src.getGrid().createIndexRegular(prog);
              if (log.isNotEmpty()) {
                group.addAnalyzer(log);
              }
            } catch (final RuntimeException _evt) {
              FuLog.error(_evt);
              _impl.error(getName(), _evt.getMessage());

            }
            CtuluAnalyzeGUI.showDialog(group, _impl, MvResource.getS("Lecture"));
            if (group.containsFatalError()) {
              return;
            }
          }
          _s.setLoaded(s);
          SwingUtilities.invokeLater(openInterneFrame(_impl, _s, _file));
        }
      };
      op.start();
    }
  }

  protected Runnable openInterneFrame(final FudaaCommonImplementation _impl, final MvControllerSrc _controlSrc,
                                      final File _fileToOpen) {
    return new Runnable() {

      @Override
      public void run() {
        final MvInternalFrame i = new MvInternalFrame(_impl, _controlSrc);
        _controlSrc.setViewer(i);
        i.setTitle(_fileToOpen.getName());
        i.setToolTipText(_fileToOpen.getAbsolutePath());
        _impl.addInternalFrame(i);
      }
    };

  }

  /**
   * @param _fileName le nom de fichier
   * @param _filermtList la liste des formats
   * @return le format correspondant au nom de fichier
   */
  public static FileFormat guessFormat(final String _fileName, final List _filermtList) {
    FileFormat temp = null;
    for (int i = _filermtList.size() - 1; i >= 0; i--) {
      temp = (FileFormat) _filermtList.get(i);
      if (temp.createFileFilter().accept(null, _fileName)) {
        return temp;
      }
    }
    return null;
  }
}