/*
 *  @file         MvControlDefault.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.impl;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvActCheck;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.MvControllerSrc;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.controle.MvControlPanel;

/**
 * @author deniger
 * @version $Id: MvActCheckDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public class MvActCheckDefault implements MvActCheck {

  public MvActCheckDefault() {

  }

  List createDescValue(final MvControlResult[] _resultats) {
    final ArrayList descValue = new ArrayList();
    final int nbVariables = _resultats.length;
    for (int i = 0; i < nbVariables; i++) {
      final int[] idx = _resultats[i].getIdx();
      if ((idx != null) && (idx.length > 0)) {
        descValue.add(new String[] { _resultats[i].getDesc(), CtuluLibString.getString(idx.length) });
      }
    }
    return descValue;
  }

  @Override
  public void check(final MvControllerSrc _source, final FudaaCommonImplementation _impl) {
    if ((_source == null) || (_source.getSource().getGrid() == null)) {
      FuLog.warning(new IllegalArgumentException("grid is null"));
      return;
    }
    final MvControlPanel controlPanel = new MvControlPanel();
    if (CtuluDialogPanel.isOkResponse(controlPanel.afficheModale(_source.getParent().getMvParentComponent()))) {
      new CtuluTaskOperationGUI(_impl, MvResource.getS("Contr�le")) {

        @Override
        public void act() {
          final ProgressionInterface prog = _source.getParent().createProgression(this);
          final MvControlResult[] resultats = controlPanel.execute(_source.getSource().getGrid(), prog);
          if (resultats != null) {
            final List descValue = createDescValue(resultats);
            if (descValue.size() > 0) {
              final CtuluDialogPanel dialogPanel = new CtuluDialogPanel();
              dialogPanel.setLayout(new BuBorderLayout());
              dialogPanel.addEmptyBorder(10);
              final String[] name = new String[] { DodicoLib.getS("Description"),
                  DodicoLib.getS("Nombre d'�l�ments d�tect�s") };
              final String[][] values = new String[descValue.size()][2];
              descValue.toArray(values);

              dialogPanel.add(new BuScrollPane(new BuTable(values, name)), BuBorderLayout.CENTER);
              dialogPanel.afficheModale(_source.getParent().getMvParentComponent(), CtuluDialog.OK_OPTION);
            } else {
              _impl.setMainMessageAndClear(MvResource.getS("Aucun �l�ments trouv�s"));
            }
            _source.updateCheckResult(resultats);
          }

          _source.getParent().setActionRunning(false);
        }
      }.start();
    }
  }

}