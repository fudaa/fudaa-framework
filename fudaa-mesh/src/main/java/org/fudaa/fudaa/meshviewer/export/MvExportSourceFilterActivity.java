/*
 * @creation 13 mai 2005
 * 
 * @modification $Date: 2007-05-04 13:59:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntIntHashMap;
import java.util.ArrayList;
import java.util.BitSet;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfRemoveFilter;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author Fred Deniger
 * @version $Id: MvExportSourceFilterActivity.java,v 1.7 2007-05-04 13:59:50 deniger Exp $
 */
public class MvExportSourceFilterActivity implements EfOperation {

  private EfGridData initGrid;

  EfFilter filter_;

  int minPtIdx_;

  int maxPtIdx_;

  int minEltIdx_;

  int maxEltIdx_;

  private boolean stop_;

  private int nbElementNotRemoved;

  /**
   * @param _src la source
   * @param _filter le filtre
   */
  public MvExportSourceFilterActivity(final EfGridData _src, final EfFilter _filter) {
    super();
    initGrid = _src;
    filter_ = _filter;
  }

  @Override
  public void stop() {
    stop_ = true;
  }

  public EfFilter correctFilter(EfFilter init) {
    EfRemoveFilter removeFilter = new EfRemoveFilter();
    removeFilter.setFilter(init);
    removeFilter.setGrid(initGrid.getGrid());
    removeFilter.setSelectFilter(true);
    EfFilter process = removeFilter.process();
    nbElementNotRemoved = removeFilter.getNbElementNotRemoved();
    return process;
  }

  private EfGridDataRenumeroteDecorator buildNewGrid(final ProgressionInterface _prog, final BitSet _node,
      final BitSet _element) {
    if (stop_) { return null; }
    int idxPt = 0;
    // les nouvelles points
    final int nbPt = maxPtIdx_ - minPtIdx_ + 1;
    final int nbElt = maxEltIdx_ - minEltIdx_ + 1;
    final TDoubleArrayList newPoint = new TDoubleArrayList(nbPt * 3);
    TIntIntHashMap ptNewIdxOldIdxMap = new TIntIntHashMap(nbPt);
    final TIntIntHashMap ptOldNew = new TIntIntHashMap(nbPt);
    final EfGridInterface g = initGrid.getGrid();
    // on parcourt les noeuds
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.majProgessionStateOnly(MvResource.getS("Construction maillage"));
    up.setValue(5, nbPt + nbElt);
    for (int i = minPtIdx_; i <= maxPtIdx_; i++) {
      if (stop_) { return null; }
      // si selectionn�, on ajoute le no
      if (_node.get(i)) {
        ptNewIdxOldIdxMap.put(idxPt, i);
        ptOldNew.put(i, idxPt);
        newPoint.add(g.getPtX(i));
        newPoint.add(g.getPtY(i));
        newPoint.add(g.getPtZ(i));
        idxPt++;
      }
      up.majAvancement();
    }
    if (FuLog.isTrace()) {
      FuLog.trace("nb nodes filtres" + idxPt);
    }
    int idxElem = 0;
    final ArrayList newElement = new ArrayList(nbElt);
    TIntIntHashMap eltNewIdxOldIdxMap = new TIntIntHashMap(nbElt);
    for (int i = minEltIdx_; i <= maxEltIdx_; i++) {
      if (stop_) { return null; }
      if (_element.get(i)) {
        final int[] idx = g.getElement(i).getIndices();
        for (int j = idx.length - 1; j >= 0; j--) {
          idx[j] = ptOldNew.get(idx[j]);
        }
        eltNewIdxOldIdxMap.put(idxElem, i);
        newElement.add(new EfElement(idx));
        idxElem++;
      }
      up.majAvancement();
    }
    final EfElement[] elt = new EfElement[newElement.size()];
    if (FuLog.isTrace()) {
      FuLog.trace("nb elt filtres" + elt.length);
    }
    newElement.toArray(elt);
    EfGridDataRenumeroteDecorator res = new EfGridDataRenumeroteDecorator(initGrid, new EfGridArray(newPoint
        .toNativeArray(), elt));
    res.setEltNewIdxOldIdxMap(eltNewIdxOldIdxMap);
    res.setPtNewIdxOldIdxMap(ptNewIdxOldIdxMap);
    return res;
  }

  @Override
  public EfOperationResult process(ProgressionInterface prog) {
    CtuluAnalyze log = new CtuluAnalyze();
    EfGridData out = buildNewAdapter(prog, log);
    EfOperationResult res = new EfOperationResult();
    res.setAnalyze(log);
    res.setFrontierChanged(true);
    res.setGridData(out);
    if (nbElementNotRemoved > 0) {
      res.getAnalyze().addWarn(
          MvResource.getS("{0} �l�ment(s) n'ont pas �t� supprim�s afin de garantir un maillage final correct", Integer.toString(nbElementNotRemoved)));
    }
    return res;
  }

  @Override
  public void setInitGridData(EfGridData in) {
    initGrid = in;

  }

  public EfGridData buildNewAdapter(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    final BitSet element = new BitSet(initGrid.getGrid().getEltNb());
    final BitSet node = new BitSet(initGrid.getGrid().getPtsNb());
    findFilteredElement(_prog, node, element);
    if (element.isEmpty()) {
      _analyze.addFatalError(FSigLib.getS("Aucune donn�e � exporter"));

      return null;
    }
    if (stop_) { return null; }
    final BitSet test = new BitSet(initGrid.getGrid().getEltNb());
    test.set(0, initGrid.getGrid().getEltNb() - 1);
    if (test.equals(element)) { return initGrid; }
    final EfGridDataRenumeroteDecorator newAdapter = buildNewGrid(_prog, node, element);
    if (stop_) { return null; }
    return newAdapter;
  }

  protected void findFilteredElement(final ProgressionInterface _prog, final BitSet _node, final BitSet _element) {
    final EfGridInterface g = initGrid.getGrid();
    // le filtre est null tout est ok
    if (filter_ == null) {
      maxEltIdx_ = g.getEltNb() - 1;
      minEltIdx_ = 0;
      maxPtIdx_ = g.getPtsNb() - 1;
      minPtIdx_ = 0;
      _element.set(0, g.getEltNb() - 1);
      _node.set(0, g.getPtsNb() - 1);
      return;
    }
    EfFilter safeFilter = correctFilter(filter_);
    final int nbElt = g.getEltNb();
    // on parcourt les pas de temps
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(4, nbElt);
    if (_prog != null) {
      up.majProgessionStateOnly(DodicoLib.getS("Filtre"));
    }
    // on parcourt tous les elements
    maxEltIdx_ = -1;
    minEltIdx_ = 0;
    maxPtIdx_ = -1;
    minPtIdx_ = g.getPtsNb() + 1;
    for (int ielt = nbElt - 1; ielt >= 0; ielt--) {
      if (stop_) { return; }
      // si l'element n'est toujours pas filtre positivement
      // puis si le test est positif on active l'element
      if (safeFilter.isActivatedElt(ielt)) {
        if (maxEltIdx_ < 0) {
          maxEltIdx_ = ielt;
        }
        minEltIdx_ = ielt;
        _element.set(ielt);
        final EfElement elt = g.getElement(ielt);
        for (int ipt = elt.getPtNb() - 1; ipt >= 0; ipt--) {
          final int idxPt = elt.getPtIndex(ipt);
          if (!_node.get(idxPt)) {
            if (idxPt < minPtIdx_) {
              minPtIdx_ = idxPt;
            }
            if (idxPt > maxPtIdx_) {
              maxPtIdx_ = idxPt;
            }
            _node.set(idxPt);
          }
        }
      }
      up.majAvancement();
    }
  }

}
