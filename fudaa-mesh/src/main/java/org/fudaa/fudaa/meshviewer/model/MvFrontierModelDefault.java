/**
 * @creation 11 sept. 2003
 * @modification $Date: 2007-04-16 16:35:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Envelope;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnesAbstract;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author deniger
 * @version $Id: MvFrontierModelDefault.java,v 1.2 2007-04-16 16:35:35 deniger Exp $
 */
public class MvFrontierModelDefault extends ZModeleDonnesAbstract implements MvFrontierModel {

  /**
   * @author Fred Deniger
   * @version $Id: MvFrontierModelDefault.java,v 1.2 2007-04-16 16:35:35 deniger Exp $
   */
  public static class BcTableModel extends AbstractTableModel {

    final MvFrontierModel model_;

    final protected int[] tmp_ = new int[2];

    /**
     * @param _model
     */
    public BcTableModel(final MvFrontierModel _model) {
      super();
      model_ = _model;
    }

    @Override
    public int getColumnCount() {
      return 5;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) { return MvResource.getS("Fronti�re"); }
      if (_column == 1) { return MvResource.getS("Indice sur la fronti�re"); }
      if (_column == 2) { return MvResource.getS("Indice global"); }
      if (_column == 3) { return "X"; }
      return "Y";
    }

    @Override
    public int getRowCount() {
      return model_.getNbTotalPoint();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      model_.getIdxFrIdxOnFrFromFrontier(_rowIndex, tmp_);

      if (_columnIndex == 0) { return new Integer(tmp_[0] + 1); }
      if (_columnIndex == 1) { return new Integer(tmp_[1] + 1); }
      if (_columnIndex == 2) { return new Integer(model_.getGlobalIdx(tmp_[0], tmp_[1]) + 1); }
      if (_columnIndex == 3) { return CtuluLib.getDouble(model_.getX(tmp_[0], tmp_[1])); }
      return CtuluLib.getDouble(model_.getY(tmp_[0], tmp_[1]));
    }
  }

  protected MvInfoDelegate delegate_;

  EfGridInterface m_;

  public MvFrontierModelDefault(final EfGridInterface _m) {
    if (_m == null) { throw new IllegalArgumentException("param null"); }
    m_ = _m;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return new CtuluTable(new BcTableModel(this));
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _s) {
    if (delegate_ != null) {
      delegate_.fillWithBcPointInfo(_m, _s.getLayerSelectionMulti(), _s.getTitle());
    }
  }

  @Override
  public GrBoite getDomaine() {
    final GrBoite b = new GrBoite();
    b.o_ = new GrPoint(m_.getMinX(), m_.getMinY(), 0);
    b.e_ = new GrPoint(m_.getMaxX(), m_.getMaxY(), 0);
    return b;
  }

  @Override
  public void getDomaineForPolygoneIdx(final int _idxFr, final Envelope _boite) {
    m_.getFrontiers().getMinMax(m_, _idxFr, _boite);
  }

  @Override
  public int getFrontiereIndice(final int _idxFr, final int _idxOnFr) {
    return m_.getFrontiers().getFrontiereIndice(_idxFr, _idxOnFr);
  }

  /**
   *
   */
  @Override
  public int getGlobalIdx(final int _idxFrontier, final int _idxPtOnFrontier) {
    return m_.getFrontiers().getIdxGlobal(_idxFrontier, _idxPtOnFrontier);
  }

  @Override
  public boolean getIdxFrIdxOnFrFromFrontier(final int _frontierIdx, final int[] _idxFrIdxOn) {
    return m_.getFrontiers().getIdxFrontierIdxPtInFrontierFromFrontier(_frontierIdx, _idxFrIdxOn);
  }

  public int getGlobalIdxFromFr(final int _idxOnFrs) {
    return m_.getFrontiers().getIdxGlobalFrom(_idxOnFrs);
  }

  @Override
  public boolean getIdxFrIdxOnFrFromGeneral(final int _generalIdx, final int[] _idxFrIdxOn) {
    return m_.getFrontiers().getIdxFrontierIdxPtInFrontierFromGlobal(_generalIdx, _idxFrIdxOn);
  }

  @Override
  public int getNbElementIn(final int _i) {
    return m_.getFrontiers().getNbPt(_i);
  }

  @Override
  public int getNbFrontier() {
    return m_.getFrontiers().getNbFrontier();
  }

  @Override
  public int getNbPointInFrontier(final int _i) {
    return m_.getFrontiers().getNbPt(_i);
  }

  @Override
  public EfFrontierInterface getFr() {
    return m_.getFrontiers();
  }

  @Override
  public int getNbTotalPoint() {
    return m_.getFrontiers().getNbTotalPt();
  }

  @Override
  public int getNombre() {
    return m_.getFrontiers() == null ? 0 : m_.getFrontiers().getNbFrontier();
  }

  @Override
  public double getX(final int _idxFrontier, final int _idxPtOnFrontier) {
    return m_.getPtX(m_.getFrontiers().getIdxGlobal(_idxFrontier, _idxPtOnFrontier));
  }

  @Override
  public double getY(final int _idxFrontier, final int _idxPtOnFrontier) {
    return m_.getPtY(m_.getFrontiers().getIdxGlobal(_idxFrontier, _idxPtOnFrontier));
  }

  @Override
  public boolean isT6() {
    return m_.getEltType() == EfElementType.T6;
  }

  @Override
  public final boolean isExternFrontier(final int _idxFr) {
    return m_.getFrontiers().isExtern(_idxFr);
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public boolean point(final GrPoint _p, final int _idxFrontier, final int _idxPtOnFrontier) {
    final int idx = m_.getFrontiers().getIdxGlobal(_idxFrontier, _idxPtOnFrontier);
    _p.setCoordonnees(m_.getPtX(idx), m_.getPtY(idx), m_.getPtZ(idx));
    return true;
  }

  /**
   *
   */
  public void setDelegate(final MvInfoDelegate _delegate) {
    delegate_ = _delegate;
  }

}
