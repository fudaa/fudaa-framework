/**
 * 
 */
package org.fudaa.fudaa.meshviewer.model;


/**
 * @author CANEL Christophe (Genesis)
 *
 */
public interface TrNumberFrontierLayerI extends MvNumberLayerI {
  void setFrontiersOnly();
}
