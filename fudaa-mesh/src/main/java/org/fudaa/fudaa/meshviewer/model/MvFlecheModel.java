/*
 *  @creation     20 sept. 2005
 *  @modification $Date: 2007-01-19 13:14:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id: MvFlecheModel.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
 */
public abstract class MvFlecheModel implements ZModeleSegment {

  MvInfoDelegate delegate_;

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return new CtuluTable(new ZCalqueFleche.ValueTableModel(this));
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    delegate_.fillWithFlecheInfo(_d, _layer.getLayerSelection(), (ZCalqueFleche) _layer);
  }

  protected final MvInfoDelegate getDelegate() {
    return delegate_;
  }

  protected final void setDelegate(final MvInfoDelegate _delegate) {
    delegate_ = _delegate;
  }

}
