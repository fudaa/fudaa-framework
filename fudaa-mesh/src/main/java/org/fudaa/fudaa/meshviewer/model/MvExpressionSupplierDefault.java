/*
 *  @creation     15 juin 2005
 *  @modification $Date: 2007-01-19 13:14:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.find.CalqueFindExpression;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: MvExpressionSupplierDefault.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
 */
public class MvExpressionSupplierDefault extends CalqueFindExpression {

  public MvExpressionSupplierDefault(final ZModeleDonnees _modele) {
    super(_modele);
  }

  public static Variable buildFrIdx(final CtuluExpr _expr) {
    return _expr.addVar("frIdx", MvResource.getS("Indice de la frontière contenant le noeud"));
  }

  public static Variable buildidxGlobalOnFr(final CtuluExpr _expr) {
    return _expr.addVar("frNdIdx", MvResource.getS("Indice du noeud dans la numérotation frontière globale"));
  }

  public static Variable buildidxInFr(final CtuluExpr _expr) {
    return _expr.addVar("frNdLocalIdx", MvResource.getS("Indice du noeud dans sa frontière"));
  }

}
