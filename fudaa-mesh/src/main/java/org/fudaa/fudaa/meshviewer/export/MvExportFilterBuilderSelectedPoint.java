/*
 * @creation 16 mai 2005
 * 
 * @modification $Date: 2007-05-04 13:59:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.fudaa.meshviewer.filter.MvFilterSelectedNode;

/**
 * @author Fred Deniger
 * @version $Id: MvExportFilterBuilderSelectedPoint.java,v 1.5 2007-05-04 13:59:50 deniger Exp $
 */
public class MvExportFilterBuilderSelectedPoint implements MvExportOperationBuilderSelection {

  final CtuluListSelectionInterface selection_;
  final EfGridData grid_;
  EfFilter filter_;

  /**
   * @param _selectedPt les elements selectionnés
   * @param _grid le maillage
   */
  public MvExportFilterBuilderSelectedPoint(final CtuluListSelectionInterface _selectedPt, final EfGridData _grid) {
    super();
    selection_ = _selectedPt;
    grid_ = _grid;
  }

  @Override
  public void buildOperation(final ProgressionInterface _progression) {
    if (filter_ == null) {
      filter_ = new MvFilterSelectedNode(selection_, grid_.getGrid());
    }
  }

  @Override
  public CtuluListSelectionInterface getSelectedMeshes() {
    buildOperation(null);
    return ((MvFilterSelectedNode) filter_).getSelectedElt();
  }

  @Override
  public EfOperation getOperation(final int _tidx) {
    return new MvExportSourceFilterActivity(grid_, filter_);
  }

  @Override
  public void stop() {}
}
