package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.PairString;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.helper.LRUCache;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCoucheContainer;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentMng;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MvSedDataAdapter implements H2dRubarSedimentInterface {
  private final EfGridData nodeData;

  public MvSedDataAdapter(EfGridData nodeData) {
    this.nodeData = nodeData;
  }

  @Override
  public H2dRubarSedimentCoucheContainer getCoucheContainer(int idxTime, int idxNoeud) {

    Map<PairString, H2dRubarSedimentVariableType> variablesById = new HashMap<>();
    for (H2dRubarSedimentVariableType var : H2dRubarSedimentMng.getVariables()) {
      variablesById.put(new PairString(var.getParentVariable().getShortName(), CtuluLibString.getString(var.getCoucheIdx())), var);
    }
    H2dRubarSedimentCoucheContainer res = new H2dRubarSedimentCoucheContainer();
    try {
      for (int i = 0; i < res.getNbCouches(); i++) {
        updateValue(i, idxNoeud, res, H2dRubarSedimentVariableType.EPAISSEUR, variablesById);
        updateValue(i, idxNoeud, res, H2dRubarSedimentVariableType.DIAMETRE, variablesById);
        updateValue(i, idxNoeud, res, H2dRubarSedimentVariableType.ETENDUE, variablesById);
        updateValue(i, idxNoeud, res, H2dRubarSedimentVariableType.CONTRAINTE, variablesById);
      }
    } catch (IOException e) {
      //nothing to do here....
    }
    return res;
  }

  private final Map<PairString, EfData> valuesCached = new LRUCache<>(50);

  public void updateValue(int coucheIdx, int idxNoeud, H2dRubarSedimentCoucheContainer res, H2dVariableTransType variable,
                          Map<PairString, H2dRubarSedimentVariableType> variablesById) throws IOException {
    final PairString key = new PairString(variable.getShortName(), CtuluLibString.getString(coucheIdx));
    EfData data = valuesCached.get(key);
    if (data == null) {
      H2dRubarSedimentVariableType sedimentVariable = variablesById.get(key);
      data = nodeData.getData(sedimentVariable, 0);
      if (data != null) {
        valuesCached.put(key, data);
      }
    } else {
      System.err.println("FOuhnd in cache !");
    }
    if (data != null) {
      res.getCoucheItem(coucheIdx).setValue(variable, data.getValue(idxNoeud));
    }
  }

  @Override
  public int getNbNoeuds() {
    return nodeData.getGrid().getPtsNb();
  }

  @Override
  public int getNbTimes() {
    return 1;
  }

  @Override
  public double getTimes(int idx) {
    return 0;
  }

  private static class CoucheContainer {
  }
}
