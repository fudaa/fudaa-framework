/*
 *  @file         MvCheckElementModelDefault.java
 *  @creation     10 f�vr. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import java.util.Arrays;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvElementModelDefault;

/**
 * @author deniger
 * @version $Id: MvCheckElementModelDefault.java,v 1.13 2007-05-04 13:59:50 deniger Exp $
 */
public class MvCheckElementModelDefault extends MvElementModelDefault implements MvCheckElementModel {

  int[] eltIdx;
  String desc_;

  /**
   * @param _g
   */
  public MvCheckElementModelDefault(final EfGridInterface _g) {
    super(_g);
  }

  @Override
  public boolean isErroneous(final int _eltIdx) {
    return eltIdx == null ? false : Arrays.binarySearch(eltIdx, _eltIdx) >= 0;
  }

  @Override
  public void updateCheckMessage(final MvControlElementResult _r) {
    eltIdx = _r.getIdx();
    desc_ = _r.getDesc();
  }

  @Override
  public int getNbEltWithError() {
    return eltIdx == null ? 0 : eltIdx.length;
  }

  /**
   * Attention: aucun controle sur _i.
   */
  @Override
  public int getEltIdxWithError(final int _i) {
    return eltIdx[_i];
  }

  /**
   * Renvoie la description du controle ou "?" si null.
   */
  @Override
  public String getDesc() {
    return desc_ == null ? "?" : desc_;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    fillWithInfo(_d, _layer.getLayerSelection());
  }

  public void fillWithInfo(final InfoData _m, final CtuluListSelectionInterface _s) {
    _m.put(DodicoLib.getS("Nombre d'�l�ments d�tect�s"), CtuluLibString.getString(eltIdx.length));
    if ((_s != null) && (_s.isOnlyOnIndexSelected())) {
      final int i = _s.getMaxIndex();
      final String s = MvResource.getS("Element n� {0}", CtuluLibString.getString(i + 1));
      _m.put(s +

      DodicoLib.getS("Aire"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(super.g_.getAire(i)));
      _m.put(s + DodicoLib.getS("Angle mini") + " (deg)", CtuluLib.DEFAULT_NUMBER_FORMAT.format(super.g_
          .getMinAngleDegre(i)));
    }
  }

}
