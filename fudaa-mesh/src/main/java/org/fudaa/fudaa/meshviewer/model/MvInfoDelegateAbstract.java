/**
 * @creation 6 f�vr. 2004
 * @modification $Date: 2007-05-04 13:59:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.bu.BuTable;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author deniger
 * @version $Id: MvInfoDelegateAbstract.java,v 1.4 2007-05-04 13:59:50 deniger Exp $
 */
public abstract class MvInfoDelegateAbstract implements MvInfoDelegate {

  /**
   * @return le maillage
   */
  public abstract EfGridInterface getGrid();

  public final int fillWithPointInfoDist(final InfoData _m, final CtuluListSelectionInterface _selection,
      final String _title) {
    _m.put(MvResource.getS("Nombre de noeuds"), CtuluLibString.getString(getGrid().getPtsNb()));
    final int n = _selection == null ? 0 : _selection.getNbSelectedIndex();
    _m.put(MvResource.getS("Nombre de noeuds s�lectionn�s"), CtuluLibString.getString(n));
    if (n > 1) {
      _m.setTitle(_title);
      if (n == 2) {
        // selection non null
        final int i = _selection.getMaxIndex();
        final int i2 = _selection.getMinIndex();
        final EfGridInterface g = getGrid();
        _m.put(MvResource.getS("Distance entre les 2 noeuds"), getXYFormatter().getXYFormatter().format(
            CtuluLibGeometrie.getDistance(g.getPtX(i), g.getPtY(i), g.getPtX(i2), g.getPtY(i2))));
      }
    }
    return n;
  }

  @Override
  public final void fillWithPointInfo(final InfoData _m, final CtuluListSelectionInterface _selection,
      final String _title) {
    final int n = fillWithPointInfoDist(_m, _selection, _title);
    if (n == 1) {
      fillWithPointInfo(_m, _selection.getMaxIndex());
    }
  }

  @Override
  public BuTable createValuesTableForNodeLayer() {
    return null;
  }

  @Override
  public boolean isValuesTableAvailableForNodeLayer() {
    return false;
  }

  /**
   * @param _m
   * @param _idx
   */
  public void fillWithPointInfo(final InfoData _m, final int _idx) {
    final int[] idx = new int[2];
    int idxOnFr = -1;
    _m.setTitle(MvResource.getS("Noeud n� {0}", CtuluLibString.getString(_idx)));
    if (getGrid().getFrontiers() != null) {
      idxOnFr = getGrid().getFrontiers().getIdxOnFrontier(_idx, idx);
    }
    fillWithPointInfo(_m, _idx, idxOnFr, idx[0], idx[1]);
  }

  /**
   * @param _m les donnees a mettre a jour
   * @param _idx l'indices du point
   */
  public final void fillWithXYZPointInfo(final InfoData _m, final int _idx) {
    if (getGrid().getFrontiers() == null) {
      fillWithXYZPointInfo(_m, _idx, -1);
    } else {
      fillWithXYZPointInfo(_m, _idx, getGrid().getFrontiers().getIdxOnFrontier(_idx));
    }
  }

  /**
   * @param _m le receveur des infos
   * @param _idx l'indice global du point
   * @param _idxOnFr l'indice sur les frontiere ( selon spec telemac)
   */
  public void fillWithXYZPointInfo(final InfoData _m, final int _idx, final int _idxOnFr) {
    String title = MvResource.getS("Noeud n� {0}", CtuluLibString.getString(_idx + 1));
    if (_idxOnFr >= 0) {
      title += " (" + CtuluLibString.getString(_idxOnFr + 1) + ')';
    }
    _m.setTitle(title);
    final EfGridInterface g = getGrid();
    _m.put("x", CtuluLib.DEFAULT_NUMBER_FORMAT.format(g.getPtX(_idx)));
    _m.put("y", CtuluLib.DEFAULT_NUMBER_FORMAT.format(g.getPtY(_idx)));
    // _m.put("z", EbliLib.THREE_DIGITS_FORMAT.format(g.getPtZ(_idx)));
  }

  /**
   * @param _m le receveur des infos
   * @param _idxGlobal l'indice global
   * @param _idxGlobOnFr
   * @param _frIdx l'indice de la frontiere
   * @param _idxLocaloOnFr l'indice du point sur la frontiere en question
   */
  public void fillWithPointInfo(final InfoData _m, final int _idxGlobal, final int _idxGlobOnFr, final int _frIdx,
      final int _idxLocaloOnFr) {
    if (_idxGlobOnFr >= 0) {
      _m.put(MvResource.getS("Fronti�re"), CtuluLibString.getString(_frIdx + 1));
      _m.put(MvResource.getS("Indice du noeud sur cette fronti�re"), CtuluLibString.getString(_idxLocaloOnFr + 1));
      _m.put(MvResource.getS("Indice du noeud dans la num�rotation des noeuds de bords"), CtuluLibString
          .getString(_idxGlobOnFr + 1));
    }

    fillWithXYZPointInfo(_m, _idxGlobal, _idxGlobOnFr);
  }

  @Override
  public void fillWithFlecheInfo(final InfoData _m, final CtuluListSelectionInterface _selection,
      final ZCalqueFleche _src) {
    fillWithPointInfo(_m, _selection, _src.getTitle());
  }

  public static void fillWithElementInfo(final InfoData _m, final CtuluListSelectionInterface _selection,
      final EfGridInterface _grid, final CtuluNumberFormatI _fmt, final boolean _addZ, final String _title) {
    _m.put(MvResource.getS("Nombre d'�l�ments"), CtuluLibString.getString(_grid.getEltNb()));
    if (_selection == null || !_selection.isOnlyOnIndexSelected()) {
      _m.setTitle(_title);
      _m.put(MvResource.getS("Nombre d'�l�ments s�lectionn�s"), CtuluLibString.getString(_selection == null ? 0
          : _selection.getNbSelectedIndex()));
    }
    if (_selection == null) { return; }
    CtuluNumberFormatI fmt = _fmt;
    if (fmt == null) {
      fmt = CtuluLib.DEFAULT_NUMBER_FORMAT;
    }
    if (_selection.getNbSelectedIndex() == 2) {
      final int idx = _selection.getMaxIndex();
      final int idx2 = _selection.getMinIndex();
      final double x1 = _grid.getMoyCentreXElement(idx);
      final double y1 = _grid.getMoyCentreYElement(idx);
      final double x2 = _grid.getMoyCentreXElement(idx2);
      final double y2 = _grid.getMoyCentreYElement(idx2);
      final double d = CtuluLibGeometrie.getDistance(x1, y1, x2, y2);
      _m.put(EbliLib.getS("Distance entre les 2 centres"), fmt.format(d));

    } else if (_selection.isOnlyOnIndexSelected()) {
      final int idxElt = _selection.getMaxIndex();
      _m.setTitle(MvResource.getS("Element n� {0}", CtuluLibString.getString(idxElt + 1)));
      final EfElement el = _grid.getElement(idxElt);
      final int nb = el.getPtNb();
      int indice;
      final double x = _grid.getMoyCentreXElement(idxElt);
      final double y = _grid.getMoyCentreYElement(idxElt);
      final double z = _addZ ? _grid.getCentreZElement(idxElt) : 0;
      _m.put(MvResource.getS("Centre de l'�l�ment"), fmt.format(x) + " , " + fmt.format(y)
          + (_addZ ? " , " + fmt.format(z) : CtuluLibString.EMPTY_STRING));
      for (int i = 0; i < nb; i++) {
        indice = el.getPtIndex(i);
        final String idx = CtuluLibString.getString(i + 1);
        int idxOnFr = -1;
        if (_grid.getFrontiers() != null) {
          idxOnFr = _grid.getFrontiers().getIdxOnFrontier(indice);
        }
        _m.put(MvResource.getS("Noeud n� {0}", idx), CtuluLibString.getString(el.getPtIndex(i) + 1)
            + (idxOnFr >= 0 ? " (" + CtuluLibString.getString(idxOnFr + 1) + ')' : CtuluLibString.EMPTY_STRING));
      }
    }

  }

  @Override
  public void fillWithElementInfo(final InfoData _m, final CtuluListSelectionInterface _selection, final String _title) {
    fillWithElementInfo(_m, _selection, getGrid(), getXYFormatter().getXYFormatter(), true, _title);
  }

  @Override
  public void fillWithBcPointInfo(final InfoData _receiver, final EbliListeSelectionMultiInterface _s,
      final String _title) {
    _receiver.setTitle(_title);
    _receiver.put(MvResource.getS("Nombre de fronti�res"), CtuluLibString.getString(getGrid().getFrontiers()
        .getNbFrontier()));
    _receiver.put(MvResource.getS("Nombre de noeuds fronti�res"), CtuluLibString.getString(getGrid().getFrontiers()
        .getNbTotalPt()));
    if (_s == null) { return; }
    final int i = _s.isSelectionInOneBloc();
    if (i >= 0) {
      final CtuluListSelectionInterface l = _s.getSelection(i);
      if (l.isOnlyOnIndexSelected()) {
        final int idxFr = i;
        final int idxOnFr = l.getMaxIndex();
        final int idxGlob = getGrid().getFrontiers().getIdxGlobal(idxFr, idxOnFr);
        final int idxGlobOnFr = getGrid().getFrontiers().getFrontiereIndice(idxFr, idxOnFr);
        fillWithPointInfo(_receiver, idxGlob, idxGlobOnFr, idxFr, idxOnFr);
      }
    } else {
      _receiver.setTitle(MvResource.getS("Noeuds fronti�res"));
    }
    _receiver.put(MvResource.getS("Nombre de noeuds s�lectionn�s"), CtuluLibString.getString(_s.getNbSelectedItem()));
    if (getGrid().getFrontiers().getNbFrontier() > 1 && !_s.isEmpty()) {
      final int[] idx = _s.getIdxSelected();
      Arrays.sort(idx);
      final int nb = idx.length;
      for (int k = 0; k < nb; k++) {
        final CtuluListSelectionInterface sel = _s.getSelection(idx[k]);
        _receiver.put(MvResource.getS("Nombre de noeuds s�lectionn�s sur la fronti�re {0}", CtuluLibString
            .getString(idx[k] + 1)), CtuluLibString.getString(sel.getNbSelectedIndex()));

      }

    }

  }
}