/*
 *  @file         MvControlDefaultPanel.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2006-09-19 15:11:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.action;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.fudaa.ctulu.gui.CtuluDialogPanel;

/**
 * @author deniger
 * @version $Id: MvItemActionsPanel.java,v 1.6 2006-09-19 15:11:19 deniger Exp $
 */
public class MvItemActionsPanel extends CtuluDialogPanel implements ItemListener {

  protected MvItemActionUI[] controls_;
  protected BuCheckBox[] cb_;

  protected final void build(final MvItemAction[] _c) {
    setLayout(new BuVerticalLayout(5));
    final int n = _c.length;
    controls_ = new MvItemActionUI[n];
    cb_ = new BuCheckBox[n];

    for (int i = 0; i < n; i++) {
      controls_[i] = _c[i].buildUI();
      cb_[i] = new BuCheckBox(_c[i].getDesc());
      cb_[i].setSelected(true);
      cb_[i].addItemListener(this);
      add(cb_[i]);
      final JComponent c = controls_[i].getComponent();
      c.setBorder(BorderFactory.createEmptyBorder(3, 20, 3, 3));
      add(c);
    }
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    final Object o = _e.getSource();
    for (int i = cb_.length - 1; i >= 0; i--) {
      if (cb_[i] == o) {
        final JComponent c = controls_[i].getComponent();
        final boolean b = cb_[i].isSelected();
        for (int j = c.getComponentCount() - 1; j >= 0; j--) {
          c.getComponent(j).setEnabled(b);
        }
      }
    }

  }

}
