/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.fu.FuLog;
import java.util.Comparator;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPainterComparator.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public class MvIsoPainterComparator implements Comparator {

  @Override
  public int compare(final Object _o1, final Object _o2) {
    if (_o1 == _o2) { return 0; }
    if ((_o1 instanceof MvIsoPainter) && (_o2 instanceof MvIsoPainter)) {
      final MvIsoPainter i1 = (MvIsoPainter) _o1;
      final MvIsoPainter i2 = (MvIsoPainter) _o2;
      int r = i1.getNom().compareTo(i2.getNom());
      if (r == 0) {
        r = i1.hashCode() - i2.hashCode();
        if (r == 0) {
          r = 1;
        }
      }
      return r;
    }
    FuLog.warning(new Throwable());
    return -1;
  }
}