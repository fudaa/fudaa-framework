/*
 *  @creation     17 juin 2005
 *  @modification $Date: 2007-04-16 16:35:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.calque.find.CalqueFindExpression;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: MvExpressionSupplierFrNode.java,v 1.2 2007-04-16 16:35:35 deniger Exp $
 */
public class MvExpressionSupplierFrNode extends MvExpressionSupplierDefault {

  Variable x_;

  Variable y_;

  Variable frIdx_;

  Variable idx_;

  Variable idxGlobalOnFr_;

  Variable idxInFr_;

  public MvExpressionSupplierFrNode(final MvFrontierModel _model) {
    super(_model);
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    frIdx_ = buildFrIdx(_expr);
    idxGlobalOnFr_ = buildidxGlobalOnFr(_expr);
    idxInFr_ = buildidxInFr(_expr);
    idx_ = CalqueFindExpression.buildIdxNode(_expr);
    x_ = CalqueFindExpression.buildXNode(_expr);
    y_ = CalqueFindExpression.buildYNode(_expr);

  }

  protected void majVariable(final int _idx, final Variable[] _varToUpdate, final int[] _frIdx) {
    final MvFrontierModel nodes = (MvFrontierModel) data_;
    x_.setValue(CtuluLib.getDouble(nodes.getX(_frIdx[0], _frIdx[1])));
    y_.setValue(CtuluLib.getDouble(nodes.getY(_frIdx[0], _frIdx[1])));
    frIdx_.setValue(CtuluLib.getDouble(_frIdx[0] + 1));
    idxInFr_.setValue(CtuluLib.getDouble(_frIdx[1] + 1));
    final int idx = CtuluLibArray.findObjectEgalEgal(_varToUpdate, idx_);
    if (idx >= 0) {
      idx_.setValue(CtuluLib.getDouble(1 + nodes.getGlobalIdx(_frIdx[0], _frIdx[1])));
    }
  }
  
  @Override
  public void majVariable(int _idx, Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    idxGlobalOnFr_.setValue(CtuluLib.getDouble(_idx + 1));

    final int[] frIdx = new int[2];
    if (!((MvFrontierModel) data_).getIdxFrIdxOnFrFromFrontier(_idx, frIdx)) { return; }
    majVariable(_idx, _varToUpdate, frIdx);

  }

}
