/**
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;

/**
 * @author deniger
 * @version $Id: MvControlResult.java,v 1.1 2007-01-19 13:14:16 deniger Exp $
 */
public abstract class MvControlResult {
  public String description_;
  public int[] idx_;


  public String getDesc() {
    return description_;
  }

  public int[] getIdx() {
    return idx_;
  }

  public abstract ZCalqueAffichageDonnees buildLayer(EfGridInterface _g);

}
