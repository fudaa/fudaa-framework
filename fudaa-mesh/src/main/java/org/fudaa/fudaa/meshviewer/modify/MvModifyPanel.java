/*
 *  @file         MvModifyPanel.java
 *  @creation     11 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.modify;

import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.impl.EfGridSourceMutable;
import org.fudaa.fudaa.meshviewer.action.MvItemActionsPanel;

/**
 * @author deniger
 * @version $Id: MvModifyPanel.java,v 1.5 2007-01-19 13:14:35 deniger Exp $
 */
public class MvModifyPanel extends MvItemActionsPanel {

  public MvModifyPanel() {}

  public boolean modify(final EfGridSource _src, final EfGridSourceMutable _currentGrid) {
    return false;
  }
}
