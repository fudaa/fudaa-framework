package org.fudaa.fudaa.meshviewer.gridprocess;

import com.memoire.bu.BuValueValidator;
import java.text.DecimalFormat;
import java.util.Collection;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

public class BathyVariable implements H2dVariableProviderInterface
{
  private EfGridInterface grid;
  private CtuluArrayDouble doubles;
  
  public BathyVariable()
  {
    this(new EfGridArray(new double[0], new EfElement[0]));
  }
  
  public BathyVariable(EfGridInterface grid)
  {
    this.setGrid(grid);
  }

  public void setGrid(EfGridInterface grid)
  {
    this.grid = grid;
    int ptsNb = this.grid.getPtsNb();
    double[] zPts = new double[ptsNb];
    
    for (int i = 0; i < ptsNb; i++)
    {
      zPts[i] = this.grid.getPtZ(i);
    }

    this.doubles = new CtuluArrayDouble(zPts);
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }

  @Override
  public boolean containsVarToModify()
  {
    return true;
  }

  @Override
  public CtuluNumberFormatI getFormater(H2dVariableType t)
  {
    return new CtuluNumberFormatDefault(new DecimalFormat());
  }

  @Override
  public EfGridInterface getGrid()
  {
    return this.grid;
  }

  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(H2dVariableType t)
  {
    return this.doubles;
  }

  @Override
  public Collection getUsableVariables()
  {
    return null;
  }

  @Override
  public BuValueValidator getValidator(H2dVariableType t)
  {
    return null;
  }

  @Override
  public H2dVariableType[] getVarToModify()
  {
    return new H2dVariableType[]{H2dVariableType.BATHYMETRIE};
  }

  @Override
  public CtuluCollectionDouble getViewedModel(H2dVariableType t)
  {
    return this.doubles;
  }

  @Override
  public boolean isElementVar()
  {
    return false;
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable var)
  {
    return this.doubles;
  }
}
