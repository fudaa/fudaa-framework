/*
 *  @file         GVSourceChecker.java
 *  @creation     5 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer;

import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author deniger
 * @version $Id: MvActCheck.java,v 1.7 2007-01-19 13:14:17 deniger Exp $
 */
public interface MvActCheck extends MvAct {

  void check(MvControllerSrc _src, FudaaCommonImplementation _impl);

}
