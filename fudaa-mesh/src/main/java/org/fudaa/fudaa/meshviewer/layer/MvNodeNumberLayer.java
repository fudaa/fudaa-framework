/**
 *  @creation     4 d�c. 2003
 *  @modification $Date: 2008-02-20 10:11:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntIterator;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.fudaa.fudaa.meshviewer.model.MvNumberDelegate;
import org.fudaa.fudaa.meshviewer.model.MvNumberLayerI;

/**
 * @author deniger
 * @version $Id: MvNodeNumberLayer.java,v 1.26.6.1 2008-02-20 10:11:50 bmarchan Exp $
 */
public class MvNodeNumberLayer extends ZCalqueAffichageDonnees implements MvNumberLayerI {

  private final MvNodeModel model_;
  private boolean showFrontierIdx_;
  private boolean showValueForIdx_;
  final MvNumberDelegate delegate_;  
  private TraceBox traceBox = new TraceBox();

  public MvNodeNumberLayer(final MvNodeModel _m) {
    model_ = _m;
    delegate_ = new MvNumberDelegate(_m.getNombre());
    super.setTitle(MvResource.getS("Indices des noeuds"));
  }

  @Override
  public void setItem(final int[] _idx) {
    if (delegate_.setSelectedPoint(_idx)) {
      quickRepaint();
    }
  }

    public TraceBox getTraceBox() {
        return traceBox;
    }

  @Override
  public LineString getSelectedLine() {
    return null;
  }

  @Override
  public int[] getSelectedIdx(final MvLayerGrid _layer) {
    return _layer == null ? null : _layer.getSelectedPtIdx();
  }

  @Override
  public boolean isSelectionOk(final MvLayerGrid _layer) {
    return _layer == null ? false : !_layer.isSelectionPointEmpty();
  }

  @Override
  public void clearIdxShow() {
    if (delegate_.clearIdxShow()) {
      quickRepaint();
    }
  }

  @Override
  public void setAll() {
    if (delegate_.setAll()) {
      quickRepaint();
    }
  }

  @Override
  public boolean isAll() {
    return delegate_.isAll();
  }

  @Override
  public boolean isSomethingToView() {
    return delegate_.isSomethingToView();
  }

  public void changeFrontierIdxShownState() {
    showFrontierIdx_ = !showFrontierIdx_;
    if(showFrontierIdx_)
        showValueForIdx_ = false;
    quickRepaint();
  }
  public void changeFrontierIdxValueShownState() {
    showValueForIdx_ = !showValueForIdx_;
    if(showValueForIdx_)
        showFrontierIdx_ = false;
    quickRepaint();
  }

  
  
  public MvNodeModel getModel() {
    return model_;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return null;
  }

  /**
   *
   */
  public boolean isFrontierIdxShown() {
    return showFrontierIdx_;
  }
  public boolean isValueIdxShown() {
    return showValueForIdx_;
  }
  

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return model_;
  }
  
  public String getValueForIdx(final int idX) {
      return "";
  }

  
  
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if (isRapide()) { return; }
    if (!isSomethingToView()) { return; }
    final GrBoite clip = _clipReel;
    final GrPoint p = new GrPoint();
    final GrMorphisme versEcran = _versEcran;
    final Font old = _g.getFont();
    final Color oldC = _g.getColor();
    _g.setFont(getFont());
    if (isAttenue()) {
      _g.setColor(attenueCouleur(getForeground()));
    } else {
      _g.setColor(getForeground());
    }
    double x, y;
    String s;

    if (isAll()) {
      final FontMetrics fm = _g.getFontMetrics();
      final Color fgColor = _g.getColor();
      final Color bgColor = getBackground();
      for (int idx = model_.getNombre() - 1; idx >= 0; idx--) {
        model_.point(p, idx, true);
        if (clip.contientXY(p)) {
          p.autoApplique(versEcran);
          if(showValueForIdx_) {
            final int idxFr = model_.getGrid().getFrontiers().getIdxOnFrontier(idx);
            s = getValueForIdx(idxFr);                      
          }else
          if (showFrontierIdx_) {
            final int idxFr = model_.getGrid().getFrontiers().getIdxOnFrontier(idx);
            if (idxFr >= 0) {
              s = CtuluLibString.getString(idx + 1) + "( " + (idxFr + 1) + " )";
            } else {
              s = CtuluLibString.getString(idx + 1);
            }
          } else {
            s = CtuluLibString.getString(idx + 1);
          }
          final Rectangle2D rec = fm.getStringBounds(s, _g);
          x = p.x_ - rec.getWidth() / 2;
          y = p.y_ - 3;
          rec.setFrame(x, y - fm.getAscent(), rec.getWidth(), fm.getAscent() + 2);             
          traceBox.paintBox(_g,(int) x, (int) y, s);
          
        }
      }
    } else {
      final TIntIterator it = delegate_.getIdxIterator();
      final FontMetrics fm = _g.getFontMetrics();
      final Color fgColor = _g.getColor();
      final Color bgColor = getBackground();
      // int h = g.getFontMetrics().getHeight() / 2;
      for (int i = delegate_.getNbIdxSelected(); i-- > 0;) {
        final int idx = it.next();
        model_.point(p, idx, true);
        if (clip.contientXY(p)) {
          p.autoApplique(versEcran);
          if(showValueForIdx_) {
            //final int idxFr = model_.getGrid().getFrontiers().getIdxOnFrontier(idx);
            s = getValueForIdx(idx);                      
          }else
          if (showFrontierIdx_) {
            final int idxFr = model_.getGrid().getFrontiers().getIdxOnFrontier(idx);
            if (idxFr >= 0) {
              s = CtuluLibString.getString(idx + 1) + "( " + (idxFr + 1) + " )";
            } else {
              s = CtuluLibString.getString(idx + 1);
            }
          } else {
            s = CtuluLibString.getString(idx + 1);
          }
          final Rectangle2D rec = fm.getStringBounds(s, _g);
          x = p.x_ - rec.getWidth() / 2;
          y = p.y_ - 3;
          rec.setFrame(x, y - fm.getAscent(), rec.getWidth(), fm.getAscent() + 2); 
          traceBox.paintBox(_g,(int) x, (int) y, s);
        }
      }
    }
    _g.setFont(old);
    _g.setColor(oldC);
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
      final GrBoite _clipReel) {}

  @Override
  public GrBoite getDomaineOnSelected() {
    return null;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    return null;
  }

  public void setFrontierIdxShown(final boolean _b) {
    if (showFrontierIdx_ != _b) {
      showFrontierIdx_ = _b;
      if(_b)
        showValueForIdx_ = false;
      quickRepaint();
    }
  }
  public void setValueIdxShown(final boolean _b) {
    if (showValueForIdx_ != _b) {
      showValueForIdx_ = _b;
      if(_b)
          showFrontierIdx_ = false;
      quickRepaint();
    }
  }
}
