/*
 *  @creation     18 août 2005
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.*;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author Fred Deniger
 * @version $Id: MvExportChooseVarAndTime.java,v 1.12 2007-05-04 13:59:50 deniger Exp $
 */
public class MvExportChooseVarAndTime extends CtuluDialogPanel {

  private static class LbUpdater implements ListSelectionListener {

    final JLabel lb_;
    final String nb_;

    public LbUpdater(final JLabel _lb, final int _nb) {
      super();
      lb_ = _lb;
      nb_ = _nb < 0 ? CtuluLibString.EMPTY_STRING : ('/' + CtuluLibString.getString(_nb));
    }

    @Override
    public void valueChanged(final ListSelectionEvent _e) {
      lb_.setText(CtuluLibArray.getSelectedIdxNb((ListSelectionModel) _e.getSource()) + nb_);

    }

  }

  class ListenerForError implements ListSelectionListener {

    private final CtuluDialogPanel pn_;

    public CtuluDialogPanel getPn() {
      return pn_;
    }

    public ListenerForError(final CtuluDialogPanel _pn) {
      super();
      pn_ = _pn;
    }

    public ListenerForError() {
      this(MvExportChooseVarAndTime.this);
    }

    @Override
    public void valueChanged(final ListSelectionEvent _e) {
      pn_.setErrorText(computeErrorText());
    }

  }

  public static boolean isOk(final MvExportChooseVarAndTime _pn, final String _titre, final Frame _f) {
    _pn.activeErrorListener();
    return CtuluDialogPanel.isOkResponse(_pn.afficheModale(_f, _titre));
  }

  ListSelectionListener errorListener_;

  final BuEmptyList timeList_;

  ListSelectionListener timeListener_;
  BuScrollPane timeSc_;

  final BuEmptyList varList_;

  ListSelectionListener varListener_;

  public MvExportChooseVarAndTime(final ListModel _varModel, final FudaaCourbeTimeListModel _timeModel,
      final String _help) {
    timeList_ = _timeModel == null ? null : new BuEmptyList();
    if (timeList_ != null) {
      timeList_.setModel(_timeModel);
    }
    // les variables
    varList_ = _varModel == null ? null : new BuEmptyList();
    if (varList_ != null) {
      varList_.setModel(_varModel);
    }
    setLayout(new BuBorderLayout(0, 5));
    if (_help != null) {
      add(new BuLabel(_help), BuBorderLayout.NORTH);
    }
    final boolean onlyOne = varList_ == null || timeList_ == null;
    final BuPanel pnCenter = new BuPanel(onlyOne ? (LayoutManager) new BuBorderLayout(3, 3) : new BuGridLayout(2, 5, 5,
        true, true, true, true));
    if (varList_ != null) {
      final BuPanel tmpPanel = onlyOne ? pnCenter : new BuPanel(new BuBorderLayout());
      tmpPanel.add(new BuLabel(CtuluLib.getS("Variables")), BuBorderLayout.NORTH);
      tmpPanel.add(new BuScrollPane(varList_));
      tmpPanel.setBorder(BorderFactory.createEtchedBorder());
      if (!onlyOne) {
        pnCenter.add(tmpPanel);
      }
    }
    if (timeList_ != null) {
      final BuPanel tmpPanel = onlyOne ? pnCenter : new BuPanel(new BuBorderLayout());
      tmpPanel.add(new BuLabel(CtuluLib.getS("Pas de temps")), BuBorderLayout.NORTH);
      timeSc_ = new BuScrollPane(timeList_);
      tmpPanel.add(timeSc_);
      tmpPanel.setBorder(BorderFactory.createEtchedBorder());
      if (!onlyOne) {
        pnCenter.add(tmpPanel);
      }
    }
    add(pnCenter, BuBorderLayout.CENTER);
    final BuPanel pnInfo = new BuPanel(new BuGridLayout(2, 3, 3, true, true, true, true));
    pnInfo.setName("pnInfo");
    if (timeList_ != null) {
      final BuLabel lbTimeSelectedValue = new BuLabel();
      timeListener_ = new LbUpdater(lbTimeSelectedValue, timeList_.getModel().getSize());
      timeList_.getSelectionModel().addListSelectionListener(timeListener_);
      pnInfo.add(new BuLabel(MvResource.getS("Nombre de pas de temps sélectionnés:")));
      pnInfo.add(lbTimeSelectedValue);
    }
    if (varList_ != null) {
      final BuLabel lbVarSelectedValue = new BuLabel();
      varListener_ = new LbUpdater(lbVarSelectedValue, varList_.getModel().getSize());
      varList_.getSelectionModel().addListSelectionListener(varListener_);
      pnInfo.add(new BuLabel(MvResource.getS("Nombre de variables sélectionnées:")));
      pnInfo.add(lbVarSelectedValue);
    }

    add(pnInfo, BuBorderLayout.SOUTH);
    if (timeList_ != null) {
      timeList_.clearSelection();
      timeList_.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      timeList_.getSelectionModel().setSelectionInterval(0, timeList_.getModel().getSize() - 1);
    }

    if (varList_ != null) {
      varList_.clearSelection();
      varList_.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      // VarList non null donc _varModel non null
      varList_.getSelectionModel().setSelectionInterval(0, varList_.getModel().getSize() - 1);

    }
  }

  public void activeErrorListener() {
    activeErrorListener(this);
  }

  public void activeErrorListener(final CtuluDialogPanel _pn) {
    if (errorListener_ == null) {
      errorListener_ = new ListenerForError(_pn);
      if (varList_ != null) {
        varList_.getSelectionModel().addListSelectionListener(errorListener_);
      }
      if (timeList_ != null) {
        timeList_.getSelectionModel().addListSelectionListener(errorListener_);
      }
    }
  }

  public Runnable getListRunnable() {
    return new Runnable() {

      @Override
      public void run() {
        scrollTimeToEnd();

      }

    };
  }

  public void addCompInInfoComponent(final JComponent _cp, final JComponent _value) {
    final JComponent c = (JComponent) BuLib.findNamedComponent(this, "pnInfo");
    c.add(_cp);
    c.add(_value);
  }

  public String computeErrorText() {
    String err = null;
    if (timeList_ != null && timeList_.isSelectionEmpty()) {
      err = MvResource.getS("Aucun pas de temps sélectionné");
    }
    if (varList_ != null && varList_.isSelectionEmpty()) {
      if (err == null) {
        err = MvResource.getS("Aucune variable sélectionnée");
      } else {
        err += CtuluLibString.LINE_SEP + MvResource.getS("Aucune variable sélectionnée");
      }
    }
    return err;
  }

  public double[] getSelectedTimeStep() {
    final int[] idx = getSelectedTimeStepIdx();
    if (idx == null) { return null; }
    final double[] res = new double[idx.length];
    final FudaaCourbeTimeListModel list = getTimeModel();
    for (int i = idx.length - 1; i >= 0; i--) {
      res[i] = list.getTimeInSec(idx[i]);
    }
    return res;
  }

  public int[] getSelectedTimeStepIdx() {
    return timeList_ == null ? null : timeList_.getSelectedIndices();
  }

  public CtuluVariable[] getSelectedVar() {
    if (varList_ == null) { return new CtuluVariable[0]; }
    final Object[] res = varList_.getSelectedValues();
    final CtuluVariable[] resfinal = new CtuluVariable[res.length];
    System.arraycopy(res, 0, resfinal, 0, resfinal.length);
    return resfinal;
  }

  public FudaaCourbeTimeListModel getTimeModel() {
    return timeList_ == null ? null : (FudaaCourbeTimeListModel) timeList_.getModel();
  }

  public ListSelectionModel getTimeSelectionModel() {
    return timeList_ == null ? null : timeList_.getSelectionModel();
  }

  public ListModel getVarModel() {
    return varList_ == null ? null : varList_.getModel();
  }

  public ListSelectionModel getVarSelectionModel() {
    return varList_ == null ? null : varList_.getSelectionModel();
  }

  public void scrollTimeToEnd() {
    if (timeList_ != null && timeSc_ != null) {
      final Rectangle cellBounds = timeList_.getCellBounds(timeList_.getModel().getSize() - 1, timeList_.getModel()
          .getSize() - 1);
      timeSc_.getViewport().scrollRectToVisible(cellBounds);
    }
  }

  public void setTimeSelection(final ListSelectionModel _selection) {
    if (timeList_ != null) {
      timeList_.getSelectionModel().removeListSelectionListener(timeListener_);
      if (errorListener_ != null) {
        timeList_.getSelectionModel().removeListSelectionListener(errorListener_);
      }
      timeList_.setSelectionModel(_selection);
      _selection.addListSelectionListener(timeListener_);
      if (errorListener_ != null) {
        _selection.addListSelectionListener(errorListener_);
      }
    }
  }

  public void setVarSelected(final Object _var) {
    varList_.setSelectedValue(_var, true);
  }

  public void setVarSelection(final ListSelectionModel _selection) {
    if (varList_ != null) {
      varList_.getSelectionModel().removeListSelectionListener(varListener_);
      if (errorListener_ != null) {
        timeList_.getSelectionModel().removeListSelectionListener(errorListener_);
      }
      varList_.setSelectionModel(_selection);
      _selection.addListSelectionListener(varListener_);
      if (errorListener_ != null) {
        _selection.addListSelectionListener(errorListener_);
      }
    }
  }

  @Override
  public boolean isDataValid() {
    return getErrorText() == null || getErrorText().trim().length() == 0;
  }

}
