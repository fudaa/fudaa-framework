/*
 *  @creation     17 mai 2005
 *  @modification $Date: 2007-06-05 09:01:11 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfNeighborMesh;

/**
 * @author Fred Deniger
 * @version $Id: MvExportToNodeDataActivity.java,v 1.6 2007-06-05 09:01:11 deniger Exp $
 */
public class MvExportToNodeDataActivity implements CtuluActivity {

  final EfGridData src_;
  final InterpolationVectorContainer vects_;

  /**
   * @param _src
   */
  public MvExportToNodeDataActivity(final EfGridData _src, final InterpolationVectorContainer _vects) {
    super();
    src_ = _src;
    vects_ = _vects;
  }

  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
  }

  public EfGridData process(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    final EfNeighborMesh neighbor = EfNeighborMesh.compute(src_.getGrid(), _prog);
    final MvExportDecoratorEltData res = new MvExportDecoratorEltData(src_, neighbor, vects_);
    if (stop_) { return null; }
    return res;

  }
}
