/*
 GPL 2
 */
package org.fudaa.fudaa.meshviewer.threedim;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TIntArrayList;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.media.j3d.Appearance;
import javax.media.j3d.IndexedGeometryArray;
import javax.media.j3d.Material;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionInteger;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.volume.BGrille;
import org.fudaa.ebli.volume.BGrilleIrreguliere;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.Mv3DFrontierData;

/**
 *
 * @author Frederic Deniger
 */
public class Mv3DBathy {
  private Mv3DBathy() {}

  public static BGrille addBuilding(final EfGridData _src, final CtuluCollectionInteger _type,
      final CtuluCollectionDouble _heightBuilding) {
    final EfGridInterface grid = _src.getGrid();
    EfFrontierInterface fr = grid.getFrontiers();
    if (fr == null) {
      grid.computeBord(null, new CtuluAnalyze());
      fr = grid.getFrontiers();
    }
    if (fr == null) { return null; }
    if (!_src.isDefined(H2dVariableType.BATHYMETRIE)) { return null; }
    EfData data = null;
    try {
      data = _src.getData(H2dVariableType.BATHYMETRIE, 0);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      return null;
    }
    final List pointsList = new ArrayList(100);
    final TIntArrayList index = new TIntArrayList(300);
    final int nbFr = fr.getNbFrontier();
    final GrBoite boiteZ = new GrBoite(grid.getEnvelope(null));
    int idxFr = 0;
    for (int i = 0; i < nbFr; i++) {
      if (!fr.isExtern(i)) {
        if (_type.getValue(idxFr) != Mv3DFrontierData.getIdxNone() && _heightBuilding.getValue(idxFr) > 0) {
          double maxZ = 0;
          final int nbPt = fr.getNbPt(i);
          final Point3d[] ptsHaut = new Point3d[nbPt];
          int firstIdx = pointsList.size();
          for (int j = 0; j < nbPt; j++) {
            final int idxGlobal = fr.getIdxGlobal(i, j);
            final double x = grid.getPtX(idxGlobal);
            final double y = grid.getPtY(idxGlobal);
            final double z = data.getValue(idxGlobal);
            boiteZ.ajusteZ(z);
            if (z > maxZ) {
              maxZ = z;
            }
            pointsList.add(new Point3d(x, y, z));
            ptsHaut[j] = new Point3d(x, y, z);
          }
          final double d = maxZ + _heightBuilding.getValue(idxFr);
          boiteZ.ajusteZ(d);
          for (int j = 0; j < nbPt; j++) {
            ptsHaut[j].z = d;
            pointsList.add(ptsHaut[j]);
            final int thisIdxBas = firstIdx + j;
            final int nextIdxBas = ((j + 1) % nbPt) + firstIdx;
            final int nextIdxHaut = nextIdxBas + nbPt;
            final int thisIdxHaut = thisIdxBas + nbPt;
            index.add(thisIdxBas);
            index.add(nextIdxHaut);
            index.add(thisIdxHaut);
            index.add(thisIdxBas);
            index.add(nextIdxBas);
            index.add(nextIdxHaut);
          }
          final com.sun.j3d.utils.geometry.GeometryInfo info = new com.sun.j3d.utils.geometry.GeometryInfo(
              com.sun.j3d.utils.geometry.GeometryInfo.POLYGON_ARRAY);
          info.setCoordinates(ptsHaut);
          // info.setContourCounts(new int[] { 1 });
          info.setStripCounts(new int[] { ptsHaut.length });
          final IndexedGeometryArray res = info.getIndexedGeometryArray();
          final int nbPtTop = res.getVertexCount();
          final Point3d[] topPt = new Point3d[nbPtTop];
          for (int j = 0; j < nbPtTop; j++) {
            topPt[j] = new Point3d(0, 0, 0);
          }
          res.getCoordinates(0, topPt);
          firstIdx = pointsList.size();
          pointsList.addAll(Arrays.asList(topPt));
          final int nbConnect = res.getIndexCount();
          final int[] newConnect = new int[nbConnect];
          res.getCoordinateIndices(0, newConnect);
          for (int j = newConnect.length - 1; j >= 0; j--) {
            newConnect[j] += firstIdx;
          }
          index.add(newConnect);
        }
        idxFr++;
      }
    }
    if (index.size() > 0) {
      final BGrilleIrreguliere res = new BGrilleIrreguliere(MvResource.getS("B�timents"));
      res.setGeometrie(pointsList.size(), (Point3d[]) pointsList.toArray(new Point3d[pointsList.size()]), index.size(),
          index.toNativeArray());
      res.setCouleur(Color.GRAY);
      res.setBoite(boiteZ);
      res.setVisible(true);
      final Appearance app = res.getAppearence();
      Material mat = app.getMaterial();
      if (mat == null) {
        mat = new Material();
        BGrille.updateDefaultCapabilities(app);
      }
      mat.setEmissiveColor(new Color3f(new Color(16, 16, 16)));
      mat.setShininess(0.01f);
      res.setRapide(false);
      res.setEclairage(true);
      return res;
    }
    return null;

  }

  public static BGrille addBuilding(final EfGridData _src, final ZCalqueLigneBrisee layer) {
    List<GISPolygone> buildings = extractBuildings(layer);

    if (buildings.size() == 0) { return null; }

    final EfGridInterface grid = _src.getGrid();
    final GrBoite boiteZ = new GrBoite(grid.getEnvelope(null));
    final List<Point3d> pointsList = new ArrayList<Point3d>();
    final TIntArrayList index = new TIntArrayList();
    final EfGridDataInterpolator interpolator = new EfGridDataInterpolator(_src);

    for (GISPolygone building : buildings) {
      final List<Point3d> ptsBas = new ArrayList<Point3d>();
      final List<Point3d> ptsHaut = new ArrayList<Point3d>();

      for (int i = 0; i < (building.getNumPoints() - 1); i++) {
        final Coordinate coord = building.getCoordinateN(i);
        final Point3d gridPt = calculateGridPoint3d(_src, interpolator, coord);

        // if the point is outside of the grid.
        if (gridPt == null) {
          continue;
        }

        //TODO Voir si garder + gridPt.z.
        final Point3d buildingPt = new Point3d(coord.x, coord.y, coord.z + gridPt.z);

        boiteZ.ajusteZ(gridPt.z);
        boiteZ.ajusteZ(buildingPt.z);

        ptsBas.add(gridPt);
        ptsHaut.add(buildingPt);
      }

      final int nbPt = ptsBas.size();

      // Not enough points to draw the  current building.
      if (nbPt < 2) {
        continue;
      }

      int offset = pointsList.size();

      for (int i = 0; i < nbPt; i++) {
        pointsList.add(ptsBas.get(i));
        pointsList.add(ptsHaut.get(i));

        final int thisIdxBas = offset + (i * 2);
        final int nextIdxBas = offset + (((i + 1) % nbPt) * 2);

        index.add(thisIdxBas);
        index.add(nextIdxBas + 1);
        index.add(thisIdxBas + 1);
        index.add(thisIdxBas);
        index.add(nextIdxBas);
        index.add(nextIdxBas + 1);
      }

      // If the number of point is heigher than 2, create the building roof.
      if (nbPt > 2) {
        final com.sun.j3d.utils.geometry.GeometryInfo info = new com.sun.j3d.utils.geometry.GeometryInfo(
            com.sun.j3d.utils.geometry.GeometryInfo.POLYGON_ARRAY);
        info.setCoordinates(ptsHaut.toArray(new Point3d[0]));
        info.setStripCounts(new int[] { ptsHaut.size() });
  
        final IndexedGeometryArray res = info.getIndexedGeometryArray();
        final int nbPtTop = res.getVertexCount();
        final Point3d[] topPt = new Point3d[nbPtTop];
        for (int j = 0; j < nbPtTop; j++) {
          topPt[j] = new Point3d(0, 0, 0);
        }
        res.getCoordinates(0, topPt);
        offset = pointsList.size();
        pointsList.addAll(Arrays.asList(topPt));
        final int nbConnect = res.getIndexCount();
        final int[] newConnect = new int[nbConnect];
        res.getCoordinateIndices(0, newConnect);
        for (int j = newConnect.length - 1; j >= 0; j--) {
          newConnect[j] += offset;
        }
        index.add(newConnect);
      }
    }

    //TODO Voir si mettre getName � la place de getTitle.
    final BGrilleIrreguliere grid3d = new BGrilleIrreguliere(layer.getTitle());

    grid3d.setGeometrie(pointsList.size(), (Point3d[]) pointsList.toArray(new Point3d[pointsList.size()]),
        index.size(), index.toNativeArray());
    grid3d.setCouleur(Color.GRAY);
    grid3d.setBoite(boiteZ);
    grid3d.setVisible(true);

    final Appearance app = grid3d.getAppearence();
    Material mat = app.getMaterial();

    if (mat == null) {
      mat = new Material();
      BGrille.updateDefaultCapabilities(app);
    }

    mat.setEmissiveColor(new Color3f(new Color(16, 16, 16)));
    mat.setShininess(0.01f);

    grid3d.setRapide(false);
    grid3d.setEclairage(true);

    return grid3d;
  }

  private static List<GISPolygone> extractBuildings(final ZCalqueLigneBrisee layer) {
    final List<GISPolygone> buildings = new ArrayList<GISPolygone>();
    layer.modeleDonnees().prepareExport();
    final GISZoneCollectionLigneBrisee geomData = layer.modeleDonnees().getGeomData();

    for (int i = 0; i < geomData.getNumGeometries(); i++) {
      final Geometry geometry = geomData.getGeometry(i);

      if (geometry instanceof GISPolygone) {
        buildings.add((GISPolygone) geometry);
      }
    }

    return buildings;
  }

  private static Point3d calculateGridPoint3d(final EfGridData _src, final EfGridDataInterpolator _interpolator,
      Coordinate coord) {
    final int elemIdx = EfIndexHelper.getElementEnglobant(_src.getGrid(), coord.x, coord.y, null);

    // if the point is outside of the grid.
    if (elemIdx == -1) { return null; }

    try {
      return new Point3d(coord.x, coord.y, _interpolator.interpolate(elemIdx, coord.x, coord.y,
          H2dVariableType.BATHYMETRIE, 0));
    } catch (final IOException _evt) {
      FuLog.error(_evt);

      return null;
    }
  }
}

