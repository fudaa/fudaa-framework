/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2007-01-19 13:14:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import java.util.Collection;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPaintersSource.java,v 1.1 2007-01-19 13:14:18 deniger Exp $
 */
public interface MvIsoPaintersSource {

  /**
   * Permet de recuperer les isopainter.
   * 
   * @param setToFill la collection qui recevra tous les isopainter
   */
  void getIsoPainters(Collection _setToFill);

}
