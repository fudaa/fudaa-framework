/*
 * @creation 11 mai 2005
 *
 * @modification $Date: 2007-06-05 09:01:11 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureWriter;
import org.geotools.data.Transaction;
import org.geotools.feature.AttributeTypeBuilder;
import org.geotools.feature.FeatureTypes;
import org.opengis.feature.IllegalAttributeException;
import org.geotools.feature.SchemaException;
import org.geotools.feature.type.BasicFeatureTypes;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: MvExportActDataStoreSrc.java,v 1.14 2007-06-05 09:01:11 deniger Exp $
 */
public class MvExportActDataStoreSrc extends MvExportActAbstract {
  final static class VarTime {
    int time_;
    CtuluVariable var_;
  }

  private static final int MAX = 18;
  final GISFileFormat gisFileFormat;
  DecimalFormat decFmt_;
  DecimalFormat exposant_;
  CtuluDurationFormatter fmt_ = new CtuluDurationFormatter(true, true);
  List<AttributeDescriptor> listType_;
  Map<AttributeDescriptor, VarTime> varType_;

  /**
   * @param gisFileFormat la fournisseur des writers
   */
  public MvExportActDataStoreSrc(final GISFileFormat gisFileFormat) {
    this.gisFileFormat = gisFileFormat;
    fmt_.setAlwaysDisplayZero(false);
  }

  private final AttributeTypeBuilder attributeBuilder = new AttributeTypeBuilder();

  private void buildVarType() {
    varType_ = null;

    final int nbVar = getNbSelectedVar();

    // si des pas de temps sont fournis
    final int nbTime = getNbSelectedTimeStep();
    if (nbTime * nbVar == 0) {
      varType_ = Collections.EMPTY_MAP;
      listType_ = Collections.EMPTY_LIST;
      return;
    }
    varType_ = new HashMap<>(nbVar * nbTime);
    listType_ = new ArrayList<>(nbVar * nbTime);
    final Set usedName = new HashSet();
    int idx = 0;
    for (int i = 0; i < nbTime; i++) {
      for (int j = 0; j < nbVar; j++) {
        final CtuluVariable tj = super.selectedVar_[j];
        String name = tj.getID();
        if (nbTime > 1) {
          name += '_' + fmt_.format(selectedTimeStep_[i]);
        }
        name = FuLib.clean(name);
        if (nbTime > 1 && name.length() > 10) {
          name = FuLib.clean(tj.getID() + '_' + selectedTimeStep_[i]);
          if (name.length() > 10) {
            name = FuLib.clean(tj.getID() + '_' + (i + 1));
          }
        }
        if (name.length() > 10) {
          name = name.substring(0, 10);
        }
        if (usedName.contains(name)) {
          name = Integer.toString(++idx);
          if (usedName.contains(name)) {
            continue;
          }
        }
        usedName.add(name);
        final VarTime varTime = new VarTime();
        varTime.time_ = i;
        varTime.var_ = tj;
        final AttributeDescriptor t = attributeBuilder.binding(Double.class).defaultValue(CtuluLib.ZERO).buildDescriptor(name);
        varType_.put(t, varTime);
        listType_.add(t);
      }
    }
  }

  private AttributeDescriptor createFrElementType() {
    return attributeBuilder.binding(LinearRing.class).buildDescriptor(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME);
  }

  private AttributeDescriptor createFrOuvertType() {
    return attributeBuilder.binding(LineString.class).buildDescriptor(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME);
  }

  private AttributeDescriptor createNodeType() {
    return attributeBuilder.binding(Point.class).buildDescriptor(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME);
  }

  /**
   * @param _store la destination
   * @param _prog la barre de progression
   */
  private int exportElement(EfGridData src, final DataStore _store, final ProgressionInterface _prog)
      throws IOException, SchemaException, IllegalAttributeException {
    if (_store == null) {
      return 0;
    }
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    final EfGridInterface g = src.getGrid();
    final int nbElt = src.getGrid().getEltNb();
    up.setValue(10, nbElt);
    AttributeDescriptor[] att = null;
    EfGridData data = src;
    if (listType_ != null && listType_.size() > 0) {
      // la geom + les valeurs
      att = new AttributeDescriptor[1 + listType_.size()];
      att[0] = createFrElementType();
      // on copie les attributs des variables
      for (int i = 1; i < att.length; i++) {
        att[i] = listType_.get(i - 1);
      }
      data = new MvExportDecoratorNodeData(src_, vects_);
    }

    if (att == null) {
      att = new AttributeDescriptor[]{createFrElementType()};
    }
    final SimpleFeatureType featureType = FeatureTypes.newFeatureType(att, "mesh");
    _store.createSchema(featureType);

    if (!goOn_) {
      return 0;
    }
    int nbEltExported = 0;
    try (FeatureWriter<SimpleFeatureType, SimpleFeature> writer = _store.getFeatureWriter(_store.getTypeNames()[0], Transaction.AUTO_COMMIT)) {
      for (int i = 0; i < nbElt && goOn_; i++) {
        final SimpleFeature feature = writer.next();
        feature.setDefaultGeometry(data.getGrid().getElementRing(i));
        fillObject(feature, i, data);
        writer.write();
        nbEltExported++;
        up.majAvancement();
      }
    } catch (final IOException e) {
      throw e;
    } catch (final IllegalAttributeException e) {
      throw e;
    }
    return nbEltExported;
  }

  /**
   * @param _store
   * @param _prog
   */
  private String exportFrontier(final EfGridData grid, final DataStore _store, final ProgressionInterface _prog,
                                CtuluAnalyze _analyze) throws IOException, SchemaException, IllegalAttributeException {
    if (_store == null) {
      return CtuluLibString.ZERO;
    }

    final AttributeDescriptor[] att = new AttributeDescriptor[]{operation_ == null ? createFrElementType() : createFrOuvertType()};
    final SimpleFeatureType featureType = FeatureTypes.newFeatureType(att, "frontiers");
    _store.createSchema(featureType);
    if (!goOn_) {
      return CtuluLibString.ZERO;
    }

    String res = null;

    grid.getGrid().computeBord(_prog, _analyze);
    try (FeatureWriter<SimpleFeatureType, SimpleFeature> writer = _store.getFeatureWriter(_store.getTypeNames()[0], Transaction.AUTO_COMMIT)) {
      if (operation_ == null) {
        int nbExported = 0;
        final EfFrontierInterface fr = grid.getGrid().getFrontiers();
        final EfGridInterface g = grid.getGrid();
        final int nbFr = fr.getNbFrontier();
        // pas de filtre le plus simple
        final ProgressionUpdater up = new ProgressionUpdater(_prog);
        up.setValue(10, nbFr);
        for (int i = 0; i < nbFr && goOn_; i++) {
          final SimpleFeature feature = writer.next();
          feature.setDefaultGeometry(g.getFrontierRing(i));
          writer.write();
          nbExported++;
          up.majAvancement();
        }
        res = CtuluLibString.getString(nbExported);
      } else {
        res = exportFrPartial(grid, _prog, writer);
      }
    } catch (final IllegalAttributeException e) {
      throw e;
    } catch (final IOException e) {
      throw e;
    }
    return res;
  }

  private String exportFrPartial(final EfGridData grid, final ProgressionInterface _prog, final FeatureWriter<SimpleFeatureType, SimpleFeature> _writer)
      throws IOException, IllegalAttributeException {
    final EfFrontierInterface fr = grid.getGrid().getFrontiers();
    final EfGridInterface g = grid.getGrid();
    final int nbFr = fr.getNbFrontier();
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    // pas de filtre le plus simple
    up.setValue(10, nbFr);
    int nbExported = 0;
    boolean isPartial = false;
    // on construit des polylignes pour les morceaux de frontieres selectionnees
    for (int i = 0; i < nbFr && goOn_; i++) {
      boolean allSelected = true;
      final int nbPointInFr = fr.getNbPt(i);
      TDoubleArrayList listEnCours = null;
      // la liste contenant le point 0 de m....
      TDoubleArrayList listFirst = null;
      boolean start = false;
      allSelected = true;
      for (int j = 0; j < nbPointInFr && goOn_; j++) {
        final int globIdx = fr.getIdxGlobal(i, j);
        start = true;
        if (listEnCours == null) {
          listEnCours = new TDoubleArrayList(nbPointInFr * 3);
        }
        if (j == 0) {
          listFirst = listEnCours;
        }
        listEnCours.add(g.getPtX(globIdx));
        listEnCours.add(g.getPtY(globIdx));
        listEnCours.add(g.getPtZ(globIdx));
      }
      if (allSelected) {
        listFirst = null;
        final SimpleFeature feature = _writer.next();
        feature.setDefaultGeometry(g.getFrontierRing(i));
        _writer.write();
        nbExported++;
      }

      if (listFirst != null && listFirst.size() >= 6) {

        final SimpleFeature feature = _writer.next();
        feature.setDefaultGeometry(GISGeometryFactory.INSTANCE.createLineString(new GISCoordinateSequence(listFirst)));
        _writer.write();
        nbExported++;
      }

      up.majAvancement();
    }

    return (isPartial) ? nbExported + " (" + MvResource.getS("extraits") + ')' : CtuluLibString.getString(nbExported);
  }

  private void fillObject(final SimpleFeature _o, final int _idxObject, final EfGridData _data) throws IOException {
    if (listType_ != null) {
      final int size = listType_.size();

      for (int i = 0; i < size; i++) {
        final VarTime varTime = varType_.get(listType_.get(i));
        // on commence a 1 car le premier attribut correspond a la geometrie
        _o.setAttribute(i + 1, formatDouble(_data.getData(varTime.var_, varTime.time_, _idxObject)));
      }
    }
  }

  private boolean containsEltData(final EfGridData _data) throws IOException {
    if (listType_ != null) {
      for (int i = 0; i < listType_.size(); i++) {
        final VarTime varTime = (VarTime) varType_.get(listType_.get(i));
        if (_data.isElementVar(varTime.var_)) {
          return true;
        }
      }
    }
    return false;
  }

  private Double formatDouble(final double _init) {
    String s = Double.toString(_init);
    if (s.length() > MAX) {
      if (decFmt_ == null) {
        decFmt_ = CtuluLib.getDecimalFormat();
      }
      decFmt_.setMaximumFractionDigits(10);
      s = decFmt_.format(_init);
      if (s.length() > MAX) {
        if (exposant_ == null) {
          exposant_ = new DecimalFormat("0.#####E0");
        }
        s = exposant_.format(_init);
        if (s.length() > MAX) {
          if (Math.abs(_init) < 1E-6) {
            return CtuluLib.ZERO;
          }
          final double d = Math.round(_init * 1E2) / 1E2;
          s = decFmt_.format(d);
          if (s.length() > MAX) {
            s = exposant_.format(d);
          }
          return new Double(s);
        }
      } else {
        return new Double(s);
      }
    }
    return CtuluLib.getDouble(_init);
  }

  @Override
  public void doExport(final ProgressionInterface _prog, final File[] _dest, final CtuluAnalyzeGroup _analyze,
                       final String[] _message) {
    buildVarType();
    int node = 0;
    int ele = 0;
    String fr = CtuluLibString.ZERO;
    if (_dest == null) {
      return;
    }
    buildVarType();
    final Envelope env = src_.getGrid().getEnvelope(null);
    EfGridData finalGrid = processOperations(_prog, _analyze);
    try {
      if (_dest[0] != null) {
        node = exportNode(finalGrid,
            GISExportDataStoreFactory.createDataStore(gisFileFormat.getDataStoreFactorySpi(), _dest[0].toURI().toURL(), env, true), _prog);
      }
      if (goOn_ && _dest[1] != null) {
        ele = exportElement(finalGrid, GISExportDataStoreFactory.createDataStore(gisFileFormat.getDataStoreFactorySpi(), _dest[1].toURI().toURL(), env,
            true), _prog);
      }
      if (goOn_ && _dest[2] != null) {
        CtuluAnalyze exportFr = new CtuluAnalyze();
        exportFr.setDesc(MvResource.getS("Cr�ation de la fronti�re"));
        _analyze.addAnalyzer(exportFr);
        fr = exportFrontier(finalGrid, GISExportDataStoreFactory.createDataStore(gisFileFormat.getDataStoreFactorySpi(), _dest[2].toURI().toURL(), env,
            true), _prog, exportFr);
      }
    } catch (final Exception _e) {
      FuLog.warning(_e);
      _analyze.getMainAnalyze().addFatalError(_e.getClass().getName() + ": " + _e.getMessage());
    }
    int idx = 0;
    _message[idx++] = MvResource.getS("Nombre de noeuds export�s");
    _message[idx++] = Integer.toString(node);
    _message[idx++] = MvResource.getS("Nombre d'�l�ments export�s");
    _message[idx++] = Integer.toString(ele);
    _message[idx++] = MvResource.getS("Nombre de fronti�res export�es");
    _message[idx++] = fr;
  }

  /**
   * @param _store la destination
   * @param _prog la barre de progression
   * @return le nombre de noeuds exporter
   */
  public int exportNode(EfGridData datas, final DataStore _store, final ProgressionInterface _prog) throws IOException,
      SchemaException, IllegalAttributeException {
    if (_store == null) {
      return 0;
    }
    final int nbPt = datas.getGrid().getPtsNb();
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(10, nbPt);
    AttributeDescriptor[] att = null;
    EfGridData data = datas;
    if (listType_ != null && listType_.size() > 0) {
      // la geom + les valeurs
      att = new AttributeDescriptor[1 + listType_.size()];
      att[0] = createNodeType();
      // on copie les attributs des variables
      for (int i = 1; i < att.length; i++) {
        att[i] = listType_.get(i - 1);
      }
      // s'il y a des valeurs definies sur les elements, il faut les traduire sur les noeuds
      if (containsEltData(data)) {
        final MvExportToNodeDataActivity act = new MvExportToNodeDataActivity(data, vects_);
        setCurrentActivity(act);
        final CtuluAnalyze analyze = new CtuluAnalyze();
        data = act.process(_prog, analyze);
        if (!isInternOk(analyze, data)) {
          if (FuLog.isTrace()) {
            FuLog.trace("TRE: stop after filter");
          }
          return 0;
        }
      }
    }
    if (att == null) {
      att = new AttributeDescriptor[]{createNodeType()};
    }
    final SimpleFeatureType featureType = FeatureTypes.newFeatureType(att, "nodes");
    _store.createSchema(featureType);
    if (!goOn_) {
      return 0;
    }

    final EfGridInterface grid = datas.getGrid();

    int nbNode = 0;


    try (FeatureWriter<SimpleFeatureType, SimpleFeature> writer = _store.getFeatureWriter(_store.getTypeNames()[0], Transaction.AUTO_COMMIT)) {
      for (int i = 0; i < nbPt && goOn_; i++) {
        final SimpleFeature feature = writer.next();
        feature.setDefaultGeometry(new GISPoint(grid.getPtX(i), grid.getPtY(i), grid.getPtZ(i)));
        fillObject(feature, i, data);
        writer.write();
        nbNode++;
        up.majAvancement();
      }
    } catch (final IOException e) {
      throw e;
    } catch (final IllegalAttributeException e) {
      throw e;
    }
    return nbNode;
  }

  @Override
  public int getNbLabelInfoNeeded() {
    return 3;
  }

  @Override
  public String getTitre() {
    return gisFileFormat.name();
  }
}
