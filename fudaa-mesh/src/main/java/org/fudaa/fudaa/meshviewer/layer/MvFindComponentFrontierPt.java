/**
 *  @file         TrFindComponentBcPoint.java
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer.layer;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ebli.find.EbliFindComponentExpr;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author deniger
 * @version $Id: MvFindComponentFrontierPt.java,v 1.1 2007-01-19 13:14:08 deniger Exp $
 */
public class MvFindComponentFrontierPt extends EbliFindComponentExpr {

  /**
   *
   */
  public MvFindComponentFrontierPt(final String _l, final CtuluExpr _expr) {
    super(_l, _expr);
  }

  JComboBox cb_;

  @Override
  protected void buildOptionsPanel() {
    final BuPanel pn = new BuPanel(new BuGridLayout(2, 3, 3));
    pn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), MvResource.getS("Options")));
    final DefaultComboBoxModel model_ = new DefaultComboBoxModel(new String[] {
        MvResource.getS("Utiliser les indices globaux sur les fronti�res"),
        MvResource.getS("Utiliser les indices des noeuds") });
    pn.add(btIdx_);
    cb_ = new BuComboBox();
    cb_.setModel(model_);
    btIdx_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        cb_.setEnabled(getBtIdx().isSelected());
      }
    });
    pn.add(cb_);
    pn.add(btExpr_);
    add(pn, BuBorderLayout.SOUTH);
  }

  protected BuRadioButton getBtIdx() {
    return btIdx_;
  }

  @Override
  public String getSearchId() {
    if (cb_.getSelectedIndex() == 1) { return "GLOB_IDX"; }
    return super.getSearchId();
  }
}
