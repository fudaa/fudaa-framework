/**
 * @creation 5 d�c. 2003
 * @modification $Date: 2007-01-19 13:14:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer.model;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntHashSet;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.ebli.calque.ZModelePolygone;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author deniger
 * @version $Id: MvElementModel.java,v 1.1 2007-01-19 13:14:17 deniger Exp $
 */
public interface MvElementModel extends ZModelePolygone {

  /**
   * @return le nombre de point
   */
  int getNbPoint();

  /**
   * @param _i l'indice voulu
   * @param _list la liste des points a remplir
   */
  void fillWithPtIdx(int _i, TIntHashSet _list);

  /**
   * @return le conteneur d'expression.
   */
  EbliFindExpressionContainerInterface getExpressionContainer();

  /**
   * @param _g le maillage
   */
  void setGrid(EfGrid _g);

  /**
   * @param _idxElt l'indice de l'element
   * @param _idxPtInElt l'indice du point dans l'element
   * @return l'indice du point
   */
  int getIndice(int _idxElt, int _idxPtInElt);

  /**
   * @param _idxFrontier l'indice de la frontiere
   * @param _idxPtOnFrontier l'indice du point sur la frontiere
   * @return l'indice global
   */
  int getFrGlobalIdx(int _idxFrontier, int _idxPtOnFrontier);

  /**
   * @param _idx l'indice du noeud
   * @param _pt le point a remplir
   * @return true si le point doit etre dessine
   */
  boolean point(int _idx, GrPoint _pt);

  /**
   * @return le nombre de frontiere
   */
  int getFrNbFrontier();

  /**
   * @param _i l'indice de l'element
   * @return le nombre de points de l'element appartenant a la frontiere
   */
  int getFrNbPointInFrontier(int _i);

  /**
   * @return le maillage support
   */
  EfGridInterface getGrid();

  double getX(int idxElt, int idxPtInElt);

  Envelope getEnvelopeForElement(int idxElt);

  double getY(int idxElt, int idxPtInElt);

  int getNbPt(int idxElt);
  
  @Override
  boolean isPainted(int idxElt);

}
