/*
 * @creation 26 sept. 06
 * @modification $Date: 2007-05-04 13:59:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TObjectIntHashMap;
import java.util.Observable;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluArrayInteger;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorChoice;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluValuesEditorPanel;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author fred deniger
 * @version $Id: Mv3DFrontierData.java,v 1.4 2007-05-04 13:59:50 deniger Exp $
 */
public class Mv3DFrontierData extends Observable {

  CtuluArrayDouble elevation_;
  CtuluArrayInteger type_;

  private final static TIntObjectHashMap IDX_NAME = new TIntObjectHashMap(2);
  private final static TIntObjectHashMap IDX_ID = new TIntObjectHashMap(2);

  private static String getStrBuilding() {
    return MvResource.getS("B�timent");
  }

  private static String getStrNone() {
    return MvResource.getS("Vide");
  }

  static {
    IDX_NAME.put(getIdxNone(), getStrNone());
    IDX_ID.put(getIdxNone(), "NOTHING");
    IDX_NAME.put(getIdxBuilding(), getStrBuilding());
    IDX_ID.put(getIdxBuilding(), "BUILDING");
  }

  public static TObjectIntHashMap getIdIdxMap() {
    final TObjectIntHashMap res = new TObjectIntHashMap(IDX_ID.size());
    final TIntObjectIterator it = IDX_ID.iterator();
    for (int i = IDX_ID.size(); i-- > 0;) {
      it.advance();
      res.put(it.value(), it.key());
    }
    return res;

  }

  public static int getIdxBuilding() {
    return 1;
  }

  public static int getIdxNone() {
    return 0;
  }

  public static String getFileIdx(final int _i) {
    final String res = (String) IDX_ID.get(_i);
    if (res == null) { return (String) IDX_ID.get(0); }
    return res;
  }

  private final EfFrontierInterface fr_;

  /**
   * @param _nb le nombre de fronti�res internes
   */
  public Mv3DFrontierData(final EfFrontierInterface _fr) {
    fr_ = _fr;
    final int nb = fr_.getNbFrontierIntern();
    elevation_ = new CtuluArrayDouble(nb) {
      
      @Override
      protected void fireObjectChanged(int indexGeom, Object newValue) {
        dataFor3DChanged();
      }
    };
    type_ = new CtuluArrayInteger(nb) {
      @Override
      protected void fireDataChanged() {
        dataFor3DChanged();
      }
    };
    type_.setAll(getIdxBuilding());
  }

  protected final void dataFor3DChanged() {
    setChanged();
    notifyObservers();

  }

  public int getType(final int _idxFrInternes) {
    return type_.getValue(_idxFrInternes);
  }

  public double getElevetion(final int _idxFrInternes) {
    return elevation_.getValue(_idxFrInternes);
  }

  public CtuluArrayDouble getElevation() {
    return elevation_;
  }

  public void setElevation(final CtuluArrayDouble _elevation) {
    elevation_ = _elevation;
  }

  public CtuluArrayInteger getType() {
    return type_;
  }

  public void setType(final CtuluArrayInteger _type) {
    type_ = _type;
  }

  /**
   * @param _frSelection la selection des frontieres (internes/externes) dans l'ordre pr��tabli par Telemac.
   */
  public void edit(final CtuluListSelectionInterface _frSelection, final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (_frSelection == null || _frSelection.isEmpty()) { return; }
    // on reconstruit la selection en prenant que les frontieres internes.
    final TIntArrayList selectionIntern = new TIntArrayList(fr_.getNbFrontierIntern());
    final int nb = fr_.getNbFrontier();
    int idxIntern = 0;
    for (int i = 0; i < nb; i++) {
      if (!fr_.isExtern(i)) {
        if (_frSelection.isSelected(i)) {
          selectionIntern.add(idxIntern);
        }
        idxIntern++;
      }
    }
    selectionIntern.sort();
    final CtuluValueEditorChoice type = new CtuluValueEditorChoice(new String[] { getStrNone(), getStrBuilding() },null);
    type.setReturnInteger(true);
    final CtuluValueEditorDouble db = new CtuluValueEditorDouble(true);
    final CtuluValuesEditorPanel pn = new CtuluValuesEditorPanel(new String[] { MvResource.getS("Type"),
        MvResource.getS("Hauteur") }, selectionIntern.toNativeArray(), new CtuluValueEditorI[] { type, db },
        new CtuluCollection[] { type_, elevation_ }, _cmd);
    pn
        .setHelpText(MvResource
            .getS("Lors de l'affichage 3D, des b�timents peuvent �tre dessin�s sur les fronti�res internes.\nCe panneau permet d'activer ou non cette option pour chaque fronti�re\net de pr�ciser la hauteur du b�timent."));
    pn.afficheModale(_ui.getParentComponent(), MvResource.getS("Editer les b�timents pour la vue 3D"));
  }

}
