/*
 * @creation 16 mai 2005
 * 
 * @modification $Date: 2007-06-05 09:01:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.fu.FuLog;
import java.io.File;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author Fred Deniger
 * @version $Id: MvExportActAbstract.java,v 1.10 2007-06-05 09:01:12 deniger Exp $
 */
public abstract class MvExportActAbstract implements MvExportActInterface {

  protected EfOperation operation_;
  protected EfGridData src_;
  protected boolean goOn_;
  protected CtuluVariable[] selectedVar_;
  protected double[] selectedTimeStep_;

  protected final CtuluActivity getCurrentActivity() {
    return currentActivity_;
  }

  protected final EfGridData processOperations(final ProgressionInterface _prog, final CtuluAnalyzeGroup _analyze) {
    if (operation_ != null) {
      // final MvExportSourceFilterActivity filterAct = new MvExportSourceFilterActivity(src_, operation_);
      setCurrentActivity(operation_);
      operation_.setInitGridData(src_);
      EfOperationResult process = operation_.process(_prog);
      _analyze.addAnalyzer(process.getAnalyze());
      return process.getGridData();
      // return filterAct.buildNewAdapter(_prog, _analyze);
    }
    return src_;
  }

  public boolean isInternOk(final CtuluAnalyzeGroup _analyze, final EfGridData _data) {
    boolean isOk = goOn_ && _data != null && !_analyze.containsFatalError();
    if (isOk) {
      if (_data.getGrid() == null || _data.getGrid().getPtsNb() == 0) {
        isOk = false;
        _analyze.getMainAnalyze().addError("Aucune donn�e � exporter. Le filtre utilis� � �liminer toutes les donn�es du maillage");
      }
    }
    return isOk;
  }

  public boolean isInternOk(final CtuluAnalyze _analyze, final EfGridData _data) {
    return goOn_ && _data != null && !_analyze.containsFatalError();

  }

  protected EfGridData filterToNodes(final ProgressionInterface _prog, final EfGridData _data,
                                     final CtuluAnalyze _analyze) {
    final MvExportToNodeDataActivity filterAct = new MvExportToNodeDataActivity(_data, vects_);
    setCurrentActivity(filterAct);
    return filterAct.process(_prog, _analyze);
  }

  /**
   * transforme la efgrid en volumique.
   *
   * @param _prog
   * @param _data
   * @param _analyze
   * @return
   */
  protected EfGridData filterToElements(final ProgressionInterface _prog, final EfGridData _data,
                                        final CtuluAnalyze _analyze) {
    final MvExportToElementDataActivity filterAct = new MvExportToElementDataActivity(_data, vects_);
    setCurrentActivity(filterAct);

    return filterAct.process(_prog, _analyze);
  }

  protected void addInfo(final String[] _messages, final int _nbNoeud, final int _nbEle) {
    addInfo(_messages, _nbNoeud);
    _messages[2] = MvResource.getS("Nombre d'�l�ments export�s:");
    _messages[3] = Integer.toString(_nbEle);
  }

  protected void addInfo(final String[] _messages, final int _nbNoeud) {
    _messages[0] = MvResource.getS("Nombre de noeuds export�s:");
    _messages[1] = Integer.toString(_nbNoeud);

  }

  protected final void setCurrentActivity(final CtuluActivity _currentActivity) {
    currentActivity_ = _currentActivity;
  }

  public MvExportActAbstract() {
  }
  InterpolationVectorContainer vects_;

  @Override
  public final void setSrc(final EfGridData _src, final InterpolationVectorContainer _vect) {
    src_ = _src;
    vects_ = _vect;
  }

  public boolean containsVarDefinedOnElt() {
    if (selectedVar_ == null || selectedVar_.length == 0) {
      return false;
    }
    for (int i = selectedVar_.length - 1; i >= 0; i--) {
      if (src_.isElementVar(selectedVar_[i])) {
        return true;
      }
    }
    return false;
  }

  public int getNbSelectedVar() {
    return selectedVar_ == null ? 0 : selectedVar_.length;
  }

  public int getNbSelectedTimeStep() {
    return selectedTimeStep_ == null ? 0 : selectedTimeStep_.length;
  }

  @Override
  public void setSelectedTimeStep(final double[] _idx) {
    selectedTimeStep_ = _idx;
    if (selectedTimeStep_ == null || selectedTimeStep_.length == 0) {
      selectedTimeStep_ = new double[]{0};
    }

  }

  @Override
  public void setSelectedVar(final CtuluVariable[] _idx) {
    selectedVar_ = _idx;

  }

  public EfGridData getSrc() {
    return src_;
  }

  @Override
  public final void setOperation(final EfOperation _filtrer) {
    operation_ = _filtrer;
  }
  private CtuluActivity currentActivity_;

  protected void initActivity() {
    goOn_ = true;
    currentActivity_ = null;
  }

  protected void cancelExport(final String[] _messages) {
    initActivity();
    addNullInfo(_messages);
  }

  protected void addNullInfo(final String[] _messages) {
    Arrays.fill(_messages, CtuluLibString.EMPTY_STRING);
  }

  protected abstract void doExport(ProgressionInterface _prog, File[] _dest, CtuluAnalyzeGroup _analyze,
                                   String[] _message);

  @Override
  public final void actExport(final ProgressionInterface _prog, final File[] _dest, final CtuluAnalyzeGroup _analyze,
                              final String[] _message) {
    initActivity();
    addNullInfo(_message);
    doExport(_prog, _dest, _analyze, _message);
    initActivity();
    _prog.reset();

  }

  @Override
  public final void stop() {
    goOn_ = false;
    if (currentActivity_ != null && currentActivity_ != this) {
      currentActivity_.stop();
      currentActivity_ = null;
    }
  }

  public EfGridData toNodesData(final ProgressionInterface _prog, final EfGridData _data, final CtuluAnalyze _analyze) {
    if (_data.isElementVar(H2dVariableType.BATHYMETRIE)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: transform element data to node data");
      }
      return filterToNodes(_prog, _data, _analyze);

    }
    return _data;
  }
}
