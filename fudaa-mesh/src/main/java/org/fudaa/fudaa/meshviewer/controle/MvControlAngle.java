/*
 *  @file         MvControlAngle.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.action.MvItemAction;
import org.fudaa.fudaa.meshviewer.action.MvItemActionUI;

/**
 * @author deniger
 * @version $Id: MvControlAngle.java,v 1.10 2007-01-19 13:14:15 deniger Exp $
 */
public class MvControlAngle implements MvItemAction {

  @Override
  public String getDesc() {
    return DodicoLib.getS("Contr�le des angles");
  }

  @Override
  public MvItemActionUI buildUI() {
    return new AngleUI();
  }

  final static class AngleUI extends JPanel implements MvControlItemUI {

    BuTextField ft_;

    AngleUI() {
      setLayout(new BuGridLayout(2, 10, 5));
      final BuLabel lb = new BuLabel(DodicoLib.getS("Angle minimum (en degr�)"));
      lb.setVerticalAlignment(SwingConstants.CENTER);
      add(lb);
      ft_ = BuTextField.createDoubleField();
      add(ft_);
    }

    @Override
    public MvControlResult execute(final EfGridInterface _g, final ProgressionInterface _progress) {
      final String f = ft_.getText().trim();
      final MvControlResult res = new MvControlElementResult();
      res.description_ = DodicoLib.getS("Angle inf�rieur � {0}", f);
      if (f.length() > 0) {
        double d = Double.parseDouble(f);
        if (d < 0) {
          d = -d;
        }
        final int[] r = _g.getEltIdxWithAngleLessThan(d, _progress);
        if (r != null) {
          res.idx_ = r;
        }
      } else {
        return null;
      }
      return res;
    }

    /**
     *
     */
    @Override
    public JComponent getComponent() {
      return this;
    }

  }

}
