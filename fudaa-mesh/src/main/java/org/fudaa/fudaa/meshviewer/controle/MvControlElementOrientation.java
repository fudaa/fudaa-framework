/*
 *  @file         MvControlElementOrientation.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuVerticalLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.action.MvItemAction;
import org.fudaa.fudaa.meshviewer.action.MvItemActionUI;

/**
 * @author deniger
 * @version $Id: MvControlElementOrientation.java,v 1.9 2007-01-19 13:14:15 deniger Exp $
 */
public class MvControlElementOrientation implements MvItemAction {

  @Override
  public String getDesc() {
    return FudaaLib.getS("Contr�le de l'orientation des �l�ments");
  }

  @Override
  public MvItemActionUI buildUI() {
    return new ElementOrientationUI();
  }

  private static final class ElementOrientationUI extends BuPanel implements MvControlItemUI {

    ButtonGroup bg_;

    public ElementOrientationUI() {
      bg_ = new ButtonGroup();
      setLayout(new BuVerticalLayout(5));
      BuRadioButton r = new BuRadioButton(FudaaLib.getS("Rechercher les �l�ments orient�s dans le sens horaire"));
      r.setSelected(true);
      r.setActionCommand("HORAIRE");
      bg_.add(r);

      add(r);
      r = new BuRadioButton(FudaaLib.getS("Rechercher les �l�ments orient�s dans le sens trigo"));
      bg_.add(r);
      r.setActionCommand("TRIGO");
      add(r);
    }

    /**
     *
     */
    @Override
    public MvControlResult execute(final EfGridInterface _g, final ProgressionInterface _progress) {
      final String com = bg_.getSelection().getActionCommand();
      final boolean trigo = "TRIGO".equals(com);
      final String r = FudaaLib.getS(trigo ? "Recherche des �l�ments orient�s dans le sens trigo"
          : "Recherche des �l�ments orient�s dans le sens horaire");
      if (_progress != null) {
        _progress.setDesc(r);
      }
      final int[] array = EfLib.getIdxWithOrientation(_g, trigo, _progress);
      final MvControlResult result = new MvControlElementResult();
      result.idx_ = array;
      result.description_ = FudaaLib.getS(trigo ? "El�ments orient�s dans le sens trigo"
          : "El�ments orient�s dans le sens horaire");
      return result;
    }

    /**
     *
     */
    @Override
    public JComponent getComponent() {
      return this;
    }

  }

}
