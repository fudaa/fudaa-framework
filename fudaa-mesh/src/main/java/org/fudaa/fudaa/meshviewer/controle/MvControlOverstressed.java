/*
 *  @file         MvControlConnection.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.action.MvItemAction;
import org.fudaa.fudaa.meshviewer.action.MvItemActionUI;

/**
 * @author deniger
 * @version $Id: MvControlOverstressed.java,v 1.10 2007-01-19 13:14:15 deniger Exp $
 */
public class MvControlOverstressed implements MvItemAction {

  @Override
  public String getDesc() {
    return DodicoLib.getS("El�ments surcontraints");
  }

  @Override
  public MvItemActionUI buildUI() {
    return new OverstressedUI();
  }

  final static class OverstressedUI extends JPanel implements MvControlItemUI {

    OverstressedUI() {
      setLayout(new BuGridLayout(1, 5, 0));
      add(new BuLabel(DodicoLib.getS("Recherche valable uniquement pour les triangles")));
      add(new BuLabel(DodicoLib
          .getS("Triangle surcontraint= triangle dont les 3 extr�mit�s appartiennent � une fronti�re")));
    }

    @Override
    public MvControlResult execute(final EfGridInterface _g, final ProgressionInterface _progress) {
      final MvControlResult r = new MvControlElementResult();
      r.description_ = DodicoLib.getS("El�ments surcontraints");
      r.idx_ = _g.getOverstressedElement(_progress);
      return r;
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

  }

}