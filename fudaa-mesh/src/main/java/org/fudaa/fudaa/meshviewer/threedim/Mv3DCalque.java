/*
 GPL 2
 */
package org.fudaa.fudaa.meshviewer.threedim;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.io.IOException;
import javax.media.j3d.IndexedGeometryArray;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.volume.BGrille;
import org.fudaa.ebli.volume.BGrilleIrreguliere;

/**
 *
 * @author Frederic Deniger
 */
public class Mv3DCalque extends BGrilleIrreguliere {

  BGrille bathy_;
  Color4f colorBathy_;
  double eps_ = 1E-3;
  protected int idx_;
  final EfGridData src_;
  boolean useH_;
  H2dVariableType var_;

  public Mv3DCalque(final H2dVariableType _var, final EfGridData _src) {
    super(_var.getName());
    src_ = _src;
    var_ = _var;
  }

  public static Color getBathyColor() {
    return new Color(255, 200, 100);
  }

  protected void affiche(final boolean _colorOnly) {
    try {
      final EfData cote = src_.getData(var_, idx_);
      final EfData dataForColor = useH_ ? src_.getData(H2dVariableType.HAUTEUR_EAU, idx_) : cote;

      final EfData bathy = var_ == H2dVariableType.BATHYMETRIE ? null : src_.getData(H2dVariableType.BATHYMETRIE, idx_);

      final int ptsNb = src_.getGrid().getPtsNb();
      final Point3d[] nextFigure = _colorOnly ? null : new Point3d[ptsNb];
      final Color4f[] c = new Color4f[ptsNb];
      final Color4f defaultColor = new Color4f(getCouleur());
      // permet de v�rifier qu'une valeur z a �t� modifi�e
      //allowed to check that a value has been modified
      boolean changed = false;
      final IndexedGeometryArray indexedGeometryArray = _colorOnly ? null : ((IndexedGeometryArray) shape_.getGeometry());
      for (int i = 0; i < ptsNb; i++) {
        final double value = cote.getValue(i);
        final double valueForColor = useH_ ? dataForColor.getValue(i) : value;
        if (!_colorOnly) {
          final Point3d ancien = new Point3d();
          indexedGeometryArray.getCoordinate(i, ancien);
          if (!changed) {
            changed = value != ancien.z;
          }
          nextFigure[i] = new Point3d(ancien.x, ancien.y, value);
        }
        // il faut rendre transparent le
        // we must clear
        final Color4f trans = new Color4f(0, 0, 0, 0);

        // la couleur par d�faut: pour etre sur d'en avoir une ....
        // color by default in order to be sure to get one
        c[i] = defaultColor;
        // si on utilise la hauteur, on la compare a eps_
        // sinon, on utilise cote-bathy pour la comparaison a eps_
        //If we use the heigth, we compare it to eps
        //otherwise, we use bathy cote for the comparison to eps
        if ((useH_ && valueForColor <= eps_) || (!useH_ && bathy != null && ((value - bathy.getValue(i)) <= eps_))) {
          c[i] = trans;
        } else if (!isColorUsed_ && palette_ != null) {
          Color color = palette_.getColorFor(valueForColor);
          if (color == null) {
            color = palette_.getNearestPlage(valueForColor).getCouleur();
          }
          if (color != null) {
            c[i] = new Color4f(color);
          }
        }
      }
      // on ne change la g�om�trie que si n�cessaire: op�ration couteuse.
      // we modify the geometry if necessary : costly operation
      if (!_colorOnly && changed) {
        indexedGeometryArray.setCoordinates(0, nextFigure);
      }
      setCouleurs(c);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    // u_.getCanvas3D().swap();
  }

  BGrille getBathy() {
    return bathy_;
  }

  public void setBathy(final BGrille _bathy) {
    bathy_ = _bathy;
  }

  protected H2dVariableType getVarDisplayed() {
    return useH_ ? H2dVariableType.HAUTEUR_EAU : var_;
  }

  public Color4f getBathyColorFor(final int _i) {
    if (bathy_ != null && bathy_.isVisible()) {
      return bathy_.getColorFor(_i);
    }
    if (colorBathy_ == null) {
      colorBathy_ = new Color4f(getBathyColor());
    }
    return colorBathy_;
  }

  public int getNbTimeStep() {
    return 0;
  }

  @Override
  public BPalettePlageInterface createPaletteCouleur() {
    BPalettePlage s = new BPalettePlage();
    CtuluRange r = new CtuluRange();
    getRange(r);
    if (r.max_ - r.min_ < 0.01) {
      s.initPlages(1, r.min_, r.max_);
    } else {
      s.initPlages(10, r.min_, r.max_);
    }
    return s;
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    EfData d = null;
    try {
      d = src_.getData(getVarDisplayed(), idx_);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    if (d != null) {
      d.expandTo(_b);
      return true;
    }
    return false;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return getRange(_b);
  }

  @Override
  public String getTitle() {
    return super.getName();
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }

  public boolean isUseH() {
    return useH_;
  }

  public boolean isHUsable() {
    return var_ == H2dVariableType.COTE_EAU;
  }

  @Override
  public boolean setCouleur(final Color _c) {
    final boolean r = super.setCouleur(_c);
    affiche(true);
    return r;
  }

  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    super.setPaletteCouleurPlages(_newPlage);
    affiche(true);
  }
  BPalettePlageInterface cotePalette_;
  BPalettePlageInterface hPalette_;

  public void setUseH(final boolean _useH) {
    if (useH_ == _useH) {
      return;
    }
    if (useH_ && !src_.isDefined(H2dVariableType.HAUTEUR_EAU)) {
      return;
    }
    useH_ = _useH;
    if (super.palette_ != null) {
      if (useH_) {
        cotePalette_ = palette_;
      } else {
        hPalette_ = palette_;
      }
    }
    setName(getVarDisplayed().getName());
    // la palette est utilisee
    //the palette is used
    if (isColorUsed_) {
      support_.firePropertyChange("paletteCouleur", null, palette_);
    } else {
      // la palette a ete sauvegardee
      // pour h
      // the palette has been saved for h
      if (useH_ && hPalette_ != null) {
        setPaletteCouleurPlages(hPalette_);
        // pour cote
        // for level
      } else if (useH_ || cotePalette_ == null) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("FTR: on initialise les palettes");
        }
        final int nb = palette_.getNbPlages();
        final Color min = palette_.getPlage(0).getCouleur();
        final Color max = palette_.getPlage(nb - 1).getCouleur();
        final CtuluRange r = new CtuluRange();
        getTimeRange(r);
        final BPalettePlage plages = new BPalettePlage();
        plages.initPlages(nb, r.getMin(), r.getMax(), min, max);
        setPaletteCouleurPlages(plages);

      } else {
        setPaletteCouleurPlages(cotePalette_);
        // sinon on construit
        // otherwise we build
      }
    }

  }
}