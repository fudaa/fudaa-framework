/*
 * @creation 13 mai 2005
 * 
 * @modification $Date: 2007-06-05 09:01:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.fu.FuLog;
import java.io.File;
import java.util.HashMap;

import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.dico.DicoLanguage;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinVolumeFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinWriter;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.telemac.io.TelemacCLAdapter;
import org.fudaa.dodico.telemac.io.TelemacCLFileFormat;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;

/**
 * @author Fred Deniger
 * @version $Id: MvExportActSerafin.java,v 1.12 2007-06-05 09:01:12 deniger Exp $
 */
public class MvExportActSerafin extends MvExportActGrid {

  private static class SerafinCustomPanel implements MvExportTaskSkeleton.ExportPanelCustom {

    BuCheckBox cbVolume = new BuCheckBox(MvResource.getS("Exporter au format volumique"));
    BuCheckBox cbDoublePrecision = new BuCheckBox(MvResource.getS("Exporter format double pr�cision (x,y)"));
    JPanel panel;

    public SerafinCustomPanel(boolean _withVolumePanel) {
      panel = new JPanel(new BuGridLayout(1));
      panel.add(cbDoublePrecision);
      if (_withVolumePanel)
        panel.add(cbVolume);
    }

    @Override
    public void update(MvExportActInterface target) {
      ((MvExportActSerafin) target).volumique_ = cbVolume.isSelected();
      ((MvExportActSerafin) target).xyDoublePrecision = cbDoublePrecision.isSelected();
    }

    @Override
    public JComponent getComponent() {
      return panel;
    }

    @Override
    public String getErrorMessage() {
      return null;
    }

    @Override
    public void close() {
    }
  }

  public static FudaaPanelTaskModel createSerafinTaskModel(final CtuluUI ui, final MvExportFactory _fact, final File _initFile,
          final MvSelectionNodeOrEltData _selection, boolean isVolumique, boolean xyDoublePrecision, boolean _withVolumePanel, HashMap<CtuluVariable, String> _vars2TelemacNames) {
    final FileFormat fmt;
    // -- on met l'extension qui correspond le mieux au format demand� --//
    if (!isVolumique) {
      fmt = SerafinFileFormat.getInstance();
    } else {
      fmt = SerafinVolumeFileFormat.getInstance();
    }
    final MvExportActSerafin ser = new MvExportActSerafin(_fact.initIpobo_, _fact.iparams_, _fact.getIdate(), _vars2TelemacNames);
    final MvExportTaskSkeletonWithCondLim skeleton = new MvExportTaskSkeletonWithCondLim(_fact.getDatas(), _fact.vects_, ser);
    MvExportFactory.addSingleFileChooser(skeleton, fmt, _initFile);

    // -- conditions limites --//
    MvExportFactory.addSingleFileCondLimitesChooser(skeleton, TelemacCLFileFormat.getInstance(), null);

    final MvExportTaskSkeleton.ExportPanelVarTime selection = _fact.buildVarChooser();
    ser.volumique_ = isVolumique;
    ser.xyDoublePrecision = xyDoublePrecision;
    skeleton.setPnVar(selection);
    skeleton.setPnFilter(_fact.buildFilter(_selection, selection.getTimeSelectionModel()));
    SerafinCustomPanel volumiqueCustomPanel = new SerafinCustomPanel(_withVolumePanel);
    volumiqueCustomPanel.cbVolume.setSelected(isVolumique);
    volumiqueCustomPanel.cbDoublePrecision.setSelected(xyDoublePrecision);
    skeleton.setCustom(new MvExportTaskSkeleton.ExportPanelCustom[]{volumiqueCustomPanel});
    return skeleton;
  }
  private int[] initIpobo_;
  private int[] iparams_;
  private long idate;
  private int language_ = DicoLanguage.getCurrentID();
  private boolean volumique_;
  private boolean xyDoublePrecision;
  /** Un mapping entre les variables de l'application et les noms de variable Telemac */
  private HashMap<CtuluVariable, String> vars2TelemacNames_;

  public MvExportActSerafin(final int[] _initIpobo, final int[] _iparams, final long idate, HashMap<CtuluVariable, String> _vars2TelemacNames) {
    super(SerafinFileFormat.getInstance());
    iparams_ = _iparams;
    initIpobo_ = _initIpobo;
    this.idate = idate;
    vars2TelemacNames_ = _vars2TelemacNames;

  }

  @Override
  protected CtuluIOOperationSynthese writeExport(final File[] _dest, final EfGridData _last, final ProgressionInterface _prog) {
    final MvExportSerafinFormatAdapter adapter = new MvExportSerafinFormatAdapter(selectedVar_, _last, selectedTimeStep_, language_, volumique_, vars2TelemacNames_);
    adapter.setxYdoublePrecision(xyDoublePrecision);
    if (operation_ == null && initIpobo_ != null) {
      adapter.setInitIpobo(initIpobo_);
    }
    if (iparams_ != null) {
      adapter.setIparam(iparams_);
    }
    adapter.setDate(idate);
    if (!goOn_) {
      return null;
    }

    final SerafinWriter w = SerafinFileFormat.getInstance().createSerafinWriter();
    // on d�clare l'activit� en cours
    setCurrentActivity(w);
    // on ecrit
    CtuluIOOperationSynthese serafinWriterSynth = w.write(adapter, _dest[0], _prog);

    // Si il y a pas de fichier de conditions limites, la synth�se du SerafinVriter est retourn�e.
    if (_dest.length == 1) {
      return serafinWriterSynth;
    }

    H2dTelemacCLElementSource[] cLEltSrc = new H2dTelemacCLElementSource[_last.getGrid().getFrontiers().getNbTotalPt()];

    for (int i = 0; i < cLEltSrc.length; i++) {
      cLEltSrc[i] = H2dTelemacCLElementSource.createDefault();

      // TODO Voir si mettre l'indice global du point dans le H2dTelemacCLElementSource cr�� par default.
    }

    TelemacCLAdapter cLAdapter = new TelemacCLAdapter(cLEltSrc);

    // OK
    CtuluIOOperationSynthese cLWriterSyth = TelemacCLFileFormat.write(TelemacCLFileFormat.getInstance().getLastVersionImpl(), _dest[1], cLAdapter,
            _prog);

    // c'est ok surtout que cette synthese n'est utilisee que pour afficher de beau message.
    CtuluIOOperationSynthese exportSynth = new CtuluIOOperationSynthese();

    CtuluAnalyze analyze = new CtuluAnalyze();

    analyze.merge(serafinWriterSynth.getAnalyze());
    analyze.merge(cLWriterSyth.getAnalyze());

    exportSynth.setAnalyze(analyze);
    exportSynth.setClosingOperation(serafinWriterSynth.getClosingException() + cLWriterSyth.getClosingException());
    exportSynth.setSource(serafinWriterSynth.getSource());

    return exportSynth;
  }

  @Override
  public EfGridData toDestType(final ProgressionInterface _prog, final EfGridData _data, final CtuluAnalyze _analyze) {
    final EfElementType type = _data.getGrid().getEltType();
    MvExportAdaptI act = null;

    // le cas simple
    if (type == EfElementType.T3 || type == EfElementType.T3_FOR_3D) {
      return _data;
    }
    // le cas connu
    if (type == EfElementType.T6) {
      act = new MvExportT6T3Activity(_data);
    } else {
      act = new MvExportToT3Activity(_data);
    }
    setCurrentActivity(act);
    return act.process(_prog, _analyze);

  }

  // private boolean isAllVolumique() {
  // if (selectedVar_ == null || selectedVar_.length == 0) { return false; }
  // for (int i = selectedVar_.length - 1; i >= 0; i--) {
  // if (!src_.isElementVar(selectedVar_[i])) { return false; }
  // }
  // return true;
  // }
  @Override
  public EfGridData toNodesData(final ProgressionInterface _prog, final EfGridData _data, final CtuluAnalyze _analyze) {
    if (FuLog.isTrace()) {
      FuLog.trace("TRE: transform element data to node data");
    }
    // -- Cas volumique: on transforme le tout en volumique --//
    if (volumique_) {
      return filterToElements(_prog, _data, _analyze);
    }
    return filterToNodes(_prog, _data, _analyze);
  }
}
