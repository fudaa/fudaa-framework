/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.palette.BPalettePlageAbstract;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPainter.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public interface MvIsoPainter {

  /**
   * @return le nom (identifiant) de l'iso a peindre
   */
  String getNom();

  /**
   * Dessin des iso.
   * 
   * @param _g
   * @param _versEcran
   * @param _clipReel
   */
  void paint(Graphics2D _g, GrMorphisme _versEcran, GrBoite _clipReel, int _alpha);

  /**
   * @return true si les donn�es ne sont pas continue :donn�es discretes.
   */
  boolean isDiscrete();

  /**
   * @return le min du painter
   */
  double getMin();

  /**
   * @return le max
   */
  double getMax();

  boolean isPaletteInitialized();

  /**
   * @return la palette
   */
  BPalettePlageAbstract getPalette();

  void setPalette(BPalettePlageAbstract _palette);

}