/*
 * @creation 26 sept. 06
 * @modification $Date: 2007-02-02 11:22:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.persistence;

import com.memoire.fu.FuLog;
import gnu.trove.TObjectIntHashMap;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.fudaa.ctulu.CsvWriter;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluArrayInteger;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionInteger;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalquePersistenceSingle;
import org.fudaa.ebli.calque.BCalqueSaverInterface;
import org.fudaa.ebli.calque.BCalqueSaverSingle;
import org.fudaa.ebli.calque.BCalqueSaverTargetInterface;
import org.fudaa.fudaa.meshviewer.layer.MvFrontierLayerAbstract;
import org.fudaa.fudaa.meshviewer.model.Mv3DFrontierData;

/**
 * @author fred deniger
 * @version $Id: MvFrontiere3DPersistence.java,v 1.5 2007-02-02 11:22:29 deniger Exp $
 */
public class MvFrontiere3DPersistence extends BCalquePersistenceSingle {

  @Override
  public BCalqueSaverInterface saveIn(final BCalque _cqToSave, final CtuluArkSaver _saver,
      final String _parentDirEntry, final String _parentDirIndice) {
    final BCalqueSaverSingle res = (BCalqueSaverSingle) super.saveIn(_cqToSave, _saver, _parentDirEntry,
        _parentDirIndice);
    final MvFrontierLayerAbstract layer = (MvFrontierLayerAbstract) _cqToSave;
    if (layer.getData() != null) {
      final String entry = getEntryBase(_cqToSave, _parentDirEntry, res.getId()) + ".csv";
      try {
        _saver.startEntry(entry);
        final CsvWriter writer = new CsvWriter(_saver.getOutStream());
        writer.setSepChar(' ');
        writer.writeLine("#Nombre de fronti�re interne");
        writer.writeLine("#Number of intern frontier");
        final Mv3DFrontierData data = layer.getData();
        final CtuluCollectionDouble depth = data.getElevation();
        final CtuluCollectionInteger type = data.getType();
        final int size = depth.getSize();
        writer.writeLine(CtuluLibString.getString(size));
        writer.writeLine("#Type de la frontiere;Hauteur b�timent");
        writer.writeLine("#Type of the frontier;Building Depth");
        for (int i = 0; i < size; i++) {
          writer.appendString(Mv3DFrontierData.getFileIdx(type.getValue(i)));
          writer.appendDouble(depth.getValue(i));
          writer.newLine();
        }
        writer.flush();

      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }

    return res;
  }

  @Override
  protected boolean restoreFromSpecific(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader,
      final BCalqueSaverTargetInterface _parentPanel, final BCalque _parentCalque, final String _parentDirEntry,
      final String _entryName, final ProgressionInterface _proj) {

    final BCalque cq = findCalque(_saver, _parentPanel, _parentCalque);
    if (cq == null) { return true; }
    final InputStream str = _loader.getReader(_parentDirEntry, _entryName + ".csv");
    int nbRead = 0;
    int nbFrIntern = 0;
    if (str != null) {
      final FortranReader reader = new FortranReader(new InputStreamReader(str));
      reader.setJumpBlankLine(true);
      reader.setJumpCommentLine(true);
      reader.setCommentInOneField("#");

      try {
        reader.readFields();
        nbFrIntern = reader.intField(0);
        final MvFrontierLayerAbstract layer = (MvFrontierLayerAbstract) cq;
        if (nbFrIntern != layer.getModeleCl().getFr().getNbFrontierIntern()) {
          FuLog.warning("FMV: le nombre de frontiere du fichier csv est incoh�rent lu " + nbFrIntern);
          return true;
        }
        final Mv3DFrontierData data = layer.createData();
        final CtuluArrayDouble depth = data.getElevation();
        final CtuluArrayInteger type = data.getType();
        final TObjectIntHashMap equiv = Mv3DFrontierData.getIdIdxMap();
        reader.readFields();
        String r = reader.getLine();
        while (r != null && nbRead < nbFrIntern) {
          type.set(nbRead, equiv.get(reader.stringField(0)));
          depth.set(nbRead, reader.doubleField(1));
          nbRead++;
          reader.readFields();
          r = reader.getLine();

        }

      } catch (final NumberFormatException _evt) {
        FuLog.error(_evt);
      } catch (final EOFException _evt) {

      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
    }
    if (nbRead != nbFrIntern) {
      FuLog.warning("FMV: le nombre de frontiere lu dans le  fichier csv est incoh�rent lu " + nbRead + "!= "
          + nbFrIntern);

    }
    return true;
  }
}
