/**
 *  @creation     4 d�c. 2003
 *  @modification $Date: 2008-02-20 10:11:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntIterator;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.calque.ZSelectionTrace;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.model.MvNumberDelegate;
import org.fudaa.fudaa.meshviewer.model.MvNumberLayerI;
/**
 * @author deniger
 * @version $Id: MvEdgeNumberLayer.java,v 1.12.6.1 2008-02-20 10:11:51 bmarchan Exp $
 */
public class MvEdgeNumberLayer extends ZCalqueAffichageDonnees implements MvNumberLayerI {

  private final ZModeleSegment model_;
  final MvNumberDelegate delegate_;

  public MvEdgeNumberLayer(final ZModeleSegment _m) {
    model_ = _m;
    delegate_ = new MvNumberDelegate(_m.getNombre());
    super.setTitle(MvResource.getS("Indices des ar�tes"));
  }

  @Override
  public void setItem(final int[] _idx){
    if (delegate_.setSelectedPoint(_idx)) {
      quickRepaint();
    }
  }
  
  

  @Override
  public LineString getSelectedLine() {
    return null;
  }

  @Override
  public void clearIdxShow(){
    if (delegate_.clearIdxShow()) {
      quickRepaint();
    }
  }

  @Override
  public void setAll(){
    if (delegate_.setAll()) {
      quickRepaint();
    }
  }

  @Override
  public boolean isAll(){
    return delegate_.isAll();
  }

  @Override
  public boolean isSomethingToView(){
    return delegate_.isSomethingToView();
  }

  @Override
  public boolean isPaletteModifiable(){
    return false;
  }

  @Override
  public ZModeleDonnees modeleDonnees(){
    return model_;
  }

  @Override
  public int[] getSelectedIdx(final MvLayerGrid _layer){
    return _layer == null ? null : _layer.getSelectedEdgeIdx();
  }

  @Override
  public boolean isSelectionOk(final MvLayerGrid _layer){
    return _layer == null ? false : !_layer.isSelectionEdgeEmpty();
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if (isRapide()) { return; }
    if (!delegate_.isSomethingToView()) { return; }
    final GrBoite clip = _clipReel;
    final GrSegment p = new GrSegment(new GrPoint(), new GrPoint());
    final GrMorphisme versEcran = _versEcran;
    final Font old = _g.getFont();
    final Color oldC = _g.getColor();
    _g.setFont(getFont());
    if (isAttenue()) {
      _g.setColor(attenueCouleur(getForeground()));
    } else {
      _g.setColor(getForeground());
    }
    double x, y;
    String s;

    if (isAll()) {
      final FontMetrics fm = _g.getFontMetrics();
      final Color fgColor = _g.getColor();
      final Color bgColor = getBackground();
      for (int idx = model_.getNombre() - 1; idx >= 0; idx--) {
        model_.segment(p, idx, true);
        p.milieu(p.e_);
        if (clip.contientXY(p.e_)) {
          p.autoApplique(versEcran);
          s = CtuluLibString.getString(idx + 1);
          final Rectangle2D rec = fm.getStringBounds(s, _g);
          x = p.e_.x_ - rec.getWidth() / 2;
          y = p.e_.y_ - 3;
          rec.setFrame(x, y - fm.getAscent(), rec.getWidth(), fm.getAscent() + 2);
          _g.setColor(bgColor);
          (_g).fill(rec);
          _g.setColor(fgColor);
          _g.drawString(s, (int) x, (int) y);
        }
      }
    }
    else {
      final TIntIterator it = delegate_.getIdxIterator();
      final FontMetrics fm = _g.getFontMetrics();
      final Color fgColor = _g.getColor();
      final Color bgColor = getBackground();
      //int h = g.getFontMetrics().getHeight() / 2;
      for (int i = delegate_.getNbIdxSelected(); i-- > 0;) {
        final int idx = it.next();
        model_.segment(p, idx, true);
        p.milieu(p.e_);
        if (clip.contientXY(p.e_)) {
          p.e_.autoApplique(versEcran);
          s = CtuluLibString.getString(idx + 1);
          final Rectangle2D rec = fm.getStringBounds(s, _g);
          x = p.e_.x_ - rec.getWidth() / 2;
          y = p.e_.y_ - 3;
          rec.setFrame(x, y - fm.getAscent(), rec.getWidth(), fm.getAscent() + 2);
          _g.setColor(bgColor);
          _g.fill(rec);
          _g.setColor(fgColor);
          _g.drawString(s, (int) x, (int) y);
        }
      }
    }
    _g.setFont(old);
    _g.setColor(oldC);
  }

  @Override
  public void doPaintSelection(final Graphics2D _g,final ZSelectionTrace _trace, final GrMorphisme _versEcran, final GrBoite _clipReel){}

  /**
   *
   */
  @Override
  public GrBoite getDomaineOnSelected() {
    return null;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt,final int _tolerance){
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode){
    return null;
  }

}
