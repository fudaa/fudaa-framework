/*
 * @creation 9 sept. 2005
 * 
 * @modification $Date: 2007-06-05 09:01:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.fu.FuLog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.sig.FSigLib;

/**
 * @author Fred Deniger
 * @version $Id: MvExportActX3D.java,v 1.9 2007-06-05 09:01:12 deniger Exp $
 */
public class MvExportActX3D extends MvExportActAbstract {

  private static class ExportX3DVarTimeChooser extends JPanel implements MvExportTaskSkeleton.ExportPanelVarTime {

    BuCheckBox cbH_;
    JComboBox timeBathy_;
    JComboBox timeH_;
    final FudaaCourbeTimeListModel initTime_;
    DefaultListSelectionModel select_;

    @Override
    public ListSelectionModel getTimeSelectionModel() {
      return select_;
    }

    @Override
    public void close() {}

    final void updateList() {
      select_.setValueIsAdjusting(true);
      select_.clearSelection();
      if (cbH_ != null && cbH_.isSelected() && timeH_ != null) {
        final int idx = timeH_.getSelectedIndex();
        if (idx >= 0) {
          select_.addSelectionInterval(idx, idx);
        }
      }
      if (timeBathy_ != null) {
        final int idx = timeBathy_.getSelectedIndex();
        if (idx >= 0) {
          select_.addSelectionInterval(idx, idx);
        }
      }
      select_.setValueIsAdjusting(false);
    }

    public ExportX3DVarTimeChooser(final boolean _h, final FudaaCourbeTimeListModel _timeModel) {
      initTime_ = _timeModel;
      select_ = new DefaultListSelectionModel();
      if (_h) {
        cbH_ = new BuCheckBox(FSigLib.getS("Exporter la c�te d'eau"));
        cbH_.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(final ActionEvent _e) {
            updateList();
          }

        });
      }

      if (_timeModel == null) {
        if (_h) {
          setLayout(new BuBorderLayout());
          add(cbH_);
        }
      } else {
        setLayout(new BuGridLayout(2, 4, 4));
        add(new BuLabel(H2dVariableType.BATHYMETRIE.getName()));
        timeBathy_ = new BuComboBox();
        timeBathy_.setModel(new CtuluComboBoxModelAdapter(_timeModel));
        timeBathy_.setSelectedIndex(0);
        timeBathy_.getModel().addListDataListener(new ListDataListener() {

          @Override
          public void intervalRemoved(final ListDataEvent _e) {}

          @Override
          public void intervalAdded(final ListDataEvent _e) {}

          @Override
          public void contentsChanged(final ListDataEvent _e) {
            updateList();
          }

        });

        add(timeBathy_);
        if (_h) {
          add(cbH_);
          cbH_.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(final ItemEvent _e) {
              timeH_.setEnabled(cbH_.isSelected());
            }
          });
          timeH_ = new BuComboBox();
          timeH_.setModel(new CtuluComboBoxModelAdapter(_timeModel));
          timeH_.setSelectedIndex(0);
          cbH_.setSelected(true);
          add(timeH_);
          timeH_.getModel().addListDataListener(new ListDataListener() {

            @Override
            public void intervalRemoved(final ListDataEvent _e) {}

            @Override
            public void intervalAdded(final ListDataEvent _e) {}

            @Override
            public void contentsChanged(final ListDataEvent _e) {
              updateList();
            }

          });
        }
      }
      updateList();
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public String getErrorMessage() {
      return null;
    }

    @Override
    public double[] getSelectedTimeStep() {
      if (timeBathy_ == null) { return null; }
      if (cbH_ == null) { return new double[] { initTime_.getTimeInSec(timeBathy_.getSelectedIndex()) }; }
      return new double[] { initTime_.getTimeInSec(timeBathy_.getSelectedIndex()),
          initTime_.getTimeInSec(timeH_.getSelectedIndex()) };
    }

    @Override
    public CtuluVariable[] getSelectedVar() {
      if (cbH_ != null && cbH_.isSelected()) { return new CtuluVariable[] { H2dVariableType.BATHYMETRIE,
          H2dVariableType.COTE_EAU }; }
      return new CtuluVariable[] { H2dVariableType.BATHYMETRIE };
    }

    @Override
    public EfGridData getTimeSrc(final EfGridData _init) {
      if (timeBathy_ == null) { return _init; }
      if (cbH_ != null) { return new MvExportSrcTimeAdapter(_init, new int[] { timeBathy_.getSelectedIndex(),
          timeH_.getSelectedIndex() }); }
      return new MvExportSrcTimeAdapter(_init, new int[] { timeBathy_.getSelectedIndex() });
    }
  }

  public static MvExportTaskSkeleton.ExportPanelVarTime createChooser(final EfGridData _src,
      final FudaaCourbeTimeListModel _time) {
    if (!_src.isDefined(H2dVariableType.BATHYMETRIE)) { return null; }
    return new ExportX3DVarTimeChooser(_src.isDefined(H2dVariableType.COTE_EAU), _time);

  }

  @Override
  protected void doExport(final ProgressionInterface _prog, final File[] _dest, final CtuluAnalyzeGroup _analyze,
      final String[] _message) {
    EfGridData last = src_;
    if (FuLog.isTrace()) {
      FuLog.trace("TRE: start x3d export");
    }
    last = processOperations(_prog, _analyze);
    // if (operation_ != null) {
    // final MvExportSourceFilterActivity filterAct = new MvExportSourceFilterActivity(src_, operation_);
    // setCurrentActivity(filterAct);
    // last = filterAct.buildNewAdapter(_prog, _analyze);
    // }
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("FTR: stop after filter goOne=" + goOn_ + " last null " + last);
      }
      return;
    }
    CtuluAnalyze logAdapt=new CtuluAnalyze();
    logAdapt.setDesc(MvResource.getS("Adaptation des donn�es"));
    _analyze.addAnalyzer(logAdapt);
    // s'il y a des variables definis sur les elements on les transforme en vari sur les noeuds
    if (super.containsVarDefinedOnElt()) {
      if (FuLog.isTrace()) {
        FuLog.trace("FTR: transform element data to node data");
      }
      final MvExportToNodeDataActivity filterAct = new MvExportToNodeDataActivity(last, vects_);
      setCurrentActivity(filterAct);
      last = filterAct.process(_prog, logAdapt);
    }
    if (!goOn_ || last == null || _analyze.containsFatalError()) {
      if (FuLog.isTrace()) {
        FuLog.trace("FTR: top after transofrm to node data");
      }
      return;
    }
    if (last.getGrid().getEltType() != EfElementType.T3) {
      MvExportAdaptI adapt = null;
      if (last.getGrid().getEltType() == EfElementType.T6) {
        adapt = new MvExportT6T3Activity(last);
      } else {
        adapt = new MvExportToT3Activity(last);
      }
      setCurrentActivity(adapt);
      last = adapt.process(_prog, logAdapt);
    }
    if (!isInternOk(_analyze, last)) {
      if (FuLog.isTrace()) {
        FuLog.trace("TRE: top after transform in T3");
      }
      return;
    }
    if (FuLog.isTrace()) {
      FuLog.trace("TRE: write serafin file " + _dest[0].getAbsolutePath());
    }
    final MvExportX3D x3d = new MvExportX3D(last.getGrid());
    try {
      x3d.setBathy(last.getData(selectedVar_[0], 0));
      if (selectedVar_.length == 2) {
        x3d.setH(last.getData(selectedVar_[1], 1));
      }
      x3d.doExport(_dest[0], _prog);
    } catch (final IOException e) {
      _analyze.getMainAnalyze().addError(e.getMessage());
      return;
    }
    addInfo(_message, last.getGrid().getPtsNb(), last.getGrid().getEltNb());

  }

  @Override
  public int getNbLabelInfoNeeded() {
    return 2;
  }

  @Override
  public String getTitre() {
    return "X3D";
  }

}
