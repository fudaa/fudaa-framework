/*
 * @creation 19 juin 2006
 * 
 * @modification $Date: 2007-06-05 09:01:11 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuSeparator;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.io.File;
import java.io.IOException;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.impl.EfGridBathyAdapter;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;

class MvExportCorEleBthGridAct extends MvExportActGrid {

  boolean isOld_;
  boolean isT6_;

  public boolean isOld() {
    return isOld_;
  }

  public void setOld(final boolean _isOld) {
    isOld_ = _isOld;
  }

  public MvExportCorEleBthGridAct(final FileFormatGridVersion _ft) {
    super(_ft);
  }

  @Override
  public EfGridData toDestType(final ProgressionInterface _prog, final EfGridData _data, final CtuluAnalyze _analyze) {
    final EfElementType type = _data.getGrid().getEltType();
    MvExportAdaptI act = null;
    if (isT6_) {
      if (type == EfElementType.T6) { return _data; }
      act = new MvExportT6Activity(_data);
    } else if (type == EfElementType.T3) {
      return _data;
    } else if (type == EfElementType.T6) {
      act = new MvExportT6T3Activity(_data);
    } else {
      act = new MvExportToT3Activity(_data);
    }
    setCurrentActivity(act);
    return act.process(_prog, _analyze);

  }

  MvExportTaskSkeleton.ExportPanelCustom pn_;

  MvExportTaskSkeleton.ExportPanelCustom getCustomPanel(final int _nbNode, final int _nbElt, final EfElementType _isT6) {
    if (pn_ == null) {
      pn_ = new CustomPanel(_nbNode, _nbElt, _isT6);
    }
    return pn_;
  }

  @Override
  protected CtuluIOOperationSynthese writeExport(final File[] _dest, final EfGridData _last,
      final ProgressionInterface _prog) {
    try {
      final EfGridInterface g = new EfGridBathyAdapter(_last.getData(H2dVariableType.BATHYMETRIE, 0), _last.getGrid());
      if (isOld()) { return ((CorEleBthFileFormat) ft_).writeGridSimpleColumn(_dest[0], g, _prog); }
      return ((CorEleBthFileFormat) ft_).writeGrid(_dest[0], g, _prog);
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }
    return null;

  }

  public static FudaaPanelTaskModel createCorEleBthTaskModel(final MvExportFactory _fact, final EfGridInterface _grid,
      final FileFormatGridVersion _fmt, final File _initFile, final MvSelectionNodeOrEltData _selection) {
    final MvExportCorEleBthGridAct ser = new MvExportCorEleBthGridAct(_fmt);
    final MvExportTaskSkeleton skeleton = new MvExportTaskSkeleton(_fact.getDatas(), _fact.vects_, ser);
    _fact.updateGridTaskModel(skeleton, _fmt.getFileFormat(), _initFile, _selection);
    skeleton.setCustom(new MvExportTaskSkeleton.ExportPanelCustom[] { ser.getCustomPanel(_grid.getPtsNb(), _grid
        .getEltNb(), _grid.getEltType()) });
    return skeleton;
  }

  private static class CustomPanel implements MvExportTaskSkeleton.ExportPanelCustom {

    BuCheckBox cb_ = new BuCheckBox(MvResource.getS("G�n�rer ancien format ( support� par Oasis)"));
    final BuRadioButton cbT6_;
    final BuRadioButton cbT3_;

    BuPanel pn_;

    CustomPanel(final int _nbPt, final int _nbElt, final EfElementType _type) {
      cb_.setToolTipText(MvResource.getS("Si vous voulez �diter les fichiers g�n�r�s par Oasis, cochez cette option"));
      cbT3_ = new BuRadioButton(MvResource.getS((_type == EfElementType.T3) ? "Conserver les �l�ments T3"
          : "Transformer en T3"));
      cbT3_.setToolTipText("T3: triangle compos� de 3 noeuds");
      cbT6_ = new BuRadioButton(MvResource.getS((_type == EfElementType.T6) ? "Conserver les �l�ments T6"
          : "Transformer en T6"));
      cbT6_.setToolTipText("T3: triangle compos� de 6 noeuds");
      final ButtonGroup bg = new ButtonGroup();
      bg.add(cbT3_);
      bg.add(cbT6_);
      cbT3_.setSelected(true);
      cb_.setEnabled(_nbElt < 1E6 && _nbPt < 1E6);
      if (!cb_.isEnabled()) {
        cb_.setToolTipText(MvResource.getS("Le maillage contient trop de donn�es et n'est pas support� par Oasis"));
      }
      if (_type == EfElementType.T6) {
        cbT6_.setSelected(true);
      }
    }

    @Override
    public void close() {}

    @Override
    public JComponent getComponent() {
      if (pn_ == null) {
        pn_ = new BuPanel(new BuVerticalLayout());
        pn_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Options")));
        pn_.add(cbT3_);
        pn_.add(cbT6_);
        pn_.add(new BuSeparator());
        pn_.add(cb_);
      }
      return pn_;
    }

    @Override
    public String getErrorMessage() {
      return null;
    }

    public boolean isOld() {
      return cb_.isSelected();
    }

    public boolean isT6() {
      return cbT6_.isSelected();
    }

    @Override
    public void update(final MvExportActInterface _target) {
      if (_target instanceof MvExportCorEleBthGridAct) {
        ((MvExportCorEleBthGridAct) _target).setOld(isOld());
        ((MvExportCorEleBthGridAct) _target).setT6(isT6());
      }
    }

  }

  public boolean isT6() {
    return isT6_;
  }

  public void setT6(final boolean _isT6) {
    isT6_ = _isT6;
  }
}