/*
 *  @creation     17 juin 2005
 *  @modification $Date: 2007-04-16 16:35:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id: MvExpressionSupplierElement.java,v 1.2 2007-04-16 16:35:34 deniger Exp $
 */
public class MvExpressionSupplierElement extends MvExpressionSupplierDefault {

  /**
   * le x du milieu.
   */
  Variable xMid_;
  /**
   * le y du milieu.
   */
  Variable yMid_;

  /**
   * le max x.
   */
  Variable xMax_;
  /**
   * le max y.
   */
  Variable yMax_;
  /**
   * le min x.
   */
  Variable xMin_;
  /**
   * le min y.
   */
  Variable yMin_;
  Variable angleMin_;
  Variable angleMax_;
  Variable area_;
  Variable idx_;

  /**
   * @param _nodes le calque des �l�ments
   */
  public MvExpressionSupplierElement(final MvElementModel _nodes) {
    super(_nodes);
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    idx_ = _expr.addVar("ndElt", MvResource.getS("L'indice de l'�l�ment"));
    xMid_ = _expr.addVar("eltXMid", MvResource.getS("L'abscisse du milieu de l'�l�ment"));
    yMid_ = _expr.addVar("eltYMid", MvResource.getS("L'ordonn�e du milieu de l'�l�ment"));
    xMin_ = _expr.addVar("eltXMin", MvResource.getS("L'abscisse minimale de l'�l�ment"));
    yMin_ = _expr.addVar("eltYMin", MvResource.getS("L'ordonn�e minimale de l'�l�ment"));
    xMax_ = _expr.addVar("eltXMax", MvResource.getS("L'abscisse maximale de l'�l�ment"));
    yMax_ = _expr.addVar("eltYMax", MvResource.getS("L'ordonn�e maximale de l'�l�ment"));
    angleMax_ = _expr.addVar("eltAngleMax", MvResource.getS("L'angle maximal de l'�l�ment en degr�"));
    angleMin_ = _expr.addVar("eltAngleMin", MvResource.getS("L'angle minimal de l'�l�ment en degr�"));
    area_ = _expr.addVar("eltArea", MvResource.getS("L'aire de l'�l�ment"));

  }
  
  @Override
  public void majVariable(int _idx, Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    idx_.setValue(CtuluLib.getDouble(_idx + 1));
    final MvElementModel elt = (MvElementModel) data_;
    final EfGridInterface grid = elt.getGrid();
    xMid_.setValue(CtuluLib.getDouble(grid.getCentreXElement(_idx)));
    yMid_.setValue(CtuluLib.getDouble(grid.getCentreYElement(_idx)));
    final EfElement el = grid.getElement(_idx);
    xMin_.setValue(CtuluLib.getDouble(el.getMinX(grid)));
    xMax_.setValue(CtuluLib.getDouble(el.getMaxX(grid)));
    yMin_.setValue(CtuluLib.getDouble(el.getMinY(grid)));
    yMax_.setValue(CtuluLib.getDouble(el.getMaxY(grid)));
    angleMin_.setValue(CtuluLib.getDouble(el.getMinAngleDegre(grid)));
    angleMax_.setValue(CtuluLib.getDouble(el.getMaxAngleDegre(grid)));
    area_.setValue(CtuluLib.getDouble(grid.getAire(_idx)));
  }
}
