/*
 GPL 2
 */
package org.fudaa.fudaa.meshviewer.threedim;

import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Envelope;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.vecmath.Point3d;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.volume.BCalqueBoite;
import org.fudaa.ebli.volume.BCanvas3D;
import org.fudaa.ebli.volume.BGroupeStandard;
import org.fudaa.ebli.volume.BGroupeVolume;
import org.fudaa.ebli.volume.ZVue3DPanel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.export.MvExportToNodeDataActivity;
import org.fudaa.fudaa.meshviewer.export.MvExportToT3Activity;

/**
 *
 * @author Frederic Deniger
 */
public class Mv3DFactory {

  public Mv3DFactory() {
  }

  public final void afficheFrame(final JFrame _f, final EfGridData _grid, final BGroupeCalque _cqFond,
          final CtuluUI _ui) {
    if (_grid == null) {
      return;
    }
    final CtuluTaskDelegate task = _ui.createTask("3D");
    task.start(new Runnable() {
      @Override
      public void run() {
        afficheFrameAction(_f, _grid, _ui, _cqFond, task.getStateReceiver());
      }
    });
  }

  protected final EfGridData convertGrid(final EfGridData _grid, final CtuluUI _ui, final ProgressionInterface _prog,
          final CtuluVariable[] _vars,
          final InterpolationVectorContainer _vects) {
    final EfElementType elt = _grid.getGrid().getEltType();
    EfGridData endData = _grid;
    final CtuluAnalyze res = new CtuluAnalyze();
    if (elt == null || elt != EfElementType.T3 || elt != EfElementType.T3_FOR_3D) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("FTR: 3D convert to T3");
      }
      final MvExportToT3Activity act = new MvExportToT3Activity(_grid);

      endData = act.process(_prog, res);
      try {
        EfData data = endData.getData(H2dVariableType.BATHYMETRIE, 0);
        data.getSize();
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      if (_ui.manageAnalyzeAndIsFatal(res)) {
        return null;
      }
    }
    boolean isElt = false;
    for (int i = _vars.length - 1; i >= 0; i--) {
      if (_grid.isElementVar(_vars[i])) {
        isElt = true;
        break;
      }
    }
    if (isElt) {
      final MvExportToNodeDataActivity toNode = new MvExportToNodeDataActivity(endData, _vects);
      endData = toNode.process(_prog, res);
      try {
        EfData data = endData.getData(H2dVariableType.BATHYMETRIE, 0);
        data.getSize();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return endData;
  }

  private void afficheFrameAction(final JFrame _f, final EfGridData _initSrc,
          final CtuluUI _ui,
          final BGroupeCalque _cqFond,
          final ProgressionInterface _prog) {
    // on doit avoir des T3
    //we must get T3
    H2dVariableType varBathy = H2dVariableType.BATHYMETRIE;
    boolean containsBathy = _initSrc.isDefined(varBathy);
    final EfGridData endData = convertGrid(_initSrc, _ui, _prog, new CtuluVariable[]{varBathy}, null);
    if (endData == null) {
      return;
    }
    if (_prog != null) {
      _prog.setDesc(MvResource.getS("Conversion 3D"));
    }
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    final EfGridInterface grid = endData.getGrid();
    up.setValue(5, grid.getPtsNb() * 2, 0, 50);
    Point3d[] ptsBathy = null;
    // pour telemac 3d, il peut y avoir plusieur couche
    //for telemac 3d, there can get several layers
    int idx = 0;
    boolean isAnim = false;
    final Envelope envelope = grid.getEnvelope(null);
    final GrBoite boiteBathy = new GrBoite(envelope);
    final GrBoite boiteAll = new GrBoite(envelope);
    if (containsBathy) {
      ptsBathy = create3d(endData, varBathy, idx, _ui, up, boiteBathy);
      // Erreur non g�r�e
      // Error not managed
      if (ptsBathy == null) {
        return;
      }
      boiteAll.ajuste(boiteBathy);
    }
    final int[] connect = buildConnections(up, grid);
    // on construit les calques 3D
    // We build the 3D layers
    final BGroupeVolume gv = new BGroupeVolume();
    gv.setName(EbliLib.getS("Calques"));
    // les axes
    // the axis
    final BCalqueBoite chp = new BCalqueBoite(EbliLib.getS("Axes"));
    boiteAll.factor(1.2);
    chp.setGeometrie(boiteAll);

    gv.add(chp);
    // la boite englobante xy
    // the box including xy 
    if (containsBathy) {
      final Mv3DCalque cq = createGrille(varBathy, _initSrc, endData, idx);
      cq.setBoite(boiteBathy);
      cq.setGeometrie(grid.getPtsNb(), ptsBathy, connect.length, connect);
      cq.setCouleur(Mv3DCalque.getBathyColor());
      cq.setBrillance(0);
      Mv3DImage.addTexture(_cqFond, cq, endData.getGrid());
      gv.add(cq);
    }
    afficheFrame(_f, isAnim, gv, _initSrc);
    chp.setVisible(false);

  }

  protected final int[] buildConnections(final ProgressionUpdater _up, final EfGridInterface _grid) {
    final int nbPtParEle = _grid.getEltType().getNbPt();
    final int[] connect = new int[_grid.getEltNb() * nbPtParEle];
    _up.setValue(5, _grid.getPtsNb(), 50, 50);
    for (int i = 0; i < _grid.getEltNb(); i++) {
      final EfElement element = _grid.getElement(i);
      for (int k = 0; k < nbPtParEle; k++) {
        connect[nbPtParEle * i + k] = element.getPtIndex(k);
      }
    }
    return connect;
  }

  protected final void afficheFrame(final JFrame _f, final boolean _isAnim, final BGroupeVolume _gv, final EfGridData _grid) {
    final ZVue3DPanel fille = new ZVue3DPanel(null);
    fille.setRoot(initGroupeStandard(_gv));
    fille.build(_isAnim);
    fille.getUnivers().init();
    initFille(fille, _grid, _isAnim);

    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        fille.updateFrame(_f);
        _f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        _f.setContentPane(fille);
        _f.pack();
        _f.setVisible(true);
        fille.getUnivers().getCanvas3D().requestFocusInWindow();
      }
    });
  }

  protected final BGroupeStandard initGroupeStandard(final BGroupeVolume _gv) {
    final BGroupeStandard gs = new BGroupeStandard();
    gs.setName(EbliLib.getS("Univers"));
    gs.add(_gv);
    _gv.setAllVisible();
    gs.add(BCanvas3D.getGroupeLumiere(_gv));
    gs.setVisible(true);
    return gs;
  }

  protected Mv3DCalque createGrille(final H2dVariableType _var, final EfGridData _init, final EfGridData _end, final int _idx) {
    Mv3DCalque r = new Mv3DCalque(_var, _end);
    r.setVisible(true);
    r.setRapide(false);
    r.setEclairage(true);
    return r;

  }

  protected final Point3d[] create3d(final EfGridData _grid, final H2dVariableType _var, final int _timeIdx, final CtuluUI _ui,
          final ProgressionUpdater _up, final GrBoite _r) {
    final EfGridInterface grid = _grid.getGrid();
    final Point3d[] pts = new Point3d[grid.getPtsNb()];
    EfData data;

    try {
      data = _grid.getData(_var, _timeIdx);
      final CtuluRange r = new CtuluRange();
      data.expandTo(r);
      _r.e_.z_ = r.max_;
      _r.o_.z_ = r.min_;
      if (Math.abs(_r.e_.z_ - _r.o_.z_) < 1E-1) {
        _r.e_.z_ = _r.o_.z_ + 1;
      }
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      _ui.error("3D", _evt.getMessage(), false);
      return null;

    }
    for (int i = 0; i < grid.getPtsNb(); i++) {
      pts[i] = new Point3d(grid.getPtX(i), grid.getPtY(i), data.getValue(i));
      _up.majAvancement();
    }
    return pts;
  }

  protected void initFille(final ZVue3DPanel fille, final EfGridData _grid, final boolean _isAnim) {
  }
}
