/*
 * @creation 9 sept. 2005
 *
 * @modification $Date: 2007-06-05 09:01:12 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuComparator;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gis.exporter.GISExportDataStoreFactory;
import org.fudaa.ctulu.gis.exporter.GISFileFormat;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.corelebth.CorEleBthFileFormat;
import org.fudaa.dodico.ef.io.dunes.DunesMAIFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinVolumeFileFormat;
import org.fudaa.dodico.ef.io.trigrid.TrigridFileFormat;
import org.fudaa.dodico.rubar.io.RubarDATFileFormat;
import org.fudaa.dodico.rubar.io.RubarMAIFileFormat;
import org.fudaa.fudaa.commun.courbe.FudaaCourbeTimeListModel;
import org.fudaa.fudaa.commun.impl.FudaaGuiLib;
import org.fudaa.fudaa.commun.impl.FudaaPanelTask;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.MvSelectionNodeOrEltData;
import org.fudaa.fudaa.sig.layer.FSigVisuPanel;
import org.geotools.data.FileDataStoreFactorySpi;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: MvExportFactory.java,v 1.23 2007-06-05 09:01:12 deniger Exp $
 */
public class MvExportFactory {
  private String additionalText;
  protected final EfGridData datas_;
  protected Map<BuFileFilter, GISFileFormat> gisFormat_;
  protected Map gridFormat_;
  private long idate;
  int[] initIpobo_;
  int[] iparams_;
  private final FSigVisuPanel panel;
  BuFileFilter rubarDatFileFilter;
  /**
   * R�serv� aux volumes finis
   */
  BuFileFilter serafinVolume_;
  BuFileFilter serafin_;
  protected final FudaaCourbeTimeListModel time_;
  final CtuluUI ui;
  protected final ListModel vars_;
  protected final InterpolationVectorContainer vects_;
  BuFileFilter x3d_;
  private final int nbBlockConcentration;

  /**
   * @param _vars
   * @param _time
   * @param _datas
   */
  public MvExportFactory(final CtuluUI ui, final ListModel _vars, final FudaaCourbeTimeListModel _time, final EfGridData _datas,
                         final InterpolationVectorContainer _vects, FSigVisuPanel panel, int nbBlockConcentration) {
    super();
    this.panel = panel;
    vars_ = _vars;
    time_ = _time;
    datas_ = _datas;
    vects_ = _vects;
    this.ui = ui;
    this.nbBlockConcentration = nbBlockConcentration;
    assert ui != null;
  }

  public int getNbBlockConcentration() {
    return nbBlockConcentration;
  }

  public static void addSingleFileChooser(final MvExportTaskSkeleton _skeleton, final FileFormat _fmt, final File _initFile) {
    final MvExportTaskSkeleton.ExportPanelFileChooserSingle file = new MvExportTaskSkeleton.ExportPanelFileChooserSingle(CtuluLib.getS("Fichier:"),
        _initFile, _fmt.getExtensions()[0], new FileFilter[]{_fmt.createFileFilter()});
    file.setExtensionsCreated(_fmt.getLinkedExtension());
    file.setBorder(BuBorders.EMPTY2222);
    _skeleton.setPnFile(file);
  }

  public static void addSingleFileCondLimitesChooser(final MvExportTaskSkeletonWithCondLim _skeleton, final FileFormat _fmt, final File _initFile) {
    // FIXME attention pour les ressource utilis�s celle de Mv. On utilise des fois celle de ctulu car on sait que la
    // traduction est deja
    // faite mais c'est dangereux et p�nible: on ne maitrise pas directement les fichiers de ctulu
    final MvExportTaskSkeleton.ExportPanelFileChooserSingle file = new MvExportTaskSkeleton.ExportPanelFileChooserSingle(
        MvResource.getS("Fichier cond. limites:"), _initFile, _fmt.getExtensions()[0], new FileFilter[]{_fmt.createFileFilter()});
    file.setExtensionsCreated(_fmt.getLinkedExtension());
    file.setBorder(BuBorders.EMPTY2222);
    _skeleton.setPnFileCondLimites(file);
  }

  public static FileFormat[] getT3Format() {
    // serafin et rubar mai sont g�r� differemment
    return new FileFormat[]{DunesMAIFileFormat.getInstance(), TrigridFileFormat.getInstance(), CorEleBthFileFormat.getInstance()};
  }

  public static void startExport(final MvExportFactory _factory, final CtuluUI _impl, final MvSelectionNodeOrEltData _selection) {

    final BuFileFilter[] filters = _factory.createFilter();
    final CtuluFileChooser fileChooser = FudaaGuiLib.getFileChooser(MvResource.getS("Export"), filters, null);
    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setText(_factory.additionalText);
    fileChooser.setFileFilter(_factory.getPreferredFileFilter());
    final File initFile = FudaaGuiLib.chooseFile(_impl.getParentComponent(), true, fileChooser);
    if (initFile == null) {
      return;
    }
    final FudaaPanelTaskModel task = _factory.createExporter(initFile, (BuFileFilter) fileChooser.getFileFilter(), _selection);
    if (task != null) {
      new FudaaPanelTask(_impl, task).afficheDialog();
    }
  }

  public String getAdditionalText() {
    return additionalText;
  }

  public void setAdditionalText(String additionalText) {
    this.additionalText = additionalText;
  }

  protected MvExportTaskSkeleton.ExportPanelVarTime buildBathySingleTimeChooser() {
    return new MvExportTaskSkeleton.ExportPaneVarTimelSingleChooser(time_);
  }

  protected MvExportPanelFilter buildFilter(final MvSelectionNodeOrEltData _selection, final ListSelectionModel _time) {
    final MvExportOperationItem[] item = new MvExportOperationItem[1];
    item[0] = new MvExportPanelFilterDefault.ExportFilterItemSelection(_selection);
    final MvExportPanelFilterDefault panel = new MvExportPanelFilterDefault(item);
    panel.setBorder(MvExportTaskSkeleton.getFilterBorder());
    return panel;
  }

  protected MvExportTaskSkeleton.ExportPanelVarTimeChooser buildVarChooser() {
    return new MvExportTaskSkeleton.ExportPanelVarTimeChooser(vars_, time_);
  }

  public FudaaPanelTaskModel createExporter(final File _initFile, final BuFileFilter _filter, final MvSelectionNodeOrEltData _selection) {
    if (_filter == serafin_ || _filter == serafinVolume_) {
      // -- on retourne un export serafin normal ou volumique selon que l'extension est volume fini --//
      return MvExportActSerafin.createSerafinTaskModel(ui, this, _initFile, _selection, (_filter == serafinVolume_), true, true, null);
    } else if (_filter == x3d_) {
      return MvExportX3D.createX3dTaskModel(this, _initFile, _selection);
    } else if (_filter == rubarDatFileFilter) {
      return MvExportRubarTaskSkeleton.createRubarMaiTaskModel(this, false, RubarMAIFileFormat.getInstance(), _initFile, _selection, nbBlockConcentration);
    }
    final FileFormat fmt = (FileFormat) gridFormat_.get(_filter);
    if (fmt instanceof CorEleBthFileFormat) {
      return MvExportCorEleBthGridAct.createCorEleBthTaskModel(this, datas_.getGrid(), (FileFormatGridVersion) fmt, _initFile, _selection);
    }
    if (fmt != null) {
      return createGridTaskModel(false, (FileFormatGridVersion) fmt, _initFile, _selection);
    }
    final GISFileFormat spi = gisFormat_.get(_filter);
    if (spi != null) {
      return createSIGTaskModel(spi, _initFile, _selection);
    }
    return null;
  }

  public BuFileFilter[] createFilter() {
    if (gisFormat_ == null) {
      gisFormat_ = GISExportDataStoreFactory.buildFileFilterMapExcludeGML();
      if (gisFormat_.size() == 0) {
        FuLog.trace("no gis format to export");
      }
      serafin_ = SerafinFileFormat.getInstance().createFileFilter();
      serafinVolume_ = SerafinVolumeFileFormat.getInstance().createFileFilter();
      x3d_ = new BuFileFilter("x3d");
      rubarDatFileFilter = RubarDATFileFormat.getInstance().createFileFilter();
      rubarDatFileFilter.setDescription("Rubar dat, mai, ima, cox, inx,...");
      gridFormat_ = new HashMap();
      final FileFormat[] ft = getT3Format();
      for (int i = 0; i < ft.length; i++) {
        gridFormat_.put(ft[i].createFileFilter(), ft[i]);
      }
    }
    final List filtersList = new ArrayList(gisFormat_.size() + gridFormat_.size() + 3);
    filtersList.add(serafin_);
    filtersList.add(serafinVolume_);
    filtersList.add(rubarDatFileFilter);
    filtersList.add(x3d_);
    filtersList.addAll(gisFormat_.keySet());
    filtersList.addAll(gridFormat_.keySet());
    final BuFileFilter[] filters = new BuFileFilter[filtersList.size()];
    filtersList.toArray(filters);
    Arrays.sort(filters, FuComparator.STRING_COMPARATOR);
    return filters;
  }

  protected FudaaPanelTaskModel createGridTaskModel(final boolean _isT6, final FileFormatGridVersion _fmt, final File _initFile,
                                                    final MvSelectionNodeOrEltData _selection) {
    final MvExportActGrid ser = _isT6 ? new MvExportActGrid.ToT6(_fmt) : new MvExportActGrid(_fmt);
    final MvExportTaskSkeleton skeleton = new MvExportTaskSkeleton(datas_, vects_, ser);
    updateGridTaskModel(skeleton, _fmt.getFileFormat(), _initFile, _selection);
    return skeleton;
  }

  protected FudaaPanelTaskModel createSIGTaskModel(final GISFileFormat _store, final File _initFile,
                                                   final MvSelectionNodeOrEltData _selection) {
    final String[] titre = new String[3];
    final String[] suffixe = new String[3];
    titre[0] = MvResource.getS("Noeuds");
    suffixe[0] = "_nds";
    titre[1] = MvResource.getS("Elements");
    suffixe[1] = "_elts";
    titre[2] = MvResource.getS("Fronti�res");
    suffixe[2] = "_fr";
    final MvExportTaskSkeleton skeleton = new MvExportTaskSkeleton(datas_, vects_, new MvExportActDataStoreSrc(_store));
    final MvExportTaskSkeleton.ExportPanelVarTimeChooser selection = buildVarChooser();
    skeleton.setPnVar(selection);
    skeleton.setPnFilter(buildFilter(_selection, selection.getTimeSelectionModel()));
    skeleton.setPnFile(new MvExportTaskSkeleton.ExportPanelFileChooserMulti(titre, suffixe, _initFile, CtuluLibFile.getCorrectExtension(_store
        .getExtension()), new FileFilter[]{new BuFileFilter(CtuluLibFile.getCorrectExtension(_store.getExtension()))}));

    return skeleton;
  }

  public EfGridData getDatas() {
    return datas_;
  }

  public EfGridInterface getGrid() {
    return datas_.getGrid();
  }

  public int[] getInitIpobo() {
    return initIpobo_;
  }

  public void setInitIpobo(final int[] _initIpobo) {
    initIpobo_ = _initIpobo;
  }

  public int[] getIparams() {
    return iparams_;
  }

  public void setIparams(final int[] _iparams) {
    iparams_ = _iparams;
  }

  public FSigVisuPanel getPanel() {
    return panel;
  }

  protected BuFileFilter getPreferredFileFilter() {
    return serafin_;
  }

  public FudaaCourbeTimeListModel getTime() {
    return time_;
  }

  public CtuluUI getUi() {
    return ui;
  }

  public void updateGridTaskModel(final MvExportTaskSkeleton _skeleton, final FileFormat _fmt, final File _initFile,
                                  final MvSelectionNodeOrEltData _selection) {
    addSingleFileChooser(_skeleton, _fmt, _initFile);
    ListSelectionModel listSelection = null;
    if (time_ != null && time_.getSize() > 0) {
      final MvExportTaskSkeleton.ExportPanelVarTime selection = buildBathySingleTimeChooser();
      selection.getComponent().setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Pas de temps")));
      _skeleton.setPnVar(selection);
      listSelection = selection.getTimeSelectionModel();
    }
    _skeleton.setPnFilter(buildFilter(_selection, listSelection));
  }

  /**
   * @return the idate
   */
  public long getIdate() {
    return idate;
  }

  /**
   * @param idate the idate to set
   */
  public void setIdate(long idate) {
    this.idate = idate;
  }
}
