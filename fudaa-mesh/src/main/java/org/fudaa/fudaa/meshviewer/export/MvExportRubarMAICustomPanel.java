package org.fudaa.fudaa.meshviewer.export;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.fudaa.meshviewer.MvResource;

import javax.swing.*;
import java.awt.*;

/**
 * @author Fred Deniger
 * @version $Id: TrExportRubarInxCoxAct.java,v 1.18 2007-05-04 14:01:54 deniger Exp $
 */
public class MvExportRubarMAICustomPanel implements MvExportTaskSkeleton.ExportPanelCustom {
  final BuCheckBox cbToTriangle = new BuCheckBox(MvResource.getS("Transfomer les �l�ments en triangle"));
  final BuCheckBox cbExportFileCOFDurSed = new BuCheckBox(MvResource.getS("G�n�rer les fichiers COF, DUR et SED"));
  final BuCheckBox cbExportFileCOX = new BuCheckBox(MvResource.getS("G�n�rer le fichier COX (cote de fond)"));
  final BuCheckBox cbExportFileFRT = new BuCheckBox(MvResource.getS("G�n�rer le fichier de frottement FRT et diffusion DIF"));
  final BuCheckBox cbExportFileFRX = new BuCheckBox(MvResource.getS("G�n�rer le fichier FRX (frottement)"));
  final BuCheckBox cbExportFileINX = new BuCheckBox(MvResource.getS("G�n�rer le fichier INX (conditions initiales)"));
  final BuCheckBox cbExportFileMAI = new BuCheckBox(MvResource.getS("G�n�rer le fichier de maillage MAI"));
  final BuCheckBox cbExportFileIMA = new BuCheckBox(MvResource.getS("G�n�rer le fichier de maillage IMA"));
  final BuCheckBox cbExportFileDAT = new BuCheckBox(MvResource.getS("G�n�rer le fichier de maillage DAT"));
  final BuComboBox cbFormat = new BuComboBox(H2dRubarFileType.getConstantArray());
  final BuCheckBox cbInxuseHe_ = new BuCheckBox(MvResource.getS("Fichier INX: exporter la hauteur d'eau"));
  final int nbBlockConcentration;
  JPanel pn_;

  public MvExportRubarMAICustomPanel(EfGridData data, int nbBlockConcentration) {
    cbInxuseHe_.setToolTipText("<html><body>" + MvResource.getS("Le fichier INX contient soit la cote d'eau soit la hauteur.") + "<br>"
        + MvResource.getS("Par d�faut, la cote d'eau est utilis�e.") + "</body></html>");
    cbExportFileCOX.setEnabled(MvExportRubarMAI.isCoxEnable(data));
    cbExportFileCOX.setSelected(cbExportFileCOX.isEnabled());
    cbExportFileCOFDurSed.setEnabled(cbExportFileCOX.isEnabled());
    cbExportFileCOFDurSed.setSelected(cbExportFileCOFDurSed.isEnabled());
    cbExportFileMAI.setEnabled(true);
    cbExportFileMAI.setSelected(true);
    cbExportFileIMA.setEnabled(true);
    cbExportFileIMA.setSelected(true);
    cbExportFileDAT.setEnabled(true);
    cbExportFileDAT.setSelected(true);
    boolean inxEnable = MvExportRubarMAI.isInxEnable(data);
    cbExportFileINX.setEnabled(inxEnable);
    cbExportFileINX.setSelected(inxEnable);
    cbInxuseHe_.setEnabled(inxEnable);

    this.nbBlockConcentration = nbBlockConcentration;
    boolean frxEnable = MvExportRubarMAI.isFrxEnable(data);
    cbExportFileFRX.setEnabled(frxEnable);
    cbExportFileFRX.setSelected(frxEnable);

    cbExportFileFRT.setEnabled(data.isDefined(H2dVariableType.COEF_FROTTEMENT_FOND));
    cbExportFileFRT.setSelected(cbExportFileFRT.isEnabled());
  }

  @Override
  public void close() {
  }

  @Override
  public JComponent getComponent() {
    if (pn_ == null) {
      pn_ = new BuPanel(new BuVerticalLayout(5));
      pn_.setBorder(CtuluLibSwing.createTitleBorder(CtuluLib.getS("Options")));
      pn_.add(cbFormat);
      pn_.add(new BuSeparator());
      pn_.add(cbToTriangle);
      pn_.add(new BuSeparator());
      pn_.add(cbExportFileDAT);
      pn_.add(cbExportFileMAI);
      pn_.add(cbExportFileIMA);
      pn_.add(new BuSeparator());
      pn_.add(cbExportFileCOFDurSed);
      pn_.add(cbExportFileFRT);
      pn_.add(new BuSeparator());
      pn_.add(cbExportFileCOX);
      pn_.add(cbExportFileFRX);
      pn_.add(cbExportFileINX);
      cbInxuseHe_.setMargin(new Insets(0, 5, 0, 2));
      pn_.add(cbInxuseHe_);
    }
    return pn_;
  }

  @Override
  public String getErrorMessage() {
    return null;
  }

  public boolean isCofDurSedGenerated() {
    return cbExportFileCOFDurSed.isEnabled() && cbExportFileCOFDurSed.isSelected();
  }

  public boolean isCox() {
    return cbExportFileCOX.isEnabled() && cbExportFileCOX.isSelected();
  }

  public boolean isFrtAndDif() {
    return cbExportFileFRT.isEnabled() && cbExportFileFRT.isSelected();
  }

  public boolean isFrx() {
    return cbExportFileFRX.isEnabled() && cbExportFileFRX.isSelected();
  }

  public boolean isInx() {
    return cbExportFileINX.isEnabled() && cbExportFileINX.isSelected();
  }

  public boolean isMai() {
    return cbExportFileMAI.isEnabled() && cbExportFileMAI.isSelected();
  }

  public boolean isIma() {
    return cbExportFileIMA.isEnabled() && cbExportFileIMA.isSelected();
  }

  public boolean isDat() {
    return cbExportFileDAT.isEnabled() && cbExportFileDAT.isSelected();
  }

  @Override
  public void update(final MvExportActInterface _target) {
    if (_target instanceof MvExportRubarMAI) {
      final MvExportRubarMAI act = (MvExportRubarMAI) _target;
      act.setFrxGenerated(isFrx());
      act.setCoxGenerated(isCox());
      act.setInxGenerated(isInx());
      act.setMaiGenerated(isMai());
      act.setImaGenerated(isIma());
      act.setDatGenerated(isDat());
      act.setCofDurSedGenerated(isCofDurSedGenerated());
      act.setFrtAndDifGenerated(isFrtAndDif());
      act.setToTriangle(cbToTriangle.isSelected());
      act.setInxUseH(cbInxuseHe_.isEnabled() && cbInxuseHe_.isSelected());
      act.setNbBlockConcentration(nbBlockConcentration);
      act.setFileType((H2dRubarFileType) cbFormat.getSelectedItem());
    }
  }
}
