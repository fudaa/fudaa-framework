/*
 * @creation 25 avr. 07
 * @modification $Date: 2007-04-26 14:36:33 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer;

/**
 * @author fred deniger
 * @version $Id: MvSelectionNodeOrEltData.java,v 1.1 2007-04-26 14:36:33 deniger Exp $
 */
public class MvSelectionNodeOrEltData {
  
  public int[] idxSelected_;
  public boolean isElement_;

}
