/**
 *  @creation     13 juil. 2004
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.calque.find.CalqueFindFlecheExpression;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;

/**
 * @author Fred Deniger
 * @version $Id: MVFlecheLayer.java,v 1.13 2007-01-19 13:14:08 deniger Exp $
 */
public abstract class MVFlecheLayer extends ZCalqueFleche implements MvLayerGrid {

  public MVFlecheLayer(final ZModeleSegment _model) {
    modele_ = _model;
  }

  @Override
  public int[] getSelectedEdgeIdx() {
    return null;
  }

  @Override
  public int[] getSelectedElementIdx() {
    return null;
  }

  @Override
  public boolean isSelectionEdgeEmpty() {
    return true;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    return true;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new CalqueFindFlecheExpression(modele_, true);
  }

}