/*
 * @creation 13 nov. 2003
 * 
 * @modification $Date: 2008-02-20 10:11:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.layer;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.ef.operation.EfIndexVisitorHashSet;
import org.fudaa.ebli.calque.ZCalquePolygone;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.model.MvElementModel;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierElement;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

import java.awt.*;
import java.util.Arrays;

/**
 * @author deniger
 * @version $Id: MvElementLayer.java,v 1.31.6.1 2008-02-20 10:11:50 bmarchan Exp $
 */
public class MvElementLayer extends ZCalquePolygone implements MvLayerGrid {
  
  public MvElementLayer() {
    super();
    iconModel_ = null;
  }
  
  public final static int[] getSelectedEdgeIdx(final EfGridVolumeInterface _grid, final CtuluListSelectionInterface _select) {
    if (_grid == null || _select == null) {
      return null;
    }
    final TIntHashSet set = new TIntHashSet();
    final int max = _select.getMaxIndex();
    for (int i = _select.getMinIndex(); i <= max; i++) {
      if (_select.isSelected(i)) {
        final EfElementVolume elt = _grid.getEltVolume(i);
        elt.fillWithAreteIdx(set);
      }
    }
    final int[] res = set.toArray();
    Arrays.sort(res);
    return res;
  }
  
  public final static int[] getSelectedEdgeIdx(final EfGridVolumeInterface _grid, final int[] _select) {
    if (_grid == null || _select == null) {
      return null;
    }
    final TIntHashSet set = new TIntHashSet();
    for (int i = _select.length - 1; i >= 0; i--) {
      final EfElementVolume elt = _grid.getEltVolume(_select[i]);
      elt.fillWithAreteIdx(set);
    }
    final int[] res = set.toArray();
    Arrays.sort(res);
    return res;
  }
  
  @Override
  public int[] getSelectedEdgeIdx() {
    if (((MvElementModel) modele_).getGrid() instanceof EfGridVolumeInterface) {
      return getSelectedEdgeIdx((EfGridVolumeInterface) ((MvElementModel) modele_).getGrid(), getLayerSelection());
    }
    return null;
  }
  
  @Override
  public boolean isSelectionEdgeEmpty() {
    return isSelectionEmpty();
  }

  /**
   * @param _modele
   */
  public MvElementLayer(final MvElementModel _modele) {
    super(_modele);
  }
  
  @Override
  public EbliFindActionInterface getFinder() {
    return new MvFindActionNodeElt(this);
  }
  
  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    final EbliFindExpressionContainerInterface i = ((MvElementModel) modele_).getExpressionContainer();
    return i == null ? new MvExpressionSupplierElement((MvElementModel) modele_) : i;
  }

  /**
   * Ne dessine que les donn�es.
   *
   * @param _g le graphics cible
   */
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final boolean rapide = isRapide();
    final int nombre = modele_.getNombre();
    // pour ne pas modifier le model par defaut
    final TraceIcon icone = iconModel_ == null ? null : new TraceIcon(new TraceIconModel(iconModel_));
    int incrementPt = 1;
    final TraceLigne tl = new TraceLigne(new TraceLigneModel(ligneModel_));
    if (isAttenue()) {
      tl.setCouleur(attenueCouleur(tl.getCouleur()));
      if (icone != null) {
        icone.setCouleur(attenueCouleur(icone.getCouleur()));
      }
    }
    if (EbliLib.isAlphaChanged(alpha_)) {
      tl.setCouleur(EbliLib.getAlphaColor(tl.getCouleur(), alpha_));
      if (icone != null) {
        icone.setCouleur(EbliLib.getAlphaColor(icone.getCouleur(), alpha_));
      }
      
    }
    
    final MvElementModel model = (MvElementModel) modele_;
    final boolean containsFr = model.getFrNbFrontier() > 0;
    Envelope envReel = _clipReel.getEnv();
    //will contain x,y of the polygones.
    int[] x = null;
    int[] y = null;
    GrPoint pt = new GrPoint();
    int w = getWidth() + 1;
    CtuluListSelection memory = new CtuluListSelection(w * getHeight());
    
    for (int idxElement = 0; idxElement < nombre; idxElement++) {
      if (!model.isPainted(idxElement)) {
        continue;
      }
      //we ignore element out of the view:
      Envelope envelopeElement = model.getEnvelopeForElement(idxElement);
      if (!envReel.intersects(envelopeElement)) {
        continue;
      }
      final int nbPt = model.getNbPt(idxElement);
      if (x == null || x.length < nbPt) {
        x = new int[nbPt];
      }
      if (y == null || y.length < nbPt) {
        y = new int[nbPt];
      }
      boolean eltIsOnePoint = true;
      for (int iPt = 0; iPt < nbPt; iPt++) {
        pt.x_ = model.getX(idxElement, iPt);
        pt.y_ = model.getY(idxElement, iPt);
        pt.z_ = 0;
        pt.autoApplique2D(_versEcran);
        final int xi = (int) pt.x_;
        final int yi = (int) pt.y_;
        x[iPt] = xi;
        y[iPt] = yi;
        if (iPt > 0 && eltIsOnePoint) {
          eltIsOnePoint = (xi == x[iPt - 1] && yi == y[iPt - 1]);
        }
      }
      
      updateTrace(icone == null ? null : icone.getModel(), tl.getModel(), idxElement);
      if (eltIsOnePoint) {
        int idxDone = x[0] + y[0] * w;
        if (idxDone >= 0 && !memory.isSelected(idxDone)) {
          memory.add(idxDone);
          if (icone != null) {
            icone.paintIconCentre(this, _g, x[0], y[0]);
          }
          tl.dessineTrait(_g, x[0], y[0], x[0], y[0]);
        }
      } else {
        int ptOri = 0;
        int idxFrom = model.getIndice(idxElement, 0);
        int idxTo = -1;
        for (int j = nbPt - 1; j >= 0; j -= incrementPt) {
          // le point de dest est initialise
          idxTo = model.getIndice(idxElement, j);
          if (!containsFr || idxTo > idxFrom) {
            if (icone != null) {
              icone.paintIconCentre(this, _g, x[ptOri], y[ptOri]);
            }
            boolean onePoint = (x[ptOri] == x[j]) && (y[ptOri] == y[j]);
            if (onePoint) {
              int idxToDo = x[j] + y[j] * w;
              if (!memory.isSelected(idxToDo)) {
                tl.dessineTrait(_g, x[ptOri], y[ptOri], x[j], y[j]);
                memory.add(idxToDo);
              }
            } else {
              tl.dessineTrait(_g, x[ptOri], y[ptOri], x[j], y[j]);
            }
          }
          ptOri = j;
          idxFrom = idxTo;
        }
      }
    }
    GrPoint ptOri = new GrPoint();
    GrPoint ptDest = new GrPoint();
    for (int i = model.getFrNbFrontier() - 1; i >= 0; i--) {
      int idxFrom = model.getFrGlobalIdx(i, 0);
      int idxTo = -1;
      for (int j = model.getFrNbPointInFrontier(i) - 1; j >= 0; j--) {
        idxTo = model.getFrGlobalIdx(i, j);
        if (idxFrom > idxTo) {
          model.point(idxFrom, ptOri);
          model.point(idxTo, ptDest);
          if (_clipReel.intersectXYBoite(ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_)) {
            ptDest.autoApplique2D(versEcran);
            ptOri.autoApplique2D(versEcran);
            if (icone != null) {
              icone.paintIconCentre(this, _g, ptOri.x_, ptOri.y_);
            }
            tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
          }
        }
        idxFrom = idxTo;
      }
      
    }
  }
  
  @Override
  public int[] getSelectedElementIdx() {
    return selection_ == null ? null : selection_.getSelectedIndex();
  }
  
  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedElementIdx();
  }
  
  protected EfGridInterface getGrid() {
    return ((MvElementModel) modele_).getGrid();
  }
  
  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return getGrid().getIndex() == null ? super.selection(_pt, _tolerance) : selectionFromIndex(_pt, _tolerance);
  }
  
  public CtuluListSelection selectionFromIndex(final LinearRing _poly, final int _mode) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FMV: layer select from index");
    }
    final EfIndexVisitorHashSet list = new EfIndexVisitorHashSet();
    final Envelope envelopeInternal = _poly.getEnvelopeInternal();
    getGrid().getIndex().query(envelopeInternal, list);
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_poly);
    final int[] nds = list.getResult();
    final CtuluListSelection r = creeSelection();
    final Coordinate c = new Coordinate();
    final GrPolygone poly = new GrPolygone();
    final GrBoite b = new GrBoite();
    for (int i = nds.length - 1; i >= 0; i--) {
      final int idx = nds[i];
      modele().polygone(poly, idx, true);
      poly.boite(b);
      if (VecteurGrPoint.estSelectionneEnv(c, envelopeInternal, _poly, tester, poly.sommets_, _mode)) {
        r.add(idx);
      }
    }
    
    return r;
  }
  
  private CtuluListSelection selectionFromIndex(final GrPoint _pt, final int _tolerance) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FMV: layer select from index");
    }
    return EfIndexHelper.getNearestElement(getGrid(), _pt.x_, _pt.y_, GrMorphisme.convertDistanceXY(getVersReel(), _tolerance), null);
    // final EfIndexVisitorNearestElt visitor = new EfIndexVisitorNearestElt(grid, _pt.x_, _pt.y_, toleranceReel);
    // grid.getIndex().query(EfIndexVisitorNearestNode.getEnvelope(_pt.x_, _pt.y_, toleranceReel), visitor);
    // return visitor.getSelection();
  }
  
  @Override
  public CtuluListSelection selection(final LinearRing _polySelection, final int _mode) {
    return getGrid().getIndex() == null ? super.selection(_polySelection, _mode) : selectionFromIndex(_polySelection, _mode);
  }
  
  @Override
  public int[] getSelectedPtIdx() {
    if (!isSelectionEmpty()) {
      final TIntHashSet res = new TIntHashSet();
      final int max = selection_.getMaxIndex();
      final MvElementModel m = (MvElementModel) modele_;
      for (int i = selection_.getMinIndex(); i <= max; i++) {
        if (selection_.isSelected(i)) {
          m.fillWithPtIdx(i, res);
        }
      }
      return res.toArray();
    }
    return null;
  }
  
  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    int m = selection_.getMaxIndex();
    if (m > modele_.getNombre()) {
      m = modele_.getNombre() - 1;
    }
    final GrBoite r = new GrBoite();
    final GrBoite temp = new GrBoite();
    final GrPolygone poly = new GrPolygone();
    for (int i = selection_.getMinIndex(); i <= m; i++) {
      if (selection_.isSelected(i)) {
        modele_.polygone(poly, i, true);
        poly.boite(temp);
        r.ajuste(temp);
      }
    }
    // ajusteZoomOnSelected(r);
    return r;
  }
  
  public boolean isConfigurable() {
    return true;
  }
  
  public boolean isFontModifiable() {
    return false;
  }
  
  @Override
  public boolean isPaletteModifiable() {
    return false;
  }
  
  @Override
  public boolean isSelectionElementEmpty() {
    return isSelectionEmpty();
  }
  
  @Override
  public boolean isSelectionPointEmpty() {
    return isSelectionEmpty();
  }
  
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    _g.setColor(getForeground());
    final int w = getIconWidth();
    final int h = getIconHeight();
    int x1 = _x + 3;
    int y1 = _y + h / 2;
    final int x2 = _x + w / 2;
    final int y2 = _y + 3;
    final int x3 = _x + w / 4;
    final int y3 = _y + h - 3;
    final int x4 = x2;
    final int y4 = _y + h / 2 + 4;
    _g.drawLine(x1, y1, x2, y2);
    _g.drawLine(x1, y1, x3, y3);
    _g.drawLine(x1, y1, x4, y4);
    _g.drawLine(x2, y2, x4, y4);
    _g.drawLine(x3, y3, x4, y4);
    x1 = _x + w - 3;
    y1 = _y + h / 2 + 3;
    _g.drawLine(x1, y1, x3, y3);
    _g.drawLine(x1, y1, x4, y4);
  }
}
