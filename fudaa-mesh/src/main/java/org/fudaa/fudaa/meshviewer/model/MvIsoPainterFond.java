/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2007-01-19 13:14:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPainterFond.java,v 1.1 2007-01-19 13:14:17 deniger Exp $
 */
public class MvIsoPainterFond extends MvIsoPainterDefault {

  /**
   * @param _g
   */
  public MvIsoPainterFond(final EfGridInterface _g) {
    super(_g);
  }

  @Override
  public double getValue(final int _idxPoint) {
    return g_.getPtZ(_idxPoint);
  }

  @Override
  public double getMax() {
    return g_.getMaxZ();
  }

  @Override
  public double getMin() {
    return g_.getMinZ();
  }

  @Override
  public String getNom() {
    return H2dResource.getS("Bathymétrie");
  }
}