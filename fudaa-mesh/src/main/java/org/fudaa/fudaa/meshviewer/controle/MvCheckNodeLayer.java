/**
 *  @creation     10 f�vr. 2004
 *  @modification $Date: 2006-11-14 09:08:07 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.meshviewer.layer.MvNodeLayer;

/**
 * @author deniger
 * @version $Id: MvCheckNodeLayer.java,v 1.13 2006-11-14 09:08:07 deniger Exp $
 */
public class MvCheckNodeLayer extends MvNodeLayer {

  /**
   * @param _modele
   */
  public MvCheckNodeLayer(final MvCheckNodeModel _modele) {
    super(_modele);
    setForeground(Color.RED);
    setIconModel(0, new TraceIconModel(TraceIcon.CARRE_PLEIN, 3, Color.RED));
  }

  @Override
  public String getTitle() {
    return ((MvCheckNodeModel) modele_).getDesc();
  }

  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel) {
    final MvCheckNodeModel model = (MvCheckNodeModel) modele_;
    final int nbPtErr = model.getNbPtWithError();
    if ((nbPtErr <= 0) && isSelectionEmpty()) { return; }
    final boolean attenue = isAttenue();
    final TraceIcon icone = new TraceIcon(new TraceIconModel(iconModel_));
    icone.setCouleur(getForeground());
    if (attenue) {
      icone.setCouleur(attenueCouleur(icone.getCouleur()));
    }
    final GrPoint p = new GrPoint();
    for (int i = nbPtErr - 1; i >= 0; i--) {
      modele_.point(p, model.getPtIdxWithError(i), true);
      if (!_clipReel.contientXY(p)) {
        continue;
      }
      p.autoApplique(_versEcran);
      icone.paintIconCentre(this, _g, p.x_, p.y_);
    }
  }

}