/*
 GPL 2
 */
package org.fudaa.fudaa.meshviewer.threedim;

import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.media.j3d.RenderingAttributes;
import javax.media.j3d.Texture;
import javax.media.j3d.TextureAttributes;
import javax.vecmath.Color4f;
import javax.vecmath.TexCoord2f;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.volume.BGrille;

/**
 *
 * @author Frederic Deniger
 */
public class Mv3DImage {

  private Mv3DImage() {
  }

  public static void paintAllInImage(final BGroupeCalque _cq, final Graphics2D _g, final GrMorphisme _versEcran,
          final GrMorphisme _versReel, final Dimension _imageDim) {
    if (_cq == null) {
      return;
    }
    final GrBoite gr = new GrBoite();
    gr.ajuste(0, 0, 0);
    gr.ajuste(_imageDim.getWidth(), _imageDim.getHeight(), 0);
    gr.autoApplique(_versReel);
    final BCalque[] cq = _cq.getCalques();
    for (int i = 0; i < cq.length; i++) {
      if (cq[i] instanceof ZCalqueImageRaster) {
        cq[i].paintAllInImage(_g, _versEcran, _versReel, gr);
      }
    }
  }

  public static void addTexture(final BGroupeCalque _fond, final BGrille _geom, final EfGridInterface _grid) {
    if (_fond != null) {
      final BCalque[] cq = _fond.getCalques();
      for (int i = 0; i < cq.length; i++) {
        if (cq[i] instanceof ZCalqueImageRaster) {
          addTexture((ZCalqueImageRaster) cq[i], _geom, _grid);
        }
      }
    }
  }

  public static boolean isIn(final float _f) {
    return _f >= 0F && _f <= 1F;
  }

  public static void addTexture(final ZCalqueImageRaster _fond, final BGrille _geom, final EfGridInterface _grid) {
    // on transforme l'image en 512*512 car Java3D n'aime que les images dont les dimensions
    // sont des puissances de 2.
    // we transform the picture in 512*512 because Java3D only supports the pictures which
    // dimensions are power of 2
    final BufferedImage imInit = _fond.getModelImage().getImage();
    final int sizeMax = 512;
    final double hInit = imInit.getHeight();
    final double wInit = imInit.getWidth();
    double hNew = hInit;
    double wNew = wInit;
    double e = 1;
    final boolean resized = hInit > sizeMax || wInit > sizeMax;
    if (resized) {
      if (hInit > wInit) {
        e = sizeMax / hInit;
        hNew = sizeMax;
        wNew = (int) (e * wInit);
      } else {
        e = sizeMax / wInit;
        hNew = (int) (e * hInit);
        wNew = sizeMax;
      }
    }

    BufferedImage imNew = imInit;
    if (hInit != sizeMax || wInit != sizeMax) {
      imNew = new BufferedImage(sizeMax, sizeMax, BufferedImage.TYPE_INT_ARGB);
      final Graphics2D imGraphics = imNew.createGraphics();
      // imGraphics.drawImage(imInit, sizeMax - wNew, sizeMax - hNew, wNew, hNew, null);
      imGraphics.drawImage(imInit, 0, (int) (sizeMax - hNew), (int) wNew, (int) hNew, null);

    }
    final AffineTransform toReal = _fond.getModeleAffineTransform();
    /*
     * if (resized) { toReal.concatenate(AffineTransform.getScaleInstance(e, e)); }
     */
    // la transformation inverson
    //the inversion transformation
    AffineTransform toImg = null;
    try {
      toImg = toReal.createInverse();
    } catch (final NoninvertibleTransformException _evt) {
      FuLog.error(_evt);
      return;

    }
    final double h = imInit.getHeight();
    final double w = imInit.getWidth();
    final GrBoite envImage = _fond.getModelImage().getDomaine();
    final Texture texImg = new com.sun.j3d.utils.image.TextureLoader(imNew).getTexture();
    final int ptsNb = _grid.getPtsNb();
    final TexCoord2f[] texture = new TexCoord2f[ptsNb];
    final Point2D.Double init = new Point2D.Double();
    final Point2D.Double dest = new Point2D.Double();
    final float ratioX = ((float) wNew) / sizeMax;
    final float ratioY = ((float) hNew) / sizeMax;
    final TexCoord2f nullTex = new TexCoord2f(-1, -1);
    for (int i = 0; i < ptsNb; i++) {

      texture[i] = nullTex;
      if (envImage.contientXY(_grid.getPtX(i), _grid.getPtY(i))) {
        init.x = _grid.getPtX(i);
        init.y = _grid.getPtY(i);
        toImg.transform(init, dest);
        dest.y = hInit - dest.y;
        final float f = (float) (dest.x / w) * ratioX;
        final float g = ((float) (dest.y / h) * ratioY);
        /*
         * if (resized) { f = (float) (f e); g = (float) (g e); }
         */
        if (isIn(f) && isIn(g)) {
          texture[i] = new TexCoord2f(f, g);
          // geomArray.setTextureCoordinate(i, new Point2f(f, g));
        }
      }
    }
    texImg.setBoundaryModeS(Texture.CLAMP);
    texImg.setBoundaryModeT(Texture.CLAMP);
    texImg.setBoundaryColor(new Color4f(1, 1, 1, 1));
    _geom.setTextureAttributesMode(TextureAttributes.MODULATE);
    _geom.setTextures(0, texture);
    _geom.setTexture(texImg);
    _geom.setTextureEnable(true);
    RenderingAttributes rendering = _geom.getRenderingAttributes();
    if (rendering == null) {
      rendering = new RenderingAttributes();
      _geom.setRenderingAttributes(rendering);
    }
    rendering.setAlphaTestFunction(RenderingAttributes.GREATER_OR_EQUAL);
    rendering.setAlphaTestValue(0.5f);

  }
  /*
   * public static BufferedImage createImage(BVueCalque _vue, BGroupeCalque _cq) { if (_cq == null) return null;
   * Dimension dim = new Dimension(1024, 1024); GrMorphisme versEcran =
   * ZTransformationDomaine.getVersEcran(_vue.getAbstractCalque().getDomaine(), dim, BuInsets.INSETS0000); GrMorphisme
   * versReel = ZTransformationDomaine.getVersReel(_vue.getAbstractCalque().getDomaine(), dim, BuInsets.INSETS0000);
   * BufferedImage im = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB); paintAllInImage(_cq,
   * im.createGraphics(), versEcran, versReel, dim); FileOutputStream out = null; try { out = new FileOutputStream(new
   * File(FuLib.getUserHome(), "test.png")); ImageIO.write(im, "png", out); } catch (FileNotFoundException _evt) {
   * FuLog.error(_evt); } catch (IOException _evt) { FuLog.error(_evt); } finally { if (out != null) try { out.close();
   * } catch (IOException _evt) { FuLog.error(_evt); } } return im; }
   */
}
