/**
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-01-19 13:14:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.meshviewer.layer;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.find.CalqueFindActionAbstract;
import org.fudaa.ebli.find.EbliFindActionAbstract;
import org.fudaa.ebli.find.EbliFindComponent;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author deniger
 * @version $Id: MVFindActionFrontierPt.java,v 1.1 2007-01-19 13:14:08 deniger Exp $
 */
public class MVFindActionFrontierPt extends CalqueFindActionAbstract {

  public MVFindActionFrontierPt(final MvFrontierPointLayer _layer) {
    super(_layer);
  }

  @Override
  public String erreur(final Object _s, final String _findId, final EbliFindable _parent) {
    if (getGlobIdx().equals(_findId) || "IDX".equals(_findId)) {
      boolean err = false;
      final boolean gene = getGlobIdx().equals(_findId);

      final int[] idx = EbliFindActionAbstract.getIndex((String) _s);
      String r = EbliFindActionAbstract.testVide(idx);
      if (r != null) {
        if (gene) {
          err = ((MvFrontierPointLayer) layer_).isGeneralSelectionCorrect(idx);
        } else {
          err = ((MvFrontierPointLayer) layer_).isFrontierSelectionCorrect(idx);
        }
        if (err) {
          r = MvResource.getS("Des indices sont en dehors des limites");
        }
      }
      return r;
    }
    return super.erreur(_s, _findId, _parent);
  }

  private String getGlobIdx() {
    return "GLOB_IDX";
  }

  @Override
  protected CtuluListSelection createNewSelection(final String _idx, final String _action, final int _selOption) {
    if (getGlobIdx().equals(_action)) { return super.createNewSelection("IDX", _action, _selOption); }
    return super.createNewSelection(_idx, _action, _selOption);
  }

  @Override
  protected int getNbValuesToTest() {
    return ((MvFrontierLayerAbstract) layer_).getModeleCl().getNbTotalPoint();
  }

  @Override
  protected int getNbValuesInModel() {
    return ((MvFrontierLayerAbstract) layer_).getModeleCl().getNbTotalPoint();
  }

  @Override
  protected CtuluListSelection getOldSelection() {
    return ((MvFrontierLayerAbstract) layer_).getSelectionInFrNum();
  }

  @Override
  protected EbliFindComponent buildExprComp() {
    return new MvFindComponentFrontierPt(MvResource.getS("Rechercher:"), expr_);
  }

  @Override
  protected boolean changeSelection(final CtuluListSelection _l, final int _selOption, final String _action) {
    if (getGlobIdx().equals(_action)) { return ((MvFrontierPointLayer) layer_).changeSelectionFromGlobalIdx(_l,
        _selOption); }
    return ((MvFrontierPointLayer) layer_).changeSelectionFromFrIdx(_l, _selOption);
  }

  @Override
  public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
    return false;
  }

  @Override
  public String editSelected(final EbliFindable _parent) {
    return null;
  }
}