/**
 *  @creation     27 mai 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.impl;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTask;
import com.memoire.bu.BuUndoRedoInterface;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.impl.EfGridSourceMutable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.meshviewer.MvActView;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.MvControllerSrc;
import org.fudaa.fudaa.meshviewer.MvResource;
import org.fudaa.fudaa.meshviewer.controle.MvCheckLayerGroup;
import org.fudaa.fudaa.meshviewer.layer.MvVisuPanel;
import org.fudaa.fudaa.meshviewer.model.MvInfoDelegateAbstract;

/**
 * @author Fred Deniger
 * @version $Id: MvInternalFrame.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public class MvInternalFrame extends ZEbliFilleCalques implements MvActView, BuUndoRedoInterface {

  MvCheckLayerGroup gpCheck_;

  MvControllerSrc src_;

  /**
   * @param _impl l'impl parente
   * @param _src le source
   */
  public MvInternalFrame(final FudaaCommonImplementation _impl, final MvControllerSrc _src) {
    super(new MvVisuPanel(_impl), _impl, null);
    setTitle(MvResource.getS("Vue 2D"));
    src_ = _src;
  }

  private void addMvControlResult(final MvControlResult _r) {
    if (_r.getIdx() != null) {
      if (gpCheck_ == null) {
        gpCheck_ = new MvCheckLayerGroup();
        gpCheck_.setDestructible(false);
        gpCheck_.setTitle(FudaaLib.getS("Contr�les"));
        pn_.addCalque(gpCheck_, true);
        ((MvVisuPanel) pn_).getCqInfos().enPremier();
      }
      final ZCalqueAffichageDonnees z = _r.buildLayer(getGrid());
      z.setDestructible(true);
      gpCheck_.add(z);
      gpCheck_.enPremier(z);
    }
  }

  protected void check() {
    src_.check();
  }

  protected void fillSpecificMenu(final BuMenu _m) {
    _m.addMenuItem(FudaaLib.getS("Contr�le"), "CHECK", BuResource.BU.getIcon("valider"), true, KeyEvent.VK_C, this);
  }

  protected EfGridInterface getGrid() {
    return src_.getSource().getGrid();
  }

  protected FudaaCommonImplementation getImpl() {
    return (FudaaCommonImplementation) getMvVisuPanel().getCtuluUI();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("CHECK".equals(_e.getActionCommand())) {
      check();
    } else {
      super.actionPerformed(_e);
    }
  }

  public ProgressionInterface createProgression(final BuTask _t) {
    return getImpl().createProgressionInterface(_t);
  }

  public MvVisuPanel getMvVisuPanel() {
    return (MvVisuPanel) pn_;
  }

  public Component getParentComponent() {
    return getImpl().getFrame();
  }

  @Override
  public void init(final EfGridSource _s) {
    final MvVisuPanel p = getMvVisuPanel();
    if (p.getGridGroup() == null) {
      final MvInfoDelegateAbstract delegate = new MvInfoDelegateAbstract() {

        @Override
        public EfGridInterface getGrid() {
          return MvInternalFrame.this.getGrid();
        }

        @Override
        public EbliFormatterInterface getXYFormatter() {
          return getMvVisuPanel().getEbliFormatter();
        }

      };
      final EfGridInterface g = _s.getGrid();
      p.addCqInfos(g);
      p.addCqMaillage(g, delegate);
      pack();
      setVisible(true);
      getImpl().addInternalFrame(this);
      restaurer();

    }
  }

  @Override
  public void redo() {
    getMvVisuPanel().redo();
  }

  @Override
  public void refresh(final EfGridSource _srcInit, final EfGridSourceMutable _enCours) {
    if (_enCours != null && _enCours.getGrid() != getGrid()) {
      // getMvVisuPanel().getGridGroup().setGrid(_enCours.getGrid());
      pn_.restaurer();
    }
  }

  @Override
  public void undo() {
    getMvVisuPanel().undo();
  }

  @Override
  public void updateCheck(final MvControlResult[] _a) {
    if ((_a != null) && (pn_ != null)) {
      for (int i = _a.length - 1; i >= 0; i--) {
        addMvControlResult(_a[i]);
      }
    }
  }
}