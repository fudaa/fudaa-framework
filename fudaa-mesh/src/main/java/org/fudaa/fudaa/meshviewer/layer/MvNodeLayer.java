/**
 * @creation 13 nov. 2003
 * @modification $Date: 2007-04-20 16:22:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.layer;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.ef.operation.EfIndexVisitorHashSetNode;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.fudaa.meshviewer.MvLayerGrid;
import org.fudaa.fudaa.meshviewer.model.MvExpressionSupplierNode;
import org.fudaa.fudaa.meshviewer.model.MvNodeModel;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;

import java.awt.*;

/**
 * @author deniger
 * @version $Id: MvNodeLayer.java,v 1.27 2007-04-20 16:22:09 deniger Exp $
 */
public class MvNodeLayer extends ZCalquePoint implements MvLayerGrid {
  /**
   * @param _modele le nouveau modele
   */
  public MvNodeLayer(final MvNodeModel _modele) {
    super(_modele);
  }

  @Override
  public int[] getSelectedEdgeIdx() {
    return null;
  }

  @Override
  public boolean isSelectionEdgeEmpty() {
    return true;
  }

  MvNodeModel getNodeModel() {
    return (MvNodeModel) modele_;
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    getNodeModel().getDataRange(_b);
    return true;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return false;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    final EbliFindExpressionContainerInterface i = getNodeModel().getExpressionContainer();
    return i == null ? new MvExpressionSupplierNode(getNodeModel()) : i;
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new MvFindActionNodeElt(this);
  }

  @Override
  public int[] getSelectedElementIdx() {
    return null;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedPtIdx();
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return (getNodeModel().getGrid().getIndex() == null) ? super.selection(_pt, _tolerance) : selectionFromIndex(_pt,
        _tolerance);
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    return (getNodeModel().getGrid().getIndex() == null) ? super.selection(_poly, _mode) : selectionFromIndex(_poly);
  }

  public CtuluListSelection selectionFromIndex(final LinearRing _poly) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FMV: layer select from index");
    }
    final EfGridInterface grid = getNodeModel().getGrid();
    final EfIndexVisitorHashSetNode list = new EfIndexVisitorHashSetNode(grid);
    getNodeModel().getGrid().getIndex().query(_poly.getEnvelopeInternal(), list);
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_poly);
    final int[] nds = list.getResult();
    final GrPoint p = new GrPoint();
    final Coordinate c = new Coordinate();
    final CtuluListSelection r = creeSelection();
    for (int i = nds.length - 1; i >= 0; i--) {
      final int idx = nds[i];
      modele().point(p, idx, true);
      c.x = p.x_;
      c.y = p.y_;
      if (GISLib.isSelected(c, _poly, tester, 1E-5)) {
        r.add(idx);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  /**
   * Methode qui permet de savoir si un point appartient au layer.
   *
   * @param _pt
   * @param _tolerance
   */
  public CtuluListSelection selectionFromIndex(final GrPoint _pt, final int _tolerance) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FMV: layer select from index");
    }
    return EfIndexHelper.getNearestNode(getNodeModel().getGrid(), _pt.x_, _pt.y_, GrMorphisme.convertDistanceXY(
        getVersReel(), _tolerance), null);
  }

  /**
   * @return les indices des points s�lectionn�s
   */
  @Override
  public int[] getSelectedPtIdx() {
    if (!isSelectionEmpty()) {
      return selection_.getSelectedIndex();
    }
    return null;
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  public boolean isFontModifiable() {
    return false;
  }

  @Override
  public boolean isSelectionElementEmpty() {
    return true;
  }

  @Override
  public boolean isSelectionPointEmpty() {
    return isSelectionEmpty();
  }

  /******************************************************************/
  /** M�thode overrid�e pour mettre en place l'algo d'optimisation **/
  /******************************************************************/
  @Override
  public void paintDonnees(final Graphics2D _g, final int _nbPt, final GrMorphisme _versEcran, final GrBoite _clipReel) {

    if ((modele_ == null) || (_nbPt <= 0)) {
      return;
    }
    final boolean attenue = isAttenue();
    final boolean rapide = isRapide();
    final GrPoint point = new GrPoint();

    final TraceIcon icone = iconModel_.buildCopy();
    if (icone == null) {
      return;
    }
    if (isAttenue()) {
      icone.setCouleur(attenueCouleur(icone.getCouleur()));
    }
    if (EbliLib.isAlphaChanged(alpha_)) {
      icone.setCouleur(EbliLib.getAlphaColor(icone.getCouleur(), alpha_));
    }

    /*******************************************************************************/
    /** D�claration variables utilis�es par algo optimisation par centre �l�ments **/
    /*******************************************************************************/
    final int width = this.getSize().width;
    CtuluListSelection alreadyPainted = new CtuluListSelection();

    for (int i = _nbPt - 1; i >= 0; i--) {
      // si le point est filtre on passe. On ne force pas l'affectation.
      if (!modele_.point(point, i, false)) {
        continue;
      }

      final double z = point.z_;
      if (!_clipReel.contientXY(point)) {
        continue;
      }
      point.autoApplique(_versEcran);
      final int x = (int) point.x_;
      final int y = (int) point.y_;
      int indexInSelection = x + y * width;
      if (indexInSelection < 0 || alreadyPainted.isSelected(indexInSelection)) {
        continue;
      }
      alreadyPainted.add(indexInSelection);
      if (isRapide()) {
        alreadyPainted.add(indexInSelection);
        indexInSelection = x + y * width;
        alreadyPainted.add(x + 1 + y * width);
        if (x > 0) {
          alreadyPainted.add(x - 1 + y * width);
        }
        alreadyPainted.add(x + (y + 1) * width);
        if (y > 0) {
          alreadyPainted.add(x + (y - 1) * width);
        }
      }

      if (!rapide && isPaletteCouleurUsed_ && (paletteCouleur_ != null)) {
        Color c = ((BPalettePlage) paletteCouleur_).getColorFor(z);
        if (attenue) {
          c = attenueCouleur(c);
        }
        icone.setCouleur(c);
      }

      icone.paintIconCentre(this, _g, x, y);
    }
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    if (modele_ == null || modele_.getNombre() == 0) {
      return;
    }
    if (isPaletteCouleurUsed_) {
      _g.setColor(Color.RED);
    } else {
      _g.setColor(getForeground());
    }
    final int w = getIconWidth();
    final int h = getIconHeight();
    int x1 = _x + 3;
    int y1 = _y + h / 2;
    final int x2 = _x + w / 2;
    final int y2 = _y + 3;
    final int x3 = _x + w / 4;
    final int y3 = _y + h - 3;
    final int x4 = x2;
    final int y4 = _y + h / 2 + 4;
    _g.drawLine(x1 - 1, y1, x1 + 1, y1);
    _g.drawLine(x1, y1 - 1, x1, y1 + 1);
    if (isPaletteCouleurUsed_) {
      _g.setColor(Color.BLUE);
    }
    _g.drawLine(x2 - 1, y2, x2 + 1, y2);
    _g.drawLine(x2, y2 - 1, x2, y2 + 1);
    if (isPaletteCouleurUsed_) {
      _g.setColor(Color.ORANGE);
    }
    _g.drawLine(x3 - 1, y3, x3 + 1, y3);
    _g.drawLine(x3, y3 - 1, x3, y3 + 1);
    if (isPaletteCouleurUsed_) {
      _g.setColor(Color.GREEN);
    }
    _g.drawLine(x4 - 1, y4, x4 + 1, y4);
    _g.drawLine(x4, y4 - 1, x4, y4 + 1);
    x1 = _x + w - 3;
    y1 = _y + h / 2 + 3;
    if (isPaletteCouleurUsed_) {
      _g.setColor(Color.yellow);
    }
    _g.drawLine(x1 - 1, y1, x1 + 1, y1);
    _g.drawLine(x1, y1 - 1, x1, y1 + 1);
  }
}
