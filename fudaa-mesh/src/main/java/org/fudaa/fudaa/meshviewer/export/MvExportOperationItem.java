package org.fudaa.fudaa.meshviewer.export;

import java.util.Observable;
import javax.swing.JComponent;
import org.fudaa.dodico.ef.EfGridData;

/**
 * @author Fred Deniger
 * @version $Id: MvExportPanelFilterDefault.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public abstract class MvExportOperationItem extends Observable {

  /**
   * @param _src la source adaptee au pas de temps choisi
   * @return le constructeur de filtre
   */
  public abstract MvExportOperationBuilderInterface getBuilder(EfGridData _src);

  public abstract JComponent getConfigureComponent();

  public abstract String getTitle();

  public abstract boolean isEnable();

  /**
   * Ferme les resources prises par l'opération
   */
  public void close() {

  }

  public boolean isStateModifiable() {
    return false;
  }

  public abstract String getErrorMessage();

}