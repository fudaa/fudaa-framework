/*
 *  @file         MvControlDefaultPanel.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.controle;

import com.memoire.fu.FuLog;
import java.util.ArrayList;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.fudaa.meshviewer.MvControlResult;
import org.fudaa.fudaa.meshviewer.action.MvItemAction;
import org.fudaa.fudaa.meshviewer.action.MvItemActionsPanel;

/**
 * @author deniger
 * @version $Id: MvControlPanel.java,v 1.8 2007-05-04 13:59:50 deniger Exp $
 */
public class MvControlPanel extends MvItemActionsPanel {

  public MvControlPanel() {
    build(new MvItemAction[] { new MvControlArea(), new MvControlAngle(), new MvControlOverstressed() });
  }

  public MvControlResult[] execute(final EfGridInterface _g, final ProgressionInterface _i) {
    if (_g == null) {
      FuLog.warning(new Throwable("Grid is null"));
    }
    final int n = controls_.length;
    final ArrayList l = new ArrayList(n);
    for (int i = 0; i < n; i++) {
      if (cb_[i].isSelected()) {
        final MvControlResult r = ((MvControlItemUI) controls_[i]).execute(_g, _i);
        if (r != null) {
          l.add(r);
        }

      }
    }
    final MvControlResult[] rf = new MvControlResult[l.size()];
    l.toArray(rf);
    return rf;

  }

}
