/*
 *  @file         MvControlUI.java
 *  @creation     9 f�vr. 2004
 *  @modification $Date: 2007-01-19 13:14:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.meshviewer.modify;

import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.impl.EfGridSourceMutable;
import org.fudaa.fudaa.meshviewer.action.MvItemActionUI;

/**
 * @author deniger
 * @version $Id: MvModifyItemUI.java,v 1.4 2007-01-19 13:14:35 deniger Exp $
 */
public interface MvModifyItemUI extends MvItemActionUI {

  boolean modify(EfGridSource _src, EfGridSourceMutable _currentGrid);

}
