/*
 *  @creation     17 mai 2005
 *  @modification $Date: 2007-06-11 13:08:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.dodico.ef.operation.type.EfOperationToT6;

/**
 * Cette procedure permet de transformer un maillage en T6. Elle permet de garder les coordonnées en Z.
 * 
 * @author Fred Deniger
 * @version $Id: MvExportT6Activity.java,v 1.9 2007-06-11 13:08:16 deniger Exp $
 */
public class MvExportT6Activity implements MvExportAdaptI {

  final EfGridData src_;

  boolean stop_;

  CtuluActivity current_;

  /**
   * @param _src
   */
  public MvExportT6Activity(final EfGridData _src) {
    super();
    src_ = _src;
  }

  @Override
  public EfGridData process(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    stop_ = false;
    EfOperationToT6 toT6 = new EfOperationToT6();
    toT6.setInitGridData(src_);
    current_ = toT6;
    EfOperationResult process = toT6.process(_prog);
    _analyze.merge(process.getAnalyze());
    return process.getGridData();
  }

  @Override
  public void stop() {
    if (current_ != null) {
      current_.stop();
    }
    stop_ = true;
  }

}
