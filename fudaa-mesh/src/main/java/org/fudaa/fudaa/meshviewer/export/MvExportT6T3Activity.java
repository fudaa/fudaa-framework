/*
 *  @creation     17 mai 2005
 *  @modification $Date: 2007-01-19 13:14:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.decorator.EfGridDataT64T3Decorator;
import org.fudaa.dodico.ef.impl.EfLibImpl;

/**
 * @author Fred Deniger
 * @version $Id: MvExportT6T3Activity.java,v 1.5 2007-01-19 13:14:23 deniger Exp $
 */
public class MvExportT6T3Activity implements MvExportAdaptI {

  final EfGridData src_;

  boolean stop_;
  boolean fourT3_;

  public final boolean isFourT3() {
    return fourT3_;
  }

  public final void setFourT3(final boolean _fourT3) {
    fourT3_ = _fourT3;
  }

  /**
   * @param _src
   */
  public MvExportT6T3Activity(final EfGridData _src) {
    super();
    src_ = _src;
  }

  @Override
  public EfGridData process(final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    stop_ = false;
    // si c'est deja un maillage T3
    if (src_.getGrid().getEltType() == EfElementType.T3) {
      return src_;
    } else if (src_.getGrid().getEltType() != EfElementType.T6) {
      _analyze.addFatalError("bad type for elements");
      return null;
    }
    // T6->T3
    if (!fourT3_) {
      final TIntIntHashMap oldPtnewPt = new TIntIntHashMap();
      final EfGridDataRenumeroteDecorator res = new EfGridDataRenumeroteDecorator(src_,EfLibImpl.maillageT6enT3(src_.getGrid(), _prog, oldPtnewPt));
      final TIntIntHashMap newPtOld = new TIntIntHashMap(oldPtnewPt.size());
      final TIntIntIterator it = oldPtnewPt.iterator();
      for (int i = oldPtnewPt.size(); i-- > 0;) {
        it.advance();
        newPtOld.put(it.value(), it.key());
      }

      if (stop_) { return null; }
      res.setEltNewIdxOldIdxMap(null);
      res.setPtNewIdxOldIdxMap(newPtOld);
      return res;
    }
    final EfGridDataRenumeroteDecorator res = new EfGridDataT64T3Decorator(src_,EfLibImpl.maillageT6en4T3(src_.getGrid(), _prog));
    if (stop_) { return null; }
    res.setEltNewIdxOldIdxMap(null);
    res.setPtNewIdxOldIdxMap(null);
    return res;
  }

  @Override
  public void stop() {
    stop_ = true;
  }

}
