/*
 *  @creation     17 mai 2005
 *  @modification $Date: 2007-06-11 13:08:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;

/**
 * Transforme des valeurs de type elements en valeur de type noeud.
 * 
 * @author Fred Deniger
 * @version $Id: MvExportDecoratorEltData.java,v 1.12 2007-06-11 13:08:15 deniger Exp $
 */
public class MvExportDecoratorEltData implements EfGridData {

  private final EfNeighborMesh mesh_;

  final EfGridData src_;
  final InterpolationVectorContainer vects_;

  public MvExportDecoratorEltData(final EfGridData _src, final EfNeighborMesh _neighbor,
      final InterpolationVectorContainer _vects) {
    src_ = _src;
    mesh_ = _neighbor;
    vects_ = _vects;
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return src_.isDefined(_var);
  }

  @Override
  public EfData getData(final CtuluVariable _var, final int _timeIdx) throws IOException {
    final EfData init = src_.getData(_var, _timeIdx);
    if (isNodeData(init)) { return init; }
    return mesh_.getDataNode(_var, _timeIdx, init, src_, vects_);
  }

  @Override
  public double getData(final CtuluVariable _var, final int _timeIdx, final int _idxObjet) throws IOException {
    final EfData init = src_.getData(_var, _timeIdx);
    if (isNodeData(init)) { return init.getValue(_idxObjet); }
    return mesh_.getAverageForNode(_var, _idxObjet, _timeIdx, init, src_, vects_);
  }

  private boolean isNodeData(final EfData init) {
    return init == null || !init.isElementData();
  }

  @Override
  public EfGridInterface getGrid() {
    return src_.getGrid();
  }

  /**
   * Toutes les variables sont d�sormais d�finies sur les noeuds.
   * 
   * @return false
   */
  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return false;
  }

}
