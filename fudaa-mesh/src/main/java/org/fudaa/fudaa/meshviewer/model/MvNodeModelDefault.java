/**
 * @creation 6 f�vr. 2004
 * @modification $Date: 2007-01-19 13:14:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZModeleDonnesAbstract;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id: MvNodeModelDefault.java,v 1.1 2007-01-19 13:14:17 deniger Exp $
 */
public class MvNodeModelDefault extends ZModeleDonnesAbstract implements MvNodeModel, ZModelePoint.ThreeDim {

  private EfGridInterface grid_;
  private MvInfoDelegate delegate_;

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return null;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    if (delegate_ != null && delegate_.isValuesTableAvailableForNodeLayer()) { return delegate_
        .createValuesTableForNodeLayer(); }
    final BuTable res = new BuTable();
    res.setModel(new ZCalquePoint.DefaultTableModel(this));
    return res;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  /**
   * @param _g le maillage a afficher
   */
  public MvNodeModelDefault(final EfGridInterface _g) {
    this(_g, null);
  }

  @Override
  public double getX(final int _i) {
    return grid_.getPtX(_i);
  }

  @Override
  public double getY(final int _i) {
    return grid_.getPtY(_i);
  }

  @Override
  public double getZ(final int _i) {
    return grid_.getPtZ(_i);
  }

  public boolean isValuesTableAvailableFromModel() {
    return delegate_ == null ? false : delegate_.isValuesTableAvailableForNodeLayer();
  }

  /**
   * @param _g le maillage a afficher et le delehue
   * @param _d
   */
  public MvNodeModelDefault(final EfGridInterface _g, final MvInfoDelegate _d) {
    grid_ = _g;
    delegate_ = _d;
  }

  @Override
  public boolean getDataRange(final CtuluRange _b) {
    _b.max_ = grid_.getMaxZ();
    _b.min_ = grid_.getMinZ();
    return true;
  }

  @Override
  public void setGrid(final EfGrid _g) {
    grid_ = _g;
  }

  /**
   * @return le maillage en question
   */
  @Override
  public final EfGridInterface getGrid() {
    return grid_;
  }

  /**
   * @return le delegue pour l'affichage de l'info
   */
  public MvInfoDelegate getDelegate() {
    return delegate_;
  }

  /**
   * @param _delegate le nouveau delegue
   */
  public void setDelegate(final MvInfoDelegate _delegate) {
    delegate_ = _delegate;
  }

  @Override
  public boolean point(final GrPoint _p, final int _i, final boolean _force) {
    if (_p == null) { return false; }
    _p.x_ = grid_.getPtX(_i);
    _p.y_ = grid_.getPtY(_i);
    _p.z_ = grid_.getPtZ(_i);
    return true;
  }

  @Override
  public GrBoite getDomaine() {
    final GrBoite b = new GrBoite();
    b.o_ = new GrPoint(grid_.getMinX(), grid_.getMinY(), 0);
    b.e_ = new GrPoint(grid_.getMaxX(), grid_.getMaxY(), 0);
    return b;
  }

  @Override
  public int getNombre() {
    return grid_.getPtsNb();
  }

  @Override
  public Object getObject(final int _ind) {
    throw new UnsupportedOperationException("not implemented method");
  }

  @Override
  public void fillWithInfo(final InfoData _m, final ZCalqueAffichageDonneesInterface _layer) {
    if (delegate_ != null) {
      delegate_.fillWithPointInfo(_m, _layer.getLayerSelection(), _layer.getTitle());
    }

  }
}