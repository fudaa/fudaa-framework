/*
 *  @creation     25 ao�t 2005
 *  @modification $Date: 2007-01-19 13:14:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.persistence;

import org.fudaa.fudaa.sig.persistence.FSigLayerGroupPersistence;

/**
 * Une classe permettant la sauvegarde des calques SIG.
 * 
 * @author Fred Deniger
 * @version $Id: MvSigLayerGroupPersistence.java,v 1.4 2007-01-19 13:14:35 deniger Exp $
 */
public class MvSigLayerGroupPersistence extends FSigLayerGroupPersistence {

  public MvSigLayerGroupPersistence() {

  }
}
