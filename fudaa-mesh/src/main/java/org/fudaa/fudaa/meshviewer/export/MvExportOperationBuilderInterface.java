/*
 * @creation 16 mai 2005
 * 
 * @modification $Date: 2007-04-26 14:36:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.operation.EfOperation;

/**
 * @author Fred Deniger
 * @version $Id: MvExportFilterBuilderInterface.java,v 1.4 2007-04-26 14:36:47 deniger Exp $
 */
public interface MvExportOperationBuilderInterface extends CtuluActivity {

  /**
   * Permet de construire le filtre.
   * 
   * @param _progression la barre de progression
   */
  void buildOperation(ProgressionInterface _progression);

  /**
   * @param _tidx l'indice du temps demande. -1 pour tous ou si non ad�quat
   * @return le filtre
   */
  EfOperation getOperation(int _tidx);

}
