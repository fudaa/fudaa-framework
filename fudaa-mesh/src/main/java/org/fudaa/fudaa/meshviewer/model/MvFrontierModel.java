/**
 * @creation 11 sept. 2003
 * @modification $Date: 2007-01-19 13:14:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.meshviewer.model;

import org.locationtech.jts.geom.Envelope;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.ebli.calque.ZModeleDonneesMulti;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author deniger
 * @version $Id: MvFrontierModel.java,v 1.1 2007-01-19 13:14:17 deniger Exp $
 */
public interface MvFrontierModel extends ZModeleDonneesMulti {

  boolean point(GrPoint _p, int _idxFrontier, int _idxPtOnFrontier);

  double getX(int _idxFrontier, int _idxPtOnFrontier);

  double getY(int _idxFrontier, int _idxPtOnFrontier);

  int getNbFrontier();

  boolean isExternFrontier(int _idxFr);

  int getNbPointInFrontier(int _i);

  boolean isT6();

  EfFrontierInterface getFr();

  int getGlobalIdx(int _idxFrontier, int _idxPtOnFrontier);

  int getNbTotalPoint();

  int getFrontiereIndice(int _idxFr, int _idxOnFr);

  boolean getIdxFrIdxOnFrFromGeneral(int _generalIdx, int[] _idxFrIdxOn);

  boolean getIdxFrIdxOnFrFromFrontier(int _frontierIdx, int[] _idxFrIdxOn);

  void getDomaineForPolygoneIdx(int _idxFr, Envelope _boite);

}
