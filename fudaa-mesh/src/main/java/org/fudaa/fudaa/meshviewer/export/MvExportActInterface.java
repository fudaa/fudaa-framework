/*
 * @creation 16 mai 2005
 * 
 * @modification $Date: 2007-06-05 09:01:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.export;

import java.io.File;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.operation.EfOperation;

/**
 * @author Fred Deniger
 * @version $Id: MvExportActInterface.java,v 1.3 2007-06-05 09:01:12 deniger Exp $
 */
public interface MvExportActInterface extends CtuluActivity {

  void setSelectedTimeStep(double[] _idx);

  void setSrc(EfGridData _src, InterpolationVectorContainer _vects);

  void setSelectedVar(CtuluVariable[] _idx);

  int getNbLabelInfoNeeded();

  String getTitre();

  /**
   * Le constructeur de filtre.
   * 
   * @param _filtrer le nouveau filtre
   */
  void setOperation(EfOperation _filtrer);

  /**
   * @param _prog
   * @param _dest
   * @param _analyze
   */
  void actExport(ProgressionInterface _prog, File[] _dest, CtuluAnalyzeGroup _analyze, String[] _message);

}
