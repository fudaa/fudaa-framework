/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2007-05-04 13:59:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.meshviewer.model;

import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.fudaa.meshviewer.MvResource;

/**
 * @author Fred Deniger
 * @version $Id: MvIsoPainterNone.java,v 1.2 2007-05-04 13:59:50 deniger Exp $
 */
public class MvIsoPainterNone implements MvIsoPainter {

  @Override
  public boolean isDiscrete() {
    return false;
  }

  public void initPalette(final BPalettePlage _p) {}

  @Override
  public String getNom() {
    return MvResource.getS("Aucun");
  }

  @Override
  public boolean isPaletteInitialized() {
    return true;
  }

  @Override
  public BPalettePlageAbstract getPalette() {
    return null;
  }

  @Override
  public void paint(final Graphics2D _g, final GrMorphisme _versEcran, final GrBoite _clipReel, final int _alpha) {}

  @Override
  public double getMin() {
    return 0;
  }

  @Override
  public void setPalette(final BPalettePlageAbstract _palette) {}

  @Override
  public String toString() {
    return getNom();
  }

  @Override
  public double getMax() {
    return 0;
  }

}
