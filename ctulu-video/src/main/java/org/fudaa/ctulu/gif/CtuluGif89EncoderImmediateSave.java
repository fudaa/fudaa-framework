//******************************************************************************
// Gif89Encoder.java
//******************************************************************************
package org.fudaa.ctulu.gif;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;

//==============================================================================
/**
 * This is the central class of a JDK 1.1 compatible GIF encoder that, AFAIK, supports more features of the extended GIF
 * spec than any other Java open source encoder. Some sections of the source are lifted or adapted from Jef Poskanzer's
 * <cite>Acme GifEncoder</cite> (so please see the <a href="../readme.txt">readme</a> containing his notice), but much
 * of it, including nearly all of the present class, is original code. My main motivation for writing a new encoder was
 * to support animated GIFs, but the package also adds support for embedded textual comments.
 * <p>
 * There are still some limitations. For instance, animations are limited to a single global color table. But that is
 * usually what you want anyway, so as to avoid irregularities on some displays. (So this is not really a limitation,
 * but a "disciplinary feature" :) Another rather more serious restriction is that the total number of RGB colors in a
 * given input-batch mustn't exceed 256. Obviously, there is an opening here for someone who would like to add a
 * color-reducing preprocessor.
 * <p>
 * The encoder, though very usable in its present form, is at bottom only a partial implementation skewed toward my own
 * particular needs. Hence a couple of caveats are in order. (1) During development it was in the back of my mind that
 * an encoder object should be reusable - i.e., you should be able to make multiple calls to encode() on the same
 * object, with or without intervening frame additions or changes to options. But I haven't reviewed the code with such
 * usage in mind, much less tested it, so it's likely I overlooked something. (2) The encoder classes aren't thread
 * safe, so use caution in a context where access is shared by multiple threads. (Better yet, finish the library and
 * re-release it :)
 * <p>
 * There follow a couple of simple examples illustrating the most common way to use the encoder, i.e., to encode AWT
 * Image objects created elsewhere in the program. Use of some of the most popular format options is also shown, though
 * you will want to peruse the API for additional features.
 * <p>
 * <strong>Animated GIF Example</strong>
 * 
 * <pre>
 *    import net.jmge.gif.Gif89Encoder;
 *    // ...
 *    void writeAnimatedGIF(Image[] still_images,
 *                          String annotation,
 *                          boolean looped,
 *                          double frames_per_second,
 *                          OutputStream out) throws IOException
 *    {
 *      Gif89Encoder gifenc = new Gif89Encoder();
 *      for (int i = 0; i &lt; still_images.length; ++i)
 *        gifenc.addFrame(still_images[i]);
 *      gifenc.setComments(annotation);
 *      gifenc.setLoopCount(looped ? 0 : 1);
 *      gifenc.setUniformDelay((int) Math.round(100 / frames_per_second));
 *      gifenc.encode(out);
 *    }
 * </pre>
 * 
 * <strong>Static GIF Example</strong>
 * 
 * <pre>
 *    import net.jmge.gif.Gif89Encoder;
 *    // ...
 *    void writeNormalGIF(Image img,
 *                        String annotation,
 *                        int transparent_index,  // pass -1 for none
 *                        boolean interlaced,
 *                        OutputStream out) throws IOException
 *    {
 *      Gif89Encoder gifenc = new Gif89Encoder(img);
 *      gifenc.setComments(annotation);
 *      gifenc.setTransparentIndex(transparent_index);
 *      gifenc.getFrameAt(0).setInterlaced(interlaced);
 *      gifenc.encode(out);
 *    }
 * </pre>
 * 
 * @version 0.90 beta (15-Jul-2000)
 * @author J. M. G. Elliott (tep@jmge.net)
 * @see Gif89Frame
 * @see DirectGif89Frame
 * @see IndexGif89Frame
 */
public class CtuluGif89EncoderImmediateSave {

  private Dimension dispDim = new Dimension(0, 0);
  private GifColorTable colorTable;
  private int bgIndex = 0;
  private int loopCount = 1;
  private String theComments;
  private final int delay_;
  private final OutputStream out_;

  // private Vector vFrames = new Vector();

  public CtuluGif89EncoderImmediateSave(final int _delay, final OutputStream _out) {
    super();
    colorTable = new GifColorTable();
    delay_ = _delay;
    out_ = _out;
  }

  public BufferedImage getColorModel(BufferedImage _i) {
    File tmp = null;
    BufferedImage res = null;
    try {
      // on va faire travailler les plugins de java....
      Iterator it = ImageIO.getImageWritersByFormatName("gif");
      // le gif est support� en �criture
      if (it != null && it.hasNext()) {
        // on ecrit un fichier tempo
        tmp = File.createTempFile("test", ".gif");
        if (tmp != null) {
          // et on le relit... pour obtenier le Modele de couleur
          ImageIO.write(_i, "gif", tmp);
          res = ImageIO.read(tmp);
        }
      }
    } catch (FileNotFoundException _evt) {
      _evt.printStackTrace();

    } catch (IOException _evt) {
      _evt.printStackTrace();
    } finally {
      if (tmp != null) tmp.delete();
    }
    return res;
  }

  ColorConvertOp op_;
  IndexColorModel colorModel_;

  /**
   * Convenience version of addFrame() that takes a Java Image, internally constructing the requisite DirectGif89Frame.
   * 
   * @param _image Any Image object that supports pixel-grabbing.
   * @exception IOException If either (1) pixel-grabbing fails, (2) the aggregate cross-frame RGB color count exceeds
   *              256, or (3) this encoder object was constructed with an explicit color table.
   */
  public void addFrame(Image _image) throws IOException {
    //FRED
    //on va traduire l'image vers un Indexed Color Model
    //pour cela, on va essayer, dans un premier temps
    if (op_ == null) {
      //pour cela, on va essayer, dans un premier temps de r�cup�rer le ColorModel
      //utiliser par les plugins de ImageIO: on suppose qu'il construit une table mieux adapt�e
      //que celle construite par d�faut par BufferedImage
      //test� par Fred en mars 2007
      BufferedImage im = getColorModel((BufferedImage) _image);
      //on recup�re le ColorModel
      if (im != null) colorModel_ = (IndexColorModel) im.getColorModel();
      if (colorModel_ == null) colorModel_ = (IndexColorModel) new BufferedImage(_image.getWidth(null), _image
          .getHeight(null), BufferedImage.TYPE_BYTE_INDEXED).getColorModel();
      //pour avoir une meilleure interpolation
      Map hints = new HashMap();
      hints.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
      hints.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
      op_ = new ColorConvertOp(colorModel_.getColorSpace(), new RenderingHints(hints));
    }

    BufferedImage res = op_.createCompatibleDestImage((BufferedImage) _image, colorModel_);
    op_.filter((BufferedImage) _image, res);
    addFrame(new DirectGif89Frame(res));

  }

  public void addFrame(Gif89Frame gf) throws IOException {
    accommodateFrame(gf);
    gf.setDelay(delay_);
    encode(gf);
    // vFrames.addElement(gf);
  }

  // ----------------------------------------------------------------------------
  /**
   * Set the color table index for the transparent color, if any.
   * 
   * @param index Index of the color that should be rendered as transparent, if any. A value of -1 turns off
   *          transparency. (Default: -1)
   */
  public void setTransparentIndex(int index) {
    colorTable.setTransparent(index);
  }

  // ----------------------------------------------------------------------------
  /**
   * Sets attributes of the multi-image display area, if applicable.
   * 
   * @param dim Width/height of display. (Default: largest detected frame size)
   * @param background Color table index of background color. (Default: 0)
   * @see Gif89Frame#setPosition
   */
  public void setLogicalDisplay(Dimension dim, int background) {
    dispDim = new Dimension(dim);
    bgIndex = background;
  }

  // ----------------------------------------------------------------------------
  /**
   * Set animation looping parameter, if applicable.
   * 
   * @param count Number of times to play sequence. Special value of 0 specifies indefinite looping. (Default: 1)
   */
  public void setLoopCount(int count) {
    loopCount = count;
  }

  // ----------------------------------------------------------------------------
  /**
   * Specify some textual comments to be embedded in GIF.
   * 
   * @param comments String containing ASCII comments.
   */
  public void setComments(String comments) {
    theComments = comments;
  }

  boolean first_ = true;

  // ----------------------------------------------------------------------------
  /**
   * After adding your frame(s) and setting your options, simply call this method to write the GIF to the passed stream.
   * Multiple calls are permissible if for some reason that is useful to your application. (The method simply encodes
   * the current state of the object with no thought to previous calls.)
   * 
   * @param out The stream you want the GIF written to.
   * @exception IOException If a write error is encountered.
   */
  public void encode(Gif89Frame _f) throws IOException {
    boolean is_sequence = true;
    // N.B. must be called before writing screen descriptor
    colorTable.closePixelProcessing();

    // write GIF HEADER
    if (first_) {
      Put.ascii("GIF89a", out_);

      // write global blocks
      writeLogicalScreenDescriptor(out_);
      colorTable.encode(out_);
      if (is_sequence && loopCount != 1) writeNetscapeExtension(out_);
      if (theComments != null && theComments.length() > 0) writeCommentExtension(out_);
      first_ = false;
    }
    _f.encode(out_, is_sequence, colorTable.getDepth(), colorTable.getTransparent());

  }

  public void finish() throws IOException {
    out_.write((int) ';');
    out_.close();
  }

  // ----------------------------------------------------------------------------
  private void accommodateFrame(Gif89Frame gf) throws IOException {
    dispDim.width = Math.max(dispDim.width, gf.getWidth());
    dispDim.height = Math.max(dispDim.height, gf.getHeight());
    colorTable.processPixels(gf);
  }

  // ----------------------------------------------------------------------------
  private void writeLogicalScreenDescriptor(OutputStream os) throws IOException {
    Put.leShort(dispDim.width, os);
    Put.leShort(dispDim.height, os);

    // write 4 fields, packed into a byte (bitfieldsize:value)
    // global color map present? (1:1)
    // bits per primary color less 1 (3:7)
    // sorted color table? (1:0)
    // bits per pixel less 1 (3:varies)
    os.write(0xf0 | colorTable.getDepth() - 1);

    // write background color index
    os.write(bgIndex);

    // Jef Poskanzer's notes on the next field, for our possible edification:
    // Pixel aspect ratio - 1:1.
    // Putbyte( (byte) 49, outs );
    // Java's GIF reader currently has a bug, if the aspect ratio byte is
    // not zero it throws an ImageFormatException. It doesn't know that
    // 49 means a 1:1 aspect ratio. Well, whatever, zero works with all
    // the other decoders I've tried so it probably doesn't hurt.

    // OK, if it's good enough for Jef, it's definitely good enough for us:
    os.write(0);
  }

  // ----------------------------------------------------------------------------
  private void writeNetscapeExtension(OutputStream os) throws IOException {
    // n.b. most software seems to interpret the count as a repeat count
    // (i.e., interations beyond 1) rather than as an iteration count
    // (thus, to avoid repeating we have to omit the whole extension)

    os.write((int) '!'); // GIF Extension Introducer
    os.write(0xff); // Application Extension Label

    os.write(11); // application ID block size
    Put.ascii("NETSCAPE2.0", os); // application ID data

    os.write(3); // data sub-block size
    os.write(1); // a looping flag? dunno

    // we finally write the relevent data
    Put.leShort(loopCount > 1 ? loopCount - 1 : 0, os);

    os.write(0); // block terminator
  }

  // ----------------------------------------------------------------------------
  private void writeCommentExtension(OutputStream os) throws IOException {
    os.write((int) '!'); // GIF Extension Introducer
    os.write(0xfe); // Comment Extension Label

    int remainder = theComments.length() % 255;
    int nsubblocks_full = theComments.length() / 255;
    int nsubblocks = nsubblocks_full + (remainder > 0 ? 1 : 0);
    int ibyte = 0;
    for (int isb = 0; isb < nsubblocks; ++isb) {
      int size = isb < nsubblocks_full ? 255 : remainder;

      os.write(size);
      Put.ascii(theComments.substring(ibyte, ibyte + size), os);
      ibyte += size;
    }

    os.write(0); // block terminator
  }

}