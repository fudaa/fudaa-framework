/*
 * @creation 24 janv. 07
 * @modification $Date: 2007-05-04 13:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.video;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.memoire.bu.BuPreferences;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;

/**
 * @author fred deniger
 * @version $Id: CtuluMencoderFinder.java,v 1.3 2007-05-04 13:43:25 deniger Exp $
 */
public final class CtuluMencoderFinder {

  private final String execName_ = "mencoder.exe";
  private final String versionName_ = "mencoder_version.txt";
  private final String versionKey_ = "version";
  private final String mencoderDirPath_ = "mencoder";
  private final File mencoderTmpDir_;

  public CtuluMencoderFinder() {
    File dest = null;
    // si le logiciel mencoder est inclu avec la distrib
    if (isMencoderInclude()) {
      try {
        // on recherche le repertoire dans lequel il doit etre dezipp�
        dest = getMencoderDir();
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }
    mencoderTmpDir_ = dest;

  }

  public boolean isMencoderInclude() {
    return getClass().getResource("/" + execName_) != null;
  }

  private File getMencoderDir() throws IOException {
    final File fudaa = new File(FuLib.getUserHome(), ".fudaa");
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("CVI: fudaa dir is " + fudaa.getAbsolutePath());
    }
    if (!fudaa.exists()) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("CVI: fudaa dir will be created");
      }
      boolean created = fudaa.mkdirs();
      if (!created) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("CVI: can't create the fudaa dir");
        }
        return CtuluLibFile.createTempDir();
      }
    }
    final File mencoderDir = new File(fudaa, mencoderDirPath_);
    if (!mencoderDir.exists()) {
      if (!mencoderDir.mkdir()) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("CVI: can't create the mencoder dir");
        }
        return CtuluLibFile.createTempDir();
      }
    }
    return mencoderDir;
  }

  public String getMencoderPath() {
    String res = getMencoderInPathOrDistrib();
    if (res == null) {
      final String mencoderInPref = getMencoderInPref();
      if (mencoderInPref != null && isMencoderOk(mencoderInPref)) {
        res = mencoderInPref;
      }
    }
    return res;
  }

  public String getMencoderInPathOrDistrib() {
    // mencoder est inclus avec la distribution
    if (isMencoderInclude() && mencoderTmpDir_ != null) {
      extractMencoder();
      final File resFile = new File(mencoderTmpDir_, execName_);
      if (resFile.exists()) {
        return resFile.getAbsolutePath();
      }
    }

    // mencoder non trouv�: est-t-il dans le path
    return isMencoderOk("mencoder") ? "mencoder" : null;
  }

  public static String getMencoderInPref() {
    return BuPreferences.BU.getStringProperty("mencoder.path", null);
  }

  public static void setMencoderInPref(final String _path) {
    BuPreferences.BU.putStringProperty("mencoder.path", _path);
  }

  public boolean isMencoderFound() {
    return getMencoderPath() != null;
  }

  public static boolean isMencoderOk(final String _exe) {
    boolean isInPath = false;
    final String[] cmd = new String[] { _exe, "-ovc", "help" };
    try {
      FuLib.runProgram(cmd);
      FuLog.trace("CVI: mencoder is found from " + _exe);
      isInPath = true;
    } catch (final IOException _evt) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("mencoder is not in path: " + _exe);
        // if (Fu.DEBUG && FuLog.isDebug()) _evt.printStackTrace();
      }
    }
    return isInPath;
  }

  private void extractMencoder() {
    if (!isLastVersion()) {
      FuLog.trace("CVI: on extrait mencoder");
      extractFile(execName_);
      extractFile(versionName_);
    }
  }

  private FileOutputStream extractFile(final String _path) {
    FileOutputStream out = null;
    try {

      final InputStream execStream = getClass().getResourceAsStream("/" + _path);
      if (execStream == null) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("CVI: le flux pour " + _path + " n'a pas �t� trouv�");
        }
      }
      out = new FileOutputStream(new File(mencoderTmpDir_, _path));
      CtuluLibFile.copyStream(execStream, out, true, true);
    } catch (final FileNotFoundException _evt) {
      FuLog.error(_evt);
      CtuluLibFile.close(out);
    }
    return out;
  }

  /**
   * @return true si la version de mencoder est pr�sente dans le rep temp et si elle est � jour.
   */
  private boolean isLastVersion() {
    final String versionInTmp = getVersionInTmpDir();
    if (versionInTmp == null) {
      return false;
    }
    final String versionInJar = getVersionInJar();
    if (versionInJar == null) {
      return false;
    }
    final boolean res = versionInTmp.compareTo(versionInJar) >= 0;
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("CVI: la version dans le fichier tmp est � jour? = " + res);
    }
    return res;

  }

  /**
   * @return la version lue dans le dossier tmp: null si non trouv�e
   */
  private String getVersionInTmpDir() {
    final File versionFile = new File(mencoderTmpDir_, versionName_);
    // cas simple : le fichier contenant la version n'est m�me pas cr��
    if (!versionFile.exists() || !new File(mencoderTmpDir_, execName_).exists()) {
      return null;
    }
    final Properties propInFile = new Properties();
    FileInputStream inFile = null;
    try {
      inFile = new FileInputStream(versionFile);
      propInFile.load(inFile);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    } finally {
      CtuluLibFile.close(inFile);
    }
    final String versionInFile = propInFile.getProperty(versionKey_);
    if (versionInFile == null) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("CVI: il semble que le fichier de version soit erron�");
      }
    }
    return versionInFile;
  }

  /**
   * @return la version lue dans le jar: null si non trouv�e
   */
  private String getVersionInJar() {
    // on lit le contenu du fichier mencoder_version.txt contenu dans le jar
    final InputStream versionStream = getClass().getResourceAsStream("/" + versionName_);
    if (versionStream == null) {
      FuLog.error("Il manque le fichier mencoder_version.txt dans le jar mencoder");
      return null;
    }

    final Properties propInJar = new Properties();
    try {
      propInJar.load(versionStream);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    } finally {
      CtuluLibFile.close(versionStream);
    }
    return propInJar.getProperty(versionKey_);
  }

}
