/**
 * @creation 19 nov. 2004
 * @modification $Date: 2007-01-26 14:57:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.video;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import com.memoire.fu.FuLog;

/**
 * @author Fred Deniger
 * @version $Id: CtuluAviWriter.java,v 1.1 2007-01-26 14:57:25 deniger Exp $
 */
public class CtuluAviWriter {

  //private ImagePlus imp;
  private RandomAccessFile raFile_;
  private int bytesPerPixel_;

  private final File file_;
  //private int bufferFactor;
  private int xDim_;
  private final int  yDim_;
  private final int zDim_;
  private int tDim_;
  //private int lutBufferRemapped[] = null;
  private int microSecPerFrame_;
  private int xPad_;
  private byte[] bufferWrite_;
  /*
   * private int bufferSize; private int indexA, indexB; private float opacityPrime; private int
   * bufferAdr;
   */
  // private byte[] lutWrite = null;
  //private int[] dcLength = null;
  //int nbImg_;
  /*
   * int width_; int height_;
   */
  byte[] dataSignature_;
  long[] savedbLength_;
  long saveLIST2Size_;
  long saveidx1Length_;
  long savemovi_;
  int initX_;
  int initY_;

  /**
   * @param _f le fichier de destination
   * @param _nbImg le nombre d'images
   * @param _width la largeur du film
   * @param _height la hauteur du film
   * @param _frameRate le taux ( nbImage/ par sec
   */
  public CtuluAviWriter(final File _f, final int _nbImg, final int _width, final int _height, final int _frameRate) {
    file_ = _f;
    zDim_ = _nbImg;
    yDim_ = _height;
    xDim_ = _width;
    initY_=yDim_;
    initX_=xDim_;
    rate_ = _frameRate;
  }

  long saveFileSize_;
  /**
   * Debut de l'ecriture.
   * @throws IOException
   */
  public void writeImageBegin() throws IOException{
    dataSignature_ = new byte[4];
    dataSignature_[0] = 48; // 0
    dataSignature_[1] = 48; // 0
    dataSignature_[2] = 100; // d
    dataSignature_[3] = 98; // b
    // location of file size in bytes not counting first 8 bytes
    long saveLIST1Size; // location of length of CHUNK with first LIST - not including
    // first 8 bytes with LIST and size. JUNK follows the end of
    // this CHUNK
    //int[] extents;
    long saveLIST1subSize; // location of length of CHUNK with second LIST - not including
    // first 8 bytes with LIST and size. Note that saveLIST1subSize =
    // saveLIST1Size + 76, and that the length size written to
    // saveLIST2Size is 76 less than that written to saveLIST1Size.
    // JUNK follows the end of this CHUNK.
    long savestrfSize; // location of lenght of strf CHUNK - not including the first
    // 8 bytes with strf and size. strn follows the end of this
    // CHUNK.
    //int resXUnit = 0;
    //int resYUnit = 0;
    //float xResol = 0.0f; // in distance per pixel
    //float yResol = 0.0f; // in distance per pixel
    //long biXPelsPerMeter = 0L;
    //long biYPelsPerMeter = 0L;
    //byte[] strnSignature;
    // byte[] text;
    long savestrnPos;
    // byte[] JUNKsignature;
    long saveJUNKsignature;
    int paddingBytes;
    int i;

    //byte[] idx1Signature;
    //long savedbLength[];
    //long savedcLength[];
    /*
     * long idx1Pos; long endPos; int t, z;
     */

    int xMod;
    //changement on specifie une fois pour toute le type rgb
    bytesPerPixel_ = 3;

    //lutBufferRemapped = new int[1];
    /*
     * // SaveDialog sd = new SaveDialog("Save as AVI...", imp.getTitle(), ".avi"); String fileName =
     * file.getName(); if (fileName == null) return; String fileDir = sd.getDirectory(); file = new
     * File(fileDir + fileName);
     */
    raFile_ = new RandomAccessFile(file_, "rw");
    raFile_.setLength(0);
    //    imp.startTiming();
    writeString("RIFF"); // signature
    saveFileSize_ = raFile_.getFilePointer();
    // Bytes 4 thru 7 contain the length of the file. This length does
    // not include bytes 0 thru 7.
    writeInt(0); // for now write 0 in the file size location
    writeString("AVI "); // RIFF type

    // Write the first LIST chunk, which contains information on data decoding
    writeString("LIST"); // CHUNK signature
    // Write the length of the LIST CHUNK not including the first 8 bytes with LIST and
    // size. Note that the end of the LIST CHUNK is followed by JUNK.
    saveLIST1Size = raFile_.getFilePointer();
    writeInt(0); // for now write 0 in avih sub-CHUNK size location
    writeString("hdrl"); // CHUNK type
    writeString("avih"); // Write the avih sub-CHUNK
    writeInt(0x38); // Write the length of the avih sub-CHUNK (38H) not including the
    // the first 8 bytes for avihSignature and the length
    microSecPerFrame_ = (int) ((1.0 / getFrameRate()) * 1.0e6);
    //IJ.write("microSecPerFrame: "+microSecPerFrame);
    writeInt(microSecPerFrame_); // dwMicroSecPerFrame - Write the microseconds per frame
    writeInt(0); // dwMaxBytesPerSec (maximum data rate of the file in bytes per second)
    writeInt(0); // dwReserved1 - Reserved1 field set to zero
    writeInt(0x10); // dwFlags - just set the bit for AVIF_HASINDEX
    // 10H AVIF_HASINDEX: The AVI file has an idx1 chunk containing
    //   an index at the end of the file. For good performance, all
    //   AVI files should contain an index.
    tDim_ = 1;
    //zDim = nbImg_;
    xPad_ = 0;
    xMod = xDim_ % 4;
    if (xMod != 0) {
      xPad_ = 4 - xMod;
      xDim_ = xDim_ + xPad_;
    }

    writeInt(zDim_ * tDim_); // dwTotalFrames - total frame number
    writeInt(0); // dwInitialFrames -Initial frame for interleaved files.
    // Noninterleaved files should specify 0.
    writeInt(1); // dwStreams - number of streams in the file - here 1 video and zero audio.
    writeInt(0); // dwSuggestedBufferSize - Suggested buffer size for reading the file.
    // Generally, this size should be large enough to contain the largest
    // chunk in the file.
    writeInt(xDim_ - xPad_); // dwWidth - image width in pixels
    writeInt(yDim_); // dwHeight - image height in pixels
    // dwReserved[4] - Microsoft says to set the following 4 values to 0.
    writeInt(0);
    writeInt(0);
    writeInt(0);
    writeInt(0);

    // Write the Stream line header CHUNK
    writeString("LIST");
    // Write the size of the first LIST subCHUNK not including the first 8 bytes with
    // LIST and size. Note that saveLIST1subSize = saveLIST1Size + 76, and that
    // the length written to saveLIST1subSize is 76 less than the length written to saveLIST1Size.
    // The end of the first LIST subCHUNK is followed by JUNK.
    saveLIST1subSize = raFile_.getFilePointer();
    writeInt(0); // for now write 0 in CHUNK size location
    writeString("strl"); // Write the chunk type
    writeString("strh"); // Write the strh sub-CHUNK
    writeInt(56); // Write the length of the strh sub-CHUNK
    writeString("vids"); // fccType - Write the type of data stream - here vids for video stream
    // Write DIB for Microsoft Device Independent Bitmap. Note: Unfortunately,
    // at least 3 other four character codes are sometimes used for uncompressed
    // AVI videos: 'RGB ', 'RAW ', 0x00000000
    writeString("DIB ");
    writeInt(0); // dwFlags
    writeInt(0); // dwPriority
    writeInt(0); // dwInitialFrames
    writeInt(1); // dwScale
    writeInt((int) getFrameRate()); //  dwRate - frame rate for video streams
    writeInt(0); // dwStart - this field is usually set to zero
    writeInt(tDim_ * zDim_); // dwLength - playing time of AVI file as defined by scale and rate
    // Set equal to the number of frames
    writeInt(0); // dwSuggestedBufferSize - Suggested buffer size for reading the stream.
    // Typically, this contains a value corresponding to the largest chunk
    // in a stream.
    writeInt(-1); // dwQuality - encoding quality given by an integer between
    // 0 and 10,000. If set to -1, drivers use the default
    // quality value.
    writeInt(0); // dwSampleSize #
    // write 16-bit frame (unused)
    writeShort((short) 0); // left
    writeShort((short) 0); // top
    writeShort((short) 0); // right
    writeShort((short) 0); // bottom
    writeString("strf"); // Write the stream format chunk
    // Write the size of the stream format CHUNK not including the first 8 bytes for
    // strf and the size. Note that the end of the stream format CHUNK is followed by
    // strn.
    savestrfSize = raFile_.getFilePointer();
    writeInt(0); // for now write 0 in the strf CHUNK size location
    writeInt(40); // biSize - Write header size of BITMAPINFO header structure
    // Applications should use this size to determine which BITMAPINFO header structure is
    // being used. This size includes this biSize field.
    writeInt(xDim_); // biWidth - BITMAP width in pixels
    writeInt(yDim_); // biHeight - image height in pixels. If height is positive,
    // the bitmap is a bottom up DIB and its origin is in the lower left corner. If
    // height is negative, the bitmap is a top-down DIB and its origin is the upper
    // left corner. This negative sign feature is supported by the Windows Media Player, but it is
    // not
    // supported by PowerPoint.
    writeShort(1); // biPlanes - number of color planes in which the data is stored
    // This must be set to 1.
    final int bitsPerPixel = (bytesPerPixel_ == 3) ? 24 : 8;
    writeShort((short) bitsPerPixel); // biBitCount - number of bits per pixel #
    // 0L for BI_RGB, uncompressed data as bitmap
    //writeInt(bytesPerPixel*xDim*yDim*zDim*tDim); // biSizeImage #
    writeInt(0); // biSizeImage #
    writeInt(0); // biCompression - type of compression used
    writeInt(0); // biXPelsPerMeter - horizontal resolution in pixels
    writeInt(0); // biYPelsPerMeter - vertical resolution in pixels
    // per meter
    if (bitsPerPixel == 8) {
      writeInt(256); // biClrUsed
    } else {
      writeInt(0); // biClrUsed
    }
    writeInt(0); // biClrImportant - specifies that the first x colors of the color table
    // are important to the DIB. If the rest of the colors are not available,
    // the image still retains its meaning in an acceptable manner. When this
    // field is set to zero, all the colors are important, or, rather, their
    // relative importance has not been computed.
    // Write the LUTa.getExtents()[1] color table entries here. They are written:
    // blue byte, green byte, red byte, 0 byte
    if (bytesPerPixel_ == 1) {
      FuLog.warning(new Throwable("non supporte"));
      //createLUT();
      // raFile.write(lutWrite);
    }

    // Use strn to provide a zero terminated text string describing the stream
    savestrnPos = raFile_.getFilePointer();
    raFile_.seek(savestrfSize);
    writeInt((int) (savestrnPos - (savestrfSize + 4)));
    raFile_.seek(savestrnPos);
    writeString("strn");
    writeInt(16); // Write the length of the strn sub-CHUNK
    writeString("FileAVI write  ");
    raFile_.write(0); // write string termination byte

    // write a JUNK CHUNK for padding
    saveJUNKsignature = raFile_.getFilePointer();
    raFile_.seek(saveLIST1Size);
    writeInt((int) (saveJUNKsignature - (saveLIST1Size + 4)));
    raFile_.seek(saveLIST1subSize);
    writeInt((int) (saveJUNKsignature - (saveLIST1subSize + 4)));
    raFile_.seek(saveJUNKsignature);
    writeString("JUNK");
    paddingBytes = (int) (4084 - (saveJUNKsignature + 8));
    writeInt(paddingBytes);
    final int stop=paddingBytes / 2;
    for (i = 0; i < stop; i++){
      writeShort((short) 0);
    }

    // Write the second LIST chunk, which contains the actual data
    writeString("LIST");
    // Write the length of the LIST CHUNK not including the first 8 bytes with LIST and
    // size. The end of the second LIST CHUNK is followed by idx1.
    saveLIST2Size_ = raFile_.getFilePointer();
    writeInt(0); // For now write 0
    savemovi_ = raFile_.getFilePointer();
    writeString("movi"); // Write CHUNK type 'movi'
    savedbLength_ = new long[tDim_ * zDim_];
    //savedcLength = new long[tDim * zDim];
    //dcLength = new int[tDim * zDim];
  }

  /**
   * Ferme l'ecriture.
   * @throws IOException
   */
  public void close() throws IOException{
    if(raFile_!=null){
      raFile_.close();
    }
  }

  // Write the data. Each 3-byte triplet in the bitmap array represents the relative intensities
  // of blue, green, and red, respectively, for a pixel. The color bytes are in reverse order
  // from the Windows convention.
  //bufferWrite = new byte[bytesPerPixel * xDim * yDim];

  /*
   * for (z = 0; z < zDim; z++) { raFile.write(dataSignature); savedbLength[z] =
   * raFile.getFilePointer(); writeInt(bytesPerPixel * xDim * yDim); // Write the data length
   * writeRGBFrame(z + 1); }
   */
  /**
   * Ferme l'ecriture.
   * @return true si ok
   * @throws IOException
   */
  public boolean writeEnd() throws IOException{
    if (error_ || (idxFrame_ != zDim_)) {
      raFile_.close();
      return false;
    }
    // Write the idx1 CHUNK
    // Write the 'idx1' signature
    final long idx1Pos = raFile_.getFilePointer();
    raFile_.seek(saveLIST2Size_);
    writeInt((int) (idx1Pos - (saveLIST2Size_ + 4)));
    raFile_.seek(idx1Pos);
    writeString("idx1");
    // Write the length of the idx1 CHUNK not including the idx1 signature and the 4 length
    // bytes. Write 0 for now.
    saveidx1Length_ = raFile_.getFilePointer();
    writeInt(0);
    for (int z = 0; z < zDim_; z++) {
      // In the ckid field write the 4 character code to identify the chunk 00db or 00dc
      raFile_.write(dataSignature_);
      if (z == 0) {
        writeInt(0x10); // Write the flags - select AVIIF_KEYFRAME
      }
      else {
        writeInt(0x00);
      }
      // AVIIF_KEYFRAME 0x00000010L
      // The flag indicates key frames in the video sequence.
      // Key frames do not need previous video information to be decompressed.
      // AVIIF_NOTIME 0x00000100L The CHUNK does not influence video timing(for
      //   example a palette change CHUNK).
      // AVIIF_LIST 0x00000001L Marks a LIST CHUNK.
      // AVIIF_TWOCC 2L
      // AVIIF_COMPUSE 0x0FFF0000L These bits are for compressor use.
      writeInt((int) (savedbLength_[z] - 4 - savemovi_));
      // Write the offset (relative to the 'movi' field) to the relevant CHUNK
      writeInt(bytesPerPixel_ * xDim_ * yDim_); // Write the length of the relevant
      // CHUNK. Note that this length is
      // also written at savedbLength
    } // for (z = 0; z < zDim; z++)
    final long endPos = raFile_.getFilePointer();
    raFile_.seek(saveFileSize_);
    writeInt((int) (endPos - (saveFileSize_ + 4)));
    raFile_.seek(saveidx1Length_);
    writeInt((int) (endPos - (saveidx1Length_ + 4)));
    raFile_.close();
    return true;

    //IJ.showProgress(1.0);
  }

  /*
   * public void writeByteFrame(int slice) throws IOException{ ImageProcessor ip =
   * imp.getStack().getProcessor(slice); ip = ip.convertToByte(true); byte[] pixels = (byte[])
   * ip.getPixels(); int width = imp.getWidth(); int height = imp.getHeight(); int c, offset, index =
   * 0; for (int y = height - 1; y >= 0; y--) { offset = y * width; for (int x = 0; x < width; x++)
   * bufferWrite[index++] = pixels[offset++]; for (int i = 0; i < xPad; i++) bufferWrite[index++] =
   * (byte) 0; } raFile.write(bufferWrite); }
   */

  int idxFrame_;
  boolean error_;
  //int[] pixels;

  /**
   * @param _i l'image a ajouter
   * @throws IOException
   */
  public void writeRGBFrame(final BufferedImage _i) throws IOException{
    if(error_) {
      return;
    }
    raFile_.write(dataSignature_);
    savedbLength_[idxFrame_++] = raFile_.getFilePointer();
    writeInt(bytesPerPixel_ * xDim_ * yDim_); // Write the data length
    int c;
    int  /*offset=0, */index = 0;
   /* if(pixels==null)
    pixels = new int[yDim * xDim];*/
    if(bufferWrite_==null) {
      bufferWrite_ = new byte[3 * xDim_ * yDim_];
      //_i.getData().getPixel(0, 0, pixels);
/*    PixelGrabber pg = new PixelGrabber(_i, 0, 0, xDim, yDim, pixels, 0, xDim);
      try {
        pg.grabPixels();
            catch (InterruptedException e) {
        e.printStackTrace();
      }
      }*/
      //_i.getRGB(0, 0, xDim, yDim, pixels,0, xDim);
    }

    if(_i.getHeight()!=initY_) {
      error_=true;
    }
    if(_i.getWidth()!=initX_){
      error_=true;
    }
    if(error_) {
      return;
    }

    for (int y = yDim_ - 1; y >= 0; y--) {
     // offset = y * xDim;
      for (int x = 0; x < initX_; x++) {
        //c = pixels[offset++];
        c=_i.getRGB(x, y);
        //c=_i.getColorModel().getRGB(offset++);
        bufferWrite_[index++] = (byte) (c & 0xff); // blue
        bufferWrite_[index++] = (byte) ((c & 0xff00) >> 8); //green
        bufferWrite_[index++] = (byte) ((c & 0xff0000) >> 16); // red
      }
      for (int i = 0; i < xPad_; i++) {
        bufferWrite_[index++] = (byte) 0;
        bufferWrite_[index++] = (byte) 0;
        bufferWrite_[index++] = (byte) 0;
      }
    }
    raFile_.write(bufferWrite_);
  }

  /*
   * public void createLUT(){ LookUpTable lut = imp.createLut(); IndexColorModel cm =
   * (IndexColorModel) lut.getColorModel(); int mapSize = cm.getMapSize(); lutWrite = new byte[4 *
   * 256]; for (int i = 0; i < 256; i++) { if (i < mapSize) { lutWrite[4 * i] = (byte)
   * cm.getBlue(i); lutWrite[4 * i + 1] = (byte) cm.getGreen(i); lutWrite[4 * i + 2] = (byte)
   * cm.getRed(i); lutWrite[4 * i + 3] = (byte) 0; } } }
   */

  double rate_;

  double getFrameRate(){
    double rate = rate_;
    if (rate <= 1.0) {
      rate = 1.0;
    }
    if (rate > 60.0) {
      rate = 60.0;
    }
    return rate;
  }

  final void writeString(final String _s) throws IOException{
    //byte[] bytes = s.getBytes();
    final  byte[] bytes = _s.getBytes("UTF8");
    raFile_.write(bytes);
  }

  final void writeInt(final int _v) throws IOException{
    raFile_.write(_v & 0xFF);
    raFile_.write((_v >>> 8) & 0xFF);
    raFile_.write((_v >>> 16) & 0xFF);
    raFile_.write((_v >>> 24) & 0xFF);
  }

  final void writeShort(final int _v) throws IOException{
    raFile_.write(_v & 0xFF);
    raFile_.write((_v >>> 8) & 0xFF);
  }

}