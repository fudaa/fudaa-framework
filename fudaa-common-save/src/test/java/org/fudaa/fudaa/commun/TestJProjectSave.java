/*
 * @creation 11 sept. 06
 * @modification $Date: 2006-10-19 14:15:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.io.File;
import java.io.FileOutputStream;

import junit.framework.TestCase;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;

import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;

import org.fudaa.fudaa.commun.save.FudaaCommentModel;
import org.fudaa.fudaa.commun.save.FudaaSaveProject;
import org.fudaa.fudaa.commun.save.FudaaSaveZipLoader;
import org.fudaa.fudaa.commun.save.FudaaSaveZipWriter;

/**
 * @author fred deniger
 * @version $Id: TestJProjectSave.java,v 1.5 2006-10-19 14:15:26 deniger Exp $
 */
public class TestJProjectSave extends TestCase {

  public void testVersionToXml() {
    final FudaaCommentModel model = createComment();
    try {
      final File f = File.createTempFile("toto", ".xml");
      assertNotNull(f);
      final FileOutputStream fileOutputStream = new FileOutputStream(f);
      final CtuluXmlWriter ctuluXmlWriter = new CtuluXmlWriter(fileOutputStream);
      model.writeIn(ctuluXmlWriter);
      ctuluXmlWriter.finish();
      fileOutputStream.close();
      final CtuluXmlReaderHelper ctuluXmlReaderHelper = new CtuluXmlReaderHelper(f);
      final FudaaCommentModel m2 = FudaaCommentModel.load(ctuluXmlReaderHelper);
      assertTrue(model.isSame(m2));
      if (f != null) {
        f.delete();
      }
    } catch (final Exception _evt) {
      _evt.printStackTrace();
      fail();
    }

  }

  private FudaaCommentModel createComment() {
    final FudaaCommentModel model = new FudaaCommentModel();
    model.create().setData("bonjour \n de Fred");
    model.create().setData("Salut");
    return model;
  }

  public void testDocToXml() {
    File f = null;
    try {
      f = File.createTempFile("toto", ".fzip");
      assertNotNull(f);
      final BuInformationsDocument doc = new BuInformationsDocument(null);
      final BuInformationsSoftware soft=new BuInformationsSoftware();
      soft.name="TesterSoft";
      soft.version="0.01";
      doc.author = "Auteur";
      doc.comment = "commentaires\n sur des lignes";
//      doc.logo = FudaaResource.FUDAA.getIcon("fudaa-logo.png");
      doc.data = createComment();
      final FudaaSaveZipWriter writer = new FudaaSaveZipWriter(f);
      FudaaSaveProject.saveIn(writer, doc,soft);
      writer.close();
      final FudaaSaveZipLoader loader = new FudaaSaveZipLoader(f);
      final BuInformationsDocument doc2 = FudaaSaveProject.loadInfo(loader);
      loader.close();
      assertEquals(doc.author, doc2.author);
      assertEquals(doc.comment, doc2.comment);
      //assertNotNull(doc2.logo);
      assertTrue(doc.data instanceof FudaaCommentModel);
      assertTrue(((FudaaCommentModel) doc.data).isSame((FudaaCommentModel) doc.data));

    } catch (final Exception _evt) {
      _evt.printStackTrace();
      fail();
    } finally {
      if (f != null) {
        f.delete();
      }
    }

  }
}
