/*
 *  @creation     25 ao�t 2005
 *  @modification $Date: 2008-01-10 09:58:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.save;

import org.fudaa.ctulu.CtuluSavable;



/**
 * Une interface a implementer pour les composants persistants (stockables sur fichiers projet).
 * 
 * @author Fred Deniger
 * @version $Id: FudaaSavable.java,v 1.3.2.1 2008-01-22 10:00:15 bmarchan Exp $
 */
public interface FudaaSavable extends CtuluSavable {

}
