/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2008-04-01 07:20:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.save;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.Configuration;
import com.db4o.config.ObjectConstructor;
import com.db4o.ext.DatabaseFileLockedException;
import com.db4o.query.Query;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.GeometryFactory;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.fudaa.commun.FudaaLib;

import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: FudaaSaveLib.java,v 1.10.4.1 2008-04-01 07:20:52 bmarchan Exp $
 */
public final class FudaaSaveLib {
  private static boolean isConfigure_;

  static {
    configureDb4o();
  }

  public static synchronized void configureDb4o() {
    if (isConfigure_) {
      return;
    }
    isConfigure_ = true;
    final Configuration conf = Db4o.configure();
    // conf.objectClass(FilleVisuSaver.class).cascadeOnDelete(true);
    // conf.objectClass(FilleVisuSaver.class).cascadeOnUpdate(true);
    conf.objectClass(PropertyChangeSupport.class).callConstructor(false);
    conf.allowVersionUpdates(true);
    conf.weakReferences(false);
    if (Fu.DEBUG) {
      conf.exceptionsOnNotStorable(true);
    }
    conf.callConstructors(true);

    final ObjectConstructor geomFactory = new ObjectConstructor() {
      @Override
      public void onActivate(ObjectContainer _container, Object _applicationObject, Object _storedObject) {
      }

      @Override
      public Object onInstantiate(ObjectContainer _container, Object _storedObject) {
        return GISGeometryFactory.INSTANCE;
      }

      @Override
      public Object onStore(ObjectContainer _container, Object _applicationObject) {
        // new Throwable().printStackTrace();
        return GISGeometryFactory.INSTANCE;
      }

      @Override
      public Class storedClass() {
        return GeometryFactory.class;
      }
    };
    Db4o.configure().objectClass(GISGeometryFactory.class).translate(geomFactory);
    Db4o.configure().objectClass(GeometryFactory.class).translate(geomFactory);

    Db4o.configure().objectClass(GISAttributeConstants.BATHY.getClass()).translate(new ObjectConstructor() {
      @Override
      public void onActivate(final ObjectContainer _container, final Object _applicationObject,
                             final Object _storedObject) {
      }

      @Override
      public Object onInstantiate(final ObjectContainer _container, final Object _storedObject) {
        return GISAttributeConstants.BATHY;
      }

      @Override
      public Object onStore(final ObjectContainer _container, final Object _applicationObject) {
        return _applicationObject;
      }

      @Override
      public Class storedClass() {
        return GISAttributeInterface.class;
      }
    });
    Db4o.configure().objectClass(GISAttributeConstants.TITRE.getClass()).translate(new ObjectConstructor() {
      @Override
      public void onActivate(final ObjectContainer _container, final Object _applicationObject,
                             final Object _storedObject) {
      }

      @Override
      public Object onInstantiate(final ObjectContainer _container, final Object _storedObject) {
        return GISAttributeConstants.TITRE;
      }

      @Override
      public Object onStore(final ObjectContainer _container, final Object _applicationObject) {
        return _applicationObject;
      }

      @Override
      public Class storedClass() {
        return GISAttributeInterface.class;
      }
    });
  }

  public static int count(final ObjectContainer _db, final Class _c) {
    if (_db == null) {
      return 0;
    }
    final Query q = _db.query();
    q.constrain(_c);
    return q.execute().size();
  }

  /**
   * Supprime tous les objets d'une classe donn�e
   *
   * @param _db La base de donn�es.
   * @param _c La classe des objets a supprimer.
   */
  public static void deleteAll(final ObjectContainer _db, final Class _c) {
    if (_db == null) {
      return;
    }
    final Query q = _db.query();
    q.constrain(_c);
    deleteAll(_db, q);
  }

  /**
   * Supprime tous les objets d'un set resultant.
   *
   * @param _db La base de donn�es
   * @param _set Le set.
   */
  public static void deleteAll(final ObjectContainer _db, final ObjectSet _set) {
    if (_db == null) {
      return;
    }
    while (_set.hasNext()) {
      _db.delete(_set.next());
    }
  }

  /**
   * Supprime tous les objets d'une requete.
   *
   * @param _db La base de donn�es
   * @param _q La requete.
   */
  public static void deleteAll(final ObjectContainer _db, final Query _q) {
    if (_db != null) {
      for (final ObjectSet s = _q.execute(); s.hasNext(); ) {
        final Object next = s.next();
        if (next != null) {
          _db.delete(next);
        }
      }
    }
  }

  public static void deleteAllBut(final ObjectContainer _db, final Query _q, final Class _c) {
    if (_db != null) {
      for (final ObjectSet s = _q.execute(); s.hasNext(); ) {
        final Object next = s.next();
        if (next != null && !next.getClass().equals(_c)) {
          _db.delete(next);
        }
      }
    }
  }

  public static String getActionSaveTitle() {
    return FudaaLib.getS("Sauvegarde donn�es compl�mentaires");
  }

  public static FudaaSaveMainData getProjectData(final File _zip) {
    return (FudaaSaveMainData) getUniqueDataInZip(_zip, FudaaSaveMainData.class);
  }

  public static FudaaSaveMainData getProjectDataOld(final File _file) {
    if (_file == null) {
      return null;
    }
    final File dbFile = _file;
    if (!dbFile.exists()) {
      return null;
    }
    final ObjectContainer container = Db4o.openFile(dbFile.getAbsolutePath());
    FudaaSaveMainData res = null;
    try {
      res = getProjectMainData(container);
    } finally {
      if (container != null) {
        if (Fu.DEBUG) {
          FuLog.debug("FTR: close db");
        }
        container.close();
      }
    }
    return res;
  }

  public static Object getUniqueDataInDb(final File _dbFile, final Class _class) {
    if (_dbFile == null) {
      return null;
    }
    final File dbFile = _dbFile;
    if (!dbFile.exists()) {
      return null;
    }
    ObjectContainer container = null;
    Object res = null;
    try {
      container = Db4o.openFile(dbFile.getAbsolutePath());
      res = getUniqueData(_class, container);
    } finally {
      if (container != null) {
        if (Fu.DEBUG) {
          FuLog.debug("FTR: close db");
        }
        container.close();
      }
    }
    return res;
  }

  public static FudaaSaveMainData getProjectMainData(final ObjectContainer _cont) {
    if (_cont == null) {
      return null;
    }
    final FudaaSaveMainData data = (FudaaSaveMainData) getUniqueData(FudaaSaveMainData.class, _cont);
    return data == null ? null : data;
  }

  public static FileFormatSoftware getProjectVersion(final ObjectContainer _cont) {
    return (FileFormatSoftware) getUniqueData(FileFormatSoftware.class, _cont);
  }

  /**
   * @param _class la classe recherch�e
   * @param _db la base de donn�e a interroger
   * @return l'unique objet de class _class. Sinon null
   */
  public static Object getUniqueData(final Class _class, final ObjectContainer _db) {
    if (_db == null) {
      return null;
    }
    final Query query = _db.query();
    query.constrain(_class);
    final ObjectSet res = query.execute();
    if (res.size() != 1) {
      return null;
    }
    return res.next();
  }

  public static Object getUniqueDataInZip(final File _zip, final Class _clazz) {
    FudaaSaveZipLoader loader = null;
    Object res = null;
    try {
      loader = new FudaaSaveZipLoader(_zip);
      res = getUniqueData(_clazz, loader.getDb());
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    } finally {
      if (loader != null) {
        try {
          loader.close();
        } catch (final IOException _evt) {
          FuLog.error(_evt);
        }
      }
    }
    return res;
  }

  public static void printAll(final ObjectContainer _db, final Query _q) {
    if (_db != null) {
      for (final ObjectSet s = _q.execute(); s.hasNext(); ) {
        final Object next = s.next();
        if (next != null) {
          FuLog.debug(next.getClass().getName() + ": " + next);
        }
      }
    }
  }

  private FudaaSaveLib() {

  }

  public static String getNoGeogLayerOpt() {
    return "option.noGeogLayer";
  }

  public static Runnable restoreMainFille(final BuCommonImplementation _impl, final File _zipFile, final File _oldFile,
                                          final ZEbliFilleCalques _fille, final ProgressionInterface _prog) {
    Runnable res = null;
    final File zip = _zipFile;

    if (zip != null && zip.exists() && _fille != null) {
      res = FudaaSaveLib.restoreFille(_impl, _fille, _prog, zip);
    } else {
      final File oldDb = _oldFile;
      // ancienne version
      if (oldDb != null && oldDb.exists()) {
        ObjectContainer db = null;
        try {
          db = Db4o.openFile(oldDb.getAbsolutePath());
          final FudaaFilleVisuPersistence persi = new FudaaFilleVisuPersistence(_fille);
          if (db != null) {
            res = persi.restore(_impl, db, _prog);
          }
        } catch (final DatabaseFileLockedException _evt) {
          FuLog.error(_evt);
        } finally {
          if (db != null) {
            db.close();
          }
        }
      }
    }
    return res;
  }

  public static void restoreAndLaunch(final BuCommonImplementation _impl, final ZEbliFilleCalques _visu,
                                      final ProgressionInterface _prog, final FudaaSaveZipLoader _loader) {
    final Runnable r = FudaaSaveLib.restoreFille(_impl, _visu, _prog, _loader);
    if (r != null) {
      BuLib.invokeLater(r);
    }
  }

  public static void restoreAndLaunch(final BuCommonImplementation _impl, final ZEbliCalquesPanel _visu,
                                      final ProgressionInterface _prog, final FudaaSaveZipLoader _loader) {
    final Runnable r = FudaaSaveLib.restoreFille(_impl, _visu, _prog, _loader);
    if (r != null) {
      BuLib.invokeLater(r);
    }
  }

  public static Runnable restoreFille(final BuCommonImplementation _impl, final ZEbliFilleCalques _visu,
                                      final ProgressionInterface _prog, final FudaaSaveZipLoader _loader) {
    return restoreFille(_impl, _visu.getVisuPanel(), _prog, _loader);
  }

  /**
   * Restaure les calques du projet � partir du zip loader associ� au projet.
   *
   * @param _impl L'implementation.
   * @param _visu La fille des calques.
   * @param _prog L'interface de progression de tache.
   * @param _loader Le zip loader contenant l'�tat du projet.
   * @return The thread qui remettra a jour les propri�t�s graphiques de la fenetre, tout a la fin.
   */
  public static Runnable restoreFille(final BuCommonImplementation _impl, final ZEbliCalquesPanel _visu,
                                      final ProgressionInterface _prog, final FudaaSaveZipLoader _loader) {
    Runnable res = null;
    try {

      final FudaaFilleVisuPersistence fudaaFilleVisuPersistence = new FudaaFilleVisuPersistence(_visu);
      if (_loader != null) {
        res = fudaaFilleVisuPersistence.restore(_impl, _loader, _prog);
      }
    } finally {
      if (_loader != null) {
        try {
          _loader.close();
        } catch (final IOException _evt) {
          FuLog.error(_evt);
        }
      }
    }
    return res;
  }

  /**
   * Restaure les calques du projet � partir du fichier zip associ� au projet.
   *
   * @param _impl L'implementation.
   * @param _visu La fille des calques.
   * @param _prog L'interface de progression de tache.
   * @param _zip Le fichier zip contenant l'�tat du projet.
   * @return The thread de tache.
   */
  public static Runnable restoreFille(final BuCommonImplementation _impl, final ZEbliFilleCalques _visu,
                                      final ProgressionInterface _prog, final File _zip) {
    try {
      final FudaaSaveZipLoader loader = new FudaaSaveZipLoader(_zip);
      return restoreFille(_impl, _visu, _prog, loader);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    }
    return null;
  }
}
