/*
 * @creation     5 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.save;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.beans.PropertyVetoException;
import java.util.Properties;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuInternalFrame;

/**
 * Un saver pour une fenetre interne, sauvegard� en db4o. Ne peut pas �tre d�riv�. Si la fenetre sauvegarde des infos particuli�res,
 * elles peuvent �tre mises dans le tableau des propri�t�s.
 * @author Bertrand Marchand
 * @version $Id$
 */
public final class FilleSaver {

  /** Le nom de la fenetre */
  protected String name_;
  /** La localisation de la fenetre */
  protected Point ifLocation_;
  /** La dimension de la fenetre */
  protected Dimension ifDim_;
  /** Est-elle contenue par le desktop ? */
  protected boolean isVisible_;
  /** Est-elle iconifie ? */
  protected boolean isIconified_;
  /** Est-elle selectionn�e ? */
  protected boolean isSelected_;
  /** Proprietes autres (specifiques de la fenetre) */
  protected Properties props_;

  /**
   * 
   */
  public FilleSaver() {
    super();
  }
  
  /**
   * Sauvegarde les propri�t�s standards, la taille, la position, etc.
   * @param _f La fenetre
   * @param _dk Le desktop.
   */
  public void setStandardData(final BuInternalFrame _f, final BuDesktop _dk) {
    name_ = _f.getName();
    ifDim_ = _f.getSize();
    ifLocation_ = _f.getLocation();
    isIconified_=_f.isIcon();
    // On verifie que la fenetre est contenue par le desktop. Sinon, visibilt� a false, pour ne pas la
    // reafficher � la restitution.
    if (!isIconified_) {
      isVisible_=false;
      for (Component c : _dk.getComponents()) {
        if (c==_f) {
          isVisible_=true;
          break;
        }
      }
    }
    else {
      isVisible_=true;
    }
    
    isSelected_=_f.isSelected();
  }

  /**
   * Restitue les propri�t�s standards. La fenetre doit exister. Elle peut �tre
   * contenue ou non par le desktop.
   * @param _f La fenetre
   * @param _dk Le desktop.
   */
  public void restoreStandardData(final BuInternalFrame _f, final BuDesktop _dk) {
    _f.setSize(ifDim_);
    _f.setLocation(ifLocation_);
    _f.setVisible(isVisible_);
    try {
      _f.setIcon(isIconified_);
      _f.setSelected(isSelected_);
    }
    catch (PropertyVetoException _exc) {}
  }
  
  public Properties getSpecificProps() {
    return props_;
  }
  
  public void setSpecificProps(Properties _props) {
    props_=_props;
  }
}