/*
 * @creation 11 sept. 06
 * 
 * @modification $Date: 2008-01-22 10:00:14 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.save;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.fudaa.commun.FudaaLib;
import org.xml.sax.SAXException;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;

/**
 * Sauvegarde/chargement des informations g�n�rales d'un projet. Ces informations sont stock�es dans le fichier projet
 * de l'application, fichier zipp� et structur� suivant la proposition de F.Deniger.
 * 
 * @author fred deniger
 * @version $Id: FudaaSaveProject.java,v 1.5.2.1 2008-01-22 10:00:14 bmarchan Exp $
 */
public final class FudaaSaveProject {
  private FudaaSaveProject() {
  }

  /**
   * Enregistre les informations li�es au syst�me de mod�lisation (syst�me, version, etc.) sur le flux courant.
   * 
   * @param _writer Le flux pour l'ecriture.
   * @param _soft Les informations syst�me de mod�lisation. Peut �tre null.
   * @throws IOException En cas d'erreur d'�criture dans le zip.
   */
  public static void saveIn(final FudaaSaveZipWriter _writer, final FileFormatSoftware _soft) throws IOException {
    if (_soft != null) {
      _writer.startEntry("modelling-system.txt");
      // ne pas fermer le flux
      final Writer out = new BufferedWriter(new OutputStreamWriter(_writer.getOutStream()));
      out.write("#NE PAS EDITER: description du syt�me de mod�lisation utilis�");
      out.write(CtuluLibString.LINE_SEP);
      out.write("#DO NOT EDIT: descripion of the modelling system");
      out.write(CtuluLibString.LINE_SEP);
      _soft.toProperties().store(_writer.getOutStream(), null);
    }

  }

  /**
   * Enregistre les donn�es g�n�rales du document sur le flux courant. Le nom de l'auteur, le titre, etc.
   * 
   * @param _writer Le flux pour l'ecriture.
   * @param _doc Les donn�es g�n�rales du document. Peut �tre null.
   * @param _soft Les infos du soft qui a g�n�r� le document. Peut �tre null.
   * @throws IOException En cas d'erreur d'�criture dans le zip.
   */
  public static void saveIn(final FudaaSaveZipWriter _writer, final BuInformationsDocument _doc, final BuInformationsSoftware _soft)
      throws IOException {
    if (_doc != null) {
      String logo = null;
      _writer.startEntry("project.xml");
      saveOnProjectXML(_writer.getOutStream(), _doc, _soft);

      if (logo != null) {
        _writer.startEntry(logo);
        ImageIO.write(BuLib.convert(_doc.logo.getImage()), "png", _writer.getOutStream());
        _writer.getOutStream().flush();
      }
    }

  }

  public static void saveOnProjectXML(final OutputStream _os, final BuInformationsDocument _doc, final BuInformationsSoftware _soft)
      throws IOException {
    String logo = null;

    final CtuluXmlWriter writer = new CtuluXmlWriter(_os);
    writer.writeComment("Description du projet");
    writer.writeComment("Project's description");
    writer.setMainTag("document");
    writer.writeComment("Dernier enregistrement");
    writer.writeComment("Last save session");
    writer.writeEntry("date", FudaaLib.getDefaultDateFormat().format(new Date()));

    // Infos sur le software.
    if (_soft != null) {
      if (_soft.name != null) {
        writer.writeComment("Nom de l'application");
        writer.writeComment("Software name");
        writer.writeEntry("software.name", _soft.name);
      }
      if (_soft.version != null) {
        writer.writeComment("Version de l'application");
        writer.writeComment("Software version");
        writer.writeEntry("software.version", _soft.version);
      }
    }
    if (_doc.name != null) {
      writer.writeComment("Le nom du projet");
      writer.writeComment("The title of the project");
      writer.writeEntry("title", _doc.name);
    }
    writer.write(CtuluLibString.LINE_SEP);
    writer.writeComment("Le responsable");
    writer.writeComment("The author");
    writer.write(CtuluLibString.LINE_SEP);
    if (_doc.author != null) {
      writer.writeComment("Le responsable du projet");
      writer.writeComment("The responsable of the author");
      writer.writeEntry("author", _doc.author);
    }
    if (_doc.organization != null) {
      writer.writeComment("L'organisation du responsable");
      writer.writeComment("The  author's organization");
      writer.writeEntry("organization", _doc.organization);
    }
    if (_doc.department != null) {
      writer.writeComment("Le d�partement du responsable");
      writer.writeComment("The  author's department");
      writer.writeEntry("department", _doc.department);
    }
    if (_doc.contact != null) {
      writer.writeComment("Un contact pour le responsable (email, tel, ..)");
      writer.writeComment("how to contact the author (email, phone, ...)");
      writer.writeEntry("contact", _doc.contact);
    }
    if (_doc.logo != null) {
      writer.writeComment("Le nom du fichier image contenant le logo ( present dans l'archive)");
      writer.writeComment("The filename of the logo");
      logo = "author-logo.png";
      writer.writeEntry("logo", logo);
    }
    writer.write(CtuluLibString.LINE_SEP);
    writer.writeComment("Description du projet");
    writer.writeComment("Project's description");
    if (_doc.comment != null) {
      writer.writeComment("Description g�n�ral sur le projet");
      writer.writeComment("General description of the project");
      writer.writeEntry("description", _doc.comment);
    }
    writer.write(CtuluLibString.LINE_SEP);
    final Object o = _doc.data;
    if (o != null && o instanceof FudaaCommentModel) {
      ((FudaaCommentModel) o).writeIn(writer);
    }
    writer.finish();
  }

  /**
   * Charge les donn�es g�n�rales du projet depuis le flux courant. Le nom de l'auteur, le titre, etc.
   * 
   * @param _loader Le flux pour le chargement.
   * @return Les donn�es g�n�rales.
   * @throws IOException En cas d'erreur de lecture, si le fichier n'est pas pr�sent dans le zip.
   */
  public static BuInformationsDocument loadInfo(final FudaaSaveZipLoader _loader) throws IOException {
    if (_loader == null) {
      return null;
    }
    final InputStream in = _loader.getInputStream("project.xml");
    if (in == null) {
      return null;
    }
    CtuluXmlReaderHelper reader = null;
    try {
      reader = new CtuluXmlReaderHelper(in);
    } catch (final SAXException _evt) {
      FuLog.error(_evt);

    } catch (final ParserConfigurationException _evt) {
      FuLog.error(_evt);

    }
    if (reader == null) {
      return null;
    }
    final BuInformationsDocument doc = new BuInformationsDocument(null);
    doc.date = reader.getTrimTextFor("date");
    doc.author = reader.getTrimTextFor("author");
    doc.organization = reader.getTrimTextFor("organization");
    doc.department = reader.getTrimTextFor("department");
    doc.contact = reader.getTrimTextFor("contact");
    final String logo = reader.getTrimTextFor("logo");
    doc.comment = reader.getTextFor("description");
    doc.data = FudaaCommentModel.load(reader);
    if (logo != null) {
      InputStream inputStream = _loader.getInputStream(logo);
      if (inputStream != null) {
        doc.logo = new BuIcon(ImageIO.read(inputStream));
      }
    }
    return doc;

  }

  /**
   * Charge les infos software du projet depuis le zip. Le nom de l'application, la version, etc.
   * 
   * @param _loader Le zip pour le chargement.
   * @return Les infos software.
   * @throws IOException En cas d'erreur de lecture, ou si le fichier n'est pas pr�sent dans le zip.
   */
  public static BuInformationsSoftware getSoftwareInfos(final FudaaSaveZipLoader _loader) throws IOException {
    if (_loader == null) {
      return null;
    }
    final InputStream in = _loader.getInputStream("project.xml");
    return getSoftwareInfos(in);
  }

  /**
   * Charge les infos software du projet depuis le flux courant. Le nom de l'application, la version, etc.
   * 
   * @param _loader Le flux pour le chargement.
   * @return Les infos software.
   * @throws IOException En cas d'erreur de lecture.
   */
  public static BuInformationsSoftware getSoftwareInfos(final InputStream in) throws IOException {
    if (in == null) {
      return null;
    }
    CtuluXmlReaderHelper reader = null;
    try {
      reader = new CtuluXmlReaderHelper(in);
    } catch (final SAXException _evt) {
      FuLog.error(_evt);

    } catch (final ParserConfigurationException _evt) {
      FuLog.error(_evt);

    }
    if (reader == null) {
      return null;
    }
    final BuInformationsSoftware soft = new BuInformationsSoftware();
    soft.name = reader.getTrimTextFor("software.name");
    soft.version = reader.getTrimTextFor("software.version");
    return soft;

  }
  
  /**
   * Charge les informations fichier (version, format, etc.) depuis le flux courant.
   * 
   * @param _loader Le flux pour le chargement.
   * @return Les informations fichier.
   * @throws IOException En cas d'erreur de lecture, si le fichier n'est pas pr�sent dans le zip.
   */
  public static FileFormatSoftware getProjectVersion(final FudaaSaveZipLoader _loader) throws IOException {
    return FileFormatSoftware.createFromProperties(_loader.getInputStream("modelling-system.txt"));
  }

  /**
   * Charge les informations fichier (version, format, etc.) � partir d'un fichier non ouvert.
   * 
   * @param _file Le fichier sur lequel lire.
   * @return Les informations fichier.
   */
  public static FileFormatSoftware getProjectVersion(final File _file) {
    FudaaSaveZipLoader loader = null;
    FileFormatSoftware res = null;
    try {
      loader = new FudaaSaveZipLoader(_file);
      res = getProjectVersion(loader);
    } catch (final IOException _evt) {
      FuLog.warning(_evt);

    } finally {
      if (loader != null) {
        try {
          loader.close();
        } catch (final IOException _evt) {
          FuLog.error(_evt);

        }
      }
    }
    return res;
  }
}
