/*
 * @creation 4 sept. 06
 * @modification $Date: 2007/01/08 15:38:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.save;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.fudaa.ctulu.CtuluArkLoader;

import org.fudaa.ebli.calque.BCalquePersistenceGroupe;
import org.fudaa.ebli.calque.BCalqueSaverInterface;

/**
 * @author fred deniger
 * @version $Id: FudaaSaveZipLoaderAdapter.java,v 1.3 2007/01/08 15:38:19 deniger Exp $
 */
public abstract class FudaaSaveZipLoaderAdapter implements CtuluArkLoader {
  final FudaaSaveZipLoader loader_;

  public FudaaSaveZipLoaderAdapter(final FudaaSaveZipLoader _loader) {
    super();
    loader_ = _loader;
  }
  
  @Override
  public String getOption(final String _key) {
    return loader_.getOption(_key);
  }
  
  @Override
  public String getLayerProperty(String _parentDir, String _entryName, String _prop) {
    return loader_.getLayerProperty(_parentDir, _entryName, _prop);
  }

  @Override
  public File getDestDir() {
    return loader_.getDestDir();
  }

  @Override
  public String[] getEntries(final String _parentDir) {
    return loader_.getEntries(_parentDir);
  }

  @Override
  public InputStream getReader(final String _parentDir, final String _entryName) {
    return loader_.getReader(_parentDir, _entryName);
  }

  @Override
  public boolean isEntryFound(final String _parentDir, final String _entryName) {
    return loader_.isEntryFound(_parentDir, _entryName);
  }

  public static class FilleCalque extends FudaaSaveZipLoaderAdapter {
    final Map idData_;

    public  FilleCalque(final FudaaSaveZipLoader _loader, final BCalqueSaverInterface _saver) {
      super(_loader);
      idData_ = new HashMap();
      if (_saver != null) {
        putDataInMap(_saver);
      }
    }

    @Override
    public Object getDbDataFor(final String _parentDir, final String _entryName) {
      if (idData_.isEmpty()) {
        return null;
      }
      final String id = FudaaSaveZipWriter.getIdFromEntry(_entryName);
      if (id != null) {
        return idData_.get(id);
      }

      return null;
    }

    protected void putDataInMap(final BCalqueSaverInterface _saver) {
      final String id = _saver.getId();
      if (idData_.containsKey(id)) {
        return;
      }
      if (id != null) {
        idData_.put(_saver.getId(), _saver);
      }
      final BCalqueSaverInterface[] childs = (BCalqueSaverInterface[]) _saver.getUI().get(
          BCalquePersistenceGroupe.getChildUiName());
      if (childs != null) {
        for (int i = childs.length - 1; i >= 0; i--) {
          putDataInMap(childs[i]);

        }
      }

    }

  }

}
