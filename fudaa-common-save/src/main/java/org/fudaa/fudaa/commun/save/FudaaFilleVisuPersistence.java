/*
 * @creation 6 sept. 06
 *
 * @modification $Date: 2008-04-01 07:20:52 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.save;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDesktop;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.geometrie.GrBoite;

import java.io.IOException;

/**
 * Une classe pour la persistence d'un panneau de visualisation 2D, qui contient des calques 2D. Les informations de la
 * fenetre sont sauv�es sur fichier projet .fzip.
 *
 * @author fred deniger
 * @version $Id: FudaaFilleVisuPersistence.java,v 1.9.4.2 2008-04-01 07:20:52 bmarchan Exp $
 * @see ZEbliFilleCalques
 */
public class FudaaFilleVisuPersistence implements FudaaSavable {
  ZEbliCalquesPanel visu_;
  String rep_ = "data";

  /**
   * constructeur reserv� au ZEbliFilleCalques
   *
   * @param _visu
   */
  public FudaaFilleVisuPersistence(final ZEbliFilleCalques _visu) {
    super();
    FudaaSaveLib.configureDb4o();
    visu_ = _visu.getVisuPanel();
  }

  /**
   * constructeur reserv� au ZEbliCalquesPanel
   *
   * @param _calque
   */
  public FudaaFilleVisuPersistence(final ZEbliCalquesPanel _calque) {
    super();
    FudaaSaveLib.configureDb4o();
    visu_ = _calque;
  }

  @Override
  public void saveIn(final CtuluArkSaver _writer, final ProgressionInterface _prog) {
    try {
      final BGroupeCalque donneesCalque = visu_.getDonneesCalque();
      _writer.createDir(rep_, donneesCalque.getTousCalques().length + 1);
      final BCalquePersistenceGroupe persistenceMng = donneesCalque.getGroupePersistenceMng();
      persistenceMng.setTop(true);
      commitData(_writer.getDb(), persistenceMng.saveIn(donneesCalque, _writer, rep_ + '/', rep_), _prog);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
    }
  }

  @Override
  public void saveIn(final ObjectContainer _db, final ProgressionInterface _prog) {
    commitData(_db, visu_.getDonneesCalque().getPersistenceMng().save(visu_.getDonneesCalque(), _prog), _prog);
  }

  private void commitData(final ObjectContainer _db, final BCalqueSaverInterface _data, final ProgressionInterface _prog) {
    final FilleVisuSaver saveData = setData(_db);
    saveData.props_ = _data;
    saveData.cqInfosProps_ = visu_.getCqInfos().getGroupePersistenceMng().save(visu_.getCqInfos(), _prog);
    try {
      _db.set(saveData);
      _db.commit();
    } catch (Exception exception) {
      FuLog.error(exception);
    }
  }

  private FilleVisuSaver setData(final ObjectContainer _db) {
    final Query q = _db.query();
    q.constrain(FilleVisuSaver.class);
    q.descend("name_").constrain(visu_.getName());
    final ObjectSet set = q.execute();
    FilleVisuSaver saveData;
    if (set.size() == 1) {
      saveData = (FilleVisuSaver) set.next();
    } else {
      saveData = new FilleVisuSaver();
    }
    _db.commit();
    saveData.name_ = visu_.getName();
    saveData.ifDim_ = visu_.getSize();
    saveData.ifLocation_ = visu_.getLocation();
    final GrBoite b = visu_.getVueCalque().getViewBoite();
    saveData.ptMaxX_ = b.e_.x_;
    saveData.ptMaxY_ = b.e_.y_;
    saveData.ptMinX_ = b.o_.x_;
    saveData.ptMinY_ = b.o_.y_;
    saveData.userInsets_ = visu_.getVueCalque().getUserInsets();

    // saveData.legendUI_ = visu_.getCqLegend().saveUIProperties();
    final BCalque selectedLayers = visu_.getCalqueActif();
    if (selectedLayers != null) {
      saveData.selectedLayer_ = selectedLayers.getName();
    }
    return saveData;
  }

  /**
   * Restaure la fenetre de visu 2D : Les calques et les propri�t�s de la fenetre.
   *
   * @param _impl L'implementation
   * @param _loader Le zip loader
   * @param _prog La progression de tache
   * @return Le thread de chargement.
   */
  public Runnable restore(final BuCommonImplementation _impl, final FudaaSaveZipLoader _loader,
                          final ProgressionInterface _prog) {
    // pas joli joli mais c'est pour �viter certaines erreurs de db4o
    try {
      final FilleVisuSaver saveData = findDataInDb(_loader.getDb());
      final BGroupeCalque parent = visu_.getDonneesCalque();
      final FudaaSaveZipLoaderAdapter.FilleCalque adapter = new FudaaSaveZipLoaderAdapter.FilleCalque(_loader,
          saveData == null ? null : saveData.props_);
      final BCalquePersistenceGroupe persistenceMng = parent.getGroupePersistenceMng();
      // Restauration des calques de donn�es.
      persistenceMng.setTop(true);
      persistenceMng.restoreFrom(adapter, visu_, parent, rep_ + '/', null, _prog);
      // Restauration des propri�t�s de la fenetre.
      return restoreFille(saveData, _impl, parent);
    } catch (final RuntimeException _evt) {
      FuLog.error(_evt);
    }
    return null;
  }

  /**
   * Restaure pour anciens formats ??
   */
  public Runnable restore(final BuCommonImplementation _impl, final ObjectContainer _db,
                          final ProgressionInterface _prog) {
    // pas joli joli mais c'est pour �viter certaines erreurs de db4o
    try {
      final FilleVisuSaver saveData = findDataInDb(_db);
      if (saveData == null) {
        return null;
      }
      final BCalque parent = visu_.getDonneesCalque();
      if (saveData.props_ != null) {
        final BCalquePersistenceGroupe gr = new BCalquePersistenceGroupe();
        gr.setTop(true);
        gr.restore(saveData.props_, visu_, parent, _prog);
      }
      return restoreFille(saveData, _impl, parent);
    } catch (final RuntimeException _evt) {
      FuLog.error(_evt);
    }
    return null;
  }

  /**
   * Restaure les propri�t�s de la fenetre (hors calques de donn�es).
   *
   * @param _saveData Les propri�t�s de la fenetre de visu.
   * @param _impl L'implementation
   * @param _parent Le calque racine des calques de donn�es.
   */
  private Runnable restoreFille(final FilleVisuSaver _saveData, final BuCommonImplementation _impl,
                                final BCalque _parent) {
    if (_saveData == null) {
      return null;
    }
    final BuDesktop ds = _impl.getMainPanel().getDesktop();
    if (ds != null) {
      if (Fu.DEBUG) {
        FuLog.debug("FTR: check visu data");
      }

      return new Runnable() {
        @Override
        public void run() {
          if (_saveData.selectedLayer_ != null) {
            final BCalque cqi = _parent.getCalqueParNom(_saveData.selectedLayer_);
            if (cqi != null) {
              visu_.getArbreCalqueModel().setSelectionCalque(cqi);
            }
          }
          if (_saveData.ifDim_ != null) {
            visu_.setSize(_saveData.ifDim_);
          }
          if (_saveData.userInsets_ != null) {
            visu_.getVueCalque().setUserInsets(_saveData.userInsets_);
          }
          if (_saveData.ifLocation_ != null) {
            visu_.setLocation(_saveData.ifLocation_);
          }
          if (_saveData.ptMaxX_ > _saveData.ptMinX_ && _saveData.ptMaxY_ > _saveData.ptMinY_) {
            final GrBoite b = new GrBoite();
            b.ajuste(_saveData.ptMaxX_, _saveData.ptMaxY_, 0);
            b.ajuste(_saveData.ptMinX_, _saveData.ptMinY_, 0);
            visu_.getVueCalque().setViewBoite(b);
          }

          if (_saveData.legendUI_ != null && visu_.getCqLegend() != null) {
            visu_.getCqLegend().initFrom(_saveData.legendUI_);
          }

          if (_saveData.cqInfosProps_ != null && visu_.getCqInfos() != null) {
            visu_.getCqInfos().getGroupePersistenceMng().restore(_saveData.cqInfosProps_, visu_,
                (BCalque) visu_.getCqInfos().getParent(), null);
          }
        }
      };
    }
    return null;
  }

  private FilleVisuSaver findDataInDb(final ObjectContainer _db) {
    if (_db == null) {
      return null;
    }
    final Query q = _db.query();
    if (q == null || visu_ == null) {
      return null;
    }
    q.constrain(FilleVisuSaver.class);
    q.descend("name_").constrain(visu_.getName());
    final ObjectSet set = q.execute();
    if (set.size() > 1) {
      FuLog.warning("FCO: bad objects saved in db");
      return null;
    }
    final FilleVisuSaver saveData = (FilleVisuSaver) set.next();
    if (saveData == null) {
      if (Fu.DEBUG) {
        FuLog.debug("FTR: load visu fille NO DATA");
      }
      return null;
    }
    // on active tous les niveaux
    _db.activate(saveData, Integer.MAX_VALUE);
    return saveData;
  }

  public String getRep() {
    return rep_;
  }

  public void setRep(final String _rep) {
    rep_ = _rep;
  }
}
