/*
 * @creation 4 sept. 06
 * @modification $Date: 2007-01-08 15:38:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.save;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibFile;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import java.io.BufferedOutputStream;

/**
 * @author fred deniger
 * @version $Id: FudaaSaveZipLoader.java,v 1.4 2007-01-08 15:38:19 deniger Exp $
 */
public class FudaaSaveZipLoader {

  final ZipFile file_;
  final File destFile_;
  File dbFile_;
  ObjectContainer cont_;

  public FudaaSaveZipLoader(final File _f) throws IOException {
    destFile_ = _f;
    file_ = new ZipFile(_f);
  }

  boolean isBdExtracted_;

  public File getDestDir() {
    return destFile_.getParentFile();
  }

  Map m_;

  public void setOption(final String _key, final String _value) {
    if (m_ == null) {
      m_ = new HashMap();
    }
    m_.put(_key, _value);
  }

  public void safeClose() {
    try {
      close();
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
  }

  public String getOption(final String _key) {
    return m_ == null ? null : (String) m_.get(_key);
  }

  protected synchronized void createCont() {
    if (!isBdExtracted_) {
      isBdExtracted_ = true;
      final ZipEntry db = file_.getEntry(FudaaSaveZipWriter.getDbEntryName());
      if (db != null) {
        try {
          dbFile_ = File.createTempFile("fudaaTmp", ".db");
          if (dbFile_ != null) {
            CtuluLibFile.copyStream(file_.getInputStream(db), new BufferedOutputStream(new FileOutputStream(dbFile_)), false, true);
            cont_ = Db4o.openFile(dbFile_.getAbsolutePath());

          }
        } catch (final FileNotFoundException _evt) {
          FuLog.error(_evt);

        } catch (final IOException _evt) {
          FuLog.error(_evt);

        }
      }

    }
  }

  public ObjectContainer getDb() {
    createCont();
    return cont_;
  }

  public String[] getEntries(final String _parentDir) {
    if (_parentDir.endsWith("/")) {
      final List child = new ArrayList();
      final int startIdx = _parentDir.length();
      final Enumeration collection = file_.entries();
      while (collection.hasMoreElements()) {
        final ZipEntry entry = (ZipEntry) collection.nextElement();
        if (entry.getName().startsWith(_parentDir)) {
          final String end = entry.getName().substring(startIdx);
          if (end.indexOf('/') < 0) {
            child.add(end);
          }
        }

      }
      return (String[]) child.toArray(new String[child.size()]);
    }
    return FuEmptyArrays.STRING0;
  }
  
   /**
   * Retourne les sous r�pertoires d'un r�pertoire.
   * @param _parentDir Le r�pertoire parent sous la forme <rep0>/...<repN>/
   * @return La liste des sous r�pertoires sous la forme <rep>/
   */
  public String[] getSubDirEntries(final String _parentDir) {
    if (_parentDir.endsWith("/")) {
      final Set child = new HashSet();
      final int startIdx = _parentDir.length();
      final Enumeration collection = file_.entries();
      while (collection.hasMoreElements()) {
        final ZipEntry entry = (ZipEntry) collection.nextElement();
        if (entry.getName().startsWith(_parentDir)) {
          final String end = entry.getName().substring(startIdx);
          int endIdx=entry.getName().indexOf('/',startIdx);
          if (endIdx > 0) {
            child.add(entry.getName().substring(startIdx, endIdx+1));
          }
        }

      }
      return (String[]) child.toArray(new String[child.size()]);
    }
    return FuEmptyArrays.STRING0;
  }
  
  /**
   * Retourne une propri�t� associ�e � un calque (issue de {calque}.props).
   * @param _parentDir Le repertoire parent
   * @param _entryName L'entr�e correspondant au calque, sous la forme 01-layer.desc.xml.
   * @param _prop Le nom de la propri�t�.
   * @return La valeur de la propri�t�.
   */
  public String getLayerProperty(String _parentDir, String _entryName, String _prop) {
    if (_entryName.endsWith(CtuluArkSaver.DESC_EXT)) 
      _entryName=_entryName.substring(0,_entryName.lastIndexOf(CtuluArkSaver.DESC_EXT));
    
    final Enumeration collection = file_.entries();
    while (collection.hasMoreElements()) {
      final ZipEntry entry = (ZipEntry) collection.nextElement();
      if (entry.getName().equals(_parentDir+_entryName+".props")) {
        String cqName = "_bad_";
        final InputStream str = getInputStream(_parentDir+_entryName+".props");
        if (str != null) {
          final Properties props = new Properties();
          try {
            props.load(str);
            return props.getProperty(_prop);
          } catch (final IOException _evt) {
            FuLog.error(_evt);
          }
        }
      }
    }
    return null;
  }
  

  public InputStream getInputStream(final String _entry) {
    try {
      final ZipEntry entry = file_.getEntry(_entry);
      if (entry == null) { return null; }
      return file_.getInputStream(entry);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      return null;
    }
  }

  public InputStream getReader(final String _parentDir, final String _entryName) {
    try {
      final ZipEntry entry = file_.getEntry(_parentDir + _entryName);
      if (entry == null) { return null; }
      return file_.getInputStream(entry);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    return null;
  }

  public boolean isEntryFound(final String _parentDir, final String _entryName) {
    return file_.getEntry(_parentDir + _entryName) != null;
  }

  public void close() throws IOException {
    if (cont_ != null) {
      cont_.close();
    }
    if (dbFile_ != null) {
      dbFile_.delete();
    }
    if (file_ != null) {
      file_.close();
    }
  }

}
