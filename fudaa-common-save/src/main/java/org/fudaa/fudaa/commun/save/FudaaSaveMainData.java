/*
 *  @creation     25 ao�t 2005
 *  @modification $Date: 2006-09-12 08:42:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.save;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;

import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * LAISSER POUR COMPATIBILITE ANCIENNES VERSIONS.
 * 
 * @author Fred Deniger
 * @version $Id: FudaaSaveMainData.java,v 1.3 2006-09-12 08:42:31 deniger Exp $
 */
public class FudaaSaveMainData {

  public FudaaSaveMainData() {

  }

  public FileFormatSoftware system_;
  public BuInformationsDocument infoDoc_;
  public BuInformationsSoftware preproVersion_;

}
