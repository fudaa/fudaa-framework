/*
 * @creation 11 sept. 06
 * @modification $Date: 2007-05-04 13:58:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.save;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;

import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaCommentModel.java,v 1.4 2007-05-04 13:58:08 deniger Exp $
 */
public class FudaaCommentModel {

  public static class CommentItem {
    private String data_;
    private String id_;

    public CommentItem() {}

    public CommentItem(final String _id, final String _data) {
      super();
      data_ = _data;
      id_ = _id;
    }

    public String getData() {
      return data_;
    }

    public void setData(final String _data) {
      if (_data == null) {
        data_ = CtuluLibString.EMPTY_STRING;
      } else {
        data_ = _data.trim();
      }
    }

    public String getId() {
      return id_;
    }

    public void setId(final String _id) {
      if (_id == null) {
        id_ = CtuluLibString.EMPTY_STRING;
      } else {
        id_ = _id.trim();
      }
    }

    public boolean isSame(final CommentItem _s) {
      if (_s == null) {
        return false;
      }
      return CtuluLibString.isEquals(_s.getId(), getId()) && CtuluLibString.isEquals(_s.getData(), getData());

    }

  }

  List version_ = new ArrayList();
  DateFormat fmt_;

  public int getNbComment() {
    return version_.size();
  }

  public CommentItem create() {
    final CommentItem res = new CommentItem();
    version_.add(res);
    if (fmt_ == null) {
      fmt_ = FudaaLib.getDefaultDateFormat();
    }
    res.setId(fmt_.format(new Date()));
    return res;
  }

  public CommentItem create(final String _id, final String _comm) {
    final CommentItem res = new CommentItem(_id, _comm);
    version_.add(res);
    return res;
  }

  public CommentItem getItem(final int _i) {
    return (CommentItem) version_.get(_i);
  }

  public void writeIn(final CtuluXmlWriter _out) throws IOException {
    _out.write("<versions>");
    _out.write(CtuluLibString.LINE_SEP);
    final int nb = getNbComment();
    for (int i = 0; i < nb; i++) {
      _out.write("<version>");
      final CommentItem it = getItem(i);
      _out.write("<id>");
      _out.write(it.getId());
      _out.write("</id>");
      _out.write(CtuluLibString.LINE_SEP);
      _out.write(CtuluLibString.ESPACE);
      _out.write(CtuluLibString.ESPACE);
      _out.write("<desc>");
      _out.write(CtuluLibString.LINE_SEP);
      _out.write(CtuluLibString.ESPACE);
      _out.write(CtuluLibString.ESPACE);
      _out.write(it.getData());
      _out.write("</desc>");
      _out.write(CtuluLibString.LINE_SEP);
      _out.write("</version>");
      _out.write(CtuluLibString.LINE_SEP);
    }
    _out.write("</versions>");
    _out.write(CtuluLibString.LINE_SEP);
  }

  public boolean isSame(final FudaaCommentModel _m) {
    if (_m == null) {
      return false;
    }
    if (_m.getNbComment() != getNbComment()) {
      return false;
    }
    final int nb = getNbComment();
    for (int i = 0; i < nb; i++) {
      if (!getItem(i).isSame(_m.getItem(i))) {
        return false;
      }
    }
    return true;

  }

  public String toLongString() {
    final StringBuffer buf = new StringBuffer(30);
    final int nb = getNbComment();
    for (int i = 0; i < nb; i++) {
      buf.append(CtuluLibString.LINE_SEP).append("<id>").append(getItem(i).getId()).append("</id><desc>")
          .append(getItem(i).getData()).append("</desc>");
    }
    return buf.toString();
  }

  public static FudaaCommentModel load(final CtuluXmlReaderHelper _reader) {
    final NodeList vs = _reader.getDoc().getElementsByTagName("version");
    final FudaaCommentModel res = new FudaaCommentModel();
    if (vs != null && vs.getLength() > 0) {
      final int nb = vs.getLength();
      for (int i = 0; i < nb; i++) {
        final Node n = vs.item(i);
        final NodeList child = n.getChildNodes();
        if (child != null && child.getLength() > 0) {
          String id = null;
          String comment = null;
          final int nbChild = child.getLength();
          for (int j = 0; j < nbChild && (id == null || comment == null); j++) {
            final Node nj = child.item(j);
            if (nj != null) {
              if ("id".equals(nj.getNodeName())) {
                id = nj.getFirstChild().getNodeValue();
              } else if ("desc".equals(nj.getNodeName())) {
                comment = nj.getFirstChild().getNodeValue();
              }
            }
          }
          if (id != null && comment != null) {
            res.create(id.trim(), comment.trim());
          }
        }
      }

    }
    return res;
  }
}
