/*
 * @file         TestVecteurs.java
 * @creation     
 * @modification $Date: 2006-10-19 14:15:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
/**
 * @version      $Revision: 1.4 $ $Date: 2006-10-19 14:15:26 $ by $Author: deniger $
 * @author        
 */
public final class VecteursExample {
  private VecteursExample() {}
  public static void main(final String[] _args) {
    final FrameVolumeExample frame= new FrameVolumeExample();
    final BChampVecteurs chp= new BChampVecteurs("Champ");
    final Point3d[] points= new Point3d[20];
    for (int i= 0; i < 20; i++) {
      points[i]= new Point3d();
    }
    points[3]= new Point3d(25, 12, 77);
    points[10]= new Point3d(-25, 12, -77);
    final Vector3d[] vecteurs= new Vector3d[20];
    vecteurs[0]= new Vector3d(100, 0, 0);
    vecteurs[1]= new Vector3d(0, 100, 0);
    vecteurs[2]= new Vector3d(0, 0, 100);
    vecteurs[3]= new Vector3d(100, 200, 300);
    vecteurs[4]= new Vector3d(100, 100, 0);
    vecteurs[5]= new Vector3d(0, 100, 100);
    vecteurs[6]= new Vector3d(100, 0, 100);
    vecteurs[7]= new Vector3d(-100, 0, 0);
    vecteurs[8]= new Vector3d(0, -100, 0);
    vecteurs[9]= new Vector3d(0, 0, -100);
    vecteurs[10]= new Vector3d(-100, -200, -300);
    vecteurs[11]= new Vector3d(-100, -100, 0);
    vecteurs[12]= new Vector3d(0, -100, -100);
    vecteurs[13]= new Vector3d(-100, 0, -100);
    vecteurs[14]= new Vector3d(-100, 100, 0);
    vecteurs[15]= new Vector3d(0, 100, -100);
    vecteurs[16]= new Vector3d(100, 0, -100);
    vecteurs[17]= new Vector3d(100, -100, 0);
    vecteurs[18]= new Vector3d(0, -100, 100);
    vecteurs[19]= new Vector3d(-100, 0, 100);
    chp.setGeometrie(points, vecteurs);
    frame.getUnivers().addObjet(chp);
    chp.setVisible(true);
    chp.setRapide(false);
    frame.getUnivers().init();
    frame.setVisible(true);
  }
}
