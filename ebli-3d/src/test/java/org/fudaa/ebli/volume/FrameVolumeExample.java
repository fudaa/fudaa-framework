/*
 * @file         TestFrameVolume.java
 * @creation
 * @modification $Date: 2006-09-19 15:06:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Method;
import java.util.List;

import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.DirectionalLight;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

import com.memoire.fu.FuLog;

import org.fudaa.ebli.palette.BPaletteCouleurSimple;
import org.fudaa.ebli.palette.BPaletteCouleurSimpleCustomEditor;
import org.fudaa.ebli.volume.BCanvas3D;
import org.fudaa.ebli.volume.BUnivers;
import org.fudaa.ebli.volume.BVolume;
import org.fudaa.ebli.volume.controles.BArbreVolume;
import org.fudaa.ebli.volume.controles.BControleVolume;
import org.fudaa.ebli.volume.controles.BPosition;
import org.fudaa.ebli.volume.controles.BSelecteurTexture;
import org.fudaa.ebli.volume.controles.BUniversInteraction;

/**
 * @version $Revision: 1.5 $ $Date: 2006-09-19 15:06:55 $ by $Author: deniger $
 * @author
 */
public class FrameVolumeExample extends JFrame {
  BUnivers u_;
  JLabel labelSlider_;
  private JSlider slider_;
  private SliderListener sl_;
  BPaletteCouleurSimple palette_;
  private PaletteListener pl_;
  private BArbreVolume av_;

  // private JInternalFrame iframe;
  public FrameVolumeExample() {
    u_ = new BUnivers();
    // iframe=new JInternalFrame("3D",true,true,true,true);
    av_ = new BArbreVolume();
    av_.setVolume(u_.getRoot());
    final DirectionalLight l = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(1, 0, 1));
    l.setInfluencingBounds(new BoundingSphere(new Point3d(), 100000));
    // AmbientLight l2=new AmbientLight(new Color3f((float)1,(float)1,(float)1));
    // l2.setInfluencingBounds(new BoundingSphere(new Point3d(),100000));
    final DirectionalLight l3 = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(-1, 0, 1));
    l3.setInfluencingBounds(new BoundingSphere(new Point3d(), 100000));
    /*
     * SpotLight l4=new SpotLight(); l4.setInfluencingBounds(new BoundingSphere(new Point3d(),100000));
     * l4.setPosition(0,100000,0); l4.setDirection(0,-1,0);
     */
    final BranchGroup lightbg = new BranchGroup();
    lightbg.addChild(l);
    // lightbg.addChild(l2);
    lightbg.addChild(l3);
    // lightbg.addChild(l4);
    u_.addBranchGraph(lightbg);
    // Panel Sud : controle deplacement
    final JPanel jp = new JPanel();
    jp.setLayout(new FlowLayout());
    final BUniversInteraction ui = new BUniversInteraction(u_);
    final JButton init = new JButton("Init");
    init.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        u_.init();

      }

    });
    final BControleVolume cv = new BControleVolume(u_);
    cv.addRepereEventListener(ui);
    cv.addPropertyChangeListener(ui);
    final BPosition pos = new BPosition();
    u_.addPropertyChangeListener("position", pos);
    /*
     * JButton freeze=new JButton("Freeze"); ButtonListener bl=new ButtonListener(u,freeze);
     * freeze.addActionListener(bl);
     */
    final JCheckBox orbital = new JCheckBox("Orbital");
    orbital.addItemListener(ui);
    orbital.setSelected(false);
    final JPanel boutons = new JPanel();
    boutons.setLayout(new GridLayout(2, 1));
    boutons.add(init);
    boutons.add(orbital);
    jp.add(pos);
    jp.add(cv);
    // jp.add(coef);
    jp.add(boutons);
    // jp.add(freeze);
    // Panel Nord : selecteur de mode de rendu (point/filaire/polygone)
    final JPanel jp2 = new JPanel();
    jp2.setLayout(new FlowLayout());
    final ButtonGroup bg = new ButtonGroup();
    final JRadioButton point = new JRadioButton("Point");
    point.setActionCommand("Point");
    final JRadioButton filaire = new JRadioButton("Filaire");
    filaire.setActionCommand("Filaire");
    final JRadioButton polygone = new JRadioButton("Polygone");
    polygone.setActionCommand("Polygone");
    polygone.setSelected(true);
    bg.add(point);
    bg.add(filaire);
    bg.add(polygone);
    final RadioListener radio = new RadioListener();
    point.addActionListener(radio);
    filaire.addActionListener(radio);
    polygone.addActionListener(radio);
    jp2.add(point);
    jp2.add(filaire);
    jp2.add(polygone);
    final BSelecteurTexture st = new BSelecteurTexture();
    final JButton tex = new JButton("Texture");
    tex.addActionListener(new SelecteurListener(st));
    jp2.add(tex);
    // Panel gauche : selecteur d'echelle
    final JPanel jp3 = new JPanel();
    jp3.setLayout(new BorderLayout());
    slider_ = new JSlider(SwingConstants.VERTICAL);
    slider_.setMinimum(1);
    slider_.setMaximum(1000);
    slider_.setValue(500);
    sl_ = new SliderListener(this);
    // sl=new SliderListener();
    slider_.addChangeListener(sl_);
    final JLabel titreEchelle = new JLabel("Echelle");
    labelSlider_ = new JLabel("1.0");
    jp3.add("Center", slider_);
    jp3.add("East", labelSlider_);
    jp3.add("North", titreEchelle);
    // Panel droit : selecteur de couleur
    final JPanel jp4 = new JPanel();
    jp4.setLayout(new FlowLayout());
    palette_ = new BPaletteCouleurSimple();
    palette_.setOrientation(BPaletteCouleurSimple.VERTICAL);
    final BPaletteCouleurSimpleCustomEditor psce = new BPaletteCouleurSimpleCustomEditor();
    psce.setObject(palette_);
    pl_ = new PaletteListener(this);
    // coul=new BSelecteurCouleur();
    // jp4.add(coul);
    // palette.addPropertyChangeListener(pl);
    jp4.add(palette_);
    jp4.add(psce);
    palette_.addPropertyChangeListener(av_);
    st.addPropertyChangeListener(av_);
    sl_.addPropertyChangeListener(av_);
    jp4.add(av_);
    // fenetre globale
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(final WindowEvent _evt) {
        System.exit(0);
      }
    });
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setSize(new Dimension(512, 512));
    setSize(new Dimension(500, 500));
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add("Center", u_.getCanvas3D());
    getContentPane().add("North", jp2);
    getContentPane().add("South", jp);
    getContentPane().add("West", jp3);
    getContentPane().add("East", jp4);
    u_.getCanvas3D().freeze(false);
  }

  @Override
  public void setVisible(final boolean _visible) {
    super.setVisible(_visible);
    pl_.propertyChange(new PropertyChangeEvent(this, "", new Integer(0), new Integer(0)));
  }

  public void addEchelleListener(final BVolume _v) {}

  public void addPaletteListener(final BVolume _v, final double[] _z) {
    // pl.addPropertyChangeListener(v);
    pl_.setZ(_z);
  }

  public BUnivers getUnivers() {
    return u_;
  }
  class ButtonListener implements ActionListener {
    boolean frozen_;
    final BCanvas3D universe_;
    final JButton f_;

    public ButtonListener(final BCanvas3D _u, final JButton _b) {
      universe_ = _u;
      f_ = _b;
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      universe_.freeze(!frozen_);
      frozen_ = !frozen_;
      if (frozen_) {
        f_.setText("unFreeze");
      } else {
        f_.setText("Freeze");
      }
    }
  }
  class SelecteurListener implements ActionListener {
    boolean ouvert_;
    BSelecteurTexture st_;

    public SelecteurListener(final BSelecteurTexture _st) {
      st_ = _st;
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      if (ouvert_) {
        st_.setVisible(false);
      } else {
        st_.setVisible(true);
      }
      ouvert_ = !ouvert_;
    }
  }
  class RadioListener implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent _evt) {
      try {
        final List v = u_.getTousEnfants();
        for (int i = 0; i < v.size(); i++) {
          final BVolume volume = (BVolume) v.get(i);
          final Method m = volume.getClass().getMethod("set" + _evt.getActionCommand(), new Class[0]);
          m.invoke(volume, new Object[0]);
        }
      } catch (final Exception ex) {
        FuLog.error(ex);
      }
    }
  }
  class SliderListener extends PropertyChangeSupport implements ChangeListener {
    int oldValue_ = 500;

    public SliderListener(final Object _source) {
      super(_source);
    }

    @Override
    public void stateChanged(final ChangeEvent _e) {
      final JSlider source = (JSlider) _e.getSource();
      final int value = source.getValue();
      if (value != oldValue_) {
        // System.out.println("Firing PropertyChangeEvent, new_value="+(double)value/10);
        this.firePropertyChange("echelle", new Double((double) oldValue_ / 10), new Double((double) value / 10));
        labelSlider_.setText("" + (double) value / 10);
        oldValue_ = value;
      }
    }
  }
  class PaletteListener extends PropertyChangeSupport implements PropertyChangeListener {
    double[] z_;
    Color[] c_;
    Color[] cold_;

    public PaletteListener(final Object _source) {
      super(_source);
    }

    public void setZ(final double[] _z) {
      z_ = _z;
    }

    @Override
    public void propertyChange(final PropertyChangeEvent _e) {
      if (z_ != null) {
        if (c_ != null) {
          cold_ = c_;
        } else {
          cold_ = new Color[z_.length];
        }
        try {
          c_ = new Color[z_.length];
        } catch (final Exception ex) {
          System.err.println("new Color[] " + ex);
        }
        try {
          for (int i = 0; i < z_.length; i++) {
            try {
              c_[i] = palette_.couleur(z_[i]);
              // System.out.println("Couleur "+i+" :"+c_[i]);
            } catch (final Exception ex) {
              System.err.println("palette.couleur " + ex);
            }
          }
        } catch (final Exception ex) {
          System.err.println("for " + ex);
        }
        // System.out.println("Firing PCEvent ");
        try {
          firePropertyChange(new PropertyChangeEvent(this, "couleurs", cold_, c_));
        } catch (final Exception ex) {
          System.err.println("fire " + ex);
        }
      }
    }
    // seti :http://setiathome.ssl.berckley.edu
  }
}
