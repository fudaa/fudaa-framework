/*
 * @creation 23 mars 07
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.media.j3d.Canvas3D;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Raster;
import javax.vecmath.Point3f;

/**
 * @author fred deniger
 * @version $Id: BCanvas3DOffScreen.java,v 1.2 2007-05-04 13:49:46 deniger Exp $
 */
public class BCanvas3DOffScreen extends Canvas3D {

  boolean printing_;

  BCanvas3DOffScreen(final BCanvas3D _init) {
    super(_init.getGraphicsConfiguration(), true);
    initDimensions(_init);
    _init.getView().addCanvas3D(this);
  }

  protected final void initDimensions(final BCanvas3D _init) {
    setSize(_init.getSize());
    getScreen3D().setSize(_init.getScreen3D().getSize());
    getScreen3D().setPhysicalScreenHeight(_init.getScreen3D().getPhysicalScreenHeight());
    getScreen3D().setPhysicalScreenWidth(_init.getScreen3D().getPhysicalScreenWidth());

  }

  public Dimension getDefaultImageDimension() {
    return new Dimension(getWidth(), getHeight());
  }

  public BufferedImage produceImage(final BCanvas3D _init) {
    initDimensions(_init);
    printing_ = true;

    BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
    final ImageComponent2D buffer = new ImageComponent2D(ImageComponent.FORMAT_RGB, img);
    final Raster printRaster = new Raster(new Point3f(0.0f, 0.0f, 0.0f), Raster.RASTER_COLOR, 0, 0, img.getWidth(), img
        .getHeight(), buffer, null);
    img = printRaster.getImage().getImage();
    buffer.setCapability(ImageComponent.ALLOW_IMAGE_READ);
    this.setOffScreenBuffer(buffer);
    renderOffScreenBuffer();
    waitForOffScreenRendering();
    // getGraphicsContext3D().readRaster(printRaster);
    img.flush();
    return getOffScreenBuffer().getImage();
  }

}
