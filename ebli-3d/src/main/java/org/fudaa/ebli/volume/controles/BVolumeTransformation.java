/*
 * @creation     1999-12-30
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @version $Revision: 1.11 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BVolumeTransformation extends JPanel {
  final JCheckBox lock_;

  public BVolumeTransformation() {
    // setPreferredSize(new Dimension(512,64));
    setLayout(new GridLayout(4, 5));
    JLabel[] labels = new JLabel[3];
    labels[0] = new JLabel("Tra", SwingConstants.CENTER);
    labels[1] = new JLabel("Rot", SwingConstants.CENTER);
    labels[2] = new JLabel("Ech", SwingConstants.CENTER);
    add(new JLabel(" ", SwingConstants.CENTER));
    add(new JLabel("X", SwingConstants.CENTER));
    add(new JLabel("Y", SwingConstants.CENTER));
    add(new JLabel("Z", SwingConstants.CENTER));
    add(new JLabel(" ", SwingConstants.CENTER));
    final JTextField[] texte = new JTextField[9];
    final MyActionListener al = new MyActionListener();
    for (int i = 0; i < 9; i++) {
      texte[i] = new JTextField(5);
      texte[i].addActionListener(al);
    }
    texte[0].setName("translationX");
    texte[1].setName("translationY");
    texte[2].setName("translationZ");
    texte[3].setName("rotationX");
    texte[4].setName("rotationY");
    texte[5].setName("rotationZ");
    texte[6].setName("echelleX");
    texte[7].setName("echelleY");
    texte[8].setName("echelleZ");
    for (int i = 0; i < 2; i++) {
      add(labels[i]);
      add(texte[3 * i]);
      add(texte[3 * i + 1]);
      add(texte[3 * i + 2]);
      add(new JLabel(" ", SwingConstants.CENTER));
    }
    add(labels[2]);
    add(texte[6]);
    add(texte[7]);
    add(texte[8]);
    lock_ = new JCheckBox("uniforme");
    lock_.setSelected(true);
    // lock.addActionListener(new LockListener());
    add(lock_);
  }
  class MyActionListener implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent _e) {
      final JTextField texte = (JTextField) _e.getSource();
      try {
        if ((lock_.isSelected()) && (texte.getName().substring(0, 2).equals("ec"))) {
          firePropertyChange("echelleX", 0f, Float.parseFloat(texte.getText()));
          firePropertyChange("echelleY", 0f, Float.parseFloat(texte.getText()));
          firePropertyChange("echelleZ", 0f, Float.parseFloat(texte.getText()));
        } else {
          firePropertyChange(texte.getName(), 0f, Float.parseFloat(texte.getText()));
          // texte.setText("");
        }
      } catch (final NumberFormatException ex) {};
      texte.setText("");
    }
  }
  /*
   * class LockListener implements ActionListener { public void actionPerformed(final ActionEvent _e) { } }
   */
}
