/*
 * @creation     1999-09-27
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.Color;

import javax.media.j3d.*;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import org.fudaa.ebli.geometrie.GrBoite;

/**
 * @version $Revision: 1.18 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BGrilleReguliere extends BGrille {
  private int npoints_;

  public BGrilleReguliere() {
    super();
    npoints_ = 0;
  }

  public BGrilleReguliere(final String _s) {
    super(_s);
    npoints_ = 0;
  }

  /*
   * public void centre() { System.out.println("Boite: "+boite_.o+" - "+boite_.e); GrMorphisme
   * centrage=GrMorphisme.translation(-(boite_.o.x+boite_.e.x)/2,-(boite_.o.y+boite_.e.y)/2,0); autoApplique(centrage);
   * System.out.println("Boite: "+boite_.o+" - "+boite_.e); centrage=GrMorphisme.rotationX(-Math.PI/2);
   * autoApplique(centrage); System.out.println("Boite: "+boite_.o+" - "+boite_.e); }
   */
  public void setGeometrie(final TriangleStripArray _geometrie) {
    _geometrie.setCapability(Geometry.ALLOW_INTERSECT);
    _geometrie.setCapability(GeometryArray.ALLOW_COLOR_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
    _geometrie.setCapability(GeometryArray.ALLOW_COUNT_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
    shape_.setGeometry(_geometrie);
    calculeBBox();
    final Point3d low = new Point3d();
    final Point3d up = new Point3d();
    bbox_.getLower(low);
    bbox_.getUpper(up);
    // Appearance ap=shape_.getAppearance();
    final PolygonAttributes pa = new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_LINE);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final Appearance ap = new Appearance();
    ap.setPolygonAttributes(pa);
    // ap.getPolygonAttributes().setPolygonMode(PolygonAttributes.POLYGON_LINE);
    boiteEnglobante_ = new com.sun.j3d.utils.geometry.Box((float) (up.x - low.x) / 2, (float) (up.y - low.y) / 2,
        (float) (up.z - low.z) / 2, ap);
    final TransformGroup bTg = new TransformGroup();
    final Transform3D t3d = new Transform3D();
    t3d.set(new Vector3d((up.x - low.x) / 2, (float) (up.y - low.y) / 2, neg_ * (float) (up.z - low.z) / 2));
    bTg.setTransform(t3d);
    bTg.addChild(boiteEnglobante_);
    switch_.addChild(bTg);
    // centre();
  }

  public void setGeometrie(final int _nbpointsx, final int _nbpointsy, final Point3d[] _points,
      final Vector3d[] _normales, final Color[] _couleurs) {
    final int nbstrip = _nbpointsy - 1;
    final int[] stripcount = new int[nbstrip];
    for (int i = 0; i < (nbstrip); i++) {
      stripcount[i] = _nbpointsx * 2;
    }
    final Point3d[] totalpoints = new Point3d[2 * nbstrip * _nbpointsx];
    final Point3f[] texpoints = new Point3f[2 * nbstrip * _nbpointsx];
    final Vector3d[] normales = new Vector3d[2 * nbstrip * _nbpointsx];
    for (int i = 0; i < nbstrip; i++) {
      for (int j = 0; j < _nbpointsx; j++) {
        totalpoints[2 * j + i * (_nbpointsx * 2)] = _points[j + i * _nbpointsx];
        totalpoints[2 * j + 1 + i * (_nbpointsx * 2)] = _points[j + (i + 1) * _nbpointsx];
        normales[2 * j + i * (_nbpointsx * 2)] = _normales[j + i * _nbpointsx];
        normales[2 * j + 1 + i * (_nbpointsx * 2)] = _normales[j + (i + 1) * _nbpointsx];
      }
    }
    for (int i = 0; i < 2 * nbstrip * _nbpointsx; i++) {
      texpoints[i] = new Point3f(
          (float) ((totalpoints[i].x + getBoite().o_.x_) / (getBoite().e_.x_ - getBoite().o_.x_)),
          (float) ((totalpoints[i].y + getBoite().o_.y_) / (getBoite().e_.y_ - getBoite().o_.y_)),
          (float) totalpoints[i].z);
    }
    final Vector3f[] normales2 = new Vector3f[normales.length];
    for (int i = 0; i < normales.length; i++) {
      normales2[i] = new Vector3f(normales[i]);
    }
    npoints_ = _nbpointsx * (_nbpointsy - 1) * 2;
    final Color4f[] couleurs = new Color4f[npoints_];
    for (int i = 0; i < npoints_; i++) {
      couleurs[i] = new Color4f(_couleurs[i]);
    }
    final TriangleStripArray maillage = new TriangleStripArray(npoints_, GeometryArray.COORDINATES
        | GeometryArray.NORMALS | GeometryArray.COLOR_4 | GeometryArray.TEXTURE_COORDINATE_4, stripcount);
    maillage.setCoordinates(0, totalpoints);
    maillage.setTextureCoordinates(0, texpoints);
    maillage.setNormals(0, normales2);
    maillage.setColors(0, couleurs);
    setGeometrie(maillage);
  }

  public void setGeometrie(final int _nbpointx, final int _nbpointy, final double[] _z) {
    final Color[] c = new Color[2 * _nbpointx * (_nbpointy - 1)];
    for (int i = 0; i < c.length; i++) {
      c[i] = Color.white;
    }
    setGeometrie(_nbpointx, _nbpointy, _z, c);
  }

  public void setGeometrie(final int _nbpointx, final int _nbpointy, final double[] _z, final Color[] _couleurs) {
    final double[] zcopy = new double[_z.length];
    /*
     * for (int i = 0; i < zcopy.length; i++) { zcopy[i] = _z[i] / ParametresVolumes.ECHELLE; }
     */
    final Point3d[] points = computePoints3D(_nbpointx, _nbpointy, zcopy);
    bbox_ = new BoundingBox();
    bbox_.combine(points);
    final Vector3d[] normales = new Vector3d[_nbpointx * _nbpointy];
    final Vector3d[] vecteurs = new Vector3d[8];
    for (int i = 0; i < 8; i++) {
      vecteurs[i] = new Vector3d();
    }
    final Point3d[] pointsN = computePtN(_nbpointx, points, normales, vecteurs);
    normales[0].scale(1. / 3.);
    // coin superieur droit
    pointsN[0] = points[_nbpointx - 2];
    pointsN[1] = points[2 * _nbpointx - 2];
    pointsN[2] = points[2 * _nbpointx - 1];
    normales[_nbpointx - 1] = new Vector3d();
    for (int i = 0; i < 3; i++) {
      vecteurs[i].sub(points[_nbpointx - 1], pointsN[i]);
      final Vector3d normal = new Vector3d();
      normal.cross(vecteurs[i], vecteurs[(i + 1) % 5]);
      if (normal.lengthSquared() > 0) {
        normal.normalize();
      }
      normales[_nbpointx - 1].add(normal);
    }
    normales[_nbpointx - 1].scale(1. / 3.);
    // premiere ligne
    firstLine(_nbpointx, points, normales, vecteurs, pointsN);
    lastLines(_nbpointx, _nbpointy, points, normales, vecteurs, pointsN);
    for (int x = 1; x < _nbpointx - 1; x++) {
      pointsN[0] = points[(_nbpointy - 1) * _nbpointx + x + 1];
      pointsN[1] = points[(_nbpointy - 1 - 1) * _nbpointx + x + 1];
      pointsN[2] = points[(_nbpointy - 1 - 1) * _nbpointx + x];
      pointsN[3] = points[(_nbpointy - 1 - 1) * _nbpointx + x - 1];
      pointsN[4] = points[(_nbpointy - 1) * _nbpointx + x - 1];
      normales[(_nbpointy - 1) * _nbpointx + x] = new Vector3d();
      for (int i = 0; i < 5; i++) {
        vecteurs[i].sub(points[(_nbpointy - 1) * _nbpointx + x], pointsN[i]);
        final Vector3d normal = new Vector3d();
        normal.cross(vecteurs[i], vecteurs[(i + 1) % 5]);
        if (normal.lengthSquared() > 0) {
          normal.normalize();
        }
        normales[(_nbpointy - 1) * _nbpointx + x].add(normal);
      }
      normales[(_nbpointy - 1) * _nbpointx + x].scale(1. / 5.);
    }
    pointsN[0] = points[(_nbpointy - 1) * _nbpointx + 1];
    pointsN[1] = points[(_nbpointy - 2) * _nbpointx + 1];
    pointsN[2] = points[(_nbpointy - 2) * _nbpointx];
    normales[(_nbpointy - 1) * _nbpointx] = new Vector3d();
    for (int i = 0; i < 3; i++) {
      vecteurs[i].sub(points[(_nbpointy - 1) * _nbpointx], pointsN[i]);
      final Vector3d normal = new Vector3d();
      normal.cross(vecteurs[i], vecteurs[(i + 1) % 5]);
      if (normal.lengthSquared() > 0) {
        normal.normalize();
      }
      normales[(_nbpointy - 1) * _nbpointx].add(normal);
    }
    normales[(_nbpointy - 1) * _nbpointx].scale(1. / 3.);
    pointsN[0] = points[_nbpointy * _nbpointx - 2];
    pointsN[1] = points[(_nbpointy - 1) * _nbpointx - 2];
    pointsN[2] = points[(_nbpointy - 1) * _nbpointx - 1];
    normales[_nbpointy * _nbpointx - 1] = new Vector3d();
    for (int i = 0; i < 3; i++) {
      vecteurs[i].sub(points[_nbpointy * _nbpointx - 1], pointsN[i]);
      final Vector3d normal = new Vector3d();
      normal.cross(vecteurs[i], vecteurs[(i + 1) % 5]);
      if (normal.lengthSquared() > 0) {
        normal.normalize();
      }
      normales[_nbpointy * _nbpointx - 1].add(normal);
    }
    normales[_nbpointy * _nbpointx - 1].scale(1. / 3.);
    setGeometrie(_nbpointx, _nbpointy, points, normales, _couleurs);
  }

  private void lastLines(final int _nbpointx, final int _nbpointy, final Point3d[] _points, final Vector3d[] _normales,
      final Vector3d[] _vecteurs, final Point3d[] _pointsN) {
    for (int y = 1; y < _nbpointy - 1; y++) {
      // premiere colonne
      _pointsN[0] = _points[y * _nbpointx + 1];
      _pointsN[1] = _points[(y + 1) * _nbpointx + 1];
      _pointsN[2] = _points[(y + 1) * _nbpointx];
      _pointsN[3] = _points[(y - 1) * _nbpointx];
      _pointsN[4] = _points[(y - 1) * _nbpointx + 1];
      _normales[y * _nbpointx] = new Vector3d();
      for (int i = 0; i < 5; i++) {
        _vecteurs[i].sub(_points[y * _nbpointx], _pointsN[i]);
        final Vector3d normal = new Vector3d();
        normal.cross(_vecteurs[i], _vecteurs[(i + 1) % 5]);
        if (normal.lengthSquared() > 0) {
          normal.normalize();
        }
        _normales[y * _nbpointx].add(normal);
      }
      _normales[y * _nbpointx].scale(1. / 5.);
      // milieu
      for (int x = 1; x < _nbpointx - 1; x++) {
        _pointsN[0] = _points[y * _nbpointx + x + 1];
        _pointsN[1] = _points[(y + 1) * _nbpointx + x + 1];
        _pointsN[2] = _points[(y + 1) * _nbpointx + x];
        _pointsN[3] = _points[(y + 1) * _nbpointx + x - 1];
        _pointsN[4] = _points[y * _nbpointx + x - 1];
        _pointsN[5] = _points[(y - 1) * _nbpointx + x - 1];
        _pointsN[6] = _points[(y - 1) * _nbpointx + x];
        _pointsN[7] = _points[(y - 1) * _nbpointx + x + 1];
        _normales[y * _nbpointx + x] = new Vector3d();
        for (int i = 0; i < 8; i++) {
          _vecteurs[i].sub(_points[y * _nbpointx + x], _pointsN[i]);
          _vecteurs[(i + 1) % 8].sub(_points[y * _nbpointx + x], _pointsN[(i + 1) % 8]);
          final Vector3d normal = new Vector3d();
          normal.cross(_vecteurs[i], _vecteurs[(i + 1) % 8]);
          if (!normal.equals(new Vector3d(0, 0, 0))) {
            normal.normalize();
          }
          _normales[y * _nbpointx + x].add(normal);
        }
        _normales[y * _nbpointx + x].scale(1. / 8.);
      }
      // derniere colonne
      _pointsN[0] = _points[(y + 1) * _nbpointx - 1 - 1];
      _pointsN[1] = _points[(y + 1 + 1) * _nbpointx - 1 - 1];
      _pointsN[2] = _points[(y + 1 + 1) * _nbpointx - 1];
      _pointsN[3] = _points[(y + 1 - 1) * _nbpointx - 1];
      _pointsN[4] = _points[(y + 1 - 1) * _nbpointx - 1 - 1];
      _normales[(y + 1) * _nbpointx - 1] = new Vector3d();
      for (int i = 0; i < 5; i++) {
        _vecteurs[i].sub(_points[(y + 1) * _nbpointx - 1], _pointsN[i]);
        final Vector3d normal = new Vector3d();
        normal.cross(_vecteurs[i], _vecteurs[(i + 1) % 5]);
        if (!normal.equals(new Vector3d(0, 0, 0))) {
          normal.normalize();
        }
        _normales[(y + 1) * _nbpointx - 1].add(normal);
      }
      _normales[(y + 1) * _nbpointx - 1].scale(1. / 5.);
    }
  }

  private void firstLine(final int _nbpointx, final Point3d[] _points, final Vector3d[] _normales,
      final Vector3d[] _vecteurs, final Point3d[] _pointsN) {
    for (int x = 1; x < _nbpointx - 1; x++) {
      _pointsN[0] = _points[0 * _nbpointx + x + 1];
      _pointsN[1] = _points[(0 + 1) * _nbpointx + x + 1];
      _pointsN[2] = _points[(0 + 1) * _nbpointx + x];
      _pointsN[3] = _points[(0 + 1) * _nbpointx + x - 1];
      _pointsN[4] = _points[(0) * _nbpointx + x - 1];
      _normales[0 * _nbpointx + x] = new Vector3d();
      for (int i = 0; i < 5; i++) {
        _vecteurs[i].sub(_points[0 * _nbpointx + x], _pointsN[i]);
        final Vector3d normal = new Vector3d();
        normal.cross(_vecteurs[i], _vecteurs[(i + 1) % 5]);
        if (!normal.equals(new Vector3d(0, 0, 0))) {
          normal.normalize();
        }
        _normales[0 * _nbpointx + x].add(normal);
      }
      _normales[0 * _nbpointx + x].scale(1. / 5.);
    }
  }

  private Point3d[] computePtN(final int _nbpointx, final Point3d[] _points, final Vector3d[] _normales,
      final Vector3d[] _vecteurs) {
    final Point3d[] pointsN = new Point3d[8];
    // coin superieur gauche
    pointsN[0] = _points[1];
    pointsN[1] = _points[_nbpointx];
    pointsN[2] = _points[_nbpointx + 1];
    _normales[0] = new Vector3d();
    for (int i = 0; i < 3; i++) {
      _vecteurs[i].sub(_points[0], pointsN[i]);
      final Vector3d normal = new Vector3d();
      normal.cross(_vecteurs[i], _vecteurs[(i + 1) % 5]);
      if (!normal.equals(new Vector3d(0, 0, 0))) {
        normal.normalize();
      }
      _normales[0].add(normal);
    }
    return pointsN;
  }

  private Point3d[] computePoints3D(final int _nbpointx, final int _nbpointy, final double[] _zcopy) {
    final Point3d[] points = new Point3d[_nbpointx * _nbpointy];
    final GrBoite boite = getBoite();
    final double pasx = (boite.e_.x_ - boite.o_.x_) / (_nbpointx - 1)/* / ParametresVolumes.ECHELLE */;
    final double pasy = (boite.e_.y_ - boite.o_.y_) / (_nbpointy - 1) /* / ParametresVolumes.ECHELLE */;
    for (int j = 0; j < _nbpointy; j++) {
      for (int i = 0; i < _nbpointx; i++) {
        // points[i+_nbpointx*j]=new
        // Point3d((double)i*pasx+boite_.o.x,(double)j*pasy+boite_.o.y,-(zcopy[i+_nbpointx*j]+boite_.o.z));
        // points[i+_nbpointx*j]=new
        // Point3d((double)i*pasx+boite_.o.x,(double)j*pasy+boite_.o.y,-(zcopy[i+_nbpointx*j]));
        points[i + _nbpointx * j] = new Point3d(i * pasx, j * pasy, neg_ * (_zcopy[i + _nbpointx * j]));
      }
    }
    return points;
  }
}
