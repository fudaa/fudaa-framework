/*
 * @creation     2000-02-04
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.media.j3d.Transform3D;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.vecmath.Point3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ebli.volume.common.BUniversInterface;

/**
 * @version $Revision: 1.13 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BMagnetoscope extends JComponent {
  /**
   * non private car acceder depuis inner classe.
   */
  JButton debut_, enregistrer_, fin_, charger_, sauver_, prec_, suiv_, remplacer_, marche_;

  /**
   * non private car acceder depuis inner classe.
   */
  JLabel /* tps_, */cleLabel_;

  int cle_;

  // private JCheckBox boucle_;

  /**
   * non private car acceder depuis inner classe.
   */
  BUniversInterface u_;

  List pos_;

  List dir_;

  /**
   * non private car acceder depuis inner classe.
   */
  int nbCles_;

  JLabel nbClesLabel_;

  TrajectoireCamera traj_;

  Component parent_;

  boolean boolmarche_;

  public BMagnetoscope(final BUniversInterface _u) {
    parent_ = this;
    u_ = _u;
    final AL al = new AL();
    charger_ = new JButton("Charger");
    charger_.setName("charger");
    charger_.setEnabled(true);
    charger_.addActionListener(al);
    sauver_ = new JButton("Sauver");
    sauver_.setName("sauver");
    sauver_.setEnabled(false);
    sauver_.addActionListener(al);
    debut_ = new JButton("Debut");
    debut_.setName("debut");
    debut_.addActionListener(al);
    enregistrer_ = new JButton("Enregistrer");
    enregistrer_.setName("enregistrer");
    enregistrer_.setEnabled(false);
    enregistrer_.addActionListener(al);
    fin_ = new JButton("Fin");
    fin_.setName("fin");
    fin_.setEnabled(false);
    fin_.addActionListener(al);
    prec_ = new JButton("Prec.");
    prec_.setName("precedent");
    prec_.setEnabled(false);
    prec_.addActionListener(al);
    suiv_ = new JButton("Suiv.");
    suiv_.setName("suivant");
    suiv_.setEnabled(false);
    suiv_.addActionListener(al);
    cleLabel_ = new JLabel("0");
    cle_ = 0;
    remplacer_ = new JButton("Remplacer");
    remplacer_.setName("remplacer");
    remplacer_.setEnabled(false);
    remplacer_.addActionListener(al);
    marche_ = new JButton("Play");
    marche_.setName("marche");
    marche_.setEnabled(false);
    marche_.addActionListener(al);
    nbClesLabel_ = new JLabel("nb Cl�s: 0");
    nbCles_ = 0;
    final JPanel fichier = new JPanel(new GridLayout(2, 1));
    fichier.add(charger_);
    fichier.add(sauver_);
    final JPanel enr = new JPanel(new GridLayout(2, 2));
    enr.add(debut_);
    enr.add(fin_);
    enr.add(enregistrer_);
    enr.add(nbClesLabel_);
    final JPanel edit = new JPanel(new FlowLayout());
    edit.add(prec_);
    edit.add(cleLabel_);
    edit.add(suiv_);
    edit.add(remplacer_);
    final JPanel controle = new JPanel(new FlowLayout());
    controle.add(marche_);
    final JPanel main = new JPanel(new BuVerticalLayout());
    main.add(fichier);
    main.add(new JSeparator());
    main.add(enr);
    main.add(new JSeparator());
    main.add(edit);
    main.add(new JSeparator());
    main.add(controle);
    setLayout(new BorderLayout());
    add("Center", main);
  }

  public void play() {
    marche_.setText("Stop");
    prec_.setEnabled(false);
    suiv_.setEnabled(false);
    remplacer_.setEnabled(false);
    boolmarche_ = true;
    traj_.start();
  }

  public static String getSemi() {
    return ";";
  }

  class AL implements ActionListener {
    // boolean booldebut_;

    File directory_;

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final String compo = ((JButton) _e.getSource()).getName();
      if ("debut".equals(compo)) {
        pos_ = new ArrayList();
        dir_ = new ArrayList();
        nbCles_ = 0;
        nbClesLabel_.setText("nb Cl�s: 0");
        marche_.setEnabled(false);
        enregistrer_.setEnabled(true);
        sauver_.setEnabled(false);
        prec_.setEnabled(false);
        suiv_.setEnabled(false);
        remplacer_.setEnabled(false);
        cle_ = 0;
      } else if ("enregistrer".equals(compo)) {
        final Vector3f point = new Vector3f();
        final Quat4f quat = new Quat4f();
        u_.getUniversTransforms()[0].get(quat, point);
        pos_.add(new Point3f(point));
        dir_.add(quat);
        nbCles_++;
        nbClesLabel_.setText("nb Cl�s: " + nbCles_);
        if (!fin_.isEnabled()) {
          fin_.setEnabled(true);
        }
      } else if ("fin".equals(compo)) {
        enregistrer_.setEnabled(false);
        fin_.setEnabled(false);
        traj_ = new TrajectoireCamera(nbCles_, u_, true, 100000, -1);
        for (int i = 0; i < nbCles_; i++) {
          traj_.addCle(i, (float) i / nbCles_, (Point3f) pos_.get(i), (Quat4f) dir_.get(i));
        }
        marche_.setEnabled(true);
        sauver_.setEnabled(true);
        // prec_.setEnabled(true);
        suiv_.setEnabled(true);
        remplacer_.setEnabled(true);
      } else if ("marche".equals(compo)) {
        if (boolmarche_) {
          traj_.stop();
          marche_.setText("Play");
          // prec_.setEnabled(false);
          suiv_.setEnabled(true);
          remplacer_.setEnabled(true);
          u_.setUniversTransform(new Transform3D((Quat4f) dir_.get(cle_), new Vector3f((Tuple3f) pos_.get(cle_)), 1f));
          cle_ = 0;
          cleLabel_.setText("0");
        } else {
          marche_.setText("Stop");
          prec_.setEnabled(false);
          suiv_.setEnabled(false);
          remplacer_.setEnabled(false);
          traj_.start();
        }
        boolmarche_ = !boolmarche_;
      } else if ("precedent".equals(compo)) {
        cle_--;
        cleLabel_.setText("" + cle_);
        u_.setUniversTransform(new Transform3D((Quat4f) dir_.get(cle_), new Vector3f((Tuple3f) pos_.get(cle_)), 1f));
        if (cle_ == 0) {
          prec_.setEnabled(false);
        }
        if (cle_ < nbCles_ - 1) {
          suiv_.setEnabled(true);
        }
      } else if ("suivant".equals(compo)) {
        cle_++;
        cleLabel_.setText("" + cle_);
        u_.setUniversTransform(new Transform3D((Quat4f) dir_.get(cle_), new Vector3f((Tuple3f) pos_.get(cle_)), 1f));
        if (cle_ == nbCles_ - 1) {
          suiv_.setEnabled(false);
        }
        if (cle_ > 0) {
          prec_.setEnabled(true);
        }
      } else if ("remplacer".equals(compo)) {
        final Vector3f point = new Vector3f();
        final Quat4f quat = new Quat4f();
        u_.getUniversTransforms()[0].get(quat, point);
        pos_.set(cle_, new Point3f(point));
        dir_.set(cle_, quat);
        traj_.addCle(cle_, (float) cle_ / nbCles_, (Point3f) pos_.get(cle_), (Quat4f) dir_.get(cle_));
      } else if ("charger".equals(compo)) {
        File file = null;
        // String filename= null;
        if (traj_ != null) {
          traj_.stop();
        }
        marche_.setText("Play");
        boolmarche_ = false;
        final JFileChooser chooser = new JFileChooser();
        final BuFileFilter filter = new BuFileFilter(new String[] { "trj" }, "Fichier Trajectoire");
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(directory_);
        final int returnval = chooser.showOpenDialog(parent_);
        if (returnval == JFileChooser.APPROVE_OPTION) {
          file = chooser.getSelectedFile();
          /* filename= file.getAbsolutePath(); */
          chargerTrajectoire(file);
          marche_.setEnabled(true);
          suiv_.setEnabled(true);
          remplacer_.setEnabled(true);
          u_.setUniversTransform(new Transform3D((Quat4f) dir_.get(cle_), new Vector3f((Tuple3f) pos_.get(cle_)), 1f));
          cle_ = 0;
          cleLabel_.setText("0");
          directory_ = chooser.getCurrentDirectory();
        }
      } else if ("sauver".equals(compo)) {
        File file = null;
        /* String filename= null; */
        // String shortname=null;
        final JFileChooser chooser = new JFileChooser();
        final BuFileFilter filter = new BuFileFilter(new String[] { "trj" }, "Fichier Trajectoire");
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(directory_);
        final int returnval = chooser.showOpenDialog(parent_);
        if (returnval == JFileChooser.APPROVE_OPTION) {
          file = chooser.getSelectedFile();
          /* filename= file.getAbsolutePath(); */
          sauverTrajectoire(file);
          directory_ = chooser.getCurrentDirectory();
        }
      }
    }

    void chargerTrajectoire(final File _file) {
      LineNumberReader lineReader = null;
      try {
        lineReader = new LineNumberReader(new FileReader(_file));

        nbCles_ = 0;
        pos_ = new ArrayList();
        dir_ = new ArrayList();
        String ligne = null;
        try {
          ligne = lineReader.readLine();
        } catch (final IOException ex) {
          FuLog.error("Erreur de lecture", ex);
          return;
        }
        while (ligne != null) {
          final StringTokenizer token = new StringTokenizer(ligne, getSemi());
          if (token.countTokens() != 7) {
            FuLog.error("Erreur de lecture ligne " + lineReader.getLineNumber());
            return;
          }
          pos_.add(new Point3f(Float.parseFloat(token.nextToken()), Float.parseFloat(token.nextToken()), Float
              .parseFloat(token.nextToken())));
          dir_.add(new Quat4f(Float.parseFloat(token.nextToken()), Float.parseFloat(token.nextToken()), Float
              .parseFloat(token.nextToken()), Float.parseFloat(token.nextToken())));
          nbCles_++;
          try {
            ligne = lineReader.readLine();
          } catch (final IOException ex) {
            FuLog.error("Erreur de lecture", ex);
            return;
          }
        }
        enregistrer_.setEnabled(false);
        fin_.setEnabled(false);
        traj_ = new TrajectoireCamera(nbCles_, u_, true, 100000, -1);
        for (int i = 0; i < nbCles_; i++) {
          traj_.addCle(i, (float) i / nbCles_, (Point3f) pos_.get(i), (Quat4f) dir_.get(i));
        }
        marche_.setEnabled(true);
        nbClesLabel_.setText("nb Cl�s: " + nbCles_);
      } catch (final FileNotFoundException ex) {
        FuLog.error("Fichier non trouv�", ex);
      } finally {
        FuLib.safeClose(lineReader);

      }
    }

    void sauverTrajectoire(final File _file) {
      final NumberFormat nf = NumberFormat.getInstance(java.util.Locale.ENGLISH);
      nf.setMinimumFractionDigits(1);
      PrintStream out = null;
      try {
        out = new PrintStream(new FileOutputStream(_file));
      } catch (final IOException ex) {
        FuLog.error("Erreur d'�criture", ex);
        return;
      }
      for (int i = 0; i < nbCles_; i++) {
        final Point3f point = (Point3f) pos_.get(i);
        final Quat4f quat = (Quat4f) dir_.get(i);
        final String stringPos = nf.format(point.x) + getSemi() + nf.format(point.y) + getSemi() + nf.format(point.z);
        final String stringDir = nf.format(quat.x) + getSemi() + nf.format(quat.y) + getSemi() + nf.format(quat.z)
            + getSemi() + nf.format(quat.w);

        out.println(stringPos + getSemi() + stringDir);
      }
      out.close();
      FuLog.debug("Fichier sauvegard�");
    }
  }
}
