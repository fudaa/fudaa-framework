/*
 * @creation     2000-02-04
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import javax.media.j3d.Alpha;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.RotPosPathInterpolator;
import javax.media.j3d.Transform3D;
import javax.vecmath.AxisAngle4f;
import javax.vecmath.Point3f;
import javax.vecmath.Quat4f;

import com.memoire.fu.FuLog;

import org.fudaa.ebli.volume.common.BUniversInterface;

/**
 * @version $Revision: 1.10 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class TrajectoireCamera {
  private final int nbCles_;
  private float[] pourcentAtIndex_;
  private Point3f[] positions_;
  private Quat4f[] directions_;
  private final BUniversInterface univers_;
  private final boolean boucle_;
  private RotPosPathInterpolator trajectoire_;
  private int temps_;
  private final int nbCycles_;
  private Transform3D axe_;
  private BranchGroup bg_;

  public TrajectoireCamera(final int _nbCles, final BUniversInterface _u, final boolean _boucle, final int _temps,
      final int _nbCycles) {
    univers_ = _u;
    boucle_ = _boucle;
    nbCles_ = _nbCles;
    if (boucle_) {
      pourcentAtIndex_ = new float[nbCles_ + 1];
      positions_ = new Point3f[nbCles_ + 1];
      directions_ = new Quat4f[nbCles_ + 1];
    } else {
      pourcentAtIndex_ = new float[nbCles_];
      positions_ = new Point3f[nbCles_];
      directions_ = new Quat4f[nbCles_];
    }
    temps_ = _temps;
    nbCycles_ = _nbCycles;
    axe_ = new Transform3D();
    axe_.set(new AxisAngle4f(1f, 0f, 0f, 0f));
  }

  public void addCle(final int _index, final float _pourcent, final Point3f _position, final Quat4f _direction) {
    if (_index < nbCles_) {
      pourcentAtIndex_[_index] = _pourcent;
      positions_[_index] = _position;
      directions_[_index] = _direction;
      if (boucle_ && (_index == 0)) {
        pourcentAtIndex_[nbCles_] = 1f;
        positions_[nbCles_] = _position;
        directions_[nbCles_] = _direction;
      }
    } else {
      FuLog.warning("D�passement du nombre de Cl�s: " + _index + ">" + (nbCles_ - 1));
    }
  }

  public void start() {
    trajectoire_ = new RotPosPathInterpolator(new Alpha(nbCycles_, temps_), univers_.getTransformGroup(), axe_,
        pourcentAtIndex_, directions_, positions_);
    trajectoire_.setSchedulingBounds(new BoundingSphere());
    bg_ = new BranchGroup();
    bg_.setCapability(BranchGroup.ALLOW_DETACH);
    bg_.addChild(trajectoire_);
    univers_.addBranchGraph(bg_);
  }

  public void stop() {
    if (bg_ != null) {
      univers_.removeBranchGraph(bg_);
      bg_ = null;
    }
  }
}
