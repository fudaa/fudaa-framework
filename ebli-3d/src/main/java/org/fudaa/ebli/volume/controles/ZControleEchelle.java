/*
 * @creation     1999-12-30
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.Component;

import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fudaa.ebli.commun.EbliLib;

/**
 * @version $Revision: 1.6 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class ZControleEchelle implements ChangeListener {
  private EchelleClient client_;

  public interface EchelleClient {
    void setNouvelleEchelleZ(float _echelle);
  }
  private final JSpinner spinner_;

  public ZControleEchelle() {
    spinner_ = new JSpinner();
    spinner_.setToolTipText(EbliLib.getS("Modifier l'�chelle des z"));
    final SpinnerModel model = new SpinnerNumberModel(1, // initial value
        1, // min
        1000, // max
        1);
    spinner_.setModel(model);
    spinner_.getModel().addChangeListener(this);
    spinner_.setRequestFocusEnabled(false);
    spinner_.setFocusable(false);
  }

  public Component getComponent() {
    return spinner_;
  }

  private int oldVal_ = 1;

  @Override
  public void stateChanged(final ChangeEvent _e) {
    final SpinnerNumberModel source = (SpinnerNumberModel) _e.getSource();
    final int value = ((Integer) source.getValue()).intValue();
    if (client_ != null && value != oldVal_) {
      client_.setNouvelleEchelleZ(value);
      oldVal_ = value;
    }
  }

  public EchelleClient getClient() {
    return client_;
  }

  public void setClient(final EchelleClient _client) {
    client_ = _client;
  }
}
