/*
 * @creation     1999-11-29
 * @modification $Date: 2006-11-14 09:06:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.controle.BSelecteurCouleur;

/**
 * @version      $Revision: 1.12 $ $Date: 2006-11-14 09:06:28 $ by $Author: deniger $
 * @author       Christophe Delhorbe
 */
public class VolumePreferencesPanel extends BuAbstractPreferencesPanel {
  JCheckBox cbRapide_;
  BSelecteurCouleur coul_;
  BuCommonImplementation appli_;
  BuPreferences options_;

  public VolumePreferencesPanel(final BuCommonImplementation _appli) {
    options_= EbliPreferences.EBLI;
    appli_= _appli;
    final BuVerticalLayout lo= new BuVerticalLayout(5, true, false);
    setLayout(lo);
    int n= 0;
    cbRapide_=
      new JCheckBox("Desactiver le mode d'affichage 'Rapide' pendant les déplacements");
    cbRapide_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _evt) { setDirty(true); }
    });
    add(cbRapide_, n++);
    coul_= new BSelecteurCouleur();
    coul_.addPropertyChangeListener("couleur",new PropertyChangeListener() {
      @Override
      public void propertyChange(final PropertyChangeEvent _evt) { setDirty(true); }
    });
    final JLabel label= new JLabel("Couleur du fond");
    add(label, n++);
    add(coul_, n++);
    updateComponents();
  }
  @Override
  public String getToolTipText() {
    return "Volume";
  }
  @Override
  public String getTitle() {
    return "Volume";
  }
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  @Override
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  @Override
  public boolean isPreferencesApplyable() {
    return false;
  }
  @Override
  public void applyPreferences() {}
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }
  @Override
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }

  private void fillTable() {
    options_.putStringProperty(
      "volume.rapide",
      (cbRapide_.isSelected() ? "no" : "yes"));

    final Color couleur= coul_.getCouleur();
    options_.putStringProperty(
      "volume.background",
      ""
        + couleur.getRed()
        + CtuluLibString.VIR
        + couleur.getGreen()
        + CtuluLibString.VIR
        + couleur.getBlue());
      setDirty(false);
  }

  private void updateComponents() {
    final String s= options_.getStringProperty("volume.background");
    try {
      final String red= s.substring(0, s.indexOf(','));
      final String green= s.substring(s.indexOf(',') + 1, s.lastIndexOf(','));
      final String blue= s.substring(s.lastIndexOf(',') + 1);
      coul_.setCouleur(
        new Color(
          Integer.parseInt(red),
          Integer.parseInt(green),
          Integer.parseInt(blue)));
    } catch (final StringIndexOutOfBoundsException ex) {
      coul_.setCouleur(Color.black);
    }

    cbRapide_.setSelected(
      !"yes".equals(options_.getStringProperty("volume.rapide")));
    setDirty(false);
  }
}
