/*
 * @creation 2000-02-04
 * 
 * @modification $Date: 2007-05-22 14:19:03 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.Map;

import javax.media.j3d.Transform3D;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.ebli.volume.controles.BArbreVolume;
import org.fudaa.ebli.volume.controles.BControleVolume;
import org.fudaa.ebli.volume.controles.BPas;
import org.fudaa.ebli.volume.controles.BUniversInteraction;
import org.fudaa.ebli.volume.controles.ZControleEchelle;
import org.fudaa.ebli.volume.controles.ZControleEchelle.EchelleClient;

/**
 * @version $Id: ZVue3DPanel.java,v 1.12 2007-05-22 14:19:03 deniger Exp $
 * @author Christophe Delhorbe
 */
public class ZVue3DPanel extends JPanel implements CtuluImageProducer {

  class VueListener implements ItemListener {
    @Override
    public void itemStateChanged(final ItemEvent _e) {
      u_.setVue(((JComboBox) _e.getSource()).getSelectedIndex());
    }
  }

  public ZVueAnimAdapter createAnimAdapter() {
    return new ZVueAnimAdapter(this);
  }

  private BArbreVolume av_;
  private ZControleEchelle controleEchelle_;
  BGroupeStandard gs_;
  // private BuMenu mnVolumes_;
  private Transform3D[] t_;

  BUnivers u_;

  public ZVue3DPanel(final BuInformationsDocument _idAppli) {
    this(_idAppli, null);
  }

  public ZVue3DPanel(final BuInformationsDocument _idAppli, final BuCommonInterface _appli) {
    av_ = new BArbreVolume();
    setDoubleBuffered(true);
  }

  private void createUniverse() {
    if (u_ == null) {
      u_ = new BUnivers(new BCanvas3D(null/* SimpleUniverse.getPreferredConfiguration() */));
      u_.getCanvas3D().setSize(400, 300);
    }
  }

  ZVue3DController controller_;

  public final void build(final boolean _animation) {
    // u_.addPropertyChangeListener(frame_);
    Color backgroundUnivers = null;
    try {
      backgroundUnivers = EbliPreferences.EBLI.getColorProperty("volume.background", Color.LIGHT_GRAY);
    } catch (final StringIndexOutOfBoundsException ex) {
      backgroundUnivers = Color.LIGHT_GRAY;
    }
    if (backgroundUnivers == null)
      backgroundUnivers = Color.LIGHT_GRAY;
    u_.setBackground(backgroundUnivers);
    final JPanel jp = new JPanel();
    final BuHorizontalLayout lay = new BuHorizontalLayout(7, false, true);
    jp.setLayout(lay);
    final BUniversInteraction uiTmp = new BUniversInteraction(u_);

    final JComboBox vue = new JComboBox(BUnivers.getVues());
    vue.setToolTipText(EbliLib.getS("Changer le point de vue"));
    vue.setFocusable(false);
    vue.setEditable(false);
    vue.addItemListener(new VueListener());
    final BControleVolume cv = new BControleVolume(u_);
    cv.addRepereEventListener(uiTmp);
    cv.addPropertyChangeListener(uiTmp);
    final BPas coef = new BPas(cv);

    final JCheckBox orbital = new JCheckBox();
    orbital.addItemListener(uiTmp);
    orbital.setSelected(false);
    final JPanel boutons = new JPanel();
    boutons.setLayout(new BuGridLayout(2, 3, 3, true, true));
    // boutons.add(init);
    boutons.add(new BuLabel(EbliLib.getS("Vue")));
    boutons.add(vue);
    boutons.add(new BuLabel(EbliLib.getS("Mode déplacement")));
    boutons.add(cv.getComboBox());
    boutons.add(new BuLabel(coef.getTitre()));
    boutons.add(coef.getCb());

    boutons.add(new BuLabel("Orbital"));
    boutons.add(orbital);
    jp.add(cv);
    jp.add(boutons);
    controleEchelle_ = new ZControleEchelle();
    final EchelleClient cl = new EchelleClient() {

      @Override
      public void setNouvelleEchelleZ(float _echelle) {
        gs_.setNouvelleEchelleZ(_echelle);
        updateEchelle(_echelle);
      }

    };
    controleEchelle_.setClient(cl);
    final JPanel est = new JPanel(new BuVerticalLayout());
    est.add(av_);
    controller_ = new ZVue3DController(_animation);
    controller_.setVue(this);
    est.add(controller_.getPane());
    controller_.addAffComponent(EbliLib.getS("Echelle en z"), controleEchelle_.getComponent());
    controller_.getPane().setFocusable(false);
    setLayout(new BuBorderLayout());
    add(u_.getCanvas3D(), BuBorderLayout.CENTER);
    jp.setBorder(CtuluLibSwing.createTitleBorder(EbliLib.getS("Vue")));
    est.add(jp);
    add(est, BuBorderLayout.EAST);
    if (gs_ != null) {
      u_.setRoot(gs_);
    }
    if (t_ != null) {
      u_.setUniversTransforms(t_);
    }
    u_.getCanvas3D().freeze(false);
    u_.getCanvas3D().addKeyListener(cv.getKeyListener());
    u_.getCanvas3D().addMouseWheelListener(cv.getMouseWheelListener());
    final MouseListener spec = cv.getSpecMouseListener();
    u_.getCanvas3D().addMouseListener(spec);
    u_.getCanvas3D().addMouseMotionListener((MouseMotionListener) spec);
    u_.getCanvas3D().addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(final MouseEvent _e) {
        if (SwingUtilities.isMiddleMouseButton(_e)) {
          cv.translateZ(_e.isShiftDown() ? 2 : -2);
          return;
        }
      }

    });
  }

  protected void updateEchelle(final float _z) {
    getUnivers().updateEchelle(_z);

  }

  public BArbreVolume getArbreVolume() {
    return av_;
  }

  public Object getSelectedObject() {
    return av_.isSelectionEmpty() ? null : av_.getSelectionPath().getLastPathComponent();
  }

  public void updateFrame(final JFrame _f) {
    controller_.updateFrame(_f);
  }

  public void addUserComponent(final String _name, final JComponent _c) {
    controller_.addAffComponent(_name, _c);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return u_.getCanvas3D().getSize();
  }

  public String[] getEnabledActions() {
    return new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER", CtuluExportDataInterface.EXPORT_CMD, CtuluLibImage.SNAPSHOT_COMMAND };
  }

  public int getNumberOfPages() {
    return 1;
  }

  public BUnivers getUnivers() {
    return u_;
  }

  public int print(final Graphics _g, final PageFormat _f, final int _page) {
    if (_page != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    final Image i = u_.getCanvas3D().print();
    EbliPrinter.printImage(_g, _f, i);
    return Printable.PAGE_EXISTS;
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return u_.getCanvas3D().print();
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return CtuluLibImage.resize(produceImage(null), _w, _h);
  }

  public void setRoot(final BGroupeStandard _root) {
    gs_ = _root;
    createUniverse();
    u_.setRoot(gs_);
    // if (mustBuild) build();
    av_.setVolume(u_.getRoot());
    if (controleEchelle_ != null) {
      controleEchelle_.setClient(gs_);
    }

  }

  public Color getBackgroundUnivers() {
    return u_.getBackground();
  }

  public void setBackgroundUnivers(Color _backgroundUnivers) {
    u_.setBackground(_backgroundUnivers);
  }

}
