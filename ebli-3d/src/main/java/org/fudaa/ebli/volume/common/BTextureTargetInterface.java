/*
 * @creation 18 mai 2006
 * @modification $Date: 2006-06-12 09:55:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.common;

import java.io.File;

import javax.media.j3d.Texture;

/**
 * @author fred deniger
 * @version $Id: BTextureTargetInterface.java,v 1.1 2006-06-12 09:55:22 deniger Exp $
 */
public interface BTextureTargetInterface {

  /**
   * applique la texture t sur l'objet.
   */
  void setTexture(Texture _t);

  /**
   * affiche ou non le texture.
   */
  void setTextureEnable(boolean _activee);

  boolean isTextureEnable();

  /**
   * mode de la texture (Modulate/Decal/Replace/Blend).
   */
  void setTextureAttributesMode(int _mode);

  Texture getTexture();

  int getTextureAttributesMode();

  File getTextureImgUrl();

  void setTextureImgUrl(File _textureImgUrl);

}