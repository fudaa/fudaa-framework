/*
 * @creation 18 mai 2006
 * @modification $Date: 2007-05-22 14:19:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Action;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuToolButton;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluUIAbstract;
import org.fudaa.ctulu.ProgressionFuAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ebli.animation.ActionAnimationTreeSelection;
import org.fudaa.ebli.animation.EbliAnimation;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.PaletteEditAction;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.common.BControleAffichageTarget;
import org.fudaa.ebli.volume.controles.BArbreVolume;
import org.fudaa.ebli.volume.controles.BControleAffichage;
import org.fudaa.ebli.volume.controles.ZControleLumiere;

/**
 * @author fred deniger
 * @version $Id: ZVue3DController.java,v 1.12 2007-05-22 14:19:03 deniger Exp $
 */
public class ZVue3DController {

  JTabbedPane pane_;
  ZVue3DPanel vue_;
  BuPanel affichage_;
  final boolean animation_;

  public ZVue3DController(final boolean _animation) {
    super();
    animation_ = _animation;
  }

  public JTabbedPane getPane() {
    if (pane_ == null) {
      buildComponent(animation_);
    }
    return pane_;
  }

  public void addAffComponent(final String _name, final Component _c) {
    affichage_.add(new BuLabel(_name));
    affichage_.add(_c);

  }

  public void updateFrame(final JFrame _f) {
    if (_f == null) {
      return;
    }
    final Action[] act = buildAction(_f);
    if (act == null) {
      return;
    }
    final BuMenuBar bar = new BuMenuBar();

    for (int i = 0; i < act.length; i++) {
      final BuToolButton bt = new BuToolButton();
      bt.setAction(act[i]);
      bt.setToolTipText((String) act[i].getValue(Action.SHORT_DESCRIPTION));
      bar.add(bt);
    }
    EbliLib.updateMapKeyStroke(_f.getRootPane(), act);
    _f.setJMenuBar(bar);
    _f.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(final WindowEvent _evt) {
        getVue().getArbreVolume().clearSelection();
        getVue().getUnivers().destroy();

      }

    });

  }

  private static class VueUI extends CtuluUIAbstract {
    final JFrame frame_;

    public VueUI(final JFrame _frame) {
      super();
      frame_ = _frame;
    }

    @Override
    public void error(final String _titre, final String _msg, final boolean _tempo) {
      CtuluLibDialog.showError(frame_, _titre, _msg);

    }

    @Override
    public void clearMainProgression() {}

    @Override
    public ProgressionInterface getMainProgression() {
      return new ProgressionFuAdapter(frame_.getTitle());
    }

    @Override
    public Component getParentComponent() {
      return frame_;
    }

    @Override
    public void message(final String _titre, final String _msg, final boolean _tempo) {
      CtuluLibDialog.showMessage(frame_, _titre, _msg);

    }

    @Override
    public boolean question(final String _titre, final String _text) {
      return CtuluLibDialog.showConfirmation(frame_, _titre, _text);
    }

    @Override
    public void warn(final String _titre, final String _msg, final boolean _tempo) {
      CtuluLibDialog.showWarn(frame_, _titre, _msg);

    }

  }

  public Action[] buildAction(final JFrame _f) {
    final EbliActionSimple act = new EbliActionSimple(EbliLib.getS("Reinitialiser"), EbliResource.EBLI
        .getIcon("restore"), "REINIT") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        vue_.getUnivers().init();

      }
    };
    act.setKey(KeyStroke.getKeyStroke(KeyEvent.VK_R, 0));
    act.setDefaultToolTip(EbliLib.getS("Rafra�chir la vue"));
    final EbliActionSimple ex = new EbliActionSimple(CtuluLib.getS("Exporter image"), BuResource.BU
        .getIcon("photographie"), CtuluLibImage.SNAPSHOT_COMMAND) {
      @Override
      public void actionPerformed(ActionEvent _e) {
        CtuluImageExport.exportImageFor(new VueUI(_f), vue_);
      }
    };
    ex.setDefaultToolTip(CtuluLib.getS("Cr�er une image � partir de la fen�tre active"));
    //Changer la couleur du fond
    final EbliActionSimple col = new EbliActionSimple(EbliLib.getS("Changer la couleur du fond"), BuResource.BU
        .getToolIcon("arriereplan"), "COLOR") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        Color old = vue_.getBackgroundUnivers();
        Color newC = JColorChooser.showDialog(vue_, EbliResource.EBLI.getString("Couleur de fond"), old);
        if (newC != null && !CtuluLib.isEquals(newC, old)) {
          vue_.setBackgroundUnivers(newC);
        }

      }

    };
    return new Action[] { act, ex, col };
  }
  
  public JTabbedPane getTabbedPane() {
    return pane_;
  }

  void buildComponent(final boolean _animation) {
    pane_ = new BuTabbedPane();
    final BArbreVolume av = vue_.getArbreVolume();
    final BControleAffichage controleAff = new BControleAffichage();
    affichage_ = controleAff.getPn();
    pane_.addTab(EbliLib.getS("Affichage"), BuResource.BU.getIcon("configurer"), affichage_, EbliLib.getS("Affichage"));

    final PaletteEditAction paletteAct = new PaletteEditAction(av.getSelectionModel()) {
      @Override
      public boolean isSelected() {
        return true;
      }

    };
    pane_.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(final ChangeEvent _e) {
        if (pane_.getSelectedIndex() == 0) {
          controleAff.updateApplyBt();
        }

      }

    });

    pane_.addTab(EbliResource.EBLI.getString("Palette"), EbliResource.EBLI.getIcon("palettecouleur"), paletteAct
        .getPaletteContent(), EbliResource.EBLI.getString("Couleurs du volume"));
    final ZControleLumiere controleLumiere = new ZControleLumiere();
    av.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

      @Override
      public void valueChanged(final TreeSelectionEvent _e) {
        final Object o = vue_.getSelectedObject();
        controleAff.setTarget((o instanceof BControleAffichageTarget) ? (BControleAffichageTarget) o : null);
        controleLumiere.setObjectSrc(o);
      }

    });

    pane_.addTab(EbliResource.EBLI.getString("Lumi�re"), EbliResource.EBLI.getIcon("lumiere"), controleLumiere,
        EbliResource.EBLI.getString("R�glage de la lumiere"));
    if (_animation) {
      final EbliAnimation animation = new EbliAnimation() {
        @Override
        public boolean isActivated() {
          return true;
        }
      };
      final ActionAnimationTreeSelection act = new ActionAnimationTreeSelection(vue_.createAnimAdapter(), animation);
      av.getSelectionModel().addTreeSelectionListener(act);
      act.updateFromModel(av.getSelectionModel());
      final JPanel pn = act.getAnim().getPanel();
      // pour activer et enlever le bouton de configuration
      act.getAnim().getConfigureBt().setSelected(true);
      act.getAnim().getConfigureBt().setVisible(false);
      pane_.addTab(act.getTitle(), act.getIcon(), pn, act.getDefaultTooltip());
    }
  }

  public ZVue3DPanel getVue() {
    return vue_;
  }

  public void setVue(final ZVue3DPanel _vue) {
    vue_ = _vue;
  }

}
