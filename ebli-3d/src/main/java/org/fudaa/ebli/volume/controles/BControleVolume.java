/*
 * @creation     1999-10-12
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuLightBorder;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.repere.RepereEventListener;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.common.BUniversInterface;

/**
 * @version $Revision: 1.19 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BControleVolume extends JComponent {
  class KeyThread extends Thread implements KeyListener {

    private long nextTime_;

    @Override
    public void keyPressed(final KeyEvent _k) {
      final long current = System.currentTimeMillis();
      if (current >= nextTime_) {
        final float toRadians = getRotateAngle();
        final int transFactor = 2;
        if (_k.getKeyCode() == KeyEvent.VK_Z || _k.getKeyCode() == KeyEvent.VK_SPACE) {
          translateZ(_k.isShiftDown() ? transFactor : -transFactor);
        } else if (_k.getKeyCode() == KeyEvent.VK_RIGHT) {
          if (_k.isControlDown()) {
            rotateY(toRadians);
          } else {
            translateX(transFactor);
          }
        } else if (_k.getKeyCode() == KeyEvent.VK_LEFT) {
          if (_k.isControlDown()) {
            rotateY(-toRadians);
          } else {
            translateX(-transFactor);
          }
        } else if (_k.getKeyCode() == KeyEvent.VK_UP) {
          if (_k.isControlDown()) {
            rotateX(-toRadians);
          } else {
            translateZ(-transFactor);
          }
        } else if (_k.getKeyCode() == KeyEvent.VK_DOWN) {
          if (_k.isControlDown()) {
            rotateX(toRadians);
          } else {
            translateZ(transFactor);
          }
        }

        nextTime_ = current + 100;
      }

    }

    @Override
    public void keyReleased(final KeyEvent _k) {
      nextTime_ = 0;

    }

    @Override
    public void keyTyped(final KeyEvent _k) {
      if (threadX_ != null) {
        threadY_.arrete();
        threadX_.arrete();
      }
      firePropertyChange("rapide", true, false);
      direction_ = 0;
      repaint();
    }
  }

  class VueMouseListener extends MouseAdapter implements MouseMotionListener {

    int x_;
    int y_;

    @Override
    public void mouseDragged(final MouseEvent _e) {
      if (SwingUtilities.isLeftMouseButton(_e)) {
        final int newX = _e.getX();
        final int newY = _e.getY();
        float defPas = 0.5f;
        float pasX = newX - x_;
        if (pasX < 0) {
          pasX = defPas;
        } else if (pasX > 0) {
          pasX = -defPas;
        }
        float pasY = newY - y_;
        if (pasY < 0) {
          pasY = defPas;
        } else if (pasY > 0) {
          pasY = -defPas;
        }
        if (_e.isControlDown()) {
          if (Math.abs(newX - x_) > 1) {
            final float rotateAngle = getRotateAngle() / 5;
            rotateY(pasX > 0 ? rotateAngle : -rotateAngle);
          } else if (Math.abs(newY - y_) > 1) {
            final float rotateAngle = getRotateAngle() / 5;
            rotateX(pasY > 0 ? rotateAngle : -rotateAngle);
          }

        } else {
          translateX(pasX);
          translateZ(pasY);
        }
        x_ = newX;
        y_ = newY;
      }

    }

    @Override
    public void mousePressed(final MouseEvent _e) {
      super.mousePressed(_e);
      x_ = _e.getX();
      y_ = _e.getY();
    }

    @Override
    public void mouseMoved(final MouseEvent _e) {}

  }

  class Listener extends MouseAdapter implements MouseMotionListener, MouseWheelListener {
    @Override
    public void mouseWheelMoved(final MouseWheelEvent _e) {
      if (_e.isControlDown()) {
        rotateY(_e.getWheelRotation() < 0 ? -getRotateAngle() : getRotateAngle());
      } else if (_e.isShiftDown()) {
        translateX(_e.getWheelRotation() < 0 ? -2 : 2);
      } else {
        translateY(_e.getWheelRotation() < 0 ? -2 : 2);
      }

    }

    @Override
    public void mouseClicked(final MouseEvent _e) {
      if (SwingUtilities.isRightMouseButton(_e)) {
        changeMode();
      } else if (SwingUtilities.isLeftMouseButton(_e) && (_e.isShiftDown())) {
        final int x = _e.getX() - (getSize().width / 2);
        final int y = _e.getY() - (getSize().width / 2);
        final RepereEvent evt = new RepereEvent(this, false);
        switch (mode_) {
        case 0:
          if (Math.abs(x) > Math.abs(y)) {
            evt.ajouteTransformation(RepereEvent.TRANS_X, (x > 0 ? 1 : -1) * coefX_ * 10
            /* / ParametresVolumes.ECHELLE */, RepereEvent.RELATIF);
          } else {
            evt.ajouteTransformation(RepereEvent.TRANS_Z,
                (y > 0 ? 1 : -1) * coefZ_ * 10 /* / ParametresVolumes.ECHELLE */, RepereEvent.RELATIF);
          }
          break;
        case 1:
          if (Math.abs(x) > Math.abs(y)) {
            evt.ajouteTransformation(RepereEvent.ROT_Y, (x < 0 ? .1 : -.1), RepereEvent.RELATIF);
          } else {
            evt.ajouteTransformation(RepereEvent.ROT_X, (y > 0 ? .1 : -.1), RepereEvent.RELATIF);
          }
          break;
        case 2:
          evt.ajouteTransformation(RepereEvent.TRANS_Y,
              (y < 0 ? 1 : -1) * coefY_ * 10 /* / ParametresVolumes.ECHELLE */, RepereEvent.RELATIF);
          break;
        default:
          break;

        }
        fireRepereEvent(evt);
      }
    }

    @Override
    public void mouseDragged(final MouseEvent _e) {
      if (SwingUtilities.isLeftMouseButton(_e)) {
        calculPas(_e.getX(), _e.getY());
        changeDirection();
      }
    }

    @Override
    public void mouseMoved(final MouseEvent _evt) {}

    @Override
    public void mousePressed(final MouseEvent _e) {
      requestFocus();
      if (SwingUtilities.isLeftMouseButton(_e) && !(_e.isShiftDown())) {
        firePropertyChange("rapide", false, true);
        if (threadX_ != null) {
          threadX_.arrete();
        }
        if (threadY_ != null) {
          threadY_.arrete();

        }
        threadX_ = new MonThread(tg_, "X");
        threadY_ = new MonThread(tg_, "Y");

        xinit_ = _e.getX();
        yinit_ = _e.getY();
        switch (mode_) {
        case 0:
          threadX_.setTransfo(RepereEvent.TRANS_X);
          threadY_.setTransfo(RepereEvent.TRANS_Z);
          break;
        case 1:
          threadX_.setTransfo(RepereEvent.ROT_Y);
          threadY_.setTransfo(RepereEvent.ROT_X);
          break;
        case 2:
          threadX_.setTransfo(999);
          threadY_.setTransfo(RepereEvent.TRANS_Y);
          break;
        default:
          break;
        }
        pasTX_ = 0;
        pasTY_ = 0;
        pasRX_ = 0f;
        pasRY_ = 0f;
        threadY_.start();
        threadX_.start();
      }
    }

    @Override
    public void mouseReleased(final MouseEvent _e) {
      if (SwingUtilities.isLeftMouseButton(_e) && !(_e.isShiftDown())) {
        threadY_.arrete();
        threadX_.arrete();
        firePropertyChange("rapide", true, false);
        direction_ = 0;
        repaint();
        // repereEventListeners_=new Vector();
      }
    }
  }

  class MonThread extends Thread {
    private boolean arret_;

    private int transfo_;

    public MonThread(final ThreadGroup _tg, final String _s) {
      super(_tg, _s);
    }

    public synchronized void arrete() {
      arret_ = true;
    }

    @Override
    public void run() {
      while (!arret_) {
        try {
          final RepereEvent evt = new RepereEvent(this, false);
          switch (transfo_) {
          case RepereEvent.TRANS_X:
            createTranslationX(evt);
            break;
          case RepereEvent.TRANS_Y:
            createTranslationY(evt);
            break;
          case RepereEvent.TRANS_Z:
            evt.ajouteTransformation(transfo_, pasTY_ * coefZ_, RepereEvent.RELATIF);
            break;
          case RepereEvent.ROT_Y:
            createRotY(evt);
            break;
          case RepereEvent.ROT_X:
            createRotX(evt);
            break;
          default:
            break;
          }
          fireRepereEvent(evt);
          sleep(100);
          // yield();
        } catch (final InterruptedException e) {}
      }
    }

    public void setTransfo(final int _transfo) {
      transfo_ = _transfo;
    }

    @Override
    public void start() {
      arret_ = false;
      super.start();
    }
  }

  /**
   * non private car acceder depuis une inner classe.
   */
  float coefX_;

  /**
   * non private car acceder depuis une inner classe.
   */
  float coefY_;

  float coefZ_;

  int direction_;

  Image[] image_;

  Image[] imageA_;

  Image[] imageR_;

  KeyThread k_;

  int mode_;

  float pasRX_;

  float pasRY_;

  int pasTX_;

  int pasTY_;

  final BUniversInterface u_;

  boolean premiere_ = true;

  String rc_;

  List repereEventListeners_;

  int sleepX_;

  int sleepY_;

  ThreadGroup tg_;

  MonThread threadX_;

  MonThread threadY_;
  boolean translation_;
  int xinit_;

  int yinit_;

  public BControleVolume(final BUniversInterface _u) {
    u_ = _u;
    mode_ = 0;
    direction_ = 0;
    coefX_ = 1f;
    coefY_ = 1f;
    coefZ_ = 1f;
    rc_ = "fleches";
    load();
    setPreferredSize(new Dimension(96, 96));
    sleepX_ = 100;
    sleepY_ = 100;
    translation_ = true;
    repereEventListeners_ = new ArrayList();
    final Listener l = new Listener();
    addMouseListener(l);
    addMouseMotionListener(l);
    // addMouseWheelListener(l);
    // addPropertyChangeListener(new Listener());
    setBorder(new BuLightBorder(BuLightBorder.LOWERED, 1));
    tg_ = new ThreadGroup("Groupe");
    k_ = new KeyThread();
    addKeyListener(k_);
    setRequestFocusEnabled(true);
    final String s1 = "<u>" + EbliLib.getS("D�placer la vue") + "</u><ul><li>"
        + EbliLib.getS("Maintenez le bouton gauche de la souris pour d�placer la vue") + "</li><li>"
        + EbliLib.getS("Clic droit pour changer le mode de d�placement") + "</li></ul><br>"
        + EbliLib.getS("Le clavier peut-�tre utilis�:") + "<ul><li>"
        + EbliLib.getS("Les fl�ches (et Shift) pour d�placer la vue") + "</li><li>"
        + EbliLib.getS("la touche 'z' ou la barre d'espace (et Shift ) pour zoomer (ou 'd�zoomer'") + "</li><li>"
        + EbliLib.getS("la touche 'r' pour restaurer la vue");

    setToolTipText("<html><body>" + s1 + "</ul></body></html>");
    // k.start();
  }

  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  JComboBox cb_;

  private void createModel() {
    if (cb_ == null) {
      cb_ = new BuComboBox(new String[] { EbliLib.getS("Translation"), EbliLib.getS("Rotation"),
          EbliLib.getS("Translation en z") });
      cb_.getModel().addListDataListener(new ListDataListener() {

        @Override
        public void intervalRemoved(final ListDataEvent _e) {}

        @Override
        public void intervalAdded(final ListDataEvent _e) {}

        @Override
        public void contentsChanged(final ListDataEvent _e) {
          setMode(cb_.getSelectedIndex());

        }

      });
    }
  }

  public JComboBox getComboBox() {
    createModel();
    return cb_;
  }

  public void calculPas(final int _x, final int _y) {
    pasTY_ = ((_y - yinit_) / 25);
    pasTX_ = ((_x - xinit_) / 25);
    pasRX_ = ((_x - xinit_) / 10F) / 200F;
    if (pasRX_ > .01) {
      pasRX_ = .01f;
    } else if (pasRX_ < -.01) {
      pasRX_ = -.01f;
    }
    pasRY_ = ((_y - yinit_) / 10F) / 200F;
    if (pasRY_ > .01) {
      pasRY_ = .01f;
    } else if (pasRY_ < -.01) {
      pasRY_ = -.01f;
      // sleep_Y=100/(Math.abs(_y-yinit_)+1);
      // sleep_X=100/(Math.abs(_x-xinit_)+1);
    }
  }

  public void changeDirection() {
    final int oldDirection = direction_;
    double pasx, pasy;
    if (translation_) {
      pasx = pasTX_;
      pasy = pasTY_;
    } else {
      pasx = pasRX_;
      pasy = pasRY_;
    }
    if (pasx > 0) {
      if (pasy < 0) {
        direction_ = 2;
      } else if (pasy == 0) {
        direction_ = 3;
      } else {
        direction_ = 4;
      }
    } else if (pasx == 0) {
      if (pasy < 0) {
        direction_ = 1;
      } else if (pasy == 0) {
        direction_ = 0;
      } else {
        direction_ = 5;
      }
    } else if (pasy < 0) {
      direction_ = 8;
    } else if (pasy == 0) {
      direction_ = 7;
    } else {
      direction_ = 6;
    }
    if (direction_ != oldDirection) {
      repaint();
    }
  }

  // RepereEvent listeners
  public void changeMode() {
    // translation_=!translation_;
    setMode(mode_ + 1);
  }

  public void setMode(final int _i) {
    final int newMode = _i % 3;
    if (newMode != mode_) {
      mode_ = newMode;
      direction_ = 0;
      repaint();
      if (cb_ != null) {
        cb_.setSelectedIndex(mode_);
      }
    }
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    for (int i = 0; i < repereEventListeners_.size(); i++) {
      ((RepereEventListener) repereEventListeners_.get(i)).repereModifie(_evt);
    }
  }

  public String getRc() {
    return rc_;
  }

  public final void load() {
    final MediaTracker tracker = new MediaTracker(this);
    image_ = new Image[9];
    imageR_ = new Image[9];
    imageA_ = new Image[9];
    final String srot = "volume/" + rc_ + "/rotation/";
    final String strans = "volume/" + rc_ + "/translation/";
    final String sascen = "volume/" + rc_ + "/ascenseur/";
    image_[0] = EbliResource.EBLI.getImage(strans + "centre.jpg");
    image_[1] = EbliResource.EBLI.getImage(strans + "nord.jpg");
    image_[2] = EbliResource.EBLI.getImage(strans + "nord-est.jpg");
    image_[3] = EbliResource.EBLI.getImage(strans + "est.jpg");
    image_[4] = EbliResource.EBLI.getImage(strans + "sud-est.jpg");
    image_[5] = EbliResource.EBLI.getImage(strans + "sud.jpg");
    image_[6] = EbliResource.EBLI.getImage(strans + "sud-ouest.jpg");
    image_[7] = EbliResource.EBLI.getImage(strans + "ouest.jpg");
    image_[8] = EbliResource.EBLI.getImage(strans + "nord-ouest.jpg");
    imageR_[0] = EbliResource.EBLI.getImage(srot + "centre.jpg");
    imageR_[1] = EbliResource.EBLI.getImage(srot + "nord.jpg");
    imageR_[2] = EbliResource.EBLI.getImage(srot + "nord-est.jpg");
    imageR_[3] = EbliResource.EBLI.getImage(srot + "est.jpg");
    imageR_[4] = EbliResource.EBLI.getImage(srot + "sud-est.jpg");
    imageR_[5] = EbliResource.EBLI.getImage(srot + "sud.jpg");
    imageR_[6] = EbliResource.EBLI.getImage(srot + "sud-ouest.jpg");
    imageR_[7] = EbliResource.EBLI.getImage(srot + "ouest.jpg");
    imageR_[8] = EbliResource.EBLI.getImage(srot + "nord-ouest.jpg");
    imageA_[0] = EbliResource.EBLI.getImage(sascen + "centre.jpg");
    imageA_[1] = EbliResource.EBLI.getImage(sascen + "nord.jpg");
    imageA_[2] = EbliResource.EBLI.getImage(sascen + "nord-est.jpg");
    imageA_[3] = EbliResource.EBLI.getImage(sascen + "est.jpg");
    imageA_[4] = EbliResource.EBLI.getImage(sascen + "sud-est.jpg");
    imageA_[5] = EbliResource.EBLI.getImage(sascen + "sud.jpg");
    imageA_[6] = EbliResource.EBLI.getImage(sascen + "sud-ouest.jpg");
    imageA_[7] = EbliResource.EBLI.getImage(sascen + "ouest.jpg");
    imageA_[8] = EbliResource.EBLI.getImage(sascen + "nord-ouest.jpg");
    for (int i = 0; i < 9; i++) {
      tracker.addImage(image_[i], i);
      tracker.addImage(imageR_[i], i + 8);
      tracker.addImage(imageA_[i], i + 16);
    }
    try {
      tracker.waitForAll();
    } catch (final InterruptedException e) {}
    repaint();
  }

  @Override
  public void paintComponent(final Graphics _g) {
    final Dimension size = getSize();
    switch (mode_) {
    case 0:
      _g.drawImage(image_[direction_], 0, 0, (int) size.getWidth(), (int) size.getHeight(), 0, 0, image_[direction_]
          .getWidth(this), image_[direction_].getHeight(this), null);
      break;
    case 1:
      _g.drawImage(imageR_[direction_], 0, 0, (int) size.getWidth(), (int) size.getHeight(), 0, 0, image_[direction_]
          .getWidth(this), image_[direction_].getHeight(this), null);
      break;
    case 2:
      _g.drawImage(imageA_[direction_], 0, 0, (int) size.getWidth(), (int) size.getHeight(), 0, 0, image_[direction_]
          .getWidth(this), image_[direction_].getHeight(this), null);
      break;
    default:
      break;
    }
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  public void setCoef(final float _c) {
    coefX_ = _c;
    coefY_ = _c;
    coefZ_ = _c;
  }

  public void setCoef(final float _x, final float _y, final float _z) {
    coefX_ = _x;
    coefY_ = _y;
    coefZ_ = _z;
  }

  public void setRc(final String _s) {
    rc_ = _s;
    load();
  }

  public BUniversInterface getU() {
    return u_;
  }

  void translateX(final float _pas) {
    final RepereEvent evt = new RepereEvent(this, false);
    createTranslationX(_pas, evt);
    fireRepereEvent(evt);
  }

  void translateY(final float _pas) {
    final RepereEvent evt = new RepereEvent(this, false);
    createTranslationY(_pas, evt);
    fireRepereEvent(evt);
  }

  void rotateX(final float _pas) {
    final RepereEvent evt = new RepereEvent(this, false);
    createRotX(_pas, evt);
    fireRepereEvent(evt);
  }

  void rotateY(final float _pas) {
    final RepereEvent evt = new RepereEvent(this, false);
    createRotY(_pas, evt);
    fireRepereEvent(evt);
  }

  public void translateZ(final float _pas) {
    final RepereEvent evt = new RepereEvent(this, false);
    createTranslationZ(_pas, evt);
    fireRepereEvent(evt);
  }

  void createTranslationX(final float _pas, final RepereEvent _evt) {
    _evt.ajouteTransformation(RepereEvent.TRANS_X, _pas * coefX_, RepereEvent.RELATIF);
  }

  void createTranslationZ(final float _pas, final RepereEvent _evt) {
    _evt.ajouteTransformation(RepereEvent.TRANS_Z, _pas * coefZ_, RepereEvent.RELATIF);
  }

  void createTranslationY(final float _pas, final RepereEvent _evt) {
    _evt.ajouteTransformation(RepereEvent.TRANS_Y, _pas * coefY_, RepereEvent.RELATIF);
  }

  void createTranslationY(final RepereEvent _evt) {
    createTranslationY(-pasTY_, _evt);
  }

  void createTranslationX(final RepereEvent _evt) {
    createTranslationX(pasTX_, _evt);
  }

  void createRotX(final RepereEvent _evt) {
    createRotX(pasRY_, _evt);
  }

  void createRotX(final float _pas, final RepereEvent _evt) {
    _evt.ajouteTransformation(RepereEvent.ROT_X, _pas, RepereEvent.RELATIF);
  }

  void createRotY(final RepereEvent _evt) {
    createRotY(pasRX_, _evt);
  }

  void createRotY(final float _pas, final RepereEvent _evt) {
    _evt.ajouteTransformation(RepereEvent.ROT_Y, _pas, RepereEvent.RELATIF);
  }

  public KeyThread getKeyListener() {
    return k_;
  }

  public MouseWheelListener getMouseWheelListener() {
    return new Listener();
  }

  public MouseListener getSpecMouseListener() {
    return new VueMouseListener();
  }

  protected float getRotateAngle() {
    return (float) Math.toRadians(5);
  }
}
