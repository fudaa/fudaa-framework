/*
 * @creation     1999-10-12
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.vecmath.Vector3d;

/**
 * Affiche la position de la cam�ra dans l'univers.
 * 
 * @version $Revision: 1.14 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BPosition extends JPanel implements PropertyChangeListener {
  private final JLabel[] pos_;
  private final DecimalFormat nf_;

  public BPosition() {
    nf_ = new DecimalFormat();
    nf_.setMinimumFractionDigits(0);
    nf_.setMaximumFractionDigits(2);
    nf_.setMinimumIntegerDigits(1);
    nf_.setMaximumIntegerDigits(4);
    setLayout(new GridLayout(4, 2));
    JLabel[] labels = new JLabel[3];
    pos_ = new JLabel[3];
    labels[0] = new JLabel("X");
    labels[1] = new JLabel("Y");
    labels[2] = new JLabel("Z");
    pos_[0] = new JLabel(nf_.format(0));
    pos_[1] = new JLabel(nf_.format(0));
    pos_[2] = new JLabel(nf_.format(0));
    // JPanel[] lignes=new JPanel[4];
    // lignes[0]=new JPanel(new FlowLayout());
    // lignes[0].add(new JLabel(" "));
    // lignes[0].add(new JLabel("Pos"));
    add(new JLabel(" "));
    add(new JLabel("Pos"));
    // add(lignes[0]);
    for (int i = 1; i < 4; i++) {
      // lignes[i]=new JPanel(new FlowLayout());
      // lignes[i].add(labels[i-1]);
      // lignes[i].add(pos[i-1]);
      // add(lignes[i]);
      add(labels[i - 1]);
      add(pos_[i - 1]);
    }
  }

  // quand ce bean recoit un PropertyChangeEvent (coordonn�es ont chang�)
  // il remet � jour son affichage
  @Override
  public void propertyChange(final PropertyChangeEvent _e) {
    if (_e.getPropertyName() == "position") {
      final Vector3d oldPos = (Vector3d) _e.getOldValue();
      final Vector3d newPos = (Vector3d) _e.getNewValue();
      if (!(oldPos.equals(newPos))) {
        pos_[0].setText(nf_.format(newPos.x /** ParametresVolumes.ECHELLE */
        ));
        pos_[1].setText(nf_.format(newPos.y /** ParametresVolumes.ECHELLE */
        ));
        pos_[2].setText(nf_.format(newPos.z /** ParametresVolumes.ECHELLE */
        ));
      }
    }
  }
}
