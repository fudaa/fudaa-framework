/*
 * @creation     1999-11-29
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import javax.media.j3d.*;
import javax.vecmath.Color4f;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Repr�sentation d'un champ de vecteurs 3D.
 * 
 * @version $Revision: 1.13 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BChampVecteurs extends BGrille {
  private final Matrix3d rot_ = new Matrix3d();


  /**
   * constucteur par defaut.
   */
  public BChampVecteurs() {
    super();
  }
  
  
  @Override
  public void setVisible(final boolean _valeur) {
    super.setVisible(_valeur);
  }

  /**
   * On construit un champs de vecteur de nom <I>s</I>.
   */
  public BChampVecteurs(final String _s) {
    super(_s);
  }

  private Point3d[] fleche(final Point3d _point, final Vector3d _vecteur, double _chapeau) {
    Point3d[] lines = new Point3d[6];
    double alpha, beta;
    lines[0] = new Point3d();
    if (_vecteur.y > 0) {
      lines[1] = new Point3d(0, _vecteur.length(), 0);
      lines[2] = new Point3d(_chapeau, _vecteur.length() - _chapeau, 0);
      lines[3] = new Point3d(-_chapeau, _vecteur.length() - _chapeau, 0);
      lines[4] = new Point3d(0, _vecteur.length() - _chapeau, _chapeau);
      lines[5] = new Point3d(0, _vecteur.length() - _chapeau, -_chapeau);
      if (_vecteur.x != 0) {
        alpha = -Math.atan(_vecteur.x / _vecteur.y);
        rot_.setZero();
        rot_.rotZ(alpha);
      } else {
        alpha = Math.atan(_vecteur.z / _vecteur.y);
        rot_.setZero();
        rot_.rotX(alpha);
      }
      rot_.transform(lines[1]);
      rot_.transform(lines[2]);
      rot_.transform(lines[3]);
      rot_.transform(lines[4]);
      rot_.transform(lines[5]);
      beta = -_vecteur.angle(new Vector3d(lines[1].x, lines[1].y, lines[1].z));
      rot_.setZero();
      rot_.rotY(beta);
      rot_.transform(lines[1]);
      rot_.transform(lines[2]);
      rot_.transform(lines[3]);
      rot_.transform(lines[4]);
      rot_.transform(lines[5]);
    } else if (_vecteur.x > 0) {
      lines[1] = new Point3d(_vecteur.length(), 0, 0);
      lines[2] = new Point3d(_vecteur.length() - _chapeau, _chapeau, 0);
      lines[3] = new Point3d(_vecteur.length() - _chapeau, -_chapeau, 0);
      lines[4] = new Point3d(_vecteur.length() - _chapeau, 0, _chapeau);
      lines[5] = new Point3d(_vecteur.length() - _chapeau, 0, -_chapeau);
      alpha = Math.atan(_vecteur.y / _vecteur.x);
      rot_.setZero();
      rot_.rotZ(alpha);
      rot_.transform(lines[1]);
      rot_.transform(lines[2]);
      rot_.transform(lines[3]);
      rot_.transform(lines[4]);
      rot_.transform(lines[5]);
      beta = -_vecteur.angle(new Vector3d(lines[1].x, lines[1].y, lines[1].z));
      rot_.setZero();
      rot_.rotY(beta);
      rot_.transform(lines[1]);
      rot_.transform(lines[2]);
      rot_.transform(lines[3]);
      rot_.transform(lines[4]);
      rot_.transform(lines[5]);
    } else if (_vecteur.z > 0) {
      lines[1] = new Point3d(0, 0, _vecteur.length());
      lines[2] = new Point3d(_chapeau, 0, _vecteur.length() - _chapeau);
      lines[3] = new Point3d(-_chapeau, 0, _vecteur.length() - _chapeau);
      lines[4] = new Point3d(0, _chapeau, _vecteur.length() - _chapeau);
      lines[5] = new Point3d(0, -_chapeau, _vecteur.length() - _chapeau);
      alpha = -Math.atan(_vecteur.y / _vecteur.z);
      rot_.setZero();
      rot_.rotX(alpha);
      rot_.transform(lines[1]);
      rot_.transform(lines[2]);
      rot_.transform(lines[3]);
      rot_.transform(lines[4]);
      rot_.transform(lines[5]);
      beta = new Vector3d(lines[1].x, lines[1].y, lines[1].z).angle(_vecteur);
      rot_.setZero();
      rot_.rotY(beta);
      rot_.transform(lines[1]);
      rot_.transform(lines[2]);
      rot_.transform(lines[3]);
      rot_.transform(lines[4]);
      rot_.transform(lines[5]);
    } else {
      _vecteur.negate();
      lines = fleche(new Point3d(), _vecteur, _chapeau);
      _vecteur.negate();
      for (int i = 0; i < 6; i++) {
        lines[i].negate();
      }
    }
    for (int i = 0; i < 6; i++) {
      lines[i].add(_point);
    }
    return lines;
  }
  
  
  @Override
  public boolean isRapide() {
    return false;
  }
  
  @Override
  public void setRapide(final boolean _rapide) {
   // super.setRapide(_rapide);
  }

  /**
   * Genere le volume � partir d'un ensemble de points dans l'espace.
   * 
   * @param _points les points d'appuis des vecteurs
   * @param _vecteurs les vecteurs � g�n�rer
   */
  public void setGeometrie(final Point3d[] _points, final Vector3d[] _vecteurs) {
    super.setRapide(true);
   
    /*
     * for (int i= 0; i < _points.length; i++) { _points[i].x= _points[i].x / ParametresVolumes.ECHELLE; _points[i].y=
     * _points[i].y / ParametresVolumes.ECHELLE; _points[i].z= _points[i].z / ParametresVolumes.ECHELLE; _vecteurs[i].x=
     * _vecteurs[i].x / ParametresVolumes.ECHELLE; _vecteurs[i].y= _vecteurs[i].y / ParametresVolumes.ECHELLE;
     * _vecteurs[i].z= _vecteurs[i].z / ParametresVolumes.ECHELLE; if (_vecteurs[i].length() < longueur) longueur=
     * _vecteurs[i].length(); }
     */
    final Point3d[] lignes = new Point3d[_points.length * 6];
    Point3d[] lignesTempo;
    bbox_ = new BoundingBox();
    
    
    for (int i = 0; i < _points.length; i++) {
      final double chapeau=Math.max(3,_vecteurs[i].length() / 10);
      lignesTempo = fleche(_points[i], _vecteurs[i], chapeau);
      lignes[6 * i] = lignesTempo[0];
      lignes[6 * i + 1] = lignesTempo[1];
      lignes[6 * i + 2] = lignesTempo[2];
      lignes[6 * i + 3] = lignesTempo[3];
      lignes[6 * i + 4] = lignesTempo[5];
      lignes[6 * i + 5] = lignesTempo[4];
      for (int k = 0; k < 6; k++) {
        bbox_.combine(lignesTempo[k]);
      }
    }
    final Point3d low = new Point3d();
    final Point3d up = new Point3d();
    bbox_.getLower(low);
    bbox_.getUpper(up);
    final int nvecteurs = _points.length;
    final IndexedLineArray linearray = new IndexedLineArray(6 * nvecteurs, GeometryArray.COORDINATES | GeometryArray.COLOR_4,
        10 * nvecteurs);
    linearray.setCoordinates(0, lignes);
    for (int i = 0; i < nvecteurs; i++) {
      linearray.setCoordinateIndex(10 * i, 6 * i);
      linearray.setCoordinateIndex(10 * i + 1, 6 * i + 1);
      linearray.setCoordinateIndex(10 * i + 2, 6 * i + 1);
      linearray.setCoordinateIndex(10 * i + 3, 6 * i + 2);
      linearray.setCoordinateIndex(10 * i + 4, 6 * i + 1);
      linearray.setCoordinateIndex(10 * i + 5, 6 * i + 3);
      linearray.setCoordinateIndex(10 * i + 6, 6 * i + 1);
      linearray.setCoordinateIndex(10 * i + 7, 6 * i + 4);
      linearray.setCoordinateIndex(10 * i + 8, 6 * i + 1);
      linearray.setCoordinateIndex(10 * i + 9, 6 * i + 5);
    }
    final Color4f[] couleurs = new Color4f[3];
    couleurs[0] = new Color4f(1f, 0f, 0f, 1f);
    couleurs[1] = new Color4f(0f, 1f, 0f, 1f);
    couleurs[2] = new Color4f(0f, 0f, 1f, 1f);
    //parcourt les points
    for (int i = 0; i < nvecteurs; i++) {
      for (int j = 0; j < 6; j++) {
        linearray.setColor(i * 6 + j, couleurs[i % 3]);
      }
    }
    
    //parcourt les lignes
    for (int i = 0; i < nvecteurs; i++) {
      for (int j = 0; j < 10; j++) {
        linearray.setColorIndex(10 * i + j, 6 * i);
      }
    }
    linearray.setCapability(GeometryArray.ALLOW_COLOR_READ);
    linearray.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
    linearray.setCapability(GeometryArray.ALLOW_COUNT_READ);
    linearray.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
    shape_.setGeometry(linearray);
    final PolygonAttributes pa = new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_LINE);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final Appearance ap = new Appearance();
    ap.setPolygonAttributes(pa);
    boiteEnglobante_ = new com.sun.j3d.utils.geometry.Box((float) (up.x - low.x) / 2, (float) (up.y - low.y) / 2,
        (float) (up.z - low.z) / 2, ap);
    final TransformGroup btg = new TransformGroup();
    final TransformGroup bTg = new TransformGroup();
    final Transform3D t3d = new Transform3D();
    t3d.set(new Vector3d((low.x + up.x) / 2, (low.y + up.y) / 2, (low.z + up.z) / 2));
    bTg.setTransform(t3d);
    bTg.addChild(boiteEnglobante_);
    switch_.addChild(btg);
  }
}
