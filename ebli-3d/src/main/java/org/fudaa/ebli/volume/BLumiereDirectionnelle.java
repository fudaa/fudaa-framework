/*
 * @creation     1999-12-30
 * @modification $Date: 2006-11-20 08:40:15 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.Color;

import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Bounds;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Light;
import javax.media.j3d.Node;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

import org.fudaa.ebli.volume.common.BControleLumiereTarget;

/**
 * @version $Revision: 1.10 $ $Date: 2006-11-20 08:40:15 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BLumiereDirectionnelle extends BLumiere implements BControleLumiereTarget {
  DirectionalLight l_;
  Vector3f direction_;
  protected Color couleur_;
  protected double intensite_ = 1;

  public BLumiereDirectionnelle(final Vector3f _vecteur, final Color _couleur) {
    couleur_ = _couleur;
    direction_ = _vecteur;
    l_ = new DirectionalLight(new Color3f(_couleur), _vecteur);
    l_.setCapability(Light.ALLOW_STATE_WRITE);
    l_.setCapability(Light.ALLOW_STATE_READ);
    l_.setCapability(Light.ALLOW_COLOR_WRITE);
    l_.setCapability(Light.ALLOW_COLOR_READ);
    l_.setCapability(DirectionalLight.ALLOW_DIRECTION_WRITE);
    l_.setCapability(DirectionalLight.ALLOW_DIRECTION_READ);
    l_.setCapability(Node.ALLOW_BOUNDS_READ);
    l_.setCapability(Node.ALLOW_BOUNDS_WRITE);
    l_.setCapability(Light.ALLOW_INFLUENCING_BOUNDS_READ);
    l_.setCapability(Light.ALLOW_INFLUENCING_BOUNDS_WRITE);
    setCapability(ALLOW_AUTO_COMPUTE_BOUNDS_READ);
    setCapability(ALLOW_AUTO_COMPUTE_BOUNDS_READ);
    l_.setInfluencingBounds(new BoundingSphere(new Point3d(), 100));
    for (int i = 18; i < 20; i++) {
      l_.setCapability(i);
    }
    addChild(l_);
    setName("Direction");
  }

  @Override
  public void setBounds(final Bounds _b) {
    l_.setInfluencingBounds(_b);
  }

  public BoundingSphere getLightBounds() {
    return (BoundingSphere) l_.getInfluencingBounds();
  }

  @Override
  public void setVisible(final boolean _visible) {
    l_.setEnable(_visible);
    visible_ = _visible;
  }

  @Override
  public Vector3f getDirection() {
    return direction_;
  }

  @Override
  public void setDirection(final Vector3f _direction) {
    try {
      direction_ = _direction;
      l_.setDirection(direction_);
    } catch (final Exception ex) {}
  }

  public Color getCouleur() {
    return couleur_;
  }

  public void setCouleur(final Color _couleur) {
    couleur_ = _couleur;
    l_.setColor(new Color3f(couleur_));
  }

  @Override
  public void setIntensite(final double _intensite) {
    intensite_ = _intensite;
    float red, green, blue;
    red = (float) (couleur_.getRed() * intensite_ / 255);
    green = (float) (couleur_.getGreen() * intensite_ / 255);
    blue = (float) (couleur_.getBlue() * intensite_ / 255);
    try {
      l_.setColor(new Color3f(red, green, blue));
    } catch (final Exception ex) {}
  }

  @Override
  public double getIntensite() {
    return intensite_;
  }

}
