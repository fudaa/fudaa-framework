/*
 * @creation     1999-09-27
 * @modification $Date: 2006-10-19 14:13:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

/**
 * @version $Revision: 1.18 $ $Date: 2006-10-19 14:13:25 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BGrilleIrreguliere extends BGrille {
  // private int npoints_;
  // private IndexedTriangleArray geometrie_;
  public BGrilleIrreguliere() {
    super();
    // npoints_= 0;
    // geometrie_= null;
  }

  public BGrilleIrreguliere(final String _s) {
    super(_s);
    // npoints_= 0;
  }

  @Override
  public void detach() {
    switch_.removeAllChildren();
    boiteEnglobante_.removeAllChildren();
    super.detach();

  }

  public void setGeometrie(final IndexedTriangleArray _geometrie) {
    updateGeomCapabilities(_geometrie);
    // geometrie_= _geometrie;
    shape_.setGeometry(_geometrie);
    calculeBBox();
    final Point3d low = new Point3d();
    final Point3d up = new Point3d();
    bbox_.getLower(low);
    bbox_.getUpper(up);
    // Appearance ap=shape_.getAppearance();
    final PolygonAttributes pa = new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_LINE);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final ColoringAttributes ca = new ColoringAttributes();
    ca.setColor(new Color3f(1f, 0f, 0f));
    final Appearance ap = new Appearance();
    ap.setPolygonAttributes(pa);
    ap.setColoringAttributes(ca);
    updateDefaultCapabilities(ap);
   // shape_.setAppearance(ap);
    // ap.getPolygonAttributes().setPolygonMode(PolygonAttributes.POLYGON_LINE);
    boiteEnglobante_ = new com.sun.j3d.utils.geometry.Box((float) (up.x - low.x) / 2, (float) (up.y - low.y) / 2,
        (float) (up.z - low.z) / 2, ap);
    final TransformGroup bTg = new TransformGroup();
    final Transform3D t3d = new Transform3D();
    t3d.set(new Vector3d((low.x + up.x) / 2, (low.y + up.y) / 2, (low.z + up.z) / 2));
    bTg.setTransform(t3d);
    bTg.addChild(boiteEnglobante_);
    switch_.addChild(bTg);
    // centre();
  }

  public static void updateGeomCapabilities(final IndexedTriangleArray _geometrie) {
    _geometrie.setCapability(Geometry.ALLOW_INTERSECT);
    _geometrie.setCapability(GeometryArray.ALLOW_COLOR_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
    _geometrie.setCapability(GeometryArray.ALLOW_COUNT_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
    _geometrie.setCapability(GeometryArray.BY_REFERENCE);
  }

  public void updateGeom(final IndexedGeometryArray _tris) {
    shape_.setGeometry(_tris);
  }

  public IndexedGeometryArray getTriangleArray() {
    return ((IndexedGeometryArray) shape_.getGeometry());
  }

  public void setGeometrie(final int _nbNoeuds, final Point3d[] _noeuds, final int _nbIndices, final int[] _indices) {

    bbox_ = new BoundingBox();
    bbox_.combine(_noeuds);

    final Vector3d[] normales = new Vector3d[_nbIndices / 3];
    final Vector3d v1 = new Vector3d();
    final Vector3d v2 = new Vector3d();
    for (int i = 0; i < _nbIndices; i += 3) {
      normales[i / 3] = new Vector3d();
      v1.sub(_noeuds[_indices[i]], _noeuds[_indices[i + 1]]);
      v2.sub(_noeuds[_indices[i]], _noeuds[_indices[i + 2]]);
      normales[i / 3].cross(v1, v2);
      if (!normales[i / 3].equals(new Vector3d(0, 0, 0))) {
        normales[i / 3].normalize();
      }
    }
    final Vector3f[] normalesp = new Vector3f[_nbNoeuds];
    final int[] nbNormales = new int[_nbNoeuds];
    for (int i = 0; i < _nbNoeuds; i++) {
      normalesp[i] = new Vector3f();
      nbNormales[i] = 0;
    }
    for (int i = 0; i < _nbIndices / 3; i++) {
      for (int j = 0; j < 3; j++) {
        normalesp[_indices[i * 3 + j]].add(new Vector3f(normales[i]));
        nbNormales[_indices[i * 3 + j]]++;
      }
    }
    for (int i = 0; i < _nbNoeuds; i++) {
      normalesp[i].scale((float) 1. / nbNormales[i]);
    }
    final Point3f[] texpoints = new Point3f[_nbNoeuds];
    for (int i = 0; i < _nbNoeuds; i++) {
      texpoints[i] = new Point3f((float) ((_noeuds[i].x + getBoite().o_.x_) / (getBoite().e_.x_ - getBoite().o_.x_)),
          (float) ((_noeuds[i].y + getBoite().o_.y_) / (getBoite().e_.y_ - getBoite().o_.y_)), (float) _noeuds[i].z);
    }
    final IndexedTriangleArray ita = new IndexedTriangleArray(_nbNoeuds, GeometryArray.COORDINATES
        | GeometryArray.COLOR_4 | GeometryArray.NORMALS | GeometryArray.TEXTURE_COORDINATE_2, _nbIndices);
    ita.setCapability(GeometryArray.BY_REFERENCE);
    ita.setCoordinates(0, _noeuds);
    ita.setCoordinateIndices(0, _indices);
    ita.setNormals(0, normalesp);
    // ita.setColors(0,couleurs);
    ita.setColorIndices(0, _indices);
    ita.setNormalIndices(0, _indices);
    ita.setTextureCoordinates(0, texpoints);
    ita.setTextureCoordinateIndices(0, _indices);
    setGeometrie(ita);
  }
  // IndexedTriangleArray next_;
}
