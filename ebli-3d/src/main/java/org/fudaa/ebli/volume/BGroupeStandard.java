/*
 * @creation     1999-2-30
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.util.ArrayList;
import java.util.List;

import com.memoire.fu.FuLog;

import org.fudaa.ebli.volume.common.GroupeInterface;
import org.fudaa.ebli.volume.common.Objet3DInterface;
import org.fudaa.ebli.volume.controles.ZControleEchelle;

/**
 * Groupe d'objets quelconques.
 * 
 * @version $Revision: 1.19 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BGroupeStandard implements GroupeInterface, Objet3DInterface, ZControleEchelle.EchelleClient {
  private final List enfants_;
  private String nom_;
  private boolean visible_;
  private boolean rapide_;
  private int index_;
  private GroupeInterface pere_;

  /**
   * Construit et initialise le groupe.
   */
  public BGroupeStandard() {
    enfants_ = new ArrayList();
    nom_ = "Groupe";
    visible_ = true;
    rapide_ = false;
    index_ = 0;
    pere_ = null;
  }

  @Override
  public boolean isFilaireEditable() {
    return true;
  }

  @Override
  public boolean isRapideEditable() {
    return true;
  }

  @Override
  public void setNouvelleEchelleZ(final float _echelle) {
    final List l = getGroupeVolume().getTousEnfants();
    for (int i = l.size() - 1; i >= 0; i--) {
      final Object o = l.get(i);
      if (o instanceof BVolume) {
        ((BVolume) o).setNouvelleEchelleZ(_echelle);
      }
    }

  }

  public BGroupeVolume getGroupeVolume() {
    final List v = new ArrayList();
    for (int i = 0; i < getNumChildren(); i++) {
      if (enfants_.get(i) instanceof BGroupeVolume) {
        v.add(enfants_.get(i));
      }
    }
    if (v.size() == 0) {
      final BGroupeVolume vol = new BGroupeVolume();
      vol.setPere(this);
      return vol;
    }
    if (v.size() == 1) {
      return (BGroupeVolume) v.get(0);
    }
    final BGroupeVolume vol = new BGroupeVolume();
    vol.setName("Groupe Volume");
    vol.setPere(this);
    for (int i = 0; i < v.size(); i++) {
      vol.add((BVolume) v.get(i));
    }
    return vol;
  }

  public BGroupeLumiere getGroupeLumiere() {
    final List v = new ArrayList(getNumChildren());
    for (int i = 0; i < getNumChildren(); i++) {
      if (enfants_.get(i) instanceof BGroupeLumiere) {
        v.add(enfants_.get(i));
      }
    }
    if (v.size() == 0) {
      final BGroupeLumiere vol = new BGroupeLumiere();
      vol.setPere(this);
      return vol;
    }
    if (v.size() == 1) {
      return (BGroupeLumiere) v.get(0);
    }
    final BGroupeLumiere vol = new BGroupeLumiere();
    vol.setName("Groupe Lumi�re");
    vol.setPere(this);
    for (int i = 0; i < v.size(); i++) {
      vol.add((BLumiere) v.get(i));
    }
    return vol;
  }

  // ****************************************
  // Interface Objet3DInterface
  // name
  /**
   * Nomme l'objet.
   * 
   * @param _nom le nom de l'objet
   */
  @Override
  public void setName(final String _nom) {
    nom_ = _nom;
  }

  /**
   * Nom de l'objet.
   * 
   * @return le nom de l'objet
   */
  @Override
  public String getName() {
    return nom_;
  }

  /**
   * Affiche ou non cet objet.
   * 
   * @param _visible true: l'objet est visible, false: l'objet est invisible.
   */
  @Override
  public void setVisible(final boolean _visible) {
    visible_ = _visible;
    for (int i = 0; i < getNumChildren(); i++) {
      ((Objet3DInterface) enfants_.get(i)).setVisible(_visible);
    }
  }

  /**
   * L'objet est-il visible? return la r�ponse � cette �pineuse question
   */
  @Override
  public boolean isVisible() {
    return visible_;
  }

  /**
   * Affiche ou non l'objet sous forme rapide (boite...).
   * 
   * @param _rapide true: le mode d'affichage rapide est selection�
   */
  @Override
  public void setRapide(final boolean _rapide) {
    rapide_ = _rapide;
    for (int i = 0; i < getNumChildren(); i++) {
      ((Objet3DInterface) enfants_.get(i)).setRapide(_rapide);
    }
  }

  /**
   * Indique si l'objet doit �tre afficher rapidement.
   * 
   * @return vrai si le mode rapide est selectionn� pour cet objet
   */
  @Override
  public boolean isRapide() {
    return rapide_;
  }

  /**
   * Modifie l'indice de l'objet dans son groupe.
   * 
   * @param _index le nouvel indice (entier)
   */
  @Override
  public void setIndex(final int _index) {
    index_ = _index;
  }

  /**
   * Renvoie l'indice de l'objet dans le groupe.
   * 
   * @return index_ l'indice (entier)
   */
  @Override
  public int getIndex() {
    return index_;
  }

  // pere
  /**
   * Modifie le p�re de l'objet (= le groupe auquel l'objet appartient).
   * 
   * @param _pere le groupe p�re
   */
  @Override
  public void setPere(final GroupeInterface _pere) {
    pere_ = _pere;
  }

  /**
   * Renvoie le groupe p�re de l'objet.
   * 
   * @return le groupe p�re
   */
  @Override
  public GroupeInterface getPere() {
    return pere_;
  }

  /**
   * M�thode qui modifie une propri�t� de l'objet.
   * 
   * @param _name : le nom de la propri�t�
   * @param _value sa nouvelle valeur
   */
  @Override
  public boolean setProperty(final String _name, final Object _value) {
    return BVolume.setDefaultProperty(this, _name, _value);
  }
  boolean destructible_;

  /**
   * Indique si l'objet peut �tre supprim� de la sc�ne.
   */
  @Override
  public boolean isDestructible() {
    return destructible_;
  }

  /**
   * Modifie l'enlevabilit� de l'objet.
   * 
   * @param _destructible : vrai si l'objet peut �tre d�truit
   */
  @Override
  public void setDestructible(final boolean _destructible) {
    destructible_ = _destructible;
  }

  /**
   * detruit l'objet (il est retir� de l'arbre).
   */
  @Override
  public void detruire() {
    if (isDestructible()) {
      if (pere_ != null) {
        (pere_).remove(index_);
      }
    }
  }

  // ****************************************
  // Interface GroupeInterface
  /**
   * @return les enfants du groupe (sur un niveau)
   */
  @Override
  public List getEnfants() {
    return enfants_;
  }

  /**
   * @return tous les enfants du groupe (descend tous les niveaux)
   */
  @Override
  public List getTousEnfants() {
    final List v = new ArrayList();
    for (int i = 0; i < getNumChildren(); i++) {
      if (enfants_.get(i) instanceof GroupeInterface) {
        v.addAll(((GroupeInterface) (enfants_.get(i))).getTousEnfants());
      } else {
        v.add((enfants_.get(i)));
      }
    }
    return v;
  }

  /**
   * ajoute un objet au groupe.
   * 
   * @param _objet l'objet � ajouter
   */
  @Override
  public void add(final Objet3DInterface _objet) {
    final int index = getNumChildren();
    enfants_.add(_objet);
    _objet.setIndex(index);
    _objet.setPere(this);
  }

  /**
   * Supprime l'objet du groupe.
   * 
   * @param _objet : l'objet a supprimer
   */
  public void remove(final Objet3DInterface _objet) {
    if (_objet.isDestructible()) {
      if (enfants_.remove(_objet)) {
        for (int i = _objet.getIndex(); i < getNumChildren(); i++) {
          final Objet3DInterface o = (Objet3DInterface) enfants_.get(i);
          o.setIndex(o.getIndex() - 1);
        }
      }
    }
  }

  /**
   * Supprime l'objet du groupe.
   * 
   * @param index:l'index de l'objet
   */
  @Override
  public void remove(final int _index) {
    try {
      enfants_.remove(_index);
    } catch (final ArrayIndexOutOfBoundsException ex) {
      FuLog.error("Index " + _index + " incorrect");
      return;
    }
    for (int i = _index; i < getNumChildren(); i++) {
      final Objet3DInterface o = (Objet3DInterface) enfants_.get(i);
      o.setIndex(i);
    }
  }

  /**
   * @return le nombre d'enfants direct du groupe (sur 1 niveau)
   */
  @Override
  public int getNumChildren() {
    return enfants_.size();
  }
}
