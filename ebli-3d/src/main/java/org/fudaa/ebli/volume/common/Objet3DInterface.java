/*
 * @creation     1999-11-29
 * @modification $Date: 2006-11-14 09:06:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.common;

/**
 * Interface definissant un Objet abstrait.
 *
 * @version $Revision: 1.4 $ $Date: 2006-11-14 09:06:35 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public interface Objet3DInterface {
  /**
   * Nomme l'objet.
   *
   * @param _nom le nom de l'objet
   */
  void setName(String _nom);

  /**
   * Renvoie le nom de l'objet.
   *
   * @return le nom de l'objet
   */
  String getName();

  /**
   * Modifie la visibilite de cet objet.
   *
   * @param _visible indique si l'objet est visible
   */
  void setVisible(boolean _visible);

  /**
   * L'objet est-il visible ?
   *
   * @return la reponse a cette epineuse question
   */
  boolean isVisible();

  /**
   * Modifie le mode d'affichage de l'objet (rapide ou non). En mode rapide, il s'affichera par exemple sous la forme
   * d'une boite.
   *
   * @param _rapide indique si le mode d'affichage rapide est selectione
   */
  void setRapide(boolean _rapide);

  /**
   * Indique si l'objet doit etre afficher rapidement.
   *
   * @return vrai si le mode rapide est selectionne pour cet objet
   */
  boolean isRapide();

  /**
   * Modifie l'indice de l'objet dans son groupe.
   *
   * @param _index le nouvel indice de l'objet
   */
  void setIndex(int _index);

  /**
   * Renvoie l'indice de l'objet dans le groupe.
   *
   * @return l'indice de l'objet
   */
  int getIndex();

  /**
   * Modifie le pere de l'objet. (cad le groupe auquel l'objet appartient).
   *
   * @param _pere le groupe pere
   */
  void setPere(GroupeInterface _pere);

  /**
   * Renvoie le groupe pere de l'objet.
   *
   * @return le groupe pere
   */
  GroupeInterface getPere();

  /**
   * Methode qui modifie une propriete de l'objet.
   *
   * @param _name le nom de la propriete
   * @param _value sa nouvelle valeur
   */
  boolean setProperty(String _name, Object _value);

  /**
   * Indique si l'objet peut etre supprime de la scene.
   *
   * @return vrai si l'objet peut etre detruit
   */
  boolean isDestructible();

  /**
   * Modifie le caractere suppressible de l'objet.
   *
   * @param _destructible indique si l'objet peut etre detruit
   */
  void setDestructible(boolean _destructible);

  /**
   * Detruit l'objet. Il est retire de l'arbre.
   */
  void detruire();

  boolean isFilaireEditable();

  boolean isRapideEditable();
}
