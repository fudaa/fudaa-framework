/*
 * @creation     1999-12-30
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.FlowLayout;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:55:53 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BControleEchelle extends JPanel {
  PropertyChangeSupport pcs_;
  JLabel lbEchelle_;

  public BControleEchelle() {
    pcs_ = new PropertyChangeSupport(this);
    // setPreferredSize(new Dimension(200,250));
    final EchelleListener lis = new EchelleListener();
    final JSlider echelle = new JSlider(SwingConstants.VERTICAL, 1, 500, 5);
    echelle.addChangeListener(lis);
    lbEchelle_ = new JLabel("1");
    setLayout(new FlowLayout());
    add(lbEchelle_);
    add(echelle);
    doLayout();
  }

  @Override
  public void addPropertyChangeListener(final PropertyChangeListener _p) {
    pcs_.addPropertyChangeListener(_p);
  }
  class EchelleListener implements ChangeListener {
    private  int oldVal_ = 1;

    @Override
    public void stateChanged(final ChangeEvent _e) {
      final JSlider source = (JSlider) _e.getSource();
      final int value = source.getValue();
      if (value != oldVal_) {
        pcs_.firePropertyChange("nouvelleEchelleZ", new Float((float) oldVal_ / 5), new Float((float) value / 5));
        lbEchelle_.setText("" + ((float) value / 5));
        oldVal_ = value;
      }
    }
  }
}
