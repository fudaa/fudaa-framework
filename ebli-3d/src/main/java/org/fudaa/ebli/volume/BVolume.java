/*
 * @creation     1999-11-29
 * @modification $Date: 2006-11-14 09:06:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Method;

import javax.media.j3d.BranchGroup;
import javax.media.j3d.Group;
import javax.media.j3d.Node;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Vector3d;

import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.volume.common.BVolumeAbstract;
import org.fudaa.ebli.volume.common.GroupeInterface;

/**
 * @version $Revision: 1.16 $ $Date: 2006-11-14 09:06:28 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public abstract class BVolume extends BVolumeAbstract implements PropertyChangeListener, BSelecteurTargetInterface {

  private boolean destructible_;

  protected PropertyChangeSupport support_;
  // la boite englobante
  protected GrBoite boite_;
  // l'ID de l'objet
  protected int index_;
  protected String nom_;
  // son pere
  protected GroupeInterface pere_;
  protected boolean rapide_;
  // la matrice de transformation
  protected TransformGroup tg_;

  // proprietes :
  protected boolean visible_;

  /**
   * Constructeur de BVolume. Cree toute l'arborescence de l'objet et initialise une apparence par defaut.
   */
  public BVolume() {
    nom_ = null;
    pere_ = null;
    index_ = 0;
    visible_ = false;
    rapide_ = false;
    boite_ = new GrBoite();
    setCapability(BranchGroup.ALLOW_DETACH);
    tg_ = new TransformGroup();
    tg_.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
    tg_.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
    tg_.setCapability(Group.ALLOW_CHILDREN_WRITE);
    tg_.setCapability(Group.ALLOW_CHILDREN_READ);
    tg_.setCapability(Group.ALLOW_CHILDREN_EXTEND);
    tg_.setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);
    addChild(tg_);
  }

  /**
   * Cree un BVolume de nom s.
   * 
   * @param _nom le nom du volume
   */
  public BVolume(final String _nom) {
    this();
    setName(_nom);
  }
  
  

  public void actualise(final long _t) {
  // System.out.println(getName()+" : je m'actualise!");
  }

  @Override
  public void addPropertyChangeListener(final String _s, final PropertyChangeListener _l) {
    if (support_ == null) {
      support_ = new PropertyChangeSupport(this);
    }
    support_.addPropertyChangeListener(_s, _l);

  }

  public void calculeBBox() {}

  /**
   * Detruit l'objet (il est retire de l'arbre).
   */
  @Override
  public void detruire() {
    if (isDestructible() && (pere_ != null)) {
      pere_.remove(index_);
    }
  }

  public GrBoite getBoite(final GrBoite _b) {
    if (boite_ == null || boite_.e_ == null || boite_.o_ == null) {
      return null;
    } else if (_b == null) {
      return new GrBoite(boite_);
    }
    if (_b.e_ == null) {
      _b.e_ = new GrPoint(boite_.e_);
    } else {
      _b.e_.initialiseAvec(boite_.e_);
    }
    if (_b.o_ == null) {
      _b.o_ = new GrPoint(boite_.o_);
    } else {
      _b.o_.initialiseAvec(boite_.o_);
    }
    return _b;
  }

  /**
   * Renvoie l'indice de l'objet dans le groupe.
   * 
   * @return index_ l'indice (entier)
   */
  @Override
  public int getIndex() {
    return index_;
  }

  /**
   * Renvoie le nom du volume.
   * 
   * @return le nom du volume
   */
  @Override
  public String getName() {
    return nom_;
  }

  @Override
  public GroupeInterface getPere() {
    return pere_;
  }

  public TransformGroup getTransformGroup() {
    return tg_;
  }

  /**
   * Indique si l'objet peut etre supprime de la scene.
   * 
   * @return vrai si l'objet peut etre detruit
   */
  @Override
  public boolean isDestructible() {
    return destructible_;
  }

  @Override
  public boolean isRapide() {
    return rapide_;
  }

  /**
   * Le volume est-il visible? return la reponse a cette epineuse question
   */
  @Override
  public boolean isVisible() {
    return visible_;
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _e) {
    final Object old = _e.getOldValue();
    final Object newVal = _e.getNewValue();
    if (!(old.equals(newVal))) {
      setProperty(_e.getPropertyName(), newVal);
    }
  }

  @Override
  public void removePropertyChangeListener(final String _s, final PropertyChangeListener _l) {
    if (support_ != null) {
      support_.removePropertyChangeListener(_s, _l);
    }

  }

  public void setBoite(final GrBoite _boite) {
    boite_ = _boite;
  }

  public void setBoite(final GrPoint _o, final GrPoint _e) {
    boite_.o_ = _o;
    boite_.e_ = _e;
  }

  /**
   * Modifie le caractere suppressible de l'objet.
   * 
   * @param _destructible indique si l'objet peut etre detruit
   */
  @Override
  public void setDestructible(final boolean _destructible) {
    destructible_ = _destructible;
  }

  public void setEchelleX(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    tg_.getTransform(t3d);
    final Transform3D t3d2 = new Transform3D();
    final Vector3d v = new Vector3d(_echelle, 1, 1);
    t3d2.setScale(v);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setEchelleY(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    tg_.getTransform(t3d);
    final Transform3D t3d2 = new Transform3D();
    final Vector3d v = new Vector3d(1, _echelle, 1);
    t3d2.setScale(v);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setEchelleZ(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    tg_.getTransform(t3d);
    final Transform3D t3d2 = new Transform3D();
    final Vector3d v = new Vector3d(1, 1, _echelle);
    t3d2.setScale(v);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  /**
   * Modifie l'indice de l'objet dans le groupe.
   * 
   * @param _index le nouvel indice
   */
  @Override
  public void setIndex(final int _index) {
    index_ = _index;
  }

  /**
   * Nomme le volume.
   * 
   * @param _nom le nom du volume
   */
  @Override
  public final void setName(final String _nom) {
    nom_ = _nom;
  }

  public void setNouvelleEchelleZ(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    tg_.getTransform(t3d);
    final Vector3d v = new Vector3d(1, 1, _echelle);
    t3d.setScale(v);
    tg_.setTransform(t3d);
    calculeBBox();
  }

  @Override
  public void setPere(final GroupeInterface _pere) {
    pere_ = _pere;
  }

  @Override
  public boolean setProperty(final String _name, final Object _value) {
    return setDefaultProperty(this, _name, _value);
  }

  public static boolean setDefaultProperty(final Object _o, final String _name, final Object _value) {
    // System.out.println("On change la propriete "+_name+" de l'objet "+this.toString()+". Nouvelle valeur "+_value);
    boolean res = true;
    try {
      final String n = "set" + _name.substring(0, 1).toUpperCase() + _name.substring(1);
      Class z = _value.getClass();
      // System.err.println("$$$ N="+n);
      try {
        z = (Class) (z.getField("TYPE").get(z));
      } catch (final Exception ey) {
        // System.out.println(ey);
      }
      // System.err.println("$$$ Z="+z);
      Class[] c = null;
      Method m = null;
      do {
        c = new Class[] { z };
        try {
          m = _o.getClass().getMethod(n, c);
        } catch (final Exception ey) {
          // System.out.println(ey); m=null;
        }
        // System.err.println("$$$ M="+m);
        z = z.getSuperclass();
        // System.err.println("$$$ Z="+z);
      } while ((m == null) && (z != null));
      final Object[] o = new Object[] { _value };
      // System.err.println("$$$ O="+o);
      if (m != null) {
        m.invoke(_o, o);
        res = true;
      }
    } catch (final Exception ex) {
      /*
       * if(!_name.equals("ancestor")) System.err.println("Propri�t� "+_name+ " non disponible en �criture dans "+
       * getClass().getName()+".");
       */
    }
    return res;
  }

  public void setRotationX(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    final Transform3D t3d2 = new Transform3D();
    tg_.getTransform(t3d);
    t3d2.rotX(_echelle * Math.PI / 180);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setRotationY(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    final Transform3D t3d2 = new Transform3D();
    tg_.getTransform(t3d);
    t3d2.rotY(_echelle * Math.PI / 180);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setRotationZ(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    final Transform3D t3d2 = new Transform3D();
    tg_.getTransform(t3d);
    t3d2.rotZ(_echelle * Math.PI / 180);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setTransformGroup(final TransformGroup _tg) {
    tg_ = _tg;
  }

  public void setTranslationX(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    final Transform3D t3d2 = new Transform3D();
    tg_.getTransform(t3d);
    final Vector3d v = new Vector3d(_echelle /* / ParametresVolumes.ECHELLE */, 0, 0);
    t3d2.setTranslation(v);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setTranslationY(final float _echelle) {
    final Transform3D t3d2 = new Transform3D();
    final Transform3D t3d = new Transform3D();
    tg_.getTransform(t3d);
    final Vector3d v = new Vector3d(0, _echelle /* / ParametresVolumes.ECHELLE */, 0);
    t3d2.setTranslation(v);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  public void setTranslationZ(final float _echelle) {
    final Transform3D t3d = new Transform3D();
    final Transform3D t3d2 = new Transform3D();
    tg_.getTransform(t3d);
    final Vector3d v = new Vector3d(0, 0, _echelle /* / ParametresVolumes.ECHELLE */);
    t3d2.setTranslation(v);
    t3d2.mul(t3d);
    tg_.setTransform(t3d2);
    calculeBBox();
  }

  /**
   * Affiche ou non ce volume.
   * 
   * @param _valeur true: l'objetest visible, false: l'objet est invisible.
   */
  @Override
  public void setVisible(final boolean _valeur) {
    if (_valeur != visible_) {
      // c'est dans le GroupeVolume que l'on decide du caractere visible d'un objet
      if (pere_ instanceof BGroupeVolume) {
        ((BGroupeVolume) pere_).changeVisible(index_, _valeur);
      }
      visible_ = _valeur;
    }
  }

  public String toString() {
    if (getName() == null) {
      return "Volume";
    }
    return getName();
  }
}
