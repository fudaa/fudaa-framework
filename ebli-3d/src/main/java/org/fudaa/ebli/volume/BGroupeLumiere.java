/*
 * @creation     1999-12-30
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.media.j3d.Group;

import org.fudaa.ebli.volume.common.GroupeInterface;
import org.fudaa.ebli.volume.common.Objet3DInterface;

/**
 * Groupe de lumieres.
 * 
 * @version $Revision: 1.15 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BGroupeLumiere extends BLumiere implements GroupeInterface {
  public BGroupeLumiere() {
    setCapability(Group.ALLOW_CHILDREN_READ);
    setCapability(Group.ALLOW_CHILDREN_WRITE);
    setCapability(Group.ALLOW_CHILDREN_EXTEND);
    setCapability(ALLOW_DETACH);
    setVisible(true);
  }

  /**
   * @return les enfants du groupe (sur un niveau)
   */
  @Override
  public final List getEnfants() {
    return getTousEnfants();
  }

  /**
   * @return le nombre d'enfants direct du groupe (sur 1 niveau)
   */
  @Override
  public int getNumChildren() {
    return getTousEnfants().size();
  }

  /**
   * @return tous les enfants du groupe (descend tous les niveaux)
   */
  @Override
  public final List getTousEnfants() {
    final List v = new ArrayList();
    final Enumeration enfants = getAllChildren();
    for (final Enumeration e = enfants; e.hasMoreElements();) {
      final Object element = e.nextElement();
      if (element instanceof BLumiere) {
        v.add(element);
      } else if (element instanceof BGroupeLumiere) {
        v.addAll(((BGroupeLumiere) element).getTousEnfants());
      }
    }
    return v;
  }

  /**
   * ajoute un objet au groupe.
   * 
   * @param _objet l'objet � ajouter
   */
  @Override
  public void add(final Objet3DInterface _objet) {
    if (_objet instanceof BLumiere) {
      final BLumiere lumiere = (BLumiere) _objet;
      if (!lumiere.isCompiled()) {
        lumiere.compile();
      }
      addChild(lumiere);
      final int index = numChildren() - 1;
      lumiere.setIndex(index);
      lumiere.setPere(this);
    }
  }

  // public void remove(Objet3DInterface _objet) {}
  /**
   * Supprime l'objet du groupe.
   * 
   * @param index:l'index de l'objet
   */
  @Override
  public void remove(final int _index) {
    removeChild(_index);
    final List enfants = getTousEnfants();
    for (int i = _index; i < getNumChildren(); i++) {
      ((Objet3DInterface) enfants.get(i)).setIndex(i);
    }
  }

  /**
   * Affiche ou non les lumieres de ce groupe.
   * 
   * @param _visible true: elles sont allumees, false: elles sont eteintes.
   */
  @Override
  public final void setVisible(final boolean _visible) {
    final List v = getEnfants();
    final int size = v.size();
    for (int i = 0; i < size; i++) {
      ((BLumiere) v.get(i)).setVisible(_visible);
    }
    visible_ = _visible;
  }
}
