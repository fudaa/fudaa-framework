/*
 * @creation     1999-12-30
 * @modification $Date: 2006-11-14 09:06:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import javax.media.j3d.BranchGroup;

import org.fudaa.ebli.volume.common.GroupeInterface;
import org.fudaa.ebli.volume.common.Objet3DInterface;

/**
 * @version $Revision: 1.15 $ $Date: 2006-11-14 09:06:28 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public abstract class BLumiere extends BranchGroup implements Objet3DInterface {
  // l'ID de l'objet
  protected int index_;
  // son pere
  protected GroupeInterface pere_;
  // proprietes :
  protected boolean visible_;
  protected String nom_;

  public BLumiere() {
    nom_ = null;
    pere_ = null;
    index_ = 0;
    visible_ = false;
    // setCapability(BranchGroup.ALLOW_DETACH);
  }

  @Override
  public boolean isFilaireEditable() {
    return false;
  }

  @Override
  public boolean isRapideEditable() {
    return false;
  }

  /**
   * Nomme le volume.
   * 
   * @param _name le nom du volume
   */
  @Override
  public void setName(final String _name) {
    nom_ = _name;
  }

  /**
   * Nom du volume.
   * 
   * @return le nom du volume
   */
  @Override
  public String getName() {
    return nom_;
  }

  /**
   * Renvoie l'indice de l'objet dans le groupe.
   * 
   * @return index_ l'indice (entier)
   */
  @Override
  public int getIndex() {
    return index_;
  }

  /**
   * Mutateur de la propri�t� index.
   * 
   * @param _index le nouvel indice (entier)
   */
  @Override
  public void setIndex(final int _index) {
    index_ = _index;
  }

  /**
   * @return true si volume visible
   */
  @Override
  public boolean isVisible() {
    return visible_;
  }

  /**
   * Indique si l'objet doit �tre afficher rapidement.
   * 
   * @return vrai si le mode rapide est selectionn� pour cet objet
   */
  @Override
  public boolean isRapide() {
    return false;
  }

  /**
   * Affiche ou non l'objet sous forme rapide (boite...).
   * 
   * @param _rapide true: le mode d'affichage rapide est selection�
   */
  @Override
  public void setRapide(final boolean _rapide) {}

  /**
   * Renvoie le groupe p�re de l'objet.
   * 
   * @return le groupe p�re
   */
  @Override
  public GroupeInterface getPere() {
    return pere_;
  }

  /**
   * Modifie le p�re de l'objet (= le groupe auquel l'objet appartient).
   * 
   * @param _pere le groupe p�re
   */
  @Override
  public void setPere(final GroupeInterface _pere) {
    pere_ = _pere;
  }

  /**
   * M�thode qui modifie une propri�t� de l'objet.
   * 
   * @param _name : le nom de la propri�t�
   * @param _value sa nouvelle valeur
   */
  @Override
  public boolean setProperty(final String _name, final Object _value) {
    return BVolume.setDefaultProperty(this, _name, _value);
  }
  boolean destructible_;

  /**
   * Indique si l'objet peut �tre supprim� de la sc�ne.
   */
  @Override
  public boolean isDestructible() {
    return destructible_;
  }

  /**
   * Modifie l'enlevabilit� de l'objet.
   * 
   * @param _destructible : vrai si l'objet peut �tre d�truit
   */
  @Override
  public void setDestructible(final boolean _destructible) {
    destructible_ = _destructible;
  }

  /**
   * detruit l'objet (il est retir� de l'arbre).
   */
  @Override
  public void detruire() {
    if (isDestructible() && (pere_ != null)) {
      pere_.remove(index_);
    }
  }
}
