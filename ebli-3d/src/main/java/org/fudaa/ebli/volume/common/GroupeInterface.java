/*
 * @creation     1999-12-30
 * @modification $Date: 2006-09-19 14:55:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.common;

import java.util.List;

/**
 * Interface définissant un Groupe abstrait.
 *
 * @version $Revision: 1.3 $ $Date: 2006-09-19 14:55:55 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public interface GroupeInterface {
  /**
   * Renvoie les enfants directs du groupe. Seuls les enfants au premier niveau sont renvoyes.
   *
   * @return les enfants direct du groupe
   */
  List getEnfants();

  /**
   * Renvoie tous les enfants (directs et indirects) du groupe. Les niveaux sont parcourus recursivement.
   *
   * @return les enfants directs et indirects du groupe
   */
  List getTousEnfants();

  /**
   * Ajoute un objet au groupe.
   *
   * @param _objet l'objet a ajouter
   */
  void add(Objet3DInterface _objet);

  /**
   * Renvoie le nombre d'enfants directs du groupe. Au premier niveau.
   *
   * @return le nombre d'enfants directs du groupe
   */
  int getNumChildren();

  /**
   * Supprime l'objet du groupe.
   *
   * @param _index l'index de l'objet dans le groupe
   */
  void remove(int _index);
}
