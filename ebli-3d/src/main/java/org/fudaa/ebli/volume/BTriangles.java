/*
 * @creation     2000-02-04
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;
import java.awt.Color;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
/**
 * @version      $Revision: 1.12 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author       Christophe Delhorbe
 */
public class BTriangles extends BGrille {
/*  private int npoints_;
  private TriangleArray geometrie_;*/
  public BTriangles() {
    super();
    /*npoints_= 0;
    geometrie_= null;*/
  }
  public BTriangles(final String _s) {
    super(_s);
/*    npoints_= 0;*/
  }
  public void setGeometrie(final TriangleArray _geometrie) {
    _geometrie.setCapability(Geometry.ALLOW_INTERSECT);
    _geometrie.setCapability(GeometryArray.ALLOW_COLOR_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
    _geometrie.setCapability(GeometryArray.ALLOW_COUNT_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
    _geometrie.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
//    geometrie_= _geometrie;
    shape_.setGeometry(_geometrie);
    calculeBBox();
    final Point3d low= new Point3d();
    final Point3d up= new Point3d();
    bbox_.getLower(low);
    bbox_.getUpper(up);
    //       Appearance ap=shape_.getAppearance();
    final PolygonAttributes pa= new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_LINE);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final ColoringAttributes ca= new ColoringAttributes();
    ca.setColor(new Color3f(1f, 0f, 0f));
    final Appearance ap= new Appearance();
    ap.setPolygonAttributes(pa);
    ap.setColoringAttributes(ca);
    //       ap.getPolygonAttributes().setPolygonMode(PolygonAttributes.POLYGON_LINE);
    boiteEnglobante_=
      new com.sun.j3d.utils.geometry.Box(
        (float) (up.x - low.x) / 2,
        (float) (up.y - low.y) / 2,
        (float) (up.z - low.z) / 2,
        ap);
    final TransformGroup bTg= new TransformGroup();
    final Transform3D t3d= new Transform3D();
    t3d.set(
      new Vector3d((low.x + up.x) / 2, (low.y + up.y) / 2, (low.z + up.z) / 2));
    bTg.setTransform(t3d);
    bTg.addChild(boiteEnglobante_);
    switch_.addChild(bTg);
    //  centre();
  }
  public void setGeometrie(final int _nbNds, final Point3d[] _noeuds) {
    final Color[] c= new Color[_nbNds];
    for (int i= 0; i < _nbNds; i++) {
      c[i]= Color.white;
    }
    setGeometrie(_nbNds, _noeuds, c);
  }
  public void setGeometrie(final int _nbNds, final Point3d[] _noeuds, final Color[] _c) {
    /*for (int i= 0; i < _nbNds; i++) {
      _noeuds[i].x= _noeuds[i].x / ParametresVolumes.ECHELLE;
      _noeuds[i].y= _noeuds[i].y / ParametresVolumes.ECHELLE;
      _noeuds[i].z= _noeuds[i].z / ParametresVolumes.ECHELLE;
    }*/
    bbox_=new BoundingBox();
    bbox_.combine(_noeuds);
    final Point3f[] texpoints= new Point3f[_nbNds];
    for (int i= 0; i < _nbNds; i++) {
      texpoints[i]=
        new Point3f(
          (float) ((_noeuds[i].x + getBoite().o_.x_)
            / (getBoite().e_.x_ - getBoite().o_.x_)),
          (float) ((_noeuds[i].y + getBoite().o_.y_)
            / (getBoite().e_.y_ - getBoite().o_.y_)),
          (float)_noeuds[i].z);
    }
    /*    System.out.println("Generation des normales...");
        GeometryInfo geo=new GeometryInfo(GeometryInfo.TRIANGLE_ARRAY);
        geo.setCoordinates(noeuds);
        new NormalGenerator().generateNormals(geo);
        System.out.println("...OK!");
    */
    final TriangleArray ta=
      new TriangleArray(
        _nbNds,
        GeometryArray.COORDINATES | GeometryArray.COLOR_4      //                                                                |TriangleArray.NORMALS
  | GeometryArray.TEXTURE_COORDINATE_3);
    final Color4f[] couleurs= new Color4f[_nbNds];
    for (int i= 0; i < _nbNds; i++) {
      couleurs[i]=
        new Color4f(
          (float)_c[i].getRed() / 255,
          (float)_c[i].getGreen() / 255,
          (float)_c[i].getBlue() / 255,
          (float)_c[i].getAlpha() / 255);
    }
    ta.setCoordinates(0, _noeuds);
    //    ta.setNormals(0,geo.getNormals());
    ta.setColors(0, couleurs);
    ta.setTextureCoordinates(0, texpoints);
    setGeometrie(ta);
  }
}
