/*
 * @creation 1999-09-27
 * 
 * @modification $Date: 2007-05-04 13:49:46 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.Color;
import java.io.File;
import java.util.Arrays;
import java.util.BitSet;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.TexCoord2f;
import javax.vecmath.Vector3d;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.controle.BSelecteurColorChooser;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.ebli.volume.common.BControleAffichageTarget;
import org.fudaa.ebli.volume.common.BGrilleAbstract;

/**
 * Classe de base pour les grilles ( grilles*, nuage de points, champs de vecteurs).
 * 
 * @version $Revision: 1.22 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BGrille extends BVolume implements BGrilleAbstract, BPalettePlageTarget, BControleAffichageTarget {
  protected BoundingBox bbox_;

  // l'objet en lui meme
  protected Shape3D shape_;
  // le switch pour choisir entre l'affichage de l'objet
  // ou de sa boite englobante
  protected Switch switch_;
  // la boite repr�sentant la boite englobante de l'objet
  protected com.sun.j3d.utils.geometry.Box boiteEnglobante_;

  // private BranchGroup bg_;
  // les hauteurs

  // la palette de couleur;

  @Override
  public String getTitle() {
    return getName();
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == BSelecteurColorChooser.DEFAULT_PROPERTY) {
      return getCouleur();
    }
    return null;
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public boolean setProperty(final String _name, final Object _value) {
    if (_name == BSelecteurColorChooser.DEFAULT_PROPERTY) {
      return setCouleur((Color) _value);
    }
    return super.setProperty(_name, _value);
  }

  protected int neg_ = 1;

  public void setInversionZ(final boolean _negatif) {
    neg_ = (_negatif ? -1 : 1);
  }

  @Override
  public void detach() {
    super.detach();
    bbox_.setLower(null);
    bbox_.setUpper(null);
    shape_.getAppearance().setMaterial(null);
    shape_.setAppearance(null);
    shape_.removeAllGeometries();
    switch_.removeAllChildren();
  }

  /**
   * Cree toute l'arborescence de l'objet, et initialise une apparence par defaut.
   */
  public BGrille() {
    super();
    // Travail d'initialisation :
    // initialiser les capabilities de chaque objet de la branche.
    switch_ = new Switch(Switch.CHILD_MASK);
    switch_.setCapability(Group.ALLOW_CHILDREN_EXTEND);
    switch_.setCapability(Group.ALLOW_CHILDREN_READ);
    switch_.setCapability(Group.ALLOW_CHILDREN_WRITE);
    switch_.setCapability(Switch.ALLOW_SWITCH_WRITE);
    switch_.setCapability(Switch.ALLOW_SWITCH_READ);
    tg_.addChild(switch_);
    shape_ = new Shape3D();
    shape_.setCapability(Shape3D.ALLOW_GEOMETRY_READ);
    shape_.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
    shape_.setCapability(Shape3D.ALLOW_APPEARANCE_READ);
    shape_.setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
    shape_.setCapability(Shape3D.ALLOW_APPEARANCE_OVERRIDE_READ);
    shape_.setCapability(Shape3D.ALLOW_APPEARANCE_OVERRIDE_WRITE);
    switch_.addChild(shape_);
    final ColoringAttributes ca = new ColoringAttributes();
    ca.setCapability(ColoringAttributes.ALLOW_COLOR_READ);
    ca.setCapability(ColoringAttributes.ALLOW_COLOR_WRITE);
    ca.setCapability(ColoringAttributes.ALLOW_SHADE_MODEL_READ);
    ca.setCapability(ColoringAttributes.ALLOW_SHADE_MODEL_WRITE);
    ca.setColor((float) 1., (float) 1., (float) 1.);
    ca.setShadeModel(ColoringAttributes.NICEST);
    final TransparencyAttributes ta = new TransparencyAttributes();
    ta.setCapability(TransparencyAttributes.ALLOW_VALUE_READ);
    ta.setCapability(TransparencyAttributes.ALLOW_VALUE_WRITE);
    ta.setTransparencyMode(TransparencyAttributes.NICEST);
    ta.setDstBlendFunction(TransparencyAttributes.BLEND_ZERO);
    final PointAttributes pointa = new PointAttributes();
    pointa.setPointSize(pointa.getPointSize() * 2);
    pointa.setPointAntialiasingEnable(true);
    final LineAttributes la = new LineAttributes();
    la.setLineAntialiasingEnable(true);
    final PolygonAttributes pa = new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_FILL);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final Material m = new Material();
    m.setCapability(Material.ALLOW_COMPONENT_READ);
    m.setCapability(Material.ALLOW_COMPONENT_WRITE);
    m.setShininess(2);
    m.setLightingEnable(false);
    final TextureAttributes texa = new TextureAttributes();
    texa.setCapability(TextureAttributes.ALLOW_MODE_WRITE);
    texa.setTextureMode(TextureAttributes.REPLACE);
    texa.setPerspectiveCorrectionMode(TextureAttributes.NICEST);
    final Appearance appear = new Appearance();
    appear.setColoringAttributes(ca);
    appear.setTransparencyAttributes(ta);
    appear.setPointAttributes(pointa);
    appear.setLineAttributes(la);
    appear.setPolygonAttributes(pa);
    appear.setMaterial(m);
    appear.setTextureAttributes(texa);
    updateDefaultCapabilities(appear);

    shape_.setAppearance(appear);
    shape_.setAppearanceOverrideEnable(true);
  }

  public static void updateDefaultCapabilities(final Appearance _appear) {
    // rendering
    _appear.setCapability(Appearance.ALLOW_RENDERING_ATTRIBUTES_READ);
    _appear.setCapability(Appearance.ALLOW_RENDERING_ATTRIBUTES_WRITE);
    // coloring
    _appear.setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_READ);
    _appear.setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_WRITE);
    // TRANSPARENCY
    _appear.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
    _appear.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
    // TEXTURE_ATTRIBUTES
    _appear.setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_READ);
    _appear.setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_WRITE);
    // TEXTURE_UNIT
    _appear.setCapability(Appearance.ALLOW_TEXTURE_UNIT_STATE_READ);
    _appear.setCapability(Appearance.ALLOW_TEXTURE_UNIT_STATE_WRITE);
    // POLYGON_ATTRIBUTES
    _appear.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_READ);
    _appear.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
    // MATERIAL_READ
    _appear.setCapability(Appearance.ALLOW_MATERIAL_READ);
    _appear.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
    // TEXTURE_READ
    _appear.setCapability(Appearance.ALLOW_TEXTURE_READ);
    _appear.setCapability(Appearance.ALLOW_TEXTURE_WRITE);
    _appear.setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_READ);
    _appear.setCapability(Appearance.ALLOW_TEXTURE_ATTRIBUTES_WRITE);

    final TextureAttributes att = _appear.getTextureAttributes();
    if (att != null) {
      att.setCapability(TextureAttributes.ALLOW_MODE_READ);
      att.setCapability(TextureAttributes.ALLOW_MODE_WRITE);
    }
  }

  /**
   * Cree un BVolume de nom s.
   * 
   * @param s le nom du volume
   */
  public BGrille(final String _s) {
    this();
    setName(_s);
  }

  /**
   * affichage plein ou seulement boite englobante.
   * 
   * @param _rapide : vrai->boite;faux->plein
   */
  @Override
  public void setRapide(final boolean _rapide) {
    rapide_ = _rapide;
    if (isRapide()) {
      final BitSet bs = new BitSet();
      bs.clear(0);
      bs.set(1);
      switch_.setChildMask(bs);
    } else {
      final BitSet bs = new BitSet();
      bs.clear(1);
      bs.set(0);
      switch_.setChildMask(bs);
    }
  }

  File textureImgUrl_;

  @Override
  public Texture getTexture() {
    return shape_.getAppearance().getTexture();
  }

  protected void updateTextureCapabilities(final Texture _t) {
    _t.setCapability(Texture.ALLOW_ENABLE_READ);
    _t.setCapability(Texture.ALLOW_ENABLE_WRITE);
    _t.setCapability(Texture.ALLOW_FORMAT_READ);
    _t.setCapability(Texture.ALLOW_IMAGE_WRITE);
    _t.setCapability(Texture.ALLOW_IMAGE_READ);
  }

  @Override
  public boolean isTextureEnable() {
    try {
      if (shape_.getAppearance().getTexture() != null) {
        return shape_.getAppearance().getTexture().getEnable();
      }
    } catch (final RuntimeException _evt) {
      FuLog.error(_evt);
    }
    return false;
  }

  /**
   * applique la texture t sur l'objet.
   */
  @Override
  public void setTexture(final Texture _t) {
    final Appearance appearance = shape_.getAppearance();
    if (_t != null) {
      updateTextureCapabilities(_t);
    }
    appearance.setTexture(_t);
  }

  /**
   * affiche ou non le texture.
   */
  @Override
  public void setTextureEnable(final boolean _activee) {
    try {
      shape_.getAppearance().getTexture().setEnable(_activee);
    } catch (final Exception e) {
    }
  }

  /**
   * mode de la texture (Modulate/Decal/Replace/Blend).
   */
  @Override
  public void setTextureAttributesMode(final int _mode) {
    shape_.getAppearance().getTextureAttributes().setTextureMode(_mode);
  }

  /**
   * renvoie la couleur de l'objet.
   */
  @Override
  public Color getCouleur() {
    final Color3f couleur = new Color3f();
    shape_.getAppearance().getColoringAttributes().getColor(couleur);
    return couleur.get();
  }

  public RenderingAttributes getRenderingAttributes() {
    return shape_.getAppearance().getRenderingAttributes();
  }

  public void setRenderingAttributes(final RenderingAttributes _att) {
    shape_.getAppearance().setRenderingAttributes(_att);
  }

  public void setTextures(final int _texCoordSet, final TexCoord2f[] _tex) {
    ((GeometryArray) shape_.getGeometry()).setTextureCoordinates(_texCoordSet, 0, _tex);
  }

  public GeometryArray getGeomArray() {
    return ((GeometryArray) shape_.getGeometry());
  }

  /**
   * colore l'objet d'une couleur unique.
   */
  @Override
  public boolean setCouleur(final Color _c) {
    if (shape_ == null || shape_.getGeometry() == null) {
      return false;
    }
    isColorUsed_ = true;
    final int nbPts = ((GeometryArray) shape_.getGeometry()).getVertexCount();
    shape_.getAppearance().getColoringAttributes().setColor(new Color3f(_c));
    final Color4f[] cs = new Color4f[nbPts];
    Arrays.fill(cs, new Color4f(_c));
    ((GeometryArray) shape_.getGeometry()).setColors(0, cs);
    return true;
  }

  public void setForeground(final Color _c) {
    setCouleur(_c);

  }

  /**
   * colore l'objet suivant l'altitude de chaque sommet.
   */
  public void setCouleurs(final Color[] _c) {
    final Color4f[] c4f = new Color4f[_c.length];
    final Color4f trans = new Color4f(1, 1, 1, 1);
    for (int i = 0; i < _c.length; i++) {
      c4f[i] = _c[i] == null ? trans : new Color4f(_c[i]);
    }
    setCouleurs(c4f);

  }

  protected void setCouleurs(final Color4f[] _c) {
    ((GeometryArray) shape_.getGeometry()).setColors(0, _c);
  }

  public Color4f getColorFor(final int _idxVertex) {
    final Color4f r = new Color4f();
    ((GeometryArray) shape_.getGeometry()).getColor(_idxVertex, r);
    return r;
  }

  /**
   * mode ombr� ou pas.
   */
  @Override
  public void setEclairage(final boolean _valeur) {
    shape_.getAppearance().getMaterial().setLightingEnable(_valeur);
  }

  @Override
  public boolean isEclairage() {
    return shape_.getAppearance().getMaterial().getLightingEnable();
  }

  /**
   * recupere la transparence de l'objet.
   */
  public float getTransparence() {
    return shape_.getAppearance().getTransparencyAttributes().getTransparency();
  }

  /**
   * modifie la transparence de l'objet.
   */
  public void setTransparence(final float _t) {
    shape_.getAppearance().getTransparencyAttributes().setTransparency(_t);
  }

  /**
   * modifie la brillance de l'objet.
   */
  public void setBrillance(final double _b) {
    shape_.getAppearance().getMaterial().setShininess((float) _b);
  }

  public Appearance getAppearence() {
    return shape_.getAppearance();
  }

  /**
   * recupere la brillance de l'objet.
   */
  public float getBrillance() {
    return shape_.getAppearance().getMaterial().getShininess();
  }

  public boolean isCouleurModifiable() {
    return true;
  }

  @Override
  public BPalettePlageInterface createPaletteCouleur() {
    return new BPalettePlage();
  }

  protected boolean isColorUsed_ = true;

  @Override
  public String getDataDescription() {
    return getName();
  }

  protected BPalettePlage palette_;

  @Override
  public BPalettePlageInterface getPaletteCouleur() {
    if (palette_ == null) {
      palette_ = (BPalettePlage) createPaletteCouleur();
    }
    return palette_;
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    final GeometryArray geometrie = (GeometryArray) shape_.getGeometry();
    final int nbPt = geometrie.getVertexCount();
    _b.setToNill();
    final double[] vs = new double[3];
    for (int i = 0; i < nbPt; i++) {
      geometrie.getCoordinate(i, vs);
      _b.expandTo(vs[2]);
    }
    return true;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return true;
  }

  @Override
  public boolean isDiscrete() {
    return false;
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }

  @Override
  public boolean isPaletteModifiable() {
    return true;
  }

  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    isColorUsed_ = false;
    palette_ = new BPalettePlage(_newPlage.getPlages());
    final GeometryArray geometrie = (GeometryArray) shape_.getGeometry();
    final int nbPt = geometrie.getVertexCount();
    final Color4f[] cs = new Color4f[nbPt];
    final double[] vs = new double[3];
    for (int i = 0; i < nbPt; i++) {
      geometrie.getCoordinate(i, vs);
      Color c = palette_.getColorFor(vs[2]);
      if (c == null) {
        c = new Color(1, 1, 1, 1);
      }
      cs[i] = new Color4f(c);
    }
    setCouleurs(cs);
    if (support_ != null) {
      support_.firePropertyChange("paletteCouleur", null, palette_);
    }

  }

  /**
   * affiche l'objet en fil de fer.
   */
  @Override
  public void setFilaire(final boolean _filaire) {
    if (_filaire) {
      shape_.getAppearance().getPolygonAttributes().setPolygonMode(PolygonAttributes.POLYGON_LINE);
    } else {
      shape_.getAppearance().getPolygonAttributes().setPolygonMode(PolygonAttributes.POLYGON_FILL);
    }
  }

  /**
   * l'objet est affich� en fil de fer?
   */
  @Override
  public boolean isFilaire() {
    return (shape_.getAppearance().getPolygonAttributes().getPolygonMode() == PolygonAttributes.POLYGON_LINE);
  }

  /**
   * recupere la boite englobante.
   */
  public GrBoite getBoite() {
    final Point3d point = new Point3d();
    final Transform3D t3d = new Transform3D();
    tg_.getTransform(t3d);
    final Vector3d echelle = new Vector3d();
    t3d.getScale(echelle);
    bbox_.getLower(point);
    final GrPoint o = new GrPoint(point.x * echelle.x, point.y * echelle.y, point.z * echelle.z);
    bbox_.getUpper(point);
    final GrPoint e = new GrPoint(point.x * echelle.x, point.y * echelle.y, point.z * echelle.z);
    final GrBoite boite = new GrBoite();
    boite.ajuste(e);
    boite.ajuste(o);
    return boite;
  }

  /**
   * modifie la boite englobante.
   */
  @Override
  public void setBoite(final GrPoint _o, final GrPoint _e) {
    bbox_ = new BoundingBox();
    bbox_.setLower(new Point3d(_o.x_, _o.y_, _o.z_));
    bbox_.setUpper(new Point3d(_e.x_, _e.y_, _e.z_));
  }

  /**
   * calcule la boitze englobante � partir de la geometrie de l'objet.
   */
  @Override
  public void calculeBBox() {
    bbox_ = new BoundingBox();
    final GeometryArray geometrie = (GeometryArray) shape_.getGeometry();
    calculeBBox(geometrie);
  }

  protected void calculeBBox(final GeometryArray _geometrie) {
    if (_geometrie == null) {
      return;
    }
    if (bbox_ == null) {
      bbox_ = new BoundingBox();
    }
    final int compte = _geometrie.getVertexCount();
    final Point3d pts = new Point3d();
    for (int i = 0; i < compte; i++) {
      _geometrie.getCoordinate(i, pts);
      bbox_.combine(pts);
    }
  }

  /**
   * centre l'objet.
   */
  public void centre() {
    final double tx = (getBoite().o_.x_ + getBoite().e_.x_) / 2;
    final double ty = (getBoite().o_.y_ + getBoite().e_.y_) / 2;
    final double tz = (getBoite().o_.z_ + getBoite().e_.z_) / 2;
    GrMorphisme centrage = GrMorphisme.translation(-tx, -ty, -tz);
    autoApplique(centrage);
    final GrMorphisme rotation = GrMorphisme.rotationX(-Math.PI / 2);
    autoApplique(rotation);
    centrage = GrMorphisme.translation(tx, ty, tz);
    autoApplique(centrage);
  }

  /**
   * recupere la matrice transformation de l'objet.
   */
  public double[][] getMatrice() {
    final Matrix4d m = new Matrix4d();
    final Transform3D t = new Transform3D();
    tg_.getTransform(t);
    t.get(m);
    final double[][] d = new double[4][4];
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        d[i][j] = m.getElement(j, i);
      }
    }
    return d;
  }

  /**
   * applique une transformation a l'objet.
   */
  public void autoApplique(final GrMorphisme _t) {
    // boite_.autoApplique(_t);
    GrMorphisme matrice = GrMorphisme.identite();
    matrice.initFrom(getMatrice());
    matrice = matrice.composition(_t);
    final double[] m = new double[16];
    final double[] mb = new double[16];
    for (int j = 0; j < 4; j++) {
      for (int i = 0; i < 4; i++) {
        m[(j * 4) + i] = matrice.a_[i][j];
        mb[(j * 4) + i] = _t.a_[i][j];
      }
    }
    final Transform3D t = new Transform3D(m);
    final Transform3D tb = new Transform3D(mb);
    tg_.setTransform(t);
    bbox_.transform(tb);
  }

  @Override
  public File getTextureImgUrl() {
    return textureImgUrl_;
  }

  @Override
  public void setTextureImgUrl(final File _textureImgUrl) {
    textureImgUrl_ = _textureImgUrl;
  }

  @Override
  public int getTextureAttributesMode() {
    return shape_.getAppearance().getTextureAttributes().getTextureMode();
  }

  @Override
  public boolean isColorUsed() {
    return isColorUsed_;
  }

}
