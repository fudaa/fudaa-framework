/*
 * @creation     1999-10-12
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.memoire.bu.BuComboBox;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Classe de selection du pas de translation.
 * 
 * @version $Revision: 1.13 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BPas implements ItemListener {
  private final BControleVolume cv_;

  /**
   * Cree le panel contenant 4 radios de selection de pas.
   * 
   * @param _cv le bean de controle de volume
   */

  private final float[] coef_;

  public String getTitre() {
    return EbliLib.getS("Vitesses");
  }

  private  final BuComboBox cb_;

  public BPas(final BControleVolume _cv) {
    cv_ = _cv;
    // par defaut : coef de 1
    cv_.setCoef(1f);

    final int nbVal = 5;
    final String[] values = new String[nbVal];
    coef_ = new float[nbVal];
    values[0] = EbliLib.getS("Tr�s rapide");
    coef_[0] = 50f;
    values[1] = EbliLib.getS("Rapide");
    coef_[1] = 3 * coef_[0];
    values[2] = EbliLib.getS("Normal");
    coef_[2] = 3 * coef_[1];
    values[3] = EbliLib.getS("Lent");
    coef_[3] = 3 * coef_[2];
    values[4] = EbliLib.getS("Tr�s lent");
    coef_[4] = 3 * coef_[3];
    cb_ = new BuComboBox(values);
    cb_.setToolTipText(EbliLib.getS("Choisir la vitesse de d�placement"));
    cb_.setFocusable(false);
    cb_.addItemListener(this);
    cb_.setSelectedIndex(1);

  } // listener du changement du radio selectionn�

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.DESELECTED) {
      return;
    }
    final BuComboBox cb = (BuComboBox) _e.getSource();
    final float coef = coef_[cb.getSelectedIndex()];
    final GrBoite b = cv_.getU().getBoite();
    float x = coef;
    float y = x;
    float z = x;
    if (b != null) {
      final double xy = Math.max(b.getDeltaX(), b.getDeltaY());
      x = (float) (xy / coef);
      y = x;
      z = x;
    }
    cv_.setCoef(x, y, z);

  }

  public BuComboBox getCb() {
    return cb_;
  }

}
