/*
 * @creation     1999-12-30
 * @modification $Date: 2006-11-14 09:06:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.Canvas;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;

import org.fudaa.ebli.volume.common.GroupeInterface;
import org.fudaa.ebli.volume.common.Objet3DInterface;

/**
 * @version $Revision: 1.13 $ $Date: 2006-11-14 09:06:29 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public abstract class BObjet2D extends Canvas implements PropertyChangeListener, Objet3DInterface {
  private int index_;
  private String nom_;
  private GroupeInterface pere_;
  private boolean rapide_;

  /**
   * Nomme le volume.
   * 
   * @param _nom le nouveau nom du volume
   */
  @Override
  public void setName(final String _nom) {
    nom_ = _nom;
  }

  @Override
  public boolean isFilaireEditable() {
    return true;
  }

  @Override
  public boolean isRapideEditable() {
    return true;
  }

  /**
   * Renvoie le nom du volume.
   * 
   * @return le nom du volume
   */
  @Override
  public String getName() {
    return nom_;
  }

  /**
   * Renvoie l'indice de l'objet dans le groupe.
   * 
   * @return l'indice
   */
  @Override
  public int getIndex() {
    return index_;
  }

  /**
   * Modifie l'indice de l'objet dans le groupe.
   * 
   * @param _index le nouvel indice
   */
  @Override
  public void setIndex(final int _index) {
    index_ = _index;
  }

  @Override
  public boolean isRapide() {
    return rapide_;
  }

  @Override
  public void setRapide(final boolean _rapide) {
    rapide_ = _rapide;
  }

  @Override
  public GroupeInterface getPere() {
    return pere_;
  }

  @Override
  public void setPere(final GroupeInterface _pere) {
    pere_ = _pere;
  }

  @Override
  public boolean setProperty(final String _name, final Object _value) {
    boolean res = false;
    try {
      final String n = "set" + _name.substring(0, 1).toUpperCase() + _name.substring(1);
      Class z = _value.getClass();
      try {
        z = (Class) (z.getField("TYPE").get(z));
      } catch (final Exception ey) {}
      Class[] c = null;
      Method m = null;
      do {
        c = new Class[] { z };
        try {
          m = getClass().getMethod(n, c);
        } catch (final Exception ey) {}
        z = z.getSuperclass();
      } while ((m == null) && (z != null));
      final Object[] o = new Object[] { _value };
      if (m != null) {
        m.invoke(this, o);
        res = true;
      }
    } catch (final Exception ex) {}
    return res;
  }
  private boolean destructible_;

  /**
   * Indique si l'objet peut etre supprime de la scene.
   * 
   * @return vrai si l'objet peut etre detruit
   */
  @Override
  public boolean isDestructible() {
    return destructible_;
  }

  /**
   * Modifie le caractere suppressible de l'objet.
   * 
   * @param _destructible indique si l'objet peut etre detruit
   */
  @Override
  public void setDestructible(final boolean _destructible) {
    destructible_ = _destructible;
  }

  /**
   * Detruit l'objet. Il est retire de l'arbre.
   */
  @Override
  public void detruire() {
    if (isDestructible() && (pere_ != null)) {
      pere_.remove(index_);
    }
  }
}
