/*
 * @creation 18 mai 2006
 * @modification $Date: 2006-11-14 09:06:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.common;

import java.awt.Color;

import org.fudaa.ebli.controle.BSelecteurTargetInterface;

/**
 * @author fred deniger
 * @version $Id: BControleAffichageTarget.java,v 1.3 2006-11-14 09:06:35 deniger Exp $
 */
public interface BControleAffichageTarget extends BTextureTargetInterface, BSelecteurTargetInterface {

  boolean isColorUsed();

  Color getCouleur();

  boolean setCouleur(Color _c);

  boolean isEclairage();

  void setEclairage(boolean _b);

  void setFilaire(boolean _b);

  boolean isFilaire();

  boolean isFilaireEditable();

  boolean isRapideEditable();

  void setVisible(boolean _b);

  boolean isVisible();

  void setRapide(boolean _b);

  boolean isRapide();

}
