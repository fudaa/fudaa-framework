/*
 *  @creation     5 mai 2004
 *  @modification $Date: 2006-06-12 09:55:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.common;

import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;

import org.fudaa.ebli.geometrie.GrBoite;

/**
 * @author Fred Deniger
 * @version $Id: BUniversInterface.java,v 1.3 2006-06-12 09:55:22 deniger Exp $
 */
public interface BUniversInterface {

  /**
   * @return la position et l'orientation de la camera
   */
  Transform3D[] getUniversTransforms();

  /**
   * @param _t la nouveau trasnfo pour la camera
   */
  void setUniversTransform(Transform3D _t);

  /**
   * @return le TransformGroup, pour pouvoir controler la camera
   */
  TransformGroup getTransformGroup();


  /**
   * Ajoute une Branche � l'arbre de la scene, apres l'avoir compil�e.
   *
   * @param bg BranchGroup � ajouter.
   */
  void addBranchGraph(BranchGroup _bg);

  /**
   * Retire une Branche � l'arbre de la scene.
   *
   * @param bg BranchGroup � supprimer
   */
  void removeBranchGraph(BranchGroup _bg);

  /**
   * Initialise la camera : calcul de la boite englobante generale et positionement de la camera en consequence.
   */
  void init();

  GrBoite getBoite();


  void  scale(double _scale);

  /**
   * Change l'affichage de la sc�ne : choix entre rapidit� et apparence.
   *
   * @param _rapide : mode rapide? ( =affichage d�t�rior�)
   */
  void setRapide(boolean _rapide);

  /**
   * Tourne les objets sur un des axes d'origine.
   *
   * @param _axe axe de rotation (X,Y ou Z)
   * @param _valeur angle, en radians
   */
  void orbital(int _axe, double _valeur);

  /**
   * Translate la camera sur un axe.
   *
   * @param _axe axe de translation (X,Y ou Z)
   * @param _valeur longueur de la translation en metres
   */
  void translation(int _axe, double _valeur);

  /**
   * Tourne la camera sur un axe.
   *
   * @param _axe axe de rotation (X,Y ou Z)
   * @param _valeur angle, en radians
   */
  void rotation(int _axe, double _valeur);
}