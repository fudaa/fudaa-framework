/*
 * @creation     1999-12-30
 * @modification $Date: 2007-04-16 16:35:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Vector3f;

import com.memoire.bu.BuVerticalLayout;

import org.fudaa.ctulu.CtuluLib;

/**
 * @version $Revision: 1.12 $ $Date: 2007-04-16 16:35:08 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BControleLumiere extends JPanel {
  // private int mode;
  private final static int XY = 0;
  private final static int YZ = 1;
  final String[] modes_ = { "XY", "YZ" };
  final Dimension taille_ = new Dimension(64, 64);
  final int centreX_ = taille_.width / 2;
  final int centreY_ = taille_.height / 2;
  Vector3f vecteur_;
  PropertyChangeSupport pcs_;
  final private Carre yz_;
  final private Carre xy_;
  JLabel lbIntensite_;

  public BControleLumiere() {
    pcs_ = new PropertyChangeSupport(this);
    this.setPreferredSize(new Dimension(200, 130));
    /* mode= XY; */
    vecteur_ = new Vector3f();
    final Listener lis = new Listener();
    xy_ = new Carre(XY);
    xy_.addMouseListener(lis);
    yz_ = new Carre(YZ);
    yz_.addMouseListener(lis);
    final JPanel carres = new JPanel(new FlowLayout());
    carres.add(xy_);
    carres.add(yz_);
    final JPanel intensite = new JPanel(new BuVerticalLayout());
    final JLabel lbIntensite = new JLabel("Intensité");
    final JSlider slIntensite = new JSlider(0, 25, 25);
    slIntensite.addChangeListener(new IntensiteListener());
    lbIntensite_ = new JLabel("1.00");
    intensite.add(lbIntensite);
    intensite.add(slIntensite);
    intensite.add(lbIntensite_);
    setLayout(new BuVerticalLayout());
    add(carres);
    add(intensite);
    doLayout();
    // setSize(getPreferredSize());
  }

  @Override
  public void addPropertyChangeListener(final PropertyChangeListener _p) {
    pcs_.addPropertyChangeListener(_p);
  }
  class Carre extends JComponent {
    final int carreMode_;

    public Carre(final int _mode) {
      super();
      carreMode_ = _mode;
      this.setPreferredSize(taille_);
    }

    @Override
    public void paintComponent(final Graphics _g) {
      // setSize(taille);
      _g.setColor(Color.white);
      _g.fillRect(0, 0, taille_.width, taille_.height);
      int x = 0, y = 0;
      switch (carreMode_) {
      case 0:
        x = (int) vecteur_.x + centreX_;
        y = (int) vecteur_.z + centreY_;
        break;
      case 1:
        x = (int) vecteur_.y + centreX_;
        y = (int) vecteur_.z + centreY_;
        break;
      case 2:
        x = (int) vecteur_.z + centreX_;
        y = (int) vecteur_.x + centreY_;
        break;
      default:
      }
      _g.setColor(Color.black);
      _g.drawLine(0, centreY_, taille_.width, centreY_);
      _g.drawLine(taille_.width, centreY_, taille_.width - 5, centreY_ - 5);
      _g.drawLine(taille_.width, centreY_, taille_.width - 5, centreY_ + 5);
      _g.drawString(modes_[carreMode_].substring(0, 1), taille_.width - 10, centreY_ - 10);
      _g.drawLine(centreX_, 0, centreX_, taille_.height);
      _g.drawLine(centreX_, 0, centreX_ - 5, 5);
      _g.drawLine(centreX_, 0, centreX_ + 5, 5);
      _g.drawString(modes_[carreMode_].substring(1, 2), centreX_ + 10, 10);
      _g.setColor(Color.blue);
      _g.drawLine(centreX_, centreY_, x, y);
      _g.setColor(Color.red);
      _g.fillOval(x - 2, y - 2, 4, 4);
    }

    public int getMode() {
      return carreMode_;
    }
  }
  class Listener implements MouseListener {
    @Override
    public void mousePressed(final MouseEvent _evt) {}

    @Override
    public void mouseReleased(final MouseEvent _evt) {}

    @Override
    public void mouseClicked(final MouseEvent _evt) {
      if (SwingUtilities.isLeftMouseButton(_evt)) {
        final Vector3f old = new Vector3f(vecteur_);
        final int x = _evt.getX();
        final int y = _evt.getY();
        switch (((Carre) _evt.getSource()).getMode()) {
        case 0:
          vecteur_.x = x - centreX_;
          vecteur_.z = y - centreY_;
          break;
        case 1:
          vecteur_.y = x - centreX_;
          vecteur_.z = y - centreY_;
          break;
        case 2:
          vecteur_.z = x - centreX_;
          vecteur_.x = y - centreY_;
          break;
        default:
          // TODO ajouter des instructions au cas "default"
        }
        repaint();
        if (!old.equals(vecteur_)) {
          final Vector3f v = new Vector3f(-vecteur_.x, vecteur_.y, vecteur_.z);
          v.normalize();
          pcs_.firePropertyChange("direction", old, v);
        }
      }
    }

    @Override
    public void mouseEntered(final MouseEvent _evt) {}

    @Override
    public void mouseExited(final MouseEvent _evt) {}
  }
  class IntensiteListener implements ChangeListener {
    int oldValue_ = 25;

    @Override
    public void stateChanged(final ChangeEvent _evt) {
      final JSlider source = (JSlider) _evt.getSource();
      final int value = source.getValue();
      if (value != oldValue_) {
        pcs_.firePropertyChange("intensite", CtuluLib.getDouble((double) oldValue_ / 25), CtuluLib
            .getDouble((double) value / 25));
        lbIntensite_.setText("" + ((double) value / 25));
        oldValue_ = value;
      }
    }
  }
}
