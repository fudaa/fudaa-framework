/*
 * @creation     2000-02-04
 * @modification $Date: 2007-06-05 08:58:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

import javax.media.j3d.Transform3D;
import javax.swing.*;

import com.memoire.bu.*;
import com.sun.j3d.utils.universe.SimpleUniverse;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.ebli.palette.BPaletteSelecteurReduitCouleur;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.controles.*;

/**
 * @version $Id: EbliFilleVue3D.java,v 1.19 2007-06-05 08:58:39 deniger Exp $
 * @author Christophe Delhorbe
 */
public class EbliFilleVue3D extends EbliFilleImprimable implements WindowListener {
  BUnivers u_;
  private Color backgroundUnivers_;
  private BArbreVolume av_;
  private BuMenu mnVolumes_;
  private BuPopupButton pbPalette_;
  private BuPopupButton pbTexture_;
  private BuPopupButton pbLumiere_;
  private BuPopupButton pbEchelle_;
  private BuPopupButton pbTransfo_;
  protected MyFrame frame_;
  private Transform3D[] t_;
  private BGroupeStandard gs_;
  private boolean hide_;
  private BuInformationsDocument idAppli_;
  BCartouche cart_;
  private BGroupeStandard objets2d_;
  private BControleLumiere controleLumiere_;
  private BControleEchelle controleEchelle_;
  private BVolumeTransformation transfoVolume_;
  private BImportVolume importVolume_;
  BMagnetoscope mag_;
  boolean anim_;

  public BUnivers getUnivers() {
    return u_;
  }

  public BArbreVolume getArbreVolume() {
    return av_;
  }

  public void setArbreVolume(final BArbreVolume _av) {
    av_ = _av;
    controleLumiere_.addPropertyChangeListener(av_);
  }

  public EbliFilleVue3D(final BuInformationsDocument _idAppli, final boolean _animation) {
    this(_idAppli, null, _animation);
  }

  public EbliFilleVue3D(final BuInformationsDocument _idAppli, final BuCommonInterface _appli, final boolean _animation) {
    super(EbliLib.getS("Vue 3D"), true, false, true, true, _appli, _idAppli);
    // addInternalFrameListener(this);
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build arbre volume");
    }
    av_ = new BArbreVolume();
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build normal menu");
    }
    mnVolumes_ = av_.buildNormalMenu();
    idAppli_ = _idAppli;
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("build animation");
    }
    build(_animation);
  }

  public final void build(final boolean _animation) {
    frame_ = new MyFrame(EbliLib.getS("Vue 3D"));
    u_ = new BUnivers(new BCanvas3D(SimpleUniverse.getPreferredConfiguration()));
    u_.addPropertyChangeListener(frame_);
    final String s = EbliPreferences.EBLI.getStringProperty("volume.background");
    try {
      final String red = s.substring(0, s.indexOf(','));
      final String green = s.substring(s.indexOf(',') + 1, s.lastIndexOf(','));
      final String blue = s.substring(s.lastIndexOf(',') + 1);
      backgroundUnivers_ = new Color(Integer.parseInt(red), Integer.parseInt(green), Integer.parseInt(blue));
    } catch (final StringIndexOutOfBoundsException ex) {
      backgroundUnivers_ = Color.GRAY;
    }
    backgroundUnivers_ = Color.GRAY;
    u_.setImmediateBackground(backgroundUnivers_);
    frame_.addPropertyChangeListener(av_);
    // Panel Sud : controle deplacement
    final JPanel jp = new JPanel();
    jp.setLayout(new FlowLayout());
    final BUniversInteraction uiTmp = new BUniversInteraction(u_);
    final JButton init = new JButton("Init");
    init.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        u_.init();

      }

    });
    final JComboBox vue = new JComboBox();
    vue.addItem("Perspective");
    vue.addItem("Dessus");
    vue.addItem("Face");
    vue.addItem("Gauche");
    vue.setEditable(false);
    vue.addItemListener(new VueListener());
    final BControleVolume cv = new BControleVolume(u_);
    cv.addRepereEventListener(uiTmp);
    cv.addPropertyChangeListener(uiTmp);
    // final BPas coef = new BPas(cv);
    // BPosition pos=new BPosition();
    // u_.addPropertyChangeListener("position",pos);
    final TransformTypeIn transf = new TransformTypeIn();
    transf.addRepereEventListener(uiTmp);
    transf.addItemListener(uiTmp);
    u_.addPropertyChangeListener("position", transf);
    final JCheckBox orbital = new JCheckBox("Orbital");
    orbital.addItemListener(uiTmp);
    orbital.setSelected(false);
    final JPanel boutons = new JPanel();
    boutons.setLayout(new BuVerticalLayout());
    boutons.add(init);
    boutons.add(vue);
    boutons.add(orbital);
    cart_ = new BCartouche();
    cv.addPropertyChangeListener(cart_);
    cart_.setName("Cartouche");
    cart_.setFont(new Font("SansSerif", Font.PLAIN, 10));
    cart_.setInformations(idAppli_);
    cart_.setForeground(Color.black);
    cart_.setBackground(new Color(255, 255, 224));
    cart_.setVisible(false);
    objets2d_ = new BGroupeStandard();
    objets2d_.setName("2D");
    objets2d_.add(cart_);
    // jp.add(pos);
    jp.add(transf);
    jp.add(cv);
    // jp.add(coef);
    jp.add(boutons);
    jp.add(cart_);
    final BPaletteSelecteurReduitCouleur palette = new BPaletteSelecteurReduitCouleur();
    palette.addPropertyChangeListener(av_);
    pbPalette_ = new BuPopupButton(EbliResource.EBLI.getString("Palette"), palette);
    pbPalette_.setToolTipText(EbliResource.EBLI.getString("Couleurs du volume"));
    pbPalette_.setIcon(EbliResource.EBLI.getIcon("palettecouleur"));
    final BSelecteurTexture stex = new BSelecteurTexture();
    stex.addPropertyChangeListener(av_);
    pbTexture_ = new BuPopupButton(EbliResource.EBLI.getString("Texture"), stex);
    pbTexture_.setToolTipText(EbliResource.EBLI.getString("Texture du volume"));
    pbTexture_.setIcon(EbliResource.EBLI.getIcon("texture"));
    controleLumiere_ = new BControleLumiere();
    controleLumiere_.addPropertyChangeListener(av_);
    pbLumiere_ = new BuPopupButton(EbliResource.EBLI.getString("Lumiere"), controleLumiere_);
    pbLumiere_.setToolTipText(EbliResource.EBLI.getString("R�glage de la lumiere"));
    pbLumiere_.setIcon(EbliResource.EBLI.getIcon("lumiere"));
    controleEchelle_ = new BControleEchelle();
    controleEchelle_.addPropertyChangeListener(av_);
    pbEchelle_ = new BuPopupButton(EbliResource.EBLI.getString("Echelle"), controleEchelle_);
    pbEchelle_.setToolTipText(EbliResource.EBLI.getString("Deformation suivant les Z"));
    pbEchelle_.setIcon(EbliResource.EBLI.getIcon("echellez"));
    transfoVolume_ = new BVolumeTransformation();
    transfoVolume_.addPropertyChangeListener(av_);
    pbTransfo_ = new BuPopupButton(EbliResource.EBLI.getString("Transfo"), transfoVolume_);
    pbTransfo_.setToolTipText(EbliResource.EBLI.getString("Transformation"));
    pbTransfo_.setIcon(EbliResource.EBLI.getIcon("transform"));
    importVolume_ = new BImportVolume();
    importVolume_.addPropertyChangeListener(av_);
    importVolume_.setToolTipText(EbliResource.EBLI.getString("Importer un Objet VRML"));
    importVolume_.setIcon(BuResource.BU.getIcon("importer"));
    final JPanel est = new JPanel(new BuVerticalLayout());
    est.add(av_);
    if (_animation) {
      final JButton animbu = new JButton("Anim");
      // anim.addPropertyChangeListener(av_);
      animbu.addActionListener(new AL());
      est.add(animbu);
    }
    // est.add(controle_echelle);
    mag_ = new BMagnetoscope(u_);
    est.add(mag_);
    final JComponent content = (JComponent) frame_.getContentPane();
    content.setLayout(new BorderLayout());
    content.add("Center", u_.getCanvas3D());
    content.add("South", jp);
    content.add("East", est);
    if (gs_ != null) {
      u_.setRoot(gs_);
    }
    if (t_ != null) {
      u_.setUniversTransforms(t_);
    }
    u_.getCanvas3D().freeze(false);
    setVisible(false);
    frame_.addWindowListener(this);
    frame_.setSize(new Dimension(600, 600));
    frame_.setVisible(true);
  }

  // BuInternalFrame
  public void cache() {
    hide_ = true;
    frame_.setVisible(false);
  }

  public void montre() {
    frame_.setVisible(true);
  }

  @Override
  public JMenu[] getSpecificMenus() {
    final JMenu[] r = new JMenu[1];
    r[0] = mnVolumes_;
    return r;
  }

  @Override
  public JComponent[] getSpecificTools() {
    pbPalette_.setDesktop((BuDesktop) getDesktopPane());
    pbTexture_.setDesktop((BuDesktop) getDesktopPane());
    pbLumiere_.setDesktop((BuDesktop) getDesktopPane());
    pbEchelle_.setDesktop((BuDesktop) getDesktopPane());
    pbTransfo_.setDesktop((BuDesktop) getDesktopPane());
    // import_volume.setDesktop((BuDesktop)getDesktopPane());
    final JComponent[] r = new JComponent[6];
    r[0] = pbPalette_;
    r[1] = pbTexture_;
    r[2] = pbLumiere_;
    r[3] = pbEchelle_;
    r[4] = pbTransfo_;
    r[5] = importVolume_;
    return r;
  }

  @Override
  public String[] getEnabledActions() {
    final String[] r = new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER" };
    return r;
  }

  public void setRoot(final BGroupeStandard _root) {
    gs_ = _root;
    gs_.add(objets2d_);
    u_.setRoot(gs_);
    av_.setVolume(u_.getRoot());
  }

  // // InternalFrameListener
  // public void internalFrameActivated(InternalFrameEvent _evt) {}
  //
  // public void internalFrameClosed(InternalFrameEvent _evt) {}
  //
  // public void internalFrameClosing(InternalFrameEvent _evt) {}
  //
  // public void internalFrameDeactivated(InternalFrameEvent _evt) {}
  //
  // public void internalFrameDeiconified(InternalFrameEvent _evt) {}
  //
  // public void internalFrameIconified(InternalFrameEvent _evt) {}
  //
  // public void internalFrameOpened(InternalFrameEvent _evt) {}

  // WindowListener
  @Override
  public void windowActivated(final WindowEvent _evt) {
    try {
      setSelected(true);
    } catch (final PropertyVetoException ex) {}
  }

  @Override
  public void windowClosed(final WindowEvent _evt) {}

  @Override
  public void windowClosing(final WindowEvent _evt) {
  /*
   * cache(); u_.animate(false); try { setClosed(true); } catch (PropertyVetoException ex) {}
   */
  }

  public JFrame getFrame() {
    return frame_;
  }

  @Override
  public void windowDeactivated(final WindowEvent _evt) {
    // deb
    if (!hide_) {
      frame_.toFront();
      // fin
      /*
       * else hide=true;
       */
    }
  }

  @Override
  public void windowDeiconified(final WindowEvent _evt) {}

  @Override
  public void windowIconified(final WindowEvent _evt) {}

  @Override
  public void windowOpened(final WindowEvent _evt) {}

  // BuPrintable
  /*
   * public void print(PrintJob _job, Graphics _g) { BuPrinter.INFO_DOC =new BuInformationsDocument();
   * BuPrinter.INFO_DOC.name=getTitle(); BuPrinter.INFO_DOC.logo=BuResource.BU.getIcon("calque");
   * u_.setBackground(Color.white); try{ Thread.currentThread().sleep(1000); } catch(InterruptedException ex)
   * {System.out.println(ex);} Image i=u_.print(); System.out.println("Image OK, on imprime");
   * BuPrinter.printImage(_job,_g,i); u_.setBackground(backgroundUnivers_); }
   */
  @Override
  public int getNumberOfPages() {
    return 1;
  }

  @Override
  public int print(final Graphics _g, final PageFormat _f, final int _page) {
    if (_page != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    u_.setBackground(Color.white);
    final Image i = u_.getCanvas3D().print();
    EbliPrinter.printImage(_g, _f, i);
    u_.setBackground(backgroundUnivers_);
    return Printable.PAGE_EXISTS;
  }

  @Override
  public BuInformationsDocument getInformationsDocument() {
    return idAppli_;
  }
  class MyFrame extends JFrame implements PropertyChangeListener {
    public MyFrame(final String _s) {
      super(_s);
    }
    int indice_;

    @Override
    synchronized public void propertyChange(final PropertyChangeEvent _evt) {
      if (_evt.getPropertyName() == "swap") {
        final Graphics g = u_.getCanvas3D().getGraphics();
        if (cart_ != null) {
          final Rectangle rect = u_.getCanvas3D().getBounds();
          Dimension size = cart_.getPreferredSize();
          if (size.equals(new Dimension(0, 0))) {
            size = new Dimension(300, 150);
          }
          final int x = rect.x + rect.width - size.width;
          final int y = rect.y + rect.height - size.height;
          cart_.setLocation(x, y);
          cart_.paint(g);
        }
      }
    }
  }
  class AL implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent _evt) {
      anim_ = !anim_;
      u_.animate(anim_);
    }
  }
  class VueListener implements ItemListener {
    @Override
    public void itemStateChanged(final ItemEvent _e) {
      u_.setVue(((JComboBox) _e.getSource()).getSelectedIndex());
    }
  }

  public void fullscreen() {
    final JWindow win = new JWindow();
    final JComponent cwin = (JComponent) win.getContentPane();
    final Dimension de = Toolkit.getDefaultToolkit().getScreenSize();
    cwin.add(u_.getCanvas3D(), BuBorderLayout.CENTER);
    win.setSize(de.width, de.height);
    win.setLocation(0, 0);
    frame_.setVisible(false);
    win.doLayout();
    win.validate();
    win.setVisible(true);
    win.setLocation(0, 0);
    win.requestFocus();
    win.addKeyListener(new KeyListener() {
      @Override
      public void keyPressed(final KeyEvent _ke) {}

      @Override
      public void keyReleased(final KeyEvent _ke) {}

      @Override
      public void keyTyped(final KeyEvent _ke) {
        if (_ke.getKeyChar() == 27) {
          win.setVisible(false);
          cwin.remove(u_.getCanvas3D());
          frame_.getContentPane().add("Center", u_.getCanvas3D());
          frame_.setVisible(true);
        } else if (_ke.getKeyChar() == ' ') {
          mag_.play();
        }
      }
    });
  }
}
