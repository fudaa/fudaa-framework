/*
 * @creation     1999-10-12
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.repere.RepereEventListener;
import org.fudaa.ebli.volume.common.BUniversInterface;

/**
 * Permet l'interaction avec l'Univers en gerant les déplacements.
 * 
 * @version $Revision: 1.12 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BUniversInteraction implements RepereEventListener, ItemListener, PropertyChangeListener {
  private final BUniversInterface u_;
  private boolean orbital_;

  /**
   * Constructeur reliant le module d'interaction et un univers.
   * 
   * @param _u BUnivers dans lequel on veut se deplacer
   */
  public BUniversInteraction(final BUniversInterface _u) {
    u_ = _u;
  }

  /**
   * Reception d'un ItemEvent : changement de mode de rotation ( centré sur la camera ou sur les objets ).
   * 
   * @param _e ItemEvent
   */
  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.SELECTED) {
      orbital_ = true;
    } else {
      orbital_ = false;
    }
  }

  /**
   * Reception d'un RepereEvent : demande de déplacement dans l'univers.
   * 
   * @param _e RepereEvent contenant la matrice de transformation
   */
  @Override
  public void repereModifie(final RepereEvent _e) {
    boolean changed = false;
    for (int i = 0; i < 3 && !changed; i++) {
      // if (_e.aChange(0, i)) u_.orbital(i, _e.valeur(0, i));
      if (_e.aChange(0, i)) {
        changed = true;
      }
    }
    if (changed) {
      u_.scale(_e.valeur(0, 0));
    }
    for (int i = 0; i < 3; i++) {
      if (_e.aChange(1, i)) {
        u_.translation(i, _e.valeur(1, i));
      }
    }
    for (int i = 0; i < 3; i++) {
      if (_e.aChange(2, i)) {
        if (orbital_) {
          u_.orbital(i, -_e.valeur(2, i));
        } else {
          u_.rotation(i, -_e.valeur(2, i));
        }
      }
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _e) {
    if (_e.getPropertyName().equals("rapide") && "yes".equals(EbliPreferences.EBLI.getStringProperty("volume.rapide"))) {
      u_.setRapide(((Boolean) _e.getNewValue()).booleanValue());
    }
  }
}
