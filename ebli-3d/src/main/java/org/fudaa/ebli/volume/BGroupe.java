/*
 * @creation     1999-12-30
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;
/**
 * @version      $Revision: 1.5 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Christophe Delhorbe
 */
public interface BGroupe {}
