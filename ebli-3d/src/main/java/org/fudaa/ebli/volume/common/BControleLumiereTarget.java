/*
 * @creation 19 mai 2006
 * @modification $Date: 2006-06-12 09:55:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.common;

import javax.vecmath.Vector3f;

/**
 * @author fred deniger
 * @version $Id: BControleLumiereTarget.java,v 1.1 2006-06-12 09:55:22 deniger Exp $
 */
public interface BControleLumiereTarget {

  Vector3f getDirection();

  void setDirection(Vector3f _v);

  double getIntensite();

  void setIntensite(double _i);

}
