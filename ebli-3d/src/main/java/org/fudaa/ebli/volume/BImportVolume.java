/*
 * @creation     2000-01-20
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import com.memoire.bu.BuButton;

/**
 * @version      $Revision: 1.8 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author       Christophe Delhorbe
 */
public class BImportVolume extends BuButton implements ActionListener {
  Component parent_;
  //	PropertyChangeSupport pcs;
  File directory_;
  public BImportVolume() {
    directory_= null;
    addActionListener(this);
    //		pcs=new PropertyChangeSupport(this);
  }
  /*	public void addPropertyChangeListener(PropertyChangeListener _p)
  	{
  		pcs.addPropertyChangeListener(_p);
  	}
  */
  @Override
  public void actionPerformed(final ActionEvent _e) {
   /* File file= null;
    String filename= null;
    JFileChooser chooser= new JFileChooser();
    BuFileFilter filter=
      new BuFileFilter(new String[] { "wrl" }, "Objets VRML");
    chooser.setFileFilter(filter);
    chooser.setCurrentDirectory(directory_);
    int returnval= chooser.showOpenDialog(parent_);
    if (returnval == JFileChooser.APPROVE_OPTION) {
      file= chooser.getSelectedFile();
      directory_= chooser.getCurrentDirectory();
      System.out.println(file);
      filename= file.getAbsolutePath();
      System.out.println(filename);
      BObjetVRML obj= null;
      obj= new BObjetVRML(filename);
      //pcs.
      firePropertyChange("nouvelObjet", null, obj);
    }*/
  }
}
