/*
 * @creation     1999-12-07
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyChangeEvent;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInformationsDocument;

import org.fudaa.ctulu.CtuluLibString;
/**
 * Repr�sentation du cartouche sur la vue 3D.
 *
 * @version      $Id: BCartouche.java,v 1.14 2007-05-04 13:49:46 deniger Exp $
 * @author       Christophe Delhorbe
 */
public class BCartouche extends BObjet2D {
  private Image cache_;
  private Dimension taille_;
  private boolean premierefois_= true;
  /**
  * Constuit le Cartouche.
  * Il faudra ensuite lui affecter les informations via <I>setInformations</I>
  *
  */
  public BCartouche() {
    super();
    taille_= null;
    setRapide(false);
  }
  /**
  *
  * Paint le cartouche.
  * Devrait �tre appel�e � chaque <I>postswap</I> du Canvas 3D
  *
  */
  @Override
  public void paint(final Graphics _g) {
    if (premierefois_ && taille_!=null) {
      cache_= createImage(taille_.width, taille_.height);
      final Graphics gd= cache_.getGraphics();
      final Color fg= getForeground();
      final Color bg= getBackground();
      final Font font= getFont();
      if (font != null) {
        gd.setFont(font);
      }
      final FontMetrics fm= gd.getFontMetrics();
      final Dimension d= getSize();
      final BuIcon icon= informations_.logo;
      int wi= 0;
      int hi= 0;
      if (icon != null) {
        wi= icon.getIconWidth();
        hi= icon.getIconHeight();
      }
      if (isRapide()) {
        int x= d.width - 10;
        int y= d.height - 10;
        if (taille_ == null) {
          gd.setColor(fg);
          gd.drawLine(x - 5, y, x, y);
          gd.drawLine(x, y - 5, x, y);
        } else {
          x -= taille_.width;
          y -= taille_.height;
          gd.setColor(bg);
          gd.fillRect(x, y, taille_.width, taille_.height);
          gd.setColor(fg);
          gd.drawRect(x, y, taille_.width, taille_.height);
          gd.setColor(bg);
          gd.fillRect(x - 5 - wi, y, wi, hi);
          gd.setColor(fg);
          gd.drawRect(x - 5 - wi, y, wi, hi);
        }
      } else {
        int i;
        final String[] l= getInfosInArray();
        final int hu= fm.getAscent() + fm.getDescent();
        int wu= 0;
        for (i= 0; i < l.length; i++) {
          if (wu < fm.stringWidth(l[i])) {
            wu= fm.stringWidth(l[i]);
          }
        }
        int hs= 2 + hu * l.length;
        final int ws= 2 + wu;
        if (hs + 5 < hi) {
          hs= hi - 5;
        }
        final int x= d.width - 10 - ws;
        final int y= d.height - 10 - hs;
        gd.setColor(bg);
        gd.fillRect(x, y, ws + 4, hs + 4);
        gd.setColor(fg);
        for (i= 0; i < l.length; i++) {
          gd.drawString(l[i], x + 3, y + 3 + hu * i + fm.getAscent());
        }
        gd.drawRect(x, y, ws + 4, hs + 4);
        if (icon != null) {
          icon.paintIcon(this, gd, x - wi - 5, y);
        }
        taille_= new Dimension(ws + 4, hs + 4);
      }
      premierefois_= false;
    }
    if (isVisible() && !isRapide()) {
      final int xorig= getX();
      final int yorig= getY();
      _g.drawImage(cache_, xorig, yorig, this);
    }
  }
  private String[] getInfosInArray() {
    return new String[] {
      informations_.name
        + CtuluLibString.ESPACE
        + informations_.version
        + CtuluLibString.ESPACE
        + informations_.date,
      informations_.organization,
      informations_.author,
      informations_.contact };
  }
  // Proprietes
  private BuInformationsDocument informations_;
  /**
    * Accesseur de la propriete <I>informations</I>.
    * Elle donne l'information a afficher sur le document.
    */
/*  BuInformationsDocument getInformations() {
    return informations_;
  }*/
  /**
    * Affectation de la propriete <I>informations</I>.
    */
  public void setInformations(final BuInformationsDocument _informations) {
    if (informations_ != _informations) {
      final BuInformationsDocument vp= informations_;
      informations_= _informations;
      firePropertyChange("informations", vp, informations_);
      final FontMetrics fm= getFontMetrics(getFont());
      final BuIcon icon= informations_.logo;
      int wi= 0;
      int hi= 0;
      if (icon != null) {
        wi= icon.getIconWidth();
        hi= icon.getIconHeight();
      }
      int i;
      final String[] l=
        getInfosInArray();
      final int hu= fm.getAscent() + fm.getDescent();
      int wu= 0;
      for (i= 0; i < l.length; i++) {
        if (wu < fm.stringWidth(l[i])) {
          wu= fm.stringWidth(l[i]);
        }
      }
      int hs= 2 + hu * l.length;
      final int ws= 2 + wu;
      if (hs + 5 < hi) {
        hs= hi - 5;
      }
      taille_= new Dimension(4 + ws + 4 + 5 + wi + 5, hs + 4 + 10);
      setSize(taille_);
      repaint();
    }
  }
  /**
  * Interprete les evenements propertyChange (ici seulement <I>rapide</I>.
  */
  @Override
  public void propertyChange(final PropertyChangeEvent _e) {
    if (_e.getPropertyName().equals("rapide")) {
      setRapide(((Boolean)_e.getNewValue()).booleanValue());
    }
  }
}
