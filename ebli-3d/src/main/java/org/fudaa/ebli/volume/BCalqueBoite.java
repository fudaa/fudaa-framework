/*
 * @creation     1999-11-29
 * @modification $Date: 2007-01-17 10:45:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import gnu.trove.TIntArrayList;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.media.j3d.*;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.fudaa.ctulu.iterator.NumberIterator;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Repr�sentation d'un champ de vecteurs 3D.
 * 
 * @version $Revision: 1.5 $ $Date: 2007-01-17 10:45:16 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BCalqueBoite extends BGrille {

  /**
   * constucteur par defaut.
   */
  public BCalqueBoite() {
    super();
  }

  /**
   * On construit un champs de vecteur de nom <I>s</I>.
   */
  public BCalqueBoite(final String _s) {
    super(_s);

  }

  private int nbExtPt_ = -1;

  @Override
  public boolean isFilaireEditable() {
    return false;
  }

  @Override
  public boolean isRapideEditable() {
    return true;
  }

  private void boite() {
    bbox_ = new BoundingBox();
    final List lines = new ArrayList(30);
    final double minZ = boite_.getMinZ();
    lines.add(new Point3d(boite_.getMinX(), boite_.getMinY(), minZ));
    lines.add(new Point3d(boite_.getMinX(), boite_.getMaxY(), minZ));
    lines.add(new Point3d(boite_.getMaxX(), boite_.getMaxY(), minZ));
    lines.add(new Point3d(boite_.getMaxX(), boite_.getMinY(), minZ));
    lines.add(new Point3d(boite_.getMinX(), boite_.getMinY(), boite_.getMaxZ()));
    lines.add(new Point3d(boite_.getMinX(), boite_.getMaxY(), boite_.getMaxZ()));
    lines.add(new Point3d(boite_.getMaxX(), boite_.getMaxY(), boite_.getMaxZ()));
    nbExtPt_ = lines.size();

    final TIntArrayList connexion = new TIntArrayList();
    // on construite les segments du bords
    connexion.add(0);
    connexion.add(1);
    connexion.add(1);
    connexion.add(2);
    connexion.add(2);
    connexion.add(3);
    connexion.add(3);
    connexion.add(0);
    connexion.add(4);
    connexion.add(5);
    connexion.add(5);
    connexion.add(6);
    connexion.add(0);
    connexion.add(4);
    connexion.add(1);
    connexion.add(5);
    connexion.add(2);
    connexion.add(6);

    // grille sur X
    final NumberIterator it = new NumberIterator();
    it.init(boite_.getMinX(), boite_.getMaxX(), 10);
    int idx = lines.size();
    while (it.hasNext()) {
      it.nextMajor();
      final double x = it.currentValue();
      if (x >= boite_.getMaxX()) {
        break;
      }
      lines.add(new Point3d(x, boite_.getMinY(), minZ));
      lines.add(new Point3d(x, boite_.getMaxY(), minZ));
      connexion.add(idx++);
      connexion.add(idx++);
    }
    it.init(boite_.getMinY(), boite_.getMaxY(), 10);
    while (it.hasNext()) {
      it.nextMajor();
      final double y = it.currentValue();
      if (y >= boite_.getMaxY()) {
        break;
      }
      lines.add(new Point3d(boite_.getMinX(), y, minZ));
      lines.add(new Point3d(boite_.getMaxX(), y, minZ));
      connexion.add(idx++);
      connexion.add(idx++);
    }

    // on construit
    final IndexedLineArray linearray = new IndexedLineArray(lines.size(), GeometryArray.COORDINATES
        | GeometryArray.COLOR_4, connexion.size());
    final Point3d[] toArray = (Point3d[]) lines.toArray(new Point3d[lines.size()]);
    for (int i = 0; i < toArray.length; i++) {
      bbox_.combine(toArray[i]);
    }

    linearray.setCoordinates(0, toArray);
    linearray.setCoordinateIndices(0, connexion.toNativeArray());

    // les capacit�s
    linearray.setCapability(GeometryArray.ALLOW_COLOR_READ);
    linearray.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
    linearray.setCapability(GeometryArray.ALLOW_COUNT_WRITE);
    linearray.setCapability(IndexedGeometryArray.ALLOW_COLOR_INDEX_READ);
    linearray.setCapability(IndexedGeometryArray.ALLOW_COLOR_INDEX_WRITE);
    linearray.setCapability(GeometryArray.ALLOW_COUNT_READ);
    linearray.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
    shape_.setGeometry(linearray);

    final PolygonAttributes pa = new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_LINE);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final LineAttributes la = new LineAttributes();
    la.setLineWidth(2f);
    la.setLinePattern(LineAttributes.PATTERN_SOLID);
    final Appearance ap = new Appearance();
    ap.setPolygonAttributes(pa);
    ap.setLineAttributes(la);
    final Point3d low = new Point3d();
    final Point3d up = new Point3d();
    bbox_.getLower(low);
    bbox_.getUpper(up);
    boiteEnglobante_ = new com.sun.j3d.utils.geometry.Box((float) (up.x - low.x) / 2, (float) (up.y - low.y) / 2,
        (float) (up.z - low.z) / 2, ap);
    final TransformGroup btg = new TransformGroup();
    final TransformGroup bTg = new TransformGroup();
    final Transform3D t3d = new Transform3D();
    t3d.set(new Vector3d((low.x + up.x) / 2, (low.y + up.y) / 2, (low.z + up.z) / 2));
    bTg.setTransform(t3d);

    bTg.addChild(boiteEnglobante_);
    switch_.addChild(btg);

  }

  @Override
  public boolean setCouleur(final Color _c) {
    if (nbExtPt_ > 0) {
      if (shape_ == null || shape_.getGeometry() == null) {
        return false;
      }
      isColorUsed_ = true;
      final IndexedLineArray indexedLineArray = ((IndexedLineArray) shape_.getGeometry());
      final int nbPts = indexedLineArray.getVertexCount();
      final int nbIndices = indexedLineArray.getIndexCount();
      // MODIF AVA pour j3d1.2 Color3f couleur=new
      // Color3f((float)c.getRed()/255,(float)c.getGreen()/255,(float)c.getBlue()/255);
      // MODIF AVA pour j3d1.2
      // shape_.getAppearance().getMaterial().setDiffuseColor(new Color3f(_c));
      // shape_.getAppearance().getColoringAttributes().setColor(new Color3f(_c));
      final Color4f[] cs = new Color4f[nbPts];
      Arrays.fill(cs, new Color4f(_c));
      final Color4f in = new Color4f(_c.brighter());
      in.scale(0.6f);
      for (int i = nbExtPt_; i < cs.length; i++) {
        cs[i] = in;
      }
      ((GeometryArray) shape_.getGeometry()).setColors(0, cs);
      for (int i = 0; i < nbIndices; i++) {

        indexedLineArray.setColorIndex(i, i / 2);

      }

    } else {
      return super.setCouleur(_c);
    }
    return true;
  }

  @Override
  public void setFilaire(final boolean _filaire) {
    super.setFilaire(_filaire);
  }

  @Override
  public void setRapide(final boolean _rapide) {
    super.setRapide(_rapide);
  }

  @Override
  public void setBoite(final GrBoite _boite) {
    setGeometrie(_boite);
  }

  /**
   * Genere le volume � partir d'un ensemble de points dans l'espace.
   * 
   * @param _points les points d'appuis des vecteurs
   * @param _vecteurs les vecteurs � g�n�rer
   */
  public void setGeometrie(final GrBoite _b) {
    // super.setRapide(true);
    boite_ = new GrBoite(_b);
    boite();
    setCouleur(Color.GRAY);
    super.setRapide(true);
    super.setRapide(false);
    // super.setFilaire(true);
  }
}
