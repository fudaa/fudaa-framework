/*
 *  @creation     5 mai 2004
 *  @modification $Date: 2006-04-12 15:28:03 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.common;

/**
 * @author Fred Deniger
 * @version $Id: BGrilleAbstract.java,v 1.2 2006-04-12 15:28:03 deniger Exp $
 */
public interface BGrilleAbstract {

  boolean isFilaire();

  void setFilaire(boolean _b);

}
