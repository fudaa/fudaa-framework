/*
 * @creation     1999-12-30
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputAdapter;
import javax.vecmath.Vector3f;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuSlider;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.common.BControleLumiereTarget;

/**
 * @version $Revision: 1.6 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class ZControleLumiere extends JPanel {
  class Carre extends JComponent {
    final int carreMode_;

    public Carre(final int _mode) {
      super();
      carreMode_ = _mode;
      this.setPreferredSize(taille_);
    }

    public int getMode() {
      return carreMode_;
    }

    @Override
    public void paintComponent(final Graphics _g) {
      // setSize(taille);
      _g.setColor(Color.white);
      final int width = taille_.width;
      _g.fillRect(0, 0, width, taille_.height);
      _g.setColor(Color.black);
      final int bas = taille_.height - 5;
      _g.drawLine(0, bas, width, bas);
      _g.drawLine(width, bas, width - 5, bas - 5);
      _g.drawLine(width, bas, width - 5, bas + 5);
      _g.setFont(CtuluLibSwing.getMiniFont());
      _g.drawString(modes_[carreMode_].substring(0, 1), width - 8, bas - 5);
      _g.drawLine(centreX_, 0, centreX_, bas);
      _g.drawLine(centreX_, 0, centreX_ - 5, 5);
      _g.drawLine(centreX_, 0, centreX_ + 5, 5);
      _g.drawString(modes_[carreMode_].substring(1, 2), centreX_ + 7, 10);
      int x = 0;
      if (src_ == null) {
        return;
      }

      if (vecteur_.x > 1) {
        vecteur_.x = 1;
      }
      if (vecteur_.x < -1) {
        vecteur_.x = -1;
      }
      if (vecteur_.y > 1) {
        vecteur_.y = 1;
      }
      if (vecteur_.y < -1) {
        vecteur_.y = -1;
      }
      final double vx = vecteur_.x * centreX_;
      final double vy = vecteur_.y * centreY_;
      switch (carreMode_) {
      case XZ:
        x = (int) vx + centreX_;
        break;
      case YZ:
        x = (int) vy + centreX_;
        break;
      default:
      }
      _g.setColor(Color.blue);
      _g.drawLine(centreX_, 7, x, bas);
      _g.setColor(Color.red);
      _g.fillOval(x - 2, bas - 2, 4, 4);
    }
  }
  class Listener extends MouseInputAdapter {

    public void doMouse(final MouseEvent _evt) {
      if (src_ == null) {
        return;
      }
      if (SwingUtilities.isLeftMouseButton(_evt)) {
        final int x = _evt.getX();
        final float newVal = ((float) (x - centreX_)) / (float) centreX_;
        switch (((Carre) _evt.getSource()).getMode()) {
        case XZ:
          vecteur_.x = newVal;
          break;
        case YZ:
          vecteur_.y = newVal;
          break;
        default:
          return;
        }

        // vecteur_.normalize();
        src_.setDirection(vecteur_);
        vecteur_ = new Vector3f(src_.getDirection());
        // vecteur_.normalize();
        repaint();
      }
    }

    @Override
    public void mouseDragged(final MouseEvent _e) {
      doMouse(_e);
    }

    @Override
    public void mouseReleased(final MouseEvent _e) {
      doMouse(_e);
    }

  }
  private static final double MAX_SLIDER = 100;
  private final static int XZ = 0;
  private final static int YZ = 1;
  private boolean isUpdating_;
  private final Carre xz_;

  private final Carre yz_;
  final Dimension taille_ = new Dimension(64, 64);
  final int centreX_ = taille_.width / 2;
  final int centreY_ = taille_.height / 2;
  final String[] modes_ = { "XZ", "YZ" };
  JSlider slIntensite_;

  // private int mode;
  BControleLumiereTarget src_;
  

  Vector3f vecteur_;

  public ZControleLumiere() {
    this.setPreferredSize(new Dimension(200, 130));
    /* mode= XY; */
    vecteur_ = new Vector3f();
    final Listener lis = new Listener();
    xz_ = new Carre(XZ);
    xz_.addMouseListener(lis);
    xz_.addMouseMotionListener(lis);
    yz_ = new Carre(YZ);
    yz_.addMouseListener(lis);
    yz_.addMouseMotionListener(lis);
    final JPanel carres = new JPanel(new FlowLayout());
    carres.add(xz_);
    carres.add(yz_);
    slIntensite_ = new BuSlider(0, (int) MAX_SLIDER, (int) MAX_SLIDER);
    slIntensite_.getModel().addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(final ChangeEvent _e) {
        slideChanged();

      }

    });
    slIntensite_.setToolTipText(EbliResource.EBLI.getString("Intensité"));
    slIntensite_.setPaintLabels(true);
    slIntensite_.setOrientation(SwingConstants.HORIZONTAL);
    slIntensite_.setMajorTickSpacing(10);
    setLayout(new BuGridLayout(2, 3, 3, false, false));
    add(new BuLabel(EbliLib.getS("Direction")));
    add(carres);
    final BuLabel buLabel = new BuLabel(EbliResource.EBLI.getString("Intensité"));
    add(buLabel);
    add(slIntensite_);
    updateSrc();
    // setSize(getPreferredSize());
  }

  private void updateSrc() {
    isUpdating_ = true;
    slIntensite_.setEnabled(src_ != null);
    if (src_ != null) {
      slIntensite_.getModel().setValue((int) (MAX_SLIDER * src_.getIntensite()));
      vecteur_ = new Vector3f(src_.getDirection());
    }
    repaint();
    isUpdating_ = false;

  }

  void slideChanged() {
    final int val = slIntensite_.getModel().getValue();
    slIntensite_.setToolTipText(EbliResource.EBLI.getString("Intensité") + ": " + val);
    // lbIntensite_.setText(Integer.toString(val));
    if (!isUpdating_ && src_ != null) {
      src_.setIntensite(val / MAX_SLIDER);
    }
  }

  public BControleLumiereTarget getSrc() {
    return src_;
  }

  public void setObjectSrc(final Object _src) {
    if (_src instanceof BControleLumiereTarget) {
      setSrc((BControleLumiereTarget) _src);
    } else {
      setSrc(null);
    }
  }

  public void setSrc(final BControleLumiereTarget _src) {
    src_ = _src;
    updateSrc();
  }
}
