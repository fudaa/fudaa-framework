/*
 * @creation 18 mai 2006
 * @modification $Date: 2007-01-26 14:58:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.controles;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.j3d.Texture;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import com.sun.j3d.utils.image.TextureLoader;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageFileChooser;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.common.BTextureTargetInterface;

/**
 * @author fred deniger
 * @version $Id: BControleTexture.java,v 1.7 2007-01-26 14:58:11 deniger Exp $
 */
public class BControleTexture implements ItemListener, ActionListener {

  private BTextureTargetInterface src_;

  private JCheckBox chbEnable_;
  private JComboBox cbMode_;
  private BuButton btFileChooser_;
  private JPanel pn_;

  public int getMode() {
    return cbMode_.getSelectedIndex() + 2;
  }

  public int getCbIDx(final int _mode) {
    return _mode - 2;
  }

  public JPanel getPanel() {
    buildPanel();
    return pn_;
  }

  public void buildPanel() {
    if (pn_ != null) {
      return;
    }
    pn_ = new BuPanel();
    pn_.setLayout(new BuHorizontalLayout(3));
    chbEnable_ = new BuCheckBox();
    cbMode_ = new BuComboBox(new String[] { "Modulate", "Decal", "Blend", "Replace" });
    btFileChooser_ = new BuButton();
    setBtDefaultTltip();
    chbEnable_.addActionListener(this);
    btFileChooser_.addActionListener(this);
    setNoneIcone();
    cbMode_.addItemListener(this);
    pn_.add(chbEnable_);
    pn_.add(btFileChooser_);
    pn_.add(cbMode_);
    updateSrc();
  }

  private void setBtDefaultTltip() {
    btFileChooser_.setToolTipText(EbliResource.EBLI.getString("Choisir l'image"));
  }

  public void setEnable(final boolean _b) {
    chbEnable_.setEnabled(_b);
    cbMode_.setEnabled(_b);
    btFileChooser_.setEnabled(_b);

  }

  private boolean updating_;

  private void updateSrc() {
    updating_ = true;
    setEnable(src_ != null);
    if (src_ == null) {
      setNoneIcone();
      setBtDefaultTltip();
    } else {
      setEnable(true);
      chbEnable_.setSelected(src_.isTextureEnable());
      if (src_.isTextureEnable()&& src_.getTextureImgUrl()!=null) {
        setIcon(src_.getTextureImgUrl());
        btFileChooser_.setToolTipText(BuResource.BU.getString("Image:") + CtuluLibString.ESPACE
            + src_.getTextureImgUrl().getName());

      } else {
        setNoneIcone();
        setBtDefaultTltip();
      }
      cbMode_.setSelectedIndex(getCbIDx(src_.getTextureAttributesMode()));
    }

    updating_ = false;
  }

  private void setIcon(final File _f) {
    if(_f==null) {
      return;
    }
    BufferedImage im;
    try {
      im = ImageIO.read(_f);
      btFileChooser_.setIcon(new ImageIcon(im.getScaledInstance(12, 12, Image.SCALE_SMOOTH)));
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      setNoneIcone();

    }

  }

  private void setNoneIcone() {
    btFileChooser_.setIcon(BuResource.BU.getIcon("aucun"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (updating_) {
      return;
    }
    if (btFileChooser_ == _e.getSource()) {
      final File f = selectFile();
      if (f != null) {
        updateTexture(f);
      }
    } else if (chbEnable_ == _e.getSource()) {
      if (src_.getTextureImgUrl() == null) {
        final File f = selectFile();
        if (f == null) {
          chbEnable_.setSelected(false);
        } else {
          updateTexture(f);
        }
      } else {
        src_.setTextureEnable(true);
      }
    }

  }

  private Texture updateTexture(final File _f) {
    src_.setTextureImgUrl(_f);
    setIcon(_f);
    final Texture t = new TextureLoader(_f.getAbsolutePath(), pn_).getTexture();
    t.setCapability(Texture.ALLOW_ENABLE_WRITE);
    t.setCapability(Texture.ALLOW_ENABLE_READ);
    t.setEnable(chbEnable_.isSelected());
    src_.setTexture(t);
    src_.setTextureAttributesMode(getMode());
    return t;
  }

  private File selectFile() {
    final File init = src_ == null ? null : src_.getTextureImgUrl();
    final CtuluImageFileChooser fileChooser = new CtuluImageFileChooser(true,null);
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    if (init != null) {
      fileChooser.setSelectedFile(init);
    }
    if (fileChooser.showSaveDialog(pn_) == JFileChooser.APPROVE_OPTION) {
      return fileChooser.getSelectedFile();
    }
    return null;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (updating_ || src_ == null) {
      return;
    }
    src_.setTextureAttributesMode(getMode());

  }

  public BTextureTargetInterface getSrc() {
    return src_;
  }

  public void setSrc(final BTextureTargetInterface _src) {
    if (_src == src_) {
      return;
    }
    src_ = _src;
    updateSrc();
  }
}
