/*
 * @creation 18 mai 2006
 * @modification $Date: 2006-12-20 16:08:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.controles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.controle.BSelecteurColorChooser;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.volume.common.BControleAffichageTarget;

/**
 * @author fred deniger
 * @version $Id: BControleAffichage.java,v 1.6 2006-12-20 16:08:23 deniger Exp $
 */
public class BControleAffichage implements ActionListener {

  BControleAffichageTarget target_;
  BControleTexture txt_;
  BuCheckBox cbOmbre_;
  BuCheckBox cbVisible_;
  BuCheckBox cbFilaire_;
  BuCheckBox cbRapide_;
  BuButton btColorApply_;
  TraceIcon icColor_;
  JButton btColor_;
  BuPanel pn_;

  public void buildPanel() {
    if (pn_ != null) {
      return;
    }
    icColor_ = new TraceIcon(TraceIcon.CARRE_PLEIN, 9);
    pn_ = new BuPanel();
    pn_.setLayout(new BuGridLayout(2, 5, 5));
    pn_.add(new BuLabel(EbliResource.EBLI.getString("Visible")));
    cbVisible_ = new BuCheckBox();
    pn_.add(cbVisible_);
    cbRapide_ = new BuCheckBox();
    pn_.add(new BuLabel(EbliResource.EBLI.getString("Rapide")));
    pn_.add(cbRapide_);

    cbFilaire_ = new BuCheckBox();
    pn_.add(new BuLabel(EbliResource.EBLI.getString("Filaire")));
    pn_.add(cbFilaire_);
    pn_.add(new BuLabel(EbliResource.EBLI.getString("Couleurs")));
    final BuPanel pnColor = new BuPanel();
    pnColor.setLayout(new BuGridLayout(2, 2, 2, false, false, false, false, false));
    btColor_ = new BuButton();
    btColor_.setIcon(icColor_);
    pnColor.add(btColor_);
    btColorApply_ = new BuButton(BuResource.BU.getString("Appliquer"));
    pnColor.add(btColorApply_);
    pn_.add(pnColor);
    pn_.add(new BuLabel(EbliResource.EBLI.getString("Ombrage")));
    cbOmbre_ = new BuCheckBox();
    pn_.add(cbOmbre_);
    pn_.add(new BuLabel(EbliResource.EBLI.getString("Texture")));
    txt_ = new BControleTexture();
    pn_.add(txt_.getPanel());

    cbOmbre_.addActionListener(this);
    cbVisible_.addActionListener(this);
    cbRapide_.addActionListener(this);
    cbFilaire_.addActionListener(this);
    btColorApply_.addActionListener(this);
    btColor_.addActionListener(this);

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (target_ == null) {
      return;
    }
    final Object src = _e.getSource();
    if (btColorApply_ == src) {
      target_.setCouleur(icColor_.getCouleur());
    } else if (cbOmbre_ == src) {
      target_.setEclairage(cbOmbre_.isSelected());
    } else if (cbRapide_ == src) {
      target_.setRapide(cbRapide_.isSelected());
    } else if (cbFilaire_ == src) {
      target_.setFilaire(cbFilaire_.isSelected());
    } else if (cbVisible_ == src) {
      target_.setVisible(cbVisible_.isSelected());
    } else if (btColor_ == src) {
      final BuButton btVal = new BuButton(BuResource.BU.getIcon("valider"));
      final BuButton btClose = new BuButton(BuResource.BU.getIcon("fermer"));
      final BSelecteurColorChooser ch = new BSelecteurColorChooser(new BuButton[] { btVal, btClose }) {
        @Override
        public void apply() {
          super.apply();
          icColor_.setCouleur(getSelectedColor());
        }
      };
      ch.setSelecteurTarget(target_);
      final JDialog dial = new JDialog(CtuluLibSwing.getFrameAncestor(pn_));
      dial.setModal(true);
      dial.setContentPane(ch.getComponent());
      btClose.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent _evt) {
          dial.dispose();
        }

      });
      btVal.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent _evt) {
          ch.apply();
          dial.dispose();
        }

      });
      dial.pack();
      dial.setVisible(true);
    }

  }

  public BControleAffichageTarget getTarget() {
    return target_;
  }

  private void setEnable(final boolean _b) {
    cbOmbre_.setEnabled(_b);
    cbRapide_.setEnabled(_b);
    cbFilaire_.setEnabled(_b);
    cbVisible_.setEnabled(_b);
    btColor_.setEnabled(_b);
    btColorApply_.setEnabled(_b);

  }

  public void setTarget(final BControleAffichageTarget _target) {
    target_ = _target;
    txt_.setSrc(target_);
    setEnable(target_ != null);
    if (target_ != null) {
      cbOmbre_.setSelected(target_.isEclairage());
      cbRapide_.setSelected(target_.isRapide());
      cbRapide_.setEnabled(target_.isRapideEditable());
      cbVisible_.setSelected(target_.isVisible());
      cbFilaire_.setSelected(target_.isFilaire());
      cbFilaire_.setEnabled(target_.isFilaireEditable());
      icColor_.setCouleur(target_.getCouleur());
      btColor_.repaint();
      updateApplyBt();
    }

  }

  public void updateApplyBt() {
    if (target_ != null) {
      btColorApply_.setEnabled(!target_.isColorUsed());
    }
  }

  public BuPanel getPn() {
    buildPanel();
    return pn_;
  }

  public void setPn(final BuPanel _pn) {
    pn_ = _pn;
  }

}
