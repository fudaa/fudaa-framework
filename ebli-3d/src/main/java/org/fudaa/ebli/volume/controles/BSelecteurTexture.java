/*
 * @creation     1999-11-03
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;

import javax.media.j3d.Texture;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import com.sun.j3d.utils.image.TextureLoader;

import org.fudaa.ctulu.CtuluLib;

/**
 * @version $Id: BSelecteurTexture.java,v 1.13 2007-05-04 13:49:45 deniger Exp $
 * @author Christophe Delhorbe
 */
public class BSelecteurTexture extends JPanel {
  public JCheckBox activee_;

  JButton fichier_;
  private FichListener fl_;

  ButtonGroup groupe_;
  ModeListener ml_;
  ActiveeListener al_;
  EclaireeListener el_;
  JSlider shine_;
  JLabel shineValue_;
  ShineListener sl_;
  JSlider transp_;
  JLabel lbTranspValue_;
  TranspListener tl_;
  Component parent_;
  Image prevue_;
  JComponent imageCompo_;

  public BSelecteurTexture(final boolean _activee, final boolean _ombree, final String _fichier, final int _mode,
      final int _shine, final int _transp) {
    parent_ = this;
    imageCompo_ = new JComponent() {
      @Override
      public void paint(final Graphics _g) {
        if (prevue_ != null) {
          final Dimension taille = this.getSize();
          final Image miniature = prevue_.getScaledInstance(taille.width, taille.height, Image.SCALE_DEFAULT);
          final MediaTracker tracker = new MediaTracker(this);
          tracker.addImage(miniature, 0);
          try {
            tracker.waitForID(0);
            if (tracker.isErrorAny()) {
              FuLog.error("Error loading image");
            }
          } catch (final Exception ex) {
            FuLog.error(ex);
          }
          _g.drawImage(miniature, 0, 0, this);
        }
      }
    };
    if (_fichier != null) {
      prevue_ = imageCompo_.getToolkit().createImage(_fichier);
    }
    final MediaTracker tracker = new MediaTracker(imageCompo_);
    tracker.addImage(prevue_, 0);
    try {
      tracker.waitForID(0);
      if (tracker.isErrorAny()) {
        FuLog.error("Error loading image");
      }
    } catch (final Exception ex) {
      FuLog.error(ex);
    }
    imageCompo_.setPreferredSize(new Dimension(64, 64));
    final JPanel pnImage = new JPanel(new FlowLayout());
    pnImage.add(imageCompo_);
    activee_ = new JCheckBox("Image");
    activee_.setSelected(_activee);
    al_ = new ActiveeListener(_activee, this);
    activee_.addActionListener(al_);
    JCheckBox ombree = new JCheckBox("Ombrage");
    ombree.setSelected(_ombree);
    el_ = new EclaireeListener(_ombree, this);
    ombree.addActionListener(el_);
    String fichier = _fichier;
    if (fichier == null) {
      fichier = "Aucune";
    }
    fichier_ = new JButton(fichier);
    fl_ = new FichListener(fichier, this);
    fichier_.addActionListener(fl_);
    JRadioButton[] modes = new JRadioButton[4];
    modes[0] = new JRadioButton("MODULATE");
    modes[1] = new JRadioButton("DECAL");
    modes[2] = new JRadioButton("BLEND");
    modes[3] = new JRadioButton("REPLACE");
    for (int i = 0; i < 4; i++) {
      modes[i].setActionCommand(modes[i].getText());
    }
    modes[_mode].setSelected(true);
    ml_ = new ModeListener(_mode, this);
    groupe_ = new ButtonGroup();
    for (int i = 0; i < 4; i++) {
      modes[i].addActionListener(ml_);
      groupe_.add(modes[i]);
    }
    final JLabel shineLbl = new JLabel("Brillance");
    shine_ = new JSlider(10, 20, _shine);
    sl_ = new ShineListener(_shine, this);
    shine_.addChangeListener(sl_);
    shineValue_ = new JLabel(Double.toString(_shine / 10D));
    final JLabel lbTransp = new JLabel("Transparence");
    transp_ = new JSlider(0, 10, _transp);
    tl_ = new TranspListener(_transp, this);
    transp_.addChangeListener(tl_);
    lbTranspValue_ = new JLabel("" + ((double) _transp / 10));
    final JPanel main = new JPanel(new BuVerticalLayout());
    final JPanel ligne1 = new JPanel(new FlowLayout());
    final JPanel modePanel1 = new JPanel(new GridLayout(1, 2));
    final JPanel modePanel2 = new JPanel(new GridLayout(1, 2));
    final JPanel ombreePanel = new JPanel(new FlowLayout());
    final JPanel shinePanel = new JPanel(new FlowLayout());
    final JPanel shineLabelPanel = new JPanel(new FlowLayout());
    final JPanel shineValuePanel = new JPanel(new FlowLayout());
    final JPanel transpPanel = new JPanel(new FlowLayout());
    final JPanel pnTranspLabel = new JPanel(new FlowLayout());
    final JPanel pnTranspV = new JPanel(new FlowLayout());
    shineLabelPanel.add(shineLbl);
    shineValuePanel.add(shineValue_);
    pnTranspLabel.add(lbTransp);
    pnTranspV.add(lbTranspValue_);
    ombreePanel.add(ombree);
    shinePanel.add(shine_);
    transpPanel.add(transp_);
    ligne1.add(activee_);
    ligne1.add(fichier_);
    modePanel1.add(modes[0]);
    modePanel1.add(modes[1]);
    modePanel2.add(modes[2]);
    modePanel2.add(modes[3]);
    main.add(ligne1);
    main.add(pnImage);
    main.add(new JSeparator());
    main.add(modePanel1);
    main.add(modePanel2);
    main.add(new JSeparator());
    main.add(ombreePanel);
    main.add(new JSeparator());
    main.add(shineLabelPanel);
    main.add(shinePanel);
    main.add(shineValuePanel);
    main.add(new JSeparator());
    main.add(pnTranspLabel);
    main.add(transpPanel);
    main.add(pnTranspV);
    setLayout(new BorderLayout());
    add("Center", main);
    setSize(new Dimension(256, 374));
  }

  public BSelecteurTexture() {
    this(true, false, null, 3, 20, 0);
  }

  @Override
  public void addPropertyChangeListener(final PropertyChangeListener _pcl) {
    ml_.addPropertyChangeListener(_pcl);
    al_.addPropertyChangeListener(_pcl);
    el_.addPropertyChangeListener(_pcl);
    sl_.addPropertyChangeListener(_pcl);
    tl_.addPropertyChangeListener(_pcl);
    fl_.addPropertyChangeListener(_pcl);
  }
  static class ModeListener extends PropertyChangeSupport implements ActionListener {
    int oldValue_ = 3;

    public ModeListener(final int _value, final Object _source) {
      super(_source);
      oldValue_ = _value;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final char c = _e.getActionCommand().charAt(0);
      int value = oldValue_;
      switch (c) {
      /*
       * TextureAttributes.DECAL :3 TextureAttributes.REPLACE :5 TextureAttributes.BLEND :4 TextureAttributes.MODULATE
       * :2
       */
      case 'M':
        value = 2;
        break;
      case 'D':
        value = 3;
        break;
      case 'B':
        value = 4;
        break;
      case 'R':
        value = 5;
        break;
      default:
        // TODO ajouter des instructions au cas "default"
      }
      this.firePropertyChange("TextureAttributesMode", oldValue_, value);
      oldValue_ = value;
    }
  }
  static class ActiveeListener extends PropertyChangeSupport implements ActionListener {
    boolean oldValue_;

    public ActiveeListener(final boolean _value, final Object _source) {
      super(_source);
      oldValue_ = _value;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final boolean value = !oldValue_;
      this.firePropertyChange("TextureEnable", oldValue_, value);
      oldValue_ = value;
    }
  }
  static class EclaireeListener extends PropertyChangeSupport implements ActionListener {
    boolean oldValue_;

    public EclaireeListener(final boolean _value, final Object _source) {
      super(_source);
      oldValue_ = _value;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final boolean value = !oldValue_;
      this.firePropertyChange("Eclairage", oldValue_, value);
      oldValue_ = value;
    }
  }
  class ShineListener extends PropertyChangeSupport implements ChangeListener {
    int oldValue_;

    public ShineListener(final int _old, final Object _source) {
      super(_source);
      oldValue_ = _old;
    }

    @Override
    public void stateChanged(final ChangeEvent _v) {
      final JSlider source = (JSlider) _v.getSource();
      final int value = source.getValue();
      if (value != oldValue_) {
        this.firePropertyChange("brillance", CtuluLib.getDouble((double) oldValue_ / 10), CtuluLib
            .getDouble((double) value / 10));
        shineValue_.setText("" + ((double) value / 10));
        oldValue_ = value;
      }
    }
  }
  class TranspListener extends PropertyChangeSupport implements ChangeListener {
    int oldValue_;

    public TranspListener(final int _old, final Object _source) {
      super(_source);
      oldValue_ = _old;
    }

    @Override
    public void stateChanged(final ChangeEvent _evt) {
      final JSlider source = (JSlider) _evt.getSource();
      final int value = source.getValue();
      if (value != oldValue_) {
        this.firePropertyChange("transparence", CtuluLib.getDouble((double) oldValue_ / 10), CtuluLib
            .getDouble((double) value / 10));
        lbTranspValue_.setText("" + ((double) value / 10));
        oldValue_ = value;
      }
    }
  }
  class FichListener extends PropertyChangeSupport implements ActionListener {
    String oldValue_;
    File directory_;

    public FichListener(final String _old, final Object _source) {
      super(_source);
      oldValue_ = _old;
      directory_ = null;
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      File file = null;
      String filename = null;
      String shortname = null;
      final JFileChooser chooser = new JFileChooser();
      final BuFileFilter filter = new BuFileFilter(new String[] { "gif", "jpg" }, "JPEG & GIF Images");
      chooser.setFileFilter(filter);
      chooser.setCurrentDirectory(directory_);
      final int returnval = chooser.showOpenDialog(parent_);
      if (returnval == JFileChooser.APPROVE_OPTION) {
        file = chooser.getSelectedFile();
        filename = file.getAbsolutePath();
        directory_ = chooser.getCurrentDirectory();
        shortname = file.getName();
        if (filename != oldValue_) {
          fichier_.setText(shortname);
          final Texture t = new TextureLoader(filename, parent_).getTexture();
          t.setCapability(Texture.ALLOW_ENABLE_WRITE);
          t.setEnable(activee_.isSelected());
          this.firePropertyChange("texture", null, t);
          prevue_ = imageCompo_.getToolkit().createImage(filename);
          final MediaTracker tracker = new MediaTracker(imageCompo_);
          tracker.addImage(prevue_, 0);
          try {
            tracker.waitForID(0);
            if (tracker.isErrorAny()) {
              FuLog.error("Error loading image");
            }
          } catch (final Exception ex) {
            FuLog.error(ex);
          }
          imageCompo_.repaint();
          oldValue_ = filename;
        }
      }
    }
  }
}
