/*
 * @file         BNuagePoints.java
 * @creation     1999-11-29
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * @version $Revision: 1.13 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BNuagePoints extends BGrille {
  private int npoints_;

  public BNuagePoints() {
    super();
    npoints_ = 0;
  }

  public BNuagePoints(final String _s) {
    super(_s);
    npoints_ = 0;
  }

  public void setGeometrie(final Point3d[] _points) {
   /* for (int i = 0; i < _points.length; i++) {
      _points[i].x = _points[i].x / ParametresVolumes.ECHELLE;
      _points[i].y = _points[i].y / ParametresVolumes.ECHELLE;
      _points[i].z = _points[i].z / ParametresVolumes.ECHELLE;
      // System.out.println("Point "+points[i]);
    }*/
    bbox_=new BoundingBox();
    bbox_.combine(_points);
    final Point3d low = new Point3d();
    final Point3d up = new Point3d();
    bbox_.getLower(low);
    bbox_.getUpper(up);
    npoints_ = _points.length;
    final PointArray pointarray = new PointArray(npoints_, GeometryArray.COORDINATES | GeometryArray.COLOR_3);
    pointarray.setCoordinates(0, _points);
    for (int i = 0; i < npoints_; i++) {
      pointarray.setColor(i, new Color3f(1f, 1f, 1f));
    }
    pointarray.setCapability(GeometryArray.ALLOW_COLOR_READ);
    pointarray.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
    shape_.setGeometry(pointarray);
    final PolygonAttributes pa = new PolygonAttributes();
    pa.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    pa.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);
    pa.setPolygonMode(PolygonAttributes.POLYGON_LINE);
    pa.setCullFace(PolygonAttributes.CULL_NONE);
    pa.setBackFaceNormalFlip(true);
    final Appearance ap = new Appearance();
    ap.setPolygonAttributes(pa);
    boiteEnglobante_ = new com.sun.j3d.utils.geometry.Box((float) (up.x - low.x) / 2, (float) (up.y - low.y) / 2,
        (float) (up.z - low.z) / 2, ap);
    final TransformGroup bTg = new TransformGroup();
    final Transform3D t3d = new Transform3D();
    t3d.set(new Vector3d((up.x - low.x) / 2, (float) (up.y - low.y) / 2, -(float) (up.z - low.z) / 2));
    bTg.setTransform(t3d);
    bTg.addChild(boiteEnglobante_);
    switch_.addChild(bTg);
    /*for (int i = 0; i < _points.length; i++) {
      _points[i].x = _points[i].x * ParametresVolumes.ECHELLE;
      _points[i].y = _points[i].y * ParametresVolumes.ECHELLE;
      _points[i].z = _points[i].z * ParametresVolumes.ECHELLE;
      // System.out.println("Point "+points[i]);
    }*/
  }
}
