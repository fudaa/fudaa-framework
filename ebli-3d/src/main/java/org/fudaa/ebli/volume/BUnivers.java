/*
 * @creation 11 mai 2006
 * @modification $Date: 2007-05-22 14:19:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Enumeration;
import java.util.List;

import javax.media.j3d.*;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.volume.common.BUniversInterface;

/**
 * @author fred deniger
 * @version $Id: BUnivers.java,v 1.22 2007-05-22 14:19:03 deniger Exp $
 */
public class BUnivers extends VirtualUniverse implements BUniversInterface {

  BCanvas3D canvas_;

  class TimerListener implements ActionListener {
    long time_ = System.currentTimeMillis();

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      canvas_.stopRenderer();
      final List v = BUnivers.this.volumes_.getTousEnfants();
      for (int idx = 0; idx < v.size(); idx++) {
        ((BVolume) v.get(idx)).actualise(time_ - System.currentTimeMillis());
      }
      canvas_.startRenderer();
    }
  }

  // trois constantes pour definir les axes de transformation
  public final static int X = 0;

  public final static int Y = 1;
  public final static int Z = 2;

  // le pere de tous les groupes
  private BGroupeStandard groupe_;
  // le pere de tous les objets : non private pour etre acceder depuis un thread sans artifact
  // le pere de toutes les lumieres
  // Position de la camera, pour affichage
  private Vector3d position_;
  private javax.swing.Timer timer_;
  private TransformGroup transformU_;
  // la vue en cours
  private int vue_;
  // le fond
  // les quatre vues
  private Transform3D[] vues_;
  Background background_;
  BGroupeLumiere lumieres_;
  PropertyChangeSupport support_;
  Locale locale_;

  BGroupeVolume volumes_;

  public BUnivers() {
    this(new BCanvas3D(null));
  }

  public BUnivers(final BCanvas3D _canvas) {
    canvas_ = _canvas;
    locale_ = new Locale(this);
    final ViewPlatform camera = new ViewPlatform();
    final BranchGroup viewGroup = new BranchGroup();
    final TransformGroup viewTg = new TransformGroup();
    viewTg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
    viewTg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
    viewTg.setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);
    viewTg.addChild(camera);
    viewGroup.addChild(viewTg);

    final PhysicalBody body = new PhysicalBody();
    final PhysicalEnvironment env = new PhysicalEnvironment();

    final View view = new View();
    view.setPhysicalBody(body);
    view.setPhysicalEnvironment(env);
    view.setBackClipDistance(100);
    view.setFrontClipDistance(.1);
    view.addCanvas3D(canvas_);
    view.attachViewPlatform(camera);
    transformU_ = viewTg;
    position_ = new Vector3d();
    viewGroup.setCapability(BranchGroup.ALLOW_DETACH);
    locale_.addBranchGraph(viewGroup);
  }

  private void buildFond(Color3f _c) {
    if (background_ == null) {
      background_ = _c == null ? new Background(0F, 0F, 0F) : new Background(_c);
      background_.setCapability(Background.ALLOW_COLOR_WRITE);
      background_.setCapability(Background.ALLOW_COLOR_READ);
      background_.setCapability(Background.ALLOW_APPLICATION_BOUNDS_READ);
      background_.setCapability(Background.ALLOW_APPLICATION_BOUNDS_WRITE);
      // background_.setApplicationBounds(new BoundingSphere(new Point3d(0, 0, 0), 1E12));
      background_.setBoundsAutoCompute(true);

      final BranchGroup fond = new BranchGroup();
      fond.addChild(background_);
      fond.setCapability(BranchGroup.ALLOW_DETACH);
      addBranchGraph(fond);
    }
  }

  /**
   * Ajoute une Branche � l'arbre de la scene, apres l'avoir compil�e.
   * 
   * @param _bg BranchGroup � ajouter
   */
  @Override
  public void addBranchGraph(final BranchGroup _bg) {
    if (!_bg.isCompiled()) {
      _bg.compile();
    }

    locale_.addBranchGraph(_bg);
  }

  public void addObjet(final BVolume _v) {
    volumes_.add(_v);
  }

  public void addPropertyChangeListener(final PropertyChangeListener _l) {
    if (support_ == null) {
      support_ = new PropertyChangeSupport(this);
    }
    support_.addPropertyChangeListener(_l);
  }

  public void addPropertyChangeListener(final String _propr, final PropertyChangeListener _l) {
    if (support_ == null) {
      support_ = new PropertyChangeSupport(this);
    }
    support_.addPropertyChangeListener(_propr, _l);
  }

  /**
   * met en route ou arrete le timer.
   */
  public void animate(final boolean _b) {
    if (_b) {
      if (timer_ == null) {
        timer_ = new javax.swing.Timer(100, new TimerListener());
      }
      timer_.start();
    } else if (timer_ != null) {
      timer_.stop();
    }
  }

  /**
   * detruit le groupe d'objet racine. A modifier par la suite: essaie de contourner les bugs de Java3D. Voir le site
   * http://wiki.java.net/bin/view/Javadesktop/Java3DApplicationDevelopment#Releasing_Canvas3D_View_and_Virt
   */
  public void destroy() {
    for (final Enumeration e = getLocale().getAllBranchGraphs(); e.hasMoreElements();) {
      ((BranchGroup) e.nextElement()).detach();
    }
    BranchGroup emptyGroup = new BranchGroup();
    emptyGroup.setCapability(BranchGroup.ALLOW_DETACH);
    locale_.addBranchGraph(emptyGroup);
    locale_.removeBranchGraph(emptyGroup);
    emptyGroup = null;
    try {
      getCanvas3D().stopRenderer();
      getCanvas3D().setVisible(false);
      try {
        getCanvas3D().setOffScreenBuffer(null);
      } catch (final RuntimeException _evt) {

      }
      final View view = getCanvas3D().getView();
      view.removeAllCanvas3Ds();
      try {
        view.attachViewPlatform(null);
      } catch (final RuntimeException _evt) {

      }
      groupe_.detruire();

      removeAllLocales();
      com.sun.j3d.utils.universe.Viewer.clearViewerMap();
    } catch (final RuntimeException _evt) {

    }
  }

  public void freeze(final boolean _b) {
    getCanvas3D().freeze(_b);
  }

  public BCanvas3D getCanvas3D() {
    return canvas_;
  }

  public TransformGroup getObjectTransformGroup() {
    return volumes_.getTransformGroup();
  }

  /**
   * Renvoie l'objet racine de la scene.
   * 
   * @return le p�re de tous les objets.
   */
  public BGroupeStandard getRoot() {
    return groupe_;
  }

  /**
   * Renvoie tous les volumes de la scene (descends dans les groupes de volumes).
   * 
   * @return vecteur contenant tous les volumes.
   */
  public List getTousEnfants() {
    return groupe_.getTousEnfants();
  }

  @Override
  public TransformGroup getTransformGroup() {
    return transformU_;
  }

  /**
   * renvoie la position et l'orientation de la camera.
   */
  @Override
  public Transform3D[] getUniversTransforms() {
    final Transform3D[] t = new Transform3D[2];
    t[0] = new Transform3D();
    transformU_.getTransform(t[0]);
    t[1] = new Transform3D();
    volumes_.getTransformGroup().getTransform(t[1]);
    return t;
  }

  public int getVue() {
    return vue_;
  }

  public final static int SUD = 0;
  public final static int EST = 1;
  public final static int NORD = 2;
  public final static int OUEST = 3;
  public final static int DESSUS = 4;

  public final static String[] getVues() {
    return new String[] { EbliLib.getS("Sud"), EbliLib.getS("Est"), EbliLib.getS("Nord"), EbliLib.getS("Ouest"),
        EbliLib.getS("Dessus") };
  }

  double zmid_;

  private void updateFond(GrBoite _boite) {
    buildFond(null);
    if (_boite == null) return;
    double maxDist = Math.max(Math.max(_boite.getDeltaX(), _boite.getDeltaY()), _boite.getDeltaZ());
    background_.setApplicationBounds(new BoundingSphere(new Point3d(_boite.getMidX(), _boite.getMidY(), _boite
        .getMidZ()), maxDist * 20));
  }

  @Override
  public void init() {
    if (vues_ == null) {
      vues_ = new Transform3D[5];
    }
    for (int idx = 0; idx < vues_.length; idx++) {
      vues_[idx] = new Transform3D();
    }
    // on cree une nouvelle boite.
    final GrBoite boite = volumes_.getBoite();
    // on calcule les future coordonn�es de la camera.
    final double midX = boite.getMidX();
    final double deltaX = boite.getDeltaX();
    final double midY = boite.getMidY();
    zmid_ = boite.getMidZ();
    final double deltaY = boite.getDeltaY();
    updateFond(boite);

    volumes_.getTransformGroup().setTransform(new Transform3D());

    // l'angle de vision
    final double angleCamera = getCanvas3D().getView().getFieldOfView();
    final double maxL = Math.max(deltaX, deltaY);

    final double z = 0.5 * maxL / Math.tan(angleCamera / 2) + (boite.e_.z_ * echelle_);
    // on suppose qu'une �chelle de 20 est le max pour z
    final double zForClip = 0.5 * maxL / Math.tan(angleCamera / 2) + (boite.e_.z_ * echelle_ * 20);
    final double backClip = Math.hypot(maxL , zForClip);
    getCanvas3D().getView().setBackClipDistance(Math.abs(backClip));
    // conseille par la javadoc de java3D
    getCanvas3D().getView().setFrontClipDistance(backClip / 100);
    // getCanvas3D().getView().setFrontClipDistance(Math.abs(backClip)/4000);
    // L'angle pour la vue perspective
    // final double anglePersp = Math.toRadians(50);
    // VUE SUD
    // on positionne en y pour respecte l'angle de la perspective et
    // et la distance du milieu de la scene
    // on se place en bas de la scene : nord
    /*******************************************************************************************************************
     * <pre>
     *                                  A
     *                                  |\   E
     *                                  | \  |
     *                                  |  \ |
     *                                  |_ _\|_________________________________
     *                                  B    C                                D
     *                                  yMoins                deltaY/2
     * </pre>
     ******************************************************************************************************************/
    // l'angle CAD est l'angle de la camera
    // l'angle BAC est alpha1
    // l'angle CDA est b�ta
    // AED sont align�s
    // BCD sont a l'attitude zmin
    // ||EC|| vaut deltaZ
    // ||AC||=dminForX
    // la camera se trouve en A et on veut quelle soit au dessus du zmax donc ||EC||=deltaZ et que l'on voit les bords
    // du projet: donc AC=dminForX.
    // dminForX =distance entre le milieu de la scene et la camera
    // calcule afin que l'on puisse voir les bords des X
    double ac = deltaX / (2 * Math.tan(angleCamera / 2));

    // double beta = Math.atan(boite.getDeltaZ() / deltaY);
    final double alpha1 = Math.toRadians(70);
    // puis pour la boite
    ac += boite.getDeltaZ() / Math.cos(alpha1);
    double yMoins = Math.sin(alpha1) * ac;

    double yA = boite.o_.y_ - yMoins;
    double zA = (boite.o_.z_ * echelle_) + (Math.cos(alpha1) * ac);

    // on peut calculer le z qui se place au milieu des z et qui prend en compte
    // l'angle de la perspective et la distance par rapport au milieu

    Transform3D t3d2;
    // angle pour le faire regarder vers le bas
    // SUD
    // transformU_.getTransform(vues_[SUD]);
    double alphaCam = alpha1 + angleCamera / 4;
    vues_[SUD].setTranslation(new Vector3d(midX, yA/* + z / 4 */, zA));
    vues_[SUD].setRotation(new AxisAngle4d(1, 0, 0, alphaCam));
    // pour le nord on passe de l'autre cote
    yA = boite.e_.y_ + yMoins;
    vues_[NORD].setTranslation(new Vector3d(midX, yA/* + z / 4 */, zA));
    vues_[NORD].setRotation(new AxisAngle4d(1, 0, 0, -alphaCam));
    t3d2 = new Transform3D();
    // oblige pour remettre la cam�ra dans le bon sens
    t3d2.setRotation(new AxisAngle4d(0, 0, 1, Math.PI));
    vues_[NORD].mul(t3d2);

    // DESSUS simple ....
    vues_[DESSUS].set(new Vector3d(midX, midY, z));

    // EST, OUEST:meme combat que nord sud
    // dans ce cas, il suffit de tourner la camera de + ou - pi/2 selon Z
    // pour l'est et l'ouest
    ac = deltaY / (2 * Math.tan(angleCamera / 2));
    ac += boite.getDeltaZ() / Math.cos(alpha1);
    // beta = Math.atan(boite.getDeltaZ() / (deltaX / 2));
    // alpha1 = Math.PI / 2 - beta - angleCamera;
    yMoins = Math.sin(alpha1) * ac;
    double xA = boite.e_.x_ + yMoins;
    zA = (boite.o_.z_ * echelle_) + Math.cos(alpha1) * ac;
    alphaCam = alpha1 + angleCamera / 2;
    vues_[EST].setTranslation(new Vector3d(xA, midY, zA));
    vues_[EST].setRotation(new AxisAngle4d(0, 0, 1, Math.PI / 2));
    t3d2 = new Transform3D();
    t3d2.setRotation(new AxisAngle4d(1, 0, 0, alphaCam));
    vues_[EST].mul(t3d2);
    xA = boite.o_.x_ - yMoins;
    vues_[OUEST].setTranslation(new Vector3d(xA, midY, zA));
    vues_[OUEST].setRotation(new AxisAngle4d(0, 0, 1, -Math.PI / 2));
    t3d2 = new Transform3D();
    t3d2.setRotation(new AxisAngle4d(1, 0, 0, alphaCam));
    vues_[OUEST].mul(t3d2);

    transformU_.setTransform(getCurrentCamera());
    // vues_[3].setRotation(new AxisAngle4d(1,0,0,Math.PI/2));
  }

  /**
   * Tourne les objets sur un des axes d'origine.
   * 
   * @param _axe axe de rotation (X,Y ou Z)
   * @param _valeur angle, en radians
   */
  @Override
  public void orbital(final int _axe, final double _valeur) {
    // on procede comme pour la translation sauf que la transformation est
    // appliqu�e � objets_ (pere de tous les objets) plutot qu'a la camera.
    final Transform3D t3d = new Transform3D();
    volumes_.getTransformGroup().getTransform(t3d);
    final Transform3D t3d2 = new Transform3D();
    AxisAngle4d v = new AxisAngle4d();
    switch (_axe) {
    case X:
      v = new AxisAngle4d(1, 0, 0, _valeur);
      break;
    // penser que la camera a ete tourn� de PI/2 suivant l'axe X -> echange des axes Y et Z
    case Y:
      v = new AxisAngle4d(0, 0, -1, _valeur);
      break;
    case Z:
      v = new AxisAngle4d(0, -1, 0, _valeur);
      break;
    default:
    }
    t3d2.setRotation(v);
    // on applique cette transformation AVANT les autres
    t3d2.mul(t3d);
    volumes_.getTransformGroup().setTransform(t3d2);
  }

  @Override
  public void removeBranchGraph(final BranchGroup _bg) {
    getLocale().removeBranchGraph(_bg);

  }

  public void removePropertyChangeListener(final PropertyChangeListener _l) {
    if (support_ != null) {
      support_.removePropertyChangeListener(_l);
    }
  }

  public void removePropertyChangeListener(final String _propr, final PropertyChangeListener _l) {
    if (support_ != null) {
      support_.removePropertyChangeListener(_propr, _l);
    }
  }

  public void repaintCanvas() {
    getCanvas3D().repaint();
  }

  /**
   * Tourne la camera sur un axe.
   * 
   * @param _axe axe de rotation (X,Y ou Z)
   * @param _valeur angle, en radians
   */
  @Override
  public void rotation(final int _axe, final double _valeur) {
    // On procede comme pour la translation, sauf qu'il n'est pas necessaire d'envoier un
    // PropertyChangeEvent: la position ne change pas
    final Transform3D t3d = new Transform3D();
    transformU_.getTransform(t3d);
    final Transform3D t3d2 = new Transform3D();
    // AxisAngle: vecteur 4*1 qui stocke l'axe de rotation et l'angle en radians
    AxisAngle4d v = null;
    switch (_axe) {
    case X:
      v = new AxisAngle4d(1, 0, 0, _valeur);
      break;
    case Y:
      v = new AxisAngle4d(0, 1, 0, _valeur);
      break;
    case Z:
      v = new AxisAngle4d(0, 0, 1, _valeur);
      break;
    default:
    }
    if (v == null) {
      return;
    }
    t3d2.setRotation(v);
    t3d.mul(t3d2);
    transformU_.setTransform(t3d);
    getCurrentCamera().set(t3d);
  }

  double echelle_ = 1f;

  protected void updateEchelle(final double _newEchelle) {
    if (_newEchelle == echelle_) {
      return;
    }
    final GrBoite boite = getBoite();

    final Transform3D t3d = new Transform3D();
    transformU_.getTransform(t3d);
    final Vector3d trans = new Vector3d();
    t3d.get(trans);
    final double deltaInit = trans.z - boite.o_.z_ * echelle_;
    trans.z = boite.o_.z_ * _newEchelle + deltaInit;
    t3d.setTranslation(trans);
    transformU_.setTransform(t3d);
    for (final Enumeration en = lumieres_.getAllChildren(); en.hasMoreElements();) {
      final Object obj = en.nextElement();
      if (obj instanceof BLumiereDirectionnelle) {
        final BLumiereDirectionnelle calque = (BLumiereDirectionnelle) obj;
        final BoundingSphere b = calque.getLightBounds();
        final Point3d center = new Point3d();
        b.getCenter(center);
        center.z = boite.e_.z_ * _newEchelle;
        b.setCenter(center);
      }
    }
    echelle_ = _newEchelle;

    // init();
    // getCurrentCamera().set(t3d);
  }

  @Override
  public void scale(final double _scale) {
    final Vector3d pos = new Vector3d();
    getCurrentCamera().get(pos);
    final double deltaZ = -(pos.z - zmid_) / 5;
    translation(Z, deltaZ);
  }

  private Transform3D getCurrentCamera() {
    return vues_[vue_];
  }

  @Override
  public GrBoite getBoite() {
    return volumes_ == null ? null : volumes_.getBoite();
  }

  /**
   * Fixe la couleur du fond.
   */
  public void setBackground(final Color _c) {
    background_.setColor(new Color3f(_c));
    // getCanvas3D().waitForSwap();
  }

  public void setImmediateBackground(final Color _c) {
    if (background_ == null) buildFond(new Color3f(_c));
    else
      background_.setColor(new Color3f(_c));
  }

  public Color getBackground() {
    Color3f c3f = new Color3f();
    background_.getColor(c3f);
    return c3f.get();
  }

  /**
   * Change l'affichage de la sc�ne : choix entre rapidit� et apparence.
   * 
   * @params _rapide : mode rapide? ( =affichage d�t�rior�)
   */
  @Override
  public void setRapide(final boolean _rapide) {
    groupe_.setRapide(_rapide);
  }

  public void setRoot(final BGroupeStandard _root) {
    buildFond(new Color3f(Color.WHITE));
    if (groupe_ != null) {
      getLocale().removeBranchGraph(volumes_);
      getLocale().removeBranchGraph(lumieres_);
    }
    groupe_ = _root;
    volumes_ = _root.getGroupeVolume();
    lumieres_ = _root.getGroupeLumiere();
    addBranchGraph(volumes_);
    addBranchGraph(lumieres_);
  }

  @Override
  public void setUniversTransform(final Transform3D _t) {
    transformU_.setTransform(_t);
  }

  /**
   * positione la camera.
   */
  public void setUniversTransforms(final Transform3D[] _t) {
    transformU_.setTransform(_t[0]);
    volumes_.getTransformGroup().setTransform(_t[1]);
  }

  public void setVue(final int _vue) {
    if (vues_[_vue] != null) {
      transformU_.setTransform(vues_[_vue]);
      vue_ = _vue;
    }
  }

  /**
   * Translate la camera sur un axe.
   * 
   * @param _axe axe de translation (X,Y ou Z)
   * @param _valeur longueur de la translation en metres
   */
  @Override
  public void translation(final int _axe, final double _valeur) {
    // on cree une nouvelle transformation
    final Transform3D t3d = new Transform3D();
    // o� l'on stocke celle de la camera
    transformU_.getTransform(t3d);
    // on stocke l'ancien vecteur position de la camera
    final Vector3d oldPosition = new Vector3d();
    t3d.get(oldPosition);
    final Transform3D t3d2 = new Transform3D();
    Vector3d v = new Vector3d();
    switch (_axe) {
    case X:
      v = new Vector3d(_valeur, 0, 0);
      break;
    case Y:
      v = new Vector3d(0, _valeur, 0);
      break;
    case Z:
      v = new Vector3d(0, 0, _valeur);
      break;
    default:
    }
    // on stocke la translation dans une nouvelle matrice de transformation
    t3d2.setTranslation(v);
    // on multiplie l'ancienne matrice par la nouvelle
    // t3d.mul(t3d2);
    t3d.mul(t3d2);
    // on prend le nouveau vecteur position
    t3d.get(position_);
    // on envoie un PropertyChanceEvent pour raffraichir l'affichage de la position de la camera
    if (support_ != null) {
      support_.firePropertyChange("position", oldPosition, position_);
    }
    // on remplace l'ancienne matrice par la nouvelle -> la camera s'est d�plac�e
    transformU_.setTransform(t3d);
    getCurrentCamera().set(t3d);
  }

  public Locale getLocale() {
    return locale_;
  }

}
