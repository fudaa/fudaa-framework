/*
 *  @creation     5 mai 2004
 *  @modification $Date: 2006-06-27 12:09:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume.common;

import javax.media.j3d.BranchGroup;

/**
 * @author Fred Deniger
 * @version $Id: BVolumeAbstract.java,v 1.4 2006-06-27 12:09:44 deniger Exp $
 */
public abstract class BVolumeAbstract extends BranchGroup implements Objet3DInterface {

  public BVolumeAbstract() {
    super();
  }

  @Override
  public boolean isFilaireEditable() {
    return true;
  }

  @Override
  public boolean isRapideEditable() {
    return false;
  }

}
