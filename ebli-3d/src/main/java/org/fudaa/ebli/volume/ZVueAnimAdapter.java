/*
 * @creation 23 mai 2006
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.volume;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.image.BufferedImage;
import java.util.Map;

import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.animation.EbliAnimationSourceAbstract;

class ZVueAnimAdapter extends EbliAnimationSourceAbstract {

  private final ZVue3DPanel panel_;

  public ZVueAnimAdapter(final ZVue3DPanel _panel) {
    panel_ = _panel;}

  @Override
  public Component getComponent() {
    return panel_.u_.getCanvas3D();
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return panel_.getDefaultImageDimension();
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return panel_.produceImage(null);
  }
  
  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return panel_.produceImage(_w,_h, null);
  }


  @Override
  public void setVideoMode(final boolean _b) {
    // pour eviter qu'un thread soit cree par la vue calque
    // getVueCalque().setNoRepaintTask(_b);
    final Frame f = CtuluLibSwing.getFrameAncestor(panel_);
    if (f != null) {
      f.setResizable(!_b);
    }

  }
}
