/*
 * @creation     1999-09-27
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.media.j3d.Group;
import javax.media.j3d.Switch;

import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.volume.common.GroupeInterface;
import org.fudaa.ebli.volume.common.Objet3DInterface;

/**
 * Groupe de Volumes.
 * 
 * @version $Revision: 1.18 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BGroupeVolume extends BVolume implements GroupeInterface {
  private final Switch switch_;

  /**
   * Construit et initialise le groupe.
   */
  public BGroupeVolume() {
    switch_ = new Switch(Switch.CHILD_MASK);
    switch_.setCapability(Group.ALLOW_CHILDREN_EXTEND);
    switch_.setCapability(Group.ALLOW_CHILDREN_READ);
    switch_.setCapability(Group.ALLOW_CHILDREN_WRITE);
    switch_.setCapability(Switch.ALLOW_SWITCH_WRITE);
    switch_.setCapability(Switch.ALLOW_SWITCH_READ);
    switch_.setCapability(ALLOW_DETACH);
    tg_.addChild(switch_);
  }

  /**
   * ajoute un objet au groupe.
   * 
   * @param _objet l'objet � ajouter
   */
  @Override
  public void add(final Objet3DInterface _objet) {
    if (_objet instanceof BVolume) {
      final BVolume volume = (BVolume) _objet;
      if (!volume.isCompiled()) {
        volume.compile();
      }
      switch_.addChild(volume);
      volume.setIndex(switch_.numChildren() - 1);
      volume.setPere(this);
      setBoite(getBoite());
    }
  }

  /**
   * rend un objet du groupe visible ou pas.
   */
  public void changeVisible(final int _index, final boolean _b) {
    final BitSet bs = switch_.getChildMask();
    bs.set(_index, _b);
    switch_.setChildMask(bs);
  }

  /**
   * Renvoie la boite englobant tous les objets du groupe.
   */
  public GrBoite getBoite() {
    GrBoite boite = null;
    final GrBoite tmp = new GrBoite();
    // =new GrBoite();
    // boite.ajuste(new GrPoint());
    final Enumeration enfants = switch_.getAllChildren();
    for (final Enumeration e = enfants; e.hasMoreElements();) {
      final Object element = e.nextElement();
      if (element instanceof BVolume) {
        final BVolume volume = ((BVolume) element);
        if (volume.getBoite(tmp) != null) {
          if (boite == null) {
            boite = new GrBoite(tmp);
          } else {
            boite.ajuste(tmp);
          }
        }
        // if (element instanceof BGroupeVolume) boite=boite.union(((BGroupeVolume)element).getBoite());
      }
    }
    return boite;
  }

  /**
   * @return les enfants du groupe (sur un niveau)
   */
  @Override
  public List getEnfants() {
    return getTousEnfants();
  }

  /**
   * @return le nombre d'enfants direct du groupe (sur 1 niveau)
   */
  @Override
  public int getNumChildren() {
    return getTousEnfants().size();
  }

  /**
   * @return tous les enfants du groupe (descend tous les niveaux)
   */
  @Override
  public List getTousEnfants() {
    final Vector v = new Vector();
    final Enumeration enfants = switch_.getAllChildren();
    for (final Enumeration e = enfants; e.hasMoreElements();) {
      final Object element = e.nextElement();
      if (element instanceof BVolume) {
        v.addElement(element);
      } else if (element instanceof BGroupeVolume) {
        v.addAll(((BGroupeVolume) element).getTousEnfants());
      }
    }
    return v;
  }

  /**
   * Supprime l'objet du groupe.
   * 
   * @param index:l'index de l'objet
   */
  @Override
  public void remove(final int _index) {
    switch_.removeChild(_index);
    final List enfants = getTousEnfants();
    for (int i = _index; i < getNumChildren(); i++) {
      final Objet3DInterface o = (Objet3DInterface) enfants.get(i);
      o.setIndex(i);
    }
    final BitSet bs = switch_.getChildMask();
    for (int i = _index + 1; i < bs.length(); i++) {
      if (bs.get(i)) {
        bs.set(i - 1);
      }
    }
    switch_.setChildMask(bs);
  }

  /**
   * Affiche tous les objets.
   */
  public void setAllVisible() {
    final int nb = switch_.numChildren();
    final BitSet bs = new BitSet();
    for (int i = 0; i < nb; i++) {
      bs.set(i);
    }
    switch_.setChildMask(bs);
  }

  /**
   * Affiche ou non les objets sous forme rapide (boite...).
   * 
   * @param _rapide true: le mode d'affichage rapide est selection�
   */
  @Override
  public void setRapide(final boolean _rapide) {
    final List v = getTousEnfants();
    for (int i = 0; i < v.size(); i++) {
      ((BVolume) v.get(i)).setRapide(_rapide);
    }
    rapide_ = _rapide;
  }

  /**
   * Affiche ou non le groupe.
   * 
   * @param _visible true: les objets sont visibles, false: non.
   */
  @Override
  public void setVisible(final boolean _visible) {
    final List v = getTousEnfants();
    for (int i = 0; i < v.size(); i++) {
      ((BVolume) v.get(i)).setVisible(_visible);
    }
    visible_ = _visible;
    if (pere_ instanceof BGroupeVolume) {
      ((BGroupeVolume) pere_).changeVisible(index_, _visible);
    }
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getProperty(final String _key) {
    return null;
  }
}
