/*
 * @creation     1999-11-29
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Label;
import java.awt.image.BufferedImage;

import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Shape3D;
import javax.vecmath.Point3f;

import org.fudaa.ebli.controle.BSelecteurColorChooser;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Volume qui affiche un texte 2D dans l'espace .
 * 
 * @version $Revision: 1.16 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BEtiquette extends BVolume {
  // la chaine � afficher

  // la couleur du texte
  private Color couleur_;

  // la police du texte

  @Override
  public Object getProperty(final String _key) {
    return _key == BSelecteurColorChooser.DEFAULT_PROPERTY ? couleur_ : null;
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  /**
   * Constructeur de BEtiquette. construit une etiquette, la remplit d'une couleur, ecrit une chaine d'une autre
   * couleur, et l'ancre au point d�sign�
   * 
   * @param _point Point d'ancrage
   * @param _fond Couleur de remplissage de l'etiquette
   * @param _couleur couleur du texte
   */
  public BEtiquette(final String _texte, final Point3f _point, final Color _fond, final Color _couleur,
      final Font _fonte) {
    // on cree le volume, avec pour nom "Etiquette" suivi du texte
    super("Etiquette :" + _texte);
    // on met le point � l'�chelle
    /*
     * _point.x= _point.x / ParametresVolumes.ECHELLE; _point.y= _point.y / ParametresVolumes.ECHELLE; _point.z=
     * _point.z / ParametresVolumes.ECHELLE;
     */
    // on construit un nouveau Shape3D, dont on pourra modifier l'apparence
    // le point d'ancrage (haut-gauche) de l'etiquette
    // private Point3f ancre_;
    // la surface � afficher
    // la couleur du fond
    Shape3D shape = new Shape3D();
    shape.setCapability(Shape3D.ALLOW_APPEARANCE_READ);
    shape.setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
    // on initialise les variables membres
    String texte = _texte;
    // ancre_= _point;
    Color fond = _fond;
    couleur_ = _couleur;
    Font fonte = _fonte;
    final FontMetrics fm = new Label().getFontMetrics(fonte);
    final int hu = fm.getAscent() + fm.getDescent();
    final int wu = fm.stringWidth(texte) + 15;
    final BufferedImage image = new BufferedImage(wu, hu, BufferedImage.TYPE_4BYTE_ABGR);
    final Graphics2D g2d = image.createGraphics();
    g2d.setBackground(fond);
    g2d.clearRect(0, 0, wu, hu);
    g2d.setPaint(couleur_);
    g2d.drawString(texte, 5, 10);
    final ImageComponent2D image2 = new ImageComponent2D(ImageComponent.FORMAT_RGBA, image);
    final javax.media.j3d.Raster raster = new javax.media.j3d.Raster();
    raster.setType(javax.media.j3d.Raster.RASTER_COLOR);
    raster.setPosition(_point);
    raster.setSize(image2.getWidth(), image2.getHeight());
    raster.setImage(image2);
    shape.setGeometry(raster);
    getTransformGroup().addChild(shape);
    setBoite(new GrPoint(_point.x, _point.y, _point.z), new GrPoint(_point.x + .001, _point.y + .001, _point.z + .001));
    // on remet le point � l'�chelle
    /*
     * _point.x= _point.x * ParametresVolumes.ECHELLE; _point.y= _point.y * ParametresVolumes.ECHELLE; _point.z=
     * _point.z * ParametresVolumes.ECHELLE;
     */
  }

  /**
   * Constructeur de BEtiquette. construit une etiquette, la remplit en noir, ecrit une chaine d'une autre couleur, et
   * l'ancre au point d�sign�
   * 
   * @param _texte Chaine � afficher
   * @param _point Point d'ancrage
   * @param _couleur couleur du texte
   */
  public BEtiquette(final String _texte, final Point3f _point, final Color _couleur) {
    this(_texte, _point, Color.black, _couleur, new Font("SansSerif", Font.PLAIN, 10));
  }

  /**
   * methode de la l'interface Objet3DInterface. ne fait rien
   */
  @Override
  public void setRapide(final boolean _rapide) {}
}
