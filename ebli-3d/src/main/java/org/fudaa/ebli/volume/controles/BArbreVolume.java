/*
 * @creation     1999-10-27
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.volume.controles;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPopupMenu;

import org.fudaa.ebli.commun.EbliPopupListener;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.volume.common.BGrilleAbstract;
import org.fudaa.ebli.volume.common.BVolumeAbstract;
import org.fudaa.ebli.volume.common.GroupeInterface;
import org.fudaa.ebli.volume.common.Objet3DInterface;

/**
 * @version $Revision: 1.18 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Christophe Delhorbe
 */
public class BArbreVolume extends JTree implements PropertyChangeListener {
  public BArbreVolume() {
    setRequestFocusEnabled(false);
    setShowsRootHandles(true);
    setBorder(new EmptyBorder(5, 5, 5, 5));
    setCellRenderer(new DefaultTreeCellRenderer());
    addMouseListener(new PopupListener());
    getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
  }

  class PopupListener extends EbliPopupListener {

    @Override
    public void popup(final Component _src, final int _x, final int _y) {
      final int xe = _x;
      int ye = _y;
      final int selRow = getRowForLocation(xe, ye);
      if (selRow != -1) {
        final TreePath selPath = getPathForLocation(xe, ye);
        final Object selObject = selPath.getLastPathComponent();
        final BArbrePopupMenu menu = buildPopupMenu((Objet3DInterface) selObject);
        // xe=20-menu.getSize().width;
        ye -= 5;
        menu.show(_src, xe, ye);
      }

    }

  }

  static String getDelete() {
    return "DETRUIRE";
  }

  public void setVolume(final Objet3DInterface _volume) {
    setModel(new BArbreVolumeModel(_volume));
  }

  /**
   * Retourne le volume racine de l'arbre.
   */
  public Objet3DInterface getVolume() {
    return (Objet3DInterface) getModel().getRoot();
  }

  /**
   * Met l'arbre � jour (apr�s ajout ou suppression de volumes).
   */
  public void refresh() {
    setVolume(getVolume());
    repaint();
  }

  @Override
  public String convertValueToText(final Object _value, final boolean _selected, final boolean _expanded,
                                   final boolean _leaf, final int _row, final boolean _hasFocus) {
    if (_value instanceof Objet3DInterface) {
      return ((Objet3DInterface) _value).getName();
    }
    return super.convertValueToText(_value, _selected, _expanded, _leaf, _row, _hasFocus);
  }

  public void propertyChangedForSelectedLayer() {
    final TreePath path = getSelectionPath();
    if (path != null) {
      ((BArbreVolumeModel) getModel()).propertyChange(path);
    }
  }

  /**
   * Renvoie le calque selectionne.
   */
  public Objet3DInterface[] getSelection() {
    Objet3DInterface[] r = null;
    final TreePath[] path = getSelectionPaths();
    if (path == null) {
      r = new Objet3DInterface[0];
    } else {
      final int l = path.length;
      r = new Objet3DInterface[l];
      for (int i = 0; i < l; i++) {
        r[i] = (Objet3DInterface) path[i].getLastPathComponent();
      }
    }
    return r;
  }

  /**
   * Reception des <I>PropertyChangeEvent</I>. Repercute les evenements au calque selectionne.
   */
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    final Objet3DInterface[] v = getSelection();
    for (int i = 0; i < v.length; i++) {
      v[i].setProperty(_evt.getPropertyName(), _evt.getNewValue());
    }
    if (_evt.getPropertyName().equals("nouvelObjet")) {
      refresh();
    }
    repaint();
  }

  public BArbreNormalMenu buildNormalMenu() {
    return new BArbreNormalMenu(this);
  }

  public BArbrePopupMenu buildPopupMenu(final Objet3DInterface _volume) {
    return new BArbrePopupMenu(this, _volume);
  }

}

class BArbreNormalMenu extends BuMenu {
  private final BArbreVolume arbre_;

  /**
   * Constructeur.
   * 
   * @param _arbre l'arbre de volumes
   */
  BArbreNormalMenu(final BArbreVolume _arbre) {
    super(EbliResource.EBLI.getString("Volumes"), "VOLUMES");
    arbre_ = _arbre;
    addMenuItem(EbliResource.EBLI.getString("Visible"), "VISIBLE_OUI", null, true);
    addMenuItem(EbliResource.EBLI.getString("Cach�"), "VISIBLE_NON", null, true);
    /*
     * addSeparator(); addMenuItem(EbliResource.EBLI.getString("Att�nu�"), "ATTENUE_OUI",null,true);
     * addMenuItem(EbliResource.EBLI.getString("Normal"), "ATTENUE_NON",null,true);
     */
    addSeparator();
    addMenuItem(EbliResource.EBLI.getString("Rapide"), "RAPIDE_OUI", null, true);
    addMenuItem(EbliResource.EBLI.getString("Normal"), "RAPIDE_NON", null, true);
    addSeparator();
    addMenuItem(EbliResource.EBLI.getString("D�truire"), BArbreVolume.getDelete(), null, true);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final Objet3DInterface[] v = arbre_.getSelection();
    if (v.length == 0) {
      return;
    }
    final String action = _evt.getActionCommand();
    if (action.equals(BArbreVolume.getDelete())) {
      for (int i = 0; i < v.length; i++) {
        v[i].detruire();
      }
      arbre_.refresh();
    } else {
      int i = action.indexOf('_');
      if (i >= 0) {
        final String propriete = action.substring(0, i).toLowerCase();
        final boolean valeur = action.endsWith("OUI");
        // System.err.println(">"+propriete+"="+valeur);
        for (i = 0; i < v.length; i++) {
          v[i].setProperty(propriete, Boolean.valueOf(valeur));
        }
      }
    }
    arbre_.repaint();
  }
}

/**
 * Menu contextuel pour une cellule de l'arbre. Il permet de regler les proprietes du calque selectionne (rapide,
 * attenue, gele, ...)
 */
class BArbrePopupMenu extends BuPopupMenu {
  private Objet3DInterface volume_;
  private BArbreVolume arbre_;

  /**
   * Constructeur.
   * 
   * @param _arbre l'arbre de volumes
   * @param _volume le volume selectionne
   */
  BArbrePopupMenu(final BArbreVolume _arbre, final Objet3DInterface _volume) {
    super();
    arbre_ = _arbre;
    volume_ = _volume;
    // addMenuItem(EbliResource.EBLI.getString("Att�nu�"),
    // "ATTENUE",true,volume_.isAttenue());
    addMenuItem(EbliResource.EBLI.getString("Visible"), "VISIBLE", true, volume_.isVisible());
    if (volume_ instanceof BVolumeAbstract) {
      addMenuItem(EbliResource.EBLI.getString("Rapide"), "RAPIDE", volume_.isRapideEditable(), volume_.isRapide());
    }
    if (volume_ instanceof BGrilleAbstract) {
      addMenuItem(EbliResource.EBLI.getString("Filaire"), "FILAIRE", volume_.isFilaireEditable(),
          ((BGrilleAbstract) volume_).isFilaire());
    }
    if (volume_.isDestructible()) {
      addSeparator();
      addMenuItem(EbliResource.EBLI.getString("D�truire"), BArbreVolume.getDelete(), null, true);
    }
    setSize(getPreferredSize());
    // setDefaultLightWeightPopupEnabled(false);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    // System.err.println("ACTION="+action);
    // if(action.equals("ATTENUE"))
    // { ca.setAttenue(!volume_.isAttenue()); arbre_.repaint(); }
    // else
    if ("RAPIDE".equals(action)) {
      volume_.setRapide(!volume_.isRapide());
      arbre_.repaint();
    }
    if ("VISIBLE".equals(action)) {
      volume_.setVisible(!volume_.isVisible());
      arbre_.repaint();
    }
    if ("FILAIRE".equals(action)) {
      ((BGrilleAbstract) volume_).setFilaire(!((BGrilleAbstract) volume_).isFilaire());
      arbre_.repaint();
    }
    if (BArbreVolume.getDelete().equals(action)) {
      volume_.detruire();
      arbre_.refresh();
    }
  }

  // Menu Item
  public final JMenuItem addMenuItem(final String _s, final String _cmd, final boolean _enabled, final boolean _value) {
    // BuIcon icon=BuLib.loadCommandIcon(_cmd);
    final JCheckBoxMenuItem r = new JCheckBoxMenuItem();
    r.setName("cb" + _cmd);
    r.setActionCommand(_cmd);
    r.setText(_s);
    // r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setSelected(_value);
    this.add(r);
    return r;
  }

  public final JMenuItem addMenuItem(final String _s, final String _cmd, final BuIcon _icon, final boolean _enabled) {
    final JMenuItem r = new JMenuItem();
    r.setName("mi" + _cmd);
    r.setActionCommand(_cmd);
    r.setText(_s);
    // r.setIcon(_icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    this.add(r);
    return r;
  }
}

/**
 * Modele de donnees pour l'arbre de volumes.
 */
class BArbreVolumeModel implements TreeModel {
  private final List listeners_;
  private final Objet3DInterface volume_;

  /**
   * Constructeur.
   * 
   * @param _volume volume racine de l'arbre (GroupeVolume de l'Univers)
   */
  public BArbreVolumeModel(final Objet3DInterface _volume) {
    listeners_ = new ArrayList();
    volume_ = _volume;
  }

  public void propertyChange(final TreePath _path) {
    fireTreeModelEvent(new TreeModelEvent(this, _path));
  }

  private void fireTreeModelEvent(final TreeModelEvent _evt) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((TreeModelListener) it.next()).treeNodesChanged(_evt);
      }
    }
  }

  /**
   * Nombre de fils du volume <I>_parent</I>.
   */
  @Override
  public int getChildCount(final Object _parent) {
    int r = 0;
    if (_parent instanceof GroupeInterface) {
      final List v = ((GroupeInterface) _parent).getEnfants();
      r = v.size();
    }
    return r;
  }

  /**
   * Renvoie le fils numero <I>_index</I> du volume <I>_parent</I>.
   */
  @Override
  public Object getChild(final Object _parent, final int _index) {
    Object r = null;
    if (_parent instanceof GroupeInterface) {
      final List v = ((GroupeInterface) _parent).getEnfants();
      r = v.get(_index);
    }
    return r;
  }

  /**
   * Indice du fils <I>_child</I> du calque <I>_parent</I>.
   */
  @Override
  public int getIndexOfChild(final Object _parent, final Object _child) {
    int r = -1;
    if (_parent instanceof GroupeInterface) {
      final List v = ((GroupeInterface) _parent).getEnfants();
      for (int i = 0; i < v.size(); i++) {
        if (v.get(i) == _child) {
          r = i;
        }
      }
    }
    return r;
  }

  /**
   * Volume racine de l'arbre.
   */
  @Override
  public Object getRoot() {
    return volume_;
  }

  /**
   * Teste si le volume <I>_node</I> est une feuille.
   */
  @Override
  public boolean isLeaf(final Object _node) {
    boolean r = true;
    if (_node instanceof GroupeInterface) {
      final List v = ((GroupeInterface) _node).getEnfants();
      r = (v.size() == 0);
    }
    return r;
  }

  @Override
  public void addTreeModelListener(final TreeModelListener _l) {
    listeners_.add(_l);
  }

  @Override
  public void removeTreeModelListener(final TreeModelListener _l) {
    listeners_.remove(_l);
  }

  @Override
  public void valueForPathChanged(final TreePath _path, final Object _newValue) {}
}
