/*
 *  @file         DodicoPermanentTreeMap.java
 *  @creation     17 nov. 2003
 *  @modification $Date: 2006-07-13 13:34:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
/**
 * @author deniger
 * @version $Id: CtuluPermanentTreeMap.java,v 1.3 2006-07-13 13:34:36 deniger Exp $
 */
public class CtuluPermanentTreeMap extends TreeMap {
  /**
   * @param _m la map initialisatrice
   */
  public CtuluPermanentTreeMap(final Map _m) {
    final Iterator it= _m.entrySet().iterator();
    while (it.hasNext()) {
      final Map.Entry e= (Map.Entry)it.next();
      super.put(e.getKey(), e.getValue());
    }
  }
  @Override
  public Object put(final Object _key, final Object _value) {
    throw new UnsupportedOperationException();
  }
  @Override
  public void putAll(final Map _map) {
    throw new UnsupportedOperationException();
  }
  @Override
  public Object remove(final Object _key) {
    throw new UnsupportedOperationException();
  }
  @Override
  public void clear() {
    super.clear();
  }
  @Override
  public SortedMap subMap(final Object _fromKey, final Object _toKey) {
    return new CtuluPermanentTreeMap(super.subMap(_fromKey, _toKey));
  }
  @Override
  public SortedMap tailMap(final Object _fromKey) {
    return new CtuluPermanentTreeMap(super.tailMap(_fromKey));
  }
  @Override
  public SortedMap headMap(final Object _toKey) {
    return new CtuluPermanentTreeMap(super.headMap(_toKey));
  }
}
