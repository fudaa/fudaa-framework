package org.fudaa.ctulu;

import java.awt.Dimension;

/**
 * @author deniger
 */
public class CtuluRatio {

  private double xOverY;

  public CtuluRatio(double w, double h) {
    assert h != 0;
    xOverY = w / h;
  }

  public CtuluRatio(int w, int h) {
    this((double) w, (double) h);
  }

  public CtuluRatio(Dimension d) {
    this(d.getWidth(), d.getHeight());
  }

  public double toX(double y) {
    return y * xOverY;
  }

  /**
   * @param x
   * @return x valu
   */
  public double toY(double x) {
    return x / xOverY;
  }

  public int toY(int x) {
    return (int) toY((double) x);
  }

  public int toX(int y) {
    return (int) toX((double) y);
  }

}
