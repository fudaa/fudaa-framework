/**
 * @creation 17 juin 2003
 * @modification $Date: 2006-09-19 14:36:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ctulu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * @author deniger
 * @version $Id: CtuluPermanentList.java,v 1.10 2006-09-19 14:36:54 deniger Exp $
 */
public class CtuluPermanentList extends ArrayList {

  /**
   * @param _collection la collection pour initialiser le tout
   */
  public CtuluPermanentList(final Collection _collection) {
    super(_collection);
    super.trimToSize();
  }

  /**
   * @param _o les objets a ajouter
   */
  public CtuluPermanentList(final Object[] _o) {
    this(Arrays.asList(_o));
  }

  @Override
  public void add(final int _index, final Object _element) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean add(final Object _o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(final Collection _c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(final int _index, final Collection _c) {
    throw new UnsupportedOperationException();
  }

  protected void superClear() {
    super.clear();
  }

  protected void superAddAll(final Collection _c) {
    super.addAll(_c);
  }

  /**
   * @see java.util.Collection#clear()
   */
  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Object remove(final int _index) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void removeRange(final int _fromIndex, final int _toIndex) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Object set(final int _index, final Object _element) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void trimToSize() {
    throw new UnsupportedOperationException();
  }

   class DodicoComboModel extends AbstractListModel implements ComboBoxModel {

    Object select_;
    
    public DodicoComboModel(){
    }

    @Override
    public Object getSelectedItem() {
      return select_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != select_) {
        select_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    @Override
    public Object getElementAt(final int _idx) {
      return CtuluPermanentList.this.get(_idx);
    }

    @Override
    public int getSize() {
      return CtuluPermanentList.this.size();
    }

  }

  private class DodicoComboModelWithFirst extends DodicoComboModel {

    Object first_;

    DodicoComboModelWithFirst(final Object _first) {
      first_ = _first;
    }

    @Override
    public Object getElementAt(final int _arg0) {
      if (_arg0 == 0) {
        return first_;
      }
      return CtuluPermanentList.this.get(_arg0 - 1);
    }

    @Override
    public int getSize() {
      return CtuluPermanentList.this.size() + 1;
    }

  }

  /**
   * @return un combobox model a jour
   */
  public ComboBoxModel createComboBoxModel() {
    return new DodicoComboModel();
  }

  /**
   * @param _first le premier objet du model
   * @return un model pour les combobox.
   */
  public ComboBoxModel createComboBoxModel(final Object _first) {
    return new DodicoComboModelWithFirst(_first);
  }
}