/**
 * @creation 21 oct. 2003
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import java.util.ArrayList;
import java.util.List;
/**
 * Classe contenant plusieurs commandes. Les actions undo,redo sont transferees aux commandes stockees.
 * L'action undo se fait dans l'ordre derniere commande -> premiere commande
 *
 * @author deniger
 * @version $Id$
 */
public class CtuluCommandComposite implements CtuluNamedCommand,CtuluCommandContainer {

  List command_;
  final String name_;

  public CtuluCommandComposite() {
    this(null);
  }

  public CtuluCommandComposite(String _name) {
    super();
    name_=_name;
  }

  /**
   * @return false
   */
  protected boolean isActionToDoAfterUndoOrRedo(){
    return false;
  }

  /**
   *  Vide: pas d'action a effectuer apres les operations d'undo/redo.
   */
  protected void actionToDoAfterUndoOrRedo(){

  }

  /**
   * @return true si ne contient aucune commande
   */
  public  final boolean isEmpty() {
    return command_ == null || command_.size() == 0;
  }

  /**
   * @return la commande simplifiee: si elle ne contient qu'une seule commande seule celle ci est
   * renvoyee. Sinon renvoie this.
   */
  public  final CtuluCommand getSimplify() {
    if(isActionToDoAfterUndoOrRedo()) {
      return this;
    }
    final int n = getNbCmd();
    if (n == 0) {
      return this;
// B.M. 31/12/2008 - Suppression de de cette simplification, sinon perte du nom de commande.
//    } else if (n == 1) {
//      return (CtuluCommand) command_.get(0);
    } else {
      return this;
    }

  }

  /**
   * @param _cmd la commande a ajoutee si non nulle.
   * @return true si _cmd non nulle (et donc ajout�e)
   */
  @Override
  public final  boolean addCmd(final CtuluCommand _cmd) {
    if (_cmd != null) {
      if (command_ == null) {
        command_ = new ArrayList(10);
      }
      command_.add(_cmd);
      return true;
    }
    return false;
  }

  /**
   * @param _cmd la commande a enlever
   */
  public final  void removeCmd(final CtuluCommand _cmd) {
    if ((command_ != null) && (_cmd != null)) {
      command_.remove(_cmd);
    }
  }

  /**
   * @return le nombre de commande.
   */
  public final  int getNbCmd() {
    return command_ == null ? 0 : command_.size();
  }

  /**
   * Appelle undo de la premiere � la derniere commande.
   */
  @Override
  public void undo() {
    if (command_ != null) {
      for (int i = command_.size()-1; i >=0; i--) {
        ((CtuluCommand) command_.get(i)).undo();
      }
    }
    actionToDoAfterUndoOrRedo();
  }

  /**
   * Appelle redo de la premiere � la derniere commande.
   */
  @Override
  public  void redo() {
    if (command_ != null) {
      final   int n = command_.size();
      for (int i = 0; i < n; i++) {
        ((CtuluCommand) command_.get(i)).redo();
      }
    }
    actionToDoAfterUndoOrRedo();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.CtuluNamedCommand#getName()
   */
  @Override
  public String getName() {
    return name_;
  }
}