/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * Une classe permettant de gerer un vecteur de double. en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 *
 * @author Fred Deniger
 * @version $Id: CtuluCollectionAbstract.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public abstract class CtuluCollectionAbstract implements CtuluCollection {

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  protected CtuluCollectionAbstract() {}

  protected void fireObjectChanged(int _indexGeom, Object _newValue) {}

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public Object[] getObjectValues() {
    final Object[] r = new Object[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getObjectValueAt(i);
    }
    return r;
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {}

}
