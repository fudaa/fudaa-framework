/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double. en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 * 
 * @author Fred Deniger
 * @version $Id: CtuluArrayObject.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluArrayObject extends CtuluCollectionObjectAbstract {

  protected Object[] list_;

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  public CtuluArrayObject() {
    this(0);
  }

  /**
   * @param _nb taille initiales
   */
  public CtuluArrayObject(final int _nb) {
    list_ = new Object[_nb];
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayObject(final Object[] _init) {
    list_ = CtuluLibArray.copy(_init);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue) {
  }

  @Override
  protected boolean internalSet(final int _i, final Object _newV) {
    if (list_[_i] != _newV) {
      list_[_i] = _newV;
      return true;
    }
    return false;
  }

  @Override
  public Object[] getObjectValues() {
    return CtuluLibArray.copy(list_);
  }

  @Override
  public final int getSize() {
    return list_.length;
  }

  @Override
  public final Object getValueAt(final int _i) {
    return list_[_i];
  }

  /**
   * @param _o l'objet a tester
   * @return l'indice de l'objet. -1 si non trouve.
   */
  @Override
  public int indexOf(final Object _o) {
    return CtuluLibArray.findObject(list_, _o);
  }

  /**
   * @param _array la liste a recupere
   * @param _fireEvent true si on doit envoyer un evt
   */
  @Override
  public void initWith(final CtuluCollection _array, final boolean _fireEvent) {
    list_ = _array.getObjectValues();
    if (_fireEvent) {
      fireObjectChanged(-1, null);
    }
  }

  /**
   * @param _array la liste a recupere
   * @param _fireEvent true si on doit envoyer un evt
   */
  public void initWith(final CtuluArrayObject _array, final boolean _fireEvent) {
    list_ = _array.getObjectValues();
    if (_fireEvent) {
      fireObjectChanged(-1, null);
    }
  }

  /**
   * @param _array la liste a recupere
   * @param _fireEvent true si on doit envoyer un evt
   */
  public void initWith(final CtuluListObject _array, final boolean _fireEvent) {
    list_ = _array.getObjectValues();
    if (_fireEvent) {
      fireObjectChanged(-1, null);
    }

  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean isAllSameValue() {
    final Object r = getValueAt(0);
    for (int i = getSize() - 1; i > 0; i--) {
      final Object ri = getValueAt(i);
      if (r == null) {
        if (ri != null) {
          return false;
          // r n'est pas null
        }
      } else if (!r.equals(ri)) {
        return false;
      }

    }
    return true;
  }

  @Override
  public boolean isSameValues(final int[] _idx) {
    if (_idx == null) {
      return isAllSameValue();
    }
    if (_idx.length == 0) {
      return false;
    }
    final Object object = getValueAt(_idx[0]);
    for (int i = _idx.length - 1; i > 0; i--) {
      final Object ri = getValueAt(_idx[i]);
      if (object == null) {
        if (ri != null) {
          return false;
          // r n'est pas null
        }
      } else if (!object.equals(ri)) {
        return false;
      }

    }
    return true;
  }

}
