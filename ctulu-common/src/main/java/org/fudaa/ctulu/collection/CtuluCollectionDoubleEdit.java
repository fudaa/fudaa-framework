/*
 *  @creation     24 sept. 2004
 *  @modification $Date: 2007-03-23 17:16:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluRange;

/**
 * @author Fred Deniger
 * @version $Id: CtuluCollectionDoubleEdit.java,v 1.2 2007-03-23 17:16:17 deniger Exp $
 */
public interface CtuluCollectionDoubleEdit extends CtuluCollectionDouble,CtuluCollection {

  boolean set(int[] _i,double[] _newV,CtuluCommandContainer _c);
  boolean setAll(double[] _newV,CtuluCommandContainer _c);

  boolean set(int[] _i,double _newV,CtuluCommandContainer _c);
  boolean set(int _i,double _newV,CtuluCommandContainer _c);


  void initWithDouble(CtuluCollectionDouble _m,boolean _throwEvent);
  @Override
  void getRange(CtuluRange _r);


}