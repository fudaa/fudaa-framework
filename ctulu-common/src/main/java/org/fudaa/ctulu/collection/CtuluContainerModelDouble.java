/*
 * @creation 4 oct. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;


/**
 * @author Fred Deniger
 * @version $Id: CtuluContainerModelDouble.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public interface CtuluContainerModelDouble {

  /**
   * @return le nombre de modele a prendre en compte
   */
  int getNbValues();

  /**
   * @param _idx l'indice du model
   * @return le modele correspondant
   */
  CtuluCollectionDoubleEdit getDoubleValues(int _idx);
  /**
   * @param _i l'indice du model
   * @return l'identifiant du modele
   */
  Object getValueIdentifier(int _i);
}