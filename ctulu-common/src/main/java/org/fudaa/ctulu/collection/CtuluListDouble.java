/*
 * @creation 22 sept. 2004
 * 
 * @modification $Date: 2007-01-10 08:58:48 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import gnu.trove.TDoubleArrayList;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRange;

/**
 * Une classe permettant de gerer un vecteur de double. Fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 *
 * @author Fred Deniger
 * @version $Id: CtuluListDouble.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluListDouble extends CtuluCollectionDoubleEditAbstract implements CtuluCollection, CtuluCollectionDoubleEdit {

  protected class CommandAdd implements CtuluCommand {

    int idx_;

    double newValue_;

    protected CommandAdd(final int _idx, final double _newV) {
      idx_ = _idx;
      newValue_ = _newV;
    }

    @Override
    public void redo() {

      if (idx_ == list_.size()) {
        add(newValue_);
      } else {
        insert(idx_, newValue_);
      }
    }

    @Override
    public void undo() {
      remove(idx_);

    }

  }

  protected class CommandAddAll implements CtuluCommand {

    int idxBefore_;

    double[] newValue_;

    protected CommandAddAll(final int _idx, final double[] _newV) {
      idxBefore_ = _idx;
      newValue_ = _newV;
    }

    @Override
    public void redo() {
      list_.add(newValue_);
      r_ = null;
      fireDoubleAdded();
    }

    @Override
    public void undo() {
      list_.remove(idxBefore_, newValue_.length);
      r_ = null;
      fireDoubleRemoved();

    }

  }

  protected class CommandRemove implements CtuluCommand {

    int idx_;

    double oldValue_;

    protected CommandRemove(final int _idx, final double _newV) {
      idx_ = _idx;
      oldValue_ = _newV;
    }

    /**
     * @overrride
     */
    @Override
    public void redo() {
      remove(idx_);
    }

    /**
     * @overrride
     */
    @Override
    public void undo() {
      insert(idx_, oldValue_);
    }

  }

  protected class CommandRemoveAll implements CtuluCommand {

    CtuluListDouble memento_;

    protected CommandRemoveAll(final CtuluListDouble _m) {
      memento_ = _m;
    }

    @Override
    public void redo() {
      removeAll();
    }

    @Override
    public void undo() {
      internInitWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireDoubleAdded();
    }
  }

  protected class CommandRemoveDangerous implements CtuluCommand {

    int[] idx_;

    double[] oldValues_;

    protected CommandRemoveDangerous(final double[] _oldValues, final int[] _idx) {
      oldValues_ = _oldValues;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      insertDangerous(idx_, oldValues_);
    }
  }

  protected class CommandRemoveMemento implements CtuluCommand {

    int[] idx_;

    CtuluListDouble memento_;

    protected CommandRemoveMemento(final CtuluListDouble _m, final int[] _idx) {
      memento_ = _m;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      internInitWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireDoubleAdded();
    }
  }

  protected CDoubleArrayList list_;

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  public CtuluListDouble() {
    this(0, 20);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListDouble(final CtuluListDouble _init) {
    this(0, _init.getSize());
    internInitWith(_init, false);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListDouble(final double[] _init) {
    list_ = new CDoubleArrayList(_init);
  }

  /**
   * Visite de i=0 a i=nbElement.
   */
  @Override
  public void iterate(final CtuluDoubleVisitor _visitor) {
    final int n = getSize();
    for (int i = 0; i < n; i++) {
      if (!_visitor.accept(i, list_.getQuick(i))) {
        return;
      }
    }

  }

  @Override
  public Double getCommonValue(final int[] _i) {
    return CtuluCollectionDoubleAbstract.getCommonValue(this, _i);
  }

  public CtuluListDouble(final int _initCapacity) {
    list_ = new CDoubleArrayList(_initCapacity);
  }

  /**
   * Pour creer un vecteur vide et ayant une capacite de 100 new(0,100). Pour creer un vecteur avec les 10 premiers double a 0 et une capacite de 30
   * new(20,30)
   *
   * @param _nb le nombre initial de double ajoute au vecteur
   * @param _initCapacity la capacite du veceur
   */
  public CtuluListDouble(final int _nb, final int _initCapacity) {
    int initCapacity = _initCapacity;
    if (initCapacity < _nb) {
      initCapacity = _nb + 2;
    }
    list_ = new CDoubleArrayList(initCapacity);
    list_.add(new double[_nb]);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListDouble(final TDoubleArrayList _init) {
    this(_init.toNativeArray());
  }

  protected void fireDoubleAdded() {

  }

  protected void fireDoubleRemoved() {
  }

  /**
   * Methode appele lorsque toutes les valeurs ont ete modifiee. Les methodes qui lancent cet evt sont initFrom.
   */
  protected void fireDoubleStructureChanged() {

  }

  protected void internalAdd(final double _v) {
    list_.add(_v);
    if (r_ != null && !r_.isValueContained(_v)) {
      if (_v < r_.min_) {
        r_.min_ = _v;
      }
      if (_v > r_.max_) {
        r_.max_ = _v;
      }
    }
  }

  protected void internalAdd(final int _i, final double _v) {
    if (_i == list_.size()) {
      list_.add(_v);
    } else {
      list_.insert(_i, _v);
    }
    if (r_ != null && !r_.isValueContained(_v)) {
      if (_v < r_.min_) {
        r_.min_ = _v;
      }
      if (_v > r_.max_) {
        r_.max_ = _v;
      }
    }
  }

  protected void internalRemove(final int _i) {
    final double xRemoved = list_.remove(_i);
    if (r_ != null && !r_.isValueStrictlyContained(xRemoved)) {
      r_ = null;
    }
  }

  @Override
  protected void internalSetValue(final int _i, final double _newV) {
    list_.set(_i, _newV);

  }

  @Override
  public void expandTo(final CtuluRange _rangeToExpand) {
    if (_rangeToExpand != null) {
      _rangeToExpand.expandTo(getMax());
      _rangeToExpand.expandTo(getMin());
    }

  }

  final void internInitWith(final CtuluArrayDouble _m, final boolean _throwEvent) {
    list_ = new CDoubleArrayList(_m.list_.length);
    list_.add(_m.list_);
    if (_m.r_ == null) {
      r_ = null;
    } else {
      if (r_ == null) {
        r_ = new CtuluRange(_m.r_);
      } else {
        r_.initWith(_m.r_);
      }
    }
    if (_throwEvent) {
      fireDoubleStructureChanged();
    }
  }

  final void internInitWith(final CtuluListDouble _m, final boolean _throwEvent) {
    list_ = new CDoubleArrayList(_m.list_.size());
    list_.add(_m.list_.toNativeArray());
    if (_m.r_ == null) {
      r_ = null;
    } else {
      if (r_ == null) {
        r_ = new CtuluRange(_m.r_);
      } else {
        r_.initWith(_m.r_);
      }
    }
    if (_throwEvent) {
      fireDoubleStructureChanged();
    }
  }

  /**
   * Fonction a utiliser avec pr�caution: effectue des insert des values en suivant les valeurs dans l'ordre. Pour fonctionner les points
   *
   * @param _idx les indices dans l'ordre
   * @param _values les valeurs dans l'ordre
   * @return true
   */
  final boolean insertDangerous(final int[] _idx, final double[] _values) {
    if (_idx == null || _values == null || _idx.length == 0 || _values.length != _idx.length) {
      return false;
    }
    final int n = _idx.length;
    for (int i = 0; i < n; i++) {
      insert(_idx[i], _values[i]);
    }
    fireDoubleAdded();
    return true;
  }

  public final boolean add(final double _x) {
    return add(_x, null);
  }

  public final boolean add(final double _x, final CtuluCommandContainer _cmd) {
    internalAdd(_x);
    if (_cmd != null) {
      _cmd.addCmd(new CommandAdd(list_.size() - 1, _x));
    }
    fireDoubleAdded();
    return true;
  }

  public void addAll(final double[] _d, final CtuluCommandContainer _cmd) {
    final int size = list_.size();
    list_.add(_d);
    r_ = null;
    if (_cmd != null) {
      _cmd.addCmd(new CommandAddAll(size, CtuluLibArray.copy(_d)));
    }
  }

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    if (_dataArray instanceof double[]) {
      addAll((double[]) _dataArray, _c);
      return true;
    } else if (_dataArray instanceof Double[]) {
      final Double[] d = (Double[]) _dataArray;
      final double[] v = new double[d.length];
      for (int i = v.length - 1; i >= 0; i--) {
        v[i] = (d[i]).doubleValue();
      }
      addAll(v, _c);
      return true;
    } else if (_dataArray instanceof Object[]) {
      final Object[] d = (Object[]) _dataArray;
      final double[] v = new double[d.length];
      for (int i = v.length - 1; i >= 0; i--) {
        if (d[i] instanceof Number) {
          v[i] = ((Number) d[i]).doubleValue();
        } else if (d[i] instanceof CtuluCollectionDouble) {
          CtuluCollectionDouble collection = (CtuluCollectionDouble) d[i];
          if (collection.getSize() > 0) {
            v[i] = collection.getValue(0);
          }
        }
      }
      addAll(v, _c);
      return true;
    } else {
      (new Throwable()).printStackTrace();
    }
    return false;
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    double d = 0;
    if (_data != null) {
      if (_data instanceof Number) {
        d = ((Number) _data).doubleValue();
      } else {
        try {
          d = Double.parseDouble(_data.toString());
        } catch (final NumberFormatException e) {
          //on ne fait rien ...

        }
      }
    }
    return add(d, _c);
  }

  public CtuluListDouble createListMemento() {
    return new CtuluListDouble(this);
  }

  /**
   * @return la liste
   */
  public CtuluCollectionDouble getDoubleList() {
    return list_;
  }

  /**
   * @return toutes valeurs sous forme de chaine
   */
  public String getFullDesc() {
    return CtuluLibString.arrayToString(list_.toNativeArray());
  }

  @Override
  public int getSize() {
    return list_.size();
  }

  @Override
  public double getValue(final int _i) {
    return list_.getQuick(_i);
  }

  @Override
  public double[] getValues() {
    return this.list_.toNativeArray();
  }

  public void initWithDouble(final CtuluArrayDouble _m, final boolean _throwEvent) {
    internInitWith(_m, _throwEvent);
  }

  public void initWithDouble(final CtuluListDouble _m, final boolean _throwEvent) {
    internInitWith(_m, _throwEvent);
  }

  @Override
  public void initWithDouble(final CtuluCollectionDouble _m, final boolean _throwEvent) {
    if (_m instanceof CtuluArrayDouble) {
      internInitWith((CtuluArrayDouble) _m, _throwEvent);
    } else if (_m instanceof CtuluListDouble) {
      internInitWith((CtuluListDouble) _m, _throwEvent);
    } else {
      list_ = new CDoubleArrayList(_m.getSize());
      final int nb = _m.getSize();
      for (int i = 0; i < nb; i++) {
        list_.add(_m.getValue(i));
      }
      r_ = null;
      if (_throwEvent) {
        fireDoubleStructureChanged();
      }
    }

  }

  /**
   * Reinitialise tout depuis _m et envoie un evt.
   *
   * @param _m le modele depuis lequel l'initialisation sera effectuee
   */
  public void initWith(final CtuluListDouble _m) {
    initWithDouble(_m, true);
  }

  public void initWith(final double[] _m) {
    list_ = new CDoubleArrayList(_m);
    r_ = null;
    fireDoubleStructureChanged();
  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluCollectionDouble) {
      initWithDouble((CtuluCollectionDouble) _model, false);
    }
  }

  /**
   * @see #insert(int, double, CtuluCommandContainer)
   */
  public final boolean insert(final int _i, final double _x) {
    return insert(_i, _x, null);
  }

  /**
   * @param _i l'indice [0,size()] si _i==size(), il sera ajoute a la fin
   * @param _x la valeur a ajouter
   * @param _cmd le receveur da la commande. Si null pas de commande creee.
   * @return true si inserer
   */
  public boolean insert(final int _i, final double _x, final CtuluCommandContainer _cmd) {
    if (_i < 0 || _i > list_.size()) {
      return false;
    }
    internalAdd(_i, _x);
    if (_cmd != null) {
      _cmd.addCmd(new CommandAdd(_i, _x));
    }
    fireDoubleAdded();
    return true;
  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    double d = 0;
    if (_data instanceof Double) {
      d = ((Double) _data).doubleValue();
    }
    return insert(_i, d, _c);
  }

  /**
   * @return true si cette liste contient une valeur unique
   */
  @Override
  public boolean isAllSameValue() {
    final double r = list_.getQuick(0);
    for (int i = list_.size() - 1; i > 0; i--) {
      if (list_.getQuick(i) != r) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isSameValues(final int[] _i) {
    if (_i == null) {
      return isAllSameValue();
    }
    if (_i.length == 0) {
      return false;
    }
    final double r = list_.getQuick(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (list_.getQuick(_i[i]) != r) {
        return false;
      }
    }
    return true;
  }

  public boolean remove(final int _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    if (_i >= 0 && _i < list_.size()) {
      final double xRemoved = list_.remove(_i);

      if (r_ != null && !r_.isValueStrictlyContained(xRemoved)) {
        r_ = null;
      }
      if (_c != null) {
        _c.addCmd(new CommandRemove(_i, xRemoved));
      }
      fireDoubleRemoved();
      return true;
    }
    return false;
  }

  /**
   * @param _i
   * @return true si remove
   * @see #remove(int[], CtuluCommandContainer)
   */
  public boolean remove(final int[] _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return remove(_i, _c, false);
  }

  /**
   * Enleve les indices appartenant au tableau _i. Attention: aucune copie du tableau _i n'est effectuee. De plus il est suppose que le tableau est
   * correct : les indices appartiennent � [0,size()[ et tous differents.
   *
   * @param _i i trie ordre naturel
   * @param _c la
   * @return true si modif
   */
  public boolean remove(final int[] _i, final CtuluCommandContainer _c, final boolean _forceMemento) {
    if (_i == null || _i.length == 0) {
      return false;
    }
    final int n = _i.length;
    if (n == 1) {
      return remove(_i[0], _c);
    }
    final int initSize = list_.size();
    final double[] newArray = new double[initSize];
    int idx = 0;
    final double[] oldV = new double[_i.length];
    int idxOldV = 0;
    // on copie le debut du tableau
    list_.toNativeArray(newArray, 0, _i[0]);
    idx = _i[0];
    final int maxI = _i[_i.length - 1];
    for (int i = idx; i <= maxI; i++) {
      if (Arrays.binarySearch(_i, i) < 0) {
        newArray[idx++] = list_.getQuick(i);
      } else {
        final double d = list_.getQuick(i);
        if (r_ != null && !r_.isValueStrictlyContained(d)) {
          r_ = null;
        }
        oldV[idxOldV++] = d;
      }
    }
    for (int i = maxI + 1; i < initSize; i++) {
      newArray[idx++] = list_.getQuick(i);
    }
    if (idx + idxOldV == initSize) {
      if (_c != null) {
        if (_forceMemento || idxOldV > 10) {
          _c.addCmd(new CommandRemoveMemento(createListMemento(), _i));
        } else {
          _c.addCmd(new CommandRemoveDangerous(oldV, _i));
        }
      }

      list_.resetQuick();
      list_.add(newArray, 0, idx);
      fireDoubleRemoved();
      return true;
    }
    return false;

  }

  /**
   * @see #removeAll(CtuluCommandContainer)
   */
  public final void removeAll() {
    removeAll(null);
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {
    if (_c != null) {
      _c.addCmd(new CommandRemoveAll(createListMemento()));
    }
    list_.resetQuick();
    fireDoubleRemoved();
  }

}
