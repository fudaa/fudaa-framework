/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-04-16 16:33:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluRange;

import java.util.Arrays;

/**
 * Une classe permettant de gerer un tableau de doubleun vecteur de double en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement.
 *
 * @author Fred Deniger
 * @version $Id: CtuluArrayDoubleImmutable.java,v 1.2 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluArrayDoubleImmutable extends CtuluCollectionDoubleAbstract implements CtuluCollectionDouble {
  protected double[] list_;
  CtuluRange r_;

  /**
   * @param _init si non null est utilise pour l'initialisation
   */
  public CtuluArrayDoubleImmutable(final CtuluCollectionDouble _init) {
    if (_init == null) {
      list_ = new double[0];
    } else {
      list_ = _init.getValues();
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayDoubleImmutable(final double[] _init) {
    list_ = CtuluLibArray.copy(_init);
  }

  /**
   * @param _nb taille initiales
   */
  public CtuluArrayDoubleImmutable(final int _nb) {
    list_ = new double[_nb];
  }

  public CtuluArrayDoubleImmutable() {
    this(0);
  }

  public CtuluArrayDoubleImmutable(final Object[] _init) {
    if (_init == null) {
      list_ = new double[0];
    } else {
      list_ = new double[_init.length];
      for (int i = list_.length - 1; i >= 0; i--) {
        list_[i] = ((Double) _init[i]).doubleValue();
      }
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayDoubleImmutable(final TDoubleArrayList _init) {
    this(_init.toNativeArray());
  }

  @Override
  public double getMax() {
    if (list_.length == 0) {
      return 0;
    }
    getRange(null);
    return r_.max_;
  }

  public void expandRange(final CtuluRange _rToExpand) {
    if (_rToExpand != null) {
      if (r_ == null) {
        getRange(null);
      }
      _rToExpand.expandTo(r_);
    }
  }

  @Override
  public double getMin() {
    if (list_.length == 0) {
      return 0;
    }
    getRange(null);
    return r_.min_;
  }

  @Override
  public Object getObjectValueAt(final int _i) {
    return CtuluLib.getDouble(getValue(_i));
  }

  public Object[] getObjectValues() {
    final Double[] r = new Double[list_.length];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = CtuluLib.getDouble(list_[i]);
    }
    return r;
  }

  @Override
  public void getRange(final CtuluRange _r) {
    if (list_.length == 0) {
      _r.max_ = 0;
      _r.min_ = 0;
      return;
    }
    if (r_ == null) {
      r_ = new CtuluRange();
      r_.max_ = list_[0];
      r_.min_ = r_.max_;
      double d;
      for (int i = list_.length - 1; i > 0; i--) {
        d = list_[i];
        if (d > r_.max_) {
          r_.max_ = d;
        }
        if (d < r_.min_) {
          r_.min_ = d;
        }
      }
    }
    if (_r != null) {
      _r.initWith(r_);
    }
  }

  @Override
  public final int getSize() {
    return list_.length;
  }

  @Override
  public double getValue(final int _i) {
    return list_[_i];
  }

  @Override
  public double[] getValues() {
    return CtuluLibArray.copy(list_);
  }

  /**
   * @return true si cette liste contient une valeur unique
   */
  public boolean isAllSameValue() {
    final double r = list_[0];
    for (int i = list_.length - 1; i > 0; i--) {
      if (list_[i] != r) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _a le tableau a tester
   * @return true si memes valeurs
   */
  public boolean isEquivalentTo(final CtuluArrayDoubleImmutable _a) {
    if (_a == null) {
      return false;
    }
    return Arrays.equals(this.list_, _a.list_);
  }

  protected void fireObjectChanged(int _indexGeom, Object _newValue) {
  }

  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
  }

  public boolean isSameValues(final int[] _idx) {
    return false;
  }

  public boolean setAll(final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  public boolean setAllObject(final int[] _i, final Object[] _data, final CtuluCommandContainer _c) {
    return false;
  }

  public boolean setObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  public boolean setObject(final int[] _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }
}
