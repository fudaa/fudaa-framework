/*
 *  @creation     22 sept. 2004
 *  @modification $Date: 2007-01-10 08:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluRange;

/**
 * @author fred deniger
 * @version $Id: CtuluAdapterDoubleInverse.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluAdapterDoubleInverse extends CtuluCollectionDoubleEditAbstract implements CtuluCollectionDoubleEdit {

  final CtuluCollectionDouble init_;

  public CtuluAdapterDoubleInverse(final CtuluCollectionDouble _init) {
    this.init_ = _init;
  }
  @Override
  protected void internalSetValue(final int _i, final double _newV) {}

  @Override
  public void expandTo(final CtuluRange _rangeToExpand) {
    init_.expandTo(_rangeToExpand);
  }

  @Override
  public Double getCommonValue(final int[] _i) {
    return init_.getCommonValue(_i);
  }

  @Override
  public double getMax() {
    return init_.getMax();
  }

  @Override
  public double getMin() {
    return init_.getMin();
  }

  @Override
  public int getSize() {
    return init_.getSize();
  }

  @Override
  public double getValue(final int _i) {
    final double d = init_.getValue(_i);
    return d == 0 ? 0 : 1 / d;
  }

  @Override
  public double[] getValues() {
    return init_.getValues();
  }

  @Override
  public void iterate(final CtuluDoubleVisitor _visitor) {
    init_.iterate(_visitor);
  }

}
