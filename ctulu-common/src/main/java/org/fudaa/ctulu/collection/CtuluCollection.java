/*
 * @creation 24 sept. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * @author Fred Deniger
 * @version $Id: CtuluCollection.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public interface CtuluCollection {

  /**
   * @param _i l'indice
   * @return l'objet a cette position
   */
  Object getObjectValueAt(int _i);

  /**
   * @return toutes les valeurs
   */
  Object[] getObjectValues();

  /**
   * @param _model le model servant � l'initialisation
   * @param _throwEvent true si un evt doit etre envoye
   */
  void initWith(CtuluCollection _model, boolean _throwEvent);

  /**
   * @param _idx les indices a tester
   * @return true si ce sont les meme valeurs
   */
  boolean isSameValues(int[] _idx);

  /**
   * @param _data l'objet a ajouter. Si l'objet n'est pas donne ajoute une valeur par defaut.
   * @param _c le receveur de command
   * @return true si effectue
   */
  boolean addObject(Object _data,CtuluCommandContainer _c);

  /**
   * @param _dataArray le tableau
   * @param _c le receveur de commandes
   * @return true si effectue
   */
  boolean addAllObject(Object _dataArray,CtuluCommandContainer _c);

  /**
   * @param _i l'indice d'insertion (decalage vers la droite)
   * @param _data l'objet a inserer. Si l'objet n'est pas donne ajoute une valeur par defaut.
   * @param _c le receveur de command
   * @return true si effectue
   */
  boolean insertObject(int _i,Object _data,CtuluCommandContainer _c);

  /**
   * @param _i l'indice a enlever
   * @param _c le receveur de command
   * @return true si effectue
   */
  boolean remove(int _i,CtuluCommandContainer _c);

  /**
   * @param _i l'indice a modifier
   * @param _data la nouvelle valeurs
   * @param _c le receveur de commande
   * @return true si action reussie
   */
  boolean setObject(int _i,Object _data,CtuluCommandContainer _c);

  /**
   * @param _i les indices a modifier
   * @param _data la nouvelle valeurs
   * @param _c le receveur de commande
   * @return true si action reussie
   */
  boolean setObject(int[] _i,Object _data,CtuluCommandContainer _c);

  boolean setAllObject(int[] _i,Object[] _data,CtuluCommandContainer _c);

  /**
   * @param _data la valeur a utiliser pour tous les indices
   * @param _c le receveur de commandes
   * @return true si ok
   */
  boolean setAll(Object _data,CtuluCommandContainer _c);

  /**
   * @param _i les indices a enlever. Ils sont supposes etre tous differents.
   * @param _c le receveur de commande
   * @return true si effectue
   */
  boolean remove(int[] _i,CtuluCommandContainer _c);

  /**
   * Efface tous les elements de la liste.
   * @param _c le receveur de commande
   */
  void removeAll(CtuluCommandContainer _c);

  /**
   * @return le nombre de valeur
   */
  int getSize();

}