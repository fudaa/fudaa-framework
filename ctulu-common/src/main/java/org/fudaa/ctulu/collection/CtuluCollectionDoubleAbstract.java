/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2007-05-04 13:43:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluRange;

/**
 * @author fred deniger
 * @version $Id: CtuluCollectionDoubleAbstract.java,v 1.5 2007-05-04 13:43:25 deniger Exp $
 */
public abstract class CtuluCollectionDoubleAbstract implements CtuluCollectionDouble {

  public CtuluCollectionDoubleAbstract() {}

  /**
   * @param _model le model a parcourir
   * @return le tableau de double du modele passe en argument.
   */
  public final static double[] getArray(final CtuluCollectionDouble _model) {
    if (_model == null) {
      return null;
    }
    final double[] r = new double[_model.getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = _model.getValue(i);
    }
    return r;
  }

  @Override
  public Object getObjectValueAt(final int _i) {
    return CtuluLib.getDouble(getValue(_i));
  }

  /**
   * @param _l le conteneur de double
   * @param _i les indices a tester. Si null tous les indices sont test�es.
   * @return la valeur commune
   */
  public final static Double getCommonValue(final CtuluCollectionDouble _l, final int[] _i) {
    if (_i == null) {
      final double r = _l.getValue(0);
      for (int i = _l.getSize() - 1; i > 0; i--) {
        if (_l.getValue(i) != r) {
          return null;
        }
      }
      return CtuluLib.getDouble(r);
    }
    if (_i.length == 0) {
      return null;
    }
    final double r = _l.getValue(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (_l.getValue(_i[i]) != r) {
        return null;
      }
    }
    return CtuluLib.getDouble(r);
  }

  public final static Double getCommonValue(final double[] _d, final int[] _i) {
    if (CtuluLibArray.isEmpty(_i) || CtuluLibArray.isEmpty(_d)) {
      return null;
    }
    final double r = _d[_i[0]];
    for (int i = _i.length - 1; i > 0; i--) {
      if (_d[_i[i]] != r) {
        return null;
      }
    }
    return CtuluLib.getDouble(r);
  }

  /**
   * @param _d le tableau de double a parcourir
   * @return la valeur max contenue par ce tableau . 0 si aucune valeur.
   */
  public static double getMax(final CtuluCollectionDouble _d) {
    if (_d == null || _d.getSize() == 0) {
      return 0;
    }
    double r = _d.getValue(0);
    for (int i = _d.getSize() - 1; i > 0; i--) {
      final double ri = _d.getValue(i);
      if (ri > r) {
        r = ri;
      }
    }
    return r;
  }

  /**
   * @param _d le tableau de double a parcourir
   * @return la valeur min contenue par ce tableau . 0 si aucune valeur.
   */
  public static double getMin(final CtuluCollectionDouble _d) {
    if (_d == null || _d.getSize() == 0) {
      return 0;
    }
    double r = _d.getValue(0);
    for (int i = _d.getSize() - 1; i > 0; i--) {
      final double ri = _d.getValue(i);
      if (ri < r) {
        r = ri;
      }
    }
    return r;
  }

  public static void iterate(final CtuluCollectionDouble _target, final CtuluDoubleVisitor _visitor) {
    final int n = _target.getSize();
    for (int i = 0; i < n; i++) {
      if (!_visitor.accept(i, _target.getValue(i))) {
        return;
      }
    }

  }

  @Override
  public Double getCommonValue(final int[] _i) {
    return CtuluCollectionDoubleAbstract.getCommonValue(this, _i);
  }

  @Override
  public double getMax() {
    return getMax(this);
  }

  @Override
  public void expandTo(final CtuluRange _rangeToExpand) {
    if (_rangeToExpand != null) {
      _rangeToExpand.expandTo(getMax());
      _rangeToExpand.expandTo(getMin());
    }

  }

  @Override
  public void getRange(final CtuluRange _r) {
    if (_r != null) {
      _r.setMin(getMin());
      _r.setMax(getMax());
    }

  }

  @Override
  public double getMin() {
    return getMin(this);

  }

  @Override
  public double[] getValues() {
    return CtuluCollectionDoubleAbstract.getArray(this);
  }

  @Override
  public void iterate(final CtuluDoubleVisitor _visitor) {
    iterate(this, _visitor);

  }

}
