/*
 * @creation 7 mai 07
 * @modification $Date: 2007-05-21 10:28:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

/**
 * @author fred deniger
 * @version $Id: CourbeInterface.java,v 1.1 2007-05-21 10:28:26 deniger Exp $
 */
public interface CourbeInterface {

  /**
   * @param _idx l'indice demande
   * @return la valeur x a l'indice _idx
   */
  double getX(int _idx);

  /**
   * @param _idx l'indice demande
   * @return la valeur y a l'indice _idx
   */
  double getY(int _idx);

  /**
   * @return le nombre de valeur.
   */
  int getNbValues();

}
