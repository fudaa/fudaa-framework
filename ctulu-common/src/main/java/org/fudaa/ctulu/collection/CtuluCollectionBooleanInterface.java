/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-01-10 08:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

/**
 * @author Fred Deniger
 * @version $Id: CtuluCollectionBooleanInterface.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public interface CtuluCollectionBooleanInterface {

  boolean getValue(int _idx);

  int getSize();

}
