/*
 * @creation 21 mars 2006
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * @author fred deniger
 * @version $Id: CtuluCollectionBooleanAbstract.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public abstract class CtuluCollectionBooleanAbstract extends CtuluCollectionAbstract implements CtuluCollectionBooleanInterface, CtuluCollection {

  protected class CommandSet implements CtuluCommand {

    private final int idx_;

    private final boolean newV_;

    boolean oldV_;

    public CommandSet(final boolean _newV, final boolean _oldV, final int _idx) {
      oldV_ = _oldV;
      newV_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      set(idx_, newV_, null);
    }

    @Override
    public void undo() {
      set(idx_, oldV_, null);
    }

  }

  protected class CommandSetAll implements CtuluCommand {

    boolean[] newMemento_;

    boolean[] oldMemento_;

    /**
     * @param _oldMemento les anciennes donnees
     */
    public CommandSetAll(final boolean[] _oldMemento) {
      oldMemento_ = _oldMemento;

    }

    @Override
    public void redo() {
      setAll(newMemento_, null);
    }

    @Override
    public void undo() {
      if (newMemento_ == null) {
        newMemento_ = getValues();
      }
      setAll(oldMemento_, null);
    }

  }

  protected class CommandSets implements CtuluCommand {

    int[] idxs_;

    boolean[] newVs_;

    boolean[] oldV_;

    public CommandSets(final boolean[] _newVs, final boolean[] _oldV, final int[] _idx) {
      oldV_ = _oldV;
      newVs_ = _newVs;
      idxs_ = _idx;
    }

    @Override
    public void redo() {
      set(idxs_, newVs_, null);
    }

    @Override
    public void undo() {
      set(idxs_, oldV_, null);
    }
  }

  protected class CommandSetsOneDouble implements CtuluCommand {

    int[] idx_;

    boolean newV_;

    boolean[] oldV_;

    public CommandSetsOneDouble(final boolean _newV, final boolean[] _oldV, final int[] _idx) {
      oldV_ = _oldV;
      newV_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      set(idx_, newV_, null);
    }

    @Override
    public void undo() {
      set(idx_, oldV_, null);
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  protected CtuluCollectionBooleanAbstract() {}


  protected abstract boolean internalSet(int _i, boolean _newV);

  public final Boolean getCommonValue(final int[] _i) {
    if (((_i == null)) || ((_i.length == 0))) {
      return null;
    }
    final boolean r = getValue(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (getValue(_i[i]) != r) {
        return null;
      }
    }
    return Boolean.valueOf(r);
  }

  @Override
  public abstract int getSize();

  @Override
  public abstract boolean getValue(int _i);

  public abstract boolean[] getValues();

  @Override
  public Object[] getObjectValues() {
    final Boolean[] r = new Boolean[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = Boolean.valueOf(getValue(i));
    }
    return r;
  }

  /**
   * @return true si cette liste contient une valeur unique
   */
  public final boolean isAllSameValue() {
    final boolean r = getValue(0);
    for (int i = getSize() - 1; i > 0; i--) {
      if (getValue(i) != r) {
        return false;
      }
    }
    return true;
  }

  @Override
  public final boolean isSameValues(final int[] _i) {
    if (_i == null) {
      return isAllSameValue();
    }
    if (_i.length == 0) {
      return false;
    }
    final boolean r = getValue(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (getValue(_i[i]) != r) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _i
   * @param _newV
   * @see #set(int, boolean, CtuluCommandContainer)
   * @return true si modif
   */
  public final boolean set(final int _i, final boolean _newV) {
    return set(_i, _newV, null);
  }

  @Override
  protected  abstract void fireObjectChanged(int _indexGeom, Object _newValue);

  /**
   * @param _i l'indice a modifier
   * @param _newB la nouvelle valeur
   * @param _c le receveur de la commande. peut-etre null
   * @return true si modif
   */
  public final boolean set(final int _i, final boolean _newB, final CtuluCommandContainer _c) {
    // verif basique
    if ((((_i < 0) || (_i > getSize()))) || (getValue(_i) == _newB)) {
      return false;
    }
    final boolean old = getValue(_i);
    internalSet(_i, _newB);
    fireObjectChanged(_i, old);
    if (_c != null) {
      _c.addCmd(new CommandSet(_newB, old, _i));
    }
    return true;
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   *
   * @param _i les indices a modifier
   * @param _newB les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final boolean _newB, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null) {
      return false;
    }
    boolean r = false;
    final boolean[] old = (_c == null) ? null : new boolean[_i.length];
    if (_c != null) {
      if (_i.length > (getSize() / 2)) {
        final boolean[] datas = getValues();
        for (int i = _i.length - 1; i >= 0; i--) {
          datas[_i[i]] = _newB;
        }
        return setAll(datas, _c);
      }
    }
    final int nbValue = getSize();
    for (int i = _i.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if ((idx >= 0) && (idx < nbValue)) {
        if (old != null) {
          old[i] = getValue(idx);
        }
        r |= internalSet(idx, _newB);
      }
    }
    if (r && (_c != null)) {
      _c.addCmd(new CommandSetsOneDouble(_newB, old, CtuluLibArray.copy(_i)));
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    return r;
  }

  /**
   * @param _i
   * @param _newV
   * @return true si modif
   */
  public final boolean set(final int[] _i, final boolean[] _newV) {
    return set(_i, _newV, null);
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   *
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final boolean[] _newV, final CtuluCommandContainer _c) {
    // verif basique
    if ((((((_i == null) || (_newV == null))) || (_newV.length == 0))) || (_newV.length != _i.length)) {
      return false;
    }
    boolean r = false;
    final boolean[] old = (_c == null) ? null : new boolean[_newV.length];
    if (_c != null) {
      if (_i.length > (getSize() / 2)) {
        final boolean[] datas = getValues();
        for (int i = _i.length - 1; i >= 0; i--) {
          datas[_i[i]] = _newV[i];
        }
        return setAll(datas, _c);
      }
    }
    final int nbValue = getSize();
    for (int i = _newV.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if ((idx >= 0) && (idx < nbValue)) {
        if (old != null) {
          old[i] = getValue(idx);
        }
        r |= internalSet(idx, _newV[i]);
      }
    }
    if (r && (_c != null)) {
      _c.addCmd(new CommandSets(CtuluLibArray.copy(_newV), old, CtuluLibArray.copy(_i)));
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    return r;
  }

  /**
   * @param _val les nouvelles valeurs : doit etre de la meme taille que ce model
   * @param _cmd le receveur de commande: peut etre null.
   * @return true si modif
   */
  public final boolean setAll(final boolean _val, final CtuluCommandContainer _cmd) {
    // verif basique
    boolean r = false;
    boolean[] memento = null;
    if (_cmd != null) {
      memento = getValues();
    }
    for (int i = getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _val);
    }
    if (r && ((_cmd != null))) {
      _cmd.addCmd(new CommandSetAll(memento));
    }
    return r;
  }

  @Override
  public final boolean setAll(final Object _data, final CtuluCommandContainer _c) {
    if (_data instanceof Boolean) {
      return setAll(((Boolean) _data).booleanValue(), _c);
    }
    return false;
  }

  @Override
  public final boolean setAllObject(final int[] _i, final Object[] _data, final CtuluCommandContainer _c) {
    if (_data == null) {
      return false;
    }
    final boolean[] newV = new boolean[_data.length];
    for (int i = newV.length - 1; i >= 0; i--) {
      newV[i] = Boolean.getBoolean((_data[i]).toString());
    }
    return set(_i, newV, _c);
  }

  /**
   * @param _val les nouvelles valeurs : doit etre de la meme taille que ce model
   * @param _cmd le receveur de commande: peut etre null.
   * @return true si modif
   */
  public final boolean setAll(final boolean[] _val, final CtuluCommandContainer _cmd) {
    // verif basique
    if ((_val == null) || (_val.length != getSize())) {
      return false;
    }
    boolean r = false;
    boolean[] memento = null;
    if (_cmd != null) {
      memento = getValues();
    }
    for (int i = _val.length - 1; i >= 0; i--) {
      r |= internalSet(i, _val[i]);
    }
    if (r && ((_cmd != null))) {
      _cmd.addCmd(new CommandSetAll(memento));
    }
    return r;
  }

  /**
   * @param _val les nouvelles valeurs : doit etre de la meme taille que ce model
   * @param _cmd le receveur de commande: peut etre null.
   * @return true si modif
   */
  public final boolean setAll(final CtuluCollectionBooleanInterface _val, final CtuluCommandContainer _cmd) {
    // verif basique
    if ((_val == null) || (_val.getSize() != getSize())) {
      return false;
    }
    boolean r = false;
    boolean[] memento = null;
    if (_cmd != null) {
      memento = getValues();
    }
    for (int i = _val.getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _val.getValue(i));
    }
    if (r && ((_cmd != null))) {
      _cmd.addCmd(new CommandSetAll(memento));
    }
    return r;
  }

  @Override
  public final boolean setObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    if (_data instanceof Boolean) {
      return set(_i, ((Boolean) _data).booleanValue(), _c);
    } else if (_data != null) {
      return set(_i, (Boolean.valueOf(_data.toString())).booleanValue(), _c);
    }
    return false;
  }

  @Override
  public final boolean setObject(final int[] _i, final Object _data, final CtuluCommandContainer _c) {
    if (_data instanceof Boolean) {
      return set(_i, ((Boolean) _data).booleanValue(), _c);
    } else if (_data != null) {
      return set(_i, (Boolean.valueOf(_data.toString())).booleanValue(), _c);
    }
    return false;
  }

  @Override
  public final Object getObjectValueAt(final int _i) {
    return Boolean.valueOf(this.getValue(_i));
  }

}
