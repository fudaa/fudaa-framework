/*
 *  @creation     22 sept. 2004
 *  @modification $Date: 2007-04-20 16:20:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluRange;

/**
 * @author Fred Deniger
 * @version $Id: CtuluCollectionDouble.java,v 1.3 2007-04-20 16:20:19 deniger Exp $
 */
public interface CtuluCollectionDouble {
  /**
   * @return UNE COPIE du tableau des double
   */
  double[] getValues();

  /**
   * @return la taille
   */
  int getSize();

  /**
   * @param _i l'indice [0;size()[
   * @return la valeur en ce point
   */
  double getValue(int _i);

  Object getObjectValueAt(int _i);

  /**
   * @return la valeur minimale
   */
  double getMin();

  /**
   * @return la valeur max
   */
  double getMax();

  /**
   * @param _rangeToExpand la range <b>� �tendre</b> avec les valeurs de cette collection
   */
  void expandTo(CtuluRange _rangeToExpand);

  /**
   * @param _r le range a <b>initialiser</b> avec les valeurs de cette collection
   */
  void getRange(final CtuluRange _r);

  /**
   * @param _i les indices a tester
   * @return la valeur commune ou nulle si aucune
   */
  Double getCommonValue(int[] _i);

  void iterate(CtuluDoubleVisitor _visitor);

}
