/*
 * @creation 22 sept. 2004
 * 
 * @modification $Date: 2007-01-10 08:58:48 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * Une classe permettant de gerer un vecteur de double. en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 * 
 * @author Fred Deniger
 * @version $Id: CtuluCollectionObjectAbstract.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public abstract class CtuluCollectionObjectAbstract extends CtuluCollectionAbstract {

  protected class CommandSet implements CtuluCommand {

    int idx_;

    Object newV_;

    Object oldV_;

    public CommandSet(final Object _newV, final Object _oldV, final int _idx) {
      oldV_ = _oldV;
      newV_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      internalSet(idx_, newV_);
      fireObjectChanged(idx_, newV_);
    }

    @Override
    public void undo() {
      internalSet(idx_, oldV_);
      fireObjectChanged(idx_, oldV_);
    }

  }

  protected class CommandSets implements CtuluCommand {

    int[] idxs_;

    Object[] newObjects_;

    Object[] oldObjects_;

    public CommandSets(final Object[] _newV, final Object[] _oldV, final int[] _idx) {
      oldObjects_ = _oldV;
      newObjects_ = _newV;
      idxs_ = _idx;
    }

    @Override
    public void redo() {
      set(idxs_, newObjects_, null);
    }

    @Override
    public void undo() {
      set(idxs_, oldObjects_, null);
    }
  }

  protected class CommandSetsOneValue implements CtuluCommand {

    int[] idx_;

    Object newV_;

    Object[] oldV_;

    protected CommandSetsOneValue(final Object _newV, final Object[] _oldV, final int[] _idx) {
      oldV_ = _oldV;
      newV_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      set(idx_, newV_, null);
    }

    @Override
    public void undo() {
      set(idx_, oldV_, null);
    }
  }

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  protected CtuluCollectionObjectAbstract() {
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue) {
  }

  protected abstract boolean internalSet(int _i, Object _newV);

  @Override
  public final Object getObjectValueAt(final int _i) {
    return getValueAt(_i);
  }

  @Override
  public Object[] getObjectValues() {
    final Object[] r = new Object[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getValueAt(i);
    }
    return r;
  }

  @Override
  public abstract int getSize();

  public abstract Object getValueAt(int _i);

  /**
   * @param _o l'objet a tester
   * @return l'indice de l'objet. -1 si non trouve.
   */
  public abstract int indexOf(Object _o);

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  public boolean isAllSameValue() {
    final Object r = getValueAt(0);
    for (int i = getSize() - 1; i > 0; i--) {
      final Object ri = getValueAt(i);
      if (r == null) {
        if (ri != null) { return false;
        // r n'est pas null
        }
      } else if (!r.equals(ri)) { return false; }

    }
    return true;
  }

  @Override
  public boolean isSameValues(final int[] _idx) {
    if (_idx == null) { return isAllSameValue(); }
    if (_idx.length == 0) { return false; }
    final Object r = getValueAt(_idx[0]);
    for (int i = _idx.length - 1; i > 0; i--) {
      final Object ri = getValueAt(_idx[i]);
      if (r == null) {
        if (ri != null) { return false;
        // r n'est pas null
        }
      } else if (!r.equals(ri)) { return false; }

    }
    return true;
  }

  /**
   * @param _i
   * @param _newV
   * @see #set(int, Object, CtuluCommandContainer)
   * @return true si modif
   */
  public final boolean set(final int _i, final Object _newV) {
    return set(_i, _newV, null);
  }

  /**
   * @param _i l'indice a modifier
   * @param _newObj la nouvelle valeur
   * @param _c le receveur de la commande. peut-etre null
   * @return true si modif
   */
  public final boolean set(final int _i, final Object _newObj, final CtuluCommandContainer _c) {
    // verif basique
    if (_i < 0 || _i >= getSize() || getValueAt(_i) == _newObj)
      return false;
    final Object old = getValueAt(_i);
    internalSet(_i, _newObj);
    fireObjectChanged(_i, _newObj);
    if (_c != null) {
      _c.addCmd(new CommandSet(_newObj, old, _i));
    }
    return true;
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   * 
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final Object _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null) { return false; }
    boolean r = false;
    final Object[] old = _c == null ? null : new Object[_i.length];
    final int nbValues = getSize();
    for (int i = _i.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if (idx >= 0 && idx < nbValues) {
        if (old != null) {
          old[i] = getValueAt(idx);
        }
        r |= internalSet(idx, _newV);
      }
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    if (r && _c != null) {
      _c.addCmd(new CommandSetsOneValue(_newV, old, _i));
    }
    return r;
  }

  /**
   * @param _i
   * @param _newV
   * @see #set(int[], Object[], CtuluCommandContainer)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final Object[] _newV) {
    return set(_i, _newV, null);
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   * 
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final Object[] _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null || _newV == null || _newV.length == 0 || _newV.length != _i.length) { return false; }
    boolean r = false;
    final Object[] old = _c == null ? null : new Object[_newV.length];
    final int nbValues = getSize();
    for (int i = _newV.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if (idx >= 0 && idx < nbValues) {
        if (old != null) {
          old[i] = getValueAt(idx);
        }
        r |= internalSet(idx, _newV[i]);
      }
    }
    if (r && _c != null) {
      _c.addCmd(new CommandSets(_newV, old, _i));
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    return r;
  }

  /**
   * @param _v la nouvelle valeur pour tous les indices
   * @return true si maj
   */
  public final boolean setAll(final Object _v) {
    return setAll(_v, null);
  }
  
  protected int[] createAllIdx() {
    int[] all = new int[getSize()];
    for (int i = 0; i < all.length; i++) {
      all[i] = i;
    }
    return all;
  }

  @Override
  public final boolean setAll(final Object _v, final CtuluCommandContainer _cmd) {
    final Object[] olds = _cmd == null ? null : getObjectValues();
    boolean r = false;
    for (int i = getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _v);
    }
    if (r) {
      fireObjectChanged(-1, null);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setAll(_v);
          }

          @Override
          public void undo() {
            setAllArray(olds);
          }
        });

      }
    }
    return true;
  }

  public final boolean setAllArray(final Object[] _v) {
    return setAll(_v, null);
  }

  public final boolean setAllArray(final Object[] _v, final CtuluCommandContainer _cmd) {
    final Object[] olds = _cmd == null ? null : getObjectValues();
    boolean r = false;
    for (int i = getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _v[i]);
    }
    if (r) {
      fireObjectChanged(-1, null);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setAllArray(_v);
          }

          @Override
          public void undo() {
            setAllArray(olds);
          }
        });

      }
    }
    return true;
  }

  @Override
  public final boolean setAllObject(final int[] _i, final Object[] _data, final CtuluCommandContainer _c) {
    return set(_i, _data, _c);
  }

  @Override
  public final boolean setObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return set(_i, _data, _c);
  }

  @Override
  public final boolean setObject(final int[] _i, final Object _data, final CtuluCommandContainer _c) {
    return set(_i, _data, _c);
  }

}
