/*
 * @creation 1 oct. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;


/**
 * @author Fred Deniger
 * @version $Id: GISCollectionPointDataDoubleInterface.java,v 1.5 2007-01-10 08:58:48 deniger Exp $
 */
public interface CollectionPointDataDoubleInterface {

  /**
   * @return le nombre de points
   */
  int getNbPoint();

  double getX(int _i);

  double getY(int _i);

  double getZ(int _i);

  int getNbValues();

  //  MNTPoint getNewPoint(int _i);
  double getDoubleValue(int _idxValue,int _idxPt);

  void fill(int _idxValue,CtuluListDouble _m);

}