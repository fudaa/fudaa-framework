/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-05-04 13:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluRange;

/**
 * Une classe permettant de gerer un vecteur de double. Nouvelles fonctionnalites: <br>
 * Undo/Redo Max/min enregistrer automatiquement.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluCollectionDoubleEditAbstract.java,v 1.6 2007-05-04 13:43:25 deniger Exp $
 */
public abstract class CtuluCollectionDoubleEditAbstract extends CtuluCollectionDoubleAbstract implements
    CtuluCollectionDoubleEdit, CtuluCollection {

  protected class CommandSet implements CtuluCommand {

    int idx_;

    double newVDouble_;

    double oldVDouble_;

    public CommandSet(final double _newV, final double _oldV, final int _idx) {
      oldVDouble_ = _oldV;
      newVDouble_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      set(idx_, newVDouble_, null);
    }

    @Override
    public void undo() {
      set(idx_, oldVDouble_, null);
    }

  }

  protected class CommandSetAll implements CtuluCommand {

    CtuluArrayDouble newMemento_;

    CtuluArrayDouble oldMemento_;

    /**
     * @param _oldMemento les anciennes donnees
     */
    public CommandSetAll(final CtuluArrayDouble _oldMemento) {
      oldMemento_ = _oldMemento;

    }

    @Override
    public void redo() {
      initWithDouble(newMemento_, true);
    }

    @Override
    public void undo() {
      if (newMemento_ == null) {
        newMemento_ = createMemento();
      }
      initWithDouble(oldMemento_, true);
    }

  }

  protected class CommandSets implements CtuluCommand {

    int[] idx_;

    double[] newVs_;

    double[] oldVs_;

    public CommandSets(final double[] _newV, final double[] _oldV, final int[] _idx) {
      oldVs_ = _oldV;
      newVs_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      set(idx_, newVs_, null);
    }

    @Override
    public void undo() {
      set(idx_, oldVs_, null);
    }
  }

  protected class CommandSetsOneDouble implements CtuluCommand {

    int[] idxs_;
    double newV_;
    double[] oldVs_;

    public CommandSetsOneDouble(final double _newV, final double[] _oldV, final int[] _idx) {
      oldVs_ = _oldV;
      newV_ = _newV;
      idxs_ = _idx;
    }

    @Override
    public void redo() {
      set(idxs_, newV_, null);
    }

    @Override
    public void undo() {
      set(idxs_, oldVs_, null);
    }
  }

  public static double getDouble(final Object _data) throws NumberFormatException {
    if (_data instanceof Number) {
      return ((Number) _data).doubleValue();
    } else if (_data == null) {
      throw new NumberFormatException("double is null");
    } else {
      return Double.parseDouble(_data.toString());
    }
  }

  CtuluRange r_;

  protected void initWith(final CtuluCollectionDouble _m) {
    initWithDouble(_m, true);
  }

  protected boolean internalSet(final int _i, final double _newV) {
    if (_newV == getValue(_i)) {
      return false;
    }
    final double old = getValue(_i);
    internalSetValue(_i, _newV);
    if (r_ != null && !r_.isValueStrictlyContained(old)) {
      r_ = null;
    }
    if (r_ != null && !r_.isValueContained(_newV)) {
      if (_newV > r_.max_) {
        r_.max_ = _newV;
      }
      if (_newV < r_.min_) {
        r_.min_ = _newV;
      }
    }
    return true;
  }

  protected abstract void internalSetValue(int _i, double _newV);

  public CtuluArrayDouble createMemento() {
    return new CtuluArrayDouble(this);
  }

  @Override
  public Double getCommonValue(final int[] _i) {
    if (_i == null) {
      return isAllSameValue() ? CtuluLib.getDouble(getValue(0)) : null;
    }
    if (_i.length == 0) {
      return null;
    }
    final double r = getValue(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (getValue(_i[i]) != r) {
        return null;
      }
    }
    return CtuluLib.getDouble(r);
  }

  @Override
  public double getMax() {
    if (getSize() == 0) {
      return 0;
    }
    getRange(null);
    return r_.max_;
  }

  @Override
  public double getMin() {
    if (getSize() == 0) {
      return 0;
    }
    getRange(null);
    return r_.min_;
  }

  @Override
  public final Object getObjectValueAt(final int _i) {
    return CtuluLib.getDouble(getValue(_i));
  }

  @Override
  public final Object[] getObjectValues() {
    final Double[] r = new Double[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = (Double) getObjectValueAt(i);
    }
    return r;
  }

  boolean useCache_ = true;

  @Override
  public void getRange(final CtuluRange _r) {
    // meme si le _r est null on fait le calcul : cela permet de mettre � jour le cache.
    if (getSize() == 0) {
      _r.max_ = 0;
      _r.min_ = 0;
      return;
    }
    if (r_ == null || !useCache_) {
      r_ = new CtuluRange();
      r_.max_ = getValue(0);
      r_.min_ = r_.max_;
      double d;
      for (int i = getSize() - 1; i > 0; i--) {
        d = getValue(i);
        if (d > r_.max_) {
          r_.max_ = d;
        }
        if (d < r_.min_) {
          r_.min_ = d;
        }
      }
    }
    if (_r != null) {
      _r.initWith(r_);
    }
  }

  @Override
  public double[] getValues() {
    final double[] r = new double[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getValue(i);
    }
    return r;
  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluCollectionDouble) {
      initWithDouble((CtuluCollectionDouble) _model, false);
    }
  }

  /**
   * Appele qui si des doubles sont modifies avec un set.
   */
  protected void fireObjectChanged(int _indexGeom, Object _newValue) {

  }

  @Override
  public void initWithDouble(final CtuluCollectionDouble _m, final boolean _throwEvent) {
    if (_m == null) {
      return;
    }
    if (getSize() != _m.getSize()) {
      FuLog.error(getClass().getName() + " Taille differente attention");
    }
    for (int i = getSize() - 1; i >= 0; i--) {
      internalSetValue(i, _m.getValue(i));
    }
    r_ = null;
    if (_throwEvent) {
      fireObjectChanged(-1, null);
    }
  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  public void clearCache() {
    r_ = null;
  }

  /**
   * @return true si cette liste contient une valeur unique
   */
  public boolean isAllSameValue() {
    final double r = getValue(0);
    for (int i = getSize() - 1; i > 0; i--) {
      if (getValue(i) != r) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _a le tableau a tester
   * @return true si memes valeurs
   */
  public boolean isEquivalentTo(final CtuluCollectionDoubleEditAbstract _a) {
    if (_a == null) {
      return false;
    }
    if (_a.getSize() != getSize()) {
      return false;
    }
    for (int i = getSize() - 1; i >= 0; i--) {
      if (_a.getValue(i) != getValue(i)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isSameValues(final int[] _i) {
    if (_i == null) {
      return isAllSameValue();
    }
    if (_i.length == 0) {
      return false;
    }
    final double r = getValue(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (getValue(_i[i]) != r) {
        return false;
      }
    }
    return true;
  }

  /**
   * Visite de i=0 a i=nbElement.
   */
  @Override
  public void iterate(final CtuluDoubleVisitor _visitor) {
    final int n = getSize();
    for (int i = 0; i < n; i++) {
      if (!_visitor.accept(i, getValue(i))) {
        return;
      }
    }

  }

  /**
   * @param _i
   * @param _newV
   * @see #set(int, double, CtuluCommandContainer)
   * @return true si modif
   */
  public final boolean set(final int _i, final double _newV) {
    return set(_i, _newV, null);
  }

  public final boolean setAll(final double _v) {
    return setAll(_v, null);
  }

  /**
   * @param _i l'indice a modifier
   * @param _newV la nouvelle valeur
   * @param _c le receveur de la commande. peut-etre null
   * @return true si modif
   */
  @Override
  public final boolean set(final int _i, final double _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i < 0 || _i >= getSize() || getValue(_i) == _newV)
      return false;
    final double old = getValue(_i);
    internalSet(_i, _newV);
    fireObjectChanged(_i, _newV);
    if (_c != null) {
      _c.addCmd(new CommandSet(_newV, old, _i));
    }
    return true;
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   * 
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  @Override
  public final boolean set(final int[] _i, final double _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null) {
      return false;
    }
    boolean r = false;
    final double[] old = _c == null ? null : new double[_i.length];
    if (_c != null) {
      if (_i.length > getSize() / 2) {
        final double[] datas = getValues();
        for (int i = _i.length - 1; i >= 0; i--) {
          final int k = _i[i];
          datas[k] = _newV;
        }
        return setAll(datas, _c);
      }
    }
    final int nbValues = getSize();
    for (int i = _i.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if (idx >= 0 && idx < nbValues) {
        if (old != null) {
          old[i] = getValue(idx);
        }
        r |= internalSet(idx, _newV);

      }
    }
    if (r && _c != null) {
      _c.addCmd(new CommandSetsOneDouble(_newV, old, CtuluLibArray.copy(_i)));
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    return r;
  }

  /**
   * @param _i
   * @param _newV
   * @see #set(int[], double[], CtuluCommandContainer)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final double[] _newV) {
    return set(_i, _newV, null);
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   * 
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  @Override
  public final boolean set(final int[] _i, final double[] _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null || _newV == null || _newV.length == 0 || _newV.length != _i.length) {
      return false;
    }
    boolean r = false;
    final double[] old = _c == null ? null : new double[_newV.length];
    if (_c != null) {
      if (_i.length > getSize() / 2) {
        final double[] datas = getValues();
        for (int i = _i.length - 1; i >= 0; i--) {
          datas[_i[i]] = _newV[i];
        }
        return setAll(datas, _c);
      }
    }
    final int nbValues = getSize();
    for (int i = _newV.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if (idx >= 0 && idx < nbValues) {
        if (old != null) {
          old[i] = getValue(idx);
        }
        r |= internalSet(idx, _newV[i]);
      }
    }
    if (r && _c != null) {
      _c.addCmd(new CommandSets(CtuluLibArray.copy(_newV), old, CtuluLibArray.copy(_i)));
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    return r;
  }

  /**
   * @param _val les nouvelles valeurs : doit etre de la meme taille que ce model
   * @param _cmd le receveur de commande: peut etre null.
   * @return true si modif
   */
  public final boolean setAll(final CtuluCollectionDouble _val, final CtuluCommandContainer _cmd) {
    // verif basique
    if (_val == null || _val.getSize() != getSize()) {
      return false;
    }
    boolean r = false;
    CtuluArrayDouble memento = null;
    if (_cmd != null) {
      memento = createMemento();
    }
    boolean changed;
    for (int i = _val.getSize() - 1; i >= 0; i--) {
      changed = _val.getValue(i) != getValue(i);
      if (changed) {
        internalSet(i, _val.getValue(i));
        r = true;
      }
    }
    if (r && _cmd != null) {
      _cmd.addCmd(new CommandSetAll(memento));
    }
    return r;
  }

  /**
   * @param _val les nouvelles valeurs : doit etre de la meme taille que ce model
   * @param _cmd le receveur de commande: peut etre null.
   * @return true si modif
   */
  public final boolean setAll(final double _val, final CtuluCommandContainer _cmd) {
    // verif basique
    boolean r = false;
    final CtuluArrayDouble memento = _cmd == null ? null : createMemento();
    for (int i = getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _val);
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    if (r && _cmd != null) {
      _cmd.addCmd(new CommandSetAll(memento));
    }
    return r;
  }

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {}

  /**
   * @param _val les nouvelles valeurs : doit etre de la meme taille que ce model
   * @param _cmd le receveur de commande: peut etre null.
   * @return true si modif
   */
  @Override
  public final boolean setAll(final double[] _val, final CtuluCommandContainer _cmd) {
    // verif basique
    if (_val == null || _val.length != getSize()) {
      return false;
    }
    boolean r = false;
    final CtuluArrayDouble memento = _cmd == null ? null : createMemento();
    for (int i = _val.length - 1; i >= 0; i--) {
      r |= internalSet(i, _val[i]);
    }
    if (r && _cmd != null) {
      _cmd.addCmd(new CommandSetAll(memento));
    }
    if (r) {
      fireObjectChanged(-1, null);
    }
    return r;
  }

  @Override
  public final boolean setAll(final Object _data, final CtuluCommandContainer _c) {
    try {
      return setAll(getDouble(_data), _c);
    } catch (final NumberFormatException _evt) {
      return false;

    }
  }

  @Override
  public final boolean setAllObject(final int[] _i, final Object[] _data, final CtuluCommandContainer _c) {
    if (_data == null) {
      return false;
    }
    final double[] newV = new double[_data.length];
    for (int i = newV.length - 1; i >= 0; i--) {
      newV[i] = ((Number) _data[i]).doubleValue();
    }
    return set(_i, newV, _c);
  }

  public boolean setObject(final int _i, final double _data, final CtuluCommandContainer _c) {
    return set(_i, _data, _c);
  }

  @Override
  public final boolean setObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    try {
      return set(_i, getDouble(_data), _c);
    } catch (final NumberFormatException e) {
      return false;
    }
  }

  @Override
  public final boolean setObject(final int[] _i, final Object _data, final CtuluCommandContainer _c) {
    try {
      return set(_i, getDouble(_data), _c);
    } catch (final NumberFormatException e) {
      return false;
    }
  }

  public boolean isUseCache() {
    return useCache_;
  }

  public void setUseCache(final boolean _useCache) {
    useCache_ = _useCache;
  }
}
