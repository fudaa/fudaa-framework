/*
 *  @creation     22 sept. 2004
 *  @modification $Date: 2007-05-04 13:43:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluRange;

/**
 * Une extension de la classe TDoubleArrayList du projet trove.
 * 
 * @author Fred Deniger
 * @version $Id: CDoubleArrayList.java,v 1.3 2007-05-04 13:43:25 deniger Exp $
 */
public class CDoubleArrayList extends TDoubleArrayList implements CtuluCollectionDouble {
  public CDoubleArrayList() {
    super();
  }

  @Override
  public void expandTo(final CtuluRange _rangeToExpand) {
    if (_rangeToExpand != null) {
      _rangeToExpand.expandTo(getMax());
      _rangeToExpand.expandTo(getMin());
    }

  }

  /**
   * @param _values
   */
  public CDoubleArrayList(final double[] _values) {
    super(_values);
  }

  /**
   * @param _capacity
   */
  public CDoubleArrayList(final int _capacity) {
    super(_capacity);
  }

  @Override
  public final Double getCommonValue(final int[] _i) {
    return CtuluCollectionDoubleAbstract.getCommonValue(this, _i);
  }

  @Override
  public double getMax() {
    return super.max();
  }

  @Override
  public double getMin() {
    return super.min();
  }

  @Override
  public void getRange(final CtuluRange _r) {
    if (size() == 0) {
      _r.max_ = 0;
      _r.min_ = 0;
    } else {
      _r.max_ = max();
      _r.min_ = min();
    }
  }

  @Override
  public final int getSize() {
    return size();
  }

  @Override
  public final double getValue(final int _i) {
    return getQuick(_i);
  }

  @Override
  public Object getObjectValueAt(final int _i) {
    return CtuluLib.getDouble(getQuick(_i));
  }

  @Override
  public final double[] getValues() {
    return toNativeArray();
  }

  @Override
  public final void iterate(final CtuluDoubleVisitor _visitor) {
    final int n = getSize();
    for (int i = 0; i < n; i++) {
      if (!_visitor.accept(i, getValue(i))) {
        return;
      }
    }
  }
}
