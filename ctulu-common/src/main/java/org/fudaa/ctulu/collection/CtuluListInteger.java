/*
 * @creation 22 sept. 2004
 * 
 * @modification $Date: 2007-01-10 08:58:48 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import gnu.trove.TIntArrayList;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double. En ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 * 
 * @author Fred Deniger
 * @version $Id: CtuluListInteger.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluListInteger extends CtuluCollectionIntegerAbstract {

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    if (_dataArray instanceof int[]) {
      addAll((int[]) _dataArray, _c);
      return true;
    } else if (_dataArray instanceof Integer[]) {
      final Integer[] d = (Integer[]) _dataArray;
      final int[] v = new int[d.length];
      for (int i = v.length - 1; i >= 0; i--) {
        v[i] = (d[i]).intValue();
      }
      addAll(v, _c);
      return true;
    } else if (_dataArray instanceof Object[]) {
      final Object[] d = (Object[]) _dataArray;
      final int[] v = new int[d.length];
      for (int i = v.length - 1; i >= 0; i--) {
        v[i] = ((Integer) d[i]).intValue();
      }
      addAll(v, _c);
      return true;
    } else {
      (new Throwable()).printStackTrace();
    }
    return false;
  }

  protected class CommandAdd implements CtuluCommand {

    int idx_;

    int newValue_;

    /**
     * @param _idx l'indice d'insertion
     * @param _newV la valeur ajoutee
     */
    public CommandAdd(final int _idx, final int _newV) {
      idx_ = _idx;
      newValue_ = _newV;
    }

    @Override
    public void redo() {

      if (idx_ == list_.size()) {
        add(newValue_);
      } else {
        insert(idx_, newValue_);
      }
    }

    @Override
    public void undo() {
      remove(idx_);

    }

  }

  protected class CommandAddAll implements CtuluCommand {

    int idxBefore_;

    int[] newValue_;

    /**
     * @param _idx l'indice d'insert des valeurs
     * @param _newV les valeurs ajoutees
     */
    public CommandAddAll(final int _idx, final int[] _newV) {
      idxBefore_ = _idx;
      newValue_ = _newV;
    }

    /**
     * @overrride
     */
    @Override
    public void redo() {
      list_.add(newValue_);
      fireDataAdded();
    }

    /**
     * @overrride
     */
    @Override
    public void undo() {
      list_.remove(idxBefore_, newValue_.length);
      fireDataRemoved();

    }

  }

  protected class CommandRemove implements CtuluCommand {

    int idx_;

    int oldValue_;

    /**
     * @param _idx l'indice enleve
     * @param _oldV la valeur en cet indice
     */
    public CommandRemove(final int _idx, final int _oldV) {
      idx_ = _idx;
      oldValue_ = _oldV;
    }

    /**
     * @overrride
     */
    @Override
    public void redo() {
      remove(idx_);
    }

    /**
     * @overrride
     */
    @Override
    public void undo() {
      insert(idx_, oldValue_);
    }

  }

  protected class CommandRemoveAll implements CtuluCommand {

    CtuluListInteger memento_;

    /**
     * @param _m la sauvegarde de l'ancienne liste
     */
    public CommandRemoveAll(final CtuluListInteger _m) {
      memento_ = _m;
    }

    @Override
    public void redo() {
      removeAll();
    }

    @Override
    public void undo() {
      initWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireDataAdded();
    }
  }

  protected class CommandRemoveDangerous implements CtuluCommand {

    int[] idx_;

    int[] oldValues_;

    /**
     * @param _oldValues les valeurs aux indices enleves
     * @param _idx les indices enleves
     */
    public CommandRemoveDangerous(final int[] _oldValues, final int[] _idx) {
      oldValues_ = _oldValues;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      insertDangerous(idx_, oldValues_);
    }
  }

  protected class CommandRemoveMemento implements CtuluCommand {

    int[] idx_;

    CtuluListInteger memento_;

    /**
     * @param _m la sauvegarde
     * @param _idx les indices enleves
     */
    public CommandRemoveMemento(final CtuluListInteger _m, final int[] _idx) {
      memento_ = _m;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      initWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireDataAdded();
    }
  }

  protected TIntArrayList list_;

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  public CtuluListInteger() {
    this(0, 20);
  }

  protected void sort() {
    list_.sort();
  }

  /**
   * @return le tableau des indices
   */
  public int[] getArray() {
    return list_.toNativeArray();
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListInteger(final CtuluListInteger _init) {
    this(0, _init.getSize());
    initWith(_init);
  }

  /**
   * Pour creer un vecteur vide et ayant une capacite de 100 new(0,100). Pour creer un vecteur avec les 10 premiers
   * double a 0 et une capacite de 30 new(20,30)
   * 
   * @param _nb le nombre initial de double ajoute au vecteur
   * @param _initCapacity la capacite du veceur
   */
  public CtuluListInteger(final int _nb, final int _initCapacity) {
    int initCapacity = _initCapacity;
    if (initCapacity < _nb) {
      initCapacity = _nb;
    }
    list_ = new TIntArrayList(initCapacity);
    list_.add(new int[_nb]);
  }

  public CtuluListInteger(final int _initCapacity) {
    list_ = new TIntArrayList(_initCapacity);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListInteger(final int[] _init) {
    list_ = new TIntArrayList(_init);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListInteger(final TIntArrayList _init) {
    this(_init.toNativeArray());
  }

  protected void fireDataAdded() {

  }

  /**
   * Appele qui si des doubles sont modifies avec un set.
   */
  @Override
  protected void fireDataChanged() {

  }

  protected void fireDataRemoved() {}

  /**
   * Methode appele lorsque toutes les valeurs ont ete modifiee. Les methodes qui lancent cet evt sont initFrom.
   */
  protected void fireIntStructureChanged() {

  }

  protected void internalAdd(final int _v) {
    list_.add(_v);
  }

  protected void internalAdd(final int _i, final int _v) {
    if (_i == list_.size()) {
      list_.add(_v);
    } else {
      list_.insert(_i, _v);
    }
  }

  protected void internalRemove(final int _i) {
    list_.remove(_i);
  }

  @Override
  protected boolean internalSet(final int _i, final int _newV) {
    final double old = list_.getQuick(_i);
    if (old == _newV) { return false; }
    list_.setQuick(_i, _newV);
    return true;
  }

  public final void initWith(final CtuluListInteger _m, final boolean _throwEvent) {
    list_ = new TIntArrayList(_m.list_.size());
    list_.add(_m.list_.toNativeArray());
    if (_throwEvent) {
      fireIntStructureChanged();
    }
  }

  public final void initWith(final CtuluArrayInteger _m, final boolean _throwEvent) {
    list_ = new TIntArrayList(_m.list_.length);
    list_.add(_m.list_);
    if (_throwEvent) {
      fireIntStructureChanged();
    }
  }

  /**
   * Fonction a utiliser avec pr�caution: effectue des insert des values en suivant les valeurs dans l'ordre. Pour
   * fonctionner les points
   * 
   * @param _idx les indices dans l'ordre
   * @param _values les valeurs dans l'ordre
   * @return true
   */
  final boolean insertDangerous(final int[] _idx, final int[] _values) {
    if ((((((_idx == null) || (_values == null))) || (_idx.length == 0))) || (_values.length != _idx.length)) { return false; }
    final int n = _idx.length;
    for (int i = 0; i < n; i++) {
      insert(_idx[i], _values[i]);
    }
    fireDataAdded();
    return true;
  }

  /**
   * @param _x l'entier a ajouter
   * @return true si ajouter (toujours en general)
   */
  public final boolean add(final int _x) {
    return add(_x, null);
  }

  /**
   * @param _x l'entier a ajouter
   * @param _cmd le receveur de commande
   * @return true si ajouter (toujours en general)
   */
  public final boolean add(final int _x, final CtuluCommandContainer _cmd) {
    internalAdd(_x);
    if (_cmd != null) {
      _cmd.addCmd(new CommandAdd(list_.size() - 1, _x));
    }
    fireDataAdded();
    return true;
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    int d = 0;
    if (_data != null) {
      if (_data instanceof Integer) {
        d = ((Integer) _data).intValue();
      } else {
        try {
          d = Integer.parseInt(_data.toString());
        } catch (final NumberFormatException e) {

        }
      }
    }
    return add(d, _c);
  }

  /**
   * @param _d les indices a ajouter a la fin
   * @param _cmd le receveur de commande
   */
  public void addAll(final int[] _d, final CtuluCommandContainer _cmd) {
    final int size = list_.size();
    list_.add(_d);
    if (_cmd != null) {
      _cmd.addCmd(new CommandAddAll(size, CtuluLibArray.copy(_d)));
    }
    fireDataAdded();
  }

  /**
   * @return une liste de sauvegarde
   */
  public CtuluListInteger createMemento() {
    return new CtuluListInteger(this);
  }

  @Override
  public int getSize() {
    return list_.size();
  }

  /**
   * @param _intValue l'indice a tester
   * @return true si contenu
   */
  @Override
  public boolean contains(final int _intValue) {
    return list_.contains(_intValue);
  }

  public int indexOf(final int _intValue) {
    return list_.indexOf(_intValue);
  }

  /**
   * Le contenu de ce modele doit etre trie.
   * 
   * @param _intValue l'indice a rechercher
   * @return true si contenu
   */
  protected boolean binaryContains(final int _intValue) {
    return list_.binarySearch(_intValue) >= 0;
  }

  protected int binarySearch(final int _idxToTest) {
    return list_.binarySearch(_idxToTest);
  }

  /**
   * @param _i l'indice demande
   * @return la valeur en cet indice
   */
  @Override
  public int getValue(final int _i) {
    return list_.getQuick(_i);
  }

  /**
   * Reinitialise tout depuis _m et envoie un evt.
   * 
   * @param _m le modele depuis lequel l'initialisation sera effectuee
   */
  public final void initWith(final CtuluListInteger _m) {
    initWith(_m, true);
  }

  /**
   * Reinitialise la liste interne avec le tableau. Envoie un evt structure modifiee.
   * 
   * @param _m le tableau utilise pour l'intialisation
   */
  public final void initWith(final int[] _m, final boolean _event) {
    list_ = new TIntArrayList(_m);
    if (_event) {
      fireIntStructureChanged();
    }
  }

  @Override
  public final void initWith(final int[] _m) {
    initWith(_m, true);
  }

  /**
   * @param _i l'indice [0,size()] si _i==size(), il sera ajoute a la fin
   * @param _x la valeur a ajouter
   * @return true si inserer
   */
  public final boolean insert(final int _i, final int _x) {
    return insert(_i, _x, null);
  }

  /**
   * @param _i l'indice [0,size()] si _i==size(), il sera ajoute a la fin
   * @param _x la valeur a ajouter
   * @param _cmd le receveur da la commande. Si null pas de commande creee.
   * @return true si inserer
   */
  public boolean insert(final int _i, final int _x, final CtuluCommandContainer _cmd) {
    if ((_i < 0) || (_i > list_.size())) { return false; }
    internalAdd(_i, _x);
    if (_cmd != null) {
      _cmd.addCmd(new CommandAdd(_i, _x));
    }
    fireDataAdded();
    return true;
  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    int d = 0;
    if (_data instanceof Integer) {
      d = ((Integer) _data).intValue();
    }
    return insert(_i, d, _c);
  }

  /**
   * Compare le modele avec un tableau d'entier.
   * 
   * @param _idx le tableau a comparer
   * @return true si les indices de ce modele sont identique au tableau _i
   */
  @Override
  public boolean isSameValues(final int[] _idx) {
    if (_idx == null) { return isAllSameValue(); }
    if (_idx.length == 0) { return false; }
    final int r = list_.getQuick(_idx[0]);
    for (int i = _idx.length - 1; i > 0; i--) {
      if (list_.getQuick(_idx[i]) != r) { return false; }
    }
    return true;
  }

  /**
   * @param _i l'indice a enlever
   * @return true si maj
   */
  public boolean remove(final int _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    if ((_i >= 0) && (_i < list_.size())) {
      final int xRemoved = list_.remove(_i);
      if (_c != null) {
        _c.addCmd(new CommandRemove(_i, xRemoved));
      }
      fireDataRemoved();
      return true;
    }
    return false;
  }

  /**
   * @param _idx les indices a enlever et non pas les valeurs
   * @return true si modif
   * @see #remove(int[], CtuluCommandContainer)
   */
  public boolean remove(final int[] _idx) {
    return remove(_idx, null);
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return remove(_i, _c, false);
  }

  /**
   * Enleve les indices appartenant au tableau _i. Attention: aucune copie du tableau _i n'est effectuee. De plus il est
   * suppose que le tableau est correct : les indices appartiennent � [0,size()[ et tous differents.
   * 
   * @param _idx les indices a enlever et non pas les valeurs
   * @param _c la receveur de commande
   * @param _forceMemento si true un tableau de sauvegarde sera effectue automatiquement. Sinon seules les indices
   *          enleves sont sauves ( peut-etre dangereux)
   * @return true si modif
   */
  public boolean remove(final int[] _idx, final CtuluCommandContainer _c, final boolean _forceMemento) {
    if (_idx == null || _idx.length == 0) { return false; }
    final int n = _idx.length;
    if (n == 1) { return remove(_idx[0], _c); }
    final int initSize = list_.size();
    final int[] newArray = new int[initSize];
    int idx = 0;
    final int[] oldV = new int[_idx.length];
    int idxOldV = 0;
    // on copie le debut du tableau
    list_.toNativeArray(newArray, 0, _idx[0]);
    idx = _idx[0];
    final int maxI = _idx[_idx.length - 1];
    for (int i = idx; i <= maxI; i++) {
      if (Arrays.binarySearch(_idx, i) < 0) {
        newArray[idx++] = list_.getQuick(i);
      } else {
        final int d = list_.getQuick(i);
        oldV[idxOldV++] = d;
      }
    }
    for (int i = maxI + 1; i < initSize; i++) {
      newArray[idx++] = list_.getQuick(i);
    }
    if (idx + idxOldV == initSize) {
      if (_c != null) {
        if (_forceMemento || idxOldV > 10) {
          _c.addCmd(new CommandRemoveMemento(createMemento(), _idx));
        } else {
          _c.addCmd(new CommandRemoveDangerous(oldV, _idx));
        }
      }

      list_.resetQuick();
      list_.add(newArray, 0, idx);
      fireDataRemoved();
      return true;
    }
    return false;

  }

  /**
   * @see #removeAll(CtuluCommandContainer)
   */
  public final void removeAll() {
    removeAll(null);
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {
    if (_c != null) {
      _c.addCmd(new CommandRemoveAll(createMemento()));
    }
    list_.resetQuick();
    fireDataRemoved();
  }

  @Override
  public final void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluArrayInteger) {
      initWith((CtuluArrayInteger) _model, false);
    }
    if (_model instanceof CtuluListInteger) {
      initWith((CtuluListInteger) _model, false);
    }
  }

}
