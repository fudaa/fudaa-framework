/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-04-30 14:21:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluRange;

/**
 * Une classe contenant une seule valeur.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluCollectionDoubleUnique.java,v 1.2 2007-04-30 14:21:16 deniger Exp $
 */
public class CtuluCollectionDoubleUnique extends CtuluCollectionDoubleAbstract implements CtuluCollectionDoubleEdit {

  final int nbValues_;

  final double v_;

  public CtuluCollectionDoubleUnique() {
    this(0);
  }

  /**
   * @param _v
   */
  public CtuluCollectionDoubleUnique(final double _v) {
    this(_v, 0);
  }

  public CtuluCollectionDoubleUnique(final double _v, final int _nbValue) {
    v_ = _v;
    nbValues_ = _nbValue;
  }

  public void addAll(final double[] _d, final CtuluCommandContainer _cmd) {}

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    return true;
  }

  public CtuluCollectionDoubleEdit createListMemento() {
    return new CtuluCollectionDoubleUnique(v_);
  }

  public CtuluCollectionDouble getDoubleList() {
    return null;
  }

  @Override
  public Object getObjectValueAt(final int _i) {
    return null;
  }

  @Override
  public Object[] getObjectValues() {
    return null;
  }

  @Override
  public void getRange(final CtuluRange _r) {
    _r.max_ = v_;
    _r.min_ = v_;
  }

  @Override
  public int getSize() {
    return nbValues_;
  }

  @Override
  public double getValue(final int _i) {
    return v_;
  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {}

  public final void initWith(final CtuluListDouble _m) {}

  public final void initWith(final double[] _m) {}

  @Override
  public void initWithDouble(final CtuluCollectionDouble _m, final boolean _throwEvent) {}

  public boolean insert(final int _i, final double _x, final CtuluCommandContainer _cmd) {
    return true;
  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return true;
  }

  @Override
  public boolean isSameValues(final int[] _i) {
    return true;
  }

  public boolean remove(final int _i) {
    return true;
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    return true;
  }

  public boolean remove(final int[] _i) {
    return true;
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return true;
  }

  public boolean remove(final int[] _i, final CtuluCommandContainer _c, final boolean _forceMemento) {
    return true;
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {}

  @Override
  public boolean set(final int _i, final double _newV, final CtuluCommandContainer _c) {
    return true;
  }

  @Override
  public boolean set(final int[] _i, final double _newV, final CtuluCommandContainer _c) {
    return true;
  }

  public boolean set(final int[] _i, final double[] _newV) {
    return true;
  }

  @Override
  public boolean set(final int[] _i, final double[] _newV, final CtuluCommandContainer _c) {
    return true;
  }

  @Override
  public boolean setAll(final double[] _newV, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean setAll(final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean setAllObject(final int[] _i, final Object[] _data, final CtuluCommandContainer _c) {
    return false;
  }

  @Override
  public boolean setObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    return true;
  }

  @Override
  public boolean setObject(final int[] _i, final Object _data, final CtuluCommandContainer _c) {
    return false;
  }
}