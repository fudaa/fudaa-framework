/*
 * @creation 22 sept. 2004
 * 
 * @modification $Date: 2007-01-10 08:58:48 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double. Fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 * 
 * @author Fred Deniger
 * @version $Id: CtuluListObject.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluListObject extends CtuluCollectionObjectAbstract implements CtuluCollection {

  @Override
  public Object[] getObjectValues() {
    return list_.toArray();
  }

  @Override
  public boolean isAllSameValue() {
    final Object r = getValueAt(0);
    for (int i = getSize() - 1; i > 0; i--) {
      final Object ri = getValueAt(i);
      if (r == null) {
        if (ri != null) { return false;
        // r n'est pas null car sinon rIsNull=true
        }
      } else if (!r.equals(ri)) { return false; }
    }
    return true;
  }

  @Override
  public boolean isSameValues(final int[] _idx) {
    if (_idx == null) { return isAllSameValue(); }
    if (_idx.length == 0) { return false; }
    final Object r = getValueAt(_idx[0]);
    for (int i = _idx.length - 1; i > 0; i--) {
      final Object ri = getValueAt(_idx[i]);
      if (r == null) {
        if (ri != null) { return false; }
      } else if (!r.equals(ri)) { return false; }

    }
    return true;
  }

  protected class CommandAdd implements CtuluCommand {

    int idx_;

    Object newValue_;

    public CommandAdd(final int _idx, final Object _newV) {
      idx_ = _idx;
      newValue_ = _newV;
    }

    /**
     * @overrride
     */
    @Override
    public void redo() {

      if (idx_ == list_.size()) {
        internalAdd(newValue_);
      } else {
        internalAdd(idx_, newValue_);
      }
      fireObjectAdded(idx_, newValue_);
    }

    /**
     * @overrride
     */
    @Override
    public void undo() {
      internalRemove(idx_);
      fireObjectRemoved(idx_, newValue_);

    }
  }

  protected class CommandAddAll implements CtuluCommand {

    int idxBefore_;

    Object[] newValue_;

    protected CommandAddAll(final int _idx, final Object[] _newV) {
      idxBefore_ = _idx;
      newValue_ = _newV;
    }

    /**
     * @overrride
     */
    @Override
    public void redo() {
      final int idx = list_.size();
      list_.addAll(Arrays.asList(newValue_));
      fireObjectAdded(-1, null);
    }

    /**
     * @overrride
     */
    @Override
    public void undo() {
      CtuluLibArray.remove(list_, idxBefore_, newValue_.length);
      fireObjectRemoved(-1, null);
    }

  }

  protected class CommandRemove implements CtuluCommand {

    int idx_;

    Object oldValue_;

    public CommandRemove(final int _idx, final Object _newV) {
      idx_ = _idx;
      oldValue_ = _newV;
    }

    /**
     * @overrride
     */
    @Override
    public void redo() {
      internalRemove(idx_);
      fireObjectRemoved(idx_, oldValue_);
    }

    /**
     * @overrride
     */
    @Override
    public void undo() {
      internalAdd(idx_, oldValue_);
      fireObjectAdded(idx_, oldValue_);
    }

  }

  protected class CommandRemoveAll implements CtuluCommand {

    CtuluListObject memento_;

    public CommandRemoveAll(final CtuluListObject _m) {
      memento_ = _m;
    }

    @Override
    public void redo() {
      internalRemoveAll();
      fireObjectRemoved(-1, null);
    }

    @Override
    public void undo() {
      initWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireObjectAdded(-1, null);
    }
  }

  protected class CommandRemoveDangerous implements CtuluCommand {

    int[] idx_;

    Object[] oldValues_;

    public CommandRemoveDangerous(final Object[] _oldValues, final int[] _idx) {
      oldValues_ = _oldValues;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      /*
       * Losqu'on supprime un indice de la liste, �a d�cale tous les indices
       * suivants de -1. Donc pour pouvoir supprimer plusieurs indices de fa�on
       * coh�rente, il faut commencer par la fin de la liste. Contrairement �
       * l'ajout.
       */
      for(int i=idx_.length-1;i>=0;i--)
        internalRemove(idx_[i]);
      fireObjectRemoved(-1, null);
    }

    @Override
    public void undo() {
      /*
       * Losqu'on ajoute un indice de la liste, �a d�cale tous les indices
       * suivants de +1. Donc pour pouvoir ajouter plusieurs indices de fa�on
       * coh�rente, il faut commencer par la d�but de la liste. Contrairement �
       * la supression.
       */
      for(int i=0;i<idx_.length;i++)
        internalAdd(idx_[i], oldValues_[i]);
      fireObjectAdded(-1, null);
    }
  }

  protected class CommandRemoveMemento implements CtuluCommand {

    int[] idx_;

    CtuluListObject memento_;

    public CommandRemoveMemento(final CtuluListObject _m, final int[] _idx) {
      memento_ = _m;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      initWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireObjectAdded(-1, null);
    }
  }

  protected List list_;

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  public CtuluListObject() {
    this(20);
  }

  protected void sort() {
    Collections.sort(list_);
  }

  /**
   * Fait appel a la methode List.toArray.
   * 
   * @param _o le tableau a completer
   * @return le tableau complete ou le tableau redimensionne et complete
   * @see List#toArray(java.lang.Object[])
   */
  public Object[] toArray(final Object[] _o) {
    return list_.toArray(_o);
  }

  /**
   * @param _o l'objet a tester
   * @return l'indice de l'objet. -1 si non trouve.
   */
  @Override
  public int indexOf(final Object _o) {
    return list_.indexOf(_o);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListObject(final Collection _init) {
    list_ = new ArrayList(_init);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListObject(final CtuluListObject _init) {
    this(_init.list_);
  }

  /**
   * @param _nb taille initiales
   */
  public CtuluListObject(final int _nb) {
    list_ = new ArrayList(_nb);
  }

  protected void fireObjectAdded(int _newIdx, Object _newGeom) {
    fireObjectChanged(_newIdx, _newGeom);
  }

  @Override
  protected void fireObjectChanged(int _oldIdx, Object _oldGeom) {
  }

  protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
    fireObjectChanged(_oldIdx, _oldGeom);
  }

  protected void fireObjectModified(int _idx, Object _geom) {
    fireObjectChanged(_idx, _geom);
  }

  protected void internalAdd(final Object _v) {
    list_.add(_v);
  }

  protected final void internalAdd(final int _i, final Object _v) {
    if (_i == list_.size()) {
      list_.add(_v);
    } else {
      list_.add(_i, _v);
    }
  }

  protected final Object internalRemove(final int _i) {
    return list_.remove(_i);
  }

  @Override
  protected final boolean internalSet(final int _i, final Object _newV) {
    if (_newV != list_.get(_i)) {
      list_.set(_i, _newV);
      return true;
    }
    return false;
  }

  public void initWith(final CtuluListObject _m, final boolean _throwEvent) {
    list_ = new ArrayList(_m.list_);
    if (_throwEvent) {
      fireObjectAdded(-1, null);
    }
  }

  public void initWith(final CtuluArrayObject _m, final boolean _throwEvent) {
    list_ = new ArrayList(Arrays.asList(_m.list_));
    if (_throwEvent) {
      fireObjectAdded(-1, null);
    }
  }

  /**
   * Fonction a utiliser avec pr�caution: effectue des insert des values en suivant les valeurs dans l'ordre. Pour
   * fonctionner les points
   * 
   * @param _idx les indices dans l'ordre
   * @param _values les valeurs dans l'ordre
   * @return true
   */
  final boolean insertDangerous(final int[] _idx, final Object[] _values) {
    if (_idx == null || _values == null || _idx.length == 0 || _values.length != _idx.length) { return false; }
    final int n = _idx.length;
    for (int i = 0; i < n; i++) {
      insert(_idx[i], _values[i]);
    }
    fireObjectAdded(-1, null);
    return true;
  }

  public boolean canAdd(final Object _o) {
    return true;
  }

  @Override
  public final boolean addObject(final Object _obj, final CtuluCommandContainer _cmd) {
    return add(_obj, null, _cmd);
  }

  public final boolean add(final Object _obj, final List _o, final CtuluCommandContainer _cmd) {
    if (canAdd(_obj)) {
      internalAdd(_obj);
      CtuluCommandCompositeInverse cmd = null;
      if (_cmd != null) {
        cmd = new CtuluCommandCompositeInverse();
      }
      internActionPointAdded(_o, cmd, 1);
      if (_cmd != null && cmd != null) {
        cmd.addCmd(new CommandAdd(list_.size() - 1, _obj));
        _cmd.addCmd(cmd.getSimplify());
      }
      fireObjectAdded(list_.size()-1, _obj);
      return true;
    }
    return false;
  }

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    if (_dataArray instanceof CtuluCollection) { return addAllObject(((CtuluCollection) _dataArray).getObjectValues(),
        _c); }
    return addAll((Object[]) _dataArray, null, _c);
  }

  public final boolean addAll(final Object[] _obj, final List _o, final CtuluCommandContainer _cmd) {
    if (canAdd(_obj)) {
      if (_obj.length>0) {
        final int idx=list_.size();
        list_.addAll(Arrays.asList(_obj));
        CtuluCommandCompositeInverse cmd=null;
        if (_cmd!=null) {
          cmd=new CtuluCommandCompositeInverse();
        }
        internActionPointAdded(_o, cmd, _obj.length);
        if (_cmd!=null&&cmd!=null) {
          cmd.addCmd(new CommandAddAll(idx, _obj));
          _cmd.addCmd(cmd.getSimplify());
        }
        fireObjectAdded(-1, null);
      }
      return true;
    }
    return false;
  }

  /**
   * @param firstIdx l'indice a utiliser dans le premier eleement du tableau
   * @param size la taille du tableau.
   * @return tableau d'entier consecutif et de taille size
   */
  private int[] createIdx(final int firstIdx, final int size) {
    final int[] res = new int[size];
    for (int i = 0; i < res.length; i++) {
      res[i] = firstIdx + i;
    }
    return res;
  }

  protected final boolean add(final Object _x) {
    return addObject(_x, null);
  }

  protected CtuluListObject createMemento() {
    return new CtuluListObject(this);
  }

  @Override
  public int getSize() {
    return list_.size();
  }

  @Override
  public Object getValueAt(final int _i) {
    return list_.get(_i);
  }

  public final boolean insert(final int _i, final Object _x) {
    return insertObject(_i, _x, null);
  }

  @Override
  public boolean insertObject(final int _i, final Object _x, final CtuluCommandContainer _c) {
    return insert(_i, _x, null, _c);

  }

  /**
   * @param _i l'indice [0,size()] si _i==size(), il sera ajoute a la fin
   * @param _x la valeur a ajouter
   * @param _c le receveur da la commande. Si null pas de commande creee.
   * @return true si inserer
   */
  public boolean insert(final int _i, final Object _x, final List _o, final CtuluCommandContainer _c) {
    if (canAdd(_x)) {
      internalAdd(_i, _x);
      CtuluCommandCompositeInverse cmd = null;
      if (_c != null) {
        cmd = new CtuluCommandCompositeInverse();
      }
      internActionPointInserted(_i, _o, cmd);
      if (_c != null && cmd != null) {
        cmd.addCmd(new CommandAdd(_i, _x));
        _c.addCmd(cmd.getSimplify());
      }

      fireObjectAdded(_i, _x);
      return true;
    }
    return false;
  }

  public boolean remove(final int _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    final Object old = internalRemove(_i);
    CtuluCommandCompositeInverse cmd = null;
    if (_c != null) {
      cmd = new CtuluCommandCompositeInverse();
    }
    internActionPointRemoved(_i, cmd);
    if (_c != null && cmd != null) {
      cmd.addCmd(new CommandRemove(_i, old));
      _c.addCmd(cmd.getSimplify());
    }
    fireObjectRemoved(_i, old);
    return true;
  }

  /**
   * @param _i
   * @return true si modif
   * @see #remove(int[], CtuluCommandContainer)
   */
  public boolean remove(final int[] _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return remove(_i, _c, false);
  }

  /**
   * Enleve les indices appartenant au tableau _i. De plus il est suppose que le tableau est correct : les indices
   * appartiennent � [0,size()[ et tous differents.
   * 
   * @param _idx les indices
   * @param _cmd le receveur de commandes
   * @param _forceMemento true si memento global
   * @return true si modif
   */
  public boolean remove(final int[] _idx, final CtuluCommandContainer _cmd, final boolean _forceMemento) {
    if (_idx == null || _idx.length == 0) { return false; }
    final int[] iToRemove = CtuluLibArray.copy(_idx);
    Arrays.sort(iToRemove);

    final int n = iToRemove.length;
    if (n == 1) { return remove(iToRemove[0], _cmd); }
    CtuluCommandComposite commandComposite = null;
    if (_cmd != null) {
      commandComposite = new CtuluCommandComposite();
    }
    final int initSize = list_.size();
    final Object[] newArray = new Object[initSize];
    int idx = 0;
    final Object[] oldV = new Object[iToRemove.length];
    int idxOldV = 0;
    // on copie le debut du tableau
    for (int i = 0; i < iToRemove[0]; i++) {
      newArray[i] = list_.get(i);
    }
    // On enl�ve les valeurs � supprimer et on les place dans oldV[]
    idx = iToRemove[0];
    final int maxI = iToRemove[iToRemove.length - 1];
    for (int i = idx; i <= maxI; i++) {
      if (Arrays.binarySearch(iToRemove, i) < 0) {
        newArray[idx++] = list_.get(i);
      } else {
        final Object d = list_.get(i);
        oldV[idxOldV++] = d;
      }
    }
    // on copie la fin du tableau
    for (int i = maxI + 1; i < initSize; i++) {
      newArray[idx++] = list_.get(i);
    }
    CtuluListObject memento = null;
    if (_forceMemento || idxOldV > 10) {
      memento = createMemento();
    }

    list_.clear();
    for (int i = 0; i < idx; i++) {
      list_.add(newArray[i]);
    }
    if (idx + idxOldV == initSize) {
      if (commandComposite != null) {
        if (_forceMemento || idxOldV > 10) {
          commandComposite.addCmd(new CommandRemoveMemento(memento, iToRemove));
        } else {
          commandComposite.addCmd(new CommandRemoveDangerous(oldV, iToRemove));
        }
      }
      internActionPointRemoved(iToRemove, commandComposite);
      fireObjectRemoved(-1, null);
      if (_cmd != null && commandComposite != null) {
        _cmd.addCmd(commandComposite.getSimplify());
      }
      return true;
    }
    return false;

  }

  /**
   * @see #removeAll(CtuluCommandContainer)
   */
  public final void removeAll() {
    removeAll(null);
  }

  protected void internalRemoveAll() {
    list_.clear();
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {
    CtuluCommandCompositeInverse cmd = null;
    if (_c != null) {
      cmd = new CtuluCommandCompositeInverse();
    }
    internActionCleared(cmd);
    if (_c != null && cmd != null) {
      cmd.addCmd(new CommandRemoveAll(createMemento()));
    }
    internalRemoveAll();
    if (_c != null && cmd != null) {
      _c.addCmd(cmd.getSimplify());
    }
    fireObjectRemoved(-1, null);
  }

  protected void internActionCleared(final CtuluCommandContainer _c) {

  }

  /**
   * Methode appelee avant firePointAdded et juste apres l'insertion. A redefinir pour ajouter des commandes ou pour
   * mettre a jour d'autres donnees.
   * 
   * @param _c le conteneur de commande
   * @param _nbValues true si plusieurs objets ajouter en meme: dans ce cas la liste doit contenir des tableaux
   * @param _i l'index d'insertion
   */
  protected void internActionPointAdded(final List _values, final CtuluCommandContainer _c, final int _nbValues) {}

  /**
   * Methode appelee avant firePointAdded et juste apres l'insertion. A redefinir pour ajouter des commandes ou pour
   * mettre a jour d'autres donnees.
   * 
   * @param _c le conteneur de commande
   * @param _i l'index d'insertion
   */
  protected void internActionPointInserted(final int _i, final List _values, final CtuluCommandContainer _c) {}

  /**
   * Methode appelee avant firePointRemoved et juste apres la suppression. A redefinir pour ajouter des commandes ou
   * pour mettre a jour d'autres donnees.
   * 
   * @param _c le conteneur de commande
   * @param _i l'index a supprimer
   */
  protected void internActionPointRemoved(final int _i, final CtuluCommandContainer _c) {

  }

  /**
   * @param _i les indices enleves
   * @param _c la commande
   */
  protected void internActionPointRemoved(final int[] _i, final CtuluCommandContainer _c) {

  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluListObject) {
      initWith((CtuluListObject) _model, false);
    } else if (_model instanceof CtuluArrayObject) {
      initWith((CtuluArrayObject) _model, false);
    }
  }

  @Override
  protected int[] createAllIdx() {
    final int[] all = new int[getSize()];
    for (int i = 0; i < all.length; i++) {
      all[i] = i;
    }
    return all;
  }

}
