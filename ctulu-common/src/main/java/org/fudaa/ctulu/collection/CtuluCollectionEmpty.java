package org.fudaa.ctulu.collection;

/**
 * Several empty collections
 * 
 * @author deniger
 */
public final class CtuluCollectionEmpty {

  /**
   * Empty collection of double
   */
  public static final CtuluCollectionDouble DOUBLE = new CtuluCollectionDoubleAbstract() {

    @Override
    public double getValue(int i) {
      return 0;
    }

    @Override
    public int getSize() {
      return 0;
    }
  };
  /**
   * Empty collection of integer
   */
  public static final CtuluCollectionInteger INTEGER = new CtuluCollectionInteger() {

    @Override
    public int getValue(int i) {
      return 0;
    }

    @Override
    public int getSize() {
      return 0;
    }
  };
  /**
   * Empty collection of long
   */
  public static final CtuluCollectionLong LONG = new CtuluCollectionLong() {

    @Override
    public long getValue(int i) {
      return 0;
    }

    @Override
    public int getSize() {
      return 0;
    }
  };
}
