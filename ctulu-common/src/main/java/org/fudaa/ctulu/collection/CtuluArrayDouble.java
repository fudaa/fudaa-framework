/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement.
 *
 * @author Fred Deniger
 * @version $Id: CtuluArrayDouble.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluArrayDouble extends CtuluCollectionDoubleEditAbstract {

  protected double[] list_;

  @Override
  public int getSize() {
    return list_.length;
  }

  protected CtuluArrayDouble() {
    this(0);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayDouble(final CtuluArrayDouble _init) {
    if (_init == null) {
      list_ = new double[0];
    } else {
      list_ = new double[_init.getSize()];
      initWith(_init, false);
    }
  }

  /**
   * @param _init si non null est utilise pour l'initialisation
   */
  public CtuluArrayDouble(final CtuluCollectionDouble _init) {
    if (_init == null) {
      list_ = new double[0];
    } else {
      list_ = new double[_init.getSize()];
      for (int i = list_.length - 1; i >= 0; i--) {
        list_[i] = _init.getValue(i);
      }
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayDouble(final double[] _init) {
    list_ = CtuluLibArray.copy(_init);
  }

  /**
   * @param _nb taille initiales
   */
  public CtuluArrayDouble(final int _nb) {
    list_ = new double[_nb];
  }

  public CtuluArrayDouble(final Object[] _init) {
    if (_init == null) {
      list_ = new double[0];
    } else {
      list_ = new double[_init.length];
      for (int i = list_.length - 1; i >= 0; i--) {
        if (_init[i] != null) {
          list_[i] = ((Double) _init[i]).doubleValue();
        }
      }
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayDouble(final TDoubleArrayList _init) {
    this(_init.toNativeArray());
  }

  protected void initWith(final CtuluArrayDouble _m) {
    initWith(_m, true);
  }

  @Override
  protected void internalSetValue(final int _i, final double _newV) {
    if (_newV == list_[_i]) {
      return;
    }
    final double old = list_[_i];
    list_[_i] = _newV;
    if ((r_ != null) && ((!r_.isValueStrictlyContained(old)))) {
      r_ = null;
    }
    if ((r_ != null) && ((!r_.isValueContained(_newV)))) {
      if (_newV > r_.max_) {
        r_.max_ = _newV;
      }
      if (_newV < r_.min_) {
        r_.min_ = _newV;
      }
    }
  }

  @Override
  public CtuluArrayDouble createMemento() {
    return new CtuluArrayDouble(this);
  }

  @Override
  public double getValue(final int _i) {
    return list_[_i];
  }

  @Override
  public double[] getValues() {
    return CtuluLibArray.copy(list_);
  }

  public final void initWith(final CtuluArrayDouble _m, final boolean _throwEvent) {
    if (_m == null) {
      return;
    }
    if (list_.length != _m.list_.length) {
      FuLog.error((getClass()).getName() + " Taille differente attention");
    }
    list_ = CtuluLibArray.copy(_m.list_);
    r_ = _m.r_;
    if (_throwEvent) {
      fireObjectChanged(-1, null);
    }
  }

  public void initWith(final CtuluListDouble _m, final boolean _throwEvent) {
    if (_m == null) {
      return;
    }
    if (list_.length != _m.getSize()) {
      FuLog.error((getClass()).getName() + " Taille differente attention");
    }
    list_ = _m.list_.toNativeArray();
    r_ = _m.r_;
    if (_throwEvent) {
      fireObjectChanged(-1, null);
    }
  }

  /**
   * @param _a le tableau a tester
   * @return true si memes valeurs
   */
  public boolean isEquivalentTo(final CtuluArrayDouble _a) {
    if (_a == null) {
      return false;
    }
    return Arrays.equals(this.list_, _a.list_);
  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluArrayDouble) {
      initWith((CtuluArrayDouble) _model, false);
    } else {
      super.initWith(_model, _throwEvent);
    }
  }
}
