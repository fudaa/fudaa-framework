/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-03-23 17:16:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double. en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 *
 * @author Fred Deniger
 * @version $Id: CtuluArrayInteger.java,v 1.2 2007-03-23 17:16:17 deniger Exp $
 */
public class CtuluArrayInteger extends CtuluCollectionIntegerAbstract {


  protected int[] list_;

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayInteger(final CtuluArrayInteger _init) {
    this(_init.getSize());
    initWith(_init);
  }

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   *
   * @param _nb la taille du modele
   */
  public CtuluArrayInteger(final int _nb) {
    list_ = new int[_nb];
  }

  public CtuluArrayInteger() {
    this(0);
  }

  public CtuluArrayInteger(final int[] _init) {
    list_ = CtuluLibArray.copy(_init);
  }

  public CtuluArrayInteger(final Object[] _init) {
    if (_init == null) {
      list_ = new int[0];
    } else {
      list_ = new int[_init.length];
      for (int i = list_.length - 1; i >= 0; i--) {
        list_[i] = ((Integer) _init[i]).intValue();
      }
    }
  }


  @Override
  protected boolean internalSet(final int _i, final int _newV) {
    final double old = list_[_i];
    if (old == _newV) {
      return false;
    }
    list_[_i] = _newV;
    return true;
  }

  public final void initWith(final CtuluArrayInteger _m, final boolean _throwEvent) {
    list_ = CtuluLibArray.copy(_m.list_);
    if (_throwEvent) {
      fireDataStructureChanged();
    }
  }

  public final void initWith(final CtuluListInteger _m, final boolean _throwEvent) {
    list_ = _m.getArray();
    if (_throwEvent) {
      fireDataStructureChanged();
    }
  }

  /**
   * @param _idxToTest l'indice a tester
   * @return true si contenu
   */
  @Override
  public boolean contains(final int _idxToTest) {
    return CtuluLibArray.findInt(list_, _idxToTest) >= 0;
  }

  /**
   * @return une liste de sauvegarde
   */
  public CtuluArrayInteger createMemento() {
    return new CtuluArrayInteger(this);
  }

  @Override
  public final int getSize() {
    return list_.length;
  }

  /**
   * @param _i l'indice demande
   * @return la valeur en cet indice
   */
  @Override
  public int getValue(final int _i) {
    return list_[_i];
  }

  /**
   * Reinitialise tout depuis _m et envoie un evt.
   *
   * @param _m le modele depuis lequel l'initialisation sera effectuee
   */
  public final void initWith(final CtuluArrayInteger _m) {
    initWith(_m, true);
  }

  /**
   * Reinitialise la liste interne avec le tableau. Envoie un evt structure modifiee.
   *
   * @param _m le tableau utilise pour l'intialisation
   */
  @Override
  public final void initWith(final int[] _m) {
    list_ = CtuluLibArray.copy(_m);
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    return false;
  }

  public static int getInteger(final Object _data) throws NumberFormatException {
    if (_data instanceof Number) {
      return ((Number) _data).intValue();
    } else if (_data == null) {
      throw new NumberFormatException("integer is null");
    } else {
      return Integer.parseInt(_data.toString());
    }
  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluArrayInteger) {
      initWith((CtuluArrayInteger) _model, false);
    }
    if (_model instanceof CtuluListInteger) {
      initWith((CtuluListInteger) _model, false);
    }
  }
}
