/*
 *  @creation     2 mai 2005
 *  @modification $Date: 2007-01-10 08:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

/**
 * @author Fred Deniger
 * @version $Id: CtuluDoubleVisitor.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public interface CtuluDoubleVisitor {

  /**
   * @param _idxPt l'indice du point
   * @param _v la valeur au point _idxPt
   * @return false si on doit arreter la visite
   */
  boolean accept(int _idxPt, double _v);

}
