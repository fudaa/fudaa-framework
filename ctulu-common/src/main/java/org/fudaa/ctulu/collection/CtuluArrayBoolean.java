/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double. En ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 * @author Fred Deniger
 * @version $Id: CtuluArrayBoolean.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluArrayBoolean extends CtuluCollectionBooleanAbstract {

  protected boolean[] list_;

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayBoolean(final boolean[] _init) {
    list_ = CtuluLibArray.copy(_init);
  }

  public CtuluArrayBoolean() {
    this(0);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayBoolean(final Object[] _init) {
    if (_init == null) {
      list_ = new boolean[0];
    } else {
      list_ = new boolean[_init.length];
      for (int i = list_.length - 1; i >= 0; i--) {
        list_[i] = ((Boolean) _init[i]).booleanValue();
      }
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluArrayBoolean(final CtuluArrayBoolean _init) {
    if (_init == null) {
      list_ = new boolean[0];
    } else {
      list_ = new boolean[_init.getSize()];
      initWith(_init, false);
    }
  }

  /**
   * @param _nb taille initiales
   */
  public CtuluArrayBoolean(final int _nb) {
    list_ = new boolean[_nb];
  }

  @Override
  protected  void fireObjectChanged(int _indexGeom, Object _newValue){

  }

  protected void initWith(final CtuluArrayBoolean _m){
    initWith(_m, true);
  }

  @Override
  protected boolean internalSet(final int _i,final boolean _newV){
    if (list_[_i] == _newV) {
      return false;
    }
    list_[_i] = _newV;
    return true;
  }

  public CtuluArrayBoolean createMemento(){
    return new CtuluArrayBoolean(this);
  }

  @Override
  public final int getSize(){
    return list_.length;
  }

  @Override
  public boolean getValue(final int _i){
    return list_[_i];
  }

  @Override
  public boolean[] getValues(){
    return CtuluLibArray.copy(list_);
  }

  @Override
  public Object[] getObjectValues(){
    final Boolean[] r = new Boolean[list_.length];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = Boolean.valueOf(list_[i]);
    }
    return r;
  }

  public final void initWith(final CtuluArrayBoolean _m,final boolean _throwEvent){
    if (_m == null) {
      return;
    }
    if (list_.length != _m.list_.length) {
      FuLog.error((getClass()).getName() + " Taille differente attention");
    }
    list_ = CtuluLibArray.copy(_m.list_);
    if (_throwEvent) {
      fireObjectChanged(-1, null);
    }
  }

  public final void initWith(final CtuluCollectionBooleanInterface _m,final boolean _throwEvent){
    if (_m instanceof CtuluArrayBoolean) {
      initWith((CtuluArrayBoolean) _m, _throwEvent);
      return;
    }
    if (_m == null) {
      return;
    }
    if (list_.length != _m.getSize()) {
      FuLog.error((getClass()).getName() + " Taille differente attention");
      list_ = new boolean[_m.getSize()];
    }
    for (int i = list_.length - 1; i >= 0; i--) {
      list_[i] = _m.getValue(i);
    }
    if (_throwEvent) {
      fireObjectChanged(-1, null);
    }
  }


  @Override
  public void initWith(final CtuluCollection _model,final boolean _throwEvent){
    if (_model instanceof CtuluArrayBoolean) {
      initWith((CtuluArrayBoolean) _model);
    }
    else if (_model instanceof CtuluCollectionBooleanInterface) {
      initWith((CtuluCollectionBooleanInterface) _model, false);
    }
  }
}
