/*
 *  @creation     22 sept. 2004
 *  @modification $Date: 2007-01-10 08:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

/**
 * @author Fred Deniger
 * @version $Id: CtuluCollectionInteger.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public interface CtuluCollectionInteger {

  /**
   * @return la taille
   */
  int getSize();

  /**
   * @param _i l'indice [0;size()[
   * @return la valeur en ce point
   */
  int getValue(int _i);


}
