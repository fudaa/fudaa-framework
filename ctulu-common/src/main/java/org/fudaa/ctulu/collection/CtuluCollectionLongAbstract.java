/*
 * @creation 22 sept. 2004
 * 
 * @modification $Date: 2007-05-04 13:43:25 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de double. en ajoutant des fonctionnalites sup: <br>
 * Undo/Redo Max/min enregistrer automatiquement
 * 
 * @author Fred Deniger
 * @version $Id: CtuluCollectionIntegerAbstract.java,v 1.3 2007-05-04 13:43:25 deniger Exp $
 */
public abstract class CtuluCollectionLongAbstract extends CtuluCollectionAbstract implements CtuluCollectionLong {

  protected class CommandSet implements CtuluCommand {

    int idx_;

    long newI_;

    long oldI_;

    /**
     * @param _newV la nouvelle valeurs
     * @param _oldi les anciennes valeurs
     * @param _idx l'indice concerne
     */
    public CommandSet(final long _newV, final long _oldi, final int _idx) {
      oldI_ = _oldi;
      newI_ = _newV;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      set(idx_, newI_, null);
    }

    @Override
    public void undo() {
      set(idx_, oldI_, null);
    }

  }

  protected class CommandSets implements CtuluCommand {

    int[] idxs_;

    long[] newVs_;

    long[] oldVs_;

    /**
     * @param _newV les nouvelles valeurs
     * @param _oldV les anciennes valeurs
     * @param _idx mes indices concernes
     */
    public CommandSets(final long[] _newV, final long[] _oldV, final int[] _idx) {
      oldVs_ = _oldV;
      newVs_ = _newV;
      idxs_ = _idx;
    }

    @Override
    public void redo() {
      set(idxs_, newVs_, null);
    }

    @Override
    public void undo() {
      set(idxs_, oldVs_, null);
    }
  }

  protected class CommandSetsOneValue implements CtuluCommand {

    int[] idxs_;

    long newV_;

    long[] oldVs_;

    protected CommandSetsOneValue(final long _newV, final long[] _oldV, final int[] _idx) {
      oldVs_ = _oldV;
      newV_ = _newV;
      idxs_ = _idx;
    }

    @Override
    public void redo() {
      set(idxs_, newV_, null);
    }

    @Override
    public void undo() {
      set(idxs_, oldVs_, null);
    }
  }

  public static long getLong(final Object _data) throws NumberFormatException {
    if (_data instanceof Number) {
      return ((Number) _data).longValue();
    } else if (_data == null) {
      throw new NumberFormatException("integer is null");
    } else {
      return Long.parseLong(_data.toString());
    }
  }

  /**
   * @param _init les valeurs initiales
   */
  protected CtuluCollectionLongAbstract() {}

  /**
   * Appele qui si des doubles sont modifies avec un set.
   */
  protected void fireDataChanged() {

  }

  /**
   * Methode appele lorsque toutes les valeurs ont ete modifiee. Les methodes qui lancent cet evt sont initFrom.
   */
  protected void fireDataStructureChanged() {

  }

  protected abstract boolean internalSet(int _i, long _newV);

  /**
   * @param _idxToTest l'indice a tester
   * @return true si contenu
   */
  public abstract boolean contains(long _idxToTest);

  @Override
  public final Object getObjectValueAt(final int _i) {
    return Long.valueOf(getValue(_i));
  }

  @Override
  public final Object[] getObjectValues() {
    final Long[] r = new Long[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = Long.valueOf(getValue(i));
    }
    return r;
  }

  @Override
  public abstract int getSize();

  /**
   * @param _i l'indice demande
   * @return la valeur en cet indice
   */
  @Override
  public abstract long getValue(int _i);

  public long[] getValues() {
    final long[] r = new long[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getValue(i);
    }
    return r;
  }

  /**
   * Reinitialise la liste interne avec le tableau. Envoie un evt structure modifiee.
   * 
   * @param _m le tableau utilise pour l'intialisation
   */
  public abstract void initWith(final long[] _m);

  public final boolean isAllSameValue() {
    final long r = getValue(0);
    for (int i = getSize() - 1; i > 0; i--) {
      if (r != getValue(i)) { return false; }
    }
    return true;
  }

  /**
   * @param _i le tableau a comparer
   * @return true si les indices de ce modele sont identique au tableau _i
   */
  @Override
  public boolean isSameValues(final int[] _i) {
    if (CtuluLibArray.isEmpty(_i)) { return false; }
    if (_i == null) { return isAllSameValue(); }
    final long first = getValue(_i[0]);
    for (int i = _i.length - 1; i > 0; i--) {
      if (getValue(_i[i]) != first) { return false; }

    }
    return true;
  }

  /**
   * @param _i
   * @param _newV
   * @return true si maj
   */
  public final boolean set(final int _i, final int _newV) {
    return set(_i, _newV, null);
  }

  /**
   * @param _i l'indice a modifier
   * @param _newV la nouvelle valeur
   * @param _c le receveur de la commande. peut-etre null
   * @return true si modif
   */
  public final boolean set(final int _i, final long _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i < 0 || _i > getSize() || getValue(_i) == _newV) { return false; }
    final long old = getValue(_i);
    internalSet(_i, _newV);
    fireDataChanged();
    if (_c != null) {
      _c.addCmd(new CommandSet(_newV, old, _i));
    }
    return true;
  }

  public final boolean set(final int _i, final Object _data, final CtuluCommandContainer _c) {
    int d = 0;
    if (_data != null) {
      if (_data instanceof Number) {
        d = ((Number) _data).intValue();
      } else {
        try {
          d = Integer.parseInt(_data.toString());
        } catch (final NumberFormatException e) {
          return false;
        }
      }
    }
    return set(_i, d, _c);
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   * 
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final long _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null) { return false; }
    boolean r = false;
    final long[] old = _c == null ? null : new long[_i.length];
    final int nbValues = getSize();
    for (int i = _i.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if (idx >= 0 && idx < nbValues) {
        if (old != null) {
          old[i] = getValue(idx);
        }
        r |= internalSet(idx, _newV);
      }
    }
    if (r) {
      fireDataChanged();
    }
    if (r && _c != null) {
      _c.addCmd(new CommandSetsOneValue(_newV, old, _i));
    }
    return r;
  }

  /**
   * @param _i le tableau des indices a modifier
   * @param _newV les nouvelles valeurs (meme taille que i)
   * @return true si maj
   */
  public final boolean set(final int[] _i, final long[] _newV) {
    return set(_i, _newV, null);
  }

  /**
   * Modifie les valeurs d'indice _i a partir des valeurs de tableau _newW.
   * 
   * @param _i les indices a modifier
   * @param _newV les nouvelles valeurs
   * @param _c le receveur de commmande (peut etre null)
   * @return true si modif
   */
  public final boolean set(final int[] _i, final long[] _newV, final CtuluCommandContainer _c) {
    // verif basique
    if (_i == null || _newV == null || _newV.length == 0 || _newV.length != _i.length) { return false; }
    boolean r = false;
    final long[] old = _c == null ? null : new long[_newV.length];
    final int nbValues = getSize();
    for (int i = _newV.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      if (idx >= 0 && idx < nbValues) {
        if (old != null) {
          old[i] = getValue(idx);
        }
        r |= internalSet(idx, _newV[i]);
      }
    }
    if (r) {
      fireDataChanged();
    }
    if (r && _c != null) {
      _c.addCmd(new CommandSets(_newV, old, _i));
    }
    return r;
  }

  /**
   * @param _v la nouvelle valeur pour tous les indices
   * @return true si maj
   */
  public final boolean setAll(final long _v) {
    return setAll(_v, null);
  }

  public final boolean setAll(final long _v, final CtuluCommandContainer _cmd) {
    final long[] olds = _cmd == null ? null : getValues();
    boolean r = false;
    for (int i = getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _v);
    }
    if (r) {
      fireDataChanged();
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setAll(_v);
          }

          @Override
          public void undo() {
            setAll(olds, null);
            fireDataChanged();
          }
        });

      }
    }
    return r;
  }

  public final boolean setAll(final long[] _v, final CtuluCommandContainer _cmd) {
    if (_v == null || _v.length != getSize()) { return false; }
    final long[] olds = _cmd == null ? null : getValues();
    boolean r = false;
    for (int i = getSize() - 1; i >= 0; i--) {
      r |= internalSet(i, _v[i]);
    }
    if (r) {
      fireDataChanged();
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setAll(_v, null);
          }

          @Override
          public void undo() {
            setAll(olds, null);
            fireDataChanged();
          }
        });

      }
    }
    return r;
  }

  @Override
  public final boolean setAll(final Object _data, final CtuluCommandContainer _c) {
    try {
      return setAll(getLong(_data), _c);
    } catch (final NumberFormatException _evt) {
      FuLog.error(_evt);
      return false;

    }
  }

  @Override
  public final boolean setAllObject(final int[] _i, final Object[] _data, final CtuluCommandContainer _c) {
    if (_data == null) { return false; }
    final long[] newV = new long[_data.length];
    try {
      for (int i = newV.length - 1; i >= 0; i--) {
        newV[i] = getLong(_data[i]);
      }
    } catch (final NumberFormatException _evt) {
      FuLog.error(_evt);
      return false;

    }
    return set(_i, newV, _c);
  }

  @Override
  public final boolean setObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    try {
      return set(_i, getLong(_data), _c);
    } catch (final NumberFormatException e) {
      return false;
    }

  }

  @Override
  public final boolean setObject(final int[] _i, final Object _data, final CtuluCommandContainer _c) {
    try {
      return set(_i, getLong(_data), _c);
    } catch (final NumberFormatException e) {
      return false;
    }

  }

}
