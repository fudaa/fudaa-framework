/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-01-10 08:58:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Une classe permettant de gerer un vecteur de boolean.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluListBoolean.java,v 1.1 2007-01-10 08:58:48 deniger Exp $
 */
public class CtuluListBoolean extends CtuluCollectionBooleanAbstract {

  protected class CommandAdd implements CtuluCommand {

    int idx_;

    Boolean newValue_;

    public CommandAdd(final int _idx, final Boolean _newV) {
      idx_ = _idx;
      newValue_ = _newV;
    }

    @Override
    public void redo() {

      if (idx_ == list_.size()) {
        internalAdd(newValue_);
      } else {
        internalAdd(idx_, newValue_);
      }
      fireObjectAdded(idx_, newValue_);
    }

    @Override
    public void undo() {
      internalRemove(idx_);
      fireObjectRemoved(idx_, newValue_);
    }
  }

  protected class CommandRemove implements CtuluCommand {

    int idx_;

    Boolean oldValue_;

    public CommandRemove(final int _idx, final Boolean _newV) {
      idx_ = _idx;
      oldValue_ = _newV;
    }

    @Override
    public void redo() {
      internalRemove(idx_);
      fireObjectRemoved(idx_, oldValue_);
    }

    @Override
    public void undo() {
      internalAdd(idx_, oldValue_);
      fireObjectAdded(idx_, oldValue_);
    }

  }

  protected class CommandRemoveAll implements CtuluCommand {

    CtuluListBoolean mementoRemoveAll_;

    public CommandRemoveAll(final CtuluListBoolean _m) {
      mementoRemoveAll_ = _m;
    }

    @Override
    public void redo() {
      internalRemoveAll();
      fireObjectRemoved(-1, null);
    }

    @Override
    public void undo() {
      initWith(mementoRemoveAll_, false);
      // cas particulier ou il faut envoye un evt
      fireObjectAdded(-1, null);
    }
  }

  protected class CommandRemoveDangerous implements CtuluCommand {

    int[] idx_;

    Boolean[] oldValues_;

    public CommandRemoveDangerous(final Boolean[] _oldValues, final int[] _idx) {
      oldValues_ = _oldValues;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      insertDangerous(idx_, oldValues_);
    }
  }

  protected class CommandRemoveMemento implements CtuluCommand {

    int[] idx_;

    CtuluListBoolean memento_;

    public CommandRemoveMemento(final CtuluListBoolean _m, final int[] _idx) {
      memento_ = _m;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      remove(idx_);
    }

    @Override
    public void undo() {
      initWith(memento_, false);
      // cas particulier ou il faut envoye un evt
      fireObjectAdded(-1, null);
    }
  }

  /**
   * @uml.property name="list"
   * @uml.associationEnd elementType="java.lang.Boolean" multiplicity="(0 -1)"
   */
  protected List list_;

  /**
   * Initialise avec une liste de taille de 20 par defaut.
   */
  public CtuluListBoolean() {
    this(20);
  }

  protected void sort() {
    Collections.sort(list_);
  }

  /**
   * Fait appel a la methode List.toArray.
   * 
   * @param _o le tableau a completer
   * @return le tableau complete ou le tableau redimensionne et complete
   * @see List#toArray(java.lang.Object[])
   */
  public Object[] toArray(final Object[] _o) {
    return list_.toArray(_o);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListBoolean(final Collection _init) {
    list_ = new ArrayList(_init);
  }

  /**
   * @param _init les valeurs initiales
   */
  public CtuluListBoolean(final CtuluListBoolean _init) {
    this(_init.list_);
  }

  /**
   * @param _nb taille initiales
   */
  public CtuluListBoolean(final int _nb) {
    list_ = new ArrayList(_nb);
  }

  public final boolean setAll(final Object _v) {
    if (_v instanceof Boolean) {
      Collections.fill(list_, _v);
      fireObjectChanged(-1, null);
      return true;
    }
    return false;
  }

  protected void fireObjectAdded(int _newIdx, Object _newGeom) {
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue) {
  }

  protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
  }

  protected void fireObjectModified(int _idx, Object _geom) {
  }

  protected void initWith(final CtuluListBoolean _m) {
    initWith(_m, true);
  }

  protected void internalAdd(final Boolean _v) {
    list_.add(_v);
  }

  protected void internalAdd(final int _i, final Boolean _v) {
    if (_i == list_.size()) {
      list_.add(_v);
    } else {
      list_.add(_i, _v);
    }
  }

  protected Boolean internalRemove(final int _i) {
    return (Boolean) list_.remove(_i);
  }

  @Override
  public boolean[] getValues() {
    final boolean[] r = new boolean[getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getValue(i);
    }
    return r;
  }

  @Override
  protected boolean internalSet(final int _i, final boolean _newV) {
    return internalSet(_i, Boolean.valueOf(_newV));
  }

  protected boolean internalSet(final int _i, final Boolean _newV) {
    if (_newV == list_.get(_i)) {
      return false;
    }
    list_.set(_i, _newV);
    return true;
  }

  final void initWith(final CtuluListBoolean _m, final boolean _throwEvent) {
    list_ = new ArrayList(_m.list_);
    if (_throwEvent) {
      fireObjectAdded(-1, null);
    }
  }

  /**
   * Fonction a utiliser avec pr�caution: effectue des insert des values en suivant les valeurs dans l'ordre. Pour
   * fonctionner les points
   * 
   * @param _idx les indices dans l'ordre
   * @param _values les valeurs dans l'ordre
   * @return true
   */
  final boolean insertDangerous(final int[] _idx, final Boolean[] _values) {
    if ((((((_idx == null) || (_values == null))) || (_idx.length == 0))) || (_values.length != _idx.length)) {
      return false;
    }
    final int n = _idx.length;
    for (int i = 0; i < n; i++) {
      insert(_idx[i], _values[i]);
    }
    fireObjectAdded(-1, null);
    return true;
  }

  public boolean canAdd(final Object _o) {
    return true;
  }

  public final boolean add(final Boolean _boolean, final CtuluCommandContainer _cmd) {
    if (canAdd(_boolean)) {
      internalAdd(_boolean);
      if (_cmd != null) {
        _cmd.addCmd(new CommandAdd(list_.size() - 1, _boolean));
      }
      fireObjectAdded(list_.size() - 1, _boolean);
      return true;
    }
    return false;
  }

  protected class CommandAddAll implements CtuluCommand {

    int idxBefore_;

    Boolean[] newValue_;

    protected CommandAddAll(final int _idx, final Boolean[] _newV) {
      idxBefore_ = _idx;
      newValue_ = _newV;
    }

    @Override
    public void redo() {
      list_.add(newValue_);
      fireObjectAdded(idxBefore_, newValue_);
    }

    @Override
    public void undo() {
      CtuluLibArray.remove(list_, idxBefore_, newValue_.length);
    }

  }

  public void addAll(final Boolean[] _d, final CtuluCommandContainer _cmd) {
    final int size = list_.size();
    list_.addAll(Arrays.asList(_d));
    if (_cmd != null) {
      _cmd.addCmd(new CommandAddAll(size, CtuluLibArray.copy(_d)));
    }
  }

  @Override
  public boolean addAllObject(final Object _dataArray, final CtuluCommandContainer _c) {
    if (_dataArray instanceof Boolean[]) {
      addAll((Boolean[]) _dataArray, _c);
      return true;
    } else if (_dataArray instanceof boolean[]) {
      final boolean[] bInit = (boolean[]) _dataArray;
      final Boolean[] b = new Boolean[bInit.length];
      for (int i = b.length - 1; i >= 0; i--) {
        b[i] = Boolean.valueOf(bInit[i]);
      }
      addAll(b, _c);
      return true;
    } else if (_dataArray instanceof Object[]) {
      final Object[] d = (Object[]) _dataArray;
      final Boolean[] v = new Boolean[d.length];
      for (int i = v.length - 1; i >= 0; i--) {
        v[i] = (Boolean) d[i];
      }
      addAll(v, _c);
      return true;
    } else {
      (new Throwable()).printStackTrace();
    }
    return false;
  }


  protected final boolean add(final Boolean _x) {
    return add(_x, null);
  }

  protected CtuluListBoolean createMemento() {
    return new CtuluListBoolean(this);
  }

  @Override
  public Object[] getObjectValues() {
    final Boolean[] r = new Boolean[list_.size()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = (Boolean) list_.get(i);
    }
    return r;
  }

  @Override
  public int getSize() {
    return list_.size();
  }

  @Override
  public boolean getValue(final int _i) {
    if (list_.get(_i) == null) {
      return false;
    }
    return ((Boolean) list_.get(_i)).booleanValue();
  }

  public final boolean insert(final int _i, final Boolean _x) {
    return insert(_i, _x, null);
  }

  public boolean insert(final int _i, final Boolean _x, final CtuluCommandContainer _c) {
    return insert(_i, _x, null, _c);

  }

  /**
   * @param _i l'indice [0,size()] si _i==size(), il sera ajoute a la fin
   * @param _x la valeur a ajouter
   * @param _c le receveur da la commande. Si null pas de commande creee.
   * @return true si inserer
   */
  public boolean insert(final int _i, final Boolean _x, final List _o, final CtuluCommandContainer _c) {
    if (canAdd(_x)) {
      internalAdd(_i, _x);
      if (_c != null) {
        _c.addCmd(new CommandAdd(_i, _x));
      }

      fireObjectAdded(_i, _x);
      return true;
    }
    return false;
  }

  public boolean remove(final int _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int _i, final CtuluCommandContainer _c) {
    final Boolean old = internalRemove(_i);
    if (_c != null) {
      _c.addCmd(new CommandRemove(_i, old));
    }
    fireObjectRemoved(_i, old);
    return true;
  }

  /**
   * @param _i
   * @return true si suppression
   * @see #remove(int[], CtuluCommandContainer)
   */
  public boolean remove(final int[] _i) {
    return remove(_i, null);
  }

  @Override
  public boolean remove(final int[] _i, final CtuluCommandContainer _c) {
    return remove(_i, _c, false);
  }

  /**
   * Enleve les indices appartenant au tableau _i. De plus il est suppose que le tableau est correct : les indices
   * appartiennent � [0,size()[ et tous differents.
   * 
   * @param _idx les indices
   * @param _cmd le receveur de commandes
   * @param _forceMemento true si memento global
   * @return true si modif
   */
  public boolean remove(final int[] _idx, final CtuluCommandContainer _cmd, final boolean _forceMemento) {
    if (_idx == null || _idx.length == 0) {
      return false;
    }
    final int[] newIdx = CtuluLibArray.copy(_idx);
    Arrays.sort(newIdx);
    final int n = newIdx.length;
    if (n == 1) {
      return remove(newIdx[0], _cmd);
    }

    final int initSize = list_.size();
    final Boolean[] newArray = new Boolean[initSize];
    int idx = 0;
    final Boolean[] oldV = new Boolean[newIdx.length];
    int idxOldV = 0;
    // on copie le debut du tableau
    for (int i = 0; i < newIdx[0]; i++) {
      newArray[i] = (Boolean) list_.get(i);
    }
    idx = newIdx[0];
    final int maxI = newIdx[newIdx.length - 1];
    for (int i = idx; i <= maxI; i++) {
      if (Arrays.binarySearch(newIdx, i) < 0) {
        newArray[idx++] = (Boolean) list_.get(i);
      } else {
        final Boolean d = (Boolean) list_.get(i);
        oldV[idxOldV++] = d;
      }
    }
    for (int i = maxI + 1; i < initSize; i++) {
      newArray[idx++] = (Boolean) list_.get(i);
    }
    CtuluListBoolean memento = null;
    if (_forceMemento || idxOldV > 10) {
      memento = createMemento();
    }

    list_.clear();
    for (int i = 0; i < idx; i++) {
      list_.add(newArray[i]);
    }
    if (idx + idxOldV == initSize) {
      if (_cmd != null) {
        if (_forceMemento || idxOldV > 10) {
          _cmd.addCmd(new CommandRemoveMemento(memento, newIdx));
        } else {
          _cmd.addCmd(new CommandRemoveDangerous(oldV, newIdx));
        }
      }
      fireObjectRemoved(-1, null);
      return true;
    }
    return false;

  }

  public final void removeAll() {
    removeAll(null);
  }

  protected void internalRemoveAll() {
    list_.clear();
  }

  @Override
  public void removeAll(final CtuluCommandContainer _c) {
    final CtuluListBoolean createMemento = createMemento();
    internalRemoveAll();
    if (_c != null) {
      _c.addCmd(new CommandRemoveAll(createMemento));
    }
    fireObjectRemoved(-1, null);
  }

  /**
   * @param _i
   * @param _newV
   * @see #set(int, Boolean, CtuluCommandContainer)
   * @return true si set
   */
  public final boolean set(final int _i, final Boolean _newV) {
    return set(_i, _newV.booleanValue(), null);
  }

  /**
   * @param _i
   * @param _newV
   * @return true si modif
   */
  public boolean set(final int[] _i, final Boolean[] _newV) {
    if (_newV == null) {
      return false;
    }
    final boolean[] v = new boolean[_newV.length];
    for (int i = v.length - 1; i >= 0; i--) {
      v[i] = _newV[i].booleanValue();
    }
    return set(_i, v, null);
  }

  @Override
  public boolean addObject(final Object _data, final CtuluCommandContainer _c) {
    if (_data instanceof Boolean) {
      return add((Boolean) _data, _c);
    } else if (_data != null) {
      return add(Boolean.valueOf(_data.toString()), _c);
    }
    return false;
  }

  @Override
  public boolean insertObject(final int _i, final Object _data, final CtuluCommandContainer _c) {
    if (_data instanceof Boolean) {
      return insert(_i, (Boolean) _data, _c);
    } else if (_data != null) {
      return insert(_i,Boolean.valueOf(_data.toString()), _c);
    }
    return false;
  }

  @Override
  public void initWith(final CtuluCollection _model, final boolean _throwEvent) {
    if (_model instanceof CtuluCollectionBooleanInterface) {
      this.list_.clear();
      this.list_.addAll(Arrays.asList(_model.getObjectValues()));
    }
  }

}
