/*
 *  @creation     2002-11-21
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;
import com.memoire.bu.BuTask;
/**
 * une interface pour mettre a jour la progression.
 *
 * @version   $Id: ProgressionBuAdapter.java,v 1.5 2006-09-19 14:36:53 deniger Exp $
 * @author    Fred Deniger
 */
public class ProgressionBuAdapter implements ProgressionInterface {
  private BuTask t_;
  /**
   * appelle le constructeur avec null.
   */
  public ProgressionBuAdapter() {
    this(null);
  }
  /**
   * @param _t la tache qui doit etre mis a jour
   */
  public ProgressionBuAdapter(final BuTask _t) {
    t_= _t;
  }
  /**
   * @param _t la nouvelle tache
   */
  public void setTask(final BuTask _t) {
    t_= _t;
  }

  @Override
  public void setProgression(final int _v) {
    if (t_ != null) {
      t_.setProgression(_v);
    }
  }
  @Override
  public void setDesc(final String _s) {
    if (t_ != null){
      t_.setName(_s);
    }
  }
  /**
   * Remet a zero la BuTask.
   */
  @Override
  public void reset() {
    if (t_ != null) {
      t_.setName(null);
      t_.setProgression(0);
    }
  }

}
