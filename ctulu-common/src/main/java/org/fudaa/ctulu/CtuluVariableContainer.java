package org.fudaa.ctulu;

public interface CtuluVariableContainer {
  
  CtuluVariable getVariable(String id);

}
