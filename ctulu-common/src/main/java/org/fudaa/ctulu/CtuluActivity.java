/*
 *  @creation     13 mai 2005
 *  @modification $Date: 2008-01-11 16:40:14 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

/**
 * Interface � implementer pour une activit� lanc�e dans un Thread qui peut �tre
 * interrompue. L'appel � la methode stop dans un Thread concurant est cens� lever un drapeau
 * test� dans l'activit� en cours.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluActivity.java,v 1.4 2008-01-11 16:40:14 bmarchan Exp $
 */
public interface CtuluActivity {

  /**
   * Notifie l'activit� qu'elle doit s'interrompre.
   */
  void stop();
}
