package org.fudaa.ctulu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Manager qui indique toutes les erreurs,messages qui se sont produites pendant le chargement. 
 * 
 * @author Adrien Hadoux
 */
public class CtuluAnalyzeGroup {

  private List<CtuluAnalyze> analyser = new ArrayList<CtuluAnalyze>();
  private CtuluAnalyze mainAnalyze = new CtuluAnalyze();

  /**
   * 
   */
  List<CtuluAnalyzeGroup> groups;

  List<CtuluAnalyzeGroup> groupsExt;

  ResourceBundle defaultResource;

  public CtuluAnalyzeGroup(ResourceBundle defaultResource) {
    super();
    analyser.add(mainAnalyze);
    this.defaultResource = defaultResource;
  }

  public void addAnalyzer(CtuluAnalyze analyze) {
    analyser.add(analyze);
  }

  // /**
  // * Ajoute un message d'erreur au tout dernier analyzer dispo.
  // *
  // * @param message
  // */
  // public void addMessageError(final String message) {
  // analyser.get(analyser.size() - 1).addError(message);
  // }

  public void clear() {
    analyser.clear();
  }

  public CtuluAnalyze getNewAnalyser(String desc) {
    CtuluAnalyze analyzer = createAnalyzer();
    analyzer.setDesc(desc);
    analyser.add(analyzer);
    return analyzer;
  }

  /**
   * @return true si un analyseur contient des erreur
   */
  public boolean containsError() {
    if (isContentEmpty()) { return false; }
    for (CtuluAnalyze analyze : analyser) {
      if (analyze.containsFatalError() || analyze.containsErrors()) { return true; }
    }
    if (groups != null) {
      for (CtuluAnalyzeGroup mng : groups) {
        if (mng.containsError()) { return true; }
      }
    }
    return false;
  }

  public boolean containsFatalError() {
    if (isContentEmpty()) { return false; }
    for (CtuluAnalyze analyze : analyser) {
      if (analyze.containsFatalError()) { return true; }
    }
    if (groups != null) {
      for (CtuluAnalyzeGroup mng : groups) {
        if (mng.containsFatalError()) { return true; }
      }
    }
    return false;
  }

  /**
   * @return true si les analyseur ne sont pas vide
   */
  public boolean containsSomething() {
    if (isContentEmpty()) { return false; }
    for (CtuluAnalyze analyze : analyser) {
      if (!analyze.isEmpty()) { return true; }
    }
    if (groups != null) {
      for (CtuluAnalyzeGroup mng : groups) {
        if (mng.containsSomething()) { return true; }
      }
    }
    return false;
  }

  public boolean containsWarning() {
    if (isContentEmpty()) { return false; }
    for (CtuluAnalyze analyze : analyser) {
      if (analyze.containsWarnings()) { return true; }
    }
    if (groups != null) {
      for (CtuluAnalyzeGroup mng : groups) {
        if (mng.containsWarning()) { return true; }
      }
    }
    return false;
  }

  private CtuluAnalyze createAnalyzer() {
    CtuluAnalyze analyzer = new CtuluAnalyze();
    analyzer.setDefaultResourceBundle(defaultResource);
    return analyzer;
  }

  /**
   * @param name la nom du sous-groupe
   * @return le sous-groupe ajoute.
   */
  public CtuluAnalyzeGroup createGroup(String name) {
    CtuluAnalyzeGroup res = new CtuluAnalyzeGroup(defaultResource);
    res.getMainAnalyze().setDesc(name);
    if (groups == null) {
      groups = new ArrayList<CtuluAnalyzeGroup>();
    }
    groups.add(res);
    return res;
  }

  /**
   * @return la liste des anayliser utilis�
   */
  public List<CtuluAnalyze> getAnalyser() {
    return analyser;
  }

  /**
   * @return the groups
   */
  public List<CtuluAnalyzeGroup> getGroups() {
    if (groups == null) { return Collections.emptyList(); }
    if (groupsExt == null) {
      groupsExt = Collections.unmodifiableList(groups);
    }
    return groupsExt;
  }

  /**
   * retourne le dernier ctuluanalyze cree.
   * 
   * @param message
   * @return
   */
  public CtuluAnalyze getLastAnalyser() {
    return analyser.get(analyser.size() - 1);
  }

  /**
   * @return the mainAnalyze
   */
  public CtuluAnalyze getMainAnalyze() {
    return mainAnalyze;
  }

  public String getMainDesc() {
    return mainAnalyze.getDesc();
  }

  /**
   * Ajoute un nouvel ctuluanalyze a la liste et le fournit.
   * 
   * @return
   */
  public CtuluAnalyze getNewAnalyser() {
    CtuluAnalyze analyzer = createAnalyzer();
    // ajout dans la liste des analyze
    analyser.add(analyzer);
    return analyzer;
  }

  private boolean isContentEmpty() {
    return CtuluLibArray.isEmpty(analyser) && CtuluLibArray.isEmpty(groups);
  }

  public void setListeMessageError(final List<CtuluAnalyze> listeMessageError) {
    this.analyser = listeMessageError;
  }

  /**
   * @param mainAnalyze the mainAnalyze to set
   */
  public void setMainAnalyze(CtuluAnalyze mainAnalyze) {
    this.mainAnalyze = mainAnalyze;
  }

}
