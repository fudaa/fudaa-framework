/**
 * @creation 12 mars 2003
 * @modification $Date: 2006-09-19 14:36:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

/**
 * @deprecated utiser CtuluIOResult. Dans cette classe il y a une confusion entre les erreurs de niveau SEVERE et 
 * FATAL.
 * @author deniger
 * @version $Id: CtuluIOOperationSynthese.java,v 1.7 2006-09-19 14:36:54 deniger Exp $
 */
public class CtuluIOOperationSynthese {

  private CtuluAnalyze analyze_;

  private String exClose_;

  private Object source_;

  public CtuluIOOperationSynthese() {}

  /**
   * @return true si exception lors de la fermeture des flux.
   */
  public boolean containsClosingError() {
    return exClose_ != null;
  }

  /**
   * @return true si une erreur fatale est survenue
   */
  public boolean containsSevereError() {
    return (analyze_ != null) && (analyze_.containsFatalError());
  }
  /**
   * @return true si une erreur fatale est survenue
   */
  public boolean containsError() {
    return (analyze_ != null) && (analyze_.containsErrors());
  }

  /**
   * @return true si message
   */
  public boolean containsMessages() {
    return (analyze_ != null) && (!analyze_.isEmpty());
  }

  /**
   * @return l'analyse de l'operation
   */
  public CtuluAnalyze getAnalyze() {
    return analyze_;
  }

  /**
   * @return le message d'erreur issu de la fermeture du(des) flux.
   */
  public String getClosingException() {
    return exClose_;
  }

  /**
   * @return la source utilise pour l'ecriture/recuperee par la lecture
   */
  public Object getSource() {
    return source_;
  }

  /**
   * Ecrit sur la sortie standard les messages.
   */
  public void printAnalyze() {
    if (analyze_ != null) {
      analyze_.printResume();
    }
  }

  /**
   * @param _editor l'analyse de l'operation
   */
  public void setAnalyze(final CtuluAnalyze _editor) {
    analyze_ = _editor;
  }

  /**
   * @param _exception le message d'exception obtenu lors de la fermeture du/des flux.
   */
  public void setClosingOperation(final String _exception) {
    exClose_ = _exception;
  }

  /**
   * @param _object La nouvelle source (resultat de la lecture ou source pour l'ecriture)
   */
  public void setSource(final Object _object) {
    source_ = _object;
  }
}