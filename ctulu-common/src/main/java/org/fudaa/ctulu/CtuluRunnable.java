/*
 * @creation 24 mai 07
 * @modification $Date: 2007-06-11 13:02:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuLib;

/**
 * Une classe permettant de lancer une exec dans un thread avec un runnable précédent et un suivant pouvant etre lancer
 * dans swing.
 * 
 * @author fred deniger
 * @version $Id: CtuluRunnable.java,v 1.2 2007-06-11 13:02:37 deniger Exp $
 */
public abstract class CtuluRunnable implements Runnable {

  Runnable after_;
  boolean afterSwingThread_;

  Runnable before_;
  boolean beforeSwingThread_;
  final String name_;
  final CtuluUI ui_;

  public CtuluRunnable(final String _name, final CtuluUI _ui) {
    super();
    name_ = _name;
    ui_ = _ui;
  }

  @Override
  public void run() {
    if (before_ != null && beforeSwingThread_) {
      BuLib.invokeNow(before_);
    }
    CtuluTaskDelegate task = ui_.createTask(name_);
    final ProgressionInterface prog = task.getStateReceiver();
    task.start(new Runnable() {
      @Override
      public void run() {
        if (before_ != null && !beforeSwingThread_) {
          before_.run();
        }
        boolean res = CtuluRunnable.this.run(prog);
        if (res && after_ != null) {
          if (afterSwingThread_) {
            BuLib.invokeLater(after_);
          } else
            after_.run();
        }
      }

    });

  }

  /**
   * @param _proj la barre de progression
   * @return true si on peut lancer le thread d'apres
   */
  public abstract boolean run(ProgressionInterface _proj);

  public void setAfterRunnable(Runnable _r, boolean _swing) {
    afterSwingThread_ = _swing;
    after_ = _r;
  }

  public void setBeforeRunnable(Runnable _r, boolean _swing) {
    beforeSwingThread_ = _swing;
    before_ = _r;
  }
}
