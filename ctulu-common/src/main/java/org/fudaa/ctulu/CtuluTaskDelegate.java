/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2006-02-09 18:50:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;



/**
 * @author Fred Deniger
 * @version $Id: CtuluTaskDelegate.java,v 1.3 2006-02-09 18:50:26 deniger Exp $
 */
public interface CtuluTaskDelegate {

  ProgressionInterface getStateReceiver();
  ProgressionInterface getMainStateReceiver();
  void start(Runnable _run);

}
