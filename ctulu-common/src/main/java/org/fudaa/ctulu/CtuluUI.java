/*
 *  @creation     13 juin 2003
 *  @modification $Date: 2006-10-27 10:22:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.awt.Component;

/**
 * Interface utilisateur minimale.
 *
 * @author fred deniger
 * @version $Id: CtuluUI.java,v 1.8 2006-10-27 10:22:24 deniger Exp $
 */
public interface CtuluUI {

  /**
   * @param _titre le titre du message d'erreur: peut etre null.
   * @param _msg le messsage
   * @param _tempo true si message temporaire: a effacer apres un temps donn�
   */
  void error(String _titre, String _msg, boolean _tempo);

  void error(String _msg);

  /**
   * @param _titre le titre du message: peut etre null.
   * @param _msg le messsage
   * @param _tempo true si message temporaire: a effacer apres un temps donn�
   */
  void message(String _titre, String _msg, boolean _tempo);

  void warn(String _titre, String _msg, boolean _tempo);

  /**
   * Gere l'analyse d'une commande. En general, affiche un message si erreur fatale.
   *
   * @param _analyze l'analyse a considerer
   * @return true si contient une erreur fatale
   */
  boolean manageAnalyzeAndIsFatal(CtuluAnalyze _analyze);

  boolean manageAnalyzeAndIsFatal(CtuluLog _analyze);

  /**
   * Gere les erreurs survenues pour l'operation et renvoie true si erreur fatale.
   *
   * @return true si erreure fatale
   * @param _opResult l'operation a considerer
   */
  boolean manageErrorOperationAndIsFatal(CtuluIOOperationSynthese _opResult);

  boolean manageErrorOperationAndIsFatal(CtuluIOResult _opResult);

  CtuluTaskDelegate createTask(String _name);

  /**
   * @return le composant pere: peut etre null dans le cas texte
   */
  Component getParentComponent();

  ProgressionInterface getMainProgression();

  void clearMainProgression();

  /**
   * @param _titre le titre du message de confirmation
   * @param _text le texte
   * @return true si l'utilisateur a accepte
   */
  boolean question(String _titre, String _text);
}
