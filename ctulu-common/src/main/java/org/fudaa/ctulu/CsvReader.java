/**
 * @creation 2000-10-05
 * @modification $Date: 2007-05-04 13:43:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLib;
import java.io.EOFException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
/**
 * Une classe permettant de facilement lire les fichiers Csv. Les fichiers CSV (Comma Separated
 * Value) sont des fichiers texte dont les champs sont separes par des virgules.
 * @version $Id: CsvReader.java,v 1.9 2007-05-04 13:43:20 deniger Exp $
 * @author Guillaume Desnoix
 */
public class CsvReader extends LineNumberReader {

  protected static String clearSpaces(final String _s){
    return _s.trim();
  }

  protected String[] fields_;
  protected String   line_;
  protected int      number_;
  protected String   separator_;

  /**
   * @param _reader le reader support
   */
  public CsvReader(final Reader _reader) {
    super(_reader);
    separator_ = CtuluLibString.VIR;
    number_ = -1;
  }

  boolean tranformVirgule_;

  /**
   * Renvoie le champ numero _i sous la forme d'un reel.
   * @param _i le numero du champ
   * @return la valeur interpretee comme un double
   */
  public double doubleField(final int _i){
    double r = 0.;
    if ((fields_ != null) && (_i < fields_.length)) {
      String s = clearSpaces(fields_[_i]);
      if (s.length() != 0) {
        if (tranformVirgule_) {
          s = FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT);
        }
        try {
          r = Double.parseDouble(s);
        }
        catch (final NumberFormatException e) {
          if (s.indexOf(',') >= 0) {
            s = FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT);
            tranformVirgule_ = true;
            r = Double.parseDouble(s);
          }
        }
      }
    }
    return r;
  }

  /**
   * @return la ligne lue
   */
  public String getLine(){
    return line_;
  }

  /**
   * Renvoie le champ numero _i sous la forme d'un entier.
   * @param _i le numero du champ
   * @return la valeur interprete comme un int
   */
  public int intField(final int _i){
    int r = 0;
    if ((fields_ != null) && (_i < fields_.length)) {
      final   String s = clearSpaces(fields_[_i]);
      if (s.length() != 0) {
        r = Integer.parseInt(s);
      }
    }
    return r;
  }

  /**
   * Lit une ligne et l'analyse selon le format fixe fourni.
   * @param _nbf le format sous forme d'un tableau de largeurs
   * @throws IOException
   */
  public void readFields(final int _nbf) throws IOException{
    String s = readLine();
    line_ = s;
    if (s == null) {
      throw new EOFException();
    }
    fields_ = null;
    if (_nbf >= 0) {
      int i;
      int j;
      int l;
      l = _nbf;
      fields_ = new String[l];
      j = 0;
      for (i = 0; i < l; i++) {
        j = s.indexOf(separator_, 0);
        if (j < 0) {
          fields_[i] = s;
          s = CtuluLibString.EMPTY_STRING;
        }
        else {
          fields_[i] = s.substring(0, j);
          s = s.substring(j + 1);
        }
      }
    }
  }

  /**
   * Renvoie le champ numero _i sous la forme d'une chaine. Les espaces de debut et de fin ont ete
   * retires.
   * @param _i le numero du champ
   * @return la valeur interprete comme un String
   */
  public String stringField(final int _i){
    String r = CtuluLibString.EMPTY_STRING;
    if ((fields_ != null) && (_i < fields_.length)) {
      r = clearSpaces(fields_[_i]);
    }
    return r;
  }

}