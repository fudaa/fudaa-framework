package org.fudaa.ctulu;

public class Pair<S,T> {
  public S firstValue;
  public T secondValue;

  public Pair() {
  }

  public Pair(S firstValue, T secondValue) {
    this.firstValue = firstValue;
    this.secondValue = secondValue;
  }
}
