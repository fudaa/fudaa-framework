/**
 * @creation 27 sept. 2004
 * @modification $Date: 2006-09-19 14:36:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuTextField;
import javax.measure.unit.Unit;

/**
 * Une interface definissant un numberformat pour formater correctement les valeurs et un validateur de valeur.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluNumberFormater.java,v 1.4 2006-09-19 14:36:54 deniger Exp $
 */
public class CtuluNumberFormater {

  final protected CtuluNumberFormat fmt_;
  CtuluValueValidator val_;
  Unit unit_;

  public final String getUnit() {
    return unit_ == null ? CtuluLibString.EMPTY_STRING : unit_.toString();
  }

  public final void setUnit(final Unit _unit) {
    unit_ = _unit;
  }

  /**
   * @param _fmt le formatter pour les nombres NON NULL;
   * @param _v le validateur : peut etre null.
   */
  public CtuluNumberFormater(final CtuluNumberFormat _fmt, final CtuluValueValidator _v) {
    fmt_ = _fmt;
    val_ = _v;
  }

  /**
   * @param _ft le textfield a intialiser avec ce formatteur
   * @param _initVal la valeur initiale
   */
  public void initTextField(final BuTextField _ft, final double _initVal) {
    initTextField(_ft);
    _ft.setText(format(_initVal));
  }

  /**
   * @param _ft le textfield a intialiser avec ce formatteur
   */
  public void initTextField(final BuTextField _ft) {
    _ft.setValueValidator(getValueValidator());
    _ft.setDisplayFormat(getNumberFormat());
    _ft.setToolTipText(getValueValidator().getDescription());
  }

  /**
   * @param _d le double a formatter
   * @return le chaine formatter en accord avec le neg format
   */
  public String format(final double _d) {
    return fmt_ == null ? Double.toString(_d) : fmt_.format(_d);
  }

  public void format(final double _d, final StringBuffer _dest) {
    if (fmt_ == null) {
      _dest.append(_d);
    } else {
      fmt_.format(_d, _dest);
    }
  }

  /**
   * A utiliser apres avoir valider la valeur.
   * 
   * @return permet de formatter la valeur
   */
  public CtuluNumberFormat getNumberFormat() {
    return fmt_;
  }

  /**
   * Permet de valider une valeur. A utiliser avant le NumberFormat
   * 
   * @return le "validateur"
   */
  public CtuluValueValidator getValueValidator() {
    return val_;
  }

  /**
   * @see CtuluValueValidator#isValueValid(double)
   * @param _d le double a tester
   * @return true si le validateur est nul.
   */
  public boolean isValid(final double _d) {
    return val_ == null ? true : val_.isValueValid(_d);
  }

}