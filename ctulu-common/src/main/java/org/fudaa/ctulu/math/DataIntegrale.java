package org.fudaa.ctulu.math;

/**
 * Classe qui gere les bilans ainsi que les cubatures.
 *
 * @author Adrien Hadoux
 */
public class DataIntegrale {

  /**
   * Valeur pour la zone plus
   */
  private double zonePlus_ = 0;
  /**
   * Valeur intégrale pour la zone moins
   */
  private double zoneMoins_ = 0;

  public double getZonePlus() {
    return zonePlus_;
  }

  public double getZoneMoins() {
    return zoneMoins_;
  }

  public void addZonePlus(double _d) {
    zonePlus_ = zonePlus_ + Math.abs(_d);

  }

  public void addZoneMoins(double _d) {
    zoneMoins_ = zoneMoins_ + Math.abs(_d);
  }

  public void add(double _d) {
    if (Math.signum(_d) > 0) {
      addZonePlus(_d);
    } else {
      addZoneMoins(_d);
    }
  }

  public double getResultat() {
    return zonePlus_ - zoneMoins_;
  }
}
