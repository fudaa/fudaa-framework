/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ctulu.math;

/**
 * @author deniger
 */
public class BilanHelper {

  /**
   * Integration des donnees par la methode des trapezes entre la courbe representee par les x1 et y1 et la droite d�finie par la fonction affine de
   * coeff a et d'ordonn��e a l origine b y1=a*x1+b
   *
   * @param lx
   * @param ly
   * @return
   */
  public static DataIntegrale integrerMethodeTrapeze(double[] lx, double[] ly, double a, double b) {

    DataIntegrale data = new DataIntegrale();

    for (int i = 0; i < Math.min(lx.length, ly.length) - 1; i++) {
      double x = lx[i];
      double x2 = lx[i + 1];
      double y = ly[i];
      double y2 = ly[i + 1];

      double ySeuil1 = a * x + b;
      double ySeuil2 = a * x2 + b;
      if (y > ySeuil1 && y2 > ySeuil2) {
        // courbe au dessus de la ligne
        data.addZonePlus(calculAireTrapeze(x, y, x2, y2, ySeuil1, ySeuil2));

      } else if (y < ySeuil1 && y2 < ySeuil2) {
        // courbe en dessous de la ligne
        data.addZoneMoins(calculAireTrapeze(x, y, x2, y2, ySeuil1, ySeuil2));

      } else {
        // cas particulier : intersection, on calcule l'intersection entre les
        // points
        // cas particulier : intersection, on calcule l'intersection entre les
        // points
        double yIntersect = 0;
        double xIntersect = 0;
        // on prend la droite form�e par les 2 points et on calcule son
        // intersection avec la droite affine
        if (x2 - x != 0) {
          // droite non parrallele a l axe des abscisses
          double coefDirecteur = (y2 - y) / (x2 - x);
          double ordoOrigine = y2 - coefDirecteur * x2;

          // intersection de droites avec le seuil

          if (coefDirecteur - a != 0) {
            // droie non paralleles
            xIntersect = (ordoOrigine - b) / (coefDirecteur - a);
            yIntersect = coefDirecteur * xIntersect + ordoOrigine;
          } else {
            // droite parrallele confondue au seuil, on ne fais rien...
          }

        } else {
          // x2=x1 droite d equation x1=Cste

          xIntersect = x;
          yIntersect = a * x + b;
        }

        // maintenant que l on a le point d'intersection, on test si on ajoute en positif ou negatif
        if (y >= ySeuil1 && y2 <= ySeuil2) {
          data.addZonePlus(calculAireTrapeze(x, y, xIntersect, yIntersect, ySeuil1, ySeuil2));
          data.addZoneMoins(calculAireTrapeze(xIntersect, yIntersect, x2, y2, ySeuil1, ySeuil2));

        } else {
          data.addZoneMoins(calculAireTrapeze(x, y, xIntersect, yIntersect, ySeuil1, ySeuil2));
          data.addZonePlus(calculAireTrapeze(xIntersect, yIntersect, x2, y2, ySeuil1, ySeuil2));
        }

      }

    }

    return data;
  }

  /**
   * calcul de l'aire du trapeze form� par les 4 points (x1,y1) (x2,y2) (x1,y3) (x2,y4)
   */
  public static double calculAireTrapeze(double x, double y, double x2, double y2, double y3, double y4) {
    // 1: calcul de la hauteur
    double h = Math.abs(x2 - x);
    // 2 base 1: coordonnees cartesiennes entre le point x1,y1 et x1,y3
    double base1 = Math.abs(y3 - y);
    // 3 base 2: coordonnees cartesiennes entre le point x2,y2 et x2,y4
    double base2 = Math.abs(y4 - y2);
    // 4 calcul de l'aire
    return h * (base1 + base2) / 2;
  }

  public static void computeBilan(double x1, double x2, double y1, double y2, double seuil, DataIntegrale targetData) {
    if (y1 >= seuil && y2 >= seuil) {
      // courbe au dessus de la ligne
      targetData.addZonePlus(calculAireTrapeze(x1, y1, x2, y2, seuil, seuil));
    } else if (y1 <= seuil && y2 <= seuil) {
      // courbe en dessous de la ligne
      targetData.addZoneMoins(calculAireTrapeze(x1, y1, x2, y2, seuil, seuil));
    } else {
      // cas particulier : intersection, on calcule l'intersection entre les
      // points
      final double yIntersect = seuil;
      double xIntersect = 0;
      // on prend la droite form�e par les 2 points et on calcule son
      // intersection avec la droite affine
      if (x2 - x1 != 0) {
        // droite non parrallele a l axe des ordonn�e !
        double coefDirecteur = (y2 - y1) / (x2 - x1);
        double ordoOrigine = y2 - coefDirecteur * x2;

        // intersection de droites avec le seuil
        // yIntersect = seuil;

        if (coefDirecteur != 0) // droie non parallele au seuil
        {
          xIntersect = (yIntersect - ordoOrigine) / coefDirecteur;
        } else {
          // droite parrallele confondue au seuil, on ne fais rien...
        }

      } else {
        // x2=x1 droite perpendiculaire au seuil, le point d'intersection est
        // donc (x1,seuil)
        xIntersect = x1;
        // yIntersect = seuil;
      }

      // maintenant que l on a le point d'intersection, on test si on ajoute en positif ou negatif
      if (y1 >= seuil && y2 <= seuil) {
        targetData.addZonePlus(calculAireTrapeze(x1, y1, xIntersect, yIntersect, seuil, seuil));
        targetData.addZoneMoins(calculAireTrapeze(xIntersect, yIntersect, x2, y2, seuil, seuil));

      } else {
        targetData.addZoneMoins(calculAireTrapeze(x1, y1, xIntersect, yIntersect, seuil, seuil));
        targetData.addZonePlus(calculAireTrapeze(xIntersect, yIntersect, x2, y2, seuil, seuil));
      }

    }
  }
}
