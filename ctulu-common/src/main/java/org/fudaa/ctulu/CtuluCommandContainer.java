/*
 * @creation 4 ao�t 2004
 * @modification $Date: 2007-01-17 10:45:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

/**
 * Une interface permettant d'indiquer que la classe pass�e en argument enregistre les commandes undo/redo de la
 * m�thode. Il vaut mieux utiliser cette interface que les classes CtuluCommandComposite ou CtuluCommandManage: plus
 * flexible.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluCommandContainer.java,v 1.4 2007-01-17 10:45:25 deniger Exp $
 */
public interface CtuluCommandContainer {

  /**
   * @param _cmd la commande a ajouter
   * @return true si ajoutee
   */
  boolean addCmd(CtuluCommand _cmd);

}