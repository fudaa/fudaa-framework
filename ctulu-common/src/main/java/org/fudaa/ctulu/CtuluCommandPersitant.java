/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2005-08-11 09:06:57 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;


/**
 * @author Fred Deniger
 * @version $Id: CtuluCommandPersitant.java,v 1.2 2005-08-11 09:06:57 deniger Exp $
 */
public interface CtuluCommandPersitant  {
  void undo();
  boolean canUndo();
}
