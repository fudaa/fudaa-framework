/*
 * @creation 7 ao�t 06
 * @modification $Date: 2007-05-04 13:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;

/**
 * @author fred deniger
 * @version $Id: RasterData.java,v 1.3 2007-05-04 13:43:25 deniger Exp $
 */
public class RasterData implements RasterDataInterface {

  transient CtuluImageContainer img_;

  TIntArrayList xImg_ = new TIntArrayList();

  TDoubleArrayList xReal_ = new TDoubleArrayList();

  TIntArrayList yImg_ = new TIntArrayList();

  TDoubleArrayList yReal_ = new TDoubleArrayList();

  @Override
  public boolean isImgFound() {
    return img_ != null && img_.isFileFound();
  }

  public String toLongString() {
    String s = super.toString();
    for (int i = 0; i < getNbPt(); i++) {
      s += CtuluLibString.LINE_SEP + "(" + getPtImgX(i) + " , " + getPtImgY(i) + ") " + getPtX(i) + " , " + getPtY(i);
    }
    return s;
  }

  @Override
  public void readImg(final boolean _force) throws IOException {
    if (img_ != null) {
      if (_force) {
        img_.resetCache();
      }
      img_.getSnapshot();
    }
  }

  @Override
  public CtuluImageContainer getImg() {
    return img_;
  }

  public void addPt(final int _xImage, final int _yImage, final double _x, final double _y) {
    xImg_.add(_xImage);
    yImg_.add(_yImage);
    xReal_.add(_x);
    yReal_.add(_y);

  }

  @Override
  public File getImgFile() {
    return img_.getFile();
  }

  @Override
  public int getNbPt() {
    return xImg_.size();
  }

  @Override
  public Point2D.Double[] getImgPts() {
    final Point2D.Double[] res = new Point2D.Double[getNbPt()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new Point2D.Double(getPtImgX(i), getPtImgY(i));
    }
    return res;

  }

  @Override
  public Point2D.Double[] getRealPts() {
    final Point2D.Double[] res = new Point2D.Double[getNbPt()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new Point2D.Double(getPtX(i), getPtY(i));
    }
    return res;

  }

  @Override
  public int getPtImgX(final int _i) {
    return xImg_.getQuick(_i);
  }

  @Override
  public int getPtImgY(final int _i) {
    return yImg_.getQuick(_i);
  }

  @Override
  public double getPtX(final int _i) {
    return xReal_.getQuick(_i);
  }

  @Override
  public double getPtY(final int _i) {
    return yReal_.getQuick(_i);
  }

  public void setImg(final CtuluImageContainer _imgFile) {
    img_ = _imgFile;
  }

  public void setImgFile(final File _imgFile) {
    img_ = new CtuluImageContainer(_imgFile);
  }

}
