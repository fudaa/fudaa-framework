/*
 *  @creation     28 janv. 2004
 *  @modification $Date: 2007-04-20 16:20:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

/**
 * Cette classe permet de mettre a jour automatiquement la progression d'une boucle. Il faut donner le nombre
 * d'iteration attendu et le nombre de mise a jour (ou etape) voulue.<p>
 * 
 * Exemple:
 * 
 * <pre>
 * int nbEle = _getNombreElementATraiter();
 * ProgressionUpdater prog = new ProgressionUpdater(_inter);
 * prog.setValue(4, nbEle);
 * for (int i = nbEle - 1; i &gt;= 0; i--) {
 *   //faire plein de chose
 *   prog.majAvancement();
 * }
 * </pre>
 * 
 * L'updater possede un compteur interne qui est mis a jour a chaque appel de {@link #majAvancement()}
 * 
 * @author deniger
 * @version $Id: ProgressionUpdater.java,v 1.6 2007-04-20 16:20:18 deniger Exp $
 */
public class ProgressionUpdater {

  /**
   * La progression qui doit etre mise a jour.
   */
  ProgressionInterface progress_;
  int pourcStep_;
  int pas_;
  int nextPas_;
  int avancementEnCours_;
  int iEnCours_;

  public ProgressionUpdater() {}

  /**
   * @param _progress le receveur de l'avancement
   */
  public ProgressionUpdater(final ProgressionInterface _progress) {
    setProgress(_progress);
    iEnCours_ = 0;
  }

  public ProgressionUpdater(final boolean _useMaxStep) {
    this();
    maxStepUsed_ = _useMaxStep;
  }

  /**
   * @param _progress le receveur de l'avancement
   */
  public ProgressionUpdater(final ProgressionInterface _progress, final boolean _useMaxStep) {
    this(_progress);
    maxStepUsed_ = _useMaxStep;
  }

  /**
   * @return le receveur
   */
  public ProgressionInterface getProgress() {
    return progress_;
  }

  /**
   * @param _interface le nouveau receveur
   */
  public final void setProgress(final ProgressionInterface _interface) {
    progress_ = _interface;
  }

  /**
   * Initialise l'updater avec le nombre d'�tapes voulues et le nombre d'iteration attendues.<p>
   * 
   * Exemple: <br>
   * vous avez une boucle qui contient 200 iteration et vous voulez mettre a jour la progression toutes les 50 (soit 4
   * �tapes). Il suffit d'appeler setValue(4,200). Appelle la fonction setValue(int,int,0,100)
   * 
   * @see #setValue(int, int, int, int)
   * @param _nbStep le nombre de maj voulue
   * @param _nbIterationMax le nombre total d'iteration
   */
  public void setValue(final int _nbStep, final int _nbIterationMax) {
    setValue(_nbStep, _nbIterationMax, 0, 100);
  }

  boolean maxStepUsed_ = true;

  /**
   * Initialise l'updater avec le nombre d'�tapes voulues et le nombre d'iterations attendues, le pourcentage initial
   * et l'�tendu du pourcentage.<p>
   * 
   * Exemple: <br>
   * vous avez une boucle qui contient 300 iterations et vous voulez mettre a jour la progression toutes les 100 (soit 3
   * �tapes). De plus, vous voulez que la progression commence � 20% et qu'elle se finisse a 80% ( soit une �tendue de
   * 60). Il suffit d'appeler setValue(3,300,20,60).<p>
   * 
   * Attention: <br>
   * si le nombre d'iteration pour chaque etape est trop
   * faible (&lt;20) le nombre d'etape sera de 2 pour eviter de mettre a jour trop souvent l'interface.
   * 
   * @param _nbStepToShow le nombre de maj voulue
   * @param _nbTotalIteration le nombre d'iteration
   * @param _pourcentOffSet le depart des pourcentages
   * @param _pourcentRange l'intervalle des pourcentages
   */
  public void setValue(final int _nbStepToShow, final int _nbTotalIteration, final int _pourcentOffSet,
      final int _pourcentRange) {
    int step = _nbStepToShow;
    if (maxStepUsed_) {
      step = ((_nbTotalIteration / _nbStepToShow) < 20) ? 4 : _nbStepToShow;
    }
    step=Math.min(step, _pourcentRange); // Le nombre de pas ne peut �tre sup�rieur � l'intervalle, sinon pas d'avanc�e.
    pourcStep_ = _pourcentRange / step;
    pas_ = _nbTotalIteration / step;
    nextPas_ = pas_;
    avancementEnCours_ = _pourcentOffSet;
    iEnCours_ = 0;
  }

  /**
   * Met a jour l'interface de progression uniquement.
   */
  public void majProgessionStateOnly() {
    if (progress_ != null) {
      progress_.setProgression(avancementEnCours_ > 100 ? 100 : avancementEnCours_);
    }
  }

  /**
   * Met a jour l'interface de progression uniquement avec la description <code>_message</code>.
   * 
   * @param _message le message a donne au receveur de progression
   */
  public void majProgessionStateOnly(final String _message) {
    if (progress_ != null) {
      progress_.setProgression(avancementEnCours_);
      progress_.setDesc(_message);
    }
  }

  /**
   * Calcul si i est superieur au pas attendu, et si oui, la progression est mise a jour.
   */
  public void majAvancement() {
    if (++iEnCours_ > nextPas_) {
      avancementEnCours_ += pourcStep_;
      nextPas_ += pas_;
      majProgessionStateOnly();
    }
  }

}
