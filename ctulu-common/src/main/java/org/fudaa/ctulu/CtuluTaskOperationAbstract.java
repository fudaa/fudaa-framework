/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2007-02-02 11:20:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuTask;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuUpdateGUI;

/**
 * Utiliser car BuTaskOperation recupere les exceptions qui pourraient etre utilisees par d' autres m�thodes.
 */
public abstract class CtuluTaskOperationAbstract extends BuTask implements CtuluTaskDelegate {

  Runnable run_;
  /** Un flag a tester regulierement dans la tache pour l'interrompre proprement.
      Ce flag est lev� par un autre thread */
  boolean stopIsRequested_=false;

  /**
   * Contient le traitement de la tache a r�aliser. Cette m�thode est � surcharger
   * pour chaque tache instanci�e.
   */
  public void act() {
    if (run_ != null) {
      run_.run();
    }
  }

  @Override
  public final void start(final Runnable _run) {
    run_ = _run;
    super.start();
  }

  public CtuluTaskOperationAbstract(final String _nom) {
    super();
    String nom = _nom;
    if ((_nom == null) || "".equals(_nom)) {
      nom = CtuluLib.getS("T�che");
    }
    setName(nom);
    setPriority(Thread.MIN_PRIORITY);
  }

  public void reset() {
    setName(CtuluLibString.EMPTY_STRING);
    setProgression(0);
  }

  @Override
  public final void run() {
    act();
    super.run();
    reset();
  }

  public void setDesc(final String _s) {
    super.setName(_s);
    
    BuTaskView tv=getTaskView();
    if(tv!=null) BuUpdateGUI.repaintLater(tv);

  }

  public static ProgressionInterface createComposite(final ProgressionInterface _prog1,
      final ProgressionInterface _prog2) {
    return new ProgressionInterface() {

      @Override
      public void reset() {
        _prog1.reset();
        _prog2.reset();

      }

      @Override
      public void setDesc(final String _s) {
        _prog1.setDesc(_s);
        _prog2.setDesc(_s);
      }

      @Override
      public void setProgression(final int _v) {
        _prog1.setProgression(_v);
        _prog2.setProgression(_v);

      }

    };

  }

  @Override
  public void start() {
    super.start();
  }

  /**
   * Demande l'arret de la tache depuis un autre thread.
   */
  public void requestStop() {
    stopIsRequested_=true;
  }

  /**
   * Test si un arret de tache a �t� demand� depuis un autre thread. A tester
   * r�guli�rement dans l'implementation de {@link #act}
   * @return true si un arret de la tache a �t� demand�.
   */
  public boolean isStopRequested() {
    return stopIsRequested_;
  }

  public void start(final int _priority) {
    setPriority(_priority);
    super.start();
  }
}
