/*
 *  @creation     4 ao�t 2004
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;

/**
 * @author Fred Deniger
 * @version $Id: CtuluResource.java,v 1.10 2006-09-19 14:36:53 deniger Exp $
 */
public class CtuluResource extends BuResource {
  /** Le nombre de decimales pour les editeur de double */
  public static final String COORDS_VISU_NB_DIGITS  ="gis.coords.visu.nbdigits";

  /**
   * Singleton.
   */
  public final static CtuluResource CTULU = new CtuluResource();

  protected CtuluResource() {
    super();
    setParent(BuResource.BU);
  }

  protected CtuluResource(final BuResource _parent) {
    setParent(_parent);
  }


  /**
   * @param _s la chaine a recuperer dans la bonne traduc
   * @param _v0 la chaine qui remplacera les {0}
   * @return la chaine traduite
   */
  public final String getString(final String _s,final String _v0){
    final  String r = getString(_s);
    return r==null?null:FuLib.replace(r, "{0}", _v0);
  }

  /**
   * @param _s la chaine a traduire et a transformer
   * @param _v0 la chaine qui remplacera les {0}
   * @param _v1 la chaine qui remplacera les {1}
   * @return la chaine traduite et transformee
   */
  public final String getString(final String _s,final String _v0,final String _v1){
    String r = getString(_s);
    if (r == null) {
      return r;
    }
    r = FuLib.replace(r, "{0}", _v0);
    r = FuLib.replace(r, "{1}", _v1);
    return r;
  }
  /**
   * @param _s la chaine a traduire et a transformer
   * @param _v0 la chaine qui remplacera les {0}
   * @param _v1 la chaine qui remplacera les {1}
   * @return la chaine traduite et transformee
   */
  public final String getString(final String _s,final String _v0,final String _v1,final String _v2){
    String r = getString(_s);
    if (r == null) {
      return r;
    }
    r = FuLib.replace(r, "{0}", _v0);
    r = FuLib.replace(r, "{1}", _v1);
    r = FuLib.replace(r, "{2}", _v2);
    return r;
  }

}