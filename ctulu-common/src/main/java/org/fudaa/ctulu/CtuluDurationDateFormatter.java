package org.fudaa.ctulu;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class CtuluDurationDateFormatter extends CtuluNumberFormat {

  private static final String DATE_FORMAT = "DATE_FORMAT";
  private SimpleDateFormat dateFormat;
  private String datePattern;

  private long referenceDate;

  public CtuluDurationDateFormatter() {
    this(null, new Date().getTime());
  }

  public static SimpleDateFormat createDefaultSimpleDateFormat() {
    return (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.MEDIUM);
  }

  public CtuluDurationDateFormatter(String dateFormat, long initDate) {
    super();
    initDateFormatFromPattern(dateFormat);
    this.referenceDate = initDate;
  }

  public void initFrom(CtuluDurationDateFormatter other) {
    if (other != null) {
      setDatePattern(other.getDatePattern());
      setReferenceDate(other.getReferenceDate());
    }

  }

  @Override
  public String format(double _d) {
    long millis = ((long) _d * 1000L) + referenceDate;
    return dateFormat.format(new Date(millis));
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return new CtuluDurationDateFormatter(datePattern, referenceDate);
  }

  public DateFormat getDateFormat() {
    return dateFormat;
  }

  public String getDatePattern() {
    return datePattern;
  }

  public long getReferenceDate() {
    return referenceDate;
  }

  private final void initDateFormatFromPattern(String dateFormat) {
    this.datePattern = dateFormat;
    if (CtuluLibString.isEmpty(dateFormat)) {
      this.dateFormat = createDefaultSimpleDateFormat();
    } else {
      try {
        this.dateFormat = new SimpleDateFormat(dateFormat);
      } catch (Exception e) {

      }
    }
    if (this.dateFormat == null) {
      this.dateFormat = createDefaultSimpleDateFormat();
    }
  }

  public void setDatePattern(String datePattern) {
    this.datePattern = datePattern;
    initDateFormatFromPattern(datePattern);
  }

  public void setReferenceDate(long initDate) {
    this.referenceDate = initDate;
  }

  @Override
  public String toLocalizedPattern() {
    return DATE_FORMAT + "|" + referenceDate + "|" + (datePattern == null ? "" : datePattern);
  }

  public static CtuluNumberFormatI buildFromPattern(String fmtPattern) {
    CtuluNumberFormatI fromLocalizedPattern = buildFromLocalizedPattern(fmtPattern);
    if (fromLocalizedPattern == null) {
      fromLocalizedPattern = CtuluDurationFormatter.restoreFromLocalizedPattern(fmtPattern);
    }
    return fromLocalizedPattern;
  }

  private static CtuluDurationDateFormatter buildFromLocalizedPattern(String res) {
    String prefix = DATE_FORMAT + "|";
    if (res.startsWith(prefix)) {
      String initDateFmt = res.substring(prefix.length());
      int idx = initDateFmt.indexOf('|');
      if (idx > 0) {
        String initDate = initDateFmt.substring(0, idx);
        String pattern = initDateFmt.substring(idx + 1);
        try {
          return new CtuluDurationDateFormatter(pattern, Long.parseLong(initDate));
        } catch (NumberFormatException e) {
        }
      }
    }
    return null;
  }
}
