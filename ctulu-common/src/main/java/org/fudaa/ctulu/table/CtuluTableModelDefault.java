/*
 * @creation 30 janv. 2006
 * @modification $Date: 2007-04-16 16:33:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.table;

import java.util.List;
import javax.swing.JTable;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

/**
 * @author fred deniger
 * @version $Id: CtuluTableModelDefault.java,v 1.4 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluTableModelDefault implements CtuluTableModelInterface {

  int[] column_;
  int[] row_;
  final JTable t_;
  private boolean commentSetByUser;
  private List<String> comments;
  CtuluTableModelExportDecorator decorator;

  public CtuluTableModelDefault(final JTable _t, final int[] _column, final int[] _row) {
    super();
    row_ = _row;
    column_ = _column;
    t_ = _t;
    decorator = (CtuluTableModelExportDecorator) _t.getClientProperty(CtuluTableModelExportDecorator.CLIENT_PROPERTY_);
  }

  public void setComments(List<String> comments) {
    commentSetByUser = true;
    this.comments = comments;
  }

  @Override
  public List<String> getComments() {
    if (commentSetByUser) {
      return comments;
    }
    return (List<String>) t_.getClientProperty(CtuluTableModelInterface.EXPORT_COMMENT_PROPERTY);
  }

  @Override
  public String getColumnName(final int _i) {
    if (column_ == null) {
      return t_.getColumnName(_i);
    }
    return t_.getColumnName(column_[_i]);
  }

  @Override
  public WritableCell getExcelWritable(int _row, int _col, int _rowInXls, int _colInXls) {

    int r = _row;
    int c = _col;
    if (column_ != null) {
      c = column_[c];
    }
    if (row_ != null) {
      r = row_[r];
    }
    final Object o = getValueAt(r, c);
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colInXls, _rowInXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {
    }
    return new Label(_colInXls, _rowInXls, s);
  }

  @Override
  public int getMaxCol() {
    return column_ == null ? t_.getColumnCount() : column_.length;
  }

  @Override
  public int getMaxRow() {
    return row_ == null ? t_.getRowCount() : row_.length;
  }

  @Override
  public Object getValue(final int _row, final int _col) {
    int r = _row;
    int c = _col;
    if (column_ != null) {
      c = column_[c];
    }
    if (row_ != null) {
      r = row_[r];
    }
    return getValueAt(r, c);
  }

  protected Object getValueAt(int row, int col) {
    if (decorator != null && decorator.hasSpecificExportValue(col)) {
      return decorator.getValue(row, col);
    }
    return t_.getValueAt(row, col);
  }

    @Override
    public int[] getSelectedRows() {
        return null;
    }
}