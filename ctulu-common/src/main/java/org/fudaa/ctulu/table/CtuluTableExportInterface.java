/*
 GPL 2
 */
package org.fudaa.ctulu.table;

/**
 *
 * @author Frederic Deniger
 */
public interface CtuluTableExportInterface {

  boolean isColumnExportable(int colModel);

}
