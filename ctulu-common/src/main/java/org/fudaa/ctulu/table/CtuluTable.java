/*
 * @modification $Date: 2007-02-02 11:20:10 $
 * @statut       unstable
 * @file         CtuluTable.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */
package org.fudaa.ctulu.table;

import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableStaticModel;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.*;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ToStringTransformable;
import skt.swing.table.TableColumnAutoResizer;
import skt.swing.table.TableColumnResizer;
import skt.swing.table.TableRowResizer;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.lang.Number;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Similar to JTable but add copy/select operations and row headers.
 *
 * @see javax.swing.JTable
 */
public class CtuluTable extends BuTable {
  // private String lineSeparator_ = "\n\r\f";
  // Fred: on remplace le s�parateur de ligne ( complique et ne marche pas pour openoffice par un \n tout simple.
  // lors de l'import d'une chaine on remplacera les \r et \f par des espaces
  private String lineSeparator_ = CtuluLibString.LINE_SEP_SIMPLE;
  private String cellSeparator_ = "\t";
  /**
   * Ajoute des lignes lors d'un paste. Sinon, le paste s'arrete au nombre de lignes existantes
   */
  private boolean rowsAddedWhenPaste = false;

  public CtuluTable() {
    super();
    init();
  }

  public CtuluTable(final TableModel _model) {
    super(_model);
    init();
  }

  public CtuluTable(final Object[][] _values, final Object[] _names) {
    this(new BuTableStaticModel(_values, _names));
  }

  // JTree has convertValueToText(), but why not JTable ???
  public String convertValueToText(final Object _value, final int _row, final int _column) {
    if (_value != null) {
      final String sValue = _value.toString();
      if (sValue != null) {
        return sValue;
      }
    }
    return "";
  }

  @Override
  public String getToolTipText(final MouseEvent _evt) {
    String tip = null;
    final Point p = _evt.getPoint();

    // Locate the renderer under the event location
    final int hitColumnIndex = columnAtPoint(p);
    final int hitRowIndex = rowAtPoint(p);

    if ((hitColumnIndex != -1) && (hitRowIndex != -1)) {
      final TableCellRenderer renderer = getCellRenderer(hitRowIndex, hitColumnIndex);
      final Component component = prepareRenderer(renderer, hitRowIndex, hitColumnIndex);

      // Now have to see if the component is a JComponent before
      // getting the tip
      if (component instanceof JComponent) {
        // Convert the event to the renderer's coordinate system
        final Rectangle cellRect = getCellRect(hitRowIndex, hitColumnIndex, false);
        if (cellRect.width >= component.getPreferredSize().width) {
          return super.getToolTipText(_evt);
        }
        p.translate(-cellRect.x, -cellRect.y);
        final MouseEvent newEvent = new MouseEvent(component, _evt.getID(), _evt.getWhen(), _evt.getModifiers(), p.x,
          p.y, _evt.getClickCount(), _evt.isPopupTrigger());

        tip = ((JComponent) component).getToolTipText(newEvent);
      }
    }

    // No tip from the renderer, see whether any tooltip is set on JTable
    if (tip == null) {
      tip = getToolTipText();
    }

    // calculate tooltip from cell value
    if (tip == null && hitRowIndex >= 0 && hitColumnIndex >= 0) {
      final Object value = getValueAt(hitRowIndex, hitColumnIndex);
      tip = convertValueToText(value, hitRowIndex, hitColumnIndex);
      if (tip.length() == 0) {
        tip = null; // don't show empty tooltips
      }
    }
    return tip;
  }

  // makes the tooltip's location to match table cell location
  // also avoids showing empty tooltips
  @Override
  public Point getToolTipLocation(final MouseEvent _evt) {
    final int row = rowAtPoint(_evt.getPoint());
    if (row == -1) {
      return null;
    }
    final int col = columnAtPoint(_evt.getPoint());
    if (col == -1) {
      return null;
    }

    // to avoid empty tooltips - return null location
    final boolean hasTooltip = getToolTipText() != null || getToolTipText(_evt) != null;

    return hasTooltip ? getCellRect(row, col, false).getLocation() : null;
  }

  protected void init() {

    this.getActionMap().put("paste", new AbstractAction("paste") {
      @Override
      public void actionPerformed(final ActionEvent _evt) {
        paste();
      }
    });
    this.getActionMap().put("copy", new AbstractAction("copy") {
        @Override
        public void actionPerformed(final ActionEvent _evt) {
          copy();
        }
      }

    );
  }

  /**
   * @param _lineSeparator String
   */
  public void setLineSeparator(final String _lineSeparator) {
    this.lineSeparator_ = _lineSeparator;
  }

  /**
   * @param _cellSeparator String
   */
  public void setCellSeparator(final String _cellSeparator) {
    this.cellSeparator_ = _cellSeparator;
  }

  /**
   * @param _idxColumn :l'indice de la colonne
   */
  @Override
  public void hideColumn(final int _idxColumn) {
    getColumnModel().removeColumn(getColumnModel().getColumn(_idxColumn));
  }

  public void hideColumn(final TableColumn _col) {
    final Object o = getTableHeader();
    if (o instanceof CtuluTableColumnHeader) {
      ((CtuluTableColumnHeader) o).hideColumn(_col);
    }
  }

  @Override
  public void showColumn(final int _indexInModel) {
    final Object o = getTableHeader();
    if (o instanceof CtuluTableColumnHeader) {
      ((CtuluTableColumnHeader) o).showColumn(_indexInModel);
    } else {
      super.showColumn(_indexInModel);
    }
  }

  /**
   * @return String
   */
  public String getLineSeparator() {
    return lineSeparator_;
  }

  /**
   * @return String
   */
  public String getCellSeparator() {
    return cellSeparator_;
  }

  public void paste() {
    try {
      String chaine = getClipboard();
      insertTableInTable(convertClipboardToTable(chaine, lineSeparator_, cellSeparator_, ','));
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * getClipboard.
   *
   * @return String
   */
  public static String getClipboard() throws UnsupportedFlavorException, IOException {
    final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    return (String) clipboard.getContents(null).getTransferData(DataFlavor.stringFlavor);
  }

  /**
   * @param _chaine String
   * @param _lineseparator String
   * @param _cellseparator String
   * @param _localDecimalSeparator char
   * @return ArrayList
   */
  public static ArrayList convertClipboardToTable(final String _chaine, final String _lineseparator,
                                                  final String _cellseparator, final char _localDecimalSeparator) {
    // ajouter les traitements des exceptions lev�es par les deux methodes de parse.
    final ArrayList tab = new ArrayList();
    if (_chaine != null) {
      // Fred: on va remplacer le \r et \f par rien
      String chaine = _chaine.replace('\r', ' ');
      chaine = chaine.replace('\f', ' ');
      final ArrayList listLine = parseLineFromClipboard(chaine, _lineseparator);
      for (Object o : listLine) {
        tab.add(parseCellFromClipboard((String) o, _cellseparator, _localDecimalSeparator));
      }
    }
    return tab;
  }

  /**
   * @param _chaine String
   * @param _lineseparator String
   * @return ArrayList
   */
  private static ArrayList<String> parseLineFromClipboard(final String _chaine, final String _lineseparator) {
    final ArrayList<String> listLigne = new ArrayList<>();

    final StringTokenizer st = new StringTokenizer(_chaine, _lineseparator);
    while (st.hasMoreTokens()) {
      listLigne.add(st.nextToken());
    }
    return listLigne;
  }

  /**
   * @param _line String
   * @param _cellseparator String
   * @param _localDecimalSeparator char
   * @return ArrayList
   */
  private static ArrayList parseCellFromClipboard(final String _line, final String _cellseparator,
                                                  final char _localDecimalSeparator) {
    final ArrayList listCell = new ArrayList(50);
    boolean lasttoken = false;

    final StringTokenizer st = new StringTokenizer(_line, _cellseparator, true);
    while (st.hasMoreTokens()) {
      String cell = st.nextToken();
      final boolean isCellSeparator = cell.equals(_cellseparator);
      cell = cell.trim();
      if (isCellSeparator) {
        if (lasttoken) {
          lasttoken = false;
        } else {
          listCell.add(null);
          lasttoken = false;
        }
      } else {
        cell = parseDecimalSeparator(cell, _localDecimalSeparator);
        listCell.add(cell);
        lasttoken = true;
      }
    }
    return listCell;
  }

  /**
   * parseDecimalSeparator.
   *
   * @param _chaine String
   * @param _localDecimalSeparator char
   * @return String
   */
  private static String parseDecimalSeparator(final String _chaine, final char _localDecimalSeparator) {

    return _chaine.replace(_localDecimalSeparator, '.');
  }

  /**
   * @param _tab ArrayList
   */
  protected void insertTableInTable(final ArrayList _tab) {
    ArrayList listCell;
    int selectedColumm = getSelectedColumn();
    int selectedRow = getSelectedRow();
    final int numberColumm = getColumnCount();
    boolean update = false;

    if ((selectedColumm == -1) || (selectedRow == -1)) {
      // BM 27-08-2018 : S'il n'y a pas de selection, on commencera le paste a la colonne 1 (La colonne 0 est l'index, qui est non editable).
      selectedColumm = 1;
      selectedRow = getRowCount();
    }
    if (selectedColumm == 0 && getModel() instanceof CtuluTableExportInterface) {
      int modelIndex = getColumnModel().getColumn(0).getModelIndex();
      if (!((CtuluTableExportInterface) getModel()).isColumnExportable(modelIndex)) {
        selectedColumm = 1;
      }
    }

    for (int i = 0; i < _tab.size(); i++) {
      // / BM 27-08-2018 : Ajout de lignes si le paste d�passe les limites et si le modele est modifiable.
      if (rowsAddedWhenPaste && (i + selectedRow) >= getRowCount()) {
        if (getModel() instanceof CtuluMutableTableModel) {
          ((CtuluMutableTableModel) getModel()).addRow();
        }
      }

      if ((i + selectedRow) < getRowCount()) {
        listCell = (ArrayList) _tab.get(i);
        for (int j = 0; j < listCell.size(); j++) {
          final int iCellule = i + selectedRow;
          final int jCellule = j + selectedColumm;
          if (jCellule < numberColumm) {
            if (isCellEditable(iCellule, jCellule)) {
              final Object object = convertDataToObject(this.getColumnClass(jCellule), (String) listCell.get(j));
              update = true;
              setValueAt(object, iCellule, jCellule);
              if (object == null) {
                FuLog.warning("CCG: Impossible convert data from Clipboarb to Table \n");
              }
            } else {
              FuLog.warning("CCG:  cellule (" + iCellule + ',' + jCellule + ") is not editable\n");
            }
          }
        }
      }
    }
    if (update) {
      this.updateUI();
    }
  }

  /**
   * convertDataToOject.
   *
   * @param _class Class
   * @param _chaine String
   * @return Object
   */
  private Object convertDataToObject(final Class _class, final String _chaine) {
    if (_chaine == null) {
      return null;
    }
    // cas simple d'une chaine de caractere
    if (_class == String.class) {
      return _chaine;
    }
    if (_class == Double.class) {
      try {
        return new Double(_chaine);
      } catch (final NumberFormatException _evt) {
        FuLog.error(_evt);
      }
    } else if (_class == Integer.class) {
      try {
        return new Integer(_chaine);
      } catch (final NumberFormatException _evt) {
        FuLog.error(_evt);
      }
    } else if (_class == Float.class) {
      try {
        return new Float(_chaine);
      } catch (final NumberFormatException _evt) {
        FuLog.error(_evt);
      }
    } else {
      try {
        final Object[] object = new Object[1];
        final Constructor[] tabContructor = _class.getConstructors();
        for (int i = 0; i < tabContructor.length; i++) {
          final Class[] parameters = tabContructor[i].getParameterTypes();
          if ((parameters.length == 1) && (parameters[0].equals(java.lang.String.class))) {
            object[0] = _chaine;
            return tabContructor[i].newInstance(object);
          }
        }
      } catch (final SecurityException ex) {
        FuLog.warning(ex);
      } catch (final InstantiationException ex) {
        FuLog.warning(ex);
      } catch (final InvocationTargetException ex) {
        FuLog.warning(ex);
      } catch (final IllegalAccessException ex) {
        FuLog.warning(ex);
      }
    }
    return null;
  }

  /**
   * Exportation vers un fichier Excel (.xls).
   *
   * @param _fichier Le fichier sur lequel on �crit.
   * @throws IOException    En cas d'erreur d'entr�-sortie sur le fichier.
   * @throws WriteException En cas d'erreur de g�n�ration du classeur Excel.
   */
  public void exportExcel(final File _fichier) throws IOException, WriteException {
    final WritableWorkbook classeur = Workbook.createWorkbook(_fichier);

    WritableSheet feuille = null;
    feuille = classeur.createSheet(getName(), 0);
    ecritSurFeuilleExcel(feuille, 0, 0);
    classeur.write();
    classeur.close();
  }

  /**
   * Lance Excel avec le contenu du tableau sur plateforme Windows.
   *
   * @throws IOException    En cas d'erreur d'entr�-sortie sur le fichier.
   * @throws WriteException En cas d'erreur de g�n�ration du classeur Excel.
   */
  public void lanceExcel() throws IOException, WriteException {
    if (FuLib.isWindows()) {
      final File fichierTemp = new File(System.getProperty("java.io.tmpdir") + "\\FudaaTMP.xls");
      if (fichierTemp.exists()) {
        fichierTemp.delete();
      }
      fichierTemp.deleteOnExit();
      exportExcel(fichierTemp);
      final String[] cmds = new String[4];
      cmds[0] = "cmd";
      cmds[1] = "/c";
      cmds[2] = "start";
      cmds[3] = fichierTemp.getAbsolutePath();
      FuLib.runProgram(cmds);
    }
  }

  @Override
  protected String transformToString(Object value) {
    if (value instanceof ToStringTransformable) {
      return ((ToStringTransformable) value).getAsString();
    }
    return super.transformToString(value);
  }

  /**
   * Ecrit sur une feuille Excel le tableau.
   *
   * @param _feuille WritableSheet
   * @param _colonneOrigine Indice de la colonne de la feuille Excel de d�but d'�criture.
   * @param _ligneOrigine Indice de la ligne de la feuille Excel de d�but d'�criture.
   * @return Derni�re ligne d'�criture sur la feuille Excel.
   * @throws WriteException En cas d'erreur de g�n�ration de la feuille Excel.
   */
  public int ecritSurFeuilleExcel(final WritableSheet _feuille, final int _colonneOrigine, final int _ligneOrigine)
    throws WriteException {
    final int col0 = _colonneOrigine;

    final WritableFont arial10ptBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
    final WritableCellFormat arial10BoldFormatNoBorder = new WritableCellFormat(arial10ptBold);

    final int nbLigneVideFin = rechercheNbLigneVideFin();

    // tableau des nbMax de caract�re par colonne
    final int[] nbMaxCaractereParColonne = new int[getColumnCount()];

    for (int i = 0; i < getColumnCount(); i++) {

      final int col = col0 + i;

      // Ent�te de la i�me colonne
      final String columnName = getColumnName(i);
      _feuille.addCell(new Label(col, _ligneOrigine, columnName, arial10BoldFormatNoBorder));
      nbMaxCaractereParColonne[i] = columnName.length() + 2;

      final TableCellRenderer renderer = getDefaultRenderer(getColumnClass(i));

      NumberFormat numFormat = null;
      NumberFormat intFormat = null;
      DateFormat dateFormat = null;
      if (renderer instanceof CtuluTableCellRenderer) {
        final CtuluTableCellRenderer buRenderer = (CtuluTableCellRenderer) renderer;
        numFormat = buRenderer.getNumberFormat();
        intFormat = buRenderer.getIntegerFormat();
        dateFormat = buRenderer.getDateFormat();
      }

      // corps du tableau de la i�me colonne
      final int derniereLigne = getRowCount() - nbLigneVideFin;
      for (int j = 0; j < derniereLigne; j++) {
        final int lig = j + 1 + _ligneOrigine;

        final Object value = getValueAt(j, i);

        WritableCellFormat format = new WritableCellFormat();
        String valeurTexte = null;

        if (value instanceof Number) {
          if ((intFormat != null) && (value instanceof Integer)) {
            valeurTexte = intFormat.format(value);
          } else if (numFormat == null) {
            valeurTexte = value.toString();
          } else {
            valeurTexte = numFormat.format(value);
            if (numFormat instanceof DecimalFormat) {
              final jxl.write.NumberFormat nf = new jxl.write.NumberFormat(((DecimalFormat) numFormat).toPattern());
              format = new WritableCellFormat(nf);
            }
          }
          _feuille.addCell(new jxl.write.Number(col, lig, ((Number) value).doubleValue(), format));
        } else if (value instanceof Date) {
          if (dateFormat == null) {
            valeurTexte = value.toString();
          } else {
            valeurTexte = dateFormat.format(value);
            if (dateFormat instanceof SimpleDateFormat) {
              final SimpleDateFormat simpleDateF = (SimpleDateFormat) dateFormat;
              final jxl.write.DateFormat df = new jxl.write.DateFormat(simpleDateF.toPattern());
              format = new WritableCellFormat(df);
            }
          }
          _feuille.addCell(new jxl.write.DateTime(col, lig, (Date) value, format));
        } else {

          final Component comp = renderer.getTableCellRendererComponent(this, value, false, false, j, i);
          if (comp instanceof JLabel) {
            final JLabel label = (JLabel) comp;
            valeurTexte = label.getText();
            switch (label.getVerticalAlignment()) {
              case SwingConstants.TOP:
                format.setVerticalAlignment(VerticalAlignment.TOP);
                break;
              case SwingConstants.BOTTOM:
                format.setVerticalAlignment(VerticalAlignment.BOTTOM);
                break;
              default:
                format.setVerticalAlignment(VerticalAlignment.CENTRE);
                break;
            }
            switch (label.getHorizontalAlignment()) {
              case SwingConstants.RIGHT:
                format.setAlignment(Alignment.RIGHT);
                break;
              case SwingConstants.LEFT:
                format.setAlignment(Alignment.LEFT);
                break;
              default:
                format.setAlignment(Alignment.CENTRE);
                break;
            }
          } else {
            valeurTexte = transformToString(value);
            format.setVerticalAlignment(VerticalAlignment.CENTRE);
            format.setAlignment(Alignment.CENTRE);
          }
          _feuille.addCell(new Label(col, lig, valeurTexte, format));
        }
        nbMaxCaractereParColonne[i] = Math.max(nbMaxCaractereParColonne[i], valeurTexte.length());
      }

      final int taillecolonne = _feuille.getColumnView(col).getSize() / 256;
      if (taillecolonne < nbMaxCaractereParColonne[i]) {
        _feuille.setColumnView(col, nbMaxCaractereParColonne[i]);
      }
    }
    return getRowCount() - nbLigneVideFin + _ligneOrigine;
  }

  /**
   * Recherche le nombre de ligne vide � la fin du tableau.
   *
   * @return le nombre de ligne contenant que des valeurs nulles.
   */
  public int rechercheNbLigneVideFin() {
    int nbLigneVide = 0;
    for (int i = getRowCount() - 1; i >= 0; i--) {
      if (!isLigneVide(i)) {
        return nbLigneVide;
      }
      nbLigneVide++;
    }
    return getRowCount();
  }

  /**
   * Test si la ligne est vide (contient que des valeurs nulles).
   *
   * @param _index de la ligne
   * @return true si la ligne est vide.
   */
  public boolean isLigneVide(final int _index) {
    for (int i = 0; i < getColumnCount(); i++) {
      if (getValueAt(_index, i) != null) {
        return false;
      }
    }
    return true;
  }

  // la suite a ete recopie de MyTable https://myswing.dev.java.net/MyBlog/MySwingTree.html
  private static final transient TableColumnAutoResizer COLUMN_AUTO_RESIZER = new TableColumnAutoResizer();

  @Override
  public void addNotify() {
    super.addNotify();
    if (getTableHeader() != null) {
      getTableHeader().addMouseListener(COLUMN_AUTO_RESIZER);
    }
  }

  @Override
  public void removeNotify() {
    super.removeNotify();
    if (getTableHeader() != null) {
      getTableHeader().removeMouseListener(COLUMN_AUTO_RESIZER);
    }
  }

  @Override
  public void setTableHeader(final JTableHeader _tableHeader) {
    if (getTableHeader() != null) {
      getTableHeader().removeMouseListener(COLUMN_AUTO_RESIZER);
    }
    super.setTableHeader(_tableHeader);
    if (_tableHeader != null && isShowing()) {
      _tableHeader.addMouseListener(COLUMN_AUTO_RESIZER);
    }
  }

  protected transient MouseInputAdapter rowResizer_;
  protected transient MouseInputAdapter columnResizer_;

  // turn resizing on/of
  public void setResizable(final boolean _row, final boolean _column) {
    if (_row) {
      if (rowResizer_ == null) {
        rowResizer_ = new TableRowResizer(this);
      }
    } else if (rowResizer_ != null) {
      removeMouseListener(rowResizer_);
      removeMouseMotionListener(rowResizer_);
      rowResizer_ = null;
    }
    if (_column) {
      if (columnResizer_ == null) {
        columnResizer_ = new TableColumnResizer(this);
      }
    } else if (columnResizer_ != null) {
      removeMouseListener(columnResizer_);
      removeMouseMotionListener(columnResizer_);
      columnResizer_ = null;
    }
  }

  /**
   * @param _b True : Indique que le nombre de lignes sera modifi� si le paste depasse le nombre de lignes existantes
   */
  public void setAutoExtendRowsWhenPaste(boolean _b) {
    rowsAddedWhenPaste = _b;
  }

  /**
   * @return True : Indique que le nombre de lignes sera modifi� si le paste depasse le nombre de lignes existantes
   */
  public boolean isAutoExtendRowsWhenPaste() {
    return rowsAddedWhenPaste;
  }

  // mouse events intended for resize shouldn't activate editing
  @Override
  public boolean editCellAt(final int _row, final int _column, final EventObject _evt) {
    if (getCursor() == TableColumnResizer.resizeCursor || getCursor() == TableRowResizer.RESIZE_CURSOR) {
      return false;
    }
    return super.editCellAt(_row, _column, _evt);
  }

  // mouse press intended for resize shouldn't change row/col/cell celection
  @Override
  public void changeSelection(final int _row, final int _column, final boolean _toggle, final boolean _extend) {
    if (getCursor() == TableColumnResizer.resizeCursor || getCursor() == TableRowResizer.RESIZE_CURSOR) {
      return;
    }
    try {
      super.changeSelection(_row, _column, _toggle, _extend);
    } catch (Exception e) {
      Logger.getLogger(CtuluTable.class.getName()).log(Level.WARNING, "message {0}", e);
    }
  }

  /*
   * -------------------------------------------------[ Scrollable ]---------------------------------------------------
   */
  // overriden to make the height of scroll match viewpost height
  // if smaller
  @Override
  public boolean getScrollableTracksViewportHeight() {
    return getPreferredSize().height < getParent().getHeight();
  }
}
