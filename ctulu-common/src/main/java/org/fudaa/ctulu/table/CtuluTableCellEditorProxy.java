/*
 * @creation     10 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.table;

import java.awt.Component;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

/**
 * Cette classe est un proxy qui permet de choisir un �ditor diff�rent par
 * ligne. Il est possible de donner les editeurs soit via un table soit via une
 * map.
 * 
 * tableau : le tableau doit avoir le m�me nombre de case que le nombre de ligne
 * dans le tableau � �diter. Si ce n'est pas le cas, les cellules
 * suppl�mentaires seront consid�r�es comme non �ditable. Il est possible de
 * donner des valeurs null dans le tableau, il sera alors consid�r� que la
 * cellule n'est pas �ditable.
 * 
 * map : l'avantage de la map par rapport au tableau est de ne renseigner que
 * les lignes voulues. Il est possible de donner un index par defaut. Cet index
 * servira � retourner l'editeur par defaut si il n'y en a pas de d�fini �
 * l'index demand�. Si aucune instance n'est renseign�e pour l'index par defaut,
 * les cases seront consid�r�es comme non �ditable. Comme le tableau, la valeur
 * null pour un �diteur signifie que la cellule n'est pas �ditable.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class CtuluTableCellEditorProxy extends AbstractCellEditor implements TableCellEditor, CellEditorListener {
  
  /** Le container d'editor, la clef est la ligne et la valeur le TableCellEditor.*/
  private final Map<Integer, TableCellEditor> mapEditors_;
  /** Index par defaut en cas d'absence de la ligne dans la map. */
  private final int defaultIndex_;
  /** L'index utilis� actuellement. */
  private int currentIndex_;
  /**
   * Vrai si la valeur courant est �ditable. On ne peut pas se passer de cette
   * variable en donnant une valeur sp�ciale � currentIndex_ car defaultIndex_
   * peut prendre toutes les valeurs possibles.
   */
  private boolean editable_;

  /**
   * Si le tableau n'est pas assez grand, null sera retourn� par la method
   * getTableCellEditorComponent.
   * 
   * Si _editors est null IllegalArgumentException sera lev�e.
   * 
   * @param _editors
   *          Tableau contenant l'editeur pour chaque ligne. Une valeur null
   *          pour un editeur undique que la cellule n'est pas �ditable, null
   *          sera renvoy� par getTableCellEditorComponent.
   */
  public CtuluTableCellEditorProxy(TableCellEditor[] _editors) {
    if (_editors==null)
      throw new IllegalArgumentException("_editors de doit pas �tre null.");
    mapEditors_=new HashMap<Integer, TableCellEditor>();
    defaultIndex_=-1;
    currentIndex_=-1;
    editable_=false;
    for (int i=0; i<_editors.length; i++)
      mapEditors_.put(i, _editors[i]);
  }

  /**
   * Si _editors est null IllegalArgumentException sera lev�e.
   * 
   * @param _editors
   *          Une Map avec pour key le num�ro de la ligne et pour Value un
   *          TableCellEditor. Une valeur null pour un editeur indique que la
   *          cellule n'est pas �ditable : null sera renvoy� par
   *          getTableCellEditorComponent.
   * @param _defaultChoice
   *          Si des lignes ne sont pas renseign�s, la Key _defaultChoice sera
   *          utilis�e � la place. Si la valeur par defaut n'est pas dans la map,
   *          null sera renvoy� par getTableCellEditor.
   */
  public CtuluTableCellEditorProxy(Map<Integer, TableCellEditor> _editors, int _defaultChoice) {
    if (_editors==null)
      throw new IllegalArgumentException("_editors de doit pas �tre null.");
    defaultIndex_=_defaultChoice;
    currentIndex_=-1;
    editable_=false;
    mapEditors_=_editors;
  }

  /* (non-Javadoc)
   * @see javax.swing.table.TableCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
   */
  @Override
  public Component getTableCellEditorComponent(JTable _table, Object _value, boolean _isSelected, int _row, int _column) {
    // Suppression de l'�coute d'�venement sur l'ancien editeur
    if (editable_)
      mapEditors_.get(currentIndex_).removeCellEditorListener(this);
    editable_=true;
    // Actualisation de currentIndex_
    if (mapEditors_.containsKey(_row))
      if (mapEditors_.get(_row)!=null)
        // Cas normal
        currentIndex_=_row;
      else
        // Cas o� la cellule n'est pas �ditable
        editable_=false;
    else if (mapEditors_.containsKey(defaultIndex_))
      // Cas o� l'editeur n'est pas renseign� : on utilise la valeur par defaut
      if (mapEditors_.get(defaultIndex_)!=null)
        // Cas normal de l'utilisation de l'editeur par defaut
        currentIndex_=defaultIndex_;
      else
        // Cas o� l'index par defaut pointe sur null
        editable_=false;
    else
      // Cas o� la valeur par defaut n'existe pas : la case est non �ditable
      editable_=false;
    if (editable_) {
      // Ajout de l'�coute d'�venement sur le nouvel �diteur et transfert
      // d'appelle
      mapEditors_.get(currentIndex_).addCellEditorListener(this);
      return mapEditors_.get(currentIndex_).getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
    }
    else
      return null;
  }

  /**
  * Permet d'ajouter ou de remplacer un des �diteurs utilis�.
  * 
  * @param _indexRow : l'index de la ligne � ajouter ou remplacer
  * @param _editor : le nouvel �diteur
  */
 public void putEditor(Integer _indexRow, TableCellEditor _editor){
   if(editable_&&_indexRow==currentIndex_){
     cancelCellEditing();
     editable_=false;
   }
   mapEditors_.put(_indexRow, _editor);
 }
  
  /**
   * Supprime tout les editeurs.
   */
  public void clear(){
    if(editable_)
      stopCellEditing();
    mapEditors_.clear();
    editable_=false;
  }
  
  /**
   * Supprime si il est d�fini l'editeur situ� � l'index _rowIndex
   * @param _rowIndex : l'index de l'�diteur � supprimer
   */
  public void remove(int _rowIndex){
    mapEditors_.remove(_rowIndex);
  }
  
  /* (non-Javadoc)
   * @see javax.swing.CellEditor#cancelCellEditing()
   */
  @Override
  public void cancelCellEditing() {
    if(editable_)
      mapEditors_.get(currentIndex_).cancelCellEditing();
  }

  /* (non-Javadoc)
   * @see javax.swing.CellEditor#getCellEditorValue()
   */
  @Override
  public Object getCellEditorValue() {
    if(editable_)
      return mapEditors_.get(currentIndex_).getCellEditorValue();
    return null;
  }

  /* (non-Javadoc)
   * @see javax.swing.CellEditor#isCellEditable(java.util.EventObject)
   */
  @Override
  public boolean isCellEditable(EventObject _anEvent) {
    if(editable_)
      return mapEditors_.get(currentIndex_).isCellEditable(_anEvent);
    return true;
  }

  /* (non-Javadoc)
   * @see javax.swing.CellEditor#shouldSelectCell(java.util.EventObject)
   */
  @Override
  public boolean shouldSelectCell(EventObject _anEvent) {
    if(editable_)
      return mapEditors_.get(currentIndex_).shouldSelectCell(_anEvent);
    return true;
  }

  /* (non-Javadoc)
   * @see javax.swing.CellEditor#stopCellEditing()
   */
  @Override
  public boolean stopCellEditing() {
    if(editable_)
      return mapEditors_.get(currentIndex_).stopCellEditing();
    return true;
  }

  /* (non-Javadoc)
   * @see javax.swing.event.CellEditorListener#editingCanceled(javax.swing.event.ChangeEvent)
   */
  @Override
  public void editingCanceled(ChangeEvent e) {
    fireEditingCanceled();
  }

  /* (non-Javadoc)
   * @see javax.swing.event.CellEditorListener#editingStopped(javax.swing.event.ChangeEvent)
   */
  @Override
  public void editingStopped(ChangeEvent e) {
    fireEditingStopped();
  }
}
