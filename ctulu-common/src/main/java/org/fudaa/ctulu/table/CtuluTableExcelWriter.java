/**
 *  @creation     11 f�vr. 2005
 *  @modification $Date: 2007-04-16 16:33:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.table;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import java.io.IOException;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTableExcelWriter.java,v 1.7 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluTableExcelWriter implements CtuluWriter {

  CtuluTableModelInterface table_;

  File dest_;

  /**
   * @param _table la table a ecrire
   * @param _dest le fichier de destination
   */
  public CtuluTableExcelWriter(final CtuluTableModelInterface _table, final File _dest) {
    super();
    table_ = _table;
    dest_ = _dest;
  }

  public CtuluTableExcelWriter() {
    super();
  }
  
  /**
   * @param _p le receveur de l'avancement
   * @throws IOException
   * @throws WriteException
   */
  @Override
  public void write(final ProgressionInterface _p) throws IOException, WriteException {
    WritableWorkbook classeur = null;
    try {
      classeur = Workbook.createWorkbook(dest_);
      // le max de colonne autorisee
      final int max = 256;
      //
      final ProgressionUpdater up = new ProgressionUpdater(_p);
      int col = 0;
      int sheet = 0;
      up.setValue(3, table_.getMaxCol(), 0, 60);
      WritableSheet currentSheet = classeur.createSheet(CtuluResource.CTULU.getString("Feuille {0}", CtuluLibString
          .getString(sheet + 1)), sheet);

      for (int i = 0; i < table_.getMaxCol(); i++) {
        currentSheet.addCell(new Label(col, 0, table_.getColumnName(i)));
        for (int row = 0; row < table_.getMaxRow(); row++) {
          final WritableCell cell = table_.getExcelWritable(row, i, row + 1, col);
          if (cell != null) {
            currentSheet.addCell(cell);
          }
        }
        col++;
        if (col > max) {
          sheet++;
          currentSheet = classeur.createSheet(CtuluResource.CTULU.getString("Feuille {0}", CtuluLibString
              .getString(sheet + 1)), sheet);
          col = 0;
        }
        up.majAvancement();
      }
      classeur.write();
      if (_p != null) {
        _p.setProgression(80);
      }
    } finally {
      if (classeur != null) {
        classeur.close();
      }
    }

  }
  
  BuFileFilter ftXsl_ = new BuFileFilter(new String[]{"xls"}, CtuluResource.CTULU.getString("Fichier Excel 97-2003"));

  @Override
  public BuFileFilter getFilter() {
    return ftXsl_;
  }

  @Override
  public void setFile(File f) {
    dest_ = f;
  }

  @Override
  public void setModel(CtuluTableModelInterface _model) {
    table_ = _model;
  }
  
  String extension_ = ".xls";
  @Override
  public String getMostPopularExtension() {
    return extension_;
  }

  @Override
  public boolean allowFormatData() {
    return true;
  }

}