/**
 * @creation 10 f�vr. 2005
 * @modification $Date: 2007-03-30 15:35:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.table;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import org.fudaa.ctulu.CsvWriter;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTableCsvWriter.java,v 1.5 2007-03-30 15:35:20 deniger Exp $
 */
public class CtuluTableCsvWriter implements CtuluWriter {

  CtuluTableModelInterface table_;
  CsvWriter w_;
  boolean writeHeader_ = true;
  BuFileFilter ftCsv_ = new BuFileFilter(new String[]{"csv", "txt"}, CtuluResource.CTULU.getString("Texte CSV"));
  DecimalFormat doubleFmt_;

  /**
   * @param _w
   * @param _table
   */
  public CtuluTableCsvWriter(final CsvWriter _w, final CtuluTableModelInterface _table) {
    this(_w, _table, '\t');
  }

  public CtuluTableCsvWriter(final CsvWriter _w, final CtuluTableModelInterface _table, char sepChar) {
    this(_w, _table, sepChar, null);
  }

  public CtuluTableCsvWriter(final CsvWriter _w, final CtuluTableModelInterface _table, char sepChar, DecimalFormat _doubleFmt) {
    super();
    w_ = _w;
    w_.setSepChar(sepChar);
    table_ = _table;
    doubleFmt_ = _doubleFmt;
  }

  public CtuluTableCsvWriter() {
    super();
  }

  @Override
  public void write(final ProgressionInterface _prog) throws IOException {
    try {
      final ProgressionUpdater updater = new ProgressionUpdater(_prog);
      int maxCol = table_.getMaxCol();
      int maxRow = table_.getMaxRow();
      if (maxCol == 0 || maxRow == 0) {
        return;
      }
      updater.setValue(4, maxRow);
      updater.majProgessionStateOnly();
      List<String> comments = table_.getComments();
      if (CtuluLibArray.isNotEmpty(comments)) {
        for (String comment : comments) {
          w_.appendString("# " + comment);
          w_.newLine();
        }
      }

      if (writeHeader_) {
        try {
          w_.appendString("#\"" + table_.getColumnName(0) + '\"');
          for (int i = 1; i < maxCol; i++) {
            w_.appendString('\"' + table_.getColumnName(i) + '\"');
          }
        } catch (java.nio.charset.UnmappableCharacterException _evt) {
        }
        w_.newLine();
      }
      for (int i = 0; i < maxRow; i++) {
        for (int col = 0; col < maxCol; col++) {
          final Object o = table_.getValue(i, col);
          if (doubleFmt_ != null && (o instanceof Double)) {
            w_.appendString(doubleFmt_.format((Double) o));
          } else {
            String string = o == null ? CtuluLibString.EMPTY_STRING : o.toString();
            if (string.indexOf(' ') > 0 || string.indexOf(w_.getSepChar()) > 0) {
              string = "\"" + string + "\"";
            }
            w_.appendString(string);
          }
        }
        w_.newLine();
        updater.majAvancement();
      }
    } finally {
      w_.close();
    }
  }

  public final boolean isWriteHeader() {
    return writeHeader_;
  }

  public final void setWriteHeader(final boolean _writeHeader) {
    writeHeader_ = _writeHeader;
  }

  @Override
  public BuFileFilter getFilter() {
    return ftCsv_;
  }

  @Override
  public void setFile(File _f) {
    CsvWriter c;
    try {
      c = new CsvWriter(_f);
      w_ = c;
      w_.setSepChar(';');
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  @Override
  public void setModel(CtuluTableModelInterface _model) {
    table_ = _model;
  }
  String extension_ = ".csv";

  @Override
  public String getMostPopularExtension() {
    return extension_;
  }

  @Override
  public boolean allowFormatData() {
    return true;
  }
}