/**
 * @creation 10 f�vr. 2005
 * @modification $Date: 2007-04-16 16:33:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.table;

import java.util.List;
import jxl.write.WritableCell;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTableModelInterface.java,v 1.2 2007-04-16 16:33:53 deniger Exp $
 */
public interface CtuluTableModelInterface {
  
  /**
   * property used in JComponent to give comments
   */
  public static final String EXPORT_COMMENT_PROPERTY = "EXPORT_COMMENT";

  /**
   * 
   * @return liste de commentaire � ajouter en d�but de fichier.
   */
  List<String> getComments();

  /**
   * @return le nombre max de colonnes
   */
  int getMaxCol();

  /**
   * @return le nombre max de ligne
   */
  int getMaxRow();

  /**
   * @return le nom de de la colonne
   */
  String getColumnName(int _i);

  /**
   * @param _row la ligne
   * @param _col la colonne
   * @return l'objet qui va bien
   */
  Object getValue(int _row, int _col);

  /**
   * @param _rowInModel la ligne dans ce model
   * @param _colInModel la colonne dans ce model
   * @param _rowInXls la ligne dans le fichier xls. A utiliser pour cr�er le WritableCell
   * @param _colInXls la colonne dans le fichier xls. A utiliser pour cr�er le WritableCell
   * @return l'objet permettant d'ecrire la cellule. Si null ne fait rien.
   */
  WritableCell getExcelWritable(int _rowInModel, int _colInModel, int _rowInXls, int _colInXls);
  
  public int[] getSelectedRows();
}
