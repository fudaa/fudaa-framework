/*
 GPL 2
 */
package org.fudaa.ctulu.table;

/**
 *
 * @author Frederic Deniger
 */
public interface CtuluTableModelExportDecorator {

  static String CLIENT_PROPERTY_ = "CtuluTableModelExportDecorator";

  boolean hasSpecificExportValue(int col);

  Object getValue(int row, int col);
}
