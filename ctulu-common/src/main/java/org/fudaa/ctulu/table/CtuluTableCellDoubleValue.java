/*
GPL 2
 */
package org.fudaa.ctulu.table;

/**
 *
 * @author Frederic Deniger
 */
public class CtuluTableCellDoubleValue implements Comparable<CtuluTableCellDoubleValue> {

  private final String formattedValue;
  private final double value;

  public CtuluTableCellDoubleValue(String formattedValue, double value) {
    this.formattedValue = formattedValue == null ? Double.toString(value) : formattedValue;
    this.value = value;
  }

  public String getFormattedValue() {
    return formattedValue;
  }

  public double getValue() {
    return value;
  }

  @Override
  public String toString() {
    return formattedValue;
  }


  @Override
  public int hashCode() {
    int hash = 3;
    hash = 37 * hash + (int) (Double.doubleToLongBits(this.value) ^ (Double.doubleToLongBits(this.value) >>> 32));
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CtuluTableCellDoubleValue other = (CtuluTableCellDoubleValue) obj;
    if (Double.doubleToLongBits(this.value) != Double.doubleToLongBits(other.value)) {
      return false;
    }
    return true;
  }

  @Override
  public int compareTo(CtuluTableCellDoubleValue o) {
    if (o == null) {
      return 1;
    }
    if (o == this) {
      return 0;
    }
    return value > o.value ? 1 : (value < o.value ? -1 : 0);
  }

}
