/*
 * @modification $Date: 2006-09-19 14:36:55 $
 * @statut       unstable
 * @file         BuTableColumnHeader.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package org.fudaa.ctulu.table;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuPopupMenu;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTableSortModel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.BitSet;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTableColumnHeader.java,v 1.3 2006-09-19 14:36:55 deniger Exp $
 */
public class CtuluTableColumnHeader extends JTableHeader {

  private static final int[] VTAX = new int[] { 3, 0, 6};
  private static final int[] VTAY = new int[] { 0, 6, 6};
  private static final int[] VBAX = new int[] { 3, 6, 0};
  private static final int[] VBAY = new int[] { 6, 0, 0};

  private ShowHideColumn     showHideAction_;

  ActionListener getShowHideActionListener(){
    if (showHideAction_ == null) {
      showHideAction_ = new ShowHideColumn();
    }
    return showHideAction_;

  }

  class ShowHideColumn implements ActionListener {

    @Override
    public void actionPerformed(final ActionEvent _evt){
      final String com = _evt.getActionCommand();
      if (com.startsWith("COLONNE_")) {
        final int i = Integer.parseInt(_evt.getActionCommand().substring(8));
        hideColumn(getTable().getColumnModel().getColumn(i));
      }
      else {
        final int i = Integer.parseInt(_evt.getActionCommand().substring(5));
        showColumn(i);
      }
    }
  }

  public CtuluTableColumnHeader(final TableColumnModel _model) {
    super(_model);

    addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(final MouseEvent _evt){
        if (BuLib.isMiddle(_evt)) {
          if (_evt.getClickCount() != 1) {
            return;
          }

          final JTable t = getTable();
          final int c = t.columnAtPoint(new Point(_evt.getX(), _evt.getY()));
          if (c == -1) {
            return;
          }

          final TableColumn column = _model.getColumn(c);

          int ps = column.getPreferredWidth();
          if (column.getWidth() == ps) {
            ps = computePreferredWidth(t, column, c);
          }
          column.setPreferredWidth(ps);
        }
        else if (BuLib.isRight(_evt)) {
          final JTable t = getTable();
          final int c = t.columnAtPoint(new Point(_evt.getX(), _evt.getY()));
          final TableModel model = t.getModel();
          final TableColumnModel colModel = t.getColumnModel();
          final BitSet set = new BitSet(model.getColumnCount());
          final BuPopupMenu pm = new BuPopupMenu("Columns");
          boolean first = false;
          if (c != -1) {
            pm.addMenuItem("Cacher", "COLONNE_" + c, BuResource.BU.getMenuIcon("cacher"), true);
            first = true;
          }
          for (int i = 0; i < colModel.getColumnCount(); i++) {
            final TableColumn column = colModel.getColumn(i);
            final String n = model.getColumnName(column.getModelIndex());
            if (first) {
              pm.addSeparator();
            }
            first = false;
            pm.addCheckBox(n, "COLONNE_" + i, null, true, true);
            set.set(column.getModelIndex());
          }
          for (int i = 0; i < model.getColumnCount(); i++) {
            if (!set.get(i)) {
              pm.addCheckBox(model.getColumnName(i), "SHOW_" + i, null, true, false);
            }
          }

          pm.addActionListener(getShowHideActionListener());

          final int w = pm.getPreferredSize().width;
          int x = _evt.getX() - w / 2;
          final int y = CtuluTableColumnHeader.this.getHeight() - 1;
          if (c != -1) {
            final Rectangle hr = getHeaderRect(c);
            if (x + w > hr.x + hr.width) {
              x = hr.x + hr.width - w;
            }
            if (x < hr.x) {
              x = hr.x;
            }
          }
          pm.show(CtuluTableColumnHeader.this, x, y);
        }
      }
    });
  }

  public void hideColumn(final TableColumn _column){
    getColumnModel().removeColumn(_column);
  }

  public void showColumn(final int _modelIndex){
    final TableColumn col = new TableColumn(_modelIndex);
    col.setIdentifier(getTable().getModel().getColumnName(_modelIndex));
    col.setHeaderValue(col.getIdentifier());
    getColumnModel().addColumn(col);
    adjustWidth(col);
  }

  public void adjustWidth(final TableColumn _column){
    int w = _column.getWidth();
    if (w != 0) {
      final JTable t = getTable();
      final int i = t.convertColumnIndexToView(_column.getModelIndex());
      w = computePreferredWidth(getTable(), _column, i);
      _column.setMaxWidth(Integer.MAX_VALUE);
      _column.setPreferredWidth(w);
      _column.setWidth(w);
    }
  }

  public int computePreferredWidth(final int _indexInView){
    final  JTable t = getTable();
    final TableColumn c = t.getColumn(t.getColumnName(_indexInView));
    return computePreferredWidth(t, c, _indexInView);
  }

  int computePreferredWidth(final JTable _t,final TableColumn _col,final int _civ){
    int r = 0;

    TableCellRenderer thr = _col.getHeaderRenderer();
    /*GCJ-BEGIN*/
    if (thr == null) {
      thr = getDefaultRenderer();
    }
    /*GCJ-END*/
    if (thr != null) {
      r = thr.getTableCellRendererComponent(_t, _col.getHeaderValue(), false, false, -1, _civ).getPreferredSize().width;
    }

    if (_t.getModel() instanceof BuTableSortModel) {
      r += 18;
    }

    TableCellRenderer tcr = _col.getCellRenderer();
    if (tcr == null) {
      tcr = _t.getDefaultRenderer(null);
    }
    if (tcr != null) {
      for (int i = 0; i < _t.getRowCount(); i++){
        r = Math.max(r, tcr.getTableCellRendererComponent(_t, _t.getValueAt(i, _civ), false, false, i, _civ)
          .getPreferredSize().width);
      }
    }

    r += 6;
    return r;
  }

  @Override
  public void paintComponent(final Graphics _g){
    BuLib.setAntialiasing(this, _g);
    super.paintComponent(_g);

    final JTable t = getTable();
    if ((t != null) && (t.getModel() instanceof BuTableSortModel)) {
      final BuTableSortModel m = (BuTableSortModel) t.getModel();
      final Color old = _g.getColor();
      final int[] scs = m.getSortingColumns();
      if (scs.length > 0) {
        for (int j = 0; j < scs.length; j++) {
          final int cim = scs[j];
          if (cim >= 0) {
            final int civ = t.convertColumnIndexToView(cim);
            if (civ >= 0) {
              final  Rectangle r = getHeaderRect(civ);
              _g.translate(r.x + r.width - 10, r.y + r.height / 2 - 3);
              BuLib.setColor(_g, Color.white);
              if (m.isAscending(cim)) {
                _g.fillPolygon(VBAX, VBAY, 3);
              } else {
                _g.fillPolygon(VTAX, VTAY, 3);
              }
              BuLib.setColor(_g, (j == 0) ? Color.black : Color.gray);
              if (m.isAscending(cim)) {
                _g.drawPolygon(VBAX, VBAY, 3);
              } else {
                _g.drawPolygon(VTAX, VTAY, 3);
              }
              _g.translate(-(r.x + r.width - 10), -(r.y + r.height / 2 - 3));
            }
          }
          _g.setColor(old);
        }
      }
    }
  }
}
