/*
 GPL 2
 */
package org.fudaa.ctulu.table;

import com.Ostermiller.util.CSVParser;
import com.memoire.fu.FuLog;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class CtuluExcelCsvFileReader {
  final File file;

  public CtuluExcelCsvFileReader(File file) {
    this.file = file;
  }

  public String[][] readFile() {
    boolean isXls = isExcel();
    if (isXls) {
      List<String[]> rows = new ArrayList<>();
      try (InputStream is = new FileInputStream(file)) {
        try (Workbook wb = WorkbookFactory.create(is)) {
          Sheet sheetAt = wb.getSheetAt(0);
          int maxCol = 0;
          for (Row row : sheetAt) {
            maxCol = Math.max(maxCol, row.getLastCellNum());
          }
          for (Row row : sheetAt) {
            List<String> cols = new ArrayList<>();
            for (int i = 0; i < maxCol; i++) {
              cols.add(getValue(row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK)));
            }
            rows.add(cols.toArray(new String[0]));
          }
        }
      } catch (Exception ex) {
        FuLog.error(ex);
      }
      return rows.toArray(new String[rows.size()][]);
    } else {
      return readCsv();
    }
  }

  public boolean isExcel() {
    final String nameToLowerCase = file.getName().toLowerCase();
    return nameToLowerCase.endsWith("xlsx") || nameToLowerCase.endsWith("xls");
  }

  protected String[][] readCsv() {
    String[][] values = null;
    try (Reader reader = new FileReader(file)) {
      values = new CSVParser(new BufferedReader(reader), ';').getAllValues();
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    return values;
  }

  protected String getValue(Cell cell) {
    switch (cell.getCellType()) {
      case BLANK:
        return StringUtils.EMPTY;
      case NUMERIC:
        return Double.toString(cell.getNumericCellValue());
      case STRING:
        return cell.getStringCellValue();
      default:
        try {
          return Double.toString(cell.getNumericCellValue());
        } catch (Exception ex) {
          Logger.getLogger(CtuluExcelCsvFileReader.class.getName()).log(Level.INFO, "getValue", ex);
          try {
            return cell.getStringCellValue();
          } catch (Exception ex1) {
            Logger.getLogger(CtuluExcelCsvFileReader.class.getName()).log(Level.INFO, "getValue", ex1);
          }
        }
    }
    return StringUtils.EMPTY;
  }
}
