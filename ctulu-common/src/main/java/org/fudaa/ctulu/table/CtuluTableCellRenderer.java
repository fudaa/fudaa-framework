/*
 *  @creation     16 ao�t 2005
 *  @modification $Date: 2006-07-13 13:34:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.table;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTableCellRenderer;
import java.awt.Component;
import java.text.NumberFormat;
import javax.swing.JTable;
/**
 * @author fred deniger
 * @version $Id: CtuluTableCellRenderer.java,v 1.2 2006-07-13 13:34:37 deniger Exp $
 */
public class CtuluTableCellRenderer extends BuTableCellRenderer {

  private NumberFormat integerFormat_;

  public NumberFormat getIntegerFormat(){
    return integerFormat_;
  }

  @Override
  public Component getTableCellRendererComponent(final JTable _table,final Object _value,final boolean _selected,
    final  boolean _focus,final int _row,final int _column){
    final Component r = super.getTableCellRendererComponent(_table, _value, _selected, _focus, _row,
        _column);
    if ((integerFormat_ != null) && (_value instanceof Integer)) {
      ((BuLabel) r).setText(integerFormat_.format(((Integer) _value).doubleValue()));
    }
    return r;
  }

  public void setIntegerFormat(final NumberFormat _f){
    integerFormat_ = _f;
  }

}
