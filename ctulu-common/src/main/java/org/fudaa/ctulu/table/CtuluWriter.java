package org.fudaa.ctulu.table;

import com.memoire.bu.BuFileFilter;
import java.io.File;
import java.io.IOException;
import jxl.write.WriteException;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * interface qui gere l'export dans les formats donn�s
 * 
 * @author Adrien Hadoux
 * 
 */
public interface CtuluWriter {
  
  /**
   * indique le fichier ou ecrire les donn�es
   * 
   * @param f
   */
  public void setFile(File f);

  
  /**
   * g�n�re le fichier en �crivant le model de donn�es.
   * 
   * @param _p
   * @throws IOException
   * @throws WriteException
   */
  public void write(final ProgressionInterface _p) throws IOException, WriteException;
  
  /**
   * r�cup�re les filtres g�r�s par le writer
   * 
   * @return
   */
  public BuFileFilter getFilter();

  /**
   * ins�re le mod�le de donn�es.
   * 
   * @param model
   */
  public void setModel(CtuluTableModelInterface model);
  
  
  /**
   * donne l'extension de fichier la plus fr�quente.
   * 
   * @return
   */
  public String getMostPopularExtension();
  
  /**
   * indique si le wizard peut proposer des formattages de donnees.
   * 
   * @return
   */
  public boolean allowFormatData();
  
}
