package org.fudaa.ctulu.table;

import javax.swing.table.TableModel;

/**
 * Une interface pour une table acceptant le copy/paste.
 * @author Bertrand Marchand (marchand@detacad.fr)
 *
 */
public interface CtuluMutableTableModel extends TableModel {
  
  /**
   * @param _values Les valeurs a ajouter dans la ligne. Si values est vide, la ligne est ajout�e avec des valeurs par defaut,
   * en fin de tableau
   */
  public void addRow(Object... _values);
   
  /**
   * @param _values Les valeurs a ajouter dans la ligne. Si values est vide, la ligne est ajout�e avec des valeurs par defaut.
   * @param _i L'index ou on insert la ligne.
   */
  public void addRow(int _i, Object... values);
  
  /**
   * @param _idx Les index des lignes supprim�es.
   */
  public void removeRows(int... _idx);
}
