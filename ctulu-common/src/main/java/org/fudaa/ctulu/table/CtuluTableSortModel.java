/*
 *  @creation     11 juil. 2005
 *  @modification $Date: 2006-09-19 14:36:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.table;

import com.memoire.bu.BuTableSortModel;

/**
 * @author fred deniger
 * @version $Id: CtuluTableSortModel.java,v 1.4 2006-09-19 14:36:55 deniger Exp $
 */
public class CtuluTableSortModel extends BuTableSortModel {

  protected final int compareNumberRowsByColumn(final int _row1, final int _row2, final int _col) {
    final Object o1 = model_.getValueAt(_row1, _col);
    final Object o2 = model_.getValueAt(_row2, _col);

    if ((o1 == null) && (o2 == null)) {
      return 0;
    } else if (o1 == null) {
      return -1;
    } else if (o2 == null) {
      return 1;
    }
    Class clazz = model_.getColumnClass(_col);
    if (String.class.equals(clazz) || Number.class.isAssignableFrom(clazz)) {
      return o1.toString().compareTo(o2.toString());
    }
    final double d1 = ((Number) o1).doubleValue();
    final double d2 = ((Number) o2).doubleValue();

    if (d1 < d2) {
      return -1;
    } else if (d1 > d2) {
      return 1;
    } else {
      return 0;
    }
  }

  @Override
  public int compare(final int _row1, final int _row2) {
    compares++;

    final int nc = model_.getRowCount();

    for (int i = 0; i < nbcols_; i++) //for(int i=nbcols_-1;i>=0;i--)
    {
      final int c = columns_[i];
      if ((c < 0) || (c >= nc)) {
        continue;
      }

      int s = Number.class.isAssignableFrom(model_.getColumnClass(c)) ? compareNumberRowsByColumn(
              _row1, _row2, c) : compareRowsByColumn(_row1, _row2, c);
      if (descending_[i]) {
        s = -s;
      }
      if (s != 0) {
        return s;
      }
    }
    return 0;
  }
}
