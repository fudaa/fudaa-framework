/**
 * @creation 11 f�vr. 2005
 * @modification $Date: 2007-04-16 16:33:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.table;

import com.memoire.bu.BuFileFilter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.fudaa.ctulu.*;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Fred Deniger
 * @version $Id: CtuluTableExcelWriter.java,v 1.7 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluTableXlsxWriter implements CtuluWriter {
  CtuluTableModelInterface table_;
  File dest_;

  /**
   * @param _table la table a ecrire
   * @param _dest le fichier de destination
   */
  public CtuluTableXlsxWriter(final CtuluTableModelInterface _table, final File _dest) {
    super();
    table_ = _table;
    dest_ = _dest;
  }

  public CtuluTableXlsxWriter() {
    super();
  }

  /**
   * @param _p le receveur de l'avancement
   * @throws IllegalStateException si erreur d'ecriture du fichier.
   */
  @Override
  public void write(final ProgressionInterface _p) {
    Workbook wb = new XSSFWorkbook();
    try {
      // le max de ligne autorisee
      final int maxCol = 16384;
      final int maxLignes = 1048576;
      //
      final ProgressionUpdater up = new ProgressionUpdater(_p);
      int sheet = 0;
      up.setValue(3, table_.getMaxCol(), 0, 60);

      if (wb.getNumberOfSheets() > 0) {
        int nb = wb.getNumberOfSheets();
        for (int i = 0; i < nb; i++) {
          wb.removeSheetAt(0);
        }
      }
      Sheet currentSheet = wb.createSheet(CtuluResource.CTULU.getString("Export {0}", CtuluLibString
          .getString(++sheet)));
      Row row = currentSheet.createRow(0);
      CtuluDoubleParser doubleParser = new CtuluDoubleParser();
      for (int i = 0; i < table_.getMaxCol(); i++) {
        row.createCell(i).setCellValue(table_.getColumnName(i));
      }
      for (int rowIdx = 0; rowIdx < table_.getMaxRow(); rowIdx++) {
        row = currentSheet.createRow(rowIdx + 1);
        for (int i = 0; i < table_.getMaxCol(); i++) {
          Cell createCell = row.createCell(i);
          Object value = table_.getValue(rowIdx, i);
          if (value != null) {
            Class dataClass = value.getClass();
            if (String.class.equals(dataClass) && doubleParser.isValid((String) value)) {
              createCell.setCellValue(doubleParser.parse((String) value));
            } else if (Number.class.isAssignableFrom(dataClass)) {
              createCell.setCellValue(((Number) value).doubleValue());
            } else if (Date.class.equals(dataClass)) {
              createCell.setCellValue(((Date) value));
            } else if (CtuluTableCellDoubleValue.class.equals(dataClass)) {
              createCell.setCellValue(((CtuluTableCellDoubleValue) value).getValue());
            } else {
              createCell.setCellValue(value.toString());
            }
          }
        }
        up.majAvancement();
      }
      if (_p != null) {
        _p.setProgression(80);
      }
      boolean ok = writeToFile(wb);
      if (!ok) {
        throw new IllegalStateException(CtuluResource.CTULU.getString(
            "Impossible d'�crire dans le fichier '{0}'.\nIl est probablement ouvert par un autre processus (Excel).", dest_.
                getName()));
      }
    }
    catch(Exception exception){
      Logger.getLogger(getClass().getName()).log(Level.SEVERE, "xslx export", exception);
      throw exception;
    } finally {
    }
  }

  protected boolean writeToFile(Workbook wb) {
    FileOutputStream fileOut = null;
    try {
      fileOut = new FileOutputStream(dest_);
      wb.write(fileOut);
      fileOut.close();
    } catch (Exception ex) {
      return false;
    } finally {
      CtuluLibFile.close(fileOut);
    }
    return true;
  }

  BuFileFilter ftXsl_ = new BuFileFilter(new String[]{"xlsx"}, CtuluResource.CTULU.getString("Fichier Excel"));

  @Override
  public BuFileFilter getFilter() {
    return ftXsl_;
  }

  @Override
  public void setFile(File f) {
    dest_ = f;
  }

  @Override
  public void setModel(CtuluTableModelInterface _model) {
    table_ = _model;
  }

  String extension_ = ".xlsx";

  @Override
  public String getMostPopularExtension() {
    return extension_;
  }

  @Override
  public boolean allowFormatData() {
    return true;
  }
}
