/*
 *  @creation     9 janv. 2006
 *  @modification $Date: 2007-04-16 16:33:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLib;

/**
 * Une classe permettant de parser des doubles et de prendre en compte l'�criture avec des virgules.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluDoubleParser.java,v 1.10 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluDoubleParser {

  boolean isVirugule_;
  boolean emptyIsNull_;

  public final boolean isEmptyIsNull() {
    return emptyIsNull_;
  }

  public final void setEmptyIsNull(final boolean _emptyIsNull) {
    emptyIsNull_ = _emptyIsNull;
  }

  public double parse(final String _s) throws NumberFormatException {
    if (_s == null || _s.length() == 0 || _s.trim().length() == 0) {
      if (emptyIsNull_) {
        return 0;
      }
      throw new NumberFormatException("Emty string");
    }
    final String s = _s.trim();

    if (isVirugule_) {
      return Double.parseDouble(FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT));
    }
    try {
      return Double.parseDouble(s);
    } catch (final NumberFormatException e) {
      if (CtuluLibString.containsVirgule(s)) {
        isVirugule_ = true;
        return Double.parseDouble(FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT));
      }
      throw e;
    }

  }

  public static Double parseValue(final String _s) {
    if (_s == null || _s.length() == 0 || _s.trim().length() == 0) {
      return null;
    }
    try {
      return CtuluLib.getDouble(Double.parseDouble(_s));
    } catch (final NumberFormatException _e) {
      try {
        return CtuluLib.getDouble(Double.parseDouble(FuLib.replace(_s, CtuluLibString.VIR, CtuluLibString.DOT)));
      } catch (final NumberFormatException _evt) {

      }

    }
    return null;

  }

  public boolean isValid(final String _s) {
    if (_s == null || _s.length() == 0 || _s.trim().length() == 0) {
      return emptyIsNull_;
    }
    final String s = _s.trim();
    if (isVirugule_) {
      try {
        Double.parseDouble(FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT));
        return true;
      } catch (final NumberFormatException _e) {
        return false;
      }
    }
    try {
      Double.parseDouble(s);
      return true;
    } catch (final NumberFormatException e) {
      if (CtuluLibString.containsVirgule(s)) {
        try {
          Double.parseDouble(FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT));
          isVirugule_=true;
          return true;
        } catch (final NumberFormatException _e) {
          return false;
        }
      }
      return false;
    }

  }

}
