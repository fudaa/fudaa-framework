/*
 *  @creation     16 janv. 2006
 *  @modification $Date: 2007-05-21 10:28:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;


/**
 * @author Fred Deniger
 * @version $Id: CtuluNumberFormatI.java,v 1.3 2007-05-21 10:28:30 deniger Exp $
 */
public interface CtuluNumberFormatI {

  String format(double _d);

  CtuluNumberFormatI getCopy();

  String toLocalizedPattern();
  
  boolean isDecimal();

}
