package org.fudaa.ctulu;

/**
 * Une dur�e en secondes. Ne prend pas en charge les milisecondes.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class CtuluDuration {
  long duration = 0;
  
  
  public CtuluDuration() { }

  public CtuluDuration(long _duration) {
    duration = _duration;
  }
  
  public void setDuration(long _time) {
    duration = _time;
  }
  
  public long getDuration() {
    return duration;
  }
  
  /**
   * @param _s La dur�e sous la forme "[[[jj:]hh:]mm:]ss"
   * @return La dur�e, en secondes
   */
  public static CtuluDuration fromString(String _s) {
    if (_s.trim().length() == 0)
      return null;
    
    // Le cas ou la chaine se termine par : n'est pas trait� par split.
    if (_s.trim().endsWith(":"))
      return null;
    
    CtuluDuration time = new CtuluDuration();

    int jj = 0;
    int hh = 0;
    int mm = 0;
    int ss = 0;
    
    String[] fields  = _s.trim().split(":");
    try {

      if (fields.length > 0) {
        String sss = fields[fields.length - 1];
        ss = Integer.parseInt(sss);
      }
      if (fields.length > 1) {
        String smm = fields[fields.length - 2];
        mm = Integer.parseInt(smm);
      }
      if (fields.length > 2) {
        String shh = fields[fields.length - 3];
        hh = Integer.parseInt(shh);
      }
      if (fields.length > 3) {
        String sjj = fields[fields.length - 4];
        jj = Integer.parseInt(sjj);
      }
      if (fields.length > 4) {
        return null;
      }
      
      if (ss >= 60 || mm >= 60 || hh >= 24) {
        return null;
      }
    }
    catch (NumberFormatException _exc) {
      return null;
    }    
    
    time.duration = ss + mm * 60 + hh * 3600 + jj * 86400;
    return time;
  }
  
  /**
   * La dur�e est traduite en string "jj:hh:mm:ss". Si le nombre de jours est �gal � 0, jj n'est pas affich�.
   * @return La dur�e sous forme de string.
   */
  public String toString() {
    long rest = duration;
    
    int jj = (int)(rest / 86400);
    rest %= 86400;
    
    int hh = (int)(rest / 3600);
    rest %= 3600;
    
    int mm = (int)(rest / 60);
    rest %= 60;
    
    int ss = (int)rest;
    
    if (jj == 0) {
      return String.format("%02d:%02d:%02d", hh, mm, ss);
    }
    else {
      return String.format("%d:%02d:%02d:%02d", jj, hh, mm, ss);
    }
  }
}
