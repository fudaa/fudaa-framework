/*
 * @creation 27 sept. 2004
 * @modification $Date: 2007-06-05 09:47:57 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuValueValidator;

/**
 * Etend BuValueValidator pour tester des double directement.
 *
 * @author Fred Deniger
 * @version $Id: CtuluValueValidator.java,v 1.10 2007-06-05 09:47:57 clavreul Exp $
 */
public abstract class CtuluValueValidator extends BuValueValidator {
  
  public CtuluValueValidator(){
  }

  /**
   * Cette methode a ete ajoutee pour eviter de transformer des double en Double pour faire des tests basiques.
   *
   * @param _d le double a tester
   * @return true si valide
   */
  public abstract boolean isValueValid(double _var);

  @Override
  public boolean isValueValid(final Object _value) {
    return (_value instanceof Number) && isValueValid(((Number) _value).doubleValue());
  }

  /**
   * @return la description du validator
   */
  public abstract String getDescription();

  /**
   * Classe testant le double suivant un min,max.
   *
   * @author Fred Deniger
   * @version $Id: CtuluValueValidator.java,v 1.10 2007-06-05 09:47:57 clavreul Exp $
   */
  public final static class DoubleMinMax extends CtuluValueValidator {

    /**
     * La valeur min a respecter.
     */
    private final double min_;
    /**
     * La valeur max a respecter.
     */
    private final double max_;

    /**
     * @param _min le min a respecter
     * @param _max le max a respecter
     */
    public DoubleMinMax(final double _min, final double _max) {
      min_ = _min;
      max_ = _max;
    }

    @Override
    public boolean isValueValid(final double _value) {
      return (_value >= min_) && (_value <= max_);
    }

    @Override
    public String getDescription() {
      return CtuluResource.CTULU.getString("Les values doivent apartenir �") + CtuluLibString.ESPACE + min_ + " , "
          + max_;

    }
  }

  /**
   * @author fred deniger
   * @version $Id: CtuluValueValidator.java,v 1.10 2007-06-05 09:47:57 clavreul Exp $
   */
  public static class DoubleMin extends CtuluValueValidator {

    /**
     * La valeur min a respecter.
     */
    private final double min_;

    /**
     * @param _min le min a respecter.
     */
    public DoubleMin(final double _min) {
      min_ = _min;
    }

    @Override
    public boolean isValueValid(final double _value) {
      return _value >= min_;
    }

    @Override
    public String getDescription() {
      return CtuluResource.CTULU.getString("Valeur minimale autoris�e:") + CtuluLibString.ESPACE + min_;

    }
  }
  public final static class IntMin extends DoubleMin {

    /**
     * @param _min le min a respecter.
     */
    public IntMin(final double _min) {
      super(_min);
    }

    @Override
    public boolean isValueValid(final Object _val) {
      return (_val instanceof Integer) && isValueValid(((Number) _val).intValue());
    }

  }

}