/**
 * @creation 12 mars 2003
 * @modification $Date: 2006-09-19 14:36:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

/**
 * @author deniger
 * @version $Id: CtuluIOOperationSynthese.java,v 1.7 2006-09-19 14:36:54 deniger Exp $
 */
public class CtuluIOResult<T> {

  private CtuluLog log;
  private String exClose_;
  private T source_;

  public CtuluIOResult() {
  }

  public CtuluIOResult(CtuluLog log) {
    this.log = log;
  }

  public CtuluIOResult(CtuluLog log, T source_) {
    this.log = log;
    this.source_ = source_;
  }

  /**
   * @return true si exception lors de la fermeture des flux.
   */
  public boolean containsClosingError() {
    return exClose_ != null;
  }

  /**
   * @return true si une erreur fatale est survenue
   */
  public boolean containsFatalError() {
    return (log != null) && (log.containsSevereError());
  }

  /**
   * @return true si message
   */
  public boolean containsMessages() {
    return (log != null) && (!log.isEmpty());
  }

  /**
   * @return l'analyse de l'operation
   */
  public CtuluLog getAnalyze() {
    return log;
  }

  /**
   * @return le message d'erreur issu de la fermeture du(des) flux.
   */
  public String getClosingException() {
    return exClose_;
  }

  /**
   * @return la source utilise pour l'ecriture/recuperee par la lecture
   */
  public T getSource() {
    return source_;
  }

  /**
   * Ecrit sur la sortie standard les messages.
   */
  public void printAnalyze() {
    if (log != null) {
      log.printResume();
    }
  }

  /**
   * @param _editor l'analyse de l'operation
   */
  public void setAnalyze(final CtuluLog _editor) {
    log = _editor;
  }

  /**
   * @param _exception le message d'exception obtenu lors de la fermeture du/des flux.
   */
  public void setClosingOperation(final String _exception) {
    exClose_ = _exception;
  }

  /**
   * @param _object La nouvelle source (resultat de la lecture ou source pour l'ecriture)
   */
  public void setSource(final T _object) {
    source_ = _object;
  }
}