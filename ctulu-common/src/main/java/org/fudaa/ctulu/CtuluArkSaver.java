/*
 * @creation 30 ao�t 06
 * @modification $Date: 2008-01-22 09:55:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.db4o.ObjectContainer;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Une interface pour les classes permettant de sauvegarder des donn�es dans une archive. Base sur le mode de
 * fonctionnement des Zip, tar Pour ajouter une entre dans le saver, il faut faire <code>
 * CtuluArkSaver.startEntry("fichier.txt");
 * CtuluArkSaver.getOutStream() et on ecrit ce qu'il faut
 * CtuluArkSaver.startEntry("fichier2.txt");
 * CtuluArkSaver.getOutStream() et on ecrit ce qu'il faut
 * CtuluArkSaver.close();
 * </code>
 * On peut cr�er des sous-repertoires dans lesquelles un indice sera cr�� si l'utilisateur le demande.
 * 
 * @author fred deniger
 * @version $Id: CtuluArkSaver.java,v 1.2.6.2 2008-01-22 09:55:45 bmarchan Exp $
 */
public interface CtuluArkSaver {

  /**
   * L'extension a utiliser pour les entree de description.
   */
  String DESC_EXT = ".desc.xml";

  /**
   * Retourne le flux de sortie permettant d'ecrire les donn�es.
   * @return Le flux.
   */
  OutputStream getOutStream();

  /**
   * Stoppe l'entree en cours et commence une nouvelle entree avec le nom indiqu�.
   * 
   * @param _entryName le nom de l'entree
   */
  void startEntry(String _entryName) throws IOException;

  /**
   * Cree une "indicage" pour le dossier _dirName.
   * 
   * @param _dirName le nom du dossier de base
   * @param _nbEntry le nombre d'entree attendu: permet de creer un indice adequate
   */
  void createDir(String _dirName, int _nbEntry);

  /**
   * Controle que le dossier _dirName a ete cree.
   * @param _dirName L nom du dossier de base.
   * @return true : Le dossier existe.
   */
  boolean isDirCreated(String _dirName);

  /**
   * Retourne l'identifiant pour la prochaine entr�e sous la forme "0xx-". Cet identifiant
   * est incr�ment� de 1 � chaque appel, et est associ� au nom de l'entry cr��e.
   * @param _dir Le repertoire parent de l'entry.
   * @return null si l'entree _dir n'a pas �t� cr��e
   */
  String getNextIdForDir(String _dir);

  /**
   * Retourne le repertoire dans lequel le fichier de sauvegarde sera ecrit.
   * @return Le repertoire.
   */
  File getDestDir();

  /**
   * @return la base de donn�es utilise pour sauver les props graphiques
   */
  ObjectContainer getDb()  throws IOException;

}
