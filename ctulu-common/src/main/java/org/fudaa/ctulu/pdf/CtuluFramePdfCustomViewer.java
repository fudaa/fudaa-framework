package org.fudaa.ctulu.pdf;

import com.memoire.bu.BuPanel;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;

/**
 * Fenetre viewer pdf custom. Utilise un CtuluPanelPdfViewer
 * 
 * @author Adrien Hadoux
 * 
 */
public class CtuluFramePdfCustomViewer extends JFrame{
	
	CtuluPanelPdfViewer pdfViewer;
	CtuluUI ui_;
	BuPanel outlinePanel;
	BuPanel thumbscrollContainer;
	public CtuluFramePdfCustomViewer(String title,boolean useThumbs,CtuluUI ui){
		this(title,useThumbs,null,ui);
		
	}

	public CtuluFramePdfCustomViewer(String title,boolean useThumbs, File pdfFile,CtuluUI ui){
		super(title) ;
		ui_=ui;
		init(useThumbs,pdfFile);
		
	}
	
	
	/**
	 * initialisation du lecteur de pdf.
	 * @param useThumbs
	 * @param initFile
	 */
	private void init(boolean useThumbs,File initFile){
		this.setLayout(new BorderLayout());
		outlinePanel=new BuPanel();
		outlinePanel.setBorder(BorderFactory.createTitledBorder("Pages"));
		thumbscrollContainer=new BuPanel();
		thumbscrollContainer.setBorder(BorderFactory.createTitledBorder(CtuluResource.CTULU.getString("Chapitres")));
		
		pdfViewer=new CtuluPanelPdfViewer(useThumbs,ui_,outlinePanel,thumbscrollContainer);
		
		 addWindowListener(new WindowAdapter() {
      @Override
	            public void windowClosing(WindowEvent evt) {
	            	pdfViewer.doQuit();
	            }
	        });

		 //-- menus --//
		 
	        setJMenuBar(pdfViewer.getMenuBar());
	        
		 
		 
		
		this.add(pdfViewer,BorderLayout.CENTER);
		
		this.add(outlinePanel,BorderLayout.EAST);
		this.add(thumbscrollContainer,BorderLayout.WEST);
		
		
		setSize(650,650);
		outlinePanel.setSize(100,650);
		thumbscrollContainer.setSize(100,650);
		if(initFile!=null){
			//-- tentative de transformation en pdf --//
			try {
				pdfViewer.openFile(initFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ui_.error(CtuluResource.CTULU.getString("Erreur fichier inexistant"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ui_.error(CtuluResource.CTULU.getString("Erreur lors de la lecture du fichier"));
			}
		}
		
	}
}
