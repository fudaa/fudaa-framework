package org.fudaa.ctulu.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.print.StreamPrintService;
import javax.print.StreamPrintServiceFactory;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.swing.JTable;

import org.fudaa.ctulu.CtuluResource;


/**
 * Classe qui g�re l'exportation pdf et ps.
 * utilise la librairie iText.
 *
 * @author Adrien Hadoux
 * <p>
 * FIXME : L'impression ps ne semble pas fonctionner (test� sous WinXP, jdk 1.7).
 */
public class CtuluPdfPsExport {

  public static String FORMATPdf = "pdf";
  public static String FORMATPS = "ps";

  /**
   * Format A4 portait
   */
  public static Rectangle FORMAT_A4_PORTRAIT = PageSize.A4.rotate();
  public static Rectangle FORMAT_A4 = PageSize.A4;


  public static void write(final RenderedImage _image, final File _targetFile, final String _format) throws DocumentException, IOException, Exception {

    if (isApdfFormat(_format))
      writePDF(_image, _targetFile, _format);
    else if (isApsFormat(_format))
      writePostScript(_image, _targetFile, _format);

  }

  /**
   * Un lecteur pdf pour ecrire les images
   *
   * @author Adrien Hadoux
   */
  public static void writePDF(final RenderedImage _image, final File _targetFile, final String _format) throws DocumentException, IOException {
    int w = _image.getWidth();
    int h = _image.getHeight();
    Document document = new Document(new Rectangle(w, h));
    //-- ajout des infos relatives au document --//
    document.addTitle(CtuluResource.CTULU.getString("Exportation PDF"));
    document.addAuthor("FUDAA generating document");
    try {
      PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(_targetFile));
      document.open();
      PdfContentByte cb = writer.getDirectContent();
      PdfTemplate tp = cb.createTemplate(w, h);
      Graphics2D graphics = tp.createGraphics(w, h);
      //-- dessin de l'image --//
      graphics.drawImage((BufferedImage) _image, 0, 0, null);
      graphics.dispose();
      cb.addTemplate(tp, 0, 0);
      //TODO ADRIEN cela ne sert a rien de faire des catch pour faire seulement des
      //throw a l'interieur. Eventuellement, log avec FuLog.
    } catch (DocumentException ex) {
      throw ex;
    } catch (IOException ex) {
      throw ex;
    } finally {
      document.close();
    }
  }


  public static class PrintableObject implements Printable {
    BufferedImage image_;

    //TODO ADRIEN remplacer img par _img
    public PrintableObject(BufferedImage img) {
      image_ = img;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
        throws PrinterException {
      //	graphics.drawImage(image_,0,0,image_.getWidth(),image_.getHeight(),null);

      graphics.drawImage(image_, 0, 0, (int) pageFormat.getWidth(), (int) pageFormat.getHeight(), null);
      return 0;
    }
  }

  /**
   * Un lecteur ps
   *
   * @author Adrien Hadoux
   */
  public static void writePostScript(final RenderedImage _image, final File _targetFile, final String _format) throws Exception {
    PrintableObject objectToPrint = new PrintableObject((BufferedImage) _image);
    PrinterJob job = PrinterJob.getPrinterJob();
    PageFormat format = new PageFormat();
    Paper paper = new Paper();
    paper.setSize(_image.getWidth(), _image.getHeight());
    paper.setImageableArea(0, 0, _image.getWidth(), _image.getHeight());
    format.setPaper(paper);
    job.setPrintable(objectToPrint, format);

    String psMimeType = "application/postscript";
    FileOutputStream outstream;
    StreamPrintService psPrinter = null;
    StreamPrintServiceFactory[] spsFactories =
        PrinterJob.lookupStreamPrintServices(psMimeType);
    if (spsFactories.length > 0) {
      try {
        outstream = new FileOutputStream(_targetFile);
        psPrinter = spsFactories[0].getPrintService(outstream);
        // psPrinter can now be set as the service on a PrinterJob
        //TODO ADRIEN utilit� ?
      } catch (FileNotFoundException e) {
        throw e;
      }
    }
    job.setPrintService(psPrinter); // if app wants to specify this printer.
    PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
    aset.add(new Copies(1));
    job.print(aset);
  }


  /**
   * Test si le format est un format pdf ou ps
   *
   * @param format
   * @return
   */
  public static boolean isApdfPsFormat(String format) {
    return isApdfFormat(format) || isApsFormat(format);
  }

  public static boolean isApdfFormat(String format) {
    return format.toLowerCase().equals(FORMATPdf);
  }


  public static boolean isApsFormat(String format) {
    return format.toLowerCase().equals(FORMATPS);
  }

  /**
   * Methode qui ajoute le tableau de data dans le fichier pdf
   *
   * @param _data     tableau de donn�es
   * @param _title    titre du tableau
   * @param _document document pdf
   * @throws DocumentException
   * @author Adrien Hadoux
   */
  public static void addDataToDocPdf(Object[][] _data, String _title, Document _document) throws DocumentException {
    PdfPTable table = new PdfPTable(2);
    //-- titre --//
    PdfPCell cell = new PdfPCell(new Paragraph(_title));
    cell.setColspan(2);
    table.addCell(cell);
    //-- remplissage --//
    for (int i = 0; i < _data.length; i++)
      for (int j = 0; j < _data.length; i++)
        table.addCell(_data[i][j].toString());
    _document.add(table);
  }

  /**
   * Methode qui ajoute le tableau de data dans le fichier pdf
   *
   * @param _data1    donnees colonnes 1
   * @param _data2    donnees colonnes 2
   * @param _title    titre du tableau
   * @param _document document pdf
   * @throws DocumentException
   * @author Adrien Hadoux
   */
  public static void addDataToDocPdf(java.util.List _data1, java.util.List _data2, String _title, Document _document) throws DocumentException {
    //TODO ADRIEN loguer le truc non ?
    if (_data1.size() != _data2.size())
      return;

    PdfPTable table = new PdfPTable(2);
    //-- titre --//
    PdfPCell cell = new PdfPCell(new Paragraph(_title));
    cell.setColspan(2);
    table.addCell(cell);
    //-- remplissage --//
    for (int i = 0; i < _data1.size(); i++) {
      table.addCell(_data1.get(i).toString());
      table.addCell(_data2.get(i).toString());

    }
    _document.add(table);
  }

  /**
   * Methode qui ajoute le tableau de data dans le fichier pdf
   *
   * @param _liste    liste de listes
   * @param _title    titre du tableau
   * @param _document document pdf
   * @throws DocumentException
   * @author Adrien Hadoux
   */
  public static void addDataToDocPdf(java.util.List<java.util.List> _liste, String _title, Document _document) throws DocumentException {
    //TODO ADRIEN rien a dire la. Un conseil essaie d'utiliser la fonction Clean Up et Format d'eclipse qui te permette d'avoir un
    //code + propre. En l'etat, ce code est bon, je chipote...
    PdfPTable table = new PdfPTable(_liste.size());
    //-- titre --//
    PdfPCell cell = new PdfPCell(new Paragraph(_title));
    cell.setColspan(_liste.size());
    table.addCell(cell);
    //-- remplissage --//
    for (java.util.List sousListe : _liste)
      for (Object l : sousListe)
        table.addCell(l.toString());

    _document.add(table);
  }


  /**
   * Methode qui ajoute le tableau jtable dans le fichier pdf
   *
   * @author Adrien Hadoux
   */

  public static void addJTableToPdf(JTable _tableToadd, PdfWriter _writer) {
    PdfContentByte cb = _writer.getDirectContent();
    cb.saveState();
    Graphics2D g2 = cb.createGraphicsShapes(500, 500);

    cb.createGraphics(500, 500);
    Shape oldClip = g2.getClip();
    g2.clipRect(0, 0, 500, 500);
    _tableToadd.print(g2);
    g2.setClip(oldClip);
    g2.dispose();
    cb.restoreState();
  }


  /**
   * Methode qui ajoute une image  dans le fichier pdf
   *
   * @param BufferedImage image a ins�rer
   * @author Adrien Hadoux
   */
  public static void addImageToPdf(BufferedImage _img, PdfWriter _writer, int w, int h) {
    PdfContentByte cb = _writer.getDirectContent();
    PdfTemplate tp = cb.createTemplate(w, h);
    Graphics2D graphics = tp.createGraphics(w, h);
    //-- dessin de l'image --//
    graphics.drawImage(_img, 0, 0, null);
    graphics.dispose();
    cb.addTemplate(tp, 0, 0);

  }

}
