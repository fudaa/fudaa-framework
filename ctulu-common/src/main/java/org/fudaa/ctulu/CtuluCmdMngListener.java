/*
 *  @creation     21 oct. 2003
 *  @modification $Date: 2006-04-26 08:46:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;
/**
 * @author deniger
 * @version $Id: CtuluCmdMngListener.java,v 1.2 2006-04-26 08:46:05 deniger Exp $
 */
public interface CtuluCmdMngListener {
  /**
   * @param _source la source de l'evt
   */
  void undoredoStateChange(CtuluCommandManager _source);
}
