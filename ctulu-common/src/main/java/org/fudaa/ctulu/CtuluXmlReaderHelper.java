/*
 * @creation 1 sept. 06
 * @modification $Date: 2006-09-19 14:36:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author fred deniger
 * @version $Id: CtuluXmlReaderHelper.java,v 1.3 2006-09-19 14:36:53 deniger Exp $
 */
public class CtuluXmlReaderHelper extends DefaultHandler {

  Document doc_;

  /**
   * Methode permettant de cacher les exceptions.
   * 
   * @param _in le fichier a lire
   * @return le reader. null si erreur de lecture
   */
  public static CtuluXmlReaderHelper createReader(final InputStream _in) {
    try {
      return new CtuluXmlReaderHelper(_in);
    } catch (final SAXException _evt) {
      FuLog.error(_evt);

    } catch (final IOException _evt) {
      FuLog.error(_evt);

    } catch (final ParserConfigurationException _evt) {
      FuLog.error(_evt);
    }
    return null;

  }

  /**
   * Methode permettant de cacher les exceptions.
   * 
   * @param _in le flux a lire
   * @return le reader. null si erreur de lecture
   */
  public static CtuluXmlReaderHelper createReader(final File _in) {
    try {
      return createReader(new FileInputStream(_in));
    } catch (final FileNotFoundException _evt) {
      FuLog.error(_evt);
    }
    return null;

  }

  public CtuluXmlReaderHelper(final InputStream _in) throws SAXException, IOException, ParserConfigurationException {
    final DocumentBuilderFactory builder = createBuilder();
    doc_ = builder.newDocumentBuilder().parse(_in);
  }

  public CtuluXmlReaderHelper(final File _in) throws SAXException, IOException, ParserConfigurationException {
    final DocumentBuilderFactory builder = createBuilder();
    doc_ = builder.newDocumentBuilder().parse(_in);
  }

  private DocumentBuilderFactory createBuilder() throws FactoryConfigurationError {
    final DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
    builder.setValidating(false);
    builder.setNamespaceAware(false);
    return builder;
  }

  public String getTextFor(final String _tag) {
    final NodeList list = doc_.getElementsByTagName(_tag);
    if (list.getLength() == 1) {
      final Node n = list.item(0).getFirstChild();
      if (n != null) {
        return n.getNodeValue();
      }
    }
    return null;
  }

  public String getTrimTextFor(final String _tag) {
    final String res = getTextFor(_tag);
    return res == null ? null : res.trim();
  }

  public Document getDoc() {
    return doc_;
  }

}
