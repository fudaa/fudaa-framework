/**
 *  @creation     10 f�vr. 2005
 *  @modification $Date: 2007-06-14 11:58:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;




/**
 * Une interface permettant de reconnaitre les internal frames qui ont des donn�es � exporter.
 * @author Fred Deniger
 * @version $Id: CtuluExportDataInterface.java,v 1.6 2007-06-14 11:58:04 deniger Exp $
 */
public interface CtuluExportDataInterface {

  /**
   * La commande utilisee pour exporter les donn�es.
   */
  String EXPORT_CMD="EXPORT_DATA";

  /**
   * @return le titre du composant : le titre de l'internal frame par exemple.
   */
  String getTitle();

  /**
   * Active l'exportation du composant: ouvre une boite de dialogue.
   * @param _impl l'impl parent.
   */
  void startExport(CtuluUI _impl);
  


}
