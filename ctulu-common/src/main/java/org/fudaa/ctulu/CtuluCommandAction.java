/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.KeyStroke;

/**
 * @author deniger
 */
@SuppressWarnings("serial")
public abstract class CtuluCommandAction extends AbstractAction {

  /**
   * Action de undo
   * 
   * @author deniger
   */
  public static class Undo extends CtuluCommandAction {

    public Undo(CtuluCommandManager _mng) {
      super(CtuluLib.getS("D�faire"), BuResource.BU.getIcon("defaire"), _mng);
      putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl z"));
      updateState();
    }

    @Override
    protected final void updateState() {
      setEnabled(mng_.canUndo());
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      mng_.undo();

    }

  }

  /**
   * action de redo.
   * 
   * @author deniger
   */
  public static class Redo extends CtuluCommandAction {

    public Redo(CtuluCommandManager _mng) {
      super(CtuluLib.getS("Refaire"), BuResource.BU.getIcon("refaire"), _mng);
      putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl shift Z"));
      updateState();
    }

    @Override
    protected final void updateState() {
      setEnabled(mng_.canRedo());
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      mng_.redo();
    }

  }

  final CtuluCommandManager mng_;

  protected CtuluCommandAction(String _name, Icon _icon, CtuluCommandManager _mng) {
    super(_name, _icon);
    putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
    putValue(Action.ACTION_COMMAND_KEY, getValue(Action.NAME));
    mng_ = _mng;
    mng_.addListener(new CtuluCmdMngListener() {

      @Override
      public void undoredoStateChange(CtuluCommandManager _source) {
        updateState();
      }
    });
  }

  protected void updateState() {}

}
