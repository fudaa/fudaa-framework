package org.fudaa.ctulu;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class CtuluDefaultLogFormatter extends SimpleFormatter {

  /**
   * Formateur par defaut pour les logs
   */
  public final static CtuluDefaultLogFormatter DEFAULT = new CtuluDefaultLogFormatter();
  private boolean printLevel = true;

  public CtuluDefaultLogFormatter(boolean printLevel) {
    super();
    this.printLevel = printLevel;
  }

  public CtuluDefaultLogFormatter() {
    // EMPTY
  }

  public String formatLogs(CtuluLog log) {
    if (log.isEmpty()) {
      return CtuluLibString.EMPTY_STRING;
    }
    final StringBuilder res = new StringBuilder(40 + log.getRecords().size() * 40);
    res.append(CtuluLibString.LINE_SEP);
    if (log.getResource() != null) {
      res.append(log.getResource());
      res.append(CtuluLibString.LINE_SEP);
    }
    String desc = log.getDesci18n();
    if (desc != null) {
      res.append(desc);
      res.append(CtuluLibString.LINE_SEP);
    }
    for (final CtuluLogRecord logRecord : log.getRecords()) {
      if (res.length() > 0) {
        res.append(CtuluLibString.LINE_SEP);
      }
      if (log.getDefaultResourceBundle() == null) {
        res.append(DEFAULT.format(logRecord, log.getDefaultResourceBundle()));
      }
      res.append(DEFAULT.format(logRecord, log.getDefaultResourceBundle()));
    }
    return res.toString();
  }

  /**
   * @param logAnalyse l'analyse a formater
   * @return la string contenant le tout
   */
  public static String format(final CtuluAnalyze logAnalyse) {
    if (logAnalyse.logs == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final StringBuilder res = new StringBuilder(40 + logAnalyse.logs.size() * 40);
    res.append(CtuluLibString.LINE_SEP);
    if (logAnalyse.getResource() != null) {
      res.append(logAnalyse.getResource());
    }
    for (final LogRecord logRecord : logAnalyse.logs) {
      if (res.length() > 0) {
        res.append(CtuluLibString.LINE_SEP);
      }
      res.append(DEFAULT.format(logRecord));
    }
    return res.toString();
  }

  /**
   * @param logs l'analyse a formater
   * @return la string contenant le tout
   */
  public static String format(final Collection<LogRecord> logs) {
    if (logs == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final StringBuilder res = new StringBuilder(10 + logs.size() * 40);
    for (final LogRecord logRecord : logs) {
      res.append('\n').append(DEFAULT.format(logRecord));
    }
    return res.toString();
  }

  /**
   * @param logs l'analyse a formater
   * @return la string contenant le tout
   */
  public static String formatLogs(final Collection<CtuluLogRecord> logs, ResourceBundle resourceBundle) {
    if (logs == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final StringBuilder res = new StringBuilder(10 + logs.size() * 40);
    for (final CtuluLogRecord logRecord : logs) {
      res.append('\n').append(DEFAULT.format(logRecord, resourceBundle));
    }
    return res.toString();
  }

  public static String asHtml(final CtuluLog log) {
    return asHtml(log.getRecords(), log.getDefaultResourceBundle());

  }

  public static String asHtml(final Collection<CtuluLogRecord> logs, ResourceBundle resourceBundle) {
    if (logs == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final StringBuilder res = new StringBuilder(10 + logs.size() * 40);
    res.append("<html><body>");
    boolean first = true;
    for (final CtuluLogRecord logRecord : logs) {
      if (!first) {
        res.append("<br>");
      }
      first = false;
      res.append(DEFAULT.formatMessage(logRecord, resourceBundle));
    }
    return res.toString();
  }

  @Override
  public synchronized String format(final LogRecord record) {
    final StringBuilder sb = new StringBuilder(256);
    if (printLevel) {
      if (record.getLevel() == CtuluAnalyze.FATAL) {
        sb.append(CtuluResource.CTULU.getString("Erreur bloquante"));
      } else {
        sb.append(record.getLevel().getLocalizedName());
      }
      sb.append(": ");
    }
    sb.append(formatMessage(record));
    return sb.toString();
  }

  public synchronized String format(final CtuluLogRecord record, ResourceBundle resourceBundle) {
    final StringBuilder sb = new StringBuilder(256);
    if (printLevel) {
      sb.append(record.getLevel());
      sb.append(": ");
    }
    sb.append(formatMessage(record, resourceBundle));
    return sb.toString();
  }

  @Override
  public synchronized String formatMessage(final LogRecord record) {
    String format = record.getMessage();
    final java.util.ResourceBundle catalog = record.getResourceBundle();
    if (catalog == null) {
      return format;
    }
    if (catalog != null) {
      // // We cache catalog lookups. This is mostly to avoid the
      // // cost of exceptions for keys that are not in the catalog.
      // if (catalogCache == null) {
      // catalogCache = new HashMap();
      // }
      // format = (String)catalogCache.get(record.essage);
      // if (format == null) {
      try {
        format = catalog.getString(format);
      } catch (final java.util.MissingResourceException ex) {
        // Drop through. Use record message as format
        format = record.getMessage();
      }
      // catalogCache.put(record.message, format);
      // }
    }
    // Do the formatting.
    try {
      final Object parameters[] = record.getParameters();
      if (parameters == null || parameters.length == 0) {
        // No parameters. Just return format string.
        return format;
      }
      format = MessageFormat.format(format, parameters);
    } catch (final Exception ex) {
      ex.printStackTrace();
      // Formatting failed: use localized format string.

    }
    return format;
  }

  public synchronized String formatMessage(final CtuluLogRecord record, ResourceBundle catalog) {
    String format = record.getMsg();
    if (format == null) {
      return format;
    }
    if (catalog != null) {
      // // We cache catalog lookups. This is mostly to avoid the
      // // cost of exceptions for keys that are not in the catalog.
      // if (catalogCache == null) {
      // catalogCache = new HashMap();
      // }
      // format = (String)catalogCache.get(record.essage);
      // if (format == null) {
      try {
        format = catalog.getString(format);
      } catch (final java.util.MissingResourceException ex) {
        // Drop through. Use record message as format
        format = record.getMsg();
      }
      // catalogCache.put(record.message, format);
      // }
    }
    // Do the formatting.
    try {
      final Object parameters[] = record.getArgs();
      if (parameters == null || parameters.length == 0) {
        // No parameters. Just return format string.
        return format;
      }
      format = MessageFormat.format(format, parameters);
    } catch (final Exception ex) {
      ex.printStackTrace();
      // Formatting failed: use localized format string.

    }
    return format;
  }
}
