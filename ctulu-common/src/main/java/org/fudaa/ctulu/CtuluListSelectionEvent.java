/*
 *  @creation     6 janv. 2003
 *  @modification $Date: 2006-07-13 13:34:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu;
import java.util.EventObject;
/**
 *
 * @author deniger
 * @version $Id: CtuluListSelectionEvent.java,v 1.2 2006-07-13 13:34:37 deniger Exp $
 */
public class CtuluListSelectionEvent extends EventObject {
  /**
   * Constructor for EbliListeEvent.
   * @param source
   */
  public CtuluListSelectionEvent(final Object _source) {
    super(_source);
  }
}
