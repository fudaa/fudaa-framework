/*
 * @creation 28 mars 07
 * @modification $Date: 2007-05-21 10:28:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLog;
import java.text.DecimalFormat;

/**
 * @author fred deniger
 * @version $Id: CtuluNumberFormatBuilder.java,v 1.2 2007-05-21 10:28:30 deniger Exp $
 */
public final class CtuluNumberFormatBuilder {
  private CtuluNumberFormatBuilder() {}

  public static CtuluNumberFormatI restoreFromPattern(String _s) {
    if (_s == null) return new CtuluNumberFormatDefault(null);
    String prefixForFixed = getPrefixForFixed();
    if (_s.startsWith(prefixForFixed)) {
      int nb =8;
      try {
        nb = Integer.parseInt(_s.substring(prefixForFixed.length()));
      } catch (NumberFormatException _evt) {
        FuLog.error(_evt);

      }
      if (nb > 0) return new CtuluNumberFormatFixedFigure(nb);
    }
    CtuluNumberFormatI data = CtuluDurationFormatter.restoreFromLocalizedPattern(_s);
    // pas une date
    if (data == null) {
      DecimalFormat fmt = CtuluLib.getDecimalFormat();
      try {
        fmt.applyLocalizedPattern(_s);
      } catch (IllegalArgumentException _evt) {
        FuLog.error(_evt);
        
      }
      data = new CtuluNumberFormatDefault(fmt);
    }
    return data;
  }

  public static String getPrefixForFixed() {
    return "FIXED:";
  }

  public static String getPrefixForDate() {
    return "DATE:";
  }

}
