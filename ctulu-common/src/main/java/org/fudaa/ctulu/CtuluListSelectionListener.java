/*
 *  @creation     6 janv. 2003
 *  @modification $Date: 2006-04-05 10:00:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu;

import java.util.EventListener;

/**
 * @author deniger
 * @version $Id: CtuluListSelectionListener.java,v 1.1 2006-04-05 10:00:10 deniger Exp $
 */
public interface CtuluListSelectionListener extends EventListener {
  void listeSelectionChanged(CtuluListSelectionEvent _e);
}
