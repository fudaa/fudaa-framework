/*
 *  @creation     8 sept. 2005
 *  @modification $Date: 2006-02-09 08:59:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

/**
 * @author fred deniger
 * @version $Id: CtuluVariable.java,v 1.2 2006-02-09 08:59:29 deniger Exp $
 */
public interface CtuluVariable {

  /**
   * @return l'unit�
   */
  Object getCommonUnit();

  /**
   * @return un nom court servant d'identifiant
   */
  String getID();

  /**
   * @return le nom
   */
  String getName();

  /**
   * @return le nom long
   */
  String getLongName();
  
  /**
   * Retourne le nom formatt� avec l'unit� 
   * @param var La varaible
   * @return
   */
  default String getNameWithUnit() {
    Object unit = getCommonUnit();
    String name = getName();
    
    if (unit != null) {
      name += " (" + unit.toString() + ")";
    }
    return name;
  }

}
