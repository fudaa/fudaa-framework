/**
 *
 */
package org.fudaa.ctulu;

/**
 * Classe permettant de donner le resultat et le log d'une opération.
 *
 * @author deniger
 *
 */
public class CtuluResult<T> {

  T resultat;
  CtuluAnalyze analyze;

  public T getResultat() {
    return resultat;
  }

  public void setResultat(T resultat) {
    this.resultat = resultat;
  }

  public CtuluAnalyze getAnalyze() {
    return analyze;
  }

  public void setAnalyze(CtuluAnalyze analyze) {
    this.analyze = analyze;
  }
}
