/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ctulu;


import com.db4o.ObjectContainer;

/**
 * @author deniger
 *
 */
public interface CtuluSavable {

  public abstract void saveIn(ObjectContainer _db, ProgressionInterface _prog);

  public abstract void saveIn(CtuluArkSaver _writer, ProgressionInterface _prog);

}