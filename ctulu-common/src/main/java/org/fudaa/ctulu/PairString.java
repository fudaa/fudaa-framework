package org.fudaa.ctulu;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class PairString implements Comparable<PairString> {
  public String firstValue;
  public String secondValue;

  public PairString(String firstValue, String secondValue) {
    this.firstValue = firstValue;
    this.secondValue = secondValue;
  }

  @Override
  public int compareTo(PairString o) {
    int res = StringUtils.defaultString(firstValue).compareTo(StringUtils.defaultString(o.firstValue));
    if (res == 0) {
      res = StringUtils.defaultString(secondValue).compareTo(StringUtils.defaultString(o.secondValue));
    }
    return res;
  }

  public boolean isBlank() {
    return StringUtils.isBlank(firstValue) && StringUtils.isBlank(secondValue);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PairString that = (PairString) o;
    return Objects.equals(firstValue, that.firstValue) &&
        Objects.equals(secondValue, that.secondValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstValue, secondValue);
  }

  public String getFormattedString() {
    if (StringUtils.isNotBlank(secondValue)) {
      return StringUtils.defaultString(firstValue, "") + " - " + secondValue;
    }
    return StringUtils.defaultString(firstValue, "");
  }
}
