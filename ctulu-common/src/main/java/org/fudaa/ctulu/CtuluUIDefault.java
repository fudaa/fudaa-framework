/*
 *  @creation     13 juin 2003
 *  @modification $Date: 2007-01-17 10:45:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.awt.Component;

/**
 * Un ui par defaut.
 * 
 * @author fred deniger
 * @version $Id: CtuluUIDefault.java,v 1.12 2007-01-17 10:45:25 deniger Exp $
 */
public class CtuluUIDefault extends CtuluUIAbstract {

  public CtuluUIDefault() {}

  ProgressionInterface mainProgression_;

  private void err(final String _title, final String _msg) {
    System.err.println(_title + ": " + _msg);
  }

  private void out(final String _title, final String _msg) {
    System.out.println(_title + ": " + _msg);
  }

  @Override
  public void error(final String _titre, final String _msg, final boolean _tempo) {
    err(getErrorTitle(_titre), _msg);

  }

  @Override
  public void message(final String _titre, final String _msg, final boolean _tempo) {
    out(getInfoTitle(_titre), _msg);
  }

  @Override
  public void clearMainProgression() {}

  @Override
  public ProgressionInterface getMainProgression() {
    return getMainState();
  }

  public ProgressionInterface getMainState() {
    if (mainProgression_ == null) {
      mainProgression_ = new ProgressionFuAdapter("MAIN");
    }
    return mainProgression_;
  }

  @Override
  public CtuluTaskDelegate createTask(final String _name) {
    return new CtuluTaskOperationDefault(_name);
  }

  @Override
  public Component getParentComponent() {
    return null;
  }

  @Override
  public boolean question(final String _titre, final String _text) {
    out(getQuestionTitle(_titre), _text);
    return true;
  }

  @Override
  public void warn(final String _titre, final String _msg, final boolean _tempo) {
    err(getWarnTitle(_titre), _msg);
  }

}
