/*
 * @creation 7 ao�t 06
 * @modification $Date: 2006-12-20 16:05:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;



/**
 * @author fred deniger
 * @version $Id: RasterDataInterface.java,v 1.1 2006-12-20 16:05:19 deniger Exp $
 */
public interface RasterDataInterface {

  int getNbPt();

  int getPtImgX(int _i);

  /**
   * @param _i
   * @return l'ordonn�e sur l'image a partir du HAUT de l'image
   */
  int getPtImgY(int _i);

  double getPtX(int _i);

  double getPtY(int _i);

  Point2D.Double[] getImgPts();

  Point2D.Double[] getRealPts();

  CtuluImageContainer getImg();

  File getImgFile();

  /**
   * @param _force true si on doit relire le fichier image meme si l'image est d�j� non nulle.
   */
  void readImg(boolean _force) throws IOException;

  boolean isImgFound();

}
