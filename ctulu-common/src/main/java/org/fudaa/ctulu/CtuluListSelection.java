/*
 * @creation 2002-10-9
 * @modification $Date: 2008-05-13 12:10:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ctulu;

import gnu.trove.TIntArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.ListSelectionModel;

/**
 * Liste de selection encapsulant un BitSet. Des methodes sont rajoutees pour gerer les operations avec d'autres listes d'index ou des inversions de
 * selection dans un intervalle donne.
 *
 * @see java.util.BitSet utilisation de BitSet
 * @version $Id: CtuluListSelection.java,v 1.8.4.1 2008-05-13 12:10:48 bmarchan Exp $
 * @author Fred Deniger
 */
public class CtuluListSelection implements Cloneable, CtuluListSelectionInterface {

  public static int[] isSelectionContiguous(final CtuluListSelectionInterface _selection, final int _nbItem) {
    final int max = _selection.getMaxIndex();
    final int min = _selection.getMinIndex();
    final int[] rtab = new int[2];
    rtab[0] = min;
    rtab[1] = max;
    if (_selection.getNbSelectedIndex() == _nbItem) {
      return rtab;
    }
    int r = 0;
    for (int i = min; i <= max; i++) {
      if (_selection.isSelected(i)) {
        // first bloc
        if (r == 0) {
          r = 1;
          // second bloc verify if loop
        } else if (r == 2) {
          if ((_selection.isSelected(0)) && (_selection.isSelected(_nbItem - 1))) {
            r = 3;
            rtab[0] = i;
          } else {
            return null;
          }
        } else if (r == 4) {
          return null;
        }
      } else if ((r == 1) || (r == 3)) {
        if (r == 1) {
          rtab[1] = i - 1;
        }
        r++;
      }
    }
    return rtab;
  }
  /**
   * liste stockant les indexs.
   */
  private final FastBitSet index_;
  /**
   * les listeners de cette liste de selection.
   */
  private Set listeners_;
  CtuluListSelectionEvent evt_;
  private boolean maxComputed = false;
  private int max = -1;

  /**
   * initialise la liste de selection avec comme taille 32.
   */
  public CtuluListSelection() {
    this(new FastBitSet(32));
  }

  /**
   * initialise la liste avec
   * <code>_b</code>. Envoie une exception si
   * <code>_b</code> est nul.
   *
   * @param _b la liste des indexs
   */
  public CtuluListSelection(final FastBitSet _b) {
    if (_b == null) {
      throw new RuntimeException(getClass().getName() + " BitSet est null");
    }
    index_ = _b;
  }

  /**
   * initialise la liste avec
   * <code>_b</code>. Envoie une exception si
   * <code>_b</code> est nul.
   *
   * @param _b la liste des indexs
   */
  public CtuluListSelection(final int[] _b) {
    if (_b != null && _b.length > 0) {
      index_ = createForSelectedIdx(_b);

    } else {
      index_ = new FastBitSet(50);
    }
  }

  public static FastBitSet createForSelectedIdx(final int[] _b) {
    if (_b == null || _b.length == 0) {
      return null;
    }
    FastBitSet res = new FastBitSet(CtuluLibArray.getMax(_b) + 1);
    for (int i = _b.length - 1; i >= 0; i--) {
      if (_b[i] >= 0) {
        res.set(_b[i]);
      }
    }
    return res;
  }

  /**
   * Attention: le listener n'est pas copie.
   *
   * @param _l la liste utilisee pour initialise
   */
  public CtuluListSelection(final CtuluListSelection _l) {
    this(_l.length());
    setSelection(_l);
  }

  public CtuluListSelection(final CtuluListSelectionInterface _l) {
    this(_l == null ? 50 : _l.getMaxIndex());
    setSelection(_l);
  }

  /**
   * Initialise la liste de selection avec la taille
   * <code>_taille</code>.
   *
   * @param _taille la taille de la liste de selection. Si la taille est < 0, la taille par defaut est 0.
   */
  public CtuluListSelection(final int _taille) {
    this(new FastBitSet(Math.max(0, _taille)));
  }

  private boolean add(final int _i, final boolean _event) {
    if (!isSelected(_i)) {
      index_.set(_i);
      maxComputed = false;
      if (_event) {
        fireSelectionEvent();
      }
      return true;
    }
    return false;
  }

  private boolean addInterval(final int _deb, final int _fin, final boolean _event) {
    boolean r = false;
    for (int i = _deb; i <= _fin; i++) {
      r |= add(i, false);
    }
    maxComputed = false;
    if (r && _event) {
      fireSelectionEvent();
    }
    return r;
  }

  private boolean clear(final boolean _event) {
    if (isEmpty()) {
      return false;
    }
    removeInterval(0, getMaxIndex(), false);
    maxComputed = false;
    if (_event) {
      fireSelectionEvent();
    }
    return true;
  }

  private void fireSelectionEvent() {
    if (listeners_ != null) {
      if (evt_ == null) {
        evt_ = new CtuluListSelectionEvent(this);
      }
      for (final Iterator iter = listeners_.iterator(); iter.hasNext();) {
        ((CtuluListSelectionListener) iter.next()).listeSelectionChanged(evt_);
      }
    }
  }

  /**
   * Inverse la selection dans l'intervalle [0, _taille[.
   *
   * @param _taille
   */
  private void inverse(final int _taille, final boolean _event) {
    if (isEmpty()) {
      setSelectionInterval(0, _taille - 1, false);
      maxComputed = false;
      if (_event) {
        fireSelectionEvent();
      }
      return;
    }
    final int max = getMaxIndex();
    final int tailleIdx = _taille - 1;
    final int minIdxDone = max > tailleIdx ? tailleIdx : max;
    for (int i = 0; i <= minIdxDone; i++) {
      if (isSelected(i)) {
        index_.clear(i);
      } else {
        index_.set(i);
      }
    }
    if (max < tailleIdx) {
      index_.set(max + 1, tailleIdx + 1);
    } else if (tailleIdx < max) {
      index_.clear(_taille, max + 1);
    }
    maxComputed = false;
    if (_event) {
      fireSelectionEvent();
    }
  }

  /**
   * Operation or avec la liste
   * <code>_l</code>.
   *
   * @param _l l'argument de l'operation
   */
  private boolean or(final CtuluListSelection _l, final boolean _event) {
    if (contains(_l)) {
      return false;
    }
    index_.or(_l.index_);
    maxComputed = false;
    if (_event) {
      fireSelectionEvent();
    }
    return true;
  }

  private boolean remove(final int _i, final boolean _event) {
    if (isSelected(_i)) {
      index_.clear(_i);
      maxComputed = false;
      if (_event) {
        fireSelectionEvent();
      }
      return true;
    }
    return false;
  }

  /**
   * Enleve tous les indexs de [_deb,_fin].
   *
   * @param _deb l'index inferieur
   * @param _fin l'index superieur
   */
  private boolean removeInterval(final int _deb, final int _fin, final boolean _event) {
    boolean r = false;
    for (int i = _deb; i <= _fin; i++) {
      r |= remove(i, false);
    }
    maxComputed = false;
    if (r && _event) {
      fireSelectionEvent();
    }
    return r;
  }

  private boolean setSelectionInterval(final int _deb, final int _fin, final boolean _event) {
    boolean r = removeInterval(0, _deb - 1, false);
    r |= addInterval(_deb, _fin, false);
    r |= removeInterval(_fin + 1, getMaxIndex(), false);
    maxComputed = false;
    if ((listeners_ != null) && r && _event) {
      fireSelectionEvent();
    }
    return r;
  }

  /**
   * Operation add ( soit une operation or ) avec la liste
   * <code>_l</code>. Envoie un evt seulement si des changements sont effectues.
   *
   * @param _l l'argument de l'operation
   */
  public boolean add(final CtuluListSelection _l) {
    return or(_l);
  }

  /**
   * Ajoute l'index
   * <code>_i </code> a la selection.
   *
   * @param _i
   */
  public boolean add(final int _i) {
    return add(_i, listeners_ != null);
  }

  /**
   * Ajoute tous les indexs de [_deb,_fin].
   *
   * @param _deb l'index inferieur
   * @param _fin l'index superieur
   */
  public boolean addInterval(final int _deb, final int _fin) {
    return addInterval(_deb, _fin, listeners_ != null);
  }

  public void addListeSelectionListener(final CtuluListSelectionListener _l) {
    if (listeners_ == null) {
      listeners_ = new HashSet(5);
    }
    listeners_.add(_l);
  }

  /**
   * Operation and avec la liste
   * <code>_l</code>.
   *
   * @param _l l'argument de l'operation
   */
  public boolean and(final CtuluListSelection _l) {
    if ((_l == null) || (_l.isEmpty())) {
      if (!isEmpty()) {
        clear(true);
        return true;
      }
      return false;
    } else if (!_l.contains(this)) {
      index_.and(_l.index_);
      maxComputed = false;
      if (listeners_ != null) {
        fireSelectionEvent();
      }
      return true;
    }
    return false;
  }

  /**
   * Enleve tous les indexs de la selection et envoie un evt.
   */
  @Override
  public final void clear() {
    clear(true);
  }

  public void clearListeSelectionListener() {
    if (listeners_ != null) {
      listeners_.clear();
    }
  }

  /**
   * Clone cet objet: creation d'une nouvelle instance avec le clone du BitSet.
   *
   * @return nouveau clone
   */
  @Override
  public final Object clone() {
    return new CtuluListSelection(cloneBitSet());
  }

  public FastBitSet cloneBitSet() {
    FastBitSet cloned = new FastBitSet(index_);
    return cloned;
  }

  /**
   * Renvoie true si cette selection contient totalement la selection _l.
   *
   * @return true si _l totalement inclue.
   */
  public boolean contains(final CtuluListSelection _l) {
    // Si la selection est vide ou nulle elle est inclue dans this
    if ((_l == null) || (_l.isEmpty())) {
      return true;
    }
    // Si la taille de l est superieure elle n'est pas inclue
    if (_l.getMaxIndex() > getMaxIndex()) {
      return false;
    }
    // les indices de _l
    final int[] indexL = _l.getSelectedIndex();
    final int n = indexL.length - 1;
    // si un indice de _l n'est pas inclue, renvoie false
    for (int i = n; i >= 0; i--) {
      if (!isSelected(indexL[i])) {
        return false;
      }
    }
    // tous les indices sont contenues... ok.
    return true;
  }

  /**
   * @param _l le listener a tester
   * @return true si le listener est deja enregistre.
   */
  public boolean containsListener(final CtuluListSelectionListener _l) {
    if (listeners_ != null) {
      return listeners_.contains(_l);
    }
    return false;
  }

  /**
   * Fait appel a la methode equals de BitSet.
   */
  @Override
  public final boolean equals(final Object _o) {
    if (_o instanceof CtuluListSelection) {
      return index_.equals(((CtuluListSelection) _o).index_);
    }
    return false;
  }

  /**
   * Renvoie l'inverse de cette selection dans l'intervalle [0;length()[.
   *
   * @return l'inverse dans une nouvelle instance.
   */
  public CtuluListSelection getInverse() {
    return getInverse(length());
  }

  /**
   * Renvoie l'inverse de cette selection dans l'intervalle [0;_taille[.
   *
   * @param _taille
   * @return l'inverse dans une nouvelle instance.
   */
  public CtuluListSelection getInverse(final int _taille) {
    final CtuluListSelection r = (CtuluListSelection) clone();
    r.inverse(_taille, false);
    return r;
  }

  /**
   * @return l'index selectionnee max.
   */
  @Override
  public int getMaxIndex() {
    if (maxComputed) {
      return max;
    }
    max = index_.length() - 1;
    maxComputed = true;
    return max;
  }

  @Override
  public int getMinIndex() {
    final int n = getMaxIndex();
    for (int i = 0; i <= n; i++) {
      if (index_.get(i)) {
        return i;
      }
    }
    return n;
  }

  @Override
  public int getNbSelectedIndex() {
    if (index_.isEmpty()) {
      return 0;
    }
    final int m = getMaxIndex();
    int r = 0;
    for (int i = getMinIndex(); i <= m; i++) {
      if (index_.get(i)) {
        r++;
      }
    }
    return r;
  }

  /**
   * @return la liste des index selectionnes.
   */
  @Override
  public int[] getSelectedIndex() {
    return getSelectedIndex(getMaxIndex());
  }

  /**
   * Renvoie la liste des indexs selectionnes dans l'intervalle [0;_taille].
   *
   * @param _maxIndex l'index max
   * @return la liste des index selectionnes et tries dans l'ordre
   */
  public int[] getSelectedIndex(final int _maxIndex) {
    // renvoie null si vide
    if (isEmpty()) {
      return null;
    }
    // Si l'index demande est plus grand que l'index max
    // on prend l'index max
    int max = getMaxIndex();
    max = max > _maxIndex ? _maxIndex : max;
    // Tableau temporaire stockant les indices selectionnes
    TIntArrayList list = new TIntArrayList(max);
    // on parcourt tous les indexs et les indexs selectionnes sont
    // rajoutes.
    for (int i = 0; i <= max; i++) {
      if (isSelected(i)) {
        list.add(i);
      }
    }
    // les indexs sont tous selectionnes on renvoie le tableau temporaire
    return list.toNativeArray();
  }

  @Override
  public int hashCode() {
    return index_.hashCode();
  }

  /**
   * Operation intersection ( soit une operation and) avec la liste
   * <code>_l</code>.
   *
   * @param _l l'argument de l'operation
   */
  public boolean intersection(final CtuluListSelection _l) {
    return and(_l);
  }

  /**
   * Renvoie true si l'intersection avec
   * <code>_l</code> non vide.
   *
   * @return si intersection non vide.
   */
  public boolean intersects(final CtuluListSelection _l) {
    // si l vide ou nul renvoie false
    if ((_l == null) || (_l.isEmpty())) {
      return false;
    }
    final int[] indexL = _l.getSelectedIndex();
    final int n = indexL.length - 1;
    // si un indice de _l est present intersectin non vide
    for (int i = n; i >= 0; i--) {
      if (isSelected(indexL[i])) {
        return true;
      }
    }
    // tous les indices sont differents pas d'inclusion
    return false;
  }

  /**
   * Inverse la selection dans l'intervalle [0, length()[. Envoie un evt si modification.
   */
  public void inverse() {
    inverse(index_.length(), listeners_ != null);
  }

  /**
   * Inverse la selection dans l'intervalle [0, _taille[. Envoie un evt si modification.
   *
   * @param _taille
   */
  public void inverse(final int _taille) {
    inverse(_taille, listeners_ != null);
  }

  /**
   * @return Empty
   */
  @Override
  public final boolean isEmpty() {
    return index_.length() == 0;
  }

  @Override
  public boolean isOnlyOnIndexSelected() {
    return (index_.length() > 0) && (getNbSelectedIndex() == 1);
  }

  /**
   * @param _i
   * @return Selected
   */
  @Override
  public boolean isSelected(final int _i) {
    if (_i < 0) {
      return false;
    }
    if (maxComputed && _i > getMaxIndex()) {
      return false;
    }
    return index_.get(_i);
  }

  /**
   * @return la taille de la selection
   */
  public int length() {
    return getNbSelectedIndex();
  }

  /**
   * Operation or avec la liste
   * <code>_l</code>.
   *
   * @param _l l'argument de l'operation
   */
  public boolean or(final CtuluListSelection _l) {
    return or(_l, listeners_ != null);
  }

  /**
   * Enleve tous les indexs selectionnes de
   * <code>_l</code>.
   *
   * @param _l la liste des indexs a enlever.
   * @return true si modif
   */
  public boolean remove(final CtuluListSelection _l) {
    if (_l == null || _l.isEmpty() || isEmpty()) {
      return false;
    }
    boolean r = false;
    final int max = getMaxIndex();
    for (int i = 0; i <= max; i++) {
      if ((isSelected(i)) && (_l.isSelected(i))) {
        r = true;
        remove(i, false);
      }
    }
    maxComputed = false;
    if ((listeners_ != null) && r) {
      fireSelectionEvent();
    }
    return r;
  }

  /**
   * Enleve l'index
   * <code>_i</code> a la selection.
   *
   * @param _i
   */
  public boolean remove(final int _i) {
    return remove(_i, listeners_ != null);
  }

  public boolean removeInterval(final int _deb, final int _fin) {
    return removeInterval(_deb, _fin, listeners_ != null);
  }

  public void removeListeSelectionListener(final CtuluListSelectionListener _l) {
    if (listeners_ != null) {
      listeners_.remove(_l);
    }
  }

  public final boolean setSelection(final CtuluListSelection _l) {
    if (equals(_l)) {
      return false;
    }
    clear(false);
    or(_l, false);
    maxComputed = false;
    if (listeners_ != null) {
      fireSelectionEvent();
    }
    return true;
  }

  public final void setSelection(final CtuluListSelectionInterface _l) {
    if (equals(_l)) {
      return;
    }
    clear(false);
    final int max = _l.getMaxIndex();
    for (int i = _l.getMinIndex(); i <= max; i++) {
      if (_l.isSelected(i)) {
        index_.set(i);
      }
    }
    maxComputed = false;
    if (listeners_ != null) {
      fireSelectionEvent();
    }
  }

  public final void setSelection(final ListSelectionModel _m) {
    if (_m.isSelectionEmpty()) {
      clear();
    } else {
      final int max = _m.getMaxSelectionIndex();
      final FastBitSet s = new FastBitSet(max);
      for (int i = _m.getMinSelectionIndex(); i <= max; i++) {
        if (_m.isSelectedIndex(i)) {
          s.set(i);
        }
      }

      if (!s.equals(index_)) {
        clear(false);
        index_.or(s);
        maxComputed = false;
        fireSelectionEvent();
      }
    }
  }

  /**
   * @param _deb SelectionInterval
   * @param _fin SelectionInterval
   */
  public void setSelectionInterval(final int _deb, final int _fin) {
    setSelectionInterval(_deb, _fin, true);
  }

  /**
   * Operation xor avec la liste
   * <code>_l</code>.
   *
   * @param _l l'argument de l'operation
   */
  public boolean xor(final CtuluListSelection _l) {
    if ((_l == null) || (_l.isEmpty())) {
      return false;
    }
    index_.xor(_l.index_);
    maxComputed = false;
    if (listeners_ != null) {
      fireSelectionEvent();
    }
    return true;
  }
}