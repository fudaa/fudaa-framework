package org.fudaa.ctulu;

import java.util.*;

/**
 * Manager qui indique toutes les erreurs,messages qui se sont produites pendant le chargement.
 *
 * @author Adrien Hadoux
 */
public class CtuluLogGroup {

  private List<CtuluLog> logs = new ArrayList<CtuluLog>();
  /**
   *
   */
  List<CtuluLogGroup> groups;
  List<CtuluLogGroup> groupsExt;
  ResourceBundle defaultResource;
  String description;
  private String[] descriptionArgs = null;

  public CtuluLogGroup(ResourceBundle defaultResource) {
    super();
    this.defaultResource = defaultResource;
  }

  public String getDescription() {
    return description;
  }

  public CtuluLogLevel getHigherLevel() {
    CtuluLogLevel higher = null;
    if (logs != null) {
      for (CtuluLog ctuluLog : logs) {
        CtuluLogLevel thisLogHigherLevel = ctuluLog.getHigherLevel();
        if (higher == null || higher.isMoreVerboseThan(thisLogHigherLevel, true)) {
          higher = thisLogHigherLevel;
        }
      }
    }
    if (groups != null) {
      for (CtuluLogGroup ctuluLogGroup : groups) {
        CtuluLogLevel thisLogHigherLevel = ctuluLogGroup.getHigherLevel();
        if (higher == null || higher.isMoreVerboseThan(thisLogHigherLevel, true)) {
          higher = thisLogHigherLevel;
        }
      }
    }
    return higher;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String[] getDescriptionArgs() {
    return descriptionArgs;
  }

  /**
   *
   * @return group with empty logs or child group
   */
  public CtuluLogGroup createCleanGroup() {
    CtuluLogGroup res = new CtuluLogGroup(defaultResource);
    res.descriptionArgs = descriptionArgs;
    res.description = description;
    for (CtuluLog log : logs) {
      if (log.isNotEmpty()) {
        res.addLog(log);
      }
    }
    if (groups != null) {
      for (CtuluLogGroup log : groups) {
        CtuluLogGroup clean = log.createCleanGroup();
        if (clean.containsSomething()) {
          res.addGroup(clean);
        }

      }
    }
    return res;

  }

  public void setDescriptionArgs(String... descriptionArgs) {
    this.descriptionArgs = descriptionArgs;
  }

  public String getDesci18n() {
    if (defaultResource == null) {
      return description;
    }
    if (description == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    try {
      final String i18n = defaultResource.getString(description);
      if (descriptionArgs != null) {
        return java.text.MessageFormat.format(i18n, (Object[]) descriptionArgs);
      }
      return i18n;
    } catch (MissingResourceException ex) {
      //do nothing...
    }
    return description;
  }

  public void addLog(CtuluLog analyze) {
    logs.add(analyze);
  }

  public void addLogFirstPosition(CtuluLog analyze) {
    if (logs.isEmpty()) {
      logs.add(analyze);
    } else {
      logs.add(0, analyze);
    }
  }

  public void addAll(Collection<CtuluLog> allLogs) {
    logs.addAll(allLogs);
  }

  public void addGroup(CtuluLogGroup logGroup) {
    if (groups == null) {
      groups = new ArrayList<CtuluLogGroup>();
    }
    groups.add(logGroup);
  }

  public void addNotEmptyLog(CtuluLog log) {
    if (log.isNotEmpty()) {
      addLog(log);
    }

  }

  public void addNotEmptyLogs(Collection<CtuluLog> logs) {
    for (CtuluLog log : logs) {
      addNotEmptyLog(log);
    }

  }

  public int getNbOccurence(CtuluLogLevel level) {
    int count = 0;
    for (CtuluLog log : logs) {
      count += log.getNbOccurence(level);

    }
    if (groups != null) {
      for (CtuluLogGroup group : groups) {
        count += group.getNbOccurence(level);

      }
    }

    return count;
  }

  public void clear() {
    for (CtuluLog log : logs) {
      log.clear();
    }
    logs.clear();
    if (groups != null) {
      for (CtuluLogGroup logGroup : groups) {
        logGroup.clear();
      }
      groups.clear();
    }
  }

  public CtuluLog createNewLog(String desc) {
    CtuluLog analyzer = createAnalyzer();
    analyzer.setDesc(desc);
    logs.add(analyzer);
    return analyzer;
  }

  /**
   * @return true si un analyseur contient des erreur
   */
  public boolean containsError() {
    if (isContentEmpty()) {
      return false;
    }
    for (CtuluLog analyze : logs) {
      if (analyze.containsSevereError() || analyze.containsErrors()) {
        return true;
      }
    }
    if (groups != null) {
      for (CtuluLogGroup mng : groups) {
        if (mng.containsError()) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean containsFatalError() {
    if (isContentEmpty()) {
      return false;
    }
    for (CtuluLog analyze : logs) {
      if (analyze.containsSevereError()) {
        return true;
      }
    }
    if (groups != null) {
      for (CtuluLogGroup mng : groups) {
        if (mng.containsFatalError()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @return true si les analyseur ne sont pas vide
   */
  public boolean containsSomething() {
    if (isContentEmpty()) {
      return false;
    }
    for (CtuluLog analyze : logs) {
      if (!analyze.isEmpty()) {
        return true;
      }
    }
    if (groups != null) {
      for (CtuluLogGroup mng : groups) {
        if (mng != this) {
          if (mng.containsSomething()) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public boolean containsWarning() {
    if (isContentEmpty()) {
      return false;
    }
    for (CtuluLog analyze : logs) {
      if (analyze.containsWarnings()) {
        return true;
      }
    }
    if (groups != null) {
      for (CtuluLogGroup mng : groups) {
        if (mng.containsWarning()) {
          return true;
        }
      }
    }
    return false;
  }

  private CtuluLog createAnalyzer() {
    CtuluLog analyzer = new CtuluLog();
    analyzer.setDefaultResourceBundle(defaultResource);
    return analyzer;
  }

  /**
   * @param name la nom du sous-groupe
   * @return le sous-groupe ajoute.
   */
  public CtuluLogGroup createGroup(String name) {
    CtuluLogGroup res = new CtuluLogGroup(defaultResource);
    res.setDescription(name);
    if (groups == null) {
      groups = new ArrayList<CtuluLogGroup>();
    }
    groups.add(res);
    return res;
  }

  /**
   * @return la liste des anayliser utilis�
   */
  public List<CtuluLog> getLogs() {
    return logs;
  }

  /**
   * @return the groups
   */
  public List<CtuluLogGroup> getGroups() {
    if (groups == null) {
      return Collections.emptyList();
    }
    if (groupsExt == null) {
      groupsExt = Collections.unmodifiableList(groups);
    }
    return groupsExt;
  }

  /**
   * retourne le dernier ctuluanalyze cree.
   *
   * @param message
   * @return
   */
  public CtuluLog getLastLog() {
    return logs.get(logs.size() - 1);
  }

  /**
   * Ajoute un nouvel CtuluLog a la liste et le fournit.
   *
   * @return
   */
  public CtuluLog createLog() {
    CtuluLog log = createAnalyzer();
    logs.add(log);
    return log;
  }

  private boolean isContentEmpty() {
    return CtuluLibArray.isEmpty(logs) && CtuluLibArray.isEmpty(groups);
  }

  public void setListeMessageError(final List<CtuluLog> listeMessageError) {
    this.logs = listeMessageError;
  }
//  /**
//   * @param mainAnalyze the mainAnalyze to set
//   */
//  public void setMainAnalyze(CtuluLog mainAnalyze) {
//    logs.remove(this.mainLog);
//    this.mainLog = mainAnalyze;
//    logs.add(0, mainLog);
//  }
}
