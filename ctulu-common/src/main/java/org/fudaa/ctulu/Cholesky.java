/**************************************************************************
 *   vthierry@sophia.inria.fr, Copyright (C) 1999  All rights reserved.   *
 **************************************************************************/

package org.fudaa.ctulu;

/**
 * Encapsulate the Cholesky decomposition for linear equations resolution. The <a href=#Cholesky>Cholesky</a>
 * decomposition of a symmetric positive definite matrix S is a lower triangular matrix L such that:
 *
 * <pre>
 *     S = L L&circ;T
 * </pre>
 */

public class Cholesky {

  protected int n_;

  protected float[] lfloat_;

  /**
   * Construct the Cholesky decomposition of the matrix S.
   *
   * @param _s A <i>n x n</i> definite symmetric positive matrix stored in a float <a href=#smatrix>array</a>.
   */
  public Cholesky(final float[] _s, final int _n) throws NonDefiniteException {
    this.n_ = _n;
    init(_n);
    lfloat_ = new float[_n * (_n + 1) / 2];
    if (convert(_s, (_s == null) ? 0 : _s.length, lfloat_, _n) != 0) {
      throw new IllegalArgumentException(
          "S does not correspond " + "to a symmetric matrix");
    }
    switch (calc(lfloat_, lArray, _n)) {
    case -2:
      throw new IllegalArgumentException("S does not correspond " + "to a positive matrix");
    case -1:
      throw new NonDefiniteException();
    default:
    }
  }

  /** Get a vector such that S x = y. */
  public float[] getX(final float[] _y) {
    final float[] x = (float[]) _y.clone();
    solve(lfloat_, x, lArray, n_);
    return x;
  }

  /**
   * Return the Cholesky lower triangular matrix L.
   *
   * @return A <a href=#lmatrix>n(n+1)/2</a> array containing L.
   */
  public float[] getL() {
    return lfloat_;
  }

  /** <hr> */

  /** RunTime exception thrown if the matrix is not definite. */

  public static class NonDefiniteException extends ArithmeticException {
    /* ! */public NonDefiniteException() {
      super();
    }

    /* ! */public NonDefiniteException(final String _s) {
      super(_s);
    }
  }

  /** *********************************************** */
  /** * Native implementation of the algorithm ** */
  /** *********************************************** */

  // Index mechanism in lower triangular matrix so that :
  // M[i,j] = M[i+l_[j]] for j < nl_
  static int[] lArray;

  static int nl;

  // Initialize the index buffer for lower triangular matrix up to n x n.

  static final void init(final int _n) {
    if (nl < _n) {
      nl = _n;
      lArray = new int[_n];
      for (int k = 0; k < _n; k++) {
        lArray[k] = (k * (k + 1)) / 2;
      }
    }
  }

  // Convert a symmetric matrix Q given in various forms
  // to a lower triangular matrix q

  static/* nativez */int convert(final float[] _q, final int _dimQ, final float[] _q2, final int _n) {
    if (_dimQ == 0) {
      for (int ij = 0; ij < _n * (_n + 1) / 2; _q2[ij++] = 0) {};
    } else if (_dimQ == 1) {
      int ij = 0;
      for (int j = 0; j < _n; j++) {
        _q2[ij++] = _q[0];
        for (int i = 0; i < j; i++) {
          _q2[ij++] = 0;
        }
      }
    } else if (_dimQ == _n) {
      int ij = 0;
      for (int j = 0; j < _n; j++) {
        for (int i = 0; i < j; i++) {
          _q2[ij++] = 0;
        }
        _q2[ij++] = _q[j];
      }
    } else if (_dimQ == _n * (_n + 1) / 2) {
      for (int ij = 0; ij < _n * (_n + 1) / 2; ij++) {
        _q2[ij] = _q[ij];
      }
    } else if (_dimQ == _n * _n) {
      int ij = 0;
      for (int j = 0; j < _n; j++) {
        for (int i = 0; i < j; i++) {
          _q2[ij++] = (_q[i + j * _n] + _q[j + i * _n]) / 2;
        }
        _q2[ij++] = _q[j + j * _n];
      }
    } else {
      return -2;
    }
    return 0;
  }

  // Square-Root decomposition of a lower triangular matrix s[]
  // On output, s[] containts l[] with s = l l^T

  static final/* nativez */int calc(final float[] _s,
  /* const */final int[] _l, final int _n) {
    for (int k = 0; k < _n; k++) {
      int l0 = k + _l[k];
      float s0 = _s[l0];
      if (s0 <= 0) {
        if (s0 < 0) {
          return -2;
        }
        return -1;
      }
      s0 = (float) Math.sqrt(s0);
      _s[l0] = s0;
      l0 = k + 1;
      for (int j = l0; j < _n; j++) {
        _s[k + _l[j]] /= s0;
        for (int i = l0; i <= j; i++) {
          _s[i + _l[j]] -= _s[k + _l[i]] * _s[k + _l[j]];
        }
      }
    }
    return 0;
  }

  // Resolution of l l^T x = x
  // On output x is replaced by the solution

  static final/* nativez */void solve(/* const */final float[] _l, final float[] _x,
  /* const */final int[] _l2, final int _n) {
    for (int j = 0; j < _n; j++) {
      for (int i = 0; i < j; i++) {
        _x[j] -= _l[i + _l2[j]] * _x[i];
      }
      _x[j] /= _l[j + _l2[j]];
    }
    for (int j = _n - 1; j >= 0; j--) {
      for (int i = j + 1; i < _n; i++) {
        _x[j] -= _l[j + _l2[i]] * _x[i];
      }
      _x[j] /= _l[j + _l2[j]];
    }
  }
}

/**
 * <hr>
 * <center>
 * <h1> Matrices Format Conventions </h1>
 * </center>
 */

/**
 * <a name=smatrix> Here n x n symmetric matrices are stored either :
 * <dt>
 * <dd> - as a true symmetric matrix stored in a <a href=#matrix>n x n</a> float array or
 * <dt>
 * <dd> - as a true symmetric matrix stored in a <a href=#lmatrix>n (n+1) / 2 </a> float array or
 * <dt>
 * <dd> - as a diagonal matrix stored in a <i>n</i> float array or
 * <dt>
 * <dd> - as a multiple of the indentity matrix I stored as a <i>1</i> element float array or a float
 */

/**
 * <a name=matrix> Here p x n matrices are stored in a p x n float array, row by row: a = {
 * <dt>
 * <dd> A[1,1], A[1,2], .. A[1,n]
 * <dt>
 * <dd> ..
 * <dt>
 * <dd> A[p,1], A[p,2], .. A[p,n] }
 */

/**
 * <a name=lmatrix> Here n x n symmetric or lower triangular matrices are stored in a n x (n+1) / 2 float array, row by
 * row: s = {
 * <dt>
 * <dd> S[1,1],
 * <dt>
 * <dd> S[1,2], S[2,2],
 * <dt>
 * <dd> ..
 * <dt>
 * <dd> S[1,n], S[2,n], .. S[n,n] }
 */

/**
 * <hr>
 * <a name=Cholesky> The Cholesky Decomposition </h1>
 * The Cholesky or ``square-root'' decomposition of a symmetric positive definite matrix S is a lower triangular matrix
 * L such that S = L L^T. Two matrices L and L' are the square-root of a symmetric positive matrix S if and only if L =
 * R L' for some orthogonal matrix R, i.e. with R R^T = I This particular decomposition corresponds to the reduction of
 * a quadratic form:
 *
 * <pre>
 *     -n   -n                      -n   /  -n             \ 2
 *     q(x) = &gt;    &gt;      S_i,j x_i x_j =  &gt;    |  &gt;     Li,k x_i |
 *     -i=1 -j=1                    -k=1 \  -j=k           /
 * </pre>
 *
 * as a sum of squares. It is defined thank's to the fact that for a positive definite symmetric matrix:
 *
 * <pre>
 *     S_i,i &gt; 0  and  (S_i,j)&circ;2 &lt; S_i,i S_j,j  and  max_i,j |S_i,j| = S_k,k
 * </pre>
 *
 * for some k, by the following algorithm:
 *
 * <pre>
 *     L = S
 *     for k in {1..n}
 *     L_k,k = Sqrt(L_k,k)
 *     for j in {k+1..n}
 *     L_k,j = L_k,j / L_k,k
 *     for i in {k+1..j}
 *     L_i,j = L_i,j - L_k,i L_k,j
 * </pre>
 *
 * Using this decomposition, the triangular equation L L^T x = y is solved in o(n^2) steps.
 */

