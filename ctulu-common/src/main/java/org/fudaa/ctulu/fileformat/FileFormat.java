/*
 * @modification $Date: 2007-06-29 15:09:35 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuInformationsSoftware;
import org.fudaa.ctulu.*;

import java.io.File;
import java.util.Collection;
import java.util.Comparator;

/**
 * @author deniger
 * @version $Id: FileFormat.java,v 1.20 2007-06-29 15:09:35 deniger Exp $
 */
public abstract class FileFormat implements Comparable {
  // Le nombre de fichiers demand� par ce format.
  final private int nbFile_;
  protected String[] extensions_;
  protected String nom_;
  protected String id_;
  protected String description_;
  // le type du fichier projet,maillage,autre,...
  protected Object type_;
  protected BuInformationsSoftware software_;

  public static BuFileFilter[] createFilters(final FileFormat[] _fmt) {
    if (_fmt == null) {
      return null;
    }
    final BuFileFilter[] res = new BuFileFilter[_fmt.length];
    for (int i = res.length - 1; i >= 0; i--) {
      if (_fmt[i] != null) {
        res[i] = _fmt[i].createFileFilter();
      }
    }
    return res;
  }

  public static FileFormat findFileFormat(final FileFormat[] _fts, final File _file) {
    final int nb = _fts.length;
    for (int i = 0; i < nb; i++) {
      final BuFileFilter filter = _fts[i].createFileFilter();
      if (filter.accept(_file)) {
        return _fts[i];
      }
    }
    return null;
  }

  public static FileFormat findFileFormat(final Collection<? extends FileFormat> _fts, final File _file) {
    if (_fts == null) {
      return null;
    }
    for (FileFormat fileFormat : _fts) {
      final BuFileFilter filter = fileFormat.createFileFilter();
      if (filter.accept(_file)) {
        return fileFormat;
      }
    }
    return null;
  }

  public static int findFileFormat(final FileFormat[] _fts, final String _id) {
    if (_fts == null || _id == null) {
      return -1;
    }
    for (int i = _fts.length - 1; i >= 0; i--) {
      if (_id.equals(_fts[i].getID())) {
        return i;
      }
    }
    return -1;
  }

  /**
   * @return les extensions des fichiers qui seront cr��s ou lus en meme temps
   */
  public String[] getLinkedExtension() {
    return null;
  }

  /**
   * @param _f le fichier a lire
   * @param _prog la barre de progression
   * @return la synthese de la lecture. Les donnees lues sont dans la synthese (getSource()).
   */
  public static CtuluIOOperationSynthese read(final FileFormatVersionInterface _v, final File _f,
                                              final ProgressionInterface _prog) {
    final FileReadOperationAbstract i = _v.createReader();
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  /**
   * @param _f le fichier a ecrire
   * @param _source la source. Il faut tester si l'instance est correcte
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public static CtuluIOOperationSynthese write(final FileFormatVersionInterface _v, final File _f,
                                               final Object _source, final ProgressionInterface _prog) {
    final FileWriteOperationAbstract i = _v.createWriter();
    final CtuluIOOperationSynthese r = i.write(_source, _f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  /**
   * @param _nbFile le nombre de fichier requis par ce format
   */
  public FileFormat(final int _nbFile) {
    if (_nbFile < 1) {
      throw new IllegalArgumentException("Nombre de fichier strictement positif");
    }
    nbFile_ = _nbFile;
  }

  /**
   * @return un filtre correspondant aux extension
   */
  public BuFileFilter createFileFilter() {
    return new BuFileFilter(extensions_, nom_);
  }

  /**
   * @param _dir le repertoire contenant le fichier
   * @param _name le nom du fichier sans extension
   * @return le fichier _dir/_name.extension[0]
   */
  public File getFileFor(final File _dir, final String _name) {
    return getFileFor(_dir, _name, extensions_[0]);
  }

  public static File getFileFor(final File _dir, final String _name, final String _ext) {
    return new File(_dir, CtuluLibFile.getFileName(_name, _ext));
  }

  public File getFileExistFor(final File _dir, final String _name) {
    File r = getFileFor(_dir, _name);
    if (!r.exists()) {
      r = getFileFor(_dir, _name, extensions_[0].toLowerCase());
    }
    return r.exists() ? r : null;
  }

  public static File getFileExistFor(final File _dir, final String _name, final String _ext) {
    File r = getFileFor(_dir, _name, _ext);
    if (!r.exists()) {
      r = getFileFor(_dir, _name, _ext.toLowerCase());
    }
    return r.exists() ? r : null;
  }

  /**
   * @return le nombre de version supporte par ce format
   */
  public int getVersionNb() {
    return 1;
  }

  /**
   * @return les identifiants des versions
   */
  public String[] getVersions() {
    return new String[]{getLastVersion()};
  }

  /**
   * @return l'identifiant de la version la plus recente
   */
  public abstract String getLastVersion();

  /**
   * @param _ext l'extension a tester
   * @return true si cette extension correspond a ce format
   */
  public boolean isExtension(final String _ext) {
    if (extensions_ == null || _ext == null) {
      return false;
    }
    final boolean r = CtuluLibArray.findObject(extensions_, _ext) >= 0;
    if (!r) {
      return CtuluLibArray.findObject(extensions_, _ext.toLowerCase()) >= 0;
    }
    return r;
  }

  public boolean isAccepted(final File _f) {
    return isAccepted(_f.getName());
  }

  public boolean isAccepted(final String _f) {
    return isExtension(CtuluLibFile.getExtension(_f));
  }

  /**
   * @param _f le fichier a ouvrir peut influencer sur le format a utiliser
   * @return la version par defaut
   */
  public abstract FileFormatVersionInterface getLastVersionInstance(File _f);

  /**
   * @param _id identifiant permettant de specifier le type de la version
   * @return la version selon l'id
   */
  /*
   * public FileFormatVersionInterface getLastVersionInstance(final String _id) { return null
   * getLastVersionInstance(null) ; }
   */

  /**
   * Returns the description.
   *
   * @return String
   */
  public String getDescription() {
    return description_;
  }

  /**
   * @return l'identifiant de ce format
   */
  public String getID() {
    return id_;
  }

  /**
   * Returns the extensions.
   *
   * @return String[]
   */
  public String[] getExtensions() {
    return extensions_;
  }

  public Object getType() {
    return type_;
  }

  /**
   * Returns the nbFile.
   *
   * @return int
   */
  public final int getNbFile() {
    return nbFile_;
  }

  /**
   * Returns the nom.
   *
   * @return String
   */
  public String getName() {
    return nom_;
  }

  /**
   * Returns the software.
   *
   * @return BuInformationsSoftware
   */
  public BuInformationsSoftware getSoftware() {
    return software_;
  }

  /**
   * Renvoie a partir du nombre de fichier et des extensions du format les fichiers correspondants (si le nombre de
   * fichier est 1, fichier donne est renvoye).
   *
   * @param _fInit le fichier initiale
   * @return un tableau de taille getNbFile() avec les fichiers existants et correspondant aux extensions.
   */
  public final File[] getFile(final File _fInit) {
    final File[] r = new File[nbFile_];
    if (_fInit == null) {
      return r;
    }
    if (nbFile_ == 1) {
      return new File[]{_fInit};
    }
    final String base = CtuluLibFile.getSansExtension(_fInit.getAbsolutePath());
    for (int i = 0; i < nbFile_; i++) {
      r[i] = new File(base + CtuluLibString.DOT + extensions_[i]);
    }
    return r;
  }

  /**
   * Ce comparateur permet de classer des formats selon leurs nom (ATTENTION 2 formats avec le meme nom seront supposes
   * egaux).
   */
  public static class FileFormatNameComparator implements Comparator<FileFormat> {
    /**
     * Compare 2 FileFormat selon le nom (uniquement).
     */
    @Override
    public int compare(final FileFormat _o1, final FileFormat _o2) {
      return _o1.getName().compareTo(_o2.getName());
    }
  }

  /**
   * Compare les noms des formats.
   */
  @Override
  public int compareTo(final Object _o) {
    if (!(_o instanceof FileFormat)) {
      throw new IllegalArgumentException("o doit etre de type FileFormat");
    }
    return getName().compareTo(((FileFormat) _o).getName());
  }

  /**
   *
   */
  @Override
  public String toString() {
    return getName();
  }
}
