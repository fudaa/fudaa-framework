/*
 * @creation 14 ao�t 2003
 * @modification $Date: 2006-09-19 14:36:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author deniger
 * @version $Id: FileFormatVersion.java,v 1.7 2006-09-19 14:36:56 deniger Exp $
 */
public abstract class FileFormatVersion implements FileFormatVersionInterface {

  String version_;

  FileFormat ft_;

  /**
   * @param _ft le format parent
   * @param _nom le nom de cette version
   */
  public FileFormatVersion(final FileFormat _ft, final String _nom) {
    version_ = _nom;
    ft_ = _ft;
  }

  /**
   * @return le format parent
   */
  @Override
  public FileFormat getFileFormat() {
    return ft_;
  }

  /**
   * @return le nom de cette version
   */
  @Override
  public String getVersionName() {
    return version_;
  }

  /**
   * @return le nom du format parent
   */
  public String getFormatName() {
    return ft_.getName();
  }

  /**
   * @return le nom complet de cette version = getFormatName+getVersionName
   */
  public String getName() {
    return getFormatName() + " " + version_;
  }

  /**
   * @see java.lang.Object#toString()
   * @see #getName()
   */
  @Override
  public String toString() {
    return getName();
  }

  /**
   * Compare selon le num de version si exactement meme version (meme classe).
   *
   * @param _obj la version a comparer
   * @return 1,-1 ou 0
   */
  public int compareTo(final Object _obj) {
    if (_obj == this) {
      return 0;
    }
    if (getClass().equals(_obj.getClass())) {
      return version_.compareTo(((FileFormatVersion) _obj).version_);
    }
    throw new IllegalArgumentException("compareTo " + getClass().getName());
  }


  @Override
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog) {
    return FileFormat.read(this, _f, _prog);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return FileFormat.write(this, _f, _source, _prog);
  }
}