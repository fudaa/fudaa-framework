/*
 *  @creation     8 juin 2003
 *  @modification $Date: 2006-02-09 08:59:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;
import java.io.IOException;
/**
 * @author fred deniger
 * @version $Id: FortranInterface.java,v 1.3 2006-02-09 08:59:30 deniger Exp $
 *
 */
public interface FortranInterface {
  /**
   *  fermeture du flux.
   * @throws IOException exception levee lors de la fermeture
   */
  void close() throws IOException;
}
