/*
 *  @creation     24 sept. 2004
 *  @modification $Date: 2007-06-29 15:09:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author Fred Deniger
 * @version $Id: FileFormatUnique.java,v 1.7 2007-06-29 15:09:34 deniger Exp $
 */
public abstract class FileFormatUnique extends FileFormat implements FileFormatVersionInterface {

  /**
   * @param _nbFile
   */
  public FileFormatUnique(final int _nbFile) {
    super(_nbFile);
  }

  @Override
  public final String getLastVersion() {
    return getVersionName();
  }

  @Override
  public String getVersionName() {
    return CtuluLibString.UN;
  }

  @Override
  public final FileFormat getFileFormat() {
    return this;
  }
  
  @Override
  public final FileFormatVersionInterface getLastVersionInstance(File _f) {
    return this;
  }

  @Override
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog) {
    return read(this, _f, _prog);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return write(this, _f, _source, _prog);
  }

}
