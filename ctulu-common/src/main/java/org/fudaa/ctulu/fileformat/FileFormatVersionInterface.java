/*
 *  @creation     24 sept. 2004
 *  @modification $Date: 2006-02-09 08:59:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;


/**
 * Une interface d�finissant un format de fichier. Elle permet d'acc�der a la classe d'�criture/lecture de ce format
 * de fichier.
 * @author Fred Deniger
 * @version $Id: FileFormatVersionInterface.java,v 1.3 2006-02-09 08:59:30 deniger Exp $
 */
public interface FileFormatVersionInterface {

  /**
   * @return le format parent
   */
  FileFormat getFileFormat();

  /**
   * @return le nom de cette version
   */
  String getVersionName();


  /**
   * @return le reader asssocie a ce format
   */
  FileReadOperationAbstract createReader();

  /**
   * @return le writer associe a cette version
   */
  FileWriteOperationAbstract createWriter();

  /**
   * @param _f le fichier a lire
   * @param _prog la barre de progression
   * @return la synthese de la lecture. Les donnees lues sont dans la synthese (getSource()).
   */
  CtuluIOOperationSynthese read(File _f,ProgressionInterface _prog);

  /**
   * @param _f le fichier a ecrire
   * @param _source la source. Il faut tester si l'instance est correcte
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  CtuluIOOperationSynthese write(File _f,Object _source,ProgressionInterface _prog);
}