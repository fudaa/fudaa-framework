/*
 *  @file         FileWriteOperation.java
 *  @creation     22 ao�t 2003
 *  @modification $Date: 2006-07-13 13:34:41 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu.fileformat;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;

import java.io.*;

/**
 * @author deniger
 * @version $Id: FileOperationAbstract.java,v 1.5 2006-07-13 13:34:41 deniger Exp $
 * @deprecated use {@link FileReadWriteAbstract}
 */
public abstract class FileOperationAbstract {
  protected ProgressionInterface progress_;
  protected CtuluAnalyze analyze_;

  /**
   * @param _progressReceiver l'ecouter pour la progression
   */
  public void setProgressReceiver(final ProgressionInterface _progressReceiver) {
    progress_ = _progressReceiver;
  }

  protected abstract FortranInterface getFortranInterface();

  public CtuluAnalyze getAnalyze() {
    return analyze_;
  }

  /**
   * @param _f le fichier a lire
   */
  public abstract void setFile(File _f);

  protected final CtuluIOOperationSynthese closeOperation(final Object _o) {
    final CtuluIOOperationSynthese r = new CtuluIOOperationSynthese();
    final FortranInterface out = getFortranInterface();
    if (closeStreamAtEnd) {
      r.setClosingOperation(FortranLib.closeState(out));
    }
    r.setAnalyze(analyze_);
    r.setSource(_o);
    progress_ = null;
    analyze_ = null;
    return r;
  }

  private boolean closeStreamAtEnd = true;

  /**
   * @param closeStreamAtEnd si false, le flux ne sera pas ferm� � la fin
   */
  public void setCloseOutputStreamAtEnd(boolean closeStreamAtEnd) {
    this.closeStreamAtEnd = closeStreamAtEnd;
  }

  /**
   * @param _f le fichier a lire
   * @return le FileReader correspondant
   */
  public static final FileReader initFileReader(final File _f) {
    FileReader r = null;
    try {
      r = new FileReader(_f);
    } catch (final FileNotFoundException _e) {
      FuLog.error(_e);
    }
    return r;
  }

  /**
   * @param _f le fichier a ecrire
   * @return le FileWriter correspondant
   */
  public static final FileWriter initFileWriter(final File _f) {
    FileWriter r = null;
    try {
      r = new FileWriter(_f);
    } catch (final IOException _e) {
      FuLog.error(_e);
    }
    return r;
  }
}
