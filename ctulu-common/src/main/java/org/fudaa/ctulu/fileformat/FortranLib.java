/*
 * @creation 8 juin 2003
 * @modification $Date: 2006-09-19 14:36:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.CtuluValueValidator;

/**
 * @author fred deniger
 * @version $Id: FortranLib.java,v 1.8 2006-09-19 14:36:56 deniger Exp $
 */
public final class FortranLib {

  private FortranLib() {

  }

  /**
   * @param _r le flux a fermer
   * @return l'exception levee ou null si aucune exception
   */
  public static IOException close(final FortranInterface _r) {
    IOException envoie = null;
    if (_r == null) {
      return null;
    }
    try {
      _r.close();
    }
    // Si echoue,on modifie l'exception envoie.
    catch (final IOException e) {
      envoie = e;
      FuLog.error(e);
    }
    return envoie;
  }

  /**
   * Methode statique permettant de fermer plusieurs readers. Renvoie dans l'ordre respectif les exceptions levees.
   *
   * @param _r les flux a fermer
   * @return null si aucune exception.
   */
  public static String close(final FortranInterface[] _r) {
    final StringBuffer b = new StringBuffer();
    final int l = _r.length;
    String s;
    for (int i = 0; i < l; i++) {
      s = FortranLib.closeState(_r[i]);
      if (s != null) {
        if (b.length() > 0) {
          b.append(CtuluLibString.LINE_SEP);
        }
        b.append(s);
      }
    }
    return b.length() == 0 ? null : b.toString();
  }

  /**
   * A voir si le calcul est tout le temps correct : probl�me d'arrondi.
   *
   * @param _nbCar le nombre total de caractere
   * @param _nbFrac le nombre de chiffres apres la virgule.
   * @return le max
   */
  public static double getMaxValue(final int _nbCar, int _nbFrac) {
    return Math.pow(10, _nbCar - 1 - _nbFrac) - Math.pow(10, -_nbFrac);
  }

  /**
   * A voir si le calcul est tout le temps correct : probl�me d'arrondi.
   *
   * @param _nbCar le nombre total de caractere
   * @param _nbFrac le nombre de chiffres apres la virgule.
   * @return le min
   */
  public static double getMinValue(final int _nbCar, final int _nbFrac) {
    return -getMaxValue(_nbCar - 1, _nbFrac);
  }

  /**
   * A voir si le calcul est tout le temps correct : probl�me d'arrondi.
   *
   * @param _nbChar le nombre total de caractere
   * @param _maxFractionDigits le nombre de chiffres apres la virgule.
   * @param _minIsZero true si le min doit etre 0
   * @return le validator
   */
  public static CtuluValueValidator getValidator(final int _nbChar, final int _maxFractionDigits, final boolean _minIsZero) {
    return new CtuluNumberFormatFortran(_nbChar).createValueValidator(_minIsZero);
  }

  /**
   * A voir si le calcul est tout le temps correct : probl�me d'arrondi.
   *
   * @param _nbChar le nombre total de caractere
   * @param _maxFractionDigits le nombre de chiffres apres la virgule.
   * @return le validator
   */
  public static CtuluValueValidator getValidator(final int _nbChar, final int _maxFractionDigits) {
    return getValidator(_nbChar, _maxFractionDigits, false);
  }

  public static CtuluNumberFormater getFormater(final int _nbChar, final int _maxFractionDigits) {
    return getFormater(_nbChar, _maxFractionDigits, false);
  }

  public static CtuluNumberFormater getFormater(final int _nbChar, final int _maxFractionDigits, final boolean _isPos) {
    final CtuluNumberFormatFortran fmt = getFortranFormat(_nbChar);
    return new CtuluNumberFormater(fmt, fmt.createValueValidator(_isPos));
  }

  /**
   * @deprecated Use {@link #getFortranFormat(int)} or {@link #getImprovedFortranFormat(int, int)} instead
   */
  public static CtuluNumberFormatFortran getFortranFormat(final int _nbCharTotal, final int _nbFractionDigits) {
    return new CtuluNumberFormatFortran(_nbCharTotal);
  }
  
  /**
   * permet de creer un format pour les double POSITIFS pour les champ "FX". X represente le nombre total de caractere
   * du champ.
   *
   * @param _nbCharTotal le 'X' : nombre total de car.
   * @return le formatter adapte
   */
  public static CtuluNumberFormatFortran getFortranFormat(final int _nbCharTotal) {
    return new CtuluNumberFormatFortran(_nbCharTotal);
  }
  
  /**
   * permet de creer un format pour les double POSITIFS pour les champ "FX.Y". X represente le nombre total de caractere
   * du champ. Y represente le nombre de chiffre apres la virgule
   *
   * @param _nbCharTotal le 'X' : nombre total de car.
   * @param _nbFractionDigits le 'Y' nombre de chiffre apres la virgule
   * @return le formatter adapte
   */
  public static CtuluNumberFormatFortran getImprovedFortranFormat(final int _nbCharTotal, final int _nbFractionDigits) {
    return new CtuluNumberFormatFortran(_nbCharTotal,_nbFractionDigits);
  }

  /**
   * @param _r le flux a fermer
   * @return le message d'erreur ou null si tout ok.
   */
  public static String closeState(final FortranInterface _r) {
    final IOException r = close(_r);
    if (r != null) {
      return CtuluLib.getS("Erreur lors de la fermeture du flux") + CtuluLibString.LINE_SEP + r.getMessage();
    }
    return null;
  }

  /**
   * @param _out le flux a fermer
   * @return le message d'erreur ou null si tout ok.
   */
  public static String closeState(final Writer _out) {
    return closeState(getFortranInterface(_out));
  }

  /**
   * @param _r le flux a transformer dans la bonne interface
   * @return l'interface englobant le flux _r.
   */
  public static FortranInterface getFortranInterface(final InputStream _r) {
    return new FortranInterface() {
      @Override
      public void close() throws IOException {
        _r.close();
      }
    };
  }

  /**
   * @param _r le flux a transformer dans la bonne interface
   * @return l'interface englobant le flux _r.
   */
  public static FortranInterface getFortranInterface(final OutputStream _r) {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        _r.close();
      }
    };
  }

  /**
   * @param _r le flux a transformer dans la bonne interface
   * @return l'interface englobant le flux _r.
   */
  public static FortranInterface getFortranInterface(final Reader _r) {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        _r.close();
      }
    };
  }

  /**
   * @param _r le flux a transformer dans l'interface qui va bien
   * @return l'interface englobant _r.
   */
  public static FortranInterface getFortranInterface(final Writer _r) {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        _r.close();
      }
    };
  }
}