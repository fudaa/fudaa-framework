/*
 * @creation 22 ao�t 2003
 * @modification $Date: 2007-01-26 14:57:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.fileformat;

import java.io.File;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
/**
 * @author deniger
 * @version $Id: FileWriteOperationAbstract.java,v 1.8 2007-01-26 14:57:26 deniger Exp $
 */
public abstract class FileOperationWriterAbstract<T> extends FileReadWriteAbstract<T> {

  protected abstract void internalWrite(T _o);

  /**
   * @param _o la source pour l'ecriture
   * @return la synthese de l'operation
   */
  public final CtuluIOResult<T> write(final T _o){
    internalWrite(_o);
    return closeOperation(_o);
  }

  /**
   * @param _o
   * @param _f
   * @param _inter
   * @return la synthese de l'ecriture
   */
  public final CtuluIOResult<T> write(final T _o,final File _f,final ProgressionInterface _inter){
    setFile(_f);
    final String s = getOperationDescription(_f);
    analyze_.setDesc(s);
    if (_inter != null) {
      _inter.setDesc(s);
    }
    setProgressReceiver(_inter);
    return write(_o);
  }

  /**
   *
   */
  protected String getOperationDescription(final File _f){
    return CtuluLib.getS("Ecriture") + CtuluLibString.ESPACE + _f.getName();
  }

  protected void donneesInvalides(final T _o){
    analyze_.addSevereError(CtuluLib.getS("Donn�es invalides"));
    analyze_.addInfo(_o==null?"null":_o.getClass().toString(), 0);
  }
}