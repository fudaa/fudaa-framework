/*
 * @creation 27 mai 2005
 * 
 * @modification $Date: 2006-09-19 14:36:54 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.nfunk.jep.*;
import org.nfunk.jep.function.Add;
import org.nfunk.jep.type.NumberFactory;

/**
 * @author Fred Deniger
 * @version $Id: CtuluParser.java,v 1.6 2006-09-19 14:36:54 deniger Exp $
 */
public class CtuluParser extends JEP {

  /**
   * L'operateur "+" est red�fini pour que Add(String,Object) -> String. L'operateur par d�faut d�clenche une exception.
   */
  private class CtuluOperationAdd extends Add {

    @Override
    public Object add(Object param1, Object param2) throws ParseException {
      if ((param1 instanceof String) || (param2 instanceof String)) {
        return add(param1.toString(), param2.toString());
      } else {
        return super.add(param1, param2);
      }
    }

    /**
     * Ajout de 2 string.
     * 
     * @param _param1 La premiere string
     * @param _param2 La deuxieme string.
     * @return La chaine r�sultante.
     */
    public String add(String _param1, String _param2) {
      return _param1 + _param2;
    }
  }

  /**
   * Le set des op�rations est surcharg� pour int�grer la nouvelle m�thode Add().
   */
  private class CtuluOperatorSet extends OperatorSet {
    public CtuluOperatorSet() {
      OP_ADD = new Operator("+", new CtuluOperationAdd());
    }
  }

  public static Variable addOldVar(final CtuluExpr _expr) {
    return _expr.addVar(CtuluParser.getOldVariable(), CtuluLib.getS("L'ancienne valeur de la variable"));
  }

  public static String getOldVariable() {
    return "old";
  }

  /**
   * @param _v le visiteur
   * @param _n le noeud a parcourir entierement.
   */
  public static void visitAll(final ParserVisitor _v, final Node _n) {
    if (_n == null) {
      return;
    }
    try {
      _n.jjtAccept(_v, null);
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    final int nb = _n.jjtGetNumChildren();
    for (int i = 0; i < nb; i++) {
      visitAll(_v, _n.jjtGetChild(i));
    }
  }

  String lastExpr_;

  public CtuluParser() {
    super();
    opSet = new CtuluOperatorSet();
  }

  /**
   * @param _traverseIn
   * @param _allowUndeclaredIn
   * @param _implicitMulIn
   * @param _numberFactoryIn
   */
  public CtuluParser(final boolean _traverseIn, final boolean _allowUndeclaredIn, final boolean _implicitMulIn, final NumberFactory _numberFactoryIn) {
    super();
    setTraverse(_traverseIn);
    setAllowUndeclared(_allowUndeclaredIn);
    setImplicitMul(_implicitMulIn);
    super.numberFactory = _numberFactoryIn;
    opSet = new CtuluOperatorSet();
  }

  /**
   * @param _j
   */
  public CtuluParser(final JEP _j) {
    this(_j.getTraverse(), _j.getAllowUndeclared(), _j.getImplicitMul(), _j.getNumberFactory());
    final SymbolTable table = _j.getSymbolTable();
    if (table != null) {
      super.symTab = new SymbolTable(table.getVariableFactory());
      for (final Iterator it = table.values().iterator(); it.hasNext();) {
        final Variable v = (Variable) it.next();
        if (v.isConstant()) {
          symTab.addConstant(v.getName(), v.getValue());
        } else {
          symTab.addVariable(v.getName(), v.getValue());
        }
      }
      final FunctionTable funcTable = _j.getFunctionTable();
      if (funcTable != null) {
        super.funTab = new FunctionTable();
        for (final Iterator it = funcTable.entrySet().iterator(); it.hasNext();) {
          final Map.Entry e = (Map.Entry) it.next();
          funTab.put(e.getKey(), e.getValue());
        }
      }
    }
  }

  public void clearUnusedVar() {
    final Variable[] vs = findUsedVarAndConstant();
    if (vs != null) {
      final String[] names = new String[vs.length];
      for (int i = names.length - 1; i >= 0; i--) {
        names[i] = vs[i].getName();
      }
      Arrays.sort(names);
      for (final Iterator it = super.symTab.keySet().iterator(); it.hasNext();) {
        final String s = (String) it.next();
        if (Arrays.binarySearch(names, s) < 0) {
          it.remove();
        }
      }

    }
  }

  /**
   * Retourne le tableau des variables utilis�es dans la derniere expression pars�e.
   * 
   * @return Le tableau, de longueur 0 si aucune variable dans l'expression.
   */
  public Variable[] findUsedVar() {
    final CtuluExprVarCollector coll = new CtuluExprVarCollector();
    coll.findOnlyVar(true);
    visitAll(coll, getTopNode());
    return coll.getFoundVar();
  }

  /**
   * Retourne le tableau des variables et constantes utilis�es dans la derniere expression pars�e.
   * 
   * @return Le tableau, de longueur 0 si aucune variable/constante dans l'expression.
   */
  public Variable[] findUsedVarAndConstant() {
    final CtuluExprVarCollector coll = new CtuluExprVarCollector();
    visitAll(coll, getTopNode());
    return coll.getFoundVar();
  }

  public String getError(final int _i) {
    return (String) errorList.get(_i);
  }

  public String getHtmlError() {
    final StringBuffer buf = new StringBuffer();
    buf.append("<html><body>");
    final int nb = getNbError();
    for (int i = 0; i < nb; i++) {
      if (i > 0) {
        buf.append("<br>");
      }
      buf.append(getError(i));
    }
    return buf.append("</body></html>").toString();
  }

  /**
   * @return La derni�re expression pars�e ou null si aucun parsing effectu�.
   */
  public final String getLastExpr() {
    return lastExpr_;

  }

  public int getNbError() {
    return hasError() ? super.errorList.size() : 0;
  }

  /**
   * Surcharge pour conserver la derni�re expression.
   * 
   * @param _expression L'expression � evaluer.
   */
  @Override
  public Node parseExpression(final String _expression) {
    lastExpr_ = _expression;
    return super.parseExpression(_expression);
  }

}
