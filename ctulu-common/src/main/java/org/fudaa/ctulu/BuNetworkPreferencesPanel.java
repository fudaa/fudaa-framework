/*
 * @modification $Date: 2006-09-19 14:36:53 $
 * @statut       unstable
 * @file         KorteNetworkPreferencesPanel.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    2000-2005 Guillaume Desnoix
 */
package org.fudaa.ctulu;

import com.memoire.bu.*;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.ScrollPaneConstants;

/**
 * A panel where the user can set the network options.
 *
 * network.connected
 */
public class BuNetworkPreferencesPanel extends BuAbstractPreferencesPanel implements ActionListener {

  /* private static final String[] SPEED_ITEMS  = { "14kbs RTC", "28kbs", "56kbs", "92kbs", "128kbs RNIS", "256kbs",
   "512kbs", "1Mbs", "2Mbs", "8Mbs", "16Mbs WAN"};
   private static final int[]    SPEED_VALUES = { 14400, 28800, 56700, 92000, 128000, 256000, 512000, 1024000, 2048000,
   8192000, 16536000                      };*/

  transient BuPreferences     options_;
  Hashtable         optionsStr_;
  BuOptionRenderer  cbRenderer_;

  //BuComboBox        chConnection_, chSpeed_;

  //  BuCheckBox                    cbSocksProxyActivated_, cbHttpProxyActivated_, cbFtpProxyActivated_;
  BuCheckBox        cbHttpProxyActivated_;
  //BuTextField                   tfSocksProxyHost_, tfSocksProxyPort_, tfSocksProxyUsername_;
  //  BuPasswordField               tfSocksProxyPassword_;
  BuTextField       tfHttpProxyHost_;
  BuTextField tfHttpProxyPort_;
  BuTextField tfHttpNonProxyHosts_;
  //BuTextField                   tfFtpProxyHost_, tfFtpProxyPort_, tfFtpNonProxyHosts_;

  /*  BuLabel                       lbConnection_, lbSpeed_, lbSocksProxyHost_, lbHttpProxyHost_, lbFtpProxyHost_,
   lbSocksProxyPort_, lbHttpProxyPort_, lbFtpProxyPort_, lbSocksProxyUserName_, lbSocksProxyPassword_,
   lbHttpNonProxyHosts_, lbFtpNonProxyHosts_;*/
  BuLabel           lbHttpProxyHost_;
  BuLabel lbHttpProxyPort_;
  BuLabel lbHttpNonProxyHosts_;

  @Override
  protected String getS(final String _s){
    return CtuluResource.CTULU.getString(_s);
  }

  @Override
  public String getTitle(){
    return getS("R�seau");
  }

  @Override
  public String getCategory(){
    return getS("Syst�me");
  }

  // Constructeur

  public BuNetworkPreferencesPanel() {
    super();
    options_ = BuPreferences.BU;
    cbRenderer_ = new BuOptionRenderer();

    setLayout(new BuBorderLayout(5, 5));
    setBorder(EMPTY5555);

    final BuMultiFormLayout mflo = new BuMultiFormLayout(5, 5, BuFormLayout.LAST, BuFormLayout.NONE,
      new int[] { 0}, null);


    // Http
    cbHttpProxyActivated_ = new BuCheckBox("HTTP " + getS("Proxy Server"));
    cbHttpProxyActivated_.addActionListener(this);
    final BuPanel pnHttp = new BuTitledPanel(cbHttpProxyActivated_, mflo);

    lbHttpProxyHost_ = new BuLabel(getS("Proxy host:"));
    tfHttpProxyHost_ = BuTextField.createHostField();
    tfHttpProxyHost_.addKeyListener(this);
    lbHttpProxyPort_ = new BuLabel(getS("Proxy port:"));
    tfHttpProxyPort_ = BuTextField.createPortField();
    tfHttpProxyPort_.addKeyListener(this);
    lbHttpNonProxyHosts_ = new BuLabel(getS("Non proxy hosts:"));
    tfHttpNonProxyHosts_ = new BuTextField();
    tfHttpNonProxyHosts_.addKeyListener(this);

    pnHttp.add(lbHttpProxyHost_, BuFormLayout.constraint(0, 0, false, 1.f));
    pnHttp.add(tfHttpProxyHost_, BuFormLayout.constraint(1, 0));
    pnHttp.add(lbHttpProxyPort_, BuFormLayout.constraint(0, 1, false, 1.f));
    pnHttp.add(tfHttpProxyPort_, BuFormLayout.constraint(1, 1));
    pnHttp.add(lbHttpNonProxyHosts_, BuFormLayout.constraint(0, 2, false, 1.f));
    pnHttp.add(tfHttpNonProxyHosts_, BuFormLayout.constraint(1, 2));


    final  BuPanel proxies = new BuPanel(new BuVerticalLayout(2));
    proxies.setBorder(EMPTY5555);
    //proxies.add(pnSocks_);
    proxies.add(pnHttp);
    //proxies.add(pnFtp_);
   final  BuScrollPane spProxies = new BuScrollPane(proxies);
    spProxies.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    spProxies.setPreferredSize(new Dimension(200, 150));

    add(spProxies, BuBorderLayout.CENTER);
    updateComponents();
  }

  // Evenements

  @Override
  public void actionPerformed(final ActionEvent _evt){
    setDirty(true);
    updateAbility();
  }

  // Methodes publiques

  @Override
  public boolean isPreferencesValidable(){
    return true;
  }

  @Override
  public void validatePreferences(){
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable(){
    return true;
  }

  @Override
  public void applyPreferences(){
    fillTable();
    options_.applyNetwork();
  }

  @Override
  public boolean isPreferencesCancelable(){
    return true;
  }

  @Override
  public void cancelPreferences(){
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees

  private String e2n(final String _s){
    if ((_s != null) && _s.trim().equals("")) {
      return null;
    }
    return _s;
  }

  private void fillTable(){
   /* int i = chSpeed_.getSelectedIndex();
    if (i < 0)
      i = 2;
    options_.putIntegerProperty("network.speed", SPEED_VALUES[i]);

    Object o;

    o = chConnection_.getSelectedItem();
    o = (o == null) ? "sometimes" : optionsStr_.get(o);
    options_.putStringProperty("network.connected", o.toString());

    options_.putBooleanProperty("socks.proxy.activated", cbSocksProxyActivated_.isSelected());

    options_.putBooleanProperty("ftp.proxy.activated", cbFtpProxyActivated_.isSelected());

    options_.putStringProperty("socks.proxy.host", e2n(tfSocksProxyHost_.getText()));
    options_.putStringProperty("socks.proxy.port", e2n(tfSocksProxyPort_.getText()));
    options_.putStringProperty("socks.proxy.username", e2n(tfSocksProxyUsername_.getText()));
    options_.putStringProperty("socks.proxy.password", e2n(tfSocksProxyPassword_.getText()));*/
    options_.putBooleanProperty("http.proxy.activated", cbHttpProxyActivated_.isSelected());
    options_.putStringProperty("http.proxy.host", e2n(tfHttpProxyHost_.getText()));
    options_.putStringProperty("http.proxy.port", e2n(tfHttpProxyPort_.getText()));
    options_.putStringProperty("http.proxy.skip", e2n(tfHttpNonProxyHosts_.getText()));

   /* options_.putStringProperty("ftp.proxy.host", e2n(tfFtpProxyHost_.getText()));
    options_.putStringProperty("ftp.proxy.port", e2n(tfFtpProxyPort_.getText()));
    options_.putStringProperty("ftp.proxy.skip", e2n(tfFtpNonProxyHosts_.getText()));*/

    setDirty(false);
  }

  private void updateComponents(){
   /* int s = options_.getIntegerProperty("network.speed", 28800);
    chSpeed_.setSelectedIndex(2);
    for (int i = SPEED_VALUES.length - 1; i >= 0; i--)
      if (SPEED_VALUES[i] <= s) {
        chSpeed_.setSelectedIndex(i);
        break;
      }

    chConnection_.setSelectedItem(optionsStr_.get(options_.getStringProperty("network.connected", "sometimes")));

    cbSocksProxyActivated_.setSelected(options_.getBooleanProperty("socks.proxy.activated", false));*/
    cbHttpProxyActivated_.setSelected(options_.getBooleanProperty("http.proxy.activated", false));
    //cbFtpProxyActivated_.setSelected(options_.getBooleanProperty("ftp.proxy.activated", false));

   /* tfSocksProxyHost_.setText(options_.getStringProperty("socks.proxy.host"));
    tfSocksProxyPort_.setText(options_.getStringProperty("socks.proxy.port"));
    tfSocksProxyUsername_.setText(options_.getStringProperty("socks.proxy.username"));
    tfSocksProxyPassword_.setText(options_.getStringProperty("socks.proxy.password"));*/

    tfHttpProxyHost_.setText(options_.getStringProperty("http.proxy.host"));
    tfHttpProxyPort_.setText(options_.getStringProperty("http.proxy.port"));
    tfHttpNonProxyHosts_.setText(options_.getStringProperty("http.proxy.skip"));

    /*tfFtpProxyHost_.setText(options_.getStringProperty("ftp.proxy.host"));
    tfFtpProxyPort_.setText(options_.getStringProperty("ftp.proxy.port"));
    tfFtpNonProxyHosts_.setText(options_.getStringProperty("ftp.proxy.skip"));*/

    setDirty(false);
    setAbility();
  }

  @Override
  protected void updateAbility(){
    //chConnection_.setEnabled(true);

    boolean b;

   /* b = cbSocksProxyActivated_.isSelected();
    tfSocksProxyHost_.setEnabled(b);
    tfSocksProxyPort_.setEnabled(b);
    tfSocksProxyUsername_.setEnabled(b);
    tfSocksProxyPassword_.setEnabled(b);*/

    b = cbHttpProxyActivated_.isSelected();
    tfHttpProxyHost_.setEnabled(b);
    tfHttpProxyPort_.setEnabled(b);
    tfHttpNonProxyHosts_.setEnabled(b);

   /* b = cbFtpProxyActivated_.isSelected();
    tfFtpProxyHost_.setEnabled(b);
    tfFtpProxyPort_.setEnabled(b);
    tfFtpNonProxyHosts_.setEnabled(b);*/
  }
}
