/*
 * @creation 20 fevr. 2003
 * 
 * @modification $Date: 2007-05-21 10:28:29 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Cette classe sert a transmettre les informations d'une operation de lecture ou d'ecriture. Les informations et les erreurs de l'operation sont
 * stockees avec eventuellement leur numero de ligne.
 *
 * @author deniger
 * @version $Id: CtuluAnalyze.java,v 1.12 2007-05-21 10:28:29 deniger Exp $
 */
public class CtuluLog {

  ResourceBundle defaultResourceBundle;
  private String description = "Analyzing";
  private String[] descriptionArgs = null;
  protected final List<CtuluLogRecord> logs = new ArrayList<CtuluLogRecord>();
  protected final List<CtuluLogRecord> logsExt = Collections.unmodifiableList(logs);
  private String ressource;
  private boolean containsSevereError;
  private boolean containsError;

  /**
   * Constructeur par defaut
   */
  public CtuluLog() {
    super();
  }

  /**
   * Constructeur precisant le resourceBundle a utiliser
   *
   * @param defaultResourceBundle
   */
  public CtuluLog(final ResourceBundle defaultResourceBundle) {
    super();
    this.defaultResourceBundle = defaultResourceBundle;
  }

  public void updateLocalizedMessage(ResourceBundle resourceBundle) {
    if (logs != null) {
      for (CtuluLogRecord ctuluLogRecord : logs) {
        CtuluLogRecord.updateLocalizedMessage(ctuluLogRecord, resourceBundle);
      }
    }
  }

  /**
   *
   * @return the higher level contained in this log.
   */
  public CtuluLogLevel getHigherLevel() {
    CtuluLogLevel higher = null;
    for (CtuluLogRecord ctuluLogRecord : logs) {
      CtuluLogLevel level = ctuluLogRecord.getLevel();
      if (higher == null || higher.isMoreVerboseThan(level, true)) {
        higher = level;
      }
    }
    return higher;
  }

  /**
   * @param msg le message
   */
  public void addError(final String msg) {
    addRecord(CtuluLogLevel.ERROR, msg);
  }

  /**
   * @param msg le message
   * @param e l'erreur
   */
  public void addError(final String msg, final Throwable e) {
    addRecord(CtuluLogLevel.ERROR, msg).setThrown(e);
  }

  /**
   * @param msg le message
   * @param data
   */
  public void addError(final String msg, Object... data) {
    addRecord(CtuluLogLevel.ERROR, msg, data);
  }

  /**
   * @param msg le message
   * @param data
   * @param e l'erreur
   */
  public void addErrorThrown(final String msg, final Throwable e, Object... data) {
    addRecord(CtuluLogLevel.ERROR, msg, data).setThrown(e);
  }

  /**
   * @param msg
   * @param i
   */
  public void addErrorFromFile(final String msg, final int i) {
    addRecord(CtuluLogLevel.ERROR, msg, Integer.valueOf(i));
  }

  /**
   * @param msg le message
   * @param reader le reader occasionnant l'erreur
   */
  public void addErrorFromFile(final String msg, final LineNumberReader reader) {
    addRecord(CtuluLogLevel.ERROR, msg, reader.getLineNumber());
  }

  /**
   * @param msg le message
   * @param data les donnees complementaire
   * @param i le numero de ligne de l'erreur
   */
  public void addErrorFromFile(final String msg, final int i, Object... data) {
    if (data == null) {
      addRecord(CtuluLogLevel.ERROR, msg, i);
    } else {
      Object[] all = new Object[data.length + 1];
      System.arraycopy(data, 0, all, 1, data.length);
      all[0] = i;
      addRecord(CtuluLogLevel.ERROR, msg, all);
    }
  }

  /**
   * @param msg le message
   * @param data les donnees complementaire
   * @param reader le reader occasionnant l'erreur
   */
  public void addErrorFromFile(final String msg, final LineNumberReader reader, Object... data) {
    addErrorFromFile(msg, reader.getLineNumber(), data);
  }

  /**
   * @param _m le message
   */
  public CtuluLogRecord addSevereError(final String _m) {
    return addRecord(CtuluLogLevel.SEVERE, _m);
  }

  /**
   * Initialise le champ "erreur fatale". Si une erreur fatale a deja ete initialisee, ne fait rien.
   *
   * @param _m le message
   * @param _index le num de ligne
   */
  public CtuluLogRecord addSevereError(final String _m, final int _index) {
    return addRecord(CtuluLogLevel.SEVERE, _m, Integer.valueOf(_index));
  }

  /**
   * @param _m
   * @param arg
   */
  public CtuluLogRecord addSevereError(final String _m, final Object... arg) {
    return addRecord(CtuluLogLevel.SEVERE, _m, arg);
  }

  /**
   * Initialise le champ "erreur fatale". Si une erreur fatale a deja ete initialisee, ne fait rien.
   *
   * @param _m le message
   * @param _in pour recuperer le num de ligne
   */
  public CtuluLogRecord addSevereError(final String _m, final LineNumberReader _in) {
    return addSevereError(_m, _in == null ? -1 : _in.getLineNumber());
  }

  /**
   * @param msg le message d'info a ajouter
   */
  public CtuluLogRecord addInfo(final String msg) {
    return addRecord(CtuluLogLevel.INFO, msg);
  }

  /**
   * @param msg
   * @param args
   */
  public CtuluLogRecord addInfo(final String msg, final Object... args) {
    return addRecord(CtuluLogLevel.INFO, msg, args);
  }

  /**
   * @param msg le message d'info a ajouter
   * @param _index l'index identifiant le message (peut etre un numero de ligne)
   */
  public CtuluLogRecord addInfoFromFile(final String msg, final int _index) {
    return addRecord(CtuluLogLevel.INFO, msg, _index);
  }

  /**
   * @param msg
   * @param i
   * @param data
   */
  public CtuluLogRecord addInfoFromFile(final String msg, final int i, Object... data) {
    return addRecord(CtuluLogLevel.INFO, msg, i, data);
  }

  /**
   * @param msg le message d'info a ajouter
   * @param _in le reader permettant de recuperer le numero de ligne
   */
  public CtuluLogRecord addInfoFromFile(final String msg, final LineNumberReader _in) {
    return addRecord(CtuluLogLevel.INFO, msg, _in.getLineNumber());
  }

  /**
   * @param level le level
   * @param msg le message
   * @param data les donnees complementaire
   * @return le logRecord ajoute
   */
  public CtuluLogRecord addRecord(final CtuluLogLevel level, final String msg, Object... data) {
    final CtuluLogRecord o = new CtuluLogRecord(level, msg);
    updatePrivateFlags(level);
    o.setArgs(data);
    logs.add(o);
    return o;
  }

  public void addRecord(CtuluLogRecord record) {
    if (record != null) {
      updatePrivateFlags(record.getLevel());
      logs.add(record);
    }
  }

  /**
   * @param msg le message d'avertissement
   */
  public CtuluLogRecord addWarn(final String msg) {
    return addRecord(CtuluLogLevel.WARNING, msg);
  }

  /**
   * @param msg le message d'avertissement
   * @param in le reader permettant de recuperer le numero de ligne
   */
  public CtuluLogRecord addWarn(final String msg, final LineNumberReader in) {
    return addRecord(CtuluLogLevel.WARNING, msg, in.getLineNumber());
  }

  /**
   * @param msg le message d'avertissement
   * @param args les arguments supplementaires
   */
  public CtuluLogRecord addWarn(final String msg, final Object... args) {
    return addRecord(CtuluLogLevel.WARNING, msg, args);
  }

  /**
   * @param msg le message d'avertissement
   * @param index le numero identifiant le message
   */
  public CtuluLogRecord addWarnFromFile(final String msg, final int index) {
    return addRecord(CtuluLogLevel.WARNING, msg, index);
  }

  /**
   * @param _m
   */
  @Deprecated
  public void changeSevereError(final String _m) {
    addError(_m);
  }

  /**
   * Efface tous les canaux.
   */
  public void clear() {
    logs.clear();
    containsSevereError = false;
    containsError = false;
  }

  public boolean containsErrorOrSevereError() {
    return containsErrors() || containsSevereError();
  }

  /**
   * @return true si contient au moins une erreur
   */
  public boolean containsErrors() {
    return containsError;
  }

  /**
   * @return true si contient une erreur fatale
   */
  public boolean containsSevereError() {
    //otpimisation
    return containsSevereError;
  }

  /**
   * @return true si contient au moins une info
   */
  public boolean containsInfos() {
    return containsLevel(CtuluLogLevel.INFO);
  }

  public int getNbOccurence(CtuluLogLevel level) {
    int count = 0;
    for (final CtuluLogRecord log : logs) {
      if (level.equals(log.getLevel())) {
        count++;
      }
    }
    return count;

  }

  /**
   * @param l le level a tester
   * @return true si au moins un message et de niveau l.
   */
  public boolean containsLevel(final CtuluLogLevel l) {
    int nb = logs.size();
    for (int i = 0; i < nb; i++) {
      final CtuluLogRecord log = logs.get(i);
      if (l.equals(log.getLevel())) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return true si contient au moins un avertissement
   */
  public boolean containsWarnings() {
    return containsLevel(CtuluLogLevel.WARNING);
  }

  /**
   * @return the defaultResourceBundle
   */
  public ResourceBundle getDefaultResourceBundle() {
    return defaultResourceBundle;
  }

  /**
   * @return la description de l'analyse en cours
   */
  public String getDesc() {
    return description;
  }

  /**
   * @return l'ensemble des enregistrements de log
   */
  public List<CtuluLogRecord> getRecords() {
    return logsExt;
  }

  /**
   * @return chaine decrivant la ressource ( fichier, url, ...)
   */
  public String getResource() {
    return ressource;
  }

  /**
   * @return le resume de l'analyse
   */
  public String getResume() {
    return getDesc() + "\n" + CtuluDefaultLogFormatter.formatLogs(logs, getDefaultResourceBundle());
  }

  public String getSevereError() {
    CtuluLogRecord log = getFirstOfLevel(CtuluLogLevel.SEVERE);
    return log == null ? null : CtuluDefaultLogFormatter.DEFAULT.format(log, getDefaultResourceBundle());
  }

  /**
   * @param l le level a tester
   * @return true si au moins un message et de niveau l.
   */
  private CtuluLogRecord getFirstOfLevel(final CtuluLogLevel l) {
    for (final CtuluLogRecord log : logs) {
      if (l.equals(log.getLevel())) {
        return log;
      }
    }
    return null;
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return logs.isEmpty();
  }

  public boolean isNotEmpty() {
    return !isEmpty();
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   */
  public void manageException(final Exception _e) {
    manageException(_e, "exception.occured");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   * @param msg le message
   */
  public void manageException(final Exception _e, final String msg) {
    addRecord(CtuluLogLevel.ERROR, msg).setThrown(_e);
    _e.printStackTrace();
  }

  /**
   * Ajoute un message d'erreur.
   *
   * @param _e l'exception a loguer
   */
  public void manageException(final FileNotFoundException _e) {
    manageException(_e, "exception.FileNotFoundException");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   */
  public void manageException(final IllegalArgumentException _e) {
    manageException(_e, "exception.IllegalArgumentException");
  }

  /**
   * Ajoute un message d'erreur.
   *
   * @param _e l'exception a loguer
   */
  public void manageException(final IOException _e) {
    manageException(_e, "exception.IOException");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   * @param _l le numero de ligne
   */
  public void manageException(final NumberFormatException _e, final int _l) {
    manageException(_e, "exception.NumberFormatException");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   * @param msg le message
   * @param line la ligne
   */
  public void manageExceptionFromFile(final Exception _e, final String msg, final int line) {
    addRecord(CtuluLogLevel.ERROR, msg, line).setThrown(_e);
  }

  public String getDesci18n() {
    if (defaultResourceBundle == null) {
      return description;
    }
    if (description == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    try {
      final String i18n = defaultResourceBundle.getString(description);
      if (descriptionArgs != null) {
        return java.text.MessageFormat.format(i18n, (Object[]) descriptionArgs);
      }
      return i18n;
    } catch (MissingResourceException ex) {
      //do nothing...
    }
    return description;
  }

  /**
   * Ajoute tous les canaux de l'analyse passee en parametres. Les pointeurs sont copiees.
   *
   * @param _analyze l'analyse a ajouter a celle-ci
   */
  public void merge(final CtuluLog _analyze) {
    setDesc(_analyze.getDesc());
    setDescriptionArgs(_analyze.getDescriptionArgs());
    setDefaultResourceBundle(_analyze.getDefaultResourceBundle());
    setResource(_analyze.getResource());
    logs.addAll(_analyze.logs);
    updatePrivateFlags(_analyze);
  }

  /**
   *
   * @param log tous les records de <code>log</code> sont copies dans ce log.
   */
  public void addAllLogRecord(final CtuluLog logToCopyFrom) {
    if (logToCopyFrom != null) {
      logs.addAll(logToCopyFrom.logs);
      updatePrivateFlags(logToCopyFrom);
    }
  }

  /**
   * Permet d'ecrire sur la sortie standart le contenu de cette analyse.
   */
  public void printResume() {
    System.err.println(getResume());// NOPMD
  }

  /**
   * @param defaultResourceBundle the defaultResourceBundle to set
   */
  public void setDefaultResourceBundle(final ResourceBundle defaultResourceBundle) {
    this.defaultResourceBundle = defaultResourceBundle;
  }

  /**
   * @param _string la nouvelle description de l'analyse
   */
  public void setDesc(final String _string) {
    description = _string;
  }

  public String[] getDescriptionArgs() {
    return descriptionArgs;
  }

  public void setDescriptionArgs(String... descriptionArgs) {
    this.descriptionArgs = descriptionArgs;
  }

  /**
   * @param _string la nouvelle ressource de l'analyse
   */
  public void setResource(final String _string) {
    ressource = _string;
  }

  private void updatePrivateFlags(final CtuluLogLevel level) {
    if (CtuluLogLevel.SEVERE.equals(level)) {
      containsSevereError = true;
    } else if (CtuluLogLevel.ERROR.equals(level)) {
      containsError = true;
    }
  }

  private void updatePrivateFlags(final CtuluLog _analyze) {
    if (_analyze.containsError) {
      containsError = true;
    }
    if (_analyze.containsSevereError) {
      containsSevereError = true;
    }
  }
}
