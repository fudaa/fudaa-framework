package org.fudaa.ctulu;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Permet de savoir quel
 *
 * @author deniger ( genesis)
 */
public class FileDeleteResult {

  private final List<File> filesNotDeleted = new ArrayList<File>();
  private final List<File> dirNotDeleted = new ArrayList<File>();

  public List<File> getDirNotDeleted() {
    return dirNotDeleted;
  }

  public boolean isNotEmpty() {
    return !isEmpty();
  }

  public boolean isEmpty() {
    return filesNotDeleted.isEmpty() && dirNotDeleted.isEmpty();
  }

  public List<File> getFilesNotDeleted() {
    return filesNotDeleted;
  }

  public void addNotDeleted(File f) {
    if (f == null) {
      return;
    }
    if (f.isFile()) {
      filesNotDeleted.add(f);
    } else {
      dirNotDeleted.add(f);
    }
  }
}
