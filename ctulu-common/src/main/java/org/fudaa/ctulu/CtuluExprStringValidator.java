/*
 *  @creation     2 ao�t 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuStringValidator;

/**
 * Un validateur de string, utilis� pour v�rifier que le format saisi est correct. Il
 * peut g�rer une string sous forme de formule, ou sous forme standard.
 *
 * @author Fred Deniger
 * @version $Id: CtuluExprStringValidator.java,v 1.4 2006-09-19 14:36:53 deniger Exp $
 */
public abstract class CtuluExprStringValidator extends BuStringValidator {

  final CtuluExpr expr_;
  boolean isEmptyAccepted_ = true;
  /** True : Une expression est attendue */
  boolean exprIsExpected_=true;

  /**
   * Expression construite par defaut.
   */
  public CtuluExprStringValidator() {
    this(new CtuluExpr());
  }

  /**
   * @param _expr expression : non null.
   */
  public CtuluExprStringValidator(final CtuluExpr _expr) {
    this(_expr,true);
  }
  
  public CtuluExprStringValidator(final CtuluExpr _expr, boolean _exprExpected) {
    expr_ = _expr;
    exprIsExpected_=_exprExpected;
  }

  /**
   * @return l'exp
   */
  public final CtuluExpr getExpr(){
    return expr_;
  }

  public final boolean isEmptyAccepted(){
    return isEmptyAccepted_;
  }

  @Override
  public boolean isStringValid(final String _string){
    if (_string == null || _string.length() == 0) {
      return isEmptyAccepted_;
    }
    expr_.getParser().parseExpression(_string);
    return !(expr_.getParser().hasError());
  }

  public final void setEmptyAccepted(final boolean _isEmptyAccepted){
    isEmptyAccepted_ = _isEmptyAccepted;
  }
  
  /**
   * Definit que le validator attend une formule
   * @param _b True : Une formule est attendue, une valeur standard sinon.
   */
  public void setExpressionExpected(boolean _b) {
    exprIsExpected_=_b;
  }
  
  public boolean isExpressionExpected() {
    return exprIsExpected_;
  }

  @Override
  public String valueToString(final Object _value){
    if (_value instanceof CtuluExpr) {
      return ((CtuluExpr) _value).getLastExpr();
    }
    return _value == null ? CtuluLibString.EMPTY_STRING : _value.toString();
  }

}
