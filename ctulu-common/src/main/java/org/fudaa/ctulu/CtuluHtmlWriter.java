/*
 *  @creation     28 nov. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuResource;
import java.util.Stack;

/**
 * @author Fred Deniger
 * @version $Id: CtuluHtmlWriter.java,v 1.6 2006-09-19 14:36:53 deniger Exp $
 */
public class CtuluHtmlWriter {

  /**
   * @author Fred Deniger
   * @version $Id: CtuluHtmlWriter.java,v 1.6 2006-09-19 14:36:53 deniger Exp $
   */
  public final class Tag {

    private final String id_;

    /**
     * @param _id l'idendifiant du tag.
     */
    public Tag(final String _id) {
      super();
      id_ = _id;
    }

    Tag add() {
      buffer_.append("<" + id_ + ">");
      tag_.push(this);
      return this;
    }

    Tag add(final String _props) {
      return addStyleProps(null, _props);
    }

    Tag addStyleProps(final String _style, final String _props) {
      buffer_.append("<").append(id_);
      if (_props != null) {
        buffer_.append(CtuluLibString.ESPACE).append(_props);
      }
      if (_style != null) {
        buffer_.append(CtuluLibString.ESPACE).append("style=\"").append(_style).append("\"");
      }

      buffer_.append(">");
      tag_.push(this);
      return this;
    }

    Tag addCenter() {
      return add("align=\"center\"");
    }

    Tag addStyle(final String _style) {
      return addStyleProps(_style, null);
    }

    Tag writeClose() {
      buffer_.append("</" + id_ + ">");
      return this;
    }

    public void close() {
      Tag current = null;
      do {
        if (tag_.size() > 0) {
          current = removeCurrent();
          current.writeClose();
        }
      } while (current != this && tag_.size() >= 0);
    }
  }

  private BuResource defaultResource_;
  final StringBuffer buffer_ = new StringBuffer(100);
  final Stack tag_ = new Stack();

  public CtuluHtmlWriter() {
    this(false);
  }

  public CtuluHtmlWriter(final boolean _allTag) {
    this(_allTag, true);
  }

  public CtuluHtmlWriter(final boolean _allTag, final boolean _padding) {
    if (_allTag) {
      addTag("html");
      if (_padding) {
        addStyledTag("body", "padding:2px;");
      } else {
        addTag("body");
      }
    }
  }

  protected Tag getCurrent() {
    return (Tag) tag_.peek();
  }

  protected Tag removeCurrent() {
    return (Tag) tag_.pop();
  }

  public Tag addCenterTag(final String _tag) {
    return new Tag(_tag).addCenter();
  }

  public CtuluHtmlWriter addi18n(final String _seq) {
    if (defaultResource_ == null) {
      buffer_.append(_seq);
    } else {
      buffer_.append(defaultResource_.getString(_seq));
    }
    return this;
  }

  public final Tag addStyledTag(final String _tag, final String _style) {
    return new Tag(_tag).addStyle(_style);
  }

  public final Tag addTag(final String _tag) {
    return new Tag(_tag).add();
  }

  public CtuluHtmlWriter addTagAndText(final String _tag, final String _text) {
    final Tag r = new Tag(_tag).add();
    append(_text);
    r.close();
    return this;
  }

  public Tag addTag(final String _tag, final String _props) {
    return new Tag(_tag).add(_props);
  }

  public Tag addStyledTag(final String _tag, final String _style, final String _props) {
    return new Tag(_tag).addStyleProps(_style, _props);
  }

  public CtuluHtmlWriter addLink(final String _url) {
    buffer_.append("<a href=\"").append(_url).append("\">").append(_url).append("</a>");
    return this;
  }

  public CtuluHtmlWriter append(final CharSequence _seq) {
    buffer_.append(_seq);
    return this;
  }

  public void close() {
    if (tag_.size() > 0) {
      ((Tag) tag_.get(0)).close();
    }
  }

  public CtuluHtmlWriter close(final Tag _tag, final String _txt) {
    addi18n(_txt);
    _tag.close();
    return this;
  }

  public String getHtml() {
    close();
    return buffer_.toString();
  }

  public CtuluHtmlWriter h1(final String _title) {
    close(addTag("h1"), _title);
    return this;
  }

  public CtuluHtmlWriter h1Center(final String _title) {
    return h1Center(_title, false);
  }

  public CtuluHtmlWriter h1Center(final String _title, final boolean _underline) {
    final Tag h1 = addCenterTag("h1");
    if (_underline) {
      addTag("u");
    }
    addi18n(_title);
    h1.close();
    return this;
  }

  public CtuluHtmlWriter h2(final String _title) {
    close(addTag("h2"), _title);
    return this;
  }

  public CtuluHtmlWriter h2Styled(final String _title, final String _style) {
    close(addStyledTag("h2", _style), _title);
    return this;
  }

  public CtuluHtmlWriter h3(final String _title) {
    close(addTag("h3"), _title);
    return this;
  }

  public CtuluHtmlWriter h3Styled(final String _title, final String _style) {
    close(addStyledTag("h3", _style), _title);
    return this;
  }

  public CtuluHtmlWriter h4Styled(final String _title, final String _style) {
    close(addStyledTag("h4", _style), _title);
    return this;
  }

  public CtuluHtmlWriter h2Center(final String _title) {
    close(addCenterTag("h2"), _title);
    return this;
  }

  public boolean isEmpty() {
    return tag_.isEmpty();
  }

  public CtuluHtmlWriter nl() {
    buffer_.append("<br>");
    return this;
  }

  @Override
  public String toString() {
    return getHtml();
  }

  public Tag para() {
    return new Tag("p").add();
  }

  public Tag paraCenter() {
    return new Tag("p").addCenter();
  }

  public Tag paraStyled(final String _style) {
    return new Tag("p").addStyle(_style);
  }

  public final void setDefaultResource(final BuResource _defaultResource) {
    defaultResource_ = _defaultResource;
  }

}
