/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2007-01-17 10:45:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;


/**
 * Utiliser car BuTaskOperation recupere les exceptions qui pourraient etre utilisees par d' autres m�thodes.
 */
public class CtuluTaskOperationDefault extends CtuluTaskOperationAbstract {

  ProgressionInterface prog_;

  @Override
  public ProgressionInterface getStateReceiver() {
    if (prog_ == null) {
      prog_ = new ProgressionFuAdapter(getName());
    }
    return prog_;
  }

  @Override
  public ProgressionInterface getMainStateReceiver() {
    return getStateReceiver();
  }

  public CtuluTaskOperationDefault(final String _nom) {
    super(_nom);
  }

}
