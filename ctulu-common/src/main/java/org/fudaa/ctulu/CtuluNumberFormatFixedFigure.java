/*
 *  @creation     12 janv. 2006
 *  @modification $Date: 2007-04-02 08:55:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.ostermiller.util.SignificantFigures;

/**
 * Un formatter pour les nombre fortran qui doivent etre codes sur n caract�res.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluNumberFormatFixedFigure.java,v 1.2 2007-04-02 08:55:34 deniger Exp $
 */
public class CtuluNumberFormatFixedFigure extends CtuluNumberFormat {

  double maxBorne_ = 1E6;

  final int maxFigure_;

  double minBorne_ = 1E-6;

  /**
   * @param _maxChar
   */
  public CtuluNumberFormatFixedFigure(final int _maxChar) {
    super();
    maxFigure_ = _maxChar;
  }

  boolean mustBeTransformToScien(double _d) {
    return _d >= maxBorne_ || _d <= -maxBorne_ || (_d <= minBorne_ && _d >= -minBorne_);
  }

  protected boolean isExp(final String _s) {
    return _s.indexOf('E') >= 0;
  }

  @Override
  public String format(final double _d) {
    String s = Double.toString(_d);
    if (s.length() > maxFigure_) {

      final SignificantFigures fig = new SignificantFigures(_d);
      fig.setNumberSignificantFigures(maxFigure_);
      if (mustBeTransformToScien(_d)) {
        s = CtuluNumberFormatFortran.removeZeroAtEndScien(fig.toScientificNotation());
      } else {
        s = fig.toString();
        // il se peut que le retour soit une �criture scientifique.
        if (CtuluNumberFormatFortran.isExp(s)) s = CtuluNumberFormatFortran.removeZeroAtEndScien(s);
        else
          s = CtuluNumberFormatFortran.removeZeroAtEnd(s, true);
      }
    }
    else if(CtuluNumberFormatFortran.isExp(s)){
      s=CtuluNumberFormatFortran.removeZeroAtEndScien(s);
    }
    else s=CtuluNumberFormatFortran.removeZeroAtEnd(s, true);
    return s;
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }

  public int getMaxChar() {
    return maxFigure_;
  }

  @Override
  public String toLocalizedPattern() {
    return CtuluNumberFormatBuilder.getPrefixForFixed() + CtuluLibString.getString(maxFigure_);
  }

}
