/*
 * @creation 13 d�c. 06
 * @modification $Date: 2007-06-05 08:57:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.swing.Icon;
import javax.swing.ImageIcon;


/**
 * @author fred deniger
 * @version $Id: CtuluLibImage.java,v 1.12 2007-06-05 08:57:44 deniger Exp $
 */
public final class CtuluLibImage {
  /**
   * Le nom de la commande pour exporter une vue d'�cran dans un fichier. Utilis�e pour les commandes de
   * BuCommonImplementation.
   */
  public static String SNAPSHOT_COMMAND = "EXPORT_SNAPSHOT";

  /**
   * La commande utilis�e pour copier l'image dans le presse papier.
   */
  public static String SNAPSHOT_CLIPBOARD_COMMAND = "EXPORT_SNAPSHOT_CLIP";
  /**
   * cl� � utiliser dans la table params pour remplir le fond de l'image ou non: la valeur doit etre de type Boolean.<br>
   * Par exemple, cette option n'est pas n�cessaire lorsque l'on exporte en png, car support� par le format, par contre
   * cela est n�cessaire lors de l'export en jpg.
   */
  public static String PARAMS_FILL_BACKGROUND_BOOLEAN = "background.fill";
  /**
   * cl� � utiliser dans la table params pour demander au client de cr�er une image compatible avec l'�cran pour des
   * raisons d'optimisation.
   */
  public static String PARAMS_CREATE_SCREEN_COMPATIBLE_IMAGE = "image.compatible";

  private CtuluLibImage() {}

  public static float getRatio(final int _sup, final int _inf) {
    return ((float) _sup) / ((float) _inf);
  }

  public static boolean mustFillBackground(Map _imageParams) {
    return _imageParams == null || _imageParams.get(PARAMS_FILL_BACKGROUND_BOOLEAN) == Boolean.TRUE;
  }

  /**
   * @param _imageParams la tables des parametres: peut etre null.
   * @return true si le client doit cr�er une image compatible avec l'affichage de l'�cran
   * @see CtuluImageProducer#PARAMS_CREATE_SCREEN_COMPATIBLE_IMAGE
   */
  public static boolean isCompatibleImageAsked(Map _imageParams) {
    return _imageParams != null
        && _imageParams.get(PARAMS_CREATE_SCREEN_COMPATIBLE_IMAGE) == Boolean.TRUE;
  }

  public static void setCompatibleImageAsked(Map _imageParams) {
    if (_imageParams != null) {
      _imageParams.put(PARAMS_CREATE_SCREEN_COMPATIBLE_IMAGE, Boolean.TRUE);
    }
  }

  public static int getImageRGBType(Map _imageParams) {
    return mustFillBackground(_imageParams) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
  }

  public static void setFillBackground(Map _imageParams) {
    if (_imageParams != null) {
      _imageParams.put(PARAMS_FILL_BACKGROUND_BOOLEAN, Boolean.TRUE);
    }
  }

  public static void updateDimForMaxSize(final Dimension _dimToModify, final int _max) {
    if (_dimToModify == null) { return; }
    if (_dimToModify.height > _max || _dimToModify.width > _max) {
      final float ratio = getRatio(_dimToModify, _max);
      _dimToModify.height = (int) (_dimToModify.height * ratio);
      _dimToModify.width = (int) (_dimToModify.width * ratio);
    }

  }

  public static BufferedImage produceImageForComponent(final Component _c, final Map _params) {
    return produceImageForComponent(_c, _c.getWidth(), _c.getHeight(), _params);
  }

  /**
   * @param _c le compsant a imprimer
   * @param _w la largeur de l'image
   * @param _h la hauteur
   * @param _params
   * @return
   */
  public static BufferedImage produceImageForComponent(final Component _c, final int _w, final int _h, final Map _params) {
    BufferedImage i = createImage(_w, _h, _params);
    final Graphics2D g2d = i.createGraphics();
    CtuluLibImage.setBestQuality(g2d);
    if (CtuluLibImage.mustFillBackground(_params)) {
      g2d.setColor(Color.WHITE);
      g2d.fillRect(0, 0, _w, _h);
    }
    if (_c.getWidth() != _w || _c.getHeight() != _h) {
      g2d.scale(CtuluLibImage.getRatio(_w, _c.getWidth()), CtuluLibImage.getRatio(_h, _c.getHeight()));
    }

    // g2d.setClip(0, 0, _w, _h);
    _c.printAll(g2d);

    g2d.dispose();
    i.flush();
    return i;
  }

  public static BufferedImage createImage(final int _w, final int _h, final Map _params) {
    BufferedImage i = null;
    if (isCompatibleImageAsked(_params)) {
      try {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gs = ge.getDefaultScreenDevice();
        GraphicsConfiguration gc = gs.getDefaultConfiguration();
        // Create an image that does not support transparency
        i = gc.createCompatibleImage(_w, _h, Transparency.TRANSLUCENT);
      } catch (HeadlessException _evt) {
        FuLog.error(_evt);

      }
    }
    if (i == null) i = new BufferedImage(_w, _h, getImageRGBType(_params));
    return i;
  }

  public static float getRatio(final Dimension _dimToModify, final int _max) {
    return getRatio(_dimToModify.width, _dimToModify.height, _max);
  }

  public static float getRatio(final int _w, final int _h, final int _max) {
    return getRatio(_max, Math.max(_w, _h));
  }

  public static ImageReader getImageReader(final File _f, final CtuluAnalyze _analyze) {
    if (_f == null) { return null; }
    String extension = CtuluLibFile.getExtension(_f.getName());
    final Iterator it = ImageIO.getImageReadersByFormatName(extension);

    ImageReader res = null;
    if (it != null && it.hasNext()) {
      res = (ImageReader) it.next();
    }

    if (res == null && _analyze != null) {
      _analyze.addFatalError(CtuluLib.getS("Le format de l'image {0} n'est pas support�", _f.getName()));

    }
    boolean inputOk = true;
    if (res != null) {
      try {
        res.setInput(ImageIO.createImageInputStream(_f));
      } catch (final IOException _evt) {
        FuLog.error(_evt);
        inputOk = false;

      }
    }
    // il y a un bogue avec le plugin javaio sur certains fichiers jpg: on va
    // essayer de le virer
    if (res != null && inputOk) {
      try {
        res.getHeight(0);
        res.getWidth(0);
        // exception due au bogue
      } catch (Exception _evt) {
        if (it != null && it.hasNext()) {
          // on prend le prochain lecteur
          res = (ImageReader) it.next();
          if (res == null) inputOk = false;
          else {
            try {
              res.setInput(ImageIO.createImageInputStream(_f));
            } catch (final IOException _evt1) {
              FuLog.error(_evt1);
              inputOk = false;

            }
          }
          // vraiment une erreur
        } else inputOk = false;

      }
    }
    return inputOk ? res : null;
  }

  public static ImageWriter getImageWriter(final File _f) {
    if (_f == null) { return null; }
    final Iterator it = ImageIO.getImageWritersByFormatName(CtuluLibFile.getExtension(_f.getName()));
    ImageWriter res = null;
    if (it != null && it.hasNext()) {
      res = (ImageWriter) it.next();
    }

    boolean inputOk = true;
    if (res != null) {
      try {
        res.setOutput(ImageIO.createImageOutputStream(_f));
      } catch (final IOException _evt) {
        FuLog.error(_evt);
        inputOk = false;

      }
    }
    return inputOk ? res : null;
  }

  public static BufferedImage createImage(int _width, int _height, int _type) {
    return new BufferedImage(_width, _height, _type);
  }

  /**
   * SWINGX.
   * 
   * @param _initImage
   * @param _newWidth
   * @param _newHeight
   * @param _type
   * @return
   */
  private static BufferedImage createThumbnail(BufferedImage _initImage, int _newWidth, int _newHeight, int _type) {
    if (Fu.DEBUG && FuLog.isDebug()) FuLog.debug("CIM: on utilise technique swingx pour les thumbnails");
    int width = _initImage.getWidth();
    int height = _initImage.getHeight();

    boolean isTranslucent = _initImage.getColorModel().getTransparency() != Transparency.OPAQUE;

    BufferedImage thumb = _initImage;
    BufferedImage temp = null;

    Graphics2D g2 = null;

    int previousWidth = width;
    int previousHeight = height;

    do {
      if (width > _newWidth) {
        width /= 2;
        if (width < _newWidth) {
          width = _newWidth;
        }
      }

      if (height > _newHeight) {
        height /= 2;
        if (height < _newHeight) {
          height = _newHeight;
        }
      }

      if (temp == null || isTranslucent) {
        if (g2 != null) g2.dispose();
        temp = createImage(width, height, _type);
        g2 = temp.createGraphics();
        setBestQuality(g2);
      }
      g2.drawImage(thumb, 0, 0, width, height, 0, 0, previousWidth, previousHeight, null);

      previousWidth = width;
      previousHeight = height;

      thumb = temp;
    } while (width != _newWidth || height != _newHeight);

    g2.dispose();

    if (width != thumb.getWidth() || height != thumb.getHeight()) {
      temp = createImage(width, height, _type);
      g2 = temp.createGraphics();
      setBestQuality(g2);
      g2.drawImage(thumb, 0, 0, width, height, null);
      g2.dispose();
      thumb = temp;
    }

    return thumb;
  }

  public static Icon resize(final Icon _ic, final int _newWidth, final int _newHeight) {
    if (_ic.getIconHeight() == _newHeight && _ic.getIconWidth() == _newWidth) { return _ic; }

    final BufferedImage im = new BufferedImage(_ic.getIconWidth(), _ic.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
    final Graphics g = im.createGraphics();
    _ic.paintIcon(BuLib.HELPER, g, 0, 0);
    final Image res = resize(im, _newWidth, _newHeight);
    im.flush();
    return new ImageIcon(res);

  }

  public static BufferedImage resize(final BufferedImage _init, final int _maxSize) {
    final Dimension d = new Dimension(_init.getWidth(null), _init.getHeight(null));
    updateDimForMaxSize(d, _maxSize);
    return resize(_init, d.width, d.height);
  }

  public static BufferedImage resize(final BufferedImage _init, final int _newW, final int _newH) {
    return resize(_init, _newW, _newH, BufferedImage.TYPE_INT_ARGB);
  }

  public static BufferedImage resize(final BufferedImage _init, final int _newW, final int _newH, final int _type) {
    if (_newH < _init.getHeight() && _newW < _init.getWidth()) return createThumbnail(_init, _newW, _newH, _type);
    final BufferedImage res = new BufferedImage(_newW, _newH, _type);
    final Graphics2D g2 = res.createGraphics();
    g2.getRenderingHints().put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    g2.getRenderingHints().put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.getRenderingHints().put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.getRenderingHints()
        .put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    g2.getRenderingHints().put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    g2.getRenderingHints().put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    g2.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    g2.drawImage(_init, 0, 0, _newW, _newH, null);
    g2.dispose();
    return res;
  }

  public static void setBestQuality(final Graphics2D _g) {
    _g.getRenderingHints().put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    _g.getRenderingHints().put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    _g.getRenderingHints().put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    _g.getRenderingHints()
        .put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    _g.getRenderingHints().put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    _g.getRenderingHints().put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    _g.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
  }

}
