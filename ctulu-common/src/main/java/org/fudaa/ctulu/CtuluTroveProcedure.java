/*
 *  @creation     3 ao�t 2005
 *  @modification $Date: 2006-09-19 14:36:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import gnu.trove.TDoubleProcedure;

/**
 * Ensemble de procedures utiles pour les tableaux ,listes de primitives.
 * @author Fred Deniger
 * @version $Id: CtuluTroveProcedure.java,v 1.4 2006-09-19 14:36:54 deniger Exp $
 */
public class CtuluTroveProcedure {

  /**
   * Pour rechercher le max.
   * @author Fred Deniger
   * @version $Id: CtuluTroveProcedure.java,v 1.4 2006-09-19 14:36:54 deniger Exp $
   */
  public static class DoubleMaxValue implements TDoubleProcedure {

    double max_ = -Double.MAX_VALUE;

    @Override
    public boolean execute(final double _value){
      if (_value > max_) {
        max_ = _value;
      }
      return true;
    }

    /**
     * @return la valeur max
     */
    public double getMax(){
      return max_;
    }
  };

  /**
   * Pour rechercher le min.
   * @author Fred Deniger
   * @version $Id: CtuluTroveProcedure.java,v 1.4 2006-09-19 14:36:54 deniger Exp $
   */
  public static class DoubleMinValue implements TDoubleProcedure {

    double min_ = Double.MAX_VALUE;

    @Override
    public boolean execute(final double _value){
      if (_value < min_) {
        min_ = _value;
      }
      return true;
    }

    /**
     * @return la valeur min.
     */
    public double getMin(){
      return min_;
    }
  };

}
