/*
 *  @creation     12 janv. 2006
 *  @modification $Date: 2007-04-02 08:55:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.ostermiller.util.SignificantFigures;
import java.text.DecimalFormat;

/**
 * Un formatter pour les nombre fortran qui doivent etre codes sur n caract�res.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluNumberFormatFortran.java,v 1.13 2007-04-02 08:55:34 deniger Exp $
 */
public class CtuluNumberFormatFortran extends CtuluNumberFormat {

  /**
   * @author Fred Deniger
   * @version $Id: CtuluNumberFormatFortran.java,v 1.13 2007-04-02 08:55:34 deniger Exp $
   */
  public static class Validator extends CtuluValueValidator {

    private final CtuluNumberFormatFortran fmt_;

    private final boolean pos_;

    /**
     * @param _fmt
     * @param _pos true si doit etre positif
     */
    public Validator(final CtuluNumberFormatFortran _fmt, final boolean _pos) {
      super();
      fmt_ = _fmt;
      pos_ = _pos;
    }

    public Validator(final CtuluNumberFormatFortran _fmt) {
      this(_fmt, false);
    }

    @Override
    public String getDescription() {
      if (pos_) {
        return CtuluLib.getS("Le nombre doit �tre positif et contenir au plus {0} caract�res", CtuluLibString
            .getString(fmt_.maxChar_));
      }
      return CtuluLib.getS("Le nombre doit contenir au plus {0} caract�res", CtuluLibString.getString(fmt_.maxChar_));
    }

    @Override
    public boolean isValueValid(final double _d) {
      if (pos_ && _d < 0) {
        return false;
      }
      return fmt_.isValid(_d);
    }

  }

  @Override
  public String toLocalizedPattern() {
    return "FORTRAN:" + CtuluLibString.getString(maxChar_);
  }

  public static final String WRONG_VALUE = "X";

  final int maxChar_;
  /** Le pr�formatteur pour le nombre maxi de d�cimales apr�s la virgule. <tt>null</tt> si pas de limitation */
  final DecimalFormat fractionFmt_;


  /**
   * @param _maxChar
   */
  public CtuluNumberFormatFortran(final int _maxChar) {
    super();
    maxChar_ = _maxChar;
    fractionFmt_=null;
  }
  
  /**
   * Un formatteur tenant compte du nombre de digits de fractions.
   * @param _maxChar Le nombre maxi de caract�res du champ de codage
   * @param _maxFractionDigits Le nombre max de digits. Ce nombre max n'est pas
   * consid�r� si le nombre est �crit en notation scientifique.
   */
  public CtuluNumberFormatFortran(final int _maxChar, final int _maxFractionDigits) {
    super();
    maxChar_=_maxChar;
    
    fractionFmt_=CtuluLib.getNoEffectDecimalFormat();
    fractionFmt_.setMaximumFractionDigits(_maxFractionDigits);
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return this;
  }

  public CtuluValueValidator createValueValidator(final boolean _pos) {
    return new Validator(this, _pos);
  }

  protected static boolean isDouble(final String _s) {
    return _s.indexOf('.') >= 0;
  }

  protected static boolean isExp(final String _s) {
    return _s.indexOf('E') >= 0;
  }

  @Override
  public String format(final double _d) {
    String s;
    
    // Pr�formatage pour tenir compte du nombre maxi de digits fraction.
    if (fractionFmt_!=null) {
      s = fractionFmt_.format(_d);
    }
    else {
      s = Double.toString(_d);
    }
    
    // on ajoute un point et on enleve le 0 inutile
    final boolean isExp = isExp(s);
    if (!isExp) {
      s = formatDot(s);
    }
    if (s.length() > maxChar_) {
      final SignificantFigures fig = new SignificantFigures(_d);
      // si le double est entre -1 et 0 il faut prendre en compte les caractere '-0.' d'ou le -3
      final int nbSign = _d < 0 ? (_d > -1 ? (maxChar_ - 3) : (maxChar_ - 2)) : (maxChar_ - 1);

      if (nbSign <= 0) {
        return WRONG_VALUE;
      }
      fig.setNumberSignificantFigures(nbSign);
      if (isExp) {
        s = fig.toScientificNotation();
        if (s.length() > maxChar_) {
          s = removeZeroAtEndScien(s);

        }
      } else {
        s = formatDot(fig.toString());
        if (s.length() > maxChar_) {
          s = removeZeroAtEnd(s, false);
        }
      }
      if (s.length() > maxChar_) {
        String newS = removeZeroAtEndScien(fig.toScientificNotation());
        if (newS.length() > maxChar_) {
          final int nbAfter = newS.length() - newS.indexOf('E');
          // on recalcule ne nombre signficatif reel
          int nbNewSign = maxChar_ - nbAfter;
          if (newS.startsWith("-")) {
            nbNewSign--;
          }
          if (newS.indexOf('.') >= 0) {
            nbNewSign--;
          }
          if (nbNewSign > 0) {
            fig.setNumberSignificantFigures(nbNewSign);
            newS = removeZeroAtEndScien(fig.toScientificNotation());
            if (newS.length() <= maxChar_) {
              s = newS;
            }
          }
        } else {
          s = newS;
        }
      }
      // return s.length() <= maxChar_ ? s : WRONG_VALUE;
    }
    return s;

  }

  public String formatDot(final String _s) {
    String res = _s;
    if (!isDouble(_s)) {
      res += '.';
    }
    if (res.length() > 2 && res.length() > maxChar_ && res.startsWith("0.")) {
      res = res.substring(1);
    }
    return res;
  }

  public static String removeZeroAtEnd(final String _s, boolean _removeDot) {
    if (_s.indexOf('E') >= 0 || _s.indexOf('.') < 0) {
      return _s;
    }
    int idx = _s.length();
    /*
     * if (_s.charAt(idx - 1) != '0') { return _s; }
     */
    while (idx > 0 && _s.charAt(idx - 1) == '0') {
      idx--;
    }
    if (_removeDot && idx > 0 && _s.charAt(idx - 1) == '.') {
      idx--;
    }

    return _s.substring(0, idx);
  }

  public static String removeZeroAtEndScien(final String _s) {
    final int idx = _s.indexOf('E');
    if (idx < 0) {
      return _s;
    }
    String firstPart = removeZeroAtEnd(_s.substring(0, idx), true);
    return firstPart + "E" + _s.substring(idx + 1);
  }

  public boolean isValid(final double _d) {
    return WRONG_VALUE != format(_d);
  }

  public int getMaxChar() {
    return maxChar_;
  }

}
