/**
 *
 */
package org.fudaa.ctulu;

/**
 * Classe permettant de donner le resultat et le log d'une opération.
 *
 * @author deniger
 *
 */
public class CtuluLogResult<T> {

  private T resultat;
  private CtuluLog log;

  public CtuluLogResult() {
  }

  public CtuluLogResult(T resultat, CtuluLog log) {
    this.resultat = resultat;
    this.log = log;
  }
  
  

  public T getResultat() {
    return resultat;
  }

  public void setResultat(T resultat) {
    this.resultat = resultat;
  }

  public CtuluLog getLog() {
    return log;
  }

  public void setLog(CtuluLog log) {
    this.log = log;
  }
}
