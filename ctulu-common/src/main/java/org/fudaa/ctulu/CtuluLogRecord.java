package org.fudaa.ctulu;

import java.text.MessageFormat;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class CtuluLogRecord {

  public static void updateLocalizedMessage(CtuluLogRecord record, ResourceBundle resourceBundle) {
    final Object[] args = record.getArgs();
    String msg = null;
    if (resourceBundle != null) {
      try {
        msg = resourceBundle.getString(record.getMsg());
      } catch (MissingResourceException e) {
        msg = record.getMsg();
      }
      if (!ArrayUtils.isEmpty(args)) {
        try {
          msg = MessageFormat.format(msg, args);
        } catch (Exception ex) {
          Logger.getLogger(CtuluLogRecord.class.getName()).log(Level.INFO, "cant localized message: " + msg, ex);
        }
      }
    }
    if (msg == null) {
      if (!ArrayUtils.isEmpty(args)) {
        msg = record.getMsg() + " " + StringUtils.join(args);
      } else {
        msg = record.getMsg();
      }
    }
    record.setLocalizedMessage(msg);
  }

  private Object[] args;
  private CtuluLogLevel level;
  private String levelDetail;
  private String msg;
  private Throwable thrown;
  private Date logDate;
  private String id;
  private String helpUrl;
  private String helpSupportUrl;
  private String localizedMessage;
  private String ressource;
  private String ressourceLine;
  private String ressourceFunction;
  

  /**
   * @param level
   * @param msg
   */
  public CtuluLogRecord(CtuluLogLevel level, String msg) {
    super();
    this.level = level;
    this.msg = msg;
    logDate = new Date(System.currentTimeMillis());
  }

  public String getRessourceFunction() {
    return ressourceFunction;
  }

  public void setRessourceFunction(String ressourceFunction) {
    this.ressourceFunction = ressourceFunction;
  }
  

  public String getRessource() {
    return ressource;
  }

  public void setRessource(String ressource) {
    this.ressource = ressource;
  }

  public String getRessourceLine() {
    return ressourceLine;
  }

  public void setRessourceLine(String ressourceLine) {
    this.ressourceLine = ressourceLine;
  }

  public String getLocalizedMessage() {
    return localizedMessage;
  }

  public void setLocalizedMessage(String localizedMessage) {
    this.localizedMessage = localizedMessage;
  }

  public String getHelpSupportUrl() {
    return helpSupportUrl;
  }

  public void setHelpSupportUrl(String helpSupportUrl) {
    this.helpSupportUrl = helpSupportUrl;
  }

  public String getHelpUrl() {
    return helpUrl;
  }

  public void setHelpUrl(String helpUrl) {
    this.helpUrl = helpUrl;
  }

  /**
   * @param level
   * @param msg
   */
  public CtuluLogRecord(CtuluLogLevel level, String msg, String id) {
    super();
    this.level = level;
    this.msg = msg;
    this.id = id;
    logDate = new Date(System.currentTimeMillis());
  }

  public String getId() {
    return id;
  }

  public String getLevelDetail() {
    return levelDetail;
  }

  public void setLevelDetail(String levelDetail) {
    this.levelDetail = levelDetail;
  }

  public Date getLogDate() {
    return logDate;
  }

  public void setLogDate(Date logDate) {
    this.logDate = logDate;
  }

  /**
   * @return the args
   */
  public Object[] getArgs() {
    return args;
  }

  /**
   * @return the level
   */
  public CtuluLogLevel getLevel() {
    return level;
  }

  /**
   * @return the msg
   */
  public String getMsg() {
    return msg;
  }

  /**
   * @param args the args to set
   */
  public void setArgs(Object[] args) {
    this.args = args;
  }

  /**
   * @return the thrown
   */
  public Throwable getThrown() {
    return thrown;
  }

  /**
   * @param thrown the thrown to set
   */
  public void setThrown(Throwable thrown) {
    this.thrown = thrown;
  }
}
