/*
 GPL 2
 */
package org.fudaa.ctulu;

/**
 *
 * @author Frederic Deniger
 */
public interface ToStringTransformable {
  
  
  String getAsString();
  
}
