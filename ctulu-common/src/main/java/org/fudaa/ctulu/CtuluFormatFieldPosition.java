/**
 *  @creation     27 sept. 2004
 *  @modification $Date: 2006-04-05 10:00:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import java.text.FieldPosition;


/**
 * @author Fred Deniger
 * @version $Id: CtuluFormatFieldPosition.java,v 1.2 2006-04-05 10:00:10 deniger Exp $
 */
public class CtuluFormatFieldPosition extends FieldPosition {
  

  public CtuluFormatFieldPosition() {
    super(0);
  }
  
  
  @Override
  public void setBeginIndex(final int _bi){
  }
  @Override
  public void setEndIndex(final int _ei){
  }
}
