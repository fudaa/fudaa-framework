/**************************************************************************
*   vthierry@sophia.inria.fr, Copyright (C) 1999  All rights reserved.   *
**************************************************************************/

package org.fudaa.ctulu;

/** Encapsulate the local resolution of a set of non-linear equations.
 *
 * The estimation problem is stated in the form:
 <pre> min || x - x0 ||_Q^2  with c(x) = 0 </pre>
 with: <pre> || x ||_Q^2 = x^T Q x </pre>
 which is equivalent to a <a href=#proj>projection</a> problem.
 *
 */

public class LocalSolve {

    static final double EPS = Double.MIN_VALUE * 10;

    /** Return the local solution of a set of equations
     * @param _x0 Initial estimate for the resolution size n
     * @param _Q Optional <a href=#smatrix>quadratic form</a> defining
     the distance to x0
     * @param _eqs Equations to be locally solved
     * @return The solution x[] as an array.
     * @param _e with e[1] On output return || x - x0 ||_Q^2 (if specified)
     *               e[2] On output return ||c_i(x)||_oo (if specified)
     */

    public static double[] get(final double _x0[], final double _Q[], final Equation _eqs[],
                              final double _e[]) {
        return get_(_x0, _Q, _eqs, _e);
    }
    /*!*/ public static double[] get(final double _x0[], final Equation _eqs[], final double _e[]) {
        return get_(_x0, null, _eqs, _e);
    }
    /*!*/ public static double[] get(final double _x0[], final double Q[], final Equation _eqs[]) {
        return get_(_x0, Q, _eqs, null);
    }
    /*!*/ public static double[] get(final double _x0[], final Equation _eqs[]) {
        return get_(_x0, null, _eqs, null);
    }

    /** Define an equation <i>c(x) = 0</i> with gradient <i>C(x) = d c(x) / d x</i> */

    public static abstract class Equation {
        public abstract double c(/*const*/ double x[]);
        public abstract double[] C(/*const*/ double x[]);
    }

    /** Construct a linear equation of the form <i>a^T x = b</i> */
    public static class LinearEquation extends Equation {
        public LinearEquation(final double[] _a, final double _b) { this.a = _a; this.b = _b; }
        private double a[], b;
        /*!*/@Override
 public double c(final double x[]) {
            double _e = 0;
            for (int i = 0; i < x.length; i ++) {
              _e += a[i] * x[i];
            }
            return _e - b;
        }
        /*!*/@Override
 public double[] C(final double x[]) { return a; }
    }

    /** Construct the equation <i>||x||^2 = 1</i> */
    public static class UnaryEquation extends Equation {
        private final double n2 = 1;
        /*!*/ public UnaryEquation() { }
        //private UnaryEquation(double n) { n2 = n*n; }
        /*!*/@Override
 public double c(final double x[]) {
            double e2 = 0;
            for (int i = 0; i < x.length; i ++) {
              e2 += x[i] * x[i];
            }
            return (e2 - n2)/2;
        }
        /*!*/@Override
 public double[] C(final double x[]) { return x; }
    }

    ///////////////////////////////////////////////////////////////////////////
    /** <p><br><br><hr> */
    ///////////////////////////////////////////////////////////////////////////

    static final int OK = -1;

    //**********************************************************************
    //***   Internal representation of matrices
    //**********************************************************************

    /** Internal Representation of matrices in double buffers <ul> */

    /** <a name=matrix></a><li>
        Here p x n matrices are stored in a p x n double array, row by row:
        * a = {
        * <dt><dd> A[1,1], A[1,2], .. A[1,n]
        * <dt><dd> ..
        * <dt><dd> A[p,1], A[p,2], .. A[p,n]
        * }
        */

    // Index mechanism in a n dimensional matrix so that :
    //   M[j,i] = M[i+k_[j]] for j < p

    static int[] kTab;
    static int nkTab ;
    static int pkTab ;

    // Initialize the index buffer for rectangular matrix up to p x n

    static final void initK(final int _p, final int _n) {
        if ((pkTab < _p) || (nkTab != _n)) {
            pkTab = _p; nkTab = _n;
            kTab = new int[_p];
            for(int j = 0 ; j < _p ; j++) {
              kTab[j] = j * _n;
            }
        }
    }

    /** <a name=lmatrix></a><li>
        Here n x n symmetric or lower triangular matrices are stored
        in a n x (n+1) / 2 double array, row by row:
        * s = {
        * <dt><dd>  S[1,1],
        * <dt><dd>  S[1,2], S[2,2],
        * <dt><dd>  ..
        * <dt><dd>  S[1,n], S[2,n], .. S[n,n]
        * }
        */

    // Index mechanism in lower triangular matrix so that :
    //   M[i,j] = M[i+l_[j]] for j < nl_

    static int[] lTab ;
    static int  nlTab ;

    // Initialize the index buffer for lower triangular matrix up to n x n.

    static final void initL(final int n) {
        if (nlTab < n) {
            nlTab = n;
            lTab = new int[n];
            for(int k = 0 ; k < n ; k++) {
              lTab[k] = (k*(k+1)) / 2;
            }
        }
    }

    /** <a name=smatrix></a><li>
        Here the n x n quadratic form represented by a symmetric matrix is
        stored either :
        * <dt><dd> - as the symmetric part of a true matrix stored
        in a <a href=#matrix>n x n</a> double array or
        * <dt><dd> - as a true symmetric matrix stored
        in a <a href=#lmatrix>n (n+1) / 2 </a> double array or
        * <dt><dd> - as a diagonal matrix stored
        in a <i>n</i> double array
    */

    // Convert a symmetric matrix Q given in various forms
    // to a lower triangular matrix q

    static final int qConvert(final double Q[], final int dimQ,
                              final double q[], final int n) {
        if (dimQ == 0) {
            int ij = 0;
            for (int j = 0 ; j < n ; j++) {
                for (int i = 0 ; i < j ; i++) {
                  q[ij++] = 0;
                }
                q[ij++] = 1;
            }
        } else if (dimQ == 1) {
            int ij = 0;
            for (int j = 0 ; j < n ; j++) {
                for (int i = 0 ; i < j ; i++) {
                  q[ij++] = 0;
                }
                q[ij++] = Q[0];
            }
        } else if (dimQ == n) {
            int ij = 0;
            for (int j = 0 ; j < n ; j++) {
                for (int i = 0 ; i < j ; i++) {
                  q[ij++] = 0;
                }
                q[ij++] = Q[j];
            }
        } else if (dimQ == n*(n+1)/2) {
            for (int ij = 0; ij < n*(n+1)/2; ij++) {
              q[ij] = Q[ij];
            }
        } else if (dimQ == n*n) {
            int ij = 0;
            for (int j = 0 ; j < n ; j++) {
                for (int i = 0 ; i < j ; i++) {
                  q[ij++] = (Q[i+j*n] + Q[j+i*n]) / 2;
                }
                q[ij++] = Q[j+j*n];
            }
        } else {
          return 0;
        }
        return OK;
    }

    // Cholesky decomposition of a Q matrix with inirialization and check

    static final double[] Q2l(final double Q[], final int n) {
        final double _l[] = new double[n*(n+1)/2];

	if (qConvert(Q, (Q != null) ? Q.length : 0, _l, n) != OK) {
    throw new IllegalArgumentException("A ``Q'' matrix " +
    		       "does not correspond " +
    		       "to a symmetric matrix");
  }
	chol2Calc(_l, n);
        return _l;
    }

    // Compute the distance || x - x0 ||_Q^2

    static final double qDist(/*const*/ final double _x[],
                             /*const*/ final double _x01[],
                             final double _e[], // temporary buffer
                             /*const*/ final double _l[]) {
        final int n = _x.length;
        for(int j = n-1 ; j >= 0 ; j--) {
            _e[j] = _l[j+lTab[j]] * (_x[j] - _x01[j]);
            for(int i = 0 ; i < j ; i++) {
              _e[j] += _l[i+lTab[j]] * (_x[i] - _x01[i]);
            }
        }
        double e2 = 0;
        for (int j = 0 ; j < n ; j++) {
          e2 += _e[j] * _e[j];
        }
        return e2;
    }

    /** </ul> */

    //**********************************************************************
    //***   Cholesky decomposition of a definite positive matrix
    //**********************************************************************

    /** <a name=chol></a><hr> The Cholesky Decomposition </h1>
     * The Cholesky or ``square-root'' decomposition of a symmetric positive
     definite matrix S is a lower triangular matrix L such that S = L L^T.
     *
     * Two matrices L and L' are the square-root of a symmetric positive matrix
     S if and only if L = R L' for some orthogonal matrix R, i.e. with R R^T = I
     *
     * This particular decomposition corresponds to the reduction of a
     quadratic form: <pre>
        -n   -n                      -n   /  -n              \ 2
 q(x) = >    >      S_i,j x_i x_j =  >    |  >     L_i,k x_i |
        -i=1 -j=1                    -k=1 \  -j=k            /</pre>
     as a sum of squares.
     *
     * It is defined thank's to the fact that for a positive definite symmetric
     matrix: <pre>
 S_i,i > 0  and  (S_i,j)^2 < S_i,i S_j,j  and  max_i,j |S_i,j| = S_k,k</pre>
     for some k while it is computed in o(n^3) steps and is well conditionned.
     *
     * Using this decomposition, the triangular equation L L^T x = y is solved
     in o(n^2) steps.
     *
     * <a name=closest></a> <a name=reduced></a>If the matrix is not definite,
     L_k,k <= 0 for some k, but we can easily compute instead either :
     * - the <i>``closest'' </i> Cholesky decomposition of the matrix
     S + u e_k e_k^T for some small minimal u (e_k is the k-th basic vector)
     ``close'' to S; or :
     * - the <i>``reduced'' </i> Cholesky decomposition of the matrix from
     which the k-th rows and column has been removed can be computed.
     *
     * In this case, if S = C C^T and of rank r, the 1st r rows of C which are
     independent are selected.
     *
     */

    // Square-Root decomposition of a symmetric positive definite matrix s[]
    // @param s[n*(n+1)/2] Symmetric matric to be factorized
    //  On output, s[] containts l[] with s = l l^T (on success)
    // @return -1 on succes, else the index of the singular component

    static final int cholCalc(final double s[], final int n) {
        for(int k = 0 ; k < n ; k++) {
            int l0 = k+lTab[k];
            double s0 = s[l0];
            if (s0 <= EPS) {
              return k;
            }
            s0 =  Math.sqrt(s0); s[l0] = s0;
            l0 = k+1;
            for (int j = l0 ; j < n ; j++) {
                s[k+lTab[j]] /= s0;
                for (int i = l0 ; i <= j ; i++) {
                    s[i+lTab[j]] -= s[k+lTab[i]] * s[k+lTab[j]];
                }
            }
        }
        return OK;
    }

    // Closest Square-Root decomposition of a symmetric positive matrix s[]
    //  -> If undefined a minimal quantity is added to get a definite matrix
    static final void chol2Calc(final double s[], final int n) {
        for(int k = 0 ; k < n ; k++) {
            int l0 = k+lTab[k];
            double s0 = s[l0];
            if (s0 <= EPS) {
              s0 = 2 * EPS;
            }
            s0 =  Math.sqrt(s0); s[l0] = s0;
            l0 = k+1;
            for (int j = l0 ; j < n ; j++) {
                s[k+lTab[j]] /= s0;
                for (int i = l0 ; i <= j ; i++) {
                    s[i+lTab[j]] -= s[k+lTab[i]] * s[k+lTab[j]];
                }
            }
        }
    }

    // Reduced Square-Root decomposition of a symmetric positive matrix s[]
    //  -> If undefined the redundant row/column is removed
    // @param i_[] index of non removed collums
    // @return the number of used equations
    static final int chol3Calc(final double s[], final int _i[], final int n) {
        int _r = n;
        for(int k0 = 0, k = 0 ; k0 < n ; k0++) {
            int l0 = k+lTab[k];
            double s0 = s[l0];
            if (s0 <= EPS) {
                int _d = k+1;
                for (int j = k+1 ; j < _r ; j++) {
                    for (int i = 0 ; i <= j ; i++) {
                        if (i == k) {
                          _d++;
                        } else {
                          s[i+lTab[j]-_d] = s[i+lTab[j]];
                        }
                    }
                }
                _r--;
            } else {
                _i[k] = k0;
                s0 =  Math.sqrt(s0); s[l0] = s0;
                l0 = k+1;
                for (int j = l0 ; j < _r ; j++) {
                    s[k+lTab[j]] /= s0;
                    for (int i = l0 ; i <= j ; i++) {
                        s[i+lTab[j]] -= s[k+lTab[i]] * s[k+lTab[j]];
                    }
                }
                k++;
            }
        }
        return _r;
    }

    // Resolution of l l^T x = x
    // @param l[n*(n+1)/2] Cholesky decomposition of the S matrix
    // @param x[n] Vector of the linear system of equation
    //  On output x is replaced by the solution

    static final void cholSolve(/*const*/ final double _l[], final double x[], final int n) {
        for(int j = 0 ; j < n ; j++) {
            for(int i = 0 ; i < j ; i++) {
              x[j] -= _l[i+lTab[j]] * x[i];
            }
            x[j] /= _l[j+lTab[j]];
        }
        for(int j = n-1 ; j >=0 ; j--) {
            for(int i = j+1 ; i < n ; i++) {
              x[j] -= _l[j+lTab[i]] * x[i];
            }
            x[j] /= _l[j+lTab[j]];
        }
    }

    //**********************************************************************
    //***   QL resolution
    //**********************************************************************

    /** <a name=ql></a><hr> Solving a QL problem.
     *
     * A quadratic criterion with linear constraints, say a <i>QL</i>-problem,
     of the form:
     <pre> min || x - x0 ||_Q^2 with C x = d </pre>
     has normal equations of the form:
     <pre> Q x + C^T l = Q x0 with C x = d</pre>
     where l is the related Lagrange multiplier, which explicit solution is:
     <pre> x = x0 - Q^(-1) C^T (C Q^(-1) C^T)^(-1) [ C x0 - d ]</pre>
     which is defined if and only if Q and C are of full rank.
     *
     * Here, the <a href=#chol>Cholesky</a> decomposition allows a rather
     simple implementation of this huge formula computing:
     <pre> (1) Q = L L^T with (2) y0 = L^T x0 and (3) L B^T = C^T</pre>
     the system simplifies to:
     <pre> L^T x = y0 - B^T (B B^T)^(-1) [ B y0 - d ]</pre>
     easily computed using the Cholesky decomposition of (B B^T) since:
     <pre> (4) (B B^T) l = B y0 - d while (5) L^T x = y0 - B^T l</pre>
     *
     * - If Q is not definite its <a href=#closest>closest</a> Cholesky
     decomposition is computed.
     *
     * - If the equations are not independent the <a href=#reduced>reduced</a>
     Cholesky decomposition of (B B^T) is computed.
     * If r equations are independent, this means that the the 1st min(n,r)
     equations are selected.
     */

    // Temporary buffer for qlSolve mechanism
    private static double m[];

    // Initialize the buffers for a Qlsolve problem up to dim n x q

    static final void initQ(final int n, final int q) {
        initK(q, n);
        initL(Math.max(n, q));
        m = new double[q*(q+1)/2];
    }

    // QL resolution for a set of q linear equations
    // @param l[n*(n+1)/2] Cholesky decomposition of the Q matrix (if any)
    // @param C[q*n], d[q] Linear system of equation C x = d
    //  These are modified by the routine
    // @param x[n] Initial value x0
    //  On output x is replaced by the solution
    //
    // @param i_[q] indexes of used equations
    // @return the number of used equations

    static final int qlSolve(final double x[],
                             final double _C[], final double _d[], final int _i[],
                             final int n, final int q) {
        // d = d - C x
        for(int k = 0; k < q; k++) {
          for(int i = 0 ; i < n ; i++) {
            _d[k] -= _C[i+kTab[k]] * x[i];
          }
        }
        // m = B B^T
        {
            int ij = 0;
            for (int j = 0 ; j < q ; j ++) {
                for (int i = 0 ; i <= j ; i++) {
                    m[ij] = 0;
                    for (int k = 0 ; k < n ; k++) {
                      m[ij] += _C[k+kTab[i]] * _C[k+kTab[j]];
                    }
                    ij++;
                }
            }
        }
        // d = l, m l = d
        final int _r = chol3Calc(m, _i, q);
        if (_r < q) {
          for(int k = 0; k < _r; k++) {
            _d[k] = _d[_i[k]];
          }
        }
        cholSolve(m, _d, _r);
        // x += C^T d
        for(int i = 0 ; i < n ; i++) {
          for(int k = 0; k < _r; k++) {
            x[i] += _C[i+kTab[_i[k]]] * _d[k];
          }
        }
        return _r;
    }

    static final int qlSolve(final double x[], /*const*/ final double _l[],
                             final double _C[], final double _d[], final int _i[],
                             final int n, final int q) {
        if (_l == null) {
            return qlSolve(x, _C, _d, _i, n, q);
        }
        // x = y0, y0 = L^T x0
        for(int j = n-1 ; j >= 0 ; j--) {
            x[j] *= _l[j+lTab[j]];
            for(int i = 0 ; i < j ; i++) {
              x[j] += _l[i+lTab[j]] * x[i];
            }
        }
        // C = B, L B^T = C^T
        for(int k = 0; k < q; k++) {
            final int j0 = kTab[k];
            for(int j = 0 ; j < n ; j++) {
                for(int i = 0 ; i < j ; i++) {
                  _C[j0+j] -= _l[i+lTab[j]] * _C[j0+i];
                }
                _C[j0+j] /= _l[j+lTab[j]];
            }
        }
        final int _r = qlSolve(x, _C, _d, _i, n, q);
        // x = y0, L^T y0 = x
        for(int j = n-1 ; j >=0 ; j--) {
            for(int i = j+1 ; i < n ; i++) {
              x[j] -= _l[j+lTab[i]] * x[i];
            }
            x[j] /= _l[j+lTab[j]];
        }
        return _r;
    }

    //**********************************************************************
    //***  Projection implementation
    //**********************************************************************

    /** <a name=proj></a><hr> Euclidean projection onto the manifold.
     *
     * The solution x of the estimation problem:
     <pre> min || x - x0 ||_Q^2  with c(x) = 0 </pre>
     * is the projection of x0 onto the manifold defined by the implicit
     equations c(x) = 0.
     *
     * It is iteratively solved by the series x_(n+1) = p(x_n) where
     x_(n+1) is the projection of x0 on the tangeant linear manifold defined by:
     <pre> C x = d with C = d c(x_n) / d x and d = C x_n - c(x_n) </pre>
     * which is the solution of a <a href=#ql>QL</a> problem.
     *
     * This is known to converge, at a quadratic rate, almost everywhere for
     convex manifold or at least locally for any regular manifold, while it
     converges in one step for linear mainfold (contrary to Newton's like
     methods).
     *
     * Global convergence is reinforced by ``backtracking'' if not
     ||c(x_(n+1))|| < ||c(x_(n))||,  i.e. by considering
     <pre> x_(n+1) <- (x_n + x_(n+1))/2</pre> until either ||c(x_(n+1))||
     decreases or x_n = x_(n+1) up to the double resolution,
     which terminates the estimation.
     *
     * With this implementation, the 1st maximal set of non-redundent
     equations is selected.
     */

    // Buffers for the projection algorithm

    private static double C[], d[];
    private static int i_[], r;

    // Constants for the projection algorithm

    private static double l[];
    private static Equation eqs[];

    // Variables for the projection algorithm
    // @param x0[] modified state vector of size n
    // @param x1[] previous state vector
    // @param e[2] modified and previous error

    private static double x0[], x1[], e[];

    // Initializes the data used for the projection algorithm

    private static final void initP(final double x[], final double Q[], final Equation _eqs[]) {
        final int n = x.length;
        final int q = _eqs.length;

        // Init buffers
        initQ(n, q);
        C = new double[q*n]; d = new double[q]; i_ = new int[q];
        r = q;
        for(int j = 0; j < q; j++) {
          i_[j] = j;
        }

        // Init constants
        l = Q2l(Q, n);
        LocalSolve.eqs = _eqs;

        // Init variables
        x0 = (double []) x.clone(); x1 = new double[n];
        e = new double[2]; e[1] = Double.MAX_VALUE;
    }

    // Implement one iteration of the projection algorithm

    private static final boolean iter() {
        final int n = x0.length;
        final int q = eqs.length;
        e[0] = 0;
        int ij = 0;
        for (int j = 0, k = 0; j < q; j++) {
            final Equation eq = eqs[j];
            d[j] = -eq.c(x0);
	    if ((k < r) && (j == i_[k])) {
		final double ei = (d[j] < 0) ? -d[j] : d[j];
		e[0] = (e[0] < ei) ? ei : e[0];
		k++;
	    }
            final double g[] = eq.C(x0);
            for (int i = 0; i < n; i++) {
                C[ij++] = g[i];
                d[j] += g[i] * x0[i];
            }
        }
        // print(" e0=" + e[0] + " x0", x0);
        if (e[0] < e[1] - EPS) {
            e[1] = e[0];
            for (int i = 0; i < n; i++) {
              x1[i] = x0[i];
            }
            r = qlSolve(x0, l, C, d, i_, n, q);
            return true;
        }
        boolean loop = false;
        for (int i = 0; i < n; i++) {
            final double x0_i = (x0[i] + x1[i]) / 2;
            loop |= x0_i != x0[i];
            x0[i] = x0_i;
        }
        return loop;
    }

    static final double[] get_(/*const*/ final double x[],
                              /*const*/ final double Q[],
                              /*const*/ final Equation _eqs[],
                              final double err[]) {
        initP(x, Q, _eqs);
        while (iter()) {
          ;
        }
	if (err != null) {
	    err[0] = qDist(x1, x, x0, l);
	    if (err.length > 1) {
        err[1] = e[1];
      }
	}
	return x1;
    }

    /*

    //**********************************************************************
    //***   Testing the numerical layers of the algorithm
    //**********************************************************************

    // Print a vector as a line

    static final void print(String s, double v[], int dim) {
        System.out.print(s + " = [ ");
        if (v == null)
            System.out.print("<null> ");
        else
            for (int k = 0; k < dim; k++)
                System.out.print(v[k] + " ");
        System.out.println("]");
    }
    static final void print(String s, double v[]) {
        print(s, v, (v == null) ? 0 : v.length);
    }
    static final void print(String s, int v[], int dim) {
        System.out.print(s + " = [ ");
        if (v == null)
            System.out.print("<null> ");
        else
            for (int k = 0; k < dim; k++)
                System.out.print(v[k] + " ");
        System.out.println("]");
    }
    static final void print(String s, int v[]) {
        print(s, v, (v == null) ? 0 : v.length);
    }


    private static void cholTst() {

        // Test with :
        //   L := matrix(3,3, [2,0,0,3,1,0,4,1,4]);
        //   A := evalm(L &* transpose(L));
        //   x := evalm(A &* vector(3, [1,2,3]));

        // Test at Java level
        int n = 3;
        double s[] = {4, 6, 10, 8, 13, 33}, x[] = {40, 65, 133};
        // Test at the native level
        initL(n);
        cholCalc(s, n);
        print("l", s);
        cholSolve(s, x, n);
        print("x", x);
    }

    private static void qlTst() {
        // Test with linear call
        double x0[] = {0, 0, 3};
        double C[] = {1, 0, 0, 0, 1, 0}, d[] = {1, 2};
        double l[] = {1, 0, 1, 0, 0, 1};
        initQ(3, 2);
        qlSolve(x0, l, C, d, new int[3], 3, 2);
        print("x0", x0);
        // Test with linear but as equation
        double x1[] = {0, 0, 3};
        double c1[] = {1, 0, 0}, c2[] = {0, 1, 0};
        Equation e1[] = {
            new LinearEquation(c1, 1),
            new LinearEquation(c2, 2)
        };
        print("x1", get(x1, null, e1));
        // Test with c(x) = ||x||^2 -1 => x = (1+k^2)/(2 k) x0/||x0||
        double x2[] = {0, 0, 3};
        Equation e2[] = {
            new UnaryEquation()
        };
        print("x2", get(x2, null, e2));
        double x3[] = {0, 0, 0.3};
        print("x3", get(x3, null, e2));
        // Test with c(x) = ||x||^2 - 1 and c3^T x = 0
        double x4[] = {1, 1, 3}, c3[] = {0, 0, 1};
        Equation e4[] = {
            new UnaryEquation(),
            new LinearEquation(c3, 0)
        };
        print("x4", get(x4, null, e4));
    }

    private static void ql2Tst() {
        initL(4);
        // Test with a semi-definite positive matrix
        double s1[] = {9, 0, 0, 0, 0, 4};
        chol2Calc(s1, 3);
        print("l1", s1);
        // Test with a null equation inserted
        double s2[] = {4, 0, 0, 6, 0, 10, 8, 0, 13, 33};
        int i2[] = new int[4];
        int r = chol3Calc(s2, i2, 4);
        print("l2", s2, r*(r+1)/2);
        print("i2", i2, r);
        // Test with c(x) = ||x||^2 - 1
        double x3[] = {0, 0, 3};
        Equation e3[] = {
            new UnaryEquation(21),
            new UnaryEquation(1),
            new UnaryEquation(1000),
            new UnaryEquation(2),
            new UnaryEquation(1),
            new UnaryEquation(1),
            new UnaryEquation(3)
        };
        print("x3", get(x3, null, e3));
     }

    public static void main(String argv[]) {
        cholTst();
        qlTst();
        ql2Tst();
    }

    */

    /** <hr> Solve a simple Least-Square problem of the form:
	<pre> min || A x - b ||^2 </pre>
	of m equations in n unknows.
    */
    static public double[] leastsquare(final double A[], final double b[], final int _m, final int n) {
	final double Q[] = new double[n*n], x[] = new double[n];
	initK(_m, n);
	initL(n);
	int ij = 0;
	for (int j = 0 ; j < n ; j ++) {
	    for (int i = 0 ; i <= j ; i++) {
		Q[ij] = 0;
		for (int k = 0 ; k < _m ; k++) {
      Q[ij] += A[i+kTab[k]] * A[j+kTab[k]];
    }
		ij++;
	    }
	}
	for (int j = 0 ; j < n ; j ++) {
	    x[j] = 0;
	    for (int k = 0 ; k < _m ; k++) {
        x[j] += A[j+kTab[k]] * b[k];
      }
	}
	chol2Calc(Q, n);
	cholSolve(Q, x, n);
	return x;
    }

    /*
    public static void main(String argv[]) {
	//
	// A := array([[10,2,3],[2,14,5],[3,5,16]]);
	// b := evalm(A &* array([1,2,3]));
	int n = 3;
	initL(n);
	double Q[] = {10, 2, 14, 3, 5, 16};
	double x[] = {23, 45, 61};
	chol2Calc(Q, n);
	cholSolve(Q, x, n);
        print("x1", x);
	double A[] = {10, 2, 3, 2, 14, 5, 3, 5, 16};
	double b[] = {23, 45, 61};
        print("x2", leastsquare(A, b, n, n));
    }
    */
}

