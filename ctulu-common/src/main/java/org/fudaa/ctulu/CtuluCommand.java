/**
 *  @creation     20 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;
/**
 * Une interface d�finissant une commande utilisateur qui peut �tre defaite ou refaite.
 * Une commande peut contenir une ou plusieurs op�rations �l�mentaires, chacune d'elles stock�e
 * sous forme de commande (voir {@link CtuluCommandComposite})
 * 
 * @author deniger
 * @version $Id$
 */
public interface CtuluCommand {
  /**
   * Defaire la commande.
   */
  void undo();
  /**
   * Refaire la commande.
   */
  void redo();
}
