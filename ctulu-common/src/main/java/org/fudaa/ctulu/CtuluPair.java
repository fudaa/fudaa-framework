package org.fudaa.ctulu;

/**
 * @author Fred Deniger
 * @param <T>
 */
public class CtuluPair<T> implements Comparable<CtuluPair<T>> {

  private String key;

  private T object;

  public CtuluPair() {
    super();
  }

  public CtuluPair(String key, T object) {
    super();
    this.key = key;
    this.object = object;
  }

  @Override
  public int compareTo(CtuluPair<T> o) {
    if (o == this)
      return 0;
    if (o == null)
      return 1;
    return key.compareTo(o.getKey());
  }

  public String getKey() {
    return key;
  }

  public T getObject() {
    return object;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public void setObject(T object) {
    this.object = object;
  }

}
