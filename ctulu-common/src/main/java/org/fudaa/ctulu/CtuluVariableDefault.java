/*
 * @creation 6 juin 07
 * @modification $Date: 2007-06-11 13:02:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

/**
 * @author fred deniger
 * @version $Id: CtuluVariableDefault.java,v 1.1 2007-06-11 13:02:37 deniger Exp $
 */
public class CtuluVariableDefault implements CtuluVariable {

  final String name_;
  final String id_;

  public CtuluVariableDefault(final String _name, final String _id) {
    name_ = _name;
    id_ = _id;
  }

  public CtuluVariableDefault(final String _id) {
    this(_id, _id);
  }

  @Override
  public Object getCommonUnit() {
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public String getID() {
    return id_;
  }

  @Override
  public String getLongName() {
    return name_;
  }

  @Override
  public String getName() {
    return name_;
  }

}
