/*
 GPL 2
 */
package org.fudaa.ctulu;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 * @author Frederic Deniger
 */
public class LogConverter {
  
  public static CtuluLogLevel getLevel(Level level) {
    if (CtuluAnalyze.FATAL == level) {
      return CtuluLogLevel.FATAL;
    }
    if (CtuluAnalyze.ERROR == level) {
      return CtuluLogLevel.ERROR;
    }
    if (Level.SEVERE == level) {
      return CtuluLogLevel.SEVERE;
    }
    if (Level.INFO == level || Level.FINE == level || Level.FINER == level || Level.FINEST == level || Level.FINE == level) {
      return CtuluLogLevel.INFO;
    }
    if (Level.WARNING == level) {
      return CtuluLogLevel.WARNING;
    }
    return CtuluLogLevel.ERROR;
    
    
  }
  
  public static Level getLevel(CtuluLogLevel level) {
    if (CtuluLogLevel.FATAL == level) {
      return CtuluAnalyze.FATAL;
    }
    if (CtuluLogLevel.ERROR == level) {
      return CtuluAnalyze.ERROR;
    }
    if (CtuluLogLevel.SEVERE == level) {
      return Level.SEVERE;
    }
    if (CtuluLogLevel.INFO == level) {
      return Level.INFO;
    }
    if (CtuluLogLevel.WARNING == level) {
      return Level.WARNING;
    }
    if (CtuluLogLevel.DEBUG == level) {
      return Level.FINE;
    }
    return CtuluAnalyze.ERROR;
    
    
  }
  
  public static CtuluLogRecord convert(LogRecord record) {
    CtuluLogRecord res = new CtuluLogRecord(getLevel(record.getLevel()), record.getMessage());
    res.setLocalizedMessage(record.getMessage());
    res.setArgs(record.getParameters());
    res.setThrown(record.getThrown());
    return res;
  }
  
  public static CtuluLog convert(CtuluAnalyze in) {
    CtuluLog res = new CtuluLog(in.getDefaultResourceBundle());
    res.setDesc(in.getDesc());
    for (LogRecord logRecord : in.getRecords()) {
      res.addRecord(convert(logRecord));
    }
    return res;
  }
  
  public static CtuluAnalyze convert(CtuluLog in) {
    CtuluAnalyze res=new CtuluAnalyze(in.getDefaultResourceBundle());
    res.setDesc(in.getDesc());
    for (CtuluLogRecord logRecord : in.getRecords()) {
      res.addRecord(getLevel(logRecord.getLevel()),logRecord.getMsg(),logRecord.getArgs());
    }
    return res;
  }
  
  public static CtuluLogGroup convert(CtuluAnalyzeGroup in) {
    CtuluLogGroup res = new CtuluLogGroup(in.defaultResource);
    res.setDescription(in.getMainDesc());
    List<CtuluAnalyze> analyser = in.getAnalyser();
    for (CtuluAnalyze ctuluAnalyze : analyser) {
      res.addLog(convert(ctuluAnalyze));
    }
    List<CtuluAnalyzeGroup> groups = in.getGroups();
    for (CtuluAnalyzeGroup group : groups) {
      res.addGroup(convert(group));
      
    }
    return res;
  }
}
