/**
 * @creation 12 d�c. 2003
 * @modification $Date: 2007-12-05 09:25:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Locale;

/**
 * @author deniger
 * @version $Id: CtuluLib.java,v 1.41 2007-12-05 09:25:20 jm_lacombe Exp $
 */
public final class CtuluLib {

  public static boolean isEquals(final Object _o1, final Object _o2) {
    return (_o1 == _o2) || (_o1 != null && _o1.equals(_o2));
  }

  public static boolean isZero(final double _d) {
    return _d < 1E-18 && _d > -1E-18;
  }

  public static boolean isZero(final double _d, final double _eps) {
    return _d < _eps && _d > -_eps;
  }

  public static boolean isEquals(final double _d, final double _d1, final double _eps) {
    return isZero(_d - _d1, _eps);
  }

  public static boolean isEquals(final double _d, final double _d1) {
    return isZero(_d - _d1);
  }

  public static String getHelpProperty() {
    return "HELP_URL";
  }

  public static double getSign(double _d) {
    return _d >= 0 ? 1D : -1D;
  }

  /**
   * @param _m le table a copier
   * @return la table copiee
   */
  public static TObjectDoubleHashMap copy(final TObjectDoubleHashMap _m) {
    final TObjectDoubleHashMap r = new TObjectDoubleHashMap(_m);
    final TObjectDoubleIterator it = _m.iterator();
    for (int i = _m.size(); i-- > 0;) {
      it.advance();
      r.put(it.key(), it.value());
    }
    return r;
  }

  /**
   * @return decimal format avec des points.
   */
  public static DecimalFormat getDecimalFormat() {
    return getDecimalFormat(null);
  }

  public static DecimalFormat getDecimalFormat(final String _s) {
    final DecimalFormat r = _s == null ? new DecimalFormat() : new DecimalFormat(_s);
    r.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
    r.setGroupingUsed(false);
    return r;
  }
  
  public static DecimalFormat getDecimalFormat(int nbMaxFractionDigits) {
    DecimalFormat res=getDecimalFormat();
    res.setMaximumFractionDigits(nbMaxFractionDigits);
    return res;
  }
  
  /**
   * Un format d�cimal sans effet, equivalent � Double.toString(), avec s�parateur
   * "point".
   * @return Le format d�cimal.
   */
  public static DecimalFormat getNoEffectDecimalFormat() {
    DecimalFormat res=getDecimalFormat();
    res.setMinimumFractionDigits(1);
    res.setMaximumFractionDigits(Integer.MAX_VALUE);
    return res;
  }

  /**
   * @return true si la locale est egale a "fr".
   */
  public static boolean isFrenchLanguageSelected() {
    return "fr".equals(Locale.getDefault().getLanguage());
  }

  /**
   * Permet d'enlever plusieurs indices d'un tableau. Les anciennes valeurs peuvent etre enregistree. Attention, si un
   * index n'est pas correct cela peut tout fausser.
   * 
   * @param _init le tableau a modifier
   * @param _sortedIdx les indices a enlever donner dans l'ordre
   * @param _removedValues le tableau qui va contenir (si non null et si de meme taille qui _init) les valeurs enlevees.
   * @return true si toutes les valeurs
   */
  public static boolean removeIdx(final TDoubleArrayList _init, final int[] _sortedIdx, final double[] _removedValues) {
    if (_init == null) { return false; }
    final int n = _init.size();
    final double[] newValues = new double[n];
    int idx = 0;
    if (_removedValues != null) {
      final boolean addOld = _removedValues.length == _sortedIdx.length;
      int idxOld = 0;
      for (int i = 0; i < n; i++) {
        if (Arrays.binarySearch(_sortedIdx, i) < 0) {
          newValues[idx++] = _init.getQuick(i);
        } else if (addOld) {
          _removedValues[idxOld++] = _init.getQuick(i);
        }
      }
    }
    _init.clear();
    _init.add(newValues, 0, idx);
    // true si les tailles concordent
    return n == _sortedIdx.length + _init.size();
  }

  public static final CtuluNumberFormatFixedFigure DEFAULT_NUMBER_FORMAT = new CtuluNumberFormatFixedFigure(8);
  public static final Double ZERO = new Double(0);
  public static final Double UN = new Double(1);

  private CtuluLib() {
    super();
  }

  /**
   * Appele le singleton CTULU.
   * 
   * @param _s la chaine a traduire
   * @return le chaine traduite par ctulu
   */
  public static String getS(final String _s) {
    return CtuluResource.CTULU.getString(_s);
  }

  /**
   * Appele le singleton CTULU.
   * 
   * @param _s la chaine a traduire
   * @param _v1 la chaine remplacant {0}
   * @return le chaine traduite par ctulu
   */
  public static String getS(final String _s, final String _v1) {
    return CtuluResource.CTULU.getString(_s, _v1);
  }

  /**
   * Appele le singleton CTULU.
   * 
   * @param _s la chaine a traduire
   * @param _v1 la chaine remplacant {0}
   * @param _v2 la chaine remplacant {1}
   * @return le chaine traduite par ctulu
   */
  public static String getS(final String _s, final String _v1, final String _v2) {
    return CtuluResource.CTULU.getString(_s, _v1, _v2);
  }

  public static Double getDouble(double _d) {
    if (isZero(_d)) return CtuluLib.ZERO;
    if (isZero(1 - _d)) return CtuluLib.UN;
    return new Double(_d);
  }

  /**
   * Retourne vrai si l'application est lanc� ou d�ploy� avec Java Web Start
   * 
   * @return si l'application est lanc� ou d�ploy� avec Java Web Start
   */
  public static boolean isJavaWebStart() {
    return (System.getProperty("javawebstart.version") != null);
  }

}
