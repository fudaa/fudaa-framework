/**
 * @creation 2 ao�t 2004
 * @modification $Date: 2007-06-20 12:20:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import java.io.File;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Classe utilitaire permettant de manipuler des chaines de caracteres.
 *
 * @author Fred Deniger
 * @version $Id: CtuluLibString.java,v 1.30 2007-06-20 12:20:45 deniger Exp $
 */
public final class CtuluLibString {

  /**
   * Chaine "5". Utile pour la conversion.
   */
  public final static String VIR = ",";
  public final static String DOT = ".";
  public final static String CINQ = "5";
  /**
   * Chaine "2". Utile pour la conversion.
   */
  public final static String DEUX = "2";
  /**
   * Chaine "10". Utile pour la conversion.
   */
  public final static String DIX = "10";
  /**
   * Chaine vide.
   */
  public final static String EMPTY_STRING = "";

  public static boolean containsVirgule(final String _num) {
    if (isEmpty(_num)) {
      return false;
    }
    final String s = _num.trim();
    try {
      Double.parseDouble(s);
      return false;
    } catch (final NumberFormatException e) {
      if (s.indexOf(',') > 0) {
        try {
          Double.parseDouble(FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT));
          return true;
        } catch (final NumberFormatException exWithComma) {

        }
      }
    }
    return false;
  }

  /**
   * @param _s la chaine a tester
   * @return true si la chaine est vide ou null ou si elle ne contient que des espaces
   */
  public static boolean isEmpty(final String _s) {
    if (_s == null || _s == EMPTY_STRING) {
      return true;
    }
    final int nb = _s.length();
    for (int i = nb - 1; i >= 0; i--) {

      if (!Character.isWhitespace(_s.charAt(i))) {
        return false;
      }
    }

    return true;

  }

  /**
   * @param _s1 la premiere chaine a comparer
   * @param _s2 la deuxieme chaine a comparer
   * @return
   */
  public static boolean isEquals(final String _s1, final String _s2) {
    return _s1 == _s2 || (_s1 != null && _s2 != null && _s1.equals(_s2));
  }

  /**
   * @param _s la chaine a tester
   * @return true si c'est un nombre
   */
  public static boolean isNumeric(final String _s) {
    if (isEmpty(_s)) {
      return false;
    }
    final String s = _s.trim();
    try {
      Double.parseDouble(s);
      return true;
    } catch (final NumberFormatException e) {
      if (s.indexOf(',') > 0) {
        return isNumeric(FuLib.replace(s, CtuluLibString.VIR, CtuluLibString.DOT));
      }
      return false;
    }
  }
  /**
   * Tableau vide.
   */
  public final static String[] EMPTY_STRING_ARRAY = new String[0];
  /**
   * Chaine " ".
   */
  public final static String ESPACE = " ";
  /**
   * Chaine "8". Utile pour la conversion.
   */
  public final static String HUIT = "8";
  /**
   * Chaine separatrice de ligne.
   */
  public final static String LINE_SEP = System.getProperty("line.separator");
  public final static String LINE_SEP_SIMPLE = "\n";
  /**
   * Chaine "9". Utile pour la conversion.
   */
  public final static String NEUF = "9";
  /**
   * Chaine "4". Utile pour la conversion.
   */
  public final static String QUATRE = "4";
  /**
   * Chaine "7". Utile pour la conversion.
   */
  public final static String SEPT = "7";
  /**
   * Chaine "6". Utile pour la conversion.
   */
  public final static String SIX = "6";
  /**
   * Chaine "3". Utile pour la conversion.
   */
  public final static String TROIS = "3";
  /**
   * Chaine "1". Utile pour la conversion.
   */
  public final static String UN = "1";
  /**
   * Chaine "0". Utile pour la conversion.
   */
  public final static String ZERO = "0";

  public static String toString(final boolean _b) {
    return _b ? "true" : "false";
  }

  public static boolean toBoolean(final String _b) {
    return "true" == _b || "true".equals(_b);
  }

  /**
   * Ajuste la taille de _s a
   * <code>_taille</code>. Si la taille de _s est plus grande, les derniers caracteres de s seront enleves. Sinon, les espaces n�cessaires seront
   * ajoutes.
   *
   * @param _taille la taille
   * @param _s la chaine a ajuster
   * @return la chaine ajustee.
   */
  public static String adjustSize(final int _taille, final String _s) {
    return adjustSize(_taille, _s, false);
  }

  /**
   * Ajuste la taille de _s a
   * <code>_taille</code>. Si la taille de _s est plus grande, les derniers caracteres de s seront enleves. Sinon, les espaces n�cessaires seront
   * ajoutes.
   *
   * @param _taille la taille
   * @param _s la chaine a ajuster
   * @param _addSpaceBefore true si les espace doivent etre ajoute avant
   * @return la chaine ajustee.
   */
  public static String adjustSize(final int _taille, final String _s, boolean _addSpaceBefore) {
    final String s = _s == null ? EMPTY_STRING : _s;
    final int l = s.length();
    if (l > _taille) {
      return s.substring(0, _taille);
    }
    final StringBuffer r = new StringBuffer(_taille);
    if (!_addSpaceBefore) {
      r.append(s);
    }
    final int lMotif = 5;
    final String b = "     ";
    final int nbMotif = (_taille - l) / lMotif;
    final int sup = (_taille - l) % lMotif;
    for (int i = nbMotif; i > 0; i--) {
      r.append(b);
    }
    switch (sup) {
      case 0:
        break;
      case 1:
        r.append(ESPACE);
        break;
      case 2:
        r.append("  ");
        break;
      case 3:
        r.append("   ");
        break;
      case 4:
        r.append("    ");
        break;
      default:
        break;
    }
    if (_addSpaceBefore) {
      r.append(s);
    }
    if (r.length() != _taille) {
      FuLog.error("length error" + r.length() + " instead of " + _taille);
    }
    return r.toString();
  }

  public static String adjustSizeBefore(final int _taille, final String _s, final char _charToUse) {
    return adjustSize(_taille, _s, true, _charToUse);
  }

  public static String getInfiniSymbol() {
    return new DecimalFormatSymbols().getInfinity();
  }

  public static String adjustSize(final int _taille, final String _s, boolean _addSpaceBefore, final char _charToUse) {
    final String s = _s == null ? EMPTY_STRING : _s;
    final int l = s.length();
    if (l > _taille) {
      return s.substring(0, _taille);
    }
    final StringBuffer r = new StringBuffer(_taille);
    if (!_addSpaceBefore) {
      r.append(s);
    }
    final int lMotif = 5;
    String b = String.valueOf(_charToUse);
    b = b + b + b + b + b;
    final int nbMotif = (_taille - l) / lMotif;
    final int sup = (_taille - l) % lMotif;
    for (int i = nbMotif; i > 0; i--) {
      r.append(b);
    }
    switch (sup) {
      case 0:
        break;
      case 1:
        r.append(_charToUse);
        break;
      case 2:
        r.append(_charToUse).append(_charToUse);
        break;
      case 3:
        r.append(_charToUse).append(_charToUse).append(_charToUse);
        break;
      case 4:
        r.append(_charToUse).append(_charToUse).append(_charToUse).append(_charToUse);
        break;
      default:
        break;
    }
    if (_addSpaceBefore) {
      r.append(s);
    }
    if (r.length() != _taille) {
      FuLog.error("length error" + r.length() + " instead of " + _taille);
    }
    return r.toString();
  }

  public static String adjustSizeBefore(final int _taille, final String _s) {
    return adjustSize(_taille, _s, true);
  }

  public static String getNullString() {
    return "NULL";
  }

  public static String getEmptyTableau() {
    return "[0]";
  }

  private static void appendInArray(final StringBuffer _dest, final String _item) {
    _dest.append(", ").append(_item);
  }

  /**
   * @param _o le tableau d'entier a voir
   * @return une chaine contenant tous les entiers separees par des virgules
   */
  public static String arrayToString(final double[] _o) {
    return arrayToString(_o, ", ");
  }

  public static String arrayToString(final float[] _o) {
    return arrayToString(_o, ", ");
  }

  public static String arrayToString(final int[] _o) {
    return arrayToString(_o, ", ");
  }

  public static String arrayToString(final double[] _o, final String _sep) {
    if (_o == null) {
      return getNullString();
    }
    if (_o.length == 0) {
      return getEmptyTableau();
    }
    final StringBuffer r = new StringBuffer();
    r.append(_o[0]);
    for (int i = 1; i < _o.length; i++) {
      r.append(_sep).append(_o[i]);
    }
    return r.toString();
  }

  public static String arrayToString(final int[] _o, final String _sep) {
    if (_o == null) {
      return getNullString();
    }
    if (_o.length == 0) {
      return getEmptyTableau();
    }
    final StringBuffer r = new StringBuffer();
    r.append(_o[0]);
    for (int i = 1; i < _o.length; i++) {
      r.append(_sep).append(_o[i]);
    }
    return r.toString();
  }

  public static String arrayToString(final float[] _o, final String _sep) {
    if (_o == null) {
      return getNullString();
    }
    if (_o.length == 0) {
      return getEmptyTableau();
    }
    final StringBuffer r = new StringBuffer();
    r.append(_o[0]);
    for (int i = 1; i < _o.length; i++) {
      r.append(_sep).append(_o[i]);
    }
    return r.toString();
  }

  public static String arrayToString(final boolean[] _o, final String _sep) {
    if (_o == null) {
      return getNullString();
    }
    if (_o.length == 0) {
      return getEmptyTableau();
    }
    final StringBuffer r = new StringBuffer();
    r.append(_o[0]);
    for (int i = 1; i < _o.length; i++) {
      r.append(_sep).append(_o[i]);
    }
    return r.toString();
  }

  /**
   * @param _o le tableau d'entier a voir
   * @return une chaine contenant tous les entiers separees par des virgules
   */
  public static String arrayToString(final long[] _o) {
    if (_o == null) {
      return getNullString();
    }
    if (_o.length == 0) {
      return getEmptyTableau();
    }
    final StringBuffer r = new StringBuffer();
    r.append(_o[0]);
    for (int i = 1; i < _o.length; i++) {
      appendInArray(r, Long.toString(_o[i]));
    }
    return r.toString();
  }

  /**
   * @param _o le tableau a transformer en chaine
   * @return une chaine contenant tous les elements du tableau transformes en chaine ( toString) separes par des virgules.
   */
  public static String arrayToString(final Object[] _o) {
    if (_o == null) {
      return getNullString();
    }
    if (_o.length == 0) {
      return getEmptyTableau();
    }
    final StringBuffer r = new StringBuffer();
    r.append(_o[0].toString());
    for (int i = 1; i < _o.length; i++) {
      appendInArray(r, _o[i] == null ? getNullString() : _o[i].toString());
    }
    return r.toString();
  }

  public static String arrayToHtmlString(final Object[] _o) {
    if ((_o == null) || (_o.length == 1)) {
      return EMPTY_STRING;
    }
    final StringBuffer r = new StringBuffer("<html><body>");
    r.append(_o[0].toString());
    for (int i = 1; i < _o.length; i++) {
      r.append("<br>").append(_o[i].toString());
    }
    return r.append("</body></html>").toString();
  }

  /**
   * Transforme un tableau de chaine en une unique chaine avec des saut de lignes.
   *
   * @param _tab le tableau a transformer
   * @return la chaine
   */
  public static String arrayToString(final String[] _tab) {
    return arrayToString(_tab, LINE_SEP);
  }

  /**
   * Transforme un tableau de chaine en une unique chaine avec des saut de lignes.
   *
   * @param _tab le tableau a transformer
   * @return la chaine
   */
  public static String arrayToString(final String[] _tab, final String _sep) {
    return arrayToString(_tab, 0, _sep);

  }

  /**
   * @param _tab
   * @param offset le premier indice a utiliser dans le tableau
   * @param _sep
   * @return
   */
  public static String arrayToString(final String[] _tab, int offset, final String _sep) {
    if (_tab == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final int n = _tab.length;
    if (n > 0) {
      final StringBuffer obj = new StringBuffer();
      for (int i = offset; i < n; i++) {
        obj.append(_tab[i]);
        if (i < n - 1) {
          obj.append(_sep);
        }
      }
      return obj.toString();
    }
    return null;

  }

  /**
   * @param _s la chaine a modifier
   * @return la chaine avec la premier lettre en majuscule.
   */
  public static String capitalyze(final String _s) {
    if (isEmpty(_s)) {
      return _s;
    }
    final String prem = _s.substring(0, 1);
    return prem.toUpperCase() + _s.substring(1);
  }

  /**
   * @param _v la collection a transformer en tableau
   * @return le tableau de string correspondant. Renvoie null si la taille est null ou si _v null.
   */
  public static String[] enTableau(final Collection _v) {
    if ((_v == null) || (_v.size() == 0)) {
      return null;
    }
    final String[] r = new String[_v.size()];
    _v.toArray(r);
    return r;
  }

  /**
   * Recherche parmi parmi le tableau ( en commencant par la fin)
   * <code>_prefixe</code> la chaine. ayant le prefixe demande
   *
   * @param _tabToTest le tableau a parcourir
   * @param _prefixe le prefixe a chercher
   * @return la chaine du tableau ayant le prefixe demande. null si aucune.
   */
  public static String findPrefixInTab(final String[] _tabToTest, final String _prefixe) {
    for (int i = _tabToTest.length - 1; i >= 0; i--) {
      final String s = _tabToTest[i];
      if (_prefixe.startsWith(s)) {
        return s;
      }
    }
    return null;
  }

  /**
   * Recherche dans le tableau la chaine finissant par _end. Commence la recherche a la fin du tableau.
   *
   * @param _tab le tableau a parcourir
   * @param _end le suffixe cherche
   * @return la chaine du tableau finissant par _end. null si aucune
   */
  public static String findStringEndWith(final String[] _tab, final String _end) {
    if (_tab != null) {
      for (int i = _tab.length - 1; i >= 0; i--) {
        final String s = _tab[i];
        if (s.endsWith(_end)) {
          return s;
        }
      }
    }
    return null;
  }

  /**
   * Recherche dans le tableau (a partir de la fin) une chaine contenue dans la ligne a tester (
   * <code>_sToTest</code>).
   *
   * @param _stringToLookFor le tableau a parcourir
   * @param _sToTest la chaine qui doit contenir un des elements du tableau
   * @return La chaine du tableau contenue dans _sToTest
   */
  public static String findStringIn(final String[] _stringToLookFor, final String _sToTest) {
    for (int i = _stringToLookFor.length - 1; i >= 0; i--) {
      final String s = _stringToLookFor[i];
      if (_sToTest.indexOf(s) >= 0) {
        return s;
      }
    }
    return null;
  }

  /**
   * @param _tab le tableau a parcourir
   * @param _start la prefixe a cherche
   * @return la chaine ( d'indice le plus eleve) commencant par _start.
   */
  public static String findStringStartsWith(final String[] _tab, final String _start) {
    for (int i = _tab.length - 1; i >= 0; i--) {
      final String s = _tab[i];
      if (s.startsWith(_start)) {
        return s;
      }
    }
    return null;
  }

  /**
   * Le format renvoy� permet d'ecrire des entiers sur le meme nombre de caract�re. Par exemple <br> NumberFormat
   * fm=getFormgetFormatForIndexingInteger(5); <br> String cinq= fm.format(5); //cinq =00005 String quinze=fm.format(15); //quinze=00015
   *
   * @param _nb le nombre d'entier a consid�rer
   * @return un format permettant d'ecrire les entiers de 0 a _nb sur le meme nombre de caract�res
   */
  public static NumberFormat getFormatForIndexingInteger(final int _nb) {
    final double log = Math.log(_nb) / Math.log(10);
    int logf = 0;
    if (!Double.isInfinite(log) && !Double.isNaN(log)) {
      logf = ((int) log) + 1;
    }
    final DecimalFormat fm = new DecimalFormat();
    fm.setMaximumFractionDigits(0);
    fm.setGroupingUsed(false);
    fm.setMinimumIntegerDigits(logf);
    fm.setParseIntegerOnly(true);
    return fm;
  }

  /**
   * Renvoie sous forme de String l'objet _o. Pour une matrice, renvoie "[taille]" si non verbose. Si _verbose, renvoie la description de tous les
   * objets de la matrice (peut etre ennuyeux si les matrices sont tres grandes.
   *
   * @param _o l'objet a decrire
   * @param _verbose si true decrit tous les objets de la matrice.
   * @return descriprion
   */
  public static String getObjectInString(final Object _o, final boolean _verbose) {
    if (_o == null) {
      return "null";
    }
    final StringBuffer b = new StringBuffer();
    if (_o.getClass().isArray()) {
      final int n = Array.getLength(_o);
      b.append("  [");
      if (_verbose) {
        if (n > 0) {
          b.append(getObjectInString(Array.get(_o, 0), _verbose));
          for (int j = 1; j < n; j++) {
            b.append(CtuluLibString.VIR).append(getObjectInString(Array.get(_o, j), _verbose));
          }
        }
      } else {
        b.append(n);
      }
      b.append(']');
    } else {
      b.append(ESPACE).append(_o.toString());
    }
    return b.toString();
  }

  /**
   * Utilise une constante si _i &lt11. Sinon utilise la methode Integer.
   *
   * @param _i l'entier a transformer en chaine
   * @return la chaine correpondante
   */
  public static String getString(final int _i) {
    if (_i < 11) {
      switch (_i) {
        case 0:
          return ZERO;
        case 1:
          return UN;
        case 2:
          return DEUX;
        case 3:
          return TROIS;
        case 4:
          return QUATRE;
        case 5:
          return CINQ;
        case 6:
          return SIX;
        case 7:
          return SEPT;
        case 8:
          return HUIT;
        case 9:
          return NEUF;
        case 10:
          return DIX;
        default:
          return Integer.toString(_i);
      }
    }
    return Integer.toString(_i);
  }

  public static String getEspaceString(final int _i) {
    return ESPACE + getString(_i);
  }

  /**
   * Methode permettant de parser les arguments passes a une methode main. Stocke les valeurs pour chaque
   * <code>_ids</code> dans la table de correspondance passee en argument. Les arguments du type -f=monFichier seront ajoutee sous la forme cle=-f et
   * valeur=monFichier. Les arguments de type -l donneront cle=-l et valeur=null.
   *
   * @param _args les argument a parser
   * @return table cle,valeur ( voir doc)
   */
  public static synchronized Map parseArgs(final String[] _args) {
    if (_args == null) {
      return null;
    }
    final int l = _args.length;
    final Map r = new HashMap(l);
    int index;
    String champ;
    for (int i = 0; i < l; i++) {
      champ = _args[i];
      index = champ.indexOf('=');
      if (index < 0) {
        r.put(champ, null);
      } else {
        r.put(champ.substring(0, index), champ.substring(index + 1, champ.length()));
      }
    }
    return r;
  }

  /**
   * Permet de decouper une chaine grace a un separateur. Par exemple, l'appel parseString("1;3;4" , ";") renvoie [1,3,4];
   *
   * @param _s la chaine a traiter
   * @param _sepChar le separateur
   * @return le tableau des champs ou null si un des 2 parametres est nul.
   */
  public static String[] parseString(final String _s, final String _sepChar) {
    if ((_s == null) || (_sepChar == null)) {
      return null;
    }
    return enTableau(parseStringList(_s, _sepChar));
  }

  public static double[] parseStringDouble(final String _s, final String _sepChar) {
    if ((_s == null) || (_sepChar == null)) {
      return null;
    }
    final List l = (parseStringList(_s, _sepChar));
    final TDoubleArrayList res = new TDoubleArrayList();
    for (int i = 0; i < l.size(); i++) {
      try {
        res.add(CtuluDoubleParser.parseValue((String) l.get(i)));
      } catch (final NumberFormatException _evt) {
        FuLog.error(_evt);

      }
    }
    return res.toNativeArray();
  }

  public static boolean[] parseStringBoolean(final String _s, final String _sepChar) {
    if ((_s == null) || (_sepChar == null)) {
      return null;
    }
    final List l = (parseStringList(_s, _sepChar));
    final List<Boolean> res = new ArrayList<Boolean>();
    for (int i = 0; i < l.size(); i++) {
      try {
        res.add(Boolean.parseBoolean((String) l.get(i)));
      } catch (final NumberFormatException _evt) {
        FuLog.error(_evt);

      }
    }
    boolean[] tab = new boolean[res.size()];
    int cpt = 0;
    for (Boolean bool : res) {
      tab[cpt++] = bool.booleanValue();
    }

    return tab;
  }

  /**
   * Permet de decouper une chaine grace a un separateur. Par exemple, l'appel parseString("1;3;4" , ";") renvoie [1,3,4];
   *
   * @param _s la chaine a traiter
   * @param _sepChar le separateur
   * @return le tableau des champs ou null si un des 2 parametres est nul.
   */
  public static List parseStringList(final String _s, final String _sepChar) {
    if ((_s == null) || (_sepChar == null)) {
      return null;
    }
    final StringTokenizer tk = new StringTokenizer(_s, _sepChar);
    final List l = new ArrayList();
    while (tk.hasMoreTokens()) {
      l.add(tk.nextToken().trim());
    }
    return l;
  }

  public static String trimRight(String _s) {
    if (_s == null || _s.length() == 0) {
      return _s;
    }
    int idxToTrim = _s.length() - 1;
    while (idxToTrim >= 0 && Character.isWhitespace(_s.charAt(idxToTrim))) {
      idxToTrim--;
    }
    if (idxToTrim < 0) {
      return EMPTY_STRING;
    }
    if (idxToTrim == _s.length() - 1) {
      return _s;
    }
    return _s.substring(0, idxToTrim + 1);

  }

  /**
   * A partir d'un nom de fichier ou d'une url locale incorrecte, construit une url locale valide.
   *
   * @return url valide pour _file
   * @param _file le chemin du fichier a transformer
   */
  public static String pathToUrl(final String _file) {
    String path = FuLib.decodeWwwFormUrl(new File(_file).toURI().getPath());
    if (FuLib.isWindows()) {
      path = FuLib.replace(path.substring(1), "/", "//");
    }
    return "file://" + path;
  }

  /**
   * Classe utilitaire : constructeur prive.
   */
  private CtuluLibString() {
    super();
  }
}