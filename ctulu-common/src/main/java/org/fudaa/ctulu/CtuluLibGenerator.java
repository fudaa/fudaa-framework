package org.fudaa.ctulu;

import com.memoire.fu.FuLib;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Generateur d'Id uniques en singleton.
 * Gere des contraintes (id ajout�s, retir�s)
 * afin d'�viter des doublons.
 * 
 * Gen�re des nombres comprise entre  0 et MAX-1. 
 * 
 * @author Adrien Hadoux
 *
 */
public class CtuluLibGenerator {

	private static CtuluLibGenerator generator_;
	
	/**
	 * Tous les nombres seront inf�rieurs a Max-1
	 */
	private  long max=65535;
	
	/**
	 * Pour avoir toujours la meme sequence, il suffit de mettre le mode debug a true,
	 * cela initialisera toujours la seed de la mme facon.
	 * Pratique pour tester
	 */
	private boolean modeDebug=false;
	
	//Random randomizer_;
	
	/**
	 * Blacklist des elements a ne pas generer.
	 */
	List<Long> constraints_;
	
	/**
	 * Le dernier nombre g�n�r� r�inject� dans la formule
	 */
	long old;
	
	/**
	 * a-1 doit �tre un multiple de p, pour tout p nombre premier diviseur de MAX 
	 */
	long a_=25;
	
	/**
	 * c et MAX doivent �tre premiers entre eux
	 * 
	 */
	long c_=16;
	
	private CtuluLibGenerator(){
		constraints_=new ArrayList<Long>();
		//getPseudoAleatoire();
		old=initSeed();
	}
	
	/**
	 * Initialise la graine avec l'heure actuelle.
	 * @return
	 */
	private long initSeed(){
		if(modeDebug)
			return 0;
		return Calendar.getInstance().getTimeInMillis();
	}
	
//	private Random getPseudoAleatoire(){
//		if(randomizer_==null)
//			randomizer_=new Random(initSeed());
//		return randomizer_;
//	}
	
	
	public long useCongruenceLineaireGenerateur(){
		old= (a_*old + c_)% max;
		return old;
	}
	
	/**
	 * Accesseur du singleton. 
	 * @return
	 */
	public static CtuluLibGenerator getInstance(){
		if(generator_==null)
			generator_=new CtuluLibGenerator();
		return generator_;
	}
	
	
	/**
	 * Donne un id unique
	 * @return
	 */
	public long deliverUniqueId(){
		int nbTentatives=0;
		Long id;
		do{
			nbTentatives++;
			if(nbTentatives>max-1){
				clear();
			}
				
			id=new Long(useCongruenceLineaireGenerateur());
		}
		while(constraints_.contains(id));
		addConstraint(id);
		
		return id;
	}
	
	/**
	 * version string
	 * @return
	 */
	public String deliverUniqueStringId(){
		
		return ""+deliverUniqueId();
	}
	
	public String deliverUniqueStringId(String name){
		
		return FuLib.clean(name)+deliverUniqueId();
	}
	
	/**
	 * Clear la liste des contraintes
	 */
	public void clear(){
		constraints_=new ArrayList<Long>();
	}
	
	
	public void addConstraint(long val){
		constraints_.add(val);
	}
	public void addConstraint(String value){
		long val=Long.parseLong(value);
		constraints_.add(val);
	}
	public void addConstraint(List<Long> list){
		constraints_.addAll(list);
	}
	
	public void removeConstraint(long val){
		constraints_.remove(new Long(val));
	}
	public void removeConstraint(String value){
		long val=Long.parseLong(value);
		constraints_.remove(new Long(val));
	}

	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}
}
