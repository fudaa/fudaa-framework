/**
 * @creation 25 f�vr. 2005
 * @modification $Date: 2007-05-21 10:28:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 * @author Fred Deniger
 * @version $Id: CtuluNumberFormatDefault.java,v 1.8 2007-05-21 10:28:29 deniger Exp $
 */
public class CtuluNumberFormatDefault extends CtuluNumberFormat {

  public static CtuluNumberFormatDefault DEFAULT_FMT = new CtuluNumberFormatDefault();
  public static CtuluNumberFormatDefault DEFAULT_FMT_3DIGITS = new CtuluNumberFormatDefault();
  static {
    final NumberFormat fmt1 = CtuluLib.getDecimalFormat();
    fmt1.setMaximumFractionDigits(2);
    DEFAULT_FMT.setFmt(fmt1);
    final NumberFormat fmt = CtuluLib.getDecimalFormat();
    fmt.setMaximumFractionDigits(3);
    DEFAULT_FMT_3DIGITS.setFmt(fmt);
  }

  /**
   * @author fred deniger
   * @version $Id: CtuluNumberFormatDefault.java,v 1.8 2007-05-21 10:28:29 deniger Exp $
   */
  protected static final class NoneFormater extends CtuluNumberFormatDefault {
    private final boolean decimal_;

    /**
     * @param _fmt
     * @param _decimal
     */
    protected NoneFormater(NumberFormat _fmt, boolean _decimal) {
      super(_fmt);
      decimal_ = _decimal;
    }

    @Override
    public String toString() {
      return CtuluLib.getS("Aucun");
    }

    @Override
    public boolean isDecimal() {
      return decimal_;
    }
  }

  private final static NumberFormat DEFAULT = CtuluLib.getDecimalFormat();

  public static CtuluNumberFormatDefault buildNoneFormatter() {
    return new NoneFormater(null, true);
  }

  public static CtuluNumberFormatDefault buildNoneFormatter(int _nbMaxDigits, final boolean _isDecimal) {
    NumberFormat fmt = CtuluLib.getDecimalFormat(_nbMaxDigits);
    return new NoneFormater(fmt, _isDecimal);
  }

  NumberFormat fmt_;

  public CtuluNumberFormatDefault() {
    this(null);
  }

  public static String formatPoint(double x, double y, CtuluNumberFormatI formatter) {
    if (formatter == null) { return "(" + x + ";" + y + ")"; }
    return "(" + formatter.format(x) + ";" + formatter.format(y) + ")";
  }

  /**
   * @param _fmt
   */
  public CtuluNumberFormatDefault(final NumberFormat _fmt) {
    super();
    fmt_ = _fmt == null ? DEFAULT : _fmt;
  }

  @Override
  public String format(final double _d) {
    if (fmt_ == null) { return Double.toString(_d); }
    return fmt_.format(_d);
  }

  @Override
  public StringBuffer format(final Object _obj, final StringBuffer _toAppendTo, final FieldPosition _pos) {
    return fmt_.format(_obj, _toAppendTo, _pos);
  }

  public DecimalFormat cloneDecimalFormat() {
    return (DecimalFormat) fmt_.clone();
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    return new CtuluNumberFormatDefault(fmt_);
  }

  @Override
  public Object parseObject(final String _source, final ParsePosition _pos) {
    return fmt_.parseObject(_source, _pos);
  }

  /**
   * @param _fmt le format a utiliser peut etre null
   */
  public final void setFmt(final NumberFormat _fmt) {
    fmt_ = _fmt == null ? DEFAULT : _fmt;
  }

  public NumberFormat getFmt() {
    return fmt_;
  }
  
  @Override
  public String toLocalizedPattern() {
    return fmt_ instanceof DecimalFormat ? ((DecimalFormat) fmt_).toLocalizedPattern() : null;
  }

}