/*
 * @creation 22 oct. 2004
 * @modification $Date: 2006-09-19 14:36:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * @author Fred Deniger
 * @version $Id: CtuluSystemEnv.java,v 1.7 2006-09-19 14:36:53 deniger Exp $
 */
public final class CtuluSystemEnv {
  private CtuluSystemEnv() {
    super();
  }

  public static String[] transfEnvVarForProcess(final Map<String, String> _props) {
    if (_props == null) {
      return null;
    }
    final String[] r = new String[_props.size()];
    int idx = 0;
    for (final Iterator it = _props.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry e = (Map.Entry) it.next();
      r[idx++] = e.getKey() + "=" + e.getValue();
    }
    return r;
  }

  public static Map<String, String> getEnvVars() {
    return System.getenv();
  }
}
