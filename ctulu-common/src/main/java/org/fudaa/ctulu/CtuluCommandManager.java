/*
 *  @creation     21 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuPreferences;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Une classe permettant de g�rer une liste de commande: les actions undo/redo recherche la derni�re action effectu�e et
 * transmet l'action undo/redo � cette derniere puis met � jour l'�tat de la liste: reste-t-til des action �
 * annuler/refaire, .... <br>
 * Cette classe sera utilis� par le cote applicatif (BuCommonImplementation, ...) et permet � l'utilisateur de savoir
 * s'il reste des actions annulables ou non. <br>
 * Des qu'une action est ajout�e ou une action redo/undo est effectu�e, un evenement est envoy� au CtuluCmdMngListener.
 * 
 * @author Fred Deniger
 * @version $Id$
 */
public class CtuluCommandManager implements CtuluCommandContainer {

  private boolean canRedo_;
  private boolean canUndo_;
  private LinkedList cmd_;
  private int indexLastUndoCmd_;
  private CtuluCmdMngListener listener_;
  private int nbMaxCommand_ = BuPreferences.BU.getIntegerProperty("undo.cmd.nb", 15);

  private List<CtuluCmdMngListener> listeners;

  public void addListener(CtuluCmdMngListener newListener) {
    if (listeners == null) listeners = new ArrayList<CtuluCmdMngListener>();
    listeners.add(newListener);
  }
  
  public void removeListener(CtuluCmdMngListener listenerToRemove) {
    if (listeners != null) listeners.remove(listenerToRemove);
  }

  CtuluCommandPersitant cmdPersistant_;

  public CtuluCommandManager() {}

  /**
   * @param _maxNb le nombre max de commandes a stocker
   */
  public CtuluCommandManager(final int _maxNb) {
    if (_maxNb > 0) {
      nbMaxCommand_ = _maxNb;
    }
  }

  private void update() {
//    boolean change = false;
    boolean newValue = (cmdPersistant_ != null) || (((cmd_ != null) && (cmd_.size() > 0) && (indexLastUndoCmd_ > 0)));
    if (newValue != canUndo_) {
      canUndo_ = newValue;
//      change = true;
    }
    newValue = cmdPersistant_ == null && ((cmd_ != null) && (cmd_.size() > 0) && (indexLastUndoCmd_ != cmd_.size()));
    if (newValue != canRedo_) {
      canRedo_ = newValue;
//      change = true;
    }
    // B.M. : Appel� a chaque mise a jour de commande, car la commande poss�de un nom qui doit �tre visualis� dans l'ihm.
    if (/*change && */(listener_ != null)) {
      listener_.undoredoStateChange(this);
    }
     if(listeners!=null){
        for (CtuluCmdMngListener listener : listeners) {
          listener.undoredoStateChange(this);
        }
      }
  }

  /**
   * @param _c la commande a ajouter
   */
  @Override
  public boolean addCmd(final CtuluCommand _c) {
    if (_c == null) {
      return false;
    }
    if (cmd_ == null) {
      cmd_ = new LinkedList();
      cmd_.add(_c);
      indexLastUndoCmd_ = 1;
    } else {
      // reset undo command
      if (indexLastUndoCmd_ != cmd_.size()) {
        final int nbToRemove = cmd_.size() - indexLastUndoCmd_;
        for (int i = nbToRemove; i > 0; i--) {
          cmd_.removeLast();
        }
      }
      // add the new command
      cmd_.add(_c);
      if (cmd_.size() > nbMaxCommand_) {
        cmd_.removeFirst();
      }
      indexLastUndoCmd_ = cmd_.size();
    }
    update();
    return true;
  }

  /**
   * @return true si une commande peut etre refaite
   */
  public boolean canRedo() {
    return canRedo_;
  }

  /**
   * @return true si une commande peut etre defaite
   */
  public boolean canUndo() {
    return canUndo_;
  }

  /**
   * Efface toutes les commandes.
   */
  public void clean() {
    if ((cmd_ != null) && (cmd_.size() > 0)) {
      cmd_.clear();
      indexLastUndoCmd_ = 0;
      update();
      if (listener_ != null) {
        listener_.undoredoStateChange(this);
      }
    }
  }

  /**
   * @param _c la commande a tester
   * @return true si contient la commande <code>_c</code>.
   */
  public boolean containsCmd(final CtuluCommand _c) {
    return cmd_.contains(_c);
  }

  /**
   * @return une commande persistante: une commande qui n'est pas ajout�e � la liste ( temporaire) mais qui devra �tre
   *         annul�e au prochain appel � undo.
   */
  public final CtuluCommandPersitant getCmdPersistant() {
    return cmdPersistant_;
  }

  /**
   * @return Returns the listener.
   */
  public CtuluCmdMngListener getListener() {
    return listener_;
  }

  /**
   * @return le nombre de commande actuellement stockees
   */
  public int getNbCmd() {
    return cmd_.size();
  }

  /**
   * Le nombre max de commandes stock�es est modifiable via la propri�t�: undo.cmd.nb de Bu. Par d�faut elle 15.
   * 
   * @return le nombre max de commandes a gerer
   */
  public int getNbMaxCmd() {
    return nbMaxCommand_;
  }

  /**
   * @return true si au moins une commande a ete defaite.
   */
  public boolean isACommandUndo() {
    return indexLastUndoCmd_ != cmd_.size();
  }

  /**
   * @param _c la commande a tester
   * @return true si commande _c a ete defaite
   */
  public boolean isUndo(final CtuluCommand _c) {
    return isUndo(cmd_.indexOf(_c));
  }

  /**
   * @param _idx l'indice de la commande a tester.
   * @return true si la commande a ete defaite.
   */
  public boolean isUndo(final int _idx) {
    return _idx >= indexLastUndoCmd_;
  }

  /**
   * Refaire la derniere commande defaite.
   */
  public void redo() {
    if (canRedo()) {
      ((CtuluCommand) cmd_.get(indexLastUndoCmd_++)).redo();
      update();
    }
  }
  
  /**
   * Retourne le nom de la commande a d�faire. Affich� dans le texte de la commande "undo".
   * @return Le nom de la commande. Null si pas de nom.
   */
  public String getUndoName() {
    if (!canUndo()) return null;
    int ind=indexLastUndoCmd_-1;
    if (ind<0) return null;
    CtuluCommand cmd=(CtuluCommand)cmd_.get(ind);
    if (cmd instanceof CtuluNamedCommand)
      return ((CtuluNamedCommand)cmd).getName();
    else 
      return null;
  }
  
  /**
   * Retourne le nom de la commande a refaire. Affich� dans le texte de la commande "redo".
   * @return Le nom de la commande. Null si pas de nom.
   */
  public String getRedoName() {
    if (!canRedo()) return null;
    int ind=indexLastUndoCmd_;
    CtuluCommand cmd=(CtuluCommand)cmd_.get(ind);
    if (cmd instanceof CtuluNamedCommand)
      return ((CtuluNamedCommand)cmd).getName();
    else 
      return null;
  }

  /**
   * Enleve le listener.
   */
  public void removeListener() {
    listener_ = null;
  }

  /**
   * Une commande persistante est une action qui a la priorit� sur les autres et qui sera annul�e tant qu'elle le peut:
   * tant que sa m�thode canUndo renvoie <code>true</code>.<br>
   * Un evenement est envoy� indiquant que l'�tat de ce manager a �t� modifi�.
   * 
   * @param _cmdPersistant une commande temporaire qui n'est pas ajout�e � la liste des actions: elle est seulement
   *          annulable et reste active tant quelle est annulable.
   * @see CtuluCommandPersitant
   */
  public final void setCmdPersistant(final CtuluCommandPersitant _cmdPersistant) {
    cmdPersistant_ = _cmdPersistant;
    if (cmdPersistant_ != null && !cmdPersistant_.canUndo()) {
      cmdPersistant_ = null;
    }
    update();
  }

  /**
   * @param _l le listener recevant les evt lorsque une commande est faite/defaite.
   */
  public void setListener(final CtuluCmdMngListener _l) {
    if ((_l != null) && (_l != listener_)) {
      listener_ = _l;
    }
  }

  /**
   * Defaire la derniere commande non defaite.
   */
  public void undo() {
    if (canUndo()) {
      // une commande tempo est presente: on l'annule
      if (cmdPersistant_ != null && cmdPersistant_.canUndo()) {
        cmdPersistant_.undo();
        // s'il reste des choses � annuler on conserve cette commande
        if (!cmdPersistant_.canUndo()) {
          // sinon on l'oublie
          cmdPersistant_ = null;
        }
      } else {
        // cas simple: on defait la derniere commande
        ((CtuluCommand) cmd_.get(--indexLastUndoCmd_)).undo();
      }
      // le statut de la commande persistante a �t� modifi� par une autre classe et n'a pas �t� annul�e par ce manager:
      // on l'oublie en la mettant � null.
      if (cmdPersistant_ != null && !cmdPersistant_.canUndo()) {
        cmdPersistant_ = null;
      }
      update();
    }

  }

}
