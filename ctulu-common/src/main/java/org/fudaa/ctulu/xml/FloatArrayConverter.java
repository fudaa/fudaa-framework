/*
 GPL 2
 */
package org.fudaa.ctulu.xml;

import com.memoire.fu.FuEmptyArrays;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import gnu.trove.TFloatArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;

/**
 *
 * @author Frederic Deniger
 */
public class FloatArrayConverter implements Converter {

  private String emptyValue = CtuluLibString.getEmptyTableau();

  public FloatArrayConverter() {
  }

  public FloatArrayConverter(String emptyValue) {
    this.emptyValue = emptyValue;
  }

  public String getEmptyValue() {
    return emptyValue;
  }

  public void setEmptyValue(String emptyValue) {
    this.emptyValue = emptyValue;
  }

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    float[] values = (float[]) source;
    if (values == null || values.length == 0) {
      writer.setValue(emptyValue);
    } else {
      writer.setValue(CtuluLibString.arrayToString(values, " "));
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    String valueAsText = reader.getValue();
    if (CtuluLibString.getEmptyTableau().equals(valueAsText) || emptyValue.equals(valueAsText)) {
      return FuEmptyArrays.FLOAT0;
    } else if (CtuluLibString.getNullString().equals(valueAsText)) {
      return null;
    }
    TFloatArrayList res = new TFloatArrayList();
    String[] split = StringUtils.split(valueAsText, " ");
    for (String value : split) {
      res.add(Float.parseFloat(value));
    }
    return res.toNativeArray();
  }

  @Override
  public boolean canConvert(Class type) {
    return float[].class.equals(type);
  }
}
