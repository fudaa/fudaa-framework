/*
 GPL 2
 */
package org.fudaa.ctulu.xml;

import com.memoire.fu.FuEmptyArrays;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.util.ArrayList;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;

/**
 *
 * @author Frederic Deniger
 */
public class BooleanArrayConverter implements Converter {

  private String emptyValue = CtuluLibString.getEmptyTableau();

  public BooleanArrayConverter() {
  }

  public BooleanArrayConverter(String emptyValue) {
    this.emptyValue = emptyValue;
  }

  public String getEmptyValue() {
    return emptyValue;
  }

  public void setEmptyValue(String emptyValue) {
    this.emptyValue = emptyValue;
  }

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    boolean[] values = (boolean[]) source;
    if (values == null || values.length == 0) {
      writer.setValue(emptyValue);
    } else {
      writer.setValue(CtuluLibString.arrayToString(values, " "));
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    String value = reader.getValue();
    if (CtuluLibString.getEmptyTableau().equals(value) || emptyValue.equals(value)) {
      return FuEmptyArrays.BOOLEAN0;
    } else if (CtuluLibString.getNullString().equals(value)) {
      return null;
    }
    ArrayList res = new ArrayList();
    String[] split = StringUtils.split(value, " ");
    for (String string : split) {
      res.add(Boolean.parseBoolean(string));
    }
    Boolean[] booleans = (Boolean[]) res.toArray(new Boolean[res.size()]);
    return ArrayUtils.toPrimitive(booleans);
  }

  @Override
  public boolean canConvert(Class type) {
    return boolean[].class.equals(type);
  }
}
