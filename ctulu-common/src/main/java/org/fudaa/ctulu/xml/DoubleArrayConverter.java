/*
 GPL 2
 */
package org.fudaa.ctulu.xml;

import com.memoire.fu.FuEmptyArrays;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import gnu.trove.TDoubleArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;

/**
 *
 * @author Frederic Deniger
 */
public class DoubleArrayConverter implements Converter {

  private String emptyValue = CtuluLibString.getEmptyTableau();

  public DoubleArrayConverter() {
  }

  public DoubleArrayConverter(String emptyValue) {
    this.emptyValue = emptyValue;
  }

  public String getEmptyValue() {
    return emptyValue;
  }

  public void setEmptyValue(String emptyValue) {
    this.emptyValue = emptyValue;
  }

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    double[] values = (double[]) source;
    if (values == null || values.length == 0) {
      writer.setValue(emptyValue);
    } else {
      writer.setValue(CtuluLibString.arrayToString(values, " "));
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    try {
      String value = reader.getValue();
      if (CtuluLibString.getEmptyTableau().equals(value) || emptyValue.equals(value)) {
        return FuEmptyArrays.DOUBLE0;
      } else if (CtuluLibString.getNullString().equals(value)) {
        return null;
      }
      TDoubleArrayList res = new TDoubleArrayList();
      String[] split = StringUtils.split(value, " ");
      for (String string : split) {
        res.add(Double.parseDouble(string));
      }
      return res.toNativeArray();
    } catch (Exception numberFormatException) {
    }
    return FuEmptyArrays.DOUBLE0;
  }

  @Override
  public boolean canConvert(Class type) {
    return double[].class.equals(type);
  }
}
