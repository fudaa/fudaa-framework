/*
 GPL 2
 */
package org.fudaa.ctulu.xml;

import com.Ostermiller.util.CSVParser;
import com.Ostermiller.util.CSVPrinter;
import com.memoire.fu.FuEmptyArrays;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;

/**
 *
 * @author Frederic Deniger
 */
public class StringArrayConverter implements Converter {

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    String[] values = (String[]) source;
    if (values == null) {
      writer.setValue(CtuluLibString.getNullString());
    } else if (values.length == 0) {
      writer.setValue(StringUtils.EMPTY);
    } else {
      StringWriter stringWriter = new StringWriter();
      CSVPrinter printer = new CSVPrinter(stringWriter);
      printer.setAlwaysQuote(true);
      printer.changeDelimiter(' ');
      printer.print(values);
      try {
        printer.flush();
      } catch (IOException ex) {
        Logger.getLogger(StringArrayConverter.class.getName()).log(Level.SEVERE, null, ex);
      }
      final String toString = stringWriter.toString();
      writer.setValue(toString);
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    try {
      String value = reader.getValue();
      if (StringUtils.isBlank(value)) {
        return FuEmptyArrays.STRING0;
      } else if (CtuluLibString.getNullString().equals(value)) {
        return null;
      }
      CSVParser parser = new CSVParser(new StringReader(value));
      parser.changeDelimiter(' ');
      return parser.getLine();
    } catch (IOException ex) {
      Logger.getLogger(StringArrayConverter.class.getName()).log(Level.SEVERE, null, ex);
    }
    return FuEmptyArrays.STRING0;
  }

  @Override
  public boolean canConvert(Class type) {
    return String[].class.equals(type);
  }
}
