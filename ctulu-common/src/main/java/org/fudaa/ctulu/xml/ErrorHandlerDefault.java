/*
 GPL 2
 */
package org.fudaa.ctulu.xml;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLog;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author deniger
 */
public final class ErrorHandlerDefault implements ErrorHandler {

  private boolean hasError;
  /**
   *
   */
  private final CtuluLog res;

  /**
   * @param res
   */
  public ErrorHandlerDefault(final CtuluLog res) {
    this.res = res;
  }

  @Override
  public void error(final SAXParseException exception) throws SAXException {
    hasError = true;
    res.addSevereError(CtuluLib.getS("Ligne {0}: le fichier xml n'est pas valide. Message: {1}", Integer.toString(exception.getLineNumber()), exception.getMessage()));
  }

  @Override
  public void fatalError(final SAXParseException exception) throws SAXException {
    hasError = true;
    res.addSevereError(CtuluLib.getS("Ligne {0}: le fichier xml n'est pas valide. Message: {1}", Integer.toString(exception.getLineNumber()), exception.getMessage()));

  }

  /**
   * @return the hasError
   */
  public boolean isHasError() {
    return hasError;
  }

  @Override
  public void warning(final SAXParseException exception) throws SAXException {
    res.addWarnFromFile(exception.getMessage(), exception.getLineNumber());

  }
}