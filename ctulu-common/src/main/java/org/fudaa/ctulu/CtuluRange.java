/**
 * @creation 23 juin 2004
 * @modification $Date: 2007-02-07 09:54:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import gnu.trove.TDoubleArrayList;

/**
 * @author Fred Deniger
 * @version $Id: CtuluRange.java,v 1.12 2007-02-07 09:54:11 deniger Exp $
 */
public class CtuluRange {

  public CtuluRange() {
  }

  public CtuluRange(final CtuluRange _range) {
    initWith(_range);
  }

  public CtuluRange(final double _min, final double _max) {
    min_ = _min;
    max_ = _max;
  }

  public CtuluRange intersectWith(CtuluRange other) {
    if (isNill() || other == null || other.isNill()) {
      return new CtuluRange();
    }
    double min = Math.max(getMin(), other.getMin());
    double max = Math.min(getMax(), other.getMax());
    return new CtuluRange(min, max);

  }
  /**
   * Le min.
   */
  public double min_;
  /**
   * Le max.
   */
  public double max_ = -1;

  public boolean isNill() {
    return max_ < min_;
  }

  public void setToNill() {
    min_ = 0;
    max_ = -1;
  }

  /**
   * Modifie le range afin de contenir la nouvelle valeur.
   */
  public void expandTo(final double _value) {
    if (isNill()) {
      max_ = _value;
      min_ = _value;
    } else if (_value < min_) {
      min_ = _value;
    } else if (_value > max_) {
      max_ = _value;
    }
  }

  public void expandTo(final CtuluRange _range) {
    if (_range == null || _range.isNill()) {
      return;
    }
    expandTo(_range.max_);
    expandTo(_range.min_);
  }

  @Override
  public String toString() {
    return "range min=" + min_ + " max=" + max_;
  }

  /**
   * @param _value la valeur a tester
   * @return true si la valeur est contenue dans l'intervalle
   */
  public boolean isValueContained(final double _value) {
    return (_value >= min_) && (_value <= max_);
  }

  /**
   * @param _value la valeur a tester
   * @return true si la valeur est strictement contenue dans l'intervalle
   */
  public boolean isValueStrictlyContained(final double _value) {
    return (_value > min_) && (_value < max_);
  }

  /**
   * Initialise cet intervalle a partir de _r.
   *
   * @param _range l'intervalle servant a l'initialisation
   */
  public final void initWith(final CtuluRange _range) {
    if (_range != null) {
      min_ = _range.min_;
      max_ = _range.max_;
    }
  }

  /**
   * @param _l la liste a parcourir
   * @param _r l'intervalle qui contiendra le min,max
   */
  public static final void getRangeFor(final TDoubleArrayList _l, final CtuluRange _r) {
    if (_l.size() <= 0) {
      _r.max_ = 0;
      _r.min_ = 0;
    } else {
      _r.max_ = _l.getQuick(0);
      _r.min_ = _r.max_;
      for (int i = _l.size() - 1; i > 0; i--) {
        _r.expandTo(_l.getQuick(i));
      }
    }
  }

  /**
   * @param _l la liste a parcourir
   * @param _r l'intervalle qui contiendra le min,max
   */
  public static final void getRangeFor(final double[] _l, final CtuluRange _r) {
    if (CtuluLibArray.isEmpty(_l)) {
      _r.max_ = 0;
      _r.min_ = 0;
    } else {
      _r.max_ = _l[0];
      _r.min_ = _r.max_;
      for (int i = _l.length - 1; i > 0; i--) {
        _r.expandTo(_l[i]);
      }
    }
  }

  public double getMax() {
    return max_;
  }

  public void setMax(final double _max) {
    max_ = _max;
  }

  public double getMin() {
    return min_;
  }

  public void setMin(final double _min) {
    min_ = _min;
  }
}