/*
 *  @creation     5 ao�t 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuValueValidator;


/**
 * Validator pour les expressions.
 * @author Fred Deniger
 * @version $Id: CtuluExprValueValidator.java,v 1.4 2006-09-19 14:36:53 deniger Exp $
 */
public class CtuluExprValueValidator extends BuValueValidator {
  final BuValueValidator val_;
  public CtuluExprValueValidator(){
    val_=null;

  }
  /**
   * @param _val
   */
  public CtuluExprValueValidator(final BuValueValidator _val) {
    super();
    val_ = _val;
  }

  @Override
  public boolean isValueValid(final Object _value){
    if(_value instanceof CtuluExpr || _value instanceof CtuluParser) {
      return true;
    }
    return val_==null?true:val_.isValueValid(_value);
  }

  /**
   * @param _value la valeur finale: pas d'expression
   * @return true si valide ....
   */
  public boolean isFinalValueValid(final Object _value){
    return val_==null?true:val_.isValueValid(_value);
  }



}
