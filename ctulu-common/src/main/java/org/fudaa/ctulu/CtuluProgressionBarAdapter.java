/*
 * @creation 2 avr. 2004
 * 
 * @modification $Date: 2007-02-02 11:20:09 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import java.awt.EventQueue;
import javax.swing.JProgressBar;

/**
 * @author Fred Deniger
 * @version $Id: CtuluProgressionBarAdapter.java,v 1.2 2007-02-02 11:20:09 deniger Exp $
 */
public class CtuluProgressionBarAdapter implements ProgressionInterface {

  JProgressBar bar_;
  String desc_;

  public CtuluProgressionBarAdapter(final JProgressBar _bar) {
    bar_ = _bar;
    bar_.setStringPainted(true);
  }

  @Override
  public void reset() {
    bar_.setValue(0);
    setDesc(CtuluLibString.EMPTY_STRING);
  }

  @Override
  public void setDesc(final String _s) {
    desc_ = _s;
    setProgression(bar_.getValue());
  }

  @Override
  public void setProgression(final int _v) {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(new Runnable() {

        @Override
        public void run() {
          setProgression(_v);
        }
      });
      return;
    }
    bar_.setValue(_v);
    if (desc_ == null) {
      bar_.setString(Integer.toString(_v) + " %");
    }
    bar_.setString(desc_ + " - " + Integer.toString(_v) + " %");
    if (_v >= 100) {
      reset();
    }
  }
}
