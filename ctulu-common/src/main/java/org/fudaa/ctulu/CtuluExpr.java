/*
 *  @creation     27 mai 2005
 *  @modification $Date: 2007-04-16 16:33:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuComparator;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.ParserVisitor;
import org.nfunk.jep.Variable;
import org.nfunk.jep.function.PostfixMathCommandI;

import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

/**
 * Un conteneur d'expression. Il permet de renseigner les variables utilisables
 * avant parsing, et de r�cup�rer des infos sur le parser associ� une fois
 * une evaluation r�alis�e.
 *
 * @author Fred Deniger
 * @version $Id: CtuluExpr.java,v 1.13 2007-04-16 16:33:53 deniger Exp $
 */
public class CtuluExpr {
  Map varDesc_;
  CtuluParser parser_;

  public boolean containsDescFor(final String _name) {
    return varDesc_ != null && varDesc_.containsKey(_name);
  }

  public String getDesc(final String _name) {
    return varDesc_ == null ? null : (String) varDesc_.get(_name);
  }

  public void initExpr() {
    parser_.getFunctionTable().clear();
    addStandardFunctions();
  }

  public Variable[] findUsedVarAndConstant() {
    return parser_.findUsedVarAndConstant();
  }

  public Variable[] findUsedVar() {
    return parser_.findUsedVar();
  }

  /**
   * @param _v le visiteur
   * @param _n le noeud a parcourir entierement.
   */
  public static void visitAll(final ParserVisitor _v, final Node _n) {
    if (_n == null) {
      return;
    }
    try {
      _n.jjtAccept(_v, null);
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    final int nb = _n.jjtGetNumChildren();
    for (int i = 0; i < nb; i++) {
      visitAll(_v, _n.jjtGetChild(i));
    }
  }

  public final void initVar() {
    if (varDesc_ != null) {
      varDesc_.clear();
    }
    parser_.getSymbolTable().clear();
    parser_.addStandardConstants();
  }

  final void addStandardFunctions() {
    parser_.addStandardFunctions();
    parser_.getFunctionTable().remove("sum");
    parser_.getFunctionTable().put("isIn", new IsInFunction());
  }

  /**
   * @author Fred Deniger
   * @version $Id: CtuluExpr.java,v 1.13 2007-04-16 16:33:53 deniger Exp $
   */
  static class IsInFunction implements PostfixMathCommandI {
    @Override
    public int getNumberOfParameters() {
      return 3;
    }

    @Override
    public boolean checkNumberOfParameters(final int _arg0) {
      return (getNumberOfParameters() == _arg0);
    }

    @Override
    public void run(final Stack _stack) throws ParseException {
      if (null == _stack) {
        throw new ParseException("Stack argument null");
      }

      try {
        double sup = ((Double) _stack.pop()).doubleValue();
        double inf = ((Double) _stack.pop()).doubleValue();
        final double val = ((Double) _stack.pop()).doubleValue();

        if (inf > sup) {
          final double tmp = inf;
          inf = sup;
          sup = tmp;
        }
        _stack.push((val >= inf && val <= sup) ? CtuluLib.UN : CtuluLib.ZERO);
      } catch (final RuntimeException e) {
        throw new ParseException(CtuluLib.getS("Des r��ls sont attendus"));
      }
    }

    @Override
    public void setCurNumberOfParameters(final int _n) {
    }
  }

  public final String getLastExpr() {
    return parser_ == null ? CtuluLibString.EMPTY_STRING : parser_.lastExpr_;
  }

  public String getLastError() {
    return parser_.getErrorInfo();
  }

  public final CtuluParser getParser() {
    return parser_;
  }

  public void setParser(CtuluParser parser) {
    this.parser_ = parser;
  }

  public CtuluExpr(final CtuluParser _p) {
    parser_ = new CtuluParser(_p);
  }

  public CtuluExpr() {
    parser_ = new CtuluParser();
    addStandardFunctions();
    initVar();
    parser_.setImplicitMul(true);
  }

  public CtuluExpr(final CtuluExpr _expr) {
    parser_ = new CtuluParser(_expr.parser_);
    if (_expr.varDesc_ != null) {
      createMap();
      varDesc_.putAll(_expr.varDesc_);
    }
  }

  public void addVariable(final Variable[] _vs) {
    if (_vs == null) {
      return;
    }
    for (int i = 0; i < _vs.length; i++) {
      final Variable v = addVar(_vs[i].getName(), _vs[i].getName());
      v.setValue(_vs[i].getValue());
      v.setIsConstant(true);
    }
  }

  public void removeVar(final String _s) {
    if (varDesc_ != null) {
      varDesc_.remove(_s);
    }
    parser_.getSymbolTable().remove(_s);
  }

  public Variable addVar(final String _var, final String _desc) {
    createMap();
    varDesc_.put(_var, _desc);
    return parser_.getSymbolTable().makeVarIfNeeded(_var);
  }

  private void createMap() {
    if (varDesc_ == null) {
      varDesc_ = new TreeMap(FuComparator.STRING_IGNORE_CASE_COMPARATOR);
    }
  }
}
