/*
 * @creation 1 sept. 06
 * @modification $Date: 2007/01/08 15:36:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.io.File;
import java.io.InputStream;

/**
 * @author fred deniger
 * @version $Id: CtuluArkLoader.java,v 1.3 2007/01/08 15:36:50 deniger Exp $
 */
public interface CtuluArkLoader {

  /** Une option pour ignorer la r�cuperation du calque donn�. */
  public static final String OPTION_LAYER_IGNORE="layer.ignore.";
  
  /**
   * @param le rep parent utile ?
   * @param _entryName du style 001-layeer.desc.xml. '001-' represente l'ide qui sera utilise pour retrouve l'objet
   *          associ� dans la base
   * @return null si non trouv�
   */
  Object getDbDataFor(String _parentDir, String _entryName);

  /**
   * @param _parentDir le rep parent
   * @return toutes les entree du style xxx.desc.xml. return null si pas un repertoire ou si aucune entree trouv�e.
   */
  String[] getEntries(String _parentDir);
  
  /**
   * Retourne une propri�t� associ�e � un calque (issue de {calque}.props).
   * @param _parentDir Le repertoire parent
   * @param _entryName L'entr�e correspondant au calque, sous la forme 01-layer.desc.xml.
   * @param _prop Le nom de la propri�t�.
   * @return La valeur de la propri�t�.
   */
  String getLayerProperty(String _parentDir, String _entryName, String _prop);

  /**
   * @param _parentDir le rep parent dans l'archive
   * @param _entryName le nom de l'entree
   * @return le flux permettant de lire le tout.null si non trouve
   */
  InputStream getReader(String _parentDir, String _entryName);

  boolean isEntryFound(String _parentDir, String _entryName);
  
  
  /**
   * @return le dossier dans lequel la sauvegarde est effectuee ( utilise pour des chemins relatifs)
   */
  File getDestDir();
  
  /**
   * @param _key la cle connue
   * @return une option pour le chargement
   */
  String getOption(String _key);

}
