/*
 * @creation 20 fevr. 2003
 * 
 * @modification $Date: 2007-05-21 10:28:29 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Cette classe sert a transmettre les informations d'une operation de lecture ou d'ecriture. Les informations et les erreurs de l'operation sont
 * stockees avec eventuellement leur numero de ligne.
 * @author deniger
 * @version $Id: CtuluAnalyze.java,v 1.12 2007-05-21 10:28:29 deniger Exp $
 */
public class CtuluAnalyze {

  public static final Level ERROR = Level.SEVERE;
  public static Level FATAL = Level.ALL;

  /**
   * Constructeur par d�faut
   */
  public CtuluAnalyze() {
    super();
  }

  /**
   * Constructeur pr�cisant le resourceBundle � utiliser
   *
   * @param defaultResourceBundle
   */
  public CtuluAnalyze(final ResourceBundle defaultResourceBundle) {
    super();
    this.defaultResourceBundle = defaultResourceBundle;
  }
  ResourceBundle defaultResourceBundle;
  private String desc = "Analyzing";
  protected final List<LogRecord> logs = new ArrayList<LogRecord>();
  private String ressource;

  /**
   * @param msg le message
   */
  public void addError(final String msg) {
    addRecord(Level.SEVERE, msg);
  }

  /**
   * @param msg le message
   * @param e l'erreur
   */
  public void addError(final String msg, final Throwable e) {
    addRecord(Level.SEVERE, msg).setThrown(e);
  }

  /**
   * @param msg le message
   * @param data
   */
  public void addError(final String msg, Object... data) {
    addRecord(Level.SEVERE, msg, data);
  }

  /**
   * @param msg le message
   * @param data
   * @param e l'erreur
   */
  public void addErrorThrown(final String msg, final Throwable e, Object... data) {
    addRecord(Level.SEVERE, msg, data).setThrown(e);
  }

  /**
   * @param msg
   * @param i
   */
  public void addErrorFromFile(final String msg, final int i) {
    addRecord(Level.SEVERE, msg, Integer.valueOf(i));
  }

  /**
   * @param msg le message
   * @param reader le reader occasionnant l'erreur
   */
  public void addErrorFromFile(final String msg, final LineNumberReader reader) {
    addRecord(Level.SEVERE, msg, reader.getLineNumber());
  }

  /**
   * @param msg le message
   * @param data les donnees complementaire
   * @param i le numero de ligne de l'erreur
   */
  public void addErrorFromFile(final String msg, final int i, Object... data) {
    if (data == null) {
      addRecord(Level.SEVERE, msg, i);
    } else {
      Object[] all = new Object[data.length + 1];
      System.arraycopy(data, 0, all, 1, data.length);
      all[0] = i;
      addRecord(Level.SEVERE, msg, all);
    }
  }

  /**
   * @param msg le message
   * @param data les donnees complementaire
   * @param reader le reader occasionnant l'erreur
   */
  public void addErrorFromFile(final String msg, final LineNumberReader reader, Object... data) {
    addErrorFromFile(msg, reader.getLineNumber(), data);
  }

  /**
   * @param _m le message
   */
  public void addFatalError(final String _m) {
    addRecord(FATAL, _m);
  }

  /**
   * Initialise le champ "erreur fatale". Si une erreur fatale a deja ete initialisee, ne fait rien.
   *
   * @param _m le message
   * @param _index le num de ligne
   */
  public void addFatalError(final String _m, final int _index) {
    addRecord(FATAL, _m, Integer.valueOf(_index));
  }

  /**
   * @param _m
   * @param arg
   */
  public void addFatalError(final String _m, final Object... arg) {
    addRecord(FATAL, _m, arg);
  }

  /**
   * Initialise le champ "erreur fatale". Si une erreur fatale a deja ete initialisee, ne fait rien.
   *
   * @param _m le message
   * @param _in pour recuperer le num de ligne
   */
  public void addFatalError(final String _m, final LineNumberReader _in) {
    addFatalError(_m, _in == null ? -1 : _in.getLineNumber());
  }

  /**
   * @param msg le message d'info a ajouter
   */
  public void addInfo(final String msg) {
    addRecord(Level.INFO, msg);
  }

  /**
   * @param msg
   * @param args
   */
  public void addInfo(final String msg, final Object... args) {
    addRecord(Level.INFO, msg, args);
  }

  /**
   * @param msg le message d'info a ajouter
   * @param _index l'index identifiant le message (peut etre un numero de ligne)
   */
  public void addInfoFromFile(final String msg, final int _index) {
    addRecord(Level.INFO, msg, _index);
  }

  /**
   * @param msg
   * @param i
   * @param data
   */
  public void addInfoFromFile(final String msg, final int i, Object... data) {
    addRecord(Level.INFO, msg, i, data);
  }

  /**
   * @param msg le message d'info a ajouter
   * @param _in le reader permettant de recuperer le numero de ligne
   */
  public void addInfoFromFile(final String msg, final LineNumberReader _in) {
    addRecord(Level.INFO, msg, _in.getLineNumber());
  }

  // /**
  // * @param level le level
  // * @param msg le message
  // * @return le logRecord ajoute
  // */
  // public LogRecord addRecord(final Level level, final String msg) {
  // return addRecord(level, msg, null);
  // }
  /**
   * @param level le level
   * @param msg le message
   * @param data les donnees complementaire
   * @return le logRecord ajoute
   */
  public LogRecord addRecord(final Level level, final String msg, Object... data) {
    final LogRecord o = new LogRecord(level, msg);
    o.setParameters(data);
    o.setResourceBundle(defaultResourceBundle);
    o.setLoggerName(getDesc());
    logs.add(o);
    return o;
  }

  /**
   * @param msg le message d'avertissement
   */
  public void addWarn(final String msg) {
    addRecord(Level.WARNING, msg);
  }

  /**
   * @param msg le message d'avertissement
   * @param in le reader permettant de recuperer le numero de ligne
   */
  public void addWarn(final String msg, final LineNumberReader in) {
    addRecord(Level.WARNING, msg, in.getLineNumber());
  }

  /**
   * @param msg le message d'avertissement
   * @param args les arguments supplementaires
   */
  // public void addWarn(final String msg, final Map<String, Object> args) {
  public void addWarn(final String msg, final Object... args) {
    addRecord(Level.WARNING, msg, args);
  }

  /**
   * @param msg le message d'avertissement
   * @param index le numero identifiant le message
   */
  public void addWarnFromFile(final String msg, final int index) {
    addRecord(Level.WARNING, msg, index);
  }

  /**
   * @param _m
   */
  @Deprecated
  public void changeFatalError(final String _m) {
    addError(_m);
  }

  /**
   * Efface tous les canaux.
   */
  public void clear() {
    logs.clear();
  }

  public boolean containsErrorOrFatalError() {
    return containsErrors() || containsFatalError();
  }

  /**
   * @return true si contient au moins une erreur
   */
  public boolean containsErrors() {
    return containsLevel(Level.SEVERE);
  }

  /**
   * @return true si contient une erreur fatale
   */
  public boolean containsFatalError() {
    return containsLevel(FATAL);
  }

  /**
   * @return true si contient au moins une info
   */
  public boolean containsInfos() {
    return containsLevel(Level.INFO);
  }

  /**
   * @param l le level a tester
   * @return true si au moins un message et de niveau l.
   */
  public boolean containsLevel(final Level l) {
    for (final LogRecord log : logs) {
      if (l.equals(log.getLevel())) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return true si contient au moins un avertissement
   */
  public boolean containsWarnings() {
    return containsLevel(Level.WARNING);
  }

  /**
   * @return the defaultResourceBundle
   */
  public ResourceBundle getDefaultResourceBundle() {
    return defaultResourceBundle;
  }

  /**
   * @return la description de l'analyse en cours
   */
  public String getDesc() {
    return desc;
  }

  /**
   * @return l'ensemble des enregistrements de log
   */
  public Collection<LogRecord> getRecords() {
    return Collections.unmodifiableCollection(logs);
  }

  /**
   * @return chaine decrivant la ressource ( fichier, url, ...)
   */
  public String getResource() {
    return ressource;
  }

  /**
   * @return le resume de l'analyse
   */
  public String getResume() {
    return getDesc() + "\n" + CtuluDefaultLogFormatter.format(logs);
  }

  public String getFatalError() {
    LogRecord log = getFirstOfLevel(FATAL);
    return log == null ? null : CtuluDefaultLogFormatter.DEFAULT.format(log);
  }
  public LogRecord getFatalErrorRecord() {
    return getFirstOfLevel(FATAL);
  }

  /**
   * @param l le level a tester
   * @return true si au moins un message et de niveau l.
   */
  private LogRecord getFirstOfLevel(final Level l) {
    for (final LogRecord log : logs) {
      if (l.equals(log.getLevel())) {
        return log;
      }
    }
    return null;
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return logs.isEmpty();
  }

  public boolean isNotEmpty() {
    return !isEmpty();
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   */
  public void manageException(final Exception _e) {
    manageException(_e, "exception.occured");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   * @param msg le message
   */
  public LogRecord manageException(final Exception _e, final String msg) {
    final LogRecord addedRecord = addRecord(FATAL, msg);
    addedRecord.setThrown(_e);
    _e.printStackTrace();
    return addedRecord;
  }

  /**
   * Ajoute un message d'erreur.
   *
   * @param _e l'exception a loguer
   */
  public void manageException(final FileNotFoundException _e) {
    manageException(_e, "exception.FileNotFoundException");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   */
  public void manageException(final IllegalArgumentException _e) {
    manageException(_e, "exception.IllegalArgumentException");
  }

  /**
   * Ajoute un message d'erreur.
   *
   * @param _e l'exception a loguer
   */
  public void manageException(final IOException _e) {
    manageException(_e, "exception.IOException");
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   * @param _l le numero de ligne
   */
  public LogRecord manageException(final NumberFormatException _e, final int _l) {
    return manageException(_e, CtuluLib.getS("Erreur de lecture � la ligne {0}", Integer.toString(_l)));
  }

  /**
   * @param _e l'exception a ajouter a l'anayse
   * @param msg le message
   * @param line la ligne
   */
  public void manageExceptionFromFile(final Exception _e, final String msg, final int line) {
    addRecord(Level.SEVERE, msg, line).setThrown(_e);
  }

  public String getDesci18n() {
    if (defaultResourceBundle == null) {
      return desc;
    }
    return defaultResourceBundle.getString(desc);
  }

  /**
   * Ajoute tous les canaux de l'analyse passee en parametres. Les pointeurs sont copiees.
   *
   * @param _analyze l'analyse a ajouter a celle-ci
   */
  public void merge(final CtuluAnalyze _analyze) {
    setDesc(_analyze.getDesc());
    setDefaultResourceBundle(_analyze.getDefaultResourceBundle());
    setResource(_analyze.getResource());
    logs.addAll(_analyze.logs);
  }

  /**
   * Permet d'ecrire sur la sortie standart le contenu de cette analyse.
   */
  public void printResume() {
    System.err.println(getResume());// NOPMD
  }

  /**
   * @param defaultResourceBundle the defaultResourceBundle to set
   */
  public void setDefaultResourceBundle(final ResourceBundle defaultResourceBundle) {
    this.defaultResourceBundle = defaultResourceBundle;
  }

  /**
   * @param _string la nouvelle description de l'analyse
   */
  public void setDesc(final String _string) {
    desc = _string;
  }

  /**
   * @param _string la nouvelle ressource de l'analyse
   */
  public void setResource(final String _string) {
    ressource = _string;
  }
}
