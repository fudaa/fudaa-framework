/*
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2007-03-23 17:16:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.*;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import java.awt.*;

/**
 * @author Fred Deniger
 * @version $Id: EbliValueEditorInteger.java,v 1.3 2005/08/16 13:02:14 deniger
 *     Exp $
 */
public class CtuluValueEditorInteger implements CtuluValueEditorI {
  private final CtuluExpr expr_;
  boolean isEditable_;
  BuValueValidator val_;
  private String separator;
  private final TableCellRenderer renderer_ = new BuTableCellRenderer();

  @Override
  public TableCellRenderer createTableRenderer() {
    return renderer_;
  }

  @Override
  public String getSeparator() {
    return separator;
  }

  public void setSeparator(String separator) {
    this.separator = separator;
  }

  public CtuluValueEditorInteger() {
    this(true);
  }

  public CtuluValueEditorInteger(final boolean _formule) {
    expr_ = _formule ? new CtuluExpr() : null;
  }

  protected CtuluExpr getExpr() {
    return expr_;
  }

  @Override
  public JComponent createCommonEditorComponent() {
    return createCommonEditorComponent(true);
  }

  public JComponent createCommonEditorComponent(final boolean _addOld) {
    BuTextField r = null;
    if (isEditable_ && expr_ != null) {
      final CtuluExpr exp = new CtuluExpr(expr_);
      if (_addOld) {
        CtuluParser.addOldVar(exp);
      }
      r = new CtuluExprTextField(new CtuluExprStringValidators.IntegerOption());
      if (val_ != null) {
        r.setValueValidator(new CtuluExprValueValidator(val_));
      }
    } else {
      r = BuTextField.createIntegerField();
      if (val_ != null) {
        r.setValueValidator(val_);
      }
    }
    r.setColumns(10);
    r.setEditable(isEditable_);
    return r;
  }

  @Override
  public JComponent createEditorComponent() {
    return createCommonEditorComponent(false);
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createEditorComponent(), true);
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createCommonEditorComponent(), true);
  }

  @Override
  public Class getDataClass() {
    return String.class;
  }

  @Override
  public String getStringValue(final Component _comp) {
    return ((JTextField) _comp).getText();
  }

  public BuValueValidator getVal() {
    return val_;
  }

  @Override
  public String getValidationMessage() {
    if (val_ instanceof CtuluValueValidator) {
      return ((CtuluValueValidator) val_).getDescription();
    }
    return null;
  }

  @Override
  public Object getValue(final Component _comp) {
    return ((BuTextField) _comp).getValue();
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  @Override
  public boolean isEmpty(final Component _c) {
    final String txt = ((JTextField) _c).getText();
    return txt == null || txt.trim().length() == 0;
  }

  @Override
  public boolean isValid(final Object _o) {
    return isValid(_o.toString());
  }

  public boolean isValid(final String _s) {
    if (_s.length() > 0) {
      try {
        Integer.parseInt(_s);
        return true;
      } catch (final NumberFormatException e) {
        if (expr_ != null) {
          expr_.getParser().parseExpression(_s);
          return !expr_.getParser().hasError();
        }
      }
    }

    return false;
  }

  @Override
  public boolean isValueValidFromComponent(final Component _comp) {
    return ((BuTextField) _comp).getValue() != null;
  }

  @Override
  public void setEditable(final boolean _isEditable) {
    isEditable_ = _isEditable;
  }

  public void setVal(final BuValueValidator _val) {
    val_ = _val;
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    ((JTextField) _comp).setText(toString(_s));
  }

  @Override
  public String toString(final Object _s) {
    if (_s == null) {
      return CtuluLibString.EMPTY_STRING;
    } else if (_s instanceof Number) {
      return Integer.toString(((Number) _s).intValue());
    } else {
      try {
        return Integer.toString(Integer.parseInt(_s.toString()));
      } catch (final NumberFormatException e) {
      }
    }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public Integer parseString(String in) {
    if (in == null) {
      return null;
    }
    try {
      return Integer.parseInt(in);
    } catch (Exception ex) {
    }
    return null;
  }
}
