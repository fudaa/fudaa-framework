/*
 * @creation 2 ao�t 2005
 * 
 * @modification $Date: 2007-06-28 09:24:23 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBoxMenuItem;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuPopupMenu;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuToolButton;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JToolTip;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluExprStringValidator;
import org.fudaa.ctulu.CtuluExprStringValidators;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;

/**
 * Un champ texte pour saisir des valeurs reelles, entieres ou string, sous forme ou non de formule.
 * 
 * @author Fred Deniger, marchand@deltacad.fr
 * @version $Id: CtuluExprTextField.java,v 1.9 2007-06-28 09:24:23 deniger Exp $
 */
public class CtuluExprTextField extends BuTextField {

  class FormulaPopupMenu extends BuPopupMenu {
    BuCheckBoxMenuItem cbActivate_;
    BuMenuItem cbValid_;
    BuMenuItem cbEdit_;

    public FormulaPopupMenu() {
      cbActivate_ = addCheckBox(CtuluLib.getS("Activer"), "ACTIVATE", true, false);
      cbValid_ = addMenuItem(CtuluLib.getS("Valider"), "VALID", BuResource.BU.getToolIcon("valider"), false);
      cbEdit_ = addMenuItem(CtuluLib.getS("Editeur"), "EDIT", CtuluResource.CTULU.getToolIcon("formule"), false);
      addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent _ae) {
          if ("ACTIVATE".equals(_ae.getActionCommand())) {
            ((CtuluExprStringValidator) getStringValidator()).setExpressionExpected(cbActivate_.isSelected());
            // Controle que la valeur actuelle est correcte.
            isValideValue();
          }
          if ("VALID".equals(_ae.getActionCommand())) {
            final boolean isValidValue = isValideValue();
            final JToolTip tp = createToolTip();
            tp.setTipText(isValidValue ? CtuluLib.getS("Valide") : CtuluLib.getS("Non valide"));
            tp.setSize(tp.getPreferredSize());
            final Point l = CtuluExprTextField.this.getLocationOnScreen();
            final Popup popup = PopupFactory.getSharedInstance().getPopup(CtuluExprTextField.this, tp, (int) l.getX(), (int) l.getY());
            popup.show();
            final Timer t = new Timer(750, new ActionListener() {

              @Override
              public void actionPerformed(ActionEvent _evt) {
                popup.hide();
              }
            });
            t.setRepeats(false);
            t.start();

          }
          // boite de dialogue pour l'editeur d'expression.
          else if ("EDIT".equals(_ae.getActionCommand())) {
            editFormula();
          }

        }
      });
    }

    public void update() {
      boolean isExprExpected = ((CtuluExprStringValidator) getStringValidator()).isExpressionExpected();
      cbActivate_.setSelected(isExprExpected);
      cbValid_.setEnabled(isExprExpected);
      cbEdit_.setEnabled(isExprExpected);
    }
  }

  /** Le menu pour proposer l'�diteur de formule. */
  FormulaPopupMenu menu_;
  BuToolButton formulaButton_;

  /**
   * Construit avec un validateur de double.
   * 
   * @see CtuluExprStringValidators.DoubleOption
   */
  public CtuluExprTextField() {
    this(new CtuluExprStringValidators.DoubleOption());
  }

  /**
   * @param _expr l'expression a utiliser
   */
  public CtuluExprTextField(final CtuluExpr _expr) {
    this(new CtuluExprStringValidators.DoubleOption(_expr));
  }

  /**
   * @param _validator le validator de chaine.
   */
  public CtuluExprTextField(final CtuluExprStringValidator _validator) {
    super.setStringValidator(_validator);
  }

  @Override
  protected void processFocusEvent(final FocusEvent _evt) {
    super.processFocusEvent(_evt);
    // pour modifier le tooltip en cas d'erreur
    if (_evt.getID() == FocusEvent.FOCUS_LOST) {
      isValideValue();
    }
  }

  /**
   * Redefinie pour prendre en compte la commande popup.
   */
  @Override
  protected void processMouseEvent(final MouseEvent _e) {
    if (formulaButton_ == null && _e.getID() == MouseEvent.MOUSE_CLICKED && (BuLib.isRight(_e) || _e.isPopupTrigger())) {
      popup(this, _e.getX(), _e.getY());
    } else {
      super.processMouseEvent(_e);
    }
  }

  public boolean isValideValue() {
    final String oldTipId = "oldToolTip";
    if (!getStringValidator().isStringValid(super.getText()) && getExpr().getLastError() != null) {
      if (getClientProperty(oldTipId) == null) {
        String tp = getToolTipText();
        putClientProperty(oldTipId, tp == null ? "" : tp);
      }
      setForeground(Color.RED);
      setToolTipText(getExpr().getParser().getHtmlError());
      return false;
    }
    if (getClientProperty(oldTipId) != null) {
      String tp = (String) getClientProperty(oldTipId);
      setToolTipText(tp.equals("") ? null : tp);
    }
    setForeground(UIManager.getColor(getUIClassID() + ".foreground"));
    return true;
  }

  private boolean buttonLaunchEditDialog;

  public boolean isButtonLaunchEditDialog() {
    return buttonLaunchEditDialog;
  }

  public void setButtonLaunchEditDialog(boolean buttonLaunchEditDialog) {
    this.buttonLaunchEditDialog = buttonLaunchEditDialog;
  }

  public BuToolButton getButton() {
    final BuToolButton bt = new BuToolButton(CtuluResource.CTULU.getToolIcon("formule"));
    bt.setToolTipText(CtuluLib.getS("Editeur d'expressions"));
    //    bt.setActionCommand("EDIT");
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        if (buttonLaunchEditDialog) {
          editFormula();

        } else {
          popup(bt, 0, bt.getHeight());
        }
      }
    });
    return bt;
  }

  public void setFormulaMenuAsButton(boolean _b) {
    if (_b) {
      if (formulaButton_ != null) {
        return;
      }
      formulaButton_ = getButton();
      setLayout(new BuBorderLayout(0, 0));
      add(formulaButton_, BuBorderLayout.EAST);
    } else {
      remove(formulaButton_);
      formulaButton_ = null;
    }
  }

  void popup(JComponent _comp, int _x, int _y) {
    if (menu_ == null) {
      menu_ = new FormulaPopupMenu();
    }
    menu_.update();
    menu_.show(_comp, _x, _y);

  }

  /**
   * @return Le conteneur d'expression associ�.
   */
  public final CtuluExpr getExpr() {
    return ((CtuluExprStringValidator) getStringValidator()).getExpr();
  }

  @Override
  public void setStringValidator(final BuStringValidator _validator) {
    if (_validator instanceof CtuluExprStringValidator) {
      super.setStringValidator(_validator);
    } else {
      new Throwable().printStackTrace();
    }
  }

  void editFormula() {
    getExpr().getParser().parseExpression(getText());
    final CtuluExprGUI gui = new CtuluExprGUI(getExpr());
    final BuDialogConfirmation d = new BuDialogConfirmation((BuCommonInterface) SwingUtilities.getAncestorOfClass(BuCommonInterface.class, this),
        null, gui) {

      {
        btOui_.setText(CtuluLib.getS("Utiliser"));
        btOui_.setToolTipText(CtuluLib.getS("Utiliser cette expression"));
        final String txt = CtuluLib.getS("Ignorer");
        btNon_.setText(txt);
        btNon_.setToolTipText(txt);
      }
    };
    d.setResizable(true);
    d.setTitle(CtuluLib.getS("Expression"));
    d.pack();
    if (d.activate() == JOptionPane.OK_OPTION) {
      setText(gui.getExprText());
    }
  }

}