/*
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2007-03-09 08:37:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import java.awt.*;

/**
 * Une interface pour editer une valeur. Cr�e le composant pour �diter la
 * valeur. L'editeur peut ou non g�rer les formules.
 *
 * @author Fred Deniger
 * @version $Id: CtuluValueEditorI.java,v 1.3 2007-03-09 08:37:37 deniger Exp $
 */
public interface CtuluValueEditorI {
  /**
   * @return the string to use to separate from the previous editor
   */
  String getSeparator();

  /**
   * @return La classe de la valeur g�r�e par cet �diteur
   */
  Class getDataClass();

  /**
   * Affecte la valeur au composant cr�� par cet �diteur. La valeur doit �tre
   * de type getDataClass().
   *
   * @param _s La valeur
   * @param _comp Le composant.
   */
  void setValue(Object _s, Component _comp);

  /**
   * @param _o La valeur
   * @return La valeur pass�e traduite sous forme de chaine
   */
  String toString(Object _o);

  /**
   * @param _c Le composant cr�� par cet editeur.
   * @return True : Si la chaine saisie au niveau du composant est vide.
   */
  boolean isEmpty(Component _c);

  /**
   * @return True : Si la valeur est �ditable par l'�diteur.
   */
  boolean isEditable();

  /**
   * @param editable the new editable property
   */
  void setEditable(boolean editable);

  /**
   * @param _comp Le composant cr�� par cet editeur.
   * @return La chaine brute saisie au niveau du composant
   */
  String getStringValue(Component _comp);

  /**
   * @param _comp Le composant cr�� par cet editeur.
   * @return La valeur obtenue. La valeur peut �tre de type getDataClass() ou
   *     CtuluExpr si le composant utilise un editeur d'expression.
   */
  Object getValue(Component _comp);

  /**
   * @return Le composant simple pour �diter une valeur. Utilis� dans les editeurs
   */
  JComponent createEditorComponent();

  /**
   * @return Le composant commun pour �diter simultan�ment plusieurs valeurs
   */
  JComponent createCommonEditorComponent();

  /**
   * @return Le composant simple pour �diter une valeur, adapt� � un tableau.
   */
  TableCellEditor createTableEditorComponent();

  /**
   * @return Le composant commun pour �diter simultan�ment plusieurs valeurs, adapt� � un tableau.
   */
  TableCellEditor createCommonTableEditorComponent();

  /**
   * @return Le renderer pour le rendu de la valeur dans un tableau.
   */
  TableCellRenderer createTableRenderer();
  
  /**
   * @param _comp Le composant cr�� par cet editeur.
   * @return True : Si le format est correct et que la valeur est dans les bornes
   *     autoris�es pour le composant.
   */
  boolean isValueValidFromComponent(Component _comp);

  /**
   * @return Le message d�livr� en cas de valeur en dehors des bornes autoris�es.
   */
  String getValidationMessage();

  /**
   * @param _o La valeur a tester.
   * @return True : Si la format est correct et que la valeur est dans les bornes
   *     autoris�es.
   */
  boolean isValid(Object _o);

  /**
   *
   * @param in l'objet au format chaine de caractere
   * @return la valeure
   */
  Object parseString(String in);
}
