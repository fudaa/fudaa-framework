/*
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2007-04-30 14:21:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import java.awt.Component;
import java.text.DecimalFormat;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluExprValueValidator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluValueValidator;

import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;

/**
 * Un editeur pour un double.
 * Le nombre de decimales pour le renderer est fix� par propri�t�.
 * 
 * @author Fred Deniger
 * @version $Id: CtuluValueEditorDouble.java,v 1.11 2007-04-30 14:21:17 deniger Exp $
 */
public class CtuluValueEditorDouble implements CtuluValueEditorI {
  protected final CtuluExpr expr_;
  protected CtuluNumberFormatI formatter_;
  protected BuValueValidator val_;
  protected boolean isEditable_ = true;
  private String separator;
  private final BuTableCellRenderer renderer_ = new BuTableCellRenderer();

  @Override
  public TableCellRenderer createTableRenderer() {
    return renderer_;
  }

  @Override
  public String getSeparator() {
    return separator;
  }

  public void setSeparator(String separator) {
    this.separator = separator;
  }

  /**
   * Editeur pour des double avec support des formules.
   */
  public CtuluValueEditorDouble() {
    this(true);
  }

  @Override
  public Class getDataClass() {
    return Double.class;
  }

  /**
   * @param _formule true si les formules sont support�es
   */
  public CtuluValueEditorDouble(final boolean _formule) {
    expr_ = _formule ? new CtuluExpr() : null;
    
    DecimalFormat fmt = CtuluLib.getDecimalFormat(BuPreferences.BU.getIntegerProperty(CtuluResource.COORDS_VISU_NB_DIGITS, 2));
    renderer_.setNumberFormat(fmt);
  }

  public CtuluValueEditorDouble(final CtuluExpr _expr) {
    expr_ = _expr;
  }

  protected String getFormatValue(final double _d) {
    return formatter_ == null ? Double.toString(_d) : formatter_.format(_d);
  }

  @Override
  public JComponent createCommonEditorComponent() {
    return createCommonEditorComponent(true);
  }

  public JComponent createCommonEditorComponent(final boolean _addOld) {
    BuTextField r = null;
    if (isEditable_ && expr_ != null) {
      final CtuluExpr exp = new CtuluExpr(expr_);
      if (_addOld) {
        CtuluParser.addOldVar(exp);
      }
      r = new CtuluExprTextField(exp);
      if (val_ != null) {
        r.setValueValidator(new CtuluExprValueValidator(val_));
      }
    } else {
      r = BuTextField.createDoubleField();
      if (val_ != null) {
        r.setValueValidator(val_);
      }
    }
    r.setColumns(10);
    r.setEditable(isEditable_);

    return r;
  }

  @Override
  public JComponent createEditorComponent() {
    return createCommonEditorComponent(false);
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createEditorComponent(), true);
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createCommonEditorComponent(), true);
  }

  public final CtuluNumberFormatI getFormatter() {
    return formatter_;
  }

  @Override
  public String getStringValue(final Component _comp) {
    return ((JTextField) _comp).getText();
  }

  public final BuValueValidator getVal() {
    return val_;
  }

  @Override
  public Object getValue(final Component _comp) {
    return ((BuTextField) _comp).getValue();
  }

  @Override
  public boolean isEditable() {
    return isEditable_;
  }

  @Override
  public boolean isEmpty(final Component _c) {
    final String txt = ((JTextField) _c).getText();
    return txt == null || txt.trim().length() == 0;
  }

  @Override
  public boolean isValid(final Object _o) {
    return isValid(_o == null ? null : _o.toString());
  }

  protected boolean isValid(final String _s) {
    if (!CtuluLibString.isEmpty(_s)) {
      if (expr_ != null) {
        expr_.getParser().parseExpression(_s);
        return !expr_.getParser().hasError();
      }
      try {
        final Double d = Double.valueOf(_s);
        if (d != null && val_ != null) {
          return val_.isValueValid(d);
        }
        return true;
      } catch (final NumberFormatException e) {
        return false;
      }
    }
    return false;
  }

  @Override
  public String getValidationMessage() {
    if (val_ instanceof CtuluValueValidator) {
      return ((CtuluValueValidator) val_).getDescription();
    }
    return null;
  }

  @Override
  public boolean isValueValidFromComponent(final Component _comp) {
    return ((BuTextField) _comp).getValue() != null;
  }

  @Override
  public void setEditable(final boolean _isEditable) {
    isEditable_ = _isEditable;
  }

  public void setFormatter(final CtuluNumberFormatI _formatter) {
    formatter_ = _formatter;
    
    // Si le format est de type format defaut, on l'associe au render.
    if (_formatter instanceof CtuluNumberFormatDefault)
      renderer_.setNumberFormat(((CtuluNumberFormatDefault) _formatter).getFmt());
  }

  public final void setVal(final BuValueValidator _val) {
    val_ = _val;
  }

  public void setValue(final double _v, final Component _comp) {
    ((JTextField) _comp).setText(getFormatValue(_v));
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    ((JTextField) _comp).setText(toString(_s));
  }

  public String toString(final double _d) {
    return getFormatValue(_d);
  }

  @Override
  public Double parseString(String in) {
    if (in == null) {
      return null;
    }
    try {
      return Double.parseDouble(in);
    } catch (Exception ex) {
    }
    return null;
  }

  @Override
  public String toString(final Object _s) {
    if (_s == null || _s == CtuluLibString.EMPTY_STRING) {
      return CtuluLibString.EMPTY_STRING;
    } else if (_s instanceof Number) {
      return getFormatValue(((Number) _s).doubleValue());
    } else if (_s.toString() == null || _s.toString().trim().length() == 0) {
      return CtuluLibString.EMPTY_STRING;
    } else {
      try {
        return getFormatValue(Double.parseDouble(_s.toString()));
      } catch (final NumberFormatException e) {
        // e.printStackTrace();
      }
    }
    return CtuluLibString.EMPTY_STRING;
  }
}
