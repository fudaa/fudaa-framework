package org.fudaa.ctulu.editor;

import java.awt.Component;
import java.text.ParsePosition;

import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluDuration;
import org.fudaa.ctulu.CtuluDurationStringValidator;
import org.fudaa.ctulu.CtuluNumberFormat;
import org.fudaa.ctulu.CtuluNumberFormatI;

import com.memoire.bu.BuTextField;

/**
 * Un editeur de dur�e en jours, heures, minutes, secondes.
 * Le format de la dur�e est suppos� �tre jj:hh:mm:ss.
 * Lors de la saisie, les jj, hh, mm peuvent �tre omis.
 * Lors de la restitution, si la dur�e est inf�rieure � 1 jour, les jours ne sont pas affich�s.<p>
 * 
 * Les valeurs peuvent �tre g�r�es en secondes ou millisecondes.
 * <p>
 * Exemples de temps :<br>
 * 12 => 00:00:12<br>
 * 10:0 => 00:10:00<br>
 * 1:12:0:0 => 1:12:00:00
 * 
 * @author Bertrand Marchand
 */
public class CtuluValueEditorDuration extends CtuluValueEditorDouble {
  protected boolean valueInMillis = false;

  public static CtuluNumberFormat SECONDS_DEFAULT_FORMATTER = new CtuluNumberFormat() {

    @Override
    public String format(double _d) {
      CtuluDuration t = new CtuluDuration((long)_d);
      return t.toString();
    }

    @Override
    public CtuluNumberFormatI getCopy() {
      return this;
    }

    @Override
    public String toLocalizedPattern() {
      return "";
    }

    @Override
    public Object parseObject(String _source, ParsePosition _pos) {
      CtuluDuration t = CtuluDuration.fromString(_source);
      return t == null ? null : t.getDuration();
    }
  };
  
  public static CtuluNumberFormat MILLIS_DEFAULT_FORMATTER = new CtuluNumberFormat() {

    @Override
    public String format(double _d) {
      CtuluDuration t = new CtuluDuration((long)_d/1000);
      return t.toString();
    }

    @Override
    public CtuluNumberFormatI getCopy() {
      return this;
    }

    @Override
    public String toLocalizedPattern() {
      return "";
    }

    @Override
    public Object parseObject(String _source, ParsePosition _pos) {
      CtuluDuration t = CtuluDuration.fromString(_source);
      return t == null ? null : t.getDuration()*1000;
    }
  };

  /**
   * Cr�ation d'un �diteur de dur�e.
   * @param valueInMillis True : La valeur est en milissecondes. False : En secondes.
   */
  public CtuluValueEditorDuration(boolean valueInMillis) {
    // Pas d'expressions autoris�es
    super(false);
    
    this.valueInMillis = valueInMillis;
    setFormatter(getDefaultFormatter());
  }
  
  /**
   * Cr�ation d'un �diteur de valeurs, la valeur est en secondes.
   */
  public CtuluValueEditorDuration() {
    this(false);
  }
  
  public CtuluNumberFormat getDefaultFormatter() {
    return valueInMillis ? MILLIS_DEFAULT_FORMATTER:SECONDS_DEFAULT_FORMATTER;
  }
  
  @Override
  public JComponent createCommonEditorComponent(final boolean _addOld) {
    BuTextField r = null;
    r = BuTextField.createTimeField();
    r.setStringValidator(new CtuluDurationStringValidator(valueInMillis));
    if (val_ != null) {
      r.setValueValidator(val_);
    }
    r.setColumns(10);
    r.setEditable(isEditable_);

    return r;
  }

  @Override
  public String getStringValue(Component _comp) {
    // TODO Auto-generated method stub
    String str = super.getStringValue(_comp);
    return str;
  }

  @Override
  public Object getValue(Component _comp) {
    // TODO Auto-generated method stub
    return super.getValue(_comp);
  }
  
  

}
