/*
 * @creation 26 sept. 06
 * @modification $Date: 2007-03-23 17:16:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import java.awt.Component;
import java.util.Arrays;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuTableCellRenderer;

/**
 * @author fred deniger
 * @version $Id: CtuluValueEditorChoice.java,v 1.5 2007-03-23 17:16:16 deniger Exp $
 */
public class CtuluValueEditorChoice implements CtuluValueEditorI {
  /**
   * Les deux tableaux existent pour permettre un affichage diff�rent des valeurs r�els retourn�es. Le cas d'utilisation �tant
   * l'internationnalisation : on poss�de des infomations au sujet d'un �l�ment quelconque, par exemple les couleurs primaires ;
   * on veut afficher ces couleurs dans la langue de l'utilisateur, mais poss�der les m�mes d�nominations dans le code. Ainsi
   * reelChoices poss�dera les valeurs constantes (exemple BLUE, RED, GREEN) et displayChoices poss�dera les traductions dans la
   * langue de l'utilisateur (Bleu, Rouge, Vert). Dans le cas o� displayChoices n'est pas renseign� � l'instanciation, reelChoice
   * sera consid�r� comme valeur reel et d'affichage.
   */
  private String[] displayChoices_;
  private String[] reelChoices_;
  private boolean useInteger_;
  private final String undefineValue_ = "<" + CtuluLib.getS("Mixte") + ">";
  private final TableCellRenderer renderer_ = new BuTableCellRenderer();

  /**
   * Dans le cas o� dChoices est null, reelChoices sera �galement affich�.
   *
   * @param _reelChoices Les choix retourn�s lors des getValue
   * @param _dChoices Les choix affich�s par la comboBox
   */
  public CtuluValueEditorChoice(final String[] _reelChoices, final String[] _dChoices) {
    super();
    useInteger_ = false;
    reelChoices_ = _reelChoices;
    displayChoices_ = _dChoices;
    if (displayChoices_ != null && reelChoices_.length != displayChoices_.length) {
      throw new IllegalArgumentException("reelChoices_ and displayChoices_ must have the same size.");
    }
  }

  private String separator;

  @Override
  public String getSeparator() {
    return separator;
  }

  public void setSeparator(String separator) {
    this.separator = separator;
  }

  private BuComboBox getCb() {
    if (displayChoices_ != null) {
      return new BuComboBox(displayChoices_);
    } else {
      return new BuComboBox(reelChoices_);
    }
  }

  @Override
  public Class<Arrays> getDataClass() {
    return Arrays.class;
  }

  private boolean isMixteComponent(final BuComboBox _buComboBox) {
    return _buComboBox.getModel().getSize() > reelChoices_.length;
  }

  private boolean isMixteValuesSelected(final BuComboBox _buComboBox) {
    return isMixteComponent(_buComboBox) && _buComboBox.getSelectedIndex() >= reelChoices_.length;
  }

  @Override
  public TableCellRenderer createTableRenderer() {
    return renderer_;
  }

  @Override
  public JComponent createCommonEditorComponent() {
    String[] elements;
    if (displayChoices_ != null) {
      elements = displayChoices_;
    } else {
      elements = reelChoices_;
    }
    final String[] extendedChoices = new String[elements.length + 1];
    extendedChoices[elements.length] = undefineValue_;
    System.arraycopy(elements, 0, extendedChoices, 0, elements.length);
    BuComboBox bu = new BuComboBox(extendedChoices);
    bu.setSelectedIndex(elements.length);
    return bu;
  }

  @Override
  public JComponent createEditorComponent() {
    return getCb();
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    final BuComboBox cb = getCb();
    if (useInteger_) {
      return new DefaultCellEditor(cb) {
        @Override
        public Object getCellEditorValue() {
          return new Integer(cb.getSelectedIndex());
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
          BuComboBox cb = (BuComboBox) super.getTableCellEditorComponent(table, value, isSelected, row, column);
          cb.setSelectedIndex((Integer) value);
          return cb;
        }
      };
    } else {
      return new DefaultCellEditor(cb) {
        @Override
        public Object getCellEditorValue() {
          if (cb.getSelectedIndex() == -1) {
            return null;
          } else {
            return reelChoices_[cb.getSelectedIndex()];
          }
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
          BuComboBox cb = (BuComboBox) super.getTableCellEditorComponent(table, value, isSelected, row, column);
          for (int i = 0; i < reelChoices_.length; i++) {
            if (reelChoices_[i].equals(value)) {
              cb.setSelectedIndex(i);
              break;
            }
          }
          return cb;
        }
      };
    }
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    final BuComboBox cb = (BuComboBox) createCommonEditorComponent();
    if (useInteger_) {
      return new DefaultCellEditor(cb) {
        @Override
        public Object getCellEditorValue() {
          return new Integer(cb.getSelectedIndex());
        }
      };
    } else {
      return new DefaultCellEditor(cb) {
        /*
         * (non-Javadoc) @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object,
         * boolean, int, int)
         */

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
          if (displayChoices_ != null) {
            // R�cup�ration de l'index de la valeur dans reelChoices_
            boolean found = false;
            int i = -1;
            while (!found && ++i < reelChoices_.length) {
              found = reelChoices_[i].equals(value);
            }
            if (found) {
              value = displayChoices_ != null ? displayChoices_[i] : value;
            } else {
              value = undefineValue_;
            }
          }
          return super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }

        @Override
        public Object getCellEditorValue() {
          if (cb.getSelectedIndex() < 0 || cb.getSelectedIndex() >= reelChoices_.length) {
            return cb.getSelectedItem();
          } else {
            return reelChoices_[cb.getSelectedIndex()];
          }
        }
      };
    }
  }

  public int getSelectedIdx(final Component _c) {
    return ((BuComboBox) _c).getSelectedIndex();
  }

  /**
   * @return null si c'est la chaine vide qui est selectionne
   */
  @Override
  public String getStringValue(final Component _comp) {
    final BuComboBox buComboBox = ((BuComboBox) _comp);
    if (isMixteValuesSelected(buComboBox)) {
      return null;
    }
    /*
     * if (useInteger_) { return CtuluLibString.getString(buComboBox.getSelectedIndex()); }
     */
    return reelChoices_[buComboBox.getSelectedIndex()];
  }

  /**
   * @return null si c'est la chaine vide qui est selectionne
   */
  @Override
  public Object getValue(final Component _comp) {
    final BuComboBox buComboBox = ((BuComboBox) _comp);
    if (isMixteValuesSelected(buComboBox)) {
      return null;
    }
    if (useInteger_) {
      return new Integer(buComboBox.getSelectedIndex());
    }
    return reelChoices_[buComboBox.getSelectedIndex()];
  }

  boolean editable = true;

  @Override
  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  @Override
  public boolean isEmpty(final Component _c) {
    final BuComboBox buComboBox = ((BuComboBox) _c);
    return (isMixteValuesSelected(buComboBox));
  }

  @Override
  public boolean isValid(final Object _o) {
    if (_o instanceof String && !_o.equals(undefineValue_)) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String getValidationMessage() {
    return null;
  }

  @Override
  public boolean isValueValidFromComponent(final Component _comp) {
    return true;
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    if (useInteger_) {
      if (_s == null && isMixteComponent((BuComboBox) _comp)) {
        ((BuComboBox) _comp).setSelectedIndex(reelChoices_.length);
      } else if (_s != null) {
        ((BuComboBox) _comp).setSelectedIndex(((Integer) _s).intValue());
      }
    } else {
      final int idx = CtuluLibArray.findObject(reelChoices_, _s);
      if (idx >= 0) {
        ((BuComboBox) _comp).setSelectedIndex(idx);
      } else if (isMixteComponent(((BuComboBox) _comp))) {
        ((BuComboBox) _comp).setSelectedIndex(reelChoices_.length);
      }
    }
  }

  @Override
  public String toString(final Object _o) {
    if (useInteger_) {
      return _o == null ? "null" : reelChoices_[((Integer) _o).intValue()];
    }
    return _o == null ? "null" : _o.toString();
  }

  @Override
  public Object parseString(String in) {
    return null;
  }

  public boolean isReturnInteger() {
    return useInteger_;
  }

  public void setReturnInteger(final boolean _returnInteger) {
    useInteger_ = _returnInteger;
  }
}
