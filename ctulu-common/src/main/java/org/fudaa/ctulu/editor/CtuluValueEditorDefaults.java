/*
 *  @creation     8 avr. 2005
 *  @modification $Date: 2007-05-04 13:43:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

/**
 * @author Fred Deniger
 * @version $Id: CtuluValueEditorDefaults.java,v 1.2 2007-05-04 13:43:22 deniger Exp $
 */
public final class CtuluValueEditorDefaults {

  /**
   * Editeur par defaut pour les doubles.
   */
  public static final CtuluValueEditorDouble DOUBLE_EDITOR = new CtuluValueEditorDouble() {

    @Override
    public void setEditable(final boolean _isEditable) {}
  };

  /**
   * Editeur par defaut pour les chaines.
   */
  public static final CtuluValueEditorString STRING_EDITOR = new CtuluValueEditorString();

  /**
   * Editeur avec formule pour les chaines
   */
  public static final CtuluValueEditorString EXPR_STRING_EDITOR = new CtuluValueEditorString(true);
  
  private CtuluValueEditorDefaults() {
    super();
  }

}
