/*
 *  @creation     2 mars 2005
 *  @modification $Date: 2007-03-30 15:35:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.JTextComponent;
import java.awt.*;

/**
 * Un editeur pour le temps.
 *
 * @author Fred Deniger
 * @version $Id: CtuluValueTimeEditor.java,v 1.7 2007-03-30 15:35:20 deniger Exp $
 */
public class CtuluValueEditorTime implements CtuluValueEditorI {
  CtuluNumberFormatI fmt_;
  boolean editable = true;
  private String separator;
  private final TableCellRenderer renderer_ = new BuTableCellRenderer();

  @Override
  public TableCellRenderer createTableRenderer() {
    return renderer_;
  }

  @Override
  public String getSeparator() {
    return separator;
  }

  public void setSeparator(String separator) {
    this.separator = separator;
  }

  public final CtuluNumberFormatI getFmt() {
    return fmt_;
  }

  @Override
  public JComponent createCommonEditorComponent() {
    return createEditorComponent();
  }

  public final void setFmt(final CtuluNumberFormatI _fmt) {
    fmt_ = _fmt;
  }

  @Override
  public boolean isEmpty(final Component _c) {
    final String txt = ((JTextField) _c).getText();
    return txt == null || txt.trim().length() == 0;
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    ((JTextComponent) _comp).setText(toString(_s));
  }

  @Override
  public Class getDataClass() {
    return String.class;
  }

  @Override
  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  @Override
  public boolean isEditable() {
    return editable;
  }

  @Override
  public String toString(final Object _s) {
    if (_s == null) {
      return CtuluLibString.EMPTY_STRING;
    } else if (_s instanceof Number) {
      return getFinalString(((Number) _s).doubleValue());
    } else {
      try {
        return getFinalString(Double.parseDouble(_s.toString()));
      } catch (final NumberFormatException e) {
      }
    }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public Object parseString(String in) {
    return CtuluDurationFormatter.getSec(in);
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createEditorComponent(), true) {
      @Override
      public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _selected,
                                                   final int _row, final int _column) {
        final Component r = super.getTableCellEditorComponent(_table, _value, _selected, _row, _column);

        r.requestFocusInWindow();
        return r;
      }

      @Override
      public boolean stopCellEditing() {
        if (getCellEditorValue() == null) {
          return false;
        }
        return super.stopCellEditing();
      }
    };
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createCommonEditorComponent(), true) {
      @Override
      public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _selected,
                                                   final int _row, final int _column) {
        final Component r = super.getTableCellEditorComponent(_table, _value, _selected, _row, _column);
        r.requestFocusInWindow();
        return r;
      }

      @Override
      public boolean stopCellEditing() {
        if (getCellEditorValue() == null) {
          return false;
        }
        return super.stopCellEditing();
      }
    };
  }

  public String getFinalString(final double _s) {
    if (fmt_ == null) {
      return Integer.toString((int) _s);
    }
    return fmt_.format(_s);
  }

  public void setValue(final double _s, final Component _comp) {
    ((JTextComponent) _comp).setText(getFinalString(_s));
  }

  @Override
  public Object getValue(final Component _comp) {
    return stringToValue(((JTextComponent) _comp).getText());
  }

  public Object stringToValue(final String _value) {
    try {
      return new Integer(CtuluDurationFormatter.getSec(_value));
    } catch (final NumberFormatException e) {
      return null;
    }
  }

  @Override
  public String getStringValue(final Component _comp) {
    final String s = ((JTextComponent) _comp).getText();
    try {
      return Integer.toString(CtuluDurationFormatter.getSec(s));
    } catch (final NumberFormatException e) {
      return null;
    }
  }

  @Override
  public JComponent createEditorComponent() {
    final BuTextField tf = new BuTextField();
    tf.setColumns(10);
    tf.setValueValidator(new BuValueValidator() {
      @Override
      public boolean isValueValid(final Object _value) {
        if (_value == null) {
          return true;
        }
        return CtuluValueEditorTime.this.isValid(_value);
      }
    });
    tf.setStringValidator(new BuStringValidator() {
      @Override
      public String valueToString(final Object _value) {
        return CtuluValueEditorTime.this.toString(_value);
      }

      @Override
      public Object stringToValue(final String _string) {
        return CtuluValueEditorTime.this.stringToValue(_string);
      }

      @Override
      public boolean isStringValid(final String _string) {
        return CtuluValueEditorTime.this.isValid(_string);
      }
    });
    return tf;
  }

  @Override
  public boolean isValid(final Object _o) {
    return _o == null ? false : isValid(_o.toString());
  }

  public boolean isValid(final String _o) {
    try {
      CtuluDurationFormatter.getSec(_o);
      return true;
    } catch (final NumberFormatException _f) {
      FuLog.warning(_f);
      return false;
    }
  }

  @Override
  public String getValidationMessage() {

    return CtuluDurationFormatter.getDesc();
  }

  @Override
  public boolean isValueValidFromComponent(final Component _comp) {
    return isValid(((JTextComponent) _comp).getText());
  }
}
