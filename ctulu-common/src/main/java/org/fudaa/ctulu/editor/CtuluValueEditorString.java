/*
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2007-03-09 08:37:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.*;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import java.awt.*;

/**
 * Un editeur pour une string, sous forme standard ou sous forme de formule.
 *
 * @author Fred Deniger, marchand@deltacad.fr
 * @version $Id: CtuluValueEditorString.java,v 1.4 2007-03-09 08:37:37 deniger Exp $
 */
public final class CtuluValueEditorString implements CtuluValueEditorI {
  protected final CtuluExpr expr_;
  boolean isEditable_ = true;
  private String separator;
  private final TableCellRenderer renderer_ = new BuTableCellRenderer();

  @Override
  public TableCellRenderer createTableRenderer() {
    return renderer_;
  }

  @Override
  public String getSeparator() {
    return separator;
  }

  public void setSeparator(String separator) {
    this.separator = separator;
  }

  protected BuValueValidator val_;
  // Le string validator si l'editeur accepte les expressions.
  protected CtuluExprStringValidator stringVal_;

  @Override
  public Class getDataClass() {
    return String.class;
  }

  @Override
  public String getValidationMessage() {
    if (val_ instanceof CtuluValueValidator) {
      return ((CtuluValueValidator) val_).getDescription();
    }
    return null;
  }

  @Override
  public String parseString(String in) {
    return in;
  }

  @Override
  public boolean isValueValidFromComponent(final Component _comp) {
    return ((BuTextField) _comp).getValue() != null;
  }

  @Override
  public boolean isEmpty(final Component _c) {
    final String txt = ((JTextField) _c).getText();
    return txt == null || txt.trim().length() == 0;
  }

  /**
   * Un editeur ne g�rant pas les formules.
   */
  public CtuluValueEditorString() {
    this(false);
  }

  /**
   * Un editeur g�rant ou non les formules.
   *
   * @param _formule True : G�re les formules.
   */
  public CtuluValueEditorString(final boolean _formule) {
    expr_ = _formule ? new CtuluExpr() : null;
    if (expr_ != null) {
      stringVal_ = new CtuluExprStringValidators.StringOption(expr_);
    }
  }

  @Override
  public boolean isEditable() {
    return isEditable_;
  }

  /**
   * Teste la valeur pass�e. On consid�re le dernier string validator si
   * les formules sont autoris�es.
   *
   * @param _s La chaine
   */
  public boolean isValid(final String _s) {
    if (expr_ != null) {
      if (stringVal_ != null) {
        return stringVal_.isStringValid(_s);
      }
//      if (!(expr_.getParser().getValueAsObject() instanceof String))
//        return false;
//      return !expr_.getParser().hasError();
    }
    return true;
  }

  @Override
  public Object getValue(final Component _comp) {
    return ((BuTextField) _comp).getValue();
  }

  @Override
  public boolean isValid(final Object _o) {
    return isValid(_o == null ? null : _o.toString());
  }

  @Override
  public JComponent createCommonEditorComponent() {
    BuTextField r = null;
    if (isEditable_ && expr_ != null) {
      final CtuluExpr exp = new CtuluExpr(expr_);

      stringVal_.setExpressionExpected(false);
      r = new CtuluExprTextField(stringVal_);
      if (val_ != null) {
        r.setValueValidator(new CtuluExprValueValidator(val_));
      }
    } else {
      r = new BuTextField();
      if (val_ != null) {
        r.setValueValidator(val_);
      }
    }
    r.setColumns(10);
    r.setEditable(isEditable_);

    return r;
  }

  @Override
  public void setEditable(final boolean _isEditable) {
    isEditable_ = _isEditable;
  }

  public void setVal(final BuValueValidator _val) {
    val_ = _val;
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    ((JTextField) _comp).setText(toString(_s));
  }

  @Override
  public String toString(final Object _o) {
    return _o == null ? CtuluLibString.EMPTY_STRING : _o.toString();
  }

  @Override
  public String getStringValue(final Component _comp) {
    return ((JTextField) _comp).getText();
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createEditorComponent(), true);
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return new BuTableCellEditor((BuTextField) createCommonEditorComponent(), true);
  }

  @Override
  public JComponent createEditorComponent() {
    return createCommonEditorComponent();
  }
}
