/*
 GPL 2
 */
package org.fudaa.ctulu.editor;

/**
 *
 * @author Frederic Deniger
 */
public interface PostfixMathCommandEnhancedI {

  boolean needQuotes();
}
