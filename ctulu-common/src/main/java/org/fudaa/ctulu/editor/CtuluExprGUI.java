/*
 *  @creation     27 mai 2005
 *  @modification $Date: 2006-09-19 14:36:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.editor;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTable;
import org.nfunk.jep.Variable;
import org.nfunk.jep.function.PostfixMathCommandI;

/**
 * @author Fred Deniger
 * @version $Id: CtuluExprGUI.java,v 1.9 2006-09-19 14:36:55 deniger Exp $
 */
public class CtuluExprGUI extends BuPanel implements ActionListener, DocumentListener {

  /**
   * @author fred deniger
   * @version $Id: CtuluExprGUI.java,v 1.9 2006-09-19 14:36:55 deniger Exp $
   */
  private final class InfoUpdater implements ListSelectionListener {

    /**
     *
     */
    private final BuLabel lb_;

    /**
     * @param _lb
     */
    InfoUpdater(final BuLabel _lb) {
      lb_ = _lb;
    }

    public void updateCst() {
      Variable v = null;
      if (!cstSelected_.isSelectionEmpty()) {
        v = (Variable) cstModel_.getElementAt(cstSelected_.getMaxSelectionIndex());

      }
      updateTxt(v);
    }

    public void updateVar() {
      Variable v = null;
      if (!varSelected_.isSelectionEmpty()) {
        v = (Variable) model_.getElementAt(varSelected_.getMaxSelectionIndex());

      }
      updateTxt(v);
    }

    @Override
    public void valueChanged(final ListSelectionEvent _e) {

      if (_e.getSource() == cstSelected_) {
        updateCst();
      }
      if (_e.getSource() == varSelected_) {
        updateVar();
      }

    }

    private void updateTxt(final Variable _v) {
      String txt = CtuluLibString.ESPACE;
      if (_v != null) {

        final String name = _v.getName();
        if (expr_.containsDescFor(name)) {
          txt = name + ": " + expr_.getDesc(name);
        } else {
          txt = name;
        }
      }
      lb_.setText(txt);
    }
  }

  class ValueTableModel extends AbstractTableModel {

    protected void fireChanged() {
      super.fireTableRowsDeleted(0, getRowCount());
      super.fireTableRowsInserted(0, getRowCount());
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex == 0) {
        return String.class;
      }
      return Double.class;
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public String getColumnName(final int _column) {
      return CtuluLibString.ESPACE;
    }

    @Override
    public int getRowCount() {
      return varToSet_ == null ? 0 : varToSet_.length;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return varToSet_[_rowIndex].getName() + "= ";
      }
      return varToSet_[_rowIndex].getValue();
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return _columnIndex == 1;
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_value == null || _value.equals(varToSet_[_rowIndex].getValue())) {
        return;
      }
      varToSet_[_rowIndex].setValue(_value);
      clearResult();
    }
  }

  private static final class VariableModel extends AbstractListModel implements ComboBoxModel {

    final List l_;
    Object selected_;

    VariableModel(final List _l) {
      l_ = _l;
    }

    protected void fireAllChanged() {
      super.fireIntervalAdded(this, 0, getSize());
    }

    @Override
    public Object getElementAt(final int _index) {
      return l_.get(_index);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return l_.size();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (selected_ != _anItem) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  class VarRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
            final boolean _isSelected, final boolean _cellHasFocus) {
      final Component c = super.getListCellRendererComponent(_list, _value, _index, _isSelected, _cellHasFocus);
      if (_value == null) {
        return c;
      }
      final Variable v = (Variable) _value;
      final String name = v.getName();
      setText(name);
      final StringBuffer buf = new StringBuffer("<html><body><table cellpadding=\"0\" cellspacing=\"0\"><tr><td>");
      buf.append(name).append("</td><td>");
      if (v.isConstant()) {
        buf.append('(').append(CtuluLib.getS("Constante")).append(')');
      }
      buf.append("</td></tr><tr><td>");
      if (expr_.containsDescFor(name)) {
        buf.append(CtuluLib.getS("Desc")).append("</td><td>: ").append(expr_.getDesc(name))
                .append("</td></tr><tr><td>");
      }
      buf.append(CtuluLib.getS("Valeur courante")).append("</td><td>: ").append(CtuluLibString.ESPACE).append(
              v.getValue());
      buf.append("</td></tr></table></body></html>");
      setToolTipText(buf.toString());
      return c;
    }
  }

  static class TableCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
            final boolean _hasFocus, final int _row, final int _column) {
      final Component r = super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      if (_value == null) {
        setText(CtuluLib.getS("Pr�ciser une valeur"));
        setForeground(Color.RED);
      }
      return r;
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: CtuluExprGUI.java,v 1.9 2006-09-19 14:36:55 deniger Exp $
   */
  public static class VariableComparator implements Comparator {

    @Override
    public int compare(final Object _o1, final Object _o2) {
      if (_o1 == _o2) {
        return 0;
      }
      if (_o1 == null) {
        return -1;
      }
      return ((Variable) _o1).getName().compareToIgnoreCase(((Variable) _o2).getName());
    }
  }
  private boolean isValid_;
  BuButton btEvaluate_;
  final BuButton btValid_;
  final transient CtuluExpr expr_;
  final VariableModel fctModel_;
  final transient ListSelectionModel fctSelected_;
  final VariableModel model_;
  final VariableModel cstModel_;
  BuTextField tfResult_;
  final BuTextArea txt_;
  final BuLabel txtError_;
  transient Variable[] varToSet_;
  final transient ListSelectionModel varSelected_;
  final transient ListSelectionModel cstSelected_;
  final ValueTableModel tModel_;
  final BuTable table_;

  /**
   * Construit a partir de l'expression par default.
   *
   * @param _expr l'expression
   */
  public CtuluExprGUI(final CtuluExpr _expr) {
    expr_ = _expr;
    ArrayList list = new ArrayList(expr_.getParser().getFunctionTable().keySet());
    Collections.sort(list);
    fctModel_ = new VariableModel(list);
    list = new ArrayList(expr_.getParser().getSymbolTable().values());
    Collections.sort(list, new VariableComparator());
    final ArrayList varList = new ArrayList(list.size());
    final ArrayList cstList = new ArrayList(list.size());
    final int nb = list.size();
    for (int i = 0; i < nb; i++) {
      final Variable vi = (Variable) list.get(i);
      if (vi.isConstant()) {
        cstList.add(vi);
      } else {
        varList.add(vi);
      }
    }
    model_ = new VariableModel(varList);
    cstModel_ = new VariableModel(cstList);
    final BuEmptyList listVar = new BuEmptyList();
    final BuEmptyList listCst = new BuEmptyList();
    initCstVar(listVar, listCst);
    final BuEmptyList listFct = new BuEmptyList();
    fctSelected_ = listFct.getSelectionModel();
    initFctList(listFct);
    setLayout(new BuVerticalLayout(5, true, false));
    final BuPanel top = new BuPanel();
    final LayoutManager lay = new BuVerticalLayout(2);
    top.setLayout(new BuHorizontalLayout(8, true, false));
    top.setBorder(BuBorders.EMPTY5555);
    BuPanel var = new BuPanel(lay);
    var.add(new BuLabel(CtuluLib.getS("Fonctions")));
    BuScrollPane scroll = new BuScrollPane(listFct);
    var.add(scroll);
    top.add(var);
    add(top);
    final BuLabel lb = new BuLabel(CtuluLibString.ESPACE);
    add(lb);

    var = new BuPanel(lay);
    var.add(new BuLabel(CtuluLib.getS("Constantes")));
    listCst.setVisibleRowCount(3);
    var.add(new BuScrollPane(listCst));
    final VarRenderer render = new VarRenderer();
    listCst.setCellRenderer(render);
    top.add(var);
    var = new BuPanel(lay);
    var.add(new BuLabel(CtuluLib.getS("Variables")));
    listVar.setVisibleRowCount(3);
    var.add(new BuScrollPane(listVar));
    listVar.setCellRenderer(render);
    top.add(var);
    tModel_ = new ValueTableModel();
    table_ = new CtuluTable(tModel_);
    table_.getColumnModel().getColumn(0).setMaxWidth(25);
    table_.getColumnModel().getColumn(1).setCellRenderer(new TableCellRenderer());
    var = new BuPanel(lay);
    var.add(new BuLabel(CtuluLib.getS("Variables utilis�es")));
    final int prefH = scroll.getPreferredSize().height;
    table_.setTableHeader(null);
    scroll = new BuScrollPane(table_);
    scroll.setPreferredHeight(prefH);
    var.add(scroll);
    top.add(var);

    txt_ = new BuTextArea();
    txt_.getDocument().addDocumentListener(this);
    txt_.setColumns(30);
    txt_.setRows(3);
    scroll.setPreferredWidth(60);
    add(new BuScrollPane(txt_));
    final BuPanel pnS = new BuPanel(new BuGridLayout(2, 1, 1, true, false));
    btEvaluate_ = new BuButton();
    btEvaluate_.addActionListener(this);
    btEvaluate_.setText(CtuluLib.getS("Evaluer"));
    btEvaluate_.setIcon(BuResource.BU.getIcon("calculer"));
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 1, 1, true, false));
    pn.add(btEvaluate_);
    tfResult_ = new BuTextField();
    tfResult_.setEditable(false);
    tfResult_.setBackground(txt_.getBackground());
    tfResult_.setOpaque(true);
    tfResult_.setColumns(10);
    pn.add(tfResult_);
    btValid_ = new BuButton();
    btValid_.setActionCommand("VALID");
    btValid_.setEnabled(false);
    btValid_.addActionListener(this);
    btValid_.setIcon(BuResource.BU.getIcon("valider"));
    btValid_.setText(CtuluLib.getS("Valider"));
    pnS.add(btValid_);
    txtError_ = new BuLabel();
    pnS.add(txtError_);
    pnS.add(btEvaluate_);
    pnS.add(tfResult_);
    txt_.setText(expr_.getLastExpr());
    add(pnS);
    valideExpr();
    final InfoUpdater listener = new InfoUpdater(lb);
    varSelected_ = listVar.getSelectionModel();
    varSelected_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    cstSelected_ = listCst.getSelectionModel();
    cstSelected_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    cstSelected_.addListSelectionListener(listener);
    varSelected_.addListSelectionListener(listener);
    listVar.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(final FocusEvent _e) {
      }

      @Override
      public void focusGained(final FocusEvent _e) {
        listener.updateVar();

      }
    });
    listCst.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(final FocusEvent _e) {
      }

      @Override
      public void focusGained(final FocusEvent _e) {
        listener.updateCst();

      }
    });
  }

  private void initFctList(final BuEmptyList _listFct) {
    _listFct.setVisibleRowCount(3);
    _listFct.setModel(fctModel_);
    fctSelected_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _listFct.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent _e) {
        if (_e.getClickCount() >= 2) {
          insertFct();
        }
      }
    });
  }

  private void initCstVar(final BuEmptyList _listVar, final BuEmptyList _listCst) {
    _listVar.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent _e) {
        if (_e.getClickCount() >= 2) {
          insertVar(model_, varSelected_);
        }
      }
    });
    _listCst.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent _e) {
        if (_e.getClickCount() >= 2) {
          insertVar(cstModel_, cstSelected_);
        }
      }
    });
    _listVar.setModel(model_);
    _listCst.setModel(cstModel_);
  }

  public boolean isExprValide() {
    if (btValid_.isEnabled()) {
      valideExpr();
    }
    return isValid_;
  }

  public BuTextArea getTxtArea() {
    return txt_;
  }

  private void updateBt() {
    btValid_.setEnabled(true);
    if (btEvaluate_ != null) {
      btEvaluate_.setEnabled(true);
    }
  }

  public void setError(String msg) {
    txtError_.setForeground(Color.RED);
    txtError_.setToolTipText(msg);
    txtError_.setText(msg);
  }

  public final void valideExpr() {
    expr_.getParser().parseExpression(getExprText());
    isValid_ = !expr_.getParser().hasError();
    if (isValid_) {
      txtError_.setForeground(Color.BLACK);
      txtError_.setText(CtuluLib.getS("Valide"));
      txtError_.setToolTipText(txtError_.getText());
    } else {
      setError(expr_.getParser().getError(0)
              + (expr_.getParser().getNbError() > 0 ? "..." : CtuluLibString.EMPTY_STRING));
      txtError_.setToolTipText(expr_.getParser().getHtmlError());
    }
    btValid_.setEnabled(false);
    if (btEvaluate_ != null) {
      btEvaluate_.setEnabled(isValid_);
    }
    Variable[] newVar = null;
    if (isValid_) {
      newVar = expr_.findUsedVar();
      Arrays.sort(newVar, new VariableComparator());
    }
    if (newVar != varToSet_ && !Arrays.equals(varToSet_, newVar)) {
      varToSet_ = newVar;
      tModel_.fireChanged();
    }

    expr_.findUsedVar();
  }

  protected void clearResult() {
    if (tfResult_ != null) {
      tfResult_.setText(CtuluLibString.EMPTY_STRING);
    }
  }

  String containsNullValue() {
    final StringBuffer buf = new StringBuffer();
    boolean aie = false;
    final int nb = varToSet_ == null ? 0 : varToSet_.length;
    for (int i = 0; i < nb; i++) {
      final Variable v = varToSet_[i];
      if (v.getValue() == null) {
        aie = true;
        if (buf.length() > 0) {
          buf.append(", ");
        }
        buf.append(v.getName());
      }
    }
    return aie ? buf.toString() : null;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == btValid_) {
      valideExpr();
    } else if (btEvaluate_ == _e.getSource()) {
      valideExpr();
      if (isValid_) {
        if (table_.isEditing()) {
          table_.getCellEditor().stopCellEditing();
        }
        tfResult_.setForeground(Color.BLACK);
        final Object o = expr_.getParser().getValueAsObject();
        if (o == null) {
          final String badV = containsNullValue();
          if (badV != null) {
            final String txt = CtuluLib.getS("Des valeurs ne sont pas d�finies:") + CtuluLibString.ESPACE + badV;
            tfResult_.setText(txt);
            tfResult_.setToolTipText(txt);
            tfResult_.setForeground(Color.RED);
            return;
          }
        }
        tfResult_.setText(o == null ? CtuluLibString.EMPTY_STRING : o.toString());
      } else {
        tfResult_.setText(CtuluLibString.EMPTY_STRING);
      }
    }
  }

  protected void insertFct() {
    final Object functionName = fctModel_.getElementAt(fctSelected_.getMaxSelectionIndex());
    PostfixMathCommandI command = expr_.getParser().getFunctionTable().get((String) functionName);
    String preSeparator = "(";
    String postSeparator = ")";
    if (command instanceof PostfixMathCommandEnhancedI && ((PostfixMathCommandEnhancedI) command).needQuotes()) {
      preSeparator = "(\"";
      postSeparator = "\")";
    }
    txt_.insert(functionName + preSeparator, txt_.getCaretPosition());
    final int pos = txt_.getCaretPosition();
    txt_.insert(postSeparator, pos);
    txt_.setCaretPosition(pos);
    txt_.requestFocus();
  }

  public void setButtonValidVisible(boolean b) {
    btValid_.setVisible(b);
  }

  protected void insertVar(final VariableModel _var, final ListSelectionModel _select) {
    txt_.insert(((Variable) _var.getElementAt(_select.getMaxSelectionIndex())).getName(), txt_.getCaretPosition());
    txt_.requestFocus();
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    updateBt();
  }

  public final CtuluExpr getExpr() {
    return expr_;
  }

  public String getExprText() {
    String txt = txt_.getText();
    txt = txt.replace('\r', ' ');
    txt = txt.replace('\n', ' ');
    return txt;
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    updateBt();
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    updateBt();
  }
}
