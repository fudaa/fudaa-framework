/**
 *  @creation     25 f�vr. 2005
 *  @modification $Date: 2007-05-21 10:28:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLog;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

/**
 * @author Fred Deniger
 * @version $Id: CtuluNumberFormat.java,v 1.10 2007-05-21 10:28:29 deniger Exp $
 */
public abstract class CtuluNumberFormat extends Format implements CtuluNumberFormatI {
  
 
  

  public void format(final double _d, final StringBuffer _dest) {
    _dest.append(format(_d));
  }

  /**
   * Les FieldMachin sont plus qu'obscurs.
   */
  public final StringBuffer format(final double _number, final StringBuffer _toAppendTo, final FieldPosition _pos) {
    _toAppendTo.append(format(_number));
    return _toAppendTo;
  }

  /**
   * Les FieldMachin sont plus qu'obscurs.
   */
  public final StringBuffer format(final long _number, final StringBuffer _toAppendTo, final FieldPosition _pos) {
    _toAppendTo.append(format(_number));
    return _toAppendTo;
  }

  @Override
  public StringBuffer format(final Object _obj, final StringBuffer _toAppendTo, final FieldPosition _pos) {
    if (_obj instanceof Number) {
      return format(((Number) _obj).doubleValue(), _toAppendTo, _pos);
    } else if (_obj != null) {
      try {
        return format(Double.parseDouble(_obj.toString()), _toAppendTo, _pos);
      } catch (NumberFormatException _evt) {
        FuLog.error(_evt);

      }
    }
    throw new IllegalArgumentException("Cannot format given Object as a Number");
  }

  @Override
  public boolean isDecimal() {
    return true;
  }

  public Number parse(final String _source, final ParsePosition _parsePosition) {
    new Throwable("not supported yet").printStackTrace();
    return null;
  }

  @Override
  public Object parseObject(final String _source, final ParsePosition _pos) {
    new Throwable("Not implemented yet").printStackTrace();
    return null;
  }

}
