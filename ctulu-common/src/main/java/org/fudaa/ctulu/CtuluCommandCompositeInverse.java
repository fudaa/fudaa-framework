/**
 * @creation 21 oct. 2003
 * @modification $Date: 2007-01-17 10:45:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

/**
 * Classe contenant plusieurs commandes. Les actions undo,redo sont transferees aux commandes stockees. Les actions
 * undo/redo se font toujours dans le sens premiere /derniere commande ajoutee. Cette classe existe car dans certains
 * cas, des actions doivent �tre annul�es dans le sens ou elles ont �t� enregistr�e pour des questions de validation de
 * donn�es.
 * 
 * @author deniger
 * @version $Id: CtuluCommandCompositeInverse.java,v 1.5 2007-01-17 10:45:25 deniger Exp $
 */
public class CtuluCommandCompositeInverse extends CtuluCommandComposite {

  public CtuluCommandCompositeInverse() {
    super();
  }

  /**
   * Appelle undo de la premiere � la derniere commande.
   */
  @Override
  public void undo() {
    if (command_ != null) {
      final int n = command_.size();
      for (int i = 0; i < n; i++) {
        ((CtuluCommand) command_.get(i)).undo();
      }
    }
    actionToDoAfterUndoOrRedo();
  }
}