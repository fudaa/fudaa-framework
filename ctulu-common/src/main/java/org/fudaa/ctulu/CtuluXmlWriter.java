/*
 * @creation 1 sept. 06
 * @modification $Date: 2007-05-04 13:43:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLib;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Un writer XML <u>tr�s</u> simplifi�. Ne se base sur aucune librairie XML.
 * 
 * @author fred deniger
 * @version $Id: CtuluXmlWriter.java,v 1.5 2007-05-04 13:43:20 deniger Exp $
 */
public class CtuluXmlWriter {

  Writer out_;
  String mainTag_;

  public CtuluXmlWriter(final OutputStream _out) throws IOException {
    super();
    out_ = new BufferedWriter(new OutputStreamWriter(_out, CtuluLibFile.getUTF8Charset()));
    startDoc();
  }

  private void startDoc() throws IOException {
    out_.write("<?xml version='1.0' encoding='UTF-8'?>");
  }

  public CtuluXmlWriter(final Writer _out) throws IOException {
    super();
    out_ = _out;
    startDoc();
  }

  public void flush() throws IOException {
    out_.flush();
  }

  public static String getXmlLine(final String _tag, final String _entry) {
    return getStartTag() + _tag + getEndTag() + clean(_entry) + getStartClosingTag() + _tag + getEndTag();

  }

  private static String getStartClosingTag() {
    return "</";
  }

  private static String getEndTag() {
    return ">";
  }

  private static String getStartTag() {
    return CtuluLibString.LINE_SEP + "<";
  }

  public void writeEntry(final String _tag, final String _entry) throws IOException {
    out_.write(getXmlLine(_tag, _entry));
  }
  
  /**
   * Debute un tag sans attributs.
   * @param _tag Le tag.
   * @throws IOException
   */
  public void startTag(final String _tag) throws IOException {
    startTagWithAttrs(_tag, null, null);
  }
  
  /**
   * D�bute un tag avec ou sans attributs
   * @param _tag Le tag
   * @param _attNames Les noms d'attributs. Peut etre null.
   * @param _attVals Les valeursz d'attributs. Peut etre null.
   * @throws IOException
   */
  public void startTagWithAttrs(final String _tag, String[] _attNames, String[] _attVals) throws IOException {
    out_.write(getStartTag() + _tag + attrs2Xml(_attNames, _attVals) + getEndTag());
  }
  
  /**
   * Finalise l'�criture du tag
   * @param _tag Le tag.
   * @throws IOException
   */
  public void endTag(final String _tag) throws IOException {
    out_.write(getStartClosingTag() + _tag + getEndTag());
  }
  
  /**
   * Ecrit le texte d'un tag ouvert.
   * @param _entry Le texte.
   * @throws IOException
   */
  public void writeEntry(final String _entry) throws IOException {
    out_.write(clean(_entry));
  }

  /**
   * Transforme les attributs name/valeur en une chaine � �crire dans le tag.
   * @param _attNames Les noms d'attributs
   * @param _attVals Les valeurs d'attributs associ�s
   * @return La chaine.
   */
  private String attrs2Xml(String[] _attNames, String[] _attVals) {
    if (_attNames==null || _attVals==null) return "";
    
    StringBuilder sb=new StringBuilder();
    for (int i=0; i<Math.min(_attNames.length,_attVals.length); i++) {
      sb.append(" ").append(_attNames[i]).append("=\"").append(clean(_attVals[i])).append("\"");
    }
    
    return sb.toString();
  }

  public void writeComment(final String _comment) throws IOException {
    out_.write(CtuluLibString.LINE_SEP);
    out_.write("<!-- " + _comment + "-->");
  }

  public void write(final String _str) throws IOException {
    out_.write(_str);
  }

  public void write(final char _str) throws IOException {
    out_.write(_str);
  }

  protected static String clean(final String _s) {
    String res = FuLib.replace(_s, "&", "&amp;");
    res = FuLib.replace(res, getStartTag(), "&lt;");
    res = FuLib.replace(res, getEndTag(), "&gt;");
    res = FuLib.replace(res, "\"", "&quot;");
    return res;
  }

  protected static String cleanInverse(final String _s) {
    String res = FuLib.replace(_s, "&amp;", "&");
    res = FuLib.replace(res, "&lt;", getStartTag());
    res = FuLib.replace(res, "&gt;", getEndTag());
    res = FuLib.replace(res, "&quot;", "\"");
    return res;
  }

  public String getMainTag() {
    return mainTag_;
  }

  public void setMainTag(final String _mainTag) throws IOException {
    if (mainTag_ == null) {
      mainTag_ = _mainTag;
      out_.write(getStartTag() + mainTag_ + getEndTag());
    }
  }

  /**
   * Finalise le tag principal.
   * @throws IOException
   */
  public void finish() throws IOException {
    if (mainTag_ != null) {
      out_.write(CtuluLibString.LINE_SEP + getStartClosingTag() + mainTag_ + getEndTag());
    }
    flush();
  }

}
