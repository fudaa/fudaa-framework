package org.fudaa.ctulu;

/**
 * Level used in log
 *
 * @author deniger
 */
public enum CtuluLogLevel {

  DEBUG(50),
  INFO(40),
  WARNING(30),
  ERROR(20),
  SEVERE(10),
  FATAL(0);
  private final int verbosity;

  private CtuluLogLevel(int verbosity) {
    this.verbosity = verbosity;
  }

  public int getVerbosity() {
    return verbosity;
  }

  public boolean isMoreVerboseThan(CtuluLogLevel level, boolean strict) {
    if (level == null) {
      return false;
    }
    if (strict) {
      return verbosity > level.verbosity;
    }
    return verbosity >= level.verbosity;
  }

  public boolean isLessVerboseThan(CtuluLogLevel level, boolean strict) {
    if (level == null) {
      return false;
    }
    if (strict) {
      return verbosity < level.verbosity;
    }
    return verbosity <= level.verbosity;
  }
}
