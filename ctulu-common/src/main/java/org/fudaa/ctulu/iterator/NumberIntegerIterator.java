/*
 *  @creation     24 f�vr. 2005
 *  @modification $Date: 2007-01-17 10:45:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.iterator;

/**
 * @author Fred Deniger
 * @version $Id: NumberIntegerIterator.java,v 1.1 2007-01-17 10:45:26 deniger Exp $
 */
public class NumberIntegerIterator extends NumberIterator {

  @Override
  public void init(final double _minimum,final double _maximum,final int _nbTick){
    super.init((int) _minimum, (int) _maximum, _nbTick);
    increment_=(int)increment_;
  }

  @Override
  protected void buildFormat(final boolean _sci){
    super.buildFormat(false);
    format_.setParseIntegerOnly(true);
  }
}