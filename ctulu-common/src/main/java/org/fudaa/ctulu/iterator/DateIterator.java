/*
 * Geotools 2 - OpenSource mapping toolkit
 * (C) 2003, Geotools Project Managment Committee (PMC)
 * (C) 2000, Institut de Recherche pour le D�veloppement
 * (C) 1999, P�ches et Oc�ans Canada
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Contacts:
 *     UNITED KINGDOM: James Macgill
 *             mailto:j.macgill@geog.leeds.ac.uk
 *
 *     FRANCE: Surveillance de l'Environnement Assist�e par Satellite
 *             Institut de Recherche pour le D�veloppement / US-Espace
 *             mailto:seasnet@teledetection.fr
 *
 *     CANADA: Observatoire du Saint-Laurent
 *             Institut Maurice-Lamontagne
 *             mailto:osl@osl.gc.ca
 */
package org.fudaa.ctulu.iterator;

// Date and time
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.fudaa.ctulu.CtuluLib;

/**
 * It�rateur balayant les barres et �tiquettes de graduation d'un axe du temps. Cet it�rateur retourne les positions des
 * graduations � partir de la date la plus ancienne jusqu'� la date la plus r�cente. Il choisit les intervalles de
 * graduation en supposant qu'on utilise un calendrier gr�gorien.
 * 
 * @version $Id: DateIterator.java,v 1.1 2007-01-17 10:45:26 deniger Exp $
 * @author Martin Desruisseaux
 */
public final class DateIterator implements TickIterator {

  @Override
  public double getPositionFromValue(final double _v) {
    return _v;
  }

  @Override
  public double getValueFromPosition(final double _v) {
    return _v;
  }

  @Override
  public String formatSubValue(final double _v) {
    return formatValue(_v);
  }

  /**
   * Nombre de millisecondes dans certaines unit�s de temps.
   */
  private static final long SEC = 1000;

  private static final long MIN = 60 * SEC;

  private static final long HRE = 60 * MIN;

  private static final long DAY = 24 * HRE;

  private static final long YEAR = 365 * DAY + (DAY / 4) - (DAY / 100) + (DAY / 400);

  private static final long MNT = YEAR / 12;

  /**
   * Liste des intervales souhait�s pour la graduation. Les �l�ments de cette table doivent obligatoirement appara�tre
   * en ordre croissant. Voici un exemple d'interpr�tation: la pr�sence de <code>5*MIN</code> suivit de
   * <code>10*MIN</code> implique que si le pas estim� se trouve entre 5 et 10 minutes, ce sera le pas de 10 minutes qui
   * sera s�lectionn�.
   */
  private static final long[] INTERVAL = { SEC, 2 * SEC, 5 * SEC, 10 * SEC, 15 * SEC, 20 * SEC, 30 * SEC, MIN, 2 * MIN,
      5 * MIN, 10 * MIN, 15 * MIN, 20 * MIN, 30 * MIN, HRE, 2 * HRE, 3 * HRE, 4 * HRE, 6 * HRE, 8 * HRE, 12 * HRE, DAY,
      2 * DAY, 3 * DAY, 7 * DAY, 14 * DAY, 21 * DAY, MNT, 2 * MNT, 3 * MNT, 4 * MNT, 6 * MNT, YEAR, 2 * YEAR, 3 * YEAR,
      4 * YEAR, 5 * YEAR };

  /**
   * Intervalles des graduations principales et des sous-graduations correspondants � chaque des intervalles du tableau
   * {@link #INTERVAL}. Cette classe cherchera d'abord un intervalle en millisecondes dans le tableau {@link #INTERVAL},
   * puis traduira cet intervalle en champ du calendrier gr�gorien en lisant les �l�ments correspondants de ce tableau
   * {@link #ROLL}.
   */
  private static final byte[] ROLL = {
      1,
      (byte) Calendar.SECOND,
      25,
      (byte) Calendar.MILLISECOND, // x10
      // millis
      2,
      (byte) Calendar.SECOND,
      50,
      (byte) Calendar.MILLISECOND, // x10 millis
      5, (byte) Calendar.SECOND, 1, (byte) Calendar.SECOND, 10, (byte) Calendar.SECOND, 2, (byte) Calendar.SECOND, 15,
      (byte) Calendar.SECOND, 5, (byte) Calendar.SECOND, 20, (byte) Calendar.SECOND, 5, (byte) Calendar.SECOND, 30,
      (byte) Calendar.SECOND, 5, (byte) Calendar.SECOND, 1, (byte) Calendar.MINUTE, 10, (byte) Calendar.SECOND, 2,
      (byte) Calendar.MINUTE, 30, (byte) Calendar.SECOND, 5, (byte) Calendar.MINUTE, 1, (byte) Calendar.MINUTE, 10,
      (byte) Calendar.MINUTE, 2, (byte) Calendar.MINUTE, 15, (byte) Calendar.MINUTE, 5, (byte) Calendar.MINUTE, 20,
      (byte) Calendar.MINUTE, 5, (byte) Calendar.MINUTE, 30, (byte) Calendar.MINUTE, 5, (byte) Calendar.MINUTE, 1,
      (byte) Calendar.HOUR_OF_DAY, 15, (byte) Calendar.MINUTE, 2, (byte) Calendar.HOUR_OF_DAY, 30,
      (byte) Calendar.MINUTE, 3, (byte) Calendar.HOUR_OF_DAY, 30, (byte) Calendar.MINUTE, 4,
      (byte) Calendar.HOUR_OF_DAY, 1, (byte) Calendar.HOUR_OF_DAY, 6, (byte) Calendar.HOUR_OF_DAY, 1,
      (byte) Calendar.HOUR_OF_DAY, 8, (byte) Calendar.HOUR_OF_DAY, 2, (byte) Calendar.HOUR_OF_DAY, 12,
      (byte) Calendar.HOUR_OF_DAY, 2, (byte) Calendar.HOUR_OF_DAY, 1, (byte) Calendar.DAY_OF_MONTH, 4,
      (byte) Calendar.HOUR_OF_DAY, 2, (byte) Calendar.DAY_OF_MONTH, 6, (byte) Calendar.HOUR_OF_DAY, 3,
      (byte) Calendar.DAY_OF_MONTH, 12, (byte) Calendar.HOUR_OF_DAY, 7, (byte) Calendar.DAY_OF_MONTH, 1,
      (byte) Calendar.DAY_OF_MONTH, 14, (byte) Calendar.DAY_OF_MONTH, 2, (byte) Calendar.DAY_OF_MONTH, 21,
      (byte) Calendar.DAY_OF_MONTH, 7, (byte) Calendar.DAY_OF_MONTH, 1, (byte) Calendar.MONTH, 7,
      (byte) Calendar.DAY_OF_MONTH, 2, (byte) Calendar.MONTH, 14, (byte) Calendar.DAY_OF_MONTH, 3,
      (byte) Calendar.MONTH, 14, (byte) Calendar.DAY_OF_MONTH, 4, (byte) Calendar.MONTH, 1, (byte) Calendar.MONTH, 6,
      (byte) Calendar.MONTH, 1, (byte) Calendar.MONTH, 1, (byte) Calendar.YEAR, 2, (byte) Calendar.MONTH, 2,
      (byte) Calendar.YEAR, 4, (byte) Calendar.MONTH, 3, (byte) Calendar.YEAR, 6, (byte) Calendar.MONTH, 4,
      (byte) Calendar.YEAR, 1, (byte) Calendar.YEAR, 5, (byte) Calendar.YEAR, 1, (byte) Calendar.YEAR };

  /**
   * Nombre de colonne dans le tableau {@link ROLL}. Le tableau {@link ROLL}doit �tre interpr�t� comme une matrice de 4
   * colonnes et d'un nombre ind�termin� de lignes.
   */
  private static final int ROLL_WIDTH = 4;

  /**
   * Liste des champs de dates qui apparaissent dans le tableau {@link ROLL}. Cette liste doit �tre du champ le plus
   * grand (YEAR) vers le champ le plus petit (MILLISECOND).
   */
  private static final int[] FIELD = { Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY,
      Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND };

  /**
   * Liste des noms des champs (� des fins de d�boguage seulement). Cette liste doit �tre dans le m�me ordre que les
   * �l�ments de {@link #FIELD}.
   */
  private static final String[] FIELD_NAME = { "YEAR", "MONTH", "DAY", "HOUR", "MINUTE", "SECOND", "MILLISECOND" };

  /**
   * Date de la premi�re graduation principale. Cette valeur est fix�e par {@link #init}.
   */
  private long minimum_;

  /**
   * Date limite des graduations. La derni�re graduation ne sera pas n�cessairement � cette date. Cette valeur est fix�e
   * par {@link #init}.
   */
  private long maximum_;

  /**
   * Estimation de l'intervalle entre deux graduations principales. Cette valeur est fix�e par {@link #init}.
   */
  private long increment_;

  /**
   * Longueur de l'axe (en points). Cette information est conserv�e afin d'�viter de refaire toute la proc�dure
   * {@link #init}si les param�tres n'ont pas chang�s.
   */
  private float visualLength_;

  /**
   * Espace � laisser (en points) entre les graduations principales. Cette information est conserv�e afin d'�viter de
   * refaire toute la proc�dure {@link #init}si les param�tres n'ont pas chang�s.
   */
  private float visualTickSpacing_;

  /**
   * Nombre de fois qu'il faut incr�menter le champ {@link #tickField_}du calendrier pour passer � la graduation
   * suivante. Cette op�ration peut se faire avec <code>calendar.add(tickField, tickAdd)</code>.
   */
  private int tickAdd_;

  /**
   * Champ du calendrier qu'il faut incr�menter pour passer � la graduation suivante. Cette op�ration peut se faire avec
   * <code>calendar.add(tickField, tickAdd)</code>.
   */
  private int tickField_;

  /**
   * Nombre de fois qu'il faut incr�menter le champ {@link #tickField_}du calendrier pour passer � la sous-graduation
   * suivante. Cette op�ration peut se faire avec <code>calendar.add(tickField, tickAdd)</code>.
   */
  private int subTickAdd_;

  /**
   * Champ du calendrier qu'il faut incr�menter pour passer � la sous-graduation suivante. Cette op�ration peut se faire
   * avec <code>calendar.add(tickField, tickAdd)</code>.
   */
  private int subTickField_;

  /**
   * Date de la graduation principale ou secondaire actuelle. Cette valeur sera modifi�e � chaque appel � {@link #next}.
   */
  private long value_;

  /**
   * Date de la prochaine graduation principale. Cette valeur sera modifi�e � chaque appel � {@link #next}.
   */
  private long nextTick_;

  /**
   * Date de la prochaine graduation secondaire. Cette valeur sera modifi�e � chaque appel � {@link #next}.
   */
  private long nextSubTick_;

  /**
   * Indique si {@link #value_}repr�sente une graduation principale.
   */
  private boolean isMajorTick_;

  /**
   * Valeurs de {@link #value_},{@link #nextTick_}et {@link #nextSubTick0_}imm�diatement apr�s l'appel de
   * {@link #rewind}.
   */
  private long value0_;

  private long nextTick0_;

  private long nextSubTick0_;

  /**
   * Valeur de {@link #isMajorTick_}imm�diatement apr�s l'appel de {@link #rewind}.
   */
  private boolean isMajorTick0_;

  /**
   * Calendrier servant � avancer d'une certaine p�riode de temps (jour, semaine, mois...). Note: Par convention et pour
   * des raisons de performances (pour �viter d'imposer au calendrier de recalculer ses champs trop souvent), ce
   * calendrier devrait toujours contenir la date {@link #nextSubTick_}.
   */
  private Calendar calendar_;

  /**
   * Objet temporaire � utiliser pour passer des dates en argument � {@link #calendar_}et {@link #format_}.
   */
  private final Date date_ = new Date();

  /**
   * Format � utiliser pour �crire les �tiquettes de graduation. Ce format ne sera construit que la premi�re fois o�
   * {@link #currentLabel}sera appel�e.
   */
  private transient DateFormat format_;

  /**
   * Code du format utilis� pour construire le champ de date de {@link #format_}. Les codes valides sont notamment
   * {@link DateFormat#SHORT},{@link DateFormat#MEDIUM}ou {@link DateFormat#LONG}. La valeur -1 indique que le format ne
   * contient pas de champ de date, seulement un champ des heures.
   */
  private transient int dateFormat_ = -1;

  /**
   * Code du format utilis� pour construire le champ des heures de {@link #format_}. Les codes valides sont notamment
   * {@link DateFormat#SHORT},{@link DateFormat#MEDIUM}ou {@link DateFormat#LONG}. La valeur -1 indique que le format ne
   * contient pas de champ des heures, seulement un champ de date.
   */
  private transient int timeFormat_ = -1;

  /**
   * Indique si {@link #format_}est valide. Le format peut devenir invalide si {@link #init}a �t� appel�e. Dans ce cas,
   * il peut falloir changer le nombre de chiffres apr�s la virgule qu'il �crit.
   */
  private transient boolean formatValid_;

  /**
   * Conventions � utiliser pour le formatage des nombres.
   */
  private Locale locale_;

  @Override
  public String formatValue(final double _timeInSec) {
    if (!formatValid_) {
      currentLabel();
    }
    return format_.format(new Date((long) (_timeInSec * 1000)));
  }

  public DateIterator() {
    this(TimeZone.getDefault(), Locale.getDefault());
  }

  /**
   * Construit un it�rateur pour la graduation d'un axe du temps. La m�thode {@link #init(double, double, int)}<u>doit
   * </u> �tre appel�e avant que l'it�rateur ne soit utilisable.
   * 
   * @param _timezone Fuseau horaire des dates.
   * @param _locale Conventions � utiliser pour le formatage des dates.
   */
  public DateIterator(final TimeZone _timezone, final Locale _locale) {
    // assert INTERVAL.length*ROLL_WIDTH == ROLL.length;
    if (INTERVAL.length * ROLL_WIDTH != ROLL.length) { throw new IllegalArgumentException(); }
    calendar_ = Calendar.getInstance(_timezone, _locale);
    this.locale_ = _locale;
  }

  public DateIterator(final DateIterator _it) {
    if (_it == null) {
      calendar_ = Calendar.getInstance();
      locale_ = Locale.getDefault();
    } else {
      calendar_ = (Calendar) _it.calendar_.clone();
      date_.setTime(_it.date_.getTime());
      dateFormat_ = _it.dateFormat_;
      if (_it.format_ != null) {
        format_ = (DateFormat) _it.format_.clone();
      }
      formatValid_ = _it.formatValid_;
      increment_ = _it.increment_;
      isMajorTick_ = _it.isMajorTick_;
      isMajorTick0_ = _it.isMajorTick0_;
      locale_ = _it.locale_;
      maximum_ = _it.maximum_;
      minimum_ = _it.minimum_;
      nextSubTick_ = _it.nextSubTick_;
      nextSubTick0_ = _it.nextSubTick0_;
      nextTick_ = _it.nextTick_;
      nextTick0_ = _it.nextTick0_;
      subTickAdd_ = _it.subTickAdd_;
      subTickField_ = _it.subTickField_;
      timeFormat_ = _it.timeFormat_;
      value_ = _it.value_;
      value0_ = _it.value0_;
      visualLength_ = _it.visualLength_;
      visualTickSpacing_ = _it.visualLength_;
    }
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return new DateIterator(this);
  }

  public boolean isValid() {
    return formatValid_;
  }

  public void setValid(final boolean _formatValid) {
    formatValid_ = _formatValid;
  }

  @Override
  public double getIncrement() {
    return increment_ / 1000D;
  }

  @Override
  public void init(final double _min, final double _max, final int _nbTick) {
    init((long) (_min * 1000), (long) (_max * 1000), _nbTick <= 2 ? 2 : (_nbTick - 1), 1);
  }

  @Override
  public void initExact(final double _min, final double _max, final int _nbTick, final int nbSousTick) {
    init((long) (_min * 1000), (long) (_max * 1000), _nbTick <= 2 ? 2 : (_nbTick - 1), 1);
  }

  @Override
  public void initExactFromDist(double _min, double _max, double _increment, int _nbSousIteration) {
    if (CtuluLib.isZero(_increment)) {
      init(_min, _max, 1);
    } else init(_min, _max, Math.abs(_max - _min) / Math.abs(_increment));

  }

  /**
   * Initialise l'it�rateur.
   * 
   * @param _minimum Date minimale de la premi�re graduation.
   * @param _maximum Date limite des graduations. La derni�re graduation ne sera pas n�cessairement � cette date.
   * @param _visualLength Longueur visuelle de l'axe sur laquelle tracer la graduation. Cette longueur doit �tre
   *          exprim�e en pixels ou en points.
   * @param visualTickSpace Espace � laisser visuellement entre deux marques de graduation. Cet espace doit �tre exprim�
   *          en pixels ou en points (1/72 de pouce).
   */
  protected void init(final long _minimum, final long _maximum, final float _visualLength,
      final float _visualTickSpacing) {
    if (_minimum == this.minimum_ && _maximum == this.maximum_ && _visualLength == this.visualLength_
        && _visualTickSpacing == this.visualTickSpacing_) {
      rewind();
      return;
    }
    AssertValues.ensureNonNull("visualLength", _visualLength);
    AssertValues.ensureNonNull("visualTickSpacing", _visualTickSpacing);
    this.visualLength_ = _visualLength;
    this.visualTickSpacing_ = _visualTickSpacing;
    this.formatValid_ = false;
    this.minimum_ = _minimum;
    this.maximum_ = _maximum;
    this.increment_ = Math.round((_maximum - _minimum) * ((double) _visualTickSpacing / (double) _visualLength));
    /*
     * Apr�s avoir fait une estimation de l'intervalle d'�chantillonage, v�rifie si on trouve cette estimation dans le
     * tableau 'INTERVAL'. Si on trouve la valeur exacte, tant mieux! Sinon, on cherchera l'intervalle le plus proche.
     */
    int index = Arrays.binarySearch(INTERVAL, increment_);
    if (index < 0) {
      index = ~index;
      if (index == 0) {
        // L'intervalle est plus petit que le
        // plus petit �l�ment de 'INTERVAL'.
        round(Calendar.MILLISECOND);
        findFirstTick();
        return;
      } else if (index >= INTERVAL.length) {
        // L'intervalle est plus grand que le
        // plus grand �l�ment de 'INTERVAL'.
        increment_ /= YEAR;
        round(Calendar.YEAR);
        increment_ *= YEAR;
        findFirstTick();
        return;
      } else {
        // L'index pointe vers un intervalle plus grand que
        // l'intervalle demand�. V�rifie si l'intervalle
        // inf�rieur ne serait pas plus proche.
        if (increment_ - INTERVAL[index - 1] < INTERVAL[index] - increment_) {
          index--;
        }
      }
    }
    this.increment_ = INTERVAL[index];
    index *= ROLL_WIDTH;
    this.tickAdd_ = ROLL[index + 0];
    this.tickField_ = ROLL[index + 1];
    this.subTickAdd_ = ROLL[index + 2];
    this.subTickField_ = ROLL[index + 3];
    if (subTickField_ == Calendar.MILLISECOND) {
      subTickAdd_ *= 10;
    }
    findFirstTick();
  }

  /**
   * Arrondi {@link #increment_}� un nombre qui se lit bien. Le nombre choisit sera un de ceux de la suite 1, 2, 5, 10,
   * 20, 50, 100, 200, 500, etc.
   */
  private void round(final int _field) {
    int factor = 1;
    while (factor <= increment_) {
      factor *= 10;
    }
    if (factor >= 10) {
      factor /= 10;
    }
    increment_ /= factor;
    if (increment_ <= 0) {
      increment_ = 1;
    } else if (increment_ >= 3 && increment_ <= 4) {
      increment_ = 5;
    } else if (increment_ >= 6) {
      increment_ = 10;
    }
    increment_ = Math.max(increment_ * factor, 5);
    tickAdd_ = (int) increment_;
    subTickAdd_ = (int) (increment_ / (increment_ == 2 ? 4 : 5));
    tickField_ = _field;
    subTickField_ = _field;
  }

  /**
   * Replace l'it�rateur sur la premi�re graduation. La position de la premi�re graduation sera calcul�e et retenue pour
   * un positionnement plus rapide � l'avenir.
   */
  private void findFirstTick() {
    calendar_.clear();
    value_ = minimum_;
    date_.setTime(value_);
    calendar_.setTime(date_);
    // Arrondie la date de d�part. Note: ce calcul exige que
    // tous les champs commencent � 0 plut�t que 1, y compris
    // les mois et le jour du mois.
    final int offset = calendar_.getActualMinimum(tickField_);
    int toRound = calendar_.get(tickField_) - offset;
    toRound = (toRound / tickAdd_) * tickAdd_;
    calendar_.set(tickField_, toRound + offset);
    truncate(calendar_, tickField_);
    nextTick_ = calendar_.getTime().getTime();
    nextSubTick_ = nextTick_;
    while (nextTick_ < minimum_) {
      calendar_.add(tickField_, tickAdd_);
      nextTick_ = calendar_.getTime().getTime();
    }
    date_.setTime(nextSubTick_);
    calendar_.setTime(date_);
    while (nextSubTick_ < minimum_) {
      calendar_.add(subTickField_, subTickAdd_);
      nextSubTick_ = calendar_.getTime().getTime();
    }
    /*
     * 'calendar' a maintenant la valeur 'nextSubTick', comme le veut la sp�cification de ce champ. On appelle
     * maintenant 'next' pour transf�rer cette valeur 'nextSubTick' vers 'value'. Notez que 'next' peut �tre appel�e
     * m�me si value>maximum.
     */
    next();

    // Retient les positions trouv�es.
    this.value0_ = this.value_;
    this.nextTick0_ = this.nextTick_;
    this.nextSubTick0_ = this.nextSubTick_;
    this.isMajorTick0_ = this.isMajorTick_;

    // assert ;
    if (calendar_.getTime().getTime() != nextSubTick_) { throw new IllegalArgumentException(); }
  }

  /**
   * Met � 0 tous les champs du calendrier inf�rieur au champ <code>field</code> sp�cifi�. Note: si le calendrier
   * sp�cifi� est {@link #calendar_}, il est de la responsabilit� de l'appelant de restituer {@link #calendar_}dans son
   * �tat correct apr�s l'appel de cette m�thode.
   */
  private static void truncate(final Calendar _calendar, final int _field) {
    int field = _field;
    for (int i = 0; i < FIELD.length; i++) {
      if (FIELD[i] == field) {
        _calendar.get(field); // Force la mise � jour des champs.
        int k = i;
        while (++k < FIELD.length) {
          field = FIELD[k];
          _calendar.set(field, _calendar.getActualMinimum(field));
        }
        break;
      }
    }
  }

  /**
   * Indique s'il reste des graduations � retourner. Cette m�thode retourne <code>true</code> tant que
   * {@link #currentValue}ou {@link #currentLabel}peuvent �tre appel�es.
   */
  @Override
  public boolean hasNext() {
    return value_ <= maximum_;
  }

  /**
   * Indique si la graduation courante est une graduation majeure.
   * 
   * @return <code>true</code> si la graduation courante est une graduation majeure, ou <code>false</code> si elle est
   *         une graduation mineure.
   */
  @Override
  public boolean isMajorTick() {
    return isMajorTick_;
  }

  /**
   * Returns the position where to draw the current tick. The position is scaled from the graduation's minimum to
   * maximum. This is usually the same number than {@link #currentValue}.
   */
  @Override
  public double currentPosition() {
    return value_ / 1000;
  }

  /**
   * Retourne la valeur de la graduation courante. Cette m�thode peut �tre appel�e pour une graduation majeure ou
   * mineure.
   */
  @Override
  public double currentValue() {
    return value_ / 1000;
  }

  /**
   * Retourne l'�tiquette de la graduation courante. On n'appele g�n�ralement cette m�thode que pour les graduations
   * majeures, mais elle peut aussi �tre appel�e pour les graduations mineures. Cette m�thode retourne <code>null</code>
   * s'il n'y a pas d'�tiquette pour la graduation courante.
   */
  @Override
  public String currentLabel() {
    if (!formatValid_) {
      date_.setTime(minimum_);
      calendar_.setTime(date_);
      final int firstDay = calendar_.get(Calendar.DAY_OF_YEAR);
      final int firstYear = calendar_.get(Calendar.YEAR);

      date_.setTime(maximum_);
      calendar_.setTime(date_);
      final int lastDay = calendar_.get(Calendar.DAY_OF_YEAR);
      final int lastYear = calendar_.get(Calendar.YEAR);

      final int dateFormatTmp = (firstYear == lastYear && firstDay == lastDay) ? -1 : DateFormat.MEDIUM;
      final int timeFormatTmp;

      if (increment_ >= DAY) {
        timeFormatTmp = -1;
      } else if (increment_ >= MIN) {
        timeFormatTmp = DateFormat.SHORT;
      } else if (increment_ >= SEC) {
        timeFormatTmp = DateFormat.MEDIUM;
      } else {
        timeFormatTmp = DateFormat.LONG;
      }

      if (dateFormatTmp != this.dateFormat_ || timeFormatTmp != this.timeFormat_ || format_ == null) {
        this.dateFormat_ = dateFormatTmp;
        this.timeFormat_ = timeFormatTmp;
        if (dateFormatTmp == -1) {
          if (timeFormatTmp == -1) {
            format_ = DateFormat.getDateInstance(DateFormat.DEFAULT, locale_);
          } else {
            format_ = DateFormat.getTimeInstance(timeFormatTmp, locale_);
          }
        } else if (timeFormatTmp == -1) {
          format_ = DateFormat.getDateInstance(dateFormatTmp, locale_);
        } else {
          format_ = DateFormat.getDateTimeInstance(dateFormatTmp, timeFormatTmp, locale_);
        }
        format_.setCalendar(calendar_);
      }
      formatValid_ = true;
    }
    date_.setTime(value_);
    final String label = format_.format(date_);
    // Remet 'calendar' dans l'�tat qu'il est senc� avoir
    // d'apr�s la sp�cification du champ {@link #calendar}.
    date_.setTime(nextSubTick_);
    calendar_.setTime(date_);
    return label;
  }

  /**
   * Passe � la graduation suivante.
   */
  @Override
  public void next() {
    // assert calendar.getTime().getTime() == nextSubTick;
    if (calendar_.getTime().getTime() != nextSubTick_) { throw new IllegalArgumentException(); }
    if (nextSubTick_ < nextTick_) {
      isMajorTick_ = false;
      value_ = nextSubTick_;
      /*
       * IMPORTANT: On suppose ici que 'calendar' a d�j� la date 'nextSubTick'. Si ce n'�tait pas le cas, il faudrait
       * ajouter les lignes suivantes:
       */
      // if (false) {
      // date_.setTime(value_);
      // calendar_.setTime(date_);
      // // 'setTime' oblige 'calendar' � recalculer ses
      // // champs, ce qui a un impact sur la performance.
      // }
      calendar_.add(subTickField_, subTickAdd_);
      nextSubTick_ = calendar_.getTime().getTime();
      // 'calendar' contient maintenant la date 'nextSubTick',
      // comme le veut la sp�cification du champ {@link #calendar}.
    } else {
      nextMajor();
    }
  }

  /**
   * Passe directement � la graduation majeure suivante.
   */
  @Override
  public void nextMajor() {
    isMajorTick_ = true;
    value_ = nextTick_;
    date_.setTime(value_);

    calendar_.setTime(date_);
    calendar_.add(tickField_, tickAdd_);
    truncate(calendar_, tickField_);
    nextTick_ = calendar_.getTime().getTime();

    calendar_.setTime(date_);
    calendar_.add(subTickField_, subTickAdd_);
    nextSubTick_ = calendar_.getTime().getTime();
    // 'calendar' contient maintenant la date 'nextSubTick',
    // comme le veut la sp�cification du champ {@link #calendar}.
  }

  /**
   * Replace l'it�rateur sur la premi�re graduation.
   */
  @Override
  public void rewind() {
    this.value_ = value0_;
    this.nextTick_ = nextTick0_;
    this.nextSubTick_ = nextSubTick0_;
    this.isMajorTick_ = isMajorTick0_;
    // Pour �tre en accord avec la sp�cification
    // du champs {@link #calendar}...
    date_.setTime(nextSubTick_);
    calendar_.setTime(date_);
  }

  /**
   * Retourne les conventions � utiliser pour �crire les �tiquettes de graduation.
   */
  public Locale getLocale() {
    return locale_;
  }

  /**
   * Modifie les conventions � utiliser pour �crire les �tiquettes de graduation.
   */
  public void setLocale(final Locale _locale) {
    if (!_locale.equals(this.locale_)) {
      calendar_ = Calendar.getInstance(getTimeZone(), _locale);
      initFormatVar();

    }
    if (calendar_.getTime().getTime() != nextSubTick_) { throw new IllegalArgumentException();
    // assert calendar.getTime().getTime() == nextSubTick;
    }
  }

  private void initFormatVar() {
    format_ = null;
    formatValid_ = false;
    // Pour �tre en accord avec la sp�cification
    // du champs {@link #calendar}...
    date_.setTime(nextSubTick_);
    calendar_.setTime(date_);
  }

  /**
   * Retourne le fuseau horaire utilis� pour exprimer les dates dans la graduation.
   */
  public TimeZone getTimeZone() {
    return calendar_.getTimeZone();
  }

  /**
   * Modifie le fuseau horaire utilis� pour exprimer les dates dans la graduation.
   */
  public void setTimeZone(final TimeZone _timezone) {
    if (!_timezone.equals(getTimeZone())) {
      calendar_.setTimeZone(_timezone);
      initFormatVar();
    }
    if (calendar_.getTime().getTime() != nextSubTick_) { throw new IllegalArgumentException();
    // assert calendar.getTime().getTime() == nextSubTick;
    }
  }

  /**
   * Retourne le nom du champ de {@link Calendar}correspondant � la valeur sp�cifi�e.
   */
  private static String getFieldName(final int _field) {
    for (int i = 0; i < FIELD.length; i++) {
      if (FIELD[i] == _field) { return FIELD_NAME[i]; }
    }
    return String.valueOf(_field);
  }

  /**
   * Returns a string representation of this iterator. Used for debugging purpose only.
   */
  @Override
  public String toString() {
    // Note: in this particular case, using PrintWriter with 'println' generates
    // less bytecodes than chaining StringBuffer.append(...) calls.
    final StringWriter buf = new StringWriter();
    final PrintWriter out = new PrintWriter(buf);
    final DateFormat formatTmp = DateFormat.getDateTimeInstance();
    formatTmp.setTimeZone(calendar_.getTimeZone());
    out.print("Minimum      = ");
    out.println(formatTmp.format(new Date(minimum_)));
    out.print("Maximum      = ");
    out.println(formatTmp.format(new Date(maximum_)));
    out.print("Increment    = ");
    out.print(increment_ / (24 * 3600000f));
    out.println(" days");
    out.print("Tick inc.    = ");
    out.print(tickAdd_);
    out.print(' ');
    out.println(getFieldName(tickField_));
    out.print("SubTick inc. = ");
    out.print(subTickAdd_);
    out.print(' ');
    out.println(getFieldName(subTickField_));
    out.print("Next tick    = ");
    out.println(formatTmp.format(new Date(nextTick_)));
    out.print("Next subtick = ");
    out.println(formatTmp.format(new Date(nextSubTick_)));
    out.flush();
    return buf.toString();
  }

  public void init(double _min, double _max, double pasDeTemps) {
    init(_min, _max, (int) pasDeTemps);

  }

}