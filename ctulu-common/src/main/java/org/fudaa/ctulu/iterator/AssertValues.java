/*
 * Geotools 2 - OpenSource mapping toolkit
 * (C) 2003, Geotools Project Managment Committee (PMC)
 * (C) 2000, Institut de Recherche pour le D�veloppement
 * (C) 1999, P�ches et Oc�ans Canada
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Contacts:
 *     UNITED KINGDOM: James Macgill
 *             mailto:j.macgill@geog.leeds.ac.uk
 *
 *     FRANCE: Surveillance de l'Environnement Assist�e par Satellite
 *             Institut de Recherche pour le D�veloppement / US-Espace
 *             mailto:seasnet@teledetection.fr
 *
 *     CANADA: Observatoire du Saint-Laurent
 *             Institut Maurice-Lamontagne
 *             mailto:osl@osl.gc.ca
 */
package org.fudaa.ctulu.iterator;
// J2SE dependencies
import java.io.Serializable;


/**
 * Base class for graduation.
 *
 * @version $Id: AssertValues.java,v 1.1 2007-01-17 10:45:25 deniger Exp $
 * @author Martin Desruisseaux
 */
public final  class AssertValues implements Serializable {


    /**
     * Serial number for interoperability with different versions.
     */
    private static final long serialVersionUID = 5215728323932315112L;


    /**
     * V�rifie que le nombre sp�cifi� est non-nul. S'il
     * est 0, NaN ou infini, une exception sera lanc�e.
     *
     * @param  _name Nom de l'argument.
     * @param  _n Nombre � v�rifier.
     * @throws IllegalArgumentException Si <var>n</var> est NaN ou infini.
     */
    static void ensureNonNull(final String _name, final double _n) throws IllegalArgumentException {
        if (Double.isNaN(_n) || Double.isInfinite(_n) || _n==0) {
            throw new IllegalArgumentException("invalid data "+_n);
        }

    }

    /**
     * V�rifie que le nombre sp�cifi� est r�el. S'il
     * est NaN ou infini, une exception sera lanc�e.
     *
     * @param  _name Nom de l'argument.
     * @param  _n Nombre � v�rifier.
     * @throws IllegalArgumentException Si <var>n</var> est NaN ou infini.
     */
    static void ensureFinite(final String _name, final double _n) throws IllegalArgumentException {
        if (Double.isNaN(_n) || Double.isInfinite(_n)) {
            throw new IllegalArgumentException("invalid data "+_n);
        }
    }

    /**
     * V�rifie que le nombre sp�cifi� est r�el. S'il
     * est NaN ou infini, une exception sera lanc�e.
     *
     * @param  _name Nom de l'argument.
     * @param  _n Nombre � v�rifier.
     * @throws IllegalArgumentException Si <var>n</var> est NaN ou infini.
     */
    static void ensureFinite(final String _name, final float _n) throws IllegalArgumentException {
        if (Float.isNaN(_n) || Float.isInfinite(_n)) {
            throw new IllegalArgumentException("invalid data "+_n);
        }
    }

    private AssertValues() {
      super();
    }
}
