/*
 */
package org.fudaa.ctulu.iterator;

// Miscellaneous
import com.memoire.fu.FuEmptyArrays;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibArray;

/**
 */
public class FixedIntegerIterator implements TickIterator {

  private final int[] values;
  /**
   * Pour eviter les loop infinie.
   */
  private static final int MAX_ITERATION = 200;
  int iterationDone_;
  /**
   * Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur. Cette valeur est fix�e par {@link #init}.
   */
  private int maximum_;
  /**
   * Valeur de la premi�re graduation principale. Cette valeur est fix�e par {@link #init}.
   */
  private int minimum_;
  /**
   * Index de la graduation principale en cours de tra�age. Cette valeur commence � 0 et sera modifi�e � chaque appel � {@link #next}.
   */
  private int idx;

  /**
   * Construit un it�rateur par d�faut. La m�thode {@link #init(double, double, float, float)}<u>doit </u> �tre appel�e avant que cet it�rateur ne
   * soit utilisable.
   */
  public FixedIntegerIterator(int[] values) {
    this.values = values == null ? FuEmptyArrays.INT0 : CtuluLibArray.copy(values);
    Arrays.sort(this.values);
  }

  public FixedIntegerIterator(final FixedIntegerIterator _it) {
    this.values = _it.values;
    initFrom(_it);
    // this.locale = locale;
  }

  protected final void initFrom(final FixedIntegerIterator _it) {
    if (_it != null) {
      maximum_ = _it.maximum_;
      minimum_ = _it.minimum_;
      idx = _it.idx;
    }
  }

  /**
   * Initialise l'it�rateur.
   *
   * @param _minimum Valeur minimale de la premi�re graduation.
   * @param _maximum Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur.
   * @param _visualLength Longueur visuelle de l'axe sur laquelle tracer la graduation. Cette longueur doit �tre exprim�e en pixels ou en points.
   * @param _visualTickSpacing Espace � laisser visuellement entre deux marques de graduation. Cet espace doit �tre exprim� en pixels ou en points
   * (1/72 de pouce).
   */
  protected void init(final double _minimum, final double _maximum, final float _visualLength,
          final float _visualTickSpacing) {
    if (_minimum == this.minimum_ && _maximum == this.maximum_) {
      rewind();
      return;
    }
    AssertValues.ensureFinite("minimum", _minimum);
    AssertValues.ensureFinite("maximum", _maximum);
    minimum_ = (int) Math.min(_minimum, _maximum);
    maximum_ = (int) Math.max(_minimum, _maximum);

    initCounters();
  }

  @Override
  public void initExact(double _min, double _max, int _nbIteration, int _nbSousIteration) {
    initExactCommon(_min, _max, _nbSousIteration);
  }

  private void initExactCommon(double _min, double _max, int _nbSousIteration) {
    minimum_ = (int) Math.min(_min, _max);
    maximum_ = (int) Math.max(_min, _max);
    initCounters();
  }

  @Override
  public void initExactFromDist(double _min, double _max, double _increment, int _nbSousIteration) {
    initExactCommon(_min, _max, _nbSousIteration);

  }

  @Override
  public final Object clone() throws CloneNotSupportedException {
    final FixedIntegerIterator res = (FixedIntegerIterator) super.clone();
    res.initFrom(this);
    return res;
  }

  /**
   * Retourne l'�tiquette de la graduation courante. On n'appele g�n�ralement cette m�thode que pour les graduations majeures, mais elle peut aussi
   * �tre appel�e pour les graduations mineures. Cette m�thode retourne
   * <code>null</code> s'il n'y a pas d'�tiquette pour la graduation courante.
   */
  @Override
  public String currentLabel() {
    if (idx >= 0 && idx < values.length) {
      return Integer.toString(values[idx]);
    }
    return StringUtils.EMPTY;
  }

  /**
   * Returns the position where to draw the current tick. The position is scaled from the graduation's minimum to maximum. This is usually the same
   * number than {@link #currentValue}. The mean exception is for logarithmic graduation, in which the tick position is not proportional to the tick
   * value.
   */
  @Override
  public double currentPosition() {
    if (idx >= 0 && idx < values.length) {
      return values[idx];
    }
    return Double.NaN;
  }

  /**
   * Retourne la valeur de la graduation courante. Cette m�thode peut �tre appel�e pour une graduation majeure ou mineure.
   */
  @Override
  public double currentValue() {
    return currentPosition();
  }

  @Override
  public String formatSubValue(final double _v) {
    return Integer.toString((int) _v);
  }

  @Override
  public String formatValue(final double _v) {
    return Integer.toString((int) _v);
  }

  @Override
  public double getIncrement() {
    return 1;
  }

  /**
   * Indique s'il reste des graduations � retourner. Cette m�thode retourne
   * <code>true</code> tant que {@link #currentValue}ou {@link #currentLabel}peuvent �tre appel�es.
   */
  @Override
  public boolean hasNext() {
    return iterationDone_ < MAX_ITERATION && idx >= 0 && idx < values.length && values[idx] <= maximum_;
  }

  /**
   * Initialise l'it�rateur.
   *
   * @param _minimum Valeur minimale de la premi�re graduation.
   * @param _maximum Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur.
   * @param _nbTick le nombre de graduation
   */
  @Override
  public void init(final double _minimum, final double _maximum, final int _nbTick) {
    init(_minimum, _maximum, _nbTick <= 1 ? DEFAULT_STICK_NUMBER : _nbTick, 1);
  }

  /**
   * Indique si la graduation courante est une graduation majeure.
   *
   * @return <code>true</code> si la graduation courante est une graduation majeure, ou <code>false</code> si elle est une graduation mineure.
   */
  @Override
  public boolean isMajorTick() {
    return true;
  }

  @Override
  public double getValueFromPosition(final double _v) {
    return _v;
  }

  @Override
  public double getPositionFromValue(final double _v) {
    return _v;
  }

  /**
   * Passe � la graduation suivante.
   */
  @Override
  public void next() {
    iterationDone_++;
    idx++;
  }

  /**
   * Passe directement � la graduation majeure suivante.
   */
  @Override
  public void nextMajor() {
    next();
  }

  /**
   * Replace l'it�rateur sur la premi�re graduation.
   */
  @Override
  public void rewind() {
    initCounters();
  }

  public void initCounters() {
    idx = Arrays.binarySearch(values, minimum_);
    if (idx < 0) {
      idx = -idx - 1;
    }

    iterationDone_ = 0;
  }
}