/*
 * Geotools 2 - OpenSource mapping toolkit
 * (C) 2003, Geotools Project Managment Committee (PMC)
 * (C) 2000, Institut de Recherche pour le D�veloppement
 * (C) 1999, P�ches et Oc�ans Canada
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Contacts:
 *     UNITED KINGDOM: James Macgill
 *             mailto:j.macgill@geog.leeds.ac.uk
 *
 *     FRANCE: Surveillance de l'Environnement Assist�e par Satellite
 *             Institut de Recherche pour le D�veloppement / US-Espace
 *             mailto:seasnet@teledetection.fr
 *
 *     CANADA: Observatoire du Saint-Laurent
 *             Institut Maurice-Lamontagne
 *             mailto:osl@osl.gc.ca
 */
package org.fudaa.ctulu.iterator;

// Dependencies

/**
 * It�rateur balayant les barres et �tiquettes de graduation d'un axe logarithmique. Cet it�rateur retourne les
 * positions des graduations � partir de la valeur minimale jusqu'� la valeur maximale.
 * 
 * @version $Id: LogarithmicNumberIterator.java,v 1.1 2007-01-17 10:45:26 deniger Exp $
 * @author Martin Desruisseaux
 */
public final class LogarithmicNumberIterator extends NumberIterator {

  /**
   * Scale and offset factors for {@link #currentPosition}.
   */
  private double scale_;
  private double offset_;

  /**
   * Construit un it�rateur par d�faut. La m�thode {@link #init} <u>doit</u> �tre appel�e avant que cet it�rateur ne
   * soit utilisable.
   */
  public LogarithmicNumberIterator() {
    super();
  }

  /**
   * Initialise l'it�rateur.
   * 
   * @param _minimum Valeur minimale de la premi�re graduation.
   * @param _maximum Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur.
   * @param _visualLength Longueur visuelle de l'axe sur laquelle tracer la graduation. Cette longueur doit �tre
   *          exprim�e en pixels ou en points.
   * @param _visualTickSpacing Espace � laisser visuellement entre deux marques de graduation. Cet espace doit �tre
   *          exprim� en pixels ou en points (1/72 de pouce).
   */
  @Override
  protected void init(final double _minimum, final double _maximum, final float _visualLength,
      final float _visualTickSpacing) {
    double mini = _minimum;
    if (_minimum == 0) {
      mini = 1E-15;
    }
    final double logMin = XMath.log10(mini);
    final double logMax = XMath.log10(_maximum);
    super.init(logMin, logMax, _visualLength, _visualTickSpacing);
    scale_ = (_maximum - mini) / (logMax - logMin);
    offset_ = mini - scale_ * logMin;
  }

  @Override
  public String formatSubValue(final double _v) {
    if (!isValid()) {
      currentLabel();
    }
    if (subFormat_ == null) {
      buildSubFormat();
    }
    int log10 = (int) Math.floor(XMath.log10(_v));
    if (log10 < 0) {
      log10 = -log10 + 1;
      final int oldr = subFormat_.getMaximumFractionDigits();
      if (oldr < log10) {
        subFormat_.setMaximumFractionDigits(log10);
        final String r = subFormat_.format(_v);
        subFormat_.setMaximumFractionDigits(oldr);
        return r;
      }
    }
    return super.formatValue(_v);
  }

  @Override
  public String formatValue(final double _v) {
    if (!isValid()) {
      currentLabel();
    }
    int log10 = (int) Math.floor(XMath.log10(_v));
    if (log10 < 0) {
      log10 = -log10;
      final int oldr = format_.getMaximumFractionDigits();
      if (oldr < log10) {
        format_.setMaximumFractionDigits(log10);
        final String r = format_.format(_v);
        format_.setMaximumFractionDigits(oldr);
        return r;
      }
    }
    return super.formatValue(_v);
  }

  /**
   * Returns the position where to draw the current tick. The position is scaled from the graduation's minimum to
   * maximum.
   */
  @Override
  public double currentPosition() {
    return super.currentPosition() * scale_ + offset_;
  }

  @Override
  public double getValueFromPosition(final double _v) {
    if (scale_ == 0) { return _v; }
    return XMath.pow10((_v - offset_) / scale_);

  }

  @Override
  public double getPositionFromValue(final double _v) {
    return XMath.log10(_v) * scale_ + offset_;
  }

  /**
   * Retourne la valeur de la graduation courante. Cette m�thode peut �tre appel�e pour une graduation majeure ou
   * mineure.
   */
  @Override
  public double currentValue() {
    return XMath.pow10(super.currentValue());
  }
}
