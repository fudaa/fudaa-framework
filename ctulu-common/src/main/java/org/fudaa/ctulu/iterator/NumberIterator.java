/*
 * Geotools 2 - OpenSource mapping toolkit
 * (C) 2003, Geotools Project Managment Committee (PMC)
 * (C) 2000, Institut de Recherche pour le D�veloppement
 * (C) 1999, P�ches et Oc�ans Canada
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 2.1 of the License, or (at your option) any later version.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Contacts:
 *     UNITED KINGDOM: James Macgill
 *             mailto:j.macgill@geog.leeds.ac.uk
 *     FRANCE: Surveillance de l'Environnement Assist�e par Satellite
 *             Institut de Recherche pour le D�veloppement / US-Espace
 *             mailto:seasnet@teledetection.fr
 *
 *     CANADA: Observatoire du Saint-Laurent
 *             Institut Maurice-Lamontagne
 *             mailto:osl@osl.gc.ca
 */
package org.fudaa.ctulu.iterator;

// Miscellaneous
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;


/**
 * It�rateur balayant les barres et �tiquettes de graduation d'un axe. Cet it�rateur retourne les positions des
 * graduations � partir de la valeur minimale jusqu'� la valeur maximale.
 * 
 * @version $Id: NumberIterator.java,v 1.1 2007-01-17 10:45:26 deniger Exp $
 * @author Martin Desruisseaux
 */
public class NumberIterator implements TickIterator {

  /**
   * @return the maxFractionDigits
   */
  public int getMaxFractionDigits() {
    return maxFractionDigits;
  }

  /**
   * @param _maxFractionDigits the maxFractionDigits to set
   */
  public void setMaxFractionDigits(int _maxFractionDigits) {
    maxFractionDigits = _maxFractionDigits;
  }

  /**
   * Petite quantit� utilis�e pour �viter les erreurs d'arrondissements dans les comparaisons.
   */
  private static final double EPS = 1E-10;
  /**
   * Pour eviter les loop infinie.
   */
  private static final int MAX_ITERATION = 200;

  /**
   * Indique si {@link #format_}est valide. Le format peut devenir invalide si {@link #init}a �t� appel�e. Dans ce cas,
   * il peut falloir changer le nombre de chiffres apr�s la virgule qu'il �crit.
   */
  private transient boolean formatValid_;

  /**
   * Intervalle entre deux graduations principales. Cette valeur est fix�e par {@link #init}.
   */
  double increment_;

  int iterationDone_;

  /**
   * Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur. Cette valeur est
   * fix�e par {@link #init}.
   */
  private double maximum_;

  /**
   * Valeur de la premi�re graduation principale. Cette valeur est fix�e par {@link #init}.
   */
  private double minimum_;

  /**
   * Nombre de sous-divisions dans une graduation principale. Cette valeur est fix�e par {@link #init}.
   */
  private int subTickCount_;

  /**
   * Index de la graduation secondaire en cours de tra�age. Cette valeur va de 0 inclusivement jusqu'�
   * {@link #subTickCount_}exclusivement. Elle sera modifi�e � chaque appel � {@link #next}.
   */
  private int subTickIndex_;

  /**
   * Index de la premi�re sous-graduation dans la premi�re graduation principale. Cette valeur est fix�e par
   * {@link #init}.
   */
  private int subTickStart_;

  /**
   * Index de la graduation principale en cours de tra�age. Cette valeur commence � 0 et sera modifi�e � chaque appel �
   * {@link #next}.
   */
  private int tickIndex_;

  /**
   * Valeur de la graduation principale ou secondaire actuelle. Cette valeur sera modifi�e � chaque appel �
   * {@link #next}.
   */
  private double value_;

  /**
   * Longueur de l'axe (en points). Cette information est conserv�e afin d'�viter de refaire toute la proc�dure
   * {@link #init}si les param�tres n'ont pas chang�s.
   */
  private float visualLength_;

  /**
   * Espace � laisser (en points) entre les graduations principales. Cette information est conserv�e afin d'�viter de
   * refaire toute la proc�dure {@link #init}si les param�tres n'ont pas chang�s.
   */
  private float visualTickSpacing_;

  /**
   * Format � utiliser pour �crire les �tiquettes de graduation. Ce format ne sera construit que la premi�re fois o�
   * {@link #currentLabel}sera appel�e.
   */
  protected transient NumberFormat format_;

  NumberFormat subFormat_;

  /**
   * Construit un it�rateur par d�faut. La m�thode {@link #init(double, double, float, float)}<u>doit </u> �tre appel�e
   * avant que cet it�rateur ne soit utilisable.
   */
  public NumberIterator() {
  // this.locale = locale;
  }

  public NumberIterator(final NumberIterator _it) {
    initFrom(_it);
    // this.locale = locale;
  }

  protected final void initFrom(final NumberIterator _it) {
    if (_it != null) {
      maximum_ = _it.maximum_;
      minimum_ = _it.minimum_;
      value_ = _it.value_;
      increment_ = _it.increment_;
      subTickCount_ = _it.subTickCount_;
      subTickIndex_ = _it.subTickIndex_;
      subTickStart_ = _it.subTickStart_;
      visualLength_ = _it.visualLength_;
      tickIndex_ = _it.tickIndex_;
      visualTickSpacing_ = _it.visualTickSpacing_;
      formatValid_ = _it.formatValid_;
      if (_it.format_ != null) {
        format_ = (NumberFormat) _it.format_.clone();
      }
    }
  }

  protected void buildSubFormat() {
    if (format_ == null) {
      subFormat_ = null;
    } else if (subFormat_ == null) {
      subFormat_ = (NumberFormat) format_.clone();
      updateSubFormat();
    }
  }

  private void updateSubFormat() {
    if (subFormat_ != null && format_ != null) {
      subFormat_.setMaximumFractionDigits(format_.getMaximumFractionDigits() + 1);
      subFormat_.setMinimumFractionDigits(format_.getMinimumFractionDigits());
    }
  }

  protected void buildFormat(final boolean _sci) {
    if (format_ == null) {

      final DecimalFormat decFormat = _sci ? new DecimalFormat("0.###E0") : new DecimalFormat();
      final DecimalFormatSymbols sym = decFormat.getDecimalFormatSymbols();
      sym.setDecimalSeparator('.');
      decFormat.setDecimalFormatSymbols(sym);
      format_ = decFormat;

    }
  }

  /**
   * Initialise l'it�rateur.
   * 
   * @param _minimum Valeur minimale de la premi�re graduation.
   * @param _maximum Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur.
   * @param _visualLength Longueur visuelle de l'axe sur laquelle tracer la graduation. Cette longueur doit �tre
   *          exprim�e en pixels ou en points.
   * @param _visualTickSpacing Espace � laisser visuellement entre deux marques de graduation. Cet espace doit �tre
   *          exprim� en pixels ou en points (1/72 de pouce).
   */
  protected void init(final double _minimum, final double _maximum, final float _visualLength,
      final float _visualTickSpacing) {
    if (_minimum == this.minimum_ && _maximum == this.maximum_ && _visualLength == this.visualLength_
        && _visualTickSpacing == this.visualTickSpacing_) {
      rewind();
      return;
    }
    AssertValues.ensureFinite("minimum", _minimum);
    AssertValues.ensureFinite("maximum", _maximum);
    AssertValues.ensureFinite("visualLength", _visualLength); // May be 0.
    AssertValues.ensureNonNull("visualTickSpacing", _visualTickSpacing);
    this.visualLength_ = _visualLength;
    this.visualTickSpacing_ = _visualTickSpacing;
    /*
     * Estime le pas qui donnera au moins l'espacement sp�cifi� entre chaque graduation. D�termine ensuite si ce pas est
     * de l'ordre des dizaines, centaines ou autre et on ram�nera temporairement ce pas � l'ordre des unit�es.
     */
    double incrementTmp = (_maximum - _minimum) * (_visualTickSpacing / _visualLength);
    final double factor = XMath.pow10((int) Math.floor(XMath.log10(incrementTmp)));
    incrementTmp /= factor;
    if (Double.isNaN(incrementTmp) || Double.isInfinite(incrementTmp) || incrementTmp == 0) {
      this.minimum_ = _minimum;
      this.maximum_ = _maximum;
      this.increment_ = Double.NaN;
      this.value_ = Double.NaN;
      this.tickIndex_ = 0;
      this.subTickIndex_ = 0;
      this.subTickStart_ = 0;
      this.subTickCount_ = 1;
      this.formatValid_ = false;
      return;
    }
    /*
     * Le pas se trouve maintenant entre 1 et 10. On l'ajuste maintenant pour lui donner des valeurs qui ne sont
     * habituellement pas trop difficiles � lire.
     */
    final int subTickCountTmp;
    if (incrementTmp <= 1.0) {
      incrementTmp = 1.0;
      subTickCountTmp = 5;
    } else if (incrementTmp <= 2.0) {
      incrementTmp = 2.0;
      subTickCountTmp = 4;
    } else if (incrementTmp <= 2.5) {
      incrementTmp = 2.5;
      subTickCountTmp = 5;
    } else if (incrementTmp <= 4.0) {
      incrementTmp = 4.0;
      subTickCountTmp = 4;
    } else if (incrementTmp <= 5.0) {
      incrementTmp = 5.0;
      subTickCountTmp = 5;
    } else {
      incrementTmp = 10.0;
      subTickCountTmp = 5;
    }
    incrementTmp = incrementTmp * factor;
    /*
     * Arrondie maintenant le minimum sur une des graduations principales. D�termine ensuite combien de graduations
     * secondaires il faut sauter sur la premi�re graduation principale.
     */
    final double tmp = _minimum;
    double minimumAxe = Math.floor(_minimum / incrementTmp + EPS) * incrementTmp;
    int subTickStartTmp = (int) Math.ceil((tmp - minimumAxe - EPS) * (subTickCountTmp / incrementTmp));
    final int extra = subTickStartTmp / subTickCountTmp;
    minimumAxe += extra * incrementTmp;
    subTickStartTmp -= extra * subTickCountTmp;

    this.increment_ = incrementTmp;
    this.subTickCount_ = subTickCountTmp;
    this.maximum_ = _maximum + Math.abs(_maximum * EPS);
    this.minimum_ = minimumAxe;
    this.subTickStart_ = subTickStartTmp;
    this.subTickIndex_ = subTickStartTmp;
    this.tickIndex_ = 0;
    this.value_ = this.minimum_ + incrementTmp * (subTickStartTmp / (double) subTickCountTmp);
    this.formatValid_ = false;
    iterationDone_ = 0;
  }

  @Override
  public void initExact(double _min, double _max, int _nbIteration, int _nbSousIteration) {
    initExactCommon(_min, _max, _nbSousIteration);
    increment_ = (maximum_ - minimum_) / Math.max(_nbIteration, 1);
  }

  private void initExactCommon(double _min, double _max, int _nbSousIteration) {
    minimum_ = Math.min(_min, _max);
    maximum_ = Math.max(_min, _max);
    subTickCount_ = Math.max(0, _nbSousIteration);
    tickIndex_ = 0;
    value_ = _min;
    this.subTickStart_ = 0;
    this.subTickIndex_ = 0;
    this.formatValid_ = false;
    iterationDone_ = 0;
  }

  @Override
  public void initExactFromDist(double _min, double _max, double _increment, int _nbSousIteration) {
    initExactCommon(_min, _max, _nbSousIteration);
    increment_ = _increment;

  }

  @Override
  public final Object clone() throws CloneNotSupportedException {
    final NumberIterator res = (NumberIterator) super.clone();
    res.initFrom(this);
    return res;
  }

  /**
   * Retourne l'�tiquette de la graduation courante. On n'appele g�n�ralement cette m�thode que pour les graduations
   * majeures, mais elle peut aussi �tre appel�e pour les graduations mineures. Cette m�thode retourne <code>null</code>
   * s'il n'y a pas d'�tiquette pour la graduation courante.
   */
  @Override
  public String currentLabel() {
    if (!formatValid_) {
      buildFormat(false);
      /*
       * Trouve le nombre de chiffres apr�s la virgule n�cessaires pour repr�senter les �tiquettes de la graduation.
       * Impose une limite de six chiffres, limite qui pourrait �tre atteinte notamment avec les nombres p�riodiques
       * (par exemple des intervalles de temps exprim�s en fractions de jours).
       */
      int precision;
      double step = Math.abs(increment_);

      boolean sci = false;
      if (minimum_ < 1E-3 && minimum_ > -1E-3 && step <= 1E-3 && !format_.isParseIntegerOnly()) {
        sci = true;
        step = step * XMath.pow10(-XMath.log10(step));
        // format=new Decima
      }
      for (precision = 0; precision < 10; precision++) {
        final double check = Math.rint(step * 1E+4) % 1E+4;
        if (!(check > step * EPS)) { // 'step' may be NaN
          break;
        }
        step *= 10;
      }
      if (sci) {
        ((DecimalFormat) format_).applyPattern("0.###E0");
        precision++;
      } else if (!format_.isParseIntegerOnly()) {
        ((DecimalFormat) format_).applyPattern("0.00");
      }
      if (precision == 0 && step < 1E-4) {
        format_.setMinimumFractionDigits(0);
        format_.setMaximumFractionDigits(10);
        String s = format_.format(step);
        while (s.endsWith("0")) {
          s = s.substring(0, s.length() - 1);
        }
        final int i = s.indexOf('.');
        if (i >= 0) {
          precision = s.length() - i;
          format_.setMinimumFractionDigits(precision);
          format_.setMaximumFractionDigits(precision);
        } else {
          format_.setMinimumFractionDigits(0);
          format_.setMaximumFractionDigits(30);
        }
      } else {
        precision = computePrecision(precision);
        format_.setMinimumFractionDigits(precision);
        format_.setMaximumFractionDigits(precision);
      }
      updateSubFormat();
      formatValid_ = true;
    }
    return formatValue(currentValue());
  }

  int maxFractionDigits = -1;

  protected int computePrecision(final int _i) {
    return maxFractionDigits >= 0 ? Math.min(maxFractionDigits, _i) : _i;
  }

  /**
   * Returns the position where to draw the current tick. The position is scaled from the graduation's minimum to
   * maximum. This is usually the same number than {@link #currentValue}. The mean exception is for logarithmic
   * graduation, in which the tick position is not proportional to the tick value.
   */
  @Override
  public double currentPosition() {
    return value_;
  }

  /**
   * Retourne la valeur de la graduation courante. Cette m�thode peut �tre appel�e pour une graduation majeure ou
   * mineure.
   */
  @Override
  public double currentValue() {
    return value_;
  }

  @Override
  public String formatSubValue(final double _v) {
    if (!formatValid_) {
      currentLabel();
    }
    if (subFormat_ == null) {
      buildSubFormat();
    }
    if (subFormat_ == null) { return Double.toString(_v); }
    return subFormat_.format(_v);
  }

  @Override
  public String formatValue(final double _v) {
    if (!formatValid_) {
      currentLabel();
    }
    return format_.format(_v);
  }

  @Override
  public double getIncrement() {
    return increment_;
  }

  /**
   * Indique s'il reste des graduations � retourner. Cette m�thode retourne <code>true</code> tant que
   * {@link #currentValue}ou {@link #currentLabel}peuvent �tre appel�es.
   */
  @Override
  public boolean hasNext() {
    return iterationDone_ < MAX_ITERATION && value_ <= maximum_;
  }

  /**
   * Initialise l'it�rateur.
   * 
   * @param _minimum Valeur minimale de la premi�re graduation.
   * @param _maximum Valeur limite des graduations. La derni�re graduation n'aura pas n�cessairement cette valeur.
   * @param _nbTick le nombre de graduation
   */
  @Override
  public void init(final double _minimum, final double _maximum, final int _nbTick) {
    init(_minimum, _maximum, _nbTick <= 1 ? DEFAULT_STICK_NUMBER : _nbTick, 1);
  }

  /**
   * Indique si la graduation courante est une graduation majeure.
   * 
   * @return <code>true</code> si la graduation courante est une graduation majeure, ou <code>false</code> si elle est
   *         une graduation mineure.
   */
  @Override
  public boolean isMajorTick() {
    return subTickIndex_ == 0;
  }

  @Override
  public double getValueFromPosition(final double _v) {
    return _v;
  }

  /**
   * Conventions � utiliser pour le formatage des nombres.
   */
  /*
   * private Locale locale;
   */

  public final boolean isValid() {
    return formatValid_;
  }

  @Override
  public double getPositionFromValue(final double _v) {
    return _v;
  }

  /**
   * Passe � la graduation suivante.
   */
  @Override
  public void next() {
    if (++subTickIndex_ >= subTickCount_) {
      subTickIndex_ = 0;
      iterationDone_++;
      tickIndex_++;
    }
    // On n'utilise pas "+=" afin d'�viter les erreurs d'arrondis
    value_ = minimum_ + increment_ * (tickIndex_ + subTickIndex_ / (double) subTickCount_);
  }

  /**
   * Passe directement � la graduation majeure suivante.
   */
  @Override
  public void nextMajor() {
    subTickIndex_ = 0;
    iterationDone_++;
    value_ = minimum_ + increment_ * (++tickIndex_);
  }

  /**
   * Replace l'it�rateur sur la premi�re graduation.
   */
  @Override
  public void rewind() {
    iterationDone_ = 0;
    tickIndex_ = 0;
    subTickIndex_ = subTickStart_;
    value_ = minimum_ + increment_ * (subTickStart_ / (double) subTickCount_);
  }

  public final void setValid(final boolean _formatValid) {
    formatValid_ = _formatValid;
  }

  /**
   * Retourne les conventions � utiliser pour �crire les �tiquettes de graduation.
   */
  /*
   * public final Locale getLocale(){ return locale; }
   */

  /**
   * Modifie les conventions � utiliser pour �crire les �tiquettes de graduation.
   */
  /*
   * public final void setLocale(final Locale locale){ if (!locale.equals(this.locale)) { this.locale = locale;
   * this.format = null; formatValid = false; } }
   */
}