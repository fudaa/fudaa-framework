/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2006-07-13 13:34:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLog;

/**
 * @author fred deniger
 * @version $Id: ProgressionFuAdapter.java,v 1.3 2006-07-13 13:34:37 deniger Exp $
 */
public class ProgressionFuAdapter implements ProgressionInterface {

  String pref_ = CtuluLibString.EMPTY_STRING;

  public ProgressionFuAdapter() {

  }

  /**
   * @param _pref
   */
  public ProgressionFuAdapter(final String _pref) {
    pref_ = _pref + ": ";
  }

  @Override
  public void reset(){}

  @Override
  public void setDesc(final String _s){
    FuLog.trace(pref_ + " desc -> " + _s);

  }

  @Override
  public void setProgression(final int _v){
    FuLog.trace(pref_ + "prog ->" + _v);

  }

}
