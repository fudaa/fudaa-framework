/*
 * @creation 16 sept. 2005
 * 
 * @modification $Date: 2007-02-02 11:20:09 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuResource;

/**
 * @author Fred Deniger
 * @version $Id: CtuluUIAbstract.java,v 1.9 2007-02-02 11:20:09 deniger Exp $
 */
public abstract class CtuluUIAbstract implements CtuluUI {

  protected String errorString_;
  protected String infoString_;
  protected String warnString_;
  protected String questionString_;

  public CtuluUIAbstract() {
  }

  public static void showAnalyzeWarnAndInfo(final CtuluAnalyze _analyze, final CtuluUI _ui) {
    if (_analyze == null) {
      return;
    }
    if (_analyze.isEmpty()) {
      return;
    }
    // _analyze.printResume();
    _ui.message(_analyze.getDesc(), _analyze.getResume(), false);

  }

  public static String getDefaultErrorTitle() {
    return BuResource.BU.getString("Erreur");
  }

  public static String getDefaultInfoTitle() {
    return BuResource.BU.getString("Information");
  }

  public static String getDefaultWarnTitle() {
    return BuResource.BU.getString("Avertissement");
  }

  public static String getDefaultQuestionTitle() {
    return BuResource.BU.getString("Confirmation");
  }

  protected String getErrorTitle(final String _init) {
    if (_init != null) {
      return _init;
    }
    if (errorString_ == null) {
      errorString_ = getDefaultErrorTitle();
    }
    return errorString_;
  }

  protected String getInfoTitle(final String _init) {
    if (_init != null) {
      return _init;
    }
    if (infoString_ == null) {
      infoString_ = getDefaultInfoTitle();
    }
    return infoString_;
  }

  protected String getWarnTitle(final String _init) {
    if (_init != null) {
      return _init;
    }
    if (warnString_ == null) {
      warnString_ = getDefaultWarnTitle();
    }
    return warnString_;
  }

  protected String getQuestionTitle(final String _init) {
    if (_init != null) {
      return _init;
    }
    if (questionString_ == null) {
      questionString_ = getDefaultQuestionTitle();
    }
    return questionString_;
  }

  @Override
  public CtuluTaskDelegate createTask(final String _name) {
    return new CtuluTaskOperationDefault(_name);
  }

  @Override
  public void error(final String _msg) {
    error(CtuluUIAbstract.getDefaultErrorTitle(), _msg, false);
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(final CtuluAnalyze _analyze) {
    if (_analyze == null) {
      return false;
    }
    if (_analyze.containsFatalError() || _analyze.containsErrors()) {
      error(_analyze.getDesc(), _analyze.getResume(), false);
    }
    return _analyze.containsFatalError();
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(final CtuluLog _analyze) {
    if (_analyze == null) {
      return false;
    }
    if (_analyze.containsErrorOrSevereError()) {
      error(_analyze.getDesc(), _analyze.getResume(), false);
    }
    return _analyze.containsSevereError();
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(final CtuluIOOperationSynthese _opResult) {
    if (_opResult == null) {
      return false;
    }
    return manageAnalyzeAndIsFatal(_opResult.getAnalyze());
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(final CtuluIOResult _opResult) {
    if (_opResult == null) {
      return false;
    }
    return manageAnalyzeAndIsFatal(_opResult.getAnalyze());
  }
}
