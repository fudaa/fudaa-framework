/**
 *  @creation     28 f�vr. 2005
 *  @modification $Date: 2007-05-21 10:28:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.fu.FuLib;
import java.text.ParsePosition;
import java.util.StringTokenizer;

/**
 * @author Fred Deniger
 * @version $Id: CtuluDurationFormatter.java,v 1.14 2007-05-21 10:28:29 deniger Exp $
 */
public class CtuluDurationFormatter extends CtuluNumberFormat {

  private static final String FIELDS_ABBREV_DAY = CtuluLib.getS("j[duration-abbr]");

  private static final String FIELDS_ABBREV_HOUR = CtuluLib.getS("h[duration-abbr]");

  private static final String FIELDS_ABBREV_MIN = CtuluLib.getS("min[duration-abbr]");

  private static final String FIELDS_ABBREV_MONTH = CtuluLib.getS("m[duration-abbr]");

  private static final String FIELDS_ABBREV_SEC = CtuluLib.getS("s[duration-abbr]");
  private static final String FIELDS_ABBREV_YEAR = CtuluLib.getS("a[duration-abbr]");
  private static final String[] FIELDS_NAME = new String[] { CtuluLib.getS("Ann�e"), CtuluLib.getS("Mois"),
      CtuluLib.getS("Jour"), CtuluLib.getS("Heure"), CtuluLib.getS("Minute"), CtuluLib.getS("Seconde") };

  public final static CtuluNumberFormatI DEFAULT_TIME = CtuluNumberFormatDefault.buildNoneFormatter(1, false);

  public static String getDesc() {
    return CtuluLib.getS("Une date est attendue");
  }

  /**
   * @param _str un chaine du style "3a 3m 14min 1s" ou "3a 3m 0:14:1"
   * @return equivalent en seconde
   */
  public static int getSec(final String _str) throws NumberFormatException {
    try {
      return (int) Double.parseDouble(_str);
    } catch (final NumberFormatException e) {
      // le test bateau a echoue
    }
    if (_str == null) return 0;
    String dur = _str.trim();
    if (dur.length() == 0) return 0;
    boolean neg = dur.charAt(0) == '-';
    if (neg) {
      if (dur.length() == 0) return 0;
      dur = dur.substring(1);
    }
    final String minSep = "n";
    dur = FuLib.replace(dur, FIELDS_ABBREV_MIN, minSep);
    final StringTokenizer tok = new StringTokenizer(dur, FIELDS_ABBREV_DAY + FIELDS_ABBREV_HOUR + minSep
        + FIELDS_ABBREV_SEC + FIELDS_ABBREV_YEAR + FIELDS_ABBREV_MONTH + CtuluLibString.ESPACE, true);
    double r = 0;
    boolean secDone = false;
    boolean minDone = false;
    boolean hDone = false;
    boolean jDone = false;
    boolean monthDone = false;
    boolean yearDone = false;
    while (tok.hasMoreTokens()) {
      final String nexttok = tok.nextToken().trim();
      if (nexttok.length() == 0) {
        continue;
      }
      if (nexttok.indexOf(':') > 0) {
        if (secDone || minDone || hDone) { throw new NumberFormatException(_str + " h, min,s alreadydone"); }
        secDone = true;
        minDone = true;
        hDone = true;
        final StringTokenizer hmsTok = new StringTokenizer(nexttok, ":");
        if (hmsTok.countTokens() == 3) {
          r += 3600 * Double.parseDouble(hmsTok.nextToken());
          r += 60 * Double.parseDouble(hmsTok.nextToken());
          r += Double.parseDouble(hmsTok.nextToken());

        } else {
          throw new NumberFormatException(_str + hmsTok.countTokens());
        }
      } else {
        double val = 0;
        try {
          val = Double.parseDouble(nexttok);
        } catch (final NumberFormatException e1) {
          throw new NumberFormatException(_str + nexttok);
        }
        int multiple = 1;
        if (tok.hasMoreTokens()) {
          final String abbr = tok.nextToken();
          if (abbr.equals(FIELDS_ABBREV_SEC)) {
            if (secDone) { throw new NumberFormatException(_str + " sec alreadydone"); }
            secDone = true;
            multiple = 1;
          } else if (abbr.equals(minSep)) {
            multiple = 60;
            if (minDone) { throw new NumberFormatException(_str + " min alreadydone"); }
            minDone = true;
          } else if (abbr.equals(FIELDS_ABBREV_HOUR)) {
            if (hDone) { throw new NumberFormatException(_str + " hour alreadydone"); }
            multiple = 3600;
          } else if (abbr.equals(FIELDS_ABBREV_DAY)) {
            if (jDone) { throw new NumberFormatException(_str + " day alreadydone"); }
            jDone = true;
            multiple = 3600 * 24;
          } else if (abbr.equals(FIELDS_ABBREV_MONTH)) {
            if (monthDone) { throw new NumberFormatException(_str + " month alreadydone"); }
            monthDone = true;
            multiple = 3600 * 24 * 30;
          } else if (abbr.equals(FIELDS_ABBREV_YEAR)) {
            if (yearDone) { throw new NumberFormatException(_str + " year alreadydone"); }
            yearDone = true;
            multiple = 3600 * 24 * 365;
          } else {
            throw new NumberFormatException(_str);
          }
        }
        r += multiple * val;
      }
    }
    return (int) (neg ? -r : r);
  }

  public static CtuluDurationFormatter restoreFromLocalizedPattern(String _s) {
    
    String prefix = CtuluNumberFormatBuilder.getPrefixForDate();
    if (_s == null || !_s.startsWith(prefix)) return null;
    String[] data = CtuluLibString.parseString(_s.substring(prefix.length()), "|");
    if (data == null || data.length < 3) return new CtuluDurationFormatter();
    boolean monthDay = "USE_MONTH_DAY_YES".equals(data[0]);
    boolean hms = "USE_HMS_YES".equals(data[1]);
    boolean useZero = "USE_ZERO_YES".equals(data[2]);
    CtuluDurationFormatter res = new CtuluDurationFormatter(hms, monthDay);
    res.setAlwaysDisplayZero(useZero);
    return res;
  }

  private boolean hms_;

  private boolean useMonthYear_;

  boolean alwaysDisplayZero_ = true;

  String description_;

  public CtuluDurationFormatter() {}

  /**
   * @param _hms true si on utilise le format hh:mm:ss pour l'heure
   * @param _useMonthDay true si on utilise les mois, ann�e. Sinon, que les jours.
   */
  public CtuluDurationFormatter(final boolean _hms, final boolean _useMonthDay) {
    hms_ = _hms;
    useMonthYear_ = _useMonthDay;
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) { return true; }
    if (_obj != null) {
      if (_obj.getClass().equals(getClass())) {
        final CtuluDurationFormatter fmt = (CtuluDurationFormatter) _obj;
        return fmt.hms_ == hms_ && fmt.useMonthYear_ == useMonthYear_;
      }
    }
    return false;
  }

  @Override
  public String format(final double _d) {
    final StringBuffer buf = new StringBuffer();
    format(_d, buf);
    return buf.toString();
  }

  @Override
  public void format(final double _timeInSec, final StringBuffer _buf) {
    boolean moins = false;
    int time = (int) _timeInSec;
    if (time < 0) {
      time = -time;
      moins = true;
    }
    final int s = time % 60;
    time /= 60;
    final int m = time % 60;
    time /= 60;
    final int h = time % 24;
    time /= 24;

    int j = time;
    int n = 0;
    int a = 0;
    if (useMonthYear_) {
      a = time / 365;
      j = time % 365;
      n = j / 30;
      j = j % 30;
    }
    boolean add = false;

    if (moins) {
      _buf.append("- ");
      add = true;
    }
    boolean isFill = false;
    if (useMonthYear_ && (a > 0 || alwaysDisplayZero_)) {
      isFill = true;
      _buf.append(a).append(FIELDS_ABBREV_YEAR);
      add = true;
    }
    boolean ifZero = isFill && (hms_ || j > 0 || h > 0 || m > 0 || s > 0);
    if (useMonthYear_ && (n > 0 || ifZero || alwaysDisplayZero_)) {
      if (isFill) {
        _buf.append(CtuluLibString.ESPACE);
      }
      isFill = true;
      _buf.append(n).append(FIELDS_ABBREV_MONTH);
      add = true;
    }
    ifZero = isFill && (hms_ || h > 0 || m > 0 || s > 0);
    if (j > 0 || ifZero || alwaysDisplayZero_) {
      if (isFill) {
        _buf.append(CtuluLibString.ESPACE);
      }
      isFill = true;
      _buf.append(j).append(FIELDS_ABBREV_DAY);
      add = true;
    }
    if (hms_) {
      if (isFill) {
        _buf.append(CtuluLibString.ESPACE);
      }
      _buf.append(h);
      _buf.append(':');
      if (m < 10) {
        _buf.append(CtuluLibString.ZERO);
      }
      _buf.append(m);
      _buf.append(':');
      if (s < 10) {
        _buf.append(CtuluLibString.ZERO);
      }
      _buf.append(s);
      isFill = true;
      add = true;
    } else {
      ifZero = isFill && (m > 0 || s > 0);
      if (h > 0 || ifZero || alwaysDisplayZero_) {
        if (isFill) {
          _buf.append(CtuluLibString.ESPACE);
        }
        isFill = true;
        _buf.append(h).append(FIELDS_ABBREV_HOUR);
        add = true;
      }
      ifZero = isFill && s > 0;
      if (m > 0 || ifZero || alwaysDisplayZero_) {
        if (isFill) {
          _buf.append(CtuluLibString.ESPACE);
        }
        isFill = true;
        _buf.append(m).append(FIELDS_ABBREV_MIN);
        add = true;
      }
      if (s > 0 || alwaysDisplayZero_) {
        if (isFill) {
          _buf.append(CtuluLibString.ESPACE);
        }
        _buf.append(s).append(FIELDS_ABBREV_SEC);
        add = true;
      }
    }
    if (!add) {
      _buf.append("0s");
    }
  }

  @Override
  public CtuluNumberFormatI getCopy() {
    CtuluDurationFormatter res = new CtuluDurationFormatter(isHms(), isUseMonthYear());
    res.setAlwaysDisplayZero(isAlwaysDisplayZero());
    return res;
  }

  /**
   * @return la description du format
   */
  public String getDescription() {
    if (description_ == null) {
      final StringBuffer r = new StringBuffer();
      final int min = useMonthYear_ ? 0 : 2;
      final int max = FIELDS_NAME.length;
      for (int i = min; i < max; i++) {
        if (r.length() > 0) {
          r.append(',');
        }
        r.append(FIELDS_NAME[i]);
      }
      if (hms_) {
        r.append(" (").append(CtuluLib.getS("hh:mm:ss")).append(")");
      }
      description_ = r.toString();
    }
    return description_;
  }

  @Override
  public int hashCode() {
    return (hms_ ? 0 : 37) + (useMonthYear_ ? 1 : 0);
  }

  public final boolean isAlwaysDisplayZero() {
    return alwaysDisplayZero_;
  }

  /**
   * @return true si le format hh:mm:ss
   */
  public final boolean isHms() {
    return hms_;
  }

  /**
   * @return true si les mois, ann�e sont utilis�s.
   */
  public final boolean isUseMonthYear() {
    return useMonthYear_;
  }

  @Override
  public Object parseObject(final String _source, final ParsePosition _pos) {
    throw new IllegalAccessError("not yet implemented");
  }

  public final void setAlwaysDisplayZero(final boolean _alwaysDisplayZero) {
    alwaysDisplayZero_ = _alwaysDisplayZero;
  }

  @Override
  public boolean isDecimal() {
    return false;
  }

  /**
   * @param _s true si heure sous la forme hh:mm:ss
   */
  public final void setHms(final boolean _s) {
    if (_s != hms_) {
      hms_ = _s;
      description_ = null;
    }
  }

  /**
   * @param _useMonthYear true si utilise le annee, mois
   */
  public final void setUseMonthYear(final boolean _useMonthYear) {
    if (useMonthYear_ != _useMonthYear) {
      useMonthYear_ = _useMonthYear;
      description_ = null;
    }
  }

  @Override
  public String toLocalizedPattern() {
    String res = CtuluNumberFormatBuilder.getPrefixForDate();
    if (useMonthYear_) res += "USE_MONTH_DAY_YES";
    else res += "USE_MONTH_DAY_NO";
    res += "|";
    if (hms_) {
      res += "USE_HMS_YES";
    } else res += "USE_HMS_NO";
    res += "|";
    if (isAlwaysDisplayZero()) {
      res += "USE_ZERO_YES";
    } else {
      res += "USE_ZERO_NO";
    }
    return res;
  }

  @Override
  public String toString() {
    return getDescription();
  }

}