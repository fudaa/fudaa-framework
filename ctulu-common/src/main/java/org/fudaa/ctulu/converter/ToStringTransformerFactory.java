/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class ToStringTransformerFactory {

  public static Map<Class, AbstractPropertyToStringTransformer> createTransformers() {
    Map<Class, AbstractPropertyToStringTransformer> res = new HashMap<Class, AbstractPropertyToStringTransformer>();
    res.put(String.class, new StringToStringTransformer());
    res.put(Color.class, new ColorToStringTransformer());
    res.put(Font.class, new FontToStringTransformer());
    DoubleToStringTransformer doubleTransformer = new DoubleToStringTransformer();
    res.put(Double.TYPE, doubleTransformer);
    res.put(Double.class, doubleTransformer);
    FloatToStringTransformer floatTransformer = new FloatToStringTransformer();
    res.put(Float.TYPE, floatTransformer);
    res.put(Float.class, floatTransformer);
    IntegerToStringTransformer integerTransformer = new IntegerToStringTransformer();
    res.put(Integer.TYPE, integerTransformer);
    res.put(Integer.class, integerTransformer);
    BooleanToStringTransformer booleanTransformer = new BooleanToStringTransformer();
    res.put(Boolean.TYPE, booleanTransformer);
    res.put(Boolean.class, booleanTransformer);
    res.put(Point2D.Double.class, new Point2DDoubleToStringTransformer());
    res.put(Point2D.Double[].class, new Point2DDoubleArrayToStringTransformer());
    return res;
  }

  public static AbstractPropertyToStringTransformer<Point2D.Double[]> getPoint2dArrayTransformer() {
    return ToStringTransformerFactory.createTransformers().get(Point2D.Double[].class);
  }
  
}
