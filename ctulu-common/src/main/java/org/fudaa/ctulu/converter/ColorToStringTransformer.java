/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import com.memoire.fu.FuLib;
import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class ColorToStringTransformer extends AbstractPropertyToStringTransformer<Color> {

  public ColorToStringTransformer() {
    super(Color.class);
  }

  @Override
  public Color fromStringSafe(String in) {
    String[] split = FuLib.split(in, ',');
    if (split.length != 3) {
      return null;
    }
    try {
      int r = Integer.parseInt(split[0]);
      int g = Integer.parseInt(split[1]);
      int b = Integer.parseInt(split[2]);

      final Color color = new Color(r, g, b);
      return color;
    } catch (Exception exception) {
      Logger.getLogger(ColorToStringTransformer.class.getName()).log(Level.INFO, "message {0}", exception);
    }
    return null;
  }

  @Override
  public String toStringSafe(Color in) {
    return in.getRed() + "," + in.getGreen() + "," + in.getBlue();
  }

  @Override
  public boolean isValidSafe(String in) {
    return in == null || NULL_STRING.equals(in) || fromString(in) != null;
  }
}
