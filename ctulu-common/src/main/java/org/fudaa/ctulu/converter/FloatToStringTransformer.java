/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class FloatToStringTransformer extends AbstractPropertyToStringTransformer<Float> {
  public FloatToStringTransformer() {
    super(Float.class);
  }
  
  @Override
  public Float fromStringSafe(String in) {
    try {
      return Float.parseFloat(in);
    } catch (NumberFormatException numberFormatException) {
      Logger.getLogger(FloatToStringTransformer.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
    }
    return null;
  }

  @Override
  public boolean isValidSafe(String in) {
    try {
      Float.parseFloat(in);
      return true;
    } catch (NumberFormatException numberFormatException) {
    }
    return false;
  }

  @Override
  public String toStringSafe(Float in) {
    return in.toString();
  }
  
}
