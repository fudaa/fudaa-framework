/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Frederic Deniger
 */
public class PropertyToStringCacheDecorator<T> extends AbstractPropertyToStringTransformer<T> {

  private final AbstractPropertyToStringTransformer<T> decorated;
  Map<String, T> cache = new HashMap<String, T>();

  public PropertyToStringCacheDecorator(AbstractPropertyToStringTransformer<T> decorated) {
    super(decorated.getSupportedClass());
    this.decorated = decorated;
  }

  @Override
  public String toStringSafe(T in) {
    return decorated.toStringSafe(in);
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public T fromStringSafe(String in) {
    T value = cache.get(in);
    if (value != null) {
      return value;
    }
    value = decorated.fromStringSafe(in);
    cache.put(in, value);
    return value;
  }
}
