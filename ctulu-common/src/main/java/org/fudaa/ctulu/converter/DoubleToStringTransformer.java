/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class DoubleToStringTransformer extends AbstractPropertyToStringTransformer<Double> {

  public DoubleToStringTransformer() {
    super(Double.class);
  }

  @Override
  public Double fromStringSafe(String in) {
    try {
      return Double.parseDouble(in);
    } catch (NumberFormatException numberFormatException) {
      Logger.getLogger(DoubleToStringTransformer.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
    }
    return null;
  }

  @Override
  public boolean isValidSafe(String in) {
    try {
      Double.parseDouble(in);
      return true;
    } catch (NumberFormatException numberFormatException) {
    }
    return false;
  }

  @Override
  public String toStringSafe(Double in) {
    return in.toString();
  }
}
