/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import com.memoire.fu.FuLib;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class FontToStringTransformer extends AbstractPropertyToStringTransformer<Font> {

  public FontToStringTransformer() {
    super(Font.class);
  }

  @Override
  public Font fromStringSafe(String in) {
    String[] split = FuLib.split(in, ',');
    if (split.length != 3) {
      return null;
    }
    try {
      String family = split[0];
      int type = Integer.parseInt(split[1]);
      int size = Integer.parseInt(split[2]);
      final Font font = new Font(family, type, size);
      return font;
    } catch (Exception exception) {
      Logger.getLogger(FontToStringTransformer.class.getName()).log(Level.INFO, "message {0}", exception);
    }
    return null;
  }

  @Override
  public String toStringSafe(Font in) {
    return in.getFamily() + "," + in.getStyle() + "," + in.getSize();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }
}
