/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import com.memoire.fu.FuLib;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluRange;

/**
 *
 * @author Frederic Deniger
 */
public class CtuluRangeToStringTransformer extends AbstractPropertyToStringTransformer<CtuluRange> {

  public CtuluRangeToStringTransformer() {
    super(CtuluRange.class);
  }

  @Override
  public CtuluRange fromStringSafe(String in) {
    String[] split = FuLib.split(in, ' ');
    if (split != null && split.length == 2) {
      try {
        double x = Double.parseDouble(split[0]);
        double y = Double.parseDouble(split[1]);
        return new CtuluRange(x, y);
      } catch (NumberFormatException numberFormatException) {
        Logger.getLogger(CtuluRangeToStringTransformer.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
      }
    }
    return null;
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public String toStringSafe(CtuluRange in) {
    return in.getMin() + " " + in.getMax();
  }
}
