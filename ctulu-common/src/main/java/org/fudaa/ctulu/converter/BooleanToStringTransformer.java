/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class BooleanToStringTransformer extends AbstractPropertyToStringTransformer<Boolean> {

  public BooleanToStringTransformer() {
    super(Boolean.class);
  }

  @Override
  public Boolean fromStringSafe(String in) {
    try {
      return Boolean.valueOf(in);
    } catch (NumberFormatException numberFormatException) {
      Logger.getLogger(BooleanToStringTransformer.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
    }
    return null;
  }

  @Override
  public boolean isValidSafe(String in) {
    return "false".equals(in) || "true".equals(in);
  }

  @Override
  public String toStringSafe(Boolean in) {
    return in.toString();
  }
}
