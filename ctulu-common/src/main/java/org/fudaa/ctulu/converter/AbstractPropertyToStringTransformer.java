package org.fudaa.ctulu.converter;

/**
 *
 * @author deniger ( genesis)
 */
public abstract class AbstractPropertyToStringTransformer<T> {

  public static final String NULL_STRING = "null";
  private final Class supportedClass;

  public AbstractPropertyToStringTransformer(Class supportedClass) {
    this.supportedClass = supportedClass;
  }

  public Class getSupportedClass() {
    return supportedClass;
  }

  public String toString(T in) {
    if (in == null) {
      return "null";
    }
    return toStringSafe(in);
  }

  public abstract String toStringSafe(T in);

  public abstract boolean isValidSafe(String in);

  public boolean isValid(String in) {
    if (in == null) {
      return false;
    }
    return isValidSafe(in);
  }

  public T fromString(String in) {
    if (in == null || NULL_STRING.equals(in)) {
      return null;
    }
    return fromStringSafe(in);
  }

  public abstract T fromStringSafe(String in);
}
