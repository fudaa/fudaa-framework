/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class IntegerToStringTransformer extends AbstractPropertyToStringTransformer<Integer> {

  public IntegerToStringTransformer() {
    super(Integer.class);
  }
  
  @Override
  public Integer fromStringSafe(String in) {
    try {
      return Integer.parseInt(in);
    } catch (NumberFormatException numberFormatException) {
      Logger.getLogger(IntegerToStringTransformer.class.getName()).log(Level.INFO, "message {0}", numberFormatException);
    }
    return null;
  }

  @Override
  public boolean isValidSafe(String in) {
    try {
      Integer.parseInt(in);
      return true;
    } catch (NumberFormatException numberFormatException) {
    }
    return false;
  }

  @Override
  public String toStringSafe(Integer in) {
    return in.toString();
  }
  
}
