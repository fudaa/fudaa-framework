/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import com.memoire.fu.FuLib;
import java.awt.geom.Point2D;

/**
 *
 * @author Frederic Deniger
 */
public class Point2DDoubleArrayToStringTransformer extends AbstractPropertyToStringTransformer<Point2D.Double[]> {

  public Point2DDoubleArrayToStringTransformer() {
    super(Point2D.Double[].class);
  }

  @Override
  public Point2D.Double[] fromStringSafe(String in) {
    Point2DDoubleToStringTransformer elemTransformer = new Point2DDoubleToStringTransformer();
    String[] splitArray = FuLib.split(in, ';');
    Point2D.Double[] res = new Point2D.Double[splitArray.length];
    int idx = 0;
    for (String item : splitArray) {
      Point2D.Double fromString = elemTransformer.fromString(item);
      if (fromString == null) {
        return null;
      }
      res[idx++] = fromString;
    }
    return res;
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public String toStringSafe(Point2D.Double[] in) {
    Point2DDoubleToStringTransformer elemTransformer = new Point2DDoubleToStringTransformer();
    StringBuilder builder = new StringBuilder();
    boolean first = true;
    for (Point2D.Double doubleValue : in) {
      if (!first) {
        builder.append(';');
      }
      first = false;
      builder.append(elemTransformer.toString(doubleValue));
    }
    return builder.toString();
  }
}
