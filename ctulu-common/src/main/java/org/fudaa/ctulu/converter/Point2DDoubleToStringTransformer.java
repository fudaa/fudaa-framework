/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

import com.memoire.fu.FuLib;
import java.awt.geom.Point2D;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Frederic Deniger
 */
public class Point2DDoubleToStringTransformer extends AbstractPropertyToStringTransformer<Point2D.Double> {

  public Point2DDoubleToStringTransformer() {
    super(Point2D.Double.class);
  }

  @Override
  public Point2D.Double fromStringSafe(String in) {
    String[] split = FuLib.split(in, ' ');
    if (split != null && split.length == 2) {
      try {
        double x = Double.parseDouble(split[0]);
        double y = Double.parseDouble(split[1]);
        return new Point2D.Double(x, y);
      } catch (NumberFormatException numberFormatException) {
        Logger.getLogger(Point2DDoubleToStringTransformer.class.getName()).log(Level.INFO, "fromStringSafe", numberFormatException);
      }
    }
    return null;
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public String toStringSafe(Point2D.Double in) {
    return in.getX() + " " + in.getY();
  }
}
