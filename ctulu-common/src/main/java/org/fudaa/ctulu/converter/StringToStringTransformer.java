/*
 GPL 2
 */
package org.fudaa.ctulu.converter;

/**
 *
 * @author Frederic Deniger
 */
public class StringToStringTransformer extends AbstractPropertyToStringTransformer<String> {

  public StringToStringTransformer() {
    super(String.class);
  }

  @Override
  public String fromStringSafe(String in) {
    return in;
  }

  @Override
  public String toStringSafe(String in) {
    return in;
  }

  @Override
  public boolean isValidSafe(String in) {
    return true;
  }
}
