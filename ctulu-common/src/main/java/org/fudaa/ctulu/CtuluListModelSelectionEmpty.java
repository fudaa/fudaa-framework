/**
 *  @creation     21 sept. 2004
 *  @modification $Date: 2006-07-13 13:34:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;

/**
 * @author Fred Deniger
 * @version $Id: CtuluListModelSelectionEmpty.java,v 1.5 2006-07-13 13:34:37 deniger Exp $
 */
public final class CtuluListModelSelectionEmpty implements ListSelectionModel,
    CtuluListSelectionInterface {

  /**
   * Singleton.
   */
  public static final CtuluListModelSelectionEmpty EMPTY = new CtuluListModelSelectionEmpty();

  private CtuluListModelSelectionEmpty() {

  }

  @Override
  public void addListSelectionListener(final ListSelectionListener _x){}

  @Override
  public void addSelectionInterval(final int _index0,final int _index1){}

  @Override
  public void clear(){}

  @Override
  public void clearSelection(){}

  @Override
  public int getAnchorSelectionIndex(){
    return 0;
  }

  @Override
  public int getLeadSelectionIndex(){
    return 0;
  }

  @Override
  public int getMaxIndex(){
    return 0;
  }

  @Override
  public int getMaxSelectionIndex(){
    return 0;
  }

  @Override
  public int getMinIndex(){
    return 0;
  }

  @Override
  public int getMinSelectionIndex(){
    return 0;
  }

  @Override
  public int getNbSelectedIndex(){
    return 0;
  }

  @Override
  public int[] getSelectedIndex(){
    return null;
  }

  @Override
  public int getSelectionMode(){
    return 0;
  }

  @Override
  public boolean getValueIsAdjusting(){
    return false;
  }

  @Override
  public void insertIndexInterval(final int _index,final int _length,final boolean _before){}

  @Override
  public boolean isEmpty(){
    return true;
  }

  @Override
  public boolean isOnlyOnIndexSelected(){
    return false;
  }

  @Override
  public boolean isSelected(final int _i){
    return false;
  }

  @Override
  public boolean isSelectedIndex(final int _index){
    return false;
  }

  @Override
  public boolean isSelectionEmpty(){
    return false;
  }

  @Override
  public void removeIndexInterval(final int _index0,final int _index1){}

  @Override
  public void removeListSelectionListener(final ListSelectionListener _x){}

  @Override
  public void removeSelectionInterval(final int _index0,final int _index1){}

  @Override
  public void setAnchorSelectionIndex(final int _index){}

  @Override
  public void setLeadSelectionIndex(final int _index){}

  @Override
  public void setSelectionInterval(final int _index0,final int _index1){}

  @Override
  public void setSelectionMode(final int _selectionMode){}

  @Override
  public void setValueIsAdjusting(final boolean _valueIsAdjusting){}
}
