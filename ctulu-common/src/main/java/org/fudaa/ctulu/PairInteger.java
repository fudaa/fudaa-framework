package org.fudaa.ctulu;

import java.util.Objects;

public class PairInteger {
  public final String i1;
  public final int i2;

  public PairInteger(String i1, int i2) {
    this.i1 = i1;
    this.i2 = i2;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PairInteger that = (PairInteger) o;
    return i2 == that.i2 &&
        Objects.equals(i1, that.i1);
  }

  @Override
  public int hashCode() {
    return Objects.hash(i1, i2);
  }
}
