/*
 *  @creation     21 oct. 2003
 *  @modification $Date: 2006-12-06 09:03:00 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;



/**
 * Interface a implanter pour une internal frame dont les actions peuvent etre faites/defaites.
 *
 * @author deniger
 * @version $Id: CtuluUndoRedoInterface.java,v 1.5 2006-12-06 09:03:00 deniger Exp $
 */
public interface CtuluUndoRedoInterface {
  /**
   * Si une action defaire/refaire a ete lancee dans une internal frame, toutes les autres
   * internal frames doivent etre au courant.
   * @param _source la source qui a ete modifiee.
   */
  void clearCmd(CtuluCommandManager _source);
  /**
   * @param _active true si l'internal frame est activee :permet de mettre a jour certaines donnees.
   */
  void setActive(boolean _active);
  /**
   * @return le manager de commande de l'internal frame.
   */
  CtuluCommandManager getCmdMng();
}
