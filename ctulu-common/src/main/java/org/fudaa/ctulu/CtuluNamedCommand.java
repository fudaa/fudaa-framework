/**
 *  @creation     20 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;
/**
 * Une interface d�finissant une commande utilisateur nomm�e qui peut �tre defaite ou refaite.
 * C'est une extension destin�e a supplanter � terme {@link CtuluCommand}
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public interface CtuluNamedCommand extends CtuluCommand {
  
  /** 
   * Retourne Le nom de la commande, affichable dans la liste des undo/redo. 
   */
  String getName();
}
