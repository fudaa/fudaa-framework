package org.fudaa.ctulu;

import com.memoire.bu.BuStringValidator;

/**
 * Une classe de validation d'une valeur en secondes ou millisecondes
 * pour une dur�e.
 * La chaine est sous la forme [[[jjj:]hh:]mm:]ss
 */
public class CtuluDurationStringValidator extends BuStringValidator {
  protected boolean valueInMillis = false;

  public CtuluDurationStringValidator() {
    this(false);
  }

  /**
   * @param valueInMillis True : Indique que les valeurs sont g�r�es en millisecondes. False : En secondes.
   */
  public CtuluDurationStringValidator(boolean valueInMillis) {
    this.valueInMillis = valueInMillis;
  }
    
  @Override
  public boolean isStringValid(String _s) {
    return CtuluDuration.fromString(_s) != null;
  }


  @Override
  public Object stringToValue(String _string) {
    CtuluDuration time = CtuluDuration.fromString(_string);
    if (time == null)
      return null;
    
    long duration = time.getDuration();
    if (valueInMillis)
        duration *= 1000;
    
    return duration;
  }

  @Override
  public String valueToString(Object _value) {
    long duration = (long)_value;
    if (valueInMillis)
        duration /= 1000;
      
    CtuluDuration time = new CtuluDuration(duration);
    return time.toString();
  }
}
