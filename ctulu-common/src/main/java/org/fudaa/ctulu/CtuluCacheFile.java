/*
 * @creation 14 d�c. 06
 * @modification $Date: 2007-02-02 11:20:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.io.File;
import java.io.IOException;

/**
 * @author fred deniger
 * @version $Id: CtuluCacheFile.java,v 1.2 2007-02-02 11:20:09 deniger Exp $
 */
public final class CtuluCacheFile {
  final static File DIR = CtuluLibFile.createTempDirSafe();
  {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        CtuluLibFile.deleteDir(DIR);
      }
    });

  }

  private CtuluCacheFile() {};

  public static File createTempDir(final String _prefix) throws IOException {
    return CtuluLibFile.createTempDir(_prefix, DIR);
  }

  public static File createTempFile(final String _prefix, final String _suffix) throws IOException {
    return File.createTempFile(_prefix, _suffix, DIR);
  }

}
