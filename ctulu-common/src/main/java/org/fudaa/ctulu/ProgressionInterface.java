/*
 *  @creation     2002-11-21
 *  @modification $Date: 2008-01-09 15:53:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;
/**
 * une interface pour notifier la progression d'une tache. Doit �tre utilis�e dans la tache,
 * et appel�e r�guli�rement.
 *
 * @version   $Id: ProgressionInterface.java,v 1.4 2008-01-09 15:53:17 bmarchan Exp $
 * @author    Fred Deniger
 */
public interface ProgressionInterface {
  /**
   * @param _v le nouvel avancement
   */
  void setProgression(int _v);
  /**
   * @param _s la nouvelle description de la tache en cours
   */
  void setDesc(String _s);
  /**
   * Nettoie l'avancement et la description de la tache.
   * Appel� au d�but de l'ex�cution de la tache. Reinitialise les temps de calcul de progression.
   */
  void reset();
}
