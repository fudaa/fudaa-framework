/*
 *  @creation     15 juin 2005
 *  @modification $Date: 2006-07-10 14:18:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuSelectFindReplaceInterface;


/**
 * @author Fred Deniger
 * @version $Id: CtuluSelectionInterface.java,v 1.3 2006-07-10 14:18:08 deniger Exp $
 */
public interface CtuluSelectionInterface extends BuSelectFindReplaceInterface {
  /**
   *  inverse the current selection.
   */
  void inverseSelection();

  void clearSelection();
}
