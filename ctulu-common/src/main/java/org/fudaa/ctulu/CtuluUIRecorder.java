/*
 * @creation 19 juin 07
 * @modification $Date: 2007-06-20 12:20:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import java.awt.Component;

/**
 * @author fred deniger
 * @version $Id: CtuluUIRecorder.java,v 1.1 2007-06-20 12:20:45 deniger Exp $
 */
public class CtuluUIRecorder extends CtuluUIAbstract {
  final CtuluAnalyze analyse_;

  public CtuluUIRecorder(final CtuluAnalyze _analyse) {
    super();
    analyse_ = _analyse;
  }

  @Override
  public void clearMainProgression() {}

  @Override
  public CtuluTaskDelegate createTask(String _name) {
    return null;
  }

  @Override
  public void error(String _titre, String _msg, boolean _tempo) {
    analyse_.addFatalError(_msg);

  }

  @Override
  public void error(String _msg) {
    analyse_.addFatalError(_msg);
  }

  @Override
  public ProgressionInterface getMainProgression() {
    return null;
  }

  @Override
  public Component getParentComponent() {
    return null;
  }

  @Override
  public void message(String _titre, String _msg, boolean _tempo) {
    analyse_.addInfo(_msg);

  }

  @Override
  public boolean question(String _titre, String _text) {
    return false;
  }

  @Override
  public void warn(String _titre, String _msg, boolean _tempo) {
    analyse_.addWarn(_msg, -1);
  }

}
