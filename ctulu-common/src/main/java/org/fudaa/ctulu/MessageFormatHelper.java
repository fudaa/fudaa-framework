/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.ctulu;

import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * @author deniger
 */
public final class MessageFormatHelper {

  /**
   * @param bundle
   * @param s le message initial
   * @param args les arguements a utiliser pour le message
   * @return la chaine traduite
   */
  public static String getS(ResourceBundle bundle, final String s, Object... args) {
//    System.err.println("getS "+s+" args= "+args);
    if (bundle.containsKey(s)) {
      try {
        String res = bundle.getString(s);
        if (!CtuluLibArray.isEmpty(args)) {
          return new MessageFormat(res).format(args);
        }
        return res;
      } catch (Exception e) {
      }
    }
    return s;

  }
}
