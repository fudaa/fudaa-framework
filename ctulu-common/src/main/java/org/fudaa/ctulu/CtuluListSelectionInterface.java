/*
 *  @creation     3 nov. 2003
 *  @modification $Date: 2006-04-05 10:00:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ctulu;
/**
 * @author deniger
 * @version $Id: CtuluListSelectionInterface.java,v 1.4 2006-04-05 10:00:08 deniger Exp $
 */
public interface CtuluListSelectionInterface {
  /**
   *  Efface la selection.
   */
  void clear();
  /**
   * @return l'indice max selectionne
   */
  int getMaxIndex();
  /**
   * @return l'indice mini selectionne
   */
  int getMinIndex();
  /**
   * @return le nombre d'indice selectionne
   */
  int getNbSelectedIndex();
  /**
   * @return les indices selectionne. null si aucun
   */
  int[] getSelectedIndex();
  /**
   * @return true si vide
   */
  boolean isEmpty();
  /**
   * @return true si un seul indice est selectionne
   */
  boolean isOnlyOnIndexSelected();
  /**
   * @param _idx l'indice a teste
   * @return true si selection
   */
  boolean isSelected(int _idx);
}
