/**
 *
 */
package org.fudaa.ctulu;

/**
 * Classe permettant de donner le resultat et le log d'une opération.
 *
 * @author deniger
 *
 */
public class CtuluLogGroupResult<T, U> {

  private T resultat;
  private U complement;
  private CtuluLogGroup log;

  public CtuluLogGroupResult() {
  }

  public CtuluLogGroupResult(T resultat, CtuluLogGroup log) {
    this.resultat = resultat;
    this.log = log;
  }

  public CtuluLogGroupResult(T resultat, U complement, CtuluLogGroup log) {
    this.resultat = resultat;
    this.complement = complement;
    this.log = log;
  }

  public U getComplement() {
    return complement;
  }

  public void setComplement(U complement) {
    this.complement = complement;
  }

  public T getResultat() {
    return resultat;
  }

  public void setResultat(T resultat) {
    this.resultat = resultat;
  }

  public CtuluLogGroup getLog() {
    return log;
  }

  public void setLog(CtuluLogGroup log) {
    this.log = log;
  }
}
