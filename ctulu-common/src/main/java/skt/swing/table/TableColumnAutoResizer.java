package skt.swing.table;

/**
 * MySwing: Advanced Swing Utilites
 * Copyright (C) 2005  Santhosh Kumar T
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

/**
 * Allows table columns to be resized to its preferred width
 * by double-clicking at the column-splitter in table header.
 *
 * the preferred width here is the min width reqd to make the
 * data of all rows of that column visible
 *
 * @author Santhosh Kumar T
 * @email  santhosh@in.fiorano.com
 */
public class TableColumnAutoResizer extends MouseAdapter{
  @Override
    public void mouseClicked(final MouseEvent _e){
        if(_e.getClickCount()!=2) {
          return;
        }

        final JTableHeader header = (JTableHeader)_e.getSource();
        final TableColumn tableColumn = getResizingColumn(header, _e.getPoint());
        if(tableColumn==null) {
          return;
        }
        final int col = header.getColumnModel().getColumnIndex(tableColumn.getIdentifier());
        final JTable table = header.getTable();
        final int rowCount = table.getRowCount();
        int width = (int)header.getDefaultRenderer()
                .getTableCellRendererComponent(table, tableColumn.getIdentifier()
                        , false, false, -1, col).getPreferredSize().getWidth();
        for(int row = 0; row<rowCount; row++){
            final int preferedWidth = (int)table.getCellRenderer(row, col).getTableCellRendererComponent(table,
                    table.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth();
            width = Math.max(width, preferedWidth);
        }
        header.setResizingColumn(tableColumn); // this line is very important
        tableColumn.setWidth(width+table.getIntercellSpacing().width);
    }

    // copied of BasicTableHeader.MouseInputHandler.getResizingColumn
    private TableColumn getResizingColumn(final JTableHeader _header, final Point _p){
        return getResizingColumn(_header, _p, _header.columnAtPoint(_p));
    }

    // copied of BasicTableHeader.MouseInputHandler.getResizingColumn
    private TableColumn getResizingColumn(final JTableHeader _header, final Point _p, final int _column){
        if(_column==-1){
            return null;
        }
        final Rectangle r = _header.getHeaderRect(_column);
        r.grow(-3, 0);
        if(r.contains(_p)) {
          return null;
        }
        final int midPoint = r.x+r.width/2;
        int columnIndex;
        if(_header.getComponentOrientation().isLeftToRight()) {
          columnIndex = (_p.x<midPoint) ? _column-1 : _column;
        } else {
          columnIndex = (_p.x<midPoint) ? _column : _column-1;
        }
        if(columnIndex==-1) {
          return null;
        }
        return _header.getColumnModel().getColumn(columnIndex);
    }
}