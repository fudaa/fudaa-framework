package skt.swing.table;

/**
 * MySwing: Advanced Swing Utilites
 * Copyright (C) 2005  Santhosh Kumar T
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.TableColumn;

/**
 * Allows table columns to be resized not only using the header but from any
 * rows. Based on the BasicTableHeaderUI.MouseInputHandler code.
 *
 * @author Santhosh Kumar T
 * @email  santhosh@in.fiorano.com
 */
public class TableColumnResizer extends MouseInputAdapter{
    public final static Cursor resizeCursor = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);

    private int mouseXOffset_;
    private Cursor otherCursor_ = resizeCursor;

    private JTable table_;

    public TableColumnResizer(final JTable _table){
        this.table_ = _table;
        table_.addMouseListener(this);
        table_.addMouseMotionListener(this);
    }

    private boolean canResize(final TableColumn _column){
        return _column != null
                && table_.getTableHeader().getResizingAllowed()
                && _column.getResizable();
    }

    private TableColumn getResizingColumn(final Point _p){
        return getResizingColumn(_p, table_.columnAtPoint(_p));
    }

    private TableColumn getResizingColumn(final Point _p, final int _column){
        if(_column == -1){
            return null;
        }
        final int row = table_.rowAtPoint(_p);
        if(row==-1) {
          return null;
        }
        final Rectangle r = table_.getCellRect(row, _column, true);
        r.grow( -3, 0);
        if(r.contains(_p)) {
          return null;
        }

        final int midPoint = r.x + r.width / 2;
        int columnIndex;
        if(table_.getTableHeader().getComponentOrientation().isLeftToRight()) {
          columnIndex = (_p.x < midPoint) ? _column - 1 : _column;
        } else {
          columnIndex = (_p.x < midPoint) ? _column : _column - 1;
        }

        if(columnIndex == -1) {
          return null;
        }
        return table_.getTableHeader().getColumnModel().getColumn(columnIndex);
    }

  @Override
    public void mousePressed(final MouseEvent _e){
        table_.getTableHeader().setDraggedColumn(null);
        table_.getTableHeader().setResizingColumn(null);
        table_.getTableHeader().setDraggedDistance(0);

        final Point p = _e.getPoint();

        // First find which header cell was hit
        final int index = table_.columnAtPoint(p);
        if(index==-1) {
          return;
        }

        // The last 3 pixels + 3 pixels of next column are for resizing
        final TableColumn resizingColumn = getResizingColumn(p, index);
        if(!canResize(resizingColumn)) {
          return;
        }

        table_.getTableHeader().setResizingColumn(resizingColumn);
        if(table_.getTableHeader().getComponentOrientation().isLeftToRight()) {
          mouseXOffset_ = p.x - resizingColumn.getWidth();
        } else {
          mouseXOffset_ = p.x + resizingColumn.getWidth();
        }
    }

    private void swapCursor(){
        final Cursor tmp = table_.getCursor();
        table_.setCursor(otherCursor_);
        otherCursor_ = tmp;
    }

  @Override
    public void mouseMoved(final MouseEvent _e){
        if(canResize(getResizingColumn(_e.getPoint()))
           != (table_.getCursor() == resizeCursor)){
            swapCursor();
        }
    }

  @Override
    public void mouseDragged(final MouseEvent _e){
        final int mouseX = _e.getX();

        final TableColumn resizingColumn = table_.getTableHeader().getResizingColumn();
        if(resizingColumn != null){
          boolean headerLeftToRight =
            table_.getTableHeader().getComponentOrientation().isLeftToRight();

            final int oldWidth = resizingColumn.getWidth();
            int newWidth;
            if(headerLeftToRight){
                newWidth = mouseX - mouseXOffset_;
            } else{
                newWidth = mouseXOffset_ - mouseX;
            }
            resizingColumn.setWidth(newWidth);

            Container container;
            if((table_.getTableHeader().getParent() == null)
               || ((container = table_.getTableHeader().getParent().getParent()) == null)
                                || !(container instanceof JScrollPane)){
                return;
            }

            if(!container.getComponentOrientation().isLeftToRight()
               && !headerLeftToRight){
                if(table_ != null){
                    final JViewport viewport = ((JScrollPane)container).getViewport();
                    final int viewportWidth = viewport.getWidth();
                    final int diff = newWidth - oldWidth;
                    final int newHeaderWidth = table_.getWidth() + diff;

                    /* Resize a table */
                    final Dimension tableSize = table_.getSize();
                    tableSize.width += diff;
                    table_.setSize(tableSize);

                    /*
                     * If this table is in AUTO_RESIZE_OFF mode and has a horizontal
                     * scrollbar, we need to update a view's position.
                     */
                    if((newHeaderWidth >= viewportWidth)
                       && (table_.getAutoResizeMode() == JTable.AUTO_RESIZE_OFF)){
                        final Point p = viewport.getViewPosition();
                        p.x =
                                Math.max(0, Math.min(newHeaderWidth - viewportWidth, p.x + diff));
                        viewport.setViewPosition(p);

                        /* Update the original X offset value. */
                        mouseXOffset_ += diff;
                    }
                }
            }
        }
    }

  @Override
    public void mouseReleased(final MouseEvent _e){
        table_.getTableHeader().setResizingColumn(null);
        table_.getTableHeader().setDraggedColumn(null);
    }
}