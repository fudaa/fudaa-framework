package skt.swing.table;

/**
 * MySwing: Advanced Swing Utilites
 * Copyright (C) 2005  Santhosh Kumar T
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

import java.util.EventObject;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * Enhanced JTable.
 *
 * @author Santhosh Kumar T
 * @email  santhosh@in.fiorano.com
 */
public class MyTable extends JTable{


    public MyTable(){
    }

    public MyTable(final int _numRows, final int _numColumns){
        super(_numRows, _numColumns);
    }

    public MyTable(final TableModel _dm){
        super(_dm);
    }

    public MyTable(final Object[][] _rowData, final Object[] _columnNames){
        super(_rowData, _columnNames);
    }

    public MyTable(final Vector _rowData, final Vector _columnNames){
        super(_rowData, _columnNames);
    }

    public MyTable(final TableModel _dm, final TableColumnModel _cm){
        super(_dm, _cm);
    }

    public MyTable(final TableModel _dm, final TableColumnModel _cm, final ListSelectionModel _sm){
        super(_dm, _cm, _sm);
    }

    private static final TableColumnAutoResizer COLUMN_AUTO_RESIZER = new TableColumnAutoResizer();

  @Override
    public void addNotify(){
        super.addNotify();
        getTableHeader().addMouseListener(COLUMN_AUTO_RESIZER);
    }

  @Override
    public void removeNotify(){
        super.removeNotify();
        getTableHeader().removeMouseListener(COLUMN_AUTO_RESIZER);
    }

  @Override
    public void setTableHeader(final JTableHeader _tableHeader){
        if(getTableHeader()!=null) {
          getTableHeader().removeMouseListener(COLUMN_AUTO_RESIZER);
        }
        super.setTableHeader(_tableHeader);
        if(_tableHeader!=null && isShowing()) {
          _tableHeader.addMouseListener(COLUMN_AUTO_RESIZER);
        }
    }


    protected MouseInputAdapter rowResizer_;
    protected MouseInputAdapter  columnResizer_ ;

    // turn resizing on/of
    public void setResizable(final boolean _row, final boolean _column){
        if(_row){
            if(rowResizer_==null) {
              rowResizer_ = new TableRowResizer(this);
            }
        }else if(rowResizer_!=null){
            removeMouseListener(rowResizer_);
            removeMouseMotionListener(rowResizer_);
            rowResizer_ = null;
        }
        if(_column){
            if(columnResizer_==null) {
              columnResizer_ = new TableColumnResizer(this);
            }
        }else if(columnResizer_!=null){
            removeMouseListener(columnResizer_);
            removeMouseMotionListener(columnResizer_);
            columnResizer_ = null;
        }
    }

    // mouse events intended for resize shouldn't activate editing
  @Override
    public boolean editCellAt(final int _row, final int _column, final EventObject _e){
        if(getCursor()==TableColumnResizer.resizeCursor || getCursor()==TableRowResizer.RESIZE_CURSOR) {
          return false;
        }
        return super.editCellAt(_row, _column, _e);
    }

    // mouse press intended for resize shouldn't change row/col/cell celection
  @Override
    public void changeSelection(final int _row, final int _column, final boolean _toggle, final boolean _extend) {
        if(getCursor()==TableColumnResizer.resizeCursor || getCursor()==TableRowResizer.RESIZE_CURSOR) {
          return;
        }
        super.changeSelection(_row, _column, _toggle, _extend);
    }

    /*-------------------------------------------------[ Scrollable ]---------------------------------------------------*/

    // overriden to make the height of scroll match viewpost height
    // if smaller
  @Override
    public boolean getScrollableTracksViewportHeight() {
        return getPreferredSize().height < getParent().getHeight();
    }
}