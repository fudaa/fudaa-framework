package skt.swing.table;

/**
 * MySwing: Advanced Swing Utilites
 * Copyright (C) 2005  Santhosh Kumar T
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.event.MouseInputAdapter;

/**
 * Allows table Rows to be resized without using any row headers just by dragging the horizontal grid lines.
 * 
 * @author Santhosh Kumar T
 * @email santhosh@in.fiorano.com
 */
public class TableRowResizer extends MouseInputAdapter {
  public static final Cursor RESIZE_CURSOR = Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);

  private int mouseYOffset_;
  private int resizingRow_;
  private Cursor otherCursor_ = RESIZE_CURSOR;
  private JTable table_;

  public TableRowResizer(final JTable _table) {
    this.table_ = _table;
    _table.addMouseListener(this);
    _table.addMouseMotionListener(this);
  }

  private int getResizingRow(final Point _p) {
    return getResizingRow(_p, table_.rowAtPoint(_p));
  }

  private int getResizingRow(final Point _p, final int _row) {
    if (_row == -1) {
      return -1;
    }
    final int col = table_.columnAtPoint(_p);
    if (col == -1) {
      return -1;
    }
    final Rectangle r = table_.getCellRect(_row, col, true);
    r.grow(0, -3);
    if (r.contains(_p)) {
      return -1;
    }

    final int midPoint = r.y + r.height / 2;
    final int rowIndex = (_p.y < midPoint) ? _row - 1 : _row;

    return rowIndex;
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
    final Point p = _e.getPoint();

    resizingRow_ = getResizingRow(p);
    mouseYOffset_ = p.y - table_.getRowHeight(resizingRow_);
  }

  private void swapCursor() {
    final Cursor tmp = table_.getCursor();
    table_.setCursor(otherCursor_);
    otherCursor_ = tmp;
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {
    if ((getResizingRow(_e.getPoint()) >= 0) != (table_.getCursor() == RESIZE_CURSOR)) {
      swapCursor();
    }
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    final int mouseY = _e.getY();

    if (resizingRow_ >= 0) {
      final int newHeight = mouseY - mouseYOffset_;
      if (newHeight > 0) {
        table_.setRowHeight(resizingRow_, newHeight);
      }
    }
  }
}
