/*
 GPL 2
 */
package org.fudaa.ctulu.iterator;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Frederic Deniger
 */
public class FixedIntegerIteratorTest extends TestCase {

  public void testIterate() {
    int[] values = new int[]{-1, 1, 10, 100};
    FixedIntegerIterator iterator = new FixedIntegerIterator(values);
    iterator.init(-100, 1000, 1);
    assertTrue(iterator.hasNext());
    assertEquals(-1, (int) iterator.currentValue());


    List<String> readValues = getValuesForIterator(iterator);
    assertSame(readValues, new String[]{"-1", "1", "10", "100"});

    iterator.init(-1, 1000, 1);
    readValues = getValuesForIterator(iterator);
    assertSame(readValues, new String[]{"-1", "1", "10", "100"});

    iterator.init(-1, 99, 1);
    readValues = getValuesForIterator(iterator);
    assertSame(readValues, new String[]{"-1", "1", "10"});


    iterator.init(9, 99, 1);
    readValues = getValuesForIterator(iterator);
    assertSame(readValues, new String[]{"10"});


    iterator.init(101, 1000, 1);
    readValues = getValuesForIterator(iterator);
    assertTrue(readValues.isEmpty());
  }

  private List<String> getValuesForIterator(FixedIntegerIterator iterator) {
    List<String> readValues = new ArrayList<String>();
    for (final TickIterator it = iterator; it.hasNext(); it.next()) {
      readValues.add(iterator.currentLabel());
    }
    return readValues;
  }

  public void assertSame(List<String> readValues, String[] expectedValues) {
    assertEquals(readValues.size(), expectedValues.length);
    for (int i = 0; i < expectedValues.length; i++) {
      assertEquals(expectedValues[i], readValues.get(i));
    }
  }
}
