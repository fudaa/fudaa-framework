/*
 *  @creation     9 d�c. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import junit.framework.TestCase;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class TestJCtuluParser extends TestCase {

  public void testParser(){
    final String s = "1+23.4";
    /* final JEP j = new JEP();
     j.parseExpression(s);
     assertFalse(j.hasError());
     final JEP j2 = new JEP(j.getTraverse(), j.getAllowUndeclared(), j.getImplicitMul(), j
     .getNumberFactory());
     j2.parseExpression(s);
     assertFalse(j2.hasError());*/

    final CtuluParser parser = new CtuluParser();
    parser.parseExpression(s);
    assertFalse(parser.hasError());
    assertEquals(s, parser.getLastExpr());
    final CtuluParser copie = new CtuluParser(parser);
    copie.parseExpression(s);
    assertFalse(copie.hasError());
    assertEquals(s, copie.getLastExpr());
  }

}
