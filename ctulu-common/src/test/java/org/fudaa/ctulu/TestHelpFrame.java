/*
 *  @creation     10 janv. 2006
 *  @modification $Date: 2006-11-14 09:04:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import com.memoire.bu.BuApplication;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuHelpFrame;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLib;
import java.io.InputStream;
import java.net.URL;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * @author Fred Deniger
 * @version $Id: TestHelpFrame.java,v 1.4 2006-11-14 09:04:08 deniger Exp $
 */
public final class TestHelpFrame extends BuHelpFrame {

  class TocHandler implements ContentHandler {

    DefaultMutableTreeNode root_;

    DefaultMutableTreeNode currentFile_;

    int currentDepth_;

    TreeModel getTreeModel() {
      if (root_ != null) {
        return new DefaultTreeModel(root_);
      }
      return null;
    }

    @Override
    public void characters(char[] _ch, int _start, int _length) throws SAXException {}

    @Override
    public void endDocument() throws SAXException {

    }

    @Override
    public void endElement(String _namespaceURI, String _localName, String _name) throws SAXException {
      currentDepth_--;

    }

    @Override
    public void endPrefixMapping(String _prefix) throws SAXException {}

    @Override
    public void ignorableWhitespace(char[] _ch, int _start, int _length) throws SAXException {}

    @Override
    public void processingInstruction(String _target, String _data) throws SAXException {}

    @Override
    public void setDocumentLocator(Locator _locator) {}

    @Override
    public void skippedEntity(String _name) throws SAXException {}

    @Override
    public void startDocument() throws SAXException {}

    private DefaultMutableTreeNode createFromAtt(Attributes _atts) {
      
      return new DefaultMutableTreeNode(_atts.getValue("label") + '|' + _atts.getValue("href"));
    }

    @Override
    public void startElement(String _namespaceURI, String _localName, String _name, Attributes _atts)
        throws SAXException {
      if (root_ == null) {
        root_ = createFromAtt(_atts);
      }
      // file
      else if (currentDepth_ == 1) {
        currentFile_ = createFromAtt(_atts);
        System.out.println("ajout file " + currentFile_);
        root_.add(currentFile_);
      } else if (currentDepth_ == 2 && currentFile_ != null) {
        System.out.println("ajout chpater " + createFromAtt(_atts));
        currentFile_.add(createFromAtt(_atts));
      }
      currentDepth_++;

    }

    @Override
    public void startPrefixMapping(String _prefix, String _uri) throws SAXException {}

  }

  public TestHelpFrame(BuCommonImplementation _app) {
    super(_app);
  }

  @Override
  protected TreeModel createIndex(URL urlIndex) throws Exception {
    TocHandler handler = new TocHandler();
    InputStream io = getClass().getResourceAsStream("toc.fr.xml");
    SAXParserFactory parser = SAXParserFactory.newInstance();
    parser.setNamespaceAware(false);
    parser.setValidating(false);
    SAXParser saxparser = parser.newSAXParser();
    XMLReader reader = saxparser.getXMLReader();
    reader.setFeature("http://xml.org/sax/features/validation", false);
    reader.setFeature("http://xml.org/sax/features/namespaces", false);
    reader.setFeature("http://xml.org/sax/features/namespace-prefixes", false);
    reader.setFeature("http://xml.org/sax/features/lexical-handler/parameter-entities", false);
    reader.setFeature("http://xml.org/sax/features/resolve-dtd-uris", false);
    reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
    // reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    reader.setContentHandler(handler);
    InputSource s = new InputSource(io);
    s.setEncoding("UTF-8");
    saxparser.getXMLReader().parse(s);
    return handler.getTreeModel();

  }

  public static void main(String[] _args) {
    System.setProperty("proxyHost", "proxyinternet");
    System.setProperty("proxyPort", "80");

    final Runnable r = new Runnable() {

      @Override
      public void run() {
        final BuCommonImplementation imp = new BuCommonImplementation() {

          @Override
          public com.memoire.bu.BuInformationsSoftware getInformationsSoftware() {
            final BuInformationsSoftware soft = super.getInformationsSoftware();
            soft.man = "file:/C:/aide_prepro/build/doc/post/";
            return soft;
          }
        };
        final BuApplication app = new BuApplication();
        app.setImplementation(imp);
        app.init();
        app.start();
        final BuHelpFrame help = new TestHelpFrame(imp);
        imp.addInternalFrame(help);
        help.setSize(200, 300);
      }
    };
    BuLib.invokeLater(r);

  }
}
