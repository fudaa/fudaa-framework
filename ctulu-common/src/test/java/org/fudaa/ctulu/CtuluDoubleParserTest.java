package org.fudaa.ctulu;

import junit.framework.TestCase;
import org.junit.Assert;

public class CtuluDoubleParserTest extends TestCase {

  public void testIsValid() {
    CtuluDoubleParser parser=new CtuluDoubleParser();
    Assert.assertTrue("2.2 is a double",parser.isValid("2.2"));
    Assert.assertFalse("No virgule found", parser.isVirugule_);
    Assert.assertTrue("3 is a double",parser.isValid("3"));
    Assert.assertFalse("No virgule found", parser.isVirugule_);
    Assert.assertTrue("2,2 is a valid double for frend", parser.isValid("2,2"));
    Assert.assertTrue("virgule found in 2,2",parser.isVirugule_);
  }
  public void testIsValid_shouldReturnFalse() {
    CtuluDoubleParser parser=new CtuluDoubleParser();
    Assert.assertFalse("Two is not a double",parser.isValid("Two"));
    Assert.assertFalse("No virgule found", parser.isVirugule_);
    Assert.assertFalse("Thow,Three is a not double",parser.isValid("Thow,Three"));
    Assert.assertFalse("No virgule found", parser.isVirugule_);
  }
}