/*
 *  @creation     12 janv. 2006
 *  @modification $Date: 2007-04-02 08:58:06 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import junit.framework.TestCase;

/**
 * @author Fred Deniger
 * @version $Id: TestJFortranFormat.java,v 1.8 2007-04-02 08:58:06 deniger Exp $
 */
public class TestJFortranFormat extends TestCase {

  public void testNumber() {
    CtuluNumberFormatFortran fmt = new CtuluNumberFormatFortran(5);
    final double[] values = new double[] { 1.001, 1E13, 23.456, -23.456, 1E111 };
    int idx = 0;
    /*
     * DecimalFormat fmtExp=CtuluLib.getDecimalFormat("0.#####E0"); fmtExp.setDecimalSeparatorAlwaysShown(false);
     * fmtExp.setMaximumFractionDigits(Integer.MAX_VALUE); fmtExp.setMinimumFractionDigits(0);
     * System.out.println("laav"); System.out.println(fmtExp.format(1.4E13));
     */
    assertTrue(fmt.isValid(values[idx]));
    assertEquals("1.001", fmt.format(values[idx++]));
    assertTrue(fmt.isValid(values[idx]));
    idx++;
    /*
     * assertTrue(fmt.isValid(values[idx])); assertEquals("1.E13", fmt.format(values[idx++]));
     */assertTrue(fmt.isValid(values[idx]));
    assertEquals("23.46", fmt.format(values[idx++]));
    assertTrue(fmt.isValid(values[idx]));
    assertEquals("-23.5", fmt.format(values[idx++]));
    assertTrue(fmt.isValid(values[idx]));
    assertEquals("1E111", fmt.format(values[idx++]));
    fmt = new CtuluNumberFormatFortran(7);
    for (int i = values.length - 1; i >= 0; i--) {
      assertTrue(fmt.isValid(values[i]));
      assertEquals(Double.toString(values[i]), fmt.format(values[i]));
    }
    fmt = new CtuluNumberFormatFortran(2);
    for (int i = values.length - 1; i > 0; i--) {
      assertEquals("format a 2 chiffre de " + i + ": " + values[i], i != 3, fmt.isValid(values[i]));
    }
    assertTrue(fmt.isValid(values[0]));
    assertEquals("1.", fmt.format(values[0]));
    fmt = new CtuluNumberFormatFortran(8);
    assertEquals("-0.45455", fmt.format(-0.454554));
    System.out.println(fmt.format(-8.4703E-22));
    assertEquals("600.0", fmt.format(600D));
  }

  public void testFixedFigure() {
    CtuluNumberFormatFixedFigure figure = new CtuluNumberFormatFixedFigure(6);
    assertEquals("12345", figure.format(12345.0));
    assertEquals("266000", figure.format(266000.0));
    assertEquals("266200", figure.format(266200.0));
    assertEquals("8000", figure.format(8000));
    assertEquals("8E-5", figure.format(0.000080000));
    assertEquals("1.23E8", figure.format(123000000));
    figure = new CtuluNumberFormatFixedFigure(3);
    assertEquals("12300", figure.format(12345.0));

  }
  
  
  public void testDurationFmt(){
    CtuluNumberFormatI fmt=CtuluDurationFormatter.DEFAULT_TIME;
    assertEquals("266000", fmt.format(266000.0));
    assertEquals("266200", fmt.format(266200.0));
  }

}
