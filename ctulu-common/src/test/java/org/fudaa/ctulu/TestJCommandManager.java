/**
 * @creation 21 oct. 2003
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu;

import junit.framework.TestCase;


/**
 * @author deniger
 * @version $Id: TestJCommandManager.java,v 1.5 2007-01-19 13:07:19 deniger Exp $
 */
public class TestJCommandManager extends TestCase {

  /**
   * Constructeur par defaut.
   */
  public TestJCommandManager() {}

  /**
   * test DodicoCommandManager.
   */
  public void testCmdManager() {
    int max = 6;
    CtuluCommandManager mng = new CtuluCommandManager(max);
    assertEquals(max, mng.getNbMaxCmd());
    assertFalse(mng.canRedo());
    assertFalse(mng.canUndo());
    TestCommand[] cmds = new TestCommand[5];
    cmds[0] = createDodicoCommand();
    mng.addCmd(cmds[0]);
    assertFalse(mng.canRedo());
    assertTrue(mng.canUndo());
    assertFalse(mng.isACommandUndo());
    mng.undo();
    assertTrue(mng.canRedo());
    assertFalse(mng.canUndo());
    assertTrue(mng.isACommandUndo());
    mng.redo();
    assertFalse(mng.canRedo());
    assertTrue(mng.canUndo());
    assertFalse(mng.isACommandUndo());
    for (int i = 1; i < cmds.length; i++) {
      cmds[i] = createDodicoCommand();
      mng.addCmd(cmds[i]);
    }
    assertEquals(cmds.length, mng.getNbCmd());
    mng.undo();
    assertTrue(cmds[cmds.length - 1].undo_);
    mng.redo();
    assertFalse(cmds[cmds.length - 1].undo_);
    mng.undo();
    assertTrue(cmds[cmds.length - 1].undo_);
    mng.undo();
    assertTrue(mng.isUndo(cmds.length - 2));
    assertTrue(mng.isUndo(cmds[cmds.length - 2]));
    assertTrue(cmds[cmds.length - 2].undo_);
    mng.redo();
    assertFalse(cmds[cmds.length - 2].undo_);
    assertTrue(cmds[cmds.length - 1].undo_);
    assertTrue(mng.isUndo(cmds.length - 1));
    assertTrue(mng.isUndo(cmds[cmds.length - 1]));
    mng.undo();
    mng.addCmd(createDodicoCommand());
    assertEquals(cmds.length - 1, mng.getNbCmd());
    mng.addCmd(createDodicoCommand());
    mng.addCmd(createDodicoCommand());
    assertEquals(max, mng.getNbCmd());
    mng.addCmd(createDodicoCommand());
    assertEquals(max, mng.getNbCmd());
    assertFalse(mng.containsCmd(cmds[0]));
    assertTrue(mng.containsCmd(cmds[1]));
    mng.addCmd(createDodicoCommand());
    assertFalse(mng.containsCmd(cmds[1]));
  }

  /**
   * @return testCommand
   */
  public TestCommand createDodicoCommand() {
    return new TestCommand();
  }

  class TestCommand implements CtuluCommand {

    boolean undo_;

    /**
     * Met undo a true.
     * 
     * @see org.fudaa.ctulu.CtuluCommand#undo()
     */
    @Override
    public void undo() {
      undo_ = true;
    }

    /**
     * Met undo a false.
     * 
     * @see org.fudaa.ctulu.CtuluCommand#undo()
     */
    @Override
    public void redo() {
      undo_ = false;
    }
  }
}