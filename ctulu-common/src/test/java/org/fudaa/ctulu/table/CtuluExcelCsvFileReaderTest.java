/*
GPL 2
 */
package org.fudaa.ctulu.table;

import java.io.File;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibFile;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;
/**
 *
 * @author Frederic Deniger
 */
public class CtuluExcelCsvFileReaderTest extends TestCase {

  @Test
  public void testReadXls() throws IOException {
    File f = CtuluLibFile.getFileFromJar("/org/fudaa/ctulu/table/tabler.xlsx", File.createTempFile("tabler", ".xlsx"));
    String[][] values = new CtuluExcelCsvFileReader(f).readFile();
    assertEquals(38, values.length);
    assertEquals(11, values[10].length);
    assertArrayEquals(new String[]{"Cc_P10", "Qapp", "245.0", "Qapp", "115.0", "", "", "Zimp", "26.24", "Qapp", "-2189.0"}, values[10]);
    assertArrayEquals(new String[]{"Cc_P37", "Qapp", "8600.0", "Qapp", "2400.0", "Qapp", "1500.0", "Zimp", "34.56", "", ""}, values[37]);

  }

  @Test
  public void testReadCsv() throws IOException {
    File f = CtuluLibFile.getFileFromJar("/org/fudaa/ctulu/table/tabler.csv", File.createTempFile("example", ".csv"));
    String[][] values = new CtuluExcelCsvFileReader(f).readFile();
    assertEquals(38, values.length);
    assertEquals(11, values[10].length);
    assertArrayEquals(new String[]{"Cc_P10", "Qapp", "245", "Qapp", "115", "", "", "Zimp", "26,24", "Qapp", "-2189"}, values[10]);
    assertArrayEquals(new String[]{"Cc_P37", "Qapp", "8600", "Qapp", "2400", "Qapp", "1500", "Zimp", "34,56", "", ""}, values[37]);

  }

}
