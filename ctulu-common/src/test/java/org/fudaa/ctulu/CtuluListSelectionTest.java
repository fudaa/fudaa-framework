/*
 GPL 2
 */
package org.fudaa.ctulu;

import junit.framework.TestCase;

/**
 *
 * @author Frederic Deniger
 */
public class CtuluListSelectionTest extends TestCase {

  public CtuluListSelectionTest() {
  }

  public void testCreate() {
    CtuluListSelection selection = new CtuluListSelection();
    assertEquals(-1, selection.getMaxIndex());
    assertEquals(-1, selection.getMinIndex());


    FastBitSet fastset = new FastBitSet();
    fastset.set(2);
    assertTrue(fastset.get(2));

    selection = new CtuluListSelection(fastset);
    assertEquals(2, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());

    selection = new CtuluListSelection(selection);
    assertEquals(2, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());


    selection = new CtuluListSelection(10);
    assertEquals(-1, selection.getMaxIndex());
    assertEquals(-1, selection.getMinIndex());


  }

  public void testAdd() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    assertEquals(5, selection.getMaxIndex());
    assertEquals(5, selection.getMinIndex());
  }

  public void testAddList() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    assertEquals(5, selection.getMaxIndex());
    assertEquals(5, selection.getMinIndex());

    CtuluListSelection selectionToAdd = new CtuluListSelection();
    selectionToAdd.add(2);
    selectionToAdd.add(6);
    selection.add(selectionToAdd);
    assertEquals(6, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
  }

  public void testInverse() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    selection.inverse();
    assertEquals(4, selection.getMaxIndex());
    assertEquals(0, selection.getMinIndex());

    selection.inverse(10);
    assertEquals(9, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
  }

  public void testRemove() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    assertEquals(5, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
    selection.remove(5);
    assertEquals(2, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());

  }

  public void testRemoveList() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    assertEquals(5, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
    CtuluListSelection selectionToRemove = new CtuluListSelection();
    selectionToRemove.add(5);
    selection.remove(selectionToRemove);
    assertEquals(2, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());

  }

  public void testAddInterval() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    assertEquals(5, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
    selection.addInterval(1, 10);
    assertEquals(10, selection.getMaxIndex());
    assertEquals(1, selection.getMinIndex());
  }

  public void testRemoveInterval() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    selection.removeInterval(4, 10);
    assertEquals(2, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
  }

  public void testAnd() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    CtuluListSelection selectionAnd = new CtuluListSelection();
    selectionAnd.add(2);
    selection.and(selectionAnd);
    assertEquals(2, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
  }

  public void testOr() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    CtuluListSelection selectionOr = new CtuluListSelection();
    selectionOr.add(6);
    selection.or(selectionOr);
    assertEquals(6, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
  }

  public void testXOr() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    CtuluListSelection selectionOr = new CtuluListSelection();
    selectionOr.add(5);
    selectionOr.add(3);
    selection.xor(selectionOr);
    assertEquals(3, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
  }

  public void testClear() {
    CtuluListSelection selection = new CtuluListSelection();
    selection.add(5);
    selection.add(2);
    assertEquals(5, selection.getMaxIndex());
    assertEquals(2, selection.getMinIndex());
    selection.clear();
    assertEquals(-1, selection.getMaxIndex());
    assertEquals(-1, selection.getMinIndex());
  }
}
