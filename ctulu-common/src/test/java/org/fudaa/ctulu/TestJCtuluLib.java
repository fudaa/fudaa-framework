/*
 *  @creation     6 d�c. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;

import gnu.trove.TDoubleArrayList;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import junit.framework.TestCase;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class TestJCtuluLib extends TestCase {

  public void testXml() {
    StringWriter out = new StringWriter();
    try {
      CtuluXmlWriter writer = new CtuluXmlWriter(out);
      writer.setMainTag("layer");
      final String name = "name";
      writer.writeEntry(name, "Nom du calque");
      writer.finish();
      // System.out.println(out.getBuffer().toString());
      DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
      builder.setValidating(false);
      builder.setNamespaceAware(false);
      InputStream reader = new ByteArrayInputStream(out.getBuffer().toString().getBytes());
      Document doc = builder.newDocumentBuilder().parse(reader);
      Node el = doc.getFirstChild();
      assertNotNull(el);
      assertEquals("layer", el.getNodeName());
      assertEquals(3, el.getChildNodes().getLength());
      el = doc.getElementsByTagName(name).item(0);
      assertNotNull(el);
      assertEquals(1, el.getChildNodes().getLength());
      assertEquals(name, el.getNodeName());
      // System.out.println("nombre de fils: " + el.getChildNodes().getLength());
      // System.out.println(el.getFirstChild().getNodeValue());
      CtuluXmlReaderHelper helper = CtuluXmlReaderHelper.createReader(new ByteArrayInputStream(out.getBuffer()
          .toString().getBytes()));
      assertEquals("Nom du calque", helper.getTextFor(name));

    } catch (Exception _evt) {
      _evt.printStackTrace();

    }

  }

  

  public void testTrimRight() {
    assertEquals(CtuluLibString.EMPTY_STRING, CtuluLibString.trimRight(""));
    assertEquals(CtuluLibString.EMPTY_STRING, CtuluLibString.trimRight("  "));
    assertEquals("5", CtuluLibString.trimRight("5 "));
    assertEquals(" 5", CtuluLibString.trimRight(" 5"));
    assertEquals(" 5 3", CtuluLibString.trimRight(" 5 3  "));
  }

  public void testFilterTable() {
    String[][] data = new String[2][3];
    String string = getToto();
    data[0][0] = string;
    data[1][0] = CtuluLibString.UN;
    data[0][1] = CtuluLibString.DEUX;
    data[1][1] = CtuluLibString.TROIS;
    data[0][2] = CtuluLibString.QUATRE;
    data[1][2] = CtuluLibString.CINQ;
    double[][] res = CtuluLibArray.getCorrectValue(data, new CtuluDoubleParser());
    assertNotNull(res);
    assertEquals(2, res.length);
    assertEquals(2, res[0].length);
    assertEquals(2, res[1].length);
    final double eps = 1E-15;
    assertEquals(2, res[0][0], eps);
    assertEquals(3, res[1][0], eps);
    assertEquals(4, res[0][1], eps);
    assertEquals(5, res[1][1], eps);
    data[0][0] = string;
    data[1][0] = CtuluLibString.UN;
    data[0][1] = CtuluLibString.DEUX;
    data[1][1] = CtuluLibString.TROIS;
    data[0][2] = CtuluLibString.QUATRE;
    data[1][2] = "tata";
    res = CtuluLibArray.getCorrectValue(data, new CtuluDoubleParser());
    assertNotNull(res);
    assertEquals(2, res.length);
    assertEquals(1, res[0].length);
    assertEquals(1, res[1].length);
    assertEquals(2, res[0][0], eps);
    assertEquals(3, res[1][0], eps);

  }

  private String getToto() {
    return "toto";
  }

  public void testFilterTableSplit() {
    String[][] data = new String[2][3];
    String string = getToto();
    data[0][0] = string;
    data[1][0] = CtuluLibString.UN;
    data[0][1] = CtuluLibString.DEUX;
    data[1][1] = CtuluLibString.TROIS;
    data[0][2] = CtuluLibString.QUATRE;
    data[1][2] = CtuluLibString.CINQ;
    List res = CtuluLibArray.getCorrectValueSplit(data, new CtuluDoubleParser());
    assertNotNull(res);
    assertEquals(1, res.size());
    TDoubleArrayList[] doubleRes = (TDoubleArrayList[]) res.get(0);
    assertEquals(2, doubleRes.length);
    assertEquals(2, doubleRes[0].size());
    assertEquals(2, doubleRes[1].size());
    final double eps = 1E-15;
    assertEquals(2, doubleRes[0].get(0), eps);
    assertEquals(3, doubleRes[1].get(0), eps);
    assertEquals(4, doubleRes[0].get(1), eps);
    assertEquals(5, doubleRes[1].get(1), eps);
    data[0][0] = "0";
    data[1][0] = CtuluLibString.UN;
    data[0][1] = CtuluLibString.DEUX;
    data[1][1] = "tata";
    data[0][2] = CtuluLibString.QUATRE;
    data[1][2] = CtuluLibString.CINQ;
    res = CtuluLibArray.getCorrectValueSplit(data, new CtuluDoubleParser());
    assertNotNull(res);
    assertEquals(2, res.size());
  }

  public void testCreateDeleteDir() {
    final String name = getToto();
    File f = null;
    try {
      f = CtuluLibFile.createTempDir();
    } catch (IOException e) {
      e.printStackTrace();
    }
    if (f == null) return;
    assertNotNull(f);
    assertTrue(f.exists());
    assertTrue(f.isDirectory());
    try {
      assertNotNull(File.createTempFile(name, "tatat", f));
      final File sousDir = CtuluLibFile.createTempDir(name, f);
      assertNotNull(sousDir);
      assertNotNull(File.createTempFile(name, "tatat", sousDir));

    } catch (IOException e1) {
      e1.printStackTrace();
      fail(e1.getMessage());
    }
    CtuluLibFile.deleteDir(f);
    assertFalse(f.exists());
    try {
      f = CtuluLibFile.createTempDir(name);
    } catch (IOException e) {
      e.printStackTrace();
    }
    assertNotNull(f);
    System.out.println("fichier cree " + f.getAbsolutePath());
    assertTrue(f.exists());
    assertTrue(f.isDirectory());
    assertTrue(f.getName().startsWith(name));
    CtuluLibFile.deleteDir(f);
    assertFalse(f.exists());
  }

  public void testLibString() {
    final String init = "dede";
    final String esp = "   ";
    String finalString = CtuluLibString.adjustSize(7, init);

    assertEquals(init + esp, finalString);
    finalString = CtuluLibString.adjustSizeBefore(7, init);
    assertEquals(esp + init, finalString);
  }
  
  public void testToString(){
	  File fzip=new File("toto.zip");
	  File fgz=CtuluLibFile.changeExtension(fzip,"gz");
	  assertEquals("toto.gz",fgz.getName());
  }

  public void testZip() {
    final String name = getToto();
    try {
      final File f1 = File.createTempFile(name, ".cas");

      final File f2 = File.createTempFile("toto2", ".cas");
      assertNotNull(f1);
      assertNotNull(f2);
      assertTrue(f1.exists());
      assertTrue(f2.exists());
      final File zip = File.createTempFile("toto2", ".zip");
      assertTrue(zip.delete());
      assertTrue(CtuluLibFile.zip(new File[] { f1, f2 }, zip));
      assertTrue(zip.exists());
      final String[] n = CtuluLibFile.getEntries(zip);
      assertNotNull(n);
      assertEquals(2, n.length);
      assertTrue(CtuluLibArray.findObject(n, f1.getName()) >= 0);
      assertTrue(CtuluLibArray.findObject(n, f2.getName()) >= 0);
      assertTrue(f1.delete());
      assertTrue(f2.delete());
    } catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  public void testDouble() {
    assertEquals(CtuluLib.ZERO, CtuluLib.getDouble(0));
    assertEquals(CtuluLib.UN, CtuluLib.getDouble(1));
    assertFalse(CtuluLib.UN.equals(CtuluLib.getDouble(0)));
    assertFalse(CtuluLib.ZERO.equals(CtuluLib.getDouble(1)));
  }
}
