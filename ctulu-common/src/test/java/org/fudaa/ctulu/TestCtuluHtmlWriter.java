/*
 *  @creation     28 nov. 2005
 *  @modification $Date: 2006-09-19 14:36:56 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu;


/**
 * @author fred deniger
 * @version $Id: TestCtuluHtmlWriter.java,v 1.4 2006-09-19 14:36:56 deniger Exp $
 */
public final class TestCtuluHtmlWriter {

  private TestCtuluHtmlWriter(){
  }
  
  

  public static void main(String[] _args){
  CtuluHtmlWriter writer=new CtuluHtmlWriter(true);
  writer.h1Center("Bonjour");
  CtuluHtmlWriter.Tag p=writer.addTag("p");
  writer.close(p, "premier paragraphe");
  writer.para();
  writer.addi18n("deuxieme paragraphe");
  writer.nl();
  writer.addi18n("deuxieme paragraphe 2 ligne");
  System.out.println(writer.getHtml());
  }

}
