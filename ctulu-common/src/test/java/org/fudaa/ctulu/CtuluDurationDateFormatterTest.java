package org.fudaa.ctulu;

import java.text.SimpleDateFormat;
import java.util.Date;
import junit.framework.TestCase;

public class CtuluDurationDateFormatterTest extends TestCase {

  public void testToString() {
    long today = new Date().getTime();
    CtuluDurationDateFormatter toString = new CtuluDurationDateFormatter(null, today);
    int oneDay = 1000 * 24 * 36000;
    //we add one day
    Date res = new Date(today + oneDay);

    String datePattern = "yyyy.MM.dd G 'at' HH:mm:ss z";
    toString.setDatePattern(datePattern);
    SimpleDateFormat  fmt = new SimpleDateFormat(datePattern);
    String formatted = toString.format((double) oneDay / 1000d);
    assertEquals(fmt.format(res), formatted);

  }
}
