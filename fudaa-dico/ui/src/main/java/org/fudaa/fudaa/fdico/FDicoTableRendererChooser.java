/*
 *  @creation     13 mai 2003
 *  @modification $Date: 2006-09-19 15:01:58 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.fdico;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.gui.CtuluCellBooleanRenderer;
import org.fudaa.ctulu.gui.CtuluCellDecorator;
import org.fudaa.ctulu.gui.CtuluCellRenderer;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

import org.fudaa.dodico.dico.DicoComponentVisitor;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;

/**
 * @author deniger
 * @version $Id: FDicoTableRendererChooser.java,v 1.11 2006-09-19 15:01:58 deniger Exp $
 */
public final class FDicoTableRendererChooser implements DicoComponentVisitor {
  public final static class ChoixRenderer extends CtuluCellTextRenderer {
    private DicoDataType.ChoixType data_;
    private boolean isCompletionEnable_;

    public ChoixRenderer() {
      this(null, null);
    }

    public ChoixRenderer(final CtuluCellDecorator _deco) {
      this(_deco, null);
    }

    public ChoixRenderer(final CtuluCellDecorator _deco, final DicoDataType.ChoixType _data) {
      super(_deco);
      setData(_data);
    }

    private void complete(final String _s) {
      if (isCompletionEnable_) {
        final String s = data_.getValue(_s);
        if (s != null) {
          setText(getText() + ": " + s);
        }
      }
    }

    @Override
    public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
                                                  final boolean _isSelected, final boolean _cellHasFocus) {
      super.getListCellRendererComponent(_list, _value, _index, _isSelected, _cellHasFocus);
      complete((String) _value);
      return this;
    }

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                   final boolean _hasFocus, final int _row, final int _column) {
      super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      complete((String) _value);
      return this;
    }

    public void setData(final DicoDataType.ChoixType _data) {
      data_ = _data;
      if ((data_ == null) || (data_.getChoiceValues() == null)) {
        isCompletionEnable_ = false;
      } else {
        isCompletionEnable_ = true;
      }
    }
  }
  CtuluCellBooleanRenderer booleanRenderer_;
  ChoixRenderer choixRenderer_;
  CtuluCellRenderer default_;
  CtuluCellRenderer toReturn_;
  CtuluCellDecorator decorator_;

  private FDicoTableRendererChooser() {}

  private FDicoTableRendererChooser(final CtuluCellDecorator _deco) {
    decorator_ = _deco;
  }

  private void computeChoixRenderer(final DicoDataType.ChoixType _data) {
    if (choixRenderer_ == null) {
      choixRenderer_ = new ChoixRenderer(decorator_);
    }
    choixRenderer_.setData(_data);
    toReturn_ = choixRenderer_;
  }

  private void defaultRenderer() {
    if (default_ == null) {
      default_ = new CtuluCellTextRenderer(decorator_);
    }
    toReturn_ = default_;
  }

  public CtuluCellRenderer getRenderer(final DicoDataType _ent) {
    _ent.accept(this);
    return toReturn_;
  }

  public CtuluCellRenderer getRenderer(final DicoEntite _ent) {
    _ent.accept(this);
    return toReturn_;
  }

  @Override
  public void visitBinaire(final DicoDataType.Binaire _data) {
    if (booleanRenderer_ == null) {
      booleanRenderer_ = new FDicoBooleanCellEditor(decorator_);
    }
    toReturn_ = booleanRenderer_;
  }

  @Override
  public void visitChaine(final DicoDataType.Chaine _data) {
    if (_data.isChoiceEnable()) {
      computeChoixRenderer(_data);
    } else {
      defaultRenderer();
    }
  }

  @Override
  public void visitEntier(final DicoDataType.Entier _data) {
    if (_data.isChoiceEnable()) {
      computeChoixRenderer(_data);
    } else {
      defaultRenderer();
    }
  }

  @Override
  public void visitReel(final DicoDataType.Reel _data) {
    defaultRenderer();
  }

  @Override
  public void visitSimple(final DicoEntite.Simple _ent) {
    _ent.getType().accept(this);
  }

  @Override
  public void visitTableau(final DicoEntite.Tableau _ent) {
    defaultRenderer();
  }

  @Override
  public void visitVecteur(final DicoEntite.Vecteur _ent) {
    defaultRenderer();
  }

  public static TableCellRenderer createCellRenderer(final DicoDataType _ent, final CtuluCellDecorator _deco) {
    return new FDicoTableRendererChooser(_deco).getRenderer(_ent);
  }

  // public static TableCellRenderer createCellRenderer(H2dDicoEntite _ent)
  // {
  // return new TrDicoTableRendererChooser().getRenderer(_ent);
  // }
  public static TableCellRenderer createCellRendererChooser(final FDicoEntiteTableModel _model,
      final CtuluCellDecorator _deco) {
    final FDicoTableRendererChooser ch = new FDicoTableRendererChooser(_deco);
    return new TableCellRenderer() {
      @Override
      public Component getTableCellRendererComponent(final JTable _table, final Object _value,
                                                     final boolean _isSelected, final boolean _hasFocus, final int _row, final int _column) {
        return ch.getRenderer(_model.getEntite(_row)).getTableCellRendererComponent(_table, _value, _isSelected,
            _hasFocus, _row, _column);
      }
    };
  }
}
