/*
 *  @file         FdicoFiltreChooserInterface.java
 *  @creation     5 sept. 2003
 *  @modification $Date: 2007-06-28 09:28:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Observable;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.memoire.bu.BuResource;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluPopupMenu;

import org.fudaa.dodico.dico.DicoEntite;

/**
 * @author deniger
 * @version $Id: FDicoFiltreChooserAbstract.java,v 1.12 2007-06-28 09:28:20 deniger Exp $
 */
public abstract class FDicoFiltreChooserAbstract extends Observable implements FDicoFiltre {
  public final static void filtreList(final List _l, final FDicoFiltre _f) {
    for (int i = _l.size() - 1; i >= 0; i--) {
      if (!_f.accept((DicoEntite) (_l.get(i)))) {
        _l.remove(i);
      }
    }
  }

  public abstract JMenuItem[] getActionFor(DicoEntite _ent);

  public abstract void clearFiltre();

  public abstract boolean isFiltreEmpty();

  public abstract String getName();

  public abstract JPanel buildPanel(Font _f, ListCellRenderer _r);

  protected static final class FiltrePopupMenu implements CtuluPopupListener.PopupReceiver, ActionListener {
    FDicoFiltreChooserAbstract f_;

    protected FiltrePopupMenu(final FDicoFiltreChooserAbstract _f) {
      f_ = _f;
    }

    @Override
    public void popup(MouseEvent _evt) {
      final CtuluPopupMenu menu = new CtuluPopupMenu();
      menu.addMenuItem(FDicoLib.getS("Initialiser le filtre") + CtuluLibString.ESPACE + f_.getName(), "CLEAR",
          BuResource.BU.getToolIcon("effacer"), true).addActionListener(this);
      menu.show(_evt.getComponent(), _evt.getX(), _evt.getY());
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if ("CLEAR".equals(_e.getActionCommand())) {
        f_.clearFiltre();
      }
    }

  }
}
