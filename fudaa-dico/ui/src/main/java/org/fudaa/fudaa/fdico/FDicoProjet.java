/**
 * @creation 25 juin 2003
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.dico.*;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.calcul.FudaaCalculAction;
import org.fudaa.fudaa.commun.calcul.FudaaCalculSupportInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaProjetInformationPanel;
import org.fudaa.fudaa.ressource.FudaaResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;

/**
 * @author deniger
 * @version $Id: FDicoProjet.java,v 1.34 2007-06-05 09:01:13 deniger Exp $
 */
public class FDicoProjet implements FDicoProjetInterface, FudaaCalculSupportInterface {
  /**
   * Creer un projet dico. Change de langue si n�cessaire.
   *
   * @param _file le fichier cas
   * @param _fileFormat le format utilise
   * @param _progress la barre de progression
   * @param _ui l'interface utilisateur
   * @param _dicoMng le manager des fichiers dico.
   */
  public static FDicoProjet init(final File _file, final DicoCasFileFormatVersion _fileFormat,
                                 final ProgressionInterface _progress, final FudaaCommonImplementation _ui, final DicoManager _dicoMng) {
    final FDicoParams.DicoReadData m = FDicoParams.createData(_file, _fileFormat, _progress, _ui, _dicoMng);
    if ((m != null) && (m.src_ != null)) {
      return new FDicoProjet(new FDicoParams(_ui, _file, new DicoParams(m.src_, m.version_)));
    }
    return null;
  }

  private EbliActionSimple actionDiff_;
  BuInformationsDocument infos_;
  FDicoParams params_;
  protected FudaaCalculAction actions_;
  protected FDicoFilleProjet entiteFille_;

  @Override
  public FDicoProjectState getState() {
    return params_.getState();
  }

  protected FDicoProjet(final FDicoParams _param) {
    params_ = _param;
    actions_ = new FudaaCalculAction(this, false);
    actions_.setOnlyOneProcess(false);
  }

  private void validLastModification() {
    if (entiteFille_ != null) {
      entiteFille_.saveLastModification();
    }
  }

  /**
   * Methode a ne pas appele ( utiliser active). Laisse en protected afin de pouvoir etre red�finie.
   */
  protected void activeEntiteFille() {
    if (entiteFille_ == null) {
      buildEntiteFille();
      /*
       * final Dimension dim = getImpl().getMainPanel().getDesktop().getSize(); entiteFille_.setSize((int)
       * (dim.getWidth() * 0.8), (int) (dim.getHeight() * 0.8)); entiteFille_.setLocation(0, 0);
       */
      entiteFille_.pack();
      getImpl().addInternalFrame(entiteFille_);
    } else if (entiteFille_.isClosed()) {
      getImpl().addInternalFrame(entiteFille_);
    } else {
      getImpl().activateInternalFrame(entiteFille_);
    }
  }

  protected void buildEntiteFille() {
    if (entiteFille_ != null) {
      return;
    }
    entiteFille_ = new FDicoFilleProjet(this, getImpl());
  }

  @Override
  public CalculLauncher actionCalcul() {
    final FDicoCalculLocalBuilder r = new FDicoCalculLocalBuilder(getCodeName(), getVersionManager(), getParamsFile());
    final int resp = r.afficheModale(getImpl().getFrame(), FudaaLib.getS("Lancer le calcul"));
    if (CtuluDialogPanel.isOkResponse(resp)) {
      final FDicoCalculLocalOp op = new FDicoCalculLocalOp(this, r.getTelemacExec(r.isParallel()));
      op.setUi(getUI());
      return op;
    }
    return null;
  }

  @Override
  public String getFrameTitle() {
    final File f = getParamsFile();
    return getCodeName() + CtuluLibString.ESPACE + (f == null ? "?" : f.getAbsolutePath());
  }

  @Override
  public String getLastSaveDate() {
    final File f = getParamsFile();
    if (f == null || f.lastModified() <= 0) {
      return null;
    }
    return DateFormat.getDateTimeInstance().format(new Date(f.lastModified()));
  }

  /**
   * Active ( si pas deja fait) ce projet dans l'implemention passee en parametre.
   */
  public void active() {
    activeEntiteFille();
  }

  public void addDocMenuItem(final BuMenuInterface _menu) {
    _menu
        .addMenuItem(FudaaLib.getS("Modifier les informations"), "MODIFY_DOC", BuResource.BU.getIcon("document"), true)
        .addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent _evt) {
            modifyDoc(getImpl().getFrame());
          }

          ;
        });
  }

  public void addParamModelListener(final DicoParamsListener _l) {
    params_.getDicoParams().addModelListener(_l);
  }

  /**
   * Change le titre du projet et met a jour l'historique des actions undo/redo.
   *
   * @param _mng le manager des commandes
   */
  public void changeTitleProjet(final CtuluCommandContainer _mng) {
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    final BuTextField f = new BuTextField();
    f.setText(getTitle());
    pn.setLayout(new BuBorderLayout());
    pn.addEmptyBorder(5);
    pn.add(f, BuBorderLayout.CENTER);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(getImpl().getFrame(), FDicoLib
        .getS("Modifier le titre du projet")))) {
      getDicoParams().setCheckedValue(getTitleEntite(), f.getText(), _mng);
    }
  }

  /**
   * Ferme le projet et ses fenetres.
   */
  public void close() {
    if (entiteFille_ != null) {
      entiteFille_.setVisible(false);
      getImpl().removeInternalFrame(entiteFille_);
      entiteFille_.dispose();
      entiteFille_ = null;
    }
  }

  public FDicoComparator compareWith(final File _n, final ProgressionInterface _progress) {
    return params_.compareWith(_n, _progress);
  }

  public void compareWithInInternalFrame() {
    final File fic = getUI().ouvrirFileChooser(FDicoLib.getS("Le fichier � comparer"), null, false);
    if (fic == null) {
      return;
    }
    final CtuluTaskDelegate task = params_.getImpl().createTask(FDicoLib.getS("Comparaison"));
    task.start(new Runnable() {
      @Override
      public void run() {
        final ProgressionInterface main = task.getMainStateReceiver();
        main.setDesc(FDicoLib.getS("Initialisation comparaison"));
        main.setProgression(0);
        final FDicoComparator c = compareWith(fic, task.getStateReceiver());
        if (c == null) {
          return;
        }
        final BuInternalFrame f = new BuInternalFrame(FDicoLib.getS("Comparaison") + ": " + getParamsFile().getName()
            + " <-> " + fic.getName(), true, true, true, true);
        final BuPanel pn = new BuPanel();
        pn.setLayout(new BuBorderLayout(5, 5));
        pn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        final JTable table = new JTable(c.createTableModel());
        c.createRenderer(table);
        // table.setDefaultRenderer(String.class, new FudaaCellTextRenderer());
        pn.add(new BuScrollPane(table), BuBorderLayout.CENTER);
        f.setContentPane(pn);
        getImpl().addInternalFrame(f);
        f.pack();
        f.setVisible(true);
        f.setSize(500, 400);
        f.setPreferredSize(new Dimension(500, 400));
        getUI().unsetMainMessage();
        getUI().unsetMainProgression();
      }
    });
  }

  /**
   * @return model des entites fichiers pour Jtable
   * @see FDicoParams#getEntiteFileNb()
   */
  public FDicoEntiteFileTableModelInterface createEntiteFileTableModel() {
    return params_.getEntiteFilesTableModel();
  }

  /**
   * @return les actions
   */
  public FudaaCalculAction getCalculActions() {
    return actions_;
  }

  @Override
  public String getCodeExecDir() {
    return getCodeName();
  }

  /**
   * @return le code issu du fichier dico
   */
  @Override
  public String getCodeName() {
    if (params_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    return params_.getDico().getCodeName();
  }

  @Override
  public DicoParams getDicoParams() {
    return params_.getDicoParams();
  }

  public EbliActionInterface getDiffAction() {
    if (actionDiff_ == null) {
      actionDiff_ = new EbliActionSimple(FDicoLib.getS("Comparer"), FudaaResource.FUDAA.getIcon("diff"), "COMPARER") {
        @Override
        public void actionPerformed(final ActionEvent _ae) {
          compareWithInInternalFrame();
        }
      };
      actionDiff_.putValue(Action.SHORT_DESCRIPTION, FDicoLib.getS("Comparer avec un autre fichier cas"));
    }
    return actionDiff_;
  }

  @Override
  public File getDirBase() {
    return params_.getDirBase();
  }

  /**
   * @return les parametres dico issus de fudaa
   */
  public FDicoParams getFDicoParams() {
    return params_;
  }

  @Override
  public final FudaaCommonImplementation getImpl() {
    return params_.getImpl();
  }

  /**
   * @return les infos sur le document
   */
  public final BuInformationsDocument getInformationsDocument() {
    if (infos_ == null) {
      infos_ = new BuInformationsDocument();
    }
    return infos_;
  }

  @Override
  public File getParamsFile() {
    if (getDicoParams() == null) {
      return null;
    }
    return params_.getMainFile();
  }

  /**
   * Ne construit pas le panneau.
   *
   * @return le panneau projet
   */
  public FDicoFilleProjet getProjetFille() {
    return entiteFille_;
  }

  public EbliActionInterface[] getSpecificActions() {
    final EbliActionInterface[] r = new EbliActionInterface[2];
    r[0] = getCalculActions().getCalcul();
    r[1] = getDiffAction();
    return r;
  }

  public FileFormatSoftware getSystemVersion() {
    return getDicoParams().getDicoFileFormatVersion().getSoftVersion();
  }

  @Override
  public String getTitle() {
    if (params_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    return params_.getTitle();
  }

  /**
   * @return l'entite specifiant le titre de l'etude.
   */
  public DicoEntite getTitleEntite() {
    return getDicoParams().getDicoFileFormatVersion().getTitreEntite();
  }

  /**
   * @return l'interface utilisateur
   */
  public FudaaUI getUI() {
    return params_.getUI();
  }

  public DicoVersionManager getVersionManager() {
    return null;
  }

  public void initInformationsDocument(final BuInformationsDocument _info) {
    infos_ = _info;
  }

  public boolean isModified() {
    return isParamsModified() || isUIModified();
  }

  public boolean isParamsModified() {
    if (params_ != null) {
      return params_.isModified();
    }
    return false;
  }

  public boolean isSpaceInParentDir() {
    return getDirBase().getAbsolutePath().trim().indexOf(' ') > 0;
  }

  public boolean isUIModified() {
    return false;
  }

  /**
   * On verifie �galement que le projet ne se trouve pas dans un rep contenant des espace.
   *
   * @return true si les parametres sont valides
   */
  public boolean isDataValide() {
    if (isSpaceInParentDir()) {
      return false;
    }
    return params_.isDataValide();
  }

  /**
   * @param _f la fenetre parente
   */
  public void modifyDoc(final Frame _f) {
    final BuInformationsDocument d = FudaaProjetInformationPanel.editInfoDoc(_f, getInformationsDocument());
    if (d != null) {
      setDocument(d);
    }
  }

  public void removeParamModelListener(final DicoParamsListener _l) {
    params_.removeModelListener(_l);
  }

  @Override
  public boolean save(final ProgressionInterface _prog) {
    validLastModification();
    if (!isModified()) {
      return true;
    }
    if (!isDataValide() && userStoppedSave()) {
      return false;
    }
    if (_prog != null) {
      _prog.setDesc(FDicoLib.getS("Enregistrement fichier cas"));
    }
    if (!params_.save(_prog)) {
      saveAs(_prog);
    }
    if (_prog != null) {
      _prog.setProgression(100);
    }
    return true;
  }

  /**
   * Sauve le projet dans un autre fichier.
   *
   * @param _prog la barre de progression
   * @return true si l'operation n'a pas �t� annul�e
   */
  public boolean saveAs(final ProgressionInterface _prog) {
    validLastModification();
    if (!isDataValide() && userStoppedSave()) {
      return false;
    }
    final String s = FDicoLib.getS("Enregistrement fichier cas");
    final File fic = getUI().ouvrirFileChooser(s, null, true);
    if (fic == null) {
      return false;
    }
    if (_prog != null) {
      _prog.setDesc(s);
    }
    if (!params_.saveAs(fic, _prog)) {
      saveAs(_prog);
    }
    if (_prog != null) {
      _prog.setProgression(100);
    }
    return true;
  }

  private boolean userStoppedSave() {
    return !getUI().question(
        BuResource.BU.getString("Confirmation"),
        FDicoLib.getS("Les param�tres contiennent des erreurs") + ".\n"
            + FDicoLib.getS("Voulez-vous poursuivre l'enregistrement") + '?');
  }

  public File saveCopy(final ProgressionInterface _prog, final File _f) {
    validLastModification();
    final String s = FudaaLib.getS("Enregistrement Copie");
    final File fic = _f == null ? getUI().ouvrirFileChooser(s, null, true) : _f;
    if (fic == null) {
      return null;
    }
    final File res = params_.saveCopy(fic, _prog);
    if (res == null && _f == null) {
      return saveCopy(_prog, null);
    }
    if (_prog != null) {
      _prog.setProgression(100);
    }
    return res;
  }

  /**
   * Met a jour les actions lance/arret calcul.
   *
   * @param _b le nouveau etat du bouton arret du calcul
   */
  public void setActionStopVisible(final boolean _b) {
    if (_b != actions_.isStopCalculVisible()) {
      actions_ = new FudaaCalculAction(this, _b);
    }
  }

  /**
   * @param _doc la nouvelle doc.
   */
  public void setDocument(final BuInformationsDocument _doc) {
    if (_doc != infos_) {
      infos_ = _doc;
    }
  }

  /**
   * Met a jour l'etat des boutons pour le lancement du calcul.
   */
  public void validChange() {
    actions_.setEnableCalcul(isDataValide());
  }
}
