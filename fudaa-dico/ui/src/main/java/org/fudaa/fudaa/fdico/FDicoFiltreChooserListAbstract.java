/*
 *  @file         TrDicoFiltreChooser.java
 *  @creation     20 mai 2003
 *  @modification $Date: 2007-06-28 09:28:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.gui.CtuluPopupListener;

/**
 * @author deniger
 * @version $Id: FDicoFiltreChooserListAbstract.java,v 1.4 2007-06-28 09:28:19 deniger Exp $
 */
public abstract class FDicoFiltreChooserListAbstract extends FDicoFiltreChooserAbstract implements ListModel {
  protected ListSelection list_;

  public abstract void setActifFiltre(int[] _i);

  public abstract void setActifFiltre(int _i);

  public abstract void addFiltre(int _i);

  public abstract void removeFiltre(int _i);

  public abstract boolean isSelectedFiltre(int _i);

  public abstract void updateFromList(int _firstChangedIndex, int _lastChangedIndex);

  public abstract void updateListModel();

  public abstract boolean isMultipleSelection();

  public abstract String getFiltreName(int _i);

  @Override
  public final Object getElementAt(final int _i) {
    return getFiltreName(_i);
  }

  @Override
  public final void addListDataListener(final ListDataListener _l) {}

  @Override
  public final void removeListDataListener(final ListDataListener _l) {}

  public final ListSelectionModel buildListSelectionModel() {
    if (list_ == null) {
      list_ = new ListSelection();
      list_.setLeadAnchorNotificationEnabled(false);
      if (isMultipleSelection()) {
        list_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      } else {
        list_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      }
      updateListModel();
      // addObserver(list_);
    }
    return list_;
  }
  protected class ListSelection extends DefaultListSelectionModel {
    public ListSelection() {}

    @Override
    protected void fireValueChanged(final int _firstIndex, final int _lastIndex, final boolean _isAdjusting) {
      super.fireValueChanged(_firstIndex, _lastIndex, _isAdjusting);
      if (!_isAdjusting) {
        FDicoFiltreChooserListAbstract.this.updateFromList(_firstIndex, _lastIndex);
      }
    }
  }

  @Override
  public JPanel buildPanel(final Font _f, final ListCellRenderer _r) {
    final JList l = new JList(this);
    l.setFont(_f);
    l.setBorder(BorderFactory.createLoweredBevelBorder());
    l.setCellRenderer(_r);
    l.setSelectionModel(buildListSelectionModel());
    final JPanel pn = new BuPanel();
    pn.setLayout(new BuBorderLayout(0, 0));
    pn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), getName()));
    // pn.add(pnButton, BuBorderLayout.NORTH);
    final FDicoFiltreChooserAbstract.FiltrePopupMenu target = new FDicoFiltreChooserAbstract.FiltrePopupMenu(this);
    pn.add(l, BuBorderLayout.CENTER);
    CtuluPopupListener listener = new CtuluPopupListener(target, pn);
    l.addMouseListener(listener);
    return pn;
  }
}
