/**
 * @creation 19 mai 2003
 * @modification $Date: 2007-06-28 09:28:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.fdico;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.ProgressionInterface;

import org.fudaa.dodico.dico.*;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.exec.FudaaEditor;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author deniger
 * @version $Id: FDicoParams.java,v 1.31 2007-06-28 09:28:20 deniger Exp $
 */
public class FDicoParams {

  /**
   * @author fred deniger
   * @version $Id: FDicoParams.java,v 1.31 2007-06-28 09:28:20 deniger Exp $
   */
  public static class DicoReadData {

    public DicoCasInterface src_;

    public DicoCasFileFormatVersion version_;
  }

  /**
   * @author fred deniger
   * @version $Id: FDicoParams.java,v 1.31 2007-06-28 09:28:20 deniger Exp $
   */
  public class EntiteFileTableModel extends AbstractTableModel implements FDicoEntiteFileTableModelInterface,
      DicoParamsListener, DicoParamsChangeStateListener {

    private transient CtuluCommandManager cmdMng_;

    private transient List entites_;

    public EntiteFileTableModel() {
      entites_ = new ArrayList(dicoParams_.getEntiteFileSetAndRequiredList());
    }

    private void updateFor(final DicoEntite _ent) {
      if (_ent.isFileType()) {
        final int index = entites_.indexOf(_ent);
        if (index >= 0) {
          fireTableRowsUpdated(index, index);
        }
      }
    }

    @Override
    public void paramsModified(final DicoParamsChangeState _state) {}

    @Override
    public void paramsStateLoadedEntiteChanged(final DicoParams _params, final DicoEntite _kw) {
      updateFor(_kw);
    }

    @Override
    public void dicoParamsVersionChanged(final DicoParams _cas) {}

    @Override
    public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
      if ((_ent.isFileType()) && (!entites_.contains(_ent))) {
        entites_.add(_ent);
        fireTableDataChanged();
      }
    }

    @Override
    public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _old) {
      if (_ent.isFileType()) {
        if (!_ent.isRequired()) {
          entites_.remove(_ent);
        }
        fireTableDataChanged();
      }
    }

    @Override
    public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _old) {
      updateFor(_ent);
    }

    @Override
    public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {}

    @Override
    public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
      updateFor(_ent);
    }

    @Override
    public void editer(final int _r) {
      final DicoEntite entFile = getEntite(_r);
      final File f = CtuluLibFile.getAbsolutePath(dirBase_, this.getValue(entFile));
      if (f.exists()) {
        FudaaEditor.getInstance().edit(f);
      } else {
        ui_.error(BuResource.BU.getString("Edition"), FudaaLib.getS("Fichier non trouv�"), false);
      }
    }

    @Override
    public String getAbsolutePath(final DicoEntite _ent) {
      return FDicoParams.this.getAbsolutePath(_ent);
    }

    public CtuluCommandManager getCmdMng() {
      return cmdMng_;
    }

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public String getColumnName(final int _c) {
      if (_c == 0) { return CtuluLibString.ESPACE; }
      if (_c == 1) { return FDicoLib.getS("Mot-cl�s"); }
      if (_c == 2) { return BuResource.BU.getString("Fichiers"); }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public DicoEntite getEntite(final int _r) {
      if (_r < entites_.size()) { return (DicoEntite) entites_.get(_r); }
      return null;
    }

    @Override
    public int getRowCount() {
      return entites_.size();
    }

    public String getValue(final DicoEntite _fileEnt) {
      return FDicoParams.this.getValue(_fileEnt);
    }

    @Override
    public Object getValueAt(final int _r, final int _c) {
      if (_c == 0) { return null; }
      if (_c == 1) {
        return getEntite(_r);
      } else if (_c == 2) { return this.getValue(getEntite(_r)); }
      return null;
    }

    @Override
    public int indexOf(final DicoEntite _e) {
      return entites_.indexOf(_e);
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return _columnIndex == 2;
    }

    @Override
    public void remove(final int[] _row) {
      if ((_row != null) && (_row.length > 0)) {
        final DicoEntite[] es = new DicoEntite[_row.length];
        for (int i = _row.length - 1; i >= 0; i--) {
          es[i] = getEntite(_row[i]);
        }
        dicoParams_.removeValues(es, cmdMng_);
      }
    }

    @Override
    public void setCmdMng(final CtuluCommandManager _manager) {
      if ((_manager != null) && (cmdMng_ == null)) {
        cmdMng_ = _manager;
      } else {
        FuLog.warning(new Throwable());
      }
    }

    @Override
    public void setValueAt(final Object _aValue, final int _rowIndex, final int _columnIndex) {
      dicoParams_.setCheckedValue(getEntite(_rowIndex), (String) _aValue, cmdMng_);
    }
  }

  public static DicoReadData createData(final File _file, final DicoCasFileFormatVersion _fileFormat,
      final ProgressionInterface _progress, final FudaaUI _ui, final DicoManager _dicoManager) {
    DicoCasFileFormatVersion ftFormat = _fileFormat;
    // quick test
    if (!ftFormat.createDicoCasReader().testFile(_file)) {
      final int newLanguage = ftFormat.getOtherLanguageIndex();
      ftFormat = _dicoManager.createVersionImpl((DicoCasFileFormat) _fileFormat.getFileFormat(), _fileFormat
          .getVersionName(), newLanguage);
    }
    CtuluIOOperationSynthese op = ftFormat.read(_file, _progress);
    DicoCasInterface inter = (DicoCasInterface) op.getSource();
    final boolean showOnTime = op.getAnalyze().containsErrors();
    if (_ui != null) {
      _ui.manageErrorOperationAndIsFatal(op);
    }
    if (inter == null) { return null; }
    if ((inter.getInputs() == null) || (inter.getInputs().size() == 0)) {
      // test whith the other language
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("try in another language");
      }
      final int newLanguage = ftFormat.getOtherLanguageIndex();
      ftFormat = _dicoManager.createVersionImpl((DicoCasFileFormat) _fileFormat.getFileFormat(), _fileFormat
          .getVersionName(), newLanguage);
      op = ftFormat.read(_file, _progress);
      op.getAnalyze().addInfo(
          FDicoLib.getS(
              "Echec de lecture du fichier des param�tres avec le langage {0}. Tentative avec le langage {1}",
              DicoLanguage.getName(_fileFormat.getDico().getLanguageIndex()), DicoLanguage.getName(ftFormat.getDico()
                  .getLanguageIndex())));
      inter = (DicoCasInterface) op.getSource();
    }
    if (_ui != null && !showOnTime) {
      _ui.manageErrorOperationAndIsFatal(op);
    }
    if (inter == null) { return null; }
    if ((inter.getInputs() == null) || (inter.getInputs().size() == 0)) {
      if (_ui != null) {
        _ui.error(FudaaLib.getS("Format inconnu"), FudaaLib.getS("Le fichier n'est pas reconnu (fichier vide)"), false);
      }
      return null;
    }
    final DicoReadData r = new DicoReadData();
    r.src_ = inter;
    r.version_ = ftFormat;
    return r;
  }

  private String casFileName_;

  private EntiteFileTableModel entiteFileTableModel_;

  private boolean fichierCasIsProjectFile_;

  private boolean isValide_;

  protected DicoParams dicoParams_;

  protected File dirBase_;

  protected long fileLastModified_;

  protected FudaaCommonImplementation ui_;

  final FDicoProjectState state_;

  // BuInformationsDocument idoc_;

  // private List newEntiteFileToSave_;
  /**
   * True si le modele ne correspond plus au fichier.
   */
  protected FDicoParams(final FudaaCommonImplementation _ui, final File _file, final DicoParams _p) {
    dicoParams_ = _p;
    dirBase_ = _file.getParentFile().getAbsoluteFile();
    casFileName_ = _file.getName();
    fileLastModified_ = _file.lastModified();
    ui_ = _ui;
    state_ = new FDicoProjectState(_p.createState());
    manageName();
  }

  public final FudaaCommonImplementation getImpl() {
    return ui_;
  }

  private Map computeNewEntiteFileToSave(final DicoEntite[] _ent) {
    if (_ent == null) { return null; }
    final int n = _ent.length;
    final HashMap r = new HashMap(n);
    for (int i = n - 1; i >= 0; i--) {
      r.put(_ent[i], computeNewEntieFileToSave(_ent[i]));
    }
    return r;
  }

  private boolean copyOrSaveDicoEntiteFile(final DicoEntite _ent, final File _initFile, final File _destFile,
      final ProgressionInterface _progress) { // if modified we have to save
    if (state_.isLoadedAndModified(_ent)) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("save file for " + _ent.getNom() + " in " + _destFile.getAbsolutePath());
      }
      return saveDicoEntiteFileAction(_ent, _destFile, _progress);
    }
    if ((_initFile != null) && _initFile.exists()) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("copy file for " + _ent.getNom() + " in " + _destFile.getAbsolutePath());
      }
      return CtuluLibFile.copyFile(_initFile, _destFile);
    }
    CtuluLibMessage.error(_ent.getNom() + " file " + (_initFile == null ? "?" : _initFile.getAbsolutePath())
        + " unknown source");
    return saveDicoEntiteFileAction(_ent, _destFile, _progress);
  }

  private void errorFileWithFalseAccess(final String _f, final String _err) {
    ui_.error(CtuluResource.CTULU.getString(
        "Le fichier {0} ne peut pas �tre �crit. V�rifier les droits utilisateur de votre syst�me", _f)
        + (_err == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.LINE_SEP + _err)));
  }

  private void errorFileAlreadyUsed(final String _f) {
    ui_.error(CtuluResource.CTULU.getString("Le fichier {0} est d�ja ouvert et ne peut pas �tre �cras�", _f));
  }

  protected boolean isAlreadyOpened(File _f) {
    return false;
  }

  private void manageName() {
    if (casFileName_.indexOf(' ') >= 0) {
      ui_.message(FDicoLib.getS("Le nom du fichier ne doit pas comporter d'espace"));
      isValide_ = false;
    } else {
      isValide_ = true;
    }
  }

  private boolean saveDicoEntiteFileAndUpdate(final DicoEntite _entFile, final File _f,
      final ProgressionInterface _interface) {
    final boolean r = saveDicoEntiteFileAction(_entFile, _f, _interface);
    if (r) {
      state_.setLoaded(_entFile, _f, false, true);
    }
    return r;
  }

  protected String computeNewEntieFileToSave(final DicoEntite _e) {
    return getCasFileName() + CtuluLibString.DOT + _e.getNom().substring(0, 3).toLowerCase();
  }

  /**
   * Method to be redefined in order to save the file _f. return true if save action is ok
   */
  protected boolean saveDicoEntiteFileAction(final DicoEntite _entFile, final File _f,
      final ProgressionInterface _interface) {
    FuLog.warning("had to save " + _entFile.getNom() + " in " + _f.getAbsolutePath());
    return false;
    // To redefine
  }

  protected File changeNameForUsedFile(File _destFile, String _prefix) {
    // non par defaut
    File res = new File(_destFile.getParentFile(), _prefix + "-" + _destFile.getName());
    // aie : il extiste encore
    if (res.exists()) {
      try {
        // nom temporaire
        String name = res.getName();
        res = File.createTempFile(CtuluLibFile.getSansExtension(name), CtuluLibFile.getExtension(name));
      } catch (IOException _evt) {
        FuLog.error(_evt);

      }
      // encore rat�: on annule
      if (res == null || res.exists()) {
        errorFileAlreadyUsed(_destFile.getName());
        return null;
      }
    }
    return res;
  }

  public boolean isUsed(File _f) {
    if (_f == null) return false;
    if (_f.equals(getMainFile())) return true;
    for (Iterator it = dicoParams_.getDicoEntiteFileEnum(); it.hasNext();) {
      if (_f.equals(new File(dirBase_, getValue((DicoEntite) it.next())))) return true;
    }
    return false;
  }

  public boolean willBackupBeDone(DicoEntite _kw) {
    return false;
  }

  public boolean internSaveCopy(final File _n, final ProgressionInterface _progress, final boolean _saveIn) {
    final File destDir = _n.getParentFile();
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("saveCopy in " + _n.getAbsolutePath());
    }
    final String r = CtuluLibFile.canWrite(destDir);
    if (r != null) {
      errorFileWithFalseAccess(_n.getAbsolutePath(), r);
      return false;
    }
    if (!destDir.isDirectory()) {
      ui_.error(destDir + CtuluLibString.ESPACE + FDicoLib.getS("n'est pas un r�pertoire"));
      return false;
    } else if (!destDir.canWrite()) {
      ui_.error(destDir + CtuluLibString.ESPACE + "n'est pas accessible en �criture");
      return false;
    } else {
      int n = dicoParams_.getEntiteFileNb();
      final Map entitesValues = getEntiteValues();
      Map modifiedEntite = null;
      if (_saveIn) {
        modifiedEntite = new HashMap(20);
      }
      final File[] destFiles = new File[n];
      final File[] initFile = new File[n];
      final DicoEntite[] entFile = new DicoEntite[n];
      final StringBuffer filesToDelete = new StringBuffer(200);
      if (_n.exists()) {
        filesToDelete.append("<li>").append(_n.getAbsolutePath()).append("</li>");
      }
      final Iterator fileIt = dicoParams_.getDicoEntiteFileEnum();
      final String casSansExtension = CtuluLibFile.getSansExtension(_n.getName());
      boolean saveInSameDir = destDir.equals(dirBase_);
      boolean willBackup = false;
      for (int i = n - 1; i >= 0; i--) {
        entFile[i] = (DicoEntite) fileIt.next();
        initFile[i] = CtuluLibFile.getAbsolutePath(this.dirBase_, getValue(entFile[i]));
        destFiles[i] = new File(destDir, initFile[i].getName());
        // si le fichier existe
        if (destFiles[i].exists()) {
          // le fichier existe d�ja et est utilise: on essaie de le changer
          boolean addWarn = true;
          if ((destFiles[i].equals(initFile[i]) || isAlreadyOpened(destFiles[i]))) {
            // nom par defaut
            destFiles[i] = changeNameForUsedFile(destFiles[i], casSansExtension);
            if (destFiles[i] == null) return false;
            if (_saveIn && modifiedEntite != null) {
              modifiedEntite.put(entFile[i], destFiles[i].getName());
            }
            addWarn = false;

          }
          if (!CtuluLibFile.isWritable(destFiles[i])) {
            errorFileWithFalseAccess(destFiles[i].getAbsolutePath(), null);
            return false;
          }
          if (addWarn) {
            willBackup |= appendOverwrittenInfos(filesToDelete, entFile[i], destFiles[i]);
          }
        } else if (saveInSameDir) {
          destFiles[i] = changeNameForUsedFile(destFiles[i], casSansExtension);
          if (destFiles[i] == null) return false;
          if (_saveIn && modifiedEntite != null) {
            modifiedEntite.put(entFile[i], destFiles[i].getName());
          }
        }
        final String s = destFiles[i].getName();
        entitesValues.put(entFile[i], s);
        if (_saveIn && modifiedEntite != null) {
          modifiedEntite.put(entFile[i], s);
        }
      }
      final Map newEntiteFile = computeNewEntiteFileToSave(state_.getNewFileEntiteToSave());
      File[] newFile = null;
      DicoEntite[] newEntite = null;
      if (newEntiteFile != null) {
        newFile = new File[newEntiteFile.size()];
        newEntite = new DicoEntite[newEntiteFile.size()];
        int index = 0;
        for (final Iterator it = newEntiteFile.entrySet().iterator(); it.hasNext();) {
          final Map.Entry entry = (Map.Entry) it.next();
          String s = (String) entry.getValue();
          final DicoEntite e = (DicoEntite) entry.getKey();
          newFile[index] = CtuluLibFile.getAbsolutePath(destDir, s);
          File file = newFile[index];
          if (isAlreadyOpened(file) || saveInSameDir) {
            newFile[index] = changeNameForUsedFile(file, casSansExtension);
            if (file == null) return false;
            s = file.getName();
          }
          newEntite[index] = e;
          entitesValues.put(e, s);
          if (_saveIn && modifiedEntite != null) {
            modifiedEntite.put(e, s);
          }
          if (file.exists()) {

            if (!file.canWrite()) {
              errorFileWithFalseAccess(file.getAbsolutePath(), null);
              return false;
            }
            willBackup |= appendOverwrittenInfos(filesToDelete, e, file);
          }
          index++;
        }
      }
      if (filesToDelete.length() > 0) {

        if (userRefuse(filesToDelete, willBackup)) { return false; }
      }
      final CtuluIOOperationSynthese op = getFileFormatVersion().write(_n,
          new DicoCasResult(entitesValues, dicoParams_.getCommentMap()), _progress);
      if (!ui_.manageErrorOperationAndIsFatal(op)) {
        // save action for modified entitefile and update this dico if needed
        n = entFile.length;
        for (int i = 0; i < n; i++) {
          final boolean b = copyOrSaveDicoEntiteFile(entFile[i], initFile[i], destFiles[i], _progress);
          if (_saveIn && modifiedEntite != null) {
            dicoParams_.setValue(entFile[i], (String) modifiedEntite.get(entFile[i]));
            if (b && state_.isLoaded(entFile[i])) {
              state_.setLoaded(entFile[i], destFiles[i], false);
            }
          }
        }
        // new File save and update this dico if needed.
        if (newFile != null) {
          n = newFile.length;
          for (int i = 0; i < n; i++) {
            final DicoEntite dicoEntite = newEntite == null ? null : newEntite[i];
            final boolean er = saveDicoEntiteFileAction(dicoEntite, newFile[i], _progress);
            if (_saveIn && modifiedEntite != null) {
              dicoParams_.setValue(dicoEntite, (String) modifiedEntite.get(dicoEntite));
              if ((state_.isLoaded(dicoEntite)) && er) {
                state_.setLoaded(dicoEntite, newFile[i], false);
              }
              state_.setParamsModified(false);
            }
          }
        }
        return true;
      }
      return false;
    }
  }

  /*
   * private static void addOverwrittenInfo(final StringBuffer _filesToDelete, boolean _willBackupBeDone, File _f) {
   * openLi(_filesToDelete); if (_willBackupBeDone) { openUnderlined(_filesToDelete); }
   * _filesToDelete.append(_f.getAbsolutePath()); if (_willBackupBeDone) { closeUnderlined(_filesToDelete);
   * _filesToDelete.append(" (backup)"); } closeLi(_filesToDelete); }
   */

  private static void closeUnderlined(final StringBuffer _filesToDelete) {
    _filesToDelete.append("</b>");
  }

  private static void closeLi(final StringBuffer _filesToDelete) {
    _filesToDelete.append("</li>");
  }

  private static void openLi(final StringBuffer _filesToDelete) {
    _filesToDelete.append("<li>");
  }

  private static void openUnderlined(final StringBuffer _filesToDelete) {
    _filesToDelete.append("<b>");
  }

  public String canUpdate(final DicoEntite _ent) {
    return dicoParams_.canUpdate(_ent);
  }

  public FDicoComparator compareWith(final File _n, final ProgressionInterface _progress) {
    if (_progress != null) {
      _progress.setDesc(FudaaLib.getS("lecture") + CtuluLibString.ESPACE + _n.getName());
    }
    final CtuluIOOperationSynthese syn = getFileFormatVersion().read(_n, _progress);
    final DicoCasInterface inter = (DicoCasInterface) syn.getSource();
    if (syn.containsSevereError()) {
      ui_.error(FudaaLib.getS("lecture") + CtuluLibString.ESPACE + _n.getName(), syn.getAnalyze().getFatalError(),
          false);
      return null;
    }
    if (_progress != null) {
      _progress.setDesc("Comparaison");
    }
    return new FDicoComparator(getEntiteValues(), casFileName_, inter.getInputs(), _n.getName());
  }

  public boolean contains(final DicoEntite _ent) {
    return dicoParams_.contains(_ent);
  }

  public final File getAbsoluteFileSet(final DicoEntite _f) {
    if (isValueSetFor(_f)) {
      final String r = dicoParams_.getValue(_f);
      if (r == null) { return null; }
      return CtuluLibFile.getAbsolutePath(dirBase_, r);
    }
    return null;
  }

  public final String getAbsolutePath(final DicoEntite _f) {
    final String r = dicoParams_.getValue(_f);
    if (r == null) { return CtuluLibString.EMPTY_STRING; }
    return CtuluLibFile.getAbsolutePath(dirBase_, r).getAbsolutePath();
  }

  public final String getAbsolutePathSet(final DicoEntite _f) {
    if (isValueSetFor(_f)) {
      final String r = dicoParams_.getValue(_f);
      if (r == null) { return CtuluLibString.EMPTY_STRING; }
      return CtuluLibFile.getAbsolutePath(dirBase_, r).getAbsolutePath();
    }
    return null;
  }

  /**
   *
   */
  public Set getAllEntiteWithValuesSet() {
    return dicoParams_.getAllEntiteWithValuesSet();
  }

  /**
   *
   */
  public String getCasFileName() {
    return casFileName_;
  }

  /**
   *
   */
  public long getCasFileTimeLoaded() {
    return fileLastModified_;
  }

  /**
   *
   */
  public DicoModelAbstract getDico() {
    return dicoParams_.getDico();
  }

  public final DicoParams getDicoParams() {
    return dicoParams_;
  }

  /**
   *
   */
  public File getDirBase() {
    return dirBase_;
  }

  public int getEntiteFileNb() {
    return dicoParams_.getEntiteFileNb();
  }

  /**
   * Renvoie un model pour la table des fichiers.
   */
  public final EntiteFileTableModel getEntiteFilesTableModel() {
    if (entiteFileTableModel_ == null) {
      entiteFileTableModel_ = new EntiteFileTableModel();
      dicoParams_.addModelListener(entiteFileTableModel_);
      state_.getProjectState().addChangeStateListener(entiteFileTableModel_);
    }
    return entiteFileTableModel_;
  }

  public Map getEntiteValues() {
    return dicoParams_.getEntiteValues();
  }

  public void getEntiteValues(final Map _m) {
    dicoParams_.getEntiteValues(_m);
  }

  public DicoCasFileFormatVersionAbstract getFileFormatVersion() {
    return dicoParams_.getDicoFileFormatVersion();
  }

  public File getFileSet(final DicoEntite _f) {
    if (isValueSetFor(_f)) {
      final String s = getValue(_f);
      if (s == null) { return null; }
      return CtuluLibFile.getAbsolutePath(getDirBase(), s);
    }
    return null;
  }

  /* *//**
   * @return les informations sur le document
   */
  /*
   * public BuInformationsDocument getInformationsDocument(){ if (idoc_ == null) idoc_ = new BuInformationsDocument();
   * return idoc_; }
   */

  public File getMainFile() {
    return new File(dirBase_, casFileName_);
  }

  /**
   *
   */
  public String getTitle() {
    return dicoParams_.getTitle();
  }

  /**
   *
   */
  public FudaaUI getUI() {
    return ui_;
  }

  /**
   *
   */
  public String getValue(final DicoEntite _e) {
    return dicoParams_.getValue(_e);
  }

  /**
   *
   */
  public int getValuesSize() {
    return dicoParams_.getValuesSize();
  }

  /**
   * @return true si les donn�es ont ete modifiees.
   */
  public boolean isModified() {
    return state_.isModified();
  }

  public boolean isDataValide() {
    return isValide_ && dicoParams_.isDataValide();
  }

  public boolean isValueSetFor(final DicoEntite _ent) {
    return dicoParams_.isValueSetFor(_ent);
  }

  public boolean isValueValideFor(final DicoEntite _ent) {
    return dicoParams_.isValueValideFor(_ent);
  }

  public void removeModelListener(final DicoParamsListener _l) {
    dicoParams_.removeModelListener(_l);
  }

  /**
   * @return false si l'operation n'a pu se derouler a cause d'une impossibilit� d'acc�s ou du choix de l'utilisateur de
   *         ne pas modifier des fichiers existants.
   */
  public boolean save(final ProgressionInterface _progress) {
    if (!isModified()) { return true; }
    final File file = getMainFile();
    final String err = CtuluLibFile.canWrite(file);
    if (err != null) {
      errorFileWithFalseAccess(file.getAbsolutePath(), err);
      return false;
    }
    final Map m = computeNewEntiteFileToSave(state_.getNewFileEntiteToSave());
    // Warn if a new file will be overwritten
    // steering file
    final StringBuffer fileToOverWritten = new StringBuffer();
    if ((!fichierCasIsProjectFile_) && (file.exists())) {
      fileToOverWritten.append("<li>").append(file.getAbsolutePath()).append("</li>");
    }
    boolean willBackup = false;
    if (state_.getEntiteLoadedNb() > 0) {
      for (final Iterator it = state_.getEntiteLoadedEnum(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        final DicoEntiteFileState state = (DicoEntiteFileState) e.getValue();
        final DicoEntite ent = (DicoEntite) e.getKey();
        // the dest file
        File f = getFileSet(ent);
        if ((f == null) && (m != null)) {
          f = CtuluLibFile.getAbsolutePath(dirBase_, (String) m.get(ent));
        }
        if (f == null) {
          FuLog.warning(new Throwable());
        } else {
          if (state.isModified()) {
            if ((((!f.equals(state.getF())) || (f.lastModified() != state.getFileLastTimeModifiedWhenLoad()) || (!state
                .isProjectFile())))
                && (f.exists())) {
              willBackup |= appendOverwrittenInfos(fileToOverWritten, ent, f);
            }
          }
          // file modify on disk
          else if ((!f.equals(state.getF())) || ((f.lastModified() != state.getFileLastTimeModifiedWhenLoad()))) {
            state_.setLoadedModified(ent, true);
            if (f.exists()) {
              if (CtuluLibMessage.DEBUG) {
                CtuluLibMessage.debug("file not good on disk " + f.getAbsolutePath());
              }
              willBackup |= appendOverwrittenInfos(fileToOverWritten, ent, f);
            }
          }
        }
      }
    }
    if (fileToOverWritten.length() > 0 && userRefuse(fileToOverWritten, willBackup)) { return false; }
    if ((m != null) && (m.size() > 0)) {
      for (final Iterator it = m.entrySet().iterator(); it.hasNext();) {
        final Map.Entry entry = (Map.Entry) it.next();
        dicoParams_.setValue((DicoEntite) entry.getKey(), (String) entry.getValue());
      }
    } // end warn
    boolean canContinue = true;
    if (state_.isParamsModified()) {
      final CtuluIOOperationSynthese r = getFileFormatVersion().write(file,
          dicoParams_.createCasWriterInterface(casFileName_), _progress);
      canContinue = !ui_.manageErrorOperationAndIsFatal(r);
    }
    if (!canContinue) { return false; }
    fileToOverWritten.setLength(0);
    if (CtuluLibMessage.INFO) {
      CtuluLibMessage.info("save steering file in " + file.getAbsolutePath());
    }
    fileLastModified_ = file.lastModified();

    fichierCasIsProjectFile_ = true;
    // keywords file save
    if (!state_.isEntiteFilesModified()) {
      state_.setParamsModified(false);
      return true;
    }
    for (final Iterator it = state_.getEntiteLoadedEnum(); it.hasNext();) {
      final Map.Entry entry = (Map.Entry) it.next();
      final DicoEntite entFile = (DicoEntite) entry.getKey();
      final DicoEntiteFileState state = (DicoEntiteFileState) entry.getValue();
      if (state.isModified()) {
        final File f = getFileSet(entFile);
        final String errSave = CtuluLibFile.canWrite(f);
        if (errSave == null) {
          saveDicoEntiteFileAndUpdate(entFile, f, _progress);
        } else {
          errorFileWithFalseAccess(f.getAbsolutePath(), errSave);
        }
      }
    }
    state_.setParamsModified(false);
    return true;
  }

  private boolean appendOverwrittenInfos(final StringBuffer _fileToOverWritten, final DicoEntite _ent, File _f) {
    openLi(_fileToOverWritten);
    boolean res = false;
    if (willBackupBeDone(_ent)) {
      res = true;
      openUnderlined(_fileToOverWritten);
    }
    _fileToOverWritten.append(_f.getAbsolutePath());
    if (willBackupBeDone(_ent)) {
      closeUnderlined(_fileToOverWritten);
      _fileToOverWritten.append(" (backup)");
    }
    closeLi(_fileToOverWritten);
    return res;
  }

  private boolean userRefuse(final StringBuffer _fileToOverWritten, boolean _willbackup) {
    String mes = "<html><body>" + FudaaLib.getS("Voulez-vous remplacer les fichiers suivants") + "?<br><ul>"
        + _fileToOverWritten.toString() + "</ul>";
    if (_willbackup) {
      mes += "<br><b>" + FudaaLib.getS("Des copies des sauvegardes seront effectu�es pour les fichiers en italique");
    }
    return !ui_.question(BuResource.BU.getString("Confirmation"), mes);
  }

  public boolean saveAs(final File _n, final ProgressionInterface _progress) {
    final File casInitfile = new File(dirBase_, casFileName_);
    if (casInitfile.equals(_n)) { return save(_progress); }
    if (isAlreadyOpened(_n)) {
      errorFileAlreadyUsed(_n.getName());
      return false;
    }
    if (internSaveCopy(_n, _progress, true)) {
      dirBase_ = _n.getParentFile();
      casFileName_ = _n.getName();
      manageName();
      fileLastModified_ = _n.lastModified();
      state_.setParamsModified(false);
      fichierCasIsProjectFile_ = true;
      return true;
    }
    return false;
  }

  /**
   * @param _n le fichier de dest
   * @param _progress la barre de progression
   * @return le fichier principal dest de la copie ou null si l'operation a ete annul�e.
   */
  public File saveCopy(final File _n, final ProgressionInterface _progress) {
    if (isAlreadyOpened(_n)) {
      errorFileAlreadyUsed(_n.getName());
      return null;
    }
    if (internSaveCopy(_n, _progress, false)) { return _n; }
    return null;
  }

  public FDicoProjectState getState() {
    return state_;
  }

}
