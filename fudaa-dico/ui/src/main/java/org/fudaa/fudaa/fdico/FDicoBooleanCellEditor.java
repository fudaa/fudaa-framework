/*
 *  @file         FDicoBooleanCellEditor.java
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2006-09-22 15:46:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import org.fudaa.ctulu.gui.CtuluCellBooleanRenderer;
import org.fudaa.ctulu.gui.CtuluCellDecorator;

import org.fudaa.dodico.dico.DicoDataType;


/**
 * @author deniger
 * @version $Id: FDicoBooleanCellEditor.java,v 1.9 2006-09-22 15:46:05 deniger Exp $
 */
public class FDicoBooleanCellEditor extends CtuluCellBooleanRenderer {
  public FDicoBooleanCellEditor () {
    super(null);
  }
  public FDicoBooleanCellEditor (final CtuluCellDecorator _deco) {
    super(_deco);
  }
  @Override
  public void setValue(final Object _value) {
    setSelected(DicoDataType.Binaire.getValue((String)_value));
  }

}
