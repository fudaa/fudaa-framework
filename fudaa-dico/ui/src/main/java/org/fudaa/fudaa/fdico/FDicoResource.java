/*
 *  @file         FDicoResource.java
 *  @creation     16 sept. 2003
 *  @modification $Date: 2006-09-19 15:01:59 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import com.memoire.bu.BuResource;

import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * @author deniger
 * @version $Id: FDicoResource.java,v 1.7 2006-09-19 15:01:59 deniger Exp $
 */
public final class FDicoResource extends FudaaResource {
  public final static FDicoResource FD= new FDicoResource(FudaaResource.FUDAA);
  private FDicoResource(final BuResource _parent) {
    super(_parent);
  }
}
