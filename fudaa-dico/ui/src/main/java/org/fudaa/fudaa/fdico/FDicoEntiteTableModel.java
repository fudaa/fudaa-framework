/**
 *  @creation     7 mai 2003
 *  @modification $Date: 2007-03-30 15:37:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.fdico;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.dico.DicoComponentVisitor;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoEntiteComparator;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.dico.DicoParamsListener;

/**
 * @author deniger
 * @version $Id: FDicoEntiteTableModel.java,v 1.18 2007-03-30 15:37:36 deniger Exp $
 */
public class FDicoEntiteTableModel extends AbstractTableModel implements Observer, DicoParamsListener,
    FDicoFiltreController.FiltreControllerListener {
  private List entitesViewed_;

  private final FDicoProjetInterface projet_;

  private transient FDicoTableSortIndicator sortIndicator_;

  private transient FDicoFiltreController filtreController_;

  private DicoEntiteComparator compEnCours_;

  private ListSelectionModel listSelection_;

  private final transient CtuluCommandManager cmdMng_;

  private CommentForwader commentForwarder_;

  /**
   * @param _model le modele
   * @param _mng le mng de undo/redo
   */
  public FDicoEntiteTableModel(final FDicoProjetInterface _model, final CtuluCommandManager _mng) {
    projet_ = _model;
    projet_.getDicoParams().addModelListener(this);
    cmdMng_ = _mng;
  }
  

  public void setCommentForwarder(final CommentForwader _f) {
    commentForwarder_ = _f;
  }

  public FDicoFiltreController getFiltreController() {
    return filtreController_;
  }
  
  

  public void setFiltreController(final FDicoFiltreController _c) {
    if (filtreController_ != null) {
      filtreController_.setListener(null);
    }
    filtreController_ = _c;
    if (filtreController_ != null) {
      filtreController_.setListener(this);
    }
    initEntiteViewed();
  }

  public String getInvalideMessage(final DicoEntite _e) {
    return projet_.getDicoParams().getInvalidMessage(_e);
  }


  public void setTableSelectionModel(final ListSelectionModel _listSelection) {
    listSelection_ = _listSelection;
  }

  public void setFiltreTemporaire(final FDicoFiltre _f) {
    if (filtreController_ != null) {
      filtreController_.setTempoFiltre(_f);
    }
  }

  public void show() {
    if (entitesViewed_ == null) {
      initEntiteViewed();
    }
  }

  public void setCommentaire(final DicoEntite _e, final String _s) {
    projet_.getDicoParams().setComment(_e, _s);
  }

  public DicoParamsInterface getParams() {
    return projet_.getDicoParams();
  }

  public File getDirBase() {
    return projet_.getDirBase();
  }

  public void removeValues(final DicoEntite[] _ents) {
    if (commentForwarder_ != null) {
      commentForwarder_.saveCurrentComment();
    }
    projet_.getDicoParams().removeCheckedValue(_ents, cmdMng_);
  }

  public CtuluCommandManager getCmdMng() {
    return cmdMng_;
  }

  private void initEntiteViewed() {
    final DicoEntite[] ents = remindSelectedEntite();
    // entitesViewed_= new Vector(params_.getDico().getList());
    entitesViewed_ = projet_.getDicoParams().getViewableEntite();
    if (filtreController_ != null) {
      filtreController_.filtre(entitesViewed_);
    }
    if (compEnCours_ == null) {
      Collections.sort(entitesViewed_);
    } else {
      Collections.sort(entitesViewed_, compEnCours_);
    }
    fireTableDataChanged();
    initSelectionFromRemind(ents);
  }

  public void setSortIndicator(final FDicoTableSortIndicator _sortIndicator) {
    if (sortIndicator_ != null) {
      sortIndicator_.deleteObserver(this);
    }
    sortIndicator_ = _sortIndicator;
    if (sortIndicator_ == null) {
      return;
    }
    sortIndicator_.addObserver(this);
    if (sortIndicator_ == null) {
      setSortedColumn(null);
    } else {
      setSortedColumn(((FDicoTableColumn) sortIndicator_.getSortDescription().getColumnIndex()).getTrIdentifier()
          .createComparator(projet_.getDicoParams(), sortIndicator_.getSortDescription().isDescendingSort()));
    }
  }

  // public abstract Set getAllEntiteSet();
  // public abstract void setValue(DicoEntite _ent, String _value);
  // public abstract String getValue(DicoEntite _ent);
  // public abstract Map getUnModifiableEntiteValue();
  public void chooseRenderer(final int _r, final DicoComponentVisitor _m) {
    getEntite(_r).accept(_m);
  }

  public void setH2dComparator(final DicoEntiteComparator _c) {
    if (_c == null) {
      Collections.sort(entitesViewed_);
    } else {
      Collections.sort(entitesViewed_, _c);
    }
    fireTableDataChanged();
  }

  public final DicoEntite getEntite(final int _row) {
    return (DicoEntite) entitesViewed_.get(_row);
  }

  public int getEntiteRow(final DicoEntite _e) {
    return entitesViewed_.indexOf(_e);
  }

  public String getEntiteValue(final int _row) {
    return projet_.getDicoParams().getValue(getEntite(_row));
  }

  public boolean isValide(final int _row) {
    return projet_.getDicoParams().isValueValideFor(getEntite(_row));
  }

  /**
   *
   */
  @Override
  public final int getRowCount() {
    return entitesViewed_ == null ? 0 : entitesViewed_.size();
  }

  /**
   *
   */
  @Override
  public final Object getValueAt(final int _r, final int _c) {
    return FDicoTableColumnIdentifier.getIdentifer(_c).getValue(_r, this);
  }

  public final String getTooltip(final int _r, final int _c) {
    return FDicoTableColumnIdentifier.getIdentifer(_c).getToolTip(_r, this);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return (getEntite(_rowIndex).isModifiable())
        && FDicoTableColumnIdentifier.getIdentifer(_columnIndex).isEditable(_rowIndex, this);
  }

  public void accept(final int _r, final DicoComponentVisitor _m) {
    getEntite(_r).accept(_m);
  }

  @Override
  public void setValueAt(final Object _aValue, final int _rowIndex, final int _columnIndex) {
    if (FDicoTableColumnIdentifier.getIdentifer(_columnIndex) != FDicoTableColumnIdentifier.COLUMN_VALEUR) {
      return;
    }
    final DicoEntite e = getEntite(_rowIndex);
    if (commentForwarder_ != null) {
      commentForwarder_.saveCurrentComment();
    }
    projet_.getDicoParams().setCheckedValue(e, (String) _aValue, cmdMng_);
  }

  private void setSortedColumn(final DicoEntiteComparator _comp) {
    if (_comp == compEnCours_) {
      return;
    }
    // DicoEntite[] ents= remindSelectedEntite();
    Collections.sort(entitesViewed_, _comp);
    fireTableDataChanged();
    compEnCours_ = _comp;
    // initSelectionFromRemind(ents);
  }

  @Override
  public int getColumnCount() {
    return 3;
  }

  @Override
  public void update(final Observable _obs, final Object _arg) {
    if (_obs == sortIndicator_) {
      setSortedColumn(((FDicoTableColumn) sortIndicator_.getSortDescription().getColumnIndex()).getTrIdentifier()
          .createComparator(projet_.getDicoParams(), sortIndicator_.getSortDescription().isReverse()));
    }
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    dicoParamsModelEntiteEvent(_ent, _ent.getDefautValue());
  }

  private void dicoParamsModelEntiteEvent(final DicoEntite _ent, final String _oldValue) {
    if (entitesViewed_ == null) {
      return;
    }
    final int index = entitesViewed_.indexOf(_ent);
    if (index >= 0) {
      fireTableRowsUpdated(index, index);
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Maj table index " + index);
      }
    }
    filtreController_.updateFiltreModifiedChanged();

    if (projet_.getDicoParams().isEntiteWithComportBehavior(_ent)) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug(getClass().getName() + " Comport in tableModel");
      }
      final DicoEntite[][] oldState = projet_.getDicoParams().getEntiteToHideToTie(_ent, _oldValue);
      final DicoEntite[][] newState = projet_.getDicoParams().getEntiteToHideToTie(_ent,
          projet_.getDicoParams().getValue(_ent));
      final DicoEntite[][] hideView = projet_.getDicoParams().getEntiteToHideChange(oldState[0], newState[0]);
      if (hideView[1].length > 0) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug(getClass().getName() + " Comport -> compute all");
        }
        initEntiteViewed();
      } else {
        final int l = hideView[0].length;
        if (l > 0) {
          if (CtuluLibMessage.DEBUG) {
            CtuluLibMessage.debug(getClass().getName() + " Comport -> hide some keywords");
          }
          boolean remove = false;
          final DicoEntite[] remind = remindSelectedEntite();
          for (int i = 0; i < l; i++) {
            if (entitesViewed_.remove(hideView[0][i])) {
              remove = true;
            }
          }
          if (remove) {
            fireTableDataChanged();
            initSelectionFromRemind(remind);
          }
        }
      }
    }
  }

  /**
   *
   */
  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _old) {
    if (projet_.getDicoParams() == _cas) {
      dicoParamsModelEntiteEvent(_ent, _old);
    }
  }

  /**
   *
   */
  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _old) {
    dicoParamsModelEntiteEvent(_ent, _old);
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {}

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
    final int index = entitesViewed_.indexOf(_ent);
    if (index >= 0) {
      fireTableRowsUpdated(index, index);
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Maj table index " + index);
      }
    }
  }

  @Override
  public void controllerChanged() {
    initEntiteViewed();
  }

  private DicoEntite[] remindSelectedEntite() {
    if (listSelection_.isSelectionEmpty()) {
      return null;
    }
    final int min = listSelection_.getMinSelectionIndex();
    final int max = listSelection_.getMaxSelectionIndex();
    final int maxIndex = entitesViewed_.size();
    final ArrayList ent = new ArrayList(max - min + 1);
    for (int i = min; (i <= max) && (i < maxIndex); i++) {
      if (listSelection_.isSelectedIndex(i)) {
        ent.add(entitesViewed_.get(i));
      }
    }
    final DicoEntite[] r = new DicoEntite[ent.size()];
    ent.toArray(r);
    return r;
  }

  private void initSelectionFromRemind(final DicoEntite[] _r) {
    if (_r == null) {
      listSelection_.clearSelection();
    } else {
      listSelection_.setValueIsAdjusting(true);
      listSelection_.clearSelection();
      listSelection_.setAnchorSelectionIndex(-1);
      listSelection_.setLeadSelectionIndex(-1);
      int index;
      int minIndex = Integer.MAX_VALUE;
      for (int i = _r.length - 1; i >= 0; i--) {
        index = entitesViewed_.indexOf(_r[i]);
        if (index >= 0) {
          listSelection_.addSelectionInterval(index, index);
        }
        if (index < minIndex) {
          minIndex = index;
        }
      }
      listSelection_.setLeadSelectionIndex(minIndex);
      listSelection_.setValueIsAdjusting(false);
    }
  }

  /**
   *
   */
  @Override
  public void controllerMoreRestrictive(final FDicoFiltre _f) {
    final DicoEntite[] ents = remindSelectedEntite();
    FDicoFiltreChooserAbstract.filtreList(entitesViewed_, filtreController_);
    fireTableDataChanged();
    initSelectionFromRemind(ents);
  }

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {}

  public void dicoParamsProjectModifyStateChanged(final DicoParams _cas) {}

  
  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
    filtreController_.setFiltresEmpty();
    fireTableStructureChanged();
    
    
  }
  

  /**
   * @author fred deniger
   * @version $Id: FDicoEntiteTableModel.java,v 1.18 2007-03-30 15:37:36 deniger Exp $
   */
  public interface CommentForwader {

    /**
     * Doit renvoyer les commentaires de l'entite en cours.
     */
    void saveCurrentComment();
  }
}
