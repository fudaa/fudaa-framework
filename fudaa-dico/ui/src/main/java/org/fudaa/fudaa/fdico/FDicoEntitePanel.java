/**
 * @creation 13 mai 2003
 * @modification $Date: 2007-06-28 09:28:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.fdico;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.gui.CtuluCellButtonEditor;
import org.fudaa.ctulu.gui.CtuluCellDecorator;
import org.fudaa.ctulu.gui.CtuluCellRenderer;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluPopupMenu;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.dico.DicoParamsListener;

import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author deniger
 * @version $Id: FDicoEntitePanel.java,v 1.32 2007-06-28 09:28:19 deniger Exp $
 */
public class FDicoEntitePanel extends BuSplit2Pane implements ActionListener, CtuluUndoRedoInterface,
        CtuluPopupListener.PopupReceiver {
  
  public FDicoFiltreController updateFilters() {
    FDicoFiltreController old = tableModel_.getFiltreController();
    final FDicoFiltreController filtre = new FDicoFiltreController();
    findFiltre_ = new FDicoFiltreChooserName();
    filtre.add(findFiltre_);
    filtre.add(new FDicoFiltreChooserModified(tableModel_.getParams()));
    if (filtreNiveau) {
      filtre.add(new FDIcoFiltreChooserMode());
    }
    filtre.add(new FDicoFiltreChooserRubrique(tableModel_.getParams()));
    if (old != null) {
      filtre.setTargetPanel(old.getTargetPanel());
    }
    tableModel_.setFiltreController(filtre);
    return filtre;
  }

  /**
   * @author Fred Deniger
   * @version $Id: FDicoEntitePanel.java,v 1.32 2007-06-28 09:28:19 deniger Exp $
   */
  class DecoratorCellRenderer implements CtuluCellDecorator {
    
    @Override
    public void decore(final JComponent _cmp, final JList _list, final Object _value, final int _index) {
    }
    
    @Override
    public void decore(final JComponent _cmp, final JTable _table, final Object _value, final int _row, final int _col) {
      final boolean b = tableModel_.isValide(_row);
      final DicoEntite e = tableModel_.getEntite(_row);
      if (!b) {
        _cmp.setForeground(b ? Color.black : Color.red);
        return;
      } else if (!e.isModifiable()) {
        _cmp.setForeground(Color.gray);
      } else if (tableModel_.getParams().isValueSetFor(e)) {
        _cmp.setForeground(Color.blue);
      } else {
        _cmp.setForeground(Color.black);
      }
      
      final FDicoTableColumnIdentifier id = (FDicoTableColumnIdentifier) _table.getColumnModel().getColumn(_col)
              .getIdentifier();
      
      if ((id != FDicoTableColumnIdentifier.COLUMN_VALEUR) && (tableModel_.getParams().getComment(e) != null)
              && (_cmp instanceof JLabel)) {
        final String txt = ((JLabel) _cmp).getText();
        ((JLabel) _cmp).setText("<html><u>" + txt + "</u></html>");
      }
    }
  }
  
  final class DecoratorModifiedCellRenderer implements CtuluCellDecorator {
    
    @Override
    public void decore(final JComponent _cmp, final JList _list, final Object _value, final int _index) {
    }
    
    @Override
    public void decore(final JComponent _cmpc, final JTable _table, final Object _value, final int _row, final int _col) {
      final DicoEntite e = tableModel_.getEntite(_row);
      if (tableModel_.getParams().isValueSetFor(e)) {
        _cmpc.setForeground(Color.blue);
      } else {
        _cmpc.setForeground(Color.black);
      }
    }
  }
  
  class DecoratorValueCellRenderer extends DecoratorCellRenderer {
    
    @Override
    public void decore(final JComponent _cmp, final JTable _table, final Object _value, final int _row, final int _col) {
      super.decore(_cmp, _table, _value, _row, _col);
      final DicoEntite e = tableModel_.getEntite(_row);
      String s = tableModel_.getParams().getValue(e);
      if (s == null) {
        _cmp.setToolTipText(CtuluLibString.EMPTY_STRING);
      } else {
        if (e.isFileType()) {
          s = CtuluLibFile.getAbsolutePath(tableModel_.getDirBase(), s).getAbsolutePath();
        }
        _cmp.setToolTipText(s);
      }
    }
  }
  
  private class PanelInfo extends BuTabbedPane implements DicoParamsListener, FDicoEntiteTableModel.CommentForwader {
    
    JButton btComportAffichage_;
    JButton btEditer_;
    JButton btInitialiser_;
    JLabel lbNom_;
    BuTextArea txtAreaHelp_;
    BuTextArea txtComment_;
    JLabel txtDefault_;
    JLabel txtError_;
    JLabel txtRubrique_;
    JLabel txtValue_;
    
    @Override
    public void dicoParamsVersionChanged(final DicoParams _cas) {
      updateFilters();
    }
    
    PanelInfo(final boolean _comment, final boolean _keywordLie) {
      init(_comment, _keywordLie);
    }
    
    @Override
    public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {
      if (_ent == lastEntiteSelected_) {
        refreshValue();
      }
    }
    
    private void clearComportButton() {
      if (btComportAffichage_ != null) {
        btComportAffichage_.setForeground(Color.black);
        btComportAffichage_.setEnabled(false);
        btComportAffichage_.setText(FDicoLib.getS("aucun"));
      }
    }
    
    private void init(final boolean _comment, final boolean _kwTied) {
      final BuPanel main = new BuPanel();
      main.setLayout(new BuBorderLayout(3, 3, true, true));
      final BuPanel pnLabels = new BuPanel();
      final String s = ":";
      pnLabels.setLayout(new BuGridLayout(2, 5, 5));
      pnLabels.add(new BuLabel(FDicoLib.getS("Nom") + s));
      lbNom_ = FDicoLib.buildLabelPlainFont();
      pnLabels.add(lbNom_);
      txtRubrique_ = FDicoLib.buildLabelPlainFont();
      pnLabels.add(new BuLabel(FDicoLib.getS("Rubrique") + s));
      pnLabels.add(txtRubrique_);
      /*
       * pnLabels_.add(new BuLabel(FDicoResource.getS("Type") + s)); txtType_ = FdicoLib.buildLabelPlainFont();
       * pnLabels_.add(txtType_); txtNiveau_ = FdicoLib.buildLabelPlainFont(); pnLabels_.add(new
       * BuLabel(FDicoResource.getS("Niveau") + s)); pnLabels_.add(txtNiveau_); pnLabels_.add(new
       * BuLabel(FDicoResource.getS("Contr�le") + s)); txtControle_ = FdicoLib.buildLabelPlainFont();
       * pnLabels_.add(txtControle_);
       */
      pnLabels.add(new BuLabel(FudaaResource.FUDAA.getString("Valeur par d�faut") + s));
      txtDefault_ = FDicoLib.buildLabelPlainFont();
      pnLabels.add(txtDefault_);
      pnLabels.add(new BuLabel(BuResource.BU.getString("Valeur") + s));
      txtValue_ = FDicoLib.buildLabelPlainFont();
      pnLabels.add(txtValue_);
      pnLabels.add(new BuLabel(BuResource.BU.getString("Erreur") + s));
      txtError_ = FDicoLib.buildLabelPlainFont();
      txtError_.setForeground(Color.red);
      pnLabels.add(txtError_);
      if (_kwTied) {
        final JLabel lb = new BuLabel(FDicoLib.getS("Mot-cl�s d�pendants") + s);
        lb.setVerticalAlignment(SwingConstants.CENTER);
        pnLabels.add(lb);
        btComportAffichage_ = new BuButton();
        btComportAffichage_.addActionListener(FDicoEntitePanel.this);
        clearComportButton();
        pnLabels.add(btComportAffichage_);
      }
      
      main.add(pnLabels, BuBorderLayout.CENTER);
      final JPanel pnButtons = new BuPanel();
      pnButtons.setLayout(new BuVerticalLayout());
      btEditer_ = new BuToolButton();
      btEditer_.setAction(actionEditerSelected_);
      btEditer_.setText(CtuluLibString.EMPTY_STRING);
      btInitialiser_ = new BuToolButton();
      btInitialiser_.setAction(actionDefaultSelected_);
      btInitialiser_.setText(CtuluLibString.EMPTY_STRING);
      pnButtons.add(btInitialiser_);
      pnButtons.add(btEditer_);
      main.add(pnButtons, BuBorderLayout.EAST);
      txtAreaHelp_ = new BuTextArea();
      txtAreaHelp_.setLineWrap(true);
      txtAreaHelp_.setWrapStyleWord(true);
      // txtAreaHelp_.setRows(5);
      BuScrollPane js = new BuScrollPane(txtAreaHelp_);
      js.setPreferredWidth(100);
      js.setPreferredHeight(150);
      js.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      js.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      js.setBorder(BorderFactory.createTitledBorder(BuResource.BU.getString("Aide")));
      main.add(js, BuBorderLayout.SOUTH);
      addTab(FDicoLib.getS("Infos"), main);
      if (!_comment) {
        return;
      }
      txtComment_ = new BuTextArea() {
        @Override
        protected void processFocusEvent(final FocusEvent _evt) {
          if (_evt.getID() == FocusEvent.FOCUS_LOST) {
            saveCurrentComment();
          }
          super.processFocusEvent(_evt);
        }
      };
      txtComment_.setLineWrap(true);
      txtComment_.setWrapStyleWord(true);
      /*
       * txtComment_.setRows(5); txtComment_.setColumns(75);
       */
      txtComment_.setEditable(false);
      txtComment_.setToolTipText(FDicoLib
              .getS("Pour �diter le commentaire, double-cliquer sur le mot-cl� ou sur cette zone"));
      txtComment_.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(final MouseEvent _e) {
          if (_e.getClickCount() == 2 && lastEntiteSelected_ != null) {
            showHelpComment(lastEntiteSelected_);
          }
        }
      });
      final BuPanel pn = new BuPanel();
      pn.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10), BorderFactory
              .createEtchedBorder()));
      pn.setLayout(new BuBorderLayout(2, 2, true, true));
      js = new BuScrollPane(txtComment_);
      js.setPreferredWidth(100);
      js.setPreferredHeight(150);
      pn.add(js, BuBorderLayout.CENTER);
      addTab(FDicoLib.getS("Commentaires"), pn);
    }

    /**
     * utilise pour mettre � jour une valeur.
     */
    private void refreshValue() {
      final DicoParamsInterface params = tableModel_.getParams();
      final String v = params.getValue(lastEntiteSelected_);
      txtValue_.setText(v);
      if (params.isValueValideFor(lastEntiteSelected_)) {
        txtValue_.setForeground(params.isValueSetFor(lastEntiteSelected_) ? Color.blue : Color.black);
      } else {
        txtValue_.setForeground(Color.red);
      }
      txtError_.setText(tableModel_.getInvalideMessage(lastEntiteSelected_));
      if (txtComment_ != null) {
        txtComment_.setText(params.getComment(lastEntiteSelected_));
        // txtComment_.setEditable(params.isValueSetFor(lastEntiteSelected_));
      }
      if ((btComportAffichage_ != null) && params.isEntiteWithComportBehavior(lastEntiteSelected_)) {
        final DicoEntite[] entsHideTie = params.getEntiteToHideToTie(lastEntiteSelected_, v)[1];
        if (entsHideTie.length > 0) {
          btComportAffichage_.setForeground(Color.blue);
          btComportAffichage_.setText(FDicoLib.getS("Voir"));
          btComportAffichage_.setEnabled(true);
        } else {
          clearComportButton();
        }
      } else {
        clearComportButton();
      }
    }
    
    void clearPanel() {
      lbNom_.setText(CtuluLibString.EMPTY_STRING);
      txtAreaHelp_.setText(CtuluLibString.EMPTY_STRING);
      txtRubrique_.setText(CtuluLibString.EMPTY_STRING);
      txtDefault_.setText(CtuluLibString.EMPTY_STRING);
      txtValue_.setText(CtuluLibString.EMPTY_STRING);
      /*
       * txtNiveau_.setText(CtuluLib.EMPTY_STRING); txtControle_.setText(CtuluLib.EMPTY_STRING);
       * txtType_.setText(CtuluLib.EMPTY_STRING);
       */
      txtError_.setText(CtuluLibString.EMPTY_STRING);
      if (txtComment_ != null) {
        txtComment_.setText(CtuluLibString.EMPTY_STRING);
      }
      clearComportButton();
    }
    
    @Override
    public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
      if (_ent == lastEntiteSelected_) {
        refreshValue();
      }
    }
    
    @Override
    public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
      if (_ent == lastEntiteSelected_) {
        refreshValue();
      }
    }
    
    @Override
    public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
      if (_ent == lastEntiteSelected_) {
        refreshValue();
      }
    }
    
    public void dicoParamsProjectModifyStateChanged(final DicoParams _cas) {
    }
    
    public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {
    }
    
    @Override
    public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
      if (_ent == lastEntiteSelected_) {
        refreshValue();
      }
    }

    /**
     * utilise pour mettre � jour le mot-cle selectionne.
     */
    public final void refresh() {
      if (lastEntiteSelected_ == null) {
        clearPanel();
      } else {
        refreshValue();
        lbNom_.setText(lastEntiteSelected_.getNom());
        final StringBuffer help = new StringBuffer();
        final String s = ": ";
        String tx = lastEntiteSelected_.getEntiteTypeDescription();
        if (!CtuluLibString.isEmpty(tx)) {
          help.append(FDicoLib.getS("Type")).append(s);
          help.append(tx).append(CtuluLibString.LINE_SEP);
        }
        tx = lastEntiteSelected_.getControleDescription();
        if (!CtuluLibString.isEmpty(tx)) {
          help.append(FDicoLib.getS("Contr�le")).append(s);
          help.append(tx).append(CtuluLibString.LINE_SEP);
        }
        tx = lastEntiteSelected_.getNiveauDesc();
        if (!CtuluLibString.isEmpty(tx)) {
          help.append(FDicoLib.getS("Niveau")).append(s);
          help.append(tx).append(CtuluLibString.LINE_SEP);
        }
        txtAreaHelp_.setText(help.append(lastEntiteSelected_.getAide()).toString());
        txtAreaHelp_.setCaretPosition(0);
        txtRubrique_.setText(lastEntiteSelected_.getRubrique());
        txtDefault_.setText(lastEntiteSelected_.getDefautValue());
        
      }
    }
    
    @Override
    public final void saveCurrentComment() {
      if ((txtComment_ != null) && (lastEntiteSelected_ != null)) {
        tableModel_.setCommentaire(lastEntiteSelected_, txtComment_.getText());
      }
    }
  }
  EbliActionSimple actionDefaultSelected_;
  EbliActionSimple actionEditerSelected_;
  FDicoTableColumnIdentifier.ColumnManager colMng_;
  FDicoTableColumnIdentifier.ColumnModelWithoutException colModel_;
  FDicoFiltreChooserName findFiltre_;
  DicoEntite lastEntiteSelected_;
  PanelInfo pnInfos_;
  BuPanel pnTable_;
  JTable table_;
  FDicoEntiteTableModel tableModel_;
  JTextField txtFind_;

  /**
   * @param _model le model pour les mot-cles
   */
  public FDicoEntitePanel(final FDicoEntiteTableModel _model) {
    this(_model, null, true, true, true);
  }

  /**
   * @param _model le model pour les mot-cles
   */
  public FDicoEntitePanel(final FDicoEntiteTableModel _model, final boolean _comment, final boolean _kwTied,
          final boolean _filtreNiveau) {
    this(_model, null, _comment, _kwTied, _filtreNiveau);
  }
  
  public FDicoEntitePanel(final FDicoEntiteTableModel _model, final Component _leftUp) {
    this(_model, _leftUp, true, true, true);
  }

  /**
   * Permet d'editer le commentaire et d'afficher l'aide du mot-cle passe en parametres.
   *
   * @param _entite le mot-cle concerne
   */
  protected void showHelpComment(final DicoEntite _entite) {
    if (_entite == null) {
      return;
    }
    final BuTabbedPane pane = new BuTabbedPane();
    BuTextArea txtComment = null;
    if (pnInfos_.txtComment_ != null) {
      txtComment = new BuTextArea();
      txtComment.setLineWrap(true);
      txtComment.setWrapStyleWord(true);
    }
    final BuTextArea txtHelp = new BuTextArea();
    txtHelp.setLineWrap(true);
    txtHelp.setWrapStyleWord(true);
    txtHelp.setText(_entite.getAide());
    if (txtComment != null) {
      pane.addTab(FDicoLib.getS("Commentaires"), null, txtComment, FDicoLib.getS("Commentaires"));
    }
    pane.addTab(FDicoLib.getS("Aide"), null, txtHelp, FDicoLib.getS("Aide"));
    if (txtComment != null) {
      if (tableModel_.getParams().isValueSetFor(_entite)) {
        txtComment.setEditable(true);
        txtComment.setText(tableModel_.getParams().getComment(_entite));
      } else {
        txtComment.setEditable(false);
        txtComment.setText(FDicoLib.getS("Seuls les mot-cl�s modifi�s peuvent �tre comment�s"));
        pane.setSelectedIndex(1);
      }
    }
    final CtuluDialogPanel pn = new CtuluDialogPanel(false);
    pn.setLayout(new BuBorderLayout());
    pn.add(pane, BuBorderLayout.CENTER);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(CtuluLibSwing.getFrameAncestorHelper(this), _entite.getNom()))
            && (txtComment != null) && txtComment.isEditable()) {
      tableModel_.setCommentaire(lastEntiteSelected_, txtComment.getText());
    }
  }
  private boolean filtreNiveau;

  /**
   * @param _model le model pour les mot-cles
   * @param _leftUp un composant qui sera ajoute en haut a gauche (si non null)
   */
  public FDicoEntitePanel(final FDicoEntiteTableModel _model, final Component _leftUp, final boolean _comment,
          final boolean _keywordLie, final boolean _fitreNiveau) {
    this.filtreNiveau = _fitreNiveau;
    tableModel_ = _model;
    final TableCellRenderer valueRenderer = FDicoTableRendererChooser.createCellRendererChooser(tableModel_,
            new DecoratorValueCellRenderer());
    final TableCellEditor valueEditor = FDicoTableEditorChooser.createCellEditorChooser(tableModel_);
    final CtuluCellRenderer defaultRenderer = new CtuluCellTextRenderer(new DecoratorCellRenderer());
    final CtuluCellButtonEditor nomEditor = new CtuluCellButtonEditor(null) {
      @Override
      public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                   final int _row, final int _column) {
        final Component c = super.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
        super.value_ = tableModel_.getEntite(_row);
        return c;
      }
      
      @Override
      protected void doAction() {
        showHelpComment((DicoEntite) super.value_);
      }
    };
    nomEditor.setDoubleClick(true);
    colMng_ = new FDicoTableColumnIdentifier.ColumnManager(defaultRenderer, new CtuluCellTextRenderer(
            new DecoratorModifiedCellRenderer()), valueRenderer, valueEditor, nomEditor);
    colModel_ = colMng_.createColModel();
    table_ = new JTable(tableModel_, colModel_) {
      @Override
      public String getToolTipText(final MouseEvent _event) {
        final java.awt.Point p = _event.getPoint();
        final int rowIndex = rowAtPoint(p);
        final int colIndex = convertColumnIndexToModel(columnAtPoint(p));
        return tableModel_.getTooltip(rowIndex, colIndex);
      }
      
      @Override
      public void tableChanged(final TableModelEvent _evt) {
        if (getCellEditor() != null) {
          getCellEditor().cancelCellEditing();
        }
        final int selectedCol = getSelectedColumn();
        if (selectedCol >= 0) {
          final FDicoTableColumn col = (FDicoTableColumn) getColumnModel().getColumn(selectedCol);
          if (col.getIdentifier() == FDicoTableColumnIdentifier.COLUMN_VALEUR) {
            final int i = getColumnModel().getColumnIndex(FDicoTableColumnIdentifier.COLUMN_NOM);
            setColumnSelectionInterval(i, i);
          }
        }
        super.tableChanged(_evt);
      }
    };
    table_.setColumnSelectionAllowed(false);
    // final MouseListener l = new TableMouseListener();
    CtuluPopupListener l = new CtuluPopupListener(this, table_);
    // table_.addMouseListener(l);
    final ListSelectionModel tableSelectionModel = new DefaultListSelectionModel() {
      @Override
      public void fireValueChanged(final int _f, final int _l, final boolean _isAdjusting) {
        super.fireValueChanged(_f, _l, _isAdjusting);
        if ((!_isAdjusting)) {
          refreshSelectionEntite();
        }
      }
    };
    actionEditerSelected_ = new EbliActionSimple(BuResource.BU.getString("Editer"), BuResource.BU.getIcon("editer"),
            "EDIT") {
      @Override
      public void actionPerformed(final ActionEvent _ae) {
        if (lastEntiteSelected_ == null) {
          return;
        }
        final int i = tableModel_.getEntiteRow(lastEntiteSelected_);
        if (i >= 0
                && table_.editCellAt(i, table_.getColumnModel().getColumnIndex(FDicoTableColumnIdentifier.COLUMN_VALEUR))) {
          final Component c = table_.getEditorComponent();
          c.requestFocus();
          if (c instanceof AbstractButton) {
            ((AbstractButton) c).doClick();
          }
        }
      }
    };
    actionDefaultSelected_ = new EbliActionSimple(FudaaLib.getS("Initialiser"), BuResource.BU.getIcon("enlever"),
            "DELETE") {
      @Override
      public void actionPerformed(final ActionEvent _ae) {
        if ((table_.isEditing()) && (table_.getCellEditor() != null)) {
          table_.getCellEditor().stopCellEditing();
        }
        final ListSelectionModel sel = table_.getSelectionModel();
        final int min = sel.getMinSelectionIndex();
        final int max = sel.getMaxSelectionIndex();
        DicoEntite ent;
        final List entsFin = new ArrayList(max + 2 - min);
        if (table_.getCellEditor() != null) {
          table_.getCellEditor().stopCellEditing();
        }
        for (int i = min; i <= max; i++) {
          ent = tableModel_.getEntite(i);
          if ((ent != null) && (tableModel_.getParams().isValueSetFor(ent))) {
            entsFin.add(ent);
          }
        }
        if (entsFin.size() > 0) {
          tableModel_.removeValues((DicoEntite[]) entsFin.toArray(new DicoEntite[entsFin.size()]));
        }
      }
    };
    actionDefaultSelected_.setEnabled(false);
    actionDefaultSelected_.putValue(Action.SHORT_DESCRIPTION, FDicoLib.getS("Initialiser les mot-cl�s s�lectionn�s"));
    actionEditerSelected_.setEnabled(false);
    actionEditerSelected_.putValue(Action.SHORT_DESCRIPTION, BuResource.BU.getString("Editer"));
    tableSelectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    table_.setSelectionModel(tableSelectionModel);
    tableModel_.setTableSelectionModel(tableSelectionModel);
    final FDicoTableSortIndicator sortIndicator = new FDicoTableSortIndicator(table_, BuResource.BU
            .getIcon("bu_menu_down"), BuResource.BU.getIcon("bu_menu_up"));
    sortIndicator.getSortDescription().setColumnSorted(
            colModel_.findColumnWithIdentifierFast(FDicoTableColumnIdentifier.COLUMN_NOM));
    table_.getTableHeader().addMouseListener(l);
    FDicoFiltreController filtre = updateFilters();
    tableModel_.setSortIndicator(sortIndicator);
    tableModel_.show();
    pnTable_ = new BuPanel();
    pnTable_.setLayout(new BuBorderLayout(5, 5));
    final JScrollPane sp = new BuScrollPane(table_);
    sp.setMinimumSize(new Dimension(200, 300));
    sp.addMouseListener(l);
    pnTable_.add(sp, BuBorderLayout.CENTER);
    pnTable_.setPreferredSize(new Dimension(200, 300));
    final InputMap thisMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK), "INITIALISER");
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "INITIALISER");
    getActionMap().put("INITIALISER", actionDefaultSelected_);
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK), "EDITER");
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.KEY_TYPED), "EDITER");
    getActionMap().put("EDITER", actionEditerSelected_);
    final JPanel left = new BuPanel();
    left.setLayout(new BuVerticalLayout(5, true, false));
    if (_leftUp != null) {
      left.add(_leftUp);
    }
    filtre.setTargetPanel(left);
    // JPanel pn= filtre.setTargetPanel();
    left.setPreferredSize(new Dimension(150, 500));
    setLeftComponent(left);
    setOneTouchExpandable(true);
    pnInfos_ = new PanelInfo(_comment, _keywordLie);
    pnInfos_.refresh();
    // pnTable_.add(pnInfos_, BuBorderLayout.SOUTH);
    pnInfos_.addMouseListener(l);
    tableModel_.getParams().addModelListener(pnInfos_);
    tableModel_.setCommentForwarder(pnInfos_);
    final BuSplit2Pane middle = new BuSplit2Pane(BuSplit3Pane.VERTICAL);
    middle.setMiddleComponent(pnTable_);
    pnInfos_.setMinimumSize(new Dimension(200, 100));
    final BuScrollPane sc = new BuScrollPane(pnInfos_);
    sc.setPreferredHeight(250);
    middle.setBottomComponent(sc);
    middle.resetToPreferredSizes();
    setMiddleComponent(middle);
    setPreferredSize(new Dimension(400, 450));
    resetToPreferredSizes();
  }
  
  protected void saveLastModification() {
    if (table_.getCellEditor() != null) {
      table_.getCellEditor().stopCellEditing();
    }
    pnInfos_.saveCurrentComment();
  }
  
  protected void saveView() {
    for (final Iterator it = FDicoTableColumnIdentifier.IDENTIFIERS.iterator(); it.hasNext();) {
      final FDicoTableColumnIdentifier colId = (FDicoTableColumnIdentifier) it.next();
      final TableColumn col = colModel_.findColumnWithIdentifierFast(colId);
      if (colId.isCloseable()) {
        FDicoPreferences.FD.putBooleanProperty(FDicoTableColumnIdentifier.getColVisiblePref(colId), col != null);
      }
      if (col != null) {
        colId.savePos(colModel_.getColumnIndex(colId));
      }
    }
  }
  
  void refreshSelectionEntite() {
    final ListSelectionModel select = table_.getSelectionModel();
    final int lead = select.getLeadSelectionIndex();
    DicoEntite ent = null;
    if ((!select.isSelectionEmpty()) && (lead >= 0)) {
      table_.scrollRectToVisible(table_.getCellRect(lead, 0, true));
      ent = tableModel_.getEntite(lead);
      actionDefaultSelected_.setEnabled(!ent.isRequired());
      actionEditerSelected_.setEnabled(ent.isModifiable());
    } else {
      actionDefaultSelected_.setEnabled(false);
      actionEditerSelected_.setEnabled(false);
    }
    if (ent != lastEntiteSelected_) {
      if (pnInfos_ != null) {
        pnInfos_.saveCurrentComment();
      }
      lastEntiteSelected_ = ent;
      if (pnInfos_ != null) {
        pnInfos_.refresh();
      }
    }
  }
  
  @Override
  public void popup(MouseEvent _evt) {
    // create the popupmenu
    final CtuluPopupMenu menu = new CtuluPopupMenu();
    menu.add(actionDefaultSelected_.buildMenuItem(EbliComponentFactory.INSTANCE));
    menu.addSeparator(FudaaLib.getS("Filtre"));
    menu.addMenuItem(FudaaLib.getS("Aucun"), "NONE", true).addActionListener(tableModel_.getFiltreController());
    final JMenuItem[] items = tableModel_.getFiltreController().getMenuFiltre(lastEntiteSelected_);
    if (items != null) {
      final int n = items.length;
      for (int i = 0; i < n; i++) {
        menu.add(items[i]);
      }
    }
    menu.addSeparator(FudaaLib.getS("Affichage"));
    for (final Iterator it = FDicoTableColumnIdentifier.IDENTIFIERS.iterator(); it.hasNext();) {
      final FDicoTableColumnIdentifier colId = (FDicoTableColumnIdentifier) it.next();
      if (colId.isCloseable()) {
        final BuCheckBoxMenuItem ch = new BuCheckBoxMenuItem(colId.getNom());
        ch.setActionCommand(colId.getId());
        ch.setSelected(colModel_.findColumnWithIdentifierFast(colId) != null);
        menu.add(ch);
        ch.addActionListener(this);
      }
    }
    menu.show(_evt.getComponent(), _evt.getX(), _evt.getY());
  }
  
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    // mis en place d'un filtre temporaire.
    if ((pnInfos_ != null) && (_evt.getSource() == pnInfos_.btComportAffichage_)) {
      final DicoParamsInterface p = tableModel_.getParams();
      final DicoEntite[] entsTie = p.getEntiteToHideToTie(lastEntiteSelected_, p.getValue(lastEntiteSelected_))[1];
      if (entsTie.length > 0) {
        tableModel_.setFiltreTemporaire(new FDicoFiltreEntiteList(entsTie));
      }
      return;
    }
    // manage the popup menu.
    final String com = _evt.getActionCommand();
    if ("ENREGISTRER".equals(com)) {
      saveView();
    } else {
      final BuCheckBoxMenuItem chIt = (BuCheckBoxMenuItem) _evt.getSource();
      final FDicoTableColumnIdentifier id = FDicoTableColumnIdentifier.getIdentifier(com);
      if (id == null) {
        setVisible(false);
        return;
      }
      if (chIt.isSelected()) {
        colModel_.insertColumn(colMng_.createColumn(id), id.getPrefPosition());
      } else {
        final TableColumn colIndex = colModel_.findColumnWithIdentifierFast(id);
        if (colIndex != null) {
          colModel_.removeColumn(colIndex);
        }
      }
    }
  }
  
  public void activeFind() {
    if (txtFind_ == null) {
      final JComponent leftCp = getLeftComponent();
      int n = leftCp.getComponentCount();
      for (int i = 0; i < n; i++) {
        if ("pnFind".equals(leftCp.getComponent(i).getName())) {
          final JPanel pn = (JPanel) leftCp.getComponent(i);
          n = pn.getComponentCount();
          for (int j = 0; j < n; j++) {
            if ("txtFind".equals(pn.getComponent(j).getName())) {
              txtFind_ = (JTextField) pn.getComponent(j);
              break;
            }
          }
          break;
        }
      }
    }
    if (txtFind_ != null) {
      txtFind_.requestFocus();
    }
  }
  
  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if (tableModel_.getCmdMng() != _source) {
      tableModel_.getCmdMng().clean();
    }
  }
  
  @Override
  public CtuluCommandManager getCmdMng() {
    return tableModel_.getCmdMng();
  }
  
  public JTable getTable() {
    return table_;
  }

  /**
   * *
   */
  @Override
  public void setActive(boolean _b) {
    if (!_b) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("entite panel unactive");
      }
      if (table_.getCellEditor() != null) {
        table_.getCellEditor().stopCellEditing();
        if (pnInfos_ != null) {
          pnInfos_.saveCurrentComment();
        }
      }
      final int i = colModel_.getColumnIndex(FDicoTableColumnIdentifier.COLUMN_NOM);
      table_.setColumnSelectionInterval(i, i);
    }
  }
  
  public void setTxtAreaHelpVisible(boolean _b) {
    if (_b && (pnInfos_ == null)) {
      pnInfos_ = new PanelInfo(true, true);
      pnInfos_.refresh();
      pnTable_.add(pnInfos_, BuBorderLayout.SOUTH);
    } else if ((!_b) && (pnInfos_ != null)) {
      pnInfos_.setVisible(false);
      pnTable_.remove(pnInfos_);
      pnInfos_ = null;
    }
  }
}
