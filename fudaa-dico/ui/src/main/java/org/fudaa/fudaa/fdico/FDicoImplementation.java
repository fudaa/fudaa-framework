/**
 * @creation 6 juin 2003
 * @modification $Date: 2007-06-29 15:09:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionBuAdapter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;

import org.fudaa.dodico.commun.DodicoPreferences;
import org.fudaa.dodico.dico.DicoCasFileFormatAbstract;
import org.fudaa.dodico.dico.DicoCasFileFormatDefault;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoCasInterface;
import org.fudaa.dodico.dico.DicoCasReader;
import org.fudaa.dodico.dico.DicoDynamiqueGenerator;
import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.dico.DicoParams;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author deniger
 * @version $Id: FDicoImplementation.java,v 1.21 2007-06-29 15:09:42 deniger Exp $
 */
public final class FDicoImplementation extends FudaaCommonImplementation {

  DicoDynamiqueGenerator generator_;

  private final static BuInformationsSoftware INFO_SOFT = new BuInformationsSoftware();
  static {

    INFO_SOFT.name = "mox";
    INFO_SOFT.version = "0.0003";
    INFO_SOFT.date = "2005-01-31";
    INFO_SOFT.rights = FudaaLib.getS("Tous droits r�serv�s") + ". CETMEF (c)2003-2004";
    INFO_SOFT.contact = "deniger@users.sourceforge.net";
    INFO_SOFT.license = "GPL2";
    INFO_SOFT.languages = "fr,en";
    INFO_SOFT.http = "http://www.fudaa.fr";
    INFO_SOFT.man = INFO_SOFT.http;
    INFO_SOFT.authors = new String[] { "Fr�d�ric Deniger" };
  }

  public static BuInformationsSoftware getInfoSoft() {
    return INFO_SOFT;
  }

  @Override
  public void actionPerformed(final ActionEvent event) {
    final String action = event.getActionCommand();
    if ("OUVRIR".equals(action)) {
      cmdOuvrirFile(null);
    } else if ("CREER".equals(action)) {
      cmdOuvrirFile(null, true);
    } else if ("ENREGISTRER".equals(action)) {
      new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {

        @Override
        public void act() {
          projetCourant().save(createProgressionInterface(this));
        }
      }.start();
    } else if ("ENREGISTRERSOUS".equals(action)) {
      new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {

        @Override
        public void act() {
          projetCourant().saveAs(createProgressionInterface(this));
        }
      }.start();
    } else if ("ENREGISTRERCOPIE".equals(action)) {
      new CtuluTaskOperationGUI(this, CtuluLibString.EMPTY_STRING) {

        @Override
        public void act() {
          projetCourant().saveCopy(createProgressionInterface(this), null);
        }
      }.start();
    } else if ("FERMER".equals(action)) {
      new CtuluTaskOperationGUI(null, CtuluLibString.EMPTY_STRING) {

        @Override
        public void act() {
          saveAndCloseProjet(createProgressionInterface(this));
        }
      }.start();
    } else {
      super.actionPerformed(event);
    }
  }

  FDicoProjet projetCourant() {
    return proj_;
  }

  /**
   * @return la cle utilisee pour stocker le dernier fichier dico utilise
   */
  public static String getPreferencesDicoFileKey() {
    return "last.dico.file";
  }

  /**
   * @return le fichier enregistree dans les pref ou null si aucune valeur
   */
  public static File getDicoFileInPreferences() {
    final String s = FDicoPreferences.FD.getStringProperty(getPreferencesDicoFileKey(), null);
    return s == null ? null : new File(s);
  }

  /**
   * @param _f le dernier fichier dico lu
   */
  public static void saveDicoFileInPreferences(final File _f) {
    if (_f != null) {
      FDicoPreferences.FD.putStringProperty(getPreferencesDicoFileKey(), _f.getAbsolutePath());
    }
  }

  /**
   * @return le dernier dico lu
   */
  public File getLastDicoFile() {
    if (generator_ != null) {
      return generator_.getDicoPath().toFile();
    }
    return getDicoFileInPreferences();
  }

  protected void loadDicoFile(final File _f, final ProgressionInterface _prog) {
    // si pas encore genere ou different
    if (generator_ == null || !generator_.getDicoPath().equals(_f.toPath())) {
      saveDicoFileInPreferences(_f);
      generator_ = DicoDynamiqueGenerator.loadGenerator(_f, _prog);
      manageAnalyzeAndIsFatal(generator_.getResult());
      if (generator_.getResult() != null && generator_.getResult().containsErrors()) {
        generator_.getResult().printResume();
      }
    }
  }

  FDicoProjet proj_;

  @Override
  public void cmdOuvrirFile(final File _f) {
    cmdOuvrirFile(_f, false);
  }

  public void cmdOuvrirFile(final File _f, final boolean _creation) {
    final File dico = getLastDicoFile();
    final FDicoOpenDialog dial = new FDicoOpenDialog(dico, _f);
    final int r = dial.afficheModale(getFrame(), _creation ? FudaaLib.getS("Cr�er") : FudaaLib.getS("Ouverture"));
    if (CtuluDialogPanel.isOkResponse(r)) {
      new CtuluTaskOperationGUI(this, _creation ? FudaaLib.getS("Cr�er") : FudaaLib.getS("Ouverture")
          + CtuluLibString.ESPACE + dial.getSelectedCasFile().getName()) {

        @Override
        public void act() {
          openProjet(dial.getSelectedDicoFile(), dial.getSelectedCasFile(), dial.getSelectedLanguage(),
              new ProgressionBuAdapter(this));
        }
      }.start();
    }
  }

  @Override
  public boolean confirmExit() {

    if ((proj_ != null)) {
      if (proj_.isModified()) {
        if (!saveAndCloseProjet(null)) {
          return false;
          /*
           * new BuTaskOperation(this, CtuluLibString.EMPTY_STRING) { public void act() {
           * saveAndCloseProjet(createProgressionInterface(this)); } }.start();
           */
        }
      } else {
        proj_.close();
      }
    }
    return super.confirmExit();
  }

  boolean saveAndCloseProjet(final ProgressionInterface _progression) {
    if (proj_ == null) {
      return true;
    }
    if (proj_.isModified()) {
      final int i = CtuluLibDialog.confirmExitIfProjectisModified(getFrame());
      if (i == JOptionPane.CANCEL_OPTION) {
        return false;
      } else if (i != JOptionPane.NO_OPTION) {
        proj_.save(_progression);
      }
    }
    final Runnable r = new Runnable() {

      /**
       * @see java.lang.Runnable#run()
       */
      @Override
      public void run() {
        proj_.close();
        proj_ = null;
        removeInternalFrames(getAllInternalFrames());
      }
    };
    try {
      SwingUtilities.invokeAndWait(r);
    } catch (final InterruptedException e) {} catch (final InvocationTargetException e) {}
    return true;
  }

  void openProjet(final File _dico, final File _steering, final int _language, final ProgressionInterface _prog) {
    setMainMessage(BuResource.BU.getString("Fermeture du projet"));
    if (!saveAndCloseProjet(_prog)) {
      return;
    }
    setMainMessage(BuResource.BU.getString("Ouverture du projet"));
    loadDicoFile(_dico, _prog);
    final DicoModelAbstract modelMotCle = generator_.buildModel(_prog, _language);
    if (modelMotCle == null) {
      return;
    }
    final DicoCasFileFormatAbstract ft = new DicoCasFileFormatDefault(modelMotCle);
    DicoParams param;
    if (_steering != null && _steering.exists()) {
      final DicoCasReader reader = new DicoCasReader((DicoCasFileFormatVersionAbstract) ft
          .getLastVersionInstance(_steering));
      reader.setFile(_steering);
      reader.setProgressReceiver(_prog);
      final CtuluIOOperationSynthese s = reader.read();
      if (super.manageAnalyzeAndIsFatal(s.getAnalyze())) {
        return;
      }
      param = new DicoParams((DicoCasInterface) s.getSource(), (DicoCasFileFormatVersionAbstract) ft
          .getLastVersionInstance(_steering));
    } else {
      param = new DicoParams(null, null, (DicoCasFileFormatVersionAbstract) ft.getLastVersionInstance(_steering));
    }
    if (param != null) {
      proj_ = new FDicoProjet(new FDicoParams(this, _steering, param));
      final Runnable r = new Runnable() {

        /**
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
          proj_.active();
          proj_.getProjetFille().setClosable(false);
          getFrame().setTitle(_steering.getAbsolutePath());
          FDicoImplementation.this.setEnabledForAction("FERMER", true);
          FDicoImplementation.this.setEnabledForAction("ENREGISTRER", true);
          FDicoImplementation.this.setEnabledForAction("ENREGISTRER_SOUS", true);
        }
      };
      SwingUtilities.invokeLater(r);

    }
  }

  @Override
  public void init() {
    super.init();
    FDicoImplementation.this.setEnabledForAction("OUVRIR", true);
    FDicoImplementation.this.setEnabledForAction("CREER", true);
    FDicoImplementation.this.setEnabledForAction("QUITTER", true);
  }

  public void savePref() {
    DodicoPreferences.DODICO.writeIniFile();
    CtuluFavoriteFiles.INSTANCE.saveFavorites();
    FudaaPreferences.FUDAA.writeIniFile();
    FDicoPreferences.FD.writeIniFile();
    EbliPreferences.EBLI.writeIniFile();
  }

  @Override
  public void exit() {
    savePref();
    super.exit();
  }

  @Override
  public void start() {
    super.start();
    final Frame f = getFrame();
    f.setTitle("Fudaa-mox");
    f.setIconImage(FudaaResource.FUDAA.getImage("fudaa-logo"));
    f.repaint();
  }

  @Override
  public BuPreferences getApplicationPreferences() {
    return FDicoPreferences.FD;
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return getInfoSoft();
  }
}
