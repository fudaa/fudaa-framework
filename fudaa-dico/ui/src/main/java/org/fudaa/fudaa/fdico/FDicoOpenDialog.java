/**
 * @creation 7 janv. 2005
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import java.io.File;

import javax.swing.JTextField;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;

import org.fudaa.ctulu.gui.CtuluDialogPanel;

import org.fudaa.dodico.dico.DicoLanguage;

/**
 * @author Fred Deniger
 * @version $Id: FDicoOpenDialog.java,v 1.11 2007-06-05 09:01:13 deniger Exp $
 */
public class FDicoOpenDialog extends CtuluDialogPanel {

  JTextField ftDico_;
  JTextField ftCas_;
  DicoLanguage.LanguageComboBoxModel cbLangModel_;

  protected FDicoOpenDialog(final File _dico, final File _cas) {
    setLayout(new BuGridLayout(2, 5, 5));
    ftDico_ = addLabelFileChooserPanel(FDicoLib.getS("Fichier dico:"), _dico == null ? null : _dico, false, false);
    ftCas_ = addLabelFileChooserPanel(FDicoLib.getS("Fichier cas:"), _cas == null ? null : _cas, false, false);
    cbLangModel_ = new DicoLanguage.LanguageComboBoxModel();
    final BuComboBox cbLang = new BuComboBox();
    cbLang.setModel(cbLangModel_);
    addLabel(BuResource.BU.getString("Langue:"));
    add(cbLang);
  }

  /**
   * @return le langage selectionne
   */
  public int getSelectedLanguage() {
    return cbLangModel_.getSelectedLanguage();
  }

  /**
   * @return le fichier dico
   */
  public File getSelectedDicoFile() {
    return new File(ftDico_.getText()).getAbsoluteFile();
  }

  /**
   * @return le fichier cas.
   */
  public File getSelectedCasFile() {
    return new File(ftCas_.getText()).getAbsoluteFile();
  }

  @Override
  public boolean isDataValid() {
    String s = ftDico_.getText().trim();
    if (s.length() == 0) {
      setErrorText(FDicoLib.getS("Pr�ciser le fichier dico"));
      return false;
    }
    final File f = new File(s);
    if (!f.exists()) {
      setErrorText(FDicoLib.getS("Le fichier dico n'existe pas"));
      return false;
    }
    s = ftCas_.getText().trim();
    if (s.length() == 0) {
      setErrorText(FDicoLib.getS("Pr�ciser le fichier cas"));
      return false;
    }
    return true;
  }
}
