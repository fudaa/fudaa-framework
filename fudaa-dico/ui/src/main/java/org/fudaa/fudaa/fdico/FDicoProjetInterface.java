/**
 *  @creation     10 juin 2003
 *  @modification $Date: 2007-04-30 14:22:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.io.File;

import org.fudaa.ctulu.ProgressionInterface;

import org.fudaa.dodico.dico.DicoParams;

import org.fudaa.fudaa.commun.FudaaProjectInterface;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * @author deniger
 * @version $Id: FDicoProjetInterface.java,v 1.9 2007-04-30 14:22:43 deniger Exp $
 */
public interface FDicoProjetInterface extends FudaaProjectInterface  {
  @Override
  String getTitle();

  DicoParams getDicoParams();

  FDicoProjectState getState();

  FudaaCommonImplementation getImpl();

  @Override
  File getParamsFile();

  File getDirBase();

  String getCodeName();

  String getCodeExecDir();

  /**
   * @param _inter la progression
   * @return true si l'utilisateur n'a pas annul� l'op�ration
   */
  boolean save(ProgressionInterface _inter);
}
