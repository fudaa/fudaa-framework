/*
 *  @file         FDicoFiltreEntiteList.java
 *  @creation     5 sept. 2003
 *  @modification $Date: 2006-09-19 15:02:01 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import java.util.Arrays;

import org.fudaa.dodico.dico.DicoEntite;
public class FDicoFiltreEntiteList implements FDicoFiltre {
  final DicoEntite[] ents_;
  public FDicoFiltreEntiteList(final DicoEntite[] _ent) {
    ents_= _ent;
  }
  @Override
  public boolean accept(final DicoEntite _ent) {
    return Arrays.binarySearch(ents_, _ent) >= 0;
  }
  @Override
  public boolean isSameFiltre(final FDicoFiltre _f) {
    return _f == this;
  }
}
