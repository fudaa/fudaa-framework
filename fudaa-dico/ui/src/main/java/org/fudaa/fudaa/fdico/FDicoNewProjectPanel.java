/*
 * @file FDicoNewProjectPanel.java
 * 
 * @creation 6 nov. 2003
 * 
 * @modification $Date: 2007-03-30 15:37:36 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuResource;

import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.dodico.dico.DicoManager;

/**
 * @author deniger
 * @version $Id: FDicoNewProjectPanel.java,v 1.14 2007-03-30 15:37:36 deniger Exp $
 */
public class FDicoNewProjectPanel extends FDicoChooserPanel {
  BuLabelMultiLine lbWarn_;

  public FDicoNewProjectPanel(final DicoManager _m, final File _gridFile) {
    super(_m, true, true);
    // le fichier cas sera cr��
    pnForFic_.setWriteMode(true);
    if (_gridFile != null) {
      ficGrid_.setText(_gridFile.getAbsolutePath());
      ficGrid_.setToolTipText(_gridFile.getAbsolutePath());
    }
  }

  public File getFile() {
    return getSelectedFile();
  }

  @Override
  public boolean isDataValid() {
    boolean r = super.isDataValid();
    if (r) {
      final String s = fic_.getText();
      if (s != null) {
        final File f = new File(s);
        if (f.exists()) {
          r = CtuluLibDialog.showConfirmation(this, FDicoLib.getS("Ecraser le fichier cas") + " ?", "<html><body>"
              + FDicoLib
                  .getS("Attention: le fichier des param�tres existe d�j�. Il sera �cras� � la prochaine sauvegarde.")
              + "<br>" + BuResource.BU.getString("Voulez-vous vraiment continuer?") + "</body></html>");
        }
      }

    }
    return r;
  }

  @Override
  protected JComponent[] getDicoManagementUI() {
    List<JComponent> components = new ArrayList<JComponent>();

    this.addComponentsToList(components, this.getDictionnaireUI());
    this.addComponentsToList(components, this.getDicoChoiceUI());
    this.addComponentsToList(components, this.getLangueUI());

    return components.toArray(new JComponent[0]);
  }

  @Override
  public String getVersionSelected() {
    if (this.versionButton.isSelected()) {
      return super.getVersionSelected();
    } else if (getDicoFile() != null) { return getDicoFile().getName(); }
    return null;
  }

  public File getDicoFile() {
    if (this.fileButton.isSelected()) {
      return this.filePanel.getFile();
    } else {
      return null;
    }
  }
}
