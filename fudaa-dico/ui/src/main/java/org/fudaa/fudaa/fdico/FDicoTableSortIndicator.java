/*
 *  @file         TrDicoTableSortIndicator.java
 *  @creation     14 mai 2003
 *  @modification $Date: 2007-05-04 13:59:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.Observable;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

/**
 * @author deniger
 * @version $Id: FDicoTableSortIndicator.java,v 1.10 2007-05-04 13:59:04 deniger Exp $
 */
public class FDicoTableSortIndicator extends Observable {
  private class HeaderRenderer extends CtuluCellTextRenderer {
    private final Icon ascendingIcon_;
    private final Icon descendingIcon_;
    private final Border normalBorder_ = UIManager.getBorder("TableHeader.cellBorder");
    private final Border selectedBorder_ = UIManager.getBorder("Table.focusCellHighlightBorder");

    public HeaderRenderer(final Icon _ascendingIcon, final Icon _descendingIcon) {
      ascendingIcon_ = _ascendingIcon;
      descendingIcon_ = _descendingIcon;
      setHorizontalAlignment(SwingConstants.CENTER);
      setBorder(normalBorder_);
      setForeground(tableHeader_.getForeground());
      setBackground(tableHeader_.getBackground());
      setFont(tableHeader_.getFont());
    }

    private Icon getSortIcon(final TableColumn _c) {
      if (_c != getSortDescription().getColumnIndex()) {
        return null;
      }
      return getSortDescription().getSortOrder() == DESCENDING_SORT ? descendingIcon_ : ascendingIcon_;
    }

    private void setSelected(final boolean _b) {
      setBorder(_b ? selectedBorder_ : normalBorder_);
      repaint();
    }

    /**
     *
     */
    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                   final boolean _hasFocus, final int _row, final int _column) {
      final TableColumn col = tableHeader_.getColumnModel().getColumn(_column);
      setIcon(getSortIcon(col));
      setText((String) _value);
      if (col == pressedColumn_) {
        setSelected(true);
      } else {
        setSelected(false);
      }
      return this;
    }
  }
  public final class SortDescription {
    private TableColumn column_;
    private int sortOrder_;

    public SortDescription() {
      sortOrder_ = ASCENDING_SORT;
    }

    private void updateTable() {
      tableHeader_.resizeAndRepaint();
    }

    /**
     *
     */
    public TableColumn getColumnIndex() {
      return column_;
    }

    /**
     *
     */
    public int getSortOrder() {
      return sortOrder_;
    }

    public boolean isDescendingSort() {
      return isReverse();
    }

    public boolean isReverse() {
      return sortOrder_ == DESCENDING_SORT;
    }

    public boolean isSortEnable() {
      return column_ != null;
    }

    public void setColumnSorted(final TableColumn _c) {
      setSort(_c, ASCENDING_SORT);
    }

    public void setSort(final TableColumn _columnToSort, final int _order) {
      if (_columnToSort == column_) {
        setSortOrder(_order);
      } else {
        FDicoTableSortIndicator.this.internSetInternalChanged();
        column_ = _columnToSort;
        sortOrder_ = _order;
        notifyObservers(CHANGE_COLUMN);
        updateTable();
      }
    }

    public void setSortOrder(final int _i) {
      if (_i != sortOrder_) {
        toggleSortOrder();
      }
    }

    public void toggleSortOrder() {
      if (sortOrder_ == ASCENDING_SORT) {
        sortOrder_ = DESCENDING_SORT;
      } else {
        sortOrder_ = ASCENDING_SORT;
      }
      FDicoTableSortIndicator.this.internSetInternalChanged();
      notifyObservers(CHANGE_SORT);
      updateTable();
    }
  }
  public final static int ASCENDING_SORT = 1;
  public final static String CHANGE_COLUMN = "COLUMN_CHANGE";
  public final static String CHANGE_SORT = "SORT_CHANGE";
  public final static int DESCENDING_SORT = -1;
  private HeaderRenderer headerRenderer_;
  private SortDescription sort_;

  protected TableColumn pressedColumn_;

  // private final static Icon NO_ICON = null;
  protected JTableHeader tableHeader_;

  public FDicoTableSortIndicator(final JTable _table, final Icon _ascendingIcon, final Icon _descendingIcon) {
    tableHeader_ = _table.getTableHeader();
    headerRenderer_ = new HeaderRenderer(_ascendingIcon, _descendingIcon);
    initTable();
  }

  private void initTable() {
    sort_ = new SortDescription();
    final MouseInputAdapter listener = new MouseInputAdapter() {
      private boolean valideAction_;

      private void validateMouseEvent(MouseEvent _evt) {
        if ((_evt.getModifiers() != InputEvent.BUTTON1_MASK) || (tableHeader_.getResizingColumn() != null)) {
          valideAction_ = false;
        }
      }

      @Override
      public void mouseDragged(MouseEvent _evt) {
        valideAction_ = false;
      }

      @Override
      public void mousePressed(MouseEvent _evt) {
        valideAction_ = true;
        TableColumnModel model = tableHeader_.getColumnModel();
        int s = model.getColumnIndexAtX(_evt.getX());
        pressedColumn_ = model.getColumn(s);
        tableHeader_.repaint(tableHeader_.getHeaderRect(s));
        validateMouseEvent(_evt);
      }

      @Override
      public void mouseReleased(MouseEvent _evt) {
        TableColumnModel model = tableHeader_.getColumnModel();
        int colSelectedIndex = model.getColumnIndexAtX(_evt.getX());
        TableColumn col = model.getColumn(colSelectedIndex);
        if (col != pressedColumn_) {
          valideAction_ = false;
        }
        pressedColumn_ = null;
        tableHeader_.repaint(tableHeader_.getHeaderRect(colSelectedIndex));
        validateMouseEvent(_evt);
        if (valideAction_) {
          if (col == getSortDescription().getColumnIndex()) {
            getSortDescription().toggleSortOrder();
          } else {
            getSortDescription().setColumnSorted(col);
          }
        }
        valideAction_ = false;
      }
    };
    tableHeader_.addMouseListener(listener);
    tableHeader_.addMouseMotionListener(listener);
    tableHeader_.setDefaultRenderer(headerRenderer_);
    pressedColumn_ = null;
  }

  /**
   * utilise uniquement par la classe SortDescription .Cette methode sert a modifier la visible de la methode
   * Observable.setChanged et de permettre a l'inner class d'acceder a cette methode sans emulation (performance).
   */
  void internSetInternalChanged() {
    super.setChanged();
  }

  public SortDescription getSortDescription() {
    return sort_;
  }

  public JTable getTable() {
    return tableHeader_.getTable();
  }

  public void setNoSort() {
    sort_.setColumnSorted(null);
  }

  public void setSort(final SortDescription _s) {
    sort_ = _s;
  }

  public void setTable(final JTable _t) {
    tableHeader_ = _t.getTableHeader();
    initTable();
  }
}
