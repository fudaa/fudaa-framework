/**
 *  @creation     5 sept. 2003
 *  @modification $Date: 2006-09-19 15:01:59 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.fdico;

import javax.swing.JMenuItem;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.dico.DicoEntite;

import org.fudaa.fudaa.commun.FudaaLib;
/**
 * @author Fred Deniger
 * @version $Id: FDIcoFiltreChooserMode.java,v 1.10 2006-09-19 15:01:59 deniger Exp $
 */
public final class FDIcoFiltreChooserMode extends FDicoFiltreChooserSimple {

  /**
   * Filtre selon le mode des mot-cl�s
   */
  public FDIcoFiltreChooserMode() {
    super(CtuluLibString.enTableau(DicoEntite.NIVEAU));
  }

  /**
   * @see org.fudaa.fudaa.fdico.FDicoFiltreChooserAbstract#getName()
   */
  @Override
  public String getName(){
    return FudaaLib.getS("Mode");
  }

  /**
   * @see org.fudaa.fudaa.fdico.FDicoFiltre#accept(org.fudaa.dodico.dico.DicoEntite)
   */
  @Override
  public boolean accept(final DicoEntite _ent){
    if (indexSelected_ >= 0) {
      int indic = _ent.getNiveau();
      indic = indic < 0 ? -indic : indic;
      return (indic <= indexSelected_ + 1) ? true : false;
    }
    return true;
  }

  /**
   * @see org.fudaa.fudaa.fdico.FDicoFiltreChooserAbstract#getActionFor(org.fudaa.dodico.dico.DicoEntite)
   */
  @Override
  public JMenuItem[] getActionFor(final DicoEntite _ent){
    return null;
  }
}
