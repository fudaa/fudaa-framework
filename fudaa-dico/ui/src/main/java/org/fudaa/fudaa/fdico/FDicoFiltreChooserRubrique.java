/*
 *  @creation     5 sept. 2003
 *  @modification $Date: 2006-09-19 15:02:02 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.BitSet;

import javax.swing.JMenuItem;

import com.memoire.bu.BuMenuItem;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParamsInterface;

import org.fudaa.fudaa.commun.FudaaLib;

public final class FDicoFiltreChooserRubrique extends FDicoFiltreChooserListAbstract implements ActionListener {
  String[] rubriques_;
  boolean[] rubriquesSelected_;
  int nbFiltreActifs_;

  // TObjectIntHashMap stringInt_;
  public FDicoFiltreChooserRubrique(final DicoParamsInterface _model) {
    rubriques_ = _model.getDico().getRubriques();
    rubriquesSelected_ = new boolean[rubriques_.length];
    nbFiltreActifs_ = 0;
  }

  @Override
  public boolean isSelectedFiltre(final int _i) {
    return rubriquesSelected_[_i];
  }

  @Override
  public JMenuItem[] getActionFor(final DicoEntite _ent) {
    if (_ent == null) {
      return null;
    }
    final String rub = _ent.getRubrique();
    final int i = CtuluLibArray.findObject(rubriques_, rub);
    if (i < 0) {
      FuLog.trace(getClass().getName() + " probleme filtre rubrique");
    } else {
      final JMenuItem[] r = new JMenuItem[1];
      r[0] = new BuMenuItem(FudaaLib.getS("Rubrique") + ": " + rub);
      r[0].addActionListener(this);
      r[0].setActionCommand(CtuluLibString.getString(i));
      return r;
    }
    return null;
  }

  @Override
  public void clearFiltre() {
    if (isFiltreEmpty()) {
      return;
    }
    for (int i = rubriques_.length - 1; i >= 0; i--) {
      rubriquesSelected_[i] = false;
    }
    nbFiltreActifs_ = 0;
    setChanged();
    notifyObservers(Boolean.FALSE);
    updateListModel();
  }

  @Override
  public String getFiltreName(final int _i) {
    return rubriques_[_i];
  }

  @Override
  public String getName() {
    return FudaaLib.getS("Rubriques");
  }

  @Override
  public boolean isFiltreEmpty() {
    return nbFiltreActifs_ == 0;
  }

  @Override
  public boolean isMultipleSelection() {
    return true;
  }

  @Override
  public void setActifFiltre(final int _newFiltreIndex) {
    if (_newFiltreIndex >= 0) {
      if (isFiltreEmpty()) {
        addFiltre(_newFiltreIndex);
      } else {
        final BitSet s = new BitSet(_newFiltreIndex);
        s.set(_newFiltreIndex);
        setActifFiltre(s);
      }
    }
  }

  public void setActifFiltre(final BitSet _s) {
    updateActifFiltre(_s, 0, rubriques_.length - 1);
  }

  private boolean internAddFiltre(final int _i) {
    if (_i < 0) {
      return false;
    }
    if (!rubriquesSelected_[_i]) {
      rubriquesSelected_[_i] = true;
      nbFiltreActifs_++;
      return true;
    }
    return false;
  }

  private boolean internRemoveFiltre(final int _i) {
    if (_i < 0) {
      return false;
    }
    if (rubriquesSelected_[_i]) {
      rubriquesSelected_[_i] = false;
      nbFiltreActifs_--;
      return true;
    }
    return false;
  }

  @Override
  public void addFiltre(final int _i) {
    if (internAddFiltre(_i)) {
      setChanged();
      updateListModel();
      notifyObservers(Boolean.FALSE);
    }
  }

  @Override
  public void removeFiltre(final int _i) {
    if (internRemoveFiltre(_i)) {
      setChanged();
      updateListModel();
      notifyObservers(Boolean.TRUE);
    }
  }

  @Override
  public void setActifFiltre(final int[] _indexs) {
    final BitSet s = new BitSet(rubriques_.length);
    for (int i = _indexs.length - 1; i >= 0; i--) {
      s.set(_indexs[i]);
    }
    updateActifFiltre(s, 0, rubriques_.length - 1);
  }

  public void updateActifFiltre(final BitSet _s, final int _minIndex, final int _maxIndex) {
    updateActifFiltre(_s, _minIndex, _maxIndex, true);
  }

  public void updateActifFiltre(final BitSet _s, final int _minIndex, final int _maxIndex, final boolean _updateList) {
    final int oldNb = nbFiltreActifs_;
    Boolean moreRestrictive = Boolean.TRUE;
    for (int i = _minIndex; i <= _maxIndex; i++) {
      if (_s.get(i)) {
        if (internAddFiltre(i)) {
          moreRestrictive = Boolean.FALSE;
          setChanged();
        }
      } else {
        if (internRemoveFiltre(i)) {
          setChanged();
        }
      }
    }
    if ((nbFiltreActifs_ == 0) && (oldNb > 0)) {
      moreRestrictive = Boolean.FALSE;
    }
    notifyObservers(moreRestrictive);
    if (_updateList) {
      updateListModel();
    }
  }

  @Override
  public int getSize() {
    return rubriques_.length;
  }

  @Override
  public boolean accept(final DicoEntite _ent) {
    if (isFiltreEmpty()) {
      return true;
    }
    for (int i = rubriques_.length - 1; i >= 0; i--) {
      if ((rubriquesSelected_[i]) && (_ent.getRubrique().equals(rubriques_[i]))) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isSameFiltre(final FDicoFiltre _f) {
    if (_f instanceof FDicoFiltreChooserRubrique) {
      final FDicoFiltreChooserRubrique r = (FDicoFiltreChooserRubrique) _f;
      return (Arrays.equals(r.rubriques_, rubriques_)) && (rubriquesSelected_ == r.rubriquesSelected_);
    }
    return false;
  }

  @Override
  public void updateFromList(final int _firstChangedIndex, final int _lastChangedIndex) {
    final BitSet s = new BitSet(_lastChangedIndex);
    for (int i = _firstChangedIndex; i <= _lastChangedIndex; i++) {
      if (list_.isSelectedIndex(i)) {
        s.set(i);
      }
    }
    updateActifFiltre(s, _firstChangedIndex, _lastChangedIndex, false);
  }

  @Override
  public void updateListModel() {
    if (isFiltreEmpty()) {
      list_.clearSelection();
      return;
    }
    list_.setValueIsAdjusting(true);
    final int a = list_.getAnchorSelectionIndex();
    final int l = list_.getLeadSelectionIndex();
    list_.setAnchorSelectionIndex(-1);
    list_.setLeadSelectionIndex(-1);
    for (int i = rubriques_.length - 1; i >= 0; i--) {
      if (rubriquesSelected_[i]) {
        if (!list_.isSelectedIndex(i)) {
          list_.addSelectionInterval(i, i);
        }
      } else if (list_.isSelectedIndex(i)) {
        list_.removeSelectionInterval(i, i);
      }
    }
    if (list_.getAnchorSelectionIndex() != a) {
      list_.setAnchorSelectionIndex(a);
    }
    if (list_.getLeadSelectionIndex() != l) {
      list_.setLeadSelectionIndex(l);
    }
    list_.setValueIsAdjusting(false);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    try {
      final int i = Integer.parseInt(_e.getActionCommand());
      if (i >= 0) {
        setActifFiltre(i);
      }
    } catch (final NumberFormatException _ex) {}
  }
}
