/*
 *  @file         FDicoComparator.java
 *  @creation     16 juin 2003
 *  @modification $Date: 2007-05-04 13:59:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuResource;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

import org.fudaa.dodico.dico.DicoEntite;

/**
 * @author deniger
 * @version $Id: FDicoComparator.java,v 1.19 2007-05-04 13:59:04 deniger Exp $
 */
public class FDicoComparator {

  Map                  entiteBase_;
  String               titleBase_;
  Map                  entiteCompare_;
  String               titleCompare_;
  ComparaisonField[]   compFields_;
  ProgressionInterface progression_;
  BuIcon               ajout_ = BuResource.BU.getIcon("oui");
  BuIcon               supp_  = BuResource.BU.getIcon("non");
  BuIcon               mod_   = BuResource.BU.getIcon("editer");

  /**
   * @param _base les mots-cl�s de base a comparer
   * @param _titleBase le titre pour les mot-cl�s de base
   * @param _compare les mot-cl�s de dest a comparer
   * @param _titleCompare le titre des mot-cl�s de dest
   */
  public FDicoComparator(
  final Map _base,
  final String _titleBase,
  final Map _compare,
  final String _titleCompare) {
    entiteBase_ = _base;
    entiteCompare_ = _compare;
    titleBase_ = _titleBase;
    titleCompare_ = _titleCompare;
    compare();
  }

  public void setProgressionInterface(final ProgressionInterface _prog) {
    progression_ = _prog;
  }

  private void compare() {
    final ArrayList compTemp = new ArrayList(entiteBase_.size());
    final List compareValues = new ArrayList(entiteCompare_.keySet());
    DicoEntite ent;
    String value;
    String valueCompare;
    //int index = 0;
    final int pourc = 0;
    final boolean afficheAvance = (progression_ == null ? false : true);
    if (afficheAvance) {
      progression_.setProgression(pourc);
    }
    final int nb = (int) (entiteBase_.size() * 1.5);
    final ProgressionUpdater up = new ProgressionUpdater(progression_);
    up.setValue(2, nb, 0, 50);
    up.majProgessionStateOnly();
    for (final Iterator it = entiteBase_.keySet().iterator(); it.hasNext();) {
      //index++;
      ent = (DicoEntite) it.next();
      value = (String) entiteBase_.get(ent);
      if (entiteCompare_.containsKey(ent)) {
        compareValues.remove(ent);
        valueCompare = (String) entiteCompare_.get(ent);
        if (!valueCompare.equals(value)) {
          final ComparaisonField c = new ComparaisonField();
          c.ent_ = ent;
          c.valueBase_ = value;
          c.valueCompare_ = valueCompare;
          c.state_ = ComparaisonField.CHANGE;
          compTemp.add(c);
        }
      } else {
        final ComparaisonField c = new ComparaisonField();
        c.ent_ = ent;
        c.valueBase_ = value;
        c.valueCompare_ = null;
        c.state_ = ComparaisonField.ADD;
        compTemp.add(c);
      }
      up.majAvancement();
    }
    for (final Iterator it = compareValues.iterator(); it.hasNext();) {
      ent = (DicoEntite) it.next();
      final ComparaisonField c = new ComparaisonField();
      c.ent_ = ent;
      c.valueBase_ = null;
      c.valueCompare_ = (String) entiteCompare_.get(ent);
      c.state_ = ComparaisonField.REMOVE;
      compTemp.add(c);
    }
    compFields_ = new ComparaisonField[compTemp.size()];
    compTemp.toArray(compFields_);
    Arrays.sort(compFields_, new ComparaisonFieldComparator());
    if (afficheAvance) {
      progression_.setProgression(100);
    }
  }

  /**
   * Un comparateur pour les noms de mots-cl�s.
   * @author Fred Deniger
   * @version $Id: FDicoComparator.java,v 1.19 2007-05-04 13:59:04 deniger Exp $
   */
  public static class ComparaisonFieldComparator implements Comparator {


    /**
     * Compare les "ComparaisonField".
     */
    @Override
    public int compare(final Object _o1, final Object _o2) {
      return compare((ComparaisonField) _o1, (ComparaisonField) _o2);
    }

    /**
     * @param _f1 le premier field a comparer
     * @param _f2 le deuxieme
     * @return le resultat qui va bien
     */
    public int compare(final ComparaisonField _f1, final ComparaisonField _f2) {
      return _f1.getEntite().compareTo(_f2.getEntite());
    }
  }

  /**
   * @return un modele pour les JTable.
   */
  public TableModel createTableModel() {
    return new ComparaisonTableModel();
  }

  /**
   * Cree des renderer pour le modele JTable de cette classe.
   * @param _t la table de dest
   */
  public void createRenderer(final JTable _t) {
    final TableColumnModel colModel = _t.getColumnModel();
    final CtuluCellTextRenderer textRender = new CtuluCellTextRenderer();
    final CtuluCellTextRenderer iconRender = new CtuluCellTextRenderer() {

      @Override
      public void setValue(Object _o) {
        setIcon((Icon) _o);
      }
    };
    final TableColumn col = colModel.getColumn(0);
    col.setCellRenderer(iconRender);
    final int n = colModel.getColumnCount() - 1;
    for (int i = n; i >= 1; i--) {
      colModel.getColumn(i).setCellRenderer(textRender);
    }
  }

  class ComparaisonTableModel extends AbstractTableModel {

    @Override
    public int getColumnCount() {
      return 4;
    }

    @Override
    public int getRowCount() {
      return compFields_.length;
    }


    @Override
    public Object getValueAt(final int _row, final int _col) {
      final ComparaisonField f = compFields_[_row];
      if (_col == 0) {
        return f.getStateIcon();
      } else if (_col == 1) {
        return f.ent_.getNom();
      } else if (_col == 2) {
        return f.valueBase_;
      } else if (_col == 3) {
        return f.valueCompare_;
      }
      return null;
    }


    @Override
    public String getColumnName(final int _col) {
      if (_col == 0) {
        return FDicoLib.getS("Etat");
      } else if (_col == 1) {
        return FDicoLib.getS("Nom");
      } else if (_col == 2) {
        return titleBase_;
      } else if (_col == 3) {
        return titleCompare_;
      }
      return CtuluLibString.EMPTY_STRING;
    }
  }

  class ComparaisonField {

    private final static int ADD    = 0;
    private final static int REMOVE = 1;
    private final static int CHANGE = 2;
    DicoEntite               ent_;
    String                   valueBase_;
    String                   valueCompare_;
    int                      state_;

    /**
     * @return l'etat du champ
     */
    public String getState() {
      if (state_ == ADD) {
        return FDicoLib.getS("Ajout�");
      } else if (state_ == REMOVE) {
        return FDicoLib.getS("Supprim�");
      } else if (state_ == CHANGE) {
        return CtuluLib.getS("Modifi�");
      }
      return CtuluLibString.EMPTY_STRING;
    }

    /**
     * @return l'entite du comparateur
     */
    public DicoEntite getEntite() {
      return ent_;
    }

    /**
     * @return l'icone associe
     */
    public BuIcon getStateIcon() {
      if (state_ == ADD) {
        return FDicoComparator.this.ajout_;
      } else if (state_ == REMOVE) {
        return FDicoComparator.this.supp_;
      } else if (state_ == CHANGE) {
        return FDicoComparator.this.mod_;
      }
      return null;
    }
  }
}
