/*
 * @creation 5 juin 2003
 * 
 * @modification $Date: 2007-05-04 13:59:04 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.fdico;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
import org.fudaa.ctulu.gui.CtuluPopupMenu;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsListener;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaProjectStateSupportLabelListener;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author deniger
 * @version $Id: FDicoProjectPanel.java,v 1.26 2007-05-04 13:59:04 deniger Exp $
 */
public class FDicoProjectPanel extends BuPanel implements ActionListener, MouseListener, DicoParamsListener, CtuluUndoRedoInterface,
    ListSelectionListener {

  BuTable table_;
  FDicoEntiteFileTableModelInterface model_;
  FDicoProjet proj_;
  CtuluCommandManager cmdMng_;
  FDicoTableEditorChooser.DicoFileEditor fileEditor_;
  JLabel lbFile_;
  JTextField txtName_;
  // DicoEntite title_;
  JLabel lbErreur_;
  JLabel lbModifie_;
  JLabel lbDate_;
  Action actionDefaultSelected_;
  Action actionEditerSelected_;

  public FDicoProjectPanel(final FDicoProjet _proj) {
    proj_ = _proj;
    this.cmdMng_ = new CtuluCommandManager();
    model_ = _proj.createEntiteFileTableModel();
    if (model_ == null) {
      return;
    }
    model_.setCmdMng(cmdMng_);
    table_ = new BuTable(model_);
    table_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    table_.addMouseListener(this);
    table_.getSelectionModel().addListSelectionListener(this);
    setLayout(new BuBorderLayout(5, 5));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(new BuScrollPane(table_), BuBorderLayout.CENTER);
    final TableColumnModel columnModel = table_.getColumnModel();
    TableColumn col = columnModel.getColumn(0);
    col.setCellRenderer(new IconCellRenderer());
    col.setMaxWidth(20);
    col.setWidth(18);
    col = columnModel.getColumn(1);
    col.setCellRenderer(new FirstCellRenderer());
    col = columnModel.getColumn(2);
    col.setCellRenderer(new SecondCellRenderer());
    fileEditor_ = new FDicoTableEditorChooser.DicoFileEditor();
    fileEditor_.setFileChooserSelectionMode(JFileChooser.FILES_ONLY);
    fileEditor_.setBaseDir(proj_.getDirBase());
    col.setCellEditor(fileEditor_);
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 10, 10));
    final DicoEntite title = _proj.getDicoParams().getDicoFileFormatVersion().getTitreEntite();
    if (title != null) {
      pn.add(new BuLabel(FDicoLib.getS("Nom du projet:")));
      txtName_ = new BuTextField(80);
      txtName_.setEditable(false);
      txtName_.addMouseListener(this);
      txtName_.setText(proj_.getTitle());
      pn.add(txtName_);
    }
    pn.add(new BuLabel(FDicoLib.getS("Fichier principal:")));
    lbFile_ = new BuLabel();
    final String s = _proj.getFDicoParams().getMainFile().getAbsolutePath();
    lbFile_.setText(s);
    lbFile_.setToolTipText(s);
    pn.add(lbFile_);
    pn.add(new BuLabel(FDicoLib.getS("Type projet:")));
    pn.add(new BuLabel(proj_.getDicoParams().getDicoFileFormatVersion().toString()));
    pn.add(new BuLabel(FDicoLib.getS("Dernier enregistrement:")));
    lbDate_ = new BuLabel();
    pn.add(lbDate_);
    pn.add(new BuLabel(FDicoLib.getS("Etat:")));
    lbModifie_ = new BuLabel();
    pn.add(lbModifie_);
    pn.add(new BuLabel(CtuluLibString.EMPTY_STRING));
    lbErreur_ = new BuLabel();
    pn.add(lbErreur_);
    add(pn, BuBorderLayout.NORTH);
    updateErreur();
    final FudaaProjectStateSupportLabelListener listener = new FudaaProjectStateSupportLabelListener(lbModifie_, lbDate_) {
      @Override
      public void finishUpdate(final FudaaProjetStateInterface _projState) {
        dicoParamsProjectModifyStateChanged();
      }
    };
    listener.setSupport(_proj);
    listener.initWith(_proj.getState());
    buildAction();
    final BuPanel p = new BuPanel();
    p.setLayout(new BuVerticalLayout(5, false, false));
    BuButton bt = new BuButton();
    bt.setAction(actionEditerSelected_);
    bt.setText(CtuluLibString.EMPTY_STRING);
    p.add(bt);
    bt = new BuButton();
    bt.setAction(actionDefaultSelected_);
    bt.setText(CtuluLibString.EMPTY_STRING);
    p.add(bt);
    add(p, BuBorderLayout.EAST);
    final InputMap thisMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK), "INITIALISER");
    getActionMap().put("INITIALISER", actionDefaultSelected_);
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK), getStrEdit());
    thisMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.KEY_TYPED), getStrEdit());
    getActionMap().put(getStrEdit(), actionEditerSelected_);
    revalidate();
    _proj.getDicoParams().addModelListener(this);
  }

  private String getStrEdit() {
    return "EDITER";
  }

  private void updateErreur() {
    if (proj_.isDataValide()) {
      lbErreur_.setForeground(Color.black);
      lbErreur_.setText(FDicoLib.getS("Projet valide"));
    } else {
      lbErreur_.setForeground(Color.red);
      if (proj_.isSpaceInParentDir()) {
        lbErreur_.setText(FDicoLib
            .getS("<html>Le chemin du fichier cas ne doit pas contenir d'espace.<br>Enregistrer votre projet dans un autre r�pertoire</html>"));
      } else {
        lbErreur_.setText(FDicoLib.getS("Le projet contient des erreurs"));
      }

    }
    proj_.validChange();
  }

  private void buildAction() {
    actionDefaultSelected_ = new AbstractAction(FudaaLib.getS("Initialiser"), BuResource.BU.getIcon("annuler")) {

      @Override
      public void actionPerformed(final ActionEvent _ae) {
        model_.remove(table_.getSelectedRows());
      }
    };
    actionEditerSelected_ = new AbstractAction(BuResource.BU.getString("Editer"), BuResource.BU.getIcon("editer")) {

      @Override
      public void actionPerformed(final ActionEvent _ae) {
        final int i = table_.getSelectedRow();
        if (i >= 0) {
          table_.editCellAt(i, 2);
        }
        fileEditor_.doClick();
      }
    };
    actionDefaultSelected_.setEnabled(false);
    actionDefaultSelected_.putValue(Action.SHORT_DESCRIPTION, FDicoLib.getS("Initialiser les mot-cl�s s�lectionn�s"));
    actionEditerSelected_.setEnabled(false);
    actionEditerSelected_.putValue(Action.SHORT_DESCRIPTION, BuResource.BU.getString("Editer"));
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if (getStrEdit().equals(_evt.getActionCommand())) {
      model_.editer(table_.getSelectedRow());
    }
  }

  private class IconCellRenderer extends DefaultTableCellRenderer {

    public IconCellRenderer() {
      setPreferredSize(new Dimension(16, 16));
    }

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected, final boolean _hasFocus,
                                                   final int _row, final int _column) {
      final DicoEntite e = model_.getEntite(_row);
      if (!proj_.getDicoParams().isValueValideFor(e)) {
        setToolTipText(FDicoLib.getS("Mot-cl� invalide"));
        setIcon(FudaaResource.FUDAA.getIcon("status-error"));
      } else if (proj_.getState().isLoadedAndModified(e)) {
        setToolTipText(FDicoLib.getS("Fichier charg� et modifi�"));
        setIcon(FudaaResource.FUDAA.getIcon("livre-ouvert-modif"));
      } else if (proj_.getState().isLoaded(e)) {
        setToolTipText(FDicoLib.getS("Fichier charg�"));
        setIcon(FudaaResource.FUDAA.getIcon("livre-ouvert"));
      } else {
        setToolTipText(null);
        setIcon(null);
      }
      return this;
    }
  }

  private class BaseCellRenderer extends DefaultTableCellRenderer {

    Font init_;
    Font loaded_;

    public BaseCellRenderer() {
      init_ = table_.getFont();
    }

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected, final boolean _hasFocus,
                                                   final int _row, final int _column) {
      super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      final DicoEntite e = model_.getEntite(_row);
      if (proj_.getDicoParams().isValueValideFor(e)) {
        setForeground(Color.black);
        setToolTipText(CtuluLibString.EMPTY_STRING);
      } else {
        setForeground(Color.red);
        setToolTipText(proj_.getDicoParams().getInvalidMessage(e));
      }
      if (proj_.getState().isLoaded(e)) {
        if (loaded_ == null) {
          loaded_ = init_.deriveFont(Font.BOLD);
        }
        setFont(loaded_);
      } else {
        setFont(init_);
      }
      return this;
    }
  }

  private class FirstCellRenderer extends BaseCellRenderer {

    public FirstCellRenderer() {
      super();
    }

    @Override
    protected void setValue(final Object _value) {
      if (_value != null)
        setText(((DicoEntite) _value).getNom());
    }
  }

  private class SecondCellRenderer extends BaseCellRenderer {

    public SecondCellRenderer() {
      super();
    }

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected, final boolean _hasFocus,
                                                   final int _row, final int _column) {
      super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      setToolTipText(model_.getAbsolutePath(model_.getEntite(_row)));
      return this;
    }

    @Override
    protected void setValue(final Object _value) {
      setText((String) _value);
    }
  }

  public void mouseDown(final MouseEvent _evt) {
  }

  @Override
  public void mouseEntered(final MouseEvent _evt) {
  }

  @Override
  public void mouseExited(final MouseEvent _evt) {
  }

  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (_evt.getSource() == txtName_) {
      return;
    }
    if (_evt.isPopupTrigger()) {
      popupMenu(_evt);
    }
  }

  private void popupMenu(final MouseEvent _evt) {
    final int i = table_.rowAtPoint(new Point(_evt.getX(), _evt.getY()));
    if (i >= 0) {
      table_.setRowSelectionInterval(i, i);
    }
    // int r = table_.getSelectedRow();
    final int xe = _evt.getX();
    final int ye = _evt.getY();
    final CtuluPopupMenu menu = new CtuluPopupMenu();
    // xe = 20 - menu.getSize().width;
    // ye -= 5;
    final BuMenuItem item = new BuMenuItem(FudaaResource.FUDAA.getString("Editer le contenu du fichier"));
    item.setActionCommand(getStrEdit());
    item.addActionListener(this);
    menu.add(item);
    menu.show((JComponent) _evt.getSource(), xe, ye);
  }

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (_evt.isPopupTrigger()) {
      popupMenu(_evt);
    }
  }

  public void mouseUp(final MouseEvent _evt) {
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {
    if ((_evt.getSource() == txtName_)) {
      proj_.changeTitleProjet(cmdMng_);
    }
  }

  /**
   *
   */
  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    updateErreur();
    updateTitle(_ent);
  }

  /**
   *
   */
  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _old) {
    updateErreur();
    updateTitle(_ent);
  }

  /**
   *
   */
  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _old) {
    updateErreur();
    updateTitle(_ent);
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {

  }

  @Override
  public CtuluCommandManager getCmdMng() {
    return cmdMng_;
  }

  @Override
  public void setActive(final boolean _b) {
    if (!_b && table_.getCellEditor() != null) {
      table_.getCellEditor().cancelCellEditing();
    }
  }

  private void updateTitle(final DicoEntite _e) {
    if ((txtName_ != null) && (_e == proj_.getTitleEntite())) {
      txtName_.setText(proj_.getTitle());
    }
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
    updateErreur();
  }

  /**
   * maj du nom du fichier et de l'etat de sauvegarde.
   * 
   * @see org.fudaa.dodico.dico.DicoParamsListener#dicoParamsProjectModifyStateChanged(org.fudaa.dodico.dico.DicoParams)
   */
  public void dicoParamsProjectModifyStateChanged() {
    fileEditor_.setBaseDir(proj_.getDirBase());
    final String s = proj_.getFDicoParams().getMainFile().getAbsolutePath();
    lbFile_.setText(s);
    lbFile_.setToolTipText(s);
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    final boolean isKeywordSelected = !table_.getSelectionModel().isSelectionEmpty();
    if (isKeywordSelected) {

      final DicoEntite kw = model_.getEntite(table_.getSelectedRow());
      if (kw == null) {
        actionDefaultSelected_.setEnabled(false);
        actionEditerSelected_.setEnabled(false);
      } else {
        actionDefaultSelected_.setEnabled(!kw.isRequired());
        actionEditerSelected_.setEnabled(kw.isModifiable());
      }
    } else {
      actionDefaultSelected_.setEnabled(false);
      actionEditerSelected_.setEnabled(false);
    }

  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if (cmdMng_ != _source) {
      cmdMng_.clean();
    }
  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
  }

}
