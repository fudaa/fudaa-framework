/*
 *  @file         FDicoPreferences.java
 *  @creation     19 sept. 2003
 *  @modification $Date: 2006-04-05 12:51:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import com.memoire.bu.BuPreferences;
/**
 * @author deniger
 * @version $Id: FDicoPreferences.java,v 1.3 2006-04-05 12:51:13 deniger Exp $
 */
public final class FDicoPreferences extends BuPreferences {
  public final static FDicoPreferences FD= new FDicoPreferences();
  private FDicoPreferences() {}
}
