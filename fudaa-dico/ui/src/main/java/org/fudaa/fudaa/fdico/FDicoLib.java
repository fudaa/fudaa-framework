/*
 *  @file         PrertLib.java
 *  @creation     2002-12-16
 *  @modification $Date: 2006-09-19 15:01:58 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;

import com.memoire.bu.BuLabel;

/**
 * @author deniger
 * @version $Id: FDicoLib.java,v 1.2 2006-09-19 15:01:58 deniger Exp $
 */
public final class FDicoLib {

  private FDicoLib() {}
  public final static Font FONT_DIALOG = UIManager.getFont("Font.dialog");

  public static void decoreComponent(final JComponent _c, final boolean _valide) {
    _c.setForeground(_valide ? Color.black : Color.red);
  }

  public static JLabel buildLabelPlainFont() {
    final JLabel r = new BuLabel();
    r.setFont(FONT_DIALOG);
    return r;
  }

  public static String getS(final String _s) {
    return FDicoResource.FD.getString(_s);
  }

  public static String getS(final String _s, final String _v0) {
    return FDicoResource.FD.getString(_s, _v0);
  }

  public static String getS(final String _s, final String _v0, final String _v1) {
    return FDicoResource.FD.getString(_s, _v0, _v1);
  }
}
