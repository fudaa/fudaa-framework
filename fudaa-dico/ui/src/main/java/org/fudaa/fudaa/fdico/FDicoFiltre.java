/*
 *  @file         TrDicoFiltre.java
 *  @creation     19 mai 2003
 *  @modification $Date: 2006-09-19 15:01:59 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import org.fudaa.dodico.dico.DicoEntite;
/**
 * @author deniger
 * @version $Id: FDicoFiltre.java,v 1.3 2006-09-19 15:01:59 deniger Exp $
 */
public interface FDicoFiltre {
   boolean accept(DicoEntite _ent);
   boolean isSameFiltre(FDicoFiltre _f);
}