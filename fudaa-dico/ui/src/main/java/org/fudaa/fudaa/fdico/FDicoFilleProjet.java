/**
 *  @creation     29 avr. 2003
 *  @modification $Date: 2007-05-04 13:59:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.fdico;

import java.awt.Component;
import java.beans.PropertyVetoException;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.InternalFrameEvent;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuResource;

import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluSelectionInterface;

import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;

import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.undo.FudaaUndoPaneFille;

/**
 * @author deniger
 * @version $Id: FDicoFilleProjet.java,v 1.21 2007-05-04 13:59:04 deniger Exp $
 */
public class FDicoFilleProjet extends FudaaUndoPaneFille implements CtuluSelectionInterface {

  protected JComponent[] specificTools_;
  private FDicoProjet projet_;
  private FDicoEntitePanel pnEnt_;
  BuCommonImplementation impl_;

  @Override
  public String[] getEnabledActions() {
    return new String[] { "TOUTSELECTIONNER" };
  }

  /**
   * Appelle le constructeur complet sans composant pour le partie "haut-gauche".
   *
   * @param _proj le projet en question : non null!
   * @param _impl l'implementation parent : non null!
   */
  public FDicoFilleProjet(final FDicoProjet _proj, final FudaaCommonImplementation _impl) {
    this(_proj, _impl, null);
  }

  /**
   * @param _proj le projet en question : non null!
   * @param _impl l'implementation parent : non null!
   * @param _leftUp le composant a ajouter en haut a gauche du panneau des mot-cl� : null possible
   */
  public FDicoFilleProjet(final FDicoProjet _proj, final FudaaCommonImplementation _impl, final Component _leftUp) {
    super(_proj.getTitle(), true, true, true, true, _impl.getUndoCmdListener());
    impl_ = _impl;
    setName("ifKeywords");
    final FDicoProjectPanel projPane = new FDicoProjectPanel(_proj);
    projPane.setName("pnProjectPane");
    super.addTab(BuResource.BU.getString("Projet"), null, projPane, FDicoLib.getS("Les fichiers du projet"));
    final CtuluCommandManager cmd = new CtuluCommandManager();
    pnEnt_ = new FDicoEntitePanel(new FDicoEntiteTableModel(_proj, cmd), _leftUp);
    super.addTab(FDicoLib.getS("Mot-cl�s"), null, pnEnt_, FDicoLib.getS("Mot-cl�s"));
    projet_ = _proj;
  }

  /**
   * @return le panneau des mot-cl�s
   */
  public FDicoEntitePanel getEntitePanel() {
    return pnEnt_;
  }

  public FDicoProjectPanel getProjectPanel() {
    return (FDicoProjectPanel) super.getPaneComponent("pnProjectPane");
  }

  /**
   * Remet a null les outils specifiques.
   */
  public void applicationPreferencesChanged() {
    fireInternalFrameEvent(InternalFrameEvent.INTERNAL_FRAME_DEACTIVATED);
    specificTools_ = null;
    fireInternalFrameEvent(InternalFrameEvent.INTERNAL_FRAME_ACTIVATED);
  }

  /**
   * Construit les outils specifiques si n�cessaires.
   *
   * @return les outils specifiques.
   */
  @Override
  public JComponent[] getSpecificTools() {
    if (specificTools_ == null) {
      final EbliActionInterface[] actions = projet_.getSpecificActions();
      if (actions == null) {
        return null;
      }
      specificTools_ = new JComponent[actions.length];
      final int n = specificTools_.length;
      for (int i = 0; i < n; i++) {
        specificTools_[i] = actions[i].buildToolButton(EbliComponentFactory.INSTANCE);
      }
    }
    return specificTools_;
  }

  private void antiTableBug() {
    final Component c = getSelectedComponent();
    if (c instanceof FDicoEntitePanel) {
      final JTable table = ((FDicoEntitePanel) c).getTable();
      if (table.getCellEditor() != null) {
        table.getCellEditor().cancelCellEditing();
      }
      final int selectedCol = table.getSelectedColumn();
      if (selectedCol >= 0) {
        final FDicoTableColumn col = (FDicoTableColumn) table.getColumnModel().getColumn(selectedCol);
        if (col.getIdentifier() == FDicoTableColumnIdentifier.COLUMN_VALEUR) {
          final int i = table.getColumnModel().getColumnIndex(FDicoTableColumnIdentifier.COLUMN_NOM);
          table.setColumnSelectionInterval(i, i);
        }
      }
    }
  }

  /**
   * *
   */
  @Override
  public void redo() {
    final CtuluCommandManager mng = getActiveCmdMng();
    if (mng != null) {
      antiTableBug();
      mng.redo();
    }
  }

  /**
   * *
   */
  @Override
  public void undo() {
    final CtuluCommandManager mng = getActiveCmdMng();
    if (mng != null) {
      antiTableBug();
      mng.undo();
    }
  }

  /**
   * *
   */
  @Override
  public void find() {
    if (getSelectedComponent() == pnEnt_) {
      pnEnt_.activeFind();
    }
  }

  /**
   * NE fait rien.
   */
  @Override
  public void replace() {}

  protected JTable getCurrentTable() {
    JTable t = null;
    final Component c = getSelectedComponent();
    if (c == pnEnt_) {
      t = pnEnt_.table_;
    } else if (c == getProjectPanel()) {
      t = getProjectPanel().table_;
    }
    return t;
  }

  /**
   */
  @Override
  public void select() {
    final JTable t = getCurrentTable();
    if (t != null) {
      t.getSelectionModel().setSelectionInterval(0, t.getRowCount() - 1);
    }

  }

  @Override
  public void inverseSelection() {
    final JTable t = getCurrentTable();
    if (t != null && !t.getSelectionModel().isSelectionEmpty()) {
      final CtuluListSelection select = new CtuluListSelection();
      final ListSelectionModel model = t.getSelectionModel();
      select.setSelection(t.getSelectionModel());
      select.inverse(t.getRowCount());
      for (int i = t.getRowCount() - 1; i >= 0; i--) {
        if (select.isSelected(i)) {
          model.addSelectionInterval(i, i);
        } else {
          model.removeIndexInterval(i, i);
        }

      }
    }

  }

  @Override
  public void clearSelection() {
    final JTable t = getCurrentTable();
    if (t != null && !t.getSelectionModel().isSelectionEmpty()) {
      final ListSelectionModel model = t.getSelectionModel();
      model.clearSelection();
    }

  }

  /**
   * Met a jour l'etat du bouton "rechercher".
   */
  @Override
  public void setSelected(final boolean _b) throws PropertyVetoException {
    super.setSelected(_b);
    if (impl_ != null) {
      impl_.setEnabledForAction("RECHERCHER", _b && (pnEnt_ != null)
          && (getSelectedComponent() == pnEnt_));
    }
  }

  /**
   * Met a jour l'etat du bouton "rechercher".
   */
  @Override
  public void stateChanged(final ChangeEvent _e) {
    super.stateChanged(_e);
    if (impl_ != null) {
      impl_.setEnabledForAction("RECHERCHER", (pnEnt_ != null) && (getSelectedComponent() == pnEnt_));
    }
  }

  /**
   * Sauvegarde des derni�res modifs dans le panneau des mot-cl�s.
   */
  public void saveLastModification() {
    if (pnEnt_ != null) {
      pnEnt_.saveLastModification();
    }
  }

  /**
   * Sauvegarde la vue avant que la fenetre soit ferm�e.
   *
   * @see javax.swing.JInternalFrame#fireInternalFrameEvent(int)
   */
  @Override
  protected void fireInternalFrameEvent(final int _id) {
    if (_id == InternalFrameEvent.INTERNAL_FRAME_CLOSED && pnEnt_ != null) {
      pnEnt_.saveView();
    }
    super.fireInternalFrameEvent(_id);
  }
}
