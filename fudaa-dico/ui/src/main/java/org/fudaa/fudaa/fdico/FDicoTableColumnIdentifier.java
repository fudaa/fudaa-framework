/**
 *  @creation     15 mai 2003
 *  @modification $Date: 2007-05-04 13:59:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.fdico;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.swing.event.TableColumnModelEvent;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoEntiteComparator;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsInterface;

import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author deniger
 * @version $Id: FDicoTableColumnIdentifier.java,v 1.21 2007-05-04 13:59:04 deniger Exp $
 */
public abstract class FDicoTableColumnIdentifier {

  public static class ColmunIdNameComparator implements Comparator {

    @Override
    public int compare(final Object _o1, final Object _o2) {
      if ((!(_o1 instanceof FDicoTableColumnIdentifier)) || (!(_o2 instanceof FDicoTableColumnIdentifier))) {
        throw new IllegalArgumentException("pas bon type");
      }
      if (_o1 == _o2) {
        return 0;
      }
      return ((FDicoTableColumnIdentifier) _o1).getId().compareTo(((FDicoTableColumnIdentifier) _o2).getId());
    }
  }

  static String getFinP() {
    return "</p>";
  }

  public final static boolean isVisible(final FDicoTableColumnIdentifier _id) {
    final boolean defVal = (_id == COLUMN_NOM) || (_id == COLUMN_VALEUR);
    return FDicoPreferences.FD.getBooleanProperty(getColVisiblePref(_id), defVal);
  }

  public static class ColumnIdPositionComparator implements Comparator {

    @Override
    public int compare(final Object _o1, final Object _o2) {
      if ((!(_o1 instanceof FDicoTableColumnIdentifier)) || (!(_o2 instanceof FDicoTableColumnIdentifier))) {
        throw new IllegalArgumentException("pas bon type");
      }
      if (_o1 == _o2) {
        return 0;
      }
      final int r = ((FDicoTableColumnIdentifier) _o1).getPrefPosition()
          - ((FDicoTableColumnIdentifier) _o2).getPrefPosition();
      return r == 0 ? ((FDicoTableColumnIdentifier) _o1).getId().compareTo(((FDicoTableColumnIdentifier) _o2).getId())
          : r;
    }
  }

  public static class ColumnManager {

    TableCellRenderer defaultRenderer_;
    TableCellRenderer modifieRenderer_;
    TableCellEditor valueEditor_;
    TableCellEditor nomEditor_;
    TableCellRenderer valueRenderer_;

    public ColumnManager(final TableCellRenderer _defaultRenderer, final TableCellRenderer _modifieRenderer,
        final TableCellRenderer _valueRenderer, final TableCellEditor _valueEditor, final TableCellEditor _nomEditor) {
      defaultRenderer_ = _defaultRenderer;
      modifieRenderer_ = _modifieRenderer;
      valueEditor_ = _valueEditor;
      valueRenderer_ = _valueRenderer;
      nomEditor_ = _nomEditor;
    }

    public ColumnModelWithoutException createColModel() {
      final ArrayList visibleCol = new ArrayList(IDENTIFIERS.size());
      for (final Iterator it = IDENTIFIERS.iterator(); it.hasNext();) {
        final FDicoTableColumnIdentifier colId = (FDicoTableColumnIdentifier) it.next();
        if ((!colId.isCloseable()) || (isVisible(colId))) {
          visibleCol.add(colId);
        }
      }
      Collections.sort(visibleCol, new ColumnIdPositionComparator());
      final ColumnModelWithoutException r = new ColumnModelWithoutException();
      for (final Iterator it = visibleCol.iterator(); it.hasNext();) {
        r.addColumn(createColumn((FDicoTableColumnIdentifier) it.next()));
      }
      return r;
    }

    public TableColumn createColumn(final FDicoTableColumnIdentifier _id) {
      if (_id == COLUMN_VALEUR) {
        return FDicoTableColumnIdentifier.COLUMN_VALEUR.createTableColumn(valueRenderer_, valueEditor_, null);
      } else if (_id == COLUMN_MODIFIE) {
        return FDicoTableColumnIdentifier.COLUMN_MODIFIE.createTableColumn(modifieRenderer_, null, null);
      } else if (_id == COLUMN_NOM) {
        return FDicoTableColumnIdentifier.COLUMN_NOM.createTableColumn(defaultRenderer_, nomEditor_, null);
      } else {
        return _id.createTableColumn(defaultRenderer_, null, null);
      }
    }
  }

  public static class ColumnModelWithoutException extends DefaultTableColumnModel {

    public TableColumn findColumnWithIdentifierFast(final Object _o) {
      for (final Iterator it = tableColumns.iterator(); it.hasNext();) {
        final TableColumn r = (TableColumn) it.next();
        if (_o == r.getIdentifier()) {
          return r;
        }
      }
      return null;
    }

    public void insertColumn(final TableColumn _aColumn, final int _i) {
      if (_aColumn == null) {
        throw new IllegalArgumentException("Object is null");
      }
      if ((_i < tableColumns.size()) && (_i >= 0)) {
        tableColumns.add(_i, _aColumn);
        _aColumn.addPropertyChangeListener(this);
        recalcWidthCache();
        fireColumnAdded(new TableColumnModelEvent(this, _i, getColumnCount() - 1));
      } else {
        super.addColumn(_aColumn);
      }
    }
  }

  public final static FDicoTableColumnIdentifier COLUMN_MODIFIE = new FDicoTableColumnIdentifier(CtuluLib
      .getS("Modifi�"), "modifie", 3) {

    @Override
    public String getToolTip(final int _r, final FDicoEntiteTableModel _table) {
      return getValue(_r, _table);
    }

    class StringComparator extends DicoEntiteComparator {

      DicoParams table_;

      public StringComparator(final DicoParams _t, final boolean _reverse) {
        super(_reverse);
        table_ = _t;
      }

      @Override
      public int compareField(final DicoEntite _e1, final DicoEntite _e2) {
        return getValue(_e1, table_).compareTo(getValue(_e2, table_));
      }
    }

    private StringComparator comp_;

    @Override
    public DicoEntiteComparator createComparator(final DicoParams _table, final boolean _reverse) {
      if ((comp_ == null) || (comp_.table_ != _table) || (comp_.isReverse() != _reverse)) {
        comp_ = new StringComparator(_table, _reverse);
      }
      return comp_;
    }

    public String getValue(final DicoEntite _e, final DicoParamsInterface _params) {
      return _params.isValueSetFor(_e) ? CtuluLib.getS("Modifi�") : FudaaLib.getS("Valeur par defaut");
    }

    @Override
    public String getValue(final int _r, final FDicoEntiteTableModel _table) {
      return getValue(_table.getEntite(_r), _table.getParams());
    }
  };
  public final static FDicoTableColumnIdentifier COLUMN_NOM = new FDicoTableColumnIdentifier(FDicoLib.getS("Nom"),
      "nom", false, 1) {

    @Override
    public boolean isEditable(final int _columnIndex, final FDicoEntiteTableModel _table) {
      return true;
    }

    @Override
    public String getToolTip(final int _r, final FDicoEntiteTableModel _table) {
      final StringBuffer b = new StringBuffer(100);
      final DicoEntite e = _table.getEntite(_r);
      b.append("<html><body><h4 align=\"center\" style=\"margin:5px\"><u>").append(e.getNom()).append("</u></h4>");
      if (e.getAide() != null && e.getAide().length() > 0) {
        b.append(getH4()).append(FDicoLib.getS("Aide:")).append(getFinH4());
        String aide = e.getAide();
        if (aide.length() > 300) {
          aide = aide.substring(0, 300) + " (...)";
        }
        b.append(getP()).append(FuLib.replace(aide, "\n", "<br>")).append(getFinP());
      }
      if (_table.getParams().getComment(e) != null) {
        b.append(getH4()).append(FDicoLib.getS("Commentaires:")).append(getFinH4());

        b.append(getP()).append(FuLib.replace(_table.getParams().getComment(e), "\n", "<br>")).append(getFinP());
      }
      b.append("</body></html>");
      return b.toString();
    }

    @Override
    public DicoEntiteComparator createComparator(final DicoParams _table, final boolean _reverse) {
      return _reverse ? DicoEntiteComparator.getNomInverseComparator() : DicoEntiteComparator.getNomComparator();
    }

    @Override
    public String getValue(final int _r, final FDicoEntiteTableModel _table) {
      return _table.getEntite(_r).getNom();
    }
  };
  public final static FDicoTableColumnIdentifier COLUMN_RUBRIQUE = new FDicoTableColumnIdentifier(FudaaLib
      .getS("Rubrique"), "rubrique", 0) {

    @Override
    public DicoEntiteComparator createComparator(final DicoParams _table, final boolean _reverse) {
      return _reverse ? DicoEntiteComparator.getRubriqueReverseComparator() : DicoEntiteComparator
          .getRubriqueComparator();
    }

    @Override
    public String getValue(final int _r, final FDicoEntiteTableModel _table) {
      return _table.getEntite(_r).getRubrique();
    }

    @Override
    public String getToolTip(final int _r, final FDicoEntiteTableModel _table) {
      return getValue(_r, _table);
    }
  };
  public final static FDicoTableColumnIdentifier COLUMN_TYPE = new FDicoTableColumnIdentifier(FDicoLib.getS("Type"),
      "type", 4) {

    @Override
    public DicoEntiteComparator createComparator(final DicoParams _table, final boolean _reverse) {
      return new DicoEntiteComparator.TypeDescription(_reverse);
    }

    @Override
    public String getToolTip(final int _r, final FDicoEntiteTableModel _table) {
      return getValue(_r, _table);
    }

    @Override
    public String getValue(final int _r, final FDicoEntiteTableModel _table) {
      return _table.getEntite(_r).getType().getDescription();
    }
  };
  public final static FDicoTableColumnIdentifier COLUMN_VALEUR = new FDicoTableColumnIdentifier(BuResource.BU
      .getString("Valeur"), "valeur", false, 2) {

    private DicoEntiteComparator.Value comp_;

    @Override
    public DicoEntiteComparator createComparator(final DicoParams _table, final boolean _reverse) {
      if ((comp_ == null) || (comp_.getValueInterface() != _table) || (comp_.isReverse() != _reverse)) {
        comp_ = new DicoEntiteComparator.Value(_table, _reverse);
      }
      return comp_;
    }

    @Override
    public String getValue(final int _r, final FDicoEntiteTableModel _table) {
      return _table.getEntiteValue(_r);
    }

    /**
     * @see org.fudaa.fudaa.fdico.FDicoTableColumnIdentifier#getToolTip(int,
     *      org.fudaa.fudaa.fdico.FDicoEntiteTableModel)
     */
    @Override
    public String getToolTip(final int _r, final FDicoEntiteTableModel _table) {
      final DicoEntite e = _table.getEntite(_r);
      final String defaultValue = e.getDefautValue();
      final String error = _table.getParams().getInvalidMessage(e);
      final StringBuffer r = new StringBuffer(100);
      r.append("<html><body><h4 align=\"center\" style=\"margin:5px;\"><u>").append(e.getNom()).append("</u></h4>");
      if (_table.getParams().isValueSetFor(e)) {
        r.append(getH4()).append(FDicoLib.getS("Valeur:")).append(getFinH4());
        r.append(getP()).append(_table.getParams().getValue(e)).append(getFinP());
      }
      r.append(getH4()).append(FDicoLib.getS("Valeur par d�faut:")).append(getFinH4());
      r.append(getP()).append(defaultValue).append(getFinP());
      if (error != null) {
        r.append("<h4 style=\"margin:2px;margin-bottom:0px;margin-top:5px;\" color=\"red\">").append(
            FDicoLib.getS("Erreur:")).append(getFinH4());
        r.append(getP()).append(error).append(getFinP());
      }
      return r.append("</body></html>").toString();

    }

    @Override
    public boolean isEditable(final int _columnIndex, final FDicoEntiteTableModel _table) {
      return _table.getEntite(_columnIndex).getNiveau() >= 0;
    }
  };
  public final static List IDENTIFIERS = buildIdentifiers();

  private static List buildIdentifiers() {
    final ArrayList r = new ArrayList(5);
    r.add(COLUMN_RUBRIQUE);
    r.add(COLUMN_NOM);
    r.add(COLUMN_TYPE);
    r.add(COLUMN_VALEUR);
    r.add(COLUMN_MODIFIE);
    Collections.sort(r, new ColmunIdNameComparator());
    return Collections.unmodifiableList(r);
  }

  public static String getColPositionPref(final FDicoTableColumnIdentifier _id) {
    return "entitePanel." + _id.getId() + ".col.position";
  }

  public static String getColVisiblePref(final FDicoTableColumnIdentifier _id) {
    return "entitePanel." + _id.getId() + ".col.visible";
  }

  public static FDicoTableColumnIdentifier getIdentifer(final int _modelIndex) {
    return (FDicoTableColumnIdentifier) IDENTIFIERS.get(_modelIndex);
  }

  public static FDicoTableColumnIdentifier getIdentifier(final String _id) {
    for (final java.util.Iterator it = IDENTIFIERS.iterator(); it.hasNext();) {
      final FDicoTableColumnIdentifier colId = (FDicoTableColumnIdentifier) it.next();
      if (colId.getId().equals(_id)) {
        return colId;
      }
    }
    return null;
  }

  public static int getIndexModel(final FDicoTableColumnIdentifier _id) {
    return IDENTIFIERS.indexOf(_id);
  }

  private String id_;
  private boolean isCloseable_;
  private String nom_;
  private int prefPosition_;

  FDicoTableColumnIdentifier(final String _nom, final String _id, final boolean _closeable, final int _prefPosition) {
    nom_ = _nom;
    id_ = _id;
    isCloseable_ = _closeable;
    prefPosition_ = _prefPosition;
  }

  FDicoTableColumnIdentifier(final String _nom, final String _id, final int _prefPosition) {
    this(_nom, _id, true, _prefPosition);
  }

  public abstract DicoEntiteComparator createComparator(final DicoParams _table, final boolean _reverse);

  public FDicoTableColumn createTableColumn(final TableCellRenderer _renderer, final TableCellEditor _editor,
      final CtuluCellTextRenderer _headerRenderer) {
    final FDicoTableColumn col = new FDicoTableColumn(this, getIndexModel(this), _renderer, _editor);
    col.setHeaderRenderer(_headerRenderer);
    return col;
  }

  public String getId() {
    return id_;
  }

  public String getNom() {
    return nom_;
  }

  public int getPrefPosition() {
    return FDicoPreferences.FD.getIntegerProperty(getColPositionPref(this), prefPosition_);
  }

  public abstract String getValue(final int _r, final FDicoEntiteTableModel _table);

  public abstract String getToolTip(final int _r, final FDicoEntiteTableModel _table);

  public boolean isCloseable() {
    return isCloseable_;
  }

  public boolean isEditable(final int _rowIndex, final FDicoEntiteTableModel _table) {
    return false;
  }

  public void savePos(final int _i) {
    FDicoPreferences.FD.putIntegerProperty(getColPositionPref(this), _i);
  }

  public String toString() {
    return getClass().getName() + CtuluLibString.ESPACE + getId();
  }

  static String getFinH4() {
    return "</h4>";
  }

  static String getP() {
    return "<p style=\"margin:5px;margin-top:1px\">";
  }

  static String getH4() {
    return "<h4 style=\"margin:2px;margin-bottom:0px;margin-top:5px;\">";
  }
}
