/**
 *  @creation     5 sept. 2003
 *  @modification $Date: 2006-09-19 15:01:59 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.fdico;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

abstract class FDicoFiltreChooserSimple extends FDicoFiltreChooserListAbstract implements ActionListener {
  int indexSelected_ = -1;
  String[] modes_;

  public FDicoFiltreChooserSimple(final String[] _modes) {
    modes_ = _modes;
  }

  @Override
  public final boolean isMultipleSelection() {
    return false;
  }

  @Override
  public final String getFiltreName(final int _i) {
    return modes_[_i];
  }

  @Override
  public final int getSize() {
    return modes_.length;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    try {
      setActifFiltre(Integer.parseInt(_e.getActionCommand()));
    } catch (final NumberFormatException _ne) {
      return;
    }
  }

  @Override
  public final void setActifFiltre(final int _i) {
    setActifFiltre(_i, true);
  }

  private void setActifFiltre(final int _i, final boolean _notifyList) {
    if (_i < 0) {
      return;
    }
    if (_i != indexSelected_) {
      final boolean isOldEmpty = isFiltreEmpty();
      indexSelected_ = _i;
      setChanged();
      notifyObservers(isOldEmpty ? Boolean.TRUE : Boolean.FALSE);
      if (_notifyList) {
        updateListModel();
      }
    }
  }

  @Override
  public final void setActifFiltre(final int[] _i) {}

  @Override
  public final boolean isSameFiltre(final FDicoFiltre _f) {
    if (_f.getClass().equals(getClass())) {
      return ((FDicoFiltreChooserSimple) _f).indexSelected_ == indexSelected_;
    }
    return false;
  }

  @Override
  public final void clearFiltre() {
    if (!isFiltreEmpty()) {
      indexSelected_ = -1;
      setChanged();
      notifyObservers(Boolean.FALSE);
      updateListModel();
    }
  }

  @Override
  public boolean isFiltreEmpty() {
    return indexSelected_ < 0;
  }

  @Override
  public void addFiltre(final int _i) {
    setActifFiltre(_i);
  }

  @Override
  public void removeFiltre(final int _i) {
    if (indexSelected_ == _i) {
      clearFiltre();
    }
  }

  @Override
  public boolean isSelectedFiltre(final int _i) {
    return _i == indexSelected_;
  }

  @Override
  public void updateFromList(final int _firstChangedIndex, final int _lastChangedIndex) {
    if (list_.isSelectionEmpty()) {
      if (!isFiltreEmpty()) {
        clearFiltre();
      }
    } else {
      final int newInt = list_.getMinSelectionIndex();
      if (!isSelectedFiltre(newInt)) {
        setActifFiltre(newInt, false);
      }
    }
  }

  @Override
  public void updateListModel() {
    if (isFiltreEmpty()) {
      list_.clearSelection();
    } else {
      if (!list_.isSelectedIndex(indexSelected_)) {
        list_.setSelectionInterval(indexSelected_, indexSelected_);
      }
    }
  }
}
