/*
 * @creation 13 mai 2003
 * 
 * @modification $Date: 2007-02-02 11:22:29 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.fdico;

import java.awt.Color;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellDecorator;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;

import org.fudaa.dodico.dico.DicoComponentVisitor;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;

import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author deniger
 * @version $Id: FDicoMultiEntiteViewer.java,v 1.19 2007-02-02 11:22:29 deniger Exp $
 */
public class FDicoMultiEntiteViewer extends CtuluDialogPanel {

  class AntiDoublonsListener implements TableModelListener {

    @Override
    public void tableChanged(final TableModelEvent _e) {
      if (comboModel_ != null) {
        comboModel_.setForbiddenValues(Arrays.asList(model_.getValues()));
      }
    }
  }

  class MultiCellDecorator implements CtuluCellDecorator {

    @Override
    public void decore(final JComponent _c, final JList _table, final Object _value, final int _index) {}

    @Override
    public void decore(final JComponent _c, final JTable _table, final Object _value, final int _row, final int _col) {
      if (_value == null) { return; }
      final String s = entite_.getType().getInvalideMessage((String) _value);
      FDicoLib.decoreComponent(_c, s == null);
      if (s != null) {
        _c.setToolTipText(s);
      }
    }
  }

  class ProposalValueVisitor implements DicoComponentVisitor {

    private void internVisitChoix(final DicoDataType.ChoixType _data) {
      model_.addProposedValuesIfNotPresent(_data.getChoiceKeys());
      model_.setDefaultValue(_data.getChoiceKeys()[0]);
    }

    /**
     * Mis a jour des valeurs � proposer.
     * 
     * @param _v le mot-cle
     */
    public void setProposalValues(final DicoEntite.Vecteur _v) {
      model_.emptyProposedValue();
      if (_v.getDefautValue() != null) {
        _v.accept(this);
      }
    }

    @Override
    public void visitBinaire(final DicoDataType.Binaire _data) {
      model_.setDefaultValue("false");
    }

    @Override
    public void visitChaine(final DicoDataType.Chaine _data) {
      if (_data.isChoiceEnable()) {
        internVisitChoix(_data);
      } else {
        model_.setDefaultValue(CtuluLibString.EMPTY_STRING);
      }
    }

    @Override
    public void visitEntier(final DicoDataType.Entier _data) {
      if (_data.isChoiceEnable()) {
        internVisitChoix(_data);
      } else {
        model_.setDefaultValue(CtuluLibString.ZERO);
      }
    }

    @Override
    public void visitReel(final DicoDataType.Reel _data) {
      model_.setDefaultValue("0.0");
    }

    @Override
    public void visitSimple(final DicoEntite.Simple _ent) {}

    @Override
    public void visitTableau(final DicoEntite.Tableau _ent) {
      final String[] r = _ent.getValues(_ent.getDefautValue());
      if (r != null) {
        model_.setProposedValues(r);
      }
      _ent.getType().accept(this);
    }

    @Override
    public void visitVecteur(final DicoEntite.Vecteur _ent) {
      final String[] r = _ent.getValues(_ent.getDefautValue());
      if (r != null) {
        model_.setProposedValues(r);
      }
      _ent.getType().accept(this);
    }
  }

  private final MultiCellDecorator cellDeco_;

  FDicoTableEditorChooser.ChoixTypeComboBoxModel comboModel_;

  DicoEntite.Vecteur entite_;
  JLabel lbControle_;
  JLabel lbDataControle_;
  // BuTextArea txtinfo_;
  JLabel lbDefault_;
  JLabel lbType_;

  TableModelListener listener_;
  CtuluListEditorModel model_;
  ProposalValueVisitor proposalValuesVisitor_;
  CtuluListEditorPanel table_;

  public FDicoMultiEntiteViewer() {
    setLayout(new BuBorderLayout(5, 15));
    model_ = new CtuluListEditorModel(true);
    table_ = new CtuluListEditorPanel(model_);
    table_.setAutoscrolls(true);
    addEmptyBorder(10);
    proposalValuesVisitor_ = new ProposalValueVisitor();
    cellDeco_ = new MultiCellDecorator();
    add(table_, BuBorderLayout.CENTER);
    final BuPanel infos = new BuPanel();
    infos.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(BorderFactory
        .createLineBorder(Color.gray), FudaaLib.getS("Informations")), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    infos.setLayout(new BuGridLayout(2));
    // valeur par defaut
    final String s = ":  ";
    addLabel(infos, FDicoLib.getS("Valeurs par d�faut") + s);
    lbDefault_ = FDicoLib.buildLabelPlainFont();
    infos.add(lbDefault_);
    // type
    addLabel(infos, FDicoLib.getS("Type") + s);
    lbType_ = FDicoLib.buildLabelPlainFont();
    infos.add(lbType_);
    // controle
    addLabel(infos, FDicoLib.getS("Contr�le") + s);
    lbControle_ = FDicoLib.buildLabelPlainFont();
    infos.add(lbControle_);
    addLabel(infos, CtuluLibString.EMPTY_STRING);
    lbDataControle_ = FDicoLib.buildLabelPlainFont();
    infos.add(lbDataControle_);
    add(infos, BuBorderLayout.SOUTH);
  }

  private static boolean isBoundaryRubrique(final DicoEntite _e) {
    final String r = _e.getRubrique();
    return r.indexOf("CONDITIONS LIMITES") >= 0 || r.indexOf("BOUNDARY CONDITIONS") >= 0;
  }

  private void setEntite(final DicoEntite.Vecteur _v, final int _taille) {
    entite_ = _v;
    table_.setValueListCellRenderer(FDicoTableRendererChooser.createCellRenderer(entite_.getType(), cellDeco_));
    final TableCellEditor editor = FDicoTableEditorChooser.createCellEditor(entite_.getType());
    if (editor instanceof FDicoTableEditorChooser.ChoixTypeEditor && !isBoundaryRubrique(_v)) {
      comboModel_ = ((FDicoTableEditorChooser.ChoixTypeEditor) editor).getModel();
      if (listener_ == null) {
        listener_ = new AntiDoublonsListener();
        model_.addTableModelListener(listener_);
      }
    } else if (listener_ != null) {
      model_.removeTableModelListener(listener_);
      listener_ = null;
      comboModel_ = null;
    }
    table_.setValueListCellEditor(editor);
    proposalValuesVisitor_.setProposalValues(_v);
    model_.setMaxValueNb(_taille);
    final String s = _v.getDefautValue();
    lbDefault_.setText(s);
    lbType_.setText(entite_.getType().getDescription());
    lbControle_.setText(entite_.getControleDescription());
    lbDataControle_.setText(entite_.getType().getControleDescription());
  }

  @Override
  public void closeDialog() {
    table_.stopCellEditing();
    super.closeDialog();
  }

  @Override
  public Object getValue() {
    String[] r = new String[model_.getValueNb()];
    model_.getValues(r);
    final String[] rFinal = new String[r.length];
    int index = 0;
    // remove null values
    for (int i = 0; i < r.length; i++) {
      if (r[i] != null) {
        rFinal[index++] = r[i];
      }
    }
    if (index < r.length) {
      r = new String[index];
      System.arraycopy(rFinal, 0, r, 0, index);
    }
    return entite_.getStringValuesArray(r);
  }

  @Override
  public boolean isDataValid() {
    table_.stopCellEditing();
    final String v = (String) getValue();
    final String s = entite_.getInvalideMessage(v);
    if (s == null) {
      cancelErrorText();
      return true;
    }
    setErrorText(s);
    return false;
  }

  @Override
  public void setValue(final Object _o) {
    setValue((String) _o);
  }

  public void setValue(final String _s) {
    model_.setData(entite_.getValues(_s));
    doLayout();
  }

  public void setVecteurEntite(final DicoEntite.Tableau _v) {
    setEntite(_v, _v.getTaille());
  }

  public void setVecteurEntite(final DicoEntite.Vecteur _v) {
    if (_v.getNbElemFixed() >= 0) {
      setEntite(_v, _v.getNbElemFixed());
    } else {
      setEntite(_v, -1);
    }
  }

}
