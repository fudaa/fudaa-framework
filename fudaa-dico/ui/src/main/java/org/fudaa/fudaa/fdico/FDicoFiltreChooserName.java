/*
 *  @file         FDicoFiltreChooserName.java
 *  @creation     5 sept. 2003
 *  @modification $Date: 2007-06-28 09:28:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.awt.Color;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluPopupListener;

import org.fudaa.dodico.dico.DicoEntite;

/**
 * @author deniger
 * @version $Id: FDicoFiltreChooserName.java,v 1.9 2007-06-28 09:28:20 deniger Exp $
 */
public class FDicoFiltreChooserName extends FDicoFiltreChooserAbstract {
  String filtre_;

  @Override
  public JMenuItem[] getActionFor(final DicoEntite _ent) {
    return null;
  }

  @Override
  public void clearFiltre() {
    if (filtre_ != null) {
      filtre_ = null;
      setChanged();
      // moins restrictif
      notifyObservers(Boolean.FALSE);
    }
  }

  @Override
  public boolean isFiltreEmpty() {
    return filtre_ == null;
  }

  @Override
  public String getName() {
    return FDicoLib.getS("Nom");
  }

  public void setFiltre(final String _s) {
    if (_s == null) {
      clearFiltre();
    } else if (!_s.equals(filtre_)) {
      setChanged();
      Boolean b = Boolean.FALSE;
      if ((filtre_ != null) && (_s.indexOf(filtre_) >= 0)) {
        b = Boolean.TRUE;
      }
      filtre_ = _s;
      notifyObservers(b);
    }
  }

  @Override
  public JPanel buildPanel(final Font _f, final ListCellRenderer _r) {
    final BuPanel r = new BuPanel();
    // this name is important. It is used to search the text field "txtFind" : for the
    // "find" action
    r.setName("pnFind");
    r.setLayout(new BuBorderLayout());
    r.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), getName()));
    final FiltreTextField t = new FiltreTextField();
    t.setName("txtFind");
    if (filtre_ != null) {
      t.setText(filtre_);
    }
    r.add(t, BuBorderLayout.CENTER);
    addObserver(t);
    final FDicoFiltreChooserAbstract.FiltrePopupMenu target = new FDicoFiltreChooserAbstract.FiltrePopupMenu(this);
    CtuluPopupListener listener = new CtuluPopupListener(target, r);
    t.addMouseListener(listener);
    return r;
  }
  private class FiltreTextField extends BuTextField implements CaretListener, Observer {
    private boolean launch_ = true;

    public FiltreTextField() {
      addCaretListener(this);
    }

    @Override
    public void caretUpdate(final CaretEvent _e) {
      launch_ = false;
      FDicoFiltreChooserName.this.setFiltre(getText().toUpperCase());
      launch_ = true;
    }

    @Override
    public void update(final Observable _o, final Object _arg) {
      if ((launch_) && (_o == FDicoFiltreChooserName.this)) {
        super.setText(FDicoFiltreChooserName.this.filtre_);
      }
    }
  }

  @Override
  public boolean accept(final DicoEntite _ent) {
    if (CtuluLibString.isEmpty(filtre_)) {
      return true;
    }
    return _ent.getNom().toUpperCase().indexOf(filtre_) >= 0;
  }

  @Override
  public boolean isSameFiltre(final FDicoFiltre _f) {
    if (_f == this) {
      return true;
    }
    if (_f instanceof FDicoFiltreChooserName) {
      return filtre_.equals(((FDicoFiltreChooserName) _f).filtre_);
    }
    return false;
  }
}
