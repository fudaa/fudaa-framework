/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsChangeState;
import org.fudaa.dodico.dico.DicoParamsChangeStateInterface;
import org.fudaa.dodico.dico.DicoParamsChangeStateListener;

import org.fudaa.fudaa.commun.FudaaProjectStateListener;
import org.fudaa.fudaa.commun.FudaaProjetStateInterface;

/**
 * @author fred deniger
 * @version $Id: FDicoProjectState.java,v 1.3 2007-06-05 09:01:13 deniger Exp $
 */
public class FDicoProjectState implements DicoParamsChangeStateListener, FudaaProjetStateInterface {

  final Set fudaaProjectListener_ = new HashSet();

  final DicoParamsChangeStateInterface projectState_;

  boolean uiChanged_;

  public FDicoProjectState(final DicoParamsChangeStateInterface _projectState) {
    super();
    projectState_ = _projectState;
    projectState_.addChangeStateListener(this);
  }

  void fireProjectChanged() {
    for (final Iterator it = fudaaProjectListener_.iterator(); it.hasNext();) {
      ((FudaaProjectStateListener) it.next()).projectStateChanged(this);
    }
  }

  @Override
  public void addListener(final FudaaProjectStateListener _l) {
    fudaaProjectListener_.add(_l);
  }

  public Iterator getEntiteLoadedEnum() {
    return projectState_.getEntiteLoadedEnum();
  }

  public int getEntiteLoadedNb() {
    return projectState_.getEntiteLoadedNb();
  }

  public DicoEntite[] getNewFileEntiteToSave() {
    return projectState_.getNewFileEntiteToSave();
  }

  public DicoParamsChangeStateInterface getProjectState() {
    return projectState_;
  }

  public boolean isEntiteFilesModified() {
    return projectState_.isEntiteFilesModified();
  }

  public boolean isLoaded(final DicoEntite _e) {
    return projectState_.isLoaded(_e);
  }

  public boolean isLoadedAndModified(final DicoEntite _e) {
    return projectState_.isLoadedAndModified(_e);
  }

  @Override
  public boolean isParamsModified() {
    return projectState_.isModified();
  }

  @Override
  public boolean isUIModified() {
    return uiChanged_;
  }

  @Override
  public void paramsModified(final DicoParamsChangeState _state) {
    fireProjectChanged();
  }

  @Override
  public void paramsStateLoadedEntiteChanged(final DicoParams _params, final DicoEntite _kw) {
    fireProjectChanged();
  }

  public void removeFudaaProjectListener(final FudaaProjectStateListener _l) {
    fudaaProjectListener_.remove(_l);
  }

  public boolean setLoaded(final DicoEntite _e, final File _f, final boolean _m) {
    return projectState_.setLoaded(_e, _f, _m);
  }

  public boolean setLoaded(final DicoEntite _e, final File _f, final boolean _m, final boolean _isProjectFile) {
    return projectState_.setLoaded(_e, _f, _m, _isProjectFile);
  }

  public void setLoadedModified(final DicoEntite _e, final boolean _isModified) {
    projectState_.setLoadedModified(_e, _isModified);
  }

  public boolean isModified() {
    return isUIModified() || isParamsModified();
  }

  public void setUIModified(final boolean _b) {
    if (_b != uiChanged_) {
      uiChanged_ = _b;
      fireProjectChanged();
    }
  }

  public void clearChanged() {
    setUIModified(false);
    setParamsModified(false);
  }

  public void setParamsModified(final boolean _b) {
    projectState_.setParamsModified(_b);
    fireProjectChanged();
  }

  public void setUnloaded(final DicoEntite _e) {
    projectState_.setUnloaded(_e);
  }

}
