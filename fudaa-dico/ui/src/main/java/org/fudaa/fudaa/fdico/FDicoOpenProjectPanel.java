package org.fudaa.fudaa.fdico;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import org.fudaa.dodico.dico.DicoManager;

public class FDicoOpenProjectPanel extends FDicoChooserPanel {

  public FDicoOpenProjectPanel(DicoManager m, boolean fileChooser, boolean ficGrid) {
    super(m, fileChooser, ficGrid);
  }

  public FDicoOpenProjectPanel(DicoManager m) {
    super(m);
  }

  @Override
  protected JComponent[] getDicoManagementUI() {
    List<JComponent> components = new ArrayList<JComponent>();

    this.addComponentsToList(components, this.getDictionnaireUI());
    this.addComponentsToList(components, this.getDicoChoiceUI());
    this.addComponentsToList(components, this.getLangueUI());

    return components.toArray(new JComponent[0]);
  }

  @Override
  public String getVersionSelected() {
    if (this.versionButton.isSelected()) {
      return super.getVersionSelected();
    } else {
      return null;
    }
  }

  public File getDicoFile() {
    if (this.fileButton.isSelected()) {
      return this.filePanel.getFile();
    } else {
      return null;
    }
  }
}
