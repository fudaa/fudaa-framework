/*
 *  @creation     20 oct. 2003
 *  @modification $Date: 2006-09-19 15:01:59 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.fdico;
import java.awt.Component;

import javax.swing.table.TableCellEditor;
/**
 *
 * @author deniger
 * @version $Id: FDicoEditorInterface.java,v 1.6 2006-09-19 15:01:59 deniger Exp $
 */
public interface FDicoEditorInterface extends TableCellEditor {
   Component getComponent();
   String getValue();
   void setValue(String _s);
}
