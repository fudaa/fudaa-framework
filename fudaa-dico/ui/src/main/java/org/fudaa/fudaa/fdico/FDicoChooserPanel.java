/**
 * @creation 18 ao�t 2003
 * @modification $Date: 2007-06-05 09:01:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.commun.DodicoResource;
import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoLanguage;
import org.fudaa.dodico.dico.DicoManager;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author deniger
 * @version $Id: FDicoChooserPanel.java,v 1.28 2007-06-05 09:01:13 deniger Exp $
 */
public class FDicoChooserPanel extends CtuluDialogPanel implements ItemListener {
  JRadioButton versionButton;
  JRadioButton fileButton;
  CtuluFileChooserPanel filePanel;
  BuLabelMultiLine lbWarn_;

  static class DicoDescCellRenderer extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
                                                  final boolean _isSelected, final boolean _cellHasFocus) {
      setComponentOrientation(_list.getComponentOrientation());
      if (_isSelected) {
        setBackground(_list.getSelectionBackground());
        setForeground(_list.getSelectionForeground());
      } else {
        setBackground(_list.getBackground());
        setForeground(_list.getForeground());
      }
      setEnabled(_list.isEnabled());
      setFont(_list.getFont());
      setBorder((_cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder") : noFocusBorder);
      final DicoCasFileFormat d = (DicoCasFileFormat) _value;
      setText(d == null ? "" : d.getName());
      return this;
    }
  }

  public static class FormatComboBoxModel extends AbstractListModel implements ComboBoxModel {
    List l_;
    DicoCasFileFormat selected_;

    public FormatComboBoxModel(final List _l) {
      l_ = _l;
      if (!l_.isEmpty()) {
        selected_ = (DicoCasFileFormat) l_.get(0);
      }
    }

    /**
     * Renvoie un objet du type DicoFileFormat.
     */
    @Override
    public Object getElementAt(final int _arg0) {
      return l_.get(_arg0);
    }

    public DicoCasFileFormat getFormatSelected() {
      return selected_;
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return l_.size();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (((_anItem != null) && (!_anItem.equals(selected_)) || ((_anItem == null) && selected_ != null))) {
        selected_ = (DicoCasFileFormat) _anItem;
        fireContentsChanged(this, -1, -1);
        saveLastCodeUsed(selected_.getName());
      }
    }
  }

  public static FormatComboBoxModel createModel(final DicoManager _mng) {
    final FormatComboBoxModel r = new FormatComboBoxModel(_mng.getFormats());
    selectFormat(r, getLastCodeUsed());
    return r;
  }

  public static String getLastCodeUsed() {
    return FDicoPreferences.FD.getStringProperty("last.dico.soft.used");
  }

  public static void saveLastCodeUsed(final String _code) {
    FDicoPreferences.FD.putStringProperty("last.dico.soft.used", _code);
  }

  public static void selectFormat(final FormatComboBoxModel _cbModel, final String _s) {
    if (_cbModel != null) {
      for (int i = _cbModel.getSize() - 1; i >= 0; i--) {
        final DicoCasFileFormat ft = (DicoCasFileFormat) _cbModel.getElementAt(i);
        if (ft.getName().equals(_s)) {
          _cbModel.setSelectedItem(ft);
          return;
        }
      }
    }
  }

  public static ListCellRenderer getDicoCellRenderer() {
    return new DicoDescCellRenderer();
  }

  protected JTextField fic_;
  protected JTextField ficGrid_;
  FormatComboBoxModel dicoChooser_;
  DicoManager dicoMng_;
  private DicoLanguage.LanguageComboBoxModel langChooser_;
  private DefaultComboBoxModel versionChooser_;
  final CtuluFileChooserPanel pnForFic_;
  protected final static String LABEL_END = " :";

  /**
   * @param _m les dico a prendre en compte
   */
  public FDicoChooserPanel(final DicoManager _m) {
    this(_m, true, false);
  }

  /**
   * @param _m
   * @param _fileChooser
   */
  public FDicoChooserPanel(final DicoManager _m, final boolean _fileChooser, final boolean _ficGrid) {
    dicoMng_ = _m;
    addEmptyBorder(10);
    final BuGridLayout lay = new BuGridLayout(2, 5, 5, true, false, true, true, true);
    lay.setXAlign(0f);
    setLayout(lay);
    if (_ficGrid) {
      ficGrid_ = addLabelFileChooserPanel(DodicoResource.DODICO.getString("Fichier s�rafin") + LABEL_END, null, false,
          false);
    }
    if (_fileChooser) {
      pnForFic_ = addFileChooserPanel(this, FDicoLib.getS("Fichier des param�tres") + LABEL_END, false, false);
      fic_ = pnForFic_.getTf();
    } else {
      pnForFic_ = null;
    }

    this.addComponentsToUI(this.getDicoManagementUI());
    setLastVersionSelected();
  }

  private void addComponentsToUI(JComponent[] components) {
    for (int i = 0; i < components.length; i++) {
      add(components[i]);
    }
  }

  protected JComponent[] getDictionnaireUI() {
    JComponent[] components = new JComponent[2];
    components[0] = new JLabel(FDicoLib.getS("Dictionnaire") + LABEL_END);
    JComboBox cb = new JComboBox();
    dicoChooser_ = new FormatComboBoxModel(dicoMng_.getFormats());
    cb.setModel(dicoChooser_);
    cb.setRenderer(new DicoDescCellRenderer());
    cb.addItemListener(this);
    components[1] = cb;

    return components;
  }

  protected final JComponent[] getVersionUI() {
    JComponent[] components = new JComponent[2];
    final DicoCasFileFormat f = (DicoCasFileFormat) dicoMng_.getFormats().get(0);
    dicoChooser_.setSelectedItem(f);
    components[0] = new JLabel(BuResource.BU.getString("Version") + LABEL_END);
    JComboBox cb = new BuComboBox();
    versionChooser_ = new DefaultComboBoxModel(new Vector(dicoMng_.getVersions(f)));
    cb.setModel(versionChooser_);

    components[1] = cb;

    return components;
  }

  protected JComponent[] getLangueUI() {
    JComponent[] components = new JComponent[2];

    components[0] = new BuLabel(BuResource.BU.getString("Langue") + LABEL_END);
    JComboBox cb = new JComboBox();
    langChooser_ = new DicoLanguage.LanguageComboBoxModel();
    cb.setModel(langChooser_);
    components[1] = cb;

    return components;
  }

  protected void addComponentsToList(final List<JComponent> list, JComponent[] components) {
    for (int i = 0; i < components.length; i++) {
      list.add(components[i]);
    }
  }

  protected JComponent[] getDicoChoiceUI() {
    JComponent[] components = new JComponent[4];
    JComponent[] versionUI = this.getVersionUI();
    versionButton = new JRadioButton(FDicoResource.FD.getString("Version du fichier dico") + LABEL_END);
    fileButton = new JRadioButton(FDicoResource.FD.getString("Charger un fichier dico") + LABEL_END);
    filePanel = new CtuluFileChooserPanel();
    DicoChoiceUIListener listener = new DicoChoiceUIListener(versionUI[1], filePanel);

    versionButton.setSelected(true);
    filePanel.setEnabled(false);

    BuFileFilter filter = new BuFileFilter("dico");
    filePanel.setExt(filter.getFirstExt());
    filePanel.setFilter(new FileFilter[]{filter});

    versionButton.setActionCommand(DicoChoiceUIListener.CHOICE1_CMD);
    fileButton.setActionCommand(DicoChoiceUIListener.CHOICE2_CMD);
    versionButton.addActionListener(listener);
    fileButton.addActionListener(listener);

    components[0] = versionButton;
    components[1] = versionUI[1];
    components[2] = fileButton;
    components[3] = filePanel;

    ButtonGroup group = new ButtonGroup();

    group.add(versionButton);
    group.add(fileButton);

    return components;
  }

  private class DicoChoiceUIListener implements ActionListener {
    public static final String CHOICE1_CMD = "CHOICE1";
    public static final String CHOICE2_CMD = "CHOICE2";
    private JComponent component1;
    private JComponent component2;

    public DicoChoiceUIListener(JComponent component1, JComponent component2) {
      this.component1 = component1;
      this.component2 = component2;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getActionCommand().equals(CHOICE1_CMD)) {
        this.component1.setEnabled(true);
        this.component2.setEnabled(false);
      } else if (e.getActionCommand().equals(CHOICE2_CMD)) {
        this.component1.setEnabled(false);
        this.component2.setEnabled(true);
      }
    }
  }

  protected JComponent[] getDicoManagementUI() {
    List<JComponent> components = new ArrayList<JComponent>();

    this.addComponentsToList(components, this.getDictionnaireUI());
    this.addComponentsToList(components, this.getDicoChoiceUI());
    this.addComponentsToList(components, this.getLangueUI());

    return components.toArray(new JComponent[0]);
  }

  /**
   * @return un thread permettant de mettre a jour le composant text du fichier.
   * @see org.fudaa.ctulu.gui.CtuluDialog#afficheDialog(boolean, Runnable)
   */
  public Runnable createRunnableForFileText() {
    return new Runnable() {
      /**
       * @see java.lang.Runnable#run()
       */
      @Override
      public void run() {
        Rectangle rect = null;
        try {
          rect = fic_.modelToView(fic_.getText().length());
        } catch (final BadLocationException _e) {
          FuLog.warning(_e);
        }
        if (rect != null) {
          fic_.scrollRectToVisible(rect);
        }
        if (ficGrid_ != null) {
          rect = null;
          try {
            rect = ficGrid_.modelToView(ficGrid_.getText().length());
          } catch (final BadLocationException _e) {
            FuLog.warning(_e);
          }
          if (rect != null) {
            ficGrid_.scrollRectToVisible(rect);
          }
        }
      }
    };
  }

  /**
   * @return le textfield comportant le chemin du fichier cas
   */
  public JTextField getFileField() {
    return fic_;
  }

  /**
   * @return le textfield comportant le chemin du fichier serafin
   */
  public JTextField getGridFileField() {
    return ficGrid_;
  }

  @Override
  public boolean isDataValid() {
    final String casFile = fic_.getText();
    String err = null;
    if (CtuluLibString.isEmpty(casFile)) {
      err = FDicoLib.getS("Le nom du fichier cas est vide");
    } else {
      final File cas = new File(casFile).getAbsoluteFile();
      if (cas.isDirectory()) {
        err = FDicoLib.getS("Le nom du fichier cas est invalide: c'est le nom d'un dossier");
      }
    }
    if (fileButton.isSelected() && filePanel.getFile() == null) {
      err = FDicoResource.FD.getString("Vous devez choisir un fichier dico existant");
    }
    if (err == null) {
      cancelErrorText();
    } else {
      setErrorText(err);
    }
    return err == null;
  }

  /**
   * @return le format selectionne
   */
  public DicoCasFileFormat getFileFormatSelected() {
    return (DicoCasFileFormat) dicoChooser_.getSelectedItem();
  }

  /**
   * @return le langage
   */
  public int getLanguageSelected() {
    return langChooser_.getSelectedLanguage();
  }

  /**
   * @return Le fichier selectionne
   */
  public File getSelectedFile() {
    return new File(fic_.getText());
  }

  /**
   * @return la version selectionnee
   */
  public String getVersionSelected() {
    return (String) versionChooser_.getSelectedItem();
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.SELECTED) {
      versionChooser_.removeAllElements();
      final List<String> vs = dicoMng_.getVersions(dicoChooser_.getFormatSelected());
      vs.forEach(version -> versionChooser_.addElement(version));
    }
    setLastVersionSelected();
  }

  /**
   * @param _f le fichier a afficher par defaut
   */
  public void setDefaultFile(final String _f) {
    fic_.setText(_f);
  }

  /**
   * @param _f
   */
  public void setDicoSelected(final DicoCasFileFormat _f) {
    if (dicoChooser_ != null) {
      dicoChooser_.setSelectedItem(_f);
    }
  }

  public void setSelected(final FileFormatSoftware _ft) {
    if (_ft == null) {
      return;
    }
    setDicoSelected(dicoMng_.getFileFormat(_ft.soft_));
    langChooser_.setSelectedLanguageFromLocal(_ft.language_);
    final int idx = CtuluLibArray.getIndex(_ft.version_, versionChooser_);
    if (idx < 0) {
      setLastVersionSelected();
    } else {
      versionChooser_.setSelectedItem(versionChooser_.getElementAt(idx));
    }
  }

  private void setLastVersionSelected() {
    if (versionChooser_.getSize() > 0) {
      versionChooser_.setSelectedItem(versionChooser_.getElementAt(versionChooser_.getSize() - 1));
    }
  }

  /**
   * @param _f le chemin absolu du fichier
   */
  public void setSelectedFic(final String _f) {
    final String s = _f;
    fic_.setText(s);
  }
}
