/**
 * @file TrDicoTableEditorChooser.java
 * @creation 13 mai 2003
 * @modification $Date: 2007-05-04 13:59:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import org.fudaa.ctulu.gui.*;
import org.fudaa.dodico.dico.DicoComponentVisitor;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

/**
 * @author deniger
 * @version $Id: FDicoTableEditorChooser.java,v 1.17 2007-05-04 13:59:04 deniger Exp $
 */
public final class FDicoTableEditorChooser implements DicoComponentVisitor {
  FDicoEditorInterface editor_;
  MultiDialogEditor multiEditor_;
  FDicoMultiEntiteViewer dialog_;
  ChoixTypeEditor choixEditor_;
  DicoDefaultEditor simpleEditor_;
  BuTextField simpleEditorTextField_;
  DicoBooleanEditor booleanCellEditor_;
  DicoFileEditor fileEditor_;

  public FDicoTableEditorChooser() {
    simpleEditorTextField_ = new BuTextField();
    simpleEditor_ = new DicoDefaultEditor(simpleEditorTextField_);
    editor_ = simpleEditor_;
  }

  public FDicoEditorInterface getEditor(final DicoEntite _ent) {
    _ent.accept(this);
    return editor_;
  }

  public FDicoEditorInterface getEditor(final DicoDataType _ent) {
    _ent.accept(this);
    return editor_;
  }

  @Override
  public void visitBinaire(final DicoDataType.Binaire _data) {
    if (booleanCellEditor_ == null) {
      booleanCellEditor_ = new DicoBooleanEditor();
    }
    editor_ = booleanCellEditor_;
  }

  @Override
  public void visitChaine(final DicoDataType.Chaine _data) {
    if (_data.isChoiceEnable()) {
      computeChoiceEditor(_data);
    } else if (_data.isFileType()) {
      if (fileEditor_ == null) {
        fileEditor_ = new DicoFileEditor();
        fileEditor_.setFileChooserSelectionMode(JFileChooser.FILES_ONLY);
      }
      editor_ = fileEditor_;
    } else {
      simpleEditorTextField_.setCharValidator(null);
      editor_ = simpleEditor_;
    }
  }

  @Override
  public void visitEntier(final DicoDataType.Entier _data) {
    if (_data.isChoiceEnable()) {
      computeChoiceEditor(_data);
    } else {
      if (_data.isControle() && _data.getControleMinValue() >= 0) {
        simpleEditorTextField_.setCharValidator(BuCharValidator.INTEGER);
      } else {
        simpleEditorTextField_.setCharValidator(BuCharValidator.INTEGER_NEGATIVE);
      }
      editor_ = simpleEditor_;
    }
  }

  private void computeChoiceEditor(final DicoDataType.ChoixType _type) {
    if (choixEditor_ == null) {
      choixEditor_ = new ChoixTypeEditor();
    }
    choixEditor_.setKeysValues(_type);
    editor_ = choixEditor_;
  }

  @Override
  public void visitReel(final DicoDataType.Reel _data) {
    simpleEditorTextField_.setCharValidator(DICO_DOUBLE_CHAR_VALIDATOR);
    editor_ = simpleEditor_;
  }

  @Override
  public void visitSimple(final DicoEntite.Simple _ent) {
    _ent.getType().accept(this);
  }

  @Override
  public void visitTableau(final DicoEntite.Tableau _ent) {
    dialog_ = new FDicoMultiEntiteViewer();
    final CtuluDialog s = new CtuluDialog(dialog_);
    s.setTitle(_ent.getNom());
    multiEditor_ = new MultiDialogEditor(s, null);
    dialog_.setVecteurEntite(_ent);
    editor_ = multiEditor_;
  }

  @Override
  public void visitVecteur(final DicoEntite.Vecteur _ent) {
    dialog_ = new FDicoMultiEntiteViewer();
    final CtuluDialog s = new CtuluDialog(dialog_);
    s.setTitle(_ent.getNom());
    multiEditor_ = new MultiDialogEditor(s, null);
    dialog_.setVecteurEntite(_ent);

    editor_ = multiEditor_;
  }

  public static final BuCharValidator DICO_DOUBLE_CHAR_VALIDATOR = new BuCharValidator() {
    @Override
    public boolean isCharValid(final char _char) {
      return Character.isDigit(_char) || (".,+-EedD".indexOf(_char) >= 0);
    }
  };
  public static final BuStringValidator DICO_DOUBLE_STRING_VALIDATOR = new BuStringValidator() {
    @Override
    public boolean isStringValid(final String _string) {
      return getDouble(_string) != null;
    }

    public Double getDouble(final String _s) {
      Double d = null;
      try {
        d = new Double(_s.replace(',', '.').replace('d', 'E').replace('D', 'E'));
      } catch (Exception ex) {
      }
      return d;
    }

    @Override
    public Object stringToValue(final String _string) {
      return getDouble(_string);
    }
  };

  private static final class MultiDialogEditor extends CtuluCellDialogEditor implements FDicoEditorInterface {
    public MultiDialogEditor(final CtuluDialog _dialog, final CtuluCellDecorator _deco) {
      super(_dialog, _deco);
    }

    @Override
    public Component getComponent() {
      return this;
    }

    @Override
    public String getValue() {
      return (String) super.getCellEditorValue();
    }

    @Override
    public void setValue(String _s) {
      super.setText(_s);
    }
  }

  public static final class DicoFileEditor extends CtuluCellFileEditor implements FDicoEditorInterface {
    @Override
    public Component getComponent() {
      return this;
    }
  }

  public static final class DicoBooleanEditor extends CtuluCellBooleanEditor implements FDicoEditorInterface {
    @Override
    public Component getComponent() {
      return editorComponent;
    }

    @Override
    public boolean getBooleanValue(final Object _value) {
      return DicoDataType.Binaire.getValue((String) _value);
    }

    @Override
    public Object getObjectValue(final boolean _o) {
      return _o ? DicoDataType.Binaire.TRUE_VALUE : DicoDataType.Binaire.FALSE_VALUE;
    }

    @Override
    public String getValue() {
      return (String) getCellEditorValue();
    }

    @Override
    public void setValue(final String _s) {
      ((JCheckBox) editorComponent).setSelected(DicoDataType.Binaire.getValue(_s));
    }
  }

  private static final class DicoDefaultEditor extends DefaultCellEditor implements FDicoEditorInterface {
    public DicoDefaultEditor(final JTextField _c) {
      super(_c);
    }

    @Override
    public String getValue() {
      return (String) super.getCellEditorValue();
    }

    @Override
    public void setValue(final String _s) {
      delegate.setValue(_s);
    }
  }

  public static final class ChoixTypeComboBoxModel extends AbstractListModel implements ComboBoxModel {
    /**
     * On utilise une arrayList et non une list car on veut utiliser la m�thode ensureCapacity
     */
    ArrayList objets_;
    Object selected_;
    DicoDataType.ChoixType keys_;

    public ChoixTypeComboBoxModel() {
      objets_ = new ArrayList(10);
    }

    /**
     * @param _l la liste des valeurs a enlever
     */
    public void setForbiddenValues(final List _l) {
      if (keys_ == null) {
        return;
      }
      synchronized (objets_) {
        objets_.clear();
        objets_.ensureCapacity(keys_.getNbChoice());
        keys_.addChoiceKeys(objets_);
        objets_.removeAll(_l);
        fireIntervalAdded(this, 0, objets_.size());
        if (selected_ != null && (!objets_.contains(selected_))) {
          setSelectedItem(null);
        }
      }
    }

    public void refill(final DicoDataType.ChoixType _data) {
      synchronized (objets_) {
        keys_ = _data;
        objets_.clear();
        objets_.ensureCapacity(keys_.getNbChoice());
        keys_.addChoiceKeys(objets_);
      }
      fireIntervalAdded(this, 0, objets_.size());
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if ((selected_ != null && !selected_.equals(_anItem)) || selected_ == null && _anItem != null) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    @Override
    public Object getElementAt(final int _idx) {
      return objets_.get(_idx);
    }

    @Override
    public int getSize() {
      return objets_.size();
    }
  }

  public static final class ChoixTypeEditor extends DefaultCellEditor implements FDicoEditorInterface {
    ChoixTypeComboBoxModel model_;
    FDicoTableRendererChooser.ChoixRenderer renderer_;

    public ChoixTypeComboBoxModel getModel() {
      return model_;
    }

    public ChoixTypeEditor() {
      super(new JComboBox());
      model_ = new ChoixTypeComboBoxModel();
      final JComboBox cb = (JComboBox) editorComponent;
      cb.setModel(model_);
      renderer_ = new FDicoTableRendererChooser.ChoixRenderer();
      cb.setRenderer(renderer_);
    }

    public void setKeysValues(final DicoDataType.ChoixType _data) {
      final JComboBox cb = (JComboBox) editorComponent;
      if (_data.isEditable()) {
        cb.setEditable(true);
        cb.setEditor(new BasicComboBoxEditor());
      } else {
        cb.setEditable(false);
        // cb.setEditor(null);
      }
      model_.refill(_data);
      renderer_.setData(_data);
    }

    @Override
    public String getValue() {
      return (String) model_.getSelectedItem();
    }

    @Override
    public void setValue(final String _s) {
      model_.setSelectedItem(_s);
    }
  }

  public static TableCellEditor createCellEditor(final DicoEntite _ent) {
    return new FDicoTableEditorChooser().getEditor(_ent);
  }

  public static TableCellEditor createCellEditor(final DicoDataType _ent) {
    return new FDicoTableEditorChooser().getEditor(_ent);
  }

  public static TableCellEditor createCellEditorChooser(final FDicoEntiteTableModel _model) {
    return new EntiteCellEditorChooser(_model);
  }

  public static class EditorCreator implements DicoComponentVisitor {
    FDicoEditorInterface editor_;

    public EditorCreator() {
      editor_ = null;
    }

    public FDicoEditorInterface createEditor(final DicoEntite _ent) {
      _ent.accept(this);
      return editor_;
    }

    public FDicoEditorInterface createEditor(final DicoDataType _ent) {
      _ent.accept(this);
      return editor_;
    }

    @Override
    public void visitBinaire(final DicoDataType.Binaire _data) {
      editor_ = new DicoBooleanEditor();
    }

    @Override
    public void visitChaine(final DicoDataType.Chaine _data) {
      if (_data.isChoiceEnable()) {
        computeChoiceEditor(_data);
      } else {
        final BuTextField txt = new BuTextField(10);
        txt.setCharValidator(null);
        editor_ = new DicoDefaultEditor(txt);
      }
    }

    @Override
    public void visitEntier(final DicoDataType.Entier _data) {
      if (_data.isChoiceEnable()) {
        computeChoiceEditor(_data);
      } else {
        final BuTextField txt = new BuTextField(10);
        if (_data.isControle() && _data.getControleMinValue() >= 0) {
          txt.setCharValidator(BuCharValidator.INTEGER);
        } else {
          txt.setCharValidator(BuCharValidator.INTEGER_NEGATIVE);
        }
        editor_ = new DicoDefaultEditor(txt);
      }
    }

    private void computeChoiceEditor(final DicoDataType.ChoixType _type) {
      final ChoixTypeEditor choixEditor = new ChoixTypeEditor();
      choixEditor.setKeysValues(_type);
      editor_ = choixEditor;
    }

    @Override
    public void visitReel(final DicoDataType.Reel _data) {
      final BuTextField txt = new BuTextField(10);
      txt.setCharValidator(DICO_DOUBLE_CHAR_VALIDATOR);
      editor_ = new DicoDefaultEditor(txt);
    }

    @Override
    public void visitSimple(final DicoEntite.Simple _ent) {
      _ent.getType().accept(this);
    }

    @Override
    public void visitTableau(final DicoEntite.Tableau _ent) {
      final FDicoMultiEntiteViewer dialog = new FDicoMultiEntiteViewer();
      dialog.setVecteurEntite(_ent);
      final CtuluDialog s = new CtuluDialog(dialog);
      s.setTitle(_ent.getNom());
      editor_ = new MultiDialogEditor(s, null);
    }

    @Override
    public void visitVecteur(final DicoEntite.Vecteur _ent) {
      final FDicoMultiEntiteViewer dialog = new FDicoMultiEntiteViewer();
      dialog.setVecteurEntite(_ent);
      final CtuluDialog s = new CtuluDialog(dialog);
      s.setTitle(_ent.getNom());
      editor_ = new MultiDialogEditor(s, null);
    }
  }

  private static class EntiteCellEditorChooser implements TableCellEditor {
    private TableCellEditor editor_;
    private final FDicoEntiteTableModel model_;
    private final FDicoTableEditorChooser chooser_;

    public EntiteCellEditorChooser(final FDicoEntiteTableModel _model) {
      chooser_ = new FDicoTableEditorChooser();
      model_ = _model;
    }

    @Override
    public boolean shouldSelectCell(final EventObject _anEvent) {
      return true;
    }

    @Override
    public boolean stopCellEditing() {
      return editor_.stopCellEditing();
    }

    @Override
    public void cancelCellEditing() {
      editor_.cancelCellEditing();
    }

    @Override
    public void addCellEditorListener(final CellEditorListener _l) {
      editor_.addCellEditorListener(_l);
    }

    @Override
    public void removeCellEditorListener(final CellEditorListener _l) {
      editor_.removeCellEditorListener(_l);
    }

    @Override
    public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                 final int _row, final int _column) {
      editor_ = chooser_.getEditor(model_.getEntite(_row));
      if (editor_ == chooser_.fileEditor_) {
        chooser_.fileEditor_.setBaseDir(model_.getDirBase());
      }
      return editor_.getTableCellEditorComponent(_table, _value, _isSelected, _row, _column);
    }

    @Override
    public Object getCellEditorValue() {
      return editor_.getCellEditorValue();
    }

    @Override
    public boolean isCellEditable(final EventObject _anEvent) {
      return true;
    }
  }
}
