/*
 *  @file         TrDicoTableColumn.java
 *  @creation     15 mai 2003
 *  @modification $Date: 2006-09-19 15:02:01 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
/**
 * @author deniger
 * @version $Id: FDicoTableColumn.java,v 1.3 2006-09-19 15:02:01 deniger Exp $
 */
public class FDicoTableColumn extends TableColumn {
  public FDicoTableColumn(final FDicoTableColumnIdentifier _id, final int _modelIndex) {
    this(_id, _modelIndex, null, null);
  }
  public FDicoTableColumn(
    final FDicoTableColumnIdentifier _id,
    final int _modelIndex,
    final TableCellRenderer _r,
    final TableCellEditor _e) {
    super(_modelIndex, 75, _r, _e);
    headerValue= _id.getNom();
    identifier= _id;
  }
  public FDicoTableColumnIdentifier getTrIdentifier() {
    return (FDicoTableColumnIdentifier)getIdentifier();
  }
  @Override
  public void setHeaderValue(final Object _headerValue) {
    throw new IllegalAccessError("setHeaderValue impossible");
  }
  @Override
  public void setIdentifier(final Object _identifier) {
    throw new IllegalAccessError("setIdentifier impossible");
  }
  @Override
  public void setModelIndex(final int _modelIndex) {
    throw new IllegalAccessError("setModelIndex impossible");
  }
}
