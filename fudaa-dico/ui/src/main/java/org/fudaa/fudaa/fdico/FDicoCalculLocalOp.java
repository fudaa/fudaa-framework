/*
 * @creation 13 juin 2003
 * @modification $Date: 2006-09-19 15:02:00 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.dico.TelemacExec;
import org.fudaa.fudaa.commun.calcul.FudaaCalculOp;

import java.io.File;

/**
 * @author deniger
 */
public class FDicoCalculLocalOp extends FudaaCalculOp {
  private FDicoProjetInterface projetInterface;

  public FDicoCalculLocalOp(final FDicoProjetInterface projet, final TelemacExec telemacExec) {
    super(telemacExec);
    projetInterface = projet;
  }

  @Override
  public File proceedParamFile(final ProgressionInterface progressionInterface) {
    projetInterface.save(progressionInterface);
    return projetInterface.getParamsFile();
  }

  @Override
  protected String getTaskName() {
    return FDicoLib.getS("lancement") + CtuluLibString.ESPACE + projetInterface.getCodeName();
  }
}
