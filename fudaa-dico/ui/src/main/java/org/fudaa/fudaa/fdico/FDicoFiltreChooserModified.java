/*
 *  @file         Modified.java
 *  @creation     5 sept. 2003
 *  @modification $Date: 2006-09-19 15:02:00 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import javax.swing.JMenuItem;

import com.memoire.bu.BuMenuItem;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParamsInterface;
/**
 * @author Fred Deniger
 * @version $Id: FDicoFiltreChooserModified.java,v 1.10 2006-09-19 15:02:00 deniger Exp $
 */
public final class FDicoFiltreChooserModified
  extends FDicoFiltreChooserSimple {
  DicoParamsInterface model_;
  private final static int MODIFIED_ID= 1;
  private final static int ERREUR_ID= 2;
  public FDicoFiltreChooserModified(final DicoParamsInterface _model) {
    super(
      new String[] {
        CtuluLib.getS("Non modifi�"),
        CtuluLib.getS("Modifi�"),
        CtuluLib.getS("Erreur")});
    model_= _model;
  }
  @Override
  public String getName() {
    return FDicoLib.getS("Etat");
  }
  @Override
  public boolean accept(final DicoEntite _ent) {
    if (indexSelected_ >= 0) {
      if (indexSelected_ == FDicoFiltreChooserModified.ERREUR_ID) {
        return !model_.isValueValideFor(_ent);
      }
     final  boolean isEntiteModified= model_.isValueSetFor(_ent);
      return indexSelected_ == FDicoFiltreChooserModified.MODIFIED_ID
        ? isEntiteModified
        : !isEntiteModified;
    }
    return true;
  }
  @Override
  public JMenuItem[] getActionFor(final DicoEntite _ent) {
    int index= 0;
    JMenuItem[] r;
    if (model_.isDataValide()) {
      r= new BuMenuItem[2];
    } else {
      index++;
      r= new BuMenuItem[3];
      r[0]= new BuMenuItem(FDicoLib.getS("Mot-cl�s erron�s"));
      r[0].setActionCommand(
        CtuluLibString.getString(FDicoFiltreChooserModified.ERREUR_ID));
      r[0].addActionListener(this);
    }
    r[index]= new BuMenuItem(FDicoLib.getS("Mot-cl�s modifi�s"));
    r[index].setActionCommand(
      CtuluLibString.getString(FDicoFiltreChooserModified.MODIFIED_ID));
    r[index++].addActionListener(this);
    r[index]= new BuMenuItem(FDicoLib.getS("Mot-cl�s non modifi�s"));
    r[index].setActionCommand(CtuluLibString.getString(0));
    r[index++].addActionListener(this);
    return r;
  }
}
