/*
 *  @file         FDicoEntiteFileTableModelInterface.java
 *  @creation     4 nov. 2003
 *  @modification $Date: 2006-09-19 15:02:00 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;
import javax.swing.table.TableModel;

import org.fudaa.ctulu.CtuluCommandManager;

import org.fudaa.dodico.dico.DicoEntite;
/**
 * @author deniger
 * @version $Id: FDicoEntiteFileTableModelInterface.java,v 1.9 2006-09-19 15:02:00 deniger Exp $
 */
public interface FDicoEntiteFileTableModelInterface extends TableModel {
   String getAbsolutePath(DicoEntite _f);
   void setCmdMng(CtuluCommandManager _manager);
   int indexOf(DicoEntite _e);
   void editer(int _i);
   DicoEntite getEntite(int _row);
   void remove(int[] _row);
}
