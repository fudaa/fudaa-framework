/*
 *  @file         TrDicoFiltreFiltreChooserController.java
 *  @creation     21 mai 2003
 *  @modification $Date: 2007-05-04 13:59:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.fdico;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import com.memoire.bu.BuVerticalLayout;
import javax.swing.JComponent;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

import org.fudaa.dodico.dico.DicoEntite;

/**
 * @author deniger
 * @version $Id: FDicoFiltreController.java,v 1.12 2007-05-04 13:59:04 deniger Exp $
 */
public class FDicoFiltreController implements Observer, FDicoFiltre, ActionListener {

  /**
   * @author fred deniger
   * @version $Id: FDicoFiltreController.java,v 1.12 2007-05-04 13:59:04 deniger Exp $
   */
  static final class FiltreCellRenderer extends CtuluCellTextRenderer {

    @Override
    public Component getListCellRendererComponent(JList _list, Object _value, int _index, boolean _isSelected,
            boolean _cellHasFocus) {
      super.getListCellRendererComponent(_list, _value, _index, _isSelected, _cellHasFocus);
      setToolTipText((String) _value);
      return this;
    }
  }

  public interface FiltreControllerListener {

    void controllerChanged();

    void controllerMoreRestrictive(FDicoFiltre _f);
  }
  private List filtreChoosers_;
  private FiltreControllerListener listener_;
  private FDicoFiltre tempoFiltre_;
  private boolean stopEvent_;

  public FDicoFiltreController() {
    this(null);
  }

  public FDicoFiltreController(final FiltreControllerListener _listener) {
    listener_ = _listener;
    filtreChoosers_ = new ArrayList(10);
  }

  public void setListener(final FiltreControllerListener _l) {
    listener_ = _l;
  }

  public void add(final FDicoFiltreChooserAbstract _f) {
    synchronized (filtreChoosers_) {
      if (!filtreChoosers_.contains(_f)) {
        filtreChoosers_.add(_f);
        if ((listener_ != null) && (!_f.isFiltreEmpty())) {
          listener_.controllerMoreRestrictive(_f);
        }
        _f.addObserver(this);
      }
    }
  }

  public void setTempoFiltre(final FDicoFiltre _f) {
    setFiltresEmpty();
    tempoFiltre_ = _f;
    if (listener_ != null) {
      listener_.controllerChanged();
    }
  }

  public void remove(final FDicoFiltreChooserAbstract _f) {
    synchronized (filtreChoosers_) {
      if (filtreChoosers_.contains(_f)) {
        filtreChoosers_.remove(_f);
        if ((listener_ != null) && (!_f.isFiltreEmpty())) {
          listener_.controllerChanged();
        }
        _f.deleteObserver(this);
      }
    }
  }
  private JPanel targetPanel;

  public JPanel getTargetPanel() {
    return targetPanel;
  }

  public void setTargetPanel(final JPanel _p) {
    this.targetPanel = _p;
    final ListCellRenderer renderer = new FiltreCellRenderer();
    final Font f = new Font("SansSerif", Font.PLAIN, 10);
    Component[] components = _p.getComponents();
    for (Component component : components) {
      if (Boolean.TRUE.equals(((JComponent) component).getClientProperty("IS_FILTER"))) {
        _p.remove(component);
      }
    }
    for (final Iterator it = filtreChoosers_.iterator(); it.hasNext();) {
      final FDicoFiltreChooserAbstract filtre = (FDicoFiltreChooserAbstract) it.next();
      final JPanel buildPanel = filtre.buildPanel(f, renderer);
      buildPanel.putClientProperty("IS_FILTER", Boolean.TRUE);
      _p.add(buildPanel);
    }

  }

  public JPanel buildPanel() {
    final JPanel r = new JPanel();
    r.setLayout(new BuVerticalLayout(5, true, false));
    setTargetPanel(r);
    return r;
  }

  public void setFiltresEmpty() {
    stopEvent_ = true;
    boolean change = false;
    for (final Iterator it = filtreChoosers_.iterator(); it.hasNext();) {
      final FDicoFiltreChooserAbstract f = (FDicoFiltreChooserAbstract) it.next();
      if (!f.isFiltreEmpty()) {
        change = true;
        f.clearFiltre();
      }
    }
    stopEvent_ = false;
    if ((change) && (listener_ != null)) {
      listener_.controllerChanged();
    }
  }

  @Override
  public void update(final Observable _o, final Object _arg) {
    if (stopEvent_) {
      return;
    }
    if (listener_ == null) {
      return;
    }
    if (filtreChoosers_.contains(_o)) {
      if (tempoFiltre_ != null) {
        tempoFiltre_ = null;
        listener_.controllerChanged();
      } else if (_arg == Boolean.TRUE) {
        listener_.controllerMoreRestrictive((FDicoFiltre) _o);
        // if (FudaaLib.DEBUG)
        // FudaaLib.debug(getClass().getName()+" more restrictive");
      } else {
        listener_.controllerChanged();
      }
    }
  }

  public void updateFiltreModifiedChanged() {
    if (isFiltreModifieActive() >= 0) {
      listener_.controllerChanged();
    }

  }

  @Override
  public boolean accept(final DicoEntite _ent) {
    for (final Iterator it = filtreChoosers_.iterator(); it.hasNext();) {
      final FDicoFiltreChooserAbstract f = (FDicoFiltreChooserAbstract) it.next();
      if ((!f.isFiltreEmpty()) && (!f.accept(_ent))) {
        return false;
      }
    }
    return true;
  }

  public void filtre(final List _l) {
    synchronized (_l) {
      if (tempoFiltre_ == null) {
        for (final Iterator it = filtreChoosers_.iterator(); it.hasNext();) {
          final FDicoFiltreChooserAbstract f = (FDicoFiltreChooserAbstract) it.next();
          if (!f.isFiltreEmpty()) {
            FDicoFiltreChooserAbstract.filtreList(_l, f);
          }
        }
      } else {
        FDicoFiltreChooserAbstract.filtreList(_l, tempoFiltre_);
      }
    }
  }

  public int isFiltreModifieActive() {
    for (int i = 0; i < filtreChoosers_.size(); i++) {

      final FDicoFiltreChooserAbstract f = (FDicoFiltreChooserAbstract) filtreChoosers_.get(i);
      if (!f.isFiltreEmpty() && f instanceof FDicoFiltreChooserModified) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public boolean isSameFiltre(final FDicoFiltre _f) {
    return _f == this;
  }

  public JMenuItem[] getMenuFiltre(final DicoEntite _ent) {
    final ArrayList r = new ArrayList();
    JMenuItem[] items;
    for (final Iterator it = filtreChoosers_.iterator(); it.hasNext();) {
      final FDicoFiltreChooserAbstract f = (FDicoFiltreChooserAbstract) it.next();
      items = f.getActionFor(_ent);
      if (items != null) {
        r.addAll(Arrays.asList(items));
      }
    }
    if (r.size() > 0) {
      return (JMenuItem[]) r.toArray(new JMenuItem[r.size()]);
    }
    return null;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String com = _e.getActionCommand();
    if ("NONE".equals(com)) {
      setFiltresEmpty();
    }
  }
}
