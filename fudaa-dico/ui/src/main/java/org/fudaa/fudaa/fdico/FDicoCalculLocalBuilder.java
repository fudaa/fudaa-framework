/*
 * @creation 13 juin 2003
 * @modification $Date: 2007-05-04 13:59:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.fdico;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoManager;
import org.fudaa.dodico.dico.DicoVersionManager;
import org.fudaa.dodico.dico.TelemacExec;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.List;
import java.util.*;

/**
 * @author deniger
 * @version $Id: FDicoCalculLocalBuilder.java,v 1.16 2007-05-04 13:59:04 deniger Exp $
 */
public class FDicoCalculLocalBuilder extends CtuluDialogPanel {
  /**
   * Pour qu'un bouton puisse d�s�lectionnable on a ajoute une checkbox imaginaire.
   *
   * @author Fred Deniger
   * @version $Id: FDicoCalculLocalBuilder.java,v 1.16 2007-05-04 13:59:04 deniger Exp $
   */
  private static class DefaultButtonGroup extends ButtonGroup {
    // Cb imaginaire
    final JCheckBox cbInutile_ = new JCheckBox();

    /**
     * Ajoute la cb imaginaire.
     */
    public DefaultButtonGroup() {
      add(cbInutile_);
    }

    @Override
    public void setSelected(final ButtonModel _m, final boolean _b) {
      if (!_b && _m != null && _m == super.getSelection()) {
        cbInutile_.setSelected(true);
      } else {
        super.setSelected(_m, _b);
      }
    }
  }

  /**
   * option type booleen.
   *
   * @author Fred Deniger
   * @version $Id: FDicoCalculLocalBuilder.java,v 1.16 2007-05-04 13:59:04 deniger Exp $
   */
  public static class Option {
    BuCheckBox cb_;
    String info_;
    String lb_;
    String opt_;

    /**
     * @param _opt l'option a ajouter (-s par exemple) : requis
     * @param _lb le label de l'option : requis
     * @param _info l'info plus compl�te
     */
    public Option(final String _opt, final String _lb, final String _info) {
      opt_ = _opt;
      lb_ = _lb;
      info_ = _info;
    }

    public void addExecArg(final List _argList) {
      if (cb_ != null && cb_.isSelected()) {
        _argList.add(opt_);
      }
    }

    public void buildView(final JPanel _dest) {
      final JLabel lb = new BuLabel();
      lb.setText(getLabel());
      lb.setToolTipText(getInfo());
      _dest.add(lb);
      final JComponent c = getEditor();
      if (c == null) {
        _dest.add(getBooleanEditor());
      } else {
        _dest.add(getEditor());
      }
    }

    public JCheckBox getBooleanEditor() {
      if (cb_ == null) {
        cb_ = new BuCheckBox();
        cb_.setToolTipText(FDicoLib.getS("Activer/D�sactiver l'option"));
      }
      return cb_;
    }

    /**
     * @return le composant d'�dition
     */
    public JComponent getEditor() {
      return null;
    }

    public String getGroupId() {
      return null;
    }

    public String getInfo() {
      return info_;
    }

    public String getLabel() {
      return lb_;
    }

    public boolean isDataValid() {
      return true;
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: FDicoCalculLocalBuilder.java,v 1.16 2007-05-04 13:59:04 deniger Exp $
   */
  public static class OptionEditTime extends OptionTime implements ItemListener {
    /**
     * @author fred deniger
     * @version $Id: FDicoCalculLocalBuilder.java,v 1.16 2007-05-04 13:59:04 deniger Exp $
     */
    static final class MaxValueValidator extends BuValueValidator {
      /**
       *
       */
      private final int max_;

      /**
       * @param _max
       */
      MaxValueValidator(final int _max) {
        max_ = _max;
      }

      @Override
      public boolean isValueValid(final Object _value) {
        if (_value == null) {
          return false;
        }
        int h = -1;
        if (_value instanceof Integer) {
          h = ((Integer) _value).intValue();
        } else {
          try {
            h = Integer.parseInt(_value.toString());
          } catch (final NumberFormatException e) {
            return false;
          }
        }
        return h >= 0 && h < max_;
      }
    }

    BuTextField ftHour_;
    BuTextField ftMin_;
    JLabel lbOption_;
    BuPanel pn_;

    /**
     * @param _opt
     * @param _lb
     * @param _info
     */
    public OptionEditTime(final String _opt, final String _lb, final String _info) {
      super(_opt, _lb, _info);
    }

    @Override
    public void buildView(final JPanel _dest) {
      if (lbOption_ != null) {
        return;
      }
      lbOption_ = new BuLabel();
      lbOption_.setText(getLabel());
      lbOption_.setToolTipText(getInfo());
      _dest.add(lbOption_);
      final JComponent c = getEditor();
      if (c == null) {
        _dest.add(getBooleanEditor());
      } else {
        _dest.add(getEditor());
      }
    }

    @Override
    public JCheckBox getBooleanEditor() {
      if (cb_ == null) {
        cb_ = new BuCheckBox();
        cb_.setToolTipText(FDicoLib.getS("Activer/D�sactiver l'option"));
        cb_.addItemListener(this);
      }
      return cb_;
    }

    @Override
    public JComponent getEditor() {
      if (pn_ == null) {
        pn_ = new BuPanel();
        pn_.setLayout(new BuHorizontalLayout(1));
        pn_.add(getBooleanEditor());
        ftHour_ = BuTextField.createIntegerField();
        ftHour_.setToolTipText(FDicoLib.getS("Pr�ciser l'heure entre 0 et 23"));
        ftHour_.setValueValidator(createValidator(24));
        ftMin_ = BuTextField.createIntegerField();
        ftMin_.setToolTipText(FDicoLib.getS("Pr�ciser les minutes entre 0 et 59"));
        ftMin_.setValueValidator(createValidator(60));
        ftHour_.setColumns(2);
        ftMin_.setColumns(2);
        pn_.add(ftHour_);
        pn_.add(new BuLabel(":"));
        pn_.add(ftMin_);
        ftHour_.setEnabled(false);
        ftMin_.setEnabled(false);
      }
      return pn_;
    }

    private BuValueValidator createValidator(final int _max) {
      return new MaxValueValidator(_max);
    }

    @Override
    public boolean isDataValid() {
      final boolean r = (cb_.isSelected()) ? (ftHour_.getValueValidator().isValueValid(ftHour_.getValue()) && ftMin_.getValueValidator().isValueValid(
          ftMin_.getValue())) : true;
      lbOption_.setForeground(r ? Color.BLACK : Color.RED);
      return r;
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      ftHour_.setEnabled(cb_.isSelected());
      ftMin_.setEnabled(cb_.isSelected());
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: FDicoCalculLocalBuilder.java,v 1.16 2007-05-04 13:59:04 deniger Exp $
   */
  public static class OptionTime extends Option {
    /**
     * @param _opt
     * @param _lb
     * @param _info
     */
    public OptionTime(final String _opt, final String _lb, final String _info) {
      super(_opt, _lb, _info);
    }

    @Override
    public final String getGroupId() {
      return "TIME";
    }
  }

  private String code_;
  private BuCheckBox cbFollow_;
  private BuCheckBox cbParallel;
  private File file_;
  private Option[] options_;
  private BuComboBox softModel_;
  private BuTextField tfArgs_;
  private BuComboBox versionModel_;
  private DicoManager dicoMng_;
  private FDicoProjetInterface projet_;
  private DicoVersionManager versionMng_;

  public FDicoCalculLocalBuilder(final DicoManager _dicos, final DicoVersionManager _version, final File _f,
                                 final boolean _follow) {
    file_ = _f;
    dicoMng_ = _dicos;
    if (dicoMng_ == null) {
      throw new IllegalArgumentException();
    }
    versionMng_ = _version;
    setDefaultOptions();
    build();
    if (_follow) {
      setFollow();
    }
  }

  public boolean isParallel() {
    return cbParallel.isSelected();
  }

  /**
   * @param _codeExec le nom de l'exe
   * @param _version le gestionnaire de version
   */
  public FDicoCalculLocalBuilder(final String _codeExec, final DicoVersionManager _version, final File _f) {
    this(_codeExec, _version, _f, false);
  }

  public FDicoCalculLocalBuilder(final String _codeExec, final DicoVersionManager _version, final File _f,
                                 final boolean _follow) {
    file_ = _f;
    code_ = _codeExec;
    versionMng_ = _version;
    if (code_ == null) {
      throw new IllegalArgumentException();
    }
    setDefaultOptions();
    build();
    if (_follow) {
      setFollow();
    }
  }

  /**
   * @param _codeExec le nom de l'exe
   */
  public FDicoCalculLocalBuilder(final String _codeExec, final File _f) {
    this(_codeExec, null, _f);
  }

  private void build() {
    setLayout(new BuGridLayout(2, 5, 5));
    addLabel(FDicoLib.getS("Application:"));
    if (dicoMng_ == null) {
      addLabel(code_);
    } else {
      final ComboBoxModel softModel = FDicoChooserPanel.createModel(dicoMng_);
      softModel_ = new BuComboBox();
      softModel_.setModel(softModel);
      softModel_.setRenderer(FDicoChooserPanel.getDicoCellRenderer());
      add(softModel_);
    }
    if (versionMng_ != null && versionMng_.getNbVersion() > 0) {
      addLabel(FDicoLib.getS("Version:"));
      final ComboBoxModel versionModel = versionMng_.getVersionModel();
      versionModel_ = new BuComboBox();
      versionModel_.setModel(versionModel);
      versionModel_.setSelectedIndex(versionMng_.getNbVersion() - 1);
      add(versionModel_);
    }
    addLabel(FDicoLib.getS("Execution parall�le"));
    cbParallel = new BuCheckBox();
    add(cbParallel);
    if (options_ != null) {
      add(new BuSeparator());
      add(new BuSeparator());
      // groupe->Option
      final Map groupOption = gatherGroupOption();
      for (int i = 0; i < options_.length; i++) {
        final Option option = options_[i];
        if (option == null) {
          add(new BuSeparator());
          add(new BuSeparator());
        } else {
          option.buildView(this);
        }
      }
      if (groupOption != null && groupOption.size() > 0) {
        for (final Iterator it = groupOption.values().iterator(); it.hasNext(); ) {
          final List l = (List) it.next();
          final DefaultButtonGroup bg = new DefaultButtonGroup();
          for (int i = l.size() - 1; i >= 0; i--) {
            bg.add(((Option) l.get(i)).getBooleanEditor());
          }
        }
      }
    }
    addLabel(FDicoLib.getS("Autres arguments:"));
    tfArgs_ = addStringText();
    add(tfArgs_);
    if (file_ != null) {
      addLabel(FDicoLib.getS("Fichier:"));
      final BuLabel lbFile = addLabel(file_.getName());
      lbFile.setToolTipText(file_.getAbsolutePath());
    }
  }

  private Map gatherGroupOption() {
    final Map groupOption = new HashMap();
    if (options_ != null) {
      // groupe->Option
      for (int i = options_.length - 1; i >= 0; i--) {
        if (options_[i] != null) {
          final String gr = options_[i].getGroupId();
          if (gr != null) {
            List l = (List) groupOption.get(gr);
            if (l == null) {
              l = new ArrayList(5);
              groupOption.put(gr, l);
            }
            l.add(options_[i]);
          }
        }
      }
    }
    return groupOption;
  }

  private void setFollow() {
    cbFollow_ = new BuCheckBox();
    add(new BuSeparator());
    add(new BuSeparator());
    add(new BuLabel(FDicoLib.getS("Afficher l'avancement temps r�el")));
    add(cbFollow_);
  }

  public TelemacExec getTelemacExec(boolean parallel) {
    String execDir = null;
    if (versionMng_ != null && versionModel_ != null) {
      execDir = versionMng_.getVersionPath(versionModel_.getSelectedIndex());
    }
    String exec = code_;
    if (softModel_ != null) {
      exec = ((DicoCasFileFormat) softModel_.getSelectedItem()).getLastVersionImpl().getDico().getCodeName();
    }
    if (exec == null) {
      return null;
    }
    final TelemacExec r = new TelemacExec(execDir, exec, parallel);
    final List args = new ArrayList();
    if (options_ != null) {
      for (int i = 0; i < options_.length; i++) {
        if (options_[i] != null) {
          options_[i].addExecArg(args);
        }
      }
    }
    final String txt = tfArgs_.getText().trim();
    if (txt.length() > 0) {
      args.addAll(CtuluLibString.parseStringList(txt, CtuluLibString.ESPACE));
    }
    if (args.size() > 0) {
      r.setArgs(CtuluLibString.enTableau(args));
    }
    return r;
  }

  public boolean isFollow() {
    return cbFollow_ != null && cbFollow_.isSelected();
  }

  public final void setDefaultOptions() {
    final List options = new ArrayList();

    options.add(new OptionTime("-s", "Create a log file (-s)", FDicoLib
        .getS("Create a log file (-s)")));
    options.add(new Option("-t", FDicoLib.getS("Ne pas effacer le r�pertoire de travail")+" (-t)", FDicoLib
        .getS("Ne pas effacer le r�pertoire de travail apr�s un lancement")));
    options.add(new Option("-x", FDicoLib.getS("Cr�er uniquement un ex�cutable et ne pas lancer de simulation"), FDicoLib
        .getS("Cr�er uniquement un ex�cutable et ne pas lancer de simulation")));
    options.add(null);
    options.add(new Option("--mpi", FDicoLib.getS("Make sure the mpi command is executed, ignoring any hpc command (--mpi)"), FDicoLib
        .getS("Make sure the mpi command is executed, ignoring any hpc command (--mpi)")));
    options.add(new Option("--split", FDicoLib.getS("Only do the trace and the split in parallel (--split)"), FDicoLib
        .getS("Only do the trace and the split in parallel (--split)")));
    options.add(new Option("--merge", FDicoLib.getS("Do the output copying and recollection in parallel (--merge)"), FDicoLib
        .getS("Do the output copying and recollection in parallel (--merge)")));
    options.add(new Option("--run", FDicoLib.getS("Only run the simulation (--run)"), FDicoLib
        .getS("Only run the simulation (--run)")));
    options.add(new Option("--use-link", FDicoLib.getS("Use link instead of copy in the temporary folder - Unix system only (--use-link)"), FDicoLib
        .getS("Use link instead of copy in the temporary folder - Unix system only (--use-link)")));

    final Option[] opts = new Option[options.size()];
    options.toArray(opts);
    setOptions(opts);
  }

  /**
   * @param _options les options
   */
  public final void setOptions(final Option[] _options) {
    options_ = _options;
  }

  public void setSelected(final FileFormatSoftware _ft) {
    if (_ft == null || versionModel_ == null) {
      return;
    }
    softModel_.setSelectedItem(dicoMng_.getFileFormat(_ft.soft_));
    int idx = CtuluLibArray.getIndex(_ft.version_, versionModel_.getModel());
    if (idx < 0) {
      idx = CtuluLibArray.getIndex(_ft.version_.toUpperCase(), versionModel_.getModel());
    }
    if (idx < 0) {
      versionModel_.getModel().setSelectedItem(null);
      if (versionModel_.getModel().getSize() == 1) {
        versionModel_.getModel().setSelectedItem(versionModel_.getModel().getElementAt(0));
      }
    } else {
      versionModel_.setSelectedItem(versionModel_.getModel().getElementAt(idx));
    }
  }

  @Override
  public boolean isDataValid() {
    if (options_ != null) {
      for (int i = options_.length - 1; i >= 0; i--) {
        if (options_[i] != null && !options_[i].isDataValid()) {
          return false;
        }
      }
    }
    return true;
  }
}
