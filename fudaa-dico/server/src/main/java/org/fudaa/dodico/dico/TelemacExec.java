/**
 * @creation 8 nov. 2004
 * @modification $Date: 2006-09-19 14:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.CtuluSystemEnv;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TelemacExec.java,v 1.8 2006-09-19 14:45:58 deniger Exp $
 */
public class TelemacExec extends DicoExec {
  private static final String PYTHON_FOLDER = "python27";
  private static final String PARRALEL_FORTRAN_FOLDER = "mpich2";
  private File telemacDir;

  /**
   * @param _telemacDir le repertoire des binaires : peut-etre nul.
   * @param moduleName l'exe a lancer
   */
  public TelemacExec(final String _telemacDir, final String moduleName, boolean parallel) {
    super(moduleName);
    telemacDir = _telemacDir == null ? null : new File(_telemacDir);
    if (telemacDir != null) {
      final Map<String, String> env = getEnvProperties(parallel);
      if (env != null) {
        setEnv(CtuluSystemEnv.transfEnvVarForProcess(env));
      }
    }
  }

  /**
   * @return les proprietes avec le bon path si le rep telemac n'est pas dans le path par defaut.
   */
  private Map<String, String> getEnvProperties(boolean parallel) {
    String version = telemacDir.getName();
    final Map<String, String> envVariables = new HashMap<>(CtuluSystemEnv.getEnvVars());
    if (envVariables != null) {
      File configDir = new File(telemacDir, "configs");
      File systelConfig = new File(configDir, "systel_" + version + ".cfg");
      if (parallel) {
        systelConfig = new File(configDir, "systel_parallel_" + version + ".cfg");
        addMpich2InPath(envVariables);
      }
      envVariables.put("SYSTELCFG", systelConfig.getAbsolutePath());
    }
    return envVariables;
  }

  private void addMpich2InPath(Map<String, String> prop) {
    if (telemacDir.getParentFile() != null && new File(telemacDir.getParentFile(), PARRALEL_FORTRAN_FOLDER).isDirectory()) {
      final String pathSep = System.getProperty("path.separator");
      String pathKey = getPathKey(prop);
      if (pathKey == null) {
        pathKey = "PATH";
      }
      String path = prop.get(pathKey);
      String mpichPath = new File(telemacDir.getParentFile(), PARRALEL_FORTRAN_FOLDER).getAbsolutePath()+File.separator+"bin";
      if (path == null) {
        path = mpichPath;
      } else {
        path = mpichPath + pathSep + path;
      }
      prop.put(pathKey, path);
    }
  }

  private String getPathKey(Map<String, String> prop) {
    for (Map.Entry<String, String> stringStringEntry : prop.entrySet()) {
      final Map.Entry entry = (Map.Entry) stringStringEntry;
      final String key = (String) entry.getKey();
      if ("path".equalsIgnoreCase(key)) {
        return key;
      }
    }
    return null;
  }

  @Override
  protected void buildExecCommand(List<String> r) {
    String pythonExec = "python";
    if (telemacDir.getParentFile() != null && new File(telemacDir.getParentFile(), PYTHON_FOLDER).exists()) {
      pythonExec = new File(telemacDir.getParentFile(), PYTHON_FOLDER).getAbsolutePath() + File.separator + "python";
    }
    r.add(pythonExec);
    r.add(new File(telemacDir, "scripts" + File.separator + PYTHON_FOLDER + File.separator + "runcode.py").getAbsolutePath());
    r.add(getExecName());
  }
}
