/**
 *  @creation     14 ao�t 2003
 *  @modification $Date: 2006-09-19 14:42:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: DicoCasFileFormatVersion.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
 */
public class DicoCasFileFormatVersion extends DicoCasFileFormatVersionAbstract {

  /**
   * @param _ft le format correspondant
   * @param _dico le model contenant tous les mot-cles
   */
  public DicoCasFileFormatVersion(final DicoCasFileFormatAbstract _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico);
  }

  @Override
  public FileFormatSoftware getSoftVersion(){
    final FileFormatSoftware r = new FileFormatSoftware();
    r.system_ = "LNHE";
    r.soft_ = getCodeName();
    r.language_ = super.getDico().getLanguage();
    r.version_ = getDico().getVersion();
    return r;
  }

  /**
   * @return le nombre de caractere max par ligne
   */
  public int getMaxCharPerLine(){
    return 72;
  }

  @Override
  public DicoEntite getTitreEntite(){
    return getEntiteFor(new String[] { "TITRE", "TITLE"});
  }

  @Override
  public DicoEntite getFichierPrincipalEntite(){
    return getEntiteFor(new String[] { "FICHIER DES PARAMETRES", "STEERING FILE"});
  }

  /**
   * @return le lecteur pour un fichier cas
   */
  public DicoCasReader createDicoCasReader(){
    return new DicoCasReader(this);
  }

  /**
   * @return un writer pour un fichier cas.
   */
  public DicoCasWriter createDicoCasWriter(){
    return new DicoCasWriter(this);
  }

  /**
   * return lecteur fichier cas.
   */
  @Override
  public FileReadOperationAbstract createReader(){
    return createDicoCasReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter(){
    return createDicoCasWriter();
  }

  /**
   * @param _f le fichier a ecrire
   * @param _source  l'inteface contenant les donnees
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _f,final DicoCasInterface _source,final ProgressionInterface _prog){
    final  DicoCasWriter i = createDicoCasWriter();
    if (i == null) {
      return super.write(_f, _source, _prog);
    }
    i.setFile(_f);
    i.setProgressReceiver(_prog);
    final  CtuluIOOperationSynthese r = i.write(_source);
    i.setProgressReceiver(null);
    return r;
  }
}
