/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-04-30 14:21:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;
import java.util.Iterator;

/**
 * @author fred deniger
 * @version $Id: DicoParamsChangeStateInterface.java,v 1.1 2007-04-30 14:21:38 deniger Exp $
 */
public interface DicoParamsChangeStateInterface {

  Iterator getEntiteLoadedEnum();

  int getEntiteLoadedNb();

  DicoEntite[] getNewFileEntiteToSave();

  boolean isEntiteFilesModified();

  boolean isLoaded(final DicoEntite _e);

  boolean isLoadedAndModified(final DicoEntite _e);

  boolean isModified();

  boolean isParamsModified();
  
  void addChangeStateListener(DicoParamsChangeStateListener _l) ;

  /**
   * Enregistre que le mot-cle _e est charge a partir du fichier _f et le rend modifie si _m.
   * 
   * @param _e le mot-cle charge
   * @param _f le fichier a partir duquel le mot-cl� a ete charge
   * @param _m mot-cle modifie ou non
   * @return true si modif
   */
  boolean setLoaded(final DicoEntite _e, final File _f, final boolean _m);

  /**
   * Enregistre que le mot-cle _e est charge a partir du fichier _f et le rend modifie si _m.
   * 
   * @param _e le mot-cle charge
   * @param _f le fichier a partir duquel le mot-cl� a ete charge
   * @param _m mot-cle modifie ou non
   * @return true si modif
   * @param _isProjectFile true si c'est le mot-cle decrivant le fichier principal
   */
  boolean setLoaded(final DicoEntite _e, final File _f, final boolean _m, final boolean _isProjectFile);

  void setLoadedModified(final DicoEntite _e, final boolean _isModified);

  void setParamsModified(boolean _paramsModified);

  void setUnloaded(final DicoEntite _e);

}