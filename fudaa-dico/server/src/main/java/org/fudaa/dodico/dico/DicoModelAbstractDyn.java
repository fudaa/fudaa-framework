/**
 * @creation 7 janv. 2005
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

/**
 * @author Fred Deniger
 * @version $Id: DicoModelAbstractDyn.java,v 1.3 2006-09-19 14:42:27 deniger Exp $
 */
public class DicoModelAbstractDyn extends DicoModelAbstract {
  String codeName_;
  String version_;
  DicoEntite[] ent_;

  /**
   * @param _language l'indice du langage 0=french 1 =english
   * @param _codeName le nom du code
   * @param _version la version
   * @param _ent les mot-cl�s
   */
  public DicoModelAbstractDyn(final int _language, final String _codeName, final String _version, final DicoEntite[] _ent) {
    super(_language);
    codeName_ = _codeName;
    version_ = _version;
    ent_ = _ent;
  }

  @Override
  protected DicoEntite[] createEntites() {
    return ent_;
  }

  @Override
  protected String[] createNoms() {
    final String[] noms = new String[ent_.length];
    for (int i = noms.length - 1; i >= 0; i--) {
      noms[i] = ent_[i].getNom();
    }
    return null;
  }

  @Override
  public String getCodeName() {
    return codeName_;
  }

  @Override
  public String getVersion() {
    return version_;
  }
}
