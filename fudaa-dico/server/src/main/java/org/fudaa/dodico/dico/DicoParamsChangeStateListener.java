/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-04-30 14:21:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

/**
 * @author fred deniger
 * @version $Id: DicoParamsChangeStateListener.java,v 1.1 2007-04-30 14:21:38 deniger Exp $
 */
public interface DicoParamsChangeStateListener {

  void paramsStateLoadedEntiteChanged(DicoParams _params, DicoEntite _kw);

  void paramsModified(DicoParamsChangeState _state);

}
