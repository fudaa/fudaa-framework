/*
 * @creation 8 avr. 2003
 * @modification $Date: 2007-06-28 09:25:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;

import org.fudaa.dodico.commun.DodicoLib;

/**
 * @author deniger
 * @version $Id: DicoGenerator.java,v 1.27 2007-06-28 09:25:08 deniger Exp $
 */
public final class DicoGenerator // extends FileOperationAbstract
{

  private static final String FIN = ";";

  public static class DicoWriter {

    public static String cleanString(final String _s) {
      return _s.replace('�', '�').replace('\\', '?');
    }
    private final String lineSeparator_;
    private final Writer out_;
    private final String tab_;

    DicoWriter(final Writer _out, final String _tab) {
      this(_out, _tab, CtuluLibString.LINE_SEP);
    }

    DicoWriter(final Writer _out, final String _tab, final String _lineSep) {
      out_ = _out;
      tab_ = _tab;
      lineSeparator_ = _lineSep;
    }

    protected void close() throws IOException {
      out_.close();
    }

    protected void flush() throws IOException {
      out_.flush();
    }

    protected void printStringArray(final String[] _s, final boolean _ptVirgule) throws IOException {
      printStringArray(_s, _ptVirgule, true);
    }

    protected void printStringArray(final String[] _s, final boolean _ptVirgule, final boolean _prefixNewString)
            throws IOException {
      if (_prefixNewString) {
        write("new String[]{", false);
      } else {
        write("{", false);
      }
      // StringTokenizer temp;
      for (int i = 0; i < _s.length; i++) {
        if (i != 0) {
          write(CtuluLibString.VIR, false);
          if (i % 10 == 0) {
            writelnTab();
          }
        }
        // temp = new StringTokenizer(_s[i], "\n");
        // write("\"", false);
        // if (temp.hasMoreTokens())
        // {
        // write(cleanString(temp.nextToken()), false);
        // }
        // write("\"", false);
        // while (temp.hasMoreTokens())
        // {
        // writeln();
        // write("+\"\\n" + cleanString(temp.nextToken()) + "\"", true);
        // }
        printString(_s[i], false);
      }
      write("}", false);
      if (_ptVirgule) {
        write(FIN, false);
      }
    }

    protected void printStringArrayByLanguage(final String _var, final String[][] _languageString, final boolean _sort)
            throws IOException {
      final String prefi0 = "if(" + VAR_LANGUAGE_IDX + "==";
      final String prefiAutre = "else " + prefi0;
      String temp = null;
      for (int i = 0; i < _languageString.length; i++) {
        temp = (i == 0) ? prefi0 : prefiAutre;
        writeln(temp + i + ")", true);
        writeln("{", true);
        write(_var + "=", true);
        if (_sort) {
          Arrays.sort(_languageString[i]);
        }
        printStringArray(_languageString[i], true);
        writeln();
        writeln("}", true);
      }
      writeln("else " + _var + "=null;", true);
    }

    protected void printStringArrayDouble(final String[][] _s, final boolean _ptVirgule) throws IOException {
      write("new String[][] {", false);
      for (int i = 0; i < _s.length; i++) {
        if (i != 0) {
          write(lineSeparator_ + tab_ + CtuluLibString.VIR, false);
        }
        printStringArray(_s[i], false, false);
      }
      write("}", true);
      if (_ptVirgule) {
        write(FIN, false);
      }
    }

    protected void write(final String _s, final boolean _tab) throws IOException {
      if (_tab) {
        out_.write(tab_ + _s);
      } else {
        out_.write(_s);
      }
    }

    protected void writeln() throws IOException {
      out_.write(lineSeparator_);
    }

    protected void writeln(final String _s, final boolean _tab) throws IOException {
      write(_s + lineSeparator_, _tab);
    }

    protected void writelnFin(final String _s, final boolean _tab) throws IOException {
      writeln(_s + FIN, _tab);
    }

    protected void writelnTab() throws IOException {
      out_.write(lineSeparator_ + tab_);
    }

    protected void writeTab() throws IOException {
      out_.write(tab_);
    }

    public void printString(final String _s, final boolean _ptVirgule) throws IOException {
      final String guill = "\"";
      final StringTokenizer temp = new StringTokenizer(_s, "\n");
      write(guill, false);
      if (temp.hasMoreTokens()) {
        write(cleanString(temp.nextToken()), false);
      }
      write(guill, false);
      while (temp.hasMoreTokens()) {
        writeln();
        write("+\"\\n" + cleanString(temp.nextToken()) + guill, true);
      }
      if (_ptVirgule) {
        write(FIN, false);
      }
    }
  }
  private final String varEntitesName_ = "entites_";
  private final static String VAR_LANGUAGE_IDX = "languageIndex_";
  /**
   * Le prefixe pour les classes generees?
   */
  public static final String DICO_PREFIX = "Dico";

  /**
   * @param _name le nom du code parse
   * @param _version la version
   * @return le nom de la classe correctement ecrite
   */
  public static String getClassName(final String _name, final String _version) {
    return DICO_PREFIX + CtuluLibString.capitalyze(_name) + _version;
  }

  /**
   * @param _args -dicoFile=fichier -destDir=leRepDeDest -package=lePackage
   */
  public static void main(final String[] _args) {
    final Map m = CtuluLibString.parseArgs(_args);
    File dico = null;
    String t = (String) m.get("-dicoFile");
    if (t == null) {
      System.out.println("Fichier dico non sp�cifi�");
      return;
    }
    System.out.println("Fichier dico = " + t);
    dico = new File(t);
    t = (String) m.get("-destDir");
    if (t == null) {
      System.out.println(DicoResource.getS("R�pertoire cible non sp�cifi�"));
      return;
    }
    System.out.println("Destdir = " + t);
    final String className = (String) m.get("-className");
    if (className == null) {
      System.out.println(DicoResource.getS("Le nom de la classe n'est pas sp�cifi�"));
      return;
    }
    System.out.println("classe name = " + t);
    final String packageName = (String) m.get("-package");
    if (packageName == null) {
      System.out.println(DicoResource.getS("Le nom du package n'est pas sp�cifi�"));
      return;
    }
    if (!dico.exists()) {
      System.err.println("le fichier dico n'existe pas");
      return;
    }
    System.out.println("pacakge name= " + packageName);
    final File dest = new File(t);
    final CtuluAnalyze[] r = readAndGeneration(dico.toPath(), dest, packageName);
    if (r[0] != null) {
      System.out.println(DodicoLib.getS("Analyse"));
      r[0].printResume();
    }
    if (r[1] != null) {
      if (r[0] != null) {
        System.out.println("\n\n");
      }
      System.out.println(DodicoLib.getS("G�n�ration"));
      r[1].printResume();
    }
  }

  public static CtuluAnalyze[] readAndGeneration(final Path _in, final File _dirOut, final String _packageName) {
    final CtuluAnalyze[] r = new CtuluAnalyze[2];
    DicoAnalyzer log = null;
    if (_in == null) {
      throw new IllegalArgumentException("dico file null");
    }
    log = new DicoAnalyzer(_in);
    r[0] = log.read();

    if ((r[0] != null)) {
      if (r[0].containsErrorOrFatalError()) {
        r[0].printResume();
      }
      if (r[0].containsFatalError()) {
        return r;
      }
    }
    final DicoGenerator g = new DicoGenerator(_packageName);
    g.setTelemacDicoAnalyzer(log);
    g.setDirOut(_dirOut);
    r[1] = g.write();
    return r;
  }
  private CtuluAnalyze analyze_;
  private DicoEntiteGenerate[] chaineEntites_;
  private DicoWriter dicoOut_;
  private File dir_;
  private DicoEntiteGenerate[] entites_;
  private Map entitesNum_;
  private String lineSep_;
  private final String methodePrefix_ = "intern";
  private Method[] methodsCreation_;
  private int nbLanguage_;
  private final String packageName_;
  private DicoAnalyzer source_;
  protected final String dataClass_ = "DicoDataType";
  protected final String entClass_ = "DicoEntite";
  protected ProgressionInterface progress_;

  /**
   * @param _destPackage le package des classes generees
   */
  public DicoGenerator(final String _destPackage) {
    super();
    packageName_ = _destPackage;
    lineSep_ = CtuluLibString.LINE_SEP;
  }

  private boolean compute(final String _s) throws InvocationTargetException, IllegalAccessException {
    final Method m = find(_s);
    if (m == null) {
      analyze_.addError(DicoResource.getS("Erreur interne: methode non trouv�e") + " (" + _s + ")", 0);
      return false;
    }
    m.invoke(this, null);
    return true;
  }

  private void createChaineEntites() throws IOException {
    if (chaineEntites_ == null) {
      chaineEntites_ = getTypeEntites(DicoDataTypeGenerate.Chaine.class, false);
    }
  }

  private void createGenerationMethods() {
    final Class cl = this.getClass();
    final Method[] mArray = cl.getDeclaredMethods();
    final int l = mArray.length;
    int index = 0;
    final Method[] mArrayTemp = new Method[l];
    Method m;
    int temp;
    String stemp;
    for (int i = 0; i < l; i++) {
      m = mArray[i];
      temp = m.getModifiers();
      if (temp == Modifier.PRIVATE) {
        stemp = m.getName();
        if (stemp.startsWith(methodePrefix_)) {
          mArrayTemp[index++] = m;
        }
      }
    }
    methodsCreation_ = new Method[index];
    System.arraycopy(mArrayTemp, 0, methodsCreation_, 0, index);
  }

  private boolean duplicateNameExists() {
    final DicoEntiteGenerate[] ents = source_.getEntites();
    final int nbLang = source_.getNbLanguage();
    final int nbEnt = ents.length;
    final Set set = new HashSet(ents.length);
    String nom;
    boolean r = false;
    for (int ilanguage = 0; ilanguage < nbLang; ilanguage++) {
      for (int iEnt = nbEnt - 1; iEnt >= 0; iEnt--) {
        if (ents[iEnt] == null) {
          System.err.println("Entite null n� " + iEnt);
        } else if (ents[iEnt].getEntite(ilanguage, false) == null) {
          System.err.println("null");
        }
        nom = ents[iEnt].getEntite(ilanguage, false).getNom();
        if (!set.add(nom)) {
          analyze_.addError(nom + ": " + DicoResource.getS("Le mot-cl� est d�fini plusieurs fois") + " \"" + nom
                            + "\" ( " + DodicoLib.getS("langage") + " " + DicoLanguage.getName(ilanguage) + ")", iEnt);
          r = true;
        }
      }
      set.clear();
    }
    set.clear();
    return r;
  }

  private Method find(final String _s) {
    if (methodsCreation_ == null) {
      createGenerationMethods();
    }
    final int n = methodsCreation_.length - 1;
    final String test = methodePrefix_ + _s;
    for (int i = n; i >= 0; i--) {
      if (methodsCreation_[i].getName().startsWith(test)) {
        return methodsCreation_[i];
      }
    }
    return null;
  }

  private void generePrivateTab() {
    entites_ = source_.getEntites();
    if (entites_ == null) {
      throw new IllegalArgumentException(source_.getName() + " " + source_.getVersion());
    }
    final int l = entites_.length;
    entitesNum_ = new HashMap(l);
    for (int i = l - 1; i >= 0; i--) {
      entitesNum_.put(entites_[i], new Integer(i));
    }
    nbLanguage_ = source_.getNbLanguage();
  }

  private String getFileName() {
    return getClassName(source_.getName(), source_.getVersion());
  }

  private int getInt(final DicoEntiteGenerate _e) {
    final Integer in = (Integer) entitesNum_.get(_e);
    return (in == null) ? -1 : in.intValue();
  }

  private String[][] getNomsByLanguage() {
    final String[][] r = new String[nbLanguage_][entites_.length];
    for (int i = 0; i < entites_.length; i++) {
      final String[] noms = entites_[i].getNoms();
      for (int j = 0; j < nbLanguage_; j++) {
        r[j][i] = noms[j];
      }
    }
    return r;
  }

  private void getTypeEntites(final Class _c) throws IOException {
    getTypeEntites(_c, true);
  }

  private DicoEntiteGenerate[] getTypeEntites(final Class _c, final boolean _p) throws IOException {
    final int n = entites_.length;
    final DicoEntiteGenerate[] entTemp = new DicoEntiteGenerate[entites_.length];
    DicoEntiteGenerate entite;
    int index = 0;
    for (int i = n - 1; i >= 0; i--) {
      entite = entites_[i];
      if (_c.isInstance(entite.type_)) {
        entTemp[index++] = entite;
      }
    }
    final DicoEntiteGenerate[] r = new DicoEntiteGenerate[index];
    System.arraycopy(entTemp, 0, r, 0, index);
    Arrays.sort(r, DicoEntiteGenerate.getComparatorIndex());
    if (_p) {
      printArray(r);
    }
    return r;
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interncreateEntites() throws Exception {
    final Map entiteCompor = source_.getEntiteComportement();
    final String mapEntiteComporVar = "entiteComportValues_";
    final String comportValues = "comportValues";
    if (entiteCompor != null) {
      dicoOut_.writeln("DicoComportValues[]       " + comportValues + FIN, true);
    }
    dicoOut_.writeln(entClass_ + ".Simple       entiteSimple;", true);
    dicoOut_.writeln(entClass_ + ".Tableau      entiteTableau;", true);
    dicoOut_.writeln(entClass_ + ".Vecteur      entiteVecteur;", true);
    dicoOut_.writeln(dataClass_ + ".Entier      typeEntier;", true);
    dicoOut_.writeln(dataClass_ + ".Binaire     typeBinaire;", true);
    dicoOut_.writeln(dataClass_ + ".Chaine      typeChaine;", true);
    dicoOut_.writeln(dataClass_ + ".Reel        typeReel;", true);
    dicoOut_.writeln("String[]                       choiceKeys;", true);
    dicoOut_.writeln("String[][]                     choiceValues;", true);
    dicoOut_.writeln("String[]                     valueByLanguage=new String[" + nbLanguage_ + "];", true);
    DicoEntiteGenerate ent;
    String var;
    dicoOut_.writeln(entClass_ + "[] entites=new " + entClass_ + "[" + entites_.length + "];", true);
    for (int i = 0; i < entites_.length; i++) {
      ent = entites_[i];
      dicoOut_.writeln();
      dicoOut_.writeln("//start " + ent.getNoms()[0], true);
      final DicoDataTypeGenerate type = ent.getType();
      var = type.print(dicoOut_, VAR_LANGUAGE_IDX, "choiceKeys", "choiceValues");
      if (var == null) {
        analyze_.addError(ent.getNoms()[0] + ": " + DicoResource.getS("Erreur lors de la g�n�ration du type"), i);
        return;
      }
      dicoOut_.writeln();
      var = ent.print(dicoOut_, var, VAR_LANGUAGE_IDX, "valueByLanguage");
      if (var == null) {
        analyze_.addError(ent.getNoms()[0] + ": " + DicoResource.getS("Erreur lors de la g�n�ration du mot-cl�"), i);
        return;
      }
      dicoOut_.writelnFin("entites[" + i + "]=" + var + ".getImmutable()", true);
      if (entiteCompor.containsKey(ent)) {
        ent.printComport(dicoOut_, "entites[" + i + "]", mapEntiteComporVar, comportValues, " choiceValues ",
                         VAR_LANGUAGE_IDX);
      }
      dicoOut_.writeln();
    }
    dicoOut_.writelnFin("return entites", true);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interncreateNoms() throws IOException {
    dicoOut_.writelnFin("String[] noms", true);
    dicoOut_.printStringArrayByLanguage("noms", getNomsByLanguage(), true);
    dicoOut_.writelnFin("return noms", true);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetBooleenEntites() throws IOException {
    getTypeEntites(DicoDataTypeGenerate.Binaire.class);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetChaineEntites() throws IOException {
    createChaineEntites();
    printArray(chaineEntites_);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetClassName() throws IOException {
    dicoOut_.writeln(getFileName(), true);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetCodeName() throws IOException {
    dicoOut_.writeln("return \"" + source_.getName() + "\";", true);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetEntierEntites() throws IOException {
    getTypeEntites(DicoDataTypeGenerate.Entier.class);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetEntitesNombre() throws IOException {
    dicoOut_.writelnFin("return " + entites_.length, true);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetFichierEntites() throws IOException {
    createChaineEntites();
    final DicoEntiteGenerate[] temp = new DicoEntiteGenerate[chaineEntites_.length];
    int index = 0;
    DicoDataTypeGenerate.Chaine d;
    for (int i = 0; i < chaineEntites_.length; i++) {
      d = (DicoDataTypeGenerate.Chaine) (chaineEntites_[i].type_);
      if (d.isFileType()) {
        temp[index++] = chaineEntites_[i];
      }
    }
    final DicoEntiteGenerate[] r = new DicoEntiteGenerate[index];
    System.arraycopy(temp, 0, r, 0, index);
    printArray(r);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetPackage() throws IOException {
    dicoOut_.writelnFin(packageName_, true);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetReelEntites() throws IOException {
    getTypeEntites(DicoDataTypeGenerate.Reel.class);
  }

  /**
   * Appele par reflexion.
   *
   * @throws IOException
   */
  private void interngetVersion() throws IOException {
    dicoOut_.writelnFin("return \"" + source_.getVersion() + "\"", true);
  }

  private void printArray(final DicoEntiteGenerate[] _a) throws IOException {
    if (_a == null) {
      throw new IllegalArgumentException("array is nul");
    }
    final int n = _a.length;
    int temp;
    dicoOut_.writeln(entClass_ + "[] r=new " + entClass_ + "[] {", true);
    for (int i = 0; i < n; i++) {
      final DicoEntiteGenerate entite = _a[i];
      temp = getInt(entite);
      if (temp < 0) {
        analyze_.addError(DicoResource.getS("Erreur interne: index non trouv�"), 0);
      }
      if (i != 0) {
        dicoOut_.write(CtuluLibString.VIR, false);
        if (i % 10 == 0) {
          dicoOut_.writelnTab();
        }
      }
      dicoOut_.write(varEntitesName_ + "[" + temp + "]", false);
    }
    dicoOut_.flush();
    dicoOut_.writeln();
    dicoOut_.writeln("};", true);
    dicoOut_.writeln("return r;", true);
  }

  private void writeDico() {
    if (dir_ == null) {
      analyze_.addFatalError(DicoResource.getS("R�pertoire cible non sp�cifi�"));
      return;
    }
    if (source_ == null) {
      analyze_.addFatalError(DicoResource.getS("Source non trouv�e"));
      return;
    }
    if (source_.getName() == null) {
      analyze_.addFatalError(DicoResource.getS(
              "Le nom du dictionnaire n'a pas �t� trouv�. Le fichier doit etre du type nomv5p2.dico"));
      return;
    }
    if (duplicateNameExists()) {
      analyze_.addError(DicoResource.getS("Noms en double"), 0);
    }
    final File f = new File(dir_, getFileName() + ".java");
    final URL tpl = getFile("DicoAbstract.java.tpl");
    if (tpl == null) {
      analyze_.addFatalError(DicoResource.getS("Fichier template non trouv�"));
      return;
    }
    LineNumberReader tplIn = null;
    try(InputStream openStream = tpl.openStream()) {
      dicoOut_ = new DicoWriter(new FileWriter(f), "    ", lineSep_);
      tplIn = new LineNumberReader(new InputStreamReader(openStream), 16384);
      String line = null;
      final String prefix = "${";
      int temp;
      while ((line = tplIn.readLine()) != null) {
        if (line.startsWith(prefix)) {
          temp = line.lastIndexOf('}');
          if (!compute(line.substring(2, temp))) {
            return;
          }
        } else {
          dicoOut_.writeln(line, false);
        }
      }
    } catch (final IllegalAccessException _e) {
      analyze_.manageException(_e);
    } catch (final InvocationTargetException _e) {
      analyze_.manageException(_e);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    } finally {
      try {
        if (dicoOut_ != null) {
          dicoOut_.close();
        }
      } catch (final IOException _e) {
        analyze_.manageException(_e);
      }
      try {
        if (tplIn != null) {
          tplIn.close();
        }
      } catch (final IOException e) {
        e.printStackTrace();
      }
    }
  }

  protected URL getFile(final String _f) {
    return getClass().getResource(_f);
  }

  public String getLineSep() {
    return lineSep_;
  }

  public boolean setDirOut(final File _dir) {
    if (!_dir.isDirectory()) {
      dir_ = null;
      return false;
    }
    if (!_dir.canWrite()) {
      // analyze_.addFatalError(DodicoLib.geti18n("Impossible d'�crire dans le r�pertoire"));
      // analyze_.addInfo(DodicoLib.geti18n("R�pertoire") + ": " + _dir.getAbsolutePath());
      dir_ = null;
      return false;
    }
    dir_ = _dir;
    return true;
  }

  public void setDirOut(final String _dir) {
    setDirOut(new File(_dir));
  }

  // /**
  // * @see org.fudaa.dodico.tr.TrWriterInterface#setInterface(java.lang.Object)
  // */
  // public void setInterface(Object _interface)
  // {
  // if (_interface instanceof DicoAnalyzer)
  // {
  // setTelemacDicoAnalyzer((DicoAnalyzer) _interface);
  // }
  // else
  // {
  // analyze_.addFatalError(DodicoLib.geti18n("L'objet n'est pas un analyseur de fichier dico"));
  // }
  // }
  /**
   * @param _lineSep
   */
  public void setLineSep(final String _lineSep) {
    lineSep_ = _lineSep;
  }

  /**
   * @param _progressReceiver la barre d'avancement
   */
  public void setProgressReceiver(final ProgressionInterface _progressReceiver) {
    progress_ = _progressReceiver;
  }

  public void setTelemacDicoAnalyzer(final DicoAnalyzer _interface) {
    source_ = _interface;
    generePrivateTab();
  }

  public CtuluAnalyze write() {
    analyze_ = new CtuluAnalyze();
    analyze_.setDesc(DicoResource.getS("G�n�ration fichier dico"));
    writeDico();
    try {
      if (dicoOut_ != null) {
        dicoOut_.close();
      }
    } catch (final IOException e) {
      e.printStackTrace();
    }
    return analyze_;
  }
}
