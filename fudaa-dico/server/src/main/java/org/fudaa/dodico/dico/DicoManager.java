/*
 * @creation 18 juin 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.fileformat.FileFormat;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author deniger
 * @version $Id: DicoManager.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
 */
public abstract class DicoManager {
  private Map<DicoCasFileFormat, List<String>> formatVersions;
  private URI dicoZipUrl;
  /**
   * les formats geres.
   */
  private List<DicoCasFileFormat> formats;

  protected DicoManager(final URI dicoZip) {
    dicoZipUrl = dicoZip;
  }

  /**
   * @return les formats geres
   */
  public List<DicoCasFileFormat> getFormats() {
    return formats;
  }

  protected DicoModelAbstract createDico(final String _name, final String _version, final int _language) {
    //in a jar, we have to deal with a new file system
    if (dicoZipUrl.toString().indexOf('!') >= 0) {
      final String[] array = dicoZipUrl.toString().split("!");
      try (final FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), new HashMap<>())) {
        Path path = fs.getPath(array[1]).resolve(_version).resolve(_name + ".dico");
        DicoDynamiqueGenerator generator = DicoDynamiqueGenerator.loadGenerator(path, null);
        if (generator.getResult().containsErrorOrFatalError()) {
          generator.getResult().printResume();
        }
        return generator.buildModel(null, _language);
      } catch (IOException ioe) {
        FuLog.error(ioe);
      }
    } else {
      final Path path = Paths.get(dicoZipUrl).resolve(_version).resolve(_name + ".dico");
      DicoDynamiqueGenerator generator = DicoDynamiqueGenerator.loadGenerator(path, null);
      if (generator == null) {
        return null;
      }
      return generator.buildModel(null, _language);
    }
    return null;
  }

  /**
   * Choisit la derniere version.
   *
   * @param dicoCasFileFormat le format
   * @param language le langage
   * @return l'instance correspondante
   */
  DicoCasFileFormatVersion createLastVersionImpl(final DicoCasFileFormat dicoCasFileFormat, final int language) {
    return createVersionImpl(dicoCasFileFormat, getLastVersion(dicoCasFileFormat), language);
  }

  /**
   * Choisit le langage courant.
   *
   * @param dicoCasFileFormat le format
   * @param version la version
   * @return l'instance correspondante
   */
  public DicoCasFileFormatVersion createVersionImpl(final DicoCasFileFormat dicoCasFileFormat, final String version) {
    return createVersionImpl(dicoCasFileFormat, version, DicoLanguage.getCurrentID());
  }

  /**
   * @param dicoCasFileFormat le format
   * @param version la version
   * @param language le language
   * @return l'instance correspondante
   */
  public DicoCasFileFormatVersion createVersionImpl(final DicoCasFileFormat dicoCasFileFormat, final String version, final int language) {
    final DicoModelAbstract model = createDico(dicoCasFileFormat.getName(), version, language);
    return model == null ? null : new DicoCasFileFormatVersion(dicoCasFileFormat, model);
  }

  /**
   * @param _nom le nom du format voulu
   * @return le format ou null si aucun
   */
  public DicoCasFileFormat getFileFormat(final String _nom) {
    for (final DicoCasFileFormat r : formats) {
      if (r.getName().equals(_nom)) {
        return r;
      }
    }
    return null;
  }

  /**
   * @param dicoCasFileFormat le format voulu
   * @return la derniere version pour ce format
   */
  String getLastVersion(final DicoCasFileFormat dicoCasFileFormat) {
    final List<String> vs = getVersions(dicoCasFileFormat);
    return CtuluLibArray.isEmpty(vs) ? null : vs.get(vs.size() - 1);
  }

  /**
   * @param dicoCasFileFormat le format a tester
   * @return les versions gerees pour ce format
   */
  public List<String> getVersions(final DicoCasFileFormat dicoCasFileFormat) {
    return formatVersions.get(dicoCasFileFormat);
  }

  /**
   * @param dicoCasFileFormat le format a tester
   * @return le nombre de version pour ce format
   */
  int getVersionsNb(final DicoCasFileFormat dicoCasFileFormat) {
    final List<String> v = formatVersions.get(dicoCasFileFormat);
    return v == null ? 0 : v.size();
  }

  /**
   * @param formatListMap la table contenant les formats geres (nom, version[])
   */
  public void init(final Map<DicoCasFileFormat, List<String>> formatListMap) {
    formatVersions = new TreeMap<>(new FileFormat.FileFormatNameComparator());
    formatVersions.putAll(formatListMap);
    formats = Collections.unmodifiableList(new ArrayList<>(formatVersions.keySet()));
  }
}
