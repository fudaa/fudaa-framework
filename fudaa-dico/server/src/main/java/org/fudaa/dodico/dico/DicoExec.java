/*
 * @creation 13 juin 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.calcul.CalculExec;
import org.fudaa.dodico.commun.DodicoLib;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 * @version $Id: DicoExec.java,v 1.20 2006-09-19 14:42:27 deniger Exp $
 */
public class DicoExec extends CalculExec {
  public final String[] getArgs() {
    return args_;
  }

  public final void setArgs(final String[] _args) {
    args_ = _args;
  }

  String execName_;
  String[] args_;

  /**
   * @param _execName le nom de l'exe
   */
  public DicoExec(final String _execName) {
    if (_execName == null) {
      throw new IllegalArgumentException("Argument null");
    }
    setChangeWorkingDirectory(true);
    setLauchInNewTerm(true);
    setStartCmdUse(true);
    execName_ = _execName;
  }

  /**
   * @return le nom de l'exe
   */
  public String getExecName() {
    return execName_;
  }

  @Override
  public String getExecTitle() {
    return execName_;
  }

  /**
   * @see org.fudaa.dodico.calcul.CalculExec#getLaunchCmd(java.io.File, org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public String[] getLaunchCmd(final File _paramsFile, final CtuluUI _ui) {
    final List<String> r = new ArrayList();
    buildExecCommand(r);
    if (args_ != null) {
      for (int i = 0; i < args_.length; i++) {
        final String s = args_[i].trim();
        if (s.length() > 0) {
          r.add(s);
        }
      }
    }
    if (_paramsFile != null) {
      r.add(_paramsFile.getName());
    }
    return r.toArray(new String[0]);
  }

  protected void buildExecCommand(List<String> r) {
    r.add(execName_);
  }

  /**
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return DodicoLib.getS("Exécutable") + CtuluLibString.ESPACE + execName_;
  }
}
