/*
 *  @creation     18 mai 2004
 *  @modification $Date: 2006-04-14 14:51:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.CtuluCommand;

/**
 * Une classe qui est liee a un projet DicoParams: la modification d'un mot-cl�.
 *
 * @author Fred Deniger
 * @version $Id: DicoParamsLinkedSource.java,v 1.4 2006-04-14 14:51:47 deniger Exp $
 */
public interface DicoParamsLinkedSource {

  /**
   * @param oldValue TODO
   * @param newValue TODO
   * @param ent le mot-cle ajoute
   * @return la commande effectue par le source lie
   */
  CtuluCommand keywordAddedOrUpdated(DicoEntite _ent, String oldValue, String newValue);

  /**
   * @param ent le mot-cle enleve
   * @param _oldVal l'ancienne valeur
   * @return la commande effectue par le source lie
   */
  CtuluCommand keywordRemoved(DicoEntite _ent);

}
