/*
 * @creation 14 mars 2006
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * @author fred deniger
 * @version $Id: DicoParamsPrevalid.java,v 1.3 2006-09-19 14:42:27 deniger Exp $
 */
public abstract class DicoParamsPrevalid {

  final DicoParams params_;

  public DicoParamsPrevalid(final DicoParams _params) {
    super();
    params_ = _params;
  }

  public abstract boolean isChecked(DicoEntite _ent);

  public abstract boolean isChecked(DicoEntite[] _ent);

  public abstract void preAcceptSet(DicoEntite _ent, String _newVal, CtuluCommandContainer _cmd);

  public abstract void preAcceptRemoved(DicoEntite[] _ent, CtuluCommandContainer _cmd);

  public final void doSet(final DicoEntite _ent, final String _val, final CtuluCommandContainer _cmd) {
    params_.setValue(_ent, _val, _cmd);
  }

  public final void doRemove(final DicoEntite[] _ent, final CtuluCommandContainer _cmd) {
    params_.removeValues(_ent, _cmd);
  }

}
