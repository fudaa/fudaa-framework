/*
 * @creation 16 mai 2003
 * 
 * @modification $Date: 2006-09-19 14:42:27 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.Comparator;

/**
 * @author deniger
 * @version $Id: DicoEntiteComparator.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
 */
public abstract class DicoEntiteComparator implements Comparator {

  private final static class Entite extends DicoEntiteComparator {

    public Entite() {
    }

    /**
     * @param _isReverse true si le resultat doit etre inverse
     */
    public Entite(final boolean _isReverse) {
      super(_isReverse);
    }

    @Override
    public int compare(final Object _o1, final Object _o2) {
      final int r = ((DicoEntite) _o1).compareTo((DicoEntite) _o2);
      return reverse_ ? -r : r;
    }

    @Override
    public int compareField(final DicoEntite _e1, final DicoEntite _e2) {
      return _e1.compareTo(_e2);
    }
  }

  private final static class Rubrique extends DicoEntiteComparator {

    public Rubrique() {
    }

    /**
     * @param _isReverse true si le resultat doit etre inverse
     */
    public Rubrique(final boolean _isReverse) {
      super(_isReverse);
    }

    @Override
    public int compareField(final DicoEntite _e1, final DicoEntite _e2) {
      return _e1.getRubrique().compareTo(_e2.getRubrique());
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: DicoEntiteComparator.java,v 1.13 2006-09-19 14:42:27 deniger Exp $ Un interface a utiliser pour les
   *          classes contenant des valeurs pour des mot-cles
   */
  public interface ComparatorValueInterface {

    /**
     * @param _e le mot-cle a considerer
     * @return renvoie la valeur de ce mot-cle
     */
    String getValue(DicoEntite _e);
  }

  /**
   * Compare les indexs de mot-cle.
   * 
   * @author Fred Deniger
   * @version $Id: DicoEntiteComparator.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
   */
  public final static class Index extends DicoEntiteComparator {

    public Index() {
    }

    /**
     * @param _isReverse true si resultat doit etre inverse
     */
    public Index(final boolean _isReverse) {
      super(_isReverse);
    }

    @Override
    public int compareField(final DicoEntite _e1, final DicoEntite _e2) {
      int res = _e1.getIndex() - _e2.getIndex();
      if (res == 0) {
        return _e1.getPosition() - _e2.getPosition();
      }
      return res;
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: DicoEntiteComparator.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
   */
  public final static class TypeDescription extends DicoEntiteComparator {

    public TypeDescription() {
    }

    /**
     * @param _isReverse true si le resultat doit etre inverse
     */
    public TypeDescription(final boolean _isReverse) {
      super(_isReverse);
    }

    @Override
    public int compareField(final DicoEntite _e1, final DicoEntite _e2) {
      return _e1.getType().getDescription().compareTo(_e2.getType().getDescription());
    }
  }

  /**
   * Comparateur de valeurs.
   * 
   * @author Fred Deniger
   * @version $Id: DicoEntiteComparator.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
   */
  public final static class Value extends DicoEntiteComparator {

    ComparatorValueInterface entitesValues_;

    /**
     * @param _entiteValues la classe contenant les valeurs des mot-cles
     */
    public Value(final ComparatorValueInterface _entiteValues) {
      entitesValues_ = _entiteValues;
    }

    /**
     * @param _entiteValues la classe contenant les valeurs des mot-cles
     * @param _isReverse true si les resultats doivent etre inverse
     */
    public Value(final ComparatorValueInterface _entiteValues, final boolean _isReverse) {
      super(_isReverse);
      entitesValues_ = _entiteValues;
    }

    private String getValue(final DicoEntite _e) {
      final String r = entitesValues_.getValue(_e);
      return r == null ? _e.getDefautValue() : r;
    }

    @Override
    public int compareField(final DicoEntite _e1, final DicoEntite _e2) {
      return getValue(_e1).compareTo(getValue(_e2));
    }

    /**
     * @return l'interface contenant les valeurs
     */
    public ComparatorValueInterface getValueInterface() {
      return entitesValues_;
    }
  }

  private static Entite entite_;
  private static Entite entiteReverse_;

  private static Rubrique rubrique_;
  private static Rubrique rubriqueReverse_;

  /**
   * @return le comparateur de nom
   */
  public final static Entite getNomComparator() {
    if (entite_ == null) {
      entite_ = new Entite();
    }
    return entite_;
  }

  /**
   * @return le comparateur inverse de nom
   */
  public final static Entite getNomInverseComparator() {
    if (entiteReverse_ == null) {
      entiteReverse_ = new Entite(true);
    }
    return entiteReverse_;
  }

  /**
   * @return le comparateur de rubrique
   */
  public final static Rubrique getRubriqueComparator() {
    if (rubrique_ == null) {
      rubrique_ = new Rubrique();
    }
    return rubrique_;
  }

  /**
   * @return le comparateur inverse de rubrique
   */
  public final static Rubrique getRubriqueReverseComparator() {
    if (rubriqueReverse_ == null) {
      rubriqueReverse_ = new Rubrique(true);
    }
    return rubriqueReverse_;
  }

  boolean reverse_;

  public DicoEntiteComparator() {
  }

  /**
   * @param _reverse true si le resultat doit etre inverse
   */
  public DicoEntiteComparator(final boolean _reverse) {
    reverse_ = _reverse;
  }

  /**
   * Compare les 2 objets o1 et o2. renvoie true si le pointeur est le meme.Sinon, appelle la methode compareField. Si
   * specifie dans le constructeur le resultat sera inverse.
   */
  @Override
  public int compare(final Object _o1, final Object _o2) {
    if (_o1 == _o2) {
      return 0;
    }
    if ((_o1 == null) || (_o2 == null)) {
      throw new IllegalArgumentException("Argument null");
    }
    if (_o1.equals(_o2)) {
      return 0;
    }
    int r = compareField((DicoEntite) _o1, (DicoEntite) _o2);
    r = reverse_ ? -r : r;
    if (r == 0) {
      r = ((DicoEntite) _o1).compareTo((DicoEntite) _o2);
    }
    return r;
  }

  /**
   * @param _e1 le premier mot-cle a comparer
   * @param _e2 le deuxieme
   * @return utilise la meme specificaction de Comparator
   * @see Comparator#compare(java.lang._Object, java.lang._Object)
   */
  public abstract int compareField(DicoEntite _e1, DicoEntite _e2);

  /**
   * @return true si le resultat doit etre inverse
   */
  public boolean isReverse() {
    return reverse_;
  }
}
