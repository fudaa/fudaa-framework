/**
 *  @creation     18 avr. 2003
 *  @modification $Date: 2007-05-04 13:45:07 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.StringTokenizer;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;

/**
 * @author deniger
 * @version $Id: DicoCasWriter.java,v 1.22 2007-05-04 13:45:07 deniger Exp $
 */
public class DicoCasWriter extends FileOpWriterCharSimpleAbstract {
  DicoCasFileFormatVersion version_;
  String commentSep_;
  String enteteFormat_;
  String enteteRubrique_;
  String com_;

  /**
   * @param _ft
   */
  public DicoCasWriter(final DicoCasFileFormatVersion _ft) {
    version_ = _ft;
    com_ = version_.getCommentString();
    commentSep_ = com_ + "---------------------------------------------------------------------" + lineSep_;
    enteteFormat_ = commentSep_ + com_ + " " + version_.getFileFormat().getID() + " Version ${VERSION} ${DATE}"
        + lineSep_ + com_ + " ${TITLE}" + lineSep_ + commentSep_;
    enteteRubrique_ = lineSep_ + commentSep_ + com_ + " ${RUBRIQUE}" + lineSep_ + commentSep_;
  }

  /**
   * @return la version a utiliser pour ecrire le fichier cas.
   */
  public FileFormatVersionInterface getFileFormatVersion() {
    return version_;
  }

  /**
   * @param _inter la source pour ecrire le fichier
   */
  public void writeCas(final DicoCasInterface _inter) {
    final DicoCasInterface dico = _inter;
    if (dico == null) {
      analyze_.addFatalError(DodicoLib.getS("Les donn�es sont nulles"));
      return;
    }
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    // Hashtable entitesValues = interface_.getInputs();
    final Set rest = dico.getEntiteSet();
    final String[] rubriques = version_.getDico().getRubriques();
    try {
      String dd = null;
      final DicoEntite entNom = version_.getTitreEntite();
      if (entNom != null) {
        dd = dico.getValue(entNom);
      }
      if (dd == null) {
        dd = "nom inconnu";
      }
      writeEntete(dd);
      if (progress_ != null) {
        progress_.setProgression(10);
      }
      if ((rubriques == null) || (rubriques.length == 0)) {
        final DicoEntite[] ents = new DicoEntite[rest.size()];
        rest.toArray(ents);
        writeRubrique(DodicoLib.getS("Inconnu"), ents, dico);
        rest.clear();
        if (progress_ != null) {
          progress_.setProgression(90);
        }
      }
      final int n = rubriques == null ? 0 : rubriques.length;
      final int m = n / 2;
      if (rubriques != null) {
        for (int i = 0; i < n; i++) {
          if (rubriques[i] == null) {
            FuLog.error("Headings not found");
          }

          final DicoEntite[] entToWrite = DicoEntite.getAndRemoveEntite(rubriques[i], rest);
          writeRubrique(rubriques[i], entToWrite, dico);
          if ((progress_ != null) && (i == m)) {
            progress_.setProgression(55);
          }
        }
      }
      if (progress_ != null) {
        progress_.setProgression(95);
      }
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  /**
   * @param _entValues
   * @throws IOException
   */
  private void writeEntete(final String _nomEtude) throws IOException {
    String entete = FuLib.replace(enteteFormat_, "${TITLE}", _nomEtude);
    entete = FuLib.replace(entete, "${VERSION}", version_.getVersionName());
    entete = FuLib.replace(entete, "${DATE}", DateFormat.getDateInstance().format(Calendar.getInstance().getTime()));
    final StringBuffer r = new StringBuffer(100);
    final String com = "" + version_.getCommentChar();
    for (final StringTokenizer tk = new StringTokenizer(entete, lineSep_); tk.hasMoreTokens();) {
      r.append(formatToMaxCharBrutal(tk.nextToken(), com, false)).append(lineSep_);
    }
    writelnToOut(r.toString());
  }

  private void writeRubrique(final String _rubrique, final DicoEntite[] _ent, final DicoCasInterface _inter)
      throws IOException {
    if ((_ent == null) || (_ent.length == 0)) {
      return;
    }
    final int entiteNomMaxLength = geEntiteNomtMaxLength(_ent) + 1;
    final String entete = FuLib.replace(enteteRubrique_, "${RUBRIQUE}", _rubrique);
    writelnToOut();
    out_.write(entete);
    final int n = _ent.length;
    DicoEntite ent;
    final int maxChar = version_.getMaxCharPerLine();
    for (int i = 0; i < n; i++) {
      ent = _ent[i];
      if (version_.getDico().isKey(ent)) {
        writelnToOut();
        final String com = _inter.getCommentaire(ent);
        if ((com != null) && (com.trim().length() > 0)) {
          final StringTokenizer t = new StringTokenizer(com, CtuluLibString.LINE_SEP);
          while (t.hasMoreTokens()) {
            writelnToOut(com_ + formatToMaxCharBrutal(t.nextToken(), com_, false));
          }
        }
        String value = _inter.getValue(ent);
        if (ent.containsStringDataType()&& !(ent instanceof DicoEntite.Vecteur)) {
          value = FuLib.replace(value, "'", "''");
          value = "'" + value + "'";
        }
        final String initNom = ent.getNom();
        final String nom = CtuluLibString.adjustSize(entiteNomMaxLength, initNom);
        if (nom.length() + value.length() + 1 > maxChar) {
          // on essaie avec le nom intiale non format�
          if (initNom.length() + value.length() + 1 <= maxChar) {
            writelnToOut(initNom + "=" + value);
          } else {
            if (initNom.length() + 1 > maxChar) {
              writelnToOut(version_.getCommentChar() + DicoResource.getS("Ce param�tre d�passe 72 caract�res"));
            }
            writelnToOut(initNom + "=");
            if (value.length() > maxChar) {
              value = formatToMaxChar(value, ent);
            }
            writelnToOut(value);
          }
        } else {
          writelnToOut(nom + "=" + value);
        }
      } else {
        analyze_.addWarn("Entit� inconnue pour ce format" + " " + ent.getNom(), -1);
      }
    }
  }

  /**
   * Renvoie une chaine de caractere respectant le nb de caractere max par ligne. Un separateur de ligne (et un prefixe
   * optionnel) est ajoute.
   */
  private String formatToMaxChar(final String _init, final DicoEntite _val) {
    if (_val instanceof DicoEntite.Vecteur) {
      final DicoEntite.Vecteur vect = (DicoEntite.Vecteur) _val;
      final ArrayList list = new ArrayList(50);
      DicoEntite.Vecteur.fillListWithStringValues(_init, vect.getSepChar(), list);
      final String[] toArray = (String[]) list.toArray(new String[list.size()]);
      int maxSize = 0;
      for (int i = 0; i < toArray.length; i++) {
        final int l = toArray[i].length();
        if (l > maxSize) {
          maxSize = l;
        }
      }
      if (maxSize > version_.getMaxCharPerLine()) {
        analyze_.addError(DodicoLib.getS("La valeur suivante ne tient pas dans {0} caract�res", CtuluLibString
            .getString(version_.getMaxCharPerLine()))
            + CtuluLibString.LINE_SEP_SIMPLE + _init, -1);
      }
      String pref = null;
      if (maxSize < version_.getMaxCharPerLine() - 2) {
        pref = "  ";
      } else if (maxSize < version_.getMaxCharPerLine() - 1) {
        pref = " ";
      }
      return DicoEntite.Vecteur.getStringValuesArray(toArray, vect.getSepChar(), version_.getMaxCharPerLine(),
          getLineSeparator(), pref);
    }
    // on ajoute des quotes
    return formatToMaxCharBrutal(_init, CtuluLibString.EMPTY_STRING, true);
  }

  private String formatToMaxCharBrutal(final String _init, final String _pref, final boolean _addQuot) {
    final int maxChar = version_.getMaxCharPerLine() - _pref.length();
    final StringBuffer valueFinal = new StringBuffer(_init);
    final int vfLength = valueFinal.length();
    final int lnLength = lineSep_.length() + _pref.length();
    for (int j = maxChar - 1; j < vfLength; j += (maxChar + lnLength)) {
      valueFinal.insert(j - lnLength, lineSep_ + _pref);
    }
    // on quote le truc
    if (_addQuot && valueFinal.charAt(0) != '\'') {
      valueFinal.insert(0, '\'');
      valueFinal.append('\'');
    }
    return valueFinal.toString();
  }

  /**
   * Renvoie la taille max des noms des entites.
   */
  private int geEntiteNomtMaxLength(final DicoEntite[] _ent) {
    int r = 0;
    final int n = _ent.length - 1;
    int taille = 0;
    for (int i = n; i >= 0; i--) {
      taille = _ent[i].getNom().length();
      if (taille > r) {
        r = taille;
      }
    }
    return r;
  }

  /**
   *
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof DicoCasInterface) {
      writeCas((DicoCasInterface) _o);
    } else {
      donneesInvalides(_o);
    }
  }

  protected CtuluIOOperationSynthese write(final DicoCasInterface _o) {
    writeCas(_o);
    return closeOperation(_o);
  }

  /**
   * @return la version utilisee.
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }
}
