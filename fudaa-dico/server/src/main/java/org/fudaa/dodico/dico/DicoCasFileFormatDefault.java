/**
 * @creation 7 janv. 2005
 * @modification $Date: 2007-06-29 15:10:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;

import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

/**
 * @author Fred Deniger
 * @version $Id: DicoCasFileFormatDefault.java,v 1.6 2007-06-29 15:10:27 deniger Exp $
 */
public class DicoCasFileFormatDefault extends DicoCasFileFormatAbstract {

  DicoCasFileFormatVersion version_;

  /**
   * @param _version la version a utiliser
   */
  public DicoCasFileFormatDefault(final DicoModelAbstract _version) {
    super(_version.getCodeName());
    version_ = new DicoCasFileFormatVersion(this, _version);
  }

  @Override
  public String getLastVersion() {
    return version_.getVersionName();
  }

  public FileFormatVersionInterface getLastVersionInstance() {
    return version_;
  }

  @Override
  public FileFormatVersionInterface getLastVersionInstance(File _f) {
    return version_;
  }
}
