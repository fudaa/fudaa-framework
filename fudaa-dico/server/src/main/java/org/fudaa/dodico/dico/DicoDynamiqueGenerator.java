/**
 * @creation 7 janv. 2005
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoEntiteGenerate.ComportData;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: DicoDynamiqueGenerator.java,v 1.9 2006-09-19 14:42:27 deniger Exp $
 */
public final class DicoDynamiqueGenerator {
  private final DicoAnalyzer dicoAnalyzer_;
  private CtuluAnalyze result_;

  /**
   * @param file le fichier dico
   * @param _prog la progression
   * @return le generateur
   */
  public static DicoDynamiqueGenerator loadGenerator(final File file, final ProgressionInterface _prog) {
    return loadGenerator(file.toPath(), _prog);
  }

  /**
   * @param _f le fichier dico
   * @param _prog la progression
   * @return le generateur
   */
  public static DicoDynamiqueGenerator loadGenerator(final Path _f, final ProgressionInterface _prog) {
    final DicoDynamiqueGenerator r = new DicoDynamiqueGenerator(_f);
    r.analyze(_prog);
    return r;
  }

  /**
   * @param dicoPath le fichier dico
   */
  private DicoDynamiqueGenerator(final Path dicoPath) {
    dicoAnalyzer_ = new DicoAnalyzer(dicoPath);
  }

  public CtuluAnalyze getResult() {
    return result_;
  }

  /**
   * @return le fichier dico analyse
   */
  public String getFilename() {
    return dicoAnalyzer_.getFilename();
  }

  public Path getDicoPath() {
    return dicoAnalyzer_.getDicoPath();
  }

  /**
   * Ne lit que si nÚcessaire.
   *
   * @param _prog le receveur de la progression
   */
  private void analyze(final ProgressionInterface _prog) {
    lastModel_ = null;
    result_ = dicoAnalyzer_.read();
  }

  /**
   * @return les langages valides. null si non analyse
   */
  public String[] getAvailableLanguages() {
    if (!isReadDone()) {
      return null;
    }
    final boolean en = dicoAnalyzer_.getNbLanguage() == 2;
    return en ? DicoLanguage.NAMES : new String[]{DodicoLib.getS("Franšais")};
  }

  DicoModelAbstract lastModel_;

  /**
   * @param language l'indice de la langue demandee
   * @return le model NON MODIFIABLE DES MOTS-CLES
   */
  public DicoModelAbstract buildModel(final ProgressionInterface progressionInterface, final int language) {
    analyze(progressionInterface);
    if (result_.containsFatalError()) {
      return null;
    }
    int idxLanguage = DicoLanguage.getNormalizeLanguage(language);
    if ((lastModel_ != null) && (lastModel_.getLanguageIndex() == idxLanguage)) {
      return lastModel_;
    }
    if (idxLanguage > dicoAnalyzer_.getNbLanguage()) {
      idxLanguage = 0;
    }
    final DicoEntiteGenerate[] entToGen = dicoAnalyzer_.getEntites();
    if (entToGen == null) {
      return null;
    }
    Map<DicoEntite, DicoComportValues[]> m = new HashMap<DicoEntite, DicoComportValues[]>();
    final DicoEntite[] ent = new DicoEntite[entToGen.length];
    for (int i = ent.length - 1; i >= 0; i--) {
      ent[i] = entToGen[i].getEntite(idxLanguage, true);
      ComportData[] comportDatas = entToGen[i].getComportData();
      if (comportDatas != null) {
        DicoComportValues[] values = new DicoComportValues[comportDatas.length];
        for (int j = 0; j < values.length; j++) {
          ComportData comportData = comportDatas[j];
          values[j] = new DicoComportValues(comportData.valueToSoustract_, comportData.not_,
              comportData.entitesName_[idxLanguage]);
        }
        m.put(ent[i], values);
      }
    }
    Arrays.sort(ent);
    lastModel_ = new DicoModelAbstractDyn(idxLanguage, dicoAnalyzer_.getName(), dicoAnalyzer_.getVersion(), ent);
    if (!m.isEmpty()) {
      lastModel_.entiteComportValues_ = m;
    }
    return lastModel_;
  }

  /**
   * @return true l'analyze du fichier dico deja effectuee
   */
  public boolean isReadDone() {
    return result_ != null;
  }
}
