/*
 * @creation 7 avr. 2003
 * 
 * @modification $Date: 2007-05-22 13:11:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;

/**
 * Decrit une entree du fichier dico. Certaines donnees (nom, valeur par defaut et l'aide) sont donnees dans plusieurs
 * langues. Pour l'instant le francais (indice 0) et l'anglais (indice 1) sont supportes.
 * 
 * @author deniger
 * @version $Id: DicoEntite.java,v 1.29 2007-05-22 13:11:22 deniger Exp $
 */
public abstract class DicoEntite implements Comparable {

  /**
   * @author Fred Deniger
   * @version $Id: DicoEntite.java,v 1.29 2007-05-22 13:11:22 deniger Exp $
   */
  public final static class ImmutableSimple extends Simple {

    /**
     * @param _s source pour initialiser.
     */
    public ImmutableSimple(final Simple _s) {
      super(_s.getNom(), _s.getType().getImmutable());
      copyAideDefautIdxNiveau(_s, this);
    }

    @Override
    public DicoEntite getImmutable() {
      return this;
    }

    /**
     * lance une exception.
     */
    @Override
    public void setAide(final String _string) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setDefautValue(final String _string) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setIndex(final int _i) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setNiveau(final int _i) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setRubrique(final String _s) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setType(final DicoDataType _type) {
      throw new NoSuchMethodError();
    }
  }

  /**
   * Tableau immutable.
   * 
   * @author Fred Deniger
   * @version $Id: DicoEntite.java,v 1.29 2007-05-22 13:11:22 deniger Exp $
   */
  public final static class ImmutableTableau extends Tableau {

    /**
     * @param _s source pour initialiser.
     */
    public ImmutableTableau(final Tableau _s) {
      super(_s.getNom(), _s.getType().getImmutable());
      copyAideDefautIdxNiveau(_s, this);
      this.sepChar_ = _s.sepChar_;
      this.taille_ = _s.taille_;
    }

    @Override
    public DicoEntite getImmutable() {
      return this;
    }

    /**
     * lance une exception.
     */
    @Override
    public void setAide(final String _string) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setDefautValue(final String _string) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setIndex(final int _i) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setNiveau(final int _i) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setRubrique(final String _s) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setType(final DicoDataType _type) {
      throw new NoSuchMethodError();
    }
  }

  /**
   * La version immutable pour le vecteur.
   * 
   * @author Fred Deniger
   * @version $Id: DicoEntite.java,v 1.29 2007-05-22 13:11:22 deniger Exp $
   */
  public final static class ImmutableVecteur extends Vecteur {

    /**
     * @param _s source pour initialiser
     */
    public ImmutableVecteur(final Vecteur _s) {
      super(_s.getNom(), _s.getType().getImmutable(), _s.sepChar_);
      copyAideDefautIdxNiveau(_s, this);
    }

    @Override
    public DicoEntite getImmutable() {
      return this;
    }

    /**
     * lance une exception.
     */
    @Override
    public void setAide(final String _string) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setDefautValue(final String _string) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setIndex(final int _i) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setNiveau(final int _i) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setRubrique(final String _s) {
      throw new NoSuchMethodError();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setType(final DicoDataType _type) {
      throw new NoSuchMethodError();
    }
  }

  /**
   * Une entree ne comportant qu'une seule donnee.
   */
  public static class Simple extends DicoEntite {

    /**
     * @param _t le type des donnees
     * @param _n le nom
     */
    public Simple(final String _n, final DicoDataType _t) {
      super(_n, _t);
    }

    /**
     * @overrride
     */
    @Override
    public void accept(final DicoComponentVisitor _m) {
      _m.visitSimple(this);
    }

    @Override
    public String getControleDescription() {
      if (!isModifiable()) {
        return DicoResource.getS("non modifiable");
      }
      return type_.getControleDescription();
    }

    /**
     * @overrride
     */
    @Override
    public String getEntiteTypeDescription() {
      return type_.getDescription();
    }

    @Override
    public DicoEntite getImmutable() {
      return new ImmutableSimple(this);
    }

    @Override
    public String getInvalideMessage(final String _data) {
      return type_.getInvalideMessage(_data);
    }

    /**
     * @overrride
     */
    @Override
    public int getNbFields() {
      return 1;
    }

    /**
     * @overrride
     */
    @Override
    public final boolean isFileType() {
      return type_.isFileType();
    }

    @Override
    public boolean isValide(final String _data) {
      return type_.isValide(_data);
    }

    public String toString() {
      return getClass().getName();
    }
  }

  public final static String getSep() {
    return ";";
  }

  public final static String getVir() {
    return CtuluLibString.VIR;
  }

  /**
   * une entree possedant un nombre de donnees fixe. Le tableau peut etre traite de facon dynamique : si trop de donnees
   * sont presentes, elles seront tronquees pour correspondre a la taille du tableau.
   */
  public static class Tableau extends Vecteur {

    /**
     * Le separateur de tableau.
     */
    public final static String SEPARATOR = getSep();
    boolean dynamique_;
    int taille_;

    /**
     * @param _nom le nom du mot-cle
     * @param _t le type pour chaque entree du mot-cle
     */
    public Tableau(final String _nom, final DicoDataType _t) {
      super(_nom, _t, SEPARATOR);
    }

    @Override
    public void accept(final DicoComponentVisitor _m) {
      _m.visitTableau(this);
    }

    @Override
    public String getControleDescription() {
      if (!isModifiable()) {
        return DicoResource.getS("non modifiable");
      }
      return DicoResource.getS("Nombre maximal de champs") + ": " + taille_;
    }

    @Override
    public String getEntiteTypeDescription() {
      return DicoResource.getS("Tableau") + " (" + taille_ + " " + type_.getDescription() + ")";
    }

    @Override
    public DicoEntite getImmutable() {
      return new ImmutableTableau(this);
    }

    @Override
    public String getInvalideMessage(final String _data) {
      final List l = new ArrayList(taille_ + 5);
      final String r = super.getInvalideMessage(_data, l);
      if (l.size() > taille_) {
        final String s = DicoResource.getS("La valeur poss�de trop de champs") + " (max= " + taille_ + ")";
        return r == null ? s : s + "\n" + r;
      }
      return r;
    }

    @Override
    public int getNbFields() {
      return taille_;
    }

    /**
     * @return la taille du tableau.
     */
    public int getTaille() {
      return taille_;
    }

    /**
     * @return true si les tableaux sont geres dynamiquement/
     */
    public boolean isDynamique() {
      return dynamique_;
    }

    @Override
    public boolean isValide(final String _data) {
      final List l = new ArrayList(30);
      final String s = fillListWithValidValues(_data, l);
      if (s != null) {
        return false;
      }
      final int nb = l.size();
      if (nb != taille_) {
        if ((dynamique_) && (nb > taille_)) {
          return true;
        }
      }
      return true;
    }

    /**
     * @param _b la nouvelle valeur du caractere dynamique.
     */
    public void setDynamique(final boolean _b) {
      dynamique_ = _b;
    }

    /**
     * @param _i la nouvelle taille du tableau.
     */
    public void setTaille(final int _i) {
      taille_ = _i;
      //Nombre magique qui indique qu'un tableau est dynamique
      //      if(taille_==2) dynamique_=true;
    }

    public String toString() {
      return getClass().getName() + " [" + taille_ + "]";
    }
  }

  /**
   * Une entree pouvant contenir plusieurs donnee separees par une virgule.
   */
  public static class Vecteur extends DicoEntite {

    public final void setDataColumn(final boolean _isDataColumn) {
      isDataColumn_ = _isDataColumn;
    }

    /**
     * @return true si les valeurs doivent etre unique
     */
    public final boolean isDataColumn() {
      return isDataColumn_;
    }

    /**
     * Permet de recuperer les elements de _s separes par <code>_sepChar</code> et en prenant en compte les quotes
     * <code>'</code>. Les elements sont mis dans la liste _l.
     * 
     * @param _s la chaine a parser
     * @param _sepChar le separateur
     * @param _l la liste qui recevra les elements de _s
     * @return null if no error. Otherwise return the error description
     */
    public static String fillListWithStringValues(final String _s, final String _sepChar, final List _l) {
      if (_l == null) {
        return "INTERNE: list is null";
      }
      _l.clear();
      String s = _s.trim();
      if (s.length() == 0) {
        return null;
      }
      // Chaine dont les champs ne sont pas quote, on ne s'occupe que des caractere de separation
      // on enleve si necessaire les quote de debut et de fin.
      if ((s.indexOf(QUOTE_CHAR) < 0)
          || ((s.endsWith(QUOTE_STRING)) && (s.startsWith(QUOTE_STRING)) && (s.substring(1, s.length() - 1).indexOf(QUOTE_CHAR) < 0))) {
        if (s.indexOf(QUOTE_CHAR) >= 0) {
          s = s.substring(1, s.length() - 1).trim();
        }
        if (s.length() == 0) {
          _l.add(EMPTY_CHAMP);
          return null;
        }
        final StringTokenizer tk = new StringTokenizer(s.trim(), _sepChar);
        // l.ensureCapacity(tk.countTokens());
        while (tk.hasMoreTokens()) {
          _l.add(tk.nextToken().trim());
        }
        return null;
      }
      // plus difficile si "quotee"
      final int n = s.length();
      int index = 0;
      int indexTemp;
      final StringBuffer b = new StringBuffer(30);
      int lastIndex = -1;
      boolean quoted = false;
      if (s.startsWith(QUOTE_STRING)) {
        quoted = true;
        index = 1;
      }
      while (index < n) {
        if (index == lastIndex) {
          FuLog.error("ERROR BOUCLE");
          break;
        }
        lastIndex = index;
        indexTemp = s.indexOf(QUOTE_CHAR, index);
        if ((!quoted) && ((indexTemp < 0) || (indexTemp > s.indexOf(_sepChar, index)))) {
          indexTemp = s.indexOf(_sepChar, index);
          if (indexTemp < 0) {
            _l.add(s.substring(index, n).trim());
            return null;
          }
          _l.add(s.substring(index, indexTemp).trim());
          index = indexTemp + 1;
          if (index > (n - 1)) {
            return null;
          }
          if (s.charAt(index) == QUOTE_CHAR) {
            index++;
            quoted = true;
          }
          continue;
        }
        // le dernier quote
        if (indexTemp == (n - 1)) {
          _l.add(b.append(s.substring(index, indexTemp)).toString());
          break;
        } else if (s.charAt(indexTemp + 1) == QUOTE_CHAR) {
          b.append(s.substring(index, indexTemp + 1));
          index = indexTemp + 2;
          if (index > (n - 1)) {
            final String sT = DicoResource.getS("Probl�me lors de l'analyse de {0}", s);
            // System.err.println(sT);
            return sT;
          }
        } else {
          // les quotes sont finis ; c'est un champ
          _l.add(b.append(s.substring(index, indexTemp)).toString());
          b.setLength(0);
          final int indexSep = s.indexOf(_sepChar, indexTemp + 1);
          // il n'y plus de separateur, on a fini
          if (indexSep < 0) {
            return null;
          }
          index = indexSep + 1;
          // c'est le separateur de fin
          if (index > (n - 1)) {
            return null;
          }
          while (s.charAt(index) == ' ') {
            index++;
            if (index > (n - 1)) {
              return null;
            }
          }
          if (s.charAt(index) == QUOTE_CHAR) {
            quoted = true;
            index++;
          } else {
            quoted = false;
          }
        }
      }
      return null;
    }

    /**
     * Permet de valider les champs obtenus a partir de la chaine <code>_s</code>. The values are validate with the data
     * type return null if valid and the error description if not.
     * 
     * @param _s la chaine contenant la valeurs du vecteur/tableau
     * @param _sepChar le separateur de valeurs
     * @param _type le type utilise pour valider chaque valeur.
     * @param _l la liste de destination qui contiendra toutes les valeurs de _s
     * @return messages d'erreurs
     */
    public static String fillListWithValidValues(final String _s, final String _sepChar, final DicoDataType _type, final List _l) {
      String s = fillListWithStringValues(_s, _sepChar, _l);
      if (s != null) {
        return s;
      }
      final int n = _l.size() - 1;
      final StringBuffer sb = new StringBuffer();
      for (int i = n; i >= 0; i--) {
        s = _type.getInvalideMessage((String) _l.get(i));
        if (s != null) {
          sb.append(CtuluLibString.LINE_SEP);
          sb.append("Value ").append((String) _l.get(i)).append(' ');
          sb.append(s);
        }
      }
      return sb.length() == 0 ? null : sb.toString();
    }

    public final static String getStringValuesArray(final String[] _d, final String _sepChar) {
      return getStringValuesArray(_d, _sepChar, -1, null, null);
    }

    /**
     * @param _d le tableau des valeurs
     * @param _sepChar le separateur
     * @return la chaine contenant toutes les valeurs de _d. les ' sont utilises si necessaires.
     */
    public final static String getStringValuesArray(final String[] _d, final String _sepChar, final int _maxSize, final String _lineSep,
        final String _prefixForNewLine) {
      final StringBuffer r = new StringBuffer(100);
      String lineSep = _lineSep;
      if (_maxSize > 0 && lineSep == null) {
        lineSep = CtuluLibString.LINE_SEP;
      }
      if (_d != null) {
        if (_d.length == 0) {
          return EMPTY_CHAMP;
        }
        final int n = _d.length;
        boolean quote = false;
        for (int i = 0; i < n; i++) {
          if ((_d[i].indexOf(QUOTE_CHAR) >= 0) || (_d[i].indexOf(_sepChar) >= 0) || _d[i].indexOf(' ') >= 0) {
            quote = true;
          }
        }
        if (_prefixForNewLine != null) {
          r.append(_prefixForNewLine);
        }
        if (quote) {
          r.append(QUOTE_CHAR);
        }
        r.append(_d[0]);
        if (quote) {
          r.append(QUOTE_CHAR);
        }
        int lastSize = 0;
        for (int i = 1; i < n; i++) {
          r.append(_sepChar);

          if (_maxSize > 0) {
            // 3= 2 quotes et un separateur
            // la taille de la ligne en cours
            final int newSize = r.length() + 3 + _d[i].length() - lastSize;
            if (newSize >= _maxSize) {
              r.append(lineSep);
              // la taille apres la nouvelle ligne
              lastSize = r.length();
              if (_prefixForNewLine != null) {
                r.append(_prefixForNewLine);
              }
            }
          }
          if (quote) {
            r.append(QUOTE_CHAR);
          }
          r.append(_d[i]);
          if (quote) {
            r.append(QUOTE_CHAR);
          }
        }
      }
      return r.toString();
    }

    /**
     * @param _s la chaine a parser
     * @param _sepChar le separateur de valeurs
     * @return le tableau des valeurs contenues dans _s.
     */
    public static String[] getValues(final String _s, final String _sepChar, final boolean _unique) {
      List l = new ArrayList(30);
      String sep = _sepChar;
      if (getSep().equals(_sepChar) && _s != null && _s.indexOf(';') < 0 && _s.indexOf(',') > 0) {
        sep = getVir();
      }
      if (fillListWithStringValues(_s, sep, l) == null) {
        if (_unique) {
          l = new ArrayList(new HashSet(l));
        }
        return CtuluLibString.enTableau(l);
      }
      return null;
    }

    int nbElem_;
    String sepChar_ = getVir();

    /**
     * Si true, les valeurs doivent �tre unique.
     */
    boolean isDataColumn_;

    /**
     * @param _nom le nom du mot-cle
     * @param _t le type des donnees
     * @param _sepChar le separateur
     */
    public Vecteur(final String _nom, final DicoDataType _t, final String _sepChar) {
      super(_nom, _t);
      sepChar_ = _sepChar;
      if (sepChar_ == null)
        sepChar_ = getSep();
      nbElem_ = -1;
    }

    @Override
    public void accept(final DicoComponentVisitor _m) {
      _m.visitVecteur(this);
    }

    /**
     * Fill the list l with the values of the string _s.
     * 
     * @param _l la liste qui contiendra toutes les valeurs de _s.
     * @param _s la chaine a parser
     * @return false if the string _s is in wrong format.
     */
    public boolean fillList(final List _l, final String _s) {
      return fillListWithValidValues(_s, _l) == null;
    }

    /**
     * @param _s la chaine a parser
     * @param _l la liste a remplir avec les valeurs
     * @return les description des erreurs
     * @see #fillListWithValidValues(String, String, DicoDataType, List)
     */
    public String fillListWithValidValues(final String _s, final List _l) {
      String sep = sepChar_;
      if (getSep().equals(sepChar_) && _s != null && _s.indexOf(';') < 0 && _s.indexOf(',') > 0) {
        sep = getVir();
      }
      List l = _l;
      if (isDataColumn_) {
        l = new ArrayList();
      }
      final String r = fillListWithValidValues(_s, sep, type_, l);
      if (isDataColumn_ && l.size() > 0) {
        _l.addAll(new HashSet(l));
      }
      return r;
    }

    /**
     * @param _s
     * @param _l
     * @return la list
     * @see #fillListWithStringValues(String, String, List)
     */
    public String fillListWithValues(final String _s, final List _l) {
      String sep = sepChar_;
      if (getSep().equals(sepChar_) && _s != null && _s.indexOf(';') < 0 && _s.indexOf(',') > 0) {
        sep = getVir();
      }
      List l = _l;
      if (isDataColumn_) {
        l = new ArrayList();
      }
      final String r = fillListWithStringValues(_s, sep, l);
      if (isDataColumn_ && l.size() > 0) {
        _l.addAll(new HashSet(l));
      }
      return r;
    }

    @Override
    public String getControleDescription() {
      if (isModifiable()) {
        if (nbElem_ >= 0) {
          return DicoResource.getS("Nombre de champs") + ": " + nbElem_;
        }
      } else {
        return DicoResource.getS("non modifiable");
      }
      return null;
    }

    /**
     * @return la valeur par defaut du type
     */
    public String getDefaultDataTypeValue() {
      return getType().getDefaultValue();
    }

    @Override
    public String getEntiteTypeDescription() {
      return DicoResource.getS("Vecteur") + " (" + type_.getDescription() + ")";
    }

    /**
     * Return a list with the values of the string _data.
     * 
     * @param _data la chaine a parser
     * @return a non null value.
     */
    public List getFields(final String _data) {
      final List l = new ArrayList();
      fillList(l, _data);
      return l;
    }

    @Override
    public DicoEntite getImmutable() {
      return new ImmutableVecteur(this);
    }

    @Override
    public String getInvalideMessage(final String _data) {
      return getInvalideMessage(_data, new ArrayList(20));
    }

    protected String getInvalideMessage(final String _data, final List _l) {
      _l.clear();
      final String s = fillListWithValidValues(_data, _l);
      if ((s == null) && (nbElem_ >= 0)) {
        if (_l.size() != nbElem_) {
          if (nbElem_ == 0) {
            return DicoResource.getS("Ce mot-cl� est inutile");
          }
          return DicoResource.getS("Le nombre d'�l�ments doit �tre �gal � {0}", CtuluLibString.getString(nbElem_));
        }
      }
      return s;
    }

    /**
     * @return le nombre d'element fixe
     */
    public int getNbElemFixed() {
      return nbElem_;
    }

    @Override
    public int getNbFields() {
      return -1;
    }

    /**
     * @return le sep de charactere
     */
    public String getSepChar() {
      return sepChar_;
    }

    /**
     * @param _d la collection des valeurs
     * @return la chaine issue de ces valeurs (correctement format�e pour le fichier cas).
     */
    public final String getStringValue(final Collection _d) {
      return getStringValuesArray(CtuluLibString.enTableau(_d));
    }

    /**
     * @param _d le tableau des valeurs
     * @return la chaine issue de ces valeurs (correctement format�e pour le fichier cas).
     */
    public final String getStringValuesArray(final String[] _d) {
      return getStringValuesArray(_d, sepChar_);
    }

    /**
     * @param _s la chaine a parser
     * @return le tableau de toutes les valeurs issues de _s.
     */
    public String[] getValues(final String _s) {
      return getValues(_s, sepChar_, isDataColumn_);
    }

    /**
     *
     */
    @Override
    public final boolean isFileType() {
      return false;
    }

    @Override
    public boolean isValide(final String _data) {
      if (nbElem_ < 0) {
        return (fillListWithValidValues(_data, new ArrayList()) == null);
      }
      final List l = new ArrayList();
      final String s = fillListWithValidValues(_data, l);
      if (s == null) {
        return l.size() == nbElem_;
      }
      return false;
    }

    /**
     * @param _n le nombre de valeur max
     * @return true si affectation
     */
    public boolean setNbElem(final int _n) {
      if (_n != nbElem_) {
        nbElem_ = _n;
        return true;
      }
      return false;
    }

    public String toString() {
      return getClass().getName() + " sep=" + sepChar_;
    }
  }

  /**
   * Chaine decrivant un champ vide.
   */
  public final static String EMPTY_CHAMP = "' '";
  /**
   * Les niveaux utilises pour les fichiers dicos.
   */
  public final static List NIVEAU = new CtuluPermanentList(new String[] { DicoResource.getS("Normal"), DicoResource.getS("Avanc�"),
      DicoResource.getS("Expert") });
  /**
   * le caractere '.
   */
  public final static char QUOTE_CHAR = '\'';
  /**
   * le caractere '.
   */
  public final static String QUOTE_STRING = "'";

  static void copyAideDefautIdxNiveau(final DicoEntite _entiteToCopy, final DicoEntite _dest) {
    _dest.aide_ = _entiteToCopy.aide_;
    _dest.defautValue_ = _entiteToCopy.defautValue_;
    _dest.index_ = _entiteToCopy.index_;
    _dest.niveau_ = _entiteToCopy.niveau_;
    _dest.rubrique_ = _entiteToCopy.rubrique_;
    _dest.position= _entiteToCopy.position;
    _dest.setModifiable(_entiteToCopy.isModifiable_);
  }

  /**
   * Enleve les entites de rubrique :<code>_rubrique</code> de la liste <code>_entites</code> et les renvoie dans un
   * tableau.
   * 
   * @param _rubrique la rubrique recherchee
   * @param _entites la liste des entites a modifier
   * @return les entites enleve de <code>_entite</code>.
   */
  public static DicoEntite[] getAndRemoveEntite(final String _rubrique, final Set _entites) {
    final int nb = _entites.size();
    final Set l = new HashSet(nb);
    DicoEntite ent;
    for (final Iterator it = _entites.iterator(); it.hasNext();) {
      ent = (DicoEntite) it.next();
      if (_rubrique.equals(ent.getRubrique())) {
        l.add(ent);
        it.remove();
      }
    }
    return DicoParams.enTableau(l);
  }

  /**
   * @return la liste des rubriques contenues dans ce tableau d'entite.
   * @param _entitesSet la liste des mot-cle a parcourir
   */
  public static String[] getRubriques(final Set _entitesSet) {
    final int nb = _entitesSet.size();
    final Set l = new HashSet(nb);
    for (final Iterator it = _entitesSet.iterator(); it.hasNext();) {
      l.add(((DicoEntite) it.next()).getRubrique());
    }
    return CtuluLibString.enTableau(l);
  }

  private String aide_ = CtuluLibString.EMPTY_STRING;
  private String defautValue_ = CtuluLibString.EMPTY_STRING;
  private int index_;
  private int position;

  private boolean isModifiable_ = true;
  private boolean isRequired_;
  private int niveau_;
  private final String nom_;
  private String rubrique_ = DicoResource.getS("Aucune");
  protected DicoDataType type_;

  /**
   * @param _t le type des donnees
   * @param _nom le nom du mot-cle
   */
  public DicoEntite(final String _nom, final DicoDataType _t) {
    nom_ = _nom;
    if (_t == null) {
      throw new IllegalArgumentException("Type must not be null");
    }
    type_ = _t;
  }

  /**
   * @param _m le composant a visiter
   */
  public abstract void accept(DicoComponentVisitor _m);

  /**
   * Tri selon l'ordre alphabetique.
   * 
   * @param _o le mot-cle a comparer
   * @return la valeur renvoyee par String.compareTo
   */
  public final int compareTo(final DicoEntite _o) {
    if (_o == this) {
      return 0;
    }
    return nom_.compareTo(_o.getNom());
  }

  /**
   * Compare le nom.
   * 
   * @return la valeur renvoyee par String.compareTo
   */
  @Override
  public final int compareTo(final Object _o) {
    if (_o == this) {
      return 0;
    }
    if (!(_o instanceof DicoEntite)) {
      throw new IllegalArgumentException("arg is not DicoEntite");
    }
    final DicoEntite toCompare = (DicoEntite) _o;
    return nom_.compareTo(toCompare.getNom());
  }

  /**
   * @return true si contient des donnees de type chaine.
   */
  public boolean containsStringDataType() {
    return type_.isChaine();
  }

  /**
   * @param _obj le mot-cle a comparer
   * @return true si possede le meme nom.
   */
  public final boolean equalsEntite(final DicoEntite _obj) {
    if (this == _obj) {
      return true;
    }
    if (_obj == null) {
      return false;
    }
    return _obj.getNom().equals(nom_);
  }

  /**
   * Renvoie true si les noms sont identiques.
   */
  public final boolean equals(final Object _obj) {
    if (this == _obj) {
      return true;
    }
    if (!(_obj instanceof DicoEntite)) {
      return false;
    }
    return ((DicoEntite) _obj).getNom().equals(nom_);
  }

  /**
   * @return l'aide dans le langue utilisee sur cette machine.
   */
  public String getAide() {
    return aide_;
  }

  /**
   * @return la chaine decrivant les controles. null si pas de controle
   */
  public abstract String getControleDescription();

  /**
   * @return la valeur par defaut correspondant au caracteristique locale de cette machine.
   */
  public String getDefautValue() {
    return defautValue_;
  }

  /**
   * @return une description du mot-cle ( simple, tableau, vecteur ...)
   */
  public abstract String getEntiteTypeDescription();

  /**
   * @return la version immutable du mot-cle
   */
  public abstract DicoEntite getImmutable();

  /**
   * @return l'index du mot-cle
   */
  public int getIndex() {
    return index_;
  }

  protected abstract String getInvalideMessage(String _data);

  /**
   * @return le nombre de champs a utilise pour le mot-cle
   */
  public abstract int getNbFields();

  /**
   * @return le niveau ( expert,normal, non utilise)
   */
  public int getNiveau() {
    return niveau_;
  }

  /**
   * @return une description du niveau
   */
  public String getNiveauDesc() {
    int n = niveau_;
    if (n < 0) {
      n = -n;
    }
    n--;
    if ((n >= 0) && (n < NIVEAU.size())) {
      return (String) NIVEAU.get(n);
    }
    return CtuluLibString.getString(niveau_);
  }

  /**
   * @return le nom dans le langue utilisee.
   */
  public String getNom() {
    return nom_;
  }

  /**
   * Renvoie la chaine a utiliser pour les preferences. Le nom est mis en minuscule et les espaces sont remplaces par
   * '_'?
   * 
   * @return chaine utilise pour les preferences
   * @see com.memoire.fu.FuLib#clean(String)
   */
  public String getPrefId() {
    return FuLib.clean(nom_.toLowerCase().replace(' ', '_'));
  }

  /**
   * @return la rubrique de ce mot-cle
   */
  public String getRubrique() {
    return rubrique_;
  }

  public String toString() {
    return getNom();
  }

  /**
   * @return le type des donnees.
   */
  public DicoDataType getType() {
    return type_;
  }

  /**
   *
   */
  public final int hashCode() {
    return nom_.hashCode();
  }

  /**
   * @return true si mot-cle de type fichier
   */
  public abstract boolean isFileType();

  /**
   * @return true si modifiable ( depend du niveau et d'un parametre).
   */
  public boolean isModifiable() {
    return (niveau_ >= 0) && (isModifiable_);
  }

  /**
   * @return true si ce mot-cle doit etre defini par l'utilisateur
   */
  public boolean isRequired() {
    return isRequired_;
  }

  /**
   * @param _n le mot-cle a comparer
   * @return true si les deux mot-cle ont le meme nom
   */
  public final boolean isSameEntiteName(final DicoEntite _n) {
    return nom_.equals(_n.getNom());
  }

  /**
   * @param _data la valeur a tester
   * @return true si la valeur est valide
   */
  public abstract boolean isValide(String _data);

  /**
   * @param _aide l'aide dans les differentes langues.
   */
  public void setAide(final String _aide) {
    if (_aide != null) {
      aide_ = _aide;
    }
  }

  /**
   * @param _string la valeur par defaut
   */
  public void setDefautValue(final String _string) {
    if (_string != null) {
      defautValue_ = _string;
    }
  }

  public void setIndex(final int _idx) {
    index_ = _idx;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  /**
   * @param _b true si le mot-cle peut etre modifie par l'utilisateur
   */
  public void setModifiable(final boolean _b) {
    isModifiable_ = _b;
  }

  public void setNiveau(final int _niveau) {
    niveau_ = _niveau;
  }

  /**
   * Ce mot-cl� est n�cessaire a l'appli.
   * 
   * @param _b true si ce mot-cle doit etre modifie par l'utilisateur
   */
  public void setRequired(final boolean _b) {
    isRequired_ = _b;
  }

  /**
   * @param _s la nouvelle rubrique du mot-cle
   */
  public void setRubrique(final String _s) {
    rubrique_ = _s;
  }

  /**
   * @param _type le nouveau type gere par ce mot-cle
   */
  public void setType(final DicoDataType _type) {
    type_ = _type;
  }
}
