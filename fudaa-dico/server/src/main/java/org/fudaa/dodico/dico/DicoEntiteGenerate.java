/*
 * @creation 7 avr. 2003
 * @modification $Date: 2007-05-22 13:11:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoGenerator.DicoWriter;

/**
 * Decrit une entree du fichier dico. Certaines donnees (nom, valeur par defaut et l'aide) sont donnees dans plusieurs
 * langues. Pour l'instant le francais (indice 0) et l'anglais (indice 1) sont supportes.
 * 
 * @author deniger
 * @version $Id: DicoEntiteGenerate.java,v 1.24 2007-05-22 13:11:22 deniger Exp $
 */
public abstract class DicoEntiteGenerate {

  String[] noms_;
  private static final String CROCHET_O = "[";
  private String[] aides_;
  private String[] defautValues_;
  int index_;
  private int niveau_;
  private String[][] rubriques_;
  protected DicoDataTypeGenerate type_;

  static String getStrEnd() {
    return "]);";
  }

  static String getParFerm() {
    return ");";
  }

  static String getGuill() {
    return "\"";
  }

  static String get2Pt() {
    return ": ";
  }
  
  protected abstract ComportData[] getComportData();

  /**
   * Une classe decrivant le comportement d'un mot-cle.
   * 
   * @author Fred Deniger
   * @version $Id: DicoEntiteGenerate.java,v 1.24 2007-05-22 13:11:22 deniger Exp $
   */
  public static class ComportData {

    /**
     * true si l'on doit inverser le resultat de la fonction.
     */
    boolean not_;
    /**
     * La valeur a soustraire a la valeur du mot-clef (si null rien n'est fait).
     */
    String valueToSoustract_;
    /**
     * Le nom des mot-clef concern�s ( par langage).
     */
    String[][] entitesName_;

    public String toString() {
      return (not_ ? "NOT" : "") + " " + (valueToSoustract_ == null ? DicoResource.getS("Binaire") : valueToSoustract_)
          + CtuluLibString.getObjectInString(entitesName_, true);
    }
  }

  /**
   * @param _t le type des donnees
   */
  public DicoEntiteGenerate(final DicoDataTypeGenerate _t) {
    if (_t == null) { throw new IllegalArgumentException("Le type must be not null"); }
    type_ = _t;
  }

  /**
   * @param _kw les identifiants
   * @param _entites les entites a gerer
   * @param _m
   * @param _analyze
   */
  public void computeActionAffichage(final DicoKeyword _kw, final DicoEntiteGenerate[] _entites, final List _m,
      final CtuluAnalyze _analyze) {
    if ((_m != null) && (_m.size() > 0)) {
      _analyze.addError(
          noms_[0] + CtuluLibString.ESPACE + DicoResource.getS("fonction de comportement non support�e"));
    }
  }

  protected void initialyzeEntite(final DicoEntite _e, final int _l) {
    if (aides_ != null) {
      _e.setAide(aides_[_l]);
    }
    if (rubriques_ != null) {
      _e.setRubrique(rubriques_[_l][0]);
    }
    if (defautValues_ != null) {
      computeDefaultValues(defautValues_);
      _e.setDefautValue(defautValues_[_l]);
    }
    _e.setNiveau(niveau_);
    _e.setIndex(index_);
  }

  /**
   * @param _out la cible pour ecrire
   * @param _type le type de donnees
   * @param _varNbLanguageName le parametres pour choisir le bon langage.
   * @param _varArrayByLanguage le nom de la variable utilisee pour stocker les parametres linguistiques
   * @return le nom de la variable utilisee pour coder le mot-cle
   * @throws IOException
   */
  public abstract String printNew(DicoGenerator.DicoWriter _out, String _type, String _varNbLanguageName,
      String _varArrayByLanguage) throws IOException;

  /**
   * @param _out la cible
   * @param _values les valeurs par langages
   * @param _varArrayByLanguage le nom de la variable utilisee pour stocker les noms
   * @throws IOException
   */
  public void writeByLanguage(final DicoGenerator.DicoWriter _out, final String[] _values,
      final String _varArrayByLanguage) throws IOException {
    final int language = _values.length;
    for (int i = 0; i < language; i++) {
      _out.write(_varArrayByLanguage + "[" + i + "]=", true);
      _out.printString(_values[i], true);
      _out.writeln();
    }
  }

  /**
   * @param _out la cible
   * @param _values les valeurs par langages (la premiere colonne sera utilisee)
   * @param _varArrayByLanguage le nom de la variable utilisee pour stocker les noms
   * @throws IOException
   */
  public void writeByLanguage(final DicoGenerator.DicoWriter _out, final String[][] _values,
      final String _varArrayByLanguage) throws IOException {
    final int language = _values.length;
    for (int i = 0; i < language; i++) {
      _out.write(_varArrayByLanguage + CROCHET_O + i + "]=", true);
      _out.printString(_values[i][0], true);
      _out.writeln();
    }
  }

  /**
   * Ecrit les donnees relatives au comportement.
   * 
   * @param _out la cible
   * @param _varEntite la variable utilisee pour specifier un mot-cle
   * @param _varMapEntiteComport la variable utilisee pour definit la table
   * @param _varComportData
   * @param _varTabTabString
   * @param _varNbLanguageName
   * @throws IOException
   */
  public void printComport(final DicoGenerator.DicoWriter _out, final String _varEntite,
      final String _varMapEntiteComport, final String _varComportData, final String _varTabTabString,
      final String _varNbLanguageName) throws IOException {}

  /**
   * @param _out la cible
   * @param _type le type de donnees
   * @param _varNbLanguageName l'indice a choisir pour le langage
   * @param _varArrayByLanguage le variable utilisee pour stockee les donnees par langage
   * @return la variable decrivant le mot-cle
   * @throws IOException
   */
  public String print(final DicoGenerator.DicoWriter _out, final String _type, final String _varNbLanguageName,
      final String _varArrayByLanguage) throws IOException {
    writeByLanguage(_out, noms_, _varArrayByLanguage);
    final String var = printNew(_out, _type, _varNbLanguageName, _varArrayByLanguage);
    if (var == null) { return null; }
    _out.writeln(var + ".setNiveau(" + niveau_ + getParFerm(), true);
    // aide
    if (aides_ != null) {
      writeByLanguage(_out, aides_, _varArrayByLanguage);
      _out.writeln(var + ".setAide(" + _varArrayByLanguage + CROCHET_O + _varNbLanguageName + getStrEnd(), true);
    }
    // rubriques
    if (rubriques_ != null) {
      writeByLanguage(_out, rubriques_, _varArrayByLanguage);
      _out.writeln(var + ".setRubrique(" + _varArrayByLanguage + CROCHET_O + _varNbLanguageName + getStrEnd(), true);
    }
    // valeurs par defaut
    if (defautValues_ != null) {
      computeDefaultValues(defautValues_);
      writeByLanguage(_out, defautValues_, _varArrayByLanguage);
      _out.writeln(var + ".setDefautValue(" + _varArrayByLanguage + CROCHET_O + _varNbLanguageName + getStrEnd(), true);
    }
    return var;
  }

  /**
   * @param _array les valeurs par defaut a modifier si necessaire.
   */
  public void computeDefaultValues(final String[] _array) {}

  /**
   * @param _string les aides par langage
   */
  public void setAides(final String[] _string) {
    aides_ = _string;
  }

  /**
   * @param _string
   */
  public void setDefautValues(final String[] _string) {
    defautValues_ = _string;
  }

  /**
   * @param _string
   */
  public void setNoms(final String[] _string) {
    noms_ = _string;
  }

  /**
   * @param _type
   */
  protected void setType(final DicoDataTypeGenerate _type) {
    type_ = _type;
  }

  protected abstract DicoEntite getEntite(int _l, boolean _initialyzeAll);

  protected String isDefautValuesValides() {
    if (defautValues_ == null) { return null; }
    final int l = defautValues_.length;
    DicoEntite ent;
    for (int i = 0; i < l; i++) {
      ent = getEntite(i, false);
      final String s = ent.getInvalideMessage(defautValues_[i]);
      if (s != null) { return getGuill() + defautValues_[i] + getGuill() + get2Pt() + DodicoLib.getS("non valide"); }
    }
    return null;
  }

  /**
   * @return l'index
   */
  public int getIndex() {
    return index_;
  }

  public void setIndex(final int _idx) {
    index_ = _idx;
  }

  /**
   * @return le niveau
   */
  public int getNiveau() {
    return niveau_;
  }

  /**
   * @param _niv le niveau
   */
  public void setNiveau(final int _niv) {
    niveau_ = _niv;
  }

  /**
   * Compare des entites de meme TYPE (entier,binaire,reel,chaine) selon leurs indexs.
   * 
   * @return le comparateur d'indice
   */
  public static Comparator getComparatorIndex() {
    return new IndexComparator();
  }

  /**
   * Renvoie les noms dans toutes les langues correspondants au nom francais passe en parametre. Un nom par ligne du
   * tableau.
   * 
   * @param _name le nom a rechercher
   * @param _ent les mot-cle a parcourir
   * @return un tableau a une colonne
   */
  public static String[][] getEntiteGenerationWithFrenchName(final String _name, final DicoEntiteGenerate[] _ent) {
    final int n = _ent.length - 1;
    for (int i = n; i >= 0; i--) {
      final DicoEntiteGenerate generate = _ent[i];
      if (generate == null) {
        FuLog.error("generate keyword is null");
      } else if (generate.noms_[0].equals(_name)) {
        final int l = generate.noms_.length;
        final String[][] r = new String[l][1];
        for (int j = 0; j < l; j++) {
          r[j][0] = generate.noms_[j];
        }
        return r;
      }
    }
    return null;
  }

  /**
   * @return la rubrique en francairs contenu dans le comport
   */
  public String getFrenchRubriqueInComportFormat() {
    if (rubriques_ != null) {
      final String[] rInL = rubriques_[0];
      if (rInL == null) { return null; }
      final StringBuffer b = new StringBuffer(50);
      final char rSep = DicoKeyword.getComportRubriqueSep();
      final int l = rInL.length;
      for (int i = 0; i < l; i++) {
        if (i != 0) {
          b.append(rSep);
        }
        b.append(rInL[i]);
      }
      return b.toString();
    }
    return null;
  }

  /**
   * @param _name la rubrique a cherche
   * @param _ent les mot-cles
   * @return les noms des mot-cles ayant une rubrique francaise egale a _name.
   */
  public static String[][] getEntiteGenerationWithFrenchRubrique(final String _name, final DicoEntiteGenerate[] _ent) {
    final int n = _ent.length - 1;
    final int l = _ent[0].noms_.length;
    final List[] listByLangage = new ArrayList[l];
    for (int i = 0; i < l; i++) {
      listByLangage[i] = new ArrayList();
    }
    for (int i = n; i >= 0; i--) {
      final DicoEntiteGenerate generate = _ent[i];
      if ((generate.rubriques_ != null) && (generate.getFrenchRubriqueInComportFormat().equals(_name))) {
        for (int j = 0; j < l; j++) {
          listByLangage[j].add(generate.noms_[j]);
        }
      }
    }
    if (listByLangage[0].size() == 0) {
      // c'est peut-etre un mot-cle
      final int id = _name.lastIndexOf('*');
      if ((id > 0) && (id < _name.length())) { return getEntiteGenerationWithFrenchName(_name.substring(id + 1), _ent); }
      return null;
    }
    final String[][] r = new String[l][];
    for (int i = 0; i < l; i++) {
      r[i] = CtuluLibString.enTableau(listByLangage[i]);
    }
    return r;
  }

  public void setRubriques(final String[] _newRubr) {
    if (_newRubr != null) {
      final int l = _newRubr.length;
      rubriques_ = new String[l][];
      for (int i = 0; i < l; i++) {
        final String[] rub = CtuluLibString.parseString(_newRubr[i], ";");
        final int n = rub.length;
        rubriques_[i] = new String[n];
        for (int j = 0; j < n; j++) {
          rubriques_[i][j] = DicoKeyword.computeValue(rub[j]);
        }
      }
    }
  }

  /**
   * Une entree ne comportant qu'une seule donnee.
   */
  public static class Simple extends DicoEntiteGenerate {

    /**
     * Tableau des comportements (affichage) a mettre a jour.
     */
    private ComportData[] comportData_;

    /**
     * @param _t le type des donnees
     */
    public Simple(final DicoDataTypeGenerate _t) {
      super(_t);
    }
    
    @Override
    protected ComportData[] getComportData(){
      return comportData_;
    }

    @Override
    public void computeActionAffichage(final DicoKeyword _kw, final DicoEntiteGenerate[] _entites, final List _m,
                                       final CtuluAnalyze _analyze) {
      if ((_m == null) || (_m.size() <= 0)) { return; }
      comportData_ = new ComportData[_m.size()];
      int index = 0;
      for (final Iterator it = _m.iterator(); it.hasNext();) {
        String stemp = ((String) it.next()).trim();
        // on recupere la rubrique ou l'entite voulue.
        int idxDeb = stemp.indexOf('(');
        int idxFin = stemp.indexOf(')');
        String rubrOuEntite = null;

        if ((idxDeb > 0) && (idxFin > idxDeb)) {
          String substring = stemp.substring(idxDeb + 1, idxFin);
          rubrOuEntite = DicoKeyword.computeValue(substring);
        }
        if (rubrOuEntite == null) {
          _analyze.addWarn(
              noms_[0] + get2Pt() + DicoResource.getS("erreur lors de l'analyse de COMPORT"));
          continue;
        }
        final String test = CtuluLibString.findStringIn(_kw.getComportValues(), stemp);
        // NOT_VALUE
        final boolean no = (test == _kw.getComportNotValueId());
        // on recupere la fin de la chaine
        stemp = stemp.substring(stemp.indexOf(test) + test.length());
        if (stemp.length() <= 0) {
          _analyze.addWarn(
              noms_[0] + get2Pt() + DicoResource.getS("erreur dans recherche de COMPORT"));
          continue;
        }
        idxDeb = stemp.indexOf('(');
        idxFin = stemp.indexOf(')');
        String number = null;
        if ((idxDeb > 0) && (idxFin > idxDeb)) {
          final String s = stemp.substring(idxDeb + 1, idxFin).trim();
          if (s.length() > 0) {
            final String sToTest = "#\'" + noms_[0] + "\' -";
            if (s.startsWith(sToTest)) {
              number = s.substring(sToTest.length()).trim();
            } else {
              // pour prendre en compte les erreurs dans les fichiers dico
              if ((s.startsWith("#\'")) && (s.indexOf("\' -") > 0)) {
                number = s.substring(s.indexOf("\' -")).trim();
                _analyze.addError(
                    noms_[0]
                        + get2Pt()
                        + DicoResource.getS("La fonction est incorrecte. La condition {0} a �t� remplac�e par {1}", s,
                            sToTest) + " " + number);
              }
              // il y erreur donc on passe
              else {
                _analyze.addWarn(
                    noms_[0] + get2Pt() + DicoResource.getS("erreur dans recherche de COMPORT"), -1);
                continue;
              }
            }
          }
        } else {
          _analyze.addWarn(
              noms_[0] + get2Pt() + DicoResource.getS("erreur dans recherche de COMPORT"), -1);
          // erreur
          continue;
        }
        // c'est une entite
        String[][] ents;
        if (rubrOuEntite.indexOf('*') < 0) {
          ents = DicoEntiteGenerate.getEntiteGenerationWithFrenchName(rubrOuEntite, _entites);
        } else {
          ents = getEntiteGenerationWithFrenchRubrique(rubrOuEntite, _entites);
        }
        if (ents == null) {
          _analyze.addWarn(
              noms_[0]
                  + get2Pt()
                  + DicoResource
                      .getS("La rubrique {0} n'a pas �t� trouv� (demand� par le champ COMPORT)", rubrOuEntite), -1);
        } else {
          final ComportData c = new ComportData();
          c.entitesName_ = ents;
          c.not_ = no;
          c.valueToSoustract_ = number;
          comportData_[index++] = c;
        }
      }
      if (index != comportData_.length) {
        final ComportData[] newComport = new ComportData[index];
        System.arraycopy(comportData_, 0, newComport, 0, index);
        comportData_ = newComport;
      }
    }

    @Override
    public DicoEntite getEntite(final int _l, final boolean _allData) {
      final DicoEntite entite = new DicoEntite.Simple(noms_[_l], type_.getType(0));
      entite.setType(type_.getType(_l));
      if (_allData) {
        super.initialyzeEntite(entite, _l);
      }
      return entite;
    }

    public String toString() {
      return "TelemacDicoEntite Simple";
    }

    @Override
    public String printNew(final DicoWriter _out, final String _type, final String _varNbLanguageName,
                           final String _varArray) throws IOException {
      final String var = "entiteSimple";
      _out.writeln(var + "=new DicoEntite.Simple(" + _varArray + CROCHET_O + _varNbLanguageName + "]," + _type
          + getParFerm(), true);
      return var;
    }

    @Override
    public void printComport(final DicoGenerator.DicoWriter _out, final String _varEntite,
                             final String _varMapEntiteComport, final String _varComportData, final String _varTabTabString,
                             final String _varNbLanguageName) throws IOException {
      if ((comportData_ != null) && (comportData_.length > 0)) {
        _out.writeln(_varComportData + "=new DicoComportValues[" + comportData_.length + "];", true);
        for (int i = 0; i < comportData_.length; i++) {
          final ComportData cd = comportData_[i];
          _out.write(_varTabTabString + "=", true);
          _out.printStringArrayDouble(cd.entitesName_, true);
          _out.writeln();
          _out.writeln(_varComportData + CROCHET_O + i + "]=new DicoComportValues("
              + (cd.valueToSoustract_ == null ? "null" : getGuill() + cd.valueToSoustract_ + getGuill())
              + CtuluLibString.VIR + (cd.not_ ? "true" : "false") + CtuluLibString.VIR + _varTabTabString + CROCHET_O
              + _varNbLanguageName + getStrEnd(), true);
        }
        _out.writeln(_varMapEntiteComport + ".put(" + _varEntite + CtuluLibString.VIR + _varComportData + getParFerm(),
            true);
      }
    }

    /**
     * @overrride
     */
    @Override
    public void computeDefaultValues(final String[] _array) {
      type_.computeDefaultValues(_array);
    }
  }

  /**
   * Une entree pouvant contenir plusieurs donnee separees par une virgule.
   */
  public static class Vecteur extends DicoEntiteGenerate {

    String sepChar_;
    boolean isDataColumn_;

    public final boolean isDataColumn() {
      return isDataColumn_;
    }
    
    @Override
    protected ComportData[] getComportData() {
      return null;
    }

    public final void setDataColumn(final boolean _isDataColumn) {
      isDataColumn_ = _isDataColumn;
    }

    /**
     * @param _t le type des donnees
     * @param _sepChar le separateur de valeurs
     */
    public Vecteur(final DicoDataTypeGenerate _t, final String _sepChar) {
      super(_t);
      sepChar_ = _sepChar;
      if (sepChar_ == null) sepChar_ = DicoEntite.getSep();
    }

    @Override
    public void computeDefaultValues(final String[] _array) {
      if (_array == null) { return; }
      final int n = _array.length - 1;
      for (int i = n; i >= 0; i--) {
        final String string = _array[i];
        final String[] values = DicoEntite.Vecteur.getValues(string, sepChar_, isDataColumn_);
        if (values == null) {
          continue;
        }
        type_.computeDefaultValues(values);
        _array[i] = DicoEntite.Vecteur.getStringValuesArray(values, sepChar_);
        if (_array[i] == null) {
          FuLog.warning("DDI: " + noms_[0] + ": problem with default value");
          _array[i] = string;
        }
      }
    }

    public String toString() {
      return DicoResource.getS("Vecteur");
    }

    /**
     * @overrride
     */
    @Override
    protected DicoEntite getEntite(final int _l, final boolean _all) {
      final DicoEntite.Vecteur entite = new DicoEntite.Vecteur(noms_[_l], type_.getType(0), sepChar_);
      entite.setType(type_.getType(_l));
      if (_all) {
        entite.setDataColumn(isDataColumn_);
        super.initialyzeEntite(entite, _l);
      }
      return entite;
    }

    @Override
    public String printNew(final DicoWriter _out, final String _type, final String _varNbLanguageName,
                           final String _varArray) throws IOException {
      final String var = "entiteVecteur";
      _out.writeln(var + "=new DicoEntite.Vecteur(" + _varArray + CROCHET_O + _varNbLanguageName + "]," + _type + ",\""
          + sepChar_ + "\");", true);
      if (isDataColumn_) {
        _out.writeln(var + ".setDataColumn(true);", true);
      }
      return var;
    }
  }

  /**
   * une entree possedant un nombre de donnees fixe. Le tableau peut etre traite de facon dynamique : si trop de donnees
   * sont presentes, elles seront tronquees pour correspondre a la taille du tableau.
   */
  public static class Tableau extends DicoEntiteGenerate {

    private int taille_;
    private boolean dyn_;

    boolean isDataColumn_;

    public final boolean isDataColumn() {
      return isDataColumn_;
    }
    
    

    public final void setDataColumn(final boolean _isDataColumn) {
      isDataColumn_ = _isDataColumn;
    }

    /**
     * @param _t les donnees necessaires
     */
    public Tableau(final DicoDataTypeGenerate _t) {
      super(_t);
    }

    @Override
    public void computeDefaultValues(final String[] _array) {
      if (_array == null) { return; }
      final int n = _array.length - 1;
      for (int i = n; i >= 0; i--) {
        final String string = _array[i];
        final String[] values = DicoEntite.Vecteur.getValues(string, DicoEntite.Tableau.SEPARATOR, isDataColumn_);
        if (values == null) {
          continue;
        }
        type_.computeDefaultValues(values);
        _array[i] = DicoEntite.Vecteur.getStringValuesArray(values, DicoEntite.Tableau.SEPARATOR);
        if (_array[i] == null) {
          FuLog.warning("DDI: " + noms_[0] + ": problem with default value");
          _array[i] = string;
        }
      }
    }

    public String toString() {
      return DicoResource.getS("Tableau");
    }

    /**
     * @overrride
     */
    @Override
    protected DicoEntite getEntite(final int _l, final boolean _all) {
      final DicoEntite.Tableau entite = new DicoEntite.Tableau(noms_[_l], type_.getType(0));
      entite.setTaille(taille_);
      entite.setDynamique(dyn_);
      entite.setType(type_.getType(_l));
      if (_all) {
        entite.setDataColumn(isDataColumn_);
        super.initialyzeEntite(entite, _l);
      }
      return entite;
    }

    /**
     * @param _i la nouvelle taille du tableau.
     */
    public void setTaille(final int _i) {
      taille_ = _i;
    }

    /**
     * @param _b la nouvelle valeur du caractere dynamique.
     */
    public void setDynamique(final boolean _b) {
      dyn_ = _b;
    }

    @Override
    public String printNew(final DicoWriter _out, final String _type, final String _varNbLanguageName,
                           final String _varArray) throws IOException {
      final String var = "entiteTableau";
      _out.writeln(var + "=new DicoEntite.Tableau(" + _varArray + CROCHET_O + _varNbLanguageName + "]," + _type
          + getParFerm(), true);
      _out.writeln(var + ".setTaille(" + taille_ + getParFerm(), true);
      if (dyn_) {
        _out.writeln(var + ".setDynamique(true);", true);
      }
      if (isDataColumn_) {
        _out.writeln(var + ".setDataColumn(true);", true);
      }
      return var;
    }



    @Override
    protected ComportData[] getComportData() {
      return null;
    }
  }

  static class IndexComparator implements Comparator {

    @Override
    public int compare(final Object _o1, final Object _o2) {
      if ((!(_o1 instanceof DicoEntite)) || (!(_o2 instanceof DicoEntite))) { throw new IllegalArgumentException(
          "Comparator can process telemac keyword only"); }
      final DicoEntite t1 = (DicoEntite) _o1;
      final DicoEntite t2 = (DicoEntite) _o2;
      // si les types de donnees sont du meme type
      if (!t1.type_.getClass().isInstance(t2.type_)) { throw new IllegalArgumentException(
          "Only keywords with same type (logical,real,..) are comparable"); }
      if (t1.equalsEntite(t2)) { return 0; }
      final int r = t1.getIndex() - t2.getIndex();
      if (r == 0) { throw new IllegalArgumentException(t1.getNom() + " and  " + t2.getNom() + " have identic index"
          + t1.getIndex()); }
      return r;
    }
  }

  public String[] getAides() {
    return aides_;
  }

  public String[] getDefautValues() {
    return defautValues_;
  }

  public String[] getNoms() {
    return noms_;
  }

  public String[][] getRubriques() {
    return rubriques_;
  }

  public DicoDataTypeGenerate getType() {
    return type_;
  }
}
