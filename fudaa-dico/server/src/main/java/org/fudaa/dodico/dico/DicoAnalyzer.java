/*
 * @file DicoAnalyzer.java
 *
 * @creation 4 avr. 2003
 *
 * @modification $Date: 2007-11-20 11:42:57 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.dico;

import com.memoire.fu.FuLog;
import gnu.trove.TObjectIntHashMap;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * @author deniger
 * @version $Id: DicoAnalyzer.java,v 1.27 2007-11-20 11:42:57 bmarchan Exp $
 */
public class DicoAnalyzer // extends FileOpReaderWriterSimpleAbstract
{
  /**
   * Permet d'obtenir a partir d'un nom du type telemac3dv4p3 le nom (telemac3d) et la version (v4p3). La version doit avoir une
   * longueur de 4.
   *
   * @param _s la chaine a analyser
   * @return le nom + la version
   */
  public final static String[] getNameAndVersion(final String _s) {
    return getNameAndVersion(_s, _s.length());
  }

  /**
   * Recupere a partir du nom de fichier dico
   * <code>_s</code> le nom et la version du code correspondant. Il est suppose que la version est codee sur 4 caractere. Par
   * exemple pour le fichier Telemac2dv2p5, la methode renvoie [telemac2d,v5p3] (nom en minuscule et version telle quelle).
   *
   * @param _s la chaine a analyser
   * @param _endIndex le dernier index de la chaine a considerer
   * @return le nom + version
   */
  public final static String[] getNameAndVersion(final String _s, final int _endIndex) {
    final int versionLength = 4;
    if (_endIndex > (versionLength + 1)) {
      final String[] r = new String[2];
      r[1] = _s.substring(_endIndex - versionLength, _endIndex - versionLength + 4);
      r[0] = _s.substring(0, _endIndex - versionLength).toLowerCase();
      return r;
    }
    return null;
  }

  CtuluAnalyze analyze_;
  private boolean dicoDynamique_;
  private final Path dicoPath;
  // stocke les COMPORT
  Map entiteComport_ = new HashMap();
  private DicoEntiteGenerate[] entites_;
  private DicoKeyword kw_;
  private String name_;
  private int nbLanguage_;
  private String version_;

  /**
   * @param _f le fichier dico
   */
  public DicoAnalyzer(final Path _f) {
    dicoPath = _f;
    findNameAndVersion();
    kw_ = new DicoKeyword();
  }

  public Path getFileName() {
    return dicoPath;
  }

  /**
   * @return le fichier dico
   */
  public String getFilename() {
    return dicoPath.getFileName().toString();
  }

  public Path getDicoPath() {
    return dicoPath;
  }

  private boolean computeChoice(final DicoDataTypeGenerate _type, final String[] _choiceByLanguage, final int _index) {
    if (_choiceByLanguage == null) {
      throw new IllegalArgumentException("argument _choice nul");
    }
    final int t = _choiceByLanguage.length;
    String[] choiceList = CtuluLibString.parseString(_choiceByLanguage[0], DicoKeyword.getChoiceSep());
    int nbChoix = choiceList.length;
    String[] keys = new String[nbChoix];
    String[][] values = new String[t][nbChoix];
    final String[] stemp = new String[2];
    boolean b;
    boolean nokeys = false;
    boolean orUsed = false;
    for (int i = 0; i < nbChoix; i++) {
      b = computeChoiceItem(choiceList[i], stemp);
      if (!b) {
        FuLog.warning("Errors while analyzing keywords " + choiceList[i]);
        return false;
      }
      keys[i] = stemp[0];
      if (stemp[0] == null) {
        nokeys = true;
      } else if (stemp[0].indexOf(getOuTxt()) >= 0) {
        orUsed = true;
      }
      values[0][i] = stemp[1];
    }
    if (orUsed) {
      final List newKeys = new ArrayList(nbChoix + 5);
      final List newValues = new ArrayList(nbChoix + 5);
      for (int i = 0; i < nbChoix; i++) {
        /*
         * b =
         */
        computeChoiceItem(choiceList[i], stemp);
        StringTokenizer tk = null;
        if (stemp[0].indexOf(getOuTxt()) >= 0) {
          tk = new StringTokenizer(stemp[0], getOuTxt());
        } else if (stemp[0].indexOf(getOrTxt()) >= 0) {
          tk = new StringTokenizer(stemp[0], getOrTxt());
        }
        if (tk == null) {
          newKeys.add(stemp[0]);
          newValues.add(stemp[1]);
        } else {
          while (tk.hasMoreTokens()) {
            final String s = tk.nextToken().trim();
            newKeys.add(s);
            newValues.add(stemp[1]);
          }
        }
      }
      keys = new String[newKeys.size()];
      newKeys.toArray(keys);
      values = new String[t][newKeys.size()];
      newValues.toArray(values[0]);
      nbChoix = keys.length;
    }
    for (int i = 1; i < t; i++) {
      choiceList = CtuluLibString.parseString(_choiceByLanguage[i], DicoKeyword.getChoiceSep());
      for (int j = 0; j < nbChoix; j++) {
        b = computeChoiceItem(choiceList[j], stemp);
        if (!b) {
          analyze_.addFatalError("Error while analyzing " + choiceList[j], _index);
          return false;
        }
        if (nokeys) {
          values[i][j] = stemp[1];
        } else {
          if ((orUsed) && (stemp[0].indexOf(getOrTxt()) >= 0)) {
            final StringTokenizer tk = new StringTokenizer(stemp[0], getOrTxt());
            while (tk.hasMoreTokens()) {
              final String k = tk.nextToken();
              if (k.equals(keys[j])) {
                values[i][j] = stemp[1];
              } else {
                final int index = CtuluLibArray.findObject(keys, k);
                if (index >= 0) {
                  values[i][index] = k;
                } else {
                  analyze_.addFatalError("OR keywords not found" + k, _index);
                  return false;
                }
              }
              j++;
            }
          } else {
            if (stemp[0].equals(keys[j])) {
              values[i][j] = stemp[1];
            } else {
              final int index = CtuluLibArray.findObject(keys, stemp[0]);
              if (index >= 0) {
                values[i][index] = stemp[1];
              } else {
                analyze_.addFatalError(DicoResource.getS("Cl� \"{0}\" non trouv�es", choiceList[j]), _index);
                return false;
              }
            }
          }
        }
      }
    }
    if (values.length != nbLanguage_) {
      analyze_.addFatalError(DicoResource.getS("Les choix propos�s ne sont pas suffisants"), _index);
    }
    String s = null;
    if (nokeys) {
      s = _type.setChoix(null, values);
    } else {
      s = _type.setChoix(keys, values);
    }
    if (s != null) {
      analyze_.addFatalError(s, _index);
      return false;
    }
    return true;
  }

  /**
   * @return ou
   */
  private String getOuTxt() {
    return " ou ";
  }

  /**
   * @return or
   */
  private String getOrTxt() {
    return " or ";
  }

  private boolean computeChoiceItem(final String _choice, final String[] _destArray) {
    final String stemp = DicoKeyword.computeValue(_choice);
    final int index = stemp.indexOf('=');
    String value = null;
    if (index < 1) {
      _destArray[0] = null;
      value = stemp;
    } else {
      String key = stemp.substring(0, index).trim();
      if (key.startsWith(getQuoteString())) {
        if (!key.endsWith(getQuoteString())) {
          return false;
        }
        key = key.substring(1, key.length() - 1);
      }
      _destArray[0] = key.trim();
      value = stemp.substring(index + 1).trim();
    }
    if (value == null) {
      return false;
    }
    if (value.startsWith(getQuoteString())) {
      if (!value.endsWith(getQuoteString())) {
        return false;
      }
      _destArray[1] = value.substring(1, value.length() - 1).trim();
    } else {
      _destArray[1] = value.trim();
    }
    _destArray[1] = _destArray[1].replace('�', '�');
    return true;
  }

  /**
   *
   */
  private String getQuoteString() {
    return "'";
  }

  private void computeConditionAffichage() {
    final int n = entites_.length;
    for (int i = 0; i < n; i++) {
      final List m = (List) entiteComport_.get(entites_[i]);
      if (m != null) {
        entites_[i].computeActionAffichage(kw_, entites_, m, analyze_);
      }
    }
  }

  private String[] computeDefautValues(final String[] _initValues, final boolean _isMulti, final String _sep,
                                       final int _indexEntite) {
    if (_initValues == null) {
      return null;
    }
    if (_initValues.length == 0) {
      return _initValues;
    }
    final StringBuffer b = new StringBuffer(_initValues[0].length());
    final int t = _initValues.length;
    final String[] r = new String[t];
    StringTokenizer st;
    // String q= kw_.getQuotedString();
    for (int i = t - 1; i >= 0; i--) {
      b.setLength(0);
      st = new StringTokenizer(_initValues[i], "\n");
      while (st.hasMoreTokens()) {
        b.append(st.nextToken().trim());
      }
      if (_isMulti) {
        final String[] rTemp = DicoEntite.Vecteur.getValues(b.toString(), _sep, false);
        if (rTemp == null) {
          analyze_.addInfoFromFile(
              DicoResource.getS("Erreur avec la valeur par d�faut") + CtuluLibString.ESPACE + b.toString(),
              _indexEntite);
        }
        r[i] = DicoEntite.Vecteur.getStringValuesArray(rTemp, _sep);
        if (r[i] == null) {
          r[i] = CtuluLibString.EMPTY_STRING;
          analyze_.addInfoFromFile(
              DicoResource.getS("Erreur avec la valeur par d�faut") + CtuluLibString.ESPACE + b.toString(),
              _indexEntite);
        }
      } else {
        r[i] = DicoKeyword.computeValue(b.toString());
      }
    }
    return r;
  }

  /**
   * A partir du nom du fichier dico trouve la version.
   */
  private void findNameAndVersion() {
    final String t = dicoPath.getFileName().toString();
    version_ = dicoPath.getParent().getFileName().toString();
    name_ = StringUtils.substringBefore(t, ".");
    if (StringUtils.isBlank(version_)) {
      version_ = DicoResource.getS("Inconnu");
    }
    if (StringUtils.isBlank(name_)) {
      name_ = DicoResource.getS("Inconnu");
    }
  }

  /**
   * Retrouve dans les champ _keys celles qui commencent par le prefixe _prefixKey (par exemples "RUBRIQUE"). Ensuite, recupere
   * dans la table _map les valeurs pour ces champ dans l'ordre des langage (RUBRIQUE,RUBRIQUE1,...). Si _compute, les valeurs
   * sont trait�es avec la methode computeValue de DicoKeyword.
   */
  private String[] findValues(final String _prefixKey, final Set _keys, final Map _map, final boolean _compute) {
    String stemp;
    int t = 0;
    for (final Iterator iter = _keys.iterator(); iter.hasNext(); ) {
      stemp = (String) iter.next();
      if (stemp.startsWith(_prefixKey)) {
        t++;
      }
    }
    if (t == 0) {
      return null;
    }
    String[] noms = new String[t];
    stemp = (String) _map.get(_prefixKey);
    if ((stemp == null) || (stemp.trim().length() == 0)) {
      return null;
    }
    if (_compute) {
      noms[0] = DicoKeyword.computeValue(stemp);
    } else {
      noms[0] = stemp;
    }
    for (int j = 1; j < t; j++) {
      if (_compute) {
        noms[j] = DicoKeyword.computeValue((String) _map.get(_prefixKey + j));
      } else {
        noms[j] = (String) _map.get(_prefixKey + j);
      }
    }
    if (nbLanguage_ == 0) {
      nbLanguage_ = t;
    } else {
      if (t > nbLanguage_) {
        analyze_.addInfo(
            DicoResource.getS("Le mot-cl� {0} propose trop de traduction", (String) _map.get(getNomId())));
        final String[] temp = new String[nbLanguage_];
        System.arraycopy(noms, 0, temp, 0, nbLanguage_);
        noms = temp;
      } else if (t < nbLanguage_) {
        final String copie = (t > 1) ? noms[1] : noms[0];
        final String[] temp = new String[nbLanguage_];
        System.arraycopy(noms, 0, temp, 0, t);
        Arrays.fill(temp, t, nbLanguage_, copie);
        noms = temp;
      }
      // t = nbLanguage_;
    }
    return noms;
  }

  /**
   *
   */
  private String getNomId() {
    return "NOM";
  }

  public DicoEntiteGenerate[] getEntites() {
    return entites_;
  }

  public DicoKeyword getKw() {
    return kw_;
  }

  /**
   * @return le nom du code
   */
  public String getName() {
    return name_;
  }

  /**
   * @return l'indice du langage utilise
   */
  public int getNbLanguage() {
    return nbLanguage_;
  }

  /**
   * @return la version
   */
  public String getVersion() {
    return version_;
  }

  Set knownFileName_ = null;

  public static boolean isKnownFile(final String _frNom) {
    return _frNom.startsWith("FICHIER ");
  }

  private void parseProprietes(final List _props, final TObjectIntHashMap _nomLigne) {
    final int nProp = _props.size();
    final Map[] maps = new Map[nProp];
    _props.toArray(maps);
    _props.clear();
    entites_ = new DicoEntiteGenerate[nProp];
    Map mapEncours;
    Set keys;
    String stemp;
    DicoEntiteGenerate entite;
    DicoDataTypeGenerate type;
    for (int i = 0; i < nProp; i++) {
      mapEncours = maps[i];
      keys = mapEncours.keySet();
      // recherche des noms.
      final String[] noms = findValues(kw_.getNom(), keys, mapEncours, true);
      if ((noms == null) || (noms.length == 0) || noms[0] == null) {
        analyze_.addFatalError(DicoResource.getS("Noms nuls ou pas de version anglaise"));
        return;
      }
      final int ligne = _nomLigne.get(noms[0]);
      /**
       * Type de donnee reel,entier,caractere,logique
       */
      type = kw_.getMatchingType((String) (mapEncours.get(kw_.getType())));
      if (type == null) {
        analyze_.addFatalError(DicoResource.getS("Type non reconnu"), ligne);
        analyze_.addInfoFromFile(noms[0], ligne);
        return;
      }
      // Si type chaine et represente un fichier
      stemp = (String) mapEncours.get(kw_.getApparence());
      if (isKnownFile(noms[0]) || ((stemp != null) && (stemp.indexOf("LISTE IS FICHIER") > 0))) {
        if (!type.setFic(true)) {
          analyze_.addWarn(DicoResource.getS("Le mot-cl� {0} ne peut pas �tre de type fichier", noms[0]), ligne);
        }
      }
      // certains choix sont editable: on recherche un champ dans apparence
      type.setChoiceEditable((stemp != null) && (stemp.indexOf("LISTE IS EDITABLE") > 0));
      final boolean uniqueValueInTab = (stemp != null) && stemp.indexOf("packing IS XmPACK_COLUMN") > 0;
      // MARK: Choix
      stemp = (String) mapEncours.get(kw_.getChoix());
      if (stemp != null) {
        if (!computeChoice(type, findValues(kw_.getChoix(), keys, mapEncours, false), ligne)) {
          analyze_.addFatalError(DicoResource.getS("Erreur lors de l'analyse des choix"), ligne);
          analyze_.addInfoFromFile(noms[0], ligne);
          return;
        }
      }

      // MARK: Controle
      stemp = (String) mapEncours.get(kw_.getControle());
      String[] arraytemp;
      if (stemp != null) {
        arraytemp = CtuluLibString.parseString(stemp, DicoKeyword.getChoiceSep());
        if (arraytemp.length > 2) {
          analyze_.addFatalError(DicoResource.getS("champ CONTROLE invalide"), ligne);
          analyze_.addInfoFromFile(noms[0], ligne);
          return;
        }
        boolean b = true;
        if (arraytemp.length == 1) {
          b = type.setControle(arraytemp[0]);
        } else {
          b = type.setControle(arraytemp[0], arraytemp[1]);
        }
        if (!b) {
          analyze_.addInfoFromFile(noms[0] + " " + DicoResource.getS("CONTROLE non support�"), ligne);
        }
      }
      /**
       * MARK: entite simple,tableau, vecteur
       */
      stemp = (String) mapEncours.get(kw_.getTaille());
      boolean isMulti = false;

      int temp = 0;
      if (stemp != null) {
        temp = Integer.parseInt(stemp);
      }
      final String compose = DicoKeyword.computeValue((String) mapEncours.get(kw_.getCompose()));

      boolean isComposedVect = (compose != null) && (compose.length() > 0);
      final String sepChar = isComposedVect ? compose : DicoKeyword.SEPARATEUR_TABLEAU;
      // String sep=isComposedVect
      // Correction du 7 mai 2007
      // les mot-cl�s qui ont une taille de 2 sont �galement consid�r�s comme �tant a taille variable
      boolean isVect = (temp == 2);
      // if(temp==2 || )
      // si une taille est donnee et est differente de 2, c'est un tableau
      if (temp > 1 && !isVect && !isComposedVect) {
        final DicoEntiteGenerate.Tableau tab = new DicoEntiteGenerate.Tableau(type);
        tab.setTaille(temp);
        if (dicoDynamique_) {
          tab.setDynamique(true);
        }
        tab.setDataColumn(uniqueValueInTab);
        entite = tab;
        isMulti = true;
        // vecteur si taille vaut 2 ou si un separateur est donne
      } else if (isComposedVect || isVect) {
        entite = new DicoEntiteGenerate.Vecteur(type, compose);
        ((DicoEntiteGenerate.Vecteur) entite).setDataColumn(uniqueValueInTab);
        isMulti = true;
      } else {
        entite = new DicoEntiteGenerate.Simple(type);
      }
      entites_[i] = entite;
      entite.setNoms(noms);
      // recherche des valeurs par defaut.
      arraytemp = findValues(kw_.getDefaut(), keys, mapEncours, false);
      arraytemp = computeDefautValues(arraytemp, isMulti, sepChar, ligne);
      // MARK: valeurs par defaut.
      if (arraytemp != null) {
        boolean emptyStringCanBeDefaut = false;
        if (type.isChoiceEnable()) {
          String[] keysChoice = type.getKeys();
          if (keysChoice == null) {
            keysChoice = type.getValues()[0];
          }
          if (keysChoice == null) {
            analyze_.addErrorFromFile("les choix sont nuls", ligne);
            return;
          }
          for (int j = 0; j < keysChoice.length; j++) {
            if (keysChoice[j].trim().length() == 0) {
              emptyStringCanBeDefaut = true;
            }
          }
        }
        if ((emptyStringCanBeDefaut) || (arraytemp[0].trim().length() > 0)) {
          entite.setDefautValues(arraytemp);
        }
      }
      // recherche de l'aide.
      arraytemp = findValues(kw_.getAide(), keys, mapEncours, true);
      if (arraytemp == null) {
        analyze_.addInfoFromFile(DicoResource.getS("Aide non trouv�e"), ligne);
      } else {
        entite.setAides(arraytemp);
      }
      // index
      String s = (String) mapEncours.get(kw_.getIndex());
      if (s == null) {
        entite.setIndex(0);
        analyze_.addInfoFromFile(DicoResource.getS("INDEX mis � 0 pour") + CtuluLibString.ESPACE + entite.getNoms()[0],
            ligne);
      } else {
        entite.setIndex(Integer.parseInt(s));
      }
      s = (String) mapEncours.get(kw_.getNiveau());
      if (s == null) {
        entite.setNiveau(0);
        analyze_.addInfoFromFile(
            DicoResource.getS("NIVEAU mis � 0 pour") + CtuluLibString.ESPACE + entite.getNoms()[0], ligne);
      } else {
        entite.setNiveau(Integer.parseInt(s));
      }
      // MARK: Comport (definition du comportement)
      stemp = (String) mapEncours.get(kw_.getComport());
      if ((stemp != null)) {
        // les comports sont s�par�s par des du style '....';'.....'
        // mais des ; peuvent appartenir au champ dans on parse sur les '
        // et on vire les champs vide ou contenant les ;
        // FRED 10/05/2007 les ; servent r�ellement de s�parateurs
        arraytemp = CtuluLibString.parseString(stemp, ";");
        if (arraytemp != null) {
          final List validToken = new ArrayList();
          for (int tokI = 0; tokI < arraytemp.length; tokI++) {
            final String tk = arraytemp[tokI].trim();
            // on evite les blancs et les ; separant les donn�es
            if (tk.length() > 1) {
              validToken.add(tk);
            }
          }
          arraytemp = new String[validToken.size()];
          validToken.toArray(arraytemp);
        }

        final int l = arraytemp == null ? 0 : arraytemp.length;
        // La cle represente l'affichage voulue (cela peut etre une
        // rubrique ou un mot-cl�) et la valeur represente la condition
        // qui declenche cette affichage.
        final List actionAffCondition = new ArrayList(5);
        final String pref = kw_.getComportAffichageChamp();

        for (int j = l - 1; j >= 0; j--) {
          if (arraytemp != null) {
            stemp = DicoKeyword.computeValue(arraytemp[j]);
          } else {
            stemp = null;
          }
          if (stemp != null && stemp.startsWith(pref)) {
            actionAffCondition.add(stemp);
          }
        }
        if (actionAffCondition.size() > 0) {
          entiteComport_.put(entite, actionAffCondition);
        }
      }
      // MARK: rubrique
      arraytemp = findValues(kw_.getRubrique(), keys, mapEncours, false);
      if (arraytemp == null) {
        analyze_.addInfoFromFile(DicoResource.getS("Rubrique non trouv�e"), ligne);
      } else {
        entite.setRubriques(arraytemp);
      }
      stemp = entite.isDefautValuesValides();
      final String app = (String) mapEncours.get(kw_.getApparence());

      if (stemp != null && ((app == null) || (app.indexOf("IS REQUIRED") < 0))) {
        analyze_.addFatalError(getQuoteString() + noms[0] + "': "
            + DicoResource.getS("Les valeurs par d�faut sont invalides"), ligne);
        analyze_.addInfoFromFile(stemp, ligne);
        return;
      }
      stemp = type.isChoicesValides();

      if (stemp != null) {
        analyze_.addFatalError(getQuoteString() + noms[0] + "': "
            + DicoResource.getS("Les valeurs CHOIX sont invalides"), ligne);
        analyze_.addInfoFromFile(stemp, ligne);
        return;
      }
      if (entites_[i] == null) {
        analyze_.addFatalError(getQuoteString() + noms[0] + "': " + DicoResource.getS("Donn�es invalide"), ligne);
      }
    }
  }

  /**
   * Stocke dans une structure interne les proprietes du fichier dico.
   */
  protected CtuluAnalyze read() {
    analyze_ = new CtuluAnalyze();
    final List props = new ArrayList(200);
    final TObjectIntHashMap nomLigne = new TObjectIntHashMap(200);
    LineNumberReader lineNumberReader = null;
    try (BufferedReader reader = Files.newBufferedReader(dicoPath, Charset.forName("UTF-8"))) {
      lineNumberReader = new LineNumberReader(reader);
      String line;
      String trimLine;
      final char commentaire = kw_.getCommentChar();
      final char commande = kw_.getCommandChar();
      final String nom = kw_.getNom();
      final char eq = '=';
      Map map = null;
      int egalIndex = -1;
      String currentKey = null;
      final StringBuffer currentValue = new StringBuffer(200);
      while ((line = lineNumberReader.readLine()) != null) {
        trimLine = line.trim();
//        System.err.println(trimLine);
        if (trimLine.length() == 0) {
          continue;
        }
        if (trimLine.indexOf(commentaire) == 0) {
          continue;
        }
        if (trimLine.indexOf(commande) == 0) {
          if (kw_.getDynamique().equals(trimLine)) {
            dicoDynamique_ = true;
          }
          continue;
        }
        egalIndex = trimLine.indexOf(eq);
        // C'est une cle
        if ((egalIndex > 0) && (kw_.isKey(trimLine.substring(0, egalIndex).trim()))) {
          if (currentKey != null) {
            // il se peut que le fichier dico soit mal decrit ( 2 fois aide) au
            // lieu de aide et aide1
            if (map != null && map.get(currentKey) != null) {
              analyze_.addInfoFromFile(DicoResource.getS("Le champ {0} est pr�sent plusieurs fois", currentKey),
                  lineNumberReader);
              if (currentKey.endsWith("1")) {
                analyze_.addFatalError(DicoResource.getS(
                    "Mot-cl� {0}: ce type d'erreur n'est pas corrig�. La cl� {1} apparait plusieurs fois",
                    (String) map.get(getNomId()), currentKey), lineNumberReader.getLineNumber());
                return analyze_;
              }
              currentKey += "1";
              if (map.get(currentKey) != null) {
                analyze_.addFatalError(DicoResource.getS(
                    "Mot-cl� {0}: ce type d'erreur n'est pas corrig�. La cl� {1} apparait plusieurs fois",
                    (String) map.get(getNomId()), currentKey), lineNumberReader.getLineNumber());
                return analyze_;
              }
            }
            if (map != null) {
              map.put(currentKey, currentValue.toString().trim());
            }
            if (currentKey.equals(getNomId())) {
              nomLigne.put(DicoKeyword.computeValue(currentValue.toString().trim()), lineNumberReader.getLineNumber());
            }
            currentValue.setLength(0);
          }
          currentKey = trimLine.substring(0, egalIndex).trim();
          if (currentKey.equals(nom)) {
            map = new HashMap();
            props.add(map);
          }
          if (trimLine.length() > egalIndex + 1) {
            currentValue.append(trimLine.substring(egalIndex + 1).trim());
          }
        } else {
          currentValue.append('\n').append(line);
        }
      }
    } catch (final FileNotFoundException e) {
      analyze_.addError(DicoResource.getS("Fichier non trouv�") + ": " + dicoPath.getFileName().toString());
      FuLog.error(e);
    } catch (final IOException e1) {
      IOException io = new IOException(dicoPath + " ,line " + (lineNumberReader == null ? "?" : ("" + lineNumberReader.getLineNumber())), e1);
      analyze_.manageException(io);
      return analyze_;
    }
    if (analyze_.containsFatalError()) {
      return analyze_;
    }
    // A partir des proprietes lues construit les entites utiles
    // a la generation.
    parseProprietes(props, nomLigne);
    if (analyze_.containsFatalError() || analyze_.containsErrors()) {
      entites_ = null;
      return analyze_;
    }
    // Certains mots-cles dependent des autres : on gere cette dependance.
    computeConditionAffichage();
    return analyze_;
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return entites_ == null;
  }

  /**
   * @param _kw le conteneur des identifiants utilises
   */
  public void setDicoKeyword(final DicoKeyword _kw) {
    kw_ = _kw;
  }

  /**
   * @return le comportement de certains mot-cles.
   */
  public Map getEntiteComportement() {
    return entiteComport_;
  }
}
