/**
 * @creation 7 avr. 2003
 * @modification $Date: 2007-06-28 09:25:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.Collection;
import java.util.Iterator;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

import com.memoire.fu.FuLog;

/**
 * @author deniger
 * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
 */
public abstract class DicoDataType {

  static IllegalAccessError createEx() {
    return new IllegalAccessError("immutable object");
  }

  /**
   * Type Binaire.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public final static class Binaire extends DicoDataType {

    /**
     * Toutes les valeurs possibles pour FALSE.
     */
    final static String[] FALSE_VALUES = new String[] { "0", "NON", "FAUX", "NO", "FALSE", ".FALSE." };
    /**
     * Toutes les valeurs possible pour TRUE.
     */
    final static String[] TRUE_VALUES = new String[] { "1", "OUI", "VRAI", "YES", "TRUE", ".TRUE." };
    /**
     * La valeur simple pour un type binaire.
     */
    public final static Binaire EMPTY = new Binaire();
    /**
     * la valeur false a utiliser.
     */
    public final static String FALSE_VALUE = "false";

    /**
     * La valeur true a utiliser.
     */
    public final static String TRUE_VALUE = "true";

    /**
     * @param _t la valeur a tester
     * @return true si appartient aux valeurs TRUE_VALUES. false sinon
     */
    public static boolean getValue(final String _t) {
      return (CtuluLibArray.findObject(TRUE_VALUES, _t.toUpperCase()) >= 0);
    }

    private Binaire() {}

    @Override
    public void accept(final DicoComponentVisitor _m) {
      _m.visitBinaire(this);
    }

    @Override
    public String getControleDescription() {
      return null;
    }

    @Override
    public String getDefaultValue() {
      return FALSE_VALUE;
    }

    @Override
    public String getDescription() {
      return DicoResource.getS("Logique");
    }

    @Override
    public DicoDataType getImmutable() {
      return this;
    }

    @Override
    public String getInvalideMessage(final String _data) {
      if (_data == null) { return DicoResource.getS("Valeur nulle"); }
      final String t = _data.toUpperCase();
      if (((CtuluLibArray.findObject(TRUE_VALUES, t) < 0) && (CtuluLibArray.findObject(FALSE_VALUES, t) < 0))) { return DicoResource
          .getS("Valeur non reconnue"); }
      return null;
    }

    @Override
    public boolean isChaine() {
      return false;
    }

    @Override
    public boolean isFileType() {
      return false;
    }

    @Override
    public boolean isFonctionTrue(final DicoComportValues _fct, final String _v) {
      return (_fct.isNot()) ? !getValue(_v) : getValue(_v);
    }

    @Override
    public boolean isValide(final String _data) {
      if (_data == null) { return false; }
      final String t = _data.toUpperCase();
      return ((CtuluLibArray.findObject(TRUE_VALUES, t) >= 0) || (CtuluLibArray.findObject(FALSE_VALUES, t) >= 0)) ? true
          : false;
    }

    public String toString() {
      return getClass().getName();
    }
  }

  /**
   * Type chaine.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public static class Chaine extends ChoixType {

    private boolean isFic_;

    @Override
    public final void accept(final DicoComponentVisitor _m) {
      _m.visitChaine(this);
    }

    @Override
    public String getControleDescription() {
      return null;
    }

    @Override
    public String getDefaultValue() {
      if (isChoiceEnable()) { return super.getDefaultValue(); }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public String getDescription() {
      return DicoResource.getS("Cha�ne");
    }

    @Override
    public DicoDataType getImmutable() {
      return ImmutableChaine.getImmutableChaine(this);
    }

    @Override
    public String getInvalideMessage(final String _data) {
      if (_data.length() == 0) {
        return null;
      } else if (isChoiceEnable()) {
        return getInvalideMessageIntern(_data);
      } else {
        return null;
      }
    }

    @Override
    public void initialise(final Chaine _c) {
      super.initialise(_c);
      _c.isFic_ = isFic_;
    }

    @Override
    public final boolean isChaine() {
      return true;
    }

    /**
     * @return true si fichier
     */
    @Override
    public final boolean isFileType() {
      return isFic_;
    }

    @Override
    public boolean isFonctionTrue(final DicoComportValues _fct, final String _v) {
      FuLog.error("isFonctionTrue not supported");
      return true;
    }

    @Override
    public final boolean isValide(final String _data) {
      if (_data.length() == 0) { return true; }
      if (isChoiceEnable()) { return isValideIntern(_data); }
      return true;
    }

    public void setFileType(final boolean _b) {
      isFic_ = _b;
    }

    public String toString() {
      return getClass().getName();
    }
  }

  /**
   * Type avec choix. En general, les choix sont decomposes en 2 tableaux: le premier comporte les cl�s et le deuxieme
   * les valeurs correspondantes. Pour un type entier, les cles seront des entiers.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public abstract static class ChoixType extends DicoDataType {

    String[] choiceKeys_;
    String[] choiceValues_;
    /**
     * si true les donnees sont editable.
     */
    private boolean isEditable_;

    /**
     * @return true si editable
     */
    public final boolean isEditable() {
      return isEditable_;
    }

    /**
     * @param _isEditable true si editable
     */
    public final void setEditable(final boolean _isEditable) {
      isEditable_ = _isEditable;
    }

    protected final String getInvalideMessageIntern(final String _data) {
      if (!isValide(_data)) { return DicoResource.getS("La valeur ne correspond pas � un choix impos�"); }
      return null;
    }

    protected boolean isValideIntern(final String _data) {
      if (isEditable_) { return true; }
      return (CtuluLibArray.findObject(choiceKeys_, _data) < 0) ? false : true;
    }

    /**
     * @param _c les cl�s sous forme de collection (chaine)
     */
    public void addChoiceKeys(final Collection _c) {
      final int n = choiceKeys_.length;
      for (int i = 0; i < n; i++) {
        _c.add(choiceKeys_[i]);
      }
    }

    /**
     * Annule les valeurs pour les choix.
     */
    public final void cancelChoice() {
      choiceValues_ = null;
      choiceKeys_ = null;
    }

    /**
     * @return les cles des choix possible
     */
    public final String[] getChoiceKeys() {
      final String[] r = new String[choiceKeys_.length];
      System.arraycopy(choiceKeys_, 0, r, 0, r.length);
      return r;
    }

    /**
     * @return les valeurs
     */
    public final String[] getChoiceValues() {
      if (choiceValues_ == null) { return null; }
      final String[] r = new String[choiceValues_.length];
      System.arraycopy(choiceValues_, 0, r, 0, r.length);
      return r;
    }

    @Override
    public String getDefaultValue() {
      return choiceKeys_[0];
    }

    /**
     * @return le nombre de choix possible
     */
    public int getNbChoice() {
      return choiceKeys_ == null ? 0 : choiceKeys_.length;
    }

    /**
     * @param _keys la cle
     * @return la valeur correspondante dans les choix geres par ce type
     */
    public String getValue(final String _keys) {
      if (!isChoiceEnable()) { return null; }
      final int tempi = CtuluLibArray.findObject(choiceKeys_, _keys);
      return tempi < 0 ? null : choiceValues_[tempi];
    }

    /**
     * @param _c le type utilise pour l'initialisation
     */
    public void initialise(final Chaine _c) {
      if (isChoiceEnable()) {
        initialise((ChoixType) _c);
      }
    }

    /**
     * @param _c source pour initialiser cette instance
     */
    public void initialise(final ChoixType _c) {
      _c.choiceKeys_ = choiceKeys_;
      _c.choiceValues_ = choiceValues_;
      _c.isEditable_ = isEditable_;
    }

    /**
     * @return true si des choix sont proposes
     */
    public final boolean isChoiceEnable() {
      return choiceKeys_ != null;
    }

    /**
     * En general, les choix sont sous la forme cle-> valeur. Il se peut que des choix ne contiennent que les cles.
     * 
     * @return true si des valeurs sont proposees.
     */
    public final boolean isChoiceValuesEnable() {
      return choiceValues_ != null;
    }

    /**
     * Initialisation des choix. La taille des tableaux est controlee.
     * 
     * @param _keys le tableau des cles
     * @param _values le tableau des valeurs correspondantes
     */
    public void setChoice(final String[] _keys, final String[] _values) {
      if (_keys == null) { throw new IllegalArgumentException("keywords null"); }
      choiceKeys_ = _keys;
      choiceValues_ = _values;
      if ((choiceValues_ != null) && (choiceKeys_.length != choiceValues_.length)) { throw new IllegalArgumentException(
          "Arrays have different size"); }
    }
  }

  /**
   * Le type entier.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public static class Entier extends ChoixType {

    private boolean controle_;
    private int max_;
    private int min_;
    private boolean minOnly_;

    @Override
    public void accept(final DicoComponentVisitor _m) {
      _m.visitEntier(this);
    }

    /**
     * @param _keys la cle
     * @return la valeur correspondante dans les choix geres par ce type
     */
    @Override
    public final String getValue(final String _keys) {
      if (!isChoiceEnable()) { return null; }
      int tempi = CtuluLibArray.findObject(choiceKeys_, _keys);
      if (_keys != null && tempi < 0 && _keys.length() > 1 && _keys.endsWith(CtuluLibString.DOT)) {
        tempi = CtuluLibArray.findObject(choiceKeys_, _keys.substring(0, _keys.length() - 1));
      }
      return tempi < 0 ? null : choiceValues_[tempi];
    }

    @Override
    protected final boolean isValideIntern(final String _data) {
      final boolean r = super.isValideIntern(_data);
      if (!r && _data != null && _data.endsWith(CtuluLibString.DOT)) { return isValideIntern(_data.substring(0, _data
          .length() - 1)); }
      return r;

    }

    @Override
    public String getControleDescription() {
      if (controle_) {
        if (minOnly_) { return DicoResource.getS("Valeur sup�rieure � {0}", CtuluLibString.getString(min_)); }
        return DicoResource.getS("Valeur comprise dans l'intervalle [{0},{1}]", CtuluLibString.getString(min_),
            CtuluLibString.getString(max_));
      }
      return null;
    }

    /**
     * @return la valeur max autorisee.
     */
    public int getControleMaxValue() {
      return max_;
    }

    /**
     * @return la valeur min autorisee.
     */
    public int getControleMinValue() {
      return min_;
    }

    @Override
    public String getDefaultValue() {
      if (isChoiceEnable()) { return super.getDefaultValue(); }
      return CtuluLibString.ZERO;
    }

    @Override
    public String getDescription() {
      return DicoResource.getS("Entier");
    }

    @Override
    public DicoDataType getImmutable() {
      return ImmutableEntier.getImmutableEntier(this);
    }

    @Override
    public final String getInvalideMessage(final String _data) {
      if (isChoiceEnable()) { return getInvalideMessageIntern(_data); }
      int t;
      try {
        t = Integer.parseInt(_data);
      } catch (final NumberFormatException e) {
        return DicoResource.getS("La valeur n'est pas un entier");
      }
      if (controle_) {
        if ((minOnly_) && (t < min_)) {
          return DicoResource.getS("La valeur doit �tre sup�rieure � {0}", CtuluLibString.getString(min_));
        } else if ((t < min_) || (t > max_)) { return DicoResource.getS(
            "La valeur doit appartenir � l'intervalle [ {0},{1}]", CtuluLibString.getString(min_), CtuluLibString
                .getString(max_)); }
      }
      return null;
    }

    /**
     * @param _e l'entier qui sera utilise pour initialise ce type
     */
    public void initialiseEntier(final Entier _e) {
      super.initialise(_e);
      _e.controle_ = controle_;
      _e.minOnly_ = minOnly_;
      _e.max_ = max_;
      _e.min_ = min_;
    }

    @Override
    public final boolean isChaine() {
      return false;
    }

    /**
     * @return true si controle actif
     */
    public boolean isControle() {
      return controle_;
    }

    /**
     * @return true si seul le minimum doit etre controle
     */
    public boolean isControleMinOnly() {
      return minOnly_;
    }

    @Override
    public boolean isFileType() {
      return false;
    }

    @Override
    public boolean isFonctionTrue(final DicoComportValues _fct, final String _v) {
      int v = Integer.parseInt(_v);
      int soustract = 0;
      try {
        soustract = _fct.getValueToSoustract() == null ? 0 : Integer.parseInt(_fct.getValueToSoustract());
      } catch (NumberFormatException _evt) {
        FuLog.error(_evt);

      }
      v = v - soustract;
      // si not on inverse le resultat.
      return _fct.isNot() ? (v == 0) : (v != 0);
    }

    @Override
    public boolean isValide(final String _data) {
      if (isChoiceEnable()) { return isValideIntern(_data); }
      int t;
      try {
        t = Integer.parseInt(_data);
      } catch (final NumberFormatException e) {
        return false;
      }
      if (controle_) {
        if ((minOnly_) && (t < min_)) {
          return false;
        } else if ((t < min_) || (t > max_)) { return false; }
      }
      return true;
    }

    /**
     * @param _min le nouveau min a considerer
     */
    public void setControle(final int _min) {
      min_ = _min;
      max_ = Integer.MAX_VALUE;
      controle_ = true;
      minOnly_ = true;
    }

    /**
     * @param _min le min
     * @param _max le max
     */
    public void setControle(final int _min, final int _max) {
      if (_max > _min) {
        min_ = _min;
        max_ = _max;
      } else {
        min_ = _max;
        max_ = _min;
      }
      controle_ = true;
    }

    public String toString() {
      return getClass().getName();
    }
  }

  /**
   * La version immutable pour le type chaine.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public final static class ImmutableChaine extends Chaine {

    /**
     * Un type chaine vide sans controle.
     */
    public static final ImmutableChaine EMPTY = new ImmutableChaine();
    /**
     * Un type pour les type chaine decrivant un fichier.
     */
    public static final ImmutableChaine FILE_TYPE = ImmutableChaine.buildChaineFile();

    private static ImmutableChaine buildChaineFile() {
      final Chaine s = new Chaine();
      s.setFileType(true);
      return new ImmutableChaine(s);
    }

    /**
     * @param _s le type a transformer en immutable
     * @return la version immutable
     */
    public static ImmutableChaine getImmutableChaine(final Chaine _s) {
      if (_s.isChoiceEnable()) {
        return new ImmutableChaine(_s);
      } else if (_s.isFileType()) {
        return FILE_TYPE;
      } else {
        return EMPTY;
      }
    }

    private ImmutableChaine() {}

    private ImmutableChaine(final Chaine _s) {
      _s.initialise(this);
    }

    /**
     * lance une exception.
     */
    @Override
    public void setChoice(final String[] _keys, final String[] _values) {
      throw createEx();
    }

    /**
     * lance une exception.
     */
    @Override
    public void setFileType(final boolean _b) {
      throw createEx();
    }
  }

  /**
   * La version immutable pour le type entier.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public final static class ImmutableEntier extends Entier {

    /**
     * Un type entier de base sans controle. Utiliser pour tous les types simples
     */
    public final static ImmutableEntier EMPTY = new ImmutableEntier();

    /**
     * Si le type passe en parametre n'a pas de choix ni de controle, l'instance EMPTY est utilise.
     * 
     * @param _s le type entier a rendre immutable
     * @return la version immutable pour le type entier
     */
    public static ImmutableEntier getImmutableEntier(final Entier _s) {
      if (_s.isChoiceEnable() || _s.isControle()) { return new ImmutableEntier(_s); }
      return EMPTY;
    }

    private ImmutableEntier() {}

    /**
     * @param _s l'entier utilise pour cette nouvelle instance
     */
    public ImmutableEntier(final Entier _s) {
      _s.initialiseEntier(this);
    }

    @Override
    public void setChoice(final String[] _keys, final String[] _values) {
      throw createEx();
    }

    @Override
    public void setControle(final int _min) {
      throw createEx();
    }

    @Override
    public void setControle(final int _min, final int _max) {
      throw createEx();
    }
  }

  /**
   * La version immutable pour le type reel.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public final static class ImmutableReel extends Reel {

    /**
     * Le type utilisee pour le cas simple (sans controle).
     */
    public final static ImmutableReel EMPTY = new ImmutableReel();

    /**
     * @param _s le type a transformer en immutable
     * @return la version immutable (EMPTY si pas de controle)
     */
    public static ImmutableReel getImmutableReel(final Reel _s) {
      if (_s.isControle()) { return new ImmutableReel(_s); }
      return EMPTY;
    }

    private ImmutableReel() {}

    /**
     * @param _s intialisation a partir de _s
     */
    public ImmutableReel(final Reel _s) {
      _s.initialiseReel(this);
    }

    @Override
    public void setControle(final double _min) {
      if (isControle()) super.setControle(_min);
    }

    @Override
    public void setControle(final double _min, final double _max) {
      if (isControle()) super.setControle(_min, _max);
    }
  }

  /**
   * type reel.
   * 
   * @author Fred Deniger
   * @version $Id: DicoDataType.java,v 1.24 2007-06-28 09:25:08 deniger Exp $
   */
  public static class Reel extends ChoixType {

    private boolean controle_;
    private double max_;
    private double min_;
    private boolean minOnly_;

    @Override
    public void accept(final DicoComponentVisitor _m) {
      _m.visitReel(this);
    }

    @Override
    public String getControleDescription() {
      if (controle_) {
        if (minOnly_) { return DicoResource.getS("Valeur sup�rieure � {0}", Double.toString(min_)); }
        return DicoResource.getS("Valeur comprise dans l'intervalle [{0},{1}]", Double.toString(min_), Double
            .toString(max_));
      }
      return null;
    }

    /**
     * @return la valeur max acceptee
     */
    public double getControleMaxValue() {
      return max_;
    }

    /**
     * @return la valeur min acceptee
     */
    public double getControleMinValue() {
      return min_;
    }

    @Override
    public String getDefaultValue() {
      if (isChoiceEnable()) { return super.getDefaultValue(); }
      return CtuluLibString.ZERO;
    }

    /**
     * return la description du type.
     */
    @Override
    public String getDescription() {
      return DicoResource.getS("R�el");
    }

    /**
     * Gere les notations fortran du type 1D-5 (1E-5).
     * 
     * @param _data la chaine a parser
     * @return la valeur (0 si impossible de transformer)
     */
    public final double getDoubleValue(final String _data) {
      double t = 0D;
      String data = _data;
      if (data.indexOf('d') > 0) {
        data = _data.replace('d', 'E');
      }
      if (data.indexOf('D') > 0) {
        data = _data.replace('D', 'E');
      }
      try {
        t = Double.parseDouble(data);
      } catch (final NumberFormatException e) {}
      return t;
    }

    @Override
    public DicoDataType getImmutable() {
      return ImmutableReel.getImmutableReel(this);
    }

    @Override
    public final String getInvalideMessage(final String _data) {
      double t = 0D;
      String data = _data;
      if (data.indexOf('d') > 0) {
        data = _data.replace('d', 'E');
      }
      if (data.indexOf('D') > 0) {
        data = _data.replace('D', 'E');
      }
      try {
        t = Double.parseDouble(data);
      } catch (final NumberFormatException e) {
        return DicoResource.getS("La valeur n'est pas un r�el");
      }
      if (controle_) {
        if ((minOnly_) && (t < min_)) {
          return DicoResource.getS("La valeur doit �tre sup�rieure � {0}", Double.toString(min_));
        } else if ((t < min_) || (t > max_)) {
          DicoResource.getS("La valeur doit appartenir � l'intervalle [ {0},{1}]", Double.toString(min_), Double
              .toString(max_));
        }
      }
      return null;
    }

    /**
     * @param _e le type utiliser initialiser ce type
     */
    public void initialiseReel(final Reel _e) {
      _e.controle_ = controle_;
      _e.minOnly_ = minOnly_;
      _e.max_ = max_;
      _e.min_ = min_;
    }

    @Override
    public final boolean isChaine() {
      return false;
    }

    /**
     * @return true si controle active
     */
    public boolean isControle() {
      return controle_;
    }

    /**
     * @return true si seul le min doit etre controle
     */
    public boolean isControleMinOnly() {
      return minOnly_;
    }

    @Override
    public final boolean isFileType() {
      return false;
    }

    @Override
    public boolean isFonctionTrue(final DicoComportValues _fct, final String _v) {
      double v = Double.parseDouble(_v);
      final double soustract = _fct.getValueToSoustract() == null ? 0 : Double.parseDouble(_fct.getValueToSoustract());
      v = v - soustract;
      // si not on inverse le resultat.
      return _fct.isNot() ? (v == 0) : (v != 0);
    }

    @Override
    public final boolean isValide(final String _data) {
      if (isChoiceEnable()) { return isValideIntern(_data); }
      double t = 0D;
      String data = _data;
      // if (CtuluLibMessage.DEBUG) {
      if (data.indexOf('d') > 0) {
        data = _data.replace('d', 'E');
      }
      // }
      if (data.indexOf('D') > 0) {
        data = _data.replace('D', 'E');
      }
      try {
        t = Double.parseDouble(data);
      } catch (final NumberFormatException e) {
        return false;
      }
      if (controle_) {
        if ((minOnly_) && (t < min_)) {
          return false;
        } else if ((t < min_) || (t > max_)) {
          return false; }
      }
      return true;
    }

    /**
     * En appelant cette methode, seul le min sera controle.
     * 
     * @param _min le min a controle.
     */
    public void setControle(final double _min) {
      min_ = _min;
      max_ = Double.MAX_VALUE;
      controle_ = true;
      minOnly_ = true;
    }

    /**
     * @param _min le min a accepter
     * @param _max la max a accepter
     */
    public void setControle(final double _min, final double _max) {
      if (_max > _min) {
        min_ = _min;
        max_ = _max;
      } else {
        min_ = _max;
        max_ = _min;
      }
      controle_ = true;
      minOnly_ = false;
    }

    public String toString() {
      return getClass().getName();
    }
  }

  /**
   * @param _dt le type de base pour le test
   * @param _s la collection contenant les valeurs a tester
   * @return true si toutes les valeurs sont valides.
   */
  public final static boolean areAllValid(final DicoDataType _dt, final Collection _s) {
    if (_s == null) { return false; }
    for (final Iterator iter = _s.iterator(); iter.hasNext();) {
      if (!_dt.isValide((String) iter.next())) { return false; }
    }
    return true;
  }

  /**
   * @param _dt le type a utiliser pour les tests
   * @param _s un ensemble de valeurs a tester
   * @return true si toutes les valeurs sont valides
   */
  public final static boolean areAllValid(final DicoDataType _dt, final String[] _s) {
    if (_s == null) { return false; }
    final int n = _s.length - 1;
    for (int i = n; i >= 0; i--) {
      if (!_dt.isValide(_s[i])) { return false; }
    }
    return true;
  }

  /**
   *
   */
  public DicoDataType() {
    super();
  }

  /**
   * @param _m l'interface a visiter
   */
  public abstract void accept(DicoComponentVisitor _m);

  /**
   * @return la chaine decrivant le controle (null si aucun controle)
   */
  public abstract String getControleDescription();

  /**
   * @return la valeur par defaut
   */
  public abstract String getDefaultValue();

  /**
   * @return la description
   */
  public abstract String getDescription();

  /**
   * @return la version immutable du type en cours
   */
  public abstract DicoDataType getImmutable();

  /**
   * @param _data la valeur a verifier
   * @return la chaine d'erreur
   */
  public abstract String getInvalideMessage(String _data);

  /**
   * @return true si de type chaine
   */
  public abstract boolean isChaine();

  /**
   * @return true si de type chaine et fichier
   */
  public abstract boolean isFileType();

  /**
   * @param _fct la fonction (COMPORT) attache a un mot-cle
   * @param _v la valeur du mot-cle
   * @return true si la fonction COMPORT renvoie true pour la valeur _v
   */
  public abstract boolean isFonctionTrue(DicoComportValues _fct, String _v);

  /**
   * @param _data la valeur a verifier
   * @return true si valeur valide
   */
  public abstract boolean isValide(String _data);
}
