/*
 *  @creation     11 janv. 2005
 *  @modification $Date: 2006-04-14 14:51:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import javax.swing.ComboBoxModel;

/**
 * @author Fred Deniger
 * @version $Id: DicoVersionManager.java,v 1.3 2006-04-14 14:51:47 deniger Exp $
 */
public interface DicoVersionManager {

  /**
   * @return le nombre de version definie
   */
  int getNbVersion();

  /**
   * @param _i l'indice de la version
   * @return l'id de la version
   */
  String getVersion(int _i);

  /**
   * @param _i l'indice de la version
   * @return le path de la version
   */
  String getVersionPath(int _i);

  /**
   * @return la liste des versions utilisees
   */
  String[] getVersionId();

  /**
   * @return la liste des path utilises
   */
  String[] getVersionPath();

  ComboBoxModel getVersionModel();
}