/*
 * @creation 25 juin 2003
 * 
 * @modification $Date: 2007-07-16 13:38:23 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

import com.memoire.fu.FuLog;

/**
 * Cette classe permet de gerer les parametres d'un fichier cas: mot-cles invalides, mot-cles representant des fichiers.
 * Des evenements (simples !) sont envoyes a chaque modification. Manage the parameters of a cas file : list the
 * keywords with error and the keywords indicating a file. An event is forward for each modification.
 * 
 * @author deniger
 * @version $Id: DicoParams.java,v 1.39 2007-07-16 13:38:23 bmarchan Exp $
 */
public class DicoParams implements DicoEntiteComparator.ComparatorValueInterface, DicoParamsInterface {

  private Set badValues_;

  private DicoEntite casFileEntite_;

  private Set entitesFiles_;

  private Map entitesValues_;

  protected DicoCasFileFormatVersionAbstract ft_;

  private Set modelListener_;

  Map entitesCommentaires_;

  /**
   * La classe permettant de pr�valider des donn�es.
   */
  DicoParamsPrevalid prevalid_;

  Collection<DicoParamsLinkedSource> linkedSources = new HashSet<DicoParamsLinkedSource>();

  /**
   * @param _i les valeurs initiales des mot-cles
   * @param _dico le format utilise
   */
  public DicoParams(final DicoCasInterface _i, final DicoCasFileFormatVersionAbstract _dico) {
    this(_i == null ? null : _i.getInputs(), _i == null ? null : _i.getComments(), _dico);
  }

  /**
   * @param _inits les valeurs initiales DicoEntite->String
   * @param _keysCommentaire les commentaires DicoEntite->String
   * @param _dico la version utilisee
   */
  public DicoParams(final Map _inits, final Map _keysCommentaire, final DicoCasFileFormatVersionAbstract _dico) {
    ft_ = _dico;
    casFileEntite_ = ft_.getFichierPrincipalEntite();
    entitesValues_ = ((_inits == null) ? (new Hashtable()) : (new Hashtable(_inits)));
    if (casFileEntite_ != null) {
      entitesValues_.remove(casFileEntite_);
    }
    entitesFiles_ = ft_.getDico().getFichierEntitesFor(entitesValues_);
    if (entitesFiles_ == null) {
      entitesFiles_ = new HashSet(20);
    }
    computeValide();
    if (_keysCommentaire != null) {
      entitesCommentaires_ = new Hashtable(_keysCommentaire);
    }

  }

  @Override
  public DicoParamsChangeStateInterface createState() {
    return new DicoParamsChangeState(this);
  }

  private boolean isPrevalid(final DicoEntite _ent) {
    return prevalid_ != null && prevalid_.isChecked(_ent);
  }

  private boolean isPrevalid(final DicoEntite[] _ent) {
    return prevalid_ != null && prevalid_.isChecked(_ent);
  }

  public boolean willChanged(final DicoEntite _ent, final String _newValue) {
    return _ent != null && (canUpdate(_ent) == null) && !getValue(_ent).equals(_newValue);
  }

  public boolean willRemoved(final DicoEntite _ent) {
    return _ent != null && (canUpdate(_ent) == null) && isValueSetFor(_ent) && !_ent.isRequired();
  }

  /**
   * @return true if modified
   */
  final boolean internSetValue(final DicoEntite _ent, final String _value) {
    String value = _value;
    if (value == null) {
      value = CtuluLibString.EMPTY_STRING;
    }
    // L'entite fait deja partie des modifiees
    if (entitesValues_.containsKey(_ent)) {
      // on lui affecte la valeur par defaut : on l'enleve des modifiees
      if (value.equals(_ent.getDefautValue()) && !_ent.isFileType()) {
        final String old = getValue(_ent);
        entitesValues_.remove(_ent);
        badValues_.remove(_ent);
        fireEntiteRemoved(_ent, old);
        return true;
      }
      // on lui affecte la meme valeur ->on oublie
      if (value.equals(getValue(_ent))) {
        return false;
      }
      // on met a jour si valide
      final String old = getValue(_ent);
      if (_ent.isFileType()) {
        if (value.indexOf('\\') > 0) {
          value = value.replace('\\', '/');
        }
      }
      entitesValues_.put(_ent, value);
      if (_ent.isValide(value)) {
        badValues_.remove(_ent);
      } else {
        badValues_.add(_ent);
      }

      fireEntiteUpdated(_ent, old);
      return true;
    }
    // L'entite n'est pas encore modifiee, on l'ajoute
    // on lui affecte la valeur par defaut-> on oublie
    if (value.equals(_ent.getDefautValue())) {
      return false;
    }
    // Si on peut modifier
    if (canUpdate(_ent) == null) {
      entitesValues_.put(_ent, value);
      if (_ent.isValide(value)) {
        badValues_.remove(_ent);
      } else {
        badValues_.add(_ent);
      }
      fireEntiteAdded(_ent);
      return true;
    }
    return false;
  }

  /**
   * Enleve le mot s'il n'est pas requis. Le commentaire est egalement enleve.
   * 
   * @param _ent le mot-cle a enlever
   * @return une commande non null si l'action a eu lieu
   */
  boolean removeValueIntern(final DicoEntite _ent, final CtuluCommandContainer _cmd) {
    if (_ent == null) {
      return false;
    }
    if (_ent.isRequired()) {
      return false;
    }
    final String old = getValue(_ent);
    final String oldCommentaire = getComment(_ent);
    // il faut enlever d'abord le commentaire pour que l'evenement
    // soit envoye avec le bon commentaire !
    if (entitesCommentaires_ != null) {
      entitesCommentaires_.remove(_ent);
    }
    final boolean com = (oldCommentaire != null) && (oldCommentaire.trim().length() > 0);
    final boolean b = removeValue(_ent);

    if (b && _cmd != null) {
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          if (com && (entitesCommentaires_ != null)) {
            entitesCommentaires_.remove(_ent);
          }
          removeValue(_ent);

        }

        @Override
        public void undo() {
          if (com && (entitesCommentaires_ != null)) {
            entitesCommentaires_.put(_ent, oldCommentaire);
          }
          internSetValue(_ent, old);

        }
      });
    }
    return b;
  }

  /**
   *
   */
  final void removeValues(final DicoEntite[] _ent) {
    if (_ent.length == 1) {
      removeValue(_ent[0]);
    }
    removeValues(_ent, _ent.length);
  }

  final void removeValues(final DicoEntite[] _ent, final int _max) {
    for (int i = _max - 1; i >= 0; i--) {
      removeValue(_ent[i]);
    }
  }

  protected final void computeValide() {
    badValues_ = new HashSet(20);
    for (final Iterator it = entitesValues_.keySet().iterator(); it.hasNext();) {
      final DicoEntite ent = (DicoEntite) it.next();
      if (ft_.getDico().contains(ent)) {
        if (ent.isValide(getValue(ent))) {
          if (ent.isFileType()) {
            String value = getValue(ent);
            if (value.indexOf('\\') > 0) {
              value = value.replace('\\', '/');
              // System.err.println("keyword "+ent.getNom()+" modified");
              internSetValue(ent, value);
            }
          }
        } else {
          badValues_.add(ent);
        }
      } else {
        badValues_.add(ent);
      }
    }
    ft_.fillListWithNotSetRequiredEntite(this, badValues_);
  }

  protected void fireEntiteAdded(final DicoEntite _ent) {
    if ((_ent.isFileType()) && (!entitesFiles_.contains(_ent))) {
      entitesFiles_.add(_ent);
    }
    if (modelListener_ != null) {
      synchronized (modelListener_) {
        for (final Iterator it = modelListener_.iterator(); it.hasNext();) {
          ((DicoParamsListener) it.next()).dicoParamsEntiteAdded(this, _ent);
        }
      }
    }
  }

  protected void fireVersionChanged() {
    if (modelListener_ != null) {
      synchronized (modelListener_) {
        for (final Iterator it = modelListener_.iterator(); it.hasNext();) {
          ((DicoParamsListener) it.next()).dicoParamsVersionChanged(this);
        }
      }
    }
  }

  protected void fireEntiteCommentChange(final DicoEntite _e) {
    if (modelListener_ != null) {
      synchronized (modelListener_) {
        for (final Iterator it = modelListener_.iterator(); it.hasNext();) {
          ((DicoParamsListener) it.next()).dicoParamsEntiteCommentUpdated(this, _e);
        }
      }
    }
  }

  protected void fireEntiteRemoved(final DicoEntite _ent, final String _oldValue) {
    if (_ent.isRequired()) {
      badValues_.add(_ent);
    }
    if (_ent.isFileType()) {
      entitesFiles_.remove(_ent);
    }
    if (modelListener_ != null) {
      synchronized (modelListener_) {
        for (final Iterator it = modelListener_.iterator(); it.hasNext();) {
          ((DicoParamsListener) it.next()).dicoParamsEntiteRemoved(this, _ent, _oldValue);
        }
      }
    }
  }

  protected void fireEntiteUpdated(final DicoEntite _ent, final String _oldValue) {
    if (modelListener_ != null) {
      synchronized (modelListener_) {
        for (final Iterator it = modelListener_.iterator(); it.hasNext();) {
          ((DicoParamsListener) it.next()).dicoParamsEntiteUpdated(this, _ent, _oldValue);
        }
      }
    }
  }

  protected void fireEntiteValidStateUpdated(final DicoEntite _ent) {
    if (modelListener_ != null) {
      synchronized (modelListener_) {
        for (final Iterator it = modelListener_.iterator(); it.hasNext();) {
          ((DicoParamsListener) it.next()).dicoParamsValidStateEntiteUpdated(this, _ent);
        }
      }
    }
  }

  /**
   * @param _params les valeurs a ajouter
   */
  public final void addAllValues(final DicoParams _params) {
    for (final Iterator it = _params.entitesValues_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      internSetValue((DicoEntite) e.getKey(), (String) e.getValue());
    }
  }

  /**
   * Cette methode ne peut etre utilisee qu'une seule fois: si la source liee est deja affecte elle n'est pas modifie.
   * 
   * @param _s la nouvelle source li�e peut etre nulle
   * @see #isLinkedSourceSet()
   */
  public void addLinkedSource(final DicoParamsLinkedSource _s) {
    if (_s != null) {
      linkedSources.add(_s);
    }
  }

  /**
   *
   */
  @Override
  public final synchronized void addModelListener(final DicoParamsListener _l) {
    if (modelListener_ == null) {
      modelListener_ = new HashSet(10);
    }
    modelListener_.add(_l);
  }

  @Override
  public final String canUpdate(final DicoEntite _ent) {
    if (ft_.getDico().contains(_ent)) {
      if (_ent.getNiveau() < 0) {
        return DicoResource.getS("Ce mot-cl� n'est pas modifiable");
      }
      return null;
    }
    return DicoResource.getS("Ce mot-cl� n'est pas d�fini dans le fichier dico");
  }

  @Override
  public boolean contains(final DicoEntite _ent) {
    return entitesValues_.containsKey(_ent);
  }

  /**
   * @param _casFileName le nom du fichier cas (pour initialise le mot-cle principal)
   * @return l'interface permettant d'ecrire le fichier cas.
   */
  public DicoCasInterface createCasWriterInterface(final String _casFileName) {
    final Map m = getEntiteValues();
    if (casFileEntite_ != null) {
      if (_casFileName != null) {
        m.put(casFileEntite_, _casFileName);
      }
    }
    return new DicoCasResult(m, (entitesCommentaires_ == null ? null : new HashMap(entitesCommentaires_)));
  }

  /**
   *
   */
  @Override
  public final Set getAllEntiteWithValuesSet() {
    return new HashSet(entitesValues_.keySet());
  }

  /**
   * @return le nom du code
   */
  public String getCodeName() {
    return ft_.getCodeName();
  }

  /**
   * Renvoie le commentaire pour le mot-cle e ou null si aucun commentaire.
   */
  @Override
  public String getComment(final DicoEntite _e) {
    return (entitesCommentaires_ == null) ? null : (String) entitesCommentaires_.get(_e);
  }

  /**
   * @return la table des commentaires ou null si aucun commentaire
   */
  public Map getCommentMap() {
    return (entitesCommentaires_ == null) ? null : new Hashtable(entitesCommentaires_);
  }

  @Override
  public DicoModelAbstract getDico() {
    return ft_.getDico();
  }

  /**
   * Do not remove item !!!!
   * 
   * @return un iterateur sur les mot-cles "fichiers" definis
   */
  public Iterator getDicoEntiteFileEnum() {
    return entitesFiles_.iterator();
  }

  @Override
  public DicoCasFileFormat getDicoFileFormat() {
    return (DicoCasFileFormat) ft_.getFileFormat();
  }

  // public DicoEntite getDicoEntiteFile(int _idx) {
  // return (DicoEntite)entitesFiles_.get(_idx);
  // }
  @Override
  public DicoCasFileFormatVersionAbstract getDicoFileFormatVersion() {
    return ft_;
  }

  @Override
  public int getEntiteFileNb() {
    return (entitesFiles_ == null ? 0 : entitesFiles_.size());
  }

  /**
   * @return un tableau des mot-cles "fichier" utilises et requis
   */
  public Set getEntiteFileSetAndRequiredList() {
    // on recupere tous les mot-cles fichiers definis
    final Set s = new HashSet(entitesFiles_);
    // on ajoute les mot-cles fichiers requis mais non initialise
    getDicoFileFormatVersion().fillListWithNotSetRequiredEntite(this, s);
    return s;
  }

  /**
   *
   */
  @Override
  public Set getEntiteFileSetList() {
    return new HashSet(entitesFiles_);
  }

  /**
   * A partir des listes anciennes et nouvelles des mot-cle a cacher renvoie le tableau des mot-cle a cacher-montrer.
   * Return an hide-view array from to the old hide-keyword-list and the new one
   */
  @Override
  public DicoEntite[][] getEntiteToHideChange(final DicoEntite[] _oldHide, final DicoEntite[] _newHide) {
    final List hide = new ArrayList();
    final List view = new ArrayList();
    int n = _oldHide.length;
    DicoEntite ent;
    for (int i = 0; i < n; i++) {
      ent = _oldHide[i];
      if (Arrays.binarySearch(_newHide, ent) < 0) {
        view.add(ent);
      }
    }
    n = _newHide.length;
    for (int i = 0; i < n; i++) {
      ent = _newHide[i];
      if (Arrays.binarySearch(_oldHide, ent) < 0) {
        hide.add(ent);
      }
    }
    final DicoEntite[][] r = new DicoEntite[2][];
    r[0] = new DicoEntite[hide.size()];
    hide.toArray(r[0]);
    r[1] = new DicoEntite[view.size()];
    view.toArray(r[1]);
    return r;
  }

  // public int getEntiteFileNb() {
  // return entitesFiles_.size();
  // }
  /**
   * Renvoie un tableau a deux lignes (ou null): premiere ligne represente les mot-cles a cacher et la deuxieme ligne
   * represente les mot-cles lies au mot-cle passe en param. Cela depend de la valeur donnee. Renvoie null le mot-cle
   * n'a pas de comport specifique. Return an array with two rows ( or null) : first row list the keywords to hide and
   * the second row list the keywords which depend on <code>_ent</code>. return null when the keyword has no specific
   * behavior. The result depends on the value <code>_value</code>.
   * 
   * @return null ou 2 tableaux tries dans l'ordre. null or 2 arrays in the natural order.
   */
  @Override
  public DicoEntite[][] getEntiteToHideToTie(final DicoEntite _ent, final String _value) {
    final Map m = ft_.getDico().getEntiteComportValues();
    if ((m != null) && (m.containsKey(_ent))) {
      final DicoEntite[][] r = new DicoEntite[2][];
      final DicoComportValues[] cvs = (DicoComportValues[]) m.get(_ent);
      final int l = cvs.length;
      final List view = new ArrayList();
      final List hide = new ArrayList();
      final DicoDataType type = _ent.getType();
      DicoComportValues cv;
      final DicoEntiteList list = ft_.getDico().getList();
      for (int i = 0; i < l; i++) {
        cv = cvs[i];
        if (type.isFonctionTrue(cv, _value)) {
          view.addAll(cv.getDicoEntite(list));
        } else {
          hide.addAll(cv.getDicoEntite(list));
        }
      }
      // si des mot-cl�s sont dans les 2 listes, on donne la priorit� � la visibilit�
      hide.removeAll(view);
      r[0] = new DicoEntite[hide.size()];
      hide.toArray(r[0]);
      r[1] = new DicoEntite[view.size()];
      view.toArray(r[1]);
      return r;
    }
    return null;
  }

  @Override
  public final Map getEntiteValues() {
    return new HashMap(entitesValues_);
  }

  @Override
  public final void getEntiteValues(final Map _m) {
    _m.putAll(entitesValues_);
  }

  /**
   * @see org.fudaa.dodico.dico.DicoParamsInterface#getInvalidMessage(org.fudaa.dodico.dico.DicoEntite)
   * @return null si aucun message d'erreur pour ce mot-cle
   */
  @Override
  public String getInvalidMessage(final DicoEntite _e) {
    if (!isValueValideFor(_e)) {
      if (isValueSetFor(_e)) {
        return _e.getInvalideMessage(getValue(_e));
      } else if (_e.isRequired()) {
        return DicoResource.getS("Ce mot-cl� doit �tre d�fini");
      } else {
        return DicoResource.getS("Inconnu");
      }
    }
    return null;
  }

  public final static DicoEntite[] enTableau(final List _l) {
    return (DicoEntite[]) _l.toArray(new DicoEntite[_l.size()]);
  }

  public final static DicoEntite[] enTableau(final Set _l) {
    return (DicoEntite[]) _l.toArray(new DicoEntite[_l.size()]);
  }

  /**
   * @return le titre du projet
   */
  @Override
  public String getTitle() {
    /**
     * @todo B.M. Pour eviter le plantage quand le mot cl� "TITRE" est inexistant. Voir s'il n'y a pas mieux a faire
     */
    if (ft_.getTitreEntite() == null)
      return "";
    return getValue(ft_.getTitreEntite());
  }

  @Override
  public final String getValue(final DicoEntite _ent) {
    String r = getSetValue(_ent);
    if (r == null) {
      r = _ent.getDefautValue();
    }
    return r;
  }

  public String getSetValue(final DicoEntite _ent) {
    if (_ent == null) {
      return null;
    }
    return (String) entitesValues_.get(_ent);
  }

  @Override
  public final int getValuesSize() {
    return entitesValues_.size();
  }

  @Override
  public List getViewableEntite() {
    final Map m = ft_.getDico().getEntiteComportValues();
    if (m == null) {
      return ft_.getDico().getEntitesInNewList();
    }
    final Set toHide = new HashSet();
    final Set toView = new HashSet();
    for (final Iterator it = m.keySet().iterator(); it.hasNext();) {
      final DicoEntite ent = (DicoEntite) it.next();
      final DicoEntite[][] hideView = getEntiteToHideToTie(ent, getValue(ent));
      if (!CtuluLibArray.isEmpty(hideView[0]))
        toHide.addAll(Arrays.asList(hideView[0]));
      if (!CtuluLibArray.isEmpty(hideView[1]))
        toView.addAll(Arrays.asList(hideView[1]));
    }
    final Set r = new HashSet(ft_.getDico().getEntitesInNewList());
    r.removeAll(toHide);
    r.addAll(toView);
    return new ArrayList(r);
  }

  @Override
  public boolean isEntiteWithComportBehavior(final DicoEntite _ent) {
    final Map m = ft_.getDico().getEntiteComportValues();
    return (m != null) && (m.containsKey(_ent));
  }

  /**
   * Si renvoie true, la source ne sera plus modifiee.
   * 
   * @return true si la source liee est non nulle.
   */
  public boolean isLinkedSourceSet() {
    return linkedSources != null;
  }

  /*
   * public boolean isModified() { return isModified_; }
   */

  @Override
  public final boolean isDataValide() {
    return badValues_.size() == 0;
  }

  @Override
  public final boolean isValueSetFor(final DicoEntite _ent) {

    return _ent == null ? false : entitesValues_.containsKey(_ent);
  }

  @Override
  public final boolean isValueValideFor(final DicoEntite _ent) {
    return !badValues_.contains(_ent);
  }

  /**
   * Attention n'envoie pas d'evt.
   */
  public final void removeAll() {
    if (entitesValues_ != null) {
      entitesValues_.clear();
    }
    if (badValues_ != null) {
      badValues_.clear();
    }
  }

  public void removeCheckedValue(final DicoEntite _ent, final CtuluCommandContainer _cmd) {
    if (isPrevalid(_ent)) {
      prevalid_.preAcceptRemoved(new DicoEntite[] { _ent }, _cmd);
    } else {
      removeValue(_ent, _cmd);
    }
  }

  public void removeCheckedValue(final DicoEntite[] _ent, final CtuluCommandContainer _cmd) {
    if (isPrevalid(_ent)) {
      prevalid_.preAcceptRemoved(_ent, _cmd);
    } else {
      removeValues(_ent, _cmd);
    }

  }

  /**
   * @param _l le listener a enlever
   */
  public final void removeModelListener(final DicoParamsListener _l) {
    if (modelListener_ == null) {
      return;
    }
    synchronized (modelListener_) {
      modelListener_.remove(_l);
    }
  }

  /**
   * @param _ent le mot-cle a initialiser
   * @return true si le mot-cle a ete initialise
   */
  public final boolean removeValue(final DicoEntite _ent) {
    if (_ent == null) {
      return false;
    }
    if (entitesValues_.containsKey(_ent)) {
      final String old = getValue(_ent);
      entitesValues_.remove(_ent);
      badValues_.remove(_ent);
      fireEntiteRemoved(_ent, old);
      return true;
    }
    return false;
  }

  /**
   * @param _ent le mot-cle a initialise
   * @return la commande si modification ou null sinon
   */
  public boolean removeValue(final DicoEntite _ent, final CtuluCommandContainer _cmd) {
    final CtuluCommandCompositeInverse cmp = _cmd == null ? null : new CtuluCommandCompositeInverse();
    final boolean modified = removeValueIntern(_ent, cmp);
    if (modified) {
      if (!linkedSources.isEmpty()) {
        for (DicoParamsLinkedSource linked : linkedSources) {
          final CtuluCommand c2 = linked.keywordRemoved(_ent);
          if (c2 != null && cmp != null) {
            cmp.addCmd(c2);
          }
        }

      }
      if (_cmd != null && cmp != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
    }
    return modified;
  }

  /**
   * @param _ent les mot-cles a enlever
   * @return la commande generee ou null si aucun changement
   */
  public final boolean removeValues(final DicoEntite[] _ent, final CtuluCommandContainer _cmd) {
    if (CtuluLibArray.isEmpty(_ent)) {
      return false;
    }
    if (_ent.length == 1) {
      return removeValue(_ent[0], _cmd);
    }
    final CtuluCommandCompositeInverse r = _cmd == null ? null : new CtuluCommandCompositeInverse();
    boolean modified = false;
    for (int i = _ent.length - 1; i >= 0; i--) {
      if (removeValueIntern(_ent[i], _cmd)) {
        modified = true;
        for (DicoParamsLinkedSource linked : linkedSources) {
          final CtuluCommand cmdi = linked.keywordRemoved(_ent[i]);
          if (r != null && cmdi != null) {
            r.addCmd(cmdi);
          }
        }
      }
    }
    if (_cmd != null && r != null) {
      _cmd.addCmd(r.getSimplify());
    }
    return modified;
  }

  public void setCheckedValue(final DicoEntite _ent, final String _value, final CtuluCommandContainer _cmd) {
    if (isPrevalid(_ent)) {
      prevalid_.preAcceptSet(_ent, _value, _cmd);
    } else {
      setValue(_ent, _value, _cmd);
    }

  }

  /**
   * Les commentaires ne sont pris en compte que si la valeur est modifiee.
   * 
   * @param _e le mot-cle
   * @param _s le nouveau commentaire
   */
  public void setComment(final DicoEntite _e, final String _s) {
    if (isValueSetFor(_e)) {
      if (entitesCommentaires_ == null) {
        entitesCommentaires_ = new Hashtable();
      }
      if ((!entitesCommentaires_.containsKey(_e)) || !entitesCommentaires_.get(_e).equals(_s)) {
        entitesCommentaires_.put(_e, _s);
        fireEntiteCommentChange(_e);
      }
    }
  }

  /**
   * @param _v le vecteur a modifier
   * @param _newVal la nouvel etat du mot-cle
   * @return true si modif, null sinon
   */
  public boolean setRequired(final DicoEntite.Vecteur _v, final boolean _newVal, final CtuluCommandContainer _cmd) {
    if (_newVal == _v.isRequired()) {
      return false;
    }
    final boolean old = _v.isRequired();
    _v.setRequired(_newVal);
    testIfValid(_v);
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          _v.setRequired(_newVal);
          testIfValid(_v);
        }

        @Override
        public void undo() {
          _v.setRequired(old);
          testIfValid(_v);
        }
      });
    }
    return true;
  }

  /**
   * @param _v le vecteur a modifier
   * @param _newVal la nouvelle taille du vecteur
   * @return commande si modif, null sinon
   */
  public boolean setTaille(final DicoEntite.Vecteur _v, final int _newVal, final CtuluCommandContainer _cmd) {
    if (_newVal == _v.getNbElemFixed()) {
      return false;
    }
    final int old = _v.getNbElemFixed();
    _v.setNbElem(_newVal);
    testIfValid(_v);
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          _v.setNbElem(_newVal);
          testIfValid(_v);
        }

        @Override
        public void undo() {
          _v.setNbElem(old);
          testIfValid(_v);
        }
      });
    }
    return true;
  }

  public boolean setValue(final DicoEntite _ent, final String _value) {
    return setValue(_ent, _value, null);
  }

  /**
   * @param _ent le mot-cle a modifier
   * @param _value la nouvelle valeur
   * @return la commande associee ou null si aucun changement
   */
  public boolean setValue(final DicoEntite _ent, final String _value, final CtuluCommandContainer _cmd) {
    if (_ent == null) {
      return false;
    }
    final String old = getValue(_ent);
    final boolean b = internSetValue(_ent, _value);
    if (b) {
      // c est la commande qui sera ajouter au container
      CtuluCommand c = null;
      if (_cmd != null) {
        c = new CtuluCommand() {

          @Override
          public void redo() {
            internSetValue(_ent, _value);
          }

          @Override
          public void undo() {
            internSetValue(_ent, old);
          }
        };
      }
      if (!linkedSources.isEmpty()) {
        final CtuluCommandCompositeInverse cr = _cmd == null ? null : new CtuluCommandCompositeInverse();
        if (cr != null) {
          cr.addCmd(c);
          c=cr;
        }
        for (DicoParamsLinkedSource linked : linkedSources) {
          final CtuluCommand cmdi = linked.keywordAddedOrUpdated(_ent, old, _value);
          if (cr != null && cmdi != null) {
            cr.addCmd(cmdi);
          }
        }
      }
      if (_cmd != null) {
        _cmd.addCmd(c);
      }
    }
    return b;
  }

  /**
   * Test si le mot-cle est valide et met a jour le tableau des mot-cles invalide. Si changement un evt est envoye.
   * 
   * @param _e le mot-cle a tester
   */
  public void testIfValid(final DicoEntite _e) {
    if ((ft_.getDico().contains(_e)) && (isValueSetFor(_e))) {
      final String s = getValue(_e);
      if (_e.isValide(s)) {
        if (badValues_.contains(_e)) {
          badValues_.remove(_e);
          fireEntiteValidStateUpdated(_e);
        }
      } else if (!badValues_.contains(_e)) {
        badValues_.add(_e);
        fireEntiteValidStateUpdated(_e);
      }
    }
  }

  /**
   * Enleve la source liee.
   */
  public void unsetLinkedSource() {
    linkedSources = null;
  }

  public void setPrevalid(final DicoParamsPrevalid _prevalid) {
    if (prevalid_ == _prevalid) {
      return;
    }
    if (prevalid_ != null) {
      FuLog.error("DDI: Two prevalidator");
    }
    prevalid_ = _prevalid;
  }

}
