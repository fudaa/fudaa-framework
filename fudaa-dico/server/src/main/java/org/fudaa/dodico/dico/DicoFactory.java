/**
 * @creation 19 mai 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;
import java.util.Map;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
/**
 * Permet de generer les classes pour des fichiers dico.
 * @author deniger
 * @version $Id: DicoFactory.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
 */
public class DicoFactory {

  private final String packageName_;
  private File dico_;
  private File dir_;

  /**
   * @param _packageName le nom du paquetage a utiliser. Envoie une exception si null.
   */
  public DicoFactory(final String _packageName) {
    if (_packageName == null) {
      throw new IllegalArgumentException("Argument null");
    }
    packageName_ = _packageName;
  }

  /**
   * @param _dest le dossier qui contiendra les classes generees.
   */
  public void setDestDir(final File _dest){
    dir_ = _dest;
  }

  /**
   * @param _dico le fichier dico a parser.
   */
  public void setDicoFile(final File _dico){
    dico_ = _dico;
  }

  /**
   * @param _an le tableau d'analyze a ecrire sur la sortie standard.
   */
  public void printAnalyze(final CtuluAnalyze[] _an){
    if (_an[0] != null) {
      FuLog.all("Analyse");
      _an[0].printResume();
    }
    if (_an[1] != null) {
      FuLog.all("Generation");
      _an[1].printResume();
    }
  }

  /**
   * Permet d'analyser les parametres : le fichier dico est bien specifie, le rep de dest existe...
   * @return l'analyse de l'operation
   */
  public CtuluAnalyze analyzeParametres(){
    final CtuluAnalyze r = new CtuluAnalyze();
    if (dico_ == null) {
      r.addError(DicoResource.getS("Fichier dico non sp�cifi�"), 0);
    }
    else if (!dico_.exists()) {
      r.addError(DicoResource.getS("Fichier dico non trouv�"), 0);
    }
    if (dir_ == null) {
      r.addError(DicoResource.getS("Le r�pertoire de destination n'est pas sp�cifi�"), 0);
    }
    else {
      if (!dir_.isDirectory()) {
        r.addError(DicoResource.getS("La cible n'est pas un r�pertoire"), 0);
      }
    }
    if (packageName_ == null) {
      r.addError(DicoResource.getS("Package de destination non sp�cifi�"), 0);
    }
    return r.containsErrors() ? r : null;
  }

  /**
   * Dans un premier temps, verifie les parametres. Puis analyse les fichiers dico et enfin genere
   * les classes.
   * @return les resultats des operations analyse/ecriture des classes
   */
  public CtuluAnalyze[] generate(){
    final CtuluAnalyze an = analyzeParametres();
    if (an != null) {
      return new CtuluAnalyze[] { an};
    }
    return DicoGenerator.readAndGeneration(dico_.toPath(), dir_, packageName_);
  }

  /**
   * A partir des arguments, initialise la classe.
   * @param _args les arguments passes en ligne de commande
   */
  public void getValueFromMainArgs(final String[] _args){
    final Map m = CtuluLibString.parseArgs(_args);
    String t = (String) m.get("-dicoFile");
    if (t != null) {
      dico_ = new File(t);
    }
    t = (String) m.get("-destDir");
    if (t != null) {
      dir_ = new File(t);
    }
  }

}
