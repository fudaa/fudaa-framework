/**
 * 
 */
package org.fudaa.dodico.dico;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.fudaa.ctulu.CtuluCommand;

/**
 * @author genesis
 */
public class DicoMultipleUndoCommands implements CtuluCommand {

  private final Map<DicoEntite, String> newValues;
  private final Map<DicoEntite, String> oldValue;
  private final DicoParams target;

  public DicoMultipleUndoCommands(Map<DicoEntite, String> newValues, DicoParams target) {
    super();
    this.newValues = newValues;
    this.target = target;
    oldValue = new HashMap<DicoEntite, String>();
    for (Entry<DicoEntite, String> entry : newValues.entrySet()) {
      oldValue.put(entry.getKey(), target.getValue(entry.getKey()));
    }
  }

  @Override
  public void undo() {
    for (Entry<DicoEntite, String> entry : oldValue.entrySet()) {
      target.setValue(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public void redo() {
    for (Entry<DicoEntite, String> entry : newValues.entrySet()) {
      target.setValue(entry.getKey(), entry.getValue());
    }
  }

}
