/*
 *  @creation     5 mai 2003
 *  @modification $Date: 2006-04-14 14:51:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

/**
 * Pattern visiteur.
 *
 * @author deniger
 * @version $Id: DicoComponentVisitor.java,v 1.8 2006-04-14 14:51:47 deniger Exp $
 */
public interface DicoComponentVisitor {
  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitSimple(DicoEntite.Simple _ent);

  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitVecteur(DicoEntite.Vecteur _ent);

  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitTableau(DicoEntite.Tableau _ent);

  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitChaine(DicoDataType.Chaine _ent);

  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitEntier(DicoDataType.Entier _ent);

  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitReel(DicoDataType.Reel _ent);

  /**
   * @param _ent le mot-cle qui visite cette interface
   */
  void visitBinaire(DicoDataType.Binaire _ent);
}
