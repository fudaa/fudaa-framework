/*
 * @creation 4 avr. 2003
 * @modification $Date: 2007-05-22 13:11:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import com.memoire.fu.FuLog;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author deniger
 * @version $Id: DicoKeyword.java,v 1.11 2007-05-22 13:11:22 deniger Exp $
 */
public class DicoKeyword {
  /**
   * le separateur de tableau.
   */
  public final static String SEPARATEUR_TABLEAU = ";";

  /**
   * Retire les ' en double, ainsi que le premier et le dernier.
   *
   * @param _s la valeur a valider
   * @return valeur sans '
   */
  public static String computeValue(final String _s) {
    if (_s == null) {
      return null;
    }
    final StringBuffer r = new StringBuffer(_s.trim());
    final char quote = getQuotedChar();
    int l = _s.length();
    int t = _s.indexOf(quote);
    if (t == 0) {
      t = _s.lastIndexOf(quote);
      if (t == l - 1) {
        r.deleteCharAt(t);
        r.deleteCharAt(0);
      } else {
        FuLog.warning("The field " + _s + " must be terminated with '");
        return null;
      }
    }
    l = r.length() - 1;
    for (int i = l; i >= 0; i--) {
      if (r.charAt(i) == quote && r.charAt(i - 1) == quote) {
        r.deleteCharAt(i--);
      }
      if (r.charAt(i) == '"') {
        r.setCharAt(i, '\'');
      }
    }
    return r.toString().trim();
  }

  /**
   * @return caractere de separation ;
   */
  public static String getChoiceSep() {
    return ";";
  }

  /**
   * @return caractere de separation ;
   */
  public static char getChoiceSepChar() {
    return ';';
  }

  /**
   * @return caractere de separation * pour les rubriques dans le COMPORT
   */
  public static char getComportRubriqueSep() {
    return '*';
  }

  /**
   * @return '
   */
  public static char getQuotedChar() {
    return '\'';
  }

  /**
   * les identifiants utiliser pour definir un mot-cle.
   */
  private final String[] keys_ = new String[]{"NOM", "NOM1", "TYPE", "INDEX", "MNEMO", "CONTROLE", "TAILLE",
      "DEFAUT", "DEFAUT1", "CHOIX", "CHOIX1", "RUBRIQUE", "RUBRIQUE1", "COMPORT", "NIVEAU", "COMPOSE", "APPARENCE",
      "AIDE", "AIDE1", "SUBMIT"};
  private final String[] types_ = new String[]{"ENTIER", "REEL", "CARACTERE", "LOGIQUE"};

  /**
   * initialise les tableaux.
   */
  public DicoKeyword() {
    Arrays.sort(keys_);
    Arrays.sort(types_);
  }

  /**
   * @return l'identifiant aide
   */
  public String getAide() {
    return "AIDE";
  }

  /**
   * @return l'identifiant apparence
   */
  public String getApparence() {
    return "APPARENCE";
  }

  /**
   * @return l'identifiant choix
   */
  public String getChoix() {
    return "CHOIX";
  }

  /**
   * @return l'identifiant choix 1
   */
  public String getChoix1() {
    return "CHOIX1";
  }

  /**
   * @return le caractere precedant une commande
   */
  public char getCommandChar() {
    return '&';
  }

  /**
   * @return le caractere precedant un commentaire
   */
  public char getCommentChar() {
    return '/';
  }

  /**
   * @return l'identifiant comport
   */
  public String getComport() {
    return "COMPORT";
  }

  /**
   * @return l'identifiant pour COMPORT d'affichage
   */
  public String getComportAffichageChamp() {
    return "Affichage";
  }

  /**
   * @return l'identifiant pour inverser le resultat d'un comport
   */
  public String getComportNotValueId() {
    return "IS NOT_VALEUR";
  }

  /**
   * @return l'identifiant pour la valeur d'un comport
   */
  public String getComportValueId() {
    return "IS VALEUR";
  }

  /**
   * @return les 2 identifiant pour le valeur dans les sections COMPORT
   */
  public String[] getComportValues() {
    return new String[]{"IS VALEUR", "IS NOT_VALEUR"};
  }

  /**
   * @return id COMPOSE
   */
  public String getCompose() {
    return "COMPOSE";
  }

  /**
   * @return id CONTROLE
   */
  public String getControle() {
    return "CONTROLE";
  }

  /**
   * @return id DEFAUT
   */
  public String getDefaut() {
    return "DEFAUT";
  }

  /**
   * @return id DEFAUT1
   */
  public String getDefaut1() {
    return "DEFAUT1";
  }

  /**
   * @return la commande pour rendre les tableaux dynamique (pas d'erreur si pas la bonne taille)
   */
  public String getDynamique() {
    return "&DYN";
  }

  /**
   * @return id INDEX
   */
  public String getIndex() {
    return "INDEX";
  }

  /**
   * @param _type la description du type
   * @return le type correspondant ou null si aucun.
   */
  public DicoDataTypeGenerate getMatchingType(final String _type) {
    DicoDataTypeGenerate r = null;
    if (getTypeEntier().equals(_type)) {
      r = new DicoDataTypeGenerate.Entier();
    } else if (getTypeEntier().contains(_type)) {
      r = new DicoDataTypeGenerate.Entier();
    } else if (getTypeCaractere().contains(_type)) {
      r = new DicoDataTypeGenerate.Chaine();
    } else if (getTypeReel().contains(_type)) {
      r = new DicoDataTypeGenerate.Reel();
    } else if (getTypeLogique().contains(_type)) {
      r = new DicoDataTypeGenerate.Binaire();
    }
    return r;
  }

  /**
   * @return l'id NIVEAU
   */
  public String getNiveau() {
    return "NIVEAU";
  }

  /**
   * @return l'id NOM
   */
  public String getNom() {
    return "NOM";
  }

  /**
   * @return l'id NOM1
   */
  public String getNom1() {
    return "NOM1";
  }

  /**
   * @return '
   */
  public String getQuotedString() {
    return "'";
  }

  /**
   * @return l'id RUBRIQUE
   */
  public String getRubrique() {
    return "RUBRIQUE";
  }

  /**
   * @return l'id TAILLE
   */
  public String getTaille() {
    return "TAILLE";
  }

  /**
   * @return l'id TYPE
   */
  public String getType() {
    return "TYPE";
  }

  /**
   * @return l'id pour les type caractere
   */
  public Set<String> getTypeCaractere() {
    return Collections.unmodifiableSet(new HashSet<>(Arrays.asList("CARACTERE", "CHARACTER", "STRING")));
  }

  /**
   * @return l'id pour les type entier
   */
  public Set<String> getTypeEntier() {
    return Collections.unmodifiableSet(new HashSet<>(Arrays.asList("ENTIER", "INTEGER")));
  }

  /**
   * @return l'id pour les types logique
   */
  public Set<String> getTypeLogique() {
    return Collections.unmodifiableSet(new HashSet<>(Arrays.asList("LOGIQUE", "LOGICAL")));
  }

  /**
   * @return l'id pour les type reel
   */
  public Set<String> getTypeReel() {
    return Collections.unmodifiableSet(new HashSet<>(Arrays.asList("REEL", "REAL")));
  }

  /**
   * @param _s la chaine a tester.
   * @return true si la chaine _s est un identifiant de section
   */
  public boolean isKey(final String _s) {
    final int i = Arrays.binarySearch(keys_, _s);
    final int slength = _s.length();
    if ((i >= 0)
        || ((_s.startsWith(getNom())) && ((slength == getNom().length()) || (slength == getNom().length() + 1)))
        || ((_s.startsWith(getChoix())) && ((slength == getChoix().length()) || (slength == getChoix().length() + 1)))
        || ((_s.startsWith(getDefaut())) && ((slength == getDefaut().length()) || (slength == getDefaut().length() + 1)))) {
      return true;
    }
    return false;
  }
}
