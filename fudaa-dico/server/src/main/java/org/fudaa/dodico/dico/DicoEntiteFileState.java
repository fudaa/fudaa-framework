/*
 *  @creation     5 nov. 2003
 *  @modification $Date: 2006-09-19 14:42:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;

/**
 * @author deniger
 * @version $Id: DicoEntiteFileState.java,v 1.9 2006-09-19 14:42:27 deniger Exp $
 */
public class DicoEntiteFileState {
  private long fileLastTimeModifiedWhenLoad_;
  private File f_;
  private boolean isModified_;
  private boolean isProjectFile_;

  public DicoEntiteFileState() {}

  /**
   * @param _f le fichier lie au mot-cle
   * @param _isModified true si modifie
   */
  public DicoEntiteFileState(final File _f, final boolean _isModified) {
    loadFile(_f);
    setModified(_isModified);
  }

  /**
   * @return le fichier
   */
  public final File getF() {
    return f_;
  }

  /**
   * @return la date de dernier modif du fichier
   */
  public final long getFileLastTimeModifiedWhenLoad() {
    return fileLastTimeModifiedWhenLoad_;
  }

  /**
   * @return true si modifie
   */
  public final boolean isModified() {
    return isModified_;
  }

  /**
   * Permet de noter l'instant de chargement du fichier. la date de derniere modif est sauvegardee dans un champ privee.
   *
   * @return true si changement
   * @param _file le fichier qui vient d'etre charge
   */
  protected final boolean loadFile(final File _file) {
    boolean r = false;
    if ((_file != f_) && ((_file == null) || (!_file.equals(f_)))) {
      r = true;
    }
    final long old = f_ == null ? 0L : f_.lastModified();
    f_ = _file;
    if (f_ != null) {
      fileLastTimeModifiedWhenLoad_ = f_.lastModified();
    }
    if (old != fileLastTimeModifiedWhenLoad_) {
      return true;
    }
    return r;
  }

  protected final boolean setModified(final boolean _b) {
    if (_b != isModified_) {
      isModified_ = _b;
      return true;
    }
    return false;
  }

  /**
   * @return si ce mot-cle est le mot-cle decrivant le fichier principal du projet. En general il s'agit du nom du
   *         fichier cas.
   */
  public boolean isProjectFile() {
    return isProjectFile_;
  }

  protected void setProjectFile(final boolean _b) {
    isProjectFile_ = _b;
  }
}
