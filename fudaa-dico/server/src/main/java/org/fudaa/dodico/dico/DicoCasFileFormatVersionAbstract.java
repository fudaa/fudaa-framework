/**
 * @creation 29 sept. 2003
 * @modification $Date: 2007-06-28 09:25:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersion;

import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: DicoCasFileFormatVersionAbstract.java,v 1.20 2007-06-28 09:25:08 deniger Exp $
 */
public abstract class DicoCasFileFormatVersionAbstract extends FileFormatVersion {

  DicoModelAbstract dico_;

  /**
   * @param _ft le format
   * @param _dico le modele contenant tous les mots-cles
   */
  public DicoCasFileFormatVersionAbstract(final FileFormat _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico.getVersion());
    dico_ = _dico;
  }

  public abstract FileFormatSoftware getSoftVersion();

  public final int getOtherLanguageIndex() {
    return dico_.getOtherLanguageIndex();
  }

  public final int getLanguageIndex() {
    return dico_.getLanguageIndex();
  }

  public String toString() {
    return dico_.getCodeName() + CtuluLibString.ESPACE + dico_.getVersion();
  }

  /**
   * @return le nom du code utilise
   */
  public String getCodeName() {
    return dico_.getCodeName();
  }

  /**
   * @return le mot-cle donne le nom du fichier cas
   */
  public abstract DicoEntite getFichierPrincipalEntite();

  /**
   * @return le mot-cle donnant le titre de l'etude
   */
  public abstract DicoEntite getTitreEntite();

  /**
   * @param _i le conteneur des mot-cl
   * @param _l la collection a remplir avec les mots-cle requis mais non initialises.
   */
  public void fillListWithNotSetRequiredEntite(final DicoParamsInterface _i, final Set _l) {}

  /**
   * @return le model dico
   */
  public DicoModelAbstract getDico() {
    return dico_;
  }

  /**
   * @param _noms les noms possible (francai-anglais) du mots-cle a rechercher
   * @return le mot-cle ayant le nom donnee dans le tableau
   */
  public final DicoEntite getEntiteFor(final String[] _noms) {
    DicoEntite r = null;
    if (_noms.length > dico_.getLanguageIndex()) {
      r = dico_.getEntite(_noms[dico_.getLanguageIndex()]);
    }
    if (r != null) {
      return r;
    }
    final int n = _noms.length;
    for (int i = 0; i < n; i++) {
      r = dico_.getEntite(_noms[i]);
      if (r != null) {
        return r;
      }
    }
    return null;
  }

  public final DicoEntite getEntiteFor(final String _nomFr, final String _nomEn) {
    DicoEntite r = null;
    // on teste selon le langage
    final int i = dico_.getLanguageIndex();
    if (i == 0) {
      r = dico_.getEntite(_nomFr);
      if (r != null) {
        return r;
      }
    } else if (i == 1) {
      r = dico_.getEntite(_nomEn);
      if (r != null) {
        return r;
      }
    }
    r = dico_.getEntite(_nomFr);
    if (r != null) {
      return r;
    }
    r = dico_.getEntite(_nomEn);
    if (r != null) {
      return r;
    }
    return r;
  }

  /**
   * Search in the dict list, the keyword which heading is _r.
   * 
   * @param _r la rubrique cherchee
   * @return les mot-cles ayant comme rubrique _r
   */
  public DicoEntite[] getEntitesForHeading(final String _r) {
    if (_r == null) {
      return null;
    }
    final List r = new ArrayList();
    for (final Iterator it = dico_.getList().iterator(); it.hasNext();) {
      final DicoEntite ent = (DicoEntite) it.next();
      if (_r.equals(ent.getRubrique())) {
        r.add(ent);
      }
    }
    return DicoParams.enTableau(r);
  }

  /**
   * Search in the dict list, the keyword which heading is _rInLanguage in the right language.
   * 
   * @param _rInlanguage les rubriques en francais/anglais
   * @return les mot-cles du dico ayant comme rubrique rInLanguage
   */
  public DicoEntite[] getEntitesForHeading(final String[] _rInlanguage) {
    if ((_rInlanguage == null) || (_rInlanguage.length != 2)) {
      return null;
    }
    if (DicoLanguage.FRENCH_ID == dico_.getLanguageIndex()) {
      return getEntitesForHeading(_rInlanguage[0]);
    }
    return getEntitesForHeading(_rInlanguage[1]);
  }

  /**
   * Search whith the Iterator it, the keyword which heading is _r.
   * 
   * @param _r la rubrique a considerer
   * @param _it un iterateur sur les mot-cles
   * @return les mot-cles ayant comme rubrique _r.
   */
  public DicoEntite[] getEntitesForHeading(final String _r, final Iterator _it) {
    if (_r == null) {
      return null;
    }
    final List r = new ArrayList();
    while (_it.hasNext()) {
      final DicoEntite ent = (DicoEntite) _it.next();
      if (_r.equals(ent.getRubrique())) {
        r.add(ent);
      }
    }
    return DicoParams.enTableau(r);
  }

  /**
   * Search whith the Iterator it, the keyword which heading is _rInLanguage in the right language.
   * 
   * @param _rInlanguage le tableau contenant les donnees en francais/anglais
   * @param _it un iterateur sur des mot-cles
   * @return les mot-cles ayant comme rubrique rInLanguage
   */
  public DicoEntite[] getEntitesForHeading(final String[] _rInlanguage, final Iterator _it) {
    if ((_rInlanguage == null) || (_rInlanguage.length != 2)) {
      return null;
    }
    if (DicoLanguage.FRENCH_ID == dico_.getLanguageIndex()) {
      return getEntitesForHeading(_rInlanguage[0], _it);
    }
    return getEntitesForHeading(_rInlanguage[1], _it);
  }

  /**
   * @return le caractere identifiant un commentaire
   */
  public char getCommentChar() {
    return '/';
  }

  /**
   * @return le caractere identifiant un commentaire
   */
  public String getCommentString() {
    return "/";
  }

  /**
   * @return la chaine decrivant la fin d'un fichier cas.
   */
  public String getCommandStop() {
    return "&FIN";
  }

  /**
   * @return la chaine pour stopper le programme
   */
  public String getCommandStopProgram() {
    return "&STO";
  }

  /**
   * @return la chaine pour ecrire les mot-cles
   */
  public String getCommandPrintKeys() {
    return "&LIS";
  }

  /**
   * @return la chaine pour ecrire les mot-cles en version longue
   */
  public String getCommandPrintLongKeys() {
    return "&IND";
  }

  /**
   * @return chaine pour ecrire les valeurs des mot-cles
   */
  public String getCommandPrintKeysValues() {
    return "&ETA";
  }
}