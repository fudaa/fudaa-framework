/**
 * @creation 7 avr. 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.IOException;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoGenerator.DicoWriter;

/**
 * Une classe fourre-tout utilisee lors de l'anayse d'un dictionnaire.
 *
 * @author deniger
 * @version $Id: DicoDataTypeGenerate.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
 */
public abstract class DicoDataTypeGenerate {

  protected String[] choiceKeys_;
  protected String[][] choiceValues_;
  protected boolean isChoiceEditable_;

  static String getEnd() {
    return ");";
  }

  public final static String getStrCtrl() {
    return ".setControle(";
  }

  public final boolean isChoiceEditable() {
    return isChoiceEditable_;
  }

  public final void setChoiceEditable(final boolean _isChoiceEditable) {
    isChoiceEditable_ = _isChoiceEditable;
  }

  /**
   * Ne fait rien.
   */
  public DicoDataTypeGenerate() {
    super();
  }

  /**
   * @return les cles pour les choix
   */
  public String[] getKeys() {
    return choiceKeys_;
  }

  /**
   * @param _s les valeurs par defaut a analyser
   */
  public void computeDefaultValues(final String[] _s) {}

  /**
   * @return les valeurs des choix
   */
  public String[][] getValues() {
    return choiceValues_;
  }

  /**
   * @return true si les choix sont actives
   */
  public boolean isChoiceEnable() {
    return choiceValues_ != null;
  }

  protected String print(final DicoGenerator.DicoWriter _out, final String _varNbLanguageName, final String _varChoiceKeysName,
      final String _varChoiceValuesName) throws IOException {
    final String var = printNew(_out);
    if (var == null) {
      return null;
    }
    if (choiceValues_ != null) {
      if (choiceKeys_ == null) {
        _out.writeln(_varChoiceKeysName + "=null;", true);
      } else {
        _out.write(_varChoiceKeysName + "=", true);
        _out.printStringArray(choiceKeys_, true);
        _out.writeln();
      }
      _out.write(_varChoiceValuesName + "=", true);
      _out.printStringArrayDouble(choiceValues_, true);
      _out.writeln();
      if (choiceKeys_ == null) {
        _out.writeln(var + ".setChoice(" + _varChoiceValuesName + "[" + _varNbLanguageName
            + "]," + _varChoiceKeysName + getEnd(), true);
      } else {
        _out.writeln(var + ".setChoice(" + _varChoiceKeysName + CtuluLibString.VIR + _varChoiceValuesName + "[" + _varNbLanguageName
            + "]);", true);
      }
      if (isChoiceEditable_) {
        _out.writeln(var + ".setEditable(true);", true);
      }
    }
    return var;
  }

  protected abstract String printNew(DicoGenerator.DicoWriter _out) throws IOException;

  /**
   * @param _language l'indice du langage a utiliser
   * @return le type qui va bien
   */
  protected abstract DicoDataType getType(int _language);

  protected String isChoicesValides() {
    if (isChoiceEnable()) {
      DicoDataType type;
      final int nbLanguage = choiceValues_.length;
      if (choiceKeys_ == null) {
        final int nb = choiceValues_[0].length;
        for (int i = 0; i < nbLanguage; i++) {
          type = getType(i);
          for (int j = 0; j < nb; j++) {
            if (!type.isValide(choiceValues_[i][j])) {
              return choiceValues_[i][j] + CtuluLibString.ESPACE + DodicoLib.getS("non valide");
            }
          }
        }
      } else {
        for (int i = 0; i < nbLanguage; i++) {
          type = getType(i);
          final int nChoix = choiceKeys_.length;
          for (int j = 0; j < nChoix; j++) {
            if (!type.isValide(choiceKeys_[j])) {
              return choiceKeys_[i] + CtuluLibString.ESPACE
                  + DodicoLib.getS("non valide");
            }
          }
        }
      }
    }
    return null;
  }

  protected String setChoix(final String[] _keys, final String[][] _values) {
    return "Not implemented " + toString();
  }

  protected boolean setControle(final String _c1, final String _c2) {
    return false;
  }

  protected boolean setControle(final String _c1) {
    return false;
  }

  protected boolean setFic(final boolean _b) {
    return false;
  }

  /**
   * Type pour generer des mot-cle binaire.
   *
   * @author Fred Deniger
   * @version $Id: DicoDataTypeGenerate.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
   */
  public static class Binaire extends DicoDataTypeGenerate {

    private final DicoDataType.Binaire type_ = DicoDataType.Binaire.EMPTY;

    public String toString() {
      return type_.toString();
    }

    @Override
    protected DicoDataType getType(final int _l) {
      return type_;
    }

    @Override
    protected String printNew(final DicoWriter _out) throws IOException {
      final String r = "typeBinaire";
      _out.writeln(r + "=DicoDataType.Binaire.EMPTY;", true);
      return r;
    }

    @Override
    public void computeDefaultValues(final String[] _s) {
      for (int i = _s.length - 1; i >= 0; i--) {
        _s[i] = ((DicoDataType.Binaire.getValue(_s[i])) ? DicoDataType.Binaire.TRUE_VALUE
            : DicoDataType.Binaire.FALSE_VALUE);
      }
    }
  }

  /**
   * Type chaine.
   *
   * @author Fred Deniger
   * @version $Id: DicoDataTypeGenerate.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
   */
  public static class Chaine extends ChoixType {

    private final DicoDataType.Chaine type_ = new DicoDataType.Chaine();

    /**
     * @return true si fichier
     */
    public boolean isFileType() {
      return type_.isFileType();
    }

    @Override
    protected boolean setFic(final boolean _b) {
      type_.setFileType(_b);
      return true;
    }

    @Override
    protected DicoDataType.ChoixType getType() {
      return type_;
    }

    @Override
    protected String printNew(final DicoWriter _out) throws IOException {
      final String r = "typeChaine";
      _out.writeln(r + "=new DicoDataType.Chaine();", true);
      if (type_.isFileType()) {
        _out.writeln(r + ".setFileType(true);", true);
      }
      return r;
    }
  }

  abstract static class ChoixType extends DicoDataTypeGenerate {

    @Override
    protected String setChoix(final String[] _keys, final String[][] _values) {
      if (_values == null) {
        return "zero values";
      }
      choiceKeys_ = _keys;
      choiceValues_ = _values;
      if (choiceValues_.length > 1 && choiceValues_[1] == null) {
        choiceValues_[1] = choiceValues_[0];
      }
      if (choiceValues_[0] == null) {
        return "zero values";
      }
      if ((choiceKeys_ != null) && (choiceKeys_.length != choiceValues_[0].length)) {
        return "Arrays'length are different";
      }
      return null;
    }

    protected abstract DicoDataType.ChoixType getType();

    public final String toString() {
      return getType().toString();
    }

    @Override
    protected final DicoDataType getType(final int _l) {
      final DicoDataType.ChoixType type = getType();
      if (isChoiceEnable()) {
        if (choiceKeys_ == null) {
          type.setChoice(choiceValues_[_l], choiceKeys_);
        } else {
          type.setChoice(choiceKeys_, choiceValues_[_l]);
        }
        type.setEditable(isChoiceEditable());
      }
      return type;
    }
  }

  /**
   * Type entier.
   *
   * @author Fred Deniger
   * @version $Id: DicoDataTypeGenerate.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
   */
  public static class Entier extends ChoixType {

    private final DicoDataType.Entier type_ = new DicoDataType.Entier();

    /**
     * @return true si controle active
     */
    public boolean isControle() {
      return type_.isControle();
    }

    /**
     * @return true si seul le min est controle.
     */
    public boolean isControleMinOnly() {
      return type_.isControleMinOnly();
    }

    @Override
    protected DicoDataType.ChoixType getType() {
      return type_;
    }

    /**
     * Active le controle.
     *
     * @param _min la borne min
     * @param _max la borne max
     */
    public void setControle(final int _min, final int _max) {
      type_.setControle(_min, _max);
    }

    /**
     * Active le controle sur la borne inf seulement.
     *
     * @param _min la borne min.
     */
    public void setControle(final int _min) {
      type_.setControle(_min);
    }

    @Override
    protected String printNew(final DicoWriter _out) throws IOException {
      final String r = "typeEntier";
      _out.writeln(r + "=new DicoDataType.Entier();", true);
      if (isControle()) {
        if (isControleMinOnly()) {
          _out.writeln(r + getStrCtrl() + type_.getControleMinValue() + getEnd(), true);
        } else {
          _out.writeln(r + getStrCtrl() + type_.getControleMinValue() + CtuluLibString.VIR + type_.getControleMaxValue() + getEnd(),
              true);
        }
      }
      return r;
    }

    /**
     * Permet de definir les bornes de validite pour ce type. Un des champs peut etre nul: dans ce cas, l'autre champ
     * sera considere comme le minimum.
     */
    @Override
    public boolean setControle(final String _c1, final String _c2) {
      if ((_c1 == null) || (_c2 == null)) {
        throw new IllegalArgumentException("One (or two) argument is(are) null");
      }
      int c1 = 0;
      int c2 = 0;
      try {
        c1 = Integer.parseInt(_c1);
        c2 = Integer.parseInt(_c2);
      } catch (final NumberFormatException e) {
        FuLog.warning("arguments are not integer");
        return false;
      }
      if (c2 > c1) {
        setControle(c1, c2);
      } else {
        setControle(c2, c1);
      }
      return true;
    }

    @Override
    public boolean setControle(final String _c1) {
      if ((_c1 == null)) {
        throw new IllegalArgumentException("The argument is null");
      }
      int c1 = 0;
      try {
        c1 = Integer.parseInt(_c1);
      } catch (final NumberFormatException e) {
        FuLog.warning("argument is not an integer");
        return false;
      }
      setControle(c1);
      return true;
    }
  }

  /**
   * Reel.
   *
   * @author Fred Deniger
   * @version $Id: DicoDataTypeGenerate.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
   */
  public static class Reel extends ChoixType {

    private final DicoDataType.Reel type_ = new DicoDataType.Reel();

    /**
     * @return true si controle active
     */
    public boolean isControle() {
      return type_.isControle();
    }

    @Override
    public void computeDefaultValues(final String[] _s) {
      if (_s == null) {
        return;
      }
      final int n = _s.length - 1;
      for (int i = n; i >= 0; i--) {
        if (type_.isValide(_s[i])) {
          _s[i] = _s[i];
        } else {
          throw new IllegalArgumentException("bad default value " + type_ + CtuluLibString.getObjectInString(_s, true));
        }
      }
    }

    /**
     * @return si le controle est active et seule la borne inf est testee.
     */
    public boolean isControleMinOnly() {
      return type_.isControleMinOnly();
    }

    @Override
    protected String printNew(final DicoWriter _out) throws IOException {
      final String r = "typeReel";
      _out.writeln(r + "=new DicoDataType.Reel();", true);
      if (isControle()) {
        if (isControleMinOnly()) {
          _out.writeln(r + getStrCtrl() + type_.getControleMinValue() + getEnd(), true);
        } else {
          _out.writeln(r + getStrCtrl() + type_.getControleMinValue() + CtuluLibString.VIR + type_.getControleMaxValue() + getEnd(),
              true);
        }
      }
      return r;
    }

    @Override
    protected DicoDataType.ChoixType getType() {
      return type_;
    }

    /**
     * Active le controle.
     *
     * @param _min la borne min
     * @param _max la borne max
     */
    public void setControle(final double _min, final double _max) {
      type_.setControle(_min, _max);
    }

    /**
     * Active le controle sur la borne min uniquement.
     *
     * @param _min le min
     */
    public void setControle(final double _min) {
      type_.setControle(_min);
    }

    /**
     * Permet de definir les bornes de validite pour ce type. Un des champs peut etre nul: dans ce cas, l'autre champ
     * sera considere comme le minimum.
     */
    @Override
    public boolean setControle(final String _c1, final String _c2) {
      if ((_c1 == null) || (_c2 == null)) {
        throw new IllegalArgumentException("one argument is null ( or both)");
      }
      double c1 = 0;
      double c2 = 0;
      try {
        c1 = Double.parseDouble(_c1);
        c2 = Double.parseDouble(_c2);
      } catch (final NumberFormatException e) {
        FuLog.warning("Arguments are not real");
        return false;
      }
      setControle(c1, c2);
      return true;
    }

    @Override
    public boolean setControle(final String _c1) {
      if ((_c1 == null)) {
        throw new IllegalArgumentException("Argument is null");
      }
      double c1 = 0;
      try {
        c1 = Double.parseDouble(_c1);
      } catch (final NumberFormatException e) {
        FuLog.warning("The argumentis not real");
        return false;
      }
      setControle(c1);
      return true;
    }
  }
}
