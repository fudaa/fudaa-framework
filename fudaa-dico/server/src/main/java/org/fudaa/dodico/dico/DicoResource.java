/**
 *  @creation     22 sept. 2003
 *  @modification $Date: 2006-09-19 14:42:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import com.memoire.bu.BuResource;

import org.fudaa.dodico.commun.DodicoResource;

/**
 * @author deniger
 * @version $Id: DicoResource.java,v 1.9 2006-09-19 14:42:27 deniger Exp $
 */
public final class DicoResource extends DodicoResource {
  /**
   * Le singleton.
   */
  public final static DicoResource DICO = new DicoResource(DodicoResource.DODICO);

  private DicoResource(final BuResource _parent) {
    super(_parent);
  }

  /**
   * A "shortcut" to get i18n String .
   */
  public static String getS(final String _s) {
    return DICO.getString(_s);
  }

  /**
   * A "shortcut" to get i18n String .
   */
  public static String getS(final String _s, final String _v0) {
    return DICO.getString(_s, _v0);
  }

  /**
   * A "shortcut" to get i18n String.
   */
  public static String getS(final String _s, final String _v0, final String _v1) {
    return DICO.getString(_s, _v0, _v1);
  }
}
