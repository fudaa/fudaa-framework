/**
 *  @creation     29 sept. 2003
 *  @modification $Date: 2007-07-16 13:40:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.fileformat.FileFormat;
/**
 * @author deniger
 * @version $Id: DicoCasFileFormatAbstract.java,v 1.9 2007-07-16 13:40:26 bmarchan Exp $
 */
public abstract class DicoCasFileFormatAbstract extends FileFormat {
  /**
   * @param _nom le nom du code
   */
  public DicoCasFileFormatAbstract(final String _nom) {
    super(1);
    nom_= _nom;
    id_= nom_.toUpperCase();
  }
  
}
