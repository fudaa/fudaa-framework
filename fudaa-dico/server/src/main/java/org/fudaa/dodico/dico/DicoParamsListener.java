/*
 * @creation 19 mai 2003
 * @modification $Date: 2007-04-30 14:21:38 $
 * @license GNU General License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

/**
 * Un mot-cle est dit modifie si sa valeur est differente de la valeur par defaut.
 * 
 * @author deniger
 * @version $Id: DicoParamsListener.java,v 1.13 2007-04-30 14:21:38 deniger Exp $
 */
public interface DicoParamsListener {

  /**
   * Envoye si le mot-cle est ajoute : sa valeur est differente de la valeur par defaut.
   * 
   * @param _cas la source de l'evt
   * @param _ent le mot-cle concerne
   */
  void dicoParamsEntiteAdded(DicoParams _cas, DicoEntite _ent);

  /**
   * Envoye si le mot-cle est intialise: sa valeur est egale a la valeur par defaut.
   * 
   * @param _cas la source de l'evt
   * @param _ent le mot-cle concerne
   * @param _oldValue l'ancienne valeur
   */
  void dicoParamsEntiteRemoved(DicoParams _cas, DicoEntite _ent, String _oldValue);

  /**
   * Envoye si la valeur est modifie : le mot-cle etait modifie avant et le reste.
   * 
   * @param _cas la source de l'evt
   * @param _ent le mot-cle concerne
   * @param _oldValue l'ancienne valeur
   */
  void dicoParamsEntiteUpdated(DicoParams _cas, DicoEntite _ent, String _oldValue);

  /**
   * @param _cas le fichier cas
   * @param _ent le mot-cl�s pour lequel le commentaire est modifi�
   */
  void dicoParamsEntiteCommentUpdated(DicoParams _cas, DicoEntite _ent);


  /**
   * Envoye si le mot-cle "fichier" a ete charge ou est considere comme modifie.
   * 
   * @param _cas la source de l'evt
   * @param _ent le mot-cle concerne
   */
  //void dicoParamsStateLoadedEntiteChanged(DicoParams _cas, DicoEntite _ent);

  /**
   * Envoye si la validite d'un mot-cle a ete modifie.
   * 
   * @param _cas la source de l'evt
   * @param _ent le mot-cle concerne
   */
  void dicoParamsValidStateEntiteUpdated(DicoParams _cas, DicoEntite _ent);

  void dicoParamsVersionChanged(final DicoParams _cas);
}