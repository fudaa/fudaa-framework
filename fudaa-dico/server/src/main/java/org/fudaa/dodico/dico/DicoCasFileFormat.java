/**
 * @creation 14 avr. 2003
 * @modification $Date: 2007-06-29 15:10:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

import java.io.File;

/**
 * @author deniger
 * @version $Id: DicoCasFileFormat.java,v 1.18 2007-06-29 15:10:27 deniger Exp $
 */
public abstract class DicoCasFileFormat extends DicoCasFileFormatAbstract {
  /**
   * @param _nom le nom du fichier dico lu ( telemac2d, ...�
   */
  public DicoCasFileFormat(final String _nom) {
    super(_nom);
  }

  /**
   * @return le manager des dictionnaires
   */
  public abstract DicoManager getDicoManager();

  @Override
  public String getLastVersion() {
    return getDicoManager().getLastVersion(this);
  }

  /**
   * @param _language le langage voulu
   * @return la version dans le langage demande
   * @see DicoLanguage
   */
  public DicoCasFileFormatVersion getLastVersionImpl(final int _language) {
    return getDicoManager().createLastVersionImpl(this, _language);
  }

  @Override
  public FileFormatVersionInterface getLastVersionInstance(File _f) {
    return getLastVersionImpl();
  }

  public FileFormatVersionInterface getLastVersionInstance() {
    return getLastVersionImpl();
  }

  /**
   * @return version
   */
  public DicoCasFileFormatVersion getLastVersionImpl() {
    return getDicoManager().createLastVersionImpl(this, DicoLanguage.getCurrentID());
  }

  /**
   *
   */
  @Override
  public int getVersionNb() {
    return getDicoManager().getVersionsNb(this);
  }

  /**
   *
   */
  @Override
  public String[] getVersions() {
    return getDicoManager().getVersions(this).toArray(new String[0]);
  }

  /**
   * @param _version
   * @return la version
   */
  public DicoCasFileFormatVersion getVersionImpl(final String _version) {
    return getDicoManager().createVersionImpl(this, _version);
  }

  /**
   * @param _version
   * @param _language
   * @return la version
   */
  public DicoCasFileFormatVersion getVersionImpl(final String _version, final int _language) {
    return getDicoManager().createVersionImpl(this, _version, _language);
  }
}
