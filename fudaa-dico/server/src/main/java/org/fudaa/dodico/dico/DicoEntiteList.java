/*
 * @creation 16 mai 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.fudaa.ctulu.CtuluLibString;

/**
 * @author deniger
 * @version $Id: DicoEntiteList.java,v 1.14 2006-09-19 14:42:27 deniger Exp $
 */
public class DicoEntiteList extends AbstractList {

  /**
   * @return la liste des rubriques contenues dans ce tableau d'entite.
   * @param _ents la liste des mot-cles a parcourir
   */
  public static String[] getRubriques(final DicoEntiteList _ents) {
    final int nb = _ents.size();
    final Set l = new HashSet(nb);
    for (int i = 0; i < nb - 1; i++) {
      l.add(_ents.getEntite(i).getRubrique());
    }
    final String[] r = CtuluLibString.enTableau(l);
    Arrays.sort(r);
    return r;
  }

  DicoEntite[] ents_;
  int size_;

  /**
   * @param _ents le modele source
   */
  public DicoEntiteList(final DicoModelAbstract _ents) {
    ents_ = _ents.createEntites();
    Arrays.sort(ents_);
    size_ = ents_.length;
  }

  @Override
  public void add(final int _index, final Object _element) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean add(final Object _o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(final Collection _c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(final int _index, final Collection _c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }

  /**
   * @param _d le mot-cle a tester
   * @return true si ce mot-cle est contenu dans cette liste
   */
  public boolean contains(final DicoEntite _d) {
    return indexOf(_d) >= 0;
  }

  @Override
  public boolean contains(final Object _o) {
    if (_o instanceof DicoEntite) {
      return contains((DicoEntite) _o);
    }
    return false;
  }

  @Override
  public boolean containsAll(final Collection _c) {
    return false;
  }

  /**
   * @param _d le nom a tester
   * @return true si contient un mot-cle de nom _d
   */
  public boolean containsNom(final String _d) {
    return indexOfNom(_d) >= 0;
  }

  /**
   *
   */
  @Override
  public Object get(final int _index) {
    return ents_[_index];
  }

  /**
   * @param _index l'index voulu
   * @return le mot-cle d'indice index
   */
  public DicoEntite getEntite(final int _index) {
    return ents_[_index];
  }

  /**
   * @param _nom le nom du mot-cle recherche
   * @return le mot-cle avec le bon nom ou null si non trouve
   */
  public DicoEntite getEntiteNom(final String _nom) {
    final int index = indexOfNom(_nom);
    return index >= 0 ? ents_[index] : null;
  }

  /**
   * @return toutes les rubriques utilisees par les mot-cles.
   */
  public String[] getRubriques() {
    return getRubriques(this);
  }

  /**
   * @param _d le mot-cle a tester
   * @return l'indice dans la liste ou -1 si non trouve.
   */
  public int indexOf(final DicoEntite _d) {
    if (_d == null) {
      return -1;
    }
    return indexOfNom(_d.getNom());
  }

  /**
   *
   */
  @Override
  public int indexOf(final Object _o) {
    return (_o instanceof DicoEntite) ? indexOf((DicoEntite) _o) : -1;
  }

  /**
   * @param _nom le nom a chercher
   * @return l'indice du mot-cle ayant le nom _nom. -1 si non trouve.
   */
  public int indexOfNom(final String _nom) {
    if (_nom == null) {
      return -1;
    }
    int lowIndex = 0;
    int highIndex = size_ - 1;
    int temp, tempMid;
    while (lowIndex <= highIndex) {
      tempMid = (lowIndex + highIndex) >> 1;
      temp = ents_[tempMid].getNom().compareTo(_nom);
      if (temp < 0) {
        lowIndex = tempMid + 1;
      } else if (temp > 0) {
        highIndex = tempMid - 1;
      } else {
        return tempMid;
      }
    }
    return -1;
  }

  /**
   * @return false
   */
  @Override
  public final boolean isEmpty() {
    return false;
  }

  @Override
  public int lastIndexOf(final Object _o) {
    return indexOf(_o);
  }

  /**
   * exception.
   */
  @Override
  public Object remove(final int _index) {
    throw new UnsupportedOperationException();
  }

  /**
   * exception.
   */
  @Override
  public boolean remove(final Object _o) {
    throw new UnsupportedOperationException();
  }

  /**
   * exception.
   */
  @Override
  public boolean removeAll(final Collection _c) {
    throw new UnsupportedOperationException();
  }

  /**
   * exception.
   */
  @Override
  public boolean retainAll(final Collection _c) {
    throw new UnsupportedOperationException();
  }

  /**
   * exception.
   */
  @Override
  public Object set(final int _index, final Object _element) {
    throw new UnsupportedOperationException();
  }

  /**
   * la taille.
   */
  @Override
  public final int size() {
    return size_;
  }

  /**
   * Copie dans un nouveau tableau.
   */
  @Override
  public Object[] toArray() {
    final DicoEntite[] r = new DicoEntite[size_];
    System.arraycopy(ents_, 0, r, 0, size_);
    return r;
  }

  @Override
  public Object[] toArray(final Object[] _a) {
    if (_a.length < size_) {
      return toArray();
    }
    System.arraycopy(ents_, 0, _a, 0, size_);
    return _a;
  }
}
