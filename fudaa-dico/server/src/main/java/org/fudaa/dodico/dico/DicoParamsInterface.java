/**
 * @creation 21 oct. 2003
 * @modification $Date: 2007-04-30 14:21:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author deniger
 * @version $Id: DicoParamsInterface.java,v 1.11 2007-04-30 14:21:38 deniger Exp $
 */
public interface DicoParamsInterface {
  
  
  DicoParamsChangeStateInterface createState();

  /**
   * @return le nombre de mot-cle "fichier"
   */
  int getEntiteFileNb();

  /**
   * Renvoie un tableau a deux lignes (ou null): premiere ligne represente les mot-cles a cacher et la deuxieme ligne
   * represente les mot-cles lies au mot-cle passe en param. Cela depend de la valeur donnee. Renvoie null le mot-cle
   * n'a pas de comport specifique. Return an array with two rows ( or null) : first row list the keywords to hide and
   * the second row list the keywords which depend on <code>_ent</code>. return null when the keyword has no specific
   * behavior. The result depends on the value <code>_value</code>.
   * 
   * @param _ent le mot-cle concerne
   * @param _value la valeur du mot-cle
   * @return null ou 2 tableaux tries dans l'ordre. null or 2 arrays in the natural order.
   */
  DicoEntite[][] getEntiteToHideToTie(DicoEntite _ent, String _value);

  /**
   * A partir des listes anciennes et nouvelles des mot-cle a cacher renvoie le tableau des mot-cle a cacher-montrer.
   * Return an hide-view array from to the old hide-keyword-list and the new one
   * 
   * @param _oldHide les anciens mot-cles caches
   * @param _newHide les nouveaux mot-cles a cacher
   * @return [mot-cle a cacher][mot-cle a montrer]
   */
  DicoEntite[][] getEntiteToHideChange(DicoEntite[] _oldHide, DicoEntite[] _newHide);

  /**
   * @return la liste des mot-cle visible selon les COMPORT
   */
  List getViewableEntite();

  /**
   * @return la version utilisee
   */
  DicoCasFileFormatVersionAbstract getDicoFileFormatVersion();

  /**
   * @return le format utilise
   */
  DicoCasFileFormat getDicoFileFormat();

  /**
   * @return le modele contenant tous les mot-cles
   */
  DicoModelAbstract getDico();

  /**
   * @param _ent le mot-cle a verifier
   * @return true si ce mot-cle est supporte (contenu) par le modele
   */
  boolean contains(DicoEntite _ent);

  /**
   * @return la liste des mot-cle valide
   */
  Set getEntiteFileSetList();

  /**
   * @return true si valide
   */
  boolean isDataValide();

  /**
   * @param _e le mot-cle pour lequel on demande le commentaire
   * @return le commentaire associe ou null si aucun
   */
  String getComment(DicoEntite _e);

  /**
   * @param _ent le mot-cle a verifer
   * @return true si ce mot-cle peut etre modifie
   */
  String canUpdate(DicoEntite _ent);

  /**
   * @param _ent le mot-cle demande
   * @return la valeur associe (valeur par defaut si non modifie)
   */
  String getValue(DicoEntite _ent);

  /**
   * @return le nombre de valeurs specifiee
   */
  int getValuesSize();

  /**
   * @param _ent le mot-cle a verifier
   * @return true si la valeur de ce mot-cle est modifiee: soit differente de la valeur par defaut
   */
  boolean isValueSetFor(DicoEntite _ent);

  /**
   * @param _ent le mot-cle a verifier
   * @return true si la valeur est valide pour _ent
   */
  boolean isValueValideFor(DicoEntite _ent);

  /**
   * @return tous les mot-cles modifies
   */
  Set getAllEntiteWithValuesSet();

  /**
   * @param _m la table qui sera remplie avec toutes les valeurs modifiees (DicoEntite->String)
   */
  void getEntiteValues(Map _m);

  /**
   * @return la table avec toutes les valeurs modifiees (DicoEntite->String)
   */
  Map getEntiteValues();

  /**
   * @return le titre de l'etude (issu d'un mot-cle)
   */
  String getTitle();

  /**
   * @param _ent le mot-cle a tester
   * @return true si un comport a ete defini pour ce mot-cle
   */
  boolean isEntiteWithComportBehavior(DicoEntite _ent);

  /**
   * @param _l le listener a ajouter
   */
  void addModelListener(DicoParamsListener _l);

  /**
   * @param _ent le mot-cle demande
   * @return le message d'erreur ou null si aucun
   */
  String getInvalidMessage(DicoEntite _ent);
}