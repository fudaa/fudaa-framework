/**
 * @creation 11 avr. 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Une classe abstraite pour toutes les classes generees pour les fichiers dico. s
 *
 * @author deniger
 * @version $Id: DicoModelAbstract.java,v 1.17 2006-09-19 14:42:27 deniger Exp $
 */
public abstract class DicoModelAbstract implements Comparable {

  private DicoEntiteList entites_;

  protected Map entiteComportValues_;

  protected int languageIndex_;

  /**
   * Initialise l'index du langage.
   */
  public DicoModelAbstract() {
    languageIndex_ = DicoLanguage.getCurrentID();
  }

  /**
   * @param _language le langage voulu
   */
  public DicoModelAbstract(final int _language) {
    languageIndex_ = DicoLanguage.getNormalizeLanguage(_language);
  }


  /**
   * @param _language l'identifiant du langage
   * @see DicoLanguage
   */
  public DicoModelAbstract(final String _language) {
    this(DicoLanguage.getIdFromLocalID(_language));
  }

  /**
   * Initialise les tables.
   */
  private void initEntite() {
    if (entites_ == null) {
      if (entiteComportValues_ == null) {
        entiteComportValues_ = new HashMap();
      }
      entites_ = new DicoEntiteList(this);
      if (entiteComportValues_.size() == 0) {
        entiteComportValues_ = null;
      }
    }
  }

  protected abstract DicoEntite[] createEntites();

  protected abstract String[] createNoms();

  @Override
  public int compareTo(final Object _obj) {
    if (_obj == this) {
      return 0;
    }
    if (!(_obj instanceof DicoModelAbstract)) {
      throw new IllegalArgumentException("arg is not a DicoModelAbstract");
    }
    final DicoModelAbstract m = (DicoModelAbstract) _obj;
    final int i = getCodeName().compareTo(m.getCodeName());
    return i == 0 ? getVersion().compareTo(m.getVersion()) : i;
  }

  /**
   * @param _ent le mot-cle a tester
   * @return true si contenu dans ce modele
   */
  public boolean contains(final DicoEntite _ent) {
    if (entites_ == null) {
      initEntite();
    }
    return entites_.contains(_ent);
  }

  /**
   * @param _obj le modele a tester
   * @return true si meme code, meme version et meme langage.
   */
  public boolean equalsModel(final DicoModelAbstract _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj == null) {
      return false;
    }
    return (_obj.getCodeName().equals(getCodeName())) && (_obj.getVersion().equals(getVersion()))
            && _obj.getLanguageIndex() == getLanguageIndex();
  }

  /**
   * @return true si meme code, meme version et meme langage.
   */
  public boolean equals(final Object _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj instanceof DicoModelAbstract) {
      return equalsModel((DicoModelAbstract) _obj);
    }
    return false;
  }

  /**
   * @return le nom du code correspondant
   */
  public abstract String getCodeName();

  /**
   * Renvoie l'entite correspondante.
   *
   * @return null si non trouvee.
   * @param _nom le nom a chercher
   */
  public final synchronized DicoEntite getEntite(final String _nom) {
    if (entites_ == null) {
      initEntite();
    }
    return entites_.getEntiteNom(_nom);
  }

  /**
   * @return la table pour les comport
   */
  public Map getEntiteComportValues() {
    return entiteComportValues_;
  }

  /**
   * Initialise la liste si necessaire.
   *
   * @return le nombre de mot-cles geres.
   */
  public int getEntiteNombre() {
    if (entites_ == null) {
      initEntite();
    }
    return entites_.size();
  }

  /**
   * Initialise la liste si necessaire.
   *
   * @return la liste des mot-cles.
   */
  public DicoEntiteList getEntites() {
    if (entites_ == null) {
      initEntite();
    }
    return entites_;
  }

  /**
   * @return une liste contenant tous les mot-cles
   */
  public List getEntitesInNewList() {
    return new ArrayList(getEntites());
  }

  /**
   * @param _entKeys table dont les cles sont des mot-cles
   * @return la liste des mot-cles contenus dans cette table qui caracterise des fichiers.
   */
  public Set getFichierEntitesFor(final Map _entKeys) {
    if ((_entKeys == null) || (_entKeys.size() == 0)) {
      return null;
    }
    final Set r = new HashSet(_entKeys.size() / 2);
    DicoEntite ent;
    for (final Iterator it = _entKeys.keySet().iterator(); it.hasNext();) {
      ent = (DicoEntite) it.next();
      if (ent.getType().isFileType()) {
        r.add(ent);
      }
    }
    return r;
  }

  /**
   * @param _entKeys la table avec des mot-cles en tant que cle
   * @return la list des mot-cle caracterisant des fichiers.
   */
  public List getFichierEntitesListFor(final Map _entKeys) {
    if ((_entKeys == null) || (_entKeys.size() == 0)) {
      return new ArrayList();
    }
    final List r = new ArrayList(_entKeys.size() / 2);
    DicoEntite ent;
    for (final Iterator it = _entKeys.keySet().iterator(); it.hasNext();) {
      ent = (DicoEntite) it.next();
      if (ent.getType().isFileType()) {
        r.add(ent);
      }
    }
    return r;
  }

  /**
   * @return le langage utilise (en ou fr).
   * @see DicoLanguage
   */
  public final String getLanguage() {
    return DicoLanguage.LOCAL_STRING_ID[languageIndex_];
  }

  /**
   * @return l'indice du language utilise
   * @see DicoLanguage
   */
  public final int getLanguageIndex() {
    return languageIndex_;
  }

  public final int getOtherLanguageIndex() {
    return languageIndex_ == DicoLanguage.FRENCH_ID ? DicoLanguage.ENGLISH_ID : DicoLanguage.FRENCH_ID;
  }

  /**
   * @return la liste (non modifiable) des mot-cles
   */
  public DicoEntiteList getList() {
    return getEntites();
  }

  /**
   * @return la liste des rubriques
   */
  public String[] getRubriques() {
    if (entites_ == null) {
      initEntite();
    }
    return entites_.getRubriques();
  }

  /**
   * @return la version utilisee
   */
  public abstract String getVersion();

  /**
   *
   */
  public int hashCode() {
    return (getCodeName().hashCode() | getVersion().hashCode()) + languageIndex_;
  }

  /**
   * @param _ent le mot-cle a tester
   * @return true si le mot-cle est contenu dans la liste de ce modele
   */
  public boolean isKey(final DicoEntite _ent) {
    return isKey(_ent.getNom());
  }

  /**
   * @param _nom le nom a tester
   * @return true si ce nom correspond a un mot-cle
   */
  public boolean isKey(final String _nom) {
    if (entites_ == null) {
      initEntite();
    }
    return entites_.containsNom(_nom);
  }

  /**
   *
   */
  public String toString() {
    return getCodeName() + CtuluLibString.ESPACE + getVersion() + CtuluLibString.ESPACE
            + DicoLanguage.getLocalID(languageIndex_);
  }
}
