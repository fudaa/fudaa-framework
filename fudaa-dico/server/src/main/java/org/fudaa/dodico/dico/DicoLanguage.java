/*
 * @creation 19 mai 2003
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.Locale;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.commun.DodicoPreferences;

/**
 * @author deniger
 * @version $Id: DicoLanguage.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
 */
public final class DicoLanguage {

  /**
   * Un modele pour representer les langues sous forme de combobox.
   *
   * @author Fred Deniger
   * @version $Id: DicoLanguage.java,v 1.13 2006-09-19 14:42:27 deniger Exp $
   */
  public static class LanguageComboBoxModel extends AbstractListModel implements ComboBoxModel {

    private int selectedLanguage_;

    /**
     * selectionne le langage par defaut.
     */
    public LanguageComboBoxModel() {
      selectedLanguage_ = DicoLanguage.getCurrentID();
    }

    public void setSelectedLanguageFromLocal(final String _localId) {
      setSelectedItem(getNameFromLocalID(_localId));
    }

    /**
     * le nom a l'indice _idx.
     */
    @Override
    public Object getElementAt(final int _idx) {
      return NAMES[_idx];
    }

    /**
     * la chaine de la langue selectionnee.
     */
    @Override
    public Object getSelectedItem() {
      return getName(selectedLanguage_);
    }

    /**
     * @return l'indice de la langue selectionnee
     */
    public int getSelectedLanguage() {
      return selectedLanguage_;
    }

    @Override
    public int getSize() {
      return LOCAL_STRING_ID.length;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      final int oldLanguage = selectedLanguage_;
      selectedLanguage_ = DicoLanguage.getIdFromName((String) _anItem);
      if (oldLanguage != selectedLanguage_) {
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  final static String[] LOCAL_STRING_ID = new String[] { "fr", "en" };
  final static String[] NAMES = new String[] { DodicoLib.getS("Fran�ais"), DodicoLib.getS("Anglais") };
  /**
   * l'indice pour l'anglais.
   */
  public final static int ENGLISH_ID = 1;

  /**
   * l'indice pour le francais.
   */
  public final static int FRENCH_ID = 0;

  /**
   * @return l'indice en fonction des proprietes locales.
   */
  public static int getCurrentID() {
    return "fr".equals(DodicoPreferences.DODICO.getStringProperty("dico.default.language", Locale.getDefault()
        .getLanguage())) ? FRENCH_ID : ENGLISH_ID;
  }

  /**
   * @param _id la chaine a tester
   * @return FRENCH_ID si id est egale a l'identifiant francais. Sinon ENGLISH_ID est renvoye
   */
  public static int getIdFromLocalID(final String _id) {
    if (LOCAL_STRING_ID[FRENCH_ID].equals(_id)) {
      return FRENCH_ID;
    }
    return ENGLISH_ID;
  }

  /**
   * @param _id en ou fr
   * @return le nom long
   */
  public static String getNameFromLocalID(final String _id) {
    if (LOCAL_STRING_ID[FRENCH_ID].equals(_id)) {
      return NAMES[FRENCH_ID];
    }
    return NAMES[ENGLISH_ID];
  }

  /**
   * @param _languageName le nom a tester
   * @return FRENCH_ID si _id est egale a francais et sinon renvoie ENGLISH_ID.
   */
  public static int getIdFromName(final String _languageName) {
    if (NAMES[FRENCH_ID].equals(_languageName)) {
      return FRENCH_ID;
    }
    return ENGLISH_ID;
  }

  /**
   * @param _id l'indice de la langue
   * @return l'identifiant correspondant
   */
  public static String getLocalID(final int _id) {
    return LOCAL_STRING_ID[_id == FRENCH_ID ? FRENCH_ID : ENGLISH_ID];
  }

  /**
   * @param _id l'id de la langue
   * @return le nom correspondant
   */
  public static String getName(final int _id) {
    return NAMES[_id == FRENCH_ID ? FRENCH_ID : ENGLISH_ID];
  }

  /**
   * Pour etre sur d'avoir un identifiant correct..
   *
   * @param _id l'id a tester
   * @return renvoie FRENCH_ID si egale a FRENCH_ID. ENGLISH_ID sinon.
   */
  public static int getNormalizeLanguage(final int _id) {
    return _id == FRENCH_ID ? FRENCH_ID : ENGLISH_ID;
  }

  /**
   * @param _s le nouvelle valeur par defaut.
   */
  public static void setDefaultDicoLanguage(final String _s) {
    DodicoPreferences.DODICO.putStringProperty("dico.default.language", _s);
  }

  private DicoLanguage() {

  }
}
