/*
 * @creation 27 avr. 07
 * @modification $Date: 2007-05-04 13:45:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author fred deniger
 * @version $Id: DicoParamsChangeState.java,v 1.2 2007-05-04 13:45:07 deniger Exp $
 */
public class DicoParamsChangeState implements DicoParamsListener, DicoParamsChangeStateInterface {

  private final DicoParams dico_;

  private Map loadedEntiteState_;
  boolean paramsModified_;

  Set stateListener_;

  public DicoParamsChangeState(final DicoParams _dico) {
    super();
    dico_ = _dico;
    dico_.addModelListener(this);

  }

  protected void fireDicoParamsStateChanged() {
    fireDicoParamsStateChanged(true);
  }

  protected void fireDicoParamsStateChanged(final boolean _newVal) {
    paramsModified_ = _newVal;
    if (stateListener_ != null) {
      synchronized (stateListener_) {
        for (final Iterator it = stateListener_.iterator(); it.hasNext();) {
          ((DicoParamsChangeStateListener) it.next()).paramsModified(this);
        }
      }
    }
  }

  protected void fireProjectEntiteFileStateChanged(final DicoEntite _e) {
    if (stateListener_ != null) {
      synchronized (stateListener_) {
        for (final Iterator it = stateListener_.iterator(); it.hasNext();) {
          ((DicoParamsChangeStateListener) it.next()).paramsStateLoadedEntiteChanged(dico_, _e);
        }
      }
    }
  }

  @Override
  public void addChangeStateListener(final DicoParamsChangeStateListener _l) {
    if (stateListener_ == null) {
      stateListener_ = new HashSet();
    }
    stateListener_.add(_l);
  }

  /**
   * @return les donnees fichiers qui n'ont pas de valeurs associees
   */
  @Override
  public DicoEntite[] getNewFileEntiteToSave() {
    if (loadedEntiteState_ == null) {
      return null;
    }
    final List l = new ArrayList(loadedEntiteState_.size());
    for (final Iterator it = loadedEntiteState_.keySet().iterator(); it.hasNext();) {
      final DicoEntite ent = (DicoEntite) it.next();
      if (!dico_.isValueSetFor(ent)) {
        l.add(ent);
      }
    }
    return DicoParams.enTableau(l);
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    fireDicoParamsStateChanged();

  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {
    fireDicoParamsStateChanged();

  }

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    fireDicoParamsStateChanged();
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    fireDicoParamsStateChanged();
  }

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {
    fireDicoParamsStateChanged();
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
    fireDicoParamsStateChanged();

  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
    fireDicoParamsStateChanged();
  }

  /**
   * @return un iterateur sur le tableau des "donnees fichiers" (DicoEntiteFileState). null si aucun.
   */
  @Override
  public Iterator getEntiteLoadedEnum() {
    return loadedEntiteState_ == null ? null : loadedEntiteState_.entrySet().iterator();
  }

  /**
   * @return le nombre de donnees fichiers
   */
  @Override
  public int getEntiteLoadedNb() {
    return loadedEntiteState_ == null ? 0 : loadedEntiteState_.size();
  }

  /**
   * @param _ent le mot-cle demande
   * @return les donnees concernant le fichier associe au mot-cle. null si aucune donnee issu d'un fichier
   */
  public DicoEntiteFileState getFileState(final DicoEntite _ent) {
    return loadedEntiteState_ == null ? null : (DicoEntiteFileState) loadedEntiteState_.get(_ent);
  }

  /**
   * @return true si un mot-cle fichier est modifie ou n'est pas initialise
   */
  @Override
  public boolean isEntiteFilesModified() {
    if (loadedEntiteState_ == null) {
      return false;
    }
    for (final Iterator it = loadedEntiteState_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry m = (Map.Entry) it.next();
      if ((!dico_.isValueSetFor((DicoEntite) m.getKey())) || (((DicoEntiteFileState) m.getValue()).isModified())) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _e le mot-cle
   * @return true si le fichier associe a ete defini comme charge
   */
  @Override
  public boolean isLoaded(final DicoEntite _e) {
    return loadedEntiteState_ == null ? false : loadedEntiteState_.containsKey(_e);
  }

  /**
   * @param _e le mot-cle
   * @return true si le fichier associe a ete defini comme charge et modifie
   */
  @Override
  public boolean isLoadedAndModified(final DicoEntite _e) {
    if (loadedEntiteState_ != null) {
      final DicoEntiteFileState s = ((DicoEntiteFileState) loadedEntiteState_.get(_e));
      if (s != null) {
        return s.isModified();
      }
    }
    return false;
  }

  @Override
  public boolean isModified() {
    return isParamsModified() || isEntiteFilesModified();
  }

  @Override
  public boolean isParamsModified() {
    return paramsModified_;
  }

  public void removeChangeStateListener(final DicoParamsChangeStateListener _l) {
    if (stateListener_ != null) {
      stateListener_.remove(_l);
    }
  }

  /**
   * Enregistre que le mot-cle _e est charge a partir du fichier _f et le rend modifie si _m.
   * 
   * @param _e le mot-cle charge
   * @param _f le fichier a partir duquel le mot-cl� a ete charge
   * @param _m mot-cle modifie ou non
   * @return true si modif
   */
  @Override
  public boolean setLoaded(final DicoEntite _e, final File _f, final boolean _m) {
    //Fred
    if (_e == null) {
      return false;
    }
    boolean r = false;
    if (loadedEntiteState_ == null) {
      loadedEntiteState_ = new HashMap(10);
      loadedEntiteState_.put(_e, new DicoEntiteFileState(_f, _m));
      r = true;
    } else {
      final DicoEntiteFileState fs = getFileState(_e);
      if (fs == null) {
        loadedEntiteState_.put(_e, new DicoEntiteFileState(_f, _m));
        r = true;
      } else {
        r = fs.loadFile(_f);
        r |= fs.setModified(_m);
      }
    }
    if (r) {
      fireProjectEntiteFileStateChanged(_e);
    }
    return r;
  }

  /**
   * Enregistre que le mot-cle _e est charge a partir du fichier _f et le rend modifie si _m.
   * 
   * @param _e le mot-cle charge
   * @param _f le fichier a partir duquel le mot-cl� a ete charge
   * @param _m mot-cle modifie ou non
   * @return true si modif
   * @param _isProjectFile true si c'est le mot-cle decrivant le fichier principal
   */
  @Override
  public boolean setLoaded(final DicoEntite _e, final File _f, final boolean _m, final boolean _isProjectFile) {
    if (_e == null) {
      return false;
    }
    boolean r = false;
    if (loadedEntiteState_ == null) {
      loadedEntiteState_ = new HashMap(10);
      loadedEntiteState_.put(_e, new DicoEntiteFileState(_f, _m));
      r = true;
    } else {
      final DicoEntiteFileState fs = getFileState(_e);
      if (fs == null) {
        loadedEntiteState_.put(_e, new DicoEntiteFileState(_f, _m));
        r = true;
      } else {
        r = fs.loadFile(_f);
        r |= fs.setModified(_m);
        fs.setProjectFile(_isProjectFile);
      }
    }
    if (r) {
      fireProjectEntiteFileStateChanged(_e);
    }
    return r;
  }

  /**
   * @param _e le mot-cle fichier
   * @param _isModified true si modifie
   */
  @Override
  public void setLoadedModified(final DicoEntite _e, final boolean _isModified) {
    final DicoEntiteFileState fs = getFileState(_e);
    if (fs == null || (!fs.isModified() == !_isModified)) {
      if (_isModified) {
        setLoaded(_e, null, _isModified);
        fireProjectEntiteFileStateChanged(_e);
      }
    } else {
      fs.setModified(_isModified);
      fireProjectEntiteFileStateChanged(_e);
    }

  }

  @Override
  public void setParamsModified(final boolean _paramsModified) {
    if (paramsModified_ != _paramsModified) {
      fireDicoParamsStateChanged(_paramsModified);
    }
  }

  /**
   * Enleve le mot-cl� _e des mot-cl�s charg�s.
   * 
   * @param _e le mot-cle a modifier
   */
  @Override
  public void setUnloaded(final DicoEntite _e) {
    if (loadedEntiteState_ != null) {
      loadedEntiteState_.remove(_e);
    }
  }

}
