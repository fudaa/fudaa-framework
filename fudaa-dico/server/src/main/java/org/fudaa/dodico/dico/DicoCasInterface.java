/**
 * @creation 15 avr. 2003
 * @modification $Date: 2006-04-07 09:23:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dico;

import java.util.Map;
import java.util.Set;
/**
 * @author deniger
 * @version $Id: DicoCasInterface.java,v 1.9 2006-04-07 09:23:04 deniger Exp $
 */
public interface DicoCasInterface {

  /**
   * @return le nombre d'entree du fichier
   */
  int getNbInput();

  /**
   * @param _ent le mot-cle pour lequel on veut connaitre la valeur
   * @return la valeur (String) correspondante a chaque entite.
   */
  String getValue(DicoEntite _ent);

  /**
   * @return Le commentaire pour le mot-cle _e
   * @param _e le mot-cle concerne
   */
  String getCommentaire(DicoEntite _e);

  /**
   * @return les entites sous forme de set.
   */
  Set getEntiteSet();

  /**
   * @return les donnees DicoEntite->String
   */
  Map getInputs();

  /**
   * @return les donnees DicoEntite->String (commentaires)
   */
  Map getComments();

  /**
   * @param _e le mot-cle a chercher
   * @return true si contient le mot-cle _e
   */
  boolean containsEntite(DicoEntite _e);

  /**
   * @return true si le code de calcul telemac doit ecrire les cles rencontrees.
   */
  boolean printKeys();

  /**
   * @return true si le code de calcul telemac doit ecrire les cles rencontrees sous forme longue.
   */
  boolean printLongKeys();

  /**
   * @return true si le code de calcul telemac doit ecrire les cles et les valeurs rencontrees.
   */
  boolean printKeysAndValues();

  /**
   * @return true si le commande stop a ete rencontree (utile?).
   */
  boolean commandStop();

  /**
   * @return true si le commande stopper le programme a ete rencontree (utile?).
   */
  boolean commandStopProgram();
}