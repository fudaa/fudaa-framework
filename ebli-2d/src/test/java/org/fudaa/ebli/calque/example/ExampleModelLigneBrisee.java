/*
 *  @creation     31 mars 2005
 *  @modification $Date: 2006-11-15 13:54:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.example;

import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeEditable;
import org.fudaa.ebli.geometrie.GrPolygone;


/**
 * @author Fred Deniger
 * @version $Id: TestModelLigneBrisee.java,v 1.3 2006-11-15 13:54:28 deniger Exp $
 */
public class ExampleModelLigneBrisee extends ZModeleLigneBriseeEditable {
  
  public static ExampleModelLigneBrisee buildDefault(){
    final ExampleModelLigneBrisee model=new ExampleModelLigneBrisee();
    GrPolygone p=new GrPolygone();
    p.sommets_.ajoute(1, 1, 3);
    p.sommets_.ajoute(3, 4, 1);
    p.sommets_.ajoute(3, 0, 1);
    model.addGeometry(p, null,null,null);
    p=new GrPolygone();
    p.sommets_.ajoute(8, 5, 3);
    p.sommets_.ajoute(8, 10, 1);
    p.sommets_.ajoute(15, 6, 1);
    model.addGeometry(p, null,null,null);
    return model;
  }
  
  public ExampleModelLigneBrisee() {
    super();
  }
  
  
  @Override
  protected boolean isIntersectedLigneBriseeAccepted(){
    return false;
  }
    
  

}
