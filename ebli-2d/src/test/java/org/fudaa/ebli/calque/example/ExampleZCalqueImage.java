/**
 * @file TestZCalquePoint.java
 * @creation 2002-08-27
 * @modification $Date: 2006-12-20 16:08:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.calque.example;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuSeparator;
import com.memoire.bu.BuSplit2Pane;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.geom.Point2D;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalqueContextuelListener;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueImageRaster;
import org.fudaa.ebli.calque.ZCalqueRepereInteraction;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.ebli.calque.ZModeleImageRaster;
import org.fudaa.ebli.calque.ZModeleStatiqueImageRaster;
import org.fudaa.ebli.repere.BControleNavigation;

/**
 * @version $Id: TestZCalqueImage.java,v 1.7 2006-12-20 16:08:24 deniger Exp $
 * @author Fred Deniger
 */
public class ExampleZCalqueImage extends ZEbliFilleCalques implements BCalqueContextuelListener {

  // private static String FICHIER_SEMIS = "semis.txt";
  private ZCalqueImageRaster cqZPoint_;
  ZCalqueRepereInteraction s_;

  public ExampleZCalqueImage(final File _i) {
    super(new CtuluUIDefault(), null);
    final BGroupeCalque gc = new BGroupeCalque();
    gc.setTitle("Mailleur");
    setName("MAILLEUR");
    setTitle("ModeleVSOLD");
    cqZPoint_ = new ZCalqueImageRaster();
    final ZModeleImageRaster model = new ZModeleStatiqueImageRaster(new CtuluImageContainer(_i));
    // Point2D.Double[] reel = new Point2D.Double[4];
    final Point2D.Double[] img = new Point2D.Double[4];
    int idx = 0;
    /*
     * reel[idx++] = new Point2D.Double(790816, 104000); reel[idx++] = new Point2D.Double(791968, 104000); reel[idx++] =
     * new Point2D.Double(790816, 103100); reel[idx++] = new Point2D.Double(791968, 103100);
     */
    idx = 0;
    img[idx++] = new Point2D.Double(0, 0);
    img[idx++] = new Point2D.Double(1152, 0);
    img[idx++] = new Point2D.Double(0, 900);
    img[idx++] = new Point2D.Double(1152, 900);
    // model.setProj(img, reel);
    cqZPoint_.setModele(model);
    cqZPoint_.setName("cqPOINTMAILLEUR");
    cqZPoint_.setTitle("pointsModele");
    gc.add(cqZPoint_);
    setCalque(gc);
    pn_.getVueCalque().setListener(this);
    getVueCalque().setBackground(Color.white);
    getVueCalque().setPreferredSize(new Dimension(500, 400));
    this.pack();
    restaurer();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("BUILD_POLY".equals(_e.getActionCommand())) {
      pn_.setCalqueInteractionActif(s_);
    } else {
      super.actionPerformed(_e);
    }
  }

  @Override
  public JPopupMenu getCmdsContextuelles() {
    final JPopupMenu r = new JPopupMenu();
    final JMenuItem m = new JMenuItem();
    m.setText("Construire polyligne");
    m.addActionListener(this);
    m.setActionCommand("BUILD_POLY");
    r.add(m);
    r.add(new BuSeparator());

    pn_.fillMenuWithToolsActions(r);
    return r;
  }

  @Override
  public void restaurer() {
    super.restaurer();
  }

  /*
   * public void selectedObjects(SelectionEvent _evt) { VecteurGrContour objets=_evt.getObjects();
   * System.out.println(objets.nombre()); for(int i=0;i <objets.nombre();i++) { GrPoint[]
   * contours=objets.renvoie(i).contour(); for(int j=0;j <contours.length;j++) System.out.println(contours[j]); } }
   */
  public static void main(final String[] _args) {
    // BVueCalque vue=new BVueCalque();
    if (_args.length < 1) {
      return;
    }
    final String file = _args[0];
    final File f = new File(file).getAbsoluteFile();

    if (!f.exists()) {
      return;
    }
    System.out.println("chargement de" + f.getAbsolutePath());
    final ExampleZCalqueImage testCalque = new ExampleZCalqueImage(f);
    final BArbreCalque arbre = new BArbreCalque();
    arbre.setModel(testCalque.getArbreCalqueModel());
    testCalque.setVisible(true);
    testCalque.setSize(new Dimension(450, 400));
    final JFrame frame = new JFrame(testCalque.getTitle());
    final BuPanel princ = new BuPanel();
    princ.setLayout(new BuBorderLayout());
    frame.setContentPane(princ);
    final BuDesktop desk = new BuDesktop();
    desk.setPreferredSize(new Dimension(500, 500));
    arbre.setPreferredSize(new Dimension(200, 500));
    desk.add(testCalque);
    final BuSplit2Pane split = new BuSplit2Pane(desk, arbre);
    princ.add(split, BuBorderLayout.CENTER);
    frame.setLocation(50, 50);
    frame.setSize(new Dimension(700, 500));
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    testCalque.restaurer();
    testCalque.setLocation(200, 200);
    final BControleNavigation bn = new BControleNavigation(testCalque.getVueCalque());
    bn.setVisible(true);
    bn.setAffichage("bouton");
    bn.setSize(new Dimension(150, 150));
    desk.add(bn);
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    /*
     * testCalque.calculMaillage(); testCalque.getVueCalque().repaint(0);
     */
  }
}