/**
 * @modification $Date: 2006-09-22 15:47:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque.example;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuSplit2Pane;
import java.awt.Color;
import java.awt.Dimension;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.EbliFilleCalques;
import org.fudaa.ebli.calque.SelectionEvent;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZModeleStatiquePoint;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
public class ExampleModeleMaillage extends EbliFilleCalques {
  private static final String FICHIER_SEMIS= "semis.txt";
  private VecteurGrPoint semis_;
  private ZCalquePoint cqZPoint_;
  public ExampleModeleMaillage() {
    setSelectionVisible(true);
    final BGroupeCalque gc= new BGroupeCalque();
    gc.setTitle("Mailleur");
    setName("MAILLEUR");
    setTitle("Modele");
    cqZPoint_= new ZCalquePoint();
    cqZPoint_.setName("cqPOINTMAILLEUR");
    cqZPoint_.setTitle("points");
    gc.add(cqZPoint_);
    setCalque(gc);
    getVueCalque().setBackground(Color.white);
    setSelectionVisible(true);
    getVueCalque().setPreferredSize(new Dimension(500, 400));
    //    this.getContentPane().add(getVueCalque());
    this.pack();
    restaurer();
  }
  @Override
  public void restaurer() {
    super.restaurer();
  }
  public synchronized void importerSemis() {
    double minX= Double.POSITIVE_INFINITY;
    double minY= Double.POSITIVE_INFINITY;
    int p= 0;
    try {
      semis_= new VecteurGrPoint();
      final LineNumberReader in=
        new LineNumberReader(
          new FileReader(getClass().getResource(FICHIER_SEMIS).getPath()));
      String line= in.readLine();
      StringTokenizer tok= null;
      while (line != null) {
        line= line.trim();
        if (line.length() == 0) {
          continue;
        } else if (line.startsWith("#")) {
          line= in.readLine();
          continue;
        }
        tok= new StringTokenizer(line);
        System.out.println(line);
        final double x= Double.parseDouble(tok.nextToken());
        final double y= Double.parseDouble(tok.nextToken());
        final double z= Double.parseDouble(tok.nextToken());
        if (x < minX) {
          minX= x;
        }
        if (y < minY) {
          minY= y;
        }
        semis_.ajoute(new GrPoint(x, y, z));
        p += line.length();
        line= in.readLine();
      }
      in.close();
    } catch (final IOException e) {
      semis_= null;
      return;
    }
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    final int siz= semis_.nombre();
    for (int i= 0; i < siz; i++) {
      final GrPoint po= semis_.renvoie(i);
      po.x_ -= minX;
      po.y_ -= minY;
    }
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    System.out.println("Fin lecture Fichier");
  }
  public void afficheSemis() {
    importerSemis();
    cqZPoint_.setModele(new ZModeleStatiquePoint(semis_.tableau()));
  }
  @Override
  public void selectedObjects(final SelectionEvent _evt) {
    final VecteurGrContour objets= _evt.getObjects();
    System.out.println(objets.nombre());
    for (int i= 0; i < objets.nombre(); i++) {
      final GrPoint[] contours= objets.renvoie(i).contour();
      for (int j= 0; j < contours.length; j++) {
        System.out.println(contours[j]);
      }
    }
  }
  public static void main(final String[] _args) {
    ExampleModeleMaillage testCalque= null;
    testCalque= new ExampleModeleMaillage();
    final BArbreCalque arbre= new BArbreCalque(testCalque.getArbreCalqueModel());
    final JFrame frame= new JFrame(testCalque.getTitle());
    final BuPanel princ= new BuPanel();
    princ.setLayout(new BuBorderLayout());
    frame.setContentPane(princ);
    final BuDesktop desk= new BuDesktop();
    desk.setPreferredSize(new Dimension(300, 300));
    arbre.setPreferredSize(new Dimension(50, 300));
    desk.add(testCalque);
    final BuSplit2Pane split= new BuSplit2Pane(desk, arbre);
    princ.add(split, BuBorderLayout.CENTER);
    frame.setLocation(50, 50);
    testCalque.afficheSemis();
    testCalque.setVisible(true);
    frame.setSize(new Dimension(750, 500));
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    testCalque.restaurer();
    split.updateDisplay();
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    /* testCalque.calculMaillage();
    testCalque.getVueCalque().repaint(0); */
  }
}
