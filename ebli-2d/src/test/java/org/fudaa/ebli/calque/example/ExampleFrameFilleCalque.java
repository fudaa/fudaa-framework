/*
 * @file         TestZCalquePoint.java
 * @creation     2002-08-27
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.example;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuSplit2Pane;
import java.awt.Dimension;
import javax.swing.JFrame;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.EbliFilleCalques;
/**
 * Un test pour le calque ZCalquePoint
 *
 * @version      $Id: TestFrameFilleCalque.java,v 1.2 2006-10-19 14:13:24 deniger Exp $
 * @author       Fred Deniger
 */
public class ExampleFrameFilleCalque extends JFrame {
  EbliFilleCalques calquePrincipal_;
  public ExampleFrameFilleCalque(final EbliFilleCalques _calque, final BArbreCalque arbre_) {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setTitle("Essai");
    calquePrincipal_= _calque;
    final BuPanel princ= new BuPanel();
    princ.setLayout(new BuBorderLayout());
    setContentPane(princ);
    final BuDesktop desk= new BuDesktop();
    desk.setPreferredSize(new Dimension(500, 500));
    arbre_.setPreferredSize(new Dimension(200, 500));
    desk.add(calquePrincipal_);
    final BuSplit2Pane split= new BuSplit2Pane(desk, arbre_);
    princ.add(split, BuBorderLayout.CENTER);
    setLocation(50, 50);
    setSize(new Dimension(700, 500));
  }
  public void restaurer() {
    calquePrincipal_.restaurer();
  }
}
