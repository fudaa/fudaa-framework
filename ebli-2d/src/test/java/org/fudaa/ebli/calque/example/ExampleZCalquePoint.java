/**
 * @file TestZCalquePoint.java
 * @creation 2002-08-27
 * @modification $Date: 2006-11-14 09:06:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.calque.example;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuSeparator;
import com.memoire.bu.BuSplit2Pane;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueContextuelListener;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZEbliFilleCalques;
import org.fudaa.ebli.calque.action.CalqueGISEditionAction;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.repere.ZTransformationDomaine;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 * Un test pour le calque ZCalquePoint.
 *
 * @version $Id: TestZCalquePoint.java,v 1.8 2006-11-14 09:06:26 deniger Exp $
 * @author Fred Deniger
 */
public class ExampleZCalquePoint extends ZEbliFilleCalques implements BCalqueContextuelListener {

  private final static String FICHIERSEMIS = "semis.txt";
  private ZCalquePoint cqZPoint_;

  private static class PanelEssai extends ZEbliCalquesPanel {

    /**
     * @param _vc la vue calque
     */
    public PanelEssai(final BCalque _vc) {
      super(_vc, new CtuluUIDefault());

    }

    @Override
    public EbliActionInterface[] getApplicationActions() {
      final ZEditorDefault editor = new ZEditorDefault(this);
      editor.setUi(getController().getUI());

      return new EbliActionInterface[] { new CalqueGISEditionAction(getArbreCalqueModel().getTreeSelectionModel(),
          editor,editor.getSupport()) };
    }
  }

  public ExampleZCalquePoint() {
    super(new PanelEssai(null), null, null);
    final BGroupeCalque gc = new BGroupeCalque();
    gc.setTitle("Mailleur");
    setName("MAILLEUR");
    setTitle("ModeleVSOLD");
    cqZPoint_ = new ZCalquePointEditable();
    cqZPoint_.setName("cqPOINTMAILLEUR");
    cqZPoint_.setTitle("pointsModele");
    gc.add(cqZPoint_);
    afficheSemis();
    // le modele pour les berges
    // il s'agit de preciser quel semis de point � utiliser (un ZModelePoint)
    final ExampleModelLigneBrisee m = ExampleModelLigneBrisee.buildDefault();
    // le calque berge
    final ZCalqueLigneBrisee calqueBerge = new ZCalqueLigneBriseeEditable();
    calqueBerge.setName("cqBerge");
    calqueBerge.setTitle("ligne brisee");
    calqueBerge.setSelectionMode(SelectionMode.ATOMIC);
//    calqueBerge.setAtomicMode(true);
    calqueBerge.setIconModel(0, new TraceIconModel(TraceIcon.CARRE, 1, Color.GREEN));
    calqueBerge.setForeground(Color.GREEN);
    calqueBerge.modele(m);
    gc.add(calqueBerge);
    // le calque d'interaction pour les berges
    setCalque(gc);
    pn_.getVueCalque().setListener(this);
    getVueCalque().setBackground(Color.white);
    getVueCalque().setPreferredSize(new Dimension(500, 400));
    /*
     * ZCalqueEditionInteraction forme = new ZCalqueEditionInteraction(new TestZCalqueEdtionSupport());
     * forme.setTypeForme(DeForme.RECTANGLE); forme.setName("cqForme"); pn_.addCalqueInteraction(forme);
     * pn_.setCalqueInteractionActif(forme);
     */
    this.pack();
    restaurer();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    super.actionPerformed(_e);
  }

  @Override
  public JPopupMenu getCmdsContextuelles() {
    final JPopupMenu r = new JPopupMenu();
    final JMenuItem m = new JMenuItem();
    m.setText("Construire polyligne");
    m.addActionListener(this);
    m.setActionCommand("BUILD_POLY");
    r.add(m);
    r.add(new BuSeparator());

    pn_.fillMenuWithToolsActions(r);
    return r;
  }

  public VecteurGrPoint importerSemis() {
    double minX = Double.POSITIVE_INFINITY;
    double minY = Double.POSITIVE_INFINITY;
    int p = 0;
    // int prec = 0;
    VecteurGrPoint semis = null;
    try {
      semis = new VecteurGrPoint();
      final LineNumberReader in = new LineNumberReader(new FileReader(FuLib.decodeWwwFormUrl(getClass().getResource(
          FICHIERSEMIS).getPath())));
      String line = in.readLine();
      StringTokenizer tok = null;
      while (line != null) {
        if (line.trim().startsWith("#")) {
          line = in.readLine();
          continue;
        }
        tok = new StringTokenizer(line);
        final double x = Double.parseDouble(tok.nextToken());
        final double y = Double.parseDouble(tok.nextToken());
        final double z = Double.parseDouble(tok.nextToken());
        if (x < minX) {
          minX = x;
        }
        if (y < minY) {
          minY = y;
        }
        semis.ajoute(new GrPoint(x, y, z));
        p += line.length();
        line = in.readLine();
      }
      in.close();
    } catch (final IOException e) {
      semis = null;
      return null;
    }
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    final int siz = semis.nombre();
    // prec = 0;
    for (int i = 0; i < siz; i++) {
      final GrPoint po = semis.renvoie(i);
      po.x_ -= minX;
      po.y_ -= minY;
    }
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    System.out.println("Fin lecture Fichier");
    return semis;
  }

  public void afficheSemis() {
    final VecteurGrPoint v = importerSemis();
    final ZModelePointEditable pt = new ZModelePointEditable(v.tableau());
    cqZPoint_.setModele(pt);
  }

  /*
   * public void selectedObjects(SelectionEvent _evt) { VecteurGrContour objets=_evt.getObjects();
   * System.out.println(objets.nombre()); for(int i=0;i <objets.nombre();i++) { GrPoint[]
   * contours=objets.renvoie(i).contour(); for(int j=0;j <contours.length;j++) System.out.println(contours[j]); } }
   */
  public static void main(final String[] _args) {
    // BVueCalque vue=new BVueCalque();
    final ExampleZCalquePoint testCalque = new ExampleZCalquePoint();
    final BArbreCalque arbre = new BArbreCalque();
    arbre.setModel(testCalque.getArbreCalqueModel());
    testCalque.setVisible(true);
    testCalque.setSize(new Dimension(450, 400));
    final JFrame frame = new JFrame(testCalque.getTitle());
    final BuPanel princ = new BuPanel();
    princ.setLayout(new BuBorderLayout());
    frame.setContentPane(princ);
    final BuDesktop desk = new BuDesktop();
    desk.setPreferredSize(new Dimension(500, 500));
    arbre.setPreferredSize(new Dimension(200, 500));
    desk.add(testCalque);
    final BuSplit2Pane split = new BuSplit2Pane(desk, arbre);
    princ.add(split, BuBorderLayout.CENTER);
    frame.setLocation(50, 50);
    frame.setSize(new Dimension(700, 500));
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    testCalque.restaurer();
    testCalque.setLocation(200, 200);
    final ZTransformationDomaine bn = new ZTransformationDomaine(testCalque.getVueCalque());
    bn.setVisible(true);
    bn.setSize(new Dimension(150, 150));
    desk.add(bn);
    try {
      Thread.sleep(250);
    } catch (final InterruptedException ie) {}
    /*
     * testCalque.calculMaillage(); testCalque.getVueCalque().repaint(0);
     */
  }
}