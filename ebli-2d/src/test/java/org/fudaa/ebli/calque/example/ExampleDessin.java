/*
 * @file         TestDessin.java
 * @creation     1998-08-31
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.example;

import com.memoire.bu.BuPicture;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import org.fudaa.ebli.calque.BCalqueDessin;
import org.fudaa.ebli.calque.BCalqueDessinInteraction;
import org.fudaa.ebli.calque.BControleurCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.BVueCalque;
import org.fudaa.ebli.calque.dessin.DeCarre;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.dessin.DeLigneBrisee;
import org.fudaa.ebli.calque.dessin.DePolygone;
import org.fudaa.ebli.calque.dessin.DeTrait;
import org.fudaa.ebli.example.ExampleFrame;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Un test de BCalqueDessin.
 *
 * @version $Id: TestDessin.java,v 1.2 2006-10-19 14:13:24 deniger Exp $
 * @author Axel von Arnim
 */
public class ExampleDessin {

  public static void main(final String[] args) {
    final BCalqueDessin d = new BCalqueDessin();
    final DeLigneBrisee l = new DeLigneBrisee();
    l.ajoute(new GrPoint(0., 0., 0.));
    l.ajoute(new GrPoint(100., 100., 0.));
    l.ajoute(new GrPoint(100., 200., 0.));
    l.ajoute(new GrPoint(200., 200., 0.));
    l.setTypeTrait(TraceLigne.POINTILLE);
    l.setForeground(Color.blue);
    d.ajoute(l);
    final DePolygone p = new DePolygone();
    p.ajoute(new GrPoint(200., 50., 0.));
    p.ajoute(new GrPoint(100., 80., 0.));
    p.ajoute(new GrPoint(150., 200., 0.));
    p.ajoute(new GrPoint(200., 200., 0.));
    p.setForeground(Color.red);
    d.ajoute(p);
    final DeCarre r
            = new DeCarre(
                    new GrPoint(250., 50., 0.),
                    new GrVecteur(50., 0., 0.),
                    new GrVecteur(0., 70., 0.));
    r.setForeground(Color.green);
    d.ajoute(r);
    final DeTrait t
            = new DeTrait(new GrPoint(0., 0., 0.), new GrPoint(300., 200., 0.));
    t.setTypeTrait(TraceLigne.TIRETE);
    t.setForeground(Color.white);
    d.ajoute(t);
    final BCalqueDessinInteraction di = new BCalqueDessinInteraction(d);
    di.setTypeForme(DeForme.CARRE);
    final BControleurCalque ci = new BControleurCalque();
    ci.setCalque(di);
    final BGroupeCalque gc = new BGroupeCalque();
    gc.add(d);
    gc.add(di);
    final BVueCalque vc = new BVueCalque(gc);
    //    vc.setCalque();
    vc.setPreferredSize(new Dimension(640, 480));
    final JPanel pan = new JPanel();
    pan.add(vc);
    final JComponent[] beans = new JComponent[]{pan};
    final ExampleFrame f = new ExampleFrame(beans);
    f.setSize(new Dimension(700, 520));
    gc.setBorder(new LineBorder(Color.yellow, 2));
    f.setVisible(true);
    final BuPicture im = new BuPicture(EbliResource.EBLI.getImage("test-dessin.jpg"));
    final ExampleFrame f2 = new ExampleFrame(im, "image de contr�le");
    f2.pack();
    f2.setVisible(true);
  }
}
