/*
 * @file         TestCalques.java
 * @creation     1998-08-27
 * @modification $Date: 2006-11-14 09:06:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.example;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuPopupButton;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.dessin.DeLigneBrisee;
import org.fudaa.ebli.controle.BSelecteurReduitCouleur;
import org.fudaa.ebli.example.ExampleFrame;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.repere.BTransformationBouton;
import org.fudaa.ebli.repere.BTransformationGlissiere;
import org.fudaa.ebli.repere.CoordonneesEvent;
import org.fudaa.ebli.repere.CoordonneesListener;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Un test des classes Calques.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-11-14 09:06:26 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class ExampleCalques {
  public static void main(final String[] args) {
    /*
    try
    {
      UIManager.setLookAndFeel
    ("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
    }
    catch (Exception ex) { System.err.println(""+ex); }
    */
    final JLabel ss= new JLabel();
    ss.setFont(new Font("SansSerif", Font.PLAIN, 10));
    ss.setPreferredSize(new Dimension(200, 20));
    ss.setSize(new Dimension(100, 20));
    final BCalque[] c= new BCalque[8];
    {
      final BCalque[] cu= new BCalque[2];
      {
        final BuInformationsDocument id= new BuInformationsDocument();
        id.name= "Essai";
        id.organization= "CETMEF";
        id.author= "Guillaume Desnoix";
        id.contact= "g.desnoix@stcpmvn.equipement.gouv.fr";
        id.date= "02/09/1998";
        id.logo= EbliResource.EBLI.getIcon("minlogo.gif");
        final BCalqueCartouche calque= new BCalqueCartouche();
        calque.setName("Cartouche");
        calque.setInformations(id);
        calque.setForeground(Color.black);
        calque.setBackground(new Color(255, 255, 224));
        calque.setFont(new Font("SansSerif", Font.PLAIN, 10));
        calque.setAttenue(false);
        cu[0]= calque;
      }
      {
        final GrBoite b= new GrBoite();
        b.ajuste(new GrPoint(0., 0., 0.));
        b.ajuste(new GrPoint(400., 400., 0.));
        final BCalqueGrille calque= new BCalqueGrille();
        calque.setName("Grille");
        calque.setBoite(b);
        calque.setForeground(Color.lightGray);
        calque.setFont(new Font("SansSerif", Font.PLAIN, 8));
        calque.setAttenue(false);
        cu[1]= calque;
      }
      final BGroupeCalque gcu= new BGroupeCalque();
      gcu.setName("Informations");
      gcu.add(cu[0]);
      gcu.add(cu[1]);
      c[0]= gcu;
    }
    {
      final BCalque[] ci= new BCalque[2];
      {
        final BCalqueImage calque= new BCalqueImage();
        calque.setName("Abeille");
        calque.setNW(new GrPoint(-100., -100., 0.));
        calque.setNE(new GrPoint(100., -100., 0.));
        calque.setSE(new GrPoint(100., 100., 0.));
        calque.setSW(new GrPoint(-100., 100., 0.));
        calque.setImage((new ImageIcon("abeille1.gif")).getImage());
        //	calque.setCouleur(new Color(255,255,192));
        calque.setForeground(new Color(255, 255, 192));
        ci[0]= calque;
      }
      {
        final BCalqueImage calque= new BCalqueImage();
        calque.setName("Linux");
        calque.setNW(new GrPoint(0., 200., 0.));
        calque.setNE(new GrPoint(100., 300., 0.));
        calque.setSE(new GrPoint(200., 200., 0.));
        calque.setSW(new GrPoint(100., 100., 0.));
        calque.setImage((new ImageIcon("linuxpow.gif")).getImage());
        ci[1]= calque;
      }
      final BGroupeCalque gci= new BGroupeCalque();
      gci.setName("Fonds de carte");
      gci.add(ci[0]);
      gci.add(ci[1]);
      c[1]= gci;
    }
    {
      final BCalqueTexte calque= new BCalqueTexte();
      calque.setName("Etiquettes");
      for (int i= 0; i < 300; i += 30) {
        calque.ajoute(
          new GrPoint(Math.cos(i / 10.) * 50., i * 1., 0.),
          "Etiquette " + i);
      }
      calque.setForeground(Color.black);
      calque.setBackground(Color.yellow);
      calque.setFont(new Font("SansSerif", Font.PLAIN, 10));
      calque.setEtiquettePleine(true);
      c[2]= calque;
    }
    {
      final BCalquePolyligne calque= new BCalquePolyligne();
      calque.setName("Sinus");
      final GrPolyligne pl= new GrPolyligne();
      for (int i= 0; i < 300; i++) {
        pl.sommets_.ajoute(new GrPoint(Math.cos(i / 10.) * 50., i * 1., 0.));
      }
      calque.ajoute(pl);
      calque.setForeground(Color.blue);
      c[3]= calque;
    }
    {
      final DeLigneBrisee l= new DeLigneBrisee();
      l.ajoute(new GrPoint(0., 0., 0.));
      l.ajoute(new GrPoint(150., 150., 0.));
      l.ajoute(new GrPoint(150., 250., 0.));
      l.ajoute(new GrPoint(250., 250., 0.));
      l.setTypeTrait(TraceLigne.POINTILLE);
      //      l.setCouleurContour(Color.blue);
      l.setForeground(Color.blue);
      final BCalqueDessin calque= new BCalqueDessin();
      calque.setName("Dessin");
      calque.ajoute(l);
      c[4]= calque;
    }
    {
      final BCalqueSuiviSourisInteraction calque= new BCalqueSuiviSourisInteraction();
      calque.setName("Souris");
      calque.addCoordonneesListener(new SuiviSouris(ss));
      c[5]= calque;
    }
    {
      final BCalqueDessin calque= new BCalqueDessin();
      calque.setName("Dessin");
      c[6]= calque;
    }
    {
      final BCalqueDessinInteraction calque=
        new BCalqueDessinInteraction((BCalqueDessin)c[6]);
      calque.setName("Dessin (interaction)");
      c[7]= calque;
    }
    final BGroupeCalque gc= new BGroupeCalque();
    gc.setName("Plan-masse");
    for (int i= c.length - 1; i >= 0; i--) {
      gc.add(c[i]);
    }
    final BVueCalque vc= new BVueCalque(gc);
    //    vc.setCalque();
    vc.setBackground(Color.white);
    final BTransformationGlissiere e= new BTransformationGlissiere();
    e.setTypeTransfo(RepereEvent.ZOOM);
    e.addRepereEventListener(vc);
    final BTransformationGlissiere tx= new BTransformationGlissiere();
    tx.setTypeTransfo(RepereEvent.TRANS_X);
    tx.addRepereEventListener(vc);
    final BTransformationGlissiere ty= new BTransformationGlissiere();
    ty.setTypeTransfo(RepereEvent.TRANS_Y);
    ty.addRepereEventListener(vc);
    final BTransformationGlissiere rz= new BTransformationGlissiere();
    rz.setTypeTransfo(RepereEvent.ROT_Z);
    rz.addRepereEventListener(vc);
    final BTransformationBouton bt= new BTransformationBouton(RepereEvent.ROT_Z);
    bt.addRepereEventListener(vc);
    final BArbreCalqueModel acModel= new BArbreCalqueModel(gc);
    final BArbreCalque ac= new BArbreCalque();
    ac.setModel(acModel);
    final JScrollPane spac= new JScrollPane(ac);
    final Dimension dspac= ac.getPreferredSize();
    final Dimension de= e.getPreferredSize();
    dspac.width += 20;
    dspac.height= de.height;
    spac.setPreferredSize(dspac);
    spac.setRequestFocusEnabled(false);
    final BSelecteurReduitCouleur srcfg= new BSelecteurReduitCouleur();
    srcfg.addPropertyChangeListener(acModel);
    srcfg.setPropertyName("foreground");
    final BuPopupButton pbfg= new BuPopupButton(srcfg);
    pbfg.setText("Trac�");
    final BSelecteurReduitCouleur srcbg= new BSelecteurReduitCouleur();
    srcbg.addPropertyChangeListener(acModel);
    srcbg.setPropertyName("background");
    final BuPopupButton pbbg= new BuPopupButton(srcbg);
    pbbg.setText("Remplissage");
    // BControleurCalque cc=new BControleurCalque((BCalqueAffichage)c[4]);
    // SelectionCalque s=new SelectionCalque(cc);
    // ac.addTreeSelectionListener(s);
    final JComponent[] ctrls=
      new JComponent[] { spac, e, tx, ty, rz, bt, ss, pbfg, pbbg };
    final JPanel panel= new JPanel(new FlowLayout(FlowLayout.LEFT));
    for (int i= 0; i < ctrls.length; i++) {
      panel.add(ctrls[i]);
    }
    final JComponent[] beans= new JComponent[] { vc, panel };
    final ExampleFrame f = new ExampleFrame(beans);
    vc.setPreferredSize(new Dimension(500, 500));
    panel.setPreferredSize(new Dimension(500, 500));
    f.setVisible(true);
  }
}
/*
class SelectionCalque
      implements TreeSelectionListener
{
  BControleurCalque controleur_;

  public SelectionCalque(BControleurCalque _controleur)
  {
    controleur_=_controleur;
  }

  public void valueChanged(TreeSelectionEvent _evt)
  {
    Object o=_evt.getPath().getLastPathComponent();
    if(o instanceof BCalqueAffichage)
      controleur_.setCalque((BCalqueAffichage)o);
  }
}
*/
class SuiviSouris implements CoordonneesListener {
  JLabel label_;
  public SuiviSouris(final JLabel _label) {
    label_= _label;
  }
  @Override
  public void coordonneesModifiees(final CoordonneesEvent _evt) {
    final double[] c= _evt.valeur();
    //    boolean[] d=_evt.defini();
    //    boolean[] m=_evt.modifie();
    String r= "";
    for (int i= 0; i < c.length; i++) {
      if (i == 0) {
        r += " X:" + (int)c[i];
      }
      if (i == 1) {
        r += " Y:" + (int)c[i];
      }
      if (i == 2) {
        r += " Z:" + (int)c[i];
      }
      if (i >= 3) {
        r += " V" + (i - 3) + ":" + (int)c[i];
      }
    }
    label_.setText(r);
  }
}
