/*
 *  @file         BCalqueMaillage.java
 *  @creation     1998-09-28
 *  @modification $Date: 2007-05-04 13:49:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.PrintGraphics;
import java.awt.Rectangle;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMaillage;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.palette.BPaletteCouleur;
import org.fudaa.ebli.palette.BPaletteCouleurSimple;
import org.fudaa.ebli.trace.Gouraud;
import org.fudaa.ebli.trace.TraceIsoLignes;
import org.fudaa.ebli.trace.TraceIsoSurfaces;
/**
 * Un calque d'affichage de cartes.
 *
 * @version   $Id: BCalqueMaillage.java,v 1.16 2007-05-04 13:49:42 deniger Exp $
 * @author    Guillaume Desnoix
 */
public class BCalqueMaillage extends BCalqueAffichage {

  /** */
  private GrMaillage maillageEcran_;
  /** */
  private GrBoite boite_;
  /** */
  private double vmin_, vmax_;
  // Propriete maillage
  /** */
  private GrMaillage maillage_;
  // Propriete valeurs
  /** */
  private double[] valeurs_;
  // Propriete paletteCouleur
  /** */
  private BPaletteCouleur paletteCouleur_;
  /*
   *  private int typeTrait_;
   *  public int getTypeTrait() { return typeTrait_; }
   *  public void setTypeTrait(int _typeTrait)
   *  {
   *  if(typeTrait_!=_typeTrait)
   *  {
   *  int vp=typeTrait_;
   *  typeTrait_=_typeTrait;
   *  firePropertyChange("typeTrait",vp,typeTrait_);
   *  quick_repaint();
   *  }
   *  }
   */
  /** */
  private boolean contour_;
  /** */
  private boolean surface_;
  /** */
  private boolean isolignes_;
  /** */
  private boolean isosurfaces_;
  /** */
  private double ecart_;

  /** */
  public BCalqueMaillage() {
    super();
    maillage_ = null;
    paletteCouleur_ = null;
    maillageEcran_ = null;
    boite_ = null;
    vmax_ = 255.;
    vmin_ = 0.;
    isosurfaces_ = true;
    isolignes_ = true;
    ecart_ = 0.10;
    setBackground(Color.white);
    setForeground(Color.black);
  }

  // Propriete heritee versEcran
  /**
   * @param _v  VersEcran
   */
  @Override
  public void setVersEcran(final GrMorphisme _v){
    maillageEcran_ = null;
    super.setVersEcran(_v);
  }

  /**
   * @param _maillage  Maillage
   */
  public void setMaillage(final GrMaillage _maillage){
    if (maillage_ != _maillage) {
      final GrMaillage vp = maillage_;
      maillage_ = _maillage;
      boite_ = null;
      maillageEcran_ = null;
      firePropertyChange("maillage", vp, maillage_);
      repaint();
    }
  }

  /**
   * @param _valeurs  Valeurs
   */
  public void setValeurs(final double[] _valeurs){
    if (valeurs_ != _valeurs) {
      final double[] vp = valeurs_;
      valeurs_ = _valeurs;
      firePropertyChange("valeurs", vp, valeurs_);
      vmax_ = Double.MIN_VALUE;
      vmin_ = Double.MAX_VALUE;
      for (int i = 0; i < valeurs_.length; i++) {
        final double v = valeurs_[i];
        if (vmin_ > v) {
          vmin_ = v;
        }
        if (vmax_ < v) {
          vmax_ = v;
        }
      } 
      if (FuLog.isDebug()) {
        FuLog.debug("EBL: " + getClass().getName() + ' ' + "setValeurs DV=" + vmin_ + ' ' + vmax_);
      }
      repaint();
    }
  }

  /**
   * @param _palette  PaletteCouleur
   */
  public void setPaletteCouleur(final BPaletteCouleur _palette){
    if ((_palette != null) && (paletteCouleur_ != _palette)) {
      final BPaletteCouleur vp = paletteCouleur_;
      paletteCouleur_ = _palette;
      // if(vp!=null) vp.removePropertyChangeListener(this);
      // paletteCouleur_.addPropertyChangeListener(this);
      // try/catch temporaire... bug dans quick ?
      try {
        firePropertyChange("paletteCouleur", vp, paletteCouleur_);
        quickRepaint();
      }
      catch (final Exception ex) {
        FuLog.warning(ex);
      }
    }
  }

  /**
   * D�finit si le trac� des �l�ments du maillage doit �tre r�alis�. <p>
   *
   * Si le trac� sous forme de surfaces ou d'isosurfaces est actif, les contours
   * des �l�ments sont trac�s dans la couleur de fond du calque. Autrement,
   * chaque ar�te est trac� avec une couleur correspondant la valeur moyenne
   * entre les 2 noeuds de cette ar�te.
   *
   * @param _v  <i>true</i> Le trac� est r�alis�. <i>false</i> Sinon.
   */
  public void setContour(final boolean _v){
    if (_v != contour_) {
      final boolean vp = contour_;
      contour_ = _v;
      firePropertyChange("contour", vp, contour_);
      quickRepaint();
    }
  }

  /**
   * D�finit si le trac� du maillage doit �tre r�alis� sous forme de surfaces.
   * Pour chaque �l�ment, une seule couleur est alors employ�e correspondant �
   * la moyenne des valeurs sur les noeuds. <p>
   *
   * IMPORTANT : Le trac� de surfaces est inhib� par le trac� sous forme de
   * surfaces.
   *
   * @param _v  <i>true</i> Le trac� se fait sous forme de surfaces. <i>false
   *      </i> Sinon.
   */
  public void setSurface(final boolean _v){
    if (_v != surface_) {
      final boolean vp = surface_;
      surface_ = _v;
      firePropertyChange("surface", vp, surface_);
      quickRepaint();
    }
  }

  /**
   * D�finit si les valeurs aux noeuds du maillage doivent �tre trac�e sous
   * forme d'isolignes. Ces isolignes correspondent aux diff�rents niveaux de la
   * palette de couleurs. <p>
   *
   * Si le trac� sous forme de surfaces ou d'isosurfaces est actif, les
   * isolignes sont trac�es en noir. Autrement, elles sont trac�es suivant la
   * couleur correspondant � leur niveau respectif.
   *
   * @param _v  <i>true</i> Le trac� se fait sous forme d'isolignes. <i>false
   *      </i> Sinon.
   */
  public void setIsolignes(final boolean _v){
    if (_v != isolignes_) {
      final boolean vp = isolignes_;
      isolignes_ = _v;
      firePropertyChange("isolignes", vp, isolignes_);
      quickRepaint();
    }
  }

  /**
   * D�finit si le trac� du maillage doit �tre r�alis� sous forme d'isosurfaces.
   * Le trac� d'isosurface est prioritaire sur le trac� sous forme de surfaces.
   *
   * @param _v  <i>true</i> Le trac� se fait sous forme d'isosurfaces. <i>false
   *      </i> Sinon.
   */
  public void setIsosurfaces(final boolean _v){
    if (_v != isosurfaces_) {
      final boolean vp = isosurfaces_;
      isosurfaces_ = _v;
      firePropertyChange("isosurfaces", vp, isosurfaces_);
      quickRepaint();
    }
  }

  /**
   * @param _v  Ecart
   */
  public void setEcart(final double _v){
    double v=_v;
    if (v < 0.001) {
      v = 0.001;
    }
    if (v > 0.500) {
      v = 0.500;
    }
    if (v != ecart_) {
      final double vp = ecart_;
      ecart_ = v;
      firePropertyChange("ecart", vp, ecart_);
      quickRepaint();
    }
  }

  /**
   * @return   Domaine
   */
  @Override
  public GrBoite getDomaine(){
    GrBoite r = super.getDomaine();
    if (maillage_ != null) {
      final GrBoite b = maillage_.boite();
      if (r == null) {
        r = b;
      } else {
        r = r.union(b);
      }
    }
    return r;
  }

  /**
   * @return   Maillage
   */
  public GrMaillage getMaillage(){
    return maillage_;
  }

  /**
   * @return   Valeurs
   */
  public double[] getValeurs(){
    return valeurs_;
  }

  /**
   * @return   PaletteCouleur
   */
  public BPaletteCouleur getPaletteCouleur(){
    return paletteCouleur_;
  }

  /**
   * Retourne l'�tat d'activation du trac� des �l�ments du maillage.
   *
   * @return   <i>true</i> Le trac� des �l�ments est actif. <i>false</i> Sinon.
   */
  public boolean getContour(){
    return contour_;
  }

  /**
   * Retourne l'�tat d'activation du trac� sous forme de surfaces.
   *
   * @return   <i>true</i> Le trac� est effectu� sous forme de surfaces. <i>
   *      false</i> Sinon.
   */
  public boolean getSurface(){
    return surface_;
  }

  /**
   * Retourne l'�tat d'activation du trac� sous forme d'isolignes.
   *
   * @return   <i>true</i> Le trac� est effectu� sous forme d'isolignes. <i>
   *      false</i> Sinon.
   */
  public boolean getIsolignes(){
    return isolignes_;
  }

  /**
   * Retourne l'�tat d'activation du trac� sous forme d'isosurfaces.
   *
   * @return   <i>true</i> Le trac� est effectu� sous forme d'isosurfaces. <i>
   *      false</i> Sinon.
   */
  public boolean getIsosurfaces(){
    return isosurfaces_;
  }

  /**
   * @return   Ecart
   */
  public double getEcart(){
    return ecart_;
  }

  // Icon
  /**
   * @param _c
   * @param _g
   * @param _x
   * @param _y
   */
  @Override
  public void paintIcon(final Component _c,final Graphics _g,final int _x,final int _y){
    super.paintIcon(_c, _g, _x, _y);
    _g.translate(_x, _y);
    final boolean attenue = isAttenue();
    final int w = getIconWidth();
    final int h = getIconHeight();
    Color fg = getForeground();
    Color bg = getBackground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    if (isosurfaces_) {
      Color c;
      c = paletteCouleur_.couleur(0.0);
      if (attenue) {
        c = attenueCouleur(c);
      }
      _g.setColor(c);
      _g.fillRect(1, 1, w - 1, h - 1);
      c = paletteCouleur_.couleur(0.5);
      if (attenue) {
        c = attenueCouleur(c);
      }
      _g.setColor(c);
      _g.fillOval(3, 3, w - 5, h - 5);
      c = paletteCouleur_.couleur(1.0);
      if (attenue) {
        c = attenueCouleur(c);
      }
      _g.setColor(c);
      _g.fillOval(7, 7, w - 14, h - 14);
    }
    for (int i = 2; i < w - 5; i += 4) {
      for (int j = 2; j < h - 5; j += 4) {
        final int[] vx = new int[] { i, i + 4, i + 4, i};
        final int[] vy = new int[] { j, j, j + 4, j + 4};
        final double v = (double) j / (double) h;
        Color c = paletteCouleur_.couleur(v);
        if (attenue) {
          c = attenueCouleur(c);
        }
        if (surface_ && !isosurfaces_) {
          _g.setColor(c);
          _g.fillPolygon(vx, vy, 4);
        }
        if (contour_) {
          if (surface_ || isolignes_ || isosurfaces_) {
            _g.setColor(bg);
          } else {
            _g.setColor(c);
          }
          _g.drawPolygon(vx, vy, 4);
        }
      }
    }
    if (isolignes_) {
      Color c;
      if (surface_ || isosurfaces_) {
        _g.setColor(fg);
      } else {
        c = paletteCouleur_.couleur(0.5);
        if (attenue) {
          c = attenueCouleur(c);
        }
        _g.setColor(c);
      }
      _g.drawOval(3, 3, w - 5, h - 5);
      if (surface_ || isosurfaces_) {
        _g.setColor(fg);
      } else {
        c = paletteCouleur_.couleur(1.0);
        if (attenue) {
          c = attenueCouleur(c);
        }
        _g.setColor(c);
      }
      _g.drawOval(7, 7, w - 14, h - 14);
    }
    _g.translate(-_x, -_y);
  }

  /**
   * @param _g
   */
  @Override
  public void paintComponent(final Graphics _g){
    final boolean attenue = isAttenue();
    final boolean rapide = isRapide();
    int i;
    int j;
    if (boite_ == null) {
      boite_ = maillage_.boite();
    }
    final GrMorphisme versEcran = getVersEcran();
    final Polygon pecr = boite_.enPolygoneXY().applique(versEcran).polygon();
    final Rectangle clip = _g.getClipBounds();
    if (clip.intersects(pecr.getBounds())) {
      if (rapide) {
        Color c;
        if (surface_) {
          c = paletteCouleur_.couleur(0.5);
          if (attenue) {
            c = attenueCouleur(c);
          }
          _g.setColor(c);
          _g.fillPolygon(pecr);
        }
        c = paletteCouleur_.couleur(0.7);
        if (attenue) {
          c = attenueCouleur(c);
        }
        _g.setColor(c);
        _g.drawPolygon(pecr);
      }
      else {
        if (maillageEcran_ == null) {
          maillageEcran_ = maillage_.applique(versEcran);
        }
        Color fg = getForeground();
        Color bg = getBackground();
        if (attenue) {
          fg = attenueCouleur(fg);
        }
        if (attenue) {
          bg = attenueCouleur(bg);
        }
        BPaletteCouleur piso = paletteCouleur_;
        if (surface_ || isosurfaces_) {
          final BPaletteCouleurSimple ps = new BPaletteCouleurSimple();
          ps.setCouleurMin(fg);
          ps.setCouleurMax(fg);
          piso = ps;
        }
        final TraceIsoLignes isol = new TraceIsoLignes(ecart_, piso);
        final TraceIsoSurfaces isos = new TraceIsoSurfaces(ecart_, paletteCouleur_);
        final int n = maillageEcran_.nombre();
        if (isosurfaces_) {
          for (i = 0; i < n; i++) {
            final Polygon p = maillageEcran_.polygon(i);
            if (clip.intersects(p.getBounds())) {
              final int[] noeuds = (int[]) maillageEcran_.connectivites_.get(i);
              final int m = noeuds.length;
              final double[] v = new double[m];
              for (j = 0; j < m; j++) {
                v[j] = (valeurs_[noeuds[j]] - vmin_) / (vmax_ - vmin_);
              }
              isos.draw(_g, p, v);
            }
          }
        }
        if (contour_ || surface_) {
          Gouraud grd = null;
          if (_g instanceof PrintGraphics) {
            if (FuLog.isDebug()) {
              FuLog.debug("EBL: Gouraud printer");
            }
            grd = new Gouraud(_g, 2, 2);
          }
          else {
            if (FuLog.isDebug()) {
              FuLog.debug("EBL: Gouraud screen");
            }
            grd = new Gouraud(_g, 16, 16);
          }
          for (i = 0; i < n; i++) {
            final Polygon p = maillageEcran_.polygon(i);
            if (clip.intersects(p.getBounds())) {
              final int[] noeuds = (int[]) maillageEcran_.connectivites_.get(i);
              final int m = noeuds.length;
              // double v=(valeurs_[noeuds[0]]-vmin_)/(vmax_-vmin_);
              if (surface_ && !isosurfaces_) {
                /*
                 *  _g.setColor(c);
                 *  _g.fillPolygon(p);
                 */
                final int[] rc = new int[m];
                final int[] vc = new int[m];
                final int[] bc = new int[m];
                for (j = 0; j < m; j++) {
                  final double v = (valeurs_[noeuds[j]] - vmin_) / (vmax_ - vmin_);
                  Color c = paletteCouleur_.couleur(v);
                  if (attenue) {
                    c = attenueCouleur(c);
                  }
                  rc[j] = c.getRed();
                  vc[j] = c.getGreen();
                  bc[j] = c.getBlue();
                }
                grd.fillRectangle(p.xpoints, p.ypoints, rc, vc, bc);
              }
              if (contour_) {
                double v = 0.;
                for (j = 0; j < m; j++) {
                  v += (valeurs_[noeuds[j]] - vmin_) / (vmax_ - vmin_);
                }
                v /= m;
                Color c = paletteCouleur_.couleur(v);
                if (attenue) {
                  c = attenueCouleur(c);
                }
                if (surface_ || isolignes_ || isosurfaces_) {
                  _g.setColor(bg);
                } else {
                  _g.setColor(c);
                }
                _g.drawPolygon(p);
              }
            }
          }
        }
        if (isolignes_) {
          for (i = 0; i < n; i++) {
            final Polygon p = maillageEcran_.polygon(i);
            if (clip.intersects(p.getBounds())) {
              final int[] noeuds = (int[]) maillageEcran_.connectivites_.get(i);
              final int m = noeuds.length;
              final double[] v = new double[m];
              for (j = 0; j < m; j++) {
                v[j] = (valeurs_[noeuds[j]] - vmin_) / (vmax_ - vmin_);
              }
              isol.draw(_g, p, v);
            }
          }
        }
      }
    }
    super.paintComponent(_g);
  }
  /*
   *  class TransparentFilter extends RGBImageFilter
   *  {
   *  int r_,g_,b_;
   *  public TransparentFilter(Color _color)
   *  {
   *  r_=_color.getRed();
   *  g_=_color.getGreen();
   *  b_=_color.getBlue();
   *  }
   *  public int filterRGB(int x, int y, int rgb)
   *  {
   *  int a=(rgb & 0xff000000) >>24;
   *  int r=(rgb & 0x00ff0000) >>16;
   *  int g=(rgb & 0x0000ff00) >> 8;
   *  int b=(rgb & 0x000000ff)     ;
   *  if((r==r_)&&(g==g_)&&(b==b_)) a=0;
   *  return (a<<24) | (r<<16) | (g<<8) | b;
   *  }
   *  }
   */
}
