/*
 * @creation     15 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;

/**
 * Une selection pour des g�om�tries ou des sommets de la scene. La selection contient des indices globaux de g�om�tries. 
 * En cas de selection en mode g�om�tries, la liste des sommets pour un index de g�om�trie est vide.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZSceneSelection extends EbliListeSelectionMulti {
  ZScene scn_;

  /**
   * Cr�ation de la liste.
   * @param _scn La scene.
   */
  public ZSceneSelection(ZScene _scn) {
    super(0);
    scn_=_scn;
  }
  
  /**
   * Ajoute la selection de g�om�tries d'un calque donn�.
   * @param _cq Le calque contenant la liste de selection.
   * @param _sel La liste des indices de g�om�tries pour le calque.
   */
  public void addLayerListSelection(ZCalqueAffichageDonneesInterface _cq, CtuluListSelectionInterface _sel) {
    int idecal=0;
    for (ZCalqueAffichageDonneesInterface cq : scn_.getTargetLayers()) {
      if (cq==_cq) break;
      idecal+=cq.modeleDonnees().getNombre();
    }
    
    for (int idx=_sel.getMinIndex(); idx<=_sel.getMaxIndex(); idx++) {
      // Ajoute une liste de selection des sommets vide.
      if (_sel.isSelected(idx)) 
        set(idx+idecal,new CtuluListSelection(0));
    }
  }
  
  /**
   * Ajoute la selection de vertex d'un calque donn�.
   * @param _cq Le calque contenant la liste de selection.
   * @param _sel La liste des indices de sommets par g�om�trie pour le calque.
   */
  public void addLayerListSelectionMulti(ZCalqueAffichageDonneesInterface _cq, EbliListeSelectionMultiInterface _sel) {
    int idecal=0;
    for (ZCalqueAffichageDonneesInterface cq : scn_.getTargetLayers()) {
      if (cq==_cq) break;
      idecal+=cq.modeleDonnees().getNombre();
    }
    
    CtuluListSelection selGeom=_sel.getIdxSelection();
    for (int idx=selGeom.getMinIndex(); idx<=selGeom.getMaxIndex(); idx++) {
      if (selGeom.isSelected(idx)) 
        set(idx+idecal,(CtuluListSelection)_sel.getSelection(idx));
    }
  }
  
  /**
   * Extrait la selection de vertex d'un calque donn�.
   * @param _cq Le calque contenant la liste de selection.
   * @return La selection pour un calque donn�.
   */
  public EbliListeSelectionMultiInterface getLayerListSelectionMulti(ZCalqueAffichageDonneesInterface _cq) {
    int idecal=0;
    for (ZCalqueAffichageDonneesInterface cq : scn_.getTargetLayers()) {
      if (cq==_cq) break;
      idecal+=cq.modeleDonnees().getNombre();
    }
    
    EbliListeSelectionMulti sel=new EbliListeSelectionMulti(0);
    
    for (int idx=idecal, iend=idecal+_cq.modeleDonnees().getNombre(); idx<iend; idx++) {
      CtuluListSelectionInterface selVertices=getSelection(idx);
      if (selVertices!=null) 
        sel.set(idx-idecal,(CtuluListSelection)selVertices);
    }
    
    return sel;
  }
  
  public ZScene getScene() {
    return scn_;
  }
}
