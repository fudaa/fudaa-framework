/*
 * @creation 9 nov. 06
 * 
 * @modification $Date: 2007-06-05 08:58:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author fred deniger
 * @version $Id: ZCalqueAffichageDonneesTraceConfigure.java,v 1.3 2007-06-05 08:58:38 deniger Exp $
 */
public class ZCalqueAffichageDonneesLigneConfigure extends BCalqueConfigureSectionAbstract {

  final int idx_;
  final String propLine_;

  private boolean removeColor = false;

  public boolean isRemoveColor() {
    return removeColor;
  }

  public void setRemoveColor(boolean removeColor) {
    this.removeColor = removeColor;
  }

  public ZCalqueAffichageDonneesLigneConfigure(final ZCalqueAffichageDonneesAbstract _target, final int _idx) {
    super(_target, _target.getSetTitle(_idx));
    idx_ = _idx;
    propLine_ = BSelecteurLineModel.getProperty(idx_);
  }

  protected ZCalqueAffichageDonneesAbstract getCq() {
    return (ZCalqueAffichageDonneesAbstract) target_;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    return getCq().setLineModel(idx_, (TraceLigneModel) _newProp);
  }

  @Override
  public Object getProperty(final String _key) {
    return getCq().getLineModel(idx_);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final BSelecteurLineModel selecteurLineModel = new BSelecteurLineModel(propLine_);
    if (removeColor) {
      selecteurLineModel.setAddColor(false);
    }
    selecteurLineModel.setTitle(((ZCalqueAffichageDonneesAbstract) target_).getSetTitle(idx_));
    return new BSelecteurInterface[] { selecteurLineModel };
  }

}
