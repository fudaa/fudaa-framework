/**
 *  @creation     12 juil. 2004
 *  @modification $Date: 2006-07-13 13:35:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZModeleDonneesVide implements ZModeleDonnees {

  GrBoite domaine_;

  @Override
  public GrBoite getDomaine(){
    return domaine_;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer){
    return null;
  }

  @Override
  public boolean isValuesTableAvailable(){
    return false;
  }
  
  

  @Override
  public void fillWithInfo(final InfoData _d,final ZCalqueAffichageDonneesInterface _layer){}

  public void setDomaine(final GrBoite _b){
    domaine_ = _b;
  }

  @Override
  public int getNombre(){
    return 0;
  }

  @Override
  public Object getObject(final int _ind){
    return null;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int vertex) {
    return null;
  }
  
}
