/*
 *  @creation     4 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntObjectIterator;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorChoice;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZModeleLigneBriseeDefault extends ZModeleGeometryDefault 
  implements ZModeleLigneBrisee, ZModeleEditable {
  
  /**
   * @author Fred Deniger
   * @version $Id$
   */
  public static class AtomesTableModel extends AbstractTableModel {

    GISAttributeInterface[] att_;

    final GISZoneCollectionGeometry ligne_;

    final int[] nbLineAtoms_;

    final boolean[] lineClosed_;

    final int nb_;

    public AtomesTableModel(final GISZoneCollectionGeometry _ligne) {
      ligne_ = _ligne;
      nbLineAtoms_ = new int[_ligne.getNumGeometries()];
      lineClosed_ = new boolean[_ligne.getNumGeometries()];
      int nb = 0;
      for (int i = nbLineAtoms_.length - 1; i >= 0; i--) {
        final LineString r = (LineString) _ligne.getGeometry(i);
        lineClosed_[i] = r.isClosed();
        if (lineClosed_[i]) {
          nbLineAtoms_[i] = r.getNumPoints() - 1;
        } else {
          nbLineAtoms_[i] = r.getNumPoints();
        }
        nb += nbLineAtoms_[i];
      }
      nb_ = nb;
      final List r = new ArrayList();
      for (int i = 0; i < ligne_.getNbAttributes(); i++) {
        if (ligne_.getAttribute(i).isUserVisible() && ligne_.getAttribute(i).isAtomicValue()) {
          r.add(ligne_.getAttribute(i));
        }
      }
      att_ = new GISAttributeInterface[r.size()];
      r.toArray(att_);
    }

    @Override
    public int getColumnCount() {
      // indice de la ligne, du point, x, y
      return 4 + att_.length;
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex <= 1) {
        return Integer.class;
      }
      if (_columnIndex >= 4) {
        return att_[_columnIndex - 4].getDataClass();
      }
      return Double.class;
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
      case 0:
        return "N� " + EbliLib.getS("Ligne");
      case 1:
        return "N� " + EbliLib.getS("Point");
      case 2:
        return EbliLib.getS("X");
      case 3:
        return EbliLib.getS("Y");
      default:
      }
      return att_[_column - 4].getName();
    }

    @Override
    public int getRowCount() {
      return nb_;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      final int n = nbLineAtoms_.length;
      int shiftL = 0;
      int idxLine = -1;
      int idxAtom = -1;
      for (int i = 0; i < n && idxLine < 0; i++) {
        if (_rowIndex < (shiftL + nbLineAtoms_[i])) {
          idxLine = i;
          idxAtom = _rowIndex - shiftL;
        }
        shiftL += nbLineAtoms_[i];
      }
      if (idxLine < 0) {
        FuLog.error("EZM: index in table not found");
        return CtuluLibString.EMPTY_STRING;
      }
      if (_columnIndex == 0) {
        return new Integer(idxLine + 1);
      }
      if (_columnIndex == 1) {
        return new Integer(idxAtom + 1);
      }
      final LineString s = (LineString) ligne_.getGeometry(idxLine);
      if (_columnIndex == 2) {
        return s.getCoordinateSequence().getX(idxAtom);
      }
      if (_columnIndex == 3) {
        return s.getCoordinateSequence().getY(idxAtom);
      }
      final CtuluCollection model = (CtuluCollection) ligne_.getModel(att_[_columnIndex - 4]).getObjectValueAt(idxLine);
      return model.getObjectValueAt(idxAtom);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id$
   */
  public static class LignesTableModel extends AbstractTableModel {

    GISAttributeInterface[] att_;

    boolean fermeOuvert_;

    final GISZoneCollectionLigneBrisee ligne_;

    public LignesTableModel(final boolean _fermeOuvert, final GISZoneCollectionLigneBrisee _ligne) {
      ligne_ = _ligne;
      fermeOuvert_ = _fermeOuvert;
      final List r = new ArrayList();
      for (int i = 0; i < ligne_.getNbAttributes(); i++) {
        if (ligne_.getAttribute(i).isUserVisible() && !ligne_.getAttribute(i).isAtomicValue()) {
          r.add(ligne_.getAttribute(i));
        }
      }
      att_ = new GISAttributeInterface[r.size()];
      r.toArray(att_);
    }

    @Override
    public int getColumnCount() {
      return (fermeOuvert_ ? 4 : 3) + att_.length;
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
      case 0:
        return "N�";
      case 1:
        return EbliLib.getS("Nombre de points");
      case 2:
        return EbliLib.getS("Valide");
      default:
      }
      if (fermeOuvert_ && _column == 3) {
        return EbliLib.getS("Ferm�/Ouvert");
      }
      final int r = _column - (fermeOuvert_ ? 4 : 3);
      return att_[r].getLongName();
    }

    @Override
    public int getRowCount() {
      return ligne_.getNumGeometries();
    }

    final String ferme_ = EbliLib.getS("Ferm�");
    final String ouvert_ = EbliLib.getS("Ouvert");
    final String valide_ = EbliLib.getS("Valide");
    final String nonValide_ = EbliLib.getS("Non Valide");

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return new Integer(_rowIndex + 1);
      } else if (_columnIndex == 1) {
        final LineString s = (LineString) ligne_.getGeometry(_rowIndex);
        if (s.isClosed()) {
          return new Integer(s.getNumPoints() - 1);
        }
        return new Integer(s.getNumPoints());
      } else if (_columnIndex == 2) {
        final LineString s = (LineString) ligne_.getGeometry(_rowIndex);
        return s.isValid() ? valide_ : nonValide_;

      } else if (fermeOuvert_ && _columnIndex == 3) {
        final LineString s = (LineString) ligne_.getGeometry(_rowIndex);
        return s.isClosed() ? ferme_ : ouvert_;
      }
      return ligne_.getModel(att_[_columnIndex - (fermeOuvert_ ? 4 : 3)]).getObjectValueAt(_rowIndex);
    }
  }

  public ZModeleLigneBriseeDefault() {
    this(new GISZoneCollectionLigneBrisee());
  }

  /**
   * @param _zone la zone: non copiee
   */
  public ZModeleLigneBriseeDefault(final GISZoneCollectionLigneBrisee _zone) {
    super(_zone);
  }

  @Override
  public final boolean containsPolygone() {
    return geometries_ == null ? false : getGeomData().containsPolygone();
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable r = new CtuluTable();
    //TODO Voir si l'action se fait qu'en mode atomique (comme mnt) ou dans tout mode diff�rent du mode normal.
//    if (((ZCalqueLigneBrisee) _layer).isAtomicMode()) {
    if (((ZCalqueLigneBrisee) _layer).getSelectionMode() == SelectionMode.ATOMIC) {
      r.setModel(new AtomesTableModel(getGeomData()));
    } else {
      r.setModel(new LignesTableModel(true, getGeomData()));
    }
    return r;
  }

  public GISAttributeInterface getAttribute(final int _i) {
    return geometries_.getAttribute(_i);
  }

  public int getAttributeNb() {
    return geometries_.getNbAttributes();
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    // Selection vide : information sur le calque
    if (_layer.isSelectionEmpty())
      _d.put(EbliLib.getS("Nombre de lignes"), CtuluLibString.getString(getNombre()));
    
    // Selection non vide : information relative � la selection
    else {
      final ZCalqueLigneBrisee layer=(ZCalqueLigneBrisee)_layer;

      // En mode atomique : Info sur les sommets.
      if (layer.getSelectionMode() == SelectionMode.ATOMIC) {
        final int nb=layer.getLayerSelectionMulti().getNbListSelected();
        _d.put(EbliLib.getS("Nombre de lignes s�lectionn�es"), CtuluLibString.getString(nb)+'/'+getNombre());
        super.fillWithInfo(_d, _layer);

        // Une seule g�om�trie support.
        if (nb==1) {
          final TIntObjectIterator it=layer.getLayerSelectionMulti().getIterator();
          it.advance();
          final int idxPoly=it.key();

          // Si la g�om�trie poss�de un nom.
          int attName=layer.modeleDonnees().getGeomData().getIndiceOf(GISAttributeConstants.TITRE);
          if (attName!=-1) {
            _d.put(EbliLib.getS("Nom de la ligne"), 
                (String)layer.modeleDonnees().getGeomData().getValue(attName, idxPoly));
          }

          final CtuluListSelectionInterface list=(CtuluListSelectionInterface)it.value();
          final int nbPoint=list.getNbSelectedIndex();
          _d.put(EbliLib.getS("Nombre de sommets s�lectionn�s"), CtuluLibString.getString(nbPoint)+'/'
              +getGeomData().getGeometry(idxPoly).getNumPoints());

          // Un seul point.
          if (nbPoint==1) {
            final int idxPt=list.getMaxIndex();
            _d.put(EbliLib.getS("Indice du sommet s�lectionn�"), CtuluLibString.getString(idxPt+1));
            final Coordinate c=((LineString)getGeomData().getGeometry(idxPoly)).getCoordinateSequence().getCoordinate(idxPt);
            _d.put("X:", Double.toString(c.x));
            _d.put("Y:", Double.toString(c.y));
            fillWithAtomicInfo(idxPoly, idxPt, _d);
          }
        }
      }

      // En mode global, info sur les g�om�tries.
      else if (layer.getSelectionMode() == SelectionMode.NORMAL) {
        final int nb=layer.getLayerSelection().getNbSelectedIndex();
        _d.put(EbliLib.getS("Nombre de lignes s�lectionn�es"), CtuluLibString.getString(nb)+'/'+getNombre());
        super.fillWithInfo(_d, _layer);
        if (nb==1) {
          final int idxPoly=layer.getLayerSelection().getMaxIndex();
          if ((_layer instanceof ZCalqueEditable)&&((ZCalqueEditable)_layer).canAddForme(DeForme.LIGNE_BRISEE)&&((ZCalqueEditable)_layer).canAddForme(DeForme.POLYGONE))
            _d.put(EbliLib.getS("Ferm�"), isGeometryFermee(idxPoly) ? CtuluLib.getS("vrai"):CtuluLib.getS("faux"),
                new CtuluValueEditorChoice(new String[]{CtuluLib.getS("vrai"), CtuluLib.getS("faux")}, null), null, this,
                new int[]{idxPoly});
          else
            _d.put(EbliLib.getS("Ferm�"), isGeometryFermee(idxPoly) ? CtuluLib.getS("vrai"):CtuluLib.getS("faux"));
          _d.put(CtuluLib.getS("Valide"), getGeomData().getGeometry(idxPoly).isValid() ? CtuluLib.getS("vrai"):CtuluLib
              .getS("faux"));
          fillWithInfo(idxPoly, _d);
        }
        else if (nb>1) {
          double length=0;
          double area=0;
          final int[] selectedIdx=layer.getLayerSelection().getSelectedIndex();
          // Propri�t� ferm�
          Object value;
          boolean sameValues=true;
          int j=0;
          while (sameValues&&++j<selectedIdx.length)
            sameValues=isGeometryFermee(selectedIdx[j])==isGeometryFermee(selectedIdx[j-1]);
          if (sameValues)
            value=isGeometryFermee(selectedIdx[0]) ? CtuluLib.getS("vrai"):CtuluLib.getS("faux");
          else
            value=null;
          if ((_layer instanceof ZCalqueEditable)&&((ZCalqueEditable)_layer).canAddForme(DeForme.LIGNE_BRISEE)&&((ZCalqueEditable)_layer).canAddForme(DeForme.POLYGONE))
            _d.put(EbliLib.getS("Ferm�"), value, new CtuluValueEditorChoice(new String[]{CtuluLib.getS("vrai"),
                CtuluLib.getS("faux")}, null), null, this, selectedIdx);
          else
            _d.put(EbliLib.getS("Ferm�"), value!=null ? value.toString():"<"+CtuluLib.getS("Mixte")+">");
          // Calcul de la longueur et de l'aire
          for (int i=0; i<selectedIdx.length; i++) {
            final LineString s=(LineString)getGeomData().getGeometry(selectedIdx[i]);
            length+=s.getLength();
            if (s.isClosed())
              area+=s.getArea();
          }
          _d.put(EbliLib.getS("Longueur cumul�e"), Double.toString(length));
          if (area>0)
            _d.put(EbliLib.getS("Surface cumul�e"), Double.toString(area));
        }
      }
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.palette.BPaletteInfo.ModifyPropertyInfo#modifyProperty(java.lang.String, java.lang.Object)
   */
  @Override
  public void modifyProperty(String _key, Object _value, final int[] _index, CtuluCommandContainer _cmd) {
    CtuluCommandComposite cmd=new CtuluCommandComposite();
    super.modifyProperty(_key, _value, _index, cmd);
    if (_index!=null&&_value!=null&&EbliLib.getS("Ferm�").equals(_key)) {
      // Modification de la propri�t�
      int[] indexPolylines=_index;
      for (int i=0; i<indexPolylines.length; i++) {
        int indexPolyligne=indexPolylines[i];
        // Cr�ation de la nouvelle g�om�trie
        Geometry newGeom=null;
        if (CtuluLib.getS("vrai").equals(_value))
          newGeom=GISLib.toPolygone(((GISCoordinateSequenceContainerInterface)geometries_.getGeometry(indexPolyligne)).getCoordinateSequence());
        else if (CtuluLib.getS("faux").equals(_value))
          newGeom=GISLib.toPolyligne(((GISCoordinateSequenceContainerInterface)geometries_.getGeometry(indexPolyligne)).getCoordinateSequence());
        else
          FuLog.error("ZModeleLigneBriseeDefault.modifyProperty : la valeur pour la propri�t� 'ferm�' est inconnue donc intraitable.");
        // Remplacement de l'ancienne g�om�trie par la nouvelle
        geometries_.setGeometry(indexPolyligne, newGeom, cmd);
      }
    }
    if(_cmd!=null)
      _cmd.addCmd(cmd.getSimplify());
  }
  
  
  @Override
  public final GISZoneCollectionLigneBrisee getGeomData() {
    return (GISZoneCollectionLigneBrisee)geometries_;
  }

  @Override
  public final int getNbPolyligne() {
    return getNombre()-getNbPolygone();
  }

  @Override
  public final int getNbPointForGeometry(final int _idxLigne) {
    if (geometries_ == null) {
      return 0;
    }
    final Geometry gi = geometries_.getGeometry(_idxLigne);
    if (gi.getNumPoints() == 0) {
      return 0;
    }

    final LineString line = (LineString) gi;
    if (line.isClosed()) {
      return line.getNumPoints() - 1;
    }
    return line.getNumPoints();
  }

  /**
   * @return le nombre de polygones
   */
  @Override
  public final int getNbPolygone() {
    return GISLib.getNbGeomOf(LinearRing.class, geometries_);
  }

  @Override
  public boolean isCoordinateValid(final CoordinateSequence _seq, final CtuluAnalyze _analyze) {
    return true;
  }
  
  @Override
  public boolean isDataValid(CoordinateSequence _seq, ZEditionAttributesDataI _data, CtuluAnalyze _ana) {
    return true;
  }
  
  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  protected void fillWithAtomicInfo(final int _idxLigne, final int _idxPt, final InfoData _d) {
    final GISZoneCollection model = getGeomData();
    final int nbAtt = model.getNbAttributes();
    for (int i = 0; i < nbAtt; i++) {
      if (model.getAttribute(i).isAtomicValue() && model.getDataModel(i) != null
          && ((GISAttributeModel) model.getDataModel(i).getObjectValueAt(_idxLigne)).getObjectValueAt(_idxPt) != null) {
        _d.put(model.getAttribute(i).getName(), ((GISAttributeModel) model.getDataModel(i).getObjectValueAt(_idxLigne))
            .getObjectValueAt(_idxPt).toString());
      }
    }

  }

  protected void fillWithInfo(final int _idxLigne, final InfoData _d) {
    final LineString s = (LineString) getGeomData().getGeometry(_idxLigne);
    _d.put(CtuluLib.getS("Longueur"), Double.toString(s.getLength()));
    if (s.isClosed()){
      _d.put(EbliLib.getS("Nombre de sommets"), CtuluLibString.getString(s.getNumPoints() - 1));
      _d.put(CtuluLib.getS("Surface"), Double.toString(s.getArea()));
    }
    else
      _d.put(EbliLib.getS("Nombre de sommets"), CtuluLibString.getString(s.getNumPoints()));
  }
}
