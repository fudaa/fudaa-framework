/*
 * @creation 10 nov. 06
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.beans.PropertyChangeListener;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.controle.BSelecteurTextField;

/**
 * @author fred deniger
 * @version $Id: BCalqueSelecteurTitleVisibleTarget.java,v 1.3 2007-05-04 13:49:42 deniger Exp $
 */
public class BCalqueSelecteurTitleVisibleTarget implements BSelecteurTargetInterface {

  final BCalque target_;

  public BCalqueSelecteurTitleVisibleTarget(final BCalque _target) {
    super();
    target_ = _target;
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.addPropertyChangeListener(_key, _l);

  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == BSelecteurTextField.TITLE_PROPERTY) {
      return target_.getTitle();
    }
    return Boolean.valueOf(target_.isVisible());
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.removePropertyChangeListener(_key, _l);

  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (_newProp == null) {
      return false;
    }
    if (_key == BSelecteurTextField.TITLE_PROPERTY) {
      return target_.setTitle((String) _newProp);
    }
    target_.setVisible(((Boolean) _newProp).booleanValue());
    return true;
  }
}
