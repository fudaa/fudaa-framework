/*
 * @creation 1 sept. 06
 * @modification $Date: 2008-04-01 07:20:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author fred deniger
 * @version $Id: BCalquePersistenceGroupe.java,v 1.5.8.1 2008-04-01 07:20:53 bmarchan Exp $
 */
public class BCalquePersistenceGroupe extends BCalquePersistenceSingle {

  transient boolean top_;

  public boolean isTop() {
    return top_;
  }

  public static boolean isNoAddLayer(final CtuluArkLoader _loader) {
    return (CtuluLibString.toBoolean(_loader.getOption(BCalquePersistenceGroupe.getNoLayerOpt())));
  }

  @Override
  public BCalqueSaverInterface save(final BCalque _cq, final ProgressionInterface _prog) {
    final BGroupeCalque cqGr = (BGroupeCalque) _cq;
    final BCalqueSaverSingle res = new BCalqueSaverSingle(cqGr);
    res.setPersistenceClassFrom(this);
    final BCalque[] cq = cqGr.getCalques();
    final int nb = cq == null ? 0 : cq.length;
    final BCalqueSaverInterface[] childUI = new BCalqueSaverInterface[nb];
    if (cq != null) {
      for (int i = 0; i < nb; i++) {
        childUI[i] = cq[i].getPersistenceMng().save(cq[i], _prog);
      }
    }
    res.getUI().put(getChildUiName(), childUI);
    return res;

  }

  protected void restoreAndSordChild(final BCalque _target, final BCalqueSaverInterface _saver,
      final BCalqueSaverTargetInterface _pn, final BCalque _parent, final ProgressionInterface _prog) {
    final BCalqueSaverInterface[] childUI = (BCalqueSaverInterface[]) _saver.getUI().get(getChildUiName());
    if (childUI != null && childUI.length > 0) {
      final String[] name = restoreChilds(childUI, _pn, (BGroupeCalque) _target, _prog);
      final boolean noAdd = _saver.getUI().getBoolean(getNoLayerOpt());
      // pas d'ajout donc pas de tri
      if (noAdd) {
        return;
      }
      sortLayer(_target, name);
    }

  }

  @Override
  public String restore(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent, final ProgressionInterface _prog) {
    final BCalque target = findCalque(_saver, _pn, _parent);
    if (target == null) {
      return null;
    }
    target.initFrom(_saver.getUI());
// B.M. Les fils sont restitu�s plus tard. A priori redondant, et provoque des pbs.
    //Fred ils ne sont pas restitu�s plus tard et c'est la seul methode qui permet de le retrier dans l'ordre sauvegard�
    restoreAndSordChild(target, _saver, _pn, _parent, _prog);
    return target.getName();
  }

  @Override
  public BCalqueSaverInterface saveIn(final BCalque _cqToSave, final CtuluArkSaver _saver, final String _parentEntry,
      final String _indiceDirName) {
    if (_saver == null) {
      return null;
    }
    String entryBase = null;
    BCalqueSaverInterface single = null;
    // pour le calque de base on enregistre juste le nom
    if (top_) {
      single = new BCalqueSaverSingle();
      single.getUI().setLayerName(_cqToSave.getName());
    } else {
      single = super.saveIn(_cqToSave, _saver, _parentEntry, _indiceDirName);
      entryBase = getEntryBase(_cqToSave, _parentEntry, single.getId());
      // writeLayerInXmlEntry(_cqToSave, _saver, entryBase);
    }
    final BGroupeCalque cqGr = (BGroupeCalque) _cqToSave;
    final BCalque[] cq = cqGr.getCalques();
    final int nb = cq == null ? 0 : cq.length;
    final BCalqueSaverInterface[] childUI = new BCalqueSaverInterface[nb];
    if (cq != null) {
      final String newDir = entryBase == null ? _parentEntry : (entryBase + '/');
      for (int i = 0; i < nb; i++) {
        childUI[i] = cq[i].getPersistenceMng().saveIn(cq[i], _saver, newDir, _indiceDirName);
      }
    }
    single.getUI().put(getChildUiName(), childUI);
    return single;
  }

  public static String getChildUiName() {
    return "group.childs";
  }

  public void setTop(final boolean _top) {
    top_ = _top;
  }

  /**
   * Retourne l'interface de persistence du calque de saver donn�.
   * @param _saver Le saver pour le calque.
   * @return L'interface de persistence.
   */
  protected BCalquePersistenceInterface findPersistence(final BCalqueSaverInterface _saver) {
    return findPersistence(_saver.getPersistenceClass());

  }

  /**
   * Retourne une instance de persistence de classe donn�e.
   * @param _classSaver Le nom de la classe de persistence.
   * @return L'instance de persistence. Si la classe n'est pas trouv�e, la classe {@link BCalquePersistenceSingle} 
   * est utilis�e.
   */
  protected BCalquePersistenceInterface findPersistence(final String _classSaver) {
    if (_classSaver != null) {
      try {
        return (BCalquePersistenceInterface) Class.forName(_classSaver).newInstance();
      } catch (final InstantiationException _evt) {
        FuLog.error(_evt);

      } catch (final IllegalAccessException _evt) {
        FuLog.error(_evt);

      } catch (final ClassNotFoundException _evt) {
        FuLog.error(_evt);

      }
    }
    return new BCalquePersistenceSingle();

  }

  public String[] findChildInArk(final CtuluArkLoader _loader, final String _dir) {
    final String[] entries = _loader.getEntries(_dir);
    if (CtuluLibArray.isEmpty(entries)) {
      return null;
    }
    final Set entry = new HashSet(entries.length);
    final int length = CtuluArkSaver.DESC_EXT.length();
    for (int i = 0; i < entries.length; i++) {
      if (entries[i].endsWith(CtuluArkSaver.DESC_EXT)) {
        final String name = entries[i].substring(0, entries[i].length() - length);
        if (_loader.isEntryFound(_dir, getLayerPropEntry(name))) {
          entry.add(name);
        }
      }
    }
    final String[] rs = (String[]) entry.toArray(new String[entry.size()]);
    Arrays.sort(rs);
    return rs;
  }

  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent) {
    // On essaye de r�cup�rer le groupe s'il existe par d�faut dans la hierarchie.
    BGroupeCalque cq=(BGroupeCalque)super.findCalque(_cqName, _pn, _parent);
    if (cq!=null) return cq;
    
    // Le groupe n'a pas �t� trouv�, on le construit.
    cq=new BGroupeCalque();
    cq.setName(BGroupeCalque.findUniqueChildName(_parent));
    _parent.add(cq);
    return cq;
  }

  @Override
  public String restoreFrom(final CtuluArkLoader _loader, final BCalqueSaverTargetInterface _parentPanel,
      final BCalque _parentCalque, final String _parentDirEntry, final String _entryName,
      final ProgressionInterface _proj) {
    // on reinitialise le groupe de calque
    String name = null;
    BCalque thisCalque = null;
    // dir est l'endroit ou le groupe doit chercher ses fils
    String dir = _parentDirEntry;
    // si c'est le calque initiale, on fait tout a partir de lui
    if (isTop()) {
      name = _parentCalque.getName();
      thisCalque = _parentCalque;
      dir = _parentDirEntry;
      // sinon, on restore deja le groupe et on met a jour le repertoire contenant les fils.
    } else {
      name = super.restoreFrom(_loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
      thisCalque = _parentCalque.getCalqueParNom(name);
      dir = _parentDirEntry + _entryName + '/';
    }
    if (thisCalque == null) {
      return null;
    }
    // on retrouve le groupe ( trouve ou recr�e par findCalque)
    // on recupere toutes les entr�es pour ce groupe qui ont une entree desc.xml et .props
    final String[] subEntries = findChildInArk(_loader, dir);
    if (subEntries != null) {
      final List added = new ArrayList(subEntries.length);
      // on restaure chaque entree.
      for (int i = 0; i < subEntries.length; i++) {
        final InputStream str = _loader.getReader(dir, getLayerPropEntry(subEntries[i]));
        // on recupere la classe a utilisee pour la persistence. Issu du fichier .props
        String clazz = null;
        if (str != null) {
          final Properties prop = new Properties();

          try {
            prop.load(str);
            clazz = prop.getProperty(BCalqueSaverSingle.getPersistanceClassId());
          } catch (final IOException _evt) {
            FuLog.error(_evt);

          }
        }

        // la classe est non null:on peut appeler la classe qui va reconstruire les fils
        final BCalquePersistenceInterface pers = findPersistence(clazz);
        if (pers != null) {
          added.add(pers.restoreFrom(_loader, _parentPanel, thisCalque, dir, subEntries[i], _proj));
        }

      }
      // on trie seulement si des calques sont ajoutes.
      // si l'option getNoLayerOpt est activ� aucun calque n'a du etre ajoute
      if (!CtuluLibString.toBoolean(_loader.getOption(getNoLayerOpt()))) {
        // on trie les entr�es
        sortLayer(thisCalque, (String[]) added.toArray(new String[added.size()]));
      }
    }
    return name;
  }

  protected String[] restoreChilds(final BCalqueSaverInterface[] _childUI, final BCalqueSaverTargetInterface _pn,
      final BGroupeCalque _cqParent, final ProgressionInterface _prog) {
    if (_childUI != null && _childUI.length > 0) {
      final String[] name = new String[_childUI.length];
      for (int i = 0; i < _childUI.length; i++) {
        final BCalqueSaverInterface saver = _childUI[i];
        final BCalquePersistenceInterface persist = findPersistence(saver);
        if (persist != null && saver != null) {
          name[i] = persist.restore(saver, _pn, _cqParent, _prog);
        }
      }
      return name;
    }
    return null;
  }

  public static String getNoLayerOpt() {
    return "option.noLayer";
  }

  /**
   * @param _groupLayer le calque pere
   * @param _childs les noms des calques fils dans l'ordre voulu
   */
  public static void sortLayer(final BCalque _groupLayer, final String[] _childs) {
    if (_childs == null || _groupLayer == null || _childs.length == 0) {
      return;
    }
    final BCalque[] existantCqs = _groupLayer.getCalques();
    if (existantCqs == null) {
      return;
    }
    final String[] oldName = new String[existantCqs.length];
    for (int i = oldName.length - 1; i >= 0; i--) {
      oldName[i] = existantCqs[i].getName();
    }
    if (Arrays.equals(oldName, _childs)) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("ECA: on evite de tout refaire");
      }
      return;
    }
    final List newcq = new ArrayList(existantCqs.length);
    for (int i = 0; i < _childs.length; i++) {
      final BCalque cqi = BCalque.findCalqueByName(existantCqs, _childs[i]);
      if (cqi != null) {
        newcq.add(cqi);
      }
    }
    for (int i = 0; i < existantCqs.length; i++) {
      if (!newcq.contains(existantCqs[i])) {
        newcq.add(existantCqs[i]);
      }
    }
    if (newcq.size() == existantCqs.length) {
      _groupLayer.removeAll();
      final int nb = newcq.size();
      for (int i = 0; i < nb; i++) {
        _groupLayer.add((BCalque) newcq.get(i));
      }
    }

  }
}
