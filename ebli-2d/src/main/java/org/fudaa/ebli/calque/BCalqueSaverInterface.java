/*
 * @creation     25 ao�t 2005
 * @modification $Date: 2008-01-22 09:57:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.commun.EbliUIProperties;

/**
 * Une interface permettant de transporter les informations sur un calque lors des op�rations de
 * sauvegarde/restitution de celui ci sur/depuis le fichier projet.
 *
 * @author Fred Deniger
 * @version $Id: BCalqueSaverInterface.java,v 1.3.8.1 2008-01-22 09:57:06 bmarchan Exp $
 */
public interface BCalqueSaverInterface {
  
  /**
   * Retourne le nom complet de la classe a utiliser pour reconstruire le calque.
   * @return Si null, la classe BCalquePersistenceSingle sera utilisee.
   */
  String getPersistenceClass();

  /**
   * Retourne les propri�t�s pour le calque. On peut y trouver le nom interne du calque, le nom donn� par
   * l'utilisateur, d'autres propri�t�s temporaires, telles des geometries.
   * @return Les propri�t�s.
   */
  EbliUIProperties getUI();

  /**
   * Retourne l'identifiant associ� au calque sous la forme "0xx-".
   * @return null Si aucun identifiant cree.
   */
  String getId();

  /**
   * Retourne le nom interne du calque associ�.
   * @return le nom interne.
   */
  String getLayerName();

}
