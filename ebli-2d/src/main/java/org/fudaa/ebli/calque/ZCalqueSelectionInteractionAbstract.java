/*
 * @creation 2002-10-02
 * @modification $Date: 2008-05-13 12:10:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.commun.EbliEnum;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * @version $Id$
 * @author Bertrand Marchand, Fred Deniger
 */
public abstract class ZCalqueSelectionInteractionAbstract extends BCalqueInteraction implements MouseListener, ZCatchListener,
        MouseMotionListener, KeyListener, ZCalqueAffichageInterface {

  public static final int DEFAULT_TOLERANCE_PIXEL = 4;

  // Modes de s�lection
  /**
   * Mode de s�lection par simple clic.
   */
  public static final class SelectionMode extends EbliEnum {

    /**
     * @param _s le nom du mode
     */
    public SelectionMode(final String _s) {
      super(_s);
    }
  }
  final ZSelectionTrace trace_ = new ZSelectionTrace();
  /**
   * Mode de s�lection par un polygone. Les objets enti�rement dans le rectangle sont s�lectionn�s.
   */
  public final transient static SelectionMode POLYGONE = new SelectionMode(EbliLib.getS("S�lection polygoniale"));
  /**
   * Methode de selection ponctuelle. N'est plus utilis�e. Remplac� par selection rectangulaire, qui fait tout.
   *
   * @deprecated
   */
  public final transient static SelectionMode PONCTUEL = new SelectionMode(EbliLib.getS("S�lection ponctuelle"));
  /**
   * Mode de s�lection standard � l'aide d'un rectangle. Les objets enti�rement dans le rectangle sont s�lectionn�s. Si le rectangle est reduit a 1
   * point, une selection ponctuelle est effectu�e.
   */
  public final transient static SelectionMode RECTANGLE = new SelectionMode(EbliLib.getS("S�lection standard"));
  /**
   * S�lection en cours.
   */
  protected boolean enCours_;
  /**
   * Par defaut, le double click lance l'edition de la s�lection.
   */
  private boolean editWhenDblClick_ = true;
  /**
   * Liste des points saisis en espace r�el.
   */
  VecteurGrPoint listePoints_;
  /**
   * Mode de s�lection courant.
   */
  transient SelectionMode mode_;
  /**
   * Modificateur de s�lection.
   */
  EbliSelectionState modificateur_;
  /**
   * Polyligne d'aide pour le trac�. Elle contient les points saisis..
   */
  GrPolyligne plHelper_;
  /**
   * Point d'extr�mit� du trac� courant.
   */
  private GrPoint ptExt_;
  /**
   * Point d'origine du trac� courant.
   */
  private GrPoint ptOrig_;
  /**
   * La g�om�trie d'accrochage
   */
  protected Integer idxGeomAccroch_;
  /**
   * Le sommet d'accrochage
   */
  protected Integer idxVertexAccroch_;
  /**
   * L'accrochage est actif
   */
  protected boolean isCatchingEnabled_ = true;
  /**
   * Trace dans le contexte graphique en espace r�el..
   */
  private TraceGeometrie tg_;
  // Modificateurs de s�lection
  /**
   * Distance de tol�rance pour la s�lection en pixels.
   */
  int tolerancePixel_;
  /**
   * Type de trait de s�lection.
   */
  private int typeTraitCourant_;
  /**
   * La scene
   */
  private ZScene scene_ = null;
  /**
   * L'editeur par defaut (pour la commande Edit notamment)
   */
  protected ZEditorDefault editor_ = null;

  public final ZSelectionTrace getTrace() {
    return trace_;
  }

  @Override
  public void paintAllInImage(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    if (isVisible()) {
      paintDonnees(_g, _versEcran, _versReel, _clipReel);
    }
  }

  public int getTolerancePixel() {
    return tolerancePixel_;
  }
  final BGroupeCalque donnees_;

  /**
   * Cr�ation d'un calque de s�lection sans objets s�lectionnables.
   */
  public ZCalqueSelectionInteractionAbstract(final BGroupeCalque _donnees) {
    super();
    donnees_ = _donnees;
    setDestructible(false);
    setTypeTrait(TraceLigne.LISSE);
    setModeSelection(RECTANGLE);
    // selectionListeners_ = Collections.synchronizedSet(new HashSet());
    // Les acces au listener sont synchro
    listePoints_ = new VecteurGrPoint();
    plHelper_ = new GrPolyligne();
    plHelper_.sommets_ = listePoints_;
    enCours_ = false;
    tolerancePixel_ = DEFAULT_TOLERANCE_PIXEL;
    modificateur_ = new EbliSelectionState();
    modificateur_.setXor(true);
  }

  @Override
  public Cursor getSpecificCursor() {
    return new Cursor(Cursor.DEFAULT_CURSOR);
  }

  /**
   * Recupere les donnees.
   */
  protected abstract void formeSaisie();

  @Override
  public final void paintComponent(final Graphics _g) {
    paintDonnees((Graphics2D) _g, getVersEcran(), getVersReel(), getClipReel(_g));
    if (enCours_) {
      donnees_.setUseCache(true);
      _g.setXORMode(Color.white);
      initTrace(_g);
      if (mode_ == RECTANGLE) {
        tg_.dessineRectangle((Graphics2D) _g, ptOrig_, ptExt_, false);
      } else if (mode_ == POLYGONE) {
        if (plHelper_.nombre() < 3) {
          tg_.dessinePolyligne((Graphics2D) _g, plHelper_, false);
        } else {
          tg_.dessinePolygone((Graphics2D) _g, plHelper_, false, false);
        }
      }
    } else {
      donnees_.setUseCache(false);
    }
  }

  //
  // Appel� avant chaque trac� pour d�finir le contexte graphique
  //
  private void initTrace(final Graphics _g) {

    if (tg_ == null) {
      tg_ = new TraceGeometrie(GrMorphisme.identite());
    }
    tg_.setForeground(Color.black);
    tg_.setTypeTrait(typeTraitCourant_);
  }

  // private String controleDesc_;
  private void majControleDesc(final KeyEvent _e) {
    final String old = modificateur_.getControleDesc();
    modificateur_.majControleDesc(_e);
    if (modificateur_.getControleDesc() != old) {
      firePropertyChange(ZEbliCalquePanelController.STATE, old, modificateur_.getControleDesc());
    }
  }

  private void majControleDesc(final MouseEvent _e) {
    final String old = modificateur_.getControleDesc();
    modificateur_.majControleDesc(_e);
    if (modificateur_.getControleDesc() != old) {
      firePropertyChange(ZEbliCalquePanelController.STATE, old, modificateur_.getControleDesc());
    }
  }

  // private void paintSelectionPath() {
  // final Graphics2D g = (Graphics2D) getGraphics();
  // if (enCours_) {
  // initTrace(g);
  // if (mode_ == RECTANGLE) {
  // tg_.dessineRectangle(g, ptOrig_, ptExt_, false);
  // } else if (mode_ == POLYGONE) {
  // if (plHelper_.nombre() < 3) {
  // tg_.dessinePolyligne(g, plHelper_, false);
  // } else {
  // tg_.dessinePolygone(g, plHelper_, false, false);
  // }
  // }
  // }
  // }
//  public abstract void addCalqueActif(ZCalqueAffichageDonneesInterface _calque);
  @Override
  public final boolean alwaysPaint() {
    return true;
  }

  /**
   * Efface les selections de tous les calques.
   */
  public abstract void clearSelections();

  @Override
  public final String getDescription() {
    return modificateur_.getControleDesc() == null ? mode_.getDesc() : mode_.getDesc()
            + modificateur_.getControleDesc();
  }

  /**
   * Retour du mode de s�lection courant ( <I>PONCTUEL, RECTANGLE, POLYGONE </I>).
   */
  public final SelectionMode getModeSelection() {
    return mode_;
  }

  /**
   * Accesseur de la propriete <I>typeTrait </I>. Elle fixe le type de trait (pointille, tirete, ...) en prenant ses valeurs dans les champs statiques
   * de <I>TraceLigne </I>.
   *
   * @see org.fudaa.ebli.trace.TraceLigne
   */
  public final int getTypeTrait() {
    return typeTraitCourant_;
  }

  @Override
  public final void keyPressed(final KeyEvent _e) {
    if (!isGele()) {
      majControleDesc(_e);
    }
  }

  @Override
  public final void keyReleased(final KeyEvent _e) {
    if (!isGele()) {
      majControleDesc(_e);
    }
  }

  @Override
  public final void keyTyped(final KeyEvent _e) {
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {
  }

  /**
   * Methode invoquee quand on deplace la souris avec un bouton appuye. Rectangle : Trac� du rectangle en cours de cr�ation Polygone : Trac� des 2
   * droites reliant le point courant et les 2 points extr�mit�s du polygone en cours de cr�ation
   */
  @Override
  public  void mouseDragged(final MouseEvent _evt) {
    if (!isOkEvent(_evt)) {
      return;
    }
    final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.);
    if (mode_ == RECTANGLE && enCours_) {
      // paintSelectionPath();
      ptExt_ = ptSo;
      repaint();
    }
  }

  //
  // Methode invoquee quand on sort du calque avec la souris.
  // Polygone : Effacement du polygone, pour acc�der aux boutons de navigation.
  //
  @Override
  public void mouseEntered(final MouseEvent _evt) {
  }

  //
  // Methode invoquee quand on entre dans le calque avec la souris.
  // Polygone : Trac� du polygone, apr�s avoir acc�d� aux boutons de navigation.
  //
  @Override
  public void mouseExited(final MouseEvent _evt) {
  }

  //
  // Methode invoquee quand on deplace la souris sans appuyer sur aucun bouton.
  // Polygone : Trac� des 2 droites reliant le point courant et les 2 points
  // extr�mit�s du polygone en cours de cr�ation
  //
  @Override
  public void mouseMoved(final MouseEvent _evt) {
    if (!isOkEvent(_evt)) {
      return;
    }
    if (mode_ == POLYGONE && enCours_) {
      // effacer ancien chemin
      // paintSelectionPath();
      final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.);
      listePoints_.remplace(ptSo, listePoints_.nombre() - 1);
      repaint();
    }
  }

  /*
   * Ev�nements souris Rectangle : <br> PRESSED => Debut de rectangle <br> DRAGGED => Trac� du rectangle <br> RELEASED
   * => Fin du rectangle Polygone : RELEASED => D�but de cr�ation ou nouveau point cr�� MOVED => Trac� des 2 droites
   * DRAGGED => M�me effet 2 RELEASED => Fin du polygone Button Mask : Rien => Remplacement des anciennes s�lections
   * SHIFT => Ajout des nouvelles s�lections, suppression si d�j� s�lectionn�
   */
  /**
   * Methode invoquee quand on appuie sur un bouton de la souris. Rectangle : D�but de cr�ation.
   */
  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (!isOkLeftEvent(_evt)) {
      return;
    }
    if (mode_ == RECTANGLE) {
      enCours_ = true;
      // initTrace();
      listePoints_.vide();
      ptOrig_ = new GrPoint(_evt.getX(), _evt.getY(), 0.);
      ptExt_ = ptOrig_;
      // tg_.dessineRectangle((Graphics2D) getGraphics(), ptOrig_, ptExt_, false);
      listePoints_.ajoute(ptOrig_);
      repaint();
    }
  }

  public boolean editionAsked() {
    return false;
  }

  public void setEditor(ZEditorDefault _editor) {
    editor_ = _editor;
  }

  /**
   * Methode invoquee quand on lache un bouton de la souris. <br>
   * Ponctuel : Saisie du seul point <br>
   * Rectangle : Fin de cr�ation <br>
   * Polygone : Saisie d'un point du polygone, d�but de cr�ation ou fin (si 2 released) <br>
   */
  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (!_evt.isControlDown() && !_evt.isAltDown() && !isOkLeftEvent(_evt)) {
      return;
    }
    majControleDesc(_evt);
    final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.);
    // Saisie d'un seul point
    if (mode_ == PONCTUEL) {
      if (_evt.getClickCount() >= 2 && editWhenDblClick_ && editionAsked()) {
        return;
      }
      listePoints_.vide();
      listePoints_.ajoute(ptSo);
      enCours_ = false;
      formeSaisie();
    } // Fin du rectangle en cours => Cr�ation d'un rectangle
    else if (mode_ == RECTANGLE) {
      if (_evt.getClickCount() >= 2 && editWhenDblClick_ && editionAsked()) {
        return;
      }
      if (enCours_) {
        // effacer ancien trace
        // paintSelectionPath();
        // Effacement du rectangle pr�c�dent
        listePoints_.ajoute(new GrPoint(ptSo.x_, ptOrig_.y_, 0.));
        listePoints_.ajoute(new GrPoint(ptSo.x_, ptSo.y_, 0.));
        listePoints_.ajoute(new GrPoint(ptOrig_.x_, ptSo.y_, 0.));
        enCours_ = false;
        formeSaisie();
      }
    } else if (mode_ == POLYGONE) {
      if (enCours_) {
        // fin de polygone => Creation du point de fin et du polygone
        if (_evt.getClickCount() == 2) {
          // Effacer ancien trace
          // paintSelectionPath();
          enCours_ = false;
          formeSaisie();
        } // Saisie d'un nouveau point
        else {
          // Effacer ancien trace
          // paintSelectionPath();
          listePoints_.ajoute(ptSo);
          repaint();
        }
      } // Premier point saisi
      else if (_evt.getClickCount() != 2) {
        enCours_ = true;
        listePoints_.vide();
        listePoints_.ajoute(ptSo);
        // Le 2ieme point correspond au point flottant avant clic.
        listePoints_.ajoute(ptSo);
        repaint();
      }
    }
  }

  /**
   * Dessin de l'icone.
   *
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null </I>. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public final void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final int w = getIconWidth() - 1;
    final int h = getIconHeight() - 1;
    Image i;
    if (mode_ == PONCTUEL) {
      i = EbliResource.EBLI.getImage("fleche");
    } else if (mode_ == RECTANGLE) {
      i = EbliResource.EBLI.getImage("rectangle");
    } else {
      i = EbliResource.EBLI.getImage("polygone");
    }
    if (i != null) {
      final int wi = i.getWidth(this);
      final int hi = i.getHeight(this);
      int r = Math.max(wi / w, hi / h);
      if (r == 0) {
        r = 1;
      }
      _g.drawImage(i, _x + 1, _y + 1, wi / r - 1, hi / r - 1, _c);
    }
  }

  // boolean special_;
//  public abstract void setCalqueActif(ZCalqueAffichageDonneesInterface _calque);
  /**
   * Affecte la scene pour la selection.
   *
   * @param _scene La scene.
   */
  public void setScene(ZScene _scene) {
    scene_ = _scene;
  }

  /**
   * ATTENTION : Ne doit etre appel�e que par les classes d�rivantes.
   *
   * @return _scene La scene. Ne devrait pas etre null.
   */
  protected ZScene internalGetScene() {
    return scene_;
  }

  /**
   * Affectation du mode de s�lection ( <I>PONCTUEL, RECTANGLE, POLYGONE </I>).
   */
  public final void setModeSelection(final SelectionMode _mode) {
    if (_mode != mode_) {
      final SelectionMode old = mode_;
      mode_ = _mode;
      firePropertyChange("mode", old, mode_);
    }
  }

  /**
   * Definit si le double clic lance l'edition de la selection.
   *
   * @param _b true Le double clic lance l'�dition de la s�lection.
   */
  public void setEditSelectionWhenDoubleClick(boolean _b) {
    editWhenDblClick_ = _b;
  }

  /**
   * @return true Si le double click lance l'�dition de la selection
   */
  public boolean isEditSelectionWhenDoubleClick() {
    return editWhenDblClick_;
  }

  /**
   * Affectation de la propriete <I>typeTrait </I>.
   */
  public final void setTypeTrait(final int _typeTrait) {
    typeTraitCourant_ = _typeTrait;
  }

  /**
   * Une m�thode pour ajouter les actions de selection a un menu.
   *
   * @param _menu Le menu auquel on ajoute les methodes de selection.
   * @param _enable Si true, les actions sont actives.
   */
  public static void addSelectionAction(final BuMenu _menu, final boolean _enable) {
    Action act;
    if ((act = EbliActionMap.getInstance().getAction("TOUTSELECTIONNER")) != null) {
      _menu.add(act);
    }
    if ((act = EbliActionMap.getInstance().getAction("INVERSESELECTION")) != null) {
      _menu.add(act);
    }
    if ((act = EbliActionMap.getInstance().getAction("CLEARSELECTION")) != null) {
      _menu.add(act);
    }
    if ((act = EbliActionMap.getInstance().getAction("RECHERCHER")) != null) {
      _menu.add(act);
    }

  }

  /**
   * Une m�thode pour ajouter les actions de selection a un menu.
   *
   * @param _menu Le menu auquel on ajoute les methodes de selection.
   * @param _l Le listener pour les actions.
   * @param _enable Si true, les actions sont actives.
   */
  public static void addSelectionAction(final BuMenu _menu, final boolean _enable, ActionListener _l) {
    BuMenuItem it = _menu.addMenuItem(BuResource.BU.getString("Tout s�lectionner"), "TOUTSELECTIONNER", false, KeyEvent.VK_A);
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }
    it = _menu.addMenuItem(CtuluLib.getS("Inverser la s�lection"), "INVERSESELECTION", BuResource.BU.getIcon("aucun"),
            false, KeyEvent.VK_I);
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }
    it = _menu.addMenuItem(CtuluLib.getS("Effacer la s�lection"), "CLEARSELECTION", BuResource.BU.getIcon("effacer"),
            false, 0);
    it.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }
    it = _menu.addMenuItem(BuResource.BU.getString("Rechercher..."), "RECHERCHER", false, KeyEvent.VK_F);
    it.setEnabled(_enable);
    if (_l != null) {
      it.addActionListener(_l);
    }
  }

  @Override
  public void catchChanged(ZCatchEvent _evt) {
    if (isGele()) {
      return;
    }

    if (_evt.type == ZCatchEvent.CAUGHT) {
      idxGeomAccroch_ = _evt.idxGeom;
      idxVertexAccroch_ = _evt.idxVertex;
    } else {
      idxGeomAccroch_ = null;
      idxVertexAccroch_ = null;
    }
  }

  @Override
  public boolean isCachingEnabled() {
    return !isGele() && isCatchingEnabled_;
  }

  /**
   * D�finit si l'accrochage est actif.
   *
   * @param _b True : L'accrochage sera actif sur le calque.
   * @see #isCachingEnabled()
   */
  public void setCatchingEnabled(boolean _b) {
    isCatchingEnabled_ = _b;
  }
}
