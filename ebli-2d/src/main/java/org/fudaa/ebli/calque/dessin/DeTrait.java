/*
 * @file         DeTrait.java
 * @creation     1998-08-31
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un trait.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class DeTrait extends DeForme {
  // donnees membres publiques
  // donnees membres privees
  GrPoint p1_, p2_;
  // Constructeurs
  public DeTrait() {
    super();
    p1_= new GrPoint();
    p2_= new GrPoint();
  }
  public DeTrait(final GrPoint _p1, final GrPoint _p2) {
    super();
    p1_= _p1;
    p2_= _p2;
  }
  // Methodes publiques
  @Override
  public GrObjet getGeometrie() {
    return new GrSegment(p1_, p2_);
  }
  @Override
  public int getForme() {
    return TRAIT;
  }
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
    _t.dessineTrait(_g2d,p1_, p2_, _rapide);
  }
  // >>> Interface GrContour ---------------------------------------------------
  /**
   * Retourne les points du contour de l'objet.
   *
   * @return Les points de contour.
   * @see    org.fudaa.ebli.geometrie.GrContour
   */
  @Override
  public GrPoint[] contour() {
    return new GrPoint[] { p1_, p2_ };
  }
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    return ((GrSegment)getGeometrie()).estSelectionne(_ecran, _dist, _pt);
  }
  // <<< Interface GrContour ---------------------------------------------------
  // >>> Implementation GrObjet pour �tre d�placable ---------------------------
  @Override
  public void autoApplique(final GrMorphisme _t) {
    p1_.autoApplique(_t);
    p2_.autoApplique(_t);
  }
  /**
   * Retourne la boite englobante de l'objet.
   */
  @Override
  public GrBoite boite() {
    final GrBoite r= new GrBoite();
    r.ajuste(p1_);
    r.ajuste(p2_);
    return r;
  }
  // <<< Implementation GrObjet ------------------------------------------------
}
