/**
 *  @creation     21 d�c. 2004
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.util.List;
import org.fudaa.ebli.geometrie.GrPoint;


/**
 * @author Fred Deniger
 * @version $Id: ZCalqueSondeInterface.java,v 1.5 2006-04-12 15:27:09 deniger Exp $
 */
public interface ZCalqueSondeInterface {
  
  /**
   * @return true si la sonde est active
   */
  boolean isSondeEnable();
  
  /**
   * @return true is sonde valide et si un point est sond�
   */
  boolean isSondeActive();
  
  /**
   * @return l'indice de l'element sonde
   */
  int getElementSonde();
  /**
   * @return le x du point de sonde
   */
  double getSondeX();
  /**
   * @return le y du point de sonde
   */
  double getSondeY();

  /**
   * @param _enable nouvel etat de l'outil sonde
   */
  void setSondeEnable(boolean _enable);
  
  /**
   * @param _reel coordonn�es r�elles
   * @return true si trouv�
   */
  boolean changeSonde(GrPoint _reel,boolean ajoute);
  
  
  /**
   * @param _reel coordonnees reelles du point a sonder
   * @return true si le point _reel est sondable
   */
  boolean isPointSondable(GrPoint _reel);
  
  /**
   * Retourne la liste des sondes qui forment une ligne brisee.
   * @return
   */
  public List<GrPoint> getLigneBriseeFromSondes();
  
  
  /**
   * Use to clear all sounds uses.
   */
  public void clearSonde();
  

}
