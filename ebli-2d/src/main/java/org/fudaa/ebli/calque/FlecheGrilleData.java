/*
 * @creation 21 mai 07
 * 
 * @modification $Date: 2007-06-05 08:58:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * @author fred deniger
 * @version $Id: FlecheGrilleData.java,v 1.2 2007-06-05 08:58:38 deniger Exp $
 */
public class FlecheGrilleData {

  /**
   * Nouvelle fonctionnalité pour la densite
   */
  public final static String PROP_DENSITE_IS_ACTIVE = "calquefleche.densite.active";
  /**
   * Nouvelle fonctionnalité pour indiquer le pourcentage de fleche a afficher
   */
  public final static String PROP_DENSITE_INCREMENT = "calquefleche.densite.increment";
  public final static String PROP_GRILLE_IS_ACTIVE = "calquefleche.grille.active";
  public final static String PROP_GRILLE_IS_PAINTED = "calquefleche.grille.painted";
  public final static String PROP_GRILLE_NB_X = "calquefleche.grille.x";
  public final static String PROP_GRILLE_NB_Y = "calquefleche.grille.y";

  boolean active_;
  boolean densiteActive_;
  int densiteIncrement = 1;
  ZModeleFlecheForGrille grilleFlecheModel_;
  ZModeleFlecheForDensite grilleDensiteModel_;
  int nbXLines_ = 10;
  int nbYLines_ = 10;

  ZModeleFleche old_;

  boolean paintGrille_;

  final ZCalqueFleche support_;

  public FlecheGrilleData(final ZCalqueFleche _support) {
    super();
    support_ = _support;
  }

  private void updateGrille() {
    support_.clearSelection();
    if (grilleFlecheModel_ != null)
      grilleFlecheModel_.clearCache();
  }

  public int getNbXLines() {
    return nbXLines_;
  }

  public int getNbYLines() {
    return nbYLines_;
  }

  public int getDensiteIncrement() {
    return densiteIncrement;
  }

  public double getX(GrBoite _b, int _idxX) {
    double pas = _b.getDeltaX() / (nbXLines_ - 1);
    return _b.getMinX() + pas * _idxX;
  }

  public double getY(GrBoite _b, int _idxY) {
    double pas = _b.getDeltaY() / (nbYLines_ - 1);
    return _b.getMinY() + pas * _idxY;
  }

  public void initFrom(final EbliUIProperties _p) {
    if (_p.isDefined(PROP_GRILLE_NB_X)) {
      nbXLines_ = _p.getInteger(PROP_GRILLE_NB_X);
    }
    if (_p.isDefined(PROP_GRILLE_NB_Y)) {
      nbYLines_ = _p.getInteger(PROP_GRILLE_NB_Y);
    }
    if (_p.isDefined(PROP_GRILLE_IS_PAINTED)) {
      paintGrille_ = _p.getBoolean(PROP_GRILLE_IS_PAINTED);
    }
    if (_p.isDefined(PROP_GRILLE_IS_ACTIVE)) {
      setActive(_p.getBoolean(PROP_GRILLE_IS_ACTIVE));
    }
    if (_p.isDefined(PROP_DENSITE_IS_ACTIVE)) {
      setDensiteActive(_p.getBoolean(PROP_DENSITE_IS_ACTIVE));
    }
    if (_p.isDefined(PROP_DENSITE_INCREMENT)) {
      setDensitePourcentage(_p.getInteger(PROP_DENSITE_INCREMENT));
    }
  }

  public boolean isActive() {
    return active_;
  }

  public boolean isDensiteActive() {
    return densiteActive_;
  }

  public boolean isPaintGrille() {
    return paintGrille_;
  }

  public void saveUIProperties(EbliUIProperties _cui) {
    _cui.put(PROP_GRILLE_NB_X, nbXLines_);
    _cui.put(PROP_GRILLE_NB_Y, nbYLines_);
    _cui.put(PROP_GRILLE_IS_ACTIVE, active_);
    _cui.put(PROP_GRILLE_IS_PAINTED, paintGrille_);
    _cui.put(PROP_DENSITE_IS_ACTIVE, densiteActive_);
    _cui.put(PROP_DENSITE_INCREMENT, densiteIncrement);
  }

  protected void modeleUpdated() {
    if (grilleFlecheModel_ != null) {
      grilleFlecheModel_ = null;
      if (isActive()) {
        initGrilleFlecheModel();
        support_.setGrilleOrDensiteModele(grilleFlecheModel_);

      }
    }
  }

  public boolean setDensiteActive(final boolean densiteActive) {
    if (densiteActive != this.densiteActive_) {
      this.densiteActive_ = densiteActive;
      if (densiteActive) {
        setActive(false);
        initGrilleDensiteModel();
        this.grilleDensiteModel_.updatePourcentage();
        support_.setGrilleOrDensiteModele(grilleDensiteModel_);
      } else {
        support_.setGrilleOrDensiteModele(old_);
      }
      support_.firePropertyChange(PROP_DENSITE_IS_ACTIVE, !densiteActive_, densiteActive_);
      support_.repaint();
      return true;
    }
    return false;

  }

  public boolean setActive(final boolean _active) {
    if (active_ != _active) {
      active_ = _active;
      if (!active_) {
        support_.setGrilleOrDensiteModele(old_);
      } else {
        setDensiteActive(false);
        if (grilleFlecheModel_ == null) {
          initGrilleFlecheModel();
        } else {
          grilleFlecheModel_.updateBoite(support_);
        }
        support_.setGrilleOrDensiteModele(grilleFlecheModel_);
      }
      support_.firePropertyChange(PROP_GRILLE_IS_ACTIVE, !active_, active_);
      return true;
    }
    return false;
  }

  protected void updateView() {
    if (grilleFlecheModel_ != null && isActive()) {
      grilleFlecheModel_.updateBoite(support_);
    }
    if (grilleDensiteModel_ != null && isDensiteActive()) {
      grilleDensiteModel_.updatePourcentage();
    }
  }

  private void initGrilleFlecheModel() {
    if (old_ == null) {
      old_ = (ZModeleFleche) support_.modeleDonnees();
    }
    grilleFlecheModel_ = new ZModeleFlecheForGrille(old_, this);
    grilleFlecheModel_.updateBoite(support_);
  }

  private void initGrilleDensiteModel() {
    if (old_ == null) {
      old_ = (ZModeleFleche) support_.modeleDonnees();
    }
    grilleDensiteModel_ = new ZModeleFlecheForDensite(old_, this);
  }

  public boolean setNbXLines(final int _nbXLines) {
    if (nbXLines_ != _nbXLines && _nbXLines > 1) {
      final int old = nbXLines_;
      nbXLines_ = _nbXLines;

      support_.firePropertyChange(PROP_GRILLE_NB_Y, old, nbXLines_);
      updateGrille();
      if (isActive())
        support_.repaint();
      return true;
    }
    return false;
  }

  public boolean setDensitePourcentage(final int pourc) {
    if (densiteIncrement != pourc && pourc >= 0 && pourc <= 100) {
      final int old = densiteIncrement;
      densiteIncrement = pourc;

      support_.firePropertyChange(PROP_DENSITE_INCREMENT, old, densiteIncrement);
      if (densiteActive_) {
        support_.repaint();
      }
      return true;
    }
    return false;
  }

  public boolean setNbYLines(final int _nbYLines) {
    if (nbYLines_ != _nbYLines && _nbYLines > 1) {
      final int old = nbYLines_;
      nbYLines_ = _nbYLines;
      support_.firePropertyChange(PROP_GRILLE_NB_Y, old, nbYLines_);
      updateGrille();
      if (isActive())
        support_.repaint();
      return true;
    }
    return false;
  }

  public boolean setPaintGrille(boolean _paintGrille) {
    if (paintGrille_ != _paintGrille) {
      paintGrille_ = _paintGrille;
      support_.firePropertyChange(PROP_GRILLE_IS_PAINTED, !active_, active_);
      support_.repaint();
      return true;
    }
    return false;
  }

  protected ZModeleFlecheForGrille getGrilleFlecheModel() {
    return grilleFlecheModel_;
  }

}
