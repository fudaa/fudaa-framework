/**
 * @file         BGroupeCalque.java
 * @creation     1998-08-27
 * @modification $Date: 2006-12-08 14:23:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * Un groupe de calques. Cette classe joue un role structurant. C'est un noeud dans la hierarchie arborescente des
 * calques.
 * 
 * @version $Id: BGroupeCalque.java,v 1.28 2006-12-08 14:23:42 deniger Exp $
 * @author Guillaume Desnoix
 */
public class BGroupeCalque extends BCalque {
  boolean isTitleModifiable_=false;
  
  public BGroupeCalque(String _name, String _title) {
    this();
    setName(_name);
    setTitle(_title);
  }
  
  public BGroupeCalque() {
    super();
    setDestructible(true);
  }

  @Override
  public BConfigurableInterface getSingleConfigureInterface() {
    return null;
  }

  @Override
  public final BConfigurableInterface[] getConfigureInterfaces() {
    final BCalque[] childs = getTousCalques();
    final List l = new ArrayList(childs.length);
    for (int i = 0; i < childs.length; i++) {
      if (!childs[i].isGroupeCalque()) {
        l.add(childs[i].getSingleConfigureInterface());
      }
    }
    return (BConfigurableInterface[]) l.toArray(new BConfigurableInterface[l.size()]);
  }

  @Override
  public void setForeground(final Color _v) {
    final BCalque[] cq = getCalques();
    if (cq != null) {
      for (int i = cq.length - 1; i >= 0; i--) {
        cq[i].setForeground(_v);
      }
    }
  }

  @Override
  public final void paintAllInImage(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if (isVisible()) {
      final BCalque[] cq = getCalques();
      for (int i = cq.length - 1; i >= 0; i--) {
        cq[i].paintAllInImage(_g, _versEcran, _versReel, _clipReel);
      }
    }
  }

  @Override
  public void paintImage(final Graphics _g) {
    if (isVisible()) {
      final BCalque[] cq = getCalques();
      for (int i = cq.length - 1; i >= 0; i--) {
        cq[i].paintImage(_g);
      }
    }
  }
  
  
  

  boolean useCache_;
  Image cache_;

  @Override
  public void paint(Graphics _g) {
    if (useCache_) {
      Graphics cg = null;
      if (cache_ == null) {
        try {
          HashMap param = new HashMap();
          CtuluLibImage.setCompatibleImageAsked(param);
          cache_ = CtuluLibImage.createImage(getWidth(), getHeight(), param);
          cg = cache_.getGraphics();
        } catch (final NullPointerException e) {
          cache_ = null;
          cg = _g;
        }
        super.paint(cg);
      }
      if (cg != _g && cache_ != null) {
        _g.drawImage(cache_, 0, 0, this);
      }

    } else super.paint(_g);
  }

  @Override
  public Color getCouleur() {
    final BCalque[] cq = getCalques();
    if (cq == null) { return null; }
    boolean init = false;
    Color res = null;
    for (int i = cq.length - 1; i >= 0; i--) {
      final BCalque calque = cq[i];
      if (calque != this) {
        final Color c = calque.getCouleur();
        if (c == null) {
          return null;
        } else if (!init) {
          res = c;
          init = true;
        } else if (!c.equals(res)) { return null; }
      }
    }
    return res;

  }

  @Override
  public void fillWithInfo(final InfoData _m) {
    if (_m != null) {
      final BCalque[] cqs = getCalques();
      _m.setTitle(EbliLib.getS("Calque: ") + getTitle());
      _m.put(EbliLib.getS("Nombre de calques"), cqs == null ? CtuluLibString.ZERO : CtuluLibString
          .getString(cqs.length));
    }

  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return getGroupePersistenceMng();
  }

  public BCalquePersistenceGroupe getGroupePersistenceMng() {
    return new BCalquePersistenceGroupe();
  }

  @Override
  public final boolean isGroupeCalque() {
    return true;
  }

  /**
   * Ne fait rien a voir si utile....
   */
  @Override
  public void addSelectionListener(final ZSelectionListener _l) {}

  /**
   * Ne fait rien a voir si utile....
   */
  @Override
  public void removeSelectionListener(final ZSelectionListener _l) {}

  // Icon
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    for (int i = 0; i <= 4; i += 2) {
      _g.setColor(Color.white);
      _g.fillRect(_x + i + 1, _y + i + 1, w - 6, h - 6);
      _g.setColor(isVisible() ? Color.black : Color.lightGray);
      _g.drawRect(_x + i, _y + i, w - 4, h - 4);
    }
  }

  // Informations
  /*
   * public void setAttenue(boolean b) { Component[] c=getComponents(); for(int i=0;i<c.length;i++) if(c[i] instanceof
   * BCalqueAffichage) ((BCalqueAffichage)c[i]).setAttenue(b); if(!isValid()) repaint(); }
   */
  /*
   * public void setRapide(boolean b) { Component[] c=getComponents(); for(int i=0;i<c.length;i++) if(c[i] instanceof
   * BCalqueAffichage) ((BCalqueAffichage)c[i]).setRapide(b); if(!isValid()) repaint(); }
   */

  /**
   * Recherche un nom unique sous la forme _pref+"_"+_idx parmis les fils du calque parent.
   * @param _pref le prefix pour le nom
   * @param _parent le calque parent
   * @return Le nom unique du style _pref+"_"+indice.
   */
  public static String findUniqueChildName(final BCalque _parent, final String _pref) {
    // FIXME BM : Propriete layer.child.id get mais child.id put !!! Pas de correspondance possible.
    Integer idxSt = (Integer) _parent.getClientProperty("layer.child.id");
    if (idxSt == null) {
      idxSt = new Integer(0);
    }
    final int idx = idxSt.intValue() + 1;
    _parent.putClientProperty("child.id", new Integer(idx));
    String pref = _parent.getName();
    if (_pref != null) {
      pref += '-' + _pref;
    }
    return findUniqueName(pref, _parent, idx);
  }

  public static String findUniqueChildName(final BCalque _parent) {
    return findUniqueChildName(_parent, null);
  }

  /**
   * Construit un nom de calque sous la forme _pref+"_"+_idx
   * @param _pref le prefix xxxx
   * @param _idx l'identifiant
   * @return Le nom du calque
   */
  private static String buildName(final String _pref, final int _idx) {
    return _pref + '_' + _idx;
  }

  /**
   * Retourne l'indice idx pour un nom de calque de la forme "xxxx_"+idx
   * @param _cqName le nom du calque
   * @return -1 si forme incorrect. Sinon, l'indice
   */
  private static int getIdx(final String _cqName) {
    final int idxTmp = _cqName.lastIndexOf('_');
    if (idxTmp < 0) { return -1; }
    try {
      return Integer.parseInt(_cqName.substring(idxTmp + 1));
    } catch (final Exception e) {
      return -1;
    }
  }

  /**
   * Recherche un nom unique sous la forme _pref+"_"+_idx parmis les fils du calque parent.
   * @param _pref le prefix pour le nom
   * @param _parent le calque parent
   * @param _idx L'indice initial
   * @return Le nom unique du style _pref+"_"+indice. Si le nom pass� est unique, il est retourn�
   * tel quel. Sinon, un nouvel indice est choisit.
   */
  private static String findUniqueName(final String _pref, final BCalque _parent, final int _idx) {
    final BCalque[] cqs = _parent.getCalques();
    final String defaultName=buildName(_pref, _idx);
    if (cqs == null || cqs.length == 0) { return defaultName; }
//    final String defaultName = _pref + '_' + CtuluLibString.getString(_idx);

    // Aucun calque trouv� avec ce nom.
    final String[] cqNames = new String[cqs.length];
    final int nb = cqNames.length;
    boolean found = false;
    for (int i = 0; i < nb; i++) {
      cqNames[i] = cqs[i].getName();
      if (defaultName.equals(cqNames[i])) {
        found = true;
      }
    }
    if (!found) { return defaultName; }
    
    // Un calque a �t� trouv� avec ce nom => On en definit un nouveau avec un indice diff�rent.
    int lastIdx = 0;
    String r = null;
    Arrays.sort(cqNames);
    for (int i = 0; i < nb && (r == null); i++) {
      lastIdx = BGroupeCalque.getIdx(cqNames[i]);
      FuLog.debug("TRL: test for i=" + i);
      if (lastIdx > i) {
        r = BGroupeCalque.buildName(_pref, i);
        if (Arrays.binarySearch(cqNames, r) >= 0) {
          r = null;
        }
      } else {
        lastIdx = i;
      }
    }
    if (r == null) {
      FuLog.debug("TRL: build spec name for " + (lastIdx + 1));
      r = BGroupeCalque.buildName(_pref, lastIdx + 1);
    }
    if (Arrays.binarySearch(cqNames, r) >= 0) {
      throw new IllegalArgumentException("cant find unique name " + r);
    }
    FuLog.debug("TRL: find unique name " + r);
    return r;
  }
  
  /**
   * @return the useCache
   */
  public boolean isUseCache() {
    return useCache_;
  }

  /**
   * @param _useCache the useCache to set
   */
  public void setUseCache(boolean _useCache) {
    useCache_ = _useCache;
    if (!_useCache && cache_ != null) {
      cache_.flush();
      cache_ = null;
    }
  }
  
  /**
   * Retourne un titre de calque qui n'existe pas d�j� parmis les fils du groupe de calques.
   * @param _rootTitle La racine du titre.
   * @return Le titre, sous la forme _title+" "+num
   */
  public String findUniqueChildTitle(String _rootTitle) {
    BCalque[] cqs=this.getCalques();
    int num=0;
    for (BCalque cq : cqs) {
      if (cq.getTitle().startsWith(_rootTitle)) {
        try {
          num=Math.max(num,Integer.parseInt(cq.getTitle().substring(_rootTitle.length()).trim()));
        }
        catch (NumberFormatException _exc) {}
      }
    }
    return _rootTitle+" "+(num+1);
  }
  
  /**
   * On force le mode visible dependant des sous calques, car les calques sont des components. 
   * Les components fils ne sont affich�s que si les components parents sont visibles.
   */
  @Override
  public boolean isVisible() {
    return getTousCalques().length==1 ? super.isVisible():isAllChildrenUnvisible() ? false:true;
  }
  
  /**
   * Met les sous calques en mode visible/invisible.<br>
   */
  @Override
  public void setVisible(boolean _b) {
    super.setVisible(_b);
    
    final BCalque[] c=getCalques();
    for (int i=0; i<c.length; i++) {
      c[i].setVisible(_b);
    }
  }
  
  @Override
  public boolean isTitleModifiable() {
    return isTitleModifiable_;
  }
  
  /**
   * Definit si le titre peut �tre modifi� depuis l'arbre des calques.
   * @param _b true : Il est modifiable.
   */
  @Override
  public void setTitleModifiable(boolean _b) {
    isTitleModifiable_=_b;
  }
}
