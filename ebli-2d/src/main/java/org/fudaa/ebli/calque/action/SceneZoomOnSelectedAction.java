package org.fudaa.ebli.calque.action;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action pour un zoom sur la sélection. Est active si des objets sont selectionnés.
 * @author Fred Deniger
 * @version $Id$
 */
public class SceneZoomOnSelectedAction extends EbliActionSimple implements ZSelectionListener {

  final ZEbliCalquesPanel cqPn_;

  public SceneZoomOnSelectedAction(final ZEbliCalquesPanel _pn) {
    super(EbliLib.getS("Centrer sur la sélection"), EbliResource.EBLI.getIcon("zoom-selection"),
        "ZOOM_ON_SELECTED");
    putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
    putValue(EbliActionInterface.SECOND_KEYSTROKE, KeyStroke.getKeyStroke(KeyEvent.VK_F5, InputEvent.CTRL_DOWN_MASK));
    setDefaultToolTip(EbliLib.getS("Centrer la vue sur la sélection courante"));
    cqPn_ = _pn;
    cqPn_.getScene().addSelectionListener(this);
  }

  /**
   * Effectue le zoom sur selection
   */
  @Override
  public void actionPerformed(final ActionEvent _arg) {
    cqPn_.getEditor().getSceneEditor().zoomOnSelected();
  }

  @Override
  public void updateStateBeforeShow() {
    super.setEnabled(cqPn_.isZoomOnSelectedEnable());
  }

  /**
   * Active le bouton de zoom sur selection.
   */
  private void updateEnableState() {
    setEnabled(!cqPn_.getEditor().getSceneEditor().getScene().isSelectionEmpty());
  }

  @Override
  public void selectionChanged(final ZSelectionEvent _evt) {
    updateEnableState();
  }

}