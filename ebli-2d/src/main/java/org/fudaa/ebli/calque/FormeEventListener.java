/*
 * @file         FormeEventListener.java
 * @creation     1998-12-15
 * @modification $Date: 2005-08-16 13:02:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.util.EventListener;
/**
 * Interface auditeur de l'evenement <I>forme</I>.
 *
 * @version      $Id: FormeEventListener.java,v 1.4 2005-08-16 13:02:10 deniger Exp $
 * @author       Bertrand Marchand 
 */
public interface FormeEventListener extends EventListener {
  void formeSaisie(FormeEvent _evt);
}
