/*
 * @creation 11 sept. 06
 * @modification $Date: 2008-05-13 12:10:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.commun.EbliActionSimple;

/**
 * Une action de selection atomique.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class CalqueSelectNormalSelectionAction extends EbliActionSimple {

  private final transient ZScene scene_;

  /**
   * Cr�ation de l'action.
   */
  public CalqueSelectNormalSelectionAction(final ZScene _scn) {
    super("MODE_NORMAL", null, "MODE_NORMAL");
    putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
    scene_ = _scn;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    scene_.setSelectionMode(SelectionMode.NORMAL);
  }
}