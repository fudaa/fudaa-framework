/*
 *  @creation     31 mars 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;

/**
 * Une interface mod�le manipul�e par un calque ZCalqueLigneBrisee.
 * @see org.fudaa.ebli.calque.ZCalqueLigneBrisee
 * @author Fred Deniger
 * @version $Id: ZModeleLigneBrisee.java,v 1.7.8.3 2008-03-26 16:46:43 bmarchan Exp $
 */
public interface ZModeleLigneBrisee extends ZModeleGeometry {

  @Override
  GISZoneCollectionLigneBrisee getGeomData();

  /**
   * Retourne le nombre de polylignes (ligne bris�e ouvertes) contenues dans le mod�le.
   * @return Le nombre de poylignes.
   */
  int getNbPolyligne();

  /**
   * Le mod�le contient-il des polygones (ligne bris�e ferm�e) ?
   * @return
   * <code>true</code> s'il existe au moins 1 polygone dans le mod�le.
   */
  boolean containsPolygone();

  /**
   * Retourne le nombre de polygones (ligne bris�e ferm�e) contenus dans le mod�le.
   * @return le nombre de polygone
   */
  int getNbPolygone();
}
