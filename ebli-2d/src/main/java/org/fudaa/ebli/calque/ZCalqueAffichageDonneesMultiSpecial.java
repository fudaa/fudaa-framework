/*
 * @file ZCalqueAffichageDonneesMultiSelectionSpecial.java
 * @creation 21 nov. 2003
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * WARN the modele must be a ZModelMulti.
 * @author deniger
 * @version $Id: ZCalqueAffichageDonneesMultiSpecial.java,v 1.16 2007-05-04 13:49:42 deniger Exp $
 */
public abstract class ZCalqueAffichageDonneesMultiSpecial extends ZCalqueAffichageDonneesMulti {


  
  public int[] isSelectionContiguous(final int _b) {
    return super.isSelectionContiguous(_b, getModeleMulti().getNbElementIn(_b));
  }

  @Override
  public void inverseSelection(){
    if (isSelectionEmpty()) {
      return;
    }
    final ZModeleDonneesMulti m = getModeleMulti();
    for (int i = m.getNombre() - 1; i >= 0; i--) {
      CtuluListSelection s = selection_.get(i);
      if (s == null) {
        s = new CtuluListSelection();
        s.setSelectionInterval(0, m.getNbElementIn(i) - 1);
        selection_.set(i, s);
      }
      else {
        s.inverse(m.getNbElementIn(i));
      }
    }
    fireSelectionEvent();
  }

  @Override
  public void selectAll(){
    if (!isVisible()) return;
    
    initSelection();
    final ZModeleDonneesMulti m = getModeleMulti();
    for (int i = m.getNombre() - 1; i >= 0; i--) {
      CtuluListSelection s = selection_.get(i);
      if (s == null) {
        s = new CtuluListSelection();
        selection_.set(i, s);
      }
      s.setSelectionInterval(0, m.getNbElementIn(i) - 1);
    }
    fireSelectionEvent();
  }

  protected ZModeleDonneesMulti getModeleMulti(){
    return (ZModeleDonneesMulti) modeleDonnees();
  }

  protected int getNbElementIn(final int _i){
    return getModeleMulti().getNbElementIn(_i);
  }

  @Override
  public CtuluListSelectionInterface getLayerSelection(){
    return null;
  }

  /**
   * Add the segment [_idx1,_idx2] to the selection _i.
   */
  protected void select(final int _i,final int _idx1,final int _idx2){
    final CtuluListSelection s = (CtuluListSelection) selection_.getSelection(_i);
    //normal selection
    if (_idx1 < _idx2) {
      s.addInterval(_idx1, _idx2);
    }
    //selection passing by 0
    else {
      s.addInterval(0, _idx2);
      s.addInterval(_idx1, getNbElementIn(_i) - 1);
    }
  }

  @Override
  public boolean isSpecialSelectionAllowed(){
    return true;
  }

  @Override
  public boolean changeSelection(final GrPoint _p,final int _tolerancePixel,final int _action){
    if (_action != EbliSelectionState.ACTION_SPECIAL) {
      return super.changeSelection(_p, _tolerancePixel, _action);
    }
    final int i = selection_.isSelectionInOneBloc();
    if (i >= 0) {
      final EbliListeSelectionMulti newS = selection(_p, _tolerancePixel);
      if (newS == null) {
        return false;
      }
      final CtuluListSelection s = selection_.get(i);
      final int old = s.getMaxIndex();
      final int newi = newS.isSelectionInOneBloc();
      if (i == newi) {
        select(i, old, newS.getSelection(i).getMaxIndex());
        fireSelectionEvent();
        return true;
      }
    }
    return super.changeSelection(_p, _tolerancePixel, EbliSelectionState.ACTION_ADD);
  }

}