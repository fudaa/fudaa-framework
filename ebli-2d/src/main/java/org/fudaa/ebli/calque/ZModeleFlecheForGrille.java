/*
 * @creation 24 mai 07
 * @modification $Date: 2007-06-20 12:23:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * Une classe permettant de cr�er un modele de fleches d'apr�s la grille utilis� sur une autre modele.
 *
 * @author fred deniger
 * @version $Id$
 */
public class ZModeleFlecheForGrille implements ZModeleFleche {

  final FlecheGrilleData grille_;
  int lastInterpolIdx_ = -1;
  final GrBoite realVueBoite_ = new GrBoite(new GrPoint(-1, -1, -1), new GrPoint());
  final GrSegment seg_ = new GrSegment(new GrPoint(), new GrPoint());
  final ZModeleFleche support_;

  public ZModeleFlecheForGrille(final ZModeleFleche _support, final FlecheGrilleData _grille) {
    super();
    support_ = _support;
    grille_ = _grille;
  }

  @Override
  public void prepare() {
    if (support_ != null) {
     support_. prepare();
    }
  }

  protected void updateBoite(ZCalqueFleche _support) {
    _support.getBoiteForGrille(realVueBoite_);
    clearCache();
  }

  private int getCol(int _i) {
    return _i % grille_.getNbXLines();
  }

  private int getRow(int _i) {
    return (_i - getCol(_i)) / grille_.getNbXLines();
  }

  @Override
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer) {
    return new CtuluTable(new ZCalqueFleche.ValueTableModel(this));
  }

  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
    info.titleIfOne_ = EbliLib.getS("Grille: point n�");
    info.title_ = EbliLib.getS("Grille: points");
    ZCalqueFleche.fillWithInfo(_d, _layer.getLayerSelection(), this, info);
  }

  @Override
  public GrBoite getDomaine() {
    return support_.getDomaine();
  }

  @Override
  public int getNombre() {
    return realVueBoite_.isIndefinie() ? 0 : grille_.getNbXLines() * grille_.getNbYLines();
  }

  @Override
  public double getNorme(int _i) {
    interpolFor(_i);
    return lastRes_ ? seg_.longueurXY() : 0;
  }

  protected void clearCache() {
    lastInterpolIdx_ = -1;
    lastRes_ = false;
  }
  boolean lastRes_;

  private void interpolFor(int _idx) {
    if (_idx != lastInterpolIdx_) {
      lastRes_ = support_.interpolate(seg_, getX(_idx), getY(_idx));
      lastInterpolIdx_ = _idx;
    }

  }

  @Override
  public Object getObject(int _ind) {
    return null;
  }

  @Override
  public double getVx(int _i) {
    interpolFor(_i);
    return lastRes_ ? seg_.getVx() : 0;
  }

  @Override
  public double getVy(int _i) {
    interpolFor(_i);
    return lastRes_ ? seg_.getVy() : 0;
  }

  @Override
  public double getX(int _i) {
    if (realVueBoite_.getDeltaX() == 0) {
      return 0D;
    }
    return grille_.getX(realVueBoite_, getCol(_i));
  }

  protected double getXForCol(int _i) {
    return grille_.getX(realVueBoite_, _i);
  }

  protected double getYForRow(int _i) {
    return grille_.getY(realVueBoite_, _i);
  }

  @Override
  public double getY(int _i) {
    if (realVueBoite_.getDeltaY() == 0) {
      return 0D;
    }
    return grille_.getY(realVueBoite_, getRow(_i));
  }

  @Override
  public double getZ1(int _i) {
    return 0;
  }

  @Override
  public double getZ2(int _i) {
    return 0;
  }

  @Override
  public boolean interpolate(GrSegment _seg, double _x, double _y) {
    return false;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public boolean segment(GrSegment _s, int _i, boolean _force) {
    interpolFor(_i);
    if (lastRes_) {
      _s.e_.initialiseAvec(seg_.e_);
      _s.o_.initialiseAvec(seg_.o_);
    } else {
      _s.o_.setCoordonnees(getX(_i), getY(_i), 0);
      _s.e_.initialiseAvec(_s.o_);
    }
    return lastRes_;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int vertex) {
    return null;
  }
}
