/*
 * @file         BCalqueSegment.java
 * @creation     1999-04-29
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrSegment;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Un calque d'affichage de segments.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 14:55:48 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class BCalqueSegment extends BCalqueAffichage {
  VecteurGrSegment s_;
  // Constructeur
  public BCalqueSegment() {
    super();
    s_= new VecteurGrSegment();
    typeTrait_= TraceLigne.LISSE;
    epaisseurTrait_= 1;
  }
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    //    int w=getIconWidth();
    final int h= getIconHeight();
    final TraceLigne trace= new TraceLigne();
    trace.setCouleur(fg);
    trace.setTypeTrait(typeTrait_);
    if (s_.nombre() > 0) {
      trace.dessineTrait((Graphics2D)_g,_x + 3, _y + h - 3, _x + 7, _y + h - 7);
      trace.dessineTrait((Graphics2D)_g,_x + 7, _y + h - 11, _x + 13, _y + h - 15);
      trace.dessineTrait((Graphics2D)_g,_x + 11, _y + h - 7, _x + 15, _y + h - 11);
    }
  }
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    final TraceGeometrie trace= new TraceGeometrie( getVersEcran());
    trace.setTypeTrait(typeTrait_, epaisseurTrait_);
    trace.setForeground(fg);
    final int n= s_.nombre();
    final boolean rapide= isRapide();
    for (int i= 0; i < n; i++) {
      trace.dessineSegment((Graphics2D)_g,s_.renvoie(i), rapide);
    }
  }
  /**
   * Reinitialise la liste de segments.
   */
  public void reinitialise() {
    s_= new VecteurGrSegment();
  }
  /**
   * Ajoute un segment a la liste de segments.
   */
  public void ajoute(final GrSegment _seg) {
    s_.ajoute(_seg);
  }
  /**
   * Retire un segment a la liste de segments.
   */
  public void enleve(final GrSegment _seg) {
    s_.enleve(_seg);
  }
  public VecteurGrSegment getSegments() {
    return s_;
  }
  public void setSegments(final VecteurGrSegment _lp) {
    if (_lp == s_) {
      return;
    }
    final VecteurGrSegment vp= s_;
    reinitialise();
    for (int i= 0; i < _lp.nombre(); i++) {
      ajoute(_lp.renvoie(i));
    }
    firePropertyChange("segments", vp, s_);
  }
  @Override
  public GrBoite getDomaine() {
    GrBoite r= null;
    if (isVisible()) {
      r= super.getDomaine();
      if (s_.nombre() > 0) {
        if (r == null) {
          r= new GrBoite();
        }
        for (int i= 0; i < s_.nombre(); i++) {
          r.ajuste(s_.renvoie(i).o_);
          r.ajuste(s_.renvoie(i).e_);
        }
      }
    }
    return r;
  }
  /**
   * Renvoi des elements selectionnables (GrContour) de ce calque.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res= new VecteurGrContour();
    res.tableau(s_.tableau());
    return res;
  }
  // Propriete typeTrait
  private int typeTrait_;
  /**
   * Accesseur de la propriete <I>typeTrait</I>.
   * Elle definit le style de trace des points, en prenant ses valeurs dans
   * les champs statiques de <I>Tracepoint</I>.
   * Par defaut, elle vaut TraceLigne.CONTINU.
   * @see org.fudaa.ebli.trace.TraceLigne
    */
  public int getTypeTrait() {
    return typeTrait_;
  }
  /**
   * Affectation de la propriete <I>typeTrait</I>.
   */
  public void setTypeTrait(final int _typeTrait) {
    if (typeTrait_ != _typeTrait) {
      final int vp= typeTrait_;
      typeTrait_= _typeTrait;
      firePropertyChange("typeTrait", vp, typeTrait_);
    }
  }
  // Propriete epaisseurTrait
  private int epaisseurTrait_;
  /**
   * Accesseur de la propriete <I>epaisseurTrait</I>.
   * Elle definit le style de trace des points, en prenant ses valeurs dans
   * les champs statiques de <I>Tracepoint</I>.
   * Par defaut, elle vaut TraceLigne.CONTINU.
   * @see org.fudaa.ebli.trace.TraceLigne
    */
  public int getEpaisseurTrait() {
    return epaisseurTrait_;
  }
  /**
   * Affectation de la propriete <I>epaisseurTrait</I>.
   */
  public void setEpaisseurTrait(final int _epaisseurTrait) {
    if (epaisseurTrait_ != _epaisseurTrait) {
      final int vp= epaisseurTrait_;
      epaisseurTrait_= _epaisseurTrait;
      firePropertyChange("epaisseurTrait", vp, epaisseurTrait_);
    }
  }
}
