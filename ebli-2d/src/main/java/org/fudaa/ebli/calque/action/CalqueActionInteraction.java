/**
 * @creation 21 janv. 2005
 * @modification $Date: 2008-02-22 16:26:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.commun.EbliActionChangeState;

/**
 * Une action a 2 etats permettant d'activer ou non un calque d'interaction.
 * @author Fred Deniger
 * @version $Id: CalqueActionInteraction.java,v 1.3.8.1 2008-02-22 16:26:34 bmarchan Exp $
 */
public class CalqueActionInteraction extends EbliActionChangeState implements
    PropertyChangeListener {

  protected BCalqueInteraction bc_;

  /**
   * Cr�ation de l'action.
   * @param _n Le texte de l'action
   * @param _ic L'icone de l'action
   * @param _actionCommand La commande
   * @param _bc Le calque d'interaction li� � cette action.
   */
  public CalqueActionInteraction(final String _n, final Icon _ic, final String _actionCommand,
      final BCalqueInteraction _bc) {
    super(_n, _ic, _actionCommand);
    bc_ = _bc;
    bc_.addPropertyChangeListener("gele", this);
  }

  @Override
  public void changeAction(){
    bc_.setGele(!super.isSelected());
  }

  public BCalqueInteraction getBc_() {
	return bc_;
}

public void setBc_(BCalqueInteraction bc_) {
	this.bc_ = bc_;
}

  @Override
  public void propertyChange(final PropertyChangeEvent _evt){
    if (isSelected() == bc_.isGele()) {
      super.setSelected(!bc_.isGele());
    }
  }
}