/*
 *  @creation     10 Mars 2009
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISGeometryCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionBloc;
import org.fudaa.ebli.calque.ZModeleBloc;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Un mod�le contenant des blocs. Les modifications apport�es au mod�le peuvent �tre �cout�es.
 * 
 * @author Bertrand Marchand
 * @status Experimental
 * @see {@link GISZoneCollectionBloc}, {@link ZCalqueBloc}, 
 * @version $Id$
 */
public class ZModeleBlocDefault extends ZModeleGeometryDefault 
  implements ZModeleBloc {

  public ZModeleBlocDefault() {
    this(new GISZoneCollectionBloc());
  }

  public ZModeleBlocDefault(GISZoneCollectionBloc _zone) {
    super(_zone);
  }

  @Override
  public int getNbGeometryForBloc(int _idxBloc) {
    return getGeomData().getGeometry(_idxBloc).getNumGeometries();
  }

  @Override
  public int getNbPointForGeometry(int _idxBloc, int _idxGeom) {
    Geometry g=((GISGeometryCollection)geometries_.getGeometry(_idxBloc)).getGeometryN(_idxGeom);
    return g.getNumPoints();
  }

  @Override
  public boolean isGeometryFermeeForBloc(int _idxBloc, int _idxGeom) {
    Geometry g=((GISGeometryCollection)geometries_.getGeometry(_idxBloc)).getGeometryN(_idxGeom);
    return (g instanceof LineString && ((LineString)g).isClosed());
  }

  @Override
  public boolean isGeometryRelieeForBloc(int _idxBloc, int _idxGeom) {
    Geometry g=((GISGeometryCollection)geometries_.getGeometry(_idxBloc)).getGeometryN(_idxGeom);
    return (g instanceof LineString);
  }

  @Override
  public boolean point(GrPoint _pt, int _idxBloc, int _idxGeom, int _pointIdx) {
    final CoordinateSequence g=((GISCoordinateSequenceContainerInterface)((GISGeometryCollection)geometries_.getGeometry(_idxBloc)).
        getGeometryN(_idxGeom)).getCoordinateSequence();
    _pt.x_ = g.getX(_pointIdx);
    _pt.y_ = g.getY(_pointIdx);
    _pt.z_=g.getOrdinate(_pointIdx, 2);
    
    return true;
  }
}
