/*
 * @file         SelectionEvent.java
 * @creation     2002-08-28
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuVectorint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
/**
 * Un �v�nement <I>SelectionEventModele</I>  specifique aux modeles. Il ne
 * contient que les index selectionnes du modele.
 *
 * @version      $Id: SelectionEventModele.java,v 1.7 2006-09-19 14:55:46 deniger Exp $
 * @author       Fred Deniger
 */
public class SelectionEventModele extends SelectionEvent {
  private FuVectorint indexSelectionnes_;
  public SelectionEventModele(
    final ZCalqueAffichageDonnees _source,
    final FuVectorint _indexSelectionnes) {
    this(_source, _indexSelectionnes, LAST);
  }
  public SelectionEventModele(
    final ZCalqueAffichageDonnees _source,
    final FuVectorint _indexSelectionnes,
    final int _position) {
    super(_source, null, _position);
    indexSelectionnes_= _indexSelectionnes;
  }
  public FuVectorint getIndexSelectionnes() {
    return indexSelectionnes_;
  }
  //Temporaire.
  public ZModeleDonnees getmodeleDonnees() {
    return ((ZCalqueAffichageDonnees)getSource()).modeleDonnees();
  }
  @Override
  public VecteurGrContour getObjects() {
    FuLog.warning(
      "SelectionEventModele.getObjects() : Methode non IMPLEMENTEE");
    return null;
  }
}
