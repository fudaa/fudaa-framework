/*
 * @creation 26 juil. 06
 * @modification $Date: 2007-01-17 10:45:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import javax.swing.JTable;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gui.CtuluSelectionListTableModelUpdater;
import org.fudaa.ebli.commun.EbliSelectionState;

/**
 * @author fred deniger
 * @version $Id: ZSelectionTableUpdater.java,v 1.3 2007-01-17 10:45:16 deniger Exp $
 */
public class ZSelectionTableUpdater extends CtuluSelectionListTableModelUpdater implements ZSelectionListener {
  final ZCalqueAffichageDonnees calque_;

  public ZSelectionTableUpdater(final JTable _table, final ZCalqueAffichageDonnees _calque) {
    super(_table, new CtuluListSelection());
    calque_ = _calque;
    calque_.addSelectionListener(this);
  }

  @Override
  public void selectionChanged(final ZSelectionEvent _evt) {
    if (!isInternalUpdate_) {
      selection_.setSelection(calque_.getLayerSelection());
    }

  }

  @Override
  protected void doAfterTableSectionChanged() {
    calque_.changeSelection(selection_, EbliSelectionState.ACTION_REPLACE);
  }

}
