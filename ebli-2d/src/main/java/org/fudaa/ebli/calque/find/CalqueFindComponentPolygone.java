/*
 * @creation 5 déc. 06
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.find;

import com.memoire.bu.*;
import org.locationtech.jts.geom.LinearRing;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.CalqueGISTreeModel;
import org.fudaa.ebli.calque.CalqueGISTreeModel.LayerNode;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindComponent;

/**
 * @author fred deniger
 * @version $Id$
 */
public class CalqueFindComponentPolygone implements EbliFindComponent {
  private final CalqueGISTreeModel model_;
  TreeSelectionModel selection_;
  JPanel pn_;
  BuComboBox cb_;
  BuLabel lbOptionSel_;
  BuPanel pnCb;
  
  public CalqueFindComponentPolygone(final ZEbliCalquesPanel _panel) {
    this(_panel.getDonneesCalque());
  }

  public CalqueFindComponentPolygone(final BGroupeCalque _cqRoot) {
    model_ = new CalqueGISTreeModel(null, _cqRoot);
    model_.setMask(GISLib.MASK_POLYGONE);
    cb_ = new BuComboBox(new String[] { EbliLib.getS("Tous les sommets sont sélectionnés"),
        EbliLib.getS("Au moins un sommet est sélectionné"), EbliLib.getS("le barycentre est sélectionné"),

    });
    builComponent();
  }

  @Override
  public JComponent getComponent() {
    return pn_;
  }

  private void builComponent() {
    if (pn_ == null) {
      pn_ = new BuPanel(new BuBorderLayout(0, 5));
      pn_.setBorder(BuBorders.EMPTY3333);
      final JTree tree = model_.createView(false, true);
      selection_ = tree.getSelectionModel();
      final BuScrollPane sc = new BuScrollPane(tree);
      sc.setPreferredHeight(80);
      sc.setPreferredWidth(60);
      pn_.add(sc, BuBorderLayout.CENTER);
      pnCb = new BuPanel(new BuVerticalLayout(1));
      pnCb.add(lbOptionSel_=new BuLabel(EbliLib.getS("Une ligne est sélectionnée si:")));
      pnCb.add(cb_);
      pn_.add(pnCb, BuBorderLayout.SOUTH);
    }
  }
  
  @Override
  public String getSearchId() {
    return getID() + CtuluLibString.getString(cb_.getSelectedIndex());
  }

  @Override
  public Object getFindQuery() {
    if (selection_.isSelectionEmpty()) {
      return null;
    }
    final List res = new ArrayList(20);
    final TreePath[] paths = selection_.getSelectionPaths();
    for (int i = paths.length - 1; i >= 0; i--) {
      model_.fillWithGeometries(((LayerNode) paths[i].getLastPathComponent()), res);
    }
    return res.toArray(new LinearRing[res.size()]);
  }

  public static final String ID = "POLY";

  public static String getID() {
    return ID;
  }
  
  /**
   * Pour ne rechercher que sur des atomiques.
   * @param _b true : Grise certaines options.
   */
  public void setFindOnAtomic(boolean _b) {
    cb_.setEnabled(!_b);
    lbOptionSel_.setEnabled(!_b);
  }

}
