/*
 * @file         BCalqueCartouche.java
 * @creation     2006-03-21
 * @modification $Date: 2006-11-14 09:06:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuInformationsDocument;
import javax.swing.JLabel;
import org.fudaa.ebli.controle.BSelecteurAncre;

/**
 * Un calque d'affichage de cartouche. Il est reprise de BCalqueCartouche mais
 * utilise un JComponent pour l'affichage. Cela permettra de simplifier l'affichage
 * et de pouvoir utiliser le html pour la mise en page (les JLabels supportent le html)
 * Par la suite, il faudrait cr�er une palette qui permettrait de positionner le label.
 *
 * @version $Id: ZCalqueCartouche.java,v 1.4 2006-11-14 09:06:24 deniger Exp $
 * @author Guillaume Desnoix, Nicolas Clavreul
 */
public class ZCalqueCartouche extends BCalqueAffichage {
  // Proprietes
  private BuInformationsDocument informations_;

  // l'entier qui sera utilise pour placer le label
  // il faudrait que tu modifies BSelecteurAncre pour ajouter
  // des nouvelles positions comme BOT_CENTRE ou TOP_CENTRE
  int ancre_ = BSelecteurAncre.BOT_LEFT;

  // le label qui sera utilise pour l'affichage
  JLabel lb_;

  public ZCalqueCartouche() {

  // initilialiser le label, le layout en respectant l'ancre
  // voir le calque BCalqueLegende
  }

  /**
   * Accesseur de la propriete <I>informations</I>. Elle donne l'information a afficher sur le document.
   */
  public BuInformationsDocument getInformations() {
    return informations_;
  }

  /**
   * Affectation de la propriete <I>informations</I>.
   */
  public void setInformations(final BuInformationsDocument _informations) {
    if (informations_ != _informations) {
      final BuInformationsDocument vp = informations_;
      informations_ = _informations;
      firePropertyChange("informations", vp, informations_);
      repaint();
      // il faut que tu modifies le JLabel pour qu'il affiche l'icone et
      // et le titre des infos
    }
  }
}
