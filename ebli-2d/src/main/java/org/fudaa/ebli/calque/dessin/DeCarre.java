/*
 * @file         DeCarre.java
 * @creation     1998-08-31
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un rectangle.
 *
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 14:55:53 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class DeCarre extends DeRectangle {
  // donnees membres publiques
  // donnees membres privees
  // Constructeurs
  public DeCarre() {
    super();
  }
  public DeCarre(final GrPolyligne _l) {
    super(_l);
  }
  @Override
  public int getForme() {
    return CARRE;
  }
  public DeCarre(final GrPoint _coin, final GrVecteur _cote1, final GrVecteur _cote2) {
    super();
    GrVecteur cote1=_cote1;
    GrVecteur cote2=_cote2;
    final double norme1= cote1.norme();
    final double norme2= cote2.norme();
    if (norme1 < norme2) {
      cote2= cote2.normalise().multiplication(norme1);
    } else {
      cote1= cote1.normalise().multiplication(norme2);
    }
    ajoute(_coin);
    ajoute(_coin.addition(cote1));
    ajoute(_coin.addition(cote1).addition(cote2));
    ajoute(_coin.addition(cote2));
  }
  // Methodes publiques
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
  }
}
