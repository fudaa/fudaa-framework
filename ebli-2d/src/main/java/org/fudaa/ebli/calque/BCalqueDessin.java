/*
 * @file         BCalqueDessin.java
 * @creation     1998-08-31
 * @modification $Date: 2007-05-04 13:49:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.trace.TraceGeometrie;

/**
 * Un calque d'affichage de dessins. Il contient des objets <I>DeForme</I> et les affiche. Voir le package <I>dessin</I>.
 * 
 * @version $Id: BCalqueDessin.java,v 1.12 2007-05-04 13:49:43 deniger Exp $
 * @author Axel von Arnim
 */
public class BCalqueDessin extends BCalqueAffichage {
  // donnees membres publiques
  // donnees membres privees
  List formes_;

  // Constructeurs
  public BCalqueDessin() {
    super();
    formes_ = new ArrayList();
  }

  // Methodes publiques
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color c1 = Color.red;
    Color c2 = new Color(0, 150, 0);
    if (isAttenue()) {
      c1 = attenueCouleur(c1);
    }
    if (isAttenue()) {
      c2 = attenueCouleur(c2);
    }
    _g.setColor(c1);
    _g.drawRect(_x + 3, _y + 3, _x + 13, _y + 10);
    _g.setColor(c2);
    _g.drawOval(_x + 7, _y + 7, _x + 15, _y + 11);
  }

  /**
   * Ajoute une nouvelle forme.
   */
  public void ajoute(final DeForme _forme) {
    formes_.add(_forme);
  }

  /**
   * Retire une forme.
   */
  public void enleve(final DeForme _forme) {
    formes_.remove(_forme);
  }

  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    final TraceGeometrie trace = new TraceGeometrie(getVersEcran());
    for (int i = 0; i < formes_.size(); i++) {
      ((DeForme) formes_.get(i)).affiche((Graphics2D) _g, trace, isRapide());
    }
  }

  /**
   * Boite englobante des objets contenus dans le calque.
   * 
   * @return Boite englobante. Si le calque est vide, la boite englobante retourn�e est <I>null</I>
   */
  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible() && formes_.size() > 0) {
      r = new GrBoite();
      for (int i = 0; i < formes_.size(); i++) {
        r.ajuste(((DeForme) formes_.get(i)).getGeometrie().boite());
      }
    }
    return r;
  }

  /**
   * Renvoi la liste des �l�ments s�lectionnables, soit tous les objets.
   * <p>
   * Cette liste est retourn�e dans le sens inverse de cr�ation des objets, de fa�on que le dernier cr�� soit le premier
   * � �tre test� lors de la s�lection.
   * 
   * @return La liste des formes.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res = new VecteurGrContour();
    for (int i = formes_.size() - 1; i >= 0; i--) {
      res.ajoute((GrContour) formes_.get(i));
    }
    return res;
  }
}
