/*
 *  @creation     4 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZModelePointEditable extends ZModeleGeometryDefault implements ZModelePointEditableInterface {

  /**
   * @author Fred Deniger
   * @version $Id$
   */
  public class PointTableModel extends AbstractTableModel {

    private GISZoneCollection getCollec() {
      return ZModelePointEditable.this.getGeomData();
    }

    @Override
    public int getColumnCount() {
      return 3 + ZModelePointEditable.this.getGeomData().getNbAttributes();
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
        case 0:
          return "N�";
        case 1:
          return "X";
        case 2:
          return "Y";
        default:
      }
      final int r = _column - 3;
      final GISZoneCollection pt = ZModelePointEditable.this.getGeomData();
      return pt.getAttribute(r).getName();
    }

    @Override
    public int getRowCount() {
      return ZModelePointEditable.this.getNombre();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return new Integer(_rowIndex + 1);
      } else if (_columnIndex == 1) {
        return CtuluLib.getDouble(((GISPoint) getCollec().getGeometry(_rowIndex)).getX());
      } else if (_columnIndex == 2) {
        return CtuluLib.getDouble(((GISPoint) getCollec().getGeometry(_rowIndex)).getY());
      }
      final GISZoneCollection coll = getCollec();
      return coll.getModel(_columnIndex - 3).getObjectValueAt(_rowIndex);
    }
  }
  protected GISZoneCollectionPoint pts_;

  @Override
  public boolean isCoordinateValid(final CoordinateSequence _seq, final CtuluAnalyze _analyze) {
    return true;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int vertex) {
    GrPoint p = new GrPoint();
    point(p, _ind, true);
    return p;
  }

  @Override
  public boolean isDataValid(CoordinateSequence _seq, ZEditionAttributesDataI _data, CtuluAnalyze _ana) {
    return true;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable r = new CtuluTable(new PointTableModel());
    EbliTableInfoPanel.setTitle(r, _layer.getTitle());
    return r;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  /**
   * @param _idx l'indice du point
   * @return le x du point _idx
   */
  @Override
  public double getX(final int _idx) {
    return pts_.getX(_idx);
  }

  @Override
  public GISZoneCollectionPoint getGeomData() {
    return pts_;
  }

  @Override
  public void prepareExport() {
    pts_.prepareExport();
  }

  @Override
  public void setGeometries(GISZoneCollectionGeometry geometries) {
    super.setGeometries(geometries);
    pts_ = (GISZoneCollectionPoint) geometries;
  }

  /**
   * @param _idx l'indice du point
   * @return le y du point _idx
   */
  @Override
  public double getY(final int _idx) {
    return pts_.getY(_idx);
  }

  protected ZModelePointEditable(final boolean _create) {
    super(null);
    if (_create) {
      setGeometries(new GISZoneCollectionPoint(null));
    }
  }

  public ZModelePointEditable() {
    this(true);

  }

  public ZModelePointEditable(final GISZoneCollectionPoint _zone) {
    super(_zone);
    pts_ = _zone;
  }

  /**
   * @param _pt les points initiaux
   */
  public ZModelePointEditable(final GrPoint[] _pt) {
    this();
    if (_pt != null) {
      final int nb = _pt.length;
      for (int i = 0; i < nb; i++) {
        pts_.add(_pt[i].x_, _pt[i].y_, _pt[i].z_);
      }
    }
  }

  /**
   * @param _idx les indices des points a enlever
   * @param _cmd le receveur de commande
   * @return true si ok.
   */
  @Override
  public boolean removePoint(final int[] _idx, final CtuluCommandContainer _cmd) {
    for (int i = 0; i < _idx.length; i++) {
      setGeomModif(_idx[i], _cmd); // Modification de l'etat de la g�om�trie
    }
    return pts_.remove(_idx, _cmd);
  }

  @Override
  public boolean setPoint(final int _idx, final double _newX, final double _newY, final CtuluCommandContainer _cmd) {
    setGeomModif(_idx, _cmd); // Modification de l'etat de la g�om�trie
    return pts_.set(_idx, _newX, _newY, _cmd);
  }

  @Override
  public boolean point(final GrPoint _p, final int _i, final boolean _force) {
    _p.x_ = pts_.getX(_i);
    _p.y_ = pts_.getY(_i);
    return true;
  }

  /**
   * @param _p le point a ajouter
   * @param _data les donnees associees
   * @param _cmd le receveur de commande
   */
  @Override
  public void addPoint(final GrPoint _p, final ZEditionAttributesDataI _data, final CtuluCommandContainer _cmd) {
    final int nbAtt = pts_.getNbAttributes();
    List r = null;
    if (nbAtt > 0) {
      r = new ArrayList();
      for (int i = 0; i < nbAtt; i++) {
        r.add(_data.getValue(pts_.getAttribute(i), 0));
      }
    }
    pts_.add(_p.x_, _p.y_, _p.z_, r, _cmd);
  }

//  public GrBoite getDomaine() {
//    final Envelope e = pts_.getEnvelopeInternal();
//    return new GrBoite(new GrPoint(e.getMinX(), e.getMinY(), 0), new GrPoint(e.getMaxX(), e.getMaxY(), 0));
//  }
  @Override
  public GrBoite getDomaine() {
    if (pts_ == null || pts_.getNumGeometries() == 0) {
      return null;
    }
    GrBoite bt = null;
    for (int i = pts_.getNumGeometries() - 1; i >= 0; i--) {
      if (isGeometryVisible(i)) {
        final GISPoint g = pts_.get(i);
        if (bt == null) {
          bt = new GrBoite();
        }
        bt.ajuste(g.getX(), g.getY(), g.getZ());
      }
    }
    return bt;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    _d.setTitle(_layer.getTitle());
    _d.put(EbliLib.getS("Nombre de points"), CtuluLibString.getString(getNombre()));
    final int nb = _layer.isSelectionEmpty() ? 0 : _layer.getLayerSelection().getNbSelectedIndex();
    _d.put(EbliLib.getS("Nombre de points s�lectionn�s"), CtuluLibString.getString(nb));
    // 2 points
    if (nb == 2) {
      final int i = _layer.getLayerSelection().getMaxIndex();
      final int i2 = _layer.getLayerSelection().getMinIndex();
      _d.put(EbliLib.getS("Distance entre les 2 points"),
              CtuluLib.DEFAULT_NUMBER_FORMAT.format(CtuluLibGeometrie.getDistance(getX(i), getY(i), getX(i2), getY(i2))));
      return;
    }
    // Plusieurs points
    if (nb != 1) {
      return;
    }
    // 1 point
    _d.put("X", ""+getX(_layer.getLayerSelection().getMaxIndex()));
    _d.put("Y", ""+getY(_layer.getLayerSelection().getMaxIndex()));
    final int idxNode = _layer.getLayerSelection().getMaxIndex();
    _d.setTitle(EbliLib.getS("Point {0}", CtuluLibString.getString(idxNode + 1)));
    final GISZoneCollection model = getGeomData();
    final int nbAtt = model.getNbAttributes();
    for (int i = 0; i < nbAtt; i++) {
      if (model.getDataModel(i) != null && model.getDataModel(i).getObjectValueAt(idxNode) != null) {
        _d.put(model.getAttribute(i).getName(), model.getDataModel(i).getObjectValueAt(idxNode).toString());
      }
    }
  }

  /**
   * Copie d'une geom�trie, avec d�calage.
   */
  public boolean copyGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, final CtuluCommandContainer _cmd) {
    if (pts_ == null) {
      return false;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    for (int idx = _selection.getMinIndex(); idx <= _selection.getMaxIndex(); idx++) {
      if (_selection.isSelected(idx)) {
        // La g�ometrie, d�cal�e.
        Point geom = (Point) pts_.getGeometry(idx).clone();
        CoordinateSequence seq = geom.getCoordinateSequence();
        for (int i = 0; i < seq.size(); i++) {
          seq.setOrdinate(i, 0, seq.getX(i) + _dx);
          seq.setOrdinate(i, 1, seq.getY(i) + _dy);
        }
        // Les valeurs d'attribut.
        Object[] vals = new Object[pts_.getNbAttributes()];
        for (int iatt = 0; iatt < pts_.getNbAttributes(); iatt++) {
          Object val = pts_.getValue(iatt, idx);
          if (pts_.getAttribute(iatt).equals(GISAttributeConstants.TITRE)) {
            val = CtuluLib.getS("Copie de") + " " + val;
          }
          if (pts_.getAttribute(iatt).equals(GISAttributeConstants.ETAT_GEOM)) {
            val = GISAttributeConstants.ATT_VAL_ETAT_MODI;
          }
          int nbpt = geom.getNumPoints();
          vals[iatt] = pts_.getAttribute(iatt).createDataForGeom(val, nbpt);
        }
        pts_.addGeometry(geom, vals, cmp);
      }
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return true;
  }

  @Override
  public int getNombre() {
    return pts_.getNumGeometries();
  }

  @Override
  public Object getObject(final int _ind) {
    return pts_ == null ? null : pts_.getGeometry(_ind);
  }

  @Override
  public void getDomaineForGeometry(int _idxGeom, GrBoite _target) {
    if (!isGeometryVisible(_idxGeom)) {
      return;
    }
    _target.o_.x_ = getX(_idxGeom);
    _target.o_.y_ = getY(_idxGeom);
    _target.e_.x_ = _target.o_.x_;
    _target.e_.y_ = _target.o_.y_;
  }

  @Override
  public int getNbPointForGeometry(int _idxGeom) {
    return 1;
  }

  @Override
  public boolean isGeometryFermee(int _idxGeom) {
    return false;
  }

  @Override
  public boolean isGeometryReliee(int _idxGeom) {
    return false;
  }

  @Override
  public boolean isGeometryVisible(int _idxGeom) {
    int iattVisibility = pts_.getIndiceOf(GISAttributeConstants.VISIBILITE);
    if (iattVisibility == -1) {
      return true;
    }

    return GISAttributeConstants.ATT_VAL_TRUE.equals(pts_.getValue(iattVisibility, _idxGeom));
  }

  @Override
  public boolean point(GrPoint _pt, int _idxGeom, int _pointIdx) {
    _pt.x_ = getX(_idxGeom);
    _pt.y_ = getY(_idxGeom);
    return true;
  }

  /**
   * Fait passer la g�om�trie en Modifier
   *
   * @param _idx : index de la geom�trie
   * @param _cmd :
   */
  public void setGeomModif(int _idx, CtuluCommandContainer _cmd) {
    int idxAttr = getGeomData().getIndiceOf(GISAttributeConstants.ETAT_GEOM);
    if (idxAttr >= 0) {
      getGeomData().getDataModel(idxAttr).setObject(_idx, GISAttributeConstants.ATT_VAL_ETAT_MODI, _cmd);
    }
  }
}
