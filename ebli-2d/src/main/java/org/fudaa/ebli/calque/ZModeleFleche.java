/*
 * @creation 23 mai 07
 * @modification $Date: 2007-06-05 08:58:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.geometrie.GrSegment;

/**
 * @author fred deniger
 * @version $Id: ZModeleFleche.java,v 1.3 2007-06-05 08:58:38 deniger Exp $
 */
public interface ZModeleFleche extends ZModeleSegment {

  boolean interpolate(final GrSegment _seg, final double _x, final double _y);
  
  

}
