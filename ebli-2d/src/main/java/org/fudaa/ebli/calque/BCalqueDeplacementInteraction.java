/*
 * @file         BCalqueDeplacementInteraction.java
 * @creation     1999-02-10
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;

/**
 * Un calque de deplacement d'elements selectionnes en coordonnees reelles.
 * 
 * @version $Id: BCalqueDeplacementInteraction.java,v 1.9 2006-09-19 14:55:46 deniger Exp $
 * @author Axel von Arnim
 */
public class BCalqueDeplacementInteraction extends BCalqueInteraction implements MouseMotionListener, MouseListener {
  private VecteurGrContour selection_;
  private GrPoint pointPrec_;
  private BVueCalque vc_; // Vue calque contenant le calque.

  public BCalqueDeplacementInteraction() {
    super();
    pointPrec_ = null;
  }

  public void setSelection(final VecteurGrContour _s) {
    if (_s == null) {
      return;
    }
    if (_s == selection_) {
      return;
    }
    final VecteurGrContour vp = selection_;
    selection_ = _s;
    firePropertyChange("selection", vp, selection_);
  }

  public VecteurGrContour getSelection() {
    return selection_;
  }

  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (_evt.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    pointPrec_ = new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    // Stockage de la vueCalque qui contient ce calque.
    if (vc_ == null) {
      vc_ = (BVueCalque) SwingUtilities.getAncestorOfClass(BVueCalque.class, this);
    }
  }

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (_evt.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    pointPrec_ = null;
    if (vc_ != null) {
      // vc_.getCalque().setAjustement(false);
      vc_.repaint();
    }
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {}

  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  @Override
  public void mouseExited(final MouseEvent _evt) {}

  @Override
  public void mouseMoved(final MouseEvent _evt) {}

  @Override
  public void mouseDragged(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (_evt.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    if (selection_ == null) {
    	return;
    }
    final GrPoint pointCour = new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    for (int i = 0; i < selection_.nombre(); i++) {
      final GrContour o = selection_.renvoie(i);
      if (o instanceof GrObjet) {
        ((GrObjet) o).autoApplique(GrMorphisme.translation(pointCour.soustraction(pointPrec_)));
      } else {
        FuLog.error("Not instance of GrObjet");
      }
    }
    pointPrec_ = pointCour;
    // fireSelectionEvent(new SelectionEvent(this, selection_));
    if (vc_ != null) {
      // vc_.getCalque().setAjustement(true);
      vc_.repaint();
    }
  }
  /*
   * B.M. 18/09/2001 Suppression de l'envoi d'un ev�nement SelectionEvent � chaque d�placement d'objet. Le calque ne
   * modifie pas la liste de s�lection, mais la position des objets s�lectionn�s. Cet �v�nement �tait utilis� de fa�on
   * inappropri�e dans LidoFilleReseau pour rafraichir la vueCalque. On rafraichit � pr�sent la vue calque en interne,
   * l'envoi de l'�venement n'a donc plus de raison d'�tre. On supprime par la m�me occasion la possibilit� d'avoir des
   * auditeurs de cet �venement.
   */
  /**
   * Ajout d'un auditeur � l'�venement <I>SelectionEvent</I>
   */
  // public synchronized void addSelectionListener(SelectionListener _listener) {
  // listeners_.addElement(_listener);
  // }
  /**
   * Suppression d'un auditeur � l'�venement <I>SelectionEvent</I>
   */
  // public synchronized void removeSelectionListener(SelectionListener _listener) {
  // listeners_.removeElement(_listener);
  // }
  /**
   * Notification aux auditeurs qu'un �venement <I>SelectionEvent</I> s'est produit
   */
  // public synchronized void fireSelectionEvent(SelectionEvent _evt) {
  // for(int i=0; i<listeners_.size(); i++) {
  // ((SelectionListener)listeners_.elementAt(i)).selectedObjects(_evt);
  // }
  // }
}
