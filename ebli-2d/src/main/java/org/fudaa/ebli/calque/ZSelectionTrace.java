/*
 *  @creation     31 mars 2005
 *  @modification $Date: 2008-03-18 07:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import org.fudaa.ebli.trace.*;

/**
 * Une classe d�finissant les couleurs et types de trac� pour la s�lection.
 *
 * @author Fred Deniger
 * @version $Id: ZSelectionTrace.java,v 1.3.8.1 2008-03-18 07:58:48 bmarchan Exp $
 */
public class ZSelectionTrace {

  private final TraceIconModelImmutable iconeModel_;
  private final TraceIconImmutable icone_;
  private final TraceIconModelImmutable iconeModelInterne_;
  private final TraceIconImmutable iconeInterne_;
  private final TraceLigneModelImmutable ligneModel_;
  private final TraceLigneImmutable ligne_;

  public final TraceIconImmutable getIcone() {
    return icone_;
  }

  public final TraceIconImmutable getIconeInterne() {
    return iconeInterne_;
  }

  public final TraceLigneImmutable getLigne() {
    return ligne_;
  }

  /**
   * Par defaut, toute selection bleue.
   */
  public ZSelectionTrace() {
    this(Color.BLUE, Color.BLUE);
  }

  /**
   * Une couleur de selection pour le trac� de la g�om�trie, une couleur pour les sommets.
   *
   * @param _cgeom La couleur pour la g�om�trie.
   * @param _csom La couleur pour les sommets.
   */
  public ZSelectionTrace(Color _cgeom, Color _csom) {
    super();
    iconeModel_ = new TraceIconModelImmutable(TraceIcon.CARRE_SELECTION, 4, _cgeom);
    ligneModel_ = new TraceLigneModelImmutable(TraceLigne.LISSE, 1.5f, _cgeom);
    iconeModelInterne_ = new TraceIconModelImmutable(TraceIcon.CARRE_SELECTION_ELEMENT, 4, _csom);

    icone_ = new TraceIconImmutable(iconeModel_);
    ligne_ = new TraceLigneImmutable(ligneModel_);
    iconeInterne_ = new TraceIconImmutable(iconeModelInterne_);
  }

  /**
   * @return la couleur a utiliser pour dessiner la selection
   */
  public final Color getColor() {
    return ligneModel_.getCouleur();
  }

  /**
   * @return l'icone a utiliser pour dessiner la selection
   */
  public final TraceIconModelImmutable getIconeModel() {
    return iconeModel_;
  }

  /**
   * @return l'icone a utiliser pour dessiner la selection des points internes
   */
  public final TraceIconModelImmutable getIconeModelInterne() {
    return iconeModelInterne_;
  }

  /**
   * @return une copie de l'icone de selection
   */
  public final TraceIcon getIconeCopy() {
    return new TraceIcon(iconeModel_);
  }

  /**
   * @return la ligne a utiliser pour dessiner la selection
   */
  public final TraceLigneModelImmutable getLigneModel() {
    return ligneModel_;
  }

  /**
   * @return une copie de la ligne de selection
   */
  public final TraceLigne getLigneCopy() {
    return new TraceLigne(new TraceLigneModel(ligneModel_));
  }
}
