/*
 * @creation 9 nov. 06
 * @modification $Date: 2007-05-04 13:49:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurColorChooser;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurInterface;

/**
 * @author fred deniger
 * @version $Id: BCalqueSectionConfigure.java,v 1.3 2007-05-04 13:49:43 deniger Exp $
 */
public class BCalqueSectionConfigure extends BCalqueConfigureSectionAbstract {

  public BCalqueSectionConfigure(final BCalque _target) {
    super(_target, EbliLib.getS("Affichage"));
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == BSelecteurColorChooser.DEFAULT_PROPERTY) {
      return target_.getCouleur();
    }
    return null;
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (_key == BSelecteurColorChooser.DEFAULT_PROPERTY) {
      target_.setCouleur((Color) _newProp);
      return true;
    }
    return false;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return new BSelecteurInterface[] { new BSelecteurColorChooserBt() };
  }

}
