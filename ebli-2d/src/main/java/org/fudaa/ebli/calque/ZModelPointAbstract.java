/**
 * @creation 6 f�vr. 2004
 * @modification $Date: 2007-03-30 15:36:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ebli.calque.find.CalqueFindPointExpression;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id: ZModelPointAbstract.java,v 1.9 2007-03-30 15:36:28 deniger Exp $
 */
public abstract class ZModelPointAbstract extends ZModeleDonnesAbstract implements
    ZModelePoint.ThreeDim {

  public EbliFindExpressionContainerInterface getExpressionContainer(){
    return new CalqueFindPointExpression(this);
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int vertex) {
    GrPoint p = new GrPoint();
    point(p, _ind, true);
    return p;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer){
    final BuTable res = new BuTable();
    res.setModel(new ZCalquePoint.DefaultTableModel(this));
    return res;
  }

  @Override
  public boolean isValuesTableAvailable(){
    return true;
  }

  @Override
  public boolean point(final GrPoint _p,final int _i,final boolean _force){
    if (_p == null) {
      return false;
    }
    _p.x_ = getX(_i);
    _p.y_ = getY(_i);
    _p.z_ = getZ(_i);
    return true;
  }

  @Override
  public Object getObject(final int _ind){
    throw new UnsupportedOperationException("not implemented method");
  }

  protected void fillWithXYZInfo(final InfoData _m,final int _idx){
    _m.put("X", CtuluLib.DEFAULT_NUMBER_FORMAT.format(getX(_idx)));
    _m.put("Y", CtuluLib.DEFAULT_NUMBER_FORMAT.format(getY(_idx)));
  }

  @Override
  public void fillWithInfo(final InfoData _m,final ZCalqueAffichageDonneesInterface _layer){
    _m.put(EbliLib.getS("Nombre de points"), CtuluLibString.getString(getNombre()));
    final int nbSelected = _layer.isSelectionEmpty() ? 0 : _layer.getLayerSelection()
        .getNbSelectedIndex();
    _m.put(EbliLib.getS("Nombre de points s�lectionn�s"), CtuluLibString.getString(nbSelected));
    if(nbSelected==2){
      final int i = _layer.getLayerSelection().getMaxIndex();
      final int i2 = _layer.getLayerSelection().getMinIndex();
      _m.put(EbliLib.getS("Distance entre les 2 points"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(CtuluLibGeometrie
        .getDistance(getX(i), getY(i), getX(i2), getY(i2))));
      return;
    }
    if (nbSelected == 1) {
      _m.setTitle(EbliLib.getS("Point n� {0}", CtuluLibString.getString(_layer.getLayerSelection()
          .getMaxIndex() + 1)));
      fillWithXYZInfo(_m, _layer.getLayerSelection().getMaxIndex());
    }

  }
}