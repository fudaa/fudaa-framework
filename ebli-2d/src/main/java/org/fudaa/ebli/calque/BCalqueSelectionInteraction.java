/*
 * @file         BCalqueSelectionInteraction.java
 * @creation     1998-09-02
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Un calque de s�lection d'objets GrContour (objets s�lectionnables).
 * <p>
 * Ces objets sont ajout�s au calque pour �tre reconnus. La fin de s�lection provoque la diffusion d'un �v�nement
 * <i>SelectionEvent</i>.
 * <p>
 * La s�lection peut se faire en mode ponctuel, rectangle ou polygonal.
 * <p>
 * Il est possible d'agir sur une s�lection pr�c�dente en effectuant agissant sur des boutons modificateurs pendant la
 * s�lection :
 * <p>
 * 
 * <pre>
 *           SHIFT      : Pour ajouter � la s�lection pr�c�dente.
 *           CTRL       : Pour supprimer de la s�lection pr�c�dente.
 *           SHIFT+CTRL : Mode XOR.
 * </pre>
 * 
 * <p>
 * Les points saisis sont g�r�s en coordonn�es r�elles pour pouvoir changer le rep�re de la vue pendant la saisie.
 * 
 * @version $Id: BCalqueSelectionInteraction.java,v 1.14 2006-09-19 14:55:45 deniger Exp $
 * @author Bertrand Marchand
 */
public class BCalqueSelectionInteraction extends BCalqueInteraction implements MouseListener, MouseMotionListener {
  // Modes de s�lection
  /**
   * Mode de s�lection par simple clic.
   */
  public final static int PONCTUEL = 0;

  /**
   * Mode de s�lection � l'aide d'un rectangle. Les objets enti�rement dans le rectangle sont s�lectionn�s.
   */
  public final static int RECTANGLE = 1;

  /**
   * Mode de s�lection par un polygone. Les objets enti�rement dans le rectangle sont s�lectionn�s.
   */
  public final static int POLYGONE = 2;

  // Modificateurs de s�lection
  private final static int REP = 0; // Remplacement de la s�lection

  private final static int ADD = 1; // Ajout � la s�lection

  private final static int DEL = 2; // Suppression de la s�lection

  private final static int XOR = 3; // Mode XOR

  private int dstSel_; // Distance de tol�rance pour la s�lection en pixels

  private int mode_; // Mode de s�lection courant

  private boolean enCours_; // S�lection en cours

  private VecteurGrPoint listePoints_;

  // Liste des points saisis en espace r�el.
  private TraceGeometrie tg_;

  // Trace dans le contexte graphique en espace r�el.
  private GrPoint ptExt_; // Point d'extr�mit� du trac� courant

  private GrPoint ptOrig_; // Point d'origine du trac� courant

  private GrPolyligne plHelper_;

  // Polyligne d'aide pour le trac�. Elle contient les points saisis.
  private int modificateur_; // Modificateur de s�lection

  private int typeTraitCourant_; // Type de trait de s�lection

  private Map objets_;

  // Objets s�lectionnables (connus du calque), par calque
  // private Vector calquesModeles_; // Listes des CalqueModele geres ce calque
  // Objets s�lectionnables (connus du calque)
  // private VecteurGrContour objets_;
  private Map selects_; // Objets s�lectionn�s

  private Map selectionListeners_; // Listeners de s�lection

  /**
   * Cr�ation d'un calque de s�lection sans objets s�lectionnables.
   */
  public BCalqueSelectionInteraction() {
    super();
    setDestructible(false);
    objets_ = new Hashtable();
    selects_ = new Hashtable();
    setTypeTrait(TraceLigne.LISSE);
    setModeSelection(RECTANGLE);
    selectionListeners_ = new Hashtable();
    listePoints_ = new VecteurGrPoint();
    plHelper_ = new GrPolyligne();
    plHelper_.sommets_ = listePoints_;
    enCours_ = false;
    dstSel_ = 4;
  }

  /**
   * Cr�ation d'un calque de s�lection avec une liste d'objets s�lectionnables.
   * 
   * @deprecated
   */
  public BCalqueSelectionInteraction(final GrContour[] _objets) {
    this();
    this.add(_objets, this);
  }

  /**
   * Cr�ation d'un calque de s�lection avec une liste d'objets s�lectionnables.
   * 
   * @deprecated
   */
  public BCalqueSelectionInteraction(final VecteurGrContour _objets) {
    this();
    this.add(_objets, this);
  }

  /**
   * Cr�ation d'un calque de s�lection avec un calque d'affichage associ�.
   * 
   * @deprecated
   */
  public BCalqueSelectionInteraction(final BCalqueAffichage _calque) {
    this();
    this.add(_calque, this);
  }

  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final int w = getIconWidth() - 1;
    final int h = getIconHeight() - 1;
    Image i;
    if (mode_ == PONCTUEL) {
      i = EbliResource.EBLI.getImage("fleche");
    } else if (mode_ == RECTANGLE) {
      i = EbliResource.EBLI.getImage("rectangle");
    } else {
      i = EbliResource.EBLI.getImage("polygone");
    }
    if (i != null) {
      final int wi = i.getWidth(this);
      final int hi = i.getHeight(this);
      int r = Math.max(wi / w, hi / h);
      if (r == 0) {
        r = 1;
      }
      _g.drawImage(i, _x + 1, _y + 1, wi / r - 1, hi / r - 1, _c);
    }
  }

  /**
   * Affectation de la propriete <I>typeTrait</I>.
   */
  public final void setTypeTrait(final int _typeTrait) {
    typeTraitCourant_ = _typeTrait;
  }

  /**
   * Accesseur de la propriete <I>typeTrait</I>. Elle fixe le type de trait (pointille, tirete, ...) en prenant ses
   * valeurs dans les champs statiques de <I>TraceLigne</I>.
   * 
   * @see org.fudaa.ebli.trace.TraceLigne
   */
  public int getTypeTrait() {
    return typeTraitCourant_;
  }

  /**
   * Affectation du mode de s�lection (<I>PONCTUEL, RECTANGLE, POLYGONE</I>).
   */
  public final void setModeSelection(final int _mode) {
    mode_ = _mode;
  }

  /**
   * Retour du mode de s�lection courant (<I>PONCTUEL, RECTANGLE, POLYGONE</I>).
   */
  public int getModeSelection() {
    return mode_;
  }

  /**
   * Ajout d'un objet � la liste des objets s�lectionnables.
   * 
   * @deprecated
   */
  public void add(final GrContour _objet) {
    add(_objet, this);
  }

  /**
   * Ajout d'un objet � la liste des objets s�lectionnables.
   */
  private void add(final GrContour _objet, final BCalque _calque) {
    final VecteurGrContour objetsCalque = getListeObjets(_calque);
    objetsCalque.ajoute(_objet);
  }

  /**
   * Ajout d'un tableau d'objets � la liste des objets s�lectionnables.
   * 
   * @deprecated
   */
  public void add(final GrContour[] _objets) {
    add(_objets, this);
  }

  /**
   * Ajout d'un tableau d'objets � la liste des objets s�lectionnables.
   */
  private void add(final GrContour[] _objets, final BCalque _calque) {
    final VecteurGrContour objetsCalque = getListeObjets(_calque);
    for (int i = 0; i < _objets.length; i++) {
      objetsCalque.ajoute(_objets[i]);
    }
  }

  /**
   * Ajout d'une liste d'objets � la liste des objets s�lectionnables.
   * 
   * @deprecated
   */
  public void add(final VecteurGrContour _objets) {
    add(_objets, this);
  }

  /**
   * Ajout d'une liste d'objets � la liste des objets s�lectionnables.
   */
  private void add(final VecteurGrContour _objets, final BCalque _calque) {
    final VecteurGrContour objetsCalque = getListeObjets(_calque);
    for (int i = 0; i < _objets.nombre(); i++) {
      objetsCalque.ajoute(_objets.renvoie(i));
    }
  }

  /**
   * Ajout des objets d'un calque d'affichage � la liste des objets s�lectionnables.
   */
  public void add(final BCalqueAffichage _calque) {
    add(_calque.contours(), _calque);
  }

  /**
   * Suppression d'un objets de la liste des objets s�lectionnables.
   * 
   * @deprecated
   */
  public void remove(final GrContour _objet) {
    remove(_objet, this);
  }

  /**
   * Suppression d'un objets de la liste des objets s�lectionnables.
   */
  private void remove(final GrContour _objet, final BCalque _calque) {
    final VecteurGrContour objetsCalque = (VecteurGrContour) objets_.get(_calque);
    if (objetsCalque == null) {
      return;
    }
    objetsCalque.enleve(_objet);
    if (objetsCalque.estVide()) {
      objets_.remove(objetsCalque);
    }
  }

  /**
   * Suppression d'un tableau d'objets de la liste des objets s�lectionnables.
   * 
   * @deprecated
   */
  public void remove(final GrContour[] _objets) {
    remove(_objets, this);
  }

  /**
   * Suppression d'un tableau d'objets de la liste des objets s�lectionnables.
   */
  private void remove(final GrContour[] _objets, final BCalque _calque) {
    final VecteurGrContour objetsCalque = (VecteurGrContour) objets_.get(_calque);
    if (objetsCalque == null) {
      return;
    }
    for (int i = 0; i < _objets.length; i++) {
      objetsCalque.enleve(_objets[i]);
      if (objetsCalque.estVide()) {
        objets_.remove(objetsCalque);
        break;
      }
    }
  }

  /**
   * Suppression d'une liste d'objets de la liste des objets s�lectionnables.
   * 
   * @deprecated
   */
  public void remove(final VecteurGrContour _objets) {
    remove(_objets, this);
  }

  /**
   * Suppression d'une liste d'objets de la liste des objets s�lectionnables.
   */
  private void remove(final VecteurGrContour _objets, final BCalque _calque) {
    final VecteurGrContour objetsCalque = (VecteurGrContour) objets_.get(_calque);
    if (objetsCalque == null) {
      return;
    }
    for (int i = 0; i < _objets.nombre(); i++) {
      objetsCalque.enleve(_objets.renvoie(i));
      if (objetsCalque.estVide()) {
        objets_.remove(objetsCalque);
        break;
      }
    }
  }

  /**
   * Suppression des objets d'un calque de la liste des objets s�lectionnables.
   */
  public void remove(final BCalqueAffichage _calque) {
    remove(_calque.contours(), _calque);
  }

  /**
   * Suppression du calque de tous les objets s�lectionnables.
   */
  @Override
  public void removeAll() {
    objets_.clear();
  }

  /**
   * Ajout d'un auditeur � l'�venement <I>SelectionEvent</I>.
   */
  public synchronized void addSelectionListener(final SelectionListener _listener) {
    selectionListeners_.put(_listener, _listener);
  }

  /**
   * Suppression d'un auditeur � l'�venement <I>SelectionEvent</I>.
   */
  public synchronized void removeSelectionListener(final SelectionListener _listener) {
    selectionListeners_.remove(_listener);
  }

  /**
   * Notification aux auditeurs qu'un �venement <I>SelectionEvent</I> s'est produit.
   */
  public synchronized void fireSelectionEvent(final SelectionEvent _evt) {
    for (final Iterator it = selectionListeners_.keySet().iterator(); it.hasNext();) {
      ((SelectionListener) it.next()).selectedObjects(_evt);
    }
  }

  public synchronized void fireSelectionEvent(final SelectionEventModele _evt) {
    for (final Iterator it = selectionListeners_.keySet().iterator(); it.hasNext();) {
      ((SelectionListener) it.next()).selectedObjects(_evt);
    }
  }

  /**
   * Suppression par programmation des objets s�lectionn�s. A le m�me effet qu'une s�lection nulle � la souris.
   */
  public void videSelection() {
    BCalque calque;
    VecteurGrContour selectsCalque;
    for (final Iterator it = selects_.entrySet().iterator(); it.hasNext();) {
      SelectionEvent evt;
      final Map.Entry e = (Map.Entry) it.next();
      calque = (BCalque) e.getKey();
      selectsCalque = (VecteurGrContour) e.getValue();
      selectsCalque.vide();
      if (it.hasNext()) {
        evt = new SelectionEvent(calque, selectsCalque, SelectionEvent.HAS_NEXT);
      } else {
        evt = new SelectionEvent(calque, selectsCalque, SelectionEvent.LAST);
      }
      fireSelectionEvent(evt);
    }
  }

  /**
   * Retourne la liste des objets s�lectionnables pour ce calque.
   */
  private VecteurGrContour getListeObjets(final BCalque _calque) {
    VecteurGrContour r = (VecteurGrContour) objets_.get(_calque);
    if (r == null) {
      r = new VecteurGrContour();
      objets_.put(_calque, r);
    }
    return r;
  }

  /**
   * G�n�ration de la liste des objets s�lectionn�s.
   */
  private void formeSaisie() {
    final GrMorphisme versEcran = this.getVersEcran();
    Polygon polygon = null;
    Point pt = null;
    if (mode_ == BCalqueSelectionInteraction.PONCTUEL) {
      pt = listePoints_.renvoie(0).applique(versEcran).point();
    }
    // Mode rectangle ou polygone
    else {
      polygon = plHelper_.applique(versEcran).toGrPolygone().polygon();
    }
    GrContour objet;
    GrPoint[] points;
    GrPoint pointEcran;
    BCalque calque;
    VecteurGrContour objetsCalque;
    VecteurGrContour selectsCalque;
    if (modificateur_ == REP) {
      selects_.clear();
    }
    NEXT_CALQUE: for (final Iterator e = objets_.keySet().iterator(); e.hasNext();) {
      calque = (BCalque) e.next();
      objetsCalque = (VecteurGrContour) objets_.get(calque);
      if (modificateur_ == REP) {
        selectsCalque = new VecteurGrContour();
        selects_.put(calque, selectsCalque);
      } else if (modificateur_ == ADD) {
        selectsCalque = (VecteurGrContour) selects_.get(calque);
        if (selectsCalque == null) {
          selectsCalque = new VecteurGrContour();
          selects_.put(calque, selectsCalque);
        }
      } else if (modificateur_ == DEL) {
        selectsCalque = (VecteurGrContour) selects_.get(calque);
        if (selectsCalque == null) {
          continue NEXT_CALQUE;
        }
      } else {
        selectsCalque = (VecteurGrContour) selects_.get(calque);
        if (selectsCalque == null) {
          selectsCalque = new VecteurGrContour();
          selects_.put(calque, selectsCalque);
        }
      }
      final int l = objetsCalque.nombre();
      NEXT_OBJECT: for (int i = 0; i < l; i++) {
        objet = objetsCalque.renvoie(i);
        if (mode_ == PONCTUEL) {
          if (!objet.estSelectionne(versEcran, dstSel_, pt)) {
            continue NEXT_OBJECT;
          }
        }
        // Modes rectangle et polygone
        else {
          points = objet.contour();
          // Si l'objet ne poss�de aucun point => Hors s�lection
          if (points.length == 0) {
            continue;
          }
          // Controle que tous les points sont dans le polygone de s�lection
          if (polygon != null) {
            for (int j = 0; j < points.length; j++) {
              pointEcran = points[j].applique(versEcran);
              if (!polygon.contains((int) pointEcran.x_, (int) pointEcran.y_)) {
                continue NEXT_OBJECT;
              }
            }
          }
        }
        // Ajout suppression de l'objet dans la liste des objets s�lectionn�s
        if (modificateur_ == REP) {
          selectsCalque.ajoute(objet);
        } else if (modificateur_ == ADD) {
          if (!selectsCalque.contient(objet)) {
            selectsCalque.ajoute(objet);
          }
        } else if (modificateur_ == DEL) {
          selectsCalque.enleve(objet);
        } else if (modificateur_ == XOR) {
          if (selectsCalque.contient(objet)) {
            selectsCalque.enleve(objet);
          } else {
            selectsCalque.ajoute(objet);
          }
        } else {
          FuLog.warning("Modificateur inconnu");
        }
        if (mode_ == PONCTUEL) {
          break NEXT_OBJECT;
          // Pour le mode ponctuel, s�lection d'un seul objet.
        }
      }
    }
    for (final Iterator e = selects_.entrySet().iterator(); e.hasNext();) {
      SelectionEvent evt;
      final Map.Entry entry = (Map.Entry) e.next();
      calque = (BCalque) entry.getKey();
      selectsCalque = (VecteurGrContour) entry.getValue();
      if (e.hasNext()) {
        evt = new SelectionEvent(calque, selectsCalque, SelectionEvent.HAS_NEXT);
      } else {
        evt = new SelectionEvent(calque, selectsCalque, SelectionEvent.LAST);
      }
      fireSelectionEvent(evt);
    }
  }

  //
  // Appel� avant chaque trac� pour d�finir le contexte graphique
  //
  private void initTrace(final Graphics2D _g) {
    _g.setXORMode(Color.white);
    tg_ = new TraceGeometrie(getVersEcran());
    tg_.setForeground(Color.black);
    tg_.setTypeTrait(typeTraitCourant_);
  }

  /*
   * Ev�nements souris Rectangle : PRESSED => Debut de rectangle DRAGGED => Trac� du rectangle RELEASED => Fin du
   * rectangle Polygone : RELEASED => D�but de cr�ation ou nouveau point cr�� MOVED => Trac� des 2 droites DRAGGED =>
   * M�me effet 2 RELEASED => Fin du polygone Button Mask : Rien => Remplacement des anciennes s�lections SHIFT => Ajout
   * des nouvelles s�lections, suppression si d�j� s�lectionn�
   */
  //
  // Methode invoquee quand on appuie sur un bouton de la souris.
  // Rectangle : D�but de cr�ation.
  //
  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    modificateur_ = REP;
    if (_evt.isShiftDown()) {
      modificateur_ |= ADD;
    }
    if (_evt.isControlDown()) {
      modificateur_ |= DEL;
    }
    final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    if (mode_ == RECTANGLE) {
      // D�but de saisie du rectangle
      enCours_ = true;
      final Graphics2D g = (Graphics2D) getGraphics();
      initTrace(g);
      listePoints_.vide();
      ptOrig_ = ptSo;
      ptExt_ = ptOrig_;

      tg_.dessineRectangle(g, ptOrig_, ptExt_, false);
      listePoints_.ajoute(ptOrig_);
    }
  }

  //
  // Methode invoquee quand on lache un bouton de la souris.
  // Ponctuel : Saisie du seul point
  // Rectangle : Fin de cr�ation
  // Polygone : Saisie d'un point du polygone, d�but de cr�ation ou fin (si 2 released)
  //
  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    modificateur_ = REP;
    if (_evt.isShiftDown()) {
      modificateur_ |= ADD;
    }
    if (_evt.isControlDown()) {
      modificateur_ |= DEL;
    }
    final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    final Graphics2D g = (Graphics2D) getGraphics();
    switch (mode_) {
    // Saisie d'un seul point
    case PONCTUEL:
      initTrace(g);
      listePoints_.vide();
      listePoints_.ajoute(ptSo);
      enCours_ = false;
      formeSaisie();
      break;
    // Fin du rectangle en cours => Cr�ation d'un rectangle
    case RECTANGLE:
      if (enCours_) {
        initTrace(g);
        tg_.dessineRectangle(g, ptOrig_, ptExt_, false);
        // Effacement du rectangle pr�c�dent
        listePoints_.ajoute(new GrPoint(ptSo.x_, ptOrig_.y_, 0.));
        listePoints_.ajoute(new GrPoint(ptSo.x_, ptSo.y_, 0.));
        listePoints_.ajoute(new GrPoint(ptOrig_.x_, ptSo.y_, 0.));
        enCours_ = false;
        formeSaisie();
      }
      break;
    // Saisie d'un point de polygone ou fin
    case POLYGONE:
      if (enCours_) {
        initTrace(g);
        // fin de polygone => Creation du point de fin et du polygone
        if (_evt.getClickCount() == 2) {
          if (plHelper_.nombre() < 3) {
            tg_.dessinePolyligne(g, plHelper_, false); // Efface l'ancien trac�
          } else {
            tg_.dessinePolygone(g, plHelper_, false, false);
          }
          enCours_ = false;
          formeSaisie();
        }
        // Saisie d'un nouveau point
        else {
          if (plHelper_.nombre() < 3) {
            tg_.dessinePolyligne(g, plHelper_, false); // Efface l'ancien trac�
          } else {
            tg_.dessinePolygone(g, plHelper_, false, false);
          }
          listePoints_.ajoute(ptSo);
          if (plHelper_.nombre() < 3) {
            tg_.dessinePolyligne(g, plHelper_, false); // Noveau trac�
          } else {
            tg_.dessinePolygone(g, plHelper_, false, false);
          }
        }
      }
      // Premier point saisi
      else if (_evt.getClickCount() != 2) {
        enCours_ = true;
        listePoints_.vide();
        initTrace(g);
        listePoints_.ajoute(ptSo);
        listePoints_.ajoute(ptSo);
        // Le 2ieme point correspond au point flottant avant clic.
        tg_.dessinePolyligne(g, plHelper_, false);
      }
      break;
    default:
    }
  }

  //
  // Methode invoquee quand on deplace la souris avec un bouton appuye.
  // Rectangle : Trac� du rectangle en cours de cr�ation
  // Polygone : Trac� des 2 droites reliant le point courant et les 2 points
  // extr�mit�s du polygone en cours de cr�ation
  //
  @Override
  public void mouseDragged(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    modificateur_ = REP;
    if (_evt.isShiftDown()) {
      modificateur_ |= ADD;
    }
    if (_evt.isControlDown()) {
      modificateur_ |= DEL;
    }
    final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    final Graphics2D g = (Graphics2D) getGraphics();
    switch (mode_) {
    // Saisie du rectangle en cours => Trac� d'un rectangle �volutif
    case RECTANGLE:
      if (enCours_) {
        initTrace(g);
        tg_.dessineRectangle(g, ptOrig_, ptExt_, false);
        // Effacement du rectangle pr�c�dent
        ptExt_ = ptSo;
        tg_.dessineRectangle(g, ptOrig_, ptExt_, false);
        // Trac� du noveau rectangle
      }
      break;
    // Trac� des lignes du polygone
    case POLYGONE:
      if (enCours_) {
        drawPoly(ptSo, g);
      }
      break;
    default:
    }
  }

  private void drawPoly(final GrPoint _ptSo, final Graphics2D _g) {
    initTrace(_g);
    if (plHelper_.nombre() < 3) {
      tg_.dessinePolyligne(_g, plHelper_, false); // Efface l'ancien trac�
    } else {
      tg_.dessinePolygone(_g, plHelper_, false, false);
    }
    listePoints_.remplace(_ptSo, listePoints_.nombre() - 1);
    if (plHelper_.nombre() < 3) {
      tg_.dessinePolyligne(_g, plHelper_, false); // Noveau trac�
    } else {
      tg_.dessinePolygone(_g, plHelper_, false, false);
    }
  }

  //
  // Methode invoquee quand on deplace la souris sans appuyer sur aucun bouton.
  // Polygone : Trac� des 2 droites reliant le point courant et les 2 points
  // extr�mit�s du polygone en cours de cr�ation
  //
  @Override
  public void mouseMoved(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    modificateur_ = REP;
    if (_evt.isShiftDown()) {
      modificateur_ |= ADD;
    }
    if (_evt.isControlDown()) {
      modificateur_ |= DEL;
    }
    final GrPoint ptSo = new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    if (mode_ == POLYGONE && enCours_) {
      final Graphics2D g = (Graphics2D) getGraphics();
      drawPoly(ptSo, g);
    }
  }

  //
  // Methode invoquee quand on sort du calque avec la souris.
  // Polygone : Effacement du polygone, pour acc�der aux boutons de navigation.
  //
  @Override
  public void mouseEntered(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (mode_ == POLYGONE && enCours_) {
      final Graphics2D g = (Graphics2D) getGraphics();
      initTrace(g);
      if (plHelper_.nombre() < 3) {
        tg_.dessinePolyligne(g, plHelper_, false); // Efface le trac�
      } else {
        tg_.dessinePolygone(g, plHelper_, false, false);
      }
    }
  }

  //
  // Methode invoquee quand on entre dans le calque avec la souris.
  // Polygone : Trac� du polygone, apr�s avoir acc�d� aux boutons de navigation.
  //
  @Override
  public void mouseExited(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (mode_ == POLYGONE && enCours_) {
      final Graphics2D g = (Graphics2D) getGraphics();
      initTrace(g);
      if (plHelper_.nombre() < 3) {
        tg_.dessinePolyligne(g, plHelper_, false); // Trace le polygone
      } else {
        tg_.dessinePolygone(g, plHelper_, false, false);
      }
    }
  }

  /**
   * <I>Sans objet</I>.
   */
  @Override
  public void mouseClicked(final MouseEvent _evt) {}

}
