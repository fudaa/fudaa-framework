/*
 * @file         BCalquePositionRelativePoint.java
 * @creation     1998-08-26
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuFilters;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.FilteredImageSource;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPositionRelativePoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPositionRelativePoint;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TracePoint;

/**
 * Un calque d'affichage de points relatifs.
 *
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:46 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BCalquePositionRelativePoint extends BCalquePoint {
  private VecteurGrPositionRelativePoint relatifsPoint_;

  public BCalquePositionRelativePoint() {
    super();
    relatifsPoint_ = new VecteurGrPositionRelativePoint();
  }

  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    Color fg;
    final TraceGeometrie trace = new TraceGeometrie(getVersEcran());
    trace.setTypePoint(typePoint_);
    trace.setTaillePoint(taillePoint_);
    Icon icon = icon_;
    if (typePoint_ == TracePoint.ICONE) {
      if (isAttenue() && (icon instanceof ImageIcon)) {
        final Image image = createImage(new FilteredImageSource(((ImageIcon) icon).getImage().getSource(), BuFilters.BRIGHTER));
        icon = new ImageIcon(image);
        trace.setIcon(icon);
        for (int i = 0; i < relatifsPoint_.nombre(); i++) {
          trace.dessinePoint((Graphics2D) _g, relatifsPoint_.renvoie(i), isRapide());
        }
      }
    } else {
      if (palette_ == null) {
        fg = getForeground();
        if (isAttenue()) {
          fg = attenueCouleur(fg);
        }
        trace.setForeground(fg);
        for (int i = 0; i < relatifsPoint_.nombre(); i++) {
          trace.dessinePoint((Graphics2D) _g, relatifsPoint_.renvoie(i), isRapide());
        }
      } else {
        for (int i = 0; i < relatifsPoint_.nombre(); i++) {
          final GrPositionRelativePoint p = relatifsPoint_.renvoie(i);
          fg = palette_.couleur((p.z() - minZ_) / (maxZ_ - minZ_));
          if (isAttenue()) {
            fg = attenueCouleur(fg);
          }
          trace.setForeground(fg);
          trace.dessinePoint((Graphics2D) _g, p, isRapide());
        }
      }
    }
  }

  @Override
  public void reinitialise() {
    super.reinitialise();
    relatifsPoint_ = new VecteurGrPositionRelativePoint();
  }

  /**
   * Ajoute un point relatif a la liste de points.
   */
  public void ajoute(final GrPositionRelativePoint _point) {
    // PB: IL NE FAUT PAS FAIRE DE TRAITEMENT DANS AJOUTE
    // (ca peut etre court-circuite par ListeGrPoint)
    // TROUVER UN AUTRE MOYEN !!
    relatifsPoint_.ajoute(_point);
    if (minZ_ > maxZ_) {
      minZ_ = _point.z();
      maxZ_ = _point.z();
    }
    if (_point.z() < minZ_) {
      minZ_ = _point.z();
    }
    if (_point.z() > maxZ_) {
      maxZ_ = _point.z();
    }
  }

  /**
   * Retire un point relatif a la liste de points.
   */
  public void enleve(final GrPositionRelativePoint _point) {
    relatifsPoint_.enleve(_point);
    calculeMinMaxZ();
  }

  public VecteurGrPositionRelativePoint getRelatifsPoint() {
    return relatifsPoint_;
  }

  public void setRelatifsPoint(final VecteurGrPositionRelativePoint _lp) {
    if (_lp == relatifsPoint_) {
      return;
    }
    final VecteurGrPositionRelativePoint vp = relatifsPoint_;
    reinitialise();
    for (int i = 0; i < _lp.nombre(); i++) {
      ajoute(_lp.renvoie(i));
    }
    firePropertyChange("relatifsPoint", vp, relatifsPoint_);
  }

  /**
   * Renvoi de la liste des elements selectionnables.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res = super.contours();
    res.tableau(relatifsPoint_.tableau());
    return res;
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible()) {
      r = super.getDomaine();
      if (relatifsPoint_.nombre() > 0) {
        if (r == null) {
          r = new GrBoite();
        }
        for (int i = 0; i < relatifsPoint_.nombre(); i++) {
          r.ajuste(relatifsPoint_.renvoie(i).toGrPoint());
        }
      }
    }
    return r;
  }

  @Override
  protected void calculeMinMaxZ() {
    GrPositionRelativePoint curP = null;
    minZ_ = 1;
    maxZ_ = -1;
    super.calculeMinMaxZ();
    if (minZ_ > maxZ_) {
      if (relatifsPoint_.nombre() > 0) {
        maxZ_ = relatifsPoint_.renvoie(0).z();
        minZ_ = maxZ_;
      }
    }
    for (int i = 1; i < relatifsPoint_.nombre(); i++) {
      curP = relatifsPoint_.renvoie(i);
      if (curP.z() > maxZ_) {
        maxZ_ = curP.z();
      }
      if (curP.z() < minZ_) {
        minZ_ = curP.z();
      }
    }
  }
}
