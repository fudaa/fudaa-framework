/*
 * @creation     21 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import java.util.EventObject;

/**
 * Un evenement envoy� par le calque d'interaction d'accrochage si un sommet de g�om�trie a �t�
 * accroch� ou d�ccroch� {@link ZCalqueCatchInteraction}.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZCatchEvent extends EventObject {
  /** Le type d'evement pour un accrochage */
  public static int CAUGHT=0;
  /** Le type d'evenement pour un d�ccrochage */
  public static int UNCAUGHT=1;

  /** Le type de l'evenement */
  public int type;
  /** La liste de selection */
  public ZSceneSelection selection;
  /** L'indice de g�ometrie accroch�e */
  public int idxGeom;
  /** L'indice de sommet accroch� */
  public int idxVertex;
  
  /**
   * Construction d'un evenement d'a&ccrochage 
   * @param _src Le calque ayant d�clench� cet evenement.
   * @param _type Le type de l'evenement
   * @param _selection La liste de selection
   * @param _idxGeom L'indice de g�ometrie accroch�e
   * @param _idxVertex L'indice de sommet accroch�
   */
  public ZCatchEvent(Object _src, int _type, ZSceneSelection _selection, int _idxGeom, int _idxVertex) {
    super(_src);
    type=_type;
    selection=_selection;
    idxGeom=_idxGeom;
    idxVertex=_idxVertex;
  }
}