/*
 *  @creation     20 avr. 2005
 *  @modification $Date: 2007-03-09 08:37:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluValuesMultiEditorPanel;
import org.fudaa.ctulu.gui.CtuluValuesParameters;
import org.fudaa.ctulu.gui.CtuluValuesParametersDefault;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;

/**
 * Un editeur pour les g�om�tries d'un calque. Permet l'�dition des g�om�tries et leur reordonnancement.
 *
 * @author marchand@deltacad.fr
 * @version $Id: CtuluValuesMultiEditorPanel.java,v 1.12 2007-03-09 08:37:37 deniger Exp $
 */
public class EbliObjetsMultiEditorPanel extends CtuluValuesMultiEditorPanel {

  private BuButton btUp_;
  private BuButton btDown_;
  private JComboBox cbSort_;
  private BuButton btInvertSort_;
  /**
   * True : Seules les g�om�tries s�lectionn�es sont prisent en compte
   */
  private boolean isFiltered_ = true;
  private ZModeleEditable model_;
  ZEditorValidatorI editValidator_;
  
  /**
   * Indice de zone vers sortable row
   */
  private HashMap<Integer, SortableRow> ind2Row = new HashMap<Integer, SortableRow>();
  /**
   * Les comparateurs pour chaque colonne
   */
  private final List<ValueRowComparator> comparators_;
  private final int[] idxInitMdlSel_;

  /**
   * Une ligne triable du tableau
   */
  class SortableRow extends Row {

    /**
     * L'index dans la zone
     */
    private int indZone;

    public SortableRow(int _ind, Object[] _values) {
      super(_values);
      indZone = _ind;
    }
  }

  /**
   * Un comparateur de lignes du tableau. Permet le tri initial
   */
  class InitialRowComparator implements Comparator<Row> {

    @Override
    public int compare(Row o1, Row o2) {
      return ((SortableRow) o1).indZone - ((SortableRow) o2).indZone;
    }
  }

  /**
   * Un comparateur de lignes du tableau, suivant la colonne choisie.
   */
  class ValueRowComparator implements Comparator<Row> {

    private int sortableColumn = 0;
    private Comparator comparator_;

    public ValueRowComparator(int _col, Comparator _comp) {
      sortableColumn = _col;
      comparator_ = _comp;
    }

    @Override
    public int compare(Row o1, Row o2) {
      return comparator_.compare(o1.getValue(sortableColumn), o2.getValue(sortableColumn));
    }
  }

  /**
   * @param _model Le mod�le �ditable.
   * @param _idx Les indices selectionn�s.
   */
  public EbliObjetsMultiEditorPanel(ZModeleEditable _model, final int[] _idx, EbliCoordinateDefinition[] _coordDefs, ZEditorValidatorI _validator) {
    super(buildParams(_model.getGeomData(), _idx, _coordDefs));

    model_ = _model;
    idxInitMdlSel_ = _idx;
    editValidator_ = _validator;

    for (Row row : rows_) {
      SortableRow sr = (SortableRow) row;
      ind2Row.put(sr.indZone, sr);
    }

    comparators_ = new ArrayList();
    buildComparators();
    decorePanel();
  }

  /**
   * Contruit les param�tres pour la classe mere depuis la collection.
   * @param _zone La collection
   * @param _idx Les indices selectionn�s
   * @return Les param�tres.
   */
  private static CtuluValuesParameters buildParams(final GISZoneCollection _zone, final int[] _idx, EbliCoordinateDefinition[] _coordDefs) {
    CtuluValuesParametersDefault params = new CtuluValuesParametersDefault();

    final List<String> names = new ArrayList<String>(_zone.getNbAttributes() + 2);
    final List<CtuluValueEditorI> editor = new ArrayList<CtuluValueEditorI>(names.size());
    final List<CtuluCollection> model = new ArrayList<CtuluCollection>(names.size());

    // Pour une collection de points
    if (_zone instanceof GISZoneCollectionPoint) {
      GISZoneCollectionPoint ptZone = (GISZoneCollectionPoint) _zone;

      names.add(_coordDefs[0].getName());
      names.add(_coordDefs[1].getName());
      CtuluValueEditorDouble coordEditor;
      // Editeurs X,Y respectent le formattage.
      coordEditor=new CtuluValueEditorDouble();
      coordEditor.setFormatter(_coordDefs[0].getFormatter().getXYFormatter());
      editor.add(coordEditor);
      coordEditor=new CtuluValueEditorDouble();
      coordEditor.setFormatter(_coordDefs[1].getFormatter().getXYFormatter());
      editor.add(coordEditor);
      model.add(ptZone.createEditForX());
      model.add(ptZone.createEditForY());
      for (int i = 0; i < ptZone.getNbAttributes(); i++) {
        final GISAttributeInterface attribute = ptZone.getAttribute(i);
        //les attributs non visibles ne sont pas affiche
        if (!attribute.isUserVisible()) {
          continue;
        }
        if (attribute.isEditable()) {
          names.add(ptZone.getAttribute(i).getNameWithUnit());
          editor.add(ptZone.getAttribute(i).getEditor());
          model.add(ptZone.getDataModel(i));
        }
      }
    } // Pour tout autre type
    else {
      final int nbAtt = _zone.getNbAttributes();
      for (int i = 0; i < nbAtt; i++) {
        final GISAttributeInterface att = _zone.getAttribute(i);
        // FIXME BM: G�rer attribut non visible. 
        if (!att.isUserVisible()) {
          continue;
        }
        if (!att.isAtomicValue() && att.isEditable()) {
          names.add(att.getNameWithUnit());
          editor.add(att.getEditor());
          model.add(_zone.getDataModel(i));
        }
      }
      params.editors_ = new CtuluValueEditorI[editor.size()];
      params.names_ = new String[names.size()];
      params.values_ = new GISAttributeModel[model.size()];

    }

    params.idx_ = _idx;
    params.names_ = names.toArray(new String[0]);
    params.editors_ = editor.toArray(new CtuluValueEditorI[0]);
    params.values_ = model.toArray(new CtuluCollection[0]);

    return params;
  }

  /**
   * Construit les comparateurs pour chaque colonne.
   */
  private void buildComparators() {
    GISZoneCollection zone=model_.getGeomData();
    
    // Pour une collection de points
    if (zone instanceof GISZoneCollectionPoint) {
      // Comparateur pour X
      comparators_.add(new ValueRowComparator(comparators_.size(), new Comparator<Double>() {

        @Override
        public int compare(Double o1, Double o2) {
          return o1.compareTo(o2);
        }
      }));
      // Comparateur pour Y
      comparators_.add(new ValueRowComparator(comparators_.size(), new Comparator<Double>() {

        @Override
        public int compare(Double o1, Double o2) {
          return o1.compareTo(o2);
        }
      }));
      // Les autres comparateurs
      for (int i = 0; i < zone.getNbAttributes(); i++) {
        final GISAttributeInterface att = zone.getAttribute(i);
        // FIXME BM: G�rer attribut non visible. 
        if (att.isEditable() && att.isUserVisible()) {
          comparators_.add(new ValueRowComparator(comparators_.size(), att.getComparator()));
        }
      }
    } // Pour toute autre liste
    else {
      for (int i = 0; i < zone.getNbAttributes(); i++) {
        final GISAttributeInterface att = zone.getAttribute(i);
        // FIXME BM: G�rer attribut non visible. 
        if (!att.isAtomicValue() && att.isEditable() && att.isUserVisible()) {
          comparators_.add(new ValueRowComparator(comparators_.size(), att.getComparator()));
        }
      }
    }
  }

  @Override
  public void addRow(Object[] _cells) {
    rows_.add(new SortableRow(rows_.size(), _cells));
  }

  private void initialSort() {
    int[] idxMdl = convertViewToZoneIdx(table_.getSelectedRows());

    Collections.sort(rows_, new InitialRowComparator());

    int[] idxSel = convertZoneToViewIdx(idxMdl);
    resetSelection(idxSel);
  }

  private void columnSort(int _col) {
    int[] idxMdl = convertViewToZoneIdx(table_.getSelectedRows());

    Collections.sort(rows_, new InitialRowComparator()); // Pour remettre dans un ordre de depart.
    Collections.sort(rows_, comparators_.get(_col - 1));

    int[] idxSel = convertZoneToViewIdx(idxMdl);
    resetSelection(idxSel);
  }

  private void invertSort() {
    int[] idxMdl = convertViewToZoneIdx(table_.getSelectedRows());

    for (int i = 0; i < rows_.size() / 2; i++) {
      Row tmp = rows_.get(i);
      rows_.set(i, rows_.get(rows_.size() - 1 - i));
      rows_.set(rows_.size() - 1 - i, tmp);
    }

    int[] idxSel = convertZoneToViewIdx(idxMdl);
    resetSelection(idxSel);
  }

  private void upIndexes() {
    int[] idxSel = table_.getSelectedRows();
    int[] idxMdl = convertViewToZoneIdx(idxSel);

    for (int i = 0; i < idxSel.length; i++) {
      if (idxSel[i] > i) {
        Row tmp = rows_.get(idxSel[i] - 1);
        rows_.set(idxSel[i] - 1, rows_.get(idxSel[i]));
        rows_.set(idxSel[i], tmp);
      }
    }

    idxSel = convertZoneToViewIdx(idxMdl);
    resetSelection(idxSel);
  }

  private void downIndexes() {
    int[] idxSel = table_.getSelectedRows();
    int[] idxMdl = convertViewToZoneIdx(idxSel);

    for (int i = idxSel.length - 1; i >= 0; i--) {
      if (idxSel[i] < rows_.size() - idxSel.length + i) {
        Row tmp = rows_.get(idxSel[i]);
        rows_.set(idxSel[i], rows_.get(idxSel[i] + 1));
        rows_.set(idxSel[i] + 1, tmp);
      }
    }

    idxSel = convertZoneToViewIdx(idxMdl);
    resetSelection(idxSel);
  }

  private void resetSelection(int[] _idxSel) {
    table_.getSelectionModel().clearSelection();
    for (int i = 0; i < _idxSel.length; i++) {
      table_.getSelectionModel().addSelectionInterval(_idxSel[i], _idxSel[i]);
    }
  }

  /**
   * Filtre ou non sur la selection
   * @param _b True : N'affiche que la selection. False : Toutes les geometries du calque.
   */
  public void setFiltered(boolean _b) {
    isFiltered_ = !_b;
    if (isFiltered_) {
      idxMdlSel_ = convertZoneToViewIdx(idxInitMdlSel_);
    } else {
      idxMdlSel_ = new int[rows_.size()];
      for (int i = 0; i < idxMdlSel_.length; i++) {
        idxMdlSel_[i] = i;
      }
    }

    btInvertSort_.setEnabled(!isFiltered_);
    cbSort_.setEnabled(!isFiltered_);
    tableSelectionChanged();

    table_.revalidate();
    table_.repaint();
  }

  /**
   * Verifie qu'il existe bien des geometries modifiables.
   * @return True : Des g�om�tries sont modifiables.
   */
  public boolean hasEditableData() {
    return title_.length > 0;
  }

  /**
   * Convertit les index de vue vers les indexs de la zone
   * @param _idx Les index de vue
   * @return Les index de la zone
   */
  private int[] convertViewToZoneIdx(int[] _idx) {
    int[] idx = new int[_idx.length];

    for (int i = 0; i < _idx.length; i++) {
      idx[i] = ((SortableRow) rows_.get(_idx[i])).indZone;
    }
    return idx;
  }

  /**
   * Converti les index de zone vers les index de vue.
   * @param _idx Les index de zone
   * @return Les index de vue. Attention, en cas de filtre, certains indices retourn�s peuvent valoir -1 (si l'indice zone n'est
   * pas visualis�).
   */
  private int[] convertZoneToViewIdx(int... _idx) {
    int[] idx = new int[_idx.length];

    for (int i = 0; i < _idx.length; i++) {
      idx[i] = rows_.indexOf(ind2Row.get(_idx[i]));
    }
    return idx;
  }

  private void tableSelectionChanged() {
    btUp_.setEnabled(!isFiltered_ && table_.getSelectedRowCount() != 0);
    btDown_.setEnabled(!isFiltered_ && table_.getSelectedRowCount() != 0);
  }

  private void btUpActionPerformed() {
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }
    upIndexes();
    table_.revalidate();
    table_.repaint();
    cbSort_.setSelectedIndex(0);
  }

  private void btDownActionPerformed() {
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }
    downIndexes();
    table_.revalidate();
    table_.repaint();
    cbSort_.setSelectedIndex(0);
  }

  private void btInvertActionPerformed() {
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }
    invertSort();
    table_.revalidate();
    table_.repaint();
    cbSort_.setSelectedIndex(0);
  }

  private void cbSortActionPerformed() {
    if (cbSort_.getSelectedIndex() == 1) {
      initialSort();
    } // Tri par comparateur de colonne
    else if (cbSort_.getSelectedIndex() > 1) {
      columnSort(cbSort_.getSelectedIndex() - 1);
    }
    table_.revalidate();
    table_.repaint();
  }

  /**
   * Ajoute des options de r�ordonnancement � l'editeur g�n�rique.
   */
  private void decorePanel() {
    table_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        tableSelectionChanged();
      }
    });

    btUp_ = new BuButton(BuResource.BU.getIcon("monter"));
    btUp_.setToolTipText(EbliLib.getS("D�cr�menter l'index"));
    btDown_ = new BuButton(BuResource.BU.getIcon("descendre"));
    btDown_.setToolTipText(EbliLib.getS("Incr�menter l'index"));
    btUp_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btUpActionPerformed();
      }
    });
    btDown_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btDownActionPerformed();
      }
    });

    JPanel pnMove = new JPanel();
    pnMove.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 0));
    pnMove.add(new BuLabel(EbliLib.getS("Changer l'ordre des g�om�tries")));
    pnMove.add(btUp_);
    pnMove.add(btDown_);

    JLabel lbSort = new JLabel(EbliLib.getS("Trier les g�om�tries suivant"));
    cbSort_ = new JComboBox();
    cbSort_.addItem("");
    cbSort_.addItem("<" + EbliLib.getS("Ordre initial") + ">");
    for (Object title : title_) {
      cbSort_.addItem(title.toString());
    }
    cbSort_.setEnabled(false);
    cbSort_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        cbSortActionPerformed();
      }
    });
    btInvertSort_ = new BuButton(EbliLib.getS("Inverser l'ordre"));
    btInvertSort_.setEnabled(false);
    btInvertSort_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btInvertActionPerformed();
      }
    });
    JPanel pnSort = new JPanel();
    pnSort.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 0));
    pnSort.add(lbSort);
    pnSort.add(cbSort_);
    pnSort.add(btInvertSort_);

    JPanel pnReorder = new JPanel();
    pnReorder.setLayout(new BuVerticalLayout(2));
    pnReorder.add(pnMove);
    pnReorder.add(pnSort);
    pnReorder.setBorder(BorderFactory.createTitledBorder(EbliLib.getS("R�ordonner")));

    add(pnReorder, BorderLayout.SOUTH);

    tableSelectionChanged();
  }

  @Override
  public boolean apply() {
    // on arrete la saisie avant d'appliquer les modifs
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }
    
    if (editValidator_ != null && !editValidator_.isMultiEditionValid(model_))
      return false;

    final CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Edition des g�om�tries"));

    for (int irow = 0; irow < rows_.size(); irow++) {
      int initInd = ((SortableRow) rows_.get(irow)).indZone;
      // Les lignes ont �t� d�plac�es => Il faut d�placer aussi les g�om�tries dans la collection.
      if (initInd != irow) {
        // Les g�om�tries sont switch�es, les index initiaux aussi.
        model_.getGeomData().switchGeometries(irow, initInd, cmp);
        // Les index initiaux aussi
        for (int i = irow + 1; i < rows_.size(); i++) {
          SortableRow row = (SortableRow) rows_.get(i);
          if (row.indZone == irow) {
            row.indZone = initInd;
            break;
          }
        }
        ((SortableRow) rows_.get(irow)).indZone = irow;
      }

      // Remplacement des valeurs modifi�es.
      for (int icol = 0; icol < values_.length; icol++) {
        if (rows_.get(irow).isModified(icol)) {
          values_[icol].setObject(irow, rows_.get(irow).getValue(icol), cmp);
        }
      }
    }
    if (cmd_ != null) {
      cmd_.addCmd(cmp.getSimplify());
    }

    return true;
  }
}
