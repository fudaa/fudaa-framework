package org.fudaa.ebli.calque.find;

import org.locationtech.jts.geom.CoordinateSequence;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.nfunk.jep.Variable;

/**
 * Une recherche d'expression gerant le mode atomique ou global d'un calque.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class CalqueFindExpressionAtomic extends CalqueFindExpression {

  /** Le calque associ� */
  ZCalqueGeometry layer_;
  /** La table des variables g�r�es par le calque */
  Map<Variable,Integer> vars_;
  /** La variable X, dont le traitement est sp�cial */
  Variable varX_;
  /** La variable Y, dont le traitement est sp�cial */
  Variable varY_;
  /** Une table de stockage temporaire */
  List<Double> ord_=new ArrayList<Double>(50);

  public CalqueFindExpressionAtomic(ZCalqueGeometry _layer) {
    super(_layer.modeleDonnees());
    layer_=_layer;
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    if (layer_.isAtomicMode()) {
      initialiseExprWhenAtomic(_expr);
      return;
    }
    
    super.initialiseExpr(_expr);
    final GISZoneCollection coll = layer_.modeleDonnees().getGeomData();
    final int nb = coll.getNbAttributes();
    if (nb > 0) {
      if (vars_ == null) {
        vars_ = new HashMap<Variable,Integer>(nb);
      } else {
        vars_.clear();
      }
      for (int i = 0; i < nb; i++) {
        final GISAttributeInterface att = coll.getAttribute(i);
        if (!att.isAtomicValue()) {
          vars_.put(_expr.addVar(att.getName(), att.getLongName()), i);
        }
      }
    }
  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    if (layer_.isAtomicMode()) {
      majVariableWhenAtomic(_idx, _varToUpdate);
      return;
    }

    super.majVariable(_idx, _varToUpdate);
    if (vars_ != null && !CtuluLibArray.isEmpty(_varToUpdate)) {
      final GISZoneCollection coll = layer_.modeleDonnees().getGeomData();
      for (int i = _varToUpdate.length - 1; i >= 0; i--) {
        if (vars_.containsKey(_varToUpdate[i])) {
          _varToUpdate[i].setValue(coll.getDataModel(vars_.get(_varToUpdate[i])).getObjectValueAt(_idx));
        }
      }
    }
  }

  /**
   * Initialise le container d'expressions avec des variables atomiques uniquement.
   * @param _expr Le container.
   */
  public void initialiseExprWhenAtomic(CtuluExpr _expr) {
    _expr.initVar();
    _expr.getParser().getFunctionTable().remove("str");

    
    // Suppression de l'indice, ajout� par defaut par la classe m�re.
    _expr.removeVar("i");
    
    final GISZoneCollection coll = layer_.modeleDonnees().getGeomData();
    final int nb = coll.getNbAttributes();
    if (nb > 0) {
      if (vars_ == null) {
        vars_ = new HashMap<Variable,Integer>(nb);
      } else {
        vars_.clear();
      }
      for (int i = 0; i < nb; i++) {
        final GISAttributeInterface att = coll.getAttribute(i);
        if (att.isAtomicValue()) {
          vars_.put(_expr.addVar(att.getName(), att.getLongName()),i);
        }
      }
    }

    varX_=_expr.addVar("x","L'abscisse des sommets");
    varY_=_expr.addVar("y","L'ordonn�e des sommets");
  }

  /**
   * Le traitement pour les variables atomiques.
   * @param _idx L'indice de g�om�trie.
   * @param _varToUpdate Les variables � mettre a jour.
   * @param _values Les valeurs pour chaque variable.
   */
  public void majVariableWhenAtomic(int _idx, Variable[] _varToUpdate) {
    
    final GISZoneCollection coll = layer_.modeleDonnees().getGeomData();
    int ivar;
    if ((ivar=CtuluLibArray.getIndex(varX_, _varToUpdate))!=-1)
      _varToUpdate[ivar].setValue((getOrdinate(_idx, 0)));
    if ((ivar=CtuluLibArray.getIndex(varY_, _varToUpdate))!=-1)
      _varToUpdate[ivar].setValue(getOrdinate(_idx, 1));
    
    for (int i=0; i<_varToUpdate.length; i++) {
      if ((vars_.containsKey(_varToUpdate[i]))) {
        _varToUpdate[i].setValue(
          ((GISAttributeModel)coll.getDataModel(vars_.get(_varToUpdate[i])).getObjectValueAt(_idx)).getObjectValues());
      }
    }
  }

  protected Double[] getOrdinate(int _idxGeom, int _ordonnee) {
    CoordinateSequence seq=((GISCoordinateSequenceContainerInterface)layer_.modeleDonnees().getObject(_idxGeom)).getCoordinateSequence();
    ord_.clear();
    
    if (_ordonnee==2)
      layer_.modeleDonnees().getGeomData().initZCoordinate(_idxGeom);
    
    for (int i=0; i<seq.size(); i++)
      ord_.add(seq.getOrdinate(i, _ordonnee));
    
    return ord_.toArray(new Double[0]);
  }
}
