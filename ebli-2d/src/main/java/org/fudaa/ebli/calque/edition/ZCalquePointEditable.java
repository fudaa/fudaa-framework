/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesConfigure;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesTraceConfigure;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZCalqueGeometryLabelConfigure;
import org.fudaa.ebli.calque.ZCalquePoint;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.find.CalqueFindActionAtomic;
import org.fudaa.ebli.calque.find.CalqueFindPointExpression;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.locationtech.jts.geom.Geometry;
import org.nfunk.jep.Variable;

import com.memoire.fu.FuLog;

import gnu.trove.TObjectIntHashMap;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZCalquePointEditable extends ZCalquePoint implements ZCalqueEditable, ZModelGeometryListener {

  private Color labelsForegroundColor = Color.BLACK;
  private Color labelsBackgroundColor = Color.WHITE;
  private boolean labelsVisible  = false;
  /**
   * Le finder pour une recherche sur le calque
   */
  protected CalqueFindActionAtomic finder_;

  public void setLabelsForegroundColor(Color labelsForegroundColor) {
    if (!ObjectUtils.equals(this.labelsForegroundColor, labelsForegroundColor)) {
      Color old = this.labelsForegroundColor;
      this.labelsForegroundColor = labelsForegroundColor;
      repaint();
      firePropertyChange(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND, old, labelsForegroundColor);
    }
  }

  public void setLabelsBackgroundColor(Color labelsBackgroundColor) {
    if (!ObjectUtils.equals(this.labelsBackgroundColor, labelsBackgroundColor)) {
      Color old = this.labelsBackgroundColor;
      this.labelsBackgroundColor = labelsBackgroundColor;
      repaint();
      firePropertyChange(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND, old, labelsBackgroundColor);
    }
  }

  public void setLabelsVisible(boolean _b) {
    if (!ObjectUtils.equals(this.labelsVisible, _b)) {
      boolean old = this.labelsVisible;
      this.labelsVisible = _b;
      repaint();
      firePropertyChange(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE, old, this.labelsVisible);
    }
  }
  
  public boolean isLabelsVisible() {
    return labelsVisible;
  }

  public Color getLabelsBackgroundColor() {
    return labelsBackgroundColor;
  }

  public Color getLabelsForegroundColor() {
    return labelsForegroundColor;
  }

  protected void drawLabel(FontMetrics fm, String s, GrPoint pEcran, Color bgColor, Graphics2D _g, Color fgColor) {
    int stringWidth = fm.stringWidth(s);
    int x = (int) (pEcran.x_ - stringWidth / 2);
    int y = (int) (pEcran.y_ - 10);
    if (bgColor != null) {
      _g.setColor(bgColor);
      _g.fillRect(x, y - fm.getAscent(), stringWidth, fm.getHeight() + 2);
    }
    _g.setColor(fgColor);
    _g.drawString(s, (int) x, (int) y);
  }

  public void clearCacheAndRepaint() {
    clearCache();
    repaint();
  }

  /**
   * @author Fred Deniger
   * @version $Id$
   */
  private class VariableFindExpression extends CalqueFindPointExpression {

    TObjectIntHashMap<Variable> varAtt_;

    public VariableFindExpression() {
      super(ZCalquePointEditable.this.modele());
    }

    @Override
    public void initialiseExpr(final CtuluExpr _expr) {
      super.initialiseExpr(_expr);
      final GISZoneCollection coll = ((ZModelePointEditable) super.data_).getGeomData();
      final int nb = coll.getNbAttributes();
      if (nb > 0) {
        if (varAtt_ == null) {
          varAtt_ = new TObjectIntHashMap<>(nb);
        } else {
          varAtt_.clear();
        }
        for (int i = 0; i < nb; i++) {
          final GISAttributeInterface att = coll.getAttribute(i);
          if (Number.class.isAssignableFrom(att.getDataClass())) {
            varAtt_.put(_expr.addVar(att.getName(), att.getLongName()), i);
          }
        }
      }

    }

    @Override
    public void majVariable(final int _idx, final Variable[] _varToUpdate) {
      super.majVariable(_idx, _varToUpdate);
      if (varAtt_ != null && !CtuluLibArray.isEmpty(_varToUpdate)) {
        final GISZoneCollection coll = ((ZModelePointEditable) super.data_).getGeomData();
        for (int i = _varToUpdate.length - 1; i >= 0; i--) {
          if (varAtt_.containsKey(_varToUpdate[i])) {
            _varToUpdate[i].setValue(CtuluLib.getDouble(((Number) coll.getDataModel(varAtt_.get(_varToUpdate[i]))
                    .getObjectValueAt(_idx)).doubleValue()));
          }
        }
      }
    }
  }
  ZEditorInterface editor_;

  public ZCalquePointEditable() {
    super();
  }

  /**
   * @param _modele
   */
  public ZCalquePointEditable(final ZModelePointEditable _modele, final ZEditorInterface _editor) {
    super(_modele);
    editor_ = _editor;
    if (_modele != null) {
      ((ZModelePointEditableInterface) _modele).addModelListener(this);
    }
  }

  @Override
  public boolean addAtome(final GrPoint _ptReel, final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    return false;
  }
  protected GISAttributeInterface attrLabels_ = null;

  /**
   * D�finit l'attribut atomique pris pour afficher les labels.
   *
   * @param _att L'attribut. Si Null : Pas d'affichage de labels.
   */
  public void setAttributForLabels(GISAttributeInterface _att) {
    if (_att == attrLabels_) {
      return;
    }
    if (_att == null) {
      attrLabels_ = null;
    } else {
      attrLabels_ = _att;
    }
    clearCacheAndRepaint();
  }
  
  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[2 + getNbSet()];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    sect[1] = new ZCalqueGeometryLabelConfigure(this);
    for (int i = 2; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(this, i - 2);
    }
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    EbliUIProperties properties = super.saveUIProperties();
    properties.put(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND, labelsBackgroundColor);
    properties.put(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND, labelsForegroundColor);
    properties.put(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE, (Boolean)labelsVisible);
    return properties;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      if (_p.isDefined(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND)) {
        setLabelsBackgroundColor((Color) _p.get(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND));
      } else {
        setLabelsBackgroundColor(null);
      }
      if (_p.isDefined(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND)) {
        setLabelsForegroundColor((Color) _p.get(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND));
      }
      if (_p.isDefined(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE)) {
        setLabelsVisible((Boolean) _p.get(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE));
      }
    }
  }

  @Override
  public void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel) {
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);
    
    if (labelsVisible)
      paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
  }

  protected void paintLabelsOnAtomics(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {

    if (attrLabels_ != null && !isRapide()) {
      GISZoneCollection geomData = getModelEditable().getGeomData();
      int idxLabels = geomData.getIndiceOf(attrLabels_);

      if (idxLabels != -1) {
        final Color fgColor = labelsForegroundColor;
        final Color bgColor = labelsBackgroundColor;
        GrPoint p = new GrPoint();
        Font old = _g.getFont();
        _g.setFont(getFont());
        final FontMetrics fm = _g.getFontMetrics();
        final int nombre = modele_.getNombre();
        int w = getWidth() + 1;
        CtuluListSelection memory = new CtuluListSelection(w * getHeight());
        for (int i = nombre - 1; i >= 0; i--) {
          // La g�ometrie n'est pas visible
          if (!isPainted(i)) {
            continue;
          }

          // si le point est filtre on passe. On ne force pas l'affectation.
          if (!modele_.point(p, i, false)) {
            continue;
          }
          if (!_clipReel.contientXY(p)) {
            continue;
          }
          p.autoApplique(_versEcran);
          int idxOnScreen = (int) p.x_ + (int) p.y_ * w;
          if (idxOnScreen >= 0 && !memory.isSelected(idxOnScreen)) {
            //pour �viter de redessiner le texte au m�me endroit:
            Object o = geomData.getModel(idxLabels).getObjectValueAt(i);
            if (o == null) {
              continue;
            }
            String s = o.toString().trim();
            if (StringUtils.isBlank(s)) {
              continue;
            }
            memory.add(idxOnScreen);
            drawLabel(fm, s, p, bgColor, _g, fgColor);
          }
        }
        _g.setFont(old);

      }
    }
  }

  @Override
  public boolean addAtomeProjectedOnSelectedGeometry(GrPoint grPoint, CtuluCommandComposite cmp, CtuluUI _ui) {
    return false;
  }

  @Override
  public boolean addForme(final GrObjet _o, final int _deforme, final CtuluCommandContainer _cmd, final CtuluUI _ui,
          final ZEditionAttributesDataI _data) {
    if (_deforme == DeForme.POINT) {
      ((ZModelePointEditableInterface) modele_).addPoint((GrPoint) _o, _data, _cmd);
      clearCacheAndRepaint();
      return true;
    }
    return false;
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return _typeForme == DeForme.POINT;
  }

  @Override
  public boolean isAtomicMode() {
    return getSelectionMode() == SelectionMode.ATOMIC;
  }

  @Override
  public boolean canUseAtomicMode() {
    return false;
  }

  @Override
  public boolean canUseSegmentMode() {
    return false;
  }

  @Override
  public String editSelected() {
    return editor_ == null ? EbliLib.getS("Edition impossible") : editor_.edit();
  }

  @Override
  public final ZEditorInterface getEditor() {
    return editor_;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new VariableFindExpression();
  }

  @Override
  public ZModeleEditable getModelEditable() {
    return (ZModeleEditable) modeleDonnees();
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }
  /*
   public boolean isAtomicMode() {
   return false;
   }*/

  @Override
  public SelectionMode getSelectionMode() {
    return SelectionMode.NORMAL;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public void setModele(final ZModelePoint _modele) {
    if (null == _modele || _modele instanceof ZModelePointEditableInterface) {
      if (modele_ != null) {
        ((ZModelePointEditableInterface) modele_).removeModelListener(this);
      }
      super.setModele(_modele);
      if (_modele != null) {
        ((ZModelePointEditableInterface) _modele).addModelListener(this);
      }
    } else {
      throw new IllegalArgumentException("bad model");
    }
  }

  @Override
  public boolean moveSelectedObjects(final double _dx, final double _dy, double _dz, final CtuluCommandContainer _cmd,
          final CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final int min = selection_.getMinIndex();
    final ZModelePointEditableInterface modele = (ZModelePointEditableInterface) modele_;
    for (int i = selection_.getMaxIndex(); i >= min; i--) {
      if (selection_.isSelected(i)) {
        modele.setPoint(i, modele.getX(i) + _dx, modele.getY(i) + _dy, cmp);
        GISAttributeInterface zAtt = modele.getGeomData().getAttributeIsZ();
        // Attribut Z non null => On change la valeur.
        if (zAtt != null) {
          if (modele.getGeomData().getModel(zAtt) != null && modele.getGeomData().getModel(zAtt).getObjectValueAt(i) instanceof Double) {
            double oldVal = ((Double) modele.getGeomData().getModel(zAtt).getObjectValueAt(i)).doubleValue();
            modele.getGeomData().getModel(zAtt).setObject(i, new Double(oldVal + _dz), cmp);
          } else {
            FuLog.warning("EBL:Pb l'attribut pour Z n'est pas dans la liste des attributs du calque!");
          }
        }
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return true;
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#rotateSelectedObjects(double, double, double, org.fudaa.ctulu.CtuluCommandContainer, org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public boolean rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final int min = selection_.getMinIndex();
    final ZModelePointEditableInterface modele = (ZModelePointEditableInterface) modele_;
    for (int i = selection_.getMaxIndex(); i >= min; i--) {
      if (selection_.isSelected(i)) {
        /// Ramene � l'origine, et rotation.
        double dxold = modele.getX(i) - _xreel0;
        double dyold = modele.getY(i) - _yreel0;
        double dxnew = dxold * Math.cos(_angRad) - dyold * Math.sin(_angRad);
        double dynew = dxold * Math.sin(_angRad) + dyold * Math.cos(_angRad);

        modele.setPoint(i, dxnew + _xreel0, dynew + _yreel0, cmp);
      }
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

    return false;
  }

  @Override
  public void paintDeplacement(final Graphics2D _g2d, final int _dx, final int _dy, final TraceIcon _ic) {
    if (!isSelectionEmpty()) {
      final int min = selection_.getMinIndex();
      GrPoint pt = null;
      final GrMorphisme versEcran = getVersEcran();
      final TraceIcon icone = _ic;
      Color c = _ic.getCouleur();
      if (isAttenue()) {
        c = attenueCouleur(c);
      }
      _g2d.setColor(c);
      for (int i = selection_.getMaxIndex(); i >= min; i--) {
        if (selection_.isSelected(i)) {
          if (pt == null) {
            pt = new GrPoint();
          }
          modele_.point(pt, i, true);
          pt.autoApplique(versEcran);
          pt.x_ += _dx;
          pt.y_ += _dy;
          icone.paintIconCentre(this, _g2d, pt.x_, pt.y_);
        }
      }
    }
  }

  @Override
  public boolean removeSelectedObjects(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (!isSelectionEmpty()) {
      final boolean r = ((ZModelePointEditableInterface) modele_).removePoint(getSelectedIndex(), _cmd);
      if (r) {
        clearSelection();
      }
      return r;
    }
    return false;
  }
  /*
   public boolean setAtomicMode(final boolean _b) {
   return false;
   }
   */

  @Override
  public boolean setSelectionMode(SelectionMode mode) {
    return false;
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#copySelectedObjects(org.fudaa.ctulu.CtuluCommandContainer, org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public boolean copySelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty() || (getSelectionMode() != SelectionMode.NORMAL)) {
      return false;
    }
    int nb = modele_.getNombre();

    GrSegment pdec = new GrSegment(new GrPoint(0, 5, 0), new GrPoint(5, 0, 0));
    pdec.autoApplique(getVersReel());
    if (!((ZModelePointEditable) modele_).copyGlobal(selection_, 0/*pdec.getVx()*/, 0/*pdec.getVy()*/, _cmd)) {
      return false;
    }


    // Changement de selection.
    int[] isels = new int[selection_.getNbSelectedIndex()];
    for (int i = 0; i < isels.length; i++) {
      isels[i] = nb + i;
    }
    setSelection(isels);
    return true;
  }

  @Override
  public final void setEditor(final ZEditorInterface _editor) {
    editor_ = _editor;
  }

  /* Sans objet */
  @Override
  public boolean splitSelectedObject(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  /* Sans objet */
  @Override
  public boolean joinSelectedObjects(final CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
    clearCacheAndRepaint();
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexGeom, Object _newValue) {
    clearCacheAndRepaint();
  }

  @Override
  public void geometryAction(Object _source, int _indexGeom, Geometry _geom, int _action) {
    clearCacheAndRepaint();
  }
}
