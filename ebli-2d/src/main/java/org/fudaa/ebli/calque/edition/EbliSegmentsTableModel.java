package org.fudaa.ebli.calque.edition;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import java.util.Arrays;
import java.util.HashMap;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;

@SuppressWarnings("serial")
public class EbliSegmentsTableModel extends AbstractTableModel
{
  private static class Cell
  {
    private final int row;
    private final int column;
    
    public Cell(int row, int column)
    {
      this.row = row;
      this.column = column;
    }

    public int getRow()
    {
      return row;
    }

    public int getColumn()
    {
      return column;
    }

    @Override
    public int hashCode()
    {
      final int prime = 31;
      int result = 1;
      result = prime * result + column;
      result = prime * result + row;
      return result;
    }

    @Override
    public boolean equals(Object obj)
    {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      Cell other = (Cell) obj;
      if (column != other.column)
        return false;
      if (row != other.row)
        return false;
      return true;
    }
  }
  
  private final static int MIN_NB_COLUMN = 5;
  private final static int V1_X_COLUMN = 1;
  private final static int V1_Y_COLUMN = 2;
  private final static int V2_X_COLUMN = 3;
  private final static int V2_Y_COLUMN = 4;
  
  final int idx;
  final GISZoneCollection zone;
//  final EbliFormatterInterface formatter;
  final EbliCoordinateDefinition[] coordDefs;
  final GISAttributeModel[] modeles;
  final ZModeleLigneBriseeEditable modele;
  TIntObjectHashMap<Coordinate> newCoordinates;
  final CoordinateSequence coordinateSequence;
  final boolean isFerme;
  final int maxNbPts;
  final int[] segments;
  final HashMap<Cell, Cell> cellsLinked = new HashMap<Cell, Cell>();
  
  public EbliSegmentsTableModel(final EbliCoordinateDefinition[] _defs, final GISZoneCollection zone, ZModeleLigneBriseeEditable modele, int idx, int[] segments, boolean useAttribute)
  {
    Arrays.sort(segments);

    this.idx = idx;
    this.segments = segments;
    this.coordDefs = _defs;
    this.zone = zone;
    this.modele = modele;
    this.modeles = useAttribute ? zone.getAtomicAttributeSubModel(idx) : null;
    this.coordinateSequence = zone.getCoordinateSequence(idx);
    this.isFerme = modele.isGeometryFermee(idx);
    this.maxNbPts = modele.getNbPointForGeometry(idx);

    TIntIntHashMap segmentsRows = new TIntIntHashMap();
    for (int i = 0; i < segments.length; i++)
    {
      segmentsRows.put(segments[i], i);
    }

    for (int i = 0; i < segments.length; i++)
    {
      int nextSegment = (segments[i] + 1) % this.maxNbPts;
      
      if (segmentsRows.containsKey(nextSegment))
      {
        int currentRow = segmentsRows.get(segments[i]);
        int nextRow = segmentsRows.get(nextSegment);
        
        Cell cell11 = new Cell(currentRow, V2_X_COLUMN);
        Cell cell12 = new Cell(currentRow, V2_Y_COLUMN);
        Cell cell21 = new Cell(nextRow, V1_X_COLUMN);
        Cell cell22 = new Cell(nextRow, V1_Y_COLUMN);
        
        cellsLinked.put(cell11, cell21);
        cellsLinked.put(cell12, cell22);
        cellsLinked.put(cell21, cell11);
        cellsLinked.put(cell22, cell12);
      }
    }
  }
  
  private int getVertexIdx(int rowIdx, int columnIdx)
  {
    if (columnIdx < V2_X_COLUMN)
    {
      return segments[rowIdx];
    }
    else
    {
      return ((segments[rowIdx] + 1) % maxNbPts);
    }
  }
  
  @Override
  public int getColumnCount()
  {
    return (modeles == null ? 0 : modeles.length) + MIN_NB_COLUMN;
  }

  @Override
  public int getRowCount()
  {
    return segments.length;
  }

  private double getX(final int vertexIdx) {
    if (newCoordinates != null && newCoordinates.contains(vertexIdx))
    {
      return ((Coordinate) newCoordinates.get(vertexIdx)).x;
    }
    return coordinateSequence.getX(vertexIdx);
  }

  private double getY(final int vertexIdx) {
    if (newCoordinates != null && newCoordinates.contains(vertexIdx))
    {
      return ((Coordinate) newCoordinates.get(vertexIdx)).y;
    }
    return coordinateSequence.getY(vertexIdx);
  }

  @Override
  public Object getValueAt(int rowIdx, int columnIdx)
  {
    int vertexIdx = this.getVertexIdx(rowIdx, columnIdx);
    switch (columnIdx)
    {
      case 0 :
      {
        return String.valueOf(segments[rowIdx] + 1);
      }
      case V1_X_COLUMN :
      case V2_X_COLUMN :
      {
        return this.coordDefs[0].getFormatter().getXYFormatter().format(this.getX(vertexIdx));
      }
      case V1_Y_COLUMN :
      case V2_Y_COLUMN :
      {
        return this.coordDefs[1].getFormatter().getXYFormatter().format(this.getY(vertexIdx));
      }
    }
    return null;
  }

  @Override
  public String getColumnName(final int _columnIndex) {
    switch (_columnIndex)
    {
      case 0 :
      {
        return EbliLib.getS("Index");
      }
      case V1_X_COLUMN :
      {
        return coordDefs[0].getName()+"1";
      }
      case V1_Y_COLUMN :
      {
        return coordDefs[1].getName()+"1";
      }
      case V2_X_COLUMN :
      {
        return coordDefs[0].getName()+"2";
      }
      case V2_Y_COLUMN :
      {
        return coordDefs[1].getName()+"2";
      }
    }
    final int idx = _columnIndex - MIN_NB_COLUMN;
    if (idx >= 0)
    {
      return modeles[idx].getAttribute().getName();
    }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public boolean isCellEditable(final int rowIndex, final int columnIndex) {
    if (columnIndex == 0)
    { 
      return false;
    }
    else if (columnIndex < MIN_NB_COLUMN)
    {
      return zone.isGeomModifiable();
    }
    
    return true;
  }

  public void updateCellEditor(final JTable _table) {
    final TableColumnModel cols = _table.getColumnModel();
    // xy
    final TableCellEditor edx = coordDefs[0].getFormatter().createTableEditorComponent();
    final TableCellEditor edy = coordDefs[1].getFormatter().createTableEditorComponent();
    cols.getColumn(V1_X_COLUMN).setCellEditor(edx);
    cols.getColumn(V1_Y_COLUMN).setCellEditor(edy);
    cols.getColumn(V2_X_COLUMN).setCellEditor(edx);
    cols.getColumn(V2_Y_COLUMN).setCellEditor(edy);
    // les attribut
    if (modeles != null) {
      for (int i = 0; i < modeles.length; i++) {
        final CtuluValueEditorI editor = modeles[i].getAttribute().getEditor();
        if (editor != null) {
          cols.getColumn(MIN_NB_COLUMN + i).setCellEditor(editor.createTableEditorComponent());
        }
      }
    }
  }
  
  @Override
  public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
    if (!this.isCellEditable(rowIndex, columnIndex))
    {
      return;
    }
    
    final int segmentIdx = segments[rowIndex];//lines.get(rowIndex).getSegmentIdx();
    
    if (columnIndex >= MIN_NB_COLUMN)
    {
      // l'indice de l'attribut atomic
      final int val = columnIndex - MIN_NB_COLUMN;
      /*
      if (modelesNewValues_ == null) {
        modelesNewValues_ = new TIntObjectHashMap[modeles_.length];
      }
      if (modelesNewValues_[val] == null) {
        modelesNewValues_[val] = new TIntObjectHashMap();
      }
      modelesNewValues_[val].put(realIdx, _value);
      */
    }
    else
    {
      int vertexIdx = this.getVertexIdx(rowIndex, columnIndex);

      if (newCoordinates == null) {
        newCoordinates = new TIntObjectHashMap<Coordinate>(coordinateSequence.size());
      }
      Coordinate c = (Coordinate) newCoordinates.get(vertexIdx);
      if (c == null) {
        c = new Coordinate();
        c.x = coordinateSequence.getX(vertexIdx);
        c.y = coordinateSequence.getY(vertexIdx);
        newCoordinates.put(vertexIdx, c);
      }
      if (((columnIndex - 1) % 2) == 0) {
        c.x = ((Double)value).doubleValue();
      } else {
        c.y = ((Double)value).doubleValue();
      }
      
      Cell cell = new Cell(rowIndex, columnIndex);
      
      if (cellsLinked.containsKey(cell))
      {
        cell = cellsLinked.get(cell);
        
        fireTableCellUpdated(cell.getRow(), cell.getColumn());
      }
    }
  }
  
  /**
   * @return les nouvelles coordonnees. null si pas de modif
   */
  public CoordinateSequence getNewCoordinateSequence() {
    if (newCoordinates == null) { return null; }
    
    final GISCoordinateSequence newSeq = new GISCoordinateSequence(coordinateSequence);
    final TIntObjectIterator<Coordinate> it = newCoordinates.iterator();
    for (int j = newCoordinates.size(); j-- > 0;) {
      it.advance();
      final int idx = it.key();
      final Coordinate c = (Coordinate) it.value();
      newSeq.setX(idx, c.x);
      newSeq.setY(idx, c.y);
    }
    if (isFerme && newCoordinates.contains(0)) {
      final int last = newSeq.size() - 1;
      final Coordinate c = (Coordinate) newCoordinates.get(0);
      newSeq.setX(last, c.x);
      newSeq.setY(last, c.y);
    }
    return newSeq;
  }

  protected void apply(final CtuluCommandComposite _cmp) {
/*
    if (modelesNewValues_ != null) {
      for (int i = modelesNewValues_.length - 1; i >= 0; i--) {
        final TIntObjectHashMap val = modelesNewValues_[i];
        if (val != null) {
          final TIntObjectIterator it = val.iterator();
          final GISAttributeModel dataModel = modeles_[i];
          for (int j = val.size(); j-- > 0;) {
            it.advance();
            dataModel.setObject(it.key(), it.value(), _cmp);
          }
        }
      }
    }*/
    final CoordinateSequence newSeq = this.getNewCoordinateSequence();
    if (newSeq != null) {
      zone.setCoordinateSequence(idx, newSeq, _cmp);
    }
  }

  protected CtuluAnalyze isValid() {
    if (modele == null) { return null; }
    final CoordinateSequence seq = getNewCoordinateSequence();
    if (seq != null) {
      final CtuluAnalyze ana = new CtuluAnalyze();
      if (!modele.isCoordinateValid(seq, ana)) { return ana; }
    }
    return null;
  }
}

