/*
 * @file         BCalqueSymbole.java
 * @creation     1999-09-23
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrSymbole;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrSymbole;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Une classe d'affichage de symboles. Les symboles sont ancr�s � un point 3D,
 * peuvent ou non suivre la rotation. Leur taille est fixe.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class BCalqueSymbole extends BCalqueAffichage {

  protected VecteurGrSymbole symboles_;
  protected int typeTrait_;
  protected boolean fondVisible_;

  // Constructeur
  public BCalqueSymbole() {
    super();
    reinitialise();
    typeTrait_ = TraceLigne.LISSE;
    fondVisible_ = false;
  }

  @Override
  public void paintComponent(final Graphics _g){
    super.paintComponent(_g);
    final TraceGeometrie tg = new TraceGeometrie(getVersEcran());
    tg.setTypeTrait(typeTrait_);
    Color fg = getForeground();
    Color bg = getBackground();
    if (isAttenue()) {
      fg = attenueCouleur(fg);
    }
    if (isAttenue()) {
      bg = attenueCouleur(fg);
    }
    tg.setForeground(fg);
    tg.setBackground(bg);
    for (int i = 0; i < symboles_.nombre(); i++) {
      tg.dessineSymbole((Graphics2D) _g, symboles_.renvoie(i), isFondVisible(), isRapide());
    }
  }

  /**
   * Indique si le trac� des symboles est fait avec la couleur de fond.
   * @paramm _visible <i>true</i>Les symboles sont trac�s avec un fond.
   */
  public void setFondVisible(final boolean _visible){
    fondVisible_ = _visible;
  }

  /**
   * Retourne si le fond des symboles est trac� avec la couleur de fond ou non.
   * @return <i>true</i>Les symboles sont trac�s avec un fond.
   */
  public boolean isFondVisible(){
    return fondVisible_;
  }

  /**
   * Reinitialise la liste de vecteurs.
   */
  public final void reinitialise(){
    symboles_ = new VecteurGrSymbole();
  }

  /**
   * Ajoute un symbole au calque.
   */
  public void ajoute(final GrSymbole _s){
    symboles_.ajoute(_s);
  }

  /**
   * Retire un symbole du calque.
   */
  public void enleve(final GrSymbole _s){
    symboles_.enleve(_s);
  }

  public VecteurGrSymbole getSymboles(){
    return symboles_;
  }

  public void setSymboles(final VecteurGrSymbole _ls){
    if (_ls == symboles_) {
      return;
    }
    final VecteurGrSymbole vs = symboles_;
    reinitialise();
    for (int i = 0; i < _ls.nombre(); i++) {
      ajoute(_ls.renvoie(i));
    }
    firePropertyChange("symboles", vs, symboles_);
  }

  /**
   * Renvoie le l-ieme symbole de ce calque.
   */
  public GrSymbole getSymbole(final int _l){
    return symboles_.renvoie(_l);
  }

  /**
   * Nombre de symboles dans ce calque.
   */
  public int nombre(){
    return symboles_.nombre();
  }

  /**
   * Renvoi de la liste des symboles selectionnables.
   */
  @Override
  public VecteurGrContour contours(){
    final VecteurGrContour res = new VecteurGrContour();
    res.tableau(symboles_.tableau());
    return res;
  }

  /**
   * Renvoie la boite englobante de tous les GrSymboles du calque. Seuls les
   * points origines des symboles sont pris en compte dans ce calcul
   */
  @Override
  public GrBoite getDomaine(){
    GrBoite r = null;
    if (isVisible()) {
      r = super.getDomaine();
      if (symboles_.nombre() > 0) {
        if (r == null) {
          r = new GrBoite();
        }
        for (int i = 0; i < symboles_.nombre(); i++) {
          r.ajuste(symboles_.renvoie(i).position_);
        }
      }
    }
    return r;
  }

  /**
   * Accesseur de la propriete <I>typeTrait</I>.
   * Elle definit le style de trace des points, en prenant ses valeurs dans
   * les champs statiques de <I>Tracepoint</I>.
   * Par defaut, elle vaut TraceLigne.CONTINU.
   * @see TraceLigne
   */
  public int getTypeTrait(){
    return typeTrait_;
  }

  /**
   * Affectation de la propriete <I>typeTrait</I>.
   */
  public void setTypeTrait(final int _typeTrait){
    if (typeTrait_ != _typeTrait) {
      final int vp = typeTrait_;
      typeTrait_ = _typeTrait;
      firePropertyChange("typeTrait", vp, typeTrait_);
    }
  }
}
