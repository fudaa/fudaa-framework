/*
 * @file         SelectionEvent.java
 * @creation     1998-12-15
 * @modification $Date: 2006-07-13 13:35:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.util.EventObject;
import org.fudaa.ebli.geometrie.VecteurGrContour;
/**
 * Un �v�nement <I>SelectionEvent</I>. Cet �venement est d�clench� par un calque de
 * s�lection et contient la liste des GrContour.
 *
 * @version      $Id: SelectionEvent.java,v 1.7 2006-07-13 13:35:45 deniger Exp $
 * @author       Bertrand Marchand
 */
public class SelectionEvent extends EventObject {
  /**
   * Identificateur de groupe NULL.
   */
  public static final int LAST= 0;
  public static final int HAS_NEXT= 1;
  private int position_;
  private VecteurGrContour objets_;
  /**
   * Construction d'un �v�nement ne faisant pas partie d'un groupe.
   * @param _source Objet d�clencheur de l'�v�nement. En g�n�ral le calque
   *                contenant les objets s�lectionn�s
   * @param _objets objets s�lectionn�s
   */
  public SelectionEvent(final Object _source, final VecteurGrContour _objets) {
    this(_source, _objets, LAST);
  }
  /**
   * Construction de l'�venement.
   * @param _source Objet d�clencheur de l'�v�nement. En g�n�ral le calque
   *                contenant les objets s�lectionn�s
   * @param _objets objets s�lectionn�s
   * @param _position tous les �v�nement d'un m�me groupe doivent avoir comme
   *                  position HAS_NEXT sauf le dernier du groupe qui doit avoir
   *                  pour position LAST
   */
  public SelectionEvent(
    final Object _source,
    final VecteurGrContour _objets,
    final int _position) {
    super(_source);
    objets_= _objets;
    position_= _position;
  }
  /**
   * Retourne la liste des objets s�lectionn�s.
   * @return Objets s�lectionn�s
   */
  public VecteurGrContour getObjects() {
    return objets_;
  }
  /**
   * Retourne si un autre �v�nement du m�me groupe est attendu.
   * @return true Un autre �v�nement est attendu
   *         false C'est le dernier �v�nement du groupe
   */
  public boolean hasNext() {
    return position_ == HAS_NEXT;
  }
}
