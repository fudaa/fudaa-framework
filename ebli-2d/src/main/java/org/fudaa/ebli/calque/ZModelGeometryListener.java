/*
 * @creation     29 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.gis.GISAttributeInterface;

/**
 * Cette interface permet l'�coute des changements concernant les g�om�tries et
 * leurs attributs. Les changements sont : l'ajout d'un attribut, la suppression
 * d'un attribut, le changement d'une valeur d'un attribut, l'ajout d'une
 * g�om�trie, la supression d'une g�om�trie, le remplacement d'une g�om�trie par
 * une autre et la modification interne d'une g�om�trie (g�n�ralement l'ajout ou
 * la suppression de points).
 * 
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface ZModelGeometryListener extends ZModelListener {

  /** Action indiquant l'ajout d'un attribut dans le conteneur d'attributs. */
  public static final int ATTRIBUTE_ACTION_ADD=0;
  /**
   * Action indiquant la supression d'un attribut dans le conteneur d'attributs.
   */
  public static final int ATTRIBUTE_ACTION_REMOVE=1;

  /**
   * M�thode appel�e lorsqu'une action d'ajout ou de suppression est effectu�e
   * sur un attribut.
   * 
   * @param _source
   *          L'instance appelant attributeAction.
   * @param _indexAtt
   *          L'index de l'attribut. Si l'action est l'ajout d'un attribut,
   *          l'index est valide. Si l'action est la supression, l'index
   *          correspond � l'ancienne emplacement et donc n'est plus valide. Si
   *          �gale � -1, tout les attributs ont pu �tre modifi�s.
   * @param _att
   *          L'attribut concern�. Si null tout les attributs ont pu �tre
   *          modifi�s.
   * @param _action
   *          L'action effectu�e sur la g�om�trie. Voir les ATTRIBUTE_ACTION_*.
   */
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action);

  /**
   * M�thode appel�e lorsqu'une action de modification de la valeur d'un
   * attribut est effectu�e.
   * 
   * @param _source
   *          L'instance appelant attributeValueChangeAction.
   * @param _indexAtt
   *          L'index de l'attribut. Si �gale � -1, tout les attributs ont pu
   *          �tre modifi�s.
   * @param _att
   *          L'attribut concern�. Si null tout les attributs ont pu �tre
   *          modifi�s.
   * @param _indexGeom
   *          La g�om�trie concern�e. Si �gale � -1, toutes les g�om�tries ont
   *          pu �tre modifi�es.
   * @param _newValue
   *          La nouvelle valeur de l'attribut pour la g�om�trie indiqu�e. Si
   *          null toutes les g�om�tries ont pu �tre modifi�es.
   */
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexGeom, Object _newValue);

}
