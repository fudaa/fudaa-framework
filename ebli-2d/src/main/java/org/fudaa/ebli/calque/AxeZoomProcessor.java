package org.fudaa.ebli.calque;

import java.awt.Insets;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.repere.RepereEvent;
/**
 * Redimensionnement du calque.
 * @author Adrien
 *
 */
public class AxeZoomProcessor {

	
	public RepereEvent process(final Object _source, final GrBoite _boite, final double _rz, final int mrgHorizontal, final int mrgVertical,
	           boolean _rapide,  boolean _changeZ, BVueCalque vue, boolean zoomX) {
		
		
		  final Insets insets = vue.getInsets();
	      final double wd = _boite.getDeltaX();
	      final double hd = _boite.getDeltaY();
	      int margeGauche = mrgHorizontal;
	      int margeDroite = mrgHorizontal;
	      int margeHaut = mrgVertical;
	      int margeBas = mrgVertical;

	      if (vue.getCalqueInsets() != null) {
	    	//-- voir si le graphe ca joue mieux --//  
	    	//vue.getCalqueInsets().left = 0;
	    	 
	        margeGauche = vue.getCalqueInsets().left;
	        margeDroite = vue.getCalqueInsets().right;
	        margeHaut = vue.getCalqueInsets().top;
	        margeBas = vue.getCalqueInsets().bottom;
	      }

	      final double wv = vue.getWidth() - insets.left - insets.right - margeGauche - margeDroite;
	      final double hv = vue.getHeight() - insets.top - insets.bottom - margeHaut - margeBas;
	      final RepereEvent repere = new RepereEvent(_source, _rapide);
	     // final double e = Math.max(wd / wv, hd / hv);
	      double e = zoomX?(wd / wv):hd / hv;
	      repere.ajouteTransformation(RepereEvent.ZOOM, e, false);
	      vue.margeXCentre = (wv - wd / e) / 2;
	      repere.ajouteTransformation(RepereEvent.TRANS_X, _boite.o_.x_ / e - vue.margeXCentre - margeGauche, false);
	      vue.margeYCentre = (hv - hd / e) / 2;
	      repere.ajouteTransformation(RepereEvent.TRANS_Y, _boite.o_.y_ / e - vue.margeYCentre - margeBas, false);
	      repere.ajouteTransformation(RepereEvent.TRANS_Z, 0., false);
	      repere.ajouteTransformation(RepereEvent.ROT_X, 0., false);
	      repere.ajouteTransformation(RepereEvent.ROT_Y, 0., false);
	      if (_changeZ) {
	        repere.ajouteTransformation(RepereEvent.ROT_Z, _rz, false);
	      }
	    return repere;
	}

}
