/*
 * @creation     17 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuResource;
import org.locationtech.jts.geom.Coordinate;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZCatchEvent;
import org.fudaa.ebli.calque.ZCatchListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Permet de visionner des distances.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class ZCalqueDistanceInteraction extends BCalqueInteraction implements MouseListener, MouseMotionListener, ZCatchListener {
  private static final Cursor CURSOR_DST_=EbliResource.EBLI.getCursor("curseur_distance", -1, new Point(6, 6));
  
  /** Liste des points composant le chemin affich�. Se sont des coordonn�s r�els. */
  protected List<Coordinate> chemin_=new ArrayList<Coordinate>();  
  /** La coordonn�e correspondant � la position de la souris. C'est une coordonn�s �cran. */
  protected Coordinate coordSouris_=new Coordinate();
  /** Indique si un chemin est dessin� � l'�cran. */
  private boolean cheminIsPaint_=false;
  /** Indique si la partie variable (du dernier point � la souris) est dessin�e � l'�cran. */
  private boolean variableIsPaint_=false;
  /** Traceur de ligne. */
  private TraceLigne traceLigne_=new TraceLigne(TraceLigne.LISSE, 1.5f, Color.RED);
  /** Indique si la saisie du chemin est termin�e. */
  protected boolean cheminComplet_=false;
  /** Indique si la touche 'CTRL' est active. */
  protected boolean ctrlActif_=false;
  /** Listener des distances. */
  private List<PropertyChangeListener> listeners_=new ArrayList<PropertyChangeListener>();
  /** Point d'accrochage si existant. null si la souris n'est pas sur un point d'accorchage */
  private GrPoint ptAccroch_=null;
  
  public ZCalqueDistanceInteraction(){
    setName("cqInteractifDistance");
  }
  
  // M�thodes utilitaires \\
  
  /** Converti une coordonn�e reel en coordonn�e �cran. */ 
  protected Coordinate reelToEcran(Coordinate coord){
    GrPoint tmp=new GrPoint(coord.x, coord.y, 0);
    tmp.autoApplique(getVersEcran());
    return new Coordinate(tmp.x_, tmp.y_);
  }
  
  /** Converti une coordonn�e �cran en coordonn�e r�el. */ 
  protected Coordinate ecranToReel(Coordinate coord){
    GrPoint tmp=new GrPoint(coord.x, coord.y, 0);
    tmp.autoApplique(getVersReel());
    return new Coordinate(tmp.x_, tmp.y_);
  }

  /** Dessine un trait en mode XOR � l'�cran entre les deux coordonn�es fournies. */
  private void dessineTrait(Graphics2D _g, Coordinate coor1, Coordinate coord2){
    if(_g!=null){
      _g.setXORMode(getBackground());
      traceLigne_.dessineTrait(_g, coor1.x, coor1.y, coord2.x, coord2.y);
    }
  }
  
  // M�thodes d�riv�es \\

  @Override
  public Cursor getSpecificCursor() {
    return CURSOR_DST_;
  }

  @Override
  public boolean alwaysPaint() {
    return true;
  }

  @Override
  public String getDescription() {
    return BuResource.BU.getString("Distance");
  }
  
  @Override
  public void paintComponent(final Graphics _g) {
    effaceChemin();
    super.paintComponent(_g);
    // Affichage du chemin avec le graphics pass� en param�tre
    if(chemin_.size()>1){
      for(int i=1; i<chemin_.size();i++)
        dessineTrait((Graphics2D) _g, reelToEcran(chemin_.get(i-1)), reelToEcran(chemin_.get(i)));
      cheminIsPaint_=true;
    }
  }
  
  // M�thodes de base de dessin du chemin \\
  
  /** Dessine tout le chemin � l'�cran sauf la partie variable (du dernier point � la souris). */
  protected void afficheChemin(){
    if(!cheminIsPaint_&&chemin_.size()>1){
      for(int i=1; i<chemin_.size();i++)
        dessineTrait((Graphics2D) getGraphics(), reelToEcran(chemin_.get(i-1)), reelToEcran(chemin_.get(i)));
      cheminIsPaint_=true;
    }
  }
  /** Efface tout le chemin � l'�cran sauf la partie cariable (du dernier point � la souris).*/
  protected void effaceChemin(){
    if(cheminIsPaint_&&chemin_.size()>1){
      for(int i=1; i<chemin_.size();i++)
        dessineTrait((Graphics2D) getGraphics(), reelToEcran(chemin_.get(i-1)), reelToEcran(chemin_.get(i)));
      cheminIsPaint_=false;
    }
  }
  /** Dessine la partie variable (du dernier point � la souris) � l'�cran. */
  protected void afficheVariable(){
    if(!variableIsPaint_&&chemin_.size()>=1){
      dessineTrait((Graphics2D) getGraphics(), reelToEcran(chemin_.get(chemin_.size()-1)), coordSouris_);
      variableIsPaint_=true;
    }
  }
  /** Efface la partie variable (du dernier point � la souris) � l'�cran. */
  protected void effaceVariable(){
    if(variableIsPaint_&&chemin_.size()>=1){
      dessineTrait((Graphics2D) getGraphics(), reelToEcran(chemin_.get(chemin_.size()-1)), coordSouris_);
      variableIsPaint_=false;
    }
  }
  
  // M�thodes de controle ext�rieur et de r�cup�ration d'informations. \\

  /** Annule l'�dition en cours. */
  public void cancelEdition() {
    effaceChemin();
    effaceVariable();
    chemin_.clear();
    cheminComplet_=true;
    repaint();
  }
  
  /** @return retourne la distance totale. */
  public double getDistanceCumulee(){
    double distance=getDistanceVariable();
    for(int i=1; i<chemin_.size();i++)
      distance+=chemin_.get(i-1).distance(chemin_.get(i));
    return distance;
  }
  
  /** @return retourne la distance variable (du dernier point � la souris). */
  public double getDistanceVariable(){
    if(chemin_.size()>=1)
      return chemin_.get(chemin_.size()-1).distance(ecranToReel(coordSouris_));
    else
      return 0;
  }

  public void addDistanceChangeListener(PropertyChangeListener _listener){
    if(!listeners_.contains(_listener))
      listeners_.add(_listener);
  }
  
  public void removeDistanceChangeListener(PropertyChangeListener _listener){
    if(listeners_.contains(_listener))
      listeners_.remove(_listener);
  }
  
  protected void fireDistanceChangeListener(){
    for(PropertyChangeListener listener:listeners_)
      listener.propertyChange(new PropertyChangeEvent(this, "distance", null, null));
  }
  
  // M�thodes g�rant les �v�nements. \\

  @Override
  public void mouseClicked(final MouseEvent _evt) {
  }

  @Override
  public void mouseEntered(final MouseEvent _evt) {
    // Affichage du chemin sans XOR pour r�afficher correctement la forme apr�s un d�placement des fen�tre
    for(int i=1; i<chemin_.size();i++)
      traceLigne_.dessineTrait((Graphics2D) getGraphics(), reelToEcran(chemin_.get(i-1)).x, reelToEcran(chemin_.get(i-1)).y, reelToEcran(chemin_.get(i)).x, reelToEcran(chemin_.get(i)).y);
  }

  @Override
  public void mousePressed(final MouseEvent _evt) {}
  
  @Override
  public void mouseDragged(final MouseEvent _evt) {
    mouseMoved(_evt);
  }
  
  @Override
  public void mouseExited(final MouseEvent _evt) {
    effaceVariable();
  }

  /** Met � jour la partie variable du chemin. */
  @Override
  public void mouseMoved(final MouseEvent _evt) {
    if(isGele()) return;

    if (!cheminComplet_){
      effaceVariable();
      coordSouris_.x=_evt.getX();
      coordSouris_.y=_evt.getY();
      afficheVariable();
      fireDistanceChangeListener();
    }
  }

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (!isGele()&&_evt.getButton()==MouseEvent.BUTTON1) {
      GrPoint pt=new GrPoint(_evt.getX(),_evt.getY(),0);
      
      if (ptAccroch_!=null) 
        pt=ptAccroch_.applique(getVersEcran());
      
      effaceChemin();
      effaceVariable();
      coordSouris_.x=pt.x_;
      coordSouris_.y=pt.y_;
      // Si le chemin �tait d�j� complet, on reprend � 0
      if(cheminComplet_){
        chemin_.clear();
        cheminComplet_=false;
        repaint();
      }
      chemin_.add(ecranToReel(new Coordinate(pt.x_,pt.y_)));
      // Si c'est le dernier point, le chemin est termin�
      cheminComplet_=chemin_.size()>1&&getDistanceCumulee()>0&&(_evt.getClickCount()>=2||_evt.isControlDown());
      afficheChemin();
      fireDistanceChangeListener();
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZCalqueCatchInteraction.ZCatchListener#catchChanged(org.fudaa.ebli.calque.ZCalqueCatchInteraction.ZCatchEvent)
   */
  @Override
  public void catchChanged(ZCatchEvent _evt) {
    if (isGele()) return;
    
    if (_evt.type==ZCatchEvent.CAUGHT) {
      ptAccroch_ = _evt.selection.getScene().getVertex(_evt.idxGeom, _evt.idxVertex);
    }
    else {
      ptAccroch_=null;
    }
  }

  @Override
  public boolean isCachingEnabled() {
    return !isGele();
  }
}
