/*
 *  @creation     5 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuToolButton;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliFormatterInterface;

/**
 * Un panneau pour saisir les coordonnées de déplacement d'objets GIS. Ce panneau est intégré dans la palette des outils lorsque
 * l'outil "déplacement" est sélectionné.
 *
 * @author Fred Deniger
 * @version $Id: BPaletteDeplacement.java,v 1.1.2.1 2008-05-13 12:10:31 bmarchan Exp $
 */
public class BPaletteDeplacement extends JPanel implements BPalettePanelInterface, ZSelectionListener,
                                                           ZCalqueDeplacementInteraction.SpecPanel, ActionListener {

  protected JButton btApply_;
  protected ZCalqueDeplacementInteraction deplacement_;
  protected ZSceneEditor sceneEditor_;
  protected EbliFormatterInterface formatter_;
  protected BuTextField tfDx_;
  protected BuTextField tfDy_;
  protected BuTextField tfDz_;
  protected BuLabel lbDz_;

  /**
   * @param _deplacement le calque de deplacement
   * @param _formatter
   */
  public BPaletteDeplacement(ZSceneEditor _sceneEditor, EbliFormatterInterface _formatter) {
    super();

//    deplacement_ = _deplacement;
//    deplacement_.specPanel_ = this;

    formatter_ = _formatter;
    sceneEditor_ = _sceneEditor;

    tfDx_ = BuTextField.createDoubleField();
    tfDx_.setPreferredSize(new Dimension(90, tfDx_.getPreferredSize().height));
    tfDx_.setValue(new Double(0));
    tfDy_ = BuTextField.createDoubleField();
    tfDy_.setValue(new Double(0));
    tfDz_ = BuTextField.createDoubleField();
    tfDz_.setValue(new Double(0));
    lbDz_ = new BuLabel("dz:");

    btApply_ = new BuToolButton(BuResource.BU.getToolIcon("appliquer"));
    btApply_.addActionListener(this);

    buildComponent();
    setName("pnMove");
    sceneEditor_.getScene().addSelectionListener(this);
  }

  protected void buildComponent() {
    BuPanel pnMain = new BuPanel();
    pnMain.setLayout(new BuGridLayout(2, 2, 2));
    pnMain.add(new BuLabel("dx:"));
    pnMain.add(tfDx_);
    pnMain.add(new BuLabel("dy:"));
    pnMain.add(tfDy_);
    pnMain.add(lbDz_);
    pnMain.add(tfDz_);

    BuPanel pnButtons = new BuPanel();
    pnButtons.setLayout(new BorderLayout(5, 5));
    pnButtons.add(btApply_, BorderLayout.SOUTH);

    setLayout(new BorderLayout(5, 5));
    add(pnMain, BorderLayout.CENTER);
    add(pnButtons, BorderLayout.EAST);


  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (btApply_.isEnabled() && btApply_ == _e.getSource()) {
      Double dx = (Double) tfDx_.getValue();
      Double dy = (Double) tfDy_.getValue();
      Double dz = new Double(0);
      if (tfDz_.isEnabled()) {
        dz = (Double) tfDz_.getValue();
      }
      if (dx != null && dy != null && dz != null) {
        sceneEditor_.moveSelectedObjects(dx, dy, dz);
      }
    }
  }

  /**
   * @param _dep Le calque de déplacement : Si <code>null</code>, le déplacement ne se fait que par les Dx,Dy,DZ.
   */
  public void setCalqueDeplacement(ZCalqueDeplacementInteraction _dep) {
    deplacement_ = _dep;
    if (deplacement_!=null)
      deplacement_.specPanel_ = this;
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void updateState() {
    if (deplacement_!=null && deplacement_.isDragged()) {
      tfDx_.setEditable(false);
      tfDy_.setEditable(false);
      tfDz_.setEditable(false);
      btApply_.setEnabled(false);
      tfDx_.setText(formatter_.getXYFormatter().format(deplacement_.getReelDx()));
      tfDy_.setText(formatter_.getXYFormatter().format(deplacement_.getReelDy()));
      tfDz_.setText(formatter_.getXYFormatter().format(deplacement_.getReelDz()));
    } else {
      if (!tfDx_.isEditable()) {
        tfDx_.setText(CtuluLibString.ZERO);
        tfDy_.setText(CtuluLibString.ZERO);
        tfDz_.setText(CtuluLibString.ZERO);
      }
      tfDx_.setEditable(true);
      tfDy_.setEditable(true);
      tfDz_.setEditable(true);
      btApply_.setEnabled(!sceneEditor_.getScene().isSelectionEmpty());
    }

  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.commun.BPalettePanelInterface#doAfterDisplay()
   */
  @Override
  public void doAfterDisplay() {
    // TODO Auto-generated method stub
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.commun.BPalettePanelInterface#paletteDeactivated()
   */
  @Override
  public void paletteDeactivated() {
    FuLog.trace("BPaletteDeplacement.paletteDeactivated");
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.commun.BPalettePanelInterface#setPalettePanelTarget(java.lang.Object)
   */
  @Override
  public boolean setPalettePanelTarget(Object _target) {
    // TODO Auto-generated method stub
    return false;
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    updateState();
  }
}
