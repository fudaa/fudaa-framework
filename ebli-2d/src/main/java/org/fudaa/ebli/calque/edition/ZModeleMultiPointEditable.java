/*
 *  @creation     4 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TIntObjectIterator;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleArray;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZCalqueMultiPoint;
import org.fudaa.ebli.calque.ZModeleMultiPoint;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * Une implementation par defaut d'un mod�le �ditable pour des multipoints.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZModeleMultiPointEditable extends ZModeleGeometryDefault implements ZModeleMultiPoint, ZModeleEditable {

  /**
   * @author Bertrand Marchand
   * @version $Id$
   */
  public static class AtomesTableModel extends AbstractTableModel {

    GISAttributeInterface[] att_;

    final GISZoneCollectionMultiPoint ligne_;

    final int[] nbLineAtoms_;

    final boolean[] lineClosed_;

    final int nb_;

    public AtomesTableModel(final GISZoneCollectionMultiPoint _ligne) {
      ligne_ = _ligne;
      nbLineAtoms_ = new int[_ligne.getNumGeometries()];
      lineClosed_ = new boolean[_ligne.getNumGeometries()];
      int nb = 0;
      for (int i = nbLineAtoms_.length - 1; i >= 0; i--) {
        final Geometry r = (Geometry) _ligne.getGeometry(i);
        nbLineAtoms_[i] = r.getNumPoints();
        nb += nbLineAtoms_[i];
      }
      nb_ = nb;
      final List r = new ArrayList();
      for (int i = 0; i < ligne_.getNbAttributes(); i++) {
        if (ligne_.getAttribute(i).isUserVisible() && ligne_.getAttribute(i).isAtomicValue()) {
          r.add(ligne_.getAttribute(i));
        }
      }
      att_ = new GISAttributeInterface[r.size()];
      r.toArray(att_);
    }

    @Override
    public int getColumnCount() {
      // indice de la ligne, du point, x, y
      return 4 + att_.length;
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex <= 1) {
        return Integer.class;
      }
      if (_columnIndex >= 4) {
        return att_[_columnIndex - 4].getDataClass();
      }
      return Double.class;
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
      case 0:
        return "N� " + EbliLib.getS("Ligne");
      case 1:
        return "N� " + EbliLib.getS("Point");
      case 2:
        return EbliLib.getS("X");
      case 3:
        return EbliLib.getS("Y");
      default:
      }
      return att_[_column - 4].getName();
    }

    @Override
    public int getRowCount() {
      return nb_;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      final int n = nbLineAtoms_.length;
      int shiftL = 0;
      int idxLine = -1;
      int idxAtom = -1;
      for (int i = 0; i < n && idxLine < 0; i++) {
        if (_rowIndex < (shiftL + nbLineAtoms_[i])) {
          idxLine = i;
          idxAtom = _rowIndex - shiftL;
        }
        shiftL += nbLineAtoms_[i];
      }
      if (idxLine < 0) {
        FuLog.error("EZM: index in table not found");
        return CtuluLibString.EMPTY_STRING;
      }
      if (_columnIndex == 0) {
        return new Integer(idxLine + 1);
      }
      if (_columnIndex == 1) {
        return new Integer(idxAtom + 1);
      }
      final Geometry s = (Geometry) ligne_.getGeometry(idxLine);
      if (_columnIndex == 2) {
        return s.getCoordinates()[idxAtom].x;
      }
      if (_columnIndex == 3) {
        return s.getCoordinates()[idxAtom].y;
      }
      final CtuluCollection model = (CtuluCollection) ligne_.getModel(att_[_columnIndex - 4]).getObjectValueAt(idxLine);
      return model.getObjectValueAt(idxAtom);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id$
   */
  public static class LignesTableModel extends AbstractTableModel {

    GISAttributeInterface[] att_;

    final GISZoneCollectionMultiPoint ligne_;

    public LignesTableModel(final GISZoneCollectionMultiPoint _ligne) {
      ligne_ = _ligne;
      final List r = new ArrayList();
      for (int i = 0; i < ligne_.getNbAttributes(); i++) {
        if (ligne_.getAttribute(i).isUserVisible() && !ligne_.getAttribute(i).isAtomicValue()) {
          r.add(ligne_.getAttribute(i));
        }
      }
      att_ = new GISAttributeInterface[r.size()];
      r.toArray(att_);
    }

    @Override
    public int getColumnCount() {
      return 3 + att_.length;
    }

    @Override
    public String getColumnName(final int _column) {
      switch (_column) {
      case 0:
        return "N�";
      case 1:
        return EbliLib.getS("Nombre de points");
      case 2:
        return EbliLib.getS("Valide");
      default:
      }
      final int r = _column - 3;
      return att_[r].getLongName();
    }

    @Override
    public int getRowCount() {
      return ligne_.getNumGeometries();
    }

    final String valide_ = EbliLib.getS("Valide");
    final String nonValide_ = EbliLib.getS("Non Valide");

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return new Integer(_rowIndex + 1);
      } else if (_columnIndex == 1) {
        final Geometry s = (Geometry) ligne_.getGeometry(_rowIndex);
        return new Integer(s.getNumPoints());
      } else if (_columnIndex == 2) {
        final Geometry s = (Geometry) ligne_.getGeometry(_rowIndex);
        return s.isValid() ? valide_ : nonValide_;
      }
      return ligne_.getModel(att_[_columnIndex - 3]).getObjectValueAt(_rowIndex);
    }
  }

  /**
   * @param _zone la zone: non copiee
   */
  public ZModeleMultiPointEditable(final GISZoneCollectionMultiPoint _zone) {
    super(_zone);
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    final BuTable r = new CtuluTable();
    //TODO Voir si l'action se fait qu'en mode atomique (comme mnt) ou dans tout mode diff�rent du mode normal.
//    if (((ZCalqueMultiPoint) _layer).isAtomicMode()) {
    if (((ZCalqueMultiPoint) _layer).getSelectionMode() == SelectionMode.ATOMIC) {
      r.setModel(new AtomesTableModel(getGeomData()));
    } else {
      r.setModel(new LignesTableModel(getGeomData()));
    }
    return r;
  }

  public GISAttributeInterface getAttribute(final int _i) {
    return geometries_.getAttribute(_i);
  }

  public int getAttributeNb() {
    return geometries_.getNbAttributes();
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {
    // Selection vide : information sur le calque
    if (_layer.isSelectionEmpty())
      _d.put(EbliLib.getS("Nombre de semis"), CtuluLibString.getString(getNombre()));
    
    // Selection non vide : information relative � la selection
    else {
      final ZCalqueMultiPointEditable layer=(ZCalqueMultiPointEditable)_layer;
      
      // En mode atomique : Info sur les sommets.
      if (layer.getSelectionMode() == SelectionMode.ATOMIC) {
        final int nb=layer.getLayerSelectionMulti().getNbListSelected();
        _d.put(EbliLib.getS("Nombre de semis s�lectionn�es"), CtuluLibString.getString(nb)+'/'+getNombre());
        super.fillWithInfo(_d, _layer);
        
        // Une seule g�om�trie support.
        if (nb==1) {
          final TIntObjectIterator it=layer.getLayerSelectionMulti().getIterator();
          it.advance();
          final int idxPoly=it.key();

          // Si la g�om�trie poss�de un nom.
          int attName=layer.modeleDonnees().getGeomData().getIndiceOf(GISAttributeConstants.TITRE);
          if (attName!=-1) {
            _d.put(EbliLib.getS("Nom du semis"), 
                (String)layer.modeleDonnees().getGeomData().getValue(attName, idxPoly));
          }

          final CtuluListSelectionInterface list=(CtuluListSelectionInterface)it.value();
          final int nbPoint=list.getNbSelectedIndex();
          _d.put(EbliLib.getS("Nombre de sommets s�lectionn�s"), CtuluLibString.getString(nbPoint)+'/'
              +getGeomData().getGeometry(idxPoly).getNumPoints());
          
          // Un seul point.
          if (nbPoint==1) {
            final int idxPt=list.getMaxIndex();
            _d.put(EbliLib.getS("Indice du sommet s�lectionn�"), CtuluLibString.getString(idxPt+1));
            final Coordinate c=((GISMultiPoint)getGeomData().getGeometry(idxPoly)).getCoordinates()[idxPt];
            _d.put("X:", Double.toString(c.x));
            _d.put("Y:", Double.toString(c.y));
            fillWithAtomicInfo(idxPoly, idxPt, _d);
          }
        }
      }
      
      // En mode global, info sur les g�om�tries.
      else {
        final int nb=layer.getLayerSelection().getNbSelectedIndex();
        _d.put(EbliLib.getS("Nombre de semis s�lectionn�es"), CtuluLibString.getString(nb)+'/'+getNombre());
        super.fillWithInfo(_d, _layer);
        if (nb==1) {
          final int idxPoly=layer.getLayerSelection().getMaxIndex();
          _d.setTitle(EbliLib.getS("Semis {0}", CtuluLibString.getString(idxPoly+1)));
          _d.put(EbliLib.getS("Valide"), getGeomData().getGeometry(idxPoly).isValid() ? EbliLib.getS("vrai"):EbliLib.getS("faux"));
          fillWithInfo(idxPoly, _d);
        }
      }
    }
  }

  @Override
  public final GISZoneCollectionMultiPoint getGeomData() {
    return (GISZoneCollectionMultiPoint)geometries_;
  }

  /*
   * Surcharge pour acceleration des traitements. 
   */
  @Override
  public final int getNbPointForGeometry(final int _idxLigne) {
    if (geometries_ == null) {
      return 0;
    }
    final Geometry gi = geometries_.getGeometry(_idxLigne);
    return gi.getNumGeometries();
//    return gi.getNumPoints();
  }

  @Override
  public boolean isCoordinateValid(final CoordinateSequence _seq, final CtuluAnalyze _analyze) {
    if (_seq == null || _seq.size()<=0) {
      _analyze.addFatalError(EbliLib.getS("Pas de coordonn�es"));
      return false;
    }
    return true;
  }
  
  @Override
  public boolean isDataValid(CoordinateSequence _seq, ZEditionAttributesDataI _data, CtuluAnalyze _ana) {
    return isCoordinateValid(_seq, _ana);
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  /**
   * Remplit les propri�t�s d'un sommet de g�om�trie avec les valeurs d'attributs atomiques.
   * @param _idxGeom L'indice de g�om�trie
   * @param _idxPt L'indice du sommet sur la g�om�trie.
   * @param _d Les propri�t�s.
   */
  protected void fillWithAtomicInfo(final int _idxGeom, final int _idxPt, final InfoData _d) {
    final GISZoneCollection model = getGeomData();
    final int nbAtt = model.getNbAttributes();
    for (int i = 0; i < nbAtt; i++) {
      if (model.getAttribute(i).isAtomicValue() && model.getDataModel(i) != null
          && ((GISAttributeModel) model.getDataModel(i).getObjectValueAt(_idxGeom)).getObjectValueAt(_idxPt) != null) {
        _d.put(model.getAttribute(i).getName(), ((GISAttributeModel) model.getDataModel(i).getObjectValueAt(_idxGeom))
            .getObjectValueAt(_idxPt).toString());
      }
    }

  }

  protected void fillWithInfo(final int _idxLigne, final InfoData _d) {
    final Geometry s = (Geometry) getGeomData().getGeometry(_idxLigne);
    _d.put(EbliLib.getS("Nombre de sommets"), CtuluLibString.getString(s.getNumPoints()));
  }

  /**
   * Ajoute un sommet X,Y,Z � la g�om�trie donn�e.<p>
   * <b>ATTENTION</b> : Les valeurs des attributs pour ce sommet sont d�finies par d�faut.
   * @param _idxGeom La g�om�trie modifi�e.
   * @param _idxBefore Le sommet qui sera le pr�cedent de celui ajout�.
   * @param _x La coordonn�e X du point.
   * @param _y La coordonn�e Y du point.
   * @param _z La coordonn�e Z du point.
   * @param _cmd Le manager de commandes.
   */
  public void addAtomic(final int _idxGeom,final int _idxBefore,final double _x,final double _y, final double _z,final CtuluCommandContainer _cmd){
    if (geometries_ == null) return;

    CtuluCommandComposite cmp=new CtuluCommandComposite();

    setGeomModif(_idxGeom, _cmd); // Modification de l'�tat de la g�om�trie
    geometries_.addAtomic(_idxGeom, _idxBefore, _x, _y, _cmd);
    
    // Mise a jour du Z si necessaire.
    GISAttributeInterface attZ=geometries_.getAttributeIsZ();
    if (attZ!=null && attZ.isAtomicValue()) {
      GISAttributeModel md=(GISAttributeModel)geometries_.getModel(attZ).getObjectValueAt(_idxGeom);
      md.setObject(_idxBefore+1, _z, cmp);
    }
    
    if (_cmd!=null)
      _cmd.addCmd(cmp.getSimplify());
  }

  protected GISZoneCollectionMultiPoint createCollection(){
    return new GISZoneCollectionMultiPoint(null);
  }

  /**
   * @param _p la polyligne a ajouter
   * @param _cmd le receveur de commandes
   * @param _ui l'interface utilisateur
   * @param _d les donn�es associ�es � la polyligne
   * @return true si ajoute
   */
  public boolean addGeometry(final Geometry _p,final CtuluCommandContainer _cmd,final CtuluUI _ui,
    final ZEditionAttributesDataI _d){
    if (_p.getNumPoints() < 1) {
      _ui.error(null,EbliLib.getS("Le multi point doit contenir {0} points au moins",CtuluLibString.UN), false);
      return false;
    }
    if (geometries_ == null) {
      geometries_ = createCollection();
    }

    final boolean r = true;//isNewGeometryOk(_p, _ui);
    final Object[] data = createData(_p, _d);
//    if (data == null/* && needData()*/) {
//      if (_ui != null) {
//        _ui.error(null,EbliLib.getS("Des donn�es manquent"), false);
//      }
//      return false;
//    }
    if (r) {
      geometries_.addGeometry(_p, data, _cmd);
    }
    return r;
  }

  protected Object[] createData(final Geometry _s,final ZEditionAttributesDataI _d){
    if (_d == null || geometries_.getNbAttributes() == 0) {
      if (FuLog.isTrace()) {
        FuLog.trace("EZM: " + getClass().getName() + " data NO present");
      }
      return null;
    }
    if (FuLog.isTrace()) {
      FuLog.trace("EZM: " + getClass().getName() + " data is present");
    }
    final Object[] r = new Object[geometries_.getNbAttributes()];
    for (int i = r.length - 1; i >= 0; i--) {
      final GISAttributeInterface att = geometries_.getAttribute(i);
      if (att.isAtomicValue()) {
        final Object[] ri = new Object[_d.getNbVertex()];
        for (int gi = ri.length - 1; gi >= 0; gi--) {
          ri[gi] = _d.getValue(att, gi);
          if (FuLog.isTrace()) {
            FuLog.trace("EZM: ajout val atomic " + ri[gi]);
          }
        }
        r[i] = ri;
      }
      else {
        r[i] = _d.getValue(att, 0);
        if (FuLog.isTrace()) {
          FuLog.trace("EZM: ajout val non atomic " + r[i]);
        }
      }
    }
    return r;
  }

  /**
   * @param _selection les lignes brisees a deplacees
   * @param _dx le dx reel
   * @param _dy le dy reel
   * @param _dz le dz reel, modifi� si un attribut est affect� au Z.
   * @param _cmd le receveur de commande
   * @return true si modif
   */
  public boolean moveGeometries(final CtuluListSelectionInterface _selection,final double _dx,final double _dy,final double _dz,
    final CtuluCommandContainer _cmd){
    if (geometries_ == null) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    boolean r = false;
    final int min = _selection.getMinIndex();
    for (int i = _selection.getMaxIndex(); i >= min; i--) {
      if (_selection.isSelected(i)) {
        setGeomModif(i, _cmd); // Modification de l'�tat de la g�om�trie
        r = true;
        Coordinate[] newSeq = ((Geometry) geometries_.getGeometry(i).clone()).getCoordinates();
        for (int j = newSeq.length - 1; j >= 0; j--) {
          newSeq[j].x+=_dx;
          newSeq[j].y+=_dy;
        }

        // Translation suivant Z si attribut Z non null.
        GISAttributeInterface zAtt=getGeomData().getAttributeIsZ();
        if (zAtt!=null) {
          if (getGeomData().getModel(zAtt)!=null) {
            Object oldvalue=getGeomData().getModel(zAtt).getObjectValueAt(i);
            Object newvalue=zAtt.createDataForGeom(oldvalue, geometries_.getGeometry(i).getNumPoints());
            if (oldvalue instanceof GISAttributeModelDoubleArray) {
              GISAttributeModelDoubleArray newvals=(GISAttributeModelDoubleArray)newvalue;
              GISAttributeModelDoubleArray oldvals=(GISAttributeModelDoubleArray)oldvalue;
              for (int iv=0; iv<oldvals.getSize(); iv++) {
                newvals.setObject(iv, new Double(oldvals.getValue(iv)+_dz), null);
              }
            }
            else if (oldvalue instanceof Double) {
              newvalue=new Double(((Double)oldvalue).doubleValue()+_dz);
            }
            getGeomData().getModel(zAtt).setObject(i, newvalue, cmp);
          }
          else
            FuLog.warning("EBL:Pb l'attribut pour Z n'est pas dans la liste des attributs du calque!");
        }
        
        final GISMultiPoint geo = (GISMultiPoint) GISGeometryFactory.INSTANCE.createMultiPoint(newSeq);
        geometries_.setGeometry(i, geo, cmp);
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }

  /**
   * Scinde en 2 un multipoint. Les attributs globaux sont recopi�es dans chacune des 2
   * polylignes, les attributs atomiques sont dispatch�s.<br>
   * Si tous les points du multipoint sont selectionn�s, alors aucun split n'est r�alis�.
   * 
   * @param _geomIdx L'index de geometrie.
   * @param _idx Les indices des sommets de multipoints.
   * @param _cmd Le conteneur de commandes.
   * @return Les indices des 2 nouvelles g�om�tries cr��es si tout s'est bien pass�, null sinon.
   */
  public int[] splitGeometry(final int _geomIdx, final int[] _idx, final CtuluCommandContainer _cmd){
    if (geometries_ == null) {
      return null;
    }
    GISMultiPoint geom=(GISMultiPoint)geometries_.getGeometry(_geomIdx); // Old g�ometrie
    if (geom==null) return null;
    CoordinateSequence cgeom=geom.getCoordinateSequence();
    if (_idx.length<0 || _idx.length>=cgeom.size()) return null;

    final CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Scission de la g�om�trie"));
    CtuluListSelection sel=new CtuluListSelection(_idx);
    
    // Les coordonn�es
    Coordinate[] cgeom1=new Coordinate[_idx.length];
    Coordinate[] cgeom2=new Coordinate[cgeom.size()-cgeom1.length];
    int idgeom1=0;
    int idgeom2=0;
    for (int i=0; i<cgeom.size(); i++) {
      if (sel.isSelected(i)) 
        cgeom1[idgeom1++]=cgeom.getCoordinateCopy(i);
      else 
        cgeom2[idgeom2++]=cgeom.getCoordinateCopy(i);
    }
    
    // Les datas
    int iattHisto=geometries_.getIndiceOf(GISAttributeConstants.HISTORY);
    
    Object[] datageom1=new Object[getAttributeNb()];
    Object[] datageom2=new Object[getAttributeNb()];
    for (int i=0; i<getAttributeNb(); i++) {
      GISAttributeModel attmodel=geometries_.getModel(i);
      if (attmodel.getAttribute().isAtomicValue()) {
        GISAttributeModel atomModel1=attmodel.getAttribute().createAtomicModel(null,cgeom1.length);
        GISAttributeModel atomModel2=attmodel.getAttribute().createAtomicModel(null,cgeom2.length);

        idgeom1=0;
        idgeom2=0;
        for (int j=0; j<cgeom.size(); j++) {
          Object data=((GISAttributeModel)attmodel.getObjectValueAt(_geomIdx)).getObjectValueAt(j);
          if (sel.isSelected(j)) 
            atomModel1.setObject(idgeom1++,data,null);
          else 
            atomModel2.setObject(idgeom2++,data,null);
        }
        datageom1[i]=atomModel1;
        datageom2[i]=atomModel2;
      }
      else {
        // Cas particulier de l'attribut TITRE => Conserv�, op�ration dans l'historique
        if (attmodel.getAttribute()==GISAttributeConstants.TITRE) {
          String name=(String)attmodel.getObjectValueAt(_geomIdx);
          datageom1[i]=name;
          datageom2[i]=name+"_";
          
          if (iattHisto!=-1) {
            datageom1[iattHisto]=GISLib.appendHistory(EbliLib.getS("Scission"),name, (String)geometries_.getModel(iattHisto).getObjectValueAt(_geomIdx));
            datageom2[iattHisto]=GISLib.appendHistory(EbliLib.getS("Scission"),name, (String)geometries_.getModel(iattHisto).getObjectValueAt(_geomIdx));
          }
        }
        // Cas particulier de l'historique => Deja trait�
        else if(attmodel.getAttribute()==GISAttributeConstants.HISTORY) {}
        // Cas particulier de l'attribut ETAT_GEOM => ATT_VAL_ETAT_MODI
        else if(attmodel.getAttribute()==GISAttributeConstants.ETAT_GEOM) {
          datageom1[i]=GISAttributeConstants.ATT_VAL_ETAT_MODI;
          datageom2[i]=GISAttributeConstants.ATT_VAL_ETAT_MODI;
        }
        else {
          datageom1[i]=attmodel.getAttribute().createDataForGeom(attmodel.getObjectValueAt(_geomIdx),1);
          datageom2[i]=attmodel.getAttribute().createDataForGeom(attmodel.getObjectValueAt(_geomIdx),1);
        }
      }
    }
    
    // Remplacement dans la collection.
    geometries_.removeGeometries(new int[]{_geomIdx}, cmp);
    geometries_.addGeometry(GISGeometryFactory.INSTANCE.createMultiPoint(cgeom1), datageom1, cmp);
    geometries_.addGeometry(GISGeometryFactory.INSTANCE.createMultiPoint(cgeom2), datageom2, cmp);

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return new int[]{geometries_.getNbGeometries()-2,geometries_.getNbGeometries()-1};
  }

  /**
   * @param _selection la selection
   * @param _dx dx reel
   * @param _dy dy reel
   * @param _dz le dz reel, modifi� si un attribut est affect� au Z et qu'il est atomique.
   * @param _cmd le receveur de commande
   * @param _ui l'interface utilisateur pour les messages
   * @return true si modification
   */
  public boolean moveAtomics(final EbliListeSelectionMultiInterface _selection,final double _dx,final double _dy,final double _dz,
    final CtuluCommandContainer _cmd,final CtuluUI _ui){
    if (geometries_ == null) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final TIntObjectIterator it = _selection.getIterator();
    boolean r = false;
    for (int i = _selection.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        r = true;
        final int idx = it.key();
        setGeomModif(idx, _cmd); // Modification de l'�tat de la g�om�trie
        Coordinate[] newSeq = ((Geometry) geometries_.getGeometry(idx).clone()).getCoordinates();

        // Translation suivant Z si attribut Z non null et atomic.
        GISAttributeInterface zAtt=getGeomData().getAttributeIsZ();
        GISAttributeModelDoubleArray newvals=null;
        GISAttributeModelDoubleArray oldvals=null;
        if (zAtt!=null && zAtt.isAtomicValue()) {
          if (getGeomData().getModel(zAtt)!=null) {
            oldvals=(GISAttributeModelDoubleArray)getGeomData().getModel(zAtt).getObjectValueAt(idx);
            newvals=(GISAttributeModelDoubleArray)zAtt.createDataForGeom(oldvals, geometries_.getGeometry(idx).getNumPoints());
          }
          else
            FuLog.warning("EBL:Pb l'attribut pour Z n'est pas dans la liste des attributs du calque!");
        }
        
        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            newSeq[j].x+=_dx;
            newSeq[j].y+=_dy;

            if (newvals!=null)
              newvals.setObject(j, new Double(oldvals.getValue(j)+_dz), null);
          }
        }
        final GISMultiPoint geo=(GISMultiPoint)GISGeometryFactory.INSTANCE.createMultiPoint(newSeq);
        if (!geo.isValid()) {
          if (_ui!=null) {
            _ui.error(null, EbliLib.getS("La nouvelle ligne n'est pas valide"), false);
          }
          return false;
        }
        geometries_.setGeometry(idx, geo, cmp);

        if (newvals!=null)
          getGeomData().getModel(zAtt).setObject(idx, newvals, cmp);
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }
  
  protected boolean removeAtomics(final EbliListeSelectionMultiInterface _selection,
    final CtuluCommandContainer _cmd,final CtuluUI _ui){
    if (geometries_ == null) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final TIntObjectIterator iterator = _selection.getIterator();
    //on parcourt les lignes concern�es par la suppression
    for (int i = _selection.getNbListSelected(); i-- > 0;) {
      iterator.advance();
      //l'indice de la ligne
      final int idxLigne = iterator.key();
      setGeomModif(idxLigne, _cmd); // Modification de l'�tat de la g�om�trie
      //les points a enlever
      final CtuluListSelectionInterface sel = (CtuluListSelectionInterface) iterator.value();
      geometries_.removeAtomics(idxLigne, sel, _ui, cmp);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return true;
  }
  
  protected boolean removeGeometries(int[] _idx, CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return false;
    }
    for(int i=0 ; i<_idx.length;i++)
      setGeomModif(_idx[i], _cmd); // Modification de l'�tat de la g�om�trie
    geometries_.removeGeometries(_idx, _cmd);
    return true;
  }

  /**
   * Rotation des lignes de la liste donn�e. Les lignes sont transform�s autour d'une origine unique.
   * @param _selection La liste donn�e des lignes brisees
   * @param _angRad L'angle de rotation, en radians.
   * @param _xreel0 La coordonn�e X du pt d'origine.
   * @param _yreel0 La coordonn�e Y du pt d'origine.
   * @param _cmd le receveur de commande
   * @return true si modif ok.
   */
  public boolean rotateGeometries(CtuluListSelectionInterface _selection, double _angRad, double _xreel0, double _yreel0,
    final CtuluCommandContainer _cmd){
    if (geometries_ == null) return false;

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    boolean r = false;
    final int min = _selection.getMinIndex();
    for (int i = _selection.getMaxIndex(); i >= min; i--) {
      if (_selection.isSelected(i)) {
        setGeomModif(i, _cmd); // Modification de l'�tat de la g�om�trie
        r = true;
        final Coordinate[] newSeq = ((Geometry) geometries_.getGeometry(i).clone()).getCoordinates();
        for (int j = newSeq.length - 1; j >= 0; j--) {
          /// Ramene � l'origine, et rotation.
          double dxold=newSeq[j].x-_xreel0;
          double dyold=newSeq[j].y-_yreel0;
          double dxnew=dxold*Math.cos(_angRad)-dyold*Math.sin(_angRad);
          double dynew=dxold*Math.sin(_angRad)+dyold*Math.cos(_angRad);
          
          newSeq[j].x=dxnew+_xreel0;
          newSeq[j].y=dynew+_yreel0;
        }
        final GISMultiPoint poly = (GISMultiPoint) GISGeometryFactory.INSTANCE.createMultiPoint(newSeq);
        geometries_.setGeometry(i, poly, cmp);
      }
    }
    
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }
  
  /**
   * Rotation des points de la liste donn�e. Les points sont transform�s autour d'une origine unique.
   * @param _selection La liste donn�e des points
   * @param _angRad L'angle de rotation, en radians.
   * @param _xreel0 La coordonn�e X du pt d'origine.
   * @param _yreel0 La coordonn�e Y du pt d'origine.
   * @param _cmd le receveur de commande
   * @param _ui l'interface utilisateur pour les messages
   * @return true si modification
   */
  public boolean rotateAtomics(final EbliListeSelectionMultiInterface _selection, double _angRad, double _xreel0, double _yreel0,
    final CtuluCommandContainer _cmd,final CtuluUI _ui){
    if (geometries_ == null) return false;

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final TIntObjectIterator it = _selection.getIterator();
    boolean r = false;
    for (int i = _selection.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        r = true;
        final int idx = it.key();
        setGeomModif(idx, _cmd); // Modification de l'�tat de la g�om�trie
        final Coordinate[] newSeq = ((Geometry) geometries_.getGeometry(idx).clone()).getCoordinates();
        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            /// Ramene � l'origine, et rotation.
            double dxold=newSeq[j].x-_xreel0;
            double dyold=newSeq[j].y-_yreel0;
            double dxnew=dxold*Math.cos(_angRad)-dyold*Math.sin(_angRad);
            double dynew=dxold*Math.sin(_angRad)+dyold*Math.cos(_angRad);
            
            newSeq[j].x=dxnew+_xreel0;
            newSeq[j].y=dynew+_yreel0;
          }
        }
        final GISMultiPoint geo=(GISMultiPoint)GISGeometryFactory.INSTANCE.createMultiPoint(newSeq);
        if (!geo.isValid()) {
          if (_ui!=null) {
            _ui.error(null, EbliLib.getS("La nouvelle g�om�trie n'est pas valide"), false);
          }
          return false;
        }
        geometries_.setGeometry(idx, geo, cmp);
      }
    }
    
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }
  
  /**
   * Copie d'une geom�trie, avec d�calage.
   */
  public boolean copyGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, final CtuluCommandContainer _cmd) {
    if (geometries_ == null) return false;
    
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    for (int idx = _selection.getMinIndex(); idx<=_selection.getMaxIndex(); idx++) {
      if (_selection.isSelected(idx)) {
        // La g�ometrie, d�cal�e.
        CoordinateSequence seq=(CoordinateSequence)((GISMultiPoint)geometries_.getGeometry(idx)).getCoordinateSequence().clone();
        for (int i=0; i<seq.size(); i++) {
          seq.setOrdinate(i,0,seq.getX(i)+_dx);
          seq.setOrdinate(i,1,seq.getY(i)+_dy);
        }
        GISMultiPoint geom=(GISMultiPoint)GISGeometryFactory.INSTANCE.createMultiPoint(seq);
        // Les valeurs d'attribut.
        Object[] vals=new Object[geometries_.getNbAttributes()];
        for (int iatt=0; iatt<geometries_.getNbAttributes(); iatt++) {
          Object val=geometries_.getValue(iatt,idx);
          if (geometries_.getAttribute(iatt).equals(GISAttributeConstants.TITRE)) val=EbliLib.getS("Copie de")+" "+val;
          if (geometries_.getAttribute(iatt).equals(GISAttributeConstants.ETAT_GEOM)) val=GISAttributeConstants.ATT_VAL_ETAT_MODI;
          int nbpt=geom.getNumPoints();
          vals[iatt]=geometries_.getAttribute(iatt).createDataForGeom(val, nbpt);
        }
        geometries_.addGeometry(geom,vals,cmp);
      }
    }
    
    if (_cmd!=null) _cmd.addCmd(cmp.getSimplify());
    return true;
  }
  
  /**
   * Fait passer la g�om�trie en Modifier
   * @param _idx : index de la geom�trie
   * @param _cmd : 
   */
  public void setGeomModif(int _idx, CtuluCommandContainer _cmd) {
    int idxAttr = getGeomData().getIndiceOf(GISAttributeConstants.ETAT_GEOM);
    if(idxAttr >= 0)
      getGeomData().getDataModel(idxAttr).setObject(_idx, GISAttributeConstants.ATT_VAL_ETAT_MODI, _cmd);
  }
  
  /**
   * Joint 2 semis de points.
   * 
   * @param _idsSemis Les index des 2 semis.
   * @param _cmd Le conteneur de commandes.
   * @return L'index de la nouvelle ligne si OK, -1 sinon.
   */
  public int joinGeometries(int[] _idxSemis, final CtuluCommandContainer _cmd) {
    if (geometries_ == null||_idxSemis==null||_idxSemis.length!=2)
      return -1;
    final CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Jointure de semis de points"));
    
    // Nouvelles coordonn�es
    int nbPointMulti1=geometries_.getGeometry(_idxSemis[0]).getNumPoints();
    int nbPointMulti2=geometries_.getGeometry(_idxSemis[1]).getNumPoints();
    Coordinate[] coord=new Coordinate[nbPointMulti1+nbPointMulti2];
    // Remplisage de coord
    for(int i=0, iend=_idxSemis.length; i<iend; i++){
      GISMultiPoint multipoint=(GISMultiPoint) geometries_.getGeometry(_idxSemis[i]);
      for(int j=0, jend=multipoint.getNumPoints(); j<jend; j++)
        coord[i*nbPointMulti1+j]=multipoint.getCoordinateSequence().getCoordinate(j);
    }
    
    // Les datas
    final int iattHisto=geometries_.getIndiceOf(GISAttributeConstants.HISTORY);
    
    Object[] dataSemis=new Object[geometries_.getNbAttributes()];
    for (int att=0; att<geometries_.getNbAttributes(); att++) {
      // Attributs atomiques
      if (geometries_.getAttribute(att).isAtomicValue()) {
        // R�cup�ration des valeurs
        Object[] values=new Object[nbPointMulti1+nbPointMulti2];
        for(int i=0, iend=_idxSemis.length; i<iend;i++){
          GISMultiPoint multipoint=(GISMultiPoint) geometries_.getGeometry(_idxSemis[i]);
          for(int j=0, jend=multipoint.getNumPoints(); j<jend;j++)
            values[i*nbPointMulti1+j]=((GISAttributeModel) geometries_.getModel(att).getObjectValueAt(_idxSemis[i])).getObjectValueAt(j);
        }
        dataSemis[att]=(GISAttributeModel)geometries_.getModel(att).getAttribute().createDataForGeom(values,nbPointMulti1+nbPointMulti2);
      }
      // Attributs globaux
      else {
        // Cas particulier de l'attribut TITRE => On prend le premier, l'autre va dans l'historique
        if (geometries_.getModel(att).getAttribute()==GISAttributeConstants.TITRE) {
          String name1=(String)geometries_.getModel(att).getObjectValueAt(_idxSemis[0]);
          String name2=(String)geometries_.getModel(att).getObjectValueAt(_idxSemis[1]);
          dataSemis[att]=name1;
          
          if (iattHisto!=-1) {
            dataSemis[iattHisto]=GISLib.appendHistory(EbliLib.getS("Jonction"),name1+","+name2, (String)geometries_.getModel(iattHisto).getObjectValueAt(_idxSemis[0]));
          }
        }
        // Cas particulier de l'attribut HISTORY => D�j� trait�
        else if (geometries_.getModel(att).getAttribute()==GISAttributeConstants.HISTORY) {}
            
        // Cas particulier de l'attribut ETAT_GEOM => ATT_VAL_ETAT_MODI
        else if (geometries_.getModel(att).getAttribute()==GISAttributeConstants.ETAT_GEOM) {
          dataSemis[att]=GISAttributeConstants.ATT_VAL_ETAT_MODI;
        }
        // On prend arbitrairement la valeur de la premi�re polyligne.
        else
          dataSemis[att]=geometries_.getModel(att).getAttribute().createDataForGeom(geometries_.getModel(att).getObjectValueAt(_idxSemis[0]),1);
      }
    }
    
    // Remplacement dans la collection.
    if (getGeomData().addGeometry(GISGeometryFactory.INSTANCE.createMultiPoint(coord), dataSemis, cmp)!=-1) {
      geometries_.removeGeometries(_idxSemis, cmp);
      if (_cmd!=null)
        _cmd.addCmd(cmp.getSimplify());
      return geometries_.getNbGeometries()-1;
    }
    return -1;
  }
}
