/*
 *  @creation     31 mars 2005
 *  @modification $Date: 2008-05-13 12:10:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuToolToggleButton;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.border.EtchedBorder;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une palette d'outils pour l'edition de calques. Cette palette est affich�e sur le Desktop, � la demande de l'utilisateur. Elle inclu un panneau de
 * parametre de l'outil selectionn�.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class BPaletteEdition extends BuPanel implements ActionListener, BuInsets, BPalettePanelInterface {
  
  private static class SpecBorder extends EtchedBorder {
    
    public SpecBorder() {
      super();
    }
    
    @Override
    public void paintBorder(final Component _c, final Graphics _g, final int _x, final int _y, final int _width,
            final int _height) {
      final int w = _width;
      _g.translate(_x, _y);
      _g.setColor(etchType == LOWERED ? getHighlightColor(_c) : getShadowColor(_c));
      _g.drawLine(1, 1, w - 3, 1);
      _g.translate(-_x, -_y);
    }
  }
  protected List globalButtons_;
  BPaletteEditionClientInterface target_;
  protected BuPanel editorPanel_;
  ZScene scene_;
  
  public ZScene getScene() {
    return scene_;
  }
  
  @Override
  public void paletteDeactivated() {
    if (editorPanel_ != null) {
      final List v = BuLib.getAllSubComponents(editorPanel_);
      for (int i = v.size() - 1; i >= 0; i--) {
        final Component c = (Component) v.get(i);
        if (c.isFocusable()) {
          c.setFocusable(false);
          c.setFocusable(true);
        }
        
      }
    }
    
  }
  
  public BPaletteEdition(ZScene _scene) {
    super();
    scene_ = _scene;
//    buildComponents();
  }

  /**
   * Ne doit �tre appele qu'une fois.
   */
  public void buildComponents() {
    setLayout(new BuVerticalLayout(0, true, true));
    buildGlobalsActions(this);
    editorPanel_ = new BuPanel();
    editorPanel_.setLayout(new BuBorderLayout());
    editorPanel_.setBorder(BorderFactory.createEtchedBorder());
    editorPanel_.setVisible(false);
    add(editorPanel_);
  }
  
  @Override
  public void setVisible(final boolean _flag) {
    super.setVisible(_flag);
  }

  /**
   * Affecte � la palette un panneau de parametres de l'outil courant.
   *
   * @param _c le composant associ�e a l'outil courant. Peut �tre null si l'outil ne poss�de pas de param�tres, ou en cas de d�sactivation d'un outil.
   */
  public void setEditorPanel(final JComponent _c) {
    editorPanel_.removeAll();
    if (_c == null) {
      editorPanel_.setVisible(false);
    } else {
      editorPanel_.add(_c, BuBorderLayout.CENTER);
      editorPanel_.setVisible(true);
    }
    revalidate();
  }
  
  private LayoutManager buildLayout() {
    final BuGridLayout layout = new BuGridLayout(6, 0, 0, false, false, false, true, true);
    layout.setCfilled(false);
    return layout;
  }
  
  private void decoreButton(final AbstractButton _bt) {
    _bt.setRequestFocusEnabled(false);
    _bt.setMargin(INSETS1111);
    _bt.addActionListener(this);
    
  }
  
  private void fillWithButton(final BuPanel _dest, final List _btList) {
    final SpecBorder border = new SpecBorder();
    if (_btList == null) {
      return;
    }
    final int nb = _btList.size();
    final LayoutManager lay = buildLayout();
    BuPanel p = new BuPanel();
    p.setBorder(border);
    p.setAlignmentX(0.5f);
    p.setLayout(lay);
    _dest.add(p);
    for (int i = 0; i < nb; i++) {
      final Object o = _btList.get(i);
      if (o == null) {
        p = new BuPanel();
        p.setAlignmentX(0.5f);
        p.setLayout(lay);
        p.setBorder(border);
        _dest.add(p);
      } else {
        p.add((JComponent) o);
      }
    }
  }

  /**
   * @param _list la liste de AbstractButton a parcourir
   * @param _command la commande recherch�e
   * @return l'indice dans la liste ou -1 si pas trouv�e
   */
  private int getButtonActionIdx(final List _list, final String _command) {
    if (_list == null) {
      return -1;
    }
    for (int i = _list.size() - 1; i >= 0; i--) {
      final Object o = _list.get(i);
      if (o != null) {
        if (((AbstractButton) o).getActionCommand().equals(_command)) {
          return i;
        }
      }
    }
    return -1;
  }
  
  private void setButtonEnable(final List _list, final boolean _b) {
    if (_list == null) {
      return;
    }
    for (int i = _list.size() - 1; i >= 0; i--) {
      final Object o = _list.get(i);
      if (o != null) {
        ((AbstractButton) o).setEnabled(_b);
      }
    }
  }

  /**
   * Selectionne l'action demand�e.
   *
   * @param _cmd Le nom du bouton.
   */
  public void setButtonSelected(String _cmd) {
    AbstractButton bt = getGlobalButton(_cmd);
    if (bt != null) {
      bt.setSelected(true);
    }
    sendCommand(_cmd);
  }

  /**
   * Rend visible ou invisible les actions donn�es.
   *
   * @param _cmds Les noms de boutons. Si <tt>null</tt>, tous les boutons sont concern�s.
   */
  public void setButtonVisible(String[] _cmds, boolean _b) {
    if (globalButtons_ == null) {
      return;
    }
    
    if (_cmds == null) {
      for (int i = globalButtons_.size() - 1; i >= 0; i--) {
        if (globalButtons_.get(i) != null) {
          ((AbstractButton) globalButtons_.get(i)).setVisible(_b);
        }
      }
    } else {
      for (String cmd : _cmds) {
        AbstractButton bt = getGlobalButton(cmd);
        if (bt != null) {
          bt.setVisible(_b);
        }
      }
    }
  }
  
  private void setEnablePrefix(final String _prefix, final boolean _r) {
    for (int i = globalButtons_.size() - 1; i >= 0; i--) {
      final Object o = globalButtons_.get(i);
      if (o != null && ((AbstractButton) o).getActionCommand().startsWith(_prefix)) {
        ((AbstractButton) o).setEnabled(_r);
        if (!_r && o instanceof JToggleButton) {
          ((JToggleButton) o).setSelected(false);
        }
      }
    }
  }
  
  protected void buildGlobalsActions(final BuPanel _dest) {
    if (globalButtons_ == null) {
      globalButtons_ = new ArrayList();
      fillGlobalActions(globalButtons_);
    }
    fillWithButton(_dest, globalButtons_);
  }
  
  protected final void fillGlobalActions(final List _target) {
    // on modifie cette classe pour pouvoir d�sactiver un bouton.
    final ButtonGroup group = new ButtonGroup() {
      // le bouton fantome qui est active lorsque on demande qu'un bouton soit desactive.
      AbstractButton ghost_ = new JButton();
      // constructeur du button group

      {
        super.add(ghost_);
      }
      
      @Override
      public void setSelected(ButtonModel _m, boolean _b) {
        
        super.setSelected(_m, _b);
        // on demande que le bouton actif soit desactive-> bouton fantome.
        if (!_b && isSelected(_m)) {
          setSelected(ghost_.getModel(), true);
        }
      }
    };
    BuToolToggleButton bt = new BuToolToggleButton();
    bt.setActionCommand(ATOM_ACTION);
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-atom"));
    bt.setToolTipText(EbliLib.getS("Mode <SOMMET>"));
    decoreButton(bt);
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        AbstractButton button1 = getGlobalButton(ATOM_ACTION);
        AbstractButton button2 = getGlobalButton(SEGMENT_ACTION);
        if (button2 != null) {
          if (button1.isSelected()) {
            button2.setSelected(false);
          }
        }
      }
    });
    _target.add(bt);
    
    bt = new BuToolToggleButton();
    bt.setActionCommand(SEGMENT_ACTION);
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-atom"));
    bt.setToolTipText(EbliLib.getS("Mode <SEGMENT>"));
    decoreButton(bt);
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        AbstractButton button1 = getGlobalButton(SEGMENT_ACTION);
        if (button1 == null) {
          return;
        }
        AbstractButton button2 = getGlobalButton(ATOM_ACTION);
        
        if (button1.isSelected()) {
          button2.setSelected(false);
        }
      }
    });
    _target.add(bt);
    /*
     * bt = new BuToolToggleButton(); bt.setActionCommand("MODE_EDIT"); bt.setIcon(EbliResource.EBLI.getToolIcon("draw-edit"));
     * decoreButton(bt); group.add(bt); _target.add(bt);
     */

//    bt = new BuToolToggleButton();
//    bt.setActionCommand("GLOBAL_MOVE");
//    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-move"));
//    bt.setToolTipText(EbliLib.getS("D�placer des objets"));
//    decoreButton(bt);
//    group.add(bt);
//    _target.add(bt);


//    bt = new BuToolToggleButton();
//    bt.setActionCommand("GLOBAL_ROTATE");
//    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-rotation"));
//    bt.setToolTipText(EbliLib.getS("Rotation des objets"));
//    decoreButton(bt);
//    group.add(bt);
//    _target.add(bt);
    _target.add(null);
    // le remove sera fait par la touche Supp.
    /*
     * bt = new BuToolToggleButton(); bt.setActionCommand("GLOBAL_REMOVE"); bt.setIcon(BuResource.BU.getToolIcon("enlever"));
     * decoreButton(bt); group.add(bt); _target.add(bt);
     */
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_POINT_ACTION);
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-add-pt"));
    bt.setToolTipText(EbliLib.getS("Ajouter un point"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_MULTIPOINT_ACTION);
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-multipoint"));
    bt.setToolTipText(EbliLib.getS("Ajouter un semis"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_POLYLINE_ACTION);
    bt.setToolTipText(EbliLib.getS("Ajouter une polyligne"));
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-polyline"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_RECTANGLE_ACTION);
    bt.setToolTipText(EbliLib.getS("Ajouter un rectangle"));
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-rectangle"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_ELLIPSE_ACTION);
    bt.setToolTipText(EbliLib.getS("Ajouter une ellipse"));
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-ellipse"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_POLYGON_ACTION);
    bt.setToolTipText(EbliLib.getS("Ajouter un polygone"));
    bt.setIcon(EbliResource.EBLI.getToolIcon("draw-polygon"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
    _target.add(null);
    bt = new BuToolToggleButton();
    bt.setActionCommand(ADD_ATOM_ACTION);
    bt.setToolTipText(EbliLib.getS(
            "Ajouter un sommet (une g�om�trie, un sommet extr�mit� ou 2 sommets cons�cutifs doivent �tre s�lectionn�s)"));
    bt.setIcon(EbliResource.EBLI.getToolIcon("node-add"));
    decoreButton(bt);
    group.add(bt);
    _target.add(bt);
  }
  public final static String ATOM_ACTION = "MODE_ATOME";
  public final static String SEGMENT_ACTION = "MODE_SEGMENT";
  public final static String ADD_POINT_ACTION = "GLOBAL_ADD_POINT";
  public final static String ADD_MULTIPOINT_ACTION = "GLOBAL_ADD_SEMIS";
  public final static String ADD_POLYLINE_ACTION = "GLOBAL_ADD_POLYLIGNE";
  public final static String ADD_RECTANGLE_ACTION = "GLOBAL_ADD_RECTANGLE";
  public final static String ADD_ELLIPSE_ACTION = "GLOBAL_ADD_ELLIPSE";
  public final static String ADD_POLYGON_ACTION = "GLOBAL_ADD_POLYGONE";
  public final static String ADD_ATOM_ACTION = "ATOME_ADD";

  /**
   * Envoie les commandes au receveur suite � un click sur un bouton.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    sendCommand(_e.getActionCommand());
  }

  /**
   * Reset la palette : ferme le panel de construction de forme en cours. et d�active tout les autres bouton du panel
   */
  public void resetPalette() {
    if (globalButtons_ == null) {
      return;
    }
    
    for (int i = 0; i < globalButtons_.size(); i++) {
      if (globalButtons_.get(i) != null) {
        AbstractButton button = (AbstractButton) globalButtons_.get(i);
        if (button.isSelected()) {
          button.setSelected(false);
          sendCommand(button.getActionCommand());
        }
      }
    }
  }

  /**
   * reset les boutons d'�ditions du panel
   */
  public void resetPaletteCreation() {
    if (globalButtons_ == null) {
      return;
    }
    
    for (int i = 0; i < globalButtons_.size(); i++) {
      if (globalButtons_.get(i) != null) {
        AbstractButton button = (AbstractButton) globalButtons_.get(i);
        if (button.isSelected() && button.getActionCommand().matches("GLOBAL_ADD_[a-zA-Z0-9]*")) {
          button.setSelected(false);
          sendCommand(button.getActionCommand());
        }
      }
    }
  }

  /**
   * Retourne vrai si un des boutons de cr�ation de forme est actif.
   */
  public boolean isEditionOnGoing() {
    boolean actif = false;
    int i = 0;
    if (globalButtons_ != null) {
      while (!actif && i < globalButtons_.size()) {
        if (globalButtons_.get(i) != null) {
          AbstractButton button = (AbstractButton) globalButtons_.get(i);
          if (button.isSelected() && button.getActionCommand().matches("GLOBAL_ADD_[a-zA-Z0-9]*")) {
            actif = true;
          }
        }
        i++;
      }
    }
    return actif;
  }

  /**
   * Envoie les commandes au receveur.
   */
  private void sendCommand(String _command) {
    if (target_ != null) {
      target_.doAction(_command, this);
    }
  }

  /**
   * Verifie que les boutons coch�s soient activ�s.
   */
  public void checkEnableAndCheckBt() {
    if (globalButtons_ != null) {
      for (int i = globalButtons_.size() - 1; i >= 0; i--) {
        final Object o = globalButtons_.get(i);
        if (o instanceof JToggleButton) {
          final JToggleButton bt = (JToggleButton) o;
          if (!bt.isEnabled() && bt.isSelected()) {
            bt.setSelected(false);
          }
        }
      }
    }
  }
  
  @Override
  public JComponent getComponent() {
    return this;
  }
  
  public AbstractButton getGlobalButton(final String _actionCommand) {
    final int idx = getButtonActionIdx(globalButtons_, _actionCommand);
    return idx >= 0 ? (AbstractButton) globalButtons_.get(idx) : null;
  }

  /**
   * @return le bouton coch� autre que le MODE_ATOME ou MODE_SEGMENT.
   */
  public AbstractButton getSelectedButton() {
    if (globalButtons_ != null) {
      for (int i = globalButtons_.size() - 1; i >= 0; i--) {
        final Object o = globalButtons_.get(i);
        if (o instanceof JToggleButton) {
          final JToggleButton bt = (JToggleButton) o;
          if (bt.isSelected() && !ATOM_ACTION.equals(bt.getActionCommand()) && !SEGMENT_ACTION.equals(bt.getActionCommand())) {
            return bt;
          }
        }
      }
    }
    return null;
    
  }

  /**
   * @return le receveur des commandes
   */
  public final BPaletteEditionClientInterface getTarget() {
    return target_;
  }

  /**
   * @param _cmd la commande
   * @return true si une commande d'ajoute de polyligne
   */
  public boolean isAddMultiPoint(final String _cmd) {
    return ADD_POLYLINE_ACTION.equals(_cmd) || ADD_POLYGON_ACTION.equals(_cmd)
            || ADD_RECTANGLE_ACTION.equals(_cmd) || ADD_ELLIPSE_ACTION.equals(_cmd);
  }
  
  public boolean isAtomicMode() {
    final AbstractButton bt = (AbstractButton) getGlobalButton(ATOM_ACTION);
    return bt != null && bt.isEnabled() && bt.isSelected();
  }
  
  public SelectionMode getSelectionMode() {
    BuToolToggleButton bt = (BuToolToggleButton) getGlobalButton(ATOM_ACTION);
    
    if (bt != null && bt.isEnabled() && bt.isSelected()) {
      return SelectionMode.ATOMIC;
    }
    
    bt = (BuToolToggleButton) getGlobalButton(SEGMENT_ACTION);
    
    if (bt != null && bt.isEnabled() && bt.isSelected()) {
      return SelectionMode.SEGMENT;
    }
    
    return SelectionMode.NORMAL;
  }
  
  public void setAllEnable(final boolean _b) {
    setButtonEnable(globalButtons_, _b);
  }
  
  public void setAtomeSelected(final boolean _isSelected) {
    AbstractButton bt = (AbstractButton) getGlobalButton(ATOM_ACTION);
    setAtomeEnable(bt.isEnabled(), _isSelected);
    bt.setSelected(_isSelected);
  }
  
  public void setAtomeEnable(final boolean _enable, final boolean _isSelected) {
    AbstractButton bt = (AbstractButton) getGlobalButton(ATOM_ACTION);
    bt.setEnabled(_enable);
    if (_enable) {
      bt.setSelected(_isSelected);
    } else {
      bt.setSelected(false);
    }
    setEnablePrefix("ATOME_", bt.isEnabled());
  }
  
  public void setSegmentEnable(final boolean _enable, final boolean _isSelected) {
    final BuToolToggleButton bt = (BuToolToggleButton) getGlobalButton(SEGMENT_ACTION);
    if (bt == null) {
      return;
    }
    bt.setEnabled(_enable);
    if (_enable) {
      bt.setSelected(_isSelected);
    } else {
      bt.setSelected(false);
    }
    setEnablePrefix("SEGMENT_", bt.isEnabled());
  }

  /**
   * @param _action l'action a (d�s)activer
   * @param _enable le nouvel �tat pour cette action.
   */
  public void setEnable(final String _action, final boolean _enable) {
    final AbstractButton b = getGlobalButton(_action);
    if (b != null) {
      b.setEnabled(_enable);
    }
  }

  /**
   * Chgt du receveur.
   */
  @Override
  public boolean setPalettePanelTarget(final Object _target) {
    if (_target instanceof BPaletteEditionClientInterface) {
      setTargetClient((BPaletteEditionClientInterface) _target);
    } else {
      setTargetClient(null);
    }
    return target_ != null;
  }

  /**
   * Ne fait qu'enregistrer le nouveau receveur.
   *
   * @param _target le nouveau receveur de commande
   */
  public final void setTargetClient(final BPaletteEditionClientInterface _target) {
    if (_target == null) {
      resetPalette();
    }
    target_ = _target;
    if (target_ == null) {
      setAllEnable(false);
    }
  }
  
  @Override
  public void doAfterDisplay() {
  }
}
