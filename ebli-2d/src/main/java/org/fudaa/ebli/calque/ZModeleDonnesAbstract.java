/**
 *  @file         ZModeleDonnesAbstract.java
 *  @creation     6 f�vr. 2004
 *  @modification $Date: 2006-09-19 14:55:46 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;



/**
 * @author deniger
 * @version $Id$
 */
public abstract class ZModeleDonnesAbstract implements ZModeleDonnees {


  @Override
  public Object getObject(final int _ind) {
    return null;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int _idVertex) {
    return null;
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public void fillWithInfo(final InfoData _d, final ZCalqueAffichageDonneesInterface _layer) {}


  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }
  
  protected final List<ZModelListener> listeners_=new ArrayList<ZModelListener>();
  
  public void addModelListener(ZModelListener _listener){
    if(_listener!=null&&!listeners_.contains(_listener))
      listeners_.add(_listener);
  }
  
  public void removeModelListener(ZModelListener _listener){
    if(listeners_.contains(_listener))
      listeners_.remove(_listener);
  }
  
  protected void fireModelAction(int _idx, Geometry _geom, int _action){
    for(int i=0;i<listeners_.size();i++)
      listeners_.get(i).geometryAction(this, _idx, _geom, _action);
  }
  
}
