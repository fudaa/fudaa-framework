/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:45 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ebli.calque.edition.BPaletteRotation;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZEditorInterface;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action pour faire une rotation d'objets GIS, reagissant au changement de calque selectionn�.
 * @author Fred Deniger
 * @version $Id: SceneRotationAction.java,v 1.1.2.1 2008-05-13 12:10:45 bmarchan Exp $
 */
public class SceneRotationAction extends EbliActionPaletteTreeModel {

  ZSceneEditor sceneEditor_;
  EbliFormatterInterface formatter_;

  /**
   * @param _m l'arbre des calques
   */
  public SceneRotationAction(final TreeSelectionModel _m, final ZEditorInterface _editor, ZSceneEditor _sceneEditor, EbliFormatterInterface _formatter) {
    super(EbliLib.getS("Rotation des objets"), EbliResource.EBLI.getToolIcon("draw-rotation"), "GLOBAL_ROTATE", _m);
    setResizable(true);
    formatter_=_formatter;
    setSceneEditor(_sceneEditor);
    setEnabled(isTargetValid(super.target_));
  }
  /**
   * @param _sceneEditor l'editeur
   */
  private void setSceneEditor(final ZSceneEditor _sceneEditor) {
    sceneEditor_ = _sceneEditor;
  }

  @Override
  protected boolean isTargetValid(final Object _o) {
    // On n'utilise pas le calque actif de la scene (probablement pas encore initialis�), mais l'objet pass�.
    return (sceneEditor_!=null && (!sceneEditor_.getScene().isRestrictedToCalqueActif() || _o instanceof ZCalqueEditable/*sceneEditor_.getScene().isCalqueActifEditable()*/));
  }


  @Override
  public void updateBeforeShow() {
    if (palette_!=null) ((BPaletteRotation)palette_).updateForSelectionChanged();
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent() {
    return new BPaletteRotation(sceneEditor_,formatter_);
  }
  
  @Override
  public String getEnableCondition() {
    if (sceneEditor_!=null && sceneEditor_.getScene().isRestrictedToCalqueActif()) {
      return EbliLib.getS("Un calque �ditable doit �tre selectionn�");
    }
    else {
      return super.getEnableCondition();
    }
  }
}
