/*
 * @creation 4 avr. 2005
 * 
 * @modification $Date: 2008-03-26 16:46:43 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.LineString;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZEditionAttibutesContainer {

  /**
   * Pour ajouter des valeurs d'attributs a des lignes bris�es.
   * 
   * @author Fred Deniger
   * @version $Id$
   */
  public class LigneBriseeData implements ZEditionAttributesDataI {

    final HashMap<GISAttributeInterface, Object> globalAttribute_;
    final HashMap<GISAttributeInterface, ArrayList<Object>> atomicAttribute_;

    /**
     * Construit les maps par defaut.
     */
    public LigneBriseeData() {
      globalAttribute_ = new HashMap<GISAttributeInterface, Object>();
      atomicAttribute_ = new HashMap<GISAttributeInterface, ArrayList<Object>>();
      if (name_ != null) {
        for (int i = name_.length - 1; i >= 0; i--) {
          final GISAttributeInterface att = name_[i];
          if (att.isAtomicValue()) {
            atomicAttribute_.put(att, new ArrayList<Object>(20));
          } else {
            globalAttribute_.put(att, null);
          }
        }
      }
    }

    int nbGeom_;

      
    public void addPoint(GrPoint _pt) {
      addPoint(nbGeom_, _pt);
    }
    
    public void addPoint(int _idx, GrPoint _pt) {
      // Mise ajour des attributs globaux depuis le panneau d'�dition
      updateGlobalAttributes();
      
      // pour les param atomique: on ajoute la valeur a chaque list des parametres
      for (final Iterator it = atomicAttribute_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        Object o = null;
        if (creator_ != null) {
          GISAttributeInterface att=(GISAttributeInterface) e.getKey();
          // Cas particulier du Z, issu d'un accrochage.
          if (att==GISAttributeConstants.BATHY && _pt!=null)
            o = new Double(_pt.z_);
          else
            o = creator_.getData(att);
        }
        final ArrayList l = (ArrayList) e.getValue();
        l.add(_idx, o);
      }
      nbGeom_++;
    }
    
    /**
     * Mise ajour des param�tres globaux, si valeur diff�rente.
     */
    public void updateGlobalAttributes() {
      for (final Iterator it = globalAttribute_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        if (FuLog.isTrace()) {
          FuLog.trace("EZM: compute global attr for " + ((GISAttributeInterface) e.getKey()).getName());
        }
        Object o = null;
        // s'il y a un createur on recupere la valeur
        if (creator_ == null) {
          if (FuLog.isTrace()) {
            FuLog.trace("EZM: " + getClass().getName() + " no creator ");
          }
        } else {
          o = creator_.getData((GISAttributeInterface) e.getKey());
          if (FuLog.isTrace()) {
            FuLog.trace("EZM: new global value " + o);
          }
        }
        // on l'ajoute si non null et differente
        if (o != null && o != e.getValue()) {
          if (FuLog.isTrace()) {
            FuLog.trace("EZM: new global value SET " + o);
          }
          e.setValue(o);
        }
      }
    }

    @Override
    public int getNbVertex() {
      return nbGeom_;
    }

    @Override
    public int getNbValues() {
      return name_.length;
    }

    @Override
    public Object getValue(final GISAttributeInterface _numValue, final int _numGeometry) {
      if (globalAttribute_.containsKey(_numValue)) { return globalAttribute_.get(_numValue); }
      final ArrayList l = (ArrayList) atomicAttribute_.get(_numValue);
      if (l != null && l.size() > _numGeometry) { return l.get(_numGeometry); }
      return null;

    }

    @Override
    public GISAttributeInterface getAttribute(final int _i) {
      return name_[_i];
    }

    @Override
    public void setValue(final GISAttributeInterface _attribute, final int _numGeometry, final Object _newValue) {
      if (globalAttribute_.containsKey(_attribute)) {
        globalAttribute_.put(_attribute, _newValue);
      }
      final ArrayList l = (ArrayList) atomicAttribute_.get(_attribute);
      if (l != null && l.size() > _numGeometry) {
        l.set(_numGeometry, _newValue);
      }
    }

    public void removeLastInfo() {
      remove(nbGeom_-1);
    }
    
    public void remove(int _idx) {
      // pour les param atomique: on supprime la valeur a chaque list des parametres
      for (final Iterator<GISAttributeInterface> it = atomicAttribute_.keySet().iterator(); it.hasNext();) {
        GISAttributeInterface att=it.next();
        final ArrayList<Object> l = atomicAttribute_.get(att);
        l.remove(_idx);
      }
      nbGeom_--;
    }
  }

  public class EllipseData extends LigneBriseeData{
    public EllipseData(){
      super();
    }
  }
  
  /**
   * @author fred deniger
   * @version $Id$
   */
  public class PointData implements ZEditionAttributesDataI {

    Map values_;

    public PointData() {
      values_ = new HashMap();
      if (name_ != null) {
        for (int i = name_.length - 1; i >= 0; i--) {
          values_.put(name_[i], null);
        }
      }
    }

    @Override
    public GISAttributeInterface getAttribute(final int _i) {
      return name_[_i];
    }

    public void addPoint(GrPoint _pt) {
      if (values_ != null && creator_ != null) {
        for (final Iterator it = values_.entrySet().iterator(); it.hasNext();) {
          final Map.Entry e = (Map.Entry) it.next();
          Object o = null;
          GISAttributeInterface att=(GISAttributeInterface) e.getKey();
          // Cas particulier du Z, issu d'un accrochage.
          if (att==GISAttributeConstants.BATHY && _pt!=null)
            o = new Double(_pt.z_);
          else
            o = creator_.getData(att);
          e.setValue(o);
        }
      }
    }

    @Override
    public int getNbVertex() {
      return 1;
    }

    @Override
    public int getNbValues() {
      return name_.length;
    }

    @Override
    public Object getValue(final GISAttributeInterface _numValue, final int _numGeometry) {
      return values_.get(_numValue);
    }

    @Override
    public void setValue(final GISAttributeInterface _numValue, final int _numGeometry, final Object _val) {
      values_.put(_numValue, _val);
    }

    public void removeLastInfo() {}

  }

  /**
   * Valeur pour un rectangle.
   * 
   * @author Fred Deniger
   * @version $Id$
   */
  public class RectangleData extends LigneBriseeData {

    public RectangleData() {
      super();
    }
  }

  /**
   * Valeur pour un multipoint.
   * 
   * @author Fred Deniger
   * @version $Id$
   */
  public class MultiPointData extends LigneBriseeData {

    public MultiPointData() {
      super();
    }
  }

  protected ZEditionAttributesCreatorInterface creator_;

  final GISAttributeInterface[] name_;

  /**
   * @param _name les noms
   */
  public ZEditionAttibutesContainer(final GISAttributeInterface[] _name) {
    name_ = _name;
  }

  /**
   * @return cree un container pour les lignes
   */
  public LigneBriseeData createLigneBriseeData() {
    if (FuLog.isTrace()) {
      FuLog.trace("EZM: " + getClass().getName() + ".createLigneData for " + System.identityHashCode(this));
    }
    return new LigneBriseeData();
  }

  /**
   * @return cree un container pour les lignes
   */
  public LigneBriseeData createLigneBriseeDataFor(LineString str) {
    if (FuLog.isTrace()) {
      FuLog.trace("EZM: " + getClass().getName() + ".createLigneData for " + System.identityHashCode(this));
    }
    int nbGeo = str.getNumPoints();
    if (str.isClosed()) nbGeo--;
    LigneBriseeData res = new LigneBriseeData();
    for (int i = 0; i < nbGeo; i++) {
      res.addPoint(null);
    }
    return res;
  }

  /**
   * @param _i l'indice de la valeur
   * @return la valeur par default
   */
  public Object getDefaultValue(final int _i) {
    return name_ == null ? null : name_[_i].getDefaultValue();
  }

  /**
   * @return cree un container pour les points
   */
  public MultiPointData createMultiPointData() {
    return new MultiPointData();
  }

  /**
   * @return cree un container pour les points
   */
  public PointData createPointData() {
    return new PointData();
  }

  /**
   * @return cree un container pour les rectangles
   */
  public RectangleData createRectangleData() {
    return new RectangleData();
  }

  /**
   * 
   * @return cree un container pour les ellipses
   */
  public EllipseData createEllipseData(){
    return new EllipseData();
  }
  
  /**
   * @return le panneau donnant les valeurs
   */
  public final ZEditionAttributesCreatorInterface getCreator() {
    return creator_;
  }

  /**
   * @param _i l'indice demande
   * @return le nom
   */
  public GISAttributeInterface getName(final int _i) {
    return name_[_i];
  }

  /**
   * @return le nombre de valeurs support�es par ce container
   */
  public int getNbValues() {
    return name_.length;
  }

  /**
   * @param _creator le createur de valeur: un panneau en general
   */
  protected final void setCreator(final ZEditionAttributesCreatorInterface _creator) {
    if (FuLog.isTrace()) {
      FuLog.trace("EZM: add creator " + _creator.getClass() + "for " + System.identityHashCode(this));
    }
    creator_ = _creator;
  }

}
