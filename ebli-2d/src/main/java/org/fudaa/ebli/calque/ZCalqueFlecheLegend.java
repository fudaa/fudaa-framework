/*
 * @creation 7 nov. 06
 *
 * @modification $Date: 2007-06-20 12:23:11 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.trace.TraceLigne;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author fred deniger
 * @version $Id: ZCalqueFlecheLegend.java,v 1.13 2007-06-20 12:23:11 deniger Exp $
 */
public class ZCalqueFlecheLegend implements PropertyChangeListener {
  public class FlecheComponent implements Icon {
    int flecheWidth;
    final TraceLigne l_ = new TraceLigne(TraceLigne.LISSE, 1.5f, Color.BLACK);
    int w_;
    int h_;

    public FlecheComponent() {
      super();
      w_ = 75;
      h_ = 10;
    }

    @Override
    public int getIconHeight() {
      return h_;
    }

    public void setIconHeight(int _h) {
      h_ = _h;
    }

    public void setIconWidth(int _w) {
      w_ = _w;
    }

    @Override
    public int getIconWidth() {
      return w_;
    }

    @Override
    public void paintIcon(Component _c, Graphics _g, int _x, int _y) {
      if (flecheWidth > 0) {
        _g.translate(_x, _y);
        final int middle = h_ / 2 - 1;
        final Graphics2D g2d = (Graphics2D) _g;
        final int deb = (int) ((((double) w_) - flecheWidth) / 2d);
        final int fin = deb + flecheWidth;
        Color c = support_.isPaletteUsed() ? support_.getPaletteColorFor(realLength_) :
            support_.getLineModel(0).getCouleur();
        if (c == null) {
          c = Color.BLACK;
        }
        l_.setCouleur(c);
        l_.dessineTrait(g2d, deb, middle, fin, middle);
        l_.dessineTrait(g2d, fin, middle, fin - 3, middle - 3);
        l_.dessineTrait(g2d, fin, middle, fin - 3, middle + 3);
        _g.translate(-_x, -_y);
      }
    }
  }

  FlecheComponent fleche_;
  JLabel lbfleche_;
  BuLabel lbInfo_;
  CtuluRange minMax_ = new CtuluRange();
  private final PropertyChangeSupport publisher = new PropertyChangeSupport(this);

  public void addPropertyChangeListener(PropertyChangeListener listener) {
    publisher.addPropertyChangeListener(listener);
  }

  public void removePropertyChangeListener(PropertyChangeListener listener) {
    publisher.removePropertyChangeListener(listener);
  }

  final ZCalqueFleche support_;

  public ZCalqueFlecheLegend(final ZCalqueFleche _support) {
    super();
    support_ = _support;
    String[] props = FlecheScaleData.getScaleProperties();
    for (int i = 0; i < props.length; i++) {
      support_.addPropertyChangeListener(props[i], this);
    }
    lbInfo_ = new BuLabel();
    lbInfo_.setHorizontalAlignment(SwingConstants.CENTER);
    lbInfo_.setHorizontalTextPosition(SwingConstants.CENTER);
    fleche_ = new FlecheComponent();

    pnSize_ = new BuPanel();
    pnSize_.setLayout(new BuVerticalLayout(2, true, false));
    lbfleche_ = new JLabel(fleche_);
    pnSize_.add(lbfleche_);
    pnSize_.add(lbInfo_);
    pnSize_.setOpaque(false);
    pnSize_.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.GRAY
        .brighter()), BuBorders.EMPTY5555));

    if (support_.scale_.scaleFont_ != null) {
      lbInfo_.setFont(support_.scale_.scaleFont_);
    }
  }

  private boolean initialized;

  /**
   * @deprecated Not used
   */
  public void intializeFlechLegend() {
//    if (initialized) return;
    initialized = true;
    init();
    initLegendPalette();
    support_.paletteLegende_.addUserComponent(pnSize_);
  }

  public JPanel getFlecheComponent() {
    return pnSize_;
  }

  final BuPanel pnSize_;

  protected void init() {
    if (doNotResizeData()) {
      return;
    }
    minMax_.setToNill();
    fleche_.flecheWidth = 0;
    lbInfo_.setText(CtuluLibString.EMPTY_STRING);
    realLength_ = 0;
    publisher.firePropertyChange("repaint", false, true);
  }

  protected void addNorme(final double _norme) {
    if (doNotResizeData()) {
      return;
    }
    minMax_.expandTo(_norme);
  }

  private boolean doNotResizeData() {
    return fleche_.flecheWidth > 0 && !mustResize_;
  }

  public boolean isLegendVisible() {
    return !support_.getScaleData().isSameNorm();
  }

  protected void finish() {
    if (doNotResizeData()) {
      return;
    }
    final boolean newVisible = !support_.getScaleData().isSameNorm();
    final boolean changed = newVisible != pnSize_.isVisible();
    if (changed) {
      pnSize_.setVisible(newVisible);
      // support_.repaint();
      // return;
    }

    // La longueur des vecteurs (donn�e en pixel/unit�) d�pend de leur norme.
    if (newVisible) {
      double val = 0;
      final GrSegment seg = new GrSegment();
      seg.e_ = new GrPoint();
      seg.o_ = new GrPoint();
      int max = fleche_.getIconWidth() - 3;

      // La fleche legende est trac�e suivant une valeur de norme fix�e.
      if (support_.getScaleData().getLegendUseFixRealNorm()) {
        Double fixRealNorm = support_.getScaleData().getLegendFixRealNorm();
        if (fixRealNorm == null) {
          val = Math.max(1, realLength_);
        } else {
          val = fixRealNorm;
        }

        // B.M. 01/06/2017 : On fait en sorte que la norme ne sorte pas du cadre de la l�gende.
        double pixParUnit = 20;
        final Insets set = lbfleche_.getInsets();
        if (set != null && fixRealNorm != null) {
          max = max - (set.left + set.right);
          pixParUnit = max / fixRealNorm;
        }
        support_.getScaleData().setPixelPerUnit(pixParUnit);
      }

      // La fleche legende est trac�e pour tenir dans les dimensions du composant
      else {
//        val = minMax_.getMin();
//        final NumberIterator it = new NumberIterator();
//        it.init(val, minMax_.getMax(), 10);
//
//        final Insets set = lbfleche_.getInsets();
//        if (set != null) {
//          max = max - (set.left + set.right);
//        }
//        initSeg(seg, val);
//        support_.toEcran(seg, support_.getVersEcran());
//        final double longueurXY = seg.longueurXY();
//        boolean isIn = longueurXY <= max;
//        while (isIn && it.hasNext()) {
//          it.nextMajor();
//          final double newVal = it.currentValue();
//          initSeg(seg, newVal);
//          support_.toEcran(seg, support_.getVersEcran());
//          isIn = seg.longueurXY() < max;
//          if (isIn) {
//            val = newVal;
//
//          }
//        }
//
//        // on va essayer d'utiliser des valeurs enti�res, pour les valeurs sup�rieures � 1.
//        String format = fmt_.format(val);
//        if (!format.endsWith(".0")) {
//          // l'entier + grand
//          double newVal = Math.ceil(val);
//          if (testWidth(seg, max, newVal)) {
//            val = newVal;
//          }
//          // l'entier + petit
//          else {
//            newVal = Math.floor(val);
//            if (testWidth(seg, max, newVal)) {
//              val = newVal;
//            }
//          }
//
//        }

        // B.M. 01/03/2011 - Nouveau traitement + simple
        final Insets set = lbfleche_.getInsets();
        if (set != null) {
          max = max - (set.left + set.right);
        }
        initSeg(seg, max);
        support_.getScaleData().toReal(seg);
        val = seg.longueurXY();

        // La valeur est sup�rieure � 1 => on prend l'entier au dessous.
        if (val >= 1) {
          val = (int) val;
        }
        // La valeur est inf�rieure � 1 => On prend une valeur arrondie.
        else if (val < 1 && val > 0) {
          int i = 1;
          while ((int) (val * Math.pow(10, i)) == 0) {
            i++;
          }
          val = (int) (val * Math.pow(10, i + 1)) / Math.pow(10, i + 1);
        }
      }

      initSeg(seg, val);
      support_.toEcran(seg, support_.getVersEcran());
      fleche_.flecheWidth = (int) seg.longueurXY();
      if (!support_.getScaleData().getLegendUseFixRealNorm() && !isScreenWidthOk(fleche_.flecheWidth, max)) {
        initSeg(seg, max);
        support_.getScaleData().toReal(seg);
        val = seg.longueurXY();
        initSeg(seg, val);
        support_.toEcran(seg, support_.getVersEcran());
        fleche_.flecheWidth = (int) seg.longueurXY();
      }
      realLength_ = val;
      String format = fmt_.format(val);
      if (format.charAt(0) == '.') {
        format = '0' + format;
      }
      String flecheUnit = support_.getFlecheUnit();
      if (flecheUnit != null) {
        lbInfo_.setText(format + CtuluLibString.ESPACE + flecheUnit);
      } else {
        lbInfo_.setText(format);
      }

      // Tous les vecteurs sont repr�sent�s suivant la m�me longueur.
    } else {
      init();
    }
    if (changed) {
      support_.repaint();
    } else {
      publisher.firePropertyChange("repaintFinish", Boolean.FALSE, Boolean.TRUE);
    }
  }

  double realLength_;

  /**
   * Teste que la longueur de la fleche l�gende repr�sentant la valeur donn�e
   * est valide (elle rentre bien dans le cadre de la l�gende).
   *
   * @param _seg Un segment pour le calcul.
   * @param _max La largeur du cadre.
   * @param _newVal La valeur donn�e en unit� de longueur.
   * @return True si la fleche rentre dans le cadre.
   */
  private boolean testWidth(final GrSegment _seg, final int _max, final double _newVal) {
    initSeg(_seg, _newVal);
    support_.toEcran(_seg, support_.getVersEcran());
    return isScreenWidthOk((int) _seg.longueurXY(), _max);
  }

  /**
   * Teste que la longueur de la fleche l�gende est valide (elle rentre bien
   * dans le cadre de la l�gende). Les longueurs sont donn�es en pixel.
   *
   * @param _flecheMax Longueur de la fleche.
   * @param _max La largeur du cadre.
   * @return True si la fleche rentre dans le cadre.
   */
  private static boolean isScreenWidthOk(final int _flecheMax, final int _max) {
    return _flecheMax > 4 && _flecheMax <= _max;
  }

  final CtuluNumberFormatFortran fmt_ = new CtuluNumberFormatFortran(6);

  protected void initSeg(final GrSegment _seg, final double _v) {
    _seg.e_.x_ = 0;
    _seg.e_.y_ = 0;
    _seg.o_.x_ = _v;
    _seg.o_.y_ = 0;
  }

  /**
   * @deprecated Not used
   */
  private void initLegendPalette() {
    final BCalqueLegende l = support_.getLegende();

    support_.paletteLegende_ = new BPalettePlageLegende(support_.paletteCouleur_);
    BCalqueLegendePanel pn = new BCalqueLegendePanel(support_, support_.getTitle());
    pn.add(support_.paletteLegende_, BorderLayout.CENTER);

    if (l == null) {
      return;
    }
    l.enleve(support_);
    l.ajouteLegendPanel(pn);
    l.updateAll();
  }

  /**
   * @deprecated Not used
   */
  protected void construitLegende() {
    if (support_.paletteLegende_ == null) {
      return;
    }

    if (support_.paletteLegende_.getModel() == support_.paletteCouleur_) {
      support_.paletteLegende_.allPaletteModified(support_.paletteCouleur_);
    } else {
      support_.paletteLegende_.setModel(support_.paletteCouleur_);
    }
  }

  boolean mustResize_;

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    mustResize_ = true;
    String prop = _evt.getPropertyName();
    // we recompute the labels in this case:
    if (FlecheScaleData.PROP_IS_SAME_NORM_USED.equals(prop)) {
      support_.repaint();
    }
    if (FlecheScaleData.PROP_LEGEND_CONSTANT_NORM_VALUE.equals(prop)
        || FlecheScaleData.PROP_LEGEND_USE_CONSTANT_NORM_VALUE.equals(prop)) {
      finish();
    }
    publisher.firePropertyChange("repaint", false, true);
  }

  /**
   * @return the lbInfo_
   */
  public BuLabel getLbInfo() {
    return lbInfo_;
  }

  /**
   * @return the fleche_
   */
  public FlecheComponent getFleche() {
    return fleche_;
  }

  /**
   * @return the initialized
   */
  public boolean isInitialized() {
    return initialized;
  }
}
