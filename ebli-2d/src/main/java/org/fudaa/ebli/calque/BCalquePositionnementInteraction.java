/*
 * @file         BCalquePositionnementInteraction.java
 * @creation     2000-09-13
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.repere.RepereEventListener;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un calque pour la navigation dans la vue calque. Il permet de d�placer interactivement la zone visualis�e par glisser/d�placer. <p> Le deplacement
 * provoque la diffusion d'un �v�nement <I>RepereEvent</I>.
 *
 * @version $Id: BCalquePositionnementInteraction.java,v 1.12 2006-09-19 14:55:45 deniger Exp $
 * @author Bertrand Marchand
 */
public class BCalquePositionnementInteraction extends BCalqueInteraction implements MouseListener, MouseMotionListener {

  private GrPoint ptOrig_; // Point d'origine du d�placement
  private final Set repereEventListeners_; // Listeners de modification de rep�re
  private BVueCalque vueCalque_; // VueCalque du calque
  private boolean restoreDefaultLayer;

  /**
   * Cr�ation d'un calque de positionnement.
   */
  public BCalquePositionnementInteraction() {
    super();
    setDestructible(false);
    repereEventListeners_ = new HashSet();
    vueCalque_ = null;
  }

  @Override
  public Cursor getSpecificCursor() {
    return EbliResource.EBLI.getCursor("main", -1, new Point(8, 8));
  }

  @Override
  public String getDescription() {
    return EbliLib.getS("D�placer la vue");
  }

  /**
   * Retourne la VueCalque contenant le calque.
   *
   * @return La <code>BVueCalque</code> contenant le calque. <code>null</code> si le calque n'a pas encore �t� affect� � une fen�tre de vue.
   */
  public BVueCalque getVueCalque() {
    return vueCalque_;
  }

  /**
   * D�finit la VueCalque contenant le calque. Les �v�nements
   * <code>RepereEvent</code> g�n�r�s par le calque sont automatiquement envoy�s vers la VueCalque.
   *
   * @param _v La <code>BVueCalque</code> contenant le calque. Peut �tre <code>null</code> si le calque n'a plus affect� � une fen�tre de vue.
   */
  public void setVueCalque(final BVueCalque _v) {
    if (vueCalque_ != _v) {
      if (vueCalque_ != null) {
        removeRepereEventListener(vueCalque_);
      }
      vueCalque_ = _v;
      if (vueCalque_ != null) {
        addRepereEventListener(vueCalque_);
      }
    }
  }

  /**
   * Dessin de l'icone.
   *
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
  }

  /**
   * Ajout d'un auditeur � l'�venement <I>RepereEvent</I>.
   *
   * @param _listener Auditeur de l'�v�nement
   */
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  /**
   * Suppression d'un auditeur � l'�venement <I>RepereEvent</I>.
   *
   * @param _listener Auditeur de l'�v�nement
   */
  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  /**
   * Notification aux auditeurs qu'un �venement <I>RepereEvent</I> s'est produit.
   */
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    for (final Iterator i = repereEventListeners_.iterator(); i.hasNext();) {
      ((RepereEventListener) i.next()).repereModifie(_evt);
    }
  }

  /*
   * Ev�nements souris PRESSED => Saisie du point �cran courant DRAGGED => D�placement de la vue en mode affichage
   * rapide RELEASED => Trac� de la vue en mode affichage raffin�
   */
  /*
   * Methode invoqu�e quand on appuie sur un bouton de la souris. Saisie de la position �cran courante.
   */
  @Override
  public void mousePressed(final MouseEvent _evt) {
    restoreDefaultLayer = false;
    if (SwingUtilities.isMiddleMouseButton(_evt)) {
      if (isGele()) {
        restoreDefaultLayer = true;
      }
      setGele(false);
    }
    if (isGele() || _evt.isConsumed()) {
      return;
    }
    ptOrig_ = new GrPoint(_evt.getX(), _evt.getY(), 0.);
  }

  /*
   * Methode invoquee quand on lache un bouton de la souris. Trac� de la vue en mode affichage raffin�
   */
  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (timer != null) {
      timer.stop();
      timer = null;
    }
    if (isGele() || _evt.isConsumed()) {
      return;
    }
    final GrPoint ptCur = new GrPoint(_evt.getX(), _evt.getY(), 0.);
    final GrVecteur v = ptOrig_.soustraction(ptCur);
    if (!vueCalque_.checkTranslation(v.applique(vueCalque_.getVersReel()))) {
      v.x_ = 0;
      v.y_ = 0;
    }
    // v.autoApplique(getVersReel());
    ptOrig_ = ptCur;


    final RepereEvent re = new RepereEvent(this, false);
    re.ajouteTransformation(RepereEvent.TRANS_X, v.x_, true);
    // B.M. 20/09/2000 La translation est invers�e suivant y en attendant de
    // comprendre pourquoi
    re.ajouteTransformation(RepereEvent.TRANS_Y, -v.y_, true);
    fireRepereEvent(re);
    if (restoreDefaultLayer) {
      restoreDefaultLayer = false;
      if (defaultCalqueInteraction != null) {
        defaultCalqueInteraction.setGele(false);
      }
    }
  }
  Timer timer;
  GrPoint ptCur;

  /*
   * Methode invoqu�e quand on deplace la souris avec un bouton appuy�. D�placement de la vue en mode affichage rapide
   */
  @Override
  public void mouseDragged(final MouseEvent _evt) {
    if (isGele() || _evt.isConsumed()) {
      return;
    }
    ptCur = new GrPoint(_evt.getX(), _evt.getY(), 0.);
    if (timer == null) {
      timer = new Timer(500, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          doTranslation();
        }
      });
      timer.setInitialDelay(300);
      timer.start();
    }

  }

  /**
   * <I>Sans objet</I>.
   */
  @Override
  public void mouseMoved(final MouseEvent _evt) {
  }

  /**
   * <I>Sans objet</I>.
   */
  @Override
  public void mouseClicked(final MouseEvent _evt) {
  }

  /**
   * <I>Sans objet</I>.
   */
  @Override
  public void mouseEntered(final MouseEvent _evt) {
  }

  /**
   * <I>Sans objet</I>.
   */
  @Override
  public void mouseExited(final MouseEvent _evt) {
  }
  BCalqueInteraction defaultCalqueInteraction;

  void setDefaultCalqueInteraction(BCalqueInteraction interaction) {
    this.defaultCalqueInteraction = interaction;
  }

  private void doTranslation() {
    if(ptCur==null){
      return ;
    }
    final GrVecteur v = ptOrig_.soustraction(ptCur);
    
    v.autoApplique(getVersReel());
    if (!vueCalque_.checkTranslation(v)) {
      return;
    }
    v.autoApplique(getVersEcran());
    ptOrig_ = ptCur;
    ptCur=null;
    final RepereEvent re = new RepereEvent(this, true);
    re.ajouteTransformation(RepereEvent.TRANS_X, v.x_, true);
    // B.M. 20/09/2000 La translation est invers�e suivant y en attendant de
    // comprendre pourquoi
    re.ajouteTransformation(RepereEvent.TRANS_Y, -v.y_, true);
    fireRepereEvent(re);
  }
}
