/*
 * @file         ZSelectionEvent.java
 * @creation     2002-10-01
 * @modification $Date: 2006-07-13 13:35:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
/**
 * Un �v�nement <I>ZSelectionEvent</I>. Cet �venement est d�clench� par un calque de
 * s�lection et contient la liste des GrContour.
 *
 * @version      $Id: ZSelectionEvent.java,v 1.8 2006-07-13 13:35:45 deniger Exp $
 * @author       Bertrand Marchand
 */
public class ZSelectionEvent {
  private final ZCalqueAffichageDonneesInterface source_;
  public ZSelectionEvent(final ZCalqueAffichageDonneesInterface _source) {
    source_= _source;
  }
  public ZCalqueAffichageDonneesInterface getSource() {
    return source_;
  }
}
