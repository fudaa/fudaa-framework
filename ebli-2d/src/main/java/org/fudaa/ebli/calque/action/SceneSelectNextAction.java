/*
 * @creation     3 juil. 2008
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Une action pour sélectionner le sommet suivant d'une géométrie.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class SceneSelectNextAction extends EbliActionSimple implements ZSelectionListener {
  ZSceneEditor editor_;
  
  public SceneSelectNextAction(ZSceneEditor _editor) {
    super(EbliLib.getS("Sélection de l'objet suivant"), BuResource.BU.getIcon("avancer2"), "SELECT_NEXT");
    setKey(KeyStroke.getKeyStroke(KeyEvent.VK_G,KeyEvent.CTRL_MASK));
    editor_=_editor;
    _editor.getScene().addSelectionListener(this);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    editor_.selectNextObject();
  }

  @Override
  public void updateStateBeforeShow() {
    ZScene scn=editor_.getScene();
    SceneSelectionHelper hlp=editor_.getScene().getSelectionHelper();

    boolean b=true;
    // Si la selection n'est pas null
    b=b && !scn.isSelectionEmpty();
    // Si la selection est au nombre de 1.
    b=b && (!scn.isAtomicMode() || hlp.getNbAtomicSelected()==1);
    b=b && (scn.isAtomicMode() || scn.getLayerSelection().getNbSelectedIndex()==1);

    setEnabled(b);
  }

  @Override
  public String getEnableCondition() {
    return EbliLib.getS("Sélectionner une géométrie ou un sommet");
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    updateStateBeforeShow();
  }
}
