/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.BPaletteDeplacement;
import org.fudaa.ebli.calque.edition.ZCalqueDeplacementInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action pour faire un d�placement d'objets GIS, reagissant au changement de calque selectionn�.
 * Cette action permet un d�placement � la souris, ou par un panneau Dx,Dy,Dz.
 * 
 * @author Fred Deniger, marchand@deltacad.fr
 * @version $Id: SceneDeplacementAction.java,v 1.1.2.1 2008-05-13 12:10:47 bmarchan Exp $
 */
public class SceneDeplacementAction extends EbliActionPaletteTreeModel implements PropertyChangeListener {

  protected ZSceneEditor sceneEditor_;
  protected EbliFormatterInterface formatter_;
  protected ZCalqueDeplacementInteraction cqDep_;
  protected ZEbliCalquesPanel pnCalques_;

  /**
   * @param _m l'arbre des calques
   * @param _cqDep Le calque d'interaction pour le d�placement � la souris. Si
   *          <code>null</code>, le d�placement ne se fait que par le panneau de
   *          saisie de Dx,Dy,Dz
   */
  public SceneDeplacementAction(final TreeSelectionModel _m, final ZEditorDefault _editor, EbliFormatterInterface _formatter, ZCalqueDeplacementInteraction _cqDep) {
    super(EbliLib.getS("D�placer des objets"), EbliResource.EBLI.getToolIcon("fleche-deplacement"), "GLOBAL_MOVE", _m);
    setResizable(true);
    formatter_=_formatter;
    pnCalques_=_editor.getPanel();
    // Pour capter l'activation d'un autre calque.
    if (_cqDep!=null)
      pnCalques_.getController().addPropertyChangeListener("gele", this);
    
    cqDep_=_cqDep;
    setSceneEditor(_editor.getSceneEditor());
    if (cqDep_!=null)
      cqDep_.addPropertyChangeListener("gele",this);
    setEnabled(isTargetValid(super.target_));
  }
  /**
   * @param _sceneEditor l'editeur
   */
  private void setSceneEditor(final ZSceneEditor _sceneEditor) {
    sceneEditor_ = _sceneEditor;
  }

  @Override
  protected boolean isTargetValid(final Object _o) {
    // On n'utilise pas le calque actif de la scene (probablement pas encore initialis�), mais l'objet pass�.
    return (sceneEditor_!=null && (!sceneEditor_.getScene().isRestrictedToCalqueActif() || _o instanceof ZCalqueEditable/*sceneEditor_.getScene().isCalqueActifEditable()*/));
  }

  @Override
  public void changeAction(){
    super.changeAction();
    if (cqDep_!=null) {
      if (isSelected()) {
        pnCalques_.setCalqueInteractionActif(cqDep_);
      }
      else {
        pnCalques_.setCalqueSelectionActif();
      }
    }
  }


  @Override
  public void updateBeforeShow() {
    if (palette_!=null) ((BPaletteDeplacement)palette_).updateState();
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent() {
    BPaletteDeplacement pal=new BPaletteDeplacement(sceneEditor_,formatter_);
    if (cqDep_!=null)
      pal.setCalqueDeplacement(cqDep_);
    return pal;
  }

  /**
   * Appel� quand le statut gel� d'un calque d'interaction est modifi�.
   * @param _evt L'evenement. La source contient le calque gele.
   */
  @Override
  public void propertyChange(final PropertyChangeEvent _evt){
    if (_evt.getSource()!=cqDep_ &&!((Boolean)_evt.getNewValue()).booleanValue()&&isSelected()) {
      setSelected(false);
      super.changeAction();
    }
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent e) {
    if (cqDep_!=null)
      pnCalques_.setCalqueSelectionActif();
  }
  
  @Override
  public String getEnableCondition() {
    if (sceneEditor_!=null && sceneEditor_.getScene().isRestrictedToCalqueActif()) {
      return EbliLib.getS("Un calque �ditable doit �tre selectionn�");
    }
    else {
      return super.getEnableCondition();
    }
  }
}
