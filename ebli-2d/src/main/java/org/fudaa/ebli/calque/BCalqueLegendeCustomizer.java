/**
 * @creation 6 ao�t 2004
 * @modification $Date: 2007-06-20 12:23:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import org.fudaa.ctulu.gui.CtuluCellBooleanEditor;
import org.fudaa.ctulu.gui.CtuluCellBooleanRenderer;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurAncre;
import org.fudaa.ebli.controle.BSelecteurReduitFonteNewVersion;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceTraitRenderer;

/**
 * @author Fred Deniger
 * @version $Id: BCalqueLegendeCustomizer.java,v 1.21 2007-06-20 12:23:11 deniger Exp $
 */
public class BCalqueLegendeCustomizer extends BuTabbedPane implements ChangeListener, ActionListener, ItemListener {

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.SELECTED) {
      final TraceLigneModel d = (TraceLigneModel) cbBorder_.getSelectedItem();
      int epaisseur = 0;
      if (d != null) {
        epaisseur = (int) d.getEpaisseur();
      }
      l_.setBordEpaisseur(epaisseur);
    }
  }

  private class TableLegendModel extends AbstractTableModel implements ComboBoxModel, HierarchyListener,
      ContainerListener {

    @Override
    public void componentAdded(final ContainerEvent _e) {
      fireTableDataChanged();
      fireComboBoxModelChanged();
    }

    @Override
    public void componentRemoved(final ContainerEvent _e) {
      fireTableDataChanged();
      fireComboBoxModelChanged();
    }

    Object selected_;

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireComboChangeEvent(-1);
      }
    }

    protected void fireComboBoxModelChanged() {
      final Object[] listeners = listenerList.getListenerList();
      ListDataEvent e = null;
      for (int i = listeners.length - 2; i >= 0; i -= 2) {
        if (listeners[i] == ListDataListener.class) {
          if (e == null) {
            e = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, getRowCount());
          }
          ((ListDataListener) listeners[i + 1]).intervalAdded(e);
        }
      }
      if (!l_.containsLegend((BCalqueAffichage) selected_)) {
        setSelectedItem(null);
      }
    }

    @Override
    public void addListDataListener(final ListDataListener _l) {
      super.listenerList.add(ListDataListener.class, _l);
    }

    @Override
    public Object getElementAt(final int _index) {
      return l_.getLegendDisplayAtPosition(_index);
    }

    @Override
    public int getSize() {
      return getRowCount();
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      if (_columnIndex == 1) {
        return String.class;
      }
      return Boolean.class;
    }

    @Override
    public void removeListDataListener(final ListDataListener _l) {
      super.listenerList.remove(ListDataListener.class, _l);
    }

    protected void fireComboChangeEvent(final int _idx) {
      final Object[] listeners = listenerList.getListenerList();
      ListDataEvent e = null;
      for (int i = listeners.length - 2; i >= 0; i -= 2) {
        if (listeners[i] == ListDataListener.class) {
          if (e == null) {
            e = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, _idx, _idx);
          }
          ((ListDataListener) listeners[i + 1]).contentsChanged(e);
        }
      }
    }

    /**
     * Ajoute un listener pour les evt d'ajout/suppression de composant.
     */
    public TableLegendModel() {
      l_.addContainerListenerForLegendPanel(this);
    }

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return "V";
      } else if (_column == 1) {
        return EbliLib.getS("L�gende");
      }
      return EbliLib.getS("R�duit");
    }

    @Override
    public int getRowCount() {
      return l_.getNbLegend();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      final BCalqueAffichage c = l_.getLegendDisplayAtPosition(_rowIndex);
      if (_columnIndex == 0) {
        return Boolean.valueOf(l_.getPanelIsSetToVisible(c));
      }
      if (_columnIndex == 2) {
        final JComponent cp = l_.getMainComponent(c);
        if (cp instanceof BPalettePlageLegende) {
          final BPalettePlageLegende leg = (BPalettePlageLegende) cp;
          if (leg.isReduitEnable()) {
            return Boolean.valueOf(leg.isReduit());
          }

        }
        return null;
      }
      return c;
    }

    @Override
    public void hierarchyChanged(final HierarchyEvent _e) {
      fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return _columnIndex == 0 || (_columnIndex == 2 && getValueAt(_rowIndex, _columnIndex) != null);
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        final boolean b = ((Boolean) _value).booleanValue();
        l_.setVisible(l_.getLegendDisplayAtPosition(_rowIndex), b);
      } else if (_columnIndex == 2) {
        final boolean b = ((Boolean) _value).booleanValue();
        final JComponent cp = l_.getMainComponent(l_.getLegendDisplayAtPosition(_rowIndex));
        if (cp instanceof BPalettePlageLegende) {
          ((BPalettePlageLegende) cp).setUseReduit(b);
          l_.doLayout();
          l_.repaint();
        }

      }

    }
  }

  static class LegendLineComboboxModel extends AbstractListModel implements ComboBoxModel {

    private final TraceLigneModel[] size_ = new TraceLigneModel[] { null,
        new TraceLigneModel(TraceLigne.LISSE, 1, Color.BLACK), new TraceLigneModel(TraceLigne.LISSE, 2, Color.BLACK),
        new TraceLigneModel(TraceLigne.LISSE, 3, Color.BLACK) };
    Object selected_;

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    protected void setSelectedEpaisseur(final float _epaisseur) {
      if (_epaisseur <= 0) {
        setSelectedItem(null);
      }
      for (int i = 1; i < size_.length; i++) {
        if (size_[i].getEpaisseur() == _epaisseur) {
          setSelectedItem(size_[i]);
          return;
        }
      }
      setSelectedItem(null);
    }

    public void setColor(final Color _c) {
      for (int i = size_.length - 1; i >= 0; i--) {
        if (size_[i] != null) {
          size_[i].setColor(_c);
        }
      }
      fireContentsChanged(this, 0, size_.length);
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    @Override
    public Object getElementAt(final int _index) {
      return size_[_index];
    }

    @Override
    public int getSize() {
      return size_.length;
    }
  }

  JToggleButton basDroit_;
  JToggleButton basGauche_;
  JButton btFont_;
  JButton btColor_;
  JButton btColorBord_;
  JButton btPosition_;
  JComboBox cbPosition_;
  JCheckBox cbVisible_;
  JCheckBox cbSameHeight_;
  JToggleButton hautDroit_;
  LegendLineComboboxModel cbBorder_;
  TraceIcon icBackground_;
  TraceIcon icBord_;

  JToggleButton hautGauche_;

  BCalqueLegende l_;

  /**
   * @param _l la legende a modifier
   */
  public BCalqueLegendeCustomizer(final BCalqueLegende _l) {
    super();
    l_ = _l;
    final BuPanel firstPn = new BuPanel();
    firstPn.setLayout(new BuVerticalLayout(0,true,true));
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 4, 2));

    cbVisible_ = new BuCheckBox();
    cbVisible_.setSelected(l_.isVisible());
    cbVisible_.addChangeListener(this);
    cbSameHeight_ = new BuCheckBox();
    cbSameHeight_.setSelected(l_.isSameHeight());
    cbSameHeight_.addChangeListener(this);
    pn.add(new BuLabel(EbliLib.getS("Visible")));
    pn.add(cbVisible_);
    final String s = EbliLib.getS("Si s�lectionn�, toutes les l�gendes auront la m�me hauteur");
    final BuLabel buLabel = new BuLabel(EbliLib.getS("Hauteurs constantes"));
    buLabel.setToolTipText(s);
    pn.add(buLabel);
    pn.add(cbSameHeight_);
    cbSameHeight_.setToolTipText(s);
    pn.add(new BuLabel(EbliLib.getS("Position")));
    final BuPanel pos = new BuPanel();
    pos.setLayout(new BuGridLayout(2, 1, 1, false, false, false, false, false));
    final ButtonGroup g = new ButtonGroup();
    hautGauche_ = new BuToggleButton();
    hautGauche_.setIcon(EbliResource.EBLI.getIcon("haut-gauche"));
    g.add(hautGauche_);
    pos.add(hautGauche_);
    hautDroit_ = new BuToggleButton();
    hautDroit_.setIcon(EbliResource.EBLI.getIcon("haut-droit"));
    g.add(hautDroit_);
    pos.add(hautDroit_);
    basGauche_ = new BuToggleButton();
    basGauche_.setIcon(EbliResource.EBLI.getIcon("bas-gauche"));
    g.add(basGauche_);
    pos.add(basGauche_);
    basDroit_ = new BuToggleButton();
    basDroit_.setIcon(EbliResource.EBLI.getIcon("bas-droit"));
    g.add(basDroit_);
    pos.add(basDroit_);
    pn.add(pos);
    final int ance = l_.getAncre();
    if (ance == BSelecteurAncre.BOT_LEFT) {
      basGauche_.setSelected(true);
    } else if (ance == BSelecteurAncre.BOT_RIGHT) {
      basDroit_.setSelected(true);
    } else if (ance == BSelecteurAncre.UP_LEFT) {
      hautGauche_.setSelected(true);
    } else if (ance == BSelecteurAncre.UP_RIGHT) {
      hautDroit_.setSelected(true);
    }
    hautGauche_.addActionListener(this);
    hautDroit_.addActionListener(this);
    basGauche_.addActionListener(this);
    basDroit_.addActionListener(this);
    firstPn.add(pn);
    final TableLegendModel m = new TableLegendModel();
    final JTable t = new JTable(m);
    TableColumn c = t.getColumnModel().getColumn(0);
    c.setWidth(10);
    c.setPreferredWidth(10);
    final CtuluCellBooleanRenderer ctuluCellBooleanRenderer = new CtuluCellBooleanRenderer();
    final CtuluCellBooleanEditor ctuluCellBooleanEditor = new CtuluCellBooleanEditor();
    c.setCellRenderer(ctuluCellBooleanRenderer);
    c.setCellEditor(ctuluCellBooleanEditor);
    c = t.getColumnModel().getColumn(2);
    c.setCellRenderer(ctuluCellBooleanRenderer);
    c.setCellEditor(ctuluCellBooleanEditor);
    final BuScrollPane sc = new BuScrollPane(t);
    sc.setPreferredWidth(40);
    sc.setPreferredHeight(50);
    firstPn.add(sc);
    super.addTab(EbliLib.getS("Principal"), firstPn);
    final BuPanel secPn = new BuPanel();
    secPn.setLayout(new BuGridLayout(2, 5, 7, true, false, false, false, false));
    secPn.add(new BuLabel(EbliLib.getS("Couleur")));
    btColor_ = new JButton();
    icBackground_ = new TraceIcon(TraceIcon.CARRE_PLEIN, 5);
    icBord_ = new TraceIcon(TraceIcon.CARRE_PLEIN, 5);
    this.cbBorder_ = new LegendLineComboboxModel();
    if (l_.getNbPanelLegende() > 0) {
      final BCalqueLegendePanel pleg = l_.getLegendePanel(0);
      icBackground_.setCouleur(pleg.getBackground());
      icBord_.setCouleur(pleg.getBordColor());
      cbBorder_.setSelectedEpaisseur(pleg.getBordEpaisseur());
      cbBorder_.setColor(pleg.getBordColor());
    }
    btColor_.setIcon(icBackground_);
    btColor_.addActionListener(this);
    secPn.add(btColor_);
    secPn.add(new BuLabel(EbliLib.getS("Bord")));
    final BuComboBox cb = new BuComboBox();
    cb.setModel(cbBorder_);
    cb.addItemListener(this);
    cb.setRenderer(new TraceTraitRenderer());
    secPn.add(cb);
    secPn.add(new BuLabel(EbliLib.getS("Couleur du bord")));
    btColorBord_ = new JButton();
    btColorBord_.setIcon(icBord_);
    btColorBord_.addActionListener(this);
    secPn.add(btColorBord_);
    secPn.add(new BuLabel(EbliLib.getS("Police")));
    btFont_ = new BuButton(EbliLib.getS("Modifier"));
    btFont_.addActionListener(this);
    secPn.add(btFont_);

    super.addTab(EbliLib.getS("Affichage"), secPn);
    /*
     * BuComboBox cb=new BuComboBox(); EbliCellTextRenderer r=new EbliCellTextRenderer(); r.setDecorator(new
     * EbliCellDecorator(){
     *//**
     */
    /*
     * public void decore(JComponent _c,JTable _table,Object _value,int _row,int _col){}
     *//**
     */
    /*
     * public void decore(JComponent _c,JList _table,Object _value,int _index){ EbliCellTextRenderer
     * c=(EbliCellTextRenderer)_c; if(_value==null)c.setText(CtuluLibString.EMPTY_STRING); else
     * c.setText(((BCalqueAffichage)_value).getTitle()); } }); cb.setRenderer(r); cb.setModel(m);
     */

  }

  private int getSelectedPos() {
    if (basGauche_.isSelected()) {
      return BSelecteurAncre.BOT_LEFT;
    }
    if (basDroit_.isSelected()) {
      return BSelecteurAncre.BOT_RIGHT;
    }
    if (hautGauche_.isSelected()) {
      return BSelecteurAncre.UP_LEFT;
    }
    if (hautDroit_.isSelected()) {
      return BSelecteurAncre.UP_RIGHT;
    }
    return BSelecteurAncre.BOT_LEFT;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Object s = _e.getSource();
    if (s == btFont_) {
      final JDialog d = new JDialog(CtuluLibSwing.getFrameAncestorHelper(this));
      final BSelecteurReduitFonteNewVersion ft = new BSelecteurReduitFonteNewVersion(l_.getFont(), d);
      ft.setTarget(l_);
      d.setContentPane(ft.getComponent());
      d.setModal(true);
      d.pack();
      d.setLocationRelativeTo(this);
      d.setVisible(true);
    } else if (s == btColor_) {
      final Color c = JColorChooser.showDialog(this, EbliLib.getS("Couleur de fond"), null);
      if (c != null) {
        l_.setBackgroundForLegend(c);
        icBackground_.setCouleur(c);

      }
    } else if (s == btColorBord_) {
      final Color c = JColorChooser.showDialog(this, EbliLib.getS("Couleur des bords"), null);
      if (c != null) {
        l_.setBordColor(c);
        icBord_.setCouleur(c);
        cbBorder_.setColor(c);

      }
    } else if ((s == hautDroit_) || (s == hautGauche_) || (s == basDroit_) || (s == basGauche_)) {
      l_.setAncre(getSelectedPos());
    }
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    if (_e.getSource() == cbVisible_) {
      l_.setVisible(cbVisible_.isSelected());
    } else if (_e.getSource() == cbSameHeight_) {
      l_.setSameHeightLegend(cbSameHeight_.isSelected());
    }
  }

}
