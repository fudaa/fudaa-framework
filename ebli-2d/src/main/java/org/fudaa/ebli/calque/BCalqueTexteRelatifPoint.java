/*
 * @file         BCalqueTexteRelatifPoint.java
 * @creation     1999-05-10
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Hashtable;
import java.util.StringTokenizer;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPositionRelativePoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPositionRelativePoint;
import org.fudaa.ebli.trace.TracePoint;

/**
 * Un calque d'affichage de textes relatifs.
 *
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:55:47 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BCalqueTexteRelatifPoint extends BCalqueTexte {
  VecteurGrPositionRelativePoint pointsRel_;

  public BCalqueTexteRelatifPoint() {
    super();
    reinitialise();
  }

  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    boolean rapide = isRapide();
    final boolean attenue = isAttenue();
    final boolean pleine = isEtiquettePleine();
    Color fg = getForeground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = _g.getFontMetrics();
    TracePoint trace = null;
    if (!rapide) {
      trace = new TracePoint();
      trace.setTypePoint(typePoint_);
      trace.setCouleur(fg);
    }
    final GrMorphisme versEcran = getVersEcran();
    for (int i = 0; i < pointsRel_.nombre(); i++) {
      final GrPositionRelativePoint pr = pointsRel_.renvoie(i);
      final GrPositionRelativePoint pe = pr.applique(versEcran);
      final int x = (int) pe.x();
      final int y = (int) pe.y();
      if (rapide) {
        _g.drawLine(x + 5, y, x + 55, y);
      } else if(trace!=null){
        trace.dessinePoint((Graphics2D) _g, x, y);
        final Object o = textes_.get(pr);
        if (o != null) {
          final StringTokenizer st = new StringTokenizer((String) o, "\n");
          final String[] s = new String[st.countTokens()];
          final int hs = fm.getAscent() + fm.getDescent();
          int ws = 0;
          for (int c = 0; c < s.length; c++) {
            s[c] = st.nextToken();
            ws = Math.max(ws, fm.stringWidth(s[c]));
          }
          final int hst = hs * s.length;
          if (pleine) {
            if (surPosition_) {
              _g.setColor(bg);
              _g.fillRect(x - ws / 2 - 2, y - hst / 2 - 2, ws + 4, hst + 4);
              _g.setColor(fg);
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x - ws / 2, y + hs * c - hst / 2 + fm.getAscent());
              }
              _g.drawRect(x - ws / 2 - 2, y - hst / 2 - 2, ws + 4, hst + 4);
            } else {
              _g.setColor(bg);
              _g.fillRect(x + 5, y - hst / 2 - 2, ws + 4, hst + 4);
              _g.setColor(fg);
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x + 7, y + hs * c - hst / 2 + fm.getAscent());
              }
              _g.drawRect(x + 5, y - hst / 2 - 2, ws + 4, hst + 4);
            }
          } else {
            if (surPosition_) {
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x - ws / 2, y + hs * c - hst / 2 + fm.getAscent());
              }
            } else {
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x + 5, y + hs * c);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Reinitialise la liste des textes de ce calque.
   */
  @Override
  public final void reinitialise() {
    super.reinitialise();
    pointsRel_ = new VecteurGrPositionRelativePoint();
  }

  /**
   * Ajoute un texte a ce calque.
   *
   * @param _point position
   * @param _texte texte
   */
  public void ajoute(final GrPositionRelativePoint _point, final String _texte) {
    pointsRel_.ajoute(_point);
    textes_.put(_point, _texte);
  }

  public VecteurGrPositionRelativePoint getPositionsRelatives() {
    return pointsRel_;
  }

  public void setPositionsRelatives(final VecteurGrPositionRelativePoint _lp) {
    if (_lp == pointsRel_) {
      return;
    }
    final VecteurGrPositionRelativePoint vp = pointsRel_;
    pointsRel_ = new VecteurGrPositionRelativePoint();
    for (int i = 0; i < _lp.nombre(); i++) {
      pointsRel_.ajoute(_lp.renvoie(i));
    }
    firePropertyChange("positionsRelatives", vp, pointsRel_);
  }

  public String getTexte(final GrPositionRelativePoint _p) {
    return (String) textes_.get(_p);
  }

  @Override
  public String[] getTextes() {
    final String[] superres = super.getTextes();
    final String[] res = new String[superres.length + pointsRel_.nombre()];
    for (int i = 0; i < superres.length; i++) {
      res[i] = superres[i];
    }
    for (int i = 0; i < pointsRel_.nombre(); i++) {
      res[superres.length + i] = getTexte(pointsRel_.renvoie(i));
    }
    return res;
  }

  @Override
  public void setTextes(final String[] _t) {
    textes_ = new Hashtable();
    final String[] vp = getTextes();
    for (int i = 0; i < Math.min(points_.nombre(), _t.length); i++) {
      textes_.put(points_.renvoie(i), _t[i]);
    }
    for (int i = 0; i < Math.min(pointsRel_.nombre(), _t.length); i++) {
      textes_.put(pointsRel_.renvoie(i), _t[i + points_.nombre()]);
    }
    firePropertyChange("textes", vp, getTextes());
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible()) {
      r = super.getDomaine();
      if (pointsRel_.nombre() > 0) {
        if (r == null) {
          r = new GrBoite();
        }
        for (int i = 0; i < pointsRel_.nombre(); i++) {
          r.ajuste(pointsRel_.renvoie(i).toGrPoint());
        }
      }
    }
    return r;
  }

  /**
   * Renvoi de la liste des elements selectionnables.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res = super.contours();
    res.tableau(pointsRel_.tableau());
    return res;
  }
}
