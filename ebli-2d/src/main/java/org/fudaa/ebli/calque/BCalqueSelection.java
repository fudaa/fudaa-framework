/*
 * @file         BCalqueSelection.java
 * @creation     1999-03-02
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.trace.TracePoint;
/**
 * Un calque de d'affichage des poign�es de s�lection d'objets GrContour
 * (objets s�lectionnables).
 *
 * @version      $Id: BCalqueSelection.java,v 1.11 2006-09-19 14:55:46 deniger Exp $
 * @author       Bertrand Marchand
 */
public class BCalqueSelection
  extends BCalqueAffichage
  implements SelectionListener {
  int typePoignee_;
  int taillePoignee_;
  Color couleurPoignee_;
  //  BCalqueSelectionInteraction calque_;
  private VecteurGrContour objectsSelect_;
  private VecteurGrContour objetsTmp_;
  /**
   * Cr�ation d'un calque d'affichage de selections.
   */
  public BCalqueSelection() {
    super();
    setDestructible(false);
    typePoignee_= TracePoint.CARRE;
    taillePoignee_= 4;
    //    couleurPoignee_=Color.blue;
    setForeground(Color.blue);
    //    objectsSelect_ =new VecteurGrContour();
    //    objetsTmp_     =new VecteurGrContour();
  }
  /**
   * Cr�ation du calque correspondant � un calque de s�lection.
   */
  public BCalqueSelection(final BCalqueSelectionInteraction _calque) {
    this();
    _calque.addSelectionListener(this);
  }
  /**
   * Ajout d'un calque de selection interaction � repr�senter.
   */
  public void addSelection(final BCalqueSelectionInteraction _calque) {
    _calque.addSelectionListener(this);
  }
  /**
   * Retrait d'un calque de selection interaction � repr�senter.
   */
  public void removeSelection(final BCalqueSelectionInteraction _calque) {
    _calque.removeSelectionListener(this);
  }
  /**
    * Dessin de l'icone.
    * @param _c composant dont l'icone peut deriver des proprietes.
    *           Ce parametre peut etre <I>null</I>. Il est ignore ici.
    * @param _g le graphics sur lequel dessiner l'icone
    * @param _x lieu cible de l'icone (x)
    * @param _y lieu cible de l'icone (y)
    */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color c1= Color.black;
    //    Color c2=couleurPoignee_;
    Color c2= getForeground();
    if (isAttenue()) {
      c1= attenueCouleur(c1);
    }
    if (isAttenue()) {
      c2= attenueCouleur(c2);
    }
    final  int w= getIconWidth();
    final int h= getIconHeight();
    _g.setColor(c1);
    _g.drawLine(_x + 6, _y + 7, _x + w - 6, _y + h - 5);
    final TracePoint trace= new TracePoint();
    trace.setCouleur(c2);
    trace.setTypePoint(typePoignee_);
    trace.setTaillePoint(3);
    trace.dessinePoint((Graphics2D)_g,_x + 7, _y + 8);
    trace.dessinePoint((Graphics2D)_g,_x + w - 6, _y + h - 5);
  }
  /**
   * R�cup�ration des objets s�lectionn�s.
   */
  @Override
  public void selectedObjects(final SelectionEvent _evt) {
    //Fred il y a une erreur ici: boucle infinie
//    if (_evt instanceof SelectionEventModele) {
//      selectedObjects((SelectionEventModele)_evt);
//      return;
//    }
    final  VecteurGrContour objets= _evt.getObjects();
    if (objetsTmp_ == null) {
      objetsTmp_= new VecteurGrContour();
    }
    for (int i= 0; i < objets.nombre(); i++){
      objetsTmp_.ajoute(objets.renvoie(i));
    }
    // On n'attend plus d'autre �v�nement
    if (!_evt.hasNext()) {
      /* objectsSelect_=objetsTmp_;
      objetsTmp_=new VecteurGrContour();
      quick_repaint(); */
      selectedObjectsTermines();
    }
  }
  private void selectedObjectsTermines() {
    objectsSelect_= objetsTmp_;
    objetsTmp_= null;
    quickRepaint();
  }
  /**
   * Trac� des poign�es de s�lection.
   */
  @Override
  public void paintComponent(final Graphics _g) {
    if (!isRapide()) {
      //      GrPoint[]   poignees;
      //      GrPoint     poigneeEcran;
      final TracePoint trace= new TracePoint();
      Color c;
      final  GrMorphisme versEcran= getVersEcran();
      //      c=couleurPoignee_;
      c= getForeground();
      if (isAttenue()) {
        c= attenueCouleur(c);
      }
      trace.setCouleur(c);
      trace.setTypePoint(typePoignee_);
      trace.setTaillePoint(taillePoignee_);
      if (objectsSelect_ != null) {
        for (int i= 0; i < objectsSelect_.nombre(); i++) {
          dessineContour((Graphics2D)_g,objectsSelect_.renvoie(i), trace, versEcran);
        }
      }
      super.paintComponent(_g);
    }
  }
  private void dessineContour(
      final Graphics2D _g2d,
    final GrContour _c,
    final TracePoint _t,
    final GrMorphisme _versEcran) {
    final GrPoint[] poignees= _c.contour();
    for (int j= 0; j < poignees.length; j++) {
      final GrPoint poigneeEcran= poignees[j].applique(_versEcran);
      _t.dessinePoint(_g2d,(int)poigneeEcran.x_, (int)poigneeEcran.y_);
    }
  }
}
