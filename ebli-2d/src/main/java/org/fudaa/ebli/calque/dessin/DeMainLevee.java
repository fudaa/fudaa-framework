/*
 * @file         DeMainLevee.java
 * @creation     1998-08-31
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
/**
 * Une courbe a main levee.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class DeMainLevee extends DeLigneBrisee {
  // donnees membres publiques
  // donnees membres privees
  // Constructeur
  public DeMainLevee(final DeLigneBrisee _l) {
    super(_l.ligne_);
  }
  @Override
  public int getForme() {
    return MAIN_LEVEE;
  }
  // Methodes publiques
}
