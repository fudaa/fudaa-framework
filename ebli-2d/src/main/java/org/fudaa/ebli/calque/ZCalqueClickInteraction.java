/**
 * @creation 5 oct. 2004
 * @modification $Date: 2008-03-27 15:26:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZCalqueClickInteraction extends BCalqueInteraction implements MouseListener,
    MouseMotionListener, ZCatchListener {

  ZCalqueClikInteractionListener listener_;
  /** Le point d'accrochage, ou null si pas de point d'accrochage */
  GrPoint ptAccroch_=null;
  /** Le point intermédiaire */
  GrPoint ptTmp_=new GrPoint();

  public ZCalqueClickInteraction() {
    setName("cqInteractClicked");
  }
  
  @Override
  public Cursor getSpecificCursor() {
    return new Cursor(Cursor.CROSSHAIR_CURSOR);
  }

  String desc_;

  @Override
  public String getDescription(){
    return desc_;
  }

  /**
   * @param _s la nouvelle description du calque
   */
  public void setDescription(final String _s){
    desc_ = _s;
  }

  public final ZCalqueClikInteractionListener getListener(){
    return listener_;
  }

  @Override
  public void mouseClicked(final MouseEvent _e){
    if (isOkLeftEvent(_e) && (listener_ != null)) {
      if (ptAccroch_==null) {
        ptTmp_.setCoordonnees(_e.getX(), _e.getY(),0);
        ptTmp_.autoApplique(getVersReel());
      }
      else {
        ptTmp_.setCoordonnees(ptAccroch_.x_, ptAccroch_.y_, ptAccroch_.z_);
      }
      listener_.pointClicked(ptTmp_);
    }
  }

  @Override
  public void mouseDragged(final MouseEvent _e){}

  @Override
  public void mouseEntered(final MouseEvent _e){}

  @Override
  public void mouseExited(final MouseEvent _e){}

  @Override
  public void mouseMoved(final MouseEvent _e){}

  @Override
  public void mousePressed(final MouseEvent _e){}

  @Override
  public void mouseReleased(final MouseEvent _e){}

  public final void setListener(final ZCalqueClikInteractionListener _listener){
    listener_ = _listener;
  }

  @Override
  public void catchChanged(ZCatchEvent _evt) {
    if (_evt.type==ZCatchEvent.UNCAUGHT)
      ptAccroch_=null;
    else
      ptAccroch_=_evt.selection.getScene().getVertex(_evt.idxGeom, _evt.idxVertex);
  }

  @Override
  public boolean isCachingEnabled() {
    return true;
  }
}