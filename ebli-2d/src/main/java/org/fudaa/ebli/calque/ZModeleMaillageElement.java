/*
 *  @creation     2002-06-13
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;

/**
 * Le modele du calque d'affichage de maillage.
 *
 * @version $Id: ZModeleMaillageElement.java,v 1.6 2006-04-12 15:27:09 deniger Exp $
 * @author Frederic Deniger
 */
public interface ZModeleMaillageElement {
  /**
   * @return Le nombre de polygone du modele
   */
  int getNombrePolygones();

  /**
   * @return Le nombre de points
   */
  int getNombreNoeuds();

  boolean polygone(int _i, GrPolygone _poly);

  /**
   * @param i le numero du noeud voulu
   * @return le noeud i du maillage
   */
  boolean point(int _i, GrPoint _p);

  /**
   * Le domaine du maillage.
   */
  GrBoite getDomaine();

  /**
   * @return Le nombre d'elements composants le contours
   */
  int getNombreBord();

  /**
   * @param i le numero du bord
   * @return Le nombre de points support pour l'arete i
   */
  int getNombrePointPourBord(int _i);

  boolean getBordPoint(int _indexBord, int _indexPoint, GrPoint _p);
}
