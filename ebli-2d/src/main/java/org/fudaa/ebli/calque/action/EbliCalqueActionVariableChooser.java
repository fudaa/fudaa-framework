/**
 * @creation 12 juil. 2004
 * @modification $Date: 2007-03-09 08:38:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.controle.BSelecteurList;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EbliCalqueActionVariableChooser.java,v 1.1 2007-03-09 08:38:20 deniger Exp $
 */
public class EbliCalqueActionVariableChooser extends EbliActionPaletteTreeModel {

  /**
   * Initialise l'action.
   */
  public EbliCalqueActionVariableChooser(final BArbreCalqueModel _c) {
    super(CtuluLib.getS("Variables"), EbliResource.EBLI.getToolIcon("select-var"),
        "DISPLAY_ISO", _c.getTreeSelectionModel());
    setDefaultToolTip(CtuluLib.getS("S�lectionner la variable � afficher"));
    setResizable(true);
  }

  @Override
  protected boolean isTargetValid(final Object _o){
    return BSelecteurList.isTargetValid(_o);
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent(){
    return new BSelecteurList();
  }
  
  @Override
  public void updateBeforeShow(){
    setPaletteTarget(target_);
  }

}