/**
 * @creation 19 oct. 2004
 * @modification $Date: 2006-10-26 07:33:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.commun.EbliTableInfoTarget;

/**
 * @author Fred Deniger
 * @version $Id: CalqueActionTable.java,v 1.6 2006-10-26 07:33:06 deniger Exp $
 */
public class CalqueActionTable extends EbliActionSimple implements TreeSelectionListener {

  BArbreCalqueModel model_;
  final CtuluUI ui_;
  ZEbliCalquesPanel pnCalques_;

  public CalqueActionTable(final BArbreCalqueModel _model,final CtuluUI _ui, ZEbliCalquesPanel _pnCalques) {
    super(EbliLib.getS("Tableau des valeurs"), BuResource.BU.getToolIcon("tableau"), "TABLE");
    model_ = _model;
    ui_ = _ui;
    pnCalques_=_pnCalques;
    model_.addTreeSelectionListener(this);
    valueChanged(null);
  }

  @Override
  public void actionPerformed(final ActionEvent _e){
    final EbliTableInfoPanel t = new EbliTableInfoPanel(ui_, (EbliTableInfoTarget) model_
        .getSelectedCalque(),pnCalques_.getCoordinateDefinitions());
    t.showInDialog();
  }

  @Override
  public final void valueChanged(final TreeSelectionEvent _e){
    final BCalque cq = model_.getSelectedCalque();
    setEnabled((cq instanceof EbliTableInfoTarget) && ((EbliTableInfoTarget) cq).isValuesTableAvailable());
  }
  
  public BArbreCalqueModel getModel() {
	  return model_;
  }
  
  public CtuluUI getUi() {
	  return ui_;
  }
  
  public ZEbliCalquesPanel getCalque(){
	  return pnCalques_;
  }
  
  
}