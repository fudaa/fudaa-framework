/*
 * cache pour l'affichage des donn�es
 */
package org.fudaa.ebli.calque;

import java.awt.Graphics;

/**
 * @author deniger
 */
public class BCalqueCacheManager extends AbstractBCalqueCacheManager {

  /**
   * @param calque
   */
  public BCalqueCacheManager(final ZCalqueAffichageDonneesAbstract calque) {
    super(calque);
  }

  public static void installDefaultCacheManager(final ZCalqueAffichageDonneesAbstract calque) {
    final BCalqueCacheManager cacheManager = new BCalqueCacheManager(calque);
    calque.addPropertyChangeListener(new CacheManagerPropertyChangeListener(cacheManager));
    calque.setCacheManager(cacheManager);
  }
  
  public void paintDonnees(final Graphics graphicsOfComponent) {
    if (!isCacheUptodate()) {
      layer.paintDonnees(getImageGraphics());
    }
    graphicsOfComponent.drawImage(image, 0, 0, layer);
  }

}
