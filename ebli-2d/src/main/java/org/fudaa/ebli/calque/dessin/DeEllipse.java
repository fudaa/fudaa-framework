/*
 * @file         DeEllipse.java
 * @creation     1998-08-31
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;

import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceSurface;
/**
 * Une ellipse.
 *
 * @version      $Id: DeEllipse.java,v 1.15 2006-09-19 14:55:53 deniger Exp $
 * @author       Axel von Arnim
 */
public class DeEllipse extends DeForme {

  // donnees membres publiques
  public final static double C_MAGIC = Math.PI / 4.;
  // donnees membres privees
  GrPoint[] pointsCtrl_;

  // Constructeur
  public DeEllipse(final GrPoint[] _pc) {
    super();
    pointsCtrl_ = new GrPoint[4];
    for (int i = 0; i < 4; i++) {
      pointsCtrl_[i] = _pc[i];
    }

  }

  public DeEllipse() {
    super();
    pointsCtrl_ = new GrPoint[4];
  }

  @Override
  public int getForme(){
    return ELLIPSE;
  }

  // Methodes publiques
  @Override
  public GrObjet getGeometrie(){
    final GrPolygone res = new GrPolygone();
    for (int i = 0; i < pointsCtrl_.length; i++){
      res.sommets_.ajoute(pointsCtrl_[i]);
    }
    return res;
  }

  public void ajoute(final GrPoint _p){
    int i = 0;
    while ((i < 4) && (pointsCtrl_[i] != null)){
      i++;
    }
    if (i < 4) {
      pointsCtrl_[i++] = _p;
    }

  }

  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t,final boolean _rapide){
    if (pointsCtrl_ != null) {
      super.affiche(_g2d, _t, _rapide);
      _t.dessineEllipse(_g2d, pointsCtrl_[0], pointsCtrl_[1], pointsCtrl_[2], pointsCtrl_[3],
          ((couleurRemplissage_ != null) || (typeSurface_ != TraceSurface.UNIFORME)), _rapide);
    }
  }

  // >>> Interface GrContour ---------------------------------------------------
  /**
   * Retourne les points du contour de l'objet.
   *
   * @return Les points de contour, au nombre de 16.
   * @see    org.fudaa.ebli.geometrie.GrContour
   */
  @Override
  public GrPoint[] contour(){
    final GrPoint[] r = new GrPoint[16];
    final GrPolyligne pl = polygone().toGrPolyligne();
    pl.sommets_.ajoute(pl.sommet(pl.nombre() - 1));
    for (int i = 0; i < r.length; i++) {
      r[i] = pl.pointDAbscisse(i / 16.);
    }
    return r;
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran,final double _dist,final Point _pt){
    // Ellipse "pleine"
    if (couleurRemplissage_ != null || typeSurface_ != TraceSurface.UNIFORME) {
      return polygone().estSelectionne(_ecran, _dist, _pt);
    }
    // Ellipse "vide"
    return polygone().toGrPolyligne().estSelectionne(_ecran, _dist, _pt);
  }

  // Polygone correspondant � l'ellipse
  private GrPolygone polygone(){
    final GrPolygone r = new GrPolygone();
    final double x1 = pointsCtrl_[0].x_;
    final double y1 = pointsCtrl_[0].y_;
    final double x2 = pointsCtrl_[1].x_;
    final double y2 = pointsCtrl_[1].y_;
    final double x3 = pointsCtrl_[2].x_;
    final double y3 = pointsCtrl_[2].y_;
    final double x4 = pointsCtrl_[3].x_;
    final double y4 = pointsCtrl_[3].y_;
    int i;
    final GrPolygone arc1 = arc2polygon((x1 + x2) / 2, (y1 + y2) / 2, (x2 + x3) / 2, (y2 + y3) / 2,
        (x2 - x1) * C_MAGIC, (y2 - y1) * C_MAGIC, (x3 - x2) * C_MAGIC, (y3 - y2) * C_MAGIC);
    final GrPolygone arc2 = arc2polygon((x2 + x3) / 2, (y2 + y3) / 2, (x3 + x4) / 2, (y3 + y4) / 2,
        (x3 - x2) * C_MAGIC, (y3 - y2) * C_MAGIC, (x4 - x3) * C_MAGIC, (y4 - y3) * C_MAGIC);
    final GrPolygone arc3 = arc2polygon((x3 + x4) / 2, (y3 + y4) / 2, (x4 + x1) / 2, (y4 + y1) / 2,
        (x4 - x3) * C_MAGIC, (y4 - y3) * C_MAGIC, (x1 - x4) * C_MAGIC, (y1 - y4) * C_MAGIC);
    final GrPolygone arc4 = arc2polygon((x4 + x1) / 2, (y4 + y1) / 2, (x1 + x2) / 2, (y1 + y2) / 2,
        (x1 - x4) * C_MAGIC, (y1 - y4) * C_MAGIC, (x2 - x1) * C_MAGIC, (y2 - y1) * C_MAGIC);
    for (i = 0; i < arc1.nombre(); i++) {
      r.sommets_.ajoute(arc1.sommet(i));
    }
    for (i = 0; i < arc2.nombre(); i++) {
      r.sommets_.ajoute(arc2.sommet(i));
    }
    for (i = 0; i < arc3.nombre(); i++) {
      r.sommets_.ajoute(arc3.sommet(i));
    }
    for (i = 0; i < arc4.nombre(); i++) {
      r.sommets_.ajoute(arc4.sommet(i));
    }
    return r;
  }

  private GrPolygone arc2polygon(final double _x1,final double _y1,final double _x2,final double _y2,final double _vx1,final double _vy1,
    final double _vx2,final double _vy2){
    final GrPolygone r = new GrPolygone();
    final int nb = (int) (Math.abs(_x2 - _x1) + Math.abs(_y2 - _y1)) / 2;
    r.sommets_.ajoute(new GrPoint(_x1, _y1, 0));
    for (int i = 1; i < nb + 1; i++) {
      r.sommets_.ajoute(MatriceHermite.hermite((double) i / nb, _x1, _y1, _x2, _y2, _vx1, _vy1, _vx2, _vy2));
    }
    return r;
  }

  // <<< Interface GrContour ---------------------------------------------------
  // >>> Implementation GrObjet pour �tre d�placable ---------------------------
  @Override
  public void autoApplique(final GrMorphisme _t){
    for (int i = 0; i < pointsCtrl_.length; i++) {
      pointsCtrl_[i].autoApplique(_t);
    }
  }

  /**
   * Retourne la boite englobante de l'objet.
   */
  @Override
  public GrBoite boite(){
    return polygone().boite();
  }
  // <<< Implementation GrObjet ------------------------------------------------
  // Methodes privees
  //  private boolean verifieLosange()
  //  {
  //    double p=points_ctrl_[2].soustraction(points_ctrl_[0]).normalise().produitScalaire(
  //             points_ctrl_[3].soustraction(points_ctrl_[1]).normalise());
  //    return (Math.abs(p)<0.1);
  //  }
}

/**
 * Mini class matrice pour l'interpolation de Hermite. (Repris int�gralement
 * de la classe org.fudaa.ebli.trace.MatriceHermite.
 */
class MatriceHermite {

  public final static MatriceHermite HERMITE = new MatriceHermite(new double[][] { { 2, -2, 1, 1},
      { -3, 3, -2, -1}, { 0, 0, 1, 0}, { 1, 0, 0, 0}});
  private final double[][] a_;

  public MatriceHermite(final int _ni, final int _nj) {
    a_ = new double[_ni][_nj];
  }

  public MatriceHermite(final double[][] _a) {
    a_ = _a;
  }

  public double a(final int _i,final int _j){
    return a_[_i][_j];
  }

  public void a(final int _i,final int _j,final double _ia){
    a_[_i][_j] = _ia;
  }

  public MatriceHermite multiplication(final MatriceHermite _m){
    final int ni = a_.length;
    final int nj = _m.a_[0].length;
    final MatriceHermite r = new MatriceHermite(ni, nj);
    for (int i = 0; i < ni; i++) {
      for (int j = 0; j < nj; j++) {
        for (int k = 0; k < a_[0].length; k++) {
          r.a_[i][j] += a_[i][k] * _m.a_[k][j];
        }
      }
    }
    return r;
  }

  /**
   * Interpolation de Hermite.
   */
  public static GrPoint hermite(final double _t,final double _x1,final double _y1,final double _x2,final double _y2,final double _vx1,
    final double _vy1,final double _vx2,final double _vy2){
    final MatriceHermite h = MatriceHermite.HERMITE;
    final MatriceHermite t = new MatriceHermite(new double[][] { { _t * _t * _t, _t * _t, _t, 1}});
    final MatriceHermite x = new MatriceHermite(new double[][] { { _x1, _y1, 0, 1}, { _x2, _y2, 0, 1},
        { _vx1, _vy1, 0, 0}, { _vx2, _vy2, 0, 0}});
    final MatriceHermite q = t.multiplication(h).multiplication(x);
    return new GrPoint(q.a(0, 0), q.a(0, 1), 0);
  }
}
