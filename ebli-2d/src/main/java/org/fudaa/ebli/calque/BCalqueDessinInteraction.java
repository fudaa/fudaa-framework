/*
 * @file         BCalqueDessinInteraction.java
 * @creation     1998-09-02
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceSurface;

/**
 * Un calque d'interaction pour <I>BCalqueDessin</I>.
 * 
 * @version $Id: BCalqueDessinInteraction.java,v 1.10 2006-09-19 14:55:48 deniger Exp $
 * @author Axel von Arnim
 */
public class BCalqueDessinInteraction extends BCalqueFormeInteraction implements FormeEventListener {
  private Color couleurContourCourante_;
  private Color couleurRemplissageCourante_;
  private int typeTraitCourant_;
  private int typeSurfaceCourant_;

  // Constructeurs
  /**
   * Constructeur.
   * 
   * @param _calque calque dessin associe
   */
  public BCalqueDessinInteraction(final BCalqueDessin _calque) {
    super(_calque);
    typeTraitCourant_ = TraceLigne.LISSE;
    typeSurfaceCourant_ = TraceSurface.UNIFORME;
    couleurContourCourante_ = Color.black;
    couleurRemplissageCourante_ = null;
    setBackground(null);
    addFormeEventListener(this);
  }

  // Methodes publiques
  /**
   * Affectation de la propriete <I>typeTrait</I>.
   */
  @Override
  public void setTypeTrait(final int _typeTrait) {
    typeTraitCourant_ = _typeTrait;
  }

  /**
   * Accesseur de la propriete <I>typeTrait</I>. Elle fixe le type de trait (pointille, tirete, ...) en prenant ses
   * valeurs dans les champs statiques de <I>TraceLigne</I>.
   * 
   * @see TraceLigne
   */
  @Override
  public int getTypeTrait() {
    return typeTraitCourant_;
  }

  /**
   * Affectation de la propriete <I>typeSurface</I>.
   */
  public void setTypeSurface(final int _typeSurface) {
    typeSurfaceCourant_ = _typeSurface;
  }

  /**
   * Accesseur de la propriete <I>typeSurface</I>. Elle fixe le type de remplissage des formes (hachure, ...) en
   * prenant ses valeurs dans les champs statiques de <I>TraceSurface</I>.
   * 
   * @see org.fudaa.ebli.trace.TraceSurface
   */
  public int getTypeSurface() {
    return typeSurfaceCourant_;
  }

  /**
   * Affectation de la propriete <I>couleurContour</I>.
   */
  @Override
  public void setForeground(final Color _c) {
    couleurContourCourante_ = _c;
  }

  /**
   * Accesseur de la propriete <I>couleurContour</I>. (couleur du contour de la prochaine forme cree.
   */
  @Override
  public Color getForeground() {
    return couleurContourCourante_;
  }

  /**
   * Affectation de la propriete <I>couleurRemplissage</I>.
   */
  @Override
  public final void setBackground(final Color _c) {
    couleurRemplissageCourante_ = _c;
  }

  /**
   * Accesseur de la propriete <I>couleurRemplisssage</I>. (couleur de remplissage de la prochaine forme cree.
   */
  @Override
  public Color getBackground() {
    return couleurRemplissageCourante_;
  }

  // EVENEMENTS
  @Override
  public void formeSaisie(final FormeEvent _e) {
    final DeForme forme = _e.getForme();
    forme.setForeground(getForeground());
    forme.setBackground(getBackground());
    forme.setTypeTrait(typeTraitCourant_);
    forme.setTypeSurface(typeSurfaceCourant_);
    ((BCalqueDessin) calque_).ajoute(forme);
    ((BCalqueDessin) calque_).repaint();
  }
}
