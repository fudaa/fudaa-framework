/**
 * @creation 4 oct. 2004
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Fred Deniger
 * @version $Id: ZCalqueGrilleReguliere.java,v 1.14 2006-09-19 14:55:47 deniger Exp $
 */
public class ZCalqueGrilleReguliere extends ZCalquePoint {

  private final TraceLigne ligne_;

  boolean paintGrid_ = true;

  public ZCalqueGrilleReguliere(final ZModeleGrilleReguliere _r) {
    super(_r);
    ligne_ = new TraceLigne();
    ligne_.setTypeTrait(TraceLigne.TIRETE);
    setForeground(Color.LIGHT_GRAY);
  }

  /**
   * @param _modele le nouveau modele
   */
  public void modele(final ZModeleGrilleReguliere _modele) {
    modele_ = _modele;
  }

  /**
   * Attention: un modele de type GrilleReguliere doit etre utilise.
   */
  @Override
  public void setModele(final ZModelePoint _modele) {
    if (_modele instanceof ZModeleGrilleReguliere) {
      modele_ = _modele;
    }
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    if (!_clipReel.intersectXY(modele_.getDomaine())) {
      return;
    }
    final ZModeleGrilleReguliere grille = (ZModeleGrilleReguliere) modele_;
    if (!paintGrid_) {
      return;
    }
    final GrPoint p = new GrPoint();
    Color foreground = getForeground();
    if (isAttenue()) {
      foreground = attenueCouleur(foreground);
    }
    // la boite du polygone
    // sauvegarde pour restaurer a la fin
    final TraceLigneModel dinit = new TraceLigneModel(ligne_.getModel());
    ligne_.setCouleur(foreground);
    if (modele_.getNombre() == 1) {
      modele_.point(p, 0, true);
      if (_clipReel.contientXY(p)) {
        p.autoApplique(_versEcran);
        double x1 = p.x_ * 0.9;
        double y1 = p.y_ * 0.9;
        double x2 = p.x_ * 1.1;

        double y2 = p.y_ * 1.1;
        if (x1 == x2) {
          x1 -= 5;
          x2 += 5;
        }
        if (y1 == y2) {
          y1 -= 5;
          y2 += 5;
        }
        ligne_.dessineTrait(_g, p.x_, y1, p.x_, y2);
        ligne_.dessineTrait(_g, x1, p.y_, x2, p.y_);
      }
    } else {

      final double dx = grille.getDX();
      final double dy = grille.getDY();
      final double yno = grille.getYMin();
      final double xno = grille.getXMin();
      final double xmax = grille.getXMax();
      final double ymax = grille.getYMax();
      final GrPoint p2 = new GrPoint();
      final GrMorphisme versEcran = _versEcran;
      // on dessine les horizontales
      for (int i = grille.getNbPtOnY() - 1; i >= 0; i--) {
        // le y
        p.y_ = yno + i * dy;
        // si la ligne est dans la boite de dessin
        if ((p.y_ <= _clipReel.e_.y_) && (p.y_ >= _clipReel.o_.y_)) {
          p.x_ = xno;
          p2.x_ = xmax;
          p2.y_ = p.y_;
          // on ajuste la ligne a la boite de dessin
          // en fait on ne sait que le point max/min (dy peut etre negatif)
          if (p2.x_ > _clipReel.e_.x_) {
            p2.x_ = _clipReel.e_.x_;
          } else if (p2.x_ < _clipReel.o_.x_) {
            p2.x_ = _clipReel.o_.x_;
          }
          if (p.x_ < _clipReel.o_.x_) {
            p.x_ = _clipReel.o_.x_;
          } else if (p.x_ > _clipReel.e_.x_) {
            p.x_ = _clipReel.e_.x_;
          }
          p.autoApplique(versEcran);
          p2.autoApplique(versEcran);
          ligne_.dessineTrait(_g, p.x_, p.y_, p2.x_, p2.y_);
        }
      }
      // on dessine les verticales
      for (int i = grille.getNbPtOnX() - 1; i >= 0; i--) {
        p.x_ = xno + i * dx;
        if ((p.x_ <= _clipReel.e_.x_) && (p.x_ >= _clipReel.o_.x_)) {
          p.y_ = yno;
          p2.x_ = p.x_;
          p2.y_ = ymax;
          if (p.y_ > _clipReel.e_.y_) {
            p.y_ = _clipReel.e_.y_;
          } else if (p.y_ < _clipReel.o_.y_) {
            p.y_ = _clipReel.o_.y_;
          }
          if (p2.y_ < _clipReel.o_.y_) {
            p2.y_ = _clipReel.o_.y_;
          } else if (p2.y_ > _clipReel.e_.y_) {
            p2.y_ = _clipReel.e_.y_;
          }
          p.autoApplique(versEcran);
          p2.autoApplique(versEcran);
          ligne_.dessineTrait(_g, p.x_, p.y_, p2.x_, p2.y_);
        }
      }
    }
    ligne_.setModel(dinit);
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(Color.white);
    _g.fillRect(_x + 1, _y + 1, w - 1, h - 1);
    _g.setColor(getForeground());
    _g.drawRect(_x, _y, w, h);
    if ((modele_ == null) || (modele_.getNombre() == 0)) {
      return;
    }
    _g.setColor(getForeground());

    int x1 = _x + w / 4;
    int y1 = _y;
    final int y2 = _y + h;
    _g.drawLine(x1, y1, x1, y2);
    x1 += w / 4;
    _g.drawLine(x1, y1, x1, y2);
    x1 += w / 4;
    _g.drawLine(x1, y1, x1, y2);
    x1 = _x;
    final int x2 = _x + w;
    y1 = _y + h / 4;
    _g.drawLine(x1, y1, x2, y1);
    y1 += h / 4;
    _g.drawLine(x1, y1, x2, y1);
    y1 += h / 4;
    _g.drawLine(x1, y1, x2, y1);
  }
}