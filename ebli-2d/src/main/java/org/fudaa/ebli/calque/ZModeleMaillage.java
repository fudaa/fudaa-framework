/*
 * @file         ZModelePoint.java
 * @creation     2000-11-10
 * @modification $Date: 2006-04-12 15:27:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
/**
 * Le modele du calque d'affichage de point.
 *
 * @version      $Id: ZModeleMaillage.java,v 1.5 2006-04-12 15:27:10 deniger Exp $
 * @author       Frederic Deniger
 */
public interface ZModeleMaillage extends ZModeleDonnees {
  int nombrePolygones();
  int nombreNoeuds();
  GrPolygone polygone(int _i);
  GrPoint point(int _i);
  int[] connectivites(int _i);
}
