/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import gnu.trove.TIntObjectIterator;
import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ebli.calque.ZCalqueMultiPoint;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Un calque pour les multipoints editables.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZCalqueMultiPointEditable extends ZCalqueMultiPoint implements ZCalqueEditable {

  ZEditorInterface editor_;

  /**
   * Le seul constructeur, avec le modele �ditable.
   *
   * @param _modele
   * @param _editor
   */
  public ZCalqueMultiPointEditable(final ZModeleMultiPointEditable _modele, final ZEditorDefault _editor) {
    super(_modele);
    editor_ = _editor;
  }

  @Override
  public String editSelected() {
    return editor_ == null ? EbliLib.getS("Edition impossible") : editor_.edit();
  }

  //  /**
  //   * Scinde en 2 une polyligne de sommet s�lectionn�. Les attributs sont copi�s.
  //   * @param _cmd La pile de commandes.
  //   * @return true si le la polyligne a �t� scind�e.
  //   */
  //  public boolean splitGeometry(final CtuluCommandContainer _cmd) {
  //    if (isSelectionEmpty() || !isAtomicMode()) return false;
  //
  //    int idxLine=selectionMulti_.getIdxSelected()[0];
  //    int idxSel=selectionMulti_.get(idxLine).getSelectedIndex()[0];
  //
  //    return getModelePoly().splitGeometry(idxLine, idxSel, _cmd);
  //  }
  //  /**
  //   * Joint 2 polylignes par leurs sommets d�sign�s.
  //   *
  //   * @param _cmd La pile de commandes.
  //   * @return true si les polylignes ont �t� jointes.
  //   */
  //  public boolean joinGeometries(final CtuluCommandContainer _cmd) {
  //    if (isSelectionEmpty() || !isAtomicMode()) return false;
  //
  //    int[] idxLines=new int[2];
  //    int[] idxSels=new int[2];
  //    for (int i=0; i<2; i++) {
  //      idxLines[i]=selectionMulti_.getIdxSelected()[i];
  //      idxSels[i]=selectionMulti_.get(idxLines[i]).getSelectedIndex()[0];
  //    }
  //
  //    // On vide la selection, les 2 lignes sont supprim�es par la jonction.
  //    clearSelection();
  //    return getModelePoly().joinGeometries(idxLines, idxSels, _cmd)!=-1;
  //  }
  @Override
  public boolean addAtomeProjectedOnSelectedGeometry(GrPoint grPoint, CtuluCommandComposite cmp, CtuluUI _ui) {
    return addAtome(grPoint, cmp, _ui);
  }

  @Override
  public boolean addAtome(final GrPoint _ptReel, final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (getSelectionMode() == SelectionMode.SEGMENT || getNbSelected() != 1) {
      return false;
    }
    startMassiveOperation();
    try {
      int idx;
      if (getSelectionMode() == SelectionMode.ATOMIC) {
        idx = selectionMulti_.getIdxSelected()[0];
      } else {
        idx = selection_.getMinIndex();
      }

      int idxBefore = modeleDonnees().getGeomData().getGeometry(idx).getNumGeometries() - 1;
      ((ZModeleMultiPointEditable) modele_).addAtomic(idx, idxBefore, _ptReel.x_, _ptReel.y_, _ptReel.z_, _cmd);
    } finally {
      stopMassiveOperation();
    }
    return true;
  }

  @Override
  public boolean addForme(final GrObjet _o, final int _deforme, final CtuluCommandContainer _cmd, final CtuluUI _ui,
          final ZEditionAttributesDataI _data) {
    if (_deforme == DeForme.MULTI_POINT) {
      boolean added = false;
      startMassiveOperation();
      try {
        GrPolyligne gr = (GrPolyligne) _o;
        GISMultiPoint poly = (GISMultiPoint) GISGeometryFactory.INSTANCE.createMultiPoint(
                GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(gr.sommets_.createCoordinateSequence(false)));
        added = modeleDonnees().addGeometry(poly, _cmd, _ui, _data);
      } finally {
        stopMassiveOperation();
      }
      return added;
    }
    return false;
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return _typeForme == DeForme.MULTI_POINT;
  }

  @Override
  public boolean canUseAtomicMode() {
    return true;
  }

  @Override
  public boolean canUseSegmentMode() {
    return false;
  }

  /**
   * Retourne l'�diteur pour les objets selectionn�s.
   *
   * @return L'editeur. Peut �tre null.
   */
  @Override
  public final ZEditorInterface getEditor() {
    return editor_;
  }

  @Override
  public ZModeleMultiPointEditable modeleDonnees() {
    return (ZModeleMultiPointEditable) modele_;
  }

  @Override
  public ZModeleEditable getModelEditable() {
    return modeleDonnees();
  }

  /**
   * @deprecated Use {@link #modeleDonnees()} instead
   */
  private ZModeleMultiPointEditable getMultiPointModel() {
    return (ZModeleMultiPointEditable) modele_;
  }

  @Override
  public boolean moveSelectedObjects(final double _reelDx, final double _reelDy, final double _reelDz, final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (!isSelectionEmpty()) {
      startMassiveOperation();
      try {
        if (getSelectionMode() == SelectionMode.ATOMIC) {
          modeleDonnees().moveAtomics(selectionMulti_, _reelDx, _reelDy, _reelDz, _cmd, _ui);
        } else {
          modeleDonnees().moveGeometries(selection_, _reelDx, _reelDy, _reelDz, _cmd);
        }
      } finally {
        stopMassiveOperation();
      }
    }

    return false;
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#rotateSelectedObjects(double, double, double, org.fudaa.ctulu.CtuluCommandContainer, org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public boolean rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (!isSelectionEmpty()) {
      startMassiveOperation();
      try {
        if (getSelectionMode() == SelectionMode.ATOMIC) {
          modeleDonnees().rotateAtomics(selectionMulti_, _angRad, _xreel0, _yreel0, _cmd, _ui);
        } else {
          modeleDonnees().rotateGeometries(selection_, _angRad, _xreel0, _yreel0, _cmd);
        }
      } finally {
        stopMassiveOperation();
      }
    }
    return false;
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#paintDeplacement(Graphics2D, int, int, TraceIcon)
   */
  @Override
  public void paintDeplacement(final Graphics2D _g2d, final int _dxEcran, final int _dyEcran, final TraceIcon _trace) {
    if (!isSelectionEmpty()) {
      if (getSelectionMode() == SelectionMode.ATOMIC) {
        paintDeplacementMulti(_g2d, _dxEcran, _dyEcran, _trace);
      } else {
        paintDeplacementSimple(_g2d, _dxEcran, _dyEcran, _trace);
      }
    }
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#copySelectedObjects(org.fudaa.ctulu.CtuluCommandContainer, org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public boolean copySelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty() || (getSelectionMode() != SelectionMode.NORMAL)) {
      return false;
    }
    int nb = modeleDonnees().getNombre();

    GrSegment pdec = new GrSegment(new GrPoint(0, 5, 0), new GrPoint(5, 0, 0));
    pdec.autoApplique(getVersReel());
    if (!modeleDonnees().copyGlobal(selection_, 0/*pdec.getVx()*/, 0/*pdec.getVy()*/, _cmd)) {
      return false;
    }

    // Changement de selection.
    int[] isels = new int[selection_.getNbSelectedIndex()];
    for (int i = 0; i < isels.length; i++) {
      isels[i] = nb + i;
    }
    setSelection(isels);
    return true;
  }

  /**
   * Scinde en 2 une multipoint dont plusieurs sommets sont s�lectionn�s. Les attributs sont copi�s.
   *
   * @param _cmd La pile de commandes.
   * @return true si le multipoint a �t� scind�e.
   */
  @Override
  public boolean splitSelectedObject(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty() || (getSelectionMode() != SelectionMode.ATOMIC)) {
      return false;
    }
    startMassiveOperation();
    try {
      int idxGeom = selectionMulti_.getIdxSelected()[0];
      int[] idxSels = selectionMulti_.get(idxGeom).getSelectedIndex();

      // On vide la selection, le multipoint est supprim�es par le split.
      clearSelection();
      return modeleDonnees().splitGeometry(idxGeom, idxSels, _cmd) != null;
    } finally {
      stopMassiveOperation();
    }
  }

  /**
   * Dessine le deplacement pour les sommets selectionn�s.
   *
   * @param _g2d Le contexte graphique.
   * @param _dx Le delta suivant X
   * @param _dy Le delta suivant Y
   * @param _ic L'icone a tracer.
   */
  protected void paintDeplacementMulti(final Graphics2D _g, final int _dxEcran, final int _dyEcran, final TraceIcon _trace) {
    final GrBoite clip = getClipReel(_g);
    if (!getDomaine().intersectXY(clip)) {
      return;
    }
    Color cs = _trace.getCouleur();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TIntObjectIterator it = selectionMulti_.getIterator();
    final GrPoint p = new GrPoint();
    final GrMorphisme versEcran = getVersEcran();
    final TraceLigne tl = ligneModel_.buildCopy();
    tl.setCouleur(cs);
    for (int i = selectionMulti_.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        final int idxPoly = it.key();
        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            modele_.point(p, idxPoly, j);

            p.autoApplique(versEcran);
            final int xPivot = (int) (p.x_ + _dxEcran);
            final int yPivot = (int) (p.y_ + _dyEcran);
            _trace.paintIconCentre(this, _g, xPivot, yPivot);
            // trace de la ligne j-1 ->j
            int j0 = j - 1;
            if (j0 >= 0) {
              modele_.point(p, idxPoly, j0);
              p.autoApplique(versEcran);
              if (s.isSelected(j0)) {
                p.x_ += _dxEcran;
                p.y_ += _dyEcran;
              }
            }
            j0 = j + 1;
            if (j0 < modele_.getNbPointForGeometry(idxPoly)) {
              modele_.point(p, idxPoly, j0);
              p.autoApplique(versEcran);
              if (s.isSelected(j0)) {
                p.x_ += _dxEcran;
                p.y_ += _dyEcran;

              }
            }
          }
        }
      }
    }
  }

  /**
   * Dessine le deplacement pour les geometries s�lectionn�es.
   *
   * @param _g2d Le contexte graphique.
   * @param _dx Le delta suivant X
   * @param _dy Le delta suivant Y
   * @param _ic L'icone a tracer.
   */
  protected void paintDeplacementSimple(final Graphics2D _g2d, final int _dx, final int _dy, final TraceIcon _ic) {
    final GrBoite clip = getClipReel(_g2d);
    if (!getDomaine().intersectXY(clip)) {
      return;
    }
    final GrMorphisme versEcran = getVersEcran();
    Color cs = _ic.getCouleur();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g2d.setColor(cs);
    final TraceLigne tlSelection = ligneModel_.buildCopy();
    tlSelection.setCouleur(cs);
    final int nb = selection_.getMaxIndex();
    final GrBoite bPoly = new GrBoite();
    bPoly.e_ = new GrPoint();
    bPoly.o_ = new GrPoint();
    for (int i = nb; i >= 0; i--) {
      if (!selection_.isSelected(i)) {
        continue;
      }
      modele_.getDomaineForGeometry(i, bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }
      final int nbPoints = modele_.getNbPointForGeometry(i);
      final GrPoint ptOri = new GrPoint();
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(versEcran);
      _ic.paintIconCentre(this, _g2d, ptOri.x_ + _dx, ptOri.y_ + _dy);
      final GrPoint ptDest = new GrPoint();
      for (int j = nbPoints - 2; j >= 0; j--) {
        // le point de dest est initialise
        modele_.point(ptDest, i, j);
        ptDest.autoApplique(versEcran);
        _ic.paintIconCentre(this, _g2d, ptDest.x_ + _dx, ptDest.y_ + _dy);
        ptOri.initialiseAvec(ptDest);
      }
    }
  }

  @Override
  public boolean removeSelectedObjects(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }
    boolean r = false;
    startMassiveOperation();
    try {
      if (getSelectionMode() == SelectionMode.ATOMIC) {
        r = modeleDonnees().removeAtomics(selectionMulti_, _cmd, _ui);
      } else {
        final int[] idx = selection_.getSelectedIndex();
        super.clearSelection();
        r = modeleDonnees().removeGeometries(idx, _cmd);
      }
    } finally {
      stopMassiveOperation();
    }
    if (r) {
      super.clearSelection();
    }
    return r;
  }

  /**
   * Definit l'�diteur pour les objets selectionn�s.
   *
   * @param _editor L'editeur.
   */
  @Override
  public final void setEditor(final ZEditorInterface _editor) {
    editor_ = _editor;
  }

  /**
   * Joint 2 semis de points.
   *
   * @param _cmd La pile de commandes.
   * @return true si les semis de points ont �t� jointes.
   */
  @Override
  public boolean joinSelectedObjects(final CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }

    int[] idxSemis;
    if (isAtomicMode()) {
      idxSemis = selectionMulti_.getIdxSelected();
    } else {
      idxSemis = selection_.getSelectedIndex();
    }
    if (idxSemis.length != 2) {
      return false;
    }

    // On vide la selection, les 2 semis sont supprim�es par la jonction.
    clearSelection();
    boolean res = false;
    startMassiveOperation();
    try {
      res = modeleDonnees().joinGeometries(idxSemis, _cmd) != -1;
    } finally {
      stopMassiveOperation();
    }
    return res;
  }
}
