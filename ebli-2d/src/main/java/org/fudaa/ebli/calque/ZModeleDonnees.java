/**
 * @file         ZModeleDonnees.java
 * @creation     2002-10-8
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Une interface de base pour tous les modeles. Tous les modeles doivent founir le domaine de leurs
 * donnees et les contours.
 *
 * @version $Id$
 * @author Fred Deniger
 */
public interface ZModeleDonnees {

  /**
   * Cr�e la table pour le tableau des valeurs. Le tableau des valeurs contient
   * les donn�es du mod�le, g�om�tries ou sommets.
   * 
   * @param _layer Le calque associ� � this.
   * @return Un tableau.
   */
  BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer);

  /**
   * Renseigne les propri�t�s sur les objets du mod�le associ� au calque sp�cifi�. Elles seront affich�es dans le panneau
   * des propri�t�s.
   * 
   * @param _d
   *          Le tableau a renseigner, sous la forme cl�-valeur.
   * @param _layer
   *          Le calque associ� au mod�le.
   */
  void fillWithInfo(final BCalquePaletteInfo.InfoData _d, ZCalqueAffichageDonneesInterface _layer);

  boolean isValuesTableAvailable();

  /**
   * @return le domaine dans une nouvelle instance.
   */
  GrBoite getDomaine();

  /**
   * @return le nombre d'elements de ce modele (le nombre de points,....).
   */
  int getNombre();

  /**
   * L'objet d'index <code>_ind</code>. Lors d'une selection, cet index permet
   * de faire le lien entre le modele et la liste de s�lection sur ce mod�le.
   * 
   * @return L'objet d'index <code>_ind</code>.
   * @param _ind
   *          l'indice de l'objet
   */
  Object getObject(int _ind);
  
  /**
   * Retourne le sommet 3D d'indice _idVertex pour l'objet d'index _ind.
   * @param _ind L'indice de l'objet.
   * @param _idVertex L'indice du sommet.
   * @return Le point, ou null en cas de non conformit�.
   */
  GrPoint getVertexForObject(int _ind, int _idVertex);
  
}