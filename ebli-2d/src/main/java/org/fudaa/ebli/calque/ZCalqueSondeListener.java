package org.fudaa.ebli.calque;

import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Interface pour les listener sur les calqueSondeInteraction. En particulier
 * utilis� pour les fusions de calques afin de synchroniser les sondes entre
 * elles.
 * 
 * @author Adrien Hadoux
 * 
 */
public interface ZCalqueSondeListener {

  
 public void notifySondeMoved(final ZCalqueSondeInteraction activator, final GrPoint _e); 
  
  
}
