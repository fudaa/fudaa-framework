/**
 *  @file         ZCalqueAffichageDonneesInterface.java
 *  @creation     24 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import java.awt.Graphics2D;
import java.beans.PropertyChangeListener;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.commun.EbliTableInfoTarget;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPalettePlageTarget;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Une interface pour un calque d'affichage contenant des donn�es, supportant la recherche et
 * pouvant etre une cible d'une palette de couleurs.
 * 
 * @author deniger
 * @version $Id$
 */
public interface ZCalqueAffichageDonneesInterface extends EbliFindableItem, BPalettePlageTarget, EbliTableInfoTarget,
    ZCalqueAffichageInterface {

  void fillWithInfo(BCalquePaletteInfo.InfoData _d);

  /**
   * Efface la selection du calque.
   */
  void clearSelection();

  /**
   * Tout selectionner.
   */
  void selectAll();

  /**
   * Selectionne les objets d'indices donn�es.
   * @param _idx Les indices.
   */
  boolean setSelection(int[] _idx);
  
  /**
   * Selectionne l'objet suivant de l'objet en cours.
   * @return true si la selection a �t� modifi�e.
   */
  public boolean selectNext();

  
  /**
   * Selectionne l'objet suivant de l'objet en cours.
   * @return true si la selection a �t� modifi�e.
   */
  public boolean selectPrevious();
  
  /**
   * Inverser la selection.
   */
  void inverseSelection();

  boolean isEditable();

  /**
   * Definit si les objets du calque sont ou non selectionnables. Si la valeur retourn�e est false, les
   * m�thodes de s�lection de l'interface n'ont aucun effet.
   * @return true : Les objets sont selectionnables.
   */
  boolean isSelectable();

  /**
   * Definit si l'utilisateur peut changer le caract�re s�lectionnable/non  depuis l'action "Selectionnable".
   * @return true : Si le calque peut �tre rendu selectionnable
   */
  public boolean canSetSelectable();

  /**
   * @return la boite zoomant sur la selection. Cette boite est la boite englobante
   * augment�e d'une marge.
   */
  GrBoite getZoomOnSelected();
  
  /**
   * La boite englobante des objets s�lectionn�s.
   * @return La boite
   */
  GrBoite getDomaineOnSelected();

  /**
   * @return true si seulement un objet selectionne.
   */
  boolean isOnlyOneObjectSelected();

  /**
   * Permet de modifier la selection du calque � partir d'un selection ponctuelle.
   * 
   * @param _pointReel le point de selection en coordonnees reelles
   * @param _tolerancePixel la tolerance utilisee pour determiner si un objet est selectionne
   * @param _action voir les action de ZCalqueSelectionInteraction
   * @return true si la selection a ete modifiee
   * @see ZCalqueSelectionInteractionAbstract
   */
  boolean changeSelection(GrPoint _pointReel, int _tolerancePixel, int _action);

  /**
   * Permet de modifier la selection du calque � partir d'un poly de selection.
   * 
   * @param _p le polygone englobant en COORDONNEES REELS
   * @param _action l'action sur la selection (voir les action de ZCalqueSelectionInteraction)
   * @param _mode TODO
   * @return true si selection modifiee
   * @see ZCalqueSelectionInteractionAbstract
   */
  boolean changeSelection(LinearRing _p, int _action, int _mode);

  boolean changeSelection(LinearRing[] _p, int _action, int _mode);
  
  /**
   * Change la selection a partir d'une liste de selection.
   * @param _selection La liste de selection.
   * @param _action voir les action de ZCalqueSelectionInteraction
   * @return true si la selection a �t� modifi�e.
   */
  boolean changeSelection(EbliListeSelectionMultiInterface _selection, int _action);

  /**
   * Retourne les indices de selection des sous objets selectionn�s pour le point donn�, sous la forme d'une liste de selection. 
   * La selection s'effectue du point le plus au dessus vers le point le plus en dessous.<p>
   * Le plus souvent les sous objets selectionn�s sont des sommets (points).
   * 
   * @param _ptReel Le point donn� pour la selection.
   * @param _tolerancePixel La tol�rance, en pixels.
   * @param _inDepth Si faux, la recherche s'arrete a la premi�re occurence trouv�e. Sinon, la recherche continue pour tous les 
   * points suceptibles d'etre selectionn�s.
   * @return Les indices des sous objets selectionn�s, ou null si aucune selection.
   */
  EbliListeSelectionMultiInterface selectVertices(GrPoint _ptReel, int _tolerancePixel, boolean _inDepth);
  
  /**
   * @return la ligne repr�sentant la s�lection courant. null si s�lection non adequate
   */
  LineString getSelectedLine();

  /**
   * @return true si la selection est vide
   */
  @Override
  boolean isSelectionEmpty();

  /**
   * @return le modele utilise
   */
  ZModeleDonnees modeleDonnees();

  /**
   * @param _l un listener pour la selection
   */
  void addSelectionListener(ZSelectionListener _l);

  /**
   * @param _l le listener a enlever
   */
  void removeSelectionListener(ZSelectionListener _l);

  /**
   * @return l'id du calque
   */
  String getName();

  /**
   * @return true si la selection sur une selection entre 2 points est possible.
   */
  boolean isSpecialSelectionAllowed();

  /**
   * @param _l le listener a ajouter
   */
  void addPropertyChangeListener(PropertyChangeListener _l);

  /**
   * @param _l le listener a enlever
   */
  void removePropertyChangeListener(PropertyChangeListener _l);

  /**
   * Edition de la selection.
   * 
   * @return chaine d'erreur
   */
  String editSelected();
  
  

  /**
   * Permet au calque selection interaction de dessiner la s�lection en dernier et donc par-dessus tous les autres
   * calques.
   * 
   * @param _g le graphics a modifier
   * @param _trace le conteneur des tracer de selection
   */
  void paintSelection(Graphics2D _g, ZSelectionTrace _trace, GrMorphisme _versEcran, GrBoite _clipReel);

  /**
   * Redessiner le calque.
   */
  void repaint();

  /**
   * @return la selection multi. Peut etre null.
   */
  EbliListeSelectionMultiInterface getLayerSelectionMulti();

  /**
   * @return la selection simple. Peut etre null.
   */
  CtuluListSelectionInterface getLayerSelection();

  /**
   * Un calque peut dispose de plusieurs jeux d'icone.
   * 
   * @param _idx l'indice de l'icone.
   * @param _model
   * @return
   */
  boolean setIconModel(int _idx, final TraceIconModel _model);

  TraceIconModel getIconModel(int _idx);

  /**
   * @param _idx l'indice du jeu d'icone/ligne.
   * @return le titre du jeu
   */
  String getSetTitle(int _idx);

  /**
   * @return le nomnbre de jeu d'icone/ligne
   */
  int getNbSet();

  boolean setLineModel(int _idx, final TraceLigneModel _model);

  TraceLigneModel getLineModel(int _idx);

  /**
   * @return true si peut �tre utilis� dans les accroche.
   */
  boolean canBeUsedForAccroche();
}
