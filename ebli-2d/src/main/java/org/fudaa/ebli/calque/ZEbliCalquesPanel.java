/*
 * @creation 2002-10-8
 * 
 * @modification $Date$
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuDynamicMenu;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPopupMenu;
import com.memoire.bu.BuToolButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluListModelEmpty;
import org.fudaa.ctulu.gui.CtuluTreeComboboxModel;
import org.fudaa.ctulu.gui.CtuluTreeComboboxRenderer;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.animation.EbliAnimationSourceAbstract;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliModelInfos;
import org.fudaa.ebli.find.EbliFindDialog;
import org.fudaa.ebli.find.EbliFindable;
import org.fudaa.ebli.find.EbliFindableItem;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Fenetre d'affichage de calques contenant des services de base. (position souris, zoom fenetre, selection, transformations spatiales). Il est
 * possible d'ajouter des boutons grace aux methodes <code>addButtonGroup</code>. Par defaut, 3 groupes de boutons sont proposes : selection, standard
 * et navigation. Ils peuvent etre supprimes ou modifies avec les methodes remove
 *
 * @version $Id$
 * @author Guillaume Desnoix , Axel von Arnim, Fred deniger
 */
public class ZEbliCalquesPanel extends BuPanel implements EbliFilleCalquesInterface, BCalqueContextuelListener,
        CtuluImageProducer, BCalqueSaverTargetInterface, EbliFindable, ActionListener, ZSelectionListener,
        CtuluSelectionInterface {

  protected ZEditorDefault gisEditor_;
  private boolean addNbSelectionInInfo = false;

  protected class AnimAdapter extends EbliAnimationSourceAbstract {

    public AnimAdapter() {

    }

    @Override
    public Component getComponent() {
      return vc_;
    }

    @Override
    public Dimension getDefaultImageDimension() {
      return ZEbliCalquesPanel.this.getDefaultImageDimension();
    }

    @Override
    public BufferedImage produceImage(final Map _params) {
      return ZEbliCalquesPanel.this.produceImage(null);
    }

    @Override
    public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
      return ZEbliCalquesPanel.this.produceImage(_w, _h, null);
    }

    @Override
    public void setVideoMode(final boolean _b) {
      // pour eviter qu'un thread soit cree par la vue calque
      getVueCalque().setNoRepaintTask(_b);
      final Container f = SwingUtilities.getAncestorOfClass(JInternalFrame.class, ZEbliCalquesPanel.this);
      if (f != null) {
        ((JInternalFrame) f).setResizable(!_b);
      }
    }
  }

  /**
   * TODO BM Trop sp�cialis� pour �tre dans cette classe
   */
  public ZCalquePoint getIsoLayer() {
    return null;
  }

  /**
   * @author Fred Deniger
   * @version $Id$
   */
  public static class LayerDynamicMenu extends BuDynamicMenu {

    BCalque cq_;

    /**
     * @param _cq le calque en question
     */
    public LayerDynamicMenu(final BCalque _cq) {
      super(_cq.getTitle(), _cq.getName());
      cq_ = _cq;
    }

    @Override
    protected void build() {
      removeAll();
      final EbliActionInterface[] ac = cq_.getActions();
      if ((ac != null) && (ac.length > 0)) {
        EbliComponentFactory.INSTANCE.addActionsToMenu(ac, this);
      }
    }

    @Override
    protected boolean isActive() {
      return true;
    }
  }

  /**
   * Le menu des th�mes.
   *
   * @author Fred Deniger
   * @version $Id$
   */
  public static class ThemeMenu extends BuDynamicMenu implements TreeModelListener {

    final BCalque cqDonnees_;

    /**
     * @param _s le nom du menu
     */
    public ThemeMenu(final TreeModel _model, final BCalque _cqDonnees, final String _s) {
      super(_s, "VUE2D_CMD");
      cqDonnees_ = _cqDonnees;
      _model.addTreeModelListener(this);
    }

    public ThemeMenu(final BCalque _cqDonnees, final String _s, final String _name) {
      super(_s, _name);
      cqDonnees_ = _cqDonnees;
    }

    @Override
    protected void build() {
      if (getMenuComponentCount() == 0) {
        fillWithAllLayersAction(this);
        super.computeMnemonics();
      }
    }

    protected BuMenu createMenu(final BCalque _cq) {
      if (_cq == null) {
        return null;
      }
      BuMenu m = null;
      if (_cq.isGroupeCalque()) {
        m = new ThemeMenu(_cq, _cq.getTitle(), _cq.getName());
      } else {
        m = new LayerDynamicMenu(_cq);
      }
      return m;
    }

    @Override
    protected boolean isActive() {
      return true;
    }

    /**
     * Remplit un menu avec les actions associees aux calques.
     *
     * @param _projectMenu le menu a remplir
     */
    public void fillWithAllLayersAction(final JMenu _projectMenu) {
      final BCalque[] cqs = cqDonnees_.getCalques();
      if (cqs != null) {

        for (int i = 0; i < cqs.length; i++) {
          final BuMenu m = createMenu(cqs[i]);
          if (m != null) {
            _projectMenu.add(m);
          }
        }
      }
    }

    @Override
    public void treeNodesChanged(final TreeModelEvent _e) {
    }

    @Override
    public void treeNodesInserted(final TreeModelEvent _e) {
    }

    @Override
    public void treeNodesRemoved(final TreeModelEvent _e) {
    }

    @Override
    public void treeStructureChanged(final TreeModelEvent _e) {
      removeAll();

    }
  }

  /**
   * Rempli le menu avec les actions du calque en question.
   *
   * @param _m le menu a remplir
   * @param _c le calque dont les actions seront ajoutees au menu
   */
  public static final void fillMenuWithCalqueActions(final JMenu _m, final BCalque _c) {
    if (_c != null && _c.getActions() != null) {
      EbliComponentFactory.INSTANCE.addActionsToMenu(_c.getActions(), _m);
    }
  }

  /**
   * Rempli le menu popup avec les actions du calque en question.
   *
   * @param _m le menu a remplir
   * @param _c le calque dont les actions seront ajoutees au menu
   */
  public static final void fillMenuWithCalqueActions(final JPopupMenu _m, final BCalque _c) {
    if (_c != null && _c.getActions() != null) {
      EbliComponentFactory.INSTANCE.addActionsToMenu(_c.getActions(), _m);
    }
  }

  /**
   * Rempli le menu avec les actions de la scene
   *
   * @param _m le menu a remplir
   */
  protected void fillMenuWithSceneActions(final JMenu _m) {
    if (scene_ != null && gisEditor_.getSceneEditor().getActions() != null) {
      EbliComponentFactory.INSTANCE.addActionsToMenu(gisEditor_.getSceneEditor().getActions(), _m);
    }
  }

  /**
   * Rempli le menu popup avec les actions de la scene
   *
   * @param _m le menu a remplir
   */
  protected void fillMenuWithSceneActions(final JPopupMenu _m) {
    if (scene_ != null && gisEditor_.getSceneEditor().getActions() != null) {
      EbliComponentFactory.INSTANCE.addActionsToMenu(gisEditor_.getSceneEditor().getActions(), _m);
    }
  }

  private boolean customize_;

  private BGroupeCalque gcDonnees_;

  public BGroupeCalque getGcDonnees() {
    return gcDonnees_;
  }

  public void setGcDonnees_(final BGroupeCalque gcDonnees_) {

    this.gcDonnees_ = gcDonnees_;
  }

  /**
   * contient des informations de cr�ation du vue2d. TODO BM Trop sp�cialis� pour �tre dans cette classe.
   */
  public final static String NOM_FIC = EbliResource.EBLI.getString("Fichier res");
  public final static String PATH_FIC = EbliResource.EBLI.getString("Chemin");
  public final static String TITRE_FIC = EbliResource.EBLI.getString("Titre");
  public final static String MAILLAGE_FIC = EbliResource.EBLI.getString("Maillage utilis�");
  public final static String DIFF_FIC = EbliResource.EBLI.getString("Diff�rentiel fichiers");

  private Map<String, String> infosCreation_ = new HashMap<String, String>();

  protected BuMenu contextTools_;

  protected BuLabel mode_;

  protected BArbreCalqueModel modelArbre_;

  protected BVueCalque vc_;

  ZEbliCalquePanelController controller_;

  JLabel lbSuivi_;

  BuMenu[] specificMenus_;

  public ZEbliCalquesPanel(final CtuluUI _ui) {
    this(null, new ZEbliCalquePanelController(_ui));
  }

  public void addInfosCreation(String key, String value) {
    infosCreation_.put(key, value);
  }

  public ZEbliCalquesPanel(final BCalque _cq, final CtuluUI _ui) {
    this(_cq, new ZEbliCalquePanelController(_ui));
  }

  public CtuluUI getCtuluUI() {
    return getController().getUI();
  }

  BuLabel info_;
  BuPanel suiviPanel_;

  /**
   * Constructeur minimal. Les groupes de navigation et standard seront construits.
   *
   * @param _gcInit le calque de base
   */
  public ZEbliCalquesPanel(final BCalque _gcInit, final ZEbliCalquePanelController _controller) {
    this(_gcInit, _controller, true);
  }

  /**
   * Attention a n'utiliser que si les composants est d�truit et plus utilise.
   */
  public void clearListeners() {
    controller_.clearListeners();
    contextTools_.removeAll();
  }

  /**
   * Construction des calques par defaut.
   *
   * @param _gcInit le calque de base
   * @param _controller
   * @param _addSouth Ajout ou non des services d'info en bas de fenetre (position, mode, etc.).
   */
  public ZEbliCalquesPanel(final BCalque _gcInit, final ZEbliCalquePanelController _controller, final boolean _addSouth) {
    setBackground(Color.white);
    setOpaque(true);
    customize_ = true;
    // vc_ = _vc;
    // Les calques
    final BGroupeCalque gc = new BGroupeCalque();
    gc.setName("cqRACINE");
    gc.setTitle(EbliLib.getS("Calque racine"));
    gc.setDestructible(false);

    gcDonnees_ = new BGroupeCalque() {

      // B.M. Pour eviter le plantage, une boite est retourn�e si rien n'est affich� ou boite non conforme.
      @Override
      public GrBoite getDomaine() {
        GrBoite bt = super.getDomaine();

        if (bt == null) {
          bt = new GrBoite(new GrPoint(0, 0, 0), new GrPoint(100, 100, 0));
        } else if (bt.getDeltaX() == 0 || bt.getDeltaY() == 0) {
          bt.o_.x_ -= 10;
          bt.o_.y_ -= 10;
          bt.e_.x_ += 10;
          bt.e_.y_ += 10;
        }
        return bt;
      }
    };
    gcDonnees_.setTitle(EbliLib.getS("Calques"));
    gcDonnees_.setName("cqDonnees");
    // Calque d'administration
    gc.add(gcDonnees_);
    if (_gcInit != null && _gcInit.isGroupeCalque()) {
      final BCalque[] cq = _gcInit.getCalques();
      for (int i = 0; i < cq.length; i++) {
        gcDonnees_.add(cq[i]);
      }
    } else if (_gcInit != null) {
      gcDonnees_.add(_gcInit);
    }

    vc_ = new BVueCalque(gc);
    // addKeyListener(vc_);
    vc_.setPreferredSize(new Dimension(400, 400));
    modelArbre_ = new BArbreCalqueModel(gcDonnees_);
    // le panel sud contenant le suivi de souris et le mode

    mode_ = new BuLabel();
    mode_.setFont(CtuluLibSwing.getMiniFont());
    mode_.setPreferredSize(new Dimension(150, 20));
    mode_.setSize(new Dimension(150, 20));
    mode_.setVisible(false);
    mode_.setOpaque(false);
    info_ = new BuLabel();
    info_.setFont(mode_.getFont());
    info_.setPreferredSize(new Dimension(120, 20));
    info_.setSize(new Dimension(120, 20));
    info_.setOpaque(false);
    info_.setVisible(true);

    // construction du panel principal
    setLayout(new BuBorderLayout(0, 0));
    vc_.setListener(this);
    add(vc_, BuBorderLayout.CENTER);
    if (_addSouth) {
      suiviPanel_ = new BuPanel();
      suiviPanel_.setName("pnSuivis");
      suiviPanel_.setLayout(new BuBorderLayout());
      suiviPanel_.setOpaque(false);
      suiviPanel_.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.gray));
      suiviPanel_.add(getLabelSuiviSouris(), BuBorderLayout.WEST);
      suiviPanel_.add(mode_, BuBorderLayout.EAST);
      suiviPanel_.add(info_, BuBorderLayout.CENTER);
      add(suiviPanel_, BuBorderLayout.SOUTH);
    }

    modelArbre_.refresh();
    if (_controller == null) {
      throw new IllegalArgumentException();
    }
    controller_ = _controller;
    controller_.setView(this);

    scene_ = new ZScene(modelArbre_, controller_.getCqSelectionI());
    scene_.setRestrictedToCalqueActif(true);
    scene_.addSelectionListener(this);
    modelArbre_.addTreeModelListener(scene_);

    gisEditor_ = createGisEditor();
    if (gisEditor_ != null) {
      gisEditor_.setUi(controller_.getUI());
    }
    controller_.getCqSelectionI().setEditor(gisEditor_);
    controller_.getCqCatchI().setScene(scene_);
  }

  protected ZEditorDefault createGisEditor() {
    return new ZEditorDefault(this);
  }

  /**
   * @return retourne le ZEditorDefault poss�d�.
   */
  public ZEditorDefault getEditor() {
    return gisEditor_;
  }

  public void setAddNbSelectionInInfo(boolean addNbSelectionInInfo) {
    if (this.addNbSelectionInInfo == addNbSelectionInInfo) {
      return;
    }
    this.addNbSelectionInInfo = addNbSelectionInInfo;
    infoTextSelectionUpdated();
  }

  public void setAccrocheText(ZCatchEvent _evt) {
    GrPoint pt = _evt.selection.getScene().getVertex(_evt.idxGeom, _evt.idxVertex);
    if (pt == null) {
      return;
    }
    String idxGeom = "" + (_evt.selection.getScene().sceneId2LayerId(_evt.idxGeom) + 1);
    String idxVertex = "" + (_evt.idxVertex + 1);
    String cqName = _evt.selection.getScene().getLayerForId(_evt.idxGeom).getTitle();
    String x;
    String y;
    String z;
    String xname;
    String yname;
    String zname;
    EbliCoordinateDefinition[] defs = getCoordinateDefinitions();
    if (defs != null) {
      x = defs[0].getFormatter().getXYFormatter().format(pt.x_);
      y = defs[1].getFormatter().getXYFormatter().format(pt.y_);
      z = defs[2].getFormatter().getXYFormatter().format(pt.z_);

      xname = defs[0].getName();
      yname = defs[1].getName();
      zname = defs[2].getName();
    } else {
      x = "" + pt.x_;
      y = "" + pt.y_;
      z = "" + pt.z_;

      xname = "X";
      yname = "Y";
      zname = "Z";
    }

    String[] vars = {cqName, idxGeom, idxVertex, xname, x, yname, y, zname, z};
    setAccrocheText(vars);
  }

  public void setAccrocheText(String[] vars) {
    String text
            = EbliResource.EBLI.getString(
                    "Accroche : Calque={0} Geom={1} Sommet={2} {3}={4} {5}={6} {7}={8} (n:Suivant, u:D�crocher)", vars);
    setInfoText(text);
  }

  public void unsetAcrrocheText() {
    unsetInfoText();
  }

  public void setInfoText(final String _s) {

    if (CtuluLibString.isEmpty(_s)) {
      unsetInfoText();
    } else {
      infoTextUpdated("|" + _s);
    }
  }

  protected void infoTextUpdated(String _s) {
    info_.putClientProperty("infoText", _s);
    updateInfoText();
  }

  protected void infoTextSelectionUpdated() {
    String selection = null;
    if (addNbSelectionInInfo) {
      int selected = selected = getScene().getSelectionHelper().getNbTotalSelectedObject();
      if (selected > 0) {
        selection = EbliLib.getS("{0} obj. s�lect.", Integer.toString(selected));
      }
    }
    info_.putClientProperty("infoTextSelection", selection);
    updateInfoText();
  }

  protected void updateInfoText() {
    String info = (String) info_.getClientProperty("infoText");
    String selected = "";
    if (addNbSelectionInInfo) {
      selected = (String) info_.getClientProperty("infoTextSelection");
      if (selected == null) {
        selected = "";
      }
    }
    if (selected.length() > 0) {
      if (info != null && info.length() > 0) {
        info = "|" + selected + "  " + info;
      } else {
        info = "|" + selected;
      }

    }
    info_.setText(info);
    info_.setToolTipText(info);

  }

  public void unsetInfoText() {
    infoTextUpdated(CtuluLibString.EMPTY_STRING);
  }

  protected void buildTools() {
    contextTools_ = new BuMenu(EbliLib.getS("Outils"), "TOOLS");
    fillMenuWithToolsActions(contextTools_);
  }

  /**
   * Creation des menus sp�cifiques. Les menus sp�cifiques sont switch�s d�s que la fenetre passe de l'�tat actif � inactif et inversement. Ils sont
   * ajout�s dans la barre de menus.
   *
   * @return Les menus sp�cifiques.
   */
  protected BuMenu[] createSpecificMenus(final String _title) {
    final BuMenu[] res = new BuMenu[1];
    res[0] = new BuMenu(_title, "LAYER");
    res[0].setIcon(null);
    res[0].add(BArbreCalque.buildZNormalMenu(modelArbre_));
    fillMenuWithToolsActions(res[0]);
    return res;
  }

  /**
   * @return the suiviPanel
   */
  public JComponent getSuiviPanel() {
    return suiviPanel_;
  }

  protected void fillCmdContextuelles(final BuPopupMenu _menu) {
    final BCalque cqActif = scene_.getCalqueActif();
    if (cqActif != null) {
      fillMenuWithCalqueActions(_menu, cqActif);
    }
    if (scene_ != null) {
      fillMenuWithSceneActions(_menu);
    }
    _menu.add(getMenuSelectionPath());
    if (contextTools_ == null) {
      buildTools();
    }
    _menu.add(contextTools_);
  }

  @Override
  public final void inverseSelection() {
    getScene().inverseSelection();
  }

  @Override
  public final void clearSelection() {
    getScene().clearSelection();
  }

  public final ZEbliCalquePanelController getController() {
    return controller_;
  }

  protected void updateKeyMap(final JComponent _c) {
    controller_.updateKeyMap(_c);
  }

  public final ZCalqueSelectionInteractionAbstract getCqSelectionI() {
    return controller_.getCqSelectionI();
  }

  public void addCalqueInteraction(final BCalqueInteraction _b) {
    controller_.addCalqueInteraction(_b);
  }

  public EbliActionInterface[] getApplicationActions() {
    return null;
  }

  /**
   * Methode a utiliser pour ajouter un calque et pour mettre a jour le model de l'arbre.
   */
  public void addCalque(final BCalque _c) {
    gcDonnees_.add(_c);
    modelArbre_.refresh();
  }

  public void removeCalque(final BCalque _c) {
    gcDonnees_.remove(_c);
    modelArbre_.refresh();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String act = _e.getActionCommand();
    if ("SELECTFROMPOLY".equals(act)) {
      findPolys();
    } else if (EbliActionMap.getInstance().isEmpty()) {
      //in this case, actions are local and not in the global registry:
      if ("CLEARSELECTION".equals(act)) {
        clearSelection();
      } else if ("INVERSESELECTION".equals(act)) {
        inverseSelection();
      } else if ("TOUTSELECTIONNER".equals(act)) {
        select();
      } else if ("RECHERCHER".equals(act)) {
        find();
      }
    }

  }

  public void addCalque(final BCalque _b, final boolean _enPremier) {
    if (_enPremier) {
      gcDonnees_.enPremier(_b);
    } else {
      gcDonnees_.add(_b);
    }
    modelArbre_.refresh();
  }

  /**
   * Met a jour l'association calque/action.
   *
   * @param _c le calque en question
   * @param _act les actions associees a ce calque
   */
  public void addCalqueActions(final BCalque _c, final EbliActionInterface[] _act) {
    if (modelArbre_.getSelectedCalque() == _c) {
      for (int i = _act.length - 1; i >= 0; i--) {
        if (_act[i] != null) {
          _act[i].updateStateBeforeShow();
        }
      }
    }
    _c.setActions(_act);
  }

  public void detruireCalque(final BCalque _b) {
    gcDonnees_.detruire(_b);
  }

  public void fillMenuWithToolsActions(final JMenu _m) {
    controller_.fillMenuWithToolsActions(_m);
  }

  public void fillMenuWithToolsActions(final JPopupMenu _m) {
    controller_.fillMenuWithToolsActions(_m);
  }

  /**
   * @return ArbreCalque
   */
  @Override
  public BArbreCalqueModel getArbreCalqueModel() {
    return modelArbre_;
  }

  public EbliActionInterface[] getBaseActions() {
    return controller_.getBaseActions();
  }

  /**
   * Renvoie le calque actif ( selectionne dans l'arbre).
   */
  public BCalque getCalqueActif() {
    return controller_.getCalqueActif();
  }

  /**
   * Revoie la scene.
   */
  private ZScene scene_ = null;

  public ZScene getScene() {
    return scene_;
  }

  @Override
  public JPopupMenu getCmdsContextuelles() {
    final BuPopupMenu r = new BuPopupMenu();
    r.setInvoker(this);
    fillCmdContextuelles(r);
    return r;
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return vc_.getSize();
  }

  public BGroupeCalque getDonneesCalque() {
    return gcDonnees_;
  }

  public BCalque getDonneesCalqueByName(final String _name) {
    return gcDonnees_.getCalqueParNom(_name);
  }

  public BCalque getDonneesCalqueByTitle(final String _name) {
    return gcDonnees_.getCalqueParTitre(_name);
  }

  /**
   * @deprecated Use getCoordinateDefinitions() instead.
   * @return
   */
  public final EbliFormatterInterface getEbliFormatter() {
    return controller_.getEbliFormatter();
  }

  public final JLabel getLabelSuiviSouris() {
    if (lbSuivi_ == null) {
      lbSuivi_ = new JLabel();
      lbSuivi_.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
      lbSuivi_.setPreferredSize(new Dimension(200, 20));
      lbSuivi_.setOpaque(false);
      lbSuivi_.setSize(new Dimension(100, 20));
    }
    return lbSuivi_;
  }

  /**
   * @return NumberOfPages
   */
  public int getNumberOfPages() {
    return 1;
  }

  /**
   * Retourne les menus sp�cifiques pour la fenetre.
   *
   * @see createSpecificMenus(String);
   * @return Les menus sp�cifiques.
   */
  public JMenu[] getSpecificMenus(final String _title) {
    if (specificMenus_ == null) {
      controller_.initSpecificActions();
      specificMenus_ = createSpecificMenus(_title);
    }
    return specificMenus_;
  }

  /**
   * @return VueCalque
   */
  public BVueCalque getVueCalque() {
    return vc_;
  }

  /**
   * Renvoie true si l'affichage peut-etre "customisable".
   *
   * @return boolean
   */
  public boolean isCustomize() {
    return customize_;
  }

  /**
   * Propriete de visibilite du composant de renseignement de l'etat.
   */
  public boolean isModeVisible() {
    return mode_.isVisible();
  }

  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    final BVueCalque bv = getVueCalque();
    final Graphics2D g2d = (Graphics2D) _g;
    g2d.setColor(Color.black);
    g2d.setFont(EbliPrinter.FONTBASE);
    final double x = _format.getImageableX();
    final double y = _format.getImageableY();
    g2d.translate(x, y);
    double wComp = bv.getWidth();
    double hComp = bv.getHeight();
    // recuperation des dimension de la zone imprimable
    final double hImprimable = _format.getImageableHeight();
    final double wImprimable = _format.getImageableWidth();
    final double facteur = Math.min(wImprimable / wComp, hImprimable / hComp);
    wComp = wComp * facteur;
    hComp = hComp * facteur;
    // definition de la zone d'impression
    final double xCentre = (wImprimable - wComp) / 2;
    final double yCentre = (hImprimable - hComp) / 2;
    g2d.translate(xCentre, yCentre);
    final Shape s = _g.getClip();
    g2d.clip(new Rectangle.Double(-5D, -5D, wComp + 10, hComp + 10));
    // mise a l'echelle et impression
    g2d.scale(facteur, facteur);
    bv.paint(g2d);
    // Restauration du graphics.
    g2d.scale(1 / facteur, 1 / facteur);
    g2d.translate(-xCentre, -yCentre);
    g2d.translate(-x, -y);
    g2d.setClip(s);
    return Printable.PAGE_EXISTS;
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    final BufferedImage i = CtuluLibImage.createImage(vc_.getWidth(), vc_.getHeight(), _params);
    final Graphics2D g = i.createGraphics();
    CtuluLibImage.setBestQuality(g);
    vc_.paintImage(g, _params);
    g.dispose();
    i.flush();
    return i;
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    final BufferedImage i = CtuluLibImage.createImage(_w, _h, _params);
    return paintOnImage(_w, _h, _params, i);
  }

  public BufferedImage paintOnImage(int _w, int _h, Map _params, BufferedImage i) {
    final Graphics2D g = i.createGraphics();
    CtuluLibImage.setBestQuality(g);
    g.scale(CtuluLibImage.getRatio(_w, vc_.getWidth()), CtuluLibImage.getRatio(_h, vc_.getHeight()));
    vc_.paintImage(g, _params);
    g.dispose();
    i.flush();
    return i;
  }

  public void removeAllCalqueDonnees() {
    gcDonnees_.removeAll();
  }

  public void removeCalqueInteraction(final BCalqueInteraction _b) {
    controller_.removeCalqueInteraction(_b);
  }

  /**
   * Restaure le viewport sur le domaine �tendu � tous les calques.
   */
  public void restaurer() {
    controller_.restaurer();
  }

  /**
   * @param _c Calque
   */
  public void setCalque(final BCalque _c) {
    final BCalque[] calques = gcDonnees_.getCalques();
    for (int i = 0; i < calques.length; i++) {
      gcDonnees_.detruire(calques[i]);
    }
    if (_c != null) {
      gcDonnees_.add(_c);
    }
    modelArbre_.refresh();
  }

  /**
   * Permet de changer le calque actif.
   */
  public final void setCalqueActif(final BCalque _calque) {
    modelArbre_.setSelectionCalque(_calque);
  }

  public void setCalqueInteractionActif(final BCalqueInteraction _b) {
    controller_.setCalqueInteractionActif(_b);
  }

  public void setCalqueSelectionActif() {
    controller_.setCalqueSelectionActif();
  }

  /**
   * Si true, l'affichage peut-etre modifie.
   *
   * @param _customize The customize to set
   */
  public void setCustomize(final boolean _customize) {
    customize_ = _customize;
  }

  @Override
  public ComboBoxModel getItemModel() {
    return new CtuluTreeComboboxModel(getArbreCalqueModel()) {
      @Override
      public boolean isSelectable(final Object _o) {
        return EbliFindDialog.isCalqueFindable(_o);
      }
    };
  }

  @Override
  public boolean setSelected(final EbliFindableItem _item) {
    getArbreCalqueModel().setSelectionCalque((BCalque) _item);
    return true;
  }

  @Override
  public void zoom(final GrBoite _env) {
    getVueCalque().changeRepere(this, _env, 0, 0);
  }

  protected EbliFindDialog findDialog_;

  @Override
  public void find() {
    if (findDialog_ == null) {
      findDialog_ = new EbliFindDialog(this);
    } else {
      findDialog_.updateBeforeShow();
    }
    final BCalque cq = getCalqueActif();
    findDialog_.setSelectedFindAction((cq instanceof EbliFindableItem) ? (EbliFindableItem) cq : null);
    findDialog_.affiche(this);
  }

  public void findPolys() {
    find();
    final JComponent jc = findDialog_.getActive();
    if (jc instanceof JTabbedPane) {
      ((JTabbedPane) jc).setSelectedIndex(1);
    }
  }

  @Override
  public void replace() {
  }

  @Override
  public void select() {
    getScene().selectAll();
    // if (getCalqueActif() instanceof ZCalqueAffichageDonneesInterface) {
    // ((ZCalqueAffichageDonneesInterface) getCalqueActif()).selectAll();
    // }
  }

  @Override
  public ListCellRenderer getComboRender() {
    return new CtuluTreeComboboxRenderer(getArbreCalqueModel(), new BArbreCalque.ArbreCellLabel()) {

      @Override
      public void update(final JList _l, final JComponent _comp, final boolean _isSelected, final Object _value) {
        final JLabel lb = (JLabel) _comp;
        _comp.setToolTipText(lb.getText());
        final JComponent cq = (JComponent) _value;
        final Object val = cq.getClientProperty(Action.SHORT_DESCRIPTION);
        if (val == null) {
          setToolTipText(lb.getText());
        } else {
          setToolTipText("<html><body style=\"padding:3px;\">" + lb.getText() + "<br>" + val.toString()
                  + "</body></html>");
        }
        if (_isSelected && !EbliFindDialog.isCalqueFindable(_value)) {
          setBackground(_l.getBackground());
          _comp.setBackground(_l.getBackground());
          _comp.setForeground(_l.getForeground());
          setToolTipText(EbliLib.getS("Recherche inactive"));
        }

      }
    };
  }

  /**
   * @deprecated Use setCoordinateDefinitions() instead.
   * @param _xyFormatter Le format, unique pour chaque coordonn�e.
   */
  public final void setEbliFormatter(final EbliFormatterInterface _xyFormatter) {
    controller_.setEbliFormatter(_xyFormatter);
  }

  /**
   * Definit le nom des coordonn�es affich�es dans la barre de statut.
   *
   * @param _names Le nom des coordonn�es.
   */
  public void setCoordinateDefinitions(EbliCoordinateDefinition[] _names) {
    controller_.setCoordinateDefinitions(_names);
  }

  /**
   * Retourne le nom des coordonn�es du syst�me de coordonn�es.
   *
   * @return Le tableau des noms des coordonn�es.
   */
  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return controller_.getCoordinateDefinitions();
  }

  public void setModeText(final String _txt) {
    if (mode_ != null) {
      if (CtuluLibString.isEmpty(_txt)) {
        mode_.setText(CtuluLibString.EMPTY_STRING);
      } else {
        mode_.setText("<html><body>|" + _txt + "</body></html>");
      }
      mode_.setToolTipText(mode_.getText());
    }
  }

  /**
   * Cette fonctionnalit� est d�plac�e et �tendue dans ZSceneEditor Remarque de Fred: La fonction zoomOnSelected ajoute des marges sur le cote ce qui
   * �vite d'avoir un zoom avec une boite de dimension null si un seul point est s�lectionne.
   *
   * @deprecated: � voir...
   */
  public void zoomOnSelected() {
    final BCalque cq = getCalqueActif();
    if (cq instanceof ZCalqueAffichageDonneesInterface) {
      final GrBoite b = ((ZCalqueAffichageDonneesInterface) cq).getZoomOnSelected();
      if (b != null) {
        getVueCalque().changeRepere(this, b);
      }
    }
  }

  public boolean isZoomOnSelectedEnable() {
    final BCalque cq = getCalqueActif();
    return cq instanceof ZCalqueAffichageDonneesInterface
            && !((ZCalqueAffichageDonneesInterface) cq).isSelectionEmpty();
  }

  /**
   * Rend visible le label d�crivant l'etat de ce composant (selection, zoom, deplacement...).
   */
  public final void setModeVisible(final boolean _b) {
    mode_.setVisible(_b);
  }

  /**
   * Retourne les donn�es persistantes.
   *
   * @return
   */
  public ZEbliCalquesPanelPersistManager getPersistenceManager() {
    return null;
  }

  public void unsetCalqueInteractionActif(final BCalqueInteraction _b) {
    controller_.unsetCalqueInteractionActif(_b);
  }

  public BCalqueInteraction unsetCurrentInteractifCalque() {
    return controller_.unsetCurrentInteractifCalque();
  }

  /**
   * @return le groupe de calque d'info
   */
  public BGroupeCalque getCqInfos() {
    return (BGroupeCalque) vc_.getCalque().getCalqueParNom("gpInfo");
  }

  /**
   * Ajoute les calques d'informations. Les calques contenus dans ce groupe de calques ne sont pas visibles dans l'arbre des calques. Les objets de
   * ces calques ne sont pas s�lectionnables, mais visibles dans la fenetre de vue des calques.
   *
   * @param _g le maillage associe
   */
  protected BGroupeCalque addCqInfos() {
    if (getCqInfos() != null) {
      return getCqInfos();
    }
    final BGroupeCalque gr = new BGroupeCalque();
    gr.setName("gpInfo");
    gr.setTitle(EbliLib.getS("infos"));
    final BCalqueLegende l = new BCalqueLegende();
    l.setDestructible(false);
    l.setTitle(EbliLib.getS("L�gende"));
    l.setName("cqLegende");
    gr.add(l);
    vc_.getCalque().enPremier(gr);
    return gr;
  }

  protected BGroupeCalque addCqInfos(final BCalqueLegende l) {
    if (getCqInfos() != null) {
      return getCqInfos();
    }
    final BGroupeCalque gr = new BGroupeCalque();
    gr.setName("gpInfo");
    gr.setTitle(EbliLib.getS("infos"));
    l.setDestructible(false);
    l.setTitle(EbliLib.getS("L�gende"));
    l.setName("cqLegende");
    gr.add(l);
    vc_.getCalque().enPremier(gr);
    return gr;
  }

  public void addCqLegende(final BCalqueLegende _leg) {
    _leg.setName("cqLegende");
    if (getCqInfos() != null) {
      removeCalqueLegend();
      getCqInfos().add(_leg);
    } else {
      addCqInfos(_leg);
    }
  }

  protected final void removeCqInfos() {
    vc_.getCalque().detruire(getCqInfos());
  }

  /**
   * @return le calque legende
   */
  public final BCalqueLegende getCqLegend() {
    if (getCqInfos() == null) {
      addCqInfos();
    }
    return (BCalqueLegende) getCqInfos().getCalqueParNom("cqLegende");
  }

  public void removeCalqueLegend() {
    if (getCqLegend() != null) {
      getCqInfos().remove(getCqLegend());
      // getCqLegend().setVisible(false);
    }
  }

  public void addCalqueLegend() {
    getCqInfos().add(getCqLegend());
    getCqLegend().setVisible(true);
  }

  public final void setInfoPaletteActive() {
    getController().setInfoPaletteActive();

  }

  public JMenu getMenuSelectionPath() {
    final BuMenu selection = new BuMenu(EbliLib.getS("S�lection"), "SELECTION");
    getController().addSelectionActionTo(selection);
    selection.addSeparator();
    if (!EbliActionMap.getInstance().isEmpty()) {
      ZCalqueSelectionInteractionAbstract.addSelectionAction(selection, true);
    } else {
      ZCalqueSelectionInteractionAbstract.addSelectionAction(selection, true, this);
    }
    selection.addMenuItem(EbliLib.getS("S�lectionner � partir de polygones"), "SELECTFROMPOLY", EbliResource.EBLI
            .getMenuIcon("draw-polygon"), this);
    return selection;
  }

  /*
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  @Override
  public void selectionChanged(final ZSelectionEvent _evt) {
    modelArbre_.fireObservableChanged();
    infoTextSelectionUpdated();
  }

  public ZEbliCalquesPanel duplicate(final Map options) {
    return null;
  }

  /**
   * Cree un tableau qui r�capitule le contenu des infos de cr�ation de la vue 2d. Retourne un panel vide si aucune infos n'est disponible.
   *
   * @return
   */
  public BuPanel buildInfosCreationComposant() {

    if (infosCreation_ == null || infosCreation_.keySet() == null || infosCreation_.keySet().size() == 0) {
      return new BuPanel();
    }

    final JTable table = createTable();

    final BuPanel conteneur = new BuPanel(new BorderLayout());
    conteneur.add(new JScrollPane(table), BorderLayout.CENTER);

    final BuToolButton bt = new BuToolButton(EbliResource.EBLI.getToolIcon("haut-droit.gif"));
    bt.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        final CtuluDialogPanel pn = new CtuluDialogPanel();
        pn.setLayout(new BuBorderLayout());
        final JTable newtable = new CtuluTable(table.getModel());
        newtable.getColumnModel().getColumn(0).setPreferredWidth(20);
        pn.add(new JScrollPane(newtable), BorderLayout.CENTER);
        pn.afficheModale(table, CtuluDialog.OK_OPTION);
        newtable.setModel(CtuluListModelEmpty.EMPTY);
      }
    });
    JPanel top = new BuPanel(new BuBorderLayout());

    top.add(new JLabel("<html><body><b>" + EbliLib.getS("Infos cr�ations") + "</b></body></html>",
            SwingConstants.CENTER), BorderLayout.CENTER);
    top.add(bt, BuBorderLayout.EAST);
    bt.setBorder(null);
    bt.setMargin(BuInsets.INSETS0000);
    conteneur.add(top, BuBorderLayout.NORTH);
    return conteneur;
  }

  public JTable createTable() {
    final List<String> liste = new ArrayList<String>(infosCreation_.keySet());
    Collections.sort(liste);
    final List<String> col = new ArrayList<String>();
    col.add(EbliLib.getS("Nom"));
    col.add(EbliLib.getS("Valeur"));

    final EbliModelInfos modelInfos = new EbliModelInfos(liste, infosCreation_, col);
    final JTable table = new CtuluTable(modelInfos);
    table.getColumnModel().getColumn(0).setPreferredWidth(20);
    return table;
  }

  /**
   * @return the infosCreation_
   */
  public Map<String, String> getInfosCreation() {
    return infosCreation_;
  }

}
