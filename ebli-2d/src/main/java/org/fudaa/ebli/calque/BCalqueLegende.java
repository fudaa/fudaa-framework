/**
 * @creation 1999-04-23
 * @modification $Date: 2007-06-20 12:23:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import java.awt.*;
import java.awt.event.ContainerListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.gui.CtuluHorizontalLayout;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BSelecteurAncre;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.palette.BPaletteCouleurSimple;

/**
 * Un calque d'affichage de legendes.
 * 
 * @version $Id: BCalqueLegende.java,v 1.36 2007-06-20 12:23:12 deniger Exp $
 * @author Axel von Arnim
 */
public class BCalqueLegende extends BCalqueAffichage implements BSelecteurTargetInterface {

  public static int getSaveAncre() {
    return EbliPreferences.EBLI.getIntegerProperty("legende.ancre", BSelecteurAncre.UP_RIGHT);
  }

  public static void saveAncre(final int _ancre) {
    EbliPreferences.EBLI.putIntegerProperty("legende.ancre", _ancre);
  }

  private int ancre_;
  /** Contient les calques dont la l�gende est affich�e dans le calque l�gende */
  protected Map<BCalqueAffichage,BCalqueLegendePanel> cqAffPanels_;
  /**
   * Bloque le rafraichissement en modification d'attribut.
   */
  private boolean lockRefresh_;
  /** Le composant contenant la totalit� des l�gendes, d�placable suivant l'ancre. */
  private final JComponent pnLegendes_;
  GridBagConstraints c_;
  final CtuluHorizontalLayout hLa_;

  /**
   * Constructeur.
   */
  public BCalqueLegende() {
    super();
    cqAffPanels_ = new HashMap();
    setFont(BCalqueLegendePanel.getDefaultFont());
    ancre_ = getSaveAncre();
    setLayout(new GridBagLayout());
    c_ = new GridBagConstraints();
    c_.fill = GridBagConstraints.NONE;
    c_.gridheight = 1;
    c_.gridwidth = 1;
    c_.weightx = 1;
    c_.weighty = 1;
    // layoutPnLegendes_ = new FlowLayout(FlowLayout.LEFT);
    pnLegendes_ = new BuPanel();
    // important pour l'export
    // BorderFactory.createEmptyBorder(2, 2, 2, 2);
    // pnLegendes_.setLayout(layoutPnLegendes_);
    hLa_ = new CtuluHorizontalLayout();
    hLa_.setVfilled(BCalqueLegendePanel.getSaveSameHeight());
    hLa_.setHfilled(false);
    hLa_.setHgap(2);
    pnLegendes_.setLayout(hLa_);
    // Fred a voir
    pnLegendes_.setOpaque(false);
    pnLegendes_.setBackground(Color.WHITE);
    // add(pnLegendes_, BuBorderLayout.SOUTH);
    addPnLeg();
    updateHLayoutPosition();
  }

  private void addPnLeg() {
    if (ancre_ == BSelecteurAncre.BOT_LEFT) {
      c_.anchor = GridBagConstraints.SOUTHWEST;
    } else if (ancre_ == BSelecteurAncre.BOT_RIGHT) {
      c_.anchor = GridBagConstraints.SOUTHEAST;
    } else if (ancre_ == BSelecteurAncre.UP_LEFT) {
      c_.anchor = GridBagConstraints.NORTHWEST;
    } else if (ancre_ == BSelecteurAncre.UP_RIGHT) {
      c_.anchor = GridBagConstraints.NORTHEAST;
    } else if (ancre_ == BSelecteurAncre.UP_CENTRE) {
      c_.anchor = GridBagConstraints.NORTH;
    } else if (ancre_ == BSelecteurAncre.BOT_CENTRE) {
      c_.anchor = GridBagConstraints.SOUTH;
    } else if (ancre_ == BSelecteurAncre.MID_LEFT) {
      c_.anchor = GridBagConstraints.WEST;
    } else if (ancre_ == BSelecteurAncre.MID_RIGHT) {
      c_.anchor = GridBagConstraints.EAST;
    } else if (ancre_ == BSelecteurAncre.MID_CENTRE) {
      c_.anchor = GridBagConstraints.CENTER;
    }
    add(pnLegendes_, c_);
  }

  /**
   * Cr�ation d'un panneau avec titre devant accueillir le composant l�gende du calque d'affichage.
   */
  private JPanel ajoutePanel(final BCalqueAffichage _cq) {
    String title = _cq.getTitle();
    if (title == null) {
      title = _cq.getName();
    }
    if (title == null) {
      title = EbliLib.getS("L�gende") + ' ' + cqAffPanels_.size();
    }
    return ajoutePanel(_cq, title);
  }

  BCalqueAffichageLegendProperties default_ = new BCalqueAffichageLegendProperties();

  private JPanel ajoutePanel(final BCalqueAffichage _cq, final String _titre) {
    if (_cq.getLegende() != this) {
      _cq.setLegende(this);
    }
    updateProperties(_cq);

    String name = _cq.getName();
    if (name == null) {
      name = _cq.getClass().getName().substring(_cq.getClass().getName().lastIndexOf('.'));
    } else {
      name = name.substring(2);
    }
    final BCalqueLegendePanel pnLeg = new BCalqueLegendePanel(_cq, _titre);
    pnLeg.setName("pnLEGENDE_" + name);
    cqAffPanels_.put(_cq, pnLeg);
    addLegendToPanel(pnLeg);
    return pnLeg;
  }

  protected void addLegendToPanel(final BCalqueLegendePanel pnLeg) {
    pnLegendes_.add(pnLeg);
  }

  protected void isRestoreFromProperties(BCalqueAffichage _cq) {
    default_.initFrom(_cq.getLegendProperties());
    super.setFont(_cq.getLegendProperties().getFont());
  }

  private void updateProperties(final BCalqueAffichage _cq) {
    _cq.getLegendProperties().initFrom(default_);
  }

  private void updateHLayoutPosition() {
    hLa_.setHAligment(BSelecteurAncre.isAncreBottom(ancre_) ? SwingConstants.BOTTOM : SwingConstants.TOP);
  }

  protected void addContainerListenerForLegendPanel(final ContainerListener _l) {
    pnLegendes_.addContainerListener(_l);
  }

  /**
   * Ajout d'une palette associ�e � un composant de niveaux.
   */
  public void ajoute(final BCalqueAffichage _cq, final BPaletteCouleurSimple _c, final JComponent _t) {
    ajoute(_cq, _c, _t, null);
  }

  /**
   * @param _cq le calque
   * @param _c la palette pour ce calque
   * @param _t un composant a ajouter a la fin
   * @param _title le titre
   */
  public void ajoute(final BCalqueAffichage _cq, final BPaletteCouleurSimple _c, final JComponent _t,
      final String _title) {
    BCalqueAffichage cq = _cq;
    if (cq == null) {
      cq = this;
    }
    JPanel pnLeg = cqAffPanels_.get(cq);
    if (pnLeg == null) {
      pnLeg = (_title == null ? ajoutePanel(cq) : ajoutePanel(cq, _title));
    }
    final BuGridLayout lyNiv = new BuGridLayout(2, 5, 5, true, true);
    final JPanel pnNiv = new JPanel();
    pnNiv.setLayout(lyNiv);
    pnNiv.setOpaque(false);
    _c.setOrientation(BPaletteCouleurSimple.VERTICAL);
    // JPanel pnNiv=(JPanel)pnLeg.getComponent(1); // A priori, l'ordre dans lequel on a ajout� le
    // composant ??
    // pnNiv.removeAll();
    pnNiv.add(_c, 0);
    pnNiv.add(_t, 1);
    // int n=pnNiv.getComponentCount();
    // pnNiv.add(c, n );
    // pnNiv.add(t, n+1);
    // pnLeg.remove(1);
    pnLeg.add(pnNiv, BorderLayout.CENTER);
    if (!lockRefresh_) {
      repaint();
    }
  }

  /**
   * Permet de construire une legende en specifiant icone, texte. Si la legende pour le calque _cq existe deja,
   * l'icone/texte sera ajoute a la fin du panneau de la legende.
   * 
   * @param _cq le calque concerne peut etre null.
   * @param _i l'icone
   * @param _t le texte
   */
  public void ajoute(final BCalqueAffichage _cq, final Icon _i, final String _t) {
    BCalqueAffichage cq = _cq;
    if (cq == null) {
      cq = this;
    }

    JPanel pnLeg = cqAffPanels_.get(cq);
    if (pnLeg == null) {
      pnLeg = ajoutePanel(cq);
    }
    final BuGridLayout lyNiv = new BuGridLayout(2, 5, 5, true, true);
    final JPanel pnNiv = new JPanel();
    pnNiv.setLayout(lyNiv);
    pnNiv.setOpaque(false);
    pnNiv.add(new JLabel(_i), 0);
    pnNiv.add(new JLabel(_t), 1);
    pnLeg.add(pnNiv, BorderLayout.CENTER);
    if (!lockRefresh_) {
      repaint();
    }
  }

  /**
   * Ajout direct d'une l�gende. Le titre de la l�gende n'est pas cens� exister.
   * 
   * @param _cq le calque d'affichage
   * @param _pn la legende pour ce calque
   */
  public void ajoute(final BCalqueAffichage _cq, final JPanel _pn) {
    ajoute(_cq, _pn, null);
  }

  /**
   * @param _cq le calque
   * @param _pn le panneau de la legende
   * @param _title le titre
   */
  public void ajoute(final BCalqueAffichage _cq, final JPanel _pn, final String _title) {
    JPanel pnLeg = cqAffPanels_.get(_cq);
    if (pnLeg == null) {
      pnLeg = (_title == null ? ajoutePanel(_cq) : ajoutePanel(_cq, _title));
    }
    // pnLeg.remove(1);
    pnLeg.add(_pn, BorderLayout.CENTER);
    pnLegendes_.revalidate();
    if (!lockRefresh_) {
      repaint();
    }
  }

  /**
   * @param _finalPanel la legende a ajouter
   */
  public void ajouteLegendPanel(final BCalqueLegendePanel _finalPanel) {

    if ((_finalPanel != null) && (!cqAffPanels_.containsKey(_finalPanel.aff_))) {
      updateProperties(_finalPanel.aff_);
      cqAffPanels_.put(_finalPanel.aff_, _finalPanel);
      _finalPanel.aff_.setLegende(this);
      addLegendToPanel(_finalPanel);
      pnLegendes_.revalidate();
      if (!lockRefresh_) {
        repaint();
      }
    }
  }

  /**
   * @param _aff le calque d'affichage
   * @return true si la legende de ce calque est utilisee
   */
  public boolean containsLegend(final BCalqueAffichage _aff) {
    return (cqAffPanels_ != null) && (cqAffPanels_.containsKey(_aff));
  }

  /**
   * Suppression de la l�gende du calque sp�cifi�.
   * 
   * @param _cq Le calque pour lequel on veut supprimer la l�gende.
   */
  public void enleve(final BCalqueAffichage _cq) {
    final JPanel p = cqAffPanels_.get(_cq);
    if (p == null) {
      return;
    }
    cqAffPanels_.remove(_cq);
    pnLegendes_.remove(p);
  }

  public int getAncre() {
    return ancre_;
  }

  public BCalqueAffichage getLegendDisplayAtPosition(final int _i) {
    if (_i >= 0 && (_i < pnLegendes_.getComponentCount())) {
      return ((BCalqueLegendePanel) pnLegendes_.getComponent(_i)).aff_;
    }
    return null;
  }

  /**
   * Retourne le composant l�gende associ� au calque.
   * 
   * @param _cq le calque pour lequel la legende est demandee.
   * @return Le composant l�gende sous forme de JPanel.
   */
  public JPanel getLegende(final BCalqueAffichage _cq) {
    return cqAffPanels_.get(_cq);
  }

  public BCalqueLegendePanel getLegendePanel(final int _i) {
    if (_i >= 0 && (_i < pnLegendes_.getComponentCount())) {
      return ((BCalqueLegendePanel) pnLegendes_.getComponent(_i));
    }
    return null;
  }

  public JComponent getMainComponent(final BCalqueAffichage _cq) {
    final BCalqueLegendePanel panel = getLegendePanelFor(_cq);
    if (panel == null) {
      return null;
    }
    return panel.getMainComponent();
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  /**
   * @return le nombre de legendes affichees
   */
  public int getNbLegend() {
    return cqAffPanels_.size();
  }

  /**
   * @return le nombre de legende ajoute au panneau
   */
  public int getNbPanelLegende() {
    return pnLegendes_.getComponentCount();
  }

  /**
   * Si le calque d'affichage n'est pas visible renvoie false.
   * 
   * @param _cq le calque a tester
   * @return true si la panneau affichant ce calque est mis a visible
   */
  public boolean getPanelIsSetToVisible(final BCalqueAffichage _cq) {
    final BCalqueLegendePanel panel = getLegendePanelFor(_cq);
    if (panel == null) {
      return false;
    }
    return panel.isLegendVisible();
  }

  /**
   * @param _cq le calque a tester
   * @return true si reellement affiche
   */
  public boolean getVisible(final BCalqueAffichage _cq) {
    final JPanel panel = cqAffPanels_.get(_cq);
    if (panel == null) {
      return false;
    }
    return panel.isVisible();
  }

  @Override
  public void initFrom(EbliUIProperties _p) {
    super.initFrom(_p);
    if (_p.isDefined("legende.sameHeight")) {
      setSameHeightLegend(_p.getBoolean("legende.sameHeight"));
    }
    if (_p.isDefined("legende.ancre")) {
      setAncre(_p.getInteger("legende.ancre"));
    }
    repaint();

  }

  /**
   * return true.
   */
  public final boolean isFontModifiable() {
    return true;
  }

  /**
   * D�termine si le rafraichissement est bloqu� ou non lors d'un changement d'attribut.
   * 
   * @return true si 'reaffichage' bloque.
   */
  public boolean isRefreshLocked() {
    return lockRefresh_;
  }

  public boolean isSameHeight() {
    return hLa_.getVfilled();
  }

  @Override
  public void paintComponent(final Graphics _g) {}

  /**
   * Affichage de l'icone.
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final boolean attenue = isAttenue();
    Color fg = getForeground();
    Color bg = getBackground();
    Color rouge = Color.red;
    Color bleu = Color.blue;
    Color vert = Color.green;
    if (attenue) {
      fg = attenueCouleur(fg);
      rouge = attenueCouleur(rouge);
      bleu = attenueCouleur(bleu);
      vert = attenueCouleur(vert);
      bg = attenueCouleur(bg);
    }
    final int w = getIconWidth();
    final int h = getIconHeight();
    final int haut = (int) (h * 0.20);
    final int larg = (int) (w * 0.40);
    final int inter = 3;
    int topy = _y + inter;
    _g.setColor(bg);
    _g.fillRect(_x + 1, _y + 1, w - 1, h - 1);
    _g.setColor(bleu);
    _g.fillRect(_x + inter, topy, larg, haut);
    _g.setColor(fg);
    _g.drawLine(_x + inter + larg + inter, topy + haut, _x + inter + w - inter, topy + haut);
    topy += haut + inter;
    _g.setColor(rouge);
    _g.fillRect(_x + inter, topy, larg, haut);
    _g.setColor(fg);
    _g.drawLine(_x + inter + larg + inter, topy + haut, _x + inter + w - inter, topy + haut);
    topy += haut + inter;
    _g.setColor(vert);
    _g.fillRect(_x + inter, topy, larg, haut);
    _g.setColor(fg);
    _g.drawLine(_x + inter + larg + inter, topy + haut, _x + inter + w - inter, topy + haut);
  }

  @Override
  public void paintImage(final Graphics _g) {
    if (!isVisible()) {
      return;
    }
    // important pour eviter que le fond se voit trop
    super.paintImage(_g);
  }

  /**
   * Enleve toutes les l�gendes du calque. Les calques sont dissoci�s de ce calque.
   */
  public void enleveTousCalques() {
    for (BCalqueAffichage cq : cqAffPanels_.keySet()) {
      cq.setLegende(null);
    }
    cqAffPanels_.clear();
    pnLegendes_.removeAll();
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    res.put("legende.sameHeight", hLa_.getVfilled());
    res.put("legende.ancre", ancre_);
    return res;
  }

  /**
   * @param _ancre la nouvelle valeur de l'ancre
   * @see BSelecteurAncre
   */
  public void setAncre(final int _ancre) {
    if (ancre_ == _ancre) {
      return;
    }
    if (!BSelecteurAncre.isValideAncre(_ancre)) {
      return;
    }
    final int vp = ancre_;
    ancre_ = _ancre;
    firePropertyChange("ancre", vp, ancre_);
    removeAll();
    addPnLeg();
    updateHLayoutPosition();
    if (!lockRefresh_) {
      revalidate();
      pnLegendes_.revalidate();
      repaint();
    }
    saveAncre(_ancre);
  }

  public void setAncre(final Integer _i) {
    setAncre(_i.intValue());
  }

  /**
   * Modifie la couleur d'arriere-plan de la legende du calque cq.
   * 
   * @param _cq la calque pour laquelle la legende devra etre modifiee
   * @param _c la nouvelle couleur d'arriere-plan
   */
  public void setBackground(final BCalqueAffichage _cq, final Color _c) {
    JPanel panel = cqAffPanels_.get(_cq);
    if (panel == null) {
      panel = ajoutePanel(_cq);
    }
    panel.setBackground(_c);
    if (!lockRefresh_) {
      repaint();
    }
  }

  @Override
  public final void setFont(final Font _f) {
    if (_f.equals(getFont())) {
      return;
    }
    if ((cqAffPanels_ != null) && (cqAffPanels_.size() > 0)) {
      for (JPanel pn : cqAffPanels_.values()) {
        pn.setFont(_f);
      }
    }
    super.setFont(_f);
    if (pnLegendes_ != null) {
      pnLegendes_.revalidate();
    }
    default_.setFont(_f);
  }

  /**
   * @param _cq le calque a modifier
   * @param _c la nouvelle couleur d'avant-plan pour le calque cq.
   */
  public void setForeground(final BCalqueAffichage _cq, final Color _c) {
    JPanel panel = cqAffPanels_.get(_cq);
    if (panel == null) {
      panel = ajoutePanel(_cq);
    }
    panel.setForeground(_c);
    if (!lockRefresh_) {
      repaint();
    }
  }

  @Override
  public void setForeground(final Color _c) {
    super.setForeground(_c);
    if ((cqAffPanels_ != null) && (cqAffPanels_.size() > 0)) {
      for (JPanel pn : cqAffPanels_.values()) {
        pn.setForeground(_c);
      }
    }
  }

  /**
   * Bloque ou non le rafraichissement du calque lors d'un changement d'attribut.
   * 
   * @param _lock si true pas de rafraichissement
   */
  public void setRefreshLocked(final boolean _lock) {
    lockRefresh_ = _lock;
  }

  public void setSameHeightLegend(final boolean _b) {
    hLa_.setVfilled(_b);
    pnLegendes_.revalidate();
    repaint();
    BCalqueLegendePanel.setSaveSameHeight(_b);

  }

  /**
   * @param _cq le calque pour lequel la visibilite de la legende doit etre modifiee
   * @param _v le nouvel etat pour la legende
   */
  public void setVisible(final BCalqueAffichage _cq, final boolean _v) {
    JPanel panel = cqAffPanels_.get(_cq);
    if (panel == null) {
      panel = ajoutePanel(_cq);
    }
    panel.setVisible(_v);
    if (!lockRefresh_) {
      pnLegendes_.revalidate();
      revalidate();
      repaint();
    }

  }

  /**
   * Appele pour mettre a jour l'affichage.
   */
  public void updateAll() {
    pnLegendes_.revalidate();
    repaint();
  }

  public void updateMainTitle(final BCalqueAffichage _cq, final String _titre) {
    final BCalqueLegendePanel pnLeg = getLegendePanelFor(_cq);
    if (pnLeg != null) {
      pnLeg.updateTitreGlob(_titre);
    }
  }

  protected BCalqueLegendePanel getLegendePanelFor(final BCalqueAffichage _cq) {
    return cqAffPanels_.get(_cq);
  }

  public void setBordEpaisseur(int _ep) {
    for (int i = getNbPanelLegende() - 1; i >= 0; i--) {
      getLegendePanel(i).setBordEpaisseur(_ep);
    }
    repaint();
    default_.setBord(_ep);
  }

  public void setBackgroundForLegend(Color _c) {
    for (int i = getNbPanelLegende() - 1; i >= 0; i--) {
      getLegendePanel(i).setBackground(_c);
    }
    repaint();
    default_.setBackground(_c);
  }

  public void setBordColor(Color _c) {
    for (int i = getNbPanelLegende() - 1; i >= 0; i--) {
      getLegendePanel(i).setBordColor(_c);
    }
    default_.setBorderColor(_c);
    repaint();
  }
  

  

}
