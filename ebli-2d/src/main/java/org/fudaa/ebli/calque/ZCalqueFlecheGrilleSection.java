/*
 * @creation 9 nov. 06
 * 
 * @modification $Date: 2007-06-05 08:58:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurAbstract;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurComposite;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurSpinner;

/**
 * @author fred deniger
 * @version $Id: ZCalqueFlecheGrilleSection.java,v 1.1 2007-06-05 08:58:38 deniger Exp $
 */
public class ZCalqueFlecheGrilleSection extends BCalqueConfigureSectionAbstract {

  public ZCalqueFlecheGrilleSection(final ZCalqueFleche _target) {
    super(_target, EbliLib.getS("Grille"));
  }

  FlecheGrilleData getGrilleData() {
    return ((ZCalqueFleche) target_).grille_;
  }

  private static class SpecComposite extends BSelecteurComposite {

    public SpecComposite(BSelecteurInterface[] _comps) {
      super(_comps, true);
    }

    @Override
    public JComponent[] getComponents() {
      JPanel pn = null;
      pn = new BuPanel(new BuHorizontalLayout(25, false, true));
      JComponent[] cps = comps_[0].getComponents();
      String tooltip = comps_[0].getTooltip();
      cps[0].setToolTipText(tooltip);
      BuPanel pnTmp = new BuPanel(new BuHorizontalLayout(5, false, true));
      pnTmp.add(new BuLabel("x:"));
      pnTmp.add(cps[0]);
      pn.add(pnTmp);
      cps = comps_[1].getComponents();
      tooltip = comps_[1].getTooltip();
      cps[0].setToolTipText(tooltip);
      pnTmp = new BuPanel(new BuHorizontalLayout(5, false, true));
      pnTmp.add(new BuLabel(" y:"));
      pnTmp.add(cps[0]);
      pn.add(pnTmp);
      return BSelecteurAbstract.createComponents(pn);
    }
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final BSelecteurCheckBox cbActive = new BSelecteurCheckBox(FlecheGrilleData.PROP_GRILLE_IS_ACTIVE);
    cbActive.setTitle(EbliLib.getS("Activ�e"));
    cbActive.setTooltip(EbliLib.getS("Activer la grille r�guli�re"));
    cbActive.setAddListenerToTarget(false);
    final BSelecteurCheckBox cbPainted = new BSelecteurCheckBox(FlecheGrilleData.PROP_GRILLE_IS_PAINTED);
    cbPainted.setTitle(EbliLib.getS("Visible"));
    cbPainted.setTooltip(EbliLib.getS("Dessiner la grille r�guli�re"));
    cbPainted.setAddListenerToTarget(false);
    final BSelecteurSpinner tfNbX = new BSelecteurSpinner(FlecheGrilleData.PROP_GRILLE_NB_X, new JSpinner(new SpinnerNumberModel(10, 1, 1000, 1)));
    tfNbX.setTitle(EbliLib.getS("X"));
    tfNbX.setTooltip(EbliLib.getS("Nombre de lignes selon X"));
    tfNbX.setAddListenerToTarget(false);
    final BSelecteurSpinner tfNbY = new BSelecteurSpinner(FlecheGrilleData.PROP_GRILLE_NB_Y, new JSpinner(new SpinnerNumberModel(10, 1, 1000, 1)));
    tfNbY.setTitle(EbliLib.getS("Y"));
    tfNbY.setTooltip(EbliLib.getS("Nombre de lignes selon Y"));
    tfNbY.setAddListenerToTarget(false);
    final BSelecteurComposite cpXY = new SpecComposite(new BSelecteurInterface[] { tfNbX, tfNbY });
    cbPainted.setEnabled(false);
    cpXY.setEnabled(false);
    cpXY.setTitle(EbliLib.getS("Nombre de lignes"));
    cbActive.getCb().addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        final boolean ok = cbActive.getCb().isSelected();
        cbPainted.setEnabled(ok);
        cpXY.setEnabled(ok);

      }

    });

    return new BSelecteurInterface[] { cbActive, cbPainted, cpXY };
  }

  @Override
  public Object getProperty(final String _key) {
    if (FlecheGrilleData.PROP_GRILLE_IS_ACTIVE == _key) {
      return Boolean.valueOf(getGrilleData().isActive());
    }
    if (FlecheGrilleData.PROP_GRILLE_IS_PAINTED == _key) {
      return Boolean.valueOf(getGrilleData().isPaintGrille());
    }
    if (FlecheGrilleData.PROP_GRILLE_NB_X == _key) {
      return new Integer(getGrilleData().getNbXLines());
    }
    if (FlecheGrilleData.PROP_GRILLE_NB_Y == _key) {
      return new Integer(getGrilleData().getNbYLines());
    }
    return null;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (FlecheGrilleData.PROP_GRILLE_IS_ACTIVE == _key) {
      return getGrilleData().setActive(((Boolean) _newProp).booleanValue());
    }
    if (FlecheGrilleData.PROP_GRILLE_IS_PAINTED == _key) {
      return getGrilleData().setPaintGrille(((Boolean) _newProp).booleanValue());
    }
    if (FlecheGrilleData.PROP_GRILLE_NB_X == _key) {
      return getGrilleData().setNbXLines(((Integer) _newProp).intValue());
    }
    if (FlecheGrilleData.PROP_GRILLE_NB_Y == _key) {
      return getGrilleData().setNbYLines(((Integer) _newProp).intValue());
    }
    return false;
  }
}
