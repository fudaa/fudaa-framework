/*
 * @file         BCalqueRosace.java
 * @creation     1998-01-29
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * Un calque d'affichage de rosace. Indication des points cardinaux.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:48 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BCalqueRosace extends BCalqueAffichage {
  public BCalqueRosace() {
    super();
    angleNord_ = Math.PI / 2.;
    setDestructible(false);
  }

  // Icon
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes. En fait, le calque lui-meme.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final boolean attenue = isAttenue();
    Color fg = getForeground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(bg);
    _g.fillOval(_x + 3, _y + 3, w - 6, h - 6);
    _g.setColor(fg);
    _g.drawOval(_x + 3, _y + 3, w - 6, h - 6);
    _g.drawLine(_x + w / 2, _y + 1, _x + w / 2, _y + h - 1);
    _g.drawLine(_x + 1, _y + h / 2, _x + w - 1, _y + h / 2);
  }

  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    final boolean rapide = isRapide();
    final boolean attenue = isAttenue();
    Color fg = getForeground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    // FontMetrics fm=_g.getFontMetrics();
    // Dimension d=getSize();
    final Insets insets = getInsets();
    final double rd = 30D;
    final int ri = (int) rd;
    final int xc = insets.left + 10 + ri;
    final int yc = insets.top + 10 + ri;
    final double angle = getAngleNord();
    final double cosa = Math.cos(angle);
    final double sina = Math.sin(angle);
    final GrMorphisme versEcran = getVersEcran();
    final GrVecteur rn = new GrVecteur(cosa, sina, 0.);
    final GrVecteur en = rn.applique(versEcran).normalise().multiplication(rd);
    _g.setColor(bg);
    _g.fillOval(xc - ri + 6, yc - ri + 6, 2 * ri - 12, 2 * ri - 12);
    _g.setColor(fg);
    _g.drawOval(xc - ri + 6, yc - ri + 6, 2 * ri - 12, 2 * ri - 12);
    _g.drawLine(xc, yc, xc + (int) en.x_, yc + (int) en.y_);
    _g.setColor(bg.darker());
    if (!rapide) {
      final GrVecteur re = new GrVecteur(sina, -cosa, 0.);
      final GrVecteur rs = new GrVecteur(-cosa, -sina, 0.);
      final GrVecteur rw = new GrVecteur(-sina, cosa, 0.);
      final GrVecteur ee = re.applique(versEcran).normalise().multiplication(rd);
      final GrVecteur es = rs.applique(versEcran).normalise().multiplication(rd);
      final GrVecteur ew = rw.applique(versEcran).normalise().multiplication(rd);
      _g.drawLine(xc, yc, xc + (int) ee.x_, yc + (int) ee.y_);
      _g.drawLine(xc, yc, xc + (int) es.x_, yc + (int) es.y_);
      _g.drawLine(xc, yc, xc + (int) ew.x_, yc + (int) ew.y_);
    }
    super.paintComponent(_g);
  }
  // Proprietes
  private double angleNord_;

  /**
   * Accesseur de la propriete <I>angleNord</I>. L'orientation du Nord en radins.
   */
  public double getAngleNord() {
    return angleNord_;
  }

  /**
   * Affectation de la propriete <I>angleNord</I>.
   */
  public void setAngleNord(final double _angleNord) {
    if (angleNord_ != _angleNord) {
      final double vp = angleNord_;
      angleNord_ = _angleNord;
      firePropertyChange("angleNord", vp, angleNord_);
      repaint();
    }
  }
}
