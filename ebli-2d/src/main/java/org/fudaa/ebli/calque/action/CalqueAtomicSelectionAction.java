/*
 * @creation 11 sept. 06
 * @modification $Date: 2008-05-13 12:10:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action de selection atomique.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class CalqueAtomicSelectionAction extends EbliActionChangeState implements TreeSelectionListener {

  private final transient ZScene scene_;

  /**
   * Cr�ation de l'action.
   */
  public CalqueAtomicSelectionAction(final ZScene _scn) {
    super(EbliLib.getS("S�lection mode sommet"), EbliResource.EBLI.getToolIcon("draw-atom"), "MODE_ATOME");
    scene_ = _scn;
  }

  @Override
  public void changeAction() {
    scene_.setSelectionMode(SelectionMode.ATOMIC);
  }

  /**
   * Changement de calque selectionn�.
   */
  @Override
  public void valueChanged(TreeSelectionEvent e) {
    updateState();
  }

  public void updateState() {
    setEnabled(scene_.canUseAtomicMode(scene_.getCalqueActif()));
    setSelected(isEnabled() && scene_.getSelectionMode() == SelectionMode.ATOMIC);
  }
}