package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Classe qui gere les diff�rents fichiers pour la persistance du calque. Doit recevoir en parametre le path du
 * repertoire dans lequel sera sauvegard� le calque.
 * 
 * @author Adrien Hadoux
 */
public class ZebliCalquePersist {

  /**
   * Path du repertoire dans lequel seront enregistr� toute les courbes, groupes et model...
   */
  protected String directoryPath_;

  protected XStream parser_;

  protected GrBoite zoom_;

  /**
   * Fichier principal qui contient une instance de la classe ZEbliCalquesPanelPersistenceManager
   */
  private static String MAINFILE = "descriptorCalque.xml";
  private static String DATA = "datas.xml";
  private static String SPECIFIQUEFILE = "SPECIFICDATAS";

  public ZebliCalquePersist(String directoryPath) {
    directoryPath_ = directoryPath;
  }

  /**
   * Recupere le parser adequat.
   * 
   * @return
   */
  protected XStream getParser() {
    if (parser_ == null) parser_ = initXmlParser();
    return parser_;
  }

  /**
   * Init le parser avec les alias adequats.
   * 
   * @return
   */
  protected XStream initXmlParser() {
    XStream xstream = new XStream(new DomDriver());
    XStream.setupDefaultSecurity(xstream);
    xstream.allowTypesByWildcard(new String[] {
            "org.fudaa.**"
    });
    return xstream;
  }

  /**
   * Path vers els datas classique du calque
   * 
   * @return
   */
  protected String getDataFilePath() {
    return directoryPath_ + File.separator + DATA;
  }

  /**
   * Retourne le path vers le fiochier principal des courbes
   * 
   * @return
   */
  protected String getMainFilePath() {
    return directoryPath_ + File.separator + MAINFILE;
  }

  /**
   * Retourne le chemin vers le fichier de datats specifiques.
   * 
   * @return
   */
  protected String getSpecifiqueDataFilePath() {
    return directoryPath_ + File.separator + SPECIFIQUEFILE;
  }

  /**
   * Enrtegistre le calque au format persistant xml.
   * 
   * @param parameters des parametres supplementaires utiles.
   * @throws IOException
   */
  public void savePersitCalqueXml(ZEbliCalquesPanel panelTopersist, Map params) {

    ObjectOutputStream out = null;
    try {
      out = EbliLib.createObjectOutpuStream(new File(getMainFilePath()), getParser());

      // -- recuperation des datats persistantes specifiques au panel --//
      ZEbliCalquesPanelPersistManager dataPersistantes = panelTopersist.getPersistenceManager();

      // -- enregistrement des don�nes sp�cifiques du calque -//
      // TODO je prefere que fillInfoWith renvoie un objet et de donnees et c'est ce dernier qui est sauve
      // dans le out, il suffit d'ecrire la nom de classe du persistManager
      dataPersistantes.fillInfoWith(panelTopersist, new File(getSpecifiqueDataFilePath()));

      // -- sauvegarde de la classe dataPersistantes dans le fichier specifiqueData
      out.writeObject(dataPersistantes);

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        out.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Methode qui remplit le panel fournit en parametre avec les donn�es sauvegardees.
   * 
   * @param panelToFill
   */
  public ZEbliCalquesPanel loadPersitCalqueXml(Map param) {

    ObjectInputStream in = null;
    ZEbliCalquesPanel veritablePanel = null;

    try {

      // -- etape 1: on lit les donn�es sp�cifiques pour avoir la data persistantes
      in = EbliLib.createObjectInpuStream(new File(getMainFilePath()), getParser());
      ZEbliCalquesPanelPersistManager dataPersistantes = (ZEbliCalquesPanelPersistManager) in.readObject();
      zoom_ = dataPersistantes.getZoom();
      // -- etape 2 on creer l'obejt calque correspondant a partir des datas persitantes
      veritablePanel = dataPersistantes.generateCalquePanel(param, new File(getSpecifiqueDataFilePath()));

    } catch (Exception _e) {
      FuLog.error(_e);
    } finally {
      try {
        in.close();
      } catch (IOException _e) {
        FuLog.error(_e);
      }
    }

    return veritablePanel;
  }

  public GrBoite getZoom() {
    return zoom_;
  }

}
