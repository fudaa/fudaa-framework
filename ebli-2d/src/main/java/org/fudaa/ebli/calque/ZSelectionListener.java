/*
 * @file         ZSelectionListener.java
 * @creation     2002-10-02
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.util.EventListener;
/**
 * Une interface auditeur des changements de selections d'objets sur un calque. La classe qui l'implemente ecoute l'evenement 
 * {@link SelectionEvent}.
 *
 * @version      $Id$
 * @author       Bertrand Marchand 
 */
public interface ZSelectionListener extends EventListener {
  
  /**
   * Methode appel�e lors d'un changement de selection d'objet sur un calque.
   * @param _evt L'evenement de selection.
   */
  void selectionChanged(ZSelectionEvent _evt);
}
