/*
 *  @creation     26 mai 2005
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

/**
 * Un visiteur de calque. En g�n�ral, la visite se fait sur un ensemble de calques.
 * A chaque nouvelle visite, les informations pertinentes sont enregistr�es dans le
 * visiteur.
 * 
 * @author Fred Deniger
 * @version $Id: BCalqueVisitor.java,v 1.2 2006-04-12 15:27:09 deniger Exp $
 */
public interface BCalqueVisitor {
  
  /**
   * Visite le calque pass� en argument et stocke les informations pertinentes.
   * @param _cq Le calque a visiter.
   * @return true : Le processus de visite peut se poursuivre sur le reste des calques.<br>
   * false : Fin des visites.
   */
  boolean visit(BCalque _cq);
}
