/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import gnu.trove.TIntObjectIterator;
import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZCalqueLigneBriseeEditable extends ZCalqueLigneBrisee implements ZCalqueEditable {

  ZEditorInterface editor_;
  /**
   * Les formes autoris�e par le calque
   */
  protected int[] enabledForms_ = {DeForme.POLYGONE, DeForme.LIGNE_BRISEE, DeForme.RECTANGLE, DeForme.ELLIPSE};

  public ZCalqueLigneBriseeEditable() {
    super();
  }

  public ZCalqueLigneBriseeEditable(final ZModeleLigneBriseeEditable _modele, final ZEditorDefault _editor) {
    modele_ = _modele;
    if (modele_ != null) {
      modele_.addModelListener(this);
    }
    editor_ = _editor;
  }

  @Override
  public String editSelected() {
    return editor_ == null ? EbliLib.getS("Edition impossible") : editor_.edit();
  }

  /**
   * Scinde en 2 une polyligne dont deux sommets cons�cutifs sont s�lectionn�. Les attributs sont copi�s.
   *
   * @param _cmd La pile de commandes.
   * @return true si le la polyligne a �t� scind�e.
   */
  @Override
  public boolean splitSelectedObject(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty() || (getSelectionMode() != SelectionMode.ATOMIC)) {
      return false;
    }

    int idxLine = selectionMulti_.getIdxSelected()[0];
    int[] idxSels = selectionMulti_.get(idxLine).getSelectedIndex();

    // On vide la selection, les 2 lignes sont supprim�es par la jonction.
    clearSelection();
    return modeleDonnees().splitGeometry(idxLine, idxSels, _cmd) != null;
  }

  /**
   * Joint 2 polylignes par leurs sommets d�sign�s.
   *
   * @param _cmd La pile de commandes.
   * @return true si les polylignes ont �t� jointes.
   */
  @Override
  public boolean joinSelectedObjects(final CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty() || (getSelectionMode() != SelectionMode.ATOMIC)) {
      return false;
    }

    int[] idxLines = new int[2];
    int[] idxSels = new int[2];
    for (int i = 0; i < 2; i++) {
      idxLines[i] = selectionMulti_.getIdxSelected()[i];
      idxSels[i] = selectionMulti_.get(idxLines[i]).getSelectedIndex()[0];
    }

    // On vide la selection, les 2 lignes sont supprim�es par la jonction.
    clearSelection();
    return modeleDonnees().joinGeometries(idxLines, idxSels, _ui, _cmd) != -1;
  }

  private boolean removeAtomicObjects(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    return modeleDonnees().removeAtomicObjects(selectionMulti_, _cmd, _ui);
  }

  protected void paintDeplacementMulti(final Graphics2D _g, final int _dxEcran, final int _dyEcran,
          final TraceIcon _trace) {
    final GrBoite clip = getClipReel(_g);
    if (!getDomaine().intersectXY(clip)) {
      return;
    }
    Color cs = _trace.getCouleur();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TIntObjectIterator it = selectionMulti_.getIterator();
    final GrPoint p = new GrPoint();
    final GrMorphisme versEcran = getVersEcran();
    final TraceLigne tl = ligneModel_.buildCopy();
    tl.setCouleur(cs);
    for (int i = selectionMulti_.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        final int idxPoly = it.key();
        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            modele_.point(p, idxPoly, j);

            p.autoApplique(versEcran);
            final int xPivot = (int) (p.x_ + _dxEcran);
            final int yPivot = (int) (p.y_ + _dyEcran);
            _trace.paintIconCentre(this, _g, xPivot, yPivot);
            // trace de la ligne j-1 ->j
            int j0 = j - 1;
            if (j0 < 0 && modele_.isGeometryFermee(idxPoly)) {
              j0 = modele_.getNbPointForGeometry(idxPoly) - 1;
            }
            if (j0 >= 0) {
              modele_.point(p, idxPoly, j0);
              p.autoApplique(versEcran);
              if (s.isSelected(j0)) {
                p.x_ += _dxEcran;
                p.y_ += _dyEcran;
              }
              tl.dessineTrait(_g, xPivot, yPivot, p.x_, p.y_);
            }
            j0 = j + 1;
            if (j0 == modele_.getNbPointForGeometry(idxPoly) && modele_.isGeometryFermee(idxPoly)) {
              j0 = 0;
            }
            if (j0 < modele_.getNbPointForGeometry(idxPoly)) {
              modele_.point(p, idxPoly, j0);
              p.autoApplique(versEcran);
              if (s.isSelected(j0)) {
                p.x_ += _dxEcran;
                p.y_ += _dyEcran;

              }
              tl.dessineTrait(_g, xPivot, yPivot, p.x_, p.y_);
            }
          }

        }
      }
    }
  }

  @Override
  public boolean addAtomeProjectedOnSelectedGeometry(GrPoint _ptReel, CtuluCommandComposite cmp, CtuluUI _ui) {
    int[] selectedIndex = getSelectedIndex();
    if (selectedIndex == null || selectedIndex.length != 1) {
      return false;
    }
    GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    final int geomIdx = selectedIndex[0];
    double distance = -1;
    GrPoint projection = null;
    for (int j = modele_.getNbPointForGeometry(geomIdx) - 1; j > 0; j--) {
      modele_.point(seg.e_, geomIdx, j);
      modele_.point(seg.o_, geomIdx, j - 1);
      GrPoint localeProjection = seg.projectionXY(_ptReel);
      if (localeProjection != null) {
        double newDis = localeProjection.distanceXY(_ptReel);
        if (projection == null || newDis < distance) {
          projection = localeProjection;
          distance = newDis;
        }
      }

    }
    if (modele_.isGeometryFermee(geomIdx)) {
      modele_.point(seg.e_, geomIdx, 0);
      modele_.point(seg.o_, geomIdx, modele_.getNbPointForGeometry(geomIdx) - 1);
      GrPoint localeProjection = seg.projectionXY(_ptReel);
      if (localeProjection != null) {
        double newDis = localeProjection.distanceXY(_ptReel);
        if (projection == null || newDis < distance) {
          projection = localeProjection;
          distance = newDis;
        }
      }

    }
    if (projection != null) {
      return addAtomeOnLine(geomIdx, projection, cmp);
    }
    return false;

  }

  /**
   * Ajoute un sommet a l'objet selectionn�. Le sommet peut �tre ajout� en bout de ligne, si un point extremit� est selectionn�, ou bien inser� si 2
   * sommets s�lectionn�s.
   *
   * Si le nombre d'objets selectionn�s est diff�rent de 1, le point n'est pas ajout�.
   *
   * @param _ptReel le point 3D r�el de l'atome a ajouter
   * @param _cmd le receveur de command
   * @param _ui l'interface utilisateur
   * @return true si atome ajoute
   */
  @Override
  public boolean addAtome(final GrPoint _ptReel, final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (getNbSelected() != 1) {
      return false;
    }
//    if (getSelectionMode() != SelectionMode.NORMAL || getNbSelected()!=1) return false;

    if (isAtomicMode()) {
      int idxGeom = selectionMulti_.getIdxSelected()[0];
      int idxBefore;

      if (selectionMulti_.getNbSelectedItem() == 1) {
        // Cas d'un polygone => Interdit d'ajouter un point en fin de g�om�trie.
        if (modeleDonnees().isGeometryFermee(idxGeom)) {
          return false;
        }

        if (selectionMulti_.getSelection(idxGeom).getMinIndex() == 0) {
          idxBefore = -1;
        } else if (selectionMulti_.getSelection(idxGeom).getMaxIndex() == modeleDonnees().getGeomData().getGeometry(idxGeom).getNumPoints() - 1) {
          idxBefore = selectionMulti_.getSelection(idxGeom).getMaxIndex();
        } else {
          return false;
        }
      } else if (selectionMulti_.getNbSelectedItem() == 2) {
        if (selectionMulti_.getSelection(idxGeom).getMaxIndex() == selectionMulti_.getSelection(idxGeom).getMinIndex() + 1) {
          idxBefore = selectionMulti_.getSelection(idxGeom).getMinIndex();
        } else {
          return false;
        }
      } else {
        return false;
      }

      modeleDonnees().addPoint(idxGeom, idxBefore, _ptReel.x_, _ptReel.y_, _ptReel.z_, _cmd);
      repaint();
      return true;
    } // En mode non atomique, le point ajout� est obligatoirement sur la polyligne.
    else {
      return addAtomeOnLine(selection_.getMinIndex(), _ptReel, _cmd);
    }
  }

  @Override
  public boolean addForme(final GrObjet _o, final int _deforme, final CtuluCommandContainer _cmd, final CtuluUI _ui,
          final ZEditionAttributesDataI _data) {
    if (_deforme == DeForme.POLYGONE) {
      return modeleDonnees().addGeometry((GrPolygone) _o, _cmd, _ui, _data);
    } else if (_deforme == DeForme.LIGNE_BRISEE) {
      return modeleDonnees().addGeometry((GrPolyligne) _o, _cmd, _ui, _data);
    }
    return false;
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    for (int typeForme : enabledForms_) {
      if (_typeForme == typeForme) {
        return true;
      }
    }
    return false;
  }

  /**
   * Definit les formes autoris�es par le calque.
   *
   * @param _forms Les formes.
   * @see DeForme
   */
  public void setFormeEnable(int[] _forms) {
    enabledForms_ = _forms;
  }

  @Override
  public boolean canUseAtomicMode() {
    return true;
  }

  @Override
  public boolean canUseSegmentMode() {
    return true;
  }

  @Override
  public final ZEditorInterface getEditor() {
    return editor_;
  }

  @Override
  public ZModeleLigneBriseeEditable getModelEditable() {
    return modeleDonnees();
  }

  /**
   * @deprecated Use {@link #modeleDonnees()} instead
   */
  public ZModeleLigneBriseeEditable getModelePoly() {
    return modeleDonnees();
  }

  @Override
  public ZModeleLigneBriseeEditable modeleDonnees() {
    return (ZModeleLigneBriseeEditable) modele_;
  }

  @Override
  public void modele(final ZModeleLigneBrisee _modele) {
    if (_modele instanceof ZModeleLigneBriseeEditable) {
      super.modele(_modele);
    } else {
      throw new IllegalArgumentException("bad model");
    }
  }

  @Override
  public boolean moveSelectedObjects(final double _reelDx, final double _reelDy, final double _reelDz,
          final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (!isSelectionEmpty()) {
      // TODO Voir si l'action se fait qu'en mode atomique (comme mnt) ou dans tout mode diff�rent du mode normal.
      // if (isAtomicMode()) {
      if (getSelectionMode() == SelectionMode.ATOMIC) {
        return modeleDonnees().moveAtomic(selectionMulti_, _reelDx, _reelDy, _reelDz, _cmd, _ui);
      } else {
        return modeleDonnees().moveGlobal(selection_, _reelDx, _reelDy, _reelDz, _cmd);
      }
    }

    return false;
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#rotateSelectedObjects(double, double, double,
   * org.fudaa.ctulu.CtuluCommandContainer, org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public boolean rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd,
          CtuluUI _ui) {
    if (!isSelectionEmpty()) {
      // TODO Voir si l'action se fait qu'en mode atomique (comme mnt) ou dans tout mode diff�rent du mode normal.
      // if (isAtomicMode()) {
      if (getSelectionMode() == SelectionMode.ATOMIC) {
        return modeleDonnees().rotateAtomic(selectionMulti_, _angRad, _xreel0, _yreel0, _cmd, _ui);
      } else {
        return modeleDonnees().rotateGlobal(selection_, _angRad, _xreel0, _yreel0, _cmd);
      }
    }
    return false;
  }

  /*
   * @see org.fudaa.ebli.calque.edition.ZCalqueEditable#copySelectedObjects(org.fudaa.ctulu.CtuluCommandContainer,
   * org.fudaa.ctulu.CtuluUI)
   */
  @Override
  public boolean copySelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isSelectionEmpty() || (getSelectionMode() != SelectionMode.NORMAL)) {
      return false;
    }
    int nb = modeleDonnees().getNombre();

    GrSegment pdec = new GrSegment(new GrPoint(0, 5, 0), new GrPoint(5, 0, 0));
    pdec.autoApplique(getVersReel());
    if (!modeleDonnees().copyGlobal(selection_, 0/* pdec.getVx() */, 0/* pdec.getVy() */, _cmd)) {
      return false;
    }

    // Changement de selection.
    int[] isels = new int[selection_.getNbSelectedIndex()];
    for (int i = 0; i < isels.length; i++) {
      isels[i] = nb + i;
    }
    setSelection(isels);
    return true;
  }

  @Override
  public void paintDeplacement(final Graphics2D _g2d, final int _dxEcran, final int _dyEcran, final TraceIcon _trace) {
    if (!isSelectionEmpty()) {
      // TODO Voir si l'action se fait qu'en mode atomique (comme mnt) ou dans tout mode diff�rent du mode normal.
      // if (isAtomicMode()) {
      if (getSelectionMode() == SelectionMode.ATOMIC) {
        paintDeplacementMulti(_g2d, _dxEcran, _dyEcran, _trace);
      } else {
        paintDeplacementSimple(_g2d, _dxEcran, _dyEcran, _trace);
      }
    }
  }

  public void paintDeplacementSimple(final Graphics2D _g2d, final int _dx, final int _dy, final TraceIcon _ic) {
    final GrBoite clip = getClipReel(_g2d);
    if (!getDomaine().intersectXY(clip)) {
      return;
    }
    final GrMorphisme versEcran = getVersEcran();
    Color cs = _ic.getCouleur();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g2d.setColor(cs);
    final TraceLigne tlSelection = ligneModel_.buildCopy();
    tlSelection.setCouleur(cs);
    final int nb = selection_.getMaxIndex();
    final GrBoite bPoly = new GrBoite();
    bPoly.e_ = new GrPoint();
    bPoly.o_ = new GrPoint();
    for (int i = nb; i >= 0; i--) {
      if (!selection_.isSelected(i)) {
        continue;
      }
      modele_.getDomaineForGeometry(i, bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }
      final int nbPoints = modele_.getNbPointForGeometry(i);
      final GrPoint ptOri = new GrPoint();
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(versEcran);
      _ic.paintIconCentre(this, _g2d, ptOri.x_ + _dx, ptOri.y_ + _dy);
      final GrPoint ptDest = new GrPoint();
      for (int j = nbPoints - 2; j >= 0; j--) {
        // le point de dest est initialise
        modele_.point(ptDest, i, j);
        ptDest.autoApplique(versEcran);
        _ic.paintIconCentre(this, _g2d, ptDest.x_ + _dx, ptDest.y_ + _dy);
        tlSelection.dessineTrait(_g2d, ptOri.x_ + _dx, ptOri.y_ + _dy, ptDest.x_ + _dx, ptDest.y_ + _dy);
        ptOri.initialiseAvec(ptDest);
      }
      if (modele_.isGeometryFermee(i)) {
        modele_.point(ptOri, i, nbPoints - 1);
        ptOri.autoApplique(versEcran);
        tlSelection.dessineTrait(_g2d, ptOri.x_ + _dx, ptOri.y_ + _dy, ptDest.x_ + _dx, ptDest.y_ + _dy);
      }
    }
  }

  @Override
  public boolean removeSelectedObjects(final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (isSelectionEmpty()) {
      return false;
    }
    boolean r = false;
    // TODO Voir si l'action se fait qu'en mode atomique (comme mnt) ou dans tout mode diff�rent du mode normal.
    // if (isAtomicMode()) {
    if (getSelectionMode() == SelectionMode.ATOMIC) {
      r = removeAtomicObjects(_cmd, _ui);
    } else {
      final int[] idx = selection_.getSelectedIndex();
      super.clearSelection();
      r = modeleDonnees().removeLigneBrisee(idx, _cmd);
    }
    if (r) {
      super.clearSelection();
      repaint();
    }
    return r;
  }

  @Override
  public final void setEditor(final ZEditorInterface _editor) {
    editor_ = _editor;
  }

  /**
   * ajout un atome
   *
   * @param _ptReel le poin
   * @param geomIdx l'indice de la ligne
   * @param _cmd
   * @return
   */
  private boolean addAtomeOnLine(int geomIdx, final GrPoint _ptReel, final CtuluCommandContainer _cmd) {
    final GrMorphisme versEcran = getVersEcran();
    final GrMorphisme versReel = getVersReel();
    final GrBoite bClip = getDomaine();
    bClip.autoApplique(versEcran);
    final int tolerance = 5;
    // Point � projeter
    // final GrPoint pt = new GrPoint(_xEcran, _yEcran, 0);
    final GrPoint ptEcran = _ptReel.applique(versEcran);
    if ((!bClip.contientXY(ptEcran)) && (bClip.distanceXY(ptEcran) > tolerance)) {
      return false;
    }

    final double distanceReel = GrMorphisme.convertDistanceXY(versReel, tolerance);
    // final GrPoint _ptReel = pt.applique(versReel);
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    final GrBoite bPoly = new GrBoite(new GrPoint(), new GrPoint());

    modele_.getDomaineForGeometry(geomIdx, bPoly);
    if (bPoly.contientXY(_ptReel) || bPoly.distanceXY(_ptReel) < distanceReel) {
      for (int j = modele_.getNbPointForGeometry(geomIdx) - 1; j > 0; j--) {
        modele_.point(seg.e_, geomIdx, j);
        modele_.point(seg.o_, geomIdx, j - 1);
        if (seg.distanceXY(_ptReel) < distanceReel) {
          GrPoint ptOnSeg = seg.pointPlusProcheXY(_ptReel);
          modeleDonnees().addPoint(geomIdx, j - 1, ptOnSeg.x_, ptOnSeg.y_, null, _cmd);
          atomAdded(geomIdx, j - 1);
          return true;
        }
      }
      if (modele_.isGeometryFermee(geomIdx)) {
        modele_.point(seg.e_, geomIdx, 0);
        modele_.point(seg.o_, geomIdx, modele_.getNbPointForGeometry(geomIdx) - 1);
        if (seg.distanceXY(_ptReel) < distanceReel) {
          GrPoint ptOnSeg = seg.pointPlusProcheXY(_ptReel);
          final int idxOnLine = modele_.getNbPointForGeometry(geomIdx) - 1;
          modeleDonnees().addPoint(geomIdx, idxOnLine, ptOnSeg.x_, ptOnSeg.y_, null, _cmd);
          atomAdded(geomIdx, idxOnLine);
          return true;
        }
      }
    }
    return false;
  }

  protected void atomAdded(int geomIdx, int idxOneLine) {

  }
}
