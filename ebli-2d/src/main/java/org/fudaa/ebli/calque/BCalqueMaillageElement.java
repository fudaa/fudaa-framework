/*
 * @file         BCalqueMaillageElement.java
 * @creation     1999-10-22
 * @modification $Date: 2007-06-05 08:58:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.List;
import java.util.Vector;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrElement;
import org.fudaa.ebli.geometrie.GrMaillageElement;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrNoeud;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TracePoint;
/**
 * Un calque d'affichage de maillages �l�mentaires.
 *
 * @version      $Revision: 1.10 $ $Date: 2007-06-05 08:58:38 $ by $Author: deniger $
 * @author
 */
public class BCalqueMaillageElement extends BCalqueAffichage {
  protected List maillages_;
  //  protected GrMaillageElement maillageEcran_;
  private GrBoite boite_;
  protected boolean elementsVisibles_;
  protected boolean noeudsVisibles_;
  public BCalqueMaillageElement() {
    super();
    maillages_= new Vector();
    //    maillage_      =null;
    //    maillageEcran_ =null;
    boite_= null;
    elementsVisibles_= true;
    noeudsVisibles_= false;
  }
  // Icon
  @Override
  public void paintIcon(final Component _c, final Graphics _g, int _x, int _y) {
    super.paintIcon(_c, _g, _x, _y);
    _g.translate(_x, _y);
    final int w= getIconWidth();
    final int h= getIconHeight();
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    _g.setColor(fg);
    for (int i= 2; i < w - 5; i += 4) {
      for (int j= 2; j < h - 5; j += 4) {
        final int[] vx= new int[] { i, i + 4, i + 4, i };
        final int[] vy= new int[] { j, j, j + 4, j + 4 };
        _g.drawPolygon(vx, vy, 4);
      }
    }
    _g.translate(-_x, -_y);
  }
  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    if (maillages_.size() == 0) {
      super.paintComponent(_g);
      return;
    }
    if (boite_ == null) {
      boite_= getDomaine();
    }
    if (boite_.isIndefinie()) {
      return;
    }
    final GrMorphisme versEcran= getVersEcran();
    final Polygon pecr= boite_.enPolygoneXY().applique(versEcran).polygon();
    final Rectangle clip= _g.getClipBounds();
    Color ce= getForeground();
    if (isAttenue()) {
      ce= attenueCouleur(ce);
    }
    Color cn= new Color(0, 200, 0);
    if (isAttenue()) {
      cn= attenueCouleur(cn);
    }
    if (clip.intersects(pecr.getBounds())) {
      for (int k= 0; k < maillages_.size(); k++) {
        final GrMaillageElement maillage= (GrMaillageElement)maillages_.get(k);
        // Trac� des �l�ments de bord
        if (isRapide()) {
          _g.setColor(ce);
          final GrElement[][] elsContours= maillage.aretesContours();
          for (int i= 0; i < elsContours.length; i++) {
            for (int j= 0; j < elsContours[i].length; j++) {
              final Polygon p= elsContours[i][j].applique(versEcran).polygon();
              if (clip.intersects(p.getBounds())) {
                _g.drawPolygon(p);
              }
            }
          }
          //          _g.drawPolygon(pecr);
        } else {
          //          if(maillageEcran_==null)
          final GrMaillageElement maillageEcran= maillage.applique(versEcran);
          // Elements visibles
          if (elementsVisibles_) {
            _g.setColor(ce);
            final int n= maillageEcran.getNombre();
            for (int i= 0; i < n; i++) {
              final Polygon p= maillageEcran.element(i).polygon();
              if (clip.intersects(p.getBounds())) {
                _g.drawPolygon(p);
              }
            }
          }
          // Noeuds visibles
          if (noeudsVisibles_) {
            final GrNoeud[] nds= maillageEcran.noeuds();
            final TracePoint tp= new TracePoint();
            tp.setTypePoint(TracePoint.CROIX);
            tp.setTaillePoint(2);
            tp.setCouleur(cn);
            final int n= nds.length;
            final Graphics2D g2d=(Graphics2D)_g;
            for (int i= 0; i < n; i++) {
              GrPoint pt;
              pt= nds[i].point_;
              tp.dessinePoint(g2d,(int)pt.x_, (int)pt.y_);
            }
          }
        }
      }
    }
    super.paintComponent(_g);
  }
  /**
   * Reinitialise la liste de maillages.
   */
  public void reinitialise() {
    maillages_= new Vector();
    boite_= null;
  }
  /**
   * Ajoute un maillage a la liste de maillages de ce calque.
   */
  public void ajoute(final GrMaillageElement _mail) {
    maillages_.add(_mail);
    boite_= null;
  }
  /**
   * Retire un maillage de la liste de maillages de ce calque.
   */
  public void enleve(final GrMaillageElement _mail) {
    maillages_.remove(_mail);
    boite_= null;
  }
  /**
   * Retire tous les  maillages.
   */
  public void enleveTous() {
    maillages_.clear();
    boite_= null;
  }
  /**
   * Rend les �l�ments visibles.
   */
  public void setElementsVisibles(final boolean _etat) {
    elementsVisibles_= _etat;
  }
  public boolean isElementsVisibles() {
    return elementsVisibles_;
  }
  /**
   * Rend les noeuds visibles.
   */
  public void setNoeudsVisibles(final boolean _etat) {
    noeudsVisibles_= _etat;
  }
  public boolean isNoeudsVisibles() {
    return noeudsVisibles_;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  @Override
  public GrBoite getDomaine() {
    if (boite_ == null && maillages_.size() > 0) {
      boite_= new GrBoite();
      for (int i= 0; i < maillages_.size(); i++) {
        final GrBoite b= ((GrMaillageElement)maillages_.get(i)).boite();
        if (b != null) {
          boite_.ajuste(b);
        }
      }
    }
    return boite_;
  }
  // Propriete heritee versEcran pour calcul du maillage seulement si le
  // morphisme a chang�
  //  public void setVersEcran(GrMorphisme _v) {
  //    maillageEcran_=null;
  //    super.setVersEcran(_v);
  //  }
  // Propriete maillage
  //  protected GrMaillageElement maillage_;
  /**
   * Utiliser getMaillages() � la place.
   * @deprecated
   */
  public GrMaillageElement getMaillage() {
    return (GrMaillageElement)maillages_.get(0);
  }
  public List getMaillages() {
    return maillages_;
  }
  /**
   * Utiliser ajoute() ou enleve() � la place.
   * @deprecated
   */
  public void setMaillage(final GrMaillageElement _mail) {
    maillages_.clear();
    maillages_.add(_mail);
    boite_= null;
    /*    if(maillage_!=_maillage)
        {
          GrMaillageElement vp=maillage_;
          maillage_=_maillage;
          boite_=null;
          maillageEcran_=null;

          firePropertyChange("maillage",vp,maillage_);
          repaint();
        } */
  }
}
