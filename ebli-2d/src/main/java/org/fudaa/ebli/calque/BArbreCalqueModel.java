/**
 * @creation 15 sept. 2003
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuCheckBoxMenuItem;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.calque.action.TreeDeleteCalqueAction;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author deniger
 * @version $Id$
 */
public class BArbreCalqueModel implements TreeModel, PropertyChangeListener, ActionListener, ContainerListener {

  public static final String LAYER_ADDED = "added";
  public static final String LAYER_REMOVED = "removed";
  private static final Object[] PROP_TO_LISTEN = CtuluLibArray.sort(
          new String[]{"foreground", "visible", "attenue", "userVisible",
            "rapide", "ajustement", "paletteCouleur", "iconeChanged", "title", "nodeEdit", "alpha", "model"});

  private static String getActLast() {
    return "DERNIER";
  }

  private static String getActDown() {
    return "DESCENDRE";
  }

  private static String getActUp() {
    return "MONTER";
  }

  private static String getActPrem() {
    return "PREMIER";
  }

  private static String getActCenter() {
    return "CENTER";
  }
  BCalque calque_;
  List listeners_;
  EbliActionInterface actDelete_ = new TreeDeleteCalqueAction(this);
  final boolean mustListen_;
  TreeSelectionModel selection_;

  public BArbreCalqueModel(final BGroupeCalque _calque) {
    this(_calque, true);
  }

  /**
   * Constructeur.
   *
   * @param _calque calque racine de l'arbre (GroupeCalque de la VueCalque)
   */
  public BArbreCalqueModel(final BGroupeCalque _calque, final boolean _listener) {
    selection_ = new DefaultTreeSelectionModel();
    setSelectionMultiCalques(true);
    mustListen_ = _listener;
    calque_ = _calque;
    if ((calque_ != null) && (calque_.isGroupeCalque())) {
      addGroupeCalque(calque_);
    }
    refresh();
  }

  private void addGroupeCalque(final BCalque _g) {
    if (mustListen_) {
      _g.addContainerListener(this);
      _g.addPropertyChangeListener(this);
      for (int i = _g.getComponentCount() - 1; i >= 0; i--) {
        final BCalque c = (BCalque) _g.getComponent(i);
        if (c.isGroupeCalque()) {
          addGroupeCalque(c);
        } else {
          c.addPropertyChangeListener(this);
        }
      }
    }
  }

  private void fireTreeModelEvent(final TreeModelEvent _evt) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((TreeModelListener) it.next()).treeNodesChanged(_evt);
      }
    }
  }

  private void fireTreeModelEventRemoved(final TreeModelEvent _evt) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((TreeModelListener) it.next()).treeNodesRemoved(_evt);
      }
    }
  }

  private void fireTreeModelEventAdded(final TreeModelEvent _evt) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((TreeModelListener) it.next()).treeNodesInserted(_evt);
      }
    }
  }

  private void fireTreeModelStructureEvent(final TreeModelEvent _evt) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((TreeModelListener) it.next()).treeStructureChanged(_evt);
      }
    }
  }

  private void removeGroupeCalque(final BCalque _g) {
    if (mustListen_) {
      _g.removeContainerListener(this);
      for (int i = _g.getComponentCount() - 1; i >= 0; i--) {
        final BCalque c = (BCalque) _g.getComponent(i);
        if (c.isGroupeCalque()) {
          removeGroupeCalque(c);
        } else {
          c.removePropertyChangeListener(this);
        }
      }
    }
  }

  private void setSelectionCalque(final BCalque _cq, final TreePath _tp) {
    final Object n = _tp.getLastPathComponent();
    TreePath tpo = _tp;
    final int count = getChildCount(n);
    for (int i = 0; i < count; i++) {
      final Object o = getChild(n, i);
      tpo = _tp.pathByAddingChild(o);
      if (o == _cq) {
        // selection_.expandPath(tpo);
        // normalement le calque doit etre selectionne.
        selection_.setSelectionPath(tpo);
        return;
      }
      setSelectionCalque(_cq, tpo);
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final BCalque[] c = getSelection();
    if (c.length == 0) {
      return;
    }
    final String action = _evt.getActionCommand();
    if (action.equals(getActCenter())) {
      actionCenter(c);
    }
    if (action.equals(getActPrem())) {
      actFirst(c);
    } else if (action.equals(getActUp())) {
      actUp(c);
    } else if (action.equals(getActDown())) {
      actDown(c);
    } else if (action.equals(getActLast())) {
      actLast(c);
    } else if (action.equals("SET_SELECTABLE")) {
      actSetSelectable(c, true);
    } else if (action.equals("UNSET_SELECTABLE")) {
      actSetSelectable(c, false);
    } else {
      int i = action.indexOf('_');
      if (i >= 0) {
        final String propriete = action.substring(0, i).toLowerCase();
        final Boolean b = action.endsWith("OUI") ? Boolean.TRUE : Boolean.FALSE;
        for (i = 0; i < c.length; i++) {
          c[i].setProperty(propriete, b);
        }
      } else if (selection_.getSelectionCount() == 1) {
        final BCalque calque = getSelectedCalque();
        if (("ATTENUE".equals(action)) && (calque instanceof BCalqueAffichage)) {
          ((BCalqueAffichage) calque).setAttenue(!((BCalqueAffichage) calque).isAttenue());
          // refreshSelect = true;
        } else if ((action.equals("RAPIDE")) && (calque instanceof BCalqueAffichage)) {
          ((BCalqueAffichage) calque).setRapide(!((BCalqueAffichage) calque).isRapide());
          // refreshSelect = true;
        } else if ((action.equals("GELE")) && (calque instanceof BCalqueInteraction)) {
          final BCalqueInteraction ci = (BCalqueInteraction) calque;
          ci.setGele(!ci.isGele());
          quickRefresh();
        } else if ("VISIBLE".equals(action)) {
          calque.setVisible(!calque.isVisible());
        } else if ("USER_VISIBLE".equals(action)) {
          calque.setUserVisible(!calque.isVisible());
        } else if ("SELECTABLE".equals(action) && (calque instanceof ZCalqueAffichageDonneesAbstract)) {
          ((ZCalqueAffichageDonneesAbstract) calque).setSelectable(!((ZCalqueAffichageDonneesAbstract) calque).isSelectable());
          // refreshSelect = true;
        } else if ("RENAME".equals(action)) {
          renameLayer(calque);

        }
      }
    }
  }

  private void actLast(final BCalque[] _c) {
    for (int i = 0; i < _c.length; i++) {
      _c[i].enDernier();
    }
  }

  private void actDown(final BCalque[] _c) {
    for (int i = 0; i < _c.length; i++) {
      _c[i].descendre();
    }
  }

  private void actUp(final BCalque[] _c) {
    for (int i = 0; i < _c.length; i++) {
      _c[i].monter();
    }
  }

  private void actFirst(final BCalque[] _c) {
    for (int i = 0; i < _c.length; i++) {
      _c[i].enPremier();
    }
  }

  /**
   * Rend les calques s�lectionnables ou non, en controlaant qu'ils puissent l'etre.
   *
   * @param _c Les calques.
   * @param _b True : Selectionnables.
   */
  private void actSetSelectable(final BCalque[] _c, boolean _b) {
    for (int i = 0; i < _c.length; i++) {
      if (_c[i] instanceof ZCalqueAffichageDonneesAbstract) {
        ZCalqueAffichageDonneesAbstract cq = (ZCalqueAffichageDonneesAbstract) _c[i];
        if (cq.canSetSelectable()) {
          cq.setSelectable(_b);
        }
      }
    }
  }

  protected void renameLayer(final BCalque _calque) {
    String title = _calque.getTitle();
    if (title == null) {
      title = _calque.getName();
    }
    if (title == null) {
      title = StringUtils.EMPTY;
    }
    final BuTextField ft = new BuTextField(title);
    ft.setColumns(title.length() + 2);
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setBorder(BuBorders.EMPTY3333);
    pn.setLayout(new BuGridLayout(2, 5, 5));
    pn.add(new BuLabel(EbliLib.getS("Nom du calque")));
    pn.add(ft);
    final String s = EbliLib.getS("Entrer le nouveau nom du calque");
    ft.setToolTipText(s);
    if (pn.afficheModaleOk(CtuluLibSwing.getFrameAncestorHelper(_calque), s)) {
      _calque.setTitle(ft.getText());
    }
  }

  public void setActionDelete(EbliActionInterface _actionDelete) {
    actDelete_ = _actionDelete;
  }

  private void actionCenter(final BCalque[] _c) {
    actionCenter(_c, (BVueCalque) SwingUtilities.getAncestorOfClass(BVueCalque.class, calque_), calque_, this);
  }

  public static void actionCenter(final BCalque[] _c, final BVueCalque _vue, final BCalque _main, final Object _src) {
    if (_vue != null) {
      final GrBoite b = new GrBoite();
      for (int i = 0; i < _c.length; i++) {
        b.ajuste(_c[i].getDomaine());
      }
      if (!b.isIndefinie()) {
        ZCalqueAffichageDonneesAbstract.ajusteZoomOnSelected(b, _main.getDomaine());
        _vue.changeRepere(_src, b);
      }
    }
  }

  public static void actionCenter(final BCalque _c, final ZEbliCalquesPanel _vue) {
    actionCenter(new BCalque[]{_c}, _vue.getVueCalque(), _vue.getDonneesCalque(), _vue);
  }

  @Override
  public void addTreeModelListener(final TreeModelListener _l) {
    if (listeners_ == null) {
      listeners_ = new ArrayList();
    }
    listeners_.add(_l);
  }

  /**
   * @param _l le listener a ajouter
   */
  public void addTreeSelectionListener(final TreeSelectionListener _l) {
    selection_.addTreeSelectionListener(_l);
  }

  @Override
  public void componentAdded(final ContainerEvent _e) {
    final BCalque c = (BCalque) _e.getChild();
    if (mustListen_) {
      if (c.isGroupeCalque()) {
        addGroupeCalque(c);
      } else {
        c.addPropertyChangeListener(this);
      }
      final BCalque parent = (BCalque) c.getParent();
      final int directCalqueIdx = parent.getDirectCalqueIdx(c);
      Object[] path = getPath(parent);
      fireTreeModelEventAdded(new TreeModelEvent(this, new TreePath(path), new int[]{directCalqueIdx},
              new Object[]{c}));
    }
    setSelectionCalque(c);
    fireObservableChanged(LAYER_ADDED);
  }

  Object[] getPath(final BCalque _cq) {
    final List l = new ArrayList(5);
    if (_cq == getRootCalque()) {
      return new Object[]{_cq};
    }
    l.add(_cq);
    BCalque parent = _cq;
    do {
      parent = (BCalque) parent.getParent();
      l.add(parent);
    } while (parent != null && parent != getRootCalque());
    Object[] res = l.toArray();
    CtuluLibArray.invert(res, 0);
    return res;
  }

  @Override
  public void componentRemoved(final ContainerEvent _e) {
    final BCalque c = (BCalque) _e.getChild();
    if (mustListen_) {
      if (c.isGroupeCalque()) {
        removeGroupeCalque(c);
      } else {
        c.removePropertyChangeListener(this);
      }
    }
    final BCalque parent = (BCalque) _e.getContainer();
    if (parent != null) {
      /*
       * final Integer integer = ((Integer) c.getClientProperty("oldIndice")); if (integer == null) {
       */ refresh();
      /*
       * } else { fireTreeModelEventRemoved(new TreeModelEvent(this, new TreePath(getPath(parent)), new int[] { integer
       * .intValue() }, new Object[] { c })); }
       */ setSelectionCalque(parent);
    }
    fireObservableChanged(LAYER_REMOVED);
  }

  public void fillMenu(final BuMenu _m) {
    _m.addMenuItem(EbliResource.EBLI.getString("Centrer la vue"), getActCenter(), EbliResource.EBLI.getToolIcon("zoom-layer"),
            true, 0, this);
    _m.addMenuItem(EbliResource.EBLI.getString("Visible"), "USER_VISIBLE_OUI", null, editable, 0, this);
    _m.addMenuItem(EbliResource.EBLI.getString("Cach�"), "USER_VISIBLE_NON", BuResource.BU.getIcon("cacher"), editable, 0, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("S�lectionnable"), "SET_SELECTABLE", EbliResource.EBLI.getIcon("fleche"), true, 0,
            this);
    _m.addMenuItem(EbliResource.EBLI.getString("Non s�lectionnable"), "UNSET_SELECTABLE", EbliResource.EBLI.getIcon(
            "non-selectionnable"), true, 0, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("Att�nu�"), "ATTENUE_OUI", null, true, 0, this);
    _m.addMenuItem(EbliResource.EBLI.getString(getActNormal()), "ATTENUE_NON", null, true, 0, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("Rapide"), "RAPIDE_OUI", null, true, 0, this);
    _m.addMenuItem(EbliResource.EBLI.getString(getActNormal()), "RAPIDE_NON", null, true, 0, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("Gel�"), "GELE_OUI", null, true, 0, this);
    _m.addMenuItem(EbliResource.EBLI.getString("Actif"), "GELE_NON", null, true, 0, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("En premier"), getActPrem(), EbliResource.EBLI.getIcon("enpremier"),
            true, 0, this);
    _m.addMenuItem(CtuluLib.getS("Monter"), getActUp(), CtuluResource.CTULU.getIcon("monter"), editable, 0, this);
    _m.addMenuItem(CtuluLib.getS("Descendre"), getActDown(), CtuluResource.CTULU.getIcon("descendre"), editable, 0, this);
    _m.addMenuItem(EbliResource.EBLI.getString("En dernier"), getActLast(), EbliResource.EBLI.getIcon("endernier"),
            editable, 0, this);
    _m.addSeparator();
    _m.add(new JMenuItem(actDelete_));
  }

  private String getActNormal() {
    return "Normal";
  }

  public void fillMenu(final CtuluPopupMenu _m) {
    _m.addMenuItem(EbliResource.EBLI.getString("Centrer la vue"), getActCenter(), EbliResource.EBLI.getToolIcon("zoom-layer"),
            true, this);
    _m.addMenuItem(EbliResource.EBLI.getString("Visible"), "VISIBLE_OUI", null, editable, this);
    final JMenuItem it = _m.addMenuItem(EbliResource.EBLI.getString("Cach�"), "VISIBLE_NON", null, editable, this);
    it.setIcon(BuResource.BU.getIcon("cacher"));
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("S�lectionnable"), "SET_SELECTABLE", EbliResource.EBLI.getIcon("fleche"), true,
            this);
    _m.addMenuItem(EbliResource.EBLI.getString("Non s�lectionnable"), "UNSET_SELECTABLE", EbliResource.EBLI.getIcon(
            "non-selectionnable"), true, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("Att�nu�"), "ATTENUE_OUI", null, true, this);
    _m.addMenuItem(EbliResource.EBLI.getString(getActNormal()), "ATTENUE_NON", null, true, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("Rapide"), "RAPIDE_OUI", null, true, this);
    _m.addMenuItem(EbliResource.EBLI.getString(getActNormal()), "RAPIDE_NON", null, true, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("Gel�"), "GELE_OUI", null, true, this);
    _m.addMenuItem(EbliResource.EBLI.getString("Actif"), "GELE_NON", null, true, this);
    _m.addSeparator();
    _m.addMenuItem(EbliResource.EBLI.getString("En premier"), getActPrem(), EbliResource.EBLI.getIcon("enpremier"),
            editable, this);
    _m.addMenuItem(CtuluLib.getS("Monter"), getActUp(), CtuluResource.CTULU.getIcon("monter"), editable, this);
    _m.addMenuItem(CtuluLib.getS("Descendre"), getActDown(), CtuluResource.CTULU.getIcon("descendre"), editable, this);
    _m.addMenuItem(EbliResource.EBLI.getString("En dernier"), getActLast(), EbliResource.EBLI.getIcon("endernier"),
            editable, this);
    _m.addSeparator();
    _m.add(new JMenuItem(actDelete_));
  }

  public void fillPopupMenu(final CtuluPopupMenu _m) {
    if (selection_.getSelectionCount() > 1) {
      fillMenu(_m);
    } else if (selection_.getSelectionCount() == 1) {
      fillPopupMenu(_m, getSelectedCalque());
    }
  }

  private void fillPopupMenu(final CtuluPopupMenu _m, final BCalque _c) {
    if ((_c == null)) {
      return;
    }
    _m.addMenuItem(EbliResource.EBLI.getString("Centrer la vue"), getActCenter(), EbliResource.EBLI.getToolIcon("zoom-layer"),
            true, this);
    /*
     * if (_c instanceof BCalqueAffichage) { final BCalqueAffichage ca = (BCalqueAffichage) _c;
     * _m.addCheckBox(EbliResource.EBLI.getString("Att�nu�"), "ATTENUE", true, ca.isAttenue(), _arbre);
     * _m.addCheckBox(EbliResource.EBLI.getString("Rapide"), "RAPIDE", true, ca.isRapide(), _arbre); }
     */
    if (_c instanceof BCalqueInteraction) {
      final BCalqueInteraction ci = (BCalqueInteraction) _c;
      _m.addCheckBox(EbliResource.EBLI.getString("Gel�"), "GELE", true, ci.isGele(), this);
    }
    _m.addCheckBox(EbliResource.EBLI.getString("Visible"), "VISIBLE", editable, _c.isVisible(), this);
    if (_c instanceof ZCalqueAffichageDonneesAbstract) {
      ZCalqueAffichageDonneesAbstract cq = (ZCalqueAffichageDonneesAbstract) _c;
      if (cq.canSetSelectable()) {
        BuCheckBoxMenuItem mi
                = _m.addCheckBox(EbliResource.EBLI.getString("S�lectionnable"), "SELECTABLE", EbliResource.EBLI.getIcon(
                                "fleche"), true, cq.isSelectable());
        mi.addActionListener(this);
      }
    }
    if (_c.isMovable()) {
      _m.addSeparator();
      _m.addMenuItem(EbliResource.EBLI.getString("En premier"), getActPrem(), EbliResource.EBLI.getIcon("enpremier"),
              editable, this);
      _m.addMenuItem(CtuluLib.getS("Monter"), getActUp(), CtuluResource.CTULU.getIcon("monter"), editable, this);
      _m.addMenuItem(CtuluLib.getS("Descendre"), getActDown(), CtuluResource.CTULU.getIcon("descendre"), editable, this);
      _m.addMenuItem(EbliResource.EBLI.getString("En dernier"), getActLast(), EbliResource.EBLI.getIcon("endernier"),
              editable, this);
    }
    if (_c.isTitleModifiable()) {
      _m.addMenuItem(EbliResource.EBLI.getString("Renommer"), "RENAME", BuResource.BU.getIcon("renommer"), editable,
              this);
    }
    if (_c.isDestructible() && editable) {
      _m.addSeparator();
      _m.add(new JMenuItem(actDelete_));
    }
    final JMenuItem[] specificMenus = _c.getSpecificMenuItems();
    if (specificMenus != null) {
      _m.addSeparator();
      final int nb = specificMenus.length;
      for (int i = 0; i < nb; i++) {
        if (specificMenus[i] == null) {
          _m.addSeparator();
        } else {
          _m.add(specificMenus[i]);
        }
      }
    }
    _m.setSize(_m.getPreferredSize());
  }

  /**
   * Renvoie le fils numero <I>_index </I> du calque <I>_parent </I>.
   */
  @Override
  public Object getChild(final Object _parent, final int _index) {
    final BCalque[] c = ((BCalque) _parent).getCalques();
    return c[_index];
  }

  /**
   * Nombre de fils du calque <I>_parent </I>.
   */
  @Override
  public int getChildCount(final Object _parent) {
    if (!((BCalque) _parent).isGroupeCalque()) {
      return 0;
    }
    return ((BCalque) _parent).getCalques().length;
  }

  /**
   * Indice du fils <I>_child </I> du calque <I>_parent </I>.
   */
  @Override
  public int getIndexOfChild(final Object _parent, final Object _child) {
    final BCalque[] c = ((BCalque) _parent).getCalques();
    for (int i = 0; i < c.length; i++) {
      if (c[i] == _child) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Calque racine de l'arbre.
   */
  @Override
  public Object getRoot() {
    return calque_;
  }

  public BCalque getRootCalque() {
    return calque_;
  }

  public final BCalque getSelectedCalque() {
    final TreePath p = selection_.getSelectionPath();
    return p == null ? null : (BCalque) p.getLastPathComponent();
  }

  /**
   * Renvoie le calque selectionne.
   */
  public BCalque[] getSelection() {
    BCalque[] r = null;
    final TreePath[] path = selection_.getSelectionPaths();
    if (path == null) {
      r = new BCalque[0];
    } else {
      final int l = path.length;
      r = new BCalque[l];
      for (int i = 0; i < l; i++) {
        r[i] = (BCalque) path[i].getLastPathComponent();
      }
    }
    return r;
  }

  /**
   * Renvoie le calque selectionne.
   */
  public TreePath[] getSelectionParent() {
    TreePath[] r = null;
    final TreePath[] path = selection_.getSelectionPaths();
    if (path == null) {
      r = new TreePath[0];
    } else {
      final int l = path.length;
      r = new TreePath[l];
      for (int i = 0; i < l; i++) {
        r[i] = path[i].getParentPath();
      }
    }
    return r;
  }

  public TreeSelectionModel getTreeSelectionModel() {
    return selection_;
  }

  /**
   * Teste si le calque <I>_node </I> est une feuille.
   */
  @Override
  public boolean isLeaf(final Object _node) {
    return !((BCalque) _node).isGroupeCalque();
  }

  /**
   * @return true si le modele doit ecouter tout ce qu'il se passe sur les calques
   */
  public final boolean isMustListen() {
    return mustListen_;
  }
  Observable observable_;

  void editableChanged() {
    final TreeModelEvent ev = new TreeModelEvent(this, new Object[]{calque_});
    fireTreeModelEvent(ev);
  }

  static class SpecialObservable extends Observable {

    @Override
    protected synchronized void setChanged() {
      super.setChanged();
    }
  }

  public Observable getObservable() {
    if (observable_ == null) {
      observable_ = new SpecialObservable();
    }
    return observable_;
  }

  public void fireObservableChanged(final Object _prop) {
    if (observable_ != null) {
      ((SpecialObservable) observable_).setChanged();
      observable_.notifyObservers(_prop);
    }
  }

  public void fireObservableChanged() {
    fireObservableChanged(this);

  }

  /**
   * Reception des <I>PropertyChangeEvent </I>. Repercute les evenements au calque selectionne.
   */
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    final String propertyName = _evt.getPropertyName();
    if (Arrays.binarySearch(PROP_TO_LISTEN, propertyName) >= 0) {
      fireTreeModelEvent(new TreeModelEvent(this, new Object[]{_evt.getSource()}));

    } else if (_evt.getSource() == calque_ && _evt.getPropertyName().equals("versEcran")) {
      fireObservableChanged(propertyName);
    }
    if (!"rapide".equals(propertyName) && !"ajustement".equals(propertyName)) {
      fireObservableChanged(propertyName);
    }
  }

  public void quickRefresh() {
    final TreePath p = selection_.getSelectionPath();
    fireTreeModelStructureEvent(null);
    selection_.setSelectionPath(p);
  }
  boolean editable = true;

  public void setEditable(boolean editable) {
    this.editable = editable;
    editableChanged();
  }

  public boolean isEditable() {
    return editable;
  }

  public final void refresh() {
    final BCalque p = getSelectedCalque();
    final TreeModelEvent ev = new TreeModelEvent(this, new Object[]{calque_});
    fireTreeModelStructureEvent(ev);
    setSelectionCalque(p);
  }

  @Override
  public void removeTreeModelListener(final TreeModelListener _l) {
    if (listeners_ != null) {
      listeners_.remove(_l);
    }
  }

  public void removeTreeSelectionListener(final TreeSelectionListener _l) {
    selection_.removeTreeSelectionListener(_l);
  }

  /**
   * Selectionne un claque dans l'arbre.
   */
  public void setSelectionCalque(final BCalque _cq) {
    if (_cq == null) {
      selection_.clearSelection();
    } else {
      final Object n = calque_;
      final TreePath tp = new TreePath(n);
      if (n == _cq) {
        selection_.setSelectionPath(tp);
      } else {
        setSelectionCalque(_cq, tp);
      }
    }
  }

  public void addChildInSelection(final TreePath _p) {
    if (_p == null) {
      return;
    }
    final BCalque cq = (BCalque) _p.getLastPathComponent();
    final BCalque[] childs = cq.getCalques();
    final TreePath[] ps = new TreePath[childs.length];
    for (int i = 0; i < ps.length; i++) {
      ps[i] = _p.pathByAddingChild(childs[i]);
    }
    selection_.addSelectionPaths(ps);
  }

  /**
   * Autorise la selection multicalques.
   *
   * @param _b true La selection est multicalques, false sinon.
   */
  public void setSelectionMultiCalques(boolean _b) {
    if (_b) {
      selection_.setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    } else {
      selection_.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

  }

  public boolean isSelectionMultiCalques() {
    return selection_.getSelectionMode() != TreeSelectionModel.SINGLE_TREE_SELECTION;
  }

  @Override
  public void valueForPathChanged(final TreePath _path, final Object _newValue) {
  }
}
