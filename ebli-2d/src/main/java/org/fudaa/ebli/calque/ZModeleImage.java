/*
 * @file         ZModeleImage.java
 * @creation     2002-09-02
 * @modification $Date: 2006-08-17 16:33:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.image.BufferedImage;

/**
 * Un modele gerant les images.
 * 
 * @version $Id: ZModeleImage.java,v 1.10 2006-08-17 16:33:25 deniger Exp $
 * @author Fred Deniger
 */
public interface ZModeleImage {
  /**
   * Renvoie l'image de basee.
   */
  BufferedImage getImage();

}
