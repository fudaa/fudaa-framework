/*
 * @creation     14 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.action;

import com.memoire.bu.BuResource;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Une action pour montrer l'orientation des lignes dans les calques.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class SceneShowOrientationAction extends EbliActionChangeState implements TreeModelListener {
    
  protected BArbreCalqueModel treeModel_;
    
  public SceneShowOrientationAction(BArbreCalqueModel _treeModel) {
      super(EbliLib.getS("Afficher l'orientation des lignes"), BuResource.BU.getIcon("fleche"), "SHOW_ORIENTATION");
      treeModel_=_treeModel;
      treeModel_.addTreeModelListener(this);
      setEnabled(true);
    }

    private void applyConf(){
      treeModel_.getRootCalque().apply(new BCalqueVisitor(){
      @Override
        public boolean visit(BCalque _cq) {
          if(_cq instanceof ZCalqueLigneBrisee)
          ((ZCalqueLigneBrisee) _cq).setShowLineOrientation(isSelected());
          return true;
        }
      });
    }
    
    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeNodesChanged(javax.swing.event.TreeModelEvent)
     */
  @Override
    public void treeNodesChanged(TreeModelEvent e) {
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeNodesInserted(javax.swing.event.TreeModelEvent)
     */
  @Override
    public void treeNodesInserted(TreeModelEvent e) {
      applyConf();
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeNodesRemoved(javax.swing.event.TreeModelEvent)
     */
  @Override
    public void treeNodesRemoved(TreeModelEvent e) {
    }

    /* (non-Javadoc)
     * @see javax.swing.event.TreeModelListener#treeStructureChanged(javax.swing.event.TreeModelEvent)
     */
  @Override
    public void treeStructureChanged(TreeModelEvent e) {
    }

    /* (non-Javadoc)
     * @see org.fudaa.ebli.commun.EbliActionChangeState#changeAction()
     */
    @Override
    public void changeAction() {
      applyConf();
    }
}
