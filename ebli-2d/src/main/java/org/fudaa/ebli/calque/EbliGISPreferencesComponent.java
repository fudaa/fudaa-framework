package org.fudaa.ebli.calque;

import com.memoire.bu.BuAbstractPreferencesComponent;
import com.memoire.bu.BuTextField;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;

/**
 * Un composant pour les pr�f�rences d'export de valeurs en CVS/TXT
 * @author marchand@deltacad.fr
 */
public class EbliGISPreferencesComponent extends BuAbstractPreferencesComponent {
  private BuTextField tfCoords_;
  private int visuNbDec;

  public EbliGISPreferencesComponent() {
    super(EbliPreferences.EBLI);
    
    tfCoords_=BuTextField.createIntegerField();
    tfCoords_.setPreferredSize(new Dimension(80,tfCoords_.getPreferredSize().height));
    setLayout(new BorderLayout());
    add(new JLabel(EbliLib.getS("Nombre maxi de d�cimales")),BorderLayout.CENTER);
    add(tfCoords_,BorderLayout.EAST);
    
    updateComponent();
    
    tfCoords_.addCaretListener(new CaretListener() {
      @Override
      public void caretUpdate(CaretEvent e) {
        valueHasChanged();
      }
    });
  }
  
  @Override
  public String getTitle() {
    return EbliLib.getS("Affichage des coordonn�es 2D");
  }
  
  private void valueHasChanged() {
    setSavabled(tfCoords_.getValue()!=null);
    setModified(isSavabled() && !tfCoords_.getValue().equals(visuNbDec));
  }
  
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }
  
  /**
   * Mise a jour des composant � partir des info du fichier.
   */
  @Override
  protected void updateComponent() {
    visuNbDec=options_.getIntegerProperty(CtuluResource.COORDS_VISU_NB_DIGITS, 2);
    tfCoords_.setValue(visuNbDec);
  }
  
  /**
   * Remplit la table a partir des valeurs des combobox.
   */
  @Override
  protected void updateProperties() {
    // Nombre de decimales pour l'export des coordonn�es
    options_.putIntegerProperty(CtuluResource.COORDS_VISU_NB_DIGITS,(Integer)tfCoords_.getValue());
//    CtuluValueEditorDouble.
  }
}
