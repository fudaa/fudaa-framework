/*
 * @file         DePoint.java
 * @creation     2000-04-19
 * @modification $Date: 2006-07-13 13:35:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un point.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-07-13 13:35:49 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class DePoint extends DeForme {
  // donnees membres publiques
  // donnees membres privees
  GrPoint position_;
  // Constructeurs
  public DePoint(final GrPoint _pos) {
    super();
    position_= _pos;
  }
  public DePoint() {
    super();
    position_= new GrPoint(0., 0., 0.);
  }
  @Override
  public int getForme() {
    return POINT;
  }
  // Methodes publiques
  @Override
  public GrObjet getGeometrie() {
    return position_;
  }
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
    _t.dessinePoint(_g2d,position_, _rapide);
  }
  public void setPosition(final GrPoint _p) {
    position_= _p;
  }
  // >>> Interface GrContour ---------------------------------------------------
  /**
   * Retourne les points du contour de l'objet.
   *
   * @return Les points de contour.
   * @see    org.fudaa.ebli.geometrie.GrContour
   */
  @Override
  public GrPoint[] contour() {
    return new GrPoint[] { position_ };
  }
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    return position_.estSelectionne(_ecran, _dist, _pt);
  }
  // <<< Interface GrContour ---------------------------------------------------
  // >>> Implementation GrObjet pour �tre d�placable ---------------------------
  @Override
  public void autoApplique(final GrMorphisme _t) {
    position_.autoApplique(_t);
  }
  /**
   * Retourne la boite englobante de l'objet.
   */
  @Override
  public GrBoite boite() {
    return position_.boite();
  }
  // <<< Implementation GrObjet ------------------------------------------------
}
