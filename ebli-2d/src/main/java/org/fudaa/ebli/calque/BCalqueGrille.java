/*
 * @file         BCalqueGrille.java
 * @creation     1998-09-03
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Un calque d'affichage d'une grille.
 * 
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:55:47 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BCalqueGrille extends BCalqueAffichage {
  public BCalqueGrille() {
    super();
    boite_ = null;
    pasX_ = 100.;
    pasY_ = 100.;
    setDestructible(false);
    setTitle(EbliLib.getS("Grille"));
  }

  public int getOffset() {
    return 10;
  }

  // Icon
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color fg = getForeground();
    if (isAttenue()) {
      fg = attenueCouleur(fg);
    }
    _g.setColor(fg);
    final int w = getIconWidth();
    final int h = getIconHeight();
    for (int i = 6; i < w - 2; i += 8) {
      _g.drawLine(_x + i, _y + 1, _x + i, _y + h - 1);
    }
    for (int j = 6; j < h - 2; j += 8) {
      _g.drawLine(_x + 1, _y + j, _x + w - 1, _y + j);
    }
  }

  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    if (boite_ == null) {
      return;
    }
    final GrBoite zv= getClipReel(_g).intersectionXY(boite_);
    if (zv == null) {
      return;
    }
    double xmin= zv.o_.x_;
    double ymin= zv.o_.y_;
    double xmax= zv.e_.x_;
    double ymax= zv.e_.y_;
    if (pasX_ > 1.) {
      xmin= Math.floor(xmin * pasX_) / pasX_;
    } else {
      xmin= Math.floor(xmin / pasX_) * pasX_;
    }
    if (pasY_ > 1.) {
      ymin= Math.floor(ymin * pasY_) / pasY_;
    } else {
      ymin= Math.floor(ymin / pasY_) * pasY_;
    }
    if (pasX_ > 1.) {
      xmax= Math.ceil(xmax * pasX_) / pasX_;
    } else {
      xmax= Math.ceil(xmax / pasX_) * pasX_;
    }
    if (pasY_ > 1.) {
      ymax= Math.ceil(ymax * pasY_) / pasY_;
    } else {
      ymax= Math.ceil(ymax / pasY_) * pasY_;
    }
    double dx= pasX_;
    double dy= pasY_;
    while (((xmax - xmin) / dx > 19.) || ((ymax - ymin) / dy > 19.)) {
      dx *= 5.;
      dy *= 5.;
    }
    final boolean attenue= isAttenue();
    final boolean rapide= isRapide();
    Color fg= getForeground();
    if (attenue) {
      fg= attenueCouleur(fg);
    }
    _g.setColor(fg);
    final GrMorphisme versEcran= getVersEcran();
    if (rapide) {
      for (double x= xmin; x <= xmax; x += dx) {
        final GrPoint e1= new GrPoint(x, ymin, 0.).applique(versEcran);
        final GrPoint e2= new GrPoint(x, ymax, 0.).applique(versEcran);
        final int xe1= (int)e1.x_;
        final int ye1= (int)e1.y_;
        final int xe2= (int)e2.x_;
        final int ye2= (int)e2.y_;
        _g.drawLine(xe1, ye1, xe2, ye2);
      }
      for (double y= ymin; y <= ymax; y += dy) {
        final GrPoint e1= new GrPoint(xmin, y, 0.).applique(versEcran);
        final GrPoint e2= new GrPoint(xmax, y, 0.).applique(versEcran);
        final int xe1= (int)e1.x_;
        final int ye1= (int)e1.y_;
        final int xe2= (int)e2.x_;
        final int ye2= (int)e2.y_;
        _g.drawLine(xe1, ye1, xe2, ye2);
      }
    } else {
      final Font font= getFont();
      if (font != null) {
        _g.setFont(font);
      }
      final TraceLigne gtl= new TraceLigne();
      gtl.setTypeTrait(TraceLigne.TIRETE);
      gtl.setCouleur(fg);
      final Graphics2D g2d=(Graphics2D)_g;
      for (double x= xmin; x <= xmax; x += dx) {
        final GrPoint e1= new GrPoint(x, ymin, 0.).applique(versEcran);
        final GrPoint e2= new GrPoint(x, ymax, 0.).applique(versEcran);
        final int xe1= (int)e1.x_;
        final int ye1= (int)e1.y_;
        final int xe2= (int)e2.x_;
        final int ye2= (int)e2.y_;
        gtl.dessineTrait(g2d,xe1, ye1, xe2, ye2);
        _g.drawString(
          "" + (Math.ceil(100. * x) / 100.),
          xe1 + 3,
          ye1 + (font==null?0:font.getSize()));
      }
      for (double y= ymin; y <= ymax; y += dy) {
        final GrPoint e1= new GrPoint(xmin, y, 0.).applique(versEcran);
        final GrPoint e2= new GrPoint(xmax, y, 0.).applique(versEcran);
        final int xe1= (int)e1.x_;
        final int ye1= (int)e1.y_;
        final int xe2= (int)e2.x_;
        final int ye2= (int)e2.y_;
        gtl.dessineTrait(g2d,xe1, ye1, xe2, ye2);
        _g.drawString("" + (Math.ceil(100. * y) / 100.), xe1 + 3, ye1 - 3);
        // +font.getSize()
      }
    }
  }
  // Proprietes
  private GrBoite boite_;

  /**
   * Accesseur de la propriete <I>boite</I>. Elle donne la position, la taille, le domaine de la grille.
   */
  public GrBoite getBoite() {
    return boite_;
  }

  /**
   * Affectation de la propriete <I>boite</I>.
   */
  public void setBoite(final GrBoite _v) {
    if (boite_ != _v) {
      final GrBoite vp = boite_;
      boite_ = _v;
      firePropertyChange("boite", vp, _v);
    }
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible()) {
      GrBoite d = null;
      d = super.getDomaine();
      r = getBoite();
      if (d != null) {
        if (r == null) {
          r = d;
        } else {
          r = r.union(d);
        }
      }
    }
    return r;
  }
  private double pasX_;

  /**
   * Accesseur de la propriete <I>pasX</I>. Elle donne le pas en X de la grille.
   */
  public double getPasX() {
    return pasX_;
  }

  /**
   * Affectation de la propriete <I>pasX</I>.
   */
  public void setPasX(final double _v) {
    if (pasX_ != _v) {
      final double vp = pasX_;
      pasX_ = _v;
      firePropertyChange("pasX", vp, _v);
    }
  }
  private double pasY_;

  /**
   * Accesseur de la propriete <I>pasY</I>. Elle donne le pas en X de la grille.
   */
  public double getPasY() {
    return pasY_;
  }

  /**
   * Affectation de la propriete <I>pasY</I>.
   */
  public void setPasY(final double _v) {
    if (pasY_ != _v) {
      final double vp = pasY_;
      pasY_ = _v;
      firePropertyChange("pasY", vp, _v);
    }
  }
}
