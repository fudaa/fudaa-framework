/*
 * @creation 24 mai 07
 * 
 * @modification $Date: 2007-06-20 12:23:11 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * Une classe permettant de cr�er un modele de fleches d'apr�s la grille utilis� sur une autre modele.
 *
 * @author fred deniger
 */
public class ZModeleFlecheForDensite implements ZModeleFleche {

  final FlecheGrilleData grille_;
  private int increment;
  private int nbPoint;
  private final ZModeleFleche support_;

  public ZModeleFlecheForDensite(final ZModeleFleche _support, final FlecheGrilleData _grille) {
    super();
    support_ = _support;
    grille_ = _grille;
    increment = 1;
    nbPoint = support_.getNombre();
  }

  @Override
  public void prepare() {
    support_.prepare();
  }

  @Override
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer) {
    return new CtuluTable(new ZCalqueFleche.ValueTableModel(this));
  }

  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
    CtuluListSelectionInterface layerSelection = _layer.getLayerSelection();
    if (layerSelection==null || layerSelection.isEmpty()) {
      ZCalqueFleche.fillWithInfo(_d, layerSelection, this, info);
    } else {
      CtuluListSelection selection = new CtuluListSelection();
      int[] selectedIndex = layerSelection.getSelectedIndex();
      for (int i = 0; i < selectedIndex.length; i++) {
        int finalIdx = getFinalIdx(selectedIndex[i]);
        if (isIndexCorrect(finalIdx)) {
          selection.add(finalIdx);
        }
      }
      ZCalqueFleche.fillWithInfo(_d, selection, this, info);
    }
  }

  @Override
  public GrBoite getDomaine() {
    return support_.getDomaine();
  }

  public int getFinalIdx(int _i) {
    if (nbPoint <= 0) {
      return 0;
    }
    return _i * increment;
  }

  @Override
  public int getNombre() {
    return nbPoint;
  }

  @Override
  public double getNorme(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getNorme(idx);
    }
    return 0;
  }

  @Override
  public Object getObject(int _ind) {
    int idx = getFinalIdx(_ind);
    if (isIndexCorrect(idx)) {
      return support_.getObject(idx);
    }
    return null;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int vertex) {
    int idx = getFinalIdx(_ind);
    if (isIndexCorrect(idx)) {
      return support_.getVertexForObject(idx, vertex);
    }
    return null;
  }

  public int getVisiblePourcentage() {
    return increment;
  }

  @Override
  public double getVx(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getVx(idx);
    }
    return 0;
  }

  @Override
  public double getVy(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getVy(idx);
    }
    return 0;
  }

  @Override
  public double getX(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getX(idx);
    }
    return 0;
  }

  @Override
  public double getY(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getY(idx);
    }
    return 0;
  }

  @Override
  public double getZ1(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getZ1(idx);
    }
    return 0;
  }

  @Override
  public double getZ2(int _i) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.getZ2(idx);
    }
    return 0;
  }

  @Override
  public boolean interpolate(GrSegment _seg, double _x, double _y) {
    return support_.interpolate(_seg, _x, _y);
  }

  private boolean isIndexCorrect(int idx) {
    return idx >= 0 && idx < support_.getNombre();
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  @Override
  public boolean segment(GrSegment _s, int _i, boolean _force) {
    int idx = getFinalIdx(_i);
    if (isIndexCorrect(idx)) {
      return support_.segment(_s, idx, _force);
    }
    return false;
  }

  private void setIncrement(int increment) {
    this.increment = increment;
    this.increment = Math.min(this.increment, getNombre() - 1);
    this.increment = Math.max(this.increment, 1);
    nbPoint = (int) Math.floor((support_.getNombre() - 1d) / increment) + 1;
  }

  public void updatePourcentage() {
    setIncrement(grille_.getDensiteIncrement());
  }
}
