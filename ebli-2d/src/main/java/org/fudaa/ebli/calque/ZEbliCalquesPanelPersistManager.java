package org.fudaa.ebli.calque;

import java.io.File;
import java.util.Map;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Interface des datas persistantes du calques.
 * Sont enregistrees dans un ficheir xml.
 * Doivent etre serializables.
 * Propose les methodes de construction du calque et de sauvegarde des donn�es.
 * @author Adrien Hadoux
 *
 */
public interface ZEbliCalquesPanelPersistManager {

	/**
	 * Remplit l'interface avec les donn�es.
	 * @param p
	 */
	  public void fillInfoWith(ZEbliCalquesPanel p,File dirGeneralData);
	  
	  /**
	   * Permet de generer le calque coprrespondant aux donnees persistantes.
	   * @param parameters
	   * @return
	   */
	  public ZEbliCalquesPanel generateCalquePanel(Map parameters,File dirGeneralData);
	
	  /**
	   * Methode utilisee pour recuperer un nom plus 'comprehensible'
	   * A utiliser dans le cas d'une serialization xml de la classe: on donne ainsi un nom plus coh�rent a la balise au lieu du nom de classe.
	   * @return
	   */
	 
	  
	  public GrBoite getZoom();
	
}
