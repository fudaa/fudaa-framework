/*
 * @creation     1998-12-15
 * @modification $Date: 2006-07-13 13:35:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.util.EventObject;
import org.fudaa.ebli.calque.dessin.DeForme;
/**
 * Un �v�nement <I>Forme</I>.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-07-13 13:35:44 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class FormeEvent extends EventObject {
  DeForme forme_;
  /**
   * Construction de l'�venement.
   * @param _source Objet d�clencheur de l'�v�nement
   * @param _forme Forme (DeForme.RECTANGLE, DeForme.TRAIT, etc.)
   */
  public FormeEvent(final Object _source, final DeForme _forme) {
    super(_source);
    forme_= _forme;
  }
  /**
   * Retourne la forme associ�e � l'�v�nement.
   * @return Forme (DeForme.RECTANGLE, DeForme.TRAIT, etc.)
   */
  public DeForme getForme() {
    return forme_;
  }
}