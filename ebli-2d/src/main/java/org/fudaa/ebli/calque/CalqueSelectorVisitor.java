/*
 * @creation     13 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import java.util.ArrayList;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;

/**
 * Ce visiteur parcours la hiérarchie de calque et selectionne ceux possédant un
 * model de données du type choisie et implémentant ZCalqueEditable.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class CalqueSelectorVisitor implements BCalqueVisitor {

  /** Le type de model sur lequel se fait la selection des calques. */
  private Class<? extends ZModeleDonnees> typeModel_;
  /** Liste contenant les calques selectionnés */
  private ArrayList<ZCalqueAffichageDonneesAbstract> selectedCalques_;

  public CalqueSelectorVisitor(Class<? extends ZModeleDonnees> _typeModel) {
    typeModel_=_typeModel;
    selectedCalques_=new ArrayList<ZCalqueAffichageDonneesAbstract>();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.fudaa.ebli.calque.BCalqueVisitor#visit(org.fudaa.ebli.calque.BCalque)
   */
  @Override
  public boolean visit(BCalque _cq) {
    if (_cq instanceof ZCalqueAffichageDonneesAbstract && _cq instanceof ZCalqueEditable) {
      ZModeleDonnees modelData=((ZCalqueAffichageDonneesAbstract)_cq).modeleDonnees();
      if (modelData!=null&&typeModel_.isInstance(modelData))
        selectedCalques_.add((ZCalqueAffichageDonneesAbstract)_cq);
    }
    return true;
  }

  /**
   * @return retourne la liste de calques selectionnés.
   */
  public ZCalqueAffichageDonneesAbstract[] getSelectedCalques() {
    return selectedCalques_.toArray(new ZCalqueAffichageDonneesAbstract[0]);
  }

}
