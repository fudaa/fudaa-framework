/*
 * @file         BCalqueImage.java
 * @creation     1998-08-27
 * @modification $Date: 2006-11-14 09:06:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuFilters;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Un calque d'affichage d'une image.
 *
 * @version $Id: BCalqueImage.java,v 1.12 2006-11-14 09:06:24 deniger Exp $
 * @author Guillaume Desnoix
 */
public class BCalqueImage extends BCalqueAffichage {
  protected boolean premier_;
  protected int[] pixels_;

  public BCalqueImage() {
    super();
    couleur_ = Color.lightGray;
    premier_ = true;
    pixels_ = null;
  }

  // Icon
  /**
   * Dessin de l'icone.
   *
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    // boolean attenue=isAttenue();
    final int w = getIconWidth() - 1;
    final int h = getIconHeight() - 1;
    final Image i = getImage();
    if (i != null) {
      final int wi = i.getWidth(this);
      final int hi = i.getHeight(this);
      final int r = Math.max(wi / w, hi / h);
      _g.drawImage(i, _x + 1, _y + 1, wi / r - 1, hi / r - 1, _c);
    }
  }

  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    final boolean attenue = isAttenue();
    final boolean rapide = isRapide();
    // Dimension d =getSize();
    final Rectangle clip = _g.getClipBounds();
    final int cxmin = clip.x;
    final int cymin = clip.y;
    final int cxmax = clip.x + clip.width;
    final int cymax = clip.y + clip.height;
    final GrMorphisme versEcran = getVersEcran();
    final GrPoint enw = nw_.applique(versEcran);
    final GrPoint ene = ne_.applique(versEcran);
    final GrPoint ese = se_.applique(versEcran);
    final GrPoint esw = sw_.applique(versEcran);
    final int xenw = (int) enw.x_;
    final int yenw = (int) enw.y_;
    final int xene = (int) ene.x_;
    final int yene = (int) ene.y_;
    final int xese = (int) ese.x_;
    final int yese = (int) ese.y_;
    final int xesw = (int) esw.x_;
    final int yesw = (int) esw.y_;
    if (((xenw < cxmin) && (xene < cxmin) && (xesw < cxmin) && (xese < cxmin))
        || ((xenw > cxmax) && (xene > cxmax) && (xesw > cxmax) && (xese > cxmax))
        || ((yenw < cymin) && (yene < cymin) && (yesw < cymin) && (yese < cymin))
        || ((yenw > cymax) && (yene > cymax) && (yesw > cymax) && (yese > cymax))) {
      ;
    } else
    {
      if (getImage() != null) {
        final int wi = image_.getWidth(this);
        final int hi = image_.getHeight(this);
        if ((pixels_ == null) || (pixels_.length != wi * hi)) {
          final Image image = (attenue ? createImage(new FilteredImageSource(getImage().getSource(), BuFilters.BRIGHTER))
              : getImage());
          if (wi * hi <= 0) {
            return;
          }
          pixels_ = new int[wi * hi];
          final PixelGrabber pg = new PixelGrabber(image, 0, 0, wi, hi, pixels_, 0, wi);
          try {
            pg.grabPixels();
          } catch (final InterruptedException ex) {
            FuLog.warning("interrupted waiting for pixels!");
            return;
          }
          if ((pg.getStatus() & ImageObserver.ABORT) != 0) {
            FuLog.warning("image fetch aborted or errored");
            return;
          }
        }
        int rwx = Math.max(1, (int) (enw.distanceXY(ene) / wi));
        int rwy = Math.max(1, (int) (esw.distanceXY(enw) / hi));
        if (rapide) {
          rwx *= 5;
          rwy *= 5;
        }
        final int[][] exp = bresenhamN(xenw, yenw, xene, yene, rwx);
        final int[][] eyp = bresenhamN(0, 0, xesw - xenw, yesw - yenw, rwy);
        final int exl = exp.length;
        final int eyl = eyp.length;
        int nbLignesDehors = 0;
        int nbPixelsDehors = 0;
        for (int j = 0; j < eyl - 1; j++) {
          boolean dehors = false;
          final int x0 = exp[0][0] + eyp[j][0];
          final int x1 = exp[exl - 1][0] + eyp[j][0];
          if ((x0 < cxmin) && (x1 < cxmin)) {
            dehors = true;
          }
          if ((x0 > cxmax) && (x1 > cxmax)) {
            dehors = true;
          }
          final int y0 = exp[0][1] + eyp[j][1];
          final int y1 = exp[exl - 1][1] + eyp[j][1];
          if ((y0 < cymin) && (y1 < cymin)) {
            dehors = true;
          }
          if ((y0 > cymax) && (y1 > cymax)) {
            dehors = true;
          }
          if (dehors) {
            nbLignesDehors++;
          } else {
            for (int i = 0; i < exl - 1; i++) {
              dehors = false;
              final int x00 = exp[i][0] + eyp[j][0];
              final int x10 = exp[i + 1][0] + eyp[j][0];
              final int x11 = exp[i + 1][0] + eyp[j + 1][0];
              final int x01 = exp[i][0] + eyp[j + 1][0];
              if ((x00 < cxmin) && (x10 < cxmin) && (x11 < cxmin) && (x01 < cxmin)) {
                dehors = true;
              }
              if ((x00 > cxmax) && (x10 > cxmax) && (x11 > cxmax) && (x01 > cxmax)) {
                dehors = true;
              }
              final int y00 = exp[i][1] + eyp[j][1];
              final int y10 = exp[i + 1][1] + eyp[j][1];
              final int y11 = exp[i + 1][1] + eyp[j + 1][1];
              final int y01 = exp[i][1] + eyp[j + 1][1];
              if ((y00 < cymin) && (y10 < cymin) && (y11 < cymin) && (y01 < cymin)) {
                dehors = true;
              }
              if ((y00 > cymax) && (y10 > cymax) && (y11 > cymax) && (y01 > cymax)) {
                dehors = true;
              }
              if (dehors) {
                nbPixelsDehors++;
              } else {
                final int c = pixels_[j * hi / eyl * wi + i * wi / exl];
                if (((c >> 24) & 0xFF) == 0xFF) {
                  final int[] x = new int[] { x00, x10, x11, x01 };
                  final int[] y = new int[] { y00, y10, y11, y01 };
                  final Color couleur = new Color(c);
                  _g.setColor(couleur);
                  _g.fillPolygon(x, y, 4);
                }
              }
            }
          }
        }
      }
    }
    /*******************************************************************************************************************
     * A CONSERVER POUR LA 3D for(int i=0; i<wi; i++) for(int j=0; j<hi; j++) { int c=pixels[j*wi+i]; if(((c>>24) &
     * 0xFF) == 0xFF) { GrPoint rxy=nw_.addition(ne_.soustraction(nw_).multiplication((double)i/(double)wi))
     * .addition(sw_.soustraction(nw_).multiplication((double)j/(double)hi)); GrPoint exy=rxy.applique(versEcran); int
     * x=(int)exy.x; int y=(int)exy.y; _g.setColor(new Color(c)); _g.fillRect(x,y,2,2); } }
     ******************************************************************************************************************/
    super.paintComponent(_g);
    premier_ = false;
  }

  private int[][] bresenhamN(final int _x1, final int _y1, final int _x2, final int _y2,final  int _n) {
    int c, deltaX, deltaY, x, y, incrX, incrY, i;
    deltaX = Math.abs(_x2 - _x1) / _n;
    deltaY = Math.abs(_y2 - _y1) / _n;
    final int[][] r = new int[Math.max(deltaX, deltaY)][2];
    if ((_y2 - _y1) > 0) {
      incrY = _n;
    } else {
      incrY = -_n;
    }
    if ((_x2 - _x1) > 0) {
      incrX = _n;
    } else {
      incrX = -_n;
    }
    if (deltaX > deltaY) {
      c = 2 * deltaY - deltaX;
      y = _y1;
      x = _x1;
      for (i = 0; i < deltaX; i++) {
        r[i][0] = x;
        r[i][1] = y;
        x += incrX;
        if (c < 0) {
          c += (2 * deltaY);
        } else {
          y += incrY;
          c += (2 * deltaY - 2 * deltaX);
        }
      }
    } else {
      c = 2 * deltaX - deltaY;
      x = _x1;
      y = _y1;
      for (i = 0; i < deltaY; i++) {
        r[i][0] = x;
        r[i][1] = y;
        y += incrY;
        if (c < 0) {
          c += (2 * deltaX);
        } else {
          x += incrX;
          c += (2 * deltaX - 2 * deltaY);
        }
      }
    }
    return r;
  }

  /**
   * Rend visible (ou pas) ce calque.
   */
  @Override
  public void setVisible(final boolean _b) {
    premier_ = true;
    super.setVisible(_b);
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible()) {
      r = super.getDomaine();
      if (nw_ != null) {
        if (r == null) {
          r = new GrBoite();
        }
        r.ajuste(nw_);
        r.ajuste(ne_);
        r.ajuste(sw_);
        r.ajuste(se_);
      }
    }
    return r;
  }

  // Proprietes
  /**
   * Affectation de la propriete <I>attenue</I>. Destruction du cache.
   */
  @Override
  public  void setAttenue(final boolean _attenue) {
    if (isAttenue() != _attenue) {
      pixels_ = null;
      super.setAttenue(_attenue);
    }
  }
  private Image image_;

  /**
   * Accesseur de la propriete <I>image</I>.
   */
  public  Image getImage() {
    return image_;
  }

  /**
   * Afectation de la propriete <I>image</I>.
   */
  public  void setImage(final Image _v) {
    if (image_ != _v) {
      final Image vp = image_;
      image_ = _v;
      pixels_ = null;
      firePropertyChange("image", vp, _v);
    }
  }
  private Color couleur_;

  /**
   * Accesseur de la propriete <I>couleur</I>. C'est la couleur de remplissage en mode rapide.
   */
  @Override
  public Color getCouleur() {
    return couleur_;
  }

  /**
   * Affectation de la propriete <I>couleur</I>.
   */
  @Override
  public boolean setCouleur(final Color _v) {
    if (couleur_ != _v) {
      final Color vp = couleur_;
      couleur_ = _v;
      firePropertyChange("couleur", vp, _v);
      return true;
    }
    return false;
  }
  private GrPoint nw_;

  /**
   * Accesseur de la propriete <I>nw</I> (Nord-Ouest). Coin de l'image en coordonnees reelles.
   */
  public GrPoint getNW() {
    return nw_;
  }

  /**
   * Affectation de la propriete <I>nw</I>.
   */
  public void setNW(final GrPoint _v) {
    if (nw_ != _v) {
      final GrPoint vp = nw_;
      nw_ = _v;
      firePropertyChange("NW", vp, _v);
    }
  }
  private GrPoint ne_;

  /**
   * Accesseur de la propriete <I>ne</I> (Nord-Est). Coin de l'image en coordonnees reelles.
   */
  public GrPoint getNE() {
    return ne_;
  }

  /**
   * Affectation de la propriete <I>ne</I>.
   */
  public void setNE(final GrPoint _v) {
    if (ne_ != _v) {
      final GrPoint vp = ne_;
      ne_ = _v;
      firePropertyChange("NE", vp, _v);
    }
  }
  private GrPoint se_;

  /**
   * Accesseur de la propriete <I>se</I> (Sud-Est). Coin de l'image en coordonnees reelles.
   */
  public GrPoint getSE() {
    return se_;
  }

  /**
   * Affectation de la propriete <I>se</I>.
   */
  public void setSE(final GrPoint _v) {
    if (se_ != _v) {
      final GrPoint vp = se_;
      se_ = _v;
      firePropertyChange("SE", vp, _v);
    }
  }
  private GrPoint sw_;

  /**
   * Accesseur de la propriete <I>sw</I> (Sud-Ouest). Coin de l'image en coordonnees reelles.
   */
  public GrPoint getSW() {
    return sw_;
  }

  /**
   * Affectation de la propriete <I>so</I>.
   */
  public void setSW(final GrPoint _v) {
    if (sw_ != _v) {
      final GrPoint vp = sw_;
      sw_ = _v;
      firePropertyChange("SW", vp, _v);
    }
  }
}
