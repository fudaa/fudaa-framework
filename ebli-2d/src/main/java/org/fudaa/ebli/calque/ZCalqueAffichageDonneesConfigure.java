/*
 * @creation 9 nov. 06
 * @modification $Date: 2007-05-04 13:49:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuCheckBox;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurAlpha;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurInterface;

/**
 * @author fred deniger
 * @version $Id: ZCalqueAffichageDonneesConfigure.java,v 1.4 2007-05-04 13:49:43 deniger Exp $
 */
public class ZCalqueAffichageDonneesConfigure extends BCalqueSectionConfigure {

  boolean addVisible_;
  boolean addColor_ = true;

  public final static String ANTIALIAS = "calque.antialias";

  public ZCalqueAffichageDonneesConfigure(final ZCalqueAffichageDonneesAbstract _target, final boolean _addVisible) {
    super(_target);
    addVisible_ = _addVisible;
  }

  public ZCalqueAffichageDonneesConfigure(final ZCalqueAffichageDonneesAbstract _target) {
    this(_target, false);
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (_newProp == null) {
      return false;
    }
    if (_key == BSelecteurAlpha.DEFAULT_PROPERTY) {
      ((ZCalqueAffichageDonneesAbstract) target_).setAlpha(((Integer) _newProp).intValue());
      return true;
    }
    if (_key == BSelecteurCheckBox.PROP_VISIBLE) {
      ((ZCalqueAffichageDonneesAbstract) target_).setVisible(((Boolean) _newProp).booleanValue());
      return true;
    }
    if (_key == BSelecteurCheckBox.PROP_USER_VISIBLE) {
      ((ZCalqueAffichageDonneesAbstract) target_).setUserVisible(((Boolean) _newProp).booleanValue());
      return true;
    }
    if (_key == ANTIALIAS) {
      ((ZCalqueAffichageDonneesAbstract) target_).antialiasing_ = ((Boolean) _newProp).booleanValue();
      ((ZCalqueAffichageDonneesAbstract) target_).repaint();
      return true;
    }
    return super.setProperty(_key, _newProp);
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == BSelecteurAlpha.DEFAULT_PROPERTY) {
      return new Integer(((ZCalqueAffichageDonneesAbstract) target_).getAlpha());
    }
    if (_key == ANTIALIAS) {
      return Boolean.valueOf(((ZCalqueAffichageDonneesAbstract) target_).antialiasing_);
    }
    if (_key == BSelecteurCheckBox.PROP_VISIBLE) {
      return Boolean.valueOf(target_.isVisible());
    }
    if (_key == BSelecteurCheckBox.PROP_USER_VISIBLE) {
      return Boolean.valueOf(target_.isUserVisible());
    }
    return super.getProperty(_key);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final List dest = new ArrayList();
    if (addVisible_) {
      dest.add(new BSelecteurCheckBox(BSelecteurCheckBox.PROP_USER_VISIBLE, new BuCheckBox()));
    }
    if (((ZCalqueAffichageDonneesAbstract) target_).isAntialiasSupported()) {
      final BSelecteurCheckBox cbAntialias = new BSelecteurCheckBox(ANTIALIAS);
      cbAntialias.setTitle(EbliLib.getS("Lissage"));
      cbAntialias.setTooltip(EbliLib.getS("Permet de lisser les trac�s"));
      dest.add(cbAntialias);
    }
    if (addColor_) {
      dest.add(new BSelecteurColorChooserBt());
    }
    dest.add(new BSelecteurAlpha());
    return (BSelecteurInterface[]) dest.toArray(new BSelecteurInterface[dest.size()]);
  }

  public boolean isAddColor() {
    return addColor_;
  }

  public void setAddColor(final boolean _addColor) {
    addColor_ = _addColor;
  }
}
