/*
 *  @creation     19 sept. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuSeparator;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.action.CalqueActionInteraction;
import org.fudaa.ebli.calque.action.CalqueActionTable;
import org.fudaa.ebli.calque.action.CalqueSelectionAction;
import org.fudaa.ebli.calque.action.SceneZoomOnSelectedAction;
import org.fudaa.ebli.commun.*;
import org.fudaa.ebli.controle.BConfigurePaletteAction;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.palette.BPaletteInfo;
import org.fudaa.ebli.palette.BPaletteInfoAbstractAction;
import org.fudaa.ebli.palette.PaletteEditAction;
import org.fudaa.ebli.repere.BControleRepereTexte;
import org.fudaa.ebli.repere.ZTransformationDomaine;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un controleur des actions du panneau d'affichage 2D des calques. Il gere en particulier les actions de navigation, de selection. Il g�re �galement
 * les actions exclusives. Une action est exclusive lorsqu'elle d�sactive une autre action en cours.
 * <p>
 * Exemple : une action s�lection d�sactivera une action zoom en cours.
 * <p>
 * Ce controleur peut �galement g�rer des actions exclusives sp�cifiques.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class ZEbliCalquePanelController implements TreeSelectionListener, PropertyChangeListener {

  /**
   * Identifiant pour les propri�t�s concernant l'�tat d'un calque d'interaction.
   */
  public final static String STATE = "state";
  /**
   * Un curseur specifique pour l'accrochage.
   */
  public static final Cursor CURSOR_ACC = EbliResource.EBLI.getCursor("curseur_accroche", -1, new Point(1, 1));

  protected static EbliActionInterface[] removePaletteAction(final EbliActionInterface[] _i) {
    if (_i == null) {
      return null;
    }
    final List actions = new ArrayList(_i.length);
    for (int i = 0; i < _i.length; i++) {
      if (!(_i[i] instanceof EbliActionPaletteAbstract)) {
        actions.add(_i[i]);
      }
    }
    return (EbliActionInterface[]) actions.toArray(new EbliActionInterface[actions.size()]);
  }
  private BGroupeCalque cqAdmin_;
  private BCalqueInteraction cqInteractionActif_;
  private EbliAdapteurSuiviSouris suiviSouris_;
//  protected BCalque cqActif_;
  protected BCalquePositionnementInteraction cqDepVueI_;
  protected ZCalqueRepereInteraction cqZoomI_;
  protected EbliActionInterface[] navigationActionGroup_;
  protected EbliActionInterface[] specificActionGroup_;
  protected EbliActionInterface[] standardActionGroup_;
  protected ZCalqueSelectionInteractionAbstract cqSelectionI_;
  /**
   * Le calque interactif d'accrochage
   */
  protected ZCalqueCatchInteraction cqCatchI_;
  BPaletteInfoAbstractAction infoPalette_;
  CalqueInteractionListener interactionListener_;
  protected ZEbliCalquesPanel pn_;
  boolean isSpecificActionInit_;
  protected EbliActionInterface[] selectedActionGroup_;
  final protected CtuluUI ui_;
  boolean useInfo_;

  public ZEbliCalquePanelController(final boolean _useInfo, final CtuluUI _ui) {
    useInfo_ = _useInfo;
    ui_ = _ui;
  }

  public ZEbliCalquePanelController(final CtuluUI _ui) {
    this(true, _ui);
  }

  protected void createCqSelection() {
    cqSelectionI_ = new ZCalqueSelectionInteractionSimple(pn_.getDonneesCalque());
  }

  /**
   * Attention a n'utiliser que si les composants est d�truit et plus utilise.
   */
  public void clearListeners() {
    EbliLib.cleanListener(navigationActionGroup_);
    EbliLib.cleanListener(specificActionGroup_);
    EbliLib.cleanListener(standardActionGroup_);
  }

  public Cursor getDefautCursor() {
    if (cqInteractionActif_ != null && !cqInteractionActif_.isGele()) {
      return cqInteractionActif_.getSpecificCursor();
    }

    return getView().getVueCalque().getDefaultCursor();
  }

  private void activeActionForSelectedLayer() {
    // Le panneau des boutons de s�lection est invisible => Pas d'action.
    if (selectedActionGroup_ == null) {
      return;
    }
    boolean enable = false;
    if (!pn_.getScene().isRestrictedToCalqueActif()) {
      enable = true;
    } else {
      if (pn_.getScene().getCalqueActif() instanceof ZCalqueAffichageDonneesInterface) {
        final ZCalqueAffichageDonneesInterface z = (ZCalqueAffichageDonneesInterface) pn_.getScene().getCalqueActif();
        enable = (z.modeleDonnees() != null && pn_.getScene().getCalqueActif().isVisible());
      }
    }
    updateModeText();
    setSelectionEnabled(enable);
  }

  public final void buildActions() {
    if (navigationActionGroup_ == null) {
      buildButtonGroupNavigation();
    }
    if (selectedActionGroup_ == null) {
      buildButtonGroupSelection();
    }
    if (standardActionGroup_ == null) {
      buildButtonGroupStandard();
    }
    if (specificActionGroup_ == null) {
      buildButtonGroupSpecifique();
    }

  }

  public void setUseCacheInDonnees(boolean b) {
    pn_.getGcDonnees().setUseCache(b);
  }

  protected EbliActionInterface createRepereAction() {
    final EbliActionPaletteAbstract plAction = new EbliActionPaletteAbstract(EbliLib.getS("Rep�re"),
            EbliResource.EBLI.getIcon("repere"),
            "CHANGE_REFERENCE") {
              @Override
              public JComponent buildContentPane() {
                final BControleRepereTexte crt = new BControleRepereTexte(pn_.getVueCalque());
                crt.addRepereEventListener(pn_.getVueCalque());
                return crt;
              }
            };
    plAction.setParent(pn_);
    plAction.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Transformations du rep�re"));
    return plAction;
  }

  /**
   * Construction du groupe de navigation par defaut.
   */
  @SuppressWarnings("serial")
  protected void buildButtonGroupNavigation() {
    if (navigationActionGroup_ != null) {
      return;
    }
    navigationActionGroup_ = new EbliActionInterface[7];
    // RESTORE ACTION
    final EbliActionSimple restore = new EbliActionSimple(EbliLib.getS("Restaurer"), EbliResource.EBLI.getIcon("restore"),
            "RESTORE") {
              @Override
              public void actionPerformed(ActionEvent _arg) {
                pn_.restaurer();
              }
            };
    restore.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Restaurer la vue globale"));
    restore.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('r'));
    restore.putValue(EbliActionInterface.SECOND_KEYSTROKE, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    int i = 0;
    navigationActionGroup_[i++] = restore;
    // ZOOM ACTION
    CalqueActionInteraction action = new CalqueActionInteraction(EbliLib.getS("Zoom"), EbliResource.EBLI.getIcon("loupe"), "ZOOM",
            cqZoomI_);
    action.setDefaultToolTip(EbliLib.getZoomDesc());
    action.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('z'));

    navigationActionGroup_[i++] = action;
    navigationActionGroup_[i++] = new SceneZoomOnSelectedAction(pn_);
    // VIEW NAVIGATION

    navigationActionGroup_[i] = new EbliActionSimple(EbliLib.getS("Derni�re vue"), EbliResource.EBLI.getIcon("zoom-previous"),
            "LAST_VIEW") {
              @Override
              public void actionPerformed(final ActionEvent _arg) {
                pn_.getVueCalque().undoRepere();
              }
            };
    navigationActionGroup_[i].putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('y'));
    navigationActionGroup_[i++].setDefaultToolTip(EbliLib.getS("Retourner � la derni�re vue"));
    // action.putValue(Action.MNEMONIC_KEY, new Int);
    action = new CalqueActionInteraction(EbliLib.getS("D�placer la vue"), EbliResource.EBLI.getIcon("main"), "MOVE_VIEW",
            cqDepVueI_);
    action.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("D�placer la vue"));
    action.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('m'));
    navigationActionGroup_[i++] = action;
    // REFERENCE
    final EbliActionInterface createRepereAction = createRepereAction();
    if (createRepereAction != null) {
      navigationActionGroup_[i++] = createRepereAction;
    }
    // NAVIGATION
    final EbliActionPaletteAbstract plAction = new EbliActionPaletteAbstract(EbliLib.getS("Navigation"),
            EbliResource.EBLI.getIcon("navigation"), "NAVIGATE") {
              @Override
              public JComponent buildContentPane() {
                return new ZTransformationDomaine(pn_.getVueCalque());
              }
            };
    plAction.setParent(pn_);
    plAction.setResizable(true);
    plAction.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Outil de navigation"));
    navigationActionGroup_[i++] = plAction;
    // updateMapKeyStroke(navigationActionGroup_);
  }

  /**
   * Construction du groupe de selection par defaut.
   */
  protected void buildButtonGroupSelection() {
    if (selectedActionGroup_ != null) {
      return;
    }
    selectedActionGroup_ = new EbliActionInterface[2];
    int i = 0;
    // les eblistatebutton permettent de creer des etats et de mettre
    // a jour la fenetre en fonction.
    CalqueSelectionAction action = new CalqueSelectionAction(this, EbliResource.EBLI.getIcon("fleche"),
            "RECTANGLE_SELECTION", ZCalqueSelectionInteractionAbstract.RECTANGLE);
    action.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('s'));
    final String tltTip = "<table><tr><td><b>" + EbliLib.getS("Raccourci") + "</b></td><td><b>"
            + EbliLib.getS("Action") + "</td></tr><tr><td><i>Shift</i></td><td>"
            + EbliLib.getS("Ajouter � la s�lection courante") + "</td></tr><tr><td><i>Ctrl</i></td><td>"
            + EbliLib.getS("Enlever de la s�lection courante") + "</td></tr><tr><td><i>Ctrl+Shift</i></td><td>"
            + EbliLib.getS("Op�ration OU Exclusif") + "</td></tr><tr><td><i>Ctrl+Alt</i> (opt.) </td><td>"
            + EbliLib.getS("S�lectionner entre 2 points") + "</td></tr></table>";;
    action.setDefaultToolTip(tltTip);
    selectedActionGroup_[i++] = action;

    action = new CalqueSelectionAction(this, EbliResource.EBLI.getIcon("fleche-polygone"), "POLYGON_SELECTION",
            ZCalqueSelectionInteractionAbstract.POLYGONE);
    action.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('p'));
    action.setDefaultToolTip(tltTip);
    selectedActionGroup_[i++] = action;
    activeActionForSelectedLayer();
  }

  /**
   * Construction du groupe standard par defaut.
   */
  protected void buildButtonGroupStandard() {
    if (standardActionGroup_ != null) {
      return;
    }
    final ArrayList l = new ArrayList(6);

    // PALET
    final EbliActionPaletteSpecAbstract plAction = PaletteEditAction.build(pn_.getArbreCalqueModel().getTreeSelectionModel());
    plAction.setParent(pn_);
    l.add(plAction);
    EbliActionPaletteAbstract ebliActionPaletteAbstract = new EbliActionPaletteAbstract(EbliLib.getS("L�gende"),
            EbliResource.EBLI.getToolIcon("legend"),
            "EDIT_LEGEND", true) {
              @Override
              public JComponent buildContentPane() {
                return new BCalqueLegendeCustomizer(getView().getCqLegend());
              }
            };
    ebliActionPaletteAbstract.setParent(pn_);
    l.add(ebliActionPaletteAbstract);
    l.add(new BConfigurePaletteAction(getView().getArbreCalqueModel().getTreeSelectionModel()));
    if (useInfo_) {
      infoPalette_ = new BPaletteInfoAbstractAction() {
        @Override
        protected boolean mustBeUpdated() {
          return getCalqueActif() != null;
        }

        @Override
        public JComponent buildContentPane() {
          return new BCalquePaletteInfo(pn_.getScene(), pn_.getArbreCalqueModel(), pn_.gisEditor_.getMng());
        }
      };
      l.add(infoPalette_);
      l.add(new CalqueActionTable(pn_.getArbreCalqueModel(), ui_, pn_));
    }
    standardActionGroup_ = new EbliActionInterface[l.size()];
    l.toArray(standardActionGroup_);
    // this.updateMapKeyStroke(standardActionGroup_);
  }

  public EbliActionInterface[] buildButtonGroupSpecifique() {
    return null;
  }

  public EbliActionInterface getSpecificAction(final String _name) {
    if (specificActionGroup_ != null) {
      for (int i = specificActionGroup_.length - 1; i >= 0; i--) {
        if (_name.equals(specificActionGroup_[i].getValue(Action.NAME))) {
          return specificActionGroup_[i];
        }
      }
    }
    return null;
  }

  protected final EbliActionInterface[] getSpecificActionGroup() {
    return specificActionGroup_;
  }

  /**
   * Doit retourner les noms des actions palettes qui pourront etre affich�es dans des tab lors d'un affichage dans les widgets
   *
   * @return liste vide par defaut
   */
  public Collection<EbliActionPaletteAbstract> getTabPaletteAction() {
    return Collections.emptyList();
  }

  public final CtuluUI getUI() {
    return ui_;
  }

  /**
   * A overrider.
   *
   * @return
   */
  public BPaletteInfo getPaletteInfo() {
    return null;
  }

  protected void removePaletteAction() {
    navigationActionGroup_ = removePaletteAction(navigationActionGroup_);
    selectedActionGroup_ = removePaletteAction(selectedActionGroup_);
    standardActionGroup_ = removePaletteAction(standardActionGroup_);
    specificActionGroup_ = removePaletteAction(specificActionGroup_);
  }

  /**
   * Modifie la propriete <code>enable</code> de tous les boutons du groupe selection.
   *
   * @param _enable
   */
  protected void setSelectionEnabled(final boolean _enable) {
    if (Fu.DEBUG && FuLog.isDebug()) {
      CtuluLibMessage.debug("groupe selection active " + _enable);
    }
    EbliLib.setActionEnable(selectedActionGroup_, _enable);
  }

  protected void updateKeyMap(final JComponent _c) {
    EbliLib.updateMapKeyStroke(_c, selectedActionGroup_);
    EbliLib.updateMapKeyStroke(_c, navigationActionGroup_);
    EbliLib.updateMapKeyStroke(_c, standardActionGroup_);
    EbliLib.updateMapKeyStroke(_c, specificActionGroup_);
  }

  void build() {
    cqAdmin_ = new BGroupeCalque();
    cqAdmin_.setName("cqADMIN");
    cqAdmin_.setTitle(EbliLib.getS("Administration"));
    cqAdmin_.setDestructible(false);

    // le calque etl'affichage du suivi des coordonnees.
    final BCalqueSuiviSourisInteraction suivi = new BCalqueSuiviSourisInteraction();
    suivi.setTitle(EbliLib.getS("Position"));
    suivi.setName("cqPOSITION");
    suivi.setDestructible(false);
    suiviSouris_ = new EbliAdapteurSuiviSouris(pn_.getLabelSuiviSouris());
    suivi.addCoordonneesListener(suiviSouris_);
    addCalqueInteraction(suivi);

    // Le calque d'accrochage
    // Attention : L'ajout du calque en premier est tr�s important, l'accrochage ne
    // fonctionne que si ce calque recoit les evenements MouseEvent en premier lors de la propagation.
    cqCatchI_ = new ZCalqueCatchInteraction();
    cqCatchI_.setTitle(EbliLib.getS("Accrochage"));
    cqCatchI_.setName("cqACCROCHAGE");
    cqCatchI_.setDestructible(false);
    cqCatchI_.addCatchListener(new ControllerCatchListener());
    pn_.getVueCalque().addKeyListener(cqCatchI_);
    addCalqueInteraction(cqCatchI_);
    createCqSelection();
    // cqSelectionI_ = new ZCalqueSelectionInteractionMulti();
    cqSelectionI_.setTitle(EbliLib.getS("Interaction"));
    cqSelectionI_.setName("cqSELECTION");
    cqSelectionI_.setDestructible(false);
    cqSelectionI_.setGele(true);
    cqSelectionI_.addPropertyChangeListener(ZEbliCalquePanelController.STATE, this);
    pn_.getVueCalque().addKeyListener(cqSelectionI_);
    addCalqueInteraction(cqSelectionI_);
    // Calque de zoom
    cqZoomI_ = new ZCalqueRepereInteraction(pn_.getVueCalque(), pn_.getDonneesCalque());
    cqZoomI_.addPropertyChangeListener(ZEbliCalquePanelController.STATE, this);
    cqZoomI_.setTitle(EbliLib.getS("Agrandir"));
    cqZoomI_.setName("cqAGRANDIR");
    cqZoomI_.setDestructible(false);
    cqZoomI_.setGele(true);
    pn_.addKeyListener(cqZoomI_);
    addCalqueInteraction(cqZoomI_);
    // Calque de d�placement de vue
    cqDepVueI_ = new BCalquePositionnementInteraction();
    cqDepVueI_.setName("cqDEPLACEMENT_VUE-I");
    cqDepVueI_.setDefaultCalqueInteraction(cqSelectionI_);
    cqDepVueI_.setTitle(EbliLib.getS("D�placement de vue"));
    cqDepVueI_.setVueCalque(pn_.getVueCalque());
    cqDepVueI_.setGele(true);
    addCalqueInteraction(cqDepVueI_);
  }

  /**
   * @param _b un nouveau calque d'interaction a ajouter
   */
  public void addCalqueInteraction(final BCalqueInteraction _b) {
    cqAdmin_.add(_b);
    if (interactionListener_ == null) {
      interactionListener_ = new CalqueInteractionListener();
    }
    _b.addPropertyChangeListener("gele", interactionListener_);

    if (_b instanceof ZCatchListener) {
      cqCatchI_.addCatchListener((ZCatchListener) _b);
    }
  }

  public BCalqueInteraction getCalqueInteraction(String name) {
    return (BCalqueInteraction) cqAdmin_.getCalqueParNom(name);
  }

  public ZCalqueSondeInteraction addCalqueSondeInteraction() {
    ZCalqueSondeInteraction r = getCalqueSondeInteraction();
    if (r == null) {
      r = new ZCalqueSondeInteraction(getView().getScene());
      r.setName("cqSonde");
      addCalqueInteraction(r);
    }
    return r;
  }

  /**
   * Methode pour vider la selection.
   */
  public void clearSelections() {
    cqSelectionI_.clearSelections();
  }

  public void addSelectionActionTo(final JMenu _m) {
    EbliComponentFactory.INSTANCE.addActionsToMenu(selectedActionGroup_, _m);
  }

  public void fillMenuWithToolsActions(final JMenu _m) {
    buildActions();
    EbliComponentFactory.INSTANCE.addActionsToMenu(selectedActionGroup_, _m);
    if (selectedActionGroup_ != null) {
      _m.add(new BuSeparator());
    }
    EbliComponentFactory.INSTANCE.addActionsToMenu(navigationActionGroup_, _m);
    if (navigationActionGroup_ != null) {
      _m.add(new BuSeparator());
    }
    EbliComponentFactory.INSTANCE.addActionsToMenu(standardActionGroup_, _m);
    if (specificActionGroup_ != null) {
      if (standardActionGroup_ != null) {
        _m.add(new BuSeparator());
      }
      EbliComponentFactory.INSTANCE.addActionsToMenu(specificActionGroup_, _m);
    }
  }

  public void fillMenuWithToolsActions(final JPopupMenu _popupMenu) {
    buildActions();
    EbliComponentFactory.INSTANCE.addActionsToMenu(selectedActionGroup_, _popupMenu);
    if (selectedActionGroup_ != null) {
      _popupMenu.add(new BuSeparator());
    }
    EbliComponentFactory.INSTANCE.addActionsToMenu(navigationActionGroup_, _popupMenu);
    if (navigationActionGroup_ != null) {
      _popupMenu.add(new BuSeparator());
    }
    EbliComponentFactory.INSTANCE.addActionsToMenu(standardActionGroup_, _popupMenu);
    if (specificActionGroup_ != null) {
      if (standardActionGroup_ != null) {
        _popupMenu.add(new BuSeparator());
      }
      EbliComponentFactory.INSTANCE.addActionsToMenu(specificActionGroup_, _popupMenu);
    }
  }

  public List<EbliActionInterface> getActions() {
    final List<EbliActionInterface> actions = getNonSpecificsActions();
    initSpecificActions();
    if (specificActionGroup_ != null) {
      actions.addAll(Arrays.asList(specificActionGroup_));
    }
    return actions;
  }

  /**
   * Seulement les actions non specifiques d'une vue 2d.
   *
   * @return
   */
  public List<EbliActionInterface> getNonSpecificsActions() {
    final List<EbliActionInterface> actions = new ArrayList<EbliActionInterface>();
    buildActions();
    actions.addAll(Arrays.asList(selectedActionGroup_));
    actions.add(null);
    actions.addAll(Arrays.asList(navigationActionGroup_));
    actions.add(null);
    if (!CtuluLibArray.isEmpty(standardActionGroup_)) {
      actions.addAll(Arrays.asList(standardActionGroup_));
    }

    return actions;
  }

  /**
   * @param _name le nom de l'action a trouver: Action.ACTION_COMMAND_KEY
   * @return l'action trouvee ou null sinon
   */
  public EbliActionInterface findAction(String _name) {
    List<EbliActionInterface> l = getActions();
    for (Iterator<EbliActionInterface> iterator = l.iterator(); iterator.hasNext();) {
      EbliActionInterface act = iterator.next();
      if (act != null && act.getValue(Action.ACTION_COMMAND_KEY).equals(_name)) {
        return act;
      }
    }
    return null;
  }

  public void initSpecificActions() {
    if (!isSpecificActionInit_) {
      specificActionGroup_ = getApplicationActions();
      isSpecificActionInit_ = true;
    }
  }

  public void initTabAndSpecificAction() {
  }

  protected EbliActionInterface[] getApplicationActions() {
    return pn_.getApplicationActions();
  }

  public EbliActionInterface getSelectionRectangleAction() {
    buildButtonGroupSelection();
    return selectedActionGroup_[0];
  }

  public EbliActionInterface[] getBaseActions() {
    final ArrayList l = new ArrayList(20);
    if (selectedActionGroup_ != null) {
      l.addAll(Arrays.asList(selectedActionGroup_));
    }
    if (navigationActionGroup_ != null) {
      l.addAll(Arrays.asList(navigationActionGroup_));
    }
    if (standardActionGroup_ != null) {
      l.addAll(Arrays.asList(standardActionGroup_));
    }
    return (EbliActionInterface[]) l.toArray(new EbliActionInterface[l.size()]);
  }

  /**
   * Renvoie le calque actif ( selectionne dans l'arbre).
   */
  public BCalque getCalqueActif() {
    // return cqActif_;
    return pn_.getScene().getCalqueActif();
  }

  public final EbliActionInterface[] getCalqueActions(final BCalque _c) {
    if (_c == null) {
      return null;
    }
    return _c.getActions();
  }

  public ZCalqueSondeInteraction getCalqueSondeInteraction() {
    return (ZCalqueSondeInteraction) cqAdmin_.getCalqueParNom("cqSonde");
  }

  /**
   * @return le calque de selection
   */
  public final ZCalqueSelectionInteractionAbstract getCqSelectionI() {
    return cqSelectionI_;
  }

  public final ZCalqueCatchInteraction getCqCatchI() {
    return cqCatchI_;
  }

  /**
   * @deprecated Use getCoordinateDefinitions() instead.
   * @return
   */
  public final EbliFormatterInterface getEbliFormatter() {
    return suiviSouris_.format();
  }

  public EbliActionInterface[] getNavigationActions() {
    buildButtonGroupNavigation();
    return navigationActionGroup_;
  }

  public final ZEbliCalquesPanel getView() {
    return pn_;
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if ("visible".equals(_evt.getPropertyName())) {
      activeActionForSelectedLayer();
    } else if ((_evt.getSource() == pn_.getScene().getCalqueActif())
            || ((cqInteractionActif_ != null) && (_evt.getSource() == cqInteractionActif_))) {
      updateModeText();
    }
  }

  public void removeCalqueInteraction(final BCalqueInteraction _b) {
    if (_b == null) {
      return;
    }
    if (cqInteractionActif_ == _b) {
      cqInteractionActif_ = null;
    }
    _b.setGele(true);
    if (interactionListener_ != null) {
      _b.removePropertyChangeListener("gele", interactionListener_);
    }
    cqAdmin_.remove(_b);

    if (_b instanceof ZCatchListener) {
      cqCatchI_.removeCatchListener((ZCatchListener) _b);
    }
  }

  /**
   * Restaure le viewport sur le domaine �tendu � tous les calques.
   */
  public void restaurer() {
    final BCalqueInteraction cq = unsetCurrentInteractifCalque();
    final GrBoite b = pn_.getVueCalque().getCalque().getDomaine();
    if ((b == null) || b.isIndefinie()) {
      return;
    }
    pn_.getVueCalque().changeRepere(this, b);
    setCalqueInteractionActif(cq);
  }

  public void setCalqueInteractionActif(final BCalqueInteraction _b) {
    if (cqInteractionActif_ == _b) {
      if (cqInteractionActif_ != null) {
        cqInteractionActif_.setGele(false);
      }
      if (_b == cqSelectionI_) {
        updateModeText();
      }
      return;
    }
    if (_b == null) {
      cqInteractionActif_.setGele(true);
      updateModeText();
      cqInteractionActif_ = null;
      pn_.getVueCalque().setDefaultCursor();
    } else {
      if (cqInteractionActif_ != null && !cqInteractionActif_.isGele()) {
        cqInteractionActif_.setGele(true);
      }
      if (cqAdmin_.getCalqueParNom(_b.getName()) == null) {
        CtuluLibMessage.error(getClass().getName() + " unknown layer " + _b);
      } else {
        if (_b.isGele()) {
          _b.setGele(false);
        }
        cqInteractionActif_ = _b;
        updateModeText();
        final Cursor c = _b.getSpecificCursor();
        if (c == null) {
          pn_.getVueCalque().setDefaultCursor();
        } else {
          pn_.getVueCalque().setCursor(c);
        }
      }
    }
  }

  public void setCalqueSelectionActif() {
    setCalqueInteractionActif(cqSelectionI_);
  }

  /**
   * @return true if at least one interaction layer is enable
   */
  public boolean isCalqueInterationEnabled() {
    return cqInteractionActif_ != null && !cqInteractionActif_.isGele();

  }

  /**
   * @deprecated Use setCoordinateDefinitions() instead.
   * @param _xyFormatter
   */
  public final void setEbliFormatter(final EbliFormatterInterface _xyFormatter) {
    suiviSouris_.format(_xyFormatter);
  }

  /**
   * Affecte les d�finitions pour les coordonn�es affich�es dans la barre de statut.
   *
   * @param _defs Les d�finitions.
   */
  public void setCoordinateDefinitions(EbliCoordinateDefinition[] _defs) {
    suiviSouris_.setCoordinateDefinitions(_defs);
  }

  /**
   * Retourne les d�finition des coordonn�es du syst�me de coordonn�es.
   *
   * @return Les d�finitions
   */
  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return suiviSouris_.getCoordinateDefinitions();
  }

  public void setInfoPaletteActive() {
    if (infoPalette_ != null && !infoPalette_.isSelected()) {
      infoPalette_.changeAll();
    }
  }

  /*
   * public final void setSpecificActionGroup(EbliActionInterface[] _specificActionGroup) { specificActionGroup_ =
   * _specificActionGroup; }
   */
  public void setView(final ZEbliCalquesPanel _view) {
    if (pn_ != null) {
      throw new IllegalArgumentException("one view per controller");
    }
    pn_ = _view;
    pn_.getArbreCalqueModel().addTreeSelectionListener(this);
    build();
    pn_.getVueCalque().getCalque().enPremier(cqAdmin_);
  }

  public void unsetCalqueInteractionActif(final BCalqueInteraction _b) {
    if (_b != null && !_b.isGele()) {
      _b.setGele(true);
    }
    if (_b == cqInteractionActif_) {
      cqInteractionActif_ = null;
      updateModeText();
      pn_.getVueCalque().setDefaultCursor();
    }
  }

  public BCalqueInteraction unsetCurrentInteractifCalque() {
    final BCalqueInteraction r = cqInteractionActif_;
    unsetCalqueInteractionActif(r);
    return r;
  }

  public final void updateInfoComponent() {
    if (infoPalette_ != null) {
      infoPalette_.updateState();
    }
  }

  public void updateModeText() {
    String txt = cqInteractionActif_ == null ? null : cqInteractionActif_.getDescription();
    if (txt == null) {
      txt = CtuluLibString.EMPTY_STRING;
    }
    //TODO Traduire SEGMENT.
    if (pn_.getScene().getCalqueActif() instanceof ZCalqueGeometry) {
      SelectionMode mode = ((ZCalqueGeometry) pn_.getScene().getCalqueActif()).getSelectionMode();

      if (mode == SelectionMode.ATOMIC) {
        txt = "<u>" + EbliLib.getS("SOMMET") + "</u>: " + txt;
      } else if (mode == SelectionMode.SEGMENT) {
        txt = "<u>" + EbliLib.getS("SEGMENT") + "</u>: " + txt;
      }
    }
    pn_.setModeText(txt);
  }

  /**
   * TreeSelection (mise a jour des etats des boutons de Selection en fonction du calque). Affecte le calque actif ( <code>getCalqueActif</code>) de
   * cette classe, modifie les calque actifs du calque de selection ( un seul calque actif) et met a jour l'etat des specific tools (si option
   * validee).
   *
   * @param _evt
   */
  @Override
  public void valueChanged(final TreeSelectionEvent _evt) {
    if (getCalqueActif() != null) {
      getCalqueActif().removePropertyChangeListener("visible", this);
      getCalqueActif().setActionsEnable(false);
    }
    pn_.getScene().setCalqueActif(pn_.getArbreCalqueModel().getSelectedCalque());
    if (pn_.getScene().getCalqueActif() != null) {
      getCalqueActif().setActionsEnable(true);
    }
    if (getCalqueActif() != null) {
      getCalqueActif().addPropertyChangeListener("visible", this);
    }

    activeActionForSelectedLayer();
  }
  public ZEbliCalquesPanel getPn() {
    return pn_;
  }

  public List<EbliActionInterface> getSelectedNavigationAndStandardActionGroup() {
    final List<EbliActionInterface> actions = new ArrayList<EbliActionInterface>();
    if (this.selectedActionGroup_ == null) {
      buildButtonGroupSelection();
    }
    if (this.navigationActionGroup_ == null) {
      buildButtonGroupNavigation();
    }
    if (this.standardActionGroup_ == null) {
      buildButtonGroupStandard();
    }

    actions.addAll(Arrays.asList(selectedActionGroup_));
    actions.add(null);
    actions.addAll(Arrays.asList(navigationActionGroup_));
    actions.add(null);
    if (!CtuluLibArray.isEmpty(standardActionGroup_)) {
      actions.addAll(Arrays.asList(standardActionGroup_));
    }

    return actions;
  }

  // Gestion des properties listeners
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    interactionListener_.addPropertyChangeListener(listener);
  }

  public void removePropertyChangeListener(PropertyChangeListener listener) {
    interactionListener_.removePropertyChangeListener(listener);
  }

  public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    interactionListener_.addPropertyChangeListener(propertyName, listener);
  }

  public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    interactionListener_.removePropertyChangeListener(propertyName, listener);
  }

  protected void firePropertyChange(Object _source, String _propertyName, Object _oldValue, Object _newValue) {
    interactionListener_.firePropertyChange(_source, _propertyName, _oldValue, _newValue);
  }

  class CalqueInteractionListener implements PropertyChangeListener {

    private HashMap<String, List<PropertyChangeListener>> listeners_;

    public CalqueInteractionListener() {
      listeners_ = new HashMap<String, List<PropertyChangeListener>>();
    }

    @Override
    public void propertyChange(final PropertyChangeEvent _evt) {
      final BCalqueInteraction c = (BCalqueInteraction) _evt.getSource();
      if (c.isGele()) {
        unsetCalqueInteractionActif(c);
      } else {
        setCalqueInteractionActif(c);
      }
      firePropertyChange(_evt.getSource(), _evt.getPropertyName(), _evt.getOldValue(), _evt.getNewValue());
    }

    // Gestion des properties listeners
    public void addPropertyChangeListener(PropertyChangeListener listener) {
      if (listener != null) {
        if (!listeners_.containsKey("all")) {
          listeners_.put("all", new ArrayList<PropertyChangeListener>());
        }
        listeners_.get("all").add(listener);
      }
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
      if (listener != null) {
        for (Entry<String, List<PropertyChangeListener>> element : listeners_.entrySet()) {
          listeners_.get(element.getKey()).remove(listener);
          if (listeners_.get(element.getKey()).size() == 0) {
            listeners_.remove(element.getKey());
          }
        }
      }
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
      if (listener != null) {
        if (!listeners_.containsKey(propertyName)) {
          listeners_.put(propertyName, new ArrayList<PropertyChangeListener>());
        }
      }
      listeners_.get(propertyName).add(listener);

    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
      if (listener != null && listeners_.containsKey(propertyName)) {
        listeners_.get(propertyName).remove(listener);
        if (listeners_.get(propertyName).size() == 0) {
          listeners_.remove(propertyName);
        }
      }
    }

    protected void firePropertyChange(Object _source, String _propertyName, Object _oldValue, Object _newValue) {
      if (listeners_.containsKey(_propertyName)) {
        PropertyChangeEvent event = new PropertyChangeEvent(_source, _propertyName, _oldValue, _newValue);
        for (int i = 0; i < listeners_.get(_propertyName).size(); i++) {
          listeners_.get(_propertyName).get(i).propertyChange(event);
        }
      }
    }
  }

  class ControllerCatchListener implements ZCatchListener {

    @Override
    public void catchChanged(ZCatchEvent _evt) {
      // Si le calque interactif courant n'est pas un calque gerant l'accrochage, on passe.
      // TODO : Le calque interactif d'accrochage pourrait n'etre actif que si le calque courant interactif est un calque
      // implementant ZCatchListener
      if (isCachingDisable()) {
        return;
      }
      pn_.getVueCalque().setCursor(_evt.type == ZCatchEvent.CAUGHT ? CURSOR_ACC : cqInteractionActif_.getSpecificCursor());

      if (_evt.type == ZCatchEvent.CAUGHT) {
        pn_.setAccrocheText(_evt);
      } else {
        pn_.unsetAcrrocheText();
      }
    }

    @Override
    public boolean isCachingEnabled() {
      return !isCachingDisable();
    }

    public boolean isCachingDisable() {
      return cqInteractionActif_ == null || !(cqInteractionActif_ instanceof ZCatchListener)
              || !((ZCatchListener) cqInteractionActif_).isCachingEnabled();
    }
  }
}
