/*
 GPL 2
 */
package org.fudaa.ebli.calque.edition;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionSimple;
import org.fudaa.ebli.calque.ZCatchEvent;
import org.fudaa.ebli.calque.ZCatchListener;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;

/**
 *
 * @author Frederic Deniger
 */
public class SelectionMoveInteraction extends ZCalqueSelectionInteractionSimple implements ZCatchListener {

  private final ZEbliCalquesPanel pn;
  ZCalqueDeplacementInteraction deplacement;

  public static final Cursor CURSOR_ACC_MOVE = EbliResource.EBLI.getCursor("curseur_accroche_move", -1, new Point(1, 1));

  public SelectionMoveInteraction(ZEbliCalquesPanel panel) {
    super(panel.getGcDonnees());
    this.pn = panel;
    addMouseMotionListener(this);
    deplacement = new ZCalqueDeplacementInteraction(this);
    deplacement.setNoListener();
    deplacement.removeMouseListener(deplacement);
    deplacement.removeMouseMotionListener(deplacement);
    deplacement.setChangeSelectionOnMousePressed(false);
    deplacement.setSpecificCursor(new Cursor(Cursor.MOVE_CURSOR));
    add(deplacement);
  }

  @Override
  public void setGele(boolean _gele) {
    super.setGele(_gele);
    deplacement.setGele(_gele);
  }

  @Override
  public boolean isCachingEnabled() {
    return !isGele();
  }

  @Override
  public void catchChanged(ZCatchEvent _evt) {
    super.catchChanged(_evt);
    deplacement.catchChanged(_evt);
  }

  @Override
  public void setEditor(ZEditorDefault _editor) {
    super.setEditor(_editor);
    deplacement.setTarget(_editor);
  }

  protected boolean isMovable(int x, int y) {
    GrPoint pt = new GrPoint(x, y, 0);
    pt.autoApplique(getVersReel());
    ZCalqueAffichageDonneesInterface[] allLayersWithSelectedFirst = editor_.getSceneEditor().getScene().getAllLayersWithSelectedFirst();
    for (ZCalqueAffichageDonneesInterface calque : allLayersWithSelectedFirst) {
      if (!calque.isSelectionEmpty()) {
        if (calque instanceof ZCalqueEditable) {
          GISZoneCollection geomData = ((ZCalqueEditable) calque).getModelEditable().getGeomData();
          if (geomData == null || !geomData.isGeomModifiable()) {
            continue;
          }
        }
        CtuluListSelection layerSelection = (CtuluListSelection) calque.getLayerSelection();
        if (layerSelection != null && !layerSelection.isEmpty()) {
          ZCalqueAffichageDonnees calqueAff = (ZCalqueAffichageDonnees) calque;
          CtuluListSelection selection = calqueAff.selection(pt, super.getTolerancePixel());
          if (layerSelection.intersects(selection)) {
            return true;
          }
        } else {
          EbliListeSelectionMulti layerSelectionMulti = (EbliListeSelectionMulti) calque.getLayerSelectionMulti();
          EbliListeSelectionMultiInterface selectVertices = calque.selectVertices(pt, getTolerancePixel(), true);
          if (selectVertices != null && !selectVertices.isEmpty()) {
            if (layerSelectionMulti.intersects(selectVertices)) {
              return true;
            }
          }
        }
      }

    }
    return false;

  }
  private boolean movable;
  private boolean moveEnCours;

  @Override
  public void mouseExited(MouseEvent _evt) {
    if (moveEnCours) {
      deplacement.mouseExited(_evt);
    }
    super.mouseExited(_evt);
    moveEnCours = false;
    movable = false;
  }

  @Override
  public void mouseClicked(MouseEvent _evt) {
    if (_evt.getClickCount() >= 2) {
      moveEnCours = false;
      deplacement.mouseClicked(_evt);
      return;
    }
    if (moveEnCours) {
      deplacement.mouseClicked(_evt);
    } else {
      super.mouseClicked(_evt);
    }
  }

  @Override
  public void mouseEntered(MouseEvent _evt) {
    if (moveEnCours) {
      deplacement.mouseEntered(_evt);
    }
    super.mouseEntered(_evt);
    moveEnCours = false;
    movable = false;
  }

  @Override
  public void mousePressed(MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (!isOkLeftEvent(_evt)) {
      return;
    }
    if (movable && !_evt.isControlDown() && !_evt.isShiftDown()) {
      moveEnCours = true;
      deplacement.mousePressed(_evt);
    } else {
      moveEnCours = false;
      super.mousePressed(_evt);
    }
  }

  @Override
  public void mouseDragged(MouseEvent _evt) {
    if (moveEnCours) {
      deplacement.mouseDragged(_evt);
    } else {
      super.mouseDragged(_evt);
    }
  }

  @Override
  public void mouseReleased(MouseEvent _evt) {
    if (moveEnCours) {
      deplacement.mouseReleased(_evt);

    } else {
      super.mouseReleased(_evt);
    }
    movable = isMovable(_evt.getX(), _evt.getY());
    moveEnCours = false;
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    if (enCours_ || isGele() || moveEnCours) {
      return;
    }
    movable = isMovable(e.getX(), e.getY());
//    pn.getVueCalque().setCursor(deplacement.getSpecificCursor());
    pn.getVueCalque().setCursor(getAccrocheSpecificCursor());
  }

//  @Override
  public Cursor getAccrocheSpecificCursor() {
    if (idxGeomAccroch_ == null) {
      return movable ? deplacement.getSpecificCursor() : getSpecificCursor();
    }
    if (movable) {
      return CURSOR_ACC_MOVE;
    }
    return ZEbliCalquePanelController.CURSOR_ACC;
  }
}
