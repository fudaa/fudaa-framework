/*
 *  @creation     31 mars 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectIterator;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ebli.calque.find.CalqueFindActionAtomic;
import org.fudaa.ebli.calque.find.CalqueFindExpressionAtomic;
import org.fudaa.ebli.commun.*;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.*;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.*;

import java.awt.*;

/**
 * Un calque g�rant des g�om�tries quelconques, � un niveau global ou atomique.<p>
 * Le niveau atomique des g�om�tries est le niveau sommet. Le calque s'appuie sur un mod�le {@link ZModeleGeometry}.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZCalqueGeometry<M extends ZModeleGeometry> extends ZCalqueAffichageDonneesLineAbstract implements ZModelGeometryListener {
  private boolean selectClosedLineInterior;

  public void setSelectClosedLineInterior(boolean selectClosedLineInterior) {
    this.selectClosedLineInterior = selectClosedLineInterior;
  }

  public boolean isSelectClosedLineInterior() {
    return selectClosedLineInterior;
  }

  protected void initTraceForAtomics(TraceIconModel iconeModel, int idxPoly, int idxVertexInPoly) {
  }

  public void clearCacheAndRepaint() {
    /*
     * A chaque changement de valeur d'un attribut, tout est redessin�. On pourrai faire quelque chose de plus optimis� comme un
     * repaint seulement quand la valeur de visibilit� est modifi� et pas dans les autres cas.
     */
    if (!massiveOperationRunning) {
      clearCache();
      repaint(0);
    }
  }

  private boolean massiveOperationRunning;

  protected void startMassiveOperation() {
    massiveOperationRunning = true;
  }

  protected void stopMassiveOperation() {
    massiveOperationRunning = false;
    clearCacheAndRepaint();
  }

  public void paintLines(final GrBoite _clipReel, final GrMorphisme _versEcran, final Graphics2D _g) {
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final int nombre = modele_.getNombre();

    final TraceLigne tl;
    if (isRapide()) {
      tl = new TraceLigne(TraceLigne.LISSE, 1, ligneModel_.getCouleur());
    } else {
      tl = ligneModel_.buildCopy();
    }

    final GrBoite bPoly = new GrBoite();
    bPoly.o_ = new GrPoint();
    bPoly.e_ = new GrPoint();

    // on part de la fin : comme ca la premiere ligne apparait au-dessus
    for (int i = nombre - 1; i >= 0; i--) {
      if (!isPainted(i, _versEcran)//dans certains cas, ne pas dessiner l'objet
          || !modele_.isGeometryReliee(i)) {
        continue;
      }

      // il n'y a pas de points pour cette g�om�trie
      final int nbPoints = modele_.getNbPointForGeometry(i);
      if (nbPoints <= 0) {
        continue;
      }

      modele_.getDomaineForGeometry(i, bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }

      final GrPoint ptOri = new GrPoint();
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(versEcran);

      if (!isRapide()) {
        initTrace(ligneModel_, i);
      }
      final GrPoint ptDest = new GrPoint();
      for (int j = nbPoints - 2; j >= 0; j--) {
        // le point de dest est initialise
        modele_.point(ptDest, i, j);
        ptDest.autoApplique(versEcran);
        tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        ptOri.initialiseAvec(ptDest);
      }
      if (modele_.isGeometryFermee(i)) {
        modele_.point(ptOri, i, nbPoints - 1);
        ptOri.autoApplique(versEcran);
        tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
      }
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(versEcran);
    }
  }

  protected void drawLabelOnAtomic(final FontMetrics fm, String s, final GrPoint ptDest, final Color bgColor, final Graphics2D _g, final Color fgColor) {
    int stringWidth = fm.stringWidth(s);
    int x = (int) (ptDest.x_ - stringWidth / 2);
    int y = (int) (ptDest.y_ - 10);
    if (bgColor != null) {
      _g.setColor(bgColor);
      _g.fillRect(x, y - fm.getAscent(), stringWidth, fm.getHeight() + 2);
    }
    _g.setColor(fgColor);
    _g.drawString(s, (int) x, (int) y);
  }

  /**
   * Une enumearion pour le mode de selection
   */
  public enum SelectionMode {
    NORMAL,
    ATOMIC,
    SEGMENT
  }

  /**
   * true si selection en mode sommets
   */
  transient private SelectionMode mode;
  /**
   * Le mod�le des g�om�tries
   */
  protected M modele_;
  /**
   * La selection a utilise dans le mode selection sommets
   */
  protected EbliListeSelectionMulti selectionMulti_;
  /**
   * Pour le trac� de la selection en mode atomique.
   */
  protected ZSelectionTrace traceAtomic_;
  /**
   * L'attribut utilis� pour afficher les labels
   */
  protected GISAttributeInterface attrLabels_ = null;
  private Color labelsForegroundColor = Color.BLACK;
  private Color labelsBackgroundColor = Color.WHITE;
  private boolean labelsVisible = false;
  // Le prefix du label quand celui ci est affich�. Le lable est constitu� du prefixe + n� de g�ometrie
  public String labelPrefix = null;
  
  /**
   * Le finder pour une recherche sur le calque
   */
  protected CalqueFindActionAtomic finder_;

  public void setLabelsForegroundColor(Color labelsForegroundColor) {
    if (!ObjectUtils.equals(this.labelsForegroundColor, labelsForegroundColor)) {
      Color old = this.labelsForegroundColor;
      this.labelsForegroundColor = labelsForegroundColor;
      repaint();
      firePropertyChange(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND, old, labelsForegroundColor);
    }
  }

  public void setLabelsBackgroundColor(Color labelsBackgroundColor) {
    if (!ObjectUtils.equals(this.labelsBackgroundColor, labelsBackgroundColor)) {
      Color old = this.labelsBackgroundColor;
      this.labelsBackgroundColor = labelsBackgroundColor;
      repaint();
      firePropertyChange(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND, old, labelsBackgroundColor);
    }
  }

  public void setLabelsVisible(boolean _b) {
    if (!ObjectUtils.equals(this.labelsVisible, _b)) {
      boolean old = this.labelsVisible;
      this.labelsVisible = _b;
      repaint();
      firePropertyChange(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE, old, this.labelsVisible);
    }
  }
  
  public boolean isLabelsVisible() {
    return labelsVisible;
  }
  
  /**
   * Le prefixe des lables g�om�triques affich�s.
   * @param _prefix Le pr�fixe. Peut �tre null.
   */
  public void setLabelsPrefix(String _prefix) {
    labelPrefix  = _prefix;
  }

  public Color getLabelsBackgroundColor() {
    return labelsBackgroundColor;
  }

  public Color getLabelsForegroundColor() {
    return labelsForegroundColor;
  }

  /**
   * Le seul constructeur, avec le mod�le.
   *
   * @param _modele le modele du calque
   */
  public ZCalqueGeometry(final M _modele) {
    if (modele_ != null) {
      modele_.removeModelListener(this);
    }
    modele_ = _modele;
    if (modele_ != null) {
      modele_.addModelListener(this);
    }
    iconModel_ = new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.BLACK);
    mode = SelectionMode.NORMAL;
    setFont(BuLib.DEFAULT_FONT);
  }

  protected EbliListeSelectionMulti creeSelectionMulti() {
    return new EbliListeSelectionMulti(modeleDonnees().getNombre() / 2);
  }

  protected void initTrace(final TraceIconModel _icon, final int _idxPoly) {
    _icon.updateData(getIconModel(0));
    if (isAttenue()) {
      _icon.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_icon.getCouleur()), alpha_));
    } else if (!isRapide() && EbliLib.isAlphaChanged(alpha_)) {
      _icon.setCouleur(EbliLib.getAlphaColor(_icon.getCouleur(), alpha_));
    }
  }

  protected void initTrace(final TraceLigneModel _ligne, final int _idxGeom) {
    final int idx = modele_.isGeometryFermee(_idxGeom) ? 0 : 1;
    _ligne.updateData(getLineModel(idx));
    if (isAttenue()) {
      _ligne.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_ligne.getCouleur()), alpha_));
    } else if (!isRapide() && EbliLib.isAlphaChanged(alpha_)) {
      _ligne.setCouleur(EbliLib.getAlphaColor(_ligne.getCouleur(), alpha_));
    }
  }

  @Override
  public boolean getRange(CtuluRange _b) {
    GISAttributeDouble attributeIsZ = modeleDonnees().getGeomData().getAttributeIsZ();
    if (attributeIsZ != null) {
      GISAttributeModel model = modeleDonnees().getGeomData().getModel(attributeIsZ);
      if (model instanceof GISAttributeModelDoubleInterface) {
        GISAttributeModelDoubleInterface doubleModel = (GISAttributeModelDoubleInterface) model;
        _b.expandTo(doubleModel.getMax());
        _b.expandTo(doubleModel.getMin());
      } else if (model instanceof GISAttributeModelObjectInterface) {
        int nb = model.getSize();
        for (int i = 0; i < nb; i++) {
          GISAttributeModelDoubleInterface objectValueAt = (GISAttributeModelDoubleInterface) model.getObjectValueAt(i);
          _b.expandTo(objectValueAt.getMax());
          _b.expandTo(objectValueAt.getMin());
        }
      }
      return true;
    }
    return super.getRange(_b);
  }

  protected void paintSelectionMulti(final Graphics _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                                     final GrBoite _clipReel) {
    if ((isRapide()) || (isSelectionEmpty())) {
      return;
    }
    final GrBoite clip = _clipReel;
    if (!getDomaine().intersectXY(clip)) {
      return;
    }
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TraceIcon ic = _trace.getIconeInterne();
    final TIntObjectIterator it = selectionMulti_.getIterator();
    final GrPoint p = new GrPoint();
    int w = getWidth() + 1;
    CtuluListSelection memory = new CtuluListSelection(w * getHeight());
    final GrMorphisme versEcran = _versEcran;
    GrSegment segment = new GrSegment(new GrPoint(), new GrPoint());
    for (int i = selectionMulti_.getNbListSelected(); i-- > 0; ) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        final int idxPoly = it.key();
        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            if (this.mode == SelectionMode.ATOMIC) {
              modele_.point(p, idxPoly, j);
            } else {
              modele_.point(segment.e_, idxPoly, j);
              modele_.point(segment.o_, idxPoly, ((j + 1) < modele_.getNbPointForGeometry(idxPoly)) ? (j + 1) : 0);
              segment.milieu(p);
            }
            if (clip.contientXY(p)) {
              p.autoApplique2D(versEcran);
              int idxOnScreen = (int) p.x_ + (int) p.y_ * w;
              if (idxOnScreen >= 0 && !memory.isSelected(idxOnScreen)) {
                ic.paintIconCentre(this, _g, p.x_, p.y_);
                memory.add(idxOnScreen);
              }
            }
          }
        }
      }
    }
  }

  @Override
  public boolean changeSelection(final GrPoint _pt, final int _tolerancePixel, final int _action) {
    if (!isSelectable()) {
      return false;
    }

    if (this.mode != SelectionMode.NORMAL) {
      final EbliListeSelectionMulti l = selectionMulti(_pt, _tolerancePixel, false, this.mode);
      changeSelectionMulti(l, _action);
      if ((l == null) || (l.isEmpty())) {
        return false;
      }
      return true;
    }
    return super.changeSelection(_pt, _tolerancePixel, _action);
  }

  @Override
  public boolean changeSelection(final LinearRing _poly, final int _action, final int _mode) {
    if (!isSelectable()) {
      return false;
    }

    if (this.mode != SelectionMode.NORMAL) {
      final EbliListeSelectionMulti l = selectionMulti(_poly);
      changeSelectionMulti(l, _action);
      if ((l == null) || (l.isEmpty())) {
        return false;
      }
      return true;
    }
    return super.changeSelection(_poly, _action, _mode);
  }

  @Override
  public boolean changeSelection(final LinearRing[] _p, final int _action, final int _mode) {
    if (!isSelectable()) {
      return false;
    }

    if (this.mode != SelectionMode.NORMAL) {
      if (_p == null) {
        return false;
      }
      final EbliListeSelectionMulti l = new EbliListeSelectionMulti(0);
      for (int i = _p.length - 1; i >= 0; i--) {
        final EbliListeSelectionMulti li = selectionMulti(_p[i]);
        l.add(li);
      }
      changeSelection(l, _action);
      if (l.isEmpty()) {
        return false;
      }
      return true;
    } else {
      return super.changeSelection(_p, _action, _mode);
    }
  }

  public boolean changeSelectionMulti(final EbliListeSelectionMultiInterface _s, final int _action) {
    // Si la selection de modif est nulle, seule l'action de remplacement
    // est concernee.
    if (selectionMulti_ == null) {
      selectionMulti_ = creeSelectionMulti();
    }
    boolean sentEvent = false;
    switch (_action) {
      case EbliSelectionState.ACTION_ADD:
        sentEvent = selectionMulti_.add(_s);
        break;
      case EbliSelectionState.ACTION_DEL:
        sentEvent = selectionMulti_.remove(_s);
        break;
      case EbliSelectionState.ACTION_XOR:
        sentEvent = selectionMulti_.xor(_s);
        break;
      case EbliSelectionState.ACTION_REPLACE:
        sentEvent = selectionMulti_.setSelection(_s);
        break;
      case EbliSelectionState.ACTION_AND:
        sentEvent = selectionMulti_.intersection(_s);
        break;
      default:
        break;
    }
    if (sentEvent) {
      fireSelectionEvent();
    }
    return sentEvent;
  }

  @Override
  public void clearSelection() {
    if (!isSelectable()) {
      return;
    }

    // dans le mode edition de noeuds
    if (this.mode != SelectionMode.NORMAL) {
      if (selectionMulti_ != null && !selectionMulti_.isEmpty()) {

        selectionMulti_.clear();
        fireSelectionEvent();
      }
    } else {
      super.clearSelection();
    }
  }

  @Override
  public EbliFindActionInterface getFinder() {
    // Le finder est unique pour le calque, car il ecoute les changements de propri�t�s du calque.
    if (finder_ == null) {
      finder_ = new CalqueFindActionAtomic(this);
    }
    return finder_;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new CalqueFindExpressionAtomic(this);
  }

  @Override
  public EbliListeSelectionMultiInterface getLayerSelectionMulti() {
    return selectionMulti_;
  }

  @Override
  public int getNbSelected() {
    if (isSelectionEmpty()) {
      return 0;
    }
    if (this.mode != SelectionMode.NORMAL) {
      return selectionMulti_.getNbListSelected();
    }
    return super.getNbSelected();
  }

  @Override
  public int[] getSelectedIndex() {
    if (isSelectionEmpty()) {
      return null;
    }
    if (this.mode != SelectionMode.NORMAL) {
      return selectionMulti_.getIdxSelected();
    }
    return super.getSelectedIndex();
  }

  @Override
  public int[] getSelectedObjectInTable() {
    if (isSelectionEmpty()) {
      return null;
    }
    if (this.mode == SelectionMode.NORMAL) {
      return super.getSelectedObjectInTable();
    }
    final EbliListeSelectionMultiInterface multi = getLayerSelectionMulti();
    int nb = 0;
    // 20 points par lignes
    final TIntArrayList list = new TIntArrayList(modele_.getNombre() * 20);
    final int nbLigne = modele_.getNombre();
    for (int i = 0; i < nbLigne; i++) {
      final CtuluListSelectionInterface sel = multi.getSelection(i);
      if (sel != null) {
        final int max = sel.getMaxIndex();
        for (int j = sel.getMinIndex(); j <= max; j++) {
          if (sel.isSelected(j)) {
            list.add(nb + j);
          }
        }
      }
      nb += modele_.getNbPointForGeometry(i);
    }

    return list.toNativeArray();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    final Envelope env = new Envelope();
    if (this.mode == SelectionMode.ATOMIC) {
      final EbliListeSelectionMultiInterface multi = getLayerSelectionMulti();
      Coordinate c = new Coordinate();
      final TIntObjectIterator it = multi.getIterator();
      for (int i = multi.getNbListSelected(); i-- > 0; ) {
        it.advance();
        final CtuluListSelectionInterface atomSel = (CtuluListSelectionInterface) it.value();
        final CoordinateSequence seq = modele_.getGeomData().getCoordinateSequence(it.key());
        final int max = atomSel.getMaxIndex();
        for (int j = atomSel.getMinIndex(); j <= max; j++) {
          if (atomSel.isSelected(j)) {
            seq.getCoordinate(j, c);
            env.expandToInclude(c);
          }
        }
      }
    } else {
      final CtuluListSelectionInterface sel = getLayerSelection();
      final int max = sel.getMaxIndex();
      for (int i = sel.getMinIndex(); i <= max; i++) {
        if (sel.isSelected(i)) {
          env.expandToInclude(modele_.getGeomData().getGeometry(i).getEnvelopeInternal());
        }
      }
    }
    final GrBoite r = new GrBoite();
    r.ajuste(env.getMaxX(), env.getMaxY(), 0);
    r.ajuste(env.getMinX(), env.getMinY(), 0);
    return r;
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    EbliUIProperties properties = super.saveUIProperties();
    properties.put(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND, labelsBackgroundColor);
    properties.put(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND, labelsForegroundColor);
    properties.put(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE, (Boolean)labelsVisible);
    return properties;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      if (_p.isDefined("calqueGIS.ligneModel")) {
        setLineModel(0, (TraceLigneModel) _p.get("calqueGIS.ligneModel"));
      }
      if (_p.isDefined(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND)) {
        setLabelsBackgroundColor((Color) _p.get(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_BACKGROUND));
      } else {
        setLabelsBackgroundColor(null);
      }
      if (_p.isDefined(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND)) {
        setLabelsForegroundColor((Color) _p.get(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_FOREGROUND));
      }
      if (_p.isDefined(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE)) {
        setLabelsVisible((Boolean) _p.get(ZCalqueGeometryLabelConfigure.PROPERTY_LABELS_VISIBLE));
      }
    }
  }

  public void inverseSelectedSelection() {
    //TODO Voir si l'action doit se faire en mode normal (comme mnt) ou dans tout mode diff�rent du mode atomique.
//  if (!isAtomicMode_) {
    if (this.mode == SelectionMode.NORMAL) {
      super.inverseSelection();
      return;
    }
    if (isSelectionEmpty()) {
      return;
    }
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      CtuluListSelection s = selectionMulti_.get(i);
      if (s == null) {
        s = new CtuluListSelection();
        s.setSelectionInterval(0, modele_.getNbPointForGeometry(i) - 1);
        selectionMulti_.set(i, s);
      } else {
        s.inverse(modele_.getNbPointForGeometry(i));
      }
    }
    fireSelectionEvent();
  }

  @Override
  public void inverseSelection() {
    //TODO Voir si l'action doit se faire en mode normal (comme mnt) ou dans tout mode diff�rent du mode atomique.
//  if (!isAtomicMode_) {
    if (this.mode == SelectionMode.NORMAL) {
      super.inverseSelection();
      return;
    }
    if (isSelectionEmpty()) {
      return;
    }
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      final CtuluListSelection s = selectionMulti_.get(i);
      if (s != null) {
        s.inverse(modele_.getNbPointForGeometry(i));
      }
    }
    fireSelectionEvent();
  }

  public SelectionMode getSelectionMode() {
    return mode;
  }

  /**
   * @return true si on est dans le mode edition atomique
   */
  public boolean isAtomicMode() {
    return getSelectionMode() == SelectionMode.ATOMIC;
  }

  public boolean isConfigurable() {
    return true;
  }

  @Override
  public boolean isPaletteModifiable() {
    return false;
  }

  /**
   * D�finit l'attribut atomique pris pour afficher les labels.
   *
   * @param _att L'attribut. Si Null : Pas d'affichage de labels.
   */
  public void setAttributForLabels(GISAttributeInterface _att) {
    if (_att == attrLabels_) {
      return;
    }
    if (_att == null || !_att.isAtomicValue()) {
      attrLabels_ = null;
    } else {
      attrLabels_ = _att;
    }
    clearCacheAndRepaint();
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[2 + getNbSet()];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    sect[1] = new ZCalqueGeometryLabelConfigure(this);
    for (int i = 2; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(this, i - 2);
    }
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  /**
   * Indique si la s�lection est vide.
   *
   * @return true Si aucun objet ni aucun point en mode atomique selectionn�.
   */
  @Override
  public boolean isSelectionEmpty() {
    return (this.mode != SelectionMode.NORMAL) ? selectionMulti_ == null ? true : selectionMulti_.isEmpty() : super.isSelectionEmpty();
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  /**
   * @param _modele Modele
   */
  public void modele(final M _modele) {
    if (modele_ != _modele) {
      if (modele_ != null) {
        modele_.removeModelListener(this);
      }
      final ZModeleGeometry vp = modele_;
      modele_ = _modele;
      if (modele_ != null) {
        modele_.removeModelListener(this);
      }
      firePropertyChange("modele", vp, modele_);
    }
  }

  @Override
  public M modeleDonnees() {
    return modele_;
  }

  protected boolean isPainted(int idx, GrMorphisme _versEcran) {
    return modele_.isGeometryVisible(idx);
  }

  /**
   * @param _g
   */
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                           final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }

    // Trac� des surfaces
    paintSurfaces(_g, _versEcran, _versReel, _clipReel);
    paintLines(_clipReel, _versEcran, _g);

    // Les icones sur les atomiques
    paintIconsOnAtomics(_g, _versEcran, _versReel, _clipReel);
    // Les labels sur les g�om�tries
    if (labelsVisible)
      paintLabelsOnGeometries(_g, _versEcran, _versReel, _clipReel);
    // Enfin les labels sur les atomiques.
    paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
  }

  protected void paintSurfaces(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                               final GrBoite _clipReel) {

    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final int nombre = modele_.getNombre();
    final GrBoite bPoly = new GrBoite();

    final TraceSurface ts;
    Color iniColor = null;
    if (surfModel_ != null) {
      ts = surfModel_.buildCopy();
      iniColor = EbliLib.getAlphaColor(ts.getCouleurFond(), alpha_);
      ts.setCouleurFond(iniColor);
    } else {
      ts = null;
    }

    // Les surfaces, si a tracer
    if (!isRapide() && ts != null && ts.getTypeSurface() != TraceSurface.INVISIBLE) {
      TIntArrayList x = new TIntArrayList(50);
      TIntArrayList y = new TIntArrayList(50);

      for (int i = nombre - 1; i >= 0; i--) {
        if (!modele_.isGeometryReliee(i) || !modele_.isGeometryFermee(i) || !isPainted(i, _versEcran)) {
          continue;
        }

        // il n'y a pas de points pour cette g�om�trie
        final int nbPoints = modele_.getNbPointForGeometry(i);
        if (nbPoints <= 0) {
          continue;
        }
        // La g�ometrie n'est pas visible
        if (!isPainted(i, _versEcran)) {
          continue;
        }

        modele_.getDomaineForGeometry(i, bPoly);
        // Si la boite du polygone n'est pas dans la boite d'affichage on passe
        if (bPoly.intersectionXY(clip) == null) {
          continue;
        }

        x.reset();
        y.reset();
        final GrPoint pt = new GrPoint();
        for (int j = 0; j < nbPoints; j++) {
          // le point de dest est initialise
          modele_.point(pt, i, j);
          pt.autoApplique(versEcran);
          x.add((int) pt.x_);
          y.add((int) pt.y_);
        }
        Color surfaceColor = getSurfaceSpecColor(i, iniColor);
        ts.setCouleurFond(surfaceColor);
        ts.remplitPolygone((Graphics2D) _g, x.toNativeArray(), y.toNativeArray());
      }
    }
  }

  /**
   * methode surcharg� par des sous-classes permettant de specifier une couleur diff�rents.
   *
   * @param i
   * @param initColor
   */
  protected Color getSurfaceSpecColor(int i, Color initColor) {
    return initColor;
  }

  protected void paintIconsOnAtomics(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                                     final GrBoite _clipReel) {

    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final int nombre = modele_.getNombre();
    final GrBoite bPoly = new GrBoite();

    TraceIconModel iconeModel = iconModel_ == null ? null : new TraceIconModel(iconModel_);
    if (iconeModel.getType() == TraceIcon.RIEN || iconeModel.getTaille() <= 0) {
      return;
    }

    final TraceIcon icone;
    if (isRapide()) {
      icone = new TraceIcon(TraceIcon.CARRE, 1, iconeModel.getCouleur());
    } else {
      icone = iconeModel == null ? null : new TraceIcon(iconeModel);
    }
    if (icone != null) {
      iconeModel = icone.getModel();
    }
    int w = getWidth() + 1;
    CtuluListSelection memory = new CtuluListSelection(w * getHeight());
    // on trace les icones apres pour qu'ils soient dessin�s au-dessus des lignes.
    if (icone != null) {
      for (int i = nombre - 1; i >= 0; i--) {
        // il n'y a pas de points pour cette ligne
        if (modele_.getNbPointForGeometry(i) <= 0 || !isPainted(i, _versEcran)) {
          continue;
        }

        modele_.getDomaineForGeometry(i, bPoly);
        // Si la boite du polygone n'est pas dans la boite d'affichage on passe
        if (bPoly.intersectionXY(clip) == null) {
          continue;
        }
        final int nbPoints = modele_.getNbPointForGeometry(i);
        if (nbPoints <= 0) {
          continue;
        }
        initTrace(iconeModel, i);

        final GrPoint ptDest = new GrPoint();
        for (int j = nbPoints - 1; j >= 0; j--) {
          //l'icone ne doit pas �tre paint:
          if (!isIconOnAtomicsPainted(i, j)) {
            continue;
          }
          // le point de dest est initialise
          modele_.point(ptDest, i, j);
          if (!_clipReel.contientXY(ptDest)) {
            continue;
          }
          ptDest.autoApplique2D(versEcran);
          int idxOnScreen = (int) ptDest.x_ + (int) ptDest.y_ * w;
          if (idxOnScreen >= 0 && !memory.isSelected(idxOnScreen)) {
            initTraceForAtomics(iconeModel, i, j);
            if (rapide_) {
              iconeModel.setType(TraceIcon.CARRE);
              iconeModel.setTaille(1);
            }
            icone.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
            memory.add(idxOnScreen);
          }
        }
      }
    }
  }

  protected boolean isIconOnAtomicsPainted(int idxGeometry, int idxPoint) {
    return true;
  }

  /**
   * Le label est affich� pour chaque geometrie
   * @param _g
   * @param _versEcran
   * @param _versReel
   * @param _clipReel
   */
  protected void paintLabelsOnGeometries(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {

    if (!isRapide()) {
      GISZoneCollection geomData = modeleDonnees().getGeomData();
//      int idxLabels = geomData.getIndiceOf(attrLabels_);

//      if (idxLabels != -1) {
        final Color fgColor = getLabelsForegroundColor();
        final Color bgColor = getLabelsBackgroundColor();
        Font old = _g.getFont();
        _g.setFont(getFont());
        final FontMetrics fm = _g.getFontMetrics();
        final GrMorphisme versEcran = _versEcran;
        final int nombre = modele_.getNombre();
        int w = getWidth() + 1;
        CtuluListSelection memory = new CtuluListSelection(w * getHeight());
        for (int i = nombre - 1; i >= 0; i--) {
          // il n'y a pas de points pour cette ligne
          if (modele_.getNbPointForGeometry(i) <= 0) {
            continue;
          }
          // La g�ometrie n'est pas visible
          if (!isPainted(i, _versEcran)) {
            continue;
          }

          GrPolyligne grpl = new GrPolyligne(geomData.getCoordinateSequence(i));
          GrPoint ptDest = grpl.pointDAbscisse(0.5);

          String s = (labelPrefix == null ? "":labelPrefix) + (i + 1);
          if (StringUtils.isBlank(s)) {
            continue;
          }
          if (!_clipReel.contientXY(ptDest)) {
            continue;
          }
          ptDest.autoApplique2D(versEcran);
          int idxOnScreen = (int) ptDest.x_ + (int) ptDest.y_ * w;
          if (idxOnScreen >= 0 && !memory.isSelected(idxOnScreen)) {
            memory.add(idxOnScreen);
            drawLabelOnAtomic(fm, s, ptDest, bgColor, _g, fgColor);
          }
        }
        _g.setFont(old);
//      }
    }
  }

  protected void paintLabelsOnAtomics(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                                      final GrBoite _clipReel) {

    if (attrLabels_ != null && !isRapide()) {
      GISZoneCollection geomData = modeleDonnees().getGeomData();
      int idxLabels = geomData.getIndiceOf(attrLabels_);

      if (idxLabels != -1) {
        final Color fgColor = labelsForegroundColor;
        final Color bgColor = labelsBackgroundColor;
        Font old = _g.getFont();
        _g.setFont(getFont());
        final FontMetrics fm = _g.getFontMetrics();
        final GrMorphisme versEcran = _versEcran;
        final int nombre = modele_.getNombre();
        int w = getWidth() + 1;
        CtuluListSelection memory = new CtuluListSelection(w * getHeight());
        for (int i = nombre - 1; i >= 0; i--) {
          // il n'y a pas de points pour cette ligne
          if (modele_.getNbPointForGeometry(i) <= 0) {
            continue;
          }
          // La g�ometrie n'est pas visible
          if (!isPainted(i, _versEcran)) {
            continue;
          }

          final int nbPoints = modele_.getNbPointForGeometry(i);
          if (nbPoints <= 0) {
            continue;
          }

          GISAttributeModel mdl = (GISAttributeModel) geomData.getModel(idxLabels).getObjectValueAt(i);

          final GrPoint ptDest = new GrPoint();
          for (int j = nbPoints - 1; j >= 0; j--) {
            modele_.point(ptDest, i, j);

            //pour �viter de redessiner le texte au m�me endroit:
            Object o = mdl.getObjectValueAt(j);
            if (o == null) {
              continue;
            }
            String s = o.toString().trim();
            if (StringUtils.isBlank(s)) {
              continue;
            }
            if (!_clipReel.contientXY(ptDest)) {
              continue;
            }
            ptDest.autoApplique2D(versEcran);
            int idxOnScreen = (int) ptDest.x_ + (int) ptDest.y_ * w;
            if (idxOnScreen >= 0 && !memory.isSelected(idxOnScreen)) {
              memory.add(idxOnScreen);
              drawLabelOnAtomic(fm, s, ptDest, bgColor, _g, fgColor);
            }
          }
        }
        _g.setFont(old);
      }
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                               final GrBoite _clipReel) {
    if (isSelectionEmpty()) {
      return;
    }
    if (this.mode != SelectionMode.NORMAL) {
      if (traceAtomic_ == null) {
        Color cg = attenueCouleur(_trace.getColor());
        Color cs = _trace.getColor();
        traceAtomic_ = new ZSelectionTrace(cg, cs);
      }
      CtuluListSelection containers = new CtuluListSelection(selectionMulti_.getIdxSelection());
      paintSelectionSimple(_g, traceAtomic_, _versEcran, _clipReel, containers);
      paintSelectionMulti(_g, traceAtomic_, _versEcran, _clipReel);
    } else {
      paintSelectionSimple(_g, _trace, _versEcran, _clipReel, selection_);
    }
  }

  public void paintSelectionSimple(final Graphics _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                                   final GrBoite _clipReel, CtuluListSelection _containers) {
    if ((isRapide()) || (isSelectionEmpty())) {
      return;
    }
    final GrBoite clip = _clipReel;
    if (getDomaine() == null || !getDomaine().intersectXY(clip)) {
      return;
    }
    final GrMorphisme versEcran = _versEcran;
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    final int nb = Math.min(_containers.getMaxIndex(), modele_.getNombre() - 1);

    final GrBoite btGeom = new GrBoite();
    btGeom.e_ = new GrPoint();
    btGeom.o_ = new GrPoint();
    int w = getWidth() + 1;
    //la liste des pixels sur lesquels on ne redessine pas
    CtuluListSelection memoryNotRedraw = new CtuluListSelection(w * getHeight());
    GrBoite clipEcran = _clipReel.applique(versEcran);
    for (int i = nb; i >= 0; i--) {
      if (!_containers.isSelected(i)) {
        continue;
      }
      modele_.getDomaineForGeometry(i, btGeom);
      // Si la boite de la geometrie n'est pas dans la boite d'affichage on passe
      if (btGeom.intersectionXY(clip) == null) {
        continue;
      }

      final int nbPoints = modele_.getNbPointForGeometry(i);
      boolean bferm = modele_.isGeometryFermee(i);
      boolean breli = modele_.isGeometryReliee(i);

      final GrPoint ptOri = new GrPoint();
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique2D(versEcran);

      int idxOnScreen = (int) ptOri.x_ + (int) ptOri.y_ * w;
      if (idxOnScreen >= 0 && !memoryNotRedraw.isSelected(idxOnScreen)) {
        ic.paintIconCentre(this, _g, ptOri.x_, ptOri.y_);
        memoryNotRedraw.add(idxOnScreen);
      }

      final GrPoint ptDest = new GrPoint();
      for (int j = nbPoints - 2; j >= 0; j--) {
        // le point de dest est initialise

        modele_.point(ptDest, i, j);
        ptDest.autoApplique2D(versEcran);
        if (clipEcran.contientXY(ptDest)) {
          idxOnScreen = (int) ptDest.x_ + (int) ptDest.y_ * w;
          if (idxOnScreen >= 0 && !memoryNotRedraw.isSelected(idxOnScreen)) {
            ic.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
            memoryNotRedraw.add(idxOnScreen);
          }
        }
        //les points sous forme d'entier afin d'�viter de dessiner des lignes sur un pixel
        int x1 = (int) ptOri.x_;
        int y1 = (int) ptOri.y_;
        int x2 = (int) ptDest.x_;
        int y2 = (int) ptDest.y_;
        if ((x1 != x2 || y1 != y2) && breli && clipEcran.intersectXYBoite(ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_)) {
          tlSelection.dessineTrait((Graphics2D) _g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        }
        ptOri.initialiseAvec(ptDest);
      }
      if (bferm) {
        modele_.point(ptOri, i, nbPoints - 1);
        ptOri.autoApplique2D(versEcran);
        if (breli) {
          int x1 = (int) ptOri.x_;
          int y1 = (int) ptOri.y_;
          int x2 = (int) ptDest.x_;
          int y2 = (int) ptDest.y_;
          if ((x1 != x2 || y1 != y2) && breli && clipEcran.intersectXYBoite(ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_)) {
            tlSelection.dessineTrait((Graphics2D) _g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
          }
        }
      }
    }
  }

  @Override
  public void selectAll() {
    if (!isVisible() || !isSelectable()) {
      return;
    }

    if (this.mode == SelectionMode.NORMAL) {
      initSelection();
      selection_.addInterval(0, modele_.getNombre() - 1);
      for (int i = 0; i < modele_.getNombre(); i++) {
        if (!modele_.isGeometryVisible(i)) {
          selection_.remove(i);
        }
      }
    } else {
      if (selectionMulti_ == null) {
        selectionMulti_ = creeSelectionMulti();
      }
      for (int i = modele_.getNombre() - 1; i >= 0; i--) {
        if (!modele_.isGeometryVisible(i)) {
          continue;
        }

        CtuluListSelection s = selectionMulti_.get(i);
        if (s == null) {
          s = new CtuluListSelection(modele_.getNbPointForGeometry(i));
          selectionMulti_.set(i, s);
        }
        s.setSelectionInterval(0, modele_.getNbPointForGeometry(i) - 1);
      }
    }
    fireSelectionEvent();
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (modele_.getNombre() == 0) {
      return null;
    }
    final GrBoite bClip = getDomaine();
    if (bClip == null) {
      return null;
    }
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }

    final GrPoint pt = new GrPoint();
    final GrSegment poly = new GrSegment(new GrPoint(), new GrPoint());
    final GrBoite bPoly = new GrBoite(new GrPoint(), new GrPoint());

    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      if (!modele_.isGeometryVisible(i)) {
        continue;
      }

      modele_.getDomaineForGeometry(i, bPoly);
      if (bPoly.contientXY(_pt) || bPoly.distanceXY(_pt) < distanceReel) {

        if (modele_.isGeometryReliee(i)) {
          for (int j = modele_.getNbPointForGeometry(i) - 1; j > 0; j--) {
            modele_.point(poly.e_, i, j);
            modele_.point(poly.o_, i, j - 1);
            if (poly.distanceXY(_pt) < distanceReel) {
              final CtuluListSelection r = new CtuluListSelection(1);
              r.setSelectionInterval(i, i);
              return r;
            }
          }
          if (modele_.isGeometryFermee(i)) {
            modele_.point(poly.e_, i, 0);
            modele_.point(poly.o_, i, modele_.getNbPointForGeometry(i) - 1);
            if (poly.distanceXY(_pt) < distanceReel) {
              final CtuluListSelection r = new CtuluListSelection(1);
              r.setSelectionInterval(i, i);
              return r;
            }
            if (selectClosedLineInterior) {
              final Geometry geometry = modele_.getGeomData().getGeometry(i);
              if (geometry instanceof LinearRing) {
                LinearRing ring = (LinearRing) geometry;
                if (GISLib.isInside(GISLib.createPolygoneTester(ring), new Coordinate(_pt.x_, _pt.y_))) {
                  final CtuluListSelection r = new CtuluListSelection(1);
                  r.setSelectionInterval(i, i);
                  return r;
                }
              }
            }
          }
        } else {
          for (int j = modele_.getNbPointForGeometry(i) - 1; j >= 0; j--) {
            modele_.point(pt, i, j);
            if (pt.distanceXY(_pt) < distanceReel) {
              final CtuluListSelection r = new CtuluListSelection(1);
              r.setSelectionInterval(i, i);
              return r;
            }
          }
        }
      }
    }
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing linearRing, final int mode) {
    final GrBoite domaineBoite = getDomaine();
    if (modele_.getNombre() == 0 || !isVisible()||domaineBoite==null) {
      return null;
    }
    final Envelope polyEnv = linearRing.getEnvelopeInternal();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de
    // selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }

    final PointOnGeometryLocator tester = new IndexedPointInAreaLocator(linearRing);
    final Coordinate coordinate = new Coordinate();
    final CtuluListSelection r = creeSelection();
    final GrPoint p = new GrPoint();
    final GrBoite bPoly = new GrBoite(new GrPoint(), new GrPoint());
    for (int i = modeleDonnees().getNombre() - 1; i >= 0; i--) {
      if (!modele_.isGeometryVisible(i)) {
        continue;
      }

      modele_.getDomaineForGeometry(i, bPoly);
      // si le poly est dans dans le poly de selection
      boolean selected = false;
      if (mode == EbliSelectionMode.MODE_ALL) {
        if (polyEnv.contains(bPoly.e_.x_, bPoly.e_.y_) && polyEnv.contains(bPoly.o_.x_, bPoly.o_.y_)) {
          selected = true;
          for (int j = modele_.getNbPointForGeometry(i) - 1; (j >= 0) && selected; j--) {
            modele_.point(p, i, j);
            coordinate.x = p.x_;
            coordinate.y = p.y_;
            if (!GISLib.isInside(tester, coordinate)) {
              selected = false;
            }
          }
        }
      } else if (mode == EbliSelectionMode.MODE_ONE) {
        if (polyEnv.intersects(bPoly.getEnv())) {
          for (int j = modele_.getNbPointForGeometry(i) - 1; (j >= 0) && !selected; j--) {
            modele_.point(p, i, j);
            coordinate.x = p.x_;
            coordinate.y = p.y_;
            if (GISLib.isSelectedEnv(coordinate, linearRing, polyEnv, tester)) {
              selected = true;
            }
          }
        }
      } else if (mode == EbliSelectionMode.MODE_CENTER) {
        final CoordinateSequence coordinateSequence = modele_.getGeomData().getCoordinateSequence(i);
        coordinate.x = GISLib.getMoyX(coordinateSequence);
        coordinate.y = GISLib.getMoyY(coordinateSequence);
        selected = GISLib.isSelectedEnv(coordinate, linearRing, polyEnv, tester);
      }
      if (selected) {
        r.add(i);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  @Override
  public EbliListeSelectionMulti selectVertices(final GrPoint ptReel, final int tolerancePixel, boolean inDepth) {
    return selectionMulti(ptReel, tolerancePixel, inDepth, SelectionMode.ATOMIC);
  }

  /**
   * @param inDepth False : Seul le premier point trouv� est retourn�. Sinon, continue avec les points dessous.
   * @param selectionMode Le mode de selection, sur sommets ou segments.
   * @return La liste de selection.
   */
  public EbliListeSelectionMulti selectionMulti(final GrPoint ptReel, final int tolerancePixel, boolean inDepth,
                                                SelectionMode selectionMode) {
    final GrMorphisme versReel = getVersReel();
    GrBoite bClip = getDomaine();
    final double distanceReel = GrMorphisme.convertDistanceXY(versReel, tolerancePixel);
    if (bClip == null || (!bClip.contientXY(ptReel) && (bClip.distanceXY(ptReel) > distanceReel))) {
      return null;
    }
    bClip = getClipReel(getGraphics());
    final GrPoint p = new GrPoint();
    final GrBoite btLigne = new GrBoite();

    EbliListeSelectionMulti sel = null;
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      if (!modele_.isGeometryVisible(i)) {
        continue;
      }

      modele_.getDomaineForGeometry(i, btLigne);
      if (btLigne.contientXY(ptReel) || btLigne.distanceXY(ptReel) < distanceReel) {
        if (selectionMode == SelectionMode.ATOMIC) {
          for (int j = modele_.getNbPointForGeometry(i) - 1; j >= 0; j--) {
            modele_.point(p, i, j);
            if (bClip.contientXY(p) && (p.distanceXY(ptReel) <= distanceReel)) {
              if (sel == null) {
                sel = new EbliListeSelectionMulti(1);
              }
              sel.add(i, j);
              if (!inDepth) {
                return sel;
              }
            }
          }
        } else {
          final GrSegment segment = new GrSegment(new GrPoint(), new GrPoint());

          for (int j = 0; j < modele_.getNbPointForGeometry(i); j++) {
            int nextIdx = j + 1;
            if (nextIdx == modele_.getNbPointForGeometry(i)) {
              if (modele_.isGeometryFermee(i)) {
                nextIdx = 0;
              } else {
                break;
              }
            }

            modele_.point(segment.e_, i, j);
            modele_.point(segment.o_, i, nextIdx);
            if (segment.distanceXY(ptReel) < distanceReel) {
              if (sel == null) {
                sel = new EbliListeSelectionMulti(1);
              }
              sel.add(i, j);
              if (!inDepth) {
                return sel;
              }
            }
          }
        }
      }
    }
    return sel;
  }

  /**
   * Renvoie sous forme de liste de selection les sommets selectionnes pour la polyligne <code>_poly</code>.
   *
   * @param _poly Le point pour la selection
   * @return la liste des indexs selectionnes (ou null si aucune selection)
   */
  public EbliListeSelectionMulti selectionMulti(final LinearRing _poly) {
    if (modele_.getNombre() == 0 || !isVisible()) {
      return null;
    }
    final Envelope polyEnv = _poly.getEnvelopeInternal();
    final GrBoite domaineBoite = getDomaine();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de
    // selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final EbliListeSelectionMulti r = creeSelectionMulti();
    final GrPoint p = new GrPoint();
    final PointOnGeometryLocator tester = new IndexedPointInAreaLocator(_poly);
    final Coordinate c1 = new Coordinate();
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      if (!modele_.isGeometryVisible(i)) {
        continue;
      }

      CtuluListSelection l = null;
      final int nbPt = modele_.getNbPointForGeometry(i);
      if (this.mode == SelectionMode.ATOMIC) {
        for (int j = nbPt - 1; j >= 0; j--) {
          modele_.point(p, i, j);
          c1.x = p.x_;
          c1.y = p.y_;
          if ((polyEnv.contains(c1)) && (GISLib.isInside(tester, c1))) {
            if (l == null) {
              l = new CtuluListSelection(nbPt);
            }
            l.add(j);
          }
        }
      } else {
        final Coordinate c2 = new Coordinate();
        for (int j = 0; j < nbPt; j++) {
          int nextIdx = j + 1;

          if (nextIdx == nbPt) {
            if (modele_.isGeometryFermee(i)) {
              nextIdx = 0;
            } else {
              break;
            }
          }

          GrPoint pt1 = new GrPoint();
          GrPoint pt2 = new GrPoint();

          modele_.point(pt1, i, j);
          modele_.point(pt2, i, nextIdx);

          c1.x = pt1.x_;
          c1.y = pt1.y_;
          c2.x = pt2.x_;
          c2.y = pt2.y_;

          if ((polyEnv.contains(c1)) && (GISLib.isInside(tester, c1)) && (polyEnv.contains(c2)) && (GISLib.isInside(tester, c2))) {
            if (l == null) {
              l = new CtuluListSelection(nbPt);
            }
            l.add(j);
          }
        }
      }
      if (l != null) {
        r.set(i, l);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  public boolean setSelectionMode(SelectionMode mode) {
    if (this.mode != mode) {
      SelectionMode oldMode = this.mode;
      this.mode = mode;
      firePropertyChange("mode", oldMode, mode);
      clearSelection();
      return true;
    }

    return false;
  }

  @Override
  public boolean setSelection(final int[] _idx) {
    if (!isSelectable()) {
      return false;
    }

    // Aucun controle n'est fait sur la visibilit� ou non des objets d'indices pass�s.
    if (this.mode == SelectionMode.NORMAL) {
      return super.setSelection(_idx);
    }
    // TODO : Traitement a faire si on est en mode atomique ou segment.
    FuLog.warning("EBL:ZCalqueGeometry.setSelection() can't be called in atomic or segment mode");
    return false;
  }

  @Override
  public boolean changeSelection(EbliListeSelectionMultiInterface _selection, int _action) {
    if (!isSelectable()) {
      return false;
    }

    // Aucun controle n'est fait sur la visibilit� ou non des objets d'indices pass�s.
    if (this.mode != SelectionMode.NORMAL) {
      return changeSelectionMulti(_selection, _action);
    } else {
      return super.changeSelection(_selection.getIdxSelection(), _action);
    }
  }

  @Override
  public boolean selectNext() {
    if (this.mode == SelectionMode.NORMAL) {
      return super.selectNext();
    } // Ne fonctionne que sur une seule g�om�trie a la fois.
    else {
      if (selectionMulti_ == null) {
        return false;
      }
      if (selectionMulti_.getNbListSelected() != 1) {
        return false;
      }

      int idxGeom = selectionMulti_.getIdxSelected()[0];

      CtuluListSelection s = selectionMulti_.get(idxGeom);
      if (s == null) {
        return false;
      }
      int idx = s.getMaxIndex();
      if (idx >= modeleDonnees().getNbPointForGeometry(idxGeom) - 1) {
        return false;
      }
      s.setSelectionInterval(idx + 1, idx + 1);
      fireSelectionEvent();
      return true;
    }
  }

  @Override
  public boolean selectPrevious() {
    if (this.mode == SelectionMode.NORMAL) {
      return super.selectPrevious();
    } // Ne fonctionne que sur une seule g�om�trie a la fois.
    else {
      if (selectionMulti_ == null) {
        return false;
      }
      if (selectionMulti_.getNbListSelected() != 1) {
        return false;
      }

      int idxGeom = selectionMulti_.getIdxSelected()[0];

      CtuluListSelection s = selectionMulti_.get(idxGeom);
      if (s == null) {
        return false;
      }
      int idx = s.getMinIndex();
      if (idx <= 0) {
        return false;
      }
      s.setSelectionInterval(idx - 1, idx - 1);
      fireSelectionEvent();
      return true;
    }
  }

  @Override
  public boolean setTitle(final String _title) {
    final boolean r = super.setTitle(_title);
    if (r && modele_ != null && modele_.getGeomData() != null) {
      modele_.getGeomData().setTitle(_title, null);
    }
    return r;
  }

  protected LineString createLineFromSelection(final int _i, final CtuluListSelectionInterface _selection, final int _nbSelected) {
    final GISCoordinateSequenceContainerInterface geom = (GISCoordinateSequenceContainerInterface) modele_.getGeomData().getGeometry(
        _i);
    final Coordinate[] cs = new Coordinate[_nbSelected];
    final int[] idx = _selection.getSelectedIndex();
    for (int j = 0; j < _nbSelected; j++) {
      cs[j] = geom.getCoordinateSequence().getCoordinate(idx[j]);
    }
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  @Override
  public LineString getSelectedLine() {
    if (isSelectionEmpty()) {
      return null;
    }
    // en mode normal (pas de vertex selectionne), on renvoie la ligne s�lectionn�e, si ligne. Sinon null.
    //TODO Voir si l'action doit se faire en mode normal (comme mnt) ou dans tout mode diff�rent du mode atomique.
    if (this.mode == SelectionMode.NORMAL) {
      if (getLayerSelection().getNbSelectedIndex() == 1) {
        GISCoordinateSequenceContainerInterface geom = (GISCoordinateSequenceContainerInterface) modeleDonnees().getGeomData().getGeometry(
            getLayerSelection().getMaxIndex());
        if (geom instanceof LineString) {
          return (LineString) geom;
        }
      }
      return null;
    }
    // mode vertex
    final EbliListeSelectionMultiInterface select = getLayerSelectionMulti();
    // mode vertex
    // la selection se situe dans la meme ligne
    if (select.getNbListSelected() == 1) {
      final int i = select.getIdxSelected()[0];
      final CtuluListSelectionInterface selection = select.getSelection(i);
      final int nbSelected = selection.getNbSelectedIndex();
      // deux sommets s�lectionn�s: on choisit la ligne en les 2 sommets
      if (nbSelected == 2) {
        return createLineFromSelection(i, selection, nbSelected);
      } else if (nbSelected >= 2) {
        final int nbPointForLigne = modeleDonnees().getNbPointForGeometry(i);
        final int[] res = CtuluListSelection.isSelectionContiguous(select.getSelection(i), nbPointForLigne);
        if (res == null) {
          return null;
        }
        // selection normale
        if (res[0] < res[1]) {
          return createLineFromSelection(i, selection, nbSelected);
        } else if (modeleDonnees().isGeometryFermee(i)) {
          final Coordinate[] cs = new Coordinate[nbSelected];
          int csi = 0;
          final CoordinateSequence seq = ((GISCoordinateSequenceContainerInterface) modeleDonnees().getGeomData().getGeometry(i)).
              getCoordinateSequence();
          for (int j = res[0]; j < nbPointForLigne; j++) {
            cs[csi++] = seq.getCoordinate(j);
          }
          for (int j = 0; j <= res[1]; j++) {
            cs[csi++] = seq.getCoordinate(j);
          }
          return GISGeometryFactory.INSTANCE.createLineString(cs);
        }
      }
    } else if (select.getNbListSelected() == 2) {
      final int[] idx = select.getIdxSelected();
      final CtuluListSelectionInterface selection1 = select.getSelection(idx[0]);
      final CtuluListSelectionInterface selection2 = select.getSelection(idx[1]);
      if (selection1.getNbSelectedIndex() == 1 && selection2.getNbSelectedIndex() == 1) {
        final Coordinate[] cs = new Coordinate[2];
        cs[0] = ((GISCoordinateSequenceContainerInterface) modeleDonnees().getGeomData().getGeometry(idx[0])).getCoordinateSequence().getCoordinate(
            selection1.getMaxIndex());
        cs[1] = ((GISCoordinateSequenceContainerInterface) modeleDonnees().getGeomData().getGeometry(idx[1])).getCoordinateSequence().getCoordinate(
            selection2.getMaxIndex());
        return GISGeometryFactory.INSTANCE.createLineString(cs);
      }
    }
    return null;
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexGeom,
                                         Object _newValue) {

    clearCacheAndRepaint();
  }

  @Override
  public void geometryAction(Object _source, int _idx, Geometry _geom, int _action) {
    // Si une g�om�trie a �t� supprim�e, on r�initialise la s�lection pour eviter
    // les effets de bord.
    if (_action == ZModelListener.GEOMETRY_ACTION_REMOVE) {
      clearSelection();
    }

    // Une g�om�trie a �t� ajout�e, supprim�e ou ses points la composant ont �t�
    // modifi�s, donc on repaint le tout.
    clearCacheAndRepaint();
  }
}
