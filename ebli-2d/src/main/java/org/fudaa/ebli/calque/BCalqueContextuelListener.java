/*
 *  @creation     20 nov. 2003
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;
import javax.swing.JPopupMenu;
/**
 * @author deniger
 * @version $Id: BCalqueContextuelListener.java,v 1.6 2006-04-12 15:27:09 deniger Exp $
 */
public interface BCalqueContextuelListener {
   JPopupMenu getCmdsContextuelles();
}
