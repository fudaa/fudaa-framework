/*
 * @creation 9 nov. 06
 * @modification $Date: 2006-11-14 09:06:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author fred deniger
 * @version $Id: ZCalqueAffichageDonneesLineAbstract.java,v 1.1 2006-11-14 09:06:26 deniger Exp $
 */
public abstract class ZCalqueAffichageDonneesLineAbstract extends ZCalqueAffichageDonnees {


  public ZCalqueAffichageDonneesLineAbstract() {
    ligneModel_ = new TraceLigneModel();
  }




}
