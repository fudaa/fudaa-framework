/*
 * @creation 9 nov. 06
 * @modification $Date: 2007-05-04 13:49:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurFont;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurReduitFonteNewVersion;

/**
 * @author fred deniger
 * @version $Id: ZCalqueAffichageDonneesConfigure.java,v 1.4 2007-05-04 13:49:43 deniger Exp $
 */
public class ZCalqueGeometryLabelConfigure extends BCalqueConfigureSectionAbstract {

  public static final String PROPERTY_LABELS_FOREGROUND = "labelsForegroundColor";
  public static final String PROPERTY_LABELS_BACKGROUND = "labelsBackgroundColor";
  public static final String PROPERTY_LABELS_VISIBLE = "labelsVisible";

  public ZCalqueGeometryLabelConfigure(final ZCalqueAffichageDonneesAbstract _target, final boolean _addVisible) {
    super(_target, EbliLib.getS("Labels"));
  }

  public ZCalqueGeometryLabelConfigure(final ZCalqueAffichageDonneesAbstract _target) {
    this(_target, false);
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (BSelecteurReduitFonteNewVersion.PROPERTY.equals(_key)) {
      ((ZCalqueAffichageDonneesAbstract) target_).setFont(((Font) _newProp));
      return true;
    }
    if (PROPERTY_LABELS_VISIBLE.equals(_key)) {
      if (target_ instanceof ZCalqueGeometry) {
        ((ZCalqueGeometry) target_).setLabelsVisible((Boolean) _newProp);
      }
      // Etendu aux ZCalquePointEditable, qui devraient d�river de ZCalqueGeometry
      else if (target_ instanceof ZCalquePointEditable) {
        ((ZCalquePointEditable) target_).setLabelsVisible((Boolean) _newProp);        
      }
      return true;
    }
    if (PROPERTY_LABELS_FOREGROUND.equals(_key)) {
      if (target_ instanceof ZCalqueGeometry) {
        ((ZCalqueGeometry) target_).setLabelsForegroundColor(((Color) _newProp));
      }
      // Etendu aux ZCalquePointEditable, qui devraient d�river de ZCalqueGeometry
      else if (target_ instanceof ZCalquePointEditable) {
        ((ZCalquePointEditable) target_).setLabelsForegroundColor(((Color) _newProp));        
      }
      return true;
    }
    if (PROPERTY_LABELS_BACKGROUND.equals(_key)) {
      if (target_ instanceof ZCalqueGeometry) {
        ((ZCalqueGeometry) target_).setLabelsBackgroundColor(((Color) _newProp));
      }
      // Etendu aux ZCalquePointEditable, qui devraient d�river de ZCalqueGeometry
      else if (target_ instanceof ZCalquePointEditable) {
        ((ZCalquePointEditable) target_).setLabelsBackgroundColor(((Color) _newProp));        
      }
      return true;
    }
    return false;
  }

  @Override
  public Object getProperty(final String _key) {
    if (BSelecteurReduitFonteNewVersion.PROPERTY.equals(_key)) {
      return ((ZCalqueAffichageDonneesAbstract) target_).getFont();
    }
    if (PROPERTY_LABELS_VISIBLE.equals(_key)) {
      if (target_ instanceof ZCalqueGeometry) {
        return ((ZCalqueGeometry) target_).isLabelsVisible();
      }
      // Etendu aux ZCalquePointEditable, qui devraient d�river de ZCalqueGeometry
      else if (target_ instanceof ZCalquePointEditable) {
        return ((ZCalquePointEditable) target_).isLabelsVisible();
      }
    }
    if (PROPERTY_LABELS_FOREGROUND.equals(_key)) {
      if (target_ instanceof ZCalqueGeometry) {
        return ((ZCalqueGeometry) target_).getLabelsForegroundColor();
      }
      // Etendu aux ZCalquePointEditable, qui devraient d�river de ZCalqueGeometry
      else if (target_ instanceof ZCalquePointEditable) {
        return ((ZCalquePointEditable) target_).getLabelsForegroundColor();
      }
    }
    if (PROPERTY_LABELS_BACKGROUND.equals(_key)) {
      if (target_ instanceof ZCalqueGeometry) {
        return ((ZCalqueGeometry) target_).getLabelsBackgroundColor();
      }
      // Etendu aux ZCalquePointEditable, qui devraient d�river de ZCalqueGeometry
      else if (target_ instanceof ZCalquePointEditable) {
        return ((ZCalquePointEditable) target_).getLabelsBackgroundColor();
      }
    }
    return null;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final List<BSelecteurInterface> dest = new ArrayList<>();
    
    final BSelecteurCheckBox cbActive = new BSelecteurCheckBox(PROPERTY_LABELS_VISIBLE);
    cbActive.setTitle(EbliLib.getS("Activ�"));
    cbActive.setTooltip(EbliLib.getS("Afficher les labels"));
    cbActive.setAddListenerToTarget(true);
    dest.add(cbActive);

    BSelecteurColorChooserBt foregroundColor = new BSelecteurColorChooserBt(false, PROPERTY_LABELS_FOREGROUND);
    foregroundColor.setTitle(EbliLib.getS("Couleur texte"));
    dest.add(foregroundColor);
    
    BSelecteurColorChooserBt backgroundColor = new BSelecteurColorChooserBt(true, PROPERTY_LABELS_BACKGROUND);
    backgroundColor.setTitle(EbliLib.getS("Couleur fond"));
    dest.add(backgroundColor);
    dest.add(new BSelecteurFont());
    return dest.toArray(new BSelecteurInterface[dest.size()]);
  }
}
