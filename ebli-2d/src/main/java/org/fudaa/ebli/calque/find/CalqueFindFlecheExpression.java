/*
 * @creation 8 ao�t 2005
 * 
 * @modification $Date: 2007-04-16 16:35:08 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.find;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.nfunk.jep.Variable;

/**
 * Fournisseur d'expression pour les vecteurs.
 * 
 * @author Fred Deniger
 * @version $Id$
 */
public class CalqueFindFlecheExpression extends CalqueFindExpression {

  Variable x_;
  Variable y_;
  Variable norm_;
  Variable vx_;
  Variable vy_;
  final boolean useNode_;

  /**
   * @param _data
   */
  public CalqueFindFlecheExpression(final ZModeleSegment _data, final boolean _useNode) {
    super(_data);
    useNode_ = _useNode;
  }

  @Override
  protected Variable createIndexVariable(final CtuluExpr _expr) {
    return _expr.addVar("i", useNode_ ? EbliLib.getS("Indice du noeud") : EbliLib.getS("Indice du point"));
  }

  @Override
  protected Variable createNbObjectVariable(final CtuluExpr _expr) {
    return _expr.addVar("nb", useNode_ ? EbliLib.getS("Nombre total de noeuds") : EbliLib
        .getS("Nombre total de points"));
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    x_ = useNode_ ? CalqueFindExpression.buildXNode(_expr) : CalqueFindExpression.buildXPoint(_expr);
    y_ = useNode_ ? CalqueFindExpression.buildYNode(_expr) : CalqueFindExpression.buildYPoint(_expr);
    vx_ = _expr.addVar("vectX", EbliLib.getS("La composante du vecteur selon x"));
    vy_ = _expr.addVar("vectY", EbliLib.getS("Le vecteur selon y"));
    norm_ = _expr.addVar("vectNorm", EbliLib.getS("La norme du vecteur"));
  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    ((ZModeleSegment) super.data_).segment(seg, _idx, true);
    double x = seg.o_.x_;
    double y = seg.o_.y_;
    x_.setValue(CtuluLib.getDouble(x));
    y_.setValue(CtuluLib.getDouble(y));
    x = seg.e_.x_ - x;
    y = seg.e_.y_ - y;
    vx_.setValue(CtuluLib.getDouble(x));
    vy_.setValue(CtuluLib.getDouble(y));
    norm_.setValue(CtuluLib.getDouble(Math.hypot(x, y)));
  }

}
