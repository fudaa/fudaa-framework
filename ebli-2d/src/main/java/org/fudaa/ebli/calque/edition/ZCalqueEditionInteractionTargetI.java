/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;

/**
 * @author Fred Deniger
 * @version $Id: ZCalqueEditionInteractionTargetI.java,v 1.4.8.1 2008-03-26 16:46:43 bmarchan Exp $
 */
public interface ZCalqueEditionInteractionTargetI {

  /**
   * Appele lorsqu'un point d'une ligne brisee (poly, rectangle) est ajoutee/supprim� ou quand une forme est ajout�e.
   */
  void atomicChanged();

  /**
   * Demande d'ajout de point.
   * @param _point le point ajoute
   * @param _data les donn�es en chaque point (peut-etre null)
   * @return si false, l'edition n'est pas arrete
   */
  boolean addNewPoint(GrPoint _point,ZEditionAttributesDataI _data);

  /**
   * Cette fonction est appell�e par le constructeur de forme pour pr�venir que le point
   * en cours de construction (g�n�ralement le curseur de la sourie) est en mouvement.
   * Les coordonn�es sont la position actuelle de ce point.
   * @param _x : coordonn�e x du point en mouvement.
   * @param _y : coordonn�e y du point en mouvement.
   */
  void pointMove(double _x, double _y);
  
  /**
   * Demande d'ajout.
   * @param _pt la polyligne ajoutee
   * @param _data les donn�es en chaque point (peut-etre null)
   * @return si false, l'edition n'est pas arrete
   */
  boolean addNewPolyligne(GrPolyligne _pt,ZEditionAttributesDataI _data);

  /**
   * Ajout d'un multipoint (groupe de points).
   */
  boolean addNewMultiPoint(GrPolyligne _pt,ZEditionAttributesDataI _data);
  
  /**
   * Demande d'ajout.
   * @param _pt la polyligne ajoutee
   * @param _data les donn�es en chaque point (peut-etre null)
   * @return si false, l'edition n'est pas arrete
   */
  boolean addNewPolygone(GrPolygone _pt,ZEditionAttributesDataI _data);

  void setMessage(String _s);
  void unsetMessage();

}
