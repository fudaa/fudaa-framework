/*
 *  @creation     15 sept. 2003
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
/**
 * @author deniger
 * @version $Id: EbliFilleCalquesInterface.java,v 1.6 2006-04-12 15:27:09 deniger Exp $
 */
public interface EbliFilleCalquesInterface {
  /**
   * @return le model pour l'arbre calque
   */
   BArbreCalqueModel getArbreCalqueModel();
}
