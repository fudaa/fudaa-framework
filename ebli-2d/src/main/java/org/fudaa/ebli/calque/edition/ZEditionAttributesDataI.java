/*
 * @creation 4 avr. 2005
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license GNU General Public License 2
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.fudaa.ctulu.gis.GISAttributeInterface;

/**
 * Interface de transport des valeurs d'attributs durant une cr�ation g�om�trique.
 * @author fred deniger
 * @version $Id$
 */
public interface ZEditionAttributesDataI {

  /**
   * @return Nombre de sommets de la g�om�trie en cours.
   */
  public abstract int getNbVertex();

  /**
   * @return Le nombre de valeurs d'attribut par sommet.
   */
  public abstract int getNbValues();

  /**
   * @param _i L'index d'attribut [0,getNbValues()]
   * @return L'attribut
   */
  public abstract GISAttributeInterface getAttribute(int _i);

  /**
   * Retourne la valeur pour l'attribut et le sommet. Si l'attribut est global, le num�ro de sommet
   * n'a pas d'importance.
   * 
   * @param _attr La definition d'attribut
   * @param _idxVertex Le sommet.
   * @return La valeur atomique pour le sommet.
   */
  public abstract Object getValue(GISAttributeInterface _attr,int _idxVertex);

  /**
   * Definit la valeur pour l'attribut et le sommet.
   * @param _attr La definition d'attribut
   * @param _idxVertex Le sommet.
   * @param _val La valeur atomique.
   */
  public abstract void setValue(GISAttributeInterface _attr,int _idxVertex, Object _val);
}