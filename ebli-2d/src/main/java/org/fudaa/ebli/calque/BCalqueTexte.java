/*
 * @file         BCalqueTexte.java
 * @creation     1998-08-31
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.trace.TracePoint;

/**
 * Un calque d'affichage de textes.
 *
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BCalqueTexte extends BCalqueAffichage {
  VecteurGrPoint points_;
  Map textes_;

  public BCalqueTexte() {
    super();
    reinitialise();
  }

  // Icon
  /**
   * Dessin de l'icone.
   *
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final boolean attenue = isAttenue();
    Color fg = getForeground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(fg);
    _g.drawRect(4, 4, 8, 4);
    _g.drawRect(w - 10, h - 6, 8, 4);
    _g.drawRect(w / 4, h / 2, 8, 4);
    _g.setColor(bg);
    _g.fillRect(5, 5, 6, 2);
    _g.fillRect(w - 9, h - 5, 6, 2);
    _g.fillRect(w / 4 + 1, h / 2 + 1, 6, 2);
  }

  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    final boolean rapide = isRapide();
    final boolean attenue = isAttenue();
    final boolean pleine = isEtiquettePleine();
    Color fg = getForeground();
    // if(fg==null) fg=Color.black;
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    // if(bg==null) bg=new Color(255,255,224);
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = _g.getFontMetrics();
    TracePoint trace = null;
    if (!rapide) {
      trace = new TracePoint();
      trace.setTypePoint(typePoint_);
      trace.setCouleur(fg);
    }
    final GrMorphisme versEcran = getVersEcran();
    for (int i = 0; i < points_.nombre(); i++) {
      final GrPoint pr = points_.renvoie(i);
      final GrPoint pe = pr.applique(versEcran);
      final int x = (int) pe.x_;
      final int y = (int) pe.y_;
      if (rapide) {
        _g.drawLine(x + 5, y, x + 55, y);
      } else if(trace!=null){
        trace.dessinePoint((Graphics2D) _g, x, y);
        final Object o = textes_.get(pr);
        if (o != null) {
          final StringTokenizer st = new StringTokenizer((String) o, "\n");
          final String[] s = new String[st.countTokens()];
          final int hs = fm.getAscent() + fm.getDescent();
          int ws = 0;
          for (int c = 0; c < s.length; c++) {
            s[c] = st.nextToken();
            ws = Math.max(ws, fm.stringWidth(s[c]));
          }
          final int hst = hs * s.length;
          if (pleine) {
            if (surPosition_) {
              _g.setColor(bg);
              _g.fillRect(x - ws / 2 - 2, y - hst / 2 - 2, ws + 4, hst + 4);
              _g.setColor(fg);
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x - ws / 2, y + hs * c - hst / 2 + fm.getAscent());
              }
              _g.drawRect(x - ws / 2 - 2, y - hst / 2 - 2, ws + 4, hst + 4);
            } else {
              _g.setColor(bg);
              _g.fillRect(x + 5, y - hst / 2 - 2, ws + 4, hst + 4);
              _g.setColor(fg);
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x + 7, y + hs * c - hst / 2 + fm.getAscent());
              }
              _g.drawRect(x + 5, y - hst / 2 - 2, ws + 4, hst + 4);
            }
          } else {
            if (surPosition_) {
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x - ws / 2, y + hs * c - hst / 2 + fm.getAscent());
              }
            } else {
              for (int c = 0; c < s.length; c++) {
                _g.drawString(s[c], x + 5, y + hs * c);
              }
            }
          }
        }
      }
    }
    super.paintComponent(_g);
  }

  /**
   * Reinitialise la liste des textes de ce calque.
   */
  public void reinitialise() {
    points_ = new VecteurGrPoint();
    textes_ = new Hashtable();
  }

  /**
   * Ajoute un texte a ce calque.
   *
   * @param _point position
   * @param _texte texte
   */
  public void ajoute(final GrPoint _point, final String _texte) {
    points_.ajoute(_point);
    textes_.put(_point, _texte);
  }

  public VecteurGrPoint getPositions() {
    return points_;
  }

  public void setPositions(final VecteurGrPoint _lp) {
    if (_lp == points_) {
      return;
    }
    final VecteurGrPoint vp = points_;
    points_ = new VecteurGrPoint();
    for (int i = 0; i < _lp.nombre(); i++) {
      points_.ajoute(_lp.renvoie(i));
    }
    firePropertyChange("positions", vp, points_);
  }

  public String getTexte(final GrPoint _p) {
    return (String) textes_.get(_p);
  }

  public String[] getTextes() {
    final String[] res = new String[points_.nombre()];
    for (int i = 0; i < points_.nombre(); i++) {
      res[i] = getTexte(points_.renvoie(i));
    }
    return res;
  }

  public void setTextes(final String[] _t) {
    textes_ = new Hashtable();
    final String[] vp = getTextes();
    for (int i = 0; i < Math.min(points_.nombre(), _t.length); i++) {
      textes_.put(points_.renvoie(i), _t[i]);
    }
    firePropertyChange("textes", vp, getTextes());
  }

  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible()) {
      r = super.getDomaine();
      if (points_.nombre() > 0) {
        if (r == null) {
          r = new GrBoite();
        }
        for (int i = 0; i < points_.nombre(); i++) {
          r.ajuste(points_.renvoie(i));
        }
      }
    }
    return r;
  }

  /**
   * Renvoi de la liste des elements selectionnables.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res = new VecteurGrContour();
    res.tableau(points_.tableau());
    return res;
  }
  // Proprietes
  protected int typePoint_;

  /**
   * Accesseur de la propriete <I>typePoint</I>. Elle definit le style de trace des points, en prenant ses valeurs dans
   * les champs statiques de <I>Tracepoint</I>. Par defaut, elle vaut TracePoint.POINT
   *
   * @see org.fudaa.ebli.trace.TracePoint
   */
  public int getTypePoint() {
    return typePoint_;
  }

  /**
   * Affectation de la propriete <I>typePoint</I>.
   */
  public void setTypePoint(final int _typePoint) {
    if (typePoint_ != _typePoint) {
      final int vp = typePoint_;
      typePoint_ = _typePoint;
      firePropertyChange("TypePoint", vp, typePoint_);
      repaint();
    }
  }
  protected boolean etiquettePleine_;

  /**
   * Accesseur de la propriete <I>etiquettePleine</I>. Elle dit si les etiquette doivent etre colorees ou non.
   */
  boolean isEtiquettePleine() {
    return etiquettePleine_;
  }

  /**
   * Affectation de la propriete <I>stiquettePleine</I>.
   */
  public void setEtiquettePleine(final boolean _etiquettePleine) {
    if (etiquettePleine_ != _etiquettePleine) {
      final boolean vp = etiquettePleine_;
      etiquettePleine_ = _etiquettePleine;
      firePropertyChange("etiquettePleine", vp, etiquettePleine_);
      repaint();
    }
  }
  protected boolean surPosition_;

  /**
   * Accesseur de la propriete <I>surPosition</I>. Elle dit si les etiquettes doivent etre centrees sur la position ou
   * decalees.
   */
  public  boolean isSurPosition() {
    return surPosition_;
  }

  /**
   * Affectation de la propriete <I>surPosition</I>.
   */
  public void setSurPosition(final boolean _surPosition) {
    if (surPosition_ != _surPosition) {
      final boolean vp = surPosition_;
      surPosition_ = _surPosition;
      firePropertyChange("surPosition", vp, surPosition_);
      repaint();
    }
  }
}
