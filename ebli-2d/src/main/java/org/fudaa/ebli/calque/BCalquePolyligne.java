 /*
 * @file         BCalquePolyligne.java
 * @creation     1998-08-24
 * @modification $Date: 2006-11-14 09:06:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPolyligne;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Un calque d'affichage de polylignes.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-11-14 09:06:24 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class BCalquePolyligne extends BCalqueAffichage {
  // donnees membres publiques
  // donnees membres privees
  protected VecteurGrPolyligne lignes_;
  int densite_= 1000;
  // Constructeur
  public BCalquePolyligne() {
    super();
    lignes_= new VecteurGrPolyligne();
    typeTrait_= TraceLigne.LISSE;
    resolution_= 1000;
    //    couleur_=getForeground();
    if (getForeground() == null) {
      setForeground(Color.black);
    //    if( couleur_==null ) couleur_=Color.black;
    }
  }
  // Icon
  @Override
  public void paintIcon(final Component _c, final Graphics _g,final  int _x,final  int _y) {
    super.paintIcon(_c, _g, _x, _y);
    _g.translate(_x, _y);
    final int w= getIconWidth() - 1;
    final int h= getIconHeight() - 1;
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    _g.setColor(fg);
    if (lignes_.nombre() > 0) {
      _g.drawLine(0, h / 2, w / 2, h / 2);
      _g.drawLine(w / 2, h / 2, w, 1);
      if (lignes_.nombre() > 1) {
        _g.drawLine(0, h / 2 + h / 4, w / 2, h / 2 + h / 4);
        _g.drawLine(w / 2, h / 2 + h / 4, w, h / 4);
      }
    }
    _g.translate(-_x, -_y);
  }
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    final TraceGeometrie trace= new TraceGeometrie(getVersEcran());
    trace.setTypeTrait(typeTrait_);
    trace.setForeground(fg);
    //boolean rapide= isRapide();
    for (int i= 0; i < lignes_.nombre(); i += 1000 / densite_) {
      trace.dessinePolyligne((Graphics2D)_g,lignes_.renvoie(i), resolution_, rapide_);
    }
  }
  /**
    * Reinitialise la liste de polylignes.
    */
  public void reinitialise() {
    lignes_= new VecteurGrPolyligne();
  }
  /**
    * Ajoute une polyligne a la liste de polylignes de ce calque.
    */
  public void ajoute(final GrPolyligne _ligne) {
    lignes_.ajoute(_ligne);
  }
  /**
    * Retire une polyligne a la liste de polylignes de ce calque.
    */
  public void enleve(final GrPolyligne _ligne) {
    lignes_.enleve(_ligne);
  }
  public VecteurGrPolyligne getPolylignes() {
    return lignes_;
  }
  public void setPolylignes(final VecteurGrPolyligne _lp) {
    if (_lp == lignes_) {
      return;
    }
    final VecteurGrPolyligne vp= lignes_;
    reinitialise();
    for (int i= 0; i < _lp.nombre(); i++){
      ajoute(_lp.renvoie(i));
    }
    firePropertyChange("polylignes", vp, lignes_);
  }
  /**
    * Renvoie la l-ieme polyligne de ce calque.
    */
  public GrPolyligne getPolyligne(final int _l) {
    return lignes_.renvoie(_l);
  }
  /**
    * Nombre de polylignes dans ce calque.
    */
  public int nombre() {
    return lignes_.nombre();
  }
  /**
   * Renvoi de la liste des elements selectionnables.
   */
  @Override
  public VecteurGrContour contours() {
    final  VecteurGrContour res= new VecteurGrContour();
    res.tableau(lignes_.tableau());
    return res;
  }
  /**
   * Boite englobante des objets contenus dans le calque.
   * @return Boite englobante. Si le calque est vide,
   * la boite englobante retourn�e est <I>null</I>
   */
  @Override
  public GrBoite getDomaine() {
    GrBoite r= null;
    if (isVisible() && lignes_.nombre() > 0) {
      r= new GrBoite();
      for (int i= 0; i < lignes_.nombre(); i++){
        r.ajuste(lignes_.renvoie(i).boite());
      }
    }
    return r;
  }
  //
  // PROPRIETES INTERNES
  //
  // Propriete typeTrait
  protected int typeTrait_;
  /**
    * Accesseur de la propriete <I>typeTrait</I>.
    * Elle definit le style de trait pour le trace en prenant ses valeurs dans
    * les champs statiques de <I>TraceLigne</I>.
    * Par defaut, elle vaut TraceLigne.LISSE
    * @see TraceLigne
    */
  public int getTypeTrait() {
    return typeTrait_;
  }
  /**
    * Affectation de la propriete <I>typeTrait</I>.
    */
  public void setTypeTrait(final int _typeTrait) {
    if (typeTrait_ != _typeTrait) {
      final  int vp= typeTrait_;
      typeTrait_= _typeTrait;
      firePropertyChange("TypeTrait", vp, typeTrait_);
      repaint();
    }
  }
  // Propriete couleur
  //  private Color couleur_;
  /**
    * Affectation de la propriete <I>couleur</I>.
    */
  //  public void setCouleur(Color _couleur)
  //  {
  //    if(couleur_!=_couleur)
  //    {
  //      Color vp=couleur_;
  //      couleur_=_couleur;
  //      firePropertyChange("couleur",vp,couleur_);
  //      repaint();
  //    }
  //  }
  //  public void setForeground(Color _couleur)
  //  {
  //    super.setForeground();
  //    repaint();
  //  }
  /**
   * @deprecated
   * Accesseur de la propriete <I>couleur</I>.
   * Elle definit la couleur de trace pour ce calque.
   * Par defaut, elle est noire.
   */
  //  public Color getCouleur() { return couleur_; }
  // LAISSE POUR COMPATIBILITE
  // Propriete resolution
  protected int resolution_;
  /**
    * Affectation de la propriete <I>resolution</I>.
    */
  public void setResolution(final int _resolution) {
    if (resolution_ != _resolution) {
      final  int vp= resolution_;
      resolution_= _resolution;
      firePropertyChange("resolution", vp, resolution_);
      repaint();
    }
  }
  public void setDensite(final int _densite) {
    if (densite_ != _densite) {
      final  int vp= densite_;
      densite_= _densite;
      firePropertyChange("densite", vp, densite_);
      repaint();
    }
  }
  /**
    * Accesseur de la propriete <I>resolution</I>.
    * Elle definit la resolution de trace pour ce calque.
    * Par defaut, elle est maximum.
    */
  public int getResolution() {
    return resolution_;
  }
  public int getDensite() {
    return densite_;
  }
}
