/*
 * Cache pour eviter de redessiner la totalit� d'un calque
 */
package org.fudaa.ebli.calque;

import java.awt.Graphics;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibImage;

/**
 * @author deniger
 */
public abstract class AbstractBCalqueCacheManager {

  protected final ZCalqueAffichageDonneesAbstract layer;
  protected Image image;
  Map param = new HashMap();

  /**
   * @param calque
   */
  public AbstractBCalqueCacheManager(final ZCalqueAffichageDonneesAbstract calque) {
    super();
    this.layer = calque;
    CtuluLibImage.setCompatibleImageAsked(param);
  }

  public void clearCache() {
    if (image != null) {
      image.flush();
      image = null;
    }
  }

  public boolean isCacheUptodate() {
    return image != null;
  }

  public Graphics getImageGraphics() {
    if (image == null) {
      image = CtuluLibImage.createImage(layer.getWidth(), layer.getHeight(), param);
    }
    return image.getGraphics();
  }

  protected static class CacheManagerPropertyChangeListener implements PropertyChangeListener {

    private final AbstractBCalqueCacheManager cacheManager;
    //on reagit au versReel uniquement.
    protected HashSet<String> ignoreProperties = new HashSet<String>(Arrays.asList("ancestor", "paintingForPrint", "versEcran"));

    public CacheManagerPropertyChangeListener(AbstractBCalqueCacheManager cacheManager) {
      this.cacheManager = cacheManager;
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt) {
      if (!ignoreProperties.contains(evt.getPropertyName())) {
        cacheManager.clearCache();
      }
    }
  }
}
