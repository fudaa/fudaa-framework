/*
 * @file         BCalquePolygone.java
 * @creation     1998-08-28
 * @modification $Date: 2006-11-14 09:06:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPolygone;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceSurface;

/**
 * Un calque d'affichage de polygones.
 * 
 * @version $Revision: 1.9 $ $Date: 2006-11-14 09:06:24 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BCalquePolygone extends BCalqueAffichage {
  // donnees membres publiques
  // donnees membres privees
  VecteurGrPolygone gones_;
  // Resolution:d�tail des polygones
  // Densit�
  int resolution_ = 1000;
  int densite_ = 1000;

  // Constructeur
  public BCalquePolygone() {
    super();
    gones_ = new VecteurGrPolygone();
    typeTrait_ = TraceLigne.LISSE;
    typeSurface_ = TraceSurface.INVISIBLE;
    if (getForeground() == null) {
      setForeground(Color.black);
    }
    if (getBackground() == null) {
      setBackground(Color.white);
    }
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    _g.translate(_x, _y);
    final boolean attenue = isAttenue();
    final int w = getIconWidth();
    final int h = getIconHeight();
    Color fg = getForeground();
    Color bg = getBackground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    if (gones_.nombre() > 0) {
      _g.setColor(bg);
      _g.fillRect(2, 2, w / 2, h / 2);
      _g.setColor(fg);
      _g.drawRect(2, 2, w / 2, h / 2);
      if (gones_.nombre() > 1) {
        _g.setColor(bg);
        _g.fillRect(w / 2 - 2, h / 4 + 1, w / 2, h / 2);
        _g.setColor(fg);
        _g.drawRect(w / 2 - 2, h / 4 + 1, w / 2, h / 2);
      }
    }
    _g.translate(-_x, -_y);
  }

  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    Color fg = getForeground();
    Color bg = getBackground();
    if (isAttenue()) {
      fg = attenueCouleur(fg);
      bg = attenueCouleur(bg);
    }
    final TraceGeometrie trace = new TraceGeometrie(getVersEcran());
    trace.setForeground(fg);
    trace.setBackground(bg);
    trace.setTypeSurface(typeSurface_);
    trace.setTypeTrait(typeTrait_);
    final boolean rapide = isRapide();
    final int n = gones_.nombre();
    final Graphics2D g2d = (Graphics2D) _g;
    for (int i = 0; i < n; i += 1000 / densite_) {
      final GrPolygone gone = gones_.renvoie(i);
      trace.dessinePolygone(g2d, gone, resolution_, true, rapide);
    }
  }

  /**
   * Reinitialise la liste de polygones.
   */
  public void reinitialise() {
    gones_ = new VecteurGrPolygone();
  }

  /**
   * Ajoute un polygone a la liste de polygones.
   */
  public void ajoute(final GrPolygone _gone) {
    gones_.ajoute(_gone);
  }

  /**
   * Retire un polygone a la liste de polygones.
   */
  public void enleve(final GrPolygone _gone) {
    gones_.enleve(_gone);
  }

  public VecteurGrPolygone getPolygones() {
    return gones_;
  }

  public void setPolygones(final VecteurGrPolygone _lp) {
    if (_lp == gones_) {
      return;
    }
    final VecteurGrPolygone vp = gones_;
    reinitialise();
    for (int i = 0; i < _lp.nombre(); i++) {
      ajoute(_lp.renvoie(i));
    }
    firePropertyChange("polygones", vp, gones_);
  }

  /**
   * Renvoi de la liste des elements selectionnables.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res = new VecteurGrContour();
    res.tableau(gones_.tableau());
    return res;
  }

  /**
   * Boite englobante des objets contenus dans le calque.
   * 
   * @return Boite englobante. Si le calque est vide, la boite englobante retourn�e est <I>null</I>
   */
  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible() && gones_.nombre() > 0) {
      r = new GrBoite();
      for (int i = 0; i < gones_.nombre(); i++) {
        r.ajuste(gones_.renvoie(i).boite());
      }
    }
    return r;
  }
  //
  // PROPRIETES INTERNES
  //
  // Propriete typeTrait
  private int typeTrait_;

  /**
   * Accesseur de la propriete <I>typeTrait</I>. Elle definit le style de trait pour le trace en prenant ses valeurs
   * dans les champs statiques de <I>TraceLigne</I>. Par defaut, elle vaut TraceLigne.LISSE
   * 
   * @see org.fudaa.ebli.trace.TraceLigne
   */
  public int getTypeTrait() {
    return typeTrait_;
  }

  /**
   * Affectation de la propriete <I>typeTrait</I>.
   */
  public void setTypeTrait(final int _typeTrait) {
    if (typeTrait_ != _typeTrait) {
      final int vp = typeTrait_;
      typeTrait_ = _typeTrait;
      firePropertyChange("TypeTrait", vp, typeTrait_);
      repaint();
    }
  }
  // Propriete typeTrait
  private int typeSurface_;

  /**
   * Accesseur de la propriete <I>typeSurface</I>. Elle definit le style de surface pour le trace en prenant ses
   * valeurs dans les champs statiques de <I>TraceSurface</I>. Par defaut, elle vaut TraceSurface.LISSE
   * 
   * @see org.fudaa.ebli.trace.TraceSurface
   */
  public int getTypeSurface() {
    return typeSurface_;
  }

  /**
   * Affectation de la propriete <I>typeSurface</I>.
   */
  public void setTypeSurface(final int _typeSurface) {
    if (typeSurface_ != _typeSurface) {
      final int vp = typeSurface_;
      typeSurface_ = _typeSurface;
      firePropertyChange("TypeSurface", vp, typeSurface_);
      repaint();
    }
  }

  // private Color couleur_;
  /**
   * Affectation de la propriete <I>couleur</I>.
   */
  // public void setCouleur(Color _couleur)
  // {
  // if(couleur_!=_couleur)
  // {
  // Color vp=couleur_;
  // couleur_=_couleur;
  // firePropertyChange("couleur",vp,couleur_);
  // repaint();
  // }
  // }
  /**
   * Accesseur de la propriete <I>couleur</I>. Elle definit la couleur de trace pour ce calque. Par defaut, elle est
   * noire.
   */
  // public Color getCouleur() { return couleur_; }
  // public void setForeground(Color _couleur)
  // {
  // super.setForeground();
  // repaint();
  // }
  // LAISSE POUR COMPATIBILITE
  public void setResolution(final int _resolution) {
    if (resolution_ != _resolution) {
      final int vp = resolution_;
      resolution_ = _resolution;
      firePropertyChange("resolution", vp, resolution_);
      repaint();
    }
  }

  public int getResolution() {
    return resolution_;
  }

  public void setDensite(final int _densite) {
    if (densite_ != _densite) {
      final int vp = densite_;
      densite_ = _densite;
      firePropertyChange("densite", vp, densite_);
      repaint();
    }
  }

  public int getDensite() {
    return densite_;
  }
}
