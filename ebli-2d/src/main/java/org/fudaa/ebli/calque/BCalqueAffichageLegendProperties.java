/*
 * @creation 19 juin 07
 * @modification $Date: 2007-06-20 12:23:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuPreferences;
import java.awt.Color;
import java.awt.Font;
import org.fudaa.ebli.commun.EbliUIProperties;

public class BCalqueAffichageLegendProperties {
  private static String getBackgroundId() {
    return "legend.panel.background";
  }

  public static boolean isSavedInProperties(EbliUIProperties _props) {
    return _props != null && _props.isDefined(getBackgroundId());
  }
  private Color background_;
  private int bord_;

  private Color borderColor_;

  private Font font_;

  public BCalqueAffichageLegendProperties() {
    background_ = BCalqueLegendePanel.getDefaultBgColor();
    font_ = BCalqueLegendePanel.getDefaultFont();
    bord_ = BCalqueLegendePanel.getDefaultBordEpaisseur();
    borderColor_ = BCalqueLegendePanel.getDefaultBordColor();
  }

  protected void initFrom(BCalqueAffichageLegendProperties _prop) {
    if (_prop != null) {
      setBackground(_prop.getBackground());
      setFont(_prop.getFont());
      setBord(_prop.getBord());
      setBorderColor(_prop.getBorderColor());
    }
  }
  

  void restoreFrom(EbliUIProperties _prop, BCalqueLegendePanel _support) {
    if (_prop == null) return;
    if (_prop.isDefined(getBackgroundId())) {
      background_ = (Color) _prop.get(getBackgroundId());
      if (_support != null) _support.setBackground(background_);
    }
    if (_prop.isDefined("legend.panel.font")) {
      font_ = BuPreferences.createFontFromString(font_, _prop.getString("legend.panel.font"));
      if (_support != null) _support.setFont(font_);
    }
    if (_prop.isDefined("legend.panel.border.color")) {
      borderColor_ = (Color) _prop.get("legend.panel.border.color");
      if (_support != null) _support.setBordColor(borderColor_);
    }
    if (_prop.isDefined("legend.panel.border.height")) {
      bord_ = _prop.getInteger("legend.panel.border.height");
      if (_support != null) _support.setBordEpaisseur(bord_);
    }

  }

  void saveInProperties(EbliUIProperties _prop) {
    _prop.put(getBackgroundId(), background_);
    _prop.put("legend.panel.font", BuPreferences.getStringValue(font_));
    _prop.put("legend.panel.border.color", borderColor_);
    _prop.put("legend.panel.border.height", bord_);
  }

  public Color getBackground() {
    return background_;
  }

  public int getBord() {
    return bord_;
  }

  public Color getBorderColor() {
    return borderColor_;
  }

  public Font getFont() {
    return font_;
  }

  public void setBackground(Color _background) {
    background_ = _background;
  }

  public void setBord(int _bord) {
    bord_ = _bord;
  }

  public void setBorderColor(Color _borderColor) {
    borderColor_ = _borderColor;
  }

  public void setFont(Font _font) {
    font_ = _font;
  }

}