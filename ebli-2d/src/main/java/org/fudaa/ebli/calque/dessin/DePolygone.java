/**
 * @creation     1998-08-31
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceSurface;
/**
 * Un polygone.
 *
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 14:55:53 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class DePolygone extends DeLigneBrisee {
  // donnees membres publiques
  // donnees membres privees
  // Constructeurs
  public DePolygone() {
    super();
  }
  public DePolygone(final GrPolyligne _l) {
    super(_l);
  }
  public DePolygone(final GrPolygone _g) {
    super();
    ligne_.sommets_= _g.sommets_;
  }
  @Override
  public int getForme() {
    return POLYGONE;
  }
  public DePolygone(final DeLigneBrisee _l) {
    super(_l.ligne_);
  }
  // Methodes publiques
  @Override
  public GrObjet getGeometrie() {
    final GrPolygone res= new GrPolygone();
    res.sommets_= ligne_.sommets_;
    return res;
  }
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
    _t.dessinePolygone(_g2d,
      ligne_,
      ((couleurRemplissage_ != null)
        || (typeSurface_ != TraceSurface.UNIFORME)),
      _rapide);
  }
  // >>> Interface GrContour ---------------------------------------------------
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    // Polygone "plein"
    if (couleurRemplissage_ != null || typeSurface_ != TraceSurface.UNIFORME) {
      return ligne_.toGrPolygone().estSelectionne(_ecran, _dist, _pt);
    }
    // Polygone "vide"
      return ligne_.estSelectionne(_ecran, _dist, _pt);
  }
  // <<< Interface GrContour ---------------------------------------------------
}
