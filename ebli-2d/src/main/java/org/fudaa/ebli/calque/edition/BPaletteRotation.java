/*
 *  @creation     5 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Un panneau pour la rotation d'objets GIS. 
 * Ce panneau est int�gr� dans la palette des outils lorsque l'outil "rotation" est s�lectionn�. La rotation
 * se fait suivant le barycentre des objets s�lectionn�s, sauf si l'origine est explicitement donn�e.
 * 
 * @author Bertrand Marchand
 * @version $Id: BPaletteRotation.java,v 1.1.2.1 2008-05-13 12:10:25 bmarchan Exp $
 */
public class BPaletteRotation extends JPanel implements BPalettePanelInterface, ZSelectionListener, ActionListener {

//  private ZCalqueEditable cqTarget_;
  private ZSceneEditor sceneEditor_;
  private BuCheckBox cbOrig_;
  private BuTextField tfX0_;
  private BuTextField tfY0_;
  private BuTextField tfAng_;
  private BuToolButton btApply_;
  private BuLabel lbX0_;
  private BuLabel lbY0_;
  private EbliFormatterInterface formatter_;
//  private CtuluCommandContainer cmd_;
//  private CtuluUI ui_;
  
  /**
   * Constructeur.
   * @param _formatter Le formatteur pour les champs texte.
   * @param _cmd La pile de commandes.
   * @param _ui L'interface utilisateur.
   */
  public BPaletteRotation(ZSceneEditor _sceneEditor, final EbliFormatterInterface _formatter) {
    super();
    
    sceneEditor_=_sceneEditor;
    formatter_=_formatter;
//    ui_=_ui;
//    cmd_=_cmd;
    
    cbOrig_=new BuCheckBox(EbliLib.getS("Origine fix�e"));
    cbOrig_.setToolTipText(EbliLib.getS("Si non activ�, le centre de l'enveloppe des objets s�lectionn�s est pris"));
    cbOrig_.addActionListener(this);
    lbX0_=new BuLabel("x0:");
    lbY0_=new BuLabel("y0:");
    BuLabel lbAng=new BuLabel(EbliLib.getS("Angle:"));
    lbAng.setToolTipText(EbliLib.getS("En degr�s"));
    tfX0_ = BuTextField.createDoubleField();
    tfX0_.setPreferredSize(new Dimension(70,tfX0_.getPreferredSize().height));
    tfX0_.setValue(new Double(0));
    tfY0_ = BuTextField.createDoubleField();
    tfY0_.setValue(new Double(0));
    tfAng_ = BuTextField.createDoubleField();
    tfAng_.setValue(new Double(0));
    btApply_ = new BuToolButton(BuResource.BU.getToolIcon("appliquer"));
    btApply_.addActionListener(this);
    btApply_.setEnabled(false);
    
    BuPanel pnVals=new BuPanel();
    pnVals.setLayout(new BuGridLayout(2,2,2));
    pnVals.add(lbX0_);
    pnVals.add(tfX0_);
    pnVals.add(lbY0_);
    pnVals.add(tfY0_);
    pnVals.add(lbAng);
    pnVals.add(tfAng_);
    
    BuPanel pnMain=new BuPanel();
    pnMain.setLayout(new BorderLayout(5,5));
    pnMain.add(cbOrig_,BorderLayout.NORTH);
    pnMain.add(pnVals,BorderLayout.CENTER);
    
    BuPanel pnButtons=new BuPanel();
    pnButtons.setLayout(new BorderLayout(5,5));
    pnButtons.add(btApply_,BorderLayout.SOUTH);

    setLayout(new BorderLayout(5,5));
    add(pnMain,BorderLayout.CENTER);
    add(pnButtons,BorderLayout.EAST);

    cbOrig_.setSelected(false);
    tfX0_.setEnabled(false);
    tfY0_.setEnabled(false);
    lbX0_.setEnabled(false);
    lbY0_.setEnabled(false);

    sceneEditor_.getScene().addSelectionListener(this);
  }

  /**
   * Actions lanc�es lorsqu'un bouton du panneau est activ�.
   */
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if (_evt.getSource()==cbOrig_) {
      tfX0_.setEnabled(cbOrig_.isSelected());
      tfY0_.setEnabled(cbOrig_.isSelected());
      lbX0_.setEnabled(cbOrig_.isSelected());
      lbY0_.setEnabled(cbOrig_.isSelected());
    }
    // Rotation demand�e.
    else if (_evt.getSource()==btApply_) {
      if (sceneEditor_==null) return;
      Double angDeg=(Double)tfAng_.getValue();
      Double x0=(Double)tfX0_.getValue();
      Double y0=(Double)tfY0_.getValue();
      if (angDeg!=null && angDeg.doubleValue()!=0. && x0!=null && y0!=null) {
        sceneEditor_.rotateSelectedObjects(Math.toRadians(angDeg.doubleValue()), x0.doubleValue(), y0.doubleValue());
      }
    }
  }
 
  @Override
  public JComponent getComponent(){ return this; }
  

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    updateForSelectionChanged();
  }
  
  /**
   * Mise a jour du panneau, l'origine et le bouton apply, en fonction de la s�lection
   */
  public void updateForSelectionChanged() {
    if (sceneEditor_.getScene().isSelectionEmpty()) {
      btApply_.setEnabled(false);
      return;
    }
    
    btApply_.setEnabled(true);
    if (!cbOrig_.isSelected()) {
      GrBoite bt=sceneEditor_.getScene().getDomaineOnSelected();
      tfX0_.setText(formatter_.getXYFormatter().format(bt.getMidX()));
      tfY0_.setText(formatter_.getXYFormatter().format(bt.getMidY()));
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.commun.BPalettePanelInterface#doAfterDisplay()
   */
  @Override
  public void doAfterDisplay() {}

  /* (non-Javadoc)
   * @see org.fudaa.ebli.commun.BPalettePanelInterface#paletteDeactivated()
   */
  @Override
  public void paletteDeactivated() {}

  /*
   * Chgt du calque cible.
   */
  @Override
  public boolean setPalettePanelTarget(Object _target) {
    // TODO Auto-generated method stub
    return false;
  }
}
