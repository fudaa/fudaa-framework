/*
 * @creation 4 oct. 06
 * @modification $Date: 2008-01-17 16:20:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ebli.calque.ZModelePoint;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author fred deniger
 * @version $Id: ZModelePointEditableInterface.java,v 1.3.6.1 2008-01-17 16:20:14 bmarchan Exp $
 */
public interface ZModelePointEditableInterface extends ZModelePoint, ZModeleEditable {

  /**
   * @param _idx les indices des points a enlever
   * @param _cmd le receveur de commande
   * @return true si ok.
   */
  boolean removePoint(final int[] _idx, final CtuluCommandContainer _cmd);

  /**
   * Change les coordonnees du point d'indice _idx
   * @param _idx L'indice du point a modifier.
   * @param _newX Nouvelle coordonn�e en Y
   * @param _newY Nouvelle coordonn�e en Y
   * @param _cmd Le receveur de commande
   * @return true si le changement a �t� op�r�.
   */
  boolean setPoint(final int _idx, final double _newX, final double _newY, final CtuluCommandContainer _cmd);

  /**
   * @param _p le point a ajouter
   * @param _data les donnees associees
   * @param _cmd le receveur de commande
   */
  void addPoint(final GrPoint _p, final ZEditionAttributesDataI _data, final CtuluCommandContainer _cmd);

}
