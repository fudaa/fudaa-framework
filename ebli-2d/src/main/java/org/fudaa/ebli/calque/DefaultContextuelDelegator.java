/*
 * @creation     1999-07-07
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;
import java.beans.PropertyChangeSupport;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
/**
 * Un delegateur par defaut de menu contextuel.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 14:55:46 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class DefaultContextuelDelegator implements BCalqueContextuelListener {
  private String[] context_;
  PropertyChangeSupport bean_;
  public DefaultContextuelDelegator() {
    context_= null;
    bean_= new PropertyChangeSupport(this);
  }
  @Override
  public JPopupMenu getCmdsContextuelles() {
    return construitMenu(context_);
  }
  public JPopupMenu construitMenu(final String[] _context) {
    if ((_context == null) || (_context.length == 0)) {
      return null;
    }
    final CtuluPopupMenu menu= new CtuluPopupMenu();
    for (int i= 0; i < (_context.length - 1); i += 2) {
      if (_context[i + 1] == null) {
        menu.addSeparator();
      } else {
        String title= null;
        if (_context[i] == null) {
          title= _context[i + 1];
        } else {
          title= _context[i];
        }
        if (title.startsWith("_CB")) {
          title= title.substring(3);
          boolean state= false;
          if (title.startsWith("_ON")) {
            state= true;
            title= title.substring(4);
          } else if (title.startsWith("_OFF")) {
            state= false;
            title= title.substring(5);
          }
          menu.addCheckBox(title, _context[i + 1], true, state);
        } else {
          menu.addMenuItem(title, _context[i + 1], true);
        }
      }
    }
    return menu;
  }
  /**
   * Affectation des commandes contextuelles.
   * @param _context [2*i]->titre, [2*i+1]->actionCommand
   */
  public void setCmdsContextuelles(final String[] _context) {
    if (context_ != _context) {
      final String[] vp= context_;
      context_= _context;
      bean_.firePropertyChange("cmdsContextuelles", vp, context_);
    }
  }
}
