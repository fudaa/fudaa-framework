/**
 *  @creation     2000-11-10
 *  @modification $Date: 2006-09-19 14:55:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Le modele du calque d'affichage de point.
 *
 * @version   $Id: ZModelePoint.java,v 1.12 2006-09-19 14:55:48 deniger Exp $
 * @author    Guillaume Desnoix
 */
public interface ZModelePoint extends ZModeleDonnees {
  /**
   * Affecte a _p les valeurs du point i.
   * @param _p le point modifie
   * @param _i index
   * @param _force affecter le point _p meme si le point i est filtre.
   *
   * @return true si affectation
   */
  boolean point(GrPoint _p, int _i, boolean _force);

  double getX(int _i);
  double getY(int _i);

  /**
   * @author Fred Deniger
   * @version $Id: ZModelePoint.java,v 1.12 2006-09-19 14:55:48 deniger Exp $
   */
  interface ThreeDim extends ZModelePoint{
    double getZ(int _i);
  }


}
