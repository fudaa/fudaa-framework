/*
 * $RCSfile: BCalqueTailleInteraction.java,v $
 * @creation 10/02/99
 * @modification $Date: 2006-09-19 14:55:47 $
 * @statut instable
 */
package org.fudaa.ebli.calque;
import com.memoire.fu.FuLog;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;
import org.fudaa.ebli.geometrie.*;
/**
 * Un calque de redimensionnement d'objets s�lectionnes en coordonnees
 * reelles.
 * <p>
 * L'objet doit �tre d'abord s�lectionn�. Le redimensionnement de l'objet
 * est effectu�e en cliquant sur une de ses poign�es de s�lection. Lors de
 * l'op�ration, l'objet est retaill� en conservant comme point fixe la poign�e
 * directement oppos�e � celle saisie.
 *
 * @version $Id: BCalqueTailleInteraction.java,v 1.10 2006-09-19 14:55:47 deniger Exp $
 * @author Bertrand Marchand
 */
public class BCalqueTailleInteraction
  extends BCalqueInteraction
  implements MouseMotionListener, MouseListener {
  /**
   * Objets s�lectionn�s, pr�ts � �tre redimensionn�s.
   */
  private VecteurGrContour selection_;
  /**
   * Point de souris saisi � l'�venement MouseEvent pr�c�dent.
   */
  private GrPoint ptPrec_;
  /**
   * Point fixe pour redimensionnement de l'objet.
   */
  private GrPoint ptFixe_;
  /**
   * Point cliqu� � l'�cran.
   */
  //private GrPoint ptClic_=null;
  /**
   * Vue calque contenant le calque.
   */
  private BVueCalque vc_;
  /**
   * Cr�ation du calque de redimensionnement.
   */
  public BCalqueTailleInteraction() {
    super();
    selection_= new VecteurGrContour();
    ptPrec_= null;
  }
  /**
   * D�finition des objets s�lectionn�s, pr�ts pour �tre redimensionn�s.
   * @param _s Liste de s�lection.
   */
  public void setSelection(final VecteurGrContour _s) {
    if (_s == null) {
      return;
    }
    if (_s == selection_) {
      return;
    }
    final VecteurGrContour vp= selection_;
    selection_= _s;
    firePropertyChange("selection", vp, selection_);
  }
  /**
   * Retourne la liste des objets s�lectionn�s.
   * @return La liste.
   */
  public VecteurGrContour getSelection() {
    return selection_;
  }
  // >>> Interface MouseListener -----------------------------------------------
  /**
   * Bouton souris press�.
   * On rep�re le coin de la boite englobante de l'objet le plus �loign� du
   * point cliqu�. C'est le point fixe.
   */
  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (selection_.nombre() == 0) {
      return;
    }
    final GrBoite btAll= new GrBoite();
    for (int i= 0; i < selection_.nombre(); i++) {
      final GrContour o= selection_.renvoie(i);
      if (o instanceof GrObjet) {
        btAll.ajuste(((GrObjet)o).boite());
      } else {
        FuLog.warning(o.getClass() + ": Not instance of GrObjet");
      }
    }
    ptPrec_= new GrPoint(_evt.getX(), _evt.getY(), 0);
    final GrPolygone pgBt= btAll.enPolygoneXY();
    double dstMax= Double.NEGATIVE_INFINITY;
    double dst;
    for (int i= 0; i < pgBt.nombre(); i++) {
      dst= pgBt.sommet(i).applique(getVersEcran()).distanceXY(ptPrec_);
      if (dstMax < dst) {
        dstMax= dst;
        ptFixe_= pgBt.sommet(i);
      }
    }
    ptPrec_.autoApplique(getVersReel());
    // Stockage de la vueCalque qui contient ce calque.
    if (vc_ == null) {
      vc_= (BVueCalque)SwingUtilities.getAncestorOfClass(BVueCalque.class, this);
    }
  }
  /**
   * Bouton relach�.
   * On r�affiche la vue calque.
   */
  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (_evt.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    if (selection_.nombre() == 0) {
      return;
    }
    ptPrec_= null;
    if (vc_ != null) {
      vc_.repaint();
    }
  }
  /** Non utilis�. */
  @Override
  public void mouseClicked(final MouseEvent _evt) {}
  /** Non utilis�. */
  @Override
  public void mouseEntered(final MouseEvent _evt) {}
  /** Non utilis�. */
  @Override
  public void mouseExited(final MouseEvent _evt) {}
  // <<< Interface MouseListener -----------------------------------------------
  // >>> Interface MouseMotionListener -----------------------------------------
  /**
   * Drag souris.
   * La taille de l'objet est modifi�e, la vue est rafraichie.
   */
  @Override
  public void mouseDragged(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (_evt.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    if (selection_.nombre() == 0) {
      return;
    }
    final GrPoint ptOrig= ptFixe_.applique(getVersReel());
    final GrPoint ptCur=
      new GrPoint(_evt.getX(), _evt.getY(), 0.).applique(getVersReel());
    final double ech= ptCur.distance(ptOrig) / ptPrec_.distance(ptOrig);
    for (int i= 0; i < selection_.nombre(); i++) {
      final GrContour o= selection_.renvoie(i);
      if (o instanceof GrObjet) {
        ((GrObjet)o).autoApplique(
          GrMorphisme.translation(-ptFixe_.x_, -ptFixe_.y_, -ptFixe_.z_));
        ((GrObjet)o).autoApplique(GrMorphisme.dilatation(ech));
        ((GrObjet)o).autoApplique(
          GrMorphisme.translation(ptFixe_.x_, ptFixe_.y_, ptFixe_.z_));
      } else {
        FuLog.warning(o.getClass() + ": Not instance of GrObjet");
      }
    }
    ptPrec_= ptCur;
    if (vc_ != null) {
      vc_.repaint();
    }
  }
  /** Non utilis�. */
  @Override
  public void mouseMoved(final MouseEvent _evt) {}
  // <<< Interface MouseMotionListener -----------------------------------------
}
