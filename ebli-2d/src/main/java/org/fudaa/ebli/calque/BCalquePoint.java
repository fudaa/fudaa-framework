/*
 * @file         BCalquePoint.java
 * @creation     1998-08-26
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import com.memoire.bu.BuFilters;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.image.FilteredImageSource;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.palette.BPaletteCouleur;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TracePoint;
/**
 * Un calque d'affichage de points.
 *
 * @version      $Id: BCalquePoint.java,v 1.11 2006-09-19 14:55:47 deniger Exp $
 * @author       Axel von Arnim
 */
public class BCalquePoint extends BCalqueAffichage {
  // donnees membres publiques
  // donnees membres privees
  protected VecteurGrPoint points_;
  protected double minZ_;protected double  maxZ_;
  // Constructeur
  public BCalquePoint() {
    super();
    points_= new VecteurGrPoint();
    // PB: initialisation (vaudrait mieux mettre le plus petit double poss
    //     pour minZ et le plus grand pour maxZ direct.
    minZ_= 1;
    maxZ_= -1;
    typePoint_= TracePoint.POINT;
    taillePoint_= 3;
    palette_= null;
  }
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    final int w= getIconWidth();
    final int h= getIconHeight();
    final TracePoint trace= new TracePoint();
    trace.setCouleur(fg);
    trace.setTypePoint(typePoint_);
    trace.setTaillePoint(taillePoint_);
    Icon icon= icon_;
    if (isAttenue()
      && (typePoint_ == TracePoint.ICONE)
      && (icon instanceof ImageIcon)) {
      final Image image=
        createImage(
          new FilteredImageSource(
            ((ImageIcon)icon).getImage().getSource(),
            BuFilters.BRIGHTER));
      icon= new ImageIcon(image);
    }
    trace.setIcon(icon);

    if ((w > 20) || (h > 20)) {
      final Shape old= _g.getClip();
      _g.setClip(_x + 2, _y + 2, 20, 20);
      trace.dessinePoint((Graphics2D)_g,_x + 2, _y + h - 2);
      _g.setClip(old);
    } else {
      trace.dessinePoint((Graphics2D)_g,_x + 13, _y + h - 7);
      trace.dessinePoint((Graphics2D)_g,_x + 4, _y + h - 4);
    }
  }
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    final TraceGeometrie trace= new TraceGeometrie(getVersEcran());
    trace.setTypePoint(typePoint_);
    trace.setTaillePoint(taillePoint_);
    Color fg;
    Icon icon= icon_;
    final boolean rapide= isRapide();
    final int np= points_.nombre();
    if (typePoint_ == TracePoint.ICONE) {
      if (isAttenue() && (icon instanceof ImageIcon)) {
        final Image image=
          createImage(
            new FilteredImageSource(
              ((ImageIcon)icon).getImage().getSource(),
              BuFilters.BRIGHTER));
        icon= new ImageIcon(image);
      }
      trace.setIcon(icon);
      for (int i= 0; i < np; i++){
        trace.dessinePoint((Graphics2D)_g,points_.renvoie(i), rapide);
      }
    } else if (palette_ == null) {
      fg= getForeground();
      if (isAttenue()) {
        fg= attenueCouleur(fg);
      }
      trace.setForeground(fg);
      for (int i= 0; i < np; i++){
        trace.dessinePoint((Graphics2D)_g,points_.renvoie(i), isRapide());
      }
    } else {
      for (int i= 0; i < np; i++) {
        final GrPoint p= points_.renvoie(i);
        fg= palette_.couleur((p.z_ - minZ_) / (maxZ_ - minZ_));
        if (isAttenue()) {
          fg= attenueCouleur(fg);
        }
        trace.setForeground(fg);
        trace.dessinePoint((Graphics2D)_g,p, isRapide());
      }
    }
  }
  /**
   * Reinitialise la liste de points.
   */
  public void reinitialise() {
    minZ_= 1;
    maxZ_= -1;
    points_= new VecteurGrPoint();
  }
  /**
   * Ajoute un point a la liste de points.
   */
  public void ajoute(final GrPoint _point) {
    // PB: IL NE FAUT PAS FAIRE DE TRAITEMENT DANS AJOUTE
    //     (ca peut etre court-circuite par ListeGrPoint)
    //     TROUVER UN AUTRE MOYEN !!
    points_.ajoute(_point);
    if (minZ_ > maxZ_) {
      minZ_= _point.z_;
      maxZ_= _point.z_;
    }
    if (_point.z_ < minZ_) {
      minZ_= _point.z_;
    }
    if (_point.z_ > maxZ_) {
      maxZ_= _point.z_;
    }
  }
  /**
   * Retire un point a la liste de points.
   */
  public void enleve(final GrPoint _point) {
    points_.enleve(_point);
    calculeMinMaxZ();
  }
  public VecteurGrPoint getPoints() {
    return points_;
  }
  
  public void setPoints(final VecteurGrPoint _lp) {
    if (_lp == points_) {
      return;
    }
    final VecteurGrPoint vp= points_;
    reinitialise();
    for (int i= 0; i < _lp.nombre(); i++){
      ajoute(_lp.renvoie(i));
    }
    firePropertyChange("points", vp, points_);
  }
  /**
   * Renvoi de la liste des elements selectionnables.
   */
  @Override
  public VecteurGrContour contours() {
    final VecteurGrContour res= new VecteurGrContour();
    res.tableau(points_.tableau());
    return res;
  }
  @Override
  public GrBoite getDomaine() {
    GrBoite r= null;
    if (isVisible()) {
      r= super.getDomaine();
      if (points_.nombre() > 0) {
        if (r == null) {
          r= new GrBoite();
        }
        for (int i= 0; i < points_.nombre(); i++){
          r.ajuste(points_.renvoie(i));
        }
      }
    }
    return r;
  }
  //
  // PROPRIETES INTERNES
  //
  // Propriete typePoint
  protected int typePoint_;
 
  /**
    * Affectation de la propriete <I>typePoint</I>.
    */
  public void setTypePoint(final int _typePoint) {
    if (typePoint_ != _typePoint) {
      final int vp= typePoint_;
      typePoint_= _typePoint;
      firePropertyChange("TypePoint", vp, typePoint_);
    }
  }
  protected int taillePoint_;
  
  /**
    * Affectation de la propriete <I>taillePoint</I>.
    */
  public void setTaillePoint(final int _taillePoint) {
    if (taillePoint_ != _taillePoint) {
      final int vp= taillePoint_;
      taillePoint_= _taillePoint;
      firePropertyChange("taillePoint", vp, taillePoint_);
    }
  }
  // Propriete icon
  protected Icon icon_;
  public Icon getIcon() {
    return icon_;
  }
  public void setIcon(final Icon _icon) {
    icon_= _icon;
  }
  protected BPaletteCouleur palette_;
  public BPaletteCouleur getPaletteCouleur() {
    return palette_;
  }
  public void setPaletteCouleur(final BPaletteCouleur _palette) {
    if (palette_ != _palette) {
      final BPaletteCouleur vp= palette_;
      palette_= _palette;
      firePropertyChange("paletteCouleur", vp, palette_);
      repaint();
    }
  }
  protected void calculeMinMaxZ() {
    GrPoint cur= null;
    if (points_.nombre() > 0) {
      maxZ_= points_.renvoie(0).z_;
      minZ_= maxZ_;
    }
    for (int i= 1; i < points_.nombre(); i++) {
      cur= points_.renvoie(i);
      if (cur.z_ > maxZ_) {
        maxZ_= cur.z_;
      }
      if (cur.z_ < minZ_) {
        minZ_= cur.z_;
      }
    }
  }
}
