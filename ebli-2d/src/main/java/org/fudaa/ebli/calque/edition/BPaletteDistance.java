/*
 * @creation     18 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Palette affichant les informations de distances.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class BPaletteDistance extends BuPanel implements ActionListener, BPalettePanelInterface, PropertyChangeListener {

  /** Label affichant la distance cumul�e. */
  protected BuLabel lblDistanceCumulee_=new BuLabel();
  /** Label affichant la distance en cours. */
  protected BuLabel lblDistanceEnCours_=new BuLabel();
  /** Le calque qui contient les informations de distance. */
  protected ZCalqueDistanceInteraction calqueDistance_;
  /** Le bouton 'continuer'. */
  protected BuButton btContinuer_=new BuButton(EbliLib.getS("Reprise"));
  protected EbliFormatterInterface formatter_;
  private ZEbliCalquesPanel calquesPanel_;
  
  public BPaletteDistance(ZCalqueDistanceInteraction _calqueDistance, EbliFormatterInterface _formatter, ZEbliCalquesPanel _calquesPanel){
    calquesPanel_=_calquesPanel;
    calqueDistance_=_calqueDistance;
    calqueDistance_.addDistanceChangeListener(this);
    calqueDistance_.addPropertyChangeListener("gele", this);
    formatter_=_formatter;
    // Construction du panel \\
    setLayout(new BorderLayout());
    // Titre
    BuLabel lblTitre=new BuLabel("<html><b>"+EbliLib.getS("Distance")+"</b></html>");
    lblTitre.setHorizontalAlignment(BuLabel.CENTER);
    add(lblTitre, BorderLayout.NORTH);
    // Contenu
    Container body=new Container();
    body.setLayout(new GridLayout(2, 2, 2, 2));
    body.add(new BuLabel(EbliLib.getS("En cours: ")));
    body.add(lblDistanceEnCours_);
    body.add(new BuLabel(EbliLib.getS("Cumul�e: ")));
    body.add(lblDistanceCumulee_);
    add(body, BorderLayout.CENTER);
    // Bouton reprise
    add(btContinuer_, BorderLayout.SOUTH);
    btContinuer_.addActionListener(this);
    // Affichage
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    setVisible(true);
    // Valeurs \\
    updateValues();
  }
  
  /** Met � jours les valeurs des labels de distance. */
  protected void updateValues(){
    lblDistanceEnCours_.setText(formatter_.getXYFormatter().format(calqueDistance_.getDistanceVariable()));
    lblDistanceCumulee_.setText(formatter_.getXYFormatter().format(calqueDistance_.getDistanceCumulee()));
  }
  
  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public boolean setPalettePanelTarget(Object _target) {
    return false;
  }
  
  @Override
  public void paletteDeactivated() {}
  
  @Override
  public void doAfterDisplay() {}

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if(evt.getPropertyName().equals("gele"))
      btContinuer_.setEnabled((Boolean) evt.getNewValue()); 
    else if(evt.getPropertyName().equals("distance"))
      updateValues();
  }

  /* (non-Javadoc)
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    calquesPanel_.setCalqueInteractionActif(calqueDistance_);
  }

}
