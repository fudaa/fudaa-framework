/*
 * @file         BCalqueRepereInteraction.java
 * @creation     1998-09-02
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.repere.RepereEventListener;

/**
 * Un calque d'interaction pour faire des zoom fenetre.
 * 
 * @version $Revision: 1.11 $ $Date: 2007-05-04 13:49:42 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BCalqueRepereInteraction extends BCalqueInteraction implements MouseListener, MouseMotionListener {
  // Modes de zoom
  /**
   * Mode de zoom automatique. Le zoom avant se fait avec le bouton de la souris gauche, le zoom arri�re avec le bouton
   * droit.
   */
  public final static int AUTOMATIQUE = 0;
  /**
   * Mode de zoom avant. Le zoom se fait indiff�rement avec les 2 boutons.
   */
  public final static int AVANT = 1;
  /**
   * Mode de zoom arri�re. Le zoom se fait indiff�rement avec les 2 boutons.
   */
  public final static int ARRIERE = 2;
  private Point pointDeb_, pointFinPrec_;
  private List repereEventListeners_;
  private final BVueCalque vc_;
  private int mode_; // Mode de zoom.
  private double coefZoomClick_ = 1.5; // Coefficient de zoom (AVANT/ARRIERE)

  // pour un zoom par clic.
  public BCalqueRepereInteraction(final BVueCalque _vc) {
    super();
    vc_ = _vc;
    repereEventListeners_ = new ArrayList();
    addMouseListener(this);
    addMouseMotionListener(this);
    mode_ = AUTOMATIQUE;
  }

  // Icon
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(Color.black);
    _g.drawRect(_x + 10, _y + 6, w - 15, h - 13);
    _g.setColor(Color.lightGray);
    _g.drawRect(_x + 2, _y + 2, w - 4, h - 4);
  }

  /**
   * Affectation du mode de Zoom.
   * 
   * @param _mode (<I>AVANT, ARRIERE, AUTOMATIQUE</I>)
   */
  public void setModeZoom(final int _mode) {
    mode_ = _mode;
  }

  /**
   * Retour du mode de zoom courant.
   * 
   * @return (<I>AVANT, ARRIERE, AUTOMATIQUE</I>). <I>AUTOMATIQUE</I> par d�faut.
   */
  public int getModeZoom() {
    return mode_;
  }

  /**
   * Affectation du coefficient de zoom avant/arriere.
   * <p>
   * Ce coefficient est utilis� dans le zoom par simple clic.
   * 
   * @param _coef Le coefficient multiplicateur du zoom autre que 0. Si 0, il est d�fini par d�faut.
   */
  public void setCoefficientZoom(final double _coef) {
    if (_coef == 0) {
      coefZoomClick_ = 1.5;
    } else {
      coefZoomClick_ = _coef;
    }
  }

  /**
   * Retour du coefficient de zoom avant/arriere.
   * 
   * @return La valeur du coefficient. <I>1.5</I> par d�faut.
   */
  public double getCoefficientZoom() {
    return coefZoomClick_;
  }

  // EVENEMENTS
  @Override
  public void mouseClicked(final MouseEvent _evt) {}

  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  @Override
  public void mouseExited(final MouseEvent _evt) {}

  @Override
  public void mousePressed(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    pointDeb_ = _evt.getPoint();
  }

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (pointDeb_ == null) {
      return;
    }
    final Point pointFin = _evt.getPoint();
    GrPoint pointO;
    GrPoint pointE;
    if (pointDeb_.distance(pointFin) > 7) {
      pointE = new GrPoint(Math.min(pointDeb_.x, pointFin.x), Math.max(pointDeb_.y, pointFin.y), 0.);
      pointO = new GrPoint(Math.max(pointDeb_.x, pointFin.x), Math.min(pointDeb_.y, pointFin.y), 0.);
    }
    // Zoom suivant le coefZoomClick_ et recentrage sur le point cliqu�.
    else {
      double coef;
      if (mode_ == AVANT) {
        coef = 1 / coefZoomClick_;
      } else if (mode_ == ARRIERE) {
        coef = coefZoomClick_;
      } else if ((_evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
        coef = 1 / coefZoomClick_;
      } else {
        coef = coefZoomClick_;
      }
      final int l = getWidth();
      final int h = getHeight();
      pointO = new GrPoint();
      pointE = new GrPoint();
      pointO.x_ = pointFin.x - (l * coef / 2);
      pointO.y_ = pointFin.y - (h * coef / 2);
      pointE.x_ = pointFin.x + (l * coef / 2);
      pointE.y_ = pointFin.y + (h * coef / 2);
    }
    pointE = pointE.applique(getVersReel());
    pointO = pointO.applique(getVersReel());
    final GrBoite boite = new GrBoite();
    boite.ajuste(pointE);
    boite.ajuste(pointO);
    vc_.changeRepere(this, boite);
    // RepereEvent evt=new RepereEvent(this, false);
    // evt.ajouteTransformation(RepereEvent.ZOOM_X, zoom, RepereEvent.ABSOLU);
    // evt.ajouteTransformation(RepereEvent.ZOOM_Y, zoom, RepereEvent.ABSOLU);
    // evt.ajouteTransformation(RepereEvent.TRANS_X, pointE.x/zoom, RepereEvent.ABSOLU);
    // evt.ajouteTransformation(RepereEvent.TRANS_Y, pointE.y/zoom, RepereEvent.ABSOLU);
    // fireRepereEvent(evt);
    pointFinPrec_ = null;
    pointDeb_ = null;
  }

  @Override
  public void mouseDragged(final MouseEvent _evt) {
    if (isGele()) {
      return;
    }
    if (pointDeb_ == null) {
      return;
    }
    final Point pointFin = _evt.getPoint();
    final Graphics g = getGraphics();
    g.setXORMode(getBackground());
    g.setColor(Color.black);
    if (pointFinPrec_ != null) {
      g.drawLine(pointDeb_.x, pointDeb_.y, pointFinPrec_.x, pointDeb_.y);
      g.drawLine(pointFinPrec_.x, pointDeb_.y, pointFinPrec_.x, pointFinPrec_.y);
      g.drawLine(pointFinPrec_.x, pointFinPrec_.y, pointDeb_.x, pointFinPrec_.y);
      g.drawLine(pointDeb_.x, pointFinPrec_.y, pointDeb_.x, pointDeb_.y);
    }
    g.drawLine(pointDeb_.x, pointDeb_.y, pointFin.x, pointDeb_.y);
    g.drawLine(pointFin.x, pointDeb_.y, pointFin.x, pointFin.y);
    g.drawLine(pointFin.x, pointFin.y, pointDeb_.x, pointFin.y);
    g.drawLine(pointDeb_.x, pointFin.y, pointDeb_.x, pointDeb_.y);
    pointFinPrec_ = pointFin;
  }

  @Override
  public void mouseMoved(final MouseEvent _evt) {}

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  // fireRepereEvent
  protected synchronized void fireRepereEvent(final RepereEvent _evt) {
    for (int i = repereEventListeners_.size() - 1; i >= 0; i--) {
      ((RepereEventListener) repereEventListeners_.get(i)).repereModifie(_evt);
    }
  }
}
