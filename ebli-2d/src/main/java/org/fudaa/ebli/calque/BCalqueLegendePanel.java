/**
 * @creation 6 ao�t 2004
 * @modification $Date: 2007-06-28 09:26:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import org.fudaa.ebli.commun.EbliPreferences;

/**
 * @author Fred Deniger
 * @version $Id: BCalqueLegendePanel.java,v 1.14 2007-06-28 09:26:47 deniger Exp $
 */
public class BCalqueLegendePanel extends BuPanel {

  private boolean askVisible_ = true;

  BCalqueAffichage aff_;

  /**
   * @return la couleur du bord sauvee
   */
  public static Color getDefaultBordColor() {
    return Color.BLACK;
  }

  /**
   * @return la police sauvee
   */
  public static Font getDefaultFont() {
    return new Font("Dialog", Font.PLAIN, 10);
  }

  public static boolean getSaveSameHeight() {
    return EbliPreferences.EBLI.getBooleanProperty("legend.sameHeight", true);
  }

  public static void setSaveSameHeight(final boolean _b) {
    EbliPreferences.EBLI.putBooleanProperty("legend.sameHeight", _b);
  }

  private final static Color DEFAULT_BG = new Color(255, 255, 224);

  /**
   * @return la couleur de fond sauvee
   */
  public static Color getDefaultBgColor() {
    return DEFAULT_BG;
  }

  public static int getDefaultBordEpaisseur() {
    return 1;
  }

  private final JLabel titleComp_;

  public BCalqueLegendePanel(final BCalqueAffichage _aff, final String _title) {
    super();
    aff_ = _aff;
    setLayout(new BuBorderLayout());
    titleComp_ = new JLabel();
    titleComp_.putClientProperty("legend.type", "TITLE");
    titleComp_.setText(_title);
    titleComp_.setHorizontalAlignment(SwingConstants.CENTER);
    add(titleComp_, BuBorderLayout.NORTH);
    updateState();
  }
  
  public String getTitle(){
    return titleComp_.getText();
  }

  public BCalqueAffichage getCalque() {
    return aff_;
  }

  protected final void updateState() {
    BCalqueAffichageLegendProperties l = aff_.getLegendProperties();
    super.setBackground(l.getBackground());
    updateFont(l.getFont());
    updateBorder();
  }

  private void updateBorder() {

    int bordEpaisseur = getBordEpaisseur();
    if (bordEpaisseur <= 0) {
      setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    } else {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(getBordColor(), bordEpaisseur),
          BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    }
    revalidate();
  }

  final Color getBordColor() {
    return aff_.getLegendProperties().getBorderColor();
  }

  final int getBordEpaisseur() {
    return aff_.getLegendProperties().getBord();
  }

  final void setBordColor(final Color _bordColor) {
    Color old = getBordColor();
    aff_.getLegendProperties().setBorderColor(_bordColor);
    updateBorder();
    aff_.firePropertyChange("legendPanel", old, _bordColor);
  }

  final void setBordEpaisseur(final int _bordEpaisseur) {
    int old = getBordEpaisseur();
    aff_.getLegendProperties().setBord(_bordEpaisseur);
    updateBorder();
    aff_.firePropertyChange("legendPanel", old, _bordEpaisseur);
  }

  /**
   * Propriete interne permettant de retenir le choix de l'utilisateur.
   * 
   * @return true si le panneau est mis a visible
   */
  public boolean isLegendVisible() {
    return askVisible_;
  }

  /**
   * @return true si ce panneau et le calque associe sont visibles.
   * @see java.awt.Component#isVisible()
   */
  @Override
  public boolean isVisible() {
    return askVisible_ && aff_.isVisible();
  }

  public void updateTitreGlob(final String _titre) {
    titleComp_.setText(_titre);
    revalidate();
  }

  public JComponent getMainComponent() {
    if (getComponentCount() > 1) { return (JComponent) getComponent(1); }
    return null;
  }

  @Override
  public final void setFont(final Font _f) {
    // aie: cette m�thode non final dans le jdk est appele par le constructeur de JPanel et crac boum
    if (aff_ == null) return;
    Font old = aff_.getLegendProperties().getFont();
    updateFont(_f);
    aff_.getLegendProperties().setFont(_f);
    aff_.firePropertyChange("legendPanel", old, _f);
  }

  @Override
  public void setBackground(Color _bg) {
    // aie : m�thode non final appele par constructeur
    if (aff_ == null) return;
    Color old = getBackground();
    super.setBackground(_bg);
    aff_.getLegendProperties().setBackground(_bg);
    aff_.firePropertyChange("legendPanel", old, _bg);
  }

  /**
   * On oblig� de red�fnir pour s'assurer que les composant aurant la meme police.
   */
  @Override
  protected void addImpl(Component _comp, Object _constraints, int _index) {
    _comp.setFont(getFont());
    super.addImpl(_comp, _constraints, _index);
  }

  private void updateFont(final Font _f) {
    super.setFont(_f);
    for (int i = getComponentCount() - 1; i > 0; i--) {
      getComponent(i).setFont(_f);
      // le composant
      if (i == 1) {
        final JComponent c = (JComponent) getComponent(1);
        for (int j = c.getComponentCount() - 1; j >= 0; j--) {
          c.getComponent(j).setFont(_f);
        }
      }
    }
    if (titleComp_ != null) {
      titleComp_.setFont(BuLib.deriveFont(_f, Font.BOLD, 2));
    }
  }

  @Override
  public void setVisible(final boolean _b) {
    askVisible_ = _b;
    super.setVisible(_b);
  }

}