/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2006-11-14 09:06:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

/**
 * Une interface pour recevoir les ect d'une Palette d'�dition.
 *
 * @author Fred Deniger
 * @version $Id: BPaletteEditionClientInterface.java,v 1.1 2006-11-14 09:06:22 deniger Exp $
 */
public interface BPaletteEditionClientInterface {

  /**
   * @param _com la commande envoyee par la palette
   * @param _emitter la palette source de l'action
   */
  void doAction(String _com, BPaletteEdition _emitter);

}
