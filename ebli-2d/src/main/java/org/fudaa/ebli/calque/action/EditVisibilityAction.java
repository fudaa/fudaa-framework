/*
 * @creation     23 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.action;

import com.memoire.bu.BuResource;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.edition.BPaletteEditVisibility;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Action permettant d'afficher une palette de modification de l'attribut
 * 'visibilite' des géométries.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class EditVisibilityAction extends EbliActionPaletteAbstract {

  protected BPaletteEditVisibility palette_;
  protected final BArbreCalqueModel treeModel_;
  protected final ZScene scene_;
  protected CtuluCommandManager cmd_;
  
  public EditVisibilityAction(BArbreCalqueModel _treeModel, ZScene _scene, CtuluCommandManager _cmd) {
    super(EbliLib.getS("Edition visibilité"), BuResource.BU.getIcon("visibilite"), "VISIBILITY_EDITION", true);
    treeModel_=_treeModel;
    scene_=_scene;
    cmd_=_cmd;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.fudaa.ebli.commun.EbliActionPaletteAbstract#buildContentPane()
   */
  @Override
  public JComponent buildContentPane() {
    if(palette_==null)
      palette_=new BPaletteEditVisibility(treeModel_, scene_, cmd_);
    return palette_.getComponent();
  }
  
  @Override
  public void changeAction() {
    if (isSelected()) {
      if(palette_!=null)
        palette_.doShow();
      showWindow();
    } else {
      if(palette_!=null)
        palette_.doHide();
      hideWindow();
    }
  }
  
}
