/*
 *  @creation     14 avr. 2005
 *  @modification $Date: 2006-07-13 13:35:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuScrollPane;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ctulu.table.CtuluTableColumnHeader;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;

/**
 * @author Fred Deniger
 * @version $Id: FSigSingleGeomVisuPanel.java,v 1.2 2005/10/03 10:36:25 deniger Exp $
 */
public class EbliSingleGeomVisuPanel extends CtuluDialogPanel {

  public EbliSingleGeomVisuPanel(final GISZoneCollection _zone, final int _idx, final int[] _idxVertex,
      final EbliCoordinateDefinition[] _defs) {
    final int idx;
    // TObjectIntHashMap intAttr_;
    final EbliAtomicCoordinatesTableModel model;
    CtuluTable table;
    idx = _idx;
    final GISZoneCollection zone = _zone;
    final Geometry g = zone.getGeometry(idx);
    // le nombre de points contenu: si un on se doute que c'est un point !
    final int nbPt = g.getNumPoints();
    final boolean isPoint = nbPt == 1;
    // on construit le panneau
    if (isPoint) {
      setLayout(new BuGridLayout(2, 5, 5));
      final Coordinate coord = g.getCoordinate();
      final BuLabel x = new BuLabel();
      x.setText(_defs[0].getFormatter().getXYFormatter().format(coord.x));
      final BuLabel y = new BuLabel();
      y.setText(_defs[1].getFormatter().getXYFormatter().format(coord.y));
      addLabel(_defs[0].getName()+":");
      add(x);
      addLabel(_defs[1].getName()+":");
      add(y);

    } else {
      setLayout(new BuBorderLayout());
      table = new CtuluTable();
      model = new EbliAtomicCoordinatesTableModel.Line(_defs, zone, idx, _idxVertex, true, null) {

        @Override
        public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
          return false;
        }
      };
      table.setModel(model);
      table.setTableHeader(new CtuluTableColumnHeader(table.getColumnModel()));
      if (model.getColumnCount() > 4) {
        for (int i = model.getColumnCount() - 1; i >= 4; i--) {
          table.hideColumn(table.getColumnModel().getColumn(i));
        }
      }

      // table.hideColumn(_idxColumn)
      add(new BuScrollPane(table), BuBorderLayout.CENTER);
    }
  }
}
