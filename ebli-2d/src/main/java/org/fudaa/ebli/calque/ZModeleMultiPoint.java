/*
 *  @creation     31 mars 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;



/**
 * Une interface mod�le manipul�e par un calque ZCalqueMultiPoint.
 * @see org.fudaa.ebli.calque.ZCalqueMultiPoint
 * @author Bertrand Marchand
 * @version $Id: ZModeleMultiPoint.java,v 1.1.2.1 2008-03-26 16:46:43 bmarchan Exp $
 */
public interface ZModeleMultiPoint extends ZModeleGeometry {
}
