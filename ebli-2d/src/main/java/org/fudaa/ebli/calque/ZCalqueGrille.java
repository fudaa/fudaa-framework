/*
 * @file BCalqueGrille.java
 *
 * @creation 1998-09-03
 *
 * @modification $Date: 2008-02-20 10:14:40 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluRatio;
import org.fudaa.ctulu.iterator.NumberIterator;
import org.fudaa.ctulu.iterator.TickIterator;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Un calque d'affichage d'une grille.
 *
 * @version $Revision: 1.11.6.1 $ $Date: 2008-02-20 10:14:40 $ by $Author: bmarchan $
 * @author Guillaume Desnoix
 */
@SuppressWarnings("serial")
public class ZCalqueGrille extends ZCalqueAffichageDonnees {

  /**
   * caratere visible de l'axe x
   */
  public static final String AXIS_X_VISIBLE = "axis.x.visible";
  /**
   * caratere visible de l'axe y
   */
  public static final String AXIS_Y_VISIBLE = "axis.y.visible";
  /**
   * propriete pour la taille des graduations sur l'axe x
   */
  public static final String GRADUATION_X_LENGTH = "graduation.x.length";
  /**
   * propriete pour le calcul automatique le nombre de graduation sur l'axe x
   */
  public static final String GRADUATION_X_MODE_AUTO = "graduation.x.auto";
  /**
   * propriete pour le nombre de graduation sur l'axe x
   */
  public static final String GRADUATION_X_MODE_LENGTH = "graduation.x.mode.length";
  /**
   * propriete pour le nombre de graduation sur l'axe x
   */
  public static final String GRADUATION_X_NB = "graduation.x.nb";
  /**
   * propriete pour la taille des graduations sur l'axe y
   */
  public static final String GRADUATION_Y_LENGTH = "graduation.y.length";
  /**
   * propriete pour le nombre de graduation sur l'axe y
   */
  public static final String GRADUATION_Y_MODE_AUTO = "graduation.y.auto";
  /**
   * propriete pour le nombre de graduation sur l'axe y
   */
  public static final String GRADUATION_Y_MODE_LENGTH = "graduation.y.mode.length";
  /**
   * propriete pour le nombre de graduation sur l'axe y
   */
  public static final String GRADUATION_Y_NB = "graduation.y.nb";

  /**
   * caratere visible de la grille
   */
  public static final String GRID_VISIBLE = "grille.visible";
  /**
   * propriete pour le nombre de graduation mineure sur l'axe x
   */
  public static final String MINOR_GRADUATION_X_NB = "minor.graduation.x.nb";
  /**
   * visibilite des graduations mineures de l'axe x
   */
  public static final String MINOR_GRADUATION_X_VISIBLE = "minor.graduation.x.visible";
  /**
   * propriete pour le nombre de graduation mineure sur l'axe y
   */
  public static final String MINOR_GRADUATION_Y_NB = "minor.graduation.y.nb";
  /**
   * visibilite des graduations mineures de l'axe y
   */
  public static final String MINOR_GRADUATION_Y_VISIBLE = "minor.graduation.y.visible";
  private TraceLigneModel traceSousGraduations_;
  private boolean xMinorDraw_ = false;
  boolean yMinorDraw_ = false;
  private boolean yModeLongueur_ = false;
  /**
   * Indique si on utilise par taille de pas ou par nombre de graduations
   */
  boolean xModeLongueur_ = false;
  boolean gridDraw_;
  final TraceLigne tl_ = new TraceLigne();

  final GrPoint tmp_ = new GrPoint();
  final BVueCalque vue_;

  boolean xDraw_ = true;
  boolean yDraw_ = true;
  /**
   * Valeur du pas que devront suivrent tous les traits de la grilles si on en mode taille pas.
   */
  double xLength_;
  double yLength_;

  // -- sous graduations --//
  int xMinorGraduationNb_ = 3;
  int yMinorGraduationNb_ = 3;

  /**
   * Indique la facon de calculer les graduations. pour l'axe des x.
   */
  boolean xModeAuto_ = true;
  /**
   * Indique la facon de calculer les graduations. pour l'axe des y.
   */
  boolean yModeAuto_ = true;

  int xNbGraduation_ = 10;
  int yNbGraduation_ = 10;

  public ZCalqueGrille(final BVueCalque _vue) {
    super();
    vue_ = _vue;
    setDestructible(false);
    setName("cqGrille");
    setTitle(EbliLib.getS("Grille"));
    setForeground(Color.BLACK);
    traceSousGraduations_ = new TraceLigneModel(TraceLigne.TIRETE, 0.5F, Color.GRAY);
    ligneModel_ = new TraceLigneModel(TraceLigne.LISSE, 1F, Color.BLACK);
  }

  private void computeXValues(final GrBoite _zv, final Graphics2D _g, final TDoubleArrayList _xValue,
          final TIntArrayList _xStrWidth, final List _xString) {

    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = font == null ? null : _g.getFontMetrics(font);
    TickIterator it = createXTickIterator(_zv);
    if (it == null) {
      return;
    }
    // _it=
    while (it.hasNext()) {
      final double x = it.currentValue();
      _xValue.add(x);

      if (font == null || !it.isMajorTick()) {
        _xStrWidth.add(0);
        _xString.add(CtuluLibString.EMPTY_STRING);
      } else {
        final String currentLabel = it.currentLabel();
        _xStrWidth.add(fm.stringWidth(currentLabel));
        _xString.add(currentLabel);
      }
      it.next();
    }
  }

  private double computeYValues(final Graphics2D _g, final GrBoite _zv, final TDoubleArrayList _yValue,
          final TIntArrayList _yStrWidth, final List _yString) {
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = font == null ? null : _g.getFontMetrics(font);
    double yMax = 0;
    TickIterator it = createYIterator(_zv);

    if (it == null) {
      return 0;
    }

    while (it.hasNext()) {

      final double x = it.currentValue();
      _yValue.add(x);
      if (font == null || !it.isMajorTick()) {
        _yStrWidth.add(0);
        _yString.add(CtuluLibString.EMPTY_STRING);
      } else {
        final String currentLabel = it.currentLabel();
        final double stringWidth = fm == null ? 0 : fm.getStringBounds(currentLabel, _g).getWidth();
        if (stringWidth > yMax) {
          yMax = stringWidth;
        }
        _yStrWidth.add((int) stringWidth);
        _yString.add(currentLabel);
      }
      it.next();
    }
    return yMax;
  }

  private TickIterator createXTickIterator(final GrBoite _zv) {
    NumberIterator it = new NumberIterator();
    it.setMaxFractionDigits(2);
    if (!xModeAuto_) {
      if (xModeLongueur_) {
        it.initExactFromDist(_zv.getMinX(), _zv.getMaxX(), this.xLength_, xMinorGraduationNb_ + 1);
      } else {
        it.initExact(_zv.getMinX(), _zv.getMaxX(), getNbXGraduations(), xMinorGraduationNb_ + 1);
      }
    } else {
      it.init(_zv.getMinX(), _zv.getMaxX(), getNbXGraduations());
    }
    return it;
  }

  private TickIterator createYIterator(final GrBoite _zv) {
    NumberIterator it = new NumberIterator();
    it.setMaxFractionDigits(2);
    // -- mode manuel --//
    if (!yModeAuto_) {
      if (isYModeLongueur()) {
        it.initExactFromDist(_zv.getMinY(), _zv.getMaxY(), this.yLength_, yMinorGraduationNb_ + 1);
      } else {
        it.initExact(_zv.getMinY(), _zv.getMaxY(), getNbYGraduations(), yMinorGraduationNb_ + 1);
      }
    } else {
      it.init(_zv.getMinY(), _zv.getMaxY(), getNbYGraduations());
    }
    return it;
  }

  private void drawXAxe(final Graphics2D _g, final GrBoite _zv, final TDoubleArrayList _xValue,
          final GrMorphisme _versEcran, final int _absyAxe, final int _ordxAxe, final double _maxXForAxe) {
    int minXDraw = _absyAxe;
    int maxXDraw = (int) _maxXForAxe;

    tl_.setCouleur(getDrawColor());
    tl_.setTypeTrait(TraceLigne.LISSE);
    tl_.dessineTrait(_g, minXDraw, _ordxAxe, maxXDraw, _ordxAxe);
  }

  private void drawYaxe(final Graphics2D _g, final GrBoite _zv, final TDoubleArrayList _xValue,
          final TDoubleArrayList _yValue, final GrMorphisme _versEcran, final int _absyAxe, final int _ordxAxe,
          final double _minYForAxe) {
    int minYDraw = _ordxAxe;
    int maxYDraw = (int) _minYForAxe;

    tl_.setCouleur(getDrawColor());
    tl_.setTypeTrait(TraceLigne.LISSE);
    tl_.dessineTrait(_g, _absyAxe, minYDraw, _absyAxe, maxYDraw);
  }

  private void fillYZone(final Graphics2D _g, final GrBoite _targetBoite, final int _absyAxe, final GrBoite _ecranBoite) {
    if (_ecranBoite.getMinX() < _absyAxe) {
      _g.setColor(vue_.getBackground());
      _g.fillRect(0, 0, _absyAxe, (int) _targetBoite.getMaxY());
    }
  }

  private Color getDrawColor() {
    Color fg = ligneModel_.getCouleur();
    if (isAttenue()) {
      fg = attenueCouleur(fg);
    }
    return fg;
  }

  private void grilleChanged() {
    repaint();
    firePropertyChange("grille", true, false);
  }

  private void grilleChanged(String _prop, boolean newValue) {
    repaint();
    firePropertyChange(_prop, !newValue, newValue);
  }

  private void grilleChanged(String _prop, double oldValue, double newValue) {
    repaint();
    firePropertyChange(_prop, oldValue, newValue);
  }

  private void grilleChanged(String _prop, int oldValue, int newValue) {
    repaint();
    firePropertyChange(_prop, oldValue, newValue);
  }

  void fillXZone(final Graphics2D _g, final GrBoite _targetBoite, final int _absyAxe, final int _ordxAxe,
          final GrBoite _ecranBoite) {
    if (_ecranBoite.getMaxY() > _ordxAxe) {
      _g.setColor(vue_.getBackground());
      _g.fillRect(_absyAxe, _ordxAxe, (int) _targetBoite.getMaxX() - _absyAxe, (int) _targetBoite.getMaxY() - _ordxAxe);
    }
  }

  protected void drawYGraduations(final Graphics2D _g, final TDoubleArrayList _yValue, final TIntArrayList _yStrWidth,
          final List _yString, final GrMorphisme _versEcran, final int _marge, final int _absyAxe, final int _ordxAxe,
          final GrBoite _ecranBoite, final double _maxXForAxe) {
    final int nb = _yString.size();
    final int tiret = getTiretLength();
    final Font font = getFont();
    final FontMetrics fm = font == null ? null : _g.getFontMetrics(font);
    final int ascent = fm == null ? 0 : (fm.getAscent() - fm.getDescent());
    int idxStr = -1;
    final Color fg = getDrawColor();
    tl_.getModel().updateData(ligneModel_);
    for (int i = 0; i < nb; i++) {
      // B.M. : Remise � 0 des coordonn�es non utilis�es, pour eviter les NaN et calculs erron�s
      tmp_.x_ = 0;
      tmp_.y_ = _yValue.getQuick(i);
      tmp_.z_ = 0;
      tmp_.autoApplique(_versEcran);

      if (tmp_.y_ > _ordxAxe) {
        continue;
      }
      if (tmp_.y_ < _ecranBoite.getMinY()) {
        break;
      }
      final int w = _yStrWidth.getQuick(i);
      boolean drawGrad = w > 0 || isYMinorDraw();
      if (w <= 0) {
        tl_.getModel().updateData(traceSousGraduations_);
      } else {
        tl_.getModel().updateData(ligneModel_);
      }
      if (drawGrad) {
        int oldType = tl_.getTypeTrait();
        tl_.setTypeTrait(TraceLigne.LISSE);
        tl_.dessineTrait(_g, _absyAxe, tmp_.y_, _absyAxe - (w > 0 ? tiret : tiret - 2), tmp_.y_);
        tl_.setTypeTrait(oldType);
      }
      if (w > 0 && (tmp_.y_ < idxStr || idxStr < 0)) {
        _g.setColor(fg);
        _g.drawString((String) _yString.get(i), (_absyAxe - w - tiret - _marge), (int) (tmp_.y_ + ascent / 2D));
        idxStr = (int) (tmp_.y_ - ascent / 2D - 3D);
      }

      if (gridDraw_ && drawGrad) {
        tl_.dessineTrait(_g, _absyAxe, tmp_.y_, _maxXForAxe, tmp_.y_);
      }
    }
  }

  private void drawXGraduations(final Graphics2D _g, final GrBoite _targetBoite, final TDoubleArrayList _xValue,
          final TIntArrayList _xStrWidth, final List _xString, final int _absyAxe, final int _ordxAxe,
          final GrBoite _ecranBoite, final double _minYForAxe, final int _ordXString) {

    int idxStr = -1;
    final GrMorphisme versEcran = getVersEcran();
    final int tiret = getTiretLength();

    final int nb = _xString.size();
    final Color fg = getDrawColor();
    for (int i = 0; i < nb; i++) {
      // B.M. : Remise � 0 des coordonn�es non utilis�es, pour eviter les NaN et calculs erron�s
      tmp_.x_ = _xValue.getQuick(i);
      tmp_.y_ = 0;
      tmp_.z_ = 0;
      tmp_.autoApplique(versEcran);

      if (tmp_.x_ < _absyAxe) {
        continue;
      }
      if (tmp_.x_ > _ecranBoite.getMaxX()) {
        break;
      }

      final int w = _xStrWidth.getQuick(i);

      if (w <= 0) {
        tl_.getModel().updateData(traceSousGraduations_);
      } else {
        tl_.getModel().updateData(ligneModel_);
      }
      boolean drawGrad = w > 0 || isXMinorDraw();
      if (drawGrad) {
        int oldType = tl_.getTypeTrait();
        tl_.setTypeTrait(TraceLigne.LISSE);
        tl_.dessineTrait(_g, tmp_.x_, _ordxAxe, tmp_.x_, _ordxAxe + (w > 0 ? tiret : tiret - 2));
        tl_.setTypeTrait(oldType);
      }
      if (w > 0) {
        final int start = (int) (tmp_.x_ - w / 2D);
        if (start > idxStr + 1 && start + w < _targetBoite.getMaxX()) {
          _g.setColor(fg);
          _g.drawString((String) _xString.get(i), start, _ordXString);
          idxStr = start + w;
        }
      }
      if (gridDraw_ && drawGrad) {
        tl_.dessineTrait(_g, tmp_.x_, _ordxAxe, tmp_.x_, _minYForAxe);
      }

    }
  }

  /**
   * @return the drawSousGrilleY_
   */
  protected boolean isYMinorDraw() {
    return yMinorDraw_;
  }

  public void changeZoom(double minx, double miny, double maxx, double maxy) {
    vue_.changeViewBoite(this, new GrBoite(new GrPoint(minx, miny, 0), new GrPoint(maxx, maxy, 0)), false);
  }

  /**
   * Accesseur de la propriete <I>boite</I>. Elle donne la position, la taille, le domaine de la grille.
   */
  @Override
  public GrBoite getDomaine() {
    return null;
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    return getDomaine();
  }

  /**
   *
   */
  @Override
  public TraceLigneModel getLineModel(int _idx) {
    return _idx == 0 ? super.ligneModel_ : traceSousGraduations_;
  }

  @Override
  public int getNbSet() {
    return 2;
  }

  public int getNbXGraduations() {
    return xNbGraduation_;
  }

  public int getNbYGraduations() {
    return yNbGraduation_;
  }

  @Override
  public Object getProperty(String _name) {
    if (GRID_VISIBLE.equals(_name)) {
      return Boolean.valueOf(gridDraw_);
    }
    if (AXIS_X_VISIBLE.equals(_name)) {
      return Boolean.valueOf(xDraw_);
    }
    if (AXIS_Y_VISIBLE.equals(_name)) {
      return Boolean.valueOf(yDraw_);
    }
    if (GRADUATION_X_MODE_AUTO.equals(_name)) {
      return Boolean.valueOf(xModeAuto_);
    }
    if (GRADUATION_Y_MODE_AUTO.equals(_name)) {
      return Boolean.valueOf(yModeAuto_);
    }
    if (MINOR_GRADUATION_Y_VISIBLE.equals(_name)) {
      return Boolean.valueOf(yMinorDraw_);
    }
    if (MINOR_GRADUATION_X_VISIBLE.equals(_name)) {
      return Boolean.valueOf(xMinorDraw_);
    }
    if (GRADUATION_X_MODE_LENGTH.equals(_name)) {
      return Boolean.valueOf(xModeLongueur_);
    }
    if (GRADUATION_Y_MODE_LENGTH.equals(_name)) {
      return Boolean.valueOf(yModeLongueur_);
    }
    if (GRADUATION_X_LENGTH.equals(_name)) {
      return Double.valueOf(xLength_);
    }
    if (GRADUATION_Y_LENGTH.equals(_name)) {
      return Double.valueOf(yLength_);
    }
    if (GRADUATION_X_NB.equals(_name)) {
      return Integer.valueOf(xNbGraduation_);
    }
    if (GRADUATION_Y_NB.equals(_name)) {
      return Integer.valueOf(yNbGraduation_);
    }
    if (MINOR_GRADUATION_X_NB.equals(_name)) {
      return Integer.valueOf(xMinorGraduationNb_);
    }
    if (MINOR_GRADUATION_Y_NB.equals(_name)) {
      return Integer.valueOf(yMinorGraduationNb_);
    }
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    return null;
  }

  /**
   *
   */
  @Override
  public String getSetTitle(int _idx) {
    return EbliLib.getS(_idx == 0 ? "Graduations principales" : "Graduations mineures");
  }

  public int getTiretLength() {
    return 3;
  }

  public double getValeurPasX() {
    return xLength_;
  }

  public double getValeurPasY() {
    return yLength_;
  }

  public int getXMinorGraduationNb() {
    return xMinorGraduationNb_;
  }

  public int getYMinorGraduationNb() {
    return yMinorGraduationNb_;
  }

  @Override
  public void initFrom(EbliUIProperties _p) {
    super.initFrom(_p);
    if (_p.isDefined(GRADUATION_X_NB)) {
      setNbGraduationX(_p.getInteger(GRADUATION_X_NB));
    }
    if (_p.isDefined(GRADUATION_Y_NB)) {
      setNbGraduationY(_p.getInteger(GRADUATION_Y_NB));
    }
    if (_p.isDefined(AXIS_Y_VISIBLE)) {
      setDrawY(_p.getBoolean(AXIS_Y_VISIBLE));
    }
    if (_p.isDefined(AXIS_X_VISIBLE)) {
      setDrawX(_p.getBoolean(AXIS_X_VISIBLE));
    }
    if (_p.isDefined(GRID_VISIBLE)) {
      setDrawGrid(_p.getBoolean(GRID_VISIBLE));
    }
    if (_p.isDefined(GRADUATION_X_MODE_AUTO)) {
      setGraduationXModeAuto(_p.getBoolean(GRADUATION_X_MODE_AUTO));
    }
    if (_p.isDefined(GRADUATION_Y_MODE_AUTO)) {
      setGraduationYModeAuto(_p.getBoolean(GRADUATION_Y_MODE_AUTO));
    }
    if (_p.isDefined(GRADUATION_X_MODE_LENGTH)) {
      setModeLongueurX(_p.getBoolean(GRADUATION_X_MODE_LENGTH));
    }
    if (_p.isDefined(GRADUATION_Y_MODE_LENGTH)) {
      setModeLongueurY(_p.getBoolean(GRADUATION_Y_MODE_LENGTH));
    }
    if (_p.isDefined(GRADUATION_X_LENGTH)) {
      setXLength(_p.getDouble(GRADUATION_X_LENGTH));
    }
    if (_p.isDefined(GRADUATION_Y_LENGTH)) {
      setYLength(_p.getDouble(GRADUATION_Y_LENGTH));
    }
    if (_p.isDefined(MINOR_GRADUATION_X_VISIBLE)) {
      setXMinorDraw(_p.getBoolean(MINOR_GRADUATION_X_VISIBLE));
    }
    if (_p.isDefined(MINOR_GRADUATION_Y_VISIBLE)) {
      setYMinorDraw(_p.getBoolean(MINOR_GRADUATION_Y_VISIBLE));
    }
    if (_p.isDefined(MINOR_GRADUATION_X_NB)) {
      setXMinorGraduationNb(_p.getInteger(MINOR_GRADUATION_X_NB));
    }
    if (_p.isDefined(MINOR_GRADUATION_Y_NB)) {
      setYMinorGraduationNb(_p.getInteger(MINOR_GRADUATION_Y_NB));
    }
  }

  @Override
  public boolean isCouleurModifiable() {
    return true;
  }

  public boolean isDrawGrid() {
    return gridDraw_;
  }

  public boolean isDrawX() {
    return xDraw_;
  }

  public boolean isDrawY() {
    return yDraw_;
  }

  public boolean isAxisPainted() {
    return isDrawX() || isDrawY();
  }

  public boolean isModeAutomatiqueX() {
    return xModeAuto_;
  }

  public boolean isModeAutomatiqueY() {
    return yModeAuto_;
  }

  public boolean isModeLongueurPasX() {
    return xModeLongueur_;
  }

  /**
   * @return the xMinorDraw_
   */
  public boolean isXMinorDraw() {
    return xMinorDraw_;
  }

  /**
   * @return the yModeLongueur_
   */
  public boolean isYModeLongueur() {
    return yModeLongueur_;
  }

  @Override
  public void paintAllInImage(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    // super.paintAllInImage(_g, _versEcran, _versReel, _clipReel);
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    GrBoite domaine = vue_.getAbstractCalque().getDomaine();
    final GrBoite zv = _clipReel.intersectionXY(domaine);

    if (zv == null) {
      return;
    }
    final GrBoite targetBoite = new GrBoite();
    targetBoite.ajuste(0, 0, 0);
    // la boite d'affichage

    if (!_versEcran.isSame(getVersEcran())) {
      return;
    }
    targetBoite.ajuste(getWidth(), getHeight(), 0);
    final Color old = _g.getColor();
    final Font oldFont = _g.getFont();
    // la fonte
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = font == null ? null : _g.getFontMetrics(font);
    final TDoubleArrayList xValue = new TDoubleArrayList(20);
    final TIntArrayList xStrWidth = new TIntArrayList(20);
    final List xString = new ArrayList(20);

    final boolean isXPainted = xDraw_ && zv.getMinX() < zv.getMaxX() && xNbGraduation_ > 0;

    if (isXPainted) {
      computeXValues(zv, _g, xValue, xStrWidth, xString);
    }
    double yMax = 0;
    final TDoubleArrayList yValue = new TDoubleArrayList(20);
    final TIntArrayList yStrWidth = new TIntArrayList(20);
    final List yString = new ArrayList(20);
    final boolean isYPainted = yDraw_ && zv.getMinY() < zv.getMaxY() && yNbGraduation_ > 0;
    if (isYPainted) {
      yMax = computeYValues(_g, zv, yValue, yStrWidth, yString);
    }

    // boolean rapide = isRapide();
    final GrMorphisme versEcran = _versEcran;
    final int tiret = getTiretLength();
    //-- ahx: item 1.1.3 enlever espace inutile car le nouveau besoin est d'avoir un rep�re qui tombe juste.
    final int marge = 0;//1;
    int absyAxe = (int) (targetBoite.getMinX() + yMax + 2 * marge + tiret);
    final int margeBas = (font == null ? 0 : font.getSize()) + 2 * marge + tiret;

    int margeToRemoveXAxe = (font == null ? 0 : font.getSize()) + 2 + tiret;
    int ordxAxe = (int) (targetBoite.getMaxY() - margeToRemoveXAxe);
    // la marge droite doit etre calculee pour afficher la derniere graduation.
    //int margeDroit = getLastNonZero(xStrWidth) / 2 + 3;
    int top = 1;//Math.max(3, font == null ? 3 : font.getSize());
    //-- ahx: item 1.1.3 enlever espace inutile car le nouveau besoin est d'avoir un rep�re qui tombe juste.
    int left = absyAxe;//+ 5;
    int bottom = margeBas;// + 5;
    int right = 1;//Math.max(margeDroit, 3);
    //the insets must be determined in order to keep the ratio w/h:
    //int heightInsets = top + bottom;
    //int widthInsets = right + left;
    //CtuluRatio ratio = new CtuluRatio(vue_.getSize());
    /*
     if (ratio.toY(widthInsets) > heightInsets) {
     top = (int) (ratio.toY(widthInsets) - bottom);
     } else if (ratio.toX(heightInsets) > widthInsets) {
     right = (int) (ratio.toX(heightInsets) - left);
     }
     */
    vue_.setUserInsets(new Insets(top, left, bottom, right));
    // we get again the domain after the userInsets modification
    domaine = vue_.getAbstractCalque().getDomaine();
    final GrBoite ecranBoite = domaine.applique(_versEcran);
    //-- ahx: item 1.1.3 enlever espace inutile car le nouveau besoin est d'avoir un rep�re qui tombe juste.
    /*
     if (ecranBoite.getMinX() - 5 > absyAxe) {
     absyAxe = (int) (ecranBoite.getMinX() - 5);
     }
     if (ecranBoite.getMaxY() + 5 < ordxAxe) {
     ordxAxe = (int) (ecranBoite.getMaxY() + 5);
     }
     */
    if (isXPainted) {
      fillXZone(_g, targetBoite, absyAxe, ordxAxe, ecranBoite);

    }

    // l'axe des x
    final double maxXForAxe = ecranBoite.getMaxX() - marge;

    // l'axe des y
    final double minYForAxe = ecranBoite.getMinY() + marge;
    // les graduation en x

    final int ascent = fm == null ? 0 : fm.getAscent();
    final int ordXString = ordxAxe + tiret + ascent;

    //first y:
    if (isYPainted) {
      fillYZone(_g, targetBoite, absyAxe, ecranBoite);
      drawYaxe(_g, zv, xValue, yValue, versEcran, absyAxe, ordxAxe, minYForAxe);
      drawYGraduations(_g, yValue, yStrWidth, yString, versEcran, marge, absyAxe, ordxAxe, ecranBoite, maxXForAxe);
    }

    //then x
    if (isXPainted) {
      drawXAxe(_g, zv, xValue, versEcran, absyAxe, ordxAxe, maxXForAxe);
      drawXGraduations(_g, targetBoite, xValue, xStrWidth, xString, absyAxe, ordxAxe, ecranBoite, minYForAxe,
              ordXString);
    }
    _g.setColor(old);
    _g.setFont(oldFont);
  }

  public void paintDonneesOLD(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    GrBoite domaine = vue_.getAbstractCalque().getDomaine();
    final GrBoite zv = _clipReel.intersectionXY(domaine);

    if (zv == null) {
      return;
    }
    final GrBoite targetBoite = new GrBoite();
    targetBoite.ajuste(0, 0, 0);
    // la boite d'affichage

    if (!_versEcran.isSame(getVersEcran())) {
      return;
    }
    targetBoite.ajuste(getWidth(), getHeight(), 0);
    final Color old = _g.getColor();
    final Font oldFont = _g.getFont();
    // la fonte
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = font == null ? null : _g.getFontMetrics(font);
    final TDoubleArrayList xValue = new TDoubleArrayList(20);
    final TIntArrayList xStrWidth = new TIntArrayList(20);
    final List xString = new ArrayList(20);

    final boolean isXPainted = xDraw_ && zv.getMinX() < zv.getMaxX() && xNbGraduation_ > 0;

    if (isXPainted) {
      computeXValues(zv, _g, xValue, xStrWidth, xString);
    }
    double yMax = 0;
    final TDoubleArrayList yValue = new TDoubleArrayList(20);
    final TIntArrayList yStrWidth = new TIntArrayList(20);
    final List yString = new ArrayList(20);
    final boolean isYPainted = yDraw_ && zv.getMinY() < zv.getMaxY() && yNbGraduation_ > 0;
    if (isYPainted) {
      yMax = computeYValues(_g, zv, yValue, yStrWidth, yString);
    }

    // boolean rapide = isRapide();
    final GrMorphisme versEcran = _versEcran;
    final int tiret = getTiretLength();
    final int marge = 1;
    int absyAxe = (int) (targetBoite.getMinX() + yMax + 2 * marge + tiret);
    final int margeBas = (font == null ? 0 : font.getSize()) + 2 * marge + tiret;
    int ordxAxe = (int) (targetBoite.getMaxY() - margeBas);
    // la marge droite doit etre calculee pour afficher la derniere graduation.
    int margeDroit = getLastNonZero(xStrWidth) / 2 + 3;
    int top = Math.max(3, font == null ? 3 : font.getSize());
    int left = absyAxe + 5;
    int bottom = margeBas + 5;
    int right = Math.max(margeDroit, 3);
    //the insets must be determined in order to keep the ratio w/h:
    int heightInsets = top + bottom;
    int widthInsets = right + left;
    CtuluRatio ratio = new CtuluRatio(vue_.getSize());
    if (ratio.toY(widthInsets) > heightInsets) {
      top = (int) (ratio.toY(widthInsets) - bottom);
    } else if (ratio.toX(heightInsets) > widthInsets) {
      right = (int) (ratio.toX(heightInsets) - left);
    }
    vue_.setUserInsets(new Insets(top, left, bottom, right));
    // we get again the domain after the userInsets modification
    domaine = vue_.getAbstractCalque().getDomaine();
    final GrBoite ecranBoite = domaine.applique(_versEcran);
    if (ecranBoite.getMinX() - 5 > absyAxe) {
      absyAxe = (int) (ecranBoite.getMinX() - 5);
    }
    if (ecranBoite.getMaxY() + 5 < ordxAxe) {
      ordxAxe = (int) (ecranBoite.getMaxY() + 5);
    }
    if (isXPainted) {
      fillXZone(_g, targetBoite, absyAxe, ordxAxe, ecranBoite);

    }

    // l'axe des x
    final double maxXForAxe = ecranBoite.getMaxX() - marge;

    // l'axe des y
    final double minYForAxe = ecranBoite.getMinY() + marge;
    // les graduation en x

    final int ascent = fm == null ? 0 : fm.getAscent();
    final int ordXString = ordxAxe + tiret + ascent;

	    //first y:
    if (isYPainted) {
      fillYZone(_g, targetBoite, absyAxe, ecranBoite);
      drawYaxe(_g, zv, xValue, yValue, versEcran, absyAxe, ordxAxe, minYForAxe);
      drawYGraduations(_g, yValue, yStrWidth, yString, versEcran, marge, absyAxe, ordxAxe, ecranBoite, maxXForAxe);
    }

    //then x
    if (isXPainted) {
      drawXAxe(_g, zv, xValue, versEcran, absyAxe, ordxAxe, maxXForAxe);
      drawXGraduations(_g, targetBoite, xValue, xStrWidth, xString, absyAxe, ordxAxe, ecranBoite, minYForAxe,
              ordXString);
    }
    _g.setColor(old);
    _g.setFont(oldFont);
  }

  int getLastNonZero(TIntArrayList l) {
    if (l == null || l.isEmpty()) {
      return 0;
    }
    for (int i = l.size() - 1; i >= 0; i--) {
      int value = l.getQuick(i);
      if (value > 0) {
        return value;
      }
    }
    return 0;
  }

  // Icon
  /**
   * Dessin de l'icone.
   *
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color fg = getForeground();
    if (isAttenue()) {
      fg = attenueCouleur(fg);
    }
    _g.setColor(fg);
    final int w = getIconWidth();
    final int h = getIconHeight();
    for (int i = 6; i < w - 2; i += 8) {
      _g.drawLine(_x + i, _y + 1, _x + i, _y + h - 1);
    }
    for (int j = 6; j < h - 2; j += 8) {
      _g.drawLine(_x + 1, _y + j, _x + w - 1, _y + j);
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
          final GrBoite _clipReel) {
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    res.put(GRID_VISIBLE, gridDraw_);
    res.put(AXIS_X_VISIBLE, xDraw_);
    res.put(AXIS_Y_VISIBLE, yDraw_);
    res.put(GRADUATION_X_MODE_AUTO, this.xModeAuto_);
    res.put(GRADUATION_Y_MODE_AUTO, this.yModeAuto_);
    res.put(GRADUATION_X_MODE_LENGTH, this.xModeLongueur_);
    res.put(GRADUATION_Y_MODE_LENGTH, this.isYModeLongueur());
    res.put(GRADUATION_X_LENGTH, this.xLength_);
    res.put(GRADUATION_Y_LENGTH, this.yLength_);
    res.put(GRADUATION_X_NB, xNbGraduation_);
    res.put(GRADUATION_Y_NB, yNbGraduation_);
    res.put(MINOR_GRADUATION_X_NB, this.xMinorGraduationNb_);
    res.put(MINOR_GRADUATION_Y_NB, this.yMinorGraduationNb_);
    res.put(MINOR_GRADUATION_Y_VISIBLE, this.yMinorDraw_);
    res.put(MINOR_GRADUATION_X_VISIBLE, this.isXMinorDraw());

    return res;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _poly, final int _mode) {
    return null;
  }

  public void setDrawGrid(final boolean _drawGrid) {
    if (_drawGrid != gridDraw_) {
      gridDraw_ = _drawGrid;
      grilleChanged();
    }
  }

  public void setDrawX(final boolean _drawX) {
    if (xDraw_ != _drawX) {
      xDraw_ = _drawX;
      grilleChanged();
    }
  }

  public void setDrawY(final boolean _drawY) {
    if (yDraw_ != _drawY) {
      yDraw_ = _drawY;
      grilleChanged(AXIS_Y_VISIBLE, yDraw_);
    }
  }

  public void setGraduationXModeAuto(boolean _modeAutomatiqueX) {
    if (xModeAuto_ != _modeAutomatiqueX) {
      this.xModeAuto_ = _modeAutomatiqueX;
      grilleChanged(GRADUATION_X_MODE_AUTO, xModeAuto_);

    }
  }

  public void setGraduationYModeAuto(boolean modeAutomatiqueY) {
    if (yModeAuto_ != modeAutomatiqueY) {
      this.yModeAuto_ = modeAutomatiqueY;
      grilleChanged(GRADUATION_Y_MODE_AUTO, yModeAuto_);
    }
  }

  public void setModeLongueurX(boolean _modeLongueurPasX) {
    if (xModeLongueur_ != _modeLongueurPasX) {
      this.xModeLongueur_ = _modeLongueurPasX;
      if (xModeLongueur_ && xLength_ <= 0) {
        xLength_ = Math.max(1, vue_.getAbstractCalque().getDomaine().getDeltaX() / 10);
        firePropertyChange(GRADUATION_X_LENGTH, 0, xLength_);
      }
      grilleChanged(GRADUATION_X_MODE_LENGTH, xModeLongueur_);
    }
  }

  /**
   * @param _yModeLongueur the yModeLongueur_ to set
   */
  public void setModeLongueurY(boolean _yModeLongueur) {
    if (yModeLongueur_ != _yModeLongueur) {
      this.yModeLongueur_ = _yModeLongueur;
      if (yModeLongueur_ && yLength_ <= 0) {
        yLength_ = Math.max(1, vue_.getAbstractCalque().getDomaine().getDeltaY() / 10);
        firePropertyChange(GRADUATION_Y_LENGTH, 0, yLength_);
      }
      grilleChanged(GRADUATION_Y_MODE_LENGTH, yModeLongueur_);
    }
  }

  /**
   * @param _nbGraduationX
   */
  public void setNbGraduationX(final int _nbGraduationX) {
    if (xNbGraduation_ != _nbGraduationX) {
      int old = xNbGraduation_;
      xNbGraduation_ = Math.abs(_nbGraduationX);
      grilleChanged(GRADUATION_X_NB, old, xNbGraduation_);
    }
  }

  /**
   * @param _nbGraduationY
   */
  public void setNbGraduationY(final int _nbGraduationY) {
    if (_nbGraduationY != yNbGraduation_) {
      int old = yNbGraduation_;
      yNbGraduation_ = Math.abs(_nbGraduationY);
      grilleChanged(GRADUATION_Y_NB, old, yNbGraduation_);
    }
  }

  /**
   *
   */
  @Override
  public boolean setProperty(String _name, Object _value) {
    if (_value == null) {
      return false;
    }
    if (GRID_VISIBLE.equals(_name)) {
      setDrawGrid((Boolean) _value);
      return true;
    }
    if (AXIS_X_VISIBLE.equals(_name)) {
      setDrawX((Boolean) _value);
      return true;
    }
    if (AXIS_Y_VISIBLE.equals(_name)) {
      setDrawY((Boolean) _value);
      return true;
    }
    if (GRADUATION_X_MODE_AUTO.equals(_name)) {
      setGraduationXModeAuto((Boolean) _value);
      return true;
    }
    if (GRADUATION_Y_MODE_AUTO.equals(_name)) {
      setGraduationYModeAuto((Boolean) _value);
      return true;
    }
    if (MINOR_GRADUATION_Y_VISIBLE.equals(_name)) {
      setYMinorDraw((Boolean) _value);
      return true;
    }
    if (MINOR_GRADUATION_X_VISIBLE.equals(_name)) {
      setXMinorDraw((Boolean) _value);
      return true;
    }
    if (GRADUATION_X_MODE_LENGTH.equals(_name)) {
      setModeLongueurX((Boolean) _value);
      return true;
    }
    if (GRADUATION_Y_MODE_LENGTH.equals(_name)) {
      setModeLongueurY((Boolean) _value);
      return true;
    }
    if (GRADUATION_X_LENGTH.equals(_name)) {
      setXLength((Double) _value);
      return true;
    }
    if (GRADUATION_Y_LENGTH.equals(_name)) {
      setYLength((Double) _value);
      return true;
    }
    if (GRADUATION_X_NB.equals(_name)) {
      setNbGraduationX((Integer) _value);
      return true;
    }
    if (GRADUATION_Y_NB.equals(_name)) {
      setNbGraduationY((Integer) _value);
      return true;
    }
    if (MINOR_GRADUATION_X_NB.equals(_name)) {
      setXMinorGraduationNb((Integer) _value);
      return true;
    }
    if (MINOR_GRADUATION_Y_NB.equals(_name)) {
      setYMinorGraduationNb((Integer) _value);
      return true;
    }
    return super.setProperty(_name, _value);
  }

  /**
   * Affectation de la propriete <I>boite</I>.
   */
  // public void setBoite(final GrBoite _v) {
  // if (boite_ != _v) {
  // final GrBoite vp = boite_;
  // boite_ = _v;
  // firePropertyChange("boite", vp, _v);
  // }
  // }
  @Override
  public void setVisible(final boolean _v) {
    // if (_v && boite_ == null) {
    // boite_ = vue_.getAbstractCalque().getDomaine();
    // }
    if (!_v) {
      vue_.setUserInsets(null);
      super.setVisible(false);
    } else {
      super.setVisible(_v);
    }
    firePropertyChange("grille", true, false);

  }

  public void setXLength(final double _lenghtSteps) {
    if (xLength_ != _lenghtSteps) {
      double l = Math.abs(_lenghtSteps);
      if (CtuluLib.isZero(l)) {
        return;
      }
      double old = xLength_;
      xLength_ = _lenghtSteps;
      grilleChanged(GRADUATION_X_LENGTH, old, xLength_);
    }

  }

  public void setXMinorDraw(final boolean _drawX) {
    if (xMinorDraw_ != _drawX) {
      xMinorDraw_ = _drawX;
      grilleChanged(MINOR_GRADUATION_X_VISIBLE, xMinorDraw_);
    }
  }

  public void setXMinorGraduationNb(final int _nbGraduation) {
    if (xMinorGraduationNb_ != _nbGraduation) {
      int old = xMinorGraduationNb_;
      xMinorGraduationNb_ = Math.abs(_nbGraduation);
      grilleChanged(MINOR_GRADUATION_X_NB, old, xMinorGraduationNb_);
    }
  }

  public void setYLength(final double _lenghtSteps) {
    if (yLength_ != _lenghtSteps) {
      double l = Math.abs(_lenghtSteps);
      if (CtuluLib.isZero(l)) {
        return;
      }
      double old = yLength_;
      yLength_ = _lenghtSteps;
      grilleChanged(GRADUATION_Y_LENGTH, old, yLength_);
    }

  }

  public void setYMinorDraw(final boolean _drawY) {
    if (yMinorDraw_ != _drawY) {
      yMinorDraw_ = _drawY;
      grilleChanged(MINOR_GRADUATION_Y_VISIBLE, yMinorDraw_);
    }
  }

  /**
   * @param _yMinorNbGraduation the new _nbGraduation to set
   */
  public void setYMinorGraduationNb(final int _yMinorNbGraduation) {
    if (yMinorGraduationNb_ != _yMinorNbGraduation) {
      int old = yMinorGraduationNb_;
      yMinorGraduationNb_ = Math.abs(_yMinorNbGraduation);
      grilleChanged(MINOR_GRADUATION_Y_NB, old, yMinorGraduationNb_);
    }
  }

  public BVueCalque getVue() {
    return vue_;
  }

}
