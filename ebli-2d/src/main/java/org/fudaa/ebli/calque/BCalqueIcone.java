/*
 * @file         BCalqueIcone.java
 * @creation     2000-10-19
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import com.memoire.bu.BuFilters;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Shape;
import java.awt.image.FilteredImageSource;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
/**
 * Un calque d'affichage d'icone statique geo-localise.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:55:47 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class BCalqueIcone extends BCalqueAffichage {
  private VecteurGrPoint points_;
  public BCalqueIcone() {
    super();
    points_= new VecteurGrPoint();
    dx_= 0;
    dy_= 0;
  }
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final int w= getIconWidth();
    final int h= getIconHeight();
    if (icon_ != null) {
      Icon icon= icon_;
      if (isAttenue()) {
        final Image image=
          createImage(
            new FilteredImageSource(
              ((ImageIcon)icon).getImage().getSource(),
              BuFilters.BRIGHTER));
        icon= new ImageIcon(image);
      }
      if ((w > 20) || (h > 20)) {
        final Shape old= _g.getClip();
        _g.setClip(_x + 2, _y + 2, 20, 20);
        icon.paintIcon(_c, _g, _x + 2, _y + 2);
        _g.setClip(old);
      } else {
        icon.paintIcon(_c, _g, _x + 13, _y + 7);
        icon.paintIcon(_c, _g, _x + 4, _y + 4);
      }
    }
  }
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    Icon icon= icon_;
    final int np= points_.nombre();
    final GrMorphisme versEcran= getVersEcran();
    if (isAttenue() && (icon instanceof ImageIcon)) {
      final Image image=
        createImage(
          new FilteredImageSource(
            ((ImageIcon)icon).getImage().getSource(),
            BuFilters.BRIGHTER));
      icon= new ImageIcon(image);
    }
    if (isRapide()) {
      final int inc= 1 + np / 50;
      Color fg= getForeground();
      if (isAttenue()) {
        fg= attenueCouleur(fg);
      }
      _g.setColor(fg);
      for (int i= 0; i < np; i += inc) {
        final GrPoint p= points_.renvoie(i).applique(versEcran);
        final int x= (int)p.x_;
        final int y= (int)p.y_;
        _g.drawLine(x, y, x, y);
      }
    } else {
      for (int i= 0; i < np; i++) {
        final GrPoint p= points_.renvoie(i).applique(versEcran);
        final int x= (int)p.x_;
        final int y= (int)p.y_;
        icon.paintIcon(this, _g, x + dx_, y + dy_);
      }
    }
  }
  /**
   * Vide la liste de points.
   */
  public void vide() {
    points_.vide();
  }
  /**
   * Ajoute un point a la liste de points.
   */
  public void ajoute(final GrPoint _point) {
    points_.ajoute(_point);
  }
  /**
   * Enleve un point a la liste de points.
   */
  public void enleve(final GrPoint _point) {
    points_.enleve(_point);
  }
  public VecteurGrPoint getPoints() {
    return points_;
  }
  public void setPoints(final VecteurGrPoint _points) {
    if (_points != points_) {
      final VecteurGrPoint vp= points_;
      points_= _points;
      firePropertyChange("points", vp, points_);
    }
  }
  @Override
  public GrBoite getDomaine() {
    GrBoite r= null;
    if (isVisible()) {
      r= super.getDomaine();
      if (points_.nombre() > 0) {
        if (r == null) {
          r= new GrBoite();
        }
        final int n= points_.nombre();
        for (int i= 0; i < n; i++) {
          r.ajuste(points_.renvoie(i));
        }
      }
    }
    return r;
  }
  // Propriete icon
  protected Icon icon_;
  public Icon getIcon() {
    return icon_;
  }
  public void setIcon(final Icon _icon) {
    icon_= _icon;
  }
  // Propriete dx
  protected int dx_;
  public int getDX() {
    return dx_;
  }
  public void setDX(final int _dx) {
    dx_= _dx;
  }
  // Propriete dy
  protected int dy_;
  public int getDY() {
    return dy_;
  }
  public void setDY(final int _dy) {
    dy_= _dy;
  }
}
