/**
 * @creation 12 juil. 2004
 * @modification $Date: 2007-05-04 13:49:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurList;
import org.fudaa.ebli.controle.BSelecteurListComboBox;
import org.fudaa.ebli.controle.BSelecteurListTarget;
import org.fudaa.ebli.controle.BSelecteurListTimeTarget;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EbliCalqueActionTimeChooser.java,v 1.2 2007-05-04 13:49:47 deniger Exp $
 */
public class EbliCalqueActionTimeChooser extends EbliActionPaletteTreeModel implements BSelecteurListTarget {

  protected static class MultiAdapter implements BSelecteurListTimeTarget, ListSelectionListener {
    final ListSelectionModel common_ = new DefaultListSelectionModel();

    final BSelecteurListTimeTarget[] targets_;

    public MultiAdapter(final BSelecteurListTimeTarget[] _targets) {
      super();
      targets_ = _targets;
      common_.addListSelectionListener(this);
    }

    @Override
    public ListModel getTimeListModel() {
      return targets_[0].getTimeListModel();
    }

    @Override
    public ListSelectionModel getTimeListSelectionModel() {
      return common_;
    }

    @Override
    public void valueChanged(final ListSelectionEvent _e) {
      final boolean empty = common_.isSelectionEmpty();
      final int idx = empty ? -1 : common_.getLeadSelectionIndex();
      for (int i = targets_.length - 1; i >= 0; i--) {
        if (empty) {
          targets_[i].getTimeListSelectionModel().clearSelection();
        } else {
          targets_[i].getTimeListSelectionModel().setSelectionInterval(idx, idx);
        }
      }

    }

  }

  final boolean useCombo_;

  public EbliCalqueActionTimeChooser(final BArbreCalqueModel _c) {
    this(_c.getTreeSelectionModel());
  }

  /**
   * Initialise l'action.
   */
  public EbliCalqueActionTimeChooser(final TreeSelectionModel _c) {
    this(_c, false);
  }

  public EbliCalqueActionTimeChooser(final TreeSelectionModel _c, boolean _useCombo) {
    super(EbliLib.getS("Temps"), EbliResource.EBLI.getToolIcon("time"), "DISPLAY_TIME", _c);
    setDefaultToolTip(EbliLib.getS("S�lectionner le temps � afficher"));
    setResizable(true);
    useCombo_ = _useCombo;
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent() {
    return useCombo_?new BSelecteurListComboBox():new BSelecteurList();
  }

  @Override
  protected boolean isTargetValid(final Object _o) {
    return _o instanceof BSelecteurListTimeTarget;
  }

  @Override
  protected boolean setPaletteTarget(final Object _target) {
    if (_target == this || isTargetValid(_target)) { return palette_.setPalettePanelTarget(this); }
    return palette_.setPalettePanelTarget(null);
  }

  @Override
  public ListModel getListModel() {
    if (target_ instanceof BSelecteurListTimeTarget)
    return target_ == null ? null : ((BSelecteurListTimeTarget) target_).getTimeListModel();
    else
      return null;
  }

  @Override
  public ListSelectionModel getListSelectionModel() {
    return target_ == null ? null : ((BSelecteurListTimeTarget) target_).getTimeListSelectionModel();
  }

  /**
   * Met a jour la cible de la palette.
   */
  @Override
  public void updateBeforeShow() {
    setPaletteTarget(target_ == null ? null : this);
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    final TreeSelectionModel model = ((TreeSelectionModel) _e.getSource());
    Object newTarget = null;
    final int selectionCount = model.getSelectionCount();
    // un seul calque s�lectionn� -> ok
    if (selectionCount == 1) {
      newTarget = model.getSelectionPath().getLastPathComponent();
      // plusieurs calques: on recherche les calques qui des MvListPaletteTimeTarget et on garde les
      // calques ayant les memes pas de temps affich�s. En g�n�ral, les calques ont tous les m�mes pas de temps
    } else if (selectionCount > 1) {
      final TreePath[] selection = model.getSelectionPaths();
      final List times = new ArrayList(selectionCount);
      ListModel first = null;
      for (int i = 0; i < selectionCount; i++) {
        if (selection[i] != null && isTargetValid(selection[i].getLastPathComponent())) {
          final BSelecteurListTimeTarget lastPathComponent = (BSelecteurListTimeTarget) selection[i]
              .getLastPathComponent();

          final ListModel currentList = lastPathComponent.getTimeListModel();
          // il faut que la liste ne soit pas nulle
          boolean ok = currentList != null;
          if (first == null) {
            first = currentList;
            // si la liste n'est pas null et si on a 2 listes differentes, il faut comparer ligne par ligne.
          } else if (ok && currentList != first) {
            // pas null
            ok = currentList.getSize() == first.getSize();
            if (ok) {
              for (int j = currentList.getSize() - 1; j >= 0 && ok; j--) {
                ok = CtuluLib.isEquals(currentList.getElementAt(j), first.getElementAt(j));
              }
            }
          }
          if (ok) {
            times.add(lastPathComponent);
          }
        }
      }
      // on a plusieurs listes correctes
      newTarget = times.size() == 0 ? null : new MultiAdapter((BSelecteurListTimeTarget[]) times
          .toArray(new BSelecteurListTimeTarget[times.size()]));
    }
    setTarget(newTarget);

  }

  /**
   * Redefinie pour rendre public.
   */
  @Override
  public void setTarget(final Object _o) {
    super.setTarget(_o);
  }

}