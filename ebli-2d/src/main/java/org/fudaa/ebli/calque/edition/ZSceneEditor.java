/*
 * @creation     22 avr. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Regroupe les traitements d'�dition de la scene.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZSceneEditor {

  protected ZScene scene_;
  protected ZEbliCalquesPanel calquePanel_;
  protected CtuluCommandManager mng_;
  protected CtuluUI ui_;
  private ZSceneEditorListener editorListener;
  /**
   * Toutes les actions li�es � la scene, ajout�es dans les menus correspondants
   */
  private EbliActionInterface[] actions_;

  public ZSceneEditor(ZScene _scene, ZEbliCalquesPanel _calquePanel) {
    scene_ = _scene;
    calquePanel_ = _calquePanel;
  }

  public void setEditorListener(ZSceneEditorListener editorListener) {
    this.editorListener = editorListener;
  }

  public ZSceneEditorListener getEditorListener() {
    return editorListener;
  }

  public void setUI(CtuluUI _ui) {
    ui_ = _ui;
  }

  protected CtuluUI getUI() {
    return ui_;
  }

  public void setMng(CtuluCommandManager _mng) {
    mng_ = _mng;
  }

  protected CtuluCommandManager getMng() {
    return mng_;
  }

  public ZScene getScene() {
    return scene_;
  }

  public EbliActionInterface[] getActions() {
    return actions_;
  }

  public void setActions(EbliActionInterface[] _actions) {
    actions_ = _actions;
  }

  public void rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0) {
    ZCalqueEditable[] cqs = scene_.getEditableLayers();
    if (editorListener != null) {
      editorListener.globalRotationStart();
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite(
            EbliLib.getS("Rotation de s�lection"));
    for (int i = 0; i < cqs.length; i++) {
      cqs[i].rotateSelectedObjects(_angRad, _xreel0, _yreel0, cmp, ui_);
    }
    if (editorListener != null) {
      editorListener.globalRotationEnd();
    }
    if (mng_ != null) {
      mng_.addCmd(cmp.getSimplify());
    }
  }

  public void moveSelectedObjects(final double _dx, final double _dy, final double _dz) {
    if (_dx == 0 && _dy == 0 && _dz == 0) {
      return;
    }
    ZCalqueEditable[] cqs = scene_.getEditableLayers();
    if (editorListener != null) {
      editorListener.globalMovedStart();
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite(
            EbliLib.getS("D�placer s�lection"));
    int nb = getScene().getSelectionHelper().getNbTotalSelectedObject();
    boolean ok = false;
    for (int i = 0; i < cqs.length; i++) {
      ok |= cqs[i].moveSelectedObjects(_dx, _dy, _dz, cmp, ui_);
    }
    if (ok) {
      final String msg = EbliLib.getS("{0} objet(s) d�plac�(s)", Integer.toString(nb));
      FuLog.warning("MOD:" + msg);
      getUI().message(null, msg, true);
    }
    if (editorListener != null) {
      editorListener.globalMovedEnd();
    }
    if (mng_ != null) {
      mng_.addCmd(cmp.getSimplify());
    }
  }

  public void removeSelectedObjects(CtuluCommandContainer _cmd) {

    int nb = getScene().getSelectionHelper().getNbTotalSelectedObject();
    if (nb == 0) {
      return;
    }
    ZCalqueEditable[] cqs = scene_.getEditableLayers();
    final CtuluCommandComposite cmp = new CtuluCommandComposite(
            EbliLib.getS("Supprimer s�lection"));
    boolean done = false;
    for (int i = 0; i < cqs.length; i++) {
      done |= cqs[i].removeSelectedObjects(cmp, ui_);
    }
    if (done) {
      final String msg = EbliLib.getS("{0} objet(s) supprim�(s)", Integer.toString(nb));
      FuLog.warning("MOD:" + msg);
      getUI().message(null, msg, true);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * Copie les objets s�lectionn�s.
   */
  public void copySelectedObjects() {
    ZCalqueEditable[] cqs = scene_.getEditableLayers();
    final CtuluCommandComposite cmp = new CtuluCommandComposite(
            EbliLib.getS("Dupliquer s�lection"));
    for (int i = 0; i < cqs.length; i++) {
      cqs[i].copySelectedObjects(cmp, ui_);
    }
    if (mng_ != null) {
      mng_.addCmd(cmp.getSimplify());
    }
  }

  /**
   * Scinde une g�om�trie en 2 distinctes.<p> Principe : <br> Pour un multipoint, selection de plusieurs sommets d'un multipoint, et s�paration.<br>
   * Pour une polyligne, selection de 2 sommets cons�cutifs, et s�paration.
   */
  public void splitSelectedObject() {
    int idx = getScene().getLayerSelectionMulti().isSelectionInOneBloc();
    ZCalqueEditable cq = (ZCalqueEditable) getScene().getLayerForId(idx);
    cq.splitSelectedObject(mng_, ui_);
  }

  /**
   * Joint 2 g�om�tries d'un m�me calque en 1<p> Principe : <br> Pour 2 multipoints, selection des 2 multipoints, ou d'un ou plusieurs sommets de 2
   * multipoints.<br> Pour 2 polylignes, selection de 2 sommets extremit�.
   */
  public void joinSelectedObjects() {
    int[] idxs = getScene().getSelectionHelper().getSelectedIndexes();
    ZCalqueEditable cq = (ZCalqueEditable) getScene().getLayerForId(idxs[0]);
    cq.joinSelectedObjects(mng_, ui_);
  }

  protected void methodNotImplemented() {
    StackTraceElement stel = new Throwable().getStackTrace()[1];
    String name = stel.getClassName() + "." + stel.getMethodName();
    ui_.message("Not implemented", name, false);
  }

  /**
   * Effectue un zoom sur la selection. Cette selection peut regrouper des �l�ments de diff�rents calques.
   */
  public void zoomOnSelected() {
    GrBoite boite = null;
    for (ZCalqueAffichageDonneesInterface cq : scene_.getAllLayers()) {
      GrBoite boiteTmp = cq.getDomaineOnSelected();
      if (boiteTmp != null) {
        if (boiteTmp.getDeltaX() <= 0 || boiteTmp.getDeltaY() <= 0) {
          boiteTmp = cq.getZoomOnSelected();
        }
      }
      if (boite == null) {
        boite = boiteTmp;
      } else {
        boite.ajuste(boiteTmp);
      }
    }
    zoomOn(boite);
  }

  /**
   * Effectue un zoom sur la selection mais en gardant des dimensions minimums.
   */
  public void zoomControlledOnSelected() {
    GrBoite boite = getZoomOnSelected();
    zoomOn(boite);
  }

  /**
   * Selectionne l'objet suivant celui selectionn�.
   */
  public void selectNextObject() {
    scene_.selectNext();
  }

  /**
   * Selectionne l'objet pr�c�dent celui selectionn�.
   */
  public void selectPreviousObject() {
    scene_.selectPrevious();
  }

  public GrBoite getZoomOnSelected() {
    GrBoite boite = null;
    for (ZCalqueAffichageDonneesInterface cq : scene_.getAllLayers()) {
      GrBoite boiteTmp = cq.getZoomOnSelected();
      if (boite == null) {
        boite = boiteTmp;
      } else {
        boite.ajuste(boiteTmp);
      }
    }
    return boite;
  }

  /**
   * n'ajoute pas une marge autour de la s�lection
   *
   * @return
   */
  public GrBoite getDomaineOnSelected() {
    GrBoite boite = null;
    for (ZCalqueAffichageDonneesInterface cq : scene_.getAllLayers()) {
      GrBoite boiteTmp = cq.getDomaineOnSelected();
      if (boite == null) {
        boite = boiteTmp;
      } else {
        boite.ajuste(boiteTmp);
      }
    }
    return boite;
  }

  public void zoomOn(GrBoite boite) {
    if (boite != null) {
      calquePanel_.getVueCalque().changeRepere(this, boite);
    }
  }
}
