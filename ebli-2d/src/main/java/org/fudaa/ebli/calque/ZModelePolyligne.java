/*
 *  @file         ZModelePolyligne.java
 *  @creation     2002-09-20
 *  @modification $Date: 2005-08-16 13:02:11 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import org.fudaa.ebli.geometrie.GrPolyligne;
/**
 * Le modele du calque d'affichage de polyligne.
 *
 * @version   $Id: ZModelePolyligne.java,v 1.5 2005-08-16 13:02:11 deniger Exp $
 * @author    Fred Deniger
 */
public interface ZModelePolyligne extends ZModeleDonnees {
  /**
   * Affecte a _p les valeurs du polyligne i.
   *
   * @param _p le polyligne modifie
   * @param _i index
   * @return true si affectation effectuee
   */
  boolean polyligne(GrPolyligne _p, int _i);
}
