/*
 * @creation 10 nov. 06
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.beans.PropertyChangeListener;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BConfigurableSectionInterface;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;

/**
 * @author fred deniger
 * @version $Id: BCalqueConfigureSectionAbstract.java,v 1.4 2007-05-04 13:49:42 deniger Exp $
 */
public abstract class BCalqueConfigureSectionAbstract implements BConfigurableSectionInterface,
    BSelecteurTargetInterface, BConfigurableInterface {

  protected final BCalque target_;
  protected final String title_;

  public BCalqueConfigureSectionAbstract(final BCalque _target, final String _title) {
    super();
    target_ = _target;
    title_ = _title;
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    if (target_ != null) {
      target_.addPropertyChangeListener(_key, _l);
    }
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }
  
  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    if (target_ != null) {
      target_.removePropertyChangeListener(_key, _l);
    }
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return this;
  }

  @Override
  public final String getTitle() {
    return title_;
  }

  @Override
  public void stopConfiguration() {}

}
