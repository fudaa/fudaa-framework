/*
 *  @creation     4 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import java.awt.Graphics2D;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.trace.TraceIcon;


/**
 * @author Fred Deniger
 * @version $Id: ZCalqueDeplacementTargetInterface.java,v 1.3.8.2 2008-05-13 12:10:24 bmarchan Exp $
 */
public interface ZCalqueDeplacementTargetInterface {

  /**
   * @param _g le graphique support
   * @param _dxEcran le dx ecran
   * @param _dyEcran le dx ecran
   * @param _ic l'icone pour dessiner le deplacement
   */
  void paintMove(final Graphics2D _g,int _dxEcran,int _dyEcran,TraceIcon _ic);
  
  
  /**
   * Translate les objets.
   * @param _dx le dx reel
   * @param _dy le dy reel
   * @param _dz le dz reel. S'applique au Z attribut du calque support.
   * @param _confirmOnZ true : Pour demander � l'utilisateur de confirmer le d�placement suivant Z.
   */
  void moved(double _dx,double _dy, double _dz, boolean _confirmOnZ);
  
  /**
   * @return le calque contenant la selection
   */
  ZScene getSupport();
}
