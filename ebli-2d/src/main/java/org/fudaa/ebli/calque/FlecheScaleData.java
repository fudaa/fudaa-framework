/*
 * @creation 21 mai 07
 * 
 * @modification $Date: 2007-06-20 12:23:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Font;
import java.awt.Graphics;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;

/**
 * @author fred deniger
 * @version $Id: FlecheScaleData.java,v 1.3 2007-06-20 12:23:12 deniger Exp $
 */
public class FlecheScaleData {

  public final static String PROP_IS_SAME_NORM_USED = "calquefleche.useSameNorm";
  public final static String PROP_MIN_NORM_TO_DRAW = "calquefleche.minNormeValueToDraw";
  public final static String PROP_PIXEL_FOR_RELATIVE = "calquefleche.ratioForPixel";
  public final static String PROP_PIXEL_FOR_RELATIVE_DEFAULT = "calquefleche.ratioForPixelDefault";
  public final static String PROP_PIXEL_SAME_NORM = "calquefleche.pixelForConstantNorm";
  public final static String PROP_PIXEL_SAME_NORM_DEFAULT = "calquefleche.pixelForConstantNormDefault";
  public final static String PROP_SCALE_FONT = "calquefleche.scaleFont";
  /**
   * The norm of the legend arrow to display
   */
  public final static String PROP_LEGEND_CONSTANT_NORM_VALUE = "calquefleche.legendFixRealNorm";
  public final static String PROP_LEGEND_USE_CONSTANT_NORM_VALUE = "calquefleche.useLegendFixRealNorm";

  public static String[] getScaleProperties() {
    return new String[] { PROP_IS_SAME_NORM_USED, PROP_PIXEL_FOR_RELATIVE, PROP_PIXEL_SAME_NORM, PROP_LEGEND_CONSTANT_NORM_VALUE,
        PROP_LEGEND_USE_CONSTANT_NORM_VALUE };
  }

  public static void resizeSegment(final GrSegment _s, final double _fac) {
    _s.e_.x_ = _fac * (_s.e_.x_ - _s.o_.x_) + _s.o_.x_;
    _s.e_.y_ = _fac * (_s.e_.y_ - _s.o_.y_) + _s.o_.y_;
  }

  double minNormeValueToDraw_;

  /**
   * Le nombre de pixel a utiliser dans le cas ou la norme est constante.
   */
  int pixelForConstantNorm_ = -1;

  /**
   * 1 pixel représente un certaine norme du vecteur.
   */
  double pixelPerUnit_ = -1;

  Double legendFixRealNorm = null;
  Boolean legendUseFixRealNorm = Boolean.FALSE;

  final GrSegment seg_ = new GrSegment(new GrPoint(), new GrPoint());

  final ZCalqueFleche support_;

  Font scaleFont_;

  boolean useSameNorm_;

  public FlecheScaleData(final ZCalqueFleche _support) {
    super();
    support_ = _support;
  }

  protected void initFactor() {
    if (pixelPerUnit_ <= 0) {
      setPixelPerUnit(getDefaultFacteur(support_.getGraphics()));
    }
  }

  protected void normalizeFleche(final GrSegment _realVect, GrMorphisme _versEcran) {
    final double lReal = _realVect.longueurXY();
    double lScreen = 0;
    if (useSameNorm_) {
      lScreen = pixelForConstantNorm_;
    } else {
      lScreen = pixelPerUnit_ * lReal;
    }
    _realVect.autoApplique(_versEcran);
    double lEcranInit = _realVect.longueurXY();
    // on veut que la taille final soit de finalLength donc:
    if (lEcranInit > 0) {
      resizeSegment(_realVect, lScreen / lEcranInit);
    }
  }

  protected void toReal(final GrSegment _seg) {
    if (useSameNorm_) {
      throw new IllegalAccessError("on ne doit pas appeler cette méthode si la taille est constante pour tous les vecteurs");
    }
    double lScreen = _seg.longueurXY();
    initFactor();
    double lReal = lScreen / pixelPerUnit_;
    _seg.autoApplique(support_.getVersReel());
    // on veut que la taille final soit lReal donc
    double lRealInit = _seg.longueurXY();
    if (lRealInit > 0) {
      resizeSegment(_seg, lReal / lRealInit);
    }

  }

  public double getDefaultFacteur() {
    return getDefaultFacteur(support_.getGraphics());
  }

  public double getDefaultFacteur(final Graphics _g) {
    final GrBoite b = support_.getClipReel(_g);
    int nombre = 0;
    if (support_.modele_!=null)
      nombre=support_.modele_.getNombre();
    
    double maxNormAuCarre = 0;
    for (int i = 0; i < nombre; i++) {
      // recuperation du polygone
      support_.modele_.segment(seg_, i, true);

      if (b == null || b.contientXY(seg_.o_)) {
        double normeAuCarre = seg_.longueurAuCarre();
        if (normeAuCarre > maxNormAuCarre)
          maxNormAuCarre = normeAuCarre;
      }
    }
    maxNormAuCarre = Math.sqrt(maxNormAuCarre);
    return maxNormAuCarre <= 0 ? 1D : (getDefaultSizeInPixelForRatio() / maxNormAuCarre);

  }

  public int getDefaultSizeInPixelForRatio() {
    int taille = Math.min(support_.getHeight(), support_.getWidth()) / 10;
    return taille < 10 ? 10 : taille;
  }

  public int getDefaultSizeInPixelForFixed() {
    int taille = Math.min(support_.getHeight(), support_.getWidth()) / 10;
    return taille < 10 ? 10 : taille;
  }

  public final double getMinNormValueToDraw() {
    return minNormeValueToDraw_;
  }

  public int getPixelForConstantNorm() {
    if (pixelForConstantNorm_ < 0)
      pixelForConstantNorm_ = getDefaultSizeInPixelForFixed();
    return pixelForConstantNorm_;
  }

  public double getPixelPerUnit() {
    return pixelPerUnit_;
  }

  public void initFrom(final EbliUIProperties _p) {
    if (_p.isDefined(PROP_IS_SAME_NORM_USED)) {
      useSameNorm_ = _p.getBoolean(PROP_IS_SAME_NORM_USED);
    }
    if (_p.isDefined(PROP_PIXEL_SAME_NORM)) {
      pixelForConstantNorm_ = _p.getInteger(PROP_PIXEL_SAME_NORM);
    }

    if (_p.isDefined(PROP_PIXEL_FOR_RELATIVE)) {
      pixelPerUnit_ = _p.getDouble(PROP_PIXEL_FOR_RELATIVE);
    }
    if (_p.isDefined(PROP_MIN_NORM_TO_DRAW)) {
      minNormeValueToDraw_ = _p.getDouble(PROP_MIN_NORM_TO_DRAW);
    }
    if (_p.isDefined(PROP_SCALE_FONT)) {
      scaleFont_ = (Font) _p.get(PROP_SCALE_FONT);
    }
    if (_p.isDefined(PROP_LEGEND_CONSTANT_NORM_VALUE)) {
      legendFixRealNorm = (Double) _p.get(PROP_LEGEND_CONSTANT_NORM_VALUE);
    }
    if (_p.isDefined(PROP_LEGEND_USE_CONSTANT_NORM_VALUE)) {
      legendUseFixRealNorm = (Boolean) _p.get(PROP_LEGEND_USE_CONSTANT_NORM_VALUE);
    }
  }

  public boolean isSameNorm() {
    // a continuer
    return useSameNorm_;
  } // mettre le tout dans une meme classe

  public void saveUIProperties(EbliUIProperties _cui) {
    _cui.put(PROP_IS_SAME_NORM_USED, useSameNorm_);
    _cui.put(PROP_PIXEL_SAME_NORM, pixelForConstantNorm_);
    _cui.put(PROP_PIXEL_FOR_RELATIVE, pixelPerUnit_);
    _cui.put(PROP_MIN_NORM_TO_DRAW, minNormeValueToDraw_);
    _cui.put(PROP_SCALE_FONT, scaleFont_);
    _cui.put(PROP_LEGEND_CONSTANT_NORM_VALUE, legendFixRealNorm);
    _cui.put(PROP_LEGEND_USE_CONSTANT_NORM_VALUE, legendUseFixRealNorm);
  }

  public final boolean setMinNormeValueToDraw(final double _minNormeValueToDraw) {
    if (minNormeValueToDraw_ != _minNormeValueToDraw) {
      final double old = minNormeValueToDraw_;
      minNormeValueToDraw_ = _minNormeValueToDraw;
      support_.repaint();
      support_.firePropertyChange(PROP_MIN_NORM_TO_DRAW, old, minNormeValueToDraw_);
      return true;
    }
    return false;
  }

  public boolean setPixelForConstantNorm(int _pixelForConstantNorm) {
    if (pixelForConstantNorm_ != _pixelForConstantNorm && _pixelForConstantNorm > 0) {
      final int old = pixelForConstantNorm_;
      pixelForConstantNorm_ = _pixelForConstantNorm;
      if (useSameNorm_) {
        support_.repaint();
      }
      support_.firePropertyChange(PROP_PIXEL_SAME_NORM, old, pixelForConstantNorm_);
      return true;
    }
    return false;
  }

  /**
   * Met a jour l'affichage avec le nouveau facteur d'echelle.
   * 
   * @param _newFactor doit etre strictement positif.
   */
  public boolean setPixelPerUnit(final double _newFactor) {
    if (_newFactor < 0 || _newFactor == pixelPerUnit_) {
      return false;
    }
    final double old = pixelPerUnit_;
    pixelPerUnit_ = _newFactor;
    support_.firePropertyChange(PROP_PIXEL_FOR_RELATIVE, old, pixelPerUnit_);
    support_.repaint();
    return true;
  }

  public boolean setUseSameNorm(final boolean _r) {
    if (useSameNorm_ != _r) {
      useSameNorm_ = _r;
      support_.firePropertyChange(PROP_IS_SAME_NORM_USED, !useSameNorm_, useSameNorm_);
      support_.repaint();
      return true;
    }
    return false;
  }

  public Font getScaleFont() {
    return scaleFont_;
  }

  public void setScaleFont(Font _scaleFont) {
    scaleFont_ = _scaleFont;
  }

  /**
   * @return the legendFixRealNorm
   */
  public Double getLegendFixRealNorm() {
    return legendFixRealNorm;
  }

  /**
   * @param legendFixRealNorm the legendFixRealNorm to set
   */
  public boolean setLegendFixRealNorm(Double legendFixRealNorm) {
    Double old = this.legendFixRealNorm;
    this.legendFixRealNorm = legendFixRealNorm;
    support_.firePropertyChange(PROP_LEGEND_CONSTANT_NORM_VALUE, old, legendFixRealNorm);
    return true;
  }

  /**
   * @return the useLegendFixRealNorm
   */
  public Boolean getLegendUseFixRealNorm() {
    return legendUseFixRealNorm;
  }

  /**
   * @param useLegendFixRealNorm the useLegendFixRealNorm to set
   */
  public boolean setLegendUseFixRealNorm(Boolean useLegendFixRealNorm) {
    Boolean old = this.legendUseFixRealNorm;
    this.legendUseFixRealNorm = useLegendFixRealNorm;
    support_.firePropertyChange(PROP_LEGEND_USE_CONSTANT_NORM_VALUE, old, this.legendUseFixRealNorm);
    return true;
  }

}
