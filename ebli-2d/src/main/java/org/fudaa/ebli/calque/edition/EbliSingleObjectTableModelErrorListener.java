/*
 * @creation     26 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.edition;

/**
 * Interface d'�coute des erreurs g�n�r�es par EbliSingleObjectTableModel.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface EbliSingleObjectTableModelErrorListener {
  
  /**
   * Appel� quand une erreur est d�tect� dans les donn�es.
   */
  public void modeleAdapterError(String _message);
  
  /**
   * Appel� quand l'erreur n'a plus de raison d'�tre affich�e.
   */
  public void modeleAdpaterNoError();
}
