/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.action;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;


/**
 * Une action pour joindre 2 polylignes � partir de leurs extremit�s. Valable uniquement sur des calques
 * {@link ZCalqueLigneBriseeEditable}. Ne fonctionne pas sur des polygones.
 * @author Bertrand Marchand
 */
public class SceneJoinAction extends EbliActionSimple implements ZSelectionListener {
  ZSceneEditor editor_;

  public SceneJoinAction(ZSceneEditor _editor) {
    super(EbliLib.getS("Joindre"), null, "GIS_JOIN");
    editor_=_editor;
    editor_.getScene().addSelectionListener(this);
    setDefaultToolTip(EbliLib.getS("Joindre 2 polylignes ou 2 multipoints d'un m�me calque"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    editor_.joinSelectedObjects();
  }

  @Override
  public void updateStateBeforeShow() {
    super.setEnabled(isActivable());
  }
  
  /**
   * Definit si l'action peut �tre activ�e.
   * @return true si c'est possible.
   */
  private boolean isActivable() {
    putValue(Action.NAME, EbliLib.getS("Joindre"));

    ZScene scn=editor_.getScene();
    SceneSelectionHelper hlp=editor_.getScene().getSelectionHelper();

    // On autorise la jonction que sur 2 g�om�tries d'un m�me calque.
    
    // La selection est vide
    if (scn.isSelectionEmpty()) return false;
    // Le nombre de geometries n'est pas egal � 2
    int[] idxgeom=hlp.getSelectedIndexes();
    if (idxgeom.length!=2) return false;
    // Les g�om�tries n'appartiennent pas au m�me calque.
    if (scn.getLayerForId(idxgeom[0])!=scn.getLayerForId(idxgeom[1])) return false;
    
    // Les g�om�tries sont 2 multipoints, ok quelque soit le mode.
    if (scn.getObject(idxgeom[0]) instanceof GISMultiPoint &&
        scn.getObject(idxgeom[1]) instanceof GISMultiPoint) {

      putValue(Action.NAME, EbliLib.getS("Joindre les multipoints"));
      return true;
    }
    
    // Les g�om�tries ne sont pas 2 polylignes
    if (!(scn.getObject(idxgeom[0]) instanceof GISPolyligne) ||
        !(scn.getObject(idxgeom[1]) instanceof GISPolyligne)) return false;
    
    // Le mode n'est pas sommet pour des polylignes
    if (!scn.isAtomicMode()) return false;
    // Le nombre de sommets selectionn� n'est pas �gal � 2
    if (hlp.getNbAtomicSelected()!=2) return false;

    putValue(Action.NAME, EbliLib.getS("Joindre les polylignes"));
    return true;
  }

  @Override
  public String getEnableCondition() {
    return EbliLib.getS("S�lectionner les sommets extremit�s de 2 polylignes ou deux semis de points");
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    updateStateBeforeShow();
  }
}
