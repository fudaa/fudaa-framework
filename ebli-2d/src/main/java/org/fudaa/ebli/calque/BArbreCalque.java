/**
 * @creation 1998-09-02 @modification $Date$ @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.*;
import java.util.EventObject;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPopupListener;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un menu pour le composant de cartographie utilisant des calques non bas�s sur des mod�le.
 *
 * @see EbliFilleCalques
 * @deprecated
 */
class BArbreNormalMenu extends BuMenu {

  /**
   * @param _arbre l'arbre de calques
   */
  BArbreNormalMenu(final BArbreCalqueModel _arbre) {
    super(EbliResource.EBLI.getString("Calques"), "CALQUES");
    addMenuItem(EbliResource.EBLI.getString("Visible"), "VISIBLE_OUI", null, true, 0, _arbre);
    addMenuItem(EbliResource.EBLI.getString("Cach�"), "VISIBLE_NON", null, true, 0, _arbre);
    addSeparator();
    addMenuItem(EbliResource.EBLI.getString("Att�nu�"), "ATTENUE_OUI", null, true, 0, _arbre);
    addMenuItem(EbliResource.EBLI.getString("Normal"), "ATTENUE_NON", null, true, 0, _arbre);
    addSeparator();
    addMenuItem(EbliResource.EBLI.getString("Rapide"), "RAPIDE_OUI", null, true);
    addMenuItem(EbliResource.EBLI.getString("Normal"), "RAPIDE_NON", null, true, 0, _arbre);
    addSeparator();
    addMenuItem(EbliResource.EBLI.getString("Gel�"), "GELE_OUI", null, true, 0, _arbre);
    addMenuItem(EbliResource.EBLI.getString("Actif"), "GELE_NON", null, true, 0, _arbre);
    addSeparator();
    addMenuItem(EbliResource.EBLI.getString("En premier"), "PREMIER", EbliResource.EBLI.getIcon("enpremier"), true, 0,
            _arbre);
    addMenuItem(CtuluLib.getS("Monter"), "MONTER", CtuluResource.CTULU.getIcon("monter"), true, 0, _arbre);
    addMenuItem(CtuluLib.getS("Descendre"), "DESCENDRE", CtuluResource.CTULU.getIcon("descendre"), true, 0, _arbre);
    addMenuItem(EbliResource.EBLI.getString("En dernier"), "DERNIER", EbliResource.EBLI.getIcon("endernier"), true, 0,
            _arbre);
    addSeparator();
    add(new JMenuItem(_arbre.actDelete_));
  }
}

class ZBArbreNormalMenu extends BuDynamicMenu {

  BArbreCalqueModel arbre_;
  boolean noLayer_;

  /**
   * Constructeur.
   *
   * @param _arbre l'arbre de calques
   */
  ZBArbreNormalMenu(final BArbreCalqueModel _arbre) {
    super(EbliResource.EBLI.getString("calques s�lectionn�s"), "LAYER_STATE");
    arbre_ = _arbre;
  }

  /**
   * *
   */
  @Override
  protected void build() {
    final BCalque c = arbre_.getSelectedCalque();
    if ((c == null) && (!noLayer_)) {
      noLayer_ = true;
      removeAll();
      addMenuItem(EbliLib.getS("Pas de calques s�lectionn�s"), "NO_LAYER", false);
    }
    if ((noLayer_ || (getMenuComponentCount() == 0)) && (c != null)) {
      removeAll();
      noLayer_ = false;
      arbre_.fillMenu(this);
    }
  }

  /**
   * *
   */
  @Override
  protected boolean isActive() {
    return true;
  }
}

/**
 * Un arbre de calques. Composant graphique de representation de la hierarchie des calques sous forme d'arbre.
 *
 * @version $Id$
 * @author Guillaume Desnoix
 */
public class BArbreCalque extends JTree implements InternalFrameListener, Observer {

  class ArbreCellEditor extends AbstractCellEditor implements TreeCellEditor {

    ArbreCell1 editor_;
    BCalque cqEdite_;
    // la largeur par defaut pour la case a cocher 'visible'
    int cbWidth_;
    // la hauteur par defaut pour la case a cocher 'visible'
    int cbHeight_;

    ArbreCellEditor() {
      editor_ = new ArbreCell1(true);
      editor_.cbVisible_.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(ItemEvent e) {
          if (cqEdite_ != null) {;
            cqEdite_.setUserVisible(editor_.cbVisible_.isSelected());
            fireEditingStopped();
          }
        }
      });
      editor_.r1_.addKeyListener(new KeyListener() {

        @Override
        public void keyPressed(final KeyEvent _e) {
        }

        @Override
        public void keyReleased(final KeyEvent _e) {
          if (_e.getKeyCode() == KeyEvent.VK_ENTER && editor_.r1_.getText() != null
                  && editor_.r1_.getText().trim().length() > 0) {
            cqEdite_.setTitle(editor_.r1_.getText());
            fireEditingStopped();
          }
        }

        @Override
        public void keyTyped(final KeyEvent _e) {
        }
      });
      editor_.r1_.addFocusListener(new FocusListener() {

        @Override
        public void focusGained(final FocusEvent _e) {
          editor_.r1_.setBackground(UIManager.getColor("TextField.background"));
          editor_.r1_.setForeground(Color.BLACK);
        }

        @Override
        public void focusLost(final FocusEvent _e) {
        }
      });

      // on recupere les dimensions par defaut
      final Dimension d = editor_.cbVisible_.getPreferredSize();
      cbHeight_ = d.height;
      cbWidth_ = d.width;

    }

    @Override
    public boolean isCellEditable(final EventObject _anEvent) {
      // le calque est mis a null s'il n'est pas edite
      if (_anEvent instanceof MouseEvent) {
        final MouseEvent e = (MouseEvent) _anEvent;
        if (e.isConsumed()) {
          return false;
        }
        if (e.getButton() != MouseEvent.BUTTON1) {
          return false;
        }
        final TreePath p = getClosestPathForLocation(e.getX(), e.getY());
        if (p != null) {
          final BCalque cq = (BCalque) p.getLastPathComponent();
          // on recupere le rectangle englobant de la cellule
          final Rectangle r = getRowBounds(getRowForPath(p));
          // calcul de l'enveloppe de la case a cocher
//          // la case a cocher est a droite ....
//          final int maxX = r.x + r.width;
//          final int minX = maxX - cbWidth_;
//          final int minY = r.y;
//          final int maxY = minY + cbHeight_;
//          if (e.getX() >= minX && e.getX() <= maxX && e.getY() >= minY && e.getY() <= maxY) {
//            return true;
//          }
//          // pour modifier le nom
//          else if (e.getClickCount() == 2 && (e.getX() > r.x + cq.getIconWidth()) && (e.getX() < minX)
//              && cq.isTitleModifiable()) {
//            return true;
//          }
          // la case a cocher est a gauche, pour plus de facilit� d'acc�s ....
          final int maxX = r.x + cbWidth_;
          final int minX = r.x;
          final int minY = r.y + 4;
          final int maxY = minY + cbHeight_;
          if (e.getX() >= minX && e.getX() <= maxX && e.getY() >= minY && e.getY() <= maxY) {
            return true;
          } // pour modifier le nom
          else if (e.getClickCount() == 2 && (e.getX() > r.x + cbWidth_ + cq.getIconWidth()) && (e.getX() < r.x + r.width)
                  && cq.isTitleModifiable()) {
            return true;
          }

        }
      }
      return false;
    }

    @Override
    public boolean shouldSelectCell(final EventObject _anEvent) {
      return false;
    }

    @Override
    public Object getCellEditorValue() {
      return null;
    }

    @Override
    protected void fireEditingStopped() {
      getArbreModel().getRootCalque().requestFocusInWindow();
      super.fireEditingStopped();
    }

    @Override
    public Component getTreeCellEditorComponent(final JTree _tree, final Object _value, final boolean _isSelected,
            final boolean _expanded, final boolean _leaf, final int _row) {
      cqEdite_ = (BCalque) _value;
      editor_.getTreeCellRendererComponent(_tree, _value, _isSelected, _expanded, _leaf, _row, true);
      editor_.r1_.setEditable(cqEdite_.isTitleModifiable());
      return editor_;
    }
  }

  public static BArbreNormalMenu buildNormalMenu(final BArbreCalqueModel _m) {
    return new BArbreNormalMenu(_m);
  }

  public static ZBArbreNormalMenu buildZNormalMenu(final BArbreCalqueModel _m) {
    return new ZBArbreNormalMenu(_m);
  }

  /**
   * construit un arbre editable sans modele.
   */
  public BArbreCalque() {
    this(true);
  }

  @Override
  public void setEditable(boolean flag) {
    boolean old = isEditable();
    if (old != flag) {
      super.setEditable(flag);

      if (getArbreModel() != null) {
        getArbreModel().setEditable(flag);

      }
    }
  }

  public BArbreCalque(final BArbreCalqueModel _m) {
    this(_m, true);
  }

  public BArbreCalque(final BArbreCalqueModel _m, final boolean _editable) {
    this(_editable);
    setModel(_m);
  }

  /**
   * @param _editable true si l'arbre doit etre editable.
   */
  public BArbreCalque(final boolean _editable) {
    setShowsRootHandles(true);
    setFocusable(false);
    setBorder(new EmptyBorder(5, 5, 5, 5));

    final ArbreCell1 c = new ArbreCell1(_editable);
    setCellRenderer(c);
    if (_editable) {
      setEditable(_editable);
      setCellEditor(new ArbreCellEditor());
    }

    c.doLayout();
    setRowHeight(35);
    addMouseListener(new PopupListener());
    setExpandsSelectedPaths(true);
  }

  class PopupListener extends EbliPopupListener {

    @Override
    public void popup(final Component _c, final int _xe, final int _ye) {
//      if (!BArbreCalque.this.isEditable()) {
//        return;
//      }
      int xe = _xe;
      int ye = _ye;
      final int selRow = getRowForLocation(xe, ye);
      if (selRow != -1) {

        final TreePath selPath = getPathForLocation(xe, ye);
        getSelectionModel().setSelectionPath(selPath);
        final Object selObject = selPath.getLastPathComponent();
        final JPopupMenu menu = buildPopupMenu((BCalque) selObject);
// B.M. On abandonne ce calcul cens� mieux positionner le popupmenu : Si l'arbre
// est dans la colonne gauche et que la fenetre appli couvre tout l'�cran, le
// menu est inutilisable.
//        xe = 20 - menu.getSize().width;
//        ye -= 5;
        menu.show(_c, xe, ye);
      }
    }
  }

  public JPopupMenu buildPopupMenu(final BCalque _calque) {
    final CtuluPopupMenu r = new CtuluPopupMenu();
    getArbreModel().fillPopupMenu(r);
    return r;
  }

  public ZBArbreNormalMenu buildZNormalMenu() {
    return new ZBArbreNormalMenu(getArbreModel());
  }

  public BArbreCalqueModel getArbreModel() {
    return (BArbreCalqueModel) getModel();
  }

  /**
   * Retourne le calque racine de l'arbre.
   */
  public BCalque getCalque() {
    if (getArbreModel() == null) {
      return null;
    }
    return getArbreModel().getRootCalque();
  }

  // private void setSelectionCalque(BCalque cq, TreePath tp) {
  // TreeModel model= getModel();
  // Object n= tp.getLastPathComponent();
  // TreePath tpo= tp;
  // int count= model.getChildCount(n);
  // for (int i= 0; i < count; i++) {
  // Object o= model.getChild(n, i);
  // tpo= tp.pathByAddingChild(o);
  // if (o == cq) {
  // expandPath(tpo);
  // setSelectionPath(tpo);
  // return;
  // } else
  // setSelectionCalque(cq, tpo);
  // }
  // }
  /**
   * Renvoie le calque selectionne.
   */
  public BCalque[] getSelection() {
    return getArbreModel().getSelection();
  }

  /**
   * Permet de mettre a jour le model de l'arbre.
   */
  @Override
  public void internalFrameActivated(final InternalFrameEvent _e) {
    final JInternalFrame f = _e.getInternalFrame();
    if (f instanceof EbliFilleCalquesInterface) {
      final BArbreCalqueModel c = ((EbliFilleCalquesInterface) f).getArbreCalqueModel();
      if (c != getModel()) {
        setModel(c);
      }
    }
  }

  @Override
  public void internalFrameClosed(final InternalFrameEvent _e) {
    final JInternalFrame f = _e.getInternalFrame();
    if (f instanceof EbliFilleCalquesInterface) {
      final BArbreCalqueModel c = ((EbliFilleCalquesInterface) f).getArbreCalqueModel();
      if (c == getModel()) {
        // maj
        getSelectionModel().clearSelection();
        setModel(null);
      }
    }
  }

  @Override
  public void internalFrameClosing(final InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameDeactivated(final InternalFrameEvent _e) {
    final JInternalFrame f = _e.getInternalFrame();
    if (f instanceof EbliFilleCalquesInterface) {
      final BArbreCalqueModel c = ((EbliFilleCalquesInterface) f).getArbreCalqueModel();
      if (c == getModel()) {
        getSelectionModel().clearSelection();
        setModel(null);
      }
    }
  }

  @Override
  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameIconified(final InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameOpened(final InternalFrameEvent _e) {
  }

  /**
   * Permet de rafraichir l'arbre. Appelle la methode refresh du model.
   */
  public void refresh() {
    if (getArbreModel() != null) {
      getArbreModel().refresh();
    }
  }

  /**
   * Met a jour le modele et la selection.
   *
   * @param _m le nouveau modele
   */
  public final void setModel(final BArbreCalqueModel _m) {
    if (getModel() != null) {
      ((BArbreCalqueModel) getModel()).getObservable().deleteObserver(this);
    }
    if (_m != null) {
      _m.getObservable().addObserver(this);
    }

    // pour eviter d'initialiser l'ancien model de selection
    super.setSelectionModel(null);
    super.setModel(_m);
    if (_m != null) {
      final TreePath p = _m.getTreeSelectionModel().getSelectionPath();
      super.setSelectionModel(_m.getTreeSelectionModel());
      if (p != null) {
        final TreePath par = p.getParentPath();
        if (par != null) {
          expandPath(par);
        }
      }
    }
  }

  /**
   * appelle setModel(BarbreCalqueModel).
   */
  @Override
  public void setModel(final TreeModel _m) {
    if (_m instanceof BArbreCalqueModel) {
      setModel((BArbreCalqueModel) _m);
    }
  }

  /**
   * Selectionne un claque dans l'arbre.
   */
  public void setSelectionCalque(final BCalque _cq) {
    getArbreModel().setSelectionCalque(_cq);
  }
  
  /**
   * Ouvre ou ferme l'arbre en totalit�.
   * @param _b True : Ouvre l'arbre
   */
  public void expandAllTree(boolean _b) {
//    enableEvents_=false;
    TreePath[] _paths=this.getSelectionPaths();
    this.clearSelection();

    if (_b) {
      for (int i=0; i < this.getRowCount(); i++) {
        TreePath path=this.getPathForRow(i);
        this.expandRow(i);
      }
    }
    else {
      for (int i=this.getRowCount() - 1; i >= 0; i--) {
        this.collapseRow(i);
      }
    }
    this.setSelectionPaths(_paths);
//    enableEvents_=true;
  }

  @Override
  public void updateUI() {
    super.updateUI();
    setRowHeight(35);
  }

  /**
   * Representation d'un calque dans un cellule de l'arbre. Elle associe au nom du calque son icone, et la dessine devant. Elle dessine aussi l'etat
   * du calque (attenue, rapide, gele) sous la forme d'une chaine de caracteres. Utilisee pour tous les lnk
   */
  public static class ArbreCell1 extends JPanel implements TreeCellRenderer {

    private final Color treeForeground_ = UIManager.getColor("Tree.foreground");
    private final Color treeBackground_ = UIManager.getColor("Tree.background");
    private final Color treeSelectionForeground_ = UIManager.getColor("Tree.selectionForeground");
    private final Color treeSelectionBakground_ = UIManager.getColor("Tree.selectionBackground");
    /**
     * Le calque est visible ou non
     */
    BuCheckBox3States cbVisible_;
    /**
     * Le calque contient ou non des selections.
     */
    JLabel lbSelect_;
    /**
     * Nom du calque
     */
    final JTextField r1_;
    /**
     * Etat du calque - gel�, rapide, etc...
     */
    final JLabel r2_;
    /**
     * Icone du calque
     */
    final JLabel r3_;
    /**
     * Le panneau � droite
     */
    final JPanel pnRight_;

    /**
     * @param _editable true si editable
     */
    public ArbreCell1(final boolean _editable) {
      r1_ = new BuTextField();
      r1_.setEditable(false);
      r1_.setOpaque(false);
      r1_.setBorder(null);
      r2_ = new JLabel(" ");
      r2_.setPreferredSize(new Dimension(80, 12));
      r3_ = new JLabel(" ");
      r3_.setPreferredSize(new Dimension(25, 25));
      lbSelect_ = new JLabel(EbliResource.EBLI.getIcon("selection.gif"));
      lbSelect_.setOpaque(false);
      JPanel pnLeft = new JPanel();
      pnLeft.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
      pnLeft.setLayout(new BuVerticalLayout(1, true, true));
      pnLeft.setOpaque(false);

      final JPanel r4 = new JPanel(new BuBorderLayout(2, 1, true, true));
      r4.add(r1_, BuBorderLayout.CENTER);
      r4.add(r2_, BuBorderLayout.SOUTH);
      r4.setOpaque(false);
      final BuBorderLayout lay = new BuBorderLayout(2, 1, true, true);
      lay.setYAlign(0);
      setLayout(lay);
      pnRight_ = new JPanel();
      pnRight_.setLayout(new BuBorderLayout(2, 1, true, true));
      pnRight_.add(r3_, BuBorderLayout.CENTER);
      pnRight_.add(r4, BuBorderLayout.EAST);
      pnRight_.setOpaque(false);

      if (_editable) {
        cbVisible_ = new BuCheckBox3States();
        cbVisible_.acceptOnly2StatesWhenClicked(true);
        // pour ne pas tout dessiner
        cbVisible_.setOpaque(false);
        cbVisible_.setToolTipText(EbliLib.getS("Visible"));
        cbVisible_.setVerticalAlignment(SwingConstants.TOP);
        cbVisible_.setHorizontalAlignment(SwingConstants.RIGHT);
        cbVisible_.setMargin(new Insets(5, 0, 0, 0));
        pnLeft.add(cbVisible_);
      }
      pnLeft.add(lbSelect_);
      add(pnLeft, BuBorderLayout.WEST);
      add(pnRight_, BuBorderLayout.CENTER);
      // B.M. : Supprim� pour le L&F Nimbus
//      setOpaque(true);
    }
    final BuLightBorder light_ = new BuLightBorder(BuLightBorder.RAISED, 1);

    @Override
    public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
            final boolean _expanded, final boolean _leaf, final int _row,
            final boolean _hasFocus) {
      r1_.setText(_value.toString());
      final Font ft = _tree.getFont();
      r1_.setFont(ft);
      r2_.setFont(BuLib.deriveFont(ft, -2));
      String calqueInfo = null;
      String longTitle = null;
      final StringBuffer s = new StringBuffer();
      if (_value instanceof BCalque) {
        final BCalque calque = (BCalque) _value;
        r3_.setIcon(calque);
        longTitle = calque.getLongTitle();
        // Si on souhaite pouvoir retailler l'icon du calque.
//        r3_.setIcon(CtuluLibImage.resize(calque,r3_.getPreferredSize().width,r3_.getPreferredSize().height));

        calqueInfo = (String) calque.getClientProperty("CalqueInfo");
        if (calqueInfo != null) {
          s.append(calqueInfo);
        }
        if (calque instanceof BCalqueAffichage) {

          final BCalqueAffichage ca = (BCalqueAffichage) calque;
          if (ca.isRapide()) {
            if (s.length() > 0) {
              s.append(CtuluLibString.ESPACE);
            }
            s.append(EbliResource.EBLI.getString("rapide"));
          }

          if (ca.isAttenue()) {
            if (s.length() > 0) {
              s.append(CtuluLibString.ESPACE);
            }
            s.append(EbliResource.EBLI.getString("att�nu�"));
          }
        }
        if (calque instanceof BCalqueInteraction) {
          final BCalqueInteraction ci = (BCalqueInteraction) calque;
          if (ci.isGele()) {
            if (s.length() > 0) {
              s.append(CtuluLibString.ESPACE);
            }
            s.append(EbliResource.EBLI.getString("gel�"));
          }
        }
        if (calque instanceof ZCalqueAffichageDonneesAbstract) {
          final ZCalqueAffichageDonneesAbstract cq = (ZCalqueAffichageDonneesAbstract) calque;
          lbSelect_.setVisible(!cq.isSelectionEmpty());

          if (!cq.isSelectable()) {
            if (s.length() > 0) {
              s.append(CtuluLibString.ESPACE);
            }
            s.append(EbliResource.EBLI.getString("nonsel."));
          }
        } else {
          lbSelect_.setVisible(false);
        }
        if (calque instanceof BGroupeCalque && calque.getCalques().length > 0) {
          final BGroupeCalque cq = (BGroupeCalque) calque;
          if (cq.isAllChildrenVisible()) {
            cbVisible_.setState(BuCheckBox3States.STATE_SELECTED);
          } else if (cq.isAllChildrenUnvisible()) {
            cbVisible_.setState(BuCheckBox3States.STATE_DESELECTED);
          } else {
            cbVisible_.setState(BuCheckBox3States.STATE_MIXED);
          }
        } else if (!calque.isUserVisible()) {
          if (s.length() > 0) {
            s.append(CtuluLibString.ESPACE);
          }
          s.append(EbliResource.EBLI.getString("cach�"));
          // si l'edition est validee on met a jour l'etat
          if (cbVisible_ != null) {
            cbVisible_.setState(BuCheckBox3States.STATE_DESELECTED);
          }
        } // si l'edition est validee on met a jour l'etat
        else if (cbVisible_ != null) {
          cbVisible_.setState(BuCheckBox3States.STATE_SELECTED);
        }
      }
      r2_.setText(s.toString());
      if (_selected) {
        final Color bg = treeSelectionBakground_;
        final Color fg = treeSelectionForeground_;
        setBackground(bg);
        setForeground(fg);
        r1_.setBackground(bg);
        r1_.setForeground(fg);
        r2_.setForeground(fg);
        r3_.setForeground(fg);
        // B.M. : Opacit� necessaire pour le L&F Nimbus qui ne semble pas gerer correctement le treeBackground_
        setOpaque(true);
        r1_.setOpaque(true);
      } else {
        final Color bg = treeBackground_;
        final Color fg = treeForeground_;
        setBackground(bg);
        setForeground(fg);
        r1_.setBackground(bg);
        r1_.setForeground(fg);
        r2_.setForeground(fg);
        r3_.setForeground(fg);
        // B.M. : Opacit� necessaire pour le L&F Nimbus qui ne semble pas gerer correctement le treeBackground_
        setOpaque(false);
        r1_.setOpaque(false);
      }
      final StringBuffer tooltip = new StringBuffer(60);
      tooltip.append("<html><body style=\"margin:1px;\">");
      final JComponent cq = (JComponent) _value;
      final String link = CtuluLibSwing.getHelpUrl(cq);
      if (link == null) {
        if (longTitle == null) {
          tooltip.append(_value.toString());
        } else {
          tooltip.append(longTitle);
        }
      } else {
        tooltip.append("<a href=\"").append(link).append("\">").append(_value.toString()).append("</a>");
      }

      final String text = r2_.getText();
      if (text.length() > 0) {
        tooltip.append("<br>").append(text);
      }

      final Object val = cq.getClientProperty(Action.SHORT_DESCRIPTION);
      if (val != null) {
        tooltip.append("<br>").append(val.toString());
      }
      tooltip.append("</body></html>");
      final String str = tooltip.toString();
      ArbreCell1.this.setToolTipText(str);
      r1_.setToolTipText(str);
      r2_.setToolTipText(str);
      r3_.setToolTipText(str);
      if (cbVisible_ != null) {
        cbVisible_.setToolTipText(str);
        cbVisible_.setEnabled(_tree.isEditable());
      }
      return this;
    }
  }

  /**
   * Representation d'un calque dans une cellule de l'arbre. Elle est toute simple: nom du calque dans un label. Utilisee dans les looks Windows et
   * Motif
   */
  public static class ArbreCellLabel extends JLabel implements TreeCellRenderer {

    Color selectedBack_ = UIManager.getColor("Tree.selectionBackground");
    Color selectedFor_ = UIManager.getColor("Tree.selectionForeground");

    public ArbreCellLabel() {
      this.setOpaque(true);
      setPreferredSize(new Dimension(120, 25));
    }

    @Override
    public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
            final boolean _expanded, final boolean _leaf, final int _row,
            final boolean _hasFocus) {
      this.setFont(_tree.getFont());
      setIcon((Icon) _value);
      setText(_value.toString());
      if (_selected) {
        setBackground(selectedBack_);
        setForeground(selectedFor_);
      } else {
        setBackground(UIManager.getColor("Tree.background"));
        setForeground(UIManager.getColor("Tree.foreground"));
      }
      return this;
    }
  }

  /*
   * (non-Javadoc) @see java.util.Observer#update(java.util.Observable, java.lang.Object)
   */
  @Override
  public void update(Observable o, Object arg) {
    repaint();
  }
}
/**
 * Modele de donnees pour l'arbre de calques.
 */
