/*
 * @creation 9 nov. 06
 * 
 * @modification $Date: 2007-06-05 08:58:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurSpinner;

/**
 * @author fred deniger
 * @version $Id: ZCalqueFlecheGrilleSection.java,v 1.1 2007-06-05 08:58:38 deniger Exp $
 */
public class ZCalqueFlecheDensiteSection extends BCalqueConfigureSectionAbstract {

  public ZCalqueFlecheDensiteSection(final ZCalqueFleche _target) {
    super(_target, EbliLib.getS("Densit�"));
  }

  FlecheGrilleData getGrilleData() {
    return ((ZCalqueFleche) target_).grille_;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final BSelecteurCheckBox cbActive = new BSelecteurCheckBox(FlecheGrilleData.PROP_DENSITE_IS_ACTIVE);
    cbActive.setTitle(EbliLib.getS("Activ�e"));
    cbActive.setTooltip(EbliLib.getS("Afficher une fraction les vecteurs"));
    cbActive.setAddListenerToTarget(false);
    final BSelecteurSpinner tfDensity = new BSelecteurSpinner(FlecheGrilleData.PROP_DENSITE_INCREMENT, new JSpinner(new SpinnerNumberModel(100, 1,
        100, 1)));
    tfDensity.setTitle(EbliLib.getS("Densit�"));
    tfDensity.setTooltip(EbliLib.getS("Densit�"));
    cbActive.getCb().addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        final boolean ok = cbActive.getCb().isSelected();
        tfDensity.setEnabled(ok);
      }

    });
    tfDensity.setEnabled(cbActive.getCb().isSelected());
    return new BSelecteurInterface[] { cbActive, tfDensity };
  }

  @Override
  public Object getProperty(final String _key) {

    if (FlecheGrilleData.PROP_DENSITE_IS_ACTIVE == _key) {
      return Boolean.valueOf(getGrilleData().isDensiteActive());
    }
    if (FlecheGrilleData.PROP_DENSITE_INCREMENT == _key) {
      return new Integer(getGrilleData().getDensiteIncrement());
    }
    return null;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (FlecheGrilleData.PROP_DENSITE_IS_ACTIVE == _key) {
      return getGrilleData().setDensiteActive(((Boolean) _newProp).booleanValue());
    }
    if (FlecheGrilleData.PROP_DENSITE_INCREMENT == _key) {
      return getGrilleData().setDensitePourcentage(((Integer) _newProp).intValue());
    }
    return false;
  }
}
