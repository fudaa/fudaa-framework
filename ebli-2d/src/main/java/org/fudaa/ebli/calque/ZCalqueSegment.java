/**
 * @creation 10 juin 2004
 * @modification $Date: 2008-02-20 10:16:02 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliSelectionMode;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.table.AbstractTableModel;
import java.awt.*;

/**
 * @author Fred Deniger
 * @version $Id: ZCalqueSegment.java,v 1.44.4.1 2008-02-20 10:16:02 bmarchan Exp $
 */
public class ZCalqueSegment extends ZCalqueAffichageDonneesLineAbstract {
  /**
   * @author Fred Deniger
   * @version $Id: ZCalqueSegment.java,v 1.44.4.1 2008-02-20 10:16:02 bmarchan Exp $
   */
  public static class InfoString {
    public boolean addZinfo_;
    public CtuluNumberFormatI fmt_ = CtuluLib.DEFAULT_NUMBER_FORMAT;
    public String milieu_ = EbliLib.getS("Milieu");
    public String nbSegment_ = EbliLib.getS("Nombre de segments");
    public String nbSegmentSelectionnees_ = EbliLib.getS("Nombre de segments s�lectionn�s");
    public String norme_ = EbliLib.getS("Norme");
    public String pt1_ = EbliLib.getS("Point") + CtuluLibString.ESPACE + CtuluLibString.UN;
    public String pt2_ = EbliLib.getS("Point") + CtuluLibString.ESPACE + CtuluLibString.DEUX;
    public String selonX_ = EbliLib.getS("Selon X");
    public String selonY_ = EbliLib.getS("Selon Y");
    public String titre_ = EbliLib.getS("Segments");
    public String titreIfOne_ = EbliLib.getS("Segment n�");
  }

  /**
   * @author Fred Deniger
   * @version $Id: ZCalqueSegment.java,v 1.44.4.1 2008-02-20 10:16:02 bmarchan Exp $
   */
  public static class SegmentValueTableModel extends AbstractTableModel {
    final ZModeleSegment segModel_;

    /**
     * @param _seg
     */
    public SegmentValueTableModel(final ZModeleSegment _seg) {
      super();
      segModel_ = _seg;
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      return _columnIndex == 0 ? Integer.class : Double.class;
    }

    @Override
    public int getColumnCount() {
      return 8;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return EbliLib.getS("Indices");
      }
      if (_column == 1) {
        return "X1";
      }
      // Y
      if (_column == 2) {
        return "Y1";
      }
      if (_column == 3) {
        return EbliLib.getS("X2");
      }
      if (_column == 4) {
        return EbliLib.getS("Y2");
      }
      if (_column == 5) {
        return EbliLib.getS("Selon X");
      }
      if (_column == 6) {
        return EbliLib.getS("Selon Y");
      }
      if (_column == 7) {
        return EbliLib.getS("Norme");
      }

      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return segModel_ == null ? 0 : segModel_.getNombre();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return new Integer(_rowIndex + 1);
      }
      final double x1 = segModel_.getX(_rowIndex);
      final double y1 = segModel_.getY(_rowIndex);
      if (_columnIndex == 1) {
        return CtuluLib.getDouble(x1);
      }
      // Y
      if (_columnIndex == 2) {
        return CtuluLib.getDouble(y1);
      }
      final double vx = segModel_.getVx(_rowIndex);
      if (_columnIndex == 3) {
        return CtuluLib.getDouble(x1 + vx);
      }
      final double vy = segModel_.getVy(_rowIndex);
      if (_columnIndex == 4) {
        return CtuluLib.getDouble(y1 + vy);
      }
      if (_columnIndex == 5) {
        return CtuluLib.getDouble(vx);
      }
      if (_columnIndex == 6) {
        return CtuluLib.getDouble(vy);
      }
      if (_columnIndex == 7) {
        return CtuluLib.getDouble(segModel_.getNorme(_rowIndex));
      }
      // la valeur au point demande (si non null)
      return null;
    }
  }

  public static void fillWithInfo(final InfoData _d, final int _nbSelected, final int _idxSelected,
                                  final ZModeleSegment _seg, final InfoString _info) {
    _d.put(_info.nbSegment_, CtuluLibString.getString(_seg.getNombre()));
    _d.put(_info.nbSegmentSelectionnees_, CtuluLibString.getString(_nbSelected));
    if (_nbSelected == 1) {
      final int idx = _idxSelected;
      final double x1 = _seg.getX(idx);
      final double y1 = _seg.getY(idx);
      final double vx = _seg.getVx(idx);
      final double vy = _seg.getVy(idx);
      final boolean zInfo = _info.addZinfo_;
      final double z1 = zInfo ? _seg.getZ1(idx) : 0;
      final double z2 = zInfo ? _seg.getZ2(idx) : 0;
      final String sep = ", ";
      String msg = _info.fmt_.format(x1) + sep + _info.fmt_.format(y1);
      if (zInfo) {
        msg += sep + _info.fmt_.format(z1);
      }
      _d.put(_info.pt1_, msg);
      msg = _info.fmt_.format(x1 + vx) + " , " + _info.fmt_.format(y1 + vy);
      if (zInfo) {
        msg += sep + _info.fmt_.format(z2);
      }
      _d.put(_info.pt2_, msg);
      if (_info.norme_ != null) {
        _d.put(_info.norme_, _info.fmt_.format(_seg.getNorme(idx)));
      }
      if (_info.milieu_ != null) {
        String value = _info.fmt_.format(x1 + vx / 2) + " , " + _info.fmt_.format(y1 + vy / 2);
        if (zInfo) {
          value += " , " + _info.fmt_.format((z1 + z2) / 2);
        }
        _d.put(_info.milieu_, value);
      }
      if (_info.selonX_ != null) {
        _d.put(_info.selonX_, _info.fmt_.format(vx));
      }
      if (_info.selonY_ != null) {
        _d.put(_info.selonY_, _info.fmt_.format(vy));
      }
      _d.setTitle(_info.titreIfOne_ + CtuluLibString.getEspaceString(idx + 1));
    } else {
      _d.setTitle(_info.titre_);
    }
  }

  protected ZModeleSegment modele_;
  protected final GrSegment seg_ = new GrSegment(new GrPoint(), new GrPoint());

  public ZCalqueSegment() {
    this(null);
  }

  /**
   * @param _modele
   */
  public ZCalqueSegment(final ZModeleSegment _modele) {
    modele_ = _modele;
    iconModel_ = new TraceIconModel(TraceIcon.RIEN, 1, Color.BLACK);
  }

  protected int getNombreSegment() {
    return modele_ == null ? 0 : modele_.getNombre();
  }

  protected final Color getPaletteColorFor(final GrSegment _s) {
    // BM : Evite les traceback si la palette n'est pas une palette plage. 
    if (paletteCouleur_ instanceof BPalettePlage) {
      return ((BPalettePlage) paletteCouleur_).getColorFor(_s.longueurXY());
    } else {
      return ligneModel_.getCouleur();
    }
  }

  public final Color getPaletteColorFor(final double _l) {
    // BM : Evite les traceback si la palette n'est pas une palette plage. 
    if (paletteCouleur_ instanceof BPalettePlage) {
      return ((BPalettePlage) paletteCouleur_).getColorFor(_l);
    } else {
      return ligneModel_.getCouleur();
    }
  }

  public final boolean isPaletteUsed() {
    return (paletteCouleur_ != null) && isPaletteCouleurUsed_;
  }

  protected void toEcran(final GrSegment _seg, final GrMorphisme _versEcran) {
    _seg.autoApplique(_versEcran);
  }

  /**
   * @param _ligne le trace ligne a modifier. La couleur de ligne en retour peut �tre invisible.
   * @param _icone le trace d'icone a modifier
   * @param _s le segment concerne
   * @param _idx l'index
   * @return true si modification
   */
  protected boolean updateTraceLigne(final TraceLigneModel _ligne, final TraceIconModel _icone, final GrSegment _s,
                                     final int _idx) {
    if (isPaletteUsed()) {
      _ligne.setCouleur(EbliLib.getAlphaColor(getPaletteColorFor(_s), alpha_));
      return true;
    }
    return false;
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    // on initialise le min/max avec la norme pour i=0;
    final GrSegment s = new GrSegment(new GrPoint(), new GrPoint());
    _b.setToNill();
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      if (modele_.segment(s, i, true)) {
        _b.expandTo(s.longueurXY());
      }
    }
    return true;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return false;
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    int m = selection_.getMaxIndex();
    if (m > modele_.getNombre()) {
      m = modele_.getNombre() - 1;
    }
    final GrBoite r = new GrBoite();
    for (int i = selection_.getMinIndex(); i <= m; i++) {
      if (selection_.isSelected(i)) {
        // recuperation du polygone
        modele_.segment(seg, i, true);
        r.ajuste(seg.e_);
        r.ajuste(seg.o_);
      }
    }
//    ajusteZoomOnSelected(r);
    return r;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      if (_p.isDefined("calque.ligneModel")) {
        setLineModel(0, (TraceLigneModel) _p.get("calque.ligneModel"));
      }
    }
  }

  public boolean isConfigurable() {
    return true;
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return modele_;
  }

  /**
   * Dessine les donnees uniquement.
   *
   * @param _g la cible
   */
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
                           final GrBoite _clipReel) {
    if (getNombreSegment() == 0) {
      return;
    }
    final int nombre = getNombreSegment();
    // le clip d'affichage
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final TraceIcon icone = iconModel_ == null ? null : new TraceIcon(new TraceIconModel(iconModel_));
    final TraceLigne tl = new TraceLigne(new TraceLigneModel(ligneModel_));
    final TraceIconModel icModel = icone == null ? null : icone.getModel();
    final TraceLigneModel lgModel = tl.getModel();
    if (isAttenue()) {
      tl.setCouleur(attenueCouleur(tl.getCouleur()));
      if (icone != null) {
        icone.setCouleur(attenueCouleur(icone.getCouleur()));
      }
    }
    int w = getWidth() + 1;
    CtuluListSelection memory = new CtuluListSelection(w * getHeight());
    for (int i = 0; i < nombre; i++) {
      // recuperation du polygone

      if (modele_.segment(seg_, i, true)) {

        // si la palette est utilise et s'il n'y a pas de couleur pour le segment on passe.
        if (isPaletteUsed() && getPaletteColorFor(seg_) == null) {
          continue;
        }
        // recuperation de la boite dans bPoly
        // Si la boite du polygone n'est pas dans la boite d'affichage on passe
        if (clip.intersectXYBoite(seg_)) {
          seg_.autoApplique(versEcran);
          boolean onePoint = (((int) seg_.e_.x_) == ((int) seg_.o_.x_)) && (((int) seg_.e_.y_) == ((int) seg_.o_.y_));
          if (onePoint) {
            int idxDone = (int) seg_.e_.x_ + (int) seg_.e_.y_ * w;
            if (idxDone >= 0) {
              if (!memory.isSelected(idxDone)) {
                memory.add(idxDone);
              } else {
                //on ne dessine pas 2 fois si sur meme point.
                continue;
              }
            }
          }
          updateTraceLigne(lgModel, icModel, seg_, i);
          tl.dessineTrait(_g, seg_.e_.x_, seg_.e_.y_, seg_.o_.x_, seg_.o_.y_);
          if (icone != null) {
            icone.paintIconCentre(_g, seg_.e_.x_, seg_.e_.y_);
            icone.paintIconCentre(_g, seg_.o_.x_, seg_.o_.y_);
          }
        }
      }
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
                               final GrBoite _clipReel) {
    if (isSelectionEmpty() || isRapide()) {
      return;
    }
    GrBoite domaine = getDomaine();
    if (domaine == null) {
      return;
    }
    final GrBoite clip = _clipReel;

    if (!domaine.intersectXY(clip)) {
      return;
    }
    final GrMorphisme versEcran = _versEcran;
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    _g.setColor(cs);
    // tlSelection_.setEpaisseur(2f);
    final int nb = Math.min(modele_.getNombre() - 1, selection_.getMaxIndex());
    for (int i = nb; i >= 0; i--) {
      if (!selection_.isSelected(i)) {
        continue;
      }
      modele_.segment(seg_, i, true);
      if (clip.intersectXYBoite(seg_)) {
        toEcran(seg_, versEcran);
        tlSelection.dessineTrait(_g, seg_.e_.x_, seg_.e_.y_, seg_.o_.x_, seg_.o_.y_);
        if (ic != null) {
          ic.paintIconCentre(this, _g, seg_.e_.x_, seg_.e_.y_);
          ic.paintIconCentre(this, _g, seg_.o_.x_, seg_.o_.y_);
        }
      }
    }
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    if (ligneModel_ != null) {
      res.put("calque.ligneModel", new TraceLigneModel(ligneModel_));
    }
    return res;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    final GrBoite bClip = getDomaine();
    if (!isVisible() || bClip==null) {
      return null;
    }

    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }
    final GrSegment poly = seg_;
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      modele_.segment(poly, i, true);
      if (poly.distanceXY(_pt) <= distanceReel) {
        final CtuluListSelection r = creeSelection();
        r.add(i);
        return r;
      }
    }
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _polySelection, final int _mode) {
    final GrBoite domaineBoite = getDomaine();
    if (modele_.getNombre() == 0 || !isVisible() ||domaineBoite==null) {
      return null;
    }
    final Envelope polyEnv = _polySelection.getEnvelopeInternal();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final CtuluListSelection r = creeSelection();
    final Coordinate c = new Coordinate();
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_polySelection);
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      modele_.segment(seg, i, true);
      boolean selected = false;
      // on teste sur les enveloppes ->rapide
      if (_mode == EbliSelectionMode.MODE_ALL) {
        if ((polyEnv.contains(seg.e_.x_, seg.e_.y_)) && (polyEnv.contains(seg.o_.x_, seg.o_.y_))) {
          c.x = seg.e_.x_;
          c.y = seg.e_.y_;
          if (GISLib.isSelectedEnv(c, _polySelection, polyEnv, tester)) {
            c.x = seg.o_.x_;
            c.y = seg.o_.y_;
            selected = GISLib.isSelectedEnv(c, _polySelection, polyEnv, tester);
          }
        }
      } else if (_mode == EbliSelectionMode.MODE_ONE) {
        c.x = seg.e_.x_;
        c.y = seg.e_.y_;
        selected = GISLib.isSelectedEnv(c, _polySelection, polyEnv, tester);
        if (!selected) {
          c.x = seg.o_.x_;
          c.y = seg.o_.y_;
          selected = GISLib.isSelectedEnv(c, _polySelection, polyEnv, tester);
        }
      } else if (_mode == EbliSelectionMode.MODE_CENTER) {
        c.x = seg.getMidX();
        c.y = seg.getMidY();
        selected = GISLib.isSelectedEnv(c, _polySelection, polyEnv, tester);
      }

      if (selected) {
        r.add(i);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  @Override
  public LineString getSelectedLine() {
    if (getNbSelected() != 2) {
      return null;
    }
    final Coordinate[] cs = new Coordinate[2];
    int idx = getLayerSelection().getMinIndex();
    cs[0] = new Coordinate(modele_.getX(idx) + modele_.getVx(idx) / 2, modele_.getY(idx) + modele_.getVy(idx) / 2);
    idx = getLayerSelection().getMaxIndex();
    cs[1] = new Coordinate(modele_.getX(idx) + modele_.getVx(idx) / 2, modele_.getY(idx) + modele_.getVy(idx) / 2);
    if (cs[0].compareTo(cs[1]) > 0) {
      final Coordinate tmp = cs[0];
      cs[0] = cs[1];
      cs[1] = tmp;
    }
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  /**
   * @param _s le nouveau modele
   */
  public void setModele(final ZModeleSegment _s) {
    clearSelection();
    modele_ = _s;
    repaint();
  }
}
