/*
 * @creation 9 nov. 06
 * 
 * @modification $Date: 2007-06-05 08:58:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurIconModel;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.controle.BSelecteurSurfaceModel;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceSurfaceModel;

/**
 * @author fred deniger
 * @version $Id: ZCalqueAffichageDonneesTraceConfigure.java,v 1.3 2007-06-05 08:58:38 deniger Exp $
 */
public class ZCalqueAffichageDonneesTraceConfigure extends BCalqueConfigureSectionAbstract {

  final int idx_;
  String propIcon_;
  String propLine_;
  String propSurf_;

  public ZCalqueAffichageDonneesTraceConfigure(final ZCalqueAffichageDonneesAbstract _target, final int _idx) {
    super(_target, _target.getNbSet() == 1 ? EbliLib.getS("Style") : _target.getSetTitle(_idx));
    idx_ = _idx;
    propIcon_ = BSelecteurIconModel.getProperty(idx_);
    if (_target.getLineModel(idx_) != null)
      propLine_ = BSelecteurLineModel.getProperty(idx_);
    if (_target.getSurfaceModel(_idx) != null)
      propSurf_ = BSelecteurSurfaceModel.getProperty(idx_);
  }

  protected ZCalqueAffichageDonneesAbstract getCq() {
    return (ZCalqueAffichageDonneesAbstract) target_;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (propIcon_.equals(_key)) {
      return getCq().setIconModel(idx_, (TraceIconModel) _newProp);
    } else if (propLine_.equals(_key)) {
      return getCq().setLineModel(idx_, (TraceLigneModel) _newProp);
    } else {
      return getCq().setSurfaceModel(idx_, (TraceSurfaceModel) _newProp);
    }
  }

  private boolean showColor = true;
  private boolean showIconType = true;

  public boolean isShowIconType() {
    return showIconType;
  }

  public void setShowIconType(boolean showIconType) {
    this.showIconType = showIconType;
  }

  public boolean isShowColor() {
    return showColor;
  }

  public void setShowColor(boolean showColor) {
    this.showColor = showColor;
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key.equals(propIcon_)) {
      return getCq().getIconModel(idx_);
    } else if (_key.equals(propLine_)) {
      return getCq().getLineModel(idx_);
    } else {
      return getCq().getSurfaceModel(idx_);
    }
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {

    int nbsel = (propIcon_ == null ? 0 : 1) + (propLine_ == null ? 0 : 1) + (propSurf_ == null ? 0 : 1);
    BSelecteurInterface[] sel = new BSelecteurInterface[nbsel];

    int isel = 0;
    if (propIcon_ != null) {
      final BSelecteurIconModel selecteurIconModel = new BSelecteurIconModel(propIcon_);
      selecteurIconModel.setTitle(EbliLib.getS("Trac� des points"));
      selecteurIconModel.setShowColor(showColor);
      selecteurIconModel.setShowType(showIconType);
      sel[isel++] = selecteurIconModel;
    }

    if (propLine_ != null) {
      final BSelecteurLineModel selecteurLineModel = new BSelecteurLineModel(propLine_);
      selecteurLineModel.setTitle(EbliLib.getS("Trac� des lignes"));
      selecteurLineModel.setAddColor(showColor);
      sel[isel++] = selecteurLineModel;
    }

    if (propSurf_ != null) {
      final BSelecteurSurfaceModel selecteurSurfaceModel = new BSelecteurSurfaceModel(propSurf_);
      selecteurSurfaceModel.setTitle(EbliLib.getS("Trac� des surfaces"));
      sel[isel++] = selecteurSurfaceModel;
    }
    return sel;
  }
}
