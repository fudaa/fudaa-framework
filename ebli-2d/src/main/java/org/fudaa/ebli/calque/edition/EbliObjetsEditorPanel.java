/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.ebli.calque.edition;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gui.CtuluValuesEditorPanel;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Un editeur pour plusieurs objets d'un meme calque.
 * Cet editeur permet de modifier les valeurs des géométries, mais aussi de
 * trier les géométries dans le calque.
 * 
 * @author marchand@deltacad.fr
 */
public class EbliObjetsEditorPanel extends CtuluValuesEditorPanel {
  
  /**
   * Constructeur.
   * @param _model Le modèle éditable contenant les géométries.
   * @param _idx Les indices des objets selectionnés.
   * @param _cmd Le manager undo/redo
   */
  public EbliObjetsEditorPanel(final ZModeleEditable _model, final int[] _idx, final CtuluCommandContainer _cmd, EbliCoordinateDefinition[] _coordDefs, ZEditorValidatorI _validator) {
    super(new EbliObjetsMultiEditorPanel(_model,_idx, _coordDefs, _validator) ,_cmd);
    decorePanel(_model.getNombre()==_idx.length);
  }
  
  /**
   * Ajoute au panneau une checkbox pour switcher de la selection a toutes les géométries
   * @param _allSelected True : Toutes les géométries du calque sont selectionnées.
   */
  private void decorePanel(boolean _allSelected) {
    final JCheckBox cbAllGeom=new JCheckBox(EbliLib.getS("Sur toutes les géométries du calque"),false);
    cbAllGeom.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        ((EbliObjetsMultiEditorPanel)multi_).setFiltered(cbAllGeom.isSelected());
        if (common_!=null) common_.setIdx(((EbliObjetsMultiEditorPanel)multi_).getSelectedIndexes());
      }
    });
    cbAllGeom.setSelected(_allSelected);
    pnNorth_.add(cbAllGeom);
  }
  
  /**
   * @return True Si des valeurs sont éditables.
   */
  public boolean hasEditableData() {
    return ((EbliObjetsMultiEditorPanel)multi_).hasEditableData();
  }
}
