/**
 * @creation     1998-08-31
 * @modification $Date: 2008-03-26 14:41:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceSurface;
/**
 * Une classe de base pour les objets dessin. Elle �tend la classe GrObjet
 * pour �tre d�placable int�ractivement, et implemente l'interface
 * GrContour pour pouvoir �tre s�lectionnable.
 *
 * @version $Revision: 1.11.8.1 $ $Date: 2008-03-26 14:41:09 $ by $Author: bmarchan $
 * @author  Axel von Arnim, Bertrand Marchand
 * @see     org.fudaa.ebli.geometrie.GrContour
 */
public abstract class DeForme extends GrObjet implements GrContour {
  // donnees membres publiques
  public final static int TRAIT= 0;
  public final static int LIGNE_BRISEE= 1;
  public final static int POLYGONE= 2;
  public final static int RECTANGLE= 3;
  public final static int CARRE= 4;
  public final static int ELLIPSE= 5;
  public final static int CERCLE= 6;
  public final static int MAIN_LEVEE= 7;
  public final static int COURBE_FERMEE= 8;
  public final static int TEXTE= 9;
  public final static int POINT= 10;
  public final static int MULTI_POINT=11;
  
  // donnees membres privees
  int typeTrait_;
  float epaisseurTrait_;
  int typeSurface_;
  Color couleurContour_;
  Color couleurRemplissage_;
  // Constructeurs
  protected DeForme() {
    couleurContour_= Color.black;
    couleurRemplissage_= null;
    typeTrait_= TraceLigne.LISSE;
    typeSurface_= TraceSurface.UNIFORME;
    epaisseurTrait_= 1f;
  }
  public abstract int getForme();
  // Methodes publiques
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    _t.setTypeTrait(typeTrait_, epaisseurTrait_);
    _t.setForeground(couleurContour_);
    //    _t.setCouleurContour(couleurContour_);
    _t.setTypeSurface(typeSurface_);
    if (couleurRemplissage_ != null) {
      _t.setBackground(couleurRemplissage_);
      //      _t.setCouleurRemplissage(couleurRemplissage_);
    }
  }
  public abstract GrObjet getGeometrie();
  public Color getForeground() {
    return couleurContour_;
  }
  public void setForeground(final Color _c) {
    couleurContour_= _c;
  }
  public Color getBackground() {
    return couleurRemplissage_;
  }
  public void setBackground(final Color _c) {
    couleurRemplissage_= _c;
  }
  public int getTypeTrait() {
    return typeTrait_;
  }
  public void setTypeTrait(final int _typeTrait) {
    typeTrait_= _typeTrait;
  }
  public float getEpaisseurTrait() {
    return epaisseurTrait_;
  }
  public void setEpaisseurTrait(final float _epaisseurTrait) {
    epaisseurTrait_= _epaisseurTrait;
  }
  public int getTypeSurface() {
    return typeSurface_;
  }
  public void setTypeSurface(final int _typeSurface) {
    typeSurface_= _typeSurface;
  }
}
