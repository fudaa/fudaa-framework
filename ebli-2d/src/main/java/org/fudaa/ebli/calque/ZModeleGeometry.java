/*
 *  @creation     31 mars 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneListener;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Un mod�le contenant toutes sortes de GIS g�om�tries (non sp�cialis�). Les modifications
 * apport�es au mod�le peuvent �tre �cout�es.
 * @see org.fudaa.ebli.calque.ZCalqueGeometry
 * @author Bertrand Marchand
 * @version $Id$
 */
public interface ZModeleGeometry extends ZModeleDonnees, GISZoneListener {
  
  /**
   * Retourne la collection qui associe les g�om�tries du mod�le � des atrributs.
   * @return la collection.
   */
  GISZoneCollection getGeomData();

  /**
   * Prepare l'exportation des donn�es, en particulier le Z des g�om�tries est rempli depuis
   * l'attribut Z.
   */
  void prepareExport();
  
  /**
   * Retourne le nombre de points de la g�om�trie d'indice donn�.
   * @param _idxGeom l'indice de la g�om�trie
   * @return le nombre de point pour cette g�om�trie
   */
  int getNbPointForGeometry(int _idxGeom);
  
  /**
   * Modifie la boite englobante _target pour correspondre � la boite englobante de la g�om�trie.
   * @param _idxGeom l'indice de la g�om�trie
   * @param _target la boite a modifier
   */
  void getDomaineForGeometry(int _idxGeom, GrBoite _target);
  
  /**
   * Modifie les coordonn�es X,Y,Z du sommet _pt pour correspondre au point d'indice _pointIdx de la
   * g�om�trie.
   * 
   * @param _pt le point a modifier
   * @param _idxGeom l'indice de la g�om�trie
   * @param _pointIdx l'indice du point dans la g�om�trie
   * @return true si indices valides
   */
  boolean point(GrPoint _pt, int _idxGeom, int _pointIdx);
  
  /**
   * La g�ometrie est-elle reli�e.
   * @param _idxGeom L'indice de la g�om�trie dans le mod�le.
   * @return True : Si la g�om�trie est reli�e.
   */
  public boolean isGeometryReliee(int _idxGeom);
  
  /**
   * La g�ometrie est-elle ferm�e.
   * @param _idxGeom L'indice de la g�om�trie dans le mod�le.
   * @return True : Si la g�om�trie est ferm�e.
   */
  public boolean isGeometryFermee(int _idxGeom);
  
  /**
   * La g�om�trie est-elle visible.
   * @param _idxGeom L'indice de la g�om�trie dans le mod�le.
   * @return True : Si la g�om�trie est visible.
   */
  public boolean isGeometryVisible(int _idxGeom);
  
  /**
   * Ajoute un listener �coutant les modifications apport�es au mod�le.
   * @param _listener Le listener.
   */
  public void addModelListener(ZModelGeometryListener _listener);
  
  /**
   * Supprime le listener �coutant les modifications apport�es au mod�le.
   * @param _listener Le listener.
   */
  public void removeModelListener(ZModelGeometryListener _listener);
}
