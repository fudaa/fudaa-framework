/*
 * @file BCalque.java
 * 
 * @creation 1998-08-25
 * 
 * @modification $Date: 2007-05-04 13:49:42 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.commun.CalqueLayout;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.*;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.repere.AbstractCalque;

/**
 * Une classe de base pour tous les calques. Elle gere les transformations du repere et l'organisation des calques en une hierarchie arborescente. Un
 * calque peut etre de trois sortes: <UL> <LI>un groupe de calques: structurant, regroupant plusieurs calques ou familles de calques, <LI>un calque
 * d'affichage: calque representant des donnees a l'ecran, <LI>un calque d'interaction: gerant les evenements exterieurs et generalement associe a un
 * calque d'affichage (ex: BCalqueDessin et BCalqueDessinInteraction). </UL>
 *
 * @version $Id: BCalque.java,v 1.54 2007-05-04 13:49:42 deniger Exp $
 * @author Guillaume Desnoix , Axel von Arnim
 */
public abstract class BCalque extends AbstractCalque implements Icon, BConfigurePaletteTargetInterface {

  public static final Comparator createComparator() {
    return new Comparator() {
      @Override
      public int compare(final Object _o1, final Object _o2) {
        if ((_o1 instanceof BCalque) && (_o2 instanceof BCalque)) {
          int i = ((BCalque) _o1).getTitle().compareTo(((BCalque) _o2).getTitle());
          if ((i == 0) && (_o1 != _o2)) {
            i = ((BCalque) _o1).getName().compareTo(((BCalque) _o2).getName());
            if (i == 0) {
              return _o1.hashCode() - _o2.hashCode();
            }
          }
          return i;
        }
        throw new IllegalArgumentException();
      }
    };
  }

  public static BCalque findCalqueByName(final BCalque[] _cq, final String _name) {
    if (_cq != null && _name != null) {
      for (int i = 0; i < _cq.length; i++) {
        if (_name.equals(_cq[i].getName())) {
          return _cq[i];
        }
      }
    }
    return null;

  }

  /**
   *
   * @return true if the layer can be move up or down.
   */
  public boolean isMovable() {
    return true;
  }

  public static int findCalqueIdxByName(final BCalque[] _cq, final String _name) {
    if (_cq != null && _name != null) {
      for (int i = 0; i < _cq.length; i++) {
        if (_name.equals(_cq[i].getName())) {
          return i;
        }
      }
    }
    return -1;

  }
  private boolean destructible_;
  protected boolean titleModifiable_;
  // Informations
  /**
   * Chaine affichee dans l'arbre des calques.
   */
  private String title_;
  /**
   * Le titre long affich� dans le tooltip de l'arbre des calques
   */
  private String longTitle_;
  private transient GrMorphisme versEcran_;
  private transient GrMorphisme versReel_;
  EbliActionInterface[] actions_;
  // Proprietes
  protected boolean rapide_;

  /**
   * Constructeur. Le constructeur initialise les transformations versEcran et versReel avec la transformation identite. versEcran transforme les
   * coordonnees reelles en coordonnees ecran et versReel fait l'inverse. versEcran sert a l'affichage des objets et versReel a la gestion de la
   * souris dans le repere reel.
   */
  protected BCalque() {
    super();
    // ajustement_ = false;
    destructible_ = true;
    versEcran_ = GrMorphisme.identite();
    versReel_ = GrMorphisme.identite();
    super.setOpaque(false);
    setLayout(new CalqueLayout());
    setRequestFocusEnabled(false);
    setDoubleBuffered(false);
  }

  /**
   * Methode de la classe Container surchargee. Elle est appelee lors de l'ajout d'un nouveau sous-calque. Elle initialise les transformations
   * versEcran et versReel aux valeurs du calque hote.
   *
   * @param _comp calque a ajouter
   * @param _constraints contraintes de layout
   * @param _index indice dans la liste de calques
   * @see java.awt.Container#add(java.awt.Component)
   */
  @Override
  protected void addImpl(final Component _comp, final Object _constraints, final int _index) {
    BCalque calque = null;
    if (_comp instanceof BCalque) {
      calque = (BCalque) _comp;
      final int idx = getDirectCalqueIdx(calque);
      if (idx >= 0) {
        calque.putClientProperty("oldIndice", new Integer(idx));
      }
      calque.setVersEcran(getVersEcran());
      calque.setVersReel(getVersReel());
    }
    super.addImpl(_comp, _constraints, _index);
    if (calque != null) {
      calque.putClientProperty("oldIndice", new Integer(getDirectCalqueIdx(calque)));
    }
  }

  protected GrBoite getClipEcran(final Graphics _g) {
    Rectangle clip = _g == null ? null : _g.getClipBounds();
    if (clip == null) {
      clip = new Rectangle(0, 0, getWidth(), getHeight());
    }
    return new GrBoite(new GrPoint(clip.x, clip.y, 0.), new GrPoint(clip.x + clip.width - 1, clip.y + clip.height - 1,
            0.));
  }

  protected Dimension getTargetDimension(final GrMorphisme _versEcran, final GrMorphisme _versReel) {
    return getTargetDimension(_versEcran, _versReel, null);
  }

  protected Dimension getTargetDimension(final GrMorphisme _versEcran, final GrMorphisme _versReel, final Dimension _d) {
    if (_versEcran == null) {
      return null;
    }
    if (getVersEcran().isSame(_versEcran)) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("ECA: target is same size");
      }
      return getSize(_d);
    }
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("ECA: target is not same size");
    }
    final GrMorphisme versReel = _versReel;
    final GrPoint p = new GrPoint();
    final GrPoint p1 = new GrPoint(getWidth(), getHeight(), 0);
    p.autoApplique(versReel);
    p1.autoApplique(versReel);
    p.autoApplique(_versEcran);
    p1.autoApplique(_versEcran);
    final Dimension r = _d == null ? new Dimension() : _d;
    r.height = (int) Math.abs(p.y_ - p1.y_);
    r.width = (int) Math.abs(p.x_ - p1.x_);
    return r;
  }

  protected void superPaint(Graphics g) {
    super.paint(g);
  }

  /**
   * ajouter un listener pour les evts de selection.
   */
  public void addSelectionListener(final ZSelectionListener _l) {
    listenerList.add(ZSelectionListener.class, _l);
  }

  /**
   * @param _visitor le visitor
   * @return true si la visite doit continuer
   */
  public final boolean apply(final BCalqueVisitor _visitor) {
    if (_visitor.visit(this)) {
      final int nb = getComponentCount();
      for (int i = 0; i < nb; i++) {
        // B.M. : Certains calques comportent des composants autres que des calques !! 
        // (BCalqueLegend entre autres).
        if ((getComponent(i) instanceof BCalque)
                && !((BCalque) getComponent(i)).apply(_visitor)) {
          return false;
        }
      }
      return true;
    }
    return false;

  }

  public JMenuItem createRenamerItem() {
    return new BuMenuItem(BuResource.BU.getIcon("aucun"), BuResource.BU.getString("renommer")) {
      @Override
      protected void fireActionPerformed(final ActionEvent _event) {
        super.fireActionPerformed(_event);
        final BuTextField ft = new BuTextField(getTitle());
        ft.setColumns(12);
        final CtuluDialogPanel pn = new CtuluDialogPanel() {
          @Override
          public boolean apply() {
            BCalque.this.setTitle(ft.getText());
            return true;
          }

          @Override
          public boolean isDataValid() {
            final String txt = ft.getText();
            if (txt.length() == 0) {
              setErrorText(CtuluLib.getS("Le champ est vide") + '!');
              return false;
            }
            return true;
          }
        };
        pn.addLabel(BuResource.BU.getString("renommer"));
        pn.add(ft);
        pn.afficheModale(this, BuResource.BU.getString("renommer..."));
      }
    };

  }

  public void descendre() {
    final Container p = getParent();
    if (p instanceof BCalque) {
      ((BCalque) p).descendre(this);
    }
  }

  /**
   * Place le calque specifie en tete de la liste des fils.
   *
   * @see #enPremier()
   */
  public void descendre(final BCalque _c) {
    if (_c == null) {
      return;
    }
    final Component[] cqs = getComponents();
    if (cqs == null) {
      return;
    }
    final int idx = CtuluLibArray.findObjectEgalEgal(cqs, _c);
    if (idx >= 0 && idx < cqs.length - 1) {
      add(_c, idx + 1);
      _c.revalidate();
    }
  }

  /**
   * Detruit ce calque. Equivalent a pere.detruire(this)
   *
   * @see #detruire(BCalque)
   */
  public void detruire() {
    if (!isDestructible()) {
      return;
    }
    final Container p = getParent();
    if (p instanceof BCalque) {
      ((BCalque) p).detruire(this);
    }
  }

  @Override
  public BSelecteurTargetInterface getVisibleTitleTarget() {
    return new BCalqueSelecteurTitleVisibleTarget(this);
  }

  /**
   * Detruit le calque specifie de la liste des fils.
   *
   * @see #detruire()
   */
  public void detruire(final BCalque _c) {
    if (_c.isDestructible()) {
      final BCalque[] cq = _c.getCalques();
      for (int i = 0; i < cq.length; i++) {
        _c.detruire(cq[i]);
      }
      final BVueCalque p = (BVueCalque) SwingUtilities.getAncestorOfClass(BVueCalque.class, this);
      if (p != null) {
        if (_c instanceof MouseListener) {
          p.removeMouseListener((MouseListener) _c);
        }
        if (_c instanceof MouseMotionListener) {
          p.removeMouseMotionListener((MouseMotionListener) _c);
        }
      }
      // pour pouvoir etre recuperer par l'arbre des calques
      _c.putClientProperty("oldIndice", new Integer(getDirectCalqueIdx(_c)));
      remove(_c);
      revalidate();
      if (p != null) {
        p.repaint();
      }
    }
  }

  /**
   * Place ce calque en fin de la liste des calques fils du pere. Equivalent a pere.enDernier(this)
   *
   * @see #enDernier(BCalque)
   */
  public void enDernier() {
    final Container p = getParent();
    if (p instanceof BCalque) {
      ((BCalque) p).enDernier(this);
    }
  }

  /**
   * Place le calque specifie en fin de la liste des fils.
   *
   * @see #enDernier()
   */
  public void enDernier(final BCalque _c) {
    if (_c == null) {
      return;
    }
    add(_c);
    _c.revalidate();
  }
  JMenuItem[] spec_;

  protected void buildSpecificMenuItemsForAction(final List _l) {
  }

  public final JMenuItem[] getSpecificMenuItems() {
    if (spec_ == null) {
      final List l = new ArrayList();
      buildSpecificMenuItemsForAction(l);
      if (!CtuluLibArray.isEmpty(getActions())) {
        final EbliActionInterface[] act = getActions();
        for (int i = 0; i < act.length; i++) {
          if (act[i] == null) {
            l.add(null);
          } else {
            act[i].updateStateBeforeShow();
            l.add(act[i].buildMenuItem(EbliComponentFactory.INSTANCE));
          }
        }
        spec_ = (JMenuItem[]) l.toArray(new JMenuItem[l.size()]);
      }
    } else {
      final EbliActionInterface[] act = getActions();
      for (int i = 0; i < act.length; i++) {
        if (act[i] != null) {
          act[i].updateStateBeforeShow();
        }
      }
    }
    return spec_;
  }

  // Organisation
  /**
   * Place ce calque en tete de la liste des calques fils du pere. Equivalent a pere.enPremier(this)
   *
   * @see #enPremier(BCalque)
   */
  public void enPremier() {
    final Container p = getParent();
    if (p instanceof BCalque) {
      ((BCalque) p).enPremier(this);
    }
  }

  /**
   * Place le calque specifie en tete de la liste des fils.
   *
   * @see #enPremier()
   */
  public void enPremier(final BCalque _c) {
    add(_c, 0);
    _c.revalidate();
  }

  public void fillWithInfo(final BCalquePaletteInfo.InfoData _d) {
    _d.setTitle(getTitle());

  }

  public final EbliActionInterface[] getActions() {
    return actions_;
  }

  /**
   * Renvoie le premier sous-calque de ce calque de nom donn�.
   *
   * @param _name Nom du sous calque.
   * @return Le sous calque de nom donn�. <i>null </i> si aucun sous calque ne porte ce nom.
   * @deprecated
   */
  public BCalque getCalque(final String _name) {
    return getCalqueParNom(_name);
  }

  public int getCalqueIdx(final BCalque _cq) {
    return findCalqueIdxByName(getTousCalques(), _cq.getName());
  }

  public int getCalqueIdxParNom(final String _name) {
    return findCalqueIdxByName(getTousCalques(), _name);
  }

  /**
   * Renvoie le premier sous-calque de ce calque de nom donn�.
   *
   * @param _name Nom du sous calque.
   * @return Le sous calque de nom donn�. <i>null </i> si aucun sous calque ne porte ce nom.
   */
  public BCalque getCalqueParNom(final String _name) {
    return findCalqueByName(getTousCalques(), _name);
  }

  /**
   * Renvoie le premier sous-calque de ce calque de nom donn�.
   *
   * @param _title Le titre du sous calque.
   * @return Le sous calque de titre donn�. <i>null </i> si aucun sous calque ne porte ce titre.
   */
  public BCalque getCalqueParTitre(final String _title) {
    if (_title == null) {
      return null;
    }
    final BCalque[] cqs = getTousCalques();
    for (int i = 0; i < cqs.length; i++) {
      if (_title.equals(cqs[i].getTitle())) {
        return cqs[i];
      }
    }
    return null;
  }

  /**
   * Renvoie les calques fils de ce calque.
   *
   * @return les calques fils
   * @see #getTousCalques()
   */
  public BCalque[] getCalques() {
    int i/* , n */;
    final Component[] c = getComponents();
    final List r = new ArrayList(c.length);
    // n = 0;
    for (i = 0; i < c.length; i++) {
      if (c[i] instanceof BCalque) {
        r.add(c[i]);
      }
    }
    return (BCalque[]) r.toArray(new BCalque[r.size()]);
  }

  public GrBoite getClipReel(final Graphics _g) {
    final GrBoite b = getClipEcran(_g);
    b.autoApplique(getVersReel());
    return b;
  }

  public BConfigurableInterface getSingleConfigureInterface() {
    return null;
  }

  @Override
  public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[]{getSingleConfigureInterface()};
  }

  /**
   * Oblige de creer une m�thode soeur a getForeground() pour la methode de BGroupeCalque. Dans la logique des calques, la couleur d'un groupe est la
   * couleur commune � ces calques fils. Or dans la logique de swing, la couleur d'un composant (qui n'a pas de couleur) est celui de son p�re. Dans
   * ce cas, on boucle ...
   *
   * @return getForeground()
   */
  public Color getCouleur() {
    return getForeground();
  }

  public int getDirectCalqueIdx(final BCalque _cq) {
    return CtuluLibArray.findObjectEgalEgal(getCalques(), _cq);
  }

  /**
   * Renvoie le domaine (l'etendue) du contenu du calque. null si non significatif.
   *
   * @return domaine en coordonnees reelles
   */
  @Override
  public GrBoite getDomaine() {
    GrBoite r = null;
    if (isVisible()) {
      final BCalque[] c = getCalques();
      for (int i = 0; i < c.length; i++) {
        final GrBoite d = c[i].getDomaine();
        if (d != null) {
          if (r == null) {
            try {
              r = (GrBoite) d.clone();
            } catch (final CloneNotSupportedException _evt) {
              FuLog.error(_evt);

            }
          } else {
            r = r.union(d);
          }
        }
      }
    }
    return r;
  }

  /**
   * la hauteur de l'icone correspondant (vue de l'arbre).
   *
   * @return 24
   */
  @Override
  public int getIconHeight() {
    return 24;
  }

  /**
   * Icone: la largeur de l'icone correspondant (vue de l'arbre).
   *
   * @return 24
   */
  @Override
  public int getIconWidth() {
    return 24;
  }

  public BCalquePersistenceInterface getPersistenceMng() {
    return new BCalquePersistenceSingle();
  }

  /**
   * Accesseur generique de propriete.
   *
   * @param _name nom de la propriete a lire
   * @return valeur de la propriete
   */
  public Object getProperty(final String _name) {
    Object r = null;
    try {
      final String n = "get" + _name.substring(0, 1).toUpperCase() + _name.substring(1);
      final Class[] c = new Class[0];
      final Method m = getClass().getMethod(n, c);
      final Object[] o = new Object[0];
      r = m.invoke(this, o);
    } catch (final Exception ex) {
      if (!"ancestor".equals(_name)) {
        FuLog.warning("Property " + _name + " can't be read " + getClass().getName() + CtuluLibString.DOT, ex);
      }
    }
    return r;
  }

  public String getTitle() {
    return title_;
  }

  /**
   * Renvoie tous les sous-calques de ce calque. parcourt toute l'arborescence des sous-calques jusqu'aux feuilles.
   *
   * @return les sous-calques
   * @see #getCalques()
   */
  public BCalque[] getTousCalques() {
    int i;
    int j;
    int n;
    final BCalque[] c = getCalques();
    n = 1;
    for (i = 0; i < c.length; i++) {
      n += c[i].getTousCalques().length;
    }
    final BCalque[] r = new BCalque[n];
    r[0] = this;
    n = 1;
    for (i = 0; i < c.length; i++) {
      final BCalque[] sc = c[i].getTousCalques();
      for (j = 0; j < sc.length; j++) {
        r[n] = sc[j];
        n++;
      }
    }
    return r;
  }

  /**
   * Accesseur de la propriete versEcran. Elle est la matrice de transformation a appliquer lors de l'affichage du calque.
   */
  public GrMorphisme getVersEcran() {
    return versEcran_;
  }

  /**
   * Accesseur de la propriete versReel. Elle est la matrice de transformation a appliquer lors de la lecture d'un evenement souris par exemple.
   */
  public GrMorphisme getVersReel() {
    return versReel_;
  }

  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      if (_p.isDefined("calque.visible")) {
        setVisible(_p.getBoolean("calque.visible"));
      }
      if (_p.isDefined("calque.rapide")) {
        setRapide(_p.getBoolean("calque.rapide"));
      }
      if (_p.isTitleDefined()) {
        setTitle(_p.getTitle());
      }
      if (_p.isDefined("calque.foreground") && !isGroupeCalque()) {
        setForeground((Color) _p.get("calque.foreground"));
      }
      if (_p.isDefined("calque.font")) {
        setFont((Font) _p.get("calque.font"));
      }

    }
  }

  /**
   * Indique si un calque peut etre detruit par l'utilisateur. Faux par defaut.
   *
   * @return vrai si destructible
   */
  public boolean isDestructible() {
    boolean r = destructible_;
    final BCalque[] c = getCalques();
    for (int i = 0; i < c.length; i++) {
      r &= c[i].isDestructible();
    }
    return r;
  }

  public boolean isGroupeCalque() {
    return false;
  }

  /**
   * Accesseur de la propriete ajustement. Elle est envoyee par les beans de controle de repere comme BTransformationGlissiere pour preciser que
   * l'evenement repere associe est intermediaire. Ceci permet de passer en mode rapide pour l'affichage des calques lors des rafales d'evenements
   * repere.
   */
  public boolean isRapide() {
    return rapide_;
  }

  /**
   * @return true si le titre peut etre modifie depuis l'arbre des calques
   */
  @Override
  public boolean isTitleModifiable() {
    return titleModifiable_;
  }

  /**
   * Definit si le titre peut etre modifie depuis l'arbre des calques
   *
   * @param _b True : Il peut l'etre.
   */
  public void setTitleModifiable(boolean _b) {
    titleModifiable_ = _b;
  }

  @Override
  public final boolean isValidateRoot() {
    return false;
  }

  public void monter() {
    final Container p = getParent();
    if (p instanceof BCalque) {
      ((BCalque) p).monter(this);
    }
  }

  /**
   * Place le calque specifie en tete de la liste des fils.
   *
   * @see #enPremier()
   */
  public void monter(final BCalque _c) {
    if (_c == null) {
      return;
    }
    final int idx = CtuluLibArray.findObjectEgalEgal(getComponents(), _c);
    if (idx > 0) {
      add(_c, idx - 1);
      _c.revalidate();
    }
  }

  @Override
  public void paint(final Graphics _g) {
    // Paint (HACK: forcer le clip a cause d'un bug de 1.2 (?))
    if (_g.getClipBounds() == null) {
      _g.setClip(0, 0, getWidth(), getHeight());
    }
    super.paint(_g);
  }

  public void paintImage(final Graphics _g) {
    print(_g);
  }

  public void paintAllInImage(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    /*
     * if(isVisible()) paint(_g);
     */
  }

  @Override
  public void paintComponent(final Graphics _g) {
    // super.paintComponent(_g);
    // surchargee dans chaque calque
  }

  /**
   * Dessin de l'icone.
   *
   * @param _c composant dont l'icone peut utiliser des proprietes (couleur, ...). Ce parametre est le calque lui-meme. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(Color.white);
    _g.fillRect(_x + 1, _y + 1, w - 1, h - 1);
    _g.setColor(isVisible() ? Color.black : Color.lightGray);
    _g.drawRect(_x, _y, w, h);
  }

  /**
   * Reaffichage complet du calque, plus rapide.
   */
  public final void quickRepaint() {
    final Graphics g = getGraphics();
    if (g != null) {
      update(g);
    }
    repaint(1000);
  }

  public void removeSelectionListener(final ZSelectionListener _l) {
    listenerList.remove(ZSelectionListener.class, _l);
  }

  @Override
  public void repaint() {
    if (isValidateRoot()) {
      super.repaint(0);
    } else if (getParent() != null) {
      getParent().repaint(0);
    }
  }

  /**
   * repaint apres un delai.
   *
   * @param _tm delai en millisecondes
   */
  @Override
  public void repaint(final long _tm) {
    if (isValidateRoot()) {
      super.repaint(_tm);
    } else if (getParent() != null) {
      getParent().repaint(_tm);
    }
  }

  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties prop = new EbliUIProperties(this.getName());
    prop.put("calque.visible", isVisible());
    prop.put("calque.rapide", isRapide());
    prop.put("calque.font", getFont());
    prop.setTitle(getTitle());
    if (!isGroupeCalque() && getForeground() != null) {
      prop.put("calque.foreground", getForeground());
    }
    return prop;
  }

  public final void setActions(final EbliActionInterface[] _actions) {
    actions_ = _actions;
  }

  public void setActionsEnable(final boolean _b) {
    if (getActions() != null) {
      EbliLib.setActionEnable(getActions(), _b);
    }
  }

  /**
   * @param _c la nouvelle couleur avant-plan
   */
  public boolean setCouleur(final Color _c) {
    setForeground(_c);
    return true;
  }

  public void setDestructible(final boolean _v) {
    if (destructible_ != _v) {
      final boolean vp = destructible_;
      destructible_ = _v;
      firePropertyChange("destructible", vp, destructible_);
    }
  }

  @Override
  public void setOpaque(final boolean _isOpaque) {
    // super.setOpaque(_isOpaque);
  }

  /**
   * Affectation generique de propriete.
   *
   * @param _name nom de la propriete a modifier
   * @param _value nouvelle valeur
   */
  public boolean setProperty(final String _name, final Object _value) {
    boolean res = true;
    if (_name == BSelecteurReduitFonteNewVersion.PROPERTY) {
      setFont((Font) _value);
    } else if (_name == BSelecteurColorChooser.DEFAULT_PROPERTY) {
      setForeground((Color) _value);
    } else {
      try {
        final String n = "set" + _name.substring(0, 1).toUpperCase() + _name.substring(1);
        Class z = _value.getClass();

        try {
          z = (Class) (z.getField("TYPE").get(z));
        } catch (final Exception ey) {
        }
        final Class[] c = new Class[1];
        final Class thisClass = getClass();
        Method m = null;
        do {
          c[0] = z;
          try {
            m = thisClass.getMethod(n, c);
          } catch (final Exception ey) {
            m = null;
            res = false;
          }
          z = z.getSuperclass();
        } while ((m == null) && (z != null));
        final Object[] o = new Object[]{_value};
        if (m != null) {
          m.invoke(this, o);
          res = true;
        }
      } catch (final Exception ex) {
        res = false;
        if (!"ancestor".equals(_name)) {
          FuLog.warning("EBL: Property " + _name + " can't be overwritten" + getClass().getName() + CtuluLibString.DOT);
        }
      }
    }
    return res;
  }

  /**
   * Affectation de la propriete ajustement. Cette affectation est appliquee aux calques fils.
   */
  public void setRapide(final boolean _v) {
    if (rapide_ != _v) {
      final boolean vp = rapide_;
      rapide_ = _v;
      final BCalque[] c = getCalques();
      for (int i = 0; i < c.length; i++) {
        c[i].setRapide(_v);
      }
      firePropertyChange("ajustement", vp, rapide_);
      if (isVisible() && !rapide_) {
        repaint();
      }
    }

  }

  /**
   * @param _title le nouveau titre non null
   * @return true si changement
   */
  public boolean setTitle(final String _title) {
    if (_title != null && getName() == null) {
      super.setName(FuLib.clean(_title));
    }
    if (_title != null && !_title.equals(title_)) {
      final String old = title_;
      title_ = _title;
      firePropertyChange(BSelecteurTextField.TITLE_PROPERTY, old, title_);
      return true;
    }
    return false;
  }

  /**
   * Definit le titre long, affich� en tooltip dans l'arbre des calques.
   *
   * @param _title Le nouveau titre long. Peut �tre null.
   * @return true si changement.
   */
  public boolean setLongTitle(final String _title) {
    if (_title != longTitle_ || (_title != null && !_title.equals(longTitle_))) {
      final String old = longTitle_;
      longTitle_ = _title;
      firePropertyChange("longTitle", old, longTitle_);
      return true;
    }
    return false;
  }

  /**
   * @return Le titre long, affich� en tooltip dans l'arbre des calques. null si aucun titre long d�fini.
   */
  public String getLongTitle() {
    return longTitle_;
  }

  /**
   * Affectation de la propriete versEcran. Cette affectation est appliquee aux calques fils.
   */
  public void setVersEcran(final GrMorphisme _v) {
    if (versEcran_ != _v) {
      final GrMorphisme vp = versEcran_;
      versEcran_ = _v;
      final BCalque[] c = getCalques();
      for (int i = 0; i < c.length; i++) {
        c[i].setVersEcran(_v);
      }
      firePropertyChange("versEcran", vp, versEcran_);
    }
  }

  /**
   * Affectation de la propriete versReel. Cette affectation est appliquee aux calques fils.
   */
  public void setVersReel(final GrMorphisme _v) {
    if (versReel_ != _v) {
      final GrMorphisme vp = versReel_;
      versReel_ = _v;
      final BCalque[] c = getCalques();
      for (int i = 0; i < c.length; i++) {
        c[i].setVersReel(_v);
      }
      firePropertyChange("versReel", vp, versReel_);
    }
  }

  @Override
  public void setVisible(final boolean _flag) {
    if (isVisible() != _flag) {
      super.setVisible(_flag);
      firePropertyChange(BSelecteurCheckBox.PROP_VISIBLE, !_flag, _flag);
    }
  }

  public boolean isUserVisible() {
    return isVisible();
  }

  public void setUserVisible(boolean userVisible) {
    if (isUserVisible() != userVisible) {
      setVisible(userVisible);
      firePropertyChange(BSelecteurCheckBox.PROP_USER_VISIBLE, !userVisible, userVisible);
    }
  }

  /**
   * Controle si tous les calques descendants sont visible.
   *
   * @return Vrai si tous les calques descendants sont visibles.
   */
  public boolean isAllChildrenVisible() {
    BCalque[] cqs = getTousCalques();
    // Le calque d'indice 0 est le calque courant.
    for (int i = 1; i < cqs.length; i++) {
      if (!cqs[i].isVisible()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Controle si tous les calques descendants sont invisible.
   *
   * @return Vrai si tous les calques descendants sont invisibles.
   */
  public boolean isAllChildrenUnvisible() {
    BCalque[] cqs = getTousCalques();
    // Le calque d'indice 0 est le calque courant.
    for (int i = 1; i < cqs.length; i++) {
      if (cqs[i].isVisible()) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    String r = getTitle();
    if ((r == null) || r.equals("")) {
      r = getName();
    }
    if ((r == null) || r.equals("")) {
      r = super.toString();
    }
    return r;
  }
}
