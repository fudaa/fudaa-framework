/**
 * @creation 21 janv. 2005 @modification $Date: 2006-09-19 14:55:45 $ @license GNU General Public License 2 @copyright
 * (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author Fred Deniger
 * @version $Id: ZCalqueSondeInteraction.java,v 1.8 2006-09-19 14:55:45 deniger Exp $
 */
@SuppressWarnings("serial")
public class ZCalqueSondeInteraction extends ZCalqueClickInteraction {
  
  private ZCalqueSondeInterface target_;
  private final ZScene zScene;
  /**
   * liste des listener sur le calquesondeInteraction
   */
  private List<ZCalqueSondeListener> listeners_ = new ArrayList<ZCalqueSondeListener>();
  
  protected ZCalqueSondeInteraction(ZScene zScene) {
    super();
    super.setGele(true);
    this.zScene = zScene;
  }
  
  public final ZCalqueSondeInterface getTarget() {
    return target_;
  }
  
  public void addZCalqueSondeListener(ZCalqueSondeListener _l) {
    listeners_.add(_l);
  }
  
  public void removeZCalqueSondeListener(ZCalqueSondeListener _l) {
    if (listeners_.contains(_l)) {
      listeners_.remove(_l);
    }
  }
  
  public void notifySondeListener(final GrPoint ptReel) {
    for (ZCalqueSondeListener l : listeners_) {
      l.notifySondeMoved(this, ptReel);
    }
    zScene.sondeChanged(ptReel);
  }
  
  @Override
  public void mouseClicked(final MouseEvent _e) {
    if (_e.isPopupTrigger()) {
      return;
    }
    mouseDragged(_e);
  }

  /**
   * Ralise l'action de clic d une sonde. PRePRO 7
   *
   * @param pt
   * @param add
   */
  public void moveEvent(GrPoint pt, boolean add, boolean autoAppliqueTranslation) {
    if (autoAppliqueTranslation) {
      pt.autoApplique(((BCalque) target_).getVersReel());
    }
    target_.changeSonde(pt, add);
    notifySondeListener(pt);
  }
  
  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (target_ != null && !isGele()) {
      final GrPoint pt = new GrPoint(_e.getX(), _e.getY(), 0);
      moveEvent(pt, _e.isShiftDown(), true);
    }
  }
  
  @Override
  public void setGele(final boolean _gele) {
    //to avoid loose the current sondes we do nothing here:
    if (target_ != null && !_gele) {
      target_.setSondeEnable(!_gele);
    }
    super.setGele(_gele);
  }
  
  public final void setTarget(final ZCalqueSondeInterface _target) {
    if (target_ == _target) {
      return;
    }
    target_ = _target;
    if (target_ != null) {
      target_.setSondeEnable(!isGele());
    }
  }
}
