/**
 * @creation 2002-10-8
 * @modification $Date: 2008-01-17 11:42:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JMenu;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ctulu.CtuluSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.impression.EbliFilleImprimable;

/**
 * Fenetre d'affichage de calques contenant des services de base. (position souris, zoom fenetre, selection,
 * transformations spatiales). Il est possible d'ajouter des boutons grace aux methodes <code>addButtonGroup</code>. Par
 * defaut, 3 groupes de boutons sont proposes : selection, standard et navigation. Ils peuvent etre supprimes ou
 * modifies avec les methodes remove
 * 
 * @version $Id: ZEbliFilleCalques.java,v 1.64.6.1 2008-01-17 11:42:51 bmarchan Exp $
 * @author Guillaume Desnoix , Axel von Arnim, Fred deniger
 */
public class ZEbliFilleCalques extends EbliFilleImprimable implements EbliFilleCalquesInterface, CtuluImageProducer,
    CtuluFilleWithComponent, /* MouseListener, */
    CtuluSelectionInterface {

  protected BArbreCalque arbre_;

//  protected BuMenu[] mnSpecifics_;

  protected ZEbliCalquesPanel pn_;

  protected JComponent[] specificTools_;

  /**
   * Constructeur minimal. Les groupes de navigation et standard seront construits.
   * 
   * @param _vc la vue calque associee
   */
  public ZEbliFilleCalques(final CtuluUI _ui, final BCalque _vc) {
    this(_ui, _vc, null, null);
  }

  /**
   * @param _vc la vue calque associee autres
   * @param _appli l'appli associee
   */
  public ZEbliFilleCalques(final CtuluUI _ui, final BCalque _vc, final BuCommonInterface _appli) {
    this(_ui, _vc, _appli, null);
  }

  /**
   * @param _vc un calque a ajouter a la vue
   * @param _appli l'application : permet de recuperer les informations sur le soft
   * @param _id les infos du document (pour l'impression)
   */
  public ZEbliFilleCalques(final CtuluUI _ui, final BCalque _vc, final BuCommonInterface _appli,
      final BuInformationsDocument _id) {
    this(new ZEbliCalquesPanel(_vc, _ui), _appli, _id);
  }

  /**
   * @param _pn le panneau a ajouter a la fenetre.
   * @param _appli l'application : permet de recuperer les informations sur le soft
   * @param _id les infos du document (pour l'impression)
   */
  public ZEbliFilleCalques(final ZEbliCalquesPanel _pn, final BuCommonInterface _appli, final BuInformationsDocument _id) {
    super("", true, false, true, true, _appli, _id);
    pn_ = _pn;

    setName("ifCALQUES");
    setContentPane(pn_);
    /*
     * pn_.setFocusable(true); setFocusable(true); getVueCalque().setFocusable(true);
     */
    getVueCalque().requestFocus();
    pack();
  }


  protected void addCalque(final BCalque _b, final boolean _enPremier) {
    pn_.addCalque(_b, _enPremier);
  }

  protected void applyPreferences() {
    fireInternalFrameEvent(InternalFrameEvent.INTERNAL_FRAME_DEACTIVATED);
    specificTools_ = null;
    fireInternalFrameEvent(InternalFrameEvent.INTERNAL_FRAME_ACTIVATED);
  }

  /**
   * Modifie la propriete <code>enable</code> de tous les boutons du groupe selection.
   * 
   * @param _enable
   */
  protected void setSelectionEnabled(final boolean _enable) {
    pn_.getController().setSelectionEnabled(_enable);
  }

  protected void writePreferences() {
    EbliPreferences.EBLI.writeIniFile();
  }

  /**
   * Methode a utiliser pour ajouter un calque et pour mettre a jour le model de l'arbre.
   * 
   * @param _c le calque a ajouter
   */
  public void addCalque(final BCalque _c) {
    pn_.addCalque(_c);
  }

  @Override
  public final void clearSelection() {
    pn_.clearSelection();
  }

  /**
   * Efface la selection.
   */
  public void clearSelections() {
    pn_.getController().clearSelections();
  }

  @Override
  public JComponent createPanelComponent() {
    if (arbre_ == null) {
      arbre_ = new BArbreCalque();
      arbre_.setName("ARBRE_CALQUE");
      arbre_.setRootVisible(false);
      arbre_.setModel(null);
    }
    return arbre_;
  }

  @Override
  public final void find() {
    getVisuPanel().find();
  }

  /**
   * @return ArbreCalque
   */
  @Override
  public BArbreCalqueModel getArbreCalqueModel() {
    return pn_.getArbreCalqueModel();
  }

  /**
   * @return les actions de base
   */
  public EbliActionInterface[] getBaseActions() {
    return pn_.getBaseActions();
  }

  /**
   * @return le calque actif ( selectionne dans l'arbre).
   */
  public BCalque getCalqueActif() {
    return pn_.getCalqueActif();
  }

  /*  *//**
   * @return le menu contextuel a utiliser
   */
  /*
   * public JPopupMenu getCmdsContextuelles() { return null; }
   */

  @Override
  public Class getComponentClass() {
    return BArbreCalque.class;
  }

  @Override
  public String getComponentTitle() {
    return EbliLib.getS("Calques");
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return pn_.getDefaultImageDimension();
  }

  /**
   * @return le groupe contenant les donnees
   */
  public BGroupeCalque getDonneesCalque() {
    return pn_.getDonneesCalque();
  }

  /**
   * @return NumberOfPages
   */
  @Override
  public int getNumberOfPages() {
    return pn_.getNumberOfPages();
  }

  /**
   * @return SpecificMenus
   */
  @Override
  public final JMenu[] getSpecificMenus() {
    return pn_.getSpecificMenus(getTitle());
  }

  /**
   * Renvoie les specific tools de cette fenetre. Ils seront affiches dans la tool bar de l'application. Des boutons
   * suppl�mentaires peuvent �tre ajout�s grace aux fonctions <code>addButtonGroup</code>.
   * 
   * @return specificTools_
   */
  @Override
  public JComponent[] getSpecificTools() {
    if (specificTools_ == null) {

      final JDesktopPane j = getDesktopPane();
      BuDesktop buJ = null;
      // JComponent c;
      if (j instanceof BuDesktop) {
        buJ = (BuDesktop) j;
      }
      final List l = EbliLib.updateToolButtons(pn_.getController().getActions(), buJ);
      specificTools_ = new JComponent[l.size()];
      l.toArray(specificTools_);
      pn_.updateKeyMap(this);
    }
    if (specificTools_ == null) { return null; }
    final JComponent[] r = new JComponent[specificTools_.length];
    System.arraycopy(specificTools_, 0, r, 0, r.length);
    return r;
  }

  public ZEbliCalquesPanel getVisuPanel() {
    return pn_;
  }

  /**
   * @return VueCalque
   */
  public BVueCalque getVueCalque() {
    return pn_.getVueCalque();
  }

  @Override
  public final void inverseSelection() {
    pn_.inverseSelection();
  }

  /**
   * Propriete de visibilite du composant de renseignement de l'etat.
   * 
   * @return true si le mode est visible
   */
  public boolean isModeVisible() {
    return pn_.isModeVisible();
  }

  @Override
  public void majComponent(final Object _o) {
    if (_o instanceof BArbreCalque) {
      final BArbreCalque arbre = (BArbreCalque) _o;

      arbre.setModel(getArbreCalqueModel());
    } else {
      FuLog.error("ECA: not a bcalque");
    }
  }

  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    return pn_.print(_g, _format, _numPage);
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return pn_.produceImage(_params);
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return pn_.produceImage(_w, _h, _params);
  }

  @Override
  public void replace() {}

  public void restaurer() {
    pn_.restaurer();
  }

  @Override
  public final void select() {
    pn_.select();

  }

  /**
   * @param _c Calque
   */
  public void setCalque(final BCalque _c) {
    pn_.setCalque(_c);
  }

  /**
   * Permet de changer le calque actif.
   * 
   * @param _cq le calque a activer
   */
  public final void setCalqueActif(final BCalque _cq) {
    pn_.setCalqueActif(_cq);
  }

  /**
   * Permet d'activer le calque de selection.
   */
  public void setCalqueSelectionActif() {
    pn_.setCalqueSelectionActif();
  }

  /**
   * Rend visible le label d�crivant l'etat de ce composant (selection, zoom, deplacement...).
   * 
   * @param _b le nouvelle valeur pour le mode
   */
  public final void setModeVisible(final boolean _b) {
    pn_.setVisible(_b);
  }

}