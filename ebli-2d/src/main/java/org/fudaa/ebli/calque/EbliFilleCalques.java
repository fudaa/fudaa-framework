/*
 * @file         EbliFilleCalques.java
 * @creation     1998-11-05
 * @modification $Date: 2007-06-05 08:58:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.controle.BSelecteurReduitFonte;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.ebli.palette.BPaletteSelecteurReduitCouleur;
import org.fudaa.ebli.repere.BControleNavigation;
import org.fudaa.ebli.repere.BControleRepereTexte;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Fenetre d'affichage de calques contenant des services de base. (position souris, zoom fenetre, selection,
 * transformations spatiales).
 * 
 * @version $Id: EbliFilleCalques.java,v 1.28 2007-06-05 08:58:38 deniger Exp $
 * @author Guillaume Desnoix , Axel von Arnim
 */
public class EbliFilleCalques extends EbliFilleImprimable implements SelectionListener, TreeSelectionListener,
    EbliFilleCalquesInterface, BCalqueContextuelListener {
  private BGroupeCalque cqAdmin_;
  private BArbreCalqueModel arbreModel_;
  protected BCalqueSelectionInteraction cqSelectionI_;
  private BCalquePositionnementInteraction cqDepVueI_;
  private BCalqueRepereInteraction cqZoomI_;
  protected BuToggleButton[] btSelection_;
  // private BuPopupButton pbRepere;
  /** Panel containing navigation buttons. */
  protected JPanel pnNavigation_;
  /** Panel containing navigation tool. */
  protected BControleNavigation ctNavigation_;
  protected JPanel pnSelection_;
  /** Column tools panel. */
  private JPanel pnBoutons_;
  /** Tool buttons list. Buttons are referenced by action command */
  private final Hashtable toolList_ = new Hashtable();
  /** Tool buttons visibility. Buttons are referenced by action command */
  private final Hashtable toolVisu_ = new Hashtable();
  /** Whether column tools are visible in specific toolbar. */
  private boolean toolsInToolBar_;
  private BCalque cqActif_;
  /**
   * Liste des calques d'interaction non exclusifs.
   */
  private final Set hciNonExclusifs_ = new HashSet();
  private EbliAdapteurSuiviSouris suiviSouris_;

  public EbliFilleCalques() {
    this(null, null, null);
  }

  public EbliFilleCalques(final BCalque _vc) {
    this(_vc, null, null);
  }

  public EbliFilleCalques(final BCalque _vc, final BuCommonInterface _appli) {
    this(_vc, _appli, null);
  }

  public EbliFilleCalques(final BuCommonInterface _appli, final BuInformationsDocument _id) {
    this(null, _appli, _id);
  }

  public EbliFilleCalques(final BCalque _gcInit, final BuCommonInterface _appli, final BuInformationsDocument _id) {
    super("", true, false, true, true, _appli, _id);
    setName("ifCALQUES");
    // vc_ =_vc;
    final BGroupeCalque gc = new BGroupeCalque();
    gc.setName("cqRACINE");
    gc.setTitle(EbliLib.getS("Calques"));
    gc.setDestructible(false);
    arbreModel_ = new BArbreCalqueModel(gc);
    arbreModel_.addTreeSelectionListener(this);
    cqAdmin_ = new BGroupeCalque();
    cqAdmin_.setName("cqADMIN");
    cqAdmin_.setTitle(EbliLib.getS("Administration"));
    cqAdmin_.setDestructible(false);
    gc.add(cqAdmin_);
    // if(vc_.getCalque()==null) {
    // vc_.setCalque(gc);
    // } else {
    // BCalque cqtmp=vc_.getCalque();
    // gc.add(cqtmp);
    // vc_.setCalque(gc);
    // }
    if (_gcInit != null) {
      gc.add(_gcInit);
    }
    vc_ = new BVueCalque(gc);
    buildToolButtons();
    vc_.setPreferredSize(new Dimension(400, 400));
    final JLabel ss = new JLabel();
    ss.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
    ss.setPreferredSize(new Dimension(200, 20));
    ss.setSize(new Dimension(100, 20));
    final BCalqueSuiviSourisInteraction suivi = new BCalqueSuiviSourisInteraction();
    suivi.setTitle(EbliLib.getS("Position"));
    suivi.setName("cqPOSITION");
    suivi.setDestructible(false);
    suiviSouris_ = new EbliAdapteurSuiviSouris(ss);
    suivi.addCoordonneesListener(suiviSouris_);
    setCalqueInteractionExclusif(suivi, false);
    cqAdmin_.add(suivi);
    // S�lection
    final BGroupeCalque gs = new BGroupeCalque();
    gs.setTitle(EbliLib.getS("S�lection"));
    cqSelectionI_ = new BCalqueSelectionInteraction();
    cqSelectionI_.setTitle(EbliLib.getS("Interaction"));
    cqSelectionI_.setName("cqSELECTION");
    cqSelectionI_.setDestructible(false);
    cqSelectionI_.setGele(true);
    cqSelectionI_.addSelectionListener(this);
    final BCalqueSelection s = new BCalqueSelection(cqSelectionI_);
    s.setName("cqSELECTIONAFFICHAGE");
    s.setTitle(EbliLib.getS("Affichage"));
    gs.add(cqSelectionI_);
    gs.add(s);
    cqAdmin_.add(gs);
    associeBoutonsCalqueInteraction(cqSelectionI_, btSelection_);
    final BCalqueContextuel cqContext = new BCalqueContextuel(this);
    // cqContext.setArbreCalque(_arbre);
    // cqContext.addActionListener(this);
    cqContext.setTitle(EbliResource.EBLI.getString("Menu contextuel"));
    cqContext.setName("cqCONTEXTUEL");
    cqContext.setDestructible(false);
    cqContext.setGele(false);
    cqAdmin_.add(cqContext);
    // Navigation
    // Calque de zoom
    cqZoomI_ = new BCalqueRepereInteraction(vc_);
    cqZoomI_.setTitle(EbliLib.getS("Agrandir"));
    cqZoomI_.setName("cqAGRANDIR");
    cqZoomI_.setDestructible(false);
    cqZoomI_.setGele(true);
    cqAdmin_.add(cqZoomI_);
    // Calque de d�placement de vue
    cqDepVueI_ = new BCalquePositionnementInteraction();
    cqDepVueI_.setName("cqDEPLACEMENT_VUE-I");
    cqDepVueI_.setTitle("D�placement de vue");
    cqDepVueI_.setVueCalque(getVueCalque());
    cqDepVueI_.setGele(true);
    cqAdmin_.add(cqDepVueI_);
    associeBoutonCalqueInteraction(cqZoomI_, (JToggleButton) toolList_.get(getStrAgr()));
    associeBoutonCalqueInteraction(cqDepVueI_, (JToggleButton) toolList_.get(getStrDeplace()));
    arbreModel_.refresh();
    // Column tools
    pnSelection_ = new JPanel();
    pnSelection_.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 0));
    pnSelection_.setBorder(new TitledBorder(EbliLib.getS("S�lection")));
    pnNavigation_ = new JPanel();
    pnNavigation_.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 0));
    pnNavigation_.setBorder(new TitledBorder(EbliLib.getS("Navigation")));
    pnBoutons_ = new JPanel();
    pnBoutons_.setLayout(new BuVerticalLayout(2, true, false));
    pnBoutons_.setBorder(new EmptyBorder(2, 2, 2, 2));
    setToolsInSpecificBar(!EbliPreferences.OUTILS_CALQUES_EN_COLONNE);
    final JComponent content = (JComponent) getContentPane();
    content.setLayout(new BuBorderLayout());
    content.add(vc_, BuBorderLayout.CENTER);
    content.add(pnBoutons_, BuBorderLayout.EAST);
    content.add(ss, BuBorderLayout.SOUTH);
    setTitle(EbliLib.getS("Calques"));
    setFrameIcon(BuResource.BU.getIcon("calque"));
    pack();
  }

  private String getStrDeplace() {
    return "DEPLACER_VUE";
  }

  private String getStrAgr() {
    return "AGRANDIR";
  }

  // public void repereModifie(RepereEvent e) {}
  public void restaurer() {
    final GrBoite b = vc_.getCalque().getDomaine();
    if ((b == null) || b.isIndefinie()) {
      return;
    }
    vc_.changeRepere(this, b);
  }

  public void addBouton(final AbstractButton _b) {
    pnBoutons_.add(_b);
    pnBoutons_.setVisible(true);
    pack();
  }

  /**
   * modifer le format d'afficahe des coordonnees de la souris. Si _f est null, les double sont castes en int.
   */
  public void setXYFormat(final EbliFormatterInterface _f) {
    suiviSouris_.format(_f);
  }
  // Proprietes
  private BVueCalque vc_;

  public final BVueCalque getVueCalque() {
    return vc_;
  }

  public void setVueCalque(final BVueCalque _vc) {
    vc_ = _vc;
  }

  @Override
  public BArbreCalqueModel getArbreCalqueModel() {
    return arbreModel_;
  }

  // public void setArbreCalque(BArbreCalque _arbre) { arbreModel_=_arbre; }
  public void setCalque(final BCalque _c) {
    final BCalque cqPpal = vc_.getCalque();
    final BCalque[] calques = cqPpal.getCalques();
    for (int i = 0; i < calques.length; i++) {
      if (calques[i] != cqAdmin_) {
        cqPpal.detruire(calques[i]);
      }
    }
    cqPpal.add(_c);
    arbreModel_.refresh();
  }

  public void setSelectionVisible(final boolean _b) {
    toolVisu_.put(getStrSelectPt(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrSelecRec(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrSelecPoly(), _b ? Boolean.TRUE : Boolean.FALSE);
    // pnSelection_.setVisible(_visible);
  }

  private String getStrSelecPoly() {
    return "SELECTION_POLYGONE";
  }

  private String getStrSelecRec() {
    return "SELECTION_RECTANGLE";
  }

  private String getStrSelectPt() {
    return "SELECTION_PONCTUELLE";
  }

  public void setBoutonRepereVisible(final boolean _b) {
    toolVisu_.put(getStrChanger(), _b ? Boolean.TRUE : Boolean.FALSE);
    // pbRepere.setVisible(_visible);
    // if( !_visible ) pnBoutons_.remove(pbRepere);
  }

  private String getStrChanger() {
    return "CHANGER_REPERE";
  }

  /**
   * Set navigation buttons visible.
   * 
   * @param _b <code>true</code> set buttons visible, <code>false</code> otherwise.
   */
  public void setBoutonsNavigationVisible(final boolean _b) {
    toolVisu_.put(getStrRestaure(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrDeplace(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrAgr(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrChanger(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrNav(), _b ? Boolean.TRUE : Boolean.FALSE);
  }

  private String getStrNav() {
    return "NAVIGUER";
  }

  private String getStrRestaure() {
    return "RESTAURER_REPERE";
  }

  /**
   * Set standard buttons visible.
   * 
   * @param _b <code>true</code> set buttons visible, <code>false</code> otherwise.
   */
  public void setBoutonsStandardVisible(final boolean _b) {
    toolVisu_.put(getStrCoul(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrRemp(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrFo(), _b ? Boolean.TRUE : Boolean.FALSE);
    toolVisu_.put(getStrPal(), _b ? Boolean.TRUE : Boolean.FALSE);
  }

  private String getStrPal() {
    return "PALETTE_COULEUR";
  }

  private String getStrFo() {
    return "CHOIX_FONTE";
  }

  private String getStrRemp() {
    return "COULEUR_REMPLISSAGE";
  }

  private String getStrCoul() {
    return "COULEUR_TRACE";
  }

  /**
   * Set column tools in specific toolbar.
   * 
   * @param _b <code>true</code> set tools in toolbar. <code>false</code> otherwise.
   */
  public final void setToolsInSpecificBar(final boolean _b) {
    toolsInToolBar_ = _b;
    if (_b) {
      pnBoutons_.remove(ctNavigation_);
      pnBoutons_.remove(pnNavigation_);
      pnBoutons_.remove(pnSelection_);
    } else {
      pnNavigation_.add((AbstractButton) toolList_.get(getStrRestaure()));
      pnNavigation_.add((AbstractButton) toolList_.get(getStrDeplace()));
      pnNavigation_.add((AbstractButton) toolList_.get(getStrAgr()));
      pnSelection_.add((AbstractButton) toolList_.get(getStrSelectPt()));
      pnSelection_.add((AbstractButton) toolList_.get(getStrSelecRec()));
      pnSelection_.add((AbstractButton) toolList_.get(getStrSelecPoly()));
      pnBoutons_.add(ctNavigation_);
      pnBoutons_.add(pnNavigation_);
      pnBoutons_.add(pnSelection_);
    }
    pnBoutons_.setVisible(pnBoutons_.getComponentCount() > 0);
    pack();
  }

  /**
   * Methode pour vider programmatiquement la selection.
   */
  public void videSelection() {
    cqSelectionI_.fireSelectionEvent(new SelectionEvent(cqSelectionI_, new VecteurGrContour()));
  }

  /**
   * Rend le calque d'interaction sp�cifi� actif (D�gel�). Les autres sont alors gel�s, sauf les calques d'interactions
   * non exclusifs.
   */
  public void activeCalqueInteraction(final BCalqueInteraction _cq) {
    // Les calques non exclusifs n'agissent pas sur l'�tat des autres calques.
    if (hciNonExclusifs_.contains(_cq)) {
      _cq.setGele(false);
      return;
    }
    final BCalque[] cqs = getVueCalque().getCalque().getTousCalques();
    for (int i = 0; i < cqs.length; i++) {
      if (cqs[i] == _cq) {
        _cq.setGele(false);
      } else if (cqs[i] instanceof BCalqueInteraction && !hciNonExclusifs_.contains(cqs[i])) {
        ((BCalqueInteraction) cqs[i]).setGele(true);
      }
    }
  }

  /**
   * D�finit qu'un calque d'interaction est exclusif. C'est � dire qu'il inhibe (g�le) les autres calques exclusifs
   * lorsqu'il est d�gel�. Par d�faut, tous les calques d'int�raction sont exclusifs.
   */
  public final void setCalqueInteractionExclusif(final BCalqueInteraction _cq, final boolean _exclusif) {
    if (_exclusif) {
      hciNonExclusifs_.remove(_cq);
    } else {
      hciNonExclusifs_.add(_cq);
    }
  }

  /**
   * Etat d'exclusivit� d'un calque d'interaction.
   * 
   * @return <i>true</i> Le calque est exclusif.
   */
  public boolean isCalqueInteractionExclusif(final BCalqueInteraction _cq) {
    return !hciNonExclusifs_.contains(_cq);
  }

  /**
   * Associe un bouton � un calque d'interaction.
   * <p>
   * De ce fait, lorsque le bouton est actionn�, le calque d'interaction associ� est d�gel�/gel�. Si ce calque est
   * d�gel�, tous les autres calques exclusifs sont gel�s.
   * <p>
   * Ce bouton peut �tre �galement ajout� au panneau des boutons de contr�le par la m�thode addBouton.
   * 
   * @param _cqi Le calque d'interaction.
   * @param _bt Le bouton.
   * @see #addBouton(AbstractButton b)
   */
  public final void associeBoutonCalqueInteraction(final BCalqueInteraction _cqi, final AbstractButton _bt) {
    new AssociationCalqueBouton(_cqi, _bt);
  }

  /**
   * Associe un groupe de boutons � un calque d'interaction.
   * <p>
   * De ce fait, lorsqu'un bouton est actionn�, le calque d'interaction associ� est d�gel�/gel�. Si ce calque est
   * d�gel�, tous les autres calques exclusifs sont gel�s.
   * <p>
   * Ces boutons peuvent �tre �galement ajout�s au panneau des boutons de contr�le par la m�thode addBouton.
   * 
   * @param _cqi Le calque d'interaction.
   * @param _bts La liste des boutons.
   * @see #addBouton(AbstractButton b)
   */
  public void associeBoutonsCalqueInteraction(final BCalqueInteraction _cqi, final AbstractButton[] _bts) {
    new AssociationCalqueBouton(_cqi, _bts);
  }
  // BuInternalFrame
  JMenu[] mnSpecifics_;

  @Override
  public JMenu[] getSpecificMenus() {
    if (mnSpecifics_ == null) {
      // Pour ne plus avoir de pb de menus qui ne sont pas effac�s quand on passe d'une fenetre � une autre.
      mnSpecifics_ = new JMenu[1];
      mnSpecifics_[0] = BArbreCalque.buildNormalMenu(arbreModel_);
    }
    return mnSpecifics_;
  }

  /**
   * @return SpecificTools
   */
  @Override
  public JComponent[] getSpecificTools() {
    final ArrayList vtools = new ArrayList(30);
    if (toolsInToolBar_) {
      // Selection buttons
      if (toolVisu_.get(getStrSelectPt()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrSelectPt()));
      }
      if (toolVisu_.get(getStrSelecRec()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrSelecRec()));
      }
      if (toolVisu_.get(getStrSelecPoly()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrSelecPoly()));
      }
      vtools.add(null);
      // Navigation buttons
      if (toolVisu_.get(getStrRestaure()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrRestaure()));
      }
      if (toolVisu_.get(getStrDeplace()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrDeplace()));
      }
      if (toolVisu_.get(getStrAgr()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrAgr()));
      }
      vtools.add(null);
      if (toolVisu_.get(getStrChanger()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrChanger()));
      }
      if (toolVisu_.get(getStrNav()).equals(Boolean.TRUE)) {
        vtools.add(toolList_.get(getStrNav()));
      }
      vtools.add(null);
    }
    // Standard buttons
    if (toolVisu_.get(getStrCoul()).equals(Boolean.TRUE)) {
      vtools.add(toolList_.get(getStrCoul()));
    }
    if (toolVisu_.get(getStrRemp()).equals(Boolean.TRUE)) {
      vtools.add(toolList_.get(getStrRemp()));
    }
    if (toolVisu_.get(getStrFo()).equals(Boolean.TRUE)) {
      vtools.add(toolList_.get(getStrFo()));
    }
    if (toolVisu_.get(getStrPal()).equals(Boolean.TRUE)) {
      vtools.add(toolList_.get(getStrPal()));
    }
    final JComponent[] btTools = (JComponent[]) vtools.toArray(new JComponent[vtools.size()]);
    try {
      for (int i = 0; i < btTools.length; i++) {
        if (btTools[i] != null && btTools[i] instanceof BuPopupButton) {
          ((BuPopupButton) btTools[i]).setDesktop((BuDesktop) getDesktopPane());
        }
      }
    } catch (final ClassCastException e) {}
    return btTools;
  }

  /**
   * Construct tool buttons.
   */
  private void buildToolButtons() {
    // Selection buttons
    btSelection_ = new BuToggleButton[3];
    btSelection_[0] = new BuToggleButton((EbliResource.EBLI.getIcon("fleche")));
    btSelection_[0].setName("btSELPONCTUELLE");
    btSelection_[0].setActionCommand(getStrSelectPt());
    btSelection_[0].setToolTipText(EbliLib.getS("S�lection ponctuelle"));
    btSelection_[0].setEnabled(false);
    btSelection_[0].setMargin(new Insets(0, 0, 0, 0));
    btSelection_[0].addActionListener(this);
    toolList_.put(getStrSelectPt(), btSelection_[0]);
    toolVisu_.put(getStrSelectPt(), Boolean.TRUE);
    btSelection_[1] = new BuToggleButton((EbliResource.EBLI.getIcon("rectangle")));
    btSelection_[1].setName("btSELRECTANGLE");
    btSelection_[1].setActionCommand(getStrSelecRec());
    btSelection_[1].setToolTipText(EbliLib.getS("S�lection par rectangle"));
    btSelection_[1].setEnabled(false);
    btSelection_[1].setMargin(new Insets(0, 0, 0, 0));
    btSelection_[1].addActionListener(this);
    toolList_.put(getStrSelecRec(), btSelection_[1]);
    toolVisu_.put(getStrSelecRec(), Boolean.TRUE);
    btSelection_[2] = new BuToggleButton((EbliResource.EBLI.getIcon("polygone")));
    btSelection_[2].setName("btSELPOLYGONE");
    btSelection_[2].setActionCommand(getStrSelecPoly());
    btSelection_[2].setToolTipText(EbliLib.getS("S�lection par polygone"));
    btSelection_[2].setEnabled(false);
    btSelection_[2].setMargin(new Insets(0, 0, 0, 0));
    btSelection_[2].addActionListener(this);
    toolList_.put(getStrSelecPoly(), btSelection_[2]);
    toolVisu_.put(getStrSelecPoly(), Boolean.TRUE);
    // Navigation buttons
    final BuButton btRestaurer = new BuButton(EbliResource.EBLI.getIcon("loupe-etendue"));
    btRestaurer.setName("btRESTAURER_REPERE");
    btRestaurer.setActionCommand(getStrRestaure().intern());
    btRestaurer.setToolTipText(EbliLib.getS("Restaurer"));
    btRestaurer.setMargin(new Insets(0, 0, 0, 0));
    btRestaurer.addActionListener(this);
    btRestaurer.setRequestFocusEnabled(false);
    toolList_.put(getStrRestaure(), btRestaurer);
    toolVisu_.put(getStrRestaure(), Boolean.TRUE);
    final BuToggleButton btDeplacer = new BuToggleButton(EbliResource.EBLI.getIcon("main"));
    btDeplacer.setName("btDEPLACER_VUE");
    btDeplacer.setActionCommand(getStrDeplace());
    btDeplacer.setToolTipText(EbliLib.getS("D�placer la vue"));
    btDeplacer.setMargin(new Insets(0, 0, 0, 0));
    toolList_.put(getStrDeplace(), btDeplacer);
    toolVisu_.put(getStrDeplace(), Boolean.TRUE);
    final BuToggleButton btZoom = new BuToggleButton(EbliResource.EBLI.getIcon("loupe"));
    btZoom.setName("btAGRANDIR");
    btZoom.setActionCommand(getStrAgr());
    btZoom.setToolTipText(EbliLib.getS("Zoom +/-"));
    btZoom.setMargin(new Insets(0, 0, 0, 0));
    toolList_.put(getStrAgr(), btZoom);
    toolVisu_.put(getStrAgr(), Boolean.TRUE);
    final BControleRepereTexte crt = new BControleRepereTexte(vc_);
    crt.addRepereEventListener(vc_);
    // crt.addRepereEventListener(this);
    BuPopupButton btRepere;
    btRepere = new BuPopupButton(EbliLib.getS("Rep�re"), crt);
    btRepere.setName("REPERE");
    btRepere.setActionCommand(getStrChanger());
    btRepere.setMargin(new Insets(0, 0, 0, 0));
    btRepere.setIcon(EbliResource.EBLI.getIcon("repere"));
    btRepere.setToolTipText(EbliResource.EBLI.getString("Transformations du rep�re"));
    toolList_.put(getStrChanger(), btRepere);
    toolVisu_.put(getStrChanger(), Boolean.TRUE);
    final BControleNavigation ctNavTool = new BControleNavigation(vc_);
    // ctNavTool.addRepereEventListener(vc_);
    // ctNavTool.addRepereEventListener(this);
    BuPopupButton btNavTool;
    btNavTool = new BuPopupButton(EbliLib.getS("Navigation"), ctNavTool);
    btNavTool.setName("btNavigation");
    btNavTool.setActionCommand(getStrNav());
    btNavTool.setMargin(new Insets(0, 0, 0, 0));
    btNavTool.setIcon(EbliResource.EBLI.getIcon("navigation"));
    btNavTool.setToolTipText(EbliLib.getS("Outil de navigation"));
    toolList_.put(getStrNav(), btNavTool);
    toolVisu_.put(getStrNav(), Boolean.TRUE);
    // Navigation tool for column tools only
    ctNavigation_ = new BControleNavigation(vc_);
    // ctNavigation_.addRepereEventListener(vc_);
    // ctNavigation_.addRepereEventListener(this);
    // Standard buttons
    BuToolButton btForeground;
    btForeground = new BuToolButton();
    btForeground.setMargin(new Insets(0, 0, 0, 0));
    btForeground.setActionCommand(getStrCoul().intern());
    btForeground.setToolTipText(EbliLib.getS("Couleur de trac�"));
    btForeground.setIcon(BuResource.BU.getIcon("avantplan"));
    btForeground.addActionListener(this);
    toolList_.put(getStrCoul(), btForeground);
    toolVisu_.put(getStrCoul(), Boolean.TRUE);
    BuToolButton btBackground;
    btBackground = new BuToolButton();
    btBackground.setToolTipText(EbliLib.getS("Couleur de remplissage"));
    btBackground.setActionCommand(getStrRemp().intern());
    btBackground.setIcon(BuResource.BU.getIcon("arriereplan"));
    btBackground.addActionListener(this);
    btBackground.setMargin(new Insets(0, 0, 0, 0));
    toolList_.put(getStrRemp(), btBackground);
    toolVisu_.put(getStrRemp(), Boolean.TRUE);
    final BSelecteurReduitFonte srcft = new BSelecteurReduitFonte();
    srcft.addPropertyChangeListener(arbreModel_);
    BuPopupButton btFont;
    btFont = new BuPopupButton(EbliLib.getS("Fonte"), srcft);
    btFont.setToolTipText(EbliLib.getS("Choix de la fonte"));
    btFont.setActionCommand(getStrFo().intern());
    btFont.setIcon(BuResource.BU.getIcon("fonte"));
    btFont.setMargin(new Insets(0, 0, 0, 0));
    toolList_.put(getStrFo(), btFont);
    toolVisu_.put(getStrFo(), Boolean.TRUE);
    final BPaletteSelecteurReduitCouleur srcpl = new BPaletteSelecteurReduitCouleur();
    srcpl.addPropertyChangeListener(arbreModel_);
    BuPopupButton btPalette;
    btPalette = new BuPopupButton(EbliLib.getS("Palette"), srcpl);
    btPalette.setToolTipText(EbliLib.getS("Choix de la palette de couleurs"));
    btPalette.setIcon(EbliResource.EBLI.getIcon("palettecouleur"));
    btPalette.setActionCommand(getStrPal().intern());
    btPalette.setMargin(new Insets(0, 0, 0, 0));
    toolList_.put(getStrPal(), btPalette);
    toolVisu_.put(getStrPal(), Boolean.TRUE);
  }

  // BuPrintable
  /*
   * public void print(PrintJob _job, Graphics _g) { BuPrinter.INFO_DOC =new BuInformationsDocument();
   * BuPrinter.INFO_DOC.name=getTitle(); BuPrinter.INFO_DOC.logo=BuResource.BU.getIcon("calque");
   * BuPrinter.printComponent(_job,_g,getVueCalque()); }
   */
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    final BVueCalque bv = getVueCalque();
    final Graphics2D g2d = (Graphics2D) _g;
    g2d.setColor(Color.black);
    g2d.setFont(EbliPrinter.FONTBASE);
    final double x = _format.getImageableX();
    final double y = _format.getImageableY();
    g2d.translate(x, y);
    double wComp = bv.getWidth();
    double hComp = bv.getHeight();
    // recuperation des dimension de la zone imprimable
    final double hImprimable = _format.getImageableHeight();
    final double wImprimable = _format.getImageableWidth();
    final double facteur = Math.min(wImprimable / wComp, hImprimable / hComp);
    wComp = wComp * facteur;
    hComp = hComp * facteur;
    // definition de la zone d'impression
    final double xCentre = (wImprimable - wComp) / 2;
    final double yCentre = (hImprimable - hComp) / 2;
    g2d.translate(xCentre, yCentre);
    final Shape s = _g.getClip();
    g2d.clip(new Rectangle.Double(-5D, -5D, wComp + 10, hComp + 10));
    // mise a l'echelle et impression
    g2d.scale(facteur, facteur);
    bv.paint(g2d);
    // Restauration du graphics.
    g2d.scale(1 / facteur, 1 / facteur);
    g2d.translate(-xCentre, -yCentre);
    g2d.translate(-x, -y);
    g2d.setClip(s);
    return Printable.PAGE_EXISTS;
    // return EbliPrinter.printComponent(_g,_format,getVueCalque(),true,_numPage);
  }

  @Override
  public int getNumberOfPages() {
    return 1;
  }

  // SelectionListener
  /**
   * R�cup�ration des objets s�lectionn�s (a surcharger).
   */
  @Override
  public void selectedObjects(final SelectionEvent _evt) {}

  // TreeSelection (mise a jour des etats des boutons de Selection en fonction du calque)
  @Override
  public void valueChanged(final TreeSelectionEvent _evt) {
    if ((pnSelection_ == null) || (!pnSelection_.isVisible())) {
      return;
    }
    final Object[] cqs = _evt.getPath().getPath();
    cqActif_ = (BCalque) cqs[cqs.length - 1];
    // Le panneau des boutons de s�lection est invisible => Pas d'action.

    boolean enable = false;
    cqSelectionI_.removeAll();
    for (int i = 0; i < cqs.length; i++) {
      if (cqs[i] instanceof BCalqueAffichage && ((BCalqueAffichage) cqs[i]).contours().nombre() != 0) {
        enable = true;
        cqSelectionI_.add((BCalqueAffichage) cqs[i]);
      }
    }
    for (int i = 0; i < btSelection_.length; i++) {
      btSelection_[i].setEnabled(enable);
      /*
       * boolean enable=false; // boolean ponctuel=false; Object[] calques=_evt.getPath().getPath(); for(int i=0; i<calques.length;
       * i++) { if( calques[i] instanceof BCalqueAffichage ) enable=true; // if( (calques[i] instanceof BCalquePoint)|| //
       * (calques[i] instanceof BCalqueTexte) ) // ponctuel=true; } for(int b=0; b<btSelection_.length; b++) { // if(
       * btSelection_[b].getName().equals("btSELPONCTUELLE") ) // btSelection_[b].setEnabled(ponctuel); // else
       * btSelection_[b].setEnabled(enable); }
       */
    }
  }

  /**
   * @param _e
   */
  public void changeModeSelection(final ActionEvent _e) {
    final Object source = _e.getSource();
    if (btSelection_[0] == source) {
      cqSelectionI_.setModeSelection(BCalqueSelectionInteraction.PONCTUEL);
    } else if (btSelection_[1] == source) {
      cqSelectionI_.setModeSelection(BCalqueSelectionInteraction.RECTANGLE);
    } else if (btSelection_[2] == source) {
      cqSelectionI_.setModeSelection(BCalqueSelectionInteraction.POLYGONE);
    }
  }

  // Action (calqueContextuel)
  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String com = _e.getActionCommand();
    if (getStrRemp() == com) {
      Color bg = Color.white;
      if (cqActif_ != null) {
        bg = cqActif_.getBackground();
      }
      bg = JColorChooser.showDialog(this, EbliLib.getS("Couleur de fond"), bg);
      if ((cqActif_ != null) && (bg != null)) {
        cqActif_.setBackground(bg);
      }
    }
    if (getStrCoul() == com) {
      Color fg = Color.black;
      if (cqActif_ != null) {
        fg = cqActif_.getForeground();
      }
      fg = JColorChooser.showDialog(this, EbliLib.getS("Couleur de fond"), fg);
      if ((cqActif_ != null) && (fg != null)) {
        cqActif_.setForeground(fg);
      }
    }
    if (com.startsWith("SELECTION_")) {
      changeModeSelection(_e);
    } else if (com == getStrRestaure()) {
      restaurer();
    }
  }
  /**
   * Une classe d'association entre un groupe de boutons � 2 �tats et un calque d'interaction.
   * <p>
   * Lorsqu'un des boutons est actionn� (par une action de l'utilisateur), 2 op�rations sont effectu�es :
   * <p>
   * <ul>
   * <li>le calque d'interaction associ� est d�gel�. Les autres calques d'interaction de l'arbre sont gel�s.
   * <li>les autres boutons du groupe sont d�selectionn�s, ainsi que les boutons associ�s aux autres calques.
   * </ul>
   * <p>
   * Lorsque le calque est d�gel� (par une action de l'utilisateur ou par le programme), les op�rations suivantes sont
   * effectu�es :
   * <p>
   * <ul>
   * <li>les autres calques de l'arbre sont gel�s.</li>
   * <li>le dernier bouton du groupe a avoir �t� actionn� est s�lectionn�, les autres boutons sont d�selectionn�s.</li>
   * </ul>
   */
  class AssociationCalqueBouton implements PropertyChangeListener, ActionListener {
    BCalqueInteraction cqi_;
    AbstractButton[] bts_;
    AbstractButton btLastActive_;

    /**
     * Association d'un bouton et d'un calque d'interaction.
     */
    public AssociationCalqueBouton(final BCalqueInteraction _cqi, final AbstractButton _bt) {
      this(_cqi, new AbstractButton[] { _bt });
    }

    /**
     * Association d'un groupe de boutons et d'un calque d'interaction.
     */
    public AssociationCalqueBouton(final BCalqueInteraction _cqi, final AbstractButton[] _bts) {
      cqi_ = _cqi;
      bts_ = _bts;
      btLastActive_ = bts_[0];
      cqi_.addPropertyChangeListener("gele", this);
      for (int i = 0; i < bts_.length; i++) {
        bts_[i].addActionListener(this);
      }
    }

    @Override
    public void propertyChange(final PropertyChangeEvent _evt) {
      for (int i = 0; i < bts_.length; i++) {
        if (bts_[i] != btLastActive_) {
          bts_[i].setSelected(false);
        }
      }
      btLastActive_.setSelected(!((Boolean) _evt.getNewValue()).booleanValue());
    }

    @Override
    public void actionPerformed(final ActionEvent _evt) {
      btLastActive_ = (AbstractButton) _evt.getSource();
      for (int i = 0; i < bts_.length; i++) {
        if (bts_[i] != btLastActive_) {
          bts_[i].setSelected(false);
        }
      }
      if (btLastActive_.isSelected()) {
        activeCalqueInteraction(cqi_);
      } else {
        cqi_.setGele(true);
      }
    }
  }

  @Override
  public JPopupMenu getCmdsContextuelles() {
    return null;
  }
}
