package org.fudaa.ebli.calque.edition;

/**
 *
 * @author deniger ( genesis)
 */
public interface ZSceneEditorListener {

  void globalMovedStart();

  void globalMovedEnd();

  void globalRotationStart();

  void globalRotationEnd();
}
