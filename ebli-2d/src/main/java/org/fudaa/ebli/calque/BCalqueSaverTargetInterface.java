/*
 *  @creation     15 sept. 2005
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

/**
 * @author Fred Deniger
 * @version $Id: BCalqueSaverTargetInterface.java,v 1.2 2006-04-12 15:27:09 deniger Exp $
 */
public interface BCalqueSaverTargetInterface {

}
