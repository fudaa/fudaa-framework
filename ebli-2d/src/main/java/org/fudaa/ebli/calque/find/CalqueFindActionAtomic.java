/*
 * @creation     23 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.find;

import com.memoire.fu.FuLog;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZCalqueGeometry;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.find.EbliFindComponent;
import org.fudaa.ebli.find.EbliFindComponentExpr;
import org.fudaa.ebli.find.EbliFindable;
import org.nfunk.jep.Variable;

/**
 * Une action de recherche permettant de rechercher des geometries ou des sommets. Cette action r�agit au changement de mode de
 * selection sommet<->geometrie.
 * @author Bertrand Marchand
 * @version $Id$
 */

public class CalqueFindActionAtomic extends CalqueFindActionDefault implements PropertyChangeListener {
  /** Le panneau de recherche par formule recup�r� pour �tre mis a jour en cas de changement de mode de selection */
  EbliFindComponentExpr exprComp_;
  /** Le panneau de recherche par polygone recup�r� pour �tre mis a jour en cas de changement de mode de selection */
  CalqueFindComponentPolygone polyComp_;

  /**
   * Cr�ation de l'action
   * @param _item Le calque associ� � la recherche.
   */
  public CalqueFindActionAtomic(ZCalqueGeometry _calque) {
    super(_calque);
    // Attention : L'instance ne doit pas �tre cr��e plus d'une fois par calque, car elle ecoute un changement de propri�t�
    // sur ce calque.
    _calque.addPropertyChangeListener("mode",this);
  }
  
  @Override
  public boolean find(final Object _idx, final String _action, final int _selOption, final EbliFindable _parent) {
    if (_action.startsWith(CalqueFindComponentPolygone.getID())) {
      return findPolygone(_idx, _action, _selOption, _parent);
    }
    final String exp = (String) _idx;
    final EbliListeSelectionMulti selection = createSelection(exp, _action, _selOption);
    return selection == null ? false : ((ZCalqueAffichageDonnees)layer_).changeSelection(selection, _selOption);
  }
  
  /**
   * Cr�e une selection de sommet ou de geometries, suivant qu'on est en mode atomique ou en mode global.
   * @param _idx La chaine utilis�e pour la recherche (indexs ou formule)
   * @param _action L'onglet a partir duquel on effectue la recherche
   * @param _selOption L'option de selection (ajout, suppression, etc).
   * @return
   */
  protected EbliListeSelectionMulti createSelection(final String _idx, final String _action, final int _selOption) {
    EbliListeSelectionMulti selection = null;
    if ("IDX".equals(_action)) {
      final int[] idx = getIndex(_idx);
      if (idx != null && !((ZCalqueGeometry)layer_).isAtomicMode()) {
        selection = new EbliListeSelectionMulti(idx.length);
        for (int i : idx) {
          selection.set(i,new CtuluListSelection(0));
        }
      }
    } else if ("EXPR".equals(_action)) {
      if (FuLog.isTrace()) {
        FuLog.trace("find from expression");
      }
      if (expr_.getParser().hasError()) {
        return null;
      }
      final int nb = getNbValuesToTest();
      selection = new EbliListeSelectionMulti(nb);
      final Variable[] vars =expr_.findUsedVar();
      final Object[] values=new Object[vars.length];
//      CtuluListSelection old = null;
//      if (_selOption == EbliSelectionState.ACTION_AND || _selOption == EbliSelectionState.ACTION_DEL) {
//        old = getOldSelection();
//      }
//      final int min = old == null ? 0 : old.getMinIndex();
//      final int max = old == null ? nb - 1 : old.getMaxIndex();
      final int min = 0;
      final int max = nb - 1;
      
      
      for (int idxGeom = max; idxGeom >= min; idxGeom--) {
//        if (old == null || old.isSelected(i)) {
        if (vars!=null) {
          exprFiller_.majVariable(idxGeom, vars);

          // Mode atomique.
          if (((ZCalqueGeometry)layer_).isAtomicMode()) {
            // On recupere le tableau pour chaque atomique stock� dans la valeur de la variable
            for (int i=0; i<values.length; i++) {
              values[i]=vars[i].getValue();
            }
            
            int nbAtomics=((ZCalqueGeometry)layer_).modeleDonnees().getNbPointForGeometry(idxGeom);
            
            for (int iat=0; iat<nbAtomics; iat++) {
              for (int j=0; j<vars.length; j++) {
                vars[j].setValue(((Object[])values[j])[iat]);
              }
              final int val=(int)expr_.getParser().getValue();
              if (val==1) {
                selection.add(idxGeom, iat);
              }
            }
          }
          
          // Mode global
          else {
            final int val=(int)expr_.getParser().getValue();
            if (val==1) {
              // La liste des sous objets est vide, mais pr�sente.
              selection.set(idxGeom,new CtuluListSelection(0));
            }
          }
        }
        // }
      }
    }
    return selection;
  }
  
  
  @Override
  protected EbliFindComponent buildExprComp() {
    exprComp_=(EbliFindComponentExpr)super.buildExprComp();
    exprComp_.setFindOnAtomic(((ZCalqueGeometry)layer_).isAtomicMode());
    
    return exprComp_;
  }
  
  @Override
  protected EbliFindComponent buildPolygoneComponent(final EbliFindable _parent) {
    polyComp_ = new CalqueFindComponentPolygone(((ZEbliCalquesPanel) _parent).getDonneesCalque());
    polyComp_.setFindOnAtomic(((ZCalqueGeometry)layer_).isAtomicMode());
    return polyComp_;
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    if ("mode".equals(_evt.getPropertyName())) {
      boolean isAtomic=ZCalqueGeometry.SelectionMode.ATOMIC.equals(_evt.getNewValue());
      
      buildExpr();
      if (exprComp_!=null) {
        exprComp_.setExpr(expr_);
        exprComp_.setFindOnAtomic(isAtomic);
      }
      
      if (polyComp_!=null)
        polyComp_.setFindOnAtomic(isAtomic);
    }
  }
}
