/*
 * @creation 21 nov. 06
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.Geometry;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gui.CtuluCellRenderer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un TreeModel repr�sentant les calques contenant des g�om�tries. Ces g�om�tries peuvent �tre filtr�es par type.
 * Ce mod�le est automatiquement mis a jour si une instance de l'arbre des calque lui est fourni.
 * 
 * @author Fred Deniger, Bertrand Marchand
 * @version $Id$
 */
public class CalqueGISTreeModel extends DefaultTreeModel implements Observer {

  BGroupeCalque rootLayer_;
  final CalqueGeometryVisitor finder_ = new CalqueGeometryVisitor();
  GISVisitorChooser chooser_ = new GISVisitorChooser();

  /**
   * Le renderer pour l'arbre, gerant la non selection des calques si non autoris�.
   * @author Bertrand Marchand
   */
  public static class CellRenderer extends JLabel implements TreeCellRenderer {
    final Color selectedBackground_ = UIManager.getColor("Tree.selectionBackground");
    final Color selectedForground_ = UIManager.getColor("Tree.selectionForeground");
    final Border focusBorderColor_ = BorderFactory.createLineBorder(UIManager.getColor("Tree.selectionBorderColor"), 1);
    final boolean onlyLineSelectable_;

    public CellRenderer(final boolean _onlyGeomSelectable) {
      this.setOpaque(true);
      onlyLineSelectable_ = _onlyGeomSelectable;
    }

    protected boolean isSelectable(final Object _value, final boolean _leaf) {
      if (onlyLineSelectable_) {
        if (!_leaf) {
          return false;
        }
        final LayerNode node = (LayerNode) _value;
        return node.idxGeom_ >= 0;
      }
      return true;

    }

    @Override
    public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
        final boolean _expanded, final boolean _leaf, final int _row, final boolean _hasFocus) {
      this.setFont(_tree.getFont());
      setIcon(((LayerNode) _value).getIcon());
      setText(_value.toString());
      final boolean selectable = isSelectable(_value, _leaf);
      if (_selected && selectable) {
        setBackground(selectedBackground_);
        setForeground(selectedForground_);
      } else {
        setBackground(_tree.getBackground());
        setForeground(_tree.getForeground());
      }
      if (selectable) {
        setToolTipText(getText());
      } else {
        setToolTipText(getText() + ": " + CtuluLib.getS("Non s�lectionnable"));
      }
      setBorder((selectable && _hasFocus) ? focusBorderColor_ : CtuluCellRenderer.BORDER_NO_FOCUS);
      setEnabled(_tree.isEnabled());
      return this;
    }
  }

  /**
   * Modele de selection de l'arbre, gerant la non selection des calques si non autoris�.
   * @author Bertrand Marchand
   */
  public static class OnlyGeomTreeSelectionModel extends DefaultTreeSelectionModel {

    @Override
    public void setSelectionPaths(final TreePath[] _paths) {
      super.setSelectionPaths(getCorrectPaths(_paths));
    }

    @Override
    public void addSelectionPaths(final TreePath[] _paths) {
      super.addSelectionPaths(getCorrectPaths(_paths));
    }

    private TreePath[] getCorrectPaths(final TreePath[] _paths) {
      TreePath[] paths = _paths;
      if (!CtuluLibArray.isEmpty(_paths)) {
        final List<TreePath> correctPath = new ArrayList<TreePath>(paths.length);
        for (int i = 0; i < paths.length; i++) {
          if (paths[i] != null && paths[i].getLastPathComponent() != null
              && ((LayerNode) paths[i].getLastPathComponent()).idxGeom_ >= 0) {
            correctPath.add(paths[i]);
          }
        }
        if (correctPath.size() != paths.length) {
          paths=correctPath.toArray(new TreePath[correctPath.size()]);
        }
      }
      return paths;
    }

  }

  /**
   * Un noeud de l'arbre, customis� suivant le nom de la g�om�trie et son type.
   * @author Bertrand Marchand
   */
  public static class LayerNode extends DefaultMutableTreeNode {

    int idxGeom_ = -1;
    String titre_;
    final Icon icon_;

    public LayerNode(final BCalque _userObject) {
      super(_userObject, true);
      icon_ = CtuluLibImage.resize(_userObject, 16, 16);
    }

    public LayerNode(final ZCalqueAffichageDonneesAbstract _userObject, final int _idx, final String _titre) {
      super(_userObject, false);
      idxGeom_ = _idx;
      titre_ = _titre;
      
      if (_userObject instanceof ZCalqueMultiPoint) {
        icon_ = EbliResource.EBLI.getIcon("draw-multipoint");
      }
      else if (_userObject instanceof ZCalquePoint) {
        icon_ = EbliResource.EBLI.getIcon("draw-add-pt");
      }
      else {
        if (((ZCalqueLigneBrisee)_userObject).modeleDonnees().isGeometryFermee(_idx)) {
          icon_=EbliResource.EBLI.getIcon("draw-polygon");
        }
        else {
          icon_=EbliResource.EBLI.getIcon("draw-polyline");
        }
      }
    }

    @Override
    public String toString() {
      if (titre_ != null) {
        return titre_;
      }
      return ((BCalque) userObject).getTitle();
    }

    public Icon getIcon() {
      return icon_;
    }

    public int getIdxGeom() {
      return idxGeom_;
    }

  }

  /** 
   * Cr�ation du modele a partir de la racine des calques.
   */
  public CalqueGISTreeModel(final BArbreCalqueModel _model, final BGroupeCalque _root) {
    this(_model,_root,GISLib.MASK_ALL);
  }
  public CalqueGISTreeModel(final BArbreCalqueModel _model, final BGroupeCalque _root,int mask) {
    super(new LayerNode(_root));
    finder_.setMask(mask);
    if (_model != null) {
      _model.getObservable().addObserver(this);
    }
    setRootLayer(_root);
  }
  
  /**
   * Definit le calque racine.
   * @param _root Le calque racine.
   */
  public void setRootLayer(BGroupeCalque _root) {
    setRoot(new LayerNode(_root));
    rootLayer_=_root;
    
    rebuildTree();
  }

  /**
   * Reconstruit l'arbre.
   */
  public final void rebuildTree() {
    ((LayerNode)getRoot()).removeAllChildren();
    addLayer((LayerNode)getRoot(), rootLayer_);

    fireTreeStructureChanged(this, new Object[]{getRoot()}, null, null);
  }

  protected boolean containsGeometries(final BCalque _cq) {
    finder_.resetFound();
    _cq.apply(finder_);
    return finder_.isFound();
  }

  /**
   * Remplit la liste avec les geometries du noeud.
   * @param _nd Le noeud.
   * @param _dest La liste de destination.
   */
  public void fillWithGeometries(LayerNode _nd, final List<Geometry> _dest) {
    if (_nd.idxGeom_ >= 0) {
      final ZModeleGeometry md=(ZModeleGeometry)((ZCalqueAffichageDonneesAbstract)_nd.getUserObject()).modeleDonnees();
      _dest.add(md.getGeomData().getGeometry(_nd.idxGeom_));
    } else if (((BCalque) _nd.getUserObject()).isGroupeCalque()) {
      final BCalque[] cqs = ((BCalque) _nd.getUserObject()).getTousCalques();
      if (cqs != null) {
        for (int i = cqs.length - 1; i >= 0; i--) {
          fillWithLayerGeometries(_dest, cqs[i]);
        }
      }

    } else {
      fillWithLayerGeometries(_dest, (BCalque) _nd.getUserObject());
    }

  }

  private void fillWithLayerGeometries(final List<Geometry> _dest, final BCalque _cq) {
    if (_cq instanceof ZCalqueAffichageDonneesAbstract) {
      final GISZoneCollection collec = ((ZModeleGeometry)((ZCalqueAffichageDonneesAbstract)_cq).modeleDonnees()).getGeomData();
      final GISVisitorGeometryCollector visitor = new GISVisitorGeometryCollector(finder_.getMask());
      collec.accept(visitor);
      _dest.addAll(visitor.getGeometries());
    }
  }

  void addLayer(final LayerNode _parent, final BGroupeCalque _gc) {
    final BCalque[] cq = _gc.getCalques();
    if (CtuluLibArray.isEmpty(cq)) {
      return;
    }
    for (int i = 0; i < cq.length; i++) {
      final LayerNode node = new LayerNode(cq[i]);
      if (finder_.isMatching(cq[i])) {
        _parent.add(node);
        addGeometries(node, (ZCalqueAffichageDonneesAbstract) cq[i]);
      } 
      else if (cq[i].isGroupeCalque() && containsGeometries(cq[i])) {
        _parent.add(node);
        addLayer(node, (BGroupeCalque) cq[i]);
      }
    }
  }

  void addGeometries(final LayerNode _parent, final ZCalqueAffichageDonneesAbstract _cq) {
    final GISZoneCollection collec = ((ZModeleGeometry)_cq.modeleDonnees()).getGeomData();
    final GISAttributeModelObjectInterface nameAtt = (GISAttributeModelObjectInterface) collec.getModel(GISAttributeConstants.TITRE);
    final int nb = collec.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      chooser_.clear();
      ((GISGeometry) collec.getGeometry(i)).accept(chooser_);
      
      boolean badd=false;
      String name=null;
      if (chooser_.isMultiPoint() && (finder_.getMask()&GISLib.MASK_MULTIPOINT)!=0) {
        badd=true;
        name="Multipoint {0}";
      }
      else if (chooser_.isPt() && (finder_.getMask()&GISLib.MASK_POINT)!=0) {
        badd=true;
        name="Point {0}";
      }
      else if (chooser_.isPolyligne() && (finder_.getMask()&GISLib.MASK_POLYLINE)!=0) {
        badd=true;
        name="Ligne ouverte {0}";
      }
      else if (chooser_.isPolygone() && (finder_.getMask()&GISLib.MASK_POLYGONE)!=0) {
        badd=true;
        name="Ligne ferm�e {0}";
      }

      if (badd) {
        if (nameAtt!=null)
          name=(String)nameAtt.getValue(i);
        else
          name=EbliLib.getS(name, CtuluLibString.getString(i+1));
        _parent.add(new LayerNode(_cq, i, name));
      }
    }
  }

  @Override
  public void update(final Observable _o, final Object _arg) {
    rebuildTree();
  }

  /**
   * Filtre sur les g�om�tries. Par defaut, toutes g�om�tries.
   */
  public void setMask(int _mask) {
    finder_.setMask(_mask);
    rebuildTree();
  }

  /**
   * Creation d'une vue pour le modele.
   */
  public JTree createView(final boolean _onlyGeomSelected, final boolean _multiSelection) {
    final JTree tree = new JTree(this);
    tree.setEditable(false);
    tree.setShowsRootHandles(true);
    tree.setExpandsSelectedPaths(true);
    tree.setCellRenderer(new CellRenderer(_onlyGeomSelected));
    tree.setRootVisible(false);
    tree.setFocusable(true);
    if (_onlyGeomSelected) {
      final TreeSelectionModel onlyGeomSelectionModel = new OnlyGeomTreeSelectionModel();
      tree.setSelectionModel(onlyGeomSelectionModel);
    }
    
    if (_multiSelection)
      tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    else 
      tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    return tree;
  }

}
