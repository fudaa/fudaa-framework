/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.awt.event.ActionEvent;
import javax.swing.tree.TreePath;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action pour d�truire un ou plusieurs calque sur un arbre.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class TreeDeleteCalqueAction extends EbliActionSimple {

  protected BArbreCalqueModel treeModel_;

  /**
   * Construit l'action.
   *
   * @param _treeModel Le modele d'arbre.
   */
  public TreeDeleteCalqueAction(BArbreCalqueModel _treeModel) {
    super(EbliLib.getS("Supprimer"), EbliResource.EBLI.getIcon("detruire"), "DETRUIRE");
    treeModel_ = _treeModel;
  }

  @Override
  public void updateStateBeforeShow() {
    super.updateStateBeforeShow();
    setEnabled(treeModel_ != null && treeModel_.isEditable());
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (treeModel_ == null || !treeModel_.isEditable()) {
      return;
    }

    final BCalque[] c = treeModel_.getSelection();
    if (c.length == 0) {
      return;
    }
    final TreePath[] parent = treeModel_.getSelectionParent();
    for (int i = 0; i < c.length; i++) {
      c[i].detruire();
    }
    treeModel_.getTreeSelectionModel().setSelectionPaths(parent);
  }
}
