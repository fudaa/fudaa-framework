/*
 * @creation 19 janv. 07
 * @modification $Date: 2008-01-17 11:42:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;

/**
 * Une classe abstraite pour un groupe de calque pouvant contenir des calques SIG points
 * ou polylignes. Cette classe g�re �galement les commandes undo-redo.
 * 
 * @author fred deniger
 * @version $Id: ZCalqueEditionGroup.java,v 1.1.6.1 2008-01-17 11:42:50 bmarchan Exp $
 */
public abstract class ZCalqueEditionGroup extends BGroupeCalque {

  /**
   * 
   */
  public ZCalqueEditionGroup() {
    super();
  }
  
  
  public boolean canAddNewLayer(){
    return true;
  }

  /**
   * Les attributs du calques, en nombre quelconque.
   * @return Les attributs.
   */
  public abstract GISAttributeInterface[] getAttributes();

  public abstract CtuluCommandContainer getCmdMng();

  public abstract GISZoneCollectionLigneBrisee createLigneBriseeZone();

  public abstract GISZoneCollectionPoint createPointZone();

  public abstract ZCalqueLigneBrisee addLigneBriseeLayerAct(final String _title,
      final GISZoneCollectionLigneBrisee _zone, final CtuluCommandContainer _cmd);

  public abstract ZCalquePointEditable addPointLayerAct(final String _title, final GISZoneCollectionPoint _zone,
      final CtuluCommandContainer _cmd);

}