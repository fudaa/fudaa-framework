/*
 * @creation     29 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.Geometry;

/**
 * Cette interface permet l'�coute des changements concernant les g�om�tries.
 * Les changements sont : l'ajout d'une g�om�trie, la supression d'une
 * g�om�trie et la modification d'une g�om�trie.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public interface ZModelListener {
  /** Action indiquant l'ajout d'une g�om�trie dans le conteneur de g�om�tries. */
  public static final int GEOMETRY_ACTION_ADD=2;
  /**
   * Action indiquant la supression d'une g�om�trie dans le conteneur de
   * g�om�tries.
   */
  public static final int GEOMETRY_ACTION_REMOVE=3;
  /** Action indiquant la modification d'une g�om�trie */
  public static final int GEOMETRY_ACTION_MODIFY=4;

  /**
   * M�thode appel�e lorsqu'une action est effectu�e sur une g�om�trie.
   * 
   * @param _source
   *          L'instance appelant geometryAction.
   * @param _indexGeom
   *          L'index de la g�om�trie. Si l'action est l'ajout ou la
   *          modification, l'index est valide. Si l'action est la supression,
   *          l'index correspond � l'ancienne emplacement et donc n'est plus
   *          valide. Si �gale � -1, toutes les g�om�tries ont pu �tre
   *          modifi�es.
   * @param _geom
   *          La g�om�trie concern�e. Si null toutes les g�om�tries ont pu �tre
   *          modifi�es.
   * @param _action
   *          L'action effectu�e sur la g�om�trie. Voir les GEOMETRY_ACTION_*.
   */
  public void geometryAction(Object _source, int _indexGeom, Geometry _geom, int _action);

}
