/*
 *  @creation     4 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLabel;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.operation.valid.IsValidOp;
import gnu.trove.TIntObjectIterator;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluNamedCommand;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleArray;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.ZModelListener;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZModeleLigneBriseeEditable extends ZModeleLigneBriseeDefault {

  public ZModeleLigneBriseeEditable() {
    this(new GISZoneCollectionLigneBrisee());
  }

  public ZModeleLigneBriseeEditable(final GISZoneCollectionLigneBrisee _zone) {
    super(_zone);
  }

  protected Object[] createData(final LineString _s, final ZEditionAttributesDataI _d, final CtuluCommandComposite cmp) {
    if (_d == null || geometries_.getNbAttributes() == 0) {
      FuLog.debug("EZM: " + getClass().getName() + " data NO present");
      return null;
    }
    FuLog.debug("EZM: " + getClass().getName() + " data is present");
    final Object[] r = new Object[geometries_.getNbAttributes()];
    for (int i = r.length - 1; i >= 0; i--) {
      final GISAttributeInterface att = geometries_.getAttribute(i);
      if (att.isAtomicValue()) {
        final Object[] ri = new Object[_d.getNbVertex()];
        for (int gi = ri.length - 1; gi >= 0; gi--) {
          ri[gi] = _d.getValue(att, gi);
          FuLog.debug("EZM: ajout val atomic " + ri[gi]);
        }
        r[i] = ri;
      } else {
        r[i] = _d.getValue(att, 0);
        FuLog.debug("EZM: ajout val non atomic " + r[i]);
      }
    }
    return r;
  }

  protected GISZoneCollectionLigneBrisee createGeometry() {
    return new GISZoneCollectionLigneBrisee(null);
  }

  protected boolean isIntersectedLigneBriseeAccepted() {
    return true;
  }

  protected boolean isNewPolygoneOk(final GISPolygone _p, final CtuluUI _ui) {
    return true;
  }

  protected boolean isNewPolyligneOk(final GISPolyligne _p, final CtuluUI _ui) {
    return true;
  }

  protected boolean needData() {
    return false;
  }

  protected boolean removeAtomicObjects(final EbliListeSelectionMultiInterface _selection,
          final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (cantModify()) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final TIntObjectIterator iterator = _selection.getIterator();
    //on parcourt les lignes concern�es par la suppression
    for (int i = _selection.getNbListSelected(); i-- > 0;) {
      iterator.advance();
      //l'indice de la ligne
      final int idxLigne = iterator.key();
      setGeomModif(idxLigne, _cmd); // Modification de l'�tat de la g�om�trie
      //les points a enlever
      final CtuluListSelectionInterface sel = (CtuluListSelectionInterface) iterator.value();
      geometries_.removeAtomics(idxLigne, sel, _ui, cmp);
      sel.clear();
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return true;
  }

  /**
   * Ajout d'une geometrie. Celle ci doit �tre de type LineString uniquement.
   *
   * @param _p la geometrue a ajouter
   * @param _cmd le receveur de commandes
   * @param _ui l'interface utilisateur
   * @param _d les donn�es associ�es � la polyligne
   * @return true si ajoute
   */
  public boolean addGeometry(final Geometry _p, final CtuluCommandContainer _cmd, final CtuluUI _ui,
          final ZEditionAttributesDataI _d) {
    if (_p.getNumPoints() < 2) {
      _ui.error(null, EbliLib.getS("La polyligne doit contenir {0} points au moins", CtuluLibString.DEUX), false);
      return false;
    }
    if (!(_p instanceof LineString)) {
      throw new IllegalArgumentException("Bad geometry type");
    }

    if (geometries_ == null) {
      geometries_ = createGeometry();
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final Object[] data = createData((LineString) _p, _d, cmp);
    if (data == null && needData()) {
      if (_ui != null) {
        _ui.error(null, EbliLib.getS("Des donn�es manquent"), false);
      }
      return false;
    }
    geometries_.addGeometry(_p, data, cmp);
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return true;
  }

  /**
   * @param _p la polygone a ajouter
   * @param _cmd le receveur de commandes
   * @param _ui l'interface utilisateur
   * @param _d les donn�es associ�es � la polyligne
   * @return true si ajoute
   */
  public boolean addGeometry(final GrPolygone _p, final CtuluCommandContainer _cmd, final CtuluUI _ui,
          final ZEditionAttributesDataI _d) {
    FuLog.debug("EZM: ajout polygone");
      
    if (_p.sommets_.nombre() < 3) {
      _ui.error(null, getErrorMinPt(), false);
      return false;
    }
    if (geometries_ == null) {
      geometries_ = createGeometry();
    }
    if (!isIntersectedLigneBriseeAccepted() && !_p.estSimple()) {
      if (_ui != null) {
        _ui.error(null, EbliLib.getS("Les formes complexes ne sont pas accept�es"), false);
      }
      return false;
    }
    final GISPolygone poly = (GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(GISGeometryFactory.INSTANCE
            .getCoordinateSequenceFactory().create(_p.sommets_.createCoordinateSequence(true)));
    final boolean r = isNewPolygoneOk(poly, _ui);
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final Object[] data = createData(poly, _d, cmp);
    if (data != null) {
      FuLog.debug("EZM: nb data proposed " + data.length);
      for (int i = 0; i < data.length; i++) {
        FuLog.debug("EZM: data " + i + " = " + data[i]);
      }
    }
    if (data == null && needData()) {
      if (_ui != null) {
        _ui.error(null, EbliLib.getS("Des donn�es manquent"), false);
      }
      return false;
    }
    if (r) {
      getGeomData().addPolygone(poly, data, cmp);
      if (_cmd != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
    }
    return r;
  }

  private String getErrorMinPt() {
    return EbliLib.getS("La ligne brisee doit contenir {0} points au moins",
            CtuluLibString.TROIS);
  }

  /**
   * @param _p la polyligne a ajouter
   * @param _cmd le receveur de commandes
   * @param _ui l'interface utilisateur
   * @param _d les donn�es associ�es � la polyligne
   * @return true si ajoute
   */
  public boolean addGeometry(final GrPolyligne _p, final CtuluCommandContainer _cmd, final CtuluUI _ui,
          final ZEditionAttributesDataI _d) {
    if (_p.sommets_.nombre() < 2) {
      _ui.error(null, getErrorMin2Pt(), false);
      return false;
    }
    if (geometries_ == null) {
      geometries_ = createGeometry();
    }
    if (!isIntersectedLigneBriseeAccepted() && !_p.estSimple()) {
      if (_ui != null) {
        _ui.error(null, EbliLib.getS("Les formes complexes ne sont pas accept�es"), false);
      }
      return false;
    }
    final GISPolyligne poly = (GISPolyligne) GISGeometryFactory.INSTANCE
            .createLineString(GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(
            _p.sommets_.createCoordinateSequence(false)));
    final boolean r = isNewPolyligneOk(poly, _ui);
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final Object[] data = createData(poly, _d, cmp);
    if (data == null && needData()) {
      if (_ui != null) {
        _ui.error(null, EbliLib.getS("Des donn�es manquent"), false);
      }
      return false;
    }
    if (r) {
      getGeomData().addPolyligne(poly, data, cmp);
      if (_cmd != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
    }
    return r;
  }

  private String getErrorMin2Pt() {
    return EbliLib.getS("La ligne brisee doit contenir {0} points au moins",
            CtuluLibString.DEUX);
  }

  /**
   * Ajoute un sommet 3D � une g�om�trie.
   *
   * @param _ligneIdx L'indice de la g�om�trie
   * @param _idxBefore L'indice de sommet pr�c�dent pour l'insertion. Peut �tre �gal � -1.
   * @param _x La coordonn�e X du sommet � inserer
   * @param _y La coordonn�e Y du sommet � inserer
   * @param _z La coordonn�e Z du sommet � inserer. Peut �tre null. Dans ce cas, le z est interpol� depuis les autres valeurs.
   * @param _cmd Le container de commande.
   */
  public void addPoint(final int _ligneIdx, final int _idxBefore, final double _x, final double _y, final Double _z, final CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return;
    }

    CtuluCommandComposite cmp = new CtuluCommandComposite();

    // Modification de l'�tat de la g�om�trie
    setGeomModif(_ligneIdx, cmp);
    geometries_.addAtomic(_ligneIdx, _idxBefore, _x, _y, cmp);

    // Mise a jour du Z si necessaire.
    GISAttributeInterface attZ = geometries_.getAttributeIsZ();
    if (_z != null && attZ != null && attZ.isAtomicValue()) {
      GISAttributeModel md = (GISAttributeModel) geometries_.getModel(attZ).getObjectValueAt(_ligneIdx);
      md.setObject(_idxBefore + 1, _z, cmp);
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * Scinde en 2 une polyligne au sommet d�sign�. Les attributs globaux sont recopi�es dans chacune des 2 polylignes, les attributs atomiques sont
   * dispatch�s.
   *
   * @param _ligneIdx L'index de ligne.
   * @param _idx Les indices des 2 sommets cons�cutifs sur la ligne.
   * @param _cmd Le conteneur de commandes.
   * @return Les indices des 2 nouvelles lignes cr��es si tout s'est bien pass�, null sinon.
   */
  public int[] splitGeometry(final int _ligneIdx, final int[] _idx, final CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return null;
    }
    //TODO gere le cas des lignes ferm�es.
    //si ligne fermee il faut remplacer la ligne ferme par ligne ouverte
    final GISPolyligne line = (GISPolyligne) geometries_.getGeometry(_ligneIdx); // Old line
    if (line == null || _idx[0] <= 0 || _idx[1] >= line.getNumPoints() - 1) {
      return null;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Scission de la g�om�trie"));

    // Les coordonn�es
    final Coordinate[] clold = line.getCoordinates();
    final Coordinate[] cline1 = new Coordinate[_idx[0] + 1];
    for (int i = 0; i < cline1.length; i++) {
      cline1[i] = (Coordinate) clold[i].clone();
    }
    final Coordinate[] cline2 = new Coordinate[clold.length - _idx[1]];
    for (int i = 0; i < cline2.length; i++) {
      cline2[i] = (Coordinate) clold[_idx[1] + i].clone();
    }

    // Les datas
    final int iattHisto = geometries_.getIndiceOf(GISAttributeConstants.HISTORY);

    final Object[] dataline1 = new Object[getAttributeNb()];
    final Object[] dataline2 = new Object[getAttributeNb()];
    for (int i = 0; i < getAttributeNb(); i++) {
      final GISAttributeModel attmodel = geometries_.getModel(i);
      if (attmodel.getAttribute().isAtomicValue()) {
        final GISAttributeModel atomModel1 = attmodel.getAttribute().createAtomicModel(null, cline1.length);
        for (int j = 0; j < cline1.length; j++) {
          atomModel1.setObject(j, ((GISAttributeModel) attmodel.getObjectValueAt(_ligneIdx)).getObjectValueAt(j), null);
        }
        final GISAttributeModel atomModel2 = attmodel.getAttribute().createAtomicModel(null, cline2.length);
        for (int j = 0; j < cline2.length; j++) {
          atomModel2.setObject(j, ((GISAttributeModel) attmodel.getObjectValueAt(_ligneIdx)).getObjectValueAt(j
                  + cline1.length), null);
        }

        dataline1[i] = atomModel1;
        dataline2[i] = atomModel2;
      } else {
        // Cas particulier de l'attribut TITRE => Conserv�, op�ration dans l'historique
        if (attmodel.getAttribute() == GISAttributeConstants.TITRE) {
          final String name = (String) attmodel.getObjectValueAt(_ligneIdx);
          dataline1[i] = name;
          dataline2[i] = name + "_";

          if (iattHisto != -1) {
            dataline1[iattHisto] = GISLib.appendHistory(EbliLib.getS("Scission"), name, (String) geometries_.getModel(iattHisto).getObjectValueAt(_ligneIdx));
            dataline2[iattHisto] = GISLib.appendHistory(EbliLib.getS("Scission"), name, (String) geometries_.getModel(iattHisto).getObjectValueAt(_ligneIdx));
          }
        } // Cas particulier de l'historique => Deja trait�
        else if (attmodel.getAttribute() == GISAttributeConstants.HISTORY) {
        } // Cas particulier de l'attribut ETAT_GEOM => ATT_VAL_ETAT_MODI
        else if (attmodel.getAttribute() == GISAttributeConstants.ETAT_GEOM) {
          dataline1[i] = GISAttributeConstants.ATT_VAL_ETAT_MODI;
          dataline2[i] = GISAttributeConstants.ATT_VAL_ETAT_MODI;
        } else {
          dataline1[i] = attmodel.getAttribute().createDataForGeom(attmodel.getObjectValueAt(_ligneIdx), 1);
          dataline2[i] = attmodel.getAttribute().createDataForGeom(attmodel.getObjectValueAt(_ligneIdx), 1);
        }
      }
    }
    // Remplacement dans la collection.
    geometries_.removeGeometries(new int[]{_ligneIdx}, cmp);
    getGeomData().addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(cline1), dataline1, cmp);
    getGeomData().addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(cline2), dataline2, cmp);

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return new int[]{geometries_.getNbGeometries() - 2, geometries_.getNbGeometries() - 1};
  }

  /**
   * @deprecated
   */
  public int[] splitGeometry(final int _ligneIdx, final int _idx, final CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return null;
    }
    final LineString line = (LineString) geometries_.getGeometry(_ligneIdx); // Old line
    if (line.isClosed()) {
      // pas faisable si ferme
      return null;
    }
    if (line == null || _idx <= 0 || _idx >= line.getNumPoints() - 1) {
      return null;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Scission de la g�om�trie"));

    // Les coordonn�es
    final Coordinate[] clold = line.getCoordinates();
    final Coordinate[] cline1 = new Coordinate[_idx + 1];
    for (int i = 0; i < cline1.length; i++) {
      cline1[i] = (Coordinate) clold[i].clone();
    }
    final Coordinate[] cline2 = new Coordinate[clold.length - _idx];
    for (int i = 0; i < cline2.length; i++) {
      cline2[i] = (Coordinate) clold[_idx + i].clone();
    }

    // Les datas
    final int iattHisto = geometries_.getIndiceOf(GISAttributeConstants.HISTORY);

    final Object[] dataline1 = new Object[getAttributeNb()];
    final Object[] dataline2 = new Object[getAttributeNb()];
    for (int i = 0; i < getAttributeNb(); i++) {
      final GISAttributeModel attmodel = geometries_.getModel(i);
      final Object model = attmodel.getObjectValueAt(_ligneIdx);
      final GISAttributeInterface attribute = attmodel.getAttribute();
      if (attribute.isAtomicValue()) {
        final GISAttributeModel atomModel1 = attribute.createAtomicModel(null, cline1.length);
        for (int j = 0; j < cline1.length; j++) {
          atomModel1.setObject(j, ((GISAttributeModel) model).getObjectValueAt(j), null);
        }
        final GISAttributeModel atomModel2 = attribute.createAtomicModel(null, cline2.length);
        for (int j = 0; j < cline2.length; j++) {
          atomModel2.setObject(j, ((GISAttributeModel) model).getObjectValueAt(j + _idx), null);
        }

        dataline1[i] = atomModel1;
        dataline2[i] = atomModel2;
      } else {
        // Cas particulier de l'attribut TITRE => Conserv�, op�ration dans l'historique
        if (attribute == GISAttributeConstants.TITRE) {
          final String name = (String) model;
          dataline1[i] = name;
          dataline2[i] = name + "_";

          if (iattHisto != -1) {
            dataline1[iattHisto] = GISLib.appendHistory(EbliLib.getS("Scission"), name, (String) geometries_.getModel(iattHisto).getObjectValueAt(_ligneIdx));
            dataline2[iattHisto] = GISLib.appendHistory(EbliLib.getS("Scission"), name, (String) geometries_.getModel(iattHisto).getObjectValueAt(_ligneIdx));
          }
        } // Cas particulier de l'historique => Deja trait�
        else if (attmodel.getAttribute() == GISAttributeConstants.HISTORY) {
        } // CAs particulier de l'attribut ETAT_GEOM => ATT_VAL_ETAT_MODI
        else if (attmodel.getAttribute() == GISAttributeConstants.ETAT_GEOM) {
          dataline1[i] = GISAttributeConstants.ATT_VAL_ETAT_MODI;
          dataline2[i] = GISAttributeConstants.ATT_VAL_ETAT_MODI;
        } else {
          dataline1[i] = attribute.createDataForGeom(model, 1);
          dataline2[i] = attribute.createDataForGeom(model, 1);
        }
      }
    }

    // Remplacement dans la collection.
    geometries_.removeGeometries(new int[]{_ligneIdx}, cmp);
    getGeomData().addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(cline1), dataline1, cmp);
    getGeomData().addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(cline2), dataline2, cmp);

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return new int[]{geometries_.getNbGeometries() - 2, geometries_.getNbGeometries() - 1};
  }

  /**
   * Joint 2 polylignes par leurs sommets d�sign�s.
   *
   * @param _ligneIdx Les index des 2 lignes.
   * @param _idx Les index des 2 sommets des 2 lignes.
   * @param _ui l'interface pour pouvoir demander � l'utilisateur la valeur de Z. Peut �tre null.
   * @param _cmd Le conteneur de commandes.
   * @return L'index de la nouvelle ligne si OK, -1 sinon.
   */
  public int joinGeometries(int[] _ligneIdx, int[] _idx, final CtuluUI _ui, final CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return -1;
    }
    GISPolyligne[] lines = new GISPolyligne[2]; // Old lines
    for (int i = 0; i < lines.length; i++) {
      lines[i] = (GISPolyligne) geometries_.getGeometry(_ligneIdx[i]);
      if (_idx[i] != 0 && _idx[i] != lines[i].getNumPoints() - 1) {
        return -1; // Seuls les points extremit�s sont autoris�s.
      }
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Jointure de polylignes"));

    // Les coordonn�es
    Coordinate[] cline = new Coordinate[lines[0].getNumPoints() + lines[1].getNumPoints()];
    Coordinate[] clold = null;
    int indpt = 0;
    int sens = 1;
    for (int i = 0; i < cline.length; i++) {

      // Premiere ligne
      if (i == 0) {
        clold = lines[0].getCoordinates();
        if (_idx[0] == 0) {
          indpt = lines[0].getNumPoints() - 1;
          sens = -1;
        } else {
          indpt = 0;
          sens = 1;
        }
      } // Deuxieme ligne
      else if (i == lines[0].getNumPoints()) {
        clold = lines[1].getCoordinates();
        if (_idx[1] == 0) {
          indpt = 0;
          sens = 1;
        } else {
          indpt = lines[1].getNumPoints() - 1;
          sens = -1;
        }
      }

      cline[i] = (Coordinate) clold[indpt].clone();
      indpt += sens;
    }

    // Les datas
    final int iattHisto = geometries_.getIndiceOf(GISAttributeConstants.HISTORY);

    Object[] dataline = new Object[geometries_.getNbAttributes()];
    for (int att = 0; att < geometries_.getNbAttributes(); att++) {
      if (geometries_.getAttribute(att).isAtomicValue()) {
        GISAttributeModel mdl = (GISAttributeModel) geometries_.getModel(att).getAttribute().createDataForGeom(null, cline.length);

        GISAttributeModel mdlold = null;
        for (int i = 0; i < cline.length; i++) {

          // Premiere ligne
          if (i == 0) {
            mdlold = (GISAttributeModel) geometries_.getModel(att).getObjectValueAt(_ligneIdx[0]);
            if (_idx[0] == 0) {
              indpt = mdlold.getSize() - 1;
              sens = -1;
            } else {
              indpt = 0;
              sens = 1;
            }
          } // Deuxieme ligne
          else if (i == lines[0].getNumPoints()) {
            mdlold = (GISAttributeModel) geometries_.getModel(att).getObjectValueAt(_ligneIdx[1]);
            if (_idx[1] == 0) {
              indpt = 0;
              sens = 1;
            } else {
              indpt = lines[1].getNumPoints() - 1;
              sens = -1;
            }
          }

          mdl.setObject(i, mdlold.getObjectValueAt(indpt), null);
          indpt += sens;
        }
        dataline[att] = mdl;
      } else {
        // Cas particulier de l'attribut TITRE => On prend le premier, l'autre va dans l'historique
        if (geometries_.getModel(att).getAttribute() == GISAttributeConstants.TITRE) {
          String name1 = (String) geometries_.getModel(att).getObjectValueAt(_ligneIdx[0]);
          String name2 = (String) geometries_.getModel(att).getObjectValueAt(_ligneIdx[1]);
          dataline[att] = name1;

          if (iattHisto != -1) {
            dataline[iattHisto] = GISLib.appendHistory(EbliLib.getS("Jonction"), name1 + "," + name2, (String) geometries_.getModel(iattHisto).getObjectValueAt(_ligneIdx[0]));
          }
        } // Cas particulier de l'attribut HISTORY => D�j� trait�
        else if (geometries_.getModel(att).getAttribute() == GISAttributeConstants.HISTORY) {
        } // Cas particulier de l'attribut ETAT_GEOM => ATT_VAL_ETAT_MODI
        else if (geometries_.getModel(att).getAttribute() == GISAttributeConstants.ETAT_GEOM) {
          dataline[att] = GISAttributeConstants.ATT_VAL_ETAT_MODI;
        } // Cas particulier de l'attribut Z, demande de la valeur � l'utilisateur si non identique entre les deux polylignes
        else if (geometries_.getModel(att).getAttribute().getID().equals("Z")) {
          // Valeurs identiques
          if (geometries_.getModel(att).getObjectValueAt(_ligneIdx[0]).equals(geometries_.getModel(att).getObjectValueAt(_ligneIdx[1]))) {
            dataline[att] = geometries_.getModel(att).getAttribute().createDataForGeom(geometries_.getModel(att).getObjectValueAt(_ligneIdx[0]), 1);
          } // Valeurs diff�rentes => demande � l'utilisateur
          else {
            // Utilisation d'une fen�tre modale pour l'obtension du Z.
            class myDialogModal extends JDialog implements ActionListener {

              private JComponent text_;

              public myDialogModal(Frame _frame) {
                super(_frame, EbliLib.getS("Choix du Z"), true);
                // Position & resizable
                setLocationRelativeTo(_frame);
                setResizable(false);
                // Contenu
                JPanel container = new JPanel(new BuBorderLayout(2, 2));
                container.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
                text_ = GISAttributeConstants.BATHY.getEditor().createEditorComponent();
                JButton ok = new JButton(EbliLib.getS("Valider"));
                ok.addActionListener(this);
                container.add(new BuLabel(EbliLib.getS("Valeur de Z: ")), BuBorderLayout.WEST);
                container.add(text_, BuBorderLayout.EAST);
                container.add(ok, BuBorderLayout.SOUTH);
                add(container);
                pack();
              }

              @Override
              public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
              }

              public Double getValue() {
                return (Double) GISAttributeConstants.BATHY.getEditor().getValue(text_);
              }
            }
            myDialogModal dialog = new myDialogModal(_ui == null ? null : (Frame) _ui.getParentComponent());
            dialog.setVisible(true);
            dataline[att] = geometries_.getModel(att).getAttribute().createDataForGeom(dialog.getValue(), 1);
          }
        } // On prend arbitrairement la valeur de la premi�re polyligne.
        else {
          dataline[att] = geometries_.getModel(att).getAttribute().createDataForGeom(geometries_.getModel(att).getObjectValueAt(_ligneIdx[0]), 1);
        }
      }
    }

    // Remplacement dans la collection.
    getGeomData().addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(cline), dataline, cmp);
    geometries_.removeGeometries(_ligneIdx, cmp);

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return geometries_.getNbGeometries() - 1;
  }

  public boolean closeGeometry(final int _ligneIdx, final CtuluCommandContainer _cmd) {
    final GISPolyligne line = (GISPolyligne) geometries_.getGeometry(_ligneIdx);

    if (line.isClosed()) {
      return false;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite();

    // Les coordonn�es
    final Coordinate[] cline = new Coordinate[line.getNumPoints() + 1];
    final Coordinate[] clold = line.getCoordinates();

    for (int i = 0; i < cline.length; i++) {
      if (i < (cline.length - 1)) {
        cline[i] = (Coordinate) clold[i].clone();
      } else {
        cline[i] = (Coordinate) clold[0].clone();
      }
    }

    // Les datas
    final Object[] dataline = new Object[geometries_.getNbAttributes()];
    for (int att = 0; att < geometries_.getNbAttributes(); att++) {
      if (geometries_.getAttribute(att).isAtomicValue()) {
        final GISAttributeModel mdl = (GISAttributeModel) geometries_.getModel(att).getAttribute().createDataForGeom(
                null, cline.length);

        final GISAttributeModel mdlold = (GISAttributeModel) geometries_.getModel(att).getObjectValueAt(_ligneIdx);

        for (int i = 0; i < cline.length; i++) {
          if (i < (cline.length - 1)) {
            mdl.setObject(i, mdlold.getObjectValueAt(i), null);
          } else {
            mdl.setObject(i, mdlold.getObjectValueAt(0), null);
          }
        }

        dataline[att] = mdl;
      } else {
        // Cas particulier de l'attribut TITRE => <TITRE_GEOM1>_<TITRE_GEOM2>
        if (geometries_.getModel(att).getAttribute() == GISAttributeConstants.TITRE) {
          final String name = (String) geometries_.getModel(att).getObjectValueAt(_ligneIdx);
          dataline[att] = name;
        } else {
          dataline[att] = geometries_.getModel(att).getAttribute().createDataForGeom(
                  geometries_.getModel(att).getObjectValueAt(_ligneIdx), 1);
        }
      }
    }

    // Remplacement dans la collection.
    getGeomData().addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(cline), dataline, cmp);
    geometries_.removeGeometries(new int[]{_ligneIdx}, cmp);

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

    return true;
  }

  /**
   * Inverse le sens de la polyligne.
   *
   * @param _idxGeom index de la g�om�trie � inverser.
   * @param _cmd Le container de commandes.
   */
  public void invertGeometry(final int _idxGeom, CtuluCommandContainer _cmd) {
    // Inversion des coordonn�es
    LineString geom = (LineString) getGeomData().getGeometry(_idxGeom);
    CoordinateSequence seq = geom.getCoordinateSequence();
    for (int i = 0; i < seq.size() / 2; i++) {
      for (int j = 0; j < 3; j++) {
        double valTmp = seq.getOrdinate(i, j);
        seq.setOrdinate(i, j, seq.getOrdinate(seq.size() - 1 - i, j));
        seq.setOrdinate(seq.size() - 1 - i, j, valTmp);
      }
    }
    // Inversion des attributs
    for (int i = 0; i < getAttributeNb(); i++) {
      if (getAttribute(i).isAtomicValue()) {
        GISAttributeModel values = (GISAttributeModel) getDataModel(i).getObjectValueAt(_idxGeom);
        for (int j = 0; j < values.getSize() / 2; j++) {
          Object valTmp = values.getObjectValueAt(j);
          values.setObject(j, values.getObjectValueAt(values.getSize() - 1 - j), null);
          values.setObject(values.getSize() - 1 - j, valTmp, null);
        }
      }
    }
    // Fires
    fireModelAction(_idxGeom, geom, ZModelListener.GEOMETRY_ACTION_MODIFY);
    fireAttributeValueChangeAction(this, -1, null, _idxGeom, null);

    if (_cmd != null) {
      _cmd.addCmd(new CtuluNamedCommand() {
        @Override
        public void redo() {
          invertGeometry(_idxGeom, null);
        }

        @Override
        public void undo() {
          invertGeometry(_idxGeom, null);
        }

        @Override
        public String getName() {
          return EbliLib.getS("Inversion de l'orientation");
        }
      });
    }
  }

  public GISAttributeModel getDataModel(final int _i) {
    return geometries_.getDataModel(_i);
  }

  @Override
  public boolean isCoordinateValid(final CoordinateSequence _seq, final CtuluAnalyze _analyze) {
    if (_seq == null) {
      _analyze.addFatalError(EbliLib.getS("Pas de coordonn�es"));
      return false;
    }
    if (_seq.size() < 2) {
      _analyze.addFatalError(getErrorMin2Pt());
      return false;
    }
    final boolean isClosed = _seq.getCoordinate(0).equals2D(_seq.getCoordinate(_seq.size() - 1));
    if (isClosed && _seq.size() < 4) {
      _analyze.addFatalError(getErrorMinPt());
      return false;
    }
    LineString s = null;
    if (isClosed) {
      s = GISGeometryFactory.INSTANCE.createLinearRing(_seq);
    } else {
      s = GISGeometryFactory.INSTANCE.createLineString(_seq);
    }
    if (!s.isValid()) {
      if (_analyze != null) {
        _analyze.addFatalError(EbliLib.getS("La nouvelle ligne bris�e n'est pas valide", CtuluLibString.TROIS));
      }
      return false;
    }
    return true;
  }

  /**
   * @param _selection les lignes brisees a deplacees
   * @param _dx le dx reel
   * @param _dy le dy reel
   * @param _dz le dz reel, modifi� si un attribut est affect� au Z.
   * @param _cmd le receveur de commande
   * @return true si modif
   */
  public boolean moveGlobal(final CtuluListSelectionInterface _selection, final double _dx, final double _dy, final double _dz,
          final CtuluCommandContainer _cmd) {
    if (cantModify()) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    boolean r = false;
    final int min = _selection.getMinIndex();
    for (int i = _selection.getMaxIndex(); i >= min; i--) {
      if (_selection.isSelected(i)) {
        setGeomModif(i, _cmd); // Modification de l'�tat de la g�om�trie
        r = true;
        final CoordinateSequence newSeq = ((LineString) geometries_.getGeometry(i).clone()).getCoordinateSequence();
        for (int j = newSeq.size() - 1; j >= 0; j--) {
          final double newX = newSeq.getX(j) + _dx;
          final double newY = newSeq.getY(j) + _dy;
          newSeq.setOrdinate(j, 0, newX);
          newSeq.setOrdinate(j, 1, newY);
        }

        // Translation suivant Z si attribut Z non null.
        final GISAttributeInterface zAtt = getGeomData().getAttributeIsZ();
        if (zAtt != null) {
          if (getGeomData().getModel(zAtt) != null) {
            Object oldvalue = getGeomData().getModel(zAtt).getObjectValueAt(i);
            Object newvalue = zAtt.createDataForGeom(oldvalue, getGeomData().getGeometry(i).getNumPoints());
            if (oldvalue instanceof GISAttributeModelDoubleArray) {
              final GISAttributeModelDoubleArray newvals = (GISAttributeModelDoubleArray) newvalue;
              final GISAttributeModelDoubleArray oldvals = (GISAttributeModelDoubleArray) oldvalue;
              for (int iv = 0; iv < oldvals.getSize(); iv++) {
                newvals.setObject(iv, new Double(oldvals.getValue(iv) + _dz), null);
              }
            } else if (oldvalue instanceof Double) {
              newvalue = new Double(((Double) oldvalue).doubleValue() + _dz);
            }
            getGeomData().getModel(zAtt).setObject(i, newvalue, cmp);
          } else {
            FuLog.warning("EBL:Pb l'attribut pour Z n'est pas dans la liste des attributs du calque!");
          }
        }

        final Geometry poly = GISGeometryFactory.INSTANCE.createGeometry(geometries_.getGeometry(i).getClass(), newSeq);
        geometries_.setGeometry(i, poly, cmp);
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }

  /**
   * @param _selection la selection
   * @param _dx dx reel
   * @param _dy dy reel
   * @param _dz le dz reel, modifi� si un attribut est affect� au Z et qu'il est atomique.
   * @param _cmd le receveur de commande
   * @param _ui l'interface utilisateur pour les messages
   * @return true si modification
   */
  public boolean moveAtomic(final EbliListeSelectionMultiInterface _selection, final double _dx, final double _dy, final double _dz,
          final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (cantModify()) {
      return false;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final TIntObjectIterator it = _selection.getIterator();
    boolean r = false;
    for (int i = _selection.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        r = true;
        final int idx = it.key();
        setGeomModif(idx, _cmd); // Modification de l'�tat de la g�om�trie
        final CoordinateSequence newSeq = ((LineString) geometries_.getGeometry(idx).clone())
                .getCoordinateSequence();

        // Translation suivant Z si attribut Z non null et atomic.
        GISAttributeInterface zAtt = getGeomData().getAttributeIsZ();
        GISAttributeModelDoubleArray newvals = null;
        GISAttributeModelDoubleArray oldvals = null;
        if (zAtt != null && zAtt.isAtomicValue()) {
          if (getGeomData().getModel(zAtt) != null) {
            oldvals = (GISAttributeModelDoubleArray) getGeomData().getModel(zAtt).getObjectValueAt(idx);
            newvals = (GISAttributeModelDoubleArray) zAtt.createDataForGeom(oldvals, geometries_.getGeometry(idx).getNumPoints());
          } else {
            FuLog.warning("EBL:Pb l'attribut pour Z n'est pas dans la liste des attributs du calque!");
          }
        }

        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            final double newX = newSeq.getX(j) + _dx;
            final double newY = newSeq.getY(j) + _dy;
            newSeq.setOrdinate(j, 0, newX);
            newSeq.setOrdinate(j, 1, newY);
            if (j == 0 && isGeometryFermee(idx)) {
              newSeq.setOrdinate(newSeq.size() - 1, 0, newX);
              newSeq.setOrdinate(newSeq.size() - 1, 1, newY);
            }

            if (newvals != null) {
              newvals.setObject(j, new Double(oldvals.getValue(j) + _dz), null);
            }
          }
        }

        final Geometry poly = GISGeometryFactory.INSTANCE.createGeometry(geometries_.getGeometry(idx).getClass(), newSeq);
        IsValidOp valid = new IsValidOp(poly);
        if (!valid.isValid()) {
          if (_ui != null) {
            _ui.error(null, EbliLib.getS("La nouvelle ligne n'est pas valide") + ":\n" + valid.getValidationError().getMessage() + ". Point: " + valid.getValidationError().getCoordinate(), false);
          }
          return false;
        }
        geometries_.setGeometry(idx, poly, cmp);

        if (newvals != null) {
          getGeomData().getModel(zAtt).setObject(idx, newvals, cmp);
        }
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }

  /**
   * Rotation des lignes de la liste donn�e. Les lignes sont transform�s autour d'une origine unique.
   *
   * @param _selection La liste donn�e des lignes brisees
   * @param _angRad L'angle de rotation, en radians.
   * @param _xreel0 La coordonn�e X du pt d'origine.
   * @param _yreel0 La coordonn�e Y du pt d'origine.
   * @param _cmd le receveur de commande
   * @return true si modif ok.
   */
  public boolean rotateGlobal(CtuluListSelectionInterface _selection, double _angRad, double _xreel0, double _yreel0,
          final CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return false;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    boolean r = false;
    final int min = _selection.getMinIndex();
    for (int i = _selection.getMaxIndex(); i >= min; i--) {
      if (_selection.isSelected(i)) {
        setGeomModif(i, _cmd); // Modification de l'�tat de la g�om�trie
        r = true;
        final CoordinateSequence newSeq = ((LineString) geometries_.getGeometry(i).clone()).getCoordinateSequence();
        for (int j = newSeq.size() - 1; j >= 0; j--) {
          /// Ramene � l'origine, et rotation.
          final double dxold = newSeq.getX(j) - _xreel0;
          final double dyold = newSeq.getY(j) - _yreel0;
          final double dxnew = dxold * Math.cos(_angRad) - dyold * Math.sin(_angRad);
          final double dynew = dxold * Math.sin(_angRad) + dyold * Math.cos(_angRad);

          newSeq.setOrdinate(j, 0, dxnew + _xreel0);
          newSeq.setOrdinate(j, 1, dynew + _yreel0);
        }
        if (isGeometryFermee(i)) {
          final GISPolygone poly = (GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(newSeq);
          geometries_.setGeometry(i, poly, cmp);
        } else {
          final GISPolyligne poly = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(newSeq);
          geometries_.setGeometry(i, poly, cmp);
        }
      }
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }

  /**
   * Rotation des points de la liste donn�e. Les points sont transform�s autour d'une origine unique.
   *
   * @param _selection La liste donn�e des points
   * @param _angRad L'angle de rotation, en radians.
   * @param _xreel0 La coordonn�e X du pt d'origine.
   * @param _yreel0 La coordonn�e Y du pt d'origine.
   * @param _cmd le receveur de commande
   * @param _ui l'interface utilisateur pour les messages
   * @return true si modification
   */
  public boolean rotateAtomic(final EbliListeSelectionMultiInterface _selection, double _angRad, double _xreel0, double _yreel0,
          final CtuluCommandContainer _cmd, final CtuluUI _ui) {
    if (cantModify()) {
      return false;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final TIntObjectIterator it = _selection.getIterator();
    boolean r = false;
    for (int i = _selection.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelectionInterface s = (CtuluListSelectionInterface) it.value();
      if (!s.isEmpty()) {
        r = true;
        final int idx = it.key();
        setGeomModif(idx, _cmd); // Modification de l'�tat de la g�om�trie
        final CoordinateSequence newSeq = ((LineString) geometries_.getGeometry(idx).clone())
                .getCoordinateSequence();
        final int min = s.getMinIndex();
        for (int j = s.getMaxIndex(); j >= min; j--) {
          if (s.isSelected(j)) {
            /// Ramene � l'origine, et rotation.
            double dxold = newSeq.getX(j) - _xreel0;
            double dyold = newSeq.getY(j) - _yreel0;
            double dxnew = dxold * Math.cos(_angRad) - dyold * Math.sin(_angRad);
            double dynew = dxold * Math.sin(_angRad) + dyold * Math.cos(_angRad);

            newSeq.setOrdinate(j, 0, dxnew + _xreel0);
            newSeq.setOrdinate(j, 1, dynew + _yreel0);
            if (j == 0 && isGeometryFermee(idx)) {
              newSeq.setOrdinate(newSeq.size() - 1, 0, dxnew + _xreel0);
              newSeq.setOrdinate(newSeq.size() - 1, 1, dynew + _yreel0);
            }
          }
        }
        if (isGeometryFermee(idx)) {
          final GISPolygone poly = (GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(newSeq);
          if (!poly.isValid()) {
            if (_ui != null) {
              _ui.error(null, EbliLib.getS("La nouvelle ligne n'est pas valide"), false);
            }
            return false;
          }
          geometries_.setGeometry(idx, poly, cmp);
        } else {
          final GISPolyligne poly = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(newSeq);
          if (!poly.isValid()) {
            if (_ui != null) {
              _ui.error(null, EbliLib.getS("La nouvelle ligne n'est pas valide"), false);
            }
            return false;
          }
          geometries_.setGeometry(idx, poly, cmp);
        }
      }
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return r;
  }

  /**
   * @param _idx les indices des lignes a enlever
   * @param _cmd le receveur de commandes
   * @return true si enlevee
   */
  public boolean removeLigneBrisee(final int[] _idx, final CtuluCommandContainer _cmd) {
    if (cantModify()) {
      return false;
    }
    for (int i = 0; i < _idx.length; i++) {
      setGeomModif(_idx[i], _cmd); // Modification de l'�tat de la g�om�trie
    }
    geometries_.removeGeometries(_idx, _cmd);
    return true;
  }

  /**
   * Copie d'une geom�trie, avec d�calage.
   */
  public boolean copyGlobal(CtuluListSelectionInterface _selection, double _dx, double _dy, final CtuluCommandContainer _cmd) {
    if (geometries_ == null) {
      return false;
    }

    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    for (int idx = _selection.getMinIndex(); idx <= _selection.getMaxIndex(); idx++) {
      if (_selection.isSelected(idx)) {
        // La g�ometrie, d�cal�e.
        LineString geom = (LineString) geometries_.getGeometry(idx).clone();
        CoordinateSequence seq = geom.getCoordinateSequence();
        for (int i = 0; i < seq.size(); i++) {
          seq.setOrdinate(i, 0, seq.getX(i) + _dx);
          seq.setOrdinate(i, 1, seq.getY(i) + _dy);
        }
        // Les valeurs d'attribut.
        Object[] vals = new Object[geometries_.getNbAttributes()];
        for (int iatt = 0; iatt < geometries_.getNbAttributes(); iatt++) {
          Object val = geometries_.getValue(iatt, idx);
          if (geometries_.getAttribute(iatt).equals(GISAttributeConstants.TITRE)) {
            val = EbliLib.getS("Copie de") + " " + val;
          }
          if (geometries_.getAttribute(iatt).equals(GISAttributeConstants.ETAT_GEOM)) {
            val = GISAttributeConstants.ATT_VAL_ETAT_MODI;
          }
          int nbpt = geom.getNumPoints();
          vals[iatt] = geometries_.getAttribute(iatt).createDataForGeom(val, nbpt);
        }
        geometries_.addGeometry(geom, vals, cmp);
      }
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    return true;
  }

  /**
   * Fait passer la g�om�trie en Modifier
   *
   * @param _idx : index de la geom�trie
   * @param _cmd :
   */
  public void setGeomModif(int _idx, CtuluCommandContainer _cmd) {
    int idxAttr = getGeomData().getIndiceOf(GISAttributeConstants.ETAT_GEOM);
    if (idxAttr >= 0) {
      getGeomData().getDataModel(idxAttr).setObject(_idx, GISAttributeConstants.ATT_VAL_ETAT_MODI, _cmd);
    }
  }

  public boolean cantModify() {
    return geometries_ == null || !geometries_.isGeomModifiable();
  }
}
