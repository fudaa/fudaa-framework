/*
 * @creation 21 nov. 06
 * @modification $Date: 2006-12-05 10:14:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.find;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;

/**
 * @author fred deniger
 * @version $Id: CalqueFindPolyligneVisitor.java,v 1.1 2006-12-05 10:14:38 deniger Exp $
 */
public class CalqueFindPolyligneVisitor implements BCalqueVisitor {

  boolean onlyPolygone_;

  boolean found_;

  public CalqueFindPolyligneVisitor() {
    super();
  }

  public CalqueFindPolyligneVisitor(final boolean _onlyPolygone) {
    super();
    onlyPolygone_ = _onlyPolygone;
  }

  @Override
  public boolean visit(final BCalque _cq) {
    // on arrete la visite
    if (found_) {
      return false;
    }
    if (_cq == null || _cq.isGroupeCalque()) {
      return true;
    }

    found_ = isMatching(_cq);
    if (found_) {
      return false;
    }
    return true;
  }

  public boolean isMatching(final BCalque _cq) {
    boolean ok = _cq instanceof ZCalqueLigneBrisee;
    if (ok) {
      ok = ((ZCalqueLigneBrisee) _cq).modeleDonnees().getNombre() > 0;
    }
    if (ok && onlyPolygone_) {
      ok = ((ZCalqueLigneBrisee) _cq).modeleDonnees().containsPolygone();
    }
    return ok;

  }

  public boolean isOnlyPolygone() {
    return onlyPolygone_;
  }

  public void setOnlyPolygone(final boolean _onlyPolygone) {
    onlyPolygone_ = _onlyPolygone;
  }

  public boolean isFound() {
    return found_;
  }

  public void resetFound() {
    found_ = false;
  }

}
