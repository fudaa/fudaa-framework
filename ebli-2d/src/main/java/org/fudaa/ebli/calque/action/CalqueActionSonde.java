/**
 * @creation 21 janv. 2005
 * @modification $Date: 2006-07-13 13:35:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZCalqueSondeInteraction;
import org.fudaa.ebli.calque.ZCalqueSondeInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: CalqueActionSonde.java,v 1.6 2006-07-13 13:35:50 deniger Exp $
 */
public class CalqueActionSonde extends CalqueActionInteraction implements TreeSelectionListener {

  public CalqueActionSonde(final ZCalqueSondeInteraction _cq, final BArbreCalqueModel _m) {
    super(EbliLib.getS("Interpolation"), EbliResource.EBLI.getToolIcon("pointeur"),
        "SONDE", _cq);
    _m.addTreeSelectionListener(this);
    super.setDefaultToolTip(EbliLib.getS("<html><body>Interpoler les valeurs en un point<br /><b>On peut construire une ligne bris�e en cr�ant plusieurs sondes en maintenant shift. </b></body></html>"));
    updateForLayer(_m.getSelectedCalque());
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e){

    if (super.isSelected()) {
      super.setSelected(false);
    }
    final BCalque c = (BCalque) _e.getPath().getLastPathComponent();
    //le calque en cours doit passer en mode inactif
    //    active si le nouveau calque le permet
    updateForLayer(c);

  }

  private void updateForLayer(final BCalque _c) {
    if (_c instanceof ZCalqueSondeInterface) {
      super.setEnabled(true);
      ((ZCalqueSondeInteraction)bc_).setTarget((ZCalqueSondeInterface)_c);
    }
    else {
      super.setEnabled(false);
      ((ZCalqueSondeInteraction)bc_).setTarget(null);
    }
  }
}