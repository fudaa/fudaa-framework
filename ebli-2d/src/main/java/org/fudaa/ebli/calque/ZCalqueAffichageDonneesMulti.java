/*
 *  @creation     24 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class ZCalqueAffichageDonneesMulti extends ZCalqueAffichageDonneesAbstract {

  protected EbliListeSelectionMulti selection_;

  public ZCalqueAffichageDonneesMulti() {
    super();
  }

  public int isSelectionInUniqueBloc() {
    return selection_ == null ? -1 : (selection_.isSelectionInOneBloc());
  }

  @Override
  public boolean isOnlyOneObjectSelected() {
    final int bloc = isSelectionInUniqueBloc();
    if (bloc >= 0) {
      return selection_.getSelection(bloc).isOnlyOnIndexSelected();
    }
    return false;
  }

  public int[] isSelectionContiguous(final int _b, final int _nbItem) {
    final CtuluListSelection l = selection_.get(_b);
    if ((l == null) || (l.isEmpty())) {
      return null;
    }
    return CtuluListSelection.isSelectionContiguous(l, _nbItem);
  }

  protected EbliListeSelectionMulti createSelection() {
    return new EbliListeSelectionMulti(modeleDonnees().getNombre());
  }

  @Override
  public EbliListeSelectionMultiInterface getLayerSelectionMulti() {
    return selection_;
  }

  @Override
  public void clearSelection() {
    if ((selection_ != null) && (!selection_.isEmpty())) {
      selection_.clear();
      fireSelectionEvent();
    }
  }

  protected void initSelection() {
    if (selection_ == null) {
      selection_ = new EbliListeSelectionMulti(modeleDonnees().getNombre());
    }
  }

  @Override
  public boolean changeSelection(final EbliListeSelectionMultiInterface _s, final int _action) {
    // Si la selection de modif est nulle, seule l'action de remplacement
    // est concernee.
    if (selection_ == null) {
      initSelection();
    }
    boolean sentEvent = false;
    switch (_action) {
    case EbliSelectionState.ACTION_ADD:
      sentEvent = selection_.add(_s);
      break;
    case EbliSelectionState.ACTION_DEL:
      sentEvent = selection_.remove(_s);
      break;
    case EbliSelectionState.ACTION_XOR:
      sentEvent = selection_.xor(_s);
      break;
    case EbliSelectionState.ACTION_REPLACE:
      selection_.setSelection(_s);
      sentEvent = true;
      break;
    case EbliSelectionState.ACTION_AND:
      sentEvent = selection_.intersection(_s);
      break;
    default:
      break;
    }
    if (sentEvent) {
      fireSelectionEvent();
    }
    return sentEvent;
  }

  @Override
  public boolean changeSelection(final GrPoint _p, final int _tolerancePixel, final int _action) {
    final EbliListeSelectionMulti l = selection(_p, _tolerancePixel);
    changeSelection(l, _action);
    if ((l == null) || (l.isEmpty())) {
      return false;
    }
    return true;
  }

  @Override
  public boolean changeSelection(final LinearRing _p, final int _action, final int _mode) {
    final EbliListeSelectionMulti l = selection(_p);
    changeSelection(l, _action);
    if ((l == null) || (l.isEmpty())) {
      return false;
    }
    return true;
  }

  @Override
  public boolean changeSelection(final LinearRing[] _p, final int _action, final int _mode) {
    final EbliListeSelectionMulti l = createSelection();
    if (_p == null) {
      return false;
    }
    for (int i = _p.length - 1; i >= 0; i--) {
      l.add(selection(_p[i]));
    }
    changeSelection(l, _action);
    if (l.isEmpty()) {
      return false;
    }
    return true;
  }

  /**
   * Selectionne les objets d'indice donn�s. La toltalit� de l'objet doit �tre selectionn�e.
   * @param _idx Les indices a selectionner.
   * @return true si une selection a �t� effectu�e.
   */
  @Override
  public boolean setSelection(final int[] _idx) {
    return false; 
  }

  @Override
  public boolean selectPrevious() {
    return false;
  }

  @Override
  public boolean selectNext() {
    return false;
  }
  
  @Override
  public boolean isSelectionEmpty() {
    return selection_ == null ? true : selection_.isEmpty();
  }

  public abstract EbliListeSelectionMulti selection(LinearRing _poly);

  /**
   * Renvoie la liste des objets selectionnees pour le point <code>_pt</code> avec pour tolerance
   * <code>_tolerance</code>.
   * 
   * @param _pt
   * @param _tolerance
   * @return la liste des indexs selectionnes (ou null si aucune selection)
   */
  public abstract EbliListeSelectionMulti selection(GrPoint _pt, int _tolerance);
}
