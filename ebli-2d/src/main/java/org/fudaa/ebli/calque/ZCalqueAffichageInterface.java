/*
 * @creation 2 juin 2006
 * @modification $Date: 2008-02-21 15:16:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;

/**
 * L'interface pour un calque qui dessine quelque chose (calque d'affichage, calque de s�lection, etc).
 * @author fred deniger
 * @version $Id: ZCalqueAffichageInterface.java,v 1.2.8.1 2008-02-21 15:16:09 bmarchan Exp $
 */
public interface ZCalqueAffichageInterface {

  void paintDonnees(Graphics2D _g, GrMorphisme _versEcran, GrMorphisme _versReel, GrBoite _clipReel);

}
