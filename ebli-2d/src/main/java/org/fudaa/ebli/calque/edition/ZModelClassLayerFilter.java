/*
 *  @creation     26 mai 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelZoneAdapter;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueVisitor;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModeleDonnees;
import org.fudaa.ebli.calque.ZModeleGeometry;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class ZModelClassLayerFilter implements BCalqueVisitor {

  final List res_ = new ArrayList();
  final Class model_;

  public ZModelClassLayerFilter(final Class _modeleClass) {
    model_ = _modeleClass;
  }

  public ZCalqueAffichageDonneesInterface[] getCalques(){
    final ZCalqueAffichageDonneesInterface[] res=new ZCalqueAffichageDonneesInterface[res_.size()];
    res_.toArray(res);
    return res;
  }

  /**
   * @return le nombre de calques trouv�s.
   */
  public int getNbLayerFound(){
    return res_.size();
  }

  /**
   * @param _dest la destination qui doit etre de la meme taille que getNbLayersFound
   */
  public void fillArray(final GISZoneCollection[] _dest){
    for (int i = res_.size() - 1; i >= 0; i--) {
      _dest[i] = ((ZModeleGeometry) (((ZCalqueAffichageDonneesInterface) res_.get(i)).modeleDonnees()))
          .getGeomData();
    }
  }

  public void fillArrayModel(final GISDataModel[] _dest){
    for (int i = res_.size() - 1; i >= 0; i--) {
      _dest[i] = new GISDataModelZoneAdapter(((ZModeleGeometry) (((ZCalqueAffichageDonneesInterface) res_
          .get(i)).modeleDonnees())).getGeomData(), null);
    }
  }

  @Override
  public boolean visit(final BCalque _cq){
    if (!res_.contains(_cq) && _cq instanceof ZCalqueAffichageDonneesInterface) {
      final ZModeleDonnees data = ((ZCalqueAffichageDonneesInterface) _cq).modeleDonnees();
      if (data instanceof ZModeleGeometry
        && model_.isAssignableFrom(((ZModeleGeometry) data).getGeomData().getClass())) {
        res_.add(_cq);
      }
    }
    return true;
  }
}
