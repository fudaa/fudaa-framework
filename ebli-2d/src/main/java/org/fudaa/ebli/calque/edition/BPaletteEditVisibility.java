/*
 * @creation     23 oct. 2008
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuVerticalLayout;
import org.locationtech.jts.geom.Geometry;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Cette palette a pour but de permettre de modifier la visibilit� de toutes les
 * g�om�tries. Concr�trement un tableau listant les g�om�tries propose de cocher
 * ou d�cocher une checkbox correspondant � la valeur de visibilit� de la
 * g�om�trie et r�percute imm�diatement le changement effectu�.
 * 
 * De plus deux boutons permettent de changer l'ordre des g�om�tries dans le
 * tableau et dans le model les contenant.
 * 
 * @author Emmanuel MARTIN
 * @version $Id: BPaletteEditVisibility.java 4172 2008-11-05 14:49:55Z
 *          emmanuel_martin $
 */
public class BPaletteEditVisibility extends BuPanel implements ListSelectionListener, ZSelectionListener, ZModelGeometryListener, TreeModelListener, BPalettePanelInterface, ActionListener {

  /** Calques disponible pour l'utilisateur. */
  protected ZCalqueEditable[] calques_;
  /** Models en cours d'�coute. */
  protected ZModeleEditable[] listenModels_;
  protected final String allCalquesEntry_=EbliLib.getS("Tous les calques");
  /**
   * vrai quand une modification de la selection dans le tableau ou dans le
   * calque est en cours. Permet d'�viter des r�cursions � cause des listeners de
   * selections.
   */
  protected boolean listenSelection=true;
  /** Vrai quand la visibilit� n'est pas en train d'�tre chang� par le panel. */
  protected boolean listenVisibility=true;
  protected CtuluCommandManager cmd_;
  protected ZScene scene_;
  // Composants graphiques
  protected final BuComboBox calque_;
  protected boolean onlyInvisible_;
  protected final CtuluTable table_;
  protected BuButton btUp_;
  protected BuButton btDown_;

  /**
   * @param _treeModel
   *          utile pour avoir connaissance de la cr�ation et de la suppression
   *          de calque.
   * @param _scene
   *          utile pour l'�coute des selections.
   */
  public BPaletteEditVisibility(BArbreCalqueModel _treeModel, ZScene _scene, CtuluCommandManager _cmd){
    cmd_=_cmd;
    scene_=_scene;
    calques_=new ZCalqueEditable[0];
    listenModels_=new ZModeleEditable[0];
    _treeModel.addTreeModelListener(this);
    scene_.addSelectionListener(this);
    // Configuration du panel \\
    setLayout(new BuBorderLayout());
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    setPreferredSize(new Dimension(210, 350));
    JPanel pnHeader=new JPanel();
    pnHeader.setLayout(new BuBorderLayout(5, 5));
    // Ajout du titre \\
    BuLabel titre=new BuLabel("<html><b>"+EbliLib.getS("Visibilit�")+"</b></html>");
    titre.setHorizontalAlignment(SwingConstants.CENTER);
    pnHeader.add(titre, BuBorderLayout.NORTH);
    // Ajout des composants de configuration \\
    add(pnHeader, BuBorderLayout.NORTH);
    // Calque
    calque_=new BuComboBox();
    JPanel pnLayer=new JPanel();
    pnLayer.setLayout(new BorderLayout(2,2));
    pnLayer.add(new BuLabel(EbliLib.getS("Calque")),BorderLayout.WEST);
    pnLayer.add(calque_,BorderLayout.CENTER);
    // Only invisible
    BuCheckBox cbFilter=new BuCheckBox(EbliLib.getS("Invisible uniquement"), false);
    
    JPanel pnParams=new JPanel();
    pnParams.setLayout(new BuVerticalLayout(2));
    pnParams.add(pnLayer);
    pnParams.add(cbFilter);
    pnHeader.add(pnParams, BuBorderLayout.CENTER);
    // Ajout du tableau de geom�tries \\
    table_=new CtuluTable();
    table_.getSelectionModel().addListSelectionListener(this);
    add(new BuScrollPane(table_), BuBorderLayout.CENTER);
    // Ajout des boutons de d�placement des g�om�tries \\
    JPanel pnButtons=new JPanel();
    pnButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
    btUp_=new BuButton(BuResource.BU.getIcon("monter"));
    btUp_.setToolTipText(EbliLib.getS("D�cr�menter l'index de la g�om�trie"));
    btDown_=new BuButton(BuResource.BU.getIcon("descendre"));
    btDown_.setToolTipText(EbliLib.getS("Incr�menter l'index de la g�om�trie"));
    btUp_.addActionListener(this);
    btDown_.addActionListener(this);
    pnButtons.add(btUp_);
    pnButtons.add(btDown_);
    add(pnButtons, BuBorderLayout.SOUTH);
    // Ajout des informations \\
    calque_.addActionListener(this);
    fillCalqueComboBox();
    cbFilter.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbFilterActionPerformed();
      }
    });
    fillTableGeometry();
    updateBtMovement();
  }

  /**
   * Le modele de la table qui liste les g�om�tries. Il contient �galement le
   * calque d'origine de et l'index de la g�om�trie dans ce calque pour pouvoir
   * r�percuter le changement.
   * 
   * @author Emmanuel MARTIN
   * @version $Id$
   */
  protected class GeomTableModel extends AbstractTableModel {

    private String[] titreColonnes_;
    private Object[][] rows_;
    
    public GeomTableModel(String[] _titreColonnes, Object[][] _rows){
      if(_titreColonnes.length!=2)
        throw new IllegalArgumentException("Il doit y avoir deux titres de colonnes.");
      for(int i=0;i<_rows.length;i++)
        if(_rows[i].length!=4)
          throw new IllegalArgumentException("Au moins une ligne n'a pas exactement 4 informations.");
      titreColonnes_=_titreColonnes;
      rows_=_rows;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      switch(columnIndex){
        case 0: return String.class;
        case 1: return Boolean.class;
        default: return null;
      }
    }
    
    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
      return titreColonnes_[columnIndex];
    }
    
    @Override
    public int getRowCount() {
      return rows_.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      return rows_[rowIndex][columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex==1;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
      boolean selected=table_.getSelectionModel().isSelectedIndex(rowIndex);
      if(columnIndex==0){
        rows_[rowIndex][columnIndex]=value;
        listenSelection=false;
        fireTableDataChanged();
        if(selected)
          table_.getSelectionModel().addSelectionInterval(rowIndex, rowIndex);
        listenSelection=true;
      }
      else if(columnIndex==1){
        if (rows_[rowIndex][columnIndex]!=value) {
          rows_[rowIndex][columnIndex]=value;
          changeVisibility((Integer)rows_[rowIndex][3], (ZModeleEditable)rows_[rowIndex][2], (Boolean)rows_[rowIndex][1]);
          // Dans le cas o� on n'affiche que les g�om�tries invisibles, on
          // enl�ve celle qu'on passe � visible.
          if (onlyInvisible_&&((Boolean)rows_[rowIndex][1])==true) {
            Object[][] newRows=new Object[rows_.length-1][];
            for (int i=0; i<rowIndex; i++)
              newRows[i]=rows_[i];
            for (int i=rowIndex+1; i<rows_.length; i++)
              newRows[i-1]=rows_[i];
            rows_=newRows;
            listenSelection=false;
            fireTableDataChanged();
            if (selected)
              table_.getSelectionModel().addSelectionInterval(rowIndex, rowIndex);
            listenSelection=true;
          }
        }
      }
    }
  }
  
  protected class GeomTableRenderer implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable _table, Object _value, boolean _isSelected, boolean _hasFocus, int _row,
        int _column) {
      Color selectionBackground = UIManager.getColor("Table.selectionBackground");
      JPanel pn=new JPanel();
      pn.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
      if(_isSelected)
        pn.setBackground(selectionBackground);
      else
        pn.setBackground(Color.WHITE);
      if (_value instanceof Boolean) {
        BuCheckBox cb=new BuCheckBox("", (Boolean)_value);
        if(_isSelected)
          cb.setBackground(selectionBackground);
        else
          cb.setBackground(Color.WHITE);
        pn.add(cb);
      }
      else{
        BuLabel lbl =new BuLabel(_value.toString());
        if(_isSelected)
          lbl.setBackground(selectionBackground);
        else
          lbl.setBackground(Color.WHITE);
        pn.add(lbl);
      }
      return pn;
    }
  }

  protected class GeomTableEditor extends AbstractCellEditor implements TableCellEditor, ChangeListener {

    private boolean value;
    /** Compteur d'�v�nement. */
    private int a=1;

    /* (non-Javadoc)
     * @see
     * javax.swing.table.TableCellEditor#getTableCellEditorComponent(javax.swing
     * .JTable, java.lang.Object, boolean, int, int)
     */
    @Override
    public Component getTableCellEditorComponent(JTable _table, Object _value, boolean _isSelected, int _row, int _column) {
      value=(Boolean)_value;
      JPanel pn=new JPanel();
      pn.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
      pn.setBackground(Color.WHITE);
      BuCheckBox cb=new BuCheckBox("", (Boolean)_value);
      cb.addChangeListener(this);
      cb.setBackground(Color.WHITE);
      pn.add(cb);
      return pn;
    }

    /* (non-Javadoc)
     * @see javax.swing.CellEditor#getCellEditorValue()
     */
    @Override
    public Object getCellEditorValue() {
      return value;
    }
    
    @Override
    public boolean shouldSelectCell(EventObject anEvent){
      return false;
    }

    /* (non-Javadoc)
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     */
    @Override
    public void stateChanged(ChangeEvent e) {
      value=!value;
      // Un hack : il semble que la checkbox envoie 5 �v�nements � chaque click.
      // Pour arr�ter l'�dition automatiquement au bout d'un click, on teste sur
      // le compteur de click.
      if(a==0)
        stopCellEditing();
      a=(a+1)%5;
    }
  }
  
  /**
   * Place dans le comboBox de choix des calques les calques qui peuvent �tre
   * choisis ainsi que l'entr�e sp�ciale 'tout les calques'.
   */
  protected void fillCalqueComboBox(){
    calque_.removeActionListener(this);
    // Changement du contenu de la comboBox
    Object selectedItem=calque_.getSelectedItem();
    calque_.removeAllItems();
    // R�cup�ration des calques
    calques_=scene_.getEditableLayers();
    // Ajout des noms dans le comboBox
    calque_.addItem(allCalquesEntry_);
    for(int i=0;i<calques_.length;i++)
      calque_.addItem(calques_[i].getTitle());
    if(selectedItem!=null)
      calque_.setSelectedItem(selectedItem);
    else
      calque_.setSelectedIndex(0);
    calque_.addActionListener(this);
    updateListen();
  }

  /**
   * Met � jour l'�coute des models
   */
  protected void updateListen(){
    // Suppression de l'�coute de tout les models
    for(int i=0;i<listenModels_.length;i++)
      if(listenModels_[i]!=null)
        listenModels_[i].removeModelListener(this);
    // Mise a jour du tableau de listeners
    if(calque_.getSelectedItem()==allCalquesEntry_){
      listenModels_=new ZModeleEditable[calques_.length];
      for(int i=0;i<calques_.length;i++)
        listenModels_[i]=calques_[i].getModelEditable();
    }
    else{
      boolean found=false;
      int i=-1;
      while(!found&&++i<calques_.length)
        found=calques_[i].getTitle().equals(calque_.getSelectedItem());
      if(found)
        listenModels_=new ZModeleEditable[]{calques_[i].getModelEditable()};
      else
        listenModels_=new ZModeleEditable[0];
    }
    // Ajout de l'�coute de tout les calques
    for(int i=0;i<listenModels_.length;i++){
      if(listenModels_[i]!=null)
        listenModels_[i].addModelListener(this);
    }
  }

  /**
   * Rempli le tableau avec toutes les g�om�tries contenues dans le calque
   * selectionn� dans combobox. Si la checkbox indiquant que seules les
   * g�om�tries invisibles sont affich�s est coch�, cette restriction est prise
   * en compte ici.
   * Le model du tableau est r�instanc�, donc la s�l�ction est
   * perdu, ce qui peut entrainer un comportement non voulu. Il est donc
   * recommand� de n'utiliser cette m�thode que pour le premier remplissage du
   * tableau.
   */
  protected void fillTableGeometry(){
    listenSelection=false;
    // Remplissage du tableau
    ArrayList<Object[]> rows=new ArrayList<Object[]>();
    for(int i=0;i<calques_.length;i++){
      if(calques_[i].getTitle().equals(calque_.getSelectedItem())||allCalquesEntry_.equals(calque_.getSelectedItem())){
        ZModeleEditable model = calques_[i].getModelEditable();
        if (model!=null) {
          // Extraction des donn�es utiles
          GISZoneCollection zone=model.getGeomData();
          int indexTitre=zone.getIndiceOf(GISAttributeConstants.TITRE);
          int indexVisibility=zone.getIndiceOf(GISAttributeConstants.VISIBILITE);
          if (indexTitre != -1 && indexVisibility != -1) {
            for (int j=0; j < zone.getNbGeometries(); j++) {
              if (!onlyInvisible_ || GISAttributeConstants.ATT_VAL_FALSE.equals(zone.getValue(indexVisibility, j))) {
                if (j < zone.getModel(indexVisibility).getSize() && j < zone.getModel(indexTitre).getSize()) {
                  rows.add(new Object[]{zone.getValue(indexTitre, j),
                        zone.getValue(indexVisibility, j).equals(GISAttributeConstants.ATT_VAL_TRUE) ? true : false, model,
                        scene_.layerId2SceneId(calques_[i], j)});
                }
              }
            }
          }
        }
      }
    }
    table_.setModel(new GeomTableModel(new String[]{EbliLib.getS("Nom"), EbliLib.getS("Visibilit�")}, rows.toArray(new Object[0][])));
    table_.getColumnModel().getColumn(0).setCellRenderer(new GeomTableRenderer());
    table_.getColumnModel().getColumn(1).setCellRenderer(new GeomTableRenderer());
    table_.getColumnModel().getColumn(1).setPreferredWidth(25);
    table_.getColumnModel().getColumn(1).setCellEditor(new GeomTableEditor());
    // Mise � jour de la selection
    updateTableSelection();
    listenSelection=true;
  }

  /**
   * Cette m�thode effectue un changement de visibilit� sur la g�om�trie
   * indiqu�e en param�tre.
   * 
   * @param _numGeom
   *          l'index de la g�om�trie dans le calque
   * @param _calque
   *          le calque dans lequel se trouve la g�o�mtrie
   * @param _visibility
   *          la nouvelle valeur de visibilit�
   */
  protected void changeVisibility(int _numGeom, ZModeleEditable _model, boolean _visibility) {
    listenVisibility=false;
    GISZoneCollection zone=_model.getGeomData();
    int indexVisibility=zone.getIndiceOf(GISAttributeConstants.VISIBILITE);
    if (indexVisibility!=-1)
      zone.getModel(indexVisibility).setObject(scene_.sceneId2LayerId(_numGeom), _visibility==true ? GISAttributeConstants.ATT_VAL_TRUE:GISAttributeConstants.ATT_VAL_FALSE, cmd_);
    listenVisibility=true;
  }
  
  /**
   * Met � jour la selection dans le tableau pour qu'elle corresponde � la selection dans les calques.
   */
  protected void updateTableSelection() {
    int[] selection=scene_.getLayerSelection().getSelectedIndex();
    for(int i=0;i<table_.getRowCount();i++)
      // Si la g�om�trie n'est pas s�lectionn�e dans le calque mais dans le tableau => d�s�lectionne
      if(!in(((Integer) table_.getModel().getValueAt(i, 3)).intValue(), selection)&&table_.getSelectionModel().isSelectedIndex(i))
        table_.getSelectionModel().removeSelectionInterval(i, i);
      // Si la g�om�trie est selectionn� dans le calque mais pas dans le tableau => s�lection
      else if(in(((Integer) table_.getModel().getValueAt(i, 3)).intValue(), selection)&&!table_.getSelectionModel().isSelectedIndex(i))
        table_.getSelectionModel().addSelectionInterval(i, i);
  }
  
  /**
   * Met � jour la selection dans le(s) calque(s) pour qu'elle corresponde � la selection dans le tableau.
   */
  protected void updateCalqueSelection() {
    // Conversion des indices de lignes selectionn�es en index globals de g�om�tries
    int[] selection=table_.getSelectedRows();
    int[] selectedIdx=new int[selection.length];
    for(int i=0;i<selectedIdx.length;i++)
      selectedIdx[i]=((Integer) table_.getModel().getValueAt(selection[i], 3)).intValue();
    scene_.setSelection(selectedIdx);
  }
  
  /**
   * Active ou d�active les deux boutons de mouvement
   */
  protected void updateBtMovement(){
    boolean enableUp=false;
    boolean enableDown=false;
    if(calque_.getSelectedItem()!=allCalquesEntry_&&table_.getSelectedRowCount()>0){
      // Verification qu'il est possible de d�placer la s�lection vers le haut
      int i=0;
      while(!enableUp&&++i<table_.getRowCount())
        enableUp=!table_.getSelectionModel().isSelectedIndex(i-1)&&table_.getSelectionModel().isSelectedIndex(i);
      // V�rification qu'il est possible de d�placer la selection vers le bas
      i=table_.getRowCount()-1;
      while(!enableDown&&--i>=0)
        enableDown=!table_.getSelectionModel().isSelectedIndex(i+1)&&table_.getSelectionModel().isSelectedIndex(i);
    }
    btUp_.setEnabled(enableUp);
    btDown_.setEnabled(enableDown);
  }
  
  /**
   * Retourne vrai si _value est dans _table
   * @param _value
   * @param _table
   * @return
   */
  private boolean in(int _value, int[] _table){
    boolean found=false;
    int i=-1;
    while(!found&&++i<_table.length)
      found=_table[i]==_value;
    return found;
  }

  /**
   * "remonte" d'1 les g�om�tries selectionn�es dans leur calque. C'est � dire
   * que si une g�om�trie �tait � l'index 6, elle passe � l'index 5 (l'ancienne
   * g�om�trie � 5 passe � 6).
   */
  protected void moveUpSelectedGeometies(){
    int[] selection=table_.getSelectedRows();
    ArrayList<Integer> newSelection=new ArrayList<Integer>();
    Arrays.sort(selection);
    int lastIndexCompute=-1;
    for(int i=0;i<selection.length;i++){
      // Si la place du dessus est libre, on monte
      if(selection[i]>0&&(!in(selection[i]-1, selection)||selection[i]-1==lastIndexCompute)){
        newSelection.add(selection[i]-1);
        lastIndexCompute=selection[i];
        ZModeleEditable model=(ZModeleEditable) table_.getModel().getValueAt(selection[i], 2);
        int indexGeom=((Integer) table_.getModel().getValueAt(selection[i], 3)).intValue();
        model.getGeomData().switchGeometries(scene_.sceneId2LayerId(indexGeom), scene_.sceneId2LayerId(indexGeom-1), cmd_);
      }
      else
        newSelection.add(selection[i]);
    }
    // R�tablissement de la selection dans le tableau
    table_.getSelectionModel().clearSelection();
    for(int i=0;i<newSelection.size();i++)
      table_.getSelectionModel().addSelectionInterval(newSelection.get(i), newSelection.get(i));
  }
  
  /**
   * "descend" d'1 les g�om�tries selectionn�es dans leur calque. C'est � dire
   * que si une g�om�trie �tait � l'index 5, elle passe � l'index 6 (l'ancienne
   * g�om�trie � 6 passe � 5).
   */
  protected void moveDownSelectedGeometies(){
    int[] selection=table_.getSelectedRows();
    ArrayList<Integer> newSelection=new ArrayList<Integer>();
    Arrays.sort(selection);
    int lastIndexCompute=-1;
    for(int i=selection.length-1;i>=0;i--){
      // Si la place du dessous est libre, on descend
      if(selection[i]<(table_.getRowCount()-1)&&(!in(selection[i]+1, selection)||selection[i]+1==lastIndexCompute)){
        newSelection.add(selection[i]+1);
        lastIndexCompute=selection[i];
        ZModeleEditable model=(ZModeleEditable) table_.getModel().getValueAt(selection[i], 2);
        int indexGeom=((Integer) table_.getModel().getValueAt(selection[i], 3)).intValue();
        model.getGeomData().switchGeometries(scene_.sceneId2LayerId(indexGeom), scene_.sceneId2LayerId(indexGeom+1), cmd_);
      }
      else
        newSelection.add(selection[i]);
    }
    // R�tablissement de la selection dans le tableau
    table_.getSelectionModel().clearSelection();
    for(int i=0;i<newSelection.size();i++)
      table_.getSelectionModel().addSelectionInterval(newSelection.get(i), newSelection.get(i));
  }
  
  /**
   * Est appel�e par EditVisibilityAction quand la palette est d�couverte.
   */
  public void doShow(){
    updateListen();
    fillCalqueComboBox();
    fillTableGeometry();
    updateBtMovement();
  }
  
  /**
   * Est appel�e par EditVisibilityAction quand la palette est cach�e.  
   */
  public void doHide(){
    // Suppression de l'�coute de tout les calques
    for(int i=0;i<listenModels_.length;i++)
      if(listenModels_[i]!=null)
        listenModels_[i].removeModelListener(this);
    // Mise a jour du tableau de listeners
    listenModels_=new ZModeleEditable[0];
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ebli.commun.BPalettePanelInterface#doAfterDisplay()
   */
  @Override
  public void doAfterDisplay() {
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.commun.BPalettePanelInterface#getComponent()
   */
  @Override
  public JComponent getComponent() {
    return this;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.commun.BPalettePanelInterface#paletteDeactivated()
   */
  @Override
  public void paletteDeactivated() {
  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.commun.BPalettePanelInterface#setPalettePanelTarget(java.lang.Object)
   */
  @Override
  public boolean setPalettePanelTarget(Object _target) {
    return false;
  }

  // Changement dans le comboBox des calques ou dans les boutons de d�placements \\
  
  @Override
  public void actionPerformed(ActionEvent e) {
    if(e.getSource()==calque_){
      updateListen();
      fillTableGeometry();
      updateBtMovement();
    }
    else if(e.getSource()==btUp_)
      moveUpSelectedGeometies();
    else if(e.getSource()==btDown_)
      moveDownSelectedGeometies();
  }

  //Changement dans le checkBox onlyInvisible \\ 
  
  private void cbFilterActionPerformed() {
    onlyInvisible_=!onlyInvisible_;
    fillTableGeometry();
  }

  // Changement dans l'arbre des calques \\
  
  @Override
  public void treeNodesChanged(TreeModelEvent e) {
    fillCalqueComboBox();
  }

  @Override
  public void treeNodesInserted(TreeModelEvent e) {
    fillCalqueComboBox();
  }

  @Override
  public void treeNodesRemoved(TreeModelEvent e) {
    fillCalqueComboBox();
  }

  @Override
  public void treeStructureChanged(TreeModelEvent e) {
    fillCalqueComboBox();
  }
  
  // Modification dans le ou les model(s) �cout�(s).
  
  @Override
  public void attributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
    fillTableGeometry();
  }

  @Override
  public void attributeValueChangeAction(Object _source, int att, GISAttributeInterface _att, int _geom, Object _value) {
    if(listenVisibility&&(_att==GISAttributeConstants.TITRE||_att==GISAttributeConstants.VISIBILITE||_att==null)){
      // Recherche le calque contenant le model
      boolean foundCalque=false;
      int j=-1;
      ZCalqueEditable[] calques=scene_.getEditableLayers();
      while (!foundCalque&&++j<calques.length)
        foundCalque=calques[j].getModelEditable()==_source;
      if (foundCalque) {      
        if (_geom>=0) {
          int idxGeom=scene_.layerId2SceneId(calques[j], _geom);
          // Mise � jour seulement de la g�om�trie.
          boolean found=false;
          int i=-1;
          while (!found&&++i<table_.getModel().getRowCount()) {
            ZModeleEditable model=(ZModeleEditable)table_.getModel().getValueAt(i, 2);
            Integer indexGeom=(Integer)table_.getModel().getValueAt(i, 3);
            found=(model==_source)&&(indexGeom==idxGeom);
          }
          if (found)
            if (_att==GISAttributeConstants.TITRE)
              table_.getModel().setValueAt(_value, i, 0);
            else if (_att==GISAttributeConstants.VISIBILITE)
              table_.getModel().setValueAt(_value.equals(GISAttributeConstants.ATT_VAL_TRUE) ? true:false, i, 1);
            else {
              GISZoneCollection zone=((ZModeleEditable)_source).getGeomData();
              table_.getModel().setValueAt(
                  ((ZModeleEditable)_source).getGeomData().getValue(zone.getIndiceOf(GISAttributeConstants.TITRE), _geom), i, 0);
              table_.getModel().setValueAt(
                  ((ZModeleEditable)_source).getGeomData().getValue(zone.getIndiceOf(GISAttributeConstants.VISIBILITE), _geom)
                      .equals(GISAttributeConstants.ATT_VAL_TRUE) ? true:false, i, 1);
            }
        }
        else
          // Mise � jour de toutes les g�om�tries.
          fillTableGeometry();
      }
    }
  }

  @Override
  public void geometryAction(Object _source, int geom, Geometry _geom, int _action) {
    if(_action!=GEOMETRY_ACTION_MODIFY)
      fillTableGeometry();
  }

  // La selection dans un calque � chang�e \\
  
  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    if (listenSelection) {
      listenSelection=false;
      updateTableSelection();
      updateBtMovement();
      listenSelection=true;
    }
  }

  // La selection dans le tableau � chang�e \\
  
  @Override
  public void valueChanged(ListSelectionEvent _e) {
    if (listenSelection) {
      listenSelection=false;
      updateCalqueSelection();
      updateBtMovement();
      listenSelection=true;
    }
  }
}
