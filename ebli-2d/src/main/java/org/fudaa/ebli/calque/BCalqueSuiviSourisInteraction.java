/*
 * @file         BCalqueSuiviSourisInteraction.java
 * @creation     1998-10-16
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuEmptyArrays;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.repere.CoordonneesEvent;
import org.fudaa.ebli.repere.CoordonneesListener;
/**
 * Un calque d'ecoute de la souris.
 * Destine a transmettre les coordonnees reelles de la position
 * pointee par la souris.
 *
 * @version      $Id: BCalqueSuiviSourisInteraction.java,v 1.12 2006-09-19 14:55:46 deniger Exp $
 * @author       Guillaume Desnoix
 */
public class BCalqueSuiviSourisInteraction extends BCalqueInteraction implements
    MouseMotionListener, MouseListener {

  private List listeners_;

  public BCalqueSuiviSourisInteraction() {
    listeners_ = new ArrayList();
    // addMouseMotionListener(this);
    setForeground(Color.magenta);
    setDestructible(false);
  }

  public void addCoordonneesListener(final CoordonneesListener _l){
    listeners_.add(_l);
  }

  public void removeCoordonneesListener(final CoordonneesListener _l){
    listeners_.remove(_l);
  }

  public void fireCoordonneesEvent(final CoordonneesEvent _evt){
    for (final Iterator e = listeners_.iterator(); e.hasNext();) {
      final CoordonneesListener l = (CoordonneesListener) e.next();
      l.coordonneesModifiees(_evt);
    }
  }

  // Icon
  /**
   * Dessin de l'icone.
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...).
   *           Ce parametre peut etre <I>null</I>. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c,final Graphics _g,final int _x,final int _y){
    super.paintIcon(_c, _g, _x, _y);
    final Color fg = getForeground();
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(fg);
    _g.drawLine(_x + 1, _y + h / 2 + 2, _x + w - 1, _y + h / 2 + 2);
    _g.drawLine(_x + w / 2 + 2, _y + 1, _x + w / 2 + 2, _y + h - 1);
  }

  // Mouse
  /**
   * Methode inactive.
   */
  public void mouseDown(final MouseEvent _evt){}

  /**
   * Detecte l'entree de la souris dans ce calque.
   * Ceci declenche un changement du curseur.
   */
  @Override
  public void mouseEntered(final MouseEvent _evt){
  //in= true;
  }

  /**
   * Sortie de la souris du calque.
   * Restauration du curseur et envoi d'un evenement <I>CoordonneesEvent</I>.
   */
  @Override
  public void mouseExited(final MouseEvent _evt){
    //in= false;
    cleanCursor();
    if (!isGele()) {
      fireCoordonneesEvent(new CoordonneesEvent(this, FuEmptyArrays.DOUBLE0,
          FuEmptyArrays.BOOLEAN0, FuEmptyArrays.BOOLEAN0, true));
    }
  }

  /**
   * Methode inactive.
   */
  @Override
  public void mousePressed(final MouseEvent _evt){}

  /**
   * Methode inactive.
   */
  @Override
  public void mouseReleased(final MouseEvent _evt){}

  /**
   * Methode inactive.
   */
  public void mouseUp(final MouseEvent _evt){}

  /**
   * Methode inactive.
   */
  @Override
  public void mouseClicked(final MouseEvent _evt){}

  // MouseMotion
  /**
   * Appelle la methode <I>mouseMoved</I>.
   */
  @Override
  public void mouseDragged(final MouseEvent _evt){
    mouseMoved(_evt);
  }

  /**
   * Envoi d'un evenement <I>CoordonneesEvent</I> (en mode non gele).
   */
  @Override
  public void mouseMoved(final MouseEvent _evt){
    if (!isGele()) {
      final  int x = _evt.getX();
      final  int y = _evt.getY();
      // drawCursor(x,y);
      GrPoint p = new GrPoint(x, y, 0.);
      p = p.applique(getVersReel());
      final  double[] c = new double[] { p.x_, p.y_};
      final  boolean[] m = new boolean[] { true, true};
      final  boolean[] d = new boolean[] { false, false};
      fireCoordonneesEvent(new CoordonneesEvent(this, c, m, d, true));
    }
  }

  // Paint
  /*
   public void paintComponent(Graphics _g)
   {
   cleanCursor();
   super.paintComponent(_g);
   cp=false;
   }
   */
  //  private boolean in= false;
  private boolean cp_ ;
  private int xp_ ;
  private int yp_ ;

  /**
   * Dessin du curseur.
   */
  protected void paintCursor(final Graphics _g,final int _x,final int _y){
    int x=_x;
    int y=_y;
    final Dimension size = getSize();
    final Insets insets = getInsets();
    x += insets.left;
    y += insets.right;
    size.width -= insets.left + insets.right;
    size.height -= insets.top - insets.bottom;
    _g.setXORMode(new Color(128, 255, 128));
    _g.drawLine(x, 0, x, y - 50);
    _g.drawLine(x, y + 50, x, size.height - 1);
    _g.drawLine(0, y, x - 50, y);
    _g.drawLine(x + 50, y, size.width - 1, y);
    _g.drawOval(x - 50, y - 50, 101, 101);
  }

  /**
   * Dessin du curseur.
   */
  protected void drawCursor(final int _x,final int _y){
    if (cp_) {
      if ((_x != xp_) || (_y != yp_)) {
        cleanCursor();
      }
    }
    if (!cp_) {
      paintCursor(getGraphics(), _x, _y);
      cp_ = true;
      xp_ = _x;
      yp_ = _y;
    }
  }

  /**
   * Restauration du curseur.
   */
  protected void cleanCursor(){
    if (cp_) {
      paintCursor(getGraphics(), xp_, yp_);
      cp_ = false;
    }
  }
}
