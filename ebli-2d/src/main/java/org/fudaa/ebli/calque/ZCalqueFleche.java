/**
 * @creation 13 juil. 2004
 * @modification $Date: 2008-02-20 10:14:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ebli.calque.find.CalqueFindActionDefault;
import org.fudaa.ebli.calque.find.CalqueFindFlecheExpression;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.trace.*;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.text.NumberFormat;

/**
 * @author Fred Deniger
 * @version $Id: ZCalqueFleche.java,v 1.39.4.1 2008-02-20 10:14:40 bmarchan Exp $
 */
public class ZCalqueFleche extends ZCalqueSegment {

  /**
   * @author Fred Deniger
   * @version $Id: ZCalqueFleche.java,v 1.39.4.1 2008-02-20 10:14:40 bmarchan Exp $
   */
  public static class StringInfo {

    public String nbVecteurs_ = EbliLib.getS("Nombre de vecteurs");

    public String nbVecteursSelectionne_ = EbliLib.getS("Nombre de vecteurs s�lectionn�s");

    public String norme_ = EbliLib.getS("Norme");

    public String title_ = EbliLib.getS("Vecteurs");

    public String titleIfOne_ = EbliLib.getS("Vecteur n�");

    public NumberFormat vecteur_;

    public String vx_ = EbliLib.getS("Vx");

    public String vy_ = EbliLib.getS("Vy");

    public String x_ = EbliLib.getS("X");

    public CtuluNumberFormatI xy_ = CtuluLib.DEFAULT_NUMBER_FORMAT;

    public String y_ = EbliLib.getS("Y");

    public String formatVect(final double _v) {
      return vecteur_ == null ? Double.toString(_v) : vecteur_.format(_v);
    }

    public String formatXY(final double _v) {
      return xy_ == null ? Double.toString(_v) : xy_.format(_v);
    }
  }

  /**
   * Un modele de table par d�faut.
   * 
   * @author Fred Deniger
   * @version $Id: ZCalqueFleche.java,v 1.39.4.1 2008-02-20 10:14:40 bmarchan Exp $
   */
  public static class ValueTableModel extends AbstractTableModel {

    final ZModeleSegment segModel_;

    /**
     * @param _seg
     */
    public ValueTableModel(final ZModeleSegment _seg) {
      super();
      segModel_ = _seg;
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      return _columnIndex == 0 ? Integer.class : Double.class;
    }

    @Override
    public int getColumnCount() {
      return 6;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return EbliLib.getS("Indices");
      }
      if (_column == 1) {
        return "X";
      }
      // Y
      if (_column == 2) {
        return "Y";
      }
      if (_column == 3) {
        return EbliLib.getS("Norme");
      }
      if (_column == 4) {
        return EbliLib.getS("Vx");
      }
      if (_column == 5) {
        return EbliLib.getS("Vy");
      }
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return segModel_ == null ? 0 : segModel_.getNombre();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return new Integer(_rowIndex + 1);
      }
      // X
      if (_columnIndex == 1) {
        return CtuluLib.getDouble(segModel_.getX(_rowIndex));
      }
      // Y
      if (_columnIndex == 2) {
        return CtuluLib.getDouble(segModel_.getY(_rowIndex));
      }
      if (_columnIndex == 3) {
        return CtuluLib.getDouble(segModel_.getNorme(_rowIndex));
      }
      if (_columnIndex == 4) {
        return CtuluLib.getDouble(segModel_.getVx(_rowIndex));
      }
      if (_columnIndex == 5) {
        return CtuluLib.getDouble(segModel_.getVy(_rowIndex));
      }
      // la valeur au point demande (si non null)
      return null;
    }
  }

  public static void fillWithInfo(final InfoData _d, final CtuluListSelectionInterface _selection, final ZModeleSegment _seg, final StringInfo _infos) {
    _d.put(_infos.nbVecteurs_, CtuluLibString.getString(_seg.getNombre()));
    final int nbSelected = CtuluLibArray.getSelectedIdxNb(_selection);
    _d.put(_infos.nbVecteursSelectionne_, CtuluLibString.getString(nbSelected));
    if (nbSelected == 1) {
      final int idx = _selection.getMaxIndex();
      _d.setTitle(_infos.titleIfOne_ + CtuluLibString.ESPACE + CtuluLibString.getString(idx + 1));
      _d.put(_infos.x_, _infos.formatXY(_seg.getX(idx)));
      _d.put(_infos.y_, _infos.formatXY(_seg.getY(idx)));
      if (_infos.norme_ != null) {
        _d.put(_infos.norme_, _infos.formatVect(_seg.getNorme(idx)));
      }
      if (_infos.vx_ != null) {
        _d.put(_infos.vx_, _infos.formatVect(_seg.getVx(idx)));
      }
      if (_infos.vy_ != null) {
        _d.put(_infos.vy_, _infos.formatVect(_seg.getVy(idx)));
      }

    } else if (nbSelected == 2) {
      final int idx = _selection.getMaxIndex();
      final int idx2 = _selection.getMinIndex();
      _d.put(EbliLib.getS("Distance entre les 2 points"),
          _infos.formatVect(CtuluLibGeometrie.getDistance(_seg.getX(idx), _seg.getY(idx), _seg.getX(idx2), _seg.getY(idx2))));

    }
  }

  protected ZCalqueFlecheLegend flecheLegend_;

  final FlecheScaleData scale_ = new FlecheScaleData(this);
  final FlecheGrilleData grille_ = new FlecheGrilleData(this);

  public ZCalqueFleche() {
    super();

    // Par defaut aucune plage, mais la palette couleur est cr��e (n�cessaire pour affichage
    // du composant fleche).
    flecheLegend_ = new ZCalqueFlecheLegend(this);
    BPalettePlage newPlage = new BPalettePlage(new BPlageInterface[0]);
    setPaletteCouleurPlages(newPlage);
  }

  protected boolean isGrilleActivated() {
    return grille_.isActive();
  }

  protected boolean isDensiteModeleActivated() {
    return grille_.isDensiteActive();
  }

  /**
   * @param _modele
   */
  public ZCalqueFleche(final ZModeleFleche _modele) {
    super(_modele);
  }

  public void setFlecheModele(ZModeleFleche _s) {
    super.setModele(_s);
  }

  @Override
  public void setModele(ZModeleSegment _s) {
    if (_s==null || _s instanceof ZModeleFleche)
      setFlecheModele((ZModeleFleche) _s);
    else {
      FuLog.error(new Throwable("ZmodeleFleche is expected"));
    }
  }

  protected void setGrilleOrDensiteModele(ZModeleFleche _fleche) {
    super.setModele(_fleche);
    repaint();
  }

  public Font getFontForFlechLegend() {
    return scale_.getScaleFont();
  }

  public void setFontForFlechLegend(Font _f) {
    Font old = getFontForFlechLegend();
    if (!old.equals(_f)) {
      scale_.setScaleFont(_f);
      if (flecheLegend_ != null) {
        flecheLegend_.lbInfo_.setFont(_f);
        getLegende().repaint();
        firePropertyChange("scale.font", old, _f);
      }
    }
  }

  @Override
  protected void construitLegende() {
    final BCalqueLegende l = getLegende();
    if (l == null) {
      return;
    }
    
    if (paletteLegende_ == null) {
      paletteLegende_ = new BPalettePlageLegende();
      paletteLegende_.addUserComponent(flecheLegend_.getFlecheComponent());
    }
    
    paletteLegende_.setModel(paletteCouleur_);
    
    super.construitLegende();
    updateLegendeTitre();
  }

  @Override
  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[4 + getNbSet()];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    for (int i = 1; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(this, i - 1);
    }
    sect[sect.length - 3] = new ZCalqueFlecheScaleSection(this);
    sect[sect.length - 2] = new ZCalqueFlecheDensiteSection(this);
    sect[sect.length - 1] = new ZCalqueFlecheGrilleSection(this);
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  protected String getFlecheUnit() {
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  protected void toEcran(GrSegment _seg, GrMorphisme _versEcran) {
    scale_.normalizeFleche(_seg, _versEcran);
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new CalqueFindFlecheExpression(modele_, false);
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new CalqueFindActionDefault(this);
  }

  @Override
  public LineString getSelectedLine() {
    if (getNbSelected() != 2) {
      return null;
    }
    final Coordinate[] cs = new Coordinate[2];
    cs[0] = new Coordinate(modele_.getX(getLayerSelection().getMinIndex()), modele_.getY(getLayerSelection().getMinIndex()));
    cs[1] = new Coordinate(modele_.getX(getLayerSelection().getMaxIndex()), modele_.getY(getLayerSelection().getMaxIndex()));
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    final GrSegment seg = new GrSegment(new GrPoint(), new GrPoint());
    int m = selection_.getMaxIndex();
    if (m > modele_.getNombre()) {
      m = modele_.getNombre() - 1;
    }
    final GrBoite r = new GrBoite();
    final GrMorphisme versEcran = getVersEcran();
    for (int i = selection_.getMinIndex(); i <= m; i++) {
      if (selection_.isSelected(i)) {
        // recuperation du polygone
        modele_.segment(seg, i, true);
        seg.o_.autoApplique(versEcran);
        r.ajuste(seg.o_);
      }
    }
    // ajusteZoomOnSelected(r);
    return r;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      scale_.initFrom(_p);
      if (scale_.scaleFont_ != null && flecheLegend_ != null) {
        flecheLegend_.lbInfo_.setFont(scale_.scaleFont_);
      }
      grille_.initFrom(_p);
    }
  }

  @Override
  public boolean isConfigurable() {
    return true;
  }

  @Override
  public boolean legendContainsOnlyPalette() {
    return false;
  }

  /**
   * @param _rToAjust la boite a ajuster
   * @return true si intersection entre le domaine et la vue en cours
   */
  public void getBoiteForGrille(GrBoite _rToAjust) {

    _rToAjust.e_.setCoordonnees(0, 0, 0);
    _rToAjust.o_.setCoordonnees(0, 0, 0);
    _rToAjust.ajuste(getWidth(), getHeight(), 0);
    _rToAjust.autoApplique(getVersReel());
    _rToAjust.autoIntersectionXY(modele_.getDomaine());

  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    //to update view:
    this.grille_.updateView();
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    // le clip d'affichage
    final TraceIcon icone = iconModel_ == null ? null : new TraceIcon(new TraceIconModel(iconModel_));
    final TraceLigne tl = new TraceLigne(new TraceLigneModel(ligneModel_));
    final TraceIconModel icModel = icone == null ? null : icone.getModel();
    final TraceLigneModel lgModel = tl.getModel();
    initColors(icone, tl);
    paintGrille(_g, _versEcran, tl);
    scale_.initFactor();
    if (flecheLegend_ != null) {
      flecheLegend_.init();
    }
    final int nombre = modele_.getNombre();
    //if(!grille_.isActive()) tenter de sauter des pas pour la densit�
    for (int i = 0; i < nombre; i++) {
      // recuperation du polygone
      // le segment i n'est pas correct ou pas activ�: on saute
      if (!modele_.segment(seg_, i, true) || seg_.e_.distanceXY(seg_.o_) < 1E-3) {
        continue;
      }
      if (flecheLegend_ != null) {
        flecheLegend_.addNorme(seg_.longueurXY());
      }
      if (scale_.minNormeValueToDraw_ > 0 && seg_.longueurXY() < scale_.minNormeValueToDraw_) {
        continue;
      }

      // recuperation de la boite dans bPoly
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (_clipReel.contientXY(seg_.o_)) {
        updateTraceLigne(lgModel, icModel, seg_, i);
        toEcran(seg_, _versEcran);
        // Si trac� invisible on passe.
        if (lgModel.getTypeTrait() != TraceLigne.INVISIBLE) {
          tl.dessineFleche(_g, (int) seg_.o_.x_, (int) seg_.o_.y_, (int) seg_.e_.x_, (int) seg_.e_.y_);
        }
        if (icone != null) {
          icone.paintIconCentre(this, _g, seg_.o_.x_, seg_.o_.y_);
        }
      }
    }

    if (flecheLegend_ != null) {
      flecheLegend_.finish();
    }
  }

  private void initColors(final TraceIcon _icone, final TraceLigne _tl) {
    if (isAttenue()) {
      _tl.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_tl.getCouleur()), alpha_));
      if (_icone != null) {
        _icone.setCouleur(EbliLib.getAlphaColor(attenueCouleur(_icone.getCouleur()), alpha_));
      }
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      _tl.setCouleur(EbliLib.getAlphaColor(_tl.getCouleur(), alpha_));
      if (_icone != null) {
        _icone.setCouleur(EbliLib.getAlphaColor(_icone.getCouleur(), alpha_));
      }
    }
  }

  private void paintGrille(final Graphics2D _g, final GrMorphisme _versEcran, final TraceLigne _tl) {
    if (grille_.isActive() && grille_.isPaintGrille()) {
      int xStep = grille_.getNbXLines();
      int yStep = grille_.getNbYLines();

      Color old = _tl.getCouleur();
      _tl.setCouleur(Color.LIGHT_GRAY);
      ZModeleFlecheForGrille grilleModel = grille_.getGrilleFlecheModel();
      for (int ix = 0; ix < xStep; ix++) {
        double xReal = grilleModel.getXForCol(ix);
        double yMin = grilleModel.getYForRow(0);
        double yMax = grilleModel.getYForRow(yStep - 1);
        seg_.e_.x_ = xReal;
        seg_.o_.x_ = xReal;
        seg_.e_.y_ = yMax;
        seg_.o_.y_ = yMin;
        seg_.autoApplique(_versEcran);
        _tl.dessineTrait(_g, seg_.e_.x_, seg_.e_.y_, seg_.o_.x_, seg_.o_.y_);
      }
      for (int iy = 0; iy < yStep; iy++) {
        double yReal = grilleModel.getYForRow(iy);
        double xMin = grilleModel.getXForCol(0);
        double xMax = grilleModel.getXForCol(xStep - 1);
        seg_.e_.x_ = xMax;
        seg_.o_.x_ = xMin;
        seg_.e_.y_ = yReal;
        seg_.o_.y_ = yReal;
        seg_.autoApplique(_versEcran);
        _tl.dessineTrait(_g, seg_.e_.x_, seg_.e_.y_, seg_.o_.x_, seg_.o_.y_);
      }
      _tl.setCouleur(old);
    }
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final TraceLigne l = new TraceLigne();
    l.setCouleur(Color.BLUE);
    l.setEpaisseur(2);
    final int h = getIconHeight();
    final int w = getIconWidth();
    l.dessineFleche((Graphics2D) _g, _x + 3, _y + h - 3, _x + 8, _y + 3);
    l.setCouleur(Color.ORANGE);
    l.dessineFleche((Graphics2D) _g, _x + w - 7, _y + h - 5, _x + w - 4, _y + 8);
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties cui = super.saveUIProperties();
    scale_.saveUIProperties(cui);
    grille_.saveUIProperties(cui);
    return cui;
  }

  public FlecheScaleData getScaleData() {
    return scale_;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {

    if (!isVisible()) {
      return null;
    }
    final GrBoite bClip = getDomaine();
    if(bClip==null){
      return null;
    }
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > GrMorphisme.convertDistanceXY(getVersReel(), _tolerance))) {
      return null;
    }
    final GrSegment poly = seg_;
    GrMorphisme versEcran = getVersEcran();
    _pt.autoApplique(versEcran);
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      modele_.segment(poly, i, true);
      // on fait cela car dans le cas des vecteurs la norme ecran peut etre modifiee
      toEcran(poly, versEcran);
      if (poly.distanceXY(_pt) <= _tolerance) {
        final CtuluListSelection r = creeSelection();
        r.add(i);
        return r;
      }
    }
    return null;
  }

  @Override
  public CtuluListSelection selection(final LinearRing _polySelection, final int _mode) {
    final GrBoite domaineBoite = getDomaine();
    if (modele_.getNombre() == 0 || !isVisible()||domaineBoite==null) {
      return null;
    }
    final Envelope polyEnv = _polySelection.getEnvelopeInternal();

    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_, domaineBoite.o_.y_);
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final CtuluListSelection r = creeSelection();
    final Coordinate c = new Coordinate();
    final PointOnGeometryLocator tester = new IndexedPointInAreaLocator(_polySelection);
    for (int i = modele_.getNombre() - 1; i >= 0; i--) {
      double x = modele_.getX(i);
      double y = modele_.getY(i);
      if (polyEnv.contains(x, y)) {
        c.x = x;
        c.y = y;
        if (GISLib.isSelectedEnv(c, _polySelection, polyEnv, tester)) {
          r.add(i);
        }
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  /**
   * @return the flecheLegend_
   */
  public ZCalqueFlecheLegend getFlecheLegend() {
    return flecheLegend_;
  }
}
