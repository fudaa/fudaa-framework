/*
 *  @creation     15 avr. 2005
 *  @modification $Date: 2008-02-01 14:42:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuScrollPane;
import javax.swing.JTable;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;


/**
 * Un panneau d'edition pour les valeurs atomiques d'un objet (les points). Ce panneau ne peut �tre utilis� que pour les points
 * d'un seul objet. Il panneau est associ� a un mod�le editable de calque.
 * @author Fred Deniger
 * @version $Id: EbliAtomicsEditorPanel.java,v 1.6.8.1 2008-02-01 14:42:43 bmarchan Exp $
 */
public class EbliAtomicsEditorPanel extends CtuluDialogPanel {

  final CtuluCommandContainer           cmd_;
  final EbliAtomicCoordinatesTableModel tableModel_;
  final JTable                          table_;
  ZEditorValidatorI                     editValidator_;
  ZModeleEditable                       mdl_;

  /**
   * @param _idx l'indice de la ligne brisee selectionnee
   * @param _vertex les sommets selectionnes
   * @param _xyFormatter le formatteur de xy
   * @param _modele le modele source
   * @param _editVertex true si les donn�es associ�es aux sommets doivent etre modifiees.
   * @param _cmd la commande
   *
   */
  public EbliAtomicsEditorPanel(final int _idx, final int[] _vertex, final EbliCoordinateDefinition[] _defs,
      final ZModeleEditable _modele, final boolean _editVertex, ZEditorValidatorI _validator, final CtuluCommandManager _cmd) {
    this(new EbliAtomicCoordinatesTableModel.Line(_defs, _modele.getGeomData(), _idx, _vertex, _editVertex,
      _modele), _cmd);
    
    mdl_ = _modele;
    editValidator_ = _validator;
  }

  public EbliAtomicsEditorPanel(final EbliAtomicCoordinatesTableModel _model, final CtuluCommandManager _cmd) {
    tableModel_ = _model;
    cmd_ = _cmd;
    table_ = new CtuluTable();
    table_.setModel(tableModel_);
    tableModel_.updateEditorAndRenderer(table_);
    setLayout(new BuBorderLayout());
    add(new BuScrollPane(table_));
  }

  @Override
  public boolean apply(){
    if (editValidator_ != null && !editValidator_.isAtomicEditionValid(mdl_))
      return false;
    
    final CtuluCommandComposite cmp = cmd_ == null ? null : new CtuluCommandComposite();
    tableModel_.apply(cmp);
    if (cmd_ != null) {
      cmd_.addCmd(cmp.getSimplify());
    }
    return true;
  }

  @Override
  public boolean isDataValid(){
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }
    final CtuluAnalyze ana = tableModel_.isValid();
    if (ana != null && ana.containsFatalError()) {
      setErrorText(ana.getFatalError());
      return false;
    }
    return true;
  }
}
