/**
 *  @creation     2002-09-20
 *  @modification $Date: 2006-04-12 15:27:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
/**
 * Le modele du calque d'affichage de polygone.
 *
 * @version   $Id: ZModelePolygone.java,v 1.8 2006-04-12 15:27:10 deniger Exp $
 * @author    Fred Deniger
 */
public interface ZModelePolygone extends ZModeleDonnees{
  /**
   * Affecte a _p les valeurs du polygone i.
   * @param _p le polygone a modifier
   * @param _i index
   * @param _force TODO
   */
  void polygone(GrPolygone _p, int _i, boolean _force);
  
  boolean isPainted(int idx);
  
  boolean getCentre(GrPoint pt,int _i, boolean _force);
  
}
