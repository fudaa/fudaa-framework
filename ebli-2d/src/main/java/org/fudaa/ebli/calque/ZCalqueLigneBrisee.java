/*
 *  @creation     31 mars 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Un calque g�rant des lignes bris�es, ouvertes ou ferm�es � un niveau global ou atomique.<p> Le niveau atomique des lignes bris�es est le niveau
 * points. Le calque s'appuie sur un mod�le {@link ZModeleLigneBrisee}.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class ZCalqueLigneBrisee extends ZCalqueGeometry {

  /**
   * Le modele d'icone pour les lignes ouvertes
   */
  protected TraceIconModel iconeModelOuvert_;
  /**
   * Le modele de lignes pour les lignes ouvertes
   */
  protected TraceLigneModel ligneModelOuvert_;
  /**
   * On dessine une fl�che sur chaque ligne pour indiquer le sens de celle-ci.
   */
  protected boolean showLineOrientation_;

  /**
   *
   */
  public ZCalqueLigneBrisee() {
    this(null);
  }

  /**
   * @param _modele le modele du calque
   */
  public ZCalqueLigneBrisee(final ZModeleLigneBrisee _modele) {
    super(_modele);
    modele_ = _modele;
    if (modele_ != null) {
      modele_.addModelListener(this);
    }
    iconModel_ = new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.BLACK);
    ligneModelOuvert_ = new TraceLigneModel(ligneModel_);
    iconeModelOuvert_ = new TraceIconModel(iconModel_);
    showLineOrientation_ = false;
  }

  public boolean getShowLineOrientation() {
    return showLineOrientation_;
  }

  public void setShowLineOrientation(boolean _value) {
    if (showLineOrientation_ != _value) {
      showLineOrientation_ = _value;
      clearCacheAndRepaint();
    }
  }

  @Override
  protected void initTrace(final TraceLigneModel _ligne, final int _idxPoly) {
    final int idx = modele_.isGeometryFermee(_idxPoly) ? 0 : 1;
    _ligne.updateData(getLineModel(idx));
    _ligne.setCouleur(getColorWithAttenuAlpha(_ligne.getCouleur()));
  }

  public static void initTraceWithAlpha(TraceIconModel model, int alpha) {
    if (model.getCouleur() != null && model.getCouleur().getAlpha() != alpha) {
      model.setCouleur(EbliLib.getAlphaColor(model.getCouleur(), alpha));
    }
    if (model.getBackgroundColor() != null && model.getBackgroundColor().getAlpha() != alpha) {
      model.setBackgroundColor(EbliLib.getAlphaColor(model.getBackgroundColor(), alpha));
    }
  }

  protected static void initTraceWithAlpha(TraceLigneModel model, int alpha) {
    if (model.getCouleur() != null && model.getCouleur().getAlpha() != alpha) {
      model.setCouleur(EbliLib.getAlphaColor(model.getCouleur(), alpha));
    }
  }

  @Override
  protected void initTrace(final TraceIconModel _icon, final int _idxPoly) {
    final int idx = modele_.isGeometryFermee(_idxPoly) ? 0 : 1;
    _icon.updateData(getIconModel(idx));
    _icon.setCouleur(getColorWithAttenuAlpha(_icon.getCouleur()));
  }

  @Override
  public TraceIconModel getIconModel(final int _idx) {
    if (_idx == 1) {
      return iconeModelOuvert_;
    }
    return iconModel_;
  }

  @Override
  public TraceLigneModel getLineModel(final int _idx) {
    if (_idx == 1) {
      return ligneModelOuvert_;
    }
    return ligneModel_;
  }

  @Override
  public int getNbSet() {
    return 2;
  }

  @Override
  public String getSetTitle(final int _idx) {
    if (_idx == 0) {
      return EbliLib.getS("Lignes ferm�es");
    }
    return EbliLib.getS("Lignes ouvertes");
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      if (_p.isDefined("calqueLigne.ligneModel")) {
        setLineModel(0, (TraceLigneModel) _p.get("calqueLigne.ligneModel"));
      }
      if (_p.isDefined("calqueLigne.ligneModelOuvert")) {
        setLineModelOuvert((TraceLigneModel) _p.get("calqueLigne.ligneModelOuvert"));
      }
    }
  }

  /**
   * @param _modele Modele
   */
  public void modele(final ZModeleLigneBrisee _modele) {
    if (modele_ != _modele) {
      if (modele_ != null) {
        modele_.removeModelListener(this);
      }
      final ZModeleGeometry vp = modele_;
      modele_ = _modele;
      if (modele_ != null) {
        modele_.addModelListener(this);
      }
      firePropertyChange("modele", vp, modele_);
    }
  }

  @Override
  public ZModeleLigneBrisee modeleDonnees() {
    return (ZModeleLigneBrisee) modele_;
  }

  protected boolean showOrientation(int idxPoly) {
    return showLineOrientation_;
  }

  /**
   * @param _g
   */
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }

    paintSurfaces(_g, _versEcran, _versReel, _clipReel);

    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final int nombre = modele_.getNombre();

    final TraceLigne tl;
    if (isRapide()) {
      tl = new TraceLigne(TraceLigne.LISSE, 1, ligneModel_.getCouleur());
    } else {
      tl = ligneModel_.buildCopy();
    }

    final TraceLigneModel ligneModel = tl.getModel();
    final GrBoite bPoly = new GrBoite();
    bPoly.o_ = new GrPoint();
    bPoly.e_ = new GrPoint();
    Shape oldClip = _g.getClip();
    GrBoite clipEcran = _clipReel.applique(_versEcran);

    // on part de la fin : comme ca la premiere ligne apparait au-dessus
    for (int i = nombre - 1; i >= 0; i--) {
      if (!isPainted(i, _versEcran)) {
        continue;
      }
      // il n'y a pas de points pour cette ligne
      if (modele_.getNbPointForGeometry(i) <= 0) {
        continue;
      }
      modele_.getDomaineForGeometry(i, bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (!bPoly.intersectXY(clip)) {
        continue;
      }
      final int nbPoints = modele_.getNbPointForGeometry(i);
      if (nbPoints <= 0) {
        continue;
      }
      updateClip(_g, oldClip, _versEcran, i);

      if (!isRapide()) {
        initTrace(ligneModel, i);
      }

      if (nbPoints < 2) {
        continue;
      }

      // Affichage de la polyligne ou polygone \\
      /*
       * On part de la fin pour commencer � afficher la fleche si l'option est activ�e. Pour �tre sur que la fleche s'affiche
       * correctement, on recherche deux points non confondus.
       */
      GrPoint ptDest = new GrPoint();
      GrPoint ptOrig = new GrPoint();
      int j;

      // Si c'est une polyligne on prend le dernier point si c'est un polygone,
      // on prend le premier (le permier et le dernier doivent �tre confondus).
      if (modele_.isGeometryFermee(i)) {
        modele_.point(ptDest, i, 0);
        modele_.point(ptOrig, i, nbPoints - 1);
        j = nbPoints - 1;
      } else {
        modele_.point(ptDest, i, nbPoints - 1);
        modele_.point(ptOrig, i, nbPoints - 2);
        j = nbPoints - 2;
      }
      // Recherche du point pr�c�dent non identique
      while (ptOrig.x_ == ptDest.x_ && ptOrig.y_ == ptDest.y_ && j > 0) {
        modele_.point(ptOrig, i, --j);
      }
      if (j == 0 && (ptOrig.x_ == ptDest.x_ && ptOrig.y_ == ptDest.y_)) {
        continue;
      }
      ptOrig.autoApplique2D(versEcran);
      ptDest.autoApplique2D(versEcran);


      // Dessin de la fleche
      if (showOrientation(i) && clipEcran.contientXY(ptDest)) {
        GrVecteur vct = ptDest.soustraction(ptOrig);
        vct.autoNormaliseXY();
        vct.autoMultiplication(20);
        // Cr�ation d'un point interm�diare
        double xorig = ptDest.x_ - vct.x_;
        double yorig = ptDest.y_ - vct.y_;
        // La point est lisse
        int typeTrait = tl.getTypeTrait();
        tl.setTypeTrait(TraceLigne.LISSE);
        tl.dessinePointe(_g, (int) xorig, (int) yorig, (int) ptDest.x_, (int) ptDest.y_, getFlecheSize(), getFlecheSize());
        tl.setTypeTrait(typeTrait);
      }

      paintValueOfPoint(_g, ptDest, j+1, _versEcran, _clipReel);
      // Dessin des segments
      int nbPainted = 0;
      for (; j >= 0; j--) {
        modele_.point(ptOrig, i, j);
        ptOrig.autoApplique2D(versEcran);
        int x1 = (int) ptOrig.x_;
        int y1 = (int) ptOrig.y_;
        int x2 = (int) ptDest.x_;
        int y2 = (int) ptDest.y_;
        //pour �viter de dessiner des lignes sur un pixel:
        if ((x1 != x2 || y1 != y2) && clipEcran.intersectXYBoite(ptOrig.x_, ptOrig.y_, ptDest.x_, ptDest.y_)) {
          tl.dessineTrait(_g, ptOrig.x_, ptOrig.y_, ptDest.x_, ptDest.y_);
          nbPainted++;
        }
        ptDest.initialiseAvec(ptOrig);
        paintValueOfPoint(_g, ptOrig, j, _versEcran, _clipReel);
      }
      
    }
    _g.setClip(oldClip);

    // Les icones sur les atomiques
    if (!isRapide()) {
      paintIconsOnAtomics(_g, _versEcran, _versReel, _clipReel);
    }
    // Les labels sur les g�om�tries
    if (isLabelsVisible())
      paintLabelsOnGeometries(_g, _versEcran, _versReel, _clipReel);
    // Enfin les labels sur les atomiques.
    paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
  }
  
  public void paintValueOfPoint(final Graphics2D _g, final GrPoint p, final int indicePoint, final GrMorphisme _versEcran, final GrBoite _clipReel) {
      
  }

  public void setLineModelOuvert(final TraceLigneModel _model) {
    if (_model == null) {
      return;
    }
    if (ligneModelOuvert_ == null) {
      ligneModelOuvert_ = new TraceLigneModel(_model);
    } else {
      ligneModelOuvert_.updateData(_model);
    }
    firePropertyChange(BSelecteurLineModel.PROPERTY + "open", null, ligneModelOuvert_);
  }

  /**
   * To be used to update clip before painting a line
   *
   * @param _g
   * @param oldClip clip at the beginning of paintDonnees.
   */
  protected void updateClip(Graphics2D _g, Shape oldClip, GrMorphisme _versEcran, int idx) {
  }

  public int getFlecheSize() {
    return 11;
  }
}
