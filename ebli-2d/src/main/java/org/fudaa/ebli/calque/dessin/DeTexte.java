/*
 * @file         DeTexte.java
 * @creation     1998-08-31
 * @modification $Date: 2006-07-13 13:35:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Font;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un texte.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-07-13 13:35:49 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class DeTexte extends DePoint {
  // donnees membres publiques
  // donnees membres privees
  String text_;
  Font police_;
  // Constructeurs
  public DeTexte(final String _tex, final GrPoint _pos) {
    super(_pos);
    text_= _tex;
    police_= new Font("Times", Font.PLAIN, 12);
  }
  public DeTexte() {
    super();
    text_= "";
    police_= new Font("Times", Font.PLAIN, 12);
  }
  // Methodes publiques
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
    _t.setFont(police_);
    _t.dessineTexte(_g2d,text_, position_, _rapide);
  }
  @Override
  public int getForme() {
    return TEXTE;
  }
  /**
   * Definition de la fonte.
   * @param _p Police.
   */
  public void setFont(final Font _p) {
    police_= _p;
  }
  /**
   * Retourne la fonte du texte.
   * @return La police.
   */
  public Font getFont() {
    return police_;
  }
  /**
   * Définition du texte.
   * @param _t Texte.
   */
  public void setText(final String _t) {
    text_= _t;
  }
  /**
   * Retourne le texte.
   * @return Le texte.
   */
  public String getText() {
    return text_;
  }
}
