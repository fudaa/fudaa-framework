/*
 * @creation 11 sept. 06
 * @modification $Date: 2007/05/04 13:49:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author fred deniger
 * @version $Id: BCalqueImagePersistence.java,v 1.10 2007/05/04 13:49:42 deniger Exp $
 */
public class BCalqueImagePersistence extends BCalquePersistenceSingle {

  public static String getImageId() {
    return "raster.image";
  }

  public static String getImageFile() {
    return "raster.image.file";
  }

  public static String getPointImageId() {
    return "raster.point.img";
  }

  public static String getPointRealId() {
    return "raster.point.real";
  }

  @Override
  public void readXmlSpecific(final CtuluXmlReaderHelper _helper, final BCalqueSaverInterface _target) {
    String img = _helper.getTrimTextFor("relative-path");
    File imgFile = null;
    // on teste en premier le chemin relatif
    if (img != null) {
      imgFile = new File(dir_, img);
      try {
        if (imgFile.exists()) {
          imgFile = imgFile.getCanonicalFile();
        }
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
    }
    if (imgFile == null || !imgFile.exists()) {
      img = _helper.getTrimTextFor("file");
      if (img != null) {
        imgFile = new File(img);
      }
    }
    // l'image n'est toujours pas trouv�e:
    // on teste si l'image est dans le dossier courant
    if (imgFile != null && !imgFile.exists()) {
      imgFile = new File(dir_, imgFile.getName());
    }
    // On renseigne malgr� tout l'entr�e file pour pouvoir afficher le chemin de
    // l'image � l'utilisateur pour qu'il puisse le changer.
    if (imgFile != null && !imgFile.exists()) {
      _target.getUI().put(getImageFile(), _helper.getTrimTextFor("file"));
    }
    else {
      _target.getUI().put(getImageFile(), imgFile.getAbsolutePath());
    }
      
    /*if (imgFile == null || !imgFile.exists()) {
      FuLog.warning("ECQ: no file");
      return;
    }
    _target.getUI().put(getImageFile(), imgFile.getAbsolutePath());
*/
    
    final NodeList list = _helper.getDoc().getElementsByTagName("point");

    if (list != null && list.getLength() > 0) {
      final int nb = list.getLength();
      final List realList = new ArrayList(nb + 1);
      final List imgList = new ArrayList(nb + 1);
      for (int i = 0; i < nb; i++) {
        final Node nodei = list.item(i);
        // il possede du texte
        if (nodei.getChildNodes().getLength() > 0) {
          final Node item = nodei.getFirstChild();
          String nodeValue = item.getNodeValue();
          if (nodeValue != null) {
            nodeValue = nodeValue.trim();
          }
          final String[] tokens = CtuluLibString.parseString(nodeValue, CtuluLibString.ESPACE);
          if (tokens.length == 4) {
            Point2D.Double imgPt = null;
            Point2D.Double realPt = null;
            try {
              imgPt = new Point2D.Double(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]));
              realPt = new Point2D.Double(Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3]));
            } catch (final NumberFormatException _evt) {
              FuLog.error(_evt);
            } finally {
              if (imgPt != null && realPt != null) {
                imgList.add(imgPt);
                realList.add(realPt);
              }
            }
          }
        }
      }
      if (realList.size() >= 2) {
        _target.getUI().put(getPointImageId(), imgList.toArray(new Point2D.Double[imgList.size()]));
        _target.getUI().put(getPointRealId(), realList.toArray(new Point2D.Double[imgList.size()]));
      }
    }
  }
  
  @Override
  protected void writeBodyData(final CtuluXmlWriter _out, final BCalque _cqToSave) throws IOException {
    final ZModeleImageRaster img=((ZCalqueImageRaster)_cqToSave).getModelImage();
    if (img == null) {
      FuLog.error("ECQ: no image");
      return;
    }
    if (img.getData() == null) {
      FuLog.error("ECQ: no image data");
      return;
    }
    final File f = img.getData().getImgFile();
    
    _out.write("<!--Le fichier contenant l'image: chemin absolu-->\n");
    _out.write("<!--The absolute path of the image file-->\n");
    _out.write("<file>\n");
    _out.write(f.getAbsolutePath());
    _out.write("</file>\n");
    _out.write("<!--Le chemin relatif-->\n");
    _out.write("<!--The relative path-->\n");
    _out.write("<relative-path>\n");
    _out.write(CtuluLibFile.getRelativeFile(f, dir_, 100));
    _out.write("</relative-path>\n");
    _out.write("<points>\n");
    _out.write("<!--image_x image_y_from_top real_x real_y-->");
    _out.write("<!--pass points-->");

    final int nb=img.getData().getNbPt();
    for (int i=0; i<nb; i++) {
      _out.write("<point>\n");
      _out.write(Integer.toString(img.getXImgCalage(i)));
      _out.write(CtuluLibString.ESPACE);
      _out.write(Integer.toString(img.getYImgCalageFromTop(i)));
      _out.write(CtuluLibString.ESPACE);
      _out.write(CtuluLib.DEFAULT_NUMBER_FORMAT.format(img.getX(i)));
      _out.write(CtuluLibString.ESPACE);
      _out.write(CtuluLib.DEFAULT_NUMBER_FORMAT.format(img.getY(i)));
      _out.write("</point>\n");
    }

    _out.write("</points>");
  }

  @Override
  public BCalqueSaverInterface save(final BCalque _cq, final ProgressionInterface _prog) {
    final BCalqueSaverInterface all = super.save(_cq, _prog);
    final ZCalqueImageRaster img = (ZCalqueImageRaster) _cq;
    all.getUI().put(getImageId(), img.getModelImage().getImage());
    all.getUI().put(getPointImageId(), img.getModelImage().getData().getImgPts());
    all.getUI().put(getPointRealId(), img.getModelImage().getData().getRealPts());
    final File imgFile = img.getModelImage().getData().getImgFile();
    if (imgFile != null) {
      all.getUI().put(getImageFile(), imgFile.getAbsolutePath());
    }
    return all;
  }

  @Override
  protected BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent) {
    /*
     * final BufferedImage img = (BufferedImage) _cqName.getUI().get(getImageId()); final Point2D.Double[] ptImg =
     * (Point2D.Double[]) _cqName.getUI().get(getPointImageId()); final Point2D.Double[] ptReal = (Point2D.Double[])
     * _cqName.getUI().get(getPointRealId()); if (img == null || ptImg == null || ptReal == null || ptReal.length !=
     * ptImg.length || ptReal.length < 3) { return null; }
     */
    final ZCalqueImageRaster res = new ZCalqueImageRaster();
    res.setTitle(CtuluLib.getS("Image"));
    res.setName(BGroupeCalque.findUniqueChildName(_parent));
    _parent.enDernier(res);
    return res;
  }

  @Override
  public String restoreFrom(final CtuluArkLoader _loader, final BCalqueSaverTargetInterface _parentPanel, final BCalque _parentCalque,
      final String _parentDirEntry, final String _entryName, final ProgressionInterface _proj) {
    String cqName=_loader.getLayerProperty(_parentDirEntry,_entryName, "name");
    if (CtuluLibString.toBoolean(_loader.getOption(CtuluArkLoader.OPTION_LAYER_IGNORE+cqName))) return null;

    dir_ = _loader.getDestDir();
    return super.restoreFrom(_loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj);
  }

  @Override
  public String restore(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent, final ProgressionInterface _prog) {
    final String res = super.restore(_saver, _pn, _parent, _prog);
    final ZCalqueImageRaster cq = (ZCalqueImageRaster) _parent.getCalqueParNom(res);
    // final BufferedImage img = (BufferedImage) _saver.getUI().get(getImageId());
    final Point2D.Double[] ptImg = (Point2D.Double[]) _saver.getUI().get(getPointImageId());
    final Point2D.Double[] ptReal = (Point2D.Double[]) _saver.getUI().get(getPointRealId());
    String tmp=getImageFile();
    final String file = _saver.getUI().getString(getImageFile());
    if (ptImg == null || ptReal == null || ptReal.length != ptImg.length || ptReal.length < 2)
      return null;
    
    if (file != null) {
      final File f = new File(file);
      final ZModeleImageRaster model = new ZModeleStatiqueImageRaster(new CtuluImageContainer(f));
      model.setProj(ptImg, ptReal);
      cq.setModele(model);
      return res;
    }
    return null;
  }

  File dir_;

  @Override
  public BCalqueSaverInterface saveIn(final BCalque _cqToSave, final CtuluArkSaver _saver,
      final String _parentDirEntry, final String _parentDirIndice) {
    dir_ = _saver.getDestDir();
    return super.saveIn(_cqToSave, _saver, _parentDirEntry, _parentDirIndice);
  }

}
