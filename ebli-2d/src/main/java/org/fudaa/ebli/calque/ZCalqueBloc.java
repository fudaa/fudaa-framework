/*
 *  @creation     10 mars 2009
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import gnu.trove.TIntArrayList;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceSurface;

/**
 * Un calque g�rant des blocs, � un niveau global ou atomique.<p>
 * Le niveau atomique des blocs est le niveau points. Le calque s'appuie sur un mod�le
 * {@link ZModeleBloc}.
 * 
 * @author Bertrand Marchand
 * @status Experimental
 * @version $Id$
 */
public class ZCalqueBloc extends ZCalqueGeometry<ZModeleBloc> {

  /**
   * Le seul constructeur, avec le mod�le.
   * @param _modele le modele du calque
   */
  public ZCalqueBloc(final ZModeleBloc _modele) {
    super(_modele);
    iconModel_ = new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.BLACK);
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    final GrBoite bClip = getDomaine();
    if (modele_.getNombre() == 0||bClip==null) {
      return null;
    }
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }

    final GrPoint pt=new GrPoint();
    final GrSegment poly = new GrSegment(new GrPoint(), new GrPoint());
    final GrBoite bPoly = new GrBoite(new GrPoint(), new GrPoint());
    
    for (int ibloc = modele_.getNombre() - 1; ibloc >= 0; ibloc--) {
      if (!modele_.isGeometryVisible(ibloc)) continue;

      modele_.getDomaineForGeometry(ibloc, bPoly);
      if (!bPoly.contientXY(_pt) && bPoly.distanceXY(_pt)>=distanceReel)
        continue;
        
      int nbGeom=modele_.getNbGeometryForBloc(ibloc);
      for (int igeom=nbGeom-1; igeom>=0; igeom--) {
        if (modele_.isGeometryRelieeForBloc(ibloc,igeom)) {
          for (int j=modele_.getNbPointForGeometry(ibloc,igeom)-1; j>0; j--) {
            modele_.point(poly.e_, ibloc, igeom, j);
            modele_.point(poly.o_, ibloc, igeom, j-1);
            if (poly.distanceXY(_pt)<distanceReel) {
              final CtuluListSelection r=new CtuluListSelection(1);
              r.setSelectionInterval(ibloc, ibloc);
              return r;
            }
          }
          if (modele_.isGeometryFermeeForBloc(ibloc,igeom)) {
            modele_.point(poly.e_, ibloc, igeom, 0);
            modele_.point(poly.o_, ibloc, igeom, modele_.getNbPointForGeometry(ibloc, igeom)-1);
            if (poly.distanceXY(_pt)<distanceReel) {
              final CtuluListSelection r=new CtuluListSelection(1);
              r.setSelectionInterval(ibloc, ibloc);
              return r;
            }
          }
        }
        else {
          for (int j=modele_.getNbPointForGeometry(ibloc, igeom)-1; j>=0; j--) {
            modele_.point(pt, ibloc, igeom, j);
            if (pt.distanceXY(_pt)<distanceReel) {
              final CtuluListSelection r=new CtuluListSelection(1);
              r.setSelectionInterval(ibloc, ibloc);
              return r;
            }
          }
        }
      }
    }
    return null;
  }
  
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final int nbBloc = modele_.getNombre();

    final TraceLigne tl;
    if (isRapide())
      tl=new TraceLigne(TraceLigne.LISSE,1,ligneModel_.getCouleur());
    else
      tl= ligneModel_.buildCopy();
    
    final TraceSurface ts;
    if (surfModel_!=null) {
      ts=surfModel_.buildCopy();
      ts.setCouleurFond(EbliLib.getAlphaColor(ts.getCouleurFond(), alpha_));
    }
    else {
      ts=null;
    }
    
    final GrBoite bPoly = new GrBoite();
    bPoly.o_ = new GrPoint();
    bPoly.e_ = new GrPoint();

    // Les surfaces, si a tracer
    if (!isRapide() && ts!=null && ts.getTypeSurface()!=TraceSurface.INVISIBLE) {
      TIntArrayList x=new TIntArrayList(50);
      TIntArrayList y=new TIntArrayList(50);
      
      for (int ibloc=nbBloc-1; ibloc>=0; ibloc--) {
        // La g�ometrie n'est pas visible
        if (!modele_.isGeometryVisible(ibloc))
          continue;
        
        modele_.getDomaineForGeometry(ibloc, bPoly);
        // Si la boite du polygone n'est pas dans la boite d'affichage on
        // passe
        if (bPoly.intersectionXY(clip)==null) {
          continue;
        }

        int nbGeom=modele_.getNbGeometryForBloc(ibloc);
        for (int igeom=nbGeom-1; igeom>=0; igeom--) {
          if (!modele_.isGeometryRelieeForBloc(ibloc, igeom) ||
              !modele_.isGeometryFermeeForBloc(ibloc, igeom))
            continue;
          
          // il n'y a pas de points pour cette g�om�trie
          final int nbPoints=modele_.getNbPointForGeometry(ibloc,igeom);
          if (nbPoints<=0)
            continue;
          
          x.reset();
          y.reset();
          final GrPoint pt=new GrPoint();
          for (int j=0; j<nbPoints; j++) {
            // le point de dest est initialise
            modele_.point(pt, ibloc, igeom, j);
            pt.autoApplique(versEcran);
            x.add((int)pt.x_);
            y.add((int)pt.y_);
          }
          ts.remplitPolygone((Graphics2D)_g, x.toNativeArray(), y.toNativeArray());
        }
      }
    }
    
    // on part de la fin : comme ca la premiere ligne apparait au-dessus
    for (int ibloc = nbBloc - 1; ibloc >= 0; ibloc--) {
      // La g�ometrie n'est pas visible
      if (!modele_.isGeometryVisible(ibloc)) 
        continue;
      
      modele_.getDomaineForGeometry(ibloc, bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }
      
      int nbGeom=modele_.getNbGeometryForBloc(ibloc);
      for (int idxgeom=nbGeom-1; idxgeom>=0; idxgeom--) {
        if (!modele_.isGeometryRelieeForBloc(ibloc, idxgeom))
          continue;

        // il n'y a pas de points pour cette g�om�trie
        final int nbPoints=modele_.getNbPointForGeometry(ibloc, idxgeom);
        if (nbPoints<=0)
          continue;

        final GrPoint ptOri=new GrPoint();
        modele_.point(ptOri, ibloc, idxgeom, nbPoints-1);
        ptOri.autoApplique(versEcran);

        if (!isRapide())
          initTrace(ligneModel_, ibloc);
        final GrPoint ptDest=new GrPoint();
        for (int j=nbPoints-2; j>=0; j--) {
          // le point de dest est initialise
          modele_.point(ptDest, ibloc, idxgeom, j);
          ptDest.autoApplique(versEcran);
          tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
          ptOri.initialiseAvec(ptDest);
        }
        if (modele_.isGeometryFermeeForBloc(ibloc,idxgeom)) {
          modele_.point(ptOri, ibloc, idxgeom, nbPoints-1);
          ptOri.autoApplique(versEcran);
          tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        }
      }
    }

    // On trace les icones
    paintIconsOnAtomics(_g, _versEcran, _versReel, _clipReel);
    // Enfin les labels sur les atomiques.
    paintLabelsOnAtomics(_g, _versEcran, _versReel, _clipReel);
  }
  
  @Override
  public void paintSelectionSimple(final Graphics _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
      final GrBoite _clipReel, CtuluListSelection _sel) {
    if ((isRapide()) || (isSelectionEmpty())) {
      return;
    }
    final GrBoite clip = _clipReel;
    if (getDomaine() == null || !getDomaine().intersectXY(clip)) {
      return;
      
    }
    final GrMorphisme versEcran = _versEcran;
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    final int nbBloc = Math.min(_sel.getMaxIndex(), modele_.getNombre() - 1);

    final GrBoite btGeom = new GrBoite();
    btGeom.e_ = new GrPoint();
    btGeom.o_ = new GrPoint();
    for (int ibloc = nbBloc; ibloc >= 0; ibloc--) {
      if (!_sel.isSelected(ibloc)) {
        continue;
      }
      modele_.getDomaineForGeometry(ibloc, btGeom);
      // Si la boite de la geometrie n'est pas dans la boite d'affichage on passe
      if (btGeom.intersectionXY(clip) == null) {
        continue;
      }
      
      final int nbGeom=modele_.getNbGeometryForBloc(ibloc);
      for (int igeom=nbGeom-1; igeom>=0; igeom--) {
        final int nbPoints=modele_.getNbPointForGeometry(ibloc,igeom);
        boolean bferm=modele_.isGeometryFermeeForBloc(ibloc,igeom);
        boolean breli=modele_.isGeometryRelieeForBloc(ibloc,igeom);

        final GrPoint ptOri=new GrPoint();
        modele_.point(ptOri, ibloc, igeom, nbPoints-1);
        ptOri.autoApplique(versEcran);
        ic.paintIconCentre(this, _g, ptOri.x_, ptOri.y_);
        final GrPoint ptDest=new GrPoint();
        for (int j=nbPoints-2; j>=0; j--) {
          // le point de dest est initialise
          modele_.point(ptDest, ibloc, igeom, j);
          ptDest.autoApplique(versEcran);
          ic.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
          if (breli)
            tlSelection.dessineTrait((Graphics2D)_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
          ptOri.initialiseAvec(ptDest);
        }
        if (bferm) {
          modele_.point(ptOri, ibloc, igeom, nbPoints-1);
          ptOri.autoApplique(versEcran);
          if (breli)
            tlSelection.dessineTrait((Graphics2D)_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        }
      }
    }
  }
}
