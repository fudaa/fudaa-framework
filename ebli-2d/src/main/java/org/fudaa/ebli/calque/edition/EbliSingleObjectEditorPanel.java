/*
 *  @creation     14 avr. 2005
 *  @modification $Date: 2008-04-01 07:28:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequenceUnique;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.fu.FuLog;

/**
 * Un panneau d'�dition d'un objet GIS unique contenu dans un calque editable {@link ZCalqueEditable}. Ce panneau permet l'edition des coordonn�es de
 * l'objet ainsi que de ses attributs. Ce panneau est associ� � un mod�le �ditable de calque. Un gestionnaire de commandes peut �tre ajout� au
 * panneau.
 *
 * @author Fred Deniger
 * @version $Id: EbliSingleObjectEditorPanel.java,v 1.10.6.2 2008-04-01 07:28:15 bmarchan Exp $
 */
public class EbliSingleObjectEditorPanel extends CtuluDialogPanel {

  protected boolean isGeomModifiable() {
    return this.modeleSrc_.getGeomData().isGeomModifiable();
  }
  
  /**
   * Une classe de gestion des attributs globaux
   * @author Bertrand Marchand (marchand@detacad.fr)
   *
   */
  public class GlobalAttributsPanel extends BuPanel {
    // Tableau des attributs globaux
    private GISAttributeModel[] globalAttr_ = new GISAttributeModel[0];
    // Tableau des editeurs d'attributs, cr�e en utilisant globalAttr_
    private CtuluValueEditorI[] globalAttrEditor_ = new CtuluValueEditorI[0];
    // tableau des composants graphique des attributs globaux,
    // cr�e en utilisant globalAttrEditor_ Coordonn�es
    private JComponent[] globalComp_ = new JComponent[0];
    

    /**
     * @return Le panneau des attributs globaux.
     */
    public  GlobalAttributsPanel() {
      BuPanel pnAttrGlobaux=this;

      final List<GISAttributeModel> globalAttributs=new ArrayList<GISAttributeModel>();
      
      // On ne conserve que les attributs globaux �ditables et visibles, les autres sont ignor�s
      for (int i=0; i < zone_.getNbAttributes(); i++) {
        final GISAttributeModel mdlAtt=zone_.getDataModel(i);
        if (mdlAtt.getAttribute().isUserVisible() && mdlAtt.getAttribute().isEditable() && ((zone_.getGeometry(idx_) instanceof GISPoint) || !mdlAtt.getAttribute().isAtomicValue())) {
          globalAttributs.add(mdlAtt);
        }
      }
      
      // Affecte les attributs globaux selectionn�s � globalAttr_
      globalAttr_=new GISAttributeModel[globalAttributs.size()];
      globalAttributs.toArray(globalAttr_);

      globalAttrEditor_=new CtuluValueEditorI[globalAttr_.length];
      globalComp_=new JComponent[globalAttr_.length];

      // Construction du panneau \\
      // Zone de t�te contenant les attributs globaux
      pnAttrGlobaux.setLayout(new BuGridLayout(2, 5, 5));
      for (int i=0; i < globalAttr_.length; i++) {
        // Cr�ation de l'�diteur pour la variable globale
        final CtuluValueEditorI ei=globalAttr_[i].getAttribute().getEditor();
        // Enregistrement de cette �diteur et de son composant graphique
        globalAttrEditor_[i]=ei;
        
        globalComp_[i]=ei.createEditorComponent();
        // Pour que la gis soit inform�e d'un changement d'attribut
//        globalComp_[i].addKeyListener(new KeyAdapter() {
//          @Override
//          public void keyTyped(KeyEvent e) {
//          }
//        });
        
        // Initialisation du composant graphique avec la valeur de l'attribut
        ei.setValue(globalAttr_[i].getObjectValueAt(idx_), globalComp_[i]);
        // Ajout du nom/unit� et du champs �ditable dans le panel
        addLabel(pnAttrGlobaux, globalAttr_[i].getAttribute().getNameWithUnit() + ':');
        pnAttrGlobaux.add(globalComp_[i]);
      }
    }
    
    /**
     * Enregistre les modifications effectu�es.
     */
    public void apply(CtuluCommandContainer cmd, boolean _modification) {
      // Application des modifications des attributs globaux
      for (int i = globalAttr_.length - 1; i >= 0; i--) {
        if (!globalAttrEditor_[i].isEditable()) {
          continue;
        }
        final Object val = globalAttrEditor_[i].getValue(globalComp_[i]);
        // Si une modificatin a �t� faite, modification est mis � true, sauf si
        // cette modification est faite sur ETAT_GEOM
        if (!globalAttr_[i].getObjectValueAt(idx_).equals(val) && globalAttr_[i].getAttribute() != GISAttributeConstants.ETAT_GEOM) {
          _modification = true;
          globalAttr_[i].setObject(idx_, val, cmd);
        }
      }

      // Si une modification a �t� faite, l'attribut 'Etat' de la g�om�trie passe � modifi�
      if (_modification) {
        boolean found = false;
        int i = -1;
        while (!found && ++i < globalAttr_.length) {
          found = globalAttr_[i].getAttribute() == GISAttributeConstants.ETAT_GEOM;
        }
        if (found) {
          globalAttr_[i].setObject(idx_, GISAttributeConstants.ATT_VAL_ETAT_MODI, cmd);
        }
      }
    }
  }
  

  /**
   * Cette classe repr�sente les informations sous forme d'un tableau de donn�es. Les donn�es affich�es dans ce tableau peuvent �tre �dit�es.
   *
   * @author MARTIN Emmanuel
   * @version $Id: EbliSingleObjectEditorPanel.java,v 1.10.6.2 2008-04-01 07:28:15 emartin Exp $
   */
  public class TableCoordinatesPanel extends BuPanel {

    /**
     * Repr�sentation graphique dans le cas o� on a un point unique � repr�senter.
     */
    private JComponent tfX_, tfY_;
    /**
     * Repr�sentation graphique dans le cas o� on plusieurs points � repr�senter.
     */
    private CtuluTable table_;
    /**
     * Vrai si l'objet contient des sommets (ce n'est pas le cas d'un point).
     */
    private boolean isTableStat_;
    /**
     * Les d�finitions des coordonn�es � afficher.
     */
    private final EbliCoordinateDefinition[] coordDefs_;
    /**
     * Le checkBox indiquant le mode d'application des changements.
     */
    protected BuCheckBox modeChangement_;

    /**
     * @param _editAttribut indique si les attributs globaux sont �ditable (dans le cas o� ils ne le sont pas, ils ne sont pas affich�s)
     * @param _editVertexAttribut indique si les attributs des points sont �ditable (ils seront dans tout les cas affich�s)
     * @param _xyFormatter formateur g�rant le format des informations � afficher
     */
    public TableCoordinatesPanel(final boolean _editAttributGlobaux, final boolean _editVertexAttribut,
            final EbliCoordinateDefinition[] _defs) {
      coordDefs_ = _defs;
      // Zone centrale contenant soit le tableau de coordonn�es en cas de points
      // multiple, soit les informations d'un point unique.
      if ((zone_.getGeometry(idx_) instanceof GISPoint)) {
        isTableStat_ = false;
        setLayout(new BuGridLayout(2, 5, 5));
        final Coordinate coord = zone_.getGeometry(idx_).getCoordinate();
        // Cas o� les informations sont �ditables => cr�ation de zones de saisie
        if (zone_.isGeomModifiable()) {
          tfX_ = coordDefs_[0].getFormatter().createEditorComponent();
          tfY_ = coordDefs_[1].getFormatter().createEditorComponent();
          if (FuLog.isTrace()) {
            FuLog.trace("FTR: set value for x,y");
          }
          coordDefs_[0].getFormatter().setValue(CtuluLib.getDouble(coord.x), tfX_);
          coordDefs_[1].getFormatter().setValue(CtuluLib.getDouble(coord.y), tfY_);
        } // Cas o� les informations ne sont pas �ditables => cr�ation de simple
        // label
        else {
          final BuLabel x = new BuLabel();
          x.setText(coordDefs_[0].getFormatter().getXYFormatter().format(coord.x));
          final BuLabel y = new BuLabel();
          y.setText(coordDefs_[1].getFormatter().getXYFormatter().format(coord.y));
          tfX_ = x;
          tfY_ = y;
        }
        // Ajout des �l�ments � l'interface
        addLabel(this, coordDefs_[0].getName() + ":");
        add(tfX_);
        addLabel(this, coordDefs_[1].getName() + ":");
        add(tfY_);
      } // Cas o� on a affaire � un ensemble de point. Un tableau permettra
      // d'afficher les informations
      else {
        isTableStat_ = true;
        setLayout(new BuBorderLayout());
        // Construction de la table
        table_ = new CtuluTable();
        // Pour permettre le copy/paste facilement surle tableau.
        table_.setCellSelectionEnabled(true);
        table_.setAutoExtendRowsWhenPaste(true);
        table_.setModel(tbCoordinatesModel_);
        tbCoordinatesModel_.updateEditorAndRenderer(table_);
        // Gestion des erreurs
        setErrorText(tbCoordinatesModel_.getErrorMessage());
        tbCoordinatesModel_.addTableModelModeleAdapterErrorListener(new EbliSingleObjectTableModelErrorListener() {
          @Override
          public void modeleAdapterError(String _message) {
            setErrorText(_message);
          }

          @Override
          public void modeleAdpaterNoError() {
            setErrorText("");
          }
        });
        // Container des �l�ments en dessous du tableau
        JPanel pnStructure = new JPanel();
        pnStructure.setLayout(new BorderLayout());
        pnStructure.setBorder(BorderFactory.createTitledBorder(EbliLib.getS("Modification de la structure")));

        // Construction des boutons de d�placement des points
        JPanel pnOrder = new JPanel();
        pnOrder.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 0));
        final BuButton btUp_ = new BuButton(BuResource.BU.getIcon("monter"));
        btUp_.setToolTipText(EbliLib.getS("D�cr�menter l'index"));
        btUp_.setEnabled(false);

        final BuButton btDown_ = new BuButton(BuResource.BU.getIcon("descendre"));
        btDown_.setToolTipText(EbliLib.getS("Incr�menter l'index"));
        btDown_.setEnabled(false);

        btUp_.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            setErrorText("");
            int[] newSelection = tbCoordinatesModel_.movePoints(table_.getSelectedRows(), -1);
            // R�tablissement de la selection dans le tableau
            table_.getSelectionModel().clearSelection();
            for (int i = 0; i < newSelection.length; i++) {
              table_.getSelectionModel().addSelectionInterval(newSelection[i], newSelection[i]);
            }
          }
        });
        btDown_.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            setErrorText("");
            int[] newSelection = tbCoordinatesModel_.movePoints(table_.getSelectedRows(), 1);
            // R�tablissement de la selection dans le tableau
            table_.getSelectionModel().clearSelection();
            for (int i = 0; i < newSelection.length; i++) {
              table_.getSelectionModel().addSelectionInterval(newSelection[i], newSelection[i]);
            }
          }
        });
        pnOrder.add(new BuLabel(EbliLib.getS("Changer l'ordre des sommets")));
        pnOrder.add(btUp_);
        pnOrder.add(btDown_);

        // Boutons d'ajout/supp
        JPanel pnAdd = new JPanel();
        pnAdd.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 0));
        final BuButton btAdd_ = new BuButton(BuResource.BU.getIcon("ajouter"), EbliLib.getS("Ajouter"));
        btAdd_.setToolTipText(EbliLib.getS("Ajouter un sommet avant la s�lection (ou en dernier si aucune s�lection)"));
        btAdd_.setEnabled(isGeomModifiable());

        final BuButton btDel_ = new BuButton(BuResource.BU.getIcon("enlever"), EbliLib.getS("Supprimer"));
        btDel_.setToolTipText(EbliLib.getS("Supprimer un sommet"));
        btDel_.setEnabled(false);

        btAdd_.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            setErrorText("");
            int idxSel = tbCoordinatesModel_.addPoint(table_.getSelectedRow());
            table_.getSelectionModel().setSelectionInterval(idxSel, idxSel);
          }
        });
        btDel_.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            setErrorText("");
            tbCoordinatesModel_.removePoints(table_.getSelectedRows());
          }
        });
        pnAdd.add(btAdd_);
        pnAdd.add(btDel_);

        table_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
          @Override
          public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting()) {
              return;
            }
            final boolean geomModifiable = isGeomModifiable();

            btUp_.setEnabled(geomModifiable && table_.getSelectedRowCount() > 0);
            btDown_.setEnabled(geomModifiable && table_.getSelectedRowCount() > 0);
            btAdd_.setEnabled(geomModifiable && table_.getSelectedRowCount() <= 1);
            btDel_.setEnabled(
                    geomModifiable && table_.getSelectedRowCount() > 0 && table_.getSelectedRowCount() < table_.getRowCount());
          }
        });

        // CheckBox permettant de changer le mode d'application des changements
        modeChangement_ = new BuCheckBox(EbliLib.getS("Mode diff�r�"));
        modeChangement_.setEnabled(isGeomModifiable());
        modeChangement_.setToolTipText(
                EbliLib.getS(
                "<html>En mode diff�r� : les modifications ne sont appliqu�es qu'� la fin.<br>En mode imm�diat : les modifications sont appliqu�es imm�diatement.</html>"));
        modeChangement_.addChangeListener(new ChangeListener() {
          @Override
          public void stateChanged(ChangeEvent e) {
            if (modeChangement_.isSelected()) {
              tbCoordinatesModel_.setDeferredModifications(true);
            } else {
              tbCoordinatesModel_.setDeferredModifications(false);
            }
          }
        });

        pnStructure.add(pnAdd, BuBorderLayout.WEST);
        pnStructure.add(pnOrder, BuBorderLayout.CENTER);
        pnStructure.add(modeChangement_, BuBorderLayout.EAST);

        // Ajout des �l�ments � l'interface
        add(pnStructure, BuBorderLayout.SOUTH);
        add(new BuScrollPane(table_), BuBorderLayout.CENTER);
      }
    }

    /**
     * @return la s�quence des coordonn�es modifi�es.
     */
    protected CoordinateSequence getNewCoordinate() {
      // Cas o� on a repr�sent� un point unique
      if (zone_.isGeomModifiable()) {
        Double newX = null;
        Double newY = null;
        if (!coordDefs_[0].getFormatter().isEmpty(tfX_)) {
          newX = (Double) coordDefs_[0].getFormatter().getValue(tfX_);
        }
        if (!coordDefs_[1].getFormatter().isEmpty(tfY_)) {
          newY = (Double) coordDefs_[1].getFormatter().getValue(tfY_);
        }
        if (newX != null && newY != null) {
          return new GISCoordinateSequenceUnique(newX.doubleValue(), newY.doubleValue(), 5.);
        }
      }
      return null;
    }

    /**
     * Enregistre les modifications effectu�es.
     */
    public boolean apply(CtuluCommandContainer cmd) {
      boolean modification = false; // true si une modification a �t� faite

      // Cas du tableau modifi�
      if (isTableStat_) {
        CtuluCommandContainer c = tbCoordinatesModel_.getUndoRedoContainer();
        tbCoordinatesModel_.setUndoRedoContainer(cmd);
        tbCoordinatesModel_.flushData();
        tbCoordinatesModel_.setUndoRedoContainer(c);
      } // Cas du point unique modifi�
      else if (zone_.isGeomModifiable() && !zone_.getCoordinateSequence(idx_).equals(getNewCoordinate())) {
        modification = true;
        zone_.setCoordinateSequence(idx_, getNewCoordinate(), cmd);
      }
      
      return modification;
    }

    /**
     * Stop l'�dition si le mode tableau est actif
     */
    public void stopCellEditing() {
      if (isTableStat_ && table_.isEditing()) {
        table_.getCellEditor().stopCellEditing();
      }
    }
  }
  private CtuluCommandContainer cmd_;
  // Num�ro de la g�om�rie dans zone_ que l'on veut traiter
  private final int idx_;
  private ZModeleEditable modeleSrc_;
  // Objet contenant toutes les g�nom�ries (seule celle � idx_ nous interesse)
  // et divers info
  private final GISZoneCollection zone_;
  // Onglet repr�sentant les informations sous forme de table
  private TableCoordinatesPanel pnTableCoordinates_;
  private GlobalAttributsPanel pnAttributes_;
  /**
   * Le modele de donn�es � utiliser comme model de tabelau
   */
  protected EbliSingleObjectTableModel tbCoordinatesModel_;
  
  private JTabbedPane tabbedPane_;// Onglets
  ZEditorValidatorI editValidator_;

  /**
   * @param _idx
   * @param _editAttribut
   */
  public EbliSingleObjectEditorPanel(final ZModeleEditable _model, final int _idx, final boolean _editAttribut,
          final boolean _editVertexAttribut, final EbliCoordinateDefinition[] _defs, ZEditorValidatorI _validator) {
    
    idx_ = _idx;
    zone_ = _model.getGeomData();
    modeleSrc_ = _model;
    editValidator_ = _validator;

    setLayout(new BuBorderLayout(0,10));
    
    // Les attrbuts globaux
    pnAttributes_= new GlobalAttributsPanel();
    if (_editAttribut)
      add(pnAttributes_, BuBorderLayout.NORTH);

    // Contruction du model de donn�es commun aux deux repr�sentations
    tbCoordinatesModel_ = new EbliSingleObjectTableModel(modeleSrc_, idx_, _defs, new CtuluCommandComposite(), _editVertexAttribut);
    pnTableCoordinates_ = new TableCoordinatesPanel(_editAttribut, _editVertexAttribut, _defs);
    add(pnTableCoordinates_, BuBorderLayout.CENTER);
  }
  
  public void setDeferredModifications(boolean _active) {
    tbCoordinatesModel_.setDeferredModifications(_active);
    if (pnTableCoordinates_.modeChangement_ != null) {
      this.pnTableCoordinates_.modeChangement_.setSelected(_active);
    }
  }

  public EbliSingleObjectTableModel getTableModel() {
    return tbCoordinatesModel_;
  }

  /**
   * Ajoute un nouveau panneau en plus du panneau tableau. Des onglets sont apparents dans ce cas.
   *
   * @param _title Le titre
   * @param _pn Le panneau.
   */
  public void addNewTab(String _title, BuPanel _pn) {
    if (tabbedPane_ == null) {
      tabbedPane_ = new JTabbedPane();

      remove(pnTableCoordinates_);
      tabbedPane_.add(EbliLib.getS("Table"), pnTableCoordinates_);
      add(tabbedPane_, BuBorderLayout.CENTER);
    }
    tabbedPane_.add(_title, _pn);
  }

  /**
   * Selectionne l'onglet num�ro 'index'.
   *
   * @param index : index de l'onglet � selecionner.
   */
  public void setSelectedTab(int index) {
    if (tabbedPane_ != null) {
      tabbedPane_.setSelectedIndex(index);
    }
  }

  /**
   * Retourne l'index de l'onglet selectionnn�.
   */
  public int getSelectedTab() {
    if (tabbedPane_ != null) {
      return tabbedPane_.getSelectedIndex();
    }
    return 0;
  }

  /**
   * Retourne vrai si l'edition contient des onglets. Faux sinon.
   */
  public boolean hasTab() {
    return tabbedPane_ != null;
  }

  /**
   * Enregistre les modifications effectu�es. Cette m�thode est automatiquement appell�e lors du click sur le bouton 'Valider'
   * @return 
   */
  @Override
  public boolean apply() {
    if (editValidator_!= null && !editValidator_.isSingleEditionValid(modeleSrc_))
      return false;
    
    modeleSrc_.modificationWillBeDone();
    CtuluCommandComposite cmd = new CtuluCommandComposite("Modifications de la g�om�trie");
    boolean modified = false;
    if (pnTableCoordinates_ != null) {
      modified = pnTableCoordinates_.apply(cmd);
    }
    if (pnAttributes_ != null) {
      pnAttributes_.apply(cmd, modified);
    }
    ((CtuluCommandComposite)tbCoordinatesModel_.getUndoRedoContainer()).addCmd(cmd);
//    cmd.addCmd(((CtuluCommandComposite) tbCoordinatesModel_.getUndoRedoContainer()).getSimplify());
    if (getCmd() != null) {
      getCmd().addCmd(((CtuluCommandComposite)tbCoordinatesModel_.getUndoRedoContainer()).getSimplify());
    }
    
    modeleSrc_.modificationDone();
    return true;
  }

  @Override
  public boolean cancel() {
    // D�active le model
    if (tbCoordinatesModel_ != null) {
      // Desactive le listener
      tbCoordinatesModel_.setSource(null, -1);
      ((CtuluCommandComposite) tbCoordinatesModel_.getUndoRedoContainer()).undo();
    }
    return super.cancel();
  }
  
  @Override
  public boolean ok() {
    if (!apply())
      return false;
    
    // Desactive le listener
    tbCoordinatesModel_.setSource(null, -1);
    return true;
  }

  public final CtuluCommandContainer getCmd() {
    return cmd_;
  }

  public final void setCmd(final CtuluCommandContainer _cmd) {
    cmd_ = _cmd;
  }

  /**
   * Appel� lors du click sur le bouton 'valider' pour sortir de la fen�tre. Retourne vrai si toutes les modifications sont valides.
   */
  @Override
  public boolean isDataValid() {
    if (pnTableCoordinates_ != null) {
      pnTableCoordinates_.stopCellEditing();
      String error = tbCoordinatesModel_.getErrorMessage();
      if (error != null) {
        setErrorText(error);
        return false;
      }
    }
    return true;
  }
}
