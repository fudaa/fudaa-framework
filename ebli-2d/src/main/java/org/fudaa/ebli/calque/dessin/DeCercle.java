/*
 * @file         DeCercle.java
 * @creation     1998-08-31
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * Un cercle.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:53 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class DeCercle extends DeEllipse {
  // donnees membres publiques
  // donnees membres privees
  // Constructeur
  public DeCercle() {
    super();
  }

  public DeCercle(final GrPoint[] _pc) {
    super(_pc);
  }

  @Override
  public int getForme() {
    return CERCLE;
  }

  public DeCercle(final GrPoint _coin, final GrVecteur _cote1, final GrVecteur _cote2) {
    super();
    GrVecteur cote1 = _cote1;
    GrVecteur cote2 = _cote2;

    final double norme1 = cote1.norme();
    final double norme2 = cote2.norme();
    if (norme1 < norme2) {
      cote2 = cote2.normalise().multiplication(norme1);
    } else {
      cote1 = cote1.normalise().multiplication(norme2);
    }
    ajoute(_coin);
    ajoute(_coin.addition(cote1));
    ajoute(_coin.addition(cote1).addition(cote2));
    ajoute(_coin.addition(cote2));
  }
}
