/*
 * @creation     2000-11-10
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrContour;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
/**
 * Le modele du calque d'affichage de point.
 *
 * @version      $Id$
 * @author       Guillaume Desnoix
 */
public class ZModeleStatiquePoint extends ZModeleDonnesAbstract implements ZModelePoint {

  private final GrPoint[] points_;
  double xmin_;
  double ymin_;
  double zmin_;
  double xmax_;
  double ymax_;
  double zmax_;
  private final int nombre_;

  public ZModeleStatiquePoint(final GrPoint[] _points) {
    points_ = _points;
    nombre_ = _points.length;
    calculBoite();
  }

  @Override
  public BuTable createValuesTable(final ZCalqueAffichageDonneesInterface _layer){
    return null;
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int vertex) {
    GrPoint p = new GrPoint();
    point(p, _ind, true);
    return p;
  }

  @Override
  public double getX(final int _i){
    return points_[_i].x_;
  }

  @Override
  public double getY(final int _i){
    return points_[_i].y_;
  }

  @Override
  public void fillWithInfo(final InfoData _d,final ZCalqueAffichageDonneesInterface _layer){}

  @Override
  public boolean isValuesTableAvailable(){
    return false;
  }

  /**
   * Des modeles ne peuvent afficher qu'une partie des donnees.
   */
  public boolean visible(final int _i){
    return true;
  }

  /**
   *  Renvoie <code>true</code>, si le modele affiche toutes les donnees.
   * Utiliser pour optimiser ?? l'affichage
   */
  public boolean entierementVisible(){
    return true;
  }

  @Override
  public final int getNombre(){
    return nombre_;
  }

  public final int nombreContours(){
    return nombre_;
  }

  @Override
  public final GrBoite getDomaine(){
    final GrBoite r = new GrBoite();
    getDomaine(r);
    return r;
  }

  public final void getDomaine(final GrBoite _b){
    if (nombre_ <= 0) {
      if (_b.o_ == null) {
        _b.o_ = new GrPoint(0, 0, 0);
      } else {
        _b.o_.setCoordonnees(0, 0, 0);
      }
      if (_b.e_ == null) {
        _b.e_ = new GrPoint(0, 0, 0);
      } else {
        _b.e_.setCoordonnees(0, 0, 0);
      }
    }
    if (_b.o_ == null) {
      _b.o_ = new GrPoint(xmin_, ymin_, zmin_);
    } else {
      _b.o_.setCoordonnees(xmin_, ymin_, zmin_);
    }
    if (_b.e_ == null) {
      _b.e_ = new GrPoint(xmax_, ymax_, zmax_);
    } else {
      _b.e_.setCoordonnees(xmax_, ymax_, zmax_);
    }
  }

  private void calculBoite(){
    final GrBoite temp = new GrBoite();
    for (int i = nombre_ - 1; i >= 0; i--) {
      temp.ajuste(points_[i]);
    }
    xmin_ = temp.o_.x_;
    ymin_ = temp.o_.y_;
    zmin_ = temp.o_.z_;
    xmax_ = temp.e_.x_;
    ymax_ = temp.e_.y_;
    zmax_ = temp.e_.z_;
  }

  @Override
  public final boolean point(final GrPoint _p,final int _i,final boolean _force){
    if ((_i < 0) || (_i >= points_.length)) {
      return false;
    }
    final GrPoint p = points_[_i];
    if (p == null) {
      return false;
    }
    _p.x_ = p.x_;
    _p.y_ = p.y_;
    _p.z_ = p.z_;
    return true;
  }

  public final GrPoint point(final int _i){
    final GrPoint pt = new GrPoint();
    if (point(pt, _i, true)) {
      return pt;
    }
    return null;
  }

  @Override
  public final Object getObject(final int _i){
    return point(_i);
  }

  public final GrContour contour(final int _i){
    return point(_i);
  }
  //  public final boolean contourSelectionnable(int _i)  { return true;  }

}
