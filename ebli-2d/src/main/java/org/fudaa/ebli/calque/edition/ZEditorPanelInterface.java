/*
 *  @creation     4 avr. 2005
 *  @modification $Date: 2008-03-26 16:39:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import javax.swing.JComponent;

/**
 * Une interface pour un panneau parametres d'outil de la palette d'�dition GIS. Ce panneau est sp�cifique de l'outil
 * et affich� lors de sa s�lection.
 * 
 * @author Fred Deniger
 * @version $Id: ZEditorPanelInterface.java,v 1.3.8.3 2008-03-26 16:39:51 bmarchan Exp $
 */
public interface ZEditorPanelInterface {

  /**
   * Appele a chaque modification de la forme en cours (point ajout�, etc).
   */
  void atomicChanged();

  /**
   * @return le composant a ajouter en bas de la palette d'edition (en general this :-)
   */
  JComponent getComponent();

  /**
   * Permet de pr�venir que le point en cours de construction � changer.
   * @param _x
   * @param _y
   */
  void pointMove(double _x, double _y);
  
  /**
   * Appele des qu'un objet est ajoute.
   */
  void objectAdded();

  void calqueInteractionChanged();

  /**
   * Appel� lorsqu'un changement de calque cible est affectu�.
   * @param _target Le calque editable cible. 
   */
  void targetChanged(ZCalqueEditable _target);

  void editionStopped();

  ZEditionAttibutesContainer getAttributeContainer();

  /**
   * Appele lorsque ce panneau est reellement ferme.
   */
  void close();
}