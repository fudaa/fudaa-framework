/**
 * @creation 2002-09-23
 * @modification $Date: 2008-02-20 10:16:02 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import java.awt.*;

/**
 * Un calque d'affichage de polyligne.
 *
 * @version $Id: ZCalquePolygone.java,v 1.37.6.1 2008-02-20 10:16:02 bmarchan Exp $
 * @author Fred Deniger
 */
public class ZCalquePolygone extends ZCalqueAffichageDonneesLineAbstract {

  /**
   * Propriete modele.
   */
  protected ZModelePolygone modele_;

  /**
   *    */
  public ZCalquePolygone() {
    super();
    ligneModel_ = new TraceLigneModel();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    int m = selection_.getMaxIndex();
    if (m > modele_.getNombre()) {
      m = modele_.getNombre() - 1;
    }
    final GrBoite r = new GrBoite();
    final GrBoite temp = new GrBoite();
    final GrPolygone poly = new GrPolygone();
    for (int i = selection_.getMinIndex(); i <= m; i++) {
      if (selection_.isSelected(i)) {
        modele_.polygone(poly, i, true);
        poly.boite(temp);
        r.ajuste(temp);
      }
    }
    //    ajusteZoomOnSelected(r);
    return r;
  }

  public ZCalquePolygone(final ZModelePolygone _modele) {
    this();
    modele_ = _modele;
  }

  /**
   * @param _modele Modele
   */
  public void setModele(final ZModelePolygone _modele) {
    if (modele_ != _modele) {
      final ZModelePolygone vp = modele_;
      modele_ = _modele;
      firePropertyChange("modele", vp, modele_);
    }
  }

  /**
   * @return Modele
   */
  public ZModelePolygone modele() {
    return modele_;
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return modele();
  }
  protected GrPolygone poly_ = new GrPolygone();

  /**
   * @param _ic le modele de l'icone a modifier: peut etre null
   * @param _ligne le modele de trace de ligne a modifier: non null
   * @param _idx l'indice du polygone
   */
  protected void updateTrace(final TraceIconModel _ic, final TraceLigneModel _ligne, final int _idx) {
  }

  /**
   * Ne dessine que les donn�es.
   *
   * @param _g le graphics cible
   */
  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final boolean rapide = isRapide();
    // BPaletteCouleur paletteCouleur = getPaletteCouleur();
    // BPaletteIcone paletteIcone = getPaletteIcone();
    final int nombre = modele_.getNombre();
    // pour ne pas modifier le model par defaut
    final TraceIcon icone = iconModel_ == null ? null : new TraceIcon(new TraceIconModel(iconModel_));
    final GrBoite bPoly = new GrBoite();
    int incrementPt = 1;
    final TraceLigne tl = new TraceLigne(new TraceLigneModel(ligneModel_));
    if (isAttenue()) {
      tl.setCouleur(EbliLib.getAlphaColor(attenueCouleur(tl.getCouleur()), alpha_));
      if (icone != null) {
        icone.setCouleur(EbliLib.getAlphaColor(attenueCouleur(icone.getCouleur()), alpha_));
      }
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      tl.setCouleur(EbliLib.getAlphaColor(tl.getCouleur(), alpha_));
      if (icone != null) {
        icone.setCouleur(EbliLib.getAlphaColor(icone.getCouleur(), alpha_));
      }
    }
    for (int i = 0; i < nombre; i++) {
      if (!modele_.isPainted(i)) {
        continue;
      }
      // si le polygone i est filtre on passe.
      modele_.polygone(poly_, i, false);
      poly_.boite(bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (!bPoly.intersectXY(clip)) {
        continue;
      }
      final int nbPoints = poly_.sommets_.nombre();
      if (nbPoints <= 0) {
        continue;
      }
      if (rapide && (nbPoints > 15)) {
        incrementPt = 3;
      } else {
        incrementPt = 1;
      }
      // QUESTION: a rajouter
      /*
       * double z = 0.; if(domaine.e.z > domaine.o.z) z = (p.z - domaine.o.z) / (domaine.e.z - domaine.o.z);
       */
      poly_.autoApplique(versEcran);
      updateTrace(icone == null ? null : icone.getModel(), tl.getModel(), i);
      paintPoly(poly_, incrementPt, tl, icone, _g, this);
    }
  }

  /**
   * @param _p le polygone a tracer
   * @param _inc l'increment pour les points du polygone
   * @param _tl le trace ligne a utiliser
   * @param _ic le trace icone a utiliser
   * @param _g le graphics
   * @param _parent le composant parent pour dessiner les icones
   */
  public static void paintPoly(final GrPolygone _p, final int _inc, final TraceLigne _tl, final TraceIcon _ic, final Graphics _g,
          final Component _parent) {

    GrPoint ptOri = _p.sommets_.renvoie(0);
    GrPoint ptDest = null;
    for (int j = _p.nombre() - 1; j >= 0; j -= _inc) {
      // le point de dest est initialise
      ptDest = _p.sommets_.renvoie(j);
      if (_ic != null) {
        _ic.paintIconCentre(_parent, _g, ptDest.x_, ptDest.y_);
      }
      _tl.dessineTrait((Graphics2D) _g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
      ptOri = ptDest;
    }
  }

  /**
   * Ne dessine que la selection.
   *
   * @param _g le graphics cible
   */
  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran, final GrBoite _clipReel) {
    if (isSelectionEmpty()) {
      return;
    }
    final GrBoite clip = _clipReel;
    final GrBoite domaine = modele_.getDomaine();
    if (!domaine.intersectXY(clip)) {
      return;
    }
    final GrMorphisme versEcran = _versEcran;
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    _g.setColor(cs);
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    _g.setColor(cs);
    final int nb = selection_.getMaxIndex();
    final int min = selection_.getMinIndex();
    for (int i = nb; i >= min; i--) {
      if (!selection_.isSelected(i)) {
        continue;
      }
      modele_.polygone(poly_, i, true);
      if (clip.intersectXY(poly_.boite())) {
        poly_.autoApplique(versEcran);
        paintPoly(poly_, 1, tlSelection, ic, _g, this);
      }
    }
  }

  @Override
  public CtuluListSelection selection(final LinearRing _polySelection, final int _mode) {
    if (!isVisible()) {
      return null;
    }
    return selection(_polySelection, _mode, modele());
  }

  public static CtuluListSelection selection(final LinearRing _polySelection, final int _mode, final ZModelePolygone _modele) {
    if (_modele == null || _modele.getNombre() == 0) {
      return null;
    }
    final GrBoite domaineBoite = _modele.getDomaine();
    if (domaineBoite == null) {
      return null;
    }
    final Envelope polyEnv = _polySelection.getEnvelopeInternal();
    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_, domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final CtuluListSelection r = new CtuluListSelection(_modele.getNombre());
    final Coordinate c = new Coordinate();
    final GrPolygone poly = new GrPolygone();
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_polySelection);
    for (int i = _modele.getNombre() - 1; i >= 0; i--) {
      _modele.polygone(poly, i, true);
      if ((VecteurGrPoint.estSelectionneEnv(c, polyEnv, _polySelection, tester, poly.sommets_, _mode))) {
        r.add(i);
      }
    }
    return r.isEmpty() ? null : r;
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (!isVisible()) {
      return null;
    }
    GrBoite bClip = getDomaine();
    if (bClip == null) {
      return null;
    }
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }
    bClip = getClipReel(getGraphics());
    final GrPolygone poly = new GrPolygone();
    int r = -1;
    boolean found = false;
    for (int i = modele_.getNombre() - 1; i >= 0 && !found; i--) {
      modele_.polygone(poly, i, true);
      if (bClip.intersectXY(poly.boite())) {
        if (poly.contientXY(_pt)) {
          r = i;
          found = true;
        } else if (poly.distanceXY(_pt) <= distanceReel) {
          r = i;
        }
      }
    }
    if (r >= 0) {
      final CtuluListSelection sel = new CtuluListSelection(1);
      sel.setSelectionInterval(r, r);
      return sel;
    }
    return null;
  }

  @Override
  public LineString getSelectedLine() {
    return getSelectedLine(this, modele_);
  }

  public static LineString getSelectedLine(final ZCalqueAffichageDonnees _cq, final ZModelePolygone _modele) {
    if (_cq.getNbSelected() != 2) {
      return null;
    }
    return getSelectedLine(_cq.getLayerSelection().getMinIndex(), _cq.getLayerSelection().getMaxIndex(), _modele);
  }

  public static LineString getSelectedLine(int first, int last, final ZModelePolygone _modele) {
    final Coordinate[] cs = new Coordinate[2];
    final GrPolygone poly = new GrPolygone();
    _modele.polygone(poly, first, true);
    cs[0] = poly.sommets_.barycentre();
    _modele.polygone(poly, last, true);
    cs[1] = poly.sommets_.barycentre();
    if (cs[0].compareTo(cs[1]) > 0) {
      final Coordinate tmp = cs[0];
      cs[0] = cs[1];
      cs[1] = tmp;

    }
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }
}
