/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2008-02-01 14:37:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.fudaa.ctulu.gis.GISAttributeInterface;

/**
 * Une interface a implementer dans les panneaux d'IHM permettant � l'utilisateur de sp�cifier une valeur pour un attribut
 * GIS.
 * 
 * @author Fred Deniger
 * @version $Id: ZEditionAttributesCreatorInterface.java,v 1.3.8.1 2008-02-01 14:37:24 bmarchan Exp $
 */
public interface ZEditionAttributesCreatorInterface {

  /**
   * Retourne la valeurs associ�e � un l'attribut GIS sp�cifi�.
   * @param _att L'attribut
   * @return La valeur de l'attribut.
   */
  Object getData(GISAttributeInterface _att);
}
