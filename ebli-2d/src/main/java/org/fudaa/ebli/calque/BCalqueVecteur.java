/*
 * @file         BCalqueVecteur.java
 * @creation     1999-01-29
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Un calque d'affichage de vecteurs localises.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 14:55:47 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BCalqueVecteur extends BCalqueAffichage {
  VecteurGrPoint a_;
  VecteurGrPoint b_;
  int densite_= 1000;
  // Constructeur
  public BCalqueVecteur() {
    super();
    a_= new VecteurGrPoint();
    b_= new VecteurGrPoint();
    typeTrait_= TraceLigne.LISSE;
  }
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    //    int w=getIconWidth();
    final int h= getIconHeight();
    final TraceLigne trace= new TraceLigne();
    trace.setCouleur(fg);
    trace.setTypeTrait(typeTrait_);
    trace.dessineTrait((Graphics2D)_g,_x + 2, _y + h - 2, _x + 7, _y + h - 7);
  }
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    final GrMorphisme versEcran= getVersEcran();
    final TraceLigne trace= new TraceLigne();
    trace.setTypeTrait(typeTrait_);
    trace.setCouleur(fg);
    for (int i= 0; i < a_.nombre(); i += 1000 / densite_) {
      final GrPoint p= a_.renvoie(i).applique(versEcran);
      final GrPoint q= b_.renvoie(i).applique(versEcran);
      trace.dessineTrait((Graphics2D)_g,(int)p.x_, (int)p.y_, (int)q.x_, (int)q.y_);
      _g.fillOval((int)p.x_ - 2, (int)p.y_ - 2, 5, 5);
    }
  }
  /**
   * Reinitialise la liste de vecteurs.
   */
  public void reinitialise() {
    a_= new VecteurGrPoint();
    b_= new VecteurGrPoint();
  }
  /**
   * Ajoute un vecteur a la liste de vecteurs.
   */
  public void ajoute(final GrPoint _point, final GrVecteur _vecteur) {
    a_.ajoute(_point);
    b_.ajoute(_point.addition(_vecteur));
  }
  /**
   * Ajoute un vecteur a la liste de vecteurs.
   */
  public void ajoute(final GrPoint _p, final GrPoint _q) {
    a_.ajoute(_p);
    b_.ajoute(_q);
  }
  /**
   * Retire un point a la liste de points.
   */
  public void enleve(final GrPoint _point) {
    final int i= a_.indice(_point);
    a_.enleve(i);
    b_.enleve(i);
  }
  public VecteurGrPoint getOrigines() {
    return a_;
  }
  public VecteurGrPoint getExtremites() {
    return b_;
  }
  @Override
  public GrBoite getDomaine() {
    GrBoite r= null;
    if (isVisible()) {
      r= super.getDomaine();
      if (a_.nombre() > 0) {
        if (r == null) {
          r= new GrBoite();
        }
        for (int i= 0; i < a_.nombre(); i++) {
          r.ajuste(a_.renvoie(i));
        }
      }
      if (b_.nombre() > 0) {
        if (r == null) {
          r= new GrBoite();
        }
        for (int i= 0; i < b_.nombre(); i++) {
          r.ajuste(b_.renvoie(i));
        }
      }
    }
    return r;
  }
  // Propriete typePoint
  private int typeTrait_;
  /**
   * Accesseur de la propriete <I>typeTrait</I>.
   * Elle definit le style de trace des points, en prenant ses valeurs dans
   * les champs statiques de <I>Tracepoint</I>.
   * Par defaut, elle vaut TraceLigne.CONTINU.
   * @see org.fudaa.ebli.trace.TraceLigne
    */
  public int getTypeTrait() {
    return typeTrait_;
  }
  /**
   * Affectation de la propriete <I>typeTrait</I>.
   */
  public void setTypeTrait(final int _typeTrait) {
    if (typeTrait_ != _typeTrait) {
      final int vp= typeTrait_;
      typeTrait_= _typeTrait;
      firePropertyChange("typeTrait", vp, typeTrait_);
    }
  }
  public void setDensite(final int _densite) {
    if (densite_ != _densite) {
      final int vp= densite_;
      densite_= _densite;
      firePropertyChange("densite", vp, densite_);
      repaint();
    }
  }
  public int getDensite() {
    return densite_;
  }
}
