/*
 * @creation 9 nov. 06
 *
 * @modification $Date: 2007-06-28 09:26:47 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author fred deniger
 * @version $Id: ZCalqueFlecheScaleSection.java,v 1.4 2007-06-28 09:26:47 deniger Exp $
 */
public class ZCalqueFlecheScaleSection extends BCalqueConfigureSectionAbstract {
  public ZCalqueFlecheScaleSection(final ZCalqueFleche _target) {
    super(_target, EbliLib.getS("Norme"));
  }

  FlecheScaleData getScaleFlecheData() {
    return getFleche().scale_;
  }

  private ZCalqueFleche getFleche() {
    return ((ZCalqueFleche) target_);
  }

  SpinnerNumberModel spinnerForRatio;

  public BSelecteurInterface createLegendSelecteur() {
    return createLegendSelecteur(true);
  }

  public BSelecteurInterface createLegendSelecteur(boolean addListenerToTarget) {
    final BSelecteurCheckBox selectUseFixNorm = new BSelecteurCheckBox(
        FlecheScaleData.PROP_LEGEND_USE_CONSTANT_NORM_VALUE, new BuCheckBox(addListenerToTarget ? CtuluLibString.EMPTY_STRING : EbliLib.getS("Utiliser une taille fixe")));
    selectUseFixNorm.setTooltip(EbliLib.getS("S�lectionner pour utiliser une norme fixe pour la l�gende"));
    final BSelecteurTextField selectNorm = new BSelecteurTextField(FlecheScaleData.PROP_LEGEND_CONSTANT_NORM_VALUE,
        BuTextField.createDoubleField());

    selectUseFixNorm.getCb().getModel().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent arg0) {
        selectNorm.setEnabled(selectUseFixNorm.getCb().isSelected());
      }
    });
    selectNorm.setEnabled(selectUseFixNorm.getCb().isSelected());
    selectNorm.setAddListenerToTarget(addListenerToTarget);
    selectUseFixNorm.setAddListenerToTarget(addListenerToTarget);
    final BSelecteurComposite legend = new BSelecteurComposite(new BSelecteurInterface[]{new BSelecteurComposite(
        new BSelecteurInterface[]{selectUseFixNorm, selectNorm}, true)});
    legend.setTitle(EbliLib.getS("L�gende: utiliser une norme fixe"));
    return legend;
  }

  private BSelecteurInterface createRelativeSelecteur(final BSelecteurInterface createFixLegendSelecteur, BuRadioButton bt) {
    final BSelecteurCheckBox selectUseRatioNorm = new BSelecteurCheckBox(FlecheScaleData.PROP_IS_SAME_NORM_USED, bt);
    selectUseRatioNorm.setInverseResult(true);
    selectUseRatioNorm.setTooltip(EbliLib.getS("Utiliser une norme proportionnelle"));

    final double d = getIncForSpinner();
    spinnerForRatio = new SpinnerNumberModel(0D, 0D, 10000D, d);
    final BSelecteurSpinner selectRatioValue = new BSelecteurSpinner(FlecheScaleData.PROP_PIXEL_FOR_RELATIVE,
        new JSpinner(spinnerForRatio));
    spinnerForRatio.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(final ChangeEvent _e) {
        selectRatioValue.getSpinner().setToolTipText(
            "<html><body>" + EbliLib.getS("Taille des vecteurs") + "<br>"
                + EbliLib.getS("{0} pixels repr�sente 1 unit�", spinnerForRatio.getValue().toString()));
      }
    });
    final BSelecteurButton selectBtDefault = new BSelecteurButton(FlecheScaleData.PROP_PIXEL_FOR_RELATIVE_DEFAULT,
        new BuToolButton(BuResource.BU.getIcon("rafraichir")));
    selectBtDefault.setTooltip(EbliLib.getS("Initialiser avec la valeur par d�faut"));
    selectBtDefault.setUpdateWithMinValue(true);
    selectBtDefault.setAddListenerToTarget(false);
    selectUseRatioNorm.getCb().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        final boolean selected = selectUseRatioNorm.getCb().isSelected();
        selectRatioValue.setEnabled(selected);
        selectBtDefault.setEnabled(selected);
        createFixLegendSelecteur.setEnabled(selected);
      }
    });
    final boolean selected = !getScaleFlecheData().isSameNorm();
    selectRatioValue.setEnabled(selected);
    selectBtDefault.setEnabled(selected);
    final BSelecteurComposite ratio = new BSelecteurComposite(new BSelecteurInterface[]{
        new BSelecteurComposite(new BSelecteurInterface[]{selectUseRatioNorm, selectRatioValue}, true),
        selectBtDefault});
    ratio.setTitle(EbliLib.getS("Relative"));
    return ratio;
  }

  private double getIncForSpinner() {
    double d = getFleche().getScaleData().getDefaultFacteur() / 5D;
    d = ((int) (d * 100)) / 100D;
    if (d < 0.01) {
      d = 0.01;
    } else if (d > 1) {
      d = 1;
    }
    return d;
  }

  private BSelecteurInterface createFixedSelecteur(final AbstractButton btToVaryNormOfVector) {
    ButtonGroup group = new ButtonGroup();
    group.add(btToVaryNormOfVector);

    final BSelecteurCheckBox selectSameNorm = new BSelecteurCheckBox(FlecheScaleData.PROP_IS_SAME_NORM_USED,
        new BuRadioButton());
    group.add(selectSameNorm.getCb());
    selectSameNorm.setTooltip(EbliLib.getS("Si activ�, tous les vecteurs seront dessin�s avec la m�me norme"));
    selectSameNorm.setTitle(EbliLib.getS("Utiliser une norme constante"));

    final BSelecteurButton selectBtDefault = new BSelecteurButton(FlecheScaleData.PROP_PIXEL_SAME_NORM_DEFAULT,
        new BuToolButton(BuResource.BU.getIcon("rafraichir")));
    selectBtDefault.setTooltip(EbliLib.getS("Initialiser avec la valeur par d�faut"));
    selectBtDefault.setUpdateWithMinValue(true);
    selectBtDefault.setAddListenerToTarget(false);
    selectBtDefault.setTitle(EbliLib.getS("R�initialiser"));

    final BSelecteurSpinner selectFixedValue = new BSelecteurSpinner(FlecheScaleData.PROP_PIXEL_SAME_NORM,
        new JSpinner(new SpinnerNumberModel(10, 0, 100, 1)));
    selectSameNorm.getCb().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        final boolean selected = selectSameNorm.getCb().isSelected();
        selectBtDefault.setEnabled(selected);
        selectFixedValue.setEnabled(selected);
      }
    });
    final boolean selected = getScaleFlecheData().isSameNorm();
    selectBtDefault.setEnabled(selected);
    selectFixedValue.setEnabled(selected);
    final BSelecteurComposite ratio = new BSelecteurComposite(
        new BSelecteurInterface[]{
            new BSelecteurComposite(new BSelecteurInterface[]{selectSameNorm, selectFixedValue}, true),
            selectBtDefault});
    ratio.setTitle(EbliLib.getS("Norme constante"));
    return ratio;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    // final BSelecteurFont font = new BSelecteurFont("scale.font");
    // font.setTitle(EbliLib.getS("Police �chelle"));
    final BSelecteurTextField tfMinNorm = new BSelecteurTextField(FlecheScaleData.PROP_MIN_NORM_TO_DRAW,
        BuValueValidator.MIN(0));
    tfMinNorm.setTitle(EbliLib.getS("Norme minimale"));
    tfMinNorm.setTooltip(EbliLib.getS("Afficher seulement les vecteurs dont la norme est sup�rieure �"));
    tfMinNorm.setAddListenerToTarget(false);
    final BSelecteurInterface createLegendSelecteur = createLegendSelecteur();
    BuRadioButton bt = new BuRadioButton(CtuluLibString.EMPTY_STRING);
    final BSelecteurInterface createRelativeSelecteur = createRelativeSelecteur(createLegendSelecteur, bt);
    return new BSelecteurInterface[]{ /** font, **/
        createRelativeSelecteur, createFixedSelecteur(bt), createLegendSelecteur, tfMinNorm};
  }

  @Override
  public Object getProperty(final String _key) {
    if ("scale.font".equals(_key)) {
      return getFleche().getFontForFlechLegend();
    }
    if (FlecheScaleData.PROP_IS_SAME_NORM_USED == _key) {
      return Boolean.valueOf(getScaleFlecheData().isSameNorm());
    }
    if (FlecheScaleData.PROP_MIN_NORM_TO_DRAW == _key) {
      return CtuluLib.getDouble(getScaleFlecheData()
          .getMinNormValueToDraw());
    }
    if (FlecheScaleData.PROP_PIXEL_FOR_RELATIVE_DEFAULT == _key) {
      spinnerForRatio.setStepSize(new Double(getIncForSpinner()));
      return CtuluLib.getDouble(getScaleFlecheData().getDefaultFacteur());
    }
    if (FlecheScaleData.PROP_PIXEL_FOR_RELATIVE == _key) {
      return CtuluLib.getDouble(getScaleFlecheData()
          .getPixelPerUnit());
    }
    if (FlecheScaleData.PROP_PIXEL_SAME_NORM == _key) {
      return new Integer(getScaleFlecheData()
          .getPixelForConstantNorm());
    }
    if (FlecheScaleData.PROP_PIXEL_SAME_NORM_DEFAULT == _key) {
      return new Integer(getScaleFlecheData()
          .getDefaultSizeInPixelForFixed());
    }
    if (FlecheScaleData.PROP_LEGEND_CONSTANT_NORM_VALUE == _key) {
      return getScaleFlecheData().getLegendFixRealNorm();
    }
    if (FlecheScaleData.PROP_LEGEND_USE_CONSTANT_NORM_VALUE == _key) {
      return getScaleFlecheData()
          .getLegendUseFixRealNorm();
    }
    return null;
  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if ("scale.font".equals(_key)) {
      getFleche().setFontForFlechLegend((Font) _newProp);
      return true;
    }
    if (FlecheScaleData.PROP_IS_SAME_NORM_USED == _key) {
      return getScaleFlecheData().setUseSameNorm(
          ((Boolean) _newProp).booleanValue());
    }
    if (FlecheScaleData.PROP_LEGEND_CONSTANT_NORM_VALUE == _key) {
      return getScaleFlecheData().setLegendFixRealNorm(
          (Double) _newProp);
    }
    if (FlecheScaleData.PROP_LEGEND_USE_CONSTANT_NORM_VALUE == _key) {
      return getScaleFlecheData()
          .setLegendUseFixRealNorm(((Boolean) _newProp).booleanValue());
    }
    if (FlecheScaleData.PROP_MIN_NORM_TO_DRAW == _key && _newProp != null) {
      return getScaleFlecheData().setMinNormeValueToDraw(
          ((Double) _newProp).doubleValue());
    }
    if (FlecheScaleData.PROP_PIXEL_FOR_RELATIVE_DEFAULT == _key || FlecheScaleData.PROP_PIXEL_FOR_RELATIVE == _key) {
      return getScaleFlecheData()
          .setPixelPerUnit((((Double) _newProp).doubleValue()));
    }
    if (FlecheScaleData.PROP_PIXEL_SAME_NORM == _key || FlecheScaleData.PROP_PIXEL_SAME_NORM_DEFAULT == _key) {
      return getScaleFlecheData()
          .setPixelForConstantNorm(((Integer) _newProp).intValue());
    }
    return false;
  }
}
