/*
 * @file         CalqueGuiHelper.java
 * @creation     1998-10-27
 * @modification $Date: 2007-05-04 13:49:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuPopupButton;
import com.memoire.bu.BuResource;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.JComponent;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.*;
import org.fudaa.ebli.palette.BPaletteForme;
import org.fudaa.ebli.palette.BPaletteProprietesSurface;
import org.fudaa.ebli.palette.BPaletteSelecteurReduitCouleur;
import org.fudaa.ebli.palette.BPaletteTexture;
import org.fudaa.ebli.palette.BPaletteTrait;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Classe statique de gestion des composants de controle des calques. Elle sert a la fenetre d'affichage de VueCalque
 * pour savoir quels sont les beans de controle de proprietes a integrer. ex: pour le calque de dessin, on integrera une
 * palette de couleur, une palette de trait, une palette de formes, ...
 * 
 * @version $Revision: 1.15 $ $Date: 2007-05-04 13:49:43 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class CalqueGuiHelper implements PropertyChangeListener {
  /**
   * L'objet statique CalqueGuiHelper. C'est le seul disponible pour toute l'application.
   */
  public static final CalqueGuiHelper CALQUE_OUTILS = new CalqueGuiHelper();
  public final static String CALQUE_DESSIN = "org.fudaa.ebli.calque.BCalqueDessin";
  public final static String CALQUE_DESSIN_INTERACTION = "org.fudaa.ebli.calque.BCalqueDessinInteraction";
  public final static String CALQUE_POLYLIGNE = "org.fudaa.ebli.calque.BCalquePolyligne";
  public final static String CALQUE_POLYGONE = "org.fudaa.ebli.calque.BCalquePolygone";
  public final static String CALQUE_POINT = "org.fudaa.ebli.calque.BCalquePoint";
  public final static String CALQUE_GRILLE_REGULIERE = "org.fudaa.ebli.calque.BCalqueGrilleReguliere";
  public final static String CALQUE_CARTE = "org.fudaa.ebli.calque.BCalqueCarte";
  public final static String CALQUE_LEGENDE = "org.fudaa.ebli.calque.BCalqueLegende";
  // public final static String VAG_CALQUE_HAUTEUR_HOULE="org.fudaa.fudaa.vag.VagCalqueHauteurHoule";
  Hashtable manager_;
  PropertyChangeSupport pSupport_;

  /**
   * Constructeur. Initialise un table de hachage contenant des tableaux de Composants indexes par le nom des classes
   * des calques associes.
   */
  protected CalqueGuiHelper() {
    pSupport_ = new PropertyChangeSupport(this);
    // JComponent[] comps;
    manager_ = new Hashtable();
    // SpecificTools
    final BuPopupButton popSrpc = createPaletteSelecteur();
    final BSelecteurPlanCouleur spc = new BSelecteurPlanCouleur();
    spc.addPropertyChangeListener(this);
    final BuPopupButton popSpc = createPopPlan(spc);
    final BSelecteurCouleur sc = new BSelecteurCouleur();
    sc.addPropertyChangeListener(spc);
    final BuPopupButton popSc = createPopCouleur(sc);
    final BSelecteurReduitParametresGouraud srpg = new BSelecteurReduitParametresGouraud();
    srpg.addPropertyChangeListener(this);
    srpg.setPropertyName("parametresGouraud");
    final BuPopupButton popSrpg = createPopLissage(srpg);
    final BPaletteProprietesSurface ccs = new BPaletteProprietesSurface();
    ccs.addPropertyChangeListener(this);
    final BuPopupButton popCcs = createPopSurface(ccs);
    final BPaletteTrait pt = new BPaletteTrait();
    pt.addPropertyChangeListener(this);
    final BuPopupButton popPt = createPopTrait(pt);
    final BSelecteurResolution sr = new BSelecteurResolution();
    sr.addPropertyChangeListener(this);
    final BuPopupButton popSr = createPopResolution(sr);
    final BPaletteForme pf = new BPaletteForme();
    pf.addPropertyChangeListener(this);
    final BuPopupButton popPf = createPopForma(pf);
    final BPaletteTexture px = new BPaletteTexture();
    px.addPropertyChangeListener(this);
    final BuPopupButton popPx = createPopTexture(px);
    final BSelecteurReduitFonteNewVersion srf = new BSelecteurReduitFonteNewVersion();
    srf.setSelecteurTarget(new BSelecteurTargetInterface() {

      @Override
      public boolean setProperty(final String _key, final Object _newProp) {
        CalqueGuiHelper.this.propertyChange(new PropertyChangeEvent(srf, srf.getProperty(), null, _newProp));
        return true;
      }
      
      @Override
      public Object getMoy(final String _key) {
        return getProperty(_key);
      }
      @Override
      public Object getMin(final String _key) {
        return getProperty(_key);
      }

      @Override
      public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {}

      @Override
      public Object getProperty(final String _key) {
        return null;
      }

      @Override
      public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {}

    });
    final BuPopupButton popSrf = createPopPolice(srf);
    final BSelecteurAncre sa = new BSelecteurAncre();
    sa.addPropertyChangeListener(this);
    final BuPopupButton popSa = createPopAncre(sa);
    // Affectation aux calques
    // BCalquePoint
    manager_.put(CALQUE_POINT, new JComponent[] { popSrpc });
    manager_.put(CALQUE_GRILLE_REGULIERE, new JComponent[] { popSpc, popSc, popSrpc, popSrpg, popCcs });
    manager_.put(CALQUE_CARTE, new JComponent[] { popSpc, popSc, popSrpc, popSrpg, popCcs });
    manager_.put(CALQUE_POLYLIGNE, new JComponent[] { popPt, popSpc, popSc, popSr });
    manager_.put(CALQUE_DESSIN_INTERACTION, new JComponent[] { popSpc, popSc, popPf, popPx, popSrf });
    manager_.put(CALQUE_LEGENDE, new JComponent[] { popSpc, popSc, popSrf, popSa });
  }

  private BuPopupButton createPopAncre(final BSelecteurAncre _sa) {
    final BuPopupButton popSa = new BuPopupButton(EbliLib.getS("Ancre"), _sa);
    popSa.setMargin(new Insets(0, 0, 0, 0));
    popSa.setActionCommand("DEFINIRANCRE");
    popSa.setToolTipText("Ancre de l�gende");
    popSa.setIcon(EbliResource.EBLI.getIcon("ancre"));
    return popSa;
  }

  private BuPopupButton createPopPolice(final BSelecteurReduitFonteNewVersion _srf) {
    final BuPopupButton popSrf = new BuPopupButton(EbliLib.getS("Police"), _srf.getComponent());
    popSrf.setMargin(new Insets(0, 0, 0, 0));
    popSrf.setActionCommand("DEFINIRPOLICE");
    popSrf.setToolTipText("Police");
    popSrf.setIcon(BuResource.BU.getIcon("fonte"));
    return popSrf;
  }

  private BuPopupButton createPopTexture(final BPaletteTexture _px) {
    final BuPopupButton popPx = new BuPopupButton(EbliLib.getS("Texture"), _px);
    popPx.setMargin(new Insets(0, 0, 0, 0));
    popPx.setActionCommand("DEFINIRTEXTURE");
    popPx.setToolTipText("Texture");
    popPx.setIcon(BuResource.BU.getIcon("texture"));
    popPx.setName("stTEXTURE");
    return popPx;
  }

  private BuPopupButton createPopForma(final BPaletteForme _pf) {
    final BuPopupButton popPf = new BuPopupButton(EbliLib.getS("Forme"), _pf);
    popPf.setMargin(new Insets(0, 0, 0, 0));
    popPf.setActionCommand("DEFINIRFORME");
    popPf.setToolTipText("Forme");
    popPf.setIcon(EbliResource.EBLI.getIcon("polyg"));
    return popPf;
  }

  private BuPopupButton createPopResolution(final BSelecteurResolution _sr) {
    final BuPopupButton popSr = new BuPopupButton(EbliLib.getS("R�solution"), _sr);
    popSr.setMargin(new Insets(0, 0, 0, 0));
    popSr.setActionCommand("DEFINIRRESOLUTION");
    popSr.setToolTipText("R�solution");
    popSr.setIcon(EbliResource.EBLI.getIcon("resolution"));
    return popSr;
  }

  private BuPopupButton createPopTrait(final BPaletteTrait _pt) {
    final BuPopupButton popPt = new BuPopupButton(EbliLib.getS("Trait"), _pt);
    popPt.setMargin(new Insets(0, 0, 0, 0));
    popPt.setActionCommand("DEFINIRTRAIT");
    popPt.setToolTipText("Trait");
    popPt.setIcon(EbliResource.EBLI.getIcon("trait"));
    return popPt;
  }

  private BuPopupButton createPopSurface(final BPaletteProprietesSurface _ccs) {
    final BuPopupButton popCcs = new BuPopupButton(EbliLib.getS("Surface"), _ccs);
    popCcs.setMargin(new Insets(0, 0, 0, 0));
    popCcs.setActionCommand("DEFINIRSURFACE");
    popCcs.setToolTipText("Param�tres de surface");
    popCcs.setIcon(EbliResource.EBLI.getIcon("surface"));
    return popCcs;
  }

  private BuPopupButton createPopLissage(final BSelecteurReduitParametresGouraud _srpg) {
    final BuPopupButton popSrpg = new BuPopupButton(EbliLib.getS("Lissage"), _srpg);
    popSrpg.setMargin(new Insets(0, 0, 0, 0));
    popSrpg.setActionCommand("DEFINIRLISSAGE");
    popSrpg.setToolTipText("Param�tres de lissage");
    popSrpg.setIcon(EbliResource.EBLI.getIcon("lissage"));
    return popSrpg;
  }

  private BuPopupButton createPopCouleur(final BSelecteurCouleur _sc) {
    final BuPopupButton popSc = new BuPopupButton(EbliLib.getS("Couleur"), _sc);
    popSc.setMargin(new Insets(0, 0, 0, 0));
    popSc.setActionCommand("DEFINIRCOULEUR");
    popSc.setToolTipText("Couleur");
    popSc.setIcon(EbliResource.EBLI.getIcon("couleur"));
    return popSc;
  }

  private BuPopupButton createPopPlan(final BSelecteurPlanCouleur _spc) {
    final BuPopupButton popSpc = new BuPopupButton(EbliLib.getS("Plan"), _spc);
    popSpc.setMargin(new Insets(0, 0, 0, 0));
    popSpc.setActionCommand("DEFINIRPLAN");
    popSpc.setToolTipText("Plan");
    popSpc.setIcon(EbliResource.EBLI.getIcon("plan"));
    popSpc.setName("stPLAN");
    return popSpc;
  }

  private BuPopupButton createPaletteSelecteur() {
    final BPaletteSelecteurReduitCouleur srpc = new BPaletteSelecteurReduitCouleur();
    srpc.addPropertyChangeListener(this);
    srpc.setPropertyName("paletteCouleur");
    final BuPopupButton popSrpc = new BuPopupButton(EbliLib.getS("Palette"), srpc);
    popSrpc.setMargin(new Insets(0, 0, 0, 0));
    popSrpc.setActionCommand("DEFINIRPALETTE");
    popSrpc.setToolTipText("Palette de couleurs");
    popSrpc.setIcon(EbliResource.EBLI.getIcon("palettecouleur"));
    return popSrpc;
  }

  /**
   * Renvoie la liste des composants associes a un calque.
   * 
   * @param _c calque interroge
   * @return tableau des composants
   */
  public synchronized JComponent[] getSpecificTools(final BCalque _c) {
    final Vector compos = new Vector();
    JComponent[] res;
    Class clazz = _c.getClass();
    String clazzname = clazz.getName();
    while (!clazzname.equals("org.fudaa.ebli.calque.BCalque")) {
      res = (JComponent[]) manager_.get(clazzname);
      if (res != null) {
        for (int i = 0; i < res.length; i++) {
          compos.addElement(res[i]);
        }
      }
      clazz = clazz.getSuperclass();
      clazzname = clazz.getName();
    }
    res = (JComponent[]) manager_.get(clazzname);
    if (res != null) {
      for (int i = 0; i < res.length; i++) {
        compos.addElement(res[i]);
      }
    }
    res = new JComponent[compos.size()];
    compos.copyInto(res);
    return res;
  }

  public synchronized void addPropertyChangeListener(final PropertyChangeListener _listener) {
    pSupport_.addPropertyChangeListener(_listener);
  }

  public synchronized void removePropertyChangeListener(final PropertyChangeListener _listener) {
    pSupport_.removePropertyChangeListener(_listener);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    pSupport_.firePropertyChange(_evt.getPropertyName(), _evt.getOldValue(), _evt.getNewValue());
  }
}
