/*
 * @creation     21 janv. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

/**
 * Une interface qu'un calque d'interaction doit implementer pour gerer l'accrochage sur des
 * objets.
 * @author Bertrand Marchand
 * @version $Id$
 */
public interface ZCatchListener {
  
  /**
   * L'accrochage a chang�. Par exemple, le point accroch� n'est plus le m�me. N'est pas appel�e
   * si l'accrochage est inactif (voir {@link #isCachingEnabled()})
   * @param _evt L'evenement suite a un accrochage.
   */
  public void catchChanged(ZCatchEvent _evt);
  
  /**
   * L'accrochage est-il actif ou non.<p>
   * 
   * Si l'accrochage est actif :
   * <ul><li>Le curseur est modifi� au passage sur les sommets/g�om�tries</li>
   * <li>Les keys pour changer de sommet/g�ometrie sont utilisables.</li>
   * <li>La m�thode {@link #catchChanged(ZCatchEvent)} est appel�e pour rendre compte de la g�om�trie accroch�e.</li></ul>
   * @return true : L'accrochage est actif.
   */
  public boolean isCachingEnabled();
}