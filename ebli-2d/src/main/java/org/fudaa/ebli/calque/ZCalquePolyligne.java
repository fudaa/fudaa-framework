/*
 * @creation 2002-09-23
 * @modification $Date: 2008-02-20 10:16:02 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

import java.awt.*;

/**
 * Un calque d'affichage de polyligne.
 * 
 * @version $Id: ZCalquePolyligne.java,v 1.32.6.1 2008-02-20 10:16:02 bmarchan Exp $
 * @author Fred Deniger
 */
public class ZCalquePolyligne extends ZCalqueAffichageDonnees {

  protected ZModelePolyligne modele_;

  public ZCalquePolyligne() {
    super();
  }

  @Override
  public GrBoite getDomaineOnSelected() {
    if (isSelectionEmpty()) {
      return null;
    }
    int m = selection_.getMaxIndex();
    if (m > modele_.getNombre()) {
      m = modele_.getNombre() - 1;
    }
    final GrBoite r = new GrBoite();
    final GrBoite temp = new GrBoite();
    final GrPolyligne poly = new GrPolyligne();
    for (int i = selection_.getMinIndex(); i <= m; i++) {
      if (selection_.isSelected(i)) {
        modele_.polyligne(poly, i);
        poly.boite(temp);
        r.ajuste(temp);
      }
    }
//    ajusteZoomOnSelected(r);
    return r;
  }

  /**
   * @param _ic le modele de l'icone a modifier: peut etre null
   * @param _ligne le modele de trace de ligne a modifier: non null
   * @param _idx l'indice du polygone
   */
  protected void updateTrace(final TraceIconModel _ic, final TraceLigneModel _ligne, final int _idx) {

  }

  /**
   * @param _modele le modele a considerer
   */
  public ZCalquePolyligne(final ZModelePolyligne _modele) {
    modele_ = _modele;
  }

  /**
   * @param _modele Modele
   */
  public void modele(final ZModelePolyligne _modele) {
    if (modele_ != _modele) {
      final ZModelePolyligne vp = modele_;
      modele_ = _modele;
      firePropertyChange("modele", vp, modele_);
    }
  }

  /**
   * @return Modele
   */
  public ZModelePolyligne modele() {
    return modele_;
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return modele();
  }

  private final GrPolyligne p_ = new GrPolyligne();

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    final int nombre = modele_.getNombre();
    // le clip d'affichage
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    int incrementPt = 1;
    int nbPoints;
    // la boite du polygone
    final GrBoite bPoly = new GrBoite();
    final TraceIcon icone = iconModel_ == null ? null : new TraceIcon(new TraceIconModel(iconModel_));
    final TraceLigne tl = new TraceLigne(new TraceLigneModel(ligneModel_));
    if (isAttenue()) {
      tl.setCouleur(EbliLib.getAlphaColor(attenueCouleur(tl.getCouleur()), alpha_));
      if (icone != null) {
        icone.setCouleur(EbliLib.getAlphaColor(attenueCouleur(icone.getCouleur()), alpha_));
      }
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      tl.setCouleur(EbliLib.getAlphaColor(tl.getCouleur(), alpha_));
      if (icone != null) {
        icone.setCouleur(EbliLib.getAlphaColor(icone.getCouleur(), alpha_));
      }
    }
    for (int i = 0; i < nombre; i++) {
      // recuperation du polygone
      modele_.polyligne(p_, i);
      // recuperation de la boite dans bPoly
      p_.boite(bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }
      nbPoints = p_.sommets_.nombre();
      if (nbPoints <= 0) {
        continue;
      }
      p_.autoApplique(versEcran);
      GrPoint ptOri = null;
      GrPoint ptDest;
      if (isRapide() && nbPoints > 15) {
        incrementPt = 3;
      } else {
        incrementPt = 1;
      }
      for (int j = nbPoints - 1; j >= 0; j -= incrementPt) {
        // le point de dest est initialise
        ptDest = p_.sommets_.renvoie(j);
        if (icone != null) {
          icone.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
        }
        if (ptOri != null) {
          tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        }
        ptOri = ptDest;
      }
    }
  }

  @Override
  public void doPaintSelection(final Graphics2D _g, final ZSelectionTrace _trace, final GrMorphisme _versEcran,
      final GrBoite _clipReel) {
    if ((isRapide()) || (isSelectionEmpty())) {
      return;
    }
    final GrBoite clip = _clipReel;
    // Si le domaine des polys n'est pas dans le domaine d'affichage on arrete
    if (!getDomaine().intersectXY(clip)) {
      return;
    }
    final GrMorphisme versEcran = _versEcran;
    Color cs = _trace.getColor();
    if (isAttenue()) {
      cs = attenueCouleur(cs);
    }
    final TraceLigne tlSelection = _trace.getLigne();
    final TraceIcon ic = _trace.getIcone();
    _g.setColor(cs);
    final int nb = selection_.getMaxIndex();
    int nbPoints;
    for (int i = nb; i >= 0; i--) {
      if (!selection_.isSelected(i)) {
        continue;
      }
      modele_.polyligne(p_, i);
      if (clip.intersectXY(p_.boite())) {
        p_.autoApplique(versEcran);
        GrPoint ptOri = null;
        GrPoint ptDest;
        nbPoints = p_.nombre();
        for (int j = nbPoints - 1; j >= 0; j--) {
          // le point de dest est initialise
          ptDest = p_.sommets_.renvoie(j);
          ic.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
          if (ptOri != null) {
            tlSelection.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
          }
          ptOri = ptDest;
        }
      }
    }
  }

  @Override
  public CtuluListSelection selection(final LinearRing _polySelection, final int _mode) {
    final GrBoite domaineBoite = getDomaine();
    if (domaineBoite==null|| modele_.getNombre() == 0 || !isVisible()) {
      return null;
    }
    final Envelope polyEnv = _polySelection.getEnvelopeInternal();

    final Envelope domaine = new Envelope(domaineBoite.e_.x_, domaineBoite.o_.x_, domaineBoite.e_.y_,
        domaineBoite.o_.y_);
    // si l'envelop du polygone n'intersect pas le domaine, il n'y a pas de selection
    if (!polyEnv.intersects(domaine)) {
      return null;
    }
    final CtuluListSelection r = creeSelection();
    final Coordinate c = new Coordinate();
    final GrPolyligne poly = new GrPolyligne();
    final IndexedPointInAreaLocator tester = new IndexedPointInAreaLocator(_polySelection);
    for (int i = modele().getNombre() - 1; i >= 0; i--) {
      modele().polyligne(poly, i);
      if (VecteurGrPoint.estSelectionneEnv(c, polyEnv, _polySelection, tester, poly.sommets_, _mode)) {
        r.add(i);
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    return r;
  }

  @Override
  public LineString getSelectedLine() {
    if (getNbSelected() != 2) {
      return null;
    }
    final Coordinate[] cs = new Coordinate[2];
    final GrPolyligne poly = new GrPolyligne();
    modele_.polyligne(poly, getLayerSelection().getMinIndex());
    cs[0] = poly.sommets_.barycentre();
    modele_.polyligne(poly, getLayerSelection().getMaxIndex());
    cs[1] = poly.sommets_.barycentre();
    if (cs[0].compareTo(cs[1]) > 0) {
      final Coordinate tmp = cs[0];
      cs[0] = cs[1];
      cs[1] = tmp;

    }
    return GISGeometryFactory.INSTANCE.createLineString(cs);
  }

  @Override
  public CtuluListSelection selection(final GrPoint _pt, final int _tolerance) {
    if (!isVisible()) {
      return null;
    }
    GrBoite bClip = getDomaine();
    if(bClip==null){
      return null;
    }
    final double distanceReel = GrMorphisme.convertDistanceXY(getVersReel(), _tolerance);
    if ((!bClip.contientXY(_pt)) && (bClip.distanceXY(_pt) > distanceReel)) {
      return null;
    }
    final int nb = modele().getNombre() - 1;
    bClip = getClipReel(getGraphics());
    final GrPolyligne poly = new GrPolyligne();
    for (int i = nb; i >= 0; i--) {
      modele().polyligne(poly, i);
      if (bClip.intersectXY(poly.boite()) && poly.distanceXY(_pt) < distanceReel) {
        final CtuluListSelection r = creeSelection();
        r.add(i);
        return r;
      }
    }
    return null;
  }
}
