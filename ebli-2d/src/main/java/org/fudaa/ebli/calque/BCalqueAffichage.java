/*
 * @creation     1998-08-27
 * @modification $Date: 2007-06-20 12:23:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuIcon;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BConfigurableSingleSection;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.trace.FiltreAttenuation;

/**
 * Une classe de base pour tous les calques d'affichage.
 *
 * @version $Id: BCalqueAffichage.java,v 1.31 2007-06-20 12:23:12 deniger Exp $
 * @author Guillaume Desnoix , Axel von Arnim
 */
public abstract class BCalqueAffichage extends BCalque {

  /**
   * Cette methode utilise de <I>FiltreAttenuation </I>.
   */
  public static final Color attenueCouleur(final Color _c) {
    Color r = _c;
    if (_c != null) {
      r = FiltreAttenuation.attenueCouleur(_c);
    }
    return r;
  }
  private BCalqueAffichageLegendProperties legendProperties_;

  @Override
  public BConfigurableInterface getSingleConfigureInterface() {
    return new BConfigurableSingleSection(new BCalqueSectionConfigure(this));
  }
  
  

  public boolean isLegendProperitesSet() {
    return legendProperties_ != null;
  }

  public BCalqueAffichageLegendProperties getLegendProperties() {
    if (legendProperties_ == null) {
      legendProperties_ = new BCalqueAffichageLegendProperties();
    }
    return legendProperties_;
  }

  @Override
  public void firePropertyChange(String _propertyName, Object _oldValue, Object _newValue) {
    super.firePropertyChange(_propertyName, _oldValue, _newValue);
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties cui = super.saveUIProperties();
    cui.put("calque.attenue", isAttenue());
    if (legendProperties_ != null) {
      legendProperties_.saveInProperties(cui);
    }
    return cui;
  }

  public void putCalqueInfo(final String _s) {
    putClientProperty("CalqueInfo", _s);
    firePropertyChange("title", true, false);
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      if (_p.isDefined("calque.attenue")) {
        setAttenue(_p.getBoolean("calque.attenue"));
      }
      if (BCalqueAffichageLegendProperties.isSavedInProperties(_p)) {
        getLegendProperties().restoreFrom(_p, getLegende() == null ? null : getLegende().getLegendePanelFor(this));
        if (getLegende() != null) {
          getLegende().repaint();
          getLegende().isRestoreFromProperties(this);
        }
      }
    }
  }

  /**
   * Cette methode utilise de <I>FiltreAttenuation </I>.
   */
  public static final Icon attenueIcone(final Icon _i) {
    Icon r = _i;
    if (_i instanceof BuIcon) {
      r = FiltreAttenuation.attenueIcone((BuIcon) _i);
    } else if (_i instanceof ImageIcon) {
      r = FiltreAttenuation.attenueIcone((ImageIcon) _i);
    }
    return r;
  }

  /**
   * Cette methode utilise de <I>FiltreAttenuation </I>.
   */
  public static final Image attenueImage(final Image _i) {
    return FiltreAttenuation.attenueImage(_i);
  }
  private boolean attenue_;
  private BCalqueLegende cqLegende_;
  private BCalqueContextuelListener ctxDeleg_;

  protected BCalqueAffichage() {
  }

  /**
   * Construit la l�gende (JPanel) et l'associe au calque des legendes. Si pas de calque legende, la l�gende n'est pas construite.
   */
  protected void construitLegende() {
  }

  /**
   * Renvoi des elements selectionnables (GrContour) de ce calque.
   */
  public VecteurGrContour contours() {
    // message pour prevenir qd un calque ne surchage pas
    FuLog.warning("EBL: Selection warning: le calque "
            + getClass().getName().substring(getClass().getName().lastIndexOf('.') + 1)
            + " n'implemente pas la methode:\n  public VecteurGrContour contours()");
    return new VecteurGrContour();
  }

  /**
   * Surcharge de la destruction du calque pour faire disparaitre la l�gende.
   */
  @Override
  public void detruire() {
    if (cqLegende_ != null) {
      cqLegende_.enleve(this);
    }
    super.detruire();
  }

  public JPopupMenu getCmdsContextuelles() {
    return (ctxDeleg_ == null) ? null : ctxDeleg_.getCmdsContextuelles();
  }

  public BCalqueLegende getLegende() {
    return cqLegende_;
  }

  /**
   * Accesseur de la propriete <I>attenue </I>. Cette propriete est un mode d'affichage du calque. Il attenue les couleurs.
   */
  public boolean isAttenue() {
    return attenue_;
  }

  @Override
  public boolean isGroupeCalque() {
    return false;
  }

  /**
   * @return true si la palette du calque est modifiable par l'utilisateur
   */
  public boolean isPaletteModifiable() {
    return true;
  }

  /**
   * Affectation de la propriete <I>attenue </I>.
   */
  public void setAttenue(final boolean _attenue) {
    if (attenue_ != _attenue) {
      final boolean vp = attenue_;
      attenue_ = _attenue;
      firePropertyChange("attenue", vp, attenue_);
      if (isVisible()) {
        quickRepaint();
      }
    }
  }

  /**
   * Change la couleur de fond.
   */
  @Override
  public void setBackground(final Color _v) {
    final Color vp = getBackground();
    if (!_v.equals(vp)) {
      super.setBackground(_v);
      if (isVisible()) {
        quickRepaint();
      }
    }
  }

  //
  // public ContextuelDelegator getContextuelDelegator()
  // {
  // return ctxDeleg_;
  // }
  //
  public void setContextuelDelegator(final BCalqueContextuelListener _ctxDeleg) {
    if (ctxDeleg_ != _ctxDeleg) {
      final BCalqueContextuelListener vp = ctxDeleg_;
      ctxDeleg_ = _ctxDeleg;
      firePropertyChange("contextuelDelegator", vp, ctxDeleg_);
    }
  }

  /* */
  /**
   * Change la fonte (si utilisee) pour ce calque.
   */
  /*
   * public void setFont(Font _v){ Font vp = getFont(); if (!_v.equals(vp)) { super.setFont(_v); if (isVisible())
   * quick_repaint(); } }
   */
  /**
   * @param _v la nouvelle couleur
   * @param _repaint true si on doit redessiner tout de suite
   * @return true si changement
   */
  private boolean setForeground(final Color _v, final boolean _repaint) {
    final Color vp = getForeground();
    if (_v != null && !_v.equals(vp)) {
      super.setForeground(_v);
      firePropertyChange("foreground", vp, _v);
      if (_repaint && isVisible()) {
        quickRepaint();
      }
      return true;
    }
    return false;
  }

  /**
   * Change la couleur de premier-plan.
   */
  @Override
  public void setForeground(final Color _v) {
    setForeground(_v, false);
  }

  /**
   * Associe le calque qui contient les l�gendes.
   *
   * @param _cqLegende Le calque. Peut �tre null.
   */
  public void setLegende(final BCalqueLegende _cqLegende) {
    if (cqLegende_ != _cqLegende) {
      final BCalqueLegende vp = cqLegende_;
      cqLegende_ = _cqLegende;
      construitLegende();
      firePropertyChange("legende", vp, cqLegende_);
    }
  }

  // Proprietes
  /**
   * Rend (ou pas) visible ce calque.
   */
  @Override
  public void setVisible(final boolean _v) {
    if (isVisible() != _v) {
      super.setVisible(_v);
      if (getLegende() != null && getLegende().containsLegend(this)) {
        getLegende().setVisible(this, _v);
        // getLegende().getP
        getLegende().updateAll();
        getLegende().revalidate();
      }
      // deja fait par BCalque
      // firePropertyChange("visible", vp, !vp);
    }
  }
  /**
   * Ce champ statique est un <I>FiltreAttenuation </I>.
   */
  // public static final FiltreAttenuation FILTRE_ATTENUATION
  // new FiltreAttenuation();
}
