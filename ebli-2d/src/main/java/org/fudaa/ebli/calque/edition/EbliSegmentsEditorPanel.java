package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuScrollPane;
import javax.swing.JTable;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;

@SuppressWarnings("serial")
public class EbliSegmentsEditorPanel extends CtuluDialogPanel
{
  final CtuluCommandContainer  cmd_;
  final EbliSegmentsTableModel tableModel_;
  final JTable                 table_;
  ZEditorValidatorI            editValidator_;
  ZModeleEditable              mdl_;

  public EbliSegmentsEditorPanel(final int _idx, final int[] _segments, final EbliCoordinateDefinition[] _defs,
      final ZModeleLigneBriseeEditable _modele, final boolean _editSegment, ZEditorValidatorI _validator, final CtuluCommandManager _cmd) {
    this(new EbliSegmentsTableModel(_defs, _modele.getGeomData(), _modele, _idx, _segments, _editSegment), _cmd);
    
    editValidator_ = _validator;
    mdl_ = _modele;
  }

  public EbliSegmentsEditorPanel(final EbliSegmentsTableModel _model, final CtuluCommandManager _cmd) {
    tableModel_ = _model;
    cmd_ = _cmd;
    table_ = new CtuluTable();
    table_.setModel(tableModel_);
    tableModel_.updateCellEditor(table_);
    setLayout(new BuBorderLayout());
    add(new BuScrollPane(table_));
  }

  @Override
  public boolean apply(){
    if (editValidator_!= null && !editValidator_.isSegmentEditionValid(mdl_))
      return false;
    
    final CtuluCommandComposite cmp = cmd_ == null ? null : new CtuluCommandComposite();
    tableModel_.apply(cmp);
    if (cmd_ != null) {
      cmd_.addCmd(cmp.getSimplify());
    }
    return true;
  }

  @Override
  public boolean isDataValid(){
    if (table_.isEditing()) {
      table_.getCellEditor().stopCellEditing();
    }
    final CtuluAnalyze ana = tableModel_.isValid();
    if (ana != null && ana.containsFatalError()) {
      setErrorText(ana.getFatalError());
      return false;
    }
    return true;
  }
}
