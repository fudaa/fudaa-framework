/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package org.fudaa.ebli.calque;

import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;

/**
 * @author deniger
 */
public class BCalqueCacheManagerSelection extends AbstractBCalqueCacheManager implements ZSelectionListener {

  /**
   * @param calque
   */
  public BCalqueCacheManagerSelection(final ZCalqueAffichageDonneesAbstract calque) {
    super(calque);
  }

  public static void installDefaultCacheManagerSelection(final ZCalqueAffichageDonneesAbstract calque) {
    final BCalqueCacheManagerSelection cacheManager = new BCalqueCacheManagerSelection(calque);
    calque.addPropertyChangeListener(new CacheManagerPropertyChangeListener(cacheManager));
    calque.addSelectionListener(cacheManager);
    calque.setCacheManagerSelection(cacheManager);
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    clearCache();
  }

  public void paintInImage(ZSelectionTrace selectionTrace) {
    layer.paintSelection((Graphics2D) getImageGraphics(), selectionTrace);
  }

  void paint(Graphics2D _g, ZSelectionTrace _trace, GrMorphisme _versEcran, GrBoite _clipReel) {
    if (!isCacheUptodate()) {
      layer.doPaintSelection((Graphics2D) getImageGraphics(), _trace, _versEcran, _clipReel);
    }
    _g.drawImage(image, 0, 0, layer);
  }
}
