/*
 * @creation     17 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.edition.BPaletteDistance;
import org.fudaa.ebli.calque.edition.ZCalqueDistanceInteraction;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteSpecAbstract;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action pour afficher une palette indiquant la distance entre une s�rie de points.
 *
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class SceneShowDistanceAction extends EbliActionPaletteSpecAbstract implements PropertyChangeListener {

  private ZEbliCalquesPanel calquesPanel_;
  // Calque ne fermant pas l'edition \\
  private BCalqueInteraction calqueZoom_;
  private BCalqueInteraction calqueDeplacementVue_;
  /**
   * Le calque contenant les informations de distance.
   */
  private ZCalqueDistanceInteraction calqueDistance_;
  protected EbliFormatterInterface formatter_;

  public SceneShowDistanceAction(final ZEditorDefault _editor, EbliFormatterInterface _formatter) {
    super(EbliLib.getS("Distance"), EbliResource.EBLI.getToolIcon("bu_scrollpane_corner"), "PALETTE_DISTANCE");
    formatter_ = _formatter;
    calquesPanel_ = _editor.getPanel();
    calquesPanel_.getController().addPropertyChangeListener("gele", this);
    calqueZoom_ = calquesPanel_.getController().getCalqueInteraction("cqAGRANDIR");
    calqueDeplacementVue_ = calquesPanel_.getController().getCalqueInteraction("cqDEPLACEMENT_VUE-I");
    calqueDistance_ = new ZCalqueDistanceInteraction();
    calqueDistance_.setGele(true);
    calquesPanel_.addCalqueInteraction(calqueDistance_);
    calqueDistance_.addPropertyChangeListener("gele", this);
    setResizable(true);
    setEnabled(true);
  }

  @Override
  public void changeAction() {
    super.changeAction();
    if (isSelected()) {
      calquesPanel_.setCalqueInteractionActif(calqueDistance_);
      calquesPanel_.getVueCalque().requestFocus();
    } else {
      cleanCalques();
    }
  }

  protected void cleanCalques() {
    calqueDistance_.cancelEdition();
    calquesPanel_.unsetCalqueInteractionActif(calqueDistance_);
    if (calquesPanel_.getController().getCqSelectionI() != null) {
      calquesPanel_.getController().getCqSelectionI().setGele(false);
    }
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent() {
    return new BPaletteDistance(calqueDistance_, formatter_, calquesPanel_);
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    // Cas un autre calque que celui du zoom, du deplacement de vue et d'edition
    // est d�gel� donc on ferme le pannel d'edition si il �tait ouvert.
    if (_evt.getSource() != calqueZoom_ && _evt.getSource() != calqueDeplacementVue_ && _evt.getSource() != calqueDistance_
            && !((Boolean) _evt.getNewValue()).booleanValue() && isSelected()) {
      calqueDistance_.cancelEdition();
      setSelected(false);
      super.changeAction();
    }
  }

  @Override
  protected void hideWindow() {
    super.hideWindow();
    cleanCalques();
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent e) {
    cleanCalques();
  }
}
