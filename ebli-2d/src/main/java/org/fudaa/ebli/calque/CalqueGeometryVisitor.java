/*
 * @creation 21 nov. 06
 * @modification $Date: 2006-12-05 10:14:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.gis.GISLib;

/**
 * Un visiteur qui permet de determiner si un calque contient des g�ometries
 * d'un type donn�e.
 * @author Bertrand Marchand
 * @version $Id: CalqueFindPolyligneVisitor.java,v 1.1 2006-12-05 10:14:38 deniger Exp $
 */
public class CalqueGeometryVisitor implements BCalqueVisitor {
  int mask_=GISLib.MASK_ALL;

  boolean found_;

  public CalqueGeometryVisitor() {
    super();
  }

  public CalqueGeometryVisitor(final int _mask) {
    super();
    mask_=_mask;
  }

  @Override
  public boolean visit(final BCalque _cq) {
    // on arrete la visite
    if (found_) {
      return false;
    }
    if (_cq == null) {
      return true;
    }

    found_ = isMatching(_cq);
    if (found_) {
      return false;
    }
    return true;
  }

  /**
   * Controle que le calque contient des objets du type donn�.<p>
   * Attention : Un polygone n'est pas consid�r� comme une polyligne sp�cialis�e.
   * @param _cq Le calque a tester.
   * @return True si trouv�.
   */
  public boolean isMatching(final BCalque _cq) {
    if (_cq instanceof ZCalqueMultiPoint && (mask_&GISLib.MASK_MULTIPOINT)!=0) {
      return ((ZCalqueMultiPoint)_cq).modeleDonnees().getNombre() > 0;
    }
//    else if (_cq instanceof ZCalquePoint && (mask_&GISLib.MASK_POINT)!=0) {
//      return ((ZCalquePoint) _cq).modeleDonnees().getNombre() > 0;
//    }
    else if (_cq instanceof ZCalquePoint && (mask_ & GISLib.MASK_POINT) != 0
        && ((ZCalquePoint) _cq).modeleDonnees() instanceof ZModeleGeometry) {
      return ((ZCalquePoint) _cq).modeleDonnees().getNombre() > 0;
    }
    else if (_cq instanceof ZCalqueLigneBrisee && (mask_&GISLib.MASK_POLYLINE)!=0) {
      boolean bok=((ZCalqueLigneBrisee) _cq).modeleDonnees().getNombre() > 0;
      return bok && ((ZCalqueLigneBrisee)_cq).modeleDonnees().getNbPolyligne()!=0;
    }
    else if (_cq instanceof ZCalqueLigneBrisee && (mask_&GISLib.MASK_POLYGONE)!=0) {
      boolean bok=((ZCalqueLigneBrisee)_cq).modeleDonnees().getNombre() > 0;
      return bok && ((ZCalqueLigneBrisee)_cq).modeleDonnees().getNbPolygone()!=0;
    }
    return false;
  }
  
  public void setMask(int _mask) {
    mask_=_mask;
  }
  
  public int getMask() {
    return mask_;
  }

  public boolean isFound() {
    return found_;
  }

  public void resetFound() {
    found_ = false;
  }

}
