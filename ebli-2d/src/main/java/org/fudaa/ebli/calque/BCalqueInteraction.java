/*
 * @file         BCalqueInteraction.java
 * @creation     1998-08-27
 * @modification $Date: 2008-03-27 15:26:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

/**
 * Une classe de base pour tous les calques d'interaction.
 *
 * @version $Revision: 1.15.8.1 $ $Date: 2008-03-27 15:26:28 $ by $Author: bmarchan $
 * @author Guillaume Desnoix , Axel von Arnim
 */
public abstract class BCalqueInteraction extends BCalque {

  public static final String NO_MOUSE_LISTENER = "NO_LISTENER";

  protected BCalqueInteraction() {
  }

  public void setNoListener() {
    putClientProperty(NO_MOUSE_LISTENER, Boolean.TRUE);
  }

  public String getDescription() {
    return "";
  }

  @Override
  public final boolean isGroupeCalque() {
    return false;
  }

  public Cursor getSpecificCursor() {
    return new Cursor(Cursor.DEFAULT_CURSOR);
  }

  // Icon
  /**
   * Dessine l'icone associe au calque. Elle dessine un flocon en mode <I>gele</I>, et appelle la methode <I>paintIcon</I>
   * de la classe <I>BCalque</I> pour dessiner l'icone proprement dit.
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    if (isGele()) {
      _g.setColor(new Color(128, 128, 255));
      _g.drawLine(_x + 3, _y + 3, _x + 9, _y + 9);
      _g.drawLine(_x + 3, _y + 9, _x + 9, _y + 3);
      _g.drawLine(_x + 6, _y + 3, _x + 6, _y + 9);
      _g.drawLine(_x + 3, _y + 6, _x + 9, _y + 6);
    }
  }

  /**
   * @return true si ce calque d'interaction doit etre dessine.
   */
  public boolean alwaysPaint() {
    return false;
  }

  // Paint
  @Override
  public final void paint(final Graphics _g) {
    if (alwaysPaint() || getComponentCount() > 0) {
      super.paint(_g);
    }
  }

  @Override
  public final void repaint() {
    if (alwaysPaint() || getComponentCount() > 0) {
      super.repaint();
    }
  }

  @Override
  public final void repaint(final long _tm) {
    if (alwaysPaint() || getComponentCount() > 0) {
      super.repaint(_tm);
    }
  }
  // Proprietes
  private boolean gele_;

  /**
   * Accesseur de la propriete <I>gele</I>. Elle "gele" l'interaction. Cad: le calque devient insensible aux evenements souris, clavier, ... Par
   * exemple, si plusieurs calque d'interaction sont presents en meme temps, on voudra n'en activer qu'un, ie geler les autres.
   */
  public boolean isGele() {
    return gele_;
  }

  /**
   * Affectation de la propriete <I>gele</I>.
   */
  public void setGele(final boolean _gele) {
    if (gele_ != _gele) {
      final boolean vp = gele_;
      gele_ = _gele;
      firePropertyChange("gele", vp, gele_);
    }
  }

  protected final boolean isOkLeftEvent(final MouseEvent _evt) {
    return !isGele() && !_evt.isConsumed() && SwingUtilities.isLeftMouseButton(_evt);
  }

  protected final boolean isOkEvent(final MouseEvent _evt) {
    return !isGele() && !_evt.isConsumed() && !SwingUtilities.isRightMouseButton(_evt)
            && !SwingUtilities.isMiddleMouseButton(_evt);
  }
}
