/*
 *  @creation     15 sept. 2005
 *  @modification $Date: 2006-09-19 14:55:46 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.commun.EbliUIProperties;

/**
 * Classe a ne pas modifier car utilisee pour la sauvegarde.
 * 
 * @author Fred Deniger
 * @version $Id: BCalqueSaverGroup.java,v 1.6 2006-09-19 14:55:46 deniger Exp $
 */
public class BCalqueSaverGroup extends BCalqueSaverSingle {

  /**
   * @deprecated ne plus utiliser. Laisser temporairement pour les anciennes sauvegardes
   */
  BCalqueSaverInterface[] childUI_;

  /**
   * @deprecated ne plus utiliser. Laisser temporairement pour les anciennes sauvegardes
   */
  protected BCalqueSaverGroup() {
    setPersistenceClass(BCalquePersistenceGroupe.class.getName());
  }

  @Override
  public EbliUIProperties getUI() {
    //cela ne doit arrive que lorsque on recharge d'ancien fichier
    if(childUI_!=null){
      final EbliUIProperties res=super.getUI();
      res.put(BCalquePersistenceGroupe.getChildUiName(), childUI_);
      childUI_=null;
    }
    return super.getUI();
  }

 
}
