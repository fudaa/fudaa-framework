/*
 * @creation 31 ao�t 06
 * @modification $Date: 2008-05-13 12:10:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ebli.commun.EbliUIProperties;

/**
 * Une interface permettant de g�rer la persistence d'un calque. Elle offre 2 niveaux de sauvegarde: un rapide
 * (s�rialisation ou db4o) et un plus long ou les donn�es seraient sauvegard�s en clair (xml,ascii) et le tout dans une
 * archive.
 * 
 * @author fred deniger
 * @version $Id: BCalquePersistenceInterface.java,v 1.2.8.2 2008-05-13 12:10:36 bmarchan Exp $
 */
public interface BCalquePersistenceInterface {

  /**
   * Lit les fichiers d'un calque (.prop, .desc.xml et sp�cifiques) et restaure ses propri�t�s, ses donn�es dans un {@link EbliUIProperties}.
   * @param _loader le conteneur des donn�es
   * @param _parentPanel le panneau
   * @param _parentCalque le calque parent
   * @param _parentDirEntry le repertoire des donn�es dans le conteneur de donn�es finissant par '/'
   * @param _entryName le nom de l'entree sans extension
   * @param _proj la barre de progression
   * @return le nom du calque ajoute au tout
   */
  String restoreFrom(CtuluArkLoader _loader, BCalqueSaverTargetInterface _parentPanel, BCalque _parentCalque,
      String _parentDirEntry, String _entryName, ProgressionInterface _proj);

  /**
   * Permet de sauvegarder en clair (xml,ascii) les donn�es importantes et de retourner les donn�es � sauvegarder en
   * binaire. Cela s'apparente a une sauvegarde complete.
   * 
   * @param _saver le fichier de sauvegarde sous forme d'archive
   * @param _parentDirEntry le repertoire parent finissant par /
   * @param _parentDirIndice un repertoire parent (direct ou non) a utiliser pour les indice
   * @param le repertoire parent dans l'archive
   * @return this si non adapte. Sinon, renvoie les donn�es qui peuvent �tre sauvegard�es en binaire.
   */
  BCalqueSaverInterface saveIn(BCalque _cqToSave, CtuluArkSaver _saver, String _parentDirEntry, String _parentDirIndice);

  /**
   * Permet de sauvegarder le tout dans une seule r�f�rence. Permet de faire une sauvegarde rapide d'un calque. Pourrait
   * �tre utilis� pour faire de sauvegarde (undo), un historique .....
   * 
   * @param _prog la barre de progression
   * @return le saver contenant toutes les donn�es n�cessaires
   */
  BCalqueSaverInterface save(BCalque _cqToSave, ProgressionInterface _prog);

  /**
   * Appele lorsque toutes les donn�es ont �t� r�cup�r�es et eventuellement le modele de donn�es dans un {@link EbliUIProperties}.
   * Appel� apr�s {@link #restoreFrom(CtuluArkLoader, BCalqueSaverTargetInterface, BCalque, String, String, ProgressionInterface)}
   * 
   * @param _savedData Contient les donn�es propre a ce type de calque dans un {@link EbliUIProperties}.
   * @param _pn le panneau contenant la vue calque
   * @param _parent le calque parent
   * @param _prog la barre de progression
   * @return le nouveau nom du calque
   */
  String restore(BCalqueSaverInterface _savedData, BCalqueSaverTargetInterface _pn, BCalque _parent, ProgressionInterface _prog);
  

}
