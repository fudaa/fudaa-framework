/*
 * @creation 2002-10-02
 * @modification $Date: 2008-05-13 12:10:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.LinearRing;
import java.awt.Color;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ebli.commun.EbliSelectionMode;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;

/**
 * @version $Id$
 * @author Bertrand Marchand, Fred Deniger
 */
public class ZCalqueSelectionInteractionSimple extends ZCalqueSelectionInteractionAbstract {

  public interface EditorDelegate {

    String edit();
  }
  EditorDelegate editorDelegate;

//  private ZCalqueAffichageDonneesInterface zcalquesActif_;
  /**
   * Création d'un calque de sélection sans objets sélectionnables.
   */
  public ZCalqueSelectionInteractionSimple(final BGroupeCalque _donnees) {
    super(_donnees);
  }

  public EditorDelegate getEditorDelegate() {
    return editorDelegate;
  }

  public void setEditorDelegate(EditorDelegate editorDelegate) {
    this.editorDelegate = editorDelegate;
  }

  @Override
  public boolean editionAsked() {
    String message = null;
    if (editorDelegate != null) {
      message = editorDelegate.edit();
    } else {
      message = editor_.edit();
    }
    if (message != null) {
      FuLog.trace("EBL : ZCalqueSelectionInteractionSimple : " + message);
    }
    return message != null;
  }

  /**
   * Recupere les donnees.
   */
  @Override
  protected void formeSaisie() {
    if (internalGetScene().isRestrictedToCalqueActif() && internalGetScene().getCalqueActif() == null) {
      return;
    }
    if (mode_ == PONCTUEL
            || (mode_ == RECTANGLE && listePoints_.renvoie(0).distanceXY(listePoints_.renvoie(2)) < 4)) {
      if (idxGeomAccroch_ != null) {
        ZSceneSelection ssel = new ZSceneSelection(internalGetScene());
        CtuluListSelection sel = new CtuluListSelection(new int[]{idxVertexAccroch_});
        ssel.set(idxGeomAccroch_, sel);
        internalGetScene().changeSelection(ssel, modificateur_.getModificateur());
      } else {
        final GrPoint pt = listePoints_.renvoie(0);
        pt.autoApplique(getVersReel());
        internalGetScene().changeSelection(pt, tolerancePixel_, modificateur_.getModificateur());
      }
    } else {
      final GrPolygone poly = plHelper_.toGrPolygone();
      if (poly.nombre() >= 3) {
        poly.autoApplique(getVersReel());
        final LinearRing l = GISGeometryFactory.INSTANCE.createLinearRing(poly.sommets_.createCoordinateSequence(true));
        internalGetScene().changeSelection(l, modificateur_.getModificateur(), EbliSelectionMode.MODE_ALL);
      }
    }
  }

  /**
   * Efface les selections de tous les calques.
   */
  @Override
  public void clearSelections() {
    internalGetScene().clearSelection();
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    if (!internalGetScene().isSelectionEmpty()) {
      final Color old = _g.getColor();
      internalGetScene().paintSelection(_g, super.trace_, _versEcran, _clipReel);
      _g.setColor(old);
    }
  }
  // boolean special_;

  /*
   * public void setCalqueActif(final ZCalqueAffichageDonneesInterface _calque) { if (FuLog.isTrace()) { FuLog.trace("EBL: selection calque=" +
   * (_calque == null ? "null" : _calque.getTitle())); } // addCalqueActif(_calque); if (_calque != zcalquesActif_) { if (zcalquesActif_ != null) {
   * zcalquesActif_.clearSelection(); } zcalquesActif_ = _calque; if (zcalquesActif_ == null) { modificateur_.setSpecial(false); } else {
   * modificateur_.setSpecial(zcalquesActif_.isSpecialSelectionAllowed()); } repaint(); }
   *
   * }
   */
}
