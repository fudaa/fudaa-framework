/*
 * @creation     2002-06-13
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque;

import java.util.ArrayList;
import java.util.Arrays;
import org.fudaa.ebli.geometrie.*;

/**
 * Le modele du calque d'affichage de maillage.
 *
 * @version $Id: ZModeleStatiqueMaillageElement.java,v 1.13 2006-09-19 14:55:47 deniger Exp $
 * @author Fred Deniger
 */
public class ZModeleStatiqueMaillageElement implements ZModeleMaillageElement {

  private final GrMaillageElement maillage_;
  private GrNoeud[] noeuds_;
  private int nbNoeuds_;
  // private GrBoite domaine_;
  GrPoint[][] pointsBords_;

  /**
   * @param _maillage le maillage a afficher
   */
  public ZModeleStatiqueMaillageElement(final GrMaillageElement _maillage) {
    maillage_ = _maillage;
    nbNoeuds_ = -1;
    computeBord();
  }

  private void computeBord() {
    final GrElement[][] aretes = maillage_.aretesContours();
    pointsBords_ = new GrPoint[aretes.length][];
    final ArrayList l = new ArrayList(aretes[0].length);
    final int n = aretes.length;
    for (int i = 0; i < n; i++) {
      final GrElement[] ele = aretes[i];
      final int nele = ele.length;
      for (int j = 0; j < nele; j++) {
        l.addAll(Arrays.asList(ele[j].noeuds_));
      }
      pointsBords_[i] = new GrPoint[l.size()];
      l.toArray(pointsBords_[i]);
      l.clear();
    }
  }

  @Override
  public int getNombreNoeuds() {
    if (nbNoeuds_ < 0) {
      if (noeuds_ == null) {
        noeuds_ = maillage_.noeuds();
      }
      nbNoeuds_ = noeuds_.length;
    }
    return nbNoeuds_;
  }

  /**
   * @return le nb d'arete de contours
   */
  public int nombreAretesContours() {
    return pointsBords_.length;
  }

  @Override
  public boolean getBordPoint(final int _indexBord, final int _indexPoint, final GrPoint _p) {
    final GrPoint p = pointsBords_[_indexBord][_indexPoint];
    _p.initialiseAvec(p);
    return true;
  }

  /**
   *
   */
  @Override
  public GrBoite getDomaine() {
    final GrBoite r = new GrBoite();
    if (noeuds_ == null) {
      noeuds_ = maillage_.noeuds();
    }
    for (int i = getNombreNoeuds() - 1; i >= 0; i--) {
      r.ajuste(noeuds_[i].point_);
    }
    return r;
  }

  @Override
  public int getNombreBord() {
    return pointsBords_.length;
  }

  @Override
  public int getNombrePointPourBord(final int _i) {
    if ((_i >= 0) && (_i < pointsBords_.length)) {
      return pointsBords_[_i].length;
    }
    return 0;
  }

  @Override
  public int getNombrePolygones() {
    return maillage_.getNombre();
  }

  @Override
  public boolean point(final int _i, final GrPoint _p) {
    final GrPoint p = maillage_.noeud(_i).point_;
    _p.x_ = p.x_;
    _p.y_ = p.y_;
    _p.z_ = p.z_;
    return true;
  }

  @Override
  public boolean polygone(final int _i, final GrPolygone _poly) {
    final GrPolygone p = maillage_.element(_i).polygone();
    final int n = p.nombre();
    if (_poly.sommets_.nombre() != n) {
      _poly.sommets_ = new VecteurGrPoint(p.nombre());
    }
    for (int i = n - 1; i >= 0; i--) {
      GrPoint pt = _poly.sommet(i);
      if (pt == null) {
        pt = new GrPoint(p.sommet(i));
        _poly.sommets_.remplace(pt, i);
      } else {
        pt.initialiseAvec(p.sommet(i));
      }
    }
    return false;
  }
}