/*
 *  @creation     15 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import org.locationtech.jts.geom.CoordinateSequence;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ebli.calque.ZModeleGeometry;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public interface ZModeleEditable extends ZModeleGeometry {

  /**
   * pour �ventuellement avertir d'une modification.
   */
  void modificationWillBeDone();

  /**
   * @param _seq la sequence pour la nouvel objet
   * @param _analyze le receveur d'info
   * @return true si valide.
   */
  boolean isCoordinateValid(CoordinateSequence _seq, CtuluAnalyze _analyze);

  /**
   * V�rifie la conformit� de la g�om�trie avant cr�ation.
   *
   * @param _seq Les coordonn�es
   * @param _data Le transporteur de datas.
   * @param _ana Le container d'analyse. Un message d'erreur en fatalError est retourn� si probl�me sur les donn�es.
   * @return True si la g�om�trie est valide.
   */
  public boolean isDataValid(CoordinateSequence _seq, ZEditionAttributesDataI _data, CtuluAnalyze _ana);

  public void modificationDone();
}
