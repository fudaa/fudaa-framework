/**
 *  @file         TrFindComponent.java
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2006-07-13 13:35:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.find;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;

/**
 * @author deniger
 * @version $Id: CalqueFindActionDefault.java,v 1.1 2005/09/22 08:46:50 deniger
 *          Exp $
 */
public class CalqueFindActionDefault extends CalqueFindActionAbstract {

  public CalqueFindActionDefault(final ZCalqueAffichageDonnees _item) {
    super(_item);
  }



  @Override
  protected boolean changeSelection(final CtuluListSelection _l, final int _selOption, final String _action) {
    return ((ZCalqueAffichageDonnees) layer_).changeSelection(_l, _selOption);
  }

}