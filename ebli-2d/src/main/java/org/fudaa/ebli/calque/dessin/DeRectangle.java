/*
 * @file         DeRectangle.java
 * @creation     1998-08-31
 * @modification $Date: 2006-07-13 13:35:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Graphics2D;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.trace.TraceGeometrie;
/**
 * Un rectangle.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-07-13 13:35:48 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class DeRectangle extends DePolygone {
  // donnees membres publiques
  // donnees membres privees
  // Constructeurs
  public DeRectangle() {
    super();
  }
  public DeRectangle(final GrPolyligne _l) {
    super(_l);
  }
  @Override
  public int getForme() {
    return RECTANGLE;
  }
  public DeRectangle(final GrPoint _coin, final GrVecteur _largeur, final GrVecteur _hauteur) {
    super();
    ajoute(_coin);
    ajoute(_coin.addition(_largeur));
    ajoute(_coin.addition(_largeur).addition(_hauteur));
    ajoute(_coin.addition(_hauteur));
  }
  // Methodes publiques
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
  }
}
