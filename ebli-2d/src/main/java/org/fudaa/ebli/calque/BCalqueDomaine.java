/*
 * @file         BCalqueDomaine.java
 * @creation     1999-02-08
 * @modification $Date: 2006-09-19 14:55:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Polygon;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
/**
 * Un calque d'affichage du domaine.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 14:55:45 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BCalqueDomaine extends BCalqueAffichage {
  public BCalqueDomaine() {
    super();
    setDestructible(false);
    setForeground(new Color(128, 160, 192));
    setBackground(new Color(224, 240, 255));
  }
  // Icon
  /**
   * Dessin de l'icone.
   * @param _c composant dont l'icone peut deriver des proprietes
   * (couleur, ...). Ce parametre peut etre <I>null</I>. Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final int w= getIconWidth();
    final int h= getIconHeight();
    Color bg= getBackground();
    if (isAttenue()) {
      bg= attenueCouleur(bg);
    }
    _g.setColor(bg);
    _g.fillRect(_x + 3, _y + 3, w - 6, h - 6);
    Color fg= getForeground();
    if (isAttenue()) {
      fg= attenueCouleur(fg);
    }
    _g.setColor(fg);
    _g.drawRect(_x + 3, _y + 3, w - 6, h - 6);
  }
  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    BCalque p= this;
    while ((p.getParent() != null) && (p.getParent() instanceof BCalque)) {
      p = (BCalque) p.getParent();
    }
    final GrBoite boite= p.getDomaine();
    if (boite == null) {
      return;
    }
    final double xmin= boite.o_.x_;
    final double ymin= boite.o_.y_;
    final double xmax= boite.e_.x_;
    final double ymax= boite.e_.y_;
    final GrMorphisme versEcran= getVersEcran();
    final GrPolygone pr= new GrPolygone();
    pr.sommets_.ajoute(new GrPoint(xmin, ymin, 0.));
    pr.sommets_.ajoute(new GrPoint(xmin, ymax, 0.));
    pr.sommets_.ajoute(new GrPoint(xmax, ymax, 0.));
    pr.sommets_.ajoute(new GrPoint(xmax, ymin, 0.));
    final Polygon pe= pr.applique(versEcran).polygon();
    final boolean attenue= isAttenue();
    final boolean rapide= isRapide();
    if (!rapide) {
      Color bg= getBackground();
      if (isAttenue()) {
        bg= attenueCouleur(bg);
      }
      _g.setColor(bg);
      _g.fillPolygon(pe);
    }
    Color fg= getForeground();
    if (attenue) {
      fg= attenueCouleur(fg);
    }
    _g.setColor(fg);
    _g.drawPolygon(pe);
    super.paintComponent(_g);
  }
}
