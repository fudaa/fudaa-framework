/*
 *  @creation     5 avr. 2005
 *  @modification $Date: 2008-02-21 15:18:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuToolButton;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliFormatterInterface;

/**
 * Un panneau pour saisir les coordonnées de déplacement d'objets GIS. 
 * Ce panneau est intégré dans la palette des outils lorsque l'outil "déplacement" est sélectionné.
 * 
 * @author Fred Deniger
 * @version $Id: ZCalqueDeplacementPanel.java,v 1.6.8.2 2008-02-21 15:18:29 bmarchan Exp $
 */
public class ZCalqueDeplacementPanel extends JPanel implements ZEditorPanelInterface,
    ZCalqueDeplacementInteraction.SpecPanel, ActionListener {

  JButton btApply_;

  final ZCalqueDeplacementInteraction deplacement_;

  EbliFormatterInterface formatter_;

  BuTextField tfDx_;
  BuTextField tfDy_;
  BuTextField tfDz_;
  BuLabel lbDz_;
  boolean bZ_=false;

  /**
   * @param _deplacement le calque de deplacement
   * @param _formatter
   */
  public ZCalqueDeplacementPanel(final ZCalqueDeplacementInteraction _deplacement,
      final EbliFormatterInterface _formatter) {
    super();
    
    deplacement_ = _deplacement;
    deplacement_.specPanel_ = this;
    formatter_ = _formatter;
    
    tfDx_ = BuTextField.createDoubleField();
    tfDx_.setSize(new Dimension(50,tfDx_.getPreferredSize().height));
    tfDx_.setValue(new Double(0));
    tfDy_ = BuTextField.createDoubleField();
    tfDy_.setValue(new Double(0));
    tfDz_ = BuTextField.createDoubleField();
    tfDz_.setValue(new Double(0));
    lbDz_=new BuLabel("dz:");
    
    btApply_ = new BuToolButton(BuResource.BU.getToolIcon("appliquer"));
    btApply_.addActionListener(this);

    BuPanel pnMain=new BuPanel();
    pnMain.setLayout(new BuGridLayout(2,2,2));
    pnMain.add(new BuLabel("dx:"));
    pnMain.add(tfDx_);
    pnMain.add(new BuLabel("dy:"));
    pnMain.add(tfDy_);
    pnMain.add(lbDz_);
    pnMain.add(tfDz_);
    
    BuPanel pnButtons=new BuPanel();
    pnButtons.setLayout(new BorderLayout(5,5));
    pnButtons.add(btApply_,BorderLayout.SOUTH);

    setLayout(new BorderLayout(5,5));
    add(pnMain,BorderLayout.CENTER);
    add(pnButtons,BorderLayout.EAST);

    setName("pnMove");
  }

  @Override
  public void actionPerformed(final ActionEvent _e){
    if (btApply_.isEnabled() && btApply_ == _e.getSource()) {
      Double dx = (Double) tfDx_.getValue();
      Double dy = (Double) tfDy_.getValue();
      Double dz=new Double(0);
      if (tfDz_.isEnabled()) dz=(Double) tfDz_.getValue();
      if (dx != null && dy != null && dz!=null) {
        deplacement_.getTarget().moved(dx.doubleValue(), dy.doubleValue(), dz.doubleValue(),false);
      }

    }
  }

  @Override
  public void atomicChanged(){}

  @Override
  public void calqueInteractionChanged(){}

  @Override
  public void close(){
    if (FuLog.isTrace()) {
      FuLog.trace("EBL: " + getClass().getName() + " close");
    }
    deplacement_.specPanel_ = null;
  }

  @Override
  public void editionStopped() {}

  @Override
  public ZEditionAttibutesContainer getAttributeContainer(){
    return null;
  }

  @Override
  public JComponent getComponent(){
    return this;
  }

  @Override
  public void objectAdded(){
    atomicChanged();
  }
  
  @Override
  public void targetChanged(final ZCalqueEditable _target) {
    bZ_= (_target != null && _target.getModelEditable() != null && _target.getModelEditable().getGeomData().getAttributeIsZ() != null);
    lbDz_.setEnabled(bZ_);
    tfDz_.setEnabled(bZ_);
  }

  @Override
  public void updateState(){
    if (deplacement_.isDragged()) {
      tfDx_.setEditable(false);
      tfDy_.setEditable(false);
      tfDz_.setEditable(false);
      btApply_.setEnabled(false);
      tfDx_.setText(formatter_.getXYFormatter().format(deplacement_.getReelDx()));
      tfDy_.setText(formatter_.getXYFormatter().format(deplacement_.getReelDy()));
    }
    else {
      if (!tfDx_.isEditable()) {
        tfDx_.setText(CtuluLibString.ZERO);
        tfDy_.setText(CtuluLibString.ZERO);
      }
      tfDx_.setEditable(true);
      tfDy_.setEditable(true);
      tfDz_.setEditable(bZ_);
      btApply_.setEnabled(!deplacement_.isSelectionEmpty());
    }

  }

  /* (non-Javadoc)
   * @see org.fudaa.ebli.calque.edition.ZEditorPanelInterface#pointMove(double, double)
   */
  @Override
  public void pointMove(double _x, double _y) {
    // TODO Auto-generated method stub
    
  }
}
