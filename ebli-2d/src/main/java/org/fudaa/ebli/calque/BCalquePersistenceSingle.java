/*
 * @creation 31 ao�t 06
 * @modification $Date: 2008-05-13 12:10:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;
import org.fudaa.ctulu.CtuluArkLoader;
import org.fudaa.ctulu.CtuluArkSaver;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluXmlReaderHelper;
import org.fudaa.ctulu.CtuluXmlWriter;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ebli.commun.EbliUIProperties;

/**
 * Une impl�mentation g�n�raliste de l'interface pour g�rer la persistance d'un calque ou d'un groupe de
 * calques.
 * 
 * @author fred deniger
 * @version $Id: BCalquePersistenceSingle.java,v 1.4.8.3 2008-05-13 12:10:38 bmarchan Exp $
 */
public class BCalquePersistenceSingle implements BCalquePersistenceInterface {

  public BCalquePersistenceSingle() {
    super();
  }

  public void readXml(final InputStream _reader, final BCalqueSaverInterface _target) {
    final CtuluXmlReaderHelper helper = CtuluXmlReaderHelper.createReader(_reader);
    if (helper == null) {
      return;
    }

    final EbliUIProperties ui = _target.getUI();
    final String title = helper.getTextFor("title");
    if (title != null) {
      ui.setTitle(title);
    }
    readXmlSpecific(helper, _target);

  }

  public void readXmlSpecific(final CtuluXmlReaderHelper _helper, final BCalqueSaverInterface _target) {}

  @Override
  public String restoreFrom(final CtuluArkLoader _loader, final BCalqueSaverTargetInterface _parentPanel,
      final BCalque _parentCalque, final String _parentDirEntry, final String _entryName,
      final ProgressionInterface _proj) {
    if (_entryName == null) {
      return null;
    }
    BCalqueSaverInterface saver = (BCalqueSaverInterface) _loader.getDbDataFor(_parentDirEntry, _entryName);
    if (saver == null) {
      saver = new BCalqueSaverSingle();
    }
    InputStream in = _loader.getReader(_parentDirEntry, getLayerPropEntry(_entryName));
    if (in != null) {
      final Properties props = new Properties();
      try {
        props.load(in);
        final String name = props.getProperty("name");
        if (name != null) {
          saver.getUI().setLayerName(name);
        }
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
    }
    in = _loader.getReader(_parentDirEntry, _entryName + CtuluArkSaver.DESC_EXT);
    if (in != null) {
      readXml(in, saver);
    }
    // BCalque cq = findCalque(saver, _parentPanel, _parentCalque);
    if (restoreFromSpecific(saver, _loader, _parentPanel, _parentCalque, _parentDirEntry, _entryName, _proj)) {
      return restore(
          saver, _parentPanel, _parentCalque, _proj);
    }
    return null;
  }

  /**
   * @param _saver
   * @param _loader
   * @param _parentPanel
   * @param _parentCalque
   * @param _parentDirEntry
   * @param _entryName
   * @param _proj
   * @return true si ok
   */
  protected boolean restoreFromSpecific(final BCalqueSaverInterface _saver, final CtuluArkLoader _loader,
      final BCalqueSaverTargetInterface _parentPanel, final BCalque _parentCalque, final String _parentDirEntry,
      final String _entryName, final ProgressionInterface _proj) {
    return true;

  }

  @Override
  public BCalqueSaverInterface save(final BCalque _cq, final ProgressionInterface _prog) {
    final BCalqueSaverSingle saver = new BCalqueSaverSingle(_cq);
    if (!getClass().equals(BCalquePersistenceSingle.class)) {
      saver.setPersistenceClassFrom(this);
    }
    return saver;
  }

  /**
   * Sauvegarde standard d'un calque. Les propri�t�s du calque (fond, largeur de ligne, etc)
   * sont sauvegard�es ainsi que les informations du calque (nom, etc). Sur entry .props et xml.
   */
  @Override
  public BCalqueSaverInterface saveIn(final BCalque _cqToSave, final CtuluArkSaver _saver,
      final String _parentDirEntry, final String _parentDirIndice) {
    if (_saver == null) {
      return null;
    }
    final String id = _saver.getNextIdForDir(_parentDirIndice);
    final String entry = getEntryBase(_cqToSave, _parentDirEntry, id);

    writeLayerInXmlEntry(_cqToSave, _saver, entry);
    final EbliUIProperties ui = createPropForArk(_cqToSave);

    return new BCalqueSaverSingle(ui, id);
  }

  protected EbliUIProperties createPropForArk(final BCalque _cqToSave) {
    return _cqToSave.saveUIProperties();
  }

  /**
   * Retrouve le calque de nom donn� _cqName.getLayerName() dans la hierarchie des calques, � partir du calque _parent.<p>
   * Cette m�thode peut �tre surcharg�e pour recr�er les calques au fur et a mesure de leur lecture sur le fichier projet. En effet,
   * la hierarchie des calques peut �tre immuable, fix�e par l'application, ou d�pendre du projet pour les applications qui
   * supportent l'ajout ou la suppression de calques par l'utilisateur.
   * 
   * @param _cqName L'interface de transport des infos li�es au calque � retrouver.
   * @param _pn Le panneau contenant les calques.
   * @param _parent Le calque parent
   * @return Le calque retrouv�, null si rien trouv�.
   */
  protected BCalque findCalque(final BCalqueSaverInterface _cqName, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent) {
    return _parent == null ? null : _parent.getCalqueParNom(_cqName.getLayerName());
  }

  @Override
  public String restore(final BCalqueSaverInterface _saver, final BCalqueSaverTargetInterface _pn,
      final BCalque _parent, final ProgressionInterface _prog) {
    if (_saver != null) {
      final BCalque target = findCalque(_saver, _pn, _parent);
      if (target != null) {
        target.initFrom(_saver.getUI());
        return target.getName();
      }
    }
    return null;
  }

  protected String getEntryBase(final BCalque _cqToSave, final String _parentEntry, final String _id) {
    return _parentEntry + _id + FuLib.clean(_cqToSave.getTitle());
  }

  protected void writeHeaderData(final CtuluXmlWriter _out, final BCalque _cqToSave) throws IOException {}

  protected void writeBodyData(final CtuluXmlWriter _out, final BCalque _cqToSave) throws IOException {}

  public String getLayerPropEntry(final String _entry) {
    return _entry + ".props";
  }

  protected final void writeLayerInXmlEntry(final BCalque _cqToSave, final CtuluArkSaver _saver, final String _entry) {
    try {
      _saver.startEntry(_entry + CtuluArkSaver.DESC_EXT);
      final CtuluXmlWriter out = new CtuluXmlWriter(_saver.getOutStream());
      out.write("<layer>");
      // TODO a continuer

//      out.writeEntry("name", _cqToSave.getName());
      writeHeaderData(out, _cqToSave);
      out
          .write("<!--Vous pouvez modifier la suite\nAttention � l'encodage UTF-8 utiliser un �diteur apropri�\nInfo:\n le caract�re '&' est remplac� par &amp;'\n\t \">\" par '&gt;'\n\t\"<\" par '&lt;' -->");
      out
          .write("<!--You can edit following entries\n Warning: the encoding used is UTF-8. Please, use a text editor that support this encoding\n"
              + "Info:\n the char '&' must be replaced by &amp;'\n\t\">\" by '&gt;'\n \t\"<\" by '&lt;' -->");
      out.writeEntry("title", _cqToSave.getTitle());
      writeBodyData(out, _cqToSave);
      out.write("\n</layer>");
      out.flush();
      final String cqData = getLayerPropEntry(_entry);
      _saver.startEntry(cqData);
      //ce flux ne sera pas ferme car il ne faut pas fermer le flux _saver.getOutStream() 
      final Writer propWriter = new OutputStreamWriter(_saver.getOutStream());
      propWriter.write("#NE PAS EDITER");
      propWriter.write(CtuluLibString.LINE_SEP);
      propWriter.write("#DO NOT EDIT");
      propWriter.write(CtuluLibString.LINE_SEP);
      propWriter.write("name" + "= " + _cqToSave.getName());
      propWriter.write(CtuluLibString.LINE_SEP);
      if (!getClass().equals(BCalquePersistenceSingle.class)) {
        propWriter.write(BCalqueSaverSingle.getPersistanceClassId() + "= " + getClass().getName());
      }
      propWriter.flush();

    } catch (final IOException _evt) {
      FuLog.warning(_evt);

    }
  }
}
