/*
 * @creation     20 janv. 08
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuTable;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionGeometry;
import org.fudaa.ctulu.gis.GISZoneListenerDispatcher;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZModelGeometryListener;
import org.fudaa.ebli.calque.ZModeleDonnesAbstract;
import org.fudaa.ebli.calque.ZModeleGeometry;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPaletteInfo.ModifyPropertyInfo;

/**
 * Une impl�mentation par defaut d'un mod�le contenant des g�om�tries de tous types.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZModeleGeometryDefault extends ZModeleDonnesAbstract implements ZModeleGeometry, ModifyPropertyInfo {

  protected GISZoneCollectionGeometry geometries_;

  /**
   * Constructeur
   */
  public ZModeleGeometryDefault() {
    this(new GISZoneCollectionGeometry());
  }

  public void modificationWillBeDone() {
  }

  public void modificationDone() {
  }

  public ZModeleGeometryDefault(GISZoneCollectionGeometry _zone) {
    setGeometries(_zone);

  }

  /**
   * Called from constructor !
   *
   * @param geometries
   */
  public void setGeometries(GISZoneCollectionGeometry geometries) {
    if (geometries_ != null) {
      if (geometries_.getListener() instanceof GISZoneListenerDispatcher) {
        ((GISZoneListenerDispatcher) geometries_.getListener()).removeListener(this);
      } else if (geometries_.getListener() == this) {
        geometries_.setListener(null);
      }

    }
    geometries_ = geometries;
    if (geometries_ == null) {
      return;
    }
    if (geometries_.getListener() instanceof GISZoneListenerDispatcher) {
      ((GISZoneListenerDispatcher) geometries_.getListener()).addListener(this);
    } else {
      geometries_.setListener(this);
    }
  }

  @Override
  public void getDomaineForGeometry(int _idxGeom, GrBoite _target) {
    if (geometries_ == null || geometries_.getNumGeometries() == 0 || !isGeometryVisible(_idxGeom)) {
      return;
    }
    final Geometry g = geometries_.getGeometry(_idxGeom);
    final Envelope e = g.getEnvelopeInternal();
    if (_target.o_ == null) {
      _target.o_ = new GrPoint();
    }
    if (_target.e_ == null) {
      _target.e_ = new GrPoint();
    }
    if (e != null && !e.isNull()) {
      _target.o_.x_ = e.getMinX();
      _target.o_.y_ = e.getMinY();
      _target.e_.x_ = e.getMaxX();
      _target.e_.y_ = e.getMaxY();
    }
  }

  @Override
  public int getNbPointForGeometry(int _idxGeom) {
    if (geometries_ == null) {
      return 0;
    }
    final Geometry gi = geometries_.getGeometry(_idxGeom);
    return gi.getNumPoints();
  }

  @Override
  public boolean isGeometryReliee(final int _idxGeom) {
    Geometry g = getGeomData().getGeometry(_idxGeom);
    return (g instanceof LineString);
  }

  @Override
  public boolean isGeometryFermee(final int _idxGeom) {
    Geometry g = getGeomData().getGeometry(_idxGeom);
    return (g instanceof LineString && ((LineString) g).isClosed());
  }

  @Override
  public boolean isGeometryVisible(int _idxGeom) {
    int iattVisibility = geometries_.getIndiceOf(GISAttributeConstants.VISIBILITE);
    if (iattVisibility == -1) {
      return true;
    }

    return GISAttributeConstants.ATT_VAL_TRUE.equals(geometries_.getValue(iattVisibility, _idxGeom));
  }

  @Override
  public boolean point(GrPoint _pt, int _idxGeom, int _pointIdx) {
    // Pour des multipoints, on s'appuie sur la m�thode GISMultiPoint.getCoordinateSequence() cr��e pour l'occasion et non 
    // GISMultiPoint.getCoordinates() pour r�cuperer les coordonn�es d'un point. En effet, recup�rer le tableau des coordonn�es
    // s'av�re terriblement penalisant pour l'affichage.
    final CoordinateSequence g = ((GISCoordinateSequenceContainerInterface) geometries_.getGeometry(_idxGeom)).getCoordinateSequence();
    _pt.x_ = g.getX(_pointIdx);
    _pt.y_ = g.getY(_pointIdx);
    if(g.getDimension()==3) {
      _pt.z_ = g.getOrdinate(_pointIdx, 2);
    }else{
      _pt.z_ =0;
    }
    // B.M. Ne devrait pas se produire, cela devrait �tre fait en amont, a la cr�ation de la g�om�trie.
    if (Double.isNaN(_pt.z_)) {
      _pt.z_ = 0;
    }

    return true;
  }

  @Override
  public GISZoneCollection getGeomData() {
    return geometries_;
  }

  @Override
  public void prepareExport() {
    geometries_.prepareExport();
  }

  @Override
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer) {
    return null;
  }

  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
    // Selection en mode geom�trie
    if (_layer.getLayerSelection() != null && _layer.getLayerSelection().getNbSelectedIndex() > 0) {
      if (_layer.getLayerSelection().getNbSelectedIndex() == 1) {
        _d.setOnlyOneGeometry(true);
        _d.setTitle(EbliLib.getS("G�om�trie"));
        // Informations sur le calque
        _d.put(EbliLib.getS("Calque"), _layer.getTitle());
        // Ajout des attributs et des valeurs de la g�om�trie selectionn�e
        int geometrySelected = _layer.getLayerSelection().getSelectedIndex()[0];
        for (int i = 0; i < geometries_.getNbAttributes(); i++) {
          GISAttributeInterface attr = geometries_.getAttribute(i);
          // Si l'attribut nom est d�fini, il est utilis� pour le titre et
          // remplace celui pr�c�dement d�fini
          if (attr == GISAttributeConstants.TITRE) {
            _d.setTitle(EbliLib.getS("Objet") + " : " + geometries_.getValue(i, geometrySelected).toString());
          }
          // Ajout de l'information au tableau
          if (!attr.isAtomicValue() && attr.isUserVisible()) {
            if (attr.isEditable()) {
              _d.put(attr.getLongName(), geometries_.getValue(i, geometrySelected), attr.getEditor(),
                      null, this, new int[]{geometrySelected});
            } else {
              _d.put(attr.getLongName(), attr.getEditor().toString(geometries_.getValue(i, geometrySelected)));
            }
          }
        }
      } else {
        _d.setOnlyOneGeometry(false);
        // Informations sur les g�om�tries selectionn�es
        _d.setTitle(EbliLib.getS("S�lection multiple"));
        // Informations sur le calque
        _d.put(EbliLib.getS("Calque"), _layer.getTitle());
        // Ajout des attributs et des valeurs de la g�om�trie selectionn�e
        int[] geometriesSelected = _layer.getLayerSelection().getSelectedIndex();
        for (int i = 0; i < geometries_.getNbAttributes(); i++) {
          GISAttributeInterface attr = geometries_.getAttribute(i);
          // Ajout de l'information au tableau
          if (!attr.isAtomicValue() && attr.isUserVisible()) {
            // D�termine la valeur commune aux g�om�tries selectionn�es
            Object value;
            boolean sameValues = true;
            int j = 0;
            while (sameValues && ++j < geometriesSelected.length) {
              sameValues = geometries_.getValue(i, geometriesSelected[j - 1]).equals(
                      geometries_.getValue(i, geometriesSelected[j]));
            }
            if (sameValues) {
              value = geometries_.getValue(i, geometriesSelected[0]);
            } else {
              value = null;
            }
            // Ajout de cette valeur
            if (attr.isEditable()) {
              TableCellRenderer renderer = new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                        int row, int column) {
                  if (value == null) {
                    value = "<" + EbliLib.getS("Mixte") + ">";
                  }
                  return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
              };
              _d.put(attr.getLongName(), value, attr.getEditor(), renderer, this, geometriesSelected);
            } else {
              _d.put(attr.getLongName(), value == null ? "<" + EbliLib.getS("Mixte") + ">" : value.toString());
            }
          }
        }
      }
    } // Selection en mode sommet
    else if (_layer.getLayerSelectionMulti() != null && _layer.getLayerSelectionMulti().getNbSelectedItem() > 0) {
      if (_layer.getLayerSelectionMulti().getNbSelectedItem() == 1) {
        _d.setTitle(EbliLib.getS("Sommet"));
        // Informations sur le calque
        _d.put(EbliLib.getS("Calque"), _layer.getTitle());
      } else {
        // Informations sur les g�om�tries selectionn�es
        _d.setTitle(EbliLib.getS("Calque") + " : " + _layer.getTitle());
        // Informations sur le calque
        _d.put(EbliLib.getS("Calque"), _layer.getTitle());
      }
    } // Pas de selection
    else // Informations sur le calque
    {
      _d.setTitle(EbliLib.getS("Calque") + " : " + _layer.getTitle());
    }
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.palette.BPaletteInfo.ModifyPropertyInfo#modifyProperty(java.lang.String, java.lang.Object)
   */
  @Override
  public void modifyProperty(String _key, Object _value, int[] _index, CtuluCommandContainer _cmd) {
    if (_index != null) {
      // Recherche de l'attribut d�fini par _key
      boolean found = false;
      int i = -1;
      while (!found && ++i < geometries_.getNbAttributes()) {
        found = geometries_.getAttribute(i).getLongName().equals(_key);
      }
      // Modification de cet attribut
      if (found) {
        int[] geometries = _index;
        for (int j = 0; j < geometries.length; j++) {
          geometries_.getModel(i).setObject(geometries[j], _value, _cmd);
        }
      }
    }
  }

  @Override
  public GrBoite getDomaine() {
    if (geometries_ == null || geometries_.getNumGeometries() == 0) {
      return null;
    }
    GrBoite bt = null;
    for (int i = geometries_.getNumGeometries() - 1; i >= 0; i--) {
      if (isGeometryVisible(i)) {
        final Geometry g = geometries_.getGeometry(i);
        final Envelope e = g.getEnvelopeInternal();
        if (bt == null) {
          bt = new GrBoite();
        }
        bt.ajuste(e);
      }
    }
    return bt;
//    final Envelope e = geometries_.getEnvelopeInternal();
//    if (e == null) {
//      return null;
//    }
//    return new GrBoite(new GrPoint(e.getMinX(), e.getMinY(), 0), new GrPoint(e.getMaxX(), e.getMaxY(), 0));
  }

  @Override
  public int getNombre() {
    return geometries_ == null ? 0 : geometries_.getNumGeometries();
  }

  @Override
  public Object getObject(final int _ind) {
    return geometries_ == null ? null : geometries_.getGeometry(_ind);
  }

  @Override
  public boolean isValuesTableAvailable() {
    return false;
  }

  @Override
  public void objectAction(Object _source, int _idx, Object _obj, int _action) {
    fireModelAction(_idx, (Geometry) _obj, _action);
  }

  protected void fireAttributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
    for (int i = 0; i < listeners_.size(); i++) {
      if (listeners_.get(i) instanceof ZModelGeometryListener) {
        ((ZModelGeometryListener) listeners_.get(i)).attributeAction(_source, att, _att, _action);
      }
    }
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
    fireAttributeAction(_source, _indexAtt, _att, _action);
  }

  protected void fireAttributeValueChangeAction(Object _source, int _idxAtt, GISAttributeInterface _att, int _indexGeom,
          Object _newValue) {
    for (int i = 0; i < listeners_.size(); i++) {
      if (listeners_.get(i) instanceof ZModelGeometryListener) {
        ((ZModelGeometryListener) listeners_.get(i)).attributeValueChangeAction(this, _idxAtt, _att, _indexGeom, _newValue);
      }
    }
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexGeom,
          Object _newValue) {
    fireAttributeValueChangeAction(_source, _indexAtt, _att, _indexGeom, _newValue);
  }

  @Override
  public void addModelListener(ZModelGeometryListener _listener) {
    if (_listener != null && !listeners_.contains(_listener)) {
      listeners_.add(_listener);
    }
  }

  @Override
  public void removeModelListener(ZModelGeometryListener _listener) {
    if (listeners_.contains(_listener)) {
      listeners_.remove(_listener);
    }
  }

  @Override
  public GrPoint getVertexForObject(int _ind, int _idVertex) {
    getGeomData().initZCoordinate(_ind);
    GrPoint pt = new GrPoint();
    point(pt, _ind, _idVertex);
    return pt;
  }

  /**
   * @return the geometries_
   */
  public GISZoneCollectionGeometry getGeometries() {
    return geometries_;
  }
}
