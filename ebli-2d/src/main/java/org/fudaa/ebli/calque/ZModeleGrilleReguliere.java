/**
 *  @creation     4 oct. 2004
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;


/**
 * @author Fred Deniger
 * @version $Id: ZModeleGrilleReguliere.java,v 1.4 2006-04-12 15:27:09 deniger Exp $
 */
public interface ZModeleGrilleReguliere extends ZModelePoint{
  
  /**
   * @return le x du point nord-ouest
   */
  double getXMin();
  /**
   * @return le y du point nord-ouest
   */
  double getYMin();
  
  double getXMax();
  double getYMax();
  
  /**
   * @return l'ecart entre 2 lignes
   */
  double getDX();
  /**
   * @return l'ecart entre 2 colonnes
   */
  double getDY();
  
  /**
   * @return le nombre de point sur X
   */
  int getNbPtOnX();
  /**
   * @return le nombre de point sur Y.
   */
  int getNbPtOnY();

}
