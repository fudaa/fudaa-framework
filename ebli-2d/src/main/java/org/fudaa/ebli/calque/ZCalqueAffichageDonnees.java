/**
 * @creation 2002-08-27
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.calque.find.CalqueFindActionDefault;
import org.fudaa.ebli.calque.find.CalqueFindExpression;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.find.EbliFindActionInterface;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Un calque de base pour tous les nouveaux calques bases sur les modeles. Ce calque permet de definir en plus si
 * l'affichage doit etre total,inutile ou seulement la selection.
 * 
 * @version $Id$
 * @author Fred Deniger
 */
public abstract class ZCalqueAffichageDonnees extends ZCalqueAffichageDonneesAbstract {

  protected CtuluListSelection selection_;

  public ZCalqueAffichageDonnees() {
    painted_ = true;
  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new CalqueFindExpression(modeleDonnees());
  }

  @Override
  public EbliFindActionInterface getFinder() {
    return new CalqueFindActionDefault(this);
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return getSelectedIndex();
  }

  /**
   * Renvoie une instance de EbliListeSelection initialisee avec comme taille la moitie du nombre maxi de donnees.
   */
  protected CtuluListSelection creeSelection() {
    return new CtuluListSelection(modeleDonnees().getNombre() / 2 + 1);
  }

  /**
   * Initialise la liste des selections.
   */
  public void initSelection() {
    if (selection_ == null) {
      if(modeleDonnees()!=null)
        selection_ = new CtuluListSelection(modeleDonnees().getNombre() / 2 + 1);
      else
        selection_ = new CtuluListSelection(0);
      // selection_.addListeSelectionListener(this);
    }
  }

  /**
   * Methode appelee pour mettre a jour la selection du calque.
   * 
   * @param _s la liste des index selectionnes.
   * @param _action : l'action voulue
   */
  public boolean changeSelection(final CtuluListSelection _s, final int _action) {
    // Si la selection de modif est nulle, seule l'action de remplacement
    // est concernee.
    if (selection_ == null) {
      initSelection();
    }
    boolean sentEvent = false;
    switch (_action) {
    case EbliSelectionState.ACTION_ADD:
      sentEvent = selection_.add(_s);
      break;
    case EbliSelectionState.ACTION_DEL:
      sentEvent = selection_.remove(_s);
      break;
    case EbliSelectionState.ACTION_XOR:
      sentEvent = selection_.xor(_s);
      break;
    case EbliSelectionState.ACTION_REPLACE:
      sentEvent = selection_.setSelection(_s);
      break;
    case EbliSelectionState.ACTION_AND:
      sentEvent = selection_.intersection(_s);
      break;
    default:
      break;
    }
    if (sentEvent) {
      fireSelectionEvent();
    }
    return sentEvent;
  }

  /**
   * Modifie la selection de ce calque avec la selection ponctuelle determinee par <code>_pt</code>. Envoie un
   * evenement si la selection est modifiee et renvoie <code>true</code> si un objet de ce calque est selectionne par
   * ce point.
   * 
   * @param _pt le point de selection
   * @param _action l'action a effectuer sur la selection de ce calque.
   * @param _tolerancePixel la tolerance pour la selection
   * @return true si un objet de ce calque est selection par le point.
   */
  @Override
  public boolean changeSelection(final GrPoint _pt, final int _tolerancePixel, final int _action) {
    if (!isSelectable()) return false;
    
    final CtuluListSelection l = selection(_pt, _tolerancePixel);
    changeSelection(l, _action);
    if ((l == null) || (l.isEmpty())) {
      return false;
    }
    return true;
  }

  /**
   * Modifie la selection de ce calque a partir de la nouvelle selection polygoniale <code>_poly</code> et de l'action
   * <code>_action</code>. Renvoie <code>true</code> si des objets de ce calque sont selectionnes par le polygone
   * et envoie un evenement si la selection de ce calque a ete modifiee.
   * 
   * @param _action l'action a appliquer ( voir ZCalqueSelectionInteraction)
   * @param _poly le polygone de selection
   * @return true si des objets ont ete selectionnes par le polygone
   */
  @Override
  public boolean changeSelection(final LinearRing _poly, final int _action, final int _mode) {
    if (!isSelectable()) return false;
    
    final CtuluListSelection l = selection(_poly, _mode);
    changeSelection(l, _action);
    if ((l == null) || (l.isEmpty())) {
      return false;
    }
    return true;
  }

  @Override
  public boolean changeSelection(final LinearRing[] _p, final int _action, final int _mode) {
    if (!isSelectable()) return false;
    
    if (_p == null) {
      return false;
    }
    final CtuluListSelection l = new CtuluListSelection();
    for (int i = _p.length - 1; i >= 0; i--) {
      final CtuluListSelection li = selection(_p[i], _mode);
      l.add(li);
    }
    changeSelection(l, _action);
    if (l.isEmpty()) {
      return false;
    }
    return true;
  }

  @Override
  public void clearSelection() {
    if (!isSelectable()) return;
    
    if ((selection_ != null) && (!selection_.isEmpty())) {
      selection_.clear();
      fireSelectionEvent();
    }
  }

  @Override
  public CtuluListSelectionInterface getLayerSelection() {
    return selection_;
  }

  /**
   * @return le nombre d'object s�lectionne par ce calque
   */
  public int getNbSelected() {
    return (selection_ == null) ? 0 : selection_.getNbSelectedIndex();
  }

  public int[] getSelectedIndex() {
    return selection_ == null ? null : selection_.getSelectedIndex();
  }

  @Override
  public void inverseSelection() {
    if (!isSelectable()) return;
    
    if (isSelectionEmpty()) {
      return;
    }
    initSelection();
    selection_.inverse(modeleDonnees().getNombre());
    fireSelectionEvent();
  }

  public boolean isCouleurModifiable() {
    return true;
  }

  /**
   * @return true si un seul objet est selectionne
   */
  @Override
  public boolean isOnlyOneObjectSelected() {
    return selection_ != null && selection_.isOnlyOnIndexSelected();
  }

  @Override
  public boolean isSelectionEmpty() {
    if (selection_ == null) {
      return true;
    }
    return selection_.isEmpty();
  }

  @Override
  public void selectAll() {
    if (!isVisible() || !isSelectable()) return;
    initSelection();
    selection_.addInterval(0, modeleDonnees().getNombre() - 1);
    fireSelectionEvent();
  }

  /**
   * Renvoie la liste des objets selectionnees pour le point <code>_pt</code> avec pour tolerance
   * <code>_tolerance</code>.
   * 
   * @param _pt coordonnees reelles
   * @param _tolerance
   * @return la liste des index des objets selectionnes (ou null si aucune selection)
   */
  public abstract CtuluListSelection selection(GrPoint _pt, int _tolerance);

  /**
   * renvoie la liste des objets selectionnes par le polygone <code>_poly</code>.
   * 
   * @param _poly le polygone EN coordonnees reelles
   * @param _mode TODO
   * @return la liste des indexs des objets selectionn�s ( ou null si aucun selectionne).
   */
  public abstract CtuluListSelection selection(LinearRing _poly, int _mode);

  /**
   * Selectionne les objets d'indice donn�s.
   * @param _idx Les indices a selectionner.
   * @return true si une selection a �t� effectu�e.
   */
  @Override
  public boolean setSelection(final int[] _idx) {
    if (!isSelectable()) return false;
    
    if (_idx == null) {
      return false;
    }
    if (selection_ == null) {
      selection_ = creeSelection();
    } else {
      selection_.clear();
    }
    final int max = modeleDonnees().getNombre();
    boolean r = false;
    for (int i = _idx.length - 1; i >= 0; i--) {
      final int idx = _idx[i];
      if (idx >= 0 && idx < max) {
        selection_.add(_idx[i]);
        r = true;
      }
    }
    fireSelectionEvent();
    return r;
  }
  
  @Override
  public boolean changeSelection(final EbliListeSelectionMultiInterface _selection, int _action) {
    return changeSelection(_selection.getIdxSelection(),_action);
  }
  
  /**
   * Selectionne l'objet suivant de l'objet en cours.
   * @return true si la selection a �t� modifi�e.
   */
  @Override
  public boolean selectNext() {
    if (isSelectionEmpty()) return false;
    int idx=getLayerSelection().getMaxIndex();
    if (idx>=modeleDonnees().getNombre()-1) return false;
    return setSelection(new int[]{idx+1});
  }

  
  /**
   * Selectionne l'objet suivant de l'objet en cours.
   * @return true si la selection a �t� modifi�e.
   */
  @Override
  public boolean selectPrevious() {
    if (isSelectionEmpty()) return false;
    int idx=getLayerSelection().getMinIndex();
    if (idx<=0) return false;
    return setSelection(new int[]{idx-1});
  }
}
