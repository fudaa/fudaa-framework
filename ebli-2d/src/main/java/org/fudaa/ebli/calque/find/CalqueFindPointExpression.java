/*
 * @creation 12 oct. 2005
 * 
 * @modification $Date: 2007-04-16 16:35:08 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.find;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.calque.ZModelePoint;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class CalqueFindPointExpression extends CalqueFindExpression {

  Variable x_;

  Variable y_;
  Variable z_;

  final boolean buildZ_;

  public CalqueFindPointExpression(final ZModelePoint _data) {
    super(_data);
    buildZ_ = _data instanceof ZModelePoint.ThreeDim;
  }

  @Override
  public void initialiseExpr(final org.fudaa.ctulu.CtuluExpr _expr) {
    super.initialiseExpr(_expr);
    x_ = CalqueFindExpression.buildXPoint(_expr);
    y_ = CalqueFindExpression.buildYPoint(_expr);
    if (buildZ_) {
      z_ = CalqueFindExpression.buildZPoint(_expr);
    }

  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    super.majVariable(_idx, _varToUpdate);
    x_.setValue(CtuluLib.getDouble(((ZModelePoint) super.data_).getX(_idx)));
    y_.setValue(CtuluLib.getDouble(((ZModelePoint) super.data_).getY(_idx)));
    z_.setValue(CtuluLib.getDouble(((ZModelePoint.ThreeDim) super.data_).getZ(_idx)));
  }
}