/**
 * @creation 24 oct. 2003
 * @modification $Date: 2008-05-13 12:10:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuTable;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurAlpha;
import org.fudaa.ebli.controle.BSelecteurIconModel;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.controle.BSelecteurSurfaceModel;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageAbstract;
import org.fudaa.ebli.palette.BPalettePlageDiscret;
import org.fudaa.ebli.palette.BPalettePlageInterface;
import org.fudaa.ebli.palette.BPalettePlageLegende;
import org.fudaa.ebli.palette.BPalettePlageProperties;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceSurfaceModel;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class ZCalqueAffichageDonneesAbstract extends BCalqueAffichage implements ZCalqueAffichageDonneesInterface {

  public static final void ajusteZoomOnSelected(final GrBoite _r, final ZModeleDonnees _modele) {
    ajusteZoomOnSelected(_r, _modele.getDomaine());
  }

  protected BConfigurableInterface getAffichageConf() {
    final BConfigurableInterface[] sect = new BConfigurableInterface[1 + getNbSet()];
    sect[0] = new ZCalqueAffichageDonneesConfigure(this);
    for (int i = 1; i < sect.length; i++) {
      sect[i] = new ZCalqueAffichageDonneesTraceConfigure(this, i - 1);
    }
    return new BConfigurableComposite(sect, EbliLib.getS("Affichage"));
  }

  @Override
  public BConfigurableInterface getSingleConfigureInterface() {
    return new BConfigurableComposite(getAffichageConf(), null);
  }

  public static final void ajusteZoomOnSelected(final GrBoite _r, final GrBoite _envGeneral) {
    if(_envGeneral==null){
      return;
    }
    double wT = _envGeneral.getDeltaX();
    double hT = _envGeneral.getDeltaY();
    if (wT <= 0) {
      wT = hT;
    } else if (hT <= 0) {
      hT = wT;
    }
    final double coef = 0.1;
    // premier test: le zoom ne doit pas etre inferieur a 10% du domaine total
    // sinon l'utilisateur ne sait pas ou il est
    if (_r.getDeltaY() < hT * coef) {
      final double delta = (hT * coef - _r.getDeltaY()) / 2;
      _r.e_.y_ += delta;
      _r.o_.y_ -= delta;
    }
    if (_r.getDeltaX() < wT * coef) {
      final double delta = (wT * coef - _r.getDeltaX()) / 2;
      _r.e_.x_ += delta;
      _r.o_.x_ -= delta;
    }
    // on agrandit la boite de zoom de 10% pour tout voir
    double delta = _r.getDeltaY() * coef / 2;
    _r.e_.y_ += delta;
    _r.o_.y_ -= delta;
    delta = _r.getDeltaX() * coef / 2;
    _r.e_.x_ += delta;
    _r.o_.x_ -= delta;
  }
  /**
   * Le modele pour le trac� des icones.
   */
  protected TraceIconModel iconModel_;
  /**
   * Le modele pour le trac� des lignes.
   */
  protected TraceLigneModel ligneModel_;
  /**
   * Le modele pour le trac� des surfaces. Si null, le selecteur de surface n'est pas repr�sent� dans la configuration du calque, le trac� de surface
   * ne se fait pas.
   */
  protected TraceSurfaceModel surfModel_;
  protected int alpha_ = 255;
  /**
   * Permet de savoir si la palette de couleur doit etre utilisee ou non. Jusqu'a maintenant on testait paletteCouleur_: si null pas de palette et
   * sinon palette. Or cette methode ne permet pas de sauvegarder l'etat de la palette.
   */
  protected boolean isPaletteCouleurUsed_;
  protected boolean painted_ = true;
  /**
   * Les objets du calques sont-ils selectionnables
   */
  protected boolean isSelectable_ = true;
  /** La palette de couleurs. Peut �tre null */
  protected BPalettePlageAbstract paletteCouleur_;
  /** Le panneau de l�gende associ� au calque. Peut �tre null */
  protected BPalettePlageLegende paletteLegende_;

  public ZCalqueAffichageDonneesAbstract() {
    super();
    setDestructible(false);
  }

  @Override
  public boolean isEditable() {
    // B.M. : Bizarre, on pourrait penser que le caractere editable ou non n'est
    // pas lie au nombre de geometries dans le modele...
    return modeleDonnees() != null;
  }

  @Override
  public boolean canBeUsedForAccroche() {
    return true;
  }

  protected Color getColorWithAttenuAlpha(Color init) {
    if (isAttenue()) {
      return EbliLib.getAlphaColor(attenueCouleur(init), alpha_);
    } else if (EbliLib.isAlphaChanged(alpha_)) {
      return EbliLib.getAlphaColor(init, alpha_);
    }
    return init;
  }

  @Override
  public boolean isSelectable() {
    return isSelectable_;
  }

  /**
   * Definit si les objets du calque sont selectionnables.
   */
  public void setSelectable(boolean _b) {
    if (isSelectable() != _b) {
      if (!_b) {
        clearSelection();
      }
      isSelectable_ = _b;
      firePropertyChange("selectable", !_b, _b);
    }
  }

  @Override
  public boolean canSetSelectable() {
    return false;
  }

  @Override
  public void paintAllInImage(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
    if (isVisible()) {
      paintDonnees(_g, _versEcran, _versReel, _clipReel);
    }
  }

  /**
   * Construit si necessaire la legende specifique. Sinon met a jour la legende.
   */
  @Override
  protected void construitLegende() {
    final BCalqueLegende l = getLegende();
    if (l == null) {
      return;
    }
    // il n'y a pas de palette, on enleve la legende.
    if (isPaletteCouleurUsed_ || !legendContainsOnlyPalette()) {
      if (paletteLegende_ != null) {
        if (paletteLegende_.getModel() == paletteCouleur_) {
          paletteLegende_.allPaletteModified(paletteCouleur_);
        } else {
          paletteLegende_.setModel(paletteCouleur_);
        }

      } else if (paletteCouleur_ != null) {
        paletteLegende_ = new BPalettePlageLegende(paletteCouleur_);
      }
      if (!l.containsLegend(this) && paletteLegende_ != null) {
        l.ajoute(this, paletteLegende_, getTitle());
      }
      l.updateAll();
    } else {
      l.enleve(this);
      l.revalidate();
    }
  }

  protected void fireSelectionEvent() {
    repaint(0);
    ZSelectionEvent evt = null;
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ZSelectionListener.class) {
        if (evt == null) {
          evt = new ZSelectionEvent(this);
        }
        ((ZSelectionListener) listeners[i + 1]).selectionChanged(evt);
      }
    }
  }

  /**
   * @param _paletteCouleur PaletteCouleur
   */
  protected void setPaletteCouleur(final BPalettePlageAbstract _paletteCouleur) {
    if (paletteCouleur_ != _paletteCouleur) {
      final BPalettePlageInterface vp = paletteCouleur_;
      paletteCouleur_ = _paletteCouleur;
      isPaletteCouleurUsed_ = (paletteCouleur_ != null) && paletteCouleur_.getNbPlages() > 0;
      construitLegende();
      firePropertyChange("paletteCouleur", vp, paletteCouleur_);
      repaint();
    }
  }

  protected void updateLegendeTitre() {
    if (paletteLegende_ != null) {
      paletteLegende_.paletteTitreModified(paletteCouleur_);
      firePropertyChange("paletteTitle", true, false);
      if ((getLegende() != null) && (getLegende().isVisible())) {
        getLegende().revalidate();
        getLegende().doLayout();
      }
    }
  }

  /**
   * Permet d'ajuster le zoom (la boite _r) en fonction du modele: la boite est au moins egale a 10% du domaine total et elle est ensuite agrandie de
   * 10% pour tout voir.
   *
   * @param _r la boite de zoom a modifier
   */
  public final void ajusteZoomOnSelected(final GrBoite _r) {
    ajusteZoomOnSelected(_r, modeleDonnees());
  }

  @Override
  public BPalettePlageInterface createPaletteCouleur() {
    if (isDiscrete()) {
      return new BPalettePlageDiscret(null);
    } else {
      return new BPalettePlage();
    }
  }

  @Override
  public BuTable createValuesTable() {
    final BuTable table = modeleDonnees().createValuesTable(this);
    if (table != null && EbliTableInfoPanel.containsTitle(table) == null) {
      EbliTableInfoPanel.setTitle(table, getTitle());
    }
    return table;
  }

  /**
   * Lance la premiere action.
   */
  @Override
  public String editSelected() {
    if (actions_ != null && actions_.length > 0) {
      actions_[0].updateStateBeforeShow();
      if (actions_[0].isEnabled()) {
        actions_[0].actionPerformed(null);
      }
    }
    return null;
  }

  @Override
  public void fillWithInfo(final InfoData _d) {
    if (modeleDonnees() != null) {
      modeleDonnees().fillWithInfo(_d, this);
      if (!_d.isTitleSet()) {
        _d.setTitle(EbliLib.getS("Calque: ") + getTitle());
      }
    }
  }

  @Override
  public String getDataDescription() {
    return getTitle();
  }

  /**
   * @return Domaine
   */
  @Override
  public GrBoite getDomaine() {
    // GrBoite r = null;
    if (isVisible()) {
      final ZModeleDonnees m = modeleDonnees();
      return m == null ? null : m.getDomaine();
      /*
       * //r = super.getDomaine(); if (m == null) return r; if (r != null) { r.ajuste(m.getDomaine()); return r;
       */
    }
    return null;
  }

  @Override
  public TraceIconModel getIconModel(final int _idx) {
    return iconModel_;
  }

  @Override
  public String getSetTitle(final int _idx) {
    return EbliLib.getS("Trac�");
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
  }

  public void paintTransient(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
          final GrBoite _clipReel) {
  }

  @Override
  public CtuluListSelectionInterface getLayerSelection() {
    return null;
  }

  @Override
  public EbliListeSelectionMultiInterface getLayerSelectionMulti() {
    return null;
  }

  @Override
  public EbliListeSelectionMultiInterface selectVertices(GrPoint _ptReel, int _tolerancePixel, boolean _inDepth) {
    return null;
  }

  @Override
  public BPalettePlageInterface getPaletteCouleur() {
    return paletteCouleur_;
  }

  @Override
  public boolean getRange(final CtuluRange _b) {
    return false;
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return null;
  }

  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    return false;
  }

  @Override
  public GrBoite getZoomOnSelected() {
    GrBoite bt = getDomaineOnSelected();
    if (bt == null) {
      return null;
    }

    ajusteZoomOnSelected(bt);
    return bt;
  }

  public boolean isAntialiasSupported() {
    return true;
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {
      super.initFrom(_p);
      if (_p.isDefined("calque.alpha")) {
        setAlpha(_p.getInteger("calque.alpha"));
      }
      if (_p.isDefined("calque.antialias") && isAntialiasSupported()) {
        antialiasing_ = _p.getBoolean("calque.antialias");
      }
      final int nbSet = getNbSet();
      for (int i = 0; i < nbSet; i++) {
        String prop = createPropIcon(i);
        if (_p.isDefined(prop)) {
          setIconModel(i, (TraceIconModel) _p.get(prop));
        }
        prop = createPropLine(i);
        if (_p.isDefined(prop)) {
          setLineModel(i, (TraceLigneModel) _p.get(prop));
        }
        prop = createPropSurf(i);
        if (_p.isDefined(prop)) {
          setSurfaceModel(i, (TraceSurfaceModel) _p.get(prop));
        }
      }
      if (_p.isDefined("calque.paletteCouleurUsed")) {
        isPaletteCouleurUsed_ = _p.getBoolean("calque.paletteCouleurUsed");
      }
      if (_p.isDefined("calque.paletteCouleur")) {
        final BPalettePlageInterface newPalette = createPaletteCouleur();
        newPalette.load((BPalettePlageProperties) _p.get("calque.paletteCouleur"));
        setPaletteCouleurPlages(newPalette, false);
      }

    }
  }

  private String createPropLine(final int _i) {
    return "calque." + BSelecteurLineModel.getProperty(_i);
  }

  private String createPropSurf(final int _i) {
    return "calque." + BSelecteurSurfaceModel.getProperty(_i);
  }

  private String createPropIcon(final int _i) {
    return "calque." + BSelecteurIconModel.getProperty(_i);
  }

  /**
   * @return false
   */
  @Override
  public boolean isDiscrete() {
    return false;
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return false;
  }

  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return false;
  }

  @Override
  public boolean isOnlyOneObjectSelected() {
    return false;
  }

  /**
   * @return true si seule la selection est dessinee.
   */
  public boolean isPainted() {
    return painted_;
  }

  @Override
  public boolean isPaletteModifiable() {
    return true;
  }

  @Override
  public boolean isSpecialSelectionAllowed() {
    return false;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return modeleDonnees() == null ? false : modeleDonnees().isValuesTableAvailable();
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return null;
  }
  boolean antialiasing_ = true;
  BCalqueCacheManager cacheManager;
  BCalqueCacheManagerSelection cacheManagerSelection;

  public void setCacheManager(BCalqueCacheManager cacheManager) {
    this.cacheManager = cacheManager;
  }

  public void setCacheManagerSelection(BCalqueCacheManagerSelection cacheManager) {
    this.cacheManagerSelection = cacheManager;
  }

  protected void clearCache() {
    if (cacheManager != null) {
      cacheManager.clearCache();
    }
    if (cacheManagerSelection != null) {
      cacheManagerSelection.clearCache();
    }
  }

  public RenderingHints getRenderingHintsToUse(Graphics2D graphics2D) {
    RenderingHints newRenderer = null;
    if (isRapide()) {
      newRenderer = graphics2D.getRenderingHints();
      newRenderer = (RenderingHints) graphics2D.getRenderingHints().clone();
      newRenderer.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
      newRenderer.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
      newRenderer.put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
      newRenderer.put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
      newRenderer.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
      newRenderer.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
      newRenderer.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
      newRenderer.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);

    } else if (isAntialiasSupported() && isAntialiasing()) {
      newRenderer = (RenderingHints) graphics2D.getRenderingHints().clone();
      newRenderer.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      newRenderer.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

    }
    return newRenderer;
  }

  @Override
  public final void paintComponent(final Graphics _g) {
    if (!isVisible()) {
      return;
    }

    if (painted_) {
      final Graphics2D graphics2D = (Graphics2D) _g;
      if (cacheManager != null) {
        cacheManager.paintDonnees(_g);
      } else {
        paintDonnees(graphics2D, getVersEcran(), getVersReel(), getClipReel(_g));
      }
      paintTransient(graphics2D, getVersEcran(), getVersReel(), getClipReel(_g));
    }
  }

  /**
   * Dessine les donnees.
   *
   * @param _g la cible
   */
  public final void paintDonnees(final Graphics2D _g) {
    paintDonnees(_g, getVersEcran(), getVersReel(), getClipReel(_g));

  }

  public final void paintDonnees(final Graphics _g) {
    paintDonnees((Graphics2D) _g, getVersEcran(), getVersReel(), getClipReel(_g));
  }

  public final void paintSelection(final Graphics2D _g, final ZSelectionTrace _trace) {
    paintSelection(_g, _trace, getVersEcran(), getClipReel(_g));
  }

  @Override
  public final void paintSelection(Graphics2D _g, ZSelectionTrace _trace, GrMorphisme _versEcran, GrBoite _clipReel) {
    if (cacheManagerSelection != null) {
      cacheManagerSelection.paint(_g, _trace, _versEcran, _clipReel);
    } else {
      doPaintSelection(_g, _trace, _versEcran, _clipReel);
    }
  }

  /**
   * Methode permettant de dessiner la s�lection
   *
   * @param _g
   * @param _trace
   * @param _versEcran
   * @param _clipReel
   */
  public abstract void doPaintSelection(Graphics2D _g, ZSelectionTrace _trace, GrMorphisme _versEcran, GrBoite _clipReel);

  /**
   * @param _c
   * @param _g
   * @param _x
   * @param _y
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(Color.white);
    _g.fillRect(_x + 1, _y + 1, w - 1, h - 1);
    if (isPaletteCouleurUsed_) {
      _g.setColor(Color.RED);
      // le trait horizontal d'en haut en rouge
      _g.drawLine(_x, _y, _x + w, _y);
      _g.setColor(Color.BLUE);
      // le trait horizontal d'en bas en bleu
      _g.drawLine(_x, _y + h, _x + w, _y + h);
      _g.setColor(Color.GREEN);
      // trait vertical gauche
      _g.drawLine(_x, _y, _x, _y + h);
      // trait vertical droit
      _g.setColor(Color.YELLOW);
      _g.drawLine(_x + w, _y, _x + w, _y + h);

    } else {
      _g.setColor(getForeground());
      _g.drawRect(_x, _y, w, h);
    }
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    final EbliUIProperties res = super.saveUIProperties();
    final int nbSet = getNbSet();
    for (int i = 0; i < nbSet; i++) {
      final TraceIconModel model = getIconModel(i);
      if (model != null) {
        res.put(createPropIcon(i), new TraceIconModel(model));
      }
      final TraceLigneModel lineModel = getLineModel(i);
      if (lineModel != null) {
        res.put(createPropLine(i), new TraceLigneModel(lineModel));
      }
      final TraceSurfaceModel surfModel = getSurfaceModel(i);
      if (surfModel != null) {
        res.put(createPropSurf(i), new TraceSurfaceModel(surfModel));
      }
    }
    if (EbliLib.isAlphaChanged(alpha_)) {
      res.put("calque.alpha", alpha_);
    }
    if (paletteCouleur_ != null) {
      res.put("calque.paletteCouleur", paletteCouleur_.save());
    }
    if (isAntialiasSupported()) {
      res.put("calque.antialias", Boolean.valueOf(antialiasing_));
    }
    res.put("calque.paletteCouleurUsed", isPaletteCouleurUsed_);
    return res;
  }

  @Override
  public void setForeground(final Color _v) {
    for (int i = getNbSet() - 1; i >= 0; i--) {
      final TraceIconModel ic = getIconModel(i);
      if (ic != null && ic.setCouleur(_v)) {
        firePropertyChange(BSelecteurIconModel.getProperty(i), null, ic);
      }
      final TraceLigneModel line = getLineModel(i);
      if (line != null && line.setCouleur(_v)) {
        firePropertyChange(BSelecteurLineModel.getProperty(i), null, line);
      }
      final TraceSurfaceModel surf = getSurfaceModel(i);
      if (surf != null && surf.setBgColor(_v)) {
        firePropertyChange(BSelecteurSurfaceModel.getProperty(i), null, surf);
      }
    }
    super.setForeground(_v);
    removeUsedPalette();
    if (isVisible()) {
      quickRepaint();
    }
  }

  protected void removeUsedPalette() {
    if (isPaletteCouleurUsed_) {
      firePropertyChange("paletteCouleur", paletteCouleur_, null);
      isPaletteCouleurUsed_ = false;
      if (legendContainsOnlyPalette()) {
        final BCalqueLegende l = getLegende();
        if (l != null) {
          l.enleve(this);
          l.revalidate();
        }
      }
    }
  }

  /**
   * @return true si la l�gende ne contient que des donn�es concerant la palette de couleur. dans ce cas, la legende est enlevee si aucune palette
   * n'est utilis�e.
   */
  public boolean legendContainsOnlyPalette() {
    return true;
  }

  /**
   * Tout sera dessine.
   *
   * @param _b si true, les donnees +selection seront dessines. si false, seule la selection sera dessinee.
   */
  public void setPainted(final boolean _b) {
    painted_ = _b;
  }

  @Override
  public void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage) {
    setPaletteCouleurPlages(_newPlage, true);
  }

  private void setPaletteCouleurPlages(final BPalettePlageInterface _newPlage, boolean updatePaletteCouleurProp) {
    if (_newPlage == null) {
      paletteCouleur_ = null;
    } else {
      if (paletteCouleur_ == null) {
        if (isDiscrete()) {
          paletteCouleur_ = new BPalettePlageDiscret(new Object[0]);
          paletteCouleur_.setPlages(_newPlage.getPlages());
        } else {
          paletteCouleur_ = new BPalettePlage();
          paletteCouleur_.initFrom(_newPlage);
        }
      } else {
        paletteCouleur_.initFrom(_newPlage);
      }
    }
    if (updatePaletteCouleurProp) {
      isPaletteCouleurUsed_ = (paletteCouleur_ != null) && paletteCouleur_.getNbPlages() > 0;
    }
    construitLegende();
    firePropertyChange("paletteCouleur", null, paletteCouleur_);
    repaint();
  }

  @Override
  public boolean setTitle(final String _title) {
    final boolean r = super.setTitle(_title);
    if (r && paletteLegende_ != null && getLegende() != null) {
      getLegende().updateMainTitle(this, _title);
    }
    return r;
  }

  @Override
  public boolean setIconModel(final int _idx, final TraceIconModel _model) {
    final TraceIconModel model = getIconModel(_idx);
    final boolean r = model == null || model.updateData(_model);
    if (r) {
      firePropertyChange(BSelecteurIconModel.getProperty(_idx), null, model);
      repaint();
    }
    return r;
  }

  public int getAlpha() {
    return alpha_;
  }

  @Override
  public TraceLigneModel getLineModel(final int _idx) {
    return ligneModel_;
  }

  @Override
  public int getNbSet() {
    return 1;
  }

  public final boolean setSurfaceModel(final int _idx, final TraceSurfaceModel _model) {
    TraceSurfaceModel surf = getSurfaceModel(_idx);
    if (surf == null && _model == null) {
      return false;
    }

    if (surf == null || _model == null) {
      internalSetSurfaceModel(_idx, _model);
      surf = getSurfaceModel(_idx);
      firePropertyChange(BSelecteurSurfaceModel.getProperty(_idx), null, surf);
      return true;
    }

    if (_model != null && surf != null && surf.updateData(_model)) {
      firePropertyChange(BSelecteurSurfaceModel.getProperty(_idx), null, surf);
      repaint();
      return true;
    }
    return false;
  }

  /**
   * A surcharger pour mettre a jour les modeles de trac� de surface.
   *
   * @param _idx L'indice de modele.
   * @param _model Le modele.
   */
  protected void internalSetSurfaceModel(final int _idx, final TraceSurfaceModel _model) {
    surfModel_ = _model;
  }

  public TraceSurfaceModel getSurfaceModel(final int _idx) {
    return surfModel_;
  }

  @Override
  public boolean setLineModel(final int _idx, final TraceLigneModel _model) {
    final TraceLigneModel ligne = getLineModel(_idx);
    if (_model != null && ligne != null && ligne.updateData(_model)) {
      firePropertyChange(BSelecteurLineModel.getProperty(_idx), null, ligne);
      repaint();
      return true;
    }
    return false;
  }

  public void setAlpha(final int _alpha) {
    if (_alpha != alpha_ && _alpha >= 0 && _alpha < 256) {
      final int old = alpha_;
      alpha_ = _alpha;
      firePropertyChange(BSelecteurAlpha.DEFAULT_PROPERTY, old, alpha_);
      if (isVisible()) {
        repaint();
      }
    }
  }

  public boolean isAntialiasing() {
    return antialiasing_;
  }

  protected boolean clearSelectionIfLayerNonVisible() {
    return true;
  }

  /**
   * Efface la selection si le calque est rendu invisible.
   */
  @Override
  public void setVisible(boolean _b) {
    if (isVisible() != _b) {
      super.setVisible(_b);
      if (!_b && clearSelectionIfLayerNonVisible()) {
        clearSelection();
      }
    }
  }
}
