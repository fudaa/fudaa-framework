/*
 * @creation 10 avr. 08
 * 
 * @modification $Date$
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectIterator;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.action.CalqueSwitchAtomicSelectionAction;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.EbliListeSelectionMulti;
import org.fudaa.ebli.commun.EbliListeSelectionMultiInterface;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * La scene. Contient les modeles de donn�es des calques qui en possedent. Elle permet de proposer une interface unique pour la selection, le
 * deplacement, etc.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZScene implements TreeModelListener {

  /**
   * Helper contenant un ensemble de methodes simplifiant les tests sur la selection
   */
  public class SceneSelectionHelper {

    /**
     * @return La geometrie unique s�lectionn�e (m�me en mode sommet), ou -1 si plusieurs g�om�tries.
     */
    public int getUniqueSelectedIdx() {
      int r;
      if (ZScene.this.getSelectionMode() != SelectionMode.NORMAL) {
        r = ZScene.this.getLayerSelectionMulti().isSelectionInOneBloc();
      } else {
        r = (ZScene.this.getLayerSelection().getNbSelectedIndex() == 1) ? ZScene.this.getLayerSelection().getMinIndex() : -1;
      }
      return r;
    }

    /**
     * Retourne les indices pour une classe d'objets donn�e.
     *
     * @param _clazz La classe d'objets.
     * @return Les indices.
     */
    public int[] getSelectedForClass(Class<?> _clazz) {
      TIntArrayList idxs = new TIntArrayList();
      for (int idx : getSelectedIndexes()) {
        if (getObject(idx).getClass().equals(_clazz)) {
          idxs.add(idx);
        }
      }
      return idxs.toNativeArray();
    }

    /**
     * @return La liste de selection des atomiques pour la g�om�trie unique s�lectionn�e. <code>null</code> si plusieurs g�om�tries ou pas en mode
     * atomique.
     */
    public CtuluListSelectionInterface getUniqueAtomicSelection() {
      if (ZScene.this.getSelectionMode() == SelectionMode.NORMAL) {
        return null;
      }
      int idGeom = getUniqueSelectedIdx();
      if (idGeom == -1) {
        return null;
      }

      return ZScene.this.getLayerSelectionMulti().getSelection(idGeom);
    }

    /**
     * @return Le nombre d'atomiques s�lectionn�s, ou 0 si mode global.
     */
    public int getNbAtomicSelected() {
      return (ZScene.this.getSelectionMode() != SelectionMode.NORMAL) ? ZScene.this.getLayerSelectionMulti().getNbSelectedItem() : 0;
    }

    /**
     *
     * @return le nombre d'objets s�lectionn�s en tout ( atomic, simple)
     */
    public int getNbTotalSelectedObject() {
      if (ZScene.this.getSelectionMode() != SelectionMode.NORMAL) {
        return ZScene.this.getLayerSelectionMulti().getNbSelectedItem();
      }
      return ZScene.this.getLayerSelection().getNbSelectedIndex();

    }

    /**
     * @return Les indices des g�om�tries s�lectionn�es, que ce soit en atomique ou sommet.
     */
    public int[] getSelectedIndexes() {
      int r[];

      if (ZScene.this.getSelectionMode() != SelectionMode.NORMAL) {
        r = ZScene.this.getLayerSelectionMulti().getIdxSelected();
      } else {
        r = ZScene.this.getLayerSelection().getSelectedIndex();
      }
      return r;
    }
    /**
     * @return La liste des sommets selectionn�s pour une g�om�trie. Si en mode global, tous les sommets le sont.
     */
    //    public CtuluListSelectionInterface getVerticesList(int _idGeom) {
    //      CtuluListSelectionInterface r;
    //      if (ZScene.this.isAtomicMode()) {
    //        r=ZScene.this.getLayerSelectionMulti().getSelection(_idGeom);
    //      }
    //      else {
    //        r=ZScene.this.getObject(_idGeom);
    //      }
    //      return r;
    //    }
  }

  /**
   * Liste des objets geometrique selectionn�s de la scene
   */
  class SceneListSelection implements CtuluListSelectionInterface {

    //    CtuluListSelectionInterface[] lists_=null;
    //    public SceneListSelection(ZCalqueAffichageDonneesInterface[] _cqs) {
    //      lists_=new CtuluListSelectionInterface[_cqs.length];
    //      for (int i=0; i<lists_.length; i++) {
    //        lists_[i]=_cqs[i].getLayerSelection();
    //      }
    //    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#clear()
     */
    @Override
    public void clear() {
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (cq != null) {
          cq.clearSelection();
        }
      }
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#getMaxIndex()
     */
    @Override
    public int getMaxIndex() {
      int max = -1;
      int idecal = 0;
      ZCalqueAffichageDonneesInterface[] cqs = getTargetLayers();
      for (int i = 0; i < cqs.length; i++) {
        if (!cqs[i].isSelectionEmpty()) {
          max = cqs[i].getLayerSelection().getMaxIndex() + idecal;
        }
        idecal += cqs[i].modeleDonnees().getNombre();
      }
      return max;
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#getMinIndex()
     */
    @Override
    public int getMinIndex() {
      ZCalqueAffichageDonneesInterface[] cqs = getTargetLayers();
      int idecal = 0;
      for (int i = 0; i < cqs.length; i++) {
        if (!cqs[i].isSelectionEmpty()) {
          return cqs[i].getLayerSelection().getMinIndex() + idecal;
        }
        idecal += cqs[i].modeleDonnees().getNombre();
      }
      return -1;
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#getNbSelectedIndex()
     */
    @Override
    public int getNbSelectedIndex() {
      int nb = 0;
      //      for (CtuluListSelectionInterface list : lists_) nb+=list.getNbSelectedIndex();
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (cq.getLayerSelection() != null) {
          nb += cq.getLayerSelection().getNbSelectedIndex();
        }
      }
      return nb;
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#getSelectedIndex() @return Tableau de longueur 0 si aucune
     * selection.
     */
    @Override
    public int[] getSelectedIndex() {
      int idecal = 0;
      int ipt = 0;
      int[] iglobsels = new int[getNbSelectedIndex()];

      ZCalqueAffichageDonneesInterface[] cqs = getTargetLayers();
      for (int i = 0; i < cqs.length; i++) {
        if (cqs[i].getLayerSelection() != null) {
          int[] isels = cqs[i].getLayerSelection().getSelectedIndex();
          if (isels != null) {
            for (int j = 0; j < isels.length; j++) {
              isels[j] += idecal;
            }
            System.arraycopy(isels, 0, iglobsels, ipt, isels.length);
            ipt += isels.length;
          }
        }
        if (cqs[i].modeleDonnees() != null) {
          idecal += cqs[i].modeleDonnees().getNombre();
        }
      }
      //      for (int i=0; i<lists_.length-1; i++) {
      //        int[] isels=lists_[i].getSelectedIndex();
      //        for (int j=0; j<isels.length; j++) isels[j]+=idecal;
      //        System.arraycopy(isels, 0, iglobsels, idecal, isels.length);
      //        idecal+=cqs_[i].modeleDonnees().getNombre();
      //      }
      return iglobsels;
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#isEmpty()
     */
    @Override
    public boolean isEmpty() {
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (!cq.isSelectionEmpty()) {
          return false;
        }
      }
      //      for (CtuluListSelectionInterface list : lists_) {
      //        if (!list.isEmpty()) return false;
      //      }
      return true;
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#isOnlyOnIndexSelected()
     */
    @Override
    public boolean isOnlyOnIndexSelected() {
      boolean b = false;
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (cq.getLayerSelection().isOnlyOnIndexSelected()) {
          if (!b) {
            b = true;
          } else {
            return false;
          }
        } else if (!cq.getLayerSelection().isEmpty()) {
          return false;
        }
      }
      return b;
    }

    /*
     * (non-Javadoc) @see org.fudaa.ctulu.CtuluListSelectionInterface#isSelected(int)
     */
    @Override
    public boolean isSelected(int _idx) {
      int idecal = 0;
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (_idx <= cq.getLayerSelection().getMaxIndex() + idecal) {
          return cq.getLayerSelection().isSelected(_idx - idecal);
        }
        idecal += cq.modeleDonnees().getNombre();
      }
      return false;
    }
  }

  /**
   * Liste de sommets selectionn�s par objet geometrique s�lectionn�.
   */
  class SceneListSelectionMulti implements EbliListeSelectionMultiInterface {

    public SceneListSelectionMulti() {
    }

    /*
     * (non-Javadoc) @return Talbeau de longueur 0 si aucune selection.
     */
    @Override
    public int[] getIdxSelected() {
      int idecal = 0;
      int ipt = 0;
      int[] iglobsels = new int[getNbListSelected()];

      ZCalqueAffichageDonneesInterface[] cqs = getTargetLayers();
      for (int i = 0; i < cqs.length; i++) {
        if (cqs[i].getLayerSelectionMulti() != null) {
          int[] isels = cqs[i].getLayerSelectionMulti().getIdxSelected();
          if (isels != null) {
            for (int j = 0; j < isels.length; j++) {
              isels[j] += idecal;
            }
            System.arraycopy(isels, 0, iglobsels, ipt, isels.length);
            ipt += isels.length;
          }
        }
        idecal += cqs[i].modeleDonnees().getNombre();
      }
      return iglobsels;
      //      return selection_.getSelectedIndex();
    }

    @Override
    public CtuluListSelection getIdxSelection() {
      return new CtuluListSelection(getIdxSelected());
    }

    /*
     * (non-Javadoc) @see org.fudaa.ebli.commun.EbliListeSelectionMultiInterface#getIterator()
     */
    @Override
    public TIntObjectIterator getIterator() {
      throw new RuntimeException("Method not implemented");
    }

    @Override
    public int getNbListSelected() {
      int r = 0;
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (cq.getLayerSelectionMulti() != null) {
          r += cq.getLayerSelectionMulti().getNbListSelected();
        }
      }
      return r;
    }

    @Override
    public int getNbSelectedItem() {
      int r = 0;

      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        if (cq.getLayerSelectionMulti() != null) {
          r += cq.getLayerSelectionMulti().getNbSelectedItem();
        }
      }
      return r;
    }

    @Override
    public CtuluListSelectionInterface getSelection(int _idGeom) {
      int idecal = 0;
      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
        int nb = cq.modeleDonnees().getNombre();
        if (_idGeom < idecal + nb) {
          return cq.getLayerSelectionMulti().getSelection(_idGeom - idecal);
        }
        idecal += nb;
      }
      //      for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      //        cq.getLayerSelectionMulti().getNbSelectedItem();
      //        if (_idx <= cq.getLayerSelectionMulti().getMaxIndex()+idecal) {
      //          return cq.getLayerSelectionMulti().getSelection(_idx-idecal);
      //        }
      //        idecal+=cq.modeleDonnees().getNombre();
      //      }
      return null;
    }

    @Override
    public boolean isEmpty() {
      return selection_.isEmpty();
    }

    @Override
    public int isSelectionInOneBloc() {
      int[] idx = getIdxSelected();
      return idx.length == 1 ? idx[0] : -1;
    }
  }
  protected BArbreCalqueModel treeModel_ = null;
  protected ZCalqueAffichageDonneesInterface[] cqs_;
  /**
   * Le calque de selection interactif
   */
  protected ZCalqueSelectionInteractionAbstract cqSelectionI_ = null;
  protected SceneListSelection selection_ = null;
  protected SceneListSelectionMulti selectionMulti_ = null;
  protected SceneSelectionHelper selectionHelper_ = null;
  // protected boolean atomicMode_=false;
  protected SelectionMode selectionMode = SelectionMode.NORMAL;
  /**
   * La selection pour l'accrochage. Conserv�e pour ne pas etre reconstruite a chaque appel.
   */
  protected ZSceneSelection selTmp_ = new ZSceneSelection(this);
  /**
   * La scene est r�duite au calque s�lectionn� pour toutes les actions.
   */
  protected boolean layerRestricted_ = false;
  /**
   * La scene doit �tre remise a jour, en fonction des calques ajout�s, supprim�s.
   */
  protected boolean brefreshRequested_ = true;
  /**
   * La liste des listeners de selection chang�e.
   */
  protected HashSet<ZSelectionListener> selListeners_ = new HashSet<ZSelectionListener>();

  /**
   * Cr�ation a partir d'un groupe de calques racine.
   *
   * @param _treeModel Le groupe de calque racine.
   */
  public ZScene(BArbreCalqueModel _treeModel, ZCalqueSelectionInteractionAbstract _cqSelectionI) {
    treeModel_ = _treeModel;
    cqSelectionI_ = _cqSelectionI;
    cqSelectionI_.setScene(this);
    initSelection();
    selectionHelper_ = new SceneSelectionHelper();
  }

  public void repainSelection() {
    cqSelectionI_.repaint();
  }

  /**
   * Efface la selection de la scene.
   */
  public void clearSelection() {
    internalClearSelection();
    fireSelectionEvent();
  }

  /**
   * Efface la selection sans envoyer d'evenement.
   */
  private void internalClearSelection() {
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      cq.clearSelection();
    }
  }

  /**
   * Tout selectionner.
   */
  public void selectAll() {
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      cq.selectAll();
    }
    fireSelectionEvent();
  }

  public boolean selectNext() {
    if (isSelectionEmpty()) {
      return false;
    }
    int idx = getSelectionHelper().getUniqueSelectedIdx();
    ZCalqueAffichageDonneesInterface cq = getLayerForId(idx);
    selectionUpdatedFromScene = true;
    boolean b = cq.selectNext();
    if (b) {
      fireSelectionEvent();
    }
    selectionUpdatedFromScene = false;
    return b;
  }

  public boolean selectPrevious() {
    if (isSelectionEmpty()) {
      return false;
    }
    int idx = getSelectionHelper().getUniqueSelectedIdx();
    ZCalqueAffichageDonneesInterface cq = getLayerForId(idx);
    selectionUpdatedFromScene = true;
    boolean b = cq.selectPrevious();
    if (b) {
      fireSelectionEvent();
    }
    selectionUpdatedFromScene = false;
    return b;
  }

  /**
   * Inverser la selection.
   */
  public void inverseSelection() {
    selectionUpdatedFromScene = true;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      cq.inverseSelection();
    }
    selectionUpdatedFromScene = false;
    fireSelectionEvent();
  }

  /**
   * Selection d'indices de scene.
   *
   * @param _idx Les indices.
   */
  public void setSelection(int[] _idx) {
    TIntArrayList tb = new TIntArrayList(_idx);
    int idecal = 0;
    int idx = 0;
    boolean b = false;
    selectionUpdatedFromScene = true;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      int nb = cq.modeleDonnees().getNombre();
      tb.clear();
      while (idx < _idx.length && _idx[idx] < idecal + nb) {
        tb.add(_idx[idx] - idecal);
        idx++;
      }
      b |= cq.setSelection(tb.toNativeArray());
      idecal += nb;
    }
    if (b) {
      fireSelectionEvent();
    }
    selectionUpdatedFromScene = false;
  }

  //  boolean isEditable();
  /**
   * La boite englobante des objets s�lectionn�s.
   *
   * @return La boite, ou null si aucun objet selectionn�.
   */
  public GrBoite getDomaineOnSelected() {
    GrBoite bt = null;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (bt == null) {
        bt = cq.getDomaineOnSelected();
      } else {
        bt.ajuste(cq.getDomaineOnSelected());
      }
    }
    return bt;
  }

  /**
   * @return true si 1 objet et 1 seul est selectionne.
   */
  public boolean isOnlyOneObjectSelected() {
    boolean b = false;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (cq.isOnlyOneObjectSelected()) {
        if (!b) {
          b = true;
        } else {
          return false;
        }
      } else if (!cq.isSelectionEmpty()) {
        return false;
      }
    }
    return b;
  }

  boolean useSelectedLayerFirstForSelection = true;

  public void setUseSelectedLayerFirstForSelection(boolean useSelectedLayerFirstForSelection) {
    this.useSelectedLayerFirstForSelection = useSelectedLayerFirstForSelection;
  }

  public boolean isUseSelectedLayerFirstForSelection() {
    return useSelectedLayerFirstForSelection;
  }

  /**
   * Permet de modifier la selection de la scene � partir d'un selection ponctuelle. Le premier objet trouv� interrompt la s�lection.
   *
   * @param _pointReel le point de selection en coordonnees reelles
   * @param _tolerancePixel la tolerance utilisee pour determiner si un objet est selectionne
   * @param _action voir les action de ZCalqueSelectionInteraction
   * @return true si un objet a �t� selectionn�
   * @see ZCalqueSelectionInteractionAbstract
   */
  public boolean changeSelection(GrPoint _pointReel, int _tolerancePixel, int _action) {
    if (_action == EbliSelectionState.ACTION_REPLACE) {
      internalClearSelection();
    }
    selectionUpdatedFromScene = true;
    boolean ret = false;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers(useSelectedLayerFirstForSelection)) {
      if (cq.changeSelection(_pointReel, _tolerancePixel, _action)) {
        ret = true;
        break;
      }
    }
    fireSelectionEvent();
    selectionUpdatedFromScene = false;

    return ret;
  }

  /**
   * Permet de modifier la selection de la scene � partir d'un poly de selection.
   *
   * @param _p le polygone englobant en COORDONNEES REELS
   * @param _action l'action sur la selection (voir les action de ZCalqueSelectionInteraction)
   * @param _mode TODO
   * @return true si selection modifiee
   * @see ZCalqueSelectionInteractionAbstract
   */
  public boolean changeSelection(LinearRing _p, int _action, int _mode) {
    boolean b = false;
    selectionUpdatedFromScene = true;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers(useSelectedLayerFirstForSelection)) {
      b |= cq.changeSelection(_p, _action, _mode);
    }
    fireSelectionEvent();
    selectionUpdatedFromScene = false;
    return b;
  }

  public boolean changeSelection(LinearRing[] _p, int _action, int _mode) {
    boolean b = false;
    selectionUpdatedFromScene = true;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers(useSelectedLayerFirstForSelection)) {
      b |= cq.changeSelection(_p, _action, _mode);
    }
    fireSelectionEvent();
    selectionUpdatedFromScene = false;
    return b;
  }

  /**
   * Change la selection a partir d'une liste de selection.
   *
   * @param _selection La liste de selection.
   * @param _action voir les action de ZCalqueSelectionInteraction
   * @return true si la selection a �t� modifi�e.
   */
  public boolean changeSelection(ZSceneSelection _selection, int _action) {
    selectionUpdatedFromScene = true;
    boolean b = false;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers(useSelectedLayerFirstForSelection)) {
      EbliListeSelectionMultiInterface sel = _selection.getLayerListSelectionMulti(cq);
      b |= cq.changeSelection(sel, _action);
    }
    fireSelectionEvent();
    selectionUpdatedFromScene = false;
    return b;
  }

  /**
   * Retourne la selection pour le point donn�.
   * <p>
   * <b>Remarque:</b> Uniquement des sommets pour le moment, et en tenant compte du fait que la selection est restreinte a un calque ou pas.
   *
   * @param _pt Le point donn�.
   * @param _tolerance La tol�rance, en pixels.
   * @return Les sommets selectionn�s, ou vide si aucune selection.
   */
  public ZSceneSelection selection(final GrPoint _pt, final int _tolerance) {
    selTmp_.clear();
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      EbliListeSelectionMulti sel = (EbliListeSelectionMulti) cq.selectVertices(_pt, _tolerance, true);
      if (sel != null) {
        selTmp_.addLayerListSelectionMulti(cq, sel);
      }
    }
    return selTmp_;
  }

  public ZSceneSelection selectionForAccroche(final GrPoint _pt, final int _tolerance) {
    selTmp_.clear();
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (cq.canBeUsedForAccroche()) {
        EbliListeSelectionMulti sel = (EbliListeSelectionMulti) cq.selectVertices(_pt, _tolerance, true);
        if (sel != null) {
          selTmp_.addLayerListSelectionMulti(cq, sel);
        }
      }
    }
    return selTmp_;
  }

  /**
   * Retourne le sommet 3D correspondant a l'indice de geometrie _idScene et l'indice de sommet _idVertex.
   *
   * @param _idScene L'indice de la g�om�trie dans la scene
   * @param _idVertex L'indice de sommet sur la g�om�trie.
   * @return Le sommet 3D.
   */
  public GrPoint getVertex(int _idScene, int _idVertex) {
    int idxGeom = sceneId2LayerId(_idScene);
    return getLayerForId(_idScene).modeleDonnees().getVertexForObject(idxGeom, _idVertex);
  }

  /**
   * @return la ligne repr�sentant la s�lection courant. null si s�lection non adequate
   */
  //  LineString getSelectedLine();
  /**
   * @return true si la selection est vide
   */
  public boolean isSelectionEmpty() {
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (!cq.isSelectionEmpty()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Retourne le nombre d'objets dans le mod�le
   */
  public int getNombre() {
    int nb = 0;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (cq.modeleDonnees() != null) {
        nb += cq.modeleDonnees().getNombre();
      }
    }
    return nb;
  }

  /**
   * Retourne l'objet d'index i par rapport a la liste de selections.
   */
  public Object getObject(int _idGeom) {
    int idecal = 0;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      int nb = cq.modeleDonnees().getNombre();
      if (_idGeom < idecal + nb) {
        return cq.modeleDonnees().getObject(_idGeom - idecal);
      }
      idecal += nb;
    }
    //    int idecal=0;
    //    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
    //      if (_idGeom <= (idecal+=cq.modeleDonnees().getNombre())) 
    //        return cq.modeleDonnees().getObject(_idGeom-idecal);
    //    }
    return null;
  }

  /**
   * Retourne le calque contenant la g�om�trie d'indice _idGeom.
   *
   * @param _idGeom L'indice de scene.
   * @return Le calque.
   */
  public ZCalqueAffichageDonneesInterface getLayerForId(int _idGeom) {
    int idecal = 0;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (_idGeom < (idecal += cq.modeleDonnees().getNombre())) {
        return cq;
      }
    }
    return null;
  }

  /**
   * Converti un indice de scene (global) en un indice pour un calque.
   *
   * @param _idScene L'index dans la scene.
   * @return L'index pour le calque.
   * @see #getLayerForId(int)
   */
  public int sceneId2LayerId(int _idScene) {
    int idecal = 0;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      int nb = cq.modeleDonnees().getNombre();
      if (_idScene < idecal + nb) {
        return _idScene - idecal;
      }
      idecal += nb;
    }
    return -1;
  }

  /**
   * Converti un indice de calque en un indice de scene (global).
   *
   * @param _cq Le calque.
   * @param _idLayer L'index dans le calque.
   * @return L'index global pour la scene.
   * @see #getLayerForId(int)
   */
  public int layerId2SceneId(ZCalqueAffichageDonneesInterface _cq, int _idLayer) {
    int idecal = 0;
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (cq == _cq) {
        return _idLayer + idecal;
      }
      idecal += cq.modeleDonnees().getNombre();
    }
    return -1;
  }

  /**
   * @return la selection multi. Peut etre null.
   */
  public EbliListeSelectionMultiInterface getLayerSelectionMulti() {
    //    initSelection();
    return selectionMulti_;
  }

  /**
   * @return la selection simple. Peut etre null.
   */
  public CtuluListSelectionInterface getLayerSelection() {
    return selection_;
  }

  public SceneSelectionHelper getSelectionHelper() {
    return selectionHelper_;
  }

  private void initSelection() {
    if (selection_ == null) {
      selection_ = new SceneListSelection();
      selectionMulti_ = new SceneListSelectionMulti();
    }
  }

  private void initAtomicMode() {
    boolean done = false;
    for (ZCalqueEditable cq : getLayersForAtomicMode()) {
      if (!done && cq.getEditor() instanceof ZEditorDefault) {
        done = true;
        ((ZEditorDefault) cq.getEditor()).updateAtomicModeFromSupport();
      }
      cq.setSelectionMode(selectionMode);
    }
  }

  public void paintSelection(Graphics2D _g, ZSelectionTrace _trace, GrMorphisme _versEcran, GrBoite _clipReel) {
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      cq.paintSelection(_g, _trace, _versEcran, _clipReel);
    }
  }

  /**
   * Peut �tre de taille = 0. Pas null.
   *
   * @return
   */
  public ZCalqueEditable[] getEditableLayers() {
    ArrayList<ZCalqueEditable> cqs = new ArrayList<ZCalqueEditable>();
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (cq instanceof ZCalqueEditable && ((ZCalqueEditable) cq).isEditable()) {
        cqs.add((ZCalqueEditable) cq);
      }
    }
    return cqs.toArray(new ZCalqueEditable[0]);
  }

  public ZCalqueEditable[] getLayersForAtomicMode() {
    ArrayList<ZCalqueEditable> cqs = new ArrayList<ZCalqueEditable>();
    for (ZCalqueAffichageDonneesInterface cq : getTargetLayers()) {
      if (cq instanceof ZCalqueEditable) {
        cqs.add((ZCalqueEditable) cq);
      }
    }
    return cqs.toArray(new ZCalqueEditable[0]);
  }
  ZCalqueAffichageDonneesInterface oldActif;
  private boolean selectionUpdatedFromScene;

  private class CalqueActifSelectionListener implements ZSelectionListener {

    @Override
    public void selectionChanged(ZSelectionEvent _evt) {
      if (!selectionUpdatedFromScene) {
        fireSelectionEvent();
      }

    }
  }
  private CalqueActifSelectionListener calqueActifSelectionListener;

  private void createCalqueActifSelectionListener() {
    if (calqueActifSelectionListener == null) {
      calqueActifSelectionListener = new CalqueActifSelectionListener();
    }
  }

  /**
   * En mode global, ne change pas la selection. Sinon, clear.
   *
   * @param _calque Peut �tre null.
   */
  public void setCalqueActif(BCalque _calque) {

    boolean sendEvent = false;

    if (treeModel_.getSelectedCalque() instanceof ZCalqueAffichageDonneesInterface && isRestrictedToCalqueActif()) {

      ZCalqueAffichageDonneesInterface newSelectedCalque = (ZCalqueAffichageDonneesInterface) treeModel_.getSelectedCalque();

      if (oldActif != null) {
        oldActif.clearSelection();
        if (calqueActifSelectionListener != null) {
          oldActif.removeSelectionListener(calqueActifSelectionListener);
        }
      }
      oldActif = newSelectedCalque;
      if (oldActif != null) {
        createCalqueActifSelectionListener();
        //afin de forwarder les modification de selection sur le calque actif.
        oldActif.addSelectionListener(calqueActifSelectionListener);
      }
      sendEvent = true;
    }
    if (!(treeModel_.getSelectedCalque() instanceof ZCalqueAffichageDonneesInterface)) {
      cqSelectionI_.modificateur_.setSpecial(false);
    } else {
      cqSelectionI_.modificateur_.setSpecial(
              ((ZCalqueAffichageDonneesInterface) treeModel_.getSelectedCalque()).isSpecialSelectionAllowed());
    }
    cqSelectionI_.repaint();
    //on met a jour le caractere atomique
    this.initAtomicMode();
    if (sendEvent) {
      fireSelectionEvent();
    }
  }

  public BCalque getCalqueActif() {
    return treeModel_.getSelectedCalque();
  }

  public boolean isCalqueActifEditable() {
    return treeModel_.getSelectedCalque() instanceof ZCalqueEditable;
  }

  /**
   * Definit si la scene est reduite au calque actif.
   *
   * @param _b true : La scene est reduite au calque actif. Sinon, globale.
   */
  public void setRestrictedToCalqueActif(boolean _b) {
    layerRestricted_ = _b;
  }

  public boolean isRestrictedToCalqueActif() {
    return layerRestricted_;
  }

  public void refreshRequested() {
    brefreshRequested_ = true;
  }

  public ZCalqueAffichageDonneesInterface[] getTargetLayers() {
    return getTargetLayers(true);

  }

  /**
   * Retourne les calques cibles, soit tous les calques de donn�es si global, soit le calque actif si restreint, soit [0] si restreint et non calque
   * de donn�es.
   *
   * @return Les calques cibles.
   */
  public ZCalqueAffichageDonneesInterface[] getTargetLayers(boolean selectedFirst) {
    if (!isRestrictedToCalqueActif()) {
      return selectedFirst ? getAllLayersWithSelectedFirst() : getAllLayers();
    } else if (treeModel_.getSelectedCalque() instanceof ZCalqueAffichageDonneesInterface) {
      return new ZCalqueAffichageDonneesInterface[]{(ZCalqueAffichageDonneesInterface) treeModel_.getSelectedCalque()};
    } else {
      return new ZCalqueAffichageDonneesInterface[0];
    }
  }

  /**
   * Retourne tous les calques de donn�es contenues dans la scene, dans l'ordre de parcours de l'arbre des calques.
   *
   * @return Les calques de donn�es.
   */
  public ZCalqueAffichageDonneesInterface[] getAllLayers() {
    if (brefreshRequested_) {
      ArrayList<BCalque> lcqs = new ArrayList<BCalque>();
      for (BCalque cq : treeModel_.getRootCalque().getTousCalques()) {
        if (cq instanceof ZCalqueAffichageDonneesInterface) {
          lcqs.add(cq);
        }
      }
      cqs_ = lcqs.toArray(new ZCalqueAffichageDonneesInterface[0]);
      brefreshRequested_ = false;
    }
    return cqs_;
  }

  /**
   * Retourne tous les calques de donn�es contenus dans la scene, en mettant en premier les calques selectionn�s, puis dans l'ordre de parcours de
   * l'arbre des calques.
   * <p>
   * Notamment, sert pour la selection de sommets/g�om�tries. TODO : Devrait en toute logique remplacer getAllLayers().
   *
   * @return Les calques de donn�es.
   */
  public ZCalqueAffichageDonneesInterface[] getAllLayersWithSelectedFirst() {
    ArrayList<ZCalqueAffichageDonneesInterface> lcqs = new ArrayList<ZCalqueAffichageDonneesInterface>();
    lcqs.addAll(Arrays.<ZCalqueAffichageDonneesInterface>asList(getAllLayers()));
    BCalque[] cqsel = treeModel_.getSelection();
    for (int i = 0; i < cqsel.length; i++) {
      if (cqsel[i] instanceof ZCalqueAffichageDonneesInterface) {
        lcqs.remove((ZCalqueAffichageDonneesInterface) cqsel[i]);
        lcqs.add(i, (ZCalqueAffichageDonneesInterface) cqsel[i]);
      }
    }
    return lcqs.toArray(new ZCalqueAffichageDonneesInterface[0]);
  }

  /*
   * public void setAtomicMode(boolean _b) { if (atomicMode_!=_b) { atomicMode_=_b; clearSelection(); // Pour remise a zero de la
   * selection en cas de changement de mode. initAtomicMode(); } }
   *
   * public boolean isAtomicMode() { return atomicMode_; }
   */
  public boolean isAtomicMode() {
    return selectionMode == SelectionMode.ATOMIC;
  }
  CalqueSwitchAtomicSelectionAction selectionAction;

  public CalqueSwitchAtomicSelectionAction getAtomicSelectionAction() {
    if (selectionAction == null) {
      selectionAction = new CalqueSwitchAtomicSelectionAction(this);
    }
    return selectionAction;
  }

  public void setSelectionMode(SelectionMode mode) {
    if (selectionMode != mode) {
      selectionMode = mode;
      if (selectionAction != null) {
        selectionAction.updateState();
      }
      clearSelection(); // Pour remise a zero de la selection en cas de changement de mode.
      initAtomicMode();
    }
  }

  public SelectionMode getSelectionMode() {
    return selectionMode;
  }

  /**
   * WARN: on ne peut pas se baser sur le calque actif de cette scene car elle peut etre initialisee trop tard: depend de l'ordre des listener sur
   * l'arbre.
   *
   * @return
   */
  public boolean canUseAtomicMode(BCalque calque) {
    return !isRestrictedToCalqueActif() || (calque instanceof ZCalqueEditable && ((ZCalqueEditable) calque).canUseAtomicMode());
  }

  public boolean canUseAtomicMode(ZCalqueEditable calque) {
    return !isRestrictedToCalqueActif() || (calque != null && ((ZCalqueEditable) calque).canUseAtomicMode());
  }

  public boolean canUseSegmentMode(ZCalqueEditable calque) {
    return !isRestrictedToCalqueActif() || (calque != null && ((ZCalqueEditable) calque).canUseSegmentMode());
  }

  /**
   * WARN: on ne peut pas se baser sur le calque actif de cette scene car elle peut etre initialisee trop tard: depend de l'ordre des listener sur
   * l'arbre.
   *
   * @param calque
   * @return
   */
  public boolean canUseSegmentMode(BCalque calque) {
    return !isRestrictedToCalqueActif() || (calque instanceof ZCalqueEditable && ((ZCalqueEditable) calque).canUseSegmentMode());
  }

  /*
   * @see javax.swing.event.TreeModelListener#treeNodesChanged(javax.swing.event.TreeModelEvent)
   */
  @Override
  public void treeNodesChanged(TreeModelEvent e) {
  }

  /*
   * @see javax.swing.event.TreeModelListener#treeNodesInserted(javax.swing.event.TreeModelEvent)
   */
  @Override
  public void treeNodesInserted(TreeModelEvent e) {
    refreshRequested();
  }

  /*
   * @see javax.swing.event.TreeModelListener#treeNodesRemoved(javax.swing.event.TreeModelEvent)
   */
  @Override
  public void treeNodesRemoved(TreeModelEvent e) {
    refreshRequested();
  }

  /*
   * @see javax.swing.event.TreeModelListener#treeStructureChanged(javax.swing.event.TreeModelEvent)
   */
  @Override
  public void treeStructureChanged(TreeModelEvent e) {
    refreshRequested();
  }

  public void addSelectionListener(ZSelectionListener _listener) {
    selListeners_.add(_listener);
  }

  public void removeSelectionListener(ZSelectionListener _listener) {
    selListeners_.remove(_listener);
  }

  public void fireSelectionEvent() {
    ZSelectionEvent evt = new ZSelectionEvent(null);
    for (ZSelectionListener listener : selListeners_) {
      listener.selectionChanged(evt);
    }
  }

  public void sondeChanged(GrPoint ptReel) {
    fireSelectionEvent();
  }
}
