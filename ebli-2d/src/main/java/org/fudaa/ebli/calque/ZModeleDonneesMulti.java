/*
 *  @creation     21 nov. 2003
 *  @modification $Date: 2006-04-12 15:27:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.calque;
/**
 * Permet de gerer des elements qui poss�dent plusieurs objets: par exemple, les frontieres
 * d'un maillage. Un maillage possede plusieurs frontieres qui possede plusieurs points ...
 * @author deniger
 * @version $Id: ZModeleDonneesMulti.java,v 1.5 2006-04-12 15:27:09 deniger Exp $
 */
public interface ZModeleDonneesMulti extends ZModeleDonnees {
  /**
   * @param _i l'indice de l'element a considerer
   * @return le nombre d'objet contenu par l'element i
   */
  int getNbElementIn(int _i);
}
