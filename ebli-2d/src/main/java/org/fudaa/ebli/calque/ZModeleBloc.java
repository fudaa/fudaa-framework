/*
 *  @creation     10 mars 2009
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Un mod�le contenant des blocs. Les modifications apport�es au mod�le peuvent �tre �cout�es.
 * 
 * @author Bertrand Marchand
 * @status Experimental
 * @see {@link GISZoneCollectionBloc}, {@link ZCalqueBloc}, 
 * @version $Id$
 */
public interface ZModeleBloc extends ZModeleGeometry {

  int getNbGeometryForBloc(int _idxBloc);
  
  int getNbPointForGeometry(int _idxBloc, int _idxGeom);
  
  boolean isGeometryFermeeForBloc(int _idxBloc, int _idxGeom);

  boolean isGeometryRelieeForBloc(int _idxBloc, int _idxGeom);
  
  boolean point(GrPoint _pt, int _idxBloc, int _idxGeom, int _pointIdx);
}
