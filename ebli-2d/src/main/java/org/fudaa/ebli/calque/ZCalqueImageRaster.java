/*
 *  @file         BCalqueImage.java
 *  @creation     1998-08-27
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.calque.find.CalqueFindPointExpression;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.find.EbliFindExpressionContainerInterface;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;

/**
 * Un calque d'affichage d'une image.
 * 
 * @version $Id: ZCalqueImageRaster.java,v 1.20.6.1 2008-03-26 16:46:43 bmarchan Exp $
 * @author Guillaume Desnoix
 */
/**
 * @author fred deniger
 * @version $Id: ZCalqueImageRaster.java,v 1.20.6.1 2008-03-26 16:46:43 bmarchan Exp $
 */
public class ZCalqueImageRaster extends ZCalquePointEditable {
  public static final String PROP_IMAGE = "image";

  BufferedImage imgAlpha_;

  public ZCalqueImageRaster() {
    this(null);
  }

  @Override
  public BCalquePersistenceInterface getPersistenceMng() {
    return new BCalqueImagePersistence();
  }
  
  @Override
  public void setAlpha(final int _alpha) {
    if (alpha_ != _alpha) {
      imgAlpha_ = null;
    }
    super.setAlpha(_alpha);
  }

  @Override
  public boolean canAddForme(int _typeForme) {
    return false;
  }

  public AffineTransform getModeleAffineTransform() {
    return modele_ == null ? null : getModelImage().getRasterTransform();
  }

  public void imgChanged() {
    imgAlpha_ = null;
    repaint();
    firePropertyChange(PROP_IMAGE, false , true);//pour envoyer un evt dans tous les cas.
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel,
      final GrBoite _clipReel) {
    if (modele_ == null || !getModelImage().isImageLoaded()) {
      return;
    }

    final AffineTransform raster = new AffineTransform();
    int w = getWidth();
    int h = getHeight();
    final Insets marges = getInsets();
    if (marges != null) {
      w = w - marges.left - marges.right;
      h = h - marges.top - marges.bottom;
    }
    BufferedImage im = getModelImage().getImage(_clipReel, w, h, raster, isRapide());
    if (EbliLib.isAlphaChanged(alpha_)) {
      if (imgAlpha_ == null) {
        final AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.DST_OVER, ((float) alpha_) / 255);
        imgAlpha_ = new BufferedImage(im.getWidth(), im.getHeight(), BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g2D = (Graphics2D) imgAlpha_.getGraphics();
        g2D.setComposite(composite);
        g2D.drawImage(im, 0, 0, this);
      }
      im = imgAlpha_;
    }
    final AffineTransform t = new AffineTransform(_versEcran.creeAffineTransorm2D());
    t.concatenate(raster);
    _g.drawImage(im, t, this);
    super.paintDonnees(_g, _versEcran, _versReel, _clipReel);

  }

  @Override
  public EbliFindExpressionContainerInterface getExpressionContainer() {
    return new CalqueFindPointExpression(modele_);
  }

  /**
   * @param _modele
   */
  public ZCalqueImageRaster(final ZModeleImageRaster _modele) {
    super();
    setModele(_modele);
  }


  /**
   * Afectation de la propriete <I> modele </I>.
   * 
   * @param _m Modele
   */
  public final void setModele(final ZModeleImageRaster _m) {
    if (modele_ != _m) {
      final Object vp = modele_;
      modele_ = _m;
      firePropertyChange("modele", vp, _m);
    }
  }

  @Override
  public void setModele(final ZModelePoint _modele) {
    if (_modele instanceof ZModeleImageRaster) {
      setModele((ZModeleImageRaster) _modele);
    } else {
      throw new IllegalArgumentException("must be ZModeleImageRaster but get " + modele_ == null ? "null" : modele_
          .getClass().getName());
    }
  }

  /**
   * Accesseur de la propriete <I>modele </I>.
   * 
   * @return Modele
   */
  public ZModeleImageRaster getModelImage() {
    return (ZModeleImageRaster) modele_;
  }

  @Override
  public ZModeleDonnees modeleDonnees() {
    return modele_;
  }

  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null </I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w=getIconWidth()-1;
    final int h=getIconHeight()-1;
    if (modele_ == null || getModelImage().getImage()==null || !getModelImage().isImageLoaded()) {
      // Dessine une croix rouge indiquant une erreur.
      Color c=_g.getColor();
      _g.setColor(Color.red);
      _g.drawLine(0, 0, w, h);
      _g.drawLine(0, h, w, 0);
      _g.setColor(c);
    }
    else {
      final Image im=getModelImage().getImage();
      if (im!=null) {
        final int wi=im.getWidth(this);
        final int hi=im.getHeight(this);
        int r=Math.max(wi/w, hi/h);
        if(r==0)
          r=1;
        final int wImage=wi/r-1;
        final int hImage=hi/r-1;
        _g.drawImage(im, (w-wImage)/2+1, (h-hImage)/2+1, wImage, hImage, _c);
      }
    }
  }

}