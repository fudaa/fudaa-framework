/*
 * @file         DeLigneBrisee.java
 * @creation     1998-08-31
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;
import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceLigne;
/**
 * Une ligne brisee.
 *
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 14:55:53 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class DeLigneBrisee extends DeForme {
  // donnees membres publiques
  // donnees membres privees
  GrPolyligne ligne_;
  // Constructeurs
  public DeLigneBrisee() {
    super();
    ligne_= new GrPolyligne();
  }
  public DeLigneBrisee(final GrPolyligne _l) {
    super();
    ligne_= _l;
  }

  public int getNombre(){
    return ligne_.nombre();
  }
  @Override
  public int getForme() {
    return LIGNE_BRISEE;
  }
  // Methodes publiques
  @Override
  public GrObjet getGeometrie() {
    return ligne_;
  }

  /**
   * @return le dernier point
   */
  public GrPoint getDernier(){
    if(getNombre()>0){
      return ligne_.sommet(getNombre()-1);
    }
    return null;
  }
  public void ajoute(final GrPoint _p) {
    ligne_.sommets_.ajoute(_p);
  }
  
  public void ajoute(int _idx, GrPoint _pt) {
    ligne_.sommets_.insere(_pt, _idx);
  }

  public void enleveDernier(){
    if(ligne_.sommets_.nombre()>1) {
      ligne_.sommets_.enleveDernier();
    }
  }
  
  /**
   * @param _idx L'index a supprimer de la ligne.
   */
  public void enleve(int _idx) {
    if (ligne_.sommets_.nombre()>_idx) {
      ligne_.sommets_.enleve(_idx);
    }
  }
  
  public GrPoint getSommet(int _i) {
    return ligne_.sommet(_i);
  }
  
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
    _t.dessinePolyligne(_g2d,ligne_, _rapide);
  }

  public void affiche(final Graphics2D _g,final TraceLigne _tl,final boolean _rapide,final GrPoint _tmp,final GrMorphisme _versEcran){
    final int nb=ligne_.nombre();
    if(nb==0) {
      return;
    }
    _tmp.initialiseAvec(ligne_.sommet(nb-1));
    _tmp.autoApplique(_versEcran);
    double x1=_tmp.x_;
    double y1=_tmp.y_;
    for(int i=nb-2;i>=0;i--){
      _tmp.initialiseAvec(ligne_.sommet(i));
      _tmp.autoApplique(_versEcran);
      _tl.dessineTrait(_g, x1, y1, _tmp.x_, _tmp.y_);
      x1=_tmp.x_;
      y1=_tmp.y_;
    }
  }
  // >>> Interface GrContour ---------------------------------------------------
  /**
   * Retourne les points du contour de l'objet.
   *
   * @return Les points de contour.
   * @see    org.fudaa.ebli.geometrie.GrContour
   */
  @Override
  public GrPoint[] contour() {
    return ligne_.contour();
  }
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    return ligne_.estSelectionne(_ecran, _dist, _pt);
  }
  // <<< Interface GrContour ---------------------------------------------------
  // >>> Implementation GrObjet pour �tre d�placable ---------------------------
  @Override
  public void autoApplique(final GrMorphisme _t) {
    ligne_.autoApplique(_t);
  }
  /**
   * Retourne la boite englobante de l'objet.
   */
  @Override
  public GrBoite boite() {
    return ligne_.boite();
  }
  // <<< Implementation GrObjet ------------------------------------------------
}
