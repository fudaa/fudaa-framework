/**
 * @creation 8 d�c. 2003
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.BPaletteInfo;
/**
 * Un panel affichant les infos envoyes par TrPanelInfoSender.
 * @author deniger
 * @version $Id$
 */
public class BCalquePaletteInfo extends BPaletteInfo implements TreeSelectionListener, ZSelectionListener, ZModelGeometryListener {

  private List<ZCalqueAffichageDonneesInterface> calqueWithGeometriesSelected_;
  private BArbreCalqueModel treeModel_;
  private ZScene scene_;
  /** Le calque selectionn� dans l'arbre. */
  private ZCalqueAffichageDonnees calqueSelected_;
  
  /**
   * Cr�ation de la palette.
   * @param _scene La scene, pour r�cup�rer tous les calques de donn�es.
   * @param _m Le modele d'arbre, pour en r�cup�rer les evenements.
   * @param _cmd Le manager de commande.
   */
  public BCalquePaletteInfo(final ZScene _scene, final BArbreCalqueModel _m, CtuluCommandManager _cmd) {
    super(_cmd);
    treeModel_ = _m;
    treeModel_.addTreeSelectionListener(this);
    
    scene_=_scene;
    scene_.addSelectionListener(this);
    calqueWithGeometriesSelected_=new ArrayList<ZCalqueAffichageDonneesInterface>();
  }
  
  /**
   * Met � jour les informations contenues dans le tableau.
   */
  private void updateTableModel(){
    BCalque calque=treeModel_.getSelectedCalque();
    updateCalqueWithSelection();
    if(calqueSelected_==null||calque!=calqueSelected_)
      updateCalqueSelected();
    // Suppression des anciennes informations
    txtTitle_.setText(CtuluLibString.EMPTY_STRING);
    tableModel_.clear();
    // Ajout des nouvelles informations \\
    
    // Informations g�n�rales
    int nbGeom=scene_.getNombre();
    int nbGeomSelected=scene_.getLayerSelection().getNbSelectedIndex();
    if(nbGeomSelected>0)
      tableModel_.put(EbliLib.getS("Nombre total de s�lections"), Integer.toString(nbGeomSelected)+"/"+Integer.toString(nbGeom));
    // Informations relatives aux calques
    switch (calqueWithGeometriesSelected_.size()) {
    case 0:
      tableModel_.clear();
      if(treeModel_.getSelectedCalque()!=null)
        treeModel_.getSelectedCalque().fillWithInfo(tableModel_);
      tableModel_.fireTableDataChanged();
      break;
    case 1:
      calqueWithGeometriesSelected_.get(0).fillWithInfo(tableModel_);
      tableModel_.fireTableDataChanged();
      break;
    default:
      tableModel_.setTitle(EbliLib.getS("S�lection multicalques"));
    }
    updateSize();
  }
  
  /** Met � jour la liste des calques poss�dant une selection. */
  private void updateCalqueWithSelection() {
    // Supression de l'�coute sur tout les models sauf celui selectionn�
    for (ZCalqueAffichageDonneesInterface calque : calqueWithGeometriesSelected_)
      if(calque != calqueSelected_)
        if (calque.modeleDonnees() instanceof ZModeleGeometry)
          ((ZModeleGeometry)calque.modeleDonnees()).removeModelListener(this);
        else if (calque.modeleDonnees() instanceof ZModeleDonnesAbstract)
          ((ZModeleDonnesAbstract)calque.modeleDonnees()).removeModelListener(this);
    // Nombre de calques ayant des selections (atomiques ou globales)
    calqueWithGeometriesSelected_.clear();
    ZCalqueAffichageDonneesInterface[] cqs=scene_.getAllLayers();
    for (ZCalqueAffichageDonneesInterface cq : cqs) {
      if (cq.getLayerSelection()!=null&&cq.getLayerSelection().getNbSelectedIndex()>0||cq.getLayerSelectionMulti()!=null
          &&cq.getLayerSelectionMulti().getNbSelectedItem()>0) {
        if (!calqueWithGeometriesSelected_.contains(cq))
          calqueWithGeometriesSelected_.add(cq);
      }
    }
    // Ajout des �coutes
    for (ZCalqueAffichageDonneesInterface calque : calqueWithGeometriesSelected_)
      if (calque.modeleDonnees() instanceof ZModeleGeometry)
        ((ZModeleGeometry)calque.modeleDonnees()).addModelListener(this);
      else if (calque.modeleDonnees() instanceof ZModeleDonnesAbstract)
        ((ZModeleDonnesAbstract)calque.modeleDonnees()).addModelListener(this);
  }
  
  // Changement de geometries selectionn�es. \\
  @Override
  public void selectionChanged(final ZSelectionEvent _evt) {
    if (isAvailable()){
      updateCalqueWithSelection();
      updateTableModel();
    }
  }


  @Override
  public void updateState() {
    if (isAvailable())
      updateTableModel();
  }
  
  public void updateCalqueSelected(){
    // Supression de l'�coute sur l'ancien calque
    if(calqueSelected_!=null&&!calqueWithGeometriesSelected_.contains(calqueSelected_))
      if(calqueSelected_.modeleDonnees() instanceof ZModeleGeometry)
        ((ZModeleGeometry) calqueSelected_.modeleDonnees()).removeModelListener(this);
      else if(calqueSelected_.modeleDonnees() instanceof ZModeleDonnesAbstract)
        ((ZModeleDonnesAbstract) calqueSelected_.modeleDonnees()).removeModelListener(this);
    // M�morisation du calque et ajout de l'acoute
    if(treeModel_.getSelectedCalque() instanceof ZCalqueAffichageDonnees){
      calqueSelected_=(ZCalqueAffichageDonnees)treeModel_.getSelectedCalque();
      if(calqueSelected_.modeleDonnees() instanceof ZModeleGeometry)
        ((ZModeleGeometry) calqueSelected_.modeleDonnees()).addModelListener(this);
      else if(calqueSelected_.modeleDonnees() instanceof ZModeleDonnesAbstract)
        ((ZModeleDonnesAbstract) calqueSelected_.modeleDonnees()).addModelListener(this);
    }
  }
  
  // Changement de selection dans l'arbre. \\
  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    updateCalqueSelected();
    // Si aucune selection n'est faite, affiche les donn�es du calque selectionn� dans l'arbre
    if (isAvailable()&& calqueWithGeometriesSelected_.size()==0)
      updateTableModel();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.AttributeListener#attributeAction(java.lang.Object, int, org.fudaa.ctulu.gis.GISAttributeInterface, int)
   */
  @Override
  public void attributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
    if (isAvailable())
      updateTableModel();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.AttributeListener#attributeValueChangeAction(java.lang.Object, int, org.fudaa.ctulu.gis.GISAttributeInterface, int, java.lang.Object)
   */
  @Override
  public void attributeValueChangeAction(Object _source, int att, GISAttributeInterface _att, int geom, Object value) {
    if (isAvailable())
      updateTableModel();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GeometryListener#geometryAction(java.lang.Object, int, com.vividsolutions.jts.geom.Geometry, int)
   */
  @Override
  public void geometryAction(Object _source, int geom, Geometry _geom, int _action) {
    if (isAvailable())
      updateTableModel();
  }
}
