/**
 *  @creation     5 oct. 2004
 *  @modification $Date: 2006-04-12 15:27:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.geometrie.GrPoint;


/**
 * Un listener du calque {@link ZCalqueClickInteraction}.
 * @author Fred Deniger
 * @version $Id: ZCalqueClikInteractionListener.java,v 1.4 2006-04-12 15:27:10 deniger Exp $
 */
public interface ZCalqueClikInteractionListener {
  
  /**
   * Le point 3D saisi (la coordonn�e Z peut provenir d'un sommet accroch�), en coordonn�es r�elles.
   * @param  _ptReel Le point saisi.
   */
  void pointClicked(GrPoint _ptReel);

}
