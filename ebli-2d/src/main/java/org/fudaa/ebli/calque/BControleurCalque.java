/*
 * @file         BControleurCalque.java
 * @creation     1998-09-02
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import com.memoire.fu.FuLog;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
/**
 * Un controleur de calques.
 * Il ecoute les evenements <I>PropertyChange</I> et les repercute au calque
 * sous forme d'appel de ses accesseurs de propriete.
 *
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 14:55:46 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class BControleurCalque implements PropertyChangeListener {
  // donnees membres publiques
  // donnees membres privees
  // Constructeurs
  public BControleurCalque() {}
  // Methodes publiques
  /**
    * Reception des evenements <I>PropertyChange</I>.
    * Appel dynamique de l'accesseur de propriete correspondant a la
    * propriete sur le calque.
    */
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    String pname= _evt.getPropertyName();
    final Object vn= _evt.getNewValue();
    final Class[] paramType= new Class[1];
    final Object[] param= new Object[1];
    try {
      paramType[0]= vn.getClass();
    } catch (final NullPointerException e) {
      FuLog.warning(
        "property: source=" + _evt.getSource().toString() + ", name=" + pname,e);
      return;
    }
    param[0]= vn;
    try {
      if (vn instanceof Number) {
        paramType[0]= (Class)paramType[0].getField("TYPE").get(vn);
      }

      pname=pname.substring(0,1).toUpperCase() + pname.substring(1);
      final Method set= calque_.getClass().getMethod("set" + pname, paramType);
      set.invoke(calque_, param);
      
    } catch (final Exception e) {
    }
  }
  //
  // PROPRIETES INTERNES
  //
  // Propriete calque
  private BCalque calque_;
  /**
    * Accessseur de la propriete <I>calque</I>. Calque controle.
    */
  public BCalque getCalque() {
    return calque_;
  }
  /**
    * Affectation de la propriete <I>calque</I>.
    */
  public void setCalque(final BCalque _calque) {
    if (calque_ != _calque) {
      //      BCalque vp=calque_;
      calque_= _calque;
      //firePropertyChange("Calque",vp,calque_);
    }
  }
}
