/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.BPaletteEdition;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action pour afficher une palette d'outils GIS, reagissant au changement de calque selectionn�.
 * @author Fred Deniger
 * @version $Id$
 */
public class CalqueGISEditionAction extends EbliActionPaletteTreeModel implements PropertyChangeListener, ZSelectionListener {

  protected ZEditorDefault editor_;
  private ZScene scene_;
  private boolean hide_;  // Vrai si le panel d'�dition est ferm�.
  // Calques utilis� dans cette action
  private BCalqueInteraction calqueZoom_;             // Calque ne fermant pas l'edition
  private BCalqueInteraction calqueDeplacementVue_;   // Calque ne fermant pas l'edition
  protected ZCalqueEditionInteraction calqueEdition_;   // Le calque d'�dition

  /**
   * @param _editor l'editeur
   */
  public final void setScene(final ZScene _scene) {
    scene_ = _scene;
  }

  /**
   * @param _m l'arbre des calques
   */
  public CalqueGISEditionAction(final TreeSelectionModel _m, final ZEditorDefault _editor, ZScene _scene) {
    super(EbliLib.getS("Edition dessin"), EbliResource.EBLI.getToolIcon("draw-palette"), "PALETTE_EDTION", _m);
    hide_ = true;
    editor_ = _editor;
    calqueZoom_ = editor_.getPanel().getController().getCalqueInteraction("cqAGRANDIR");
    calqueDeplacementVue_ = editor_.getPanel().getController().getCalqueInteraction("cqDEPLACEMENT_VUE-I");
    editor_.getPanel().getController().addPropertyChangeListener("gele", this);
    setScene(_scene);
    setEnabled(isTargetValid(null));
    setResizable(true);
    target_ = (target_ instanceof ZCalqueEditable) ? target_ : null;
    _scene.addSelectionListener(this);
  }

  @Override
  protected boolean isTargetValid(final Object _o) {
    return (scene_ != null && (!scene_.isRestrictedToCalqueActif() || scene_.isCalqueActifEditable()));
  }

  public ZScene getScene() {
    return scene_;
  }
  
  

  /**
   * D�active le panel.
   */
  protected void unactivePanel() {
    if (calqueEdition_ != null) {
      calqueEdition_.cancelEdition();
    }
    if (palette_ != null) {
      ((BPaletteEdition) palette_).resetPalette();
    }
  }

  protected void unactivePaletteCreation() {
    if (calqueEdition_ != null) {
      calqueEdition_.cancelEdition();
    }
    if (palette_ != null) {
      ((BPaletteEdition) palette_).resetPaletteCreation();
    }
  }

  /**
   * Le calque target, c'est � dire le calque actif.
   */
  @Override
  protected void setTarget(final Object _o) {
    final Object newTarget = (_o instanceof ZCalqueEditable) ? _o : null;
    if (target_ != newTarget) {
      target_ = newTarget;
      if (isSelected()) {
        setPaletteTarget(editor_);
      } else {
        setEnabled(isTargetValid(null));
      }
    }
    setDisableWhenHide_ = !isTargetValid(null);
    if (editor_ != null) {
      editor_.setActivated(newTarget, (BPaletteEdition) palette_);
    }
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.commun.EbliActionPaletteAbstract#changeAction()
   */
  @Override
  public void changeAction() {
    super.changeAction();
    hide_ = !hide_;
    if (hide_) {
      unactivePanel();
      if (editor_.getPanel().getController().getCqSelectionI() != null) {
        editor_.getPanel().getController().getCqSelectionI().setGele(false);
      }
      // Gele du calque d'edition
      if (calqueEdition_ != null) {
        editor_.getPanel().unsetCalqueInteractionActif(calqueEdition_);
      }
    } else {
      initCalqueEdition();
    }
  }

  private void initCalqueEdition() {
    // R�cup�ration du calque d'edition
    if (calqueEdition_ == null) {
      calqueEdition_ = editor_.buildCalqueDessin("");
    }
  }

  @Override
  public void updateBeforeShow() {
    if (palette_ != null) {
      initCalqueEdition();
      if (editor_ == null) {
        palette_.setPalettePanelTarget(null);
      } else {
        editor_.setDesktop(desktop_);
        editor_.setActivated(target_, (BPaletteEdition) palette_);
        palette_.setPalettePanelTarget(editor_);
      }
    }
  }

  @Override
  protected BPalettePanelInterface buildPaletteContent() {
    final BPaletteEdition bPaletteEdition = new BPaletteEdition(scene_);
    bPaletteEdition.buildComponents();
    return bPaletteEdition;
  }

  /**
   * Appel�e quand il y a un changement de selection dans l'arbre des calques.
   */
  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    unactivePaletteCreation();
    if (editor_.getPanel().getController().getCqSelectionI() != null) {
      editor_.getPanel().getController().getCqSelectionI().setGele(false);
    }
    setTarget(getTarget((TreeSelectionModel) _e.getSource()));
  }

  /*
   * (non-Javadoc) @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
   */
  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    // Cas un autre calque que celui du zoom, du deplacement de vue et d'edition
    // est d�gel� donc on ferme le pannel d'edition si il �tait ouvert.
    //a priori c'est faux car sinon on garde les boutons activ�s pour rien.
    if (_evt.getSource() != calqueZoom_ && _evt.getSource() != calqueDeplacementVue_ && _evt.getSource() != calqueEdition_
        && !((Boolean) _evt.getNewValue()).booleanValue() && palette_ != null && ((BPaletteEdition) palette_).isEditionOnGoing()) {
      unactivePanel();
    }
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent e) {
    unactivePanel();
    if (editor_.getPanel().getController().getCqSelectionI() != null) {
      editor_.getPanel().getController().getCqSelectionI().setGele(false);
    }
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.calque.ZSelectionListener#selectionChanged(org.fudaa.ebli.calque.ZSelectionEvent)
   */
  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    if (editor_ != null) {
      editor_.updatePaletteWhenSelectionChanged();
    }
  }
}
