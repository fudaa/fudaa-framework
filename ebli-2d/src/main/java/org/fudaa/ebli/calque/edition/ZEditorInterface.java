/*
 *  @creation     1 avr. 2005
 *  @modification $Date: 2008-02-01 14:37:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuDesktop;



/**
 * @author Fred Deniger
 * @version $Id: ZEditorInterface.java,v 1.5.6.1 2008-02-01 14:37:24 bmarchan Exp $
 */
public interface ZEditorInterface extends BPaletteEditionClientInterface{

  /**
   * Definit ce qui est �ditable pour cet editeur. Si la cible est editable, alors la m�thode {@link edit} pourra �tre
   * utilis�e pour l'�dition.
   * 
   * @param _target la cible a tester
   * @return true si cette cible est editable
   */
  boolean isEditable(final Object _target);

  /**
   * Edition pour la selection quelque soit le calque selectionn�.
   * @return Une information sur le deroulement de l'op�ration d'edition. Si null, tout s'est bien pass�.
   */
  String edit();
  
  /**
   * @param _validator Le validateur, pour l'edition des objets selectionn�s.
   */
  void setEditionValidator(ZEditorValidatorI _validator);
  
  /**
   * @param _o la cible
   * @param _palette la palette associ�e
   */
  void setActivated(Object _o,BPaletteEdition _palette);
  /**
   * @param _desk le desktop parent. peut etre null.
   */
  void setDesktop(BuDesktop _desk);



}
