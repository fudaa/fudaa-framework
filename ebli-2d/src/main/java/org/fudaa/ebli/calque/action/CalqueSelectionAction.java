/*
 * @creation 11 sept. 06
 * @modification $Date: 2008-05-13 12:10:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.action;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.ebli.calque.ZEbliCalquePanelController;
import org.fudaa.ebli.commun.EbliActionChangeState;

/**
 * Une action de selection.
 * @author Fred Deniger
 * @version $Id: CalqueSelectionAction.java,v 1.2.8.1 2008-05-13 12:10:48 bmarchan Exp $
 */
public class CalqueSelectionAction extends EbliActionChangeState implements PropertyChangeListener {

  private final transient ZCalqueSelectionInteractionAbstract.SelectionMode actionMode_;
  private final transient ZEbliCalquePanelController cq_;

  /**
   * @param _ic l'icone
   * @param _actionCommand la commande
   * @param _mode le mode
   */
  public CalqueSelectionAction(final ZEbliCalquePanelController _selec, final Icon _ic, final String _actionCommand,
      final ZCalqueSelectionInteractionAbstract.SelectionMode _mode) {
    super(_mode.getDesc(), _ic, _actionCommand);
    cq_ = _selec;
    cq_.getCqSelectionI().addPropertyChangeListener(this);
    super.setEnabled(false);
    actionMode_ = _mode;
  }

  @Override
  public void changeAction() {
    final ZCalqueSelectionInteractionAbstract cqSelectionI = cq_.getCqSelectionI();
    if (isSelected()) {
      cqSelectionI.setModeSelection(actionMode_);
      cq_.setCalqueInteractionActif(cqSelectionI);
//      cqSelectionI.setCalqueActif((ZCalqueAffichageDonneesInterface) cq_.getCalqueActif());
    } else {
      cq_.unsetCalqueInteractionActif(cqSelectionI);
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    final String s = _evt.getPropertyName();
    if (("gele".equals(s)) || ("mode".equals(s))) {
      super.setSelected((!cq_.getCqSelectionI().isGele()) && (cq_.getCqSelectionI().getModeSelection() == actionMode_));
    }
  }
}