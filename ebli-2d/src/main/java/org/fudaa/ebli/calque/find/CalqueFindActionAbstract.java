/**
 * @file TrFindComponent.java
 * @creation 8 d�c. 2003
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.calque.find;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.find.*;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;

import javax.swing.*;
import java.util.StringTokenizer;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class CalqueFindActionAbstract extends EbliFindActionAbstract {
  public CalqueFindActionAbstract(final ZCalqueAffichageDonneesInterface _layer) {
    super(_layer);
  }

  public boolean isSelectionEmpty() {
    return ((ZCalqueAffichageDonneesInterface) layer_).isSelectionEmpty();
  }

  @Override
  protected int getNbValuesToTest() {
    return ((ZCalqueAffichageDonneesInterface) layer_).modeleDonnees().getNombre();
  }

  @Override
  public String editSelected(final EbliFindable _parent) {
    return ((ZCalqueAffichageDonneesInterface) layer_).editSelected();
  }

  @Override
  public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
    return ((ZCalqueAffichageDonneesInterface) layer_).isEditable();
  }

  @Override
  protected int getNbValuesInModel() {
    return ((ZCalqueAffichageDonneesInterface) layer_).modeleDonnees().getNombre();
  }

  @Override
  protected CtuluListSelection getOldSelection() {
    return new CtuluListSelection(((ZCalqueAffichageDonneesInterface) layer_).getLayerSelection());
  }

  @Override
  public final EbliFindComponent createFindComponent(final EbliFindable _parent) {
    buildExpr();
    final EbliFindComponent poly = buildPolygoneComponent(_parent);
    final EbliFindComponent first = (expr_ == null) ? buildSimpleComp() : buildExprComp();
    if (layer_ instanceof ZCalqueSondeInterface) {
      final EbliFindComponent[] cmp = new EbliFindComponent[]{first, poly, new EbliFindComponentSonde()};
      final String[] str = new String[]{EbliLib.getS("Indices"), getPolygonStr(), EbliLib.getS("Interpolation")};
      final Icon[] ic = new BuIcon[]{BuResource.BU.getToolIcon("rechercher"), getPolygonIcon(), EbliResource.EBLI.getToolIcon("pointeur")};
      final EbliFindComponentComposite c = new EbliFindComponentComposite(cmp, str, ic);
      final ZCalqueSondeInterface sonde = (ZCalqueSondeInterface) layer_;
      if (sonde.isSondeActive()) {
        c.setSelectedIndex(1);
      }
      return c;
    }
    return new EbliFindComponentComposite(new EbliFindComponent[]{first, poly}, new String[]{EbliLib.getS("Rechercher:"), getPolygonStr()},
        new BuIcon[]{BuResource.BU.getToolIcon("rechercher"), getPolygonIcon()});
  }

  protected EbliFindComponent buildPolygoneComponent(final EbliFindable _parent) {
    final CalqueFindComponentPolygone poly = new CalqueFindComponentPolygone(((ZEbliCalquesPanel) _parent).getDonneesCalque());
    return poly;
  }

  private String getPolygonStr() {
    return EbliLib.getS("Lignes ferm�es");
  }

  private BuIcon getPolygonIcon() {
    return EbliResource.EBLI.getToolIcon("draw-polygon");
  }

  @Override
  public String erreur(final Object _s, final String _action, final EbliFindable _parent) {
    if (_action.startsWith(CalqueFindComponentPolygone.getID())) {
      return (_s == null || ((Object[]) _s).length == 0) ? EbliLib.getS("Entr�e vide") : null;
    }
    final String exp = (String) _s;
    if (exp == null || exp.length() == 0) {
      return EbliLib.getS("Entr�e vide");
    }
    if (isSonde(_action)) {
      String r = null;
      final StringTokenizer tk = new StringTokenizer(exp, ";");
      if (tk.countTokens() == 2) {
        try {
          final double x = Double.parseDouble(tk.nextToken());
          final double y = Double.parseDouble(tk.nextToken());
          final ZCalqueSondeInterface target = (ZCalqueSondeInterface) layer_;
          if (!target.isPointSondable(new GrPoint(x, y, 0))) {
            r = EbliLib.getS("Le point � sonder est en dehors des limites");
          }
        } catch (final NumberFormatException _e) {
          FuLog.warning(_e.getMessage());
        }
      } else {
        r = CtuluLib.getS("Donn�es invalides");
      }
      return r;
    }
    return super.erreur(_s, _action, _parent);
  }

  protected final boolean findPolygone(final Object _idx, final String _action, final int _selOption, final EbliFindable _parent) {
    final String modeStr = _action.substring(CalqueFindComponentPolygone.getID().length());
    int mode = 0;
    try {
      mode = Integer.parseInt(modeStr);
    } catch (final NumberFormatException _evt) {
      FuLog.error(_evt);
    }
    if (_idx == null) {
      return false;
    }
    final LinearRing[] rings = (LinearRing[]) _idx;
    if (rings.length == 0) {
      return false;
    }
    final ZCalqueAffichageDonneesInterface target = (ZCalqueAffichageDonneesInterface) layer_;
    return target.changeSelection(rings, _selOption, mode);
  }

  @Override
  public boolean find(final Object _idx, final String _action, final int _selOption, final EbliFindable _parent) {
    if (_action.startsWith(CalqueFindComponentPolygone.getID())) {
      return findPolygone(_idx, _action, _selOption, _parent);
    }
    final String exp = (String) _idx;
    final boolean sonde = isSonde(_action);
    final ZEbliCalquesPanel visu = (ZEbliCalquesPanel) _parent;
    final ZCalqueSondeInteraction s = visu.getController().getCalqueSondeInteraction();
    if (s != null) {
      s.setGele(!sonde);
    }
    if (s != null && sonde) {
      boolean r = false;
      final StringTokenizer tk = new StringTokenizer(exp, ";");
      if (tk.countTokens() == 2) {
        try {
          final double x = Double.parseDouble(tk.nextToken());
          final double y = Double.parseDouble(tk.nextToken());
          final ZCalqueSondeInterface target = (ZCalqueSondeInterface) layer_;
          target.setSondeEnable(true);
          s.setTarget(target);

          r = target.changeSonde(new GrPoint(x, y, 0), EbliSelectionState.ACTION_ADD == _selOption);
          if (r) {
            visu.setInfoPaletteActive();
          }
          return r;
        } catch (final NumberFormatException _e) {
          FuLog.warning(_e.getMessage());
        }
      }
      return r;
    }
    final CtuluListSelection selection = createNewSelection(exp, _action, _selOption);
    if (layer_ instanceof ZCalqueAffichageDonneesInterface) {
      int nombre = ((ZCalqueAffichageDonneesInterface) layer_).modeleDonnees().getNombre();
      for (int i = nombre; i <= selection.getMaxIndex(); i++) {
        selection.remove(i);
      }
    }
    boolean b = selection == null ? false : changeSelection(selection, _selOption, _action);
    if (b) {
      visu.getScene().fireSelectionEvent();
    }
    return selection != null && !selection.isEmpty();
  }

  private boolean isSonde(final String _action) {
    return "SONDE".equals(_action);
  }

  @Override
  public final EbliFindableItem getCalque() {
    return layer_;
  }

  @Override
  public final GrBoite getZoomOnSelected() {
    if (layer_ instanceof ZCalqueSondeInterface) {
      final ZCalqueSondeInterface s = (ZCalqueSondeInterface) layer_;
      if (s.isSondeActive()) {
        final GrPoint p = new GrPoint(s.getSondeX(), s.getSondeY(), 0);
        final GrBoite b = new GrBoite();
        b.e_ = p;
        b.o_ = new GrPoint(p);
        ZCalqueAffichageDonneesAbstract.ajusteZoomOnSelected(b, ((ZCalqueAffichageDonneesInterface) layer_).modeleDonnees());
        return b;
      }
    }
    return ((ZCalqueAffichageDonneesInterface) layer_).getZoomOnSelected();
  }

  @Override
  public final String toString() {
    return ((ZCalqueAffichageDonneesInterface) layer_).getTitle();
  }
}
