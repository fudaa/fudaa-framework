/*
 * @file         DeLigneBrisee.java
 * @creation     2008-03-26
 * @modification $Date: 2008-03-26 14:41:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque.dessin;

import java.awt.Graphics2D;
import java.awt.Point;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.trace.TraceGeometrie;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TracePoint;
/**
 * Un multipoint.
 *
 * @version      $Revision: 1.1.2.1 $ $Date: 2008-03-26 14:41:09 $ by $Author: bmarchan $
 * @author       Bertrand Marchand
 */
public class DeMultiPoint extends DeForme {
  // donnees membres publiques
  // donnees membres privees
  GrPolyligne ligne_;
  // Constructeurs
  public DeMultiPoint() {
    super();
    ligne_= new GrPolyligne();
  }
  public DeMultiPoint(final GrPolyligne _l) {
    super();
    ligne_= _l;
  }

  public int getNombre(){
    return ligne_.nombre();
  }
  @Override
  public int getForme() {
    return MULTI_POINT;
  }
  // Methodes publiques
  @Override
  public GrObjet getGeometrie() {
    return ligne_;
  }
  
  public GrPoint getSommet(int _i) {
    return ligne_.sommet(_i);
  }

  /**
   * @return le dernier point
   */
  public GrPoint getDernier(){
    if(getNombre()>0){
      return ligne_.sommet(getNombre()-1);
    }
    return null;
  }
  public void ajoute(final GrPoint _p) {
    ligne_.sommets_.ajoute(_p);
  }
  
  public void ajoute(int _idx, GrPoint _pt) {
    ligne_.sommets_.insere(_pt, _idx);
  }

  public void enleveDernier(){
    if(ligne_.sommets_.nombre()>1) {
      ligne_.sommets_.enleveDernier();
    }
  }
  
  /**
   * @param _idx L'index a supprimer de la ligne.
   */
  public void enleve(int _idx) {
    if (ligne_.sommets_.nombre()>_idx) {
      ligne_.sommets_.enleve(_idx);
    }
  }
  
  /**
   * Affiche le multipoint en bouclant sur tous les points.
   */
  @Override
  public void affiche(final Graphics2D _g2d,final TraceGeometrie _t, final boolean _rapide) {
    super.affiche(_g2d,_t, _rapide);
    for (int i=0; i<ligne_.nombre(); i++)
      _t.dessinePoint(_g2d,ligne_.sommet(i), _rapide);
  }

  public void affiche(final Graphics2D _g,final TraceIcon _tp,final boolean _rapide,final GrPoint _tmp,final GrMorphisme _versEcran){
    final int nb=ligne_.nombre();
    if(nb==0) {
      return;
    }
    for(int i=nb-1;i>=0;i--){
      _tmp.initialiseAvec(ligne_.sommet(i));
      _tmp.autoApplique(_versEcran);
      _tp.paintIconCentre(_g, _tmp.x_, _tmp.y_);
    }
  }

  /**
   * Affiche le multipoint en bouclant sur tous les points.
   */
  public void affiche(final Graphics2D _g,final TracePoint _tp,final boolean _rapide,final GrPoint _tmp,final GrMorphisme _versEcran){
    final int nb=ligne_.nombre();
    if(nb==0) {
      return;
    }
    for(int i=nb-1;i>=0;i--){
      _tmp.initialiseAvec(ligne_.sommet(i));
      _tmp.autoApplique(_versEcran);
      _tp.dessinePoint(_g, _tmp.x_, _tmp.y_);
    }
  }
  // >>> Interface GrContour ---------------------------------------------------
  /**
   * Retourne les points du contour de l'objet.
   *
   * @return Les points de contour.
   * @see    org.fudaa.ebli.geometrie.GrContour
   */
  @Override
  public GrPoint[] contour() {
    return ligne_.contour();
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    for (int i=0; i<ligne_.nombre(); i++) {
      if (ligne_.sommet(i).estSelectionne(_ecran, _dist, _pt)) return true;
    }
    return false;
  }
  // <<< Interface GrContour ---------------------------------------------------
  // >>> Implementation GrObjet pour �tre d�placable ---------------------------
  @Override
  public void autoApplique(final GrMorphisme _t) {
    ligne_.autoApplique(_t);
  }
  /**
   * Retourne la boite englobante de l'objet.
   */
  @Override
  public GrBoite boite() {
    return ligne_.boite();
  }
  // <<< Implementation GrObjet ------------------------------------------------
}
