/*
 *  @creation     15 sept. 2005
 *  @modification $Date: 2006-09-19 14:55:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import org.fudaa.ebli.commun.EbliUIProperties;

/**
 * @author Fred Deniger
 * @version $Id: BCalqueSaverSingle.java,v 1.7 2006-09-19 14:55:47 deniger Exp $
 */
public class BCalqueSaverSingle implements BCalqueSaverInterface {

  private String id_;
  //non final pour db4o
  private EbliUIProperties ui_;

  protected BCalqueSaverSingle(final EbliUIProperties _ui, final String _id) {
    ui_ = _ui;
    id_ = _id;
  }

  protected BCalqueSaverSingle(final BCalqueSaverInterface _ui, final String _id) {
    ui_ = _ui.getUI();
    id_ = _id;

  }

  public static final String getPersistanceClassId() {
    return "persistence.class";
  }

  public final void setPersistenceClassFrom(final Object _s) {
    ui_.put(getPersistanceClassId(), _s.getClass().getName());
  }

  public final void setPersistenceClass(final String _s) {
    ui_.put(getPersistanceClassId(), _s);
  }

  @Override
  public final String getPersistenceClass() {
    return ui_.getString(getPersistanceClassId());
  }

  public BCalqueSaverSingle() {
    ui_=new EbliUIProperties();
    
  }

  public BCalqueSaverSingle(final BCalque _cqToSave) {
    ui_ = _cqToSave.saveUIProperties();
  }

  @Override
  public String getId() {
    return id_;
  }

  @Override
  public final String getLayerName() {
    return ui_==null?null:ui_.getLayerName();
  }

  @Override
  public EbliUIProperties getUI() {
    return ui_;
  }

}
