/*
 *  @creation     1 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuResource;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.commun.*;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.trace.TraceIcon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

/**
 * Classe de gestion des outils/commandes d'�dition d'un calque. Cette classe g�re le undo/redo sur les objets manipul�s, met � jour le panneau des
 * outils, contient les actions possibles, etc.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class ZEditorDefault implements ZCalqueEditionInteractionTargetI, ZEditorInterface,
    PropertyChangeListener, ZCalqueDeplacementTargetInterface {
  class CommandSaisieMultiple implements CtuluCommandPersitant {
    @Override
    public boolean canUndo() {
      return dessin_ != null && dessin_.isEnCours();
    }

    @Override
    public void undo() {
      dessin_.getFormeEnCours().removeLastSaisie();
    }
  }

  class NodeAddListener implements ZCalqueClikInteractionListener {
    @Override
    public void pointClicked(GrPoint _ptReel) {
      if (getSupport().canUseAtomicMode(getSupport().getCalqueActif())) {
        int idGeom = getSupport().getSelectionHelper().getUniqueSelectedIdx();
        if (idGeom != -1) {
          CtuluCommandComposite cmp = new CtuluCommandComposite(EbliLib.getS("Ajout d'un sommet"));
          if (((ZCalqueEditable) getSupport().getLayerForId(idGeom)).addAtome(_ptReel, cmp, null)) {
            sceneEditor_.getScene().repainSelection();
            if (mng_ != null) {
              mng_.addCmd(cmp.getSimplify());
            }
          }
        }
      }
    }
  }

  private String state_;
  private boolean displayInfo = true;
  // protected ZAttributeEditorMapper attrEditors_ = new
  // ZAttributeEditorMapper();
  BuDesktop desktop_;
  ZCalqueEditionInteraction dessin_;
  protected ZEditorPanelInterface editorPanel_;
  CtuluCommandManager mng_;
  protected ZSceneEditor sceneEditor_ = null;
  ZCalqueDeplacementInteraction move_;
  ZCalqueClickInteraction nodeAdd_;
  protected BPaletteEdition palette_;
  ZEbliCalquesPanel panel_;
  protected ZEditorValidatorI editorValidator_;

  public boolean isDisplayInfo() {
    return displayInfo;
  }

  public void setDisplayInfo(boolean displayInfo) {
    this.displayInfo = displayInfo;
  }

  //  ZEditorRotationPanel pnRotation_=null;
  public final EbliActionSimple getActionCancel() {
    return actionCancel_;
  }

  @Override
  public void setMessage(final String _s) {
    panel_.setInfoText(_s);
  }

  @Override
  public void unsetMessage() {
    panel_.setInfoText(null);
  }

  public final EbliActionSimple getActionDelete() {
    return actionDelete_;
  }

  public void repaintPanel() {
    panel_.getVueCalque().repaint();
  }

  CommandSaisieMultiple saisie_;
  protected ZCalqueEditable target_;

  /**
   * @return l'interface utilisateur
   */
  public final CtuluUI getUi() {
    return ui_ == null ? sceneEditor_.getUI() : ui_;
  }

  /**
   * @param _ui l'interface utilisateur
   */
  public final void setUi(final CtuluUI _ui) {
    ui_ = _ui;
    sceneEditor_.setUI(_ui);
  }

  public final BPaletteEdition getEditionPalette() {
    return palette_;
  }

  public ZEbliCalquesPanel getPanel() {
    return panel_;
  }

  /**
   * @return la cible de cet editeur
   */
  public final ZCalqueEditable getTarget() {
    return target_;
  }

  public ZEditorDefault(final ZEbliCalquesPanel _panel) {
    this(_panel, new ZSceneEditor(_panel.getScene(), _panel));
  }

  /**
   * @param _panel le panneau des calques
   */
  protected ZEditorDefault(final ZEbliCalquesPanel _panel, final ZSceneEditor _editor) {
    super();
    panel_ = _panel;
    sceneEditor_ = _editor;
    setMng(new CtuluCommandManager());
    // panel_.addKeyListener(this);
    buildActions();
    EbliLib.updateMapKeyStroke(_panel, getDeleteActions());
    installSceneActions();
  }

  /**
   * Installation des actions de scene. En particulier, rend visible les actions de scene dans le menu contextuel de la fenetre 2D.
   */
  protected void installSceneActions() {
  }

  private void activeCurrentState() {
    boolean updateDessinLayer = false;
    if ("MODE_ATOME".equals(state_)) {
      //TODO Pour moi state_ vaut jamais "MODE_ATOME". A Verifier.
//      getSupport().setAtomicMode(true);
    } else if (isStateGlobalMove()) {
      panel_.setCalqueInteractionActif(buildDeplacement());
    } else if ("ATOME_ADD".equals(state_)) {
      panel_.setCalqueInteractionActif(buildNodeAdd());
    } else if (isCalqueDessin(state_)) {
      updateDessinLayer = true;
      panel_.setCalqueInteractionActif(buildCalqueDessin(state_));
    }
    updateEditorPanel();
    if (palette_ == null) {
      return;
    }
    if (editorPanel_ == null) {
      palette_.setEditorPanel(null);
    } else {
      palette_.setEditorPanel(editorPanel_.getComponent());
      // pour le calque dessin, on lui ajouter le constructeur d'attribut
      if (updateDessinLayer) {
        dessin_.setFeatures(editorPanel_.getAttributeContainer());
      }
    }
    palette_.revalidate();
    Object o = SwingUtilities.getAncestorOfClass(JInternalFrame.class, palette_);
    if (o == null) {
      o = SwingUtilities.getAncestorOfClass(JDialog.class, palette_);
      if (o != null) {
        ((JDialog) o).pack();
      }
    } else {
      ((JInternalFrame) o).pack();
    }
  }

  public ZCalqueEditionInteraction getCalqueDessin() {
    return dessin_;
  }

  private boolean isStateGlobalMove() {
    return "GLOBAL_MOVE".equals(state_);
  }

  private ZCalqueDeplacementInteraction buildDeplacement() {
    if (move_ == null) {
      move_ = new ZCalqueDeplacementInteraction(panel_.getCqSelectionI());
      move_.addPropertyChangeListener("gele", this);
      panel_.addCalqueInteraction(move_);
      move_.setTarget(this);
    }
    return move_;
  }

  private ZCalqueClickInteraction buildNodeAdd() {
    if (nodeAdd_ == null) {
      nodeAdd_ = new ZCalqueClickInteraction();
      nodeAdd_.setDescription(EbliLib.getS("Ajouter un sommet"));
      nodeAdd_.setName("cqNodeAdd");
      nodeAdd_.addPropertyChangeListener("gele", this);
      nodeAdd_.setListener(new NodeAddListener());
      panel_.addCalqueInteraction(nodeAdd_);
    }
    return nodeAdd_;
  }

  private void unactiveCurrentState() {
    if ("MODE_ATOME".equals(state_)) {
      //TODO Pour moi state_ vaut jamais "MODE_ATOME". A Verifier.
//      getSupport().setAtomicMode(false);
    } else if (dessin_ != null && !dessin_.isGele()) {
      dessin_.cancelEdition();
      panel_.unsetCalqueInteractionActif(dessin_);
    } else if (move_ != null && !move_.isGele()) {
      panel_.unsetCalqueInteractionActif(move_);
    } else if (nodeAdd_ != null && !nodeAdd_.isGele()) {
      panel_.unsetCalqueInteractionActif(nodeAdd_);
    }
    if (palette_ != null) {
      palette_.setEditorPanel(null);
    }
  }

  /**
   * Retourne le panneau de parametre pour l'outil s�lectionn�. Si aucun outil s�lectionn� ou si outil n'a pas de panneau, retourne null.
   *
   * @return Le panneau de parametres.
   */
  protected ZEditorPanelInterface getToolPanel() {
    ZEditorPanelInterface newPanel = null;
    if (isCalqueDessin(state_)) {
      if (palette_ != null) {
        // Recr��, car peut changer en fonction de la forme en cours.
        newPanel = new ZEditorLigneBriseePanel(this);
      }
    } else if (isStateGlobalMove()) {
      newPanel = new ZCalqueDeplacementPanel(move_, getXYFormatter());
    }
    if (newPanel != null) {
      newPanel.targetChanged(getTarget());
    }
    return newPanel;
  }

  private void updateEditorPanel() {
    final ZEditorPanelInterface newPanel = getToolPanel();
    if (editorPanel_ != null) {
      editorPanel_.editionStopped();
    }
    if (editorPanel_ != newPanel) {
      if (editorPanel_ != null) {
        editorPanel_.close();
      }
      editorPanel_ = newPanel;
    }
  }

  public void updateAtomicModeFromSupport() {
    if (palette_ != null) {
      palette_.setAtomeSelected(getSupport().getSelectionMode() == SelectionMode.ATOMIC);
    }
  }

  /**
   * Methode appelee a chaque mise � jour de l'etat de la palette.
   */
  public void updatePalette() {
    if (palette_ == null) {
      return;
    }
    if (target_ == null) {
      palette_.setAllEnable(false);
      palette_.checkEnableAndCheckBt();
      changeState(null);
    } else {
      palette_.setAllEnable(true);

      SelectionMode mode = palette_.getSelectionMode();
      getSupport().setSelectionMode(mode);
      palette_.setAtomeEnable(getSupport().canUseAtomicMode(target_), getSupport().getSelectionMode() == SelectionMode.ATOMIC);
      palette_.setSegmentEnable(getSupport().canUseSegmentMode(target_), getSupport().getSelectionMode() == SelectionMode.SEGMENT);

      boolean isModifiable = true;
      if (target_.getModelEditable() != null) {
        if (target_.getModelEditable().getGeomData() != null) {
          isModifiable = target_.getModelEditable().getGeomData().isGeomModifiable();
        }
      }
      palette_.setEnable("GLOBAL_ADD_POINT", target_.canAddForme(DeForme.POINT) && isModifiable);
      palette_.setEnable("GLOBAL_ADD_POLYLIGNE", target_.canAddForme(DeForme.LIGNE_BRISEE) && isModifiable);
      palette_.setEnable("GLOBAL_ADD_RECTANGLE", target_.canAddForme(DeForme.RECTANGLE) && isModifiable);
      palette_.setEnable("GLOBAL_ADD_ELLIPSE", target_.canAddForme(DeForme.ELLIPSE) && isModifiable);
      palette_.setEnable("GLOBAL_ADD_POLYGONE", target_.canAddForme(DeForme.POLYGONE) && isModifiable);
      palette_.setEnable("GLOBAL_ADD_SEMIS", target_.canAddForme(DeForme.MULTI_POINT) && isModifiable);
    }

    updatePaletteWhenSelectionChanged();
    palette_.checkEnableAndCheckBt();
    final AbstractButton bt = palette_.getSelectedButton();
    if (bt == null) {
      changeState(null);
    } else {
      changeState(bt.getActionCommand());
    }
  }

  public void updatePaletteWhenSelectionChanged() {
    if (palette_ == null) {
      return;
    }

    ZScene scn = getSupport();
    SceneSelectionHelper hlp = scn.getSelectionHelper();
    int idGeom = -1;
    //si la geometrie n'est pas editable, on ne peut rien faire.
    boolean isModifiable = false;
    if (target_ != null && target_.getModelEditable() != null) {
      if (target_.getModelEditable().getGeomData() != null) {
        isModifiable = target_.getModelEditable().getGeomData().isGeomModifiable();
      }
    }

    boolean b = isModifiable;
    // Si la selection est sur le m�me objet.
    b = b && (idGeom = hlp.getUniqueSelectedIdx()) != -1;
    // Si le nombre d'atomiques est de 2 cons�cutifs sur une g�om�trie de type polyligne.
    if (b && scn.isAtomicMode()) {
      if (scn.getObject(idGeom) instanceof GISCoordinateSequenceContainerInterface) {
        GISCoordinateSequenceContainerInterface geom = (GISCoordinateSequenceContainerInterface) scn.getObject(idGeom);
        if (!(geom instanceof GISMultiPoint)) {
          if (hlp.getNbAtomicSelected() == 1) {
            b = b
                && (hlp.getUniqueAtomicSelection().getMinIndex() == 0 || hlp.getUniqueAtomicSelection().getMaxIndex() + 1 == geom.
                getCoordinateSequence().size());
          } else if (hlp.getNbAtomicSelected() == 2) {
            b = b && Math.abs(hlp.getUniqueAtomicSelection().getMinIndex() - hlp.getUniqueAtomicSelection().getMaxIndex()) == 1;
          } else {
            b = false;
          }
        }
      }
    }

    palette_.setEnable("ATOME_ADD", b);
  }

  private boolean stateChanging;

  /**
   * @return true if the state of the button is under modification.
   */
  public boolean isStateChanging() {
    return stateChanging;
  }

  protected boolean changeState(final String _s) {
    stateChanging = true;
    // Pas de changement: le cas null,null et meme etat
    if (_s == state_ || (_s != null && _s.equals(state_))) {
      return false;
    }
    unactiveCurrentState();
    state_ = _s;
    activeCurrentState();
    stateChanging = false;
    final Object o = SwingUtilities.getAncestorOfClass(JInternalFrame.class, palette_);
    if (o != null) {
      ((JInternalFrame) o).revalidate();
    }
    return true;
  }

  protected String getCurrentAction() {
    return state_;
  }

  @Override
  public void atomicChanged() {
    // on prend en compte la saisie dans les undo/redo
    if (mng_ != null && (dessin_.isEnCours() && (saisie_ == null || mng_.getCmdPersistant() != saisie_))) {
      if (saisie_ == null) {
        saisie_ = new CommandSaisieMultiple();
      }
      mng_.setCmdPersistant(saisie_);
    }
    // mise a jour de l'editeur
    if (editorPanel_ != null) {
      editorPanel_.atomicChanged();
    }
  }

  /**
   * @param _com la commande
   * @return le calque de dessin
   */
  public final ZCalqueEditionInteraction buildCalqueDessin(final String _com) {
    int forme = getForme();
    if (dessin_ == null) {
      dessin_ = new ZCalqueEditionInteraction(this);
      panel_.getVueCalque().addKeyListener(dessin_);
      dessin_.setName("cqInteractifDessin");
      panel_.addCalqueInteraction(dessin_);
      dessin_.setGele(true);
      dessin_.addPropertyChangeListener("gele", this);
    }
    dessin_.setTypeForme(forme);
    return dessin_;
  }

  /**
   * @return La forme en construction en fonction de l'action en cours.
   */
  protected int getForme() {
    int forme = -1;
    if ("GLOBAL_ADD_POINT".equals(state_)) {
      forme = DeForme.POINT;
    } else if ("GLOBAL_ADD_POLYLIGNE".equals(state_)) {
      forme = DeForme.LIGNE_BRISEE;
    } else if ("GLOBAL_ADD_POLYGONE".equals(state_)) {
      forme = DeForme.POLYGONE;
    } else if ("GLOBAL_ADD_RECTANGLE".equals(state_)) {
      forme = DeForme.RECTANGLE;
    } else if ("GLOBAL_ADD_ELLIPSE".equals(state_)) {
      forme = DeForme.ELLIPSE;
    } else if ("GLOBAL_ADD_SEMIS".equals(state_)) {
      forme = DeForme.MULTI_POINT;
    }
    return forme;
  }

  EbliActionSimple actionDelete_;
  EbliActionSimple actionCancel_;

  public final EbliActionInterface[] getDeleteActions() {
    return new EbliActionInterface[]{actionDelete_, actionCancel_};
  }

  private void buildActions() {
    actionDelete_ = new EbliActionSimple(CtuluLib.getS("Supprimer"), BuResource.BU.getIcon("enlever"), "DeleteSelectedAction") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        delete();
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(target_ != null && !target_.isSelectionEmpty());
      }
    };
    actionDelete_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));

    actionCancel_ = new EbliActionSimple(CtuluLib.getS("Annuler"), BuResource.BU.getIcon("annuler"), "CancelEditionAction") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        cancelEdition();
      }

      @Override
      public void updateStateBeforeShow() {
        super.setEnabled(dessin_ != null && !dessin_.isGele() && dessin_.isEnCours());
      }
    };
    actionCancel_.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
  }

  /**
   * Si en cours de cr�ation de forme: la forme est annulee, sinon les points s�lectionn�s sont supprim�s.
   */
  private void delete() {
    if (cancelEdition()) {
      return;
    }
    sceneEditor_.removeSelectedObjects(getMng());
  }

  public boolean cancelEdition() {
    if (dessin_ != null && dessin_.isEnCours()) {
      dessin_.cancelEdition();
      return true;
    }
    return false;
  }

  @Override
  public void doAction(final String _com, final BPaletteEdition _emitter) {
    FuLog.trace("EBL:Editor doAction");
    if (("MODE_ATOME".equals(_com)) || ("MODE_SEGMENT".equals(_com))) {
      updatePalette();
      // on change de type de selection
//      if (target_ != null) {
//        target_.clearSelection();
//      }
//      getSupport().clearSelection();

    } else {
      if (_com.equals(state_)) {
        changeState(null);
      } else {
        changeState(_com);
      }
    }
  }

  @Override
  public ZScene getSupport() {
    return panel_.getScene();
  }

  public ZSceneEditor getSceneEditor() {
    return sceneEditor_;
  }

  public void setSceneEditor(ZSceneEditor _edit) {
    sceneEditor_ = _edit;
    sceneEditor_.setUI(getUi());
    sceneEditor_.setMng(getMng());
  }
  
  /**
   * Le validateur pour l'�dition des objets selectionn�s. Peut �tre null.
   */
  public void setEditionValidator(ZEditorValidatorI _validatator) {
    editorValidator_ = _validatator;
  }

  public final BuDesktop getDesktop() {
    return desktop_;
  }

  /**
   * @return le receveur de commandes
   */
  public final CtuluCommandManager getMng() {
    return mng_;
  }

  public Frame getFrame() {
    return CtuluLibSwing.getFrameAncestorHelper(getUi().getParentComponent());
  }

  /**
   * @deprecated Use getCoordinateDefinitions() instead.
   */
  public EbliFormatterInterface getXYFormatter() {
    return panel_.getEbliFormatter();
  }

  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return panel_.getCoordinateDefinitions();
  }

  /**
   * @param _com la commande
   * @return true si est une commande de dessin (point, poly, rectangle,...)
   */
  public boolean isCalqueDessin(final String _com) {
    return _com != null && _com.startsWith("GLOBAL_ADD");
  }

  @Override
  public boolean isEditable(final Object _target) {
    return (_target instanceof ZCalqueEditable) && ((ZCalqueEditable) _target).isEditable();
  }

  // Pas sur que la m�thode soit utilis�.
  public void keyReleased(final KeyEvent _e) {
//    final BCalque calque = getSupport().getCalqueActif();
//    if (calque != null && _e.getKeyCode() == KeyEvent.VK_DELETE && isEditable(calque)) {
//      if (Fu.DEBUG && FuLog.isDebug()) {
//        FuLog.warning("EBL: remove selected object on selected layer only");
//      }
//      ((ZCalqueEditable) calque).removeSelectedObjects(mng_, null);
//    }
    if (_e.getKeyCode() == KeyEvent.VK_DELETE) {
      delete();
    } else if (_e.getKeyCode() == KeyEvent.VK_ESCAPE && dessin_ != null) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("EBL: CANCEL EDITION");
      }
      dessin_.cancelEdition();
    }
  }

  @Override
  public void moved(double _dx, double _dy, double _dz, boolean _confirmOnZ) {
    if (_confirmOnZ && _dz != 0
        && !ui_.question(EbliLib.getS("Accroche suivant Z"),
        EbliLib.getS("Voulez vous translater les objets\ns�lectionn�s suivant DZ={0} ?",
            getXYFormatter().getXYFormatter().format(_dz)))) {
      _dz = 0;
    }

    sceneEditor_.moveSelectedObjects(_dx, _dy, _dz);
  }

  @Override
  public void paintMove(final Graphics2D _g, final int _dx, final int _dy, final TraceIcon _ic) {
    ZCalqueEditable[] cqs = getSupport().getEditableLayers();
//    if (target_ != null) {
//      target_.paintDeplacement(_g, _dx, _dy, _ic);
//    }
    for (int i = 0; i < cqs.length; i++) {
      cqs[i].paintDeplacement(_g, _dx, _dy, _ic);
    }
  }

  @Override
  public boolean addNewPoint(final GrPoint _pt, final ZEditionAttributesDataI _d) {
    if (target_ != null) {
      final boolean r = target_.addForme(_pt, DeForme.POINT, mng_, ui_, _d);
      if (r && editorPanel_ != null) {
        final String msg = EbliLib.getS("point ajout�: {0}", _pt.toString());
        FuLog.warning("MOD:" + msg);
        editorPanel_.objectAdded();
        if (displayInfo) {
          getUi().message(null, msg, true);
        }
      }
      return r;
    }
    return false;
  }

  @Override
  public boolean addNewPolygone(final GrPolygone _pt, final ZEditionAttributesDataI _d) {
    if (target_ != null) {
      if (FuLog.isTrace()) {
        FuLog.trace("EBL: ajout polygone");
      }
      final boolean r = target_.addForme(_pt, DeForme.POLYGONE, mng_, ui_, _d);
      if (r && editorPanel_ != null) {
        final String msg = EbliLib.getS("ligne ferm�e avec {0} sommets ajout�e", Integer.toString(_pt.sommets_.nombre()));
        FuLog.warning("MOD:" + msg);
        editorPanel_.objectAdded();
        if (displayInfo && getUi() != null) {
          getUi().message(null, msg, true);
        }
      }
      return r;
    }
    return false;
  }

  @Override
  public boolean addNewMultiPoint(final GrPolyligne _pt, final ZEditionAttributesDataI _d) {
    if (target_ != null) {
      if (FuLog.isTrace()) {
        FuLog.trace("EBL: ajout semis");
      }
      final boolean r = target_.addForme(_pt, DeForme.MULTI_POINT, mng_, ui_, _d);
      if (r && editorPanel_ != null) {
        final String msg = EbliLib.getS("{0} points ajout�s", Integer.toString(_pt.sommets_.nombre()));
        FuLog.warning("MOD:" + msg);
        editorPanel_.objectAdded();
        if (displayInfo) {
          getUi().message(null, msg, true);
        }
      }
      return r;
    }
    return false;
  }

  @Override
  public boolean addNewPolyligne(final GrPolyligne _pt, final ZEditionAttributesDataI _d) {
    if (target_ != null) {
      if (FuLog.isTrace()) {
        FuLog.trace("EBL: ajout polyligne");
      }
      final boolean r = target_.addForme(_pt, DeForme.LIGNE_BRISEE, mng_, ui_, _d);
      if (r && editorPanel_ != null) {
        final String msg = EbliLib.getS("ligne ouverte avec {0} sommets ajout�e", Integer.toString(_pt.sommets_.nombre()));
        FuLog.warning("MOD:" + msg);
        editorPanel_.objectAdded();
        if (displayInfo) {
          getUi().message(null, msg, true);
        }
      }
      return r;
    }
    return false;
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (_evt.getSource() == dessin_ && isCalqueDessin(state_)) {
      if (editorPanel_ != null) {
        editorPanel_.calqueInteractionChanged();
      }
    } // on desactive le bouton GLOBAL_MOVE sur le calque associ� devient gel�.
    else if (_evt.getSource() == move_ && move_ != null && move_.isGele()) {
      final AbstractButton bt = palette_.getGlobalButton("GLOBAL_MOVE");
      if (bt != null && bt.isSelected()) {
        bt.doClick();
      }
    } else if (nodeAdd_ != null && _evt.getSource() == nodeAdd_ && nodeAdd_.isGele()) {
      final AbstractButton bt = palette_.getGlobalButton("ATOME_ADD");
      if (bt != null && bt.isSelected()) {
        bt.doClick();
      }
    } // dans ce cas il s'agit de mettre a jour le caractere atomique
    else if (_evt.getSource() == target_) {
      if ("nodeEdit".equals(_evt.getPropertyName())) {
        // Voir comment g�rer pour les segments.
        palette_.setAtomeEnable(getSupport().canUseAtomicMode(target_), getSupport().getSelectionMode() == SelectionMode.ATOMIC);
      } else if (dessin_ != null && !dessin_.isGele() && dessin_.isLigneEncours()) {
        dessin_.repaint();
      }
    }
  }

  CtuluUI ui_;

  @Override
  public void setActivated(final Object _o, final BPaletteEdition _palette) {
    if (palette_ == _palette && _o == target_) {
      updatePalette();
      return;
    }

    palette_ = _palette;
    if (target_ != null) {
      target_.removePropertyChangeListener("nodeEdit", this);
      target_.removePropertyChangeListener("versEcran", this);
    }
    if (_o instanceof ZCalqueEditable) {
      target_ = (ZCalqueEditable) _o;
    } else {
      target_ = null;
    }
    if (target_ != null) {
      target_.addPropertyChangeListener("nodeEdit", this);
      target_.addPropertyChangeListener("versEcran", this);
    }
    updatePalette();
    activeCurrentState();
  }

  @Override
  public final void setDesktop(final BuDesktop _desktop) {
    desktop_ = _desktop;
  }

  /**
   * @param _mng le receveur de commandes
   */
  public final void setMng(final CtuluCommandManager _mng) {
    mng_ = _mng;
    sceneEditor_.setMng(_mng);
  }

  @Override
  public String edit() {
    // R�cup�ration des calques contenant des g�om�tries selectionn�es \\
    ZCalqueAffichageDonneesInterface[] calques = panel_.getScene().getAllLayers();
    ArrayList<ZCalqueAffichageDonneesInterface> calquesWithSelectedObjects = new ArrayList<ZCalqueAffichageDonneesInterface>();
    for (int i = 0; i < calques.length; i++) {
      if (!calques[i].isSelectionEmpty()) {
        calquesWithSelectedObjects.add(calques[i]);
      }
    }
    // Verification du nombre de calques contenant des selections
    if (calquesWithSelectedObjects.size() == 0) {
      return EbliLib.getS("La s�lection courante est vide");
    } 
    else if (calquesWithSelectedObjects.size() > 1) {
      ui_.warn(EbliLib.getS("Edition impossible"),
          EbliLib.getS("L'�dition ne peut se faire simultan�ment\nque sur des g�om�tries d'un m�me calque."), false);
      return EbliLib.getS("L'�dition ne peut se faire simultan�ment\nque sur des g�om�tries d'un m�me calque.");
    }
    if (!(calquesWithSelectedObjects.get(0) instanceof ZCalqueEditable)) { // Calque n'impl�mentant pas l'interface ZCalqueEditable, donc on le laisse g�rer son �dition.
      calquesWithSelectedObjects.get(0).editSelected();
    } 
    else {
      ZCalqueEditable calque = (ZCalqueEditable) calquesWithSelectedObjects.get(0);
      // G�n�ration d'une boite d'�dition selon le type de selection et
      // d'�dition \\
      if (calque.getSelectionMode() == SelectionMode.ATOMIC) {
        // On controle qu'en mode sommet, une seule g�om�trie est selectionn�e.
        if (getSupport().getSelectionHelper().getUniqueSelectedIdx() == -1) {
          ui_.warn(EbliLib.getS("Edition impossible"), EbliLib.getS(
              "L'�dition ne peut se faire simultan�ment que sur\n des sommets d'une m�me g�om�trie."), false);
          return EbliLib.getS("L'�dition ne peut se faire simultan�ment que sur\n des sommets d'une m�me g�om�trie.");
        }
        editVertexObject(calque);
      } 
      else if (calque.getSelectionMode() == SelectionMode.SEGMENT) {
        editSegmentObject(calque);
      } 
      else if (calque.isOnlyOneObjectSelected()) { // Une seule g�om�trie est � �diter
        editSingleObject(calque);
      } 
      else {
        // Plusieurs g�om�tries sont � �diter
        EbliObjetsEditorPanel pn = new EbliObjetsEditorPanel(calque.getModelEditable(),
            calque.getLayerSelection().getSelectedIndex(), getMng(), getCoordinateDefinitions(), editorValidator_);
//        if (calque.getModelEditable().getGeomData() instanceof GISZoneCollectionPoint)
//          pn=GISGuiBuilder.buildFor((GISZoneCollectionPoint)calque.getModelEditable().getGeomData(), calque.getLayerSelection()
//              .getSelectedIndex(), getMng());
//        else
//          pn=GISGuiBuilder.buildForGlobAtt(calque.getModelEditable().getGeomData(), calque.getLayerSelection().getSelectedIndex(),
//              getMng());
        if (!pn.hasEditableData()) {
          getUi().message(calque.getTitle(), EbliLib.getS("Aucune donn�e � �diter"), false);
        } 
        else {
          pn.afficheModale(getFrame(), calque.getTitle());
        }
      }
    }
    return null;
  }

  protected void configureEditorPanel(JComponent jc) {

  }

  /**
   * L'�dition pour un objet selectionn� unique.
   *
   * @param _target Le calque cible editable.
   */
  protected void editSingleObject(final ZCalqueEditable _target) {
    if (_target.getModelEditable() == null) {
      return;
    }

    final int idxSelected = ((ZCalqueAffichageDonnees) _target).getLayerSelection().getMaxIndex();
    final EbliSingleObjectEditorPanel ed = new EbliSingleObjectEditorPanel(_target.getModelEditable(), idxSelected,
        true, true, getCoordinateDefinitions(), editorValidator_);
    configureEditorPanel(ed);
    ed.setCmd(getMng());
    ed.afficheModale(getFrame(), _target.getTitle(), CtuluDialog.OK_CANCEL_APPLY_OPTION, null);
  }

  protected void editVertexObject(final ZCalqueEditable _target) {
    if (_target.getModelEditable() == null) {
      return;
    }

    final EbliListeSelectionMultiInterface idxSelected = ((ZCalqueAffichageDonnees) _target).getLayerSelectionMulti();
    if (idxSelected.getNbListSelected() > 1) {
      return;
    }
    final TIntObjectIterator it = idxSelected.getIterator();
    it.advance();
    final int idx = it.key();
    final int[] vertex = ((CtuluListSelectionInterface) it.value()).getSelectedIndex();
    final boolean editAttribute = true;
    final EbliAtomicsEditorPanel ed = new EbliAtomicsEditorPanel(idx, vertex, getCoordinateDefinitions(),
        _target.getModelEditable(), editAttribute, editorValidator_, getMng());
    
    configureEditorPanel(ed);
    ed.afficheModale(getFrame(), _target.getTitle());
  }

  protected void editSegmentObject(final ZCalqueEditable _target) {
    if (!(_target.getModelEditable() instanceof ZModeleLigneBriseeEditable)) {
      return;
    }
    final EbliListeSelectionMultiInterface idxSelected = ((ZCalqueAffichageDonnees) _target).getLayerSelectionMulti();
    if (idxSelected.getNbListSelected() > 1) {
      return;
    }
    final TIntObjectIterator it = idxSelected.getIterator();
    it.advance();
    final int idx = it.key();
    final int[] segments = ((CtuluListSelectionInterface) it.value()).getSelectedIndex();

    final boolean editAttribute = true;
    final EbliSegmentsEditorPanel ed = new EbliSegmentsEditorPanel(idx, segments, getCoordinateDefinitions(),
        (ZModeleLigneBriseeEditable) _target.getModelEditable(),
        editAttribute, editorValidator_, getMng());
    configureEditorPanel(ed);
    ed.afficheModale(getFrame(), _target.getTitle());
  }

  public void visuSelectedLayer() {
    final BCalque cq = this.getPanel().getArbreCalqueModel().getSelectedCalque();
    if (!(cq instanceof ZCalqueAffichageDonnees) || ((ZCalqueAffichageDonnees) cq).isSelectionEmpty()) {
      return;
    }
    if (!(((ZCalqueAffichageDonnees) cq).modeleDonnees() instanceof ZModeleGeometry)) {
      return;
    }
    final ZCalqueAffichageDonnees cqDonnees = (ZCalqueAffichageDonnees) cq;
    int idxGeom = -1;
    int[] idxVertex = null;

    final EbliListeSelectionMultiInterface idxSelected = cqDonnees.getLayerSelectionMulti();
    if (idxSelected != null && idxSelected.getNbListSelected() == 1) {
      final TIntObjectIterator it = idxSelected.getIterator();
      it.advance();
      idxGeom = it.key();
      idxVertex = ((CtuluListSelectionInterface) it.value()).getSelectedIndex();
    } else {
      final CtuluListSelectionInterface selec = cqDonnees.getLayerSelection();
      if (selec.getNbSelectedIndex() == 1) {
        idxGeom = selec.getMaxIndex();
      }
    }
    if (idxGeom < 0) {
      return;
    }
    final EbliSingleGeomVisuPanel ed = new EbliSingleGeomVisuPanel(((ZModeleGeometry) cqDonnees.modeleDonnees()).getGeomData(),
        idxGeom, idxVertex, getCoordinateDefinitions());
    ed.afficheModale(getFrame(), cqDonnees.getTitle());
  }

  /*
   * (non-Javadoc) @see org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI#pointMove(double, double)
   */
  @Override
  public void pointMove(double _x, double _y) {
    // mise a jour de l'editeur
    if (editorPanel_ != null) {
      editorPanel_.pointMove(_x, _y);
    }
  }
}
