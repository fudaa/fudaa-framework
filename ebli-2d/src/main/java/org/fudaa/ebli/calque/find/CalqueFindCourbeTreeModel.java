/*
 * @creation 21 nov. 06
 *
 * @modification $Date: 2007-05-04 13:49:46 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.find;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gui.CtuluCellRenderer;
import org.fudaa.ebli.calque.BArbreCalqueModel;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZCalqueLigneBrisee;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.tree.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Une classe permettant d'avoir un TreeModel repr�sentant les calques conteant des lignes bris�es et les lignes bris�es Il est possible d'avoir que
 * les calques contenant des lignes ferm�es. Ce mod�le est automatiquement mis a jour si une instance de l'arbre des calque lui est fourni.
 *
 * @author fred deniger
 * @version $Id: CalqueFindCourbeTreeModel.java,v 1.5 2007-05-04 13:49:46 deniger Exp $
 */
public class CalqueFindCourbeTreeModel implements Observer {
  final BGroupeCalque rootLayer_;
  LayerNode rootNode_;
  boolean onlyLigneFerme_;
  final CalqueFindPolyligneVisitor finder_ = new CalqueFindPolyligneVisitor(false);

  public static class CellRenderer extends JLabel implements TreeCellRenderer {
    final Color selectedBackground_ = UIManager.getColor("Tree.selectionBackground");
    final Color selectedForground_ = UIManager.getColor("Tree.selectionForeground");
    final Border focusBorderColor_ = BorderFactory.createLineBorder(UIManager.getColor("Tree.selectionBorderColor"), 1);
    final boolean onlyLineSelectable_;

    public CellRenderer(final boolean _onlyLineSelectable) {
      this.setOpaque(true);
      setPreferredSize(new Dimension(120, 25));
      onlyLineSelectable_ = _onlyLineSelectable;
    }

    protected boolean isSelectable(final Object _value, final boolean _leaf) {
      if (onlyLineSelectable_) {
        if (!_leaf) {
          return false;
        }
        final LayerNode node = (LayerNode) _value;
        return node.idxPoly_ >= 0;
      }
      return true;
    }

    @Override
    public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
                                                  final boolean _expanded, final boolean _leaf, final int _row, final boolean _hasFocus) {
      this.setFont(_tree.getFont());
      setIcon(((LayerNode) _value).getIcon());
      setText(_value.toString());
      final boolean selectable = isSelectable(_value, _leaf);
      if (_selected && selectable) {
        setBackground(selectedBackground_);
        setForeground(selectedForground_);
      } else {
        setBackground(_tree.getBackground());
        setForeground(_tree.getForeground());
      }
      if (selectable) {
        setToolTipText(getText());
      } else {
        setToolTipText(getText() + ": " + CtuluLib.getS("Non s�lectionnable"));
      }
      setBorder((selectable && _hasFocus) ? focusBorderColor_ : CtuluCellRenderer.BORDER_NO_FOCUS);
      setEnabled(_tree.isEnabled());
      return this;
    }
  }

  public static CellRenderer createRenderer(final boolean _selectOnlyLeaf) {
    return new CellRenderer(_selectOnlyLeaf);
  }

  public static TreeSelectionModel getOnlyCourbeSelectionModel() {
    return new OnlyCourbeTreeSelectionModel();
  }

  public static class OnlyCourbeTreeSelectionModel extends DefaultTreeSelectionModel {
    @Override
    public void setSelectionPaths(final TreePath[] _paths) {
      super.setSelectionPaths(getCorrectPaths(_paths));
    }

    @Override
    public void addSelectionPaths(final TreePath[] _paths) {
      super.addSelectionPaths(getCorrectPaths(_paths));
    }

    private TreePath[] getCorrectPaths(final TreePath[] _paths) {
      TreePath[] paths = _paths;
      if (!CtuluLibArray.isEmpty(_paths)) {
        final List correctPath = new ArrayList(paths.length);
        for (int i = 0; i < paths.length; i++) {
          if (paths[i] != null && paths[i].getLastPathComponent() != null
              && ((LayerNode) paths[i].getLastPathComponent()).idxPoly_ >= 0) {
            correctPath.add(paths[i]);
          }
        }
        if (correctPath.size() != paths.length) {
          paths = (TreePath[]) correctPath.toArray(new TreePath[correctPath.size()]);
        }
      }
      return paths;
    }
  }

  public static class LayerNode extends DefaultMutableTreeNode {
    int idxPoly_ = -1;
    String titre_;
    final Icon icon_;

    public LayerNode(final BCalque _userObject) {
      super(_userObject, true);
      icon_ = CtuluLibImage.resize(_userObject, 16, 16);
    }

    public LayerNode(final ZCalqueLigneBrisee _userObject, final int _idx, final String _titre) {
      super(_userObject, false);
      idxPoly_ = _idx;
      titre_ = _titre;
      if (_userObject.modeleDonnees().isGeometryFermee(_idx)) {
        icon_ = EbliResource.EBLI.getIcon("draw-polygon");
      } else {
        icon_ = EbliResource.EBLI.getIcon("draw-polyline");
      }
    }

    public BCalque getCalque() {
      return (BCalque) getUserObject();
    }

    public void fillWithPolygone(final List _dest) {

      if (idxPoly_ >= 0) {
        final ZCalqueLigneBrisee cq = (ZCalqueLigneBrisee) userObject;
        if (cq.modeleDonnees().isGeometryFermee(idxPoly_)) {
          _dest.add(cq.modeleDonnees().getGeomData().getGeometry(idxPoly_));
        }
      } else if (((BCalque) userObject).isGroupeCalque()) {
        final BCalque[] cqs = ((BCalque) userObject).getTousCalques();
        if (cqs != null) {
          for (int i = cqs.length - 1; i >= 0; i--) {
            addPolygones(_dest, cqs[i]);
          }
        }
      }
      addPolygones(_dest, (BCalque) userObject);
    }

    private void addPolygones(final List _dest, final BCalque _cq) {
      if (_cq instanceof ZCalqueLigneBrisee) {
        final ZCalqueLigneBrisee cq = (ZCalqueLigneBrisee) _cq;
        final GISVisitorLigneCollector visitor = new GISVisitorLigneCollector(true);
        cq.modeleDonnees().getGeomData().accept(visitor);
        _dest.addAll(visitor.getPolygones());
      }
    }

    @Override
    public String toString() {
      if (titre_ != null) {
        return titre_;
      }
      return ((BCalque) userObject).getTitle();
    }

    public Icon getIcon() {
      return icon_;
    }

    public int getIdxPoly() {
      return idxPoly_;
    }
  }

  DefaultTreeModel treeModel_;

  public CalqueFindCourbeTreeModel(final BArbreCalqueModel _model, final BGroupeCalque _root) {
    super();
    rootLayer_ = _root;
    if (_model != null) {
      _model.getObservable().addObserver(this);
    }

    rootNode_ = new LayerNode(rootLayer_);
    treeModel_ = new DefaultTreeModel(rootNode_);
    buildTree();
  }

  public final void buildTree() {
    rootNode_.removeAllChildren();
    addLayer(rootNode_, rootLayer_);
    treeModel_.setRoot(null);
    treeModel_.setRoot(rootNode_);
  }

  private boolean containsAtLeastOnLayer;

  protected boolean containsLigne(final BCalque _cq) {
    finder_.resetFound();
    _cq.apply(finder_);
    return finder_.isFound();
  }

  public LayerNode findLayerNode(BCalque calque) {
    return findLayerNode(calque, rootNode_);
  }

  public LayerNode findLayerNode(LayerNode parent, int idxLigne) {
    int nb = parent.getChildCount();
    for (int i = 0; i < nb; i++) {
      final LayerNode childAt = (LayerNode) parent.getChildAt(i);
      if (childAt.getIdxPoly() == idxLigne) {
        return childAt;
      }
    }
    return null;
  }

  public LayerNode findLayerNode(BCalque calque, LayerNode parent) {
    if (parent == null) {
      return null;
    }
    if (parent.getCalque() == calque) {
      return parent;
    }
    int nb = parent.getChildCount();
    for (int i = 0; i < nb; i++) {
      final LayerNode childAt = (LayerNode) parent.getChildAt(i);
      if (childAt.getCalque() == calque) {
        return childAt;
      }
      LayerNode inChildren = findLayerNode(calque, childAt);
      if (inChildren != null) {
        return inChildren;
      }
    }

    return null;
  }

  public TreePath selectLayerAndLigne(BCalque calqueActif, int idxPolyline) {
    final LayerNode layerNode = findLayerNode(calqueActif);
    if (layerNode != null) {
      final LayerNode layerForLigne = findLayerNode(layerNode, idxPolyline);
      if (layerForLigne != null) {
        return new TreePath(treeModel_.getPathToRoot(layerForLigne));
      }
    }
    return null;
  }

  void addLayer(final LayerNode _parent, final BGroupeCalque _gc) {
    final BCalque[] cq = _gc.getCalques();
    if (CtuluLibArray.isEmpty(cq)) {
      return;
    }
    for (int i = 0; i < cq.length; i++) {
      final LayerNode node = new LayerNode(cq[i]);
      if (finder_.isMatching(cq[i])) {
        _parent.add(node);
        addLigne(node, (ZCalqueLigneBrisee) cq[i]);
      } else if (cq[i].isGroupeCalque() && containsLigne(cq[i])) {
        containsAtLeastOnLayer = true;
        _parent.add(node);
        addLayer(node, (BGroupeCalque) cq[i]);
      }
    }
  }

  public boolean isContainsAtLeastOnLayer() {
    return containsAtLeastOnLayer;
  }

  GISVisitorChooser chooser_ = new GISVisitorChooser();

  void addLigne(final LayerNode _parent, final ZCalqueLigneBrisee _brisee) {
    final GISZoneCollectionLigneBrisee collec = (GISZoneCollectionLigneBrisee) _brisee.modeleDonnees().getGeomData();
    final GISAttributeModelObjectInterface str = (GISAttributeModelObjectInterface) collec
        .getModel(GISAttributeConstants.TITRE);
    final int nb = collec.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      chooser_.clear();
      ((GISGeometry) collec.getGeometry(i)).accept(chooser_);
      if (chooser_.isPolygone() || (chooser_.isPolyligne() && !onlyLigneFerme_)) {
        String titre = null;
        if (str != null) {
          titre = (String) str.getValue(i);
        } else if (chooser_.isPolygone()) {
          titre = EbliLib.getS("Ligne ferm�e {0}", CtuluLibString.getString(i + 1));
        } else {
          titre = EbliLib.getS("Ligne ouverte {0}", CtuluLibString.getString(i + 1));
        }
        _parent.add(new LayerNode(_brisee, i, titre));
      }
    }
  }

  @Override
  public void update(final Observable _o, final Object _arg) {
    buildTree();
  }

  public boolean isOnlyLigneFerme() {
    return onlyLigneFerme_;
  }

  public void setOnlyLigneFerme(final boolean _onlyLigneFerme) {
    onlyLigneFerme_ = _onlyLigneFerme;
    finder_.setOnlyPolygone(onlyLigneFerme_);
    buildTree();
  }

  public TreeModel getTreeModel() {
    return treeModel_;
  }

  public static JTree createCalqueTree(final CalqueFindCourbeTreeModel _treeModel, final boolean _onlyCourbeSelected) {
    final JTree tree = new JTree(_treeModel.getTreeModel());
    tree.setEditable(false);
    tree.setShowsRootHandles(true);
    tree.setExpandsSelectedPaths(true);
    tree.setCellRenderer(createRenderer(_onlyCourbeSelected));
    tree.setRootVisible(false);
    tree.setFocusable(true);
    if (_onlyCourbeSelected) {
      final TreeSelectionModel onlyCourbeSelectionModel = getOnlyCourbeSelectionModel();
      onlyCourbeSelectionModel.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
      tree.setSelectionModel(onlyCourbeSelectionModel);
    }
    return tree;
  }
}
