/*
 *  @creation     4 avr. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.table.CtuluMutableTableModel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction.FormDelegateEllipse;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction.FormDelegateRectangle;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;

/**
 * Un panneau d'�dition d'une ligne bris�e qui s'affiche lorsque l'outil ligne bris�e est s�lectionn�.
 * Ce panneau est inclu dans le panneau des outils.
 * 
 * @author Fred Deniger
 * @version $Id: ZEditorLigneBriseePanel.java,v 1.21.4.3 2008-03-26 16:46:43 bmarchan Exp $
 */
public class ZEditorLigneBriseePanel extends BuPanel implements ZEditionAttributesCreatorInterface, ZEditorPanelInterface, ActionListener {

  public class CoordinatesModel extends AbstractTableModel implements CtuluMutableTableModel {

    GISAttributeInterface[] atomicAttr_;

    protected CoordinatesModel() {
      updateAtomicArrays();
    }

    /**
     * @param _columnIndex
     * @return L'�diteur propre � chaque colonne
     */
    public CtuluValueEditorI getEditor(int _columnIndex) {
      if (_columnIndex > 2) {
        return atomicAttr_[_columnIndex - 3].getEditor();
      }
      else {
        return new CtuluValueEditorDouble();
      }
    }
    
    protected void updateAtomic() {
      super.fireTableDataChanged();
    }

    protected final void updateAtomicArrays() {
      if (editorComps_ != null && attEditable_ != null && attEditable_.size() > 0) {
        final List<GISAttributeInterface> r = new ArrayList<GISAttributeInterface>(attEditable_.size());
        for (GISAttributeInterface att : attEditable_) {
          if (att.isAtomicValue()) {
            r.add(att);
          }
        }
        atomicAttr_ = new GISAttributeInterface[r.size()];
        r.toArray(atomicAttr_);
      }
    }

    @Override
    public Class<?> getColumnClass(final int _columnIndex) {
      if (_columnIndex > 2) {
        return atomicAttr_[_columnIndex - 3].getDataClass();
      }
      return Double.class;
    }

    @Override
    public int getColumnCount() {
      return 3 + (atomicAttr_ == null ? 0 : atomicAttr_.length);
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 0) {
        return CtuluLibString.ESPACE;
      }
      if (_column == 1) {
        return coordDefs_==null?"X":coordDefs_[0].getName();
      }
      if (_column == 2) {
        return coordDefs_==null?"Y":coordDefs_[1].getName();
      }
      final int idxFeature = _column - 3;
      if (atomicAttr_ != null) {
        return atomicAttr_[idxFeature].getName();
      }
      return super.getColumnName(_column);
    }

    @Override
    public int getRowCount() {
      final ZCalqueEditionInteraction.FormDelegate forme = edition_.getFormeEnCours();
      if (!forme.enCours()) {
        return 0;
      }
      if (forme.getForme() == DeForme.RECTANGLE) {
        return 1;
      }
      return forme.enCours() ? ((GrPolyligne) forme.getFormeEnCours()).nombre() : 0;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return CtuluLibString.getString(_rowIndex + 1);
      }
      final ZCalqueEditionInteraction.FormDelegate forme = edition_.getFormeEnCours();
      if (!forme.enCours()) {
        return CtuluLibString.EMPTY_STRING;
      }
      final boolean isRect = forme.getForme() == DeForme.RECTANGLE;
      GrPolyligne o = null;
      GrPoint init = null;
      if (isRect) {
        init = (GrPoint) forme.getFormeEnCours();
      } else {
        o = (GrPolyligne) forme.getFormeEnCours();
      }
      if (o != null) {
        if (_columnIndex == 1) {
          CtuluNumberFormatI fmt=(coordDefs_==null?null:coordDefs_[0].getFormatter().getXYFormatter());
          if (init == null) {
            return fmt==null?String.valueOf(o.sommets_.renvoieX(_rowIndex)):fmt.format(o.sommets_.renvoieX(_rowIndex));
          }
          fmt.format(init.x_);
        } else if (_columnIndex == 2) {
          CtuluNumberFormatI fmt=(coordDefs_==null?null:coordDefs_[1].getFormatter().getXYFormatter());
          if (init == null) {
            return fmt==null?String.valueOf(o.sommets_.renvoieY(_rowIndex)):fmt.format(o.sommets_.renvoieY(_rowIndex));
          }
          fmt.format(init.y_);
        } else if (atomicAttr_ != null) {
          final int featureIdx = _columnIndex - 3;
          if (featureIdx >= 0 && attributes_ != null && attributes_.length > 0) {
            return forme.getData().getValue(atomicAttr_[featureIdx], _rowIndex);
          }

        }
      }
      return null;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return _columnIndex > 0;
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_value == null)
        return;

      final ZCalqueEditionInteraction.FormDelegate forme = edition_.getFormeEnCours();

      if (forme != null) {
        if (_columnIndex == 1 || _columnIndex == 2) {
          double val = Double.parseDouble(_value.toString());
          GrPoint pt = forme.getPoint(_rowIndex);

          if (_columnIndex == 1)
            pt.x_ = val;
          else
            pt.y_ = val;

          forme.setPoint(_rowIndex, pt);
        }
        else if (atomicAttr_ != null) {
          forme.getData().setValue(atomicAttr_[_columnIndex - 3], _rowIndex, _value);
        }
      }
      super.fireTableDataChanged();
    }

    @Override
    public void addRow(Object... _values) {
      addRow(getRowCount(), _values);
    }

    @Override
    public void addRow(int _irow, Object... values) {
      final ZCalqueEditionInteraction.FormDelegate f = edition_.getFormeEnCours();
      f.addPoint(_irow, new GrPoint(0,0,0));
      
      for (int i=0; i<Math.min(getColumnCount()-1, values.length); i++) {
        setValueAt(values[i], _irow, i+1);
      }
    }

    /**
     * Les _idx doivent �tre dans l'ordre croissant.
     */
    @Override
    public void removeRows(int... _idx) {
      final ZCalqueEditionInteraction.FormDelegate f=edition_.getFormeEnCours();
      for (int i=_idx.length - 1; i >= 0; i--) {
        f.removePoint(_idx[i]);
      }
    }
  }

  /**
   * Interface graphique permettant de g�rer les propri�t�s de l'ellipse lors de
   * sa construction
   * 
   * @author Emmanuel Martin
   */
  protected class EllipseBuilderGUI extends BuPanel implements ActionListener {

    private BuTextField tfCoordX_;
    private BuTextField tfCoordY_;
    private BuTextField tfRayonX_;
    private BuTextField tfRayonY_;
    private BuTextField tfNbPoints_;
    private String nbPointsDefaultValue_;

    public EllipseBuilderGUI() {
      super();
      int hauteur=20;
      int largeur=67;
      nbPointsDefaultValue_ = "3";
      setBorder(new EmptyBorder(0, 0, 0, 0));
      // Create BuTextFields & Dimension
      tfCoordX_=BuTextField.createDoubleField();
      tfCoordY_=BuTextField.createDoubleField();
      tfRayonX_=BuTextField.createDoubleField();
      tfRayonY_=BuTextField.createDoubleField();
      tfNbPoints_=BuTextField.createIntegerField();
      tfCoordX_.setPreferredSize(new Dimension(largeur, hauteur));
      tfCoordY_.setPreferredSize(new Dimension(largeur, hauteur));
      tfRayonX_.setPreferredSize(new Dimension(largeur, hauteur));
      tfRayonY_.setPreferredSize(new Dimension(largeur, hauteur));
      tfNbPoints_.setPreferredSize(new Dimension(largeur, hauteur));
      // ActionListener
      tfCoordX_.addActionListener(this);
      tfCoordY_.addActionListener(this);
      tfRayonX_.addActionListener(this);
      tfRayonY_.addActionListener(this);
      tfNbPoints_.addActionListener(this);
      // Default value
      tfNbPoints_.setText(nbPointsDefaultValue_);
      // Conteneur principal
      Box mainContainer=new Box(BoxLayout.Y_AXIS);
      Box head=new Box(BoxLayout.X_AXIS);
      Container foot=new Container();
      foot.setLayout(new GridLayout(1, 1, 0, 0));
      // Gestion des coordonn�es
      String xcname="X";
      String ycname="Y";
      if (coordDefs_!=null) {
        xcname=coordDefs_[0].getName();
        ycname=coordDefs_[1].getName();
      }
      Box tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(new JLabel(EbliLib.getS("Coord ")+xcname+" "));
      tmp.add(new JLabel(EbliLib.getS("Coord ")+ycname+" "));
      head.add(tmp);
      tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(tfCoordX_);
      tmp.add(tfCoordY_);
      head.add(tmp);
      // Taille
      tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(new JLabel(EbliLib.getS("Rayon ")+xcname+" "));
      tmp.add(new JLabel(EbliLib.getS("Rayon ")+ycname+" "));
      head.add(tmp);
      tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(tfRayonX_);
      tmp.add(tfRayonY_);
      head.add(tmp);
      // Nombre de point
      foot.add(new JLabel(EbliLib.getS("Nombre de points:")));
      foot.add(tfNbPoints_);
      // Add to mainContainer
      mainContainer.add(head);
      mainContainer.add(foot);
      // Add to Panel
      add(mainContainer);
    }

    /**
     * Met � jour le contenu de l'interface en cas de changement des coordonn�es
     * du point d'origine.
     */
    public void update() {
      if (edition_!=null&&edition_.getFormeEnCours()!=null&&edition_.getFormeEnCours().enCours()) {
        ((FormDelegateEllipse)edition_.getFormeEnCours()).setDataAttributs(attEditable_,editorComps_);
        GrPoint pointOrigine=((FormDelegateEllipse)edition_.getFormeEnCours()).getPointOrigine();
        if (pointOrigine!=null) {
          if (coordDefs_!=null) {
            tfCoordX_.setText(coordDefs_[0].getFormatter().getXYFormatter().format(new Double(pointOrigine.x_)));
            tfCoordY_.setText(coordDefs_[1].getFormatter().getXYFormatter().format(new Double(pointOrigine.y_)));
          }
          else {
            tfCoordX_.setText(String.valueOf(pointOrigine.x_));
            tfCoordY_.setText(String.valueOf(pointOrigine.y_));
          }
        }
        else {
          tfCoordX_.setText("");
          tfCoordY_.setText("");
        }
        ajoutFormeManager();
      }
      else
        reinitialisation();
    }

    /**
     * Cette m�thode g�re la mise � jour de l'interface lors de mouvement de la
     * souris.
     * 
     * @param _x
     *          : coordonn�e x de la sourie
     * @param _y
     *          coordonn�e y de la sourie
     */
    public void updateMove(double _x, double _y) {
      if (edition_!=null&&edition_.getFormeEnCours()!=null&&edition_.getFormeEnCours().enCours()) {
        GrPoint pointOrigine=((FormDelegateEllipse)edition_.getFormeEnCours()).getPointOrigine();
        if (coordDefs_!=null) {
          tfRayonX_.setText(coordDefs_[0].getFormatter().getXYFormatter().format(new Double(Math.abs(_x-pointOrigine.x_))));
          tfRayonY_.setText(coordDefs_[1].getFormatter().getXYFormatter().format(new Double(Math.abs(_y-pointOrigine.y_))));
        }
        else {
          tfRayonX_.setText(String.valueOf(new Double(Math.abs(_x-pointOrigine.x_))));
          tfRayonY_.setText(String.valueOf(new Double(Math.abs(_y-pointOrigine.y_))));
        }
        ajoutFormeManager();
      }
    }

    /**
     * Active (ou d�active) le bouton d'ajout de forme si les conditions sont
     * r�unies.
     */
    private void ajoutFormeManager() {
      if (tfCoordX_.getText().compareTo("")!=0&&tfCoordY_.getText().compareTo("")!=0&&tfRayonX_.getText().compareTo("")!=0
          &&tfRayonY_.getText().compareTo("")!=0&&tfNbPoints_.getText().compareTo("")!=0)
        btSaveLine_.setEnabled(true);
      else
        btSaveLine_.setEnabled(false);
    }
    
    /**
     * Si un rayon est donn� par l'utilisateur, fnir l'ellipse celle-ci, sinon
     * ne fait rien.
     */
    private void buildEllipse() {
      if (edition_!=null&&edition_.getFormeEnCours()!=null) {
        double rayonX, rayonY, pointDebX, pointDebY;
        int nbPoints;
        try {
          pointDebX=Double.parseDouble(tfCoordX_.getText());
          pointDebY=Double.parseDouble(tfCoordY_.getText());
          rayonX=Double.parseDouble(tfRayonX_.getText());
          rayonY=Double.parseDouble(tfRayonY_.getText());
          nbPoints=Integer.parseInt(tfNbPoints_.getText());
        }
        catch (NumberFormatException ex) {
          return; // Cas o� n'importe quoi est entr�e dans les champs de saisie.
        }
        FormDelegateEllipse ell=((FormDelegateEllipse)edition_.getFormeEnCours());
        ell.setPointOrigine(new GrPoint(pointDebX, pointDebY, 0));
        ell.setRayonX(rayonX);
        ell.setRayonY(rayonY);
        ell.setNbPoints(nbPoints);
        ell.setDataAttributs(attEditable_,editorComps_);
        if(ell.buildEllipse())
          reinitialisation();
      }
    }

    /**
     * R�initialise le contenu des textFields
     */
    public void reinitialisation(){
      tfCoordX_.setText("");
      tfCoordY_.setText("");
      tfRayonX_.setText("");
      tfRayonY_.setText("");
      btSaveLine_.setEnabled(false);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent _e) {
      if (edition_!=null&&edition_.getFormeEnCours()!=null&&edition_.getFormeEnCours().getForme()==DeForme.ELLIPSE) {
        boolean error=false;
        if (_e.getSource()==tfCoordX_||_e.getSource()==tfCoordY_) {
          double x=0, y=0;
          try {
            x=Double.parseDouble(tfCoordX_.getText());
          }
          catch (NumberFormatException ex) {
            tfCoordX_.setText("");
            error=true;
          }
          try {
            y=Double.parseDouble(tfCoordY_.getText());
          }
          catch (NumberFormatException ex) {
            tfCoordY_.setText("");
            error=true;
          }
          if (!error)
            ((FormDelegateEllipse)edition_.getFormeEnCours()).setPointOrigine(new GrPoint(x, y, 0));
        }
        else if (_e.getSource()==tfNbPoints_) {
          try {
            ((FormDelegateEllipse)edition_.getFormeEnCours()).setNbPoints(Integer.parseInt(tfNbPoints_.getText()));
          }
          catch (NumberFormatException ex) {
            tfNbPoints_.setText("");
          }
        }
        else if (_e.getSource()==tfRayonX_) {
          try {
            ((FormDelegateEllipse)edition_.getFormeEnCours()).setRayonX(Double.parseDouble(tfRayonX_.getText()));
          }
          catch (NumberFormatException ex) {
            tfRayonX_.setText("");
          }
        }
        else if (_e.getSource()==tfRayonY_) {
          try {
            ((FormDelegateEllipse)edition_.getFormeEnCours()).setRayonY(Double.parseDouble(tfRayonY_.getText()));
          }
          catch (NumberFormatException ex) {
            tfRayonY_.setText("");
          }
        }
        else {
          // Mise a jour de l'interface graphique suite � une action de
          // l'utilisateur
          update();
        }
      }
      ajoutFormeManager();
    }
  }

  /**
   * Interface graphique permettant de g�rer les propri�t�s du rectangle lors de
   * sa construction
   * 
   * @author Emmanuel Martin
   */
  protected class RectangleBuilderGUI extends BuPanel implements ActionListener {

    private BuTextField tfCoordX_;
    private BuTextField tfCoordY_;
    private BuTextField tfSizeY_;
    private BuTextField tfSizeX_;
    private BuTextField tfNbPointX_;
    private BuTextField tfNbPointY_;
    private String nbPointsDefaultValue_; 

    public RectangleBuilderGUI() {
      super();
      nbPointsDefaultValue_ = "0";
      int hauteur=20;
      int largeur=67;
      setBorder(new EmptyBorder(0, 0, 0, 0));
      // Create BuTextFields & Dimension
      tfCoordX_=BuTextField.createDoubleField();
      tfCoordY_=BuTextField.createDoubleField();
      tfSizeY_=BuTextField.createDoubleField();
      tfSizeX_=BuTextField.createDoubleField();
      tfNbPointY_=BuTextField.createIntegerField();
      tfNbPointX_=BuTextField.createIntegerField();
      tfCoordX_.setPreferredSize(new Dimension(largeur, hauteur));
      tfCoordY_.setPreferredSize(new Dimension(largeur, hauteur));
      tfSizeY_.setPreferredSize(new Dimension(largeur, hauteur));
      tfSizeX_.setPreferredSize(new Dimension(largeur, hauteur));
      tfNbPointY_.setPreferredSize(new Dimension(largeur, hauteur));
      tfNbPointX_.setPreferredSize(new Dimension(largeur, hauteur));
      // ActionListener
      tfCoordX_.addActionListener(this);
      tfCoordY_.addActionListener(this);
      tfSizeY_.addActionListener(this);
      tfSizeX_.addActionListener(this);
      tfNbPointY_.addActionListener(this);
      tfNbPointX_.addActionListener(this);
      // Default value
      tfNbPointY_.setText(nbPointsDefaultValue_);
      tfNbPointX_.setText(nbPointsDefaultValue_);
      // Conteneur principal
      Box mainContainer=new Box(BoxLayout.Y_AXIS);
      Box head=new Box(BoxLayout.X_AXIS);
      Container foot=new Container();
      foot.setLayout(new GridLayout(2, 2, 0, 0));
      // Gestion des coordonn�es
      String xcname="X";
      String ycname="Y";
      if (coordDefs_!=null) {
        xcname=coordDefs_[0].getName();
        ycname=coordDefs_[1].getName();
      }
      Box tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(new JLabel(EbliLib.getS("Coord ")+xcname+" "));
      tmp.add(new JLabel(EbliLib.getS("Coord ")+ycname+" "));
      head.add(tmp);
      tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(tfCoordX_);
      tmp.add(tfCoordY_);
      head.add(tmp);
      // Taille
      tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(new JLabel(EbliLib.getS("Hauteur ")));
      tmp.add(new JLabel(EbliLib.getS("Largeur ")));
      head.add(tmp);
      tmp=new Box(BoxLayout.Y_AXIS);
      tmp.add(tfSizeY_);
      tmp.add(tfSizeX_);
      head.add(tmp);
      // Nombre de point
      foot.add(new JLabel(EbliLib.getS("Nb points hauteur: ")));
      foot.add(tfNbPointY_);
      foot.add(new JLabel(EbliLib.getS("Nb points largeur: ")));
      foot.add(tfNbPointX_);
      // Add to mainContainer
      mainContainer.add(head);
      mainContainer.add(foot);
      // Add to Panel
      add(mainContainer);
    }

    /**
     * Met � jour le contenu de l'interface en cas de changement des coordonn�es
     * du point d'origine.
     */
    public void update() {
      if (edition_!=null&&edition_.getFormeEnCours()!=null&&edition_.getFormeEnCours().enCours()) {
        GrPoint pointOrigine=(GrPoint)edition_.getFormeEnCours().getFormeEnCours();
        ((FormDelegateRectangle)edition_.getFormeEnCours()).setDataAttributs(attEditable_,editorComps_);
        if (pointOrigine!=null) {
          if (coordDefs_!=null) {
            tfCoordX_.setText(coordDefs_[0].getFormatter().getXYFormatter().format(new Double(pointOrigine.x_)));
            tfCoordY_.setText(coordDefs_[1].getFormatter().getXYFormatter().format(new Double(pointOrigine.y_)));
          }
          else {
            tfCoordX_.setText(String.valueOf(new Double(pointOrigine.x_)));
            tfCoordY_.setText(String.valueOf(new Double(pointOrigine.y_)));
          }
        }
        else {
          tfCoordX_.setText("");
          tfCoordY_.setText("");
        }
        // Met � jour le nombre de point, au cas o� celui ci � �t� donn� avant la cr�ation de la forme.
        try {
          ((FormDelegateRectangle)edition_.getFormeEnCours()).setNbPointsLargeur(Integer.parseInt(tfNbPointX_.getText()));
        }
        catch (NumberFormatException ex) {
          tfNbPointX_.setText("");
        }
        try {
          ((FormDelegateRectangle)edition_.getFormeEnCours()).setNbPointsHauteur(Integer.parseInt(tfNbPointY_.getText()));
        }
        catch (NumberFormatException ex) {
          tfNbPointY_.setText("");
        }
        ajoutFormeManager();
      }
      else
        reinitialisation();
    }

    /**
     * Cette m�thode g�re la mise � jour de l'interface lors de mouvement de la
     * souris.
     * 
     * @param _x
     *          : coordonn�e x de la sourie
     * @param _y
     *          coordonn�e y de la sourie
     */
    public void updateMove(double _x, double _y) {
      if (edition_!=null&&edition_.getFormeEnCours()!=null&&edition_.getFormeEnCours().enCours()) {
        GrPoint pointOrigine=(GrPoint)edition_.getFormeEnCours().getFormeEnCours();
        if (coordDefs_!=null) {
          tfSizeX_.setText(coordDefs_[0].getFormatter().getXYFormatter().format(new Double(Math.abs(_x-pointOrigine.x_))));
          tfSizeY_.setText(coordDefs_[1].getFormatter().getXYFormatter().format(new Double(Math.abs(_y-pointOrigine.y_))));
        }
        else {
          tfSizeX_.setText(String.valueOf(new Double(Math.abs(_x-pointOrigine.x_))));
          tfSizeY_.setText(String.valueOf(new Double(Math.abs(_y-pointOrigine.y_))));
        }
        ajoutFormeManager();
      }
    }

    /**
     * Si une largeur ou une hauteur est donn� par l'utilisateur, finir le
     * rectangle avec celle-ci, sinon ne fait rien.
     */
    private void buildRectangle() {
      if (edition_!=null&&edition_.getFormeEnCours()!=null) {
        double pointEndX, pointEndY, pointDebX, pointDebY;
        int nbPointsX, nbPointsY;
        try {
          pointDebX=Double.parseDouble(tfCoordX_.getText());
          pointDebY=Double.parseDouble(tfCoordY_.getText());
          pointEndX=Double.parseDouble(tfSizeX_.getText())+pointDebX;
          pointEndY=Double.parseDouble(tfSizeY_.getText())+pointDebY;
          nbPointsX=Integer.parseInt(tfNbPointX_.getText());
          nbPointsY=Integer.parseInt(tfNbPointY_.getText());
        }
        catch (NumberFormatException ex) {
          return; // Cas o� n'importe quoi est entr�e dans les champs de saisie.
        }
        FormDelegateRectangle rec=((FormDelegateRectangle)edition_.getFormeEnCours());
        rec.setPointOrigine(new GrPoint(pointDebX, pointDebY, 0));
        rec.setPointFin(new GrPoint(pointEndX, pointEndY, 0));
        rec.setNbPointsLargeur(nbPointsX);
        rec.setNbPointsHauteur(nbPointsY);
        rec.setDataAttributs(attEditable_,editorComps_);
        if(rec.buildRectangle())
          reinitialisation();
      }
    }

    /**
     * R�initialise le contenu des textFields
     */
    public void reinitialisation(){
      tfCoordX_.setText("");
      tfCoordY_.setText("");
      tfSizeX_.setText("");
      tfSizeY_.setText("");
      tfNbPointX_.setText(nbPointsDefaultValue_);
      tfNbPointY_.setText(nbPointsDefaultValue_);
      btSaveLine_.setEnabled(false);
    }
    
    /**
     * Active (ou d�active) le bouton d'ajout de forme si les conditions sont
     * r�unies.
     */
    private void ajoutFormeManager() {
      if (tfCoordX_.getText().compareTo("")!=0&&tfCoordY_.getText().compareTo("")!=0&&tfSizeX_.getText().compareTo("")!=0
          &&tfSizeY_.getText().compareTo("")!=0&&tfNbPointX_.getText().compareTo("")!=0&&tfNbPointY_.getText().compareTo("")!=0)
        btSaveLine_.setEnabled(true);
      else
        btSaveLine_.setEnabled(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent _e) {
      if (edition_!=null&&edition_.getFormeEnCours()!=null&&edition_.getFormeEnCours().getFormeEnCours()!=null
          &&edition_.getFormeEnCours().getForme()==DeForme.RECTANGLE) {
        boolean error=false;
        if (_e.getSource()==tfCoordX_||_e.getSource()==tfCoordY_) {
          double x=0, y=0;
          try {
            x=Double.parseDouble(tfCoordX_.getText());
          }
          catch (NumberFormatException ex) {
            tfCoordX_.setText("");
            error=true;
          }
          try {
            y=Double.parseDouble(tfCoordY_.getText());
          }
          catch (NumberFormatException ex) {
            tfCoordY_.setText("");
            error=true;
          }
          if (!error)
            ((FormDelegateRectangle)edition_.getFormeEnCours()).setPointOrigine(new GrPoint(x, y, 0));
        }
        else if (_e.getSource()==tfNbPointX_||_e.getSource()==tfNbPointY_) {
          try {
            ((FormDelegateRectangle)edition_.getFormeEnCours()).setNbPointsLargeur(Integer.parseInt(tfNbPointX_.getText()));
          }
          catch (NumberFormatException ex) {
            tfNbPointX_.setText("");
          }
          try {
            ((FormDelegateRectangle)edition_.getFormeEnCours()).setNbPointsHauteur(Integer.parseInt(tfNbPointY_.getText()));
          }
          catch (NumberFormatException ex) {
            tfNbPointY_.setText("");
          }
        }
        else if (_e.getSource()==tfSizeY_ || _e.getSource()==tfSizeX_) {
          double coordX=0, coordY=0;
          try {
            coordX=Double.parseDouble(tfSizeX_.getText())+((GrPoint)edition_.getFormeEnCours().getFormeEnCours()).x_;
          }
          catch (NumberFormatException ex) {
            tfSizeX_.setText("");
            error=true;
          }
          try {
            coordY=Double.parseDouble(tfSizeY_.getText())+((GrPoint)edition_.getFormeEnCours().getFormeEnCours()).y_;
          }
          catch (NumberFormatException ex) {
            tfSizeY_.setText("");
            error=true;
          }
          if(!error)
          ((FormDelegateRectangle)edition_.getFormeEnCours()).setPointFin(new GrPoint(coordX, coordY, 0));
        }
        else {
          // Mise a jour de l'interface graphique suite � une action de
          // l'utilisateur
          update();
        }
      }
      ajoutFormeManager();
    }
  }

  private CoordinatesModel tableModel_;
  
  BuButton btReprise_;
  JButton btSaveLine_;

  RectangleBuilderGUI rectangleBuilderGUI_;
  EllipseBuilderGUI ellipseBuilderGUI_;
  
  ZEditionAttibutesContainer container_;

  ZCalqueEditionInteraction edition_;

//  LinkedHashMap<GISAttributeInterface, JComponent> editorAttribute_;
  
  /** Type de g�ometrie a �diter */
  int typeForme_;

//  String initState_;

  BPaletteEdition palette_;
  /** Le panneau principal, hors boutons */
  JPanel pnMain_;

  protected GISAttributeInterface[] attributes_;
  /** L'attribut utilis� pour Z s'il y en a 1 */
  protected GISAttributeInterface attIsZ_;
  /** Les editeurs pour chacun des attributs editables */
  protected List<JComponent> editorComps_;
  /** Les attributs editables, dans le meme ordre que les editeurs */
  protected List<GISAttributeInterface> attEditable_=new ArrayList<GISAttributeInterface>();
  /** Le nom des coordonn�es */
  protected EbliCoordinateDefinition[] coordDefs_;

  /**
   * Constructeur depuis l'editeur.
   */
  public ZEditorLigneBriseePanel(final ZEditorDefault _editor) {
    this(_editor.palette_, _editor.dessin_);
    targetChanged(_editor.getTarget());
    coordDefs_=_editor.getPanel().getCoordinateDefinitions();
  }
  /**
   * Constructeur sans editeur par defaut.
   * @param _editor l'editeur parent
   * @param _typeForme Le type de g�om�trie a �diter.
   */
  public ZEditorLigneBriseePanel(final BPaletteEdition _palette, final ZCalqueEditionInteraction _cqEdition) {
    setLayout(new BorderLayout());
    pnMain_=new JPanel();
    pnMain_.setLayout(new BuVerticalLayout(2, true, true));
    add(pnMain_,BorderLayout.CENTER);
    
    palette_ = _palette;
    edition_ = _cqEdition;
//    initState_ = _editor.getCurrentAction();
    updateFeaturePanel();
    if (tableModel_ != null) {
      tableModel_.updateAtomic();
    }
    typeForme_=edition_.getTypeForme();
    addParametersPanel();
    addButtons();
  }

  public BuButton getBtReprise() {
    return btReprise_;
  }
  
  /**
   * Remplace le bouton faisant office d'ajout de forme
   * @param _bt Le bouton
   */
  public void replaceAddButton(BuButton _bt) {
    btSaveLine_=_bt;
    btSaveLine_.addActionListener(this);
  }

  /**
   * Ajoute les boutons de validation et reprise
   */
  private void addButtons() {
    if (btReprise_ == null) {
      btReprise_ = new BuButton();
      btReprise_.setText(EbliLib.getS("Reprise"));
      btReprise_.setToolTipText("<html><body>" + EbliLib.getS("Continuer la saisie apr�s un zoom ou un d�placement de vue"));
      btReprise_.setEnabled(false);
      btReprise_.addActionListener(this);
      if (palette_.getSelectedButton() != null) {
        btReprise_.setIcon(palette_.getSelectedButton().getIcon());
      }
    }
    if (btSaveLine_ == null) {
      btSaveLine_ = new BuButton();
      btSaveLine_.setText(EbliLib.getS("Ajouter"));
      btSaveLine_.setIcon(BuResource.BU.getToolIcon("ajouter"));
      btSaveLine_.setEnabled(false);
      btSaveLine_.addActionListener(this);
      btSaveLine_.setToolTipText("<html><body>" + EbliLib.getS("Ajouter la forme en cours") + "<br>"
          + EbliLib.getS("Vous pouvez �galement cliquer 2 fois pour ajouter la forme") + "<br>"
          + EbliLib.getS("ou utiliser la touche 'Ctrl' et cliquer sur le dernier point de la forme"));
    }
    
    JPanel pnButtons=new JPanel();
    pnButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
    pnButtons.add(btReprise_);
    if (isParametersEditable()) {
      pnButtons.add(btSaveLine_);
    }
    add(pnButtons,BorderLayout.SOUTH);
  }
  /**
   * Ajoute le panneau pour pr�ciser les parametres de g�om�trie.
   */
  private void addParametersPanel() {
    FuLog.trace("Type de g�om�trie en cours: "+typeForme_);
    if (!isParametersEditable()) return;

    if (typeForme_==DeForme.LIGNE_BRISEE || 
        typeForme_==DeForme.POLYGONE || typeForme_==DeForme.MULTI_POINT)
      pnMain_.add(getPolyParametersPanel());
    else if(typeForme_==DeForme.RECTANGLE){
      pnMain_.add(getRectangleParametersPanel());
    }
    else if (typeForme_==DeForme.ELLIPSE)
      pnMain_.add(getEllipseParametersPanel());
  }
  
  /**
   * 
   * @return un panneau de propri�t�s modifiables lors de la cr�ation d'une ellipse.
   */
  private JPanel getEllipseParametersPanel(){
    ellipseBuilderGUI_ = new EllipseBuilderGUI();
    return ellipseBuilderGUI_;
  }
  
  /**
   * @return un panneau de propri�t�s modifiables lors de la cr�ation d'un
   * rectangle.
   */
  private JPanel getRectangleParametersPanel() {
    rectangleBuilderGUI_ = new RectangleBuilderGUI();
    return rectangleBuilderGUI_;
  }
  
  /**
   * @return un panneau de coordonn�es modifiables lors de la cr�ation d'une
   * polyligne.
   */
  private JPanel getPolyParametersPanel() {
    BuPanel pn=new BuPanel();
    pn.setLayout(new BorderLayout());
    pn.setBorder(new EmptyBorder(2, 2, 2, 2));
    tableModel_=createValueModel();
    final CtuluTable tb=new CtuluTable(tableModel_);
    tb.setAutoExtendRowsWhenPaste(true);
    tb.setCellSelectionEnabled(true);
    
    for (int i=0; i<tb.getColumnModel().getColumnCount(); i++) {
      CtuluValueEditorI editor = tableModel_.getEditor(i);
      tb.getColumnModel().getColumn(i).setCellEditor(editor.createTableEditorComponent());
      tb.getColumnModel().getColumn(i).setCellRenderer(editor.createTableRenderer());
    }
    
    final BuScrollPane sp=new BuScrollPane(tb);
    sp.setPreferredWidth(190);
    sp.setPreferredHeight(60);
    tb.getColumnModel().getColumn(0).setWidth(200);
    pn.add(new BuLabel(EbliLib.getS("Coordonn�es")), BorderLayout.NORTH);
    pn.add(sp, BorderLayout.CENTER);
    
    final BuButton btRemove = new BuButton();
    btRemove.setText(EbliLib.getS("Supprimer"));
    btRemove.setToolTipText("<html><body>" + EbliLib.getS("Supprimer les points s�lectionn�s"));
    btRemove.setEnabled(false);
    btRemove.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tableModel_.removeRows(tb.getSelectedRows());
      }
    });
    tb.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        btRemove.setEnabled(tb.getSelectedRowCount()>0);
      }
    });
    
    JPanel pnButtons = new JPanel();
    pnButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pnButtons.add(btRemove);
    pn.add(pnButtons, BorderLayout.SOUTH);
    
    return pn;
  }

  protected final boolean isParametersEditable() {
    if (edition_ == null) {
      return false;
    }
    final ZCalqueEditionInteraction.FormDelegate delegate = edition_.getFormeEnCours();
    if (delegate != null) {
      final int forme = delegate.getForme();
      if (forme == DeForme.RECTANGLE || forme == DeForme.LIGNE_BRISEE || forme == DeForme.POLYGONE || forme==DeForme.MULTI_POINT || forme==DeForme.ELLIPSE) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return le modele a utiliser pour afficher les valeurs
   */
  protected CoordinatesModel createValueModel() {
    if (isParametersEditable()) {
      return new CoordinatesModel();
    }
    return null;
  }

  /**
   * Met a jour le panneau des attributs globaux.
   */
  protected void updateFeaturePanel() {
    if (attributes_ != null && attributes_.length > 0) {
      final BuPanel pnFeature = new BuPanel();
      pnFeature.setLayout(new BuGridLayout(2, 0, 0));
      final List<JComponent> editor = new ArrayList<JComponent>(attributes_.length);
//      if (editorAttribute_ == null) {
//        editorAttribute_ = new LinkedHashMap<GISAttributeInterface, JComponent>();
//      } else {
//        editorAttribute_.clear();
//      }
      attEditable_.clear();
      for (int i = 0; i < attributes_.length; i++) {
        final GISAttributeInterface attr = attributes_[i];
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.trace("EZM: add for " + attr.getName());
        }
        if (attr.isEditable() && attr.isUserVisible()) {
          final CtuluValueEditorI ed = attr.getEditor();
          final JComponent ci = ed.createEditorComponent();
//          editorAttribute_.put(attr, ci);
          if (ed.getValue(ci) == null) {
            final Object defaultVal = attr.getDefaultValue();
            ed.setValue(defaultVal == null ? null : defaultVal, ci);
          }
          // Si l'attribut est pris pour Z, on le place en premier.
          if (attr.equals(attIsZ_)) {
            attEditable_.add(0,attr);
            editor.add(0,ci);
            if (!attr.isAtomicValue()) {
              pnFeature.add(new BuLabel(attr.getNameWithUnit() + ':'), 0);
              pnFeature.add(ci, 1);
            }
          }
          else {
            attEditable_.add(attr);
            editor.add(ci);
            if (!attr.isAtomicValue()) {
              pnFeature.add(new BuLabel(attr.getNameWithUnit() + ':'));
              pnFeature.add(ci);
            }
          }
        }
      }
//      editorComps_ = (JComponent[]) editor.toArray(new JComponent[editor.size()]);
      editorComps_=editor;
      pnMain_.add(pnFeature);
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == btReprise_) {
      edition_.setGele(false);
    } else if (_e.getSource() == btSaveLine_) {
      if(edition_.getFormeEnCours().getForme() == DeForme.RECTANGLE)
        rectangleBuilderGUI_.buildRectangle();
      else if(edition_.getFormeEnCours().getForme() == DeForme.ELLIPSE)
        ellipseBuilderGUI_.buildEllipse();
      else
        edition_.getFormeEnCours().addCurrentForme();
    }
  }

  @Override
  public void atomicChanged() {
    if (tableModel_!=null) {
      tableModel_.updateAtomic();
    }
    btSaveLine_.setEnabled(false);
    if (!edition_.isGele()
        &&edition_.getFormeEnCours()!=null
        &&edition_.getFormeEnCours().getFormeEnCours()!=null
        &&(edition_.getFormeEnCours().getForme()==DeForme.POLYGONE||edition_.getFormeEnCours().getForme()==DeForme.LIGNE_BRISEE||edition_
            .getFormeEnCours().getForme()==DeForme.MULTI_POINT)) {
      final int nb=((GrPolyligne)edition_.getFormeEnCours().getFormeEnCours()).nombre();
      boolean enable=false;
      if (edition_.getFormeEnCours().getForme()==DeForme.POLYGONE) {
        enable=nb>=3;
      }
      else if (edition_.getFormeEnCours().getForme()==DeForme.LIGNE_BRISEE) {
        enable=nb>=2;
      }
      else {
        enable=nb>=1;
      }
      btSaveLine_.setEnabled(enable);
    }
    else if (!edition_.isGele()&&edition_.getFormeEnCours().getForme()==DeForme.RECTANGLE&&rectangleBuilderGUI_!=null) {
      rectangleBuilderGUI_.update();
    }
    else if (!edition_.isGele()&&edition_.getFormeEnCours().getForme()==DeForme.ELLIPSE&&ellipseBuilderGUI_!=null) {
      ellipseBuilderGUI_.update();
    }
  }

  @Override
  public void calqueInteractionChanged() {
    btReprise_.setEnabled(edition_.isGele());
    if (palette_.getSelectedButton() != null) {
      btReprise_.setIcon(palette_.getSelectedButton().getIcon());
    }
    atomicChanged();
  }

  @Override
  public void close() {
    btReprise_ = null;
  }

  @Override
  public void editionStopped() {
    btSaveLine_.setEnabled(false);
  }

  @Override
  public ZEditionAttibutesContainer getAttributeContainer() {
    if (container_ == null || attributes_ != null) {
      container_ = new ZEditionAttibutesContainer(attributes_);
      container_.setCreator(this);
    }
    return container_;
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public Object getData(final GISAttributeInterface _attr) {
    if (editorComps_ == null) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.trace("EZM: " + getClass().getName() + " no component");
      }
      return null;
    }
    int ind=attEditable_.indexOf(_attr);
    if (ind != -1) {
      final JComponent c = (JComponent) editorComps_.get(ind);
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.trace("EZM: " + getClass().getName() + " component for attribute " + _attr.getName() + " value= "
            + _attr.getEditor().getValue(c));

      }
      return _attr.getEditor().getValue(c);
    }
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.trace("EZM: " + getClass().getName() + " NO component for attribute " + _attr.getName());
    }
    return null;
  }

  public CoordinatesModel getTableModel() {
    return tableModel_;
  }

  @Override
  public void objectAdded() {
    atomicChanged();
  }

  public void setCoordinateDefinitions(EbliCoordinateDefinition[] _defs) {
    coordDefs_=_defs;
  }

  @Override
  public void targetChanged(final ZCalqueEditable _new) {
    GISAttributeInterface[] newAtt = null;
    GISAttributeInterface attIsZ=null;
    if (_new != null && _new.getModelEditable() != null && _new.getModelEditable().getGeomData() != null) {
      newAtt = _new.getModelEditable().getGeomData().getAttributes();
      attIsZ = _new.getModelEditable().getGeomData().getAttributeIsZ();
    }
    if (!Arrays.equals(newAtt, attributes_) || attIsZ_!=attIsZ) {
      attributes_ = newAtt;
      attIsZ_=attIsZ;
      container_ = null;
      pnMain_.removeAll();
      updateFeaturePanel();
      if (tableModel_ != null) {
        tableModel_.updateAtomicArrays();
      }
      addParametersPanel();
      addButtons();
    }
  }
  /*
   * (non-Javadoc)
   * 
   * @see
   * org.fudaa.ebli.calque.edition.ZEditorPanelInterface#pointMove
   * (double, double)
   */
  @Override
  public void pointMove(double _x, double _y) {
    if (edition_!=null&&edition_.getFormeEnCours()!=null) {
      if (edition_.getFormeEnCours().getForme()==DeForme.RECTANGLE)
        rectangleBuilderGUI_.updateMove(_x, _y);
      if (edition_.getFormeEnCours().getForme()==DeForme.ELLIPSE)
        ellipseBuilderGUI_.updateMove(_x, _y);
    }
  }
}
