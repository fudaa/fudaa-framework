/*
 * @file         SelectionListener.java
 * @creation     1998-12-15
 * @modification $Date: 2005-10-03 10:35:14 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;
import java.util.EventListener;
/**
 * Interface auditeur de l'evenement <I>SelectionEvent</I>.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-10-03 10:35:14 $ by $Author: deniger $
 * @author       Bertrand Marchand
 * @see         org.fudaa.ebli.calque.SelectionEvent
 * @see         org.fudaa.ebli.calque.BCalqueSelectionInteraction
 */
public interface SelectionListener extends EventListener {
  /**
   * M�thode d�clench�e � la suite d'une s�lection d'objets GrContour.
   *
   * Lors d'une op�ration de s�lection, plusieurs �venements peuvent �tre
   * d�clench�s (1 par calque s�lectionn� dans l'arbre des calques). Ces �v�nements
   * constituent un groupe d'�v�nement. Chaque nouvelle op�ration d�clenche un
   * nouveau groupe d'�v�nements.
   *
   * Pour savoir si l'�v�nement recu est le dernier du groupe, tester dans la
   * m�thode si SelectionEvent e.hasNext() est �gal � false
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  void selectedObjects(SelectionEvent _evt);
}
