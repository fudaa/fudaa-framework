/*
 * @file         EbliAdapteurSuiviSouris.java
 * @creation     1998-10-29
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.repere.CoordonneesEvent;
import org.fudaa.ebli.repere.CoordonneesListener;
import si.uom.SI;

import javax.swing.*;
import java.text.DecimalFormat;

/**
 * Adapteur de CoordonneesEvent vers un JLabel. Il est possible de modifier l'affichage des
 * coordonnees. Pour cela, utilisez la m�thode setCoordinateDefinitions().
 *
 * @author Guillaume Desnoix
 * @version $Id: EbliAdapteurSuiviSouris.java,v 1.15 2006-09-19 14:55:47 deniger Exp $
 */
public class EbliAdapteurSuiviSouris implements CoordonneesListener {
  private JLabel jLabel;
  // Les d�finitions de coordonn�es par defaut.
  private EbliCoordinateDefinition[] defs_;

  public EbliAdapteurSuiviSouris(final JLabel jLabel) {
    this.jLabel = jLabel;

    resetCoordinateDefinitions();
  }

  /**
   * R�initialise les definitions de coordonn�es, avant un #setCoordinateDefinitions.
   */
  private void resetCoordinateDefinitions() {
    defs_ = new EbliCoordinateDefinition[]{
        new EbliCoordinateDefinition("X", new EbliFormatter(SI.METRE)),
        new EbliCoordinateDefinition("Y", new EbliFormatter(SI.METRE)),
        new EbliCoordinateDefinition("Z", new EbliFormatter(SI.METRE))
    };

    DecimalFormat fmt = CtuluLib.getDecimalFormat(EbliPreferences.EBLI.getIntegerProperty(CtuluResource.COORDS_VISU_NB_DIGITS, 2));
    ((CtuluNumberFormatDefault) defs_[0].getFormatter().getXYFormatter()).setFmt(fmt);
    ((CtuluNumberFormatDefault) defs_[1].getFormatter().getXYFormatter()).setFmt(fmt);
    ((CtuluNumberFormatDefault) defs_[2].getFormatter().getXYFormatter()).setFmt(CtuluLib.getDecimalFormat(10));
  }

  /**
   * @param _format Le format, modifie tous les formats des d�finitions
   *     coordonn�es.
   * @deprecated Use setCoordinates instead.
   */
  @Deprecated
  public void format(final EbliFormatterInterface _format) {
    for (EbliCoordinateDefinition def : defs_) {
      def.setFormatter(_format);
    }
    updateLabel();
  }

  /**
   * @return Le format retourn� est celui de la premi�re coordonn�e.
   * @deprecated Use getCoordinates instead.
   */
  @Deprecated
  public EbliFormatterInterface format() {
    return defs_[0].getFormatter();
  }

  /**
   * Definit les coordonn�es affich�es dans le label. Le tableau doit �tre de
   * longueur 1 au minimum et 3 au maximum.
   *
   * @param ebliCoordinateDefinitions Les d�finitions pour chaque coordonn�e dans l'ordre.
   */
  public void setCoordinateDefinitions(EbliCoordinateDefinition[] ebliCoordinateDefinitions) {
    resetCoordinateDefinitions();
    for (int i = 0; i < Math.min(defs_.length, ebliCoordinateDefinitions.length); i++) {
      defs_[i] = ebliCoordinateDefinitions[i];
    }
  }

  /**
   * Retourne les d�finition des coordonn�es du syst�me de coordonn�es.
   *
   * @return Les d�finitions
   */
  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return defs_;
  }

  private double[] oldValues;

  @Override
  public void coordonneesModifiees(final CoordonneesEvent coordonneesEvent) {
    oldValues = coordonneesEvent.valeur();
    updateLabel();
  }

  /**
   * Affichage des coordonn�es sous la forme X:{x}, Y:{y} ({unit�}) ou
   * X:{x} ({unit�}) Y:{y} ({unit�}) si l'unit� est diff�rente suivant X ou Y.
   */
  private void updateLabel() {
    boolean sameUnit = true;
    for (int i = 0; i < defs_.length - 1; i++) {
      sameUnit &= defs_[i].getFormatter().getUnit().equals(defs_[i + 1].getFormatter().getUnit());
    }

    final StringBuilder r = new StringBuilder(100);
    if (oldValues != null) {
      for (int i = 0; i < oldValues.length; i++) {
        if (defs_.length > i) {
          r.append(' ').append(defs_[i].getName()).append(':');
          r.append(defs_[i].getFormatter().getXYFormatter().format(oldValues[i]));
          if (!sameUnit || i == oldValues.length - 1) {
            r.append(CtuluLibString.ESPACE).append('(').append(defs_[i].getFormatter().getUnit()).append(')');
          }
        } else {
          r.append(' ').append(oldValues[i]);
        }
      }
    }
    jLabel.setText(r.toString());
  }
}
