/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.calque.action;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.ZScene.SceneSelectionHelper;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZSceneEditor;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Une action pour scinder une g�om�trie sur le sommet d�sign�. Valable uniquement sur des calques
 * {@link ZCalqueEditable}. Ne fonctionne pas sur des polygones.
 * @author Bertrand Marchand
 */
public class SceneSplitAction extends EbliActionSimple implements ZSelectionListener {
  ZSceneEditor editor_;
  
  public SceneSplitAction(ZSceneEditor _editor) {
    super(EbliLib.getS("Scinder"), BuResource.BU.getIcon("couperligne"), "GIS_SPLIT");
    editor_=_editor;
    editor_.getScene().addSelectionListener(this);
    setDefaultToolTip(EbliLib.getS("Scinder la g�om�trie"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    editor_.splitSelectedObject();
  }

  @Override
  public void updateStateBeforeShow() {
    ZScene scn=editor_.getScene();
    SceneSelectionHelper hlp=editor_.getScene().getSelectionHelper();
    int idGeom=-1;

    boolean b=true;
    // Si la selection n'est pas null et atomique
    b=b && !scn.isSelectionEmpty() && (scn.getSelectionMode() == SelectionMode.ATOMIC);
    // Si la selection est sur le m�me objet.
    b=b && (idGeom=hlp.getUniqueSelectedIdx())!=-1;
    // Si le nombre d'atomiques est de 2 cons�cutifs sur une g�om�trie de type polyligne.
    if (b && scn.getObject(idGeom) instanceof GISPolyligne) {
      b=b && hlp.getNbAtomicSelected()==2;
      b=b && Math.abs(hlp.getUniqueAtomicSelection().getMinIndex()-hlp.getUniqueAtomicSelection().getMaxIndex())==1;
      if (b) putValue(Action.NAME, EbliLib.getS("Scinder la polyligne"));
    }
    // Si ou est sur un multipoint
    else {
      b=b && (scn.getObject(idGeom) instanceof GISMultiPoint);
      if (b) putValue(Action.NAME, EbliLib.getS("Scinder le multipoint"));
    }
    if (!b) putValue(Action.NAME, EbliLib.getS("Scinder"));

    setEnabled(b);
  }

  @Override
  public String getEnableCondition() {
    return EbliLib.getS("S�lectionner 2 sommets consecutifs d'une polyligne<br>ou les sommets d'un multipoint");
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    updateStateBeforeShow();
  }
}
