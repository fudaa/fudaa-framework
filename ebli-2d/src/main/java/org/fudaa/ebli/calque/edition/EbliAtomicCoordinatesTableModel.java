/*
 *  @creation     15 avr. 2005
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gui.CtuluCellDoubleRenderer;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliLib;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LinearRing;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

/**
 * @author Fred Deniger
 * @version $Id: EbliAtomicCoordinatesTableModel.java,v 1.5 2006-09-19 14:55:49 deniger Exp $
 */
public abstract class EbliAtomicCoordinatesTableModel extends AbstractTableModel {
  /**
   * @author Fred Deniger
   * @version $Id: EbliAtomicCoordinatesTableModel.java,v 1.5 2006-09-19 14:55:49 deniger Exp $
   */
  public final static class Point extends EbliAtomicCoordinatesTableModel {
    /**
     * @param _zone
     * @param _selectedAtomic
     */
    public Point(final EbliCoordinateDefinition[] _defs, final GISZoneCollectionPoint _zone,
                 final int[] _selectedAtomic, final boolean _useAttribute) {
      super(_defs, _zone, _selectedAtomic);
      g_ = _zone.getAttachedSequence();
      modeles_ = _useAttribute ? _zone.getModels() : new GISAttributeModel[0];
    }

    @Override
    protected void apply(final CtuluCommandComposite _cmp) {
    }

    @Override
    public int getRowCount() {
      if (selectedAtomic_ != null) {
        return selectedAtomic_.length;
      }
      return zone_.getNumGeometries();
    }

    @Override
    protected CtuluAnalyze isValid() {
      return null;
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: EbliAtomicCoordinatesTableModel.java,v 1.5 2006-09-19 14:55:49 deniger Exp $
   */
  public static class Line extends EbliAtomicCoordinatesTableModel {
    boolean isFerme_;
    int idx_;
    final ZModeleEditable modele_;

    public Line(final EbliCoordinateDefinition[] _defs, final GISZoneCollection _zone, final int _idx,
                final int[] _selectedAtomic, final boolean _useAttribute, final ZModeleEditable _model) {
      super(_defs, _zone, _selectedAtomic);
      modele_ = _model;
      idx_ = _idx;
      if (selectedAtomic_ == null) {
        isFerme_ = zone_.getGeometry(idx_) instanceof LinearRing;
      }
      modeles_ = _useAttribute ? _zone.getAtomicAttributeSubModel(_idx) : new GISAttributeModel[0];
      g_ = _zone.getCoordinateSequence(idx_);
    }

    @Override
    public int getRowCount() {
      if (selectedAtomic_ != null) {
        return selectedAtomic_.length;
      }
      return isFerme_ ? g_.size() - 1 : g_.size();
    }

    @Override
    protected CtuluAnalyze isValid() {
      if (modele_ == null) {
        return null;
      }
      final CoordinateSequence seq = getNewCoordinateSequence();
      if (seq != null) {
        final CtuluAnalyze ana = new CtuluAnalyze();
        if (!modele_.isCoordinateValid(seq, ana)) {
          return ana;
        }
      }
      return null;
    }

    /**
     * @return les nouvelles coordonnees. null si pas de modif
     */
    public CoordinateSequence getNewCoordinateSequence() {
      if (rowNewCoordinates_ == null) {
        return null;
      }
      final GISCoordinateSequence newSeq = new GISCoordinateSequence(g_);
      final TIntObjectIterator it = rowNewCoordinates_.iterator();
      for (int j = rowNewCoordinates_.size(); j-- > 0; ) {
        it.advance();
        final int idx = it.key();
        final Coordinate c = (Coordinate) it.value();
        newSeq.setX(idx, c.x);
        newSeq.setY(idx, c.y);
      }
      if (isFerme_ && rowNewCoordinates_.contains(0)) {
        final int last = newSeq.size() - 1;
        final Coordinate c = (Coordinate) rowNewCoordinates_.get(0);
        newSeq.setX(last, c.x);
        newSeq.setY(last, c.y);
      }
      return newSeq;
    }

    @Override
    protected void apply(final CtuluCommandComposite _cmp) {
      if (modelesNewValues_ != null) {
        for (int i = modelesNewValues_.length - 1; i >= 0; i--) {
          final TIntObjectHashMap val = modelesNewValues_[i];
          if (val != null) {
            final TIntObjectIterator it = val.iterator();
            final GISAttributeModel dataModel = modeles_[i];
            for (int j = val.size(); j-- > 0; ) {
              it.advance();
              dataModel.setObject(it.key(), it.value(), _cmp);
            }
          }
        }
        modelesNewValues_ = null;
      }
      final CoordinateSequence newSeq = this.getNewCoordinateSequence();
      if (newSeq != null) {
        zone_.setCoordinateSequence(idx_, newSeq, _cmp);
      }
      rowNewCoordinates_ = null;
    }
  }

  GISAttributeModel[] modeles_;
  TIntObjectHashMap[] modelesNewValues_;
  CoordinateSequence g_;
  TIntObjectHashMap rowNewCoordinates_;
  int[] selectedAtomic_;
  GISZoneCollection zone_;
  EbliCoordinateDefinition[] coordDefs_;

  public boolean isModified() {
    return modelesNewValues_ != null || rowNewCoordinates_ != null;
  }

  public EbliAtomicCoordinatesTableModel(final EbliCoordinateDefinition[] _defs, final GISZoneCollection _zone,
                                         final int[] _selectedAtomic) {
    zone_ = _zone;
    coordDefs_ = _defs;
    selectedAtomic_ = _selectedAtomic;
  }

  protected abstract void apply(CtuluCommandComposite _cmp);

  protected abstract CtuluAnalyze isValid();

  private int getRealIdx(final int _rowIdx) {
    return selectedAtomic_ == null ? _rowIdx : selectedAtomic_[_rowIdx];
  }

  public void updateEditorAndRenderer(final JTable _table) {
    final TableColumnModel cols = _table.getColumnModel();
    // xy
    TableCellEditor editorXY = CtuluValueEditorDefaults.DOUBLE_EDITOR.createTableEditorComponent();
    cols.getColumn(1).setCellEditor(editorXY);
    cols.getColumn(2).setCellEditor(editorXY);
    cols.getColumn(1).setCellRenderer(new CtuluCellDoubleRenderer(coordDefs_[0].getFormatter().getXYFormatter()));
    cols.getColumn(2).setCellRenderer(new CtuluCellDoubleRenderer(coordDefs_[1].getFormatter().getXYFormatter()));

    // les attributs
    if (modeles_ != null) {
      for (int i = 0; i < modeles_.length; i++) {
        // L'editor
        final CtuluValueEditorI editor = modeles_[i].getAttribute().getEditor();
        if (editor != null) {
          cols.getColumn(3 + i).setCellEditor(editor.createTableEditorComponent());
          cols.getColumn(3 + i).setCellRenderer(editor.createTableRenderer());
        }
      }
    }
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      return;
    }
    final int realIdx = getRealIdx(_rowIndex);
    if (_columnIndex > 2) {
      // l'indice de l'attribut atomic
      final int val = _columnIndex - 3;
      if (modelesNewValues_ == null) {
        modelesNewValues_ = new TIntObjectHashMap[modeles_.length];
      }
      if (modelesNewValues_[val] == null) {
        modelesNewValues_[val] = new TIntObjectHashMap();
      }
      modelesNewValues_[val].put(realIdx, _value);
    } else {
      if (rowNewCoordinates_ == null) {
        rowNewCoordinates_ = new TIntObjectHashMap(g_.size());
      }
      Coordinate c = (Coordinate) rowNewCoordinates_.get(realIdx);
      if (c == null) {
        c = new Coordinate();
        c.x = g_.getX(realIdx);
        c.y = g_.getY(realIdx);
        rowNewCoordinates_.put(realIdx, c);
      }
      if (_columnIndex == 1) {
        c.x = ((Double) _value).doubleValue();
      } else {
        c.y = ((Double) _value).doubleValue();
      }
    }
    fireTableCellUpdated(_rowIndex, _columnIndex);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    if (_columnIndex == 0) {
      return false;
    }
    if (_columnIndex <= 2) {
      return zone_.isGeomModifiable();
    }
    return true;
  }

  private Object getAtomicValue(final int _row, final int _valIdx) {
    if (modelesNewValues_ != null && (modelesNewValues_[_valIdx] != null) && modelesNewValues_[_valIdx].contains(_row)) {
      return modelesNewValues_[_valIdx].get(_row);
    }
    return modeles_[_valIdx].getObjectValueAt(_row);
  }

  private Class getAtomicClass(final int _valIdx) {
    return modeles_[_valIdx].getAttribute().getDataClass();
  }

  private double getX(final int _row) {
    if (rowNewCoordinates_ != null && rowNewCoordinates_.contains(_row)) {
      return ((Coordinate) rowNewCoordinates_.get(_row)).x;
    }
    return g_.getX(_row);
  }

  private double getY(final int _row) {
    if (rowNewCoordinates_ != null && rowNewCoordinates_.contains(_row)) {
      return ((Coordinate) rowNewCoordinates_.get(_row)).y;
    }
    return g_.getY(_row);
  }

  @Override
  public int getColumnCount() {
    return (modeles_ == null ? 0 : modeles_.length) + 3;
  }

  @Override
  public String getColumnName(final int _columnIndex) {
    switch (_columnIndex) {
      case 0:
        return EbliLib.getS("Index");
      case 1:
        return coordDefs_[0].getName();
      case 2:
        return coordDefs_[1].getName();
      default:
    }
    final int idx = _columnIndex - 3;
    if (idx >= 0) {
      return modeles_[idx].getAttribute().getName();
    }
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public Class<?> getColumnClass(int _columnIndex) {
    switch (_columnIndex) {
      case 0:
        return Integer.class;
      case 1:
        return Double.class;
      case 2:
        return Double.class;
    }
    final int idx = _columnIndex - 3;
    return getAtomicClass(_columnIndex - 3);
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    final int realIdx = getRealIdx(_rowIndex);
    switch (_columnIndex) {
      case 0:
        return realIdx + 1;
      case 1:
        return getX(realIdx);
      case 2:
        return getY(realIdx);
      default:
    }
    final int idx = _columnIndex - 3;
    if (idx >= 0) {
      return getAtomicValue(realIdx, idx);
    }
    return CtuluLibString.EMPTY_STRING;
  }
}
