package org.fudaa.ebli.calque.action;

import java.awt.event.ActionEvent;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BVueCalque;
import org.fudaa.ebli.calque.ZCalqueAffichageDonnees;
import org.fudaa.ebli.calque.ZScene;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Permet d'ajouter un sommet sur la ligne editable s�lectionn�e. Action � utiliser depuis le menu contextuel dans la vue calque.
 *
 * @author deniger ( genesis)
 */
public class AddVertexAction extends EbliActionSimple {

  private final ZScene scene;

  public AddVertexAction(ZScene scene) {
    super(EbliLib.getS("Ajouter un sommet"), EbliResource.EBLI.getToolIcon("node-add"), "ADD_SOMMET");
    assert scene != null;
    this.scene = scene;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    updateStateBeforeShow();
    if (isEnabled()) {
      ZCalqueEditable calqueActif = (ZCalqueEditable) scene.getCalqueActif();
      ZEditorDefault editor = (ZEditorDefault) calqueActif.getEditor();
      CtuluCommandComposite cmp = null;
      if (editor.getMng() != null) {
        cmp = new CtuluCommandComposite(EbliLib.getS("Ajout d'un sommet"));
      }
      Object source = _e.getSource();
      GrPoint pointReel = null;
      if (source instanceof JComponent) {
        JPopupMenu ancestorOfClass = (JPopupMenu) SwingUtilities.getAncestorOfClass(JPopupMenu.class, (JComponent) source);
        if (ancestorOfClass != null) {
          pointReel = (GrPoint) ancestorOfClass.getClientProperty(BVueCalque.POPUP_INIT_SCREEN_LOCATION);
          if (pointReel != null) {
            pointReel = pointReel.applique(scene.getCalqueActif().getVersReel());
          }
        }
      }
      if (pointReel == null) {
        return;
      }
      if (calqueActif.addAtomeProjectedOnSelectedGeometry(pointReel, cmp, null)) {
        scene.repainSelection();
        if (editor.getMng() != null && cmp != null) {
          editor.getMng().addCmd(cmp.getSimplify());
        }
      }
    }
  }

  @Override
  public void updateStateBeforeShow() {
    BCalque calqueActif = scene.getCalqueActif();
    boolean enable = false;
    if (calqueActif instanceof ZCalqueEditable) {
      ZCalqueEditable calqueEditable = (ZCalqueEditable) calqueActif;
      if (!calqueEditable.isSelectionEmpty()) {
        ZModeleEditable modelEditable = calqueEditable.getModelEditable();
        if (modelEditable.getGeomData() != null && modelEditable.getGeomData().isGeomModifiable()) {
          if (scene.canUseAtomicMode(calqueActif)) {
            enable = calqueActif instanceof ZCalqueAffichageDonnees && ((ZCalqueAffichageDonnees) calqueActif).getNbSelected() == 1;
          }
        }
      }
    }
    setEnabled(enable);
  }

  public boolean isActivatedFor(BCalque cqActif) {
    return scene.canUseAtomicMode(cqActif);
  }
}
