package org.fudaa.ebli.calque.edition;

/**
 * Un validateur pour l'edition des objets selectionn�s.
 * @author Bertrand Marchand (marchand@detacad.fr)
 *
 */
public interface ZEditorValidatorI {
  
  /**
   * Pour le panneau editant plusieurs objets a la fois.
   * @return True : Indique que l'edition est ok, et que la fenetre d'�dition peut �tre ferm�e. False : La fenetre reste ouverte.
   */
  public boolean isMultiEditionValid(ZModeleEditable _mdl);
  
  /**
   * Pour le panneau editant un seul objet.
   * @return True : Indique que l'edition est ok, et que la fenetre d'�dition peut �tre ferm�e. False : La fenetre reste ouverte.
   */
  public boolean isSingleEditionValid(ZModeleEditable _mdl);
  
  /**
   * Pour le panneau editant des atomics.
   * @return True : Indique que l'edition est ok, et que la fenetre d'�dition peut �tre ferm�e. False : La fenetre reste ouverte.
   */
  public boolean isAtomicEditionValid(ZModeleEditable _mdl);
  
  /**
   * Pour le panneau editant des segments.
   * @return True : Indique que l'edition est ok, et que la fenetre d'�dition peut �tre ferm�e. False : La fenetre reste ouverte.
   */
  public boolean isSegmentEditionValid(ZModeleEditable _mdl);

}
