/*
 * @file         BCalqueSuiviSourisInteraction.java
 * @creation     1998-10-16
 * @modification $Date: 2006-09-19 14:55:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.fu.FuEmptyArrays;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Arrays;
import java.util.HashSet;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Un calque pour accrocher les objets de la scene. Si la scene n'existe pas, pas d'objets accroch�. Des modificateurs permettent l'accrochage ou non
 * (key u, key n).
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZCalqueCatchInteraction extends BCalqueInteraction implements
        MouseMotionListener, MouseListener, KeyListener {

  /**
   * La tol�rance en pixels pour l'accrochage
   */
  private static int tolerancePixel_ = 4;

  private int keyAccrocheActive = KeyEvent.VK_N;
  /**
   * La scene
   */
  private ZScene scene_;
  /**
   * Un point temporaire remis a jour a chaque mouvement de souris
   */
  private GrPoint ptTmp_ = new GrPoint();
  /**
   * True si le mode d'accrochage est actif
   */
  private boolean bcatch_ = true;
  /**
   * Les listeners de l'accrochage
   */
  private HashSet<ZCatchListener> listeners_ = new HashSet<ZCatchListener>();
  /**
   * La liste de selection des points d'accrochage
   */
  private ZSceneSelection selAccroch_ = null;
  /**
   * Le tableau des indices de g�om�tries accroch�es
   */
  private int[] idxGeoms_;
  /**
   * Le tableau des indices de sommets accroch�s
   */
  private int[] idxVertices_;
  /**
   * L'indice sur les indices de g�om�tries
   */
  private int iidxGeom_ = -1;
  /**
   * L'indice sur les indices de vertices
   */
  private int iidxVertex_ = -1;

  public ZCalqueCatchInteraction() {
  }

  /**
   * Methode inactive.
   */
  public void mouseDown(final MouseEvent _evt) {
  }

  /**
   * Methode inactive.
   */
  @Override
  public void mouseEntered(final MouseEvent _evt) {
  }

  public void setKeyAccrocheActive(int keyAccrocheActive) {
    this.keyAccrocheActive = keyAccrocheActive;
  }

  /**
   * Methode inactive.
   */
  @Override
  public void mouseExited(final MouseEvent _evt) {
  }

  /**
   * Methode inactive.
   */
  @Override
  public void mousePressed(final MouseEvent _evt) {
  }

  /**
   * Methode inactive.
   */
  @Override
  public void mouseReleased(final MouseEvent _evt) {
  }

  /**
   * Methode inactive.
   */
  public void mouseUp(final MouseEvent _evt) {
  }

  /**
   * Methode inactive.
   */
  @Override
  public void mouseClicked(final MouseEvent _evt) {
  }

  /**
   * Appelle la methode <I>mouseMoved</I>.
   */
  @Override
  public void mouseDragged(final MouseEvent _evt) {
    mouseMoved(_evt);
  }

  /**
   * Envoi d'un evenement <I>ZCatchEvent</I> (en mode non gele).
   */
  @Override
  public void mouseMoved(final MouseEvent _evt) {

    ptTmp_.x_ = _evt.getX();
    ptTmp_.y_ = _evt.getY();
//Fred ajout de isCachingEnableForOneLayer: si aucun calque ne se souci du catchevent pas besoin de le calculer.
    if (!isGele() && isCachingEnableForOneLayer() && scene_ != null && bcatch_) {
      mouseMoved(ptTmp_);
    }
  }

  public boolean isSomethingCatched() {
    return !CtuluLibArray.isEmpty(idxGeoms_);
  }

  /**
   * Recherche les points a accrocher. Propose par defaut le plus en avant.
   *
   * @param _pt
   */
  public void mouseMoved(GrPoint _pt) {
    if (scene_ == null) {
      return;
    }

    int idxGeomOld = -1;
    int idxVertexOld = -1;
    if (iidxGeom_ != -1) {
      idxGeomOld = idxGeoms_[iidxGeom_];
      idxVertexOld = idxVertices_[iidxVertex_];
    }

    selAccroch_ = scene_.selectionForAccroche(_pt.applique(getVersReel()), tolerancePixel_);
    idxGeoms_ = selAccroch_.getIdxSelected();
    Arrays.sort(idxGeoms_);

    int igeom = -1;
    int ivertex = -1;

    if (idxGeoms_.length != 0) {
      igeom = 0;
      // Recherche dans le nouveau tableau de l'indice.
      if (idxGeomOld != -1) {
        for (int i = 0; i < idxGeoms_.length; i++) {
          if (idxGeoms_[i] == idxGeomOld) {
            igeom = i;
            break;
          }
        }
      }
      idxVertices_ = selAccroch_.getSelection(idxGeoms_[igeom]).getSelectedIndex();
      if (idxVertices_.length != 0) {
        ivertex = 0;
      }
      // Recherche dans le nouveau tableau de l'indice.
      if (idxVertexOld != -1) {
        for (int i = 0; i < idxVertices_.length; i++) {
          if (idxVertices_[i] == idxGeomOld) {
            ivertex = i;
            break;
          }
        }
      }
    }

    if (ivertex == -1) {
      if (iidxVertex_ != -1) {
        fireCatchEvent(ZCatchEvent.UNCAUGHT, 0, 0);
      }
    } else if (iidxVertex_ == -1) {
      if (ivertex != -1) {
        fireCatchEvent(ZCatchEvent.CAUGHT, idxGeoms_[igeom], idxVertices_[ivertex]);
      }
    } else if (idxGeomOld != idxGeoms_[igeom] || idxVertexOld != idxVertices_[ivertex]) {
      fireCatchEvent(ZCatchEvent.UNCAUGHT, 0, 0);
      fireCatchEvent(ZCatchEvent.CAUGHT, idxGeoms_[igeom], idxVertices_[ivertex]);
    }

    iidxGeom_ = igeom;
    iidxVertex_ = ivertex;
  }

  /**
   * Definit la tol�rence d'accrocahge.
   *
   * @param _tolPixel La tolerance, en pixel.
   */
  public void setTolerance(int _tolPixel) {
    tolerancePixel_ = _tolPixel;
  }

  /**
   * Definit la scene.
   *
   * @param _scn La scene.
   */
  public void setScene(ZScene _scn) {
    scene_ = _scn;
  }

  public void addCatchListener(ZCatchListener _l) {
    listeners_.add(_l);
  }

  public void removeCatchListener(ZCatchListener _l) {
    listeners_.remove(_l);
  }

  /**
   * Notification des listeners.
   *
   * @param _type Le type d'evenement.
   * @param _pt Le point d'accrochage.
   */
  public void fireCatchEvent(int _type, int _idxGeom, int _idxVertex) {
    ZCatchEvent evt = new ZCatchEvent(this, _type, selAccroch_, _idxGeom, _idxVertex);
    for (ZCatchListener l : listeners_) {
      if (l.isCachingEnabled()) {
        l.catchChanged(evt);
      }
    }
  }

  boolean isCachingEnableForOneLayer() {
    for (ZCatchListener l : listeners_) {
      if (l.isCachingEnabled()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gere lees actions 'suivant' et 'decrochage'
   * .<p>
   * U : pour inactiver le mode d'accrochage, N : Pour switcher de point accroch�.
   */
  @Override
  public void keyPressed(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_U) {
      bcatch_ = false;
      fireCatchEvent(ZCatchEvent.UNCAUGHT, 0, 0);
    }
    if (e.getKeyCode() == keyAccrocheActive) {
      if (selAccroch_.getNbSelectedItem() > 1) {
        iidxVertex_++;
        if (iidxVertex_ == idxVertices_.length) {
          iidxVertex_ = 0;
          iidxGeom_++;
          if (iidxGeom_ == idxGeoms_.length) {
            iidxGeom_ = 0;
          }
          idxVertices_ = selAccroch_.getSelection(idxGeoms_[iidxGeom_]).getSelectedIndex();
        }
        fireCatchEvent(ZCatchEvent.CAUGHT, idxGeoms_[iidxGeom_], idxVertices_[iidxVertex_]);
      }
    }
  }

  @Override
  public void setGele(boolean _gele) {
    super.setGele(_gele);
    if (_gele) {
//      bcatch_ = false;
      fireCatchEvent(ZCatchEvent.UNCAUGHT, 0, 0);
      idxGeoms_ = FuEmptyArrays.INT0;
      idxVertices_ = FuEmptyArrays.INT0;
      iidxGeom_ = -1;
    }
  }

  /**
   * Gere le SHIFT.<p>
   * SHIFT : pour reactiver le mode d'accrochage
   */
  @Override
  public void keyReleased(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_U) {
      bcatch_ = true;
      iidxVertex_ = -1;
      iidxGeom_ = -1;
      mouseMoved(ptTmp_);
    }
  }

  /**
   * Methode inactive.
   */
  @Override
  public void keyTyped(KeyEvent e) {
  }
}
