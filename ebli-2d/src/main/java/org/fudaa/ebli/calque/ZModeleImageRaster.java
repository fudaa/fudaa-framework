/*
 *  @file         ZModeleImageRaster.java
 *  @creation     2002-09-02
 *  @modification $Date: 2007-04-20 16:21:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.RasterData;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Un modele pour une image Raster. Il s'agit de resoudre le systeme P=A*Pi+T ou P est un point de coordonnees reelles
 * et Pi le point du pixel correspondant. Pour resoudre ce systeme, il faut 3 paires de points. On suppose que
 * A=((a,b),(c,d)) et T=(tx,ty); et P=A*Pi+T.
 * 
 * @version $Id: ZModeleImageRaster.java,v 1.11 2007-04-20 16:21:38 deniger Exp $
 * @author Fred Deniger
 */
public interface ZModeleImageRaster extends ZModeleImage, ZModelePoint, ZModeleEditable {

  AffineTransform getRasterTransform();

  RasterData getData();
  
  boolean isImageLoaded();

  double getErreur(int _i);

  void setProj(final Point2D.Double[] _img, final Point2D.Double[] _reel, AffineTransform _trans, double[] _err);

  /**
   * @param _img peut etre null. Dans ce cas le fichier doit exister
   */
  void setImage(final CtuluImageContainer _img);

  int getHImg();

  /**
   * @param _img les coordonnees en pixel des points de calage
   * @param _reel les coordonnées reelles correspondantes
   */
  void setProj(final Point2D.Double[] _img, final Point2D.Double[] _reel);

  /**
   * @param _i l'indice du point de calage
   * @return le x en pixel du point sur l'image
   */
  int getXImgCalage(int _i);

  int getYImgCalageFromTop(int _i);

  BufferedImage getImage(GrBoite _clipReel, int _widthEcran, int _heightEcran, AffineTransform _toReel, boolean _quick);

  public ZModeleImageRaster cloneModel();

}
