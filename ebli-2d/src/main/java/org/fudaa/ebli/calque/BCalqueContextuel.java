/*
 * @file         BCalqueContextuel.java
 * @creation     1999-07-07
 * @modification $Date: 2006-09-19 14:55:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Un calque de menu contextuel.
 * 
 * @version $Revision: 1.15 $ $Date: 2006-09-19 14:55:47 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BCalqueContextuel extends BCalqueInteraction implements MouseListener {

  private final BCalqueContextuelListener listener_;

  public BCalqueContextuel(final BCalqueContextuelListener _listener) {
    listener_ = _listener;
    setDestructible(false);
  }

  // Icon
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null </I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(Color.black);
    _g.drawRect(_x + 5, _y + 5, w - 8, h - 8);
    _g.setColor(UIManager.getColor("MenuItem.background"));
    _g.fill3DRect(_x + 6, _y + 6, w - 9, h - 9, true);
    _g.setColor(UIManager.getColor("MenuItem.foreground"));
    for (int j = 9; j < h - 5; j += 3) {
      _g.drawLine(_x + 9, _y + j, _x + w - 7, _y + j);
    }
  }

  private void isPopup(final MouseEvent _e) {
    if (isGele()) {
      return;
    }
    if (EbliLib.isPopupMouseEvent(_e)) {
      _e.consume();
      popMenu(_e.getX(), _e.getY());
    }
  }

  /**
   * @param _x le x du popup menu
   * @param _y le y du popup menu
   */
  public void popMenu(final int _x, final int _y) {
    final JPopupMenu menu = listener_.getCmdsContextuelles();
    if (menu == null) {
      return;
    }
    menu.pack();
    final Component in = menu.getInvoker();
    menu.show(in == null ? this : in, _x, _y);
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
    isPopup(_e);
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {}

  @Override
  public void mouseExited(final MouseEvent _e) {}

  @Override
  public void mousePressed(final MouseEvent _e) {
    isPopup(_e);
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    isPopup(_e);
  }
}