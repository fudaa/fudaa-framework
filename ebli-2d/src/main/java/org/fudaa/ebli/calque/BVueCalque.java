/**
 * @creation 1998-10-07
 * @modification $Date: 2007-06-11 13:07:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuTaskView;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.*;
import org.fudaa.ebli.repere.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterGraphics;
import java.util.Map;

/**
 * Un composant d'affichage des calques. La vue contient UN calque qui est la racine de la hierarchie arborescente des calques.
 *
 * @author Guillaume Desnoix
 * @version $Id: BVueCalque.java,v 1.34 2007-06-11 13:07:43 deniger Exp $
 */
public class BVueCalque extends AbstractVueCalque implements ActionListener, MouseListener, RepereMouseKeyTarget {
  /**
   * Utilise pour stocker le point ( coordonn�es �cran) a partir duquel le popupmenu a �t� ouvert.
   */
  public static final String POPUP_INIT_SCREEN_LOCATION = "INIT_SCREEN_LOCATION";
  // Evenements
  private boolean ajustement_;
  // Proprietes
  private BCalque calque_;
  private BCalqueContextuelListener listener_;
  private double[][] repere_;
  private BuTaskView taskView_;
  // Paint
  Image cache_;
  Insets insets_;
  double[][] lastRepere_;
  boolean noRepaintTask_;
  final RepereMouseKeyController repereController_;
  GrVecteur vect_ = new GrVecteur();
  public double margeXCentre;
  public double margeYCentre;

  public BVueCalque(final BCalque _c) {
    repere_ = new double[4][3];
    for (int j = 0; j < 3; j++) {
      repere_[0][j] = 1.;
    }
    // setLayout(new BuBorderLayout());
    setLayout(new BorderLayout(0, 0));
    setOpaque(true);
    // setDoubleBuffered(false);
    setDefaultCursor();

    setCalque(_c);
    setFocusable(true);
    repereController_ = new RepereMouseKeyController(this);
    // pour le menu contextuel
    addMouseListener(this);
  }

  @Override
  public void setCursor(Cursor cursor) {
    super.setCursor(cursor);
  }

  private boolean checkBoite = true;

  private boolean checkBoite(final GrBoite _nouvBoite, final boolean _checkX, final boolean _checkY) {
    //dans certains cas, on ne veut pas controller la boite du zoom
    if (!checkBoite) {
      return true;
    }
    final GrBoite bVue = getCalque().getDomaine();
    if (bVue == null) {
      return false;
    }
    final double deltaX = _nouvBoite.getDeltaX();
    final double deltaY = _nouvBoite.getDeltaY();
    // marge en pourcent accept�e
    final double marge = 0.9;
    if (_checkX) {
      // si la boite est en dehors

      if (_nouvBoite.getMinX() > bVue.getMaxX()) {
        _nouvBoite.setMinX(bVue.getMaxX() - deltaX / 5);
        _nouvBoite.setMaxX(_nouvBoite.getMinX() + deltaX);
      }
      if (_nouvBoite.getMaxX() < bVue.getMinX()) {
        _nouvBoite.setMaxX(bVue.getMinX() + deltaX / 5);
        _nouvBoite.setMinX(_nouvBoite.getMaxX() - deltaX);
      }
    }
    if (_checkY) {

      // si la boite est en dehors
      if (_nouvBoite.getMinY() > bVue.getMaxY()) {
        _nouvBoite.setMinY(bVue.getMaxY() - deltaY / 5);
        _nouvBoite.setMaxY(_nouvBoite.getMinY() + deltaY);
      }
      if (_nouvBoite.getMaxY() < bVue.getMinY()) {
        _nouvBoite.setMaxY(bVue.getMinY() + deltaY / 5);
        _nouvBoite.setMinY(_nouvBoite.getMaxY() - deltaY);
      }
    }
    return true;
  }

  private double getTranslateStep(final boolean _fast) {
    return _fast ? 1 : 0.5;
  }

  private double getZoomCoef() {
    return 0.5;
  }

  private void isPopup(final MouseEvent _evt) {
    if (EbliLib.isPopupMouseEvent(_evt)) {
      _evt.consume();
      popMenu(_evt.getX(), _evt.getY());
    }
  }

  /**
   * Affectation de la propriete <I>calque</I>.
   */
  private void setCalque(final BCalque _v) {
    if (calque_ != _v) {
      final BCalque vp = calque_;
      calque_ = _v;
      firePropertyChange("calque", vp, calque_);
      if (vp != null) {
        remove(vp);
      }
      if (calque_ != null) {
        add(calque_, BuBorderLayout.CENTER);
        calque_.revalidate();
      }
      changeMorphismes();
    }
  }

  private void translateX(final double _factor, final boolean _rapide) {
    translation(_factor * getViewBoite().getDeltaX(), 0, _rapide);
  }

  private void translateY(final double _factor, final boolean _fast) {
    translation(0, _factor * getViewBoite().getDeltaY(), _fast);
  }

  @Override
  public synchronized void addMouseListener(MouseListener l) {
    super.addMouseListener(l);
  }

  void superRepaint() {
    cache_ = null;
    super.repaint();
  }

  /**
   * Methode interne de mise a jour du repere de la vue.
   */
  protected boolean ajusteRepere(final RepereEvent _evt) {
    boolean refresh = false;
    // Echelle X,Y
    for (int j = 0; j < 2; j++) {
      if (_evt.aChange(0, j)) {
        repere_[0][j] = (_evt.relatif(0, j) ? repere_[0][j] : 1.) / _evt.valeur(0, j);
        refresh = true;
      }
    }
    // Translation X,Y
    for (int j = 0; j < 3; j++) {
      if (_evt.aChange(1, j)) {
        repere_[1][j] = (_evt.relatif(1, j) ? repere_[1][j] : 0.) - _evt.valeur(1, j);
        refresh = true;
      }
    }
    // Rotation Z
    if (_evt.aChange(2, 2)) {
      repere_[2][2] = (_evt.relatif(2, 2) ? repere_[2][2] : 0.) - _evt.valeur(2, 2);
      refresh = true;
    }
    return refresh;
  }

  /**
   * Methode interne de mise a jour du calque. Elle recalcule les transformations <I>versEcran</I> et <I>versReel</I> du calque a partir de la matrice
   * repere.
   */
  protected void changeMorphismes() {
    Dimension d;
    GrMorphisme m;
    double ex, ey, rz, tx, ty;
    d = getSize();
    ex = repere_[0][0];
    ey = repere_[0][1];
    rz = repere_[2][2];
    tx = repere_[1][0];
    ty = repere_[1][1];
    m = GrMorphisme.identite();
    m.composition(GrMorphisme.dilatation(ex, ey, 0.));
    m.composition(GrMorphisme.rotation(0., 0., rz));
    m.composition(GrMorphisme.translation(tx, ty, 0.));
    m.composition(GrMorphisme.rotation(Math.PI, 0., 0.));
    m.composition(GrMorphisme.translation(0., d.height, 0.));
    getCalque().setVersEcran(m);
    ex = repere_[0][0] == 0. ? 0. : 1. / repere_[0][0];
    ey = repere_[0][1] == 0. ? 0. : 1. / repere_[0][1];
    rz = -repere_[2][2];
    tx = -repere_[1][0];
    ty = -repere_[1][1];
    m = GrMorphisme.identite();
    m.composition(GrMorphisme.translation(0., -d.height, 0.));
    m.composition(GrMorphisme.rotation(-Math.PI, 0., 0.));
    m.composition(GrMorphisme.translation(tx, ty, 0.));
    m.composition(GrMorphisme.rotation(0., 0., rz));
    m.composition(GrMorphisme.dilatation(ex, ey, 0.));
    getCalque().setVersReel(m);
    repaint();
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if ("RESTAURER_REPERE".equals(_evt.getActionCommand())) {
      changeRepere(_evt.getSource(), getCalque().getDomaine());
    }
  }

  /**
   * Effectue un zoom �tendu aux dimensions de la boite.
   *
   * @param _source La source d'ou provient l'�venement Rep�reEvent � g�n�rer.
   * @param _boite La boite englobante.
   */
  public void changeRepere(final Object _source, final GrBoite _boite) {
    changeRepere(_source, _boite, 0.);
  }

  public void changeRepere(final Object _src, final GrBoite _bt, final double _rz) {
    changeRepere(_src, _bt, _rz, 10);
  }

  public void setViewBoite(GrBoite boite) {
    changeRepere(this, boite, 0, 0);
  }

  /**
   * @param _mrg Marge � gauche, � droite, en haut et en bas en pixels.
   */
  public void changeRepere(final Object _source, final GrBoite _boite, final double _rz, final int _mrg) {
    changeRepere(_source, _boite, _rz, _mrg, false);
  }

  public void changeRepere(final Object _source, final GrBoite _boite, final double _rz, final int _mrg,
                           final boolean _rapide) {
    changeRepere(_source, _boite, _rz, _mrg, _mrg, _rapide, true);
  }

  public void changeRepere(final Object _source, final GrBoite _boite, final double _rz, final int _mrg,
                           final boolean _rapide, final boolean changeZ) {
    changeRepere(_source, _boite, _rz, _mrg, _mrg, _rapide, changeZ);
  }

  public void changeRepereZoom(final Object _source, final GrBoite _boite, final double _rz, final int mrgHorizontal, final int mrgVertical,
                               final boolean _rapide, final boolean _changeZ, boolean zoomX) {
    if (_boite == null) {
      return;
    }

    if (!checkBoite(_boite, true, true)) {
      return;
    }
    AxeZoomProcessor redi = new AxeZoomProcessor();
    RepereEvent repere = redi.process(_source, _boite, _rz, mrgHorizontal, mrgVertical, _rapide, _changeZ, this, zoomX);

    repereModifie(repere);
  }

  public void changeRepere(final Object _source, final GrBoite _boite, final double _rz, final int mrgHorizontal, final int mrgVertical,
                           final boolean _rapide, final boolean _changeZ) {
    if (_boite == null) {
      return;
    }
    if (!checkBoite(_boite, true, true)) {
      return;
    }

    final Insets insets = getInsets();
    final double wd = _boite.getDeltaX();
    final double hd = _boite.getDeltaY();
    int margeGauche = mrgHorizontal;
    int margeDroite = mrgHorizontal;
    int margeHaut = mrgVertical;
    int margeBas = mrgVertical;

    if (insets_ != null) {

      margeGauche = insets_.left;
      margeDroite = insets_.right;
      margeHaut = insets_.top;
      margeBas = insets_.bottom;
    }

    final double wv = getWidth() - insets.left - insets.right - margeGauche - margeDroite;
    final double hv = getHeight() - insets.top - insets.bottom - margeHaut - margeBas;
    final RepereEvent repere = new RepereEvent(_source, _rapide);
    final double e = Math.max(wd / wv, hd / hv);
    repere.ajouteTransformation(RepereEvent.ZOOM, e, false);
    margeXCentre = (wv - wd / e) / 2;
    repere.ajouteTransformation(RepereEvent.TRANS_X, _boite.o_.x_ / e - margeXCentre - margeGauche, false);
    margeYCentre = (hv - hd / e) / 2;
    repere.ajouteTransformation(RepereEvent.TRANS_Y, _boite.o_.y_ / e - margeYCentre - margeBas, false);
    repere.ajouteTransformation(RepereEvent.TRANS_Z, 0., false);
    repere.ajouteTransformation(RepereEvent.ROT_X, 0., false);
    repere.ajouteTransformation(RepereEvent.ROT_Y, 0., false);
    if (_changeZ) {
      repere.ajouteTransformation(RepereEvent.ROT_Z, _rz, false);
    }
    repereModifie(repere);
  }

  /**
   * Effectue un zoom �tendu aux dimensions de la boite, avec une marge sp�cifi�e.
   */
  public void changeRepere(final Object _source, final GrBoite _boite, final int _mrg) {
    changeRepere(_source, _boite, 0, _mrg);
  }

  public void changeViewBoite(final Object _source, final GrBoite _boite, final boolean _rapide) {
    changeRepere(_source, _boite, 0, 0, 0, _rapide, false);
  }

  public boolean checkTranslation(final GrVecteur _vReel) {
    // on va tester que la nouvelle vue n'est pas en dehors
    final GrBoite visuBoite = getViewBoite();
    final GrBoite domaine = getCalque().getDomaine();
    if (_vReel.x_ > 0 && visuBoite.getMidX() + _vReel.x_ > domaine.getMaxX()) {
      _vReel.x_ = 0;
    }
    if (_vReel.x_ < 0 && visuBoite.getMidX() + _vReel.x_ < domaine.getMinX()) {
      _vReel.x_ = 0;
    }
    if (_vReel.y_ > 0 && visuBoite.getMidY() + _vReel.y_ > domaine.getMaxY()) {
      _vReel.y_ = 0;
    }
    if (_vReel.y_ < 0 && visuBoite.getMidY() + _vReel.y_ < domaine.getMinY()) {
      _vReel.y_ = 0;
    }
    return _vReel.norme() > 0;
  }

  /**
   * Reorganisation du layout.
   */
  @Override
  public void doLayout() {
    final BCalque[] c = getCalque().getTousCalques();
    for (int i = 0; i < c.length; i++) {
      if (Boolean.TRUE.equals(c[i].getClientProperty(BCalqueInteraction.NO_MOUSE_LISTENER))) {
        continue;
      }
      if (c[i] instanceof MouseListener) {
        final MouseListener listener = (MouseListener) c[i];
        removeMouseListener(listener);
        addMouseListener(listener);
        c[i].removeMouseListener(listener);
      }
      if (c[i] instanceof MouseMotionListener) {
        final MouseMotionListener listener = (MouseMotionListener) c[i];
        removeMouseMotionListener(listener);
        addMouseMotionListener(listener);
        c[i].removeMouseMotionListener(listener);
      }
    }
    super.doLayout();
  }

  @Override
  public AbstractCalque getAbstractCalque() {
    return calque_;
  }

  /**
   * Accesseur de la propriete <I>calque</I>. Elle est le calque (racine de la hierarchie des calques) affiche par la vue.
   */
  public BCalque getCalque() {
    return calque_;
  }

  public Image getImageCache() {
    return cache_;
  }

  public final BCalqueContextuelListener getListener() {
    return listener_;
  }

  /**
   * Accesseur de la propriete <I>repere</I>.
   */
  public double[][] getRepere() {
    return repere_;
  }

  @Override
  public RepereMouseKeyController getRepereController() {
    return repereController_;
  }

  public BuTaskView getTaskView() {
    return taskView_;
  }

  /**
   * Renvoie le morphisme versEcran de la propriete <code>calque</code>. equivaut a <code>getCalque().getVersEcran();</code>.
   */
  @Override
  public GrMorphisme getVersEcran() {
    return getCalque().getVersEcran();
  }

  @Override
  public GrMorphisme getVersReel() {
    return getCalque().getVersReel();
  }

  /**
   * Retourne la boite en espace reel correspondant � la vue. Si Normalement, this.changeRepere(this,this.getBoite(),0) ne doit rien faire.
   */
  @Override
  public GrBoite getViewBoite() {
    GrBoite b = getGraphicBoite();
    b.autoApplique(getCalque().getVersReel());
    return b;
  }

  protected GrBoite getGraphicBoite() {
    double minX = 0;
    double minY = 0;
    double maxX = getWidth();
    double maxY = getHeight();
    if (insets_ != null) {
      minX = insets_.left;
      minY = insets_.top;
      maxX = maxX - insets_.right;
      maxY = maxY - insets_.bottom;
    }
    final GrBoite b = new GrBoite(new GrPoint(minX, minY, 0), new GrPoint(maxX, maxY, 0));
    return b;
  }

  public GrBoite getViewBoiteOnDomaine() {
    return getViewBoite().intersection(getCalque().getDomaine());
  }

  @Override
  public GrPolygone getViewPolygone() {
    final GrPolygone b = new GrBoite(new GrPoint(0, 0, 0), new GrPoint(getWidth(), getHeight(), 0)).enPolygoneXY();
    b.autoApplique(getCalque().getVersReel());
    return b;
  }

  public final boolean isNoRepaintTask() {
    return noRepaintTask_;
  }

  @Override
  public final boolean isValidateRoot() {
    return true;
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {
    requestFocus();
  }

  // Mouse
  // MouseMotion
  @Override
  public void mouseEntered(final MouseEvent _evt) {
    // TODO BM A Voir si c'est bien malin de faire ca.
    //non car bizarre
//    grabFocus();
  }

  @Override
  public void mouseExited(final MouseEvent _evt) {
  }

  @Override
  public void mousePressed(final MouseEvent _evt) {
    requestFocus();
    // requestFocusInWindow();
    isPopup(_evt);
  }

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    isPopup(_evt);
    requestFocus();
  }

  /**
   * Paint. Cette methode affiche une image cache du calque en general, sauf quand un "full repaint" est demande (changement du repere...) ou quand la
   * vue est agrandie.
   */
  @Override
  public void paint(final Graphics _g) {
    Rectangle clip = _g.getClipBounds();
    final Dimension size = getSize();
    if (clip == null) {
      clip = new Rectangle(0, 0, size.width, size.height);
    }
    _g.setClip(clip);
    if (_g instanceof PrintGraphics || _g instanceof PrinterGraphics) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("EBL: _g is a PrintGraphics/PrinterGraphics");
      }
      super.paint(_g);
    } else if (noRepaintTask_ || ajustement_) {
      cache_ = null;
      if (isOpaque()) {
        _g.setColor(getBackground());
        _g.fillRect(clip.x, clip.y, clip.width, clip.height);
      }
      super.paint(_g);
    } else if ((cache_ == null) && isShowing()) {
      Graphics g = null;
      try {
        cache_ = createImage(size.width, size.height);
        g = cache_.getGraphics();
        g.setClip(0, 0, size.width, size.height);
      } catch (final NullPointerException ex) {
        cache_ = null;
        g = _g;
      }
      if (isOpaque()) {
        g.setColor(getBackground());
        // bien remplir tout le composant sinon pas beau !
        g.fillRect(0, 0, size.width, size.height);
      }
      super.paint(g);
      if (cache_ != null) {
        _g.drawImage(cache_, 0, 0, null);
      }
    } else {
      _g.drawImage(cache_, 0, 0, null);
    }
  }

  @Override
  public void paintAllInImage(final Graphics _g, final GrMorphisme _versEcran, final GrMorphisme _versReel) {
    final GrBoite clipReel = getCalque().getClipEcran(_g).applique(_versReel);
    getCalque().paintAllInImage((Graphics2D) _g, _versEcran, _versReel, clipReel);
  }

  public void paintImage(final Graphics _g, final Map _params) {
    if (CtuluLibImage.mustFillBackground(_params)) {
      _g.setColor(getBackground());
      _g.fillRect(0, 0, getWidth(), getHeight());
    }
    final BCalque[] c = getCalque().getCalques();
    for (int i = c.length - 1; i >= 0; i--) {
      if (c[i].isVisible()) {
        c[i].paintImage(_g);
      }
    }
  }

  /**
   * @param _x le x du popup menu
   * @param _y le y du popup menu
   */
  public void popMenu(final int _x, final int _y) {
    if (listener_ == null) {
      return;
    }
    final JPopupMenu menu = listener_.getCmdsContextuelles();
    if (menu == null) {
      return;
    }
    menu.pack();
    menu.putClientProperty(POPUP_INIT_SCREEN_LOCATION, new GrPoint((double) _x, (double) _y, 0));
    menu.show(this, _x, _y);
  }

  /**
   * Lance le dessin en tache de fond.
   */
  @Override
  public void repaint() {
    cache_ = null;
    super.repaint();
  }

  @Override
  public void repaint(final long _tm) {
    cache_ = null;
    super.repaint(_tm);
  }

  /**
   * Reception de l'evenement <I>RepereEvent</I>. Si l'evenement est un ajustement, alors <I>calque.setRapide(true)</I> est appelee. Les
   * transformations <I>versEcran</I> et <I>versReel</I> du calque sont mises a jour.
   *
   * @param _evt l'evenement repere
   */
  @Override
  public void repereModifie(final RepereEvent _evt) {
    if (lastRepere_ != null && !ajustement_) {
      CtuluLibArray.copyIn(repere_, lastRepere_);
    }
    ajustement_ = _evt.ajustement();
    getCalque().setRapide(ajustement_);
    if (ajusteRepere(_evt)) {
      changeMorphismes();
      firePropertyChange("repereEvt", null, _evt);
      firePropertyChange("repere", lastRepere_, repere_);
      if (lastRepere_ == null) {
        lastRepere_ = CtuluLibArray.copy(repere_);
      }
    }
  }

  /**
   * deprecated but used by Swing !!!
   */
  @Override
  public void reshape(final int _x, final int _y, final int _width, final int _height) {
    final Rectangle r = new Rectangle(_x, _y, _width, _height);
    Rectangle bounds = getBounds();
    if (!r.equals(bounds)) {
      super.reshape(_x, _y, _width, _height);
      changeMorphismes();
    }
  }

  // Resize

  /**
   * Changement de la taille de la vue.
   */
  @Override
  public void setBounds(final Rectangle _rect) {
    if (!_rect.equals(getBounds())) {
      super.setBounds(_rect);
      changeMorphismes();
    }
  }

  public final void setDefaultCursor() {
    setCursor(getDefaultCursor());
  }

  public final void setListener(final BCalqueContextuelListener _listener) {
    listener_ = _listener;
  }

  public final void setNoRepaintTask(final boolean _noRepaintTask) {
    noRepaintTask_ = _noRepaintTask;
  }

  @Override
  public void setRapide(final boolean _b) {
    ajustement_ = _b;
    getCalque().setRapide(_b);
  }

  /**
   * Affectation de la propriete <I>repere</I>.
   */
  public void setRepere(final double[][] _v) {
    if (_v != null && repere_ != _v) {
      if (lastRepere_ != null) {
        CtuluLibArray.copyIn(repere_, lastRepere_);
      }
      final double[][] old = repere_;
      repere_ = _v;
      changeMorphismes();
      firePropertyChange("repere", old, repere_);
    }
  }

  // public void mouseUp(MouseEvent _evt) {}
  public void setTaskView(final BuTaskView _taskView) {
    taskView_ = _taskView;
  }

  public void setUserInsets(final Insets _insets) {
    if (CtuluLib.isEquals(insets_, _insets)) {
      return;
    }
    GrBoite viewBoite = getViewBoite();
    Insets old = insets_;
    insets_ = _insets;
    changeRepere(this, viewBoite, 0, 0);
    firePropertyChange("insets", old, insets_);
  }

  public Insets getUserInsets() {
    return (Insets) (insets_ == null ? null : insets_.clone());
  }

  @Override
  public void translateXLeft(final boolean _largeTranslate, final boolean _rapide) {
    translateX(-getTranslateStep(_largeTranslate), _rapide);
  }

  @Override
  public void translateXRight(final boolean _large, final boolean _rapide) {
    translateX(getTranslateStep(_large), _rapide);
  }

  @Override
  public void translateYDown(final boolean _largeTranslate, final boolean _rapide) {
    translateY(-getTranslateStep(_largeTranslate), _rapide);
  }

  @Override
  public void translateYUp(final boolean _largeTranslate, final boolean _rapide) {
    translateY(getTranslateStep(_largeTranslate), _rapide);
  }

  boolean checkTtranslation = true;

  public boolean isCheckTtranslation() {
    return checkTtranslation;
  }

  public void setCheckTtranslation(boolean checkTtranslation) {
    this.checkTtranslation = checkTtranslation;
  }

  @Override
  public void translation(final double _realDx, final double _realDy, final boolean _rapide) {
    vect_.x_ = _realDx;
    vect_.y_ = _realDy;
    if (checkTtranslation && !checkTranslation(vect_)) {
      return;
    }
    vect_.autoApplique(getVersEcran());
    final RepereEvent re = new RepereEvent(this, _rapide);
    re.ajouteTransformation(RepereEvent.TRANS_X, vect_.x_, true);
    re.ajouteTransformation(RepereEvent.TRANS_Y, -vect_.y_, true);
    repereModifie(re);
  }

  public void undoRepere() {
    if (lastRepere_ != null && !CtuluLibArray.equals(repere_, lastRepere_)) {
      final double[][] tmp = repere_;
      repere_ = lastRepere_;
      lastRepere_ = tmp;
      changeMorphismes();
      firePropertyChange("repere", lastRepere_, repere_);
    }
  }

  public void zoom(final boolean _out) {
    final GrBoite bReel = getViewBoite();
    zoomOnRealPoint(bReel.getMidX(), bReel.getMidY(), _out);
  }

  @Override
  public void zoomIn() {
    zoom(false);
  }

  public void zoomOnMouse(final MouseEvent _evt) {
    zoomOnMouse(_evt, _evt.isShiftDown());
  }

  @Override
  public void zoomOnMouse(final MouseEvent _evt, final boolean _isOut) {
    final GrBoite graphicBoite = getGraphicBoite();
    double coef;
    if (_isOut) {
      coef = 1 / getZoomCoef();
    } else {
      coef = getZoomCoef();
    }
    final GrPoint p = new GrPoint(_evt.getPoint());
    p.autoApplique(getVersReel());
    GrBoite bReel = getBoiteZoomedOnRealPoint(_isOut, p.x_, p.y_);
    double dx = _evt.getX() - graphicBoite.getMidX();
    double dy = _evt.getY() - graphicBoite.getMidY();
    dx = GrMorphisme.convertDistanceXY(getVersReel(), dx) * coef;
    dy = GrMorphisme.convertDistanceXY(getVersReel(), dy) * coef;
    bReel.e_.x_ += dx;
    bReel.o_.x_ += dx;
    bReel.e_.y_ += dy;
    bReel.o_.y_ += dy;
    setViewBoite(bReel);
  }

  @Override
  public void zoomOnRealPoint(final double _x, final double _y, final boolean _out) {
    GrBoite bReel = getBoiteZoomedOnRealPoint(_out, _x, _y);
    setViewBoite(bReel);
  }

  protected GrBoite getBoiteZoomedOnRealPoint(final boolean _out, final double _x, final double _y) {
    double coef;
    if (_out) {
      coef = 1 / getZoomCoef();
    } else {
      coef = getZoomCoef();
    }
//    coef = coef / 2;
    final GrBoite bReel = getViewBoite();
    final double w = bReel.getDeltaX() / 2;
    final double h = bReel.getDeltaY() / 2;
    final double wDelta = w * coef;
    final double hDelta = h * coef;
    bReel.o_.x_ = _x - wDelta;
    bReel.o_.y_ = _y - hDelta;
    bReel.e_.x_ = _x + wDelta;
    bReel.e_.y_ = _y + hDelta;
    return bReel;
  }

  @Override
  public void zoomOut() {
    zoom(true);
  }

  public Cursor getDefaultCursor() {
    return new Cursor(Cursor.CROSSHAIR_CURSOR);
  }

  public Insets getCalqueInsets() {
    return insets_;
  }

  public boolean isCheckBoite() {
    return checkBoite;
  }

  public void setCheckBoite(boolean checkBoite) {
    this.checkBoite = checkBoite;
  }
}
