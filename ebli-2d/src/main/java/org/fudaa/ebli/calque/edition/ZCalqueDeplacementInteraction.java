/*
 *  @creation     4 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:33 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque.edition;

import com.memoire.bu.BuResource;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.calque.BCalqueInteraction;
import org.fudaa.ebli.calque.ZCalqueSelectionInteractionAbstract;
import org.fudaa.ebli.calque.ZCatchEvent;
import org.fudaa.ebli.calque.ZCatchListener;
import org.fudaa.ebli.calque.ZSceneSelection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceIcon;

/**
 * Un calque d'interaction d�placement.
 *
 * @author Fred Deniger, Bertrand Marchand
 * @version $Id$
 */
public class ZCalqueDeplacementInteraction extends BCalqueInteraction implements MouseListener, MouseMotionListener, ZCatchListener {

  /**
   * @author fred deniger
   * @version $Id$
   */
  public interface SpecPanel {

    void updateState();
  }
  SpecPanel specPanel_;
  TraceIcon ic_ = new TraceIcon(TraceIcon.CARRE, 2, Color.LIGHT_GRAY);
  private boolean isDragged_;
  ZCalqueDeplacementTargetInterface target_;
  /**
   * Le point d'accrochage, pass� par le calque d'accrochage
   */
  GrPoint ptAccroch_ = null;
  /**
   * La g�om�trie d'accrochage, pour le d�but du d�placement
   */
  protected Integer idxGeomAccroch_;
  /**
   * Le sommet d'accrochage, pour le d�but du d�placement
   */
  protected Integer idxVertexAccroch_;
  /**
   * Le point d'origine
   */
  GrPoint ptOrig_ = null;
  /**
   * Vrai si le point d'origine est accroch�
   */
  boolean baccrOrig_ = false;
  /**
   * Vrai si le point courant est accroch�
   */
  boolean baccrCurrent_ = false;
  /**
   * Le vecteur de deplacement, en coordonn�es reelles.
   */
  GrVecteur vect_;

  public final SpecPanel getSpecPanel() {
    return specPanel_;
  }
  Cursor cursor;

  public void setSpecificCursor(Cursor cursor) {
    this.cursor = cursor;
  }

  @Override
  public Cursor getSpecificCursor() {
    if (cursor == null) {
      cursor = EbliResource.EBLI.getCursor("fleche-deplacement", -1, new Point(0, 0));
    }
    return cursor;
  }

  @Override
  public String getDescription() {
    return BuResource.BU.getString("D�placer");
  }

  public final void setSpecPanel(final SpecPanel _specPanel) {
    specPanel_ = _specPanel;
  }

  public double getReelDx() {
    return vect_.x_;

  }

  public double getReelDy() {
    return vect_.y_;
  }

  public double getReelDz() {
    if (baccrCurrent_ && baccrOrig_) {
      return vect_.z_;
    } else {
      return 0;
    }
  }

  /**
   * @return true si en cours de deplacement
   */
  public final boolean isDragged() {
    return isDragged_;
  }

  /**
   * @return true si selection vide
   */
  public final boolean isSelectionEmpty() {
    return target_ == null || target_.getSupport() == null || target_.getSupport().isSelectionEmpty();
  }

  /**
   * @param _select le calque de selection
   */
  public ZCalqueDeplacementInteraction(final ZCalqueSelectionInteractionAbstract _select) {
    super();
    selection_ = _select;
    setName("cqDeplacement");
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  @Override
  public boolean alwaysPaint() {
    return true;
  }

  public final ZCalqueDeplacementTargetInterface getTarget() {
    return target_;
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
    if (isGele()) {
      return;
    }
    if (_e.getClickCount() >= 2) {
      selection_.editionAsked();
    }
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (isGele()) {
      isDragged_ = false;
      return;
    }
    if (_e.isControlDown() || _e.isAltDown()) {
      return;
    }

    baccrCurrent_ = ptAccroch_ != null;
    vect_ = getCurrentPoint(_e).soustraction(ptOrig_);

    if (!isDragged_ && vect_.applique(getVersEcran()).norme() > 10) {
      isDragged_ = true;
    }
    updateSpecPanel();
    repaint();
  }

  private void updateSpecPanel() {
    if (specPanel_ != null) {
      specPanel_.updateState();
    }
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
    if (isGele()) {
      isDragged_ = false;
      return;
    }
    isDragged_ = false;
    repaint();
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
    if (isGele()) {
      isDragged_ = false;
      return;
    }
    isDragged_ = false;
    repaint();
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {
  }
  private boolean changeSelectionOnMousePressed = true;

  public boolean isChangeSelectionOnMousePressed() {
    return changeSelectionOnMousePressed;
  }

  public void setChangeSelectionOnMousePressed(boolean changeSelectionOnMousePressed) {
    this.changeSelectionOnMousePressed = changeSelectionOnMousePressed;
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (!SwingUtilities.isLeftMouseButton(_e) || EbliLib.isPopupMouseEvent(_e)) {
      return;
    }
    if (isGele()) {
      isDragged_ = false;
      return;
    }
    if (state_ == null) {
      state_ = new EbliSelectionState();
    }
    state_.majControleDesc(_e);

    baccrOrig_ = ptAccroch_ != null;
    ptOrig_ = getCurrentPoint(_e);

    // Accrochage d'un sommet/g�om�trie.
    if (changeSelectionOnMousePressed) {
      if (idxGeomAccroch_ != null) {
        ZSceneSelection ssel = new ZSceneSelection(target_.getSupport());
        CtuluListSelection sel = new CtuluListSelection(new int[]{idxVertexAccroch_});
        ssel.set(idxGeomAccroch_, sel);
        target_.getSupport().changeSelection(ssel, state_.getModificateur());
      } // Pas d'accrochage. On essaye de selectionner.
      else {
        target_.getSupport().changeSelection(ptOrig_, 5, state_.getModificateur());
      }
    }
    updateSpecPanel();
    repaint();
  }
  EbliSelectionState state_;

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (!SwingUtilities.isLeftMouseButton(_e) || EbliLib.isPopupMouseEvent(_e)) {
      return;
    }
    if (isGele()) {
      isDragged_ = false;
      return;
    } else if (isDragged_ && target_ != null && !_e.isControlDown() && !_e.isAltDown()) {

      baccrCurrent_ = ptAccroch_ != null;
      vect_ = getCurrentPoint(_e).soustraction(ptOrig_);

      target_.moved(vect_.x_, vect_.y_, baccrOrig_ && baccrCurrent_ ? vect_.z_ : 0, true);
      isDragged_ = false;
    }
    updateSpecPanel();
    repaint();
  }

  /**
   * Retourne le point courant, qui peut �tre un point d'accrochage.
   *
   * @param _e L'evenement souris
   * @return Le point courant.
   */
  private GrPoint getCurrentPoint(MouseEvent _e) {
    GrPoint pt;
    if (ptAccroch_ != null) {
      pt = ptAccroch_;
    } else {
      pt = new GrPoint(_e.getX(), _e.getY(), 0);
      pt.autoApplique(getVersReel());
    }
    return pt;
  }
  ZCalqueSelectionInteractionAbstract selection_;

  @Override
  public void paintComponent(final Graphics _g) {
    if (isGele() || target_ == null || target_.getSupport() == null) {
      return;
    }
    if (selection_ != null && !target_.getSupport().isSelectionEmpty()) {
      target_.getSupport()
              .paintSelection((Graphics2D) _g, selection_.getTrace(), getVersEcran(), getClipReel(_g));
    }
    if (target_ != null && isDragged_) {
      GrVecteur vct = vect_.applique(getVersEcran());
      target_.paintMove((Graphics2D) _g, (int) vct.x_, (int) vct.y_, ic_);
    }

  }

  public final void setTarget(final ZCalqueDeplacementTargetInterface _target) {
    if (target_ != _target) {
      target_ = _target;
      isDragged_ = false;
    }
  }

  @Override
  public void catchChanged(ZCatchEvent _evt) {
    if (_evt.type == ZCatchEvent.CAUGHT) {
      ptAccroch_ = _evt.selection.getScene().getVertex(_evt.idxGeom, _evt.idxVertex);
      idxGeomAccroch_ = _evt.idxGeom;
      idxVertexAccroch_ = _evt.idxVertex;
    } else {
      ptAccroch_ = null;
      idxGeomAccroch_ = null;
      idxVertexAccroch_ = null;
    }
  }

  @Override
  public boolean isCachingEnabled() {
    return !isGele();
  }
}
