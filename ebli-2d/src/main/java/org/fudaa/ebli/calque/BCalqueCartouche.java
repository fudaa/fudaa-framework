/*
 * @file         BCalqueCartouche.java
 * @creation     1998-08-31
 * @modification $Date: 2006-11-14 09:06:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.calque;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInformationsDocument;
import java.awt.*;
import java.awt.image.FilteredImageSource;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.fudaa.ebli.trace.FiltreAttenuation;

/**
 * Un calque d'affichage de cartouche. Informations sur le document.
 * 
 * @version $Revision: 1.9 $ $Date: 2006-11-14 09:06:26 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BCalqueCartouche extends BCalqueAffichage {
  private Dimension taille_;

  public BCalqueCartouche() {
    super();
    taille_ = null;
    setDestructible(false);
  }

  // Icon
  /**
   * Dessin de l'icone.
   * 
   * @param _c composant dont l'icone peut deriver des proprietes (couleur, ...). Ce parametre peut etre <I>null</I>.
   *          Il est ignore ici.
   * @param _g le graphics sur lequel dessiner l'icone
   * @param _x lieu cible de l'icone (x)
   * @param _y lieu cible de l'icone (y)
   */
  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    super.paintIcon(_c, _g, _x, _y);
    final boolean attenue = isAttenue();
    Color fg = getForeground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final int w = getIconWidth();
    final int h = getIconHeight();
    _g.setColor(fg);
    _g.drawRect(_x + w - 12, _y + h - 8, 10, 6);
    _g.setColor(bg);
    _g.fillRect(_x + w - 11, _y + h - 7, 8, 4);
  }

  // Paint
  @Override
  public void paintComponent(final Graphics _g) {
    final boolean rapide = isRapide();
    final boolean attenue = isAttenue();
    Color fg = getForeground();
    if (attenue) {
      fg = attenueCouleur(fg);
    }
    Color bg = getBackground();
    if (attenue) {
      bg = attenueCouleur(bg);
    }
    final Font font = getFont();
    if (font != null) {
      _g.setFont(font);
    }
    final FontMetrics fm = _g.getFontMetrics();
    final Dimension d = getSize();
    final Insets insets = getInsets();
    final BuIcon icon = informations_.logo;
    int wi = 0;
    int hi = 0;
    if (icon != null) {
      wi = icon.getIconWidth();
      hi = icon.getIconHeight();
    }
    if (rapide) {
      int x = d.width - insets.right - 10;
      int y = d.height - insets.bottom - 10;
      if (taille_ == null) {
        _g.setColor(fg);
        _g.drawLine(x - 5, y, x, y);
        _g.drawLine(x, y - 5, x, y);
      } else {
        x -= taille_.width;
        y -= taille_.height;
        _g.setColor(bg);
        _g.fillRect(x, y, taille_.width, taille_.height);
        _g.setColor(fg);
        _g.drawRect(x, y, taille_.width, taille_.height);
        _g.setColor(bg);
        _g.fillRect(x - 5 - wi, y, wi, hi);
        _g.setColor(fg);
        _g.drawRect(x - 5 - wi, y, wi, hi);
      }
    } else {
      int i;
      final String[] l = new String[] { informations_.name + ' ' + informations_.version + ' ' + informations_.date,
          informations_.organization, informations_.author, informations_.contact };
      final int hu = fm.getAscent() + fm.getDescent();
      int wu = 0;
      for (i = 0; i < l.length; i++) {
        if (wu < fm.stringWidth(l[i])) {
          wu = fm.stringWidth(l[i]);
        }
      }
      int hs = 2 + hu * l.length;
      final int ws = 2 + wu;
      if (hs + 5 < hi) {
        hs = hi - 5;
      }
      final int x = d.width - insets.right - 10 - ws;
      final int y = d.height - insets.bottom - 10 - hs;
      _g.setColor(bg);
      _g.fillRect(x, y, ws + 4, hs + 4);
      _g.setColor(fg);
      for (i = 0; i < l.length; i++) {
        _g.drawString(l[i], x + 3, y + 3 + hu * i + fm.getAscent());
      }
      _g.drawRect(x, y, ws + 4, hs + 4);
      if (icon != null) {
        if (attenue) {
          final Icon ia = new ImageIcon(createImage(new FilteredImageSource(icon.getImage().getSource(),
              FiltreAttenuation.INSTANCE)));
          ia.paintIcon(this, _g, x - wi - 5, y);
        } else {
          icon.paintIcon(this, _g, x - wi - 5, y);
        }
      }
      taille_ = new Dimension(ws + 4, hs + 4);
    }
    super.paintComponent(_g);
  }
  // Proprietes
  private BuInformationsDocument informations_;

  /**
   * Affectation de la propriete <I>informations</I>.
   */
  public void setInformations(final BuInformationsDocument _informations) {
    if (informations_ != _informations) {
      final BuInformationsDocument vp = informations_;
      informations_ = _informations;
      firePropertyChange("informations", vp, informations_);
      repaint();
    }
  }
}
