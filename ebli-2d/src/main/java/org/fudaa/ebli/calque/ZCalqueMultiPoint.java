/*
 *  @creation     31 mars 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.calque;

import java.awt.Color;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 * Un calque g�rant des multipoints, � un niveau global ou atomique.<p>
 * Le niveau atomique des multipoints est le niveau points. Le calque s'appuie sur un mod�le
 * {@link ZModeleMultiPoint}.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ZCalqueMultiPoint extends ZCalqueGeometry {

  /**
   * Le seul constructeur, avec le mod�le.
   * @param _modele le modele du calque
   */
  public ZCalqueMultiPoint(final ZModeleMultiPoint _modele) {
    super(_modele);
    iconModel_ = new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.BLACK);
  }

  /**
   * @param _modele Modele
   */
  public void modele(final ZModeleMultiPoint _modele) {
    if (modele_ != _modele) {
      if(modele_!=null)
        modele_.removeModelListener(this);
      final ZModeleMultiPoint vp = (ZModeleMultiPoint)modele_;
      modele_ = _modele;
      if(modele_!=null)
        modele_.addModelListener(this);
      firePropertyChange("modele", vp, modele_);
    }
  }

  @Override
  public ZModeleMultiPoint modeleDonnees() {
    return (ZModeleMultiPoint)modele_;
  }
}
