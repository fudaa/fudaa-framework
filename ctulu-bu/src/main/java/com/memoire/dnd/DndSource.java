/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         DndSource.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class DndSource implements DndConstants, Serializable, MouseListener, MouseMotionListener, ActionListener {
  // private static final int WI=26+12;
  // private static final int HI=26+12;

  private DndRequestData request_;
  private int mask_;

  private int counter_;
  private Cursor cursor_;
  private boolean dragging_;
  private DndTarget target_;
  private JComponent over_;
  private Point loc_;
  private Object[] values_;

  public DndSource(DndRequestData _request) {
    this(_request, COPY | MOVE);
  }

  public DndSource(DndRequestData _request, int _mask) {
    mask_ = _mask;
    request_ = _request;

    dragging_ = false;
    target_ = null;
    over_ = null;
    loc_ = null;
    values_ = null;
  }

  public void add(JComponent _comp) {
    if (_comp.getClientProperty("DND_SOURCE") == null) {
      _comp.addMouseListener(this);
      _comp.addMouseMotionListener(this);
      _comp.putClientProperty("DND_SOURCE", this);
    } else
      throw new RuntimeException("Already a Dnd Source");
  }

  public static void remove(JComponent _comp) {
    DndSource s = (DndSource) _comp.getClientProperty("DND_SOURCE");
    if (s != null) {
      _comp.removeMouseListener(s);
      _comp.removeMouseMotionListener(s);
      _comp.putClientProperty("DND_SOURCE", null);
    }
  }

  @Override
  public void mouseClicked(MouseEvent _evt) {}

  @Override
  public void mouseEntered(MouseEvent _evt) {
    counter_ = 0;
  }

  @Override
  public void mouseExited(MouseEvent _evt) {
    counter_ = 0;
  }

  @Override
  public void mousePressed(MouseEvent _evt) {
    counter_ = 0;
  }

  @Override
  public void mouseReleased(MouseEvent _evt) {
    if (over_ instanceof DndSensible) ((DndSensible) over_).exitedDrop();

    if (dragging_) {
      dragging_ = false;

      JComponent s = (JComponent) _evt.getSource();
      s.setCursor(cursor_);
      s.putClientProperty("DND_EVENTS", null);

      if (s instanceof DndSensible) ((DndSensible) s).exitedDrop();

      if ((over_ != null) && (target_ != null) && (values_ != null) && (loc_ != null)) target_.drop(values_, over_,
      /* SwingUtilities. */convertPoint(s, _evt.getX(), _evt.getY(), over_), getMode());

      Component top = getTopLevelAncestor(s, _evt.getX(), _evt.getY());
      if (top instanceof RootPaneContainer) top = ((RootPaneContainer) top).getContentPane();
      if (top instanceof JComponent) ((JComponent) top).paintImmediately(ox - 1, oy - 1, ow + 1, oh + 1);
    }

    over_ = null;
    target_ = null;
    values_ = null;
    loc_ = null;
    windows_ = null;
    timer_.stop();
    // _evt.consume();

    /*
     * if(top instanceof BuApplication) { BuApplication a=(BuApplication)top; a.setGlassPane(glass_); }
     */
  }

  @Override
  public void mouseMoved(MouseEvent _evt) {}

  private int ox, oy, ow, oh;
  private Icon icon_;
  private Component oldtop_;
  private Point oldp_;
  private boolean oldtip_;
  private javax.swing.Timer timer_ = new javax.swing.Timer(1500, this);

  // private static Icon noDropIcon =new ImageIcon("dnd-nodrop_24.gif");
  // private static Icon canDropIcon=new ImageIcon("dnd-candrop_24.gif");

  // private Component glass_=null;

  @Override
  public void mouseDragged(MouseEvent _evt) {
    if (!dragging_
        && ((_evt.getModifiers() & ~(InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK)) != InputEvent.BUTTON1_MASK)) {
      return;
    }

    counter_++;
    if (counter_ < 3) return;
    updateMode(_evt);

    /*
     * JComponent s=(JComponent)_evt.getSource(); if(counter_==5) { Component top=s.getTopLevelAncestor(); if(top
     * instanceof BuApplication) { BuApplication a=(BuApplication)top; System.err.println("PUT DND GLASS");
     * glass_=a.getGlassPane(); BuGlassPaneStop gp=new BuGlassPaneStop(a); a.setGlassPane(gp); gp.setVisible(true);
     * gp.revalidate(); } }
     */

    JComponent s = (JComponent) _evt.getSource();
    int x = _evt.getX();
    int y = _evt.getY();

    if (!dragging_) {
      Object[] v = request_.request(s, new Point(x, y));
      // System.err.println("VALUES: "+v.length);
      if ((v == null) || (v.length == 0)) {
        counter_ = 0;
        return;
      }
      // System.err.println("VALUE[0]: "+v[0]);

      dragging_ = true;
      cursor_ = s.getCursor();
      s.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
      values_ = v;
      ow = 0;
      oh = 0;
      if (DndIcons.isIconDisplayed()) icon_ = DndIcons.getIcon(values_);
      // ow=icon_.getIconWitdh ();
      // oh=icon_.getIconHeight();
      // if(values_.length>0)
      // System.err.println("VALUE="+values_[0]);
      /*
       * for(int i=0;i<values_.length;i++) System.err.println("VALUE["+i+"]="+values_[i]);
       */

      timer_.start();
      s.putClientProperty("DND_EVENTS", this);
    }

    Component top = getTopLevelAncestor(s, x, y);
    if (top instanceof RootPaneContainer) top = ((RootPaneContainer) top).getContentPane();

    Point p = convertPoint(s, x, y, top);
    display(top, p, false);
    if (top != null) {
      oldtop_ = top;
      oldp_ = p;
    }

    _evt.consume();
  }

  boolean ctrl_;
  boolean shift_;

  public void updateMode(MouseEvent _evt) {
    ctrl_ = ((_evt.getModifiers() & InputEvent.CTRL_MASK) != 0);
    shift_ = ((_evt.getModifiers() & InputEvent.SHIFT_MASK) != 0);
  }

  public int getMode() {
    int r = mask_;
    if (ctrl_) r = MOVE;
    else if (shift_) r = COPY;
    else if ((mask_ & MOVE) != 0) r = MOVE;
    else if ((mask_ & COPY) != 0) r = COPY;
    // else if((mask_&LINK)!=0) r=LINK;

    if (target_ != null) r &= target_.getMode();
    r &= mask_;
    return r;
  }

  public Icon getModeIcon() {
    Icon r;
    int m = getMode();
    if (m == mask_) m = NONE;
    switch (m) {
    case COPY:
      r = DndIcons.getCopyIcon();
      break;
    case MOVE:
      r = DndIcons.getMoveIcon();
      break;
    // case LINK: r=DndIcons.getLinkIcon(); break;
    default:
      r = null;
      break;
    }
    return r;
  }

  private Window[] windows_ ;

  private Component getTopLevelAncestor(JComponent s, int x, int y) {
    // return s.getTopLevelAncestor();

    Component r = s.getTopLevelAncestor();
    Point p = SwingUtilities.convertPoint(s, x, y, r);
    Rectangle b = r.getBounds();
    b.x = b.y = 0;

    if (b.contains(p)) {
      if (r instanceof Frame) ((Frame) r).toFront();
      // System.err.println("TOP="+r.getClass());
      return r;
    }

    p = s.getLocationOnScreen();
    p.x += x;
    p.y += y;

    if (windows_ == null) windows_ = DndLib.getAllWindows();

    if (windows_ != null) for (int i = 0; i < windows_.length; i++)
      if (windows_[i].isShowing()) {
        Point q = windows_[i].getLocationOnScreen();
        b = windows_[i].getBounds();
        b.x = q.x;
        b.y = q.y;
        if (b.contains(p)) {
          windows_[i].toFront();
          return windows_[i];
        }
      }

    return null;
  }

  private Point convertPoint(JComponent s, int x, int y, Component f) {
    // return SwingUtilities.convertPoint(s,x,y,top);

    if ((f == null) || !f.isShowing()) return null;

    Point p = s.getLocationOnScreen();
    Point q = f.getLocationOnScreen();
    p.x += x - q.x;
    p.y += y - q.y;
    return p;
  }

  private void display(Component top, Point p, boolean tip) {
    if (!dragging_) return;
    if (tip && oldtip_) return;
    oldtip_ = tip;

    /*
     * if((top==null)||(p==null)) { String tmp="DndSource: P="+p+" T="+top;
     * tmp=tmp.substring(0,Math.min(78,tmp.length())); System.err.println(tmp); }
     */

    JComponent previous = over_;

    if ((top == null) || !top.getBounds().contains(p)) {
      over_ = null;
    } else {
      // Component c=SwingUtilities.getDeepestComponentAt(top,p.x,p.y);
      // Point q=SwingUtilities.convertPoint(top,p.x,p.y,c);
      Component c = top;
      Point q = p;
      boolean again = true;
      boolean first = true;

      over_ = null;
      target_ = null;
      loc_ = null;

      while (again) {
        Component d = c;
        again = false;

        // System.err.println("C="+c.getClass()+" "+c.isShowing());

        if (first && (c instanceof JComponent) && (((JComponent) c).getClientProperty("DND_TARGET") != null)) {} else {
          if (c instanceof JRootPane) c = ((JRootPane) c).getContentPane();
          else if (c instanceof JTabbedPane) c = ((JTabbedPane) c).getSelectedComponent();
          else if (c instanceof Container) c = ((Container) c).getComponentAt(q.x, q.y);

          if ((c == null) || !c.isVisible()) c = d;

          if (c != d) {
            q = SwingUtilities.convertPoint(d, q.x, q.y, c);
            again = true;

            if (d instanceof JDesktopPane) {
              over_ = null;
              loc_ = null;
            }
          }

          if (c instanceof JInternalFrame) {
            JInternalFrame frame = (JInternalFrame) c;
            JDesktopPane desktop = frame.getDesktopPane();
            if ((desktop != null) && (desktop.getPosition(frame) != 0)) frame.moveToFront();
          }
        }

        first = false;
        JComponent jc = ((c instanceof JComponent) ? (JComponent) c : null);

        if ((jc != null) && (jc.getClientProperty("DND_TARGET") != null))
        // &&!again)//||!(jc instanceof JDesktopPane)))
        {
          target_ = (DndTarget) jc.getClientProperty("DND_TARGET");

          // System.err.println(("Target="+target_));//.substring(0,78));
          // System.err.println(("Accept="+target_.isAcceptable(values_)));

          over_ = null;
          loc_ = null;
          again = (c != d);

          if (target_.isAcceptable(values_) && jc.isEnabled() && (this != jc.getClientProperty("DND_SOURCE"))) {
            over_ = jc;
            loc_ = q;
            // again=(c!=d);//false;
          }
        } else {
          if (c instanceof JInternalFrame.JDesktopIcon) {
            over_ = null;
            loc_ = null;
            JInternalFrame frame = ((JInternalFrame.JDesktopIcon) c).getInternalFrame();
            try {
              frame.setIcon(false);
            } catch (PropertyVetoException ex) {}
          }
        }
      }

      // System.err.println("XY="+q.x+","+q.y);
      // if(over_!=null)
      // System.err.println("Over "+over_.getClass().getName()+" "+loc_);
    }

    if (dragging_ && (loc_ != null)) if (over_ instanceof DndSensible) ((DndSensible) over_).acceptableDrop(loc_);

    if (DndIcons.isIconDisplayed() && (top instanceof JComponent)) {
      try {
        if (!tip) {
          if ((oldtop_ != null) && (oldtop_ != top)) oldtop_.repaint();
          // top n'est pas null car instanceof JComponent
          /* if(top!=null) */
          ((JComponent) top).paintImmediately(ox - 1, oy - 1, ow + 1, oh + 1);

          ox = p.x;
          oy = p.y;
          ow = 0;
          oh = 0;
        }

        Graphics g = top.getGraphics();
        if (icon_ != null) {
          icon_.paintIcon(top, g, ox, oy);
          ow = icon_.getIconWidth();
          oh = icon_.getIconHeight();
        }

        if ((over_ != null) && (target_ != null)) {
          if (loc_ == null) {
            Icon icon = DndIcons.getNoDropIcon();
            if (icon != null) {
              icon.paintIcon(top, g, ox, oy);
              ow = Math.max(ow, icon.getIconWidth());
              oh = Math.max(oh, icon.getIconHeight());
            }
          } else {
            Icon mode = getModeIcon();
            if (mode != null) {
              mode.paintIcon(top, g, ox + ow + 2, oy);
              ow += 2 + mode.getIconWidth();
              oh = Math.max(oh, mode.getIconHeight());
            }

            Icon icon = DndIcons.getCanDropIcon();
            if (icon != null) {
              icon.paintIcon(top, g, ox, oy);
              ow = Math.max(ow, icon.getIconWidth());
              oh = Math.max(oh, icon.getIconHeight());
            }

            if (tip) {
              String tx = target_.getText(values_, getMode());
              if (tx != null) {
                Color oldcolor = g.getColor();
                Shape oldclip = g.getClipBounds();
                Font oldfont = g.getFont();

                g.setFont(UIManager.getFont("ToolTip.font"));

                FontMetrics fm = g.getFontMetrics();
                int ws = fm.stringWidth(tx);
                int hs = fm.getHeight();
                g.setClip(ox, oy + oh, ws + 4, hs + 4);
                g.setColor(UIManager.getColor("ToolTip.background"));
                // new Color(255,255,224,128);
                g.fillRect(ox, oy + oh, ws + 4, hs + 4);
                g.setColor(UIManager.getColor("ToolTip.foreground"));
                // Color.black
                g.drawRect(ox, oy + oh, ws + 4 - 1, hs + 4 - 1);
                g.drawString(tx, ox + 2, oy + 2 + oh + fm.getAscent());
                g.setColor(oldcolor);
                g.setClip(oldclip);
                g.setFont(oldfont);
                ow = Math.max(ow, ws + 4);
                oh += hs + 4;
              }
            }
          }
        }
      } catch (Exception ex) {
        System.err.println("DND: #563 " + ex);
      }
    }

    if ((previous != over_) && (previous instanceof DndSensible)) ((DndSensible) previous).exitedDrop();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    display(oldtop_, oldp_, true);
  }
}
