/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         DndRequestObject.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import javax.swing.JComponent;

public class DndRequestObject
    implements DndRequestData
{
    protected Object[] data_;

    public DndRequestObject(Object _data)
    {
	this(new Object[] { _data });
    }

    public DndRequestObject(Object[] _data)
    {
	data_=_data;
    }

    @Override
    public Object[] request(JComponent _component, Point _location)
    {
	return data_;
    }
}
