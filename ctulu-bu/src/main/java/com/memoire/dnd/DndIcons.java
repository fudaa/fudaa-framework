/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         DndIcons.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.lang.reflect.Array;
import java.util.Hashtable;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class DndIcons
{
  private static final Hashtable icons_=new Hashtable(11);

  public static final void put(Class _class, Icon _icon)
  {
    /*
      Object v=icons_.get(_class);
      if((v!=null)&&(v!=_icon))
      System.err.println(_class.getName()+" already has an icon");
    */

    icons_.put(_class,_icon);
  }

  public static final Icon get(Class _class)
  {
    return (Icon)icons_.get(_class);
  }

  public static final Icon getIcon(Object[] _data)
  {
    Icon    r=null;
    boolean multi=false;
    boolean array=false;
    
    for(int i=0;i<_data.length;i++)
    {
      if(_data[i]==null) continue;
      Class c=_data[i].getClass();
      array=c.isArray();
      if(array)
      {
        c=DndTarget.getCommonClass(_data[i]);
        multi|=(Array.getLength(_data[i])>1);
      }
      //System.err.println("CLASS1="+c);
      r=get(c);
      if(r!=null) break;
    }
    
    if(r==null)
    {
      for(int i=0;i<_data.length;i++)
      {
        if(_data[i]==null) continue;
        Class c=_data[i].getClass();
        if(array)
          //c=c.getComponentType();
          c=DndTarget.getCommonClass(_data[i]);
        /*
          for(Enumeration e=icons_.keys(); e.hasMoreElements(); )
          {
          Class d=(Class)e.nextElement();
          if(d.isAssignableFrom(c))
          { r=get(d); break; }
          }
        */
        while(c!=null)
        {
          //System.err.println("CLASS2="+c);
          r=get(c);
          if(r!=null) break;
          c=c.getSuperclass();
        }
      }
    }

    if(r==null) r=getDefaultIcon();
    
    return (multi ? new DndMultiIcon(r) : r);
  }

  private static boolean iconDisplayed_ =true;
  public static final boolean isIconDisplayed()
  { return iconDisplayed_; }
  public static final void setIconDisplayed(boolean _b)
  { iconDisplayed_=_b; }
  
  private static Icon defaultIcon_ ;
  public static final Icon getDefaultIcon()
  { return defaultIcon_; }
  public static final void setDefaultIcon(Icon _icon)
  { defaultIcon_=_icon; }

  private static Icon noDropIcon_ ;
  public static final Icon getNoDropIcon()
  { return noDropIcon_; }
  public static final void setNoDropIcon(Icon _icon)
  { noDropIcon_=_icon; }

  private static Icon canDropIcon_;
  public static final Icon getCanDropIcon()
  { return canDropIcon_; }
  public static final void setCanDropIcon(Icon _icon)
  { canDropIcon_=_icon; }

  // Actions

  private static Icon copyIcon_=
    new ImageIcon(DndIcons.class.getResource("dnd-copy.gif"));
  public static final Icon getCopyIcon()
  { return copyIcon_; }
  public static final void setCopyIcon(Icon _icon)
  { copyIcon_=_icon; }

  private static Icon moveIcon_=
    new ImageIcon(DndIcons.class.getResource("dnd-move.gif"));
  public static final Icon getMoveIcon()
  { return moveIcon_; }
  public static final void setMoveIcon(Icon _icon)
  { moveIcon_=_icon; }

  /*
  private static Icon linkIcon_=
    new ImageIcon(DndIcons.class.getResource("dnd-link.gif"));
  public static final Icon getLinkIcon()
  { return linkIcon_; }
  public static final void setLinkIcon(Icon _icon)
  { linkIcon_=_icon; }
  */
}
