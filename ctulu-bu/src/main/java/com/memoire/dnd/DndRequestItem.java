/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndRequestItem.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

public class DndRequestItem
    implements DndRequestData
{
    public static final DndRequestItem SINGLETON=
	new DndRequestItem();

    protected DndRequestItem() { }

    @Override
    public Object[] request(JComponent _component, Point _location)
    {
	Object v=null;

	if(_component instanceof JList)
	{
	    JList list =(JList)_component;
	    int   index=list.locationToIndex(_location);
	    if(index>=0) v=list.getModel().getElementAt(index);
	    //System.err.println("INDEX="+index);
	}
	else
	if(_component instanceof JTree)
	{
	    JTree    tree=(JTree)_component;
	    TreePath path=tree.getPathForLocation(_location.x,_location.y);
	    if(path!=null) v=path.getLastPathComponent();
	}

	/*
	  System.err.println
	  ("VALUE="+v+
	  (v!=null ? ":"+v.getClass().getName() : ""));
	*/

	return (v==null ? new Object[0] : new Object[] { v });
    }
}
