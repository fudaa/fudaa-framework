/**
 * @modification $Date: 2005-08-16 12:58:09 $
 * @statut       unstable
 * @file         DndTransferString.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

public class DndTransferString
    implements DndTransferData
{
    public static final DndTransferString SINGLETON=
	new DndTransferString();

    protected String convertToString(Object _data)
    {
      String r="";

      if(_data.getClass().isArray())
      {
	Object[] a=(Object[])_data;
	for(int i=0;i<a.length;i++)
	{
	  if(i>0) r+="\n";
	  r+=a[i].toString();
	}
      }
      else r=_data.toString();

      return r;
    }

    @Override
    public Object transfer(Class _class, Object _data)
    {
        return convertToString(_data);
    }
}
