/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut unstable
 * @file DndTest.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.dnd;

import javax.swing.*;
import java.awt.*;

public class DndTest
    implements DndConstants {
  public static void main(String[] args) {
    DndIcons.setNoDropIcon
        (new ImageIcon(DndTest.class.getResource("dnd-nodrop_24.gif")));
    DndIcons.setCanDropIcon
        (new ImageIcon(DndTest.class.getResource("dnd-candrop_24.gif")));
    DndIcons.setDefaultIcon
        (new ImageIcon(DndTest.class.getResource("dnd-default_24.gif")));

    Color[] COLOR = new Color[]
        {Color.yellow, Color.blue, Color.red, Color.green, Color.magenta};

    JPanel pc = new JPanel(new GridLayout(5, 1));
    JLabel[] cl = new JLabel[5];
    for (int i = 0; i < 5; i++) {
      cl[i] = new JLabel("  ");
      cl[i].setOpaque(true);
      cl[i].setBackground(COLOR[i]);
      pc.add(cl[i], i);
      
      /*
        new DndComponentSource(cl[i],new DndRequestData()
        {
        public Object[] request(Component _component,Point _location)
        {
        Color c=_component.getBackground();
        return new Object[]
        {
        c,
        "#"+Integer.toHexString(c.getRGB()),
        };
        }
        });
      */

      new DndSource(new DndRequestObject
          (new Object[]
              {
                  new DndProperty("foreground", COLOR[i]),
                  COLOR[i],
                  "#" + Integer.toHexString(COLOR[i].getRGB())
              }), COPY)
          .add(cl[i]);
    }

    JFrame frame = new JFrame("Drag&Drop Test");
    JTextField tf = new JTextField(20);
    JTextArea ta = new JTextArea(10, 20); //tmp()
    JLabel lb = new JLabel("file:///hello/");
    JLabel no = new JLabel("<no>");
    JPanel pn = new JPanel(new BorderLayout());

    ta.setLineWrap(true);
    //ta.setWrapStyleWord(true);

    pn.add("North", tf);
    pn.add("Center", new JScrollPane(ta));
    pn.add("South", lb);
    pn.add("East", no);
    pn.add("West", pc);
    frame.setContentPane(pn);

    new DndSource(new DndRequestObject("machin"), MOVE)
        .add(lb);

    new DndSource(new DndRequestObject
        (new DndProperty("font", new Font("SansSerif", Font.PLAIN, 10))))
        .add(no);

    new DndTarget()
        .accept(Color.class,
            DndTransferObject.SINGLETON,
            DndManageBackground.SINGLETON)
        .add(no);

    new DndTarget("Drop me a text or a color")
        .accept(Color.class,
            DndTransferObject.SINGLETON,
            DndManageBackground.SINGLETON)
        .accept(String.class,
            DndTransferString.SINGLETON,
            DndManageText.SINGLETON_REPLACE_ALL)
        .add(tf);

    new DndTarget(null, "Move to String", "Copy to String", MOVE | COPY)
        .accept(DndProperty.class,
            DndTransferObject.SINGLETON,
            DndManageProperty.SINGLETON)
        .accept(String.class,
            DndTransferString.SINGLETON,
            DndManageText.SINGLETON_INSERT_AT_CURSOR)
        .accept(Object.class,
            DndTransferString.SINGLETON,
            DndManageText.SINGLETON_INSERT_AT_CURSOR)
        .add(ta);

    frame.pack();
    frame.setLocation(100, 100);
    frame.setVisible(true);

    JWindow f2 = new JWindow(frame);
    JTextField tf2 = new JTextField(20);
    f2.getContentPane().add(tf2);
    new DndTarget("Drop me something")
        .accept(Color.class,
            DndTransferObject.SINGLETON,
            DndManageBackground.SINGLETON)
        .accept(String.class,
            DndTransferString.SINGLETON,
            DndManageText.SINGLETON_REPLACE_ALL)
        .add(tf2);
    f2.pack();
    f2.setLocation(150, 330);
    f2.setVisible(true);
  }
}
