/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndTarget.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import java.io.Serializable;
import java.util.Hashtable;
import javax.swing.JComponent;
import javax.swing.JRootPane;

public class DndTarget implements Serializable, DndConstants {
  private Hashtable transfers_;
  private Hashtable managers_;
  protected Class[] classes_;
  private int mode_;
  private String text_, moveText_, copyText_;

  public DndTarget() {
    this(null, null, null, NONE);
  }

  public DndTarget(String _text) {
    this(_text, null, null, NONE);
  }

  public DndTarget(String _text, String _moveText, String _copyText, int _mode) {
    classes_ = new Class[0];
    transfers_ = new Hashtable(5);
    managers_ = new Hashtable(5);
    mode_ = _mode;
    text_ = _text;
    moveText_ = _moveText;
    copyText_ = _copyText;
  }

  public final DndTarget accept(Class _class, DndTransferData _transfer, DndManageData _manager) {
    if (_class.isArray()) throw new IllegalArgumentException(_class + " is an array");

    int l = classes_.length;
    Class test = null;

    for (int i = 0; i < l; i++)
      if (classes_[i].isAssignableFrom(_class)) {
        test = classes_[i];
        break;
      }

    if (test == _class) throw new IllegalArgumentException(_class + " was previously added");

    if (test != null) throw new IllegalArgumentException(_class + " is already managed by " + test);

    Class[] r = new Class[l + 1];
    System.arraycopy(classes_, 0, r, 0, l);
    r[l] = _class;
    classes_ = r;
    transfers_.put(_class, _transfer);
    managers_.put(_class, _manager);

    return this;
  }

  public void add(JComponent _comp) {
    if (_comp instanceof JRootPane) throw new RuntimeException("Don't add a RootPane (add the ContentPane instead)");

    if (_comp.getClientProperty("DND_TARGET") == null) _comp.putClientProperty("DND_TARGET", this);
    else
      throw new RuntimeException("Already a Dnd Target");
  }

  protected Class getAcceptedClass(Object[] _data) {
    if (_data == null) {
      System.err.println("Error: _data is null");
      return null;
    }

    Class r = null;

    loop: for (int i = 0; i < classes_.length; i++)
      for (int j = 0; j < _data.length; j++) {
        if (_data[j] == null) continue;
        if (classes_[i].isAssignableFrom(getCommonClass(_data[j]))) {
          r = classes_[i];
          break loop;
        }
      }

    return r;
  }

  protected Object getAcceptedObject(Object[] _data) {
    if (_data == null) {
      System.err.println("Error: _data is null");
      return null;
    }

    Object r = null;

    loop: for (int i = 0; i < classes_.length; i++)
      for (int j = 0; j < _data.length; j++) {
        if (_data[j] == null) continue;
        if (classes_[i].isAssignableFrom(getCommonClass(_data[j]))) {
          r = _data[j];
          break loop;
        }
      }

    return r;
  }

  public static final Class getCommonClass(Object _data) {
    Class r = _data.getClass();

    if (r.isArray()) {
      Object[] o = (Object[]) _data;
      if (o.length == 0) r = r.getComponentType();
      else {
        r = null;
        for (int i = 0; i < o.length; i++) {
          if (o[i] == null) continue;
          if (r == null) r = o[i].getClass();
          else{
            while (r!=null&&!r.isAssignableFrom(o[i].getClass()))
              r = r.getSuperclass();
          }
        }
      }

      // System.err.println("SUPER="+r);
    }

    return r;
  }

  public boolean isAcceptable(Object[] _data) {
    return getAcceptedClass(_data) != null;
  }

  public int getMode() {
    return mode_;
  }

  public String getText(Object[] _data, int _dndmode) {
    String r = null;
    switch (_dndmode & mode_) {
    case MOVE:
      r = moveText_;
      break;
    case COPY:
      r = copyText_;
      break;
    default:
      r = text_;
      break;
    }
    return r;
    // !=nullreturn ((_dndmode==MOVE)&&(moveText_!=null)) ? moveText_ : text_;
  }

  public void drop(Object[] _data, JComponent _comp, Point _loc, int _dndmode) {
    // if(_comp instanceof DndSensible)
    // ((DndSensible)_comp).exitedDrop();

    Class clazz = getAcceptedClass(_data);

    // System.err.println("CLAZZ="+clazz);
    DndTransferData t = (DndTransferData) transfers_.get(clazz);

    Object o = t.transfer(clazz, getAcceptedObject(_data));
    // System.err.println("O="+o);

    if (o != null) {
      DndManageData m = (DndManageData) managers_.get(clazz);
      m.manage(o, _comp, _loc, _dndmode & mode_);
    }
  }
}
