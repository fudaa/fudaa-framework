/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         DndManageProperty.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import java.lang.reflect.Method;
import javax.swing.JComponent;

public class DndManageProperty
  implements DndManageData
{
  public static final DndManageProperty SINGLETON=
    new DndManageProperty();

  @Override
  public boolean manage(Object _data, JComponent _component,
                        Point _location, int _mode)
  {
    DndProperty data=(DndProperty)_data;
    String name =data.getName ();
    Object value=data.getValue();
    System.err.println("set-property "+name+": "+value);
    setProperty(_component,name,value);
    _component.repaint();
    return true;
  }

  public static final void setProperty(JComponent _comp,String _name,
                                       Object _value)
  {
    try
    {
      String n="set"+_name.substring(0,1).toUpperCase()+
        _name.substring(1);
      Class  z=_value.getClass();
      
      // System.err.println("$$$ N="+n);
      
      try
      {
        z=(Class)(z.getField("TYPE").get(z));
      }
      catch(Exception ey) { }

      Class[] c=null;
      Method  m=null;
      
      do
      {
        // System.err.println("$$$ Z="+z);
        
        c=new Class[] { z };
        try
        {
          m=_comp.getClass().getMethod(n,c);
        }
        catch(Exception ey) { m=null; }
        
        // System.err.println("$$$ M="+m);
        
        z=z.getSuperclass();
      }
      while((m==null)&&(z!=null));

      Object[] o=new Object[] { _value };

      // System.err.println("$$$ O="+o);
      if(m!=null)
      m.invoke(_comp,o);
    }
    catch(Exception ex)
    {
      if(!_name.equals("ancestor"))
        System.err.println("Property "+_name+
                           " is not writable in "+
                           _comp.getClass().getName()+".");
    }
  }
}
