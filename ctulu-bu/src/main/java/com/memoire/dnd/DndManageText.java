/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndManageText.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public class DndManageText
    implements DndManageData, DndModes
{
  public static final DndManageText SINGLETON_REPLACE_ALL=
    new DndManageText(REPLACE_ALL);

  public static final DndManageText SINGLETON_INSERT_AT_CURSOR=
    new DndManageText(INSERT_AT_CURSOR);
  
  private int mode_;
  
  public DndManageText(int _mode)
  {
    mode_=_mode;
  }
  
  @Override
  public boolean manage(Object _data, JComponent _component,
                        Point _location, int _dndmode)
  {
    boolean r=false;
    
    try
    {
      String         str =_data.toString();
      JTextComponent text=(JTextComponent)_component;
      Document       doc =text.getDocument();
      int            pos =-1;
      
      synchronized(text)
      {
        synchronized(doc)
        {
          switch(mode_)
	  {
          case INSERT_AT_BEGIN:
            pos=0;//doc.getStartPosition().getOffset();
            break;
          case INSERT_AT_END:
            pos=doc.getLength();//doc.getEndPosition().getOffset();
            break;
          case INSERT_AT_CURSOR:
            Point p=_location;
            pos=text.viewToModel(p);
            break;
          case INSERT_AT_CARET:
            pos=text.getCaretPosition();
            break;
          case INSERT_BEFORE_SELECTION:
            pos=text.getSelectionStart();
            break;
          case INSERT_AFTER_SELECTION:
            pos=text.getSelectionEnd();
            break;
          case REPLACE_SELECTION:
            pos=text.getSelectionStart();
            doc.remove(pos,text.getSelectionEnd()-pos);
            break;
          case REPLACE_ALL:
            int start=doc.getStartPosition().getOffset();
            doc.remove(start,doc.getLength());
            pos=start;
            break;
          }

          text.setCaretPosition(pos);
          doc.insertString(pos,str,null);
          r=true;
          
        }
      }
    }
    catch(BadLocationException ex) { }
    return r;
  }
}
