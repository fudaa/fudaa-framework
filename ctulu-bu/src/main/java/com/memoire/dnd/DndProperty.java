/**
 * @modification $Date: 2005-08-16 12:58:09 $
 * @statut       unstable
 * @file         DndProperty.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

public class DndProperty
{
    private String name_;
    private Object value_;

    public DndProperty(String _name,Object _value)
    {
	name_=_name;
	value_=_value;
    }

    public String getName () { return name_;  }
    public Object getValue() { return value_; }

    public String toString()
    { return "<"+name_+":"+value_+">"; }
}
