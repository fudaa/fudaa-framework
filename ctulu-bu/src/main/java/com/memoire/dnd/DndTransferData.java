/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndTransferData.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.io.Serializable;

public interface DndTransferData
    extends Serializable
{
  Object transfer(Class _class, Object _data);
}
