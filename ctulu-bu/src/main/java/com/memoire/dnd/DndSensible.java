/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         DndSensible.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;

public interface DndSensible
{
  void acceptableDrop(Point _loc);
  void exitedDrop();
}
