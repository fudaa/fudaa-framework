/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         DndSource.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;


public interface DndConstants
{
  int NONE=0;
  int MOVE=1;
  int COPY=2;
  //int LINK=4;
}
