/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndLib.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Frame;
import java.awt.Window;
import java.lang.reflect.Method;
import java.util.Vector;

public class DndLib
{
  public static final Window[] getAllWindows()
  {
    Vector v=new Vector();

    try
    {
      //Frame[] frames=Frame.getFrames();
      Class   c=Frame.class;
      Method  m=c.getMethod("getFrames",new Class[0]);
      Frame[] f=(Frame[])m.invoke(null,new Object[0]);
      for(int i=0;i<f.length;i++)
        if(!v.contains(f[i]))
	{
	  v.addElement(f[i]);
	  //Window[] w=f[i].getOwnedWindows();
	  m=f[i].getClass().getMethod("getOwnedWindows",new Class[0]);
	  Window[] w=(Window[])m.invoke(f[i],new Object[0]);
	  for(int j=0;j<w.length;j++)
	    if(!v.contains(w[j]))
	    {
	      //System.err.println("W<--"+w[j]);
	      v.addElement(w[j]);
	    }
	}
    }
    catch(Throwable th) { System.err.println(th); }

    int      l=v.size();
    Window[] r=new Window[l];
    for(int i=0;i<l;i++) r[i]=(Window)v.elementAt(i);
    return r;    
  }

  
}
