/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndMultiIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

public class DndMultiIcon
    implements Icon
{
    private Icon icon_;

    public DndMultiIcon(Icon _icon)
    {
	icon_=_icon;
    }

    @Override
    public int getIconHeight()
    {
	return 16+icon_.getIconHeight();
    }

    @Override
    public int getIconWidth()
    {
	return 16+icon_.getIconHeight();
    }
                
    @Override
    public void paintIcon(Component _c, Graphics _g, int _x, int _y)
    {
	icon_.paintIcon(_c,_g,_x   ,_y   );
	icon_.paintIcon(_c,_g,_x+ 6,_y+ 6);
	icon_.paintIcon(_c,_g,_x+12,_y+12);
    }
}
