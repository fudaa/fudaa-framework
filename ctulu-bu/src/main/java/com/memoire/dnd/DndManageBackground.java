/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndManageBackground.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Color;
import java.awt.Point;
import javax.swing.JComponent;

public class DndManageBackground
    implements DndManageData
{
  public static final DndManageBackground SINGLETON=
    new DndManageBackground();

  @Override
  public boolean manage(Object _data, JComponent _component,
                        Point _location, int _mode)
  {
    _component.setOpaque(true);
    _component.setBackground((Color)_data);
    _component.repaint();
    return true;
  }
}
