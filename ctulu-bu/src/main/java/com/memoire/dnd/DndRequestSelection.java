/**
 * @modification $Date: 2006-09-19 14:35:21 $
 * @statut       unstable
 * @file         DndRequestSelection.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

public class DndRequestSelection
    implements DndRequestData
{
  public static final DndRequestSelection SINGLETON=
    new DndRequestSelection(false);

  public static final DndRequestSelection SINGLETON_WITH_STRINGS=
    new DndRequestSelection(true);

  protected boolean strings_;

  protected DndRequestSelection(boolean _strings)
  {
    strings_=_strings;
  }

  @Override
  public Object[] request(JComponent _component, Point _location)
  {
    Object[] r=null;
    
    if(_component instanceof JList)
    {
      JList list=(JList)_component;
      r=list.getSelectedValues();
    }
    else
    if(_component instanceof JTree)
    {
      JTree      tree=(JTree)_component;
      TreePath[] path=tree.getSelectionPaths();

      if(path!=null)
      {
	int        l   =path.length;
	r=new Object[l];
	for(int i=0;i<l;i++)
	  r[i]=path[i].getLastPathComponent();
      }
    }

    /*
      if(r!=null)
      System.err.println("VALUE[]="+r.length);
    */

    if((r!=null)&&(r.length==0)) r=null;

    if(r==null)
    {
      return new Object[0];
    }
    else
    if(strings_)
    {
      int l=r.length;
      String[] s=new String[l];
      for(int i=0;i<l;i++)
	s[i]=(r[i]==null ? null : r[i].toString());
      return new Object[] { r,s };
    }
    else
    {
      return new Object[] { r };
    }
  }
}
