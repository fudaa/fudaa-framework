/**
 * @modification $Date: 2005-08-16 12:58:09 $
 * @statut       unstable
 * @file         DndTransferObject.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

public class DndTransferObject
    implements DndTransferData
{
    public static final DndTransferObject SINGLETON=
	new DndTransferObject();

    @Override
    public Object transfer(Class _class, Object _data)
    {
        return _data;
    }
}
