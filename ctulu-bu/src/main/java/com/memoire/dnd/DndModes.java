/**
 * @modification $Date: 2005-08-16 12:58:09 $
 * @statut       unstable
 * @file         DndModes.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

public interface DndModes
{
    int INSERT_AT_BEGIN        =0;
    int INSERT_AT_END          =1;
    int INSERT_AT_CURSOR       =2;
    int INSERT_AT_CARET        =3;
    int INSERT_BEFORE_SELECTION=4;
    int INSERT_AFTER_SELECTION =5;
    int REPLACE_SELECTION      =6;
    int REPLACE_ALL            =7;
}
