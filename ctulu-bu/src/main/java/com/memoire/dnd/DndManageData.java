/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         DndManageData.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dnd;

import java.awt.Point;
import java.io.Serializable;
import javax.swing.JComponent;

public interface DndManageData
  extends Serializable
{
  boolean manage(Object _data,JComponent _component,
                 Point _location, int _mode);
}
