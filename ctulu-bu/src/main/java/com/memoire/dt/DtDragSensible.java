/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtDragSensible.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

public interface DtDragSensible
{
  void dragEnter(boolean _acceptable,Point _location);
  void dragOver (boolean _acceptable,Point _location);
  void dragExit ();

  DropTargetListener SINGLETON=new DropTargetListener()
  {
    private boolean check(Component _c,DataFlavor[] _f)
    {
      boolean r=false;
      if(_c instanceof JComponent)
      {
        JComponent      c=(JComponent)_c;
        TransferHandler h=c.getTransferHandler();
        if((h!=null)&&h.canImport(c,_f))
          r=true;
      }
      return r;
    }
    
    @Override
    public final void dropActionChanged(DropTargetDragEvent _dtde)
    {
      //FuLog.debug("DDA: action changed");
      DropTargetContext ctx=_dtde.getDropTargetContext();
      Component c=ctx.getComponent();
      boolean   b=false;
      if(c instanceof DtDragSensible)
      {
        b=check(c,_dtde.getCurrentDataFlavors());
        ((DtDragSensible)c).dragOver(b,_dtde.getLocation());
      }
      if(b) _dtde.acceptDrag(_dtde.getDropAction()); else _dtde.rejectDrag();
    }

    @Override
    public final void dragEnter(DropTargetDragEvent _dtde)
    {
      //FuLog.debug("DDA: drag enter");
      DropTargetContext ctx=_dtde.getDropTargetContext();
      Component c=ctx.getComponent();
      boolean   b=false;
      if(c instanceof DtDragSensible)
      {
        b=check(c,_dtde.getCurrentDataFlavors());
        ((DtDragSensible)c).dragEnter(b,_dtde.getLocation());
      }
      if(b) _dtde.acceptDrag(_dtde.getDropAction()); else _dtde.rejectDrag();
    }

    @Override
    public final void dragExit(DropTargetEvent _dte)
    {
      //FuLog.debug("DDA: drag exit");
      DropTargetContext ctx=_dte.getDropTargetContext();
      Component c=ctx.getComponent();
      if(c instanceof DtDragSensible)
      {
        ((DtDragSensible)c).dragExit();
      }
    }

    @Override
    public final void dragOver(DropTargetDragEvent _dtde)
    {
      //FuLog.debug("DDA: drag over");
      DropTargetContext ctx=_dtde.getDropTargetContext();
      Component c=ctx.getComponent();
      boolean   b=false;
      if(c instanceof DtDragSensible)
      {
        b=check(c,_dtde.getCurrentDataFlavors());
        ((DtDragSensible)c).dragOver(b,_dtde.getLocation());
      }
      if(b) _dtde.acceptDrag(_dtde.getDropAction()); else _dtde.rejectDrag();
    }
    
    @Override
    public final void drop(DropTargetDropEvent _dtde)
    {
      //FuLog.debug("DDA: drop");
      DropTargetContext ctx=_dtde.getDropTargetContext();
      Component c=ctx.getComponent();
      
      if(c instanceof DtDragSensible)
      {
        ((DtDragSensible)c).dragExit();
      }
    }
  };
}
