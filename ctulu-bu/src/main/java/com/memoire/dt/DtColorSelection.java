/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         DtColorSelection.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;

/**
 * A container for a single color.
 * Usefull to simplify data transfers.
 */
public final class DtColorSelection
  implements Transferable, Serializable
{
  private static final long serialVersionUID=7056300990833125785L;

  private int    action_=DtLib.MOVE;
  private Color  data_  ;

  public DtColorSelection(Color _data)
  {
    data_=_data;
  }

  public int getAction()
  {
    return action_;
  }

  public void setAction(int _action)
  {
    action_=_action;
  }

  public Color getColor()
  {
    return data_;
  }

  public String getString()
  {
    if(data_==null) return null;

    int a=data_.getAlpha();
    int r=data_.getRed();
    int g=data_.getGreen();
    int b=data_.getBlue();
    String s=Integer.toHexString((a<<24)|(r<<16)|(g<<8)|b);
    while(s.length()<8) s="0"+s;
    return "#"+s.toUpperCase();
  }

  public String toString()
  {
    return data_==null ? "no color" : "one color";
  }

  // Transferable

  @Override
  public Object getTransferData(DataFlavor _flavor)
    throws UnsupportedFlavorException
  {
    if(data_!=null)
    {
      if(DtLib.colorFlavor.equals(_flavor))
        return getColor();
      else
      if(DtLib.stringFlavor.equals(_flavor))
        return getString();
    }
    throw new UnsupportedFlavorException(_flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors()
  {
    return FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor _flavor)
  {
    return (data_!=null)&&DtLib.matches(_flavor,FLAVORS);
  }

  private static final DataFlavor[] FLAVORS=
  {
    DtLib.colorFlavor,
    DtLib.stringFlavor
  };

  public static final boolean canConvert(Transferable _t)
  {
    return DtLib.matches(_t,FLAVORS);
  }

  public static final boolean canConvert(DataFlavor[] _flavors)
  {
    return DtLib.matches(_flavors,FLAVORS);
  }

  public static final DtColorSelection convert(Transferable _t)
  {
    DtColorSelection r=null;

    try
    {
      if(_t.isDataFlavorSupported(DtLib.colorFlavor))
      {
        //FuLog.debug("DCS: colorFlavor");
        r=new DtColorSelection((Color)_t.getTransferData(DtLib.colorFlavor));
      }
      else
      if(_t.isDataFlavorSupported(DtLib.stringFlavor))
      {
        //FuLog.debug("DCS: stringFlavor");
        String s=(String)_t.getTransferData(DtLib.stringFlavor);
        int    l=s.length();
        if(s.startsWith("#")&&((l==6)||(l==8)))
        {
          s=s.substring(1);
          try
          {
            int i=Integer.parseInt(s,16);
            r=new DtColorSelection(new Color(i,l==8));
          }
          catch(NumberFormatException ex) { }
        }
      }
    }
    catch(UnsupportedFlavorException ufex) { }
    catch(IOException ioex) { }

    return r;
  }
}
