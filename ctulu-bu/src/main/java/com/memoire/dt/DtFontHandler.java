/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtFontHandler.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

public final class DtFontHandler
  extends TransferHandler
{
  public interface IE
  {
    Font exportFont();
    void importFont(Font _font);
  }

  private IE ie_;

  public DtFontHandler(IE _ie)
  {
    ie_=_ie;
  }

  // Export

  @Override
  protected final Transferable createTransferable(JComponent _c)
  {
    Font o=ie_.exportFont();
    if(o==null) return null;
    return new DtFontSelection(o);
  }
  
  @Override
  public final int getSourceActions(JComponent _c)
  {
    return DtLib.COPY;
  }

  // Import

  @Override
  public boolean canImport(JComponent _c, DataFlavor[] _flavors)
  {
    return DtFontSelection.canConvert(_flavors);
  }

  @Override
  public boolean importData(JComponent _c, Transferable _t)
  {
    if(!canImport(_c,_t.getTransferDataFlavors())) return false;

    DtFontSelection s=DtFontSelection.convert(_t);
    if(s==null) return false;
    Font o=s.getFont();
    if(o==null) return false;
    ie_.importFont(o);
    return true;
  }
}
