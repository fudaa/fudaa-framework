/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtFileHandler.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

public abstract class DtFileHandler
  extends TransferHandler
{
  private static final DtFileHandler SINGLETON=
    new DtFileHandler()
    {
      @Override
      protected Object getData(JComponent _c)
      {
        return _c.getClientProperty("DT_DRAG_VALUE");
      }
    };

  public static final DtFileHandler getInstance()
  {
    return SINGLETON;
  }

  protected DtFileHandler() { }

  protected abstract Object getData(JComponent _c);

  @Override
  protected final Transferable createTransferable(JComponent _c)
  {
    Object o=getData(_c);
    if(o==null) return null;
    DtFilesSelection r=new DtFilesSelection(o);
    return r.size()==0 ? null : r;
  }
  
  @Override
  public final int getSourceActions(JComponent _c)
  {
    return DtLib.ALL;
  }
}
