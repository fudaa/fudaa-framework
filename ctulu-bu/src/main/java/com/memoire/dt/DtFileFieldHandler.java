/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtFileFieldHandler.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.vfs.VfsFile;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.net.URL;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import javax.swing.text.JTextComponent;

public final class DtFileFieldHandler
  extends TransferHandler
{
  public static final int ALL   =0;
  public static final int LOCAL =1;
  public static final int URL   =2;

  private boolean single_,reduce_;
  private int     mode_;

  public DtFileFieldHandler(boolean _single,int _mode,boolean _reduce)
  {
    single_=_single;
    mode_  =_mode;
    reduce_=_reduce;
  }

  @Override
  public boolean canImport(JComponent _c, DataFlavor[] _flavors)
  {
    if(!(_c instanceof JTextComponent)) return false;

    return DtFilesSelection.canConvert(_flavors);
  }

  @Override
  public boolean importData(JComponent _c, Transferable _t)
  {
    if(!canImport(_c,_t.getTransferDataFlavors())) return false;

    DtFilesSelection s=DtFilesSelection.convert(_t);
    if(s==null) return false;

    FuLog.debug("DFS: importData");

    StringBuffer b=new StringBuffer();

    if(mode_==ALL)
    {
      VfsFile[] f=s.getAllFiles();
      for(int i=0;i<f.length;i++)
      {
        String p=f[i].getAbsolutePath();
        if(reduce_) p=FuLib.reducedPath(p);
        b.append(p);
        if(single_) break;
        b.append('\n');
      }
    }
    else
    if(mode_==LOCAL)
    {
      File[] f=s.getLocalFiles();
      for(int i=0;i<f.length;i++)
      {
        String p=f[i].getAbsolutePath();
        if(reduce_) p=FuLib.reducedPath(p);
        b.append(p);
        if(single_) break;
        b.append('\n');
      }
    }
    else
    if(mode_==URL)
    {
      URL[] f=s.getURLs();
      for(int i=0;i<f.length;i++)
      {
        b.append(f[i].toString());
        if(single_) break;
        b.append('\n');
      }
    }

    String r="";
    if(!single_)
    {
      r=((JTextComponent)_c).getText();
      if(r==null) r="";
      r=r.trim();
      if(!"".equals(r)) r+="\n";
      r+=b.toString();
    }
    else r=b.toString();

    if(!"".equals(r))
    {
      ((JTextComponent)_c).setText(r);
      //((JTextComponent)_c).requestFocus();
      if(single_) ((JTextComponent)_c).selectAll();
      return true;
    }
    return false;
  }
}
