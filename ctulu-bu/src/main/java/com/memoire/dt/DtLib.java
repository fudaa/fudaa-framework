/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtLib.java
 * @version      0.01
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;

public class DtLib
{
  public static final int NONE=DnDConstants.ACTION_NONE;
  public static final int COPY=DnDConstants.ACTION_COPY;
  public static final int MOVE=DnDConstants.ACTION_MOVE;
  public static final int LINK=DnDConstants.ACTION_LINK;
  public static final int ALL =COPY|MOVE|LINK;

  public static final DataFlavor fileListFlavor=DataFlavor.javaFileListFlavor;
  public static final DataFlavor imageFlavor   =DataFlavor.imageFlavor;
  public static final DataFlavor stringFlavor  =DataFlavor.stringFlavor;
  public static final DataFlavor colorFlavor   =createLocal("java.awt.Color","Color");
  public static final DataFlavor fontFlavor    =createLocal("java.awt.Font" ,"Font");
  public static final DataFlavor urlFlavor     =createLocal("java.net.URL"  ,"URL");
  public static final DataFlavor vfsFlavor     =createLocal("com.memoire.dt.DtFilesSelection","List of Files");

  private static final DataFlavor createLocal(String _class,String _name)
  {
    return new DataFlavor
      (DataFlavor.javaJVMLocalObjectMimeType+"; class="+_class,_name);
  }

  public static final DataFlavor uriListFlavor=new DataFlavor
    ("text/uri-list; class=java.io.Reader","List of URIs");


  public static final boolean matches(DataFlavor _flavor,
                                      DataFlavor[] _flavors)
                                      
  {
    if((_flavors!=null)&&(_flavor!=null))
      for(int i=0;i<_flavors.length;i++)
        if(_flavor.equals(_flavors[i]))
          return true;
    return false;
  }

  public static final boolean matches(DataFlavor[] _flavors1,
                                      DataFlavor[] _flavors2)
  {
    if((_flavors1!=null)&&(_flavors2!=null))
      for(int i=0;i<_flavors1.length;i++)
        if(_flavors1[i]!=null)
          for(int j=0;j<_flavors2.length;j++)
            if(_flavors1[i].equals(_flavors2[j]))
              return true;
    return false;
  }

  public static final boolean matches(Transferable _t,DataFlavor _flavor)
  {
    return (_t!=null)&&matches(_flavor,_t.getTransferDataFlavors());
  }

  public static final boolean matches(Transferable _t,DataFlavor[] _flavors)
  {
    return (_t!=null)&&matches(_t.getTransferDataFlavors(),_flavors);
  }
}
