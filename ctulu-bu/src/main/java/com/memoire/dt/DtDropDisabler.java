/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtDropDisabler.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

//import com.memoire.fu.*;
import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

public final class DtDropDisabler
  extends DtDropAbstract
{
  private static final DtDropDisabler SINGLETON=new DtDropDisabler();

  private static final TransferHandler HANDLER=new TransferHandler()
    {
      @Override
      protected final Transferable createTransferable(JComponent _c)
      {
        return null;
      }

      @Override
      public final int getSourceActions(JComponent _c)
      {
        return DtLib.NONE;//ALL;
      }

      @Override
      public boolean canImport(JComponent _c, DataFlavor[] _flavors)
      {
        return false;
      }

      @Override
      public boolean importData(JComponent _c, Transferable _t)
      {
        return false;
      }
    };

  private DtDropDisabler() { }

  @Override
  public boolean checkDropContext(int _a, Component _c, DataFlavor[] _f, Point _p)
  {
    return false;
  }

  @Override
  public Object getData(int _a, Component _c, Transferable _data, Point _p)
  {
    return null;
  }

  @Override
  public boolean dropData(int _a, Component _c, Object _data, Point _p)
  {
    return false;
  }

  public static final void install(Component _c)
  {
    if(_c instanceof JComponent)
      ((JComponent)_c).setTransferHandler(HANDLER);
    new DropTarget(_c,DtLib.ALL,SINGLETON);
  }
}
