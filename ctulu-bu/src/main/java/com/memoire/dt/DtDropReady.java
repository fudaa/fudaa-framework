/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtDropReady.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

public interface DtDropReady
{
  boolean isDropEnabled  ();
  int     getDropActions ();
  boolean checkDropFlavor(DataFlavor[] _f,Point _p);
  Object  getDropData    (Transferable _data);
  boolean dropData       (int _a,Object _data,Point _p);
}
