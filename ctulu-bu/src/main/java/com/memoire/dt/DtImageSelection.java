/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         DtImageSelection.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;

/**
 * A container for a single font.
 * Usefull to simplify data transfers.
 */
public final class DtImageSelection
  implements Transferable, Serializable
{
  //private static final long serialVersionUID=7056300990833125785L;

  private int   action_=DtLib.MOVE;
  private Image data_  ;

  public DtImageSelection(Image _data)
  {
    data_=_data;
  }

  public int getAction()
  {
    return action_;
  }

  public void setAction(int _action)
  {
    action_=_action;
  }

  public Image getImage()
  {
    return data_;
  }

  /*
  public String getString()
  {
    if(data_==null) return null;

    StringBuffer sb=new StringBuffer(128);
    sb.append(data_.getFamily());
    sb.append(',');
    if(data_.isBold())   sb.append("bold");
    if(data_.isItalic()) sb.append("italic");
    sb.append(',');
    sb.append(data_.getSize());
    return sb.toString();
  }
  */

  public String toString()
  {
    return data_==null ? "no image" : "one image";
  }

  // Transferable

  @Override
  public Object getTransferData(DataFlavor _flavor)
    throws UnsupportedFlavorException
  {
    if(data_!=null)
    {
      if(DtLib.imageFlavor.equals(_flavor))
        return getImage();
      /*
      else
      if(DtLib.stringFlavor.equals(_flavor))
        return getString();
      */
    }
    throw new UnsupportedFlavorException(_flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors()
  {
    return FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor _flavor)
  {
    return (data_!=null)&&DtLib.matches(_flavor,FLAVORS);
  }

  private static final DataFlavor[] FLAVORS=
  {
    DtLib.imageFlavor,
    //DtLib.stringFlavor
  };

  public static final boolean canConvert(Transferable _t)
  {
    return DtLib.matches(_t,FLAVORS);
  }

  public static final boolean canConvert(DataFlavor[] _flavors)
  {
    return DtLib.matches(_flavors,FLAVORS);
  }

  public static final DtImageSelection convert(Transferable _t)
  {
    DtImageSelection r=null;

    try
    {
      if(_t.isDataFlavorSupported(DtLib.imageFlavor))
      {
        //FuLog.debug("DCS: imageFlavor");
        r=new DtImageSelection((Image)_t.getTransferData(DtLib.imageFlavor));
      }
      /*
      else
      if(_t.isDataFlavorSupported(DtLib.stringFlavor))
      {
      }
      */
    }
    catch(UnsupportedFlavorException ufex) { }
    catch(IOException ioex) { }

    return r;
  }
}
