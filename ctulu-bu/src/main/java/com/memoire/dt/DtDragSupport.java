/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtDragSupport.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import com.memoire.fu.FuLog;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

public final class DtDragSupport
{
  private static final MM SINGLETON=new MM();

  public static final void install(JComponent _c)
  {
    _c.addMouseListener(SINGLETON);
    _c.addMouseMotionListener(SINGLETON);
  }

  public static final void uninstall(JComponent _c)
  {
    _c.removeMouseListener(SINGLETON);
    _c.removeMouseMotionListener(SINGLETON);
  }

  static final class MM
    implements MouseListener, MouseMotionListener
  {
    private int     x_,y_;
    private boolean dragging_;
    //private Cursor  old_;

    @Override
    public void mouseEntered(MouseEvent _evt) { }
    @Override
    public void mouseExited (MouseEvent _evt) { }
    @Override
    public void mouseClicked(MouseEvent _evt) { }
      
    @Override
    public void mousePressed(MouseEvent _evt)
    {
      x_=_evt.getX();
      y_=_evt.getY();
      dragging_=false;

      /*
      final JComponent c=(JComponent)_evt.getSource();
      old_=c.getCursor();
      SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            c.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
          }
        });
      */
    }

    @Override
    public void mouseReleased(MouseEvent _evt)
    {
      dragging_=false;
      /*
      final JComponent c=(JComponent)_evt.getSource();
      SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            c.setCursor(old_);
          }
        });
      */
    }

    @Override
    public void mouseDragged(MouseEvent _evt)
    {
      if(!dragging_)
      {
        int x=_evt.getX();
        int y=_evt.getY();

        if(Math.abs(x-x_)+Math.abs(y-y_)>=3)
        {
          JComponent      c=(JComponent)_evt.getSource();
          TransferHandler h=c.getTransferHandler();
          if(h!=null)
          {
            dragging_=true;
            int      a=h.getSourceActions(c)&DtLib.MOVE;
            if(a==0) a=h.getSourceActions(c)&DtLib.COPY;
            if(a==0) a=h.getSourceActions(c)&DtLib.LINK;
            if(a==0) a=                      DtLib.NONE;
            h.exportAsDrag(c,_evt,a);
          }
          else
            FuLog.warning("no transfer handler set on "+
                          c.getClass().getName());
        }
      }
    }
    
    @Override
    public void mouseMoved(MouseEvent _evt)
    {
    }
  }
}
