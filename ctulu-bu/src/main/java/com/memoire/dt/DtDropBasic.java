/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtDropBasic.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

//import com.memoire.fu.*;
import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;

public final class DtDropBasic
  extends DtDropAbstract
{
  private static final DtDropBasic SINGLETON=new DtDropBasic();

  private DtDropBasic() { }

  @Override
  public boolean checkDropContext(int _a, Component _c, DataFlavor[] _f, Point _p)
  {
    DtDropReady c=(DtDropReady)_c;
    return
      //(_a!=DtLib.NONE)&&
      ((_a&c.getDropActions())!=0)&&
      c.checkDropFlavor(_f,_p);

    /*
    return
      ((_a==0   )||((_a&c.getDropActions())!=0))&&
      ((_t==null)||c.checkDropTransferable(_t));
    */
  }

  @Override
  public Object getData(int _a, Component _c, Transferable _data, Point _p)
  {
    DtDropReady c=(DtDropReady)_c;
    return c.getDropData(_data);
  }

  @Override
  public boolean dropData(int _a, Component _c, Object _data, Point _p)
  {
    DtDropReady c=(DtDropReady)_c;
    return c.dropData(_a,_data,_p);
  }

  public static final void install(DtDropReady _c)
  {
    new DropTarget((Component)_c,_c.getDropActions(),SINGLETON);
  }
}
