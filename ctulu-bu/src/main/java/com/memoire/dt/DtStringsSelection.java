/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtStringsSelection.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import com.memoire.fu.FuLib;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

/**
 * A container for strings.
 * Usefull to simplify data transfers.
 */
public final class DtStringsSelection
  implements Transferable, Serializable
{
  private static final long serialVersionUID=4741476608003021030L;

  private int    action_=DtLib.MOVE;
  private Vector data_  =new Vector(1,20);

  public DtStringsSelection()
  {
  }

  public DtStringsSelection(Collection _list)
  {
    Iterator i=_list.iterator();
    while(i.hasNext())
      add(i.next());
  }

  public DtStringsSelection(Object[] _array)
  {
    for(int i=0;i<_array.length;i++)
      add(_array[i]);
  }

  public DtStringsSelection(Object _o)
  {
    if(_o!=null) add(_o);
  }

  private void add(Object _o)
  {
    if(_o!=null)
      data_.addElement(_o.toString());
  }

  public int getAction()
  {
    return action_;
  }

  public void setAction(int _action)
  {
    action_=_action;
  }

  public int size()
  {
    return data_.size();
  }

  public String[] getAllStrings()
  {
    int      l=data_.size();
    String[] r=new String[l];
    for(int i=0;i<l;i++)
      //r[i]=(String)data_.elementAt(i);
      r[i]=(String)data_.elementAt(i);
    return r;
  }

  public String getWholeString()
  {
    int          l=data_.size();
    StringBuffer r=new StringBuffer(l*60);
    for(int i=0;i<l;i++)
    {
      r.append(data_.elementAt(i));
      r.append('\n');
    }
    return r.toString();
  }

  public String toString()
  {
    return data_.size()+" strings";
  }

  // Transferable

  @Override
  public Object getTransferData(DataFlavor _flavor)
    throws UnsupportedFlavorException
  {
    if(DtLib.stringFlavor.equals(_flavor))
      return getWholeString();
    throw new UnsupportedFlavorException(_flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors()
  {
    return FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor _flavor)
  {
    if(size()>0)
      for(int i=0;i<FLAVORS.length;i++)
        if(FLAVORS[i].equals(_flavor))
          return true;
    return false;
  }

  private static final DataFlavor[] FLAVORS=
  {
    DtLib.stringFlavor
  };

  public static final boolean canConvert(Transferable _t)
  {
    return (_t!=null)&&canConvert(_t.getTransferDataFlavors());
  }

  public static final boolean canConvert(DataFlavor[] _flavors)
  {
    if(_flavors!=null)
      for(int i=0;i<FLAVORS.length;i++)
        for(int j=0;j<_flavors.length;j++)
          if(FLAVORS[i].equals(_flavors[j]))
            return true;
    return false;
  }

  public static final DtStringsSelection convert(Transferable _t)
  {
    DtStringsSelection r=null;

    try
    {
      if(_t.isDataFlavorSupported(DtLib.stringFlavor))
      {
        //FuLog.debug("DSS: stringFlavor");
        String   s=(String)_t.getTransferData(DtLib.stringFlavor);
        String[] a=FuLib.split(s,'\n',false,true);
        r=new DtStringsSelection(a);
        //FuLog.debug("DSS: R="+r.size()+" A="+a.length);
        if(r.size()!=a.length) r=null;
      }
    }
    catch(UnsupportedFlavorException ufex) { }
    catch(IOException ioex) { }

    return r;
  }
}
