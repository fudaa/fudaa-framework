/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtDropAbstract.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import java.awt.Component;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

public abstract class DtDropAbstract
  implements DropTargetListener
{
  protected abstract boolean checkDropContext
    (int _a,Component _c,DataFlavor[] _f,Point _p);
  protected abstract Object  getData
    (int _a,Component _c,Transferable _data,Point _p);
  protected abstract boolean dropData
    (int _a,Component _c,Object _data,Point _p);

  @Override
  public final void dropActionChanged(DropTargetDragEvent _dtde)
  {
    //FuLog.debug("DDA: action changed");
    DropTargetContext ctx=_dtde.getDropTargetContext();
    Component c=ctx.getComponent();
    int       a=_dtde.getDropAction();
    boolean   b=false;
    if(c instanceof DtDragSensible)
    {
      Point p=_dtde.getLocation();
      ((DtDragSensible)c).dragOver
        (b=checkDropContext
         (a,c,_dtde.getCurrentDataFlavors(),p),p);
    }
    if(b) _dtde.acceptDrag(a);
    else  _dtde.rejectDrag();
  }

  @Override
  public final void dragEnter(DropTargetDragEvent _dtde)
  {
    //FuLog.debug("DDA: drag enter");
    DropTargetContext ctx=_dtde.getDropTargetContext();
    Component c=ctx.getComponent();
    int       a=_dtde.getDropAction();
    boolean   b=false;
    if(c instanceof DtDragSensible)
    {
      Point p=_dtde.getLocation();
      ((DtDragSensible)c).dragEnter
        (b=checkDropContext
         (a,c,_dtde.getCurrentDataFlavors(),p),p);
    }
    if(b) _dtde.acceptDrag(a);
    else  _dtde.rejectDrag();
  }

  @Override
  public final void dragExit(DropTargetEvent _dte)
  {
    //FuLog.debug("DDA: drag exit");
    DropTargetContext ctx=_dte.getDropTargetContext();
    Component c=ctx.getComponent();
    if(c instanceof DtDragSensible)
    {
      ((DtDragSensible)c).dragExit();
    }
  }

  @Override
  public final void dragOver(DropTargetDragEvent _dtde)
  {
    //FuLog.debug("DDA: drag over");
    DropTargetContext ctx=_dtde.getDropTargetContext();
    Component c=ctx.getComponent();
    int       a=_dtde.getDropAction();
    boolean   b=false;
    if(c instanceof DtDragSensible)
    {
      Point p=_dtde.getLocation();
      ((DtDragSensible)c).dragOver
        (b=checkDropContext
         (a,c,_dtde.getCurrentDataFlavors(),p),p);
    }
    if(b) _dtde.acceptDrag(a);
    else  _dtde.rejectDrag();
  }

  @Override
  public final void drop(DropTargetDropEvent _dtde)
  {
    //FuLog.debug("DDA: drop");

    DropTargetContext ctx=_dtde.getDropTargetContext();

    int          a=_dtde.getDropAction();
    Component    c=ctx.getComponent();
    Transferable t=_dtde.getTransferable();
    DataFlavor[] f=_dtde.getCurrentDataFlavors();
    Point        p=_dtde.getLocation();
    boolean      b=checkDropContext(a,c,f,p);

    if(c instanceof DtDragSensible)
      ((DtDragSensible)c).dragExit();

    if(!b)
    {
      _dtde.rejectDrop();
      return;
    }

    //FuLog.debug("DDA: drop accepted");
    _dtde.acceptDrop(a);

    Object  data=getData(a,c,t,p);
    boolean r=(data!=null);
    if(r) r=dropData(a,c,data,p);
    _dtde.dropComplete(r);
  }
}
