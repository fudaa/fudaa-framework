/**
 * @modification $Date: 2006-09-19 14:35:13 $
 * @statut       unstable
 * @file         DtFilesSelection.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.vfs.VfsFile;
import com.memoire.vfs.VfsFileFile;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * A container for VFS files.
 * Usefull to simplify data transfers.
 */
public final class DtFilesSelection
  implements Transferable, Serializable
{
  private  static final long serialVersionUID = 6831570958114622060L;

  private int    action_=DtLib.MOVE;
  private Vector data_  =new Vector(1,20);

  public DtFilesSelection()
  {
  }

  public DtFilesSelection(Collection _list)
  {
    Iterator i=_list.iterator();
    while(i.hasNext())
      add(i.next());
  }

  public DtFilesSelection(Object[] _array)
  {
    for(int i=0;i<_array.length;i++)
      add(_array[i]);
  }

  public DtFilesSelection(Object _o)
  {
    if(_o!=null) add(_o);
  }

  private void add(Object _o)
  {
    VfsFile f=VfsFile.convertToVfsFile(_o);
    if((f!=null)&&(_o instanceof String))
      if((f instanceof VfsFileFile)&&!f.exists()) f=null;

    if(f!=null) data_.addElement(f.getAbsolutePath());
  }

  public int getAction()
  {
    return action_;
  }

  public void setAction(int _action)
  {
    action_=_action;
  }

  public int size()
  {
    return data_.size();
  }

  public VfsFile[] getAllFiles()
  {
    int       l=data_.size();
    VfsFile[] r=new VfsFile[l];
    for(int i=0;i<l;i++)
      r[i]=VfsFile.createFile((String)data_.elementAt(i));
    return r;
  }

  public File[] getLocalFiles()
  {
    int       l=data_.size();
    int       j=0;
    File[] t=new File[l];
    for(int i=0;i<l;i++)
    {
      t[j]=VfsFile.createFile((String)data_.elementAt(i));
      if(t[j] instanceof VfsFileFile)
      {
        t[j]=new File(t[j].getAbsolutePath());
        j++;
      }
    }
    File[] r=new File[j];
    System.arraycopy(t,0,r,0,j);
    return r;
  }

  public URL[] getURLs()
  {
    int   l=data_.size();
    int   j=0;
    URL[] t=new URL[l];
    for(int i=0;i<l;i++)
    {
      try
      {
        t[j]=VfsFile.createFile((String)data_.elementAt(i)).toURL();
        j++;
      }
      catch(MalformedURLException ex) { }
    }
    URL[] r=new URL[j];
    System.arraycopy(t,0,r,0,j);
    return r;
  }

  public String toString()
  {
    return data_.size()+" selected file(s)";
  }

  // Transferable

  @Override
  public Object getTransferData(DataFlavor _flavor)
    throws UnsupportedFlavorException
  {
    if(DtLib.vfsFlavor.equals(_flavor))
    {
      return this;
    }
    else
    if(DtLib.urlFlavor.equals(_flavor))
    {
      try
      {
        if(data_.size()>0)
          return VfsFile.createFile((String)data_.elementAt(0)).toURL();
      }
      catch(MalformedURLException ex) { }
    }
    else
    if(DtLib.fileListFlavor.equals(_flavor))
    {
      List r=new ArrayList();
      r.addAll(Arrays.asList(getLocalFiles()));
      return r;
    }
    else
    if(DtLib.uriListFlavor.equals(_flavor))
    {
      URL[]        u =getURLs();
      int          l =u.length;
      StringBuffer sb=new StringBuffer(l*60);
      for(int i=0;i<l;i++)
      {
        sb.append(u[i].toString());
        sb.append('\n');
      }
      return new StringReader(sb.toString());
    }
    else
    if(DtLib.stringFlavor.equals(_flavor))
    {
      int          l =data_.size();
      StringBuffer sb=new StringBuffer(l*60);
      for(int i=0;i<l;i++)
      {
        sb.append(data_.elementAt(i));
        sb.append('\n');
      }      
      return sb.toString();
    }
    throw new UnsupportedFlavorException(_flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors()
  {
    return FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor _flavor)
  {
    return (size()>0)&&DtLib.matches(_flavor,FLAVORS);
  }

  private static final DataFlavor[] FLAVORS=
  {
    DtLib.vfsFlavor,
    DtLib.urlFlavor,
    DtLib.fileListFlavor,
    DtLib.uriListFlavor,
    DtLib.stringFlavor
  };

  public static final boolean canConvert(Transferable _t)
  {
    return DtLib.matches(_t,FLAVORS);
  }

  public static final boolean canConvert(DataFlavor[] _flavors)
  {
    return DtLib.matches(_flavors,FLAVORS);
  }

  public static final DtFilesSelection convert(Transferable _t)
  {
    DtFilesSelection r=null;

    try
    {
      if(_t.isDataFlavorSupported(DtLib.vfsFlavor))
      {
        //FuLog.debug("DFS: vfsFlavor");
        r=(DtFilesSelection)_t.getTransferData(DtLib.vfsFlavor);
      }
      else
      if(_t.isDataFlavorSupported(DtLib.uriListFlavor))
      {
        //FuLog.debug("DFS: uriListFlavor");
        StringBuffer sb=new StringBuffer();
        Reader rin=DtLib.uriListFlavor.getReaderForText(_t);
        int cc;
        while((cc=rin.read())>=0) sb.append((char)cc);
        FuLog.debug(sb.toString());
        String[] a=FuLib.split(sb.toString(),'\n',false,true);
        r=new DtFilesSelection(a);
        //FuLog.debug("DFS: R="+r.size()+" A="+a.length);
        if(r.size()!=a.length) r=null;
      }
      else
      if(_t.isDataFlavorSupported(DtLib.fileListFlavor))
      {
        //FuLog.debug("DFS: javaFileListFlavor");
        r=new DtFilesSelection((java.util.List)_t.getTransferData
                               (DtLib.fileListFlavor));
      }
      else
      if(_t.isDataFlavorSupported(DtLib.urlFlavor))
      {
        //FuLog.debug("DFS: urlFlavor");
        r=new DtFilesSelection(_t.getTransferData(DtLib.urlFlavor));
      }
      else
      if(_t.isDataFlavorSupported(DtLib.stringFlavor))
      {
        //FuLog.debug("DFS: stringFlavor");
        String   s=(String)_t.getTransferData(DtLib.stringFlavor);
        String[] a=FuLib.split(s,'\n',false,true);
        r=new DtFilesSelection(a);
        //FuLog.debug("DFS: R="+r.size()+" A="+a.length);
        if(r.size()!=a.length) r=null;
      }
    }
    catch(UnsupportedFlavorException ufex) { }
    catch(IOException ioex) { }

    return r;
  }
}
