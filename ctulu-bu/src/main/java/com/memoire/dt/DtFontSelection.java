/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         DtFontSelection.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.dt;

import com.memoire.fu.FuLib;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;

/**
 * A container for a single font.
 * Usefull to simplify data transfers.
 */
public final class DtFontSelection
  implements Transferable, Serializable
{
  //private static final long serialVersionUID=7056300990833125785L;

  private int   action_=DtLib.MOVE;
  private Font  data_  ;

  public DtFontSelection(Font _data)
  {
    data_=_data;
  }

  public int getAction()
  {
    return action_;
  }

  public void setAction(int _action)
  {
    action_=_action;
  }

  public Font getFont()
  {
    return data_;
  }

  public String getString()
  {
    if(data_==null) return null;

    StringBuffer sb=new StringBuffer(128);
    sb.append(data_.getFamily());
    sb.append(',');
    if(data_.isBold())   sb.append("bold");
    if(data_.isItalic()) sb.append("italic");
    sb.append(',');
    sb.append(data_.getSize());
    return sb.toString();
  }

  public String toString()
  {
    return data_==null ? "no font" : "one font";
  }

  // Transferable

  @Override
  public Object getTransferData(DataFlavor _flavor)
    throws UnsupportedFlavorException
  {
    if(data_!=null)
    {
      if(DtLib.fontFlavor.equals(_flavor))
        return getFont();
      else
      if(DtLib.stringFlavor.equals(_flavor))
        return getString();
    }
    throw new UnsupportedFlavorException(_flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors()
  {
    return FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor _flavor)
  {
    return (data_!=null)&&DtLib.matches(_flavor,FLAVORS);
  }

  private static final DataFlavor[] FLAVORS=
  {
    DtLib.fontFlavor,
    DtLib.stringFlavor
  };

  public static final boolean canConvert(Transferable _t)
  {
    return DtLib.matches(_t,FLAVORS);
  }

  public static final boolean canConvert(DataFlavor[] _flavors)
  {
    return DtLib.matches(_flavors,FLAVORS);
  }

  public static final DtFontSelection convert(Transferable _t)
  {
    DtFontSelection r=null;

    try
    {
      if(_t.isDataFlavorSupported(DtLib.fontFlavor))
      {
        //FuLog.debug("DCS: fontFlavor");
        r=new DtFontSelection((Font)_t.getTransferData(DtLib.fontFlavor));
      }
      else
      if(_t.isDataFlavorSupported(DtLib.stringFlavor))
      {
        //FuLog.debug("DCS: stringFlavor");
        String s=(String)_t.getTransferData(DtLib.stringFlavor);
        try
        {
          String[] a=FuLib.split(s,',');
          if(a.length!=3) throw new IllegalArgumentException();

          String f=a[0];
          int    h=Integer.parseInt(a[2]);

          int t;
               if(a[1].equals(""          )) t=Font.PLAIN;
          else if(a[1].equals("bold"      )) t=Font.BOLD;
          else if(a[1].equals("italic"    )) t=Font.ITALIC;
          else if(a[1].equals("bolditalic")) t=Font.BOLD|Font.ITALIC;
          else if(a[1].equals("italicbold")) t=Font.BOLD|Font.ITALIC;
          else throw new IllegalArgumentException();

          r=new DtFontSelection(new Font(f,t,h));
        }
        catch(Exception ex) { }
      }
    }
    catch(UnsupportedFlavorException ufex) { }
    catch(IOException ioex) { }

    return r;
  }
}
