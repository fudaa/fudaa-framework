/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuProgressBar.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import javax.swing.JProgressBar;

/**
 * A progress bar which displays the value as a number.
 */
public class BuProgressBar
  extends JProgressBar
{
  public BuProgressBar()
  {
    if(!BuLib.isLiquid())
      setSize(getPreferredSize());
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public String getString()
  {
    String r=super.getString();
    if(r==null)
    {
      int v   =getValue();
      int vmin=getMinimum();
      int vmax=getMaximum();

      if((vmax>vmin)&&(v>vmin))
      {
        if((vmin==0)&&(vmax==100))
          r=((v-vmin)*100/(vmax-vmin))+"%";
        else
        r=""+v;
      }
    }
    return r;
  }

  /*
  public void  paintComponent(Graphics _g)
  {
    Insets      insets=getInsets();
    Dimension   d     =getSize();
    String      s     ="";

    int v   =getValue();
    int vmin=getMinimum();
    int vmax=getMaximum();

    super.paintComponent(_g);

    if((vmax>vmin)&&(v>vmin))
    {
      Font        f     =getFont();
      FontMetrics fm    =_g.getFontMetrics(f);

      if((vmin==0)&&(vmax==100))
        s=((v-vmin)*100/(vmax-vmin))+"%";
      else
        s=""+v;

      int ws=fm.stringWidth(s);
      int hs=fm.getAscent()+fm.getDescent();
      int xs=insets.left+
	(d.width-insets.left-insets.right-ws)/2;
      int ys=insets.top+
	((d.height-insets.top-insets.bottom)+hs)/2-2;

      int wb=getWidth ()-insets.left-insets.right;
      int hb=getHeight()-insets.top-insets.bottom;
      int xb=(v-vmin)*wb/(vmax-vmin);

      Shape old=_g.getClip();

      _g.clipRect(insets.left+xb,insets.top,wb-xb,hb);
      Color fg=UIManager.getColor("ProgressBar.selectionForeground");
      if((fg==null)||fg.equals(getBackground())) fg=getForeground();
      _g.setColor(fg);
      _g.drawString(s,xs,ys);
      _g.setClip(old);

      _g.clipRect(insets.left,insets.top,xb,hb);
      Color bg=UIManager.getColor("ProgressBar.selectionBackground");
      if(bg==null) bg=getBackground();
      _g.setColor(bg);
      _g.drawString(s,xs,ys);
      _g.setClip(old);
    }
  }
  */
}
