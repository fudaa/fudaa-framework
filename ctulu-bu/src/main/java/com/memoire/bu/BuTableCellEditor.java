/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut unstable
 * @file BuTableCellEditor.java
 * @version 0.43
 * @author Axel von Arnim
 * @email axel.vonarnim@equipement.gouv.fr
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

/**
 * An editor for table cells, which controls input through input validators like date or integer validators.
 *
 * @author Axel von Arnim, axel.vonarnim@equipement.gouv.fr
 */
public class BuTableCellEditor
        implements TableCellEditor {

  Object oldValue_;
  BuTextField tf_;
  Vector listeners_;
  boolean editOnDblClick_=false;

  public BuTableCellEditor(BuTextField _tf) {
    if (_tf == null) {
      throw new IllegalArgumentException /*System.err.println*/("BuTableCellEditor: param _tf is null in the constructor");
    }

    tf_ = _tf;
    listeners_ = new Vector(1, 1);
  }
  
  public BuTableCellEditor(BuTextField _tf, boolean _editOnDblClick) {
    this(_tf);
    editOnDblClick_=_editOnDblClick;
  }

  public void setSelectAll(boolean selectAll) {
    tf_.setSelectAllIsGainFocus(selectAll);
  }

  @Override
  public Component getTableCellEditorComponent(JTable _table, Object _value, boolean _selected, int _row, int _column) {
    oldValue_ = _value;
    tf_.setValue(_value);
    if (tf_.isSelectAllIsGainFocus()) {
      tf_.selectAll();
    }
    return tf_;
  }

  @Override
  public void addCellEditorListener(CellEditorListener _l) {
    if (!listeners_.contains(_l)) {
      listeners_.addElement(_l);
    }
  }

  @Override
  public void cancelCellEditing() {
    tf_.setValue(oldValue_);
    ChangeEvent evt = new ChangeEvent(this);
    for (Enumeration e = listeners_.elements(); e.hasMoreElements();) {
      ((CellEditorListener) e.nextElement()).editingCanceled(evt);
    }
  }

  @Override
  public Object getCellEditorValue() {
    return tf_.getValue();
  }

  @Override
  public boolean isCellEditable(EventObject _evt) {
    if (!editOnDblClick_)
      return true;
    
    if (_evt instanceof MouseEvent) {
      return ((MouseEvent) _evt).getClickCount()>=2;
    }
    return true;
  }

  @Override
  public void removeCellEditorListener(CellEditorListener _l) {
    listeners_.removeElement(_l);
  }

  @Override
  public boolean shouldSelectCell(EventObject _evt) {
    return true;
  }

  @Override
  public boolean stopCellEditing() {
    ChangeEvent evt = new ChangeEvent(this);
    for (Enumeration e = listeners_.elements(); e.hasMoreElements();) {
      ((CellEditorListener) e.nextElement()).editingStopped(evt);
    }
    return false;
  }
}
