/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuFontChooser.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtDragSupport;
import com.memoire.dt.DtFontHandler;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * A simple font chooser.
 */
public class BuFontChooser
  extends BuPanel
  implements ActionListener, ListSelectionListener, DtFontHandler.IE
{
  public static final int UNDERLINED   =1<< 1;
  public static final int CROSSED      =1<< 2;
  public static final int SMALL_CAPS   =1<< 3;
  public static final int CONDENSED    =1<< 4;
  public static final int EXPANDED     =1<< 5;
  public static final int TRANSPARENT  =1<< 7;

  public static final int SHADOW_CENTER=1<< 8;
  public static final int SHADOW_SE1   =1<< 9;
  public static final int SHADOW_SW1   =1<<10;
  public static final int SHADOW_NE1   =1<<11;
  public static final int SHADOW_NW1   =1<<12;
  public static final int EDGE_E1      =1<<13;
  public static final int EDGE_W1      =1<<14;

  public static final int LIGHT_CENTER =1<<16;
  public static final int LIGHT_SE1    =1<<17;
  public static final int LIGHT_SW1    =1<<18;
  public static final int LIGHT_NE1    =1<<19;
  public static final int LIGHT_NW1    =1<<20;

  public static final int SHADOW    =SHADOW_SE1;
  public static final int EXTRA_BOLD=EDGE_E1|EDGE_W1;
  public static final int AURA      =SHADOW_SW1|LIGHT_SE1|SHADOW_NE1|LIGHT_NW1;
  public static final int EMBOSS    =SHADOW_SE1|LIGHT_NW1;
  public static final int ENGRAVE   =LIGHT_SE1|SHADOW_NW1;
  public static final int BLUR      =SHADOW_SE1|SHADOW_SW1|SHADOW_NE1|
                                     SHADOW_NW1;

  private static final int EFFECTS=0x00FFFF00;

  private static final String[] FAMILLES=new String[]
    {"SansSerif","Serif","Monospaced","Dialog", "DialogInput" };

  private static final String[] TAILLES =new String[]
    {  "4", "5", "6", "7", "8", "9",
      "10","11","12","13","14","15","16","17","18","19",
      "20","22","24","26","28","32","36","48","64","72" };


  protected BuButton      btValider_;
  protected BuButton      btAnnuler_;
  protected BuEmptyList   ltFamille_;
  protected BuEmptyList   ltTaille_;

  protected BuCheckBox    cbBold_;
  protected BuCheckBox    cbItalic_;
  protected BuCheckBox    cbUnderlined_;
  protected BuCheckBox    cbCrossed_;
  protected BuCheckBox    cbSmallcaps_;
  protected BuCheckBox    cbCondensed_;
  protected BuCheckBox    cbExpanded_;
  protected BuCheckBox    cbTransparent_;

  protected BuRadioButton rbNoEffect_;
  protected BuRadioButton rbShadow_;
  protected BuRadioButton rbExtrabold_;
  protected BuRadioButton rbAura_;
  protected BuRadioButton rbEmboss_;
  protected BuRadioButton rbEngrave_;
  protected BuRadioButton rbBlur_;

  protected BuPanel /*BuTextField*/ tfExemple_;

  boolean styled_,native_;
  private Color   bg_;

  public BuFontChooser()
  {
    this(BuLib.DEFAULT_FONT,false,0,false);
  }

  public BuFontChooser(Font _value)
  {
    this(_value,false,0,false);
  }

  public BuFontChooser(Font    _value,
		       boolean _styled,
		       int     _style,
		       boolean _native)
  {
    build();
    setBorder(UIManager.getBorder("ColorChooser.border"));
    setValue (_value);
    setStyled(_styled);
    setStyle (_style);
    setNative(_native);
  }

  public boolean isStyled()
  {
    return styled_;
  }

  public void setStyled(boolean _styled)
  {
    if(styled_==_styled) return;

    styled_=_styled;
    bg_=tfExemple_.getBackground();
    bg_=new Color(bg_.getRed(),bg_.getGreen(),bg_.getBlue());
    if(styled_) tfExemple_.setBackground(bg_);

    cbUnderlined_ .setVisible(styled_);
    cbCrossed_    .setVisible(styled_);
    cbSmallcaps_  .setVisible(styled_);
    cbCondensed_  .setVisible(styled_);
    cbExpanded_   .setVisible(styled_);
    cbTransparent_.setVisible(styled_);
    
    rbNoEffect_   .setVisible(styled_);
    rbShadow_     .setVisible(styled_);
    rbExtrabold_  .setVisible(styled_);
    rbAura_       .setVisible(styled_);
    rbEmboss_     .setVisible(styled_);
    rbEngrave_    .setVisible(styled_);
    rbBlur_       .setVisible(styled_);
    
    //tfExemple_.setEditable(false);
    tfExemple_.setTransferHandler(new DtFontHandler(this));
    DtDragSupport.install(tfExemple_);
  }

  @Override
  public Font exportFont() { return getValue(); }
  @Override
  public void importFont(Font _font) { setValue(_font); }

  public boolean isNative()
  {
    return native_;
  }

  public void setNative(boolean _native)
  {
    if(native_==_native) return;

    String[] fonts=null;

    if(_native)
    {
      try
      {
	GraphicsEnvironment ge=GraphicsEnvironment.
          getLocalGraphicsEnvironment();
	fonts=ge.getAvailableFontFamilyNames();
      }
      catch(NoClassDefFoundError ex) { }
    }

    if(fonts==null) fonts=FAMILLES;
    ltFamille_.setListData(fonts);

    String f=value_.getFamily();//getName();
    String s=""+value_.getSize();
    if(f==null) { f="SansSerif"; s="12"; }
    ltFamille_.setSelectedValue(f,true);
    ltTaille_ .setSelectedValue(s,true);
  }

  protected void build()
  {
    ltFamille_=new BuEmptyList();
    ltTaille_ =new BuEmptyList();
    ltFamille_.setListData(FAMILLES);
    ltTaille_ .setListData(TAILLES );
    ltFamille_.addListSelectionListener(this);
    ltTaille_ .addListSelectionListener(this);

    cbBold_       =new BuCheckBox("Bold"       );
    cbItalic_     =new BuCheckBox("Italic"     );
    cbUnderlined_ =new BuCheckBox("Underlined" );
    cbCrossed_    =new BuCheckBox("Crossed"    );
    cbSmallcaps_  =new BuCheckBox("Small caps" );
    cbCondensed_  =new BuCheckBox("Condensed"  );
    cbExpanded_   =new BuCheckBox("Expanded"   );
    cbTransparent_=new BuCheckBox("Transparent");

    cbSmallcaps_.setEnabled(false);

    rbNoEffect_  =new BuRadioButton("None"      );
    rbShadow_    =new BuRadioButton("Shadow"    );
    rbExtrabold_ =new BuRadioButton("Extra Bold");
    rbAura_      =new BuRadioButton("Aura"      );
    rbEmboss_    =new BuRadioButton("Emboss"    );
    rbEngrave_   =new BuRadioButton("Engrave"   );
    rbBlur_      =new BuRadioButton("Blur"      );

    ButtonGroup bg=new ButtonGroup();
    bg.add(rbNoEffect_  );
    bg.add(rbShadow_    );
    bg.add(rbExtrabold_ );
    bg.add(rbAura_      );
    bg.add(rbEmboss_    );
    bg.add(rbEngrave_   );
    bg.add(rbBlur_      );

    cbBold_       .addActionListener(this);
    cbItalic_     .addActionListener(this);
    cbUnderlined_ .addActionListener(this);
    cbCrossed_    .addActionListener(this);
    cbSmallcaps_  .addActionListener(this);
    cbCondensed_  .addActionListener(this);
    cbExpanded_   .addActionListener(this);
    cbTransparent_.addActionListener(this);

    rbNoEffect_  .addActionListener(this);
    rbShadow_    .addActionListener(this);
    rbExtrabold_ .addActionListener(this);
    rbAura_      .addActionListener(this);
    rbEmboss_    .addActionListener(this);
    rbEngrave_   .addActionListener(this);
    rbBlur_      .addActionListener(this);

    tfExemple_=new BuPanel()
      {
	@Override
  public void paintComponent(Graphics _g)
	{
	  super.paintComponent(_g);
	  if(styled_)
	  {
	    Insets      insets=getInsets();
	    //FontMetrics fm    =_g.getFontMetrics();

	    int h=getHeight()-insets.top-insets.bottom;
   
	    _g.setColor(getForeground());
	    drawStringWithStyle
              (_g,"ABC abc 123 ;.-+",
               insets.left+h/10,
               insets.top+h*9/10,
               style_,getBackground());
	  }
	}
      };
    tfExemple_.putClientProperty("SLAF_NO_TEXTURE",Boolean.TRUE);
    tfExemple_.setOpaque(true);
    tfExemple_.setBackground(UIManager.getColor("TextArea.background"));
    tfExemple_.setForeground(UIManager.getColor("TextArea.foreground"));
    tfExemple_.setPreferredSize(new Dimension(400,72));

    BuPanel pnExemple=new BuPanel(new BuBorderLayout());
    pnExemple.setBorder(new CompoundBorder
                        (new BuTitledBorder(getS("Prévisualisation")),
                         BuLib.getEmptyBorder(5)));
    pnExemple.add(tfExemple_,BuBorderLayout.CENTER);

    setLayout(new BuFormLayout(5,5,0,8));
    add(new BuScrollPane(ltFamille_),BuFormLayout.constraint(0,0,1,9));
    add(new BuScrollPane(ltTaille_ ),BuFormLayout.constraint(3,0,1,9));

    add(cbBold_       ,BuFormLayout.constraint(1,0));
    add(cbItalic_     ,BuFormLayout.constraint(1,1));
    add(cbUnderlined_ ,BuFormLayout.constraint(1,2));
    add(cbCrossed_    ,BuFormLayout.constraint(1,3));
    add(cbSmallcaps_  ,BuFormLayout.constraint(1,4));
    add(cbCondensed_  ,BuFormLayout.constraint(1,5));
    add(cbExpanded_   ,BuFormLayout.constraint(1,6));
    add(cbTransparent_,BuFormLayout.constraint(1,7));

    add(rbNoEffect_   ,BuFormLayout.constraint(2,0));
    add(rbShadow_     ,BuFormLayout.constraint(2,1));
    add(rbExtrabold_  ,BuFormLayout.constraint(2,2));
    add(rbAura_       ,BuFormLayout.constraint(2,3));
    add(rbEmboss_     ,BuFormLayout.constraint(2,4));
    add(rbEngrave_    ,BuFormLayout.constraint(2,5));
    add(rbBlur_       ,BuFormLayout.constraint(2,6));

    add(pnExemple     ,BuFormLayout.constraint(0,9,4,1));
  }

  private final Font value()
  {
    Font r=null;

    try
    {
      r=new Font(""+ltFamille_.getSelectedValue(),
		 Font.PLAIN
		 | (cbBold_    .isSelected() ? Font.BOLD : 0)
		 | (cbItalic_.isSelected() ? Font.ITALIC : 0),
		 Integer.parseInt(""+ltTaille_.getSelectedValue()));
    }
    catch(Exception ex) { }

    return r;
  }

  private final int style()
  {
    int r=0;

    if(cbUnderlined_ .isSelected()) r|=UNDERLINED;
    if(cbCrossed_    .isSelected()) r|=CROSSED;
    if(cbSmallcaps_  .isSelected()) r|=SMALL_CAPS;
    if(cbCondensed_  .isSelected()) r|=CONDENSED;
    if(cbExpanded_   .isSelected()) r|=EXPANDED;
    if(cbTransparent_.isSelected()) r|=TRANSPARENT;

    if(rbShadow_     .isSelected()) r|=SHADOW;
    if(rbExtrabold_  .isSelected()) r|=EXTRA_BOLD;
    if(rbAura_       .isSelected()) r|=AURA;
    if(rbEmboss_     .isSelected()) r|=EMBOSS;
    if(rbEngrave_    .isSelected()) r|=ENGRAVE;
    if(rbBlur_       .isSelected()) r|=BLUR;
    
    return r;
  }

  @Override
  public void valueChanged(ListSelectionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();

    if((source==ltFamille_)||(source==ltTaille_))
    {
      value_=value();
      tfExemple_.setFont(value_);
    }
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    //JComponent source=(JComponent)_evt.getSource();
    //System.err.println("BFC: font is "+value_);

    //if((source==cbBold_)||(source==cbItalic_))
    {
      value_=value();
      style_=style();

      if(rbEmboss_.isSelected()||rbEngrave_.isSelected()||rbAura_.isSelected())
	tfExemple_.setBackground(Color.lightGray);
      else
	tfExemple_.setBackground(bg_);

      tfExemple_.setFont(value_);
    }
  }

  private Font value_;

  public Font getValue()
  { return value_; }

  public void setValue(Font _value)
  {
    Font value=_value;
    value_=value;
    if(value==null) value=BuLib.DEFAULT_FONT;
    tfExemple_.setFont(value);
    ltFamille_.setSelectedValue(value.getFamily(),true);
    ltTaille_.setSelectedValue(""+value.getSize(),true);
    cbBold_.setSelected(value.isBold());
    cbItalic_.setSelected(value.isItalic());
  }

  int style_;

  public int getStyle()
  { return style_; }

  public void setStyle(int _style)
  {
    int style=_style;
    style_=style;

    if((style&(CONDENSED|EXPANDED))==(CONDENSED|EXPANDED))
      style^=(CONDENSED|EXPANDED);

    tfExemple_.setIgnoreRepaint(true);

    cbUnderlined_ .setSelected((style&UNDERLINED )!=0);
    cbCrossed_    .setSelected((style&CROSSED    )!=0);
    cbSmallcaps_  .setSelected((style&SMALL_CAPS )!=0);
    cbCondensed_  .setSelected((style&CONDENSED  )!=0);
    cbExpanded_   .setSelected((style&EXPANDED   )!=0);
    cbTransparent_.setSelected((style&TRANSPARENT)!=0);

    rbShadow_     .setSelected((style&EFFECTS)==SHADOW);
    rbExtrabold_  .setSelected((style&EFFECTS)==EXTRA_BOLD);
    rbAura_       .setSelected((style&EFFECTS)==AURA);
    rbEmboss_     .setSelected((style&EFFECTS)==EMBOSS);
    rbEngrave_    .setSelected((style&EFFECTS)==ENGRAVE);
    rbBlur_       .setSelected((style&EFFECTS)==BLUR);

    tfExemple_.setIgnoreRepaint(false);
    tfExemple_.repaint();
  }

  public static final int stringWidthWithStyle
    (FontMetrics _fm, String _s, int _style)
  {
    int style=_style;
    if((style&(CONDENSED|EXPANDED))==(CONDENSED|EXPANDED))
      style^=(CONDENSED|EXPANDED);

    int w=_fm.stringWidth(_s);

    if((style&(LIGHT_SE1|LIGHT_NE1|SHADOW_SE1|SHADOW_NE1))!=0) w++;
    if((style&(LIGHT_SW1|LIGHT_NW1|SHADOW_SW1|SHADOW_NW1))!=0) w++;

    if((style&CONDENSED)!=0) w=Math.round(w*0.70f);
    if((style&EXPANDED )!=0) w=Math.round(w*1.43f);

    return w;
  }

  public static final void drawStringWithStyle
    (Graphics _g2, String _s, int _x, int _y, int _style, Color _bg)
  {
    int style=_style;
    if((style&(CONDENSED|EXPANDED))==(CONDENSED|EXPANDED))
      style^=(CONDENSED|EXPANDED);
    Graphics gTmp=_g2;
    if(style==0)
    {
      gTmp.drawString(_s,_x,_y);
      return;
    }
    int x=_x;
    int y=_y;

    if((style&(CONDENSED|EXPANDED))!=0)
    {
      gTmp=gTmp.create();
      gTmp.translate(x,y);
      if(gTmp instanceof Graphics2D)
      {
        if((style&CONDENSED)!=0) ((Graphics2D)gTmp).scale(0.70,1.);
        if((style&EXPANDED )!=0) ((Graphics2D)gTmp).scale(1.43,1.);
      }
      x=y=0;
    }

    final Color       OLD  =gTmp.getColor();
    final FontMetrics FM   =gTmp.getFontMetrics();
    final int         SZ   =1;

    gTmp.setColor(Color.white);
    if((style&LIGHT_CENTER)!=0) gTmp.drawString(_s,x  ,y  );
    if((style&LIGHT_SE1   )!=0) gTmp.drawString(_s,x+SZ,y+SZ);
    if((style&LIGHT_SW1   )!=0) gTmp.drawString(_s,x-SZ,y+SZ);
    if((style&LIGHT_NE1   )!=0) gTmp.drawString(_s,x+SZ,y-SZ);
    if((style&LIGHT_NW1   )!=0) gTmp.drawString(_s,x-SZ,y-SZ);

    gTmp.setColor(Color.gray);
    if((style&SHADOW_CENTER)!=0) gTmp.drawString(_s,x  ,y  );
    if((style&SHADOW_SE1   )!=0) gTmp.drawString(_s,x+SZ,y+SZ);
    if((style&SHADOW_SW1   )!=0) gTmp.drawString(_s,x-SZ,y+SZ);
    if((style&SHADOW_NE1   )!=0) gTmp.drawString(_s,x+SZ,y-SZ);
    if((style&SHADOW_NW1   )!=0) gTmp.drawString(_s,x-SZ,y-SZ);

    gTmp.setColor(_bg);
    if((style&TRANSPARENT  )!=0) gTmp.drawString(_s,x,y);

    gTmp.setColor(OLD);
    if((style&TRANSPARENT  )==0)
    {
      gTmp.drawString(_s,x  ,y);
      if((style&EDGE_E1      )!=0) gTmp.drawString(_s,x+SZ,y);
      if((style&EDGE_W1      )!=0) gTmp.drawString(_s,x-SZ,y);
    }

    if((style&(UNDERLINED|CROSSED))!=0)
    {
      int w=FM.stringWidth(_s);
      int h=FM.getAscent()/3;
      int d=1+Math.max(0,(FM.getAscent()*20/19)/18);
      //System.err.println("BFC: ascent="+FM.getAscent()+" d="+d);

      for(int i=0;i<d;i++)
      {
	if((style&UNDERLINED)!=0)
	  gTmp.drawLine(x,y+2+i,x+w-1,y+2+i);
	if((style&CROSSED   )!=0)
	  gTmp.drawLine(x,y-h-d/2+i,x+w-1,y-h-d/2+i);
      }
    }

    if((style&(CONDENSED|EXPANDED))!=0)
    {
      gTmp.dispose();
    }
  }

  public static void main(String[] _args)
  {
    Font v=new Font("Flubber",Font.PLAIN,18);
    BuFontChooser p=new BuFontChooser(v,true,0,true);
    //p.setDragEnabled(true);
    p.setBorder(BuLib.getEmptyBorder(5));
    JFrame f=new JFrame("Test Font Chooser");
    f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    f.getContentPane().add(p);
    f.pack();
    f.setVisible(true);
  }
}
