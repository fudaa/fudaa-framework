/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuLabelLink.java
 * @version      0.41
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtDragSupport;
import com.memoire.dt.DtFileHandler;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Simple Link.
 */
public class BuLabelLink
  extends BuLabelMultiLine
  implements MouseListener
{
  protected URL            url_;
  protected ActionListener listener_;
  protected boolean        rollover_,wholeMode_;
 
  public BuLabelLink()
  {
    this(null,null,null);
  }

  public BuLabelLink(String _text)
  {
    this(_text,null,null);
  }

  public BuLabelLink(String _text, String _url, ActionListener _listener)
  {
    super(_text);
    listener_=_listener;
    setURL(_url);

//    if(FuLib.jdk()>=1.4)
//    {
      setFocusable(true);
      setFocusTraversalKeysEnabled(true);
//    }
  }

  /*
  public boolean isFocusTraversable()
  {
    return true;
  }
  */

  public boolean isWholeMode()
  {
    return wholeMode_;
  }

  public void setWholeMode(boolean _wholeMode)
  {
    if(wholeMode_!=_wholeMode)
    {
      wholeMode_=_wholeMode;
      updateSize();
    }
  }

  public void setURL(String _url)
  {
    if(_url!=null)
    {
      try { url_=new URL(_url); }
      catch(MalformedURLException ex) { url_=null; }
    }
    else url_=null;

    //DndSource.remove(this);
    setCursor(null);
    removeMouseListener(this);
    putClientProperty("DT_DRAG_VALUE",null);

    if(url_!=null)
    {
      if(listener_!=null)
      {
	setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	addMouseListener(this);
      }

      /*
      new DndSource(new DndRequestData()
      {
	public Object[] request(JComponent _component,Point _location)
	{
	  return new Object[] { url_, url_.toString() };
	}
      }).add(this);
      */
      putClientProperty("DT_DRAG_VALUE",url_);
      setTransferHandler(DtFileHandler.getInstance());
      DtDragSupport.install(this);
    }
  }

  @Override
  public void mousePressed(MouseEvent _evt)
  { }

  @Override
  public void mouseReleased(MouseEvent _evt)
  { }

  @Override
  public void mouseEntered(MouseEvent _evt)
  {
    if(!rollover_) { rollover_=true; repaint(); }
  }

  @Override
  public void mouseExited(MouseEvent _evt)
  {
    if(rollover_) { rollover_=false; repaint(); }
  }

  @Override
  public void mouseClicked(MouseEvent _evt)
  {
    if(!BuLib.isLeft(_evt)) return;

    String action="OUVRIR";
    if(BuLib.isShift(_evt)) action="ENREGISTER";
    if(BuLib.isCtrl (_evt)) action="VOIR";
    if(BuLib.isAlt  (_evt)) action="EDITER";

    listener_.actionPerformed
      (new ActionEvent
       (this,ActionEvent.ACTION_PERFORMED,action+"("+url_+")"));
  }

  @Override
  protected final void drawSingleLine(Graphics _g, String _s,
                                      int _x, int _y, int _w)
  {
    Color   old=null;
    boolean hla=(rollover_&&(url_!=null)&&(listener_!=null)&&
		 (wholeMode_||_s.trim().equals(url_.toString())));

    if(hla)
    {
      old=_g.getColor();
      BuLib.setColor(_g,Color.blue);
    }

    super.drawSingleLine(_g,_s,_x,_y,_w);

    if((hla&&!isUnderlined())||hasFocus())
      _g.drawLine(_x,_y+2,_x+_w-1,_y+2);

    if(hla)
    {
      _g.setColor(old);
    }
  }
}
