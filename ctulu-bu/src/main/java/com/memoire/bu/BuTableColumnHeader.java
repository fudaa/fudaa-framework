/**
 * @modification $Date: 2007-04-12 16:15:46 $
 * @statut       unstable
 * @file         BuTableColumnHeader.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class BuTableColumnHeader
  extends JTableHeader
{
  private static final int[] vtax=new int[] { 3,0,6 };
  private static final int[] vtay=new int[] { 0,6,6 };
  private static final int[] vbax=new int[] { 3,6,0 };
  private static final int[] vbay=new int[] { 6,0,0 };

  Hashtable pw_=new Hashtable(11);
  

  public BuTableColumnHeader(TableColumnModel _model)
  {
    super(_model);

    addMouseListener(new MouseAdapter()
      {
        @Override
        public void mouseClicked(MouseEvent _evt)
        {
          if(BuLib.isMiddle(_evt))
          {
            if(_evt.getClickCount()!=1) return;

            final JTable t=getTable();
            int c=t.columnAtPoint(new Point(_evt.getX(),_evt.getY()));
            if(c==-1) return;
            //Fred: identifieur non sur
            //TableColumn column=t.getColumn(t.getColumnName(c));
            TableColumn column=getColumnModel().getColumn(c);

            int ps=column.getPreferredWidth();
            //System.err.println("Col "+c+" ps="+ps+" cw="+column.getWidth());
            if(column.getWidth()==ps)
              ps=computePreferredWidth(t,column,c);
            //System.err.println("setting "+ps);
            column.setPreferredWidth(ps);
            //t.doLayout();
          }
          else
          if(BuLib.isRight(_evt))
          {
            final JTable t=getTable();
            int c=t.columnAtPoint(new Point(_evt.getX(),_evt.getY()));

            BuPopupMenu pm=new BuPopupMenu("Columns");
            boolean first=false;
            if(c!=-1)
            {
              pm.addMenuItem("Cacher","COLONNE_"+c,
                             BuResource.BU.getMenuIcon("cacher"),true);
              first=true;
            }
            //TableC
            TableColumnModel cols=getColumnModel();
            for(int i=0;i<t.getColumnCount();i++)
            {
              String n=t.getColumnName(i);
              //Fred: une exception est levee ici: il se peut que la colonne n'est pas le bon identifieur
              //t.getColumnModel().getColumn(columnIndex)
              //int    w=t.getColumn(n).getWidth();
              int w=cols.getColumn(i).getWidth();
              //if(w==0)
              //{
              if(first) pm.addSeparator();
              first=false;
              pm.addCheckBox(n,"COLONNE_"+i,null,true,(w!=0));
              //}
            }
            pm.addActionListener(new ActionListener()
              {
                @Override
                public void actionPerformed(ActionEvent _acEvt)
                {
                  int i=Integer.parseInt(_acEvt.getActionCommand().substring(8));
                  //String n=t.getColumnName(i);
                  //Fred identifieur dangereux
                  //TableColumn column=t.getColumn(n);
                  TableColumn column=getColumnModel().getColumn(i);
                  int w=column.getWidth();
                  if(w==0)
                  {
                    column.setMaxWidth(Integer.MAX_VALUE);
                    Integer j=(Integer)pw_.get(column);
                    w=(j!=null) ? j.intValue() : computePreferredWidth(t,column,i);
                    column.setPreferredWidth(w);
                  }
                  else
                  {
                    pw_.put(column,FuFactoryInteger.get(w));
                    column.setMaxWidth(0);
                  }
                }
              });

            int w=pm.getPreferredSize().width;
            int x=_evt.getX()-w/2;
            int y=BuTableColumnHeader.this.getHeight()-1;
            if(c!=-1)
            {
              Rectangle hr=getHeaderRect(c);
              if(x+w>hr.x+hr.width) x=hr.x+hr.width-w;
              if(x<hr.x) x=hr.x;
            }
            pm.show(BuTableColumnHeader.this,x,y);
          }
        }
      });
  }

  public void hideColumn(TableColumn _column)
  {
    int w=_column.getWidth();
    if(w!=0)
    {
      pw_.put(_column,FuFactoryInteger.get(w));
      _column.setMaxWidth(0);
    }
  }

  public void showColumn(TableColumn _column)
  {
    int w=_column.getWidth();
    if(w==0)
    {
      _column.setMaxWidth(Integer.MAX_VALUE);
      Integer j=(Integer)pw_.get(_column);
      JTable  t=getTable();
      int     i=t.convertColumnIndexToView(_column.getModelIndex());
      w=(j!=null) ? j.intValue() : computePreferredWidth(getTable(),_column,i);
      _column.setPreferredWidth(w);
    }
  }

  public void adjustWidth(TableColumn _column)
  {
    int w=_column.getWidth();
    if(w!=0)
    {
      JTable t=getTable();
      int    i=t.convertColumnIndexToView(_column.getModelIndex());
      w=computePreferredWidth(getTable(),_column,i);
      _column.setMaxWidth(Integer.MAX_VALUE);
      _column.setPreferredWidth(w);
    }
  }

  /*
  private int pw_=-1;

  public int getPreferredWidth()
  {
    return (pw_>=0) ? pw_ : super.getPreferredWidth();
  }

  public void setPreferredWidth(int _pw)
  {
    pw_=_pw;
  }
  */

  public int computePreferredWidth(int _indexInView)
  {
    JTable      t=getTable();
    //fred: identifieur non sur
    //TableColumn c=t.getColumn(t.getColumnName(_indexInView));
    TableColumn c=getColumnModel().getColumn(_indexInView);
    return computePreferredWidth(t,c,_indexInView);
  }

  int computePreferredWidth(JTable _t,TableColumn _col,int _civ)
  {
    int r=0;

    TableCellRenderer thr=_col.getHeaderRenderer();
    /*GCJ-BEGIN*/
    if(thr==null) thr=getDefaultRenderer();
    /*GCJ-END*/
    if(thr!=null)
      r=thr.getTableCellRendererComponent
        (_t,_col.getHeaderValue(),
         false,false,-1,_civ).getPreferredSize().width;
    
    if(_t.getModel() instanceof BuTableSortModel) r+=18;

    TableCellRenderer tcr=_col.getCellRenderer();
    if(tcr==null) tcr=_t.getDefaultRenderer(null);
    if(tcr!=null)
      for(int i=0;i<_t.getRowCount();i++)
        r=Math.max(r,tcr.getTableCellRendererComponent
                   (_t,_t.getValueAt(i,_civ),
                    false,false,i,_civ).getPreferredSize().width);

    r+=6;
    return r;
  }

  /*
  protected int findColumn(int _c)
  {
    int    r=-1;
    String n=m.getColumnName(_c);
    if(n!=null)
    {
      JTable t=getTable();
      if(t!=null)
        for(int i=t.getColumnCount()-1;i>=0;i--)
          if(n.equals(t.getColumnName(i))) { r=i; break; }
    }
    return r;
  }
  */

  @Override
  public void paintComponent(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paintComponent(_g);

    JTable t=getTable();
    if((t!=null)&&(t.getModel() instanceof BuTableSortModel))
    {
      BuTableSortModel m=(BuTableSortModel)t.getModel();
      Color old=_g.getColor();
      int[] scs=m.getSortingColumns();
      if(scs.length>0)
      {
        for(int j=0;j<scs.length;j++)
        {
          int cim=scs[j];
          if(cim>=0)
          {
            //System.err.println("sorting on "+cim);
            int civ=t.convertColumnIndexToView(cim);
            //System.err.println("  displayed at "+civ);
            if(civ>=0)
            {
              Rectangle r=getHeaderRect(civ);
              _g.translate(r.x+r.width-10,r.y+r.height/2-3);
              BuLib.setColor(_g,Color.white);
              if(m.isAscending(cim)) _g.fillPolygon(vbax,vbay,3);
              else                   _g.fillPolygon(vtax,vtay,3);
              BuLib.setColor(_g,(j==0) ? Color.black : Color.gray);
              if(m.isAscending(cim)) _g.drawPolygon(vbax,vbay,3);
              else                   _g.drawPolygon(vtax,vtay,3);
              _g.translate(-(r.x+r.width-10),-(r.y+r.height/2-3));
            }
          }
          _g.setColor(old);
        }
      }
    }
  }
}
 
