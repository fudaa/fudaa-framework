/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuLabel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * The Swing label...
 */
public class BuLabel
       extends JLabel
{
  public BuLabel()
  {
    this(null,null,LEFT,CENTER);
  }

  public BuLabel(String _label)
  {
    this(_label,null,LEFT,CENTER);
  }

  public BuLabel(Icon _icon)
  {
    this(null,_icon,LEFT,CENTER);
  }

  public BuLabel(String _label, int _halign)
  {
    this(_label,null,_halign,CENTER);
  }
  
  public BuLabel(Icon _icon, int _halign)
  {
    this(null,_icon,_halign,CENTER);
  }

  public BuLabel(String _label, int _halign, int _valign)
  {
    this(_label,null,_halign,_valign);
  }

  public BuLabel(Icon _icon, int _halign, int _valign)
  {
    this(null,_icon,_halign, _valign);
  }

  public BuLabel(String _label, Icon _icon)
  {
    this(_label,_icon,LEFT,CENTER);
  }

  public BuLabel(String _label, Icon _icon, int _halign)
  {
    this(_label,_icon,_halign,CENTER);
  }

  public BuLabel(String _label, Icon _icon, int _halign, int _valign)
  {
    super();

    setText(_label);
    setIcon(_icon);
    setHorizontalAlignment(_halign);
    setVerticalAlignment(_valign);
  }

  @Override
  public void setText(String _text)
  {
    super.setText((_text==null)?"":_text);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(0,getHeight()+1);
    return r;
  }
}
