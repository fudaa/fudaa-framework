/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         com/memoire/bu/BuGlobalFilter.java
 * @version      0.18
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2(GPL2)
 * @copyright    1999-2004 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.image.ColorModel;
import java.awt.image.ImageFilter;

public abstract class BuGlobalFilter
  extends ImageFilter
{
  protected static final ColorModel DEFAULT_CM=ColorModel.getRGBdefault();

  private int savedWidth,savedHeight,savedPixels[];

  @Override
  public final void setDimensions(int width, int height)
  {
    savedWidth =width;
    savedHeight=height;
    savedPixels=new int[width*height];

    consumer.setDimensions(width,height);
  }

  @Override
  public final void setColorModel(ColorModel model)
  {
    consumer.setColorModel(DEFAULT_CM);
  }

  @Override
  public final void setHints(int hintflags)
  {
    consumer.setHints(TOPDOWNLEFTRIGHT|COMPLETESCANLINES|
                      SINGLEPASS|(hintflags & SINGLEFRAME));
  }

  private void setPixels0(int x,int y,int width,int height,
                            ColorModel cm,Object pixels,
                            int offset,int scansize)
  {
    int     sourceOffset     =offset;
    int     destinationOffset=y*savedWidth+x;
    boolean bytearray        =(pixels instanceof byte[]);
    for(int yy=0;yy<height;yy++)
    {
      for(int xx=0;xx<width;xx++)
      {
        if(bytearray)
          savedPixels[destinationOffset++]=
            cm.getRGB(((byte[])pixels)[sourceOffset++]&0xff);
        else
          savedPixels[destinationOffset++]=
            cm.getRGB(((int[])pixels)[sourceOffset++]);
      }
      sourceOffset     +=(scansize  -width);
      destinationOffset+=(savedWidth-width);
    }
  }

  @Override
  public final void setPixels(int x, int y, int width, int height,
                              ColorModel cm, byte pixels[],
                              int offset, int scansize)
  {
    setPixels0(x,y,width,height,cm,pixels,offset,scansize);
  }

  @Override
  public final void setPixels(int x, int y, int width, int height,
                              ColorModel cm, int pixels[],
                              int offset, int scansize)
  {
    setPixels0(x,y,width,height,cm,pixels,offset,scansize);
  }

  @Override
  public final void imageComplete(int status)
  {
    if((status==IMAGEABORTED)||(status==IMAGEERROR))
    {
      consumer.imageComplete(status);
      return;
    }
    int pixels[]=new int[savedWidth];
    int position,sumArray[],sumIndex;

    sumArray=new int[9]; // maxsize - vs. Vector for performance
    for(int yy=0;yy<savedHeight;yy++)
    {
      position=0;
      int start=yy*savedWidth;
      for(int xx=0;xx<savedWidth;xx++)
      {
        sumIndex=0;
        //  xx     yy
        sumArray[sumIndex++]=savedPixels[start+xx];       // center center
        if(yy !=(savedHeight-1))                          // center bottom
          sumArray[sumIndex++]=savedPixels[start+xx+savedWidth];
        if(yy !=0)                                        // center top
          sumArray[sumIndex++]=savedPixels[start+xx-savedWidth];
        if(xx !=(savedWidth-1))                           // right  center
          sumArray[sumIndex++]=savedPixels[start+xx+1];
        if(xx !=0)                                        // left   center
          sumArray[sumIndex++]=savedPixels[start+xx-1];
        if((yy !=0)&&(xx !=0))                            // left   top
          sumArray[sumIndex++]=savedPixels[start+xx-savedWidth-1];
        if((yy !=(savedHeight-1))&&(xx !=(savedWidth-1))) // right  bottom
          sumArray[sumIndex++]=savedPixels[start+xx+savedWidth+1];
        if((yy !=0)&&(xx !=(savedWidth-1)))               //right  top
          sumArray[sumIndex++]=savedPixels[start+xx-savedWidth+1];
        if((yy !=(savedHeight-1))&&(xx !=0))              //left   bottom
          sumArray[sumIndex++]=savedPixels[start+xx+savedWidth-1];
        pixels[position++]=computeColor(sumArray,sumIndex);
      }
      consumer.setPixels(0,yy,savedWidth,1,DEFAULT_CM,
                         pixels,0,savedWidth);
    }
    consumer.imageComplete(status);
  }

  protected abstract int computeColor(int pixels[],int size);
}
