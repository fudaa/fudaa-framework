/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuDesktopManager.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.*;
import java.beans.PropertyVetoException;
import javax.swing.DesktopManager;
import javax.swing.FocusManager;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

/**
 * A desktop manager.
 */
public class BuDesktopManager
  //extends DefaultDesktopManager
  implements DesktopManager
{
  //private static final boolean TRACE=Bu.TRACE&&true;
  private static final boolean DEBUG=Bu.DEBUG&&true;

  private static final String  HAS_BEEN_ICONIFIED_PROPERTY="wasIconOnce";

  protected BuDesktop desktop_;
  //private   boolean   outline_;

  public BuDesktopManager(BuDesktop _desktop)
  {
    //super();
    desktop_=_desktop;

    /*
    if(FuLib.jdk()<1.2)
      outline_="outline".equals
	(desktop_.getClientProperty("JDesktopPane.dragMode"));
    else
      outline_=
	(desktop_.getDragMode()==BuDesktop.OUTLINE_DRAG_MODE);
    */
  }

  public int getWidth(Component _c)
  {
    return _c.getWidth();
//    return (FuLib.jdk()<=1.1)
//      ? _c.getSize().width
//      : _c.getWidth();
  }

  public int getHeight(Component _c)
  {
    return _c.getHeight();
//    return (FuLib.jdk()<=1.1)
//      ? _c.getSize().height
//      : _c.getHeight();
  }

  @Override
  public void openFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: openFrame()");

    desktop_.add(_f);
    desktop_.remove(_f.getDesktopIcon());
  }

  @Override
  public void closeFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: closeFrame()");
    //if(DEBUG) FuLog.printStackTrace();

    boolean selected=_f.isSelected();

    if(selected)
    {
      try { _f.setSelected(false); }
      catch (PropertyVetoException ex) { }
    }

    desktop_.remove(_f);
    desktop_.remove(_f.getDesktopIcon());

    /*GCJ-BEGIN*/
    if(BuLib.swing()>=1.2)
    {
      if(_f.getNormalBounds()!=null)
        _f.setNormalBounds(null);
    }
    /*GCJ-END*/

    if(wasIcon(_f))
      setWasIcon(_f,null);

    if(selected) activateNextFrame();
  }

  @Override
  public void maximizeFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: maximizeFrame()");

    if(_f.isIcon())
    {
      try { _f.setIcon(false); }
      catch(PropertyVetoException ex) { }
    }
    else
    {
      /*GCJ-BEGIN*/
      _f.setNormalBounds(_f.getBounds());
      /*GCJ-END*/

      Insets m=desktop_.getHardMargins();

      Container p=desktop_.getParent();
      if(p!=null) p=p.getParent();
      if(p instanceof JScrollPane)
      {
	Rectangle r=((JScrollPane)p).getViewport().getVisibleRect();
        r.x+=m.left;
        r.y+=m.top;
        r.width -=m.left+m.right;
        r.height-=m.top+m.bottom;
	setBoundsForFrame(_f,r.x,r.y,r.width,r.height);
      }
      else
      {
	setBoundsForFrame
	  (_f,m.left,m.top,
           getWidth(desktop_) -m.left-m.right,
           getHeight(desktop_)-m.top-m.bottom);
      }
    }

    if(!_f.isSelected())
    {
      try { _f.setSelected(true); }
      catch (PropertyVetoException ex) { }
    }
  }

  @Override
  public void minimizeFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: minimizeFrame()");

    if(_f.isIcon())
    {
      iconifyFrame(_f);
    }
    else
    {
      /*GCJ-BEGIN*/
      if(_f.getNormalBounds()!=null)
      {
	Rectangle r=_f.getNormalBounds();
	_f.setNormalBounds(null);
	try { _f.setSelected(true); }
	catch (PropertyVetoException ex) { }
	setBoundsForFrame(_f,r.x,r.y,r.width,r.height);
      }
      /*GCJ-END*/
    }
  }

  @Override
  public void iconifyFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: iconifyFrame()");

    boolean                     selected=_f.isSelected();
    JInternalFrame.JDesktopIcon icon    =_f.getDesktopIcon();

    if(!wasIcon(_f))
    {
      Rectangle r=getBoundsForIconOf(_f);
      icon.setBounds(r.x,r.y,r.width,r.height);
      setWasIcon(_f,Boolean.TRUE);
    }

    /*
    if(_f.isMaximum())
    {
      try { _f.setMaximum(false); }
      catch (PropertyVetoException ex) { }
    }
    */

    desktop_.remove(_f);
    desktop_.add(icon);

    if(selected)
    {
      activateNextFrame();
      if(desktop_.isBlocked()&&_f.isSelected()&&icon.isShowing())
      {
	try { _f.setSelected(false); }
	catch (PropertyVetoException ex) { }
      }
    }
  }

  @Override
  public void deiconifyFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: deiconifyFrame()");

    desktop_.remove(_f.getDesktopIcon());
    desktop_.add(_f);

    //if(_f.isSelected()) _f.moveToFront();
    //try { _f.setSelected(true); }
    //catch (PropertyVetoException ex) { }
    activateFrame(_f);
  }

  @Override
  public void activateFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: activateFrame()");

    /*GCJ-BEGIN*/
    JInternalFrame current=desktop_.getSelectedFrame();
    if(current!=_f)
    {
      if((current!=null)&&current.isSelected())
      {
	try { current.setSelected(false); }
	catch(PropertyVetoException ex) { }
      }

      desktop_.setSelectedFrame(_f);
    }
    /*GCJ-END*/

    _f.moveToFront();
  }

  @Override
  public void deactivateFrame(JInternalFrame _f)
  {
    if(DEBUG) FuLog.debug("BDM: deactivateFrame()");

    /*GCJ-BEGIN*/
    JInternalFrame current=desktop_.getSelectedFrame();
    if(current==_f) desktop_.setSelectedFrame(null);
    /*GCJ-END*/
  }

  private Rectangle ob_=null;
  private Insets    ib_=null;

  private void paintDraggedFrame(Graphics _g, boolean _resize)
  {
    if(_g instanceof PrintGraphics) return;

    if(/*(FuLib.jdk()>=1.2)&&*/(_g instanceof Graphics2D))
    {
      ((Graphics2D)_g).setRenderingHint
	(RenderingHints.KEY_TEXT_ANTIALIASING,
	 RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
      ((Graphics2D)_g).setRenderingHint
	(RenderingHints.KEY_ANTIALIASING,
	 RenderingHints.VALUE_ANTIALIAS_OFF);
    }

    _g.setColor(BuLib.getColor(Color.white));
    _g.setXORMode(BuLib.getColor(Color.black));
    _g.drawRect(ob_.x,ob_.y,ob_.width-1,ob_.height-1);

    int h=_g.getFont().getSize();

    if(ib_!=null)
    {
      if((ib_.left>1)&&(ib_.top>3+h)&&(ib_.right>1)&&(ib_.bottom>1))
      {
        _g.drawRect(ob_.x+ib_.left,ob_.y+ib_.top,
                    ob_.width-1-ib_.left-ib_.right,
                    ob_.height-1-ib_.top-ib_.bottom);
      }
    }

    _g.drawString(_resize
		  ? ob_.width+"x"+ob_.height
		  : ob_.x+","+ob_.y,
		  ob_.x+4,ob_.y+2+h);
  }

  // Drag

  @Override
  public void beginDraggingFrame(JComponent _f)
  {
  }

  @Override
  public void dragFrame(JComponent _f, int _x, int _y)
  {
    if(!desktop_.isTabbed()||desktop_.isPalette(_f))
      if(!desktop_.isBlocked()||!(_f instanceof JInternalFrame.JDesktopIcon))
      {
	if(desktop_.isOutline())
	{
	  Graphics g=desktop_.getGraphics();
	  if(ob_==null)
          {
            ob_=_f.getBounds();
            ib_=null;
            if(_f instanceof JInternalFrame)
            {
              JInternalFrame f=(JInternalFrame)_f;
              Component      c=f.getContentPane();
              if(c!=null)
              {
                Point p=SwingUtilities.convertPoint(c,0,0,f);
                ib_=new Insets(p.y,p.x,
                               getHeight(f)-p.y-getHeight(c),
                               getWidth (f)-p.x-getWidth (c));
              }
            }
          }
	  else paintDraggedFrame(g,false);
	  ob_.x=_x;
	  ob_.y=_y;
	  paintDraggedFrame(g,false);
          desktop_.getToolkit().sync();
	}
	else
	  setBoundsForFrame(_f,_x,_y,getWidth(_f),getHeight(_f));
      }
  }

  @Override
  public void endDraggingFrame(JComponent _f)
  {
    if(desktop_.isOutline())
    {
      if(ob_!=null)
      {
	Graphics g=desktop_.getGraphics();
	paintDraggedFrame(g,false);
	setBoundsForFrame(_f,ob_.x,ob_.y,ob_.width,ob_.height);
	ob_=null;
        ib_=null;
      }
    }

    desktop_.snapXY(_f);
    desktop_.adjustSize();
    //fred pour la gestion du focus: ce n'est pas le bon endroit
    Component c=FocusManager.getCurrentManager().getFocusOwner();
    if(c!=null){
      if(!SwingUtilities.isDescendingFrom(c, _f))
        _f.requestFocusInWindow();
    }
  }

  // Resize

  @Override
  public void beginResizingFrame(JComponent _f, int _dir)
  {
  }

  @Override
  public void resizeFrame(JComponent _f, int _x, int _y, int _w, int _h)
  {
    if(desktop_.isOutline())
    {
      Graphics g=desktop_.getGraphics();
      if(ob_==null) ob_=_f.getBounds();
      else paintDraggedFrame(g,true);
      ob_.x     =_x;
      ob_.y     =_y;
      ob_.width =_w;
      ob_.height=_h;
      paintDraggedFrame(g,true);
    }
    else
      setBoundsForFrame(_f,_x,_y,_w,_h);
  }

  @Override
  public void endResizingFrame(JComponent _f)
  {
    if(desktop_.isOutline())
    {
      if(ob_!=null)
      {
	Graphics g=desktop_.getGraphics();
	paintDraggedFrame(g,true);
	setBoundsForFrame(_f,ob_.x,ob_.y,ob_.width,ob_.height);
	ob_=null;
      }
    }

    desktop_.snapWH(_f);
    desktop_.adjustSize();
  }

  @Override
  public void setBoundsForFrame(JComponent _f, int _x, int _y, int _w, int _h)
  {
    boolean resized=(getWidth(_f)!=_w)||(getHeight(_f)!=_h);
    _f.setBounds(_x,_y,_w,_h);
    if(resized) _f.validate();
    //super.setBoundsForFrame(_f,_x,_y,_w,_h);
  }

  /*
  public void setBoundsForFrame
    (JComponent _f, int _x, int _y, int _width, int _height)
  {
    if(!desktop_.isTabbed())
      super.setBoundsForFrame(_f,_x,_y,_width,_height);
    else
    {
      Dimension dd=desktop_.getSize();
      super.setBoundsForFrame(_f,desktop_.LEFT_MARGIN+3,0,
			      dd.width-desktop_.LEFT_MARGIN-3,dd.height);
    }
  }
  */

  protected void setWasIcon(JInternalFrame _f, Boolean _value)
  {
    //if(_value!=null)
    _f.putClientProperty(HAS_BEEN_ICONIFIED_PROPERTY,_value);
  }

  protected boolean wasIcon(JInternalFrame _f)
  {
    return (_f.getClientProperty(HAS_BEEN_ICONIFIED_PROPERTY)==Boolean.TRUE);
  }

  protected Rectangle getBoundsForIconOf(JInternalFrame _f)
  {
    JInternalFrame.JDesktopIcon icon=_f.getDesktopIcon();
    Dimension                   ps  =icon.getPreferredSize();
    return new Rectangle
      (_f.getX()+_f.getWidth()-ps.width,_f.getY(),ps.width,ps.height);
  }

  protected void activateNextFrame()
  {
    if(DEBUG) FuLog.debug("BDM: activateNextFrame()");

    JInternalFrame[] frames=desktop_.getNormalFrames();
    if(frames.length>0)
    {
      try { frames[0].setSelected(true); }
      catch (PropertyVetoException ex) { }
      frames[0].moveToFront();
      return;
    }

    frames=desktop_.getPalettes();
    if(frames.length>0)
    {
      try { frames[0].setSelected(true); }
      catch (PropertyVetoException ex) { }
      frames[0].moveToFront();
    }
  }
}
