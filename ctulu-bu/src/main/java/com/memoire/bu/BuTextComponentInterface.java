/**
 * @modification $Date: 2005-08-16 12:58:04 $
 * @statut       unstable
 * @file         BuTextComponentInterface.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

public interface BuTextComponentInterface
{
  int getLength();
  int getCaretPosition();

  void go(int _line);
  void select(int _start,int _end);
  boolean find(String searchStr, int start, boolean ignoreCase);
  boolean replace(String searchStr, String replaceStr,
		  int start, int end, boolean ignoreCase);
}
