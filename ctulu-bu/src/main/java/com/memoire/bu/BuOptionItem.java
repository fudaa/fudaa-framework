/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuOptionItem.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.image.FilteredImageSource;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public final class BuOptionItem
{
  private String  text_;
  private Icon    icon_;
  private boolean enabled_;
  
  public BuOptionItem(String _text, Icon _icon, boolean _enabled)
  {
    text_   =_text;
    enabled_=_enabled;
    icon_   =_icon;

    if(!enabled_&&(icon_ instanceof ImageIcon))
      icon_=new ImageIcon
	(BuLib.HELPER.createImage
	 (new FilteredImageSource
	  (((ImageIcon)icon_).getImage().getSource(),
	   BuFilters.getDisabled())));
    
    int s=BuResource.BU.getDefaultMenuSize();
    if((icon_!=null)&&!(icon_ instanceof BuFixedSizeIcon))
    {
      int wi=icon_.getIconWidth();
      int hi=icon_.getIconHeight();
      if((wi<s)||(hi<s))
	icon_=new BuFixedSizeIcon
	  (icon_,Math.max(wi,s),Math.max(hi,s));
    }
  }
  
  public BuOptionItem(String _text, Icon _icon)
  {
    this(_text,_icon,true);
  }

  public BuOptionItem(String _text, boolean _enabled)
  {
    this(_text,null,_enabled);
  }

  public BuOptionItem(String _text)
  {
    this(_text,null,true);
  }

  public String getText()
  {
    return text_;
  }

  public Icon getIcon()
  {
    return icon_;
  }

  public boolean isEnabled()
  {
    return enabled_;
  }
}

