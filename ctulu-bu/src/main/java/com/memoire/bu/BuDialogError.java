/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuDialogError.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;


/**
 * A standard error dialog (continue).
 */

public class BuDialogError
       extends BuDialogMessage
      // implements ActionListener Fred inutile car deja dans BuDialogMessage
{
  public BuDialogError(BuCommonInterface      _parent,
		       BuInformationsSoftware _isoft,
		       Object                 _message)
  {
    super(_parent,_isoft,_message);
    setTitle(__("Erreur"));
  }
}

