/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut unstable
 * @file BuTable.java
 *
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Graphics;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.Date;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * Similar to JTable but add copy/select operations and row headers.
 *
 * @see javax.swing.JTable
 */
public class BuTable
        extends JTable
        implements ClipboardOwner {

  public BuTable() {
    this(null);
  }

  public BuTable(TableModel _model) {
    super(_model);

//    if(FuLib.jdk()>=1.4)
//    {
    setFocusCycleRoot(false);
//    }
  }

  public BuTable(Object[][] _values, Object[] _names) {
    this(new BuTableStaticModel(_values, _names));
  }

  @Override
  public boolean isFocusCycleRoot() {
    return false;
  }

  // Columns
  public void hideColumn(int _indexInModel) {
    Object o = getTableHeader();
    if (o instanceof BuTableColumnHeader) {
      ((BuTableColumnHeader) o).hideColumn(getColumnModel().getColumn(_indexInModel));
    }
  }

  public void showColumn(int _indexInModel) {
    Object o = getTableHeader();
    if (o instanceof BuTableColumnHeader) {
      ((BuTableColumnHeader) o).showColumn(getColumnModel().getColumn(_indexInModel));
    }
  }

  public void adjustColumnWidth(int _indexInModel) {
    Object o = getTableHeader();
    if (o instanceof BuTableColumnHeader) {
      ((BuTableColumnHeader) o).adjustWidth(getColumnModel().getColumn(_indexInModel));
    }
  }

  public void adjustVisibleColumns() {
    Object o = getTableHeader();
    if (o instanceof BuTableColumnHeader) {
      TableColumnModel m = getColumnModel();
      for (int i = m.getColumnCount() - 1; i >= 0; i--) {
        ((BuTableColumnHeader) o).adjustWidth(m.getColumn(i));
      }
    }
  }

  // Anti-aliasing
  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  /*
   * public void setTableData(Object[][] _values, Object[] _names) { setModel(new BuTableStaticModel(_values,_names)); }
   */
  private static final TableCellRenderer tcr_ = new BuTableCellRenderer();
  
  /**
   * BM 31/01/2020 : Les renderers sont d�finis pour chaque type suivant le renderer BuTableCellRenderer.
   * Ainsi, on pourra quand m�me les red�finir pour chacun des types.
   */
  protected void createDefaultRenderers() {
    super.createDefaultRenderers();
    // Objects
    defaultRenderersByColumnClass.put(Object.class, tcr_);

    // Numbers
    defaultRenderersByColumnClass.put(Number.class, tcr_);

    // Doubles and Floats
    defaultRenderersByColumnClass.put(Float.class, tcr_);
    defaultRenderersByColumnClass.put(Double.class, tcr_);

    // Dates
    defaultRenderersByColumnClass.put(Date.class, tcr_);

    // Icons and ImageIcons
    defaultRenderersByColumnClass.put(Icon.class, tcr_);
    defaultRenderersByColumnClass.put(ImageIcon.class, tcr_);

    // Booleans
    defaultRenderersByColumnClass.put(Boolean.class, tcr_);
}

  
//  @Override
//  public TableCellRenderer getDefaultRenderer(Class _columnClass) {
//    return tcr_;
//  }

  public Object[][] getAllValues() {
    int nr = getRowCount();
    int nc = getColumnCount();

    Object[][] r = new Object[nr][nc];

    int i, j;
    for (i = 0; i < nr; i++) {
      for (j = 0; j < nc; j++) {
        r[i][j] = getValueAt(i, j);
      }
    }

    return r;
  }

  public Object[][] getSelectedValues() {
    int[] sr = getSelectedRows();
    int[] sc = getColumnModel().getSelectedColumns();
    boolean rsa = getRowSelectionAllowed();
    boolean csa = getColumnSelectionAllowed();

    Object[][] r = new Object[rsa ? sr.length : getRowCount()][csa ? sc.length : getColumnCount()];

    int i, j;
    for (i = 0; i < r.length; i++) {
      for (j = 0; j < r[i].length; j++) {
        r[i][j] = getValueAt(rsa ? sr[i] : i, csa ? sc[j] : j);
      }
    }

    return r;
  }

  public String[] getSelectedColumnNames() {
    int[] sc = getColumnModel().getSelectedColumns();

    String[] r = new String[sc.length];
    for (int i = 0; i < sc.length; i++) {
      r[i] = getColumnName(sc[i]);
    }
    return r;
  }

  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {
  }

  public String convertAllValues(String _separator) {
    return convert0(getAllValues(), _separator);
  }

  public String convertSelectedValues(String _separator) {
    return convert0(getSelectedValues(), _separator);
  }

  protected String transformToString(Object value) {
    if (value == null) {
      return null;
    }
    return value.toString();

  }

  private String convert0(Object[][] _values, String _separator) {
    int l = _values.length;
    StringBuffer r = new StringBuffer(l * 40);

    for (int i = 0; i < l; i++) {
      for (int j = 0; j < _values[i].length; j++) {
        if (j != 0) {
          r.append(_separator);
        }
        String valueAsString = transformToString(_values[i][j]);
        if (valueAsString != null) {
          r.append(valueAsString);
        }
      }
      r.append('\n');
    }

    return r.toString();
  }

  public Transferable createTransferable() {
    Object[][] values = getSelectedValues();
    if ((values == null) || (values.length == 0)) {
      values = getAllValues();
    }

    String r = convert0(values, "\t");

    if (!r.equals("")) {
      return new StringSelection(r);
    }
    return null;
  }

  public void copy() {
    Transferable contents = createTransferable();
    if (contents != null) {
      Clipboard clipboard = getToolkit().getSystemClipboard();
      clipboard.setContents(contents, this);
    }
  }

  public void select() {
    if (getColumnSelectionAllowed() && getRowCount() > 0) {
      setColumnSelectionInterval(0, getColumnCount() - 1);
    }
    if (getRowSelectionAllowed() && (getRowCount() > 0)) {
      setRowSelectionInterval(0, getRowCount() - 1);
    }
  }

  public JList buildRowHeaders(boolean _letters) {
    /*
     * JList r=new JList (new AbstractListModel() { public int getSize() { return getModel().getRowCount(); } public
     * Object getElementAt(int index) { return null; } }); r.setFixedCellWidth(40); r.setFixedCellHeight(getRowHeight()+
     * getIntercellSpacing().height); r.setCellRenderer(new BuTableRowHeaderRenderer(this,_letters));
     * r.setRequestFocusEnabled(false); return r;
     */
    return new BuTableRowHeader(this, _letters);
  }

  public void addRowHeaders(JScrollPane _pane, boolean _letters) {
    _pane.setRowHeaderView(buildRowHeaders(_letters));
  }

  @Override
  protected JTableHeader createDefaultTableHeader() {
    return new BuTableColumnHeader(columnModel);
  }

  @Override
  public void addColumn(TableColumn _column) {
    _column.setMinWidth(0);
    super.addColumn(_column);
  }

  /*
   * return new JTableHeader(columnModel) { public void paintComponent(Graphics _g) { BuLib.setAntialiasing(this,_g);
   * super.paintComponent(_g);
   *
   * JTable t=getTable(); if((t!=null)&&(t.getModel() instanceof BuTableSortModel)) { BuTableSortModel
   * m=(BuTableSortModel)t.getModel(); Color old=_g.getColor(); int[] scs=m.getSortingColumns(); if(scs.length>0) {
   * for(int j=0;j<scs.length;j++) { int c=scs[j]; if(c>=0) { String n=m.getColumnName(c); if(n!=null) { c=-1; for(int
   * i=t.getColumnCount()-1;i>=0;i--) if(n.equals(t.getColumnName(i))) c=i; if(c>=0) { Rectangle r=getHeaderRect(c);
   * _g.translate(r.x+r.width-10,r.y+r.height/2-3); BuLib.setColor(_g,Color.white); if(m.isAscending(c))
   * _g.fillPolygon(vbax,vbay,3); else _g.fillPolygon(vtax,vtay,3); BuLib.setColor(_g,(j==0) ? Color.black :
   * Color.gray); if(m.isAscending(c)) _g.drawPolygon(vbax,vbay,3); else _g.drawPolygon(vtax,vtay,3);
   * _g.translate(-(r.x+r.width-10),-(r.y+r.height/2-3)); } } } _g.setColor(old); } } } } }; }
   */

  /*
   * public static void main(String[] _args) { JFrame frame=new JFrame("Test BuTable");
   *
   * TableModel tm=new AbstractTableModel() { public int getRowCount() { return 101; } //public String getColumnName(int
   * col) { return ""+col; } public int getColumnCount() { return 4; } public Object getValueAt(int row, int col) {
   * switch(col) { case 0: return FuFactoryInteger.get(row*(row%2==0 ? -row : row)); case 1: return new
   * String("["+row+"]"); case 2: return FuFactoryBoolean.get(row%3==0); case 3: return new java.util.Date(1000000*row);
   * } return null; } };
   *
   *
   * BuTableSortModel sorter=new BuTableSortModel(tm); final BuTable tb=new BuTable(sorter);
   * sorter.addMouseListenerToHeaderInTable(tb);
   *
   * tb.setAutoResizeMode(tb.AUTO_RESIZE_OFF);
   *
   * tb.setColumnSelectionAllowed(true); tb.setRowSelectionAllowed(true);
   *
   * //tb.getColumnModel().getSelectionModel(). //setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
   * //tb.getSelectionModel(). //setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
   *
   * TableCellRenderer tcr=new BuTableCellRenderer(); for(int i=0;i<tb.getColumnCount();i++) { TableColumn
   * c=tb.getColumn(tb.getColumnName(i)); c.setCellRenderer(tcr); }
   *
   * tb.hideColumn(2); tb.adjustVisibleColumns();
   *
   * JScrollPane sp=new JScrollPane(tb); tb.addRowHeaders(sp,false); frame.getContentPane().add(sp);
   *
   * frame.addWindowListener( new WindowAdapter() { public void windowClosing( WindowEvent e ) {
   * System.err.println(tb.convertSelectedValues(",")); System.exit(0); } });
   *
   * frame.setSize(300,200); frame.setVisible(true); }
   */
}
