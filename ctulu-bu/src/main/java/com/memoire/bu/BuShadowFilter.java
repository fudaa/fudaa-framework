/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         com/memoire/bu/BuShadowFilter.java
 * @version      0.18
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2(GPL2)
 * @copyright    1999-2004 Guillaume Desnoix
 */

package com.memoire.bu;

public class BuShadowFilter
  extends BuGlobalFilter
{
  public static final BuShadowFilter SHADOW=new BuShadowFilter( 56,0x000000);
  public static final BuShadowFilter LIGHT =new BuShadowFilter(192,0xFFFFFF);

  protected int opacity_;
  protected int color_;

  public BuShadowFilter(int _opacity,int _rgb)
  {
    opacity_=_opacity;
    color_  =(_rgb&0x00FFFFFF);
  }

  @Override
  protected int computeColor(int pixels[], int size)
  {
    float alphaSum=0;
    for(int i=0;i<size;i++)
    {
      try
      {
        int pixel=pixels[i];
        alphaSum +=DEFAULT_CM.getAlpha(pixel);
      }
      catch(ArrayIndexOutOfBoundsException e)
      {
        System.err.println("Error in BuShadowFilter");
      }
    }

    int alphaAvg=(int)(alphaSum / size);
    //return BuLib.getColor(((alphaAvg*opacity_/255)<<24)|color_);
    return ((alphaAvg*opacity_/255)<<24)|color_;
  }
}
