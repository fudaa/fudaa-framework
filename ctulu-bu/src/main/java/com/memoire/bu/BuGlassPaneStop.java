/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuGlassPaneStop.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;

/**
 * Used to stop any user event at the beginning.
 */

public class BuGlassPaneStop
       extends JComponent
{
  private Listener      listener_;
  //private BuApplication frame_;

  public BuGlassPaneStop() //BuApplication _frame)
  {
    setName("gpGLASSPANE_STOP");
    setOpaque(false);
    setVisible(false);
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

    listener_=null;
    //frame_=_frame;
  }

  @Override
  public void setVisible(boolean _v)
  {
    if(isVisible()==_v) return;

    if(listener_==null) listener_=new Listener();

    if(_v)
    {
      addMouseListener(listener_);
      addMouseMotionListener(listener_);
      addKeyListener(listener_);
    }
    else
    {
      removeMouseListener(listener_);
      removeMouseMotionListener(listener_);
      removeKeyListener(listener_);
    }

    super.setVisible(_v);
  }

  @Override
  public void paintComponent(Graphics _g)
  {
    super.paintComponent(_g);

    /*
    if(FuLib.jdk()>=1.2)
    {
      Rectangle clip=_g.getClipBounds();
      _g.setColor(new Color(0x90C0C0C0));
      _g.fillRect(clip.x,clip.y,clip.width,clip.height);
    }
    */

    /*
    else
    {
      Rectangle clip=_g.getClipBounds();
      _g.setColor(UIManager.getColor("Panel.background"));
      int y   =clip.y+clip.y%2;
      int ymax=y+clip.height;
      for(;y<ymax;y+=2)
	_g.drawLine(clip.x,y,clip.x+clip.width-1,y);
    }
    */
  }

  static class Listener
  implements MouseListener, MouseMotionListener, KeyListener
  {
    public Listener()
    {
    }

    @Override
    public void mouseClicked(MouseEvent _evt)
    {
    }
    
    @Override
    public void mouseEntered(MouseEvent _evt)
    {
    }
    
    @Override
    public void mouseExited(MouseEvent _evt)
    {
    }
    
    @Override
    public void mousePressed(MouseEvent _evt)
    {
    }
    
    @Override
    public void mouseReleased(MouseEvent _evt)
    {
    }
    
    @Override
    public void mouseMoved(MouseEvent _evt)
    {
    }
    
    @Override
    public void mouseDragged(MouseEvent _evt)
    {
    }

    @Override
    public void keyTyped(KeyEvent _evt)
    {
    }

    @Override
    public void keyPressed(KeyEvent _evt)
    {
    }

    @Override
    public void keyReleased(KeyEvent _evt)
    {
    }
  }
}
