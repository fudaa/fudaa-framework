/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuOverlayLayout.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import java.io.Serializable;

/**
 * An overlay layout where all the components have the same size.
 */
public class BuOverlayLayout
    implements LayoutManager2, Serializable
{
  // Vector components=new Vector(0,1);

  public BuOverlayLayout()
  {
    this(true,true,false);
  }

  public BuOverlayLayout(boolean _hfilled,boolean _vfilled, boolean _ratio)
  {
    hfilled_=_hfilled;
    vfilled_=_vfilled;
    ratio_  =_ratio;
    xalign_ =0.5f;
    yalign_ =0.5f;
  }

  protected boolean hfilled_;
  public boolean getHfilled() { return hfilled_; }
  public void setHfilled(boolean _hfilled) { hfilled_=_hfilled; }

  protected boolean vfilled_;
  public boolean getVfilled() { return vfilled_; }
  public void setVfilled(boolean _vfilled) { vfilled_=_vfilled; }

  protected boolean ratio_;
  public boolean getRatio() { return ratio_; }
  public void setRatio(boolean _ratio) { ratio_=_ratio; }

  protected float xalign_;
  public float getXAlign() { return xalign_; }
  public void setXAlign(float _xalign) { xalign_=_xalign; }

  protected float yalign_;
  public float getYAlign() { return yalign_; }
  public void setYAlign(float _yalign) { yalign_=_yalign; }

  @Override
  public void addLayoutComponent(Component _comp, Object _cstr)
  {
    // components.addElement(_comp);
  }

  @Override
  public void addLayoutComponent(String _name, Component _comp)
  {
    // components.addElement(_comp);
  }

  @Override
  public void removeLayoutComponent(Component _comp)
  {
    // components.removeElement(_comp);
  }

  @Override
  public Dimension minimumLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      // int nbc=components.size();
      int nbc=_parent.getComponentCount();
      int w  =0;
      int h  =0;

      for(int i=0;i<nbc;i++)
      {
	// Dimension d=((Component)components.elementAt(i)).getMinimumSize();
	Dimension d=_parent.getComponent(i).getMinimumSize();
	w=Math.max(w,d.width);
	h=Math.max(h,d.height);
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension preferredLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      // int nbc=components.size();
      int nbc=_parent.getComponentCount();
      int w  =0;
      int h  =0;

      for(int i=0;i<nbc;i++)
      {
	// Dimension d=((Component)components.elementAt(i)).getPreferredSize();
	Dimension d=_parent.getComponent(i).getPreferredSize();
	w=Math.max(w,d.width);
	h=Math.max(h,d.height);
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension maximumLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      // int nbc=components.size();
      int nbc=_parent.getComponentCount();
      int w  =0;
      int h  =0;

      for(int i=0;i<nbc;i++)
      {
	// Dimension d=((Component)components.elementAt(i)).getMaximumSize();
	Dimension d=_parent.getComponent(i).getMaximumSize();
	w=Math.max(w,d.width);
	h=Math.max(h,d.height);
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public float getLayoutAlignmentX(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public float getLayoutAlignmentY(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public void invalidateLayout(Container _parent)
  {
  }

  @Override
  public void layoutContainer(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      Insets insets = _parent.getInsets();
      Rectangle bounds = new Rectangle
	(insets.left,
	 insets.top,
	 _parent.getSize().width -(insets.left+insets.right),
	 _parent.getSize().height-(insets.top +insets.bottom));
      
      // int nbc=components.size();
      int nbc=_parent.getComponentCount();

      for(int i=0;i<nbc;i++)
      {
	// Component c=(Component)components.elementAt(i);
	Component c=_parent.getComponent(i);

	int x=bounds.x;
	int y=bounds.y;
	int w=bounds.width;
	int h=bounds.height;

	if(!hfilled_||!vfilled_)
	{
	  Dimension d=c.getPreferredSize();

	  if(ratio_&&(d.width>0)&&(d.height>0))
	  {
	    double f=Math.min(1.,Math.min(w/(double)d.width,
					  h/(double)d.height));
	    w=(int)Math.round(f*d.width );
	    h=(int)Math.round(f*d.height);
	  }

	  if(hfilled_) w=bounds.width;
	  if(vfilled_) h=bounds.height;

	  x+=(int)((bounds.width -w)*xalign_);
	  y+=(int)((bounds.height-h)*yalign_);
	}

	c.setBounds(x,y,w,h);
      }
    }
  }
}
