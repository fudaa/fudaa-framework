/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuMonitorClock.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.Color;

/**
 * A clock.
 */

public class BuMonitorClock
       extends BuMonitorAbstract
{
  protected long actu_;

  public BuMonitorClock()
  {
    super();

    actu_=System.currentTimeMillis();

    setName("buMONITOR_CLOCK");
    setForeground(new Color(64,32,96));
  }

  @Override
  public int getValue()
  { return 0; }

  @Override
  public int getMaximum()
  { return 100*(int)((actu_/1000)%60)/60; }

  @Override
  public String getText()
  { return FuLib.time(actu_); }

  @Override
  public String getToolTipText()
  { return FuLib.date(actu_); }

  @Override
  public long getDelay()
  { return 1000L; }

  @Override
  public boolean refreshState()
  {
    actu_=System.currentTimeMillis();
    return true;
  }
}
