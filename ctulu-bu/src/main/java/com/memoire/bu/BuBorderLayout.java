/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuBorderLayout.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.io.Serializable;
import java.util.Hashtable;

/**
 * A better BorderLayout.
 */
public class BuBorderLayout
       implements LayoutManager2, Serializable
{
  private Hashtable table_;

  public static final String FULL  ="Full";
  public static final String CENTER="Center";
  public static final String NORTH ="North";
  public static final String SOUTH ="South";
  public static final String EAST  ="East";
  public static final String WEST  ="West";

  // private static final long serialVersionUID = -8658291919501921765L;

  public BuBorderLayout()
  {
    this(0,0,true,true);
  }

  public BuBorderLayout(int _hgap, int _vgap)
  {
    this(_hgap,_vgap,true,true);
  }

  public BuBorderLayout(int _hgap, int _vgap,
			boolean _hfilled, boolean _vfilled)
  {
    hgap_   =_hgap;
    vgap_   =_vgap;
    hfilled_=_hfilled;
    vfilled_=_vfilled;
    xalign_ =0.5f;
    yalign_ =0.5f;
    table_  =new Hashtable(5);
  }

  protected int hgap_;
  public int getHgap() { return hgap_; }
  public void setHgap(int _hgap) { hgap_=_hgap; }

  protected int vgap_;
  public int getVgap() { return vgap_; }
  public void setVgap(int _vgap) { vgap_=_vgap; }

  protected boolean hfilled_;
  public boolean getHfilled() { return hfilled_; }
  public void setHfilled(boolean _hfilled) { hfilled_=_hfilled; }

  protected boolean vfilled_;
  public boolean getVfilled() { return vfilled_; }
  public void setVfilled(boolean _vfilled) { vfilled_=_vfilled; }

  protected float xalign_;
  public float getXAlign() { return xalign_; }
  public void setXAlign(float _xalign) { xalign_=_xalign; }

  protected float yalign_;
  public float getYAlign() { return yalign_; }
  public void setYAlign(float _yalign) { yalign_=_yalign; }

  @Override
  public void addLayoutComponent(Component _comp, Object _constraints)
  {
    synchronized(_comp.getTreeLock())
    {
      if((_constraints==null)||(_constraints instanceof String))
	addLayoutComponent2((String)_constraints,_comp);
      else
	throw new IllegalArgumentException
	  ("Can not add to layout: constraint must be a string (or null)");
    }
  }

  @Override
  public void addLayoutComponent(String _name, Component _comp)
  {
    String name=_name;
    if(name==null) name=CENTER;

    StringBuffer sb=new StringBuffer("@deprecated add(String,Component):");
    sb.append(" use add(Component,BuBorderLayout.");
    sb.append(name.toUpperCase()+" instead at ");
    String s=FuLog.getStackTrace();
    while(s.indexOf("BuBorderLayout")>0)
      s=s.substring(s.indexOf('\n')+1);
    while(s.indexOf("Container")>0)
      s=s.substring(s.indexOf('\n')+1);
    s=s.substring(s.indexOf('(')+1,s.indexOf(')'));
    sb.append(s);
    FuLog.warning(s);

    addLayoutComponent2(name,_comp);
  }

  private void addLayoutComponent2(String _name, Component _comp)
  {
    String name=_name;
    if(name==null) name=CENTER;

    synchronized(_comp.getTreeLock())
    {
      if(FULL.equals(name)||CENTER.equals(name)||
         NORTH.equals(name)||SOUTH.equals(name)||
         EAST.equals(name)||WEST.equals(name))
      {
	// table_.put(name,comp);
	table_.put(_comp,name);
      }
      else
	throw new IllegalArgumentException
	  ("Can not add to layout: unknown constraint: " + name);
    }
  }

  @Override
  public void removeLayoutComponent(Component _comp)
  {
    synchronized(_comp.getTreeLock())
    {
      // Object name=table_.get(_comp);
      // if(name!=null)
      // {
	table_.remove(_comp);
      //   table_.remove(name);
      // }
    }
  }

  private Dimension getMinimumSize(Component _c)
  {
    Dimension r=_c.getMinimumSize();
    if(r==null) r=new Dimension(0,0);
    return r;
  }

  private Dimension getPreferredSize(Component _c)
  {
    Dimension r=_c.getPreferredSize();
    if(r==null) r=new Dimension(0,0);
    return r;
  }

  private void adjustSize(Dimension _r, Dimension _d, String _s)
  {
    if(CENTER.equals(_s))
    {
      _r.width  =Math.max(_d.width,_r.width);
      _r.height =Math.max(_d.height,_r.height);
      //_r.width +=_d.width;
      //_r.height+=_d.height;
    }
    else
    if(EAST.equals(_s))
    {
      _r.width +=_d.width+hgap_;
      _r.height =Math.max(_d.height,_r.height);
    }
    else
    if(WEST.equals(_s))
    {
      _r.width +=_d.width + hgap_;
      _r.height =Math.max(_d.height,_r.height);
    }
    else
    if(NORTH.equals(_s))
    {
      _r.width  =Math.max(_d.width,_r.width);
      _r.height+=_d.height+vgap_;
    }
    else
    if(SOUTH.equals(_s))
    {
      _r.width  =Math.max(_d.width,_r.width);
      _r.height+=_d.height+vgap_;
    }
  }

  @Override
  public Dimension minimumLayoutSize(Container _parent)
  {
    synchronized(_parent.getTreeLock())
    {
      Dimension r=new Dimension(0,0);
      int nbc=_parent.getComponentCount();

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;

	  if(CENTER.equals(s))
	    adjustSize(r,getMinimumSize(c),s);
	}
      }

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;

	  if(!CENTER.equals(s))
	    adjustSize(r,getMinimumSize(c),s);
	}
      }

      Insets insets=_parent.getInsets();
      r.width +=insets.left+insets.right;
      r.height+=insets.top+insets.bottom;
      
      return r;
    }
  }
    
  @Override
  public Dimension preferredLayoutSize(Container _parent)
  {
    synchronized(_parent.getTreeLock())
    {
      Dimension r=new Dimension(0,0);
      int nbc=_parent.getComponentCount();
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;

	  if(CENTER.equals(s))
	    adjustSize(r,getPreferredSize(c),s);
	}
      }

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;

	  if(!CENTER.equals(s))
	    adjustSize(r,getPreferredSize(c),s);
	}
      }

      Insets insets=_parent.getInsets();
      r.width +=insets.left+insets.right;
      r.height+=insets.top+insets.bottom;
      
      return r;
    }
  }

  @Override
  public Dimension maximumLayoutSize(Container _parent)
  {
    return new Dimension(Short.MAX_VALUE,Short.MAX_VALUE);
  }

  @Override
  public float getLayoutAlignmentX(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public float getLayoutAlignmentY(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public void invalidateLayout(Container _parent)
  {
  }
				      
  @Override
  public void layoutContainer(Container _parent)
  {
    synchronized(_parent.getTreeLock())
    {
      Insets insets=_parent.getInsets();

      int top   =insets.top;
      int bottom=_parent.getSize().height-insets.bottom;
      int left  =insets.left;
      int right =_parent.getSize().width-insets.right;

      int nbc=_parent.getComponentCount();

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;

	  if(FULL.equals(s))
	  {
	    int x=left;
	    int y=top;
	    int w=right-left;
	    int h=bottom-top;
            c.setBounds(x,y,w,h);
          }
        }
      }

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  Dimension d=getPreferredSize(c);

	  if(s==null) s=CENTER;

	  if(NORTH.equals(s))
	  {
	    c.setSize(right-left,d.height);
	    d=getPreferredSize(c);

	    if(hfilled_)
	    {
	      c.setBounds(left,top,right-left,d.height);
	    }
	    else
	    {
	      int w=Math.min(d.width,right-left);
	      int x=left+Math.round(xalign_*((right-left)-w));
	      c.setBounds(x,top,w,d.height);
	    }

	    top+=d.height+vgap_;
	  }
	  else
	  if(SOUTH.equals(s))
	  {
	    c.setSize(right-left,d.height);
	    d=getPreferredSize(c);

	    if(hfilled_)
	    {
	      c.setBounds(left,bottom-d.height,right-left,d.height);
	    }
	    else
	    {
	      int w=Math.min(d.width,right-left);
	      int x=left+Math.round(xalign_*((right-left)-w));
	      c.setBounds(x,bottom-d.height,w,d.height);
	    }

	    bottom-=d.height+vgap_;
	  }
	}
      }

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  Dimension d=getPreferredSize(c);

	  if(s==null) s=CENTER;

	  if(EAST.equals(s))
	  {
	    c.setSize(d.width,bottom-top);
	    d=getPreferredSize(c);

	    if(vfilled_)
	    {
	      c.setBounds(right-d.width,top,d.width,bottom-top);
	    }
	    else
	    {
	      int h=Math.min(d.height,bottom-top);
	      int y=top+Math.round(yalign_*((bottom-top)-h));
	      c.setBounds(right-d.width,y,d.width,h);
	    }

	    right-=d.width+hgap_;
	  }
	  else
	  if(WEST.equals(s))
	  {
	    c.setSize(d.width,bottom-top);
	    d=getPreferredSize(c);

	    if(vfilled_)
	    {
	      c.setBounds(left,top,d.width,bottom-top);
	    }
	    else
	    {
	      int h=Math.min(d.height,bottom-top);
	      int y=top+Math.round(yalign_*((bottom-top)-h));
	      c.setBounds(left,y,d.width,h);
	    }

	    left+=d.width+hgap_;
	  }
	}
      }

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;

	  if(CENTER.equals(s))
	  {
	    int x=left;
	    int y=top;
	    int w=right-left;
	    int h=bottom-top;

	    Dimension d=getPreferredSize(c);
	    c.setSize(w,h);
	    d=getPreferredSize(c);

	    if(!hfilled_)
	    {
	      w=Math.min(d.width,right-left);
	      x=left+Math.round(xalign_*((right-left)-w));
	    }

	    if(!vfilled_)
	    {
	      h=Math.min(d.height,bottom-top);
	      y=top+Math.round(yalign_*((bottom-top)-h));
	    }

	    c.setBounds(x,y,w,h);
	  }
	}
      }
    }
  }

  public String toString()
  {
    return getClass().getName()+"[hgap="+hgap_+",vgap="+vgap_+"]";
  }
}
