/**
 * @modification $Date: 2005-08-16 12:58:04 $
 * @statut       unstable
 * @file         BuTerminalKey.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

/**
 * A struct to describe how to display in terminals.
 */
public class BuTerminalKey
{
  public static final String WHOLE="__WHOLE__";
  public static final String LEFT ="__LEFT__";
  public static final String RIGHT="__RIGHT__";

  String word_;
  int    color_;
  byte   style_;
  String til_;

  public BuTerminalKey(String _word, int _color, byte _style, String _til)
  {
    word_ =_word;
    color_=_color;
    style_=_style;
    til_  =_til;
  }
}
