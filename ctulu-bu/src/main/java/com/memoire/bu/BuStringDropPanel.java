/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuStringDropPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtLib;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public abstract class BuStringDropPanel
  extends BuAbstractDropPanel
  //implements DtDragSensible, DtDropReady
{
  public BuStringDropPanel()
  {
    super();
  }

  public BuStringDropPanel(LayoutManager _layout)
  {
    super(_layout);
  }

  protected abstract boolean dropString(String _s);

  @Override
  public final int getDropActions()
  {
    return DtLib.ALL;
  }

  @Override
  public final boolean checkDropFlavor(DataFlavor[] _f, Point _p)
  {
    for(int i=0;i<_f.length;i++)
      if(DtLib.stringFlavor.equals(_f[i]))
        return true;
    return false;
  }

  @Override
  public final Object getDropData(Transferable _t)
  {
    String r=null;
    try
    {
      r=(String)_t.getTransferData(DtLib.stringFlavor);
    }
    catch(UnsupportedFlavorException ufex) { }
    catch(IOException ioex) { }
    return r;
  }

  public final boolean dropData(int _a,Object _data)
  {
    String s=(String)_data;
    return dropString(s);
  }
}
