/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuLazyIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;
import java.util.Iterator;
import java.util.Vector;
import java.util.WeakHashMap;
import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;

/**
 * A lazy BuIcon.
 */
public class BuLazyIcon
  extends BuIcon
{
  public interface Holder
  {
    void updateLazyIcon(BuLazyIcon _icon);
  }

  private URL         url_;
  private int         w_,h_;
  private boolean     init_,loading_;
  private WeakHashMap waiters_;

  public BuLazyIcon(String _url,int _w,int _h)
  {
    this(FuLib.createURL(_url),_w,_h);
  }

  public BuLazyIcon(URL _url,int _w,int _h)
  {
    url_    =_url;
    w_      =_w;
    h_      =_h;
    init_   =false;
    loading_=false;
    waiters_=new WeakHashMap(7);
    setDescription("lazy icon "+_url);
  }

  public boolean isAvailable()
  {
    return init_;
  }

  @Override
  public boolean isDefault()
  {
    return init_ ? super.isDefault() : false;
  }

  @Override
  public int getIconWidth()
  {
    return init_&&!super.isDefault() ? super.getIconWidth() : w_;
  }

  @Override
  public int getIconHeight()
  {
    return init_&&!super.isDefault() ? super.getIconHeight() : h_;
  }

  @Override
  public Image getImage()
  {
    if(!init_)
    {
      FuLog.error("BLI: don't call getImage() for lazy icons");
      return BuLib.DEFAULT_IMAGE;
    }

    return super.getImage();
  }

  public URL getURL()
  {
    return url_;
  }

  @Override
  public void setImage(Image _image)
  {
    if(!init_)
    {
      if((_image!=null)&&(_image!=BuLib.DEFAULT_IMAGE))
      {
        if(!loading_)
        {
          FuLog.error("BLI: don't call setImage() for lazy icons");
        }

        /*
        System.err.println("*** LAZY   ="+super.getIconWidth()+"x"+
        super.getIconHeight());
        System.err.println("*** url    ="+url_);
        System.err.println("*** default="+isDefault());
        System.err.println("*** image  ="+getImage());
        */

        super.setImage(_image);
        init_=true;
      }
    }
    else super.setImage(_image);
  }

  public void setIconSize(int _w,int _h)
  {
    w_=_w;
    h_=_h;
  }

  @Override
  public void paintIcon(final Component _c, Graphics _g, int _x, int _y)
  {
    if(!init_)
    {
      //System.err.println("BLI: paintIcon "+url_+" on "+_c.getClass().getName());
      if(waiters_.get(_c)==null)
      {
        waiters_.put(_c,Boolean.TRUE);
        //QUEUE.addLast(this);
        QUEUE.addElement(this);
        synchronized(LOCK) { LOCK.notifyAll(); }
      }
    }
    else
    {
      if(!super.isDefault())
      {
        super.paintIcon(_c,_g,_x,_y);
      }
      else
      {
        Color old=_g.getColor();
        BuLib.setAntialiasing(_g);
        BuLib.setColor(_g,Color.blue);
        _g.drawLine(_x+getIconWidth()-1,_y,
                    _x,_y+getIconHeight()-1);
        /*
          _g.fillOval(_x,_y,getIconWidth(),getIconHeight());
          BuLib.setColor(_g,Color.black);
          _g.drawOval(_x,_y,getIconWidth(),getIconHeight());
        */
        _g.setColor(old);
      }
    }
  }

  public void postpone(final AbstractButton _button)
  {
    if(!init_)
    {
      //System.err.println("BLI: postpone "+url_+" on "+_button.getClass().getName());
      waiters_.put(_button,Boolean.TRUE);
      //QUEUE.addLast(this);
      QUEUE.addElement(this);
      synchronized(LOCK) { LOCK.notifyAll(); }
    }
  }

  public void postpone(final Holder _c)
  {
    if(!init_)
    {
      //System.err.println("BLI: postpone "+url_+" on "+_c.getClass().getName());
      waiters_.put(_c,Boolean.TRUE);
      //QUEUE.addLast(this);
      QUEUE.addElement(this);
      synchronized(LOCK) { LOCK.notifyAll(); }
    }
  }

  //private static final LinkedList QUEUE =new LinkedList();
  static final Vector QUEUE =new Vector();
  static final Object LOCK  =new Object();
  private static final Thread LOADER=new Thread
    (new Runnable()
      {
        @Override
        public void run()
        {
          //try { Thread.sleep(30000L); }
          //catch(InterruptedException ex) { }

          for(;;)
          {
            while(!QUEUE.isEmpty())
            {
              //BuLazyIcon icon=(BuLazyIcon)QUEUE.removeFirst();
              BuLazyIcon icon=(BuLazyIcon)QUEUE.elementAt(0);
              QUEUE.removeElementAt(0);

              if(icon!=null) icon.upload();
            }

            synchronized(LOCK)
            {
              if(QUEUE.isEmpty())
              {
                try { LOCK.wait(); }
                catch(InterruptedException ex) { }
              }
            }
          }
        }
      },"Bu Lazy Icons");

  public static final void startLoadingThread()
  {
    if(!LOADER.isAlive())
    {
      LOADER.setPriority(Thread.MIN_PRIORITY);
      LOADER.start();
    }
  }
  
  void upload()
  {
    loading_=true;
    initImage(url_);
    init_   =true;
    loading_=false;

    Image img=getImage();
    if((w_!=getIconWidth())||(h_!=getIconHeight()))
      img=img.getScaledInstance(w_,h_,Image.SCALE_SMOOTH);
    img=BuLib.filter(img);
    setImage(img);

    Iterator e=waiters_.keySet().iterator();
    while(e.hasNext())
    {
      final Component c=(Component)e.next();
      //System.err.println
      //  ("BLI: setting icon "+url_+" on "+c.getClass().getName());

      SwingUtilities.invokeLater(new Runnable()
        {
          @Override
          public void run()
          {
            if(c instanceof AbstractButton) //waiters_.get(c)==Boolean.TRUE)
              BuLib.setIcon((AbstractButton)c,BuLazyIcon.this);
            else
            if(c instanceof Holder)
              ((Holder)c).updateLazyIcon(BuLazyIcon.this);
            else
              c.repaint();
          }
        });
    }
    waiters_.clear();
    waiters_=null;
  }

  /*
  private void startThread()
  {
    if(!loading_)
    {
      loading_=true;
      Thread t=new Thread
        (new Runnable()
          {
            public void run()
            {
              try { Thread.sleep(30000L); }
              catch(InterruptedException ex) { }
              
              initImage(url_);
              //init_   =true;
              loading_=false;
              
              if((w_!=getIconWidth())||(h_!=getIconHeight()))
                setImage(getImage().getScaledInstance(w_,h_,Image.SCALE_SMOOTH));
              
              Iterator e=waiters_.keySet().iterator();
              while(e.hasNext())
              {
                Component c=(Component)e.next();
                //System.err.println("BLI: setting icon "+url_+" on "+c.getClass().getName());
                if(c instanceof AbstractButton) //waiters_.get(c)==Boolean.TRUE)
                  BuLib.setIcon((AbstractButton)c,BuLazyIcon.this);
                else
                if(c instanceof Holder)
                  ((Holder)c).updateLazyIcon(BuLazyIcon.this);
                else
                  c.repaint();
              }
              waiters_.clear();
              waiters_=null;
            }
          },"Lazy Icon "+url_);
      t.setPriority(Thread.MIN_PRIORITY);
      t.start();
    }
  }
  */
}
