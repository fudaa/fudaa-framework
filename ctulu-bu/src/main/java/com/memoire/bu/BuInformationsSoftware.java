/**
 * @modification $Date: 2007-06-05 13:00:28 $
 * @statut       unstable
 * @file         BuInformationsSoftware.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;

/**
 * Basic informations for a software.
 */
public class BuInformationsSoftware
extends Object
{
	/**
	 * name of the software
	 */
	public String   name;
	/**
	 * number of the last release
	 */
	public String   version;
	/**
	 * date of the last release.
	 */
	public String   date;
	/**
	 * copyrights
	 */
	public String   rights;
	public String   contact;
	public String   license;
	public String   languages;
	public transient BuIcon   logo;
	public transient BuIcon   banner;
	public String   ftp;
	public String   http;
	/**
	 * url of the man page
	 */
	public String   man;
	/**
	 * url of the update page
	 */
	public String   update;
	public String   citation;
	public String[] authors;
	public String[] specifiers;
	public String[] contributors;
	public String[] documentors;
	public String[] translators;
	public String[] testers;
	public String[] libraries;
	public String[] thanks;

	public BuInformationsSoftware()
	{
		name        ="Inconnu";
		version     ="inconnue";
		date        ="1970-01-01";
		rights      ="";
		contact     ="guillaume@desnoix.com";
		license     ="GNU General Public License 2 (GPL)";
		languages   ="fr";
		logo        =null;
		banner      =null;
		// ftp="ftp://ftp.jdistro.com/";
		http        ="http://www.jdistro.com/";
		man         ="http://www.jdistro.com/";
		update      ="http://www.jdistro.com/";
		citation    ="Richard M. Stallman";
		authors     =null;
		specifiers  =null;
		contributors=null;
		documentors =null;
		translators =null;
		testers     =null;
		libraries   =null;
		thanks      =null;
	}

	public void copyInto(BuInformationsSoftware _info)
	{
		_info.name     =name;
		_info.version  =version;
		_info.date     =date;
		_info.rights   =rights;
		_info.contact  =contact;
		_info.license  =license;
		_info.languages=languages;
		_info.logo     =logo;
		_info.banner   =banner;
		_info.ftp      =ftp;
		_info.http     =http;
		_info.man      =man;
		_info.update   =update;
		_info.citation =citation;

		_info.authors     =authors;
		_info.specifiers  =specifiers;
		_info.contributors=contributors;
		_info.documentors =documentors;
		_info.translators =translators;
		_info.testers     =testers;
		_info.libraries   =libraries;
		_info.thanks      =thanks;
	}

	/*
  public void fillMissingFieldsFrom(BuInformationsSoftware _info)
  {
    if(name     ==null) name     =_info.name;
    if(version  ==null) version  =_info.version;
    if(date     ==null) date     =_info.date;
    if(rights   ==null) rights   =_info.rights;
    if(contact  ==null) contact  =_info.contact;
    if(license  ==null) license  =_info.license;
    if(languages==null) languages=_info.languages;
    if(logo     ==null) logo     =_info.logo;
    if(banner   ==null) banner   =_info.banner;
    if(ftp      ==null) ftp      =_info.ftp;
    if(http     ==null) http     =_info.http;
    if(man      ==null) man      =_info.man;
    if(update   ==null) update   =_info.update;
    if(citation ==null) citation =_info.citation;

    if(authors      ==null) authors      =_info.authors;
    if(specifiers   ==null) specifiers   =_info.specifiers;
    if(contributors ==null) contributors =_info.contributors;
    if(documentors  ==null) documentors  =_info.documentors;
    if(translators  ==null) translators  =_info.translators;
    if(testers      ==null) testers      =_info.testers;
    if(libraries    ==null) libraries    =_info.libraries;
    if(thanks       ==null) thanks       =_info.thanks;
  }
	 */

	public String about()
	{
    String r=BuResource.BU.getString("{0} version {1} ({2})\n{3} \n \nDistribué sous ces termes:\n{4}\n \nContact:\n{5}\n \nSite:\n{6}",name,version,date,rights,license,contact,http);

		if(citation!=null)
			r+="\n \n"+BuResource.BU.getString("Cette version est dédicacée à:\n{0}",citation);

		int i;

		if(authors!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Développement:");
			for(i=0;i<authors.length;i++)
				r+="\n"+authors[i];
		}

		if(specifiers!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Spécifications:");
			for(i=0;i<specifiers.length;i++)
				r+="\n"+specifiers[i];
		}

		if(contributors!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Contributions:");
			for(i=0;i<contributors.length;i++)
				r+="\n"+contributors[i];
		}

		if(documentors!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Documentation:");
			for(i=0;i<documentors.length;i++)
				r+="\n"+documentors[i];
		}

		if(translators!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Traduction:");
			for(i=0;i<translators.length;i++)
				r+="\n"+translators[i];
		}

		if(testers!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Tests:");
			for(i=0;i<testers.length;i++)
				r+="\n"+testers[i];
		}

		if(libraries!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Bibliothèques:");
			for(i=0;i<libraries.length;i++)
				r+="\n"+libraries[i];
		}

		if(thanks!=null)
		{
			r+="\n \n"+BuResource.BU.getString("Remerciements:");
			for(i=0;i<thanks.length;i++)
				r+="\n"+thanks[i];
		}

		return r;
	}

	public String license()
	{
		String r=BuResource.BU.getString("{0} version {1} ({2})\n{3} \n \nDistribué sous ces termes:\n {4}\n",name,version,date,rights,license);

		if("GPL2".equals(license))
		{
			r+=BuResource.BU.getString("Texte original:");
			r+="\n  http://www.gnu.org/copyleft/gpl.html\n";
			r+=BuResource.BU.getString("Traduction en français:");
			r+="\n  http://www.april.org/gnu/gpl_french.html";
		}
		return r;
	}

	public String baseManUrl()
	{
		return man+FuLib.replace
		(FuLib.clean(name.toLowerCase()),"_","-")+"/";
	}

	public String toString()
	{
		String r="Software[";
		r+=name+","+version+","+date;
		if(!contact .equals("")) r+=","+contact;
		if(!license .equals("")) r+=","+license;
		if(!http    .equals("")) r+=","+http;
		r+="]";
		return r;
	}
}
