/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut unstable
 * @file BuButton.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

/**
 * Like JButton but with better management of icons.
 */
public class BuButton
        extends JButton {

  public BuButton() {
    this((BuIcon) null, null);
  }

  public BuButton(BuIcon _icon) {
    this(_icon, null);
  }

  public BuButton(String _label) {
    this(null, _label);
  }

  public BuButton(BuIcon _icon, String _label) {
    super();

    setActionCommand("BUTTON_PRESSED");
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setIcon(_icon);
    setText(_label);
    setHorizontalAlignment(LEFT);
  }

  public BuButton(Icon _icon) {
    this(null, _icon);
  }

  /*
   public BuButton(Action _action)
   {
   super(_action);
   setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
   }
   */
  public BuButton(String _text, Icon _icon) {
    super();
    setActionCommand("BUTTON_PRESSED");
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setIcon(_icon);
    setText(_text);
  }

  // Anti-aliasing
  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  @Override
  public boolean isOpaque() {
    boolean r = super.isOpaque();

    Container parent = getParent();

    if (parent instanceof JComponent) {
      Object p = ((JComponent) parent)
              .getClientProperty("JToolBar.isRollover");
      if (Boolean.TRUE.equals(p)) {
        r &= isEnabled() && model.isRollover();
      }
    }

    return r;
  }

  @Override
  public int getHorizontalAlignment() {
    int r = super.getHorizontalAlignment();
    if ((r == CENTER) && (getText() != null)) {
      r = LEFT;
    }
    return r;
  }

  @Override
  public void setText(String _text) {
    String text = _text;
    if ("".equals(text)) {
      text = null;
    }
    super.setText(text);

    if (!BuPreferences.BU.getBooleanProperty("button.text", true)
            && (getToolTipText() == null)) {
      super.setToolTipText(text);
    }
  }

  @Override
  public void setDisplayedMnemonicIndex(int index) throws IllegalArgumentException {
    if (BuPreferences.BU.getBooleanProperty("button.text", true)) {
      super.setDisplayedMnemonicIndex(index);
    }
  }

  @Override
  public String getText() {
    if (BuPreferences.BU.getBooleanProperty("button.text", true)
            || (super.getIcon() == null)
            || !BuPreferences.BU.getBooleanProperty("button.icon", true)) {
      return super.getText();
    }
    return null;
  }

  public void setIcon(BuIcon _icon) {
    BuLib.setIcon(this, _icon);
  }

  @Override
  public Icon getIcon() {
    if (BuPreferences.BU.getBooleanProperty("button.icon", true)
            || (super.getText() == null)
            || !BuPreferences.BU.getBooleanProperty("button.text", true)) {
      return super.getIcon();
    }
    return null;
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt) {
    Point r = super.getToolTipLocation(_evt);
    if (r == null) {
      r = new Point(0, getHeight() + 1);
    }
    return r;
  }
  private static final Insets REF = new Insets(2, 14, 2, 14);
  private static final InsetsUIResource ALT = new InsetsUIResource(0, 8, 0, 8);

  @Override
  public void updateUI() {
    super.updateUI();
    setRolloverEnabled(true);

    if (BuLib.isOcean()) {
      Insets i = getMargin();
      if ((i instanceof UIResource) && REF.equals(i)) {
        setMargin(ALT);
      }
    }
  }

  @Override
  public Color getBackground() {
    Color r;
    if (isEnabled() && model.isRollover() && BuLib.isMetal()) {
      r = new ColorUIResource(192, 192, 208);
    } else {
      r = super.getBackground();
    }
    return r;
  }

  // Focus
  @Override
  protected void processFocusEvent(FocusEvent _evt) {
    BuLib.focusScroll(this, _evt);
    super.processFocusEvent(_evt);
  }
}
