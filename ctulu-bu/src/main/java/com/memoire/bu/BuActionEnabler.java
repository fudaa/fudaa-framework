/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuActionEnabler.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.MenuElement;

/**
 * An utility class to enable/disable controls by their command name.
 * The command name is a string defined for each control
 * using setActionCommand().
 */

public class BuActionEnabler
{

  // Menu

  public static void setEnabledForAction
    (JMenuBar _bar, String _action, boolean _state)
  {
    if(_bar==null) return;

    MenuElement[] c=_bar.getSubElements();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JMenu)
	setEnabledForAction((JMenu)c[i],_action,_state);
  }

  public static void setEnabledForAction
    (JMenu _menu, String _action, boolean _state)
  {
    if(_menu==null) return;

    if(_menu.getActionCommand().equals(_action))
      _menu.setEnabled(_state);

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
	setEnabledForAction((JMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JPopupMenu)
	setEnabledForAction((JPopupMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JMenuItem)
	setEnabledForAction((JMenuItem)c[i],_action,_state);
    }
  }

  public static void setEnabledForAction
    (JPopupMenu _menu, String _action, boolean _state)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
	setEnabledForAction((JMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JPopupMenu)
	setEnabledForAction((JPopupMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JMenuItem)
	setEnabledForAction((JMenuItem)c[i],_action,_state);
      // else System.err.println("??? "+c[i]);
    }
  }

  public static void setEnabledForAction
    (JMenuItem _item, String _action, boolean _state)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setEnabled(_state);
  }

  // Tool

  public static void setEnabledForAction
    (JToolBar _bar, String _action, boolean _state)
  {
    if(_bar==null) return;

    Component[] c=_bar.getComponents();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JButton)
	setEnabledForAction((JButton)c[i],_action,_state);
  }

  public static void setEnabledForAction
    (JButton _item, String _action, boolean _state)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setEnabled(_state);
  }

  public static void setEnabledForAction
    (JPanel _panel, String _action, boolean _state)
  {
    if(_panel==null) return;

    Component[] c=_panel.getComponents();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JButton)
	setEnabledForAction((JButton)c[i],_action,_state);
  }
}
