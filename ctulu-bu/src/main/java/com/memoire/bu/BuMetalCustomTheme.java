/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuMetalCustomTheme.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.util.Arrays;
import javax.swing.Icon;
import javax.swing.LookAndFeel;
import javax.swing.UIDefaults;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalIconFactory;

/**
 * A simple customizable theme for Metal.
 */

public class BuMetalCustomTheme
  extends DefaultMetalTheme
  implements Icon
{

  private static final BuMetalCustomTheme[] THEMES=
    new BuMetalCustomTheme[]
    {
      new BuMetalCustomTheme(false),
      new BuMetalCustomTheme(true),
      null,
      new BuMetalCustomTheme
	(BuResource.BU.getString("Personnel"),
	 BuPreferences.BU.getBooleanProperty("metal.bold"   ,false),
	 BuPreferences.BU.getIntegerProperty("metal.size"   ,11),
	 BuPreferences.BU.getColorProperty("metal.primary"  ,new Color(128,192,160)),  //128,160,224
	 BuPreferences.BU.getColorProperty("metal.secondary",new Color(160,160,160)),false), //224,192,160
      new BuMetalCustomTheme
	(BuResource.BU.getString("Personnel"),
	 BuPreferences.BU.getBooleanProperty("metal.bold"   ,false),
	 BuPreferences.BU.getIntegerProperty("metal.size"   ,11),
	 BuPreferences.BU.getColorProperty("metal.primary"  ,new Color(128,192,160)),  //128,160,224
	 BuPreferences.BU.getColorProperty("metal.secondary",new Color(160,160,160)),true), //224,192,160
      null,
      new BuMetalCustomTheme("All�g�, tr�s petit",10,false),
      new BuMetalCustomTheme("All�g�, petit"     ,11,false),
      new BuMetalCustomTheme("All�g�, moyen"     ,12,false),
      new BuMetalCustomTheme("All�g�, grand"     ,13,false),
      new BuMetalCustomTheme("All�g�, tr�s grand",14,false),
      null,
      new BuMetalCustomTheme("All�g�, tr�s petit",10,true),
      new BuMetalCustomTheme("All�g�, petit"     ,11,true),
      new BuMetalCustomTheme("All�g�, moyen"     ,12,true),
      new BuMetalCustomTheme("All�g�, grand"     ,13,true),
      new BuMetalCustomTheme("All�g�, tr�s grand",14,true),
      null,
      new BuMetalCustomTheme("Atlantique",false,12,new Color(163,184,204),new Color(198,198,198),false),
      new BuMetalCustomTheme("Brique"    ,false,12,new Color(192,128,128),new Color(176,160,160),false),
      new BuMetalCustomTheme("Ocre"      ,false,12,new Color(192,160,128),new Color(176,160,144),false),
      new BuMetalCustomTheme("Turquoise" ,false,12,new Color( 96,160,160),new Color(128,160,160),false),
      null,
      new BuMetalCustomTheme("Atlantique",false,12,new Color(163,184,204),new Color(198,198,198),true),
      new BuMetalCustomTheme("Brique"    ,false,12,new Color(192,128,128),new Color(176,160,160),true),
      new BuMetalCustomTheme("Ocre"      ,false,12,new Color(192,160,128),new Color(176,160,144),true),
      new BuMetalCustomTheme("Turquoise" ,false,12,new Color( 96,160,160),new Color(128,160,160),true),
      null,
      new BuMetalCustomTheme("Atlantique",true ,12,new Color(163,184,204),new Color(198,198,198),false),
      new BuMetalCustomTheme("Brique"    ,true ,12,new Color(192,128,128),new Color(176,160,160),false),
      new BuMetalCustomTheme("Ocre"      ,true ,12,new Color(192,160,128),new Color(176,160,144),false),
      new BuMetalCustomTheme("Turquoise" ,true ,12,new Color( 96,160,160),new Color(128,160,160),false),
      null,
      new BuMetalCustomTheme("Atlantique",true ,12,new Color(163,184,204),new Color(198,198,198),true),
      new BuMetalCustomTheme("Brique"    ,true ,12,new Color(192,128,128),new Color(176,160,160),true),
      new BuMetalCustomTheme("Ocre"      ,true ,12,new Color(192,160,128),new Color(176,160,144),true),
      new BuMetalCustomTheme("Turquoise" ,true ,12,new Color( 96,160,160),new Color(128,160,160),true),
    };

  private boolean defaut_;
  private String  name_;
  private int     size_;
  private boolean ocean_;

  private BuMetalCustomTheme(boolean _ocean)
  {
    defaut_=true;
    name_  ="Traditionnel";
    size_  =0;
    ocean_ =_ocean;

    controlFont_=super.getControlTextFont();
    systemFont_ =super.getSystemTextFont();
    userFont_   =super.getUserTextFont();
    smallFont_  =super.getSubTextFont();

    if(ocean_)
    {
      primary1_=new ColorUIResource(0x6382BF);
      primary2_=new ColorUIResource(0xA3B8CC);
      primary3_=new ColorUIResource(0xB8CFE5);

      secondary1_=new ColorUIResource(0x7A8A99);
      secondary2_=new ColorUIResource(0xB8CFE5);
      secondary3_=new ColorUIResource(0xEEEEEE);
    }
    else
    {
      primary1_=super.getPrimary1();
      primary2_=super.getPrimary2();
      primary3_=super.getPrimary3();
      
      secondary1_=super.getSecondary1();
      secondary2_=super.getSecondary2();
      secondary3_=super.getSecondary3();
    }
  }

  

  private BuMetalCustomTheme(String _name, boolean _bold, int _size, Color _primary, Color _secondary,boolean _ocean)
  {
    defaut_=false;
    name_  =_name;
    size_  =_size;
    ocean_ =_ocean;

    controlFont_=new FontUIResource
      ("SansSerif", (_bold ? Font.BOLD : Font.PLAIN), size_);
    systemFont_ =new FontUIResource("SansSerif", Font.PLAIN,size_);
    userFont_   =new FontUIResource("Monospaced",Font.PLAIN,size_);
    smallFont_  =new FontUIResource("SansSerif", Font.PLAIN,7+(size_/4));

    primary1_  =new ColorUIResource(BuLib.mixColors(_primary  ,Color.black));
    primary2_  =new ColorUIResource(_primary                               );
    primary3_  =new ColorUIResource(BuLib.mixColors(_primary  ,Color.white));

    secondary1_=new ColorUIResource(BuLib.mixColors(_secondary,Color.black));
    secondary2_=new ColorUIResource(_secondary                             );
    secondary3_=new ColorUIResource(BuLib.mixColors(_secondary,Color.white));
  }

  private BuMetalCustomTheme(String _name, int _size,boolean _ocean)
  {
    defaut_=false;
    name_  =_name;
    size_  =_size;
    ocean_ =_ocean;

    controlFont_=new FontUIResource("SansSerif", Font.PLAIN,size_);
    systemFont_ =new FontUIResource("SansSerif", Font.PLAIN,size_);
    userFont_   =new FontUIResource("Monospaced",Font.PLAIN,size_);
    smallFont_  =new FontUIResource("SansSerif", Font.PLAIN,7+(size_/4));

    primary1_=super.getPrimary1();
    primary2_=super.getPrimary2();
    primary3_=super.getPrimary3();

    secondary1_=super.getSecondary1();
    secondary2_=super.getSecondary2();
    secondary3_=super.getSecondary3();
  }

  @Override
  public String getName() { return name_; }
  public Icon   getIcon() { return this; }

  private FontUIResource controlFont_;
  private FontUIResource systemFont_;
  private FontUIResource userFont_;
  private FontUIResource smallFont_;

  private ColorUIResource primary1_;
  private ColorUIResource primary2_;
  private ColorUIResource primary3_;

  private ColorUIResource secondary1_;
  private ColorUIResource secondary2_;
  private ColorUIResource secondary3_;
  
  @Override
  protected ColorUIResource getPrimary1() { return primary1_; }
  @Override
  protected ColorUIResource getPrimary2() { return primary2_; }
  @Override
  protected ColorUIResource getPrimary3() { return primary3_; }

  @Override
  protected ColorUIResource getSecondary1() { return secondary1_; }
  @Override
  protected ColorUIResource getSecondary2() { return secondary2_; }
  @Override
  protected ColorUIResource getSecondary3() { return secondary3_; }

  @Override
  public FontUIResource getControlTextFont() { return controlFont_; }
  @Override
  public FontUIResource getSystemTextFont()  { return systemFont_;  }
  @Override
  public FontUIResource getUserTextFont()    { return userFont_;    }
  @Override
  public FontUIResource getMenuTextFont()    { return controlFont_; }
  @Override
  public FontUIResource getWindowTitleFont() { return controlFont_; }
  @Override
  public FontUIResource getSubTextFont()     { return smallFont_;   }

  @Override
  public void addCustomEntriesToTable(UIDefaults _table)
  {
    super.addCustomEntriesToTable(_table);

    if(!defaut_)
    {
      final int internalFrameIconSize=Math.max(12,size_-2);
      _table.put("InternalFrameTitlePane.closeIcon",       MetalIconFactory.getInternalFrameCloseIcon(internalFrameIconSize));
      _table.put("InternalFrameTitlePane.maximizeIcon",    MetalIconFactory.getInternalFrameMaximizeIcon(internalFrameIconSize));
      _table.put("InternalFrameTitlePane.iconizeIcon",     MetalIconFactory.getInternalFrameMinimizeIcon(internalFrameIconSize));
      _table.put("InternalFrameTitlePane.altMaximizeIcon", MetalIconFactory.getInternalFrameAltMaximizeIcon(internalFrameIconSize));
      _table.put("ScrollBar.width",FuFactoryInteger.get(5+size_));
    }

    if(ocean_&&(FuLib.jdk()>1.4))
      addOceanEntriesToTable(_table);
  }

  // Ocean

  private static final ColorUIResource CONTROL_TEXT_COLOR=
    new ColorUIResource(0x333333);
  private static final ColorUIResource INACTIVE_CONTROL_TEXT_COLOR=
    new ColorUIResource(0x999999);
  private static final ColorUIResource MENU_DISABLED_FOREGROUND=
    new ColorUIResource(0x999999);
  private static final ColorUIResource OCEAN_BLACK=
    new ColorUIResource(0x333333);
  private static final ColorUIResource OCEAN_WHITE=
    new ColorUIResource(0xFFFFFF);

  @Override
  protected ColorUIResource getBlack()
  {
    return ocean_ ? OCEAN_BLACK : super.getBlack();
  }

  @Override
  public ColorUIResource getDesktopColor()
  {
    if(defaut_) return ocean_ ? OCEAN_WHITE : super.getDesktopColor();
    return getPrimary3();
  }

  @Override
  public ColorUIResource getInactiveControlTextColor()
  {
    return ocean_
      ? INACTIVE_CONTROL_TEXT_COLOR
      : super.getInactiveControlTextColor();
  }

  @Override
  public ColorUIResource getControlTextColor() {
    return ocean_
      ? CONTROL_TEXT_COLOR
      : super.getControlTextColor();
  }

  @Override
  public ColorUIResource getMenuDisabledForeground()
  {
    return ocean_
      ? MENU_DISABLED_FOREGROUND
      : super.getMenuDisabledForeground();
  }

  private Object getIconResource(String _iconID)
  {
    Object o=null;
    try { o=LookAndFeel.makeIcon(DefaultMetalTheme.class,_iconID); }
    catch(Throwable th) { th.printStackTrace(); }
    return o;
  }

  public void addOceanEntriesToTable(UIDefaults table)
  {
    Object focusBorder=new UIDefaults.ProxyLazyValue
      ("javax.swing.plaf.BorderUIResource$LineBorderUIResource",
       new Object[] {getPrimary1()});

    Color cccccc = new ColorUIResource(BuLib.mixColors(getSecondary3().getRGB(),0xAAAAAA)); //0xCCCCCC);
    Color dadada = new ColorUIResource(BuLib.mixColors(getSecondary3().getRGB(),0xC7C7C7)); //0xDADADA);
    Color c8ddf2 = new ColorUIResource(BuLib.mixColors(getSecondary2().getRGB(),0xD8EBFF)); //0xC8DDF2);

    Object directoryIcon=getIconResource("icons/ocean/directory.gif");
    Object fileIcon     =getIconResource("icons/ocean/file.gif");

    Object buttonGradient=null;
    // .30 0 DDE8F3 white secondary2
//    if(FuLib.jdk()>1.1)
//    {
      buttonGradient=Arrays.asList
        (new Object[] {
          new Float(.3f), new Float(0f),
          new ColorUIResource(BuLib.mixColors(BuLib.mixColors(getPrimary3().getRGB(),0xEEEEEE),0xE7F3FE)), //0xDDE8F3),
          getWhite(), getSecondary2() });
//    }

    Object sliderGradient=null;
//    if(FuLib.jdk()>1.1)
//    {
      sliderGradient=Arrays.asList(new Object[] {
        new Float(.3f), new Float(.2f),
        c8ddf2, getWhite(), getSecondary2() });
//    }

    Object menuGradient=null;
//    if(FuLib.jdk()>1.1)
//    {
      menuGradient=Arrays.asList(new Object[] {
        new Float(1f), new Float(0f),
        getWhite(), dadada, 
        new ColorUIResource(dadada) });
//    }

    Object[] defaults = new Object[] {
      "Button.gradient", buttonGradient,
      "Button.rollover", Boolean.TRUE,
      "Button.toolBarBorderBackground", cccccc,
      // Background color of buttons when they're in a toolbar.
      "Button.toolBarBackground", cccccc,
      "Button.rolloverIconType", "ocean",
      
      "CheckBox.rollover", Boolean.TRUE,
      "CheckBox.gradient", buttonGradient,
      
      "CheckBoxMenuItem.gradient", buttonGradient,
      
      // home2
      "FileChooser.homeFolderIcon",
      getIconResource("icons/ocean/homeFolder.gif"),
      // directory2
      "FileChooser.newFolderIcon",
      getIconResource("icons/ocean/newFolder.gif"),
      // updir2
      "FileChooser.upFolderIcon",
      getIconResource("icons/ocean/upFolder.gif"),
      
      // computer2
      "FileView.computerIcon",
      getIconResource("icons/ocean/computer.gif"),
      "FileView.directoryIcon", directoryIcon,
      // disk2
      "FileView.hardDriveIcon",
      getIconResource("icons/ocean/hardDrive.gif"),
      "FileView.fileIcon", fileIcon,
      // floppy2
      "FileView.floppyDriveIcon",
                 getIconResource("icons/ocean/floppy.gif"),
      
      "Menu.opaque", Boolean.FALSE,
      
      "MenuBar.gradient", menuGradient,
      "MenuBar.borderColor", cccccc,
      
      "InternalFrame.activeTitleGradient", buttonGradient,
      // close2
      "InternalFrame.closeIcon",
      getIconResource("icons/ocean/close.gif"),
      // minimize
      "InternalFrame.iconifyIcon",
      getIconResource("icons/ocean/iconify.gif"),
      // restore
      "InternalFrame.minimizeIcon",
      getIconResource("icons/ocean/minimize.gif"),
      // menubutton3
      "InternalFrame.menuIcon",
      getIconResource("icons/ocean/menu.gif"),
      // maximize2
      "InternalFrame.maximizeIcon",
      getIconResource("icons/ocean/maximize.gif"),
      // paletteclose
      "InternalFrame.paletteCloseIcon",
      getIconResource("icons/ocean/paletteClose.gif"),
      
      "List.focusCellHighlightBorder", focusBorder,
      
      "MenuBarUI", "javax.swing.plaf.metal.MetalMenuBarUI",
      
      "OptionPane.errorIcon",
      getIconResource("icons/ocean/error.png"),
      "OptionPane.informationIcon",
      getIconResource("icons/ocean/info.png"),
      "OptionPane.questionIcon",
      getIconResource("icons/ocean/question.png"),
      "OptionPane.warningIcon",
      getIconResource("icons/ocean/warning.png"),
      
      "RadioButton.gradient", buttonGradient,
      "RadioButton.rollover", Boolean.TRUE,
      
      "RadioButtonMenuItem.gradient", buttonGradient,
      
      "ScrollBar.gradient", buttonGradient,
      
      "Slider.altTrackColor", new ColorUIResource(0xD2E2EF),
      "Slider.gradient", sliderGradient,
      "Slider.focusGradient", sliderGradient,
      
      "SplitPane.oneTouchButtonsOpaque", Boolean.FALSE,
      "SplitPane.dividerFocusColor", c8ddf2,
      
      "TabbedPane.borderHightlightColor", getPrimary1(),
      "TabbedPane.contentAreaColor", c8ddf2,
      "TabbedPane.contentBorderInsets", new Insets(4, 2, 3, 3),
      "TabbedPane.selected", c8ddf2,
      "TabbedPane.tabAreaBackground", dadada,
      "TabbedPane.tabAreaInsets", new Insets(2, 2, 0, 6),
      "TabbedPane.unselectedBackground", getSecondary3(),
      
      "Table.focusCellHighlightBorder", focusBorder,
      "Table.gridColor", getSecondary1(),
      
      "ToggleButton.gradient", buttonGradient,
      
      "ToolBar.borderColor", cccccc,
      "ToolBar.childrenOpacity", Boolean.FALSE,
      "ToolBar.isRollover", Boolean.TRUE,
      
      "Tree.closedIcon", directoryIcon,
      "Tree.collapsedIcon",
      getIconResource("icons/ocean/collapsed.gif"),
      "Tree.expandedIcon",
      getIconResource("icons/ocean/expanded.gif"),
      "Tree.leafIcon", fileIcon,
      "Tree.openIcon", directoryIcon,
      "Tree.selectionBorderColor", getPrimary1()
    };
    table.putDefaults(defaults);
  }

  @Override
  public int getIconWidth()  { return 44; }
  @Override
  public int getIconHeight() { return BuResource.BU.getDefaultMenuSize(); }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    Color cold=_g.getColor();
    Font  fold=_g.getFont();

    FontMetrics fm=_g.getFontMetrics(controlFont_);
    int ws=fm.stringWidth("ab12");

    int w=getIconWidth();
    int h=getIconHeight();

    //_g.setColor(getSecondary2());
    BuLib.setColor(_g,getSecondary2());
    _g.fill3DRect(_x,_y,w,h,true);

    //_g.setColor(getPrimary2());
    BuLib.setColor(_g,getPrimary2());
    _g.fill3DRect(_x+2,_y+2,w/2,h-4,true);

    //_g.setColor(getSecondary3());
    BuLib.setColor(_g,getSecondary3());
    _g.fill3DRect(_x+w/2+2,_y+2,w/2-4,h-4,true);

    if(ocean_/*&&(FuLib.jdk()>1.1)*/)
    {
      Paint old=((Graphics2D)_g).getPaint();
      GradientPaint gp=new GradientPaint
        (_x+1,_y+1  ,new Color(255,255,255,192),
         _x+1,_y+h-2,new Color(  0,  0,  0, 96));
      ((Graphics2D)_g).setPaint(gp);
      _g.fillRect(_x+3,_y+3,w-6,h-6);
      ((Graphics2D)_g).setPaint(old);    
    }
      
    //_g.setColor(getBlack());
    BuLib.setColor(_g,getBlack());
    _g.setFont(controlFont_);
      
    _g.drawString("abcd",_x+(w-ws)/2+1,_y+h-3);
      
    _g.setColor(cold);
    _g.setFont(fold);
  }

  public static BuMetalCustomTheme[] getList()
  {
    return THEMES;
  }
}
