/**
 * @modification $Date: 2007-06-14 11:57:51 $
 * @statut unstable
 * @file BuFileFilter.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * A simple implementation of FileFilter.
 */

public class BuFileFilter extends javax.swing.filechooser.FileFilter implements FilenameFilter, Serializable {
  private Hashtable<String, BuFileFilter> filters_ = null;
  private boolean useExtensionsInDescription_ = true;
  protected String description_ = null;

  

  protected String fullDescription_ = null;
  protected BuIcon icon_ = null;
  protected String firstExt_ = null;

  public BuFileFilter(String extension) {
    this(extension, null);
  }
  
  public void setDescription(String description_) {
    this.description_ = description_;
  }

  public BuFileFilter(String _extension, String _description) {
    this(new String[] { _extension }, _description);
  }

  public BuFileFilter(String[] _filters) {
    this(_filters, null);
  }

  public BuFileFilter(String[] _filters, String _description) {
    if (_filters != null && _filters.length > 0) {
      firstExt_ = _filters[0];
    }
    filters_ = new Hashtable(_filters.length);
    for (int i = 0; i < _filters.length; i++)
      addExtension(_filters[i]);
    setShortDescription(_description);
  }

  protected String getString(String _s) {
    return BuResource.BU.getString(_s);
  }

  protected static final String __(String _s) {
    return BuResource.BU.getString(_s);
  }

  public boolean accept(String _f) {
    return (_f == null) ? false : accept(new File(_f));
  }

  @Override
  public boolean accept(File _f) {
    if (_f != null) {
      if (_f.isDirectory())
        return true;
      String extension = getExtension(_f);
      if ((extension != null) && (filters_.get(extension) != null))
        return true;
    }
    return false;
  }

  public boolean acceptExtension(String extension) {

    if ((extension != null) && (filters_.get(extension) != null))
      return true;
    return false;
  }

  @Override
  public boolean accept(File _d, String _fn) {
    File f = new File(_fn);
    if (f.isDirectory())
      return false;
    String extension = getExtension(f);
    if ((extension != null) && (filters_.get(extension) != null))
      return true;
    return false;
  }

  //  public String getExtension(File _f) {
  //    if (_f != null) {
  //      String filename = _f.getName();
  //      int i = filename.lastIndexOf('.');
  //      if ((i > 0) && (i < filename.length() - 1)) return filename.substring(i + 1).toLowerCase();
  //    }
  //    return null;
  //  }

  /**
   * Retourne l'extension du fichier parmis les extensions possibles du filtre
   * 
   * @param _f Le fichier sur lequel trouver l'extension
   * @return L'extension
   */
  public String getExtension(File _f) {
    for (String ext : filters_.keySet()) {
      if (_f.getPath().toLowerCase().endsWith(ext))
        return ext;
    }
    return null;
  }

  public void addExtension(String _extension) {
    if (filters_ == null)
      filters_ = new Hashtable(5);
    filters_.put(_extension.toLowerCase(), this);
    fullDescription_ = null;
    if (firstExt_ == null)
      firstExt_ = _extension;
  }

  @Override
  public String getDescription() {
    if (fullDescription_ == null) {
      if ((description_ == null) || isExtensionListInDescription()) {
        if (description_ != null)
          fullDescription_ = description_;
        else fullDescription_ = getString("Fichiers");

        fullDescription_ += " (";
        Enumeration extensions = filters_.keys();
        if (extensions != null) {
          fullDescription_ += "*." + (String) extensions.nextElement();
          while (extensions.hasMoreElements())
            fullDescription_ += ", *." + (String) extensions.nextElement();
        }
        fullDescription_ += ")";
      } else {
        fullDescription_ = description_;
      }
    }
    return fullDescription_;
  }

  /**
   * Retourne la description, sans les extensions.
   * 
   * @return La description
   */
  public String getShortDescription() {
    return description_;
  }

  /**
   * Definit la description, sans les extensions.
   * 
   * @param _description La description.
   */
  public void setShortDescription(String _description) {
    description_ = _description;
    fullDescription_ = null;
  }

  public void setExtensionListInDescription(boolean _b) {
    useExtensionsInDescription_ = _b;
    fullDescription_ = null;
  }

  public boolean isExtensionListInDescription() {
    return useExtensionsInDescription_;
  }

  public BuIcon getIcon() {
    return icon_;
  }

  public void setIcon(BuIcon _icon) {
    icon_ = _icon;
  }

  public int hashCode() {
    return toString().hashCode();
  }

  public boolean equals(Object _o) {
    boolean r = false;

    if (_o instanceof BuFileFilter)
      r = toString().equals(_o.toString());

    return r;
  }

  public String toString() {
    return getDescription();
  }

  public String getFirstExt() {
    return firstExt_;
  }
  
  public String[] getExtensions() {
    return filters_.keySet().toArray(new String[0]);
  }
}
