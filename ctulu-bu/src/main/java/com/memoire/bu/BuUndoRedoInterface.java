/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         BuUndoRedoInterface.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

/**
 * Interface for Undo/Redo services.
 */
public interface BuUndoRedoInterface
{
  void undo();
  void redo();
}
