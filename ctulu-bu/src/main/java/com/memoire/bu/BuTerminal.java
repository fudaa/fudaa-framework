/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuTerminal.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtFilesSelection;
import com.memoire.fu.FuEmptyArrays;
import com.memoire.vfs.VfsFile;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

/**
 * A small terminal (mainly for script interpreters).
 */
public class BuTerminal
  extends BuFileDropPanel
  implements ActionListener, MouseListener, MouseMotionListener,
             BuCutCopyPasteInterface, FuEmptyArrays
{
  protected BuTerminalDisplay           display_;
  protected BuTerminalDisplay.InStream  input_;
  protected BuTerminalDisplay.OutStream output_;
  protected BuTerminalDisplay.OutStream error_;
  protected BuPanel                     scp_;
  protected BuScrollPane                sp_;

  public BuTerminal()
  {
    this(80,24);
  }

  public BuTerminal(int _nbc,int _nbl)
  {
    display_=new BuTerminalDisplay(_nbc,_nbl);
    scp_    =null;
    sp_     =new BuScrollPane(display_);

    setLayout(new BuBorderLayout());

    Dimension sd=new Dimension
      (display_.c_to_x(Math.min(80,_nbc)),//+(_nbl>24 ? 21 : 0),
       display_.l_to_y(Math.min(24,_nbl)));//+(_nbc>80 ? 21 : 0));
    JViewport vp=sp_.getViewport();
    Insets    insets=vp.getInsets();
    sd.width +=insets.left+insets.right;
    sd.height+=insets.top+insets.bottom;
    vp.setPreferredSize(sd);
    sp_.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    sp_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    sp_.setPreferredSize(sp_.getPreferredSize());
    vp.setPreferredSize(null);
    sp_.setHorizontalScrollBarPolicy
      (ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    Dimension vs=vp.getViewSize();
    Dimension es=vp.getExtentSize();
    vp.setViewPosition(new Point(0,vs.height-es.height));
    add(sp_,BuBorderLayout.CENTER);

    setShortcuts(STRING0);

    display_.addMouseListener      (this);
    display_.addMouseMotionListener(this);

    /*
      new MouseAdapter()
      {
	public void mouseClicked(MouseEvent _evt)
	  { requestFocus(); }
      });
    */

    /*
    if(FuLib.jdk()>=1.4)
    {
      setTransferHandler(new TransferHandler()
      {
        public boolean importData(JComponent _c,Transferable _t)
        {
          boolean r=false;
          if(canImport(_c,_t.getTransferDataFlavors()))
          {
            try
            {
              String s=(String)_t.getTransferData(DtLib.stringFlavor);
              paste(s.replace('\n',' '));
              r=true;            
            }
            catch(UnsupportedFlavorException ufex) { }
            catch(IOException ioex) { }
          }
          return r;
        }

        public boolean canImport(JComponent _c,DataFlavor[] _flavors)
        {
          for(int i=0;i<_flavors.length;i++)
            if(DtLib.stringFlavor.equals(_flavors[i]))
              return true;
          return false;
        }
      });
    }
    */
  }

  public void setVerticalScrollBarPolicy(int _vsb)
  {
    sp_.setVerticalScrollBarPolicy(_vsb);
  }

  public void setHorizontalScrollBarPolicy(int _hsb)
  {
    sp_.setHorizontalScrollBarPolicy(_hsb);
  }

  public BuTerminalDisplay getDisplay()
  {
    return display_;
  }

  @Override
  protected Rectangle getDropRect()
  {
    Dimension d=sp_.getViewport().getExtentSize();
    Rectangle r=super.getDropRect();
    r.x=2;
    r.y=2;
    r.width =d.width-4;
    r.height=d.height-4;
    r=SwingUtilities.convertRectangle(sp_.getViewport(),r,this);
    return r;
  }

  @Override
  protected boolean dropFile(DtFilesSelection _s)
  {
    StringBuffer b=new StringBuffer();
    VfsFile[]    f=_s.getAllFiles();
    for(int i=0;i<f.length;i++)
    {
      b.append(f[i].getAbsolutePath().replace('\n',' '));
      b.append(' ');
    }
    String s=b.toString();
    if(!"".equals(s)) paste(s);
    return !"".equals(s);
  }

  // CutCopyPaste

  @Override
  public void copy()
  {
    getDisplay().copy();
  }

  @Override
  public void cut()
  {
    getDisplay().copy();
  }

  @Override
  public void duplicate()
  {
    getDisplay().copy();
    if(input_!=null) input_.paste();
  }

  @Override
  public void paste()
  {
    if(input_!=null) input_.paste();
  }

  public void paste(String _s)
  {
    if(input_!=null) input_.paste(_s);
  }

  // Mouse

  @Override
  public void mouseClicked(MouseEvent _evt)
  {
    int modifs=_evt.getModifiers();

    if((modifs&InputEvent.BUTTON2_MASK)!=0)
    {
      paste();
      return;
    }

    if((modifs&InputEvent.BUTTON3_MASK)!=0) return;

    getDisplay().requestFocus();

    int clicks=_evt.getClickCount();
    //System.err.println("CLICKS="+clicks);

    switch(clicks)
    {
    case 2:
	{
	  int l=getDisplay().y_to_l(_evt.getY());
	  int c=getDisplay().x_to_c(_evt.getX());
	  getDisplay().select_word(c,l);
	  getDisplay().copy();
	}
	break;
    case 3:
	{
	  int l=getDisplay().y_to_l(_evt.getY());
	  getDisplay().select_line(l);
	  getDisplay().copy();
	}
	break;
    /*
    case 4:
	{
	  getDisplay().select_screen();
	  dispaly_.copy();
	}
	break;
    */
    default:
	{
	  getDisplay().clear_selection();
	}
	break;
    }
  }
    
  @Override
  public void mouseEntered(MouseEvent _evt)
  {
  }
    
  @Override
  public void mouseExited(MouseEvent _evt)
  {
  }

  private int selcstart=-1;
  private int sellstart=-1;
    
  @Override
  public void mousePressed(MouseEvent _evt)
  {
    selcstart=getDisplay().x_to_c(_evt.getX());
    sellstart=getDisplay().y_to_l(_evt.getY());
  }
    
  @Override
  public void mouseReleased(MouseEvent _evt)
  {
    getDisplay().copy();
    selcstart=-1;
    sellstart=-1;
  }

  @Override
  public void mouseDragged(MouseEvent _evt)
  {
    int selcend=getDisplay().x_to_c(_evt.getX());
    int sellend=getDisplay().y_to_l(_evt.getY());
    getDisplay().clear_selection();
    getDisplay().set_selection(selcstart,sellstart,selcend,sellend);
  }
    
  @Override
  public void mouseMoved(MouseEvent _evt)
  {
  }  
    
  /*
  public void lostOwnership(Clipboard clipboard, Transferable contents)
  {
  }

  private Clipboard clipboard_;

  protected Clipboard getClipboard()
  {
    if(clipboard_!=null) return clipboard_;

    synchronized(this)
    {
      if(clipboard_!=null) return clipboard_;

      try
      {
	//Class  c=getToolkit().getClass();
	Method m=Toolkit.class.getMethod
          ("getSystemSelection",CLASS0);
	clipboard_=(Clipboard)m.invoke(getToolkit(),OBJECT0);
      }
      catch(Exception ex)
      {
	System.err.println("BTL: no system selection");
      }
      finally
      {
	if(clipboard_==null)
	  clipboard_=getToolkit().getSystemClipboard();
      }
    }

    return clipboard_;
  }

  public void copy()
  {
    String s=getDisplay().get_selection();

    int i=s.length()-1;
    while((i>=0)&&Character.isWhitespace(s.charAt(i))) i--;
    s=s.substring(0,i+1);

    if(!"".equals(s))
    {
      Transferable contents =new StringSelection(s);
      getClipboard().setContents(contents,this);
    }
  }

  public void paste()
  {
    Transferable contents=getClipboard().getContents(this);

    if(contents!=null)
    {
      try
      {
	String s=(String)contents.getTransferData(DataFlavor.stringFlavor);
	paste(s);
      }
      catch(Exception ex1)
      {
	getToolkit().beep();
	//try { term_.error_.write(ex1.getMessage().getBytes()); }
	//catch(IOException ex2) { }
      }
    }
  }

  public void paste(String _s)
  {
    if((_s!=null)&&(input_!=null)) input_.paste(_s);
  }
  */

  // Shortcuts

  public void showShortcuts()
  {
    scp_.setVisible(true);
    scp_.revalidate();
  }

  public void hideShortcuts()
  {
    scp_.setVisible(false);
    scp_.revalidate();
  }

  public void setShortcuts(String[] _ba)
  {
    if(scp_==null)
    {
      scp_=new BuPanel();
      scp_.setLayout(new BuVerticalLayout(2,true,false));
      scp_.setBorder(BuBorders.EMPTY2222);
      add(scp_,BuBorderLayout.WEST);
    }
    else scp_.removeAll();

    // String[]  ba=new String[] { "1+2\r",null,"5+6\r","a\tbbbb\tc" };
    JButton[] b =new JButton[11]; // ba.length;

    for(int i=0;i<11;i++)
    {
      b[i]=new BuButton("F"+(i+2))
	{
	  @Override
    public boolean isFocusTraversable() { return false; }
	};
      b[i].setRequestFocusEnabled(false);
      b[i].setMargin(BuInsets.INSETS1111);
      scp_.add(b[i]);

      if((i>=_ba.length)||(_ba[i]==null)||"".equals(_ba[i]))
	b[i].setEnabled(false);
      else
      {
	Shortcut sc=new Shortcut(this,_ba[i]);
	registerKeyboardAction
	  (sc,
	   KeyStroke.getKeyStroke(KeyEvent.VK_F1+(i+1),0,false),
	   JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
	b[i].addActionListener(sc);
	b[i].setActionCommand(_ba[i]);

	String tt=_ba[i];
	tt=tt.replace('\t',' ');
	tt=tt.replace('\r',' ');
	tt=tt.replace('\n','\u21B5');
        b[i].setToolTipText(tt);
      }
    }

    revalidate();
    scp_.repaint();
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String action=_evt.getActionCommand();
    // System.err.println("action="+action);
    if(action!=null)
    {
      if(input_!=null) input_.paste(action);
      getDisplay().requestFocus();
    }
  }

  static class Shortcut implements ActionListener
  {
    String         action_;
    ActionListener listener_;

    public Shortcut(ActionListener _listener, String _action)
    {
      action_=_action;
      listener_=_listener;
    }

    @Override
    public void actionPerformed(ActionEvent _evt)
    {
      listener_.actionPerformed(new ActionEvent
				(_evt.getSource(),_evt.getID(),action_));
    }
  }

  /*
  public static void main(String args[]) throws IOException
  {
    BuTerminal t=new BuTerminal();
    t.input_=t.getDisplay().createInStream
      (new Color(0,128,255),BuTerminalDisplay.NORMAL,true);
    t.output_=t.getDisplay().createOutStream
      (t.getDisplay().getForeground(),BuTerminalDisplay.NORMAL,true);
    t.error_=t.getDisplay().createOutStream
      (new Color(255,128,0),BuTerminalDisplay.NORMAL,true);

    final JFrame f=new JFrame("Terminal");
    f.getContentPane().setLayout(new BuBorderLayout());
    f.getContentPane().add(t,BuBorderLayout.CENTER);
    f.setIconImage(BuResource.BU.getFrameIcon("terminal").getImage());
    f.pack();
    f.show();
    // not in 1.1
    //f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    BuRegistry.register(f);

    f.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent _evt)
      {
	f.hide();
	f.dispose();
	BuRegistry.unregister(f);
      }
    });

    t.output_.write("Hello!\n".getBytes());
    int c='\n';
    while(true)
    {
      if(c=='\n') t.error_.write("> ".getBytes());
      c=t.input_.read();
    }
  }
  */
}
