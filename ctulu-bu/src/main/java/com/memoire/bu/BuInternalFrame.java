/**
 * @modification $Date$
 * @statut       unstable
 * @file         BuInternalFrame.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuFactoryInteger;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.lang.reflect.Method;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JRootPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

/**
 * An internal frame having specific menus, tools and actions.
 */
public class BuInternalFrame
  extends JInternalFrame
  implements ActionListener, FuEmptyArrays
{
  public static final String SHORTCUT="BU_SHORTCUT";

  private static final JComponent[] JCOMPONENT0=new JComponent[0];
  private static final JMenu[]      JMENU0     =new JMenu[0];

  public BuInternalFrame()
  {
    this("",true,true,true,true);
  }

  public BuInternalFrame(String _title,
			 boolean _resizable, boolean _closable,
			 boolean _maximizable, boolean _iconifiable)
  {
    super(_title,_resizable,_closable,_maximizable,_iconifiable);

    getRootPane().putClientProperty
      ("defeatSystemEventQueueCheck",Boolean.TRUE);
    getGlassPane().setName(null);

    setLayer(JLayeredPane.DEFAULT_LAYER);

    if(FuLib.jdk()>=1.5)
    {
      try
      {
        Class  c=getClass();
        Method m=c.getMethod("setFocusTraversalPolicyProvider",
                             new Class[] { Boolean.TYPE });
        m.invoke(this,new Object[] { Boolean.TRUE });

        //setFocusTraversalPolicy(new LayoutFocusTraversalPolicy());
      }
      catch(Throwable th) { FuLog.error(th); }
      //setFocusTraversalPolicyProvider(true);
    }

    try { setSelected(false); }
    catch(PropertyVetoException ex) { }

    //setLocation(10,10);
    setSize(111,111);
  }

  // Translation

  protected String getString(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  // Properties

  @Override
  public void setRootPane(JRootPane _rootpane)
  {
    super.setRootPane(_rootpane);
  }

  @Override
  public void setRootPaneCheckingEnabled(boolean _enabled)
  {
    super.setRootPaneCheckingEnabled(_enabled);
  }

  @Override
  public void setVisible(boolean _visible)
  {
    boolean maximum=isMaximum();

    //TMP @GDX
    if(_visible)
    {
      //BuLib.setDoubleBuffered(this,false);
      //setDoubleBuffered(true);
      if(!isVisible()&&(getParent()!=null))
      {
	try { setMaximum(false); }
	catch(PropertyVetoException ex) { }
      }
    }

    super.setVisible(_visible);

    if(isVisible()&&!isMaximum()&&maximum&&(getParent()!=null))
    {
      try { setMaximum(true); }
      catch(PropertyVetoException ex) { }
    }
  }

  @Override
  public void setClosed(boolean _closed) throws PropertyVetoException
  {
    super.setClosed(_closed);
    if(isClosed()&&isSelected()) setSelected(false);
    // should be done before ?
  }

  /*
  public void setSelected(boolean _b) throws PropertyVetoException
  {
    boolean was=isSelected();
    super.setSelected(_b);
    System.err.println
      (getTitle()+" "+was+" "+_b+" "+isSelected());
    if(_b&&!was&&isSelected()) moveToFront();
  }
  */

  private boolean simplified_;

  /*
  public void paint(Graphics _g)
  {
    if((!simplified_||isSelected())&&super.isVisible())
    {
      BuLib.setAntialiasing(this,_g);
      super.paint(_g);
    }
  }
  */

  public void simplifyTop()
  {
    // if(simplified_) return;

    simplified_=true;
    setRootPaneCheckingEnabled(false);

    /*
    for(int i=0;i<getComponentCount();i++)
    {
      Component c=getComponent(i);
      System.err.println("C["+i+"]="+c.getClass());
      if(c instanceof BasicInternalFrameTitlePane)
	remove(c);
      // add(new JLabel(getTitle()),BorderLayout.NORTH);
    }
    */

    Component c=getRootPane();

    BuPanel top=new BuPanel();
    top.setLayout(new BuBorderLayout());

    BuLabel l=new BuLabel();
    l.setText(getTitle());
    l.setBorder(BuBorders.EMPTY1111);
    // l.setFont(BuLib.deriveFont("Label",-2));
    l.setHorizontalAlignment(SwingConstants.CENTER);

    // l.setHorizontalTextPosition(JLabel.RIGHT);
    // l.setBackground(new Color(128,224,192));
    // l.setForeground(Color.black);
    // l.setRequestFocusEnabled(false);

    top.add(l,BorderLayout.CENTER);

    if(isClosable())
    { 
      Icon icon=UIManager.getIcon("InternalFrameTitlePane.closeIcon");
      if(icon==null) icon=UIManager.getIcon("InternalFrame.closeIcon");
      if(icon==null) icon=BuResource.BU.getMenuIcon("fermer");

      BuButton b=new BuButton();
      b.setBorder(BuBorders.EMPTY1111);
      b.setRequestFocusEnabled(false);
      b.setIcon(icon);
      top.add(b,BorderLayout.EAST);

      b.addActionListener(new ActionListener()
      {
        @Override
        public void actionPerformed(ActionEvent _ae)
        {
	  try { setClosed(true); }
	  catch(PropertyVetoException ex) { }
	}
      }
      );
    }

    setBorder(UIManager.getBorder("TitleBorder.border"));

    removeAll();
    setLayout(new BuBorderLayout());
    add(c,BuBorderLayout.CENTER);
    add(top,BuBorderLayout.NORTH);
    getLayout().layoutContainer(this);
    // setOpaque(true);
    repaint();
  }

  public static int getShortcut(JInternalFrame _f)
  {
    int r=0;
    if(_f!=null)
    {
      Object o=_f.getClientProperty(SHORTCUT);
      if(o!=null) r=((Integer)o).intValue();
    }
    return r;
  }

  public int getShortcut()
    { return getShortcut(this); }

  public void setShortcut(int _key)
  {
    putClientProperty(SHORTCUT,FuFactoryInteger.get(_key));
  }

  /*public boolean isFocusCycleRoot()
    { return true; }*/

  public String toString()
  {
    String      r=getName();
    if(r==null) r=getTitle();
    if(r==null) r=super.toString();
    return r;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String action=_evt.getActionCommand();
    FuLog.error("BIF: illegal action on "+getClass().getName()+": "+action);
  }

  public String[] getEnabledActions()
  {
    return STRING0;
  }

  public String[] getDisabledActions()
  {
    return STRING0;
  }

  /**
   * Retourne les menus sp�cifiques pour la fenetre.
   * Les menus sp�cifiques sont switch�s d�s que la fenetre passe de l'�tat actif � inactif
   * et inversement. Ils sont ajout�s dans la barre de menus.
   * @return Les menus sp�cifiques.
   */
  public JMenu[] getSpecificMenus()
  {
    return JMENU0;
  }

  /**
   * Retourne les boutons outils sp�cifiques pour la fenetre.
   * Les outils sp�cifiques sont switch�s d�s que la fenetre passe de l'�tat actif � inactif
   * et inversement. Ils remplacent les outils existants.
   * @return Les composants sp�cifiques.
   */
  public JComponent[] getSpecificTools()
  {
    return JCOMPONENT0;
  }

  public void adjustActions()
  {
  }

  @Override
  public void pack()
  {
    boolean old=isResizable();
    setResizable(true);
    super.pack();
    setResizable(old);
  }

  @Override
  public void updateUI()
  {
    super.updateUI();

    if(!isResizable())
    {
      //SwingUtilities.updateComponentTreeUI(getContentPane());
      pack();
    }
    //else SwingUtilities.updateComponentTreeUI(getContentPane());

    Container cp=getContentPane();
    if((cp!=null)&&(cp.getBackground()==null))
      cp.setBackground
	(UIManager.getColor("Panel.background"));

    if(simplified_) simplifyTop();
    //BuLib.setDoubleBuffered(this,false);
  }

  /*
  public final void paintBorder(Graphics _g) { }

  protected void paintSuperBorder(Graphics _g)
  {
    super.paintBorder(_g);
  }

  public void paint(Graphics _g)
  {
    super.paint(_g);
    paintSuperBorder(_g);
  }
  */
}
