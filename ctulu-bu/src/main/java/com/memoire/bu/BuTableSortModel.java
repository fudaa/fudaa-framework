package com.memoire.bu;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuSortint;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;

public class BuTableSortModel
        extends BuTableDelegateModel
        implements FuEmptyArrays, FuSortint.Comparator {

  protected int[] indexes;
  protected int nbcols_;
  protected int[] columns_;
  protected boolean[] descending_;
  protected int compares;

  public BuTableSortModel() {
    indexes = INT0;
    nbcols_ = 0;
    columns_ = new int[3];
    descending_ = new boolean[3];
  }

  public BuTableSortModel(TableModel _model) {
    this();
    setModel(_model);
  }

  @Override
  public void setModel(TableModel _model) {
    super.setModel(_model);
    reallocateIndexes();
  }

  protected int compareRowsByColumn(int _row1, int _row2, int _col) {
    Object o1 = model_.getValueAt(_row1, _col);
    Object o2 = model_.getValueAt(_row2, _col);

    if ((o1 == null) && (o2 == null)) {
      return 0;
    } else if (o1 == null) {
      return -1;
    } else if (o2 == null) {
      return 1;
    }

    if ((o1 instanceof Number) && (o2 instanceof Number)) {
      double d1 = ((Number) o1).doubleValue();
      double d2 = ((Number) o2).doubleValue();

      if (d1 < d2) {
        return -1;
      } else if (d1 > d2) {
        return 1;
      } else {
        return 0;
      }
    } else if ((o1 instanceof Comparable) && (o2 instanceof Comparable) && o1.getClass().isInstance(o2)) {
      return ((Comparable) o1).compareTo(o2);

    } else if ((o1 instanceof Date) && (o2 instanceof Date)) {
      long n1 = ((Date) o1).getTime();
      long n2 = ((Date) o2).getTime();

      if (n1 < n2) {
        return -1;
      } else if (n1 > n2) {
        return 1;
      } else {
        return 0;
      }
    } else if ((o1 instanceof Boolean) && (o2 instanceof Boolean)) {
      boolean b1 = ((Boolean) o1).booleanValue();
      boolean b2 = ((Boolean) o2).booleanValue();

      if (b1 == b2) {
        return 0;
      } else if (b1) {
        return 1;
      } else {
        return -1;
      }
    } else {
      String s1 = o1.toString();
      String s2 = o2.toString();

      int result = s1.compareTo(s2);
      if (result < 0) {
        return -1;
      } else if (result > 0) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  @Override
  public int compare(int _row1, int _row2) {
    compares++;

    int nc = model_.getRowCount();

    for (int i = 0; i < nbcols_; i++) //for(int i=nbcols_-1;i>=0;i--)
    {
      int c = columns_[i];
      if ((c < 0) || (c >= nc)) {
        continue;
      }

      int s = compareRowsByColumn(_row1, _row2, c);
      if (descending_[i]) {
        s = -s;
      }
      if (s != 0) {
        return s;
      }
    }
    return 0;
  }

  protected void reallocateIndexes() {
    int n = model_.getRowCount();
    indexes = new int[n];
    for (int i = 0; i < n; i++) {
      indexes[i] = i;
    }
  }

  private boolean sortOnUpdate_;

  public boolean isSortOnUpdate() {
    return sortOnUpdate_;
  }

  public void setSortOnUpdate(boolean _sortOnUpdate) {
    sortOnUpdate_ = _sortOnUpdate;
  }

  @Override
  public void tableChanged(TableModelEvent _evt) {
    //System.out.println("Sorter: tableChanged"); 

    int type = _evt.getType();
    int firstrow = _evt.getFirstRow();
    int lastrow = _evt.getLastRow();

    if (isSortOnUpdate() || (type != TableModelEvent.UPDATE)) {
      reallocateIndexes();
    }

    if (_evt.getSource() == model_) {
      firstrow = getReverseRowIndex(firstrow);
      lastrow = getReverseRowIndex(lastrow);
    }

    TableModelEvent e = new TableModelEvent(this, firstrow, lastrow, _evt.getColumn(), type);
    super.tableChanged(e);
  }

  public void checkModel() {
    if (indexes.length != model_.getRowCount()) {
      System.err.println("BTS: sorter not informed of a change in model");
    }
  }

  private void sort() {
    checkModel();

    compares = 0;
    //n2sort();
    FuSortint.sort(indexes, this);
    //qsort(0, indexes.length-1);
    //shuttlesort((int[])indexes.clone(), indexes, 0, indexes.length);
    //System.out.println("Compares: "+compares);
  }

  /*private void n2sort()
  {
    int n=getRowCount();
    for(int i=0;i<n;i++)
      for(int j=i+1;j<n;j++)
        if(compare(indexes[i],indexes[j])==-1)
          swap(i,j);
  }*/

 /*
  private void shuttlesort(int from[], int to[], int low, int high)
  {
    if (high - low < 2) {
      return;
    }
    int middle = (low + high)/2;
    shuttlesort(to, from, low, middle);
    shuttlesort(to, from, middle, high);

    int p = low;
    int q = middle;

    if (high - low >= 4 && compare(from[middle-1], from[middle]) <= 0) {
      for (int i = low; i < high; i++) {
        to[i] = from[i];
      }
      return;
    }

    for (int i = low; i < high; i++) {
      if (q >= high || (p < middle && compare(from[p], from[q]) <= 0)) {
        to[i] = from[p++];
      }
      else {
        to[i] = from[q++];
      }
    }
  }
   */
 /*private void swap(int i, int j)
  {
    int tmp=indexes[i];
    indexes[i]=indexes[j];
    indexes[j]=tmp;
  }*/
  public int getRowIndex(int _row) {
    checkModel();
    return indexes[_row];
  }

  private int getReverseRowIndex(int _row) {
    checkModel();
    int l = indexes.length;
    if ((_row < 0) || (_row >= l)) {
      return _row;
    }
    if (indexes[_row] == _row) {
      return _row;
    }
    for (int i = 0; i < l; i++) {
      if (_row == indexes[i]) {
        return i;
      }
    }

    System.err.println("BTS: reverse index not found");
    return -1;
  }

  @Override
  public Object getValueAt(int _row, int _column) {
    checkModel();
    return model_.getValueAt(indexes[_row], _column);
  }

  @Override
  public void setValueAt(Object _value, int _row, int _column) {
    checkModel();
    model_.setValueAt(_value, indexes[_row], _column);
    //fireTableCellUpdate(_row,_column);
  }

  public void setSortingColumn(int _column) {
    setSortingColumn(_column, true);
  }

  public void setSortingColumn(int _column, boolean _ascending) {
    columns_[0] = _column;
    descending_[0] = !_ascending;
    nbcols_ = 1;
    sort();
    super.tableChanged(new TableModelEvent(this));
  }

  public void addSortingColumn(int _column, boolean _ascending) {
    int l = columns_.length;
    if (nbcols_ == l) {
      int[] columns = new int[l + 3];
      System.arraycopy(columns_, 0, columns, 0, l);
      columns_ = columns;
      boolean[] descending = new boolean[l + 3];
      System.arraycopy(descending_, 0, descending, 0, l);
      descending_ = descending;
    }

    columns_[nbcols_] = _column;
    descending_[nbcols_] = !_ascending;
    nbcols_++;
    sort();
    super.tableChanged(new TableModelEvent(this));
  }

  public void removeSortingColumn(int _col) {
    for (int i = 0; i < nbcols_; i++) {
      if (columns_[i] == _col) {
        if (i + 1 < nbcols_) {
          System.arraycopy(columns_, i + 1, columns_, i, nbcols_ - i);
          System.arraycopy(descending_, i + 1, descending_, i, nbcols_ - i);
        }
        nbcols_--;
        break;
      }
    }
  }

  public int getSortingColumn() {
    if (nbcols_ > 0) {
      return columns_[0];
    }
    return -1;
  }

  public int[] getSortingColumns() {
    if (nbcols_ == 0) {
      return INT0;
    }

    int[] r = new int[nbcols_];
    System.arraycopy(columns_, 0, r, 0, nbcols_);
    //for(int i=0;i<nbcols_;i++) r[i]=columns_[i];
    return r;
  }

  public boolean isAscending() {
    if (nbcols_ == 0) {
      return true;
    }
    return !descending_[0];
  }

  public boolean isAscending(int _col) {
    for (int i = 0; i < nbcols_; i++) {
      if (columns_[i] == _col) {
        return !descending_[i];
      }
    }
    return true;
  }

  public void setAscending(boolean _ascending) {
    setAscending(0, _ascending);
  }

  public void setAscending(int _col, boolean _ascending) {
    //System.err.println("BTS: setAscending "+_col+" "+_ascending);

    for (int i = 0; i < nbcols_; i++) {
      if (columns_[i] == _col) {
        if (descending_[i] == _ascending) {
          descending_[i] = !_ascending;
          sort();
          super.tableChanged(new TableModelEvent(this));
          break;
        }
      }
    }
  }

  public boolean isSortingColumn(int _col) {
    for (int i = 0; i < nbcols_; i++) {
      if (_col == columns_[i]) {
        return true;
      }
    }
    return false;
  }

  /**
   * @deprecated use install() instead
   */
  public void addMouseListenerToHeaderInTable(JTable _table) {
    install(_table);
  }

  /**
   * Install a mouse listener on the table header.
   */
  public void install(final JTable _table) {
    //_table.setColumnSelectionAllowed(false); 
    MouseAdapter listener = new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent _evt) {
        if (!BuLib.isLeft(_evt)) {
          return;
        }
        if (_evt.getClickCount() != 1) {
          return;
        }

        int column = _table.columnAtPoint(new Point(_evt.getX(), _evt.getY()));
        if (column == -1) {
          return;
        }
        column = _table.convertColumnIndexToModel(column);
        if (column == -1) {
          return;
        }

        if (isSortingColumn(column)) {
          if ((column == getSortingColumn()) || !BuLib.isCtrl(_evt)) {
            setAscending(column, !isAscending(column));
          } else if (!BuLib.isShift(_evt)) {
            removeSortingColumn(column);
          }
        } else if (BuLib.isCtrl(_evt) || BuLib.isShift(_evt)) {
          addSortingColumn(column, true);
        } else {
          setSortingColumn(column, true);
        }
      }
    };
    JTableHeader header = _table.getTableHeader();
    header.addMouseListener(listener);
  }
}
