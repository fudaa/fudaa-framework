/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuLabelInfo.java
 * @version      0.41
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

/**
 * Little note.
 */
public final class BuLabelInfo extends BuLabelLink implements /* MouseListener, fred inutile */BuBorders {
  private static final Color FG = BuLib.getColor(Color.black);
  private static final Color BG = BuLib.getColor(new Color(255, 255, 224));
  private static final Border BD = new CompoundBorder(UIManager.getBorder("Theme.border") != null ? UIManager
      .getBorder("Theme.border") : new LineBorder(BuLib.getColor(BG.darker()), 1), EMPTY5555);
  private static final Font FT = BuLib.deriveFont(UIManager.getFont("Label.font"), Font.PLAIN, -2);

  public BuLabelInfo(String _text) {
    this(_text, null, null);
  }

  public BuLabelInfo(String _text, String _url, ActionListener _listener) {
    super(_text, _url, _listener);

    setWrapMode(WORD);
    setFont(FT);
    setOpaque(true);
    setBorder(BD);
    setForeground(FG);
    setBackground(BG);
    // setHorizontalAlignment(LEFT);
    setVerticalAlignment(TOP);

    // listener_=_listener;
    // setURL(_url);
  }

  /*
   * public void setURL(String _url) { if(_url!=null) { try { url_=new URL(_url); } catch(MalformedURLException ex) {
   * url_=null; } } else url_=null; DndSource.remove(this); setCursor(null); removeMouseListener(this); if(url_!=null) {
   * if(listener_!=null) { setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); addMouseListener(this); } new
   * DndSource(new DndRequestData() { public Object[] request(JComponent _component,Point _location) { return new
   * Object[] { url_, url_.toString() }; } }).add(this); } }
   */

  /*
   * public void mousePressed(MouseEvent _evt) { } public void mouseReleased(MouseEvent _evt) { } public void
   * mouseEntered(MouseEvent _evt) { if(!rollover_) { rollover_=true; repaint(); } } public void mouseExited(MouseEvent
   * _evt) { if(rollover_) { rollover_=false; repaint(); } } public void mouseClicked(MouseEvent _evt) { String
   * action="OUVRIR"; int m=_evt.getModifiers(); if((m&MouseEvent.ALT_MASK )!=0) action="EDITER";
   * if((m&MouseEvent.SHIFT_MASK)!=0) action="ENREGISTER"; if((m&MouseEvent.CTRL_MASK )!=0) action="VOIR";
   * listener_.actionPerformed (new ActionEvent (this,ActionEvent.ACTION_PERFORMED,action+"("+url_+")")); }
   */
}
