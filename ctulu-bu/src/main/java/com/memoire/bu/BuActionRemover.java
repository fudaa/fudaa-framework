/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuActionRemover.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.MenuElement;

/**
 * An utility class to remove controls by their command name.
 * The command name is a string defined for each control
 * using setActionCommand().
 */

public class BuActionRemover
{
  // Menu

  public static void removeAction(JMenuBar _bar, String _action)
  {
    if(_bar==null) return;

    MenuElement[] c=_bar.getSubElements();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JMenu)
	removeAction((JMenu)c[i],_action);
  }

  public static void removeAction(JMenu _menu, String _action)
  {
    if(_menu==null) return;

    if(_action.equals(_menu.getActionCommand()))
    {
      Container c=_menu.getParent();
      c.remove(_menu);

      /*
      try
      {
	c.doLayout();
	c.validate();
      }
      catch(ArrayIndexOutOfBoundsException ex) { }
      */
    }

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      //System.err.println("JM: "+_menu.getName()+" "+c[i].getClass());
      if(c[i] instanceof JMenu)
	removeAction((JMenu)c[i],_action);
      else
      if(c[i] instanceof JPopupMenu)
	removeAction((JPopupMenu)c[i],_action);
      else
      if(c[i] instanceof JMenuItem)
	removeAction((JMenuItem)c[i],_action);
      //else
      //System.err.println("BAR: strange "+c[i]);
    }
  }

  public static void removeAction(JPopupMenu _menu, String _action)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      //System.err.println("JP: "+_menu.hashCode()+" "+c[i].getClass());
      if(c[i] instanceof JMenu)
	removeAction((JMenu)c[i],_action);
      else
      if(c[i] instanceof JPopupMenu)
	removeAction((JPopupMenu)c[i],_action);
      else
      if(c[i] instanceof JMenuItem)
	removeAction((JMenuItem)c[i],_action);
      //else
      //System.err.println("BAR: strange "+c[i]);
    }
  }

  public static void removeAction(JMenuItem _item, String _action)
  {
    if(_item==null) return;

    if(_action.equals(_item.getActionCommand()))
    {
      //System.err.println("JI: "+_item.getText());
      Container c=_item.getParent();
      c.remove(_item);

      /*
      try
      {
	c.doLayout();
	c.validate();
      }
      catch(ArrayIndexOutOfBoundsException ex) { }
      */
    }
  }

  // Tool

  public static void removeAction(JToolBar _bar, String _action)
  {
    if(_bar==null) return;

    Component[] c=_bar.getComponents();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JButton)
	removeAction((JButton)c[i],_action);
  }

  public static void removeAction(JButton _item, String _action)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
    {
      Container c=_item.getParent();
      c.remove(_item);

      /*
      try
      {
	c.doLayout();
	c.validate();
      }
      catch(ArrayIndexOutOfBoundsException ex) { }
      */
    }
  }
}
