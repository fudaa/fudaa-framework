/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuMenuBar.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.MenuElement;

/**
 * A menu bar with simplified adding methods.
 */
public class BuMenuBar
  extends JMenuBar
  implements ActionListener
{
  public BuMenuBar()
  {
    super();
    setName("mbMAIN_MENUBAR");
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  // Mnemonics

  public /*synchronized*/ void computeMnemonics()
  {
    Hashtable t=new Hashtable();

    MenuElement[] c=getSubElements();

    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
      {
	JMenu mi=(JMenu)c[i];
	int mn=mi.getMnemonic();
	if(mn<=0)
        {
	  String tx=BuLib.candidateMnemonics(mi.getText());
	  if(tx!=null)
	    for(int j=0;j<tx.length();j++)
	    {
	      mn=tx.charAt(j);
	      if(t.get(FuFactoryInteger.get(mn))==null)
	      {
		t.put(FuFactoryInteger.get(mn),mi);
		mi.setMnemonic(mn);
		break;
	      }
	    }
	}
	if(mi instanceof BuMenu)
	  ((BuMenu)mi).computeMnemonics();
      }
    }
  }

  // Separators

  public void removeDummySeparators()
  {
    MenuElement[] c=getSubElements();
    if(c==null) return;

    for(int i=0;i<c.length;i++)
      if(c[i] instanceof BuMenu)
	((BuMenu)c[i]).removeDummySeparators();
  }

  // Menu

  public /*synchronized*/ void insertMenu(JMenu _menu)
  {
    //SwingUtilities.updateComponentTreeUI(_menu);

    Component[] c=getComponents();
    for(int i=0;i<c.length;i++)
      remove(c[i]);
    add(_menu);
    for(int i=0;i<c.length;i++)
      add(c[i]);
  }

  public /*synchronized*/ void addMenu(JMenu _menu)
  {
    //SwingUtilities.updateComponentTreeUI(_menu);

    int n=getSubElements().length;
    if(n>=2)
    {
      n-=2;
      Component fm=getComponent(n  ); // Fenetres
      Component hm=getComponent(n+1); // Aide

      remove(hm);
      remove(fm);
      add(_menu);
      add(fm);
      add(hm);

      /*
	add(_menu,n  );
	add(fm   ,n+1);
	add(hm   ,n+2);
      */
    }
    else add(_menu);

    _menu.setIcon(null);
    _menu.addActionListener(this);
  }

  public void addMenus(JMenu[] _menus)
  {
    if((_menus==null)||(_menus.length==0)) return;

    for(int i=0;i<_menus.length;i++)
      addMenu(_menus[i]);

    revalidate();

    if(isShowing())
      for(int i=0;i<_menus.length;i++)
	_menus[i].repaint(0);
  }

  public /*synchronized*/ void removeMenu(JMenu _menu)
  {
    MenuElement[] c=getSubElements();
    for(int i=0; i<c.length; i++)
      if(c[i]==_menu)
	remove(_menu);
  }

  public void removeMenus(JMenu[] _menus)
  {
    if((_menus==null)||(_menus.length==0)) return;

    for(int i=0;i<_menus.length;i++)
      removeMenu(_menus[i]);

    revalidate();
  }

  public static JMenu getMenu(MenuElement _menu, String _name)
  {
    JMenu r=null;

    //synchronized(_menu)
    {
      MenuElement[] c=_menu.getSubElements();
      for(int i=0; i<c.length; i++)
      {
	if(c[i] instanceof BuMenu)
	{
	  JMenu m=(JMenu)c[i];
	  // System.err.println(m.getName());

	  if(m.getName().equals("mn"+_name))
	    { r=m; break; }
	}

	r=getMenu(c[i],_name);
	if(r!=null) break;
      }
    }

    return r;
  }

  public JMenu getMenu(String _name)
  {
    return getMenu(this,_name);
  }

  public static JMenuItem getMenuItem(MenuElement _menu, String _name)
  {
    JMenuItem r=null;

    //synchronized(_menu)
    {
      MenuElement[] c=_menu.getSubElements();

      for(int i=0; i<c.length; i++)
      {
	if(c[i] instanceof JMenuItem)
	{
	  JMenuItem m=(JMenuItem)c[i];
	  // System.err.println(m.getName());

	  if(m.getName().equals("mi"+_name))
	    { r=m; break; }
	}

	r=getMenuItem(c[i],_name);
	if(r!=null) break;
      }
    }

    return r;
  }

  public JMenuItem getMenuItem(String _name)
  {
    return getMenuItem(this,_name);
  }

  /**
   * Retourne le menu qui contient l'item de commande donn�e, recursivement a 
   * partir de la barre de menus.
   * @param _cmd La commande
   * @return Le menu
   */
  public JMenu getMenuForAction(String _cmd)
  {
    return getMenuForAction(this,_cmd);
  }

  /**
   * Retourne le menu qui contient l'item de commande donn�e, recursivement a 
   * partir du menu en parametres.
   * @param _cmd La commande
   * @return Le menu
   */
  public static JMenu getMenuForAction(MenuElement _menu, String _cmd) {
    if (_cmd==null) return null;

    JMenu r=null;

    MenuElement[] c=_menu.getSubElements();

    for (int i=0; i<c.length; i++) {
      if (c[i] instanceof JMenuItem) {
        JMenuItem m=(JMenuItem)c[i];

        if (m.getActionCommand().equals(_cmd)) {
          r=(JMenu)((JPopupMenu)m.getParent()).getInvoker();
          break;
        }
      }

      r=getMenuForAction(c[i], _cmd);
      if (r!=null)
        break;
    }

    return r;
  }

  /*
   * public void removeMenu(String _name) { MenuElement[] c=getSubElements();
   * for(int i=0; i<c.length; i++) if(c[i] instanceof JMenu) { JMenu
   * m=(JMenu)c[i]; if(m.getName().equals("mn"+_name)) remove(m); } doLayout();
   * validate(); }
   */

  public void addRecentFile(String _path, String _icon)
  {
    BuMenu m=(BuMenu)getMenu("REOUVRIR");
    if(m instanceof BuMenuRecentFiles)
      ((BuMenuRecentFiles)m).addRecentFile(_path,_icon);
  }

  public static BuMenuBar buildBasicMenuBar()
  {
    BuMenuBar r=new BuMenuBar();

    BuMenu em=BuMenu.buildEditionMenu ();
    BuMenu fm=BuMenu.buildFileMenu    ();
    BuMenu wm=BuMenu.buildWindowMenu  ();
    BuMenu hm=BuMenu.buildHelpMenu    ();

    fm.setIcon(null);
    em.setIcon(null);
    wm.setIcon(null);
    hm.setIcon(null);

    fm.addActionListener(r);
    em.addActionListener(r);
    wm.addActionListener(r);
    hm.addActionListener(r);

    r.add(fm);
    r.add(em);
    r.add(wm);
    try
    {
      r.setHelpMenu(hm);
    }
    catch(Error ex)
    {
      // r.add(Box.createHorizontalGlue());
      r.add(hm); 
    }

    return r;
  }

  // Action

  private Vector actionListeners_=new Vector();

  public void addActionListener(ActionListener _l)
  { actionListeners_.addElement(_l); }

  public void removeActionListener(ActionListener _l)
  { actionListeners_.removeElement(_l); }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    // JComponent source=(JComponent)_evt.getSource();
    // System.err.println(""+getClass()+": "+source);

    for(Enumeration e=actionListeners_.elements(); e.hasMoreElements(); )
      ((ActionListener)e.nextElement()).actionPerformed(_evt);
  }
}
