/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuRadioButtonMenuItem.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JRadioButtonMenuItem;

/**
 * Like JRadioButtonMenuItem but with better management of icons.
 */

public class BuRadioButtonMenuItem
       extends JRadioButtonMenuItem
{
  public BuRadioButtonMenuItem()
  {
    this(null,"");
  }

  public BuRadioButtonMenuItem(BuIcon _icon)
  {
    this(_icon,"");
  }

  public BuRadioButtonMenuItem(String _label)
  {
    this(null,_label);
  }

  public BuRadioButtonMenuItem(BuIcon _icon, String _label)
  {
    super();
    if(_icon!=null) setIcon(_icon);
    setText(_label==null?"":_label);
  }

  // Icon

  @Override
  public Icon getIcon()
  {
    if(BuPreferences.BU.getBooleanProperty("icons.menu",true)||
       (super.getText()==null))
      return super.getIcon();
    return null;
  }

  public void setIcon(BuIcon _icon)
  {
    BuLib.setIcon(this,_icon);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(getWidth(),0);
    return r;
  }
}
