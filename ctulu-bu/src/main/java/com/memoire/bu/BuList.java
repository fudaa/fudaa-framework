/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuList.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseEvent;
import java.util.TooManyListenersException;
import java.util.Vector;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 * The Swing list.
 */
public class BuList
  extends JList
  implements DtDragSensible
{
  public BuList()
  {
    super();
    init();
  }

  public BuList(ListModel _model)
  {
    super(_model);
    init();
  }

  public BuList(Object[] _data)
  {
    super(_data);
    init();
  }

  public BuList(Vector _data)
  {
    super(_data);
    init();
  }

  protected void init()
  {
    setCellRenderer(BuAbstractCellRenderer.getDefaultListCellRenderer());
  }


  private long prevevt_;

  @Override
  public int getScrollableUnitIncrement(Rectangle _visibleRect,
                                        int _orientation,
                                        int _direction)
  {
    int r=super.getScrollableUnitIncrement
      (_visibleRect,_orientation,_direction);

    if((_orientation==SwingConstants.HORIZONTAL)/*&&(FuLib.jdk()>=1.4)*/)
    {
      if((getLayoutOrientation()==JList.VERTICAL_WRAP)&&
         (getCellRenderer() instanceof BuAbstractCellRenderer))
      {
        BuAbstractCellRenderer cr=(BuAbstractCellRenderer)getCellRenderer();
        int w=cr.getItemWidth();
        if(w>=30)
        {
          long now=System.currentTimeMillis();
          if(now>prevevt_+350)
          {
            int x=_visibleRect.x%w;
            if(_orientation>=0)
            {
              if(x<10) r=w+x;
              else r=x;
            }
            else
            {
              if(x>w-10) r=w+x;
              else r=x;
            }
            //FuLog.debug("W="+w+" X="+x);
            prevevt_=now;
          }
          else r=0;
        }
      }
    }
    return r;
  }

  // Translation

  protected String getString(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);

    if(drop_)
    {
      Rectangle r=getDropRect();
      //_g.setColor(new Color(255,128,0,64));
      //_g.fillRoundRect(r.x,r.y,r.width,r.height,9,9);
      _g.setColor(new Color(255,128,0));
      _g.drawRoundRect(r.x,r.y,r.width-1,r.height-1,9,9);
    }
  }

  // Drop

  @Override
  public void setDropTarget(DropTarget _dt)
  {
    super.setDropTarget(_dt);
    if(_dt!=null)
    {
      //FuLog.debug("BLI: setDropTarget");
      try { _dt.removeDropTargetListener(DtDragSensible.SINGLETON); }
      catch(IllegalArgumentException ex) { }
      try { _dt.addDropTargetListener(DtDragSensible.SINGLETON); }
      catch(TooManyListenersException ex) { }
    }
  }

  protected boolean drop_;

  protected Rectangle getDropRect()
  {
    Rectangle r=getVisibleRect();
    /*
    Insets    i=getInsets();
    r.x     +=3;
    r.y     +=3;
    r.width -=6;
    r.height-=6;
    */

    if(isDropSelectionMode())
    {
      int i=getSelectedIndex();
      if(i>=0)
      {
        Rectangle b=getCellBounds(i,i);
        r=r.intersection(b);
      }
    }
    return r;
  }

  protected boolean isDropSelectionMode()
  {
    return false;
  }

  @Override
  public void dragEnter(boolean _acceptable, Point _location)
  {
    if(_acceptable&&isDropSelectionMode()) clearSelection();
    dragOver(_acceptable,_location);
  }

  @Override
  public void dragOver(boolean _acceptable, Point _location)
  {
    if(_acceptable)
    {
      if(isDropSelectionMode())
      {
        int i=locationToIndex(_location);
        if(i>=0)
        {
          Rectangle r=getCellBounds(i,i);
          //FuLog.debug("R="+r+" P="+_location);
          //FuLog.debug(r.contains(_location) ? "inside" : "outside");
          if(!r.contains(_location))
          {
            i=-1;
          }
        }
        if(i>=0) setSelectedIndex(i);
        else     clearSelection();
        drop_=false;
      }
      else
      {
        //clearSelection();
        drop_=false;
      }
    }
    if(!drop_&&_acceptable) { drop_=true; repaint(); }
  }

  @Override
  public void dragExit()
  {
    if(isDropSelectionMode()) clearSelection();
    if(drop_) { drop_=false; repaint(); }
  }

  // bug swing 1.0
  @Override
  public Dimension getPreferredScrollableViewportSize()
  {
    Dimension r;
    try { r=super.getPreferredScrollableViewportSize(); }
    catch(Exception ex) { r=new Dimension(120,80); }
    return r;
  }

  // bug swing 1.0
  @Override
  public Dimension getPreferredSize()
  {
    Dimension r;
    try { r=super.getPreferredSize(); }
    catch(Exception ex) { r=new Dimension(120,80); }
    return r;
  }

  public boolean getRequestFocusEnabled()
  {
    return getModel().getSize()!=0;
  }

  @Override
  public boolean isFocusTraversable()
  {
    return getModel().getSize()!=0;
  }

  /*
  protected void processMouseMotionEvent(MouseEvent _evt)
  {
    Object p=getClientProperty("DND_EVENTS");
    if(p==null)
      super.processMouseMotionEvent(_evt);
    else
      ((MouseMotionListener)p).mouseDragged(_evt);
  }
  */

  public Point computePopupMenuLocation(MouseEvent _evt)
  {
    Point p=new Point(_evt.getX(),_evt.getY());
    int   i=locationToIndex(p);
    if(i>=0)
    {
      Rectangle r=getCellBounds(i,i);
      if(r!=null)
      {
        p.x=0;
        p.y=r.y+r.height;

        Container q=getParent();
        if(q!=null)
        {
          q=q.getParent();
          if(q instanceof BuScrollPane)
          {
            BuScrollPane s=(BuScrollPane)q;
            int          h=s.getHeight();
            int          b=s.getInsets().bottom;
            p=SwingUtilities.convertPoint(this,p,s);
            if(p.x<0  ) p.x=0;
            if(p.y>h-b) p.y=h-b;
            p=SwingUtilities.convertPoint(s,p,this);
          }
        }
      }
    }
    return p;
  }
}
