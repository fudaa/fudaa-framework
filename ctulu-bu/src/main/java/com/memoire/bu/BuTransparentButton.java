/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuTransparentButton.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.plaf.ColorUIResource;

/**
 * A button without border.
 */
public class BuTransparentButton
  extends JButton
  implements BuBorders, BuInsets
{
  public BuTransparentButton()
  {
    this(null,null,null,null);
  }

  public BuTransparentButton(BuIcon _icon)
  {
    this(null,null,_icon,null);
  }

  public BuTransparentButton(String _label)
  {
    this(_label,null,null,null);
  }

  public BuTransparentButton(String _label, String _cmd, ActionListener _al)
  {
    this(_label,_cmd,null,_al);
  }

  public BuTransparentButton(String _label, BuIcon _icon)
  {
    this(_label,null,_icon,null);
  }

  public BuTransparentButton(String _label, BuIcon _icon, ActionListener _al)
  {
    this(_label,null,_icon,_al);
  }

  public BuTransparentButton(String _label, String _cmd, BuIcon _icon,
			     ActionListener _al)
  {
    BuIcon icon=_icon;
    if(_cmd!=null)
    {
      setName("bt"+_cmd);
      setActionCommand(_cmd);
      if(icon==null) icon=BuResource.BU.loadMenuCommandIcon(_cmd);
    }
    else
    {
      setActionCommand("BUTTON_PRESSED");
    }

    if((icon != null)&&icon.isDefault()) icon=null;

    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setIcon(icon);
    setText(_label);
    setMargin(INSETS0000);
    setBorder(EMPTY0000);
    setRequestFocusEnabled(false);
    setOpaque(false);
    setContentAreaFilled(false);
    setHorizontalAlignment(LEFT);
    putClientProperty("SLAF_NO_TEXTURE",Boolean.TRUE);

    if(_al!=null) addActionListener(_al);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public boolean isOpaque()
  {
    return false;
  }

  public void setIcon(BuIcon _icon)
  {
    BuLib.setIcon(this,_icon);
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(0,getHeight()+1);
    return r;
  }

  @Override
  public void updateUI()
  {
    super.updateUI();
    setRolloverEnabled(true);
  }

  @Override
  public Color getBackground()
  {
    Color r;
    if(isEnabled()&&getModel().isRollover()&&BuLib.isMetal())
      r=new ColorUIResource(192,192,208);
    else
      r=super.getBackground();
    return r;
  }
}
