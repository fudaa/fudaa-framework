/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuHeaderPanel.java
 * @version      0.41
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.Icon;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class BuHeaderPanel
  extends BuPanel
  implements BuBorders
{
  protected BuLabelMultiLine title_;
  protected BuLabelMultiLine text_;
  protected BuLabel icon_;

  public BuHeaderPanel()
  {
    this(null,null,null,true);
  }

  public BuHeaderPanel(String _title,String _text,Icon _icon,
                       boolean _separator)
  {
    setLayout(new BorderLayout(5,5));
    //setBorder(EMPTY0000);
    setBackground(BuLib.getColor(Color.white));

    BuPanel p=new BuPanel(new BorderLayout(0,0));
    p.setBackground(BuLib.getColor(Color.white));
    p.setBorder(EMPTY0505);
    add(p,BorderLayout.CENTER);

    title_=new BuLabelMultiLine(_title);
    title_.setFont(BuLib.deriveFont(title_.getFont(),Font.PLAIN,+6));
    //new Font("SansSerif",Font.PLAIN,18));
    title_.setForeground(BuLib.getColor(Color.gray));
    title_.setPreferredWidth(305);
    title_.setWrapMode(BuLabelMultiLine.WORD);
    title_.setBorder(new EmptyBorder(5,5,5,0));
    p.add(title_,BorderLayout.CENTER); //NORTH);

    text_=new BuLabelMultiLine(_text);
    text_.setVerticalAlignment(SwingConstants.TOP);
    title_.setFont(BuLib.deriveFont(title_.getFont(),Font.PLAIN,+0));
    //text_.setFont(new Font("SansSerif",Font.PLAIN,12));
    text_.setForeground(BuLib.getColor(Color.black));
    text_.setPreferredWidth(305);
    text_.setWrapMode(BuLabelMultiLine.WORD);
    text_.setBorder(new EmptyBorder(0,25,0,0));
    p.add(text_,BorderLayout.SOUTH); //CENTER);

    icon_=new BuLabel(_icon);
    icon_.setBorder(EMPTY5555);
    add(icon_,BorderLayout.EAST);

    if(_separator) add(new JSeparator(),BorderLayout.SOUTH);
    else setBorder(UIManager.getBorder("Theme.border"));
  }

  @Override
  public Dimension getPreferredSize()
  {
    Dimension r=super.getPreferredSize();
    r.width=Math.max(r.width,250);
    return r;
  }

  public void setTitle(String _title)
  {
    title_.setText(_title);
  }

  public void setText(String _text)
  {
    text_.setText(_text);
  }

  public void setIcon(Icon _icon)
  {
    icon_.setIcon(_icon);
  }

  /*
  public void paint(Graphics _g)
  {
    super.paint(_g);
    Color bg=UIManager.getColor("Panel.background");
    int w=getWidth() - 1;
    int h=getHeight() - 1;
    _g.setColor(bg.darker());
    _g.drawLine(0,h - 1,w,h - 1);
    _g.setColor(bg.brighter());
    _g.drawLine(0,h,w,h);
  }
  */
}
