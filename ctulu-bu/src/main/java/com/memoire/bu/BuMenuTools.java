/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuMenuTools.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import com.memoire.fu.FuPreferences;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.KeyStroke;

public class BuMenuTools
  extends BuDynamicMenu
  implements PropertyChangeListener
{
  private int           number_;
  private boolean       valid_;
  private FuPreferences preferences_;
  private BuResource    resource_;

  public BuMenuTools()
  {
    this(null,null);
  }

  public BuMenuTools(FuPreferences _preferences, BuResource _resource)
  {
    super(__("Outils"),"MENU_OUTILS");
    setResource(_resource);
    setPreferences(_preferences);
    //build();
  }

  private int getNumber()
  {
    int r=0;
    if(preferences_!=null)
      for(int i=1;
          !"".equals(preferences_.getStringProperty("tool."+i+".exec"));
          i++)
        r++;
    return r;
  }

  public FuPreferences getPreferences()
  {
    return preferences_;
  }

  public void setPreferences(FuPreferences _preferences)
  {
    if(preferences_!=null) preferences_.removePropertyChangeListener(this);
    preferences_=_preferences;
    if(preferences_!=null) preferences_.addPropertyChangeListener(this);
    valid_ =false;
    number_=getNumber();
    //build();
  }

  public BuResource getResource()
  {
    return resource_;
  }

  public void setResource(BuResource _resource)
  {
    resource_=_resource;
    valid_ =false;
    number_=getNumber();
    //build();
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt)
  {
    if(_evt.getPropertyName().startsWith("tool."))
    {
      valid_ =false;
      number_=getNumber();
      //build();
    }
  }

  /*
  public final void setSelected(boolean _state)
  {
    if(_state&&!isSelected()&&!valid_) build();
    super.setSelected(_state);
  }
  */

  @Override
  protected void build()
  {
    if(isPopupMenuVisible()||valid_) return;

    FuLog.debug("BMT: build menu tools");

    valid_=true;
    removeAll();
    number_=0;
    if(preferences_==null) return;

    Vector v=new Vector();

    int i=1;
    while(true)
    {
      String name=preferences_.getStringProperty("tool."+i+".name");
      String exec=preferences_.getStringProperty("tool."+i+".exec");
      String icon=preferences_.getStringProperty("tool."+i+".icon");

      if("".equals(exec)) break;

      if("-".equals(exec))
      {
	addSeparator(name);
      }
      else
      {
	if("".equals(name)) name=exec;

        BuMenuItem mi=addMenuItem
	  (name,
	   "OUTIL("+exec+")",
	   true);
	if(resource_!=null) mi.setIcon(resource_.getMenuIcon(icon));
	/*
	  if(number_<10)
	  mi.setAccelerator
	  (KeyStroke.getKeyStroke
	  (KeyEvent.VK_A+number_,KeyEvent.CTRL_MASK|KeyEvent.ALT_MASK));
	*/
	v.addElement(mi);

	number_++;
      }

      i++;
    }

    /*
    addSeparator();
    addMenuItem("Ajouter","AJOUTER_OUTIL");
    */

    computeMnemonics();

    Enumeration e=v.elements();
    while(e.hasMoreElements())
    {
      BuMenuItem mi=(BuMenuItem)e.nextElement();
      int k=mi.getMnemonic();
      mi.setAccelerator
	  (KeyStroke.getKeyStroke
	   (k,InputEvent.CTRL_MASK|InputEvent.ALT_MASK));
    }
  }

  /*
  public void paintComponent(Graphics _g)
  {
    boolean r=isEnabled();
    if(r!=super.isEnabled()) setEnabled(r);
    super.paintComponent(_g);
  }

  public boolean isEnabled()
  {
    if(!valid_) build();
    return (preferences_!=null)&&(number_>0);
  }
  */

  @Override
  public boolean isActive()
  {
    return (number_>0);
  }
}
