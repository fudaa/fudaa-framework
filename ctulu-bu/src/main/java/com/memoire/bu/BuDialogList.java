/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuDialogList.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A standard dialog for input using a list.
 */
public class BuDialogList
  extends BuDialog
  implements ActionListener
{
  protected BuButton         btValider_;
  protected BuButton         btAnnuler_;
  protected BuList           ltValeur_;
  protected BuLabelMultiLine lbValeur_;
  protected BuLabelMultiLine lbCommentaire_;

  public BuDialogList(BuCommonInterface      _parent,
		      BuInformationsSoftware _isoft,
		      String                 _title,
		      String                 _label,
		      String[]               _values)
  {
    this(_parent,_isoft,_title,_label,_values,null,null);
  }

  public BuDialogList(BuCommonInterface      _parent,
		      BuInformationsSoftware _isoft,
		      String                 _title,
		      String                 _label,
		      String[]               _values,
		      String                 _value,
		      String                 _comment)
  {
    super(_parent,_isoft,_title);

    /*
    ltValeur_.removeAllItems();
    for(int i=0;i<_values.length;i++) ltValeur_.addItem(_values[i]);
    if((_value==null)&&(_values.length>0)) _value=_values[0];
    */
    ltValeur_.setListData(_values);
    //ltValeur_.setEditable(_editable);
    ltValeur_.setSelectedValue(_value,true);
    lbValeur_.setText(_label);
    valeur_=_value;

    lbCommentaire_.setText(_comment);
    
    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btValider_=new BuButton(BuResource.BU.loadButtonCommandIcon("VALIDER"),
                            getString("Valider"));
    btValider_.addActionListener(this);
    getRootPane().setDefaultButton(btValider_);
    pnb.add(btValider_);

    btAnnuler_=new BuButton(BuResource.BU.loadButtonCommandIcon("ANNULER"),
                            getString("Annuler"));
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  @Override
  public JComponent getComponent()
  {
    ltValeur_=new BuList();
    //tfValeur_.setColumns(20);
    //ltValeur_.addListSelectionListener(this);

    lbValeur_     =new BuLabelMultiLine();
    lbCommentaire_=new BuLabelMultiLine();

    BuPanel p=new BuPanel();
    p.setLayout(new BuVerticalLayout());
    p.setBorder(EMPTY5555);
    p.add(lbValeur_);
    p.add(ltValeur_);
    p.add(lbCommentaire_);
    p.add(new BuPanel());
    return p;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialogList : "+source);

    if(source==btValider_)//||(source==ltValeur_))
    {
      reponse_=JOptionPane.OK_OPTION;
      valeur_ =(String)ltValeur_.getSelectedValue();
      setVisible(false);
    }

    if(source==btAnnuler_)
    {
      reponse_=JOptionPane.CANCEL_OPTION;
      valeur_ =null;
      setVisible(false);
    }
  }

  private String valeur_;

  public String getValue()
    { return valeur_; }

  public void setValue(String _value)
  {
    ltValeur_.setSelectedValue(_value,true);
    valeur_=_value;
  }
}

