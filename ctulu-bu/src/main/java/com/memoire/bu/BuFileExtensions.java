/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuFileExtensions.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

public final class BuFileExtensions
{
  protected static final String[] ARCHIVES=
    new String[] { "ace","arc","bz","bz2","gz","jar","lha","lhz","rar",
		   "shar","sit","tar","tbz","tgz","z","zip","zoo" };

  protected static final String[] IMAGES=
    new String[] { "art","avs","bmp","cgm","cmyk","cut","dcm",
		   "dcx","dib","dpx","emf","epdf","epi","eps",
		   "eps2","eps3","epsf","epsi","ept","fax","fig",
		   "fits","fpx","gif","ico","jbig","jng","jp2","jpc",
		   "jpeg","jpg","miff","mng","mtv","mvg","otb","p7",
		   "palm","pbm","pcd","pcds","pcx","pgm","picon",
		   "pict","pix","png","pnm","ppm","ptif","pwp","rad",
		   "rgb","rgba","rla","rle","sct","sfw","sgi","sun",
		   "svg","tga","tif","tiff","tim","uyvy","vicar",
		   "viff","wbmp","wmf","wpg","xbm","xcf","xpm","xwd",
		   "yuv",
		   "ief","g3",
		   "bdz" };

  protected static final String[] MOVIES=
    new String[] { "mp2","mpe","mpeg","mpg","vob","qt","mov",
		   "moov","movie","flc","fli","mng","asf",
		   "asx","avi","wmv" };

  protected static final String[] SOUNDS=
    new String[] { "8svx","ac3","aiff","au","avr","cdr","cvs",
		   "hcom","maud","ogg","sf","sph","smp","snd",
		   "sndt","txw","vms","voc","wav","wve",
		   "mp3",
		   "mid","midi",
		   "mod","s3m","stm","ult","xi","xm",
		   "m3u","pls" };

  protected static final String[] SOURCES=
    new String[] { "c","cpp","e","el","f","for","h","hpp","java",
		   "php","php3","pl","py","rb","tcl" };

  protected static final String[] TEXTS=
    new String[] { "abw","asc","bib","doc","eps","htm","html",
		   "ltx","pdf","ps","rdf","rtx","sdw","stw",
		   "sxw","tex","txt" };
}
