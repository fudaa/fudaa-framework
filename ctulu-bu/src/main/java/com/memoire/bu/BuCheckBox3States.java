/*
 * @creation     23 juin 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package com.memoire.bu;

import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ActionMapUIResource;

/**
 * Un checkbox permettant 3 �tats (activ�, d�sactiv�, mixte). Dans ce dernier
 * cas, le bouton appara�t en gris�.
 * Le code est repris d'un composant ThreeStateCheckBox
 * <p>
 * Important : Ce composant n'envoie pas d'�venement ActionEvent. Seuls les
 * evenements ItemEvent sont g�r�s.<br>
 * Dans le cas ou l'�tat est "mixed", un evenement ItemEvent avec l'etat
 * correspondant STATE_MIXED est envoy�.
 *<p>
 * Maintenance tip - There were some tricks to getting this code
 * working:
 *
 * 1. You have to overwite addMouseListener() to do nothing
 * 2. You have to add a mouse event on mousePressed by calling
 * super.addMouseListener()
 * 3. You have to replace the UIActionMap for the keyboard event
 * "pressed" with your own one.
 * 4. You have to remove the UIActionMap for the keyboard event
 * "released".
 * 5. You have to grab focus when the next state is entered,
 * otherwise clicking on the component won't get the focus.
 * 6. You have to make a BuCheckBox3StatesDecorator as a button model that
 * wraps the original button model and does state management.
 * 
 * @author Bertrand Marchand
 */
public class BuCheckBox3States extends JCheckBox {
  /** Etat Mixte du checkbox */
  public static final int STATE_MIXED      = 0;
  /** Etat Selected du checkbox */
  public static final int STATE_SELECTED   = ItemEvent.SELECTED;
  /** Etat Deselected du checkbox */
  public static final int STATE_DESELECTED = ItemEvent.DESELECTED;

  private final BuCheckbox3StatesDecorator model;

  public BuCheckBox3States(String text, Icon icon, int initial){
    super(text, icon);
    // Add a listener for when the mouse is pressed
    super.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        grabFocus();
        model.nextState();
      }
    });
    // Reset the keyboard action map
    ActionMap map = new ActionMapUIResource();
    map.put("pressed", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        grabFocus();
        model.nextState();
      }
    });
    map.put("released", null);
    SwingUtilities.replaceUIActionMap(this, map);
    // set the model to the adapted model
    model = new BuCheckbox3StatesDecorator(getModel());
    setModel(model);
    setState(initial);
  }
  public BuCheckBox3States(String text, int initial) {
    this(text, null, initial);
  }
  public BuCheckBox3States(String text) {
    this(text, STATE_MIXED);
  }
  public BuCheckBox3States() {
    this(null);
  }

  /** No one may add mouse listeners, not even Swing! */
  @Override
  public void addMouseListener(MouseListener l) { }
  /**
   * Set the new state to either SELECTED, NOT_SELECTED or
   * DONT_CARE.  If state == null, it is treated as DONT_CARE.
   */
  public void setState(int state) { model.setState(state); }
  /** Return the current state, which is determined by the
   * selection status of the model. */
  public int getState() { return model.getState(); }
  @Override
  public void setSelected(boolean b) {
    if (b) {
      setState(STATE_SELECTED);
    } else {
      setState(STATE_DESELECTED);
    }
  }
  
  /**
   * The button accept only 2 states when clicked : SELECTED, or NOT_SELECTED
   * @param _b true : Only 2 states. 3 otherwise.
   */
  public void acceptOnly2StatesWhenClicked(boolean _b) {
    model.acceptOnly2StatesWhenClicked(_b);
  }
  
  /**
   * Exactly which Design Pattern is this?  Is it an Adapter,
   * a Proxy or a Decorator?  In this case, my vote lies with the
   * Decorator, because we are extending functionality and
   * "decorating" the original model with a more powerful model.
   */
  private class BuCheckbox3StatesDecorator extends JToggleButton.ToggleButtonModel {
    private final ButtonModel other;
    private boolean only2States_=false;
    private BuCheckbox3StatesDecorator(ButtonModel other) {
      this.other = other;
    }
    private void setState(int state) {
      if (state == STATE_DESELECTED) {
        other.setArmed(false);
        setPressed(false);
        setSelected(false);
      } else if (state == STATE_SELECTED) {
        other.setArmed(false);
        setPressed(false);
        setSelected(true);
      } else { // either "null" or DONT_CARE
        other.setArmed(true);
        setPressed(true);
        setSelected(true);
        fireItemStateChanged(new ItemEvent(this,ItemEvent.ITEM_STATE_CHANGED,this,STATE_MIXED));
      }
    }
    /**
     * The current state is embedded in the selection / armed
     * state of the model.
     *
     * We return the SELECTED state when the checkbox is selected
     * but not armed, DONT_CARE state when the checkbox is
     * selected and armed (grey) and NOT_SELECTED when the
     * checkbox is deselected.
     */
    private int getState() {
      if (isSelected() && !isArmed()) {
        // normal black tick
        return STATE_SELECTED;
      } else if (isSelected() && isArmed()) {
        // don't care grey tick
        return STATE_MIXED;
      } else {
        // normal deselected
        return STATE_DESELECTED;
      }
    }
    /** We rotate between NOT_SELECTED, SELECTED and DONT_CARE.*/
    private void nextState() {
      int current = getState();
      if (current == STATE_DESELECTED) {
        setState(STATE_SELECTED);
      } else if (current == STATE_SELECTED) {
        if (only2States_)
          setState(STATE_DESELECTED);
        else
          setState(STATE_MIXED);
      } else if (current == STATE_MIXED) {
        setState(STATE_DESELECTED);
      }
    }
    /** Filter: No one may change the armed status except us. */
    @Override
    public void setArmed(boolean b) {
    }
    /** We disable focusing on the component when it is not
     * enabled. */
    @Override
    public void setEnabled(boolean b) {
      setFocusable(b);
      if (other != null)
        other.setEnabled(b);
    }
    /** All these methods simply delegate to the "other" model
     * that is being decorated. */
    @Override
    public boolean isArmed() { return other.isArmed(); }
    @Override
    public boolean isSelected() { return other.isSelected(); }
    @Override
    public boolean isEnabled() { return other.isEnabled(); }
    @Override
    public boolean isPressed() { return other.isPressed(); }
    @Override
    public boolean isRollover() { return other.isRollover(); }
    @Override
    public void setSelected(boolean b) { other.setSelected(b); }
    @Override
    public void setPressed(boolean b) { other.setPressed(b); }
    @Override
    public void setRollover(boolean b) { other.setRollover(b); }
    @Override
    public void setMnemonic(int key) { other.setMnemonic(key); }
    @Override
    public int getMnemonic() { return other.getMnemonic(); }
    @Override
    public void setActionCommand(String s) {
      other.setActionCommand(s);
    }
    @Override
    public String getActionCommand() {
      return other.getActionCommand();
    }
    @Override
    public void setGroup(ButtonGroup group) {
      other.setGroup(group);
    }
    @Override
    public void addActionListener(ActionListener l) {
      other.addActionListener(l);
    }
    @Override
    public void removeActionListener(ActionListener l) {
      other.removeActionListener(l);
    }
    @Override
    public void addItemListener(ItemListener l) {
      other.addItemListener(l);
    }
    @Override
    public void removeItemListener(ItemListener l) {
      other.removeItemListener(l);
    }
    @Override
    public void addChangeListener(ChangeListener l) {
      other.addChangeListener(l);
    }
    @Override
    public void removeChangeListener(ChangeListener l) {
      other.removeChangeListener(l);
    }
    @Override
    public Object[] getSelectedObjects() {
      return other.getSelectedObjects();
    }
    public void acceptOnly2StatesWhenClicked(boolean _b) {
      only2States_=_b;
    }
  }
  
  public static void main(String args[]) throws Exception {
    JFrame frame = new JFrame("BuCheckBox3States");
    frame.getContentPane().setLayout(new GridLayout(0, 1, 5, 5));
    final BuCheckBox3States swingBox = new BuCheckBox3States(
        "Testing the 3states checkbox");
//    swingBox.acceptOnly2StatesWhenClicked(true);
    swingBox.setState(BuCheckBox3States.STATE_MIXED);

    swingBox.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        System.out.println("3states actionPerformed");
      }
    });
    swingBox.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        System.out.println("3states stateChanged");
      }
    });
    swingBox.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        System.out.println("3states itemStateChanged");
      }
    });
    swingBox.setMnemonic('T');
    frame.getContentPane().add(swingBox);

    JCheckBox normal=new JCheckBox("The normal checkbox");
    normal.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        System.out.println("normal actionPerformed");
      }
    });
    normal.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        System.out.println("normal stateChanged");
      }
    });
    normal.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        System.out.println("normal itemStateChanged");
      }
    });

    frame.getContentPane().add(normal);
    UIManager.setLookAndFeel(
        UIManager.getSystemLookAndFeelClassName());
    final BuCheckBox3States winBox = new BuCheckBox3States(
        "Testing the 3states checkbox");
    frame.getContentPane().add(winBox);
    final JCheckBox winNormal = new JCheckBox(
        "The normal checkbox");
    frame.getContentPane().add(winNormal);
    // wait for 3 seconds, then enable all check boxes
    new Thread() { {start();}
      @Override
      public void run() {
        try {
          winBox.setEnabled(false);
          winNormal.setEnabled(false);
          Thread.sleep(3000);
          winBox.setEnabled(true);
          winNormal.setEnabled(true);
        } catch (InterruptedException ex) { }
      }
    };
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
