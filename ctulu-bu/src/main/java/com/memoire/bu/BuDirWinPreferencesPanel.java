/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuDirWinPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose your directories and the
 * size of the main window.
 *
 * directory.open
 * directory.save
 * window.size
 */
public class BuDirWinPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  protected BuCommonInterface appli_;
  protected BuPreferences     options_;

  protected BuLabel            lbDirOpen_,lbDirSave_;
  protected BuTextField        tfDirOpen_,tfDirSave_;
  protected BuButton           btDirOpen_,btDirSave_;
  protected BuComboBox         chWinSize_;
  protected BuLabel            lbWinSize_;
  
  @Override
  public String getTitle()
  {
    return getS("Position");
  }

  @Override
  public String getCategory()
  {
    return getS("Visuel");
  }

  // Constructeur

  public BuDirWinPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_=_appli;
    options_=BuPreferences.BU;

    BuVerticalLayout lo=new BuVerticalLayout(5);
    //lo.setVgap(5);

    BuPanel p=new BuPanel(lo);
    //p.setLayout(lo);

    /*
    BuBorderLayout lo1=new BuBorderLayout();
    lo1.setHgap(5);
    lo1.setVgap(5);
    BuPanel p1=new BuPanel();
    p1.setLayout(lo1);
    p1.setBorder(EMPTY5555);
    */
    
    BuMultiFormLayout mflo=new BuMultiFormLayout
      (5,5,2,BuFormLayout.NONE,
       new int[] { 0 },null);

    //BuGridLayout lo1=new BuGridLayout(3,5,5,false,false,false,false);
    //lo1.setColumnXAlign(new float[] { 1.f,0.f,0.f });

    BuPanel p1=new BuPanel(mflo);//lo1);
    //p1.setBorder(EMPTY5555);

    lbDirOpen_=new BuLabel(getS("Ouverture:"));
    tfDirOpen_=new BuTextField(25);
    tfDirOpen_.setName("tfDIRECTORY_OPEN");
    tfDirOpen_.setEditable(false);
    btDirOpen_=new BuButton();
    //btDirOpen_.setRequestFocusEnabled(true);
    btDirOpen_.setIcon(getDirectoryIcon());
    btDirOpen_.setName("btDIRECTORY_OPEN");
    btDirOpen_.setMargin(BuInsets.INSETS0000);//0202);
    btDirOpen_.addKeyListener(this);
    btDirOpen_.addActionListener(this);

    lbDirSave_=new BuLabel(getS("Enregistrement:"));
    tfDirSave_=new BuTextField(25);
    tfDirSave_.setName("tfDIRECTORY_SAVE");
    tfDirSave_.setEditable(false);
    btDirSave_=new BuButton();
    //btDirSave_.setRequestFocusEnabled(false);
    btDirSave_.setIcon(getDirectoryIcon());
    btDirSave_.setName("btDIRECTORY_SAVE");
    btDirSave_.setMargin(BuInsets.INSETS0000);//0202);
    btDirSave_.addKeyListener(this);
    btDirSave_.addActionListener(this);

    p1.add(lbDirOpen_,BuFormLayout.constraint(0,0,false,1.f));
    p1.add(tfDirOpen_,BuFormLayout.constraint(1,0,2,true,0.f));
    p1.add(btDirOpen_,BuFormLayout.constraint(3,0));
    p1.add(lbDirSave_,BuFormLayout.constraint(0,1,false,1.f));
    p1.add(tfDirSave_,BuFormLayout.constraint(1,1,2,true,0.f));
    p1.add(btDirSave_,BuFormLayout.constraint(3,1));

    /*
    p1.add(lbDirOpen_,BuBorderLayout.WEST);
    p1.add(tfDirOpen_,BuBorderLayout.CENTER);
    p1.add(btDirOpen_,BuBorderLayout.EAST);
    p1.add(lbDirSave_,BuBorderLayout.WEST);
    p1.add(tfDirSave_,BuBorderLayout.CENTER);
    p1.add(btDirSave_,BuBorderLayout.EAST);
    */

    p1.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getS("R�pertoires")),
	EMPTY5555));

    lbWinSize_=new BuLabel(getS("Taille:"));
    chWinSize_=new BuComboBox();
    chWinSize_.setName("chWINDOW_SIZE");
    chWinSize_.addItem(getS("normale") );
    chWinSize_.addItem(getS("maximale"));
    chWinSize_.addItem(getS("derni�re"));
    chWinSize_.addItem(getS("minimale"));
    chWinSize_.addActionListener(this);

    //BuHorizontalLayout lo4=new BuHorizontalLayout(5,false,true);
    BuPanel p4=new BuPanel(mflo);//lo4);

    p4.add(lbWinSize_,BuFormLayout.constraint(0,0,false,1.f));
    p4.add(chWinSize_,BuFormLayout.constraint(1,0,false,0.f));

    /*
    p4.add(lbWinSize_,BuBorderLayout.WEST);
    p4.add(chWinSize_,BuBorderLayout.CENTER);
    */

    p4.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getS("Fen�tre principale")),
	EMPTY5555));

    p.add(p1);
    p.add(p4);

    setLayout(new BuBorderLayout());
    setBorder(EMPTY5555);
    add(p,BuBorderLayout.CENTER);

    chWinSize_.setSelectedIndex(1);

    /*
    BuLib.adjustPreferredWidth(new JComponent[]
      { lbDirOpen_,lbDirSave_,lbWinSize_ });
    */

    updateComponents();
  }

  private static final Icon getDirectoryIcon()
  {
    Icon r=UIManager.getIcon("FileView.directoryIcon");
    if(r==null) r=BuResource.BU.getIcon("ouvrir");
    return r;
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    //System.err.println("SOURCE="+source.getName());
    //System.err.println("ACTION="+_evt.getActionCommand());

    if(source==btDirOpen_)
      tfDirOpen_.setText(getDirectory(tfDirOpen_.getText()));
	
    if(source==btDirSave_)
      tfDirSave_.setText(getDirectory(tfDirSave_.getText()));
	
    //chDirWin.setEnabled(cbExterne.isSelected());

    setDirty(true);
  }
  
  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
    { return true; }
  
  @Override
  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees
  
  private void fillTable()
  {
    options_.putStringProperty("directory.open",tfDirOpen_.getText());
    options_.putStringProperty("directory.save",tfDirSave_.getText());
    options_.putIntegerProperty
	("window.size",chWinSize_.getSelectedIndex());
    setDirty(false);
  }

  private void updateComponents()
  {
    tfDirOpen_.setText(options_.getStringProperty("directory.open"));
    tfDirSave_.setText(options_.getStringProperty("directory.save"));
    chWinSize_.setSelectedIndex(options_.getIntegerProperty("window.size"));
    setDirty(false);
  }

  private String getDirectory(String _default)
  {
    String r=FuLib.expandedPath(_default);

    // System.err.println("### FD begin");
    BuFileChooser chooser=new BuFileChooser();
    chooser.setDialogTitle(getS("Repertoire"));
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    chooser.setFileHidingEnabled(true);
    chooser.setMultiSelectionEnabled(false);
    chooser.resetChoosableFileFilters();

    File f=new File(r);
    if(f.exists()) chooser.setCurrentDirectory(f);

    int returnVal=chooser.showDialog(this,getS("S�lectionner"));
    // System.err.println("### FD end");

    if(returnVal == JFileChooser.APPROVE_OPTION)
    {
      f=chooser.getSelectedFile();
      // System.err.println("### f="+f);
      if(f!=null) r=f.getPath();
    }

    return FuLib.reducedPath(r);
  }
}
