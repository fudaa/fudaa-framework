/**
 * @modification $Date: 2007-11-22 11:49:26 $
 * @statut       unstable
 * @file         BuExplorer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

// import com.memoire.dnd.*;
import com.memoire.dt.DtDragSupport;
import com.memoire.dt.DtFileFieldHandler;
import com.memoire.dt.DtFileHandler;
import com.memoire.dt.DtFilesSelection;
import com.memoire.dt.DtLib;
import com.memoire.fu.FuComparator;
import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuRefTable;
import com.memoire.fu.FuSort;
import com.memoire.vfs.VfsFile;
import com.memoire.vfs.VfsFileFile;
import com.memoire.vfs.VfsFileRam;
import com.memoire.vfs.VfsLib;
import com.memoire.vfs.VfsOperations;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class BuExplorer extends BuPanel implements ActionListener, ListSelectionListener, FuEmptyArrays {
  private static final int GAP = BuPreferences.BU.getIntegerProperty("layout.gap", 5);
  static final boolean WRAP_LIST = BuPreferences.BU.getBooleanProperty("explorer.wraplist", false);
  static final int CELL_WIDTH = BuPreferences.BU.getIntegerProperty("explorer.cellwidth", 170);

  protected Icon i_computer;
  protected Icon i_disk;
  protected Icon i_floppy;
  protected Icon i_directory;
  protected Icon i_file;
  protected Icon i_home;
  protected Icon i_parent;

  protected BuCommonInterface app_;
  protected Vector listeners_;

  protected BuComboBox roots_;
  protected BuLabel lbURL_;
  protected BuTextField current_;
  protected BuEmptyList dirs_;
  protected BuComboBox filters_;
  protected BuComboBox sorters_;
  protected BuEmptyList files_;

  protected BuFileViewer viewer_; // BuList

  protected BuPopupMenu pmCurrent_;
  protected BuPopupMenu pmDirs_;
  protected BuPopupMenu pmFiles_;

  protected BuMenuItem miResolve_;
  protected BuMenuItem miOuvrirFichier_;
  // protected BuMenuItem miCvsAdd_;
  // protected BuMenuItem miCvsUpdate_;
  // protected BuMenuItem miCvsCommit_;
  // protected BuMenuItem miAntBuild_;
  // protected BuMenuItem miMake_;

  protected VfsFile last_;
  protected BuTimer timer_;
  protected long timestamp_;
  protected int liststamp_;

  private int iconsize_;
  private FuRefTable iconcache_ = new FuRefTable(FuRefTable.WEAK, FuRefTable.SOFT);
  int dropaction_ = DtLib.NONE;

  public BuExplorer() {
    this(null, true, null, true, false);
  }

  public BuExplorer(VfsFile _path) {
    this(null, true, _path, true, false);
  }

  public BuExplorer(BuCommonInterface _app) {
    this(_app, false, null, true, false);
  }

  public BuExplorer(BuCommonInterface _app, boolean _viewer) {
    this(_app, _viewer, null, true, false);
  }

  public BuExplorer(BuCommonInterface _app, boolean _viewer, VfsFile _path, boolean _addSignet, boolean _currentDirOnTop) {
    app_ = _app;
    listeners_ = new Vector(1, 1);

    final MouseListener mh = new MouseHandler();
    final KeyListener kh = new KeyHandler();

    roots_ = new BuComboBox();
    roots_.setMaximumRowCount(20);
    lbURL_ = new BuLabel(BuResource.BU.getMenuIcon("signet"));
    lbURL_.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

    // if(FuLib.jdk()>=1.2)
    // {
    lbURL_.setTransferHandler(new DtFileHandler() {
      @Override
      protected Object getData(JComponent _c) {
        return current_.getText();
      }
    });
    DtDragSupport.install(lbURL_);
    // }

    // lbURL_.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
    /*
     * lbURL_.setTransferHandler(new TransferHandler() { protected Transferable createTransferable(JComponent _c) {
     * String s=FuLib.expandedPath(current_.getText()); if("".equals(s)) s=null; URL u=null; if(s!=null) { try { u=new
     * URL(s); } catch(MalformedURLException ex) { } if(u==null) { VfsFile f=VfsFile.createFile(s); if(f.exists()) { try {
     * u=f.toURL(); } catch(MalformedURLException ex) { } } } } return u==null ? null : new DtFilesSelection(u); }
     * public int getSourceActions(JComponent _c) { return DtLib.ALL; } });
     */

    /*
     * new DndSource(new DndRequestData() { public Object[] request(JComponent _component,Point _location) { String
     * s=FuLib.expandedPath(current_.getText()); if("".equals(s)) s=null; URL u=null; if(s!=null) { try { u=new URL(s); }
     * catch(MalformedURLException ex) { } } VfsFile f=null; if(s!=null) { f=VfsFile.createFile(s); if(!f.exists())
     * f=null; } return new Object[] { u, f, s }; } }).add(lbURL_);
     */

    final ActionListener gl = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        current_.setEnabled(false);
        current_.repaint(0);
        String p = FuLib.expandedPath(current_.getText());

        /*
         * File r=null; try { r=new URLAdapter(new URL(p)); ((URLAdapter)r).init(); } catch(Exception ex) { r=new
         * File(p); }
         */

        VfsFile r = VfsFile.createFile(p);

        if (r.exists()) {
          setCurrentDirectory(r);
        } else {
          getToolkit().beep();
          current_.setText(FuLib.reducedPath(last_ == null ? "" : "" + last_));
        }
        current_.setEnabled(true);
        current_.repaint(0);
        current_.requestFocus();
      }
    };

    final ActionListener cl = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        current_.setText("");
        current_.requestFocus();
      }
    };

    current_ = new BuTextField();// LabelMultiLine();
    // current_.setWrapMode(BuLabelMultiLine.LETTER);
    // current_.setBorder(new EmptyBorder(2,6,0,6));
    current_.addMouseListener(mh);
    current_.addActionListener(gl);

  //  final Object DFH; // TransferHandler
    final Object DTL; // DropTargetListener

    // if(FuLib.jdk()>=1.2)
    // {
    current_.setTransferHandler(new DtFileFieldHandler(true, DtFileFieldHandler.ALL, true));

    /*DFH = */new TransferHandler() {
      private int action_ = DtLib.NONE;

      @Override
      protected Transferable createTransferable(JComponent _c) {
        DtFilesSelection r = new DtFilesSelection(((JList) _c).getSelectedValues());
        // FuLog.debug("BEX: set action "+action_);
        r.setAction(action_);
        return r;
      }

      @Override
      public int getSourceActions(JComponent _c) {
        return DtLib.ALL;
      }

      /*
       * public Icon getVisualRepresentation(Transferable _t) { FuLog.debug("getVisualRepresentation"); Icon r=null;
       * if(_t.isDataFlavorSupported(DtLib.vfsFlavor)) { try { DtFilesSelection s=(DtFilesSelection)_t.getTransferData
       * (DtLib.vfsFlavor); BuIcon i=BuResource.BU.getIcon("ouvrir"); i=BuResource.BU.resizeIcon(i,32); i.getImage();
       * i=new BuIcon (BuLib.HELPER.getToolkit().createImage (new FilteredImageSource
       * (i.getImage().getSource(),BuFilters.BW))); r=i; FuLog.debug("new icon"); lbURL_.setIcon(i); }
       * catch(UnsupportedFlavorException ufex) { } catch(IOException ioex) { } } if(r==null)
       * r=super.getVisualRepresentation(_t); return r; }
       */

      @Override
      public void exportToClipboard(JComponent _c, Clipboard _clipboard, int _action) {
        action_ = _action;
        // FuLog.debug("BEX: EXPORT TO CLIPBOARD");
        super.exportToClipboard(_c, _clipboard, _action);
      }

      @Override
      public void exportAsDrag(JComponent _c, InputEvent _e, int _action) {
        action_ = _action;
        // FuLog.debug("BEX: EXPORT AS DRAG");
        super.exportAsDrag(_c, _e, _action);
      }

      @Override
      protected void exportDone(JComponent _c, Transferable _t, int _action) {
        // FuLog.debug("BEX: EXPORT DONE");
        action_ = dropaction_ = DtLib.NONE;
        // if(_t instanceof DtFilesSelection)
        // ((DtFilesSelection)_t).setAction(_action);
      }

      @Override
      public boolean importData(JComponent _c, Transferable _t) {
        // FuLog.debug("BEX: IMPORT DATA");

        boolean r = false;
        if (canImport(_c, _t.getTransferDataFlavors())) {
          VfsFile d = null;
          if (_c == dirs_) d = (VfsFile) dirs_.getSelectedValue();
          if (d == null) d = getCurrentDirectory();

          DtFilesSelection s = DtFilesSelection.convert(_t);
          // if(s!=null) FuLog.debug("BEX: PASTE S="+s.size());
          // else FuLog.debug("BEX: PASTE S=null");

          if ((d != null) && (s != null)) {
            VfsFile[] f = s.getAllFiles();
            VfsOperations o = VfsLib.getOperations();
            if (f.length > 0) {
              r = true;
              if (dropaction_ != DtLib.NONE) s.setAction(dropaction_);
              switch (s.getAction()) {
              case DtLib.COPY:
                o.copyFiles(f, d);
                break;
              case DtLib.MOVE:
                o.moveFiles(f, d);
                break;
              case DtLib.LINK:
                o.linkFiles(f, d);
                break;
              default:
                r = false;
                break;
              }
            }
          }
        }
        return r;
      }

      @Override
      public boolean canImport(JComponent _c, DataFlavor[] _flavors) {
        return DtFilesSelection.canConvert(_flavors);
        /*
         * for(int i=0; i<_flavors.length; i++) if(DtLib.vfsFlavor.equals(_flavors[i])||
         * DataFlavor.javaFileListFlavor.equals(_flavors[i])) return true; return false;
         */
      }
    };

    DTL = new DropTargetListener() {
      @Override
      public void dropActionChanged(DropTargetDragEvent _dtde) {
        dropaction_ = _dtde.getDropAction();
        // FuLog.debug("BEX: action changed");
      }

      @Override
      public void dragEnter(DropTargetDragEvent _dtde) {}

      @Override
      public void dragExit(DropTargetEvent _dte) {}

      @Override
      public void dragOver(DropTargetDragEvent _dtde) {}

      @Override
      public void drop(DropTargetDropEvent _dtde) {}
    };

    // }
    /*
     * else { DFH=null; DTL=null; }
     */

    /*
     * final DropTargetListener dtl=new DropTargetListener() { public void dropActionChanged(DropTargetDragEvent _dtde) {
     * dfh.action_=_dtde.getDropAction(); FuLog.debug("BEX: action changed"); } public void
     * dragEnter(DropTargetDragEvent _dtde) { //FuLog.debug("drag enter"); Component
     * c=((DropTarget)_dtde.getSource()).getComponent(); if(c==dirs_) dirs_.clearSelection(); } public void
     * dragExit(DropTargetEvent _dte) { //FuLog.debug("drag exit"); Component
     * c=((DropTarget)_dte.getSource()).getComponent(); if(c==dirs_) dirs_.clearSelection(); } public void
     * dragOver(DropTargetDragEvent _dtde) { Component c=((DropTarget)_dtde.getSource()).getComponent(); if(c==dirs_) {
     * Point p=_dtde.getLocation(); int i=dirs_.locationToIndex(p); //FuLog.debug("drag over P="+p+" I="+i); if(i>=0) {
     * Rectangle r=dirs_.getCellBounds(i,i); //FuLog.debug("R="+r+" P="+p); //FuLog.debug(r.contains(p) ? "inside" :
     * "outside"); if(!r.contains(p)) { i=-1; } } if(i>=0) dirs_.setSelectedIndex(i); else dirs_.clearSelection(); } }
     * public void drop(DropTargetDropEvent _dtde) { FuLog.debug("BEX: drop"); VfsFile d=null; Component
     * c=((DropTarget)_dtde.getSource()).getComponent(); if(c==dirs_) d=(VfsFile)dirs_.getSelectedValue(); if(d==null)
     * d=getCurrentDirectory(); if((d==null)||!(c instanceof JComponent)) { _dtde.rejectDrop(); } else { Transferable
     * t=_dtde.getTransferable(); if(!DtFilesSelection.canConvert(t)) { FuLog.debug("BEX: drop rejected");
     * _dtde.rejectDrop(); return; } FuLog.debug("BEX: drop accepted"); int a=_dtde.getDropAction();
     * _dtde.acceptDrop(a); DtFilesSelection s=DtFilesSelection.convert(t); //DataFlavor[]
     * df=t.getTransferDataFlavors(); //for(int i=0;i<df.length;i++) // FuLog.debug("BEX: "+i+") "+df[i]);
     * _dtde.dropComplete(s!=null); //if(s!=null) FuLog.debug("BEX: S="+s.size()); //else FuLog.debug("BEX: S=null");
     * if(s!=null) { TransferHandler h=((JComponent)c).getTransferHandler(); if(h!=null) { s.setAction(a);
     * h.importData((JComponent)c,s); } } } } };
     */

    final Action dfa = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        // FuLog.debug("BEX: delete files");
        JList c = (JList) _evt.getSource();
        DtFilesSelection s = new DtFilesSelection(c.getSelectedValues());
        VfsFile[] f = s.getAllFiles();
        VfsOperations o = VfsLib.getOperations();

        o.deleteFiles(f);
      }
    };

    dirs_ = new BuEmptyList() {
      {
        setName("LT_DIRS");
        setEmptyText("No directory");
        // setBorder(new EmptyBorder(3,3,3,3));

        //setTransferHandler((TransferHandler) DFH);
        setDragEnabled(false);
        // dirs_.setDropTarget();
        // new DropTarget(dirs_,DtLib.ALL,dtl);
       
        
        // probleme avec java 6 update 2
        // try {
        //  getDropTarget().addDropTargetListener((DropTargetListener) DTL);
        //} catch (TooManyListenersException ex) {}

        addMouseListener(mh);
        addKeyListener(kh);
        addListSelectionListener(BuExplorer.this);

        getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
        getActionMap().put("delete", dfa);
      }

      @Override
      protected boolean isDropSelectionMode() {
        return true;
      }
    };

    files_ = new BuEmptyList() {
      {
        setName("LT_FILES");
        setEmptyText("No file");
        // setBorder(new EmptyBorder(3,3,3,3));

        /*
         * if(FuLib.jdk()>=1.2) { setTransferHandler((TransferHandler)DFH); setDragEnabled(true);
         * //files_.setDropTarget(); //new DropTarget(files_,DtLib.ALL,dtl); try {
         * getDropTarget().addDropTargetListener((DropTargetListener)DTL); } catch(TooManyListenersException ex) { } }
         */

        addMouseListener(mh);
        addKeyListener(kh);
        addListSelectionListener(BuExplorer.this);

        if (FuLib.jdk() >= 1.3) {
          getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
          getActionMap().put("delete", dfa);
        }
      }
    };

    filters_ = new BuComboBox();
    filters_.setMaximumRowCount(20);

    sorters_ = new BuComboBox();
    sorters_.addItem(getS("Nom"));
    sorters_.addItem(getS("Type"));
    sorters_.addItem(getS("Date"));
    sorters_.addItem(getS("Taille"));
    sorters_.setMaximumRowCount(20);

    pmCurrent_ = new BuPopupMenu(getS("R�pertoire"));
    pmCurrent_.addMenuItem(getS("Rafra�chir"), "RAFRAICHIR", true);
    miResolve_ = pmCurrent_.addMenuItem(getS("R�soudre"), "RESOUDRE_REPERTOIRE", BuResource.BU.getMenuIcon("analyser"),
        !FuLib.isWindows());
    pmCurrent_.addMenuItem(getS("Cr�er..."), "CREER_REPERTOIRE", true);
    pmCurrent_.addSeparator();
    // miCvsUpdate_=pmCurrent_.addMenuItem("Update","CVS_UPDATE",false);
    // miCvsCommit_=pmCurrent_.addMenuItem("Commit","CVS_COMMIT",false);
    // miAntBuild_ =pmCurrent_.addMenuItem("Build" ,"ANT_BUILD" ,false);
    // miMake_ =pmCurrent_.addMenuItem("Make" ,"MAKE" ,false);

    pmDirs_ = new BuPopupMenu(getS("R�pertoires"));
    pmDirs_.addMenuItem(getS("Ouvrir"), "OUVRIR_REPERTOIRE", true);
    pmDirs_.addSeparator();
    pmDirs_.addMenuItem(getS("Cr�er..."), "CREER_REPERTOIRE", true);
    pmDirs_.addMenuItem(getS("Renommer..."), "RENOMMER_REPERTOIRE", BuResource.BU.getIcon("renommer"), true);
    pmDirs_.addMenuItem(getS("D�truire..."), "DETRUIRE_REPERTOIRE", BuResource.BU.getIcon("detruire"), true);

    pmFiles_ = new BuPopupMenu(getS("Fichiers"));
    miOuvrirFichier_ = pmFiles_.addMenuItem(getS("Ouvrir"), "OUVRIR_FICHIER", true);
    pmFiles_.addMenuItem(getS("Tout s�lectionner"), "TOUTSELECTIONNER", true);
    pmFiles_.addSeparator();
    pmFiles_.addMenuItem(getS("Renommer..."), "RENOMMER_FICHIER", true);
    pmFiles_.addMenuItem(getS("D�truire..."), "DETRUIRE_FICHIER", true);
    pmFiles_.addSeparator();
    // miCvsAdd_=pmFiles_.addMenuItem("Add","CVS_ADD",false);

    /*
     * BuPanel p0=new BuPanel(new BuBorderLayout(5,5)); p0.setBorder(BuBorders.EMPTY0000); p0.add(lbURL_
     * ,BuBorderLayout.WEST); p0.add(current_,BuBorderLayout.CENTER);
     */

    BuButton btAller = new BuButton();
    btAller.setActionCommand("ALLER");
    btAller.setIcon(BuResource.BU.loadMenuCommandIcon("ALLER"));
    btAller.setMargin(BuInsets.INSETS0000);
    btAller.addActionListener(gl);

    BuButton btEffacer = new BuButton();
    btEffacer.setActionCommand("EFFACER");
    btEffacer.setIcon(BuResource.BU.loadMenuCommandIcon("EFFACER"));
    btEffacer.setMargin(BuInsets.INSETS0000);
    btEffacer.addActionListener(cl);

    BuPanel urlBar_ = new BuPanel(new BuBorderLayout(GAP, GAP, true, false));
    // urlBar_.setBorder(new EmptyBorder(2,5,2,5));
    // urlBar_.setBorder(BuBorders.EMPTY1111);
    if (_viewer) urlBar_.add(lbURL_, BuBorderLayout.WEST);
    urlBar_.add(current_, BuBorderLayout.CENTER);
    if (_viewer) urlBar_.add(btEffacer, BuBorderLayout.EAST);
    urlBar_.add(btAller, BuBorderLayout.EAST);

    BuPanel p1 = new BuPanel(new BuVerticalLayout(GAP, true, true));
    p1.add(roots_); // new BuRotatePanel(roots_));
    // TMP p1.add(p0);

    BuScrollPane spDirs = new BuScrollPane(dirs_);
    spDirs.setPreferredSize(new Dimension(CELL_WIDTH + 15, 150));
    p1.add(spDirs);
    // p1.setPreferredSize(new Dimension(150,220));

    BuPanel p2 = new BuPanel(new BuVerticalLayout(GAP, true, true));
    // p2.setPreferredSize(new Dimension(150,220));
    p2.add(filters_);
    p2.add(sorters_);
    BuScrollPane spFiles = new BuScrollPane(files_);
    spFiles.setPreferredSize(new Dimension(CELL_WIDTH + 15, 150));
    p2.add(spFiles);

    // p2=new BuRotatePanel(p2);

    BuSplit2Pane spv = new BuSplit2Pane(p1, p2, (_viewer ? BuSplit3Pane.HORIZONTAL : BuSplit3Pane.VERTICAL));

    setLayout(new BuBorderLayout(GAP, GAP));
    add(urlBar_, BuBorderLayout.NORTH);// TMP?

    addMtab();
    addDefaultFilters();

    // new DndSource(DndRequestSelection.SINGLETON).add(dirs_);
    // new DndSource(DndRequestSelection.SINGLETON).add(files_);

    if (_viewer) {
      /*
       * viewer_=new BuList(); viewer_.setBackground(UIManager.getColor("Panel.background"));
       * viewer_.setCellRenderer(createFileViewer());
       */

      int nbcols = BuPreferences.BU.getIntegerProperty("explorer.columns", 1);
      if (nbcols < 1) nbcols = 1;
      if (nbcols > 5) nbcols = 5;

      viewer_ = createFileViewer();
      viewer_.setBorder(BuLib.getEmptyBorder(GAP));

      viewer_.setLayout(new BuGridLayout(nbcols, GAP, GAP, true, true, false, false));

      final BuScrollPane sp3 = new BuScrollPane(viewer_);
      sp3.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      sp3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      Dimension ps3 = sp3.getPreferredSize();
      sp3.setPreferredSize(new Dimension(ps3.width + 200 * nbcols - 3, ps3.height + 400 + 2));
      // (sp3.getPreferredSize().width+202*nbcols-2,409));
      sp3.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      sp3.getVerticalScrollBar().setUnitIncrement(200 + GAP);
      sp3.getVerticalScrollBar().setBlockIncrement(400 + 2 * GAP);
      sp3.getHorizontalScrollBar().setUnitIncrement(200 + GAP);
      sp3.getHorizontalScrollBar().setBlockIncrement(400 + 2 * GAP);

      /*
       * final BuSplit3Pane sph=new BuSplit3Pane (spv,sp3,null,BuSplit3Pane.HORIZONTAL); add(sph);
       */
      final BuPanel sph = new BuPanel(new BuBorderLayout(GAP, GAP));
      sph.add(spv, BuBorderLayout.CENTER);
      sph.add(sp3, BuBorderLayout.EAST);
      add(sph);

      /*
       * final BuToggleButton tb=new BuToggleButton("Viewer"); tb.setRequestFocusEnabled(false);
       * tb.setMargin(BuInsets.INSETS1111); tb.setFont(getETF());
       * tb.setIcon(BuResource.resizeIcon(BuResource.BU.getIcon("voir"),12)); tb.setSelected(true);
       * tb.setHorizontalAlignment(BuToggleButton.RIGHT); tb.addActionListener(new ActionListener() { public void
       * actionPerformed(ActionEvent _evt) { sp3.setVisible(tb.isSelected()); updateViewer(); //sph.updateSplits();
       * sph.revalidate(); } }); BuPanel ptb=new BuPanel(new FlowLayout(FlowLayout.RIGHT,0,0)); ptb.add(tb);
       * p2.add(ptb);
       */
    } else {
      add(spv);
    }

    updateLnf();
    roots_.addActionListener(this);
    filters_.addActionListener(this);
    sorters_.addActionListener(this);

    if (_viewer) {
      // adjust the top of the scrollpanes
      int ph1 = p1.getPreferredSize().height;
      int ph2 = p2.getPreferredSize().height;
      if (ph2 > ph1) {
        p1.setBorder(new EmptyBorder(ph2 - ph1, 0, 0, 0));
        p2.setBorder(BuBorders.EMPTY0000);
        // Dimension ps=current_.getPreferredSize();
        // current_.setPreferredSize(new Dimension(ps.width,ps.height+ph2-ph1));
      } else {
        p1.setBorder(BuBorders.EMPTY0000);
        p2.setBorder(new EmptyBorder(ph1 - ph2, 0, 0, 0));
      }
    }

    p1.setPreferredSize(new Dimension(CELL_WIDTH + 15, 220));
    p2.setPreferredSize(new Dimension(CELL_WIDTH + 15, 220));

    timer_ = new BuTimer(1000, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        if (isShowing() && (getCurrentDirectory() instanceof VfsFileFile)) refresh();
      }
    });
    // if(isShowing())
    setCurrentDirectory(VfsFile.ensureVfsFile(_path));

    if (VfsLib.getOperations() == null) VfsLib.setOperations(new BuVfsOperations(null));

    /*
     * new Thread(new Runnable() { public void run() { while(!isShowing()) { try { Thread.sleep(250L); }
     * catch(InterruptedException ex) { } } SwingUtilities.invokeLater(new Runnable() { public void run() {
     * setCurrentDirectory(VfsFile.ensureVfsFile(_path)); } }); } },"Explorer Initial Start").start();
     */
  }

  @Override
  public boolean isShowing() {
    boolean r = super.isShowing();

    if (timer_ != null) {
      if (r && !timer_.isRunning()) timer_.start();
      if (!r && timer_.isRunning()) timer_.stop();
    }

    return r;
  }

  @Override
  public void updateUI() {
    super.updateUI();
    try {
      updateLnf();
      pmCurrent_.updateUI();
      pmDirs_.updateUI();
      pmFiles_.updateUI();
    } catch (Exception ex) {}
  }

  static Font getETF() {
    return BuLib.deriveFont("List", Font.PLAIN, -2);
  }

  private void updateLnf() {
    i_computer = getLnfIcon("FileView.computerIcon");
    i_disk = getLnfIcon("FileView.hardDriveIcon");
    i_floppy = getLnfIcon("FileView.floppyDriveIcon");
    i_directory = getLnfIcon("FileView.directoryIcon");
    i_file = getLnfIcon("FileView.fileIcon");
    i_home = getLnfIcon("FileChooser.homeFolderIcon");
    i_parent = getLnfIcon("FileChooser.upFolderIcon");

    iconsize_ = BuResource.BU.getDefaultMenuSize();
    if (i_directory != null) {
      iconsize_ = Math.max(iconsize_, i_directory.getIconWidth());
      iconsize_ = Math.max(iconsize_, i_directory.getIconHeight());
    }
    if (i_file != null) {
      iconsize_ = Math.max(iconsize_, i_file.getIconWidth());
      iconsize_ = Math.max(iconsize_, i_file.getIconHeight());
    }
    if (i_home != null) {
      iconsize_ = Math.max(iconsize_, i_home.getIconWidth());
      iconsize_ = Math.max(iconsize_, i_home.getIconHeight());
    }

    iconcache_ = new FuRefTable(FuRefTable.WEAK, FuRefTable.SOFT);

    /*
     * if(i_file!=null) DndIcons.put(VfsFile.class,i_file); else
     * DndIcons.put(VfsFile.class,BuResource.BU.getIcon("document"));
     */

    Font ft = getETF();
    // current_.setFont(ft);
    roots_.setFont(ft);
    filters_.setFont(ft);
    sorters_.setFont(ft);
    dirs_.setFont(ft);
    files_.setFont(ft);

    roots_.setRenderer(new FileCR(true));
    dirs_.setCellRenderer(new FileCR(false));
    files_.setCellRenderer(new FileCR(false));
    filters_.setRenderer(new FilterCR());
    sorters_.setRenderer(new FilterCR());

    if (WRAP_LIST/* &&(FuLib.jdk()>=1.4) */) {
      dirs_.setLayoutOrientation(JList.VERTICAL_WRAP);
      files_.setLayoutOrientation(JList.VERTICAL_WRAP);
      dirs_.setVisibleRowCount(0);
      files_.setVisibleRowCount(0);
    }
  }

  protected Icon getLnfIcon(String _key) {
    Icon r = UIManager.getIcon(_key);

    /*
     * if(r instanceof ImageIcon) { Image img=((ImageIcon)r).getImage(); int
     * rw=Math.max(img.getWidth(this),img.getHeight(this)); int dw=BuResource.BU.getDefaultSize(); FuLog.debug("%%%
     * RW="+rw+" DW="+dw); if(rw>dw) r=new BuIcon(img.getScaledInstance (dw,dw,Image.SCALE_SMOOTH)); }
     */

    return r;
  }

  protected void updateMenuCurrent() {
    miResolve_.setEnabled(false);
    // miCvsUpdate_.setEnabled(false);
    // miCvsCommit_.setEnabled(false);
    // miAntBuild_ .setEnabled(false);
    // miMake_ .setEnabled(false);
    pmCurrent_.computeMnemonics();

    Runnable runnable = new Runnable() {
      @Override
      public void run() {
        boolean available = false;
        VfsFile r = VfsFile.createFile(FuLib.expandedPath(current_.getText()));
        try {
          available = !r.getCanonicalPath().equals(r.getAbsolutePath());
        } catch (IOException ex) {}

        // VfsFile dir=getCurrentDirectory();

        final boolean avl = available;

        /*
         * final boolean cvs=(dir!=null)&& dir.createChild("CVS" ).exists(); final boolean ant=(dir!=null)&&
         * dir.createChild("build.xml").exists(); final boolean make=(dir!=null)&& dir.createChild("Makefile"
         * ).exists();
         */

        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            miResolve_.setEnabled(avl);
            // miCvsUpdate_.setEnabled(cvs);
            // miCvsCommit_.setEnabled(cvs);
            // miAntBuild_ .setEnabled(ant);
            // miMake_ .setEnabled(make);
            pmCurrent_.computeMnemonics();
          }
        });
      }
    };

    if (SwingUtilities.isEventDispatchThread()) {
      Thread t = new Thread(runnable, "Explorer Menu Updater");
      t.setPriority(Thread.MIN_PRIORITY);
      t.start();
    } else
      runnable.run();
  }

  protected void updateMenuDirs() {
    int l = dirs_.getSelectedValues().length;
    BuActionEnabler.setEnabledForAction(pmDirs_, "OUVRIR_REPERTOIRE", l == 1);
    BuActionEnabler.setEnabledForAction(pmDirs_, "DETRUIRE_REPERTOIRE", l == 1);
    BuActionEnabler.setEnabledForAction(pmDirs_, "RENOMMER_REPERTOIRE", l == 1);
    pmDirs_.computeMnemonics();
  }

  protected void updateMenuFiles() {
    int l = files_.getSelectedValues().length;
    int s = listeners_.size();

    BuActionEnabler.setEnabledForAction(pmFiles_, "OUVRIR_FICHIER", (l > 0) && (s > 0));
    BuActionEnabler.setEnabledForAction(pmFiles_, "DETRUIRE_FICHIER", l >= 1);
    BuActionEnabler.setEnabledForAction(pmFiles_, "RENOMMER_FICHIER", l == 1);

    // VfsFile dir=getCurrentDirectory();
    // miCvsAdd_.setEnabled
    // ((dir!=null)&&dir.createChild("CVS").exists());
    pmFiles_.computeMnemonics();
  }

  protected void updateViewer() {
    if (viewer_ == null) return;

    Object[] d = dirs_.getSelectedValues();
    Object[] f = files_.getSelectedValues();

    /*
     * if((d!=null)&&(d.length>0)&&(d[0] instanceof URLAdapter)) { URLAdapter p=(URLAdapter)d[0];
     * FuLog.debug("Name="+p.getName()); FuLog.debug("Path="+p.getPath()); FuLog.debug("Dir.="+p.isDirectory()); }
     * if((f!=null)&&(f.length>0)&&(f[0] instanceof URLAdapter)) { URLAdapter p=(URLAdapter)f[0];
     * FuLog.debug("Name="+p.getName()); FuLog.debug("Path="+p.getPath()); FuLog.debug("Dir.="+p.isDirectory()); }
     */

    /*
     * DefaultListModel lm=new DefaultListModel(); for(int i=0;i<d.length;i++) lm.addElement(d[i]); for(int i=0;i<f.length;i++)
     * lm.addElement(f[i]); viewer_.setModel(lm);
     */

    viewer_.updateContent(d, f);
  }

  public BuFileViewer createFileViewer() {
    BuFileViewer r = new BuFileViewer();
    r.setFileRenderer(new BuFileRenderer());
    return r;
  }

  public void addActionListener(ActionListener _l) {
    listeners_.addElement(_l);
  }

  public void removeActionListener(ActionListener _l) {
    listeners_.removeElement(_l);
  }

  class KeyHandler extends KeyAdapter {
    @Override
    public void keyTyped(KeyEvent _evt) {
      Object source = _evt.getSource();
      char c = Character.toUpperCase(_evt.getKeyChar());

      if (c == KeyEvent.VK_ENTER) {
        if ((source == dirs_) && (dirs_.getSelectedValues().length == 1)) openDirectory();
        else if ((source == files_) && (files_.getSelectedValues().length == 1)) openFile();
      } else if (Character.isLetterOrDigit(c)) {
        if ((source == dirs_) || (source == files_)) {
          try {
            JList l = (JList) source;
            ListModel m = l.getModel();
            int t = m.getSize();
            for (int i = 0; i < t; i++) {
              VfsFile f = (VfsFile) m.getElementAt(i);
              String v = f.getViewName();
              if ((v.length() > 0) && (Character.toUpperCase(v.charAt(0)) == c)) {
                l.setSelectedValue(f, true);
                break;
              }
            }
          } catch (Throwable th) {
            FuLog.error(th);
          }
        }
      }
    }
  }

  class MouseHandler extends MouseAdapter {
    @Override
    public void mouseClicked(MouseEvent _evt) {
      Object source = _evt.getSource();

      if (BuLib.isDoubleClick(_evt)) {
        if ((source == dirs_) && (dirs_.getSelectedValues().length == 1)) openDirectory();
        else if ((source == files_) && (files_.getSelectedValues().length == 1)) openFile();
      } else if (BuLib.isRight(_evt)) {
        if (source == current_) {
          updateMenuCurrent();
          Point p = SwingUtilities.convertPoint(current_, new Point(0, current_.getHeight()), BuExplorer.this);
          pmCurrent_.show(BuExplorer.this, p.x, p.y + 2);// p.x,p.y);
        } else if (source == dirs_) {
          updateMenuDirs();
          Point p = dirs_.computePopupMenuLocation(_evt);
          p = SwingUtilities.convertPoint(dirs_, p, BuExplorer.this);
          pmDirs_.show(BuExplorer.this, p.x, p.y);
          /*
           * Point p=SwingUtilities.convertPoint (dirs_,new Point(0,_evt.getY()),BuExplorer.this);
           * pmDirs_.show(BuExplorer.this,p.x,p.y+10);//p.x,p.y);
           */
        } else if (source == files_) {
          updateMenuFiles();
          Point p = files_.computePopupMenuLocation(_evt);
          p = SwingUtilities.convertPoint(files_, p, BuExplorer.this);
          pmFiles_.show(BuExplorer.this, p.x, p.y);
          /*
           * Point p=SwingUtilities.convertPoint (files_,new Point(0,_evt.getY()),BuExplorer.this);
           * pmFiles_.show(BuExplorer.this,p.x,p.y+10);//p.x,p.y);
           */
        }
      }
    }
  }

  public class Root extends VfsFileFile {
    private String type_;

    public Root(String _mount, String _display, String _type) {
      super(_mount);
      type_ = _type;
      if ("..".equals(type_)) setViewName("..");
      else if (!_mount.equals(_display)) setViewName(_display);
    }

    public Icon getViewIcon() {
      Icon r = i_disk;

      if ("..".equals(type_)) r = i_parent;
      else if ("home".equals(type_)) r = i_home;
      else if ("directory".equals(type_)) r = i_directory;
      else if ("computer".equals(type_)) r = i_computer;
      else if ("floppy".equals(type_)) r = i_floppy;

      return r;
    }
  }

  final class FileCR extends BuAbstractCellRenderer {
    private int type = BuPreferences.BU.getIntegerProperty("explorer.fileicon", 2);

    public FileCR(boolean _cb) {
      super(_cb ? BuAbstractCellRenderer.COMBOBOX : BuAbstractCellRenderer.LIST);

      if (!_cb && WRAP_LIST/* &&(FuLib.jdk()>=1.4) */) {
        setItemWidth(CELL_WIDTH);
        setDarkerOddLines(false);
      }
    }

    @Override
    public Component getListCellRendererComponent(JList _list, Object _value, int _index, boolean _selected,
                                                  boolean _focus) {
      BuLabel r = (BuLabel) super.getListCellRendererComponent(_list, _value, _index, _selected, _focus);

      r.setFont(getETF());

      VfsFile f = (VfsFile) _value;

      String text;
      if (type_ == BuAbstractCellRenderer.COMBOBOX) text = f.getViewText();
      else
        text = f.getViewName();

      Icon icon = null;

      if (type != 0) {
        boolean testlink = true;

        if (f instanceof Root) {
          icon = ((Root) f).getViewIcon();
          testlink = false;
        } else if (f.isDirectory()) {
          if ("..".equals(text)) {
            icon = i_parent;
            testlink = false;
          } else {
            icon = i_directory;
          }
        } else if (f.isFile()) {
          icon = i_file;
          testlink = false; // tmp?

          if ((type == 2) && (viewer_ != null)) {
            Icon fi = viewer_.getFileRenderer().getIcon(f);

            if (fi instanceof BuIcon) {
              int oiw = 16;
              if (icon != null) oiw = Math.min(icon.getIconWidth(), icon.getIconHeight());
              int fiw = fi.getIconWidth();
              if ((fiw > oiw) && (fiw > 16)) fi = BuResource.resizeIcon((BuIcon) fi, oiw);
            }

            if (fi != null) icon = fi;
          }
        } else {
          icon = BuResource.BU.getIcon("detruire");// ,16);
          r.setForeground(Color.red);
          testlink = false;
        }

        if (testlink && f.isLink()) icon = new BuComposedIcon(icon, BuResource.BU.getIcon("bu_link.gif"), CENTER,
            CENTER);
      }

      // if(type_==BuAbstractCellRenderer.COMBOBOX)
      icon = adaptIcon(icon);

      r.setText(text);
      r.setIcon(icon);
      r.setDisabledIcon(icon);
      return r;
    }
  }

  private final class FilterCR extends BuAbstractCellRenderer {
    public FilterCR() {
      super(BuAbstractCellRenderer.COMBOBOX);
    }

    @Override
    public Component getListCellRendererComponent(JList _list, Object _value, int _index, boolean _selected,
                                                  boolean _focus) {
      BuLabel r = (BuLabel) super.getListCellRendererComponent(_list, _value, _index, _selected, _focus);

      r.setFont(getETF());

      String text;
      Icon icon;
      if (_value instanceof BuFileFilter) {
        icon = ((BuFileFilter) _value).getIcon();
        text = ((BuFileFilter) _value).getDescription();
      } else {
        icon = null;
        text = "" + _value;
      }

      r.setText(text);
      r.setIcon(adaptIcon(icon));
      return r;
    }
  }

  protected Icon adaptIcon(final Icon _icon) {
    Icon r = null;

    if (_icon != null) {
      r = (Icon) iconcache_.get(_icon);
      if (r != null) return r;
    }

    int s = iconsize_;

    if ((_icon != null) && ((_icon.getIconWidth() > s) || (_icon.getIconHeight() > s))) {
      if (_icon instanceof BuIcon) {
        r = BuResource.resizeIcon((BuIcon) _icon, s);
      }
    }

    if (r == null) {
      if ((_icon == null) || (_icon.getIconWidth() < s) || (_icon.getIconHeight() < s)) r = new BuFixedSizeIcon(_icon,
          s, s);
      else
        r = _icon;
    }

    if (/* (r!=null)&& */(_icon != null)) iconcache_.put(_icon, r);

    return r;
  }

  protected static String clean(String _dir) {
    String r = _dir;
    if (r != null) {
      r = r.trim();
      if ("".equals(r)) {
        r = null;
      } else {
        r = FuLib.expandedPath(r);
        if ((r.length() > 1) && r.endsWith(File.separator)) r = r.substring(0, r.length() - 1);
      }
    }
    return r;
  }

  protected void addMtab() {
    if (VfsFile.isRamMode()) {
      roots_.addItem(new VfsFileRam("/"));
      return;
    }

    String od = clean(BuPreferences.BU.getStringProperty("directory.open", null));
    if (od != null) roots_.addItem(new Root(od, od, "directory"));

    String sd = clean(BuPreferences.BU.getStringProperty("directory.save", null));
    if ((sd != null) && !sd.equals(od)) roots_.addItem(new Root(sd, sd, "directory"));

    String ud = clean(FuLib.getSystemProperty("user.dir"));
    if ((ud != null) && !ud.equals(sd) && !ud.equals(od)) roots_.addItem(new Root(ud, ud, "directory"));

    String hd = clean(FuLib.getSystemProperty("user.home"));
    if ((hd != null) && !hd.equals(ud) && !hd.equals(sd) && !hd.equals(od)) roots_.addItem(new Root(hd, getS("Maison"),
        "home"));

    if (FuLib.isUnix()) {
      LineNumberReader rin = null;
      try {
        rin = new LineNumberReader(new InputStreamReader(new FileInputStream("/etc/mtab")));
        // FileReader (TMP)

        String l;
        while ((l = rin.readLine()) != null) {
          l = l.trim();
          if (l.startsWith("#")) continue;

          try {
            StringTokenizer st = new StringTokenizer(l);
            String device = st.nextToken();
            String mount = st.nextToken();
            String type = st.nextToken();
            if (!"none".equals(device) && !"swap".equals(type) && !"devpts".equals(type) && !"devfs".equals(type)
                && !"tmpfs".equals(type) && !"usbdevfs".equals(type) && !"/boot".equals(mount)) {
              if (!device.startsWith("/")) type = "computer";
              if (mount.endsWith("floppy")) type = "floppy";

              String display = mount.substring(mount.lastIndexOf(File.separator) + 1);

              if ("".equals(display)) display = "/";
              /*
               * else display=display.substring(0,1) .toUpperCase()+display.substring(1);
               */

              roots_.addItem(new Root(mount, display, type));
            }
          } catch (Exception ex) {}
        }
      } catch (Throwable th) {} finally {
        if (rin != null) try {
          rin.close();
        } catch (IOException _evt) {
          FuLog.error(_evt);

        }
      }// IOException ex
    }

    try {
      // Method m=File.class.getMethod("listRoots",CLASS0);
      // File[] l=(File[])m.invoke(null,OBJECT0);
      // TMP
      File[] l = null;
      if (FuLib.isWindows()) l = new File[] { new File("C:\\") };
      else
        l = new File[0];

      for (int i = 0; i < l.length; i++) {
        String p = l[i].getPath(); // AbsolutePath();
        // if(!p.equals("/")&&!p.equals("A:\\")&&!p.equals("B:\\"))
        roots_.addItem(new Root(p, p, "disk"));
      }
    }
    // catch(NoSuchMethodException ex1) { }
    catch (Exception ex2) {
      FuLog.error(ex2);
    }

    for (int i = 1; i <= 9; i++) {
      String path = BuPreferences.BU.getStringProperty("explorer.favorite." + i + ".path");
      if (!"".equals(path)) {
        URL u = null;
        try {
          u = new URL(path);
        } catch (MalformedURLException ex) {}
        if (u != null) {
          roots_.addItem(VfsFile.createFile(u));
        } else {
          String type = BuPreferences.BU.getStringProperty("explorer.favorite." + i + ".type", "directory");
          Root f = new Root(path, path, type);
          if (f.exists()) roots_.addItem(f);
        }
      }
    }
  }

  public void addDefaultFilters() {
    BuFileFilter all = new BuFileFilterAll();
    all.setExtensionListInDescription(false);
    addFilter(all);

    if (app_ == null) {
      BuFileFilter archives = new BuFileFilter(BuFileExtensions.ARCHIVES, getS("Archives"));
      BuFileFilter images = new BuFileFilter(BuFileExtensions.IMAGES, getS("Images"));
      BuFileFilter sounds = new BuFileFilter(BuFileExtensions.SOUNDS, getS("Musique et sons"));
      BuFileFilter movies = new BuFileFilter(BuFileExtensions.MOVIES, getS("Films"));
      BuFileFilter sources = new BuFileFilter(BuFileExtensions.SOURCES, getS("Sources"));
      BuFileFilter texts = new BuFileFilter(BuFileExtensions.TEXTS, getS("Textes"));

      archives.setExtensionListInDescription(false);
      images.setExtensionListInDescription(false);
      movies.setExtensionListInDescription(false);
      sounds.setExtensionListInDescription(false);
      sources.setExtensionListInDescription(false);
      texts.setExtensionListInDescription(false);

      all.setIcon(BuResource.BU.getMenuIcon("document"));
      archives.setIcon(BuResource.BU.getMenuIcon("document"));// "archive" ));
      images.setIcon(BuResource.BU.getMenuIcon("image"));
      movies.setIcon(BuResource.BU.getMenuIcon("video"));
      sounds.setIcon(BuResource.BU.getMenuIcon("musique"));
      sources.setIcon(BuResource.BU.getMenuIcon("document"));// "source" ));
      texts.setIcon(BuResource.BU.getMenuIcon("texte"));

      addFilter(archives);
      addFilter(images);
      addFilter(movies);
      addFilter(sounds);
      addFilter(sources);
      addFilter(texts);
    }

    filters_.setSelectedItem(all);
  }

  private Hashtable filters_list = new Hashtable(11);

  public void addFilter(BuFileFilter _ff) {
    addFilter(_ff, false);
  }

  public void addFilter(BuFileFilter _ff, boolean _temporary) {
    // _ff.setExtensionListInDescription(false);
    if (filters_list.get(_ff) == null) {
      filters_list.put(_ff, _temporary ? Boolean.TRUE : Boolean.FALSE);
      if (filters_.isShowing()) filters_.hidePopup();
      filters_.removeAllItems();

      int l = filters_list.size();
      Object[] o = new Object[l];
      Enumeration e = filters_list.keys();
      int i = 0;
      while (e.hasMoreElements()) {
        o[i] = e.nextElement();
        i++;
      }
      FuSort.sort(o);
      for (i = 0; i < l; i++)
        filters_.addItem(o[i]);
    }
  }

  public void cleanFilters() {
    Enumeration e = filters_list.keys();
    Vector v = new Vector();
    while (e.hasMoreElements()) {
      Object f = e.nextElement();
      if (Boolean.TRUE.equals(filters_list.get(f))) v.addElement(f);
    }
    for (int i = 0; i < v.size(); i++) {
      Object o = v.elementAt(i);
      filters_list.remove(o);
    }
  }

  protected static class Model implements Serializable, ListModel {
    private VfsFile[] els_;
    private int nb_;
    private Vector ldl_;

    public Model(VfsFile[] _els, int _nb) {
      els_ = _els;
      nb_ = _nb;
      ldl_ = new Vector();
    }

    @Override
    public int getSize() {
      return nb_;
    }

    @Override
    public Object getElementAt(int _i) {
      return els_[_i];
    }

    @Override
    public void addListDataListener(ListDataListener _l) {
      ldl_.addElement(_l);
    }

    @Override
    public void removeListDataListener(ListDataListener _l) {
      ldl_.removeElement(_l);
    }
  }

  public VfsFile getCurrentRoot() {
    return (VfsFile) roots_.getSelectedItem();
  }

  public VfsFile getCurrentDirectory() {
    return last_;
  }

  public void setCurrentDirectory(VfsFile _dir) {
    if ((_dir == null) || !_dir.exists()) return;
    VfsFile dir = _dir;
    if (!dir.isAbsolute()) {
      try {
        dir = dir.getCanonicalVfsFile();
      } catch (IOException ex) {}
    }

    VfsFile d = dir;
    if (!d.isDirectory()) d = d.getParentVfsFile();

    if ((d == null) || !d.exists()) return;

    int i, n;
    synchronized (roots_) {
      n = roots_.getItemCount();
      for (i = 0; i < n; i++) {
        VfsFile r = (VfsFile) roots_.getItemAt(i);
        // FuLog.debug("ROOT: "+r+" "+d);//.equals(r));
        if (d.equals(r)) {
          roots_.setSelectedItem(r);
          break;
        }
      }
    }

    if (i == n) updateLists(d, dir);
    else if (!dir.isDirectory()) files_.setSelectedValue(dir, true);
  }

  public VfsFile getSelectedDirectory() {
    return (VfsFile) dirs_.getSelectedValue();
  }

  public VfsFile getSelectedFile() {
    return (VfsFile) files_.getSelectedValue();
  }

  // actions

  public void refresh() {
    updateLists(getCurrentDirectory(), null);
  }

  public void openDirectory() {
    setCurrentDirectory(getSelectedDirectory());
  }

  public final void openFile() {
    fireFileAction("OUVRIR");
  }

  public final void extraFile(String _cmd) {
    fireFileAction(_cmd);
  }

  protected void fireFileAction(String _action) {
    Object[] selected = files_.getSelectedValues();
    if (selected == null) return;

    for (int i = 0; i < selected.length; i++)
      if (((VfsFile) selected[i]).canRead()) {
        String path = ((VfsFile) selected[i]).getAbsolutePath();
        ActionEvent evt = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, _action + "(" + path + ")");
        for (Enumeration e = listeners_.elements(); e.hasMoreElements();)
          ((ActionListener) e.nextElement()).actionPerformed(evt);
      }
  }

  public void addExtraFileCommand(String _s, String _cmd, BuIcon _icon) {
    // pmFiles_.addSeparator();
    pmFiles_.addMenuItem(_s, "EXTRA_FICHIER(" + _cmd + ")", _icon, true);
  }

  public void selectAllFiles() {
    ListModel model = files_.getModel();
    int size = model.getSize();
    if (size > 0) files_.setSelectionInterval(0, model.getSize() - 1);
  }

  protected void resolveDirectory() {
    VfsFile r = VfsFile.createFile(FuLib.expandedPath(current_.getText()));
    try {
      String c = r.getCanonicalPath();
      if (!c.equals(r.getAbsolutePath())) current_.setText(c);
    } catch (IOException ex) {}
  }

  public void createDirectory() {
    BuDialogInput d = new BuDialogInput(null, null, "Cr�er", "Nom du sous-r�pertoire:", "");
    if (d.activate() == JOptionPane.OK_OPTION) {
      String name = d.getValue();
      VfsFile current = getCurrentDirectory();

      if (current.createChild(name).mkdir()) refresh();
      else {
        new BuDialogError(null, null, "Impossible de cr�er le sous-r�pertoire " + name + "\n" + "dans le r�pertoire "
            + current.getAbsolutePath()).activate();
      }
    }
  }

  public void renameDirectory() {
    VfsFile selected = getSelectedDirectory();
    VfsLib.getOperations().renameFile(selected);

    /*
     * BuDialogInput d=new BuDialogInput (null,null,"Renommer","Nouveau nom du r�pertoire:",selected.getName());
     * if(d.activate()==JOptionPane.OK_OPTION) { String name =d.getValue(); VfsFile
     * target=selected.getParentVfsFile().createChild(name); if(selected.renameTo(target)) refresh(); else { new
     * BuDialogError (null,null, "Impossible de renommer le r�pertoire "+selected.getName()+"\n"+ "en "+name)
     * .activate(); } }
     */
  }

  public void renameFile() {
    VfsFile selected = getSelectedFile();
    VfsLib.getOperations().renameFile(selected);

    /*
     * BuDialogInput d=new BuDialogInput (null,null,"Renommer","Nouveau nom du fichier:",selected.getName());
     * if(d.activate()==JOptionPane.OK_OPTION) { String name =d.getValue(); VfsFile
     * target=selected.getParentVfsFile().createChild(name); if(selected.renameTo(target)) refresh(); else { new
     * BuDialogError (null,null, "Impossible de renommer le fichier "+selected.getName()+"\n"+ "en "+name) .activate(); } }
     */
  }

  public void deleteDirectory() {
    VfsFile[] selected = new DtFilesSelection(getSelectedDirectory()).getAllFiles();
    VfsLib.getOperations().deleteFiles(selected);
    // refresh();

    /*
     * BuDialogConfirmation d=new BuDialogConfirmation (null,null,"Voulez-vous vraiment d�truire\n"+ "le r�pertoire
     * "+selected.getName()+" ?"); if(d.activate()==JOptionPane.OK_OPTION) { if(selected.delete()) refresh(); else { new
     * BuDialogError (null,null, "Le r�pertoire "+selected.getName()+" n'a pas pu\n"+ "�tre d�truit.") .activate(); } }
     */
  }

  public void deleteFiles() {
    VfsFile[] selected = new DtFilesSelection(files_.getSelectedValues()).getAllFiles();
    VfsLib.getOperations().deleteFiles(selected);
    // refresh();

    /*
     * BuDialogConfirmation d=new BuDialogConfirmation (null,null,"Voulez-vous vraiment d�truire\n"+ "le fichier
     * "+selected.getName()+" ?"); if(d.activate()==JOptionPane.OK_OPTION) { if(selected.delete()) refresh(); else { new
     * BuDialogError (null,null, "Le fichier "+selected.getName()+" n'a pas pu\n"+ "�tre d�truit.") .activate(); } }
     */
  }

  // utils

  protected static final Cursor WAIT_CURSOR = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);

  public final void setProgression(int _p) {
    if (app_ != null && app_.getMainPanel() != null) app_.getMainPanel().setProgression(_p);
  }

  protected int liststamp(String[] _s) {
    int r = 0;
    if (_s != null) {
      for (int i = 0; i < _s.length; i++)
        if ((_s[i].length() > 0) && (_s[i].charAt(0) != '.')) r ^= _s[i].hashCode();
    }
    r &= 0xFFFFFF;
    // FuLog.debug("count="+_s.length);
    // FuLog.debug("liststamp="+r+" previous="+liststamp_);
    if (r == 1) r = -2;
    return r;
  }

  protected boolean updating_ = false;

  protected void updateLists(final VfsFile _dir, VfsFile _sel) {
    final boolean showing = isShowing();

    if (showing && !SwingUtilities.isEventDispatchThread()) throw new RuntimeException("Not in swing thread");

    if (updating_) return;

    if ((_dir == null) || !_dir.isDirectory()) return;

    // TODO: not perfect (swing thread)
    if (_dir.equals(last_) && (_dir instanceof VfsFileFile)) {
      if (timestamp_ == _dir.lastModified()) {
        // FuLog.debug("BEX: same time stamp");
        return;
      }

      if (liststamp_ == liststamp(_dir.list())) {
        // FuLog.debug("BEX: same list stamp");
        return;
      }
    }

    updating_ = true;

    // FuLog.debug("BEX: "+FuLib.codeLocation());
    VfsFile fileSelected = _sel;
    if (fileSelected == null) fileSelected = (VfsFile) files_.getSelectedValue();

    dirs_.setEmptyText("Reading...");
    dirs_.setModel(new Model(new VfsFile[0], 0));
    // dirs_.setEnabled(false);
    // dirs_.invalidate();

    files_.setEmptyText("Reading...");
    files_.setModel(new Model(new VfsFile[0], 0));
    // files_.setEnabled(false);
    // files_.invalidate();
    // repaint();

    final BuFileFilter filter = (BuFileFilter) filters_.getSelectedItem();
    final Cursor cursor = getCursor();
    final VfsFile sel = fileSelected;

    /*
     * new BuTaskOperation(app_,_dir.getName()) { public void act() { setPriority(Thread.MIN_PRIORITY);
     * updateLists0(_dir); } } .start();
     */

    Runnable runnable = new Runnable() {
      @Override
      public void run() {
        try {
          // FuLog.debug("BEX: update lists "+_dir);
          updateLists0(_dir, filter, cursor, sel, showing);
        } catch (Throwable th) {
          FuLog.error(th);
        }
      }
    };

    if (showing || EventQueue.isDispatchThread()) {
      Thread t = new Thread(runnable, "Explorer Lists Updater");
      t.setPriority(Thread.MIN_PRIORITY);
      t.start();
    } else {
      runnable.run();
    }
  }

 protected void updateLists0(final VfsFile _dir, BuFileFilter _filter, final Cursor _cursor, final VfsFile _sel,
      final boolean _showing) {
    if (EventQueue.isDispatchThread()) throw new RuntimeException("In swing thread. " + "Use updateLists() instead");

    // long before=System.currentTimeMillis();
    // FuLog.debug("BEX: showing="+_showing);

    if (!updating_) throw new RuntimeException("updating_ should be true");

    last_ = _dir;
    setProgression(10);

    // Cursor old_cursor=null;
    Runnable runnable = null;

    runnable = new Runnable() {
      @Override
      public void run() {
        if (!WAIT_CURSOR.equals(_cursor)) setCursor(WAIT_CURSOR);

        current_.setText(FuLib.reducedPath(_dir.getAbsolutePath()));
        current_.setToolTipText(_dir.getAbsolutePath());
        current_.revalidate();
        updateMenuCurrent();
      }
    };

    if (_showing) SwingUtilities.invokeLater(runnable);
    else
      runnable.run();

    // FuLog.debug("read "+(System.currentTimeMillis()-before));
    timestamp_ = _dir.lastModified();// System.currentTimeMillis();

    String[] els = _dir.list();
    liststamp_ = liststamp(els);
    // FuLog.debug("update liststamp="+liststamp_+" for "+_dir);
    if (els == null) els = STRING0;
    // FuLog.debug("%%% DIR="+_dir+" ["+els.length+"]");

    setProgression(20);

    int l = els.length;

    final VfsFile[] dirs = new VfsFile[l + 1];
    final VfsFile[] files = new VfsFile[l];
    int nb_dirs = 0;
    int nb_files = 0;

    VfsFile pf = _dir.getParentVfsFile();
    // FuLog.debug("%%% PF="+pf);
    if (pf instanceof VfsFileFile) {
      String p = pf.getAbsolutePath();
      pf = new Root(p, p, "..");
    }

    if (pf != null) {
      pf.setViewName("..");
      dirs[0] = pf;
      nb_dirs++;
    }

    setProgression(30);

    // cleanFilters();
    // addFilter(filter);
    // filters_.setSelectedItem(filter);

    for (int i = 0; i < l; i++) {
      if (els[i].startsWith(".")) continue;
      if (els[i].endsWith("~")) continue;
      if ("CVS".equals(els[i])) continue;

      VfsFile f = _dir.createChild(els[i]);

      
    
      if (f == null) {}

    //--cas particulier: si le fichier est un repertoire .POST
      //-- on place les repertoire .POST avec les fichiers --// 
      else
      if(f.isDirectory() && f.getName().contains(".POST")){
    	  
    	  files[nb_files] = f;
          nb_files++;
      }
     
      
      else if (f.isDirectory()) {
        dirs[nb_dirs] = f;
        nb_dirs++;
      } else if (f.isFile()) {
        // if(viewer_!=null)
        // {
        // String n=f.getName();
        // int p=n.lastIndexOf('.');
        // if(p>0)
        // {
        // String e=n.substring(p+1).toLowerCase();
        // addFilter(new BuFileFilter(e,_("Fichiers")+
        // " "+e.toUpperCase()),true);
        // }
        // }

        if ((_filter == null) || _filter.accept(f)) {
          files[nb_files] = f;
          nb_files++;
        }
      } else {
        // FuLog.debug("BEX: ignore entry "+f);
      }
    }

    // FuLog.debug("sort "+(System.currentTimeMillis()-before));
    els = null;
    setProgression(40);
    FuSort.sort(dirs, 0, nb_dirs);
    setProgression(50);
    FuSort.sort(files, 0, nb_files, getSorter());

    final int fnb_dirs = nb_dirs;
    final int fnb_files = nb_files;

    
   
    
    
    if (_sel != null) _sel.build();

    runnable = new Runnable() {
      @Override
      public void run() {
        // FuLog.debug("model");
        // dirs_.setEnabled(true);
        dirs_.setEmptyText("No directory");
        dirs_.setModel(new Model(dirs, fnb_dirs));
        dirs_.revalidate();
        setProgression(60);

        // files_.setEnabled(true);
        files_.setEmptyText("No file");
        files_.setModel(new Model(files, fnb_files));
        files_.revalidate();
        setProgression(70);

        setProgression(80);
        // FuLog.debug("end");
        repaint();

        if ((_sel != null) && !_sel.isDirectory()) files_.setSelectedValue(_sel, true);
        if (!WAIT_CURSOR.equals(_cursor)) setCursor(_cursor);

        setProgression(100);
        updating_ = false;
      }
    };

    if (_showing) SwingUtilities.invokeLater(runnable);
    else
      runnable.run();
    // FuLog.debug("end "+(System.currentTimeMillis()-before));
  }

  protected FuComparator getSorter() {
    FuComparator r;
    /*
     * String s=(String)sorters_.getSelectedItem(); if("Date".equals(s)) r=DATE_COMPARATOR; else if("Size".equals(s))
     * r=SIZE_COMPARATOR; else if("Type".equals(s)) r=TYPE_COMPARATOR; else r=NAME_COMPARATOR;
     */

    switch (sorters_.getSelectedIndex()) {
    case 1:
      r = TYPE_COMPARATOR;
      break;
    case 2:
      r = DATE_COMPARATOR;
      break;
    case 3:
      r = SIZE_COMPARATOR;
      break;
    default:
      r = NAME_COMPARATOR;
      break;
    }

    return r;
  }

  static final FuComparator NAME_COMPARATOR = new FuComparator() {
    @Override
    public int compare(Object _a, Object _b) {
      String a = _a.toString().toLowerCase();
      String b = _b.toString().toLowerCase();
      return a.compareTo(b);
    }
  };

  private static final FuComparator TYPE_COMPARATOR = new FuComparator() {
    @Override
    public int compare(Object _a, Object _b) {
      String a = _a.toString();
      String b = _b.toString();
      a = a.substring(a.lastIndexOf('.') + 1).toLowerCase();
      b = b.substring(b.lastIndexOf('.') + 1).toLowerCase();
      return a.equals(b) ? NAME_COMPARATOR.compare(_a, _b) : FuComparator.STRING_COMPARATOR.compare(a, b);
    }
  };

  private static final FuComparator DATE_COMPARATOR = new FuComparator() {
    @Override
    public int compare(Object _a, Object _b) {
      long a = ((VfsFile) _a).lastModified();
      long b = ((VfsFile) _b).lastModified();

      return ((a == b) ? 0 : ((a < b) ? -1 : 1));
    }
  };

  private static final FuComparator SIZE_COMPARATOR = new FuComparator() {
    @Override
    public int compare(Object _a, Object _b) {
      long a = ((VfsFile) _a).length();
      long b = ((VfsFile) _b).length();

      return ((a == b) ? 0 : ((a < b) ? 1 : -1));
    }
  };

  @Override
  public void valueChanged(ListSelectionEvent _evt) {
    if (_evt.getValueIsAdjusting()) return;

    Object source = _evt.getSource();

    if (source == dirs_) {
      updateMenuDirs();
      updateViewer();
    } else if (source == files_) {
      updateMenuFiles();
      updateViewer();
    }
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    // FuLog.debug("SOURCE="+_evt.getSource());

    Object source = _evt.getSource();

    if (source == roots_) {
      VfsFile r = getCurrentRoot();
      if (r != null) {
        timestamp_ = 1;
        liststamp_ = 1;
        updateLists(r, null);
      }
      // else FuLog.debug("BuExplorer: CURRENT_ROOT=null");
    } else if ((source == filters_) || (source == sorters_)) {
      VfsFile r = getCurrentDirectory();
      if (r != null) {
        timestamp_ = 1;
        liststamp_ = 1;
        updateLists(r, null);
      }
      // else FuLog.debug("BuExplorer: CURRENT_DIRECTORY=null");
    } else {
      String action = _evt.getActionCommand();
      String arg = "";

      int i = action.indexOf('(');
      if (i >= 0) {
        arg = action.substring(i + 1, action.length() - 1);
        action = action.substring(0, i);
      }

      if ("RAFRAICHIR".equals(action)) refresh();
      else if ("RESOUDRE_REPERTOIRE".equals(action)) resolveDirectory();
      else if ("OUVRIR_REPERTOIRE".equals(action)) openDirectory();
      else if ("OUVRIR_FICHIER".equals(action)) openFile();
      else if ("TOUTSELECTIONNER".equals(action)) selectAllFiles();
      else if ("CREER_REPERTOIRE".equals(action)) createDirectory();
      else if ("RENOMMER_REPERTOIRE".equals(action)) renameDirectory();
      else if ("RENOMMER_FICHIER".equals(action)) renameFile();
      else if ("DETRUIRE_REPERTOIRE".equals(action)) deleteDirectory();
      else if ("DETRUIRE_FICHIER".equals(action)) deleteFiles();
      else if ("EXTRA_FICHIER".equals(action)) extraFile(arg);
      else if ("CVS_UPDATE".equals(action)) runDirCmd("cvs update");
      else if ("CVS_COMMIT".equals(action)) runDirCmd("cvs commit");
      else if ("ANT_BUILD".equals(action)) runDirCmd("ant");
      else if ("MAKE".equals(action)) runDirCmd("make");
      else if ("CVS_ADD".equals(action)) runFileCmd("cvs add");
      else
        FuLog.warning("unknown action '" + action + "'");
    }
  }

  private void runDirCmd(String _cmd) {

    BuShell.runCommand(app_, "cd " + getCurrentDirectory().getAbsolutePath() + " ; " + _cmd);
  }

  private void runFileCmd(String _cmd1) {
    String cmd = "cd " + getCurrentDirectory().getAbsolutePath() + " ; " + _cmd1;

    Object[] selected = files_.getSelectedValues();
    if (selected == null) return;

    for (int i = 0; i < selected.length; i++)
      if (((VfsFile) selected[i]).canRead()) cmd += " " + ((VfsFile) selected[i]).getName();

    BuShell.runCommand(app_, cmd);
  }

  /*
   * public static void main(String[] _args) { VfsUrlBzip2.init(); VfsUrlData. init(); VfsUrlGzip. init(); VfsUrlRam.
   * init(); VfsUrlTar. init(); VfsUrlZip. init(); BuPreferences.BU.applyLookAndFeel();
   * BuPreferences.BU.applyLanguage(BuPreferences.BU.getLanguages()); DndIcons.setNoDropIcon (BuResource.BU.getIcon
   * (DndIcons.class.getResource("dnd-nodrop_24.gif" ))); DndIcons.setCanDropIcon (BuResource.BU.getIcon
   * (DndIcons.class.getResource("dnd-candrop_24.gif"))); DndIcons.setDefaultIcon (BuResource.BU.getIcon
   * (DndIcons.class.getResource("dnd-default_24.gif"))); DndIcons.put(String.class,BuResource.BU.getIcon("texte"));
   * DndIcons.put(URL.class ,BuResource.BU.getIcon("signet")); if(_args.length==0) _args=new String[] //{ "." }; {
   * FuLib.getSystemProperty("user.dir") }; for(int i=0;i<_args.length;i++) { if(_args[i].equals("--ram")) {
   * VfsFile.setRamMode(true); continue; } VfsFile p=VfsFile.createFile(FuLib.expandedPath(_args[i])); String
   * ext=_args[i].toLowerCase(); int j=ext.lastIndexOf("."); if(j>=0) ext=ext.substring(j+1); if(ext.equals("zip")||
   * ext.equals("cbr")|| ext.equals("ear")||ext.equals("jar")||ext.equals("war")) {
   * p=VfsFile.ensureVfsFile(p.getAbsoluteFile()); try { p=VfsFile.createFile(new URL("zip:"+p.toURL()+"!/")); }
   * catch(MalformedURLException ex) { } } else if(ext.equals("tar")||ext.equals("tgz")||
   * (ext.equals("gz")&&_args[i].toLowerCase().endsWith(".tar.gz"))) { p=VfsFile.ensureVfsFile(p.getAbsoluteFile()); try {
   * p=VfsFile.createFile(new URL("tar:"+p.toURL()+"!/")); } catch(MalformedURLException ex) { } } else
   * if(ext.equals("tbz")|| (ext.equals("bz2")&&_args[i].toLowerCase().endsWith(".tar.bz2"))) {
   * p=VfsFile.ensureVfsFile(p.getAbsoluteFile()); try { p=VfsFile.createFile(new URL("tar:"+p.toURL()+"!/")); }
   * catch(MalformedURLException ex) { } } //FuLog.debug("p="+p+" "+p.getClass()); final JFrame f=new
   * JFrame(BuResource.BU.getString("Explorateur")); final BuExplorer e=new BuExplorer(p); BuRegistry.register(f);
   * f.getContentPane().add(e); e.setBorder(BuLib.getEmptyBorder(GAP)); f.setLocation(140,35); f.pack(); f.invalidate();
   * f.validate(); f.show(); e.repaint(); //new BuDrawer(f,SwingConstants.RIGHT); new BuDrawer(f,SwingConstants.LEFT);
   * new BuDrawer(f,SwingConstants.TOP); new BuDrawer(f,SwingConstants.BOTTOM); f.addWindowListener(new WindowAdapter() {
   * public void windowClosing(WindowEvent _evt) { f.hide(); f.dispose(); BuRegistry.unregister(f); } }); } }
   */
}
