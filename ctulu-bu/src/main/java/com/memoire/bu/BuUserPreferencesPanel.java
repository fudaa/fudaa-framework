/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuUserPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can enter informations about
 * himself.
 *
 * user.*
 */
public class BuUserPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  BuCommonInterface appli_;
  BuPreferences     options_;

  BuTextField[]      text_;

  @Override
  public String getTitle()
  {
    return getS("Utilisateur");
  }

  @Override
  public String getCategory()
  {
    return getS("Syst�me");
  }

  private static final int NB=7;

  private static final String[] PROP=new String[]
    { "lastname","firstname","nickname","email",
      "title","organization","department" };

  private static final String[] LABEL=new String[]
    { "Nom[personne]","Pr�nom","Surnom","M�l",
      "Titre","Organisme","D�partement" };

  // Constructeur

  public BuUserPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_  =_appli;
    options_=BuPreferences.BU;
    text_   =new BuTextField[NB];

    BuGridLayout lo=new BuGridLayout(2,5,5,false,false,true,false);
    lo.setColumnXAlign(new float[] { 1.f,0.f });
    BuPanel p=new BuPanel(lo);
    //p.setLayout(lo);

    int n=0;
    for(int i=0;i<NB;i++)
    {
      BuLabel lb=new BuLabel(getS(LABEL[i])+":");
      p.add(lb,n++);
      text_[i]=new BuTextField();
      text_[i].setName("tf"+PROP[i].toUpperCase());
      text_[i].setActionCommand(PROP[i].toUpperCase());
      text_[i].addKeyListener(this);
      text_[i].addActionListener(this);
      p.add(text_[i],n++);
    }

    p.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getTitle()),
	EMPTY5555));

    setLayout(new BuVerticalLayout(5));
    //setLayout(new BuBorderLayout());
    setBorder(EMPTY5555);
    add(p);//,BuBorderLayout.CENTER);

    updateComponents();
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
  }

  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
    { return true; }
  
  @Override
  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees
  
  private void fillTable()
  {
    for(int i=0;i<NB;i++)
      options_.putStringProperty("user."+PROP[i],text_[i].getText());
    setDirty(false);
  }

  private void updateComponents()
  {
    for(int i=0;i<NB;i++)
      text_[i].setText(options_.getStringProperty("user."+PROP[i]));
    setDirty(false);
  }
}
