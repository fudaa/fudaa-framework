/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuFixedSizeIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Shape;
import java.io.Serializable;
import javax.swing.Icon;

/**
 * An Icon but with fixed size.
 */
public class BuFixedSizeIcon
    implements Icon, Serializable
{
  private Icon icon_;
  private int  w_,h_;

  public BuFixedSizeIcon(Icon _icon, int _w, int _h)
  {
    icon_=_icon;
    w_=_w;
    h_=_h;
  }

  @Override
  public int getIconWidth ()
  {
    return w_;
  }

  public void setIconWidth(int _w)
  {
    w_=_w;
  }

  @Override
  public int getIconHeight()
  {
    return h_;
  }

  public void setIconHeight(int _h)
  {
    h_=_h;
  }

  public Icon getIcon()
  {
    return icon_;
  }

  public void setIcon(Icon _icon)
  {
    icon_=_icon;
  }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    Icon icon=getIcon();

    if(icon!=null)
    {
      int w=getIconWidth();
      int h=getIconHeight();

      Shape old=_g.getClip();
      _g.clipRect(_x,_y,w,h);
      int x=_x+(w-icon.getIconWidth ())/2;
      int y=_y+(h-icon.getIconHeight())/2;
      icon.paintIcon(_c,_g,x,y);
      _g.setClip(old);
    }
  }
}
