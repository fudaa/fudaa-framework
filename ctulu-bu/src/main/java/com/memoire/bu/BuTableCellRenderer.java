/**
 * @modification $Date: 2007-02-21 16:33:51 $
 * @statut       unstable
 * @file         BuTableCellRenderer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

/**
 * A displayer for table cells, which darkens every second line and
 * accepts different formats for numbers and dates.
 */

public class BuTableCellRenderer
    extends BuAbstractCellRenderer
    //extends BuLabel implements TableCellRenderer
{
  public BuTableCellRenderer()
  {
    super(BuAbstractCellRenderer.TABLE);
  }

  private NumberFormat numberFormat_;
  public NumberFormat getNumberFormat() { return numberFormat_; }
  public void setNumberFormat(NumberFormat _f) { numberFormat_=_f; }

  private DateFormat dateFormat_;
  public DateFormat getDateFormat() { return dateFormat_; }
  public void setDateFormat(DateFormat _f) { dateFormat_=_f; }

  @Override
  public Component getTableCellRendererComponent
    (JTable _table, Object _value, boolean _selected,
     boolean _focus, int _row, int _column) 
  {
    boolean selected=_selected;
    // Je ne comprend pas l'utilit�, et ca pose un pb quand on fait une s�lection par programmation.
    /*    if(selected)
    {
      selected=false;
      int[] sr=_table.getSelectedRows();
      int[] sc=_table.getColumnModel().getSelectedColumns();
      boolean rsa=_table.getRowSelectionAllowed();
      boolean csa=_table.getColumnSelectionAllowed();
      int j,k;
      for(j=0;j<sr.length;j++)
	for(k=0;k<sc.length;k++)
	if((!rsa||(_row==sr[j]))&&(!csa||(_column==sc[k])))
	  { selected=true; break; }
    }*/

    BuLabel r=(BuLabel)super.getTableCellRendererComponent
	  (_table,_value,selected,_focus,_row,_column);

    Font   ft=r.getFont();
    Color  bg=r.getBackground();
    Color  fg=r.getForeground();
    int    a =LEFT;//r.getHorizontalAlignment();
    String t ="";  //r.getText();
    Icon   i =null;//r.getIcon();
    int    mh=Math.max(16,ft.getSize());

    // Border bd=UIManager.getBorder("Table.cellBorder");

    /*
    if(_selected)
    {
      bg=_table.getSelectionBackground();
      fg=_table.getSelectionForeground();
    }
    */

    if(_value instanceof String)
    {
      t=(String)_value;
    }
    else
    if(_value instanceof Number)
    {
      a=RIGHT;
      if(((Number)_value).doubleValue()<0)
        fg=blueify(fg);
      if(numberFormat_!=null)
	t=numberFormat_.format(((Number)_value).doubleValue());
      else
        t=_value.toString();
    }
    else
    if(_value instanceof Date)
    {
      if(dateFormat_!=null)
	t=dateFormat_.format((Date)_value);
      else
        t=_value.toString();
    }
    else
    if(_value instanceof Boolean)
    {
      t="";
      if(Boolean.TRUE.equals(_value))
	i=UIManager.getIcon("CheckBox.selectedIcon");
      else
	i=UIManager.getIcon("CheckBox.unselectedIcon");

      if(i==null)
      {
	if(isOpaque())
	{
	  if(selected)
	    bg=((Boolean)_value).booleanValue()
	      ? BuLib.getColor(new Color(128,255,128))
	      : BuLib.getColor(new Color(255,128,128));
	  else
	    bg=((Boolean)_value).booleanValue()
	      ? BuLib.getColor(new Color(192,255,192))
	      : BuLib.getColor(new Color(255,192,192));
	}
	else t=""+_value;
      }
    }
    else
    if(_value instanceof BuIcon)
    {
      i=BuResource.resizeIcon((BuIcon)_value,mh);
    }
    else
    if(_value instanceof ImageIcon)
    {
      ImageIcon ii=(ImageIcon)_value;
      int hx=ii.getIconHeight();
      int wi=hx*ii.getIconWidth()/Math.max(1,hx);
      int hi=Math.min(mh,hx);
      i=new BuIcon(ii.getImage().getScaledInstance
                   (wi,hi,Image.SCALE_SMOOTH));
    }
    else
    if(_value instanceof Icon)
    {
      Icon ii=(Icon)_value;
      int  wx=ii.getIconWidth ();
      int  hx=ii.getIconHeight();
      if(hx<=mh) i=(Icon)_value;
      else       t="Icon("+wx+","+hx+")";
    }
    else
    if(_value instanceof Dimension)
    {
      Dimension dd=(Dimension)_value;
      t=dd.width+"x"+dd.height;
    }
    else
    if(_value instanceof Point)
    {
      Point pp=(Point)_value;
      t=pp.x+","+pp.y;
    }
    else
    if(_value instanceof Rectangle)
    {
      Rectangle rr=(Rectangle)_value;
      t="("+rr.x+","+rr.y+","+rr.width+"x"+rr.height+")";
    }
    else
    if(_value instanceof Insets)
    {
      Insets ii=(Insets)_value;
      t="("+ii.top+","+ii.left+","+ii.bottom+","+ii.right+")";
    }
    else
    if(_value==null)
    {
      t="";
    }
    else
    if(_value.getClass().isArray())
    {
      t=_value.getClass().getComponentType().getName();
      t=t.substring(t.lastIndexOf('.')+1);
      t=t.substring(t.lastIndexOf('$')+1);
      t=Array.getLength(_value)+" "+t;
    }
    else
    {
      t=_value.toString();

      int ia=t.indexOf('@');
      if(ia>=0)
      {
	t=t.substring(0,ia);
	t=t.substring(t.lastIndexOf('.')+1);
	t=t.substring(t.lastIndexOf('$')+1);
      }
    }
    
    /*
    if(_focus)
    {
      bd=UIManager.getBorder("Table.focusCellHighlightBorder");
      if(_table.isCellEditable(_row,_column))
      {
	fg=UIManager.getColor("Table.focusCellForeground");
	bg=UIManager.getColor("Table.focusCellBackground");
      }
    }
    */

    /*
    if((darkerOddLines_  &&((_row   %2)==1))||
       (darkerOddColumns_&&((_column%2)==1)))
      bg=darken(bg);
    */

    //setOpaque(true);
    //setBorder(bd);
    r.setFont(ft);
    r.setForeground(fg);
    r.setBackground(bg);
    r.setHorizontalAlignment(a);
    r.setText(t);
    r.setIcon(i);
    return r;
  }

 /* private static final Color darken(Color _c)
  {
    int r=_c.getRed();
    int g=_c.getGreen();
    int b=_c.getBlue();

    r=r*19/20;
    g=g*19/20;
    b=b*19/20;

    Color c=(_c instanceof ColorUIResource)
      ? new ColorUIResource(r,g,b)
      : new Color(r,g,b);

    return c;
  }*/

  private static final Color blueify(Color _c)
  {
    int r=_c.getRed();
    int g=_c.getGreen();
    int b=_c.getBlue();

    b+=(255-b)/2;

    Color c=(_c instanceof ColorUIResource)
      ? new ColorUIResource(r,g,b)
      : new Color(r,g,b);

    return c;
  }
}

