/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuSplit2Pane.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.border.LineBorder;

/**
 * A split panel into 2 (resizable by the user).
 * Because the JSplitPane does not work fine.
 */

public class BuSplit2Pane
       extends BuSplit3Pane
{
  public BuSplit2Pane()
  {
    this(null,null,HORIZONTAL);
  }

  public BuSplit2Pane(int _orientation)
  {
    this(null,null,_orientation);
  }

  public BuSplit2Pane(JComponent _left,JComponent _right)
  {
    this(_left,_right,HORIZONTAL);
  }

  public BuSplit2Pane(JComponent _left,JComponent _right, int _orientation)
  {
    super(_left,_right,null,_orientation);
  }

  public static void main(String[] _args)
  {
    JFrame f=new JFrame("test BuSplit2Pane");

    JButton c1=new JButton("1");
    JButton c2=new JButton("2");

    c1.setBorder(new LineBorder(Color.black,2));
    c2.setBorder(new LineBorder(Color.black,2));

    BuSplit2Pane sp=new BuSplit2Pane(c1,c2);
    sp.setBorder(new LineBorder(Color.red,2));

    f.getContentPane().add("Center",sp);
    f.setSize(300,200);
    f.setVisible(true);
    f.setLocation(200,100);
  }
}
