/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuTableVectorModel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 * A simple model using a vector of Object[].
 * @see javax.swing.table.AbstractTableModel
 */
public class BuTableVectorModel
  extends AbstractTableModel
{
  protected Vector     data_;
  protected Object[]   names_;
  protected boolean    editable_;
  protected int        rowcount_;

  public BuTableVectorModel(Vector _data, Object[] _names)
  {
    this(_data,_names,false);
  }

  public BuTableVectorModel(Vector _data, Object[] _names,
			    boolean _editable)
  {
    data_    =_data;
    names_   =_names;
    editable_=_editable;
    rowcount_=-1;
  }

  @Override
  public String getColumnName(int _col)
  {
    return names_[_col].toString();
  }

  @Override
  public int getRowCount()
  {
    return data_.size();
  }

  @Override
  public int getColumnCount()
  {
    return names_.length;
  }

  @Override
  public Object getValueAt(int _row, int _col)
  {
    return ((Object[])data_.elementAt(_row))[_col];
  }

  @Override
  public boolean isCellEditable(int _row, int _col)
  {
    return editable_;
  }

  @Override
  public void setValueAt(Object _value, int _row, int _col)
  {
    ((Object[])data_.elementAt(_row))[_col]=_value;
    fireTableCellUpdated(_row,_col);
  }

  public void addRow(Object[] _row)
  {
    int l=data_.size();
    data_.addElement(_row);
    fireTableRowsInserted(l,l);
  }

  public void addRows(Object[][] _rows)
  {
    int l=data_.size();
    int n=_rows.length;
    if(n>0)
    {
      for(int i=0;i<n;i++)
        data_.addElement(_rows[i]);
      fireTableRowsInserted(l,l+n-1);
    }
  }

  /*
  public Object[] getRow(int _i)
  {
    if((_i<0)||(_i>=data_.size())) return null;
    return data_.elementAt(_row);
  }
  */
}
