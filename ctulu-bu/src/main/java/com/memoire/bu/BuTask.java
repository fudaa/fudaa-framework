/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut       unstable
 * @file         BuTask.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * Simple extension of thread more easily viewable.
 */
public class BuTask
  extends Thread
{
  public BuTask()
  {
    this(null,"Bu task");
  }

  public BuTask(String _name)
  {
    this(null,_name);
  }

  public BuTask(ThreadGroup _group)
  {
    this(_group,null);
  }

  public BuTask(ThreadGroup _group,String _name)
  {
    super(_group==null ? Thread.currentThread().getThreadGroup() : _group,
	  null,_name);
  }

  // Estimate Time

  long delay_=-1;
  long begin_=-1;
  private long end_  =-1;

  public long getEstimateDelay() { return delay_; }
  public void setEstimateDelay(long _delay) { delay_=_delay; }

  public long getEstimateEnd()
  {
    if(begin_==-1) throw new RuntimeException("Task not yet started");
    return begin_+delay_;
  }

  public long getEstimateTime()
  {
    // if(begin_==-1) throw new RuntimeException("Task not yet started");
    return Math.max(0,getEstimateEnd()-System.currentTimeMillis());
  }

  public long getRealTime()
  {
    if(begin_==-1) throw new RuntimeException("Task not yet started");
    if(end_  ==-1) throw new RuntimeException("Task not yet ended");
    return end_-begin_;
  }

  private Thread thread_;
  long   bip_   =500;

  // Thread

  @Override
  public void start()
  {
    BuTaskView tv=getTaskView();
    if(tv!=null) tv.addTask(this);

    if(delay_!=-1)
    {
      begin_=System.currentTimeMillis();
      end_  =-1;
      bip_  =Math.max(250L,delay_/100L);

      thread_=new Thread("Bu task estimate time")
      {
	@Override
  public void run()
	{
	  long t;
	  int  p=0,q=-1;
	  while((t=System.currentTimeMillis())<begin_+delay_)
	  {
	    p=(int)(100L*(t-begin_)/delay_);
	    if(p!=q) { setProgression(p); q=p; }
	    try { Thread.sleep(bip_); }
	    catch(InterruptedException ex) { }
	  }
	  setProgression(100);
	}
      };
      thread_.setPriority(Math.min(getPriority()+1,Thread.MAX_PRIORITY));
      thread_.start();
    }

    super.start();
  }

  @Override
  public void run()
  {
    BuTaskView tv=getTaskView();
    if(tv!=null) tv.removeTask(this);

    end_=System.currentTimeMillis();
    super.run();
  }

  // Divers

  public String toString()
  {
    String r=getName();
    int p=getProgression();
    if(p>0) r=""+p+"% "+r;
    return r;
  }

  // Propriétés

  private BuTaskView taskView_;
  public BuTaskView getTaskView() { return taskView_; }
  public void setTaskView(BuTaskView _tv) { taskView_=_tv; }

  private int progression_;
  public int getProgression() { return progression_; }
  public void setProgression(int _v)
  {
    if(_v!=progression_)
    {
      //int vp=progression_;
      progression_=_v;

      BuTaskView tv=getTaskView();
      if(tv!=null) BuUpdateGUI.repaintLater(tv);
    }
  }
}
