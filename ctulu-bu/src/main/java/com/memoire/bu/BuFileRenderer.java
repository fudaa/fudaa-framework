/***
 * @modification $Date: 2007-03-09 08:37:38 $
 * @statut unstable
 * @file BuFileRenderer.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.dt.DtDragSupport;
import com.memoire.dt.DtFileHandler;
import com.memoire.fu.*;
import com.memoire.tar.TarEntry;
import com.memoire.tar.TarFile;
import com.memoire.tar.TarInputStream;
import com.memoire.vfs.VfsFile;
import com.memoire.vfs.VfsFileFile;
import com.memoire.vfs.VfsFileUrl;
import com.memoire.xml.XmlNode;
import com.memoire.xml.XmlParser;
import com.memoire.xml.XmlReader;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class BuFileRenderer {
  private static final Dimension D200X200 = new Dimension(200, 200);
  private static final Color YELLOW = BuLib.getColor(new Color(255, 255, 224));
  private static final Color BLACK = BuLib.getColor(Color.black);
  private static final Color WHITE = BuLib.getColor(Color.white);
  private static final Color GRAY = BuLib.getColor(Color.gray);
  private static final Font ETF = BuLib.deriveFont("List", Font.PLAIN, -2);
  private static final Font MP10 = new Font("Monospaced", Font.PLAIN, 10);
  private static final Border VIEW_BORDER =
      new LineBorder(GRAY, 1);
  private static final Border ACTIONS_BORDER = VIEW_BORDER;
  private static final Border LABEL_BORDER =
      new CompoundBorder(VIEW_BORDER, BuBorders.EMPTY1212);

  protected String getString(String _s) {
    return BuResource.BU.getString(_s);
  }

  private BufferedInputStream createBufferedInputStream(VfsFile _f)
      throws IOException {
    return new BufferedInputStream
        (_f.getInputStream(), 32768);
  }

  private BufferedReader createBufferedReader(VfsFile _f)
      throws IOException {
    return new BufferedReader
        (new InputStreamReader
            (_f.getInputStream()), 32768);
  }

  private BufferedReader createBufferedReader(VfsFile _f, String _encoding)
      throws IOException {
    return new BufferedReader
        (new InputStreamReader
            (_f.getInputStream(), _encoding), 32768);
  }

  /**
   * Create a thumbnail.
   * This method does not do anything in BuFileRenderer but
   * it may be overloaded in a derivated class (aka KorteFileRenderer).
   */
  protected void createThumbnail(VfsFile _file, Image _img) {
    //nothing to do
  }

  /**
   * Get a thumbnail if one is available.
   * This method returns null in BuFileRenderer but
   * it may be overloaded in a derivated class (aka KorteFileRenderer).
   */
  protected Image getThumbnail(VfsFile _file) {
    return null;
  }

  private FuRefTable cache_ = new FuRefTable(FuRefTable.SOFT, FuRefTable.SOFT);

  protected JComponent getInCache(Object _value) {
    return (JComponent) cache_.get(_value);
  }

  protected void putInCache(Object _value, JComponent _c) {
    cache_.put(_value, _c);
  }

  public synchronized Component getPanelCellRendererComponent
      (JPanel _panel,
       Object _value,
       int _index,
       boolean _selected,
       boolean _focus) {
    if (_panel != null) {
      throw new IllegalArgumentException
          ("_panel should be always null");
    }

    JComponent c = getInCache(_value);
    VfsFile f = VfsFile.ensureVfsFile(_value);

    if (c != null) {
      long d = ((Long) c.getClientProperty("VIEW_DATE")).longValue();
      if (!f.isBuilt() || (d < f.lastModified())) {
        c = null;
      }
    }

    if (c == null) {
      c = getComponent(f);
      c.putClientProperty
          ("VIEW_DATE",
              f.isBuilt()
                  ? FuFactoryLong.get(f.lastModified())
                  : FuFactoryLong.get(System.currentTimeMillis()));
      putInCache(_value, c);
    }
    return c;
  }

  protected boolean addDndSupport() {
    return true;
  }

  protected final JComponent getComponent(VfsFile _file) //,boolean _selected)
  {
    BuPanel r = new BuPanel();

    if ((_file != null) && addDndSupport()) {
      r.putClientProperty("DT_DRAG_VALUE", _file);
      r.setTransferHandler(DtFileHandler.getInstance());
      DtDragSupport.install(r);
    }

    buildComponent(_file, r);
    return r;
  }

  final void buildComponent(VfsFile _file, BuPanel r) //,boolean _selected)
  {
    r.setOpaque(false);
    r.setLayout(new BuBorderLayout(-1, -1));

    Icon icon = getIcon(_file);

    if (icon != null) {
      int wi = icon.getIconWidth();
      int hi = icon.getIconHeight();

      if ((wi < 32) || (hi < 32)) {
        icon = new BuFixedSizeIcon
            (icon, Math.max(32, wi), Math.max(32, hi));
      }
    }

    BuLabelMultiLine lb = new BuLabelMultiLine() {
      @Override
      public void setOpaque(boolean _o) {
        if (isOpaque() && !_o) {
          try {
            throw new Exception("Opacity");
          } catch (Exception ex) {
            FuLog.warning(ex);
          }
        }
        super.setOpaque(_o);
      }

      @Override
      public Point getToolTipLocation(MouseEvent _evt) {
        return new Point(0, 0);
      }
    };

    lb.setPreferredWidth(198);
    lb.setWrapMode(BuLabelMultiLine.NONE);//WORD);
    lb.setText(getText(_file));
    lb.setFont(ETF);
    lb.setIcon(icon);
    lb.setOpaque(true);
    lb.setForeground(BLACK);
    lb.setBackground(YELLOW);
    lb.setBorder(LABEL_BORDER);
    try {
      lb.setToolTipText(_file.getAbsolutePath());
    } catch (Exception ex) {
    }

    JComponent vw;

    if ((_file == null) || _file.isBuilt()) {
      vw = getView(_file);
    } else {
      vw = getRemoteView(_file, r);
    }

    vw.setBorder(VIEW_BORDER);

    JComponent ac = getActions(_file);
    if (ac != null) {
      ac.setBorder(ACTIONS_BORDER);
    }

    r.add(lb, BuBorderLayout.NORTH);
    r.add(vw, BuBorderLayout.CENTER);
    if (ac != null) {
      r.add(ac, BuBorderLayout.EAST);
    }
    r.setPreferredSize(D200X200);
    r.setSize(D200X200);
    r.revalidate();
    //return r;
  }

  public String getExtension(VfsFile _file) {
    String r = _file.getName().toLowerCase();
    int j = r.lastIndexOf(".");
    if (j >= 0) {
      r = r.substring(j + 1);
    }
    return r;
  }

  public String getBiExtension(VfsFile _file) {
    String r = _file.getName().toLowerCase();
    int j = r.lastIndexOf(".");
    if (j >= 0) {
      j = r.substring(0, j).lastIndexOf(".");
      if (j >= 0) {
        r = r.substring(j + 1);
      }
    }
    return r;
  }

  public String getText(VfsFile _file) {
    String sn = "";
    String sd = "";

    if (!_file.isBuilt()) {
      sn = getString("taille inconnue");//unknown size
      sd = getString("date inconnue");  //unknown date
    } else {
      if (_file.isDirectory()) {
        String[] l = _file.list();
        int n = (l == null ? 0 : l.length);
        if (n <= 1) {
          sn = n + " " + getString("fichier");
        } else {
          sn = n + " " + getString("fichiers");
        }
      } else {
        long n = _file.length();
        if (n < 0) {
          sn = "";
        } else if (n <= 1) {
          sn = n + " " + getString("octet");
        } else {
          sn = n + " " + getString("octets");
        }
      }

      long d = _file.lastModified();
      if (d > 0) {
        sd = DateFormat.getDateTimeInstance().format
            (new Date(d));
      }
    }

    return _file.getName() + "\n" + sd + "\n" + sn +
        " (" + (_file.canRead() ? "r" : "-") +
        (_file.canWrite() ? "w" : "-") + ")";
  }

  public Icon getIcon(VfsFile _file) {
    Icon r = null;
    //if(!_file.isBuilt())
    //r=null;
    //else
    if (_file.isDirectory()) {
      r = UIManager.getIcon("FileView.directoryIcon");
    } else {
      r = UIManager.getIcon("FileView.fileIcon");
    }
    return r;
  }

  public JComponent getActions(VfsFile _file) {
    return null;
  }

  public JComponent getView(VfsFile _file) {
    JComponent r = null;

    String ext = getExtension(_file);
    String biext = getBiExtension(_file);

    try {
      if (!_file.canRead()) {
        r = getStringView(getString("Fichier non lisible"));//Not Readable File
      } else if (_file.isDirectory()) {
        r = getDirectoryView(_file);
      } else if ("eml".equals(ext)) {
        r = getEmlView(_file);
      } else if ("html".equals(ext)
          || "shtml".equals(ext)
          || "htm".equals(ext)) {
        r = getHtmlView(_file);
      } else if ("xml".equals(ext)) {
        r = getXmlView(_file);
      } else if ("rss".equals(ext)) {
        r = getRssView(_file);
      } else if ("rtf".equals(ext)) {
        r = getRtfView(_file);
      } else if ("gif".equals(ext)
          || "jpg".equals(ext) || "jpeg".equals(ext)
          || "png".equals(ext)
          || "bmp".equals(ext)
          || "xcf".equals(ext)
          || "xpm".equals(ext)) {
        if ((_file.length() >= 0) && (_file.length() < 32768L)) {
          try {
            r = getImageView(_file);
          } catch (Exception ex) {
            FuLog.warning(ex);
          }
        }

        if (r == null) {
          r = getPamView(_file);
        }

        if (r == null) {
          if ((_file.length() >= 0) && (_file.length() < 32768L)) {
            r = getStringView(getString("Format non lisible"));//Can not read this format
          } else {
            r = getBigImageView(_file);
          }
        }
      } else if ("ics".equals(ext) || "vcs".equals(ext)) {
        r = getIcsView(_file);
      } else if ("java".equals(ext)) {
        r = getJavaView(_file);
      }
      /* else
      if("mid".equals(ext)||"midi".equals(ext))
      {
        r=getMidiView(_file);
      }
      else
           if("mp3".equals(ext))
      {
	r=getMp3View(_file);
      }
        else
     if("wav".equals(ext))
      {
	r=getWavView(_file);
      }
      else
      if("m3u".equals(ext))
      {
	r=getM3uView(_file);
      }*/
      else if ("pls".equals(ext)) {
        r = getPlsView(_file);
      } else if ("ser".equals(ext)) {
        r = getSerView(_file);
      } else if ("sxw".equals(ext) || "stw".equals(ext)) {
        r = getOasisView(_file);
      } else if ("tar".equals(ext) || "tgz".equals(ext) ||
          "tar.gz".equals(biext)) {
        r = getTarView(_file);
      } else if ("vcf".equals(ext) || "vct".equals(ext)) {
        r = getVcfView(_file);
      } else if ("zip".equals(ext) ||
          "ear".equals(ext) || "jar".equals(ext) || "war".equals(ext) ||
          "cbz".equals(ext)) {
        r = getZipView(_file);
      } else {
        try {
          String p = _file.getAbsolutePath();
          //p=FuLib.replace(p,"'","'\\''");

          if ("doc".equals(ext) && !"txt.doc".equals(biext)) {
            FuLog.trace("antiword -t '" + p + "'");
            String s = FuLib.runProgram(new String[]{"antiword", "-t", p});
            s = FuLib.replace(s, "\n\n", "\n");
            s = FuLib.replace(s, "\014", "");
            r = getStringView(s);
          } else if ("iso".equals(ext)) {
            String[] s = FuLib.split
                (FuLib.runProgram
                    (new String[]{"isoinfo", "-d", "-i", p}), '\n');
            String format = "";
            String system = "";
            String volume = "";
            int blksz = 0;
            int volsz = 0;
            for (int i = 0; i < s.length; i++) {
              if (s[i].startsWith("CD-ROM is in ")) {
                s[i] = FuLib.replace(s[i], "CD-ROM is in ", "");
                s[i] = FuLib.replace(s[i], " format", "");
                format = s[i].toLowerCase();
              } else if (s[i].startsWith("System id: ")) {
                system = s[i].substring(11).toLowerCase();
              } else if (s[i].startsWith("Volume id: ")) {
                volume = s[i].substring(11);
              } else if (s[i].startsWith("Logical block size is: ")) {
                try {
                  blksz = Integer.parseInt(s[i].substring(23));
                } catch (NumberFormatException ex) {
                }
              } else if (s[i].startsWith("Volume size is: ")) {
                try {
                  volsz = Integer.parseInt(s[i].substring(16));
                } catch (NumberFormatException ex) {
                }
              }

              s[i] = null;
              //if(s[i].startsWith("Application id:")) s[i]="";
            }
            StringBuffer b = new StringBuffer(1024);
            b.append(volume);
            b.append('\n');
            b.append("CD-ROM image\n");
            b.append(format);
            b.append(' ');
            b.append('(');
            b.append(system);
            b.append(')');
            int t = blksz * volsz;
            if (t != 0) {
              b.append('\n');
              b.append(t);
              b.append(' ');
              b.append(getString("octets"));
              b.append('\n');
              b.append(FuLib.leftpad("" + volsz, ("" + t).length(), ' '));
              b.append(' ');
              b.append(getString("blocs"));
            }
            r = getStringView(b.toString());
          } else if ("mpg".equals(ext)) {
            String[] s = FuLib.split
                (FuLib.runProgram(new String[]{"mpgtx", "-i", p}), '\n');
            s[1] = getString("Type:     ") + FuLib.replace(s[1].trim(), " System File", "");
            s[2] = FuLib.replace
                (s[2].trim(), "Muxrate :", getString("D�bit:   "));
            s[3] = FuLib.replace
                (s[3].trim(), "Estimated Duration:", getString("Dur�e:   "));
            s[4] = FuLib.replace
                (s[4].trim(), "Size [", getString("Taille:   "));
            s[4] = s[4].substring(0, s[4].indexOf(']'));
            s[4] = FuLib.replace(s[4], " x ", "x");
            StringBuffer b = new StringBuffer(1024);
            b.append(s[1]);
            b.append('\n');
            b.append(s[2]);
            b.append('\n');
            b.append(s[3]);
            b.append('\n');
            b.append(s[4]);
            r = getStringView(b.toString());
          } else if ("ogg".equals(ext)) {
            String s = FuLib.runProgram(new String[]{"ogginfo", p});
            s = s.substring(s.indexOf("Channels:"));
            s = s.substring(0, s.indexOf("Logical stream"));
            s = FuLib.replace(s, "User comments section follows...", "");
            s = FuLib.replace(s, ",000000 ", " ");
            s = FuLib.replace(s, "=", ": ");
            s = FuLib.replace(s, "\t", "  ");
            s = FuLib.replace(s, "\n\n", "\n");
            s = FuLib.replace(s, "\n  ", "\n");
            r = getStringView(s);
          } else if ("o.gz".equals(biext)) // maybe a linux module
          {
            String[] s = FuLib.split
                (FuLib.replace
                    (FuLib.runProgram(new String[]{"/sbin/modinfo", p}),
                        "\"", ""), '\n');
            for (int i = 0; i < s.length; i++) {
              if (s[i].startsWith("description: ")) {
                s[i] = s[i].substring(13) + "\n ";
                if (s[i].trim().equals("<none>")) {
                  s[i] = "";
                }
              } else if (s[i].startsWith("author:      ")) {
                s[i] = s[i].substring(13);
                s[i] = FuLib.replace(s[i], ", ", "\n");
                s[i] = FuLib.replace(s[i], "<", "\n ");
                s[i] = FuLib.replace(s[i], ">", "");
                if (s[i].trim().equals("none")) {
                  s[i] = "";
                }
              } else if (s[i].startsWith("license:     ")) {
                s[i] = FuLib.replace
                    (s[i], "license:     ", " \n" + getString("Licence: "));
              } else {
                s[i] = "";
              }
            }
            r = getStringView
                (FuLib.replace(FuLib.join(s, '\n').trim(), "\n \n\n \n", "\n \n"),
                    BuLabelMultiLine.WORD);
          } else if ("pdf".equals(ext)) {
            try {
              String s = FuLib.runProgram(new String[]
                  {"pdftotext", "-enc", "Latin1", "-eol", "unix",
                      "-f", "1", "-l", "2", p, "-"});
              s = FuLib.replace(s, "\n\n", "\n");
              s = FuLib.replace(s, "\014", "");
              r = getStringView(s);
            } catch (Exception ex) {
              r = getStringView
                  (FuLib.runProgram(new String[]{"pdfinfo", p}));
            }
          } else if ("ps".equals(ext)) {
            try {
              String s = FuLib.runProgram(new String[]
                  {"ps2ascii", p});
              s = FuLib.replace(s, "\n\n", "\n");
              r = getStringView(s);
            } catch (Exception ex) {
              r = getStringView
                  (FuLib.runProgram(new String[]{"pdfinfo", p}));
            }
          } else if ("rpm".equals(ext)) {
            String s = FuLib.runProgram(new String[]
                {"rpm", "--queryformat",
                    "%{SUMMARY}\\\\" +
                        "%{URL}\\\\ \\\\" +
                        "Size:  %{SIZE} bytes\\\\" +
                        "Group: %{GROUP}\\\\" +
                        " \\\\%{DESCRIPTION}",
                    "-qp", p});
            s = s.replace('\n', ' ').replace('\\', '\n');
            r = getStringView(s, BuLabelMultiLine.LETTER);
          }
          /*
	  else
	  if("tar".equals(ext))
	    r=getStringView
	      (FuLib.cut
	       (FuLib.runProgram(new String[] { "tar","tvf",p}),
		51,0,80,22));
	  else
	  if("tar.gz".equals(biext)||"tgz".equals(ext))
	    r=getStringView
	      (FuLib.cut
	       (FuLib.runProgram(new String[] { "tar","tvzf",p}),
		51,0,80,22));
          */
          else if ("tar.bz2".equals(biext) ||
              "tbz".equals(ext)) {
            r = getStringView
                (FuLib.cut
                    (FuLib.runProgram(new String[]{"tar", "tvjf", p}),
                        51, 0, 80, 22));
          } else if ("rar".equals(ext) || "cbr".equals(ext)) {
            FuLog.trace("unrar l '" + p + "'");
            String s = FuLib.runProgram(new String[]{"unrar", "l", p});
            s = s.substring(s.indexOf("----\n") + 5);
            s = s.substring(0, s.lastIndexOf("\n----"));
            r = getStringView(s);
          } else if ("ace".equals(ext)) {
            String s = FuLib.runProgram(new String[]{"unace", "l", p});
            s = s.substring(s.indexOf("File") + 6);
            s = s.substring(0, s.lastIndexOf("listed:") - 1);
            String[] t = FuLib.split(s, '\n', false, true);
            for (int i = 0; i < t.length; i++) {
              t[i] = t[i].substring(Math.min(44, t[i].length())).trim();
              t[i] = t[i].substring(t[i].lastIndexOf('/'));
            }
            s = FuLib.join(t, '\n');
            r = getStringView(s);
          }
        } catch (Exception ex) {
          r = null;
        }

        if (r == null) {
          try {
            String p = _file.getAbsolutePath();

            if (FuLib.contains(BuFileExtensions.MOVIES, ext)) {
              String s = FuLib.runProgram
                  (new String[]{"mplayer", "-identify", p});
              s = s.substring(s.indexOf("ID_VIDEO_FORMAT"));
              s = FuLib.replace(s, "ID_", "");
              int j = s.indexOf("Exiting...");
              if (j >= 0) {
                s = s.substring(0, j);
              }
              String[] t = FuLib.split(s, '\n', false, true);
              for (int i = 0; i < t.length; i++) {
                j = t[i].indexOf('=');
                if (j >= 0) {
                  String k = t[i].substring(0, j).toLowerCase().replace('_', ' ');
                  String v = t[i].substring(j + 1);
                  t[i] = FuLib.leftpad(k, 13, ' ') + ": " + v;
                }
              }
              s = FuLib.join(t, '\n');
              r = getStringView(s);
            } else if (FuLib.contains(BuFileExtensions.SOUNDS, ext)) {
              r = getStringView
                  (FuLib.runProgram(new String[]{"sox", p, "-e", "stat"}));
            }
          } catch (Exception ex) {
            r = null;
          }
        }

        if (r == null) {
          r = getTextView(_file);
        }
      }
    } catch (Exception ex) {
      r = getStringView(getString("Fichier non lisible"));
    }

    return r;
  }

  public JComponent getStringView(String _text) {
    return getStringView(_text, BuLabelMultiLine.NONE);
  }

  public JComponent getStringView(String _text, int _mode) {
    BuLabelMultiLine r = new BuLabelMultiLine();
    r.setWrapMode(_mode);
    r.setOpaque(true);
    r.setBackground(WHITE);
    r.setForeground(BLACK);

    //BuTextArea r=new BuTextArea(9,24);//8,24);
    //r.setEditable(false);
    //r.setBorder(BuBorders.EMPTY0200);
    r.setFont(MP10);
    r.setText(_text);
    return r;
  }

  public JComponent getDirectoryView(VfsFile _file) throws Exception {
    String s = "";
    String[] f = _file.list();

    if (f != null) {
      FuSort.sort(f);

      int l = f.length;
      int n = 0;

      for (int i = 0; i < l; i++) {
        s += f[i] + "\n";
        n++;
        if (n >= 22) {
          break;
        }
      }

      /*
	s+=l+" ";
	if(l>1) s+="entr�es";
	else    s+="entr�e";
	s+="\n\n";
      */
    }

    return getStringView(s);
  }

  public JComponent getEmlView(VfsFile _file) throws Exception {
    StringBuffer sb = new StringBuffer(2000);
    int n = 0;
    BufferedReader br = createBufferedReader(_file);

    String from_name = "";
    String from_email = "";
    String date = "(" + getString("sans date") + ")";
    String subject = "(" + getString("sans sujet") + ")";
    boolean content = false;
    boolean plain = false;

    while (n < 22) {
      String s = br.readLine();
      if (s == null) {
        break;
      }

      if (s.startsWith("From:")) {
        s = s.substring(s.indexOf(":") + 1).trim();
        if (s.startsWith("\"")) {
          int i = s.lastIndexOf('"');
          from_name = s.substring(1, i).trim();
          from_email = s.substring(i + 1).trim();
        } else {
          from_name = "";
          from_email = s.trim();
        }
        if (from_email.startsWith("<") && from_email.endsWith(">")) {
          from_email = from_email.substring(1, from_email.length() - 1);
        }
      } else if (s.startsWith("Date:")) {
        date = s.substring(s.indexOf(":") + 1).trim();
      } else if (s.startsWith("Subject:")) {
        subject = s.substring(s.indexOf(":") + 1).trim();
      } else if (s.startsWith("Content-type:") || s.startsWith("Content-Type:")) {
        if (s.substring(s.indexOf(":") + 1).trim().startsWith("text/plain")) {
          plain = true;
        }
      } else if ("".equals(s) && !content) {
        content = true;
        sb.append(from_name);
        sb.append('\n');
        sb.append(from_email);
        sb.append('\n');
        sb.append(date);
        sb.append('\n');
        sb.append(subject);
        n += 4;
        if (!plain) {
          break;
        }
        sb.append("\n \n");
        n++;
      } else {
        if (content) {
          if (s.equals(".")) {
            break;
          }
          if (s.equals("")) {
            sb.append('\n');
          } else {
            sb.append(s);
            sb.append(' ');
          }
          n++;
        }
      }
    }

    br.close();

    return getStringView(sb.toString(), BuLabelMultiLine.LETTER);
  }

  public JComponent getHtmlView(VfsFile _file) throws Exception {
    //return getTextView(_file);

    /*
    try
    {
      //r.setPage(FuLib.createURL(_file.getPath()));

      BuEditorPane r=new BuEditorPane();//8,24);
      r.setFont(new Font("Courier",Font.PLAIN,8));
      r.setEditable(false);

      HTMLEditorKit ek=new HTMLEditorKit();
      //r.getEditorKitForContentType("text/html");
      HTMLDocument  dk=new HTMLDocument();
      //ek.createDefaultDocument();
      dk.setBase(FuLib.createURL(_file.getPath()));
      ek.read(new FileReader(_file),dk,0);
      r.setEditorKit(ek);
      r.setDocument(dk);
      return r;
    }
    catch(Exception ex)
    {
      return getTextView(_file);
    }
    */

    String s = "";
    char[] tc = new char[6000];
    BufferedReader br = createBufferedReader(_file);

    int lc = 0;
    while (lc < 6000) {
      int cc = br.read();
      if (cc == -1) {
        break;
      }
      tc[lc] = (char) cc;
      lc++;
    }

    br.close();
    s = new String(tc, 0, lc);
    s = FuHtml.htmlToAscii(s);

    /*
    RE      regexp;
    REMatch match;

    try
    {

    s=FuLib.remove(s,"<!--","-->");
    s=FuLib.remove(s,"<!"  ,">"  );
    s=FuLib.removeIgnoreCase(s,"<style","</style>");
    s=FuLib.removeIgnoreCase(s,"<script","</script>");

    regexp=new RE("<br>",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s,"\n");
    regexp=new RE("</h?>",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s,"\n");

    s=FuLib.replace(s,"\t"," ");
    s=FuLib.replaceIgnoreCase(s,"</tr>","\n");

    regexp=new RE("<[^>]*>",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s," ");

    s=FuLib.replace(s,"&quot;","\"");
    s=FuLib.replace(s,"&gt;"  ,">" );
    s=FuLib.replace(s,"&lt;"  ,"<" );
    s=FuLib.replace(s,"&amp;" ,"&" );
    s=FuLib.replace(s,"&nbsp;"," " );
    s=FuLib.replace(s,"&#183;"," "+(char)183);

    regexp=new RE("&([a-zA-Z])[^&;]{1,6};",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s,"$1");

    regexp=new RE("   *",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s," ");
    regexp=new RE(" *\n *",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s,"\n");
    regexp=new RE("\n\n\n*",
		  RE.REG_ICASE|RE.REG_MULTILINE|RE.REG_DOT_NEWLINE,
		  RESyntax.RE_SYNTAX_PERL5);
    if(regexp!=null) s=regexp.substituteAll(s,"\n");
    }
    catch(Exception ex)
    {
      FuLog.error(ex);
    }
    */

    //FuLog.debug(FuLib.replace(s," ","�"));
    return getStringView(s, BuLabelMultiLine.WORD);
  }

  public JComponent getIcsView(VfsFile _file) throws Exception {
    StringBuffer sb = new StringBuffer(2000);
    int n = 0;
    BufferedReader br = createBufferedReader(_file, "UTF-8");
    DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

    long start = 0L;
    long end = 0L;
    String summary = null;

    while (n < 22) {
      String s = br.readLine();
      if (s == null) {
        break;
      }
      if (s.startsWith("DTSTART")) {
        if (s.endsWith("Z")) {
          s = s.substring(0, s.length() - 1);
        }
        start = df.parse(s.substring(s.indexOf(":") + 1)).getTime();
      } else if (s.startsWith("DTEND")) {
        if (s.endsWith("Z")) {
          s = s.substring(0, s.length() - 1);
        }
        end = df.parse(s.substring(s.indexOf(":") + 1)).getTime();
      } else if (s.startsWith("SUMMARY")) {
        summary = s.substring(s.indexOf(":") + 1);
        if (s.indexOf("QUOTED-PRINTABLE") >= 0) {
          summary = FuLib.decodeQuotedPrintable(summary);
        }
        summary = FuLib.replace(summary, "\n", "\n  ");
        summary = FuLib.replace(summary, "\r", "");
        summary = summary.trim();
      } else if (s.startsWith("END:") && s.toUpperCase().equals("END:VEVENT")) {
        sb.append(new Date(start));
        sb.append("\n  ");
        sb.append(summary);
        if (end > start) {
          sb.append(" (");
          sb.append(FuLib.duration(end - start, true));
          sb.append(')');
        }
        sb.append('\n');
        n += 2;
        start = 0L;
        end = 0L;
        summary = null;
      }
    }

    br.close();

    return getStringView(sb.toString());
  }

  public final JComponent getImageView(VfsFile _file) throws Exception {
    return getImageView(_file, false);
  }

  protected JComponent getImageView(VfsFile _file, boolean _thumbnail)
      throws Exception {
    BuPicture r = null;

    if ("bmp".equals(getExtension(_file))) {
      try {
        r = new BuPicture
            (new BuBmpLoader().getBmpImage
                (createBufferedInputStream(_file)));
      } catch (Exception ex) {
        r = null;
      }
    }

    if (r == null) {
      //FuLog.debug("getImageView "+_file);
      r = new BuPicture(_file.toURL());
    }

    if (r.getIcon().isDefault()) {
      return null;
    }

    r.setMode(BuPicture.REDUCE);
    r.setOpaque(true);
    r.setBackground(GRAY);

    if (_thumbnail) {
      createThumbnail(_file, r.getImage());
    }

    return r;
  }

  public JComponent getBigImageView(final VfsFile _file) throws Exception {
    final BuPanel r = new BuPanel(new BuBorderLayout());
    final BuLabelMultiLine l = createLabelMultiLine
        (getString("Grande image sans vignette. Cliquer pour la voir."),
            BuResource.BU.loadToolCommandIcon("VOIR"));

    l.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent _evt) {
        try {
          l.setText(getString("Ouverture..."));
          l.setIcon(UIManager.getIcon("FileView.hardDriveIcon"));
          l.paintImmediately(0, 0, l.getWidth(), l.getHeight());
          r.add(getImageView(_file, true));
          r.remove(l);
        } catch (Exception ex) {
          l.setText(getString("Impossible d'afficher cette image"));
          //Failed to view\nthis picture
          l.setIcon(BuResource.BU.loadToolCommandIcon("CACHER"));
        }
        r.revalidate();
      }
    });

    r.add(l);
    r.setOpaque(true);
    r.setBackground(GRAY);
    return r;
  }

  public JComponent getPamView(VfsFile _file) throws Exception {

    Image img = null;

    try {
      img = getThumbnail(_file);

      // very very dirty ;-)
      if (img != null) {
        throw new Exception("ok");
      }

      if (_file instanceof VfsFileUrl) {
        return null;
      }

      VfsFile fth = _file.getParentVfsFile();
      if (fth != null) {
        fth = fth.createChild(".thumbnails")
            .createChild(_file.getName() + ".png");

        if (
            fth.exists() &&
                fth.canRead() &&
                (fth.lastModified() >= _file.lastModified())) {
          try {
            img = BuLib.HELPER.getToolkit().getImage(fth.toURL());
          } catch (Exception ex) {
          }
        }

        // very very dirty ;-)
        if (img != null) {
          throw new Exception("ok");
        }
      }

      VfsFile f = null;

      VfsFile fee = VfsFile.createFile
          (FuLib.getUserHome() + File.separator + ".ee" +
              File.separator + "icons" +
              _file.getAbsolutePath());

      if (fee.exists()
          && (fee.lastModified() >= _file.lastModified())) {
        f = fee;
      }

      if (f == null) {
        VfsFile fxv = _file.getParentVfsFile();
        if (fxv != null) {
          fxv = fxv
              .createChild(".xvpics")
              .createChild(_file.getName());
        }
        //FuLog.debug("BFR: (XV) "+fxv);

        if (fxv != null && fxv.exists()
            && (fxv.lastModified() >= _file.lastModified())) {
          f = fxv;
        }
      }

      if (f == null) {
        VfsFile fem = VfsFile.createFile
            (FuLib.getUserHome() + File.separator + ".ee" +
                File.separator + "minis" +
                _file.getAbsolutePath());

        if (fem.exists()
            && (fem.lastModified() >= _file.lastModified())) {
          f = fem;
        }
      }

      if (f != null) //.exists()&&(f.lastModified()>=_file.lastModified()))
      {
        try (LineNumberReader in = new LineNumberReader(new InputStreamReader(f.getInputStream()), 32768)) {
          String l = in.readLine();
          int t = 0;
          int w = 0;
          int h = 0;

          if ("P7 332".equals(l)) {
            while ((l = in.readLine()).startsWith("#")) ;
            int p = l.indexOf(' ');
            w = Integer.parseInt(l.substring(0, p));
            h = Integer.parseInt(l.substring(p + 1, l.indexOf(' ', p + 1)));
            //FuLog.debug("P7 "+w+"x"+h);
            t = 7;
          }

          if ("P6".equals(l)) {
            l = in.readLine();
            int p = l.indexOf(' ');
            w = Integer.parseInt(l.substring(0, p));
            h = Integer.parseInt(l.substring(p + 1).trim());
            in.readLine();
            //FuLog.debug("P6 "+w+"x"+h);
            t = 6;
          }

          if (t > 0) {
            img = BuLib.HELPER.createImage(w, h);

            Graphics g = img.getGraphics();
            for (int j = 0; j < h; j++)
              for (int i = 0; i < w; i++) {
                switch (t) {
                  case 7: {
                    int v = in.read();
                    g.setColor(new Color((int) (36.42 * ((v >> 5) & 7)),
                        (int) (36.42 * ((v >> 2) & 7)),
                        (int) (85.00 * (v & 3))));
                  }
                  break;
                  case 6: {
                    int cr = in.read();
                    int cg = in.read();
                    int cb = in.read();
                    g.setColor(new Color(cr, cg, cb));
                  }
                  break;
                  default:
                    g.setColor(Color.black);
                }

                g.drawLine(i, j, i, j);
              }
          }
        }
      }
    } catch (Exception ex) {
      if (!"ok".equals(ex.getMessage())) {
        FuLog.error(ex);
        img = null;
      }
    }

    BuPicture r = null;

    if (img != null) {
      r = new BuPicture(img);
      r.setMode(BuPicture.REDUCE);
      r.setOpaque(true);
      r.setBackground(GRAY);
    }

    return r;
  }

  // TODO: ugly, should be rewritten
  public JComponent getJavaView(VfsFile _file) throws Exception {
    char[] tc = new char[2000];
    BufferedReader br = createBufferedReader(_file);

    int lc = 0;
    while (lc < 2000) {
      int cc = br.read();
      if (cc == -1) {
        break;
      }
      tc[lc] = (char) cc;
      lc++;
    }

    br.close();

    for (int k = lc - 1; k > 0; k--)
      if (tc[k - 1] == '\r') {
        if (tc[k] == '\n') {
          tc[k - 1] = ' ';
        } else {
          tc[k - 1] = '\n';
        }
      }

    String u = new String(tc, 0, lc);

    String t = u;
    t = t.replace('\t', ' ');
    t = FuLib.remove(t, "/*", "*/");
    t = FuLib.remove(t, "//", "\n");
    t = FuLib.replace(t, "package ", "\npackage ");
    t = FuLib.replace(t, "import ", "\nimport ");
    t = FuLib.replace(t, "extends ", "\nextends ");
    t = FuLib.replace(t, "implements ", "\nimplements ");
    t = FuLib.replace(t, "static", "");
    t = FuLib.replace(t, "final", "");
    t = FuLib.replace(t, "abstract", "");
    t = FuLib.replace(t, "private", "");
    t = FuLib.replace(t, "protected", "");
    t = FuLib.replace(t, "public", "");
    t = FuLib.replace(t, ";", "");
    t = FuLib.replace(t, ", ", ",");
    t = FuLib.replace(t, "{", " ");
    t = FuLib.replace(t, "}", " ");
    String[] s = FuLib.split(t, '\n', false, true);
    StringBuffer b = new StringBuffer(1000);

    for (int i = 0; i < s.length; i++) {
      s[i] = s[i].trim();
      if (s[i].startsWith("package") ||
          s[i].startsWith("class") ||
          s[i].startsWith("interface") ||
          s[i].startsWith("extends") ||
          s[i].startsWith("implements")) {
        b.append(s[i].trim());
        b.append('\n');
      }
    }

    b.append(" \n");

    Hashtable h = new Hashtable();
    for (int i = 0; i < s.length; i++)
      if (s[i].startsWith("import")) {
        t = s[i];
        int p = t.lastIndexOf('.');
        if (p >= 0) {
          t = t.substring(0, p);
        }
        t = t.trim();
        if (h.get(t) == null) {
          b.append(t);
          b.append('\n');
          h.put(t, t);
        }
      }

    t = b.toString();
    t = FuLib.replace(t, "  ", " ");
    t = FuLib.replace(t, "  ", " ");
    t = FuLib.replace(t, "  ", " ");
    t = FuLib.replace(t, "  ", " ");
    t = FuLib.replace(t, "package ", "");
    t = FuLib.replace(t, "import ", "< ");
    t = FuLib.replace(t, "\nclass ", "\n \n+ ");
    t = FuLib.replace(t, "\ninterface ", "\n \n- ");
    t = FuLib.replace(t, "\nextends ", "\n  = ");
    t = FuLib.replace(t, "\nimplements ", "\n  - ");
    t = FuLib.replace(t, ",\n", ",...\n");
    return getStringView(t.trim());
  }

  /*public JComponent getM3uView(VfsFile _file) throws Exception
  {
    int            n=0;
    String         s="";
    BufferedReader r=createBufferedReader(_file);

    while(true)
    {
      String l=r.readLine();
      if(l==null) break;
      if(l.startsWith("#")) continue;
      n++;
      if(n<22)
      {
	int i=l.lastIndexOf('/');
	l=l.substring(i+1).trim();
	if(l.endsWith(".mp3")) l=l.substring(0,l.length()-4);
	if(l.endsWith(".ogg")) l=l.substring(0,l.length()-4);
	if(l.endsWith(".wav")) l=l.substring(0,l.length()-4);
	l=l.replace('_',' ');
	s+=l+'\n';
      }
      //if(n>=22) break;
    }

    r.close();

    s=n+" "+(n>1 ? _("morceaux") : _("morceau"))+"\n \n"+s;

    return getStringView(s);
  }*/

  public JComponent getPlsView(VfsFile _file) throws Exception {
    int c = -1;
    int n = 0;
    long d = 0; // duration
    String s = "";
    BufferedReader r = createBufferedReader(_file);

    while (true) {
      String l = r.readLine();
      if (l == null) {
        break;
      }
      if (l.equals("")) {
        continue;
      }

      if (l.equals("[playlist]")) {
        //not supported
      } else if (l.startsWith("NumberOfEntries=")) {
        try {
          c = Integer.parseInt(l.substring(16));
        } catch (Exception ex) {
        }
      } else if (l.startsWith("File")) {
        int i = l.indexOf('=');
        if (i > 0) {
          l = l.substring(i + 1);
          if (n < 22) {
            i = l.lastIndexOf('/');
            l = l.substring(i + 1).trim();
            if (l.endsWith(".mp3")) {
              l = l.substring(0, l.length() - 4);
            }
            if (l.endsWith(".ogg")) {
              l = l.substring(0, l.length() - 4);
            }
            if (l.endsWith(".wav")) {
              l = l.substring(0, l.length() - 4);
            }
            l = l.replace('_', ' ');
            s += l + '\n';
          }
          n++;
        }
      } else if (l.startsWith("Length")) {
        int i = l.indexOf('=');
        if (i > 0) {
          l = l.substring(i + 1);
          try {
            d += Integer.parseInt(l);
          } catch (NumberFormatException ex) {
          }
        }
      }
      //if(n>=22) break;
    }

    r.close();

    if (d > 0) {
      s = getString("Dur�e totale:") + " " + FuLib.duration(1000L * d) + "\n" + s;
    }
    if (c >= 0) {
      s = c + " " + (c > 1 ? getString("morceaux") : getString("morceau")) + "\n" + s;
    }
    return getStringView(s);
  }

  /**
   * Not finished.
   */
  public JComponent getSerView(VfsFile _file) throws Exception {
    StringBuilder sb = new StringBuilder(1024);
    try (DataInputStream in = new DataInputStream(createBufferedInputStream(_file))) {

      in.read(); // AC
      in.read(); // ED
      in.read(); // 00
      in.read(); // 05

      sb.append(getString("Objet s�rialis�"));
      sb.append('\n');

      int t = in.read();
      switch (t) {
        case 0x73:
          t = in.read(); // 0x72
          if (t == 0x72) {
            String cn = in.readUTF();
            sb.append(cn);
            sb.append('\n');
          }
          break;
        case 0x77:
          sb.append("data (" + in.read() + " bytes)\n");
          break;
        case 0x7A:
          sb.append("data (" + in.readInt() + " bytes)\n");
          break;
        default:
          sb.append("bad format\n");
          break;
      }
    } catch (RuntimeException _evt) {
      throw _evt;
    }
    return getStringView(sb.toString());
  }

  public JComponent getOasisView(VfsFile _file) throws Exception {
    StringBuilder sb = new StringBuilder(2000);

    try (ZipFile zf = new ZipFile(_file.getAbsolutePath())) {
      ZipEntry ze = zf.getEntry("meta.xml");
      int n = 0;
      try (Reader in = new InputStreamReader
          (new BufferedInputStream(zf.getInputStream(ze)), "UTF-8")) {

        XmlParser p = new XmlParser(in, _file.getPath());
        XmlReader r = new XmlReader();
        p.setXmlListener(r);
        p.parse();

        XmlNode tree = r.getTree();

        XmlNode title = tree.findNode("dc:title");
        if (title != null) {
          sb.append(getString("Titre:  "));
          sb.append(title.getText());
          sb.append('\n');
          n++;
        }
        XmlNode date = tree.findNode("dc:date");
        if (date != null) {
          sb.append(getString("Date:   "));
          sb.append(date.getText());
          sb.append('\n');
          n++;
        }
        XmlNode lang = tree.findNode("dc:language");
        if (lang != null) {
          sb.append(getString("Langue: "));
          sb.append(lang.getText());
          sb.append('\n');
          n++;
        }
        XmlNode creator = tree.findNode("meta:initial-creator");
        if (creator != null) {
          sb.append(getString("Auteur: "));
          sb.append(creator.getText());
          sb.append('\n');
          n++;
        }
        XmlNode stats = tree.findNode("meta:document-statistic");
        if (stats != null) {
          sb.append(getString("Taille:"));
          String pages = stats.getAttribute("meta:page-count");
          if (pages != null) {
            sb.append(' ');
            sb.append(pages);
            sb.append(' ');
            sb.append(getString("pages"));
          }
          String words = stats.getAttribute("meta:word-count");
          if (words != null) {
            sb.append(' ');
            sb.append(words);
            sb.append(' ');
            sb.append(getString("mots"));
          }
          sb.append('\n');
          n++;
        }
      }
      if (sb.length() > 0) {
        sb.append(" \n");
        n++;
      }

      ze = zf.getEntry("content.xml");
      try (Reader in = new InputStreamReader
          (new BufferedInputStream(zf.getInputStream(ze)), "UTF-8")) {

        XmlParser p = new XmlParser(in, _file.getPath());
        XmlReader r = new XmlReader();
        p.setXmlListener(r);
        p.parse();

        XmlNode tree = r.getTree();

        XmlNode body = tree.findNode("office:body");
        if (body != null) {
          XmlNode[] nodes = body.getNodes();
          for (int i = 0; i < nodes.length; i++) {
            if (n > 22) {
              break;
            }
            String s = nodes[i].findText();
            if (s.length() > 80) {
              s = s.substring(0, 80);
            }
            sb.append(s);
            sb.append('\n');
            n++;
          }
        }
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    return getStringView(sb.toString());//,BuLabelMultiLine.LETTER);
  }

  public JComponent getVcfView(VfsFile _file) throws Exception {
    StringBuffer sb = new StringBuffer(2000);
    int n = 0;
    try (BufferedReader br = createBufferedReader(_file, "UTF-8")) {
      //DateFormat     df=new SimpleDateFormat("yyyyMMdd'T'HHmmss");

      String name = "";
      String email = "";
      String org = "";

      while (n < 22) {
        String s = br.readLine();
        if (s == null) {
          break;
        }
        if (s.startsWith("FN:")) {
          if ("".equals(name)) {
            name = s.substring(s.indexOf(":") + 1);
          }
        } else if (s.startsWith("N:")) {
          s = s.substring(s.indexOf(":") + 1);
          String[] e = FuLib.split(s, ';');
          name = e[0];
          if (e.length > 0) {
            name += ", " + e[1];
          }
        } else if (s.startsWith("EMAIL;INTERNET:")) {
          email = s.substring(s.indexOf(":") + 1);
        } else if (s.startsWith("ORG:")) {
          org = s.substring(s.indexOf(":") + 1);
          if (";".equals(org)) {
            org = "";
          }
        } else if (s.startsWith("END:") && s.toUpperCase().equals("END:VCARD")) {
          sb.append(name);
          sb.append("\n  ");
          sb.append(email);
          sb.append('\n');
          sb.append(org);
          sb.append("\n  ");
          n += 3;
          name = email = org = "";
        }
      }
    }

    return getStringView(sb.toString());
  }

  public JComponent getXmlView(VfsFile _file) throws Exception {
    char[] tc = new char[2000];
    int lc = 0;
    try (BufferedReader br = createBufferedReader(_file)) {

      while (lc < 2000) {
        int cc = br.read();
        if (cc == -1) {
          break;
        }
        tc[lc] = (char) cc;
        lc++;
      }
    }

    //TODO: use encoding

    String s = new String(tc, 0, lc);
    int i = s.indexOf("<!DOCTYPE");
    int j;
    if (i >= 0) {
      String t = s.substring(i + 9).trim();
      j = 0;
      while ((j < t.length()) &&
          !Character.isWhitespace(t.charAt(j)) &&
          (t.charAt(j) != '>')) j++;
      t = t.substring(0, j);

      FuLog.trace("BFR: found doctype being '" + t + "'");
      if ("rss".equals(t)) {
        return getRssView(_file);
      }
    }

    j = s.indexOf(">\n", i + 1);
    if ((j < 0) || (j > 800)) {
      s = FuLib.replace(s, ">", ">\n");
      lc = Math.min(2000, s.length());
      s.getChars(0, lc, tc, 0);
    }

    return getTextView0(tc, lc);
  }

  public JComponent getRssView(VfsFile vfsFile) throws Exception {
    try (final BufferedReader bufferedReader = createBufferedReader(vfsFile)) {
      XmlParser p = new XmlParser(bufferedReader, vfsFile.getPath());
      XmlReader r = new XmlReader();
      p.setXmlListener(r);
      p.parse();
      StringBuilder sb = new StringBuilder(2000);

      {
        XmlNode channel = r.getTree().getNode("channel");
        if (channel != null) {
          XmlNode title = channel.getNode("title");
          if (title != null) {
            String t = title.getText().trim();
            if (!"".equals(t)) {
              sb.append(t);
              sb.append('\n');
              for (int i = 0; i < t.length(); i++)
                sb.append('-');
              sb.append('\n');
            }
          }
        }
      }

      XmlNode[] n = r.getTree().findNodes("item");
      for (int i = 0; i < n.length; i++) {
        XmlNode title = n[i].getNode("title");
        if (title != null) {
          sb.append(title.getText());
          sb.append('\n');
        }
      }
      return getStringView(sb.toString());
    }
  }

  private String cleanRtfAccents(String _s) {
    String res = FuLib.replace(_s, "\\'e0", "�");
    res = FuLib.replace(res, "\\'e8", "�");
    res = FuLib.replace(res, "\\'e9", "�");
    res = FuLib.replace(res, "\\'ee", "�");
    res = FuLib.replace(res, "\\'f4", "�");

    res = FuLib.replace(res, "\n\n", "\n");
    res = FuLib.replace(res, "\n\n", "\n");
    res = FuLib.replace(res, "  ", " ");
    res = FuLib.replace(res, "  ", " ");
    res = FuLib.replace(res, "\n ", "\n");

    return res;
  }

  public JComponent getRtfView(VfsFile _file) throws Exception {
    StringBuffer r = new StringBuffer(1024);
    StringBuffer b = new StringBuffer(120);
    try (InputStream in = createBufferedInputStream(_file)) {
      boolean p = false;

      while (r.length() < 80 * 22) {
        int c = in.read();
        if (c == '\r') {
          continue;
        }
        if (c == '\t') {
          c = ' ';
        }

        if ((c != -1) && (c != '{')) {
          b.append((char) c);
        } else {
          String s = b.toString();
          int i = s.indexOf("}");
          if (s.indexOf("\\title") == 0) {
            s = s.substring(7, i);
            s = cleanRtfAccents(s);
            r.append("Title : ");
            r.append(s);
            r.append('\n');
            b.setLength(0);
            p = true;
          } else if (s.indexOf("\\author") == 0) {
            s = s.substring(8, i);
            s = cleanRtfAccents(s);
            r.append("Author: ");
            r.append(s);
            r.append('\n');
            b.setLength(0);
            p = true;
          } else {
            if (p) {
              if (i >= 0) {
                s = s.substring(0, i);
              }
              i = s.indexOf("\\cf");
              if (i >= 0) {
                s = s.substring(i);
                i = s.indexOf(" ");
                s = s.substring(i + 1);
                if (s.indexOf("\\") > 0) {
                  s = cleanRtfAccents(s);
                  s = FuLib.replace(s, "\\line", "\n");
                  s = FuLib.replace(s, "\\par", "\n");
                  s = FuLib.replace(s, "\\cell", "\n");
                  s = FuLib.replace(s, "\\~", " ");
                  if (!"".equals(s.trim())) {
                    r.append(s);
                  }
                }
              }
            }
            b.setLength(0);
          }

          if (c == -1) {
            break;
          }
        }
      }
    }

    String t = FuLib.replace(r.toString(), "()", "");
    t = FuLib.replace(t, "\n ", "\n");
    t = FuLib.replace(t, "\n\n", "\n");
    t = t.trim();
    return getStringView(t);
  }

  public JComponent getTarView(VfsFile _file) throws Exception {
    int n = 0;
    FuVectorString v = new FuVectorString(111);

    if (_file instanceof VfsFileFile) {
      TarFile zf = new TarFile(_file.getAbsolutePath());
      Enumeration ee = zf.entries();

      while (ee.hasMoreElements()) {
        TarEntry ze = (TarEntry) ee.nextElement();
        String se = ze.getName();
        v.addElement(se);
        int p = se.length() - 1;
        while ((p = se.lastIndexOf('/', Math.max(0, p - 1))) >= 0)
          v.addElement(se.substring(0, p + 1));
        n++;
        //if(n>=22) break;
      }

      zf.close();
    } else {
      try (TarInputStream zi = new TarInputStream(createBufferedInputStream(_file))) {
        TarEntry ze;

        while ((ze = zi.getNextEntry()) != null) {
          String se = ze.getName();
          v.addElement(se);
          int p = se.length() - 1;
          while ((p = se.lastIndexOf('/', Math.max(0, p - 1))) >= 0)
            v.addElement(se.substring(0, p + 1));
          n++;
        }
      }
    }

    v.sort();
    v.uniq();

    StringBuilder sb = new StringBuilder(1024);
    sb.append(n);
    sb.append(' ');
    sb.append(n > 1 ? getString("entr�es") : getString("entr�e"));
    sb.append("\n \n");

    int l = Math.min(22, v.size());
    for (int i = 0; i < l; i++) {
      String e = v.elementAt(i);
      int p = e.lastIndexOf('/', Math.max(0, e.length() - 2));
      for (int j = 0; j <= p; j++)
        if (e.charAt(j) == '/') {
          sb.append("  ");
        }
      sb.append(e.substring(p + 1));
      sb.append('\n');
    }

    return getStringView(sb.toString());
  }

  public JComponent getTextView(VfsFile vfsFile) throws Exception {
    char[] tc = new char[2000];
    int lc = 0;
    try (BufferedReader br = createBufferedReader(vfsFile)) {
      while (lc < 2000) {
        int cc = br.read();
        if (cc == -1) {
          break;
        }
        tc[lc] = (char) cc;
        lc++;
      }
    }

    return getTextView0(tc, lc);
  }

  protected JComponent getTextView0(char[] tc, int lc) throws Exception {
    boolean binary = false;
    int nc = Math.min(500, lc);
    for (int k = 0; k < nc; k++)
      if ((tc[k] < 128)
          && !Character.isWhitespace(tc[k])
          && Character.isISOControl(tc[k])) {
        binary = true;
        break;
      }

    String s;

    if (!binary) {
      for (int k = lc - 1; k > 0; k--)
        if (tc[k - 1] == '\r') {
          if (tc[k] == '\n') {
            tc[k - 1] = ' ';
          } else {
            tc[k - 1] = '\n';
          }
        }
      s = new String(tc, 0, lc);
    } else {
      s = "";
      for (int i = 0; i < lc; i++) {
        String t = "00" + Integer.toHexString(tc[i]);
        t = t.substring(t.length() - 2)
            + ((i % 8 == 7) ? '\n' : ' ');
        s += t;
      }
      s = s.toUpperCase();
    }

    return getStringView(s);
  }

  public JComponent getZipView(VfsFile _file) throws Exception {
    int n = 0;
    FuVectorString v = new FuVectorString(111);

    if (_file instanceof VfsFileFile) {
      try (ZipFile zf = new ZipFile(_file.getAbsolutePath())) {
        Enumeration ee = zf.entries();

        while (ee.hasMoreElements()) {
          ZipEntry ze = (ZipEntry) ee.nextElement();
          String se = ze.getName();
          v.addElement(se);
          int p = se.length() - 1;
          while ((p = se.lastIndexOf('/', Math.max(0, p - 1))) >= 0)
            v.addElement(se.substring(0, p + 1));
          n++;
        }
      }
    } else {
      try (ZipInputStream zi = new ZipInputStream(createBufferedInputStream(_file))) {
        ZipEntry ze;

        while ((ze = zi.getNextEntry()) != null) {
          String se = ze.getName();
          v.addElement(se);
          int p = se.length() - 1;
          while ((p = se.lastIndexOf('/', Math.max(0, p - 1))) >= 0)
            v.addElement(se.substring(0, p + 1));
          n++;
        }
      }
    }

    v.sort();
    v.uniq();

    StringBuffer sb = new StringBuffer(1024);
    sb.append(n);
    sb.append(' ');
    sb.append(n > 1 ? getString("entr�es") : getString("entr�e"));
    sb.append("\n \n");

    int l = Math.min(22, v.size());
    for (int i = 0; i < l; i++) {
      String e = v.elementAt(i);
      int p = e.lastIndexOf('/', Math.max(0, e.length() - 2));
      //   String u=e.substring(0,p+1);
      for (int j = 0; j <= p; j++)
        if (e.charAt(j) == '/') {
          sb.append("  ");
        }
      sb.append(e.substring(p + 1));
      sb.append('\n');
    }

    return getStringView(sb.toString());
  }

  protected final JComponent getRemoteView(final VfsFile _file,
                                           final BuPanel _pane) {
    if (_file.isBuilt()) {
      throw new Error("call getRemoteComponent() on a built file");
    }

    final BuPanel rc = _pane;
    final BuLabelMultiLine lc = createLabelMultiLine
        (getString("Ressource distante.") + " " + getString("Cliquer pour la r�cup�rer."),
            BuResource.BU.loadToolCommandIcon("VOIR"));

    lc.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent mouseEvent) {
        lc.setText(getString("R�cup�ration"));
        lc.setIcon(UIManager.getIcon("FileView.hardDriveIcon"));
        lc.paintImmediately(0, 0, lc.getWidth(), lc.getHeight());

        Runnable runnable = new Runnable() {
          File lf = null;

          @Override
          public void run() {
            try {
              _file.build();
              rc.removeAll();
              buildComponent(_file, rc);
              lf = _file;
            } catch (Exception ex) {
            }

            BuLib.invokeLater(() -> {
              if (lf == null) {
                lc.setText(getString("Impossible de r�cup�rer cette resource."));
                lc.setIcon(BuResource.BU.loadToolCommandIcon("CACHER"));
              }

              rc.revalidate();
              rc.repaint();
            });
          }
        };

        Thread t = new Thread(runnable, "Loading " + _file);
        t.setPriority(Thread.MIN_PRIORITY);
        t.start();
      }
    });

    lc.setOpaque(true);
    lc.setBackground(GRAY);
    return lc;
  }

  protected BuLabelMultiLine createLabelMultiLine(String _text, Icon _icon) {
    BuLabelMultiLine r = new BuLabelMultiLine(_text);
    r.setWrapMode(BuLabelMultiLine.WORD);
    r.setPreferredWidth(170);
    r.setHorizontalAlignment(SwingConstants.CENTER);
    r.setVerticalAlignment(SwingConstants.CENTER);
    r.setHorizontalTextPosition(SwingConstants.CENTER);
    r.setVerticalTextPosition(SwingConstants.BOTTOM);
    r.setFont(ETF);
    r.setBorder(BuBorders.EMPTY5555);
    r.setIcon(_icon);
    return r;
  }
}
