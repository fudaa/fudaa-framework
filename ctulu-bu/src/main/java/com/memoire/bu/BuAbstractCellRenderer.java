/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuAbstractCellRenderer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeCellRenderer;

public class BuAbstractCellRenderer
  extends    BuLabel
  implements ListCellRenderer, TreeCellRenderer, TableCellRenderer
	     // , ComponentListener, ContainerListener
{
  public static final int LIST    =0;
  public static final int COMBOBOX=1;
  public static final int TREE    =2;
  public static final int TABLE   =3;

  /*
  public static final ListCellRenderer LIST_CR=new DefaultListCellRenderer();
  public static final TreeCellRenderer TREE_CR=new DefaultTreeCellRenderer();
  */

  protected int     type_,itemWidth_;
  protected boolean darkerOddLines_,darkerOddColumns_;
  protected JLabel  delegate_;

  protected BuAbstractCellRenderer(int _type)
  {
    type_            =_type;
    delegate_        =null;//TMP this;
    darkerOddLines_  =true;
    darkerOddColumns_=false;

    switch(type_)
    {
    case LIST:
      darkerOddLines_=!(Boolean.FALSE.equals(UIManager.get("List.darkerOddLines"    )));
      break;
    case COMBOBOX:
      darkerOddLines_=!(Boolean.FALSE.equals(UIManager.get("ComboBox.darkerOddLines")));
      break;
    case TREE:
      darkerOddLines_=!(Boolean.FALSE.equals(UIManager.get("Tree.darkerOddLines"    )));
      break;
    case TABLE:
      darkerOddLines_  =!(Boolean.FALSE.equals(UIManager.get("Table.darkerOddLines"  )));
      darkerOddColumns_= (Boolean.TRUE .equals(UIManager.get("Table.darkerOddColumns")));
      break;
    }

  }

  public boolean isDarkerOddLines() { return darkerOddLines_; }
  public void setDarkerOddLines(boolean _darkerOddLines) { darkerOddLines_=_darkerOddLines; }

  public int getItemWidth() { return itemWidth_; }
  public void setItemWidth(int _itemWidth) { itemWidth_=_itemWidth; }

  @Override
  public void paint(Graphics _g)
  {
    if((delegate_==this)||(delegate_==null))
    {
      super.paint(_g);
    }
    else
    {
      /*
      Dimension ps=delegate_.getPreferredSize();
      Dimension cs=delegate_.getSize();
      delegate_.setSize(Math.max(ps.width,cs.width),
			Math.max(ps.height,cs.height));
      */
      BuLib.setAntialiasing(this,_g);

      // JDK11
      try
      {
        delegate_.paint(_g);
      }
      catch(NullPointerException ex)
      {
        FuLog.warning("BAC: BuAbstractCellRenderer:95 Null Pointer");
        Icon old=delegate_.getIcon();
        delegate_.setIcon(null);
        delegate_.paint(_g);
        delegate_.setIcon(old);
      }
    }
  }

  @Override
  public Color getBackground()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getBackground();
    return delegate_.getBackground();
  }

  @Override
  public void setBackground(Color _color)
  {
    if((delegate_==this)||(delegate_==null))
      super.setBackground(_color);
    else
      delegate_.setBackground(_color);
  }

  @Override
  public Color getForeground()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getForeground();
    return delegate_.getForeground();
  }

  @Override
  public void setForeground(Color _color)
  {
    if((delegate_==this)||(delegate_==null))
      super.setForeground(_color);
    else
      delegate_.setForeground(_color);
  }

  @Override
  public void setBounds(int _x, int _y, int _w, int _h)
  {
    if((delegate_==this)||(delegate_==null))
      super.setBounds(_x,_y,_w,_h);
    else
      delegate_.setBounds(_x,_y,_w,_h);
  }

  @Override
  public boolean isDoubleBuffered()
  {
    if((delegate_==this)||(delegate_==null))
      return super.isDoubleBuffered();
    return delegate_.isDoubleBuffered();
  }

  @Override
  public void setDoubleBuffered(boolean _state)
  {
    if((delegate_==this)||(delegate_==null))
      super.setDoubleBuffered(_state);
    else
      delegate_.setDoubleBuffered(_state);
  }

  @Override
  public boolean isOpaque()
  {
    if((delegate_==this)||(delegate_==null))
      return super.isOpaque();
    return delegate_.isOpaque();
  }

  @Override
  public void setOpaque(boolean _state)
  {
    if((delegate_==this)||(delegate_==null))
      super.setOpaque(_state);
    else
      delegate_.setOpaque(_state);
  }

  @Override
  public Container getParent()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getParent();
    return delegate_.getParent();
  }

  @Override
  public Icon getIcon()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getIcon();
    return delegate_.getIcon();
  }

  @Override
  public void setIcon(Icon _icon)
  {
    if((delegate_==this)||(delegate_==null))
      super.setIcon(_icon);
    else
      delegate_.setIcon(_icon);
  }

  @Override
  public Icon getDisabledIcon()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getDisabledIcon();
    return delegate_.getDisabledIcon();
  }

  @Override
  public void setDisabledIcon(Icon _icon)
  {
    if((delegate_==this)||(delegate_==null))
      super.setDisabledIcon(_icon);
    else
      delegate_.setDisabledIcon(_icon);
  }

  @Override
  public int getDisplayedMnemonic()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getDisplayedMnemonic();
    return delegate_.getDisplayedMnemonic();
  }

  @Override
  public void setDisplayedMnemonic(int _mnemonic)
  {
    if((delegate_==this)||(delegate_==null))
      super.setDisplayedMnemonic(_mnemonic);
    else
      delegate_.setDisplayedMnemonic(_mnemonic);
  }

  @Override
  public int getHorizontalAlignment()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getHorizontalAlignment();
    return delegate_.getHorizontalAlignment();
  }

  @Override
  public void setHorizontalAlignment(int _halign)
  {
    if((delegate_==this)||(delegate_==null))
      super.setHorizontalAlignment(_halign);
    else
      delegate_.setHorizontalAlignment(_halign);
  }

  @Override
  public int getHorizontalTextPosition()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getHorizontalTextPosition();
    return delegate_.getHorizontalTextPosition();
  }

  @Override
  public void setHorizontalTextPosition(int _htp)
  {
    if((delegate_==this)||(delegate_==null))
      super.setHorizontalTextPosition(_htp);
    else
      delegate_.setHorizontalTextPosition(_htp);
  }

  @Override
  public int getIconTextGap()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getIconTextGap();
    return delegate_.getIconTextGap();
  }

  @Override
  public void setIconTextGap(int _itg)
  {
    if((delegate_==this)||(delegate_==null))
      super.setIconTextGap(_itg);
    else
      delegate_.setIconTextGap(_itg);
  }

  @Override
  public Dimension getMinimumSize()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getMinimumSize();
    return delegate_.getMinimumSize();
  }

  @Override
  public void setMinimumSize(Dimension _size)
  {
    if((delegate_==this)||(delegate_==null))
      super.setMinimumSize(_size);
    else
      delegate_.setMinimumSize(_size);
  }

  @Override
  public Dimension getMaximumSize()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getMaximumSize();
    return delegate_.getMaximumSize();
  }

  @Override
  public void setMaximumSize(Dimension _size)
  {
    if((delegate_==this)||(delegate_==null))
      super.setMaximumSize(_size);
    else
      delegate_.setMaximumSize(_size);
  }

  @Override
  public Dimension getPreferredSize()
  {
    Dimension r=null;

    if((delegate_==this)||(delegate_==null))
    {
      r=super.getPreferredSize();
      if((type_==TREE)&&(r!=null))
	r=new Dimension(r.width+3,r.height);
    }
    else
      r=delegate_.getPreferredSize();

    if(r!=null&&(type_==LIST)&&(itemWidth_>0))
      r=new Dimension(itemWidth_,r.height);

    return r;
  }

  @Override
  public void setPreferredSize(Dimension _size)
  {
    if((delegate_==this)||(delegate_==null))
      super.setPreferredSize(_size);
    else
      delegate_.setPreferredSize(_size);
  }

  @Override
  public String getText()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getText();
      return delegate_.getText();
  }

  @Override
  public void setText(String _text)
  {
    if((delegate_==this)||(delegate_==null))
      super.setText(_text);
    else
      delegate_.setText(_text);
  }

  @Override
  public int getVerticalAlignment()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getVerticalAlignment();
      return delegate_.getVerticalAlignment();
  }

  @Override
  public void setVerticalAlignment(int _halign)
  {
    if((delegate_==this)||(delegate_==null))
      super.setVerticalAlignment(_halign);
    else
      delegate_.setVerticalAlignment(_halign);
  }

  @Override
  public int getVerticalTextPosition()
  {
    if((delegate_==this)||(delegate_==null))
      return super.getVerticalTextPosition();
      return delegate_.getVerticalTextPosition();
  }

  @Override
  public void setVerticalTextPosition(int _htp)
  {
    if((delegate_==this)||(delegate_==null))
      super.setVerticalTextPosition(_htp);
    else
      delegate_.setVerticalTextPosition(_htp);
  }

  /*
    + getLabelFor () : Component
    + getUI () : LabelUI
    + getUIClassID () : String
  */

  @Override
  public void doLayout()
  {
    if((delegate_==this)||(delegate_==null))
      super.doLayout();
    else
      delegate_.doLayout();
  }

  @Override
  public void revalidate()
  {
    if((delegate_==this)||(delegate_==null))
      super.revalidate();
    else
      delegate_.revalidate();
  }

  @Override
  public void invalidate()
  {
    if((delegate_==this)||(delegate_==null))
      super.invalidate();
    else
      delegate_.invalidate();
  }

  @Override
  public void validate()
  {
    if((delegate_==this)||(delegate_==null))
      super.validate();
    else
      delegate_.validate();
  }

  //public void validate() {}
  //public void revalidate() {}
  //public void repaint(long tm, int x, int y, int width, int height) {}
  //public void repaint(Rectangle r) {}

  @Override
  protected void firePropertyChange(String _name, Object _old, Object _new)
  {
    if(_name=="text")
      super.firePropertyChange(_name, _old, _new);
  }

  @Override
  public void firePropertyChange(String _name, byte    _old, byte    _new) { }
  @Override
  public void firePropertyChange(String _name, char    _old, char    _new) { }
  @Override
  public void firePropertyChange(String _name, short   _old, short   _new) { }
  @Override
  public void firePropertyChange(String _name, int     _old, int     _new) { }
  @Override
  public void firePropertyChange(String _name, long    _old, long    _new) { }
  @Override
  public void firePropertyChange(String _name, float   _old, float   _new) { }
  @Override
  public void firePropertyChange(String _name, double  _old, double  _new) { }
  @Override
  public void firePropertyChange(String _name, boolean _old, boolean _new) { }

  /*
  public void componentResized(ComponentEvent _evt) { }
  public void componentMoved  (ComponentEvent _evt) { }
  public void componentShown  (ComponentEvent _evt) { }
  public void componentHidden (ComponentEvent _evt) { }

  public void componentAdded  (ContainerEvent _evt) { }
  public void componentRemoved(ContainerEvent _evt) { }
  */

  @Override
  public Component getListCellRendererComponent
    (JList   _list,
     Object  _value,
     int     _row,
     boolean _selected,
     boolean _focus)
  {
    return getRealRenderer
      (_list,_value,_selected,false,true,_row,0,_focus);
  }

  @Override
  public Component getTreeCellRendererComponent
    (JTree   _tree,
     Object  _value,
     boolean _selected,
     boolean _expanded,
     boolean _leaf,
     int     _row,
     boolean _focus)
  {
    return getRealRenderer
      (_tree,_value,_selected,_expanded,_leaf,_row,0,_focus);
  }

  @Override
  public Component getTableCellRendererComponent
    (JTable  _table,
     Object  _value,
     boolean _selected,
     boolean _focus,
     int     _row,
     int     _column)
  {
    Component r=getRealRenderer
      (_table,_value,_selected,false,false,_row,_column,_focus);

    if(!_selected)
    {
      TableModel m=_table.getModel();
      if(m instanceof BuTableSortModel)
      {
        int c=((BuTableSortModel)m).getSortingColumn();
        if(c==_column)
        {
          Color bg=r.getBackground();
          bg=BuLib.mixColors(bg,getSortingBackground());

          if(darkerOddLines_||darkerOddColumns_)
          {
            if((darkerOddLines_  &&((_row   %2)==1))||
               (darkerOddColumns_&&((_column%2)==1)))
              bg=BuLib.mixColors(bg,getSortingBackground());
          }

          r.setBackground(BuLib.getColor(bg));
        }
      }
    }

    return r;
  }

  protected Color getSortingBackground()
  {
    Color r=UIManager.getColor("Table.sortingBackground");
    if(r==null)
    {
      r=UIManager.getColor("Table.background");
      if(r==null) r=Color.white;
      r=BuLib.mixColors(r,Color.lightGray);
    }
    return r;
  }

  protected Component getRealRenderer
    (JComponent _comp,
     Object     _value,
     boolean    _selected,
     boolean    _expanded,
     boolean    _leaf,
     int        _row,
     int        _column,
     boolean    _focus)
  {
    if(delegate_!=this)
    {
      Object o=null;

      if(delegate_!=null)
      {
	o=delegate_;
      }
      else
      {
        switch(type_)
        {
        case LIST:
	  o=UIManager.get("List.cellRenderer");
	  break;
	case COMBOBOX:
	  o=UIManager.get("ComboBox.cellRenderer");
	  if(o==null) o=UIManager.get("ComboBox.renderer");
	  if(o==null) o=UIManager.get("List.cellRenderer");
	  break;
	case TREE:
	  o=UIManager.get("Tree.cellRenderer");
	  break;
	case TABLE:
	  o=UIManager.get("Table.cellRenderer");
	  break;
	}
      }

      delegate_=this;

      if((o!=null)/*&&(FuLib.jdk()>1.1)*/)
      {
        if(o instanceof ListCellRenderer)
        {
	  o=((ListCellRenderer)o).
	    getListCellRendererComponent
	    ((JList)_comp,_value,_row,_selected,_focus);
	  if(o instanceof JLabel) delegate_=(JLabel)o;
	}

	if(o instanceof TreeCellRenderer)
	{
	  o=((TreeCellRenderer)o).
	    getTreeCellRendererComponent
	    ((JTree)_comp,_value,_selected,_expanded,_leaf,_row,_focus);
	  if(o instanceof JLabel) delegate_=(JLabel)o;
	}

	if(o instanceof TableCellRenderer)
        {
	  o=((TableCellRenderer)o).
	    getTableCellRendererComponent
	    ((JTable)_comp,_value,_selected,_focus,_row,_column);
	  if(o instanceof JLabel) delegate_=(JLabel)o;
	}
      }
    }

    /*
    if(delegate_==this)
    {
      switch(type_)
      {
        case LIST:
	    delegate_=(JLabel)LIST_CR.
		getListCellRendererComponent
		((JList)_comp,_value,_row,_selected,_focus);
	    break;
        case COMBOBOX:
	    delegate_=(JLabel)LIST_CR. //COMBOBOX_CR.
		getListCellRendererComponent
		((JList)_comp,_value,_row,_selected,_focus);
	    break;
        case TREE:
	    delegate_=(JLabel)TREE_CR.
		getTreeCellRendererComponent
		((JTree)_comp,_value,_selected,_expanded,_leaf,_row,_focus);
	    break;
        case TABLE:
	    break;
      }
    }
    */

    if(delegate_==this)
    {
      if(_selected)
      {
	if(_comp instanceof JList)
	{
	  super.setBackground(((JList)_comp).getSelectionBackground());
	  super.setForeground(((JList)_comp).getSelectionForeground());
	}
	else
	if(_comp instanceof JTree)
	{
	  super.setBackground(UIManager.getColor("Tree.selectionBackground"));
	  super.setForeground(UIManager.getColor("Tree.selectionForeground"));
	}
	else
	if(_comp instanceof JTable)
	{
	  super.setBackground(((JTable)_comp).getSelectionBackground());
	  super.setForeground(((JTable)_comp).getSelectionForeground());
	}
      }
      else
      {
	super.setBackground(_comp.getBackground());
	super.setForeground(_comp.getForeground());
      }

      if(_comp instanceof JTree)
      {
	super.setText(((JTree)_comp).convertValueToText
			  (_value,_selected,_expanded,_leaf,_row,_focus));

	     if(_leaf    ) super.setIcon(UIManager.getIcon("Tree.leafIcon"  ));
	else if(_expanded) super.setIcon(UIManager.getIcon("Tree.openIcon"  ));
	else               super.setIcon(UIManager.getIcon("Tree.closedIcon"));
      }
      else
      {
	super.setText(""+_value);
      }

      super.setOpaque(true);
      super.setEnabled(_comp.isEnabled());
      super.setFont(_comp.getFont());
      super.setBorder
	  (_focus
	   ? UIManager.getBorder((_comp instanceof JTable)
				 ? "Table.focusCellHighlightBorder"
				 : "List.focusCellHighlightBorder")
	   : BuBorders.EMPTY1111);
    }

    /*
    Color  bg=getBackground();
    Color  fg=getForeground();
    int    a =LEFT;
    String t ="";
    Icon   i =getIcon();

    if(_value instanceof Number)
    {
      a=RIGHT;
      if(((Number)_value).doubleValue()<0)
	fg=blueify(fg);
      if(numberFormat_!=null)
	t=numberFormat_.format(((Number)_value).doubleValue());
      else
	t=_value.toString();
    }
    else
    if(_value instanceof Date)
    {
      if(dateFormat_!=null)
	t=dateFormat_.format((Date)_value);
      else
	t=_value.toString();
    }
    else
    if(_value instanceof Boolean)
    {
      if(_selected)
	bg=((Boolean)_value).booleanValue()
	  ? BuLib.getColor(new Color(128,255,128))
	  : BuLib.getColor(new Color(255,128,128));
      else
	bg=((Boolean)_value).booleanValue()
	  ? BuLib.getColor(new Color(192,255,192))
	  : BuLib.getColor(new Color(255,192,192));
    }
    else
    if(_value instanceof BuIcon)
    {
      i=BuResource.resizeIcon((BuIcon)_value,Math.max(16,ft.getSize()));
    }
    else
    if(_value instanceof Icon)
    {
      i=(Icon)_value;
    }
    else
    if(_value==null)
    {
      t="";
    }
    else
    {
      t=_value.toString();
    }
    */

    if((getIcon()==null)&&"".equals(getText()))
      setText(" ");

    if(darkerOddLines_||darkerOddColumns_)
    {
      //setOpaque(true);
      if((darkerOddLines_  &&((_row   %2)==1))||
	 (darkerOddColumns_&&((_column%2)==1)))
	//bg=darken(bg);
	//if(darkerOddLines_&&(_row%2)==1)
      {
	Color bg=getBackground();
	// bg is null in GTKLookAndFeel
	if(bg!=null) setBackground(darken(bg));
      }
    }

    return this;
  }

  private static final Color darken(Color _c)
  {
    int r=_c.getRed();
    int g=_c.getGreen();
    int b=_c.getBlue();

    r=r*19/20;
    g=g*19/20;
    b=b*19/20;

    Color c=(_c instanceof ColorUIResource)
      ? new ColorUIResource(r,g,b)
      : new Color(r,g,b);

    return c;
  }

  // Factory

  /*
    Memory leak ?
    private static final ListCellRenderer LISTCR =
    new BuAbstractCellRenderer(LIST);
    private static final ListCellRenderer COMBOBOXCR=
    new BuAbstractCellRenderer(COMBOBOX);
    private static final TreeCellRenderer TREECR=
    new BuAbstractCellRenderer(TREE);
    private static final TableCellRenderer TABLECR=
    new BuAbstractCellRenderer(TABLE);
  */

  public static final ListCellRenderer getDefaultListCellRenderer()
  {
    //return LISTCR;
    return  new BuAbstractCellRenderer(LIST);
  }

  public static final ListCellRenderer getDefaultComboBoxCellRenderer()
  {
    //return COMBOBOXCR;
    return new BuAbstractCellRenderer(COMBOBOX);
  }

  public static final TreeCellRenderer getDefaultTreeCellRenderer()
  {
    //return TREECR;
    return new BuAbstractCellRenderer(TREE);
  }

  public static final TableCellRenderer getDefaultTableCellRenderer()
  {
    //return TABLECR;
    return new BuAbstractCellRenderer(TABLE);
  }
}
