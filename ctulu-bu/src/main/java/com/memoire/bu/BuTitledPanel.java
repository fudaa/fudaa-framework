/**
 * @modification $Date: 2007-05-04 13:41:57 $ @statut unstable @file BuTitledPanel.java
 * @version 0.43
 * @author Guillaume Desnoix @email guillaume@desnoix.com @license GNU General Public License 2 (GPL2) @copyright 1998-2005
 * Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

/**
 * A panel that takes a component for title (cf BeOS).
 */
public class BuTitledPanel
        extends BuPanel {

  private class XBorder extends TitledBorder {

    public XBorder() {
      super("");
    }

    public int getHeightAdjustment() {
      return EDGE_SPACING + TEXT_INSET_H;
    }

    public int getTitleX() {
      return EDGE_SPACING + TEXT_INSET_H + getBorderInsets(BuTitledPanel.this).left;
    }

    public int getTitleY() {
      return 0;//TitledBorder.EDGE_SPACING;
    }
  }
  private XBorder border_;
  private JComponent title_;
  private Dimension ps_;

  public BuTitledPanel(JComponent _title) {
    this(_title, new BuBorderLayout(5, 5), BuBorders.EMPTY5555);
  }

  public BuTitledPanel(JComponent _title, LayoutManager _layout) {
    this(_title, _layout, BuBorders.EMPTY5555);
  }

  public BuTitledPanel(JComponent _title, LayoutManager _layout, Border _inside) {
    super();
    title_ = _title;
    border_ = new XBorder();

    Font ft = border_.getTitleFont();
    if (ft == null) {
      ft = BuLib.DEFAULT_FONT;
    }
    Color fg = border_.getTitleColor();
    title_.setForeground(fg);
    title_.setFont(ft);

    ps_ = title_.getPreferredSize();

    ft = new Font("Monospaced", Font.PLAIN,
                  Math.max(0, ps_.height - border_.getHeightAdjustment()));

    FontMetrics fm = BuLib.getFontMetrics(null, ft);
    String tx = "";
    //int         ws=0;

    while ((/*
             * int ws=
             */fm.stringWidth(tx)) < ps_.width) {
      tx += " ";
    }
    border_.setTitle(tx);
    border_.setTitleFont(ft);

    add(title_);
    setBorder(new CompoundBorder(border_, _inside));
    setLayout(_layout);
  }

  @Override
  public void layout() {
    remove(title_);
    super.layout();
    add(title_);

    title_.setBounds(border_.getTitleX(), border_.getTitleY(),
                     ps_.width, ps_.height);
  }

  @Override
  public Dimension getPreferredSize() {
    remove(title_);
    Dimension r = super.getPreferredSize();
    add(title_);
    return r;
  }

  @Override
  public Dimension getMinimumSize() {
    remove(title_);
    Dimension r = super.getMinimumSize();
    add(title_);
    return r;
  }

  @Override
  public Dimension getMaximumSize() {
    remove(title_);
    Dimension r = super.getMaximumSize();
    add(title_);
    return r;
  }

  public JComponent getTitle() {
    return title_;
  }
}
