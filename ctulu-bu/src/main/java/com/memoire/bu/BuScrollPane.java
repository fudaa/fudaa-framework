/**
 * @modification $Date: 2007-05-04 13:41:56 $
 * @statut       unstable
 * @file         BuScrollPane.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;

/**
 * Nothing more than JScrollPane.
 */
public class BuScrollPane
       extends JScrollPane
       implements ActionListener
{
  public BuScrollPane()
  {
    this(null,VERTICAL_SCROLLBAR_AS_NEEDED,HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }

  public BuScrollPane(JComponent _c)
  {
    this(_c,VERTICAL_SCROLLBAR_AS_NEEDED,HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }

  public BuScrollPane(int _vsb,int _hsb)
  {
    this(null,_vsb,_hsb);
  }

  public BuScrollPane(JComponent _c,int _vsb,int _hsb)
  {
    super(_c,_vsb,_hsb);

    if(!(_c instanceof Scrollable))
    {
      getHorizontalScrollBar().setBlockIncrement(100);
      getHorizontalScrollBar().setUnitIncrement ( 20);
      getVerticalScrollBar()  .setBlockIncrement(100);
      getVerticalScrollBar()  .setUnitIncrement ( 20);
    }

    /*
    BuButton lr=new BuButton("")
    {
      public boolean isOpaque()          { return false; }
      public boolean isRolloverEnabled() { return false; }
    };
    lr.setBorder(BuBorders.EMPTY0000);
    lr.setMargin(BuInsets.INSETS0000);
    lr.setRequestFocusEnabled(false);
    lr.setRolloverEnabled(false);
    lr.setOpaque(false);
    lr.setIcon(BuResource.BU.getIcon("bu_scrollpane_corner"));
    lr.putClientProperty("SLAF_NO_TEXTURE",Boolean.TRUE);
    lr.addActionListener(this);
    */
    BuTransparentButton lr=new BuTransparentButton
      (null,null,BuResource.BU.getIcon("bu_scrollpane_corner"),this);
    lr.setRolloverEnabled(false);
    lr.setHorizontalAlignment(SwingConstants.CENTER);
    lr.setVerticalAlignment(SwingConstants.CENTER);
    setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER,lr);

    //JViewport v=getViewport();
    //v.setScrollMode(JViewport.BLIT_SCROLL_MODE);

//    if(FuLib.jdk()>=1.4)
//    {
      setFocusCycleRoot(false);
//    }
  }

  @Override
  public boolean isFocusCycleRoot()
  {
    return false;
  }

  private boolean psflag_;
  private int     pw_    =-1;
  private int     ph_    =-1;

  @Override
  public Dimension getPreferredSize()
  {
    Dimension r=super.getPreferredSize();
    Insets    i=getInsets();
    JViewport v=getViewport();
    Insets    j=(v!=null ? v.getInsets() : BuInsets.INSETS0000);

    if(pw_>=0) r.width =pw_+i.left+i.right+j.left+j.right;
    if(ph_>=0) r.height=ph_+i.top+i.bottom+j.top+j.bottom;

    if(!psflag_)
    {
      JScrollBar s;

      s=getVerticalScrollBar();
      if((s!=null)&&
	 (getVerticalScrollBarPolicy()==VERTICAL_SCROLLBAR_AS_NEEDED))
	r.width+=s.getPreferredSize().width;

      s=getHorizontalScrollBar();
      if((s!=null)&&
	 (getHorizontalScrollBarPolicy()==HORIZONTAL_SCROLLBAR_AS_NEEDED))
	r.height+=s.getPreferredSize().height;
    }

    return r;
  }

  @Override
  public void setPreferredSize(Dimension _ps)
  {
    psflag_=(_ps!=null);
    super.setPreferredSize(_ps);

    if(psflag_&&_ps!=null)
    {
      pw_=_ps.width;
      ph_=_ps.height;
    }
  }

  public void setPreferredWidth(int _pw)
  {
    pw_=_pw;
  }

  public void setPreferredHeight(int _ph)
  {
    ph_=_ph;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JViewport vp=getViewport();

    Point     p =vp.getViewPosition();
    Dimension vs=vp.getViewSize();
    Dimension es=vp.getExtentSize();

    if((p.x==0)&&(p.y==0))
      { p.x=vs.width-es.width; p.y=vs.height-es.height; }
    else
      { p.x=0; p.y=0; }
    vp.setViewPosition(p);
  }
}
