/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuSubApp.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 * Classe de base pour une sous-application.
 * Compatible Bu et Swing.
 * Delegue a BuCommonImplementation.
 */

public class BuSubApp
       extends BuInternalFrame
       implements BuCommonInterface,
		  // WindowListener,
		  InternalFrameListener
{
  public BuSubApp()
  {
    super("",true,false,true,true);
    BuRegistry.register(this);
    setImplementation(new BuCommonImplementation());
  }

  @Override
  public void init()
  {
    Dimension ecran=getToolkit().getScreenSize();
    setLocation(ecran.width/5,ecran.height/5);
    setSize(ecran.width*3/5,ecran.height*3/5);

    // setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    // addWindowListener(this);

    implementation_.init();
  }

  @Override
  public void start()
  {
    implementation_.prestart();
    setVisible(true);
    implementation_.start();
  }

  // App

  @Override
  public BuCommonInterface getApp()
  { return this; }

  // Implementation

  protected BuCommonImplementation implementation_;

  @Override
  public BuCommonImplementation getImplementation()
  { return implementation_; }

  public void setImplementation(BuCommonImplementation _implementation)
  {
    implementation_=_implementation;
    implementation_.setApp(this);
  }

  // Main Panel

  @Override
  public BuMainPanel getMainPanel()
  { return implementation_.getMainPanel(); }

  @Override
  public void setMainPanel(BuMainPanel _p)
  { implementation_.setMainPanel(_p); }

  // Main MenuBar

  @Override
  public BuMenuBar getMainMenuBar()
  { return implementation_.getMainMenuBar(); }

  @Override
  public void setMainMenuBar(BuMenuBar _mb)
  { implementation_.setMainMenuBar(_mb); }

  @Override
  public void setJMenuBar(JMenuBar _menubar)
  { }

  // Main ToolBar

  @Override
  public BuToolBar getMainToolBar()
  { return implementation_.getMainToolBar(); }

  @Override
  public void setMainToolBar(BuToolBar _tb)
  { implementation_.setMainToolBar(_tb); }

  // Specific Bar

  @Override
  public BuSpecificBar getSpecificBar()
  { return implementation_.getSpecificBar(); }

  @Override
  public void setSpecificBar(BuSpecificBar _sb)
  { implementation_.setSpecificBar(_sb); }

  // Action

  @Override
  public void actionPerformed(ActionEvent _evt)
  { implementation_.actionPerformed(_evt); }

  // Divers

  @Override
  public BuInformationsSoftware getInformationsSoftware()
  { return implementation_.getInformationsSoftware(); }

  @Override
  public Frame getFrame()
  {
    Frame r=null;

    Component c=this;
    do
    {
      c=c.getParent();
      if(c instanceof Frame) { r=(Frame)c; break; }
    }
    while(c!=null);

    return r;
  }

  @Override
  public void setLookAndFeel(String _lnf)
  { implementation_.setLookAndFeel(_lnf); }

  @Override
  public void removeAction(String _cmd)
  { implementation_.removeAction(_cmd); }

  @Override
  public void removeDummySeparators()
  { implementation_.removeDummySeparators(); }

  @Override
  public void setEnabledForAction(String _cmd, boolean _enabled)
  { implementation_.setEnabledForAction(_cmd,_enabled); }

  @Override
  public void setCheckedForAction(String _cmd, boolean _enabled)
  { implementation_.setCheckedForAction(_cmd,_enabled); }

  @Override
  public void showOrHide(String _cmd, JComponent _c)
  { implementation_.showOrHide(_cmd,_c); }

  @Override
  public boolean confirmExit()
  { return implementation_.confirmExit(); }

  @Override
  public void displayURL(String _url)
  {
    String url=_url;
    BuInformationsSoftware il=getInformationsSoftware();
    if(url==null) url=il.man;

    BuDialogMessage        bd=new BuDialogMessage(this,il,url);
    bd.activate();
  }

  @Override
  public void setVisible(boolean _visible)
  {
    if(_visible)
    {
      BuLib.setDoubleBuffered(getMainMenuBar(),false);
      BuLib.setDoubleBuffered(getMainToolBar(),false);
      // Main panel is done in BuInternalFrame.setVisible()
    }
    super.setVisible(_visible);
  }

  // Commands

  @Override
  public void about()
  { implementation_.about(); }

  @Override
  public void cut()
  { implementation_.cut(); }

  @Override
  public void copy()
  { implementation_.copy(); }

  @Override
  public void exit()
  {
    BuInformationsSoftware il=getInformationsSoftware();
    BuDialogConfirmation   bd=new BuDialogConfirmation
      (this,il,"Voulez-vous vraiment quitter ce logiciel ?");

    int r=bd.activate();
    if(r==JOptionPane.YES_OPTION)
    {
      BuRegistry.unregister(this);
      // System.exit(0);
    }      
  }

  @Override
  public void duplicate()
  { implementation_.duplicate(); }

  @Override
  public void find()
  { implementation_.find(); }

  @Override
  public void license()
  { implementation_.license(); }

  @Override
  public void paste()
  { implementation_.paste(); }

  @Override
  public void print()
  {
    JInternalFrame frame=getMainPanel().getCurrentInternalFrame();

    if(frame instanceof BuPrintable)
    {
      BuPrinter.print(null, // @@@
		      frame.getTitle(),
		      (BuPrintable)frame);
    }
    else
    {
      BuInformationsSoftware il=getInformationsSoftware();
     /* BuDialogError          bd=*/new BuDialogError
	(this,il,"L'impression n'est pas disponible pour \""+frame.getTitle()+"\".");
    }
  }

  @Override
  public void redo()
  { implementation_.redo(); }

  @Override
  public void replace()
  { implementation_.replace(); }

  @Override
  public void select()
  { implementation_.select(); }

  @Override
  public void undo()
  { implementation_.undo(); }

  // Window

  public void windowActivated(WindowEvent _evt)
  { implementation_.windowActivated(_evt); }

  public void windowClosed(WindowEvent _evt)
  { implementation_.windowClosed(_evt); }

  public void windowClosing(WindowEvent _evt)
  { implementation_.windowClosing(_evt);  }

  public void windowDeactivated(WindowEvent _evt)
  { implementation_.windowDeactivated(_evt); }

  public void windowDeiconified(WindowEvent _evt)
  { implementation_.windowDeiconified(_evt); }

  public void windowIconified(WindowEvent _evt)
  { implementation_.windowIconified(_evt); }

  public void windowOpened(WindowEvent _evt)
  { implementation_.windowOpened(_evt); }

  // InternalFrame

  @Override
  public void internalFrameActivated(InternalFrameEvent _evt)
  { implementation_.internalFrameActivated(_evt); }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _evt)
  { implementation_.internalFrameDeactivated(_evt); }

  @Override
  public void internalFrameClosed(InternalFrameEvent _evt)
  { implementation_.internalFrameClosed(_evt); }

  @Override
  public void internalFrameClosing(InternalFrameEvent _evt)
  { implementation_.internalFrameClosing(_evt); }

  @Override
  public void internalFrameDeiconified(InternalFrameEvent _evt)
  { implementation_.internalFrameDeiconified(_evt); }

  @Override
  public void internalFrameIconified(InternalFrameEvent _evt)
  { implementation_.internalFrameIconified(_evt); }

  @Override
  public void internalFrameOpened(InternalFrameEvent _evt)
  { implementation_.internalFrameOpened(_evt); }
}
