/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut       unstable
 * @file         BuToolBar.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JSeparator;
import javax.swing.JToolBar;

/**
 * A tool bar with simplified adding methods.
 */

public class BuToolBar
  extends JToolBar
  implements ActionListener//, PropertyChangeListener
{
  /*
  private static final int ML=8;
  private static final int MR=8;

  private int dx_=ML;
  private int dy_=ML;

  private class MA
    extends MouseAdapter
  {
    public void mouseClicked(MouseEvent _evt)
    {
      Insets    insets=getInsets();
      Dimension size  =getSize();
      boolean   done  =false;

      if(isHorizontal())
      {
	int x=_evt.getX();
	if(  ((x>=insets.left)&&(x<insets.left+ML))
	   ||(  (x>=size.width-insets.right-MR)
	      &&(x<size.width-insets.right)))
	{
	  dy_=ML;
	  if(dx_!=ML)
	    dx_=ML;
	  else
	    dx_=Math.min(ML,getWidth()-getSuperPreferredSize().width-MR);
	  done=true;
	}
      }
      else
      {
	int y=_evt.getY();
	if(  ((y>=insets.top)&&(y<insets.top+ML))
	   ||(  (y>=size.height-insets.bottom-MR)
	      &&(y<size.height-insets.bottom)))
	{
	  dx_=ML;
	  if(dy_!=ML)
	    dy_=ML;
	  else
	    dy_=Math.min(ML,getHeight()-getSuperPreferredSize().height-MR);
	  done=true;
	}
      }

      if(done)
      {
	doLayout();
	repaint();
      }
    }
  }
  */

  public BuToolBar()
  {
    super();
    setName("tbMAIN_TOOLBAR");
    setFloatable
      (BuPreferences.BU.getBooleanProperty("toolbar.floatable",false));
    putClientProperty
      ("JToolBar.isRollover",
       !BuLib.isKunststoff()
       &&BuPreferences.BU.getBooleanProperty("toolbar.rollover",true)
       ? Boolean.TRUE : Boolean.FALSE);

    /*
    addPropertyChangeListener(this);
    MA ma=new MA();
    addMouseListener(ma);
    */
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  private String title_="Main toolbar";
  public String getTitle() { return title_; }
  public void setTitle(String _title) { title_=_title; }

  private Image icon_;
  public Image getIconImage() { return icon_; }
  public void setIconImage(Image _icon) { icon_=_icon; }

  public void propertyChange(PropertyChangeEvent _evt)
  {
    if("ancestor".equals(_evt.getPropertyName()))
    {
      Container a=(Container)_evt.getNewValue();//getTopLevelAncestor();
      //System.err.println("ANCESTOR="+a);
      if((a!=null)&&(a.getClass()==JFrame.class))
      {
	try // Korte
	{
	  JFrame f=(JFrame)a;
	  if(getTitle()    !=null) f.setTitle(getTitle());
	  if(getIconImage()!=null) f.setIconImage(getIconImage());
	  f.setResizable(true);
	  f.pack();
	}
	catch(Throwable th) { }
      }

      revalidate();
    }
/*    else
    {
      //System.err.println(_evt.getPropertyName());
    }*/
  }

  @Override
  public boolean isFocusCycleRoot()
  {
    return true;
  }

  public Dimension getSuperPreferredSize()
  {
    Dimension r;
    if(!isFloatable()&&(getComponentCount()==0))
      r=new Dimension(0,0);
    else
      r=super.getPreferredSize();
    return r;
  }

  @Override
  public Dimension getPreferredSize()
  {
    Dimension r=getSuperPreferredSize();
    //if(r.width>0) r.width+=ML+MR;//+2;
    return r;
  }

  // Separators

  public void removeDummySeparators()
  {
    Component[] c;
    int         l;

    // step 1: remove double separators

    c=getComponents();
    l=c.length;
    if(l==0) return;
      
    boolean   was  =true;
    Component first=null;
    for(int i=l-1;i>=0;i--)
    {
      if(c[i] instanceof JSeparator)
      {
	if(was)
	{
	  remove(c[i]);
	}
	else
	{
	  first=c[i];
	  was=true;
	}
      }
      else
      {
	was=false;
	first=c[i];
      }
    }

    if(first instanceof JSeparator)
      remove(first);

    // step 2: remove separators around single element

    c=getComponents();
    l=c.length;
    if(l==0) return;

    for(int i=0;i<l;i++)
      if((c[i] instanceof JSeparator)&&
	 ((i<2)||(c[i-2] instanceof JSeparator)))
	remove(c[i]);

    if((l>=2)&&(c[l-2] instanceof JSeparator))
      remove(c[l-2]);
  }

  // Tool Button

  public BuToolButton addToolButton
    (String _tip, String _cmd, boolean _enabled)
  {
    BuIcon icon=BuResource.BU.loadToolCommandIcon(_cmd);
    return addToolButton(_tip,_cmd,icon,_enabled);
  }

  public BuToolButton addToolButton
    (String _text, String _tip, String _cmd, boolean _enabled)
  {
    BuIcon icon=BuResource.BU.loadToolCommandIcon(_cmd);
    return addToolButton(_text,_tip,_cmd,icon,_enabled);
  }

  public BuToolButton addToolButton
    (String _tip, String _cmd, BuIcon _icon, boolean _enabled)
  {
    String s=_tip;
    if(s.endsWith("...")) s=s.substring(0,s.length()-3);
    //if(s.length()>9) s="...";
    return addToolButton(s,_tip,_cmd,_icon,_enabled);
  }

  public BuToolButton addToolButton
    (String _text, String _tip, String _cmd, BuIcon _icon, boolean _enabled)
  {
    BuToolButton r=new BuToolButton();
    r.setName("tb"+_cmd);
    r.setActionCommand(_cmd);
    r.setText(_text);
    r.setIcon(_icon);
    r.setToolTipText(_tip);
    r.setRequestFocusEnabled(false);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setMargin(BuInsets.INSETS1111);

    add(r);

    r.setRolloverEnabled(true);
    return r;
  }

  // Tool Button

  public BuToolToggleButton addToolToggleButton
    (String _tip, String _cmd, boolean _enabled, boolean _selected)
  {
    BuIcon icon=BuResource.BU.loadToolCommandIcon(_cmd);
    return addToolToggleButton(_tip,_cmd,icon,_enabled,_selected);
  }

  public BuToolToggleButton addToolToggleButton
    (String _text, String _tip, String _cmd,
     boolean _enabled, boolean _selected)
  {
    BuIcon icon=BuResource.BU.loadToolCommandIcon(_cmd);
    return addToolToggleButton(_text,_tip,_cmd,icon,_enabled,_selected);
  }

  public BuToolToggleButton addToolToggleButton
    (String _tip, String _cmd, BuIcon _icon,
     boolean _enabled, boolean _selected)
  {
    String s=_tip;
    if(s.endsWith("...")) s=s.substring(0,s.length()-3);
    //if(s.length()>9) s="...";
    return addToolToggleButton(s,_tip,_cmd,_icon,_enabled,_selected);
  }

  public BuToolToggleButton addToolToggleButton
    (String _text, String _tip, String _cmd, BuIcon _icon,
     boolean _enabled, boolean _selected)
  {
    BuToolToggleButton r=new BuToolToggleButton();
    r.setName("tb"+_cmd);
    r.setActionCommand(_cmd);
    r.setText(_text);
    r.setIcon(_icon);
    r.setToolTipText(_tip);
    r.setSelected(_selected);
    r.setRequestFocusEnabled(false);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setMargin(BuInsets.INSETS1111);

    add(r);

    r.setRolloverEnabled(true);
    return r;
  }



  // Translation

  protected String getString(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  protected static final String __(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  // Paint


  @Override
  public void paintComponent(Graphics _g)
  {
    BuBackgroundPainter bp=(BuBackgroundPainter)
                           getClientProperty("BACKGROUND_PAINTER");
    if(bp!=null)
      bp.paintBackground(this,_g);

    super.paintComponent(_g);

    /*
    Insets insets=getInsets();
    Dimension size=getSize();
    Color c=UIManager.getColor("Desktop.background");

    if(isHorizontal())
    {
      if(isTooBigX())
      {
	int x,y;
	boolean b=(dx_==ML);
	
	y=(size.height-6)/2;

	x=insets.left+1;
	_g.translate(x,y);
	BuLib.setColor(_g,Color.white);
	if(b) _g.fillPolygon(hrax,hray,3);
	else  _g.fillPolygon(hlax,hlay,3);
	BuLib.setColor(_g,Color.black);
	if(b) _g.drawPolygon(hrax,hray,3);
	else  _g.drawPolygon(hlax,hlay,3);
	_g.translate(-x,-y);

	x=size.width-insets.right-MR+1;
	_g.translate(x,y);
	BuLib.setColor(_g,Color.white);
	if(b) _g.fillPolygon(hrax,hray,3);
	else  _g.fillPolygon(hlax,hlay,3);
	BuLib.setColor(_g,Color.black);
	if(b) _g.drawPolygon(hrax,hray,3);
	else  _g.drawPolygon(hlax,hlay,3);
	_g.translate(-x,-y);
      }
    }
    else
    {
      if(isTooBigY())
      {
	int x,y;
	boolean b=(dy_==ML);

	x=(size.width-6)/2;

	y=insets.top+1;
	_g.translate(x,y);
	BuLib.setColor(_g,Color.white);
	if(b) _g.fillPolygon(vbax,vbay,3);
	else  _g.fillPolygon(vtax,vtay,3);
	BuLib.setColor(_g,Color.black);
	if(b) _g.drawPolygon(vbax,vbay,3);
	else  _g.drawPolygon(vtax,vtay,3);
	_g.translate(-x,-y);

	y=size.height-insets.bottom-5-1;
	_g.translate(x,y);
	BuLib.setColor(_g,Color.white);
	if(b) _g.fillPolygon(vbax,vbay,3);
	else  _g.fillPolygon(vtax,vtay,3);
	BuLib.setColor(_g,Color.black);
	if(b) _g.drawPolygon(vbax,vbay,3);
	else  _g.drawPolygon(vtax,vtay,3);
	_g.translate(-x,-y);
      }
    }
    */
  }

  /*
  public void paintChildren(Graphics _g)
  {
    Insets    insets=getInsets();
    Dimension size  =getSize();
    Shape     old   =_g.getClip();

    _g.clipRect(insets.left,insets.top,size.width-insets.left-insets.right,
		size.height-insets.top-insets.bottom);
    super.paintChildren(_g);
    _g.setClip(old);
  }
  */

  /*
  private final boolean isHorizontal()
  {
    return (getWidth()>getHeight());
  }

  private final boolean isTooBigX()
  {
    return ((getWidth()-getPreferredSize().width)<0);
  }

  private final boolean isTooBigY()
  {
    return ((getHeight()-getPreferredSize().height)<0);
  }

  public void setBounds(int _x,int _y,int _w,int _h)
  {
    if((_w!=getWidth())||(_h!=getHeight()))
    {
      dx_=ML;
      dy_=ML;
    }
      
    super.setBounds(_x,_y,_w,_h);
  }
  */

  /*
  protected void addImpl(Component _c, Object _constraints, int _index)
  {
    if(_c instanceof JComponent)
      ((JComponent)_c).setMaximumSize(_c.getPreferredSize());
    super.addImpl(_c,_constraints,_index);
  }
  */

  @Override
  public void doLayout()
  {
    //synchronized (getTreeLock())
    {
      final Component[] c=getComponents();
      for(int i=0;i<c.length;i++)
        if(c[i] instanceof JComponent)
          ((JComponent)c[i]).setMaximumSize(c[i].getPreferredSize());
    }
    super.doLayout();
  }

  /*
  public synchronized void doLayout()
  {
    super.doLayout();

    Insets      b=getInsets();
    Dimension   s=getSize();
    boolean     h=isHorizontal();
    Component[] c=getComponents();

    if( h&&!isTooBigX()) dx_=ML;
    if(!h&&!isTooBigY()) dy_=ML;

    for(int i=0;i<c.length;i++)
    {
      Rectangle r=c[i].getBounds();
      if(h)
      {
	r.x+=dx_;
	if(r.x<b.left+ML) r.x=-r.width;
	if(r.x+r.width>s.width-b.right-MR) r.x=s.width;
      }
      else
      {
	r.y+=dy_;
	if(r.y<b.top+ML) r.y=-r.height;
	if(r.y+r.height>s.height-b.bottom-MR) r.y=s.height;
      }
      c[i].setBounds(r);
    }
    //System.err.println("LAYOUT:"+dx_+","+dy_);
  }
  */

  // Barre standarde

  private static final boolean isThere(String _s, boolean _d)
  {
    return BuPreferences.BU.getBooleanProperty("command."+_s,_d);
  }

  public static BuToolBar buildBasicToolBar()
  {
    BuToolBar r=new BuToolBar();

    String[][] codes=BuCommandPreferencesPanel.CODES;
    for(int i=0;i<codes.length;i++)
    {
      if(isThere(codes[i][0],"true".equals(codes[i][1])))
      {
	if("-".equals(codes[i][2]))
	  r.addSeparator();
	else
	  r.addToolButton(__(codes[i][2]),__(codes[i][3]),
			  codes[i][4],"true".equals(codes[i][5]));
      }
    }

    return r;
  }

  // Action

  private Vector actionListeners_=new Vector();

  public void addActionListener(ActionListener _l)
  { actionListeners_.addElement(_l); }

  public void removeActionListener(ActionListener _l)
  { actionListeners_.removeElement(_l); }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    //JComponent source=(JComponent)_evt.getSource();
    // System.err.println(""+getClass()+": "+source);

    for(Enumeration e=actionListeners_.elements(); e.hasMoreElements(); )
      ((ActionListener)e.nextElement()).actionPerformed(_evt);
  }
}


