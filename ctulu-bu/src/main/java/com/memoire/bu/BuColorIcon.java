/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuColorIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * A simple icon which displays a rectangle filled with
 * a given color.
 */
public class BuColorIcon
  implements Icon
{
  private Color color_;
  private Dimension size_;

  public BuColorIcon(Color _color)
  {
    this(_color,null);
  }

  public BuColorIcon(Color _color, Dimension _size)
  {
    super();
    color_=_color;
    size_ =_size;
  }

  @Override
  public int getIconHeight()
  {
    return (size_==null ? BuResource.BU.getDefaultMenuSize() : size_.height);
  }

  @Override
  public int getIconWidth()
  {
    return (size_==null ? BuResource.BU.getDefaultMenuSize() : size_.width);
  } 
                
  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    if(color_!=null)
    {
      int w=getIconWidth();
      int h=getIconHeight();

      Color old=_g.getColor();
      BuLib.setColor(_g,color_);
      _g.fillRect(_x+3,_y+3,w-6,h-6);

      Color bg=UIManager.getColor("MenuItem.background");
      bg=BuLib.getColor(bg);
      if(!BuLib.getColor(Color.white).equals(bg))
      {
        _g.draw3DRect(_x+2,_y+2,w-5,h-5,true );
        _g.setColor(bg);
        _g.draw3DRect(_x+1,_y+1,w-3,h-3,false);
      }
      else
      {
	_g.setColor(bg);
        _g.drawRect(_x+1,_y+1,w-3,h-3);
	BuLib.setColor(_g,Color.black);
        _g.drawRect(_x+2,_y+2,w-5,h-5);
      }

      _g.setColor(old);
    }
  }
}
