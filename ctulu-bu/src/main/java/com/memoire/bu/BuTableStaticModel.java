/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuTableStaticModel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import javax.swing.table.AbstractTableModel;

/**
 * A simple static model.
 * @see javax.swing.table.AbstractTableModel
 */
public class BuTableStaticModel
  extends AbstractTableModel
{
  protected Object[][] data_;
  protected Object[]   names_;
  protected boolean    editable_;
  protected int        rowcount_;

  public BuTableStaticModel(Object[][] _data, Object[] _names)
  {
    this(_data,_names,false);
  }

  public BuTableStaticModel(Object[][] _data, Object[] _names,
			    boolean _editable)
  {
    data_    =_data;
    names_   =_names;
    editable_=_editable;
    rowcount_=-1;
  }

  @Override
  public String getColumnName(int _col)
  {
    return names_[_col].toString();
  }

  public void setRowCount(int _rowcount)
  {
    rowcount_=_rowcount;
  }

  @Override
  public int getRowCount()
  {
    int r=data_.length;
    if((rowcount_>=0)&&(rowcount_<r)) r=rowcount_;
    return r;
  }

  @Override
  public int getColumnCount()
  {
    return names_.length;
  }

  @Override
  public Object getValueAt(int _row, int _col)
  {
    return data_[_row][_col];
  }

  @Override
  public boolean isCellEditable(int _row, int _col)
  {
    return editable_;
  }

  @Override
  public void setValueAt(Object _value, int _row, int _col)
  {
    data_[_row][_col]=_value;
    fireTableCellUpdated(_row,_col);
  }
}
