/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuVerticalLayout.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import java.io.Serializable;

/**
 * Lays out vertically a set of components.
 */

public class BuVerticalLayout
    implements LayoutManager2, Serializable
{
  // Vector components=new Vector(0,1);

  public BuVerticalLayout()
  {
    this(0,true,false);
  }

  public BuVerticalLayout(int _vgap)
  {
    this(_vgap,true,false);
  }

  public BuVerticalLayout(int _vgap, boolean _hfilled, boolean _vfilled)
  {
    vgap_=_vgap;
    hfilled_=_hfilled;
    vfilled_=_vfilled;
  }

  protected int vgap_;
  public int getVgap() { return vgap_; }
  public void setVgap(int _vgap) { vgap_=_vgap; }

  protected boolean hfilled_;
  public boolean getHfilled() { return hfilled_; }
  public void setHfilled(boolean _hfilled) { hfilled_=_hfilled; }

  protected boolean vfilled_;
  public boolean getVfilled() { return vfilled_; }
  public void setVfilled(boolean _vfilled) { vfilled_=_vfilled; }

  @Override
  public void addLayoutComponent(Component _comp, Object _cstr)
  {
    // components.addElement(_comp);
  }

  @Override
  public void addLayoutComponent(String _name, Component _comp)
  {
    // components.addElement(_comp);
  }

  @Override
  public void removeLayoutComponent(Component _comp)
  {
    // components.removeElement(_comp);
  }

  @Override
  public Dimension minimumLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      // int     nbc=components.size();
      int nbc=_parent.getComponentCount();
      int     w  =0;
      int     h  =0;
      boolean p  =true;

      for(int i=0;i<nbc;i++)
      {
	// Component c=(Component)components.elementAt(i);
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getMinimumSize();
	  w=Math.max(w,d.width);
	  h+=d.height;
	  if(!p) h+=vgap_;
	  p=false;
	}
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension preferredLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      // int     nbc=components.size();
      int nbc=_parent.getComponentCount();
      int     w  =0;
      int     h  =0;
      boolean p  =true;

      for(int i=0;i<nbc;i++)
      {
	// Component c=(Component)components.elementAt(i);
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getPreferredSize();
	  w=Math.max(w,d.width);
	  h+=d.height;
	  if(!p) h+=vgap_;
	  p=false;
	}
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension maximumLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      // int     nbc=components.size();
      int nbc=_parent.getComponentCount();
      int w  =Short.MAX_VALUE;
      int h  =0;
      boolean p=true;

      for(int i=0;i<nbc;i++)
      {
	// Component c=(Component)components.elementAt(i);
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getMaximumSize();
	  w=Math.min(w,d.width);
	  h+=d.height;
	  if(!p) h+=vgap_;
	  p=false;
	}
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public float getLayoutAlignmentX(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public float getLayoutAlignmentY(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public void invalidateLayout(Container _parent)
  {
  }

  @Override
  public void layoutContainer(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      Insets insets = _parent.getInsets();
      Rectangle bounds = new Rectangle
	(insets.left,
	 insets.top,
	 _parent.getSize().width  - (insets.left + insets.right),
	 _parent.getSize().height - (insets.top + insets.bottom));
      
      // int     nbc=components.size();
      int     nbc=_parent.getComponentCount();
      int     x  =0;
      int     y  =0;
      boolean p  =true;

      for(int i=0;i<nbc;i++)
      {
	// Component c=(Component)components.elementAt(i);
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getPreferredSize();

	  if(!p) y+=vgap_;
	  p=false;

	  if(x>bounds.width) x=bounds.width;
	  if(y>bounds.height) y=bounds.height;

	  Rectangle actual=new Rectangle
	    (bounds.x+x,
	     bounds.y+y,
	     (hfilled_
	      ? bounds.width-x 
	      : Math.min(bounds.width-x,d.width)),
	     (vfilled_ && (i==nbc-1)
	      ? bounds.height-y
	      : Math.min(bounds.height-y,d.height)));
	  c.setBounds(actual.x,actual.y,actual.width,actual.height);
	  
	  y+=d.height;
	}
      }
    }
  }
}
