package com.memoire.bu;

import java.awt.Component;
import javax.swing.AbstractButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToolBar;
import javax.swing.MenuElement;

/**
 * An utility class to modify controls by their command name.
 * The command name is a string defined for each control
 * using setActionCommand().
 * 
 * @author bmarchan
 * @version $Id$
 */
public class BuActionModifier{

//--------------------------------------------------------------------------
// setTooltipForAction : Permet de d�finir un tooltip sur les 
// menus, ou sur les boutons.
//--------------------------------------------------------------------------

  public static void setTooltipForAction
    (JMenuBar _bar, String _action, String _tp)
  {
    if(_bar==null) return;

    MenuElement[] c=_bar.getSubElements();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JMenu)
	setTooltipForAction((JMenu)c[i],_action,_tp);
  }

  public static void setTooltipForAction
    (JMenu _menu, String _action, String _tp)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
	setTooltipForAction((JMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JPopupMenu)
	setTooltipForAction((JPopupMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JMenuItem)
	setTooltipForAction((JMenuItem)c[i],_action,_tp);
      // else System.err.println("??? "+c[i]);
    }
  }

  public static void setTooltipForAction
    (JPopupMenu _menu, String _action, String _tp)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
	setTooltipForAction((JMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JPopupMenu)
	setTooltipForAction((JPopupMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JMenuItem)
	setTooltipForAction((JMenuItem)c[i],_action,_tp);
    }
  }

  public static void setTooltipForAction
    (JCheckBoxMenuItem _item, String _action, String _tp)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setToolTipText(_tp);
  }

  public static void setTooltipForAction
    (JRadioButtonMenuItem _item, String _action, String _tp)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setToolTipText(_tp);
  }

  // Tool

  public static void setTooltipForAction
    (JToolBar _bar, String _action, String _tp)
  {
    if(_bar==null) return;

    Component[] c=_bar.getComponents();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof AbstractButton)
	setTooltipForAction((AbstractButton)c[i],_action,_tp);
  }

  public static void setTooltipForAction
    (AbstractButton _item, String _action, String _tp)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setToolTipText(_tp);
  }

//--------------------------------------------------------------------------
// setDynamicTextForAction : Permet de d�finir un texte dynamique sur les 
// menus, un tooltip dynamique pour les boutons. Exemple : undo/redo.
//--------------------------------------------------------------------------

  public static void setDynamicTextForAction
    (JMenuBar _bar, String _action, String _tp)
  {
    if(_bar==null) return;

    MenuElement[] c=_bar.getSubElements();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JMenu)
        setDynamicTextForAction((JMenu)c[i],_action,_tp);
  }

  public static void setDynamicTextForAction
    (JMenu _menu, String _action, String _tp)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
        setDynamicTextForAction((JMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JPopupMenu)
        setDynamicTextForAction((JPopupMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JMenuItem)
        setDynamicTextForAction((JMenuItem)c[i],_action,_tp);
      // else System.err.println("??? "+c[i]);
    }
  }

  public static void setDynamicTextForAction
    (JPopupMenu _menu, String _action, String _tp)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
        setDynamicTextForAction((JMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JPopupMenu)
        setDynamicTextForAction((JPopupMenu)c[i],_action,_tp);
      else
      if(c[i] instanceof JMenuItem)
        setDynamicTextForAction((JMenuItem)c[i],_action,_tp);
      else
        if(c[i] instanceof AbstractButton)
          setDynamicTextForAction((AbstractButton)c[i],_action,_tp);
    }
  }

  public static void setDynamicTextForAction
    (JMenuItem _item, String _action, String _tp)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setText(_tp);
  }

  // Tool

  public static void setDynamicTextForAction
    (JToolBar _bar, String _action, String _tp)
  {
    if(_bar==null) return;

    Component[] c=_bar.getComponents();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof AbstractButton)
        setDynamicTextForAction((AbstractButton)c[i],_action,_tp);
  }

  public static void setDynamicTextForAction
    (AbstractButton _item, String _action, String _tp)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setText(_tp);
  }
}
