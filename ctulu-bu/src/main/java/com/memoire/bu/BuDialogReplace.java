/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuDialogReplace.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A standard dialog to replace, with history.
 */

public class BuDialogReplace
  extends BuDialog
  implements ActionListener
{
  protected BuButton                 btRemplacer_;
  protected BuButton                 btFermer_;
  protected BuTextComponentInterface tcTexte_;
  protected BuTextField              tfMotif_;
  protected BuTextField              tfCible_;
  protected BuCheckBox               cbCasse_;

  public BuDialogReplace(BuCommonInterface        _parent,
			 BuInformationsSoftware   _isoft,
			 BuTextComponentInterface _texte)
  {
    super(_parent,_isoft,__("Remplacement"));

    tcTexte_=_texte;

    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btRemplacer_=new BuButton(BuResource.BU.loadButtonCommandIcon("REMPLACER"),
                              getString("Remplacer"));
    btRemplacer_.addActionListener(this);
    getRootPane().setDefaultButton(btRemplacer_);
    pnb.add(btRemplacer_);

    btFermer_=new BuButton(BuResource.BU.loadButtonCommandIcon("QUITTER"),
                           getString("Fermer"));
    btFermer_.addActionListener(this);
    pnb.add(btFermer_);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  @Override
  public JComponent getComponent()
  {
    tfMotif_=new BuTextField();
    tfMotif_.setColumns(20);

    tfCible_=new BuTextField();
    tfCible_.setColumns(20);
    
    cbCasse_=new BuCheckBox(getString("Distinction majuscules/minuscules"));
    
    BuPanel p=new BuPanel();
    p.setLayout(new BuVerticalLayout());
    p.setBorder(EMPTY5555);
    p.add(new BuLabel(getString("Texte � rechercher:")));
    p.add(tfMotif_);
    p.add(new BuLabel(getString("Remplacer par:")));
    p.add(tfCible_);
    p.add(cbCasse_);
    p.add(new BuPanel());
    return p;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialogReplace : "+source);

    if(source==btRemplacer_)
    {
      reponse_=JOptionPane.OK_OPTION;

      int     pos  =tcTexte_.getCaretPosition();
      String  motif=tfMotif_.getText();
      String  cible=tfCible_.getText();
      boolean casse=cbCasse_.isSelected();

      if(pos==tcTexte_.getLength()) pos=0;
      if(  (!tcTexte_.replace(motif,cible,pos,tcTexte_.getLength(),!casse))
	 &&(pos>0))
	tcTexte_.replace(motif,cible,0,tcTexte_.getLength(),!casse);
	
      // setVisible(false);
    }

    if(source==btFermer_)
    {
      reponse_=JOptionPane.CANCEL_OPTION;
      setVisible(false);
    }
  }
}

