/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuMonitorCpu.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;

/**
 * A monitor to watch cpu usage.
 */

public class BuMonitorCpu
       extends BuMonitorAbstract
{
  protected int     value_;
  protected int     minimum_;
  protected int     maximum_;
  protected long    v_,vmin_,vmax_;

  public BuMonitorCpu()
  {
    super();

    value_  =0;
    minimum_=0;
    maximum_=0;
    v_      =0L;
    vmin_   =0L;
    vmax_   =0L;

    setName("buMONITOR_CPU");
    setForeground(new Color(32,64,96));
  }

  @Override
  public int getValue()
  { return value_; }

  @Override
  public int getMaximum()
  { return maximum_; }

  @Override
  public String getText()
  { return ""+value_+"%"; }

  @Override
  public String getToolTipText()
  { return "Cpu utilis�: "+value_+"%"; }

  @Override
  public long getDelay()
  { return 1000L; }

  @Override
  public synchronized boolean refreshState()
  {
    boolean r=false;

    long avant=System.currentTimeMillis();
    double z=0.;
    for(int i=0;i<100;i++)
    {
      z+=Math.cos(z*i);
      Thread.yield();
      try { Thread.sleep(10); }
      catch(Exception ex) { }
    }
    long apres=System.currentTimeMillis();

    v_=apres-avant;
    if(v_>vmax_) vmax_=v_+1;
    if(v_<vmin_) vmin_=v_;

    int p=(int)(100L*(v_-vmin_)/(vmax_-vmin_));
    if(p!=value_)
    {
      r=true;
      value_=p;
      if(value_>maximum_) maximum_=value_;
    }

    if(Math.random()<0.1) vmin_++;
    if(v_<vmin_) vmin_=v_;

    if(Math.random()<0.1) vmax_--;
    if(v_>vmax_) vmax_=v_;
    if(10>vmax_) vmax_=10;      

    return r;
  }
}
