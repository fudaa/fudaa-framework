/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuPreviewJob.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.PrintJob;
import java.util.Vector;

public class BuPreviewJob
       extends PrintJob
{
  private Frame   parent_;
  //private String  name_;
  private Vector  images_;
  private Image[] result_; 

  public BuPreviewJob(Frame _parent, String _name)
  {
    parent_=_parent;
   // name_  =_name;
    images_=new Vector(1,5);
    result_=null;
  }

  private Image reduce(Image _image)
  {
    Dimension size =getPageDimension();
    size.width /=3;
    size.height/=3;

    Image    r=parent_.createImage(size.width,size.height);
    Graphics g=r.getGraphics();
    g.setClip(0, 0,size.width,size.height);
    g.setColor(Color.white);
    g.fillRect(0, 0,size.width,size.height);
    g.setColor(Color.black);
    g.drawImage(_image.getScaledInstance
		(size.width,size.height,Image.SCALE_SMOOTH),
		0,0,parent_);
    return r;
  }

  @Override
  public Dimension getPageDimension()
  { return new Dimension(595,842); }

  @Override
  public int getPageResolution()
  { return 72; }

  @Override
  public boolean lastPageFirst()
  { return false; }

  @Override
  public Graphics getGraphics()
  {
    Dimension size;
    Image     image;

    // Reduce the previous page
    int l=images_.size();
    if(l>0)
    {
      image=(Image)images_.elementAt(l-1);
      images_.setElementAt(reduce(image),l-1);
    }

    /*
    if(app_!=null)
      app_.getMainPanel().getStatusBar()
	.setMessage("Preview: printing page "+images_.size());
    */

    // Create a new page
    size=getPageDimension();
    image=parent_.createImage(size.width,size.height);
    images_.addElement(image);

    Graphics r=image.getGraphics();
    r.setClip(0, 0,size.width,size.height);
    r.setColor(Color.white);
    r.fillRect(0, 0,size.width,size.height);
    r.setColor(Color.black);
    return r;
  }

  @Override
  public synchronized void end()
  {
    if(result_!=null) return;

    // Reduce the last page
    int l=images_.size();
    if(l>0)
    {
      Image image=(Image)images_.elementAt(l-1);
      images_.setElementAt(reduce(image),l-1);
    }

    result_=new Image[l];
    for(int i=0;i<l;i++)
      result_[i]=(Image)images_.elementAt(i);

    images_=null;

    //result_=new BuPreviewFrame(p);

    /*
    for(int p=0;p<images_.size(); p++)
    {
      Image image=(Image)images_.elementAt(p);

      Dimension size=getPageDimension();
      size.width /=3;
      size.height/=3;
      Image reduite=parent_.createImage(size.width,size.height);
      Graphics g=reduite.getGraphics();
      g.setClip(0, 0,size.width,size.height);
      g.setColor(Color.white);
      g.fillRect(0, 0,size.width,size.height);
      g.setColor(Color.black);
      g.drawImage(image.getScaledInstance
		  (size.width,size.height,Image.SCALE_SMOOTH),
		  0,0,parent_);
      result_.addTab(p+1,reduite);
      images_.setElementAt(null,p);
    }
    */
  }

  public Image[] getImages()//BuPreviewFrame getPreviewFrame()
  {
    return result_;
  }
}
