/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut unstable
 * @file BuTextField.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import com.memoire.fu.FuFactoryInteger;
import com.memoire.fu.FuFactoryLong;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DropTarget;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Date;
import java.util.TooManyListenersException;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

/**
 * An extended text field which takes validators.
 */
public class BuTextField extends JTextField implements DtDragSensible {

  private Color textFg_;
  private Color errorFg_;
  private Format displayFormat_;
  private boolean focusSelection_;
  private String realText_;
  private String previousText_;
  private boolean selectAllIsGainFocus;
  private BuCharValidator charValidator_;
  private BuStringValidator stringValidator_;
  private BuValueValidator valueValidator_;

  public BuTextField() {
    this(null, -1);
  }

  public void setSelectAllIsGainFocus(boolean selectAllIsGainFocus) {
    this.selectAllIsGainFocus = selectAllIsGainFocus;
    focusSelection_ = selectAllIsGainFocus;
  }

  public boolean isSelectAllIsGainFocus() {
    return selectAllIsGainFocus;
  }

  public BuTextField(int _cols) {
    this(null, _cols);
  }

  public BuTextField(String _text) {
    this(_text, -1);
  }

  public BuTextField(String _text, int _cols) {
    super();

    // Avoid using the text as a command
    setActionCommand("TEXT_CHANGED");

    if (_cols > 0) {
      setColumns(_cols);
    }

    setText(_text == null ? "" : _text);

    errorFg_ = UIManager.getColor("TextField.errorForeground");
    if (errorFg_ == null) {
      errorFg_ = BuLib.getColor(Color.red);
    }
  }

  // Property Display Format
  public Format getDisplayFormat() {
    return displayFormat_;
  }

  public void setDisplayFormat(Format _format) {
    if (_format == null) {
      realText_ = null;
    }
    displayFormat_ = _format;
  }

  // Property Selection Mode
  public boolean isFocusSelection() {
    return focusSelection_;
  }

  public void setFocusSelection(boolean _focusSelection) {
    focusSelection_ = _focusSelection;
  }

  // Property Text
  @Override
  public String getText() {
    String r;
    if ((displayFormat_ != null) && !hasFocus()) {
      r = realText_;
    } else {
      r = super.getText();
    }
    return r;
  }

  public void setAcceptEmptyValue() {
    if (valueValidator_ != null) {
      valueValidator_ = new BuValueValidator.EmptyValueValidator(valueValidator_);
    }
    if (stringValidator_ != null) {
      stringValidator_ = BuStringValidator.getEmptyEnable(stringValidator_);
    }
  }

  @Override
  public void setText(String _text) {
    boolean ok = true;

    String s = _text;
    if (!isStringValid(s)) {
      ok = false;
    } else if (!isValueValid(stringToValue(s))) {
      ok = false;
    }

    if (ok && !hasFocus()) {
      Object v = stringToValue(s);

      s = valueToString(v);

      if (displayFormat_ != null) {
        realText_ = s;
        s = displayFormat_.format(v);
      }
    }

    if (!ok) {
      super.setForeground(errorFg_);
    } else {
      super.setForeground(textFg_);
    }
    super.setText(s);
    if (!isEditable()) {
      setCaretPosition(0);
    }
  }

  // Properties Validators
  public BuCharValidator getCharValidator() {
    return charValidator_;
  }

  /**
   * Ce validator est sert � accepter ou non un caract�re saisi. Il est consult� � chaque caract�re saisi.
   *
   * @param _validator Le validator.
   */
  public void setCharValidator(BuCharValidator _validator) {
    charValidator_ = _validator;
  }

  public BuStringValidator getStringValidator() {
    return stringValidator_;
  }

  /**
   * Ce validator sert � accepter ou non le format de la chaine saisie. Il est consult� lors de la perte de focus.
   *
   * @param _validator Le validator
   */
  public void setStringValidator(BuStringValidator _validator) {
    stringValidator_ = _validator;
  }

  public BuValueValidator getValueValidator() {
    return valueValidator_;
  }

  /**
   * Ce validator sert � accepter la valeur de la chaine saisie. Il peut limiter la plage de valeurs. Il est consult� lors de la perte de focus.
   *
   * @param _validator Le validator
   */
  public void setValueValidator(BuValueValidator _validator) {
    valueValidator_ = _validator;
  }

  @Override
  public void setEditable(boolean _editable) {
    if (_editable == isEditable()) {
      return;
    }
    super.setEditable(_editable);
    if (!isEditable()) {
      setCaretPosition(0);
    }
  }

  @Override
  public void setForeground(Color _c) {
    textFg_ = _c;
    super.setForeground(_c);
  }

  public void setErrorForeground(Color _c) {
    errorFg_ = _c;
  }

  @Override
  public void replaceSelection(String _s) {
    boolean b = true;

    for (int i = 0; i < _s.length(); i++) {
      b &= isCharValid(_s.charAt(i));
    }

    if (b) {
      super.replaceSelection(_s);
    } else {
      getToolkit().beep();
    }
  }

  @Override
  protected void processKeyEvent(KeyEvent _evt) {
    if (_evt.getID() == KeyEvent.KEY_TYPED) {
      if (!isCharValid(_evt.getKeyChar())) {
        return;
      }
    }

    super.setForeground(textFg_);
    super.processKeyEvent(_evt);
  }

  @Override
  protected void processFocusEvent(FocusEvent _evt) {
    boolean ok = true;
    if (_evt.getID() == FocusEvent.FOCUS_LOST) {
      String s = super.getText();
      if (!isStringValid(s)) {
        ok = false;
      } else if (!isValueValid(stringToValue(s))) {
        ok = false;
      }

      if (displayFormat_ != null) {
        realText_ = s;
      }

      if (ok) {
        Object v = stringToValue(s);

        s = valueToString(v);

        if (displayFormat_ != null) {
          realText_ = s;
          s = displayFormat_.format(v);
        }

        if (s != null && !s.equals(super.getText())) {
          super.setText(s);
        }

        if (((previousText_ == null) && (s != null)) || ((previousText_ != null) && !previousText_.equals(s))) {
          fireActionPerformed();
        }
      } else {
        getToolkit().beep();
      }
    }

    if (_evt.getID() == FocusEvent.FOCUS_GAINED) {
      ok = true;
      if (selectAllIsGainFocus) {
        selectAll();
      }
      String s = getText();
      int p = -1;

      if (isStringValid(s)) {
        Object v = stringToValue(s);
        if (isValueValid(v)) {
          p = getCaretPosition();
          s = valueToString(v);
          previousText_ = s;
          if (!s.equals(super.getText())) {
            super.setText(s);
          }
        }
      }

      if (isFocusSelection()) {
        setSelectionStart(0);
        setSelectionEnd(s.length());
      } else {
        if (p >= 0) {
          setCaretPosition(Math.max(0, Math.min(p, s.length())));
        }
      }

      Container parent = getParent();
      if (parent instanceof JComponent) {
        Rectangle r = getBounds();
        r.x = Math.max(0, r.x - 200);
        ((JComponent) parent).scrollRectToVisible(r);
        r = getBounds();
        ((JComponent) parent).scrollRectToVisible(r);
      }

    }

    if (!ok) {
      super.setForeground(errorFg_);
    } else {
      super.setForeground(textFg_);
    }

    repaint();
    super.processFocusEvent(_evt);
  }

  public Object stringToValue(String _string) {
    Object r = _string;
    if ((r != null) && (stringValidator_ != null)) {
      if (isStringValid(_string)) {
        r = stringValidator_.stringToValue(_string);
        if (!isValueValid(r)) {
          r = null;
        }
      } else {
        r = null;
      }
    }
    return r;
  }

  public String valueToString(Object _value) {
    String r = null;

    if (isValueValid(_value)) {
      if (stringValidator_ != null) {
        r = stringValidator_.valueToString(_value);
      }
    }

    if (_value == null) {
      r = "";
    } else if (r == null) {
      r = "" + _value;
    }
    return r;
  }

  private boolean isCharValid(char _char) {
    boolean r = true;

    if (_char == 8) {
      r = true;
    } else if (_char == 255) {
      r = false;
    } else if (charValidator_ != null) {
      r = charValidator_.isCharValid(_char);
    }

    return r;
  }

  private boolean isStringValid(String _value) {
    boolean r = true;
    if (stringValidator_ != null) {
      r = stringValidator_.isStringValid(_value);
    }
    return r;
  }

  private boolean isValueValid(Object _value) {
    boolean r = true;
    if (valueValidator_ != null) {
      r = valueValidator_.isValueValid(_value);
    }
    return r;
  }

  public Object getValue() {
    return stringToValue(getText());
  }

  public void setValue(Object _value) {
    setText(valueToString(_value));
  }

  // Anti-aliasing
  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);

    if (drop_) {
      Rectangle r = getDropRect();
      // _g.setColor(new Color(255,128,0,64));
      // _g.fillRoundRect(r.x,r.y,r.width,r.height,9,9);
      _g.setColor(new Color(255, 128, 0));
      _g.drawRoundRect(r.x, r.y, r.width - 1, r.height - 1, 9, 9);
    }
  }

  // Drop
  @Override
  public void setDropTarget(DropTarget _dt) {
    super.setDropTarget(_dt);
    if (_dt != null) {
      // FuLog.debug("BTF: setDropTarget");
      try {
        _dt.removeDropTargetListener(DtDragSensible.SINGLETON);
      } catch (IllegalArgumentException ex) {
      }
      try {
        _dt.addDropTargetListener(DtDragSensible.SINGLETON);
      } catch (TooManyListenersException ex) {
      }
    }
  }
  protected boolean drop_;

  protected Rectangle getDropRect() {
    Rectangle r = getVisibleRect();
    /*
     * Insets i=getInsets(); r.x +=3; r.y +=3; r.width -=6; r.height-=6;
     */
    return r;
  }

  @Override
  public void dragEnter(boolean _acceptable, Point _location) {
    dragOver(_acceptable, _location);
  }

  @Override
  public void dragOver(boolean _acceptable, Point _location) {
    if (!drop_ && _acceptable) {
      drop_ = true;
      repaint();
    }
  }

  @Override
  public void dragExit() {
    if (drop_) {
      drop_ = false;
      repaint();
    }
  }

  // Factory
  public static BuTextField createIntegerField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.INTEGER);
    r.setStringValidator(BuStringValidator.INTEGER);
    r.setValueValidator(BuValueValidator.INTEGER);
    return r;
  }

  public static BuTextField createLongField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.LONG);
    r.setStringValidator(BuStringValidator.LONG);
    r.setValueValidator(BuValueValidator.LONG);
    return r;
  }

  public static BuTextField createFloatField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.FLOAT);
    r.setStringValidator(BuStringValidator.FLOAT);
    r.setValueValidator(BuValueValidator.FLOAT);
    return r;
  }

  public static BuTextField createDoubleField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.DOUBLE);
    r.setStringValidator(BuStringValidator.DOUBLE);
    r.setValueValidator(BuValueValidator.DOUBLE);
    return r;
  }

  public static BuTextField createHostField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.HOST);
    r.setStringValidator(BuStringValidator.HOST);
    r.setValueValidator(BuValueValidator.HOST);
    return r;
  }

  public static BuTextField createPortField() {
    BuTextField r = new BuTextField(5);
    r.setCharValidator(BuCharValidator.INTEGER);
    r.setStringValidator(BuStringValidator.INTEGER);
    r.setValueValidator(BuValueValidator.MINMAX(1, 65535));
    return r;
  }

  public static BuTextField createIdField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.ID);
    r.setStringValidator(BuStringValidator.ID);
    r.setValueValidator(BuValueValidator.ID);
    return r;
  }

  public static BuTextField createFileField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.FILE);
    r.setStringValidator(BuStringValidator.FILE);
    r.setValueValidator(BuValueValidator.FILE);
    return r;
  }

  public static BuTextField createDateField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.DATE);
    r.setStringValidator(BuStringValidator.DATE);
    r.setValueValidator(BuValueValidator.DATE);
    return r;
  }

  public static BuTextField createTimeField() {
    BuTextField r = new BuTextField();
    r.setCharValidator(BuCharValidator.TIME);
    r.setStringValidator(BuStringValidator.TIME);
    r.setValueValidator(BuValueValidator.TIME);
    return r;
  }

  // Test
  public static final JComponent launch(String argv[]) {
    JComponent content = new JPanel();

    BuVerticalLayout lo = new BuVerticalLayout();
    lo.setVgap(5);
    lo.setHfilled(true);
    lo.setVfilled(false);
    content.setLayout(lo);
    content.setBorder(BuBorders.EMPTY5555);

    JTextField tf0 = new JTextField("R�f�rence");

    BuTextField tf1 = createIntegerField();
    BuTextField tf2 = createDoubleField();
    BuTextField tf3 = createIdField();
    BuTextField tf4 = createDateField();
    BuTextField tf5 = createTimeField();
    BuTextField tf6 = createDoubleField();
    BuTextField tf7 = new BuTextField();

    DecimalFormat fmt = new DecimalFormat();
    fmt.setMinimumFractionDigits(2);
    fmt.setMaximumFractionDigits(2);
    // fmt.setMinimumIntegerDigits(1);
    // fmt.setMaximumIntegerDigits(6);
    tf6.setDisplayFormat(fmt);
    tf6.setValueValidator(BuValueValidator.MULTI(BuValueValidator.DOUBLE, BuValueValidator.MINMAX(0., 1.)));

    tf1.setValue(FuFactoryInteger.get(123));
    tf2.setValue(new Double(456.789));
    tf3.setValue("abc_def");
    tf4.setValue(new Date());
    tf5.setValue(FuFactoryLong.get(new Date().getTime()));
    tf6.setValue(new Double(-123456.78901));
    tf7.setValue("Standard Text Field");

    content.add(tf0);
    content.add(tf1);
    content.add(tf2);
    content.add(tf3);
    content.add(tf4);
    content.add(tf5);
    content.add(tf6);
    content.add(tf7);

    return content;
  }

  /*
   * public static final void main(String argv[]) { Locale.setDefault(Locale.FRANCE); JFrame frame =new JFrame("Test
   * BuTextField"); JComponent content=launch(argv); frame.setContentPane(content); frame.pack();//setSize(220,250);
   * frame.setLocation(200,200); frame.show(); }
   */
}
