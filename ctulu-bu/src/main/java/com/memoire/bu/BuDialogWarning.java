/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuDialogWarning.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionListener;

/**
 * A standard warning dialog (continue).
 */

public class BuDialogWarning
       extends BuDialogMessage
       implements ActionListener
{
  public BuDialogWarning(BuCommonInterface      _parent,
			 BuInformationsSoftware _isoft,
			 Object                 _message)
  {
    super(_parent,_isoft,_message);
    setTitle(__("Avertissement"));
  }
}

