/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         BuMacEnv.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuEmptyArrays;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.Hashtable;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 * An application which can contain others applications (as Mac).
 * Test.
 */
public class BuMacEnv
  extends BuApplication
  implements /*BuCommonInterface,*/ InternalFrameListener,
             /*ActionListener, */FuEmptyArrays
{
  BuMenuBar     default_mb_;
  BuToolBar     default_tb_;
  BuSpecificBar default_pb_;
  BuStatusBar   default_sb_;
  BuColumn      default_lc_,default_rc_;
  BuMenu        mac_menu_;

  // InformationsSoftware

  private static final BuInformationsSoftware isMacEnv_
    =new BuInformationsSoftware();

  static
  {
    isMacEnv_.name   ="MacEnv";
    isMacEnv_.version="0.06";
    isMacEnv_.date   ="2001-02-21";
    isMacEnv_.rights ="Guillaume Desnoix (c)1998-2001";
    isMacEnv_.contact="guillaume@desnoix.com";
    isMacEnv_.license="GPL2";
    isMacEnv_.logo   =BuResource.BU.getIcon("logo");
    isMacEnv_.banner =BuResource.BU.getIcon("banner");
    // isMacEnv_.ftp=null;
    isMacEnv_.http   ="http://www.desnoix.com/guillaume/";
    isMacEnv_.man    =null;

    isMacEnv_.authors     =new String[] { "Guillaume Desnoix" };
    isMacEnv_.contributors=null;
    isMacEnv_.testers     =null;
    isMacEnv_.libraries   =null;
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware()
  { return isMacEnv_; }

  public static BuInformationsSoftware informationsSoftware()
  { return isMacEnv_; }

  // Constructeur

  public BuMacEnv()
  {
    super();
  }

  @Override
  public void init()
  {
    super.init();  

    BuMainPanel mp=getMainPanel();

    mac_menu_=buildMacMenu();
    mac_menu_.addActionListener(this);

    BuMenuInternalFrames mw=(BuMenuInternalFrames)
      BuMenuBar.getMenu(mac_menu_,"LISTE_FENETRES");
    if(mw!=null) mw.setDesktop(mp.getDesktop());

    default_mb_=new BuMenuBar();
    default_mb_.insertMenu(mac_menu_);
    setMainMenuBar(default_mb_);

    default_tb_=new BuToolBar();
    setMainToolBar(default_tb_);

    default_pb_=mp.getSpecificBar();
    default_sb_=mp.getStatusBar();
    default_lc_=mp.getLeftColumn();
    default_rc_=mp.getRightColumn();

    default_tb_.setVisible(false);
    default_pb_.setVisible(false);
    default_sb_.setVisible(false);
    default_lc_.setVisible(false);
    default_rc_.setVisible(false);

    /*
    mp.remove(default_pb_);
    mp.remove(default_sb_);
    mp.remove(default_lc_);
    mp.remove(default_rc_);
    */

    setDefaultTitle();

    setEnabledForAction("ECHANGER_COLONNES"  ,false);
    setEnabledForAction("VISIBLE_SPECIFICBAR",false);
    setEnabledForAction("VISIBLE_LEFTCOLUMN" ,false);
    setEnabledForAction("VISIBLE_RIGHTCOLUMN",false);
    setEnabledForAction("VISIBLE_STATUSBAR"  ,false);
  }

  private void setDefaultTitle()
  {
    BuInformationsSoftware il=getInformationsSoftware();
    setTitle(il.name+" "+il.version);
  }

  // Should be in BuMacEnvImplementation
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String action=_evt.getActionCommand();

    if("EXECUTER_SUBAPP"  .equals(action))
    {
      BuSubApp subapp=new BuSubApp();
      subapp.setTitle("SubApp");
      subapp.init();
      subapp.start();
      addInternalFrame(subapp);
    }
    else
    if("EXECUTER_ALMA".equals(action))
    {
      BuSubApp subapp=null;
      try
      {
	String pkg="";
	if(BuMacEnv.class.getName().startsWith("com.memoire"))
	  pkg="com.memoire.alma.";
	Class c=Class.forName(pkg+"AlmaSubApp");

	subapp=(BuSubApp)c.newInstance();
	subapp.init();
	subapp.start();
	addInternalFrame(subapp);
      }
      catch(Exception ex)
      {
	new BuDialogError(this,
			  getInformationsSoftware(),
			  "Can not launch Alma").activate();
      }
    }
    else
    if("EXECUTER_DJA".equals(action))
    {
      // AlmaSubApp subapp=new AlmaSubApp();
      // subapp.setTitle("Alma");
      BuSubApp subapp=null;
      try
      {
	String pkg="";
	if(BuMacEnv.class.getName().startsWith("com.memoire"))
	  pkg="com.memoire.alma.";
	Class c=Class.forName(pkg+"DjaSubApp");

	subapp=(BuSubApp)c.newInstance();
	subapp.init();
	subapp.start();
	addInternalFrame(subapp);
      }
      catch(Exception ex)
      {
	new BuDialogError(this,
			  getInformationsSoftware(),
			  "Can not launch Dja").activate();
      }
    }
    else
    if("EXECUTER_FIELDS".equals(action))
    {
      /*
      JComponent pn=BuTextField.launch(STRING0);
      JInternalFrame intf=new JInternalFrame("fields");
      intf.setContentPane(pn);
      intf.setBorder(BuBorders.EMPTY0000);
      intf.pack();
      getMainPanel().addInternalFrame(intf);
      */

      /*
      JComponent pn=BuTextField.launch(STRING0);
      BuPalette intf=new BuPalette();
      intf.setTitle("fields");
      intf.setContent(pn);
      getMainPanel().addInternalFrame(intf);
      */

      JComponent pn=BuTextField.launch(STRING0);
      pn.setOpaque(false);
      pn.setBorder(BuBorders.EMPTY2222);
      pn.setSize(pn.getPreferredSize());
      pn.setLocation(100,100);
      getMainPanel().getDesktop().add(pn);
      pn.revalidate();
    }
    else
    if("QUITTER".equals(action)) System.exit(0);
    else
    getImplementation().actionPerformed(_evt);
  }

  private BuMenu buildMacMenu()
  {
    BuMenu r=new BuMenu("","CERISE");
    r.addMenuItem("SubApp","EXECUTER_SUBAPP",true);
    r.addMenuItem("Alma"  ,"EXECUTER_ALMA"  ,true);
    r.addMenuItem("Dja"   ,"EXECUTER_DJA"   ,true);
    r.addMenuItem("Fields","EXECUTER_FIELDS",true);
    r.addSeparator();
    r.addSubMenu(BuMenu.buildWindowMenu(),true);
    r.addSeparator();
    r.addMenuItem(getString("Quitter"),"QUITTER",true);
    return r;
  }

  private boolean colorized_;

  public boolean isColorized()
  { return colorized_; }

  public void setColorized(boolean _colorized)
  { colorized_=_colorized; }

  Hashtable menu_bars_ =new Hashtable();
  Hashtable tool_bars_ =new Hashtable();
  Hashtable spec_bars_ =new Hashtable();
  Hashtable stat_bars_ =new Hashtable();
  Hashtable left_cols_ =new Hashtable();
  Hashtable right_cols_=new Hashtable();
  
  private int nbf_;

  private static final Color[] COLORS=
  {
    new Color(224,192,128),
    new Color(192,224,128),
    new Color(128,192,224),
    new Color(255,192,192),
    new Color(192,255,255),
    new Color(255,192,255),
    new Color(192,192,255),
    new Color(192,255,192),
  };

  private void bg(Component _c)
  {
    if(isColorized())
      _c.setBackground(COLORS[nbf_%COLORS.length]);
  }

  private void bgr(Component _c)
  {
    bg(_c);

    if(_c instanceof Container)
    {
      Component[] f=((Container)_c).getComponents();
      for(int i=0;i<f.length;i++)
	bg(f[i]);
    }
  }

  private static final void removeComponent0(JComponent _c)
  {
    if(_c!=null)
    {
      Container parent=_c.getParent();
      if(parent!=null) parent.remove(_c);
    }
  }

  public void addInternalFrame(final BuSubApp _subapp)
  {
    SwingUtilities.invokeLater
      (new Runnable()
       {
	 @Override
   public void run()
	   { addInternalFrame0(_subapp); }
       });
  }

  void addInternalFrame0(BuSubApp _subapp)
  {
    _subapp.setEnabledForAction("ASPECT",false);
    _subapp.setEnabledForAction("ECHANGER_COLONNES"  ,false);
    _subapp.setEnabledForAction("VISIBLE_SPECIFICBAR",false);
    _subapp.setEnabledForAction("VISIBLE_LEFTCOLUMN" ,false);
    _subapp.setEnabledForAction("VISIBLE_RIGHTCOLUMN",false);
    _subapp.setEnabledForAction("VISIBLE_STATUSBAR"  ,false);

    bg(_subapp);
    bg(_subapp.getMainPanel().getDesktop());

    BuMenuBar mb=_subapp.getMainMenuBar();
    bgr(mb);
    menu_bars_.put(_subapp,mb);

    BuToolBar tb=_subapp.getMainToolBar();
    tb.setFloatable(false);
    bgr(tb);
    tool_bars_.put(_subapp,tb);

    BuSpecificBar pb=_subapp.getMainPanel().getSpecificBar();
    pb.setFloatable(false);
    bgr(pb);
    spec_bars_.put(_subapp,pb);
    removeComponent0(pb);

    BuStatusBar sb=_subapp.getMainPanel().getStatusBar();
    bgr(sb);
    stat_bars_.put(_subapp,sb);
    removeComponent0(sb);
    //_subapp.getContentPane().remove(sb);

    BuColumn lc=_subapp.getMainPanel().getLeftColumn();
    bgr(lc);
    left_cols_.put(_subapp,lc);
    removeComponent0(lc);
    //_subapp.getContentPane().remove(lc);

    BuColumn rc=_subapp.getMainPanel().getRightColumn();
    bgr(rc);
    right_cols_.put(_subapp,rc);
    removeComponent0(rc);
    //_subapp.getContentPane().remove(rc);

    nbf_++;
    _subapp.setLocation(60*nbf_,30*nbf_);
    _subapp.setSize(600,400);

    _subapp.addInternalFrameListener(this);
    getMainPanel().addInternalFrame(_subapp);

    /*
    invalidate();
    doLayout();
    validate();
    repaint();
    */

    _subapp.revalidate();
    System.err.println("ADD: "+_subapp.getTitle());
  }

  /*
  private void replace(Component _a, String _p, Component _b)
  {
    BuMainPanel mp=getMainPanel();

    try { mp.remove(_a); }
    catch(Exception ex) { System.err.println("BuMacEnv.replace: "+ex); }

    mp.add(_p,_b);

    invalidate();
    doLayout();
    validate();
    repaint();
  }
  */

  public void putEnv(BuSubApp _subapp)
  {
    System.err.println("PUT: "+_subapp.getTitle());
    // BuMainPanel mp=getMainPanel();

    BuMenuBar mb=(BuMenuBar)menu_bars_.get(_subapp);
    mb.insertMenu(mac_menu_);
    mac_menu_.setBackground(mb.getBackground());
    // mac_menu_.setText("");
    // mac_menu_.setIcon(BuResource.BU.getIcon("cerise"));
    setMainMenuBar(mb);

    BuToolBar tb=(BuToolBar)tool_bars_.get(_subapp);
    tb.setFloatable(false);
    setMainToolBar(tb);

    BuMainPanel mp=getMainPanel();

    BuSpecificBar pb =(BuSpecificBar)spec_bars_.get(_subapp);
    //BuSpecificBar xpb=getMainPanel().getSpecificBar();
    //replace(xpb,BuBorderLayout.NORTH,pb);
    mp.setSpecificBar(pb);
    
    BuStatusBar sb =(BuStatusBar)stat_bars_.get(_subapp);
    //BuStatusBar xsb=getMainPanel().getStatusBar();
    //replace(xsb,BuBorderLayout.SOUTH,sb);
    mp.setStatusBar(sb);
    
    BuColumn lc =(BuColumn)left_cols_.get(_subapp);
    //BuColumn xlc=getMainPanel().getLeftColumn();
    //replace(xlc,BuBorderLayout.WEST,lc);
    mp.setLeftColumn(lc);
    
    BuColumn rc =(BuColumn)right_cols_.get(_subapp);
    //BuColumn xrc=getMainPanel().getRightColumn();
    //replace(xrc,BuBorderLayout.EAST,rc);
    mp.setRightColumn(rc);
    
    setTitle("[ "+_subapp.getTitle()+" ]");
  }

  public void remEnv(BuSubApp _subapp)
  {
    System.err.println("REM: "+_subapp.getTitle());

    BuMenuBar mb=_subapp.getMainMenuBar();
    mb.removeMenu(mac_menu_);
    default_mb_.removeMenu(mac_menu_);
    mac_menu_.setBackground(default_mb_.getBackground());
    default_mb_.insertMenu(mac_menu_);
    setMainMenuBar(default_mb_);

    //BuToolBar tb=_subapp.getMainToolBar();
    // remove(tb);
    setMainToolBar(default_tb_);

    BuMainPanel mp=getMainPanel();

    //BuSpecificBar pb=_subapp.getMainPanel().getSpecificBar();
    //replace(pb,BuBorderLayout.NORTH,default_pb_);
    mp.setSpecificBar(default_pb_);
    
    //BuStatusBar sb=_subapp.getMainPanel().getStatusBar();
    //replace(sb,BuBorderLayout.SOUTH,default_sb_);
    mp.setStatusBar(default_sb_);
    
    // BuColumn lc=_subapp.getMainPanel().getLeftColumn();
    //replace(lc,BuBorderLayout.WEST,default_lc_);
    mp.setLeftColumn(default_lc_);
    
    //BuColumn rc=_subapp.getMainPanel().getRightColumn();
    //replace(rc,BuBorderLayout.EAST,default_rc_);
    mp.setRightColumn(default_rc_);
    
    setDefaultTitle();
  }

  // InternalFrame

  @Override
  public void internalFrameActivated(InternalFrameEvent _evt)
  { 
    super.internalFrameActivated(_evt);
    putEnv((BuSubApp)_evt.getSource());
  }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _evt)
  { 
    super.internalFrameDeactivated(_evt);
    remEnv((BuSubApp)_evt.getSource());
  }

  @Override
  public void internalFrameClosed(InternalFrameEvent _evt)
  { }

  @Override
  public void internalFrameClosing(InternalFrameEvent _evt)
  { }

  @Override
  public void internalFrameDeiconified(InternalFrameEvent _evt)
  { }

  @Override
  public void internalFrameIconified(InternalFrameEvent _evt)
  { }

  @Override
  public void internalFrameOpened(InternalFrameEvent _evt)
  { }

  // Main

  /*
  public static void main(String[] _args)
  {
    BuMacEnv app=new BuMacEnv();
    app.init();
    app.start();

    //BuSubApp subapp1=new BuSubApp();
    //subapp1.setTitle("SubApp");
    //subapp1.init();
    //subapp1.start();

    //BuSubApp subapp2=new BuSubApp();
    //subapp2.setTitle("SubApp 2");
    //subapp2.init();
    //subapp2.start();

    //BuSubApp subapp3=new BuSubApp();
    //subapp3.setTitle("SubApp 3");
    //subapp3.init();
    //subapp3.start();

    //app.addInternalFrame(subapp1);
    //app.addInternalFrame(subapp2);
    //app.addInternalFrame(subapp3);
  }
  */
}
