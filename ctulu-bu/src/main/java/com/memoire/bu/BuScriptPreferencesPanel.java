/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuScriptPreferencesPanel.java
 * @version      0.30
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose which script interpreters
 * are available and what is the the scripts directory.
 * script.directory
 * script.interpreters
 * @deprecated replaced by ScriptPreferencesPanel
 * @version      0.01, 2000-07-19
 * @author       Guillaume Desnoix
 */
public abstract class BuScriptPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  protected BuCommonInterface appli_;
  protected BuPreferences     options_;

  protected BuTextField        tfDir_;
  protected BuButton           btDir_;
  protected BuTextField        tfInt_;
  
  @Override
  public String getTitle()
  {
    return getS("Script");
  }

  @Override
  public String getCategory()
  {
    return getS("Composants");
  }

  // Constructeur

  public BuScriptPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_=_appli;
    options_=BuPreferences.BU;

    BuGridLayout lo=new BuGridLayout();
    lo.setColumns(2);
    lo.setVgap(5);
    lo.setHgap(5);

    BuPanel p=new BuPanel();
    p.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getS("Script")),
	EMPTY5555));
    p.setLayout(lo);

    BuBorderLayout lo1=new BuBorderLayout();
    lo1.setHgap(5);
    lo1.setVgap(5);
    BuPanel p1=new BuPanel();
    p1.setLayout(lo1);

    tfDir_=new BuTextField();
    tfDir_.setName("tfDIRECTORY");
    tfDir_.setEditable(false);
    tfDir_.addKeyListener(this);
    tfDir_.addActionListener(this);

    btDir_=new BuButton();
    //btDir_.setRequestFocusEnabled(false);
    btDir_.setIcon(getDirectoryIcon());
    btDir_.setName("btDIRECTORY");
    btDir_.setMargin(BuInsets.INSETS0202);
    btDir_.addActionListener(this);

    p1.add(tfDir_,BuBorderLayout.CENTER);
    p1.add(btDir_,BuBorderLayout.EAST);
    p.add(new BuLabel(getS("Répertoire:"),SwingConstants.RIGHT));
    p.add(p1);

    p.add(new BuLabel(getS("Interpréteurs:"),SwingConstants.RIGHT));
    tfInt_=new BuTextField();
    tfInt_.setName("tfINTERPRETERS");
    tfInt_.addKeyListener(this);
    tfInt_.addActionListener(this);
    p.add(tfInt_);

    setLayout(new BuBorderLayout());
    setBorder(EMPTY5555);
    add(p,BuBorderLayout.CENTER);

    updateComponents();
  }

  private static final Icon getDirectoryIcon()
  {
    Icon r=UIManager.getIcon("FileView.directoryIcon");
    if(r==null) r=BuResource.BU.getIcon("ouvrir");
    return r;
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    //System.err.println("SOURCE="+source.getName());
    //System.err.println("ACTION="+_evt.getActionCommand());

    if(source==btDir_)
      tfDir_.setText(getDirectory(tfDir_.getText()));
	
    setDirty(true);
    updateAbility();
    //chScript.setEnabled(cbExterne.isSelected());
  }
  
  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
    { return true; }
  
  @Override
  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees
  
  private void fillTable()
  {
    options_.putStringProperty("script.directory",tfDir_.getText());
    options_.putStringProperty("script.interpreters",tfInt_.getText());
  }

  private void updateComponents()
  {
    tfDir_.setText(options_.getStringProperty("script.directory"));
    tfInt_.setText(options_.getStringProperty("script.interpreters","Acto,Foo,-,Dawn,Fiji,Python,Silk"));
  }

  private String getDirectory(String _default)
  {
    String r=FuLib.expandedPath(_default);

    // System.err.println("### FD begin");
    BuFileChooser chooser=new BuFileChooser();
    chooser.setDialogTitle(getS("Repertoire"));
    chooser.setFileSelectionMode(BuFileChooser.DIRECTORIES_ONLY);
    chooser.setFileHidingEnabled(true);
    chooser.setMultiSelectionEnabled(false);
    chooser.resetChoosableFileFilters();

    File f=new File(r);
    if(f.exists()) chooser.setCurrentDirectory(f);

    int returnVal=chooser.showDialog(this,getS("Sélectionner"));
    // System.err.println("### FD end");

    if(returnVal == BuFileChooser.APPROVE_OPTION)
    {
      f=chooser.getSelectedFile();
      // System.err.println("### f="+f);
      if(f!=null) r=f.getPath();
    }

    return FuLib.reducedPath(r);
  }
}
