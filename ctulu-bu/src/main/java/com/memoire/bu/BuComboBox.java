/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuComboBox.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

/**
 * Nothing more than a JComboBox.
 */

public class BuComboBox
       extends JComboBox
{
  public BuComboBox()
  {
    super();
    setRenderer(BuAbstractCellRenderer.getDefaultComboBoxCellRenderer());
  }

  public BuComboBox(Object[] _items)
  {
    super(_items);
    setRenderer(BuAbstractCellRenderer.getDefaultComboBoxCellRenderer());
  }

  public BuComboBox(Vector _items)
  {
    super(_items);
    setRenderer(BuAbstractCellRenderer.getDefaultComboBoxCellRenderer());
  }

  public BuComboBox(ComboBoxModel _model)
  {
    super(_model);
    setRenderer(BuAbstractCellRenderer.getDefaultComboBoxCellRenderer());
  }

  public void addAllItems(Object[] _items)
  {
    if(_items!=null)
      for(int i=0;i<_items.length;i++)
        addItem(_items[i]);
  }
  
  public void setAllItems(Object[] _items)
  {
    removeAllItems();
    addAllItems(_items);
  }  

  /*
  public void updateUI()
  {
    super.updateUI();
    setMaximumSize(getPreferredSize());
  }
  */
}
