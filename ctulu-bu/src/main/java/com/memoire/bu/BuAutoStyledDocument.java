/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuAutoStyledDocument.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import com.memoire.re.RE;
import com.memoire.re.REException;
import com.memoire.re.REMatch;
import com.memoire.re.RESyntax;
import java.awt.Color;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Vector;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * A auto-colorized document (using regular expressions).
 */
public class BuAutoStyledDocument extends DefaultStyledDocument {
  public static final String KEYWORD = "KEYWORD";
  public static final String MODIFIER = "MODIFIER";
  public static final String TYPE = "TYPE";
  public static final String SPECIAL = "SPECIAL";
  public static final String OPERATOR = "OPERATOR";
  public static final String BLOCK = "BLOCK";
  public static final String NUMBER = "NUMBER";
  public static final String COMMENT = "COMMENT";
  public static final String PRAGMA = "PRAGMA";
  public static final String STRING = "STRING";

  /*
   * A faire: n'utiliser regexp que qd c'est necessaire. TYPE:STYLE boolean: replace is different than find if indexes
   * are not the same.
   */

  public static final byte IS_KEYWORD = 1;
  public static final byte IS_SIMPLE_STRING = 2;
  public static final byte IS_SIMPLE_CHAR = 3;
  public static final byte IS_ONELINE_REGEXP = 4;
  public static final byte IS_MULTILINE_REGEXP = 5;

  private class Key {
    public String match;
    public String replace;
    public String style;
    public byte type;
    public RE regexp_match;
    public RE regexp_replace;

    Key(String _match, String _replace, String _style, byte _type) {
      match = _match;
      replace = _replace;
      style = _style;
      type = _type;

      /*
       * if( (type==IS_ONELINE_REGEXP) ||(type==IS_MULTILINE_REGEXP))
       */
      {
        try {
          regexp_match = new RE(match, RE.REG_MULTILINE, RESyntax.RE_SYNTAX_PERL5);
          regexp_replace = new RE(replace, RE.REG_MULTILINE, RESyntax.RE_SYNTAX_PERL5);

          while (keys_.contains(this))
            keys_.removeElement(this);

          keys_.addElement(this);
        } catch (REException ex) {
          System.err.println("Bad regular exception\n  " + match + "\n  " + replace);
        }
      }
    }

    public boolean equals(Object _ok) {
      boolean r = false;

      if (_ok instanceof Key) {
        Key k = (Key) _ok;
        r = (match.equals(k.match) && replace.equals(k.replace));
      }

      return r;
    }

    public int hashCode() {
      return super.hashCode() + match == null ? 0 : match.hashCode() + replace == null ? 0 : replace.hashCode();
    }
  }

  public Key createKey(String _match, String _replace, String _style) {
    return new Key(_match, _replace, _style, IS_MULTILINE_REGEXP);
  }

  public Key createKey(String _match, String _style) {
    return createKey(_match, _match, _style);
  }

  public Key createKeyword(String _word, String _style) {
    return createKey("(^|[^a-zA-Z0-9_])" + _word + "($|[^a-zA-Z0-9_])", _word, _style);
  }

  public Key createKeyword(String _word) {
    return createKeyword(_word, KEYWORD);
  }

  Vector keys_;

  public BuAutoStyledDocument(String _text) {
    this(_text, null);
  }

  public BuAutoStyledDocument(String _text, String _syntax) {
    this();
    if (_syntax != null) setSyntax(_syntax);
    try {
      insertString(0, _text, null);
    } catch (BadLocationException ex) {
      FuLog.warning(ex);
    }
  }

  public BuAutoStyledDocument() {
    super(new StyleContext());
    ta_ = null;
    keys_ = new Vector();

    createStyle(KEYWORD, null, true, false);
    createStyle(TYPE, new Color(0, 128, 0), false, false);
    createStyle(MODIFIER, new Color(0, 128, 128), false, false);
    createStyle(SPECIAL, new Color(128, 128, 0), false, true);
    createStyle(OPERATOR, new Color(192, 96, 0), true, false);
    createStyle(BLOCK, Color.red, false, false);
    createStyle(NUMBER, Color.blue, false, false);
    createStyle(COMMENT, new Color(64, 64, 160), false, true);
    createStyle(PRAGMA, new Color(160, 0, 160), false, true);
    createStyle(STRING, Color.magenta, false, false);

    // createKey("(^|[^a-zA-Z0-9_.])[0-9]*\\.[0-9]*[df]?($|[^a-zA-Z0-9_.])",
    // "[0-9]*\\.[0-9]*[df]?",
    // NUMBER);
    // createKey("(^|[^a-zA-Z0-9_])[0-9][0-9]*[l]?($|[^a-zA-Z0-9_])",
    // "[0-9][0-9]*[l]?",
    // NUMBER);
    // createKeyword("hello");
    // createKeyword("world");
    // createKeyword("this" );
    // createKeyword("super");
    // createKey("\\{" ,BLOCK);
    // createKey("\\}" ,BLOCK);
    // createKey("\\(" ,BLOCK);
    // createKey("\\)" ,BLOCK);
    // createKey("\"[^\"]*\"" ,STRING);
    // createKey("\'[^\']*\'" ,STRING);
    // createKey("/\\*[^/*]*\\*/",COMMENT);
    // createKey("//.*$" ,COMMENT);

    setColorized(true);
  }

  public Style createStyle(String _name, Color _fg, boolean _bold, boolean _italic) {
    Style style = ((StyleContext) getAttributeContext()).addStyle(_name, null);
    if (_fg != null) style.addAttribute(StyleConstants.Foreground, _fg);
    if (_bold) style.addAttribute(StyleConstants.Bold, Boolean.TRUE);
    if (_italic) style.addAttribute(StyleConstants.Italic, Boolean.TRUE);
    return style;
  }

  public void loadSyntax(String _language) throws IOException {
    LineNumberReader lnr = null;
    try {
      lnr = new LineNumberReader(new InputStreamReader(getClass().getResourceAsStream(_language + "Syntax.txt")));

      String l;
      while (true) {
        l = lnr.readLine();
        if (l == null) return;
        if ("".equals(l)) continue;

        if ("INHERIT".equals(l)) {
          String f = lnr.readLine();
          System.err.println("load parent syntax: " + f);
          loadSyntax(f);
        }
        if ("KEY".equals(l)) {
          String m = lnr.readLine();
          String r = lnr.readLine();
          String s = lnr.readLine();
          createKey(m, r, s);
        }
        if ("OPERATOR".equals(l) || "BLOCK".equals(l) || "STRING".equals(l) || "COMMENT".equals(l)) {
          String s = l;
          while (true) {
            l = lnr.readLine();
            if (l == null) return;
            if ("".equals(l)) break;
            createKey(l, s);
          }
        } else {
          String s = l;
          while (true) {
            l = lnr.readLine();
            if (l == null) return;
            if ("".equals(l)) break;
            createKeyword(l, s);
          }
        }
      }
    } 
    finally{
      if(lnr!=null) lnr.close();
    }
  }

  BuTextPane ta_;

  public BuTextPane getTextPane() {
    return ta_;
  }

  public void setTextPane(BuTextPane _ta) {
    ta_ = _ta;
    if (ta_ != null) ta_.repaint(0);
  }

  private boolean colorized_ = false;

  public boolean isColorized() {
    return colorized_;
  }

  public void setColorized(boolean _b) {
    colorized_ = _b;
  }

  private String syntax_;

  public String getSyntax() {
    return syntax_;
  }

  public void setSyntax(String _syntax) {
    // System.err.println("set-syntax: "+_syntax);

    if ((_syntax == null) || (!_syntax.equals(syntax_))) {
      syntax_ = _syntax;

      keys_.removeAllElements();
      try {
        loadSyntax(syntax_);
      } catch (Exception ex) {
        FuLog.error("syntax '" + _syntax + "' not available");
      }

      if (isColorized()) colorize();
      else
        uncolorize();
    }
  }

  /*
   * private int getLineCount() { return getDefaultRootElement().getElementCount(); }
   */

  private int getLineStartOffset(int _offset) {
    return getParagraphElement(_offset).getStartOffset();
  }

  private int getLineEndOffset(int _offset) {
    return getParagraphElement(_offset).getEndOffset();
  }

  /*
   * private int getLineStartOffset(int _offset) { int r=0; try { int n=getLineCount()-1; int p=getLength();
   * while((p>=_offset)&&(n>=0)) { p=getDefaultRootElement().getElement(n).getStartOffset(); n--; } r=p; }
   * catch(Exception ex) { System.err.println(ex); } // System.err.println("Start="+r); return r; }
   */

  /*
   * private int getLineEndOffset(int _offset) { int r=getLength(); try { int nmax=getLineCount()-1; int p=0; int n=0;
   * while((p<_offset)&&(n<=nmax)) { p=getDefaultRootElement().getElement(n).getEndOffset(); n++; } r=p; }
   * catch(Exception ex) { System.err.println(ex); } // System.err.println("End ="+r); return r; }
   */

  /*
   * public void setText(String _string) { try { super.remove(0,getLength()); super.insertString(0,_string,null); //
   * colorize(); } catch(Exception ex) { } }
   */

  @Override
  public void insertString(int _offset, String _string, AttributeSet _attr) throws BadLocationException {
    if (_string != null) {
      // replace tabs ?
      // _string=BuPrinter.replace(_string,"\t"," ");
      super.insertString(_offset, _string, _attr);

      if (_string.length() < 120) {
        if (_string.indexOf('\n') >= 0) colorize(_offset, _string.length());
      }

      if (_string.length() == 1) if ((ta_ != null) && (ta_.getDocument() == this)) {
        try {
          Rectangle r = ta_.modelToView(ta_.getCaretPosition());
          r.x = Math.max(0, r.x - 10);
          r.y = Math.max(0, r.y - 10);
          r.width = 20;
          r.height = 20;
          ta_.scrollRectToVisible(r);
        } catch (Exception ex) {}
      }
    }
  }

  /*
   * public Dimension computePreferredSize() { Style fs=getStyle(StyleContext.DEFAULT_STYLE); FontMetrics
   * fm=ta_.getFontMetrics (new Font(StyleConstants.getFontFamily(fs), Font.PLAIN, StyleConstants.getFontSize(fs))); int
   * n=getDefaultRootElement().getElementCount(); int c=0; for(int i=0;i<n;i++) { int start=getLineStartOffset(i); int
   * end =getLineEndOffset(i); c=Math.max(c,end-start+1); } int w=fm.stringWidth("m")*c; int h=fm.getHeight()*n; return
   * new Dimension(w,h); }
   */

  public void uncolorize() {
    try {
      String text = getText(0, getLength());
      super.remove(0, getLength());
      super.insertString(0, text, null);
    } catch (BadLocationException ex) {}
  }

  public void colorize() {
    if (!colorized_) return;
    __colorize(0, getLength());
  }

  public void colorize(final int _start, final int _len) {
    if (!colorized_) return;

    // try
    {
      int start = getLineStartOffset(_start);
      int end = getLineEndOffset(_start + _len);
      // System.err.println("start="+start+",offset="+_start+",end="+end);
      // try { System.err.println(getText(start,Math.max(0,end-start-1))); }
      // catch(BadLocationException ex) { }
      __colorize(start, Math.max(0, end - start - 1));
    }
    // catch(BadLocationException ex)
    // { System.err.println("BAD LOC"); }

    /*
     * try { int start=getLineStartOffset(_start); int len =getText(start,80).indexOf('\n'); if((len<0)||(len<_len+_start-start))
     * len=_len+_start-start; // System.err.println("start="+start+",offset="+_start+",len="+len); //
     * System.err.println(getText(start,len)); __colorize(start,len); } catch(BadLocationException ex) { }
     */
  }

  public void colorize(BuCommonInterface _app) {
    Colorizer t = new Colorizer(_app);
    t.setPriority(Thread.MIN_PRIORITY);
    t.start();
  }

  class Colorizer extends BuTask {
    private BuCommonInterface app_;

    public Colorizer(BuCommonInterface _app) {
      super();
      app_ = _app;

      setName(BuResource.BU.getString("Coloration"));
      if (app_ != null) setTaskView(app_.getMainPanel().getTaskView());
    }

    @Override
    public void start() {
      super.start();
    }

    @Override
    public void run() {
      boolean editable = false;

      // System.err.println("TA="+ta_);
      setProgression(2);

      if (ta_ != null) {
        editable = ta_.isEditable();
        ta_.setEditable(false);
        ta_.setDocument(new BuAutoStyledDocument(ta_.getText()));
        BuAutoStyledDocument.this.removeDocumentListener(ta_);
        BuAutoStyledDocument.this.removeUndoableEditListener(ta_);
      }

      boolean colorized = isColorized();
      setColorized(true);
      setProgression(5);

      try {
        __colorize(0, getLength());
      } catch (Exception ex) {}

      setColorized(colorized);

      if (ta_ != null) {
        setProgression(97);
        ta_.setDocument(BuAutoStyledDocument.this);
        BuAutoStyledDocument.this.addDocumentListener(ta_);
        BuAutoStyledDocument.this.addUndoableEditListener(ta_);
        ta_.setEditable(editable);
      }

      setProgression(100);
      super.run();
    }
  }

  void __colorize(int _start, int _len)
  // throws BadLocationException
  {
    String text;

    try {
      text = getText(_start, _len);
    } catch (BadLocationException ex) {
      return;
    }

    int pos = 0;
    if (ta_ != null) pos = ta_.getCaretPosition();

    // System.err.println("Colorize ["+_start+","+(_start+_len)+"]");
    // System.err.println("Text="+text);

    try {
      super.remove(_start, _len);
      super.insertString(_start, text, null);
    } catch (BadLocationException ex) {
      System.err.println("BuAutoStyledDocument: bad location 1");
      return;
    }

    int l = keys_.size();
    for (int i = 0; i < l; i++) {
      Thread t = Thread.currentThread();
      if (t instanceof Colorizer) ((Colorizer) t).setProgression(5 + 90 * i / l);

      // System.err.print("\r"+(100*i/keys_.size())+"%");

      Key key = (Key) keys_.elementAt(i);
      try {
        if (key.regexp_match == null) {
          System.err.println("Bad regular expression: " + key.match);
          break;
        }

        if (key.regexp_replace == null) {
          System.err.println("Bad regular expression: " + key.replace);
          break;
        }

        REMatch mm;
        int p = 0;
        // int q=0;

        while ((mm = key.regexp_match.getMatch(text, p)) != null) {
          String oldtext = text.substring(mm.getStartIndex(), mm.getEndIndex());

          REMatch mr = key.regexp_replace.getMatch(oldtext);

          /*
           * q++; if(q==500) { System.err.println("MAYBE LOOP : AUTOSTYLED"); System.err.println ("ERROR COLORIZE: MATCH
           * "+key.match); System.err.println (" : REPL. "+key.replace); System.err.println (" : "+oldtext); break; }
           */

          if (mr == null) {
            System.err.println("MR=NULL : AUTOSTYLED");
            System.err.println("ERROR COLORIZE: MATCH " + key.match);
            System.err.println("              : REPL. " + key.replace);
            System.err.println("              : " + oldtext);
            break;
          }

          String newtext = oldtext.substring(mr.getStartIndex(), mr.getEndIndex());

          /*
           * if(oldtext.equals(newtext)) { System.err.println("SAMETEXT : AUTOSTYLED"); System.err.println ("ERROR
           * COLORIZE: MATCH "+key.match); System.err.println (" : REPL. "+key.replace); System.err.println (" :
           * "+oldtext); break; }
           */

          int si = _start + mm.getStartIndex() + mr.getStartIndex();
          int ei = _start + mm.getStartIndex() + mr.getEndIndex();

          super.remove(si, ei - si);
          super.insertString(si, newtext, getStyle(key.style));

          p = mm.getStartIndex() + mr.getStartIndex() + newtext.length();
        }
      } catch (BadLocationException ex) {
        System.err.println("BuAutoStyledDocument: bad location 2");
      } catch (Error ex) {
        System.err.println("" + ex);
      }
    }

    if (ta_ != null) {
      ta_.setCaretPosition(pos);
      ta_.repaint(0);
    }
  }

  @Override
  public void remove(int _start, int _len) throws BadLocationException {
    super.remove(_start, _len);
    colorize(_start, 0);
  }
}
