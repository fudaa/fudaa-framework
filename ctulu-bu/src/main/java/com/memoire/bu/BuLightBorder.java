/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuLightBorder.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

/**
 * A simple bevel border of thickness 1.
 */

public class BuLightBorder extends AbstractBorder
{
  public static final int RAISED  = 0;
  public static final int LOWERED = 1;

  protected int margin_;
  protected int type_;

  public BuLightBorder()
  {
    this(RAISED,1);
  }

  public BuLightBorder(int _type, int _margin)
  {
    type_  =_type;
    margin_=_margin;
  }

  @Override
  public void paintBorder(Component _c, Graphics _g,
                          int _x, int _y, int _w, int _h)
  {
         if(type_==RAISED ) paintRaisedBevel (_c,_g,_x,_y,_w,_h);
    else if(type_==LOWERED) paintLoweredBevel(_c,_g,_x,_y,_w,_h);
  }

  @Override
  public Insets getBorderInsets(Component _c)
  {
    return new Insets(margin_,margin_,margin_,margin_);
  }

  @Override
  public boolean isBorderOpaque() { return true; }

  protected void paintRaisedBevel
    (Component _c, Graphics _g, int _x, int _y, int _w, int _h)
  {
    Color old=_g.getColor();
    _g.translate(_x,_y);
    _g.setColor(UIManager.getColor("Panel.background")); //_c.getBackground());
    _g.draw3DRect(0,0,_w-1,_h-1,true);
    if(!((JComponent)_c).isOpaque())
      for(int i=1;i<margin_;i++)
	_g.drawRect(i,i,_w-1-2*i,_h-1-2*i);
    _g.translate(-_x,-_y);
    _g.setColor(old);
  }

  protected void paintLoweredBevel
    (Component _c, Graphics _g, int _x, int _y, int _w, int _h)
  {
    Color old=_g.getColor();
    _g.translate(_x,_y);
    _g.setColor(UIManager.getColor("Panel.background")); //_c.getBackground());
    _g.draw3DRect(0,0,_w-1,_h-1,false);
    if(!((JComponent)_c).isOpaque())
      for(int i=1;i<margin_;i++)
	_g.drawRect(i,i,_w-1-2*i,_h-1-2*i);
    _g.translate(-_x,-_y);
    _g.setColor(old);
  }
}
