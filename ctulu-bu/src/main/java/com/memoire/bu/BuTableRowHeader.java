/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuTableRowHeader.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractListModel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class BuTableRowHeader
  extends BuList
  implements TableModelListener, PropertyChangeListener,
             ListSelectionListener//, TableSelectionListener
{
  JTable             table_;
  private TableModel         model_;
  //private ListSelectionModel selection_;

  public BuTableRowHeader(JTable _table,boolean _letters)
  {
    table_    =_table;
    model_    =table_.getModel();
    //selection_=table_.getSelectionModel();

    setModel(new AbstractListModel()
	     {
	       @Override
         public int getSize() { return table_.getRowCount(); }
	       @Override
         public Object getElementAt(int index) { return null; }
	     });

    setSelectionModel(table_.getSelectionModel());
    //setSelectionMode(table_.getSelectionModel().getSelectionMode());

    table_.addPropertyChangeListener(this);
    if(model_!=null) model_.addTableModelListener(this);

    addListSelectionListener(this);

    //selection_.addListSelectionListener(this);

    //setSelectionModel(_table.getSelectionModel());

    /*
    _table.getSelectionModel()
      .addListSelectionListener
      (new ListSelectionListener()
       {
	 public void valueChanged(ListSelectionEvent _evt)
	 {
	   table_.setColumnSelectionInterval(0,table_.getColumnCount()-1);
	 }
       });

    _table.getColumnModel().getSelectionModel()
      .addListSelectionListener
      (new ListSelectionListener()
       {
	 public void valueChanged(ListSelectionEvent _evt)
	 {
	   table_.getSelectionModel().setSelectionInterval
	     (0,table_.getRowCount()-1);
	 }
       });
       */

    setFixedCellWidth(40);
    setFixedCellHeight(table_.getRowHeight());
    //+table_.getIntercellSpacing().height);
    setCellRenderer(new BuTableRowHeaderRenderer(_letters));
    setRequestFocusEnabled(false);
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt)
  {
    if("model".equals(_evt.getPropertyName()))
    {
      if(model_!=null) model_.removeTableModelListener(this);
      model_=table_.getModel();
      if(model_!=null) model_.addTableModelListener(this);
    }

    /*
    if("selectionModel".equals(_evt.getPropertyName()))
    {
      if(selection_!=null) selection_.removeListSelectionListener(this);
      selection_=table_.getSelectionModel();
      if(selection_!=null) selection_.addListSelectionListener(this);
    }
    */
  }

  @Override
  public void tableChanged(TableModelEvent _evt)
  {
    invalidate();
    repaint();
  }

  @Override
  public void valueChanged(ListSelectionEvent _evt)
  {
    Object source=_evt.getSource();

    if(source==this)
    {
      if(table_.getColumnSelectionAllowed()&&
         (table_.getSelectedColumnCount()==0))
      {
        table_.setColumnSelectionInterval(0,table_.getColumnCount()-1);
      }
    }
  }

  @Override
  public void updateUI()
  {
    super.updateUI();
    setOpaque    (false);
    setBorder    (UIManager.getBorder("TableHeader.border"));
    setForeground(UIManager.getColor ("ScrollPane.foreground"));
    setBackground(UIManager.getColor ("ScrollPane.background"));
    setFont      (UIManager.getFont  ("TableHeader.font"));
  }

  @Override
  public boolean isFocusTraversable() { return false; }
  /*
  public void processMouseMotionEvent(MouseEvent _evt) { }
  public void processMouseEvent(MouseEvent _evt) { }
  */
}

