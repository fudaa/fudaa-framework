/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuVfsOperations.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuComparator;
import com.memoire.fu.FuLog;
import com.memoire.vfs.VfsFile;
import com.memoire.vfs.VfsOperations;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import javax.swing.JOptionPane;

public class BuVfsOperations implements VfsOperations {
  // private static final boolean DEBUG=Bu.DEBUG&&true;
  private static final boolean TRACE = Bu.TRACE && true;

  protected BuInformationsSoftware il_;

  public BuVfsOperations(BuInformationsSoftware _il) {
    il_ = _il;
  }

  @Override
  public final void renameFile(File _o) {
    final VfsFile f = VfsFile.ensureVfsFile(_o);
    if (f == null) return;
    BuLib.invokeOutsideLater(new Runnable() {
      @Override
      public void run() {
        renameFile0(f);
      }
    });
  }

  @Override
  public final void copyFile(File _src, File _dst) {
    final VfsFile src = VfsFile.ensureVfsFile(_src);
    final VfsFile dst = VfsFile.ensureVfsFile(_dst);

    if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: copy " + src + " to " + dst);

    BuLib.invokeOutsideLater(new Runnable() {
      @Override
      public void run() {
        copyFile0(src, dst);
      }
    });
  }

  @Override
  public final void moveFiles(final File[] _o, final File _dir) {
    BuLib.invokeOutsideLater(new Runnable() {
      @Override
      public void run() {
        moveOrCopyFiles0(_o, _dir, false, true);
      }
    });
  }

  @Override
  public final void copyFiles(final File[] _o, final File _dir) {
    BuLib.invokeOutsideLater(new Runnable() {
      @Override
      public void run() {
        moveOrCopyFiles0(_o, _dir, true, true);
      }
    });
  }

  @Override
  public final void linkFiles(final File[] _o, final File _dir) {
    BuLib.invokeOutsideLater(new Runnable() {
      @Override
      public void run() {
        linkFiles0(_o, _dir);
      }
    });
  }

  @Override
  public final void deleteFiles(final File[] _o) {
    BuLib.invokeOutsideLater(new Runnable() {
      @Override
      public void run() {
        deleteFiles0(_o);
      }
    });
  }

  // Impl.

  protected boolean renameFile0(VfsFile _f) {
    String type = (_f.isDirectory() ? "r�pertoire" : "fichier");

    BuDialogInput d = new BuDialogInput(null, il_, getString("Renommer"), getString("Nouveau nom du " + type + ":"), _f.getName());

    if (d.activate() == JOptionPane.OK_OPTION) {
      String name = d.getValue();
      VfsFile target = _f.getParentVfsFile().createChild(name);

      if (!_f.renameTo(target)) {
        new BuDialogError(null, null, getString("Impossible de renommer le " + type + " {0} en {1}", new Object[] {
            _f.getName(), name })).activate();
        return false;
      }
    }

    return true;
  }

  protected boolean copyFile0(VfsFile _src, VfsFile _dst) {
    if ((_src == null) || !_src.exists()) {
      new BuDialogError(null, il_, getString("Le fichier {0} n'existe pas", _src)).activate();
      return false;
    }

    if (_src.isDirectory()) {
      if ((_dst == null) || (_dst.exists() && !_dst.isDirectory())) {
        new BuDialogError(null, il_, getString("La destination {0} n'est pas un r�pertoire", _dst)).activate();
        return false;
      }

      if (!_dst.exists()) {
        if (!_dst.mkdirs()) {
          new BuDialogError(null, il_, getString("Le r�pertoire {0} n'a pu �tre cr�e")).activate();
          return false;
        }
      }

      moveOrCopyFiles0(_src.listFiles(), _dst, true, false);
      return true;
    }

    if ((_dst == null) || _dst.isDirectory()) {
      new BuDialogError(null, il_, getString("La destination {0} est un r�pertoire", _dst)).activate();
      return false;
    }

    if (_dst.exists()) {
      if (new BuDialogConfirmation(null, il_, getString("Remplacer le fichier {0} ?", _dst)).activate() != JOptionPane.YES_OPTION) return false;
    }

    boolean failed = false;
    try {
      if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: destination " + _dst.getAbsolutePath());

      InputStream fis = _src.getInputStream();
      OutputStream fos = _dst.getOutputStream();
      int c = 0;
      while (true) {
        c = fis.read();
        if (c == -1) break;
        fos.write(c);
      }
      fis.close();
      fos.close();
    } catch (Exception ex) {
      FuLog.error("BVO: I/O #127", ex);
      failed = true;
    }
    if (failed) {
      new BuDialogError(null, il_, getString("Le fichier {0} n'a pu �tre copi� dans {1}", new Object[] { _src, _dst }))
          .activate();
      return false;
    }

    return true;
  }

  protected void linkFiles0(File[] _o, File _dir) {
    new BuDialogError(null, il_, getString("La cr�ation de liens n'est pas disponible")).activate();
  }

  public static boolean deleteDir(final File _f) {
    if (_f == null) { return false; }
    final File[] files = _f.listFiles();
    if (files != null) {
      for (int i = files.length - 1; i >= 0; i--) {
        if (files[i].isFile()) {
          try {
            files[i].delete();
          } catch (final RuntimeException _evt) {
            FuLog.error(_evt);

          }
        } else {
          deleteDir(files[i]);
        }
      }

    }
    return _f.delete();
  }

  protected void deleteFiles0(File[] _os) {
    File[] _o = new File[_os.length];
    System.arraycopy(_os, 0, _o, 0, _o.length);
    Arrays.sort(_o, FuComparator.REVERSE_STRING_COMPARATOR);
    String to = "";
    int no = 0;

    for (int i = 0; i < _o.length; i++) {
      if (_o[i] != null) {
        VfsFile src = VfsFile.ensureVfsFile(_o[i]);
        if ((src != null) && src.exists()) {
          String s = src.getName();
          no++;
          to += "    " + s + "\n";
        }
      }
    }

    if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: " + no + " files");

    if ((no > 0)
        && (new BuDialogConfirmation(null, il_, getString("D�truire ces {0} fichiers", no) + " ?\n" + to).activate() == JOptionPane.YES_OPTION)) {
      for (int i = 0; i < _o.length; i++) {
        if (_o[i] != null) {
          VfsFile src = VfsFile.ensureVfsFile(_o[i]);
          if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: src " + src + " " + src.exists());
          if ((src != null) && src.exists()) {
            // String s=src.getName();

            boolean failed = false;
            if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: delete " + src);
            try {
              if (src.isDirectory()) {
                try {
                  deleteDir(src);
                  failed = !src.delete();
                  // failed=false;
                } catch (Exception e) {
                  failed = true;
                }
              } else failed = !src.delete();

            } catch (Exception ex) {
              failed = true;
            }
            if (failed) {
              new BuDialogError(null, il_, getString("Le fichier {0} n'a pu �tre d�truit", src)).activate();
            }
          }
        }
      }
    }
  }

  protected void moveOrCopyFiles0(File[] _o, File _dir, boolean _copy, boolean _confirm) {
    VfsFile dir = VfsFile.ensureVfsFile(_dir);
    if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: " + (_copy ? "copy" : "move") + " " + _o.length + " files to "
        + dir);

    if ((dir == null) || !dir.exists()) {
      new BuDialogError(null, il_, getString("Le repertoire {0} n'existe pas", dir)).activate();
      return;
    }

    if (!dir.canWrite()) {
      new BuDialogError(null, il_, getString("Le repertoire {0} est en lecture seule", dir)).activate();
      return;
    }

    String to = "";
    String te = "";
    int no = 0;
    int ne = 0;

    for (int i = 0; i < _o.length; i++) {
      if (_o[i] != null) {
        VfsFile src = VfsFile.ensureVfsFile(_o[i]);
        // FuLog.debug("FILE="+src);
        if ((src != null) && src.exists()) {
          // FuLog.debug("BVO: dst="+cdir+" src="+src.getClass());
          // if(src.getClass()!=cdir) _copy=true;

          String s = src.getName();
          VfsFile dst = dir.createChild(s);
          if (dst.exists()) {
            ne++;
            te += "    " + s + "\n";
          } else {
            no++;
            to += "    " + s + "\n";
          }
        }
      }
    }

    if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: " + no + " files");

    if (_confirm
        && (no > 0)
        && (new BuDialogConfirmation(null, il_, getString(_copy ? "Copier ces {0} fichiers dans {1} ?"
            : "D�placer ces {0} fichiers dans {1} ?", new Object[] { "" + no, dir })
            + "\n" + to).activate() == JOptionPane.YES_OPTION)) {
      for (int i = 0; i < _o.length; i++) {
        if (_o[i] != null) {
          VfsFile src = VfsFile.ensureVfsFile(_o[i]);
          if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: src " + src + " " + src.exists());
          if ((src != null) && src.exists()) {
            String s = src.getName();
            VfsFile dst = dir.createChild(s);
            // new File(_dir,s);
            if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: dst " + dst + " " + dst.exists() + " ==" + dst.equals(src));
            if (!dst.exists() && !dst.equals(src)) {
              if (_copy) {
                // if(src.isDirectory())
                // {
                copyFile0(src, dst);
                // }
                /*
                 * else { if(TRACE&&FuLog.isTrace()) FuLog.trace("BVO: copy "+src+" to "+dst); boolean failed=false; try
                 * { if(TRACE&&FuLog.isTrace()) FuLog.trace ("BVO: destination "+dst.getAbsolutePath());
                 * 
                 * InputStream fis=src.getInputStream(); OutputStream fos=dst.getOutputStream(); int c=0; int n=0;
                 * while(true) { c=fis.read(); if(c==-1) break; fos.write(c); }
                 * 
                 * fis.close(); fos.close(); } catch(Exception ex) { FuLog.error("BVO: I/O #145",ex); failed=true; }
                 * if(failed) { new BuDialogError (null,il_, _("Le fichier {0} n'a pu �tre copi� dans {1}", new Object[]
                 * { src,dir })).activate(); //if(dst.exists()) dst.delete(); } }
                 */
              } else {
                boolean failed = false;
                if (TRACE && FuLog.isTrace()) FuLog.trace("BVO: rename " + src + " to " + dst);
                try {
                  failed = !src.renameTo(dst);
                } catch (Exception ex) {
                  failed = true;
                }
                if (failed) {
                  new BuDialogError(null, il_, getString("Le fichier {0} n'a pu �tre d�plac� dans {1}",
                      new Object[] { src, dir })).activate();
                  // if(dst.exists()) dst.delete();
                }
              }
            } else {
              te += s + "\n";
              ne++;
            }
          }
        }
      }
    }

    if (ne > 0) new BuDialogError(null, il_, getString(_copy ? "Les {0} fichiers suivants n'ont pas �t� copi�s "
        + "car ils existent d�j� dans {1}" : "Les {0} fichiers suivants n'ont pas �t� d�plac�s "
        + "car ils existent d�j� dans {1}", new Object[] { "" + ne, dir })
        + " :\n" + te).activate();
  }

  public final String getString(final String _s) {
    return BuResource.BU.getString(_s);
  }

  public final String getString(final String _s, final Object[] _p) {
    return BuResource.BU.getString(_s, _p);
  }

  public final String getString(final String _s, final Object _p) {
    return BuResource.BU.getString(_s, new Object[] { _p });
  }

  public final String getString(final String _s, final int _p) {
    return BuResource.BU.getString(_s, new Object[] { "" + _p });
  }
}
