/**
 * @modification $Date: 2006-11-15 13:49:11 $
 * @statut       unstable
 * @file         BuStringValidator.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryLong;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * An abstract root class to specify a validator for input strings. Provides standard ones for ints, doubles, dates, ...
 */
public abstract class BuStringValidator {
  
  /**
   * Verifie que la chaine pass�e en entr�e a un format valide.
   * @param _string La chaine.
   * @return True : Le format de la chaine est correct.
   */
  public abstract boolean isStringValid(String _string);

  /**
   * Convertit la chaine en entr�e en valeur suivant le type attendu.
   * @param _string La chaine a convertir.
   * @return La valeur convertie, ou null si la chaine en entr�e est invalide.
   */
  public abstract Object stringToValue(String _string);

  /**
   * Convertit la valeur en chaine. La chaine convertie peut �tre � nouveau convertie en valeur par {@link #stringToValue(String)} 
   * @param _value La valeur
   * @return La chaine resultante.
   */
  public String valueToString(Object _value) {
    return _value.toString();
  }

  // Instances

  public static final BuStringValidator INTEGER = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      boolean r = true;
      try {
        new Integer(_string);
      } catch (Exception ex) {
        r = false;
      }
      return r;
    }

    @Override
    public Object stringToValue(String _string) {
      return new Integer(_string);
    }
  };

  public static BuStringValidator getEmptyEnable(BuStringValidator _val) {
    return new EmptyValidator(_val);
  }

  public static final BuStringValidator LONG = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      boolean r = true;
      try {
        new Long(_string);
      } catch (Exception ex) {
        r = false;
      }
      return r;
    }

    @Override
    public Object stringToValue(String _string) {
      return new Long(_string);
    }
  };

  public static final BuStringValidator FLOAT = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      boolean r = true;
      try {
        new Float(_string.replace(',', '.'));
      } catch (Exception ex) {
        r = false;
      }
      return r;
    }

    @Override
    public Object stringToValue(String _string) {
      return new Float(_string.replace(',', '.'));
    }
  };

  public static final BuStringValidator DOUBLE = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      boolean r = true;
      try {
        new Double(_string.replace(',', '.'));
      } catch (Exception ex) {
        r = false;
      }
      return r;
    }

    @Override
    public Object stringToValue(String _string) {
      return new Double(_string.replace(',', '.'));
    }
  };

  public static final class EmptyValidator extends BuStringValidator {
    final BuStringValidator other_;

    public EmptyValidator(final BuStringValidator _other) {
      super();
      other_ = _other;
    }

    @Override
    public boolean isStringValid(String _string) {
      return _string == null || _string.length() == 0 || other_.isStringValid(_string);
    }

    @Override
    public String valueToString(Object _value) {
      return _value == null ? "" : _value.toString();
    }

    @Override
    public Object stringToValue(String _string) {
      return (_string == null || _string.length() == 0) ? null : other_.stringToValue(_string);
    }
  };
  public static final BuStringValidator HOST = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      return _string.length() > 0;
    }

    @Override
    public Object stringToValue(String _string) {
      return _string.toLowerCase();
    }
  };

  public static final BuStringValidator ID = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      return true;
    }

    @Override
    public Object stringToValue(String _string) {
      return _string;
    }
  };

  public static final BuStringValidator FILE = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      return (_string.length() > 0);
    }

    @Override
    public Object stringToValue(String _string) {
      return _string;
    }
  };

  public static final BuStringValidator DATE = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      boolean r = true;
      try {
        DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).parse(_string);
      } catch (Exception ex) {
        r = false;
      }
      return r;
    }

    @Override
    public Object stringToValue(String _string) {
      Date r = null;
      try {
        r = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).parse(_string);
      } catch (Exception ex) {/* r=null; */}
      // System.err.println(""+r);
      return r;
    }

    @Override
    public String valueToString(Object _value) {
      return DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).format(_value);
    }
  };

  public static final BuStringValidator TIME = new BuStringValidator() {
    @Override
    public boolean isStringValid(String _string) {
      boolean r = true;

      try {
        DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault()).parse(_string);
      } catch (Exception ex) {
        r = false;
      }

      if (r == false) {
        r = true;
        if ((_string.length() != 5) && (_string.length() != 8)) r = false;
        else {
          try {
            new SimpleDateFormat("hh:mm:ss").parse(_string);
          } catch (ParseException ex1) {
            try {
              new SimpleDateFormat("hh:mm").parse(_string);
            } catch (ParseException ex2) {
              r = false;
            }
          }
        }
      }
      return r;
    }

    @Override
    public Object stringToValue(String _string) {
      Date r = null;

      try {
        r = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault()).parse(_string);
      } catch (ParseException ex) { /* r=null; */}

      if (r == null) {
        try {
          r = new SimpleDateFormat("hh:mm:ss").parse(_string);
        } catch (ParseException ex1) {
          try {
            r = new SimpleDateFormat("hh:mm").parse(_string);
          } catch (ParseException ex2) { /* r=null; */}
        }
      }

      // System.err.println(""+r);
      if (r != null) return FuFactoryLong.get(r.getTime());
      return r;
    }

    @Override
    public String valueToString(Object _value) {
      return DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault()).format(_value);
    }
  };
}
