/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuGridLayout.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import java.io.Serializable;

/**
 * Lays out in a grid a set of components.
 * Not like the java.awt.GridLayout, rows may have different heigths
 * and columns may have different widths.
 */

public class BuGridLayout
    implements LayoutManager2, Serializable
{
  public BuGridLayout()
  {
    this(3,0,0,true,true,true,false,false);
  }

  public BuGridLayout(int _columns)
  {
    this(_columns,0,0,true,true,true,false,false);
  }

  public BuGridLayout(int _columns, int _hgap, int _vgap)
  {
    this(_columns,_hgap,_vgap,true,true,true,false,false);
  }

  public BuGridLayout(int _columns,
		      int _hgap, int _vgap,
		      boolean _hfilled, boolean _vfilled)
  {
    this(_columns,_hgap,_vgap,_hfilled,_vfilled,true,false,false);
  }

  public BuGridLayout(int _columns,
		      int _hgap, int _vgap,
		      boolean _hfilled, boolean _vfilled,
		      boolean _cfilled, boolean _rfilled)
  {
    this(_columns,_hgap,_vgap,_hfilled,_vfilled,_cfilled,_rfilled,false);
  }

  public BuGridLayout(int _columns,
		      int _hgap, int _vgap,
		      boolean _hfilled, boolean _vfilled,
		      boolean _cfilled, boolean _rfilled,
		      boolean _flow)
  {
    columns_=_columns;
    hgap_   =_hgap;
    vgap_   =_vgap;
    hfilled_=_hfilled;
    vfilled_=_vfilled;
    cfilled_=_cfilled;
    rfilled_=_rfilled;
    flow_   =_flow;
    xalign_ =0.5f;
    yalign_ =0.5f;
  }

  protected int columns_;
  public int getColumns() { return columns_; }
  public void setColumns(int _columns) { columns_=_columns; }

  protected int hgap_;
  public int getHgap() { return hgap_; }
  public void setHgap(int _hgap) { hgap_=_hgap; }

  protected int vgap_;
  public int getVgap() { return vgap_; }
  public void setVgap(int _vgap) { vgap_=_vgap; }

  protected boolean hfilled_;
  public boolean getHfilled() { return hfilled_; }
  public void setHfilled(boolean _hfilled) { hfilled_=_hfilled; }

  protected boolean vfilled_;
  public boolean getVfilled() { return vfilled_; }
  public void setVfilled(boolean _vfilled) { vfilled_=_vfilled; }

  protected boolean cfilled_;
  public boolean getCfilled() { return cfilled_; }
  public void setCfilled(boolean _cfilled) { cfilled_=_cfilled; }

  protected boolean rfilled_;
  public boolean getRfilled() { return rfilled_; }
  public void setRfilled(boolean _rfilled) { rfilled_=_rfilled; }

  protected float xalign_;
  public float getXAlign() { return xalign_; }
  public void setXAlign(float _xalign) { xalign_=_xalign; }

  protected float yalign_;
  public float getYAlign() { return yalign_; }
  public void setYAlign(float _yalign) { yalign_=_yalign; }

  protected boolean wsame_;
  public boolean getSameWidth() { return wsame_; }
  public void setSameWidth(boolean _wsame) { wsame_=_wsame; }

  protected boolean hsame_;
  public boolean getSameHeight() { return hsame_; }
  public void setSameHeight(boolean _hsame) { hsame_=_hsame; }

  protected boolean flow_;
  public boolean getFlowMode() { return flow_; }
  public void setFlowMode(boolean _flow) { flow_=_flow; }

  protected float[] colxalign_;
  public float[] getColumnXAlign() { return colxalign_; }
  public void setColumnXAlign(float[] _colxalign) { colxalign_=_colxalign; }

  protected float[] colyalign_;
  public float[] getColumnYAlign() { return colyalign_; }
  public void setColumnYAlign(float[] _colyalign) { colyalign_=_colyalign; }

  @Override
  public void addLayoutComponent(Component _comp, Object _cstr)
  {
  }

  @Override
  public void addLayoutComponent(String _name, Component _comp)
  {
  }

  @Override
  public void removeLayoutComponent(Component _comp)
  {
  }

  @Override
  public Dimension minimumLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      int nbc=_parent.getComponentCount();
      int w  =0;
      int h  =0;
    
      int nbcol=columns_;
      int nbrow=(nbc+nbcol-1)/nbcol;

      int[] wcol=new int[nbcol];
      int[] hrow=new int[nbrow];

      int wcolmax=0;
      int hrowmax=0;

      int i,n;
      
      for(i=0,n=0;n<nbc;n++)
      {
	Component c=_parent.getComponent(n);
	if(c.isVisible())
	{
	  Dimension d=c.getMinimumSize();
	  wcol[i%nbcol]=Math.max(wcol[i%nbcol],d.width);
	  hrow[i/nbcol]=Math.max(hrow[i/nbcol],d.height);
	  wcolmax=Math.max(wcol[i%nbcol],wcolmax);
	  hrowmax=Math.max(hrow[i/nbcol],hrowmax);
	  i++;
	}
	else if(!flow_) i++;
      }

      for(i=0;i<nbcol;i++)
	if(wcol[i]>0)
        {
	  if(wsame_) wcol[i]=wcolmax;
	  if(i>0) w+=hgap_;
	  w+=wcol[i];
	}

      for(i=0;i<nbrow;i++)
        if(hrow[i]>0)
	{
	  if(hsame_) hrow[i]=hrowmax;
	  if(i>0) h+=vgap_;
	  h+=hrow[i];
	}

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
                         h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension preferredLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      int nbc=_parent.getComponentCount();
      int w  =0;
      int h  =0;

      int nbcol=columns_;
      int nbrow=(nbc+nbcol-1)/nbcol;

      int[] wcol=new int[nbcol];
      int[] hrow=new int[nbrow];

      int wcolmax=0;
      int hrowmax=0;

      int i,n;

      for(i=0,n=0;n<nbc;n++)
      {
	Component c=_parent.getComponent(n);
	if(c.isVisible())
	{
	  Dimension d=c.getPreferredSize();
	  wcol[i%nbcol]=Math.max(wcol[i%nbcol],d.width);
	  hrow[i/nbcol]=Math.max(hrow[i/nbcol],d.height);
	  wcolmax=Math.max(wcol[i%nbcol],wcolmax);
	  hrowmax=Math.max(hrow[i/nbcol],hrowmax);
	  i++;
	}
	else if(!flow_) i++;
      }

      for(i=0;i<nbcol;i++)
	if(wcol[i]>0)
        {
	  if(wsame_) wcol[i]=wcolmax;
	  if(i>0) w+=hgap_;
	  w+=wcol[i];
	}

      for(i=0;i<nbrow;i++)
	if(hrow[i]>0)
        {
	  if(hsame_) hrow[i]=hrowmax;
	  if(i>0) h+=vgap_;
	  h+=hrow[i];
	}

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension maximumLayoutSize(Container _parent)
  {
    return new Dimension(Short.MAX_VALUE,Short.MAX_VALUE);

    /*
    synchronized (_parent.getTreeLock())
    {
      int nbc=_parent.getComponentCount();
      int w  =Short.MAX_VALUE;
      int h  =0;

      int nbcol=columns_;
      int nbrow=(nbc+nbcol-1)/nbcol;

      int[] wcol=new int[nbcol];
      int[] hrow=new int[nbrow];

      int i;

      for(i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getMaximumSize();
	  wcol[i%nbcol]=Math.max(wcol[i%nbcol],d.width);
	  hrow[i/nbcol]=Math.max(hrow[i/nbcol],d.height);
	}
      }

      for(i=0;i<nbcol;i++)
      {
	if(i>0) w+=hgap_;
	w+=wcol[i];
      }

      for(i=0;i<nbrow;i++)
      {
	if(i>0) h+=vgap_;
	h+=hrow[i];
      }

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
    */
  }

  @Override
  public float getLayoutAlignmentX(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public float getLayoutAlignmentY(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public void invalidateLayout(Container _parent)
  {
  }

  @Override
  public void layoutContainer(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      Insets    insets=_parent.getInsets();
      Dimension size  =_parent.getSize();
      Rectangle bounds=new Rectangle
	(insets.left,
	 insets.top,
	 size.width-(insets.left + insets.right),
	 size.height-(insets.top + insets.bottom));

      int nbc=_parent.getComponentCount();
      int x  =0;
      int y  =0;
      int w  =0;
      int h  =0;

      int nbcol=columns_;
      int nbrow=(nbc+nbcol-1)/nbcol;

      int[] wcol=new int[nbcol];
      int[] hrow=new int[nbrow];

      int wcolmax=0;
      int hrowmax=0;

      int i,n;

      for(i=0,n=0;n<nbc;n++)
      {
	Component c=_parent.getComponent(n);
	if(c.isVisible())
	{
	  Dimension d=c.getPreferredSize();
	  wcol[i%nbcol]=Math.max(wcol[i%nbcol],d.width);
	  hrow[i/nbcol]=Math.max(hrow[i/nbcol],d.height);
	  wcolmax=Math.max(wcol[i%nbcol],wcolmax);
	  hrowmax=Math.max(hrow[i/nbcol],hrowmax);
	  i++;
	}
	else if(!flow_) i++;
      }

      for(i=0;i<nbcol;i++)
	if(wcol[i]>0)
        {
	  if(wsame_) wcol[i]=wcolmax;
	  if(i>0) w+=hgap_;
	  w+=wcol[i];
	}

      for(i=0;i<nbrow;i++)
	if(hrow[i]>0)
        {
	  if(hsame_) hrow[i]=hrowmax;
	  if(i>0) h+=vgap_;
	  h+=hrow[i];
	}

      for(i=0,n=0;n<nbc;n++)
      {
	Component c=_parent.getComponent(n);
	boolean   v=c.isVisible();

	if(!v&&flow_) continue;

	Dimension d=null;
	if(v) d=c.getPreferredSize();
	else  d=new Dimension(0,0);

	final int P=i%nbcol;
	final int Q=i/nbcol;

	int wmax=wcol[P];
	int hmax=hrow[Q];

	if((i>0)&&(P==0)&&(hmax>0)) y+=vgap_;
	if((P!=0)       &&(wmax>0)) x+=hgap_;

	w=(hfilled_ ? wmax : Math.min(wmax,d.width ));
	h=(vfilled_ ? hmax : Math.min(hmax,d.height));

	if(x>bounds.width ) x=bounds.width;
	if(y>bounds.height) y=bounds.height;
	if(x+w>bounds.width ) w=bounds.width-x;
	if(y+h>bounds.height) h=bounds.height-y;

	// last
	if(cfilled_&&(P==nbcol-1)&&(x+w<bounds.width ))
	  w=wmax=bounds.width -x;
	if(rfilled_&&(Q==nbrow-1)&&(y+h<bounds.height))
	  h=hmax=bounds.height-y;
	
	// center
	int dx=0;
	int dy=0;
	if(!hfilled_)
	  dx=(int)((wmax-w)*(colxalign_==null ? xalign_ : colxalign_[P]));
	if(!vfilled_)
	  dy=(int)((hmax-h)*(colyalign_==null ? yalign_ : colyalign_[P]));

	c.setBounds(bounds.x+x+dx,bounds.y+y+dy,w,h);
	//System.err.println("-- "+P+","+Q+": "+c.getBounds());

	if((i+1)%nbcol!=0) x+=wmax;
	else { x=0; y+=hmax; }

	i++;
      }
    }
  }
}
