/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         BuAbilityInterface.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * Defines a way to ask and call a command using a string.
 */
interface BuAbilityInterface
{
  boolean can(String _action);
  boolean act(String _action);
  boolean can(String _action,Object[] _params);
  boolean act(String _action,Object[] _params);
}
