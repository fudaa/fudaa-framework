/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuPrintable.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.PrintJob;

/**
 * An interface to declare a component printable.
 */

public interface BuPrintable
{
  void print(PrintJob _job, Graphics _g);
}
