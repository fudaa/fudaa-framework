/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * An ImageIcon but with always some image.
 */
public class BuIcon
  extends BuRobustIcon
{
  /*
  public static final Image defaut_=creeDefaut();

  private static final Image creeDefaut()
  {
    short[] di=new short[]
      {
	0x49, 0x47, 0x38, 0x46, 0x61, 0x37, 0x00, 0x10, 0x00, 0x10,
	0x00, 0x80, 0xff, 0x00, 0xff, 0xff, 0xff, 0xff, 0x2c, 0xff,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x10, 0x02, 0x00,
	0x84, 0x0e, 0xa9, 0x8f, 0xed, 0xcb, 0xa3, 0x0f, 0xb4, 0x9c,
	0x8b, 0xda, 0x3e, 0xb3, 0x00, 0x05, 0x00, 0x3b
      };

    byte[] db=new byte[di.length];
    for(int i=0;i<di.length;i++) db[i]=(byte)di[i];
    //defaut_=
    return BuLib.HELPER.getToolkit().createImage(db);
  }
  */

  public BuIcon(String _url)
  {
    this(FuLib.createURL(_url));
  }

  public BuIcon(final URL _url)
  {
    if(_url==null)
    {
      setDescription("null-url");
      setImage(BuLib.DEFAULT_IMAGE);
      return;
    }

    initImage(_url);
  }

  protected void initImage(URL _url)
  {
    String u=""+_url;
    setDescription(u);

    Image img=null;

    /*
    if(FuLib.jdk()>=1.4)
    {
      try
      {
        BufferedImage i=ImageIO.read(_url);
        GraphicsConfiguration gc=BuLib.HELPER.getGraphicsConfiguration();
        img=gc.createCompatibleImage
          (i.getWidth(null),i.getHeight(null),Transparency.TRANSLUCENT);
        Graphics g=img.getGraphics();
        g.drawImage(i,0,0,null);
        g.dispose();
      }
      catch(IOException ex) { img=null; }
    }
    */

    if((u.endsWith(".bmp")||u.endsWith(".BMP")))
    {
      try
      {
	BuBmpLoader l=new BuBmpLoader();
	l.read(_url.openStream());
	img=BuLib.HELPER.getToolkit().createImage(l.getImageSource());
      }
      catch(Exception ex) { }
    }

    if(img==null)
    {
      //Toolkit.getImage uses a lock
      try
      {
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        InputStream           in =_url.openStream();
        /*
        in=new BufferedInputStream(in,32768);
        int c;
        while((c=in.read())!=-1)
          out.write(c);
        */
        FuLib.copyFully(in,out);
        in.close();
        img=BuLib.HELPER.getToolkit().createImage(out.toByteArray());
      }
      catch(Exception ex) { }
    }

    setImage(img);

    //System.err.println("PROTOCOL="+_url.getProtocol());
    // if file then skip ?
    /*
    final ByteArrayOutputStream out=new ByteArrayOutputStream();

    Runnable runnable=new Runnable()
      {
	public void run()
	{
	  try
	  {
	    InputStream in =_url.openStream();
	    in=new BufferedInputStream(in);

	    int c;
	    while((c=in.read())!=-1)
	      out.write(c);

	    in.close();
	  }
	  catch(Throwable ex) { }
	}
      };

    long before=System.currentTimeMillis();
    long after;
    Thread t=new Thread(runnable);
    t.start();
    while((after=System.currentTimeMillis())<before+10000L)
    {
      if(!t.isAlive())
      {
	Image img=null;

	if((""+_url).toLowerCase().endsWith(".bmp"))
	{
	  try
	  {
	    BuBmpLoader l=new BuBmpLoader();
	    l.read(new ByteArrayInputStream(out.toByteArray()));
	    img=BuLib.HELPER.getToolkit().createImage(l.getImageSource());
	  }
	  catch(Exception ex) { }
	}
	else
	{
	  img=BuLib.HELPER.getToolkit().createImage(out.toByteArray());
	}

	super_setImage(BuLib.ensureImageIsLoaded(img,BuLib.DEFAULT_IMAGE));
	//if((getImage()==null)||(getIconWidth()<0)||(getIconHeight()<0))
	//super_setImage(BuLib.DEFAULT_IMAGE);
	return;
      }

      try { Thread.sleep(10L); }
      catch(InterruptedException ex) { }
    }

    if(!t.isInterrupted()) t.interrupt();
    super_setImage(BuLib.DEFAULT_IMAGE);
    System.err.println("BuIcon: Fail to load "+_url);
    */
  }

  public BuIcon(byte[] _bytes)
  {
    super();

    if(_bytes!=null)
    {
      setDescription(_bytes.length+" bytes");
      setImage(BuLib.HELPER.getToolkit().createImage(_bytes));
    }
    else
    {
      setDescription("null-"+FuLib.codeLocation());
      setImage(BuLib.DEFAULT_IMAGE);
    }
  }

  public BuIcon(Image _image)
  {
    super();
    setDescription("image-"+FuLib.codeLocation());
    setImage(_image);
  }

  public BuIcon()
  {
    super();
    setDescription("default");
    setImage(BuLib.DEFAULT_IMAGE);
  }

  // Property hotspot
  private Point hotspot_=null;
  public Point getHotSpot() { return hotspot_; }
  public void setHotSpot(Point _hotspot) { hotspot_=_hotspot; }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    if(!isDefault())
    {
      BuLib.setAntialiasing(null,_g);
      if(hotspot_!=null) _g.translate(-hotspot_.x,-hotspot_.y);
      super.paintIcon(_c,_g,_x,_y);
      if(hotspot_!=null) _g.translate(hotspot_.x,hotspot_.y);
    }
  } 

  public String toString()
  {
    String r=getDescription();
    if(r==null) r="";
    if(isDefault()) r="default-"+r;
    String n=getClass().getName();
    n=n.substring(n.lastIndexOf('.')+1);
    return n+"("+r+")";
  }
}
