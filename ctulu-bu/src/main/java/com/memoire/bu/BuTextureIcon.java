/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuTextureIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * A simple icon which displays an image in a 16x16 rect.
 */

public class BuTextureIcon
  implements Icon
{
  private Image     image_;
  private Dimension size_;

  public BuTextureIcon()
  {
    this(null,null);
  }

  public BuTextureIcon(Image _image)
  {
    this(_image,null);
  }

  public BuTextureIcon(Image _image, Dimension _size)
  {
    super();
    image_=_image;
    size_=_size;
  }

  public Image getImage()
  {
    return image_;
  }

  public void setImage(Image _image)
  {
    image_=_image;
  }

  @Override
  public int getIconHeight()
  {
    return (size_==null ? BuResource.BU.getDefaultMenuSize() : size_.height);
  }

  @Override
  public int getIconWidth()
  {
    return (size_==null ? BuResource.BU.getDefaultMenuSize() : size_.width);
  } 
                
  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    if(image_!=null)
    {
      int w=getIconWidth();
      int h=getIconHeight();

      Color old=_g.getColor();
      _g.drawImage(image_,_x+3,_y+3,w-6,h-6,_c);

      Color bg=UIManager.getColor("MenuItem.background");
      bg=BuLib.getColor(bg);
      if(!Color.white.equals(bg))
      {
        _g.setColor(bg);
        _g.draw3DRect(_x+1,_y+1,w-3,h-3,false);
        _g.draw3DRect(_x+2,_y+2,w-5,h-5,true );
      }
      else
      {
	_g.setColor(bg);
        _g.drawRect(_x+1,_y+1,w-3,h-3);
	BuLib.setColor(_g,Color.black);
        _g.drawRect(_x+2,_y+2,w-5,h-5);
      }
      _g.setColor(old);
    }
  }
}
