/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuPopupWindow.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * A small window with a title.
 */
public class BuPopupWindow
       extends JPopupMenu
{
  public BuPopupWindow()
  {
    BuBorderLayout lo=new BuBorderLayout();
    lo.setVgap(2);
    setLayout(lo);

    setBorder(new CompoundBorder
	      (getBorder(),BuBorders.EMPTY1212));

    // setTitle("Hello");
  }

  static class Title extends JPanel
  {
    Title(String _title)
    {
      JLabel t=new JLabel(_title);
      t.setFont(BuLib.deriveFont("InternalFrameTitlePane",Font.PLAIN,-2));
      t.setForeground(Color.white);
      t.setBorder(new EmptyBorder(1,2,0,2));

      JButton b=new JButton("-");
      b.setFont(BuLib.deriveFont("InternalFrameTitlePane",Font.BOLD,-2));
      b.setBorder(new EmptyBorder(0,2,0,2));
      b.setRequestFocusEnabled(false);
      b.setActionCommand("LINIFY");
      // b.addActionListener(BuPopupWindow.this);

      this.setBackground(this.getBackground().darker());
      this.setOpaque(true);
      this.setLayout(new BuBorderLayout());
      this.add(t,BuBorderLayout.CENTER);
      this.add(b,BuBorderLayout.EAST);
    }
  }

  private String title_;

  public String getTitle()
    { return title_; }

  JPanel bt_;

  public void setTitle(String _title)
  {
    title_=_title;

    if(bt_!=null) // instanceof Title
    {
      // bt_.removeMouseListener(dl_);
      // bt_.removeMouseMotionListener(dl_);
      remove(bt_);
    }

    if(_title==null)
      bt_=new JPanel();
    else
    {
      bt_=new Title(_title);
      // dl_=new DockingListener(this);
      // bt_.addMouseListener(dl_);
      // bt_.addMouseMotionListener(dl_);
    }

    add(bt_,BuBorderLayout.NORTH);
    revalidate();
  }

  private JComponent content_;
  public void setContent(JComponent _content)
  {
    content_=_content;
    add(content_,BuBorderLayout.CENTER);
    revalidate();
  }

  public static void main(String[] _args)
  {
    JFrame f=new JFrame();
    BuPopupWindow w=new BuPopupWindow();
    JPanel p=new JPanel();
    p.setPreferredSize(new Dimension(100,100));
    p.setBackground(Color.white);
    w.setTitle("Hello");
    w.add(p,BuBorderLayout.CENTER);
    w.setInvoker(f);
    w.setVisible(true);
  }
}
