/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut unstable
 * @file BuMultiFormLayout.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuLog;
import com.memoire.fu.FuWeakCache;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

/**
 * A multi-container layout.
 */
public class BuMultiFormLayout
    extends BuFormLayout
    implements PropertyChangeListener {
  private static final boolean DEBUG = Bu.DEBUG && false;
  private FuWeakCache containers_;
  private int[] minw_, minh_;
  private int[] cpyw_, cpyh_;
  private int nbcol_, nbrow_;
  private boolean shouldInvalidateAll_;
  private boolean shouldReset_;

  public BuMultiFormLayout() {
    this(0, 0, LAST, LAST, null, null);
  }

  public BuMultiFormLayout(int _hgap, int _vgap) {
    this(_hgap, _vgap, LAST, LAST, null, null);
  }

  public BuMultiFormLayout(int _hgap, int _vgap, int _celastic, int _relastic) {
    this(_hgap, _vgap, _celastic, _relastic, null, null);
  }

  public BuMultiFormLayout(int _hgap, int _vgap, int _celastic, int _relastic,
                           int[] _minw, int[] _minh) {
    super(_hgap, _vgap, _celastic, _relastic);
    containers_ = new FuWeakCache();
    minw_ = _minw;
    minh_ = _minh;
    nbcol_ = (minw_ == null) ? 0 : minw_.length;
    nbrow_ = (minh_ == null) ? 0 : minh_.length;
    cpyw_ = clone(minw_);
    cpyh_ = clone(minh_);
  }

  private static int[] clone(int[] _a) {
    if (_a == null) {
      return null;
    }

    int l = _a.length;
    int[] r = new int[l];
    for (int i = 0; i < l; i++) r[i] = _a[i];
    return r;
  }

  protected void register(Container _parent) {
    Container q = _parent.getParent();
    Container o = (Container) containers_.get(_parent);
    if (o != q) {
      containers_.put(_parent, q);
    }
    if (o == null) {
      try {
        _parent.addPropertyChangeListener(this);
      } catch (NoSuchMethodError ex) {
      }
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    if (DEBUG) {
      FuLog.debug("BML: property changed: " + _evt.getPropertyName());
    }
    if ("ancestor".equals(_evt.getPropertyName())) {
      Object source = _evt.getSource();
      if (source instanceof Container) {
        Container p = (Container) source;
        if (p.getLayout() == this) {
          restore(p, p.getParent());
        }
      }
    } else if ("layout".equals(_evt.getPropertyName())) {
      Object source = _evt.getSource();
      if (source instanceof Container) {
        Container p = (Container) source;
        if ((_evt.getNewValue() != this) &&
            (_evt.getOldValue() == this)) {
          if (DEBUG) {
            FuLog.debug("BML: remove listener");
          }
          //p.removePropertyChangeListener(this);
          try {
            p.removePropertyChangeListener(this);
          } catch (NoSuchMethodError ex) {
          } // Kaffe
          restore(p, null);
        }
      }
    }
  }

  private void restore(Container _parent, Container _grandparent) {
    if (DEBUG) {
      FuLog.debug("BML: restore");
    }
    if (containers_.get(_parent) != _grandparent) {
      containers_.put(_parent, _grandparent);
      if (_grandparent != null) {
        shouldReset_ = (cpyw_ != null) || (cpyh_ != null);
      }
    }
  }

  private void reset() {
    if (!shouldReset_) {
      FuLog.error("BML: call to reset without need");
      return;
    }

    if (DEBUG) {
      FuLog.debug("BML: reset");
    }
    minw_ = clone(cpyw_);
    minh_ = clone(cpyh_);
    shouldReset_ = false;
    shouldInvalidateAll_ = true;
  }

  @Override
  public void addLayoutComponent(Component _comp, Object _cstr) {
    if (DEBUG) {
      FuLog.debug("BML: add layout");
    }
    super.addLayoutComponent(_comp, _cstr);

    // Slow but no other solution found to avoid double pack.
    Container parent = _comp.getParent();
    register(parent);
  }

  @Override
  public void removeLayoutComponent(Component _comp) {
    super.removeLayoutComponent(_comp);

    // Slow but no other solution found to avoid errors.
    Container parent = _comp.getParent();
    if (parent != null) {
      if (DEBUG) {
        FuLog.debug("BML: remove layout");
      }
      restore(parent, parent.getParent());
    }
  }

  @Override
  public void invalidateLayout(Container _parent) {
    /*
    if(invalidating_)
    {
      FuLog.warning
        ("BML: call to invalidateLayout when doing global invalidation");
      return;
    }
    */

    if (DEBUG) {
      FuLog.debug("BML: invalidate layout");
    }

    /*
    if(shouldReset_)
    {
      FuLog.warning("BML: usefull?");
      reset();
    }
    */

    super.invalidateLayout(_parent);
  }

  @Override
  public Dimension preferredLayoutSize(Container _parent) {
    if (DEBUG) {
      FuLog.debug("BML: preferred layout");
    }

    if (shouldReset_) {
      reset();
    }

    return super.preferredLayoutSize(_parent);
  }

  @Override
  public void layoutContainer(Container _parent) {
    if (DEBUG) {
      FuLog.debug("BML: layout");
    }

    if (shouldReset_) {
      FuLog.warning("BML: strange call to reset");
      minw_ = clone(cpyw_);
      minh_ = clone(cpyh_);
      shouldReset_ = false;
      shouldInvalidateAll_ = true;
    }

    Container q = _parent.getParent();
    if (containers_.get(_parent) != q) {
      containers_.put(_parent, q);
      shouldInvalidateAll_ = true;
    }
    super.layoutContainer(_parent);
    if (shouldInvalidateAll_) {
      shouldInvalidateAll_ = false;
      invalidateContainers(_parent);
    }
  }

  @Override
  protected int[] checkMinWidths(Container _parent, int[] _wcol) {
    int[] wcol = _wcol;
    if (minw_ != null) {
      int l = wcol.length;

      if (l != nbcol_) {
        if (l < nbcol_) {
          int[] a = new int[nbcol_];
          System.arraycopy(wcol, 0, a, 0, l);
          wcol = a;
          l = nbcol_;
        } else //l>nbcol_
        {
          nbcol_ = wcol.length;
          //invalidateContainers(_parent);
          shouldInvalidateAll_ = true;
        }
      }

      for (int i = 0; i < Math.min(minw_.length, l); i++) {
        if (wcol[i] < minw_[i]) {
          wcol[i] = minw_[i];
          //shouldInvalidateAll_=true;
        } else if (wcol[i] > minw_[i]) {
          minw_[i] = wcol[i];
          //invalidateContainers(_parent);
          shouldInvalidateAll_ = true;
        }
      }
    }

    return wcol;
  }

  @Override
  protected int[] checkMinHeights(Container _parent, int[] _hrow) {
    int[] hrow = _hrow;
    if (minh_ != null) {
      int l = hrow.length;
      //System.err.println("L="+l+", minh="+minh_[1]);

      if (l != nbrow_) {
        if (l < nbrow_) {
          int[] a = new int[nbrow_];
          System.arraycopy(hrow, 0, a, 0, l);
          hrow = a;
          l = nbrow_;
        } else //l>nbrow_
        {
          nbrow_ = hrow.length;
          //invalidateContainers(_parent);
          shouldInvalidateAll_ = true;
        }
      }

      for (int i = 0; i < Math.min(minh_.length, l); i++) {
        if (hrow[i] < minh_[i]) {
          hrow[i] = minh_[i];
          //shouldInvalidateAll_=true;
        } else if (hrow[i] > minh_[i]) {
          minh_[i] = hrow[i];
          //invalidateContainers(_parent);
          shouldInvalidateAll_ = true;
        }
      }

      //System.err.println("hrow[1]="+_hrow[1]);
    }

    return hrow;
  }

  private boolean invalidating_;

  protected void invalidateContainers(Container _parent) {
    if (invalidating_) {
      if (DEBUG) {
        FuLog.debug("BML: loop avoided in invalidate containers");
      }
      return;
    }
    try {
      if (DEBUG) {
        FuLog.debug("BML: invalidate containers");
      }
      invalidating_ = true;
      //shouldReset_=true;
      //reset();
      Object[] c = containers_.keys();
      Vector v = new Vector(5);
      for (int i = 0; i < c.length; i++)
        if ((c[i] != _parent) && (((Container) c[i]).getLayout() == this)) {
          //((Container)c[i]).invalidate();
          Container q = ((Container) c[i]).getParent();
          if (q != null) {
            if (!v.contains(q)) {
              v.addElement(q);
            }
          }
        }

      for (int i = 0; i < v.size(); i++) {
        Container q = (Container) v.elementAt(i);
        q.invalidate();
        q.doLayout();
        q.validate();
        //if(q instanceof JComponent) ((JComponent)q).revalidate();
      }
    } finally {
      invalidating_ = false;
    }
  }

  public static void main(String[] _args) {
    /*
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
    */
    JFrame f = new JFrame("Test Form Layout");

    BuMultiFormLayout l = new BuMultiFormLayout
        (5, 5, 2, 2, new int[]{200, 0}, new int[]{0, 0});

    JPanel c1 = new JPanel(l);
    c1.setBorder(new BuTitledBorder("C1"));
    c1.add(new JButton("AAAA"), constraint(0, 0));
    c1.add(new JButton("BBBB"), constraint(1, 0));
    c1.add(new JButton("CCCCCCC"), constraint(0, 1, 2, false, 1.f));

    JPanel c2 = new JPanel(l);
    c2.setBorder(new BuTitledBorder("C2"));
    c2.add(new JButton("D"), constraint(0, 0));
    c2.add(new JButton("E"), constraint(1, 0));
    c2.add(new JTree(), constraint(1, 1));

    JPanel c3 = new JPanel(l);
    c3.setBorder(new BuTitledBorder("C3"));
    c3.add(new JButton("FFFFFF"), constraint(0, 0, 1, false, 0.5f));
    c3.add(new JLabel("_|"), constraint(1, 1, 1, 1, false, false, 1.f, 1.f));

    JPanel p = new JPanel(new BuHorizontalLayout());//BorderLayout());
    p.add(c1, "North");
    p.add(c2, "Center");
    p.add(c3, "South");

    f.setContentPane(p);
    f.pack();

    f.setVisible(true);
  }
}
