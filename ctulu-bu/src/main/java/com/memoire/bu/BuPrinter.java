/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuPrinter.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.*;
import java.util.Properties;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyledDocument;

/**
 * An utility class to print components, strings and tables.
 */

public class BuPrinter
{
  public static final Font FONTBASE=new Font("SansSerif",Font.PLAIN,10);

  public static BuInformationsDocument INFO_DOC;
  public static BuInformationsSoftware INFO_LOG;

  public static void print
    (final Frame _parent, final String _name, final BuPrintable _composant)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      new Thread(new Runnable()
	{
	  @Override
    public void run()
	  { print(_parent,_name,_composant); }
	},"Printing "+_name).start();
      return;
    }

    System.err.println("IMPRESSION - Debut: "+_name);

    String  xxx=_name.toLowerCase().trim();
    if(INFO_LOG!=null) xxx+="_"+INFO_LOG.name.toLowerCase();

    /*
    xxx=xxx.replace('�','a');
    xxx=xxx.replace('�','e');
    xxx=xxx.replace('�','e');
    xxx=xxx.replace('�','e');
    xxx=xxx.replace('�','e');
    xxx=xxx.replace('�','i');
    xxx=xxx.replace('�','o');
    xxx=xxx.replace('�','o');
    xxx=xxx.replace('�','u');
    xxx=xxx.replace('�','u');
    xxx=xxx.replace('�','c');
    
    String  tmp="";
    boolean us =true;
    for(int i=0;i<xxx.length();i++)
    {
      char c=xxx.charAt(i);
      if(Character.isLetterOrDigit(c)&&(c>='0')&&(c<='z'))
	{ tmp+=c; us=false; }
      else
	if(!us) {tmp+='_'; us=true; }
    }
    while(tmp.endsWith("_")) tmp=tmp.substring(0,tmp.length()-1);
    */

    String tmp=FuLib.clean(xxx);
    if("".equals(tmp)) tmp="job_"+FuLib.currentTimeSeconds();

    boolean win=FuLib.isWindows();
    Properties pp=new Properties();
    pp.put("awt.print.destination",(win ? "printer" : "file"));
    pp.put("awt.print.fileName",(win ? "" : "/tmp/"+tmp+".ps"));
    pp.put("awt.print.paperSize","a4");

    PrintJob pjob=_parent.getToolkit().getPrintJob(_parent,_name,pp);
    if(pjob!=null)
    {
      Graphics pgph=pjob.getGraphics();

      System.err.println("  Largeur   ="+pjob.getPageDimension().width );
      System.err.println("  Hauteur   ="+pjob.getPageDimension().height);
      System.err.println("  R�solution="+pjob.getPageResolution()      );
      System.err.println("  Fichier   ="+(win ? "" : "/tmp/"+tmp+".ps"));
      System.err.println("  Graphics  ="+pgph);

      if(pgph==null)
      {
	System.err.println("  L'imprimante n'est pas disponible");
      }
      else
      {
	_composant.print(pjob,pgph);
	pgph.dispose();
      }

      pjob.end();
    }
    else System.err.println("  ANNULEE");

    System.err.println("IMPRESSION - Fin  : "+_name);

    // Should not use static vars
    INFO_DOC=null;
    INFO_LOG=null;
  }

  private static Image[] preview
    (final Frame _parent, final String _name, final BuPrintable _composant)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      throw new RuntimeException
	("Should not preview in swing thread");
      /*
      new Thread(new Runnable()
	{
	  public void run()
	  { preview(_parent,_name,_composant); }
	},"Previewing "+_name)
	.start();
      return;
      */
    }

    System.err.println("PREVISUALISATION - Debut: "+_name);

    //PrintJob
    BuPreviewJob pjob=new BuPreviewJob(_parent,_name);
      Graphics pgph=pjob.getGraphics();

      System.err.println("  Largeur   ="+pjob.getPageDimension().width);
      System.err.println("  Hauteur   ="+pjob.getPageDimension().height);
      System.err.println("  R�solution="+pjob.getPageResolution());

      _composant.print(pjob,pgph);
      pgph.dispose();
      pjob.end();

    System.err.println("PREVISUALISATION - Fin  : "+_name);

    // Should not use static vars
    INFO_DOC=null;
    INFO_LOG=null;

    return pjob.getImages() ;
  }

  public static void preview
    (final BuDesktop _desktop, final Frame _parent,
     final String _name, final BuPrintable _composant)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      new Thread(new Runnable()
	{
	  @Override
    public void run()
	  { preview(_desktop,_parent,_name,_composant); }
	},"Previewing "+_name).start();
      return;
    }

    final Image[] images=preview(_parent,_name,_composant);

    if(images!=null)
    {
      Runnable runnable=new Runnable()
      {
	@Override
  public void run()
	{
	  BuPreviewFrame pf=new BuPreviewFrame
	    (null,images,_parent,_name,_composant);
	  pf.setVisible(true);
	  if(_desktop!=null)
          {
	    _desktop.addInternalFrame(pf);
	    _desktop.activateInternalFrame(pf);
	  }
	  else
	  {
	    new BuDialogMessage(null,null,pf).activate();
	  }
	}
      };

      SwingUtilities.invokeLater(runnable);
    }
  }

  public static void preview
    (final BuCommonImplementation _app, final String _name,
     final BuPrintable _composant)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      new Thread(new Runnable()
	{
	  @Override
    public void run()
	  { preview(_app,_name,_composant); }
	},"Previewing "+_name).start();
      return;
    }

    final Frame   frame =_app.getFrame();
    final Image[] images=preview(frame,_name,_composant);

    if(images!=null)
    {
      Runnable runnable=new Runnable()
      {
	@Override
  public void run()
	{
	  BuPreviewFrame pf=new BuPreviewFrame
	    (_app,images,frame,_name,_composant);
	  _app.addInternalFrame(pf);
	  _app.activateInternalFrame(pf);
	}
      };

      SwingUtilities.invokeLater(runnable);
    }
  }

  public static void printHeader(PrintJob _job,Graphics _g)
  {
    printHeader(_job,_g,-1,-1);
  }

  public static void printHeader(PrintJob _job,Graphics _g, int _page)
  {
    printHeader(_job,_g,_page,-1);
  }

  public static void printHeader(PrintJob _job,Graphics _g, int _page, int _nbpages)
  {
    if(INFO_DOC==null) return;

    int pw  =_job.getPageDimension().width;
    //int ph  =_job.getPageDimension().height;
    int pres=_job.getPageResolution();

    int marge=pres*2/5; // 1 cm

    int x=0;
    int y=0;

    if(INFO_DOC.logo!=null)
    {
      INFO_DOC.logo.paintIcon(null,_g,0,0);
      x+=INFO_DOC.logo.getIconWidth()+5;
    }

    Font ft;
    FontMetrics fm;

    _g.setColor(Color.black);

    ft=new Font(FONTBASE.getFamily(),Font.BOLD,FONTBASE.getSize());
    _g.setFont(ft);
    fm=_g.getFontMetrics();
    y+=fm.getMaxAscent();
    _g.drawString(INFO_DOC.name+" "+INFO_DOC.version,x,y);
    y+=fm.getMaxDescent();

    ft=FONTBASE;
    _g.setFont(ft);
    fm=_g.getFontMetrics();
    y+=fm.getMaxAscent();
    _g.drawString(INFO_DOC.date,x,y);
    y+=fm.getMaxDescent();

    ft=new Font(FONTBASE.getFamily(),Font.PLAIN,FONTBASE.getSize()-2);
    _g.setFont(ft);
    fm=_g.getFontMetrics();
    y+=fm.getMaxAscent();
    _g.drawString(INFO_DOC.author+" "+INFO_DOC.contact,x,y);
    y+=fm.getMaxDescent();

    if(_page>0)
    {
      String s="Page: "+_page;
      if(_nbpages>0) s+="/"+_nbpages;
      System.err.println("Printing "+s);

      ft=FONTBASE;
      _g.setFont(ft);
      fm=_g.getFontMetrics();
      y=fm.getMaxAscent();
      x=pw-2*marge-fm.stringWidth(s);
      _g.drawString(s,x,y);
    }
  }

  public static void printFooter(PrintJob _job,Graphics _g)
  {
    if(INFO_LOG==null) return;

    //int pw  =_job.getPageDimension().width;
    //int ph  =_job.getPageDimension().height;
    // int pres=_job.getPageResolution();

    //int marge=pres*2/5; // 1 cm

    int x=0;
    int y=0;

    if(INFO_LOG.logo!=null)
    {
      INFO_LOG.logo.paintIcon(null,_g,0,0);
      x+=INFO_LOG.logo.getIconWidth()+5;
    }

    Font ft;
    FontMetrics fm;

    _g.setColor(Color.black);

    ft=new Font(FONTBASE.getFamily(),Font.BOLD,FONTBASE.getSize());
    _g.setFont(ft);
    fm=_g.getFontMetrics();
    y+=fm.getMaxAscent();
    _g.drawString(INFO_LOG.name+" "+INFO_LOG.version,x,y);
    y+=fm.getMaxDescent();

    /*
    ft=FONTBASE;
    _g.setFont(ft);
    fm=_g.getFontMetrics();
    y+=fm.getMaxAscent();
    _g.drawString(INFO_LOG.date,x,y);
    y+=fm.getMaxDescent();
    */

    ft=new Font(FONTBASE.getFamily(),Font.PLAIN,FONTBASE.getSize()-2);
    _g.setFont(ft);
    fm=_g.getFontMetrics();
    y+=fm.getMaxAscent();
    _g.drawString(INFO_LOG.http,x,y);
    y+=fm.getMaxDescent();
  }

  public static void printComponent
    (PrintJob _job,Graphics _g, Component _c)
  {
    printComponent(_job,_g,_c,-1,-1);
  }

  public static void printComponent
    (PrintJob _job,Graphics _g, Component _c, int _page, int _nbpages)
  {
    int nbpages=_nbpages;
    int page=_page;
    if(nbpages<2) page=nbpages=-1;

    int pw  =_job.getPageDimension().width;
    int ph  =_job.getPageDimension().height;
    int pres=_job.getPageResolution();

    int marge=pres*2/5; // 1 cm

    _g.setFont(FONTBASE);
    _g.setColor(Color.black);

    FontMetrics fm=_g.getFontMetrics();
    int x,y;
    int h=fm.getMaxAscent()+fm.getMaxDescent();

    x=marge;
    y=marge;
    _g.translate(x,y);
    printHeader(_job,_g,page,nbpages);
    _g.translate(-x,-y);
    
    x=marge;
    y=ph-marge-64;
    _g.translate(x,y);
    printFooter(_job,_g);
    _g.translate(-x,-y);

    x=marge;
    y=marge+64+2*h;
    _g.translate(x,y);

    synchronized(_c)
    {
      Dimension dc    =_c.getSize();
      int       wo    =dc.width;
      int       ho    =dc.height;
      int       wi    =wo;
      int       hi    =ho;
      boolean   reduit=false;

      int wmax=pw-2*marge;
      int hmax=ph-2*(marge+64+2*h);

      if(wi>wmax)
      {
	//hi=hi*wmax/wi;
	wi=wmax;
	reduit=true;
      }
      if(hi>hmax)
      {
	//wi=wi*hmax/hi;
	hi=hmax;
	reduit=true;
      }

      Shape     s=_g.getClip();
  //    Dimension d=_c.getSize();
      _g.clipRect(0,0,wi,hi);
      if(reduit)
      {
	_c.setSize(wi,hi);
	_c.invalidate();
	_c.doLayout();
	_c.validate();
      }
      _c.printAll(_g);
      if(reduit)
      {
	_c.setSize(wo,ho);
	_c.invalidate();
	_c.doLayout();
	_c.validate();
      }
      _g.setClip(s);
    }

    _g.translate(-x,-y);
  }
    
  public static void printRaw
    (PrintJob _job, Graphics _g, JPanel _p)
  {
    _p.printAll(_g);
    /*
    Component[] c=_p.getComponents();
    for(int i=0;i<c.length;i++)
      if(c[i].isVisible())
      {
	int x=c[i].getX();
	int y=c[i].getY();
	System.err.println("_g="+_g+" "+x+" "+y);
	_g.translate(x,y);
	c[i].print(_g);
	_g.translate(-x,-y);
      }
    */
  }

  public static void printImage
    (PrintJob _job, Graphics _g, Image _c)
  {
    printImage(_job,_g,_c,-1,-1);
  }

  public static void printImage
    (PrintJob _job, Graphics _g, Image _c, int _page, int _nbpages)
  {
    BuPicture pix=new BuPicture();
    pix.setBackground(Color.white);
    pix.setOpaque(true);
    pix.setMode(BuPicture.REDUCE);
    pix.setSmooth(false);
    pix.setImage(_c);
    //pix.setBorder(new javax.swing.border.LineBorder(Color.black,10));
    pix.computePreferredSize();
    pix.setSize(pix.getPreferredSize());
    printComponent(_job,_g,pix,_page,_nbpages);

    /*
    if(_nbpages<2) _page=_nbpages=-1;

    int pw  =_job.getPageDimension().width;
    int ph  =_job.getPageDimension().height;
    int pres=_job.getPageResolution();

    int marge=pres*2/5; // 1 cm

    _g.setFont(FONTBASE);
    _g.setColor(Color.black);

    FontMetrics fm=_g.getFontMetrics();
    int x,y;
    int h=fm.getMaxAscent()+fm.getMaxDescent();

    x=marge;
    y=marge;
    _g.translate(x,y);
    printHeader(_job,_g,_page,_nbpages);
    _g.translate(-x,-y);
    
    x=marge;
    y=ph-marge-64;
    _g.translate(x,y);
    printFooter(_job,_g);
    _g.translate(-x,-y);

    x=marge;
    y=marge+64+2*h;
    _g.translate(x,y);

    int     wi     =_c.getWidth(null);
    int     hi     =_c.getHeight(null);
    boolean reduite=false;

    int wmax=pw-2*marge;
    int hmax=ph-2*(marge+64+2*h);

    if(wi>wmax)
    {
      hi=hi*wmax/wi;
      wi=wmax;
      reduite=true;
    }
    if(hi>hmax)
    {
      wi=wi*hmax/hi;
      hi=hmax;
      reduite=true;
    }

    Shape   s=_g.getClip();
    _g.clipRect(0,0,wi,hi);
    if(reduite) _c=_c.getScaledInstance(wi,hi,Image.SCALE_SMOOTH);
    new BuIcon(_c).paintIcon(null,_g,0,0);
    //if(reduite) _g.drawImage(_c,0,0,wi,hi,null);
    //else        _g.drawImage(_c,0,0,null);
    _g.setClip(s);

    _g.translate(-x,-y);
    */
  }
    
  public static void printString
    (PrintJob _job, Graphics _g, String _t)
  {
    printString(_job,_g,_t,FONTBASE);
  }

  public static void printString
    (PrintJob _job, Graphics _g, String _t, Font _font)
  {
    Graphics g=_g;
    String t=_t;
    int pw  =_job.getPageDimension().width;
    int ph  =_job.getPageDimension().height;
    int pres=_job.getPageResolution();

    int marge=pres*2/5; // 1 cm
    int page =1;

    g.setFont(_font);
    g.setColor(Color.black);

    FontMetrics fm=g.getFontMetrics();
    int x,y;
    int h=fm.getMaxAscent()+fm.getMaxDescent();

    x=marge;
    y=marge;
    g.translate(x,y);
    printHeader(_job,g,page);
    g.translate(-x,-y);

    x=marge;
    y=ph-marge-64;
    g.translate(x,y);
    printFooter(_job,g);
    g.translate(-x,-y);

    g.setFont(_font);

    x=marge;
    y=marge+64+h;
    
    while(!t.equals(""))
    {
      int    i=t.indexOf("\n");
      String s;
      if(i>=0) { s=t.substring(0,i); t=t.substring(i+1); }
      else     { s=t; t=""; }

      if(fm.stringWidth(s)<pw-2*marge)
      {
	g.drawString(s,x,y);
	y+=h;
      }
      else
      {
	while(!"".equals(s))
	{
	  int j=s.length();
	  while(  (j>=0)
		&&(fm.stringWidth(s.substring(0,j))
		   >=pw-2*marge)) j--;
	  if(j==0) break;
	  g.drawString(s.substring(0,j),x,y);
	  y+=h;
	  s=s.substring(j);
	}
      }

      if(y>ph-marge-64-h)
      {
	g.dispose();
	g=_job.getGraphics();
	g.setFont(_font);
	g.setColor(Color.black);

	fm=g.getFontMetrics();
	h=fm.getMaxAscent()+fm.getMaxDescent();
	page++;

	x=marge;
	y=marge;
	g.translate(x,y);
	printHeader(_job,g,page);
	g.translate(-x,-y);

	x=marge;
	y=ph-marge-64;
	g.translate(x,y);
	printFooter(_job,g);
	g.translate(-x,-y);

	g.setFont(_font);

	x=marge;
	y=marge+64+h;
      }
    }
  }

  public static void printStyledDocument(PrintJob _job, Graphics _g, StyledDocument _doc)
  {
    Graphics g=_g;
    //int pw  =_job.getPageDimension().width;
    int ph  =_job.getPageDimension().height;
    int pres=_job.getPageResolution();

    int marge=pres*2/5; // 1 cm
    int page =1;

    g.setFont(FONTBASE);
    g.setColor(Color.black);

    FontMetrics fm=g.getFontMetrics();
    int x,y;
    int h=fm.getMaxAscent()+fm.getMaxDescent();

    x=marge;
    y=marge;
    g.translate(x,y);
    printHeader(_job,g,page);
    g.translate(-x,-y);

    x=marge;
    y=ph-marge-64;
    g.translate(x,y);
    printFooter(_job,g);
    g.translate(-x,-y);

    x=marge;
    y=marge+64+h;
    
    synchronized(_doc)
    {

    Element root=_doc.getDefaultRootElement();
    int     nbl =root.getElementCount();

    for(int i=0;i<nbl;i++)
    {
      Element line=root.getElement(i);
      int     nbw =line.getElementCount();
      int     hl  =0;

      for(int j=0;j<nbw;j++)
      {	
	Element word=line.getElement(j);
	String t="";
	try
	{
	  int start=word.getStartOffset();
	  int end  =word.getEndOffset();
	  t=_doc.getText(start,end-start);
	  t=FuLib.replace(t,"\t","        ");
	}
	catch(BadLocationException ex) { }
	//System.err.println(word+" ["+replace(t,"\n","")+"]");

	AttributeSet attributes=word.getAttributes();
	g.setColor(_doc.getForeground(attributes));
	g.setFont (BuLib.deriveFont(_doc.getFont(attributes),-2));
	g.drawString(t,x,y);

	fm=g.getFontMetrics();
	x+=fm.stringWidth(t);
	hl=Math.max(hl,fm.getMaxAscent()+fm.getMaxDescent());
      }

      x=marge;
      y+=hl;

      if(y>ph-marge-64-h)
      {
	g.dispose();
	g=_job.getGraphics();
	g.setFont(FONTBASE);
	g.setColor(Color.black);

	fm=g.getFontMetrics();
	h=fm.getMaxAscent()+fm.getMaxDescent();
	page++;

	x=marge;
	y=marge;
	g.translate(x,y);
	printHeader(_job,g,page);
	g.translate(-x,-y);

	x=marge;
	y=ph-marge-64;
	g.translate(x,y);
	printFooter(_job,g);
	g.translate(-x,-y);

	x=marge;
	y=marge+64+h;
      }
    }

    } // end of synchronized
  }

  private static void printCell
    (Graphics _g, FontMetrics _fm, String _s, int _x, int _y, int _w1)
  {
    String s=_s;
    int ws=_fm.stringWidth(s)+4;
    
    if(ws>_w1)
    {
      while((!"".equals(s))&&(_fm.stringWidth(s+"...")+4>_w1))
	s=s.substring(0,s.length()-1);
      s+="...";
    }

    _g.drawString(s,_x+2,_y+2+_fm.getMaxAscent());
  }
    
  public static void printTable(PrintJob _job, Graphics _g, JTable _t)
  { printTable(_job,_g,_t,-1); }

  public static void printTable(PrintJob _job, Graphics _g, JTable _t, int _height)
  {
    Graphics g=_g;
    int pw  =_job.getPageDimension().width;
    int ph  =_job.getPageDimension().height;
    int pres=_job.getPageResolution();

    int marge=pres*2/5; // 1 cm
    int page =1;

    g.setFont(FONTBASE);

    FontMetrics fm=g.getFontMetrics();
    int x,y;

    int h1=fm.getMaxAscent()+fm.getMaxDescent()+4;
    int w1=(pw-2*marge)/_t.getColumnCount();

    x=marge;
    y=marge;
    g.translate(x,y);
    printHeader(_job,g,page);
    g.translate(-x,-y);

    x=marge;
    y=ph-marge-64;
    g.translate(x,y);
    printFooter(_job,g);
    g.translate(-x,-y);

    g.setFont(FONTBASE);

    x=marge;
    y=marge+64+h1;

    final Color GRIS1=new Color(192,192,192);
    final Color GRIS2=new Color(224,224,224);

    synchronized(_t)
    {

    for(int j=0;j<_t.getColumnCount();j++)
    {
      g.setColor(GRIS1);
      g.fillRect(x,y,w1,h1);
      g.setColor(Color.black);
      g.drawRect(x,y,w1,h1);
	
      String s=_t.getColumnName(j);
      printCell(g,fm,s,x,y,w1);
      x+=w1;
    }

    x=marge;
    y+=h1;

    for(int i=0;i<_t.getRowCount();i++)
    {
      h1=Math.max(h1,_height);

      for(int j=0;j<_t.getColumnCount();j++)
      {
	g.setColor(i%2==1 ? GRIS2 : Color.white);
	g.fillRect(x,y,w1,h1);
	g.setColor(Color.black);
	g.drawRect(x,y,w1,h1);
	
	Object s=_t.getValueAt(i,j);
 	if(s instanceof Icon)
 	  ((Icon)s).paintIcon(_t,g,x+2,y+2);
 	else
	if(s!=null)
 	  printCell(g,fm,s.toString(),x,y,w1);

	x+=w1;
      }

      x=marge;
      y+=h1;

      if(y>ph-marge-64-h1)
      {
	g.dispose();
	g=_job.getGraphics();
	g.setFont(FONTBASE);
	g.setColor(Color.black);

	fm=g.getFontMetrics();
	h1=fm.getMaxAscent()+fm.getMaxDescent()+4;
	w1=(pw-2*marge)/_t.getColumnCount();
	page++;

	x=marge;
	y=marge;
	g.translate(x,y);
	printHeader(_job,g,page);
	g.translate(-x,-y);

	x=marge;
	y=ph-marge-64;
	g.translate(x,y);
	printFooter(_job,g);
	g.translate(-x,-y);

	g.setFont(FONTBASE);

	x=marge;
	y=marge+64+h1;

	for(int j=0;j<_t.getColumnCount();j++)
	{
	  g.setColor(GRIS1);
	  g.fillRect(x,y,w1,h1);
	  g.setColor(Color.black);
	  g.drawRect(x,y,w1,h1);
	
	  String s=_t.getColumnName(j);
	  printCell(g,fm,s,x,y,w1);
	  x+=w1;
	}

	x=marge;
	y+=h1;
      }
    }

    } //end of synchronized
  }
}
