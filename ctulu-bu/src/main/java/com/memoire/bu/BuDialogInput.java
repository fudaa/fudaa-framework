/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuDialogInput.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A standard dialog for input.
 */
public class BuDialogInput
  extends BuDialog
  implements ActionListener
{
  protected BuButton         btValider_;
  protected BuButton         btAnnuler_;
  protected BuTextField      tfValeur_;
  protected BuLabelMultiLine lbValeur_;

  public BuDialogInput(BuCommonInterface      _parent,
		       BuInformationsSoftware _isoft,
		       String                 _title,
		       String                 _label,
		       String                 _value)
  {
    super(_parent,_isoft,_title);

    tfValeur_.setText(_value);
    lbValeur_.setText(_label);
    valeur_=null;
    
    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btValider_=new BuButton(BuResource.BU.loadButtonCommandIcon("VALIDER"),
                            getString("Valider"));
    btValider_.addActionListener(this);
    getRootPane().setDefaultButton(btValider_);
    pnb.add(btValider_);

    btAnnuler_=new BuButton(BuResource.BU.loadButtonCommandIcon("ANNULER"),
                            getString("Annuler"));
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  @Override
  public JComponent getComponent()
  {
    tfValeur_=new BuTextField();
    tfValeur_.setColumns(20);
    tfValeur_.addActionListener(this);

    lbValeur_=new BuLabelMultiLine();

    BuPanel p=new BuPanel();
    p.setLayout(new BuVerticalLayout());
    p.setBorder(EMPTY5555);
    p.add(lbValeur_);
    p.add(tfValeur_);
    p.add(new BuPanel());
    return p;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialogInput : "+source);

    if((source==btValider_)||(source==tfValeur_))
    {
      reponse_=JOptionPane.OK_OPTION;
      valeur_ =tfValeur_.getText();
      setVisible(false);
    }

    if(source==btAnnuler_)
    {
      reponse_=JOptionPane.CANCEL_OPTION;
      valeur_ =null;
      setVisible(false);
    }
  }

  private String valeur_;

  public String getValue()
    { return valeur_; }

  public void setValue(String _value)
  {
    tfValeur_.setText(_value);
    valeur_=_value;
  }
}

