/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         BuInformationsUser.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * OBSOLETE. Replace by BuInformationsPerson.
 * Basic informations for a user.
 * @deprecated replaced by BuInformationsPerson
 */
public class BuInformationsUser
       extends Object
{
  public String lastname;
  public String firstname;
  public String nickname;
  public String email;
  public String title;
  public String organization;
  public String department;

  public BuInformationsUser()
  {
    lastname    =System.getProperty("user.name");
    firstname   ="";
    nickname    ="";
    email       ="";
    title       ="";
    organization="";
    department  ="";
  }

  public String about()
  {
    return lastname;
  }

  public String toString()
  {
    String r="User[";
    r+=lastname;
    r+="]";
    return r;
  }
}




