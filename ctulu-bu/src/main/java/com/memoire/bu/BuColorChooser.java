/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuColorChooser.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.colorchooser.ColorSelectionModel;

/**
 * Same as JColorChooser.
 */
public class BuColorChooser
  extends JColorChooser
{
  public BuColorChooser()
  {
  }

  public BuColorChooser(Color _color)
  {
    super(_color);
  }

  public BuColorChooser(ColorSelectionModel _model) 
  {
    super(_model);
  }

  public static void main(String[] _args)
  {
    BuColorChooser p=new BuColorChooser();
    p.setBorder(BuLib.getEmptyBorder(0));
    p.setDragEnabled(true);
    p.setOpaque(false);
    BuPanel q=new BuPanel(new BuBorderLayout());
    q.add(p,BuBorderLayout.CENTER);
    p.setBorder(BuLib.getEmptyBorder(5));
    JFrame f=new JFrame("Color Chooser");
    f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    f.setContentPane(q);
    f.pack();
    f.setVisible(true);
  }
}
