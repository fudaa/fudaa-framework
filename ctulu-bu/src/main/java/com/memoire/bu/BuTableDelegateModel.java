package com.memoire.bu;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

public abstract class BuTableDelegateModel
  extends AbstractTableModel 
  implements TableModelListener
{
  protected TableModel model_; 

  public TableModel getModel()
  {
    return model_;
  }

  public void setModel(TableModel _model)
  {
    if(model_!=null) model_.removeTableModelListener(this);
    model_=_model; 
    model_.addTableModelListener(this); 
  }

  // By default, implement TableModel by forwarding all messages 
  // to the model. 

  @Override
  public Object getValueAt(int _row, int _column)
  {
    return model_.getValueAt(_row,_column); 
  }
        
  @Override
  public void setValueAt(Object aValue, int _row, int _column)
  {
    model_.setValueAt(aValue, _row, _column); 
  }
  
  @Override
  public int getRowCount()
  {
    return (model_ == null) ? 0 : model_.getRowCount(); 
  }
  
  @Override
  public int getColumnCount()
  {
    return (model_ == null) ? 0 : model_.getColumnCount(); 
  }
  
  @Override
  public String getColumnName(int _column)
  {
    return model_.getColumnName(_column); 
  }
  
  @Override
  public Class getColumnClass(int _column)
  {
    return model_.getColumnClass(_column); 
  }
  
  @Override
  public boolean isCellEditable(int _row, int _column)
  { 
    return model_.isCellEditable(_row,_column); 
  }
  
  //
  // Implementation of the TableModelListener interface, 
  //

  // By default forward all events to all the listeners. 
  @Override
  public void tableChanged(TableModelEvent _evt)
  {
    fireTableChanged(_evt);
  }
}
