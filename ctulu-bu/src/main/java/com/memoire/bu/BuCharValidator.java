/**
 * @modification $Date: 2005-08-16 12:58:04 $
 * @statut unstable
 * @file BuCharValidator.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

/**
 * An abstract root class to specify a validator for input chars.
 * Provides standard ones for ints, doubles, dates, ...
 */
public abstract class BuCharValidator {
  public abstract boolean isCharValid(char _char);

  // Instances
  public static final BuCharValidator INTEGER = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char);
    }
  };
  public static final BuCharValidator INTEGER_NEGATIVE = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return '-' == _char || Character.isDigit(_char);
    }
  };
  public static final BuCharValidator LONG = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char);
    }
  };
  public static final BuCharValidator FLOAT = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char)
          || (".,+-Ee".indexOf(_char) >= 0);
    }
  };
  public static final BuCharValidator DOUBLE = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char)
          || (".,+-Ee".indexOf(_char) >= 0);
    }
  };
  public static final BuCharValidator HOST = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char)
          || (".-".indexOf(_char) >= 0)
          || ((_char >= 'a') && (_char <= 'z'))
          || ((_char >= 'A') && (_char <= 'Z'));
    }
  };
  public static final BuCharValidator ID = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isJavaIdentifierPart(_char);
    }
  };
  public static final BuCharValidator FILE = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isLetterOrDigit(_char)
          || ((System.getProperty("file.separator") + ".").indexOf(_char) >= 0);
    }
  };
  public static final BuCharValidator DATE = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char)
          || (_char == '/');
    }
  };
  public static final BuCharValidator TIME = new BuCharValidator() {
    @Override
    public boolean isCharValid(char _char) {
      return Character.isDigit(_char)
          || (_char == ':');
    }
  };
}


