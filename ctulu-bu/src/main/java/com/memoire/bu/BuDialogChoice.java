/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuDialogChoice.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A standard dialog for input.
 */
public class BuDialogChoice
       extends BuDialog
  implements ActionListener, BuBorders
{
  protected BuButton         btValider_;
  protected BuButton         btAnnuler_;
  protected BuComboBox       chValeur_;
  

  protected BuLabelMultiLine lbValeur_;
  protected BuLabelMultiLine lbCommentaire_;

  public BuDialogChoice(BuCommonInterface      _parent,
			BuInformationsSoftware _isoft,
			String                 _title,
			String                 _label,
			String[]               _values)
  {
    this(_parent,_isoft,_title,_label,_values,null,false,null);
  }

  public BuDialogChoice(BuCommonInterface      _parent,
			BuInformationsSoftware _isoft,
			String                 _title,
			String                 _label,
			String[]               _values,
			String                 _value,
			boolean                _editable,
			String                 _comment)
  {
    super(_parent,_isoft,_title);
//chValeur initialise par getComponent qui est appele par BUDialog
    chValeur_.removeAllItems();
    for(int i=0;i<_values.length;i++) chValeur_.addItem(_values[i]);
    String                 value=_value;
    if((value==null)&&(_values.length>0)) value=_values[0];
    chValeur_.setEditable(_editable);
    chValeur_.setSelectedItem(value);
    lbValeur_.setText(_label);
    valeur_=value;

    lbCommentaire_.setText(_comment);
    
    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btValider_=new BuButton(BuResource.BU.loadButtonCommandIcon("VALIDER"),
                            getString("Valider"));
    btValider_.addActionListener(this);
    getRootPane().setDefaultButton(btValider_);
    pnb.add(btValider_);

    btAnnuler_=new BuButton(BuResource.BU.loadButtonCommandIcon("ANNULER"),
                            getString("Annuler"));
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  public BuComboBox getChValeur() {
    return chValeur_;
  }
  
  @Override
  public JComponent getComponent()
  {
    chValeur_=new BuComboBox();
    //tfValeur_.setColumns(20);
    //chValeur_.addActionListener(this);

    lbValeur_     =new BuLabelMultiLine();
    lbCommentaire_=new BuLabelMultiLine();

    BuPanel p=new BuPanel();
    p.setLayout(new BuVerticalLayout());
    p.setBorder(EMPTY5555);
    p.add(lbValeur_);
    p.add(chValeur_);
    p.add(lbCommentaire_);
    p.add(new BuPanel());
    return p;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialogChoice : "+source);

    if(source==btValider_)//||(source==chValeur_))
    {
      reponse_=JOptionPane.OK_OPTION;
      valeur_ =(String)chValeur_.getSelectedItem();
      setVisible(false);
    }

    if(source==btAnnuler_)
    {
      reponse_=JOptionPane.CANCEL_OPTION;
      valeur_ =null;
      setVisible(false);
    }
  }

  private String valeur_;

  public String getValue()
    { return valeur_; }

  public int getSelectedIndex() {
    return chValeur_.getSelectedIndex();
  }
  
  public void setValue(String _value)
  {
    chValeur_.setSelectedItem(_value);
    valeur_=_value;
  }
}

