/**
 * @modification $Date: 2006-09-19 14:35:08 $
 * @statut       unstable
 * @file         BuTaskView.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

/**
 * A simple list to display running tasks.
 */
public class BuTaskView
  extends BuEmptyList
{
  protected DefaultListModel model_;

  public BuTaskView()
  {
    super();
    setName("buTASKVIEW");
    model_=new DefaultListModel();
    setModel(model_);
    setEmptyText(getString("Aucune t�che en cours"));
    setCellRenderer(new XCR());
    //setFont(BuLib.deriveFont("List",Font.PLAIN,-2));
  }

  @Override
  public void setFont(Font _f)
  {
    super.setFont(getETF());
  }

  public void addTask(BuTask _t)
  {
    model_.addElement(_t);
    revalidate();
    repaint(200);
  }

  public void removeTask(BuTask _t)
  {
    model_.removeElement(_t);
    revalidate();
    repaint(200);
  }

  static final class XCR //Cell
    extends BuLabel
    implements ListCellRenderer
  {
    private int value_;

    public void setTask(BuTask _task)
    {
      value_=_task.getProgression();

      String p;
      switch(_task.getPriority())
      {
      case Thread.MIN_PRIORITY:    p="-"; break;
      case Thread.MIN_PRIORITY+1:  p="-"; break;
      case Thread.NORM_PRIORITY-1: p="="; break;
      case Thread.NORM_PRIORITY:   p="="; break;
      case Thread.MAX_PRIORITY:    p="+"; break;
      default:                     p="?"; break;
      }
      if(_task.isInterrupted())	p="~";
      if(!_task.isAlive())      p="!";

      setText(" "+p+" "+_task.toString());
    }

    @Override
    public final void paintComponent(Graphics _g)
    {
      Insets insets=this.getInsets();
      Color fg=UIManager.getColor("ProgressBar.foreground");
      Color bg=UIManager.getColor("ProgressBar.background");
      if(fg==null) fg=this.getForeground();
      if(bg==null) bg=this.getBackground();

      int v=value_;
      int vmin=0;
      int vmax=100;
      String s=this.getText();
      int    xs=insets.left;
      int    ys=insets.top+this.getFont().getSize();

      int wb=this.getWidth ()-insets.left-insets.right;
      int hb=this.getHeight()-insets.top-insets.bottom;
      int xb=(v-vmin)*wb/(vmax-vmin);

      Shape old=_g.getClip();
      _g.clipRect(insets.left+xb,insets.top,wb-xb,hb);
      if(isOpaque())
      {
        Rectangle r=_g.getClipBounds();
        _g.setColor(bg);
        _g.fillRect(r.x, r.y, r.width, r.height);
      }

      Color xfg=UIManager.getColor("ProgressBar.selectionBackground");
      if(xfg==null) xfg=fg;
      _g.setColor(xfg);
      _g.drawString(s,xs,ys);
      _g.setClip(old);

      _g.clipRect(insets.left,insets.top,xb,hb);
      {
        Rectangle r=_g.getClipBounds();
        _g.setColor(fg);
        _g.fillRect(r.x, r.y, r.width, r.height);
      }

      Color xbg=UIManager.getColor("ProgressBar.selectionForeground");
      if(xbg==null) xbg=bg;
      _g.setColor(xbg);
      _g.drawString(s,xs+1,ys);
      _g.setClip(old);
    }

    /*
    public void paint(Graphics _g)
    {
      super.paint(_g);

      Dimension size=this.getSize();
      int w=task_.getProgression()*size.width/100;

      Color c=UIManager.getColor("ProgressBar.foreground");
      if(c==null) c=this.getForeground();
      _g.setXORMode(c);
      _g.setColor(this.getBackground());

      int cl=1,cs=1;
      try
      {
	cl=((Integer)UIManager.get("ProgressBar.cellLength")).intValue();
        cs=((Integer)UIManager.get("ProgressBar.cellSpacing")).intValue();
      }
      catch(Exception ex) { }

      if(cl==1)
	_g.fillRect(0,0,w,size.height);
      else
	for(int x=0;x+cl<=w;x+=cl+cs+1)
	  _g.fillRect(x,0,cl+1,size.height);

      if((cs==0)&&(BuLib.jdk()<1.2))
      {
	Rectangle old=_g.getClipBounds();
	_g.setClip(0,0,w,size.height);
	super.paint(_g);
	_g.setClip(old);
	_g.setPaintMode();
      }
      else
      {
	_g.setPaintMode();
	super.paint(_g);
      }
    }
    */

    @Override
    public final Component getListCellRendererComponent
      (JList _list, Object _value, int _index, boolean _selected, boolean _cellHasFocus)
    {
      setTask((BuTask)_value);
      this.setFont(_list.getFont());
      this.setBackground(_list.getBackground());
      this.setForeground(_list.getForeground());
      return this;
    }
  }

  public static void main(String[] _args) {
    try {
//      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//      UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
//      UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
      UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
    }
    catch (Exception ex) {}

    final JFrame f=new JFrame();
    BuTaskView tv=new BuTaskView();
    tv.setPreferredSize(new Dimension(300,50));
    JPanel pn=new JPanel();
    pn.setLayout(new BorderLayout());
    pn.add(tv,BorderLayout.CENTER);
    f.getContentPane().add(pn);
    f.pack();
    f.setLocationRelativeTo(null);

    BuTaskOperation ope=new BuTaskOperation(null, "Longue tache en cours") {
      @Override
      public void act() {
        for (int i=0; i<10; i++) {
          this.setProgression((i+1)*10);
          try {
            Thread.sleep(300);
          }
          catch (Exception e) {}
        }
//        System.out.println("End");
//        System.exit(0);
      }
    };
    ope.setTaskView(tv);
    ope.start();
    f.setVisible(true);
  }
  /*
  private static final class XCR
    implements ListCellRenderer, Serializable
  {
    private Cell cell_;

    public XCR()
    {
      cell_=new Cell();
    }

    public final Component getListCellRendererComponent
      (JList _list, Object _value, int _index, boolean _selected, boolean _cellHasFocus)
    {
      cell_.setTask((BuTask)_value);
      cell_.setFont(_list.getFont());
      cell_.setBackground(_list.getBackground());
      cell_.setForeground(_list.getForeground());
      return cell_;
    }
  }
  */
}
