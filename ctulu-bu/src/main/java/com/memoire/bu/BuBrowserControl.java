/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut       unstable
 * @file         BuBrowserControl.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.io.File;
import java.io.IOException;

/**
 * An utility class to control an external browser.
 * Enables to launch a browser with a specified url or to
 * modify the current displayed page.
 */

public class BuBrowserControl
{
  public static void displayURL(String _url)
  {
    String url=_url;
    if(url==null) url="http://www.fudaa.fr/";

    unixPath_=BuPreferences.BU.getStringProperty("browser.exec","");
    //dans le cas des anciennes préférences, le path etait un entier
    if(unixPath_.length()==1){
      try {
        Integer.parseInt(unixPath_);
        unixPath_="";
      }
      catch (NumberFormatException e) {
         //ce n'etait pas un entier
      }
    }

    if("".equals(unixPath_))
    {
      if(url.startsWith("mailto:"))
      {
	if(new File("/usr/bin/gnome-moz-remote").exists()) unixPath_="Gnome";
	if(new File("/usr/bin/kmail"           ).exists()) unixPath_="/usr/bin/kmail";
	if(new File("/usr/bin/evolution"       ).exists()) unixPath_="/usr/bin/evolution";
      }
      else
      {

	if(new File("/usr/bin/gnome-moz-remote").exists()) unixPath_="Gnome";
	if(new File("/usr/bin/konqueror"       ).exists()) unixPath_="Konqueror";
    if(new File("/usr/bin/firefox").exists()) unixPath_="Firefox";
	if(new File("/usr/bin/galeon"          ).exists()) unixPath_="Galeon";
      }
    }
    if("".equals(unixPath_)) unixPath_="Netscape";

    for(int i=0;i<LIST.length;i++)
      if(LIST[i][0].equals(unixPath_))
	unixPath_=LIST[i][1];

    boolean windows=isWindowsPlatform();
    String  cmd    =null;

    try
    {
      if(windows)
      {
	// cmd = 'rundll32 url.dll,FileProtocolHandler http://...'
	cmd=WIN_PATH+" "+WIN_FLAG+" "+url;
	/*Process p = */Runtime.getRuntime().exec(cmd);
      }
      else if(!unixPath_.endsWith("netscape"))
      {
	cmd=unixPath_+" "+url;
	/*Process p = */Runtime.getRuntime().exec(cmd);
      }
      else
      {
	// Under Unix, Netscape has to be running for the "-remote"
	// command to work.  So, we try sending the command and
	// check for an exit value.  If the exit command is 0,
	// it worked, otherwise we need to start the browser.

	// cmd = 'netscape -remote openURL(http://www.javaworld.com)'
	cmd=unixPath_+" "+UNIX_FLAG+"("+url+")";
	Process p = Runtime.getRuntime().exec(cmd);

	try
	{
	  // wait for exit code -- if it's 0, command worked,
	  // otherwise we need to start the browser up.
	  int exitCode = p.waitFor();

	  if(exitCode!=0)
	  {
	    // Command failed, start up the browser

	    // cmd = 'netscape http://www.javaworld.com'
	    cmd=unixPath_+" "+url;
	    /*p = */Runtime.getRuntime().exec(cmd);
	  }
	}
	catch(InterruptedException ex)
	{
	  //System.err.println("Error bringing up browser, "+cmd);
	  //System.err.println("Caught: " + ex);
	  String m="Error while bringing up the browser:\n"
	      +cmd+"\n"
	      +ex.getClass().getName()+"\n"
	      +ex.getMessage();
	  new BuDialogError(null,null,m).activate();
	}
      }
    }
    catch(IOException ex)
    {
      //System.err.println("Could not invoke browser, "+cmd);
      //System.err.println("Caught: " + ex);
      String m="Error while invoking the browser:\n"
	  +cmd+"\n"
	  +ex.getClass().getName()+"\n"
	  +ex.getMessage();
	  new BuDialogError(null,null,m).activate();
    }
  }

  public static boolean isWindowsPlatform()
  {
    String os=System.getProperty("os.name");
    return (os!=null)&&(os.startsWith(WIN_ID));
  }

  public static void main(String[] _args)
  {
    displayURL(_args.length>0 ? _args[0] : "http://www.jdistro.com/");
  }

  // Used to identify the windows platform.
  private static final String WIN_ID = "Windows";

  // The default system browser under windows.
  private static final String WIN_PATH = "rundll32";

  // The flag to display a url.
  private static final String WIN_FLAG = "url.dll,FileProtocolHandler";

  // The flag to display a url for netscape.
  private static final String UNIX_FLAG = "-remote openURL";

  // The default browser under unix.
  public static String unixPath_="netscape";

  /**
   * Commonly used browsers.
   */
  static final String[][] LIST=new String[][]
  {
    { "Firefox",    "firefox"                  },
    { "Galeon",    "galeon"                    },
    { "Gnome",     "gnome-moz-remote --newwin" },
    { "Konqueror", "konqueror"                 },
    { "Mozilla",   "mozilla"                   },
    { "Netscape",  "netscape"                  },
    { "Opera",     "opera"                     },
    { "Lynx",      "xterm -e lynx"             },
    { "W3m",       "xterm -e w3m"              },
    { "Amaya",     "amaya"                     },
    { "Chimera",   "chimera2"                  },
  };
}
