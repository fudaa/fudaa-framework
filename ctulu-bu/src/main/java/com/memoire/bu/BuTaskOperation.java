/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuTaskOperation.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A task which invokes a named method of an application.
 */
public class BuTaskOperation
  extends BuTask
  implements FuEmptyArrays
{
  private BuCommonInterface app_;
  private String            methode_;
  private String            param_;
  private Runnable          runnable_;
  private boolean           act_;

  public BuTaskOperation(BuCommonInterface _app,
			 String _nom, String _methode)
  { this(_app,_nom,false,null,_methode,null); }

  public BuTaskOperation(BuCommonInterface _app,
			 String _nom, String _methode, String _param)
  { this(_app,_nom,false,null,_methode,_param); }

  public BuTaskOperation(BuCommonInterface _app,
			 String _nom, Runnable _runnable)
  { this(_app,_nom,false,_runnable,"run",null); }

  public BuTaskOperation(BuCommonInterface _app, String _nom)
  { this(_app,_nom,true,null,"act",null); }

  private BuTaskOperation(BuCommonInterface _app,
			  String _nom, boolean _act,
			  Runnable _runnable,
			  String _methode, String _param)
  {
    super();
    app_     =_app;
    methode_ =_methode;
    param_   =_param;
    runnable_=_runnable;
    act_      =_act;
    String nom=_nom;

    if((nom==null)||"".equals(nom)) nom="Bu task operation";
    setName(nom);
    if(app_!=null) setTaskView(app_.getMainPanel().getTaskView());
    setPriority(Thread.MIN_PRIORITY);
  }

  @Override
  public void start()
  {
    super.start();
  }

  public void act()
  {
  }

  @Override
  public final void run()
  {
    try
    {
      if(runnable_!=null)
      {
	runnable_.run();
      }
      else
      if(act_)
      {
	act();
      }
      else
      if(param_==null)
      {
	Method  m=app_.getClass().getMethod(methode_,CLASS0);
	m.invoke(app_,OBJECT0);
      }
      else
      {
	Method  m=app_.getClass().getMethod
	  (methode_,new Class[] { String.class });
	m.invoke(app_,new Object[] { param_ });
      }

      super.run();
    }
    catch(Throwable ex)
    {
      String m="BuTaskOperation: "+methode_+
        (param_==null ? "" : " ("+param_+")");
      if(ex instanceof InvocationTargetException)
	ex=((InvocationTargetException)ex).getTargetException();
      FuLog.error(m,ex);
      //suspend();
    }
  }
}

