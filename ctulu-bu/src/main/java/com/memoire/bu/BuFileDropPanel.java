/**
 * @modification $Date: 2006-09-19 14:35:09 $
 * @statut       unstable
 * @file         BuFileDropPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import com.memoire.dt.DtDropReady;
import com.memoire.dt.DtFilesSelection;
import com.memoire.dt.DtLib;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

public abstract class BuFileDropPanel
  extends BuAbstractDropPanel
  implements DtDragSensible, DtDropReady
{
  public BuFileDropPanel()
  {
    super();
  }

  public BuFileDropPanel(LayoutManager _layout)
  {
    super(_layout);
  }

  protected abstract boolean dropFile(DtFilesSelection _s);

  @Override
  public final int getDropActions()
  {
    return DtLib.ALL;
  }

  @Override
  public final boolean checkDropFlavor(DataFlavor[] _f, Point _p)
  {
    return DtFilesSelection.canConvert(_f);
  }

  @Override
  public final Object getDropData(Transferable _t)
  {
    return DtFilesSelection.convert(_t);
  }

  @Override
  public final boolean dropData(int _a, Object _data, Point _p)
  {
    DtFilesSelection s=(DtFilesSelection)_data;
    boolean          r=false;

    if(s.size()>0) r=dropFile(s);
    return r;
  }
}
