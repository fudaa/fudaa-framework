/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuWizardDialog.java
 * @version      0.41
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.JTextComponent;

public class BuWizardDialog
  extends JDialog
  implements ActionListener, WindowListener
{
  protected JComponent    content_;
  protected BuHeaderPanel header_;
  protected BuPanel       view_;
  protected BuButtonPanel buttons_;
  protected BuWizardTask  task_;

  private final Updater UPDATER=new Updater();

  public BuWizardDialog(Frame _parent,BuWizardTask _task)
  {
    super(_parent);
    task_=_task;
    if(task_==null) task_=new BuWizardTaskSample();

    addWindowListener(this);

    setTitle(task_.getTaskTitle());

    content_ = (JComponent) getContentPane();
    content_.setLayout(new BorderLayout(5,5));

    header_ = new BuHeaderPanel();

    view_ = new BuPanel();
    view_.setLayout(new BorderLayout());
    view_.setBorder(new EmptyBorder(20,30,20,30));

    buttons_=new BuButtonPanel(BuButtonPanel.TERMINER,this);

    content_.add(header_ ,BorderLayout.NORTH);
    content_.add(view_   ,BorderLayout.CENTER);
    content_.add(buttons_,BorderLayout.SOUTH);

    installStep();
  }

  class Updater
    implements ActionListener, ListSelectionListener,
               TreeSelectionListener, DocumentListener
  {
    @Override
    public void actionPerformed(ActionEvent _evt)
    {
      update();
    }

    @Override
    public void valueChanged(ListSelectionEvent _evt)
    {
      update();
    }

    @Override
    public void valueChanged(TreeSelectionEvent _evt)
    {
      update();
    }

    @Override
    public void changedUpdate(DocumentEvent _evt)
    {
      update();
    }

    @Override
    public void insertUpdate(DocumentEvent _evt)
    {
      update();
    }

    @Override
    public void removeUpdate(DocumentEvent _evt)
    {
      update();
    }

    private void update()
    {
      buttons_.setDisabledButtons(task_.getStepDisabledButtons());
    }
  }

  private void addListeners()
  {
    Enumeration e=BuLib.getAllSubComponents(view_).elements();
    while(e.hasMoreElements())
    {
      Object c=e.nextElement();
      if(c instanceof AbstractButton)
        ((AbstractButton)c).addActionListener(UPDATER);
      else
      if(c instanceof JList)
        ((JList)c).addListSelectionListener(UPDATER);
      else
      if(c instanceof JTree)
        ((JTree)c).addTreeSelectionListener(UPDATER);
      else
      if(c instanceof JTextComponent)
        ((JTextComponent)c).getDocument().addDocumentListener(UPDATER);
      else
      if(c instanceof JTable)
        ((JTable)c).getSelectionModel().addListSelectionListener(UPDATER);
    }
  }

  private void removeListeners()
  {
    Enumeration e=BuLib.getAllSubComponents(view_).elements();
    while(e.hasMoreElements())
    {
      Object c=e.nextElement();
      if(c instanceof AbstractButton)
        ((AbstractButton)c).removeActionListener(UPDATER);
      else
      if(c instanceof JList)
        ((JList)c).removeListSelectionListener(UPDATER);
      else
      if(c instanceof JTree)
        ((JTree)c).removeTreeSelectionListener(UPDATER);
      else
      if(c instanceof JTextComponent)
        ((JTextComponent)c).getDocument().removeDocumentListener(UPDATER);
      else
      if(c instanceof JTable)
        ((JTable)c).getSelectionModel().removeListSelectionListener(UPDATER);
    }
  }

  protected void installStep()
  {
    if(task_.isFinished()) return;

    content_.setVisible(false);

    header_.setTitle(task_.getStepTitle());
    header_.setText (task_.getStepText ());
    header_.setIcon (new BuFixedSizeIcon(task_.getStepIcon(),64,64));

    removeListeners();

    view_.removeAll();
    view_.add(task_.getStepComponent(),BorderLayout.CENTER);

    buttons_.setButtons(task_.getStepButtons(),//|BuButtonPanel.DETRUIRE,
			this,
			task_.getStepDisabledButtons());
    BuButton db=buttons_.getDefaultButton();
    getRootPane().setDefaultButton(db);
    //if(db!=null) db.requestFocus();

    addListeners();

    content_.revalidate();
    content_.setVisible(true);
    view_.requestFocus();
    view_.transferFocus();
  }

  protected void nextStep()
  {
    task_.nextStep();
    if(!task_.isFinished())
      installStep();
  }

  protected void previousStep()
  {
    task_.previousStep();
    installStep();
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String action=_evt.getActionCommand();

    if("ABANDONNER".equals(action))
    {
      task_.cancelTask();
    }
    else
    if("PRECEDENT".equals(action))
    {
      previousStep();
    }
    else
    if("SUIVANT".equals(action))
    {
      nextStep();
    }
    else
    if("TERMINER".equals(action))
    {
      task_.doTask();
    }

    if(task_.isFinished())
    {
      setVisible(false);
      dispose();
    }
  }

  @Override
  public void windowClosing(WindowEvent _evt)
  {
    if(!task_.isFinished())
      task_.cancelTask();
  }

  @Override
  public void windowOpened     (WindowEvent _evt) { }
  @Override
  public void windowClosed     (WindowEvent _evt) { }
  @Override
  public void windowIconified  (WindowEvent _evt) { }
  @Override
  public void windowDeiconified(WindowEvent _evt) { }
  @Override
  public void windowActivated  (WindowEvent _evt) { }
  @Override
  public void windowDeactivated(WindowEvent _evt) { }

  /*
  public static void main(String[] _args)
  {
    BuWizardDialog w=new BuWizardDialog(null);
    w.pack();
    w.setModal(true);
    w.setLocation(300,200);
    w.show();
    System.exit(0);
  }
  */
}
