/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuMenuInterface.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

//import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JMenu;

public interface BuMenuInterface
  extends ActionListener
{
  void addActionListener(ActionListener _l);
  void removeActionListener(ActionListener _l);

  void computeMnemonics();
  void addSubMenu(JMenu _m,boolean _enabled);

  void        addSeparator();
  BuSeparator addSeparator(String _text);

  BuMenuItem addMenuItem(String _s,String _cmd,boolean _enabled);
  BuMenuItem addMenuItem(String _s,String _cmd,Icon _icon,
                         boolean _enabled);
  BuMenuItem addMenuItem(String _s,String _cmd,Icon _icon,
                         boolean _enabled,int _key);

  BuCheckBoxMenuItem addCheckBox(String _s,String _cmd,
                                 boolean _enabled,boolean _checked);
  BuCheckBoxMenuItem addCheckBox(String _s,String _cmd,Icon _icon,
                                 boolean _enabled,boolean _checked);

  BuRadioButtonMenuItem addRadioButton(String _s,String _cmd,
                                       boolean _enabled,boolean _checked);
  BuRadioButtonMenuItem addRadioButton(String _s,String _cmd,Icon _icon,
                                       boolean _enabled,boolean _checked);
}
