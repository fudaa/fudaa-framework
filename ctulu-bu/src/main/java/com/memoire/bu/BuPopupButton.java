/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut       unstable
 * @file         BuPopupButton.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.WindowConstants;

/**
 * A toggle button for toolbar which hides/shows a given component.
 */

public class BuPopupButton extends BuToggleButton implements ActionListener {
  private BuPalette window_;

  public BuPopupButton(JComponent _component) {
    this("NULL", null, _component);
  }

  public BuPopupButton(String _title, JComponent _component) {
    this("NULL", _title, _component);
  }

  public BuPopupButton(String _name, String _title, JComponent _component) {
    super();

    setName("pb" + _name);
    setRequestFocusEnabled(false);

    window_ = new BuPalette();
    window_.setName("pl" + _name);
    window_.setTitle(_title);
    window_.setContent(_component);
    // window_.pack();

    addActionListener(this);
  }

  /*
   * private static final int[] vtax=new int[] { 6,3,0 }; private static final int[] vtay=new int[] { 6,0,6 }; private
   * static final int[] vbax=new int[] { 0,3,6 }; private static final int[] vbay=new int[] { 0,6,0 }; public void
   * paintBorder(Graphics _g) { super.paintBorder(_g); Dimension size =getSize(); int x=size.width -7; int
   * y=size.height-7; _g.translate(x,y); _g.setColor(getForeground()); if(isSelected()) _g.fillPolygon(vtax,vtay,3);
   * else _g.fillPolygon(vbax,vbay,3); _g.translate(-x,-y); }
   */

  @Override
  public void actionPerformed(ActionEvent _evt) {
    if (isSelected()) showWindow();
    else
      hideWindow();
  }

  static int nbr_ = -1;
  private Point loc_ ;

  public void setPaletteLocation(int _x, int _y) {
    loc_ = new Point(_x, _y);
    window_.setLocation(loc_);
  }

  /**
   * Sets whether the associated palette is resizable.
   *
   * @param _b true if the palette is resizable; false otherwise
   * @deprecated a palette should not be resizable.
   */
  public void setPaletteResizable(boolean _b) {
    window_.setResizable(_b);
  }

  /**
   * Indicates whether the associated palette is resizable.
   *
   * @return true if the palette is resizable, false otherwise.
   */
  public boolean isPaletteResizable() {
    return window_.isResizable();
  }

  public Dimension getPalettePreferredSize() {
    return window_.getPreferredSize();
  }

  JDialog d;

  private void showWindow() {
    if (loc_ == null) {
      loc_ = window_.getLocation();
      if ((loc_.x == 0) && (loc_.y == 0)) {
        nbr_ = (nbr_ + 1) % 8;
        loc_ = new Point(5 + 20 * nbr_, 5 + 20 * nbr_);
        window_.setLocation(loc_);
      }
    }

    window_.pack();

    Icon i = getIcon();
    if (i != null) window_.setFrameIcon(i);

    // SwingUtilities.updateComponentTreeUI(window_);
    window_.setVisible(true);
    if (desktop_ != null) desktop_.addInternalFrame(window_);
    else {
      if(d==null){
      d = new JDialog();
      d.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      d.setContentPane(window_.getContentPane());
      d.pack();
      }
      d.setVisible(true);
    }
    window_.setLocation(loc_);
    window_.revalidate();
  }

  private void hideWindow() {
    loc_ = window_.getLocation();
    window_.setVisible(false);
    window_.revalidate();
    if (desktop_ != null) desktop_.removeInternalFrame(window_);
    else if(d!=null) d.dispose();
  }

  private BuDesktop desktop_;

  public BuDesktop getDesktop() {
    return desktop_;
  }

  public void setDesktop(BuDesktop _desktop) {
    desktop_ = _desktop;
  }

  @Override
  public void setVisible(final boolean _b) {
    super.setVisible(_b);

    if (_b) {
      if (isSelected()) showWindow();
    } else
      hideWindow();
  }
}
