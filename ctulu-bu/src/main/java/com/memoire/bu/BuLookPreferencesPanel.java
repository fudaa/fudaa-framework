/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuLookPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * A panel where the user can choose his favorite look.
 * (background, look and feel, icons, ...)
 *
 * desktop.decor
 * desktop.texture
 * desktop.background
 * lookandfeel.name
 * lookandfeel.class
 * lookandfeel.theme
 * icons.size
 */
public class BuLookPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener, ChangeListener
{
  BuCommonInterface     appli_;
  BuPreferences         options_;
  Hashtable             optionsStr_;
  BuOptionRenderer      cbRenderer_;

  //BuGridLayout   loGen_;
  BuPanel        pnGenDesk_;
  AbstractBorder boGenDesk_;
  //BuGridLayout   loGenDesk_;
  BuFormLayout   loGenDesk_;
  BuLabel        lbGenDeskDecor_, lbGenDeskTexture_, lbGenDeskCouleur_;
  BuComboBox     liGenDeskDecor_, liGenDeskTexture_, liGenDeskCouleur_;
  BuLabel        lbGenAspAspect_, lbGenAspSlaf_, lbGenAspMetal_;
  BuComboBox     liGenAspAspect_, liGenAspSlaf_, liGenAspMetal_;
  BuLabel        lbGenAspSkinlf_, lbGenAspOyoaha_;
  BuTextField    tfGenAspSkinlf_, tfGenAspOyoaha_;
  BuLabel        lbFont_;
  BuSlider       slFont_;

  @Override
  public String getTitle()
  {
    return getS("Aspect");
  }

  @Override
  public String getCategory()
  {
    return getS("Visuel");
  }

  public BuLookPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_     =_appli;
    options_   =BuPreferences.BU;
    cbRenderer_=new BuOptionRenderer();
    optionsStr_=new Hashtable();

    BuOptionItem itemOpt;
    String       itemStr;
    
    optionsStr_.put
      (itemStr="DESKTOP_UNIFORME",
       itemOpt=new BuOptionItem(getS("Uniforme"),
				BuResource.BU.getMenuIcon("uniforme")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_DEGRADE",
       itemOpt=new BuOptionItem(getS("D�grad�"),
				BuResource.BU.getMenuIcon("degrade")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_TEXTURE",
       itemOpt=new BuOptionItem(getS("Textur�"),
				BuResource.BU.getMenuIcon("texture")));
    optionsStr_.put(itemOpt, itemStr);

    for(int i=1;i<=9;i++)
    {
      optionsStr_.put
        (itemStr="DESKTOP_TEXTURE"+i,
         itemOpt=new BuOptionItem
         (getS("Texture")+" "+i,new BuTextureIcon(BuPreferences.BU.getTexture(i))));
      optionsStr_.put(itemOpt, itemStr);
    }

    optionsStr_.put
      (itemStr="DESKTOP_ROUGE",
       itemOpt=new BuOptionItem(getS("Rouge"),
				new BuColorIcon(new Color(128, 64, 64))));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_VERT",
       itemOpt=new BuOptionItem(getS("Vert"),
				new BuColorIcon(new Color( 64,129, 64))));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_BLEU",
       itemOpt=new BuOptionItem(getS("Bleu"),
				new BuColorIcon(new Color( 64, 64,128))));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_ORANGE",
       itemOpt=new BuOptionItem(getS("Orange"),
				new BuColorIcon(new Color(192,128, 96))));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_SIMILAIRE",
       itemOpt=new BuOptionItem(getS("Similaire"),
				new BuColorIcon(UIManager.getColor("Panel.background"))));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="DESKTOP_DEFAUT",
       itemOpt=new BuOptionItem(getS("D�faut"),
                    new BuColorIcon(UIManager.getColor("Panel.background"))));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_DEFAUT",
       itemOpt=new BuOptionItem(getS("D�faut"),
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_DEFAUT")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_AMIGA",
       itemOpt=new BuOptionItem("Amiga",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_AMIGA")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_FHLAF",
       itemOpt=new BuOptionItem("FHLaf",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_FHLAF")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_GTK",
       itemOpt=new BuOptionItem(getS("GTK"),
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_GTK")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_KUNSTSTOFF",
       itemOpt=new BuOptionItem("Kunststoff",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_KUNSTSTOFF")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_LIQUID",
       itemOpt=new BuOptionItem("Liquid",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_LIQUID")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_MAC",
       itemOpt=new BuOptionItem("Mac",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_MAC")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_METAL",
       itemOpt=new BuOptionItem("Metal",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_METAL")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_MOTIF",
       itemOpt=new BuOptionItem("Motif",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_MOTIF")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_NEXT",
       itemOpt=new BuOptionItem("Next",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_NEXT")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_ORGANIC",
       itemOpt=new BuOptionItem("Organic",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_ORGANIC")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_OYOAHA",
       itemOpt=new BuOptionItem("Oyoaha",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_OYOAHA")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_PLASTIC",
       itemOpt=new BuOptionItem("Plastic",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_PLASTIC")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_PLASTIC3D",
       itemOpt=new BuOptionItem("Plastic 3D",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_PLASTIC3D")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_PLASTICXP",
       itemOpt=new BuOptionItem("Plastic XP",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_PLASTICXP")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_SKINLF",
       itemOpt=new BuOptionItem("SkinLF",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_SKINLF")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_SLAF",
       itemOpt=new BuOptionItem("Slaf",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_SLAF")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_SYNTHETICA",
       itemOpt=new BuOptionItem("Synthetica",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_SYNTHETICA")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_TONIC",
       itemOpt=new BuOptionItem("Tonic",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_TONIC")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="ASPECT_WINDOWS",
       itemOpt=new BuOptionItem("Windows",
				BuResource.BU.getMenuIcon("aspect"),
				options_.isEnabled("ASPECT_WINDOWS")));
    optionsStr_.put(itemOpt, itemStr);

    Object[] items;
    //int nGen, nGenDesk;
    
    //setLayout(new BuBorderLayout());
    setLayout(new BuVerticalLayout(5));
    setBorder(EMPTY5555);

    //nGen=0;

    // Desktop
    pnGenDesk_=new BuPanel();
    boGenDesk_=new CompoundBorder
      (new BuTitledBorder
       (getS("Aspect")),EMPTY5555);
    pnGenDesk_.setBorder(boGenDesk_);
    /*
    loGenDesk_=new BuGridLayout();
    loGenDesk_.setColumns(2);
    loGenDesk_.setVgap(5);
    loGenDesk_.setHgap(5);
    */
    loGenDesk_=new BuFormLayout(5,5,BuFormLayout.LAST,BuFormLayout.NONE);
    pnGenDesk_.setLayout(loGenDesk_);
    // nGenDesk=0;
    //
    // Aspect
    lbGenAspAspect_=new BuLabel(getS("Aspect:"));//,JLabel.RIGHT);
    items=new BuOptionItem[20];
    items[ 0]=optionsStr_.get("ASPECT_DEFAUT");
    items[ 1]=optionsStr_.get("ASPECT_AMIGA");
    items[ 2]=optionsStr_.get("ASPECT_FHLAF");
    items[ 3]=optionsStr_.get("ASPECT_GTK");
    items[ 4]=optionsStr_.get("ASPECT_KUNSTSTOFF");
    items[ 5]=optionsStr_.get("ASPECT_LIQUID");
    items[ 6]=optionsStr_.get("ASPECT_MAC");
    items[ 7]=optionsStr_.get("ASPECT_METAL");
    items[ 8]=optionsStr_.get("ASPECT_MOTIF");
    items[ 9]=optionsStr_.get("ASPECT_NEXT");
    items[10]=optionsStr_.get("ASPECT_ORGANIC");
    items[11]=optionsStr_.get("ASPECT_OYOAHA");
    items[12]=optionsStr_.get("ASPECT_PLASTIC");
    items[13]=optionsStr_.get("ASPECT_PLASTIC3D");
    items[14]=optionsStr_.get("ASPECT_PLASTICXP");
    items[15]=optionsStr_.get("ASPECT_SKINLF");
    items[16]=optionsStr_.get("ASPECT_SLAF");
    items[17]=optionsStr_.get("ASPECT_SYNTHETICA");
    items[18]=optionsStr_.get("ASPECT_TONIC");
    items[19]=optionsStr_.get("ASPECT_WINDOWS");
    liGenAspAspect_=new BuComboBox(items);
    liGenAspAspect_.setRenderer(cbRenderer_);
    liGenAspAspect_.setEditable(false);
    liGenAspAspect_.addActionListener(this);
    //pnGenDesk_.add(lbGenAspAspect_, nGenDesk++);
    //pnGenDesk_.add(liGenAspAspect_, nGenDesk++);
    pnGenDesk_.add(lbGenAspAspect_,BuFormLayout.constraint(0,0,false,1.f));
    pnGenDesk_.add(liGenAspAspect_,BuFormLayout.constraint(1,0,true,0.f));

    // Metal theme
    lbGenAspMetal_=new BuLabel(getS("Th�me Metal:"));//,JLabel.RIGHT);
    BuMetalCustomTheme[] metalThemes=BuMetalCustomTheme.getList();
    items=new BuOptionItem[metalThemes.length-7];
    for(int i=0,j=0;i<metalThemes.length;i++)
    {
      BuMetalCustomTheme s=metalThemes[i];
      if(s!=null)
      {
	optionsStr_.put(itemStr="THEME_METAL"+i,
			itemOpt=new BuOptionItem(s.getName(),s.getIcon()));
	optionsStr_.put(itemOpt, itemStr);
	items[j]=optionsStr_.get("THEME_METAL"+i);
	j++;
        if(j==metalThemes.length-7) break;
      }
    }
    liGenAspMetal_=new BuComboBox(items);
    liGenAspMetal_.setRenderer(cbRenderer_);
    liGenAspMetal_.setEditable(false);
    liGenAspMetal_.addActionListener(this);
    //pnGenDesk_.add(lbGenAspMetal_, nGenDesk++);
    //pnGenDesk_.add(liGenAspMetal_, nGenDesk++);
    pnGenDesk_.add(lbGenAspMetal_,BuFormLayout.constraint(0,1,false,1.f));
    pnGenDesk_.add(liGenAspMetal_,BuFormLayout.constraint(1,1,2,true,0.f));

    // Slaf theme
    lbGenAspSlaf_=new BuLabel(getS("Th�me Slaf:"));//,JLabel.RIGHT);
    BuSlafCustomTheme[] slafThemes=BuSlafCustomTheme.getList();
    items=new BuOptionItem[slafThemes.length-3];
    for(int i=0,j=0;i<slafThemes.length;i++)
    {
      BuSlafCustomTheme s=slafThemes[i];
      if(s!=null)
      {
	optionsStr_.put(itemStr=s.getKey(),
			itemOpt=new BuOptionItem(s.getName(),s.getIcon()));
	optionsStr_.put(itemOpt, itemStr);
	items[j]=optionsStr_.get(s.getKey());
	j++;
        if(j==slafThemes.length-3) break;
      }
    }
    liGenAspSlaf_=new BuComboBox(items);
    liGenAspSlaf_.setRenderer(cbRenderer_);
    liGenAspSlaf_.setEditable(false);
    liGenAspSlaf_.addActionListener(this);
    //pnGenDesk_.add(lbGenAspSlaf_, nGenDesk++);
    //pnGenDesk_.add(liGenAspSlaf_, nGenDesk++);
    pnGenDesk_.add(lbGenAspSlaf_,BuFormLayout.constraint(0,2,false,1.f));
    pnGenDesk_.add(liGenAspSlaf_,BuFormLayout.constraint(1,2,true,0.f));

    // Skinlf theme
    lbGenAspSkinlf_=new BuLabel(getS("Th�me Skinlf:"));//,JLabel.RIGHT);
    tfGenAspSkinlf_=new BuTextField(18);
    tfGenAspSkinlf_.addKeyListener(this);
    tfGenAspSkinlf_.addActionListener(this);
    //pnGenDesk_.add(lbGenAspSkinlf_, nGenDesk++);
    //pnGenDesk_.add(tfGenAspSkinlf_, nGenDesk++);
    pnGenDesk_.add(lbGenAspSkinlf_,BuFormLayout.constraint(0,3,false,1.f));
    pnGenDesk_.add(tfGenAspSkinlf_,BuFormLayout.constraint(1,3,3,true,0.f));

    // Oyoaha theme
    lbGenAspOyoaha_=new BuLabel(getS("Th�me Oyoaha:"));//,JLabel.RIGHT);
    tfGenAspOyoaha_=new BuTextField(18);
    tfGenAspOyoaha_.addKeyListener(this);
    tfGenAspOyoaha_.addActionListener(this);
    //pnGenDesk_.add(lbGenAspOyoaha_, nGenDesk++);
    //pnGenDesk_.add(tfGenAspOyoaha_, nGenDesk++);
    pnGenDesk_.add(lbGenAspOyoaha_,BuFormLayout.constraint(0,4,false,1.f));
    pnGenDesk_.add(tfGenAspOyoaha_,BuFormLayout.constraint(1,4,3,true,0.f));

    // Decor
    lbGenDeskDecor_=new BuLabel(getS("D�cor:"));//,JLabel.RIGHT);
    items=new BuOptionItem[3];
    items[0]=optionsStr_.get("DESKTOP_UNIFORME");
    items[1]=optionsStr_.get("DESKTOP_DEGRADE"); 
    items[2]=optionsStr_.get("DESKTOP_TEXTURE"); 
    liGenDeskDecor_=new BuComboBox(items);
    liGenDeskDecor_.setRenderer(cbRenderer_);
    liGenDeskDecor_.setEditable(false);
    liGenDeskDecor_.addActionListener(this);
    //pnGenDesk_.add(lbGenDeskDecor_, nGenDesk++);
    //pnGenDesk_.add(liGenDeskDecor_, nGenDesk++);
    pnGenDesk_.add(lbGenDeskDecor_,BuFormLayout.constraint(0,5,false,1.f));
    pnGenDesk_.add(liGenDeskDecor_,BuFormLayout.constraint(1,5,true,0.f));

    // Texture
    lbGenDeskTexture_=new BuLabel(getS("Texture:"));//,JLabel.RIGHT);
    items=new BuOptionItem[9];
    for(int i=0;i<9;i++)
      items[i]=optionsStr_.get("DESKTOP_TEXTURE"+(i+1));
    liGenDeskTexture_=new BuComboBox(items);
    liGenDeskTexture_.setRenderer(cbRenderer_);
    liGenDeskTexture_.setEditable(false);
    liGenDeskTexture_.addActionListener(this);
    //pnGenDesk_.add(lbGenDeskTexture_, nGenDesk++);
    //pnGenDesk_.add(liGenDeskTexture_, nGenDesk++);
    pnGenDesk_.add(lbGenDeskTexture_,BuFormLayout.constraint(0,6,false,1.f));
    pnGenDesk_.add(liGenDeskTexture_,BuFormLayout.constraint(1,6,true,0.f));

    // Couleur
    lbGenDeskCouleur_=new BuLabel(getS("Couleur:"));//,JLabel.RIGHT);
    items=new BuOptionItem[6];
    items[0]=optionsStr_.get("DESKTOP_ROUGE");
    items[1]=optionsStr_.get("DESKTOP_VERT");
    items[2]=optionsStr_.get("DESKTOP_BLEU");
    items[3]=optionsStr_.get("DESKTOP_ORANGE");
    items[4]=optionsStr_.get("DESKTOP_SIMILAIRE");
    items[5]=optionsStr_.get("DESKTOP_DEFAUT");
    liGenDeskCouleur_=new BuComboBox(items);
    liGenDeskCouleur_.setRenderer(cbRenderer_);
    liGenDeskCouleur_.setEditable(false);
    liGenDeskCouleur_.addActionListener(this);
    //pnGenDesk_.add(lbGenDeskCouleur_, nGenDesk++);
    //pnGenDesk_.add(liGenDeskCouleur_, nGenDesk++);
    pnGenDesk_.add(lbGenDeskCouleur_,BuFormLayout.constraint(0,7,false,1.f));
    pnGenDesk_.add(liGenDeskCouleur_,BuFormLayout.constraint(1,7,true,0.f));

    lbFont_=new BuLabel(getS("Police:"));
    slFont_=new BuSlider(50,200,100);
    slFont_.setMajorTickSpacing(50);
    slFont_.setMinorTickSpacing(5);
    slFont_.setPaintTicks(true);
    slFont_.setPaintLabels(true);
    slFont_.setSnapToTicks(true);
    Hashtable labels=new Hashtable(11);
    labels.put(FuFactoryInteger.get( 50),createSliderLabel( 50));
    labels.put(FuFactoryInteger.get(100),createSliderLabel(100));
    labels.put(FuFactoryInteger.get(200),createSliderLabel(200));
    //labels.put(FuFactoryInteger.get(300),createSliderLabel(300));
    slFont_.setLabelTable(labels);
    slFont_.addChangeListener(this);
    pnGenDesk_.add(lbFont_,BuFormLayout.constraint(0,8,false,1.f));
    pnGenDesk_.add(slFont_,BuFormLayout.constraint(1,8,3,true,1.f));

    add(pnGenDesk_);//,BuBorderLayout.CENTER);

    updateComponents();
  }
  
  private JLabel createSliderLabel(int _v)
  {
    BuLabel r=new BuLabel();
    r.setFont(BuLib.deriveFont(r.getFont(),-4));
    r.setText(_v+"%");
    return r;
  }

  // Evenements
  
  @Override
  public void stateChanged(ChangeEvent _evt)
  {
    /*
    if(slFont_==_evt.getSource())
    {
      int v=5*(int)Math.round(slFont_.getValue()/5.);
      FuLog.debug("V="+v+" O="+slFont_.getValue());
      if(slFont_.getValue()!=v) slFont_.setValue(v);
    }
    */
    setDirty(true);
    updateAbility();
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
    updateAbility();
  }

  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
  {
    return true;
  }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
  {
    return false;
    //(appli_!=null);
  }
  
  @Override
  public void applyPreferences()
  {
    //fillTable();
    //options_.applyOn(appli_);
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }
  
  // Methodes privees
  
  private void fillTable()
  {
    Object o;

    o=liGenDeskDecor_.getSelectedItem();
    o=(o==null) ? "DESKTOP_UNIFORME" : optionsStr_.get(o);
    options_.putStringProperty("desktop.decor",o.toString());

    o=liGenDeskTexture_.getSelectedItem();
    o=(o==null) ? "DESKTOP_TEXTURE1" : optionsStr_.get(o);
    options_.putStringProperty("desktop.texture",o.toString());

    o=liGenDeskCouleur_.getSelectedItem();
    o=(o==null) ? "DESKTOP_DEFAUT" : optionsStr_.get(o);
    options_.putStringProperty("desktop.background",o.toString());

    o=liGenAspAspect_.getSelectedItem();
    o=(o==null) ? "ASPECT_DEFAUT" : optionsStr_.get(o);
    options_.putStringProperty("lookandfeel.name",o.toString());

    o=liGenAspMetal_.getSelectedItem();
    o=(o==null) ? "THEME_METAL0" : optionsStr_.get(o);
    options_.putStringProperty("metal.theme",o.toString());

    o=liGenAspSlaf_.getSelectedItem();
    o=(o==null) ? "default" : optionsStr_.get(o);
    options_.putStringProperty("slaf.theme",o.toString());

    options_.putStringProperty("skinlf.theme",tfGenAspSkinlf_.getText());
    options_.putStringProperty("oyoaha.theme",tfGenAspOyoaha_.getText());

    options_.putIntegerProperty("lookandfeel.fontscale",slFont_.getValue());

    setDirty(false);
  }

  private void updateComponents()
  {
    liGenDeskDecor_      .setSelectedItem(optionsStr_.get(options_.getStringProperty("desktop.decor"     ,"DESKTOP_UNIFORME"  )));
    liGenDeskTexture_    .setSelectedItem(optionsStr_.get(options_.getStringProperty("desktop.texture"   ,"DESKTOP_TEXTURE1"  )));
    liGenDeskCouleur_    .setSelectedItem(optionsStr_.get(options_.getStringProperty("desktop.background","DESKTOP_DEFAUT"    )));
    liGenAspAspect_      .setSelectedItem(optionsStr_.get(options_.getStringProperty("lookandfeel.name"  ,"ASPECT_DEFAUT"     )));
    liGenAspMetal_       .setSelectedItem(optionsStr_.get(options_.getStringProperty("metal.theme"       ,"THEME_METAL0"      )));
    liGenAspSlaf_        .setSelectedItem(optionsStr_.get(options_.getStringProperty("slaf.theme"        ,"default"           )));

    tfGenAspSkinlf_.setText
	(options_.getStringProperty
	 ("skinlf.theme","/usr/share/themes/Chrome/gtk/gtkrc"));
    tfGenAspOyoaha_.setText
	(options_.getStringProperty
	 ("oyoaha.theme","slushy.otm"));

    slFont_.setValue(options_.getIntegerProperty("lookandfeel.fontscale",100));

    updateAbility();
    setDirty(false);
  }

  @Override
  protected void updateAbility()
  {
    boolean b;

    b=(liGenDeskDecor_.getSelectedItem()==optionsStr_.get("DESKTOP_TEXTURE"));
    liGenDeskTexture_.setEnabled(b);
    liGenDeskCouleur_.setEnabled(!b);

    b=(liGenAspAspect_.getSelectedItem()==optionsStr_.get("ASPECT_METAL"));
    liGenAspMetal_.setEnabled(b);

    b=(liGenAspAspect_.getSelectedItem()==optionsStr_.get("ASPECT_SLAF"));
    liGenAspSlaf_.setEnabled(b);

    b=(liGenAspAspect_.getSelectedItem()==optionsStr_.get("ASPECT_SKINLF"));
    tfGenAspSkinlf_.setEnabled(b);

    b=(liGenAspAspect_.getSelectedItem()==optionsStr_.get("ASPECT_OYOAHA"));
    tfGenAspOyoaha_.setEnabled(b);
  }
}
