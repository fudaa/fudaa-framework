/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuWizardTask.java
 * @version      0.41
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.bu;

import javax.swing.JComponent;

/**
 * Une tache associ�e � un dialogue wizard. Cette tache peut comporter un certain nombre
 * d'�tapes, se terminant par le lancement de la tache.
 * @author
 * @see BuWizardDialog
 */
public abstract class BuWizardTask
{
  protected int     current_;
  protected boolean done_;

  public BuWizardTask()
  {
    current_=0;
    done_   =false;
  }

  /**
   * @return L'indice de l'�tape courante, commencant � 0.
   */
  public final int getStepCurrent()
  {
    return current_;
  }

  /**
   * Passe � l'�tape pr�c�dente.
   */
  public void previousStep()
  {
    if(current_>0)
      current_--;
  }

  /**
   * Passe � l'�tape suivante. La methode peut �tre surcharg�e pour des
   * controles avant passage � l'�tape suivante.
   */
  public void nextStep()
  {
    if(current_<getStepCount()-1)
      current_++;
  }

  /**
   * Previent que la task est finie, que le dialogue doit se fermer.
   * @return True si termin�.
   */
  public boolean isFinished()
  {
    return done_;
  }

  /**
   * Annule le wizard et le ferme.
   */
  public void cancelTask()
  {
    done_=true;
  }

  /**
   * A surcharger : Tache execut�e � la fin du wizard (bouton Termliner)
   */
  public void doTask()
  {
    done_=true;
  }

  public abstract String     getTaskTitle();

  /**
   * @return Le nombre total d'�tapes
   */
  public abstract int        getStepCount();

  /**
   * @return Le title de l'�tape courante
   */
  public abstract String     getStepTitle();

  /**
   * @return Le texte d'aide pour l'�tape courante
   */
  public abstract String     getStepText();

  /**
   * @return Le panneau courant.
   */
  public abstract JComponent getStepComponent();

  /**
   * @return L'icone pour l'�tape courante. Peut �tre surcharg� pour un icone
   * diff�rent � chaque �tape.
   */
  public BuIcon getStepIcon()
  {
    return BuResource.BU.getIcon("bu_wizard");
  }

  public int getStepButtons()
  {
    int r;

    if(current_==getStepCount()-1)
      r=BuButtonPanel.PRECEDENT
	| BuButtonPanel.TERMINER
	| BuButtonPanel.ABANDONNER;
    else
      r=BuButtonPanel.PRECEDENT
	| BuButtonPanel.SUIVANT
	| BuButtonPanel.ABANDONNER;

    return r;
  }

  public int getStepDisabledButtons()
  {
    int r;

    if(current_==0)
      r=BuButtonPanel.PRECEDENT;
    else
      r=BuButtonPanel.AUCUN;

    return r;
  }
}
