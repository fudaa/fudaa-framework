/**
 * @modification $Date$
 * @statut unstable
 * @file BuCommonImplementation.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuLog;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;

/**
 * Main part of the code to manage BuApplets and BuApplications. Use to avoid redundant code.
 */
public class BuCommonImplementation implements BuCommonInterface, ContainerListener, WindowListener,
    InternalFrameListener {
  protected BuCommonInterface app_;
  protected JComponent content_;
  protected BuMainPanel main_panel_;
  protected BuMenuBar main_menubar_;
  protected BuToolBar main_toolbar_;
  protected BuSplashScreen splash_screen_;

  public BuCommonImplementation() {
    super();

    app_ = null;
    content_ = null;
    main_panel_ = null;
    main_menubar_ = null;
    main_toolbar_ = null;
  }

  // Implementation

  @Override
  public BuCommonImplementation getImplementation() {
    return this;
  }

  // App (Application or Applet)

  @Override
  public BuCommonInterface getApp() {
    return app_;
  }

  public void setApp(BuCommonInterface buCommonInterface) {
    app_ = buCommonInterface;
  }

  // Translation

  protected String getString(String s) {
    return BuResource.BU.getString(s);
  }

  protected boolean buildMainPanel_ = true;

  /**
   * @return Si false, le desktop n'a pas de scrollbar.
   */
  protected boolean useScrollInBuDesktop() {
    return true;
  }

  /**
   * Mise en place des elements. (Panneau principal, barres de menu et de boutons).
   */
  @Override
  public void init() {
    BuSplashScreen ss = getSplashScreen();

    if (ss != null) {
      ss.setText("Initialisation de l'application");
      ss.setProgression(20);
    }

    content_ = new BuPanel();
    content_.setLayout(new BuBorderLayout());
    setContentPane(content_);

    JComponent mc = createMainComponent();
    BuMainPanel mp = null;
    if (buildMainPanel_) {
      mp = new BuMainPanel(mc, useScrollInBuDesktop());
      setMainPanel(mp);
    }

    if (ss != null) {
      ss.setText("Menus");
      ss.setProgression(30);
    }

    BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
    setMainMenuBar(mb);
    mb.computeMnemonics();
    mb.addActionListener(this);

    if (ss != null) {
      ss.setText("Barre d'outils");
      ss.setProgression(40);
    }

    BuToolBar tb = BuToolBar.buildBasicToolBar();
    setMainToolBar(tb);
    tb.addActionListener(this);

    if (ss != null) {
      ss.setText("Liste des fen�tres");
      ss.setProgression(45);
    }

    if (hasDesktop() && mp != null) {
      BuMenuInternalFrames mw = (BuMenuInternalFrames) mb.getMenu("LISTE_FENETRES");
      if (mw != null) {
        mw.setDesktop(mp.getDesktop());
      }
    }
  }

  public void prestart() {
    getMainPanel().updateSplits();
    menusOrganisationFenetres();
  }

  @Override
  public void start() {
    // next lines soon removed
    getMainPanel().updateSplits();
    menusOrganisationFenetres();
  }

  public String toString() {
    return "implementation " + getInformationsSoftware().name;
  }

  /**
   * Creates the main component (desktop).
   */
  protected JComponent createMainComponent() {
    return new BuDesktop();
  }

  protected boolean hasDesktop() {
    return getMainPanel() != null && getMainPanel().getDesktop() != null;
  }

  // CommonInterface

  @Override
  public void doLayout() {
    app_.doLayout();
  }

  @Override
  public void validate() {
    app_.validate();
  }

  @Override
  public void setTitle(String s) {
    app_.setTitle(s);
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return new BuInformationsSoftware();
  }

  @Override
  public void setJMenuBar(JMenuBar menuBar) {
    app_.setJMenuBar(menuBar);
  }

  @Override
  public void setContentPane(Container container) {
    app_.setContentPane(container);
  }

  @Override
  public JRootPane getRootPane() {
    return app_.getRootPane();
  }

  @Override
  public Frame getFrame() {
    return app_.getFrame();
  }

  // Main Panel

  @Override
  public BuMainPanel getMainPanel() {
    return main_panel_;
  }

  @Override
  public void setMainPanel(BuMainPanel buMainPanel) {
    if (main_panel_ != null && hasDesktop()) {
      main_panel_.removeContainerListener(this);
    }
    main_panel_ = buMainPanel;
    content_.add(main_panel_, BuBorderLayout.CENTER);
    content_.doLayout();
    content_.validate();
    if (hasDesktop()) {
      main_panel_.getDesktop().addContainerListener(this);
    }
  }

  // Main MenuBar

  @Override
  public BuMenuBar getMainMenuBar() {
    return main_menubar_;
  }

  @Override
  public void setMainMenuBar(BuMenuBar buMenuBar) {
    main_menubar_ = buMenuBar;
    setJMenuBar(main_menubar_);
  }

  // Main ToolBar

  @Override
  public BuToolBar getMainToolBar() {
    return main_toolbar_;
  }

  @Override
  public void setMainToolBar(BuToolBar buToolBar) {
    main_toolbar_ = buToolBar;
    if (!(getApp() instanceof BuSubApp)) {
      if ((content_.getComponentCount() > 1) && (content_.getComponent(1) != null)) {
        content_.remove(1);
      }

      if (main_toolbar_ != null) {
        content_.add(main_toolbar_, BuBorderLayout.NORTH);
        main_toolbar_.revalidate();
      }
    }
  }

  // Specific Bar

  @Override
  public BuSpecificBar getSpecificBar() {
    return getMainPanel().getSpecificBar();
  }

  @Override
  public void setSpecificBar(BuSpecificBar buSpecificBar) {
    getMainPanel().setSpecificBar(buSpecificBar);
  }

  // Splash Screen

  public BuSplashScreen getSplashScreen() {
    return splash_screen_;
  }

  public void setSplashScreen(BuSplashScreen buSplashScreen) {
    splash_screen_ = buSplashScreen;
  }

  // Internal Frames

  public void addInternalFrame(final JInternalFrame jInternalFrame) {
    final InternalFrameListener l = this;

    Runnable runnable = () -> {
      jInternalFrame.removeInternalFrameListener(l);
      jInternalFrame.addInternalFrameListener(l);

      BuMainPanel mp = getMainPanel();
      mp.addInternalFrame(jInternalFrame);

      menusOrganisationFenetres();
    };

    if (!SwingUtilities.isEventDispatchThread()) {
      SwingUtilities.invokeLater(runnable);
    } else {
      runnable.run();
    }
  }

  public void removeInternalFrame(final JInternalFrame _f) {
    final InternalFrameListener l = this;

    Runnable runnable = () -> {
      BuMainPanel mp = getMainPanel();
      mp.removeInternalFrame(_f);

      _f.removeInternalFrameListener(l);
      if (ifListener_ != null) {
        InternalFrameEvent event = new InternalFrameEvent(_f, InternalFrameEvent.INTERNAL_FRAME_CLOSED);
        for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
          ((InternalFrameListener) it.next()).internalFrameClosed(event);
        }
      }
      menusOrganisationFenetres();
    };

    if (!SwingUtilities.isEventDispatchThread()) {
      SwingUtilities.invokeLater(runnable);
    } else {
      runnable.run();
    }
  }

  public void addInternalFrames(final JInternalFrame[] _f) {
    final InternalFrameListener l = this;

    Runnable runnable = () -> {
      BuMainPanel mp = getMainPanel();

      for (int i = _f.length - 1; i >= 0; i--) {
        _f[i].removeInternalFrameListener(l);
        _f[i].addInternalFrameListener(l);
        mp.addInternalFrame(_f[i]);
      }

      menusOrganisationFenetres();
    };

    if (!SwingUtilities.isEventDispatchThread()) {
      SwingUtilities.invokeLater(runnable);
    } else {
      runnable.run();
    }
  }

  public void removeInternalFrames(final JInternalFrame[] _f) {
    final InternalFrameListener l = this;

    Runnable runnable = () -> {
      BuMainPanel mp = getMainPanel();

      for (int i = _f.length - 1; i >= 0; i--) {
        if (_f[i] != null) {

          mp.removeInternalFrame(_f[i]);
          _f[i].removeInternalFrameListener(l);
          if (ifListener_ != null) {
            InternalFrameEvent event = new InternalFrameEvent(_f[i], InternalFrameEvent.INTERNAL_FRAME_CLOSED);
            for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
              ((InternalFrameListener) it.next()).internalFrameClosed(event);
            }
          }
        }
      }

      menusOrganisationFenetres();
    };

    if (!SwingUtilities.isEventDispatchThread()) {
      try {
        SwingUtilities.invokeAndWait(runnable);
      } catch (InterruptedException _evt) {
        FuLog.error(_evt);
      } catch (InvocationTargetException _evt) {
        FuLog.error(_evt);
      }
    } else {
      runnable.run();
    }
  }

  public void activateInternalFrame(final JInternalFrame _f) {

    Runnable runnable = () -> {
      BuMainPanel mp = getMainPanel();
      mp.activateInternalFrame(_f);

      menusOrganisationFenetres();
    };

    if (!SwingUtilities.isEventDispatchThread()) {
      SwingUtilities.invokeLater(runnable);
    } else {
      runnable.run();
    }
  }

  public JInternalFrame getCurrentInternalFrame() {
    return getMainPanel().getCurrentInternalFrame();
  }

  public JInternalFrame[] getAllInternalFrames() {
    return getMainPanel().getAllInternalFrames();
  }

  // Action

  public void action(String _action) {
    try {
      if (_action != null) {
        actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, _action));
      }
    } catch (Exception ex) {
    }
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    String action = event.getActionCommand();
    String arg = "";
    BuMainPanel mp = getMainPanel();
    BuInformationsSoftware il = getInformationsSoftware();

    int i = action.indexOf('(');
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }

    if (action.equals("FERMER")) {
      close();
    } else if (action.equals("IMPRIMER")) {
      print();
    } else if (action.equals("PREVISUALISER")) {
      preview();
    } else if (action.equals("QUITTER")) {
      exit();
    } else if (action.equals("DEFAIRE")) {
      undo();
    } else if (action.equals("REFAIRE")) {
      redo();
    } else if (action.equals("COUPER")) {
      cut();
    } else if (action.equals("COPIER")) {
      copy();
    } else if (action.equals("COLLER")) {
      paste();
    } else if (action.equals("DUPLIQUER")) {
      duplicate();
    } else if (action.equals("TOUTSELECTIONNER")) {
      select();
    } else if (action.equals("RECHERCHER")) {
      find();
    } else if (action.equals("REMPLACER")) {
      replace();
    } else if (action.equals("PLEINECRAN")) {
      fullscreen();
    } else if (action.equals("CASCADE")) {
      mp.waterfall();
    } else if (action.equals("MOSAIQUE")) {
      mp.tile();
    } else if (action.equals("RANGERICONES")) {
      mp.arrangeIcons();
    } else if (action.equals("RANGERPALETTES")) {
      mp.arrangePalettes();
    } else if (action.equals("ECHANGER_COLONNES")) {
      mp.swapColumns();
    } else if (action.equals("ASPECT_AMIGA")) {
      setLookAndFeel("swing.addon.plaf.threeD.ThreeDLookAndFeel");
    } else if (action.equals("ASPECT_FHLAF")) {
      setLookAndFeel("com.shfarr.ui.plaf.fh.FhLookAndFeel");
    } else if (action.equals("ASPECT_GTK")) {
      setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
    } else if (action.equals("ASPECT_KUNSTSTOFF")) {
      setLookAndFeel("com.incors.plaf.kunststoff.KunststoffLookAndFeel");
    } else if (action.equals("ASPECT_MAC")) {
      setLookAndFeel("com.sun.java.swing.plaf.mac.MacLookAndFeel");
    } else if (action.equals("ASPECT_METAL")) {
      setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
    } else if (action.equals("ASPECT_METOUIA")) {
      setLookAndFeel("net.sourceforge.mlf.metouia.MetouiaLookAndFeel");
    } else if (action.equals("ASPECT_MOTIF")) {
      setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
    } else if (action.equals("ASPECT_NEXT")) {
      setLookAndFeel("nextlf.plaf.NextLookAndFeel");
    } else if (action.equals("ASPECT_ORGANIC")) {
      setLookAndFeel("javax.swing.plaf.organic.OrganicLookAndFeel");
    } else if (action.equals("ASPECT_OYOAHA")) {
      setLookAndFeel("com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel");
    } else if (action.equals("ASPECT_LIQUID")) {
      setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");
    } else if (action.equals("ASPECT_PLASTIC")) {
      setLookAndFeel("com.jgoodies.looks.plastic.PlasticLookAndFeel");
    } else if (action.equals("ASPECT_PLASTIC3D")) {
      setLookAndFeel("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
    } else if (action.equals("ASPECT_PLASTICXP")) {
      setLookAndFeel("com.jgoodies.looks.plastic.PlasticXPLookAndFeel");
    } else if (action.equals("ASPECT_SKINLF")) {
      setLookAndFeel("com.l2fprod.gui.plaf.skin.SkinLookAndFeel");
    } else if (action.equals("ASPECT_SLAF")) {
      setLookAndFeel("com.memoire.slaf.SlafLookAndFeel");
    } else if (action.equals("ASPECT_SYNTHETICA")) {
      setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel");
    } else if (action.equals("ASPECT_TONIC")) {
      setLookAndFeel("com.digitprop.tonic.TonicLookAndFeel");
    } else if (action.equals("ASPECT_WINDOWS")) {
      setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } else if (action.startsWith("THEME_METAL")) {
      setMetalTheme(arg);
    } else if (action.startsWith("THEME_SLAF")) {
      setSlafTheme(arg);
    } else if (action.equals("VISIBLE_SPECIFICBAR")) {
      showOrHide(action, getSpecificBar());
    } else if (action.equals("VISIBLE_LEFTCOLUMN")) {
      showOrHide(action, mp.getLeftColumn());
    } else if (action.equals("VISIBLE_RIGHTCOLUMN")) {
      showOrHide(action, mp.getRightColumn());
    } else if (action.equals("VISIBLE_STATUSBAR")) {
      showOrHide(action, mp.getStatusBar());
    } else if (action.equals("AIDE")) {
      displayURL(null);
    } else if (action.equals("AIDE_INDEX")) {
      displayURL(il.baseManUrl());
    } else if (action.equals("INDEX_ALPHA")) {
      displayURL(il.baseManUrl() + "alphabetique.html");
    } else if (action.equals("INDEX_THEMA")) {
      displayURL(il.baseManUrl() + "thematique.html");
    } else if (action.equals("TEXTE_LICENCE")) {
      license();
    } else if (action.equals("WWW_ACCUEIL")) {
      displayURL(il.http);
    } else if (action.startsWith("AIDE_CONTEXTUELLE")) {
      contextHelp(arg);
    } else if (action.equals("POINTEURAIDE")) {
      pointerHelp();
    } else if (action.equals("APROPOSDE")) {
      about();
    } else if (action.equals("DEBUTER")) {
      ;
    } else if (action.startsWith("FILLE_")) {
      daughter(action.substring(6), arg);
    } else {
      FuLog.error("UNKNOWN ACTION: " + action);
    }
  }

  @Override
  public void showOrHide(String a, JComponent jComponent) {
    getMainPanel().showOrHide(jComponent);
    setCheckedForAction(a, jComponent.isVisible());
  }

  @Override
  public void removeAction(String cmd) {
    BuActionRemover.removeAction(getMainMenuBar(), cmd);
    BuActionRemover.removeAction(getMainToolBar(), cmd);
  }

  @Override
  public void setEnabledForAction(String cmd, boolean enabled) {
    BuActionEnabler.setEnabledForAction(getMainMenuBar(), cmd, enabled);
    BuActionEnabler.setEnabledForAction(getMainToolBar(), cmd, enabled);
  }

  @Override
  public void setCheckedForAction(String cmd, boolean checked) {
    BuActionChecker.setCheckedForAction(getMainMenuBar(), cmd, checked);
    BuActionChecker.setCheckedForAction(getMainToolBar(), cmd, checked);
  }

  public void setDynamicTextForAction(String _cmd, String _tp) {
    BuActionModifier.setDynamicTextForAction(getMainMenuBar(), _cmd, _tp);
    BuActionModifier.setDynamicTextForAction(getMainToolBar(), _cmd, _tp);
  }

  @Override
  public void removeDummySeparators() {
    BuMenuBar mb = getMainMenuBar();
    BuToolBar mt = getMainToolBar();

    if (mb != null) {
      mb.removeDummySeparators();
    }
    if (mt != null) {
      mt.removeDummySeparators();
    }
  }

  // Divers

  public void daughter(String action, String name) {
    JInternalFrame[] frames = getMainPanel().getAllInternalFrames();

    JInternalFrame f = null;

    for (int i = 0; i < frames.length; i++) {
      String n = frames[i].getName();
      if (name.equals(n) || ("if" + name).equals(n) || name.equals("*") || name.equals(frames[i].getTitle())) {
        f = frames[i];
        try {
          if ("ACTIVER".equals(action)) {
            if (f.isIcon()) {
              f.setIcon(false);
            }
            if (!f.isSelected()) {
              f.setSelected(true);
            }
          } else if ("FERMER".equals(action)) {
            if (!f.isClosed()) {
              f.setClosed(true);
            }
          } else if ("ICONIFIER".equals(action)) {
            if (!f.isIcon()) {
              f.setIcon(true);
            }
          } else if ("DEICONIFIER".equals(action)) {
            if (f.isIcon()) {
              f.setIcon(false);
            }
          } else {
            FuLog.error("UNKNOWN SUB-ACTION: " + action);
          }
        } catch (PropertyVetoException ex) {
          FuLog.warning(ex);
        }
      }
    }

    if (f == null) {
      FuLog.error("UNKNOWN FRAME: " + name);
    }
  }

  private BuGlassPaneHelp glasspane_ = null;

  public void pointerHelp() {
    BuCommonInterface app = getApp();
    if (app instanceof BuApplication) {
      BuApplication appli = (BuApplication) app;

      if (glasspane_ == null) {
        glasspane_ = new BuGlassPaneHelp(appli);
        glasspane_.setVisible(false);
      }

      if (appli.getGlassPane() != glasspane_) {
        appli.setGlassPane(glasspane_);
      }

      glasspane_.setVisible(true);
      appli.invalidate();
      appli.doLayout();
      appli.validate();
      glasspane_.repaint();
    }
  }

  public void contextHelp(String _url) {
    try {
      URL url = new URL(new URL(getInformationsSoftware().man), _url);
      String ll = Locale.getDefault().getLanguage();

      String newUrl = url.toString();
      if (!ll.equals("fr")) {
        ll = "en";
        int i = newUrl.lastIndexOf(".html");
        if (i >= 0) {
          newUrl = newUrl.substring(0, i) + "-" + ll + newUrl.substring(i);
        }
      }
      displayURL(newUrl);
    } catch (Exception ex) {
      FuLog.warning(ex);
    }
  }

  public void contextHelp(JComponent _c) {
    JComponent comp = _c;
    if (comp instanceof JInternalFrame) {
      comp = comp.getRootPane();
    }

    JComponent cp = comp;
    String param = (String) cp.getClientProperty("HELP_URL");

    Container pt = cp;
    while ((pt != null) && (param == null)) {
      pt = pt.getParent();
      if (pt instanceof JComponent) {
        param = (String) ((JComponent) pt).getClientProperty("HELP_URL");
      }
    }

    JComponent focus = comp;
    if (focus != null) {
      String ancre = focus.getName();
      if (ancre != null) {
        int i = 0;
        while (i < ancre.length()) {
          int c = ancre.charAt(i);
          if ((c < 'A') || (c > 'Z')) {
            i++;
          } else {
            break;
          }
        }
        ancre = ancre.substring(i).toLowerCase().replace('_', '-');
        if (!"".equals(ancre)) {
          param += "#" + ancre;
        }
      }
    }

    contextHelp(param);
  }

  private ActionListener al_ = null;
  private KeyStroke f1_ = null;

  public void installContextHelp(JComponent _cp) {
    installContextHelp(_cp, null);
  }

  public void installContextHelp(JComponent _cp, String _url) {
    if (_url != null) {
      _cp.putClientProperty("HELP_URL", _url);
    }

    if (al_ == null) {
      al_ = _evt -> {
        Object source = _evt.getSource();
        if (source instanceof JComponent) {
          JComponent cp = (JComponent) source;

          String param = (String) cp.getClientProperty("HELP_URL");

          Container pt = cp;
          while ((pt != null) && (param == null)) {
            pt = pt.getParent();
            if (pt instanceof JComponent) {
              param = (String) ((JComponent) pt).getClientProperty("HELP_URL");
            }
          }

          JComponent focus = null;
          for (Enumeration e = BuLib.getAllSubComponents(cp).elements(); e.hasMoreElements(); ) {
            Object o = e.nextElement();
            if ((o instanceof JComponent) && ((JComponent) o).hasFocus()) {
              focus = (JComponent) o;
              break;
            }
          }

          if (focus != null) {
            String ancre = focus.getName();
            if (ancre != null) {
              int i = 0;
              while (i < ancre.length()) {
                int c = ancre.charAt(i);
                if ((c < 'A') || (c > 'Z')) {
                  i++;
                } else {
                  break;
                }
              }
              ancre = ancre.substring(i).toLowerCase().replace('_', '-');
              if (!"".equals(ancre)) {
                param += "#" + ancre;
              }
            } else {
              FuLog.error("NOT NAMED = " + focus);
            }
          }

          param = "(" + param + ")";

          ActionEvent evt = new ActionEvent(cp, _evt.getID(), "AIDE_CONTEXTUELLE" + param, _evt.getModifiers());
          getImplementation().actionPerformed(evt);
        }
      };
    }

    if (f1_ == null) {
      f1_ = KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0, false);
    }

    _cp.unregisterKeyboardAction(f1_);
    _cp.registerKeyboardAction(al_, f1_, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }

  /**
   * Modification du LookAndFeel.
   */
  @Override
  public void setLookAndFeel(String _lnf) {
    String os = System.getProperty("os.name");
    if ("com.sun.java.swing.plaf.mac.MacLookAndFeel".equals(_lnf)) {
      System.getProperties().put("os.name", "Mac OS");
    }
    if ("com.sun.java.swing.plaf.windows.WindowsLookAndFeel".equals(_lnf)) {
      System.getProperties().put("os.name",
          "Windows");
    }

    boolean error = !BuPreferences.BU.setLookAndFeel(_lnf, false);

    if (error) {
      BuLib.forgetLnf();

      String tm;
      int i;

      tm = _lnf;
      i = tm.lastIndexOf('.');
      if (i >= 0) {
        tm = tm.substring(i + 1);
      }
      i = tm.lastIndexOf("LookAndFeel");
      if (i >= 0) {
        tm = tm.substring(0, i);
      }
      tm = getString("L'aspect n'a pu �tre trouv�") + ". [" + tm + "]";

      BuDialogMessage dm = new BuDialogMessage(app_, getInformationsSoftware(), tm);
      dm.activate();
    }

    System.getProperties().put("os.name", os);

    if (!error) {
      try {
        SwingUtilities.updateComponentTreeUI((Component) app_);
        getMainPanel().arrangeIcons();
      } catch (Throwable ex) {
        FuLog.error("could not update component tree UI", ex);

        String tm;
        int i;

        tm = _lnf;
        i = tm.lastIndexOf('.');
        if (i >= 0) {
          tm = tm.substring(i + 1);
        }
        i = tm.lastIndexOf("LookAndFeel");
        if (i >= 0) {
          tm = tm.substring(0, i);
        }
        tm = getString("L'aspect n'a pu mettre � jour l'arbre des composants") + ". [" + tm + "]";

        BuDialogMessage dm = new BuDialogMessage(app_, getInformationsSoftware(), tm);
        dm.activate();
      }
    }
  }

  public void setMetalTheme(String metalTheme) {
    try {
      int i = Integer.parseInt(metalTheme);
      BuMetalCustomTheme[] themes = BuMetalCustomTheme.getList();
      MetalLookAndFeel.setCurrentTheme(themes[i]);
      setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
    } catch (Exception ex) {
      FuLog.error(ex);
      BuDialogMessage dm = new BuDialogMessage(app_, getInformationsSoftware(),
          getString("Le th�me pour Metal n'a pu �tre mis en place") + ".");
      dm.activate();
    }
  }

  public void setSlafTheme(String _arg) {
    try {
      int i = Integer.parseInt(_arg);
      BuSlafCustomTheme[] themes = BuSlafCustomTheme.getList();
      Class c = Class.forName("com.memoire.slaf.SlafLookAndFeel");
      Method m = c.getMethod("setCurrentTheme", new Class[]{String.class});
      m.invoke(null, new Object[]{themes[i].getKey()});

      setLookAndFeel("com.memoire.slaf.SlafLookAndFeel");
    } catch (Exception ex) {
      FuLog.warning("could not set the Slaf theme", ex);

      BuDialogMessage dm = new BuDialogMessage(app_, getInformationsSoftware(),
          getString("Le th�me pour Slaf n'a pu �tre mis en place") + ".");
      dm.activate();
    }
  }

  public void fullscreen() {
    if (!(getApp() instanceof JFrame)) {
      return;
    }
    final JInternalFrame frame = getCurrentInternalFrame();
    if (frame == null) {
      return;
    }

    final JComponent content = (JComponent) frame.getContentPane();
    final JWindow win = new JWindow();
    final JComponent cwin = (JComponent) win.getContentPane();
    final JFrame app = (JFrame) getApp();
    final Dimension de = app.getToolkit().getScreenSize();

    final BuLabel label = new BuLabel();
    final BuButton button = new BuButton();
    final BuPanel top = new BuPanel();

    top.setLayout(new BuBorderLayout());

    label.setText(app.getTitle() + ": " + frame.getTitle());
    label.setBorder(BuBorders.EMPTY1111);
    label.setHorizontalAlignment(SwingConstants.CENTER);
    top.add(label, BorderLayout.CENTER);

    button.setBorder(BuBorders.EMPTY1111);
    button.setRequestFocusEnabled(false);
    Icon icon = UIManager.getIcon("InternalFrameTitlePane.closeIcon");
    if (icon == null) {
      icon = UIManager.getIcon("InternalFrame.closeIcon");
    }
    button.setIcon(icon);
    button.setIcon(icon);
    top.add(button, BorderLayout.EAST);

    frame.setContentPane(new JPanel());

    cwin.add(content, BuBorderLayout.CENTER);
    cwin.add(top, BuBorderLayout.NORTH);
    cwin.setBorder(new LineBorder(Color.black, 2));

    win.setSize(de.width, de.height);
    win.setLocation(0, 0);
    app.setVisible(false);
    win.doLayout();
    win.validate();
    win.setVisible(true);
    win.setLocation(0, 0);

    button.addActionListener(_ae -> {
      win.setVisible(false);
      cwin.remove(content);
      frame.setContentPane(content);
      app.setVisible(true);
    });
  }

  @Override
  public boolean confirmExit() {
    BuInformationsSoftware il = getInformationsSoftware();
    BuDialogConfirmation bd = new BuDialogConfirmation(getApp(), il, getString("Voulez-vous vraiment quitter ce logiciel ?"));

    int r = bd.activate();
    return (r == JOptionPane.YES_OPTION);
  }

  @Override
  public void displayURL(String _url) {
    getApp().displayURL(_url);
  }

  public boolean isCloseFrameMode() {
    return true;
  }

  // commands

  @Override
  public void about() {
    BuInformationsSoftware il = getInformationsSoftware();
    BuDialogMessage bd = new BuDialogMessage(getApp(), il, il.about());
    bd.activate();
  }

  @Override
  public void cut() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuCutCopyPasteInterface) {
      ((BuCutCopyPasteInterface) frame).cut();
    } else {
      FuLog.warning(frame + " doesn't implement BuCutCopyPasteInterface");
    }
  }

  @Override
  public void copy() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuCutCopyPasteInterface) {
      ((BuCutCopyPasteInterface) frame).copy();
    } else {
      FuLog.warning(frame + " doesn't implement BuCutCopyPasteInterface");
    }
  }

  @Override
  public void duplicate() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuCutCopyPasteInterface) {
      ((BuCutCopyPasteInterface) frame).duplicate();
    } else {
      FuLog.warning(frame + " doesn't implement BuCutCopyPasteInterface");
    }
  }

  @Override
  public void find() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuSelectFindReplaceInterface) {
      ((BuSelectFindReplaceInterface) frame).find();
    } else {
      FuLog.warning(frame + " doesn't implement BuSelectFindReplaceInterface");
    }
  }

  @Override
  public void license() {
    BuInformationsSoftware il = getInformationsSoftware();
    BuDialogMessage bd = new BuDialogMessage(getApp(), il, il.license());
    bd.activate();
  }

  @Override
  public void paste() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuCutCopyPasteInterface) {
      ((BuCutCopyPasteInterface) frame).paste();
    } else {
      FuLog.warning(frame + " doesn't implement BuCutCopyPasteInterface");
    }
  }

  @Override
  public void undo() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuUndoRedoInterface) {
      ((BuUndoRedoInterface) frame).undo();
    } else {
      FuLog.warning(frame + " doesn't implement BuUndoRedoInterface");
    }
  }

  @Override
  public void redo() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuUndoRedoInterface) {
      ((BuUndoRedoInterface) frame).redo();
    } else {
      FuLog.warning(frame + " doesn't implement BuUndoRedoInterface");
    }
  }

  @Override
  public void replace() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuSelectFindReplaceInterface) {
      ((BuSelectFindReplaceInterface) frame).replace();
    } else {
      FuLog.warning(frame + " doesn't implement BuSelectFindReplaceInterface");
    }
  }

  @Override
  public void select() {
    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof BuSelectFindReplaceInterface) {
      ((BuSelectFindReplaceInterface) frame).select();
    } else {
      FuLog.warning(frame + " doesn't implement BuSelectFindReplaceInterface");
    }
  }

  public void close() {
    if (isCloseFrameMode()) {
      JInternalFrame frame = getCurrentInternalFrame();

      if (!frame.isClosed() && frame.isClosable()) {
        try {
          frame.setClosed(true);
        } catch (PropertyVetoException ex) {
          FuLog.warning(ex);
        }
      }
    }
  }

  @Override
  public void exit() {
    getApp().exit();
  }

  @Override
  public void print() {
    getApp().print();
  }

  public void preview() {
    JInternalFrame frame = getMainPanel().getCurrentInternalFrame();

    if (frame instanceof BuPrintable) {
      BuPrinter.preview(this, frame.getTitle(), (BuPrintable) frame);
    } else {
      BuInformationsSoftware il = getInformationsSoftware();
      /* BuDialogError bd= */
      new BuDialogError(this, il, getString("La pr�visualisation n'est pas disponible pour") + "\""
          + frame.getTitle() + "\".").activate();
    }
  }

  // Window

  @Override
  public void windowActivated(WindowEvent _evt) {
  }

  @Override
  public void windowClosed(WindowEvent _evt) {

  }

  @Override
  public void windowClosing(WindowEvent _evt) {
    exit();
  }

  @Override
  public void windowDeactivated(WindowEvent _evt) {
  }

  @Override
  public void windowDeiconified(WindowEvent _evt) {
  }

  @Override
  public void windowIconified(WindowEvent _evt) {
  }

  @Override
  public void windowOpened(WindowEvent _evt) {
  }

  Set ifListener_;

  public final void addIfListener(InternalFrameListener _l) {
    if (ifListener_ == null) {
      ifListener_ = new HashSet();
    }
    ifListener_.add(_l);
  }

  public final void removeIfListener(InternalFrameListener _l) {
    if (ifListener_ != null) {
      ifListener_.remove(_l);
    }
  }

  // InternalFrame

  @Override
  public void internalFrameActivated(InternalFrameEvent _evt) {
    ajouterActionsOutils(_evt);
    menusOrganisationFenetres();

    if (hasDesktop()) {
      BuDesktop desktop = getMainPanel().getDesktop();
      JInternalFrame frame = desktop.getCurrentInternalFrame();
      desktop.showFrame(frame);
    }
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameActivated(_evt);
      }
    }
  }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _evt) {
    enleverActionsOutils(_evt);
    menusOrganisationFenetres();
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameDeactivated(_evt);
      }
    }
  }

  @Override
  public void internalFrameClosed(InternalFrameEvent _evt) {
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameClosed(_evt);
      }
    }
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent _evt) {
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameClosing(_evt);
      }
    }
  }

  @Override
  public void internalFrameDeiconified(InternalFrameEvent _evt) {
    menusOrganisationFenetres();
    try {
      Object source = _evt.getSource();
      if (source instanceof BuInternalFrame) {
        ((BuInternalFrame) source).setSelected(true);
      }
    } catch (PropertyVetoException ex) {
      FuLog.warning(ex);
    }
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameDeiconified(_evt);
      }
    }
  }

  @Override
  public void internalFrameIconified(InternalFrameEvent _evt) {
    menusOrganisationFenetres();
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameIconified(_evt);
      }
    }
  }

  @Override
  public void internalFrameOpened(InternalFrameEvent internalFrameEvent) {
    menusOrganisationFenetres();
    if (ifListener_ != null) {
      for (Iterator it = ifListener_.iterator(); it.hasNext(); ) {
        ((InternalFrameListener) it.next()).internalFrameOpened(internalFrameEvent);
      }
    }
  }

  @Override
  public void componentAdded(ContainerEvent containerEvent) {
    menusOrganisationFenetres();
  }

  @Override
  public void componentRemoved(ContainerEvent containerEvent) {
    menusOrganisationFenetres();
  }

  protected void ajouterActionsOutils(InternalFrameEvent _evt) {
    Object source = _evt.getSource();
    if (source instanceof BuInternalFrame) {
      BuInternalFrame f = (BuInternalFrame) source;

      BuMenuBar mb = getMainMenuBar();
      BuSpecificBar sb = getSpecificBar();

      int i;

      String[] ea = f.getEnabledActions();
      for (i = 0; i < ea.length; i++)
        setEnabledForAction(ea[i], true);

      String[] da = f.getDisabledActions();
      for (i = 0; i < da.length; i++)
        setEnabledForAction(da[i], false);

      mb.addMenus(f.getSpecificMenus());
      sb.addTools(f.getSpecificTools());

      BuMainPanel mp = getMainPanel();
      if (mp.getAssistant() != null) {
        mp.getAssistant().addEmitters(sb);
      }

      f.adjustActions();
    }
  }

  protected void enleverActionsOutils(InternalFrameEvent internalFrameEvent) {
    Object source = internalFrameEvent.getSource();
    if (source instanceof BuInternalFrame) {
      BuInternalFrame f = (BuInternalFrame) source;
      BuMenuBar mb = getMainMenuBar();
      BuSpecificBar sb = getSpecificBar();

      int i;

      String[] ea = f.getEnabledActions();
      for (i = 0; i < ea.length; i++)
        setEnabledForAction(ea[i], false);

      String[] da = f.getDisabledActions();
      for (i = 0; i < da.length; i++)
        setEnabledForAction(da[i], true);

      mb.removeMenus(f.getSpecificMenus());
      sb.removeTools(f.getSpecificTools());
    }
  }

  protected void menusOrganisationFenetres() {
    if (!hasDesktop()) {
      return;
    }

    BuDesktop desktop = getMainPanel().getDesktop();
    boolean appli = (getApp() instanceof JFrame);
    JInternalFrame frame = desktop.getCurrentInternalFrame();
    boolean b;

    b = (desktop.getNormalFramesCount() > 0);
    setEnabledForAction("CASCADE", b);
    setEnabledForAction("MOSAIQUE", b);

    b = (desktop.getIconifiedFramesCount() > 0);
    setEnabledForAction("RANGERICONES", b);

    b = (desktop.getPalettesCount() > 0);
    setEnabledForAction("RANGERPALETTES", b);

    b = (frame != null);
    setEnabledForAction("PLEINECRAN", b && appli);

    if (isCloseFrameMode()) {
      b = (frame != null) && frame.isClosable();
      setEnabledForAction("FERMER", b);
    }
  }
}
