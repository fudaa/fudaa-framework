/**
 * @modification $Date: 2007-06-20 12:20:44 $
 * @statut unstable
 * @file BuPreferences.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.ColorModel;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;

import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.IconUIResource;
import javax.swing.plaf.metal.MetalTheme;

import com.memoire.fu.FuFactoryInteger;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuPreferences;
import com.memoire.fu.FuResource;

/**
 * A class to manage user preferences for the standard GUI.
 */
public class BuPreferences extends FuPreferences {
  public static final BuPreferences BU = new BuPreferences();

  protected BuPreferences() {
  }

  public String getLanguages() {
    return "de,en,es,eo,fr,it,hi,hu,nl,pt,ru";
  }

  // Specific
  public void applyLookAndFeel() {
    applyLookAndFeel(false);
  }

  public final void applyNetwork() {

    // SOCKS
    if (getBooleanProperty("socks.proxy.activated", false)) {
      FuLib.setSystemProperty("socks.proxyHost", getStringProperty("socks.proxy.host", null));
      FuLib.setSystemProperty("socks.proxyPort", getStringProperty("socks.proxy.port", null));
      FuLib.setSystemProperty("java.net.socks.username", getStringProperty("socks.proxy.username", null));
      FuLib.setSystemProperty("java.net.socks.password", getStringProperty("socks.proxy.password", null));
    }

    // HTTP
    if (getBooleanProperty("http.proxy.activated", false)) {
      FuLib.setSystemProperty("http.proxyHost", getStringProperty("http.proxy.host", null));
      FuLib.setSystemProperty("http.proxyPort", getStringProperty("http.proxy.port", null));
      FuLib.setSystemProperty("http.nonProxyHosts", getStringProperty("http.proxy.skip", null));
    }

    // FTP
    if (getBooleanProperty("ftp.proxy.activated", false)) {
      FuLib.setSystemProperty("ftp.proxyHost", getStringProperty("ftp.proxy.host", null));
      FuLib.setSystemProperty("ftp.proxyPort", getStringProperty("ftp.proxy.port", null));
      FuLib.setSystemProperty("ftp.nonProxyHosts", getStringProperty("ftp.proxy.skip", null));
    }
  }

  public void applyLookAndFeel(boolean _forced) {
    // FuLog.debug("BPR: swing.metalTheme="+FuLib.getSystemProperty("swing.metalTheme"));
    boolean forced = _forced;
    FuLib.setSystemProperty("swing.aatext", getBooleanProperty("antialias.all", false) ? "true" : "false");

    String os = FuLib.getSystemProperty("os.name");

    String lnfname = getStringProperty("lookandfeel.name");
    String lnfclass = getStringProperty("lookandfeel.class");

    if ((FuLib.jdk() <= 1.4) && lnfname.equals("ASPECT_SYNTHETICA")) {
      lnfname = "ASPECT_METAL";
    }

    if (lnfname.equals("ASPECT_DEFAUT")) {
      lnfclass = "";
    } else if (lnfname.equals("ASPECT_AMIGA")) {
      lnfclass = "swing.addon.plaf.threeD.ThreeDLookAndFeel";
    } else if (lnfname.equals("ASPECT_FHLAF")) {
      lnfclass = "com.shfarr.ui.plaf.fh.FhLookAndFeel";
    } else if (lnfname.equals("ASPECT_GTK")) {
      lnfclass = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
    } else if (lnfname.equals("ASPECT_KUNSTSTOFF")) {
      lnfclass = "com.incors.plaf.kunststoff.KunststoffLookAndFeel";
    } else if (lnfname.equals("ASPECT_LIQUID")) {
      lnfclass = "com.birosoft.liquid.LiquidLookAndFeel";
    } // else if (lnfname.equals("ASPECT_SUBSTANCE")) {
    // lnfclass = "org.jvnet.substance.skin.SubstanceBusinessLookAndFeel";
    //
    // try {
    // UIManager.setLookAndFeel(
    // "org.jvnet.substance.skin.SubstanceBusinessLookAndFeel");
    // forced = false;
    // } catch (Exception ex) {
    // FuLog.error("BPR: could not set substance theme", ex);
    // }
    //
    // }
    else if (lnfname.equals("ASPECT_MAC")) {
      FuLib.setSystemProperty("os.name", "Mac OS");
      lnfclass = "com.sun.java.swing.plaf.mac.MacLookAndFeel";
    } else if (lnfname.equals("ASPECT_METAL")) {
      lnfclass = "javax.swing.plaf.metal.MetalLookAndFeel";
      try {
        Class c = Class.forName(lnfclass);
        Method m = c.getMethod("setCurrentTheme", new Class[]{MetalTheme.class});
        int i = Integer.parseInt(getStringProperty("metal.theme", "THEME_METAL0").substring(11));

        // FuLog.debug("SET THEME "+i+" "+BuMetalCustomTheme.getList()[i]);
        // FuLog.printStackTrace();
        m.invoke(null, new Object[]{BuMetalCustomTheme.getList()[i]});
      } catch (Exception ex) {
        FuLog.warning("BPR: metal theme", ex);
      } finally {
        BuLib.forgetLnf();
      }
    } else if (lnfname.equals("ASPECT_NIMBUS")) {
      lnfclass = "javax.swing.plaf.nimbus.NimbusLookAndFeel";
    } else if (lnfname.equals("ASPECT_METOUIA")) {
      lnfclass = "net.sourceforge.mlf.metouia.MetouiaLookAndFeel";
    } else if (lnfname.equals("ASPECT_MOTIF")) {
      lnfclass = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    } else if (lnfname.equals("ASPECT_NEXT")) {
      lnfclass = "nextlf.plaf.NextLookAndFeel";
    } else if (lnfname.equals("ASPECT_ORGANIC")) {
      lnfclass = "javax.swing.plaf.organic.OrganicLookAndFeel";
    } else if (lnfname.equals("ASPECT_OYOAHA")) {
      lnfclass = "com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel";
      try {
        Class c = Class.forName(lnfclass);
        Object o = c.newInstance();
        Method m = c.getMethod("setOyoahaTheme", new Class[]{File.class});
        m.invoke(o, new Object[]{new File(getStringProperty("oyoaha.theme"))});
        UIManager.setLookAndFeel((LookAndFeel) o); // Oyoaha
        applyFontScaling();
        forced = false;
      } catch (Exception ex) {
        FuLog.error("BPR: could not set oyoaha theme", ex);
      } finally {
        BuLib.forgetLnf();
      }
    } else if (lnfname.equals("ASPECT_PLASTIC")) {
      lnfclass = "com.jgoodies.looks.plastic.PlasticLookAndFeel";
    } else if (lnfname.equals("ASPECT_PLASTIC3D")) {
      lnfclass = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
    } else if (lnfname.equals("ASPECT_PLASTICXP")) {
      lnfclass = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
    } else if (lnfname.equals("ASPECT_SKINLF")) {
      lnfclass = "com.l2fprod.gui.plaf.skin.SkinLookAndFeel";
      try {
        String t = getStringProperty("skinlf.theme");
        Class c = Class.forName(lnfclass);
        Method m = null;

        if (t.toLowerCase().endsWith(".zip") || t.toLowerCase().endsWith(".jar")) {
          m = c.getMethod("loadThemePack",
            new Class[]{String.class});
        } else {
          m = c.getMethod("loadSkin", new Class[]{String.class});
        }

        Object s = m.invoke(null, new Object[]{t});

        m = c.getMethod("setSkin", new Class[]{Class.forName("com.l2fprod.gui.plaf.skin.Skin")});
        m.invoke(null, new Object[]{s});
      } catch (Exception ex) {
        FuLog.error("BPR: could not set skinlf theme", ex);
      } finally {
        BuLib.forgetLnf();
      }
    } else if (lnfname.equals("ASPECT_SLAF")) {
      lnfclass = "com.memoire.slaf.SlafLookAndFeel";
      try {
        Class c = Class.forName(lnfclass);
        Method m = c.getMethod("setCurrentTheme", new Class[]{String.class});
        m.invoke(null, new Object[]{getStringProperty("slaf.theme")});
      } catch (Exception ex) {
      } finally {
        BuLib.forgetLnf();
      }
    } else if (lnfname.equals("ASPECT_SYNTHETICA")) {
      lnfclass = "de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel";
    } else if (lnfname.equals("ASPECT_TONIC")) {
      lnfclass = "com.digitprop.tonic.TonicLookAndFeel";
    } else if (lnfname.equals("ASPECT_WINDOWS")) {
      lnfclass = "com.jgoodies.looks.windows.WindowsLookAndFeel";

      FuLib.setSystemProperty("os.name", "Windows");
      if (!FuLib.classExists(lnfclass)) {
        lnfclass = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
      }
    }

    setLookAndFeel(lnfclass, forced);

    boolean b = getBooleanProperty("lookandfeel.decorated", false);
    if (b && FuLib.isKorteRunning()) {
      b = false;
    }
    JDialog.setDefaultLookAndFeelDecorated(b);
    JFrame.setDefaultLookAndFeelDecorated(b);

    if (os != null) {
      FuLib.setSystemProperty("os.name", os);
    }

    if (BuLib.isMetal()) {
      Icon ic = new IconUIResource(new BuEmptyIcon(10, 10));
      UIManager.getDefaults().put("Menu.checkIcon", ic);
      UIManager.getDefaults().put("MenuItem.checkIcon", ic);
    }

    if (BuLib.isOcean() && Color.white.equals(UIManager.getColor("Desktop.background"))) {
      UIManager.getDefaults().put(
        "Desktop.background", new ColorUIResource(184, 207, 229));
    }
  }

  boolean setLookAndFeel(String _lnfclass, boolean _forced) {
    boolean r = true;

    // this test is important under JDK1.2
    if (!"".equals(_lnfclass) && (_forced || !UIManager.getLookAndFeel().getClass().getName().equals(_lnfclass))) {
      try {
        UIManager.setLookAndFeel(_lnfclass);
        applyFontScaling();
      } catch (Throwable ex) {
        r = false;
      } finally {
        BuLib.forgetLnf();
      }
    } else if ("".equals(_lnfclass) && _forced) {
      applyFontScaling();
    }

    BuIcon icon;
    icon = BuResource.BU.getButtonIcon("valider");
    if (!icon.isDefault() && (UIManager.get("FileChooser.approveButtonIcon") == null)) {
      UIManager.getDefaults().put(
        "FileChooser.approveButtonIcon", icon);
    }
    icon = BuResource.BU.getButtonIcon("annuler");
    if (!icon.isDefault() && (UIManager.get("FileChooser.cancelButtonIcon") == null)) {
      UIManager.getDefaults().put(
        "FileChooser.cancelButtonIcon", icon);
    }
    icon = BuResource.BU.getButtonIcon("renommer");
    if (!icon.isDefault() && (UIManager.get("FileChooser.renameButtonIcon") == null)) {
      UIManager.getDefaults().put(
        "FileChooser.renameButtonIcon", icon);
    }

    return r;
  }

  public int getFontScaling() {
    int w = Toolkit.getDefaultToolkit().getScreenSize().width;
    int f = getIntegerProperty("lookandfeel.fontscale", Math.max(100, (int) Math.round(100. * w / 1280)));
    if (f < 10) {
      f = 10;
    }
    if (f > 1000) {
      f = 1000;
    }
    return f;
  }

  private void applyFontScaling() {
    int f = getFontScaling();
    if (f != 100) {
      Hashtable t = UIManager.getDefaults();
      Enumeration e = t.keys();
      while (e.hasMoreElements()) {
        Object o = e.nextElement();
        if (o instanceof String) {
          String k = (String) o;
          if (k.toLowerCase().endsWith("font")) {
            o = t.get(o);
            if (o instanceof FontUIResource) {
              FontUIResource v = (FontUIResource) o;
              // FuLog.debug("BPR: ui font "+k+" = "+v);
              int s = (int) Math.round((double) f * (double) v.getSize() / 100.);
              v = new FontUIResource(v.getFamily(), v.getStyle(), s);
              UIManager.getDefaults().put(k, v);
              // FuLog.debug("BPR: new font is "+v);
            }
          }
        }
      }
    }
  }

  public void applyLanguage(String _dispo) {
    applyLanguage(_dispo, false);
  }

  /**
   * Applique le langage d�fini dans les pr�f�rences parmis un ensemble de langages possibles
   *
   * @param _dispo Les langages possibles, s�par�s par une ','
   * @param _fallBackOnEnglish
   */
  public void applyLanguage(String _dispo, boolean _fallBackOnEnglish) {
    applyLanguage(_dispo, getStringProperty("locale.language"), _fallBackOnEnglish);
  }

  /**
   * Applique le langage demand� parmis un ensemble de langages possibles.
   *
   * @param _dispo Les langages possibles, s�par�s par une ','
   * @param l Le langage demand�
   * @param _fallBackOnEnglish
   */
  public void applyLanguage(String _dispo, String l, boolean _fallBackOnEnglish) {
    FuResource.setDefaultToEnglish(getBooleanProperty("translation.fallBackOnEnglish", _fallBackOnEnglish));

    String c = getStringProperty("locale.country", l.toUpperCase());
    // System.err.println("BPR: "+l+" "+_dispo+" "+BuPreferences.BU);

    if (_dispo.indexOf(l) >= 0) {
      // System.err.println("LANG="+l);
      if (!l.equals("") && !l.equals(Locale.getDefault().getLanguage())) {
        if ("en".equals(l) && "EN".equals(c)) {
          c = "US";
        }
        Locale.setDefault(new Locale(l, c));
        FuLib.setSystemProperty("user.language", l);
        FuLib.setSystemProperty("user.country", c);
      }
    } else {
      FuLog.warning("LANG=fr (" + l + " is not available)");
      Locale.setDefault(Locale.FRENCH);
    }
  }

  public void applyOn(Object _o) {
    if (_o == null) {
      return;
    }

    if (!(_o instanceof BuCommonInterface)) {
      throw new RuntimeException("" + _o + " is not a BuCommonInterface.");
    }

    BuCommonInterface appli = (BuCommonInterface) _o;
    BuMainPanel main = appli.getMainPanel();
    BuDesktop desktop = main.getDesktop();

    // Window
    BuCommonInterface a = appli.getApp();
    if (a instanceof JFrame) {
      final JFrame f = (JFrame) a;
      
      f.getRootPane().invalidate();

      // Dimension d = f.getPreferredSize();
      Dimension e = f.getToolkit().getScreenSize();

      int left = getIntegerProperty("screen.left", 0);
      int right = getIntegerProperty("screen.right", 0);
      int top = getIntegerProperty("screen.top", 0);
      int bottom = getIntegerProperty("screen.bottom", 0);

      switch (getIntegerProperty("window.size", 2)) {
        // "normale"
        case 0:
          f.setLocation(Math.max(left, e.width / 10), Math.max(top, e.height / 10));
          f.setSize(Math.min(e.width * 4 / 5, e.width - left - right), Math
            .min(e.height * 4 / 5, e.height - top - bottom));
          break;
        //"maximale"
        case 1:
          f.setLocation(left, top);
          f.setSize(Math.min(e.width - f.getLocation().x, e.width - left - right), Math.min(e.height - f.getLocation().y,
            e.height - top - bottom));
          break;
        // "derni�re"
        case 2:
          int x = getIntegerProperty("window.x");
          int y = getIntegerProperty("window.y");
          int w = getIntegerProperty("window.w");
          int h = getIntegerProperty("window.h");

          // Dimensions non maximis�es
          if ((w > 0) && (h > 0)) {
            // Controle que la position est bien dans un ecran
            // Si ce n'est pas le cas (ca peut �tre suite a un changement de configuration d'ecran), on replace la fenetre.
            boolean isPosOnScreen = false;
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            for (GraphicsDevice curGs : gs) {
              GraphicsConfiguration[] gc = curGs.getConfigurations();
              for (GraphicsConfiguration curGc : gc) {
                Rectangle bounds = curGc.getBounds();
                if (x+8 > bounds.x && x+8 < bounds.x + bounds.width && y+8 > bounds.y && y+8 < bounds.y + bounds.height) {
                  isPosOnScreen = true;
                  break;
                }
              }
            }
            // Position au dela de l'ecran
            if (!isPosOnScreen) {
              while (x+8 >= e.width) {
                x -= e.width;
              }
              while (y+8 >= e.height) {
                y -= e.height;
              }
            }
            
            f.setLocation(x, y);
            f.setSize(w, h);
          }
          // BM : Si les dimensions sont negatives ou egales a 0, on maximize la fenetre
          else {
            f.setLocation(x, y);
            f.setExtendedState(JFrame.MAXIMIZED_BOTH);
          }
          break;
        // "minimale"
        case 3:
          try {
            Method m = JFrame.class.getMethod("setState", new Class[]{Integer.class});
            m.invoke(f, new Object[]{FuFactoryInteger.get(1)});
          } catch (Exception ex) {
          }
          // f.setState(Frame.ICONIFIED);
          // Frame.ICONIFIED = 1;
          break;
      }

      f.getRootPane().revalidate();
    }

    // Aspect

    /*
     * FuLib.setSystemProperty ("swing.aatext", getBooleanProperty("antialias.all",false) ? "true" : "false"); String
     * lnfname =getStringProperty("lookandfeel.name"); String lnfclass=getStringProperty("lookandfeel.class");
     * if((FuLib.jdk()<1.4)&& (lnfname.equals("ASPECT_FHLAF")|| lnfname.equals("ASPECT_KUNSTSTOFF")||
     * lnfname.equals("ASPECT_LIQUID"))) lnfname="ASPECT_METAL"; if(BuLib.isSwing10()&&lnfname.equals("ASPECT_SLAF"))
     * lnfname="ASPECT_METAL"; if((FuLib.jdk()<=1.1)&&lnfname.equals("ASPECT_SKINLF")) lnfname="ASPECT_METAL";
     * if((FuLib.jdk()<=1.3)&&lnfname.equals("ASPECT_GTK")) lnfname="ASPECT_METAL"; if((FuLib.jdk()<=1.4)&&lnfname.equals("ASPECT_SYNTHETICA"))
     * lnfname="ASPECT_METAL"; if(lnfname.equals("ASPECT_AMIGA" )) lnfclass="swing.addon.plaf.threeD.ThreeDLookAndFeel"; //
     * else if(lnfname.equals("ASPECT_BASIC" )) lnfclass="com.memoire.bu.BuBasicLookAndFeel"; else
     * if(lnfname.equals("ASPECT_FHLAF" )) lnfclass="com.shfarr.ui.plaf.fh.FhLookAndFeel"; else
     * if(lnfname.equals("ASPECT_GTK" )) lnfclass="com.sun.java.swing.plaf.gtk.GTKLookAndFeel"; else
     * if(lnfname.equals("ASPECT_KUNSTSTOFF")) lnfclass="com.incors.plaf.kunststoff.KunststoffLookAndFeel"; else
     * if(lnfname.equals("ASPECT_LIQUID" )) lnfclass="com.birosoft.liquid.LiquidLookAndFeel"; else
     * if(lnfname.equals("ASPECT_MAC" )) lnfclass="com.sun.java.swing.plaf.mac.MacLookAndFeel"; else
     * if(lnfname.equals("ASPECT_METAL" )) lnfclass="javax.swing.plaf.metal.MetalLookAndFeel"; else
     * if(lnfname.equals("ASPECT_METOUIA" )) lnfclass="net.sourceforge.mlf.metouia.MetouiaLookAndFeel"; else
     * if(lnfname.equals("ASPECT_MOTIF" )) lnfclass="com.sun.java.swing.plaf.motif.MotifLookAndFeel"; else
     * if(lnfname.equals("ASPECT_NEXT" )) lnfclass="nextlf.plaf.NextLookAndFeel"; else
     * if(lnfname.equals("ASPECT_ORGANIC" )) lnfclass="com.sun.java.swing.plaf.organic.OrganicLookAndFeel"; else
     * if(lnfname.equals("ASPECT_OYOAHA" )) lnfclass="com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel"; else
     * if(lnfname.equals("ASPECT_PLASTIC" )) lnfclass="com.jgoodies.looks.plastic.PlasticLookAndFeel"; else
     * if(lnfname.equals("ASPECT_PLASTIC3D" )) lnfclass="com.jgoodies.looks.plastic.Plastic3DLookAndFeel"; else
     * if(lnfname.equals("ASPECT_PLASTICXP" )) lnfclass="com.jgoodies.looks.plastic.PlasticXPLookAndFeel"; else
     * if(lnfname.equals("ASPECT_SKINLF" )) lnfclass="com.l2fprod.gui.plaf.skin.SkinLookAndFeel"; else
     * if(lnfname.equals("ASPECT_SLAF" )) lnfclass="com.memoire.slaf.SlafLookAndFeel"; else
     * if(lnfname.equals("ASPECT_SYNTHETICA")) lnfclass="de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel";
     * else if(lnfname.equals("ASPECT_TONIC" )) lnfclass="com.digitprop.tonic.TonicLookAndFeel"; else
     * if(lnfname.equals("ASPECT_WINDOWS" )) lnfclass="com.sun.java.swing.plaf.windows.WindowsLookAndFeel"; // this test
     * is important under JDK1.2 if(!lnfclass.equals(""))
     * if(!UIManager.getLookAndFeel().getClass().getName().equals(lnfclass)) appli.setLookAndFeel(lnfclass);
     * if(FuLib.jdk()>=1.4) { boolean b=getBooleanProperty("lookandfeel.decorated",false); if(b&&FuLib.isKorteRunning())
     * b=false; JDialog.setDefaultLookAndFeelDecorated(b); JFrame. setDefaultLookAndFeelDecorated(b); }
     */
    applyLookAndFeel();
    // Desktop
    applyOnDesktop(desktop);

    // Main Panel
    applyOnMainPanel(main);
  }

  public Image getTexture(int _num) {
    String url = BuPreferences.BU.getStringProperty("desktop.image." + _num);
    Image img = null;

    if (!"".equals(url.trim())) {
      BuIcon icon = new BuIcon(url);
      if (!icon.isDefault()) {
        img = icon.getImage();
      }
    }

    if (img == null) {
      img = BuResource.BU.getImage("background_desktop_" + _num);
    }

    return img;
  }

  public void applyOnMainPanel(BuMainPanel _main) {
    if (_main.getSpecificBar() != null) {
      _main.getSpecificBar().setVisible(
        getBooleanProperty("specificbar.visible", true));
    }
    if (_main.getLeftColumn() != null) {
      _main.getLeftColumn().setVisible(getBooleanProperty("leftcolumn.visible", true));
    }
    if (_main.getRightColumn() != null) {
      _main.getRightColumn().setVisible(
        getBooleanProperty("rightcolumn.visible", true));
    }
    if (_main.getStatusBar() != null) {
      _main.getStatusBar().setVisible(getBooleanProperty("statusbar.visible", true));
    }

    boolean va = getBooleanProperty("assistant.visible", true);
    _main.getLeftColumn().toggleComponent("ASSISTANT", va);
    _main.getRightColumn().toggleComponent("ASSISTANT", va);

    int lcwidth = getIntegerProperty("leftcolumn.width", -1);
    if (lcwidth > 0) {
      int height = _main.getLeftColumn().getPreferredSize().height;
      _main.getLeftColumn().setPreferredSize(new Dimension(lcwidth, height));
    }

    int rcwidth = getIntegerProperty("rightcolumn.width", -1);
    if (rcwidth > 0) {
      int height = _main.getRightColumn().getPreferredSize().height;
      _main.getRightColumn().setPreferredSize(new Dimension(rcwidth, height));
    }

    if (getBooleanProperty("columns.swapped", false) ^ _main.isColumnsSwapped()) {
      _main.swapColumns();
    }

    _main.revalidate();
  }

  public void applyOnDesktop(BuDesktop _desktop) {
    if (_desktop == null) {
      return;
    }

    BuBackgroundPainter bp = _desktop.getBackgroundPainter();
    bp.setGrid(getBooleanProperty("desktop.grid", false));
    bp.setDots(getBooleanProperty("desktop.dots", false));
    _desktop.setBackground(UIManager.getColor("Panel.background"));
    if (getStringProperty("desktop.background").equals("DESKTOP_ROUGE")) {
      _desktop.setBackground(new Color(128, 64, 64));
      bp.setIcon(null);
    } else if (getStringProperty("desktop.background").equals("DESKTOP_VERT")) {
      _desktop.setBackground(new Color(64, 128, 64));
      bp.setIcon(null);
    } else if (getStringProperty("desktop.background").equals("DESKTOP_BLEU")) {
      _desktop.setBackground(new Color(64, 64, 128));
      bp.setIcon(null);
    } else if (getStringProperty("desktop.background").equals("DESKTOP_ORANGE")) {
      _desktop.setBackground(new Color(192, 128, 96));
      bp.setIcon(null);
    } else if (getStringProperty("desktop.background").equals("DESKTOP_SIMILAIRE")) {
      _desktop.setBackground(UIManager.getColor("Panel.background"));
      bp.setIcon(null);
    } else if (getStringProperty("desktop.background").equals("DESKTOP_DEFAUT")) {
      _desktop.setBackground(UIManager.getColor("Panel.background"));
      bp.setIcon(null);
    }

    if (getStringProperty("desktop.decor").equals("DESKTOP_UNIFORME")) {
      bp.setIcon(null);
      bp.setGradient(false);
    } else if (getStringProperty("desktop.decor").equals("DESKTOP_DEGRADE")) {
      bp.setIcon(null);
      bp.setGradient(true);
    } else if (getStringProperty("desktop.decor").startsWith("DESKTOP_TEXTURE")) {
      if (BuLib.getUIBoolean("Desktop.textureAlwaysUsed")) {
        bp.setIcon(null);
      } else {
        try {
          String st = getStringProperty("desktop.texture", "DESKTOP_TEXTURE1");
          int nt = Integer.parseInt(st.substring(15));
          bp.setIcon(BuLib.filter(new BuIcon(getTexture(nt))));
        } catch (Exception ex) {
          FuLog.warning("BPR: texture not found", ex);
        }
      }
    }

    _desktop.setBackgroundPainter(bp);
  }

  public boolean isEnabled(String _option) {
    boolean r = true;
    if (_option.startsWith("ASPECT_")) {
      if (_option.endsWith("AMIGA")) {
        r = FuLib.classExists("swing.addon.plaf.threeD.ThreeDLookAndFeel");
      } else if (_option.endsWith("FHLAF")) {
        r = FuLib.classExists("com.shfarr.ui.plaf.fh.FhLookAndFeel");
      } else if (_option.endsWith("GTK")) {
        r = FuLib.classExists("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
      } else if (_option.endsWith("KUNSTSTOFF")) {
        r = FuLib
          .classExists("com.incors.plaf.kunststoff.KunststoffLookAndFeel");
      } else if (_option.endsWith("LIQUID")) {
        r = FuLib.classExists("com.birosoft.liquid.LiquidLookAndFeel");
      } else if (_option.endsWith("MAC")) {
        r = FuLib.classExists("com.sun.java.swing.plaf.mac.MacLookAndFeel");
      } else if (_option.endsWith("METAL")) {
        r = FuLib.classExists("javax.swing.plaf.metal.MetalLookAndFeel");
      } else if (_option.endsWith("MOTIF")) {
        r = FuLib.classExists("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
      } else if (_option.endsWith("NEXT")) {
        r = FuLib.classExists("nextlf.plaf.NextLookAndFeel");
      } else if (_option.endsWith("ORGANIC")) {
        r = FuLib.classExists("com.sun.java.swing.plaf.organic.OrganicLookAndFeel");
      } else if (_option.endsWith("OYOAHA")) {
        r = FuLib.classExists("com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel");
      } else if (_option.endsWith("PLASTIC")) {
        r = FuLib.classExists("com.jgoodies.looks.plastic.PlasticLookAndFeel");
      } else if (_option.endsWith("PLASTIC3D")) {
        r = FuLib.classExists("com.jgoodies.looks.plastic.Plastic3DLookAndFeel");
      } else if (_option.endsWith("PLASTICXP")) {
        r = FuLib.classExists("com.jgoodies.looks.plastic.PlasticXPLookAndFeel");
      } else if (_option.endsWith("SKINLF")) {
        r = FuLib.classExists("com.l2fprod.gui.plaf.skin.SkinLookAndFeel");
      } else if (_option.endsWith("SLAF")) {
        r = FuLib.classExists("com.memoire.slaf.SlafLookAndFeel");
      } else if (_option.endsWith("SYNTHETICA")) {
        r = FuLib
          .classExists("de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel");
      } else if (_option.endsWith("TONIC")) {
        r = FuLib.classExists("com.digitprop.tonic.TonicLookAndFeel");
      } else if (_option.endsWith("WINDOWS")) {
        r = FuLib.classExists("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
      }
    }
    return r;
  }

  // GET
  public Color getColorProperty(String _key) {
    return getColorProperty(_key, Color.black);
  }

  public Color getColorProperty(String _key, Color _default) {
    String s = getStringProperty(_key);
    Color r = _default;

    if (s != null) {
      try {
        r = new Color(Integer.parseInt(s, 16));
      } catch (Exception ex) {
      }
    }

    return r;
  }

  public Font getFontProperty(String _key) {
    return getFontProperty(_key, BuLib.DEFAULT_FONT);
  }

  public Font getFontProperty(String _key, Font _default) {
    String s = getStringProperty(_key);
    Font r = createFontFromString(_default, s);

    return r;
  }

  public static Font createFontFromString(Font _default, String s) {
    Font r = _default;

    if (s != null) {
      try {
        String f = "SansSerif";
        int t = Font.PLAIN;
        int h = 12;

        int i = s.lastIndexOf(',');
        if (i >= 0) {
          h = Integer.parseInt(s.substring(i + 1), 10);
          s = s.substring(0, i);
          i = s.lastIndexOf(',');
          if (i >= 0) {
            String u = s.substring(i + 1).toLowerCase();
            s = s.substring(0, i);
            if (u.startsWith("bold")) {
              t |= Font.BOLD;
              u = u.substring(4);
            }
            if (u.endsWith("bold")) {
              t |= Font.BOLD;
              u = u.substring(0, u.length() - 4);
            }
            if (u.startsWith("italic")) {
              t |= Font.ITALIC;
              u = u.substring(6);
            }
            if (u.endsWith("italic")) {
              t |= Font.ITALIC;
              u = u.substring(0, u.length() - 6);
            }
          }
        }
        if (!"".equals(s.trim())) {
          f = s;
        }

        r = new Font(f, t, h);
      } catch (Exception ex) {
        System.err.println("BPR: getFont() " + ex + " " + s);
      }
    }
    return r;
  }

  // PUT
  public void putColorProperty(String _key, Color _color) {
    String s = Integer.toHexString(_color.getRGB());
    putStringProperty(_key, ("000000" + s).substring(s.length()));
  }

  public void putFontProperty(String _key, Font _font) {
    putStringProperty(_key, getStringValue(_font));
  }

  public static String getStringValue(Font _font) {
    String style = (((_font.getStyle() & Font.BOLD) != 0) ? "bold" : "")
      + (((_font.getStyle() & Font.ITALIC) != 0) ? "italic" : "");
    String string = _font.getFamily() + "," + style + "," + _font.getSize();
    return string;
  }
}
