/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut unstable
 * @file BuTextPane.java
 * @version 0.43
 * @author Romain Guy
 * @email guy.romain@bigfoot.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuLog;
import com.memoire.re.RE;
import com.memoire.re.REException;
import com.memoire.re.REMatch;
import com.memoire.re.RESyntax;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyleContext;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoManager;
import java.awt.*;

/**
 * This component extends a JTextPane. This component provides
 * its own methods to read and save files (even to zip them).
 * This component extends a JTextPane, providing methods to
 * undo/redo, find/replace and read/save.
 *
 * @author Romain Guy, guy.romain@bigfoot.com
 */
public class BuTextPane extends JTextPane
    implements UndoableEditListener, DocumentListener,
    BuTextComponentInterface {
  /**
   * This constant defines an open dialog box.
   */
  public static final int OPEN = 0;
  /**
   * This constant defines a save dialog box.
   */
  public static final int SAVE = 1;
  // Private declaration
  private CompoundEdit compoundEdit;
  private String currentFile;
  private int anchor;
  private boolean dirty, newText, operation;
  private UndoManager undo = new UndoManager();
  /**
   * This constant defines the size of the buffer used to read files
   */
  private static final int BUFFER_SIZE = 32768;

  public BuTextPane() {
    super();

    BuAutoStyledDocument doc = new BuAutoStyledDocument();
    doc.setTextPane(this);
    doc.addDocumentListener(this);
    doc.addUndoableEditListener(this);
    setDocument(doc);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  @Override
  public void setBorder(Border _b) {
    Border b = _b;
    if (b != null) {
      b = new CompoundBorder(b, new EmptyBorder(0, 30, 0, 0));
    } else {
      b = new EmptyBorder(0, 30, 0, 0);
    }
    super.setBorder(b);
  }

  public void select() {
    select(0, getLength());
  }

  public void duplicate() {
    int s = getSelectionStart();
    int e = getSelectionEnd();
    if (s < e) {
      copy();
      setCaretPosition(e);
      paste();
      setSelectionEnd(e + (e - s));
      setSelectionStart(e);
    }
  }

  @Override
  public void go(int _line) {
    Element map = getDocument().getDefaultRootElement();
    Element line = map.getElement(_line);
    select(line.getStartOffset(), line.getEndOffset());
  }

  @Override
  public void paintComponent(Graphics _g) {
    try {
      super.paintComponent(_g);
    } catch (Exception ex) {
    }

    Insets insets = getInsets();
    Dimension vs = getPreferredScrollableViewportSize(); // 40 cars
    Rectangle r = _g.getClipBounds();

    if (r == null) {
      r = new Rectangle(0, 0, getWidth(), getHeight());
    }

    int n, x, y, w, h;
    String s;

    x = vs.width * 2 + insets.left;
    _g.setColor(getBackground().darker());
    _g.drawLine(x, 0, x, getHeight());

    x = insets.left - 30;
    if ((x + 30 >= r.x) && (x <= r.x + r.width)) {
      n = getDocument().getDefaultRootElement().getElementCount();
      h = vs.height / 15;
      y = insets.top - 3;

      _g.setFont(BuLib.deriveFont(_g.getFont(), -2));
      FontMetrics fm = _g.getFontMetrics();

      for (int i = 1; i <= n; i++) {
        y += h;
        if ((y >= r.y) && (y - h <= r.y + r.height)) {
          s = "" + i;
          w = fm.stringWidth(s);
          _g.drawString("" + i, x + 28 - w, y);
        }
      }
    }
  }

  @Override
  public boolean getScrollableTracksViewportWidth() {
    return false;
  }

  @Override
  public boolean getScrollableTracksViewportHeight() {
    return false;
  }

  @Override
  public Dimension getPreferredScrollableViewportSize() {
    Dimension r = null;
    Document doc = getDocument();

    if (doc instanceof BuAutoStyledDocument) {
      /*Style fs=*/
      ((BuAutoStyledDocument) doc).
          getStyle(StyleContext.DEFAULT_STYLE);
      FontMetrics fm = getFontMetrics(getFont());
      r = new Dimension
          (fm.stringWidth("abcdefghijklmnopqrstuvwxyz01234567890,;:!"),
              fm.getHeight() * 15);
    } else {
      r = super.getPreferredScrollableViewportSize();
    }

    return r;
  }

  /*
  public int stringWidth(String _string)
  {
    int      r  =0;
    Font     f  =null;
    Document doc=getDocument();

    if(doc instanceof BuAutoStyledDocument)
    {
      Style fs=((BuAutoStyledDocument)doc).
	getStyle(StyleContext.DEFAULT_STYLE);
      f=new Font(StyleConstants.getFontFamily(fs),
		 Font.PLAIN,
		 StyleConstants.getFontSize(fs));
    }
    else f=getFont();

    FontMetrics fm=getFontMetrics
    r=fm.stringWidth(_string);
      int h=fm.getHeight()*n;
      
      Insets insets=getInsets();
      r=new Dimension
	(Math.max(r.width,w+insets.left+insets.right),
	 Math.max(r.height,h+insets.top+insets.bottom));
    }
    
    return r;
  }
  */

  @Override
  public Dimension getPreferredSize() {
    Dimension r = super.getPreferredSize();
    Document doc = getDocument();

    if (getParent() != null) {
      r = getParent().getSize();
    }

    if (doc instanceof BuAutoStyledDocument) {
      /*Style fs=*/
      ((BuAutoStyledDocument) doc).
          getStyle(StyleContext.DEFAULT_STYLE);
      FontMetrics fm = getFontMetrics(getFont());

      int n = doc.getDefaultRootElement().getElementCount();
      int c = 0;
      for (int i = 0; i < n; i++) {
        int start = getLineStartOffset(i);
        int end = getLineEndOffset(i);
        c = Math.max(c, end - start + 1);
      }
      // BUG: ignore tabs
      int w = fm.stringWidth("m") * c;
      int h = fm.getHeight() * n;

      Insets insets = getInsets();
      r = new Dimension
          (Math.max(r.width, w + insets.left + insets.right),
              Math.max(r.height, h + insets.top + insets.bottom));
    }

    return r;
  }

  @Override
  public void setCaretPosition(int _pos) {
    super.setCaretPosition(_pos);

    try {
      Rectangle r = modelToView(getCaretPosition());
      r.x = Math.max(0, r.x - 20);
      r.y = Math.max(0, r.y - 20);
      r.width = 40;
      r.height = 40;
      scrollRectToVisible(r);
    } catch (Exception ex) {
    }
  }

  public String getSyntax() {
    String r = null;
    if (getDocument() instanceof BuAutoStyledDocument) {
      r = ((BuAutoStyledDocument) getDocument()).getSyntax();
    }
    return r;
  }

  public void setSyntax(String _syntax) {
    if (getDocument() instanceof BuAutoStyledDocument) {
      ((BuAutoStyledDocument) getDocument()).setSyntax(_syntax);
    }
  }

  public void colorize(BuCommonInterface _app) {
    if (getDocument() instanceof BuAutoStyledDocument) {
      ((BuAutoStyledDocument) getDocument()).colorize(_app);
    }
  }

  /**
   * Display a file chooser dialog box.
   *
   * @param owner <code>Component</code> which 'owns' the dialog
   * @param mode Can be either <code>LOAD</code> or <code>SAVE</code>
   * @return The path to selected file, null otherwise
   */
  public static String chooseFile(Component owner, int mode) {
    return BuTextArea.chooseFile(owner, mode);
  }

  /**
   * Display a sample message in a dialog box.
   *
   * @param message The message to display
   */
  public static void showMessage(String message) {
    JOptionPane.showMessageDialog
        (null, message, "Message", JOptionPane.INFORMATION_MESSAGE);
  }

  /**
   * Display an error message in a dialog box.
   *
   * @param message The message to display
   */
  public static void showError(String message) {
    JOptionPane.showMessageDialog
        (null, message, "Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Return the full path of the opened file.
   */
  public String getCurrentFile() {
    return currentFile;
  }

  /**
   * When an operation has began, setChanged() cannot be called.
   * This is very important when we need to insert or remove some
   * parts of the texte without turning on the 'to_be_saved' flag.
   */
  public void beginOperation() {
    operation = true;
  }

  /**
   * Calling this will allow the DocumentListener to use setChanged().
   */
  public void endOperation() {
    operation = false;
  }

  /**
   * Return true if we can use the setChanged() method,
   * false otherwise.
   */
  public boolean getOperation() {
    return operation;
  }

  /**
   * This is just to reduce code size of other classes.
   *
   * @param off The line index
   * @return The offset in the text where the line begins
   */
  public int getLineStartOffset(int _n) {
    return getDocument().getDefaultRootElement().
        getElement(_n).getStartOffset();
  }

  public int getLineEndOffset(int _n) {
    return getDocument().getDefaultRootElement().
        getElement(_n).getEndOffset();
  }

  /**
   * Return true if current text is new, false otherwise.
   */
  public boolean isNew() {
    return newText;
  }

  /**
   * Return true if pane is empty, false otherwise.
   */
  public boolean isEmpty() {
    return (getLength() == 0);
  }

  /**
   * Return true if pane content has changed, false otherwise.
   */
  public boolean isDirty() {
    return dirty;
  }

  /**
   * Called when the content of the pane has changed.
   */
  public void setDirty() {
    dirty = true;
  }

  /**
   * Called after having saved or created a new document to ensure
   * the content isn't 'dirty'.
   */
  public void clean() {
    dirty = false;
  }

  /**
   * Discard all edits contained in the UndoManager.
   */
  public void discard() {
    undo.discardAllEdits();
  }

  /**
   * Useful for the GUI.
   */
  public UndoManager getUndo() {
    return undo;
  }

  /**
   * undo the last operation
   */
  public void undo() {
    //System.err.println("==== UNDO");
    if (undo.canUndo()) {
      undo.undo();
    }
  }

  /**
   * redo the last operation
   */
  public void redo() {
    if (undo.canRedo()) {
      undo.redo();
    }
  }

  /**
   * Return the anchor position.
   */
  public int getAnchor() {
    return anchor;
  }

  /**
   * Set the anchor postion.
   *
   * @param offset The new anchor's position
   */
  public void setAnchor(int offset) {
    anchor = offset;
  }

  /**
   * Return the lentgh of the text in the pane.
   */
  @Override
  public int getLength() {
    return getDocument().getLength();
  }

  /**
   * Used for ReplaceAll.
   * This merges all text changes made between the beginCompoundEdit()
   * and the endCompoundEdit() calls into only one undo event.
   */
  public void beginCompoundEdit() {
    if (compoundEdit == null) {
      compoundEdit = new CompoundEdit();
    }
  }

  /**
   * See beginCompoundEdit().
   */
  public void endCompoundEdit() {
    if (compoundEdit != null) {
      compoundEdit.end();
      if (compoundEdit.canUndo()) {
        undo.addEdit(compoundEdit);
      }
      compoundEdit = null;
    }
  }

  /**
   * Return the result of a string search.
   *
   * @param _searchStr The string to be found
   * @param start The search's start offset
   * @param ignoreCase Set to true, we'll ignore the text case
   * @return True if <code>searchStr</code> has been found, false otherwise
   */
  @Override
  public boolean find(String _searchStr, int start, boolean ignoreCase) {
    try {
      String searchStr = _searchStr;
      if (searchStr == null || searchStr.equals("")) {
        return false;
      }

      RE regexp = null;
      try {
        regexp = new RE(searchStr, (ignoreCase == true ? RE.REG_ICASE : 0) |
            RE.REG_MULTILINE, RESyntax.RE_SYNTAX_PERL5);
      } catch (Exception ex) {
      }

      if (regexp == null) {
        getToolkit().beep();
        return false;
      }

      String text = getText(start, getLength() - start);
      REMatch match = regexp.getMatch(text);
      if (match != null) {
        this.select(start + match.getStartIndex(), start + match.getEndIndex());
        return true;
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }

    return false;
  }

  @Override
  public boolean replace(String searchStr, String replaceStr,
                         int start, int end, boolean ignoreCase) {
    boolean r = false;
    try {
      r = replaceAll(searchStr, replaceStr, start, end, ignoreCase);
    } catch (Exception ex) {
    }
    return r;
  }

  /**
   * Return the result of a string replace.
   *
   * @param searchStrInit The string to be found
   * @param replaceStrInit The string which will replace <code>searchStr</code>
   * @param start The search's start offset
   * @param endInit The search's end offset
   * @param ignoreCase Set to true, we'll ignore the text case
   * @return True if the replace has been successfully done, false otherwise
   */
  public boolean replaceAll(String searchStrInit, String replaceStrInit, int start, int endInit, boolean ignoreCase) throws REException {
    boolean found = false;
    try {
      String searchStr = searchStrInit;
      if (searchStr == null || searchStr.equals("")) {
        return false;
      }

      RE regexp = null;
      try {
        regexp = new RE(searchStr, (ignoreCase == true ? RE.REG_ICASE : 0) |
            RE.REG_MULTILINE, RESyntax.RE_SYNTAX_PERL5);
      } catch (Exception ex) {
      }

      if (regexp == null) {
        getToolkit().beep();
        return false;
      }
      String replaceStr = replaceStrInit;
      if (replaceStr == null) {
        replaceStr = "";
      }

      beginCompoundEdit();

      Element map = getDocument().getDefaultRootElement();
      int startLine = map.getElementIndex(start);
      int end = endInit;
      int endLine = map.getElementIndex(end);

      for (int i = startLine; i <= endLine; i++) {
        Element lineElement = map.getElement(i);
        int lineStart;
        int lineEnd;

        if (i == startLine) {
          lineStart = start;
        } else {
          lineStart = lineElement.getStartOffset();
        }
        if (i == endLine) {
          lineEnd = end;
        } else {
          lineEnd = lineElement.getEndOffset() - 1;
        }

        lineEnd -= lineStart;
        String line = getText(lineStart, lineEnd);
        String newLine = regexp.substituteAll(line, replaceStr);
        if (line.equals(newLine)) {
          continue;
        }
        getDocument().remove(lineStart, lineEnd);
        getDocument().insertString(lineStart, newLine, null);

        end += (newLine.length() - lineEnd);
        found = true;
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    endCompoundEdit();
    return found;
  }

  /**
   * When an undoable event is fired, we add it to the undo/redo list.
   */
  @Override
  public void undoableEditHappened(UndoableEditEvent e) {
    if (!getOperation()) {
      if (compoundEdit == null) {
        undo.addEdit(e.getEdit());
      } else {
        compoundEdit.addEdit(e.getEdit());
      }
    }
  }

  /**
   * When a modification is made in the text, we turn
   * the 'to_be_saved' flag to true.
   */
  @Override
  public void changedUpdate(DocumentEvent e) {
    if (!getOperation() && !isDirty()) {
      setDirty();
    }
  }

  /**
   * When a modification is made in the text, we turn
   * the 'to_be_saved' flag to true.
   */
  @Override
  public void insertUpdate(DocumentEvent e) {
    if (!getOperation() && !isDirty()) {
      setDirty();
    }
  }

  /**
   * When a modification is made in the text, we turn
   * the 'to_be_saved' flag to true.
   */
  @Override
  public void removeUpdate(DocumentEvent e) {
    if (!getOperation() && !isDirty()) {
      setDirty();
    }
  }

  /**
   * Return a String representation of this object.
   */
  public String toString() {
    return "BuTextPane: " +
        "[filename: " + getCurrentFile() + ";" +
        " filesize: " + getLength() + "] -" +
        "[anchor: " + getAnchor() + "] -";
  }
}
