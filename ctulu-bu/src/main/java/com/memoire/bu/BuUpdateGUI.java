/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuUpdateGUI.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

/**
 * A way to safely repaint a component.
 * This code is very slow under 1.4 so it is all commented
 * (or maybe it is an XFree bug ?).
 */
public class BuUpdateGUI
  //implements Runnable
{
  /*
  private static final Stack stack_=createStack();

  private static final Stack createStack()
  {
    Stack r=new Stack();
    r.push(new BuUpdateGUI(null));
    return r;
  }

  private JComponent c_;
  private boolean    a_;

  private BuUpdateGUI(JComponent _c)
  {
    c_=_c;
    a_=(c_==null);
  }

  public void run()
  {
    repaintComponent(c_);
    a_=true;
    if(stack_.size()>10)
    {
      synchronized(this)
      {
	if(a_) stack_.removeElement(this);
      }
    }
  }

  private static final void repaintComponent(JComponent _c)
  {
    if(_c.isShowing()) //Visible()
    {
      //_c.update(_c.getGraphics());
      //paintImmediately(0,0,_c.getWidth(),_c.getHeight());

      RepaintManager rm=RepaintManager.currentManager(_c);
      //synchronized(rm)
      {
	rm.paintDirtyRegions();
	rm.markCompletelyDirty(_c);
	rm.paintDirtyRegions();
      }
    }
  }
  */

  public static void repaintNow(final JComponent _c)
  {
    //boolean later;

    if(SwingUtilities.isEventDispatchThread())
    {
      _c.paintImmediately(0,0,_c.getWidth(),_c.getHeight());
    }
    else
    {
      try
      {
	SwingUtilities.invokeAndWait(new Runnable()
	{
	  @Override
    public void run()
	  {
	    _c.paintImmediately(0,0,_c.getWidth(),_c.getHeight());
	  }
	});
      }
      catch(InterruptedException ex) { }
      catch(InvocationTargetException ex) { FuLog.error(ex); }
      
      /*
      _c.repaint();
      try { Thread.sleep(10); }
      catch(InterruptedException ex) { }
      */
    }

      /*
      _c.repaint();
      try { Thread.sleep(1); }
      catch(InterruptedException ex) { }
      */

      /*
      try
      {
	later=false;
	BuUpdateGUI t=(BuUpdateGUI)stack_.peek();
	if(t.a_) { t.a_=false; t.c_=_c; }
	else
	{
	  t=new BuUpdateGUI(_c); stack_.push(t);
	  //System.err.println("BUG: "+stack_.size());
	}
	SwingUtilities.invokeAndWait(t);
      }
      catch(InterruptedException ex) { }
      catch(InvocationTargetException ex) { }


    if(later)
      _c.repaint();//repaintComponent(_c);
      */
  }

  public static void repaintLater(JComponent _c)
  {
    _c.repaint();

    /*
    if(!SwingUtilities.isEventDispatchThread())
    {
      BuUpdateGUI t=(BuUpdateGUI)stack_.peek();
      if(t.a_) { t.a_=false; t.c_=_c; }
      else     { t=new BuUpdateGUI(_c); stack_.push(t); }
      SwingUtilities.invokeLater(t);
      //new BuUpdateGUI(_c));
    }
    else
      if(_c.isShowing())  // Visible()
	_c.repaint(500);

    //_c.repaint(500);
    */
  }
}
