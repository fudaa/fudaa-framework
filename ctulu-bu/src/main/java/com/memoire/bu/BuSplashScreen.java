/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut unstable
 * @file BuSplashScreen.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

/**
 * An easy-to-use splash screen which takes a list of classes to load.
 */
public class BuSplashScreen extends JWindow // BuPopupWindow
        implements BuBorders // Runnable
{
  /*
   * class XLabel extends BuLabel { protected void paintComponent(Graphics _g) { Color old=getForeground();
   * setForeground(Color.white); for(int i=-1;i<=1;i++) for(int j=-1;j<=1;j++) { _g.translate(i,j);
   * super.paintComponent(_g); _g.translate(-i,-j); } setForeground(old); super.paintComponent(_g); } }
   */

  /*
   * class XMultiLabel extends BuLabelMultiLine { protected void paintComponent(Graphics _g) { Color
   * old=getForeground(); setForeground(Color.white); for(int i=-1;i<=1;i++) for(int j=-1;j<=1;j++) { _g.translate(i,j);
   * super.paintComponent(_g); _g.translate(-i,-j); } setForeground(old); super.paintComponent(_g); } }
   */
  private ClassLoader loader_;
  private BuInformationsSoftware info_;
  private long delai_;
  private String[][] classes_;
  // private String base_;
  protected JComponent content_;
  protected JPanel panel_;
  protected BuPanel left_;
  protected BuProgressBar progression_;
  protected BuLabel load_;
  protected BuLabel name_;

  /*
   * public BuSplashScreen(BuInformationsSoftware _info, long _delai, String[] _classes, String _base) {
   * this(_info,_delai,new String[][]{_classes}); } public BuSplashScreen(BuInformationsSoftware _info, long _delai,
   * String[][] _classes, String _base) { this(_info,_delai,_classes); }
   */
  public BuSplashScreen(BuInformationsSoftware _info, long _delai, String[][] _classes) {
    this(_info, _delai, _classes, null);
  }

  public BuSplashScreen(BuInformationsSoftware _info, long _delai, String[][] _classes, ClassLoader _loader) {
    super(BuLib.HELPER);

    info_ = _info;
    classes_ = _classes;
    delai_ = _delai;
    // base_ =_base;
    loader_ = _loader;

    if (loader_ == null) {
      loader_ = getClass().getClassLoader();
    }

    content_ = new BuPanel();
    content_.setLayout(new BuBorderLayout());
    content_.setBorder(new LineBorder(Color.black, 1));
    build();
    content_.add(panel_, BuBorderLayout.CENTER);
    setContentPane(content_);
  }

  public void build() {
    panel_ = new JPanel();
    panel_.setLayout(new BuBorderLayout());

    BuPicture ml_logo = null;
    if (info_.logo != null) {
      ml_logo = new BuPicture(info_.logo);
    }

    BuVerticalLayout leftlo = new BuVerticalLayout();
    leftlo.setVfilled(false);
    leftlo.setVgap(5);

    left_ = new BuPanel();
    left_.setLayout(leftlo);
    // left_.setBorder(new BuLightBorder(BuLightBorder.RAISED,10));
    left_.setBorder(EMPTY5555);

    Color bg = BuLib.getColor(left_.getBackground().darker());
    left_.setBackground(bg);

    if (ml_logo != null) {
      left_.add(ml_logo);
    }

    BuLabel l1 = new BuLabel();
    BuLabel l2 = new BuLabel();
    BuLabelMultiLine l3 = new BuLabelMultiLine();
    BuLabelMultiLine l4 = new BuLabelMultiLine();

    name_ = l1;
    
    String mess;

    l1.setText(info_.name + " " + info_.version);
    l2.setText(info_.date);
    if (info_.authors != null && info_.authors.length > 0) {
      if (info_.authors.length > 1) {
        mess = BuResource.BU.getString("Auteurs") + ":\n";
      }
      else {
        mess = BuResource.BU.getString("Auteur") + ":\n";
      }
      
      for (int i=0 ; i<info_.authors.length ; i++) {
        mess += info_.authors[i] + "\n";
      }
      l3.setText(mess);
    }
    
    mess = "";
    if (info_.contact != null) {
      mess += BuResource.BU.getString("Contact: ") + info_.contact + "\n";
    }
    if (info_.http != null) {
      mess += BuResource.BU.getString("Site: ") + info_.http;
    }
    l4.setText(mess);

    l1.setHorizontalAlignment(SwingConstants.CENTER);
    l2.setHorizontalAlignment(SwingConstants.CENTER);
    l3.setHorizontalAlignment(SwingConstants.LEFT);
    l4.setHorizontalAlignment(SwingConstants.LEFT);

    l1.setFont(BuLib.deriveFont("Label", Font.BOLD, +6));
    l2.setFont(BuLib.deriveFont("Label", Font.BOLD, +2));
    l3.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
    l4.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));

    BuVerticalLayout lol = new BuVerticalLayout();
    lol.setVgap(5);
    lol.setVfilled(false);

    BuPanel mpl = new BuPanel();
    mpl.setLayout(lol);
    /*
     * mpl.setBorder(new CompoundBorder (new BuLightBorder(BuLightBorder.RAISED,5), EMPTY5555));
     */
    mpl.setBorder(EMPTY0505);

    progression_ = new BuProgressBar();
    /*
     * progression_.setBorder(new CompoundBorder (new LineBorder(Color.white,1), progression_.getBorder()));
     */

    load_ = new BuLabel();
    load_.setText("Chargement...");
    load_.setHorizontalAlignment(SwingConstants.LEFT);
    load_.setFont(BuLib.deriveFont("Label", Font.BOLD, -2));

    BuPanel px = new BuPanel();
    px.setOpaque(false);
    // px.setPreferredSize(new Dimension(0,0));

    mpl.add(px);
    mpl.add(l1);
    mpl.add(l2);
    mpl.add(l3);
    mpl.add(l4);
    mpl.add(progression_);
    mpl.add(load_);

    px.setPreferredSize(new Dimension(0, Math.max(0, 384 - mpl.getPreferredSize().height)));

    BuPicture ml_banner = null;
    if (info_.banner != null) {
      ml_banner = new BuPicture(info_.banner);
      Dimension d = new Dimension(384, 384);
      ml_banner.setPreferredSize(d);
      ml_banner.setSize(d);
      ml_banner.setMode(BuPicture.SCALE);
    }

    if (ml_banner != null) {
      mpl.setOpaque(false);
      BuPanel p = new BuPanel();
      p.setLayout(new BuOverlayLayout());
      p.add(mpl);
      p.add(ml_banner);
      mpl = p;
    }
    panel_.add(left_, BuBorderLayout.WEST);
    panel_.add(mpl, BuBorderLayout.CENTER);
  }

  public void start() {
    pack();

    // Rectangle bounds=getGraphicsConfiguration().getBounds();
    // System.err.println("BOUNDS="+bounds);

    Dimension dc = getSize();
    Dimension de = getToolkit().getScreenSize();

    int x = (de.width - dc.width) / 2;
    int y = (de.height - dc.height) / 2;

    /*
     * System.err.println("XY: "+x+","+y); System.err.println("DC: "+dc); System.err.println("DE: "+de);
     * setLocation(x,y);
     */

    // setInvoker(BuLib.HELPER); //new JFrame();
    BuLib.setDoubleBuffered(content_, false);
    content_.setDoubleBuffered(true);
    // content_.validate();

    /*
     * if(FuLib.jdk()<1.4) { //setBounds(x,y,0,0); //setVisible(true); //setVisible(false);
     * //setBounds(x,y,dc.width,dc.height); setLocation(x,y); //setVisible(true); show();
     * //setBounds(x,y,dc.width,dc.height); setLocation(x,y); } else {
     */
    // setBounds(x,y,dc.width,dc.height);
    setLocation(x, y);
    setVisible(true);
    // }

    if (!SwingUtilities.isEventDispatchThread()) {
      try {
        Thread.sleep(200L);
      } catch (InterruptedException ex) {
      }
    }

    run();
  }

  public void run() {
    long avant, apres, diff;
    avant = System.currentTimeMillis();

    // BuUpdateGUI.repaintNow(content_);//this);

    int i, j, p, pp, m, l;
    String ps = null;

    /*
     * String paquet=base_; i=paquet.lastIndexOf('.'); if(i>=0) paquet=paquet.substring(0,i+1); else paquet="";
     */
    // System.err.println("PAQUET="+paquet);
    pp = -1;

    l = 0;
    for (i = 0; i < classes_.length; i++) {
      l += classes_[i].length;
    }

    m = 0;
    for (i = 0; i < classes_.length; i++) {
      for (j = 0; j < classes_[i].length; j++) {
        String n = classes_[i][j];
        // int k=n.lastIndexOf('.');
        // setText(k>=0 ? n.substring(k+1) : n);
        String t = n;
        int k = t.indexOf('$');
        if (k >= 0) {
          t = t.substring(0, k);
        }
        k = t.lastIndexOf('.');
        if (k >= 0) {
          t = t.substring(0, k);
        }
        k = t.lastIndexOf('.');
        if (k >= 0) {
          t = t.substring(k + 1);
        }
        if (!t.equals(ps)) {
          setText(t);
          ps = t;
        }

        // if(k<0) n=paquet+n;
        try {
          if (loader_ == null) {
            Class.forName(n);
          } else {
            loader_.loadClass(n); // Class.forName(n,false,loader_);
          }
          // Class c=Class.forName(n);
          // FuClassLoaderDedicated.putGlobal(c);
        } catch (Exception ex) {
          /* System.err.println("Error loading class "+n); */
          break;
        }

        Thread.yield();
        /*
         * try { Thread.sleep(5); } catch(Exception ex) { }
         */

        m++;
        p = 100 * m / l;
        if (p != pp) {
          setProgression(p);
          pp = p;
        }
      }
    }

    setText("");
    setProgression(100);
    apres = System.currentTimeMillis();
    if (apres < avant + delai_) {
      delai_ = avant + delai_ - apres;
      avant = apres;
      do {
        try {
          Thread.sleep(50);
        } catch (Exception ex) {
        }

        diff = System.currentTimeMillis() - avant;
        p = (int) (diff * 100 / delai_);
        setProgression(p);
      } while (delai_ > diff);
    }
  }

  public void setText(String _s) {
    if (SwingUtilities.isEventDispatchThread()) {
      load_.setText(_s);
    } else {
      final String s = _s;
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          @Override
          public void run() {
            load_.setText(s);
            // BuUpdateGUI.repaintNow(message_);
          }
        });
      } catch (InterruptedException ex) {
      } catch (InvocationTargetException ex) {
        FuLog.error(ex);
      }
    }

    /*
     * load_.setText(_s); BuUpdateGUI.repaintNow(load_);
     */
  }

  public void setProgression(int _v) {
    if (SwingUtilities.isEventDispatchThread()) {
      progression_.setValue(_v);
    } else {
      final int v = _v;
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          @Override
          public void run() {
            progression_.setValue(v);
            // BuUpdateGUI.repaintNow(progression_);
          }
        });
      } catch (InterruptedException ex) {
      } catch (InvocationTargetException ex) {
        FuLog.error(ex);
      }
    }

    /*
     * progression_.setValue(_v); BuUpdateGUI.repaintNow(progression_);
     */
  }
}
