/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuRegistry.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;
import java.util.Enumeration;
import javax.swing.DefaultListModel;

/**
 * The registry of the running applications.
 */
public class BuRegistry
{
 // private static final boolean TRACE=Bu.TRACE&&true;
  private static final boolean DEBUG=Bu.DEBUG&&true;

  //public static final String REGISTRY_PROPERTY_NAME=
  //"com.memoire.bu.registry";

  private static BuRegistry singleton_;

  public static final BuRegistry getInstance()
  {
    if(singleton_==null) createInstance();
    return singleton_;
  }

  private static final synchronized void createInstance()
  {
    /*
      if(singleton_==null)
      {
      Properties ps=null;

      try
      {
	ps=System.getProperties();
	singleton_=(BuRegistry)ps.get(REGISTRY_PROPERTY_NAME);
      }
      catch(Throwable th) { }

      singleton_=new BuRegistry();
      //if(ps!=null) ps.put(REGISTRY_PROPERTY_NAME,singleton_);
      }
    */

    if(singleton_==null) singleton_=new BuRegistry();
  }

  public static final synchronized void setInstance(BuRegistry _r)
  {
    if(DEBUG) FuLog.debug("BRG: set new registry "+_r);

    if(singleton_!=null)
    {
      if(DEBUG) FuLog.debug("BRG: old registry was "+_r);
      Enumeration e=BuRegistry.getModel().elements();
      while(e.hasMoreElements()) BuRegistry.register(e.nextElement());
    }

    singleton_=_r;

    /*
    Properties ps=null;

    try
    {
      ps=System.getProperties();
    }
    catch(Throwable th) { }

    if(ps!=null) ps.put(REGISTRY_PROPERTY_NAME,singleton_);
    */
  }

  private DefaultListModel model_;

  protected BuRegistry()
  {
    model_=new DefaultListModel();
  }

  protected void register0(Object _t)
  {
    if(DEBUG)
      FuLog.debug("BRG: register "+_t.getClass());//+" with "+this);

    if(model_.contains(_t))
    {
      FuLog.error("BRG: already in the registry: "+_t.getClass());
      return;
    }

    model_.addElement(_t);
  }

  protected void unregister0(Object _t)
  {
    //dump();

    if(DEBUG)
      FuLog.debug("BRG: unregister "+_t.getClass());//+" with "+this);

    if(!model_.contains(_t))
    {
      FuLog.error("BRG: not in the registry: "+_t.getClass());
      return;
    }

    if((_t instanceof Component)&&((Component)_t).isShowing())
      ((Component)_t).setVisible(false);
    if(_t instanceof Window){//&&((Window)_t).isDisplayable())
      ((Window)_t).dispose();
    }

    model_.removeElement(_t);

    //Enumeration e=model_.elements();
    //while(e.hasMoreElements())
    //  FuLog.debug("BRG: "+e.nextElement());

    if(model_.size()==0)
    {
      /*
      try
      {
	System.getProperties().remove(REGISTRY_PROPERTY_NAME);
      }
      catch(SecurityException ex) { }
      */

      singleton_=null;

      // TMP ? 
      //BuLib.setFullScreen(null);

      //System.runFinalizersOnExit(true);
      //System.runFinalization();
      System.exit(0);
    }
  }

  /**
   * Register an application.
   */
  public static void register(Object _t)
  {
    getInstance().register0(_t);
  }

  /**
   * Unregister an application.
   * If there is no more running applis, exit from the JVM.
   */
  public static void unregister(Object _t)
  {
    getInstance().unregister0(_t);
  }

  public static DefaultListModel getModel()
  {
    return getInstance().model_;
  }

  public final void dump()
  {
    Enumeration e=getModel().elements();
    int         n=0;
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      String k="BRG: "+(n++)+") "+o.getClass().getName();
      if(o instanceof Frame) k+=" "+((Frame)o).getTitle();
      if(k.length()>75) k=k.substring(0,75);
      //while(k.length()<36) k=k+" ";
      FuLog.debug(k);
    }
  }

  /*
  public String[] list()
  {
    Vector      v=new Vector(size());
    Enumeration e=elements();
    while(e.hasMoreElements())
      v.addElement(e.nextElement());

    String[] r=new String[v.size()];
    for(int i=0;i<r.length;i++)
      r[i]=(String)v.elementAt(i);
    return r;
  }
  */
}
