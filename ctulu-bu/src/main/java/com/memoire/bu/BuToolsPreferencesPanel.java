/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuToolsPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuPreferences;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.table.TableColumn;

/**
 * A panel where the user can choose his favorite tools.
 *
 * tool.#.name
 * tool.#.exec
 * tool.#.icon
 */
public class BuToolsPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  protected BuCommonInterface appli_;
  protected FuPreferences     options_;
  protected BuTable           table_;
  protected BuToolsModel      model_;
  protected BuButton          btInsert_,btRemove_,btUp_,btDown_;

  @Override
  public String getTitle()
  {
    return getS("Outils");
  }

  @Override
  public String getCategory()
  {
    return getS("Composants");
  }

  // Constructeur

  public BuToolsPreferencesPanel
    (BuCommonInterface _appli, FuPreferences _options)
  {
    super();
    appli_  =_appli;
    options_=_options;

    BuPanel p=new BuPanel();
    p.setBorder
      (new CompoundBorder(new BuTitledBorder(getTitle()),EMPTY5555));
    p.setLayout(new BuBorderLayout(5,5));

    model_=new BuToolsModel(_options);
    table_=new BuTable();
    table_.setModel(model_);
    table_.setRowHeight(BuResource.BU.getDefaultMenuSize()+4);
    table_.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN); // OFF
    table_.setSelectionMode (ListSelectionModel.SINGLE_SELECTION);

    int[]     tailles  =new int[]     { 26,100,184 };
    boolean[] variables=new boolean[] { false,true,true };

    for(int i=0;i<table_.getColumnCount();i++)
    {
      TableColumn c=table_.getColumn(table_.getColumnName(i));
      c.setPreferredWidth(tailles[i]);
      if(!variables[i])
      {
	c.setResizable(false);
	c.setMinWidth (tailles[i]);
	c.setMaxWidth (tailles[i]);
      }
    }

    p.add(new BuScrollPane(table_),BuBorderLayout.CENTER);

    BuPanel q=new BuPanel();
    //q.setLayout(new BuGridLayout(4,5,5,true,true,false,false));
    q.setLayout(new GridLayout(1,4,5,5));//,true,true,false,false));

    btInsert_=new BuButton(BuResource.BU.loadButtonCommandIcon("AJOUTER"),
			   getS("Ins�rer"));
    btInsert_.setActionCommand("AJOUTER");
    btInsert_.setMargin(BuInsets.INSETS1111);
    btInsert_.setFont(BuLib.deriveFont("Button",Font.PLAIN,0));
    btInsert_.addActionListener(this);
    q.add(btInsert_);

    btRemove_=new BuButton(BuResource.BU.loadButtonCommandIcon("ENLEVER"),
			   getS("Enlever"));
    btRemove_.setActionCommand("ENLEVER");
    btRemove_.setMargin(BuInsets.INSETS1111);
    btRemove_.setFont(BuLib.deriveFont("Button",Font.PLAIN,0));
    btRemove_.addActionListener(this);
    q.add(btRemove_);

    btUp_    =new BuButton(BuResource.BU.loadButtonCommandIcon("RECULER"),
			   getS("Reculer"));
    btUp_.setActionCommand("RECULER");
    btUp_.setMargin(BuInsets.INSETS1111);
    btUp_.setFont(BuLib.deriveFont("Button",Font.PLAIN,0));
    btUp_.addActionListener(this);
    q.add(btUp_);

    btDown_    =new BuButton(BuResource.BU.loadButtonCommandIcon("AVANCER"),
			     getS("Avancer"));
    btDown_.setActionCommand("AVANCER");
    btDown_.setMargin(BuInsets.INSETS1111);
    btDown_.setFont(BuLib.deriveFont("Button",Font.PLAIN,0));
    btDown_.addActionListener(this);
    q.add(btDown_);

    BuLib.computeMnemonics(q);
    p.add(q,BuBorderLayout.SOUTH);

    setLayout(new BuBorderLayout());
    setBorder(EMPTY5555);
    add(p,BuBorderLayout.CENTER);

    //Dimension qs=q.getPreferredSize();
    setPreferredSize(new Dimension(100,100));
    //(Math.max(200,qs.width),Math.max(200,100+qs.height)));

    //updateComponents();
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    //JComponent source=(JComponent)_evt.getSource();
    String     action=_evt.getActionCommand();
    // System.err.println("SOURCE="+source.getName());
    // System.err.println("ACTION="+action);

    int index=1+table_.getSelectedRow();

    if("AJOUTER".equals(action))
	insertTool(index);
    else
    if("ENLEVER".equals(action))
	removeTool(index);
    else
    if("AVANCER".equals(action))
	downTool(index);
    else
    if("RECULER".equals(action))
	upTool(index);
  }
  
  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
    { return true; }
  
  @Override
  public void applyPreferences()
  {
    fillTable();
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees
  
  private void fillTable()
  {
  }

  private void updateComponents()
  {
  }

  // Manipulation

  protected int nbTools()
  {
    int i=1;
    while(true)
    {
      //String name=options_.getStringProperty("tool."+i+".name");
      String exec=options_.getStringProperty("tool."+i+".exec");
      //String icon=options_.getStringProperty("tool."+i+".icon");

      if("".equals(exec)) break;
      i++;
    }

    return i;
  }

  protected void setSelection(int _row)
  {
    table_.removeEditor();
    table_.clearSelection();
    table_.setColumnSelectionInterval(0,table_.getColumnCount()-1);
    table_.setRowSelectionInterval(_row-1,_row-1);
  }

  public void insertTool(int _index)
  {
    if((options_==null)||(_index<1)||(_index>=nbTools())) return;

    int i=nbTools()-1;
    for(int j=i;j>=_index;j--)
    {
      options_.putStringProperty
	  ("tool."+(j+1)+".name",
	   options_.getStringProperty("tool."+j+".name"));
      options_.putStringProperty
	  ("tool."+(j+1)+".exec",
	   options_.getStringProperty("tool."+j+".exec"));
      options_.putStringProperty
	  ("tool."+(j+1)+".icon",
	   options_.getStringProperty("tool."+j+".icon"));
    }

    i=_index;
    options_.putStringProperty("tool."+i+".name","");
    options_.putStringProperty("tool."+i+".exec","-");
    options_.putStringProperty("tool."+i+".icon","executer");
    setSelection(i);
  }

  public void removeTool(int _index)
  {
    if((options_==null)||(_index<1)||(_index>=nbTools())) return;

    int i=nbTools()-1;
    for(int j=_index;j<i;j++)
    {
      options_.putStringProperty
	  ("tool."+j+".name",
	   options_.getStringProperty("tool."+(j+1)+".name"));
      options_.putStringProperty
	  ("tool."+j+".exec",
	   options_.getStringProperty("tool."+(j+1)+".exec"));
      options_.putStringProperty
	  ("tool."+j+".icon",
	   options_.getStringProperty("tool."+(j+1)+".icon"));
    }

    options_.removeProperty("tool."+i+".name");
    options_.removeProperty("tool."+i+".exec");
    options_.removeProperty("tool."+i+".icon");
    setSelection(_index);
  }

  public void upTool(int _index)
  {
    if((options_==null)||(_index<2)||(_index>=nbTools())) return;

    int i=_index;
    int j=_index-1;

    String namei=options_.getStringProperty("tool."+i+".name");
    String execi=options_.getStringProperty("tool."+i+".exec");
    String iconi=options_.getStringProperty("tool."+i+".icon");
    String namej=options_.getStringProperty("tool."+j+".name");
    String execj=options_.getStringProperty("tool."+j+".exec");
    String iconj=options_.getStringProperty("tool."+j+".icon");

    options_.putStringProperty("tool."+i+".name",namej);
    options_.putStringProperty("tool."+i+".exec",execj);
    options_.putStringProperty("tool."+i+".icon",iconj);
    options_.putStringProperty("tool."+j+".name",namei);
    options_.putStringProperty("tool."+j+".exec",execi);
    options_.putStringProperty("tool."+j+".icon",iconi);
    setSelection(j);
  }

  public void downTool(int _index)
  {
    if((options_==null)||(_index<1)||(_index+1>=nbTools())) return;

    int i=_index;
    int j=_index+1;

    String namei=options_.getStringProperty("tool."+i+".name");
    String execi=options_.getStringProperty("tool."+i+".exec");
    String iconi=options_.getStringProperty("tool."+i+".icon");
    String namej=options_.getStringProperty("tool."+j+".name");
    String execj=options_.getStringProperty("tool."+j+".exec");
    String iconj=options_.getStringProperty("tool."+j+".icon");

    options_.putStringProperty("tool."+i+".name",namej);
    options_.putStringProperty("tool."+i+".exec",execj);
    options_.putStringProperty("tool."+i+".icon",iconj);
    options_.putStringProperty("tool."+j+".name",namei);
    options_.putStringProperty("tool."+j+".exec",execi);
    options_.putStringProperty("tool."+j+".icon",iconi);
    setSelection(j);
  }

  public void addTool(String _name, String _exec, String _icon)
  {
    if(options_==null) return;

    int i=nbTools();

    options_.putStringProperty("tool."+i+".name",_name);
    options_.putStringProperty("tool."+i+".exec",_exec);
    options_.putStringProperty("tool."+i+".icon",_icon);
  }
}
