/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuCheckBoxMenuItem.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;

/**
 * Like JCheckBoxMenuItem but with better management of icons.
 */

public class BuCheckBoxMenuItem
       extends JCheckBoxMenuItem
{
  public BuCheckBoxMenuItem()
  {
    this(null,"");
  }

  public BuCheckBoxMenuItem(BuIcon _icon)
  {
    this(_icon,"");
  }

  public BuCheckBoxMenuItem(String _label)
  {
    this(null,_label);
  }

  public BuCheckBoxMenuItem(BuIcon _icon, String _label)
  {
    super();
    if(_icon!=null) setIcon(_icon);
    setText((_label==null)?"":_label);
  }

  /**
   * @deprecated
   */
  public BuCheckBoxMenuItem(Icon _icon)
  {
    this(null,_icon,false);
  }

  /**
   * @deprecated
   */
  public BuCheckBoxMenuItem(Icon _icon, boolean _selected)
  {
    this(null,_icon,_selected);
  }

  /**
   * @deprecated
   */
  public BuCheckBoxMenuItem(String _text,Icon _icon)
  {
    this(_text,_icon,false);
  }

  /**
   * @deprecated
   */
  public BuCheckBoxMenuItem(String _text,boolean _selected)
  {
    this(_text,null,_selected);
  }

  /**
   * @deprecated
   */
  public BuCheckBoxMenuItem(String _text,Icon _icon,boolean _selected)
  {
    super();
    if(_icon instanceof BuIcon) setIcon((BuIcon)_icon);
    else                        setIcon(_icon);
    setText(_text);
    setSelected(_selected);
  }

  // Icon

  @Override
  public Icon getIcon()
  {
    if(BuPreferences.BU.getBooleanProperty("icons.menu",true)||
       (super.getText()==null))
      return super.getIcon();
    return null;
  }

  public void setIcon(BuIcon _icon)
  {
    BuLib.setIcon(this,_icon);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(getWidth(),0);
    return r;
  }
}
