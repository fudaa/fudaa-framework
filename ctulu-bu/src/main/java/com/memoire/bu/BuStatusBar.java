/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuStatusBar.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.DesktopManager;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;

/**
 * A status bar which contains a memory monitor, a text line
 * and a progress bar.
 */
public class BuStatusBar
       extends BuPanel
{
  class ResizeCorner
    extends BuLabel
  {
    @Override
    public boolean isVisible()
    {
      JInternalFrame f=getTop();
      return (f!=null)&&f.isResizable();
    }

    public Dimension getSuperPreferredSize()
    {
      return super.getPreferredSize();
    }

    @Override
    public Dimension getPreferredSize()
    {
      Dimension r;

      if(isVisible())
      {
        r=getSuperPreferredSize();
        Insets i=getInsets();
        r.width =Math.max(1,r.width -i.right);
        r.height=Math.max(1,r.height-i.top-i.bottom-2);
      }
      else
        r=new Dimension(0,0);

      return r;
    }

    @Override
    public Icon getIcon()
    {
      JInternalFrame f=getTop();
      Icon i=null;
      if((f!=null)&&f.isSelected())
        i=UIManager.getIcon("StatusBar.selectedCornerIcon");
      if(i==null) i=super.getIcon();
      return i;
    }
  }

  class ResizeListener
    implements MouseListener, MouseMotionListener
  {
    private JInternalFrame frame_;

    @Override
    public void mouseEntered(MouseEvent _evt)
    {
    }

    @Override
    public void mouseExited(MouseEvent _evt)
    {
    }

    @Override
    public void mousePressed(MouseEvent _evt)
    {
      if(BuLib.isLeft(_evt)&&(frame_==null))
      {
        frame_=getTop();
        if(frame_!=null)
          getManager(frame_).beginResizingFrame
            (frame_,SwingConstants.SOUTH_EAST);
      }
    }

    @Override
    public void mouseReleased(MouseEvent _evt)
    {
      if(frame_!=null)
      {
        getManager(frame_).endResizingFrame(frame_);
        frame_=null;
      }
    }

    @Override
    public void mouseClicked(MouseEvent _evt)
    {
      if(BuLib.isMiddle(_evt)&&BuLib.isSingleClick(_evt))
      {
        JInternalFrame f=getTop();
        if(f!=null) f.pack();
      }
    }

    @Override
    public void mouseMoved(MouseEvent _evt)
    {
    }

    @Override
    public void mouseDragged(MouseEvent _evt)
    {
      if(frame_!=null)
        getManager(frame_).resizeFrame
          (frame_,frame_.getX(),frame_.getY(),
           frame_.getWidth ()+_evt.getX()-resizeCorner_.getWidth(),
           frame_.getHeight()+_evt.getY()-resizeCorner_.getHeight());
    }
  }

  private BuPanel       monitors_;
  BuLabel       message_;
  BuProgressBar progression_;
  ResizeCorner  resizeCorner_;

  public BuStatusBar()
  {
    this(true,true);
  }

  public BuStatusBar(boolean _monitors, boolean _progression)
  {
    super();
    setName("buSTATUSBAR");
    setLayout(new BuBorderLayout(5,0));

    if(_monitors)
    {
      monitors_=new BuPanel();
      monitors_.setBorder(BuBorders.EMPTY0000);
      monitors_.setLayout(new BuHorizontalLayout(2,true,true));

      if(!FuLib.isWharfRunning())
      {
	if(BuPreferences.BU.getBooleanProperty("monitor.memory",true))
	  addMonitor(new BuMonitorMemory());

	if(BuPreferences.BU.getBooleanProperty("monitor.cpu" ,false))
	  addMonitor(new BuMonitorCpu());

	if(BuPreferences.BU.getBooleanProperty("monitor.clock" ,false))
	  addMonitor(new BuMonitorClock());

	if(BuPreferences.BU.getBooleanProperty("monitor.time"  ,false))
	  addMonitor(new BuMonitorTime());
      }

      add(monitors_,BuBorderLayout.WEST);
    }

    message_=new BuLabel(" ");
    message_.setName("lbMESSAGESTATUS");
    message_.setFont(getFont());
    add(message_,BuBorderLayout.CENTER);

    if(!Boolean.FALSE.equals(UIManager.get("StatusBar.corner")))
    {
      resizeCorner_=new ResizeCorner();

      /*
      BuLabel()
      {
	public boolean isVisible()
        {
	  JInternalFrame f=getTop();
	  return (f!=null)&&f.isResizable();
	}

	public Dimension getPreferredSize()
        {
	  Dimension r;

	  if(isVisible())
	  {
	    r=super.getPreferredSize();
	    Insets i=getInsets();
	    r.width =Math.max(1,r.width -i.right);
	    r.height=Math.max(1,r.height-i.top-i.bottom-2);
	  }
	  else
	    r=new Dimension(0,0);

	  return r;
	}

	public Icon getIcon()
        {
	  JInternalFrame f=getTop();
	  Icon i=null;
	  if((f!=null)&&f.isSelected())
	    i=UIManager.getIcon("StatusBar.selectedCornerIcon");
	  if(i==null) i=super.getIcon();
	  return i;
	}
      };
      */

      resizeCorner_.setCursor
	(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));

      Icon i=UIManager.getIcon("StatusBar.cornerIcon");
      if(i==null)
      {
	if(BuLib.isOcean())
	  i=BuResource.BU.getIcon("bu_internalframe_corner_ocean");
	else
	if(BuLib.isMetal())
	  i=BuResource.BU.getIcon("bu_internalframe_corner_metal");
	else
	  i=BuResource.BU.getIcon("bu_internalframe_corner");
      }
      resizeCorner_.setIcon(i);

      final ResizeListener RL=new ResizeListener();
      resizeCorner_.addMouseListener(RL);
      resizeCorner_.addMouseMotionListener(RL);

      /*
      resizeCorner_.addMouseListener(new MouseAdapter()
      {
        JInternalFrame frame_;

	public void mousePressed(MouseEvent _evt)
	{
          if(BuLib.isLeft(_evt)&&(frame_==null))
          {
            frame_=getTop();
            if(frame_!=null)
              getManager(frame_).beginResizingFrame
                (frame_,SwingConstants.SOUTH_EAST);
          }
	}

	public void mouseReleased(MouseEvent _evt)
	{
          if(frame_!=null)
          {
            getManager(frame_).endResizingFrame(frame_);
            frame_=null;
          }
	}
      });

      resizeCorner_.addMouseMotionListener(new MouseMotionAdapter()
      {
	public void mouseDragged(MouseEvent _evt)
	{
	  JInternalFrame f=getTop();
	  if(f!=null)
	    getManager(f).resizeFrame
	      (f,f.getX(),f.getY(),
	       f.getWidth ()+_evt.getX()-resizeCorner_.getWidth(),
	       f.getHeight()+_evt.getY()-resizeCorner_.getHeight());
	}
      });
      */

      add(resizeCorner_,BuBorderLayout.EAST);
    }

    if(_progression)
    {
      progression_=new BuProgressBar();
      progression_.setMinimum(0);
      progression_.setMaximum(100);
      progression_.setValue(0);
      progression_.setName("pgTASKPROGRESS");
      progression_.setPreferredSize(new Dimension(120,14));

      add(progression_,BuBorderLayout.EAST);
    }
  }

  @Override
  public void layout()
  {
    super.layout();
    if((resizeCorner_!=null)&&resizeCorner_.isVisible())
    {
      Dimension d=resizeCorner_.getSuperPreferredSize();
      int w=d.width;
      int h=d.height;
      //if((w!=0)||(h!=0))
      //{
      //Insets i=getInsets();
      //w+=i.right;
      //h+=i.top+i.bottom+2;
      int x=getWidth ()-w;//+i.right;
      int y=getHeight()-h;//+Math.max(1,i.bottom);
      resizeCorner_.reshape(x,y,w,h);
      //}

      /*
      if(dx<0)
      {
        if(progression_!=null)
          progression_.reshape
            (progression_.getX()+dx,progression_.getY(),
             progression_.getWidth(),progression_.getHeight());
          message_.reshape
            (message_.getX(),message_.getY(),
             Math.max(0,message_.getWidth()+dx),message_.getHeight());
        }
      }
      */
    }
  }

  @Override
  public Border getBorder()
  {
    Border r=super.getBorder();

    if((resizeCorner_!=null)&&
       resizeCorner_.isVisible()&&
       Boolean.TRUE.equals(UIManager.get("StatusBar.pad")))
    {
      Dimension d=resizeCorner_.getSuperPreferredSize();
      int       w=d.width;
      Border    b=new CompoundBorder(new EmptyBorder(0,0,0,w),r);

      if(r instanceof BorderUIResource)
        r=new BorderUIResource(b);
      else
        r=b;
    }

    return r;
  }

  JInternalFrame getTop()
  {
    Container p=this;
    while((p!=null)&&!(p instanceof JInternalFrame))
      p=p.getParent();
    return (JInternalFrame)p;
  }

  DesktopManager getManager(JInternalFrame _f)
  {
    DesktopManager r=null;
    JDesktopPane   d=_f.getDesktopPane();
    if(d!=null) r=d.getDesktopManager();
    return r;
  }

  public void addMonitor(JComponent _monitor)
  {
    if(monitors_!=null)
    {
      monitors_.add(_monitor);
      _monitor.revalidate();
    }
  }

  public void removeMonitor(JComponent _monitor)
  {
    if(monitors_!=null)
    {
      monitors_.remove(_monitor);
      _monitor.revalidate();
    }
  }

  public void setApp(BuCommonInterface _app)
  {
    if(monitors_!=null)
    {
      Component[] c=monitors_.getComponents();
      for(int i=0;i<c.length;i++)
	if(c[i] instanceof BuMonitorAbstract)
	  ((BuMonitorAbstract)c[i]).setApp(_app);
    }
  }

  @Override
  public void updateUI()
  {
    super.updateUI();

    Border b=UIManager.getBorder("StatusBar.border");
    if((b==null)&&!(getParent() instanceof BuMainPanel))
    {
      Color bc;
      if(BuLib.isMetal())
	bc=MetalLookAndFeel.getPrimaryControlDarkShadow();
      else
	bc=UIManager.getColor("Label.foreground");

      b=new BorderUIResource
	(new CompoundBorder
	 (new MatteBorder(1,0,0,0,bc),
	  new EmptyBorder(1,1,0,1)));
    }
    setBorder(b);
    Color bg=UIManager.getColor("StatusBar.background");
    if(bg!=null) setBackground(bg);
    Color fg=UIManager.getColor("StatusBar.foreground");
    if(fg!=null) setForeground(fg);
    Font ft=UIManager.getFont("StatusBar.font");
    if(ft==null) ft=UIManager.getFont("Label.font");
    if(ft!=null) setFont(ft);
  }

  @Override
  protected void addImpl(Component _c, Object _constraints, int _index)
  {
    Object constraints=_constraints;
    if(constraints==null) constraints=BuBorderLayout.WEST;
    super.addImpl(_c,constraints,_index);
  }

  @Override
  public void setFont(Font _font)
  {
    super.setFont(_font);
    if(message_!=null) message_.setFont(_font);
  }

  public String getMessage()
  {
    return message_.getText();
  }

  public void setMessage(String _s)
  {
    String sInit=_s;
    if(sInit==null) sInit="";

    if(!sInit.equals(message_.getText()))
    {
      if(SwingUtilities.isEventDispatchThread())
      {
	message_.setText(sInit);
      }
      else
      {
        final String s=sInit;
        SwingUtilities.invokeLater(new Runnable() //AndWait
	{
          @Override
          public void run()
          {
            message_.setText(s);
            //BuUpdateGUI.repaintNow(message_);
          }
	});
      }
    }
  }

  public int getProgression()
  {
    return progression_.getValue();
  }

  public void setProgression(int _v)
  {
    int vInit=_v;
    if(progression_!=null)
    {
      if(vInit<  0) vInit=0;
      if(vInit>100) vInit=100;

      if(progression_.getValue()!=vInit)
      {
	if(SwingUtilities.isEventDispatchThread())
	{
	  progression_.setValue(vInit);
	}
	else
	{
	  final int v=vInit;
          SwingUtilities.invokeLater(new Runnable() //AndWait
	  {
            @Override
            public void run()
            {
              progression_.setValue(v);
              //BuUpdateGUI.repaintNow(progression_);
            }
	  });
	}
      }
    }
  }
}
