/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuBrowserFrame.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

// import com.memoire.dnd.*;
import com.memoire.dt.DtDragSupport;
import com.memoire.dt.DtFileFieldHandler;
import com.memoire.dt.DtFileHandler;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * A tiny HTML browser. Is an internal frame on the desktop and an alternative to an external browser as lynx ;-)
 */
public class BuBrowserFrame extends BuInternalFrame implements ActionListener, BuCutCopyPasteInterface {
  private static final int GAP = BuPreferences.BU.getIntegerProperty("layout.gap", 5);

  protected BuCommonImplementation app_;
  protected BuTextField tfURL_;
  protected BuStatusBar sbStatus_;
  protected BuBrowserPane pane_;
  protected BuPanel container_;
  protected BuPanel urlBar_;

  protected BuLabel lbURL_;
  protected BuButton btAvancer_;
  protected BuButton btReculer_;
  protected BuButton btRafraichir_;
  protected BuButton btMaison_;

  public BuBrowserFrame() {
    this(null);
  }

  public BuBrowserFrame(BuCommonImplementation _app) {
    this(_app, null);
  }

  public BuBrowserFrame(BuCommonImplementation _app, String _source) {
    super("", true, true, true, true);
    app_ = _app;

    setName("ifNAVIGATEUR");
    if (app_ != null) app_.installContextHelp(getRootPane(), "bu/p-navigateur.html");

    // for dnd
    lbURL_ = new BuLabel(BuResource.BU.getMenuIcon("signet"));
    lbURL_.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    lbURL_.setTransferHandler(new DtFileHandler() {
      @Override
      protected Object getData(JComponent _c) {
        return tfURL_.getText();
      }
    });
    DtDragSupport.install(lbURL_);

    /*
     * lbURL_.setTransferHandler(new TransferHandler() { protected Transferable createTransferable(JComponent _c) {
     * String s=FuLib.expandedPath(tfURL_.getText()); if("".equals(s)) s=null; URL u=null; if(s!=null) { try { u=new
     * URL(s); } catch(MalformedURLException ex) { } if(u==null) { VfsFile f=VfsFile.createFile(s); if(f.exists()) { try {
     * u=f.toURL(); } catch(MalformedURLException ex) { } } } } return u==null ? null : new DtFilesSelection(u); }
     * public int getSourceActions(JComponent _c) { return DtLib.ALL; } });
     */

    /*
     * new DndSource(new DndRequestData() { public Object[] request(JComponent _component,Point _location) { String
     * s=tfURL_.getText(); URL u=null; try { u=new URL(s); } catch(MalformedURLException ex) { } return new Object[] {
     * u, s }; } }).add(lbURL_);
     */

    tfURL_ = new BuTextField("");
    // tfURL_.setActionCommand("ALLER");
    // tfURL_.addActionListener(this);
    tfURL_.setTransferHandler(new DtFileFieldHandler(true, DtFileFieldHandler.URL, false));

    /*
     * new DndTarget("URL") .accept(String.class, DndTransferString.SINGLETON, DndManageText.SINGLETON_REPLACE_ALL)
     * .add(tfURL_);
     */

    BuButton btAller = new BuButton();
    btAller.setActionCommand("ALLER");
    btAller.setIcon(BuResource.BU.loadMenuCommandIcon("ALLER"));
    btAller.setMargin(BuInsets.INSETS0000);
    btAller.addActionListener(this);

    BuButton btEffacer = new BuButton();
    btEffacer.setActionCommand("EFFACER");
    btEffacer.setIcon(BuResource.BU.loadMenuCommandIcon("EFFACER"));
    btEffacer.setMargin(BuInsets.INSETS0000);
    btEffacer.addActionListener(this);

    /*
     * BuPanel p0=new BuPanel(new GridLayout(1,1,5,5)); p0.setBorder(BuBorders.EMPTY0000); p0.add(lbURL_); BuGridLayout
     * l1=new BuGridLayout(1,5,5,true,false,true,false); l1.setYalign(0.5f); BuPanel p1=new BuPanel(l1);
     * p1.setBorder(BuBorders.EMPTY0000); p1.add(tfURL_); BuPanel p2=new BuPanel(new GridLayout(1,2,5,5));
     * p2.setBorder(BuBorders.EMPTY0000); p2.add(btAller); p2.add(btEffacer); BuPanel p3=new BuPanel(new
     * BuBorderLayout(5,5)); p3.setBorder(new EmptyBorder(2,5,2,5)); p3.add(p0,BuBorderLayout.WEST);
     * p3.add(p1,BuBorderLayout.CENTER); p3.add(p2,BuBorderLayout.EAST);
     */

    urlBar_ = new BuPanel(new BuBorderLayout(GAP, GAP, true, false));
    // urlBar_.setBorder(new EmptyBorder(2,5,2,5));
    // urlBar_.setBorder(BuLib.getEmptyBorder(GAP));//BuBorders.EMPTY5555);
    urlBar_.add(lbURL_, BuBorderLayout.WEST);
    urlBar_.add(tfURL_, BuBorderLayout.CENTER);
    urlBar_.add(btEffacer, BuBorderLayout.EAST);
    urlBar_.add(btAller, BuBorderLayout.EAST);

    pane_ = createBrowserPane(this);

    container_ = new BuPanel(new BuBorderLayout(GAP, GAP));// 0,0));
    container_.setBorder(BuLib.getEmptyBorder(GAP));// BuBorders.EMPTY0000);
    container_.add(urlBar_, BuBorderLayout.NORTH);
    container_.add(pane_, BuBorderLayout.CENTER);

    sbStatus_ = new BuStatusBar(false, false);
    sbStatus_.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
    // sbStatus_.setBorder(BuBorders.EMPTY2222);
    sbStatus_.setMessage(" ");

    JComponent content = (JComponent) getContentPane();
    content.setLayout(new BuBorderLayout(0, 0));
    content.add(container_, BuBorderLayout.CENTER);
    content.add(sbStatus_, BuBorderLayout.SOUTH);

    btAvancer_ = new BuButton();
    btAvancer_.setActionCommand("AVANCER");
    btAvancer_.setToolTipText(getString("Avancer � la page suivante"));
    btAvancer_.setIcon(BuResource.BU.loadToolCommandIcon("AVANCER"));
    btAvancer_.setName("btAVANCER");
    btAvancer_.setMargin(BuInsets.INSETS1111);
    btAvancer_.setRequestFocusEnabled(false);
    btAvancer_.addActionListener(this);

    btReculer_ = new BuButton();
    btReculer_.setActionCommand("RECULER");
    btReculer_.setToolTipText(getString("Revenir � la page pr�c�dente"));
    btReculer_.setIcon(BuResource.BU.loadToolCommandIcon("RECULER"));
    btReculer_.setName("btRECULER");
    btReculer_.setMargin(BuInsets.INSETS1111);
    btReculer_.setRequestFocusEnabled(false);
    btReculer_.addActionListener(this);

    btRafraichir_ = new BuButton();
    btRafraichir_.setActionCommand("RAFRAICHIR");
    btRafraichir_.setToolTipText(getString("Rafra�chir la page"));
    btRafraichir_.setIcon(BuResource.BU.loadToolCommandIcon("RAFRAICHIR"));
    btRafraichir_.setName("btRAFRAICHIR");
    btRafraichir_.setMargin(BuInsets.INSETS1111);
    btRafraichir_.setRequestFocusEnabled(false);
    btRafraichir_.addActionListener(this);

    btMaison_ = new BuButton();
    btMaison_.setActionCommand("MAISON");
    btMaison_.setToolTipText(getString("Aller � la page principale"));
    btMaison_.setIcon(BuResource.BU.getToolIcon("www"));
    btMaison_.setName("btMAISON");
    btMaison_.setMargin(BuInsets.INSETS1111);
    btMaison_.setRequestFocusEnabled(false);
    btMaison_.addActionListener(this);

    btAvancer_.setEnabled(false);
    btReculer_.setEnabled(false);
    // if(app_==null) btMaison_.setEnabled(false);

    /*
     * JRootPane rootpane=getRootPane(); KeyStroke ksAvancer=KeyStroke.getKeyStroke
     * (KeyEvent.VK_RIGHT,KeyEvent.CTRL_MASK|KeyEvent.ALT_MASK,false); KeyStroke ksReculer=KeyStroke.getKeyStroke
     * (KeyEvent.VK_LEFT ,KeyEvent.CTRL_MASK|KeyEvent.ALT_MASK,false); ActionListener alAvancer=new ActionListener() {
     * public void actionPerformed(ActionEvent _evt) { BuBrowserFrame.this.actionPerformed (new
     * ActionEvent(BuBrowserFrame.this, ActionEvent.ACTION_PERFORMED,"AVANCER")); } }; ActionListener alReculer=new
     * ActionListener() { public void actionPerformed(ActionEvent _evt) { BuBrowserFrame.this.actionPerformed (new
     * ActionEvent(BuBrowserFrame.this, ActionEvent.ACTION_PERFORMED,"RECULER")); } };
     * rootpane.unregisterKeyboardAction(ksAvancer); rootpane.unregisterKeyboardAction(ksReculer);
     * rootpane.registerKeyboardAction (alAvancer,ksAvancer,JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
     * rootpane.registerKeyboardAction (alReculer,ksReculer,JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
     */

    if (_source != null) setHtmlSource(_source);

    setTitle("");
    setFrameIcon(BuResource.BU.getFrameIcon("www"));

    Dimension ps = new Dimension(400, 500);
    setSize(ps);
    setPreferredSize(ps);
  }

  protected String getTitleBase() {
    return getString("Navigateur");
  }

  // Creation

  protected BuBrowserPane createBrowserPane(BuBrowserFrame _frame) {
    return new BuBrowserPane(_frame);
  }

  // State

  public void setMessage(String _s) {
    sbStatus_.setMessage(_s);
  }

  public void setUrlText(String _s, boolean _update) {
    if (!_update) tfURL_.removeActionListener(this);
    tfURL_.setText(_s);
    if (!_update) tfURL_.addActionListener(this);
  }

  @Override
  public void setTitle(String _s) {
    String s = _s;
    if (s == null) s = "";
    else {
      s = s.trim();
      if (!"".equals(s)) s += " - ";
    }
    super.setTitle(s + getTitleBase());
  }

  public void setBackEnabled(boolean _e) {
    btReculer_.setEnabled(_e);
  }

  public void setForwardEnabled(boolean _e) {
    btAvancer_.setEnabled(_e);
  }

  // Action

  public void beep() {
    getToolkit().beep();
  }

  @Override
  public void setSelected(boolean _state) throws PropertyVetoException {
    super.setSelected(_state);
    if (isSelected()) pane_.requestFocus();
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    String action = _evt.getActionCommand();
    // FuLog.debug("BuBrowserFrame : "+action);

    if ("ALLER".equals(action)) {
      FuLog.debug("BBR: ALLER " + tfURL_.getText());
      setDocumentUrl(tfURL_.getText());
    } else if ("EFFACER".equals(action)) {
      setUrlText("", false);
    } else if ("RECULER".equals(action)) {
      pane_.back();
    } else if ("AVANCER".equals(action)) {
      pane_.forward();
    } else if ("RAFRAICHIR".equals(action)) {
      pane_.reload();
    } else if ("MAISON".equals(action)) {
      if (app_ != null) setDocumentUrl(app_.getInformationsSoftware().baseManUrl());
      else
        setHtmlSource(INTRO, "Introduction");
    }
  }

  // Document

  public final void setDocumentUrl(URL _url) {
    FuLog.debug("BBR: setDocumentUrl " + _url);
    if (_url != null) setUrlText(_url.toExternalForm(), false);
    pane_.setContent(_url, null, null);
  }

  public void setDocumentUrl(String _s) {
    if (_s == null) return;

    URL u;
    try {
      u = new URL(_s);
      setDocumentUrl(u);
    } catch (MalformedURLException ex1) {
      FuLog.warning(ex1);
      try {
        File f = new File(_s);
        if (f.isAbsolute() && f.exists()) {
          u = FuLib.toURL(f);
          setDocumentUrl(u);
        } else {
          u = new URL("http://" + _s);
          setDocumentUrl(u);
        }
      } catch (MalformedURLException ex2) {
        FuLog.warning(ex2);
        /*
         * setHtmlSource ("<HTML><BODY><P>This URL can not be displayed:</P><SMALL><TT>"+ _s+" </TT></SMALL><P>If
         * it is correct, please use an external"+ " browser.</P></BODY></HTML>","Error");
         */
        // FuLog.debug("BBR: Malformed URL: " + ex);
        pane_.setError(null, null);
      }
    }
  }

  public final String getHtmlSource() {
    return pane_.getHtmlSource();
  }

  public final void setHtmlSource(String _source) {
    setHtmlSource(_source, null);
  }

  public final void setHtmlSource(String _source, String _titre) {
    // if((_titre==null)||("".equals(_titre))) _titre=" ";
    // pane_.setHtmlSource(_source,_titre);
    pane_.setContent(null, _source, _titre);
  }

  /**
   * @deprecated don't use this getter
   */
  /*
   * public JEditorPane getEditorPane() { return pane_.getEditorPane(); }
   */

  // BuInternalFrame
  @Override
  public String[] getEnabledActions() {
    String[] r = new String[] { "COPIER" };
    return r;
  }

  @Override
  public JComponent[] getSpecificTools() {
    JComponent[] r = new JComponent[4];
    r[0] = btReculer_;
    r[1] = btAvancer_;
    r[2] = btRafraichir_;
    r[3] = btMaison_;
    return r;
  }

  // BuCutCopyPasteInterface

  @Override
  public void cut() {}

  @Override
  public void copy() {
    pane_.copy();
  }

  @Override
  public void paste() {}

  @Override
  public void duplicate() {}

  // Test

  protected static final String INTRO = "<HTML><BODY><H1> Bienvenue </H1>"
      + "<P> Liens: <UL>"
      +
      // "<LI><A HREF=\"http://localhost/\" >Local host</A></LI>"+
      "<LI><A HREF=\"http://www.jdistro.com/\"               >JDistro</A></LI>"
      + "<LI><A HREF=\"http://www.connectandwork.com/meerkat/\">C&W Meerkat</A></LI>"
      + "<LI><A HREF=\"http://www.desnoix.com/guillaume/alma/\">Alma</A></LI>"
      + "<LI><A HREF=\"http://www.desnoix.com/guillaume/slaf/\">Slaf</A></LI>"
      +
      // "<LI><A HREF=\"http://www.fudaa.org/\" >Fudaa</A></LI>"+
      "<LI><A HREF=\"http://www.java-channel.org/\"          >Canal Java</A></LI>"
      + "<LI><A HREF=\"http://www.debian.org/\"                >Debian</A></LI>" +
      // "<LI><A HREF=\"file:///home/desnoix/conges.gif\">Test GIF</A></LI>"+
      // "<LI><A HREF=\"file:///home/desnoix/DELL.txt\">Test TXT</A></LI>"+
      "</UL></P></BODY></HTML>";

  public static void main(String[] _args) {
    final JFrame f = new JFrame();
    final BuBrowserFrame b = new BuBrowserFrame();
    final JComponent[] c = b.getSpecificTools();
    final JComponent x = (JComponent) b.getContentPane();

    x.setPreferredSize(new Dimension(640, 350));

    BuToolBar t = new BuToolBar();
    for (int i = 0; i < c.length; i++)
      t.add(c[i]);

    BuPanel p = new BuPanel(new BorderLayout());
    p.add("Center", x);
    p.add("North", t);

    f.setContentPane(p);
    f.setTitle(b.getString("Navigateur"));
    f.pack();
    f.setVisible(true);
    f.setLocation(200, 100);
    BuRegistry.register(f);

    f.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent _evt) {
        f.setVisible(false);
        f.dispose();
        BuRegistry.unregister(f);
      }
    });

    if (_args.length > 0) {
      URL url = FuLib.createURL(_args[0]);
      if (url != null) b.setDocumentUrl(url);
      else
        b.setDocumentUrl(_args[0]);
    } else {
      b.setHtmlSource(INTRO, "Introduction");
    }
  }
}
