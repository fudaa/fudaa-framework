/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuSplit3Pane.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.BorderUIResource;

/**
 * A split panel into 3 (resizable by the user).
 * Because the JSplitPane does not work fine.
 */
public class BuSplit3Pane
  extends BuPanel
  implements LayoutManager2, MouseListener, MouseMotionListener
{
  private class Divider extends JComponent
  {
    int cursor_;
    public  int p_;
    public  int q_;

    Divider()
    {
      cursor_=Cursor.MOVE_CURSOR;
      this.setCursor(Cursor.getPredefinedCursor(cursor_));
      this.p_=0;
      configure();
    }

    @Override
    public void updateUI()
    {
      super.updateUI();
      configure();
    }

    private void configure()
    {
      Color bg=UIManager.getColor("Divider.background");
      if(bg==null) bg=UIManager.getColor("Panel.background");
      if(bg!=null) this.setBackground(bg);
      Color fg=UIManager.getColor("Divider.foreground");
      if(fg==null) fg=UIManager.getColor("Panel.foreground");
      if(fg!=null) this.setForeground(fg);
      Border bd=UIManager.getBorder("Divider.border");
      /*if(bd!=null)*/ this.setBorder(bd);

      Object op=UIManager.get("Divider.opaque");
      //System.err.println("OPAQUE="+op);
      if(op instanceof Boolean)
	this.setOpaque(((Boolean)op).booleanValue());
      else
	this.setOpaque(true);

      Object wi=UIManager.get("Divider.width");
      if(wi instanceof Integer)
      {
	int wd=((Integer)wi).intValue();
	this.setPreferredSize(new Dimension(wd,wd));
      }
      else
	this.setPreferredSize(new Dimension(9,9));
    }

    @Override
    protected void paintComponent(Graphics _g)
    {
      int w=this.getWidth();
      int h=this.getHeight();
      int x=w/2-2;
      int y=h/2-2;
      Color fg=this.getForeground();
      Color bg=this.getBackground();

      boolean horizontal=isHorizontal();

      if(this.isOpaque())
      {
	_g.setColor(bg);
	_g.fillRect(0,0,w,h);
      }

      //_g.setColor(Color.yellow);
      //_g.fillRect(0,0,w,h);

      String p=(String)UIManager.get("Divider.drawing");
      //System.err.println("DRAWING="+p);

      if("single".equals(p))//||(p==null))
      {
        _g.setColor(bg.darker());
	if(horizontal)
	  _g.drawLine(x+1,0,x+1,h-1);
	else
	  _g.drawLine(0,y+1,w-1,y+1);
	int d=w%2;
	if(d==1)
	{
	  _g.setColor(Color.black);//bg.darker().darker());
	  if(horizontal)
	    _g.drawLine(x+2,0,x+2,h-1);
	  else
	    _g.drawLine(0,y+2,w-1,y+2);
	}
	_g.setColor(bg.brighter());
	if(horizontal)
	  _g.drawLine(x+2+d,0,x+2+d,h-1);
	else
	  _g.drawLine(0,y+2+d,w-1,y+2+d);
      }
      else
      if("double".equals(p)||"double_raised_line".equals(p))
      {
	_g.setColor(bg.darker());
	if(horizontal)
	{
	  _g.drawLine(x  ,2,x  ,h-5);
	  _g.drawLine(x+3,2,x+3,h-5);
	}
	else
	{
	  _g.drawLine(2,y  ,w-5,y  );
	  _g.drawLine(2,y+3,w-5,y+3);
	}
	_g.setColor(bg.brighter());
	if(horizontal)
	{
	  _g.drawLine(x+1,2,x+1,h-5);
	  _g.drawLine(x+4,2,x+4,h-5);
	}
	else
	{
	  _g.drawLine(2,y+1,w-5,y+1);
	  _g.drawLine(2,y+4,w-5,y+4);
	}
      }
      else
      if("square".equals(p))
      {
	_g.setColor(fg);
	_g.drawRect(x-1,y-1,6,6);
      }
      else
      if("filled_square".equals(p))
      {
	_g.setColor(fg);
	_g.fillRect(x-1,y-1,5,5);
      }
      else
      if("lowered_square".equals(p))
      {
	_g.setColor(bg);
	_g.draw3DRect(x-1,y-1,6,6,false);
      }
      else
      if("raised_square".equals(p))
      {
	_g.setColor(bg);
	_g.draw3DRect(x-1,y-1,6,6,true);
      }
      else
      if("line".equals(p))
      {
	_g.setColor(fg);
	if(horizontal) _g.drawLine(x+2,2,x+2,h-5);
	else           _g.drawLine(2,y+2,w-5,y+2);
      }
      else
      if("thick_line".equals(p))
      {
	_g.setColor(fg);
	if(horizontal)
	{
	  _g.drawLine(x+1,2,x+1,h-5);
	  _g.drawLine(x+2,2,x+2,h-5);
	}
	else
	{
	  _g.drawLine(2,y+1,w-5,y+1);
	  _g.drawLine(2,y+2,w-5,y+2);
	}
      }
      else
      if("double_line".equals(p))
      {
	_g.setColor(fg);
	if(horizontal)
	{
	  _g.drawLine(x  ,2,x  ,h-5);
	  _g.drawLine(x+3,2,x+3,h-5);
	}
	else
	{
	  _g.drawLine(2,y  ,w-5,y  );
	  _g.drawLine(2,y+3,w-5,y+3);
	}
      }
      else
      if("".equals(p)||"nothing".equals(p))
      {
      }
      else
      if(BuLib.isMetal())
      {
	_g.setColor(bg.darker());
	if(horizontal) _g.drawLine(x+2,0,x+2,h-1);
	else           _g.drawLine(0,y+2,w-1,y+2);
      }
      else
      {
	_g.setColor(bg);
	if(horizontal) _g.draw3DRect(x+1,2,2,h-5,true);
	else           _g.draw3DRect(2,y+1,w-5,2,true);
      }
    }
  }

  public static final int VERTICAL  =JSplitPane.VERTICAL_SPLIT;
  public static final int HORIZONTAL=JSplitPane.HORIZONTAL_SPLIT;

  private JComponent left_,middle_,right_;
  private Divider    divider1_,divider2_;
  private int        orientation_;

  public BuSplit3Pane()
  {
    this(null,null,null,HORIZONTAL);
  }

  public BuSplit3Pane(int _orientation)
  {
    this(null,null,null,_orientation);
  }

  public BuSplit3Pane(JComponent _left, JComponent _middle, JComponent _right)
  {
    this(_left,_middle,_right,HORIZONTAL);
  }

  public BuSplit3Pane(JComponent _left, JComponent _middle,
		      JComponent _right, int _orientation)
  {
    putClientProperty("BuSplit3Pane.dragMode","outline");

    orientation_=_orientation;

    divider1_=new Divider();
    divider1_.addMouseMotionListener(this);
    divider1_.addMouseListener(this);

    divider2_=new Divider();
    divider2_.addMouseMotionListener(this);
    divider2_.addMouseListener(this);

    setLayout(this);
    add(divider1_);
    add(divider2_);

    setLeftComponent  (_left);
    setMiddleComponent(_middle);
    setRightComponent (_right);

    setOpaque(false);

    updateSplits();

    /*
    if(_left  ==null) { _left  =new JPanel(); _left  .setVisible(false); }
    if(_middle==null) { _middle=new JPanel(); _middle.setVisible(false); }
    if(_right ==null) { _right =new JPanel(); _right .setVisible(false); }

    left_  =_left;
    middle_=_middle;
    right_ =_right;

    updateSplits();

    setLayout(this);
    add(left_);
    add(divider1_);
    add(middle_);
    add(divider2_);
    add(right_);
    */
  }

  @Override
  public void updateUI()
  {
    super.updateUI();
    setOpaque(false);
  }

  private int dividerSize_=-1;
  private int dividerLocation_=-1;

  public int getDividerSize()
  {
    int r=dividerSize_;
    if(r==-1) r=0;
    return r;
  }

  public void setDividerSize(int _size)
  {
    dividerSize_=_size;
  }

  public int getDividerLocation()
  {
    int r=dividerLocation_;
    if(r==-1) r=0;
    return r;
  }

  public void setDividerLocation(double _p)
  {
    Insets i=getInsets();
    if(isHorizontal()) dividerLocation_=(int)((getWidth ()-i.left-i.right)*_p);
    else               dividerLocation_=(int)((getHeight()-i.top-i.bottom)*_p);
    divider1_.p_=dividerLocation_;
    updateDisplay();
  }

  /*
  public void setBorder(Border _b)
  {
    super.setBorder(new LineBorder(Color.red,3));
  }
  */

  public JComponent getLeftComponent   () { return left_;   }
  public JComponent getTopComponent    () { return left_;   }
  public JComponent getMiddleComponent () { return middle_; }
  public JComponent getRightComponent  () { return right_;  }
  public JComponent getBottomComponent () { return right_;  }

  public void setTopComponent(JComponent _left)
    { setLeftComponent(_left); }

  public void setBottomComponent(JComponent _right)
    { setRightComponent(_right); }

  private Border contentBorder_=UIManager.getBorder("SplitPane.contentBorder");

  public void setLeftComponent(JComponent _left)
  {
    JComponent left=_left;
    if(left_!=null) remove(left_);

    if(left==null) { left=new JPanel(); left.setVisible(false); }
    left_=left;

    if(  (contentBorder_!=null)
       &&(left_.getBorder() instanceof BorderUIResource))
      left_.setBorder(contentBorder_);

    add(left_);
  }

  public void setMiddleComponent(JComponent _middle)
  {
    JComponent middle=_middle;
    if(middle_!=null) remove(middle_);

    if(middle==null) { middle=new JPanel(); middle.setVisible(false); }
    middle_=middle;

    if(  (contentBorder_!=null)
       &&(middle_.getBorder() instanceof BorderUIResource))
      middle_.setBorder(contentBorder_);

    add(middle_);
  }

  public void setRightComponent(JComponent _right)
  {
    JComponent right=_right;
    if(right_!=null) remove(right_);

    if(right==null) { right=new JPanel(); right.setVisible(false); }
    right_=right;

    if(  (contentBorder_!=null)
       &&(right_.getBorder() instanceof BorderUIResource))
      right_.setBorder(contentBorder_);

    add(right_);
  }

  // Divers

  public int getOrientation()
    { return orientation_; }

  public void setOrientation(int _orientation)
  {
    orientation_=_orientation;
    updateSplits();
  }

  public boolean isVertical()
    { return getOrientation()==VERTICAL; }

  public boolean isHorizontal()
    { return getOrientation()!=VERTICAL; }

  public void swapColumns()
  {
    JComponent ctmp=left_;
    Divider    dtmp=divider1_;

    left_ =right_;
    right_=ctmp;
    divider1_=divider2_;
    divider2_=dtmp;

    updateDisplay();
  }

  public boolean setOneTouchExpandable()
  {
    return true;
  }

  public void setOneTouchExpandable(boolean _state)
  {
  }

  public boolean isContinuousLayout()
  {
    return false;
  }

  public void setContinuousLayout(boolean _state)
  {
  }

  public void resetToPreferredSizes()
  {
    updateSplits();
  }

  public void updateSplits()
  {
    if(isHorizontal())
    {
      int wl=left_ .getPreferredSize().width;
      int wr=right_.getPreferredSize().width;

      if((divider1_.p_!=wl)||(divider2_.p_!=-wr))
      {
        divider1_.p_=wl;
        divider2_.p_=wr;
        updateDisplay();
      }
    }
    else
    {
      int hl=left_ .getPreferredSize().height;
      int hr=right_.getPreferredSize().height;

      if((divider1_.p_!=hl)||(divider2_.p_!=-hr))
      {
        divider1_.p_=hl;
        divider2_.p_=hr;
        updateDisplay();
      }
    }
  }

  public void updateDisplay()
  {
    /*
    invalidate();
    doLayout();
    validate();
    */
    revalidate();
    //repaint();
  }

  private boolean isOutlineDragMode()
  {
    return "outline".equals(getClientProperty("BuSplit3Pane.dragMode"));
  }

  // Mouse

  public void mouseDown(MouseEvent _evt)
  { }

  @Override
  public void mouseEntered(MouseEvent _evt)
  { }

  @Override
  public void mouseExited(MouseEvent _evt)
  { }

  @Override
  public void mousePressed(MouseEvent _evt)
  {
    Divider divider=(Divider)_evt.getSource();

    if(isOutlineDragMode())
      paintDraggedDivider(divider);
  }

  @Override
  public void mouseReleased(MouseEvent _evt)
  {
    Divider divider=(Divider)_evt.getSource();
    divider.cursor_=Cursor.MOVE_CURSOR;
    divider.setCursor(Cursor.getPredefinedCursor(divider.cursor_));
    //new Cursor(divider.cursor_));

    if(isOutlineDragMode())
    {
      paintDraggedDivider(divider);
      updateDisplay();
    }
  }

  public void mouseUp(MouseEvent _evt)
  { }

  @Override
  public void mouseClicked(MouseEvent _evt)
  {
    Divider divider=(Divider)_evt.getSource();
    boolean refresh=false;

    if((_evt.getModifiers()&InputEvent.BUTTON3_MASK)==0)
    {
      refresh=(divider.p_!=divider.q_);

      if(divider.p_!=0)
      {
	divider.q_=divider.p_;
	divider.p_=0;
      }
      else
      {
	divider.p_=divider.q_;
	divider.q_=0;
      }
    }
    else
    if(divider==divider1_)
    {
      if(isHorizontal())
      {
        int wl=left_.getPreferredSize().width;
        if(divider1_.p_!=wl)
        {
	  divider1_.p_=wl;
	  refresh=true;
	}
      }
      else
      {
        int hl=left_.getPreferredSize().height;
        if(divider1_.p_!=hl)
        {
	  divider1_.p_=hl;
	  refresh=true;
	}
      }
    }
    else
    if(divider==divider2_)
    {
      if(isHorizontal())
      {
        int wr=right_.getPreferredSize().width;
        if(divider2_.p_!=wr)
        {
	  divider2_.p_=wr;
	  refresh=true;
        }
      }
      else
      {
        int hr=right_.getPreferredSize().height;
        if(divider2_.p_!=hr)
        {
	  divider2_.p_=hr;
	  refresh=true;
        }
      }
    }

    if(refresh) updateDisplay();
  }

  // MouseMotion

  private void paintDraggedDivider(Divider _d)
  {
    if(isOutlineDragMode())
    {
      Insets   i=getInsets();
      Graphics g=getGraphics();

      if(isHorizontal())
      {
        int x;

        if(_d==divider1_) x=i.left+_d.p_;
        else              x=getWidth()-i.right-_d.p_-_d.getWidth();

	//g.setXORMode(new Color(64,64,64));
	//g.fillRect(x+1,i.top,_d.getWidth()-1,getHeight()-i.top-i.bottom-1);
	g.setXORMode(Color.gray);
	g.drawRect(x,i.top,_d.getWidth()-1,getHeight()-i.top-i.bottom-1);
      }
      else
      {
        int y;

        if(_d==divider1_) y=i.top+_d.p_;
        else              y=getHeight()-i.bottom-_d.p_-_d.getHeight();

	//g.setXORMode(new Color(64,64,64));
	//g.fillRect(i.left,y+1,getWidth()-i.left-i.right-1,_d.getHeight()-1);
	g.setXORMode(Color.gray);
	g.drawRect(i.left,y,getWidth()-i.left-i.right-1,_d.getHeight()-1);
      }
    }
  }

  @Override
  public void mouseDragged(MouseEvent _evt)
  {
    boolean refresh=false;
    Insets  insets=getInsets();

    if(isHorizontal())
    {
      int ex=_evt.getX();
      // System.err.println("DRAG "+ex);

      int w =getWidth()-insets.left-insets.right;

      if(_evt.getSource()==divider1_)
      {
        int pp=divider1_.p_;

	int x1=ex+divider1_.getLocation().x-insets.left;
	int x2=divider2_.getLocation().x-insets.left;
	if(x1<0) x1=0;
	if(x1+divider1_.getWidth()>=x2) x1=x2-divider1_.getWidth();
	if(x1+divider1_.getWidth()>=w ) x1=w-divider1_.getWidth();

	int nc=-1;
	if(x1!=pp)
        {
	  if(x1<pp) nc=Cursor.W_RESIZE_CURSOR;
	  else      nc=Cursor.E_RESIZE_CURSOR;

	  paintDraggedDivider(divider1_);
	  divider1_.p_=x1;
	  paintDraggedDivider(divider1_);
	  refresh=true;
	}

        if(nc>0)
        {
	  divider1_.cursor_=nc;
	  divider1_.setCursor(Cursor.getPredefinedCursor(divider1_.cursor_));
          //new Cursor(divider1_.cursor_));
	}
      }

      if(_evt.getSource()==divider2_)
      {
        int pp=divider2_.p_;

        int x1=divider1_.getLocation().x-insets.left;
	int x2=ex+divider2_.getLocation().x-insets.left;
	if(x2<0) x2=0;
	if(x1+divider1_.getWidth()>=x2-divider2_.getWidth())
	    x2=x1+divider1_.getWidth()+divider2_.getWidth();
	if(x2>=w) x2=w;

	x2=w-x2;
	int nc=-1;
	if(x2!=pp)
        {
	  if(x2<pp) nc=Cursor.W_RESIZE_CURSOR;
	  else      nc=Cursor.E_RESIZE_CURSOR;

	  paintDraggedDivider(divider2_);
	  divider2_.p_=x2;
	  paintDraggedDivider(divider2_);
	  refresh=true;
	}

	if(nc>0)
        {
	  divider2_.cursor_=nc;
	  divider2_.setCursor(Cursor.getPredefinedCursor(divider2_.cursor_));
          //(new Cursor(divider2_.cursor_));
	}
      }
    }
    else // Vertical
    {
      int ey=_evt.getY();
      // System.err.println("DRAG "+ex);

      int h =getHeight()-insets.top-insets.bottom;

      if(_evt.getSource()==divider1_)
      {
        int pp=divider1_.p_;

	int y1=ey+divider1_.getLocation().y-insets.top;
	int y2=divider2_.getLocation().y-insets.top;
	if(y1<0) y1=0;
	if(y1+divider1_.getHeight()>=y2) y1=y2-divider1_.getHeight();
	if(y1+divider1_.getHeight()>=h ) y1=h-divider1_.getHeight();

	int nc=-1;
	if(y1!=pp)
        {
	  if(y1<pp) nc=Cursor.W_RESIZE_CURSOR;
	  else      nc=Cursor.E_RESIZE_CURSOR;

	  paintDraggedDivider(divider1_);
	  divider1_.p_=y1;
	  paintDraggedDivider(divider1_);
	  refresh=true;
	}

        if(nc>0)
        {
	  divider1_.cursor_=nc;
	  divider1_.setCursor(Cursor.getPredefinedCursor(divider1_.cursor_));
          //(new Cursor(divider1_.cursor_));
	}
      }

      if(_evt.getSource()==divider2_)
      {
        int pp=divider2_.p_;

        int y1=divider1_.getLocation().y-insets.top;
	int y2=ey+divider2_.getLocation().y-insets.top;
	if(y2<0) y2=0;
	if(y1+divider1_.getHeight()>=y2-divider2_.getHeight())
	    y2=y1+divider1_.getHeight()+divider2_.getHeight();
	if(y2>=h) y2=h;

	y2=h-y2;
	int nc=-1;
	if(y2!=pp)
        {
	  if(y2<pp) nc=Cursor.W_RESIZE_CURSOR;
	  else      nc=Cursor.E_RESIZE_CURSOR;

	  paintDraggedDivider(divider2_);
	  divider2_.p_=y2;
	  paintDraggedDivider(divider2_);
	  refresh=true;
	}

	if(nc>0)
        {
	  divider2_.cursor_=nc;
	  divider2_.setCursor(Cursor.getPredefinedCursor(divider2_.cursor_));
          //(new Cursor(divider2_.cursor_));
	}
      }
    }

    if(refresh&&!isOutlineDragMode())
      updateDisplay();
  }

  @Override
  public void mouseMoved(MouseEvent _evt)
  { }

  // Layout

  @Override
  public void addLayoutComponent(String _p, Component _c) { }
  @Override
  public void addLayoutComponent(Component _c, Object _o) { }
  @Override
  public void removeLayoutComponent(Component _c) { }

  @Override
  public Dimension minimumLayoutSize(Container _c)
  {
    int w,h;

    if(isHorizontal())
    {
      w=left_.getMinimumSize().width+
	  (divider1_.isVisible() ? divider1_.getPreferredSize().width : 0)+
	  middle_.getMinimumSize().width+
	  (divider2_.isVisible() ? divider2_.getPreferredSize().width : 0)+
	  right_.getMinimumSize().width;
      
      h=Math.max(Math.max(left_.getMinimumSize().height,
			  middle_.getMinimumSize().height),
		 right_.getMinimumSize().height);
    }
    else
    {
      h=left_.getMinimumSize().height+
	  (divider1_.isVisible() ? divider2_.getPreferredSize().height : 0)+
	  middle_.getMinimumSize().height+
	  (divider2_.isVisible() ? divider2_.getPreferredSize().height : 0)+
	  right_.getMinimumSize().height;
      
      w=Math.max(Math.max(left_.getMinimumSize().width,
			  middle_.getMinimumSize().width),
		 right_.getMinimumSize().width);
    }

    return new Dimension(w,h);
  }

  @Override
  public Dimension preferredLayoutSize(Container _c)
  {
    int w,h;

    if(isHorizontal())
    {
      w=left_.getPreferredSize().width+
	  (divider1_.isVisible() ? divider1_.getPreferredSize().width : 0)+
	  middle_.getPreferredSize().width+
	  (divider2_.isVisible() ? divider2_.getPreferredSize().width : 0)+
	  right_.getPreferredSize().width;

      h=Math.max(Math.max(left_.getPreferredSize().height,
			  middle_.getPreferredSize().height),
		 right_.getPreferredSize().height);
    }
    else
    {
      h=left_.getPreferredSize().height+
	  (divider1_.isVisible() ? divider1_.getPreferredSize().height : 0)+
	  middle_.getPreferredSize().height+
	  (divider2_.isVisible() ? divider2_.getPreferredSize().height : 0)+
	  right_.getPreferredSize().height;

      w=Math.max(Math.max(left_.getPreferredSize().width,
			  middle_.getPreferredSize().width),
		 right_.getPreferredSize().width);
    }

    return new Dimension(w,h);
  }

  @Override
  public Dimension maximumLayoutSize(Container _c)
  { return preferredLayoutSize(_c); }

  @Override
  public float getLayoutAlignmentX(Container _c)
  { return 0.5f; }

  @Override
  public float getLayoutAlignmentY(Container _c)
  { return 0.5f; }

  @Override
  public void invalidateLayout(Container _c)
  {
    // System.out.println("LEFT ="+left_ .isVisible());
    // System.out.println("RIGHT="+right_.isVisible());
    divider1_.setVisible
      (  (left_.getParent()  ==this)&&left_ .isVisible()
       &&(middle_.getParent()==this)&&middle_.isVisible());
    divider2_.setVisible
      ((right_.getParent()==this)&&right_.isVisible());
  }
  
  @Override
  public void layoutContainer(Container _c)
  {
    Insets insets=getInsets();

    int w=_c.getSize().width -insets.left-insets.right;
    int h=_c.getSize().height-insets.top-insets.bottom;

    if(isHorizontal())
    {
      int d1=divider1_.getPreferredSize().width;
      int d2=divider2_.getPreferredSize().width;
      int x1=divider1_.p_;
      int x2=w-divider2_.p_;

      if(!divider1_.isVisible()) { x1=0; d1=0; }
      if(!divider2_.isVisible()) { x2=w; d2=0; }

      int wm=(x2-d2-x1-d1);
      if(!middle_.isVisible()&&left_.isVisible())
	  { x1=x2; wm=0; }

      left_    .setBounds(insets.left      ,insets.top,x1  ,h);
      divider1_.setBounds(insets.left+x1   ,insets.top,d1  ,h);
      middle_  .setBounds(insets.left+x1+d1,insets.top,wm  ,h);
      divider2_.setBounds(insets.left+x2-d2,insets.top,d2  ,h);
      right_   .setBounds(insets.left+x2   ,insets.top,w-x2,h);
    }
    else
    {
      int d1=divider1_.getPreferredSize().height;
      int d2=divider2_.getPreferredSize().height;
      int y1=divider1_.p_;
      int y2=h-divider2_.p_;

      if(!divider1_.isVisible()) { y1=0; d1=0; }
      if(!divider2_.isVisible()) { y2=h; d2=0; }

      int hm=(y2-d2-y1-d1);
      if(!middle_.isVisible()&&left_.isVisible())
	  { y1=y2; hm=0; }

      left_    .setBounds(insets.left,insets.top      ,w,y1  );
      divider1_.setBounds(insets.left,insets.top+y1   ,w,d1  );
      middle_  .setBounds(insets.left,insets.top+y1+d1,w,hm  );
      divider2_.setBounds(insets.left,insets.top+y2-d2,w,d2  );
      right_   .setBounds(insets.left,insets.top+y2   ,w,h-y2);
    }
  }

  public static void main(String[] _args)
  {
    JFrame f=new JFrame("test BuSplit3Pane");

    JButton c1=new JButton("1");
    JButton c2=new JButton("2");
    JButton c3=new JButton("3");

    c1.setBorder(new LineBorder(Color.black,2));
    c2.setBorder(new LineBorder(Color.black,2));
    c3.setBorder(new LineBorder(Color.black,2));

    BuSplit3Pane sp=new BuSplit3Pane(c1,c2,c3,VERTICAL);
    sp.setBorder(new LineBorder(Color.red,12));

    f.getContentPane().add("Center",sp);
    f.setSize(300,200);
    f.setVisible(true);
    f.setLocation(500,100);
  }
}
