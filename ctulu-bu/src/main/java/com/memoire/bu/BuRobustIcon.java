/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuRobustIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 * An ImageIcon which cares about exceptions.
 */
public class BuRobustIcon
  extends ImageIcon
{
  protected BuRobustIcon(            ) { super(      ); }
  protected BuRobustIcon(Image _image) { super(_image); }
  protected BuRobustIcon(URL   _url  ) { super(_url  ); }

  public boolean isDefault()
  {
    return (getImage()==BuLib.DEFAULT_IMAGE);
  }

  @Override
  public Image getImage()
  {
    Image r=super.getImage();
    if(r==null) r=BuLib.DEFAULT_IMAGE;
    return r;
  }

  @Override
  public void setImage(Image _image)
  {
    Image img=null;

    if((_image==null)||(_image==BuLib.DEFAULT_IMAGE))
      img=BuLib.DEFAULT_IMAGE;
    else
      img=BuLib.ensureImageIsLoaded(_image,BuLib.DEFAULT_IMAGE);

    super.setImage(img);

    /*
    final Image img1=_image;

    Runnable runnable=new Runnable()
      {
	public void run()
	{
	  super_setImage(img1);
	  //BuIcon.this.notify();
	}
      };

    long before=System.currentTimeMillis();
    long after;
    Thread t=new Thread(runnable);
    t.start();
    while((after=System.currentTimeMillis())<before+10000L)
    {
      if(!t.isAlive()) return;
      try { Thread.sleep(10L); }
      catch(InterruptedException ex) { }
    }
    if(!t.isInterrupted())
    {
      t.interrupt();
      super_setImage(BuLib.DEFAULT_IMAGE);
    }
    */
  }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    try
    {
      //if(_c==null) _c=BuLib.HELPER;
      //if(!_c.isDisplayable()) _c=BuLib.HELPER;
      super.paintIcon(null,_g,_x,_y);
    }
    catch(NullPointerException ex)
    {
      FuLog.warning("BRI: null exception in "+this);
    }
  }
}
