/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuEmptyIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Graphics;
import java.io.Serializable;
import javax.swing.Icon;

/**
 * A simple empty icon.
 * It's possible to specify the size when you instanciate it.
 */

public class BuEmptyIcon
    implements Icon, Serializable
{
  private int width_;
  private int height_;

  public BuEmptyIcon()
  {
    this(0,0);
  }

  public BuEmptyIcon(int _width, int _height)
  {
    width_ =_width;
    height_=_height;
  }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y) { }
  @Override
  public int getIconWidth()  { return width_; }
  @Override
  public int getIconHeight() { return height_; }
}

