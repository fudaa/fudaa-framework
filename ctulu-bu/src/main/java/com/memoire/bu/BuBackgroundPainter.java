/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuBackgroundPainter.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.*;
import java.io.Serializable;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;

/**
 * An utility class to paint the background of a component.
 * Accepts texture and gradient.
 */
public class BuBackgroundPainter
  implements Serializable
{

  public BuBackgroundPainter()
  { this(null,false); }

  /*
  public BuBackgroundPainter(Icon _icon)
  { this(_icon,false); }

  public BuBackgroundPainter(boolean _gradient)
  { this(null,_gradient); }
  */

  public BuBackgroundPainter(Icon _icon, boolean _gradient)
  {
    super();
    icon_=_icon;
    gradient_=_gradient;
  }

  public void paintBackground(JComponent _c, Graphics _g)
  {
    Rectangle clip=_g.getClipBounds();
    Dimension dc  =_c.getSize();
    Icon      icon=getIcon();

    if(icon==null)
    {
      String ui=_c.getUIClassID();
      ui=ui.substring(0,ui.length()-2);
      //if(ui.equals("DesktopPane")) ui="Desktop";
      ui+=".texture";
      Image image=(Image)UIManager.get(ui);

      if(image==null)
      {
        if("DesktopPane.texture".equals(ui)) ui="Desktop.texture";
        image=(Image)UIManager.get(ui);
      }

      //System.err.println("$$$ UI="+ui);
      //System.err.println("$$$ image="+image);
      //System.err.println("$$$ icon="+icon);
      if(image!=null) icon=new BuIcon(image);
    }

    if(icon!=null)
    {
      Point p,q;
      // component must be showing on the screen to determine its location
      try
      {
	p=_c.getLocationOnScreen();
	q=_c.getTopLevelAncestor().getLocationOnScreen();
	p.x-=q.x;
	p.y-=q.y;
      }
      catch(IllegalComponentStateException ex) { p=new Point(0,0); }

      int wi=icon.getIconWidth();
      int hi=icon.getIconHeight();

      while(p.x<0) p.x+=wi;
      while(p.y<0) p.y+=hi;

      int i,j,ni,nj,x,y;

      ni=clip.width /wi;
      nj=clip.height/hi;
      x =(clip.x/wi)*wi-(p.x%wi);
      y =(clip.y/hi)*hi-(p.y%hi);

      while(x+ni*wi<=clip.x+clip.width ) ni++;
      while(y+nj*hi<=clip.y+clip.height) nj++;

      if(icon instanceof ImageIcon)
      {
        for(i=0;i<ni;i++)
          for(j=0;j<nj;j++)
          {
            /*
            int xd=Math.max(x+i*wi,clip.x);
            int yd=Math.max(y+j*hi,clip.y);
            int wd=Math.min(wi,clip.width);
            int hd=Math.min(hi,clip.height);
            int xs=Math.max(0,clip.x-xd);
            int ys=Math.max(0,clip.y-yd);
            System.err.println("xs="+xs+" ys="+ys+" xd="+xd+" yd="+yd+" wd="+wd+" hd="+hd);
            _g.drawImage(((ImageIcon)icon).getImage(),
                         xs,ys,Math.min(xs+wd,wi-1),Math.min(ys+hd,hi-1),
                         xd,yd,xd-xs+Math.min(xs+wd,wi-1),yd-ys+Math.min(ys+hd,hi-1),null);
            _g.setColor(Color.yellow);
            _g.drawRect(xd,yd,wd,hd);
            */
            _g.drawImage(((ImageIcon)icon).getImage(),x+i*wi,y+j*hi,null);
          }
      }
      else
      {
        for(i=0;i<ni;i++)
          for(j=0;j<nj;j++)
            icon.paintIcon(_c,_g,x+i*wi,y+j*hi);
      }

      if(isGradient()/*&&(FuLib.jdk()>=1.2)*/&&(_g instanceof Graphics2D))
      {
        int GH=getGradientHeight();
        if(GH==0) GH=dc.height;
        Paint old=((Graphics2D)_g).getPaint();
        GradientPaint gp=new GradientPaint
          (0,dc.height-GH,new Color(255,255,255,0),
           0,dc.height-1,new Color(255,255,255,192));
        ((Graphics2D)_g).setPaint(gp);
        _g.fillRect(clip.x,clip.y,clip.width,clip.height);
        ((Graphics2D)_g).setPaint(old);
      }
    }
    else
    if(isGradient())
    {
      Color c1=_c.getBackground();
      //BuLib.getColor(_c.getBackground(),true);

      Color c2=c1.brighter().brighter();
      //BuLib.getColor(c1.brighter().brighter(),true);

      int GH=getGradientHeight();
      if(GH==0) GH=dc.height;

      int r1=c1.getRed();
      int v1=c1.getGreen();
      int b1=c1.getBlue();

      int r2=c2.getRed();
      int v2=c2.getGreen();
      int b2=c2.getBlue();

      int y0=clip.y;
      int y2=clip.y+clip.height;
      int y1=Math.max(y0,Math.min(y2,dc.height-GH));

      int x0=clip.x;
      int x2=clip.x+clip.width;

      _g.setColor(c1);

      if(c1.equals(c2))
      {
        _g.fillRect(x0,y0,x2-x0,y2-y0);
      }
      else
      {
        _g.fillRect(x0,y0,x2-x0,y1-y0);

        if(/*(FuLib.jdk()>=1.2)&&*/(_g instanceof Graphics2D))
        {
          Paint old=((Graphics2D)_g).getPaint();
          GradientPaint gp=new GradientPaint
            (0,dc.height-GH,c1,0,dc.height-1,c2);
          ((Graphics2D)_g).setPaint(gp);
          //_g.fillRect(clip.x,clip.y,clip.width,clip.height);
          _g.fillRect(x0,y1,x2-x0,y2-y1);          
          ((Graphics2D)_g).setPaint(old);
        }
        else
        {
          for(int y=y1;y<y2;y++)
          {
            int r=r1+(r2-r1)*(y-dc.height+GH)/GH;
            int v=v1+(v2-v1)*(y-dc.height+GH)/GH;
            int b=b1+(b2-b1)*(y-dc.height+GH)/GH;
            if(r<0) r=0;
            if(v<0) v=0;
            if(b<0) b=0;
            if(r>255) r=255;
            if(v>255) v=255;
            if(b>255) b=255;
            _g.setColor(new Color(r,v,b));
            _g.drawLine(x0,y,x2-1,y);
          }
        }
      }
    }
    else
    {
      Color c1=_c.getBackground();
      if(c1!=UIManager.getColor(_c.getUIClassID()+".background"))
      {
	_g.setColor(c1);
	_g.fillRect(clip.x,clip.y,clip.width,clip.height);
      }
    }

    if(isGrid())
    {
      int wi=BuDesktop.SNAPX*2;
      int hi=BuDesktop.SNAPY*2;

      int i,j,ni,nj,x,y;

      ni=clip.width /wi+2;
      nj=clip.height/hi+2;
      x =clip.x/wi*wi;
      y =clip.y/hi*hi;

      if(y==0)
      {
	y=hi;
	nj--;
	_g.setColor(_c.getBackground().brighter());
	_g.drawLine(clip.x,y-2,clip.x+clip.width,y-2);
	//_g.drawLine(0,y-2,dc.width-1,y-2);
      }

      if(isBar())
      {
	int bh=getBarHeight();
	if(bh==0) bh=44;
	while((nj>0)&&(y+nj*hi>=dc.height-1-bh))
	  nj--;
      }

      _g.setColor(_c.getBackground());
      for(i=0;i<ni;i++)
	for(j=0;j<nj;j++)
	{
	  int xg=x+i*wi-1;
	  int yg=y+j*hi-1;
	  _g.draw3DRect(xg,yg,wi-1,hi-1,false);
	}

      if(isBar())
      {
	int bh=getBarHeight();
	if(bh==0) bh=44;
	int yg=dc.height-1-bh;
	yg=yg/hi*hi-1;
	_g.setColor(_c.getBackground().darker());
	_g.drawLine(clip.x,yg,clip.x+clip.width,yg);
	//_g.drawLine(0,yg-2,dc.width-1,yg-2);
      }
    }

    if(isDots())
    {
      int wi=BuDesktop.SNAPX;
      int hi=BuDesktop.SNAPY;

      int i,j,ni,nj,x,y;

      //if((gheight_!=128)&&(clip.y+clip.height+hi>dc.height-gheight_))
      //clip.height=Math.max(0,dc.height-gheight_-clip.y-hi);

      ni=clip.width/wi+2;
      nj=clip.height/hi+2;
      x =clip.x/wi*wi;
      y =clip.y/hi*hi;

      _g.setColor(_c.getBackground());
      for(i=0;i<ni;i++)
	for(j=0;j<nj;j++)
	{
	  int xg=x+i*wi-2;
	  int yg=y+j*hi-2;
	  _g.draw3DRect(xg,yg,1,1,false);
	}
    }

    if(isBar())
    {
      int BH=getBarHeight();
      if(BH>0)
      {
	int yb=dc.height-BH;

	if(!BuLib.isSlaf())
	{
	  _g.setColor(UIManager.getColor("Panel.background"));
	  _g.fillRect(clip.x,yb,clip.width,BH);
	  if(BuLib.isMetal())
	  {
	    _g.draw3DRect(0,yb  ,dc.width-1,BH-1,false);
	    _g.draw3DRect(1,yb+1,dc.width-3,BH-3,true );
	  }
	}
	else
	{
	  Color bg=UIManager.getColor("DesktopBar.background");
	  if(bg!=null)
	  {
	    _g.setColor(bg);
	    _g.fillRect(clip.x,yb,clip.width,BH);
	  }
	  Border bx=UIManager.getBorder("DesktopBar.border");
	  if(bx!=null)
	  {
	    bx.paintBorder(_c,_g,0,yb,dc.width-1,BH-1);
	  }
	}
      }
    }
  }

  // Proprietes

  private Icon icon_;
  public Icon getIcon() { return icon_; }
  public void setIcon(Icon _icon) { icon_=_icon; }

  private boolean grid_;
  public boolean isGrid() { return grid_; }
  public void setGrid(boolean _grid) { grid_=_grid; }

  private boolean dots_;
  public boolean isDots() { return dots_; }
  public void setDots(boolean _dots) { dots_=_dots; }

  private boolean gradient_;
  public boolean isGradient() { return gradient_; }
  public void setGradient(boolean _gradient) { gradient_=_gradient; }

  private int gheight_=192;
  public int getGradientHeight() { return gheight_; }
  public void setGradientHeight(int _gheight) { gheight_=_gheight; }

  private boolean bar_;
  public boolean isBar() { return bar_; }
  public void setBar(boolean _bar) { bar_=_bar; }

  private int bheight_;
  public int getBarHeight() { return bheight_; }
  public void setBarHeight(int _bheight) { bheight_=_bheight; }
}
