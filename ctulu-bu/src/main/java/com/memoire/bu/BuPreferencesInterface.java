/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         BuPreferencesInterface.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * @deprecated use BuAbstractPreferencesPanel instead
 */

public interface BuPreferencesInterface
{
  String getTitle();
  //String getToolTipText();

  boolean isPreferencesValidable();
  void validatePreferences();

  boolean isPreferencesApplyable();
  void applyPreferences();

  boolean isPreferencesCancelable();
  void cancelPreferences();
}
