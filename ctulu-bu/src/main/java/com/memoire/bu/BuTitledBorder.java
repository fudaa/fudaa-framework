/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuTitledBorder.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class BuTitledBorder
  extends TitledBorder
{
  private static final int getDefaultJustification()
  {
    int r=UIManager.getInt("TitledBorder.justification");
    if((r<=0)||(r>5)) r=4; // LEADING
    //if((r>=4)&&(FuLib.jdk()<=1.3)) r=2;
    return r;
  }

  private static final int getDefaultPosition()
  {
    int r=UIManager.getInt("TitledBorder.position");
    if((r<=0)||(r>6)) r=2; // TOP
    return r;
  }

  public BuTitledBorder(String _title)
  {
    this(null,_title,getDefaultJustification(),
         getDefaultPosition(),null,null);
  }

  public BuTitledBorder(Border _border)
  {
    this(_border,"",getDefaultJustification(),
         getDefaultPosition(),null,null);
  }
  
  public BuTitledBorder(Border _border,String _title)
  {
    this(_border,_title,getDefaultJustification(),
         getDefaultPosition(),null,null);
  }

  public BuTitledBorder(Border _border,
                        String _title,
                        int    _titleJustification,
                        int    _titlePosition)
  {
    this(_border,_title,_titleJustification,
         _titlePosition,null,null);
  }
  
  public BuTitledBorder(Border _border,			
                        String _title,
                        int    _titleJustification,
                        int    _titlePosition,
                        Font   _titleFont)
  {
    this(_border,_title,_titleJustification,
         _titlePosition,_titleFont,null);
  }
  
  public BuTitledBorder(Border _border,                    
                        String _title,
                        int    _titleJustification,
                        int    _titlePosition,
                        Font   _titleFont,
                        Color  _titleColor)
  {
    super(_border,_title,_titleJustification,
          _titlePosition,_titleFont,_titleColor);
  }
}
