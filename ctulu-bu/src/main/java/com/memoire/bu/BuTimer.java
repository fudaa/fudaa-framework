/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuTimer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * Alias for the Swing Timer.
 * @see javax.swing.Timer
 */
public class BuTimer extends Timer
{
  public BuTimer(int delay, ActionListener listener)
  {
    super(delay,listener);
    setCoalesce(true);
  }
}
