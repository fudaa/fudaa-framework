/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuSeparator.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import javax.swing.JSeparator;
import javax.swing.UIManager;

/**
 * A separator for menus which displays a string.
 */
public class BuSeparator
  extends JSeparator
{
  private String text_;

  public BuSeparator()
  {
    this(null);
  }

  public BuSeparator(String _text)
  {
    super();
    text_=_text;
  }

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public void paintComponent(Graphics _g)
  {
    Object bar=UIManager.get("PopupMenuSeparator.bar");
    if((bar==null)||(Boolean.TRUE.equals(bar)))
    {
      if(!BuLib.isSlaf()&&
         !BuLib.isMetal()&&
         !BuLib.isLiquid()&&
         !BuLib.isSynthetica())
      {
        Color bg=UIManager.getColor("MenuItem.background");
        if(bg!=null) _g.setColor(bg);
        _g.fillRect(0,0,getWidth(),getHeight());
      }
      super.paintComponent(_g);
    }

    if(text_!=null)
    {
      Color bg=UIManager.getColor("PopupMenuSeparator.background");
      Color fg=UIManager.getColor("PopupMenuSeparator.foreground");
      if(bg==null) bg=getParent().getBackground();
      if(fg==null) fg=getParent().getForeground();

     // Dimension   d =getSize();
      Font        ft=UIManager.getFont("PopupMenuSeparator.font");
      if(ft==null) ft=BuLib.deriveFont("MenuItem",Font.BOLD,-2);
      int hs=ft.getSize()+2;

      _g.setFont(ft);

      //Object bar=UIManager.get("PopupMenuSeparator.bar");
      if((bar==null)||(Boolean.TRUE.equals(bar)))
      {
        _g.setColor(bg);
        _g.drawString(text_,2,hs-1);
        _g.drawString(text_,2,hs  );
        _g.drawString(text_,2,hs+1);
        _g.drawString(text_,4,hs-1);
        _g.drawString(text_,4,hs  );
        _g.drawString(text_,4,hs+1);
      }

      _g.setColor(fg);
      _g.drawString(text_,3,hs);
    }
  }

  @Override
  public Dimension getPreferredSize()
  {
    Font        ft=BuLib.deriveFont("MenuItem",Font.BOLD,-2);
    FontMetrics fm=BuLib.getFontMetrics(this,ft);

    Dimension r=super.getPreferredSize();

    if(text_!=null)
    {
      r.width =Math.max(r.width ,6+fm.stringWidth(text_));
      r.height=Math.max(r.height,6+fm.getAscent()+fm.getDescent());
    }

    return r;
  }
}
