/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 * Nothing more than a JPanel.
 */

public class BuPanel
  extends JPanel
{
  public BuPanel()
  {
    this(true);
  }

  public BuPanel(LayoutManager _layout)
  {
    this(_layout,true);
  }

  /**
   * @deprecated
   */
  public BuPanel(boolean _isDoubleBuffered)
  {
    this(new FlowLayout(),_isDoubleBuffered);
  }

  /**
   * @deprecated
   */
  public BuPanel(LayoutManager _layout, boolean _isDoubleBuffered)
  {
    super(_layout,_isDoubleBuffered);
  }

  // Translation

  protected String getS(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  // Texture

  @Override
  public void paintComponent(Graphics _g)
  {
    BuBackgroundPainter bp=(BuBackgroundPainter)
                           getClientProperty("BACKGROUND_PAINTER");
    if(bp!=null)
      bp.paintBackground(this,_g);

    super.paintComponent(_g);
  }
}

