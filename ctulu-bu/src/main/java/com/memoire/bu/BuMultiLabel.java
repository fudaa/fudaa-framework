/**
 * @modification $Date: 2005-08-16 12:58:04 $
 * @statut       unstable
 * @file         BuMultiLabel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * Obsolete. Use BuMultiLineLabel instead.
 * @deprecated use BuMultiLineLabel instead.
 */

public class BuMultiLabel
       extends BuLabelMultiLine
{
  public BuMultiLabel()
  {
    super(null);
  }

  public BuMultiLabel(String _texte)
  {
    super(_texte);
  }
}
