/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuFileFilterDirectory.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.io.File;

/**
 * A simple implementation of a FileFilter to choose a directory.
 */
public class BuFileFilterDirectory
  extends BuFileFilter
{
  public BuFileFilterDirectory()
  {
    super("",__("Répertoires"));
  }

  @Override
  public boolean accept(String _f)
  {
    return (_f!=null)&&accept(new File(_f));
  }

  @Override
  public boolean accept(File _f)
  {
    return (_f!=null)&&_f.isDirectory();
  }

  @Override
  public boolean accept(File _d, String _fn)
  {
    return (_fn!=null)&&accept(new File(_d,_fn));
  }

  @Override
  public String getDescription()
  {
    if(fullDescription_==null)
    {
      if((description_==null)||isExtensionListInDescription())
      {
	if(description_!=null)
	  fullDescription_=description_;
	else
	  fullDescription_= getString("Répertoires");
      }
      else
      {
	fullDescription_=description_;
      }
    }
    return fullDescription_;
  }
}
