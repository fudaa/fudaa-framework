/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuPreviewFrame.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 */
public class BuPreviewFrame
  extends BuInternalFrame
  implements ListSelectionListener, /*ActionListener, */BuBorders
{
  private static final class PRI
    implements Icon
  {
    private Image image_;

    public PRI(Image _image)
    {
      image_=_image;
    }

    @Override
    public int getIconWidth () { return 54; }
    @Override
    public int getIconHeight() { return 51; }

    @Override
    public void paintIcon(Component _c, Graphics _g, int _x, int _y)
    {
      if(image_!=null)
      {
	int wi=image_.getWidth (_c);
	int hi=image_.getHeight(_c);
	double f=Math.min(1.,Math.min(48D/wi,48D/hi));
	wi=(int)Math.round(f*wi);
	hi=(int)Math.round(f*hi);
	_g.drawImage(image_,_x+3+(48-wi)/2,_y+3+(48-hi)/2,wi,hi,null);

	Color old=_g.getColor();
	_g.setColor(Color.black);
	_g.drawRect(_x+3+(48-wi)/2,_y+3+(48-hi)/2,wi-1,hi-1);
	_g.setColor(old);
      }

      /*
      Shape old=_g.getClip();
      _g.clipRect(_x,_y,w_,h_);
      int x=_x+(w_-icon_.getIconWidth ())/2;
      int y=_y+(h_-icon_.getIconHeight())/2;
      icon_.paintIcon(_c,_g,x,y);
      _g.setClip(old);
      */
    }
  }

  private static final class PCR
    extends BuAbstractCellRenderer
  {
    public PCR()
    {
      super(BuAbstractCellRenderer.LIST);
      setDarkerOddLines(false);
    }

    @Override
    public Component getListCellRendererComponent
      (JList   _list,
       Object  _value,
       int     _index,
       boolean _selected,
       boolean _focus)
    {
      Object pri=new PRI((Image)_value);
      BuLabel r=(BuLabel)super.getListCellRendererComponent
	  (_list,pri,_index,_selected,_focus);

      r.setText(""+(_index+1));
      r.setHorizontalTextPosition(SwingConstants.CENTER);
      r.setVerticalTextPosition(SwingConstants.BOTTOM);
      return r;
    }
  }

  private BuList    ltPages_;
  private BuPanel   pnPage_;
  private BuButton  btImprimer_;
  private BuButton  btFermer_;

  private Frame       parent_;
  private String      name_;
  private BuPrintable composant_;

  //private BuTabbedPane tpMain_;
  //private BuPanel      pnButtons_;

  // Constructeur
  
  public BuPreviewFrame(Image[] _images)
  {
    this(null,_images,null,null,null);
  }

  public BuPreviewFrame(BuCommonImplementation _app,Image[] _images)
  {
    this(_app,_images,null,null,null);
  }

  public BuPreviewFrame
    (BuCommonImplementation _app,Image[] _images,
     Frame _parent, String _name, BuPrintable _composant)
  {
    super("",true,true,true,true);
    //super("",false,true,false,true);

    parent_   =_parent;
    name_     =_name;
    composant_=_composant;

    setName("ifPREVIEW");
    if(_app!=null)
      _app.installContextHelp(getRootPane(),"bu/p-previsualisateur.html");

    ltPages_=new BuList(_images);
    ltPages_.setCellRenderer(new PCR());
    ltPages_.addListSelectionListener(this);

    pnPage_=new BuPanel();
    pnPage_.setLayout(new GridLayout(0,1,5,5));
    //pnPage_.setLayout(new BuGridLayout(2,5,5,false,false,false,false));
    //pnPage_.setLayout(new BuHorizontalLayout(5,false,false));
    //FlowLayout());
    pnPage_.setBorder(EMPTY0000);
    //pnPage_.add(picture);

    /*
    tpMain_=new BuTabbedPane();
    tpMain_.setTabPlacement(BuLib.getTabPlacement());
    tpMain_.addChangeListener(this);
    */

    //Panneau de validation
    //pnButtons_=new BuPanel();
    //pnButtons_.setLayout(new BuButtonLayout());

    /*
      btFermer_=new BuButton(_("Fermer"));
      btFermer_.setIcon(BuResource.BU.loadToolCommandIcon("FERMER"));
      btFermer_.setActionCommand("FERMER");
      btFermer_.addActionListener(this);
      pnButtons_.add(btFermer_,0);
    */

    JComponent content=(JComponent)getContentPane();
    content.setLayout(new BuBorderLayout(5,5));
    content.setBorder(EMPTY5555);
    //content.add(tpMain_ ,BuBorderLayout.CENTER);
    // content.add(pnButtons_,BuBorderLayout.SOUTH);

    BuScrollPane sp1=new BuScrollPane(ltPages_);
    sp1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    sp1.setVerticalScrollBarPolicy  (ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
    Dimension ps1=sp1.getPreferredSize();
    sp1.setPreferredSize(new Dimension(ps1.width,200));
    sp1.setVisible(_images.length>1);
    content.add(sp1,BuBorderLayout.EAST);

    /*
    BuScrollPane sp2=new BuScrollPane(pnPage_);
    sp2.setHorizontalScrollBarPolicy(BuScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    sp2.setVerticalScrollBarPolicy  (BuScrollPane.VERTICAL_SCROLLBAR_NEVER   );
    content.add(sp2,BuBorderLayout.CENTER);
    */
    content.add(pnPage_,BuBorderLayout.CENTER);

    if(composant_!=null)
    {
      BuPanel pnb=new BuPanel();
      pnb.setLayout(new BuButtonLayout());

      btImprimer_=new BuButton();
      btImprimer_.setIcon(BuResource.BU.loadButtonCommandIcon("IMPRIMER"));
      btImprimer_.setText(getString("Imprimer"));
      btImprimer_.setActionCommand("IMPRIMER");
      btImprimer_.addActionListener(this);
      pnb.add(btImprimer_);
      
      btFermer_=new BuButton();
      btFermer_.setIcon(BuResource.BU.loadButtonCommandIcon("FERMER"));
      btFermer_.setText(getString("Fermer"));
      btFermer_.setActionCommand("FERMER");
      btFermer_.addActionListener(this);
      pnb.add(btFermer_);
      
      getRootPane().setDefaultButton(btImprimer_);
      content.add(pnb,BuBorderLayout.SOUTH);
    }

    setTitle(getString("Prévisualisation"));
    setFrameIcon(BuResource.BU.getFrameIcon("previsualiser"));

    ltPages_.setSelectedIndex(0);

    pack();
    //if((getHeight()<200)||(getWidth()<100)) setSize(220,320);
    //updateButtons();
  }
  
  // Evenements
  
  @Override
  public void valueChanged(ListSelectionEvent _evt)
  {
    Object[] values=ltPages_.getSelectedValues();

    pnPage_.removeAll();

    if(values!=null)
    {
      int l=values.length;
      int n;
           if(l>4)            n=(int)Math.sqrt(l);
      else if((l>=2)&&(l<=4)) n=2;
      else                    n=1;
      pnPage_.setLayout(new GridLayout(0,n,5,5));

      for(int i=0;i<l;i++)
      {
        Image image=(Image)values[i];
	BuPicture picture=new BuPicture(image);
	picture.setBorder(new LineBorder(Color.black));
	picture.setOpaque(true);
	picture.setBackground(Color.white);
	picture.computePreferredSize();
	picture.setMode(BuPicture.REDUCE);
	BuPanel q=new BuPanel(new BuOverlayLayout(false,false,true));
	q.add(picture);
	pnPage_.add(q,BuBorderLayout.CENTER);//);
      }
    }

    pnPage_.revalidate();
    pnPage_.repaint();

    if(btImprimer_!=null)
      btImprimer_.setEnabled((values!=null)&&(values.length>=1));
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String action=_evt.getActionCommand();
    System.err.println("BPF: action="+action);

    if("IMPRIMER".equals(action))
    {
      if((parent_!=null)&&(composant_!=null))
	BuPrinter.print(parent_,name_,composant_);
    }
    else
    if("FERMER".equals(action))
    {
      try { setClosed(true); }
      catch(PropertyVetoException ex) { }
    }
  }

  @Override
  public void setSelected(boolean _state) throws PropertyVetoException
  {
    super.setSelected(_state);
    if(isSelected()) ltPages_.requestFocus();
  }

  /*
  public void stateChanged(ChangeEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("Change : "+source.getName());
    updateButtons();
  }
  */

  /*
  private void updateButtons()
  {
    Component c=tpMain_.getSelectedComponent();
  }
  */

  // Methodes publiques

  /*
  public void selectTab(int _index)
  {
    tpMain_.setSelectedIndex(_index);
  }
  
  public void addTab(int _page,Image _image)
  {
    String    title  =""+_page;
    String    tooltip="Page "+_page;
    BuPicture picture=new BuPicture(_image);

    picture.setBorder(new LineBorder(Color.black));
    picture.setOpaque(true);
    picture.setBackground(Color.white);
    picture.setMode(BuPicture.ZOOM);

    BuPanel pn=new BuPanel();
    pn.setLayout(new FlowLayout());
    pn.setBorder(EMPTY5555);
    pn.add(picture);

    tpMain_.addTab(title, null, pn, tooltip);
  }
  */
  
  // Methodes privees
  
}
