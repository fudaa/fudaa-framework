/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuEditorPane.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.io.IOException;
import java.net.URL;
import javax.swing.JEditorPane;
import javax.swing.text.EditorKit;

public class BuEditorPane
  extends JEditorPane
{
  public BuEditorPane()
  {
    super();
  }

  public BuEditorPane(URL _url) throws IOException
  {
    super(_url);
  }

  public BuEditorPane(String _url) throws IOException
  {
    super(_url);
  }

  @Override
  public EditorKit getEditorKitForContentType(String _type)
  {
    /*
    if(!BuLib.isSwing11())
    {
      // remove charset if swing 1.03
      int i=_type.indexOf(";");
      if(i>=0) _type=_type.substring(0,i).trim();
    }
    */

    return super.getEditorKitForContentType(_type);
  } 

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }
}
