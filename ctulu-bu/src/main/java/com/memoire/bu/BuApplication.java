/**
 * @modification $Date: 2006-10-19 09:39:43 $
 * @statut       unstable
 * @file         BuApplication.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.WindowConstants;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 * A root class for stand-alone applications. Compatible with Bu et Swing. Delegates quite everything to
 * BuCommonImplementation.
 */
public class BuApplication extends JFrame implements BuCommonInterface, WindowListener, InternalFrameListener {
  // private static final boolean TRACE=Bu.TRACE&&true;
  // private static final boolean DEBUG=Bu.DEBUG&&true;

  /*
   * protected void processKeyEvent(KeyEvent _evt) { if( (_evt.getKeyCode()==KeyEvent.VK_F1)
   * &&(_evt.getID()==KeyEvent.KEY_PRESSED)) { System.err.println("Aide"); // System.err.println(" sur
   * "+xmouse_+","+ymouse_); // System.err.println(" sur "+help_); implementation_.displayURL(null); } else
   * super.processKeyEvent(_evt); }
   */

  /*
   * protected void processMouseEvent(MouseEvent _evt) { System.err.println("ME="+_evt); if(
   * ((_evt.getModifiers()&MouseEvent.SHIFT_MASK)!=0) &&(_evt.getID()==MouseEvent.MOUSE_CLICKED)) {
   * System.err.println("Aide point�e"); // System.err.println(" sur "+xmouse_+","+ymouse_); // System.err.println(" sur
   * "+help_); } else super.processMouseEvent(_evt); }
   */

  /*
   * int xmouse_,ymouse_; Component help_;
   */

  public BuApplication() {
    super();
    BuRegistry.register(this);
    setImplementation(new BuCommonImplementation());
  }

  protected String getString(String _s) {
    return BuResource.BU.getString(_s);
  }

  @Override
  public void init() {
    Dimension ecran = getToolkit().getScreenSize();
    setLocation(ecran.width / 10, ecran.height / 10);
    setSize(ecran.width * 4 / 5, ecran.height * 4 / 5);

    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(this);

    implementation_.init();
  }

  @Override
  public void start() {
    BuInformationsSoftware il = getInformationsSoftware();

    if (getName() == null) setName("fr" + il.name.toUpperCase());

    if (getRootPane().getName() == null) getRootPane().setName("rp" + il.name.toUpperCase());

    implementation_.installContextHelp(getRootPane(), ""); // il.man);

    addNotify();
    /*
     * pack(); getRootPane().invalidate(); getRootPane().revalidate(); getRootPane().repaint(0);
     */
    // getRootPane().revalidate();
    // getRootPane().repaint(0);
    BuIcon logo = getInformationsSoftware().logo;
    setIconImage(logo != null ? logo.getImage() : BuResource.BU.getImage("buapplication"));
    setTitle(il.name + " " + il.version);

    BuSplashScreen ss = implementation_.getSplashScreen();

    if (ss != null) {
      ss.setVisible(false);
      ss = null;
      implementation_.setSplashScreen(null);
    }

    BuPreferences.BU.applyOn(getApp());
    implementation_.prestart();
    setVisible(true);
    implementation_.start();
  }

  /*
   * public Image getIconImage() { BuIcon l=getInformationsSoftware().logo; Image r=null; if(l!=null) r=l.getImage();
   * else r=BuResource.BU.getImage("buapplication"); return r; }
   */

  // App
  @Override
  public BuCommonInterface getApp() {
    return this;
  }

  // Implementation

  protected BuCommonImplementation implementation_;

  @Override
  public BuCommonImplementation getImplementation() {
    return implementation_;
  }

  public void setImplementation(BuCommonImplementation _implementation) {
    implementation_ = _implementation;
    implementation_.setApp(this);
  }

  // Main Panel

  @Override
  public BuMainPanel getMainPanel() {
    return implementation_.getMainPanel();
  }

  @Override
  public void setMainPanel(BuMainPanel _p) {
    implementation_.setMainPanel(_p);
  }

  // Main MenuBar

  @Override
  public BuMenuBar getMainMenuBar() {
    return implementation_.getMainMenuBar();
  }

  @Override
  public void setMainMenuBar(BuMenuBar _mb) {
    implementation_.setMainMenuBar(_mb);
  }

  // Main ToolBar

  @Override
  public BuToolBar getMainToolBar() {
    return implementation_.getMainToolBar();
  }

  @Override
  public void setMainToolBar(BuToolBar _tb) {
    implementation_.setMainToolBar(_tb);
  }

  // Specific Bar

  @Override
  public BuSpecificBar getSpecificBar() {
    return implementation_.getSpecificBar();
  }

  @Override
  public void setSpecificBar(BuSpecificBar _sb) {
    implementation_.setSpecificBar(_sb);
  }

  // Action

  @Override
  public void actionPerformed(ActionEvent _evt) {
    implementation_.actionPerformed(_evt);
  }

  // Divers

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return implementation_.getInformationsSoftware();
  }

  @Override
  public Frame getFrame() {
    return this;
  }

  @Override
  public void setLookAndFeel(String _lnf) {
    implementation_.setLookAndFeel(_lnf);
  }

  @Override
  public void removeAction(String _cmd) {
    implementation_.removeAction(_cmd);
  }

  @Override
  public void removeDummySeparators() {
    implementation_.removeDummySeparators();
  }

  @Override
  public void setEnabledForAction(String _cmd, boolean _enabled) {
    implementation_.setEnabledForAction(_cmd, _enabled);
  }

  @Override
  public void setCheckedForAction(String _cmd, boolean _enabled) {
    implementation_.setCheckedForAction(_cmd, _enabled);
  }

  @Override
  public void showOrHide(String _cmd, JComponent _c) {
    implementation_.showOrHide(_cmd, _c);
  }

  @Override
  public boolean confirmExit() {
    return implementation_.confirmExit();
  }

  @Override
  public void displayURL(String _url) {
    BuInformationsSoftware il = getInformationsSoftware();
    new BuDialogMessage(this, il, (_url == null) ? il.man : _url).activate();
  }

  @Override
  public void setVisible(boolean _visible) {
    if (_visible) {
      // RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);
      // BuLib.setDoubleBuffered(this,false);

      BuLib.setDoubleBuffered(getMainMenuBar(), false);
      BuLib.setDoubleBuffered(getMainToolBar(), false);
      BuLib.setDoubleBuffered(getMainPanel(), false);
    }
    super.setVisible(_visible);
  }

  // Commands

  @Override
  public void about() {
    implementation_.about();
  }

  @Override
  public void cut() {
    implementation_.cut();
  }

  @Override
  public void copy() {
    implementation_.copy();
  }

  @Override
  public void exit() {
    if (confirmExit()) {
      if (BuPreferences.BU.getIntegerProperty("window.size",2) == 2) {
        Point p = getLocation();
        Dimension d = getSize();
        boolean maximize = (getExtendedState() == JFrame.MAXIMIZED_BOTH);
        BuPreferences.BU.putIntegerProperty("window.x", p.x);
        BuPreferences.BU.putIntegerProperty("window.y", p.y);
        // BM : En cas de fenetre maximis�e, on sauve -1. Sera restitu� comme tel.
        BuPreferences.BU.putIntegerProperty("window.w", maximize ? -1 : d.width);
        BuPreferences.BU.putIntegerProperty("window.h", maximize ? -1 : d.height);
      }
      if(implementation_.buildMainPanel_){
        BuPreferences.BU.putIntegerProperty("leftcolumn.width",getMainPanel().getLeftColumn().getWidth());
        BuPreferences.BU.putIntegerProperty("rightcolumn.width",getMainPanel().getRightColumn().getWidth());
      }

      BuPreferences.BU.writeIniFile();

      // if(DEBUG)
      // FuLog.debug("BAN: try to unregister with "+BuRegistry.getInstance());
      BuRegistry.unregister(this);

      // if(FuLib.isJDistroRunning())
      // System.exit(0);
    }
    // else if(DEBUG) FuLog.debug("BAN: exit *NOT* confirmed");
  }

  @Override
  public void duplicate() {
    implementation_.duplicate();
  }

  @Override
  public void find() {
    implementation_.find();
  }

  @Override
  public void license() {
    implementation_.license();
  }

  @Override
  public void paste() {
    implementation_.paste();
  }

  @Override
  public void print() {
    JInternalFrame frame = getMainPanel().getCurrentInternalFrame();

    if (frame instanceof BuPrintable) {
      BuPrinter.print(this, frame.getTitle(), (BuPrintable) frame);
    } else {
      BuInformationsSoftware il = getInformationsSoftware();
      new BuDialogError(this, il, getString("L'impression n'est pas disponible pour") + "\"" + frame.getTitle() + "\".")
          .activate();
    }
  }

  @Override
  public void redo() {
    implementation_.redo();
  }

  @Override
  public void replace() {
    implementation_.replace();
  }

  @Override
  public void select() {
    implementation_.select();
  }

  @Override
  public void undo() {
    implementation_.undo();
  }

  // Window

  @Override
  public void windowActivated(WindowEvent _evt) {
    implementation_.windowActivated(_evt);
  }

  @Override
  public void windowClosed(WindowEvent _evt) {
    implementation_.windowClosed(_evt);
  }

  @Override
  public void windowClosing(WindowEvent _evt) {
    implementation_.windowClosing(_evt);
  }

  @Override
  public void windowDeactivated(WindowEvent _evt) {
    implementation_.windowDeactivated(_evt);
  }

  @Override
  public void windowDeiconified(WindowEvent _evt) {
    implementation_.windowDeiconified(_evt);
  }

  @Override
  public void windowIconified(WindowEvent _evt) {
    implementation_.windowIconified(_evt);
  }

  @Override
  public void windowOpened(WindowEvent _evt) {
    implementation_.windowOpened(_evt);
  }

  // InternalFrame

  @Override
  public void internalFrameActivated(InternalFrameEvent _evt) {
    implementation_.internalFrameActivated(_evt);
  }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _evt) {
    implementation_.internalFrameDeactivated(_evt);
  }

  @Override
  public void internalFrameClosed(InternalFrameEvent _evt) {
    implementation_.internalFrameClosed(_evt);
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent _evt) {
    implementation_.internalFrameClosing(_evt);
  }

  @Override
  public void internalFrameDeiconified(InternalFrameEvent _evt) {
    implementation_.internalFrameDeiconified(_evt);
  }

  @Override
  public void internalFrameIconified(InternalFrameEvent _evt) {
    implementation_.internalFrameIconified(_evt);
  }

  @Override
  public void internalFrameOpened(InternalFrameEvent _evt) {
    implementation_.internalFrameOpened(_evt);
  }

  // Anti-aliasing
  /*
   * public void paint(Graphics _g) { BuLib.setAntialiasing(this,_g); super.paint(_g); }
   */

  // Main
  public static void main(String[] _args) {
    BuApplication app = new BuApplication();
    app.init();
    app.start();
  }
}
