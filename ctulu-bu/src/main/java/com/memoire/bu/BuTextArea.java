/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut unstable
 * @file BuTextArea.java
 * @version 0.43
 * @author Romain Guy
 * @email guy.romain@bigfoot.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import com.memoire.fu.FuLog;
import com.memoire.re.RE;
import com.memoire.re.REException;
import com.memoire.re.REMatch;
import com.memoire.re.RESyntax;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.dnd.DropTarget;
import java.util.TooManyListenersException;

/**
 * This component extends a JTextArea, providing methods to undo/redo, find/replace and read/save. This component
 * extends a JTextArea. This component provides its own methods to read and save files (even to zip them).
 *
 * @author Romain Guy, guy.romain@bigfoot.com
 */
public class BuTextArea extends JTextArea implements UndoableEditListener, DocumentListener, BuTextComponentInterface,
    DtDragSensible {
  /**
   * This constant defines an open dialog box.
   */
  public static final int OPEN = 0;
  /**
   * This constant defines a save dialog box.
   */
  public static final int SAVE = 1;
  // Private declaration
  private CompoundEdit compoundEdit;
  private String fontName;
  private int anchor, fontSize, fontStyle;
  private boolean dirty, newText, operation;
  private UndoManager undo = new UndoManager();
  /**
   * This constant defines the size of the buffer used to read files
   */
  private static final int BUFFER_SIZE = 32768;

  /**
   * The constructor add the necessary listeners, set some stuffs (caret color, borers, fonts...).
   */
  public BuTextArea() {
    this(15, 40);
  }

  public BuTextArea(int _lines, int _cols) {
    super(_lines, _cols);

    getDocument().addDocumentListener(this);
    getDocument().addUndoableEditListener(this);

    Font defaultFont = getFont();
    fontName = defaultFont.getName();
    fontSize = defaultFont.getSize();
    fontStyle = defaultFont.getStyle();
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);

    if (drop_) {
      Rectangle r = getDropRect();
      _g.setColor(new Color(255, 128, 0));
      _g.drawRoundRect(r.x, r.y, r.width - 1, r.height - 1, 9, 9);
    }
  }

  // Drop

  @Override
  public void setDropTarget(DropTarget dropTarget) {
    super.setDropTarget(dropTarget);
    if (dropTarget != null) {
      try {
        dropTarget.removeDropTargetListener(DtDragSensible.SINGLETON);
      } catch (IllegalArgumentException ex) {
      }
      try {
        dropTarget.addDropTargetListener(DtDragSensible.SINGLETON);
      } catch (TooManyListenersException ex) {
      }
    }
  }

  private boolean drop_;

  private Rectangle getDropRect() {
    return getVisibleRect();
  }

  @Override
  public void dragEnter(boolean acceptable, Point location) {
    dragOver(acceptable, location);
  }

  @Override
  public void dragOver(boolean acceptable, Point location) {
    if (!drop_ && acceptable) {
      drop_ = true;
      repaint();
    }
  }

  @Override
  public void dragExit() {
    if (drop_) {
      drop_ = false;
      repaint();
    }
  }

  // Interfaces

  public void select() {
    select(0, getLength());
  }

  public void duplicate() {
    int s = getSelectionStart();
    int e = getSelectionEnd();
    if (s < e) {
      copy();
      setCaretPosition(e);
      paste();
      setSelectionEnd(e + (e - s));
      setSelectionStart(e);
    }
  }

  @Override
  public void go(int _line) {
    Element map = getDocument().getDefaultRootElement();
    Element line = map.getElement(_line);
    select(line.getStartOffset(), line.getEndOffset());
  }

  /**
   * Display a file chooser dialog box.
   *
   * @param owner <code>Component</code> which 'owns' the dialog
   * @param mode Can be either <code>LOAD</code> or <code>SAVE</code>
   * @return The path to selected file, null otherwise
   */
  public static String chooseFile(Component owner, int mode) {
    BuFileChooser chooser = new BuFileChooser();
    if (mode == OPEN) {
      chooser.setDialogType(JFileChooser.OPEN_DIALOG);
    } else if (mode == SAVE) {
      chooser.setDialogType(JFileChooser.SAVE_DIALOG);
    }
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setFileHidingEnabled(true);
    if (chooser.showDialog(owner, null) == JFileChooser.APPROVE_OPTION) {
      return chooser.getSelectedFile()
          .getAbsolutePath();
    }
    return null;
  }

  /**
   * Display an error message in a dialog box.
   *
   * @param message The message to display
   */
  public static void showError(String message) {
    JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Return current font's name
   */
  public String getFontName() {
    return fontName;
  }

  /**
   * Return current font's size
   */
  public int getFontSize() {
    return fontSize;
  }

  /**
   * Return current font's style (bold, italic...)
   */
  public int getFontStyle() {
    return fontStyle;
  }

  /**
   * Set the font which has to be used.
   *
   * @param name The name of the font
   */
  public void setFontName(String name) {
    fontName = name;
    changeFont();
  }

  /**
   * Set the size of the font.
   *
   * @param size The new font's size
   */
  public void setFontSize(int size) {
    fontSize = size;
    changeFont();
    repaint();
  }

  /**
   * Set the style of the font.
   *
   * @param style The new style to apply
   */
  public void setFontStyle(int style) {
    fontStyle = style;
    changeFont();
    repaint();
  }

  /**
   * Set the new font.
   */
  private void changeFont() {
    setFont(new Font(fontName, fontStyle, fontSize));
  }

  /**
   * Calling this will allow the DocumentListener to use setChanged().
   */
  public void endOperation() {
    operation = false;
  }

  /**
   * Return true if we can use the setChanged() method, false otherwise.
   */
  public boolean getOperation() {
    return operation;
  }

  /**
   * This is just to reduce code size of other classes.
   *
   * @param off The line index
   * @return The offset in the text where the line begins
   */
  @Override
  public int getLineStartOffset(int off) {
    return getDocument().getDefaultRootElement().getElement(off).getStartOffset();
  }

  /**
   * Return true if current text is new, false otherwise.
   */
  public boolean isNew() {
    return newText;
  }

  /**
   * Return true if area is empty, false otherwise.
   */
  public boolean isEmpty() {
    return (getLength() == 0);
  }

  /**
   * Return true if area content has changed, false otherwise.
   */
  public boolean isDirty() {
    return dirty;
  }

  /**
   * Called when the content of the area has changed.
   */
  public void setDirty() {
    dirty = true;
  }

  /**
   * Called after having saved or created a new document to ensure the content isn't 'dirty'.
   */
  public void clean() {
    dirty = false;
  }

  /**
   * Discard all edits contained in the UndoManager.
   */
  public void discard() {
    undo.discardAllEdits();
  }

  /**
   * Useful for the GUI.
   */
  public UndoManager getUndo() {
    return undo;
  }

  /**
   * undo the last operation
   */
  public void undo() {
    if (undo.canUndo()) {
      undo.undo();
    }
  }

  /**
   * redo the last operation
   */
  public void redo() {
    if (undo.canRedo()) {
      undo.redo();
    }
  }

  /**
   * Return the anchor position.
   */
  public int getAnchor() {
    return anchor;
  }

  /**
   * Set the anchor postion.
   *
   * @param offset The new anchor's position
   */
  public void setAnchor(int offset) {
    anchor = offset;
  }

  /**
   * Return the lentgh of the text in the area.
   */
  @Override
  public int getLength() {
    Document doc = getDocument();
    return (doc != null) ? doc.getLength() : 0;
  }

  /**
   * Used for ReplaceAll. This merges all text changes made between the beginCompoundEdit() and the endCompoundEdit()
   * calls into only one undo event.
   */
  public void beginCompoundEdit() {
    if (compoundEdit == null) {
      compoundEdit = new CompoundEdit();
    }
  }

  /**
   * See beginCompoundEdit().
   */
  public void endCompoundEdit() {
    if (compoundEdit != null) {
      compoundEdit.end();
      if (compoundEdit.canUndo()) {
        undo.addEdit(compoundEdit);
      }
      compoundEdit = null;
    }
  }

  /**
   * Return the result of a string search.
   *
   * @param searchStr The string to be found
   * @param start The search's start offset
   * @param ignoreCase Set to true, we'll ignore the text case
   * @return True if <code>searchStr</code> has been found, false otherwise
   */
  @Override
  public boolean find(String searchStr, int start, boolean ignoreCase) {
    try {
      if (searchStr == null || searchStr.equals("")) {
        return false;
      }

      RE regexp = null;
      try {
        regexp = new RE(searchStr, (ignoreCase == true ? RE.REG_ICASE : 0) | RE.REG_MULTILINE, RESyntax.RE_SYNTAX_PERL5);
      } catch (Exception ex) {
        FuLog.warning(ex);
      }

      if (regexp == null) {
        getToolkit().beep();
        return false;
      }

      String text = getText(start, getLength() - start);
      REMatch match = regexp.getMatch(text);
      if (match != null) {
        select(start + match.getStartIndex(), start + match.getEndIndex());
        return true;
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    return false;
  }

  @Override
  public boolean replace(String searchStr, String replaceStr, int start, int end, boolean ignoreCase) {
    boolean r = false;
    try {
      r = replaceAll(searchStr, replaceStr, start, end, ignoreCase);
    } catch (Exception ex) {
      FuLog.warning(ex);
    }
    return r;
  }

  /**
   * Return the result of a string replace.
   *
   * @param _searchStr The string to be found
   * @param _replaceStr The string which will replace <code>searchStr</code>
   * @param _start The search's start offset
   * @param _end The search's end offset
   * @param _ignoreCase Set to true, we'll ignore the text case
   * @return True if the replace has been successfully done, false otherwise
   */
  public boolean replaceAll(String _searchStr, String _replaceStr, int _start, int _end, boolean _ignoreCase)
      throws REException {
    boolean found = false;

    try {
      if (_searchStr == null || _searchStr.equals("")) {
        return false;
      }

      RE regexp = null;
      try {
        regexp = new RE(_searchStr, (_ignoreCase == true ? RE.REG_ICASE : 0) | RE.REG_MULTILINE,
            RESyntax.RE_SYNTAX_PERL5);
      } catch (Exception ex) {
        FuLog.warning(ex);
      }
      if (regexp == null) {
        getToolkit().beep();
        return false;
      }
      String replaceStr = _replaceStr;

      if (replaceStr == null) {
        replaceStr = "";
      }

      beginCompoundEdit();
      int end = _end;
      Element map = getDocument().getDefaultRootElement();
      int startLine = map.getElementIndex(_start);
      int endLine = map.getElementIndex(end);

      for (int i = startLine; i <= endLine; i++) {
        Element lineElement = map.getElement(i);
        int lineStart;
        int lineEnd;

        if (i == startLine) {
          lineStart = _start;
        } else {
          lineStart = lineElement.getStartOffset();
        }
        if (i == endLine) {
          lineEnd = end;
        } else {
          lineEnd = lineElement.getEndOffset() - 1;
        }

        lineEnd -= lineStart;
        String line = getText(lineStart, lineEnd);
        String newLine = regexp.substituteAll(line, replaceStr);
        if (line.equals(newLine)) {
          continue;
        }
        getDocument().remove(lineStart, lineEnd);
        getDocument().insertString(lineStart, newLine, null);

        end += (newLine.length() - lineEnd);
        found = true;
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    endCompoundEdit();
    return found;
  }

  /**
   * When an undoable event is fired, we add it to the undo/redo list.
   */
  @Override
  public void undoableEditHappened(UndoableEditEvent e) {
    if (!getOperation()) {
      if (compoundEdit == null) {
        undo.addEdit(e.getEdit());
      } else {
        compoundEdit.addEdit(e.getEdit());
      }
    }
  }

  /**
   * When a modification is made in the text, we turn the 'to_be_saved' flag to true.
   */
  @Override
  public void changedUpdate(DocumentEvent e) {
    if (!getOperation() && !isDirty()) {
      setDirty();
    }
  }

  /**
   * When a modification is made in the text, we turn the 'to_be_saved' flag to true.
   */
  @Override
  public void insertUpdate(DocumentEvent e) {
    if (!getOperation() && !isDirty()) {
      setDirty();
    }
  }

  /**
   * When a modification is made in the text, we turn the 'to_be_saved' flag to true.
   */
  @Override
  public void removeUpdate(DocumentEvent e) {
    if (!getOperation() && !isDirty()) {
      setDirty();
    }
  }

  /**
   * Return a String representation of this object.
   */
  public String toString() {
    return "BuTextArea: " +
        "[filesize: " + getLength() + "] -" +
        "[anchor: " + getAnchor() + "] -" +
        " [font-name: " + getFontName() + ";" +
        " font-style: " + getFontStyle() + ";" +
        " font-size: " + getFontSize() + "]";
  }

  public static void main(String argv[]) {
    JFrame frame = new JFrame("Test BuTextArea");
    JComponent content = (JComponent) frame.getContentPane();
    BuTextArea ta1 = new BuTextArea();
    BuTextArea ta2 = new BuTextArea();
    ta2.setDocument(ta1.getDocument());

    content.setLayout(new GridLayout(1, 2));
    content.add(ta1);
    content.add(ta2);
    frame.pack();
    frame.setVisible(true);
    frame.setLocation(200, 200);
  }
}
