/**
 * @modification $Date: 2006-09-19 14:35:08 $
 * @statut unstable
 * @file BuContentFrame.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2004 Guillaume Desnoix
 */
package com.memoire.bu;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

/**
 * A frame to display an internal frame.
 */
public class BuContentFrame
    extends JFrame
    implements PropertyChangeListener {
  private BuInternalFrame content_;

  public BuContentFrame(BuInternalFrame _content) {
    content_ = _content;

    setContentPane(content_.getContentPane());
    setTitle(content_.getTitle());

    content_.addPropertyChangeListener(this);
    pack();
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    //System.err.println(_evt);

    if ("closed".equals(_evt.getPropertyName())) {
      if (Boolean.TRUE.equals(_evt.getNewValue())) {
        setVisible(false);
      }
    }
  }

  @Override
  public void show() {
    try {
      content_.setClosed(false);
    } catch (PropertyVetoException ex) {
    }
    super.show();
  }
}
