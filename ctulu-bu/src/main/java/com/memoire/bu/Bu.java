/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         Bu.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

public final class Bu
{
  public static final boolean TRACE=false;//true;
  public static final boolean DEBUG=false;//true;

  private Bu() { }
}
