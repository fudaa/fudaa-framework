/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.memoire.bu;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Un panneau contenant des composants pr�f�rences. Les composants sont globalement
 * enregistrables, s'il n'y a pas d'erreur dessus (ils sont dans ce cas "savabled").
 *
 * @author marchand@deltacad.fr
 */
public class BuContainerPreferencesPanel extends BuAbstractPreferencesPanel {
  private BuAbstractPreferencesComponent[] cmps_;
  private String category_;
  private String title_;
  private boolean isDirty_=false;
  
  /**
   * Construction d'un panneau avec cat�gorie par d�faut.
   * @param _title Le titre du panneau
   * @param _cmps Les composants du panneau.
   */
  public BuContainerPreferencesPanel(String _title, BuAbstractPreferencesComponent[] _cmps) {
    this(null,_title,_cmps);
  }
  
  public BuContainerPreferencesPanel(String _category, String _title, BuAbstractPreferencesComponent[] _cmps) {
    cmps_=_cmps;
    category_=_category;
    title_=_title;
    
    setLayout(new BuVerticalLayout(5,true,false));
    
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      cmp.setContainer(this);
      
      JPanel pn=new JPanel();
      pn.setLayout(new BorderLayout());
      pn.setBorder(new CompoundBorder(
        new TitledBorder(cmp.getTitle()),
        new EmptyBorder(5, 5, 5, 5)));
      pn.add(cmp,BorderLayout.CENTER);
      add(pn);
    }
  }
  
  @Override
  public String getCategory() {
    if (category_==null) {
      return super.getCategory();
    }
    return category_;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public boolean isPreferencesValidable() { 
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      if (!cmp.isPreferencesValidable()) return false;
    }
    return true; 
  }
  
  @Override
  public void validatePreferences() {
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      cmp.validatePreferences();
    }
  }

  @Override
  public boolean isPreferencesApplyable() { 
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      if (!cmp.isPreferencesApplyable()) return false;
    }
    return true; 
  }
  
  @Override
  public void applyPreferences() {
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      cmp.applyPreferences();
    }
  }

  @Override
  public boolean isPreferencesCancelable() {
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      if (!cmp.isPreferencesCancelable()) return false;
    }
    return true; 
  }
  
  @Override
  public void cancelPreferences() {
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      cmp.cancelPreferences();
    }
  }

  @Override
  public boolean isDirty() { 
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      if (!cmp.isSavabled()) {
        isDirty_=false;
        return isDirty_;
      }
    }
    for (BuAbstractPreferencesComponent cmp : cmps_) {
      if (cmp.isModified()) {
        isDirty_=true;
        return isDirty_;
      }
    }
    
    isDirty_=false;
    return isDirty_; 
  }
  
  /** Ne fait rien, l'�tat dirty est determin� par les composants sous jacents */
  @Override
  public void setDirty(boolean _b) {
  }
  
  public void firePropertyChange() {
    boolean oldDirty=isDirty_;
    if (isDirty()!=oldDirty)
    {
      firePropertyChange("dirty",oldDirty,isDirty_);
    }
  }
}
