/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuComposedIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.SwingConstants;

/**
 * An icon composer which overlays other icons.
 */

public class BuComposedIcon
  implements Icon, SwingConstants
{
  private static final Icon[] ICON0=new Icon[0];

  private Icon[] icons_;
  private int    halign_,valign_;

  public BuComposedIcon(Icon _icon0, Icon _icon1)
  {
    this(_icon0,_icon1,LEFT,TOP);
  }

  public BuComposedIcon(Icon _icon0, Icon _icon1, int _halign, int _valign)
  {
    icons_=new Icon[2];
    icons_[0]=_icon0;
    icons_[1]=_icon1;
    halign_  =_halign;
    valign_  =_valign;
  }

  public BuComposedIcon(Icon[] _icons)
  {
    icons_=_icons==null?ICON0:_icons;
  }

  public int getHorizontalAlignement()
  {
    return halign_;
  }

  public void setHorizontalAlignement(int _halign)
  {
    halign_=_halign;
  }

  public int getVerticalAlignement()
  {
    return valign_;
  }

  public void setVerticalAlignement(int _valign)
  {
    valign_=_valign;
  }

  @Override
  public int getIconHeight()
  {
    int h=0;
    for(int i=0;i<icons_.length;i++)
      h=Math.max(h,icons_[i].getIconHeight());
    return h;
  }

  @Override
  public int getIconWidth()
  {
    int w=0;
    for(int i=0;i<icons_.length;i++)
      w=Math.max(w,icons_[i].getIconWidth());
    return w;
  }
                
  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    int w=getIconWidth();
    int h=getIconHeight();

    for(int i=0;i<icons_.length;i++)
    {
      int dx=0;
      switch(halign_)
	{
	case CENTER: dx=(w-icons_[i].getIconWidth())/2; break;
	case RIGHT:  dx=w-icons_[i].getIconWidth(); break;
	}
      int dy=0;
      switch(valign_)
	{
	case CENTER: dy=(h-icons_[i].getIconHeight())/2; break;
	case BOTTOM: dy=h-icons_[i].getIconHeight(); break;
	}
      icons_[i].paintIcon(_c,_g,_x+dx,_y+dy);
    }
  }
}
