/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut unstable
 * @file BuAssistant.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;

/**
 * An always-happy assistant.
 */
public class BuAssistant
    extends JComponent
    implements ActionListener, FocusListener,
    InternalFrameListener, MouseListener
    //, KeyListener
{
  public static final int PAROLE = 0;
  public static final int ATTENTE = 1;
  public static final int PEUR = 2;
  public static final int COLERE = 3;
  public static final int ABSENT = 4;
  public static final int MAX_ATTITUDES = 5;
  private static final int DELAI = 100;
  private static final int AINC = 20;
  private static final int NBCOL = 6;
  private Hashtable table_ = new Hashtable();

  protected String getString(String _s) {
    return BuResource.BU.getString(_s);
  }

  protected void init() {
    // temporaire
    loadTips("assistance.txt");

    String l = Locale.getDefault().getLanguage();
    if (!"fr".equals(l)) {
      loadTips("assistance_" + l + ".txt");
    }
  }

  protected void loadTips(String _fichier) {
    loadTips(getClass(), _fichier);
  }

  private void loadTips(Class _classe, String _fichier) {
    if (_classe == null) {
      return;
    }
    if ((_classe != BuAssistant.class)) {
      loadTips(_classe.getSuperclass(), _fichier);
    }

    try (LineNumberReader rin =
        new LineNumberReader
            (new InputStreamReader
                (new BufferedInputStream
                    (_classe.getResourceAsStream(_fichier))))) {

      String k, l, v;

      k = null;
      v = null;

      while (rin.ready()) {
        l = rin.readLine();
        if (l == null) {
          break;
        }

        if (l.equals("") && (k != null) && (v != null)) {
          table_.put(k, v);
          k = null;
          v = null;
        } else if (l.startsWith("#")) {
          ;
        } else if (l.equals("")) {
          ;
        } else if (k == null) {
          k = l;
        } else if (v == null) {
          v = l;
        } else {
          v += "\n" + l;
        }
      }

      if ((k != null) && (v != null)) {
        table_.put(k, v);
      }
    } catch (Exception ex) {
      FuLog.warning(ex);
    }
  }

  protected int aval_;
  protected Blinker blinker_;
  protected Thread festival_;

  class Point {
    int x_, y_, r_, v_, b_;

    Point(int _x, int _y, Color _c) {
      x_ = _x;
      y_ = _y;
      r_ = _c.getRed();
      v_ = _c.getGreen();
      b_ = _c.getBlue();
    }

    Point(int _x, int _y, int _r, int _v, int _b) {
      x_ = _x;
      y_ = _y;
      r_ = _r;
      v_ = _v;
      b_ = _b;
    }

    Point(int _a, Point _p1, Point _p2) {
      x_ = _p1.x_ + (_p2.x_ - _p1.x_) * _a / 100;
      y_ = _p1.y_ + (_p2.y_ - _p1.y_) * _a / 100;
      r_ = _p1.r_ + (_p2.r_ - _p1.r_) * _a / 100;
      v_ = _p1.v_ + (_p2.v_ - _p1.v_) * _a / 100;
      b_ = _p1.b_ + (_p2.b_ - _p1.b_) * _a / 100;
    }
  }

  class Triangle {
    Point[] p_;

    Triangle(Point _p0, Point _p1, Point _p2) {
      p_ = new Point[3];
      p_[0] = _p0;
      p_[1] = _p1;
      p_[2] = _p2;
    }

    Triangle(int _a, Triangle _t1, Triangle _t2) {
      p_ = new Point[3];

      for (int i = 0; i < 3; i++)
        p_[i] = new Point(_a, _t1.p_[i], _t2.p_[i]);
    }

    void draw(Graphics _g) {
      int[] x = new int[]{p_[0].x_, p_[1].x_, p_[2].x_};
      int[] y = new int[]{p_[0].y_, p_[1].y_, p_[2].y_};
      int r = (p_[0].r_ + p_[1].r_ + p_[2].r_) / 3;
      int v = (p_[0].v_ + p_[1].v_ + p_[2].v_) / 3;
      int b = (p_[0].b_ + p_[1].b_ + p_[2].b_) / 3;

      BuLib.setColor(_g, new Color(r, v, b));
      _g.fillPolygon(x, y, 3);
    }
  }

  class Disque {
    Point p_;
    int r_;

    Disque(Point _p, int _r) {
      p_ = _p;
      r_ = _r;
    }

    Disque(int _a, Disque _d1, Disque _d2) {
      p_ = new Point(_a, _d1.p_, _d2.p_);
      r_ = _d1.r_ + (_d2.r_ - _d1.r_) * _a / 100;
    }

    void draw(Graphics graphics) {
      int r, v, b;

      for (int i = NBCOL; i > 0; i--) {
        r = p_.r_ - 64 + 64 * i / NBCOL;
        v = p_.v_ - 64 + 64 * i / NBCOL;
        b = p_.b_ - 64 + 64 * i / NBCOL;

        if (r < 0) {
          r = 0;
        }
        if (r > 255) {
          r = 255;
        }
        if (v < 0) {
          v = 0;
        }
        if (v > 255) {
          v = 255;
        }
        if (b < 0) {
          b = 0;
        }
        if (b > 255) {
          b = 255;
        }

        BuLib.setColor(graphics, new Color(r, v, b));
        graphics.fillOval(p_.x_ - r_ * i / NBCOL, p_.y_ - r_ * i / NBCOL, 2 * r_ * i / NBCOL, 2 * r_ * i / NBCOL);
      }
    }
  }

  class Face {
    Disque tete_;
    Triangle bouche_;
    Disque oeilGauche_, oeilDroit_;
    Disque pupilleGauche_, pupilleDroit_;
    Triangle sourcilGauche_, sourcilDroit_;

    Face() {
    }

    Face(int a, Face face1, Face face2) {
      tete_ = new Disque(a, face1.tete_, face2.tete_);
      bouche_ = new Triangle(a, face1.bouche_, face2.bouche_);
      oeilDroit_ = new Disque(a, face1.oeilDroit_, face2.oeilDroit_);
      oeilGauche_ = new Disque(a, face1.oeilGauche_, face2.oeilGauche_);
      pupilleDroit_ = new Disque(a, face1.pupilleDroit_, face2.pupilleDroit_);
      pupilleGauche_ = new Disque(a, face1.pupilleGauche_, face2.pupilleGauche_);
      sourcilDroit_ = new Triangle(a, face1.sourcilDroit_, face2.sourcilDroit_);
      sourcilGauche_ = new Triangle(a, face1.sourcilGauche_, face2.sourcilGauche_);
    }

    void draw(Graphics _g) {
      tete_.draw(_g);
      bouche_.draw(_g);
      oeilGauche_.draw(_g);
      oeilDroit_.draw(_g);
      pupilleGauche_.draw(_g);
      pupilleDroit_.draw(_g);
      sourcilGauche_.draw(_g);
      sourcilDroit_.draw(_g);
    }
  }

  Face faceActuelle_, faceAvant_, faceApres_;
  Face[] faces_ = new Face[MAX_ATTITUDES];

  public BuAssistant() {
    super();
    init();

    // F0
    {
      faces_[0] = new Face();

      Point p00 = new Point(50, 50, 255, 192, 192);
      faces_[0].tete_ = new Disque(p00, 45);

      Point p01 = new Point(25, 65, 255, 120, 120);
      Point p02 = new Point(75, 60, 255, 120, 120);
      Point p03 = new Point(52, 80, 192, 30, 30);
      faces_[0].bouche_ = new Triangle(p01, p02, p03);

      Point p04 = new Point(15, 25, 128, 128, 128);
      Point p05 = new Point(40, 15, 80, 80, 80);
      Point p06 = new Point(45, 20, 100, 100, 100);
      faces_[0].sourcilGauche_ = new Triangle(p04, p05, p06);

      Point p07 = new Point(85, 25, 128, 128, 128);
      Point p08 = new Point(60, 15, 80, 80, 80);
      Point p09 = new Point(55, 20, 100, 100, 100);
      faces_[0].sourcilDroit_ = new Triangle(p07, p08, p09);

      Point p10 = new Point(35, 40, 255, 255, 255);
      faces_[0].oeilGauche_ = new Disque(p10, 12);

      Point p11 = new Point(65, 40, 255, 255, 255);
      faces_[0].oeilDroit_ = new Disque(p11, 12);

      Point p12 = new Point(35, 35, 128, 64, 0);
      faces_[0].pupilleGauche_ = new Disque(p12, 7);

      Point p13 = new Point(65, 35, 128, 64, 0);
      faces_[0].pupilleDroit_ = new Disque(p13, 7);
    }
    // F1
    {
      faces_[1] = new Face();

      Point p00 = new Point(50, 50, 255, 198, 198);
      faces_[1].tete_ = new Disque(p00, 42);

      Point p01 = new Point(25, 65, 255, 120, 120);
      Point p02 = new Point(75, 65, 255, 120, 120);
      Point p03 = new Point(50, 75, 192, 30, 30);
      faces_[1].bouche_ = new Triangle(p01, p02, p03);

      Point p04 = new Point(15, 20, 128, 128, 128);
      Point p05 = new Point(35, 10, 80, 80, 80);
      Point p06 = new Point(40, 15, 100, 100, 100);
      faces_[1].sourcilGauche_ = new Triangle(p04, p05, p06);

      Point p07 = new Point(85, 20, 128, 128, 128);
      Point p08 = new Point(65, 10, 80, 80, 80);
      Point p09 = new Point(60, 15, 100, 100, 100);
      faces_[1].sourcilDroit_ = new Triangle(p07, p08, p09);

      Point p10 = new Point(36, 41, 255, 255, 255);
      faces_[1].oeilGauche_ = new Disque(p10, 11);

      Point p11 = new Point(66, 41, 255, 255, 255);
      faces_[1].oeilDroit_ = new Disque(p11, 11);

      Point p12 = new Point(37, 40, 128, 64, 0);
      faces_[1].pupilleGauche_ = new Disque(p12, 7);

      Point p13 = new Point(67, 40, 128, 64, 0);
      faces_[1].pupilleDroit_ = new Disque(p13, 7);
    }
    // F2
    {
      faces_[2] = new Face();

      Point p00 = new Point(50, 50, 255, 64, 64);
      faces_[2].tete_ = new Disque(p00, 34);

      Point p01 = new Point(35, 60, 128, 80, 80);
      Point p02 = new Point(65, 60, 128, 80, 80);
      Point p03 = new Point(57, 65, 64, 50, 50);
      faces_[2].bouche_ = new Triangle(p01, p02, p03);

      Point p04 = new Point(25, 20, 128, 128, 128);
      Point p05 = new Point(40, 25, 80, 80, 80);
      Point p06 = new Point(45, 35, 100, 100, 100);
      faces_[2].sourcilGauche_ = new Triangle(p04, p05, p06);

      Point p07 = new Point(75, 20, 128, 128, 128);
      Point p08 = new Point(60, 25, 80, 80, 80);
      Point p09 = new Point(55, 35, 100, 100, 100);
      faces_[2].sourcilDroit_ = new Triangle(p07, p08, p09);

      Point p10 = new Point(45, 44, 255, 255, 255);
      faces_[2].oeilGauche_ = new Disque(p10, 11);

      Point p11 = new Point(55, 44, 255, 255, 255);
      faces_[2].oeilDroit_ = new Disque(p11, 11);

      Point p12 = new Point(45, 45, 128, 64, 0);
      faces_[2].pupilleGauche_ = new Disque(p12, 5);

      Point p13 = new Point(55, 45, 128, 64, 0);
      faces_[2].pupilleDroit_ = new Disque(p13, 5);
    }
    // F3
    {
      faces_[3] = new Face();

      Point p00 = new Point(50, 50, 128, 192, 64);
      faces_[3].tete_ = new Disque(p00, 54);

      Point p01 = new Point(35, 70, 128, 80, 80);
      Point p02 = new Point(65, 70, 128, 80, 80);
      Point p03 = new Point(50, 90, 64, 50, 50);
      faces_[3].bouche_ = new Triangle(p01, p02, p03);

      Point p04 = new Point(25, 20, 128, 128, 128);
      Point p05 = new Point(40, 20, 80, 80, 80);
      Point p06 = new Point(45, 25, 100, 100, 100);
      faces_[3].sourcilGauche_ = new Triangle(p04, p05, p06);

      Point p07 = new Point(75, 20, 128, 128, 128);
      Point p08 = new Point(60, 20, 80, 80, 80);
      Point p09 = new Point(55, 25, 100, 100, 100);
      faces_[3].sourcilDroit_ = new Triangle(p07, p08, p09);

      Point p10 = new Point(37, 44, 255, 255, 255);
      faces_[3].oeilGauche_ = new Disque(p10, 16);

      Point p11 = new Point(62, 44, 255, 255, 255);
      faces_[3].oeilDroit_ = new Disque(p11, 16);

      Point p12 = new Point(37, 45, 128, 64, 0);
      faces_[3].pupilleGauche_ = new Disque(p12, 12);

      Point p13 = new Point(62, 45, 128, 64, 0);
      faces_[3].pupilleDroit_ = new Disque(p13, 12);
    }
    // F4

    faces_[4] = new Face();

    Point p00 = new Point(0, 0, 255, 224, 224);
    faces_[4].tete_ = new Disque(p00, 0);

    Point p01 = new Point(0, 0, 255, 152, 152);
    Point p02 = new Point(0, 0, 255, 152, 152);
    Point p03 = new Point(0, 0, 224, 62, 62);
    faces_[4].bouche_ = new Triangle(p01, p02, p03);

    Point p04 = new Point(0, 0, 160, 160, 160);
    Point p05 = new Point(0, 0, 112, 112, 112);
    Point p06 = new Point(0, 0, 132, 132, 132);
    faces_[4].sourcilGauche_ = new Triangle(p04, p05, p06);

    Point p07 = new Point(0, 0, 160, 160, 160);
    Point p08 = new Point(0, 0, 112, 112, 112);
    Point p09 = new Point(0, 0, 132, 132, 132);
    faces_[4].sourcilDroit_ = new Triangle(p07, p08, p09);

    Point p10 = new Point(0, 0, 255, 255, 255);
    faces_[4].oeilGauche_ = new Disque(p10, 0);

    Point p11 = new Point(0, 0, 255, 255, 255);
    faces_[4].oeilDroit_ = new Disque(p11, 0);

    Point p12 = new Point(0, 0, 160, 96, 0);
    faces_[4].pupilleGauche_ = new Disque(p12, 0);

    Point p13 = new Point(0, 0, 160, 96, 0);
    faces_[4].pupilleDroit_ = new Disque(p13, 0);

    face_ = PAROLE;
    message_ = new String[]
        {getString("Bonjour") + " !"};

    aval_ = 100;
    blinker_ = null;

    faceActuelle_ = faces_[face_];
    faceAvant_ = faces_[face_];
    faceApres_ = faces_[face_];

    setName("buASSISTANT");
    updateUI();
    setOpaque(false);

    Dimension ps = new Dimension(150, 120);
    if (BuPreferences.BU.getBooleanProperty("assistant.speech", false)) {
      ps.width = 100;
      ps.height = 100;
    }
    setPreferredSize(ps);
    setSize(ps);

    addMouseListener(this);
  }

  @Override
  public void updateUI() {
    super.updateUI();

    Font ft = BuLib.deriveFont("Label", Font.PLAIN, -2);
    if ("dialog.bold".equals(ft.getFamily())) {
      ft = new Font("SansSerif", Font.PLAIN, ft.getSize());
    }
    setFont(ft);
  }

  @Override
  public void mouseClicked(MouseEvent mouseEvent) {
  }

  @Override
  public void mouseEntered(MouseEvent mouseEvent) {
    JComponent source = (JComponent) mouseEvent.getSource();

    if (source == this) {
      changeAttitude
          (PAROLE, getString("Que puis-je pour vous ?"));
    } else {
      String info = source.getName();
      if (info == null) {
        info = "";
      }
      if (!"".equals(info)) {
        commentUser(info + "_ENTER", getDefaultText(source));
      }
    }
  }

  @Override
  public void mouseExited(MouseEvent mouseEvent) {
    JComponent source = (JComponent) mouseEvent.getSource();

    if (source == this) {
      changeAttitude(ATTENTE, "");
    }
  }

  @Override
  public void mousePressed(MouseEvent _evt) {
    JComponent source = (JComponent) _evt.getSource();

    if (source == this) {
      changeAttitude(COLERE, "Aie !");
    }
  }

  @Override
  public void mouseReleased(MouseEvent _evt) {
    JComponent source = (JComponent) _evt.getSource();

    if (source == this) {
      changeAttitude(ATTENTE, "");
    }
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  // Paint

  @Override
  public synchronized void paintComponent(Graphics _g) {
    Graphics g = _g;
    // catch temporaire.
    try {
      super.paintComponent(g);
    } catch (NullPointerException ex) {
    }

    Dimension d = getSize();
    Insets insets = getInsets();

    faceActuelle_.draw(g);

    if (!BuPreferences.BU.getBooleanProperty("assistant.speech", false)) {
      if (message_ != null) {
        Font font = getFont();
        FontMetrics fm = g.getFontMetrics(font);
        int ws = 0;
        int hs = 0;
        int hs1 = fm.getAscent() + fm.getDescent();
        Color fg = getForeground();
        Color bg = getBackground().brighter();

        int i, l;
        l = message_.length;
        hs = hs1 * l;
        for (i = 0; i < l; i++)
          ws = Math.max(ws, fm.stringWidth(message_[i]));

        int x = d.width - insets.right - ws - 4;
        int y = d.height - insets.bottom - hs - 4;

        if (!getUseCommentWindow()
            || ((x > hs1 + insets.left) && (y > insets.top))) {
          if (getUseCommentWindow()) {
            getCommentWindow().setVisible(false);
          }
          BuLib.setColor(g, bg);
          g.fillOval(x - hs1, y, 2 * hs1, 2 * hs1);
          BuLib.setColor(g, fg);
          g.drawOval(x - hs1, y, 2 * hs1, 2 * hs1);
          BuLib.setColor(g, bg);
          g.fillRect(x, y, d.width - x, d.height - y);
          g.fillRect(x - hs1, y + hs1, d.width - x + hs1, d.height - y - hs1);
          BuLib.setColor(g, fg);
          g.drawLine(x, y, d.width - 1, y);
          g.drawLine(x - hs1, y + hs1, x - hs1, d.height - 1);
          g.drawLine(d.width - 1, y, d.width - 1, d.height - 1);
          g.drawLine(x - hs1, d.height - 1, d.width - 1, d.height - 1);
        } else {
          JWindow win = getCommentWindow();

          win.setBackground(bg);
          win.setForeground(fg);
          java.awt.Point p = getLocationOnScreen();
          p.x += d.width - ws - 8;
          p.y += d.height - hs - 6;
          if (p.x < 0) {
            p.x = 0;
          }
          if (p.y < 0) {
            p.y = 0;
          }
          d.width = ws + 8;
          d.height = hs + 6;
          win.setSize(d);
          win.setLocation(p);

          win.setVisible(true);

          g = win.getGraphics();
          BuLib.setColor(g, bg);
          g.fillRect(0, 0, d.width, d.height);
          BuLib.setColor(g, fg);
          g.drawRect(0, 0, d.width - 1, d.height - 1);

          x = 4;
          y = 2;
        }

        g.setFont(font);
        for (i = 0; i < l; i++) {
          y += hs1;
          g.drawString(message_[i], x, y);
        }
      } else if (getUseCommentWindow()) {
        getCommentWindow().setVisible(false);
      }
    }
  }

  // Evenements

  private static String logfileName() {
    String f = "assistance.log";
    f = FuLib.getUserHome() + File.separator + f;
    return f;
  }

  private PrintWriter log_ = null;

  @Override
  public void actionPerformed(ActionEvent _evt) {
    Object source = _evt.getSource();
    String info = null;

    if (source instanceof JComponent) {
      info = ((JComponent) source).getName();
    }

    if (info == null) {
      info = "";
    }

    if (log_ == null && isLog_) {
      try {
        String f = logfileName();
        // System.err.println("log: opening "+f);
        log_ = new PrintWriter(new FileWriter(f, true), true);
      } catch (Throwable th) {
        FuLog.warning(th);
      }
    }

    if (log_ != null && isLog_) {
      String t = _evt.getActionCommand();
      if (source instanceof JCheckBox) {
        t += "(" + (((JCheckBox) source).isSelected() ? "VRAI" : "FAUX") + ")";
      }
      if (source instanceof JComboBox) {
        t += "(" + ((JComboBox) source).getSelectedItem() + ")";
      }
      if (source instanceof JList) {
        t += "(" + ((JList) source).getSelectedValue() + ")";
      }
      log_.println(t);
    }

    try {
      if (source instanceof JCheckBox) {
        info += "_" + (((JCheckBox) source).isSelected() ? "VRAI" : "FAUX");
      }
      if (source instanceof JComboBox) {
        info += "_" + ((JComboBox) source).getSelectedItem()
            .toString().toUpperCase();
      }
      if (source instanceof JList) {
        info += "_" + ((JList) source).getSelectedValue()
            .toString().toUpperCase();
      }
    } catch (Exception ex) {
      FuLog.warning(ex);
    }

    if (!info.equals("")) {
      commentUser(info + "_ACTION", getDefaultText(source));
    }
  }

  @Override
  public void focusGained(FocusEvent _evt) {
    JComponent source = (JComponent) _evt.getSource();
    String info = "";
    int essai = 0;

    while (essai < 3) {
      info = source.getName();

      if (info == null) {
        info = "";
      }
      if (info.equals("")) {
        essai++;
        source = (JComponent) source.getParent();
        if (source == null) {
          break;
        }
      } else {
        break;
      }
    }

    if (!"".equals(info)) {
      commentUser(info + "_FOCUS", getDefaultText(source));
    }
  }

  @Override
  public void focusLost(FocusEvent _evt) {
  }

  // InternalFrame

  private void internalFrameAction(InternalFrameEvent _evt) {
    int id = _evt.getID();
    String action = null;
    switch (id) {
      case InternalFrameEvent.INTERNAL_FRAME_ACTIVATED:
        action = "FILLE_ACTIVER";
        break;
      case InternalFrameEvent.INTERNAL_FRAME_CLOSED:
        action = "FILLE_FERMER";
        break;
      case InternalFrameEvent.INTERNAL_FRAME_ICONIFIED:
        action = "FILLE_ICONIFIER";
        break;
      case InternalFrameEvent.INTERNAL_FRAME_DEICONIFIED:
        action = "FILLE_DEICONIFIER";
        break;
    }
    String frame = ((JInternalFrame) _evt.getSource()).getName();
    if ((action != null) && (frame != null)) {
      if (frame.startsWith("if")) {
        frame = frame.substring(2);
      }
      actionPerformed(new ActionEvent
          (this, ActionEvent.ACTION_PERFORMED,
              action + "(" + frame + ")"));
    }
  }

  @Override
  public void internalFrameActivated(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  @Override
  public void internalFrameClosed(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  @Override
  public void internalFrameDeiconified(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  @Override
  public void internalFrameIconified(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  @Override
  public void internalFrameOpened(InternalFrameEvent _evt) {
    internalFrameAction(_evt);
  }

  protected String getDefaultText(Object _o) {
    String r = null;
    if (_o instanceof JComponent) {
      r = ((JComponent) _o).getToolTipText();
    }
    return r;
  }

  public void commentUser(String _info) {
    commentUser(_info, null);
  }

  public void commentUser(String _info, String _default) {

    String info;
    Object o;
    int i;

    info = BuLib.simplifyComponentName(_info);
    o = table_.get(info);

    if (o == null) {
      do {
        i = info.lastIndexOf('_');
        if (i < 0) {
          break;
        }

        info = info.substring(0, i);
        o = table_.get(info);
      }
      while ((i >= 0) && (o == null));
    }

    if (o == null) {
      o = _default;
    }

    if (o != null) {
      changeAttitude(PAROLE, o.toString());
    }
  }

  // Thread

  public void start() {
    if (blinker_ != null) {
      blinker_.exitBlinker();
    }

    blinker_ = new Blinker();
    blinker_.start();
  }

  public void stop() {
    if (blinker_ != null) {
      blinker_.exitBlinker();
      blinker_ = null;
    }
  }

  private final class Blinker extends Thread {
    private boolean exited_;

    public Blinker() {
      super("Bu assistant");
      setPriority(Thread.MIN_PRIORITY);
      exited_ = false;
    }

    public void exitBlinker() {
      exited_ = true;
    }

    @Override
    public void run() {
      while (!exited_) {
        aval_ = 0;
        while (aval_ <= 100) {
          long avant = System.currentTimeMillis();

          if (exited_) {
            break;
          }
          faceActuelle_ = new Face(aval_ % 101, faceAvant_, faceApres_);
          BuUpdateGUI.repaintNow(BuAssistant.this);
          long apres = System.currentTimeMillis();

          long d = avant + DELAI - apres;
          if (d > 40) {
            try {
              Thread.sleep(d);
            } catch (InterruptedException ex) {
              Thread.currentThread().interrupt();
              FuLog.warning(ex);
            }
          }

          aval_ += AINC;
        }
        if (exited_) {
          break;
        }

        faceActuelle_ = faceApres_;
        faceAvant_ = faceApres_;

        if (face_ != ATTENTE)//&&(face_!=ABSENT))
        {
          try {
            Thread.sleep(5000);
          } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            FuLog.warning(ex);
          }
          setMessage("");
          setFace(ATTENTE);
        } else if ("".equals(getMessage()) && (Math.random() < 0.20)) {
          try {
            Thread.sleep(5000);
          } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            FuLog.warning(ex);
          }
          setMessage("Zzz...");
          BuUpdateGUI.repaintNow(BuAssistant.this);
          try {
            Thread.sleep(2000);
          } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            FuLog.warning(ex);
          }
          setMessage("");
          BuUpdateGUI.repaintNow(BuAssistant.this);
          exited_ = true;
        } else {
          exited_ = true;
        }
      }
    }
  }

  // Proprietes
  private String[] message_;

  public synchronized String getMessage() {
    String r = "";
    if (message_ != null) {
      for (int i = 0; i < message_.length; i++) {
        if (i > 0) {
          r += '\n';
        }
        r += message_[i];
      }
    }
    return r;
  }

  public synchronized void setMessage(String _v) {
    if (_v == null) {
      return;
    }

    String vp = getMessage();

    if (!vp.equals(_v)) {
      if (_v.equals("")) {
        message_ = null;
      } else {
        int i, j, m, n;

        n = 1;
        for (i = 0; i < _v.length(); i++)
          if (_v.charAt(i) == '\n') {
            n++;
          }

        message_ = new String[n];
        m = 0;
        j = 0;
        for (i = 0; i < _v.length(); i++)
          if (_v.charAt(i) == '\n') {
            message_[m] = _v.substring(j, i);
            m++;
            j = i + 1;
          }
        message_[m] = _v.substring(j);
      }

      firePropertyChange("message", _v, vp);

      if (BuPreferences.BU.getBooleanProperty("assistant.speech", false) &&
          (festival_ == null)) {
        try {
          try (PrintWriter wout = new PrintWriter(new FileWriter("/tmp/assistant.speech"))) {
            wout.print(_v);
          }
          String[] cmd = new String[]
              {
                  "/usr/bin/festival",
                  "--tts",
                  "/tmp/assistant.speech"
              };
          final Process p = Runtime.getRuntime().exec(cmd);

          festival_ = new Thread
              (() -> {
                try {
                  p.waitFor();
                } catch (InterruptedException ex) {
                  FuLog.warning(ex);
                  Thread.currentThread().interrupt();
                }
                festival_ = null;
              }
              );
          festival_.start();
        } catch (Exception ex) {
          FuLog.warning(ex);
        }
      }
    }
  }

  private boolean use_ = false; // TMP @GDX true

  public synchronized boolean getUseCommentWindow() {
    return use_;
  }

  public synchronized void setUseCommentWindow(boolean _use) {
    use_ = _use;
  }

  private boolean isLog_ = false;

  /**
   * Defines that something is write to assistance.log
   *
   * @param _b True : Log, false : No log.
   */
  public void setLog(boolean _b) {
    isLog_ = _b;
  }

  private JWindow window_;

  public synchronized JWindow getCommentWindow() {
    if (window_ == null) {
      window_ = new JWindow();
    }
    return window_;
  }

  public synchronized void setCommentWindow(JWindow _window) {
    window_ = _window;
  }

  int face_;

  public synchronized int getFace() {
    return face_;
  }

  public synchronized void setFace(int _v) {
    int v = _v;
    v %= faces_.length;

    if (face_ != v) {
      int vp = face_;
      face_ = v;
      faceApres_ = faces_[v];
      faceAvant_ = faceActuelle_;
      firePropertyChange("face", v, vp);
    }
  }

  @Override
  public void setVisible(boolean _v) {
    if (_v != isVisible()) {
      super.setVisible(_v);
      if (_v) {
        changeAttitude(ATTENTE, "Me revoil� !");
      } else {
        changeAttitude(ABSENT, "Au revoir !");
      }
    }
  }

  public void changeAttitude(int _f, String _m) {
    if (_f >= 0) {
      setFace(_f);
    }
    if (_m != null) {
      setMessage(_m);
    }
    if (isShowing()) {
      start();
    }
  }

  // Emitters

  public void addEmitters(Container _parent) {
    addSpecificEmitters(_parent, "Action");
    addSpecificEmitters(_parent, "Focus");
    addSpecificEmitters(_parent, "InternalFrame");
  }

  public void addSpecificEmitters(Container _parent, String _type) {
    for (Enumeration e = BuLib.getAllSubComponents(_parent).elements();
         e.hasMoreElements(); ) {
      Component o = (Component) e.nextElement();
      boolean b = (o.getName() != null);

      if (o instanceof BuMenuBar) {
        b = false;
      }

      if (b && _type.equals("Focus")) {
        b = (o.isFocusable());
        if (b && (o instanceof JComponent)) {
          b = ((JComponent) o).isRequestFocusEnabled();
        }
      }

      if (b) {
        // try to remove and add after, ensuring that there is only one.

        try {
          Class[] c = new Class[]{Class.forName("java.awt.event." + _type + "Listener")};
          Method m = o.getClass().getMethod("remove" + _type + "Listener", c);

          Object[] a = new Object[]{this};
          m.invoke(o, a);
        } catch (Exception ex) {
          FuLog.warning(ex);
        }

        try {
          Class[] c = new Class[]{Class.forName("java.awt.event." + _type + "Listener")};
          Method m = o.getClass().getMethod("add" + _type + "Listener", c);

          Object[] a = new Object[]{this};
          m.invoke(o, a);
        } catch (Exception ex) {
          FuLog.warning(ex);
        }

        try {
          Class[] c = new Class[]{Class.forName("javax.swing.event." + _type + "Listener")};
          Method m = o.getClass().getMethod("remove" + _type + "Listener", c);

          Object[] a = new Object[]{this};
          m.invoke(o, a);
        } catch (Exception ex) {
          FuLog.warning(ex);
        }

        try {
          Class[] c = new Class[]{Class.forName("javax.swing.event." + _type + "Listener")};
          Method m = o.getClass().getMethod("add" + _type + "Listener", c);

          Object[] a = new Object[]{this};
          m.invoke(o, a);
        } catch (Exception ex) {
          FuLog.error(ex);
        }
      }
    }
  }

  // Run Acto Script

  public void runActoScript(ActionListener _l) {
    String f = logfileName();

    try (LineNumberReader lnr = new LineNumberReader(new FileReader(f))) {
      while (true) {
        String l = lnr.readLine();
        if (l == null) {
          break;
        }
        setMessage(l);
        try {
          _l.actionPerformed(new ActionEvent
              (this, ActionEvent.ACTION_PERFORMED, l));
        } catch (Exception ex) {
        }
      }
    } catch (IOException ex) {
    }
  }
}
