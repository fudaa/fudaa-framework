/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuTableRowHeaderRenderer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

/**
 * A displayer for table cells, which darkens every second line and
 * accepts different formats for numbers and dates.
 */

public class BuTableRowHeaderRenderer
       extends JLabel implements ListCellRenderer
{
  private boolean letters_;

  public BuTableRowHeaderRenderer(boolean _letters)
  {
    letters_=_letters;
  }
  
  @Override
  public Component getListCellRendererComponent
    (JList _list, Object _value, int _index, boolean _selected,
     boolean _hasFocus) 
  {
    int index=_index;
    setOpaque(true);
    setHorizontalAlignment(letters_ ? CENTER : RIGHT);
    setBorder    (UIManager.getBorder("TableHeader.cellBorder"));
    setForeground(UIManager.getColor ("TableHeader.foreground"));
    setBackground(UIManager.getColor ("TableHeader.background"));
    setFont      (UIManager.getFont  ("TableHeader.font"));

    String text; // .=((_value == null) ? "" : _value.toString());
    if(letters_)
    {
      text=(char)('A'+index%26)+" ";
      while(index>=26)
      {
	index=index/26-1;
	text=(char)('A'+index%26)+text;
      }
    }
    else text=index+" ";

    setText(text);

    if(_selected)
    {
      setForeground(UIManager.getColor ("Table.selectionForeground"));
      setBackground(UIManager.getColor ("Table.selectionBackground"));
    }

    /* DEBUG
    if(_selected) setBackground(Color.red);
    else          setBackground(Color.lightGray);
    */

    return this;
  }
}

