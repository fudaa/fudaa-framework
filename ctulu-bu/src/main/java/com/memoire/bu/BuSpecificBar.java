/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuSpecificBar.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

/**
 * A tool bar with simplified adding methods.
 */

public class BuSpecificBar
  extends JToolBar
{
  int     w_;
 /* private boolean horizontal_=true;
  private JPanel  espace_=null;*/
  int h_;
  boolean toolFocusable_;

  public BuSpecificBar()
  {
    super();

    setName("tbSPECIFIC_TOOLBAR");

    /*
    setFloatable
      (BuPreferences.BU.getBooleanProperty("toolbar.floatable",true));
    putClientProperty
      ("JToolBar.isRollover",
       BuPreferences.BU.getBooleanProperty("toolbar.rollover",true)
       ? Boolean.TRUE : Boolean.FALSE);
    */

    addPropertyChangeListener(new PropertyChangeListener()
      {
	@Override
  public void propertyChange(PropertyChangeEvent _evt)
	{
	  //System.err.println("PC: "+_evt.getPropertyName());
	  if("ancestor".equals(_evt.getPropertyName()))
	    { w_=0;
      h_=0; }
	}
      });
  }

  @Override
  public void updateUI()
  {
    w_=h_=0;
    super.updateUI();
  }

  @Override
  public boolean isFocusCycleRoot()
  {
    return true;
  }

  /**
   * Definit que les tools sont focusable. Dans le cas d'une combobox,
   * ceci permet de naviguer d'item en item directement depuis le clavier.
   * @param _b True : Les tools peuvent obtenir le focus.
   */
  public void setToolsFocusable(boolean _b) {
    toolFocusable_=_b;
    
    for (int i=0; i<getComponentCount(); i++) {
      if (getComponent(i) instanceof JComponent)
        ((JComponent)getComponent(i)).setRequestFocusEnabled(_b);
    }
  }
  
  public void addTool(JComponent _tool) {
    if (_tool == null)
      addSeparator();
    
    else {
      _tool.setVisible(true);
      _tool.setRequestFocusEnabled(toolFocusable_);

      if (_tool instanceof JButton) {
        ((JButton) _tool).setMargin(BuInsets.INSETS1111);
        ((JButton) _tool).setRolloverEnabled(true);
      }
      if (_tool instanceof JToggleButton) {
        ((JToggleButton) _tool).setMargin(BuInsets.INSETS1111);
        ((JToggleButton) _tool).setRolloverEnabled(true);
      }

      // SwingUtilities.updateComponentTreeUI(_tool);
      add(_tool);
    }
  }

  public void addTools(JComponent[] _tools)
  {
    if((_tools==null)||(_tools.length==0)) return;

    boolean sep = false;
    for (int i = 0; i < _tools.length; i++) {
      if (_tools[i] == null) {
        if (sep)
          continue;
        sep = true;
      }
      else sep=false;
      addTool(_tools[i]);
    }

    //doLayout();
    //validate();
    revalidate();

    /*
    Container p=getParent();
    if(p!=null)
    {
      p.doLayout();
      p.validate();
    }*/
    // B.M. : Necessaire car certains composants (ComboBox) 
    // ne sont pas réaffichés après désiconification de la fenetre.
    repaint();
  }

  public void removeTool(JComponent _tool)
  {
    if(_tool==null)
    {
      Component c=getComponentAtIndex(0);
      if(c instanceof JToolBar.Separator) remove(c);
    }
    else
    {
      _tool.setVisible(false);
      remove(_tool);
    }
  }

  public void removeTools(JComponent[] _tools)
  {
    if((_tools==null)||(_tools.length==0)) return;

    boolean sep=false;
    for(int i=0;i<_tools.length;i++)
    {
      if(_tools[i]==null)
      {
	if(sep) continue;
	sep=true;
      }
      else sep=false;
      removeTool(_tools[i]);
    }

    //doLayout();
    //validate();
    revalidate();

    /*
    Container p=getParent();
    if(p!=null)
    {
      p.doLayout();
      p.validate();
    }
    repaint();
    */
  }

  @Override
  public Dimension getPreferredSize()
  {
    Dimension d=null;

    try
    {
      d=super.getPreferredSize();

      if(d.height>d.width)
      {
	if(d.width<w_)  d.width=w_;
	else            w_=d.width;
      }
      else
      {
	if(d.height<h_) d.height=h_;
	else            h_=d.height;
      }
    }
    catch(NullPointerException ex) { } // swing 1.03

    return d;
  }
}
