/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut       unstable
 * @file         BuTerminalDisplay.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuCompletor;
import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * A component to display the content of a terminal.
 */
public class BuTerminalDisplay
  extends JComponent
  implements ClipboardOwner, ActionListener, FocusListener, FuEmptyArrays
{
  public static final byte DEFAULT   =-1;
  public static final byte NORMAL    = 0;
  public static final byte REVERSE   = 1;
  public static final byte UNDERLINED= 2;
  public static final byte BOLD      = 4;
  public static final byte SELECTED  = 8;

  public static final int DELAY= 50;
  public static final int BLINK=500;

  private char [][]  text_;
  private int  [][]  fgc_;
  private byte [][]  inv_;
  private BuTimer    timer_;
  boolean    cursor_;
  int        cpos_;
  int lpos_;

  int        nbl_;
  int nbc_;
  int        wc_;
  int hc_;
  private Color      caretForeground_;
  private Color      caretBackground_;
  private boolean    interactive_;
  private Repainter  repainter_;

  public BuTerminalDisplay()
  { this(80,24); }

  public BuTerminalDisplay(int _nbc,int _nbl)
  {
    nbc_=_nbc;
    nbl_=_nbl;

    interactive_=true;

    //setFont(new Font("Monospaced", Font.PLAIN, BuPreferences.BU.getIntegerProperty("term.font.size",10)));
    Font ft=UIManager.getFont("TextArea.font");
    setFont(BuPreferences.BU.getFontProperty
            ("term.font",
             new Font("Monospaced",Font.PLAIN,(ft==null) ? 10 : ft.getSize())));

    /*
    setForeground     (BuPreferences.BU.getColorProperty("term.display.foreground",new Color( 64,255,128)));
    setBackground     (BuPreferences.BU.getColorProperty("term.display.background",new Color(  0, 32,  0)));
    setCaretForeground(BuPreferences.BU.getColorProperty("term.caret.foreground"  ,new Color(255,255,128)));
    setCaretBackground(BuPreferences.BU.getColorProperty("term.caret.background"  ,new Color(255,255,128)));
    */
    setForeground     (BuLib.getColor(BuPreferences.BU.getColorProperty("term.display.foreground",Color.black)));
    setBackground     (BuLib.getColor(BuPreferences.BU.getColorProperty("term.display.background",Color.white)));
    setCaretForeground(BuLib.getColor(BuPreferences.BU.getColorProperty("term.caret.foreground"  ,Color.black)));
    setCaretBackground(BuLib.getColor(BuPreferences.BU.getColorProperty("term.caret.background"  ,Color.black)));

    cursor_=true;
    cpos_=0;
    lpos_=Math.max(0,nbl_-24);
    text_=new char[nbl_][nbc_];
    fgc_ =new int [nbl_][nbc_];
    inv_ =new byte[nbl_][nbc_];
    for(int i=0;i<nbl_;i++)
      for(int j=0;j<nbc_;j++)
      {
	text_[i][j]=' ';
	fgc_ [i][j]=getForeground().getRGB();
	inv_ [i][j]=NORMAL;
      }

    setSize(getPreferredSize());

    addFocusListener(this);
    
    repainter_=new Repainter();
    /*
    repainter_.setName("Termainal display repainter");
    repainter_.setDaemon(true);
    repainter_.setPriority(Thread.NORM_PRIORITY-1);
    repainter_.start();
    */
  }

  @Override
  public boolean isManagingFocus()
  {
    return true;
  }

  private final void repainter(int _x,int _y,int _w,int _h)
  {
    repainter_.update(_x,_y,_w,_h);
  }

  private final void repainter(int _d,int _x,int _y,int _w,int _h)
  {
    repainter_.update(_x,_y,_w,_h);
  }

  private final void repainter(int _d)
  {
    repainter_.update(0,0,getWidth(),getHeight());
  }

  @Override
  public void setFont(Font _f)
  {
    super.setFont(_f);
    FontMetrics fm=BuLib.getFontMetrics(this,getFont());
    wc_=fm.charWidth('m');
    hc_=fm.getHeight()+1;
    revalidate();
  }

  /*
  protected void processKeyEvent(KeyEvent _evt)
  {
    if(  (_evt.getKeyCode()  ==9)
       &&(_evt.getModifiers()==0))
    {
      //System.err.println(_evt.getKeyChar()+" "+_evt.getKeyCode());
      Object[] o=getListeners(KeyListener.class);
      if(_evt.getID()==KeyEvent.KEY_RELEASED)
      for(int i=0;i<o.length;i++)
      {
	//System.err.println(o[i]);
	((KeyListener)o[i]).keyTyped(_evt);
      }
    }
    else super.processKeyEvent(_evt);
  }
  */

  @Override
  public void focusGained(FocusEvent _evt)
  {
    if(timer_==null)
    {
      timer_=new BuTimer(BLINK,this);
      timer_.start();
    }
    repainter(c_to_x(cpos_),l_to_y(lpos_),c_to_x(1),l_to_y(1));
  }

  @Override
  public void focusLost(FocusEvent _evt)
  {
    if(timer_!=null)
    {
      timer_.stop();
      timer_.removeActionListener(this);
      timer_=null;
    }
    repainter(c_to_x(cpos_),l_to_y(lpos_),c_to_x(1),l_to_y(1));
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    if(_evt.getSource()==timer_)
    {
      cursor_=!cursor_;
      repainter(200,c_to_x(cpos_),l_to_y(lpos_),c_to_x(1),l_to_y(1));
    }
    else
    {
      String action=_evt.getActionCommand();
      System.err.println("unknown action: "+action);
    }
  }

  @Override
  public void reshape(int _x, int _y, int _w, int _h)
  {
    //System.err.println("W="+_w+" "+getWidth());
    super.reshape(_x,_y,_w,_h);

    if(/*(nbc_!=_w/wc_)&&(_w>wc_)&&*/isShowing())
    {
      int onbc=nbc_;
      nbc_=Math.max(_w/wc_,14);

      if(onbc==nbc_) return;

      //FuLog.debug("BTD: reshape _w="+_w+" onbc="+onbc+" nbc="+nbc_);

      synchronized(this)
      {
	char[][] otext=text_;
	int [][] ofgc =fgc_;
	byte[][] oinv =inv_;

	text_=new char[nbl_][nbc_];
	fgc_ =new int [nbl_][nbc_];
	inv_ =new byte[nbl_][nbc_];

	onbc=Math.min(onbc,nbc_);
	for(int i=0;i<nbl_;i++)
	{
	  for(int j=0;j<onbc;j++)
	  {
	    text_[i][j]=otext[i][j];
	    fgc_ [i][j]=ofgc [i][j];
	    inv_ [i][j]=oinv [i][j];
	  }
	  for(int j=onbc;j<nbc_;j++)
	  {
	    text_[i][j]=' ';
	    fgc_ [i][j]=getForeground().getRGB();
	    inv_ [i][j]=NORMAL;
	  }
	}

	cpos_=Math.min(cpos_,nbc_-1);
      }
    }
  }

  public Dimension getVisibleSizeInChars()
  {
    Dimension r=getSize();
    Container p=getParent();
    if(p instanceof JViewport)
    {
      JViewport v=(JViewport)p;
      r=v.getSize();
      Insets i=v.getInsets();
      r.width -=i.left+i.right;
      r.height-=i.top+i.bottom;
    }
    //FuLog.debug("BTD: getVisibleSizeInChars sizepx="+r);
    r.width /=wc_;
    r.height/=hc_;
    //FuLog.debug("BTD: getVisibleSizeInChars sizech="+r);
    return r;
  }

  public void setVisibleWidthInChars(int _nbc)
  {
    //FuLog.debug("BTD: setVisibleWidthInChars wc="+_nbc);
    //FuLog.debug("BTD: setVisibleWidthInChars wx="+_nbc*wc_);
    setSize(_nbc*wc_,getHeight());
  }

  public final int x_to_c(int _x)
    { return Math.min(Math.max(0,_x/wc_),nbc_-1); }

  public final int c_to_x(int _c)
    { return _c*wc_; }

  public final int y_to_l(int _y)
    { return Math.min(Math.max(0,_y/hc_),nbl_-1); }

  public final int l_to_y(int _l)
    { return _l*hc_; }

  @Override
  public Dimension getPreferredSize()
    { return new Dimension(c_to_x(nbc_),l_to_y(nbl_)); }

  @Override
  public boolean isFocusTraversable()
    { return isInteractive(); }

  public Color getCaretBackground()
    { return caretBackground_; }

  public void setCaretBackground(Color _cbg)
    { caretBackground_=_cbg; }

  public Color getCaretForeground()
    { return caretForeground_; }

  public void setCaretForeground(Color _cfg)
    { caretForeground_=_cfg; }

  public boolean isInteractive()
    { return interactive_; }

  public void setInteractive(boolean _interactive)
    { interactive_=_interactive; }

  @Override
  public synchronized void paintComponent(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);

    BuLib.setColor(_g,getBackground());
    _g.fillRect(0,0,getWidth(),getHeight());

    int lc=getBackground().getRGB();
    lc=BuLib.mixColors(lc,0xFF000000);
    lc=BuLib.mixColors(lc,0xFFFFFFFF);
    lc=BuLib.mixColors(lc,getBackground().getRGB());
    BuLib.setColor(_g,new Color(lc));
    _g.drawLine(0,l_to_y(nbl_),c_to_x(nbc_),l_to_y(nbl_));
    //_g.drawLine(c_to_x(nbc_),0,c_to_x(nbc_),l_to_y(nbl_));
    _g.drawLine(c_to_x( 80),0,c_to_x( 80),l_to_y(nbl_));
    _g.drawLine(c_to_x(132),0,c_to_x(132),l_to_y(nbl_));
    
    BuLib.setColor(_g,getForeground());

    int imin=0,imax=nbl_;
    int jmin=0,jmax=nbc_;
    Rectangle clip=_g.getClipBounds();
    //int h=getFont().getSize();

    if(clip!=null)
    {
      jmin=x_to_c(clip.x);
      jmax=Math.min(nbc_,x_to_c(clip.x+clip.width)+1);
      imin=y_to_l(clip.y);
      imax=Math.min(nbl_,y_to_l(clip.y+clip.height)+1);
    }
    
    //System.err.println("BTD: imin="+imin+" imax="+imax);

    int    x,y;
    String s;
    Color  c;

    for(int i=imin;i<imax;i++)
    {
      y=l_to_y(i);
      for(int j=jmin;j<jmax;j++)
      {
	x=c_to_x(j);
	s=""+text_[i][j];
	c=new Color(fgc_[i][j]);
	_g.setColor(c);

	if(  (((inv_[i][j]&SELECTED)!=0)&&((inv_[i][j]&REVERSE)==0))
	   ||(((inv_[i][j]&SELECTED)==0)&&((inv_[i][j]&REVERSE)!=0)))
	{
	  _g.fillRect(x,y,wc_,hc_);
	  _g.setColor(getBackground());
	}

	
	if((inv_[i][j]&BOLD)!=0)
	  _g.setFont(new Font(getFont().getFamily(),
			      Font.BOLD,getFont().getSize()));
	else
	  _g.setFont(getFont());

	_g.drawString(s,x,y+hc_-3);

	if((inv_[i][j]&UNDERLINED)!=0)
	  _g.drawLine(x,y+hc_-1,x+wc_-1,y+hc_-1);
      }
    }

    if(isInteractive())
    {
      x=c_to_x(cpos_);
      y=l_to_y(lpos_);
      _g.setColor(getCaretBackground());
      if(hasFocus()&&cursor_) _g.fillRect(x,y+1,wc_-1,hc_-4);
      _g.setColor(getCaretForeground());
      _g.drawRect(x,y+1,wc_-1,hc_-4);
    }

    repainter_.start();
  }

  public synchronized void scroll()
  {
    char[] text0=text_[0];
    int [] fgc0=fgc_[0];
    byte[] inv0=inv_[0];

    for(int i=1;i<nbl_;i++)
    {
	text_[i-1]=text_[i];
	fgc_ [i-1]=fgc_ [i];
	inv_ [i-1]=inv_ [i];
    }
    
    text_[nbl_-1]=text0;
    fgc_ [nbl_-1]=fgc0;
    inv_ [nbl_-1]=inv0;
    
    /*
    for(int i=0;i<nbl_-1;i++)
      for(int j=0;j<nbc_;j++)
      {
	text_[i][j]=text_[i+1][j];
	fgc_ [i][j]=fgc_ [i+1][j];
	inv_ [i][j]=inv_ [i+1][j];
      }
    */

    int color=getForeground().getRGB();

    for(int j=0;j<nbc_;j++)
    {
      text_[nbl_-1][j]=' ';
      fgc_ [nbl_-1][j]=color;
      inv_ [nbl_-1][j]=(byte)0;
    }

    repainter(DELAY);

    lpos_--;
  }

  public synchronized void insert(char _ch,int _color, byte _style)
  {
    repainter(DELAY,c_to_x(cpos_),l_to_y(lpos_),c_to_x(2),l_to_y(1));

    switch(_ch)
    {
      case '\n':
	cpos_=nbc_;
	break;
      case '\r':
	cpos_=0;
	break;
      case '\007':
	beep();
	break;
      case '\t':
	do
	{
	  text_[lpos_][cpos_]=' ';
	  fgc_ [lpos_][cpos_]=_color;
	  inv_ [lpos_][cpos_]=_style;
	  repainter(DELAY,c_to_x(cpos_),l_to_y(lpos_),c_to_x(2),l_to_y(1));
	  cpos_++;
	} while(cpos_%8!=0);
	break;
      default:
	text_[lpos_][cpos_]=_ch;
	fgc_ [lpos_][cpos_]=_color;
	inv_ [lpos_][cpos_]=_style;
	//repainter(DELAY,c_to_x(cpos_),l_to_y(lpos_),c_to_x(2),l_to_y(1));
	cpos_++;
	break;
    }

    if(cpos_>=nbc_)
    {
      if(lpos_==nbl_-1) { scroll(); lpos_++; }
      else lpos_++;
      cpos_=0;
    }
  }

  public synchronized void clear_char(int _color, byte _style)
  {
    text_[lpos_][cpos_]=' ';
    fgc_ [lpos_][cpos_]=_color;
    inv_ [lpos_][cpos_]=_style;

    repainter(DELAY,c_to_x(cpos_),l_to_y(nbl_-1),c_to_x(2),l_to_y(1));
  }

  public synchronized void clear_eol(int _color, byte _style)
  {
    for(int i=cpos_;i<nbc_;i++)
    {
      text_[lpos_][i]=' ';
      fgc_ [lpos_][i]=_color;
      inv_ [lpos_][i]=_style;
    }

    repainter(DELAY,c_to_x(cpos_),l_to_y(lpos_),c_to_x(nbc_-cpos_),l_to_y(1));
  }

  public synchronized void clear_screen(int _color, byte _style)
  {
    for(int j=0;j<nbl_;j++)
      for(int i=0;i<nbc_;i++)
      {
	text_[j][i]=' ';
	fgc_ [j][i]=_color;
	inv_ [j][i]=_style;
      }

    //cpos_=0;
    //lpos_=0;
    repainter(DELAY);
  }

  public synchronized void clear_char()
  {
    clear_char(getForeground().getRGB(),(byte)0);
  }

  public synchronized void clear_eol()
  {
    clear_eol(getForeground().getRGB(),(byte)0);
  }

  public synchronized void clear_screen()
  {
    clear_screen(getForeground().getRGB(),(byte)0);
  }

  public synchronized void clear_selection()
  {
    int mini=nbc_;
    int maxi=-1;
    int minj=nbl_;
    int maxj=-1;

    for(int j=0;j<nbl_;j++)
      for(int i=0;i<nbc_;i++)
      {
	if((inv_[j][i]&SELECTED)!=0)
	{
	  inv_[j][i]&=~SELECTED;
	  if(i<mini) mini=i;
	  if(i>maxi) maxi=i;
	  if(j<minj) minj=j;
	  if(j>maxj) maxj=j;
	}
      }

    if((maxi>=mini)&&(maxj>=minj))
      repainter(DELAY,
		c_to_x(mini),l_to_y(minj),
		c_to_x(maxi-mini+1),l_to_y(maxj-minj+1));
  }

  public synchronized String get_selection()
  {
    StringBuffer r=new StringBuffer(nbc_);
    int          n=0;

    for(int j=0;j<nbl_;j++)
    {
      boolean b=false;
      for(int i=0;i<nbc_;i++)
      {
	if((inv_[j][i]&SELECTED)!=0)
	{
	  b=true;
	  r.append(text_[j][i]);
	}
      }
      if(b) { n++; r.append('\n'); }
    }

    String s=r.toString();
    if(n==1) s=s.substring(0,s.length()-1);
    return s;
  }

  public synchronized void set_selection
    (int _cstart, int _lstart, int _cend, int _lend)
  {
    int cstart=_cstart;
    int lstart=_lstart;
    int cend=_cend;
    int lend=_lend;
    if((lstart>lend)||((lstart==lend)&&(cstart>cend)))
    {
      int ctmp=cstart; cstart=cend; cend=ctmp;
      int ltmp=lstart; lstart=lend; lend=ltmp;
    }

    int mini=nbc_;
    int maxi=-1;
    int minj=nbl_;
    int maxj=-1;

    if(lstart<0  ) lstart=0;
    if(lend>=nbl_) lend  =nbl_-1;
    if(cstart<0  ) cstart=0;
    if(cend>=nbc_) cend  =nbc_-1;

    for(int j=lstart;j<=lend;j++)
    {
      int istart=0;
      int iend  =nbc_;
      if(j==lstart) istart=cstart;
      if(j==lend  ) iend  =cend+1;
      for(int i=istart;i<iend;i++)
      {
	inv_[j][i]|=SELECTED;
	if(i<mini) mini=i;
	if(i>maxi) maxi=i;
	if(j<minj) minj=j;
	if(j>maxj) maxj=j;
      }
    }

    if((maxi>=mini)&&(maxj>=minj))
      repainter(DELAY,
		c_to_x(mini),l_to_y(minj),
		c_to_x(maxi-mini+1),l_to_y(maxj-minj+1));
  }

  private boolean isWordChar(char _c)
  {
    //return Character.isLetterOrDigit(_c);
    return !Character.isWhitespace(_c);
  }

  public void select_word(int _c, int _l)
  {
    clear_selection();
    if((_l>=0)&&(_l<nbl_))
    {
      int cstart=_c;
      int cend  =_c;
      while((cstart>=0)   &&isWordChar(text_[_l][cstart])) cstart--;
      cstart++;
      while((cend<=nbc_-1)&&isWordChar(text_[_l][cend  ])) cend++;
      cend--;
      if(cstart<cend) set_selection(cstart,_l,cend,_l);
    }
  }

  public void select_line(int _l)
  {
    clear_selection();
    if((_l>=0)&&(_l<nbl_))
      set_selection(0,_l,nbc_-1,_l);
  }

  /*
  public void select_screen()
  {
    set_selection(0,0,nbc_-1,nbl_-1);
  }
  */

  private Clipboard clipboard_;

  protected Clipboard getClipboard()
  {
    if(clipboard_!=null) return clipboard_;

    synchronized(this)
    {
      if(clipboard_!=null) return clipboard_;

      try
      {
	//Class  c=getToolkit().getClass();
	Method m=Toolkit.class.getMethod
          ("getSystemSelection",CLASS0);
	clipboard_=(Clipboard)m.invoke(getToolkit(),OBJECT0);
      }
      catch(Exception ex)
      {
	System.err.println("BTD: no system selection");
      }
      finally
      {
	if(clipboard_==null)
	  clipboard_=getToolkit().getSystemClipboard();
      }
    }

    return clipboard_;
  }

  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents)
  {
  }

  public void copy()
  {
    String s=get_selection();

    int i=s.length()-1;
    while((i>=0)&&Character.isWhitespace(s.charAt(i))) i--;
    s=s.substring(0,i+1);

    copy(s);
  }

  public void copy(String _s)
  {
    if(!"".equals(_s))
    {
      Transferable contents =new StringSelection(_s);
      getClipboard().setContents(contents,this);
    }
  }

  public void beep()
  {
    getToolkit().beep();
  }

  // Keys

  public BuTerminalKey createKey
    (String _word, int _color)
    { return createKey(_word,_color,NORMAL,BuTerminalKey.WHOLE); }

  public BuTerminalKey createKey
    (String _word, int _color, byte _style)
    { return createKey(_word,_color,_style,BuTerminalKey.WHOLE); }

  public BuTerminalKey createKey
    (String _word, int _color, byte _style, String _til)
    { return new BuTerminalKey(_word,_color,_style,_til); }

  /*
  public OutStream createOutStream()
    { return createOutStream(getForeground()); }

  public InStream createInStream()
    { return createInStream(getForeground(),true); }

  public OutStream createOutStream(Color _color)
    { return createOutStream(_color,NORMAL); }

  public InStream createInStream(Color _color,boolean _editable)
    { return createInStream(_color,NORMAL,_editable); }
  */

  public OutStream createOutStream(Color _color,byte _style, boolean _ansi)
  {
    OutStream r=null;
    if(_ansi) r=new AnsiOutStream(_color.getRGB(),_style);
    else      r=new SimpleOutStream(_color.getRGB(),_style);
    return r;
  }

  public InStream createInStream(Color _color,byte _style,boolean _editable)
  {
    InStream r=null;
    if(_editable) r=new EditableInStream(_color.getRGB(),_style);
    else          r=new SimpleInStream(_color.getRGB(),_style);
    return r;
  }

  // Out Streams

  public class OutStream extends OutputStream
  {
    protected int  color_;
    protected byte style_;
    //private char   previous_;

    public OutStream(int _color,byte _style)
    {
      color_=_color;
      style_=_style;
    }

    protected void write(char _char,int _color,byte _style)
    {
      int color=_color;
      int style=_style;
      if(color==DEFAULT) color=color_;
      if(style==DEFAULT) style=style_;

      //if((cpos_!=0)||(_char!='\n')||(previous_=='\n'))
      insert(_char,color,(byte)(style|style_));
      //previous_=_char;
    }

    @Override
    public final void write(int _b) throws IOException
    {
      write((char)_b,color_,style_);
    }

    @Override
    public void write(byte _b[]) throws IOException
    {
      String text=new String(_b);
      int    l   =text.length();

      for(int i=0;i<l;i++)
	write(text.charAt(i));
    }

    @Override
    public final void write(byte _b[], int _d, int _l) throws IOException
    {
      byte[] b=new byte[_l];
      System.arraycopy(_b,_d,b,0,_l);
      write(b);
    }

    @Override
    public final void close() throws IOException
    {
      super.flush();
    }
  }

  public class SimpleOutStream extends OutStream
  {
    private int             left_,right_;
    private BuTerminalKey[] keys_;
    private boolean         escaped_;

    public SimpleOutStream(int _color,byte _style)
    {
      super(_color,_style);
      left_   =0;
      right_  =0;
      keys_   =null;
      escaped_=false;
    }

    public void setKeys(BuTerminalKey[] _keys)
      { keys_=_keys; }

    public void setLeftMargin(int _left)
      { left_=_left; }

    public void setRightMargin(int _right)
      { right_=_right; }

    @Override
    protected void write(char _char, int _color, byte _style)
    {
      if(escaped_)
      {
        if((_char>='a')&&(_char<='z')) escaped_=false;
        if((_char>='A')&&(_char<='Z')) escaped_=false;
        return;
      }
      else
      if(_char=='\033')
      {
	escaped_=true;
	return;
      }

      while(cpos_<left_)
	insert(' ',_color,NORMAL);
      super.write(_char,_color,_style);
      if(cpos_>nbc_-right_)
      {
        if(lpos_==nbl_-1) { scroll(); lpos_++; }
	else lpos_++;
	cpos_=0;
      }
    }

    @Override
    public void write(byte _b[]) throws IOException
    {
      String text=new String(_b);
      int    l   =text.length();

      if(keys_==null)
      {
	for(int i=0;i<l;i++)
	  write(text.charAt(i));
      }
      else
      {
	int [] color=new int [l];
	byte[] style=new byte[l];
	for(int i=0;i<l;i++)
	{
	  color[i]=color_;
	  style[i]=style_;
	}

	for(int j=0;j<keys_.length;j++)
	{
	  BuTerminalKey k=keys_[j];
	  int p=0;
	  while((p=text.indexOf(k.word_,p))>=0)
	  {
	    int qmin,qmax;

	    if(BuTerminalKey.WHOLE.equals(k.til_))
	      { qmin=p; qmax=p+k.word_.length(); }
	    else
	    if(BuTerminalKey.LEFT.equals(k.til_))
	      { qmin=0; qmax=p+k.word_.length(); }
	    else
	    if(BuTerminalKey.RIGHT.equals(k.til_))
	      { qmin=p; qmax=l-p; }
	    else
	      { qmin=p; qmax=Math.max(l,1+text.indexOf(k.til_)); }

	    for(int q=qmin; q<qmax; q++)
	      { color[q]=k.color_; style[q]=k.style_; }
	    p++;
	  }
	}

	for(int i=0;i<l;i++)
	  write(text.charAt(i),color[i],style[i]);
      }
    }
  }

  private static final int ANSI_DARK_BLACK  =0x000000;
  private static final int ANSI_DARK_RED    =0x800000;
  private static final int ANSI_DARK_GREEN  =0x008000;
  private static final int ANSI_DARK_YELLOW =0x808000;
  private static final int ANSI_DARK_BLUE   =0x000080;
  private static final int ANSI_DARK_MAGENTA=0x800080;
  private static final int ANSI_DARK_CYAN   =0x008080;
  private static final int ANSI_DARK_WHITE  =0xC0C0C0;

  private static final int ANSI_BRIGHT_BLACK  =0x808080;
  private static final int ANSI_BRIGHT_RED    =0xFF0000;
  private static final int ANSI_BRIGHT_GREEN  =0x00FF00;
  private static final int ANSI_BRIGHT_YELLOW =0xFFFF00;
  private static final int ANSI_BRIGHT_BLUE   =0x0000FF;
  private static final int ANSI_BRIGHT_MAGENTA=0xFF00FF;
  private static final int ANSI_BRIGHT_CYAN   =0x00FFFF;
  private static final int ANSI_BRIGHT_WHITE  =0xFFFFFF;

  public class AnsiOutStream extends OutStream
  {
    private boolean escaped_;
    private boolean bold_;
    private String  sequence_;
    private int     basecolor_;
    private byte    basestyle_;
    private boolean bright_;
    private int     csaved_;
    private int     lsaved_;

    public AnsiOutStream(int _color,byte _style)
    {
      super(_color,_style);
      escaped_  =false;
      bold_     =false;
      sequence_ ="";
      basecolor_=_color;
      basestyle_=_style;
      bright_   =false;
      csaved_   =cpos_;
      lsaved_   =lpos_;
    }

    @Override
    protected void write(char _char, int _color, byte _style)
    {
      if(escaped_)
      {
	sequence_+=_char;
        if(  ((_char>='a')&&(_char<='z'))
           ||((_char>='A')&&(_char<='Z')))
	{
	  if(!sequence_.startsWith("["))
	  {
	    System.err.println("Bad formatted Ansi sequence: "+sequence_);
	  }
	  else
	  {
	    // Ansi Code Reference:
	    // http://www3.sympatico.ca/rhwatson/dos7/v-ansi-commands.html

	    // split an ansi 'm' sequence if it contains a ';'
	    if(sequence_.endsWith("m")&&(sequence_.indexOf(';')>=0))
	    {
	      String s="\033"+FuLib.replace(sequence_,";","m\033[");
	      escaped_=false;
	      for(int i=0;i<s.length();i++)
		write(s.charAt(i),color_,style_);
	      return;
	    }

	    sequence_=sequence_.substring(1);

	    int fg=-1;
	    int bg=-1;

	    if(bright_)
	    {
	           if(sequence_.equals("30m")) fg=ANSI_BRIGHT_BLACK;
	      else if(sequence_.equals("31m")) fg=ANSI_BRIGHT_RED;
	      else if(sequence_.equals("32m")) fg=ANSI_BRIGHT_GREEN;
	      else if(sequence_.equals("33m")) fg=ANSI_BRIGHT_YELLOW;
	      else if(sequence_.equals("34m")) fg=ANSI_BRIGHT_BLUE;
	      else if(sequence_.equals("35m")) fg=ANSI_BRIGHT_MAGENTA;
	      else if(sequence_.equals("36m")) fg=ANSI_BRIGHT_CYAN;
	      else if(sequence_.equals("37m")) fg=ANSI_BRIGHT_WHITE;

	      else if(sequence_.equals("40m")) bg=ANSI_BRIGHT_BLACK;
	      else if(sequence_.equals("41m")) bg=ANSI_BRIGHT_RED;
	      else if(sequence_.equals("42m")) bg=ANSI_BRIGHT_GREEN;
	      else if(sequence_.equals("43m")) bg=ANSI_BRIGHT_YELLOW;
	      else if(sequence_.equals("44m")) bg=ANSI_BRIGHT_BLUE;
	      else if(sequence_.equals("45m")) bg=ANSI_BRIGHT_MAGENTA;
	      else if(sequence_.equals("46m")) bg=ANSI_BRIGHT_CYAN;
	      else if(sequence_.equals("47m")) bg=ANSI_BRIGHT_WHITE;
	    }
	    else
	    {
	           if(sequence_.equals("30m")) fg=ANSI_DARK_BLACK;
	      else if(sequence_.equals("31m")) fg=ANSI_DARK_RED;
	      else if(sequence_.equals("32m")) fg=ANSI_DARK_GREEN;
	      else if(sequence_.equals("33m")) fg=ANSI_DARK_YELLOW;
	      else if(sequence_.equals("34m")) fg=ANSI_DARK_BLUE;
	      else if(sequence_.equals("35m")) fg=ANSI_DARK_MAGENTA;
	      else if(sequence_.equals("36m")) fg=ANSI_DARK_CYAN;
	      else if(sequence_.equals("37m")) fg=ANSI_DARK_WHITE;

	      else if(sequence_.equals("40m")) bg=ANSI_DARK_BLACK;
	      else if(sequence_.equals("41m")) bg=ANSI_DARK_RED;
	      else if(sequence_.equals("42m")) bg=ANSI_DARK_GREEN;
	      else if(sequence_.equals("43m")) bg=ANSI_DARK_YELLOW;
	      else if(sequence_.equals("44m")) bg=ANSI_DARK_BLUE;
	      else if(sequence_.equals("45m")) bg=ANSI_DARK_MAGENTA;
	      else if(sequence_.equals("46m")) bg=ANSI_DARK_CYAN;
	      else if(sequence_.equals("47m")) bg=ANSI_DARK_WHITE;
	    }

	         if(fg>=0) color_=BuLib.getColor(fg);
	    else if(bg>=0) ;
	    //else if(sequence_.equals("00m")) { color_=basecolor_; style_=basestyle_; }

	    else if(sequence_.endsWith("H")||sequence_.endsWith("f"))
	      {
		int ic=sequence_.indexOf(';');
		int cc=0;
		int lc=0;
		try
		{
		  if(ic>0)
		  {
		    lc=Integer.parseInt(sequence_.substring(ic+1,sequence_.length()-1));
		  }
		  else ic=sequence_.length()-1;
		  cc=Integer.parseInt(sequence_.substring(0,ic));
		}
		catch(NumberFormatException nfex) { }
		cpos_=cc;
		if(lc< 0) lc=0;
		if(lc>23) lc=23;
		lpos_=Math.max(0,nbl_-24+lc);
	      }
	    else if(sequence_.endsWith("A"))
	      {
		int lc=0;
		try
		{
		  lc=Integer.parseInt(sequence_.substring(0,sequence_.length()-1));
		}
		catch(NumberFormatException nfex) { }
		lc=lpos_-lc;
		if(lc< 0) lc=0;
		if(lc>23) lc=23;
		lpos_=Math.max(0,nbl_-24+lc);
	      }
	    else if(sequence_.endsWith("B"))
	      {
		int lc=0;
		try
		{
		  lc=Integer.parseInt(sequence_.substring(0,sequence_.length()-1));
		}
		catch(NumberFormatException nfex) { }
		lc=lpos_+lc;
		if(lc< 0) lc=0;
		if(lc>23) lc=23;
		lpos_=Math.max(0,nbl_-24+lc);
	      }
	    else if(sequence_.endsWith("C"))
	      {
		int cc=0;
		try
		{
		  cc=Integer.parseInt(sequence_.substring(0,sequence_.length()-1));
		}
		catch(NumberFormatException nfex) { }
		cc=cpos_+cc;
		if(cc<    0) cc=0;
		if(cc>=nbc_) cc=nbc_-1;
		cpos_=cc;
	      }
	    else if(sequence_.endsWith("D"))
	      {
		int cc=0;
		try
		{
		  cc=Integer.parseInt(sequence_.substring(0,sequence_.length()-1));
		}
		catch(NumberFormatException nfex) { }
		cc=cpos_-cc;
		if(cc<    0) cc=0;
		if(cc>=nbc_) cc=nbc_-1;
		cpos_=cc;
	      }

	    else if(sequence_.equals("s")) { csaved_=cpos_; lsaved_=lpos_; } 
	    else if(sequence_.equals("u")) { cpos_=csaved_; lpos_=lsaved_; } 

	    else if(sequence_.equals("m")||sequence_.equals("0m")||sequence_.equals("00m"))
	      { color_=basecolor_; style_=basestyle_; bright_=false; } // style_=NORMAL
	    else if(sequence_.equals("1m")||sequence_.equals("01m")) bright_=true;
	    else if(sequence_.equals("4m")||sequence_.equals("04m")) style_|=UNDERLINED;
	    else if(sequence_.equals("5m")||sequence_.equals("05m")) ;
	    else if(sequence_.equals("7m")||sequence_.equals("07m")) style_|=REVERSE;
	    else if(sequence_.equals("8m")||sequence_.equals("08m")) color_=getBackground().getRGB();

	    else if(sequence_.equals  ("K")) clear_eol(_color,(byte)0);
	    else if(sequence_.endsWith("J"))
	      {
		clear_screen(_color,(byte)0);
		cpos_=0;
		lpos_=Math.max(0,nbl_-24);
	      }

	    else if(sequence_.equals("=7h" )) ;
	    else if(sequence_.equals("=7l" )) ;
	    else if(sequence_.equals("=0h" )) ;
	    else if(sequence_.equals("=1h" )) ;
	    else if(sequence_.equals("=2h" )) ;
	    else if(sequence_.equals("=3h" )) ;
	    else if(sequence_.equals("=5h" )) ;
	    else if(sequence_.equals("=4h" )) ;
	    else if(sequence_.equals("=13h")) ;
	    else if(sequence_.equals("=19h")) ;
	    else if(sequence_.equals("=6h" )) ;
	    else if(sequence_.equals("=14h")) ;
	    else if(sequence_.equals("=15h")) ;
	    else if(sequence_.equals("=16h")) ;
	    else if(sequence_.equals("=17h")) ;
	    else if(sequence_.equals("=18h")) ;

	    else if(sequence_.endsWith("p")) ;

	    else System.err.println("Unknown Ansi sequence: "+sequence_);
	  }

	  escaped_ =false;
	  sequence_="";
	}
	return;
      }
      else
      if(_char==27) // Escape
      {
	escaped_ =true;
	sequence_="";
	return;
      }
      else
      if(_char== 8) // Backspace
      {
	if(cpos_>0) cpos_--;
	bold_=true;
	return;
      }

      super.write(_char,_color,(byte)(_style|(bold_ ? BOLD : NORMAL)));
      bold_=false;
    }
  }

  // Completion

  public final class KeyCompletor implements FuCompletor
  {
    private BuTerminalKey[] keys_;

    public KeyCompletor(BuTerminalKey[] _keys)
    {
      keys_=_keys;
    }

    @Override
    public String complete(String _s, int _p)
    {
      String r="";

      boolean beep=true;

      if((keys_!=null)&&!"".equals(_s))
      {
        String w=_s.substring(0,_p);

        int q=w.length()-1;
	for(;q>=0;q--)
          if(!Character.isLetter(w.charAt(q)))
            break;
        w=w.substring(q+1);

	int n=0;
	int f=-1;

	for(int j=0;j<keys_.length;j++)
	  if(keys_[j].word_.startsWith(w))
	    { f=j; n++; }

	if(n==1)
	{
	  if(keys_[f].word_.equals(w)) r=" ";
	  else r=keys_[f].word_.substring(w.length());
	  beep=false;
	}
      }

      if(beep) beep();

      return r;
    }
  }

  // In Streams

  public abstract class InStream extends InputStream
  {
    protected final void add(String _s)
    {
      for(int i=0;i<_s.length();i++)
	add(_s.charAt(i));
    }

    @Override
    public final long skip(long _l)
    {
      System.err.println("skip");

      long i;
      for(i=0; i<_l; i++)
	if(read()==-1) break;
      return i;
    }

    @Override
    public abstract int  read();
    public abstract void add(char _clef);

    public final void paste()
    {
      Transferable contents=getClipboard().getContents(this);

      if(contents!=null)
      {
        try
        {
          String s=(String)contents.getTransferData(DataFlavor.stringFlavor);
          paste(s);
        }
        catch(Exception ex1)
        {
          beep();
          //try { term_.error_.write(ex1.getMessage().getBytes()); }
          //catch(IOException ex2) { }
        }
      }
    }

    public void paste(String _s) { if(_s!=null) add(_s); }

    public boolean isCanonic() { return true; }
    public void    setCanonic(boolean _canonic) { }

    //public void setKeys(BuTerminalKey[] _keys)    { }
    public void setCompletor(FuCompletor _c) { }
    public void setFastCompletionChar(char _char) { }
  }

  public static final int BUFFER_SIZE=32768;

  public final class SimpleInStream extends InStream
  {
    char[] buf_   =new char[BUFFER_SIZE];
    int    bbegin_;
    int    bend_  ;
    Object lock_  =new Object();
    
    //private int         color_;
    //private byte        style_;
    private KeyListener kl_;

    protected SimpleInStream(int _color,byte _style)
    {
      /*color_ =_color;
      style_ =_style;*/

      kl_=new KeyAdapter()
	{
	  @Override
    public void keyTyped(KeyEvent _evt)
	  {
	    char clef=_evt.getKeyChar();

	    /*
	    if(clef=='\r'  ) clef='\n';
            else if(clef=='\007') ;
	    else if(clef<=27    ) clef='\000';
	    if(clef!='\000')
	    */
	    cursor_=true;
	    //timer_.restart();
	    add(clef);
	    _evt.consume();
	  }
	};
      addKeyListener(kl_);
    }

    @Override
    public int read()
    {
      int i=-1;

      try
      {
	if(bbegin_==bend_)
	  synchronized(lock_)
	  {
	    lock_.wait();
	  }

	i=buf_[bbegin_];
	bbegin_++;
	if(bbegin_==BUFFER_SIZE) bbegin_=0;
      }
      catch (Exception ex)
	{ System.err.println("BuTerminalDisplay.read: "+ex); }

      return i;
    }

    @Override
    public void add(char _clef)
    {
      //System.err.print("("+(int)_clef+")");
      //if((_clef=='\n')||!Character.isISOControl(_clef))
      //insert(_clef,color_,style_);

      int     old_bend=bend_;
      boolean unlock  =(bend_==bbegin_);

      buf_[bend_]=_clef;
      bend_++;
      if(bend_==BUFFER_SIZE) bend_=0;

      if(bend_==bbegin_)
      {
	System.err.println("BuTerminal overflow");
	bend_=old_bend;
      }

      if(unlock)
	synchronized(lock_)
	{
	  lock_.notifyAll();
	}
    }
    
    @Override
    public int available()
    {
      int r=bend_-bbegin_;
      if(r<0) r=(BUFFER_SIZE-bbegin_)+bend_;
      return r;
    }

    @Override
    public void close()            { }
    @Override
    public void mark(int _p)       { }
    @Override
    public boolean markSupported() { return false; }
    @Override
    public void reset()            { }
    @Override
    public int read(byte[] _b)     { return read(_b,0,_b.length); }
  
    @Override
    public int read(byte[] _b, int _d, int _l)
    {
      if(_l==0) return 0;

      /*
	int a=available();
	if(a>_l) a=_l;
	for(int i=_d;i<_d+a;i++)
	_b[i]=(byte)read();
	return a;
	*/

      _b[_d]=(byte)read();
      return 1;
    }
  }

  public static final int HISTORY_SIZE=10;

  public final class EditableInStream extends InStream
  {
    final Object STRM_LOCK=new Object();
    final Object EDIT_LOCK=new Object();
    
    private boolean           canonic_;

    //private BuTerminalKey[]   keys_;

    private FuCompletor         completor_;
    char              fcc_;
    private int               color_;
    private byte              style_;
    
    String            buf_;
    String[]          history_;
    int               previous_;
    private KeyListener       kl_;

    boolean           close_;
    int               scol_;
    int bcol_;
    String            search_;

    protected void addHistory(String _s)
    {
      if("".equals(_s.trim()))   return;
      if(history_[0].equals(_s)) return;

      for(int i=0;i<HISTORY_SIZE;i++)
	if(history_[i].equals(_s))
	{
	  history_[i]=history_[0];
	  history_[0]=_s;
	  return;
	}

      for(int i=HISTORY_SIZE-1;i>0;i--)
	history_[i]=history_[i-1];
      history_[0]=_s;
    }

    /*
    protected String previous()
    {
      return history_[0];
    }
    */

    @Override
    public boolean isCanonic()
    {
      return canonic_;
    }

    @Override
    public void setCanonic(boolean _canonic)
    {
      canonic_=_canonic;
    }

    @Override
    public void paste(String _s)
    {
      if(buf_.length()==0) { bcol_=0; scol_=cpos_; }
      super.paste(_s);
    }

    protected EditableInStream(int _color,byte _style)
    {
      buf_     ="";
      search_  ="";
      history_ =new String[HISTORY_SIZE];
      previous_=0;

      color_    =_color;
      style_    =_style;
      //keys_   =null;
      completor_=null;
      fcc_      ='\t';

      for(int i=0;i<HISTORY_SIZE;i++)
	history_[i]="";

      kl_=new KeyAdapter()
	{
	  @Override
    public void keyTyped(KeyEvent _evt)
	  {
	    traite(_evt.getKeyChar());
	    _evt.consume();
	  }

	  @Override
    public void keyPressed(KeyEvent _evt)
	  {
	    char clef=0;
	    int  code=_evt.getKeyCode();

	    if(code==KeyEvent.VK_LEFT     ) clef=2;  // B
	    if(code==KeyEvent.VK_RIGHT    ) clef=6;  // F
	    if(code==KeyEvent.VK_UP       ) clef=16; // P
	    if(code==KeyEvent.VK_DOWN     ) clef=14; // N
	    if(code==KeyEvent.VK_PAGE_UP  ) clef=16; // P
	    if(code==KeyEvent.VK_PAGE_DOWN) clef=14; // N
	    if(code==KeyEvent.VK_HOME     ) clef=1;  // A
	    if(code==KeyEvent.VK_END      ) clef=5;  // E

	    if(clef!=0)
	    {
	      traite(clef);
	      _evt.consume();
	    }
	  }

	  private void traite(final char _clef)
	  {
      char clef=_clef;
	    if(buf_.length()==0) { bcol_=0; scol_=cpos_; }

	    // System.err.print(""+(int)_clef);
	    cursor_=true;
	    //timer_.restart();
	    if(isCanonic())
	    {
	      add(clef);
	      return;
	    }

	    boolean beep   =false;
	    boolean refresh=false;

	    if(clef==fcc_) // completion
	    {
	      complete();
	      clef=0;
              refresh=true;
	    }

	    if(clef==1) // A
	    {
	      if(buf_.length()>0)
	      {
		cpos_=scol_;
                bcol_=0;
		refresh=true;
	      }

	      clef=0;
	      previous_=0;
	    }

	    if(clef==2) // B
	    {
	      if(buf_.length()>0)
	      {
		if(cpos_>scol_)
		{
		  cpos_--;
		  refresh=true;
		}
		else
                if(bcol_>0)
                {
                  bcol_--;
                  refresh=true;
                }
                else beep=true;
	      }

	      clef=0;
	    }

	    if(clef==3) // C
	    {
	      buf_="";
	      clef='\n';
	    }

	    if((clef==4)||(clef==127)) // D Suppr
	    {
	      synchronized(EDIT_LOCK)
	      {
		int l=buf_.length();
		if(l>0)
	        {
		  int i=bcol_+cpos_-scol_;
		  if((i<0)||(i>=l))
		    beep=true;
		  else
		  {
		    if(i==0) buf_=buf_.substring(1);
		    else if(i==l-1) buf_=buf_.substring(0,l-1);
		    else buf_=buf_.substring(0,i)+buf_.substring(i+1);
		    refresh=true;
		  }
		}
		else
	        {
		  if(clef==127)
		  {
		    beep=true;
		  }
		  else
		  {
		    synchronized(STRM_LOCK)
		    {
		      close_=true;
		      STRM_LOCK.notifyAll();
		    }
		  }
		}
	      }

	      clef=0;
	    }

	    if(clef==5) // E
	    {
	      cpos_=scol_+buf_.length();
              bcol_=0;
              if(cpos_>nbc_-2)
              {
                bcol_=buf_.length()-nbc_+2+scol_;
                cpos_=nbc_-2;
              }
              
	      refresh=true;
	      clef=0;
	    }

	    if(clef==6) // F
	    {
	      if(bcol_+cpos_<scol_+buf_.length())
	      {
		cpos_++;
		refresh=true;
	      }
	      else beep=true;

	      clef=0;
	    }

	    if(clef==7) // G
	    {
	      beep=true;
	      clef=0;
	    }

	    if(clef==8) // H 
	    {
	      synchronized(EDIT_LOCK)
	      {
		int l=buf_.length();
		if(l>0)
		{
		  int i=bcol_+cpos_-scol_-1;
		  if((i<0)||(i>=l))
		    beep=true;
		  else
		  {
		    if(i==0) buf_=buf_.substring(1);
		    else if(i==l-1) buf_=buf_.substring(0,l-1);
		    else buf_=buf_.substring(0,i)+buf_.substring(i+1);
		    cpos_--;
		    refresh=true;
		  }
		}
	      }
	      clef=0;
	    }

	    if((clef==9)&&(fcc_!=9))
	    {
	      clef=32;
	    }

	    // never 9 I \t

	    if(clef==11) // K
	    {
	      synchronized(EDIT_LOCK)
	      {
                int p=Math.min(Math.max(0,bcol_+cpos_-scol_),
                               buf_.length());
                copy(buf_.substring(p));
		buf_=buf_.substring(0,p);
		refresh=true;
	      }
	      clef=0;
	    }

	    if(clef==12) // L
	    {
	      clear_screen();
	      scol_=0;
	      cpos_=Math.min(buf_.length(),nbc_-2);
              bcol_=Math.max(0,buf_.length()-cpos_);
	      lpos_=Math.max(0,nbl_-24);
	      refresh=true;
	      clef=0;
	    }

	    //if(_clef==13) // M \r-->\n
            //_clef=10;

	    if(clef==10) // J
	    {
	      /*
	      if(buf_.startsWith("!ls"))
	      {
		OutStream out=createOutStream(getForeground().darker());
		BuFileFilter filtre=null;
		String ff=buf_.substring(3).trim();
		if(ff.startsWith("*.")) ff=ff.substring(2);
		if(!"".equals(ff)) filtre=new BuFileFilter(ff);
		try
		{
		  File dir=new File(System.getProperty("user.dir"));
		  String[] files=dir.list(filtre);
		  for(int i=0;i<files.length;i++)
		  {
		    out.write('\n');
		    out.write(files[i].getBytes());
		  }
		}
		catch(Exception ex)
		{
		  beep();
		  System.err.println(ex.getMessage());
		}
		buf_="";
	      }
	      */
	      /*
	      else
	      if(buf_.startsWith("!more"))
	      {
		OutStream out=createOutStream(getForeground().darker());
		String f=buf_.substring(6);
		try
		{
		  Properties pp=new Properties();
		  pp.load(new FileInputStream("java.stx"));
		  BuTerminalKey[] keys=new BuTerminalKey[pp.size()];
		  int i=0;
		  for(Enumeration e=pp.keys(); e.hasMoreElements(); )
		  {
		    String k=(String)e.nextElement();
		    try
		    {
		      String v    =(String)pp.get(k);
		      int    comma=v.indexOf(',');
		      int    color=Integer.parseInt(v.substring(0,comma),16);
		      String til  =v.substring(comma+1);
		      keys[i]=createKey(k,color,NORMAL,til);
		    }
		    catch(Exception ex)
		    {
		      keys[i]=createKey(k,Color.white.getRGB(),
					NORMAL,BuTerminalKey.WHOLE);
		    }
		    i++;
		  }
		  out.setKeys(keys);
		}
		catch(Exception ex)
		{
		  beep();
		  System.err.println(ex.getMessage());
		  return;
		}
		try
		{
		  LineNumberReader lin=new LineNumberReader(new FileReader(f));
		  while(true)
		  {
		    String l=lin.readLine();
		    if(l==null) break;
		    out.write(l.getBytes());
		    out.write('\n');
		  }
		}
		catch(Exception ex)
		{
		  beep();
		  System.err.println(ex.getMessage());
		}
		buf_="";
	      }
	      */
	    }

	    if(clef==15) // O
	    {
              beep=true;
              clef=0;
            }

	    if(clef==16) // P
	    {
	      if(previous_<0) search_=buf_;

	      if((previous_+1>=HISTORY_SIZE)||
		 ("".equals(history_[previous_+1])))
	      {
		beep=true;
	      }
	      else
	      {
		previous_++;
		cpos_=scol_;
                bcol_=0;
		buf_="";
		add(history_[previous_]);
		refresh=true;
	      }
	      clef=0;
	    }
	    
	    if(clef==14) // N
	    {
	      if(previous_<0)
		beep=true;
	      else
	      {
		previous_--;
		cpos_=scol_;
                bcol_=0;
		buf_="";
		if(previous_>=0) add(history_[previous_]);
		else             add(search_);
		refresh=true;
	      }
	      clef=0;
	    }
	    
	    if(clef==18) // R
	    {
	      boolean found=false;
	      synchronized(EDIT_LOCK)
	      {
		if(previous_==-1)
		{
		  addHistory(buf_);
		  search_=buf_;
		  previous_=0;
		}

		for(int i=previous_+1;i<history_.length;i++)
		  if(history_[i].indexOf(search_)>=0)
		  {
		    found=true;
		    previous_=i;
		    cpos_=scol_;
		    bcol_=0;
                    buf_="";
		    add(history_[i]);
		    break;
		  }
	      }
		  
	      if(found)
		refresh=true;
	      else
		beep=true;

	      clef=0;
	    }
	    
	    if(clef==27)
	    {
	      if(previous_!=-1)
	      {
		previous_=-1;
		cpos_=scol_;
                bcol_=0;
		buf_="";
		add(history_[0]);
		refresh=true;
	      }
	      clef=0;
	    }

	    if((clef>=17)&&(clef<=19)) // Q-S
	    {
              beep=true;
              clef=0;
            }

	    if(clef==20) // T
	    {
	      synchronized(EDIT_LOCK)
	      {
		int len=buf_.length();
		int pos=Math.min(Math.max(0,bcol_+cpos_-scol_),len-1);
		if(pos==0)
		{
		  beep=true;
		}
		else
		{
		  buf_=(pos==1 ? "" : buf_.substring(0,pos-1))
		    +buf_.charAt(pos  )
		    +buf_.charAt(pos-1)
		    +(pos==len-1 ? "" : buf_.substring(pos+1));
		  refresh=true;
		}
	      }
	      clef=0;
	    }

	    if(clef==21) // U
	    {
	      synchronized(EDIT_LOCK)
	      {
		int len=buf_.length();
		int pos=Math.min(Math.max(0,bcol_+cpos_-scol_),len);
		if(pos==0)
		{
		  beep=true;
		}
		else
		{
                  copy(buf_.substring(0,pos));
		  buf_=(pos==len ? "" : buf_.substring(pos));
                  cpos_=scol_;
                  bcol_=0;
		  refresh=true;
		}
	      }
	      clef=0;
	    }

	    if(clef==22) // V
	      clef=0;

	    if(clef==23) // W
	    {
	      synchronized(EDIT_LOCK)
	      {
		int len=buf_.length();
		int end=Math.min(Math.max(0,bcol_+cpos_-scol_),len);
		if(end==0)
		{
		  beep=true;
		}
		else
		{
		  int start=end-1;
		  while((start>=0)&&Character.isWhitespace(buf_.charAt(start)))
		    start--;
		  while((start>=0)&&!Character.isWhitespace(buf_.charAt(start)))
		    start--;

		  start++;
                  copy(buf_.substring(Math.max(0,start),Math.min(end,len)));
		  buf_=(start==0 ? "" : buf_.substring(0,start))
		    +(end==len ? "" : buf_.substring(end));
                  cpos_=scol_+start;
                  bcol_=0;
		  refresh=true;
		}
	      }
	      clef=0;
	    }

            if(clef==24) // X
            {
              beep=true;
              clef=0;
            }

	    if(clef==25) // Y
	    {
	      paste();
              refresh=true;
	      clef=0;
	    }

	    if(clef==26) // Z
	    {
              beep=true;
              clef=0;
            }

	    if(beep)    beep();
	    if(refresh) refresh();

	    //System.err.print("("+(int)_clef+")");
	    if(clef!=0)
	    {
	      previous_=-1;

              /*
	      int l=buf_.length();
	      if((_clef!='\n')&&(scol_+l+2>=nbc_))
	      {
		//System.err.println("CUT BUFFER");
		beep();
		if(scol_+1>=nbc_) scol_=0;
		buf_=buf_.substring(0,Math.max(0,Math.min(l,nbc_-2-scol_)));
		cpos_=scol_+l;
	        refresh();
	      }
	      else
              */
              add(clef);
	    }
	  }
	};
      addKeyListener(kl_);
    }

    /*
    public void setKeys(BuTerminalKey[] _keys)
    {
      keys_=_keys;
    }
    */

    @Override
    public void setCompletor(FuCompletor _completor)
    {
      completor_=_completor;
    }

    @Override
    public void setFastCompletionChar(char _char)
    {
      fcc_=_char;
    }

    void complete()
    {
      synchronized(EDIT_LOCK)
      {
        if(completor_!=null)
        {
          int p=Math.min(Math.max(0,cpos_-scol_),buf_.length());
          add(completor_.complete(buf_,p));
        }
      }

      /*
      String w;

      synchronized(EDIT_LOCK)
      {
	int i=buf_.length()-1;
	while((i>=0)&&Character.isJavaIdentifierPart(buf_.charAt(i))) i--;
	i++;
	w=buf_.substring(i);
      }

      boolean beep=true;

      if((keys_!=null)&&!"".equals(w))
      {
	int n=0;
	int f=-1;

	for(int j=0;j<keys_.length;j++)
	  if(keys_[j].word_.startsWith(w))
	    { f=j; n++; }

	if(n==1)
	{
	  if(keys_[f].word_.equals(w)) add(' ');
	  else add(keys_[f].word_.substring(w.length()));
	  beep=false;
	}
      }

      if(beep) beep();
      */
    }

    private boolean eol()
    {
      boolean r=false;
      synchronized(EDIT_LOCK)
      {
	if(buf_.length()!=0)
	  r=(buf_.charAt(buf_.length()-1)=='\n');
      }
      return r;
    }

    @Override
    public int read()
    {
      int r=-1;

      // TMP ?
      while(!canonic_&&!eol())
      {
	try
	{
	  synchronized(STRM_LOCK)
	  {
	    STRM_LOCK.wait();
	    if(close_) return -1;
	  }
	}
	catch (Exception e)
	{ System.err.println("\n### "+e+" ["+buf_+" ("+buf_.length()+") ]"); }
      }

      //if(eol())
      {
	try
	{
	  // if canonic then buf may be empty
	  if(buf_.length()==0)
	    synchronized(STRM_LOCK)
	    {
	      STRM_LOCK.wait();
	    }

	  synchronized(EDIT_LOCK)
	  {
	    r=buf_.charAt(0);
	    buf_=buf_.substring(1,buf_.length());
	  }
	}
	catch (Exception e)
	{ System.err.println("\n### "+e+" ["+buf_+" ("+buf_.length()+") ]"); }
      }

      return r;
    }

    @Override
    public void add(char _clef)
    {
      synchronized(STRM_LOCK)
      {
	synchronized(EDIT_LOCK)
	{
	  if(isCanonic())
	  {
	    buf_+=_clef;
	  }
	  else
	  {
	    insert(_clef,color_,style_);
      
	    if(_clef=='\n')
	    {
	      if(!"".equals(buf_)) addHistory(buf_);
	      buf_+=_clef;
	    }
	    else
	    {
	      int i=cpos_-scol_+bcol_;
	      if(i<=0)
		buf_=_clef+buf_;
	      else
		if((i>0)&&(i<buf_.length()))
		  buf_=buf_.substring(0,i-1)+_clef+buf_.substring(i-1);
		else
		  buf_+=_clef;
	      refresh();
	    }
	  }
	}
	STRM_LOCK.notifyAll();
      }
    }

    //TMP
    void refresh()
    {
      if(cpos_>=nbc_-5)
      {
        int d=Math.min(5,cpos_-scol_);
        cpos_-=d;
        bcol_+=d;
      }

      if(cpos_<=scol_+5)
      {
        int d=Math.min(5,bcol_);
        cpos_+=d;
        bcol_-=d;
      }

      int old=cpos_;
      cpos_=scol_;
      synchronized(EDIT_LOCK)
      {
	int l=buf_.length();
        //FuLog.debug("BTD: refresh bcol="+bcol_+" l="+l);
        int m=bcol_+nbc_-2-scol_;
        if(m<l) l=m;
	for(int i=bcol_;i<l;i++)
	{
          char c=buf_.charAt(i);
          if((i==bcol_)&&(bcol_>0)) c='\u00ab';
          if((i==l-1)&&(m==l))      c='\u00bb';
          insert(c,color_,style_);
        }
	//cpos_=scol_+l-bcol_;
      }
      clear_eol();
      cpos_=old;

      SwingUtilities.invokeLater(new Runnable()
        {
          @Override
          public void run()
          {
            scrollRectToVisible
              (new Rectangle(0,
                             l_to_y(lpos_),
                             wc_-1,hc_-4));
            /*
            scrollRectToVisible
              (new Rectangle(0,
                             l_to_y(lpos_)-hc_,
                             c_to_x(cpos_)+wc_-1,3*hc_-4));
            */
            scrollRectToVisible
              (new Rectangle(c_to_x(cpos_)-10*wc_,
                             l_to_y(lpos_),
                             21*wc_-1,hc_-4));
            scrollRectToVisible
              (new Rectangle(c_to_x(cpos_),
                             l_to_y(lpos_),
                             wc_-1,hc_));
          }
        });
    }
    
    @Override
    public int available()
    {
      if(close_) return 1;
      if(!canonic_&&!eol()) return 0;
      return buf_.length();
    }

    @Override
    public void close()
    {
      close_=true;
    }

    @Override
    public void mark(int _p)       { }
    @Override
    public boolean markSupported() { return false; }
    @Override
    public void reset()            { }
    @Override
    public int read(byte[] _b)     { return read(_b,0,_b.length); }
  
    @Override
    public int read(byte[] _b, int _d, int _l)
    {
      /*
      if(!close_)
      {
	if(_l==0) return 0;
	if(!canonic_&&!eol()) return 0;
      }
      */

      while(available()==0)
      {
	try { Thread.sleep(100L); }
	catch(InterruptedException ex) { }
      }

      _b[_d]=(byte)read();
      return 1;
    }

    /*
    public long skip(long _l)
    {
      System.err.println("skip");

      long i;
      for(i=0; i<_l; i++)
	if(read()==-1) break;
      return i;
    }
    */
  }

  class Repainter implements ActionListener
  {
    private final Object LOCK=new Object();

    private Rectangle r_;
    private BuTimer   t_=new BuTimer(DELAY,this);
    private boolean   s_;

    public void start()
    {
      if(!s_)
	synchronized(LOCK)
	{
	  s_=true;
	  t_.start();
	}
    }

    public void stop()
    {
      if(s_)
	synchronized(LOCK)
	{
	  t_.stop();
	  s_=false;
	}
    }

    public void update(int _x,int _y,int _w, int _h)
    {
      synchronized(LOCK)
      {
	if(r_==null)  r_=new Rectangle(_x,_y,_w,_h);
	else r_=r_.union(new Rectangle(_x,_y,_w,_h));
      }
    }

    @Override
    public void actionPerformed(ActionEvent _evt)
    {
      if(r_!=null)
      {
	Rectangle r;

	synchronized(LOCK)
	{
	  r=r_;
	  r_=null;
	}

	repaint(r.x,r.y,r.width,r.height);
      }

      if(!isShowing()) stop();
    }
  }

  /*
  private class Repainter extends Thread
  {
      private Rectangle r=null;
      private Object lock=new Object();

      public void update(int _x,int _y,int _w, int _h)
      {
	  synchronized (lock)
	  {
	      if(r==null)  r=new Rectangle(_x,_y,_w,_h);
	      else r=r.union(new Rectangle(_x,_y,_w,_h));
	  }
      }

      public void run()
      {
	  while(true)
	  {
	      try { Thread.sleep(DELAY); }
	      catch(InterruptedException ex) { }
	      if((r!=null)&&isShowing())
	      {
		  synchronized (lock)
		  {
		      //repaint(DELAY,r.x,r.y,r.width,r.height);
		      repaint(0,r.x,r.y,r.width,r.height);
		      r=null;
		  }
	      }
	  }
	  //super.run();
      }
  }
  */
}
