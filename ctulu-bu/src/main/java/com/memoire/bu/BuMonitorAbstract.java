/**
 * @modification $Date: 2006-12-20 16:45:45 $
 * @statut       unstable
 * @file         BuMonitorAbstract.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

/**
 * A base class for monitors.
 */

public abstract class BuMonitorAbstract extends JComponent
// implements Runnable
{
  private BuCommonInterface app_;
  // private WeakReference timer_;

  protected static class MonitorTimer implements ActionListener {
    BuTimer timer_;
    WeakReference ref_;

    MonitorTimer(BuMonitorAbstract _target) {
      timer_ = new BuTimer((int) _target.getDelay(), this);
      ref_ = new WeakReference(_target);
      timer_.start();
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      BuMonitorAbstract target = (BuMonitorAbstract) ref_.get();

      if (target == null || !target.isShowing()) {
        timer_.stop();
        return;
      }
      JFrame top = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, target);
      if (target.isVisible() && target.refreshState()) {
        BuUpdateGUI.repaintLater(target);
        target.setToolTipText(target.getToolTipText());
      }

    }
  }

  public BuMonitorAbstract() {
    this(null);
  }

  public BuMonitorAbstract(BuCommonInterface _app) {
    super();
    app_ = _app;

    setName("buMONITOR_ABSTRACT");
    setOpaque(true);
    setBackground(Color.gray);
    setForeground(new Color(32, 96, 64));
    setBorder(new LineBorder(Color.black));
    setPreferredSize(new Dimension(64, 14));
    setToolTipText("...");
    new MonitorTimer(this);

  }

  @Override
  public boolean isShowing() {
    boolean r = super.isShowing();
    // activeTimer(r);

    return r;
  }

  /*
   * private void activeTimer(boolean _r) { BuTimer t = (BuTimer) timer_.get(); if (!_r) System.out.println(hashCode() + "
   * on active timer " + _r); if (_r && (t == null)) { t = createTimer(); timer_ = new WeakReference(t); } if (t !=
   * null) { if (_r && !t.isRunning()) t.start(); if (!_r && t.isRunning()) t.stop(); } }
   */

  public final BuCommonInterface getApp() {
    return app_;
  }

  public final void setApp(BuCommonInterface _app) {
    app_ = _app;
  }

  public abstract int getValue();

  public abstract int getMaximum();

  public abstract String getText();

  // public abstract String getToolTipText();
  public abstract long getDelay();

  public abstract boolean refreshState();

  @Override
  public final Point getToolTipLocation(MouseEvent _evt) {
    Point r = super.getToolTipLocation(_evt);
    if (r == null) {
      JToolTip tt = new JToolTip();
      tt.setToolTipText(getToolTipText());
      r = new Point(0, -tt.getPreferredSize().height - 1);
    }
    return r;
  }

  @Override
  public final boolean isOpaque() {
    return true;
  }

  @Override
  public final void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  @Override
  public final void paintComponent(Graphics _g) {
    // super.paintComponent(_g);
    // System.out.println(hashCode()+ " on repaint" );
    // activeTimer(true);
    paintBackground(_g);
    paintBar(_g);
    paintText(_g);
  }

  public void paintBackground(Graphics _g) {
    // if(isOpaque())
    {
      Color bg = getBackground();
      BuLib.setColor(_g, bg);
      _g.fillRect(0, 0, getWidth(), getHeight());
    }
  }

  public void paintBar(Graphics _g) {
    Dimension size = getSize();
    Insets insets = getInsets();

    int w = size.width - insets.left - insets.right;
    int h = size.height - insets.top - insets.bottom;
    int x = getValue() * w / 100;
    int xmax = getMaximum() * w / 100;

    Color bg = getBackground();
    Color fg = getForeground();

    if (xmax > 0) {
      BuLib.setColor(_g, bg.brighter());
      _g.fillRect(insets.left, insets.top, xmax, h);
    }

    if (x > 0) {
      BuLib.setColor(_g, fg);
      _g.fillRect(insets.left, insets.top, x - 1, h);

      BuLib.setColor(_g, fg.brighter());
      _g.drawLine(insets.left + x - 1, insets.top, insets.left + x - 1, insets.top + h);

      BuLib.setColor(_g, fg.brighter().brighter());
      _g.drawLine(insets.left + x, insets.top, insets.left + x, insets.top + h);
    }
  }

  public void paintText(Graphics _g) {
    Dimension size = getSize();
    Insets insets = getInsets();

    int w = size.width - insets.left - insets.right;
    int h = size.height - insets.top - insets.bottom;

    String m = getText();
    int xm = (w - _g.getFontMetrics().stringWidth(m)) / 2 + insets.left;
    int ym = insets.top + h;

    Color fg = getForeground();

    BuLib.setColor(_g, fg.darker());
    _g.drawString(m, xm + 2, ym - 1);
    _g.drawString(m, xm + 2, ym - 2);
    _g.drawString(m, xm + 1, ym - 1);
    BuLib.setColor(_g, fg.brighter().brighter());
    _g.drawString(m, xm + 1, ym - 2);
  }

  /*
   * public final void run() { while (true) { if(refreshState()) { BuUpdateGUI.repaintLater(this);
   * setToolTipText(getToolTipText()); } try { Thread.sleep(getDelay()); } catch(Exception ex) { } } }
   */
}
