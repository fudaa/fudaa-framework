/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuCommandPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * A panel where the user can choose which elements to
 * put on his toolbar.
 *
 * command.*
 */
public class BuCommandPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  static final String[][] CODES=
  {
  // name              default short           long                       command            enabled
  { "create"          ,"true" ,"Cr�er"        ,"Cr�er..."                ,"CREER"           ,"false" },
  { "open"            ,"true" ,"Ouvrir"       ,"Ouvrir..."               ,"OUVRIR"          ,"false" },
  { "save"            ,"true" ,"Enregistrer"  ,"Enregistrer"             ,"ENREGISTRER"     ,"false" },
  { "save_as"         ,"false","Enregistrer"  ,"Enregistrer sous..."     ,"ENREGISTRERSOUS" ,"false" },
  { "close"           ,"false","Fermer"       ,"Fermer..."               ,"FERMER"          ,"false" },
  { "separator1"      ,"true" ,"-"            ,""                        ,""                ,"true"  },
  { "print"           ,"true" ,"Imprimer"     ,"Imprimer..."             ,"IMPRIMER"        ,"false" },
  { "preview"         ,"false","Pr�visualiser","Pr�visualiser..."        ,"PREVISUALISER"   ,"false" },
  { "separator2"      ,"true" ,"-"            ,""                        ,""                ,"true"  },
  { "undo"            ,"true" ,"D�faire"      ,"D�faire"                 ,"DEFAIRE"         ,"false" },
  { "redo"            ,"true" ,"Refaire"      ,"Refaire"                 ,"REFAIRE"         ,"false" },
  { "separator3"      ,"true" ,"-"            ,""                        ,""                ,"true"  },
  { "copy"            ,"true" ,"Copier"       ,"Copier"                  ,"COPIER"          ,"false" },
  { "cut"             ,"true" ,"Couper"       ,"Couper"                  ,"COUPER"          ,"false" },
  { "paste"           ,"true" ,"Coller"       ,"Coller"                  ,"COLLER"          ,"false" },
  { "duplicate"       ,"false","Dupliquer"    ,"Dupliquer"               ,"DUPLIQUER"       ,"false" },
  { "separator4"      ,"true" ,"-"            ,""                        ,""                ,"true"  },
  { "select_all"      ,"true" ,"S�lectionner" ,"Tout s�lectionner"       ,"TOUTSELECTIONNER","false" },
  { "find"            ,"true" ,"Rechercher"   ,"Rechercher..."           ,"RECHERCHER"      ,"false" },
  { "replace"         ,"true" ,"Remplacer"    ,"Remplacer..."            ,"REMPLACER"       ,"false" },
  { "separator5"      ,"true" ,"-"            ,""                        ,""                ,"true"  },
  { "fullscreen"      ,"false","Ecran"        ,"Plein �cran"             ,"PLEINECRAN"      ,"true"  },
  { "waterfall"       ,"false","Cascade"      ,"Ranger en cascade"       ,"CASCADE"         ,"true"  },
  { "tile"            ,"false","Mosaique"     ,"Ranger en mosaique"      ,"MOSAIQUE"        ,"true"  },
  { "arrange_icons"   ,"true" ,"Ic�nes"       ,"Ranger les ic�nes"       ,"RANGERICONES"    ,"true"  },
  { "arrange_palettes","true" ,"Palettes"     ,"Ranger les palettes"     ,"RANGERPALETTES"  ,"true"  },
  { "swap_columns"    ,"false","Echanger"     ,"Echanger les colonnes"   ,"ECHANGER"        ,"true"  },
  { "separator6"      ,"true" ,"-"            ,""                        ,""                ,"true"  },
  { "preference"      ,"false","Pr�f�rences"  ,"Pr�f�rences de d�marrage","PREFERENCE"      ,"false" },
  { "help"            ,"false","Aide"         ,"Aide en-ligne"           ,"AIDE"            ,"true"  },
  { "help_pointer"    ,"true" ,"Pointeur"     ,"Pointeur d'aide"         ,"POINTEURAIDE"    ,"true"  },
  { "tip"             ,"false","Astuces"      ,"Astuces"                 ,"ASTUCE"          ,"false" },
  };

  /*
  private static final String[] CODES=new String[]
  {
    "create",
    "open",
    "save",
    "save_as",
    "close",
    "separator1",
    "print",
    "preview",
    "separator2",
    "undo",
    "redo",
    "separator3",

    "copy",
    "cut",
    "paste",
    "duplicate",
    "separator4",
    "select_all",
    "find",
    "replace",
    "separator5",
    "fullscreen",
    "waterfall",
    "arrange_icons",
    "arrange_palettes",
    "swap_columns",

    "separator6",
    "help",
    "help_pointer",
  };

  private static final boolean[] DEFAUTS=new boolean[]
  {
    true,
    true,
    true,
    true,
    true,
    true,
    false,
    true,
    true,
    true,
    true,
    true,
    false,
  };

  private static final String[] INTITULES=new String[]
  {
    "Colonne gauche",
    "Outils sp�cifiques",
    "Colonne droite",
    "Barre d'�tat",
    "Colonnes invers�es",
    "Assistant",
    "Ic�ne de bouton",
    "D�tachement des barres",
    "Texte de bouton",
    "Survol des barres",
    "Ecran d'accueil",
    "Bureau � onglets",
  };
  */

  BuCommonInterface appli_;
  BuPreferences     options_;
  BuCheckBox[]      cbEtats_;

  @Override
  public String getTitle()
  {
    return getS("Barre");
  }

  @Override
  public String getCategory()
  {
    return getS("Visuel");
  }

  // Constructeur
  
  public BuCommandPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_     =_appli;
    options_   =BuPreferences.BU;

    BuPanel q=new BuPanel();
    q.setLayout(new BuGridLayout(2,5,2));
    q.setBorder(new EmptyBorder(0,5,0,5));

    Font ft=BuLib.deriveFont("Label",Font.PLAIN,-2);

    cbEtats_=new BuCheckBox[CODES.length];
    for(int i=0;i<CODES.length;i++)
    {
      cbEtats_[i]=new BuCheckBox();
      cbEtats_[i].setName("cb"+CODES[i][4]);
      cbEtats_[i].setActionCommand(CODES[i][4]);
      cbEtats_[i].setText(getS(CODES[i][2]));
      cbEtats_[i].addActionListener(this);
      q.add(cbEtats_[i]);
      BuLabel lb=new BuLabel();
      String  tt=getS(CODES[i][3]);
      if(!"".equals(tt))
      {
	lb.setText(tt);
	lb.setIcon(BuResource.BU.loadToolCommandIcon(CODES[i][4]));
      }
      lb.setFont(ft);
      q.add(lb);
    }

    BuPanel p=new BuPanel();
    p.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getTitle()),
	EMPTY5555));
    p.setLayout(new BuBorderLayout());
    p.add(new BuScrollPane(q),BuBorderLayout.CENTER);
    p.setPreferredSize(new Dimension(p.getPreferredSize().width,200));

    setLayout(new BuBorderLayout());
    setBorder(EMPTY5555);
    add(p,BuBorderLayout.CENTER);

    for(int i=0;i<CODES.length;i++)
      cbEtats_[i].setSelected("true".equals(CODES[i][1]));

    updateComponents();
  }
  
  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
  }

  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  /*
  public boolean isPreferencesApplyable()
    { return true; }
  
  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }
  */

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }
  
  // Methodes privees
  
  private void fillTable()
  {
    for(int i=0;i<CODES.length;i++)
      options_.putBooleanProperty
	  ("command."+CODES[i][0],cbEtats_[i].isSelected());
    setDirty(false);
  }

  private void updateComponents()
  {
    for(int i=0;i<CODES.length;i++)
      cbEtats_[i].setSelected
	(options_.getBooleanProperty
	 ("command."+CODES[i][0],"true".equals(CODES[i][1])));
    setDirty(false);
  }
}

