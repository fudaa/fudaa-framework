/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuGlassPaneHelp.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;

/**
 * Used by pointer-sensitive help.
 */

public class BuGlassPaneHelp
       extends JComponent
{
  private Listener      listener_;
  BuApplication frame_;
  private JComponent    target_;

  public BuGlassPaneHelp(BuApplication _frame)
  {
    setName("gpGLASSPANE_HELP");
    setOpaque(false);
    setVisible(false);
    setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

    listener_=null;
    frame_=_frame;
    target_=null;

    // listener_=new Listener(_frame,this);
    // addMouseListener(listener_);
    // addMouseMotionListener(listener_);
  }

  public JComponent getTarget()
  {
    return target_;
  }

  public void setTarget(JComponent _target)
  {
    if(target_!=_target)
    {
      target_=_target;
      repaint();
    }
  }

  /*
  protected void processMouseEvent(MouseEvent _evt)
  {
    System.err.println("GP.EVT="+_evt);
    super.processMouseEvent(_evt);
  }
  */

  /*
  boolean actif_=true;
  public void setActif(boolean _v)
  {
    actif_=_v;
    removeMouseListener(listener_);
    removeMouseMotionListener(listener_);
  }
  */

  @Override
  public void setVisible(boolean _v)
  {
    if(isVisible()==_v) return;

    if(listener_==null) listener_=new Listener();

    if(_v)
    {
      addMouseListener(listener_);
      addMouseMotionListener(listener_);
    }
    else
    {
      removeMouseListener(listener_);
      removeMouseMotionListener(listener_);
    }

    super.setVisible(_v);
  }

  @Override
  public void paintComponent(Graphics _g)
  {
    // System.err.println("PAINT-GLASSPANE");
    super.paintComponent(_g);

    /*
    _g.setColor(Color.yellow);
    _g.fillRect(0,0,getWidth(),getHeight());
    */

    /*
      System.err.println("GlassPaneHelp");
      Dimension d     =getSize();
      Insets    insets=getInsets();
      Rectangle clip  =g.getClipBounds();
    */

    if(target_!=null)
    {
      Rectangle r=target_.getVisibleRect();
      Point     p=SwingUtilities.convertPoint
	  (target_,new Point(r.x,r.y),this); //(0,0)
      int       w=r.width-1;  //target_.getSize().width-1;
      int       h=r.height-1; //target_.getSize().height-1;

      /*
      _g.setXORMode(new Color(192,192,192));
      _g.setColor(new Color(224,192,192));
      _g.fillRect(p.x,p.y,w+1,h+1);
      _g.setPaintMode();
      */

      _g.setColor(Color.red);
      _g.drawRect(p.x,p.y,w,h);

      JComponent cp=getTarget();
      if(cp instanceof JInternalFrame)
	cp=((JInternalFrame)cp).getRootPane();

      String param=(String)cp.getClientProperty("HELP_URL");
      
      Container pt=cp;
      while((pt!=null)&&(param==null))
      {
	pt=pt.getParent();
	if(pt instanceof JComponent)
	  param=(String)((JComponent)pt).getClientProperty("HELP_URL");
      }

      String ancre=target_.getName();
      if(ancre!=null)
      {
	int i=0;
	while(i<ancre.length())
	{
	  int c=ancre.charAt(i);
	  if((c<'A')||(c>'Z')) i++;
	  else break;
	}
	ancre=ancre.substring(i).toLowerCase().replace('_','-');
	if(!"".equals(ancre)) param+="#"+ancre;

	_g.setFont(new Font("SansSerif",Font.PLAIN,10));
	int ws=_g.getFontMetrics().stringWidth(param);
	_g.drawString(param,getSize().width-ws-10,12);
      }
    }
  }

  class Listener
  implements MouseListener, MouseMotionListener
  {
    public Listener() // BuGlassPaneHelp _glassPane)
    {
    }

    @Override
    public void mouseClicked(MouseEvent _evt)
    {
      redispatchMouseEvent(_evt, false);
      setVisible(false);
      frame_.getImplementation().contextHelp(getTarget());
    }
    
    @Override
    public void mouseEntered(MouseEvent _evt)
    {
      // redispatchMouseEvent(_evt, false);
    }
    
    @Override
    public void mouseExited(MouseEvent _evt)
    {
      // redispatchMouseEvent(_evt, false);
    }
    
    @Override
    public void mousePressed(MouseEvent _evt)
    {
      // redispatchMouseEvent(_evt, false);
    }
    
    @Override
    public void mouseReleased(MouseEvent _evt)
    {
      // redispatchMouseEvent(_evt, true);
    }
    
    @Override
    public void mouseMoved(MouseEvent _evt)
    {
      redispatchMouseEvent(_evt, false);
    }
    
    @Override
    public void mouseDragged(MouseEvent _evt)
    {
      // redispatchMouseEvent(_evt, false);
    }
    
    private void redispatchMouseEvent(MouseEvent _evt,
				      boolean _repaint)
    {
      Point     glassPanePoint=_evt.getPoint();
      Component component     =null;
      Container container     =frame_.getContentPane();
      
      Point containerPoint=SwingUtilities.convertPoint
	(BuGlassPaneHelp.this,glassPanePoint,container);

      if(containerPoint.y<0)
      {
	component=frame_.getMainMenuBar();
      }
      else
      {
	component=SwingUtilities.getDeepestComponentAt
	  (container,containerPoint.x,containerPoint.y);
      }

      if(component instanceof JComponent)
      {
	JComponent c=(JComponent)component;
	String     n=c.getName();

	while(n==null)
	{
	  component=c.getParent();
	  if(component instanceof JComponent)
	  {
	    c=(JComponent)component;
	    n=c.getName();
	  }
	  else break;
	}

	if(n!=null)
	{
	  setTarget(c);
	  // System.err.println("Over "+n);
	}
      }
    }
  }
}
