/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut unstable
 * @file BuDrawer.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.*;

/**
 * A drawer.
 */
public class BuDrawer
    extends JWindow
    implements SwingConstants
    //ActionListener, FuEmptyArrays
{
  private JFrame frame_;
  private int position_;
  boolean closed_;
  int width_;
  private int height_;

  public BuDrawer(JFrame _frame, int _position) {
    super(_frame);
    frame_ = _frame;
    position_ = _position;
    closed_ = true;

    frame_.addWindowListener(new WindowListener() {
      @Override
      public void windowActivated(WindowEvent _evt) {
        relocate();
      }

      @Override
      public void windowDeactivated(WindowEvent _evt) {
        relocate();
        //toBack();
      }

      @Override
      public void windowClosing(WindowEvent _evt) {
        relocate();
      }

      @Override
      public void windowClosed(WindowEvent _evt) {
        relocate();
      }

      @Override
      public void windowIconified(WindowEvent _evt) {
        relocate();
      }

      @Override
      public void windowDeiconified(WindowEvent _evt) {
        relocate();
      }

      @Override
      public void windowOpened(WindowEvent _evt) {
        relocate();
      }
    });
    frame_.addComponentListener(new ComponentListener() {
      @Override
      public void componentHidden(ComponentEvent _evt) {
        relocate();
      }

      @Override
      public void componentMoved(ComponentEvent _evt) {
        relocate();
      }

      @Override
      public void componentResized(ComponentEvent _evt) {
        relocate();
      }

      @Override
      public void componentShown(ComponentEvent _evt) {
        relocate();
      }
    });

    //BuLib.setUndecorated(this,true);
    final JScrollPane s = new JScrollPane(new JTree());
    s.setBorder(new EmptyBorder(0, 0, 0, 0));
    s.setPreferredSize(new Dimension(120, 120));
    getContentPane().add(s);//BuButton("xxx"));

    ((JComponent) getContentPane()).setBorder
        (createBorder(position_));

    /*
    BuButton cb=new BuButton();
    cb.setPreferredSize(new Dimension(5,5));
    getContentPane().add(cb,BorderLayout.EAST);
    cb.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent _evt)
        {
          closed_=!closed_;
          s.setVisible(!closed_);
          ((JComponent)getContentPane()).invalidate();
          //((JComponent)getContentPane()).doLayout();
          //((JComponent)getContentPane()).validate();
          relocate();
        }
      });
    */

    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent _evt) {
        closed_ = !closed_;
        s.setVisible(!closed_);
        ((JComponent) getContentPane()).invalidate();
        //((JComponent)getContentPane()).doLayout();
        //((JComponent)getContentPane()).validate();
        relocate();
      }
    });

    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseDragged(MouseEvent _evt) {
        if (closed_) {
          return;
        }
        width_ = Math.max(0, _evt.getX() + 2);
        if (!closed_ && (width_ < 30)) {
          closed_ = true;
          width_ = getPreferredSize().width;
        }
        ((JComponent) getContentPane()).invalidate();
        //((JComponent)getContentPane()).doLayout();
        //((JComponent)getContentPane()).validate();
        relocate();
      }
    });

    pack();
    width_ = getWidth();
    height_ = getHeight();
  }

  /*
  public void paint(Graphics _g)
  {
    super.paint(_g);
    _g.setColor(new Color(0,0,0,48));
    _g.fillRect(0,0,10,getHeight());
  }
  */

  public void relocate() {
    boolean v = frame_.isVisible() && ((frame_.getState() & Frame.ICONIFIED) == 0);
    if (v != isVisible()) {
      setVisible(v);
    }
    Rectangle r = frame_.getBounds();
    int x, y, w, h;

    switch (position_) {
      case LEFT:
        y = r.y + 20;
        w = closed_ ? 5 : width_;
        x = r.x - w;
        h = Math.max(0, r.height - 27);
        break;
      case BOTTOM:
        x = r.x + 20;
        w = Math.max(0, r.width - 40);
        y = r.y + r.height + 3; //???
        h = closed_ ? 5 : height_;
        break;
      case TOP:
        x = r.x + 20;
        w = Math.max(0, r.width - 40);
        h = closed_ ? 5 : height_;
        y = r.y - h;
        break;
      default:
        x = r.x + r.width;
        y = r.y + 20;
        w = closed_ ? 5 : width_;
        h = Math.max(0, r.height - 27);
    }

    Rectangle b = getBounds();
    if ((b.x != x) || (b.y != y) || (b.width != w) || (b.height != h)) {
      setBounds(x, y, w, h);
      validateTree();
      repaint();
      //System.err.println("XY="+x+","+y+" WH="+w+","+h);
    }
  }

  protected Border createBorder(int _position) {
    Border r = null;

    switch (_position) {
      case LEFT:
        r = new CompoundBorder(new MatteBorder(1, 1, 1, 0, Color.black),
            new MatteBorder(0, 4, 0, 0, Color.gray));
        break;
      case BOTTOM:
        r = new CompoundBorder(new MatteBorder(0, 1, 1, 1, Color.black),
            new MatteBorder(0, 0, 4, 0, Color.gray));
        break;
      case TOP:
        r = new CompoundBorder(new MatteBorder(1, 1, 0, 1, Color.black),
            new MatteBorder(4, 0, 0, 0, Color.gray));
        break;
      default:
        r = new CompoundBorder(new MatteBorder(1, 0, 1, 1, Color.black),
            new MatteBorder(0, 0, 0, 4, Color.gray));
        break;
    }

    return r;
  }
}
