/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuSlider.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import javax.swing.JSlider;

/**
 * A slider.
 */
public class BuSlider
  extends JSlider
{
  public BuSlider()
  {
    this(HORIZONTAL,0,100,50);
  }

  public BuSlider(int _orientation)
  {
    this(_orientation,0,100,50);
  }

  public BuSlider(int _min,int _max)
  {
    this(HORIZONTAL,_min,_max,(_min+_max)/2);
  }

  public BuSlider(int _min,int _max,int _val)
  {
    this(HORIZONTAL,_min,_max,_val);
  }

  public BuSlider(int _orientation,int _min,int _max,int _val)
  {
    super(_orientation,_min,_max,_val);
    setSize(getPreferredSize());
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }
}
