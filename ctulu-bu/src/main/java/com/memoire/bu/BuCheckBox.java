/**
 * @modification $Date: 2007-01-17 10:45:27 $
 * @statut       unstable
 * @file         BuCheckBox.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import javax.swing.Icon;
import javax.swing.JCheckBox;

/**
 * The Swing checkbox...
 */
public class BuCheckBox extends JCheckBox {
  public BuCheckBox() {
    this((String) null);
  }

  public BuCheckBox(String _text) {
    super();

    setText((_text == null) ? "" : _text);
  }

  public BuCheckBox(Icon _icon) {
    this(null, _icon, false);
  }

  public BuCheckBox(Icon _icon, boolean _selected) {
    this(null, _icon, _selected);
  }

  public BuCheckBox(String _text, boolean _selected) {
    this(_text, null, _selected);
  }

  public BuCheckBox(String _text, Icon _icon) {
    this(_text, _icon, false);
  }

  public BuCheckBox(String _text, Icon _icon, boolean _selected) {
    super();
    setIcon(_icon);
    setText(_text);
    setSelected(_selected);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  // Focus

  @Override
  protected void processFocusEvent(FocusEvent _evt) {
    BuLib.focusScroll(this, _evt);
    super.processFocusEvent(_evt);
  }
}
