/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuToggleButton.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.plaf.ColorUIResource;

/**
 * Like JToggleButton but with better management of icons.
 */
public class BuToggleButton
  extends JToggleButton
{
  public BuToggleButton()
  {
    this(null,null);
  }

  public BuToggleButton(BuIcon _icon)
  {
    this(_icon,null);
  }

  public BuToggleButton(String _label)
  {
    this(null,_label);
  }

  public BuToggleButton(BuIcon _icon, String _label)
  {
    super();

    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setIcon(_icon);
    setText(_label);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public boolean isOpaque()
  {
    boolean r=super.isOpaque();

    Container parent=getParent();

    if(parent instanceof JComponent)
    {
      Object p=((JComponent)parent)
	  .getClientProperty("JToolBar.isRollover");
      if(Boolean.TRUE.equals(p))
	  r&=isEnabled()&&model.isRollover();
    }

    return r;
  }

  @Override
  public int getHorizontalAlignment()
  {
    int r=super.getHorizontalAlignment();
    if((r==CENTER)&&(getText()!=null))
      r=LEFT;
    return r;
  }

  @Override
  public void setText(String _text)
  {
    super.setText("".equals(_text)?null:_text);

    if(  !BuPreferences.BU.getBooleanProperty("button.text",true)
       &&(getToolTipText()==null))
      super.setToolTipText(_text);
  }

  @Override
  public String getText()
  {
    if(  BuPreferences.BU.getBooleanProperty("button.text",true)
       ||(super.getIcon()==null)
       ||!BuPreferences.BU.getBooleanProperty("button.icon",true))
      return super.getText();
    return null;
  }

  public void setIcon(BuIcon _icon)
    { BuLib.setIcon(this,_icon); }

  @Override
  public Icon getIcon()
  {
    if(  BuPreferences.BU.getBooleanProperty("button.icon",true)
       ||(super.getText()==null)
       ||!BuPreferences.BU.getBooleanProperty("button.text",true))
      return super.getIcon();
    return null;
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(0,getHeight()+1);
    return r;
  }

  @Override
  public void updateUI()
  {
    super.updateUI();
    setRolloverEnabled(true);
  }

  @Override
  public Color getBackground()
  {
    Color r;
    if(isEnabled()&&model.isRollover()&&BuLib.isMetal())
      r=new ColorUIResource(192,192,208);
    else
      r=super.getBackground();
    return r;
  }

  @Override
  public void paintBorder(Graphics _g)
  {
    Container   parent=getParent();
    //ButtonModel model =getModel();

    if(!model.isRollover()&&
       !model.isSelected()&&
       !(model.isPressed()&&model.isArmed())&&
       BuLib.isMetal()&&
       (parent instanceof JComponent)&&
       Boolean.TRUE.equals
       (((JComponent)parent).getClientProperty("JToolBar.isRollover")))
      ;
    else
      super.paintBorder(_g);
  }

  @Override
  public void paintComponent(Graphics _g)
  {
    //ButtonModel model=getModel();

    if(isOpaque()&&
       model.isRollover()&&
       !model.isSelected()&&
       !(model.isPressed()&&model.isArmed())&&
       BuLib.isMetal())
      {
	_g.setColor(getBackground());
	_g.fillRect(0,0,getWidth(),getHeight());
      }

    super.paintComponent(_g);
  }

  // Focus

  @Override
  protected void processFocusEvent(FocusEvent _evt)
  {
    BuLib.focusScroll(this,_evt);
    super.processFocusEvent(_evt);
  }
}


