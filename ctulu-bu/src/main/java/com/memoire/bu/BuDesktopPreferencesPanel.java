/**
 * @modification $Date: 2006-09-19 14:35:08 $
 * @statut       unstable
 * @file         BuDesktopPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose which elements to
 * put on his desktop.
 *
 * see below.
 */
public class BuDesktopPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener, BuBorders
{
  private static final int NBL=18;

  private static final String[] CODES=new String[]
  {
    "leftcolumn.visible",
    "specificbar.visible",
    "rightcolumn.visible",
    "statusbar.visible",
    "columns.swapped",
    "assistant.visible",
    "button.icon",
    "tool.icon",
    "button.text",
    "tool.text",
    "toolbar.floatable",
    "splashscreen.visible",
    "toolbar.rollover",
    "desktop.tabbed",
    "desktop.grid",
    "desktop.snap",
    "desktop.dots",
    "antialias.all",
  };

  private static final boolean[] DEFAUTS=new boolean[]
  {
    true,  // leftcolumn.visible
    true,  // specificbar.visible
    true,  // rightcolumn.visible
    true,  // statusbar.visible
    false, // columns.swapped
    true,  // assistant.visible
    true,  // button.icon
    true,  // tool.icon
    true,  // button.text
    true,  // tool.text
    true,  // toolbar.floatable
    false, // splashscreen.visible
    true,  // toolbar.rollover
    false, // desktop.tabbed
    false, // desktop.grid
    false, // desktop.snap
    false, // desktop.dots
    false, // antialias.all
  };

  private static final String[] INTITULES=new String[]
  {
    "Colonne gauche",
    "Outils sp�cifiques",
    "Colonne droite",
    "Barre d'�tat",
    "Colonnes invers�es",
    "Assistant",
    "Ic�ne de bouton",
    "Ic�ne d'outil",
    "Texte de bouton",
    "Texte d'outil",
    "D�tachement des barres",
    "Ecran d'accueil",
    "Survol des barres",
    "Bureau � onglets",
    "Grille",
    "Magn�tisme",
    "Points",
    "Anticr�nelage",
  };

  BuCommonInterface appli_;
  BuPreferences     options_;
  BuCheckBox[]      cbEtats_;

  @Override
  public String getTitle()
  {
    return getS("Bureau");
  }

  @Override
  public String getCategory()
  {
    return getS("Visuel");
  }

  // Constructeur
  
  public BuDesktopPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_     =_appli;
    options_   =BuPreferences.BU;

    setLayout(new BuVerticalLayout(5,true,false));
    setBorder(EMPTY5555);

    build();

    //for(int i=0;i<NBL;i++)
    //cbEtats_[i].setSelected(DEFAUTS[i]);

    updateComponents();
  }

  protected void build()
  {
    BuGridLayout lo=new BuGridLayout(2,5,5,true,true,false,false);
    lo.setSameWidth(true);
    lo.setSameHeight(true);

    BuPanel p1=new BuPanel(lo);
    p1.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getTitle()),
	EMPTY5555));

    cbEtats_=new BuCheckBox[NBL];
    for(int i=0;i<NBL;i++)
    {
      cbEtats_[i]=new BuCheckBox(getS(INTITULES[i]));
      cbEtats_[i].setName("cb"+CODES[i].replace('.','_').toUpperCase());
      cbEtats_[i].setActionCommand(CODES[i].replace('.','_').toUpperCase());
      cbEtats_[i].addActionListener(this);
      p1.add(cbEtats_[i]);
    }

    add(p1);
  }
  
  // Methodes publiques

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
  }

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  /*
  public boolean isPreferencesApplyable()
    { return true; }
  
  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }
  */

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }
  
  // Methodes privees
  
  protected void fillTable()
  {
    for(int i=0;i<NBL;i++)
      options_.putBooleanProperty(CODES[i],cbEtats_[i].isSelected());
    setDirty(false);
  }

  protected void updateComponents()
  {
    for(int i=0;i<NBL;i++)
      cbEtats_[i].setSelected
	(options_.getBooleanProperty(CODES[i],DEFAUTS[i]));
    setDirty(false);
  }
}

