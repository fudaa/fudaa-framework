/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuTransparentToggleButton.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.JToggleButton;
import javax.swing.plaf.ColorUIResource;

/**
 * A button without border.
 */
public class BuTransparentToggleButton
  extends JToggleButton
  implements BuBorders, BuInsets
{
  public BuTransparentToggleButton(BuIcon _icon,BuIcon _selected)
  {
    setActionCommand("BUTTON_PRESSED");
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setIcon((_icon != null)&&_icon.isDefault()?null:_icon);
    setMargin(INSETS0000);
    setBorder(EMPTY0000);
    setRequestFocusEnabled(false);
    setOpaque(false);
    setContentAreaFilled(false);
    setHorizontalAlignment(LEFT);
    putClientProperty("SLAF_NO_TEXTURE",Boolean.TRUE);
    BuIcon selected=_selected;
    if((selected != null)&&selected.isDefault())
      selected=null;
    if(selected!=null)
      BuLib.setSelectedIcons(this,selected,true);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  @Override
  public boolean isOpaque()
  {
    return false;
  }

  public void setIcon(BuIcon _icon)
  {
    BuLib.setIcon(this,_icon);
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(0,getHeight()+1);
    return r;
  }

  @Override
  public void updateUI()
  {
    super.updateUI();
    setRolloverEnabled(true);
  }

  @Override
  public Color getBackground()
  {
    Color r;
    if(isEnabled()&&getModel().isRollover()&&BuLib.isMetal())
      r=new ColorUIResource(192,192,208);
    else
      r=super.getBackground();
    return r;
  }
}
