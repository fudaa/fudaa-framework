/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuMonitorPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose which monitors will
 * appear in the status bar.
 *
 * monitor.memory
 * ...
 */
public class BuMonitorPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  private static final int NBF=4;

  private static final String[] INTITULES=new String[]
  {
    "M�moire","Processeur","Horloge","Temps",
  };

  private static final String[] NOMS=new String[]
  {
    "MEMORY","CPU","CLOCK","TIME"
  };

  BuCommonImplementation appli_;
  BuPreferences          options_;
  BuCheckBox[]           cbMoniteurs;
  
  @Override
  public String getTitle()
  {
    return getS("Moniteur");
  }

  @Override
  public String getCategory()
  {
    return getS("Composants");
  }

  // Constructeur

  public BuMonitorPreferencesPanel(BuCommonImplementation _appli)
  {
    super();
    appli_=_appli;
    init();
  }

  private void init()
  {
    options_=BuPreferences.BU;

    /*
    BuGridLayout lo=new BuGridLayout();
    lo.setColumns(2);
    lo.setVgap(5);
    lo.setHgap(5);
    */
    //GridLayout lo=new GridLayout(0,2,5,5);
    BuGridLayout lo=new BuGridLayout(2,5,5,true,true,false,false);
    lo.setSameWidth (true);
    lo.setSameHeight(true);

    BuPanel p=new BuPanel(lo);
    p.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getTitle()),
	EMPTY5555));

    cbMoniteurs=new BuCheckBox[NBF];
    for(int i=0;i<NBF;i++)
    {
      cbMoniteurs[i]=new BuCheckBox(getS(INTITULES[i]));
      cbMoniteurs[i].setName("cb"+NOMS[i]+"_VISIBLE");
      cbMoniteurs[i].setActionCommand(NOMS[i]);
      cbMoniteurs[i].addActionListener(this);
      p.add(cbMoniteurs[i]);
    }

    //p.add(new BuPanel());
    //setLayout(new BuBorderLayout());
    setLayout(new BuVerticalLayout(5,true,false));
    setBorder(EMPTY5555);
    add(p);//,BuBorderLayout.NORTH);//CENTER);

    updateComponents();
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
    /*
    JComponent source=(JComponent)_evt.getSource();
    System.err.println("SOURCE="+source.getName());
    System.err.println("ACTION="+_evt.getActionCommand());
    */
  }
  
  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  /*
  public boolean isPreferencesApplyable()
    { return true; }
  
  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }
  */

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees
  
  private void fillTable()
  {
    for(int i=0;i<NBF;i++)
      options_.putBooleanProperty
	  ("monitor."+NOMS[i].toLowerCase(),
	   cbMoniteurs[i].isSelected());
    setDirty(false);
  }

  private void updateComponents()
  {
    for(int i=0;i<NBF;i++)
      cbMoniteurs[i].setSelected
	(options_.getBooleanProperty
	 ("monitor."+NOMS[i].toLowerCase(),(i==0)));
    setDirty(false);
  }
}
