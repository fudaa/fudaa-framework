/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuButtonLayout.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import java.io.Serializable;
import javax.swing.SwingConstants;

/**
 * Lays out horizontally a set of components (buttons)
 * with the same size and aligned on right.
 */
public class BuButtonLayout
  implements LayoutManager2, Serializable, SwingConstants
{
  // Vector components=new Vector(0,1);

  public BuButtonLayout()
  {
    this(5,RIGHT);
  }

  public BuButtonLayout(int _hgap, int _halign)
  {
    hgap_  =_hgap;
    halign_=_halign;
  }

  public BuButtonLayout(int _hgap, int _halign, int _separator)
  {
    hgap_     =_hgap;
    halign_   =_halign;
    separator_=_separator;
  }

  protected int hgap_=0;
  public int getHgap() { return hgap_; }
  public void setHgap(int _hgap) { hgap_=_hgap; }

  protected int halign_=0;
  public int getHorizontalAlignment() { return halign_; }
  public void setHorizontalAlignment(int _halign) { halign_=_halign; }

  protected int separator_=-1;
  public int getSeparator() { return halign_; }
  public void setSeparator(int _separator) { separator_=_separator; }

  @Override
  public void addLayoutComponent(Component _comp, Object _cstr)
  {
    // components.addElement(_comp);
  }

  @Override
  public void addLayoutComponent(String _name, Component _comp)
  {
    // components.addElement(_comp);
  }

  @Override
  public void removeLayoutComponent(Component _comp)
  {
    // components.removeElement(_comp);
  }

  @Override
  public Dimension minimumLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      int nbc=_parent.getComponentCount();
      int   w=0;
      int   h=0;
      int   n=0;
      
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getMinimumSize();
	  w=Math.max(w,d.width );
	  h=Math.max(h,d.height);
	  n++;
	}
      }

      w=(w*n)+(n>0 ? (n-1)*hgap_ : 0);

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension preferredLayoutSize(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      int nbc=_parent.getComponentCount();
      int   w=0;
      int   h=0;
      int   n=0;
      
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getPreferredSize();
	  w=Math.max(w,d.width );
	  h=Math.max(h,d.height);
	  n++;
	}
      }

      w=(w*n)+(n>0 ? (n-1)*hgap_ : 0);

      Insets insets=_parent.getInsets();
      return new Dimension(w+insets.left+insets.right,
			   h+insets.top+insets.bottom);
    }
  }

  @Override
  public Dimension maximumLayoutSize(Container _parent)
  {
    return preferredLayoutSize(_parent);
  }

  @Override
  public float getLayoutAlignmentX(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public float getLayoutAlignmentY(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public void invalidateLayout(Container _parent)
  {
  }

  @Override
  public void layoutContainer(Container _parent)
  {
    synchronized (_parent.getTreeLock())
    {
      Insets    insets=_parent.getInsets();
      Rectangle bounds=new Rectangle
	(insets.left,
	 insets.top,
	 _parent.getSize().width  - (insets.left + insets.right),
	 _parent.getSize().height - (insets.top + insets.bottom));

      int nbc=_parent.getComponentCount();
      int   w=0;
      int   h=0;
      int   n=0;

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  Dimension d=c.getPreferredSize();
	  w=Math.max(w,d.width );
	  h=Math.max(h,d.height);
	  n++;
	}
      }

      if(n>0)
      {
	int     t=(w*n)+(n>0 ? (n-1)*hgap_ : 0);
	int     x=bounds.width-t;
	boolean p=true;

	if(x<0)
	{
	  x=0;
	  t=bounds.width;
	  w=(t-(n>0 ? (n-1)*hgap_ : 0))/n;
	  if(w<0) w=0;
	}

	int ws=0;

	switch(getHorizontalAlignment())
	{
	case LEFT  : if(separator_>=0) { ws=x; } x=0; break;
	case CENTER: if(separator_>=0) { ws=x; x/=3; ws-=2*x; } else x/=2; break;
	default    : if(separator_>=0) { ws=x; x=0; } break;
	}

	//System.err.println("BBL: ws="+ws+"px separator="+separator_+" x="+x);
	for(int i=0;i<nbc;i++)
        {
	  Component c=_parent.getComponent(i);
	  if(c.isVisible())
	  {
	    if(!p) x+=hgap_;
	    p=false;
	    if(i==separator_) x+=ws;

	    if(x>bounds.width ) x=bounds.width;

	    Rectangle actual=new Rectangle
	      (bounds.x+x,
	       bounds.y,
	       Math.min(bounds.width -x,w),
	       Math.min(bounds.height  ,h));
	    c.setBounds(actual.x,actual.y,actual.width,actual.height);
	    x+=w;
	  }
	}
      }
    }
  }
}
