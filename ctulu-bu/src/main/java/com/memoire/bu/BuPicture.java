/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuPicture.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.net.URL;
import java.util.TooManyListenersException;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * A component to display images.
 * Enabled scaling.
 */
public class BuPicture
  extends JPanel //JComponent
  implements MouseListener, MouseMotionListener, //MouseWheelListener,
             DtDragSensible
{
  public static final int SCALE =0;
  public static final int ZOOM  =1;
  public static final int REDUCE=2;
  public static final int LEFT  =3;
  public static final int MIDDLE=4;
  public static final int RIGHT =5;
  public static final int FIT_WIDTH   =6;

  private BuIcon cache_;
 // private int    dx_;
  int dy_;

  public BuPicture()
  {
    mode_ =MIDDLE;
    icon_ =null;
    setOpaque(false);
  }

  public BuPicture(BuIcon _icon)
  {
    this();
    setIcon(_icon);
  }

  public BuPicture(Image _image)
  {
    this();
    setImage(_image);
  }

  public BuPicture(URL _url)
  {
    this();
    setImage(getToolkit().getImage(_url));
  }

  // Drop

  @Override
  public void setDropTarget(DropTarget _dt)
  {
    super.setDropTarget(_dt);
    if(_dt!=null)
    {
      //FuLog.debug("BTA: setDropTarget");
      try { _dt.removeDropTargetListener(DtDragSensible.SINGLETON); }
      catch(IllegalArgumentException ex) { }
      try { _dt.addDropTargetListener(DtDragSensible.SINGLETON); }
      catch(TooManyListenersException ex) { }
    }
  }

  protected boolean drop_;

  protected Rectangle getDropRect()
  {
    Rectangle r=getVisibleRect();
    /*
    Insets    i=getInsets();
    r.x     +=3;
    r.y     +=3;
    r.width -=6;
    r.height-=6;
    */
    return r;
  }

  @Override
  public void dragEnter(boolean _acceptable, Point _location)
  {
    dragOver(_acceptable,_location);
  }

  @Override
  public void dragOver(boolean _acceptable, Point _location)
  {
    if(!drop_&&_acceptable) { drop_=true; repaint(); }
  }

  @Override
  public void dragExit()
  {
    if(drop_) { drop_=false; repaint(); }
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    super.paint(_g);

    if(drop_)
    {
      BuLib.setAntialiasing(this,_g);
      Rectangle r=getDropRect();
      //_g.setColor(new Color(255,128,0,64));
      //_g.fillRoundRect(r.x,r.y,r.width,r.height,9,9);
      _g.setColor(new Color(255,128,0));
      _g.drawRoundRect(r.x,r.y,r.width-1,r.height-1,9,9);
    }
  }

  @Override
  public void paintComponent(Graphics _g)
  {
    super.paintComponent(_g);

    /*
    if(isOpaque())
    {
      Rectangle clip=_g.getClipBounds();
      if(clip!=null)
      {
	_g.setColor(getBackground());
	_g.fillRect(clip.x,clip.y,clip.width,clip.height);
      }
    }
    */

    if(icon_!=null)
    {
      Dimension size  =getSize();
      Insets    insets=getInsets();

      int xp=insets.left;
      int yp=insets.top;
      int wp=size.width-insets.left-insets.right;
      int hp=size.height-insets.top-insets.bottom;

      switch(getMode())
      {
	case SCALE:
	{
	  if(isSmooth())
	    paintCache(_g,xp,yp,wp,hp);
	  else
	    _g.drawImage(icon_.getImage(),xp,yp,wp,hp,this);
	  break;
	}
        case ZOOM:
	{
	  int    wi=icon_.getIconWidth();
	  int    hi=icon_.getIconHeight();
	  double wr=(double)wp/(double)wi;
	  double hr=(double)hp/(double)hi;
	  double r =Math.min(wr,hr);
	  int    wf=(int)Math.round(r*wi);
	  int    hf=(int)Math.round(r*hi);

	  if(isSmooth())
	    paintCache(_g,xp,yp,wf,hf);
	  else
	    _g.drawImage(icon_.getImage(),xp,yp,wf,hf,this);
	  break;
	}
        case REDUCE:
	{
	  int    wi=icon_.getIconWidth();
	  int    hi=icon_.getIconHeight();
	  double wr=(double)wp/(double)wi;
	  double hr=(double)hp/(double)hi;
	  double r =Math.min(wr,hr);

	  if(r>=1.0) r=1.0;
	  int    wf=(int)Math.round(r*wi);
	  int    hf=(int)Math.round(r*hi);

	  if(isSmooth())
	    paintCache(_g,xp,yp,wf,hf);
	  else
	    _g.drawImage(icon_.getImage(),xp,yp,wf,hf,this);
	  break;
	}
        case LEFT:
	{
	  //int    wi=icon_.getIconWidth();
	  //int    hi=icon_.getIconHeight();
	  _g.drawImage(icon_.getImage(),xp,yp,this);
	  break;
	}
        case MIDDLE:
	{
	  int    wi=icon_.getIconWidth();
	  int    hi=icon_.getIconHeight();
	  _g.drawImage(icon_.getImage(),xp+(wp-wi)/2,yp+(hp-hi)/2,this);
	  break;
	}
        case RIGHT:
	{
	  int    wi=icon_.getIconWidth();
	  int    hi=icon_.getIconHeight();
	  _g.drawImage(icon_.getImage(),xp+wp-wi,yp+hp-hi,this);
	  break;
	}
        case FIT_WIDTH:
	{
	  int    wi=icon_.getIconWidth();
	  int    hi=icon_.getIconHeight();
	  double wr=(double)wp/(double)wi;
	  //double hr=(double)hp/(double)hi;
	  //double r =Math.min(wr,hr);

	  //if(r>=1.0) r=1.0;
	  int    wf=(int)Math.round(wr*wi);
	  int    hf=(int)Math.round(wr*hi);

          dy_=Math.min(0,Math.max(dy_,hp-hf));
          //System.err.println("YP="+yp+" DY="+dy_);
	  if(isSmooth())
	    paintCache(_g,xp,yp+dy_,wf,hf);
	  else
	    _g.drawImage(icon_.getImage(),xp,yp+dy_,wf,hf,this);
	  break;
	}
      }
    }
  }

  private void computeCache(int _w, int _h)
  {
    if(  (cache_==null)
       ||(cache_.getIconWidth() !=_w)
       ||(cache_.getIconHeight()!=_h))
    {

      if((_w>0)&&(_h>0))
	cache_=new BuIcon(icon_.getImage().getScaledInstance
			  (_w,_h,Image.SCALE_SMOOTH));
      else
	cache_=null;
    }
  }

  private void paintCache(Graphics _g,int _x,int _y,int _w,int _h)
  {
    computeCache(_w,_h);
    if(cache_!=null) cache_.paintIcon(this,_g,_x,_y);
  }

  /*
  public Dimension getPreferredSize()
  {
    return (cache_!=null)
      ? new Dimension(cache_.getIconWidth(),cache_.getIconHeight())
      : super.getPreferredSize();
  }
  */

  public void adjustPreferredSize(int _w)
  {
    
    if(icon_==null) return;

    if((mode_==ZOOM)||(mode_==REDUCE))
    {
      Insets insets=getInsets();
      int    dw=insets.left+insets.right;
      int    dh=insets.top+insets.bottom;
      int    wi=icon_.getIconWidth ();
      int    hi=icon_.getIconHeight();
      int w=_w;
      if((mode_==REDUCE)&&(w>wi+dw)) w=wi+dw;
      double wr=(double)Math.max(0,w-dw)/(double)wi;

      if((mode_==REDUCE)&&(wr>1.0)) return;
      int    hf=(int)Math.round(wr*hi);
      setPreferredSize(new Dimension(w,hf+dh));
    }
  }

  public void adjustPreferredSize(int _w, int _h)
  {
    if(icon_==null) return;
    int w=_w;
    if((mode_==ZOOM)||(mode_==REDUCE))
    {
      Insets insets=getInsets();
      int    dw=insets.left+insets.right;
      int    dh=insets.top+insets.bottom;
      int    wi=icon_.getIconWidth ();
      int    hi=icon_.getIconHeight();

      if((mode_==REDUCE)&&(w>wi+dw)) w=wi+dw;
      double wr=(double)Math.max(0,w-dw)/(double)wi;
      int h=_h;
      if((mode_==REDUCE)&&(h>hi+dh)) h=hi+dh;
      double hr=(double)Math.max(0,h-dh)/(double)hi;

      if((mode_==REDUCE)&&(wr>1.0)&&(hr>1.0)) return;
      int    wf=(int)Math.round(Math.min(wr,hr)*wi);
      int    hf=(int)Math.round(Math.min(wr,hr)*hi);
      setPreferredSize(new Dimension(wf+dw,hf+dh));
    }
  }

  public void computePreferredSize()
  {
    int w=0;
    int h=0;

    if(icon_!=null)
    {
      w=icon_.getIconWidth ();
      h=icon_.getIconHeight();
    }

    Insets insets=getInsets();
    w+=insets.left+insets.right ;
    h+=insets.top +insets.bottom;
    setPreferredSize(new Dimension(w,h));
  }

  @Override
  public void setBorder(Border _border)
  {
    super.setBorder(_border);
    computePreferredSize();
  }

  // Properties

  int mode_;
  public int getMode() { return mode_; }
  public void setMode(int _v)
  {
    if(mode_!=_v)
    {
      int vp=mode_;
      mode_ =_v%(FIT_WIDTH+1);
      cache_=null;
      computePreferredSize();

      if(vp==FIT_WIDTH)
      {
        removeMouseListener      (this);
        removeMouseMotionListener(this);
        if(MWL!=null)
          removeMouseWheelListener((MouseWheelListener)this);
      }
      firePropertyChange("mode",vp,_v);
      if(mode_==FIT_WIDTH)
      {
        addMouseListener      (this);
        addMouseMotionListener(this);
        if(MWL!=null)
          addMouseWheelListener((MouseWheelListener)MWL);
      }
      repaint();
    }
  }

  private Object MWL;

  {
//    if(FuLib.jdk()>=1.4)
//    {
      MWL=new MouseWheelListener()
        {
          @Override
          public void mouseWheelMoved(MouseWheelEvent _evt)
          {
            if(mode_==FIT_WIDTH)
              dy_-=20*_evt.getScrollAmount()*_evt.getWheelRotation();
            repaint();
          }
        };
//    }
  }


  private boolean smooth_=
      BuPreferences.BU.getBooleanProperty("antialias.all",false);
  public boolean isSmooth() { return smooth_; }
  public void setSmooth(boolean _smooth)
  {
    if(smooth_!=_smooth)
    {
      boolean vp=smooth_;
      smooth_=_smooth;
      cache_ =null;

      firePropertyChange("smooth",vp,_smooth);
      repaint();
    }
  }

  private BuIcon icon_;
  public BuIcon getIcon() { return icon_; }
  public void setIcon(BuIcon _v)
  {
    if(icon_!=_v)
    {
      BuIcon vp=icon_;
      icon_ =_v;
      cache_=null;
      /*dx_=*/dy_=0;
      computePreferredSize();
      // setMinimumSize(size);
      // setSize(size);

      firePropertyChange("icon",vp,_v);
      revalidate();
      repaint();
    }
  }

  public Image getImage()
  {
    return (icon_!=null ? icon_.getImage() : null);
  }

  public void setImage(Image _v)
  {
    setIcon(_v!=null ? new BuIcon(_v) : null);
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(0,getHeight()+1);
    return r;
  }

  @Override
  public void mouseEntered(MouseEvent _evt)
  {
  }

  @Override
  public void mouseExited(MouseEvent _evt)
  {
  }

  @Override
  public void mouseReleased(MouseEvent _evt)
  {
  }

  private int /*mx_,*/my_;

  @Override
  public void mousePressed(MouseEvent _evt)
  {
    if(BuLib.isLeft(_evt))
    {
      //mx_=_evt.getX()+dy_;
      my_=_evt.getY()+dy_;
    }
  }

  @Override
  public void mouseClicked(MouseEvent _evt)
  {
  }

  @Override
  public void mouseMoved(MouseEvent _evt)
  {
  }

  @Override
  public void mouseDragged(MouseEvent _evt)
  {
    if(BuLib.isLeft(_evt))
    {
      //dx_=mx_-_evt.getX();
      dy_=my_-_evt.getY();
      repaint();
    }
  }

  /*
  public void mouseWheelMoved(MouseWheelEvent _evt)
  {
    //dx_=5*_evt.getScrollAmount()*_evt.getWheelRotation()/3;
    if(mode_==FIT_WIDTH)
      dy_-=20*_evt.getScrollAmount()*_evt.getWheelRotation();
    repaint();
  }
  */

  /*public static void main(String[] _args)
  {
    if(_args.length==0)
      _args=new String[]
        { ""+BuResource.class.getResource("background_desktop_2.gif") };

    for(int i=0;i<_args.length;i++)
    {
	BuIcon icon=new BuIcon(_args[i]);
	if(icon.isDefault())
	{
	  new BuDialogError(null,null,"Can not open:\n"+_args[i])
	    .activate();
	  continue;
	}

	final BuPicture p=new BuPicture(icon);
	//p.setMode(REDUCE);
        p.setMode(FIT_WIDTH);
	//p.setBackground(Color.red);
	//p.setBorder(new EmptyBorder(50,0,0,20));
	p.setToolTipText(_args[i]);

	
	DndManageData dm=new DndManageData()
	{
	  public boolean manage(Object _data, JComponent _comp,
				Point _location)
	  {
	    boolean r=false;

	    Object[] o=null;

	    if(_data.getClass().isArray())
	      o=(Object[])_data;
	    else
	      o=new Object[] { _data };

	    if(o.length>0)
	    {
	      Object v=o[0];
	      if(v instanceof File)
	      {
		try { v=((File)v).toURL(); }
		catch(MalformedURLException ex) { v=null; };
	      }
	      if(v instanceof URL)
	      {
		p.setIcon(new BuIcon((URL)v));
		r=true;
	      }
	    }

	    return r;
	  }
        };

	new DndTarget()
	    .accept(File.class,DndTransferObject.SINGLETON,dm)
	    .accept(URL.class ,DndTransferObject.SINGLETON,dm)
	    .add(p);
	

	int j;
	String t=_args[i];
	j=t.lastIndexOf('/');
	t=t.substring(j+1);
	j=t.lastIndexOf('\\');
	t=t.substring(j+1);
	t+=" ("+icon.getIconWidth()+"x"+icon.getIconHeight()+")";

	final JFrame frame=new JFrame(t);
	frame.setIconImage
	  (BuResource.BU.getFrameIcon("image").getImage());
	BuRegistry.register(frame);
	frame.getContentPane().add(p);
	frame.addWindowListener(new WindowAdapter()
	{
	  public void windowClosing(WindowEvent _evt)
	  {
	    BuRegistry.unregister(frame);
	  }
	});

        frame.pack();
	Dimension s=frame.getSize();
	if(s.width < 64) s.width =64;
	if(s.height< 64) s.height=64;
	if(s.width >450) s.width =450;
	if(s.height>450) s.height=450;
	frame.setSize(s);
	frame.setVisible(true);
    }
  }*/
}
