/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuMenu.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import com.memoire.fu.FuLib;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

/**
 * A menu with simplified adding methods.
 * A few static methods are available for standard menus as
 * File, Edition, Windows and Help.
 */
public class BuMenu
  extends JMenu
  implements BuMenuInterface //ActionListener
{
  private boolean left_;

  public BuMenu()
  { this("Menu","MENU",true,0); }

  public BuMenu(String _label, String _cmd)
  { this(_label,_cmd,null,false,0); }

  public BuMenu(String _label, String _cmd, Icon _icon)
  { this(_label,_cmd,_icon,false,0); }

  public BuMenu(String _label, String _cmd, boolean _tearoff)
  { this(_label,_cmd,null,_tearoff,0); }

  public BuMenu(String _label, String _cmd, boolean _tearoff, int _key)
  { this(_label,_cmd,null,_tearoff,0); }

  public BuMenu(String _label, String _cmd, Icon _icon,
                boolean _tearoff, int _key)
  {
    super(null,_tearoff);

    setName("mn"+_cmd);
    setActionCommand(_cmd);
    setHorizontalTextPosition(SwingConstants.RIGHT);
    Icon icon=_icon;
    if(icon==null) icon=BuResource.BU.loadMenuCommandIcon(_cmd);
    if((icon instanceof BuIcon)&&((BuIcon)icon).isDefault()) icon=null;

    if(icon instanceof BuIcon) setIcon((BuIcon)icon);
    else                        setIcon(icon);

    setText((_label==null) ?"":_label);

    // TMP
    // getPopupMenu().setLightWeightPopupEnabled(false);
  }

  public boolean isLeft()
  {
    return left_;
  }

  public void setLeft(boolean _left)
  {
    left_=_left;
  }

  /*GCJ-BEGIN*/
  private boolean isLeft(Component _m, Point _p)
  {
    if(_p.x<0  ) return true;
    if(_m==null) return false;

    Component p=_m;

    while(!(p instanceof BuMenu))
    {
      p=(p instanceof JPopupMenu)
        ? ((JPopupMenu)p).getInvoker()
        : p.getParent();
      if(p==null) return false;
    }

    Point q=((BuMenu)p).getPopupMenuOrigin();
    p=p.getParent();
    return isLeft(p,q);
  }

  @Override
  protected Point getPopupMenuOrigin()
  {
    Point r=super.getPopupMenuOrigin();
    //System.err.println("BMN: getPopupMenuOrigin "+r)

    JPopupMenu pm=getPopupMenu();
    pm.putClientProperty("SLAF_LEFT_POPUPMENU",Boolean.FALSE);

    if(isLeft()||isLeft(getParent(),r))
    {
      Container pt=getParent();
      if(pt instanceof JPopupMenu)
      {
       // Dimension dp=pt.getSize();
        r=new Point(pt.getX()-pm.getPreferredSize().width
                    -UIManager.getInt("Menu.submenuPopupOffsetX"),r.y);
        pm.putClientProperty("SLAF_LEFT_POPUPMENU",Boolean.TRUE);
      }
    }

    return r;
  }
  /*GCJ-END*/

  /*
  public void setMenuLocation(int _x,int _y)
  {
    System.err.println("BMN: setMenuLocation "+_x+","+_y);
    super.setMenuLocation(_x,_y);
  }
  */

  // Essai
  /*
  protected WinListener createWinListener(JPopupMenu p)
  {
    System.err.println("%%% BuPM: create win listener");

    p=new BuPopupMenu(getText());
    p.setInvoker(this);
    p.addPopupMenuListener(new PopupMenuListener()
    {
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) { }
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) { }
      public void popupMenuCanceled(PopupMenuEvent e) { fireMenuCanceled(); }
    });

    try {
    java.lang.reflect.Field f=JMenu.class.getDeclaredField("popupMenu");
    f.setAccessible(true);
    f.set(this,p);
    } catch(Throwable th) { System.err.println(th); }

    return super.createWinListener(p);
  }
  */

  // Icon

  @Override
  public Icon getIcon()
  {
    if(BuPreferences.BU.getBooleanProperty("icons.menu",true)||
       (super.getText()==null))
      return super.getIcon();
    return null;
  }

  public void setIcon(BuIcon _icon)
  {
    BuLib.setIcon(this,_icon);
  }

  // Translation

  protected String getString(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  protected static final String __(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  // Mnemonics

  @Override
  public void computeMnemonics()
  {
    Component[] c=getPopupMenu().getComponents();
    Hashtable   t=new Hashtable();

    //MenuElement[] c=getSubElements();
    //try { c=getMenuComponents(); }
    //catch(Exception ex) { } // swing 1.03

    {
      int mn=getMnemonic();
      if(mn>0) t.put(FuFactoryInteger.get(mn),this);
    }

    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenuItem)
      {
	JMenuItem mi=(JMenuItem)c[i];
	int mn=mi.getMnemonic();
	if(mn<=0)
	{
	  String tx=BuLib.candidateMnemonics(mi.getText());
	  if(tx!=null)
	    for(int j=0;j<tx.length();j++)
	    {
	      mn=tx.charAt(j);
	      if(t.get(FuFactoryInteger.get(mn))==null)
	      {
		t.put(FuFactoryInteger.get(mn),mi);
		mi.setMnemonic(mn);
		break;
	      }
	    }
	}
	if(mi instanceof BuMenu)
	  ((BuMenu)mi).computeMnemonics();
      }
    }
  }

  // Separator

  @Override
  public BuSeparator addSeparator(String _text)
  {
    BuSeparator r=new BuSeparator(_text);
    this.add(r);
    return r;
  }

  public void removeDummySeparators()
  {
    removeDummySeparators(getPopupMenu());
  }

  private static void removeDummySeparators(JPopupMenu _menu)
  {
    if(_menu==null) return;

    //synchronized(_menu)
    {
      Component[] c=_menu.getComponents();
      //MenuElement[] c=_menu.getSubElements();
      if(c==null) return;

      int l=c.length;
      if(l==0) return;

      boolean   was  =true;
      Component first=null;
      for(int i=l-1;i>=0;i--)
      {
	if(c[i] instanceof JSeparator)
        {
	  if(was)
	  {
	    _menu.remove(c[i]);
	  }
	  else
	  {
	    first=c[i];
	    was=true;
	  }
	}
	else
	{
	  was=false;
	  first=c[i];
	  if(first instanceof JMenu)
	    removeDummySeparators(((JMenu)first).getPopupMenu());
	}
      }

      if(first instanceof JSeparator)
	_menu.remove(first);
    }
  }

  // SubMenu

  @Override
  public void addSubMenu(JMenu _m, boolean _enabled)
  {
    _m.setEnabled(_enabled);
    _m.addActionListener(this);
    this.add(_m);
  }

  // Menu Item

  public BuMenuItem addMenuItem(String _s, String _cmd)
  {
    return addMenuItem(_s,_cmd,true);
  }
  
  /**
   * Retourne l'index de l'item de commande donn�.
   * @param _cmd Le nom de la commande.
   * @return L'index, ou -1 si non trouv�.
   */
  public int indexOf(String _cmd) {
    return indexOf(this,_cmd);
  }

  private int indexOf(BuMenu _menu, String _cmd) {
    if (_cmd==null) return -1;

    for (int i=0; i<_menu.getItemCount(); i++) {
      
//      if (!(_menu.getItem(i) instanceof JMenuItem)) continue;
      if (_menu.getItem(i)!=null && _cmd.equals(_menu.getItem(i).getActionCommand())) return i;
    }
    return -1;
  }

  public BuMenuItem addMenuItem(String _s, String _cmd,
				ActionListener _al)
  {
    return addMenuItem(_s,_cmd,null,true,0,_al);
  }
  public BuMenuItem addMenuItem(String _s, String _cmd,boolean _enable,
      ActionListener _al)
  {
    return addMenuItem(_s,_cmd,null,_enable,0,_al);
  }

  @Override
  public BuMenuItem addMenuItem(String _s, String _cmd,
                                boolean _enabled)
  {
    return addMenuItem(_s,_cmd,null,_enabled,0,this);
  }

  public BuMenuItem addMenuItem(String _s, String _cmd,
				boolean _enabled, int _key)
  {
    return addMenuItem(_s,_cmd,null,_enabled,_key,this);
  }

  @Override
  public BuMenuItem addMenuItem(String _s, String _cmd, Icon _icon,
                                boolean _enabled)
  {
    return addMenuItem(_s,_cmd,_icon,_enabled,0,this);
  }
  public BuMenuItem addMenuItem(String _s, String _cmd, Icon _icon,
      ActionListener _al)
  {
    return addMenuItem(_s,_cmd,_icon,true,0,_al);
  }

  @Override
  public BuMenuItem addMenuItem(String _s, String _cmd, Icon _icon,
                                boolean _enabled, int _key)
  {
    return addMenuItem(_s,_cmd,_icon,_enabled,_key,this);
  }

  public BuMenuItem addMenuItem(String _s, String _cmd, Icon _icon,
				boolean _enabled, int _key, ActionListener _al)
  {
    Icon icon=_icon;
    if(icon==null) icon=BuResource.BU.loadMenuCommandIcon(_cmd);
    if((icon instanceof BuIcon)&&((BuIcon)icon).isDefault()) icon=null;
    if(icon==null) icon=BuResource.BU.getMenuIcon("aucun");

    BuMenuItem r=new BuMenuItem();
    r.setText(_s);
    r.setName("mi"+_cmd);
    r.setActionCommand(_cmd);
    if(icon instanceof BuIcon) r.setIcon((BuIcon)icon);
    else                        r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(_al);
    r.setEnabled(_enabled);
    if(_key!=0)
    {
      if((_key>=KeyEvent.VK_F1)&&(_key<=KeyEvent.VK_F12))
	r.setAccelerator(KeyStroke.getKeyStroke(_key,InputEvent.ALT_MASK));
      else
	r.setAccelerator(KeyStroke.getKeyStroke(_key,InputEvent.CTRL_MASK));
    }
    this.add(r);
    return r;
  }

	public BuMenuItem addMenuItem(Action a)
	{
		BuMenuItem r=new BuMenuItem(a);
		this.add(r);
		return r;
	}

  // CheckBoxMenuItem

  @Override
  public BuCheckBoxMenuItem addCheckBox
    (String _s, String _cmd, boolean _enabled, boolean _checked)
  {
    return addCheckBox(_s,_cmd,null,_enabled,_checked);
  }

  @Override
  public BuCheckBoxMenuItem addCheckBox
    (String _s, String _cmd, Icon _icon, boolean _enabled, boolean _checked)
  {
    Icon icon=_icon;
    if(icon==null) icon=BuResource.BU.loadMenuCommandIcon(_cmd);
    if((icon instanceof BuIcon)&&((BuIcon)icon).isDefault()) icon=null;
    if(icon==null) icon=BuResource.BU.getMenuIcon("aucun");

    BuCheckBoxMenuItem r=new BuCheckBoxMenuItem();
    r.setText(_s);
    r.setName("cbmi"+_cmd);
    r.setActionCommand(_cmd);
    if(icon instanceof BuIcon) r.setIcon((BuIcon)icon);
    else                        r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setSelected(_checked);
    this.add(r);
    return r;
  }

  // RadioButtonMenuItem

  @Override
  public BuRadioButtonMenuItem addRadioButton
    (String _s, String _cmd, boolean _enabled, boolean _checked)
  {
    return addRadioButton(_s,_cmd,null,_enabled,_checked);
  }

  @Override
  public BuRadioButtonMenuItem addRadioButton
    (String _s, String _cmd, Icon _icon, boolean _enabled, boolean _checked)
  {
    Icon icon=_icon;
    if(icon==null) icon=BuResource.BU.loadMenuCommandIcon(_cmd);
    if((icon instanceof BuIcon)&&((BuIcon)icon).isDefault()) icon=null;
    if(icon==null) icon=BuResource.BU.getMenuIcon("aucun");

    BuRadioButtonMenuItem r=new BuRadioButtonMenuItem();
    r.setText(_s);
    r.setName("rbmi"+_cmd);
    r.setActionCommand(_cmd);
    if(icon instanceof BuIcon) r.setIcon((BuIcon)icon);
    else                        r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setSelected(_checked);
    this.add(r);
    return r;
  }

  /*
  public void removeMenuItem(String _cmd)
  {
    MenuElement[] cm=getSubElements();
    for(int i=0; i<cm.length; i++)
      if(cm[i] instanceof JPopupMenu)
      {
	MenuElement[] cp=((JPopupMenu)cm[i]).getSubElements();
	for(int j=0; j<cp.length; j++)
	  if(cp[j] instanceof JMenuItem)
	  {
	    JMenuItem m=(JMenuItem)cp[j];
	    if(m.getActionCommand().equals(_cmd))
	      remove(m);
	  }
      }
  }
  */

  // Menus standards

  public static BuMenu buildImportMenu()
  {
    BuMenu r=new BuMenu(__("Importer"),"IMPORTER");
    return r;
  }

  public static BuMenu buildExportMenu()
  {
    BuMenu r=new BuMenu(__("Exporter"),"EXPORTER");
    return r;
  }

  public static BuMenu buildFileMenu()
  {
    BuMenu r=new BuMenu(__("Fichier"),"MENU_FICHIER");
    r.addMenuItem(__("Cr�er")              ,"CREER"            ,false,KeyEvent.VK_N);
    r.addMenuItem(__("Ouvrir...")          ,"OUVRIR"           ,false,KeyEvent.VK_O);

    r.addSubMenu(new BuMenuRecentFiles(),true);
    r.addMenuItem(__("Propri�t�s...")      ,"PROPRIETE"        ,false);
    //r.addSeparator();

    r.addMenuItem(__("Enregistrer")        ,"ENREGISTRER"      ,false,KeyEvent.VK_S);
    r.addMenuItem(__("Enregistrer sous..."),"ENREGISTRERSOUS"  ,false)
      .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_MASK|InputEvent.SHIFT_MASK));
    r.addMenuItem(__("Fermer")             ,"FERMER"           ,false,KeyEvent.VK_W);
    r.addSeparator();

    r.addSubMenu(buildImportMenu(),false);
    r.addSubMenu(buildExportMenu(),false);
    r.addSeparator();

    r.addMenuItem(__("Imprimer...")        ,"IMPRIMER"         ,false,KeyEvent.VK_P);
    r.addMenuItem(__("Pr�visualiser")      ,"PREVISUALISER"    ,false);
    r.addMenuItem(__("Mise en page...")    ,"MISEENPAGE"       ,false);
    r.addSeparator();

    r.addMenuItem(__("Quitter...")         ,"QUITTER"          ,false,KeyEvent.VK_Q);

    return r;
  }

  public static BuMenu buildEditionMenu()
  {
    BuMenu r=new BuMenu(__("Edition"),"MENU_EDITION");
    r.addMenuItem(__("D�faire")            ,"DEFAIRE"          ,false,KeyEvent.VK_Z);
    r.addMenuItem(__("Refaire")            ,"REFAIRE"          ,false)
      .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,InputEvent.CTRL_MASK|InputEvent.SHIFT_MASK));
    // normalement Ctrl-Y
    r.addSeparator();

    r.addMenuItem(__("Copier")             ,"COPIER"           ,false,KeyEvent.VK_C);
    r.addMenuItem(__("Couper")             ,"COUPER"           ,false,KeyEvent.VK_X);
    r.addMenuItem(__("Coller")             ,"COLLER"           ,false,KeyEvent.VK_V);
    r.addMenuItem(__("Dupliquer")          ,"DUPLIQUER"        ,false)
      .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,InputEvent.CTRL_MASK|InputEvent.SHIFT_MASK));
    r.addSeparator();
    r.addMenuItem(__("Tout s�lectionner")  ,"TOUTSELECTIONNER" ,false,KeyEvent.VK_A);
    r.addMenuItem(__("Rechercher...")      ,"RECHERCHER"       ,false,KeyEvent.VK_F);
    r.addMenuItem(__("Remplacer...")       ,"REMPLACER"        ,false,KeyEvent.VK_R);
    r.addSeparator();
    r.addMenuItem(__("Pr�f�rences")        ,"PREFERENCE"       ,false,KeyEvent.VK_F2);
    return r;
  }

  public static BuMenu buildMetalThemeMenu()
  {
    BuMenu r=new BuMenu(__("Th�mes pour Metal"),"THEME_METAL");

    BuMetalCustomTheme[] themes=BuMetalCustomTheme.getList();
    for(int i=0;i<themes.length;i++)
    {
      if(themes[i]==null)
	r.addSeparator();
      else
      {
	JMenuItem mi;
	mi=r.addMenuItem(__(themes[i].getName()),"THEME_METAL("+i+")",null,true);
	mi.setIcon(themes[i].getIcon());
      }
    }

    return r;
  }

  public static BuMenu buildSlafThemeMenu()
  {
    BuMenu r=new BuMenu(__("Th�mes pour Slaf"),"THEME_SLAF");

    BuSlafCustomTheme[] themes=BuSlafCustomTheme.getList();
    for(int i=0;i<themes.length;i++)
    {
      if(themes[i]==null)
	r.addSeparator();
      else
      {
	JMenuItem mi;
	mi=r.addMenuItem(__(themes[i].getName()),"THEME_SLAF("+i+")",null,true);
	mi.setIcon(themes[i].getIcon());
      }
    }

    return r;
  }

  public static BuMenu buildLookAndFeelMenu()
  {
    BuMenu r=new BuMenu(__("Aspects"),"ASPECT");

    r.addSubMenu(buildMetalThemeMenu(),FuLib.classExists("javax.swing.plaf.metal.MetalLookAndFeel"));

    //if(!BuLib.isSwing10())
    r.addSubMenu(buildSlafThemeMenu(),FuLib.classExists("com.memoire.slaf.SlafLookAndFeel"));

    r.addSeparator();

    //if(!BuLib.isSwing10())
    r.addMenuItem("Amiga"             ,"ASPECT_AMIGA"       ,FuLib.classExists("swing.addon.plaf.threeD.ThreeDLookAndFeel"));

    //if(FuLib.jdk()>1.3)
    //{
      r.addMenuItem("FHLaf"             ,"ASPECT_FHLAF"       ,FuLib.classExists("com.shfarr.ui.plaf.fh.FhLookAndFeel"));
      r.addMenuItem("Gtk"               ,"ASPECT_GTK"         ,FuLib.classExists("com.sun.java.swing.plaf.gtk.GTKLookAndFeel"));
      r.addMenuItem("Kunststoff"        ,"ASPECT_KUNSTSTOFF"  ,FuLib.classExists("com.incors.plaf.kunststoff.KunststoffLookAndFeel"));
      r.addMenuItem("Liquid"            ,"ASPECT_LIQUID"      ,FuLib.classExists("com.birosoft.liquid.LiquidLookAndFeel"));
    //}

 // r.addMenuItem("Basic"             ,"ASPECT_BASIC"       ,FuLib.classExists("BuBasicLookAndFeel"));
    r.addMenuItem("Mac"               ,"ASPECT_MAC"         ,FuLib.classExists("com.sun.java.swing.plaf.mac.MacLookAndFeel"));
    r.addMenuItem("Metal"             ,"ASPECT_METAL"       ,FuLib.classExists("javax.swing.plaf.metal.MetalLookAndFeel"));

   // if(FuLib.jdk()>1.3)
    r.addMenuItem("Metouia"           ,"ASPECT_METOUIA"     ,FuLib.classExists("net.sourceforge.mlf.metouia.MetouiaLookAndFeel"));

    r.addMenuItem("Motif"             ,"ASPECT_MOTIF"       ,FuLib.classExists("com.sun.java.swing.plaf.motif.MotifLookAndFeel"));
 // r.addMenuItem("Multi"             ,"ASPECT_MULTI"       ,FuLib.classExists("javax.swing.plaf.multi.MultiLookAndFeel"));
    r.addMenuItem("Next"              ,"ASPECT_NEXT"        ,FuLib.classExists("nextlf.plaf.NextLookAndFeel"));
    r.addMenuItem("Organic"           ,"ASPECT_ORGANIC"     ,FuLib.classExists("javax.swing.plaf.organic.OrganicLookAndFeel"));

   // if(FuLib.jdk()>1.3)
   // {
    r.addMenuItem("Oyoaha"            ,"ASPECT_OYOAHA"      ,FuLib.classExists("com.oyoaha.swing.plaf.oyoaha.OyoahaLookAndFeel"));
    r.addMenuItem("Plastic"           ,"ASPECT_PLASTIC"     ,FuLib.classExists("com.jgoodies.looks.plastic.PlasticLookAndFeel"));
    r.addMenuItem("Plastic 3D"        ,"ASPECT_PLASTIC3D"   ,FuLib.classExists("com.jgoodies.looks.plastic.Plastic3DLookAndFeel"));
    r.addMenuItem("Plastic XP"        ,"ASPECT_PLASTICXP"   ,FuLib.classExists("com.jgoodies.looks.plastic.PlasticXPLookAndFeel"));
    r.addMenuItem("SkinLF"            ,"ASPECT_SKINLF"      ,FuLib.classExists("com.l2fprod.gui.plaf.skin.SkinLookAndFeel"));
  //  }

  //  if(!BuLib.isSwing10())
    r.addMenuItem("Slaf"              ,"ASPECT_SLAF"        ,FuLib.classExists("com.memoire.slaf.SlafLookAndFeel"));

    if(FuLib.jdk()>1.4)
    r.addMenuItem("Synthetica"        ,"ASPECT_SYNTHETICA"  ,FuLib.classExists("de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel"));

    r.addMenuItem("Tonic"             ,"ASPECT_TONIC"       ,FuLib.classExists("com.digitprop.tonic.TonicLookAndFeel"));
    r.addMenuItem("Windows"           ,"ASPECT_WINDOWS"     ,FuLib.classExists("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"));

    return r;
  }

  public static BuMenu buildWindowMenu()
  {
    BuMenu r=new BuMenu(__("Fen�tres"),"MENU_FENETRES");

/*    if(!FuLib.isJDistroRunning())
      r.addSubMenu(buildLookAndFeelMenu(),true);*/

    r.addSubMenu(new BuMenuInternalFrames(),true);

    BuMenu sm;

    if(BuPreferences.BU.getBooleanProperty("menu.short",false))
    {
      sm=new BuMenu(__("Organisation"),"CASCADE_ORGANISATION");
      r.addSubMenu(sm,true);
    }
    else
    {
      sm=r;
      r.addSeparator(__("Organisation"));
    }
    sm.addMenuItem (__("Plein �cran")          ,"PLEINECRAN"          ,true);
    sm.addMenuItem (__("Cascade")              ,"CASCADE"             ,true);
    sm.addMenuItem (__("Mosaique")             ,"MOSAIQUE"            ,true);
    sm.addMenuItem (__("Ranger les ic�nes")    ,"RANGERICONES"        ,true);
    sm.addMenuItem (__("Ranger les palettes")  ,"RANGERPALETTES"      ,true);
    sm.addMenuItem (__("Echanger les colonnes"),"ECHANGER_COLONNES"   ,true);

    if(BuPreferences.BU.getBooleanProperty("menu.short",false))
    {
      sm=new BuMenu(__("El�ments"),"VISIBLE_ELEMENTS");
      r.addSubMenu(sm,true);
    }
    else
    {
      sm=r;
      r.addSeparator(__("El�ments"));
    }
    sm.addCheckBox
      (__("Outils sp�cifiques"),"VISIBLE_SPECIFICBAR",true ,true );
    sm.addCheckBox
      (__("Colonne gauche")    ,"VISIBLE_LEFTCOLUMN" ,true ,true )
      .setAccelerator
      (KeyStroke.getKeyStroke(KeyEvent.VK_F11,InputEvent.ALT_MASK));
    sm.addCheckBox
      (__("Colonne droite")    ,"VISIBLE_RIGHTCOLUMN",true ,true )
      .setAccelerator
      (KeyStroke.getKeyStroke(KeyEvent.VK_F12,InputEvent.ALT_MASK));
    sm.addCheckBox
      (__("Barre d'�tat")      ,"VISIBLE_STATUSBAR"  ,true ,true );

    return r;
  }

  public static BuMenu buildHelpMenu()
  {
    BuMenu r=new BuMenu(__("Aide"),"MENU_AIDE");

    r.addCheckBox
      (__("Assistant")         ,"ASSISTANT"          ,true ,true );

    BuMenu sm;

    if(BuPreferences.BU.getBooleanProperty("menu.short",false))
    {
      sm=new BuMenu(__("Documentation"),"AIDE_DOCUMENTATION");
      r.addSubMenu(sm,true);
    }
    else
    {
      sm=r;
      r.addSeparator(__("Documentation")     );
    }
    sm.addMenuItem (__("Page principale")    ,"AIDE_INDEX"     ,true ,KeyEvent.VK_F1);
    sm.addMenuItem (__("Pointeur")           ,"POINTEURAIDE"   ,true )
      .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1,InputEvent.SHIFT_MASK));
    sm.addMenuItem (__("Index alphab�tique") ,"INDEX_ALPHA"    ,true );
    sm.addMenuItem (__("Index th�matique")   ,"INDEX_THEMA"    ,true );

    if(BuPreferences.BU.getBooleanProperty("menu.short",false))
    {
      sm=new BuMenu(__("Informations"),"TEXTE_INFORMATIONS");
      r.addSubMenu(sm,true);
    }
    else
    {
      sm=r;
      r.addSeparator(__("Informations")     );
    }
    sm.addMenuItem (__("Astuces...")         ,"ASTUCE"         ,true );
    sm.addMenuItem (__("A propos de...")     ,"APROPOSDE"      ,true );
    sm.addMenuItem (__("Licence...")         ,"TEXTE_LICENCE"  ,true );

    if(BuPreferences.BU.getBooleanProperty("menu.short",false))
    {
      sm=new BuMenu(__("Internet"),"WWW_INTERNET");
      r.addSubMenu(sm,true);
    }
    else
    {
      sm=r;
      r.addSeparator(__("Internet")     );
    }
    sm.addMenuItem (__("Site WWW")           ,"WWW_ACCUEIL"    ,true );
    // sm.addMenuItem (__("Site FTP"),"FTP_ACCUEIL",true );
    sm.addMenuItem (__("Mise � jour")        ,"MAJ"            ,false);
    // sm.addMenuItem (__("Bogue...")           ,"WWW_BOGUE"      ,false);
    // sm.addMenuItem (__("Achat...")           ,"WWW_ACHAT"      ,false);

    return r;
  }

  // Action

  private Vector actionListeners_=new Vector();

  @Override
  public void addActionListener(ActionListener _l)
  { actionListeners_.addElement(_l); }

  @Override
  public void removeActionListener(ActionListener _l)
  { actionListeners_.removeElement(_l); }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    // JComponent source=(JComponent)_evt.getSource();
    // System.err.println(""+getClass()+": "+source);

    for(Enumeration e=actionListeners_.elements(); e.hasMoreElements(); )
      ((ActionListener)e.nextElement()).actionPerformed(_evt);
  }
}
