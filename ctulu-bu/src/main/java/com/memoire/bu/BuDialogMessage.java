/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuDialogMessage.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A standard message dialog (continue).
 */

public class BuDialogMessage
       extends BuDialog
       implements ActionListener
{
  protected BuButton btContinuer_;

  public BuDialogMessage(BuCommonInterface      _parent,
			 BuInformationsSoftware _isoft,
			 Object                 _message)
  {
    super(_parent,_isoft,__("Message"),_message);

    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btContinuer_=new BuButton(BuResource.BU.loadButtonCommandIcon("CONTINUER"),
			     getString("Continuer"));
    btContinuer_.addActionListener(this);
    pnb.add(btContinuer_);
    getRootPane().setDefaultButton(btContinuer_);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  @Override
  public JComponent getComponent()
  { return null; }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialogue : "+source);

    if(source==btContinuer_)
    {
      reponse_=JOptionPane.OK_OPTION;
      setVisible(false);
    }
  }

  public static void main(String[] _args)
  {
    new BuDialogMessage(null,null,"Hello world!").activate();
    System.exit(0);
  }
}
