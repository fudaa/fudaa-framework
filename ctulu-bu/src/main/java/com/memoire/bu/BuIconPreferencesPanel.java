/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuIconPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose the icon set and the icon sizes.
 *
 * icons.family
 * icons.size (default)
 * icons.buttonsize
 * icons.menusize
 * icons.framesize
 * icons.toolsize
 * icons.tabsize
 * icons.grey (use grey for enabled icons)
 */
public class BuIconPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  protected final String[] FAMILIES=new String[]
    { "basic","beos1","beos2","eclipse3","fitzsimon",
      "gnome1","gnome2","jdistro","kde2","lila",
      "nuvola","oasis" };

  protected final String[] SIZES=new String[]
    { "","8","9","10","11","12","13","14","16","18","20","22","24",
      "26","28","32","36","40","44","48","52","56","64" };

  //private   BuCommonInterface appli_;
  private   BuPreferences     options_;
  private   Hashtable         optionsStr_;
  private   BuOptionRenderer  cbRenderer_;

  //private   BuGridLayout      loGen_;
  protected BuPanel           pnGenDesk_;
  protected BuFormLayout      loGenDesk_;

  protected BuLabel           lbIconSet_,
                              lbDefaultSize_,lbButtonSize_,
                              lbFrameSize_,lbMenuSize_,
                              lbTabSize_,lbToolSize_;
  protected BuComboBox        chIconSet_,
                              chDefaultSize_,chButtonSize_,
                              chFrameSize_,chMenuSize_,
                              chTabSize_,chToolSize_;

  //private   BuLabel           lbGrey_;
  private   BuCheckBox        cbGrey_,cbMenuIcon_;

  @Override
  public String getTitle()
  {
    return getS("Ic�nes");
  }

  @Override
  public String getCategory()
  {
    return getS("Visuel");
  }

  public BuIconPreferencesPanel()
  {
    super();
    //appli_     =_appli;
    options_   =BuPreferences.BU;
    cbRenderer_=new BuOptionRenderer();
    optionsStr_=new Hashtable();

    BuOptionItem itemOpt;
    String       itemStr;

    for(int i=0;i<FAMILIES.length;i++)
    {
      optionsStr_.put
        (itemStr=FAMILIES[i],
         itemOpt=new BuOptionItem
         (FAMILIES[i],null,
          BuResource.BU.isIconFamilyAvailable(FAMILIES[i],"gif")||
          (/*(FuLib.jdk()>=1.4)&&*/
           BuResource.BU.isIconFamilyAvailable(FAMILIES[i],"png"))));
      optionsStr_.put(itemOpt, itemStr);
    }

    /*
    optionsStr_.put
      (itemStr="16",
       itemOpt=new BuOptionItem("16x16 (standard)",
				BuResource.BU.getMenuIcon("ouvrir_16")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="20",
       itemOpt=new BuOptionItem("20x20 (dean)",
				BuResource.BU.getMenuIcon("ouvrir_20")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="22",
       itemOpt=new BuOptionItem("22x22 (kde)",
				BuResource.BU.getMenuIcon("ouvrir_22")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="24",
       itemOpt=new BuOptionItem("24x24 (gnome)",
				BuResource.BU.getMenuIcon("ouvrir_24")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="26",
       itemOpt=new BuOptionItem("26x26 (swing)",
				BuResource.BU.getMenuIcon("ouvrir_26")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="32",
       itemOpt=new BuOptionItem("32x32 (beos)",
				BuResource.BU.getMenuIcon("ouvrir_32")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="48",
       itemOpt=new BuOptionItem("48x48 (grem)",
				BuResource.BU.getMenuIcon("ouvrir_48")));
    optionsStr_.put(itemOpt, itemStr);
    optionsStr_.put
      (itemStr="64",
       itemOpt=new BuOptionItem("64x64 (real)",
				BuResource.BU.getMenuIcon("ouvrir_64")));
    optionsStr_.put(itemOpt, itemStr);
    */

    Object[] items;
    
    setLayout(new BuVerticalLayout(5));
    setBorder(EMPTY5555);

    // Desktop
    loGenDesk_=new BuFormLayout(5,5,BuFormLayout.NONE,BuFormLayout.NONE);
    pnGenDesk_=new BuPanel(loGenDesk_);
    pnGenDesk_.setBorder(new CompoundBorder
                         (new BuTitledBorder(getS("Ic�nes")),EMPTY5555));

    // Icon set
    lbIconSet_=new BuLabel(getS("Aspect:"));
    /*
    items=new BuOptionItem[8];
    items[0]=optionsStr_.get("16");
    items[1]=optionsStr_.get("20");
    items[2]=optionsStr_.get("22");
    items[3]=optionsStr_.get("24");
    items[4]=optionsStr_.get("26");
    items[5]=optionsStr_.get("32");
    items[6]=optionsStr_.get("48");
    items[7]=optionsStr_.get("64");
    */

    items=new BuOptionItem[FAMILIES.length];
    for(int i=0;i<FAMILIES.length;i++)
      items[i]=optionsStr_.get(FAMILIES[i]);

    chIconSet_=new BuComboBox(items);
    chIconSet_.setRenderer(cbRenderer_);
    chIconSet_.setEditable(false);
    chIconSet_.addActionListener(this);
    //pnGenDesk_.add(lbIconSet_,BuFormLayout.constraint(0,0,  false,1.f));
    //pnGenDesk_.add(chIconSet_,BuFormLayout.constraint(1,0,3,true ,0.f));
    loGenDesk_.packIn(lbIconSet_,pnGenDesk_).right();
    loGenDesk_.packAs(chIconSet_,lbIconSet_).cnext().left().cspan(5);//.hf();

    // Grey
    //lbGrey_=new BuLabel(_("Gris:"));
    cbGrey_=new BuCheckBox(getS("gris"));//_("activ�"));
    cbGrey_.addActionListener(this);
    //pnGenDesk_.add(lbGrey_,BuFormLayout.constraint(0,1,  false,1.f));
    //pnGenDesk_.add(cbGrey_,BuFormLayout.constraint(1,1,3,true ,0.f));
    //loGenDesk_.packAs(lbGrey_,lbIconSet_).rnext();
    loGenDesk_.packAs(cbGrey_,chIconSet_).rnext();

    // Icons in menus
    //lbMenuIcon_=new BuLabel(_("Menus:"));
    cbMenuIcon_=new BuCheckBox(getS("Montrer les ic�nes de menu"));
    cbMenuIcon_.addActionListener(this);
    //pnGenDesk_.add(lbGrey_,BuFormLayout.constraint(0,1,  false,1.f));
    //pnGenDesk_.add(cbGrey_,BuFormLayout.constraint(1,1,3,true ,0.f));
    //loGenDesk_.packAs(lbMenuIcon_,lbGrey_).rnext();
    loGenDesk_.packAs(cbMenuIcon_,cbGrey_).rnext();

    build();

    add(pnGenDesk_);

    updateComponents();
  }
  
  protected void build()
  {
    // DefaultSize
    lbDefaultSize_=new BuLabel(getS("D�faut:"));
    chDefaultSize_=new BuComboBox(SIZES);
    chDefaultSize_.setEditable(false);
    chDefaultSize_.addActionListener(this);
    //pnGenDesk_.add(lbDefaultSize_,BuFormLayout.constraint(0,2,false,1.f));
    //pnGenDesk_.add(chDefaultSize_,BuFormLayout.constraint(1,2,true ,0.f));
    loGenDesk_.packAs(lbDefaultSize_,lbIconSet_).row(3);
    loGenDesk_.packAs(chDefaultSize_,chIconSet_).row(3).cspan(1).hf();

    // ButtonSize
    lbButtonSize_=new BuLabel(getS("Boutons:"));
    chButtonSize_=new BuComboBox(SIZES);
    chButtonSize_.setEditable(false);
    chButtonSize_.addActionListener(this);
    //pnGenDesk_.add(lbButtonSize_,BuFormLayout.constraint(2,2,false,1.f));
    //pnGenDesk_.add(chButtonSize_,BuFormLayout.constraint(3,2,true ,0.f));
    loGenDesk_.packAs(lbButtonSize_,lbDefaultSize_).cnext().cnext();
    loGenDesk_.packAs(chButtonSize_,chDefaultSize_).cnext().cnext();

    // MenuSize
    lbMenuSize_=new BuLabel(getS("Menus:"));
    chMenuSize_=new BuComboBox(SIZES);
    chMenuSize_.setEditable(false);
    chMenuSize_.addActionListener(this);
    //pnGenDesk_.add(lbMenuSize_,BuFormLayout.constraint(0,2,false,1.f));
    //pnGenDesk_.add(chMenuSize_,BuFormLayout.constraint(1,2,true ,0.f));
    loGenDesk_.packAs(lbMenuSize_,lbDefaultSize_).rnext();
    loGenDesk_.packAs(chMenuSize_,chDefaultSize_).rnext();

    // FrameSize
    lbFrameSize_=new BuLabel(getS("Fen�tres:"));
    chFrameSize_=new BuComboBox(SIZES);
    chFrameSize_.setEditable(false);
    chFrameSize_.addActionListener(this);
    //pnGenDesk_.add(lbFrameSize_,BuFormLayout.constraint(2,2,false,1.f));
    //pnGenDesk_.add(chFrameSize_,BuFormLayout.constraint(3,2,true ,0.f));
    loGenDesk_.packAs(lbFrameSize_,lbMenuSize_).cnext().cnext();
    loGenDesk_.packAs(chFrameSize_,chMenuSize_).cnext().cnext();

    // TabSize
    lbTabSize_=new BuLabel(getS("Onglets:"));
    chTabSize_=new BuComboBox(SIZES);
    chTabSize_.setEditable(false);
    chTabSize_.addActionListener(this);
    //pnGenDesk_.add(lbTabSize_,BuFormLayout.constraint(0,3,false,1.f));
    //pnGenDesk_.add(chTabSize_,BuFormLayout.constraint(1,3,true ,0.f));
    loGenDesk_.packAs(lbTabSize_,lbMenuSize_).rnext();
    loGenDesk_.packAs(chTabSize_,chMenuSize_).rnext();

    // ToolSize
    lbToolSize_=new BuLabel(getS("Outils:"));
    chToolSize_=new BuComboBox(SIZES);
    chToolSize_.setEditable(false);
    chToolSize_.addActionListener(this);
    //pnGenDesk_.add(lbToolSize_,BuFormLayout.constraint(2,3,false,1.f));
    //pnGenDesk_.add(chToolSize_,BuFormLayout.constraint(3,3,true ,0.f));
    loGenDesk_.packAs(lbToolSize_,lbFrameSize_).rnext();
    loGenDesk_.packAs(chToolSize_,chFrameSize_).rnext();
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
  }

  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
    { return false; }
  
  @Override
  public void applyPreferences()
  {
    //fillTable();
    //options_.applyOn(appli_);
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }
  
  // protected methods
  
  protected void fillTable()
  {
    Object o;

    o=chIconSet_.getSelectedItem();
    o=(o==null) ? FAMILIES[0] : optionsStr_.get(o);
    options_.putStringProperty("icons.family",o.toString());

    options_.putBooleanProperty("icons.grey",cbGrey_    .isSelected());
    options_.putBooleanProperty("icons.menu",cbMenuIcon_.isSelected());

    o=chDefaultSize_.getSelectedItem();
    if(o==null) o="";
    options_.putStringProperty("icons.size" ,o.toString());

    o=chButtonSize_.getSelectedItem();
    if(o==null) o="";
    options_.putStringProperty("icons.buttonsize" ,o.toString());

    o=chMenuSize_.getSelectedItem();
    if(o==null) o="";
    options_.putStringProperty("icons.menusize" ,o.toString());

    o=chFrameSize_.getSelectedItem();
    if(o==null) o="";
    options_.putStringProperty("icons.framesize",o.toString());

    o=chTabSize_.getSelectedItem();
    if(o==null) o="";
    options_.putStringProperty("icons.tabsize"  ,o.toString());

    o=chToolSize_.getSelectedItem();
    if(o==null) o="";
    options_.putStringProperty("icons.toolsize" ,o.toString());

    setDirty(false);
  }

  protected void updateComponents()
  {
    chIconSet_.setSelectedItem
      (optionsStr_.get
       (options_.getStringProperty("icons.family",FAMILIES[0])));

    cbGrey_.setSelected
      (options_.getBooleanProperty("icons.grey"));
    cbMenuIcon_.setSelected
      (options_.getBooleanProperty("icons.menu"));

    chDefaultSize_.setSelectedItem
       (options_.getStringProperty("icons.size"," "));
    chButtonSize_.setSelectedItem
       (options_.getStringProperty("icons.buttonsize"," "));
    chMenuSize_.setSelectedItem
       (options_.getStringProperty("icons.menusize"," "));
    chFrameSize_.setSelectedItem
      (options_.getStringProperty("icons.framesize"," "));
    chTabSize_.setSelectedItem
      (options_.getStringProperty("icons.tabsize"  ," "));
    chToolSize_.setSelectedItem
      (options_.getStringProperty("icons.toolsize" ," "));

    setDirty(false);
  }
}
