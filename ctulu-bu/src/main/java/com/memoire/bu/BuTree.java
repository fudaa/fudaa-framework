/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuTree.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseEvent;
import java.util.TooManyListenersException;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeModel;

/**
 * The Swing tree.
 */
public class BuTree
  extends JTree
  implements DtDragSensible
{
  public BuTree()
  {
    super();
  }

  public BuTree(TreeModel _model)
  {
    super(_model);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);

    if(drop_)
    {
      Rectangle r=getDropRect();
      //_g.setColor(new Color(255,128,0,64));
      //_g.fillRoundRect(r.x,r.y,r.width,r.height,9,9);
      _g.setColor(new Color(255,128,0));
      _g.drawRoundRect(r.x,r.y,r.width-1,r.height-1,9,9);
    }
  }

  // Drop

  @Override
  public void setDropTarget(DropTarget _dt)
  {
    super.setDropTarget(_dt);
    if(_dt!=null)
    {
      //FuLog.debug("BTR: setDropTarget");
      try { _dt.removeDropTargetListener(DtDragSensible.SINGLETON); }
      catch(IllegalArgumentException ex) { }
      try { _dt.addDropTargetListener(DtDragSensible.SINGLETON); }
      catch(TooManyListenersException ex) { }
    }
  }

  protected boolean drop_;

  protected Rectangle getDropRect()
  {
    Rectangle r=getVisibleRect();
    /*
    Insets    i=getInsets();
    r.x     +=3;
    r.y     +=3;
    r.width -=6;
    r.height-=6;
    */
    return r;
  }

  @Override
  public void dragEnter(boolean _acceptable, Point _location)
  {
    dragOver(_acceptable,_location);
  }

  @Override
  public void dragOver(boolean _acceptable, Point _location)
  {
    if(!drop_&&_acceptable) { drop_=true; repaint(); }
  }

  @Override
  public void dragExit()
  {
    if(drop_) { drop_=false; repaint(); }
  }

  // bug swing 1.0
  @Override
  public Dimension getPreferredScrollableViewportSize()
  {
    Dimension r;
    try { r=super.getPreferredScrollableViewportSize(); }
    catch(Exception ex) { r=new Dimension(120,80); }
    return r;
  }

  // bug swing 1.0
  @Override
  public Dimension getPreferredSize()
  {
    Dimension r;
    try { r=super.getPreferredSize(); }
    catch(Exception ex) { r=new Dimension(120,80); }
    return r;
  }

  public boolean getRequestFocusEnabled()
  {
    return getModel().getRoot()!=null;
  }

  @Override
  public boolean isFocusTraversable()
  {
    return getModel().getRoot()!=null;
  }

  /*
  protected void processMouseMotionEvent(MouseEvent _evt)
  {
    Object p=getClientProperty("DND_EVENTS");
    if(p==null)
      super.processMouseMotionEvent(_evt);
    else
      ((MouseMotionListener)p).mouseDragged(_evt);
  }
  */

  public Point computePopupMenuLocation(MouseEvent _evt)
  {
    Point p=new Point(_evt.getX(),_evt.getY());
    int   i=getRowForLocation(p.x,p.y);
    if(i>=0)
    {
      Rectangle r=getRowBounds(i);
      if(r!=null)
      {
        p.x=0;
        p.y=r.y+r.height;

        Container q=getParent();
        if(q!=null)
        {
          q=q.getParent();
          if(q instanceof BuScrollPane)
          {
            BuScrollPane s=(BuScrollPane)q;
            int          h=s.getHeight();
            int          b=s.getInsets().bottom;
            p=SwingUtilities.convertPoint(this,p,s);
            if(p.x<0  ) p.x=0;
            if(p.y>h-b) p.y=h-b;
            p=SwingUtilities.convertPoint(s,p,this);
          }
        }
      }
    }
    return p;
  }
}
