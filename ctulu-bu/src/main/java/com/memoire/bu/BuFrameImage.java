/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuFrameImage.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.PrintJob;
import java.io.File;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * A small image displayer.
 */
public class BuFrameImage
  extends BuInternalFrame 
  implements BuPrintable, BuBorders
{
  private BuCommonImplementation app_;
  private JComponent             content_;
  private BuPicture              pane_;
  private BuScrollPane           sp_;

  public BuFrameImage()
  { this(null,null,null); }

  public BuFrameImage(BuCommonImplementation _app, String _file, Image _img)
  {
    super("",true,true,true,true);
    app_=_app;

    setName("ifIMAGEUR");
    if(app_!=null) app_.installContextHelp(getRootPane(),"bu/p-imageur.html");

    content_=(JComponent)getContentPane();
    content_.setBorder(EMPTY0000);

    pane_=new BuPicture();
    pane_.setOpaque(true);
    pane_.setBackground(Color.white);
    pane_.setBorder(EMPTY5555);
    pane_.setMode(BuPicture.LEFT);

    sp_=new BuScrollPane(pane_);
    sp_.setBorder(EMPTY0000);
    content_.add(sp_,BuBorderLayout.CENTER);

    if(_file!=null) setTitle(new File(_file).getName());
    else            setTitle(getString("Image"));

    setFrameIcon(BuResource.BU.getFrameIcon("image"));

    setImage(_img);
  }

  public Image getImage()
  {
    return pane_.getImage();
  }

  public void setImage(Image _img)
  {
    pane_.setImage(_img);
    Dimension ps=getPreferredSize();
    if(ps.width >370) ps.width =296;
    if(ps.height>370) ps.height=296;
    if(ps.width <148) ps.width =148;
    if(ps.height< 74) ps.height= 74;
    setSize(ps);
  }

  @Override
  public void setBounds(int _x, int _y, int _w, int _h)
  {
    super.setBounds(_x,_y,_w,_h);

    if(pane_!=null)
    {
      pane_.computePreferredSize();
      Dimension ps=pane_.getPreferredSize();
      sp_.getViewport().setViewPosition(new Point(0,0));
      sp_.getViewport().setViewSize    (ps);
    }
  }

  /*
  public void setSelected(boolean _state) throws PropertyVetoException
  {
    super.setSelected(_state);
    if(isSelected()) pane_.requestFocus();
  }
  */

  // BuInternalFrame

  @Override
  public String[] getEnabledActions()
  {
    String[] r=new String[]
      {
	"IMPRIMER","PREVISUALISER",
	//"COPIER"
      };
    return r;
  }

  // BuPrintable

  @Override
  public void print(PrintJob _job, Graphics _g)
  {
    BuPrinter.INFO_DOC     =new BuInformationsDocument();
    BuPrinter.INFO_DOC.name=getTitle();
    BuPrinter.INFO_DOC.logo=BuResource.BU.getIcon("image",24);

    BuPrinter.printImage(_job,_g,getImage());
  }

  // Test

  public static void main(String[] _args)
  {
    JFrame         f=new JFrame("test BuFrameImage");
    BuFrameImage   b=new BuFrameImage();
    BuFrameImage   c=new BuFrameImage();

    BuDesktop d=new BuDesktop();
    d.add(b);
    d.add(c);

    f.getContentPane().add("Center",d);
    f.setSize(300,400);
    f.setVisible(true);
    f.setLocation(200,100);

    b.setImage(BuResource.BU.getImage("background_desktop_1.gif"));
    c.setImage(BuResource.BU.getImage("ouvrir_24.gif"));
    b.setLocation(70,30);
  }
}
