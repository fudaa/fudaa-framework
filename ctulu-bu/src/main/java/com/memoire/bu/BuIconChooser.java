/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuIconChooser.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuRefTable;
import com.memoire.fu.FuSort;
import com.memoire.fu.FuVectorString;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

/**
 * A simple icon chooser.
 */
public class BuIconChooser
  extends BuDialog
  implements ActionListener, ListSelectionListener
{
  private static final String[] DIRECTORIES=new String[]
    { "intern:", "/usr/share/icons","/usr/share/pixmaps" };

  private static final Icon NO_ICON=new BuFixedSizeIcon(null,32,32);

  protected BuComboBox           chDirectory_;
  protected DefaultComboBoxModel mdDirectory_;

  protected BuButton         btValider_;
  protected BuButton         btAnnuler_;
  protected BuTable          tbIcones_;
  protected BuScrollPane     spIcones_;
  protected BuLabelMultiLine lbExemple_;

  private Hashtable names_;

  public BuIconChooser(BuCommonInterface      _parent,
		       BuInformationsSoftware _isoft,
		       String                 _title,
		       String                 _value)
  {
    super(_parent,_isoft,_title);

    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btValider_=new BuButton(BuResource.BU.loadButtonCommandIcon("VALIDER"),
                            getString("Valider"));
    btValider_.addActionListener(this);
    getRootPane().setDefaultButton(btValider_);
    pnb.add(btValider_);

    btAnnuler_=new BuButton(BuResource.BU.loadButtonCommandIcon("ANNULER"),
                            getString("Annuler"));
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);

    content_.add(pnb,BuBorderLayout.SOUTH);

    chDirectory_.addActionListener(this);

    ListSelectionModel lsm;

    lsm=tbIcones_.getSelectionModel();
    lsm.addListSelectionListener(this);
    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    lsm=tbIcones_.getColumnModel().getSelectionModel();
    lsm.addListSelectionListener(this);
    lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    setValue(_value);
    btValider_.setEnabled(valeur_!=null);
  }

  @Override
  public boolean needsScrollPane()
  {
    return false;
  }

  @Override
  public JComponent getComponent()
  {
    mdDirectory_=new DefaultComboBoxModel(DIRECTORIES);
    chDirectory_=new BuComboBox(mdDirectory_);

    tbIcones_=new BuTable()
    {
      private TableCellRenderer tcr_=new BuTableCellRenderer()
        {
          @Override
          public Component getTableCellRendererComponent
            (JTable _table, Object _value, boolean _selected,
             boolean _focus, int _row, int _column) 
          {
            Icon icon=BuIconChooser.this.getIcon((String)_value);
            //_value="";
            Component r=super.getTableCellRendererComponent
              (_table,"",_selected,_focus,_row,_column);
            if(r instanceof JLabel)
            {
              ((JLabel)r).setIcon(icon);//(Icon)_value);
              ((JLabel)r).setHorizontalAlignment(SwingConstants.CENTER);
            }
            return r;
          }
        };

      @Override
      public TableCellRenderer getDefaultRenderer(Class _columnClass)
      {
        return tcr_;
      }

      @Override
      public void setModel(TableModel _m)
      {
        super.setModel(_m);
        final int h=40;
        setRowHeight(h);
        //tbIcones_.setVisible(false);
        for(int i=0;i<getColumnCount();i++)
        {
          TableColumn c=getColumn(getColumnName(i));
          //c.setCellRenderer(tcr);
          c.setPreferredWidth(h);
          c.setMinWidth (h);
          c.setMaxWidth (h);
          c.setResizable(false);
        }
      }
    };
    tbIcones_.setModel(getModel(getData(DIRECTORIES[0])));
    tbIcones_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    tbIcones_.setColumnSelectionAllowed(true);

    lbExemple_=new BuLabelMultiLine();
    lbExemple_.setFirstMode(true);
    lbExemple_.setWrapMode(BuLabelMultiLine.LETTER);
    lbExemple_.setIcon(NO_ICON);
    //setPreferredSize(new Dimension(200,100));

    spIcones_=new BuScrollPane(tbIcones_);

    BuPanel r=new BuPanel(new BuBorderLayout(5,5));
    r.add(chDirectory_,BuBorderLayout.NORTH);
    r.add(spIcones_   ,BuBorderLayout.CENTER);
    r.add(lbExemple_  ,BuBorderLayout.SOUTH);
    return r;
  }

  private TableModel getModel(String[] _data)
  {
    final int cols=11;

    int l=_data.length;
    int rows=l/cols+(l%cols==0 ? 0 : 1);
    Object[][] r=new Object[rows][cols];
    String[]   n=new String[cols];

    for(int i=0;i<l;i++)
      r[i/cols][i%cols]=_data[i];
    for(int i=0;i<cols;i++)
      n[i]=""+(i+1);
    return new BuTableStaticModel(r,n);
  }

  private String[] getData(String _path)
  {
    names_=new Hashtable();

    FuVectorString r=new FuVectorString();

    if("intern:".equals(_path))
    {
      Properties  p =new Properties();
      InputStream in=null;
      try
      {
        in=getClass().getResourceAsStream("iconkeys.txt");
        if(in!=null)
        {
          p.load(in);
          FuLog.debug("BIC: iconkeys loaded: "+p.size()+" names");
          Enumeration e=p.keys();
          while(e.hasMoreElements())
          {
            String k=(String)e.nextElement();
            //if((k!=null)&&(v!=null)) iconmap_.setProperty(k,v);
            if(k!=null)
            {
              String v=p.getProperty(k);
              k="intern:"+k;
              r.addElement(k);
              names_.put(k,v);
            }
          }
        }
      }
      catch(IOException ex)
      {
        FuLog.debug("BIC: iconkeys: "+ex);
      }
      finally
      {
        FuLib.safeClose(in);
      }
    }
    else
    {
      File d=new File(_path);
      if(d.exists())
      {
        String[] f=d.list();
        if(f!=null)
        {
          FuSort.sort(f);
          for(int j=0;j<f.length;j++)
          {
            String n=f[j].toLowerCase();
            if(n.endsWith(".png")||
               n.endsWith(".gif"))
            {
              File g=new File(d,f[j]);
              if(g.isFile())
              {
                try
                {
                  URL u=FuLib.toURL(g);
                  String k=u.toString();
                  r.addElement(k);
                  String v=f[j];
                  v=v.substring(0,v.length()-4);
                  names_.put(k,v);
                }
                catch(MalformedURLException ex) { }
              }
            }
          }
          for(int j=0;j<f.length;j++)
          {
            File g=new File(d,f[j]);
            if(g.isDirectory())
            {
              String[] s=g.list();
              if((s!=null)&&(s.length>0))
              {
                boolean ok=false;
                for(int k=0;k<s.length;k++)
                {
                  String n=s[k].toLowerCase();
                  if(n.endsWith(".png")||
                     n.endsWith(".gif"))
                  {
                    ok=true;
                    break;
                  }
                }
                if(ok)
                {
                  String p=g.getAbsolutePath();
                  if(mdDirectory_.getIndexOf(p)<0)
                    mdDirectory_.addElement(p);
                }
              }
            }
          }
        }
      }
    }

    return r.toArray();
  }

  private final String value()
  {
    String r=null;
    int col=tbIcones_.getSelectedColumn();
    int row=tbIcones_.getSelectedRow();
    if((col>=0)&&(row>=0))
      r=(String)tbIcones_.getValueAt(row,col);
    return r;
  }

  @Override
  public void valueChanged(ListSelectionEvent _evt)
  {
    setValue(value());
    btValider_.setEnabled(valeur_!=null);
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    //System.err.println("BFC: icon is "+valeur_);

    if(source==chDirectory_)
    {
      String d=(String)chDirectory_.getSelectedItem();
      if(d!=null)
      {
        tbIcones_.setModel(getModel(getData(d)));
        setValue(null);
      }
    }
    else
    if(source==btValider_)
    {
      reponse_=JOptionPane.OK_OPTION;
      //valeur_ =tfValeur_.getText();
      setVisible(false);
    }
    else
    if(source==btAnnuler_)
    {
      reponse_=JOptionPane.CANCEL_OPTION;
      valeur_ =null;
      setVisible(false);
    }
  }

  private String valeur_;

  public String getValue()
  {
    return valeur_;
  }

  public void setValue(String _value)
  {
    valeur_=_value;
    lbExemple_.setIcon(getIcon(valeur_));
    if(valeur_!=null)
      lbExemple_.setText(names_.get(valeur_)+"\n"+valeur_);
    else
      lbExemple_.setText(" \n ");
  }

  private FuRefTable icons_=new FuRefTable(FuRefTable.STRONG,FuRefTable.SOFT);

  protected Icon getIcon(String _s)
  {
    Icon icon=null;
    if(_s!=null)
    {
      icon=(Icon)icons_.get(_s);
      if(icon==null)
      {
        if(_s.startsWith("intern:"))
          icon=BuResource.BU.getIcon(_s.substring(7));
        else
          icon=new BuIcon(_s);

        if(icon!=null)
        {
          int wi=icon.getIconWidth();
          int hi=icon.getIconHeight();

          if(((wi>32)||(hi>32))&&(icon instanceof BuIcon))
          {
            icon=BuResource.resizeIcon((BuIcon)icon,32);
            wi=hi=32;
          }

          if((wi!=32)||(hi!=32))
          {
            icon=new BuFixedSizeIcon(icon,32,32);
            //wi=hi=32;
          }

          icons_.put(_s,icon);
        }
      }
    }
    else icon=NO_ICON;

    return icon;
  }

  @Override
  public void show()
  {
    spIcones_.setColumnHeaderView(null);//new BuLabel());
    pack();
    super.show();
  }


  public static void main(String[] _args)
  {
    BuIconChooser d;

    d=new BuIconChooser(null,null,"Test BuIconChooser",null);
    d.activate();

    System.err.println("Selection: "+d.getValue());
    System.exit(0);
  }
}
