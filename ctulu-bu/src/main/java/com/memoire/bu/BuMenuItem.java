/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuMenuItem.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;

/**
 * Like JMenuItem but with better management of icons.
 */
public class BuMenuItem
  extends JMenuItem
{
	
	
	private String id_;
	
	
  public BuMenuItem()
  {
    this("",(BuIcon)null);
  }

  public BuMenuItem(BuMenuItem clone)
  {
	  if(clone.getIcon()!=null)
		  this.setIcon(clone.getIcon());
	  if(clone.getText()!=null)
			  this.setText(clone.getText());
	  if(clone.getActionCommand()!=null)
		  this.setActionCommand(clone.getActionCommand());
	  
	  if(clone.getActionListeners()!=null)
		  for(int i=0;i<clone.getActionListeners().length;i++)
			  this.addActionListener(clone.getActionListeners()[i]);
	  if(clone.getAccelerator()!=null)
		  this.setAccelerator(clone.getAccelerator());
	
		  this.setId(clone.getId());
  }
  
  public BuMenuItem(BuIcon _icon)
  {
    this("",_icon);
  }

  public BuMenuItem(String _label)
  {
    this(_label,(BuIcon)null);
  }

  public BuMenuItem(BuIcon _icon, String _label)
  {
    this(_label,_icon);
  }

  public BuMenuItem(String _label,BuIcon _icon)
  {
    super();

    if(_icon !=null) setIcon(_icon);
    setText(_label==null?"":_label);
  }

  public BuMenuItem(Icon _icon)
  {
    this("",_icon);
  }

  public BuMenuItem(String _label,Icon _icon)
  {
    super();

    if(_icon !=null) setIcon(_icon);
    setText(_label==null?"":_label);
  }

  public BuMenuItem(Action a)
  {
    
	setAction(a);
  }
  
  // Icon

  @Override
  public Icon getIcon()
  {
    if(BuPreferences.BU.getBooleanProperty("icons.menu",true)||
       (super.getText()==null))
      return super.getIcon();
    return null;
  }

  public void setIcon(BuIcon _icon)
  {
    BuLib.setIcon(this,_icon);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }


public boolean equals(Object obj) {
	if(obj instanceof BuMenuItem){}
	else return false;
	if(obj==this)
		return true;
	
	
 BuMenuItem clone=(BuMenuItem)obj;
 if(clone.getId()==null || this.getId()==null)
	 return false;
 if(!clone.getId().equals(this.getId()))
	 return false;
//	 
//		  if(clone.getText()!=null && this.getText()!=null )
//				if(!  this.getText().equals(clone.getText()))
//					return false;
//		  
//		  if(clone.getActionCommand()!=null && this.getActionCommand()!=null )
//			 if(!clone.getActionCommand().equals(this.getActionCommand())) 
//				 return false;
//		  List<ActionListener> liste=new ArrayList<ActionListener>();
//		  if(clone.getActionListeners()!=null)
//			  for(int i=0;i<clone.getActionListeners().length;i++)
//				  liste.add(clone.getActionListeners()[i]);
//		  if(clone.getActionListeners()!=null){
//			  for(int i=0;i<this.getActionListeners().length;i++)
//				  if(!liste.contains(this.getActionListeners()[i]))
//					  return false;
//				  
//		  }else if (liste.size()>0)
//			  return false;
//				  
		   
	  
	
	
	
	return true;
}

@Override
public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=super.getToolTipLocation(_evt);
    if(r==null) r=new Point(getWidth(),0);
    return r;
  }

/**
 * @param id_ the id_ to set
 */
public void setId(String id_) {
  this.id_ = id_;
}

/**
 * @return the id_
 */
public String getId() {
  return id_;
}
}
