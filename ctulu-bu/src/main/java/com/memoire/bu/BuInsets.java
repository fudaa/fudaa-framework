/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         BuInsets.java
 * @version      0.42
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Insets;

/**
 * A set of widely used insets.
 */
public interface BuInsets
{
  Insets INSETS0000=new Insets(0,0,0,0);
  Insets INSETS0202=new Insets(0,2,0,2);
  Insets INSETS1111=new Insets(1,1,1,1);
}

