/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuPreferencesFrame.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.JComponent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 * An internal frame which groups all the panels to set
 * user preferences.
 */
public class BuPreferencesMainPanel
  extends BuPanel
  implements ActionListener, PropertyChangeListener, BuBorders,
             TreeSelectionListener //ChangeListener,
{
  private BuTree       trTree_;
  private JComponent   view_;
  //private BuTabbedPane tpMain_;
  private BuPanel      pnButtons_;
  private BuButton     btValider_, btAppliquer_, btAnnuler_, btFermer_;

  private Model     model_;
  private Hashtable map_;


  // Constructeur

  public BuPreferencesMainPanel()
  {
    this(null);
  }

  BuButton getFermerButton(){
    return btFermer_;
  }
  BuButton getValiderButton(){
    return btValider_;
  }

  public BuPreferencesMainPanel(BuCommonImplementation _app)
  {
    setName("pnPREFERENCES");
    if(_app!=null)
      _app.installContextHelp(getRootPane(),"bu/p-preferences.html");

    model_=new Model();
    map_  =new Hashtable();

    trTree_=new BuTree(model_);
    trTree_.setCellRenderer(new Renderer());
    trTree_.getSelectionModel()
      .setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    trTree_.addTreeSelectionListener(this);
    trTree_.setRootVisible(false);
    trTree_.setShowsRootHandles(true);

    /*
    tpMain_=new BuTabbedPane();
    tpMain_.setTabPlacement(BuLib.getTabPlacement());
    tpMain_.addChangeListener(this);
    */

    // Panneau de validation
    pnButtons_=new BuPanel();
    pnButtons_.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btAppliquer_=new BuButton(getS("Appliquer"));
    btAppliquer_.setIcon(BuResource.BU.loadButtonCommandIcon("APPLIQUER"));
    btAppliquer_.setActionCommand("APPLIQUER");
    btAppliquer_.addActionListener(this);
    pnButtons_.add(btAppliquer_,0);

    btAnnuler_=new BuButton(getS("Annuler"));
    btAnnuler_.setIcon(BuResource.BU.loadButtonCommandIcon("ANNULER"));
    btAnnuler_.setActionCommand("ANNULER");
    btAnnuler_.addActionListener(this);
    pnButtons_.add(btAnnuler_,1);

    btValider_=new BuButton(getS("Ecrire"));
    btValider_.setIcon(BuResource.BU.loadButtonCommandIcon("VALIDER"));
    btValider_.setActionCommand("VALIDER");
    btValider_.addActionListener(this);
    //getRootPane().setDefaultButton(btValider_);
    pnButtons_.add(btValider_,2);

    btFermer_=new BuButton(getS("Fermer"));
    btFermer_.setIcon(BuResource.BU.loadButtonCommandIcon("FERMER"));
    btFermer_.setActionCommand("FERMER");
    btFermer_.addActionListener(this);
    pnButtons_.add(btFermer_,3);

    setLayout(new BuBorderLayout(5,5));
    setBorder(EMPTY5555);
    add(new BuLabel("<html><i>"+
      BuResource.BU.getString("<u>Attention</u> : La plupart des pr�f�rences ne seront prises<br>en compte qu'apr�s red�marrage de l'application")
      +"</i></html>"),BorderLayout.NORTH);
    add(new BuScrollPane(trTree_),BuBorderLayout.WEST  );
    //content.add(view_                    ,BuBorderLayout.CENTER);
    add(pnButtons_               ,BuBorderLayout.SOUTH );
    BuLib.computeMnemonics(pnButtons_,this);
    updateButtons();
    
    setPreferredSize(new Dimension(750, getPreferredSize().height));
  }

  // Evenements

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String actionCmd=_evt.getActionCommand();
    if(actionCmd.equals("VALIDER"))
    {
      BuPreferencesCategory c=getSelectedCategory();
      if(c!=null) c.validatePreferences();
      BuAbstractPreferencesPanel p=getSelectedPanel();
      if(p!=null) p.validatePreferences();
    }
    else
    if(actionCmd.equals("APPLIQUER"))
    {
      BuPreferencesCategory c=getSelectedCategory();
      if(c!=null) c.applyPreferences();
      BuAbstractPreferencesPanel p=getSelectedPanel();
      if(p!=null) p.applyPreferences();
    }
    else
    if(actionCmd.equals("ANNULER"))
    {
      BuPreferencesCategory c=getSelectedCategory();
      if(c!=null) c.cancelPreferences();
      BuAbstractPreferencesPanel p=getSelectedPanel();
      if(p!=null) p.cancelPreferences();
    }

    updateButtons();
  }


  /*
  public void stateChanged(ChangeEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    updateButtons();
  }
  */

  @Override
  public void valueChanged(TreeSelectionEvent _evt)
  {
    JComponent content=this;
    if(view_!=null) content.remove(view_);

    view_=getSelectedComponent();
    if(view_!=null)
    {
      content.add(view_,BuBorderLayout.CENTER);
      content.revalidate();
      content.repaint();
    }

    updateButtons();

    Dimension ps=getPreferredSize();
    Dimension cs=getSize();
    if((ps.width>cs.width)||(ps.height>cs.height)) doLayout();
  }

  @Override
  public void propertyChange(PropertyChangeEvent _evt)
  {
    if("dirty".equals(_evt.getPropertyName()))
      updateButtons();
  }

  private void updateButtons()
  {
    boolean dirty=false;

    BuPreferencesCategory c=getSelectedCategory();
    if(c!=null)
    {
      dirty=c.isDirty();
      btAppliquer_.setEnabled(dirty&&c.isPreferencesApplyable ());
      btAnnuler_  .setEnabled(dirty&&c.isPreferencesCancelable());
      btValider_  .setEnabled(dirty&&c.isPreferencesValidable ());
    }

    BuAbstractPreferencesPanel p=getSelectedPanel();
    if(p!=null)
    {
      dirty=p.isDirty();
      btAppliquer_.setEnabled(dirty&&p.isPreferencesApplyable ());
      btAnnuler_  .setEnabled(dirty&&p.isPreferencesCancelable());
      btValider_  .setEnabled(dirty&&p.isPreferencesValidable ());
    }

    if(!dirty)
    {
      BuPreferencesCategory[] ac=model_.getCategories();
      for(int i=0;i<ac.length;i++)
	if(ac[i].isDirty())
          dirty=true;
    }

    btFermer_.setEnabled(!dirty);
    trTree_.repaint();
  }

  // Methodes publiques

  public void selectTab(int _index)
  {
    //tpMain_.setSelectedIndex(_index);
    trTree_.setSelectionRow(_index);
  }

  public void addTab(BuAbstractPreferencesPanel _newtab)
  {
   /* if(!(_newtab instanceof JComponent))
      throw new RuntimeException(_newtab+" is not a JComponent");*/

    _newtab.setBorder(EMPTY0000);

    //if(_newtab instanceof Container)
    //BuLib.computeMnemonics((Container)_newtab,null);

    String category=_newtab.getCategory();
    //String title   =_newtab.getTitle();
    //String tooltip =_newtab.getToolTipText();
    //tpMain_.addTab(title, null, (JComponent)_newtab, tooltip);
    getCategory(category).addTab(_newtab);
    //System.err.println("ADD TAB "+_newtab+" to "+category);
    _newtab.addPropertyChangeListener(this);

    //doLayout();
    //validate();
  }

  // Methodes privees

  private synchronized BuPreferencesCategory getCategory(String _category)
  {
    String k=FuLib.clean(_category).toUpperCase();
    BuPreferencesCategory r=(BuPreferencesCategory)map_.get(k);
    if(r==null)
    {
      BuIcon i=BuResource.BU.getIcon(k);
      if(i.isDefault()) i=null;
      else i=BuResource.BU.getToolIcon(k);

      //System.err.println("NEW CATEGORY "+k+" "+_category);
      r=new BuPreferencesCategory(_category,i);
      map_.put(k,r);
      model_.addCategory(r);
      selectTab(0);
    }
    return r;
  }

  private BuPreferencesCategory getSelectedCategory()
  {
    BuPreferencesCategory r=null;
    if(trTree_.getSelectionCount()==1)
    {
      Object o=trTree_.getSelectionPath().getLastPathComponent();
      if(o instanceof BuPreferencesCategory)
        r=(BuPreferencesCategory)o;
    }
    return r;
  }

  private BuAbstractPreferencesPanel getSelectedPanel()
  {
    BuAbstractPreferencesPanel r=null;
    if(trTree_.getSelectionCount()==1)
    {
      Object o=trTree_.getSelectionPath().getLastPathComponent();
      if(o instanceof BuAbstractPreferencesPanel)
        r=(BuAbstractPreferencesPanel)o;
    }
    return r;
  }

  private JComponent getSelectedComponent()
  {
    JComponent r=null;
    if(trTree_.getSelectionCount()==1)
    {
      Object o=trTree_.getSelectionPath().getLastPathComponent();
      if(o instanceof JComponent)
        r=(JComponent)o;
    }
    return r;
  }

  private static final class Model
    implements TreeModel
  {
    Vector categories_;
    Vector listeners_;

    public Model()
    {
      categories_=new Vector();
      listeners_ =new Vector(1,1);
    }

    public BuPreferencesCategory[] getCategories()
    {
      synchronized(categories_)
      {
        int l=categories_.size();
        BuPreferencesCategory[] r=new BuPreferencesCategory[l];
        //categories_.toArray(r);
        categories_.copyInto(r);
        return r;
      }
    }

    public int getCategoryCount()
    {
      return categories_.size();
    }

    public BuPreferencesCategory getCategory(int _index)
    {
      synchronized(categories_)
      {
        return ((_index>=0)&&(_index<categories_.size()))
          ? (BuPreferencesCategory)categories_.elementAt(_index)
          : null;
      }
    }

    public void addCategory(BuPreferencesCategory _c)
    {
      categories_.addElement(_c);
      fire();
    }

    @Override
    public int getChildCount(Object _parent)
    {
      int r=0;
      if(_parent==categories_)
        r=getCategoryCount();
      else
      if(_parent instanceof BuPreferencesCategory)
        r=((BuPreferencesCategory)_parent).getTabCount();
      else
        r=0;
      return r;
    }

    @Override
    public Object getChild(Object _parent, int _index)
    {
      Object r=null;
      if(_parent==categories_)
        r=getCategory(_index);
      else
      if(_parent instanceof BuPreferencesCategory)
        r=((BuPreferencesCategory)_parent).getTab(_index);
      /*else
        r=null;*/
      return r;
    }

    @Override
    public int getIndexOfChild(Object _parent, Object _child)
    {
      return -1;
    }

    @Override
    public boolean isLeaf(Object _node)
    {
      return (getChildCount(_node)==0);
    }

    @Override
    public Object getRoot()
    {
      return categories_;
    }

    @Override
    public void addTreeModelListener(TreeModelListener _l)
    {
      listeners_.addElement(_l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener _l)
    {
      listeners_.removeElement(_l);
    }

    private void fire()
    {
      TreeModelEvent a=new TreeModelEvent(this,new TreePath(categories_));
      Enumeration e=listeners_.elements();
      while(e.hasMoreElements())
        ((TreeModelListener)e.nextElement()).
          treeStructureChanged(a);
    }

    @Override
    public void valueForPathChanged(TreePath _path, Object _newValue)
    {
    }
  }

  private static final class Renderer
    extends BuAbstractCellRenderer
  {
    public Renderer()
    {
      super(BuAbstractCellRenderer.TREE);
    }

    @Override
    protected Component getRealRenderer
      (JComponent _comp,
       Object     _value,
       boolean    _selected,
       boolean    _expanded,
       boolean    _leaf,
       int        _row,
       int        _column,
       boolean    _focus)
    {
      Component r=super.getRealRenderer(_comp,_value,_selected,_expanded,
                                        _leaf,_row,_column,_focus);

      boolean dirty=false;
      if(_value instanceof BuPreferencesCategory)
        dirty|=((BuPreferencesCategory)_value).isDirty();
      if(_value instanceof BuAbstractPreferencesPanel)
        dirty|=((BuAbstractPreferencesPanel)_value).isDirty();
      if(dirty)
        r.setForeground(BuLib.getColor(Color.red));

      return r;
    }
  }
}
