/**
 * @modification $Date: 2006-10-19 09:39:43 $
 * @statut       unstable
 * @file         BuMonitorMemory.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * A monitor to watch free and used memories.
 */

public class BuMonitorMemory extends BuMonitorAbstract {
  protected Runtime runtime_;
  protected float megas_;
  protected long kilos_;
  protected int value_;
  // protected int maximum_;
  protected long last_;

  public BuMonitorMemory() {
    super();

    megas_ = 0;
    kilos_ = 0;
    value_ = 0;
    // maximum_=0;
    last_ = 0;
    runtime_ = Runtime.getRuntime();
    maxMem_ = (runtime_.maxMemory() / (1024F * 1024F));
    setName("buMONITOR_MEMORY");
  }

  @Override
  public int getValue() {
    return value_;
  }

  @Override
  public int getMaximum() {
    return 100;
  }

  @Override
  public String getText() {
    return ((long) megas_) + "M";
  }

  @Override
  public String getToolTipText() {
    String r = super.getToolTipText();
    if (r != null) {
      r = "<html><body>&nbsp;&nbsp;<u>" + BuResource.BU.getString("M�moire utilis�e:") + "</u> " + value_ + "% (" + ((int) megas_) + "/"
          + ((int) maxMem_) + "M)" + "<br>&nbsp;&nbsp;&nbsp;" + BuResource.BU.getString("Maximum utilis�:") + " "
          + ((int) (runtime_.totalMemory() / (1024D * 1024D))) + "M";
    }
    return r;
  }

  @Override
  public long getDelay() {
    return 2500L;
  }

  final float maxMem_;

  @Override
  public synchronized boolean refreshState() {
    boolean r = false;

    // double f=((-maxMem_)/1024D);
    megas_ = (runtime_.totalMemory() - runtime_.freeMemory()) / (1024F * 1024F);
    float pourcUsed = megas_ * 100F / maxMem_;

    // if(maximum_<pourcUsed) maximum_=(int)pourcUsed;

    if (value_ != pourcUsed) {
      r = true;
      // kilos_=(int)used;
      value_ = (int) pourcUsed;

      // 5 mn
      /*
       * if( (last_+300000L<System.currentTimeMillis()) &&!FuLib.verifyMemory(3)) { new BuDialogWarning (getApp(),null,
       * "Available memory is low (less than 3Mb).\n"+ "Save your project and relaunch the software.\n"+ "Optionnaly
       * change the option -mx of the java command.\n") .activate(); last_=System.currentTimeMillis(); }
       */
    }
    return r;
  }
}
