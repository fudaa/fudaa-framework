/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuCollapsablePanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuPreferences;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.SwingConstants;

/**
 * Collapsable Panel.
 */
public class BuCollapsablePanel
  extends BuPanel
  implements ActionListener
{
  private static final boolean COLLAPSE_HORIZONTALY=
    BuPreferences.BU.getBooleanProperty("collapse.horizontaly",true);

  private JComponent                view_;
  private BuTransparentToggleButton button_;
  private FuPreferences             options_;
  private String                    key_;

  public BuCollapsablePanel(JComponent _view)
  {
    this(_view,null,null);
  }

  public BuCollapsablePanel(JComponent _view,FuPreferences _options,String _key)
  {
    super(new BuBorderLayout(2,2));
    options_=_options;
    key_    =_key;

    setName("cpCOLLAPSABLE_PANEL");

    button_=new BuTransparentToggleButton
      (BuResource.BU.getIcon("bu_expand"),
       BuResource.BU.getIcon("bu_collapse"));
    button_.addActionListener(this);
    button_.setHorizontalAlignment(SwingConstants.CENTER);
    button_.setVerticalAlignment(SwingConstants.TOP);
    button_.setPreferredSize(new Dimension(11,11));


    if(COLLAPSE_HORIZONTALY) add(button_,BuBorderLayout.NORTH);
    else                     add(button_,BuBorderLayout.WEST);

    setView(_view);

    if(options_!=null)
      setCollapsed(options_.getBooleanProperty(key_,false));
  }

  public JComponent getView()
  {
    return view_;
  }

  public void setView(JComponent _view)
  {
    if(view_!=null) remove(view_);
    view_=_view;
    if(view_!=null) add(view_,BuBorderLayout.CENTER);
  }

  public AbstractButton getButton()
  {
    return button_;
  }

  /*
  public void setToolTipText(String _s)
  {
    button_.setToolTipText(_s);
  }

  public String getToolTipText()
  {
    return button_.getToolTipText();
  }

  public Point getToolTipLocation(MouseEvent _evt)
  {
    Point r=button_.getLocation();
    r.y+=button_.getHeight();
    return r;
  }
  */

  public boolean isCollapsed()
  {
    return (view_!=null)&&!view_.isVisible();
  }

  public void setCollapsed(boolean _b)
  {
    if((view_!=null)&&(_b==view_.isVisible()))
    {
      if(options_!=null)
        options_.putBooleanProperty(key_,_b);

      view_.setVisible(!_b);
      revalidate();
    }
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    //System.err.println(_evt);

    if(view_!=null)
    {
      String  action=_evt.getActionCommand();
      boolean state =isCollapsed();

      if("BUTTON_PRESSED".equals(action)) state^=true;
      else
      if("COLLAPSE".equals(action)) state=true;
      else
      if("EXPAND".equals(action)) state=false;

      setCollapsed(state);
    }
  }

  /*
  public void paintComponent(Graphics _g)
  {
    super.paintComponent(_g);

    if((view_!=null)&&!view_.isVisible())
    {
      int y=Math.min(getHeight()/2,7);
      int x=getWidth()-1;
      Color a,b;
      a=new Color(0,0,0,64);
      b=BuLib.getColor(a);
      if(a.equals(b))
      {
        _g.setColor(a);
        _g.drawLine(14,y,x,y);
      }
      a=new Color(255,255,255,96);
      b=BuLib.getColor(a);
      if(a.equals(b))
      {
        _g.setColor(a);
        _g.drawLine(14,y+1,x,y+1);
      }
    }
  }
  */
}
