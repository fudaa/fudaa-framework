/**
 * @modification $Date: 2005-08-16 12:58:05 $
 * @statut       unstable
 * @file         BuNumericValueValidator.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

/**
 * A validator for numerical input values with a min/max control.
 * Provides standard ones for ints, doubles, positive ints and
 * positive doubles.
 */

public class BuNumericValueValidator
  extends BuValueValidator
{
  public BuNumericValueValidator()
  {
    this(false,-Double.MAX_VALUE,Double.MAX_VALUE);
  }

  public BuNumericValueValidator
    (boolean _integer_only, double _min, double _max)
  {
    setIntegerOnly(_integer_only);
    setMin(_min);
    setMax(_max);
  }
  
  private boolean integer_only_;
  public boolean isIntegerOnly()
    { return integer_only_; }
  public void    setIntegerOnly(boolean _integer_only)
    { integer_only_=_integer_only; }

  private double min_;
  public double getMin() { return min_; }
  public void setMin(double _min) { min_=_min; }

  private double max_;
  public double getMax() { return max_; }
  public void setMax(double _max) { max_=_max; }

  @Override
  public boolean isValueValid(Object _value)
  {
    boolean r=false;

    if(   (integer_only_&&(_value instanceof Integer))
        &&(((Integer)_value).intValue()>=getMin())
        &&(((Integer)_value).intValue()<=getMax())) r=true;

    if(   (!integer_only_&&(_value instanceof Double))
	&&(((Double)_value).doubleValue()>=getMin())
	&&(((Double)_value).doubleValue()<=getMax())) r=true;

    return r;
  }

  // Instances

  /*
  public static final BuNumericValueValidator INTEGER
    =new BuNumericValueValidator(true,-Integer.MAX_VALUE,Integer.MAX_VALUE);

  public static final BuNumericValueValidator DOUBLE
    =new BuNumericValueValidator();
  */

  public static final BuNumericValueValidator POSITIVE_INTEGER
    =new BuNumericValueValidator(true,0,Integer.MAX_VALUE);

  public static final BuNumericValueValidator POSITIVE_DOUBLE
    =new BuNumericValueValidator(false,0.,Double.MAX_VALUE);
}


