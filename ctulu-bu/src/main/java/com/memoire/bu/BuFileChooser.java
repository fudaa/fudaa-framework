/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuFileChooser.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Dimension;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

/**
 * Similar to JFileChooser but with a size adapted to the screen.
 */
public class BuFileChooser
  extends JFileChooser
{
  public BuFileChooser()
  {
    super();
    setDialogTitle(BuResource.BU.getString("Fichier"));
    // BM 07/03/2023 - Aucun int�ret, et pose pb sur des �crans de petite taille.
//    Dimension e=getToolkit().getScreenSize();
//    setPreferredSize(new Dimension(Math.min(480,e.width-60),
//				   Math.max(480,e.height-360)));
  }

  /**
   * @deprecated
   */
  public BuFileChooser(File _file)
  {
    this();
    setCurrentDirectory(_file);
  }

  /**
   * @deprecated
   */
  public BuFileChooser(String _file)
  {
    this(new File(_file));
  }

  public static void main(String[] _args)
  {
    BuFileChooser p=new BuFileChooser(System.getProperty("user.dir"));
    p.setDragEnabled(true);
    p.setControlButtonsAreShown(false);
    if(p.getBorder() instanceof EmptyBorder)
      p.setBorder(BuLib.getEmptyBorder(5));
    JFrame f=new JFrame("File Chooser");
    f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    f.getContentPane().add(p);
    f.pack();
    f.setVisible(true);
  }
}
