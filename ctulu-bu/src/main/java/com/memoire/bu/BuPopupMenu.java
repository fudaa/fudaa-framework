/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuPopupMenu.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

/**
 * A popup menu which follows action events and has simplified adding methods.
 */
public class BuPopupMenu extends JPopupMenu implements BuMenuInterface {
  private boolean sticky_;

  public BuPopupMenu() {
    this(null);
  }

  public BuPopupMenu(String _title) {
    this(BuPreferences.BU.getBooleanProperty("popupmenu.stickable", false), _title);
  }

  public BuPopupMenu(boolean _stickable) {
    this(_stickable, null);
  }

  public BuPopupMenu(boolean _stickable, String _title) {
    super(_title);

  }

  protected String getString(String _s) {
    return BuResource.BU.getString(_s);
  }

  protected static final String __(String _s) {
    return BuResource.BU.getString(_s);
  }

  public boolean isSticky() {
    return sticky_;
  }

  public void setSticky(boolean _sticky) {
    sticky_ = _sticky;
  }

  @Override
  public void actionPerformed(ActionEvent _evt) {
    Component c = getInvoker();
    if (c instanceof ActionListener) ((ActionListener) c).actionPerformed(_evt);

    for (Enumeration e = actionListeners_.elements(); e.hasMoreElements();) {
      ActionListener l = (ActionListener) e.nextElement();
      if (l != c) l.actionPerformed(_evt);
    }
  }

  // Action

  private Vector actionListeners_ = new Vector();

  @Override
  public void addActionListener(ActionListener _l) {
    actionListeners_.addElement(_l);
  }

  @Override
  public void removeActionListener(ActionListener _l) {
    actionListeners_.removeElement(_l);
  }

  // Separator

  @Override
  public BuSeparator addSeparator(String _text) {
    BuSeparator r = new BuSeparator(_text);
    this.add(r);
    return r;
  }

  // SubMenu

  @Override
  public void addSubMenu(JMenu _m, boolean _enabled) {
    _m.setEnabled(_enabled);
    _m.addActionListener(this);
    this.add(_m);
  }

  // MenuItem

  @Override
  public BuMenuItem addMenuItem(String _s, String _cmd, boolean _enabled) {
    return addMenuItem(_s, _cmd, null, _enabled, 0);
  }

  @Override
  public BuMenuItem addMenuItem(String _s, String _cmd, Icon _icon, boolean _enabled) {
    return addMenuItem(_s, _cmd, _icon, _enabled, 0);
  }

  @Override
  public BuMenuItem addMenuItem(String _s, String _cmd, Icon _icon, boolean _enabled, int _key) {
    Icon icon = _icon;
    if (icon == null) icon = BuResource.BU.loadMenuCommandIcon(_cmd);
    if ((icon instanceof BuIcon) && ((BuIcon) icon).isDefault()) icon = null;
    if (icon == null) icon = BuResource.BU.getMenuIcon("aucun");

    BuMenuItem r = new BuMenuItem();
    r.setName("mi" + _cmd);
    r.setActionCommand(_cmd);
    r.setText(_s);
    if (icon instanceof BuIcon) r.setIcon((BuIcon) icon);
    else
      r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    if (_key != 0) {
      if ((_key >= KeyEvent.VK_F1) && (_key <= KeyEvent.VK_F12)) r.setAccelerator(KeyStroke.getKeyStroke(_key,
          InputEvent.ALT_MASK));
      else
        r.setAccelerator(KeyStroke.getKeyStroke(_key, InputEvent.CTRL_MASK));
    }
    this.add(r);
    return r;
  }

  // CheckBoxMenuItem

  @Override
  public BuCheckBoxMenuItem addCheckBox(String _s, String _cmd, boolean _enabled, boolean _checked) {
    return addCheckBox(_s, _cmd, null, _enabled, _checked);
  }

  @Override
  public BuCheckBoxMenuItem addCheckBox(String _s, String _cmd, Icon _icon, boolean _enabled, boolean _checked) {
    Icon icon = _icon;
    if (icon == null) icon = BuResource.BU.loadMenuCommandIcon(_cmd);
    if ((icon instanceof BuIcon) && ((BuIcon) icon).isDefault()) icon = null;
    if (icon == null) icon = BuResource.BU.getMenuIcon("aucun");

    BuCheckBoxMenuItem r = new BuCheckBoxMenuItem();
    r.setText(_s);
    r.setName("cbmi" + _cmd);
    r.setActionCommand(_cmd);
    if (icon instanceof BuIcon) r.setIcon((BuIcon) icon);
    else
      r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setSelected(_checked);
    this.add(r);
    return r;
  }

  // RadioButtonMenuItem

  @Override
  public BuRadioButtonMenuItem addRadioButton(String _s, String _cmd, boolean _enabled, boolean _checked) {
    return addRadioButton(_s, _cmd, null, _enabled, _checked);
  }

  private ButtonGroup group_ ;

  @Override
  public BuRadioButtonMenuItem addRadioButton(String _s, String _cmd, Icon _icon, boolean _enabled, boolean _checked) {
    if (group_ == null) group_ = new ButtonGroup();
    Icon icon = _icon;
    if (icon == null) icon = BuResource.BU.loadMenuCommandIcon(_cmd);
    if ((icon instanceof BuIcon) && ((BuIcon) icon).isDefault()) icon = null;
    if (icon == null) icon = BuResource.BU.getMenuIcon("aucun");

    BuRadioButtonMenuItem r = new BuRadioButtonMenuItem();
    r.setText(_s);
    r.setName("rbmi" + _cmd);
    r.setActionCommand(_cmd);
    if (icon instanceof BuIcon) r.setIcon((BuIcon) icon);
    else
      r.setIcon(icon);
    r.setHorizontalTextPosition(SwingConstants.RIGHT);
    r.addActionListener(this);
    r.setEnabled(_enabled);
    r.setSelected(_checked);
    group_.add(r);
    this.add(r);
    return r;
  }

  // Mnemonics (copied fromBuMenu)

  @Override
  public void computeMnemonics() {
    Component[] c = null;
    Hashtable t = null;

    try {
      c = getComponents();
    } /* Menu */
    catch (Exception ex) {} // swing 1.03

    /*
     * { int mn=getMnemonic(); if(mn>0) t.put(FuFactoryInteger.get(mn),this); }
     */
    if (c == null) return;
    for (int i = 0; i < c.length; i++) {
      if (c[i] instanceof JMenuItem) {
        JMenuItem mi = (JMenuItem) c[i];
        int mn = mi.getMnemonic();
        if (mn <= 0) {
          String tx = BuLib.candidateMnemonics(mi.getText());
          if (tx != null) for (int j = 0; j < tx.length(); j++) {
            mn = tx.charAt(j);
            if (t == null) t = new Hashtable();
            if (t.get(FuFactoryInteger.get(mn)) == null) {
              t.put(FuFactoryInteger.get(mn), mi);
              mi.setMnemonic(mn);
              break;
            }
          }
        }
        if (mi instanceof BuMenu) ((BuMenu) mi).computeMnemonics();
      }
    }
  }
}
