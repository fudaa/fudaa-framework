/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuBrowserFrame.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

//import com.memoire.dnd.*;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.Cursor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.JViewport;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.ImageView;
import javax.swing.text.html.StyleSheet;

public class BuBrowserPane
  extends BuScrollPane
  implements HyperlinkListener
{
  private static final int GAP=
    BuPreferences.BU.getIntegerProperty("layout.gap",5);

  BuBrowserFrame frame_;
  BuEditorPane   html_;
  private HTMLEditorKit  kit_;     
  ViewFactory    factory_;
  Cursor         cursor_;
  private Vector         avant_,apres_;

  public BuBrowserPane(BuBrowserFrame _frame)
  {
    frame_=_frame;

    factory_=new HTMLEditorKit.HTMLFactory()
      {
        @Override
        public View create(Element _e)
        {
          View r=null;

          /*GCJ-BEGIN*/
          Object o=_e.getAttributes().getAttribute
            (StyleConstants.NameAttribute);

          if(o instanceof HTML.Tag)
          {
            HTML.Tag tag=(HTML.Tag)o;

            if(tag==HTML.Tag.IMG)
              r=createViewIMG(_e);
          }
          /*GCJ-END*/

          if(r==null) r=super.create(_e);

          return r;
        }
      };

    kit_=new HTMLEditorKit()
      {
        @Override
        public ViewFactory getViewFactory()
        {
          //FuLog.debug("FACTORY");
          return factory_;
        }
      };

    avant_ =new Vector(5);
    apres_ =new Vector(5);

    html_=new BuEditorPane();
    html_.setEditorKit(kit_);
    html_.setDocument(kit_.createDefaultDocument());
    html_.setEditable(false);
    html_.addHyperlinkListener(this);

    if(GAP==0) html_.setBorder(BuBorders.EMPTY0000);

    JViewport v=getViewport();
    cursor_=v.getCursor();
    v.add(html_);
  }

  /*GCJ-BEGIN*/
  protected ImageView createViewIMG(Element _e)
  {
    ImageView r=new ImageView(_e);
    //FuLog.debug("BBP: swing="+SwingUtilities.isEventDispatchThread()+" "+r.getImageURL());
    r.setLoadsSynchronously(true);
    //FuLog.debug("BBP: "+BuLib.ensureImageIsLoaded(r.getImage(),null));
    return r;
  }
  /*GCJ-END*/

  public String getHtmlSource()
  {
    return html_.getText();
  }

  /*
  public JEditorPane getEditorPane()
  { return html_; }
  */

  public void copy()
  {
    html_.copy();
  }

  public void back()
  {
    int l=avant_.size();
    if(l>1)
    {
      URL url=(URL)avant_.elementAt(l-2);
      URL cur=(URL)avant_.elementAt(l-1);
      FuLog.trace("BWB: go back    "+url);
      avant_.removeElementAt(l-1);
      apres_.insertElementAt(cur,0);
      linkActivated(url);
    }
  } 

  public void forward()
  {
    if(apres_.size()>0)
    {
      //int l=avant_.size();
      URL url=(URL)apres_.elementAt(0);
      //URL cur=(URL)avant_.elementAt(l-1);
      FuLog.trace("BWB: go forward "+url);
      apres_.removeElementAt(0);
      linkActivated(url);
    }
  }

  public void reload()
  {
    int l=avant_.size();
    if(l>0)
    {
      URL url=(URL)avant_.elementAt(l-1);
      FuLog.trace("BWB: reload     "+url);
      avant_.removeElementAt(l-1);
      //setHtmlSource("Reload "+url,"Reload");
      linkActivated(url);
    }
  } 

  @Override
  public void hyperlinkUpdate(HyperlinkEvent _evt)
  {
    if(_evt.getEventType()==HyperlinkEvent.EventType.ACTIVATED)
    {
      html_.setSelectionStart(0);
      html_.setSelectionEnd(0);
      URL u=_evt.getURL();
      frame_.setMessage(u==null ? " ": u.toExternalForm());
      linkActivated(u);
    }
    else
    if(_evt.getEventType()==HyperlinkEvent.EventType.ENTERED)
    {
      URL u=_evt.getURL();
      frame_.setMessage(u==null ? " ": u.toExternalForm());
      /*
      Element e=_evt.getSourceElement();
      if(e.getAttributes().getAttribute(HTML.Tag.A)!=null)
      {
        View v=getView(e);
        if(v instanceof GlyphView)
        {
          GlyphView.GlyphPainter p=((GlyphView)v).getGlyphPainter();
          if(!(p instanceof Painter))
          {
            p=new Painter(p);
            ((GlyphView)v).setGlyphPainter(p);
          }
          ((Painter)p).setRollover(true);
          v.getContainer().repaint();
        }
      }
      */
    }
    else
    if(_evt.getEventType()==HyperlinkEvent.EventType.EXITED)
    {
      frame_.setMessage(" ");
      /*
      Element e=_evt.getSourceElement();
      View v=getView(e);
      if(v instanceof GlyphView)
      {
        GlyphView.GlyphPainter p=((GlyphView)v).getGlyphPainter();
        if(p instanceof Painter)
        {
          ((Painter)p).setRollover(false);
          v.getContainer().repaint();
        }
      }
      */
    }
  }

  /*
  private static class Painter extends GlyphView.GlyphPainter
  {
    private GlyphView.GlyphPainter delegate_;
    private boolean                rollover_;
    private GlyphView              view_;
    private Rectangle              shape_;

    public Painter(GlyphView.GlyphPainter _delegate)
    {
      delegate_=_delegate;
    }

    public GlyphView.GlyphPainter getDelegate()
    {
      return delegate_;
    }

    public void setRollover(boolean _rollover)
    {
      rollover_=_rollover;
    }

    public float getSpan(GlyphView v, int p0, int p1, TabExpander e, float x)
    {
      return delegate_.getSpan(v,p0,p1,e,x);
    }

    public float getHeight(GlyphView v)
    {
      return delegate_.getHeight(v);
    }

    public float getAscent(GlyphView v)
    {
      return delegate_.getAscent(v);
    }

    public float getDescent(GlyphView v)
    {
      return delegate_.getDescent(v);
    }

    public void paint(GlyphView v, Graphics g, Shape a, int p0, int p1)
    {
      Color old=g.getColor();
      if(rollover_) g.setColor(Color.blue);
      delegate_.paint(v,g,a,p0,p1);
      g.setColor(old);
    }

    public Shape modelToView(GlyphView v, 
                             int pos, Position.Bias bias,
                             Shape a) throws BadLocationException
    {
      return delegate_.modelToView(v,pos,bias,a);
    }

    public int viewToModel(GlyphView v, 
                           float x, float y, Shape a, 
                           Position.Bias[] biasReturn)
    {
      return delegate_.viewToModel(v,x,y,a,biasReturn);
    }

    public int getBoundedPosition(GlyphView v, int p0, float x, float len)
    {
      return delegate_.getBoundedPosition(v,p0,x,len);
    }

    public GlyphView.GlyphPainter getPainter(GlyphView v, int p0, int p1)
    {
      return this;
    }

    public int getNextVisualPositionFrom(GlyphView v, int pos, Position.Bias b, Shape a, 
                                         int direction,
                                         Position.Bias[] biasRet) 
      throws BadLocationException
    {
      return delegate_.getNextVisualPositionFrom(v,pos,b,a,direction,biasRet);
    }
  }

  private View getRootView()
  {
    return html_.getUI().getRootView(html_);
  }

  private View getView(Element _elem)
  {
    View rootView=getRootView();
    int  start   =_elem.getStartOffset();
    if(rootView!=null)
      return getView(rootView,_elem,start);
    else
      return null;
  }

  private View getView(View _parent,Element _elem,int _start)
  {
    if(_parent.getElement()==_elem) return _parent;
    
    int index=_parent.getViewIndex(_start,Position.Bias.Forward);
    
    if((index!=-1)&&(index<_parent.getViewCount()))
      return getView(_parent.getView(index),_elem,_start);
    else
      return null;
  }
  */

  //private Runnable current_=null;

  public void linkActivated(final URL _url)
  {
    frame_.setDocumentUrl(_url);
    //setUrlText(_url.toExternalForm(),true); //TMP?
    //setContent(_url,null,null);
  }

  protected boolean isImage(URL _url)
  {
    boolean r=false;

    String f=_url.getFile();
    if(f!=null)
    {
      f=f.toLowerCase();
      if(f.endsWith(".gif")||f.endsWith(".jpg")||f.endsWith(".jpeg"))
        r=true;
    }

    return r;
  }

  protected URL getUrlFor(URL _url)
  {
    URL r=_url;

    String p=_url.getProtocol();
         if(p.equals("mailto")) r=null;
    //else if(p.equals("https" )&&(FuLib.jdk()<1.5)) r=null;
    else if(p.equals("file"))
    {
      String f=_url.getFile();
      if((f==null)||f.endsWith("/"))
      {
        try { r=new URL(_url,"index.html"); }
        catch(MalformedURLException ex) { }
      }
    }

    return r;
  }

  protected String getSourceFor(URL _url)
  {
    String r=null;

    if(isImage(_url))
      r="<IMG SRC=\""+_url+"\">";

    return r;
  }

  protected String getTitleFor(URL _url)
  {
    String r=null;

    if(isImage(_url))
    {
      r=_url.toExternalForm();
      r=r.substring(r.lastIndexOf('/')+1);
    }

    return r;
  }

  protected InputStream getInputStreamFor(URL _url)
    throws IOException
  {
    return _url.openStream();
  }

  public final void setContent(final URL _url,final String _source,
                               final String _title)
  {
    //if(_url==null) return;

    /*
    if(TRACE)
    {
      FuLog.debug("URL    : "+_url);
      FuLog.debug("Source : "+(_source!=null));
      FuLog.debug("Title  : "+_title);
    }
    */

    /*
    String h=null;
    if(_url!=null)
    {
      h=_url.getHost();
      if((h!=null)&&"".equals(h.trim())) h=null;
    }
    if(h!=null) frame_.setMessage("Connexion: "+h+" ...");
    */

    Runnable runnable=new Runnable()
    {
      @Override
      public void run()
      {
        URL    u=_url;
        String s=_source;
        String t=_title;

        if(_url!=null)
        {
          u=getUrlFor(_url);
          if(u!=null)
          {
            if(s==null) s=getSourceFor(_url);
            if(t==null) t=getTitleFor (_url);
          }
        }

        if((u!=null)||(s!=null))
        {
          setContent0(u,s,t);
        }
      }
    };

    Thread thread=new Thread(runnable,"Loading "+_url);
    thread.setPriority(Thread.MIN_PRIORITY+1);
    thread.start();
  }

  protected void setContent0(final URL _url,String _source,String _title)
  {
    Runnable runnable=new Runnable()
    {
      @Override
      public void run()
      {
        JViewport v=getViewport();
        v.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        v.remove(html_);
        v.repaint();
      }
    };
    BuLib.invokeNow(runnable);

    //final EditorKit ek=html_.getEditorKitForContentType("text/html");
    final HTMLDocument hd=(HTMLDocument)kit_.createDefaultDocument();

    if(_source!=null)
    {
      try
      {
        kit_.read(new StringReader(_source),hd,0);
      }
      catch(Exception ex)
      {
        String source=FuLib.replace(_source,"&" ,"&amp;" );
        source=FuLib.replace(source,"\"","&quot;");
        source=FuLib.replace(source,"<" ,"&lt;"  );
        source=FuLib.replace(source,">" ,"&gt;"  );
        setError(_url,"The HTML is not valid.<BR><PRE>"+source+"</PRE>");
        return;
      }
    }
    else
    if(_url!=null)
    {
      try
      {
        hd.setBase(_url);
        hd.putProperty("IgnoreCharsetDirective",Boolean.TRUE);

        kit_.read
          (new InputStreamReader
           (getInputStreamFor(_url),"iso-8859-1"),hd,0);

        /*
        Reader in=new InputStreamReader
          (getInputStreamFor(_url),"iso-8859-1");
        StringWriter sw=new StringWriter();
        //kit_.write(sw,hd,0,hd.getLength());
        int c;
        while((c=in.read())!=-1)
          sw.write((char)c);

        String data=sw.toString();
        data=data.replace('\n',' ');
        data=data.replace('\r',' ');
        data=data.replace('\t',' ');
        data=data.replaceAll
          ("<link href=\".*\" rel=\"stylesheet\" type=\"text/css\">","");
        data=data.replaceAll("style=\".*\"","");
        data=data.replaceAll("class=\".*\"","");
        data=data.replaceAll("  *"," ");
        System.out.println(data);
        kit_.read(new StringReader(data),hd,0);
        */
      }
      catch(Exception ex)
      {
        setError(_url,"The document can not be accessed.<BR>"+
                 _url.toExternalForm());
        return;
      }
    }

    updateStyles(hd.getStyleSheet());

    /*
    HTMLDocument.Iterator ii=hd.getIterator(HTML.Tag.IMG);
    while(ii.isValid())
    {
      AttributeSet as=ii.getAttributes();
      FuLog.debug(ii.getTag()+" #"+ii.getStartOffset()+" "+as);
      if(as!=null)
      {
        Enumeration e=as.getAttributeNames();
        while(e.hasMoreElements())
          FuLog.debug(e.nextElement());
      }
      ii.next();
    }
    */

      //html_.setVisible(false);

    try
    {
      html_.setDocument(hd);
    }
    catch(Exception ex2)
    {
      setError(_url,"The document can not be displayed.<BR>"+
               (_url==null?"?":_url.toExternalForm())+"<BR>"+
               "Contact the Swing Team at Sun Microsystems.");
      return;
    }

    try
    {
      html_.getPreferredSize();
    }
    catch(Exception ex1)
    {
      StyleSheet ss=hd.getStyleSheet();

      StyleSheet[] as=ss.getStyleSheets();
      if(as!=null)
        for(int i=0;i<as.length;i++)
          ss.removeStyleSheet(as[i]);

      Enumeration rules=ss.getStyleNames();
      while (rules.hasMoreElements())
      {
        String name=(String)rules.nextElement();
        //Style  rule=ss.getStyle(name);
        ss.removeStyle(name);
      }

      try
      {
        html_.getPreferredSize();
      }
      catch(Exception ex2)
      {
        setError(_url,"CSS support in Swing is broken.<BR>"+
                 "The document can not be displayed.<BR>"+
                 (_url==null?"?":_url.toExternalForm()));
        return;
      }
    }

    final String tt=_title;
    runnable=new Runnable()
    {
      @Override
      public void run()
      {
        setContent1(_url,/*ek,*/hd,tt);
      }
    };
    BuLib.invokeLater(runnable);
  }

  protected void setContent1(URL _url,/*EditorKit _ek,*/HTMLDocument _hd,
                             String _title)
  {
    frame_.setUrlText(_url==null ? "" : _url.toExternalForm(),false);
    String title=_title;
    Object t=_hd.getProperty(Document.TitleProperty);
    if(t!=null) title=t.toString();

    frame_.setTitle  (title);
    //html_.setEditorKit(_ek);
    //html_.setDocument (_hd);
    //html_.setVisible (true);
    JViewport v=getViewport();
    v.add      (html_);
    v.setCursor(cursor_);

    if(_url!=null) avant_.addElement(_url);
    frame_.setBackEnabled   (avant_.size()>1);
    frame_.setForwardEnabled(apres_.size()>0);
    frame_.setMessage(" ");
  }

  public void setError(final URL _url,final String _text)
  {
    FuLog.debug("WBP: setError "+_url+" "+_text);

    Runnable runnable=new Runnable()
    {
      @Override
      public void run()
      {
        if(_url!=null) frame_.setUrlText(_url.toExternalForm(),false);

        frame_.setTitle (frame_.getString("Erreur"));
        //BuResource.BU.getString("Erreur"));
        html_.setCursor(cursor_);

        String t=_text;
        if(t==null)
        {
          t="This URL can not be displayed";
          t+=(_url!=null) ? ":<BR> "+_url.toExternalForm() : ".";
          t+="<BR>If it is correct, please use an external browser.";
        }

        setContent(_url,"<HTML><BODY>"+t+"</BODY></HTML>","Error");
        //TMP //html_.setText("<HTML><BODY>"+t+"</BODY></HTML>");
      }
    };

    BuLib.invokeLater(runnable);
  }

  private static final void updateStyles(StyleSheet _styles)
  {
    if(_styles!=null)
    {
      int FONT=12*BuPreferences.BU.getFontScaling()/100;

      Enumeration rules=_styles.getStyleNames();
      while(rules.hasMoreElements())
      {
	String name=(String)rules.nextElement();
	Style  rule=_styles.getStyle(name);
	
	if(StyleConstants.ALIGN_JUSTIFIED
	   ==StyleConstants.getAlignment(rule))
	  StyleConstants.setAlignment
	    (rule,StyleConstants.ALIGN_LEFT);

	if(StyleConstants.getFontSize(rule)<=FONT)
        {
          //FuLog.debug("WBP: font size="+StyleConstants.getFontSize(rule));
	  StyleConstants.setFontSize(rule,FONT);
        }
      }
    }
  }

  /*
  public final void setHtmlSource(String _source,String _titre)
  {
    setHtmlSource0(_source,_titre);
  }

  protected void setHtmlSource0(final String _source,final String _titre)
  {
    Runnable runnable=new Runnable()
    {
      public void run()
      {
        frame_.setTitle  (_titre);
        frame_.setUrlText("");

        try
        {
          EditorKit    ek=html_.getEditorKitForContentType("text/html");
          HTMLDocument dk=(HTMLDocument)ek.createDefaultDocument();
          ek.read(new StringReader(_source),dk,0);

          updateStyles(dk.getStyleSheet());

          //SimpleAttributeSet s=new SimpleAttributeSet();
	  //StyleConstants.setFontFamily(s,"SansSerif");
	  //dk.setCharacterAttributes(0,dk.getLength(),s,false);

          html_.setEditorKit(ek);
          html_.setDocument(dk);
        }
        catch(Exception ex)
	{
          html_.setText("Couldn't understand html source.\n\n"+_source);
          FuLog.error(ex);
        }
      }
    };

    if(!SwingUtilities.isEventDispatchThread())
      SwingUtilities.invokeLater(runnable);
    else
      runnable.run();
  }
  */

  /*
  class XPageLoader implements Runnable
  {
    private URL url_;
    
    XPageLoader(URL _url)
    {
      url_=_url;
    }

    public void run()
    {
      boolean encore=true;

      while((current_!=null)&&(current_!=this))
      {
        try { Thread.sleep(400); }
        catch(Exception ex) { }
      }
	  
      current_=this;
      html_.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

      while(encore)
      {
        if(url_==null)
	{
          encore=false;
        }
        else
	{
          Document doc=html_.getDocument();
          try 
	  {
            //html_.setPage(url_);
            boolean again=false;
            
            EditorKit    ek=html_.getEditorKitForContentType("text/html");
            HTMLDocument dk=(HTMLDocument)ek.createDefaultDocument();
            dk.setBase(url_);
            dk.putProperty("IgnoreCharsetDirective",Boolean.TRUE);
            
            ek.read(new InputStreamReader
                    (url_.openStream(),"iso-8859-1"),dk,0);
            
            updateStyles(dk.getStyleSheet());
            
            html_.setEditorKit(ek);
            html_.setDocument(dk);

            avant_.addElement(url_);
            frame_.setBackEnabled   (avant_.size()>1);
            frame_.setForwardEnabled(apres_.size()>0);
            
            encore=false;
          }
          catch (Exception ex)
	  {
            FuLog.trace("BWB: error "+ex);
            html_.setDocument(doc);
            frame_.setMessage("Bad URL: "+url_);
            frame_.beep();
            encore=true;
          }
          finally
	  {
            frame_.setMessage(" ");
            frame_.setUrlText(""+url_);
            url_=null;
          }
        }
        
        Container parent=html_.getParent();
        parent.repaint();
      }
      
      if(current_==this)
      {
        html_.setCursor(cursor_);
        current_=null;
      }
    }
  }
  */
}
