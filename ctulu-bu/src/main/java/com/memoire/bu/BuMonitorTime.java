/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuMonitorTime.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.Color;

/**
 * A monitor to watch time since the beginning.
 */

public class BuMonitorTime
       extends BuMonitorAbstract
{
  protected long begin_,actu_;

  public BuMonitorTime()
  {
    super();

    begin_=System.currentTimeMillis();
    actu_ =begin_;

    setName("buMONITOR_TIME");
    setForeground(new Color(96,64,32));
  }

  @Override
  public int getValue()
  { return 0; }

  @Override
  public int getMaximum()
  { return 100*(int)(((actu_-begin_)/1000)%60)/60; }

  @Override
  public String getText()
  { return FuLib.duration(actu_-begin_); }

  @Override
  public String getToolTipText()
  { return null; }

  @Override
  public long getDelay()
  { return 1000L; }

  @Override
  public boolean refreshState()
  {
    actu_=System.currentTimeMillis();
    return true;
  }
}
