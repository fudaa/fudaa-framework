/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuGenericPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuPreferences;
import java.awt.Dimension;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can view/edit preferences.
 * any
 */
public class BuGenericPreferencesPanel
  extends BuAbstractPreferencesPanel
{
  private String                    title_;
 // private BuCommonInterface         appli_;
  private FuPreferences             options_;
  private boolean                   editable_,growable_;
  private BuGenericPreferencesModel model_;

  @Override
  public String getTitle() { return title_; }

  public BuGenericPreferencesPanel
    (String _title, FuPreferences _options)
  {
    this(_title,_options,"",true,false);
  }

  public BuGenericPreferencesPanel
    (String _title, FuPreferences _options, String _mask,
     boolean _editable, boolean _growable)
  {
    super();
    //appli_   =_appli;
    title_   =_title;
    options_ =_options;
    editable_=_editable;
    growable_=_growable;
    model_   =new BuGenericPreferencesModel
	(options_,_mask,editable_,growable_);

    BuBorderLayout lo=new BuBorderLayout();
    lo.setVgap(5);
    lo.setHgap(5);

    BuPanel p=new BuPanel();
    p.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getTitle()),
	EMPTY5555));
    p.setLayout(lo);

    BuTable table=new BuTable(model_);
    //table.setFont(BuLib.deriveFont("Table",-2));
    p.add(new BuScrollPane(table),BuBorderLayout.CENTER);

    setLayout(new BuBorderLayout());
    setBorder(EMPTY5555);
    add(p,BuBorderLayout.CENTER);
    setPreferredSize(new Dimension(200,200));
  }

  @Override
  public boolean isPreferencesValidable()
  { return (editable_||growable_); }

  @Override
  public void validatePreferences()
  {
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesCancelable()
  { return (editable_||growable_); }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    model_.fireTableChanged();
  }
}
