/**
 * @modification $Date: 2006-09-19 14:35:04 $
 * @statut       unstable
 * @file         BuInformationsPerson.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuSort;
import com.memoire.fu.FuVectorFast;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Basic informations about a person.
 */
public final class BuInformationsPerson
  extends BuInformationsAbstract
{
  public String lastname;
  public String firstname;
  public String secondname;
  public String prefix;
  public String suffix;
  public String photo;
  public String phonework;
  public String faxwork;
  public String phonehome;
  public String phonecell;
  public String email;
  public String url;
  public String organization;
  public String department;
  public String logo;
  public String title;
  public String nickname;

  public String[] categories;

  /*private String  uid;
  private String  path;*/
  private boolean editable;

  public BuInformationsPerson()
  {
    lastname    ="";
    firstname   ="";
    secondname  ="";
    prefix      ="";
    suffix      ="";
    photo       ="";
    phonework   ="";
    faxwork     ="";
    phonehome   ="";
    phonecell   ="";
    email       ="";
    url         ="";
    organization="";
    department  ="";
    logo        ="";
    title       ="";
    nickname    ="";

    categories  =FuEmptyArrays.STRING0;

   /* uid     =null;
    path    =null;*/
    editable=true;
  }

  /*
  public String about()
  {
    String r=lastname;

    if(!organization.equals(""))
      r+="\n"+_("Soci�t�")+": "+organization;
    if(!author.equals(""))
      r+="\n"+_("Auteur")+": " +author;
    if(!contact.equals(""))
      r+="\n"+_("Contact")+": "+contact;
    if(!location.equals(""))
      r+="\n"+_("Lien")+": "+location;
    return r;
  }
  */

  public boolean isEditable()
  {
    return editable;
  }

  public void setEditable(boolean _editable)
  {
    if(!editable&&_editable)
    {
      editable=true;
     /* uid     =null;
      path    =null;*/
    }
    else
    if(editable&&!_editable)
    {
      editable=false;
    }
  }

  public String toString()
  {
    String r="Person[";
    r+=lastname+","+firstname+","+email;
    if(!organization.equals("")) r+=","+organization;
    if(!department  .equals("")) r+=","+department;
    r+="]";
    return r;
  }

  public static final BuInformationsPerson[] loadFromVCF(File _file)
    throws IOException
  {
    FuVectorFast v=new FuVectorFast(1,10);

    BufferedReader       rin    =null;
    BuInformationsPerson current=null;

    try
    {
     // final String PATH=_file.getAbsolutePath();
      
      rin=new BufferedReader
	(new InputStreamReader
         (new BufferedInputStream
          (new FileInputStream(_file)),"UTF-8"));
      while(true)
      {
	String s=rin.readLine();
	if(s==null) break;
	if("".equals(s.trim())) continue;

	if(s.startsWith("BEGIN:")&&s.toUpperCase().equals("BEGIN:VCARD"))
	{
	  current=new BuInformationsPerson();
	  current.editable=false;
	  continue;
	}

	if(s.startsWith("END:")&&s.toUpperCase().equals("END:VCARD"))
	{
	  if(current!=null)
	  {
	    //current.path=PATH;
	    //FuLog.debug("BIP: "+current);
	    v.addElement(current);
	    current=null;
	    continue;
	  }
	}

	if(current==null) continue;

	int      i=s.indexOf(":");
	String[] t=FuLib.split(s.substring(0,i),';');
	s=s.substring(i+1);
	if(FuLib.contains(t,"QUOTED-PRINTABLE"))
	  s=FuLib.decodeQuotedPrintable(s);

	if("N".equals(t[0]))
	{
	  String[] e=FuLib.split(s,';');
	  if(e.length>0) current.lastname  =e[0];
	  if(e.length>1) current.firstname =e[1];
	  if(e.length>2) current.secondname=e[2];
	  if(e.length>3) current.prefix    =e[3];
	  if(e.length>4) current.suffix    =e[4];
	}
        else
        if("PHOTO".equals(t[0]))
        {
	  try { current.photo=new URL(s).toString(); }
	  catch(MalformedURLException ex) { }
	}
	else
	if("TEL".equals(t[0]))
        {
	  if(FuLib.contains(t,"WORK"))
	  {
	    if(FuLib.contains(t,"FAX"))
	      current.faxwork=s;
	    else
	      current.phonework=s;
	  }
	  else
	  if(FuLib.contains(t,"CELL"))
	    current.phonecell=s;
	  else
	  if(FuLib.contains(t,"HOME")||
	     "".equals(current.phonehome))
	    current.phonehome=s;
	}
	else
	if("EMAIL".equals(t[0]))
        {
	  if("".equals(current.email)||
	     FuLib.contains(t,"INTERNET")||
	     FuLib.contains(t,"PREF"))
	    current.email=s;
	}
        else
        if("URL".equals(t[0]))
        {
	  current.url=s;
	}
        else
        if("ORG".equals(t[0]))
        {
	  String[] e=FuLib.split(s,';');
	  if(e.length>0) current.organization=e[0];
	  if(e.length>1) current.department  =e[1];
	}
        else
        if("LOGO".equals(t[0]))
        {
	  try { current.logo=new URL(s).toString(); }
	  catch(MalformedURLException ex) { }
	}
        else
        if("TITLE".equals(t[0]))
        {
	  current.title=s;
	}
        else
	if("NICKNAME".equals(t[0]))
        {
	  current.nickname=s;
	}
        else
	if("CATEGORIES".equals(t[0]))
        {
	  String[] e=FuLib.split(s,',');
	  for(int j=0;j<e.length;j++)
	    e[j]=e[j].trim();
	  FuSort.sort(e);
	  current.categories=e;
	}
        else
	if("X-BU-EDITABLE".equals(t[0]))
        {
	  current.editable="true".equals(s);
	}
      }
    }
    finally
    {
      FuLib.safeClose(rin);
      //try { if(rin!=null) rin.close(); }
      //catch(IOException ex) { }
    }

    int l=v.size();
    BuInformationsPerson[] r=new BuInformationsPerson[l];
    v.copyInto(r);
    return r;
  }

  private static final String clean(String _s)
  {
    return FuLib.replace(_s.trim(),"\"","").trim();
  }

  public static final BuInformationsPerson[] loadFromLDIF(File _file)
    throws IOException
  {
    FuVectorFast v=new FuVectorFast(1,10);

    BufferedReader       rin    =null;
    BuInformationsPerson current=null;

    try
    {
      //final String PATH=_file.getAbsolutePath();
      
      rin=new BufferedReader
	(new InputStreamReader
         (new BufferedInputStream
          (new FileInputStream(_file)),"UTF-8"));
      while(true)
      {
	String s=rin.readLine();
	if(s==null) break;

	if("".equals(s.trim()))
	{
	  if(current!=null)
	  {
	    //current.path=PATH;

	    int i=current.firstname.indexOf(',');
	    if(i>=0)
	    {
	      String tmp=current.lastname;
	      current.lastname =current.firstname.substring(0,i);
	      current.firstname=current.firstname.substring(i+1)+tmp;
	    }
	    current.lastname =clean(current.lastname);
	    current.firstname=clean(current.firstname);

	    i=current.firstname.indexOf(' ');
	    if(i>=0)
	    {
	      String tmp=current.firstname.substring(i).trim();
	      current.firstname=current.firstname.substring(0,i);
	      boolean nkn=(tmp.startsWith("'")&&tmp.endsWith("'"));
	      if(nkn) tmp=tmp.substring(1,tmp.length()-1);
	      if(nkn&&"".equals(current.nickname))
		current.nickname=tmp;
	      else
		current.secondname=tmp;
	    }

	    //FuLog.debug("BIP: "+current);
	    v.addElement(current);
	  }
	  current=null;
	  continue;
	}

	int i=s.indexOf(":");
	if(i<0) continue;

	if(current==null)
	{
	  current=new BuInformationsPerson();
	  current.editable=false;
	}

	String t=s.substring(0,i);
	s=s.substring(i+1).trim();

	if("sn".equals(t))
	  current.lastname=s;
	else
	if("givenName".equals(t))
	  current.firstname=s;
	else
	if("mail".equals(t))
	  current.email=s;
	else
	if("telephoneNumber".equals(t))
	  current.phonework=s;
        else
	if("facsimileTelephoneNumber".equals(t))
	  current.faxwork=s;
        else
	if("homePhone".equals(t))
	  current.phonehome=s;
        else
	if("mobile".equals(t))
	  current.phonecell=s;
        else
        if("o".equals(t))
	  current.organization=s;
        else
        if("ou".equals(t))
	  current.department=s;
        else
        if("workurl".equals(t))
	{
	  if(current.url=="") current.url=s;
	}
        else
        if("homeurl".equals(t))
	  current.url=s;
        else
        if("title".equals(t))
	  current.title=s;
        else
	if("mozilla_AimScreenName".equals(t))
	  current.nickname=s;
        else
	if("xBuEditable".equals(t))
	  current.editable="true".equals(s);
      }
    }
    finally
    {
      FuLib.safeClose(rin);
      //try { if(rin!=null) rin.close(); }
      //catch(IOException ex) { }
    }

    int l=v.size();
    BuInformationsPerson[] r=new BuInformationsPerson[l];
    v.copyInto(r);
    return r;
  }

  private static final void appendspace(StringBuffer _sb,String _s)
  {
    if(!"".equals(_s))
    {
      append(_sb,_s);
      _sb.append(' ');
    }
  }

  private static final void spaceappend(StringBuffer _sb,String _s)
  {
    if(!"".equals(_s))
    {
      _sb.append(' ');
      append(_sb,_s);
    }
  }

  private static final void append(StringBuffer _sb,String _s)
  {
    String s=FuLib.replace(_s,"\n"," ");
    s=FuLib.replace(s,"\r","");
    s=FuLib.replace(s,"\t"," ");
    s=FuLib.replace(s,";" ," ");
    _sb.append(s);
    //_sb.append(FuLib.encodeQuotedPrintable(_s));
  }

  public static final void saveToVCF(File _file,
                                     BuInformationsPerson[] _persons)
    throws IOException
  {
    BufferedWriter wout=null;

    try
    {
      wout=new BufferedWriter
	(new OutputStreamWriter
         (new BufferedOutputStream
          (new FileOutputStream(_file)),"UTF-8"));

      for(int i=0;i<_persons.length;i++)
      {
	BuInformationsPerson current=_persons[i];
	StringBuffer sb=new StringBuffer(1024);

	sb.append("BEGIN:VCARD\n");
	sb.append("VERSION:2.1\n");

	sb.append("FN:");
	appendspace(sb,current.prefix);
	appendspace(sb,current.firstname);
	appendspace(sb,current.secondname);
	append     (sb,current.lastname);
	spaceappend(sb,current.suffix);
	sb.append('\n');

	sb.append("N:");
	append(sb,current.lastname);
	sb.append(';');
	append(sb,current.firstname);
	sb.append(';');
	append(sb,current.secondname);
	sb.append(';');
	append(sb,current.prefix);
	sb.append(';');
	append(sb,current.suffix);
	sb.append('\n');

	if(!"".equals(current.photo))
	{
	  sb.append("PHOTO;VALUE=URL:");
	  append(sb,current.photo);
	  sb.append('\n');
	}

	if(!"".equals(current.phonework))
	{
	  sb.append("TEL;WORK;VOICE:");
	  append(sb,current.phonework);
	  sb.append('\n');
	}

	if(!"".equals(current.faxwork))
	{
	  sb.append("TEL;WORK;FAX:");
	  append(sb,current.faxwork);
	  sb.append('\n');
	}

	if(!"".equals(current.phonehome))
	{
	  sb.append("TEL;HOME;VOICE:");
	  append(sb,current.phonehome);
	  sb.append('\n');
	}

	if(!"".equals(current.phonecell))
	{
	  sb.append("TEL;CELL:");
	  append(sb,current.phonecell);
	  sb.append('\n');
	}

	sb.append("EMAIL;INTERNET:");
	append(sb,current.email);
	sb.append('\n');

	if(!"".equals(current.url))
	{
	  sb.append("URL:");
	  append(sb,current.url);
	  sb.append('\n');
	}

        sb.append("ORG:");
	append(sb,current.organization);
	sb.append(';');
	append(sb,current.department);
	sb.append('\n');

	if(!"".equals(current.logo))
	{
	  sb.append("LOGO;VALUE=URL:");
	  append(sb,current.logo);
	  sb.append('\n');
	}

        sb.append("TITLE:");
	append(sb,current.title);
	sb.append('\n');

        sb.append("NICKNAME:");
	append(sb,current.nickname);
	sb.append('\n');

	String[] e=current.categories;
	if(e.length>0)
	{
	  sb.append("CATEGORIES:");
	  for(int j=0;j<e.length;j++)
	  {
	    if(j>0) sb.append(',');
	    append(sb,e[j]);
	  }
	  sb.append('\n');
	}

        sb.append("X-BU-EDITABLE:");
	append(sb,current.editable ? "true" : "false");
	sb.append('\n');

	sb.append("END:VCARD\n");
	sb.append('\n');

	wout.write(sb.toString());
      }
    }
    finally
    {
      FuLib.safeClose(wout);
      //try { if(wout!=null) wout.close(); }
      //catch(IOException ex) { }
    }
  }

  private static final void appendxml(StringBuffer _sb,String _t,String _s)
  {
    if(!"".equals(_s))
    {
      _sb.append("    <");
      _sb.append(_t);
      _sb.append('>');
      append(_sb,_s);
      _sb.append("</");

      String t=_t;
      int i=t.indexOf(" ");
      if(i>=0) t=t.substring(0,i);
      _sb.append(t);
      _sb.append(">\n");
    }
  }

  public static final void saveToXML(File _file,
                                     BuInformationsPerson[] _persons)
    throws IOException
  {
    BufferedWriter wout=null;

    try
    {
      wout=new BufferedWriter
	(new OutputStreamWriter
         (new BufferedOutputStream
          (new FileOutputStream(_file)),"UTF-8"));

      wout.write("<?xml version=\"1.0\"?>\n");
      wout.write("<vcards version=\"2.1\">\n");

      for(int i=0;i<_persons.length;i++)
      {
	BuInformationsPerson current=_persons[i];
	StringBuffer sb=new StringBuffer(1024);

	sb.append("  <vcard>\n");

	sb.append("    <fn>");
	appendspace(sb,current.prefix);
	appendspace(sb,current.firstname);
	appendspace(sb,current.secondname);
	append     (sb,current.lastname);
	spaceappend(sb,current.suffix);
	sb.append("</fn>\n");

	appendxml(sb,"lastname"  ,current.lastname);
	appendxml(sb,"firstname" ,current.firstname);
	appendxml(sb,"secondname",current.secondname);
	appendxml(sb,"prefix"    ,current.prefix);
	appendxml(sb,"suffix"    ,current.suffix);

	if(!"".equals(current.photo))
	  appendxml(sb,"photo type=\"url\"",current.photo);

	if(!"".equals(current.phonework))
	  appendxml(sb,"tel type=\"work\" media=\"voice\"",current.phonework);

	if(!"".equals(current.faxwork))
	  appendxml(sb,"tel type=\"work\" media=\"fax\"",current.faxwork);

	if(!"".equals(current.phonehome))
	  appendxml(sb,"tel type=\"home\" media=\"voice\"",current.phonehome);

	if(!"".equals(current.phonecell))
	  appendxml(sb,"tel type=\"cell\" media=\"voice\"",current.phonecell);

	appendxml(sb,"email type=\"internet\"",current.email);

	if(!"".equals(current.url))
          appendxml(sb,"url",current.url);

        appendxml(sb,"organization",current.organization);
	appendxml(sb,"department"  ,current.department);

	if(!"".equals(current.logo))
          appendxml(sb,"logo type=\"url\"",current.logo);

        appendxml(sb,"title",current.title);
        appendxml(sb,"nickname",current.nickname);

	String[] e=current.categories;
        for(int j=0;j<e.length;j++)
          appendxml(sb,"category",e[j]);

        appendxml(sb,"x-bu-editable",current.editable ? "true" : "false");

	sb.append("  </vcard>\n");
	wout.write(sb.toString());
      }

      wout.write("</vcards>\n");
    }
    finally
    {
      FuLib.safeClose(wout);
      //try { if(wout!=null) wout.close(); }
      //catch(IOException ex) { }
    }
  }

  /*
  public static void main(String[] _args)
    throws IOException
  {
    BuInformationsPerson[] p=loadFromLDIF
      (new File("/home/desnoix/mesadresses.ldif"));
  }
  */
}
