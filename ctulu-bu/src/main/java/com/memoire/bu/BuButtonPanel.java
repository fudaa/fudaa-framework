/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuButtonPanel.java
 * @version      0.41
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;

/**
 * A panel filled with standard buttons.
 */
public class BuButtonPanel
  extends BuPanel
{
  public static final int OUI       =    1;
  public static final int NON       =    2;
  public static final int CONTINUER =    4;
  public static final int VALIDER   =    8;
  public static final int ANNULER   =   16;
  public static final int APPLIQUER =   32;
  public static final int PRECEDENT =   64;
  public static final int SUIVANT   =  128;
  public static final int ABANDONNER=  256;
  public static final int REESSAYER =  512;
  public static final int TERMINER  = 1024;
  public static final int FERMER    = 2048;
  public static final int AIDE      = 4096;
  public static final int DETRUIRE  = 8192;

  public static final int AUCUN     =    0;
  public static final int TOUS      =16383;

  protected BuPanel    panel_;
  protected JSeparator separator_;

  private   ActionListener al_;
  private   int            buttons_;
  private   int            disabled_;

  public BuButtonPanel()
  {
    this(true);
  }

  public BuButtonPanel(boolean _separator)
  {
    setLayout(new BorderLayout(5,5));

    if(_separator)
    {
      setBorder(new EmptyBorder(0,5,5,5));
      add(new JSeparator(), BorderLayout.NORTH);
    }
    else
    {
      setBorder(BuBorders.EMPTY5555);
    }

    panel_=new BuPanel();
    panel_.setLayout(new BuButtonLayout());
    add(panel_,BorderLayout.CENTER);
  }

  public BuButtonPanel(int _buttons, ActionListener _al)
  {
    this(false);
    setButtons(_buttons,_al);
  }

  public BuButtonPanel(int _buttons, ActionListener _al, boolean _separator)
  {
    this(_separator);
    setButtons(_buttons,_al);
  }

  /*
  protected BuButton createButton(String _s, ActionListener _al)
  {
    return createButton(_s,_al,true);
  }
  */

  protected BuButton createButton(String _s, ActionListener _al,
                                  int _id)
  {
    String   c=_s.replace('�','e').toUpperCase();
    BuButton r= new BuButton(BuResource.BU.getString(_s));
    r.setActionCommand(c);
    r.putClientProperty("BU_BUTTON_ID",new Integer(_id));

    if("PRECEDENT" .equals(c)) c="RECULER";
    if("SUIVANT"   .equals(c)) c="AVANCER";
    if("ABANDONNER".equals(c)) c="ANNULER";
    r.setIcon(BuResource.BU.loadButtonCommandIcon(c));
    r.addActionListener(_al);
    return r;
  }

  public void setButtons(int _buttons, ActionListener _al)
  {
    setButtons(_buttons,_al,AUCUN);
  }

  public void setButtons(int _buttons, ActionListener _al, int _disabled)
  {
    if((buttons_!=_buttons)||(al_!=_al))
    {
      panel_.removeAll();
      disabled_=AUCUN;

      if((_buttons&DETRUIRE)!=0)
        panel_.add(createButton("D�truire"  ,_al,DETRUIRE));
      if((_buttons&AIDE)!=0)
        panel_.add(createButton("Aide"      ,_al,AIDE));

      if(panel_.getComponentCount()>0)
        ((BuButtonLayout)panel_.getLayout()).setSeparator(1);

      if((_buttons&APPLIQUER)!=0)
        panel_.add(createButton("Appliquer" ,_al,APPLIQUER));
      if((_buttons&OUI)!=0)
        panel_.add(createButton("Oui"       ,_al,OUI));
      if((_buttons&NON)!=0)
        panel_.add(createButton("Non"       ,_al,NON));
      if((_buttons&VALIDER)!=0)
        panel_.add(createButton("Valider"   ,_al,VALIDER));
      if((_buttons&ANNULER)!=0)
        panel_.add(createButton("Annuler"   ,_al,ANNULER));
      if((_buttons&CONTINUER)!=0)
        panel_.add(createButton("Continuer" ,_al,CONTINUER));
      if((_buttons&ABANDONNER)!=0)
        panel_.add(createButton("Abandonner",_al,ABANDONNER));
      if((_buttons&PRECEDENT)!=0)
        panel_.add(createButton("Pr�c�dent" ,_al,PRECEDENT));
      if((_buttons&SUIVANT)!=0)
        panel_.add(createButton("Suivant"   ,_al,SUIVANT));
      if((_buttons&TERMINER)!=0)
        panel_.add(createButton("Terminer"  ,_al,TERMINER));
      if((_buttons&FERMER)!=0)
        panel_.add(createButton("Fermer"    ,_al,FERMER));

      buttons_=_buttons;
      al_     =_al;
    }

    setDisabledButtons(_disabled);
  }

  public BuButton getDefaultButton()
  {
    int n=panel_.getComponentCount()-1;
    while(n>=0)
    {
      BuButton r=(BuButton)panel_.getComponent(n);
      if(r.isEnabled())
      {
        int d=((Integer)r.getClientProperty("BU_BUTTON_ID")).intValue();
        if((d&(OUI|CONTINUER|VALIDER|APPLIQUER|SUIVANT|REESSAYER|TERMINER|FERMER))!=0)
          return r;
      }
      n--;
    }
    return null;
  }

  public void setDisabledButtons(int _disabled)
  {
    if(disabled_!=_disabled)
    {
      int n=panel_.getComponentCount()-1;
      for(int i=n;i>=0;i--)
      {
        BuButton r=(BuButton)panel_.getComponent(i);
        int      d=((Integer)r.getClientProperty("BU_BUTTON_ID")).intValue();
        boolean  e=((d&_disabled)==0);
        r.setEnabled(e);
      }
      disabled_=_disabled;
    }
  }

  /*
  public void paint(Graphics _g)
  {
    super.paint(_g);
    Color bg = UIManager.getColor("Panel.background");
    int w = getWidth() - 1;
    //int h = getHeight() - 1;
    _g.setColor(bg.darker());
    _g.drawLine(0, 0, w, 0);
    _g.setColor(bg.brighter());
    _g.drawLine(0, 1, w, 1);
  }
  */
}
