/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuFontDialog.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A simple font chooser.
 */

public class BuFontDialog
  extends BuDialog
  implements ActionListener
{
  protected BuFontChooser chooser_;
  protected BuButton      btValider_;
  protected BuButton      btAnnuler_;

  public BuFontDialog(BuCommonInterface      _parent,
                      BuInformationsSoftware _isoft,
                      String                 _title,
                      Font                   _value)
  {
    this(_parent,_isoft,_title,_value,false,0,false);
  }

  public BuFontDialog(BuCommonInterface      _parent,
                      BuInformationsSoftware _isoft,
                      String                 _title,
                      Font                   _value,
                      boolean                _styled,
                      int                    _style,
                      boolean                _native)
  {
    super(_parent,_isoft,_title);

    chooser_.setStyled(_styled);
    chooser_.setNative(_native);

    setValue(_value);
    setStyle(_style);

    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btValider_=new BuButton(BuResource.BU.loadButtonCommandIcon("VALIDER"),
                            getString("Valider"));
    btValider_.addActionListener(this);
    getRootPane().setDefaultButton(btValider_);
    pnb.add(btValider_);

    btAnnuler_=new BuButton(BuResource.BU.loadButtonCommandIcon("ANNULER"),
                            getString("Annuler"));
    btAnnuler_.addActionListener(this);
    pnb.add(btAnnuler_);

    content_.add(pnb,BuBorderLayout.SOUTH);

    //setResizable(true);
  }

  @Override
  public boolean needsScrollPane()
  {
    return false;
  }

  @Override
  public JComponent getComponent()
  {
    return chooser_=new BuFontChooser();
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    //System.err.println("BFC: font is "+valeur_);

    if(source==btValider_)
    {
      reponse_=JOptionPane.OK_OPTION;
      //valeur_ =tfValeur_.getText();
      setVisible(false);
    }
    else
    if(source==btAnnuler_)
    {
      reponse_=JOptionPane.CANCEL_OPTION;
      setValue  (null);
      setVisible(false);
    }
  }

  public Font getValue()
  {
    return chooser_.getValue();
  }

  public void setValue(Font _value)
  {
    chooser_.setValue(_value);
  }

  public int getStyle()
  {
    return chooser_.getStyle();
  }

  public void setStyle(int _style)
  {
    chooser_.setStyle(_style);
  }

  public static void main(String[] _args)
  {
    BuFontDialog d;

    Font f=new Font("Flubber",Font.PLAIN,18);

    /*
    System.err.println("Font: "+f);
    d=new BuFontDialog(null,null,"Test BuFontDialog",null);
    d.activate();
    d=new BuFontDialog(null,null,"Test BuFontDialog",null,true,0,false);
    d.activate();
    */
    d=new BuFontDialog(null,null,"Test BuFontDialog",f,true,0,true);
    d.activate();

    System.err.println("Selection: "+d.getValue()+" with style "+d.getStyle());
    System.exit(0);
  }
}
