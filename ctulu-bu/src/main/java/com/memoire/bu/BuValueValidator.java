/**
 * @modification $Date: 2006-11-15 13:49:11 $
 * @statut       unstable
 * @file         BuValueValidator.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.util.Date;

/**
 * An abstract root class to specify a validator for input values. Provides standard ones for ints, doubles, dates, ...
 */

public abstract class BuValueValidator {
  public abstract boolean isValueValid(Object _value);

  // Instances

  public static final BuValueValidator INTEGER = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof Integer);
    }
  };

  public static class EmptyValueValidator extends BuValueValidator {
    final BuValueValidator other_;

    public EmptyValueValidator(final BuValueValidator _other) {
      super();
      other_ = _other;
    }

    @Override
    public boolean isValueValid(Object _value) {
      return _value == null || other_.isValueValid(_value);
    }
    
    

  }

  public static final BuValueValidator LONG = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof Long);
    }
  };

  public static final BuValueValidator FLOAT = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof Float);
    }
  };

  public static final BuValueValidator DOUBLE = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof Double);
    }
  };

  public static final BuValueValidator HOST = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof String);
    }
  };

  public static final BuValueValidator ID = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof String);
    }
  };

  public static final BuValueValidator FILE = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof String);
    }
  };

  public static final BuValueValidator DATE = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof Date);
    }
  };

  public static final BuValueValidator TIME = new BuValueValidator() {
    @Override
    public boolean isValueValid(Object _value) {
      return (_value instanceof Long);
    }
  };

  public static final BuValueValidator MIN(final double _min) {
    return new BuValueValidator() {
      @Override
      public boolean isValueValid(Object _value) {
        return (_value instanceof Number) && (((Number) _value).doubleValue() >= _min);
      }
    };
  }

  public static final BuValueValidator MAX(final double _max) {
    return new BuValueValidator() {
      @Override
      public boolean isValueValid(Object _value) {
        return (_value instanceof Number) && (((Number) _value).doubleValue() <= _max);
      }
    };
  }

  public static final BuValueValidator MINMAX(final double _min, final double _max) {
    return new BuValueValidator() {
      @Override
      public boolean isValueValid(Object _value) {
        return (_value instanceof Number) && (((Number) _value).doubleValue() >= _min)
            && (((Number) _value).doubleValue() <= _max);
      }
    };
  }

  public static final BuValueValidator MULTI(final BuValueValidator _vv1, final BuValueValidator _vv2) {
    return new BuValueValidator() {
      @Override
      public boolean isValueValid(Object _value) {
        return _vv1.isValueValid(_value) && _vv2.isValueValid(_value);
      }
    };
  }
}
