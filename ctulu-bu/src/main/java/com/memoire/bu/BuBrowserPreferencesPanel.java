/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuBrowserPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose his favorite browser.
 *
 * browser.type
 * browser.exec
 */
public class BuBrowserPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener, BuBorders
{
  BuCommonInterface appli_;
  BuPreferences     options_;

  BuRadioButton     cbInterne_;
  BuRadioButton     cbExterne_;
  BuRadioButton     cbPerso_;
  BuComboBox        chBrowser_;
  BuTextField       tfPerso_;

  @Override
  public String getTitle()
  {
    return getS("Navigateur");
  }

  @Override
  public String getCategory()
  {
    return getS("Composants");
  }

  // Constructeur

  public BuBrowserPreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_=_appli;

    options_=BuPreferences.BU;

    BuGridLayout lo=new BuGridLayout(2,5,5,false,false,true,false);
    lo.setColumnXAlign(new float[] { 0.f,0.f });
    //lo.setColumns(2);
    //lo.setVgap(5);
    //lo.setHgap(5);

    BuPanel p=new BuPanel(lo);
    //p.setLayout(lo);
    p.setBorder(new CompoundBorder
                (new BuTitledBorder(getS("Navigateur")),EMPTY5555));

    ButtonGroup bg=new ButtonGroup();

    cbInterne_=new BuRadioButton(getS("interne")  );
    cbExterne_=new BuRadioButton(getS("externe")  );
    cbPerso_  =new BuRadioButton(getS("personnel"));
    cbInterne_.setName("cbBROWSING_INTERNAL" );
    cbExterne_.setName("cbBROWSING_EXTERNAL" );
    cbPerso_  .setName("cbBROWSING_PERSONNAL");
    bg.add(cbInterne_);
    bg.add(cbExterne_);
    bg.add(cbPerso_);

    p.add(cbInterne_);
    cbInterne_.addActionListener(this);
    BuComboBox chInterne=new BuComboBox();
    chInterne.addItem("   ");
    chInterne.setVisible(false);
    p.add(chInterne);

    p.add(cbExterne_);
    cbExterne_.addActionListener(this);

    chBrowser_=new BuComboBox();
    chBrowser_.setName("chBrowser_");

    for(int i=0;i<BuBrowserControl.LIST.length;i++)
    {
      // System.err.println(BuBrowserControl.LIST[i][0]);
      chBrowser_.addItem(BuBrowserControl.LIST[i][0]);
    }

    p.add(chBrowser_);
    chBrowser_.setVisible(FuLib.isUnix());
    chBrowser_.addActionListener(this);

    p.add(cbPerso_);
    cbPerso_.addActionListener(this);

    tfPerso_=new BuTextField(25);
    tfPerso_.setName("tfBROWSER");
    p.add(tfPerso_);
    tfPerso_.addKeyListener(this);
    tfPerso_.addActionListener(this);

    //setLayout(new BuBorderLayout());
    setLayout(new BuVerticalLayout(5,true,false));
    setBorder(EMPTY5555);
    add(p);//,BuBorderLayout.CENTER);

    cbInterne_.setSelected(true);
    updateComponents();
  }

  // Evenements
  
  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    //JComponent source=(JComponent)_evt.getSource();
    //System.err.println("SOURCE="+source.getName());
    //System.err.println("ACTION="+_evt.getActionCommand());

    setDirty(true);

    chBrowser_.setEnabled(cbExterne_.isSelected());
    tfPerso_  .setEnabled(cbPerso_  .isSelected());
  }
  
  // Methodes publiques

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  @Override
  public boolean isPreferencesApplyable()
    { return true; }
  
  @Override
  public void applyPreferences()
  {
    fillTable();
  }

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees
  
  private void fillTable()
  {
    int    type=1;
    String exec="";

    if(cbInterne_.isSelected())
    {
      type=1;
      exec=options_.getStringProperty("browser.exec");
    }
    if(cbExterne_.isSelected())
    {
      type=2;
      exec=(String)chBrowser_.getSelectedItem();
    }
    if(cbPerso_.isSelected())
    {
      type=3;
      exec=tfPerso_.getText();
    }

    options_.putIntegerProperty("browser.type",type);
    options_.putStringProperty ("browser.exec",exec);
    setDirty(false);
  }

  private void updateComponents()
  {
    int    type=options_.getIntegerProperty("browser.type");
    String exec=options_.getStringProperty("browser.exec");

    switch(type)
    {
      case 2:
        cbExterne_.setSelected(true);
        chBrowser_.setSelectedItem(exec);
        break;
      case 3:
	cbPerso_.setSelected(true);
	tfPerso_.setText(exec);
	break;
      default:
	cbInterne_.setSelected(true);
	break;
    }

    chBrowser_.setEnabled(type==2);
    tfPerso_  .setEnabled(type==3);
    setDirty(false);
  }
}
