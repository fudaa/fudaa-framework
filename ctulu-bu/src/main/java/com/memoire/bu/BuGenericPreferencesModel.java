/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuGenericPreferencesModel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuPreferences;
import com.memoire.fu.FuSort;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * A table model for the user preferences.
 * any
 */
public class BuGenericPreferencesModel
  implements TableModel, FuEmptyArrays
{
  private Vector        listeners_;
  private FuPreferences options_;
  private String[]      keys_;
  private int           size_;
  private String        mask_;
  private boolean       editable_,growable_;

  public BuGenericPreferencesModel(FuPreferences _options)
  {
    this(_options,"",true,false);
  }

  public BuGenericPreferencesModel
    (FuPreferences _options, String _mask,
     boolean _editable, boolean _growable)
  {
    listeners_=new Vector();
    options_  =_options;
    keys_     =STRING0;
    size_     =0;
    editable_ =_editable;
    growable_ =_growable;

    setMask(_mask);
  }

  public String getMask()
  {
    return mask_;
  }

  public void setMask(String _mask)
  {
    mask_=(_mask==null?"":_mask);
    build();
  }

  protected void build()
  {
    synchronized(options_)
    {
      keys_=new String[options_.size()];

      Enumeration e=options_.keys();
      int         i=0;
      while(e.hasMoreElements())
      {
	String s=(String)e.nextElement();
	if(s.startsWith(mask_))
	{
	  keys_[i]=s;
	  i++;
	}
      }
      size_=i;
    }
    FuSort.sort(keys_,0,size_);
    fireTableChanged();
  }

  @Override
  public Class getColumnClass(int column)
  {
    Class r=null;
    switch(column)
    {
    case  0: r=String.class;  break; // name
    case  1: r=String.class;  break; // value
    default: r=Object.class;  break;
    }
    return r;
  }

  @Override
  public int getColumnCount()
  { return 2; }

  @Override
  public String getColumnName(int column)
  {
    String r="";

    switch(column)
    {
    case  0: r=BuResource.BU.getString("Clef");    break;
    case  1: r=BuResource.BU.getString("Valeur"); break;
    default: r="undefined"; break;
    }
    
    return r;
  }

  @Override
  public int getRowCount()
  {
    return size_+(growable_ ? 1 : 0);
  }

  @Override
  public Object getValueAt(int row, int column)
  {
    Object r=null;

    switch(column)
    {
    case 0:  r=((row==size_) ? ""
		: keys_[row]); break;
    case 1:  r=((row==size_) ? ""
		: options_.getStringProperty(keys_[row])); break;
    default: r="default"; break;
    }

    if(!(r instanceof String)) r=""+r;
    return r;
  }

  @Override
  public boolean isCellEditable(int row, int column)
  {
    return   (editable_&&(column==1))
	   ||(growable_&&(row==size_)&&(column==0));
  }

  @Override
  public void setValueAt(Object _value, int _row, int _column)
  {
    Object value=_value;
    if(!(value instanceof String)) value=""+value;

    if(_column==0)
    {
      String k=(String)value;
      k=k.toLowerCase().trim();
      if(!"".equals(k)&&k.startsWith(mask_))
      {
	String v=options_.getStringProperty(k,"");
	options_.putStringProperty(k,v);
        build();
      }
    }
    else
    if(_column==1)
    {
      options_.putStringProperty(keys_[_row],(String)value);
      fireRowChanged(_row);
    }
  }

  public void fireRowChanged(int row)
  {
    TableModelEvent e=new TableModelEvent(this,row);
    Enumeration     l=listeners_.elements();
    while(l.hasMoreElements())
      ((TableModelListener)l.nextElement()).tableChanged(e);
  }

  public void fireTableChanged()
  {
    TableModelEvent e=new TableModelEvent(this);
    Enumeration     l=listeners_.elements();
    while(l.hasMoreElements())
      ((TableModelListener)l.nextElement()).tableChanged(e);
  }

  @Override
  public void addTableModelListener(TableModelListener _l)
  { listeners_.addElement(_l); }

  @Override
  public void removeTableModelListener(TableModelListener _l)
  { listeners_.removeElement(_l); }
}
