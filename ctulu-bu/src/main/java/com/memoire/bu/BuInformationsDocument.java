/**
 * @modification $Date: 2006-10-12 07:28:13 $
 * @statut       unstable
 * @file         BuInformationsDocument.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Basic informations for a document.
 */

public class BuInformationsDocument
  extends BuInformationsAbstract
{
  public String name;
  public String organization;
  public String department;
  public String version;
  public String date;
  public String author;
  public String contact;
  public transient BuIcon logo;
  public String location;
  public String mimetype;
  public String comment;
  public Object data;
  public BuInformationsDocument(){
    this(BuResource.BU.getIcon("texte",24));
  }

  public BuInformationsDocument(BuIcon _logo)
  {
    name        ="Document";
    version     ="";
    date        =FuLib.date();
    organization=BuPreferences.BU.getStringProperty("user.organization");
    department  =BuPreferences.BU.getStringProperty("user.department");
    author      =author();
    contact     =email();
    logo        =_logo;
    location    ="";
    mimetype    ="";
    comment     ="";
    data        =null;
  }

  private String author()
  {
    String r=BuPreferences.BU.getStringProperty("user.lastname",null);

    if(r!=null)
    {
      String n=BuPreferences.BU.getStringProperty("user.firstname",null);
      if(n!=null) r+=" "+n;
    }
    else
    {
      String u=FuLib.getSystemProperty("user.name");
      if(u==null) r="";
      else        r=u.toUpperCase();
    }

    return r;
  }

  private String email()
  {
    String r=BuPreferences.BU.getStringProperty("user.email",null);

    if(r==null)
    {
      try { r=InetAddress.getLocalHost().getHostName(); }
      catch(UnknownHostException ex) { r="localhost"; }

      String u=FuLib.getSystemProperty("user.name");
      if(u==null) u="root";
      r=u+"@"+r;
    }

    return r;
  }

  public String about()
  {
    String r=name+" version "+version+" ["+date+"]";

    if(!organization.equals(""))
    {
      r+="\n"+ getString("Soci�t�")+": "+organization;
      if(!department.equals(""))
	r+=" / "+department;
    }
    if(!author.equals(""))
      r+="\n"+ getString("Auteur")+": " +author;
    if(!contact.equals(""))
      r+="\n"+ getString("Contact")+": "+contact;
    if(!location.equals(""))
      r+="\n"+ getString("Lien")+": "+location;

    return r;
  }

  public String toString()
  {
    String r="Document[";
    r+=name+","+version+","+date;
    if(!organization.equals("")) r+=","+organization;
    if(!department  .equals("")) r+=","+department;
    if(!author      .equals("")) r+=","+author;
    if(!contact     .equals("")) r+=","+contact;
    if(!location    .equals("")) r+=","+location;
    if(!mimetype    .equals("")) r+=","+mimetype;
    r+="]";
    return r;
  }
}
