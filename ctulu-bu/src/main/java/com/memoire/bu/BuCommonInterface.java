/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuCommonInterface.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;

/**
 * A common interface for Applications, Applets and SubApps.
 * To access uniformly to services and avoid redundant coding.
 */

public interface BuCommonInterface
       extends   ActionListener, BuCutCopyPasteInterface,
                 BuUndoRedoInterface, BuSelectFindReplaceInterface
{
  BuCommonInterface      getApp();
  BuCommonImplementation getImplementation();

  BuMainPanel   getMainPanel();
  void          setMainPanel(BuMainPanel _p);
  BuMenuBar     getMainMenuBar();
  void          setMainMenuBar(BuMenuBar _mb);
  BuToolBar     getMainToolBar();
  void          setMainToolBar(BuToolBar _tb);
  BuSpecificBar getSpecificBar();
  void          setSpecificBar(BuSpecificBar _sb);

  /*
  BuSplashScreen getSplashScreen();
  void           setSplashScreen(BuSplashScreen _ss);
  */

  void start();
  void init();
  @Override
  void actionPerformed(ActionEvent _evt);
  void doLayout();
  void validate();

  void setTitle(String _s);
  void setLookAndFeel(String _lnf);
  void setJMenuBar(JMenuBar _mb);
  void setContentPane(Container _c);

  JRootPane getRootPane();
  Frame     getFrame();

  void showOrHide(String _a, JComponent _c);
  void removeAction(String _cmd);
  void removeDummySeparators();
  void setEnabledForAction(String _cmd, boolean _enabled);
  void setCheckedForAction(String _cmd, boolean _checked);

  boolean confirmExit();
  void displayURL(String _url);

  void about    ();
  @Override
  void cut      ();
  @Override
  void copy     ();
  @Override
  void duplicate();
  void exit     ();
  @Override
  void find     ();
  void license  ();
  @Override
  void paste    ();
  void print    ();
  @Override
  void redo     ();
  @Override
  void replace  ();
  @Override
  void select   ();
  @Override
  void undo     ();

  BuInformationsSoftware getInformationsSoftware();
}
