/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuSplitPane.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import javax.swing.JSplitPane;

/**
 * Nothing more than a JSplitPane.
 */

public class BuSplitPane
       extends JSplitPane
{
  public BuSplitPane()
  {
    super();
  }

  public BuSplitPane(int _orientation)
  {
    super(_orientation);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }
}

