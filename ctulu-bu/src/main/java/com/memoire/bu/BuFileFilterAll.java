/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuFileFilterAll.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.io.File;

/**
 * A simple implementation of a non-filtering FileFilter.
 */
public class BuFileFilterAll
  extends BuFileFilter
{
  public BuFileFilterAll()
  {
    super("*",__("Tous"));
  }

  @Override
  public boolean accept(String _f)
  {
    return (_f!=null);
  }

  @Override
  public boolean accept(File _f)
  {
    return (_f!=null);
  }

  @Override
  public boolean accept(File _d, String _fn)
  {
    return (_fn!=null);
  }

  @Override
  public String getDescription()
  {
    if(fullDescription_==null)
    {
      if((description_==null)||isExtensionListInDescription())
      {
	if(description_!=null)
	  fullDescription_=description_;
	else
	  fullDescription_= getString("Fichiers");

	fullDescription_+=" (*.*)";
      }
      else
      {
	fullDescription_=description_;
      }
    } 
    return fullDescription_;
  }
}
