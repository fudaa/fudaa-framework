/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuLanguagePreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuResource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.ButtonGroup;
import javax.swing.border.CompoundBorder;

/**
 * A panel where the user can choose his language
 * among 9 (fr,de,en,es,eo,it,hu,pt,ru).
 *
 * locale.language
 */
public class BuLanguagePreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener
{
  private static final int NBL=12;

  private static final String[] CODES=new String[]
  {
    "bg","de","en","es","eo",
    "fr","it","hi","hu","nl",
    "pt","ru"
  };

  private static final String[] INTITULES=new String[]
  {
    "bulgarian","deutsch","english","espa\u00F1ol","esperanto",
    "français","italiano","*hindi*","magyarul","dutch",
    "portuguese",
    "\u0440\u0443\u0441\u0441\u043A\u0438\u0439" // russian
  };

  private static final String[] NOMS=new String[]
  {
    "BULGARIAN","GERMAN","ENGLISH","SPANISH","ESPERANTO",
    "FRENCH","ITALIAN","HINDI","HUNGARIAN","DUTCH",
    "PORTUGUESE","RUSSIAN"
  };


  BuCommonInterface appli_;
  BuPreferences     options_;
  String            dispo_;

  BuRadioButton[]    cbLangues_;
  BuCheckBox         cbFallBack_;

  @Override
  public String getTitle()
  {
    return getS("Langue");
  }

  @Override
  public String getCategory()
  {
    return getS("Accessibilité");
  }

  // Constructeur

  public BuLanguagePreferencesPanel(BuCommonInterface _appli)
  {
    super();
    appli_=_appli;
    dispo_=appli_.getInformationsSoftware().languages;
    init();
  }

  public BuLanguagePreferencesPanel(String _dispo)
  {
    super();
    appli_=null;
    dispo_=_dispo;
    init();
  }

  private void init()
  {
    options_=BuPreferences.BU;

    //GridLayout lo=new GridLayout(0,2,5,5);
    BuGridLayout lo=new BuGridLayout(2,5,5,true,true,false,false);
    lo.setSameWidth (true);
    lo.setSameHeight(true);

    BuPanel p=new BuPanel(lo);
    p.setBorder
      (new CompoundBorder
       (new BuTitledBorder(getTitle()),
	EMPTY5555));

    ButtonGroup bg=new ButtonGroup();

    cbLangues_=new BuRadioButton[NBL];
    for(int i=0;i<NBL;i++)
    {
      cbLangues_[i]=new BuRadioButton(INTITULES[i]+" ["+CODES[i]+"]");
      cbLangues_[i].setName("cbLOCALE_LANGUAGE_"+NOMS[i]);
      cbLangues_[i].setActionCommand(NOMS[i]);
      cbLangues_[i].addActionListener(this);
      bg.add(cbLangues_[i]);
      p.add(cbLangues_[i]);
    }

    cbFallBack_=new BuCheckBox("Fall back on english",false);
    cbFallBack_.addActionListener(this);

    /*BuLabelInfo m=new BuLabelInfo
	("You're welcome to submit new translations.\n"+
	 "It is quite easy and takes a couple of hour.\n"+
	 "Also updates are needed.\n"+
	 "Please email me at guillaume@desnoix.com.");*/
    //m.setBorder(EMPTY5555);

    setLayout(new BuVerticalLayout(5,true,false));
    setBorder(EMPTY5555);
    add(p);
    add(cbFallBack_);
    //add(m);

    String l=Locale.getDefault().getLanguage();
    for(int i=0;i<NBL;i++)
    {
      if(dispo_.indexOf(CODES[i])<0)
	cbLangues_[i].setEnabled(false);
      if(l.startsWith(CODES[i]))
	cbLangues_[i].setSelected(true);
    }
    updateComponents();
  }

  // Methodes publiques

  public String getLocaleString()
  {
    String r="";

    for(int i=0;i<NBL;i++)
      if(cbLangues_[i].isSelected())
	r=CODES[i];

    return r;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    setDirty(true);
  }

  @Override
  public boolean isPreferencesValidable()
    { return true; }

  @Override
  public void validatePreferences()
  {
    fillTable();
    options_.writeIniFile();
  }

  /*
  public boolean isPreferencesApplyable()
    { return true; }

  public void applyPreferences()
  {
    fillTable();
    options_.applyOn(appli_);
  }
  */

  @Override
  public boolean isPreferencesCancelable()
    { return true; }

  @Override
  public void cancelPreferences()
  {
    options_.readIniFile();
    updateComponents();
  }

  // Methodes privees

  private void fillTable()
  {
    for(int i=0;i<NBL;i++)
      if(cbLangues_[i].isSelected())
        options_.putStringProperty("locale.language",CODES[i]);

    options_.putBooleanProperty
      ("translation.fallBackOnEnglish",cbFallBack_.isSelected());
    setDirty(false);
  }

  private void updateComponents()
  {
    String l=options_.getStringProperty("locale.language");
    for(int i=0;i<NBL;i++)
      if(l.startsWith(CODES[i]))
	cbLangues_[i].setSelected(true);

    cbFallBack_.setSelected
      (options_.getBooleanProperty("translation.fallBackOnEnglish",
                                   FuResource.isDefaultToEnglish()));
    setDirty(false);
  }
}
