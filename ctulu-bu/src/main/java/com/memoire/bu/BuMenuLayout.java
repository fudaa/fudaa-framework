/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuMenuLayout.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuFactoryInteger;
import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.io.Serializable;
import java.util.Hashtable;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * A layout for 3-column menus.
 */
public class BuMenuLayout
  implements LayoutManager2, Serializable
{
  private Hashtable table_;

  public static final String NORTH ="North";
  public static final String SOUTH ="South";
  public static final String EAST  ="East";
  public static final String WEST  ="West";
  public static final String CENTER="Center";

  // private static final long serialVersionUID = -8658291919501921765L;

  public BuMenuLayout()
  {
    this(1,1);
  }

  public BuMenuLayout(int _hgap, int _vgap)
  {
    hgap_=_hgap;
    vgap_=_vgap;
    table_=new Hashtable(5);
  }

  private int hgap_;
  public int getHgap() { return hgap_; }
  public void setHgap(int _hgap) { hgap_=_hgap; }

  private int vgap_;
  public int getVgap() { return vgap_; }
  public void setVgap(int _vgap) { vgap_=_vgap; }

  @Override
  public void addLayoutComponent(Component _comp, Object _constraints)
  {
    synchronized(_comp.getTreeLock())
    {
      if((_constraints==null)||(_constraints instanceof String))
	addLayoutComponent2((String)_constraints,_comp);
      else
	throw new IllegalArgumentException
	  ("Can not add to layout: constraint must be a string (or null)");
    }
  }

  @Override
  public void addLayoutComponent(String _name, Component _comp)
  {
    String name=_name;
    if(name==null) name=CENTER;

    StringBuffer sb=new StringBuffer("@deprecated add(String,Component):");
    sb.append(" use add(Component,BuMenuLayout.");
    sb.append(name.toUpperCase()+" instead at ");
    String s=FuLog.getStackTrace();
    while(s.indexOf("BuMenuLayout")>0)
      s=s.substring(s.indexOf('\n')+1);
    while(s.indexOf("Container")>0)
      s=s.substring(s.indexOf('\n')+1);
    s=s.substring(s.indexOf('(')+1,s.indexOf(')'));
    sb.append(s);
    FuLog.error(sb.toString());

    addLayoutComponent2(name,_comp);
  }

  private void addLayoutComponent2(String _name, Component _comp)
  {
    String name=_name;
    if(name==null) name=CENTER;

    synchronized(_comp.getTreeLock())
    {
      if(CENTER.equals(name)||NORTH.equals(name)||
	 SOUTH.equals(name)||EAST.equals(name)||WEST.equals(name))
      {
	// table_.put(name,comp);
	table_.put(_comp,name);
      }
      else
	throw new IllegalArgumentException
	  ("Can not add to layout: unknown constraint: " + name);
    }
  }

  @Override
  public void removeLayoutComponent(Component _comp)
  {
    synchronized(_comp.getTreeLock())
    {
      // Object name=table_.get(_comp);
      // if(name!=null)
      // {
	table_.remove(_comp);
      //   table_.remove(name);
      // }
    }
  }

/*  private Dimension getMinimumSize(Component _c)
  {
    Dimension r=_c.getMinimumSize();
    if(r==null) r=new Dimension(0,0);
    return r;
  }*/

  private Dimension getPreferredSize(Component _c)
  {
    // So badly implemented in BasicMenuItemUI
    if(_c instanceof JMenuItem)
    {
      Container parent=_c.getParent();
      if(  (parent!= null)&&(parent instanceof JComponent)
	 &&!((_c instanceof JMenu)&&((JMenu)_c).isTopLevelMenu()))
      {
	JComponent p=(JComponent)parent;
	p.putClientProperty("maxTextWidth",FuFactoryInteger.get(0));
	p.putClientProperty("maxAccWidth" ,FuFactoryInteger.get(0));
      }
    }

    Dimension r=_c.getPreferredSize();
    if(r==null) r=new Dimension(0,0);
    return r;
  }

  private void adjustSize(Dimension _r, Dimension _d, String _s)
  {
    if(CENTER.equals(_s))
    {
      _r.width=Math.max(_r.width,_d.width);
      if(_r.height!=0) _r.height+=vgap_;
      _r.height+=_d.height;
    }
    else
    if(EAST.equals(_s))
    {
      _r.width=Math.max(_r.width,_d.width);
      if(_r.height!=0) _r.height+=vgap_;
      _r.height+=_d.height;
    }
    else
    if(WEST.equals(_s))
    {
      _r.width=Math.max(_r.width,_d.width);
      if(_r.height!=0) _r.height+=vgap_;
      _r.height+=_d.height;
    }
    else
    if(NORTH.equals(_s))
    {
      _r.width=Math.max(_d.width,_r.width);
      if(_r.height!=0) _r.height+=vgap_;
      _r.height+=_d.height;
    }
    else
    if(SOUTH.equals(_s))
    {
      _r.width  =Math.max(_d.width,_r.width);
      if(_r.height!=0) _r.height+=vgap_;
      _r.height+=_d.height;
    }
  }

  @Override
  public Dimension minimumLayoutSize(Container _parent)
  {
    return new Dimension(0,0);
  }
    
  @Override
  public Dimension preferredLayoutSize(Container _parent)
  {
    synchronized(_parent.getTreeLock())
    {
      Dimension dn=new Dimension(0,0);
      Dimension ds=new Dimension(0,0);
      Dimension dw=new Dimension(0,0);
      Dimension de=new Dimension(0,0);
      Dimension dc=new Dimension(0,0);

      int nbc=_parent.getComponentCount();
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(s==null) s=CENTER;
	  if(NORTH.equals(s))
	    adjustSize(dn,getPreferredSize(c),s);
	  else
	  if(SOUTH.equals(s))
	    adjustSize(ds,getPreferredSize(c),s);
	  else
	  if(CENTER.equals(s))
	    adjustSize(dc,getPreferredSize(c),s);
	  else
	  if(EAST.equals(s))
	    adjustSize(de,getPreferredSize(c),s);
	  else
	  if(WEST.equals(s))
	    adjustSize(dw,getPreferredSize(c),s);
	}
      }

      Dimension r=new Dimension(dc.width,dc.height);
      if(dw.width>0)
      {
	r.width +=dw.width+hgap_;
	r.height =Math.max(r.height,dw.height);
      }
      if(de.width>0)
      {
	r.width +=de.width+hgap_;
	r.height =Math.max(r.height,de.height);
      }
      if(dn.height>0)
      {
	r.width  =Math.max(r.width,dn.width);
	r.height+=dn.height+hgap_;
      }
      if(ds.height>0)
      {
	r.width  =Math.max(r.width,ds.width);
	r.height+=ds.height+hgap_;
      }

      Insets insets=_parent.getInsets();
      r.width +=insets.left+insets.right;
      r.height+=insets.top+insets.bottom;
      
      return r;
    }
  }

  @Override
  public Dimension maximumLayoutSize(Container _parent)
  {
    return new Dimension(Short.MAX_VALUE,Short.MAX_VALUE);
  }

  @Override
  public float getLayoutAlignmentX(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public float getLayoutAlignmentY(Container _parent)
  {
    return 0.5f;
  }

  @Override
  public void invalidateLayout(Container _parent)
  {
  }

  private int orderColumn(Container _parent,int _nbc,String _pos,int _x,int _y)
  {
    int w=0;
    int y=_y;
    for(int i=0;i<_nbc;i++)
    {
      Component c=_parent.getComponent(i);
      String    s=(String)table_.get(c);

      if(c.isVisible())
	if(_pos.equals(s))
	{
	  Dimension d=getPreferredSize(c);
	  c.setBounds(_x,y,d.width,d.height);
	  w=Math.max(w,d.width);
	  y+=d.height+vgap_;
	}
    }
    for(int i=0;i<_nbc;i++)
    {
      Component c=_parent.getComponent(i);
      String    s=(String)table_.get(c);

      if(c.isVisible())
	if(_pos.equals(s))
	{
	  Dimension d=c.getSize();
	  d.width=w;
	  c.setSize(d);
	}
    }
    return w;
  }

  @Override
  public void layoutContainer(Container _parent)
  {
    synchronized(_parent.getTreeLock())
    {
      Insets insets=_parent.getInsets();

      int top   =insets.top;
      int bottom=_parent.getSize().height-insets.bottom;
      int left  =insets.left;
      int right =_parent.getSize().width-insets.right;
      int nbc   =_parent.getComponentCount();

      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	if(c.isVisible())
	{
	  String    s=(String)table_.get(c);
	  Dimension d=getPreferredSize(c);

	  if(NORTH.equals(s))
	  {
	    c.setSize(right-left,d.height);
	    d=getPreferredSize(c);
	    c.setBounds(left,top,right-left,d.height);
	    top+=d.height+vgap_;
	  }
	  else
	  if(SOUTH.equals(s))
	  {
	    c.setSize(right-left,d.height);
	    d=getPreferredSize(c);
	    c.setBounds(left,bottom-d.height,right-left,d.height);
	    bottom-=d.height+vgap_;
	  }
	}
      }

      int w/*,y*/;

      w=orderColumn(_parent,nbc,WEST,left,top);
      /*
      y=top;
      w=0;
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(WEST.equals(s))
	  {
	    Dimension d=getPreferredSize(c);
	    c.setBounds(left,y,d.width,d.height);
	    w=Math.max(w,d.width);
	    y+=d.height+vgap_;
	  }
	}
      }
      */
      if(w>0) left+=w+hgap_;

      w=orderColumn(_parent,nbc,CENTER,left,top);
      /*
      y=top;
      w=0;
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(CENTER.equals(s))
	  {
	    Dimension d=getPreferredSize(c);
	    c.setBounds(left,y,d.width,d.height);
	    w=Math.max(w,d.width);
	    y+=d.height+vgap_;
	  }
	}
      }
      */
      if(w>0) left+=w+hgap_;

      w=orderColumn(_parent,nbc,EAST,left,top);
      /*
      y=top;
      w=0;
      for(int i=0;i<nbc;i++)
      {
	Component c=_parent.getComponent(i);
	String    s=(String)table_.get(c);

	if(c.isVisible())
	{
	  if(EAST.equals(s))
	  {
	    Dimension d=getPreferredSize(c);
	    c.setBounds(left,y,d.width,d.height);
	    w=Math.max(w,d.width);
	    y+=d.height+vgap_;
	  }
	}
      }
      */
      left+=w;
    }
  }

  public String toString()
  {
    return getClass().getName()+"[hgap="+hgap_+",vgap="+vgap_+"]";
  }
}
