/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuDynamicMenu.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public abstract class BuDynamicMenu
  extends BuMenu
{
  public BuDynamicMenu(String _s, String _cmd)
  {
    super(_s,_cmd);
  }
  public BuDynamicMenu(String _s, String _cmd,Icon _ic)
  {
    super(_s,_cmd,_ic);
  }

  protected abstract void    build();
  protected abstract boolean isActive();

  @Override
  public final void setSelected(boolean _state)
  {
    if(_state&&!isSelected()) build();
    super.setSelected(_state);
  }

  @Override
  public final boolean isEnabled()
  {
    return super.isEnabled();
  }

  @Override
  public final void setEnabled(boolean _enabled)
  {
    super.setEnabled(_enabled);
  }

  @Override
  public final void paintComponent(Graphics _g)
  {
    boolean a=isActive();
    boolean r=isEnabled();
    if(r!=a) setEnabled(a);
    super.paintComponent(_g);
  }

  @Override
  public final void removeAll()
  {
    Component[] c=getPopupMenu().getComponents();
    for(int i=0;i<c.length;i++)
    {
      if((c[i] instanceof JMenuItem)||
         (c[i] instanceof JSeparator))
        remove(c[i]);
    }
  }
}
