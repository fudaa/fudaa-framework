/**
 * @modification $Date: 2007-03-09 08:37:38 $
 * @statut unstable
 * @file BuDialog.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.StringReader;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;

/**
 * An abstract class for dialogs. Better to use than JOptionPane.
 */
public abstract class BuDialog
        extends JDialog
        implements ActionListener, BuBorders {

  private static final int GAP =
          BuPreferences.BU.getIntegerProperty("layout.gap", 5);
  protected Frame frame_;
  protected JComponent content_;
  protected JComponent lbMessage_;
  protected BuScrollPane scrollpane_;
  protected BuBorderLayout layout_;
  protected int reponse_;
  protected Object message_;

  public BuDialog(BuCommonInterface _parent,
          BuInformationsSoftware _isoft,
          String _titre) {
    this(_parent, _isoft, _titre, null);
  }

  public BuDialog(BuCommonInterface _parent,
          BuInformationsSoftware _isoft,
          String _titre,
          Object _message) {
    super(_parent == null ? BuLib.HELPER : _parent.getFrame());

    if (_parent != null) {
      frame_ = _parent.getFrame();
    }
    reponse_ = JOptionPane.CANCEL_OPTION;

    BuBorderLayout mlo = new BuBorderLayout();

    JComponent mct = (JComponent) getContentPane();
    mct.setLayout(mlo);
    BuInformationsSoftware isoft = _isoft;
    /*if((_parent!=null)&&(isoft==null))
     isoft=_parent.getInformationsSoftware();*/

    if (isoft != null) {
      BuPanel mpi = new BuPanel();
      mpi.setLayout(new BuVerticalLayout(GAP));
      mpi.setBorder(BuLib.getEmptyBorder(GAP)); //10,10,10,10));
      Color bg = BuLib.getColor(mpi.getBackground().darker());
      Color fg = BuLib.getColor(Color.white);
      if (bg.equals(fg)) {
        fg = BuLib.getColor(Color.black);
      }
      mpi.setBackground(bg);
      mpi.setForeground(fg);

      BuPicture ml_logo = null;
      if (isoft.logo != null) {
        ml_logo = new BuPicture(isoft.logo);
        // setIconImage(BuResource.BU.getImage("dialog"));
      }

      BuLabelMultiLine ml_isoft = new BuLabelMultiLine(isoft.name + "\nversion: " + isoft.version + "\ndate: " + isoft.date);
      ml_isoft.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
      ml_isoft.setForeground(fg);//Color.white);

      if (ml_logo != null) {
        mpi.add(ml_logo);
      }
      mpi.add(ml_isoft);
      mct.add(mpi, BuBorderLayout.WEST);
    }

    layout_ = new BuBorderLayout();
    layout_.setHgap(GAP);
    layout_.setVgap(GAP);
    content_ = new BuPanel();
    content_.setBorder(BuLib.getEmptyBorder(GAP));//BuBorders.EMPTY5555);
    content_.setLayout(layout_);
    mct.add(content_, BuBorderLayout.CENTER);

    setMessage(_message);
    setResizable(false);
    setTitle(_titre);
    setModal(true);

    addWindowListener(new WAD());
  }

  public void setMessage(Object _message) {
    if ((lbMessage_ != null) && (lbMessage_.getParent() == content_)) {
      content_.remove(lbMessage_);
    }
    if ((scrollpane_ != null) && (scrollpane_.getParent() == content_)) {
      content_.remove(scrollpane_);
    }
    Object message = _message;
    if (message == null) {
      message = getComponent();
    }
    if (message == null) {
      message = "Please overload getComponent() in BuDialog.";
    }

    message_ = message;

    if (message instanceof JComponent) {
      lbMessage_ = (JComponent) message;
    } else {
      String text = message.toString();
      if (text.startsWith("<HTML>") || text.startsWith("<html>")) {
        BuEditorPane html = new BuEditorPane();
        /*GCJ-BEGIN*/
        EditorKit ek = html.getEditorKitForContentType("text/html");
        Document dk = ek.createDefaultDocument();

        try {
          ek.read(new StringReader(text), dk, 0);
        } catch (Exception ex) {
          FuLog.warning(ex);
        }

        html.setOpaque(false);
        html.setBorder(EMPTY0000);
        html.setEditable(false);
        html.setEditorKit(ek);
        html.setDocument(dk);
        /*GCJ-END*/
        lbMessage_ = html;
      } else {
        lbMessage_ = new BuLabelMultiLine(text);
        ((BuLabelMultiLine) lbMessage_).setPreferredWidth(440);
        ((BuLabelMultiLine) lbMessage_).setWrapMode(BuLabelMultiLine.WORD);
        lbMessage_.setFont(BuLib.deriveFont("Label", Font.PLAIN, 0));
        lbMessage_.setBorder(EMPTY5555);
      }
    }

    if (needsScrollPane()) {
      scrollpane_ = new BuScrollPane(lbMessage_);
      int h = lbMessage_.getFont().getSize();
      scrollpane_.getHorizontalScrollBar().setUnitIncrement(150);
      scrollpane_.getVerticalScrollBar().setUnitIncrement(h + 1);

      if ((lbMessage_ instanceof BuLabelMultiLine)
              && (((BuLabelMultiLine) lbMessage_).getWrapMode()
              != BuLabelMultiLine.NONE)) {
        scrollpane_.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollpane_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollpane_.setPreferredSize(scrollpane_.getPreferredSize());
        scrollpane_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      }

      content_.add(scrollpane_, BuBorderLayout.CENTER);
    } else {
      content_.add(lbMessage_, BuBorderLayout.CENTER);
    }

    if (lbMessage_.isFocusable()) {
      lbMessage_.requestFocus();
    } else {
      transferFocus();
    }

    //if(content_.isShowing()) content_.revalidate();
  }

  class WAD extends WindowAdapter {

    @Override
    public void windowActivated(WindowEvent _evt) {
      //System.err.println("Dialog.Activated");
      JButton db = getRootPane().getDefaultButton();
      //System.err.println("Dialog.defaultButton="+db);
      //System.err.println("Dialog.focusOwner="+getFocusOwner());
      if ((getFocusOwner() == null) && (db != null)) {
        db.requestFocus();
      }
    }
  }

  // Resource
  protected String getString(String _s) {
    return BuResource.BU.getString(_s);
  }

  protected static final String __(String _s) {
    return BuResource.BU.getString(_s);
  }

  // Anti-aliasing
  @Override
  public final void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  public abstract JComponent getComponent();

  @Override
  public abstract void actionPerformed(ActionEvent _evt);

  public boolean needsScrollPane() {
    return true;
  }

  public final int activate() {
    reponse_ = JOptionPane.CANCEL_OPTION;

    JComponent mct = (JComponent) getContentPane();

    Dimension d = mct.getPreferredSize();
    Dimension ecran = getToolkit().getScreenSize();

    //d.width+=30; d.height+=30;

    if (!(message_ instanceof JComponent)) {
      if (d.width < 450) {
        d.width = 450;
      }
      if (d.width > 550) {
        d.width = 550;
      }
      if (d.height < 150) {
        d.height = 150;
      }
      if (d.height > 250) {
        d.height = 250;
      }
    }

    if (d.width > ecran.width * 3 / 4) {
      d.width = ecran.width * 3 / 4;
    }
    if (d.height > ecran.height * 3 / 4) {
      d.height = ecran.height * 3 / 4;
    }

    boolean resizable = isResizable();
    setResizable(true);
    //setSize(d);
    mct.setPreferredSize(d);
    pack();
    //setSize(getPreferredSize());

//    Point p = getLocation();
//    if ((p.x == 0) && (p.y == 0)) {
//      setLocation((ecran.width - d.width) / 2, (ecran.height - d.height) / 2);
//    }
    setLocationRelativeTo(frame_);

    setResizable(resizable);

    // SWING 1.03: LightweightDispatcher.retargetMouseEvent
    // try { setVisible(true); }
    // catch(NullPointerException ex) { }
    show();

    // release native resources
    if (isModal()) {
      dispose();
    }

    return reponse_;
  }

  @Override
  public void show() {
    BuLib.computeMnemonics(getRootPane(), this);
    super.show();
  }
}
