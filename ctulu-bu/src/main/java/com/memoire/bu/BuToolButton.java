/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut unstable
 * @file BuToolButton.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

/**
 * Like BuButton but for toolbars.
 */

public class BuToolButton extends JButton {

  public BuToolButton() {
    this((BuIcon) null, null);
  }

  public BuToolButton(BuIcon _icon) {
    this(_icon, null);
  }

  public BuToolButton(String _label) {
    this(null, _label);
  }

  /*
   * public BuToolButton(String _label, BuIcon _icon) { this(_icon,_label); }
   */

  public BuToolButton(BuIcon _icon, String _label) {
    super();

    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setIcon(_icon);
    setText(_label);
    setBorderPainted(false);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g) {
    BuLib.setAntialiasing(this, _g);
    super.paint(_g);
  }

  @Override
  public boolean isOpaque() {
    boolean r = super.isOpaque();

    Container parent = getParent();

    if (parent instanceof JComponent) {
      Object p = ((JComponent) parent).getClientProperty("JToolBar.isRollover");
      if (Boolean.TRUE.equals(p)) r &= isEnabled() && getModel().isRollover();
    }

    return r;
  }

  protected boolean isHorizontal() {
    boolean r = true;
    Container parent = getParent();

    if (parent != null) {
      Dimension size = parent.getSize();
      r = (size.width > size.height);
    }

    return r;
  }

  @Override
  public Font getFont() {
    Font r = super.getFont();
    if ((r instanceof UIResource) && isHorizontal() && (getIcon() != null)) {
      r = BuLib.deriveFont("Button", Font.PLAIN, -2);
      // System.err.println("FAMILY="+r.getFamily());
      if ("dialog.bold".equals(r.getFamily())) r = new Font("SansSerif", Font.PLAIN, r.getSize());
      r = new FontUIResource(r);
    }
    return r;
  }

  @Override
  public int getHorizontalTextPosition() {
    return (isHorizontal() ? CENTER : RIGHT);
  }

  @Override
  public int getVerticalTextPosition() {
    return (isHorizontal() ? BOTTOM : CENTER);
  }

  @Override
  public Dimension getPreferredSize() {
    Dimension r = super.getPreferredSize();
    if (isHorizontal() && (getIcon() != null)) r.width = r.height;// Math.max(r.width,r.height);
    // 40;+=39-(r.width%40);
    // else r.width=3*r.height;
    return r;
  }

  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

  @Override
  public Dimension getMaximumSize() {
    return getPreferredSize();
  }

  @Override
  public void setText(String _text) {
    super.setText("".equals(_text) ? null : _text);

    if (!BuPreferences.BU.getBooleanProperty("tool.text", true) && (getToolTipText() == null)) super
        .setToolTipText(_text);
  }

  @Override
  public String getText() {
    if (BuPreferences.BU.getBooleanProperty("tool.text", true) || (super.getIcon() == null)
        || !BuPreferences.BU.getBooleanProperty("tool.icon", true)) {
      String r = super.getText();
      if ((getIcon() != null) && (r != null) && isHorizontal()) {
        FontMetrics fm = BuLib.getFontMetrics(this, getFont());
        Insets in = getInsets();
        int w = getWidth() - in.left - in.right;

        if (fm.stringWidth(r) > w) {
          while ((r.length() > 0) && (fm.stringWidth(r + ".") > w))
            r = r.substring(0, r.length() - 1);
          while ((r.length() > 0) && ("aeiouy����".indexOf(r.charAt(r.length() - 1)) > 0))
            r = r.substring(0, r.length() - 1);
          r += ".";
        }
      }
      return r;
    }

    return null;
  }

  public void setIcon(BuIcon _icon) {
    BuLib.setIcon(this, _icon);
  }

  @Override
  public Icon getIcon() {
    if (BuPreferences.BU.getBooleanProperty("tool.icon", true) || (super.getText() == null)
        || !BuPreferences.BU.getBooleanProperty("tool.text", true)) return super.getIcon();
    return null;
  }

  @Override
  public Point getToolTipLocation(MouseEvent _evt) {
    Point r;

    Container parent = getParent();
    if ((parent == null) || isHorizontal()) r = new Point(0, getHeight() + 1);
    else r = new Point(-getX() + parent.getSize().width - 1, 0);

    return r;
  }

  private static final Insets REF = new Insets(2, 14, 2, 14);
  private static final InsetsUIResource ALT = new InsetsUIResource(0, 8, 0, 8);

  @Override
  public void updateUI() {
    super.updateUI();
    setBorderPainted(false);
    setRolloverEnabled(true);

    if (BuLib.isOcean()) {
      Insets i = getMargin();
      if ((i instanceof UIResource) && REF.equals(i)) setMargin(ALT);
    }
  }

  @Override
  public Color getBackground() {
    Color r;
    if (isEnabled() && getModel().isRollover() && BuLib.isMetal()) r = new ColorUIResource(192, 192, 208);
    else r = super.getBackground();
    return r;
  }
}
