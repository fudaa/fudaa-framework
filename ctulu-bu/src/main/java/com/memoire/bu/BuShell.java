/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuShell.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import javax.swing.JOptionPane;

public class BuShell
{
  protected static final String __(String _s)
  {
    return BuResource.BU.getString(_s);
  }

  public static void runCommand(BuCommonInterface _app, String _cmd)
  {
    runCommand
      (_app,_cmd,BuPreferences.BU.getBooleanProperty("tool.confirm",true));
  }

  public static void runCommand
    (BuCommonInterface _app, String _cmd, boolean _confirm)
  {
    if(  (!_confirm)
       ||(JOptionPane.YES_OPTION==
           (new BuDialogConfirmation
	     (_app,null,
	      FuLib.replace(_cmd,"; ",";\n").trim()+"\n"+
	      __("Voulez-vous continuer ?"))
	       .activate())))
    {
      String s=FuLib.runShellCommand(_cmd);
      if(!"".equals(s))
	new BuDialogMessage(_app,null,s).activate();
    }
  }
}
