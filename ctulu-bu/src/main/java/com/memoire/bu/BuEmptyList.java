/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuEmptyList.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.Vector;
import javax.swing.ListModel;

/**
 * A list which changes its background and displays a message
 * when it's empty.
 */
public class BuEmptyList
  extends BuList
{
  protected static Font getETF()
  {
    return BuLib.deriveFont("List",Font.PLAIN,-2);
  }

  public BuEmptyList()
  {
    super();
  }

  public BuEmptyList(ListModel _model)
  {
    super(_model);
  }

  public BuEmptyList(Object[] _data)
  {
    super(_data);
  }

  public BuEmptyList(Vector _data)
  {
    super(_data);
  }

  /*
  public Color getBackground()
  {
    Color     bg=null;
    ListModel lm=getModel();

    if(lm.getSize()==0)
      bg=UIManager.getColor("Label.background");
    else
      bg=super.getBackground();
      // UIManager.getColor("Label.background").brighter();

    return bg;
  }
  */

  @Override
  public void paintComponent(Graphics _g)
  {
    try { super.paintComponent(_g); }
    catch(ArrayIndexOutOfBoundsException ex) { } // swing 1.03

    ListModel lm=getModel();
    if(lm.getSize()==0)
    {
      Insets  insets=getInsets();
      Font    ft    =getETF();
      int     hs    =ft.getSize();
      Color   fg    =getForeground();
      //Color   bg    =getBackground();
      String  s     =getEmptyText();

      _g.setFont(ft);
      _g.setColor(fg);
      _g.drawString(s,insets.left+2,insets.top+hs);
    }
  }

  private String emptyText_="";
  public String getEmptyText() { return emptyText_; }
  public void setEmptyText(String _v)
  {
    if(!emptyText_.equals(_v))
    {
      String vp=emptyText_;
      emptyText_=_v;
      firePropertyChange("emptyText",_v,vp);
      repaint();
    }
  }
}







