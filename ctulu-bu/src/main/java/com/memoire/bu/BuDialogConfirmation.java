/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuDialogConfirmation.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 * A standard confirm dialog (yes/no).
 */
public class BuDialogConfirmation
  extends BuDialog
  implements ActionListener
{
  protected BuButton btOui_;
  protected BuButton btNon_;

  public BuDialogConfirmation(BuCommonInterface      _parent,
			      BuInformationsSoftware _isoft,
			      Object                 _message)
  {
    super(_parent,_isoft,__("Confirmation"),_message);

    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    btOui_=new BuButton(BuResource.BU.loadButtonCommandIcon("OUI"),
		       getString("Oui"));
    btOui_.addActionListener(this);
    pnb.add(btOui_);

    btNon_=new BuButton(BuResource.BU.loadButtonCommandIcon("NON"),
                        getString("Non"));
    btNon_.addActionListener(this);
    getRootPane().setDefaultButton(btNon_);
    pnb.add(btNon_);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  @Override
  public JComponent getComponent()
  { return null; }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialog : "+source);

    if(source==btOui_)
    {
      reponse_=JOptionPane.YES_OPTION;
      setVisible(false);
    }

    if(source==btNon_)
    {
      reponse_=JOptionPane.NO_OPTION;
      setVisible(false);
    }
  }

  /*
  public static void main(String[] _args)
  {
    new BuDialogConfirmation
      (null,null,
       "Please confirm the total destruction of the world.")
      .activate();
    System.exit(0);
  }
  */
}
