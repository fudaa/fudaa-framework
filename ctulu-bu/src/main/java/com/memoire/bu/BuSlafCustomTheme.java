/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuSlafCustomTheme.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * A simple theme chooser for Slaf.
 */

public class BuSlafCustomTheme
       implements Icon
{
  private static final BuSlafCustomTheme[] THEMES=
    new BuSlafCustomTheme[]
    {
      new BuSlafCustomTheme(),
      null,
      new BuSlafCustomTheme("Alien"),
      new BuSlafCustomTheme("Aluminium"),
      new BuSlafCustomTheme("Children"),
      new BuSlafCustomTheme("Clown"),
      new BuSlafCustomTheme("Ecologic"),
      new BuSlafCustomTheme("Mondrian"),
      new BuSlafCustomTheme("Sand"),
      new BuSlafCustomTheme("Shadow"),
      new BuSlafCustomTheme("Tx"),
      null,
      new BuSlafCustomTheme("Apple2c"),
      new BuSlafCustomTheme("Apple2e"),
      new BuSlafCustomTheme("Gtk"),
      new BuSlafCustomTheme("Gnome"),
      new BuSlafCustomTheme("Kde"),
      new BuSlafCustomTheme("Mezzo"),
      new BuSlafCustomTheme("Step"),
      new BuSlafCustomTheme("X11"),
      null,
      new BuSlafCustomTheme("Aqua"),
      new BuSlafCustomTheme("Beos"),
      new BuSlafCustomTheme("Edbgruppen"),
      new BuSlafCustomTheme("Galaxy"),
      new BuSlafCustomTheme("Lcars"),
      new BuSlafCustomTheme("Lothar"),
      new BuSlafCustomTheme("Napkin"),
      new BuSlafCustomTheme("Pilot"),
      new BuSlafCustomTheme("Redmond31"),
      new BuSlafCustomTheme("Redmond00"),
      new BuSlafCustomTheme("Redmondxp"),
    };

  //private boolean defaut_;
  private String  name_;
  private String  key_;
  private Icon    icon_;

  public BuSlafCustomTheme()
  {
    //defaut_=true;
    name_  ="Traditionnel";
    key_   ="default";

    try
    {
      String f=FuLib.getUserHome()
              +System.getProperty("file.separator")+".slaf"
              +System.getProperty("file.separator")+"icons"
              +System.getProperty("file.separator")+"default_lnf.gif";
      icon_=new ImageIcon(f);
    }
    catch(Throwable th) { }
  }

  public BuSlafCustomTheme(String _name)
  {
   // defaut_=false;
    name_  =_name;
    key_   =_name.toLowerCase();

    try
    {
      String f=FuLib.getUserHome()
              +System.getProperty("file.separator")+".slaf"
              +System.getProperty("file.separator")+"icons"
              +System.getProperty("file.separator")+key_+"_lnf.gif";
      icon_=new ImageIcon(f);
    }
    catch(Throwable th) { }
  }

  public String getName() { return name_; }
  public String getKey () { return key_;  }
  public Icon   getIcon() { return this;  }

  @Override
  public int getIconWidth()
  {
    int r=16;
    if(icon_!=null) r=Math.max(r,icon_.getIconWidth());
    return r;
  }

  @Override
  public int getIconHeight()
  {
    int r=16;
    if(icon_!=null) r=Math.max(r,icon_.getIconHeight());
    return r;
  }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    if(icon_!=null)
      icon_.paintIcon(_c,_g,_x,_y);
    else
    {
      Color cold=_g.getColor();
      Font  fold=_g.getFont();

      int w=getIconWidth();
      int h=getIconHeight();

      _g.setColor(Color.lightGray);
      _g.fill3DRect(_x+1,_y+1,w-2,h-2,true);

      _g.setColor(Color.black);
      _g.drawRect(_x,_y,w-1,h-1);

      _g.setColor(cold);
      _g.setFont(fold);
    }
  }

  public static BuSlafCustomTheme[] getList()
  {
    return THEMES;
  }
}
