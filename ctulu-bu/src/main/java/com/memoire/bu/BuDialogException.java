/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuDialogException.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Properties;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 * A standard exception dialog (copy/continue).
 */
public class BuDialogException
  extends BuDialog
  implements ActionListener
{
  protected BuButton         btContinuer_,btCopier_;
  protected BuTabbedPane     tbPane_;
  protected BuLabelMultiLine lbExplanation_;
  protected BuLabelMultiLine lbTrace_;
  protected BuTable          tbProperties_,tbEnvironment_;
  protected String           fullText_;

  public BuDialogException(BuCommonInterface      _parent,
			   BuInformationsSoftware _isoft,
			   String                 _message,
			   Throwable              _exception,
			   Properties             _properties)
  {
    this(_parent,_isoft,_message,_exception,_properties,null);
  }

  public BuDialogException(BuCommonInterface      _parent,
			   BuInformationsSoftware _isoft,
			   String                 _message,
			   Throwable              _exception,
			   Properties             _properties,
                           Properties             _environment)
  {
    super(_parent,_isoft,__("Exception"));

    StringBuffer sb=new StringBuffer(1024);

    sb.append("Comments:\n\n\n\n\n\n");
    BuInformationsSoftware isoft=_isoft;
    if((isoft==null)&&(_parent!=null))
      isoft=_parent.getInformationsSoftware();

    sb.append("Software:\n");
    if(isoft==null)
    {
      sb.append("unknown\n");
    }
    else
    {
      sb.append("name=");
      sb.append(isoft.name);
      sb.append('\n');
      sb.append("version=");
      sb.append(isoft.version);
      sb.append('\n');
      sb.append("date=");
      sb.append(isoft.date);
      sb.append('\n');
    }
    sb.append('\n');

    sb.append("User:\n");
    sb.append("lastname=");
    sb.append(BuPreferences.BU.getStringProperty("user.lastname"));
    sb.append('\n');
    sb.append("firstname=");
    sb.append(BuPreferences.BU.getStringProperty("user.firstname"));
    sb.append('\n');
    sb.append("email=");
    sb.append(BuPreferences.BU.getStringProperty("user.email"));
    sb.append('\n');
    sb.append("organization=");
    sb.append(BuPreferences.BU.getStringProperty("user.organization"));
    sb.append('\n');
    sb.append("department=");
    sb.append(BuPreferences.BU.getStringProperty("user.department"));
    sb.append('\n');
    sb.append('\n');

    sb.append("Context:\n");
    sb.append("lookandfeel=");
    sb.append(UIManager.getLookAndFeel());
    sb.append('\n');
    Dimension de=Toolkit.getDefaultToolkit().getScreenSize();
    sb.append("screen="+de.width+"x"+de.height);
    sb.append('\n');
    sb.append('\n');
    String                 message=_message;
    if(message==null) message="none";
    message=message.trim()+"\n";
    sb.append("Exception:\n");
    sb.append(message);
    message+=" \n \nPlease help us to improve this software.\n";
    message+="- click 'Copy'\n";
    message+="- Paste the text to a new message\n";
    message+="- Send us the message to "+
      (isoft!=null ? isoft.contact : "guillaume@desnoix.com")+"\n";
    lbExplanation_.setText(message);
    sb.append('\n');

    String t;
    if(_exception==null)
    {
      t="none";
    }
    else
    {
      ByteArrayOutputStream baout=new ByteArrayOutputStream();
      PrintStream           psout=new PrintStream(baout);
      _exception.printStackTrace(psout);
      t=new String(baout.toByteArray());
      baout=null;
      psout=null;
    }
    sb.append("Trace:\n");
    sb.append(t);
    sb.append('\n');
    lbTrace_.setText(t);
    Properties             properties=_properties;
    if(properties==null)
      properties=System.getProperties();

    // build properties model
    {
      String[][] data=FuLib.convertPropertiesToArray(properties);
      tbProperties_.setModel
        (new BuTableStaticModel
         (data,new String[]
           { getString("Propri�t�"),
             getString("Valeur")}));
      sb.append("System Properties:\n");
      int lmax=0;
      for(int i=0;i<data.length;i++)
        lmax=Math.max(lmax,(""+data[i][0]).length());
      for(int i=0;i<data.length;i++)
      {
        sb.append(FuLib.rightpad(""+data[i][0],lmax,' '));
        sb.append('=');
        sb.append(""+data[i][1]);
        sb.append('\n');
      }
      sb.append('\n');
    }

    if(_environment!=null)
    {
      String[][] data=FuLib.convertPropertiesToArray(_environment);
      tbEnvironment_.setModel
        (new BuTableStaticModel
         (data,new String[]
           { getString("Variable"),
             getString("Valeur")}));
      sb.append("Environment:\n");
      int lmax=0;
      for(int i=0;i<data.length;i++)
        lmax=Math.max(lmax,(""+data[i][0]).length());
      for(int i=0;i<data.length;i++)
      {
        sb.append(FuLib.rightpad(""+data[i][0],lmax,' '));
        sb.append('=');
        sb.append(""+data[i][1]);
        sb.append('\n');
      }
      sb.append('\n');
    }

    fullText_=sb.toString();
    sb=null;

    BuPanel pnb=new BuPanel();
    pnb.setLayout(new BuButtonLayout());
    //new FlowLayout(FlowLayout.RIGHT));

    BuButton[] bt=getButtons();
    for(int i=0;i<bt.length;i++)
      pnb.add(bt[i]);
    getRootPane().setDefaultButton(bt[bt.length-1]);

    content_.add(pnb,BuBorderLayout.SOUTH);
  }

  protected BuButton[] getButtons()
  {
    btCopier_   =new BuButton(BuResource.BU.loadButtonCommandIcon("COPIER"),
                              getString("Copier"));
    btContinuer_=new BuButton(BuResource.BU.loadButtonCommandIcon("CONTINUER"),
			      getString("Continuer"));
    btCopier_   .addActionListener(this);
    btContinuer_.addActionListener(this);

    return new BuButton[] { btCopier_,btContinuer_ };
  }

  @Override
  public boolean needsScrollPane()
  {
    return false;
  }

  @Override
  public JComponent getComponent()
  {
    lbExplanation_=new BuLabelMultiLine();
    lbTrace_      =new BuLabelMultiLine();
    tbProperties_ =new BuTable();
    tbEnvironment_=new BuTable();

    lbExplanation_.setOpaque(true);
    lbExplanation_.setBackground(BuLib.getColor(Color.white));
    lbExplanation_.setForeground(BuLib.getColor(Color.black));
    lbExplanation_.setWrapMode(BuLabelMultiLine.WORD);
    lbExplanation_.setBorder(BuBorders.EMPTY5555);
    lbExplanation_.setFont(BuLib.deriveFont("Label",Font.PLAIN,0));

    lbTrace_.setOpaque(true);
    lbTrace_.setBackground(BuLib.getColor(Color.white));
    lbTrace_.setForeground(BuLib.getColor(Color.black));
    lbTrace_.setWrapMode(BuLabelMultiLine.NONE);
    lbTrace_.setBorder(BuBorders.EMPTY5555);
    BuLib.setMonospacedFont(lbTrace_);

    tbPane_=new BuTabbedPane();
    tbPane_.addTab(getString("Texte"),null,
		   new BuScrollPane(lbExplanation_),null);
    tbPane_.addTab(getString("Trace"),null,
		   new BuScrollPane(lbTrace_),null);
    tbPane_.addTab(getString("Propri�t�s"),null,
		   new BuScrollPane(tbProperties_),null);
    tbPane_.addTab(getString("Environment"),null,
		   new BuScrollPane(tbEnvironment_),null);

    return tbPane_;
  }

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    JComponent source=(JComponent)_evt.getSource();
    // System.err.println("BuDialogue : "+source);

    if(source==btCopier_)
    {
      Clipboard clipboard=getToolkit().getSystemClipboard();
      StringSelection contents=new StringSelection(fullText_);
      clipboard.setContents(contents,null);
    }
    else
    if(source==btContinuer_)
    {
      reponse_=JOptionPane.OK_OPTION;
      setVisible(false);
    }
  }

  public static void main(String[] _args)
  {
    new BuDialogException(null,null,"Test",new Exception(),null).activate();
    System.exit(0);
  }
}

