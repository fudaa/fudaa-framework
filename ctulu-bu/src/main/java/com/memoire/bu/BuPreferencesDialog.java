/**
 *  @creation     26 mai 2004
 *  @modification $Date: 2006-09-19 14:35:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package com.memoire.bu;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JDialog;

/**
 * @author Fred Deniger
 * @version $Id: BuPreferencesDialog.java,v 1.5 2006-09-19 14:35:10 deniger Exp $
 */
public class BuPreferencesDialog extends JDialog implements ActionListener {

  /**
   * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
   */
  public void propertyChange(PropertyChangeEvent _evt){
    if (_evt.getSource().equals(getContentPane()) && (_evt.getPropertyName().equals("close"))) {
      dispose();
    }
  }

  public BuPreferencesDialog(Frame _parent, BuPreferencesMainPanel _p) {
    super(_parent);
    setModal(true);
    if (_p != null) {
      setContentPane(_p);
      _p.getFermerButton().addActionListener(this);
      this.getRootPane().setDefaultButton(_p.getValiderButton());
      setTitle(BuResource.BU.getString("Préférences de démarrage"));
    }
    pack();
  }

  @Override
  public void actionPerformed(ActionEvent _e){
    if (_e.getActionCommand().equals("FERMER")) {
      dispose();
    }

  }

}
