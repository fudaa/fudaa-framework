/**
 * @modification $Date: 2008-02-14 17:00:27 $
 * @statut       unstable
 * @file         BuMenuRecentFiles.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.fu.FuPreferences;
import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

public class BuMenuRecentFiles
  extends BuDynamicMenu
          //implements PropertyChangeListener
{
  private int           number_;
  private boolean       valid_;
  private FuPreferences preferences_;
  private BuResource    resource_;

  public BuMenuRecentFiles()
  {
    this(null,null);
  }

  public BuMenuRecentFiles(FuPreferences _preferences, BuResource _resource)
  {
    super(__("R�ouvrir"),"REOUVRIR");
    setResource(_resource);
    setPreferences(_preferences);
    //build();
  }

  private int getNumber()
  {
    int r=0;
    if(preferences_!=null)
      for(int i=1;i<=8;i++)
        if(!"".equals
           (preferences_.getStringProperty("file.recent."+i+".path")))
          r++;
    return r;
  }

  public FuPreferences getPreferences()
  {
    return preferences_;
  }

  public void setPreferences(FuPreferences _preferences)
  {
    //if(preferences_!=null) preferences_.removePropertyChangeListener(this);
    preferences_=_preferences;
    //if(preferences_!=null) preferences_.addPropertyChangeListener(this);
    valid_ =false;
    number_=getNumber();
  }

  public BuResource getResource()
  {
    return resource_;
  }

  public void setResource(BuResource _resource)
  {
    resource_=_resource;
    valid_ =false;
    number_=getNumber();
  }

  /**
   * Ajout du fichier dans la liste des fichiers recents. Si le fichier existe d�j� dans la liste,
   * il est mit en tete de liste.
   * @param _path
   * @param _icon
   */
  public void addRecentFile(String _path, String _icon)
  {
    if(preferences_!=null)
    {
      ArrayList lpath=new ArrayList();
      ArrayList licon=new ArrayList();
      lpath.add(_path==null?"":_path);
      licon.add(_icon==null?"":_icon);
      
      for (int i=1; i<9; i++) {
        String path=preferences_.getStringProperty("file.recent."+i+".path");
        if (path!=null && !path.equals(_path)) {
          lpath.add(path);
          licon.add(preferences_.getStringProperty("file.recent."+i+".icon"));
        }
        preferences_.removeProperty("file.recent."+i+".path");
        preferences_.removeProperty("file.recent."+i+".icon");
      }
      
      for (int i=0; i<lpath.size(); i++) {
        preferences_.putStringProperty("file.recent."+(i+1)+".path",(String)lpath.get(i));
        preferences_.putStringProperty("file.recent."+(i+1)+".icon",(String)licon.get(i));
      }
    }

    valid_ =false;
    number_=getNumber();
  }

  /*
  public void propertyChange(PropertyChangeEvent _evt)
  {
    if(_evt.getPropertyName().startsWith("file."))
    {
      valid_=false;
      number_=getNumber();
    }
  }
  */

  /*
  public final void setSelected(boolean _state)
  {
    if(_state&&!isSelected()&&!valid_) build();
    super.setSelected(_state);
  }
  */

  @Override
  protected void build()
  {
    if(isPopupMenuVisible()||valid_) return;

    FuLog.debug("BMT: build menu recent files");

    valid_=true;
    removeAll();
    number_=0;
    if(preferences_==null) return;

    Hashtable table=new Hashtable(11);
    
    for(int i=1; i<=9; i++)
    {
      String path=preferences_.getStringProperty("file.recent."+i+".path");
      String icon=preferences_.getStringProperty("file.recent."+i+".icon");

      if(  !"".equals(path)
         &&(new File(FuLib.expandedPath(path)).canRead())
	 &&(table.get(path)==null))
      {
	String t=FuLib.reducedPath(path);
	if(t.length()>20)
	{
	  String FS=System.getProperty("file.separator");

	  int j0=t.indexOf(FS);
	  if(j0>=0)
	  {
	    int j1=t.indexOf(FS,j0+1);
	    int j2=t.lastIndexOf(FS);
	    if((j1>=0)&&(j2>=0)&&(j1<j2))
	      t=t.substring(0,j1+1)+"..."+t.substring(j2);
	  }
	}

        BuMenuItem mi=addMenuItem(""+number_+" "+t,"REOUVRIR("+FuLib.expandedPath(path)+")",true);
        if(resource_!=null) mi.setIcon(resource_.getMenuIcon(icon));
        mi.setToolTipText(path);
        number_++;
        table.put(path,icon);
      }
    }

    computeMnemonics();
  }

  /*
  public void paintComponent(Graphics _g)
  {
    boolean r=isEnabled();
    if(r!=super.isEnabled()) setEnabled(r);
    super.paintComponent(_g);
  }

  public boolean isEnabled()
  {
    if(!valid_) build();
    return (preferences_!=null)&&(number_>0);
  }
  */

  @Override
  public boolean isActive()
  {
    return (number_>0);
  }
}
