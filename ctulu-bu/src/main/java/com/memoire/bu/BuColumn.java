/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuColumn.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;

/**
 * A simple column of components with associated toggle-buttons.
 */

public class BuColumn extends BuPanel {
  private Hashtable buttons_;
  private Hashtable views_;
  private BuPanel top_;
  private boolean compact_;

  public BuColumn() {
    super();

    buttons_ = new Hashtable();
    views_ = new Hashtable();
    top_ = null;
    compact_ = BuPreferences.BU.getBooleanProperty("column.compact", false);

    BuVerticalLayout layout = new BuVerticalLayout();
    layout.setVgap(2);
    layout.setHfilled(true);
    layout.setVfilled(true);
    setLayout(layout);

    if (compact_) layout.setVgap(0);

    if (!BuPreferences.BU.getBooleanProperty("button.text", true)) {
      top_ = new BuPanel();
      top_.setLayout(new BuHorizontalLayout(2, false, true));
      add(top_);
    }
  }

  @Override
  public void updateUI() {
    super.updateUI();

    Border b = UIManager.getBorder("Column.border");
    if (b != null) setBorder(b);
    Color bg = UIManager.getColor("Column.background");
    if (bg != null) setBackground(bg);
    Color fg = UIManager.getColor("Column.foreground");
    if (fg != null) setForeground(fg);
    Font ft = UIManager.getFont("Column.font");
    if (ft != null) setFont(ft);
  }

  @Override
  public boolean isFocusCycleRoot() {
    return true;
  }

  /*
   * public boolean isVisible() { return super.isVisible()&&(getComponentCount()>0); }
   */

// B.M. : M�me quand la colonne ne comporte aucun �l�ment, la taille pr�f�rentielle
// doit �tre retourn�e. Cette taille est conserv�e d'un appel a l'autre de l'appli
// Dans les pr�f�rences.
//
//  public Dimension getPreferredSize() {
//    if (getComponentCount() > 0) return super.getPreferredSize();
//    return new Dimension(0, 0);
//  }

  public JToggleButton addToggledComponent(String _label, String _cmd, JComponent _view, ActionListener _main) {
    BuIcon icon = BuResource.BU.loadMenuCommandIcon(_cmd);
    JToggleButton r = addToggledComponent(_label, _cmd, icon, _view, true, _main);
    return r;
  }

  public JToggleButton addToggledComponent(String _label, String _cmd, JComponent _view, boolean _visible,
      ActionListener _main) {
    BuIcon icon = BuResource.BU.loadMenuCommandIcon(_cmd);
    JToggleButton r = addToggledComponent(_label, _cmd, icon, _view, _visible, _main);
    return r;
  }

  /**
   * Add a component and create an associated toggle-button.
   * 
   * @param _label label of the toggleButton
   * @param _cmd actionCommand of the toggleButton
   * @param _icon icon of the toggleButton
   * @param _view the component
   * @param _visible initial visibility
   * @param _main a listener (application)
   */
  public JToggleButton addToggledComponent(String _label, String _cmd, BuIcon _icon, JComponent _view,
      boolean _visible, ActionListener _main) {
    BuToggleButton r = new BuToggleButton();
    r.setText(_label);
    r.setName("tb" + _cmd);
    r.setActionCommand(_cmd);
    r.setHorizontalAlignment(SwingConstants.LEFT);
    r.setRequestFocusEnabled(false);

    if (compact_) {
      r.setMargin(BuInsets.INSETS0000);
      int t = BuResource.BU.getDefaultMenuSize();
      if (t > 16) t /= 2;
      r.setIcon(BuResource.resizeIcon(_icon, t));
    } else {
      r.setMargin(BuInsets.INSETS1111);
      r.setIcon(_icon);
    }

    r.addActionListener(_main);

    // r.setSelected(_visible);
    // _view.setVisible(_visible);

    buttons_.put(_cmd, r);
    views_.put(_cmd, _view);

    if (top_ != null) top_.add(r);
    else
      add(r);

    add(_view);
    toggleComponent(_cmd, _visible);

    return r;
  }

  public void removeToggledComponent(String _cmd) {
    JToggleButton tb = (JToggleButton) buttons_.get(_cmd);
    JComponent cp = (JComponent) views_.get(_cmd);

    if ((tb != null) && (cp != null)) {
      buttons_.remove(_cmd);
      views_.remove(_cmd);

      if (top_ != null) top_.remove(tb);
      else
        remove(tb);

      remove(cp);
      revalidate();
      repaint();
    }
  }

  public boolean isToggleComponentVisible(String _cmd) {
    boolean r = false;
    JComponent cp = (JComponent) views_.get(_cmd);

    if (cp != null) r = cp.isVisible();
    return r;
  }

  public void toggleComponent(String _cmd) {
    JComponent cp = (JComponent) views_.get(_cmd);

    if (cp != null) {
      boolean state = !cp.isVisible();
      toggleComponent(_cmd, state);
    }
  }

  public void toggleComponent(String _cmd, boolean _state) {
    JToggleButton tb = (JToggleButton) buttons_.get(_cmd);
    JComponent cp = (JComponent) views_.get(_cmd);

    if ((tb != null) && (cp != null)) {
      /*
       * if(compact_&&_state) { if(top_!=null) top_.remove(tb); else remove(tb); remove(cp); if(top_!=null)
       * top_.add(tb); else add(tb); add(cp); for(Enumeration e=buttons_.keys(); e.hasMoreElements(); ) { String
       * s=(String)e.nextElement(); if( !_cmd.equals(s) &&isToggleComponentVisible(s))
       * ((JToggleButton)buttons_.get(s)).doClick(); } }
       */

      tb.setSelected(_state);
      cp.setVisible(_state);
      revalidate();
      repaint();
    }
  }

  public JToggleButton getToggleButton(String _cmd) {
    return (JToggleButton) buttons_.get(_cmd);
  }

  public JComponent getToggleComponent(String _cmd) {
    return (JComponent) views_.get(_cmd);
  }
}
