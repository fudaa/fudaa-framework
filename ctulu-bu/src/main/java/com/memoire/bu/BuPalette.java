/**
 * @modification $Date: 2007-05-04 13:41:57 $
 * @statut       unstable
 * @file         BuPalette.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;

/**
 * A component derived from JInternalFrame to make palettes.
 */
public class BuPalette
  extends JInternalFrame
  implements ActionListener
{
  private static final Border BORDER=
    new BorderUIResource(new BuLightBorder(BuLightBorder.RAISED,1));

  JComponent content_;

  public BuPalette()
  {
    super("",false,false,false,false);
    setName ("plPALETTE");
    setFrameIcon(new BuEmptyIcon());
    putClientProperty("JInternalFrame.isPalette",Boolean.TRUE);
    setLayer(JLayeredPane.PALETTE_LAYER);

    content_=(JComponent)getContentPane();
    content_.setLayout(new BuBorderLayout());
    setSize(120,120);
    //setResizable(false);
  }

  /*public boolean isFocusCycleRoot()
    { return false; }*/

  @Override
  public void updateUI()
  {
    super.updateUI();

    //if(!BuLib.isSlaf()) setBorder(BORDER);
    if(UIManager.getBorder("InternalFrame.paletteBorder")==null)
      setBorder(BORDER);

    if(!isResizable())
    {
      //SwingUtilities.updateComponentTreeUI(getContentPane());
      pack();
    }
    //else SwingUtilities.updateComponentTreeUI(getContentPane());

    Container cp=getContentPane();
    if((cp!=null)&&(cp.getBackground()==null))
      cp.setBackground
	(UIManager.getColor("Panel.background"));
  }

  private JComponent cp_;

  public JComponent getContent()
  {
    return cp_;
  }

  public void setContent(JComponent _cp)
  {
    cp_=_cp;
    content_.add(cp_,BuBorderLayout.CENTER);
    cp_.revalidate();

    pack();
    revalidate();
  }

  /**
   * A palette can not be activated (selected).
   */
  @Override
  public void setSelected(boolean _b) throws PropertyVetoException
  {
    if(_b)
    {
      moveToFront();
      throw new PropertyVetoException("Palette",null);
    }
  }

  @Override
  public boolean isSelected()
  { return false; }

  @Override
  public void setMaximum(boolean _maximum)
  { }

  /*
  public final void paintBorder(Graphics _g) { }

  protected void paintSuperBorder(Graphics _g)
  {
    super.paintBorder(_g);
  }

  public void paint(Graphics _g)
  {
    super.paint(_g);
    paintSuperBorder(_g);
  }
  */

  @Override
  public void actionPerformed(ActionEvent _evt)
  {
    String action=_evt.getActionCommand();
    FuLog.error("BPL: illegal action on "+getClass().getName()+": "+action);
  }

  // Test

  /*
  public static void main(String[] _args)
  {
    JFrame f=new JFrame();
    BuPanel barre=new BuPanel();
    BuDesktop dt=new BuDesktop();

    // ButtonGroup bg=new ButtonGroup();

    BuLabel p1=new BuLabel("Hello the world");
    BuPopupButton w1=new BuPopupButton("P1",p1);
    w1.setText("Click here!");
    w1.setDesktop(dt);

    BuButton p2=new BuButton("Bonjour Monde");
    BuPopupButton w2=new BuPopupButton("P2",p2);
    w2.setText("Click here!");
    w2.setDesktop(dt);

    // bg.add(w1);
    // bg.add(w2);

    barre.add(w1);
    barre.add(w2);
    f.getContentPane().add(barre              ,BuBorderLayout.NORTH);
    f.getContentPane().add(new BuScrollPane(dt),BuBorderLayout.CENTER);

    BuPalette pal=new BuPalette();
    pal.setTitle("WWW");
    pal.setContent(new BuLabel("Hello"));
    dt.addInternalFrame(pal);

    dt.addInternalFrame(new BuInternalFrame("XXX",true,true,true,true));

    f.setSize(300,300);
    f.setVisible(true);
    f.setLocation(300,150);
  }
  */
}
