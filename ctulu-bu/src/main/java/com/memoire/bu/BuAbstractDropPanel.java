/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuAbstractDropPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.dt.DtDragSensible;
import com.memoire.dt.DtDropBasic;
import com.memoire.dt.DtDropReady;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

public abstract class BuAbstractDropPanel
  extends BuPanel
  implements DtDragSensible, DtDropReady
{
  private boolean   drop_;
  private Rectangle rect_;

  public BuAbstractDropPanel()
  {
    super();
    //setBorder(BuBorders.EMPTY5555);
    DtDropBasic.install(this);
  }

  public BuAbstractDropPanel(LayoutManager _layout)
  {
    super(_layout);
    //setBorder(BuBorders.EMPTY5555);
    DtDropBasic.install(this);
  }

  protected Rectangle getDropRect()
  {
    if(rect_==null) rect_=new Rectangle(2,2,0,0);
    rect_.width =getWidth ()-4;
    rect_.height=getHeight()-4;
    return rect_;
  }

  @Override
  public void dragEnter(boolean _acceptable, Point _location)
  {
    dragOver(_acceptable,_location);
  }

  @Override
  public void dragOver(boolean _acceptable, Point _location)
  {
    if(!drop_&&_acceptable) { drop_=true; repaint(); }
  }

  @Override
  public void dragExit()
  {
    if(drop_) { drop_=false; repaint(); }
  }

  @Override
  public void paint(Graphics _g)
  {
    super.paint(_g);

    if(drop_)
    {
      Rectangle r=getDropRect();
      _g.setColor(new Color(255,128,0,64));
      _g.fillRoundRect(r.x,r.y,r.width,r.height,9,9);
      _g.setColor(new Color(255,128,0));
      _g.drawRoundRect(r.x,r.y,r.width-1,r.height-1,9,9);
    }
  }

  @Override
  public boolean isDropEnabled()
  {
    return isEnabled();
  }

  @Override
  public abstract int getDropActions();
  @Override
  public abstract boolean checkDropFlavor(DataFlavor[] _f, Point _p);
  @Override
  public abstract Object getDropData(Transferable _t);
  @Override
  public abstract boolean dropData(int _a, Object _data, Point _p);
}
