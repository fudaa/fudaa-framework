/**
 * @modification $Date: 2007-05-04 13:41:58 $
 * @statut       unstable
 * @file         BuMainPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;

/**
 * The main panel. Contains a status bar, a specific button bar, two columns of components and in general a desktop with
 * internal frames.
 */
public class BuMainPanel extends BuPanel {
  private BuSpecificBar specificbar_;
  protected BuPanel specificpanel_;
  private BuDesktop desktop_;
  private BuStatusBar statusbar_;
  protected BuSplit3Pane splitpane_;
  private BuColumn leftcolumn_;
  private BuColumn rightcolumn_;
  private BuScrollPane scrollDesktop_;
 

  private boolean swapped_ ;

  public BuMainPanel() {
    this(null);
  }

  public BuMainPanel(JComponent _middle) {
    this(_middle, true);

  }

  public BuScrollPane getScrollDesktop_() {
    return scrollDesktop_;
  }

  public void setScrollDesktop_(BuScrollPane scrollDesktop_) {
    this.scrollDesktop_ = scrollDesktop_;
  }
  
  public BuMainPanel(JComponent _middle, boolean _useScroll) {
    super();

    setName("buMAINPANEL");
    setLayout(new BuBorderLayout());
    setBorder(null);

    specificpanel_ = new BuPanel() {
      @Override
      public void updateUI() {
        super.updateUI();

        Border b = UIManager.getBorder("SpecificPanel.border");
        this.setBorder(b);
      }
    };

    specificpanel_.setLayout(new BuBorderLayout());
    // specificpanel_.setName("pnSPECIFIC");
    add(specificpanel_, BuBorderLayout.CENTER);

    setSpecificBar(new BuSpecificBar());
    setStatusBar(new BuStatusBar());

    JComponent middle = _middle;
    if (middle == null) middle = new BuDesktop();

    if (middle instanceof BuDesktop) {
      desktop_ = (BuDesktop) middle;
      if (!desktop_.isTabbed() && _useScroll) {
        scrollDesktop_ = new BuScrollPane(desktop_);
        scrollDesktop_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollDesktop_.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        middle = scrollDesktop_;
      }
    }

    leftcolumn_ = new BuColumn();
    leftcolumn_.setName("buLEFTCOLUMN");
    rightcolumn_ = new BuColumn();
    rightcolumn_.setName("buRIGHTCOLUMN");
    // specificpanel_.add(leftcolumn_ ,BuBorderLayout.WEST);
    // specificpanel_.add(rightcolumn_,BuBorderLayout.EAST);
    splitpane_ = new BuSplit3Pane(leftcolumn_, middle, rightcolumn_);
    specificpanel_.add(splitpane_, BuBorderLayout.CENTER);
  }

  // Specific ToolBar

  public BuSpecificBar getSpecificBar() {
    return specificbar_;
  }

  public void setSpecificBar(BuSpecificBar _specificbar) {
    if (specificbar_ != null) remove(specificbar_);
    specificbar_ = _specificbar;
    add(specificbar_, BuBorderLayout.NORTH);
  }

  // StatusBar

  public BuStatusBar getStatusBar() {
    return statusbar_;
  }

  public void setStatusBar(BuStatusBar _statusbar) {
    if (statusbar_ != null) specificpanel_.remove(statusbar_);
    statusbar_ = _statusbar;
    specificpanel_.add(statusbar_, BuBorderLayout.SOUTH);
  }

  public void setMessage(String _s) {
    statusbar_.setMessage(_s);
  }

  public void setProgression(int _v) {
    statusbar_.setProgression(_v);

    /*
     * Thread th=Thread.currentThread(); if(th instanceof BuTask) ((BuTask)th).setProgression(_v);
     */
  }

  // Desktop

  public BuDesktop getDesktop() {
    return desktop_;
  }

  public Icon getLogo() {
    return (desktop_ == null) ? null : desktop_.getLogo();
  }

  public void setLogo(Icon _icon) {
    if (desktop_ != null) desktop_.setLogo(_icon);
  }

  void addInternalFrame(JInternalFrame _f) {
    if (desktop_ != null) {
      desktop_.addInternalFrame(_f);
      if (getAssistant() != null) getAssistant().addEmitters(_f);
    }
  }

  void removeInternalFrame(JInternalFrame _f) {
    if (desktop_ != null) desktop_.removeInternalFrame(_f);
  }

  void activateInternalFrame(JInternalFrame _f) {
    if (desktop_ != null) desktop_.activateInternalFrame(_f);
  }

  void deactivateInternalFrame(JInternalFrame _f) {
    if (desktop_ != null) desktop_.deactivateInternalFrame(_f);
  }

  JInternalFrame getCurrentInternalFrame() {
    return (desktop_ == null) ? null : desktop_.getCurrentInternalFrame();
  }

  public JInternalFrame[] getAllInternalFrames() {
    return (desktop_ == null) ? null : desktop_.getAllFrames();
  }

  public void waterfall() {
    if (desktop_ != null) desktop_.waterfall();
  }

  public void tile() {
    if (desktop_ != null) desktop_.tile();
  }

  public void arrangeIcons() {
    if (desktop_ != null) desktop_.arrangeIcons();
  }

  public void arrangePalettes() {
    if (desktop_ != null) desktop_.arrangePalettes();
  }

  // Columns

  public BuColumn getLeftColumn() {
    return leftcolumn_;
  }

  public void setLeftColumn(BuColumn _leftcolumn) {
    if (leftcolumn_ != _leftcolumn) {
      leftcolumn_ = _leftcolumn;
      splitpane_.setLeftComponent(leftcolumn_);
      splitpane_.updateDisplay();
    }
  }

  public BuColumn getRightColumn() {
    return rightcolumn_;
  }

  public void setRightColumn(BuColumn _rightcolumn) {
    if (rightcolumn_ != _rightcolumn) {
      rightcolumn_ = _rightcolumn;
      splitpane_.setRightComponent(_rightcolumn);
      splitpane_.updateDisplay();
    }
  }

  public void updateDisplay() {
    splitpane_.updateDisplay();
  }

  public boolean isColumnsSwapped() {
    return swapped_;
  }

  public void swapColumns() {
    splitpane_.swapColumns();
    swapped_ = !swapped_;

    /*
     * specificpanel_.remove(leftcolumn_); specificpanel_.remove(rightcolumn_); swapped_=!swapped_; if(swapped_) {
     * specificpanel_.add(leftcolumn_ ,BuBorderLayout.EAST); specificpanel_.add(rightcolumn_,BuBorderLayout.WEST); }
     * else { specificpanel_.add(leftcolumn_ ,BuBorderLayout.WEST);
     * specificpanel_.add(rightcolumn_,BuBorderLayout.EAST); } specificpanel_.revalidate();
     */
  }

  public void updateSplits() {
    splitpane_.updateSplits();
  }

  public JComponent getMiddleComponent() {
    return splitpane_.getMiddleComponent();
  }

  // Assistant

  private BuAssistant assistant_;

  public BuAssistant getAssistant() {
    return assistant_;
  }

  public void setAssistant(BuAssistant _assistant) {
    assistant_ = _assistant;
  }

  // TaskView

  private BuTaskView task_view_;

  public BuTaskView getTaskView() {
    return task_view_;
  }

  public void setTaskView(BuTaskView _tv) {
    task_view_ = _tv;
  }

  // Divers

  public void showOrHide(JComponent _c) {
    if (_c != null) {
      boolean b = !_c.isVisible();
      _c.setVisible(b);
      revalidate();
      repaint();
    }
  }
}
