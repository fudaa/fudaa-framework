/**
 * @modification $Date: 2006-04-26 08:44:06 $
 * @statut       unstable
 * @file         BuTabbedPane.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;

/**
 * Nothing more than a JTabbedPane.
 */
public class BuTabbedPane
  extends JTabbedPane
{
  public BuTabbedPane()
  {
    super();

//    if(FuLib.jdk()>=1.4)
//    {
      setTabLayoutPolicy
        (BuPreferences.BU.getBooleanProperty("tabbedpane.scrolllayout",true)
         ? JTabbedPane.SCROLL_TAB_LAYOUT
         : JTabbedPane.WRAP_TAB_LAYOUT);
//    }
  }

  private static final void recursiveOpaque(BuPanel _p)
  {
    _p.setOpaque(false);
    Component[] c=_p.getComponents();
    for(int i=0;i<c.length;i++)
    {
      if(c[i] instanceof BuPanel)
	recursiveOpaque((BuPanel)c[i]);
      else
      if((c[i] instanceof BuCheckBox)||
	 (c[i] instanceof BuRadioButton))
	((JComponent)c[i]).setOpaque(false);
    }
  }

  public void addTab(String _title,Icon _icon,JComponent _c,String _tip)
  {
    if(BuLib.isOcean()&&(_c instanceof BuPanel))
      recursiveOpaque((BuPanel)_c);

    super.addTab(_title,_icon,_c,_tip);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }
}
