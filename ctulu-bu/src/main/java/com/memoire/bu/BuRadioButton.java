/**
 * @modification $Date: 2006-09-19 14:35:10 $
 * @statut       unstable
 * @file         BuRadioButton.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import javax.swing.JRadioButton;

/**
 * The Swing radiobutton...
 */
public class BuRadioButton
  extends JRadioButton
{
  public BuRadioButton()
  {
    this(null);
  }

  public BuRadioButton(String _text)
  {
    super();

    setText(_text==null?"":_text);
  }

  // Anti-aliasing

  @Override
  public void paint(Graphics _g)
  {
    BuLib.setAntialiasing(this,_g);
    super.paint(_g);
  }

  // Focus

  @Override
  protected void processFocusEvent(FocusEvent _evt)
  {
    BuLib.focusScroll(this,_evt);
    super.processFocusEvent(_evt);
  }
}
