/**
 * @modification $Date: 2006-09-19 14:35:05 $
 * @statut       unstable
 * @file         BuPreferencesCategory.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuSort;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

/**
 * A panel to introduce a category of preferences.
 */
public class BuPreferencesCategory
  extends BuPanel
{
  private static final Color  BG=BuLib.getColor(new Color(255,255,224));
  private static final Border BD=new CompoundBorder
    (UIManager.getBorder("Theme.border")!=null 
     ? UIManager.getBorder("Theme.border")
     : new LineBorder(BuLib.getColor(BG.darker()),1),BuBorders.EMPTY5555);

  private BuAbstractPreferencesPanel[] tabs_;

  private BuLabelInfo info_;
  private BuIcon      icon_;
  private String      title_;

  public BuPreferencesCategory(String _title,BuIcon _icon)
  {
    super(new BuBorderLayout(5,5));
    String title=_title;
    BuIcon icon=_icon;

    tabs_=new BuAbstractPreferencesPanel[0];

    if((title==null)||"".equals(title))
      title="";//_("Préférences");
    if((icon==null)||icon.isDefault())
      icon=BuResource.BU.getToolIcon("preference");

    title_=title;
    icon_ =icon;
    BuHeaderPanel hp=new BuHeaderPanel(title_,getS("Préférences"),icon_,false);
    hp.setBorder(BD);
    add(hp,BuBorderLayout.NORTH);

    info_=new BuLabelInfo("");
    add(info_,BuBorderLayout.CENTER);
  }

  public String toString()
  {
    return title_;
  }

  public boolean isDirty()
  {
    for(int i=0;i<tabs_.length;i++)
      if(tabs_[i].isDirty())
        return true;
    return false;
  }

  public BuAbstractPreferencesPanel[] getTabs()
  {
    return tabs_;
  }

  public int getTabCount()
  {
    return tabs_.length;
  }

  public BuAbstractPreferencesPanel getTab(int _index)
  {
    return ((_index>=0)&&(_index<tabs_.length))
      ? tabs_[_index]
      : null;
  }

  public void addTab(BuAbstractPreferencesPanel _newtab)
  {
    BuAbstractPreferencesPanel[] old=tabs_;
    int l=old.length;
    tabs_=new BuAbstractPreferencesPanel[l+1];
    System.arraycopy(old,0,tabs_,0,l);
    tabs_[l]=_newtab;
    FuSort.sort(tabs_);

    StringBuffer sb=new StringBuffer(1024);
    for(int i=0;i<tabs_.length;i++)
    {    
      String title  =tabs_[i].getTitle();
      String tooltip=tabs_[i].getToolTipText();
      sb.append(title);
      if(tooltip!=null)
      {
        sb.append(": ");
        sb.append(tooltip);
      }
      sb.append('\n');
    }
    info_.setText(sb.toString());

    revalidate();
  }

  public boolean isPreferencesValidable()
  {
    for(int i=0;i<tabs_.length;i++)
      if(tabs_[i].isPreferencesValidable())
        return true;
    return false;
  }

  public void validatePreferences()
  {
    for(int i=0;i<tabs_.length;i++)
      tabs_[i].validatePreferences();
  }

  public boolean isPreferencesApplyable()
  {
    for(int i=0;i<tabs_.length;i++)
      if(tabs_[i].isPreferencesApplyable())
        return true;
    return false;
  }

  public void applyPreferences()
  {
    for(int i=0;i<tabs_.length;i++)
      tabs_[i].applyPreferences();
  }

  public boolean isPreferencesCancelable()
  {
    for(int i=0;i<tabs_.length;i++)
      if(tabs_[i].isPreferencesCancelable())
        return true;
    return false;
  }

  public void cancelPreferences()
  {
    for(int i=0;i<tabs_.length;i++)
      tabs_[i].cancelPreferences();
  }
}
