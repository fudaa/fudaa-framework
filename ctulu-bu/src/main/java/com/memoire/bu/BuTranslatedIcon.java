/**
 * @modification $Date: 2006-09-19 14:35:11 $
 * @statut       unstable
 * @file         BuTranslatedIcon.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Shape;
import java.io.Serializable;
import javax.swing.Icon;

/**
 * An Icon but with fixed size.
 */

public class BuTranslatedIcon
    implements Icon, Serializable
{
  private Icon icon_;
  private int  dx_,dy_;

  public BuTranslatedIcon(Icon _icon, int _dx, int _dy)
  {
    icon_=_icon;
    dx_=_dx;
    dy_=_dy;
  }

  @Override
  public int getIconWidth ()
  { return Math.max(0,icon_.getIconWidth()+dx_); }

  @Override
  public int getIconHeight()
  { return Math.max(0,icon_.getIconHeight()+dy_); }

  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y)
  {
    Shape old=_g.getClip();
    _g.clipRect(_x,_y,getIconWidth(),getIconHeight());
    int x=_x+dx_;
    int y=_y+dy_;
    icon_.paintIcon(_c,_g,x,y);
    _g.setClip(old);
  }
}
