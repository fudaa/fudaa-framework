/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuFileViewer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

public class BuFileViewer
       extends BuPanel
       //implements ActionListener, ListSelectionListener
{
  BuFileRenderer renderer_=null;
  BuProgressBar  progress_=null;

  public BuFileRenderer getFileRenderer()
  {
    return renderer_;
  }

  public void setFileRenderer(BuFileRenderer _renderer)
  {
    renderer_=_renderer;
  }

  Runnable runnable_=null;

  public void updateContent(final Object[] _dirs, final Object[] _files)
  {
    if(renderer_==null) return;

    final Runnable r=new Runnable()
    {
      @Override
      public void run()
      {
	int ld=_dirs .length;
	int lf=_files.length;
	int lt=ld+lf;
	int pp=0; //previous pourcentage

	final Component[] v=new Component[lt];

	int n=0;

	for(int i=0;i<ld;i++)
	{
	  if(this!=runnable_) return;

	  Component c=buildView(_dirs[i],n);
	  if(c!=null)
	  {
	    v[n]=c;
	    n++;
	    //System.err.println("D"+n+" "+_dirs[i]);
	    if((progress_!=null)&&(progress_.getParent()==BuFileViewer.this))
	    {
	      final int cp=100*n/lt;
	      if(cp!=pp)
	      {
		pp=cp;
		SwingUtilities.invokeLater(new Runnable()
		{
		  @Override
      public void run()
		  {
		    progress_.setValue(cp);
		    //progress_.setString(cp+"%");
		    //progress_.repaint();
		  }
		});
	      }
	    }
	  }
	}

	for(int i=0;i<lf;i++)
	{
	  if(this!=runnable_) return;

	  Component c=buildView(_files[i],n);
	  if(c!=null)
	  {
	    v[n]=c;
	    n++;
	    //System.err.println("F"+n+" "+_files[i]);
	    if((progress_!=null)&&(progress_.getParent()==BuFileViewer.this))
	    {
	      final int cp=100*n/lt;
	      if(cp!=pp)
	      {
		pp=cp;
		SwingUtilities.invokeLater(new Runnable()
		{
		  @Override
      public void run()
		  {
		    progress_.setValue(cp);
		    //progress_.repaint();
		  }
		});
	      }
	    }
	  }
	}
	
	final int      count=n;
	final Runnable ref  =this;

	SwingUtilities.invokeLater(new Runnable()
	{
	  @Override
    public void run()
	  {
	    setVisible(false);

	    if((progress_!=null)&&(progress_.getParent()==BuFileViewer.this))
	      remove(progress_);

	    for(int i=0;i<count;i++)
	    {
	      if(ref!=runnable_) break;
	      add(v[i]);
	    }
	   

	    Container p=getParent();
	    if(p instanceof JViewport)
	      ((JViewport)p).setViewPosition(new Point(0,0));
	    revalidate();
	    //invalidate();
	    //doLayout();
	    //validate();
	    setVisible(true);
	    repaint();

	    if(ref==runnable_)
	      runnable_=null;
	  }
	});
      }
    };

    runnable_=r;
    removeAll();

    if(_dirs.length+_files.length>3)
    {
      if(progress_==null)
      {
	progress_=new BuProgressBar();
	Dimension ps=progress_.getPreferredSize();
	ps.width=200;
	progress_.setPreferredSize(ps);
	progress_.setSize(ps);
      }

      progress_.setValue(0);
      add(progress_);
      revalidate();
      repaint();
    }

    if(SwingUtilities.isEventDispatchThread())
    {
      Thread t=new Thread(r,"Explorer Content Updater");
      t.setPriority(Thread.NORM_PRIORITY-1);
      t.start();
    }
    else
    {
      r.run();
    }

    // micro wait to avoid flickering
    new Thread(new Runnable()
    {
      @Override
      public void run()
      {
	if(runnable_!=r) return;
	try { Thread.sleep(300L); }
	catch(InterruptedException ex) { }
	if(runnable_!=r) return;
	repaint();
      }
    },"Explorer Micro Wait").start();

    /*
      viewer_.setLayout(new BuGridLayout
      (3,//(Math.max(1,viewer_.getWidth()/200),
      2,2,true,true,false,false));
    */
  }

  protected Component buildView(Object _o, int _n)
  {
    Component r=null;

    try
    {
      r=renderer_.getPanelCellRendererComponent
	(null,_o,_n,false,false);
    }
    catch(Throwable th) { FuLog.error(th); }

    return r;
  }
}
