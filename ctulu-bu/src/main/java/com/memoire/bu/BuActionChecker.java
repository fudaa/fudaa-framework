/**
 * @modification $Date: 2006-09-19 14:35:06 $
 * @statut       unstable
 * @file         BuActionChecker.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.Component;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.MenuElement;

/**
 * An utility class to check/uncheck controls by their command name.
 * The command name is a string defined for each control
 * using setActionCommand().
 */

public class BuActionChecker
{

  // Menu

  public static void setCheckedForAction
    (JMenuBar _bar, String _action, boolean _state)
  {
    if(_bar==null) return;

    MenuElement[] c=_bar.getSubElements();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JMenu)
	setCheckedForAction((JMenu)c[i],_action,_state);
  }

  public static void setCheckedForAction
    (JMenu _menu, String _action, boolean _state)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
	setCheckedForAction((JMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JPopupMenu)
	setCheckedForAction((JPopupMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JCheckBoxMenuItem)
	setCheckedForAction((JCheckBoxMenuItem)c[i],_action,_state);
      else
      if(c[i] instanceof JRadioButtonMenuItem)
	setCheckedForAction((JRadioButtonMenuItem)c[i],_action,_state);
    }
  }

  public static void setCheckedForAction
    (JPopupMenu _menu, String _action, boolean _state)
  {
    if(_menu==null) return;

    MenuElement[] c=_menu.getSubElements();
    for(int i=0; i<c.length; i++)
    {
      if(c[i] instanceof JMenu)
	setCheckedForAction((JMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JPopupMenu)
	setCheckedForAction((JPopupMenu)c[i],_action,_state);
      else
      if(c[i] instanceof JCheckBoxMenuItem)
	setCheckedForAction((JCheckBoxMenuItem)c[i],_action,_state);
      else
      if(c[i] instanceof JRadioButtonMenuItem)
	setCheckedForAction((JRadioButtonMenuItem)c[i],_action,_state);
    }
  }

  public static void setCheckedForAction
    (JCheckBoxMenuItem _item, String _action, boolean _state)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setSelected(_state);
  }

  public static void setCheckedForAction
    (JRadioButtonMenuItem _item, String _action, boolean _state)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setSelected(_state);
  }

  // Tool

  public static void setCheckedForAction
    (JToolBar _bar, String _action, boolean _state)
  {
    if(_bar==null) return;

    Component[] c=_bar.getComponents();
    for(int i=0; i<c.length; i++)
      if(c[i] instanceof JToggleButton)
	setCheckedForAction((JToggleButton)c[i],_action,_state);
  }

  public static void setCheckedForAction
    (JToggleButton _item, String _action, boolean _state)
  {
    if(_item==null) return;

    if(_item.getActionCommand().equals(_action))
      _item.setSelected(_state);
  }
}
