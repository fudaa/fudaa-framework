/**
 * @modification $Date: 2006-09-19 14:35:07 $
 * @statut       unstable
 * @file         BuAbstractPreferencesPanel.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.bu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * An abstract root class for panels attended to be added in the
 * BuPreferencesFrame.
 */
public abstract class BuAbstractPreferencesPanel
  extends BuPanel
  implements BuBorders, KeyListener //BuPreferencesInterface
{
  private boolean dirty_=false;

  protected BuAbstractPreferencesPanel()
  {
  }

  public String toString()
  {
    return getTitle();
  }

  public abstract String getTitle();
  //public String getToolTipText() { return null; }
  public String getCategory() { return getS("G�n�ral"); }

  protected void updateAbility() { }

  public final void setAbility() { } // TMP DEBUG

  public boolean isDirty() { return dirty_; }

  public void setDirty(boolean _dirty)
  {
    if(dirty_!=_dirty)
    {
      boolean old=dirty_;
      dirty_=_dirty;
      firePropertyChange("dirty",old,dirty_);
    }
  }

  /**
   * @return true : Les pr�f�rences peuvent �tre ecrites. false sinon.
   */
  public boolean isPreferencesValidable() { return false; }
  
  /**
   * A surcharger si les pr�f�rences peuvent �tre ecrites.
   */
  public void validatePreferences() { }

  /**
   * @return true : Les pr�f�rences peuvent �tre appliqu�es sans sortie du dialogue.
   * false sinon.
   */
  public boolean isPreferencesApplyable() { return false; }
  
  /**
   * A surcharger si les pr�f�rences peuvent �tre appliqu�es.
   */
  public void applyPreferences() { }

  /**
   * @return true : Les pr�f�rences peuvent �tre annul�es.
   * false sinon.
   */
  public boolean isPreferencesCancelable() { return false; }
  
  /**
   * A surcharger si les pr�f�rences peuvent �tre annul�es.
   */
  public void cancelPreferences() { }

  @Override
  public void keyPressed (KeyEvent _evt) { }
  @Override
  public void keyReleased(KeyEvent _evt) { }

  @Override
  public void keyTyped(KeyEvent _evt)
  {
    setDirty(true);
  }
}
