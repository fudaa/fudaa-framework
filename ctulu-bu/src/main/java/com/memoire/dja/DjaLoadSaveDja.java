/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaLoadSaveDja.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuFileFilter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class DjaLoadSaveDja
       implements DjaLoadSaveInterface
{
  @Override
  public boolean canLoad() { return true; }
  @Override
  public boolean canSave() { return true; }

  @Override
  public DjaVector load(File _file)
    throws Exception
  {
    return DjaLoader.loadAsText
      (new BufferedInputStream(new FileInputStream(_file)));
  }

  @Override
  public void save(File _file, DjaVector _vector)
    throws Exception
  {
    DjaSaver.saveAsText
      (_vector,new BufferedOutputStream(new FileOutputStream(_file)));
  }

  @Override
  public BuFileFilter getFilter()
  {
    return new BuFileFilter
      (new String[] { "dja","DJA" },
      "Dja files");
  }
}
