/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaLabel.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class DjaLabel
       extends DjaForm
{
  private transient Dimension size_;

  public DjaLabel()
  {
    this(null);
  }

  public DjaLabel(String _text)
  {
    super(_text);

    DjaText t0=new DjaText(this,0,_text,true,0,0,
			   CENTER,0,LEFT,null,
			   null,false,false);
    setTextArray(new DjaText[] { t0 });

    super.setWidth(41);
    super.setHeight(21);
    textChanged(t0);
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x,y);
    r[1]=new DjaAnchor(this,1,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,NORTH,x+w,y);
    r[3]=new DjaAnchor(this,3,EAST ,x+w,y+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w,y+h);
    r[5]=new DjaAnchor(this,5,SOUTH,x+w/2,y+h);
    r[6]=new DjaAnchor(this,6,SOUTH,x,y+h);
    r[7]=new DjaAnchor(this,7,WEST ,x,y+h/2);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    // A ajuster selon alignement...

    DjaHandle[] r=new DjaHandle[8];
    r[0]=new DjaHandle(this,NORTH     ,x+w/2       ,y    -deltaY);
    r[2]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[4]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);
    r[6]=new DjaHandle(this,WEST      ,x    -deltaX,y+h/2       );
    r[1]=new DjaHandle(this,NORTH_EAST,x+w-1+deltaX,y    -deltaY);
    r[3]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[5]=new DjaHandle(this,SOUTH_WEST,x    -deltaX,y+h-1+deltaY);
    r[7]=new DjaHandle(this,NORTH_WEST,x    -deltaX,y    -deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    Color fg=getForeground();
    if(fg==null) fg=Color.black;
    _g.setColor(fg);

    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    for(int i=1;i<=w;i+=4)
    {
      _g.drawLine(x+i,y  ,x+i,y  );
      _g.drawLine(x+i,y+h,x+i,y+h);
    }

    for(int j=1;j<=h;j+=4)
    {
      _g.drawLine(x  ,y+j,x  ,y+j);
      _g.drawLine(x+w,y+j,x+w,y+j);
    }

    super.paintInteractive(_g);
  }

  private Dimension optimalSize()
  {
    Dimension d0=getTexts()[0].getSize();
    return new Dimension(d0.width+8,d0.height+8);
  }
}

