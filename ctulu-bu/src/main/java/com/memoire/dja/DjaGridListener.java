/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaGridListener.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;



public interface DjaGridListener
{
  void objectAdded       (DjaGridEvent _evt);
  void objectRemoved     (DjaGridEvent _evt);
  void objectConnected   (DjaGridEvent _evt);
  void objectDisconnected(DjaGridEvent _evt);
  void objectSelected    (DjaGridEvent _evt);
  void objectUnselected  (DjaGridEvent _evt);
  void objectModified    (DjaGridEvent _evt);
}
