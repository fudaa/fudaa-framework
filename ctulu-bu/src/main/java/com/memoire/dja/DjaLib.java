/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut       unstable
 * @file         DjaLib.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuFontDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.fu.FuHashtableFast;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Enumeration;
import javax.swing.JColorChooser;

public class DjaLib
       implements DjaOptions
{
   static BuInformationsSoftware SOFTWARE;

  public static final boolean close(Point p,Point q)
  {
    return (Math.abs(p.x-q.x)<=closeX)&&(Math.abs(p.y-q.y)<=closeY);
  }

  public static final boolean close(int x1, int y1, int x2, int y2)
  {
    return (Math.abs(x1-x2)<=closeX)&&(Math.abs(y1-y2)<=closeY);
  }

  public static final void snap(Point _p)
  {
    if(!snap) return;

    int x=_p.x+deltaX/2;
    int y=_p.y+deltaY/2;
    _p.x=x-x%deltaX;
    _p.y=y-y%deltaY;
  }

  public static final void snap(DjaObject _f)
  {
    if(!snap) return;

    int x=_f.getX()+deltaX/2;
    int y=_f.getY()+deltaY/2;
    x-=x%deltaX;
    y-=y%deltaY;
    _f.setX(x);
    _f.setY(y);

    int w=_f.getWidth() +deltaX/2;
    int h=_f.getHeight()+deltaY/2;
    int dw=x%deltaX; //_f.getX()%deltaX;
    int dh=y%deltaY; //_f.getY()%deltaY;
    if(dw>0) dw=deltaX-dw;
    if(dh>0) dh=deltaY-dh;
    _f.setWidth (w-w%deltaX+dw);
    _f.setHeight(h-h%deltaY+dh);
  }

  public static final boolean close
    (int ox,int oy,int ex, int ey, int px,int py)
  {
    int bx=ex-ox;
    int by=ey-oy;

    if((bx==0)&&(by==0)) return close(ox,oy,px,py);

    int t=0;

    if(Math.abs(bx)>Math.abs(by))
      t=(12800*(px-ox))/bx;
    else
      t=(12800*(py-oy))/by;

    if(t<0) t=0;
    if(t>12800) t=12800;
    int x=ox+(t*bx)/12800;
    int y=oy+(t*by)/12800;

    return close(x,y,px,py);
  }

  public static final DjaVector clone(DjaVector s)
  {
    FuHashtableFast c=new FuHashtableFast(s.size());

    for(Enumeration e=s.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      try { c.put(o,o.clone()); }
      catch(CloneNotSupportedException ex) { }
    }

    for(Enumeration e=c.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      if(o instanceof DjaLink)
      {
	DjaLink   a=(DjaLink)o;
	DjaObject t;

	t=a.getBeginObject();
	if(t!=null) t=(DjaObject)c.get(t);
	a.setBeginObject(t);

	t=a.getEndObject();
	if(t!=null) t=(DjaObject)c.get(t);
	a.setEndObject(t);
      }
    }

    DjaVector r=new DjaVector();
    for(Enumeration e=c.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      r.addElement(o);
    }

    return r;
  }

  public static final Rectangle getDirtyArea(DjaObject o)
  {
    Rectangle r=o.getExtendedBounds();
    DjaLink[] bc=o.getBeginConnections();
    //System.err.println(bc.length+" begin connections");
    for(int i=0;i<bc.length;i++)
      r=bc[i].getExtendedBounds().union(r);
    DjaLink[] ec=o.getEndConnections();
    //System.err.println(ec.length+" end connections");
    for(int i=0;i<ec.length;i++)
      r=ec[i].getExtendedBounds().union(r);
    return r;
  }

  public static final void setForeground(DjaVector _v, Color _c)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      f.setForeground(_c);
    }
  }

  public static final void setBackground(DjaVector _v, Color _c)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      f.setBackground(_c);
    }
  }

  public static final void setColor(DjaVector _v, Color _c)
  {
    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      DjaText[] texts=f.getTexts();
      int n=0;
      for(int i=0;i<texts.length;i++)
	if(texts[i].isSelected())
	  { n++; texts[i].setColor(_c); }
      if(n==0) f.setColor(_c);
    }
  }

  public static final void setFont(DjaVector _v, Font _f)
  {
    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      DjaText[] texts=f.getTexts();
      int n=0;
      for(int i=0;i<texts.length;i++)
	if(texts[i].isSelected())
	  { n++; texts[i].setFont(_f); }
      if(n==0) f.setFont(_f);
    }
  }

  public static final void resizeSelection(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      for(Enumeration e=s.elements(); e.hasMoreElements(); )
      {
	DjaObject f=(DjaObject)e.nextElement();
	f.setWidth(2*deltaX);
	f.setHeight(2*deltaY);
      }
    }
  }

  public static final void setForeground(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      Color fg=Color.black;
      fg=JColorChooser.showDialog(_g,"Foreground",fg);
      if(fg!=null)
	  setForeground(s,fg);
    }
  }

  public static final void setBackground(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      Color bg=Color.white;
      bg=JColorChooser.showDialog(_g,"Background",bg);
      if(bg!=null)
	  setBackground(s,bg);
    }
  }

  public static final void setColor(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      Color tc=Color.black;
      tc=JColorChooser.showDialog(_g,"Text Color",tc);
      if(tc!=null)
	  setColor(s,tc);
    }
  }

  public static final void setFont(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      Font tf=defaultPlainFont;
      BuFontDialog d=new BuFontDialog(null,DjaLib.SOFTWARE,"Text Font",tf);
      d.activate();
      tf=d.getValue();
      if(tf!=null)
	  setFont(s,tf);
    }
  }

  public static final void addText(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      for(Enumeration e=s.elements(); e.hasMoreElements(); )
      {
	DjaObject f=(DjaObject)e.nextElement();
	f.addText((String)null);
      }
    }
  }

  public static final void removeText(DjaGridInteractive _g)
  {
    DjaVector s=_g.getSelection();
    if(s.size()>0)
    {
      for(Enumeration e=s.elements(); e.hasMoreElements(); )
      {
	DjaObject f=(DjaObject)e.nextElement();
	DjaText[] t=f.getTextArray();
	int n=0;
	for(int i=0;i<t.length;i++)
	    if(t[i].isSelected()) n++;
	DjaText[] r=new DjaText[t.length-n];
	n=0;
	for(int i=0;i<t.length;i++)
	    if(!t[i].isSelected())
	    {
	      r[n]=t[i];
	      r[n].setNum(n);
	      n++;
	    }
	f.setTextArray(r);
      }
    }
  }

  public static final void setProperty(DjaVector _v, String _n, String _s)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      f.putProperty(_n,_s);
    }
  }

  public static final void setBeginType(DjaVector _v, int _t)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      if(f instanceof DjaLink)
	((DjaLink)f).setBeginType(_t);
    }
  }

  public static final void setEndType(DjaVector _v, int _t)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      if(f instanceof DjaLink)
	((DjaLink)f).setEndType(_t);
    }
  }

  public static final void setBeginO(DjaVector _v, int _o)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      if(f instanceof DjaLink)
	((DjaLink)f).setBeginO(_o);
    }
  }

  public static final void setEndO(DjaVector _v, int _o)
  {
    if(_v==null) return;

    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      if(f instanceof DjaLink)
	((DjaLink)f).setEndO(_o);
    }
  }

  /*
    // test contain
    _g.setColor(new Color(224,192,128));
    int xx=getX();
    int yy=getY();
    for(int i=0;i<getWidth();i++)
      for(int j=0;j<getHeight(); j++)
	if(contains(xx+i,yy+j))
	  _g.drawLine(xx+i,yy+j,xx+i,yy+j);
  */


   static final String[] DJA_CLASSES=new String[]
  {
    "com.memoire.dja.DjaObject",
    "com.memoire.dja.DjaLink",
    "com.memoire.dja.DjaPaletteBracket",
    "com.memoire.dja.DjaPaletteThickness",
    "com.memoire.dja.DjaGraphics",
    "com.memoire.dja.DjaPaletteStroke",
    "com.memoire.dja.DjaPaletteUml",
    "com.memoire.dja.DjaPaletteLink",
    "com.memoire.dja.DjaPaletteForm",
    "com.memoire.dja.DjaPreferences",
    "com.memoire.dja.DjaResource",
    "com.memoire.dja.DjaResource",
    "com.memoire.dja.DjaPreferences",
    "com.memoire.dja.DjaPaletteForm",
    "com.memoire.dja.DjaPaletteLink",
    "com.memoire.dja.DjaPaletteBracket",
    "com.memoire.dja.DjaGridInteractive",
    "com.memoire.dja.DjaGrid",
    "com.memoire.dja.DjaMouseListener",
    "com.memoire.dja.DjaKeyListener",
    "com.memoire.dja.DjaFrame",
    "com.memoire.dja.DjaLoader",
    "com.memoire.dja.DjaLink",
    "com.memoire.dja.DjaBrokenArrow",
    "com.memoire.dja.DjaGroup",
    "com.memoire.dja.DjaDiamond",
    "com.memoire.dja.DjaObject",
    "com.memoire.dja.DjaForm",
    "com.memoire.dja.DjaBox",
    "com.memoire.dja.DjaAnchor",
    "com.memoire.dja.DjaOptions",
    "com.memoire.dja.DjaLib",
    "com.memoire.dja.DjaImplementation",
    "com.memoire.dja.Dja",
  };
}
