/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaFont.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Font;
import java.io.Serializable;

public class DjaFont
  implements DjaOptions, Cloneable, Serializable
{
    private String family_;
    private int    style_;
    private int    size_;

    private transient Font font_;

    public DjaFont()
    {
	this(defaultPlainFont);
    }

    public DjaFont(String _family, int _style, int _size)
    {
	this(new Font(_family,_style,_size));
    }

    public DjaFont(Font _font)
    {
	font_  =_font;
	family_=_font.getFamily();
	style_ =_font.getStyle();
	size_  =_font.getSize();
    }

    public Font getFont()
    {
	if(font_==null) font_=new Font(family_,style_,size_);
	return font_;
    }
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
      DjaFont res=(DjaFont)super.clone();
      res.family_=family_;
      res.style_=style_;
      res.size_=size_;
      res.font_=font_;
      return res;
    }
}
