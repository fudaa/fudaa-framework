/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaAttach.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;

public class DjaAttach
       extends DjaManipulator
{
  private int p_;
  private int o_;
  private int x_;
  private int y_;

  private DjaLink   parent_;
  private DjaObject target_;

  public DjaAttach(DjaLink _parent,int _p,DjaObject _target,
		   int _o,int _x,int _y)
  {
    p_=_p;
    o_=_o;
    x_=_x;
    y_=_y;
    parent_=_parent;
    target_=_target;
  }

  @Override
  public final DjaOwner getOwner()
  { return getParent(); }

  public int getP() { return p_; }
  public int getO() { return o_; }
  public int getX() { return x_; }
  public int getY() { return y_; }

  public DjaLink   getParent() { return parent_; }
  public DjaObject getTarget() { return target_; }

  public void paint(Graphics _g)
  {
    int x=getX();
    int y=getY();

    // System.err.println("target="+target_);

    if(target_!=null) _g.setColor(attachsColor);
    else              _g.setColor(freesColor);

    if(parent_.isSelected())
      _g.fillRect(x-3,y-3,7,7);
    else
      _g.fillRect(x-1,y-1,3,3);
  }
}

