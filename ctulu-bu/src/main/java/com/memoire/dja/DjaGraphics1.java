/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaGraphics.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;
import java.awt.Polygon;

/**
 * Implements some features of Graphics2D for JDK1.1.
 */
public class DjaGraphics1
  extends DjaGraphics0
{
  @Override
  public void drawLine
    (Graphics _g, int _x1, int _y1, int _x2, int _y2, DjaGraphics.BresenhamParams _bp)
  {
    bresenham(_g,_x1,_y1,_x2,_y2,_bp);
    bresenham(_g,_x2,_y2,_x2,_y2,_bp);
  }

  @Override
  public void drawRect
    (Graphics _g, int _x, int _y, int _w, int _h, DjaGraphics.BresenhamParams _bp)
  {
    bresenham(_g,_x   ,_y+_h,_x   ,_y   ,_bp);
    bresenham(_g,_x   ,_y   ,_x+_w,_y   ,_bp);
    bresenham(_g,_x+_w,_y   ,_x+_w,_y+_h,_bp);
    bresenham(_g,_x+_w,_y+_h,_x   ,_y+_h,_bp);
  }

  private Polygon getRoundRect
    (int _x, int _y, int _w, int _h, int _rh, int _rv)
  {
    double CX=2.;
    double CY=2.;
    Polygon p,r;
    int rh=_rh;
    int rv=_rv;

    if(rh<1) rh=1;
    if(rv<1) rv=1;
    r=new Polygon();

    p=DjaMatrixHermite.arc2polyline
      (_x,_y+rv,_x+rh,_y,0.,-rv*CX,rh*CY,0.);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    p=DjaMatrixHermite.arc2polyline
      (_x+_w-rh,_y,_x+_w,_y+rv,rh*CX,0.,0.,rv*CY);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    p=DjaMatrixHermite.arc2polyline
      (_x+_w,_y+_h-rv,_x+_w-rh,_y+_h,0.,rv*CX,-rh*CY,0.);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    p=DjaMatrixHermite.arc2polyline
      (_x+rh,_y+_h,_x,_y+_h-rv,-rh*CX,0.,0.,-rv*CY);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    return r;
  }

  @Override
  public void drawRoundRect
    (Graphics _g, int _x, int _y, int _w, int _h,
     int _rh, int _rv, DjaGraphics.BresenhamParams _bp)
  {
    DjaGraphics.drawPolygon(_g,getRoundRect(_x,_y,_w,_h,_rh,_rv),_bp);
  }

  @Override
  public void fillRoundRect
    (Graphics _g, int _x, int _y, int _w, int _h,
     int _rh, int _rv)
  {
    DjaGraphics.fillPolygon(_g,getRoundRect(_x,_y,_w,_h,_rh,_rv));
  }

  private Polygon getOval
    (int _x, int _y, int _w, int _h)
  {
    double CX=1.58;
    double CY=1.58;
    Polygon p,r;

    int _rv=_h/2;
    int _rh=_w/2;
    r=new Polygon();

    p=DjaMatrixHermite.arc2polyline
      (_x,_y+_rv,_x+_rh,_y,0.,-_rv*CX,_rh*CY,0.);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    p=DjaMatrixHermite.arc2polyline
      (_x+_w-_rh,_y,_x+_w,_y+_rv,_rh*CX,0.,0.,_rv*CY);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    p=DjaMatrixHermite.arc2polyline
      (_x+_w,_y+_h-_rv,_x+_w-_rh,_y+_h,0.,_rv*CX,-_rh*CY,0.);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    p=DjaMatrixHermite.arc2polyline
      (_x+_rh,_y+_h,_x,_y+_h-_rv,-_rh*CX,0.,0.,-_rv*CY);
    for(int i=0;i<p.npoints;i++) r.addPoint(p.xpoints[i],p.ypoints[i]);

    return r;
  }

  @Override
  public void drawOval
    (Graphics _g, int _x, int _y, int _w, int _h, DjaGraphics.BresenhamParams _bp)
  {
      DjaGraphics.drawPolygon(_g,getOval(_x,_y,_w,_h),_bp);
  }

  @Override
  public void fillOval
    (Graphics _g, int _x, int _y, int _w, int _h)
  {
      DjaGraphics.fillPolygon(_g,getOval(_x,_y,_w,_h));
  }

  @Override
  public void drawPolyline
    (Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints,
     DjaGraphics.BresenhamParams _bp)
  {
    int n=_npoints;
    for(int i=1;i<n; i++)
      bresenham(_g,_xpoints[i-1],_ypoints[i-1],
		_xpoints[i  ],_ypoints[i  ],_bp);
    if(n>0) drawPoint(_g,_xpoints[n-1],_ypoints[n-1],_bp);
  }

  @Override
  public void drawPolygon
    (Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints,
     DjaGraphics.BresenhamParams _bp)
  {
      int i;
      for(i=1;i<_npoints; i++)
	bresenham(_g,_xpoints[i-1],_ypoints[i-1],
		     _xpoints[i  ],_ypoints[i  ],_bp);
      if(i>=2) bresenham(_g,_xpoints[i-1],_ypoints[i-1],
			 _xpoints[0  ],_ypoints[0  ],_bp);
  }

  public void bresenham
    (Graphics _g, int x1, int y1, int x2, int y2, DjaGraphics.BresenhamParams bp)
  {
    if((bp.vide_==0)&&(bp.epaisseur_==1))
    {
      _g.drawLine(x1,y1,x2,y2);
      return;
    }

    int xprec=x1;
    int yprec=y1;
    int c, /*deltaX, deltaY,*/ x, y, incrX, incrY, i;
    /*deltaX=Math.abs(x2-x1);
    deltaY=Math.abs(y2-y1);*/
    if( (y2-y1)>0 ) incrY=1;
    else incrY=-1;
    if( (x2-x1)>0 ) incrX=1;
    else incrX=-1;
    if( deltaX>deltaY ) {
      c=2*deltaY-deltaX;
      y=y1;
      x=x1;
      for(i=0; i<deltaX; i++) {
        if( bp.trace_ ) {
          if( bp.compteur_<bp.trait_ ) {
            // _g.drawLine(xprec, yprec, x, y);
	    drawPoint(_g,xprec,yprec,x,y,bp);
            bp.compteur_++;
          }
          else {
            bp.compteur_=0;
            bp.trace_=(bp.vide_<=0); // @GDX false;
          }
        } else {
          if( bp.compteur_<bp.vide_ ) bp.compteur_++;
          else {
            bp.compteur_=0;
            xprec=x;
            yprec=y;
            bp.trace_=true;
          }
        }
        x+=incrX;
        if( c<0 ) c+=(2*deltaY);
        else {
          y+=incrY;
          c+=(2*deltaY-2*deltaX);
        }
      }
    } else {
      c=2*deltaX-deltaY;
      x=x1;
      y=y1;
      for(i=0; i<deltaY; i++) {
        if( bp.trace_ ) {
          if( bp.compteur_<bp.trait_ ) {
            // _g.drawLine(xprec, yprec, x, y);
	    drawPoint(_g,xprec,yprec,x,y,bp);
            bp.compteur_++;
          }
          else {
            bp.compteur_=0;
            bp.trace_=(bp.vide_<=0); // @GDX false;
          }
        } else {
          if( bp.compteur_<bp.vide_ ) bp.compteur_++;
          else {
            bp.compteur_=0;
            xprec=x;
            yprec=y;
            bp.trace_=true;
          }
        }
        y+=incrY;
        if( c<0 ) c+=(2*deltaX);
        else {
          x+=incrX;
          c+=(2*deltaX-2*deltaY);
        }
      }
    }
  }
}
