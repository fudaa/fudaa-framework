/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaGridEvent.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.util.EventObject;

public class DjaGridEvent
       extends EventObject
{
    public static final int ADDED       =1;
    public static final int REMOVED     =2;
    public static final int CONNECTED   =3;
    public static final int DISCONNECTED=4;
    public static final int SELECTED    =5;
    public static final int UNSELECTED  =6;
    public static final int MODIFIED    =7;

    private int      id_;
    private DjaOwner object_;

    public DjaGridEvent(DjaGrid _grid,DjaOwner _object,int _id)
    {
	super(_grid);
	object_=_object;
	id_    =_id;
    }

    public DjaGrid getGrid()
    {
	return (DjaGrid)getSource();
    }

    public int getID()
    {
	return id_;
    }

    public DjaOwner getObject()
    {
	return object_;
    }

    public String toString()
    {
      String r=""+getObject()+" ";

      switch(getID())
      {
	case DjaGridEvent.ADDED       : r+="added"; break;
	case DjaGridEvent.REMOVED     : r+="removed"; break;
        case DjaGridEvent.CONNECTED   : r+="connected"; break;
	case DjaGridEvent.DISCONNECTED: r+="disconnected"; break;
	case DjaGridEvent.SELECTED    : r+="selected"; break;
	case DjaGridEvent.UNSELECTED  : r+="unselected"; break;
        case DjaGridEvent.MODIFIED    : r+="modified"; break;
      }

      return r+" ("+getSource()+")";
    }
}
