/*
 GPL 2
 */
package com.memoire.dja;

import java.awt.event.MouseEvent;

/**
 *
 * @author Frederic Deniger
 */
public class DjaTransformData {

  private float scale = 1;

  private int translateX = 0;
  private int translateY = 0;

  public int getTranslateX() {
    return translateX;
  }

  public void clear() {
    scale = 1;
    translateX = 0;
    translateY = 0;
  }

  public void setTranslateX(int translateX) {
    this.translateX = translateX;
  }

  public int getTranslateY() {
    return translateY;
  }

  public void setTranslateY(int translateY) {
    this.translateY = translateY;
  }

  public float getScale() {
    return scale;
  }

  public void setScale(float scale) {
    System.out.println(scale + "and scale global=" + this.scale);

    this.scale = this.scale - scale;
    if (this.scale < 0.9f) {
      this.scale = 0.9f;
    }
  }

  public int getScaledX(MouseEvent _evt) {
    return getScaledX(_evt, true);
  }

  public int getScaledY(MouseEvent _evt) {
    return getScaledY(_evt, true);
  }

  public int getScaledX(MouseEvent _evt, boolean applyTranslation) {
    final int x = _evt.getX();
    return getScaledX(applyTranslation, x);
  }

  protected int getScaledX(boolean applyTranslation, final int x) {
    if (applyTranslation) {
      if (scale == 0) {
        return x - getTranslateX();
      } else {
        return (int) ((x) / scale) - getTranslateX();
      }
    }
    if (scale == 0) {
      return x;
    } else {
      return (int) ((x) / scale);
    }
  }

  public int getUnScaledX(int x, boolean applyTranslation) {
    if (applyTranslation) {
      if (scale == 0) {
        return x + getTranslateX();
      } else {
        return (int) ((x + getTranslateX()) * scale);
      }
    }
    if (scale == 0) {
      return x;
    } else {
      return (int) (x * scale);
    }
  }

  public int getUnScaledY(int y, boolean applyTranslation) {
    if (applyTranslation) {
      if (scale == 0) {
        return y + getTranslateY();
      } else {
        return (int) ((y + getTranslateY()) * scale);
      }
    }
    if (scale == 0) {
      return y;
    } else {
      return (int) (y * scale);
    }
  }

  public int getScaledY(MouseEvent _evt, boolean applyTranslation) {
    final int y = _evt.getY();
    return getScaledY(applyTranslation, y);
  }

  protected int getScaledY(boolean applyTranslation, final int y) {
    if (applyTranslation) {
      if (scale == 0) {
        return y - getTranslateY();
      } else {
        return (int) ((y) / scale) - getTranslateY();
      }
    }
    if (scale == 0) {
      return y;
    } else {
      return (int) ((y) / scale);
    }
  }

  public void zoom(MouseEvent evt, boolean out) {
    int initX = getScaledX(evt);
    int initY = getScaledY(evt);
    if (out) {
      scale = scale * 1.1f;
    } else {
      scale = scale * 0.9f;
    }
    int newX = getScaledX(evt);
    int newY = getScaledY(evt);
    translateX = translateX + (newX - initX);
    translateY = translateY + (newY - initY);
  }

  public void zoom(int startScreenX, int startScreenY, int endScreenX, int endScreenY, int widthScreen, int heightScreen) {
    if (scale == 0) {
      return;
    }
    double wRatio = widthScreen / Math.abs((endScreenX - startScreenX) / scale);
    double hRatio = heightScreen / Math.abs((endScreenY - startScreenY) / scale);
    int x = getScaledX(true, startScreenX);
    int y = getScaledY(true, startScreenY);
    translateX = 0;
    translateY = 0;
    scale = (int) Math.min(wRatio, hRatio);
//    x = getUnScaledX(x, true);
//    y = getUnScaledY(y, true);
    translateX = -(int) (x);
    translateY = -(int) (y);

  }

}
