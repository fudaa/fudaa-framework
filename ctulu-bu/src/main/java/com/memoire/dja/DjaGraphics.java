/**
 * @modification $Date: 2007-03-15 16:58:56 $
 * @statut       unstable
 * @file         DjaGraphics.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuLib;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import javax.swing.text.html.HTMLDocument;

/**
 * Implements some features of Graphics2D for JDK1.1.
 */
public class DjaGraphics implements DjaOptions {
  public static final class BresenhamParams {
    public int trait_, vide_;
    public int epaisseur_;

    int compteur_;
    boolean trace_;

    public BresenhamParams() {
      this(1, 0, 1);
    }

    public BresenhamParams(int _trait, int _vide) {
      this(_trait, _vide, 1);
    }

    public BresenhamParams(int _trait, int _vide, int _epaisseur) {
      trait_ = _trait;
      vide_ = _vide;
      epaisseur_ = _epaisseur;
      compteur_ = 0;
      trace_ = true;
    }
  }

  private static final DjaGraphics0 INSTANCE = createInstance();

  private static final DjaGraphics0 createInstance() {
    DjaGraphics0 r = null;
    try {
      String n = "com.memoire.dja.DjaGraphics" + ((FuLib.jdk() >= 1.2) ? "2" : "1");
      Class c = DjaGraphics.class.getClassLoader().loadClass(n);
      r = (DjaGraphics0) c.newInstance();
    } catch (Throwable th) {
      r = new DjaGraphics1();
    }
    return r;
  }

  public static BresenhamParams getBresenhamParams(DjaObject _o) {
    String trace = _o.getProperty("trace");
    String epaisseur = _o.getProperty("epaisseur");

    BresenhamParams bp = new BresenhamParams();

    if (trace != null) {
      int i = trace.indexOf(".");
      bp.trait_ = Integer.parseInt(trace.substring(0, i));
      bp.vide_ = Integer.parseInt(trace.substring(i + 1));
    }

    if (epaisseur != null) {
      bp.epaisseur_ = Integer.parseInt(epaisseur);
    }

    return bp;
  }

  // color

  public static Color getColor(Graphics _g) {
    return _g.getColor();
  }

  public static void setColor(Graphics _g, Color _c) {
    _g.setColor(_c);
  }

  // text

  public static FontMetrics getFontMetrics(DjaObject _o, Font _font) {
    DjaGrid g = null;
    if (_o != null) g = _o.getGrid();
    return BuLib.getFontMetrics(g, _font);
  }

  public static Dimension getTextSize(String _text, Font _font) {
    if ((_text == null) || "".equals(_text)) return new Dimension(23, 23);
    String text = _text;

    FontMetrics fm = getFontMetrics(null, _font);
    int w = 0;
    int h = 0;
    int hs = _font.getSize();

    while (!text.equals("")) {
      int i = text.indexOf("\n");
      String s;
      if (i >= 0) {
        s = text.substring(0, i);
        text = text.substring(i + 1);
      } else {
        s = text;
        text = "";
      }
      w = Math.max(w, fm.stringWidth(s));
      h += hs;
    }

    return new Dimension(w + 2, h + 4); // (w+9,h+7);
  }

  public static void drawText(Graphics _g, String _text, int _x, int _y) {
    drawText(_g, _text, _x, _y, 0, 0, false);
  }

  public static void drawText(Graphics _g, String _text, int _x, int _y, int _a, int _w) {
    drawText(_g, _text, _x, _y, _a, _w, false);
  }

  public static View drawHtmlText(Graphics _g, JComponent _parent, String _text, Rectangle _bounds) {
    if (_text == null || _text.equals("")) {
      return null;
    }
    View v = BasicHTML.createHTMLView(_parent, _text);
    HTMLDocument doc=(HTMLDocument)v.getDocument();
    doc.getStyleSheet().addRule("body { color: #000000,padding:2px}");
    v.paint(_g, _bounds);
    return v;

  }

  public static void drawText(Graphics _g, String _text, int _x, int _y, int _a, int _w, boolean _underlined) {
    if ((_text == null) || "".equals(_text)) return;
    String text = _text;

    FontMetrics fm = _g.getFontMetrics();
    Font ft = _g.getFont();
    int x = _x;
    int hs = ft.getSize();
    int y = _y + hs;

    while (!text.equals("")) {
      int i/* ,j */;
      String s;

      i = text.indexOf("\n");
      if (i >= 0) {
        s = text.substring(0, i);
        text = text.substring(i + 1);
      } else {
        s = text;
        text = "";
      }

      int ws = fm.stringWidth(s);

      x = _x;
      if (_a == 0) x += 2;
      if (_a == 1) x += (_w - ws) / 2;
      if (_a == 2) x += (_w - ws) - 2;

      _g.drawString(s, x, y);
      if (_underlined) _g.drawLine(x, y + 1, x + ws, y + 1);
      y += hs;
    }
  }

  // draw

  public static void drawPoint(Graphics _g, int _x, int _y, BresenhamParams _bp) {
    INSTANCE.drawPoint(_g, _x, _y, _bp);
  }

  public static void drawPoint(Graphics _g, int _x1, int _y1, int _x2, int _y2, BresenhamParams _bp) {
    INSTANCE.drawPoint(_g, _x1, _y1, _x2, _y2, _bp);
  }

  public static void drawLine(Graphics _g, int _x1, int _y1, int _x2, int _y2, BresenhamParams _bp) {
    INSTANCE.drawLine(_g, _x1, _y1, _x2, _y2, _bp);
  }

  public static void drawRect(Graphics _g, int _x, int _y, int _w, int _h, BresenhamParams _bp) {
    INSTANCE.drawRect(_g, _x, _y, _w, _h, _bp);
  }

  public static void fillRect(Graphics _g, int _x, int _y, int _w, int _h) {
    INSTANCE.fillRect(_g, _x, _y, _w, _h);
  }

  public static void drawRoundRect(Graphics _g, int _x, int _y, int _w, int _h, int _rh, int _rv, BresenhamParams _bp) {
    if ((_rh == 0) && (_rv == 0)) INSTANCE.drawRect(_g, _x, _y, _w, _h, _bp);
    else
      INSTANCE.drawRoundRect(_g, _x, _y, _w, _h, _rh, _rv, _bp);
  }

  public static void fillRoundRect(Graphics _g, int _x, int _y, int _w, int _h, int _rh, int _rv) {
    if ((_rh == 0) && (_rv == 0)) INSTANCE.fillRect(_g, _x, _y, _w, _h);
    else
      INSTANCE.fillRoundRect(_g, _x, _y, _w, _h, _rh, _rv);
  }

  public static void drawOval(Graphics _g, int _x, int _y, int _w, int _h, BresenhamParams _bp) {
    INSTANCE.drawOval(_g, _x, _y, _w, _h, _bp);
  }

  public static void fillOval(Graphics _g, int _x, int _y, int _w, int _h) {
    INSTANCE.fillOval(_g, _x, _y, _w, _h);
  }

  public static void drawPolyline(Graphics _g, Polygon _p, BresenhamParams _bp) {
    INSTANCE.drawPolyline(_g, _p, _bp);
  }

  public static void drawPolyline(Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints, BresenhamParams _bp) {
    INSTANCE.drawPolyline(_g, _xpoints, _ypoints, _npoints, _bp);
  }

  public static void drawPolygon(Graphics _g, Polygon _p, BresenhamParams _bp) {
    INSTANCE.drawPolygon(_g, _p, _bp);
  }

  public static void drawPolygon(Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints, BresenhamParams _bp) {
    INSTANCE.drawPolygon(_g, _xpoints, _ypoints, _npoints, _bp);
  }

  public static void fillPolygon(Graphics _g, Polygon _p) {
    INSTANCE.fillPolygon(_g, _p);
  }

  public static void fillPolygon(Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints) {
    INSTANCE.fillPolygon(_g, _xpoints, _ypoints, _npoints);
  }
}
