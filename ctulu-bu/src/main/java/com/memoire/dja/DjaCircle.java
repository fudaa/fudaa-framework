/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaCircle.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;


public class DjaCircle
       extends DjaEllipse
{
  public DjaCircle(String _texte)
  {
    super(_texte);
    setWidth(61);
  }

  public DjaCircle()
  {
    this(null);
  }

  @Override
  public void setWidth(int _w)
  {
    super.setHeight(_w);
    super.setWidth(_w);
  }

  @Override
  public void setHeight(int _h)
  {
    super.setWidth(_h);
    super.setHeight(_h);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[8];

    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,EAST ,x+w-1,y+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w/2,y+h-1);
    r[6]=new DjaAnchor(this,6,WEST ,x    ,y+h/2);

    r[1]=new DjaAnchor(this,1,NORTH,x+w-1-295*w/2000,y+    295*h/2000);
    r[3]=new DjaAnchor(this,3,SOUTH,x+w-1-295*w/2000,y+h-1-295*h/2000);
    r[5]=new DjaAnchor(this,5,SOUTH,x+    295*w/2000,y+h-1-295*h/2000);
    r[7]=new DjaAnchor(this,7,NORTH,x+    295*w/2000,y+    295*h/2000);

    return r;
  }
}
