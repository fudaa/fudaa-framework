/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaSaver.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

public class DjaSaver
{
  /*
  public static void saveAsSer(DjaGrid _grid, OutputStream _out)
       throws IOException
  {
    new ObjectOutputStream(_out) // new HexOutputStream(_out)
      .writeObject(_grid);
  }
  */

  /*
  public static final class HexOutputStream
         extends OutputStream
  {
    private OutputStream out_;
    private int          col_;

    public HexOutputStream(OutputStream _out)
    {
      out_=_out;
      col_=0;
    }

    public void close() throws IOException { out_.close(); }
    public void flush() throws IOException { out_.flush(); }

    public void write(byte[] b) throws IOException
    { write(b,0,b.length); }

    public void write(byte[] b,int d, int f) throws IOException
    { for(int i=d; i<f; i++) write(b[i]); }

    static final char[] t=
    { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };

    public void write(int c) throws IOException
    {
      if(col_==38)
	{ out_.write('\n'); col_=0; }

      while(c<0) c+=256;
      out_.write((byte)t[c/16]);
      out_.write((byte)t[c%16]);

      col_++;
    }
  }
  */

  private static final String color(Color _c)
  {
    String r="null";
    if(_c!=null)
    {
      r=Integer.toHexString(_c.getRGB()).toUpperCase();
      if(r.length()<8)
	{ r="00000000"+r; r=r.substring(r.length()-8); }
    }
    return r;
  }

  private static final String font(Font _f)
  {
    String r="null";
    if(!DjaOptions.defaultPlainFont.equals(_f))
    {
      r=_f.getFamily();
      if((_f.getStyle()&Font.BOLD  )!=0) r+=",gras";
      if((_f.getStyle()&Font.ITALIC)!=0) r+=",italique";
      r+=","+_f.getSize();
      r="\""+DjaCoder.encode(r)+"\"";
    }
    return r;
  }

  private static final String text(String _t)
  {
    String r="null";
    if(_t!=null) r="\""+DjaCoder.encode(_t)+"\"";
    return r;
  }

  private static final String id(Hashtable _t, Object _o)
  {
    return (_o==null ? "null" : (String)_t.get(_o)); 
  }

  /*
  public static void saveGridAsText(DjaGrid _grid, OutputStream _out)
       throws IOException
  {
    StringBuffer sb=new StringBuffer();
    saveGridAsText(_grid,sb);
    _out.write(sb.toString().getBytes());
  }
  */

  /*
  public static void saveGridAsText(DjaGrid _grid, StringBuffer _buffer)
  {
    saveAsText(_grid.getObjects(),_buffer);
  }
  */

  public static void saveAsText(DjaVector _vector, OutputStream _out)
       throws IOException
  {
    StringBuffer sb=new StringBuffer();
    saveAsText(_vector,sb);
    _out.write(sb.toString().getBytes());
  }

  public static void saveAsText(DjaVector _vector, StringBuffer _buffer)
  {
    int       cpt=0;
    Hashtable tbl=new Hashtable();

    _buffer.append("grille\n");
    _buffer.append("{\n");

    _buffer.append("  registre\n");
    _buffer.append("  {\n");
    for(Enumeration e=DjaRegistry.enumerate(); e.hasMoreElements(); )
    {
      String k=(String)e.nextElement();
      Class  c=DjaRegistry.getClass(k);
      if(c!=null)
	_buffer.append("    "+k+" "+text(c.getName())+"\n");
    }
    _buffer.append("  }\n");

    int s=(""+_vector.size()).length();
    for(Enumeration e=_vector.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      cpt++;
      String id="000000"+cpt;
      tbl.put(f,"I"+id.substring(id.length()-s));
    }

    for(Enumeration e=_vector.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();

      f.beforeSaving();

      String c=DjaRegistry.getKeyword(f);
      if(c==null)
      {
	c="rectangle";
	System.err.println("### Not registred: "+f.getClass().getName());
      }

      /*
      String c=f.getClass().getName();
      int    i=c.indexOf("Dja");
      if(i>=0) c=c.substring(i+3).toLowerCase();
           if("directarrow".equals(c)) c="fleche_directe";
      else if("brokenarrow".equals(c)) c="fleche_brisee";
      else if("box"        .equals(c)) c="rectangle";
      else if("component"  .equals(c)) c="composant";
      else if("circle"     .equals(c)) c="cercle";
      else if("diamond"    .equals(c)) c="losange";
      else if("hline"      .equals(c)) c="ligne_horizontale";
      else if("icon"       .equals(c)) c="icone";
      else if("roundbox"   .equals(c)) c="pave";
      else if("square"     .equals(c)) c="carre";
      else if("vline"      .equals(c)) c="ligne_verticale";
      */
	   
      _buffer.append("\n  "+c+"\n");
      _buffer.append("  {\n");
      _buffer.append("    id          "+id(tbl,f)+"\n");
      _buffer.append("    x           "+f.getX()+"\n");
      _buffer.append("    y           "+f.getY()+"\n");
      _buffer.append("    largeur     "+f.getWidth()+"\n");
      _buffer.append("    hauteur     "+f.getHeight()+"\n");
      _buffer.append("    selectionne "+(f.isSelected  ()?"vrai":"faux")+"\n");

      _buffer.append("\n    contour\n");
      _buffer.append("    {\n");
      _buffer.append("      couleur "+color(f.getForeground())+"\n");
      _buffer.append("    }\n");

      _buffer.append("\n    surface\n");
      _buffer.append("    {\n");
      _buffer.append("      couleur "+color(f.getBackground())+"\n");
      _buffer.append("    }\n");

      /*
      _buffer.append("\n    texte\n");
      _buffer.append("    {\n");
      _buffer.append("      valeur   "+text (f.getText     ())+"\n");
      _buffer.append("      couleur  "+color(f.getTextColor())+"\n");

      Font ft=f.getFont();
      if(!DjaOptions.defaultPlainFont.equals(ft))
      {
	String p=ft.getFamily();
	if((ft.getStyle()&Font.BOLD  )!=0) p+=",gras";
	if((ft.getStyle()&Font.ITALIC)!=0) p+=",italique";
	p+=","+ft.getSize();
	_buffer.append("      police   "+text(p)+"\n");
      }

      _buffer.append("      position "+f.getTextPosition()+"\n");
      _buffer.append("    }\n");
      */

      //for(Enumeration et=f.getTexts(); et.hasMoreElements(); )
      DjaText[] texts=f.getTextArray();
      for(int i=0;i<texts.length;i++)
      {
	DjaText t=texts[i]; //(DjaText)et.nextElement();
	_buffer.append("\n    texte\n");
	_buffer.append("    {\n");
	_buffer.append("      valeur      "+text(t.getText  ())+"\n");
	_buffer.append("      avant       "+text(t.getBefore())+"\n");
	_buffer.append("      apres       "+text(t.getAfter ())+"\n");
	_buffer.append("      numero      "+t.getNum()         +"\n");
	_buffer.append("      x           "+t.getX()           +"\n");
	_buffer.append("      y           "+t.getY()           +"\n");
	_buffer.append("      largeur     "+t.getW()           +"\n");
	_buffer.append("      hauteur     "+t.getH()           +"\n");
	_buffer.append("      position    "+t.getPosition()    +"\n");
	_buffer.append("      reference   "+t.getReference()   +"\n");
	_buffer.append("      alignement  "+t.getAlignment()   +"\n");
	_buffer.append("      couleur     "+color(t.getColor())+"\n");
	_buffer.append("      police      "+font (t.getFont ())+"\n");
	_buffer.append("      multiligne  "+(t.isMultiline ()?"vrai":"faux")+"\n");
	_buffer.append("      souligne    "+(t.isUnderlined()?"vrai":"faux")+"\n");
	_buffer.append("      selectionne "+(t.isSelected  ()?"vrai":"faux")+"\n");
	_buffer.append("    }\n");
      }

      _buffer.append("\n    proprietes\n");
      _buffer.append("    {\n");
      for(Enumeration ep=f.getPropertyKeys(); ep.hasMoreElements(); )
      {
	String k=(String)ep.nextElement();
	String v=f.getProperty(k);
	_buffer.append("      "+k+" \""+v.replace('"','�')+"\"\n");
      }
      _buffer.append("    }\n");

      _buffer.append("  }\n");
    }

    for(Enumeration e=_vector.elements(); e.hasMoreElements(); )
    {
      DjaObject f=(DjaObject)e.nextElement();
      if(f instanceof DjaLink)
      {
	DjaLink a=(DjaLink)f;
	_buffer.append("\n  lien\n");
	_buffer.append("  {\n");
	_buffer.append("    id    "+id(tbl,f)+"\n");
	_buffer.append("    debut\n");
	_buffer.append("    {\n");
	_buffer.append("      type     "+a.getBeginType()+"\n");
	_buffer.append("      x        "+a.getBeginX()+"\n");
	_buffer.append("      y        "+a.getBeginY()+"\n");
	_buffer.append("      o        "+a.getBeginO()+"\n");
	_buffer.append("      id       "+id(tbl,a.getBeginObject())+"\n");
	_buffer.append("      position "+a.getBeginPosition()+"\n");
	_buffer.append("    }\n");
	_buffer.append("    fin\n");
	_buffer.append("    {\n");
	_buffer.append("      type     "+a.getEndType()+"\n");
	_buffer.append("      x        "+a.getEndX()+"\n");
	_buffer.append("      y        "+a.getEndY()+"\n");
	_buffer.append("      o        "+a.getEndO()+"\n");
	_buffer.append("      id       "+id(tbl,a.getEndObject())+"\n");
	_buffer.append("      position "+a.getEndPosition()+"\n");
	_buffer.append("    }\n");
	_buffer.append("  }\n");
      }
    }

    _buffer.append("\n  ordre\n");
    _buffer.append("  {\n");
      
    int pos=1;
    for(Enumeration e=_vector.elements(); e.hasMoreElements();pos++)
    {
      DjaObject f=(DjaObject)e.nextElement();
      _buffer.append("    "+tbl.get(f)+"\n");
    }

    _buffer.append("  }\n");
    _buffer.append("}\n");
  }
}
