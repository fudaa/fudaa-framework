/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaLoadSaveLib.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;


public class DjaLoadSaveLib
{
  public static final DjaLoadSaveInterface get(String n)
  {
    if(n==null) return null;

    DjaLoadSaveInterface r=null;

    try
    {
      String p=DjaLoadSaveLib.class.getName();
      int    i=p.indexOf("DjaLoadSaveLib");
      p=p.substring(0,i+11);
      Class c=Class.forName(p+n);
      r=(DjaLoadSaveInterface)c.newInstance();
    }
    catch (Exception ex) { }

    return r;
  }
}
