/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaResource.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuResource;


public class DjaResource
       extends BuResource
{
  public static final DjaResource DJA=new DjaResource(BuResource.BU);

  private DjaResource(BuResource _parent) { setParent(_parent); }
}
