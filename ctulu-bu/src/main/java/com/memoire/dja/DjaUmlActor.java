/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaUmlActor.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class DjaUmlActor
       extends DjaForm
{
  public DjaUmlActor()
  {
    this(null);
  }

  public DjaUmlActor(String _text)
  {
    super(null);

    DjaText t0=new DjaText(this,0,_text,true,0,0,
			   SOUTH,0,MIDDLE,null,null,false,false);
    setTextArray(new DjaText[] { t0 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (41);
    super.setHeight(61);
    textChanged(t0);
  }

  @Override
  public int getWidth()
  {
    return getHeight()/3*2+1;
  }

  @Override
  public int getHeight()
  {
    return Math.max(60,super.getHeight());
  }

  /*
  public void textChanged(DjaText _text)
  {
  }
  */

  /*
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();
    int       w=getWidth();
    r[0].setW(w-4);
    return r;
  }
  */

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x,y);
    r[1]=new DjaAnchor(this,1,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,NORTH,x+w,y);
    r[3]=new DjaAnchor(this,3,EAST ,x+w,y+7+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w,y+h);
    r[5]=new DjaAnchor(this,5,SOUTH,x+w/2,y+h);
    r[6]=new DjaAnchor(this,6,SOUTH,x,y+h);
    r[7]=new DjaAnchor(this,7,WEST ,x,y+h/2);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[1];
    r[0]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
    Color bg=getBackground();
    Color fg=getForeground();

    if(fg!=null)
    {
      _g.setColor(fg);
      DjaGraphics.drawLine(_g,x+w/2,y+12,x+w/2,y+h-w/2,bp);
      DjaGraphics.drawLine(_g,x+w/2,y+h-w/2,x,y+h,bp);
      DjaGraphics.drawLine(_g,x+w/2,y+h-w/2,x+w-1,y+h,bp);
      DjaGraphics.drawLine(_g,x,y+20,x+w-1,y+20,bp);
    }

    if(bg!=null)
    {
      _g.setColor(bg);
      DjaGraphics.fillOval(_g,x+w/2-6,y,12,12);
    }

    if(fg!=null)
    {
      _g.setColor(fg);
      DjaGraphics.drawOval(_g,x+w/2-6,y,12,12,bp);
    }
  }
}
