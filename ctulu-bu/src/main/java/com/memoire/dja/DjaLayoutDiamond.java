/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaLayoutDiamond.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.util.Enumeration;

public class DjaLayoutDiamond
  implements DjaLayoutInterface
{
  public DjaLayoutDiamond()
  {
  }

  @Override
  public void layout(DjaGrid _grid)
  {
    DjaVector v=new DjaVector();
    for(Enumeration e=_grid.enumerate(DjaForm.class);
	e.hasMoreElements(); )
      v.addElement(e.nextElement());

    int    n=v.size();
    double r=20.+10.*n;

    for(int i=0;i<n;i++)
    {
      DjaForm f=(DjaForm)v.elementAt(i);
      int x=0;
      int y=0;
      int k=(4*i)/n;
      double z=4D*(((double)i)-(k*n)/4)/n;

      switch((4*i)/n)
      {
      case 0:
	  x=(int)(r+r*z);
	  y=(int)(r*z);
	  break;
      case 1:
	  x=(int)(2.*r-r*z);
	  y=(int)(r+r*z);
	  break;
      case 2:
	  x=(int)(r-r*z);
	  y=(int)(2.*r-r*z);
	  break;
      case 3:
	  x=(int)(r*z);
	  y=(int)(r-r*z);
	  break;
      }

      f.setX(x);
      f.setY(y);
    }
  }
}
