/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaIcon.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.Icon;

public class DjaIcon
       extends DjaForm
{
  public DjaIcon(String _text, Icon _icon)
  {
    super(_text,SOUTH);
    setForeground(null);
    setBackground(null);
    setIcon(_icon);
  }

  public DjaIcon(Icon _icon)
  {
    this(null,_icon);
  }

  public DjaIcon()
  {
    this(null,null);
  }

  private transient Icon icon_;
  @Override
  public Icon getIcon() { return icon_; }
  public void setIcon(Icon _icon)
  {
    icon_=_icon;
    if(icon_!=null)
    {
      setWidth(icon_.getIconWidth());
      setHeight(icon_.getIconHeight());
    }
  }

  @Override
  public void setWidth(int _w)
  {
    int w=_w;
    if(icon_!=null) w=Math.max(w,icon_.getIconWidth());
    super.setWidth(w);
  }
    
  @Override
  public void setHeight(int _h)
  {
    int h=_h;
    if(icon_!=null) h=Math.max(h,icon_.getIconHeight());
    super.setHeight(h);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[4];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[1]=new DjaAnchor(this,1,EAST ,x+w-1,y+h/2);
    r[2]=new DjaAnchor(this,2,SOUTH,x+w/2,y+h-1);
    r[3]=new DjaAnchor(this,3,WEST ,x    ,y+h/2);
    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Color fg=getForeground();
    Color bg=getBackground();

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x,y,w,h);
    }

    if(fg!=null)
    {
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawRect(_g,x,y,w-1,h-1,bp);
    }

    if(icon_!=null)
    {
      int dx=(w-icon_.getIconWidth ())/2;
      int dy=(h-icon_.getIconHeight())/2;
      icon_.paintIcon(HELPER,_g,x+dx,y+dy);
    }

    super.paintObject(_g);
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    if(isSelected())
    {
      int x=getX();
      int y=getY();
      int w=getWidth();
      int h=getHeight();

      _g.setColor(getForeground());
      _g.drawRect(x-1,y-1,w+1,h+1);
    }

    super.paintInteractive(_g);
  }
}
