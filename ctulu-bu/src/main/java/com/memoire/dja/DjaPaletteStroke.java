/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaPaletteStroke.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.border.EmptyBorder;

public class DjaPaletteStroke
       extends BuPanel
       implements DjaOptions
{
  BuButton[]   buttons_;
  BuGridLayout layout_;

  String[] names={ "1.0", "3.2",   "2.4", "2.5",
		   "1.1", "2.2",   "3.3", "4.4",
		   "8.8", "10.10", "2.1", "2.3",
		   "4.2", "6.3",   "8.4", "10.5" };

  public DjaPaletteStroke(ActionListener _al)
  {
    super();

    layout_=new BuGridLayout(4,2,2);
    layout_.setCfilled(false);
    setLayout(layout_);
    setBorder(new EmptyBorder(2,2,2,2));

    buttons_=new BuButton[names.length];

    for(int i=0; i<buttons_.length; i++)
    {
      Icon icon=new StrokeIcon(names[i]);

      buttons_[i]=new BuButton();
      buttons_[i].setIcon(icon);
      buttons_[i].setRolloverIcon(icon);
      buttons_[i].setMargin(new Insets(1,1,1,1));
      buttons_[i].setRequestFocusEnabled(false);
      buttons_[i].setToolTipText(/* "Set stroke to "+ */ names[i]);
      buttons_[i].setActionCommand("DJA_STROKE("+names[i]+")");

      add(buttons_[i]);
      buttons_[i].addActionListener(_al);
    }
  }

  private class StrokeIcon implements Icon
  {
    private String t_;

    public StrokeIcon(String _t)
    {
      t_=_t;
    }

    @Override
    public int getIconWidth () { return 16; }
    @Override
    public int getIconHeight() { return 16; }

    @Override
    public void paintIcon(Component _c, Graphics _g, int _x, int _y)
    {
      _g.setColor(_c.getForeground());
      int x=_x+1;
      int y=_y+1;

      int i    =t_.indexOf(".");
      int trait=Integer.parseInt(t_.substring(0,i));
      int vide =Integer.parseInt(t_.substring(i+1));

      DjaGraphics.BresenhamParams bp=
	new DjaGraphics.BresenhamParams(trait,vide);
      DjaGraphics.drawLine(_g,x+15,y,x,y+15,bp);
    }
  }
}
