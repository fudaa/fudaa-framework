/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaImage.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class DjaImage
       extends DjaForm
{
  public DjaImage(String _text, Image _image)
  {
    super(_text,SOUTH);
    setForeground(null);
    setBackground(null);
    setImage(_image);
  }

  public DjaImage(Image _image)
  {
    this(null,_image);
  }

  public DjaImage()
  {
    this(null,null);
  }

  private transient Image image_;
  public Image getImage() { return image_; }
  public void setImage(Image _image)
  {
    image_=_image;
    if(image_!=null)
    {
      setWidth(image_.getWidth(HELPER));
      setHeight(image_.getHeight(HELPER));
    }
  }

  /*
  public void setSize(Dimension _size)
  {
    Dimension s=_size.getSize();

    if(image_!=null)
    {
      s.width =Math.max(s.width ,image_.getWidth (HELPER));
      s.height=Math.max(s.height,image_.getHeight(HELPER));
    }
    super.setSize(s);
  }
  */

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[4];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[1]=new DjaAnchor(this,1,EAST ,x+w-1,y+h/2);
    r[2]=new DjaAnchor(this,2,SOUTH,x+w/2,y+h-1);
    r[3]=new DjaAnchor(this,3,WEST ,x    ,y+h/2);
    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Color fg=getForeground();
    Color bg=getBackground();

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x,y,w,h);
    }

    if(fg!=null)
    {
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawRect(_g,x,y,w-1,h-1,bp);
    }

    if(image_!=null)
      _g.drawImage(image_,x,y,w,h,HELPER);

    super.paintObject(_g);
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    if(isSelected())
    {
      int x=getX();
      int y=getY();
      int w=getWidth();
      int h=getHeight();

      _g.setColor(getForeground());
      _g.drawRect(x-1,y-1,w+1,h+1);
    }
  }
}


