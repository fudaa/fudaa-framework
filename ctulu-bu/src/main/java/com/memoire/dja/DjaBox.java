/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaBox.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;

public class DjaBox
       extends DjaForm
{
  public DjaBox(String _texte)
  {
    super(_texte);
  }

  public DjaBox()
  {
    this(null);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,EAST ,x+w-1,y+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w/2,y+h-1);
    r[6]=new DjaAnchor(this,6,WEST ,x    ,y+h/2);

    if(w>=h)
    {
      r[1]=new DjaAnchor(this,1,NORTH,x+3*w/4,y);
      r[3]=new DjaAnchor(this,3,SOUTH,x+3*w/4,y+h-1);
      r[5]=new DjaAnchor(this,5,SOUTH,x+w/4,y+h-1);
      r[7]=new DjaAnchor(this,7,NORTH,x+w/4,y);
    }
    else
    {
      r[1]=new DjaAnchor(this,1,EAST,x+w-1,y+  h/4);
      r[3]=new DjaAnchor(this,3,EAST,x+w-1,y+3*h/4);
      r[5]=new DjaAnchor(this,5,WEST,x    ,y+3*h/4);
      r[7]=new DjaAnchor(this,7,WEST,x    ,y+  h/4);
    }

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Color bg=getBackground();
    Color fg=getForeground();

    int r=0;
    try
    {
	r=Integer.parseInt(getProperty("rayon"));
	r=Math.max(0,Math.min(Math.min(w,h)/2,r));
    }
    catch(Exception ex) { }

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRoundRect(_g,x,y,w-1,h-1,r,r);
    }

    if(fg!=null)
    {
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.drawRoundRect(_g,x,y,w-1,h-1,r,r,bp);
    }

    super.paintObject(_g);
  }
}



