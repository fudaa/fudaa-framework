/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaVLine.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaVLine
       extends DjaForm
{
  public DjaVLine(String _texte)
  {
    super(_texte,EAST);
  }

  public DjaVLine()
  {
    this(null);
  }

  @Override
  public int getWidth()
  {
    return 1;
  }

  @Override
  public void setWidth(int _w)
  {
    super.setWidth(1);
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    return new Rectangle(getX()-3,getY(),7,getHeight()).contains(_x,_y);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[2];
    r[0]=new DjaAnchor(this,0,NORTH,x,y);
    r[1]=new DjaAnchor(this,1,SOUTH,x,y+h-1);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[2];
    r[0]=new DjaHandle(this,NORTH,x,y-deltaY);
    r[1]=new DjaHandle(this,SOUTH,x,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    Color fg=getForeground();
    if(fg!=null)
    {
      _g.setColor(fg);

      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.drawLine(_g,getX(),getY(),getX(),getY()+getHeight(),bp);
    }

    super.paintObject(_g);
  }
}
