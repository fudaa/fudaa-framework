/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaOptions.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuLib;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;

public interface DjaOptions
{
  /*
  public static final int NORTH =0;
  public static final int EAST  =1;
  public static final int SOUTH =2;
  public static final int WEST  =3;
  public static final int CENTER=4;
  public static final int NORTH_EAST =5;
  public static final int SOUTH_EAST =6;
  public static final int SOUTH_WEST =7;
  public static final int NORTH_WEST =8;
  public static final int RELATIVE   =9;
  public static final int ABSOLUTE   =10;

  public static final int NONE          =0;
  public static final int TRIANGLE_EMPTY=1;
  public static final int TRIANGLE_FG   =2;
  public static final int TRIANGLE_BG   =3;
  public static final int LOSANGE_EMPTY =4;
  public static final int LOSANGE_FG    =5;
  public static final int LOSANGE_BG    =6;
  public static final int SQUARE_EMPTY  =7;
  public static final int SQUARE_FG     =8;
  public static final int SQUARE_BG     =9;
  public static final int DISK_EMPTY    =10;
  public static final int DISK_FG       =11;
  public static final int DISK_BG       =12;

  public static Color   selectionZone      =new Color(224,128,224);
  public static Color   selectionForeground=new Color( 32,128, 32);
  public static Color   selectionBackground=new Color(192,255,192);
  public static Color   anchorsColor       =new Color( 64, 64,255);
  public static Color   controlsColor      =new Color(255,192,  0);
  public static Color   handlesColor       =new Color(255, 64,255);
  public static Color   linksColor         =new Color(255, 64, 64);
  public static Color   freesColor         =new Color( 64,255, 64);

  public static boolean snap       =true;
  public static int     deltaX     =10;
  public static int     deltaY     =10;
  public static Font    defaultPlainFont=new Font("SansSerif",Font.PLAIN,12);
  public static Font    defaultBoldFont =new Font("SansSerif",Font.BOLD,12);

  public static final JLabel                 HELPER  =new JLabel();
  public       static BuInformationsSoftware SOFTWARE=null;
  */

  int ANY        =-1;
  int NORTH      =0;
  int EAST       =1;
  int SOUTH      =2;
  int WEST       =3;
  int CENTER     =4;
  int NORTH_EAST =5;
  int SOUTH_EAST =6;
  int SOUTH_WEST =7;
  int NORTH_WEST =8;
  int HORIZONTAL =9;
  int VERTICAL   =10;
  int BOTH       =11;
  int AUTO       =12;
  int ABSOLUTE   =13;
  int RELATIVE_NW=14;
  int RELATIVE_NE=15;
  int RELATIVE_SW=16;
  int RELATIVE_SE=17;
  int RELATIVE_ANCHOR =18;
  int RELATIVE_ATTACH =19;
  int RELATIVE_CONTROL=20;
  int RELATIVE_TEXT_NW=21;
  int RELATIVE_TEXT_NE=22;
  int RELATIVE_TEXT_SW=23;
  int RELATIVE_TEXT_SE=24;

  int NONE                =0;
  int RIGHT_TRIANGLE_EMPTY=1;
  int RIGHT_TRIANGLE_FG   =2;
  int RIGHT_TRIANGLE_BG   =3;
  int LEFT_TRIANGLE_EMPTY =4;
  int LEFT_TRIANGLE_FG    =5;
  int LEFT_TRIANGLE_BG    =6;
  int LOSANGE_EMPTY       =7;
  int LOSANGE_FG          =8;
  int LOSANGE_BG          =9;
  int SQUARE_EMPTY        =10;
  int SQUARE_FG           =11;
  int SQUARE_BG           =12;
  int DISK_EMPTY          =13;
  int DISK_FG             =14;
  int DISK_BG             =15;

  int LEFT  =0;
  int MIDDLE=1;
  int RIGHT =2;

  Color   selectionZone      =new Color(224,128,224);
  Color   selectionForeground=new Color(192,255,192);//new Color( 32,128, 32);
  Color   selectionBackground=new Color(192,255,192);
  Color   selectionTextColor =new Color(192,192,255);
  Color   anchorsColor       =new Color( 64, 64,255);
  Color   controlsColor      =new Color(255,192,  0);
  Color   attachsColor       =new Color(255, 64, 64);
  Color   handlesColor       =new Color(255, 64,255);
  Color   freesColor         =new Color( 64,255, 64);

  boolean snap       =true;
  int     deltaX     =10;
  int     deltaY     =10;
  int     closeX     =5; //3
  int     closeY     =5; //3
  Font    defaultPlainFont=new Font("SansSerif",Font.PLAIN,12);
  Font    defaultSmallFont=new Font("SansSerif",Font.PLAIN,10);
  Font    defaultBigFont  =new Font("SansSerif",Font.PLAIN,14);
  Font    defaultBoldFont =new Font("SansSerif",Font.BOLD,12);

  Frame HELPER=BuLib.HELPER;
}
