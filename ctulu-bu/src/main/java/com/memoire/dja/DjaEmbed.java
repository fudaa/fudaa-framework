/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaEmbed.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JComponent;

public class DjaEmbed
       extends DjaForm
{
  public DjaEmbed(String _text,JComponent _embed)
  {
    super(_text,SOUTH);
    setEmbed(_embed);
  }

  public DjaEmbed(JComponent _embed)
  {
    this(null,_embed);
  }

  public DjaEmbed()
  {
    this(null,null);
  }

  private JComponent embed_;
  public JComponent getEmbed() { return embed_; }
  public void setEmbed(JComponent _embed)
  {
    embed_=_embed;
    if(embed_!=null)
    {
      setForeground(embed_.getForeground());
      setBackground(embed_.getBackground());

      Dimension ps=embed_.getPreferredSize();
      setWidth(ps.width);
      setHeight(ps.height);
    }
  }

  @Override
  public void setX(int _x)
  {
    if(embed_!=null) embed_.setLocation(new Point(_x,getY()));
    super.setX(_x);
  }

  @Override
  public void setY(int _y)
  {
    if(embed_!=null) embed_.setLocation(new Point(getX(),_y));
    super.setY(_y);
  }

  @Override
  public void setWidth(int _w)
  {
    if(embed_!=null) embed_.setSize(new Dimension(_w,getHeight()));
    super.setWidth(_w);
  }

  @Override
  public void setHeight(int _h)
  {
    if(embed_!=null) embed_.setSize(new Dimension(getWidth(),_h));
    super.setHeight(_h);
  }

  @Override
  public void setForeground(Color _foreground)
  {
    if(embed_!=null)
      if(_foreground!=null)
	embed_.setForeground(_foreground);
    super.setForeground(_foreground);
  }

  @Override
  public void setBackground(Color _background)
  {
    if(embed_!=null)
    {
      if(_background!=null) embed_.setBackground(_background);
      embed_.setOpaque(_background!=null);
    }
    super.setBackground(_background);
  }

  @Override
  public void setSelected(boolean _selected)
  {
    super.setSelected(_selected);
    if(embed_!=null)
    {
      Color fg=getForeground();
      Color bg=getBackground();

      if(fg!=null) embed_.setForeground(fg);
      if(bg!=null) embed_.setBackground(bg);
      embed_.setOpaque(bg!=null);
    }
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[4];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[1]=new DjaAnchor(this,1,EAST ,x+w-1,y+h/2);
    r[2]=new DjaAnchor(this,2,SOUTH,x+w/2,y+h-1);
    r[3]=new DjaAnchor(this,3,WEST ,x    ,y+h/2);
    return r;
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    if(embed_!=null)
    {
      Rectangle old=_g.getClipBounds();
      _g.setClip(new Rectangle(x,y,w,h).intersection(old));
      _g.translate(x,y);
      embed_.paint(_g);
      _g.translate(-x,-y);
      _g.setClip(old);
    }

    super.paintInteractive(_g);

    if(isSelected())
    {
      _g.setColor(getForeground());
      _g.drawRect(x-1,y-1,w+1,h+1);
    }
  }
}

