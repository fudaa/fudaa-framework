/**
 * @modification $Date: 2007-03-15 16:58:56 $
 * @statut unstable
 * @file DjaGrid.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuPreferences;
import com.memoire.fu.FuVectorPublic;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.geom.AffineTransform;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.JComponent;

public class DjaGrid extends JComponent implements DjaOwner, DjaOptions // not serializable
{

  private final DjaTransformData transform = new DjaTransformData();

  public DjaGrid() {
    this(null);
  }

  public DjaGrid(DjaVector _objects) {
    objects_ = new DjaVector();
    if (_objects != null) {
      setObjects(_objects);
    }

    setForeground(Color.gray);
    setBackground(Color.white);
    setFont(defaultBoldFont);
    setLayout(null);
    setBorder(null);
    setOpaque(true);
    setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    addMouseListener(new MouseAdapter() {
    });

    /*
     * ActionListener al=new ActionListener() { public void actionPerformd(ActionEvent _evt) { repaint(250); } }; new
     * Timer(250,al).start();
     */
  }

  @Override
  public DjaOwner getOwner() {
    return null;
  }

  public DjaTransformData getTransform() {
    return transform;
  }

  @Override
  public DjaGrid getGrid() {
    return this;
  }

  public boolean isInteractive() {
    return false;
  }

  public boolean isAnchorsVisible() {
    return false;
  }

  public boolean isAttachsVisible() {
    return false;
  }

  @Override
  public boolean isFocusTraversable() {
    return false;
  }

  private DjaVector objects_;

  public DjaVector getObjects() {
    return objects_;
  }

  public void setObjects(DjaVector _objects) {
    Enumeration e = _objects.reverseElements();
    while (e.hasMoreElements()) {
      add((DjaObject) e.nextElement());
    }
  }

  public Enumeration enumerate(Class _c) {
    return objects_.enumerate(_c);
  }

  @Override
  public Dimension getPreferredSize() {
    Dimension r = new Dimension(2 * deltaX + 1, 2 * deltaY + 1);

    for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      Rectangle b = o.getExtendedBounds();
      r.width = Math.max(r.width, b.x + b.width + deltaX);
      r.height = Math.max(r.height, b.y + b.height + deltaY);
    }

    return r;
  }

  public void recenterIfNeeded() {
    for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      if ((o.getX() < 0) || (o.getY() < 0)) {
        recenter();
        break;
      }
    }
  }

  public void recenter() {
    getTransform().clear();
    int xmin = Integer.MAX_VALUE;
    int ymin = Integer.MAX_VALUE;

    for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      xmin = Math.min(xmin, o.getX());
      ymin = Math.min(ymin, o.getY());
    }

    if ((xmin != deltaX) || (ymin != deltaY)) {
      for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
        DjaObject o = (DjaObject) e.nextElement();
        o.setX(o.getX() - xmin + deltaX);
        o.setY(o.getY() - ymin + deltaY);
      }

      revalidate();
    }
  }

  public void avoidOverlap(DjaObject _o) {
    if (!(_o instanceof DjaForm)) {
      return;
    }

    Enumeration e = enumerate(DjaForm.class);
    Rectangle b = _o.getBounds();

    while (e.hasMoreElements()) {
      DjaForm q = (DjaForm) e.nextElement();
      Rectangle r = q.getBounds();

      if (b.intersects(r)) {
        b.x = r.x + r.width + 4 * deltaX;
      }
    }

    _o.setX(b.x);
  }

  public void avoidOverlap() {
    boolean a = true;
    Vector v = new Vector();

    for (int n = 0; a && (n < 1000); n++) // just in case...
    {
      a = false;

      Enumeration e1 = enumerate(DjaForm.class);
      while (e1.hasMoreElements()) {
        DjaForm f1 = (DjaForm) e1.nextElement();
        Rectangle b1 = f1.getBounds();

        b1.width += deltaX / 2;
        b1.height += deltaY / 2;

        Enumeration e2 = enumerate(DjaForm.class);
        while (e2.hasMoreElements()) {
          DjaForm f2 = (DjaForm) e2.nextElement();
          Rectangle b2 = f2.getBounds();

          if (f1 == f2) {
            continue;
          }

          if (b1.x > b2.x) {
            b2.width += 4 * deltaX;
            if (b1.intersects(b2)) {
              b1.x += deltaX; // b2.x+b2.width+deltaX;
              a = true;
              break;
            }
          }
        }

        if (a) {
          f1.setX(b1.x);
          if (!v.contains(f1)) {
            v.addElement(f1);
          }
        }

        // TMP
        // repaint(0);
      }
    }

    Enumeration e = v.elements();
    while (e.hasMoreElements()) {
      DjaLib.snap((DjaForm) e.nextElement());
    }
  }

  protected void addEmbeds(DjaGroup _g) {
    for (Enumeration e = _g.getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      if (o instanceof DjaEmbed) {
        JComponent c = ((DjaEmbed) o).getEmbed();
        if (c != null) {
          add(c);
        }
      } else if (o instanceof DjaGroup) {
        addEmbeds((DjaGroup) o);
      }
    }
  }

  protected void removeEmbeds(DjaGroup _g) {
    for (Enumeration e = _g.getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      if (o instanceof DjaEmbed) {
        JComponent c = ((DjaEmbed) o).getEmbed();
        if (c != null) {
          remove(c);
        }
      } else if (o instanceof DjaGroup) {
        removeEmbeds((DjaGroup) o);
      }
    }
  }

  public final void add(DjaObject _o) {
    add(_o, false);
  }

  public void add(DjaObject _o, boolean _quiet) {
    if (_o.getOwner() != null) {
      throw new RuntimeException("belong to " + _o.getOwner());
    }
    // System.err.println("ERROR: "+_o+" belongs to "+_o.getOwner());

    insert(0, _o, _quiet);
  }

  public void insert(int _index, DjaObject _o) {
    insert(_index, _o, false);
  }

  public void insert(int _index, DjaObject _o, boolean _quiet) {
    getObjects().insertElementAt(_o, _index);
    _o.setOwner(this);
    if (!isInteractive() && (_o instanceof DjaEmbed)) {
      JComponent c = ((DjaEmbed) _o).getEmbed();
      if (c != null) {
        add(c);
      }
    }
    if (!isInteractive() && (_o instanceof DjaGroup)) {
      addEmbeds((DjaGroup) _o);
    }

    if (!_quiet) {
      fireGridEvent(_o, DjaGridEvent.ADDED);
    }
  }

  public void remove(DjaObject _o) {
    remove(_o, false);
  }

  public void remove(DjaObject _o, boolean _quiet) {
    if (_o.getOwner() != this) {
      throw new RuntimeException("doesn't belong to " + this);
    }
    getObjects().removeElement(_o);
    _o.setOwner(null);
    if (!isInteractive() && (_o instanceof DjaEmbed)) {
      JComponent c = ((DjaEmbed) _o).getEmbed();
      if (c != null) {
        remove(c);
      }
    }
    if (!isInteractive() && (_o instanceof DjaGroup)) {
      removeEmbeds((DjaGroup) _o);
    }

    if (!_quiet) {
      fireGridEvent(_o, DjaGridEvent.REMOVED);
    }
  }

  public void removeDependencies(DjaObject _o) {
    for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
      DjaObject f = (DjaObject) e.nextElement();
      if (f instanceof DjaLink) {
        DjaLink a = (DjaLink) f;
        if (a.getBeginObject() == _o) {
          a.setBeginObject(null);
        }
        if (a.getEndObject() == _o) {
          a.setEndObject(null);
        }
      }
    }
  }

  public void putToFront(DjaObject _o) {
    // quiet
    remove(_o, true);
    add(_o, true);
  }

  public void putToBack(DjaObject _o) {
    // quiet
    remove(_o, true);
    insert(getObjects().size(), _o, true);
  }

  protected transient FuVectorPublic gl_ = new FuVectorPublic(1, 1);

  public void addGridListener(DjaGridListener _l) {
    if (gl_ == null) {
      gl_ = new FuVectorPublic(1, 1);
    }
    if (!gl_.contains(_l)) {
      gl_.addElement(_l);
    }
  }

  public void removeGridListener(DjaGridListener _l) {
    if (gl_ != null) {
      gl_.removeElement(_l);
    }
  }

  public void fireGridEvent(DjaOwner _object, int _id) {
    if (gl_ == null) {
      return;
    }

    DjaGridEvent evt = new DjaGridEvent(this, _object, _id);
    // System.err.println("EVT: "+evt);

    for (Enumeration e = gl_.elements(); e.hasMoreElements();) {
      DjaGridListener l = (DjaGridListener) e.nextElement();
      switch (_id) {
        case DjaGridEvent.ADDED:
          l.objectAdded(evt);
          break;
        case DjaGridEvent.REMOVED:
          l.objectRemoved(evt);
          break;
        case DjaGridEvent.CONNECTED:
          l.objectConnected(evt);
          break;
        case DjaGridEvent.DISCONNECTED:
          l.objectDisconnected(evt);
          break;
        case DjaGridEvent.SELECTED:
          l.objectSelected(evt);
          break;
        case DjaGridEvent.UNSELECTED:
          l.objectUnselected(evt);
          break;
        case DjaGridEvent.MODIFIED:
          l.objectModified(evt);
          break;
      }
    }
  }

  @Override
  public  void paint(Graphics _g) {
    boolean a = BuPreferences.BU.getBooleanProperty("antialias.all", false);

    if (!a) {
      if (isInteractive()) {
        a = DjaPreferences.DJA.getBooleanProperty("antialias.interactivegrid", false);
      } else {
        a = DjaPreferences.DJA.getBooleanProperty("antialias.grid", true);
      }
    }

    BuLib.setAntialiasing(_g, a);
    super.paint(_g);
  }

  boolean repaintOnceScaleIsDone = true;

  @Override
  public void paintComponent(Graphics _g) {

    paintGrid(_g);
    Graphics2D g2d = (Graphics2D) _g;
    AffineTransform at = g2d.getTransform();
    at.scale(getTransform().getScale(), getTransform().getScale());
    at.translate(getTransform().getTranslateX(), getTransform().getTranslateY());
    g2d.setTransform(at);
    paintObjects(_g);

    if (repaintOnceScaleIsDone) {
      repaint();
      revalidate();
      //neccessary o avoid sparkling effect...
      repaintOnceScaleIsDone = false;
    } else {
      repaintOnceScaleIsDone = true;
    }

  }

  public void paintGrid(Graphics _g) {
    Insets i = getInsets();
    int w = getWidth() - i.left - i.right;
    int h = getHeight() - i.top - i.bottom;

    if (isOpaque()) {
      _g.setColor(getBackground());
      _g.fillRect(i.left, i.top, w, h);
    }
  }

  public void paintObjects(Graphics _g) {
    Rectangle clip = _g.getClipBounds();

    // true if we repaint all
    boolean fast = (clip == null) || clip.equals(getBounds());

    for (Enumeration e = getObjects().reverseElements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      if (!fast) {
        Rectangle r = o.getExtendedBounds();
        if (clip != null && !clip.intersects(r)) {
          continue;
        }
      }

      o.paint(_g);

      if (isInteractive()) {
        o.paintInteractive(_g);
        if (isAnchorsVisible()) {
          o.paintAnchors(_g);
        }
        if (isAttachsVisible()) {
          o.paintAttachs(_g);
        }
        if (o.isSelected()) {
          o.paintControls(_g);
        }
        if (o.isSelected()) {
          o.paintHandles(_g);
        }
      }
    }
  }

  public String toString() {
    String r = getName();
    return (r == null ? "grid" : r);
  }

  public synchronized Image getImage() {
    Image r;

    Dimension d = getSize();
    r = createImage(d.width, d.height);
    Graphics g = r.getGraphics();
    g.setClip(0, 0, d.width, d.height);
    g.setColor(Color.white);
    g.fillRect(0, 0, d.width, d.height);
    paint(g);

    return r;
  }
}
