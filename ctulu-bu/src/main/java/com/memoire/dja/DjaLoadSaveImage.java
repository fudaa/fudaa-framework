/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaLoadSaveImage.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuLib;
import java.awt.Image;
import java.io.File;

public abstract class DjaLoadSaveImage
       implements DjaLoadSaveInterface
{
  private String format_;

  public DjaLoadSaveImage(String _format)
  {
    format_=_format;
  }

  @Override
  public final boolean canLoad() { return false; }
  @Override
  public final boolean canSave() { return true; }

  @Override
  public final DjaVector load(File _file)
    throws Exception
  {
    throw new Exception("Not implemented");
  }

  @Override
  public final void save(File _file, DjaVector _vector)
    throws Exception
  {
    DjaGrid grid=null;

    if(_vector.size()!=0)
    {
      DjaObject o=(DjaObject)_vector.elementAt(0);
      grid=o.getGrid();
    }

    if(grid==null) grid=new DjaGrid();
    Image   i   =grid.getImage();
    BuLib.save(i,_file.getAbsolutePath(),format_);
  }

  @Override
  public final BuFileFilter getFilter()
  {
    return new BuFileFilter
      (new String[] { format_.toLowerCase(), format_.toUpperCase() },
      format_+" images");
  }
}
