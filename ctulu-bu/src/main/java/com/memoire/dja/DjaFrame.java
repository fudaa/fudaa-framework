/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut unstable
 * @file DjaFrame.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.util.Enumeration;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class DjaFrame extends BuInternalFrame implements BuCutCopyPasteInterface, BuPrintable,
        BuSelectFindReplaceInterface // BuUndoRedoInterface
{
  
  protected BuCommonImplementation app_;
  protected String file_;
  protected DjaGrid grid_;
  protected boolean tools_;
  
  protected JComponent content_;
  private BuBorderLayout layout_;
  
  protected BuToggleButton tbInteractive_;
  protected BuButton btRecenter_;
  protected BuButton btOverlap_;
  protected BuButton btFront_;
  protected BuButton btBack_;
  protected BuButton btResize_;
  protected BuButton btForeground_;
  protected BuButton btBackground_;
  protected BuButton btTextColor_;
  protected BuButton btTextFont_;
  protected BuButton btAddText_;
  protected BuButton btRemoveText_;
  
  protected BuPopupButton pbForms_;
  protected BuPopupButton pbLinks_;
  protected BuPopupButton pbUml_;
  protected BuPopupButton pbBrcks_;
  protected BuPopupButton pbStrks_;
  protected BuPopupButton pbThkns_;
  
  public DjaFrame(BuCommonImplementation _app, String _file, DjaGrid _grid) {
    this(_app, _file, _grid, true);
  }
  
  protected JScrollPane scrollPane_;
  
  public DjaFrame(BuCommonImplementation _app, String _file, DjaGrid _grid, boolean _tools) {
    super("", true, true, true, true);
    app_ = _app;
    grid_ = _grid;
    tools_ = _tools;
    
    setName("ifDJA");
    // app_.installContextHelp(getRootPane(),"alma/source.html");

    setFile(_file);
    
    content_ = (JComponent) getContentPane();
    layout_ = new BuBorderLayout();
    content_.setLayout(layout_);
    content_.setBorder(new EmptyBorder(0, 0, 0, 0));
    content_.setDoubleBuffered(false);
    scrollPane_ = new JScrollPane(grid_);
    scrollPane_.setDoubleBuffered(false);
    content_.add(scrollPane_, BuBorderLayout.CENTER);
    
    tbInteractive_ = new BuToggleButton();
    tbInteractive_.setIcon(DjaResource.DJA.getIcon("dja-interactive", 16));
    tbInteractive_.setActionCommand("DJA_TOGGLE_INTERACTIVE");
    tbInteractive_.setToolTipText(DjaResource.DJA.getString("Mode interactif"));
    tbInteractive_.setName("tbINTERACTIVE");
    tbInteractive_.setRequestFocusEnabled(false);
    tbInteractive_.setEnabled(grid_ instanceof DjaGridInteractive);
    tbInteractive_.setSelected(grid_.isInteractive());
    tbInteractive_.addActionListener(this);
    
    btRecenter_ = new BuButton();
    btRecenter_.setIcon(DjaResource.DJA.getIcon("dja-recenter", 16));
    btRecenter_.setActionCommand("DJA_RECENTER");
    btRecenter_.setToolTipText(DjaResource.DJA.getString("Recentrer"));
    btRecenter_.setName("btRECENTER");
    btRecenter_.setRequestFocusEnabled(false);
    btRecenter_.setEnabled(grid_ instanceof DjaGridInteractive);
    btRecenter_.addActionListener(this);
    
    btOverlap_ = new BuButton();
    btOverlap_.setIcon(DjaResource.DJA.getIcon("dja-overlap", 16));
    btOverlap_.setActionCommand("DJA_OVERLAP");
    btOverlap_.setToolTipText(DjaResource.DJA.getString("Eviter les chevauchements"));
    btOverlap_.setName("btOVERLAP");
    btOverlap_.setRequestFocusEnabled(false);
    btOverlap_.setEnabled(grid_ instanceof DjaGridInteractive);
    btOverlap_.addActionListener(this);
    
    btFront_ = new BuButton();
    btFront_.setIcon(DjaResource.DJA.getIcon("disposerdevant", 16));
    btFront_.setActionCommand("DJA_FRONT");
    btFront_.setToolTipText(DjaResource.DJA.getString("Disposer devant"));
    btFront_.setName("btFRONT");
    btFront_.setRequestFocusEnabled(false);
    btFront_.setEnabled(grid_ instanceof DjaGridInteractive);
    btFront_.addActionListener(this);
    
    btBack_ = new BuButton();
    btBack_.setIcon(DjaResource.DJA.getIcon("disposerderriere", 16));
    btBack_.setActionCommand("DJA_BACK");
    btBack_.setToolTipText(DjaResource.DJA.getString("Disposer derri�re"));
    btBack_.setName("btBACK");
    btBack_.setRequestFocusEnabled(false);
    btBack_.setEnabled(grid_ instanceof DjaGridInteractive);
    btBack_.addActionListener(this);
    
    btResize_ = new BuButton();
    btResize_.setIcon(DjaResource.DJA.getIcon("redimensionner", 16));
    btResize_.setActionCommand("DJA_RESIZE");
    btResize_.setToolTipText(DjaResource.DJA.getString("Redimensionner � la taille optimale"));
    btResize_.setName("btRESIZE");
    btResize_.setRequestFocusEnabled(false);
    btResize_.setEnabled(grid_ instanceof DjaGridInteractive);
    btResize_.addActionListener(this);
    
    btForeground_ = new BuButton();
    btForeground_.setIcon(DjaResource.DJA.getIcon("avantplan", 16));
    btForeground_.setActionCommand("DJA_FOREGROUND");
    btForeground_.setToolTipText(DjaResource.DJA.getString("Couleur de contour"));
    btForeground_.setName("btFOREGROUND");
    btForeground_.setRequestFocusEnabled(false);
    btForeground_.setEnabled(grid_ instanceof DjaGridInteractive);
    btForeground_.addActionListener(this);
    
    btBackground_ = new BuButton();
    btBackground_.setIcon(DjaResource.DJA.getIcon("arriereplan", 16));
    btBackground_.setActionCommand("DJA_BACKGROUND");
    btBackground_.setToolTipText(DjaResource.DJA.getString("Couleur de remplissage"));
    btBackground_.setName("btBACKGROUND");
    btBackground_.setRequestFocusEnabled(false);
    btBackground_.setEnabled(grid_ instanceof DjaGridInteractive);
    btBackground_.addActionListener(this);
    
    btTextColor_ = new BuButton();
    btTextColor_.setIcon(DjaResource.DJA.getIcon("couleurtexte", 16));
    btTextColor_.setActionCommand("DJA_TEXTCOLOR");
    btTextColor_.setToolTipText(DjaResource.DJA.getString("Couleur du texte"));
    btTextColor_.setName("btTEXTCOLOR");
    btTextColor_.setRequestFocusEnabled(false);
    btTextColor_.setEnabled(grid_ instanceof DjaGridInteractive);
    btTextColor_.addActionListener(this);
    
    btTextFont_ = new BuButton();
    btTextFont_.setIcon(DjaResource.DJA.getIcon("fonte", 16));
    btTextFont_.setActionCommand("DJA_TEXTFONT");
    btTextFont_.setToolTipText(DjaResource.DJA.getString("Fonte du texte"));
    btTextFont_.setName("btTEXTFONT");
    btTextFont_.setRequestFocusEnabled(false);
    btTextFont_.setEnabled(grid_ instanceof DjaGridInteractive);
    btTextFont_.addActionListener(this);
    
    btAddText_ = new BuButton();
    btAddText_.setIcon(DjaResource.DJA.getIcon("ajouter", 16));
    btAddText_.setActionCommand("DJA_ADDTEXT");
    btAddText_.setToolTipText(DjaResource.DJA.getString("Ajouter un texte"));
    btAddText_.setName("btADDTEXT");
    btAddText_.setRequestFocusEnabled(false);
    btAddText_.setEnabled(grid_ instanceof DjaGridInteractive);
    btAddText_.addActionListener(this);
    
    btRemoveText_ = new BuButton();
    btRemoveText_.setIcon(DjaResource.DJA.getIcon("enlever", 16));
    btRemoveText_.setActionCommand("DJA_REMOVETEXT");
    btRemoveText_.setToolTipText(DjaResource.DJA.getString("Supprimer un texte"));
    btRemoveText_.setName("btREMOVETEXT");
    btRemoveText_.setRequestFocusEnabled(false);
    btRemoveText_.setEnabled(grid_ instanceof DjaGridInteractive);
    btRemoveText_.addActionListener(this);
    
    if (tools_) {
      DjaPaletteForm srcforms = new DjaPaletteForm(this);
      pbForms_ = new BuPopupButton(DjaResource.DJA.getString("Formes"), srcforms);
      pbForms_.setToolTipText(DjaResource.DJA.getString("Palette de formes"));
      pbForms_.setIcon(DjaResource.DJA.getIcon("dja-box", 16));
      pbForms_.setEnabled(grid_ instanceof DjaGridInteractive);
      
      DjaPaletteLink srclinks = new DjaPaletteLink(this);
      pbLinks_ = new BuPopupButton(DjaResource.DJA.getString("Liens"), srclinks);
      pbLinks_.setToolTipText(DjaResource.DJA.getString("Palette de liens"));
      pbLinks_.setIcon(DjaResource.DJA.getIcon("dja-brokenarrow", 16));
      pbLinks_.setEnabled(grid_ instanceof DjaGridInteractive);
      
      DjaPaletteUml srcuml = new DjaPaletteUml(this);
      pbUml_ = new BuPopupButton(DjaResource.DJA.getString("Elts UML"), srcuml);
      pbUml_.setToolTipText(DjaResource.DJA.getString("Palette d'�l�ments UML"));
      pbUml_.setIcon(DjaResource.DJA.getIcon("dja-umlclass", 16));
      pbUml_.setEnabled(grid_ instanceof DjaGridInteractive);
      
      DjaPaletteBracket srcbrcks = new DjaPaletteBracket(this);
      pbBrcks_ = new BuPopupButton(DjaResource.DJA.getString("Extr�mit�s"), srcbrcks);
      pbBrcks_.setToolTipText(DjaResource.DJA.getString("Palette d'extr�mit�s"));
      pbBrcks_.setIcon(DjaResource.DJA.getIcon("dja-links", 16));
      pbBrcks_.setEnabled(grid_ instanceof DjaGridInteractive);
      
      DjaPaletteStroke srcstrks = new DjaPaletteStroke(this);
      pbStrks_ = new BuPopupButton("Traits", srcstrks);
      pbStrks_.setToolTipText(DjaResource.DJA.getString("Palette de traits"));
      pbStrks_.setIcon(DjaResource.DJA.getIcon("dja-strokes", 16));
      pbStrks_.setEnabled(grid_ instanceof DjaGridInteractive);
      
      DjaPaletteThickness srcthkns = new DjaPaletteThickness(this);
      pbThkns_ = new BuPopupButton("Epaisseurs", srcthkns);
      pbThkns_.setToolTipText(DjaResource.DJA.getString("Palette d'�paisseurs"));
      pbThkns_.setIcon(DjaResource.DJA.getIcon("dja-thickness", 16));
      pbThkns_.setEnabled(grid_ instanceof DjaGridInteractive);

      /*
       * Dimension ps1=pbForms_.getPalettePreferredSize(); Dimension ps2=pbLinks_.getPalettePreferredSize();
       * pbForms_.setPaletteLocation(0,0); pbLinks_.setPaletteLocation(0,ps1.height);
       * pbBrcks_.setPaletteLocation(ps1.width,0); pbStrks_.setPaletteLocation(0,ps1.height+ps2.height);
       * pbThkns_.setPaletteLocation(0,ps1.height+ps2.height);
       */
    }
    
    setFrameIcon(BuResource.BU.getFrameIcon("graphe"));
    
    Dimension ps = new Dimension(296, 296);
    setPreferredSize(ps);
    setSize(ps);
    // setLocation(150,10);
  }
  
  @Override
  public void setSelected(boolean _state) throws PropertyVetoException {
    super.setSelected(_state);
    if (isSelected()) {
      grid_.requestFocus();
    }
  }
  
  public DjaGrid getGrid() {
    return grid_;
  }
  
  public String getFile() {
    return file_;
  }
  
  public void setFile(String _file) {
    file_ = _file;
    int i = file_.lastIndexOf(System.getProperty("file.separator"));
    setTitle(file_.substring(i + 1));
  }
  
  public String getSource() {
    StringBuffer r = new StringBuffer();
    DjaSaver.saveAsText(grid_.getObjects(), r);
    return r.toString();
  }
  
  public Image getImage() {
    Image r;
    
    synchronized (grid_) {
      Dimension d = grid_.getSize();
      r = grid_.createImage(d.width, d.height);
      Graphics g = r.getGraphics();
      g.setClip(0, 0, d.width, d.height);
      g.setColor(Color.white);
      g.fillRect(0, 0, d.width, d.height);
      grid_.paint(g);
    }
    
    return r;
  }
  
  @Override
  public void actionPerformed(ActionEvent _evt) {
    // JComponent source=(JComponent)_evt.getSource();
    // System.err.println("DjaFrame : "+source);

    String action = _evt.getActionCommand();
    String arg = "";
    
    int i = action.indexOf('(');
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }

    // System.err.println("action="+action);
    // System.err.println("arg ="+arg);
    if ("DJA_TOGGLE_INTERACTIVE".equals(action)) {
      ((DjaGridInteractive) grid_).setInteractive(!grid_.isInteractive());
      
      updateInteractiveStates();
    } else if ("DJA_RECENTER".equals(action)) {
      grid_.recenter();
    } else if ("DJA_OVERLAP".equals(action)) {
      grid_.avoidOverlap();
    } else if ("DJA_FRONT".equals(action)) {
      ((DjaGridInteractive) grid_).putSelectionToFront();
    } else if ("DJA_BACK".equals(action)) {
      ((DjaGridInteractive) grid_).putSelectionToBack();
    } else if ("DJA_RESIZE".equals(action)) {
      DjaLib.resizeSelection((DjaGridInteractive) grid_);
    } else if ("DJA_FOREGROUND".equals(action)) {
      DjaLib.setForeground((DjaGridInteractive) grid_);
    } else if ("DJA_BACKGROUND".equals(action)) {
      DjaLib.setBackground((DjaGridInteractive) grid_);
    } else if ("DJA_TEXTCOLOR".equals(action)) {
      DjaLib.setColor((DjaGridInteractive) grid_);
    } else if ("DJA_TEXTFONT".equals(action)) {
      DjaLib.setFont((DjaGridInteractive) grid_);
    } else if ("DJA_ADDTEXT".equals(action)) {
      DjaLib.addText((DjaGridInteractive) grid_);
    } else if ("DJA_REMOVETEXT".equals(action)) {
      DjaLib.removeText((DjaGridInteractive) grid_);
    } else if ("DJA_CREATE_FORM".equals(action)) {
      if ("Box".equals(arg)) {
        grid_.add(new DjaBox());
      }
      if ("RoundBox".equals(arg)) {
        grid_.add(new DjaRoundBox());
      }
      if ("Diamond".equals(arg)) {
        grid_.add(new DjaDiamond());
      }
      if ("Square".equals(arg)) {
        grid_.add(new DjaSquare());
      }
      if ("Circle".equals(arg)) {
        grid_.add(new DjaCircle());
      }
      if ("Ellipse".equals(arg)) {
        grid_.add(new DjaEllipse());
      }
      if ("HLine".equals(arg)) {
        grid_.add(new DjaHLine());
      }
      if ("VLine".equals(arg)) {
        grid_.add(new DjaVLine());
      }
    } else if ("DJA_CREATE_UML".equals(action)) {
      if ("Class".equals(arg)) {
        grid_.add(new DjaUmlClass());
      }
      if ("AbstractClass".equals(arg)) {
        grid_.add(new DjaUmlClass());
      }
      if ("TemplateClass".equals(arg)) {
        grid_.add(new DjaUmlClass());
      }
      if ("Note".equals(arg)) {
        grid_.add(new DjaUmlNote());
      }
      if ("SmallPackage".equals(arg)) {
        grid_.add(new DjaUmlSmallPackage());
      }
      if ("LargePackage".equals(arg)) {
        grid_.add(new DjaUmlLargePackage());
      }
      if ("Actor".equals(arg)) {
        grid_.add(new DjaUmlActor());
      }
      if ("Usecase".equals(arg)) {
        grid_.add(new DjaUmlUsecase());
      }
      if ("Lifeline".equals(arg)) {
        grid_.add(new DjaUmlLifeline());
      }
      if ("Object".equals(arg)) {
        grid_.add(new DjaUmlObject());
      }
      if ("Component".equals(arg)) {
        grid_.add(new DjaUmlComponent());
      }
      if ("Node".equals(arg)) {
        grid_.add(new DjaUmlNode());
      }
      if ("ClassIcon".equals(arg)) {
        grid_.add(new DjaUmlClassIcon());
      }
      if ("State".equals(arg)) {
        grid_.add(new DjaUmlState());
      }
      if ("Join".equals(arg)) {
        grid_.add(new DjaUmlJoin());
      }
      if ("Branch".equals(arg)) {
        grid_.add(new DjaUmlBranch());
      }
    } else if ("DJA_CREATE_LINK".equals(action)) {
      if ("DirectArrow".equals(arg)) {
        grid_.add(new DjaDirectArrow());
      }
      if ("BrokenArrow".equals(arg)) {
        grid_.add(new DjaBrokenArrow());
      }
      if ("ArcArrow".equals(arg)) {
        grid_.add(new DjaArcArrow());
      }
      if ("BezierArrow".equals(arg)) {
        grid_.add(new DjaBezierArrow());
      }
      if ("ZigZagArrow".equals(arg)) {
        grid_.add(new DjaZigZagArrow());
      }
      
      if ("ZigZagArrow3".equals(arg)) {
        grid_.add(new DjaZigZagArrow(3));
      }
      if ("ZigZagArrow4".equals(arg)) {
        grid_.add(new DjaZigZagArrow(4));
      }
      if ("ZigZagArrow5".equals(arg)) {
        grid_.add(new DjaZigZagArrow(5));
      }
      if ("ZigZagArrow6".equals(arg)) {
        grid_.add(new DjaZigZagArrow(6));
      }
    } else if ("DJA_STROKE".equals(action)) {
      DjaLib.setProperty(((DjaGridInteractive) grid_).getSelection(), "trace", arg);
    } else if ("DJA_THICKNESS".equals(action)) {
      DjaLib.setProperty(((DjaGridInteractive) grid_).getSelection(), "epaisseur", arg);
    } else if ("DJA_BEGIN_TYPE".equals(action)) {
      int t = Integer.parseInt(arg);
      DjaLib.setBeginType(((DjaGridInteractive) grid_).getSelection(), t);
    } else if ("DJA_END_TYPE".equals(action)) {
      int t = Integer.parseInt(arg);
      DjaLib.setEndType(((DjaGridInteractive) grid_).getSelection(), t);
    } else if ("DJA_BEGIN_ORIENTATION".equals(action)) {
      int t = Integer.parseInt(arg);
      DjaLib.setBeginO(((DjaGridInteractive) grid_).getSelection(), t);
    } else if ("DJA_END_ORIENTATION".equals(action)) {
      int t = Integer.parseInt(arg);
      DjaLib.setEndO(((DjaGridInteractive) grid_).getSelection(), t);
    } else {
      super.actionPerformed(_evt);
    }
    
    grid_.repaint();
  }
  
  private static DjaVector clipboard = new DjaVector();
  
  @Override
  public void copy() {
    if (grid_.isInteractive()) {
      clipboard = ((DjaGridInteractive) grid_).cloneSelection();
    }
  }
  
  @Override
  public void cut() {
    if (grid_.isInteractive()) {
      clipboard = ((DjaGridInteractive) grid_).cloneSelection();
      
      for (Enumeration e = ((DjaGridInteractive) grid_).getSelection().elements(); e.hasMoreElements();) {
        DjaObject o = (DjaObject) e.nextElement();
        grid_.removeDependencies(o);
        grid_.remove(o);
      }
      
      ((DjaGridInteractive) grid_).clearSelection();
      grid_.repaint();
    }
  }
  
  @Override
  public void duplicate() {
    if (grid_.isInteractive()) {
      copy();
      paste();
      ((DjaGridInteractive) grid_).moveSelection(DjaOptions.deltaX, DjaOptions.deltaY);
    }
  }
  
  @Override
  public void find() {
  }
  
  @Override
  public void paste() {
    if (grid_.isInteractive()) {
      DjaVector s = DjaLib.clone(clipboard);
      
      for (Enumeration e = s.elements(); e.hasMoreElements();) {
        DjaObject o = (DjaObject) e.nextElement();
        o.setOwner(null);
        grid_.add(o);
      }
      
      ((DjaGridInteractive) grid_).setSelection(s);
      grid_.repaint();
    }
  }
  
  @Override
  public void replace() {
  }
  
  @Override
  public void print(PrintJob _job, Graphics _g) {
    BuPrinter.INFO_DOC = new BuInformationsDocument();
    BuPrinter.INFO_DOC.name = getTitle();
    BuPrinter.INFO_DOC.logo = BuResource.BU.getIcon("graphe", 24);
    
    BuPrinter.printComponent(_job, _g, grid_);
  }
  
  @Override
  public void select() {
    if (grid_.isInteractive()) {
      ((DjaGridInteractive) grid_).setSelectionToAll();
    }
  }

  // BuInternalFrame
  @Override
  public String[] getEnabledActions() {
    String[] r = new String[]{"ENREGISTRER", "ENREGISTRERSOUS", "IMPRIMER", "PREVISUALISER", "COUPER", "COPIER",
      "COLLER", "DUPLIQUER", "TOUTSELECTIONNER",};
    return r;
  }
  
  @Override
  public JComponent[] getSpecificTools() {
    JComponent[] r;
    final int N = 15;
    
    if (!tools_) {
      r = new JComponent[N];
    } else {
      pbForms_.setDesktop((BuDesktop) getDesktopPane());
      pbLinks_.setDesktop((BuDesktop) getDesktopPane());
      pbUml_.setDesktop((BuDesktop) getDesktopPane());
      pbStrks_.setDesktop((BuDesktop) getDesktopPane());
      pbThkns_.setDesktop((BuDesktop) getDesktopPane());
      pbBrcks_.setDesktop((BuDesktop) getDesktopPane());
      
      r = new JComponent[N + 8];
      r[N] = null;
      r[N + 1] = pbStrks_;
      r[N + 2] = pbThkns_;
      r[N + 3] = pbBrcks_;
      r[N + 4] = null;
      r[N + 5] = pbForms_;
      r[N + 6] = pbLinks_;
      r[N + 7] = pbUml_;
    }
    
    r[0] = tbInteractive_;
    r[1] = null;
    r[2] = btRecenter_;
    r[3] = btOverlap_;
    r[4] = btFront_;
    r[5] = btBack_;
    r[6] = btResize_;
    r[7] = null;
    r[8] = btForeground_;
    r[9] = btBackground_;
    r[10] = btTextColor_;
    r[11] = btTextFont_;
    r[12] = null;
    r[13] = btAddText_;
    r[14] = btRemoveText_;
    return r;
  }
  
  protected void updateInteractiveStates() {
    JComponent[] c = getSpecificTools();
    updateInteractiveTools(c);
  }
  
  protected void updateInteractiveTools(JComponent[] c) {
    boolean b = grid_.isInteractive();
    for (int j = 1; j < c.length; j++) // All except the first one
    {
      if (c[j] != null && c[j].getClientProperty("buttonAlwaysEnable") != Boolean.TRUE) {
        c[j].setEnabled(b);
      }
    }
  }
  
  public static void setAlwaysEnable(JComponent jc) {
    jc.putClientProperty("buttonAlwaysEnable", Boolean.TRUE);
  }
}
