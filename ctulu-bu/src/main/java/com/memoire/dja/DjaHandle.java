/**
 * @modification $Date: 2007-03-19 13:25:37 $
 * @statut       unstable
 * @file         DjaHandle.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;

public class DjaHandle
       extends DjaManipulator
{
  private int       x_;
  private int       y_;
  private int       o_;
  private DjaObject parent_;

  public DjaHandle(DjaObject _parent,int _o,int _x,int _y)
  {
    o_=_o;
    x_=_x;
    y_=_y;
    parent_=_parent;
  }
  

  @Override
  public final DjaOwner getOwner()
  { return getParent(); }

  public int getX() { return x_; }
  public int getY() { return y_; }
  public int getO() { return o_; }

  public DjaObject getParent() { return parent_; }

  public void paint(Graphics _g)
  {
    int x=getX();
    int y=getY();

    _g.setColor(handlesColor);
    _g.fillRect(x-2,y-2,5,5);
  }


  public void setX(int _x) {
    x_ = _x;
  }


  public void setY(int _y) {
    y_ = _y;
  }
}
