/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaLibDetect.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.util.Enumeration;

public class DjaLibDetect
{
  public static final int HANDLES =1;
  public static final int CONTROLS=2;
  public static final int ATTACHS =4;
  public static final int ANCHORS =8;
  public static final int TEXTS   =16;
  public static final int OBJECTS =32;
  public static final int ALL     =63;

  public static final Object detect
  (DjaGrid _grid,int _x, int _y)
  {
    return detect(_grid,_grid.getObjects(),_x,_y,null,ALL);
  }

  public static final Object detect
  (DjaGrid _grid,int _x, int _y,Object _exclude,int _type)
  {
    return detect(_grid,_grid.getObjects(),_x,_y,_exclude,_type);
  }
  
  public static final Object detect
  (DjaGrid _grid,DjaVector _v,int _x, int _y,Object _exclude,int _type)
  {
    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      
      if(o==_exclude) continue;

      // Handles
      if(o.isSelected()&&((_type&HANDLES)!=0))
      {
	DjaHandle[] handles=o.getHandles();
	int l=handles.length;
	for(int i=0;i<l;i++)
	{
	  DjaHandle h=handles[i];
	  if(DjaLib.close(h.getX(),h.getY(),_x,_y))
	    if(h!=_exclude)
	      return h;
	}
      }

      // Controls
      if(o.isSelected()&&((_type&CONTROLS)!=0))
      {
	DjaControl[] controls=o.getControls();
	int l=controls.length;
	for(int i=0;i<l;i++)
	{
	  DjaControl c=controls[i];
	  if(DjaLib.close(c.getX(),c.getY(),_x,_y))
	    if(c!=_exclude)
	      return c;
	}
      }

      // Attachs
      if(_grid.isAttachsVisible()&&((_type&ATTACHS)!=0))
      {
	DjaAttach[] attachs=o.getAttachs();
	int l=attachs.length;
	for(int i=0;i<l;i++)
	{
	  DjaAttach a=attachs[i];
	  if(DjaLib.close(a.getX(),a.getY(),_x,_y))
	    if(a!=_exclude)
	      return a;
	}
      }
      
      // Anchors
      if(_grid.isAnchorsVisible()&&((_type&ANCHORS)!=0))
      {
	DjaAnchor[] anchors=o.getAnchors();
	int l=anchors.length;
	for(int i=0;i<l;i++)
	{
	  DjaAnchor a=anchors[i];
	  if(DjaLib.close(a.getX(),a.getY(),_x,_y))
	    if(a!=_exclude)
	      return a;
	}
      }
      
      // Texts
      if(o.isSelected()&&((_type&TEXTS)!=0))
      {
	DjaText[] texts=o.getTexts();
	int l=texts.length;
	for(int i=0;i<l;i++)
	{
	  DjaText t=texts[i];
	  if(t.getBounds().contains(_x,_y))
	    if(t!=_exclude)
	      return t;
	}
      }

      // Object
      if((_type&OBJECTS)!=0)
      {
	if(o.contains(_x,_y))
	  return o;
      }
    }

    return null;
  }

  public static final DjaObject detectObject
  (DjaGrid _grid, int _x, int _y)
  {
    return detectObject(_grid.getObjects(),_x,_y);
  }

  public static final DjaObject detectObject
  (DjaVector _v, int _x, int _y)
  {
    for(Enumeration e=_v.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      if(o.contains(_x,_y)) return o;
    }
    
    return null;
  }
}
