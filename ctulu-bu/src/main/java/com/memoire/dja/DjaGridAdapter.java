/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaGridAdapter.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;



public class DjaGridAdapter
       implements DjaGridListener
{
  @Override
  public void objectAdded       (DjaGridEvent _evt) { }
  @Override
  public void objectRemoved     (DjaGridEvent _evt) { }
  @Override
  public void objectConnected   (DjaGridEvent _evt) { }
  @Override
  public void objectDisconnected(DjaGridEvent _evt) { }
  @Override
  public void objectSelected    (DjaGridEvent _evt) { }
  @Override
  public void objectUnselected  (DjaGridEvent _evt) { }
  @Override
  public void objectModified    (DjaGridEvent _evt) { }
}
