/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaVector.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.fu.FuVectorPublic;
import java.util.Enumeration;

public class DjaVector
       extends FuVectorPublic
{
  public DjaVector()
  {
    this(11,11);
  }

  public DjaVector(int _size)
  {
    this(_size,11);
  }

  public DjaVector(int _size, int _incr)
  {
    super(_size,_incr);
  }

  public Enumeration enumerate(Class _c)
  {
    DjaVector   r=new DjaVector();
    Enumeration e=elements();

    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(_c.isInstance(o)) r.addElement(o);
    }

    return r.reverseElements();
  }

  /*
  public final synchronized Enumeration reverseElements()
  {
    return new Enumeration()
      {
	private int count=size()-1;

	public boolean hasMoreElements()
          { return count>=0; }

	public Object nextElement()
	{
	  synchronized (DjaVector.this)
	  {
	    if((count>=0)&&(count<size()))
	      return elementAt(count--);
	  }
	  throw new NoSuchElementException("DjaVector.reverseElements");
	}
      };
  }
  */
}
