/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaGraphics.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

//import com.memoire.fu.*;
//import com.memoire.bu.*;
import java.awt.Graphics;
import java.awt.Polygon;

/**
 * Interface for independant graphic ops.
 */
public abstract class DjaGraphics0
  implements DjaOptions
{
  public abstract void drawLine
    (Graphics _g, int _x1, int _y1, int _x2, int _y2, DjaGraphics.BresenhamParams _bp);
  public abstract void drawRect
    (Graphics _g, int _x, int _y, int _w, int _h, DjaGraphics.BresenhamParams _bp);
  public abstract void drawRoundRect
    (Graphics _g, int _x, int _y, int _w, int _h,
     int _rh, int _rv, DjaGraphics.BresenhamParams _bp);
  public abstract void fillRoundRect
    (Graphics _g, int _x, int _y, int _w, int _h,
     int _rh, int _rv);
  public abstract void drawOval
    (Graphics _g, int _x, int _y, int _w, int _h, DjaGraphics.BresenhamParams _bp);
  public abstract void fillOval
    (Graphics _g, int _x, int _y, int _w, int _h);
  public abstract void drawPolyline
    (Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints,
     DjaGraphics.BresenhamParams _bp);
  public abstract void drawPolygon
    (Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints,
     DjaGraphics.BresenhamParams _bp);

  // draw

  public final void drawPoint
    (Graphics _g, int _x, int _y, DjaGraphics.BresenhamParams _bp)
  {
    drawPoint(_g,_x,_y,_x,_y,_bp);
  }

  public final void drawPoint
    (Graphics _g, int _x1, int _y1, int _x2, int _y2, DjaGraphics.BresenhamParams _bp)
  {
    if(_bp.epaisseur_==1)
      _g.drawLine(_x1,_y1,_x2,_y2);
    else
    if(_bp.epaisseur_==2)
    {
      _g.drawLine(_x1  ,_y1  ,_x2  ,_y2  );
      _g.drawLine(_x1+1,_y1  ,_x2+1,_y2  );
      _g.drawLine(_x1  ,_y1+1,_x2  ,_y2+1);
      _g.drawLine(_x1+1,_y1+1,_x2+1,_y2+1);
    }
    else
    if(_bp.epaisseur_==3)
    {
      _g.drawLine(_x1  ,_y1  ,_x2  ,_y2  );
      _g.drawLine(_x1+1,_y1  ,_x2+1,_y2  );
      _g.drawLine(_x1-1,_y1  ,_x2-1,_y2  );
      _g.drawLine(_x1  ,_y1+1,_x2  ,_y2+1);
      _g.drawLine(_x1  ,_y1-1,_x2  ,_y2-1);
    }
    else
    if(_bp.epaisseur_>2)
    {
      /*
      int e=_bp.epaisseur_;
      _g.fillOval(_x1-e/2,_y1-e/2,e,e);
      _g.drawLine(_x1,_y1,_x2,_y2);
      _g.fillOval(_x2-e/2,_y2-e/2,e,e);
      */
      int e=_bp.epaisseur_;
      for(int i=0;i<e;i++)
	for(int j=0;j<e;j++)
	  if(((i!=0)&&(i!=e-1))||((j!=0)&&(j!=e-1)))
	    _g.drawLine(_x1+i-e/2,_y1+j-e/2,_x2+i-e/2,_y2+j-e/2);
    }
  }

  public final void fillRect
    (Graphics _g, int _x, int _y, int _w, int _h)
  {
    _g.fillRect(_x,_y,_w,_h);
  }

  public final void drawPolyline
    (Graphics _g, Polygon _p, DjaGraphics.BresenhamParams _bp)
  {
    drawPolyline(_g,_p.xpoints,_p.ypoints,_p.npoints,_bp);
  }

  public final void drawPolygon
    (Graphics _g, Polygon _p, DjaGraphics.BresenhamParams _bp)
  {
    drawPolygon(_g,_p.xpoints,_p.ypoints,_p.npoints,_bp);
  }

  public final void fillPolygon
    (Graphics _g, Polygon _p)
  {
    _g.fillPolygon(_p);
  }

  public final void fillPolygon
    (Graphics _g, int[] _xpoints, int[] _ypoints, int _npoints)
  {
    _g.fillPolygon(_xpoints,_ypoints,_npoints);
  }
}
