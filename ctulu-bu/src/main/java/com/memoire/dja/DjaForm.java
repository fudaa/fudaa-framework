/**
 * @modification $Date: 2007-03-09 08:37:38 $
 * @statut       unstable
 * @file         DjaForm.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Rectangle;

public abstract class DjaForm extends DjaObject {
  private int x_, y_, width_, height_;

  public DjaForm(String _text) {
    this(_text, CENTER);
  }

  public DjaForm(String _text, int _position) {
    super();

    setX(deltaX);
    setY(deltaY);
    setWidth(80);
    setHeight(40);

    if (_text != null) {
      DjaText t0 = createText(_text, 0);
      t0.setPosition(_position);
      setTextArray(new DjaText[] { t0 });
    }
  }

  protected DjaText createText(String _text, int num) {
    return new DjaText(this, num, _text, true, 0, 0);
  }

  @Override
  public void addText(String _text) {
    DjaText t = createText(_text, -1);
    int p = CENTER;
    switch (getTexts().length) {
    case 0:
      p = CENTER;
      break;
    case 1:
      p = SOUTH;
      break;
    case 2:
      p = NORTH;
      break;
    case 3:
      p = EAST;
      break;
    case 4:
      p = WEST;
      break;
    }
    t.setPosition(p);
    addText(t);
  }

  @Override
  public int getX() {
    return x_;
  }

  @Override
  public void setX(int _x) {
    x_ = _x;
  }

  @Override
  public int getY() {
    return y_;
  }

  @Override
  public void setY(int _y) {
    y_ = _y;
  }

  @Override
  public int getWidth() {
    return width_;
  }

  @Override
  public void setWidth(int _width) {
    width_ = _width;
  }

  @Override
  public int getHeight() {
    return height_;
  }

  @Override
  public void setHeight(int _height) {
    height_ = _height;
  }

  @Override
  public Rectangle getBounds() {
    return new Rectangle(getX(), getY(), getWidth(), getHeight());
  }

  @Override
  public final DjaAttach[] getAttachs() {
    return new DjaAttach[0];
  }
}
