/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         Dja.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuApplication;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuSplashScreen;
import java.util.Locale;

public class Dja
  implements Runnable
{
  private static String[] args_;

  private Dja(String[] _args)
  {
    args_=_args;
  }

  private final void step1()
  {
    // guess();

    /*    
    if(!FuLib.verifyMemory(8))
    {
      String t="You need at least 8M to run safely Alma.\n"+
	       "Please use the -mx option of java.";
      boolean ok=FuLib.verifyMemory(1);
      if(ok)
      {
	try { new BuDialogMessage(null,null,t).activate();}
	catch(Throwable e) { ok=false; }
      }
      if(!ok) System.err.println(t);
      System.exit(-1);
    }
    */

    /*
    String jv=System.getProperty("java.version");
    if(jv.compareTo("1.1.2")<0)
    {
      System.err.println("JVM >1.1.2 needed for Swing");
      System.exit(-1);
    }
    */
  }

  private final void step2()
  {
    BuPreferences.BU.applyLookAndFeel();

    /*
    try { UIManager.setLookAndFeel
	    ("BuBasicLookAndFeel"); }
    catch(Exception ex) { ex.printStackTrace(); }
    */

    /*
    // Mac Look and feel
    System.getProperties().put("os.name", "Mac OS");
    try { UIManager.setLookAndFeel
	    ("com.sun.java.swing.plaf.mac.MacLookAndFeel"); }
    catch(Exception ex) { ex.printStackTrace(); }
    */
    
    /*
    try { UIManager.setLookAndFeel
	    ("javax.swing.plaf.organic.OrganicLookAndFeel"); }
    catch(Exception ex) { ex.printStackTrace(); }
    */

    /*
    try { UIManager.setLookAndFeel
	    ("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); }
    catch(Exception ex) { ex.printStackTrace(); }
    */
  }

  private final void step3()
  {
    BuInformationsSoftware il=DjaImplementation.informationsSoftware();

    if(BuPreferences.BU.getStringProperty("locale.language").equals(""))
    {
      BuLanguagePreferencesPanel pp=
	new BuLanguagePreferencesPanel(il.languages);

      new BuDialogMessage(null,il,pp).activate();
      pp.validatePreferences();
      Locale.setDefault(new Locale(pp.getLocaleString(),""));
    }

    BuPreferences.BU.applyLanguage(il.languages);
  }

  private final void step4()
  {
    BuInformationsSoftware il=DjaImplementation.informationsSoftware();

    // Splash screen

    BuSplashScreen ss=new BuSplashScreen
      (il,1000,new String[][]
       { BuLib.SWING_CLASSES, BuLib.BU_CLASSES,
	 DjaLib.DJA_CLASSES },
       getClass().getClassLoader());
    //DjaImplementation.class.getName()

    if(BuPreferences.BU.getBooleanProperty("splashscreen.visible",true))
    {
      // Exception dans Mac L&F
      try { ss.start(); } catch(Exception ex) { }
    }

    ss.setText("Cr�ation de l'application");
    ss.setProgression(0);
    BuApplication app=new BuApplication();
    DjaImplementation imp=new DjaImplementation();
    imp.setSplashScreen(ss);
    app.setImplementation(imp);
    app.init();

    BuGlassPaneStop glasspane=new BuGlassPaneStop();
    app.setGlassPane(glasspane);
    glasspane.setVisible(true);

    ss.setProgression(100);
    ss.setText("D�marrage de l'application");

    try { Thread.sleep(30); }
    catch(Exception ex) { }

    //ss.setVisible(false);
    //ss=null;

    //app.setVisible(true);
    app.start();

    //DjaDnd.installIcons();
    glasspane.setVisible(false);

    if(args_.length>0)
      imp.cmdArguments(args_);
  }

  @Override
  public final void run()
  {
    //System.err.println("STEP1");
    step1();
    //System.err.println("STEP2");
    step2();
    //System.err.println("STEP3");
    step3();
    //System.err.println("STEP4");
    step4();
    //System.err.println("STEP5");
  }

  public static void main(String _args[])
  {
    Thread.currentThread().setName("Dja main thread");
    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

    new Dja(_args).run();
  }
}
