/**
 * @modification $Date: 2007-03-15 16:58:56 $
 * @statut unstable
 * @file DjaGridInteractive.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuToggleButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;

public class DjaGridInteractive
    extends DjaGrid {
  public DjaGridInteractive() {
    this(true, null);
  }

  public DjaGridInteractive(boolean _interactive) {
    this(_interactive, null);
  }

  public DjaGridInteractive(boolean _interactive, DjaVector _objects) {
    super(_objects);

    anchorsVisible_ = true;
    attachsVisible_ = true;
    dotsVisible_ = true;

    clearSelection();
    setInteractive(_interactive);
  }

  private boolean anchorsVisible_;

  @Override
  public boolean isAnchorsVisible() {
    return anchorsVisible_;
  }

  public void setAnchorsVisible(boolean _v) {
    anchorsVisible_ = _v;
  }

  private boolean attachsVisible_;

  @Override
  public boolean isAttachsVisible() {
    return attachsVisible_;
  }

  public void setAttachsVisible(boolean _v) {
    attachsVisible_ = _v;
  }

  private boolean dotsVisible_;

  public boolean isDotsVisible() {
    return dotsVisible_;
  }

  public void setDotsVisible(boolean _v) {
    dotsVisible_ = _v;
  }

  public DjaVector getSelection() {
    if (!isInteractive()) {
      return null; //new DjaVector(0);
    }
    DjaVector r = new DjaVector();
    for (Enumeration e = getObjects().elements(); e.hasMoreElements(); ) {
      DjaObject o = (DjaObject) e.nextElement();
      if (o.isSelected()) {
        r.addElement(o);
      }
    }
    return r;
  }

  public void setSelection(DjaVector _selection) {
    boolean refresh = false;
    DjaVector selection = _selection;

    if (selection == null) {
      selection = new DjaVector(0);
    }

    for (Enumeration e = getObjects().elements(); e.hasMoreElements(); ) {
      DjaObject o = (DjaObject) e.nextElement();
      if (o.isSelected() && !selection.contains(o)) {
        o.setSelected(false);
        refresh = true;
      }
    }

    for (Enumeration e = selection.elements(); e.hasMoreElements(); ) {
      DjaObject o = (DjaObject) e.nextElement();
      if (!o.isSelected() && getObjects().contains(o)) {
        o.setSelected(true);
        refresh = true;
      }
    }

    if (refresh) {
      repaint();
    }
  }

  public void clearSelection() {
    setSelection((DjaVector) null);
  }

  public void setSelectionToAll() {
    setSelection(getObjects());
  }

  public void setSelection(DjaObject _o) {
    DjaVector s = new DjaVector(1);
    if (_o != null) {
      s.addElement(_o);
    }
    setSelection(s);
  }

  public void addSelection(DjaObject _o) {
    DjaVector s = getSelection();
    if ((_o != null) && !s.contains(_o)) {
      s.addElement(_o);
      setSelection(s);
    }
  }

  public void removeSelection(DjaObject _o) {
    DjaVector s = getSelection();
    if ((_o != null) && s.contains(_o)) {
      s.removeElement(_o);
      setSelection(s);
    }
  }

  public DjaVector cloneSelection() {
    return DjaLib.clone(getSelection());
  }

  public void moveSelection(int _dx, int _dy) {
    DjaVector s = getSelection();
    int l = s.size();

    if (l == 0) {
      return;
    }

    if (l == 1) {
      DjaObject o = (DjaObject) s.elementAt(0);
      Rectangle r = DjaLib.getDirtyArea(o);//o.getExtendedBounds();
      //pour eviter d'alloe
      o.setX(o.getX() + _dx);
      o.setY(o.getY() + _dy);
      r = DjaLib.getDirtyArea(o).union(r);
      repaint(r);
    } else {
      for (Enumeration e = s.elements(); e.hasMoreElements(); ) {
        DjaObject o = (DjaObject) e.nextElement();
        o.setX(o.getX() + _dx);
        o.setY(o.getY() + _dy);
      }
      repaint();
    }
    //FRED a voir .....
    invalidate();
    if (getParent() != null) {
      getParent().invalidate();
      getParent().doLayout();
    }
  }

  public void putSelectionToFront() {
    // quiet
    DjaVector s = getSelection();
    for (Enumeration e = s.elements(); e.hasMoreElements(); ) {
      remove((DjaObject) e.nextElement(), true);
    }
    int l = s.size();
    for (int i = l - 1; i >= 0; i--) {
      add((DjaObject) s.elementAt(i), true);
    }
  }

  public void putSelectionToBack() {
    // quiet
    DjaVector s = getSelection();
    for (Enumeration e = s.elements(); e.hasMoreElements(); ) {
      remove((DjaObject) e.nextElement(), true);
    }
    int l = s.size();
    for (int i = 0; i < l; i++) {
      insert(getObjects().size(),
          (DjaObject) s.elementAt(i), true);
    }
  }

  protected DjaMouseListener createInteractiveMouseListener() {
    return new DjaMouseListener(this);
  }

  protected DjaKeyListener createInteractiveKeyListener() {
    return new DjaKeyListener(this);
  }

  private DjaMouseListener ml_;
  private DjaKeyListener kl_;
  private DjaVector ss_; // saved selection
  private boolean interactive_;

  @Override
  public boolean isInteractive() {
    return interactive_;
  }

  public void setInteractiveEnable(boolean enable) {
    if (ml_ != null) {
      ml_.setEnable(enable);
    }
  }

  public void setInteractive(boolean _s) {
    if (interactive_ != _s) {
      interactive_ = _s;
      if (interactive_) {
        ml_ = createInteractiveMouseListener();
        addMouseListener(ml_);
        addMouseMotionListener(ml_);
        addKeyListener(ml_);
        kl_ = createInteractiveKeyListener();
        addKeyListener(kl_);

        for (Enumeration e = getObjects().elements(); e.hasMoreElements(); ) {
          DjaObject o = (DjaObject) e.nextElement();
          if (o instanceof DjaEmbed) {
            JComponent c = ((DjaEmbed) o).getEmbed();
            if (c != null) {
              remove(c);
            }
          }
          if (o instanceof DjaGroup) {
            removeEmbeds((DjaGroup) o);
          }
          if ((ss_ != null) && ss_.contains(o)) {
            o.setSelected(true);
          }
        }
        ss_ = null;

        if (!hasFocus()) {
          requestFocus();
        }
      } else {
        removeMouseListener(ml_);
        removeMouseMotionListener(ml_);
        removeKeyListener(ml_);
        ml_ = null;
        removeKeyListener(kl_);
        kl_ = null;

        ss_ = new DjaVector();
        for (Enumeration e = getObjects().elements(); e.hasMoreElements(); ) {
          DjaObject o = (DjaObject) e.nextElement();
          if (o instanceof DjaEmbed) {
            JComponent c = ((DjaEmbed) o).getEmbed();
            if (c != null) {
              add(c);
            }
          }
          if (o instanceof DjaGroup) {
            addEmbeds((DjaGroup) o);
          }
          if (o.isSelected()) {
            ss_.addElement(o);
            o.setSelected(false);
          }

          DjaText[] texts = o.getTexts();
          for (int k = 0; k < texts.length; k++) {
            texts[k].setSelected(false);
          }
        }

        transferFocus();
      }
      repaint();
      firePropertyChange("interactive", !interactive_, interactive_);
    }
  }

  @Override
  public boolean isFocusTraversable() {
    return isInteractive();
  }

  @Override
  protected void processFocusEvent(FocusEvent _evt) {
    super.processFocusEvent(_evt);
    repaint();
  }

  @Override
  public void paintGrid(Graphics _g) {
    super.paintGrid(_g);

    if (isDotsVisible() && isInteractive()) {
      Insets i = getInsets();
      int w = getWidth() - i.left - i.right;
      int h = getHeight() - i.top - i.bottom;
      Rectangle r = new Rectangle(i.left, i.top, w, h);

      Rectangle clip = _g.getClipBounds();
      if (clip != null) {
        r = r.intersection(clip);
      }
      if (r != null) {
        final int dx = 5 * deltaX;
        final int dy = 5 * deltaY;
        final int x0 = i.left + Math.max(0, r.x - (r.x % dx));
        final int y0 = i.top + Math.max(0, r.y - (r.y % dy));
        final int x1 = Math.min(i.left + w, x0 + r.width + dx);
        final int y1 = Math.min(i.top + h, y0 + r.height + dy);

        //System.err.println("XY01="+x0+","+y0+" "+x1+","+y1);
        _g.setColor(getForeground());
        /*
         for(int x=i.left;x<w;x+=5*deltaX)
         for(int y=i.top;y<h;y+=deltaY)
         _g.drawLine(x,y,x,y);
         for(int y=i.top;y<h;y+=5*deltaY)
         for(int x=i.left;x<w;x+=deltaX)
         _g.drawLine(x,y,x,y);
         */
        for (int x = x0; x < x1; x += dx) {
          for (int y = y0; y < y1; y += deltaY) {
            _g.drawLine(x, y, x, y);
          }
        }
        for (int y = y0; y < y1; y += dy) {
          for (int x = x0; x < x1; x += deltaX) {
            _g.drawLine(x, y, x, y);
          }
        }
      }
    }
  }

  private boolean paintRect = true;

  public boolean isPaintRect() {
    return paintRect;
  }

  public void setPaintRect(boolean paintRect) {
    this.paintRect = paintRect;
  }

  @Override
  public void paintComponent(Graphics _g) {
    super.paintComponent(_g);

    if (hasFocus()) {
      Insets i = getInsets();
      int w = getWidth() - i.left - i.right;
      int h = getHeight() - i.top - i.bottom;

      _g.setColor(UIManager.getColor("Button.focus"));
      if (paintRect) {
        _g.drawRect(i.left + 1, i.top + 1, w - 3, h - 3);
      }

      if (kl_ != null) {
        int kp = kl_.getPreviousKey();
        if (kp != -1) {
          Font ft = getFont();
          int fs = ft.getSize();
          _g.fillOval(-32, -fs - 3, 64, 2 * fs + 6);
          _g.setFont(ft);
          _g.setColor(Color.white);
          _g.drawString(KeyEvent.getKeyText(kp), 4, fs);
        }
      }
    }
  }

  public static void main(String[] args) {
    final JFrame f = new JFrame("test Dja");
    final DjaGridInteractive g = new DjaGridInteractive(false);
    final BuToggleButton tbi = new BuToggleButton("Interactive");
    final BuButton tbs = new BuButton("Write");
    final BuButton tbl = new BuButton("Read");

    DjaBox b1 = new DjaBox("boite");
    DjaDiamond d1 = new DjaDiamond("losange");
    DjaBox r1 = new DjaRoundBox("rond");
    DjaLabel l1 = new DjaLabel("�tiquette\nsur 2 lignes");
    DjaBrokenArrow a1 = new DjaBrokenArrow("fl�che");
    DjaDirectArrow a2 = new DjaDirectArrow("fl�che");
    DjaEllipse e1 = new DjaEllipse("ellipse");
    DjaPoint p1 = new DjaPoint("point");
    DjaIcon ic1 = new DjaIcon("ic�ne",
        UIManager.getIcon("InternalFrame.icon"));
    DjaImage im1 = new DjaImage("image",
        new ImageIcon("logo-dja.gif").getImage());

    BuLabelMultiLine m1 = new BuLabelMultiLine("Salut tout\nle monde !");
    DjaEmbed c1 = new DjaEmbed("composant", m1);
    c1.setX(220);
    c1.setY(30);
    g.add(c1);

    BuButton j1 = new BuButton("Bouton");
    DjaEmbed c2 = new DjaEmbed("composant", j1);
    c2.setX(160);
    c2.setY(210);
    g.add(c2);

    e1.setBackground(new Color(0x80CFFFFF));
    l1.setFont(new Font("SansSerif", Font.PLAIN, 18));

    l1.setX(100);
    r1.setX(200);
    r1.setY(100);
    e1.setX(100);
    e1.setY(100);
    d1.setX(20);
    d1.setY(250);
    p1.setX(20);
    p1.setY(60);
    ic1.setX(20);
    ic1.setY(100);
    im1.setX(20);
    im1.setY(140);

    a1.setBegin(b1, 4, 11);
    a1.setEnd(e1, 6, 8);
    a2.setBegin(e1, 4, 3);
    a2.setEnd(d1, 2, 6);

    g.add(b1);
    g.add(l1);
    g.add(d1);
    g.add(r1);
    g.add(e1);
    g.add(p1);
    g.add(ic1);
    g.add(im1);
    g.add(a1);
    g.add(a2);

    f.getContentPane().add("Center", g);

    tbi.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent _evt) {
                              g.setInteractive(tbi.isSelected());
                            }
                          }
    );

    tbs.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent _evt) {
                              try {
                                FileOutputStream out;
                                out = new FileOutputStream("essai.dja");
                                DjaSaver.saveAsText(g.getObjects(), out);
                                out.close();
          /*
           out=new FileOutputStream("essai.ser");
           DjaSaver.saveAsSer(g,out);
           out.close();
           */
                              } catch (IOException ex) {
                                ex.printStackTrace();
                              }
                            }
                          }
    );

    tbl.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent _evt) {
                              try {
                                FileInputStream in;
                                DjaVector v;
                                DjaGroup gp;

                                in = new FileInputStream("essai.dja");
                                v = DjaLoader.loadAsText(in);
                                in.close();

                                gp = new DjaGroup("essai.dja");
                                Enumeration e = v.elements();
                                while (e.hasMoreElements()) {
                                  DjaObject o = (DjaObject) e.nextElement();
                                  gp.add(o);
                                }

                                gp.setSelected(true);
                                gp.setX(gp.getX() + deltaX);
                                gp.setY(gp.getY() + deltaY);
                                g.add(gp);

          /*
           in=new FileInputStream("essai.ser");
           r=DjaLoader.loadAsSer(in);
           in.close();

           gp=new DjaGroup("essai.ser");
           for(Enumeration e=r.getObjects().elements();
           e.hasMoreElements(); )
           {
           DjaObject o=(DjaObject)e.nextElement();
           gp.add(o);
           }
           gp.setSelected(true);
           gp.setX(gp.getX()+2*deltaX);
           gp.setY(gp.getY()+2*deltaY);
           g.add(gp);
           */
                                g.repaint();
                              } catch (IOException ex1) {
                                ex1.printStackTrace();
                              }
                              //catch(ClassNotFoundException ex2) { ex2.printStackTrace(); }
                            }
                          }
    );

    g.addGridListener(new DjaGridListener() {
      @Override
      public void objectAdded(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }

      @Override
      public void objectRemoved(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }

      @Override
      public void objectConnected(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }

      @Override
      public void objectDisconnected(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }

      @Override
      public void objectSelected(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }

      @Override
      public void objectUnselected(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }

      @Override
      public void objectModified(DjaGridEvent _evt) {
        System.err.println("EVT: " + _evt);
      }
    });

    JPanel p = new JPanel(new GridLayout(1, 3));
    p.add(tbi);
    p.add(tbs);
    p.add(tbl);
    f.getContentPane().add("South", p);

    f.pack();
    f.setSize(500, 500);
    f.setVisible(true);
    f.setLocation(200, 100);

    tbi.requestFocus();
  }
}
