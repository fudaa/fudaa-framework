/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut       unstable
 * @file         DjaColor.java
 * @version      0.32
 * @author       Guillaume Desnoix
 * @email        guillaume-desnoix@memoire.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 * guillaume-desnoix@memoire.com
 */

package com.memoire.dja;

import java.awt.Color;
import java.io.Serializable;

public class DjaColor
  implements DjaOptions, Cloneable, Serializable
{
    private int rgb_;

    private transient Color color_;
    

    public DjaColor()
    {
      rgb_=Color.black.getRGB();
    }

    public DjaColor(int _rgb)
    {
	rgb_=_rgb;
    }

    public DjaColor(Color _color)
    {
	rgb_  =_color.getRGB();
    }
    
    @Override
    protected Object clone() throws CloneNotSupportedException {
      DjaColor res=(DjaColor)super.clone();
      res.color_=color_;
      res.rgb_=rgb_;
      return res;
    }

    public Color getColor()
    {
	if(color_==null) color_=new Color(rgb_);
	return color_;
    }

   
}
