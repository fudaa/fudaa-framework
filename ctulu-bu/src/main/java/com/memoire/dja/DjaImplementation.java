/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut unstable
 * @file DjaImplementation.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.*;
import com.memoire.dnd.DndManageData;
import com.memoire.dnd.DndTarget;
import com.memoire.dnd.DndTransferObject;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;

public class DjaImplementation extends BuCommonImplementation {
  private BuAssistant assistant_;
  private BuExplorer explorer_;
  private BuTaskView ltTaches;
  private JScrollPane spTaches;
  private DjaTree trGrid_;
  private JScrollPane spGrid_;
  private BuPalette plForms;
  private BuPalette plLinks;
  private BuPalette plDiaUml;
  private BuPalette plBrcks;
  private BuPalette plStrks;
  private BuPalette plThkns;
  private BuPreferencesFrame ifPreferences;
  private BuHelpFrame ifAide;
  private int nbdoc_;
  // InformationsSoftware
  private static final BuInformationsSoftware isDja_ = new BuInformationsSoftware();

  static {
    isDja_.name = "Dja";
    isDja_.version = "0.13";
    isDja_.date = "2002-07-18";
    isDja_.rights = "(c)2000-2002 Guillaume Desnoix";
    isDja_.contact = "guillaume@desnoix.com";
    isDja_.license = "GPL2";
    isDja_.languages = "fr,de,en,eo,es,hu,it,pg,ru";
    isDja_.logo = DjaResource.DJA.getIcon("logo-dja");
    isDja_.banner = DjaResource.DJA.getIcon("banner-dja");
    // isDja_.ftp=null;
    isDja_.http = "http://www.memoire.com/guillaume-desnoix/dja/";
    isDja_.man = DjaPreferences.DJA
        .getStringProperty("doc.mirror", "http://www.memoire.com/guillaume-desnoix/dja/doc/");
    isDja_.update = "http://www.memoire.com/guillaume-desnoix/dja/update/";

    isDja_.authors = new String[]{"Guillaume Desnoix"};
    isDja_.contributors = new String[]{
        // "Axel von Arnim (...)",
    };
    isDja_.documentors = new String[]{
        // "Guillaume Desnoix"
    };
    isDja_.translators = new String[]{"Zsuzsanna Geczki (hongrois)", "Norbert Marchl (allemand)",
        "Norbert Batfai (hongrois)", "Joeseph Zeldin (russe)", "Thomas Fehr (allemand)",
        "Guillaume Desnoix (anglais,esperanto)", "Systran/Altavista (espagnol,italien,portuguais)"};
    isDja_.testers = new String[]{
        // "Axel von Arnim"
    };
    isDja_.libraries = new String[]{"Encodeurs: Jef Poskanzer\n    (http://www.acme.com/)",
        "Ic�nes 20x20: Dean S. Jones\n    (http://www.javalobby.org/jfa/projects/icons/)",
        "Ic�nes 22x22: Rivyn\n    (http://rivyn.derkarl.org/kti/)",
        "Ic�nes 24x24: Tuomas Kuosmanen\n    (http://tigert.net/)",};
    isDja_.thanks = new String[]{"Alexander Larsson\n    (http://www.lysator.liu.se/~alla/)"};
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return isDja_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isDja_;
  }

  // Constructeur

  @Override
  public void init() {
    super.init();

    try {
      BuSplashScreen ss = getSplashScreen();

      if (ss != null) {
        ss.setText("Ajustements");
        ss.setProgression(50);
      }

      BuInformationsSoftware il = getInformationsSoftware();
      BuInformationsDocument id = new BuInformationsDocument();

      setTitle(il.name + " " + il.version);
      BuPrinter.INFO_LOG = il;
      BuPrinter.INFO_DOC = id;

      BuMenuBar mb = getMainMenuBar();
      mb.addMenu(buildPaletteMenu());

      BuMenu ma = (BuMenu) mb.getMenu("MENU_AIDE");
      boolean rg = !"".equals(DjaPreferences.DJA.getStringProperty("user.id"));
      ma.addMenuItem(BuResource.BU.getString("Support..."), "WWW_SUPPORT", rg);
      ma.addMenuItem(BuResource.BU.getString("Bogue..."), "WWW_BOGUE", true);
      if (!rg) {
        ma.addMenuItem(BuResource.BU.getString("Achat..."), "WWW_ACHAT", true);
      }

      boolean b = (getApp() instanceof BuApplication);
      getApp().setEnabledForAction("CREER", true);
      getApp().setEnabledForAction("OUVRIR", b);
      getApp().setEnabledForAction("QUITTER", b);
      getApp().setEnabledForAction("PREFERENCE", true);

      getApp().setEnabledForAction("TOUTSELECTIONNER", true);

      getApp().setEnabledForAction("AIDE_INDEX", false);
      getApp().setEnabledForAction("POINTEURAIDE", false);
      getApp().setEnabledForAction("INDEX_ALPHA", false);
      getApp().setEnabledForAction("INDEX_THEMA", false);
      getApp().setEnabledForAction("WWW_ACCUEIL", false);
      getApp().setEnabledForAction("WWW_BOGUE", false);
      getApp().setEnabledForAction("WWW_SUPPORT", false);

      getApp().removeAction("IMPORTER");
      getApp().removeAction("EXPORTER");
      getApp().removeAction("PROPRIETE");
      getApp().removeAction("RECHERCHER");
      getApp().removeAction("REMPLACER");
      getApp().removeAction("VISIBLE_LEFTCOLUMN");
      getApp().removeAction("MAJ");

      mb.computeMnemonics();

      if (ss != null) {
        ss.setText("Bureau et assistant");
        ss.setProgression(60);
      }

      BuMainPanel mp = getMainPanel();
      BuColumn lc = mp.getLeftColumn();
      BuColumn rc = mp.getRightColumn();

      if (DjaPreferences.DJA.getBooleanProperty("module.explorer", true)) {
        explorer_ = new BuExplorer(this);
        explorer_.addActionListener(this);
        lc.addToggledComponent(BuResource.BU.getString("Explorateur"), "EXPLORATEUR", explorer_, this);

        for (String extension : EXTENSIONS) {
          DjaLoadSaveInterface lsi = DjaLoadSaveLib.get(extension);
          if ((lsi != null) && lsi.canLoad()) {
            explorer_.addFilter(lsi.getFilter());
          }
        }
      }

      if (DjaPreferences.DJA.getBooleanProperty("module.assistant", true)) {
        assistant_ = new BuAssistant();
        assistant_.setBorder(null);
        rc.addToggledComponent(BuResource.BU.getString("Assistant"), "ASSISTANT", assistant_, this);
      }

      if (ss != null) {
        ss.setText("Bureau et assistant");
        ss.setProgression(60);
      }

      ltTaches = new BuTaskView();
      spTaches = new BuScrollPane(ltTaches);
      spTaches.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", spTaches, this);

      final BuButton btRefresh = new BuButton();
      btRefresh.setIcon(BuResource.BU.getIcon("rafraichir", 16));
      btRefresh.setRequestFocusEnabled(false);
      btRefresh.setMargin(new Insets(0, 0, 0, 0));
      btRefresh.setToolTipText(BuResource.BU.getString("Rafraichir"));

      btRefresh.addActionListener(_evt -> refreshTree());

      BuPanel pnButtons = new BuPanel();
      pnButtons.setLayout(new BuHorizontalLayout(2, false, true));
      pnButtons.add(btRefresh);

      trGrid_ = new DjaTree();
      spGrid_ = new BuScrollPane(trGrid_);

      BuPanel pnGrid = new BuPanel();
      pnGrid.setLayout(new BuVerticalLayout(2, true, true));
      pnGrid.add(pnButtons);
      pnGrid.add(spGrid_);
      rc.addToggledComponent(BuResource.BU.getString("Arbre"), "ARBRE", pnGrid, this);

      if (ss != null) {
        ss.setText("Fichiers r�cents");
        ss.setProgression(80);
      }

      BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(DjaPreferences.DJA);
        mr.setResource(DjaResource.DJA);
      }

      mp.getStatusBar().setApp(this);
      mp.setLogo(il.logo);
      mp.setTaskView(ltTaches);
      mp.setAssistant(assistant_);
    } catch (Throwable t) {
      FuLog.error(t);
    }
  }

  @Override
  public void start() {
    super.start();

    BuInformationsSoftware il = getInformationsSoftware();
    DjaLib.SOFTWARE = il;

    assistant_.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "DEBUTER(" + il.name.toUpperCase()
        + ")"));

    BuPreferences.BU.applyOn(getApp());
    if (!DjaPreferences.DJA.getBooleanProperty("module.explorer", true)) {
      getMainPanel().getLeftColumn().setVisible(false);
      getApp().removeAction("VISIBLE_LEFTCOLUMN");
    }

    assistant_.changeAttitude(BuAssistant.PAROLE, BuResource.BU.getString("Bienvenue") + "!\n" + il.name + " "
        + il.version);

    new BuTaskOperation(this, BuResource.BU.getString("Mise en place")) {
      @Override
      public void act() {
        miseEnPlace();
      }
    }.start();
  }

  public void miseEnPlace() {
    try {
      final BuMainPanel mp = getMainPanel();
      mp.setProgression(10);

      assistant_.addEmitters((Container) getApp());
      assistant_.changeAttitude(BuAssistant.ATTENTE, "");
      mp.setProgression(85);

      BuMenuBar mb = getMainMenuBar();
      mb.removeActionListener(this);
      mb.addActionListener(this);
      mp.setProgression(90);

      menusOrganisationFenetres();
      mp.setProgression(100);

      new DndTarget().accept(File.class, DndTransferObject.SINGLETON, new DndManageData() {
        @Override
        public boolean manage(Object _data, JComponent _component, Point _location, int _i) {
          Object[] o;

          if (_data instanceof Object[]) {
            o = (Object[]) _data;
          } else {
            o = new Object[]{_data};
          }

          for (Object o1 : o) action("OUVRIR(" + o1 + ")");

          return true;
        }
      }).add(mp.getDesktop());

      mp.setProgression(0);
    } catch (Exception ex) {
      FuLog.error(ex);
    }
  }

  public void cmdArguments(final String[] _args) {
    FuLog.warning("PARSING ARGS...");
    new BuTaskOperation(this, BuResource.BU.getString("Traitement des arguments")) {
      @Override
      public void act() {
        opArguments(_args);
      }
    }.start();
  }

  public void opArguments(String[] _args) {
    int l = _args.length;

    if (l > 0) {
      BuMainPanel mp = getMainPanel();

      for (int i = 0; i < l; i++) {
        mp.setProgression(10 + 90 * i / l);
        openFile(_args[i]);
      }

      mp.setProgression(0);
    } else {
      if (DjaPreferences.DJA.getBooleanProperty("start.open", true)) {
        String p = DjaPreferences.DJA.getStringProperty("file.recent.1.path");
        if (!"".equals(p)) {
          cmdReouvrir(p);
        }
      } else if (DjaPreferences.DJA.getBooleanProperty("start.create", true)) {
        cmdCreer();
      }
    }
  }

  // Utilitaires

  public void refreshTree() {
    if (trGrid_ == null) {
      return;
    }

    JInternalFrame frame = getCurrentInternalFrame();

    if (frame instanceof DjaFrame) {
      trGrid_.setGrid(((DjaFrame) frame).getGrid());
    } else {
      trGrid_.setGrid(null);
    }
  }

  // Evenements

  @Override
  public synchronized void actionPerformed(ActionEvent event) {

    String action = event.getActionCommand();
    String arg = "";

    int i = action.indexOf('(');
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }

    if (action.equals("CREER")) {
      cmdCreer();
    } else if (action.equals("OUVRIR")) {
      cmdOuvrir(arg);
    } else if (action.equals("REOUVRIR")) {
      cmdReouvrir(arg);
    } else if (action.equals("ENREGISTRER")) {
      cmdEnregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      cmdEnregistrerSous();
    } else if (action.equals("PREFERENCE")) {
      cmdPreference();
    } else if (action.equals("PALETTE_FORMES")) {
      cmdFormes();
    } else if (action.equals("PALETTE_LIENS")) {
      cmdLiens();
    } else if (action.equals("PALETTE_DIAUML")) {
      cmdDiaUml();
    } else if (action.equals("PALETTE_EXTREMITES")) {
      cmdExtremites();
    } else if (action.equals("PALETTE_TRAITS")) {
      cmdTraits();
    } else if (action.equals("PALETTE_EPAISSEURS")) {
      cmdEpaisseurs();
    } else if (action.equals("WWW_SUPPORT")) {
      displayURL("mailto:dja-support@memoire.com");
    } else if (action.equals("WWW_BOGUE")) {
      displayURL("mailto:dja-bug@memoire.com");
    } else if (action.equals("WWW_ACHAT")) {
      displayURL("https://www.yaskifo.com/register.asp?LNG=FR&ID=101697");
    } else if (action.startsWith("DJA_")) {
      JInternalFrame frame = getCurrentInternalFrame();

      if (frame instanceof DjaFrame) {
        ((DjaFrame) frame).actionPerformed(event);
      }
    } else if (action.equals("EXPLORATEUR")) {
      BuColumn lc = getMainPanel().getLeftColumn();

      lc.toggleComponent(action);
      setCheckedForAction(action, lc.isToggleComponentVisible(action));
    } else if (action.equals("ASSISTANT") || action.equals("TACHE") || action.equals("ARBRE")) {
      BuColumn rc = getMainPanel().getRightColumn();

      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
    } else {
      super.actionPerformed(event);
    }
  }

  // Menu Palette

  public static BuMenu buildPaletteMenu() {
    BuMenu r = new BuMenu(DjaResource.DJA.getString("Palettes"), "MENU_PALETTE");
    r.addCheckBox(DjaResource.DJA.getString("Formes"), "PALETTE_FORMES", DjaResource.DJA.getMenuIcon("dja-box"), true,
        false);
    r.addCheckBox(DjaResource.DJA.getString("Liens"), "PALETTE_LIENS", DjaResource.DJA.getMenuIcon("dja-brokenarrow"),
        true, false);
    r.addCheckBox(DjaResource.DJA.getString("El�ments UML"), "PALETTE_DIAUML", DjaResource.DJA
        .getMenuIcon("dja-umlclass"), true, false);
    r.addSeparator();
    r.addCheckBox(DjaResource.DJA.getString("Extr�mit�s"), "PALETTE_EXTREMITES", DjaResource.DJA
        .getMenuIcon("dja-links"), true, false);
    r.addCheckBox(DjaResource.DJA.getString("Traits"), "PALETTE_TRAITS", DjaResource.DJA.getMenuIcon("dja-strokes"),
        true, false);
    r.addCheckBox(DjaResource.DJA.getString("Epaisseurs"), "PALETTE_EPAISSEURS", DjaResource.DJA
        .getMenuIcon("dja-thickness"), true, false);
    r.addSeparator();
    r.addCheckBox(BuResource.BU.getString("T�ches"), "TACHE", true, true);
    r.addCheckBox(DjaResource.DJA.getString("Arbre"), "ARBRE", true, true);
    r.computeMnemonics();
    return r;
  }

  // COMMANDES

  public void cmdCreer() {
    DjaGridInteractive grille = new DjaGridInteractive();
    grille.setInteractive(true);
    nbdoc_++;
    DjaFrame f = new DjaFrame(this, BuResource.BU.getString("SansTitre") + nbdoc_ + ".dja", grille, false);
    f.setLocation(125 + nbdoc_ * 25, -15 + nbdoc_ * 25);
    addInternalFrame(f);
  }

  private static File openDirectory_ = null;
  private static File saveDirectory_ = null;

  // private static int recent_count =1;

  public void cmdOuvrir(String argument) {

    if (!"".equals(argument)) {
      openFile(argument);
      return;
    }

    BuFileChooser chooser = new BuFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setFileHidingEnabled(true);
    chooser.setMultiSelectionEnabled(true);
    chooser.resetChoosableFileFilters();

    if (openDirectory_ == null) {
      String d = BuPreferences.BU.getStringProperty("directory.open");
      if (!d.equals("")) {
        File f = new File(FuLib.expandedPath(d));
        if (f.canRead()) {
          openDirectory_ = f;
        }
      }
    }

    if (openDirectory_ != null) {
      chooser.setCurrentDirectory(openDirectory_);
    }

    chooser.setFileFilter(DjaLoadSaveLib.get(EXTENSIONS[0]).getFilter());

    for (int i = 1; i < EXTENSIONS.length; i++) {
      DjaLoadSaveInterface lsi = DjaLoadSaveLib.get(EXTENSIONS[i]);
      if ((lsi != null) && lsi.canLoad()) {
        chooser.addChoosableFileFilter(lsi.getFilter());
      }
    }

    int returnVal = chooser.showOpenDialog((BuApplication) getApp());

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File f = chooser.getSelectedFile();
      if (f != null) {
        openDirectory_ = f;
      }

      File[] files = chooser.getSelectedFiles();

      // bug swing. selectedFiles is always null!
      if ((files.length == 0) && (f != null)) {
        files = new File[]{f};
      }

      for (int i = 0; i < files.length; i++) {
        String r = files[i].getPath();
        openFile(r);
      }
    }
  }

  public void cmdReouvrir(String arg) {
    openFile(arg);
  }

  public void cmdEnregistrer() {
    cmdEnregistrerSous();
  }

  public void cmdEnregistrerSous() {

    JInternalFrame frame = getCurrentInternalFrame();

    if ((frame != null) && (frame instanceof DjaFrame)) {
      String r = "";

      if (frame instanceof DjaFrame) {
        r = ((DjaFrame) frame).getFile();
      }

      BuFileChooser chooser = new BuFileChooser();
      chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      chooser.setFileHidingEnabled(true);
      chooser.setMultiSelectionEnabled(false);
      chooser.resetChoosableFileFilters();

      if (saveDirectory_ == null) {
        String d = BuPreferences.BU.getStringProperty("directory.save");
        if (!d.equals("")) {
          File f = new File(FuLib.expandedPath(d));
          if (f.canRead()) {
            saveDirectory_ = f;
          }
        }
      }

      if (saveDirectory_ == null) {
        saveDirectory_ = openDirectory_;
      }

      if (saveDirectory_ != null) {
        chooser.setCurrentDirectory(saveDirectory_);
      }

      chooser.setFileFilter(DjaLoadSaveLib.get(EXTENSIONS[0]).getFilter());

      for (int i = 1; i < EXTENSIONS.length; i++) {
        DjaLoadSaveInterface lsi = DjaLoadSaveLib.get(EXTENSIONS[i]);
        if ((lsi != null) && lsi.canSave()) {
          chooser.addChoosableFileFilter(lsi.getFilter());
        }
      }

      chooser.setSelectedFile(new File(r));

      int returnVal = chooser.showSaveDialog((BuApplication) getApp());
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        saveDirectory_ = chooser.getSelectedFile();
        r = saveDirectory_.getPath(); // .getName();
      } else {
        r = null;
      }

      if (r != null) {
        saveFile(r, ((DjaFrame) frame).getGrid());
        ((DjaFrame) frame).setFile(r);
      }
    }
  }

  public void cmdPreference() {
    new BuTaskOperation(this, BuResource.BU.getString("Pr�f�rences")) {
      @Override
      public void act() {
        opPreference();
      }
    }.start();
  }

  public void opPreference() {
    BuMainPanel mp = getMainPanel();
    mp.setProgression(5);

    if (ifPreferences == null) {
      ifPreferences = new BuPreferencesFrame(this);
      mp.setProgression(15);

      ifPreferences.addTab(new BuLookPreferencesPanel(this));
      mp.setProgression(25);
      ifPreferences.addTab(new BuDesktopPreferencesPanel(this));
      mp.setProgression(35);
      ifPreferences.addTab(new BuLanguagePreferencesPanel(this));
      mp.setProgression(45);
      ifPreferences.addTab(new BuBrowserPreferencesPanel(this));
      mp.setProgression(55);

      ifPreferences.addTab(new BuDirWinPreferencesPanel(this));
      mp.setProgression(65);

      ifPreferences.addTab(new BuUserPreferencesPanel(this));
      mp.setProgression(75);
    }

    mp.setProgression(85);
    addInternalFrame(ifPreferences);
    mp.setProgression(0);
  }

  public void cmdFormes() {
    if (plForms == null) {
      plForms = new BuPalette();
      plForms.setName("plFORMES");
      plForms.setTitle(DjaResource.DJA.getString("Formes"));
      plForms.setFrameIcon(DjaResource.DJA.getIcon("dja-box", 16));
      plForms.setContent(new DjaPaletteForm(this));
      addInternalFrame(plForms);
    } else {
      plForms.setVisible(!plForms.isVisible());
    }

    setCheckedForAction("PALETTE_FORMES", plForms.isVisible());
  }

  public void cmdLiens() {
    if (plLinks == null) {
      plLinks = new BuPalette();
      plLinks.setName("plLIENS");
      plLinks.setTitle(DjaResource.DJA.getString("Liens"));
      plLinks.setFrameIcon(DjaResource.DJA.getIcon("dja-brokenarrow", 16));
      plLinks.setContent(new DjaPaletteLink(this));
      addInternalFrame(plLinks);
    } else {
      plLinks.setVisible(!plLinks.isVisible());
    }

    setCheckedForAction("PALETTE_LIENS", plLinks.isVisible());
  }

  public void cmdDiaUml() {
    if (plDiaUml == null) {
      plDiaUml = new BuPalette();
      plDiaUml.setName("plDIAUML");
      plDiaUml.setTitle(DjaResource.DJA.getString("Elts UML"));
      plDiaUml.setFrameIcon(DjaResource.DJA.getIcon("dja-umlclass", 16));
      plDiaUml.setContent(new DjaPaletteUml(this));
      addInternalFrame(plDiaUml);
    } else {
      plDiaUml.setVisible(!plDiaUml.isVisible());
    }

    setCheckedForAction("PALETTE_DIAUML", plDiaUml.isVisible());
  }

  public void cmdExtremites() {
    if (plBrcks == null) {
      plBrcks = new BuPalette();
      plBrcks.setName("plEXTREMITES");
      plBrcks.setTitle(DjaResource.DJA.getString("Extr�mit�s"));
      plBrcks.setFrameIcon(DjaResource.DJA.getIcon("dja-links", 16));
      plBrcks.setContent(new DjaPaletteBracket(this));
      addInternalFrame(plBrcks);
    } else {
      plBrcks.setVisible(!plBrcks.isVisible());
    }

    setCheckedForAction("PALETTE_EXTREMITES", plBrcks.isVisible());
  }

  public void cmdTraits() {
    if (plStrks == null) {
      plStrks = new BuPalette();
      plStrks.setName("plTRAITS");
      plStrks.setTitle(DjaResource.DJA.getString("Traits"));
      plStrks.setFrameIcon(DjaResource.DJA.getIcon("dja-strokes", 16));
      plStrks.setContent(new DjaPaletteStroke(this));
      addInternalFrame(plStrks);
    } else {
      plStrks.setVisible(!plStrks.isVisible());
    }

    setCheckedForAction("PALETTE_TRAITS", plStrks.isVisible());
  }

  public void cmdEpaisseurs() {
    if (plThkns == null) {
      plThkns = new BuPalette();
      plThkns.setName("plEPAISSEURS");
      plThkns.setTitle(DjaResource.DJA.getString("Epaisseurs"));
      plThkns.setFrameIcon(DjaResource.DJA.getIcon("dja-thickness", 16));
      plThkns.setContent(new DjaPaletteThickness(this));
      addInternalFrame(plThkns);
    } else {
      plThkns.setVisible(!plThkns.isVisible());
    }

    setCheckedForAction("PALETTE_EPAISSEURS", plThkns.isVisible());
  }

  // Methodes additionnelles
  String[] EXTENSIONS = new String[]{"Dja", "Dia", "Ser", "Gif", "Jpg", "Ppm"};

  public void openFile(String _path) {
    try {
      DjaLoadSaveInterface ls = getDjaLoadSaveInterface(_path);

      DjaGridInteractive grille = new DjaGridInteractive(true, ls.load(new File(_path)));

      nbdoc_++;
      DjaFrame f = new DjaFrame(this, _path, grille, false);
      f.setLocation(155 + nbdoc_ * 25, -15 + nbdoc_ * 25);
      addInternalFrame(f);
      getMainMenuBar().addRecentFile(_path, "graphe");
    } catch (Exception ex) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Can not open " + _path + "\n" + ex.getMessage())
          .activate();
    }
  }

  public void saveFile(String _path, DjaGrid _grid) {
    try {
      DjaLoadSaveInterface ls = getDjaLoadSaveInterface(_path);

      ls.save(new File(_path), _grid.getObjects());
      getMainMenuBar().addRecentFile(_path, "graphe");
    } catch (Exception ex) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Can not save " + _path + "\n" + ex.getMessage())
          .activate();
    }
  }

  private DjaLoadSaveInterface getDjaLoadSaveInterface(String path) throws Exception {
    DjaLoadSaveInterface ls = null;

    for (int i = 0; i < EXTENSIONS.length; i++) {
      DjaLoadSaveInterface lsi = DjaLoadSaveLib.get(EXTENSIONS[i]);
      if ((lsi != null) && (lsi.getFilter().accept(path))) {
        ls = lsi;
        break;
      }
    }

    if (ls == null) {
      throw new Exception("No filter found");
    }
    return ls;
  }

  @Override
  public void exit() {
    BuInformationsSoftware il = getInformationsSoftware();
    assistant_.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "QUITTER(" + il.name.toUpperCase()
        + ")"));

    DjaPreferences.DJA.writeIniFile();

    super.exit();
  }

  @Override
  public void displayURL(String newUrl) {
    String url = newUrl;
    if (BuPreferences.BU.getIntegerProperty("browser.type", 1) != 1) {
      if (url == null) {
        BuInformationsSoftware il = getInformationsSoftware();
        url = il.http;
      }

      BuBrowserControl.displayURL(url);
    } else {
      if (ifAide == null) {
        ifAide = new BuHelpFrame(this);
      }
      addInternalFrame(ifAide);
      ifAide.setDocumentUrl(url);
    }
  }

  @Override
  public void internalFrameActivated(InternalFrameEvent internalFrameEvent) {
    super.internalFrameActivated(internalFrameEvent);
    refreshTree();
  }

  @Override
  public void internalFrameClosed(InternalFrameEvent internalFrameEvent) {
    super.internalFrameClosed(internalFrameEvent);
    refreshTree();
  }
}
