/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaEllipse.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaEllipse
       extends DjaForm
{
  public DjaEllipse(String _texte)
  {
    super(_texte);
  }

  public DjaEllipse()
  {
    this(null);
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    Rectangle b=getBounds();
    boolean   r=b.contains(_x,_y);

    if(r)
    {
      int dx=1280*((_x-b.x)-b.width /2)/Math.max(1,b.width );
      int dy=1280*((_y-b.y)-b.height/2)/Math.max(1,b.height);
      int distance=dx*dx+dy*dy;
      if(distance>1280*1280/4) r=false;
      // System.err.println("ELLIPSE: "+dx+","+dy+" --> "+r);
      // System.err.println(" "+distance+">"+(1280*1280/4));
    }

    return r;
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,EAST ,x+w-1,y+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w/2,y+h-1);
    r[6]=new DjaAnchor(this,6,WEST ,x    ,y+h/2);

    if(w>=h)
    {
      r[1]=new DjaAnchor(this,1,NORTH,x+3*w/4,y+    134*h/2000);
      r[3]=new DjaAnchor(this,3,SOUTH,x+3*w/4,y+h-1-134*h/2000);
      r[5]=new DjaAnchor(this,5,SOUTH,x+  w/4,y+h-1-134*h/2000);
      r[7]=new DjaAnchor(this,7,NORTH,x+  w/4,y+    134*h/2000);
    }
    else
    {
      r[1]=new DjaAnchor(this,1,EAST,x+w-1-134*w/2000,y+  h/4);
      r[3]=new DjaAnchor(this,3,EAST,x+w-1-134*w/2000,y+3*h/4);
      r[5]=new DjaAnchor(this,5,WEST,x+    134*w/2000,y+3*h/4);
      r[7]=new DjaAnchor(this,7,WEST,x+    134*w/2000,y+  h/4);
    }

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Color bg=getBackground();
    Color fg=getForeground();

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillOval(_g,x,y,w-1,h-1);
    }

    if(fg!=null)
    {
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.drawOval(_g,x,y,w-1,h-1,bp);
    }

    super.paintObject(_g);
  }
}
