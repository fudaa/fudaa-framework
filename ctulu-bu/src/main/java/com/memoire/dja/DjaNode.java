/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaNode.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import javax.swing.Icon;

public abstract class DjaNode
{
  abstract int     getChildCount  ();
  abstract boolean isLeaf         ();
  abstract Object  getChild       (int _index);
  abstract int     getIndexOfChild(Object _child);

  public Object getValue() { return null; }
  public String getTitle() { return toString(); }
  public Icon   getIcon () { return null; }
}

