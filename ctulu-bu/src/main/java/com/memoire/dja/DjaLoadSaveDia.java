/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaLoadSaveDia.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuFileFilter;
import com.memoire.fu.FuLib;
import com.memoire.mst.MstXmlParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;

public class DjaLoadSaveDia
       implements DjaLoadSaveInterface
{
  @Override
  public boolean canLoad() { return true;  }
  @Override
  public boolean canSave() { return false; }

  @Override
  public DjaVector load(File _file)
    throws Exception
  {
    InputStream input=new BufferedInputStream
      (new FileInputStream(_file));

    /*
    Enumeration e=System.getProperties().keys();
    while(e.hasMoreElements())
    {
      String k=(String)e.nextElement();
      System.err.println(k+"="+System.getProperty(k));
    }
    System.err.println("ENCODING="+System.getProperty("file.encoding"));
    */

    if(FuLib.isGziped(_file.getAbsolutePath()))
    {
      //System.err.println("the file "+_file.getPath()+" is gziped.");
      input=new GZIPInputStream(input);
    }
    //else System.err.println("the file "+_file.getPath()+" is NOT gziped.");

    String url=_file.getAbsolutePath();
    if(url.indexOf(':')<0) url="file://"+new File(url).getAbsolutePath();

    DjaConvertDia handler=new DjaConvertDia(_file.getAbsolutePath());

    MstXmlParser parser=new MstXmlParser();
    parser.setHandler(handler);
    parser.parse(url,null,input,null); 
    //try { parser.parse(url,null,input,null); }
    //catch (Exception ex) { ex.printStackTrace(); }

    DjaVector r=handler.getVector();
    if(r!=null) connect(r);
    return r;
  }

  public void connect(DjaVector _g)
  {
    Enumeration eo=_g.enumerate(DjaObject.class);
    Enumeration el=_g.enumerate(DjaLink.class);

    while(el.hasMoreElements())
    {
      DjaLink     l=(DjaLink)el.nextElement();
      DjaAttach[] t=l.getAttachs();

      while(eo.hasMoreElements())
      {
	DjaObject   o=(DjaObject)eo.nextElement();
	DjaAnchor[] a=o.getAnchors(); 
	if(o!=l)
	{
	  for(int i=0;i<t.length;i++)
	    for(int j=0;j<a.length;j++)
	      if(DjaLib.close(t[i].getX(),t[i].getY(),a[j].getX(),a[j].getY()))
	      {
		if(t[i].getP()==0)
		{
		  l.setBeginObject  (o);
		  l.setBeginPosition(a[j].getPosition());
		}
		else
		{
		  l.setEndObject  (o);
		  l.setEndPosition(a[j].getPosition());
		}
	      }
	}
      }
    }
  }

  @Override
  public void save(File _file, DjaVector _vector)
    throws Exception
  {
    throw new Exception("Not yet implemented");
  }

  @Override
  public BuFileFilter getFilter()
  {
    return new BuFileFilter
      (new String[] { "dia","DIA" },
      "Gnome Dia files");
  }
}
