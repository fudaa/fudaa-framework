/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaManipulator.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;



public abstract class DjaManipulator
       implements DjaOwner, DjaOptions
{
  @Override
  public DjaGrid getGrid()
  {
    DjaGrid  r=null;

    DjaOwner o=getOwner();
    while(o!=null)
    {
      if(o instanceof DjaGrid)
	{ r=(DjaGrid)o; break; }
      o=o.getOwner();
    }

    return r;
  }

  public void fireGridEvent(DjaOwner _object,int _id)
  {
    DjaGrid grid=getGrid();
    if(grid!=null) grid.fireGridEvent(_object,_id);
  }

  @Override
  public abstract DjaOwner getOwner();
}
