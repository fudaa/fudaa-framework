/**
 * @modification $Date: 2007-03-19 13:25:37 $
 * @statut unstable
 * @file DjaMouseListener.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.BuDialogInput;
import com.memoire.bu.BuDialogMulti;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Enumeration;

public class DjaMouseListener
        implements DjaOptions,
        MouseListener, MouseMotionListener,
        KeyListener // not serializable
{

  protected int xm_, ym_;
  protected boolean ctrl_, shift_;

  protected DjaGridInteractive grid_;
  protected DjaObject last_;
  protected int lastdx_;
  protected int lastdy_;
  protected int lastx_;
  protected int lasty_;
  protected int lastw_;
  protected int lasth_;
  protected DjaHandle handle_;
  protected DjaControl control_;
  protected DjaText text_;
  protected boolean linkbegin_;
  protected boolean linkend_;
  private boolean enable = true;

  public DjaMouseListener(DjaGridInteractive _grid) {
    grid_ = _grid;
  }

  public void setEnable(boolean enable) {
    this.enable = enable;
  }

  @Override
  public void keyTyped(KeyEvent _evt) {
  }

  @Override
  public void keyPressed(KeyEvent _evt) {
    if (_evt.isControlDown()) {
      ctrl_ = true;
    }
    if (_evt.isShiftDown()) {
      shift_ = true;
    }
    adjustCursor();
  }

  @Override
  public void keyReleased(KeyEvent _evt) {
    if (_evt.isControlDown()) {
      ctrl_ = false;
    }
    if (_evt.isShiftDown()) {
      shift_ = false;
    }
    adjustCursor();
  }

  @Override
  public void mouseClicked(MouseEvent _evt) {
    if (!enable) {
      return;
    }
    Object o = DjaLibDetect.detect(grid_, getScaledX(_evt), getScaledY(_evt));

    if (o instanceof DjaHandle) {
      return;
    }
    if (o instanceof DjaControl) {
      return;
    }
    if (o instanceof DjaAttach) {
      return;
    }
    if (o instanceof DjaAnchor) {
      return;
    }
    if (linkbegin_ || linkend_) {
      return;
    }

    if (o instanceof DjaText) {
      DjaText t = (DjaText) o;
      grid_.repaint(t.getBounds());

      if (_evt.isControlDown()) {
      } else if (_evt.isShiftDown()) {
        t.setSelected(!t.isSelected());
      } else {
        String s = t.getText();

        if (t.isMultiline()) {
          BuDialogMulti d = new BuDialogMulti(null, DjaLib.SOFTWARE, "Text", "Edit the text:", s);
          d.activate();
          s = d.getValue();
        } else {
          BuDialogInput d = new BuDialogInput(null, DjaLib.SOFTWARE, "Text", "Edit the text:", s);
          d.activate();
          s = d.getValue();
        }

        if (s != null) {
          if ("".equals(s)) {
            s = null;
          }
          /*
           if(t.getNum()==-1)
           t.getParent().setText(s,0);
           else
           */
          t.setText(s);
        }
      }

      last_ = null;
      adjustCursor(_evt);
      grid_.repaint(t.getBounds());
      return;
    }

    if (last_ != null) {
      if (_evt.isShiftDown() || _evt.isControlDown()) {
        if (!last_.isSelected()) {
          grid_.addSelection(last_);
        } else {
          grid_.removeSelection(last_);
        }
      } else {
        if (!last_.isSelected()) {
          grid_.setSelection(last_);
        } else {
          grid_.clearSelection();
        }
      }

      last_ = null;
    }

    adjustCursor(_evt);
  }

  @Override
  public void mousePressed(MouseEvent _evt) {
    if (!enable) {
      return;
    }
    int x = getScaledX(_evt);
    int y = getScaledY(_evt);
    Object o = DjaLibDetect.detect(grid_, x, y);

    mousePressed(_evt, x, y, o);
  }

  protected void mousePressed(MouseEvent _evt, int x, int y, Object o) {
    grid_.requestFocus();

    last_ = null;
    handle_ = null;
    control_ = null;
    text_ = null;
    linkbegin_ = false;
    linkend_ = false;

    if (o instanceof DjaHandle) {
      handle_ = (DjaHandle) o;
      last_ = handle_.getParent();
      lastdx_ = x;
      lastdy_ = y;
      lastx_ = last_.getX();
      lasty_ = last_.getY();
      lastw_ = last_.getWidth();
      lasth_ = last_.getHeight();
      return;
    } else if (o instanceof DjaControl) {
      control_ = (DjaControl) o;
      last_ = control_.getParent();
      lastdx_ = x;
      lastdy_ = y;
      lastx_ = last_.getX();
      lasty_ = last_.getY();
      lastw_ = last_.getWidth();
      lasth_ = last_.getHeight();
      return;
    } else if (o instanceof DjaAnchor) {
      return;
    } else if (o instanceof DjaText) {
      text_ = (DjaText) o;
      Point q = text_.getLocation();
      last_ = text_.getParent();
      lastx_ = last_.getX();
      lasty_ = last_.getY();
      lastw_ = last_.getWidth();
      lasth_ = last_.getHeight();
      lastdx_ = x - q.x + lastx_;
      lastdy_ = y - q.y + lasty_;
      return;
    } else if (o instanceof DjaAttach) {
      DjaAttach a = (DjaAttach) o;
      DjaObject w = a.getParent();
      if (w instanceof DjaLink) {
        DjaLink l = (DjaLink) w;
        if (a.getP() == 0) {
          last_ = l;
          lastdx_ = a.getX() - x;
          lastdy_ = a.getY() - y;
          linkbegin_ = true;
        } else if (a.getP() == 1) {
          last_ = l;
          lastdx_ = a.getX() - x;
          lastdy_ = a.getY() - y;
          linkend_ = true;
        }
      }

      adjustCursor(_evt);
      return;
    } else if (o instanceof DjaObject) {
      DjaObject f = (DjaObject) o;
      last_ = f;
      lastx_ = f.getX();
      lasty_ = f.getY();
      lastw_ = f.getWidth();
      lasth_ = f.getHeight();
      lastdx_ = f.getX() - x;
      lastdy_ = f.getY() - y;
    }

    if (last_ == null) {
      lastx_ = x;
      lasty_ = y;
      lastw_ = 0;
      lasth_ = 0;

      Graphics g = grid_.getGraphics();
      g.setXORMode(selectionZone);
      lastXDrawn = grid_.getTransform().getUnScaledX(lastx_, true);
      lastYDrawn = grid_.getTransform().getUnScaledY(lasty_, true);
      lastWDrawn = grid_.getTransform().getUnScaledX(lastw_, false);
      lastHDrawn = grid_.getTransform().getUnScaledY(lasth_, false);
      g.drawRect(lastXDrawn, lastYDrawn, lastWDrawn, lastHDrawn);
      g.setPaintMode();
    }

    adjustCursor(_evt);
  }

  @Override
  public void mouseReleased(MouseEvent _evt) {
    if (!enable) {
      return;
    }
    Rectangle r = null;

    if (last_ != null) {
      r = DjaLib.getDirtyArea(last_);

      int x, y;

      if (last_ instanceof DjaLink) {
        DjaLink a = (DjaLink) last_;

        if (linkbegin_) {
          x = a.getBeginX();
          y = a.getBeginY();
          Object o = DjaLibDetect.detect(grid_, x, y, a, DjaLibDetect.ANCHORS);
          if (o instanceof DjaAnchor) {
            DjaAnchor n = (DjaAnchor) o;
            a.setBeginObject(n.getParent());
            a.setBeginPosition(n.getPosition());
          } else {
            Point p = new Point(x, y);
            DjaLib.snap(p);
            a.setBeginX(p.x);
            a.setBeginY(p.y);
          }
        } else if (linkend_) {
          x = a.getEndX();
          y = a.getEndY();
          Object o = DjaLibDetect.detect(grid_, x, y, a, DjaLibDetect.ANCHORS);
          if (o instanceof DjaAnchor) {
            DjaAnchor n = (DjaAnchor) o;
            a.setEndObject(n.getParent());
            a.setEndPosition(n.getPosition());
          } else {
            Point p = new Point(x, y);
            DjaLib.snap(p);
            a.setEndX(p.x);
            a.setEndY(p.y);
          }
        } else {
          DjaLib.snap(a);
        }
      } else {
        DjaLib.snap(last_);
      }

      r = DjaLib.getDirtyArea(last_).union(r);

      /*
       x=last_.getX()+deltaX/2;
       y=last_.getY()+deltaY/2;
       x-=x%deltaX;
       y-=y%deltaY;
       last_.setX(x);
       last_.setY(y);

       int w=last_.getWidth()+deltaX/2;
       int h=last_.getHeight()+deltaY/2;
       w-=w%deltaX;
       h-=h%deltaY;
       last_.setWidth(w);
       last_.setHeight(h);
       */
      if (r != null) {
        grid_.repaint(r);
      }
    } else {
      Graphics g = grid_.getGraphics();
      g.setXORMode(selectionZone);
      g.drawRect(lastXDrawn, lastYDrawn, lastWDrawn, lastHDrawn);
      g.setPaintMode();

      Rectangle z = new Rectangle(lastx_, lasty_, lastw_, lasth_);
      DjaVector v = new DjaVector();
      for (Enumeration e = grid_.getObjects().elements(); e.hasMoreElements();) {
        DjaObject f = (DjaObject) e.nextElement();
        if (z.union(f.getBounds()).equals(z)) {
          v.addElement(f);
        }
      }
      grid_.setSelection(v);
    }

    adjustCursor(_evt);

  }

  @Override
  public void mouseEntered(MouseEvent _evt) {
  }

  @Override
  public void mouseExited(MouseEvent _evt) {
  }

  @Override
  public void mouseMoved(MouseEvent _evt) {
    if (!enable) {
      return;
    }
    adjustCursor(_evt);
  }

  @Override
  public void mouseDragged(MouseEvent _evt) {
    if (!enable) {
      return;
    }
    boolean erase = true;

    int x = getScaledX(_evt);
    int y = getScaledY(_evt);

    Rectangle r = null;

    if (handle_ != null) {
      DjaForm f = (DjaForm) handle_.getParent();
      r = DjaLib.getDirtyArea(f);
      //grid_.repaint(f.getExtendedBounds());

      /*int w=f.getWidth();
       int h=f.getHeight();*/
      // System.err.println("EVT_X="+x);
      // System.err.println("EVT_Y="+y);
      // System.err.println("LST_X="+lastdx_);
      // System.err.println("LST_Y="+lastdy_);
      // System.err.println("");
      switch (handle_.getO()) {
        case NORTH:
          f.setY(lasty_ + Math.min(lasth_, y - lastdy_));
          f.setHeight(Math.max(0, lasth_ - y + lastdy_));
          break;
        case WEST:
          f.setX(lastx_ + Math.min(lastw_, x - lastdx_));
          f.setWidth(Math.max(0, lastw_ - x + lastdx_));
          break;
        case EAST:
          f.setWidth(Math.max(0, lastw_ + x - lastdx_));
          break;
        case SOUTH:
          f.setHeight(Math.max(0, lasth_ + y - lastdy_));
          break;
        case NORTH_WEST:
          f.setX(lastx_ + Math.min(lastw_, x - lastdx_));
          f.setY(lasty_ + Math.min(lasth_, y - lastdy_));
          f.setWidth(Math.max(0, lastw_ - x + lastdx_));
          f.setHeight(Math.max(0, lasth_ - y + lastdy_));
          break;
        case NORTH_EAST:
          f.setY(lasty_ + Math.min(lasth_, y - lastdy_));
          f.setWidth(Math.max(0, lastw_ + x - lastdx_));
          f.setHeight(Math.max(0, lasth_ - y + lastdy_));
          break;
        case SOUTH_EAST:
          f.setWidth(Math.max(0, lastw_ + x - lastdx_));
          f.setHeight(Math.max(0, lasth_ + y - lastdy_));
          break;
        case SOUTH_WEST:
          f.setX(lastx_ + Math.min(lastw_, x - lastdx_));
          f.setWidth(Math.max(0, lastw_ - x + lastdx_));
          f.setHeight(Math.max(0, lasth_ + y - lastdy_));
          break;
      }

      r = DjaLib.getDirtyArea(f).union(r);
      //grid_.repaint(f.getExtendedBounds());
    } else if (control_ != null) {
      r = DjaLib.getDirtyArea(control_.getParent());
      //grid_.repaint(control_.getParent().getExtendedBounds());
      control_.draggedTo(x, y);
      r = DjaLib.getDirtyArea(control_.getParent()).union(r);
      //grid_.repaint(control_.getParent().getExtendedBounds());
    } else if (text_ != null) {
      if (_evt.isControlDown()) {
        if (text_.isMoveable()) {
          r = text_.getBounds();
          //grid_.repaint(text_.getBounds());
          Point p = new Point(lastx_ + x - lastdx_, lasty_ + y - lastdy_);
          //DjaLib.snap(p);
          text_.setLocation(p);
          text_.setSelected(true);
          r = text_.getBounds().union(r);
          //grid_.repaint(text_.getBounds());
        }
      }
    } else if (last_ != null) {
      if (last_ instanceof DjaForm) {
        if (!last_.isSelected()) {
          DjaForm f = (DjaForm) last_;
          r = DjaLib.getDirtyArea(f);
          //Rectangle r=f.getExtendedBounds();
          f.setX(x + lastdx_);
          f.setY(y + lastdy_);
          r = DjaLib.getDirtyArea(f).union(r);
          //grid_.repaint(f.getExtendedBounds().union(r));
        } else {
          grid_.moveSelection(x - lastx_ + lastdx_, y - lasty_ + lastdy_);
          lastx_ = last_.getX();
          lasty_ = last_.getY();
        }
      } else if (last_ instanceof DjaLink) {
        DjaLink a = (DjaLink) last_;
        r = DjaLib.getDirtyArea(a);
        //grid_.repaint(a.getExtendedBounds());
        if (linkend_) {
          a.setEndObject(null);
          a.setEndX(x + lastdx_);
          a.setEndY(y + lastdy_);
        } else if (linkbegin_) {
          a.setBeginObject(null);
          a.setBeginX(x + lastdx_);
          a.setBeginY(y + lastdy_);
        } else {
          a.setX(x + lastdx_);
          a.setY(y + lastdy_);
        }
        r = DjaLib.getDirtyArea(a).union(r);
        //grid_.repaint(a.getExtendedBounds());
      }

      // essai
      /*
       if(!last_.isSelected()&&!_evt.isShiftDown()&&!_evt.isControlDown())
       grid_.setSelection(last_);
       */
      //grid_.repaint();
    } else {
      erase = true;
    }

    if (r != null) {
      grid_.repaint(r);
    }

    Graphics g = grid_.getGraphics();
    g.setXORMode(selectionZone);

    if (last_ == null) {
      if (erase) {
        g.drawRect(lastXDrawn, lastYDrawn, lastWDrawn, lastHDrawn);
      }
      lastw_ = x - lastx_;
      lasth_ = y - lasty_;
      lastXDrawn = grid_.getTransform().getUnScaledX(lastx_, true);
      lastYDrawn = grid_.getTransform().getUnScaledY(lasty_, true);
      lastWDrawn = grid_.getTransform().getUnScaledX(lastw_, false);
      lastHDrawn = grid_.getTransform().getUnScaledY(lasth_, false);
      g.drawRect(lastXDrawn, lastYDrawn, lastWDrawn, lastHDrawn);
      g.setPaintMode();
    }
  }

  int lastXDrawn;
  int lastYDrawn;
  int lastWDrawn;
  int lastHDrawn;

  protected void adjustCursor() {
    adjustCursor(xm_, ym_, shift_, ctrl_);
  }

  protected int getScaledX(MouseEvent _evt) {
    return grid_.getTransform().getScaledX(_evt);
  }

  protected int getScaledX(MouseEvent _evt, boolean translation) {
    return grid_.getTransform().getScaledX(_evt, translation);
  }

  protected int getScaledY(MouseEvent _evt, boolean translation) {
    return grid_.getTransform().getScaledY(_evt, translation);
  }

  protected int getScaledY(MouseEvent _evt) {
    return grid_.getTransform().getScaledY(_evt);
  }

  protected void adjustCursor(MouseEvent _evt) {
    xm_ = getScaledX(_evt);
    ym_ = getScaledY(_evt);
    boolean shift = _evt.isShiftDown();
    boolean ctrl = _evt.isControlDown();
    adjustCursor(xm_, ym_, shift, ctrl);
  }

  protected void adjustCursor(int _x, int _y, boolean _shift, boolean _ctrl) {
    int r = Cursor.CROSSHAIR_CURSOR;

    Object o = DjaLibDetect.detect(grid_, _x, _y);

    if (o instanceof DjaHandle) {
      DjaHandle h = (DjaHandle) o;
      switch (h.getO()) {
        case NORTH:
          r = Cursor.N_RESIZE_CURSOR;
          break;
        case EAST:
          r = Cursor.E_RESIZE_CURSOR;
          break;
        case SOUTH:
          r = Cursor.S_RESIZE_CURSOR;
          break;
        case WEST:
          r = Cursor.W_RESIZE_CURSOR;
          break;
        case NORTH_EAST:
          r = Cursor.NE_RESIZE_CURSOR;
          break;
        case SOUTH_EAST:
          r = Cursor.SE_RESIZE_CURSOR;
          break;
        case SOUTH_WEST:
          r = Cursor.SW_RESIZE_CURSOR;
          break;
        case NORTH_WEST:
          r = Cursor.NW_RESIZE_CURSOR;
          break;
      }
    } else if (o instanceof DjaControl) {
      r = Cursor.MOVE_CURSOR;
    } else if (o instanceof DjaAttach) {
      r = Cursor.MOVE_CURSOR;
    } else if (o instanceof DjaAnchor) {
    } else if (o instanceof DjaText) {
      if (_ctrl) {
        if (((DjaText) o).isMoveable()) {
          r = Cursor.MOVE_CURSOR;
        } else {
          r = Cursor.CROSSHAIR_CURSOR;
        }
      } else if (_shift) {
        r = Cursor.CROSSHAIR_CURSOR;
      } else {
        r = Cursor.TEXT_CURSOR;
      }
    } else if (o instanceof DjaObject) {
      //if(((DjaObject)o).isSelected())
      r = Cursor.MOVE_CURSOR;
    }

    Cursor c = Cursor.getPredefinedCursor(r);
    if (grid_.getCursor() != c) {
      grid_.setCursor(c);
    }
  }
}
