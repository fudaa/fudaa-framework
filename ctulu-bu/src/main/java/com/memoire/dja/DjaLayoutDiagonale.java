/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaLayoutDiagonale.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.util.Enumeration;

public class DjaLayoutDiagonale
  implements DjaLayoutInterface
{
  public DjaLayoutDiagonale()
  {
  }

  @Override
  public void layout(DjaGrid _grid)
  {
    DjaVector v=new DjaVector();
    for(Enumeration e=_grid.enumerate(DjaForm.class);
	e.hasMoreElements(); )
      v.addElement(e.nextElement());

    int n=v.size();
    int x=10;
    int y=10;
    for(int i=0;i<n;i++)
    {
      DjaForm f=(DjaForm)v.elementAt(i);
      f.setX(x);
      f.setY(y);
      DjaLib.snap(f);

      x+=f.getWidth()+5;
      x+=10-x%10;
      y+=f.getHeight()+5;
      y+=10-y%10;
    }
  }
}
