/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaLoadSaveInterface.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuFileFilter;
import java.io.File;

interface DjaLoadSaveInterface
{
  boolean      canLoad();
  boolean      canSave();
  DjaVector    load(File _file)                    throws Exception;
  void         save(File _file, DjaVector _vector) throws Exception;
  BuFileFilter getFilter();
}
