/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaHLine.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaHLine
       extends DjaForm
{
  public DjaHLine(String _texte)
  {
    super(_texte,NORTH);
  }

  public DjaHLine()
  {
    this(null);
  }

  @Override
  public int getHeight()
  {
    return 1;
  }

  @Override
  public void setHeight(int _h)
  {
    super.setHeight(1);
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    return new Rectangle(getX(),getY()-3,getWidth(),7).contains(_x,_y);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();

    DjaAnchor[] r=new DjaAnchor[2];
    r[0]=new DjaAnchor(this,0,WEST ,x    ,y);
    r[1]=new DjaAnchor(this,1,EAST ,x+w-1,y);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();

    DjaHandle[] r=new DjaHandle[2];
    r[0]=new DjaHandle(this,WEST      ,x    -deltaX,y);
    r[1]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    Color fg=getForeground();
    if(fg!=null)
    {
      _g.setColor(fg);

      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.drawLine(_g,getX(),getY(),getX()+getWidth(),getY(),bp);
    }

    super.paintObject(_g);
  }
}
