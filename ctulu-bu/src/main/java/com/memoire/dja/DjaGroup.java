/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaGroup.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Enumeration;

public class DjaGroup
       extends DjaForm
{
  public DjaGroup(String _text)
  {
    super(_text,NORTH);

    objects_=new DjaVector();
    setForeground(null);
    setBackground(null);
  }

  public DjaGroup()
  {
    this(null);
  }

  private DjaVector objects_;
  public  DjaVector getObjects() { return objects_; }

  @Override
  public void addText(String _text)
  {
    DjaText t=new DjaText(this,-1,_text);
    int p=CENTER;
    switch(getTexts().length)
    {
    case 0: p=NORTH;  break;
    case 1: p=SOUTH;  break;
    case 2: p=WEST;   break;
    case 3: p=EAST;   break;
    }
    t.setPosition(p);
    addText(t);
  }

  @Override
  public int getX()
  {
    int xmin=Integer.MAX_VALUE;

    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      xmin=Math.min(xmin,o.getX());
    }

    if(xmin==Integer.MAX_VALUE) xmin=0;

    return xmin;
  }

  @Override
  public int getY()
  {
    int ymin=Integer.MAX_VALUE;

    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      ymin=Math.min(ymin,o.getY());
    }

    if(ymin==Integer.MAX_VALUE) ymin=0;

    return ymin;
  }

  @Override
  public int getWidth()
  {
    int xmax=-Integer.MAX_VALUE;

    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      xmax=Math.max(xmax,o.getX()+o.getWidth() );
    }

    if(xmax==-Integer.MAX_VALUE) xmax=2*deltaX;
    else                         xmax-=getX();

    return xmax;
  }

  @Override
  public int getHeight()
  {
    int ymax=-Integer.MAX_VALUE;

    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      ymax=Math.max(ymax,o.getY()+o.getHeight());
    }

    if(ymax==-Integer.MAX_VALUE) ymax=2*deltaY;
    else                         ymax-=getY();

    return ymax;
  }

  @Override
  public void setX(int _x)
  {
    int dx=getX()-_x;

    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      //if(o instanceof DjaForm)
      //((DjaForm) o).setX(o.getX()-dx);
      o.setX(o.getX()-dx);
    }

    super.setX(_x);
  }

  @Override
  public void setY(int _y)
  {
    int dy=getY()-_y;

    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      //if(o instanceof DjaForm)
      //((DjaForm) o).setY(o.getY()-dy);
      o.setY(o.getY()-dy);
    }

    super.setY(_y);
  }

  @Override
  public void setForeground(Color _foreground)
  {
    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      o.setForeground(_foreground);
    }
  }

  @Override
  public void setBackground(Color _background)
  {
    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      o.setBackground(_background);
    }
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    if(objects_==null) return super.getAnchors();

    /*int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();*/

    DjaVector v=new DjaVector();
    //int       n=0;

    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject   o=(DjaObject)e.nextElement();
      DjaAnchor[] a=o.getAnchors();

      for(int j=0;j<a.length;j++)
      {
	v.addElement(a[j]);

	/*
	int xf=a[j].getX();
	int yf=a[j].getY();

	     if(y    ==yf) v.addElement(new DjaAnchor(this,n++,NORTH,xf,yf));
	else if(x    ==xf) v.addElement(new DjaAnchor(this,n++,WEST ,xf,yf));
	else if(y+h-1==yf) v.addElement(new DjaAnchor(this,n++,SOUTH ,xf,yf));
	else if(x+w-1==xf) v.addElement(new DjaAnchor(this,n++,EAST ,xf,yf));
	*/
      }
    }

    DjaAnchor[] r=new DjaAnchor[v.size()];
    for(int i=0;i<r.length;i++)
      r[i]=(DjaAnchor)v.elementAt(i);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    return new DjaHandle[0];
  }

  public void add(DjaObject _object)
  {
    add(_object,false);
  }

  public void add(DjaObject _object, boolean _quiet)
  {
    if(objects_!=null)
    {
      if(_object.getOwner()!=null)
	throw new RuntimeException("belong to "+_object.getOwner());
      _object.setSelected(false);
      objects_.addElement(_object);
      _object.setOwner(this);

      if(!_quiet) fireGridEvent(_object,DjaGridEvent.ADDED);
    }
  }

  public void remove(DjaObject _object)
  {
    remove(_object,false);
  }

  public void remove(DjaObject _object, boolean _quiet)
  {
    if(objects_!=null)
    {
      if(_object.getOwner()!=this)
	throw new RuntimeException("doesn't belong to "+this);
      objects_.removeElement(_object);
      _object.setOwner(null);

      if(!_quiet) fireGridEvent(_object,DjaGridEvent.REMOVED);
    }
  }

  @Override
  public void paintObject(Graphics _g)
  {
    if(objects_!=null)
    {
	Rectangle clip=_g.getClipBounds();

	for(Enumeration e=objects_.reverseElements(); e.hasMoreElements(); )
	{
	    DjaObject o=(DjaObject)e.nextElement();
	    if(!clip.intersects(o.getBounds())) continue;
	    o.paint(_g);
	}
    }
   
    super.paintObject(_g);
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    if(objects_!=null)
    for(Enumeration e=objects_.elements(); e.hasMoreElements(); )
    {
      DjaObject o=(DjaObject)e.nextElement();
      o.paintInteractive(_g);
    }

    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    putProperty("epaisseur","3");
    putProperty("trace","3.5");
    _g.setColor(isSelected() ? selectionForeground : Color.gray);
    _g.drawRect(x-2,y-2,w+3,h+3);
    DjaGraphics.drawRect(_g,x-2,y-2,w+3,h+3,
			 DjaGraphics.getBresenhamParams(this));

    super.paintInteractive(_g);
  }
}
