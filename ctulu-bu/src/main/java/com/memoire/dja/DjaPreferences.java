/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaPreferences.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.fu.FuPreferences;


/**
 * La classe de preferences pour Dja
 */
public class DjaPreferences
       extends FuPreferences
{
  public static final DjaPreferences DJA=new DjaPreferences();

  protected DjaPreferences() { }
}
