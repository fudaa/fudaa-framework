/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut unstable
 * @file DjaLoadSaveSer.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.BuFileFilter;

import java.io.*;

public class DjaLoadSaveSer
    implements DjaLoadSaveInterface {
  @Override
  public boolean canLoad() {
    return true;
  }

  @Override
  public boolean canSave() {
    return true;
  }

  @Override
  public DjaVector load(File _file)
      throws Exception {
    DjaVector djaVector;
    try (final ObjectInputStream objectInputStream = new ObjectInputStream
        (new BufferedInputStream(new FileInputStream(_file)))) {
      djaVector = (DjaVector) objectInputStream
          .readObject();
    }
    return djaVector;
  }

  @Override
  public void save(File _file, DjaVector _vector)
      throws Exception {
    try (final ObjectOutputStream objectOutputStream = new ObjectOutputStream
        (new BufferedOutputStream(new FileOutputStream(_file)))) {

      objectOutputStream.writeObject(_vector);
    }
  }

  @Override
  public BuFileFilter getFilter() {
    return new BuFileFilter
        (new String[]{"ser", "SER"},
            "Dja serialized grids");
  }
}
