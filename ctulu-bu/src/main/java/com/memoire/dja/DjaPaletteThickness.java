/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaPaletteThickness.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.border.EmptyBorder;

public class DjaPaletteThickness
       extends BuPanel
       implements DjaOptions
{
  BuButton[]   buttons_;
  BuGridLayout layout_;

  public DjaPaletteThickness(ActionListener _al)
  {
    super();

    layout_=new BuGridLayout(4,2,2);
    layout_.setCfilled(false);
    setLayout(layout_);
    setBorder(new EmptyBorder(2,2,2,2));

    buttons_=new BuButton[8];

    for(int i=0; i<8; i++)
    {
      Icon icon=new ThicknessIcon(i+1);

      buttons_[i]=new BuButton();
      buttons_[i].setIcon(icon);
      buttons_[i].setRolloverIcon(icon);
      buttons_[i].setMargin(new Insets(1,1,1,1));
      buttons_[i].setRequestFocusEnabled(false);
      buttons_[i].setToolTipText(/* "Set thickness to "+ */ ""+(i+1));
      buttons_[i].setActionCommand("DJA_THICKNESS("+(i+1)+")");

      add(buttons_[i]);
      buttons_[i].addActionListener(_al);
    }
  }

  private class ThicknessIcon implements Icon
  {
    private int t_;

    public ThicknessIcon(int _t)
    {
      t_=_t;
    }

    @Override
    public int getIconWidth () { return 16; }
    @Override
    public int getIconHeight() { return 16; }

    @Override
    public void paintIcon(Component _c, Graphics _g, int _x, int _y)
    {
      _g.setColor(_c.getForeground());
      int x=_x+1;
      int y=_y+1;

      DjaGraphics.BresenhamParams bp=
	new DjaGraphics.BresenhamParams(1,0,t_);
      DjaGraphics.drawLine(_g,x+2,y+13,x+13,y+2,bp);
    }
  }
}







