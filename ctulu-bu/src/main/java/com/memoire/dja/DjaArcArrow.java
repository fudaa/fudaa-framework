/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut unstable
 * @file DjaArcArrow.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;

public class DjaArcArrow
        extends DjaLink {

  protected int xr0, yr0, xr1, yr1;

  public DjaArcArrow(String _text) {
    super();

    yend_ = ybegin_;
    ybegin_ += deltaY;
    obegin_ = NORTH;
    oend_ = SOUTH;
    //putProperty("courbure","20.");

    xr0 = xbegin_;
    yr0 = ybegin_;
    xr1 = xend_;
    yr1 = yend_;

    if (_text != null) {
      addText(_text);
    }
  }

  public DjaArcArrow() {
    this(null);
  }

  public int getMiddleX() {
    return (xr0 + xr1) / 2;
  }

  public int getMiddleY() {
    return (yr0 + yr1) / 2;
  }

  private static class PC extends DjaControl {

    public PC(DjaObject _f, int _p, int _o, int _x, int _y) {
      super(_f, _p, _o, _x, _y);
    }

    @Override
    public void draggedTo(int _x, int _y) {
      DjaArcArrow p = (DjaArcArrow) getParent();
      p.setCourbure(_x, _y);
    }
  }

  public double getCourbure() {
    double r = Math.PI * 1.25;

    String courbure = getProperty("courbure");
    if (courbure != null) {
      r *= new Double(courbure).doubleValue()
              / Math.sqrt((xr0 - xr1) * (xr0 - xr1) + (yr0 - yr1) * (yr0 - yr1));
    }

    return r;
  }

  public void setCourbure(int _x, int _y) {
    int xm = getMiddleX();
    int ym = getMiddleY();
    double c = Math.sqrt((xm - _x) * (xm - _x) + (ym - _y) * (ym - _y));
    if ((xr0 - xm) * (_y - ym) - (yr0 - ym) * (_x - xm) > 0) {
      c = -c;
    }
    putProperty("courbure", "" + c);
  }

  @Override
  public Rectangle getExtendedBounds() {
    double c = getCourbure();
    int vx0 = (int) ((yr0 - yr1) * c);
    int vy0 = (int) ((xr1 - xr0) * c);
    int vx1 = (int) ((yr1 - yr0) * c);
    int vy1 = (int) ((xr0 - xr1) * c);

    Polygon p = DjaMatrixHermite.arc2polyline(xr0, yr0, xr1, yr1, vx0, vy0, vx1, vy1);

    Rectangle r = super.getExtendedBounds();
    r = r.union(p.getBounds());
    r.grow(3, 3);

    String epaisseur = getProperty("epaisseur");
    if (epaisseur != null) {
      int e = Integer.parseInt(epaisseur);
      r.grow(e, e);
    }

    return r;
  }

  @Override
  public DjaControl[] getControls() {
    double c = getCourbure();
    int vx0 = (int) ((yr0 - yr1) * c);
    int vy0 = (int) ((xr1 - xr0) * c);
    int vx1 = (int) ((yr1 - yr0) * c);
    int vy1 = (int) ((xr0 - xr1) * c);

    Polygon p = DjaMatrixHermite.arc2polyline(xr0, yr0, xr1, yr1, vx0, vy0, vx1, vy1);
    int i = p.npoints / 2;

    DjaControl[] r = new DjaControl[1];
    r[0] = new PC(this, 0, BOTH, p.xpoints[i], p.ypoints[i]);
    return r;
  }

  @Override
  public boolean contains(int _x, int _y) {
    double c = getCourbure();
    int vx0 = (int) ((yr0 - yr1) * c);
    int vy0 = (int) ((xr1 - xr0) * c);
    int vx1 = (int) ((yr1 - yr0) * c);
    int vy1 = (int) ((xr0 - xr1) * c);

    Polygon p = DjaMatrixHermite.arc2polyline(xr0, yr0, xr1, yr1, vx0, vy0, vx1, vy1);

    boolean r = false;
    for (int i = 0; i < p.npoints - 1; i++) {
      r |= DjaLib.close(p.xpoints[i], p.ypoints[i], p.xpoints[i + 1], p.ypoints[i + 1], _x, _y);
    }

    return r;
  }

  @Override
  public DjaAnchor[] getAnchors() {
    double c = getCourbure();
    int vx0 = (int) ((yr0 - yr1) * c);
    int vy0 = (int) ((xr1 - xr0) * c);
    int vx1 = (int) ((yr1 - yr0) * c);
    int vy1 = (int) ((xr0 - xr1) * c);

    Polygon p = DjaMatrixHermite.arc2polyline(xr0, yr0, xr1, yr1, vx0, vy0, vx1, vy1);
    int i = p.npoints / 2;

    DjaAnchor[] r = new DjaAnchor[1];
    r[0] = new DjaAnchor(this, 0, ANY, p.xpoints[i], p.ypoints[i]);
    return r;
  }

  @Override
  public void paintObject(Graphics _g) {
    updateXYO();

    int x0 = xbegin_;
    int y0 = ybegin_;
    int o0 = obegin_;
    int x1 = xend_;
    int y1 = yend_;
    int o1 = oend_;

    //int /*x,*/y;
    xr0 = x0;
    yr0 = y0;
    xr1 = x1;
    yr1 = y1;

    if (tbegin_ != 0) {
      switch (o0) {
        case EAST:
          drawBracket(_g, tbegin_, o0, x0, y0 - 5, x0 + 10, y0 + 5);
          xr0 += 11;
          break;
        case WEST:
          drawBracket(_g, tbegin_, o0, x0, y0 - 5, x0 - 10, y0 + 5);
          xr0 -= 11;
          break;
        case NORTH:
          drawBracket(_g, tbegin_, o0, x0 - 5, y0, x0 + 5, y0 - 10);
          yr0 -= 11;
          break;
        case SOUTH:
          drawBracket(_g, tbegin_, o0, x0 - 5, y0, x0 + 5, y0 + 10);
          yr0 += 11;
          break;
      }
    }

    if (tend_ != 0) {
      switch (o1) {
        case EAST:
          drawBracket(_g, tend_, o1, x1, y1 - 5, x1 + 10, y1 + 5);
          xr1 += 11;
          break;
        case WEST:
          drawBracket(_g, tend_, o1, x1, y1 - 5, x1 - 10, y1 + 5);
          xr1 -= 11;
          break;
        case NORTH:
          drawBracket(_g, tend_, o1, x1 - 5, y1, x1 + 5, y1 - 10);
          yr1 -= 11;
          break;
        case SOUTH:
          drawBracket(_g, tend_, o1, x1 - 5, y1, x1 + 5, y1 + 10);
          yr1 += 11;
          break;
      }
    }

    if ((tbegin_ == 1) || (tbegin_ == 4)) {
      xr0 = xbegin_;
      yr0 = ybegin_;
    }
    if ((tend_ == 1) || (tend_ == 4)) {
      xr1 = xend_;
      yr1 = yend_;
    }

    double c = getCourbure();
    _g.setColor(getForeground());
    if (Math.abs(c) > 0.001) {
      int vx0 = (int) ((yr0 - yr1) * c);
      int vy0 = (int) ((xr1 - xr0) * c);
      int vx1 = (int) ((yr1 - yr0) * c);
      int vy1 = (int) ((xr0 - xr1) * c);

      Polygon p = DjaMatrixHermite.arc2polyline(xr0, yr0, xr1, yr1, vx0, vy0, vx1, vy1);

      DjaGraphics.BresenhamParams bp = DjaGraphics.getBresenhamParams(this);
      DjaGraphics.drawPolyline(_g, p, bp);
    } else {
      _g.drawLine(xr0, yr0, xr1, yr1);

    }
    super.paintObject(_g);
  }

  @Override
  public void paintInteractive(Graphics _g) {
    super.paintInteractive(_g);

    if (isSelected()) {
      int x1 = getMiddleX();
      int y1 = getMiddleY();
      DjaControl c = getControls()[0];
      int x2 = c.getX();
      int y2 = c.getY();

      _g.setColor(controlsColor);
      DjaGraphics.BresenhamParams bp = new DjaGraphics.BresenhamParams(2, 2);
      DjaGraphics.drawLine(_g, x1, y1, x2, y2, bp);
    }
  }
}
