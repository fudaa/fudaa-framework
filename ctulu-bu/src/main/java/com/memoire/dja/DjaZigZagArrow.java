/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaZigZagArrow.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;
import java.awt.Point;

public class DjaZigZagArrow
       extends DjaLink
{
  private int xr0,yr0,xr1,yr1;
  private int[] xr;
  private int[] yr;

  public DjaZigZagArrow(String _text, int _n)
  {
    super();
    int n=_n;
    if(n<2) n=2;
    xend_=xbegin_+(n*2-1)*deltaX;

    xr=new int[n];
    yr=new int[n];

    for(int i=0;i<n;i++)
    {
      xr[i]=xbegin_+2*i*deltaX;
      yr[i]=(i%2==0 ? ybegin_ : yend_);
    }

    if(_text!=null)
    {
      DjaText t0=new DjaText(this,0,_text,false,0,0,
			    RELATIVE_CONTROL,n/2,LEFT,null,
			    null,false,true);
      setTextArray(new DjaText[] { t0 });
      textChanged(t0);
    }
  }

  public DjaZigZagArrow(String _texte)
  {
    this(_texte,4);

    /*
    xend_=xbegin_+7*deltaX;
    xr=new int[] { xbegin_, xbegin_+2*deltaX, xbegin_+4*deltaX, xend_ };
    yr=new int[] { ybegin_, yend_, ybegin_, yend_ };
    */
  }

  public DjaZigZagArrow(int _n)
  {
    this(null,_n);
  }

  public DjaZigZagArrow()
  {
    this(null,4);
  }

  @Override
  public void beforeSaving()
  {
    String s;

    s="";
    for(int i=0;i<xr.length;i++)
    {
      if(i!=0) s+=",";
      s+=xr[i];
    }
    putProperty("xr",s);

    s="";
    for(int i=0;i<yr.length;i++)
    {
      if(i!=0) s+=",";
      s+=yr[i];
    }
    putProperty("yr",s);
  }

  @Override
  public void afterLoading()
  {
    String    sx=getProperty("xr");
    String    sy=getProperty("yr");
    DjaVector pt=new DjaVector(4);

    while(!"".equals(sx)&&!"".equals(sy))
    {
      int    ix=sx.indexOf(',');
      int    iy=sy.indexOf(',');
      String vx,vy;

      if(ix>=0) { vx=sx.substring(0,ix); sx=sx.substring(ix+1); }
      else      { vx=sx; sx=""; }
      if(iy>=0) { vy=sy.substring(0,iy); sy=sy.substring(iy+1); }
      else      { vy=sy; sy=""; }

      pt.addElement(new Point(Integer.parseInt(vx),Integer.parseInt(vy)));
    }

    setPoints(pt);
  }

  private static class PC extends DjaControl
  {
    public PC(DjaObject _f,int _p,int _o,int _x,int _y)
    {
      super(_f,_p,_o,_x,_y);
    }

    @Override
    public void draggedTo(int _x, int _y)
    {
      Point p=new Point(_x,_y);
      DjaLib.snap(p);
      ((DjaZigZagArrow)getParent()).setPoint(getP()+1,p.x,p.y);
    }
  }

  @Override
  public DjaControl[] getControls()
  {
    int l=xr.length-2;
    DjaControl[] r=new DjaControl[l];
    for(int i=0;i<l;i++)
      r[i]=new PC(this,i,BOTH,xr[i+1],yr[i+1]);
    return r;
  }

  public void setPoints(DjaVector _v)
  {
    // System.out.println("SET-POINTS: "+_v.size());

    int l=_v.size();
    xr=new int[l];
    yr=new int[l];
    for(int i=0;i<l;i++)
    {
      Point p=(Point)_v.elementAt(i);
      xr[i]=p.x;
      yr[i]=p.y;
    }

    adjust();
  }

  public void setPoint(int _i, int _x, int _y)
  {
    int l=xr.length;
    if((_i>=0)&&(_i<l))
    {
      xr[_i]=_x;
      yr[_i]=_y;
      adjust();
    }
  }

  private void adjust()
  {
    int l=xr.length;

    xbegin_=xr[0];
    ybegin_=yr[0];
    xend_  =xr[l-1];
    yend_  =yr[l-1];

    /*
    int dx0=xr[1]-xr[0];
    int dy0=yr[1]-yr[0];
    int dx1=xr[l-1]-xr[l-2];
    int dy1=yr[l-1]-yr[l-2];

    if(Math.abs(dx0)>Math.abs(dy0))
    {
      if(dx0<0) obegin_=WEST;
      else      obegin_=EAST;
    }
    else
    {
      if(dy0<0) obegin_=NORTH;
      else      obegin_=SOUTH;
    }

    if(Math.abs(dx1)>Math.abs(dy1))
    {
      if(dx1<0) oend_=EAST;
      else      oend_=WEST;
    }
    else
    {
      if(dy1<0) oend_=SOUTH;
      else      oend_=NORTH;
    }
    */

    updateXYO();
  }

  private int min(int[] v)
  {
    if((v==null)||(v.length==0)) return 0;
    int r=v[0];
    for(int i=1;i<v.length;i++) if(v[i]<r) r=v[i];
    return r;
  }

  private int max(int[] v)
  {
    if((v==null)||(v.length==0)) return 0;
    int r=v[0];
    for(int i=1;i<v.length;i++) if(v[i]>r) r=v[i];
    return r;
  }

  @Override
  public int getX()
  {
    updateXYO();
    return min(xr);
  }

  @Override
  public int getY()
  {
    updateXYO();
    return min(yr);
  }

  @Override
  public int getWidth()
  {
    updateXYO();
    return max(xr)-min(xr);
  }

  @Override
  public int getHeight()
  {
    updateXYO();
    return max(yr)-min(yr);
  }

  @Override
  public void setX(int _x)
  {
    int dx=_x-getX();

    if(begin_==null)
    {
      xbegin_+=dx;
      for(int i=0;i<xr.length;i++) xr[i]+=dx/2;
    }

    if(end_==null)
    {
      xend_+=dx;
      for(int i=0;i<xr.length;i++) xr[i]+=dx/2+dx%2;
    }

    updateXYO();    
  }

  @Override
  public void setY(int _y)
  {
    int dy=_y-getY();

    if(begin_==null)
    {
      ybegin_+=dy;
      for(int i=0;i<yr.length;i++) yr[i]+=dy/2;
    }

    if(end_==null)
    {
      yend_+=dy;
      for(int i=0;i<yr.length;i++) yr[i]+=dy/2+dy%2;
    }

    updateXYO();    
  }

  @Override
  public void setWidth(int _w)
  {
    int w=getWidth();
    if(w!=0)
    {
      int x=getX();
      xbegin_=x+(xbegin_-x)*_w/w;
      xend_  =x+(xend_  -x)*_w/w;
      for(int i=0;i<xr.length;i++) xr[i]=x+(xr[i]-x)*_w/w;
      updateXYO();
    }
  }

  @Override
  public void setHeight(int _h)
  {
    int h=getHeight();
    if(h!=0)
    {
      int y=getY();
      ybegin_=y+(ybegin_-y)*_h/h;
      yend_  =y+(yend_  -y)*_h/h;
      for(int i=0;i<yr.length;i++) yr[i]=y+(yr[i]-y)*_h/h;
      updateXYO();
    }
  }

  @Override
  protected void updateXYO()
  {
    super.updateXYO();
    xr[0]=xbegin_;
    yr[0]=ybegin_;
    xr[xr.length-1]=xend_;
    yr[yr.length-1]=yend_;
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    updateXYO();
    boolean r=false;
    int l=xr.length;
    r|=DjaLib.close(xr0,yr0,xr[1]  ,yr[1]  ,_x,_y);
    r|=DjaLib.close(xr1,yr1,xr[l-2],yr[l-2],_x,_y);
    for(int i=2;i<=l-2;i++)
      r|=DjaLib.close(xr[i-1],yr[i-1],xr[i],yr[i],_x,_y);
    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    updateXYO();

    int x0=xbegin_;
    int y0=ybegin_;
    int o0=obegin_;
    int x1=xend_;
    int y1=yend_;
    int o1=oend_;

    //int x,y;

    xr0=x0;
    yr0=y0;
    xr1=x1;
    yr1=y1;

    if(tbegin_!=0)
    switch(o0)
    {
    case EAST:
      drawBracket(_g,tbegin_,o0,x0,y0-5,x0+10,y0+5);
      xr0+=11;
      break;
    case WEST:
      drawBracket(_g,tbegin_,o0,x0,y0-5,x0-10,y0+5);
      xr0-=11;
      break;
    case NORTH:
      drawBracket(_g,tbegin_,o0,x0-5,y0,x0+5,y0-10);
      yr0-=11;
      break;
    case SOUTH:
      drawBracket(_g,tbegin_,o0,x0-5,y0,x0+5,y0+10);
      yr0+=11;
      break;
    }
    
    if(tend_!=0)
    switch(o1)
    {
    case EAST:
      drawBracket(_g,tend_,o1,x1,y1-5,x1+10,y1+5);
      xr1+=11;
      break;
    case WEST:
      drawBracket(_g,tend_,o1,x1,y1-5,x1-10,y1+5);
      xr1-=11;
      break;
    case NORTH:
      drawBracket(_g,tend_,o1,x1-5,y1,x1+5,y1-10);
      yr1-=11;
      break;
    case SOUTH:
      drawBracket(_g,tend_,o1,x1-5,y1,x1+5,y1+10);
      yr1+=11;
      break;
    }

    if((tbegin_==1)||(tbegin_==4)) { xr0=xbegin_; yr0=ybegin_; }
    if((tend_  ==1)||(tend_  ==4)) { xr1=xend_;   yr1=yend_;   }

    _g.setColor(getForeground());

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    if(xr.length==2)
      DjaGraphics.drawLine(_g,xr0,yr0,xr1,yr1,bp);
    else
    {
      DjaGraphics.drawLine(_g,xr0,yr0,xr[1],yr[1],bp);
      int l=xr.length;
      for(int i=2;i<l-1;i++)
	DjaGraphics.drawLine(_g,xr[i-1],yr[i-1],xr[i],yr[i],bp);
      DjaGraphics.drawLine(_g,xr[l-2],yr[l-2],xr1,yr1,bp);
    }

    super.paintObject(_g);
  }
}
