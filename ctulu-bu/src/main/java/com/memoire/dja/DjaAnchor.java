/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaAnchor.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;

public class DjaAnchor
       extends DjaManipulator
{
  private int p_;
  private int o_;
  private int x_;
  private int y_;

  private DjaObject parent_;

  public DjaAnchor(DjaObject _parent,int _p,int _o,int _x,int _y)
  {
    p_=_p;
    o_=_o;
    x_=_x;
    y_=_y;
    parent_=_parent;
  }
  
  @Override
  public final DjaOwner getOwner()
  { return getParent(); }

  public int getX() { return x_; }
  public int getY() { return y_; }
  public int getO() { return o_; }

  public int getPosition() { return p_; }

  public DjaObject getParent() { return parent_; }

  public void paint(Graphics _g)
  {
    int x=getX();
    int y=getY();

    _g.setColor(anchorsColor);
    _g.drawLine(x-2,y-2,x+2,y+2);
    _g.drawLine(x+2,y-2,x-2,y+2);
  }
}

