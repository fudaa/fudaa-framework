/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaLink.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Vector;

public abstract class DjaLink extends DjaObject {
  public static final int NB_TYPE = 24;

  protected DjaObject begin_;
  protected DjaObject end_;
  protected int nbegin_, nend_; // attache
  protected int tbegin_, tend_; // type
  protected int xbegin_, xend_;
  protected int ybegin_, yend_;
  protected int obegin_, oend_;

  public DjaLink() {
    super();

    setBegin(null, 0, 0);
    setEnd(null, 0, 5);

    xbegin_ = deltaX;
    ybegin_ = deltaY;
    xend_ = xbegin_ + 5 * deltaX;
    yend_ = ybegin_ + 3 * deltaY;
    obegin_ = EAST;
    oend_ = WEST;
  }

  public void setPoints(Vector _v) {
    // System.out.println("SET-POINTS: "+_v.size());

    if (_v.size() < 2) return;

    Point p0 = (Point) _v.elementAt(0);
    Point p2 = (Point) _v.elementAt(1);
    Point p3 = (Point) _v.elementAt(_v.size() - 2);
    Point p1 = (Point) _v.elementAt(_v.size() - 1);

    xbegin_ = p0.x;
    ybegin_ = p0.y;
    xend_ = p1.x;
    yend_ = p1.y;

    int dx0 = p2.x - p0.x;
    int dy0 = p2.y - p0.y;
    int dx1 = p3.x - p1.x;
    int dy1 = p3.y - p1.y;

    if (Math.abs(dx0) > Math.abs(dy0)) {
      if (dx0 < 0) obegin_ = WEST;
      else
        obegin_ = EAST;
    } else {
      if (dy0 < 0) obegin_ = NORTH;
      else
        obegin_ = SOUTH;
    }

    if (Math.abs(dx1) > Math.abs(dy1)) {
      if (dx1 < 0) oend_ = WEST;
      else
        oend_ = EAST;
    } else {
      if (dy1 < 0) oend_ = NORTH;
      else
        oend_ = SOUTH;
    }

    updateXYO();
  }

  @Override
  public Rectangle getBounds() {
    return new Rectangle(getX(), getY(), getWidth(), getHeight());
  }

  protected void updateXYO() {
    DjaAnchor[] a;

    if (begin_ != null) {
      a = begin_.getAnchors();
      if (nbegin_ < a.length) {
        xbegin_ = a[nbegin_].getX();
        ybegin_ = a[nbegin_].getY();
        int o = a[nbegin_].getO();
        if (o != ANY) obegin_ = o;
      }
    }

    if (end_ != null) {
      a = end_.getAnchors();
      if (nend_ < a.length) {
        xend_ = a[nend_].getX();
        yend_ = a[nend_].getY();
        int o = a[nend_].getO();
        if (o != ANY) oend_ = o;
      }
    }
  }

  @Override
  public void addText(String _text) {
    DjaText t = new DjaText(this, -1, _text);
    int p = RELATIVE_ATTACH;
    int r = 0;
    int l = getTexts().length;
    int a = getAnchors().length;

    if (l < a) {
      p = RELATIVE_ANCHOR;
      r = l;
    } else {
      p = RELATIVE_ATTACH;
      r = l % 2;
    }
    t.setPosition(p);
    t.setReference(r);
    addText(t);
  }

  @Override
  public int getX() {
    updateXYO();
    return Math.min(xbegin_, xend_);
  }

  @Override
  public int getY() {
    updateXYO();
    return Math.min(ybegin_, yend_);
  }

  @Override
  public int getWidth() {
    updateXYO();
    return Math.max(xbegin_, xend_) - Math.min(xbegin_, xend_);
  }

  @Override
  public int getHeight() {
    updateXYO();
    return Math.max(ybegin_, yend_) - Math.min(ybegin_, yend_);
  }

  @Override
  public void setX(int _x) {
    int dx = _x - getX();
    if (begin_ == null) xbegin_ += dx;
    if (end_ == null) xend_ += dx;
    updateXYO();
  }

  @Override
  public void setY(int _y) {
    int dy = _y - getY();
    if (begin_ == null) ybegin_ += dy;
    if (end_ == null) yend_ += dy;
    updateXYO();
  }

  @Override
  public void setWidth(int _w) {
    int w = getWidth();
    if (w != 0) {
      int x = getX();
      xbegin_ = (int) (x + ((double) (xbegin_ - x)) * _w / w);
      xend_ = (int) (x + ((double) (xend_ - x)) * _w / w);
      updateXYO();
    }
  }

  @Override
  public void setHeight(int _h) {
    int h = getHeight();
    if (h != 0) {
      int y = getY();
      ybegin_ = (int) (y + ((double) (ybegin_ - y)) * _h / h);
      yend_ = (int) (y + ((double) (yend_ - y)) * _h / h);
      updateXYO();
    }
  }

  protected Rectangle rectangle(int _x0, int _y0, int _x1, int _y1) {
    return new Rectangle(Math.min(_x0, _x1) - 2, Math.min(_y0, _y1) - 2, Math.max(_x0, _x1) - Math.min(_x0, _x1) + 5,
        Math.max(_y0, _y1) - Math.min(_y0, _y1) + 5);
  }

  public void setBegin(DjaObject _begin, int _nbegin, int _tbegin) {
    setBeginObject(_begin);
    setBeginPosition(_nbegin);
    setBeginType(_tbegin);
  }

  public void setEnd(DjaObject _end, int _nend, int _tend) {
    setEndObject(_end);
    setEndPosition(_nend);
    setEndType(_tend);
  }

  public DjaObject getBeginObject() {
    return begin_;
  }

  public void setBeginObject(DjaObject _begin) {
    if (begin_ != _begin) {
      if (begin_ != null) fireGridEvent(this, DjaGridEvent.DISCONNECTED);
      begin_ = _begin;
      if ((begin_ != null) && (nbegin_ >= begin_.getAnchors().length)) nbegin_ = 0;
      if (begin_ != null) fireGridEvent(this, DjaGridEvent.CONNECTED);
    }
  }

  public DjaObject getEndObject() {
    return end_;
  }

  public void setEndObject(DjaObject _end) {
    if (end_ != _end) {
      if (end_ != null) fireGridEvent(this, DjaGridEvent.DISCONNECTED);
      end_ = _end;
      if ((end_ != null) && (nend_ >= end_.getAnchors().length)) nend_ = 0;
      if (end_ != null) fireGridEvent(this, DjaGridEvent.CONNECTED);
    }
  }

  public int getBeginPosition() {
    return nbegin_;
  }

  public void setBeginPosition(int _nbegin) {
    if (begin_ != null) {
      if ((_nbegin < 0) || (_nbegin >= begin_.getAnchors().length)) begin_ = null;
    }
    nbegin_ = _nbegin;
  }

  public int getEndPosition() {
    return nend_;
  }

  public void setEndPosition(int _nend) {
    if (end_ != null) {
      if ((_nend < 0) || (_nend >= end_.getAnchors().length)) end_ = null;
    }
    nend_ = _nend;
  }

  public int getBeginType() {
    return tbegin_;
  }

  public void setBeginType(int _tbegin) {
    tbegin_ = (NB_TYPE + _tbegin) % NB_TYPE;
  }

  public int getEndType() {
    return tend_;
  }

  public void setEndType(int _tend) {
    tend_ = (NB_TYPE + _tend) % NB_TYPE;
  }

  public int getBeginX() {
    return xbegin_;
  }

  public int getBeginY() {
    return ybegin_;
  }

  public int getBeginO() {
    return obegin_;
  }

  public int getEndX() {
    return xend_;
  }

  public int getEndY() {
    return yend_;
  }

  public int getEndO() {
    return oend_;
  }

  public void setBeginX(int _xbegin) {
    xbegin_ = _xbegin;
  }

  public void setBeginY(int _ybegin) {
    ybegin_ = _ybegin;
  }

  public void setBeginO(int _obegin) {
    obegin_ = _obegin;
  }

  public void setEndX(int _xend) {
    xend_ = _xend;
  }

  public void setEndY(int _yend) {
    yend_ = _yend;
  }

  public void setEndO(int _oend) {
    oend_ = _oend;
  }

  @Override
  public DjaAttach[] getAttachs() {
    updateXYO();

    DjaAttach[] r = new DjaAttach[2];
    r[0] = new DjaAttach(this, 0, begin_, obegin_, xbegin_, ybegin_);
    r[1] = new DjaAttach(this, 1, end_, oend_, xend_, yend_);
    return r;
  }

  @Override
  public DjaHandle[] getHandles() {
    return new DjaHandle[0];
  }

  @Override
  public void paintText(Graphics _g) {}

  /*
   * public void paintInteractive(Graphics _g) { super.paintInteractive(_g); if(isSelected()) { if(begin_!=null)
   * _g.setColor(linksColor); else _g.setColor(freesColor); _g.fillRect(xbegin_-3,ybegin_-3,7,7); if(end_!=null)
   * _g.setColor(linksColor); else _g.setColor(freesColor); _g.fillRect(xend_-3,yend_-3,7,7); } else { if(begin_!=null)
   * _g.setColor(linksColor); else _g.setColor(freesColor); _g.fillRect(xbegin_-1,ybegin_-1,3,3); if(end_!=null)
   * _g.setColor(linksColor); else _g.setColor(freesColor); _g.fillRect(xend_-1,yend_-1,3,3); } }
   */

  /*
   * public void paintAttachs(Graphics _g) { }
   */

  protected void drawBracket(Graphics _g, int t, int o, int x0, int y0, int x1, int y1) {
    int e = 1;
    String epaisseur = getProperty("epaisseur");
    if (epaisseur != null) e = Integer.parseInt(epaisseur);

    drawBracket(_g, t, o, x0, y0, x1, y1, getForeground(), getBackground(), e);
  }

  public static void drawBracket(Graphics _g, int t, int o, int x0, int y0, int x1, int y1, Color fg, Color bg, int e) {
    Polygon p = null;
    Rectangle r = null;

    if ((t >= 1) && (t <= 3)) {
      p = new Polygon();
      switch (o) {
      case WEST:
      case EAST:
        p.addPoint(x0, y0);
        p.addPoint(x1, (y0 + y1) / 2);
        p.addPoint(x0, y1);
        break;
      default:
        p.addPoint(x0, y0);
        p.addPoint((x0 + x1) / 2, y1);
        p.addPoint(x1, y0);
        break;
      }
    }

    if ((t >= 4) && (t <= 6)) {
      p = new Polygon();
      switch (o) {
      case WEST:
      case EAST:
        p.addPoint(x1, y1);
        p.addPoint(x0, (y0 + y1) / 2);
        p.addPoint(x1, y0);
        break;
      default:
        p.addPoint(x1, y1);
        p.addPoint((x0 + x1) / 2, y0);
        p.addPoint(x0, y1);
        break;
      }
    }

    if ((t >= 7) && (t <= 9)) {
      p = new Polygon();
      p.addPoint(x0, (y0 + y1) / 2);
      p.addPoint((x0 + x1) / 2, y0);
      p.addPoint(x1, (y0 + y1) / 2);
      p.addPoint((x0 + x1) / 2, y1);
    }

    if ((t >= 10) && (t <= 12)) {
      p = new Polygon();
      p.addPoint(x0, y0);
      p.addPoint(x1, y0);
      p.addPoint(x1, y1);
      p.addPoint(x0, y1);
    }

    if ((t >= 13) && (t <= 15)) {
      r = new Rectangle();
      r.x = Math.min(x0, x1);
      r.y = Math.min(y0, y1);
      r.width = Math.abs(x0 - x1) + 1;
      r.height = Math.abs(y0 - y1) + 1;
    }

    if ((t >= 16) && (t <= 18)) {
      p = new Polygon();
      switch (o) {
      case WEST:
      case EAST:
        p.addPoint(x1, y0);
        p.addPoint(x0, (y0 + y1) / 2);
        p.addPoint(x1, (y0 + y1) / 2);
        break;
      default:
        p.addPoint(x1, y1);
        p.addPoint((x0 + x1) / 2, y0);
        p.addPoint((x0 + x1) / 2, y1);
        break;
      }
    }

    if ((t >= 19) && (t <= 21)) {
      p = new Polygon();
      switch (o) {
      case WEST:
      case EAST:
        p.addPoint(x1, y1);
        p.addPoint(x0, (y0 + y1) / 2);
        p.addPoint(x1, (y0 + y1) / 2);
        break;
      default:
        p.addPoint(x0, y1);
        p.addPoint((x0 + x1) / 2, y0);
        p.addPoint((x0 + x1) / 2, y1);
        break;
      }
    }

    // Color fg=getForeground();
    // Color bg=getBackground();

    DjaGraphics.BresenhamParams bp = new DjaGraphics.BresenhamParams(1, 0);
    bp.epaisseur_ = e;

    switch (t) {
    // rien
    case 0:
      break;
    // triangle
    case 1:
    case 4:
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawPolyline(_g, p, bp);
      }
      break;
    case 2:
    case 5:
      if (fg != null) {
        _g.setColor(fg);
        if (p != null) _g.fillPolygon(p.xpoints, p.ypoints, 3);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    case 3:
    case 6:
      if (bg != null) {
        _g.setColor(bg);
        if (p != null) _g.fillPolygon(p.xpoints, p.ypoints, 3);
      }
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    // losange et carre
    case 7:
    case 10:
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    case 8:
    case 11:
      if (fg != null) {
        _g.setColor(fg);
        _g.fillPolygon(p);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    case 9:
    case 12:
      if (bg != null) {
        _g.setColor(bg);
        _g.fillPolygon(p);
      }
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    // cercle
    case 13:
      if (fg != null) {
        _g.setColor(fg);
        if (r != null) DjaGraphics.drawOval(_g, r.x, r.y, r.width - 1, r.height - 1, bp);
      }
      break;
    case 14:
      if (fg != null) {
        _g.setColor(fg);
        if (r != null) {
          _g.fillOval(r.x, r.y, r.width - 1, r.height - 1);
          DjaGraphics.drawOval(_g, r.x, r.y, r.width - 1, r.height - 1, bp);
        }
      }
      break;
    case 15:
      if (bg != null) {
        _g.setColor(bg);
        if (r != null) _g.fillOval(r.x, r.y, r.width - 1, r.height - 1);
      }
      if (fg != null) {
        _g.setColor(fg);
        if (r != null) DjaGraphics.drawOval(_g, r.x, r.y, r.width - 1, r.height - 1, bp);
      }
      break;
    // message
    case 16:
    case 19:
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawPolyline(_g, p, bp);
      }
      break;
    case 17:
    case 20:
      if (fg != null) {
        _g.setColor(fg);
        _g.fillPolygon(p);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    case 18:
    case 21:
      if (bg != null) {
        _g.setColor(bg);
        _g.fillPolygon(p);
      }
      if (bg != null) {
        _g.setColor(fg);
        DjaGraphics.drawPolygon(_g, p, bp);
      }
      break;
    case 22:
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawLine(_g, x1, y0, x0, y1, bp);
        DjaGraphics.drawLine(_g, x1, (y0 + y1) / 2, x0, (y0 + y1) / 2, bp);
        DjaGraphics.drawLine(_g, (x0 + x1) / 2, y0, (x0 + x1) / 2, y1, bp);
      }
      break;
    case 23:
      if (fg != null) {
        _g.setColor(fg);
        DjaGraphics.drawLine(_g, x1, y0, x0, y1, bp);
        DjaGraphics.drawLine(_g, x0, y0, x1, y1, bp);
        DjaGraphics.drawLine(_g, x1, (y0 + y1) / 2, x0, (y0 + y1) / 2, bp);
        DjaGraphics.drawLine(_g, (x0 + x1) / 2, y0, (x0 + x1) / 2, y1, bp);
      }
      break;
    }
  }
}
