/**
 * @modification $Date: 2007-03-19 13:25:37 $
 * @statut unstable
 * @file DjaObject.java
 * @version 0.36
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2001 Guillaume Desnoix
 */
package com.memoire.dja;

import com.memoire.bu.BuLib;
import com.memoire.fu.FuHashtablePublic;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.Serializable;
import java.util.Enumeration;
import javax.swing.Icon;

public abstract class DjaObject
        implements DjaOwner, DjaOptions, Cloneable, Serializable {

  private boolean selected_;
  protected DjaColor foreground_;
  protected DjaColor background_;
  private DjaColor color_;
  private DjaFont font_;
  private DjaText[] texts_;

  private FuHashtablePublic properties_;

  private transient DjaOwner owner_;
  private transient FuHashtablePublic datas_;

  public DjaObject() {
    properties_ = new FuHashtablePublic(11);
    texts_ = new DjaText[0];

    setSelected(false);
    setForeground(Color.black);
    setBackground(Color.white);
    setFont(defaultPlainFont);
    setColor(Color.black);
  }

  @Override
  public DjaOwner getOwner() {
    return owner_;
  }

  void setOwner(DjaOwner _owner) {
    owner_ = _owner;
  }

  @Override
  public DjaGrid getGrid() {
    DjaGrid r = null;

    DjaOwner o = getOwner();
    while (o != null) {
      if (o instanceof DjaGrid) {
        r = (DjaGrid) o;
        break;
      }
      o = o.getOwner();
    }

    return r;
  }

  public void fireGridEvent(DjaOwner _object, int _id) {
    DjaGrid grid = getGrid();
    if (grid != null) {
      grid.fireGridEvent(_object, _id);
    }
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    DjaObject r = (DjaObject) super.clone();

    properties_ = (FuHashtablePublic) properties_.clone();

    if (datas_ != null) {
      datas_ = (FuHashtablePublic) datas_.clone();
    }

    DjaText[] t = r.getTextArray();
    DjaText[] u = new DjaText[t.length];
    for (int i = 0; i < t.length; i++) {
      u[i] = (DjaText) t[i].clone();
      u[i].setParent(r);
    }
    r.setTextArray(u);

    return r;
  }

  public Icon getIcon() {
    String s = getClass().getName().toLowerCase();
    int i = s.indexOf("dja");
    if (i >= 0) {
      s = "dja-" + s.substring(i + 3);
    }
    return DjaResource.DJA.getIcon(s, 16);
  }

  public String getProperty(String _key) {
    return (String) properties_.get(_key);
  }

  public void putProperty(String _key, String _value) {
    if (_value != null) {
      properties_.put(_key, _value);
    } else {
      removeProperty(_key);
    }
  }

  public void removeProperty(String _key) {
    properties_.remove(_key);
  }

  public Object getData(String _key) {
    return datas_ == null ? null : datas_.get(_key);
  }

  public void putData(String _key, Object _value) {
    if (datas_ == null) {
      datas_ = new FuHashtablePublic(11);
    }
    datas_.put(_key, _value);
  }

  public void removeData(String _key) {
    if (datas_ != null) {
      datas_.remove(_key);
    }
  }

  // Saver
  Enumeration getPropertyKeys() {
    return properties_.keys();
  }

  public void beforeSaving() {
  }

  public void afterLoading() {
  }

  public abstract Rectangle getBounds();

  public abstract void setX(int _x);

  public abstract void setY(int _y);

  public abstract void setWidth(int _w);

  public abstract void setHeight(int _h);

  public boolean contains(int _x, int _y) {
    return getBounds().contains(_x, _y);
  }

  public int getX() {
    return getBounds().x;
  }

  public int getY() {
    return getBounds().y;
  }

  public int getWidth() {
    return getBounds().width;
  }

  public Rectangle getExtendedBounds() {
    return getExtendedBounds(getBounds());
  }

  protected Rectangle getExtendedBounds(Rectangle _r) {
    Rectangle r = _r;
    if (isSelected()) {
      r.grow(deltaX, deltaY);
    }
    if (this instanceof DjaLink) {
      r.grow(11, 11);
    }

    /*
     {
     DjaAnchor[] anchors=getAnchors();
     for(int i=0;i<anchors.length;i++)
     anchors[i].paint(_g);
     }

     {
     DjaAttach[] attachs=getAttachs();
     for(int i=0;i<attachs.length;i++)
     attachs[i].paint(_g);
     }

     {
     DjaHandle[] handles=getHandles();
     for(int i=0;i<handles.length;i++)
     handles[i].paint(_g);
     }

     {
     DjaControl[] controls=getControls();
     for(int i=0;i<controls.length;i++)
     controls[i].paint(_g);
     }
     */
    {
      DjaText[] texts = getTexts();
      for (int i = 0; i < texts.length; i++) {
        r = r.union(texts[i].getBounds());
      }
    }

    r.grow(3, 3);

    String epaisseur = getProperty("epaisseur");
    if (epaisseur != null) {
      int e = Integer.parseInt(epaisseur);
      r.grow(e, e);
    }

    return r;
  }

  public int getHeight() {
    return getBounds().height;
  }

  public boolean isSelected() {
    return selected_;
  }

  public void setSelected(boolean _selected) {
    if (selected_ != _selected) {
      selected_ = _selected;
      fireGridEvent(this, selected_
              ? DjaGridEvent.SELECTED
              : DjaGridEvent.UNSELECTED);
    }
  }

  public Color getForeground() {
    Color r = null;

    if (foreground_ != null) {
      if (selected_) {
        r = BuLib.mixColors(foreground_.getColor(), selectionForeground);
      } else {
        r = foreground_.getColor();
      }
    }

    return r;
  }

  public void setForeground(Color _foreground) {
    if (_foreground != null) {
      foreground_ = new DjaColor(_foreground);
    } else {
      foreground_ = null;
    }
  }

  public Color getBackground() {
    Color r = null;

    if (background_ != null) {
      if (selected_) {
        r = BuLib.mixColors(background_.getColor(), selectionBackground);
      } else {
        r = background_.getColor();
      }
    }

    return r;
  }

  public void setBackground(Color _background) {
    if (_background != null) {
      background_ = new DjaColor(_background);
    } else {
      background_ = null;
    }
  }

  public DjaAnchor[] getAnchors() {
    return new DjaAnchor[0];
  }

  public DjaAttach[] getAttachs() {
    return new DjaAttach[0];
  }

  public DjaHandle[] getHandles() {
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();

    DjaHandle[] r = new DjaHandle[8];
    r[0] = new DjaHandle(this, NORTH, x + w / 2, y - deltaY);
    r[2] = new DjaHandle(this, EAST, x + w - 1 + deltaX, y + h / 2);
    r[4] = new DjaHandle(this, SOUTH, x + w / 2, y + h - 1 + deltaY);
    r[6] = new DjaHandle(this, WEST, x - deltaX, y + h / 2);
    r[1] = new DjaHandle(this, NORTH_EAST, x + w - 1 + deltaX, y - deltaY);
    r[3] = new DjaHandle(this, SOUTH_EAST, x + w - 1 + deltaX, y + h - 1 + deltaY);
    r[5] = new DjaHandle(this, SOUTH_WEST, x - deltaX, y + h - 1 + deltaY);
    r[7] = new DjaHandle(this, NORTH_WEST, x - deltaX, y - deltaY);

    return r;
  }

  public DjaControl[] getControls() {
    return new DjaControl[0];
  }

  public DjaLink[] getBeginConnections() {
    DjaGrid grid = getGrid();
    if (grid == null) {
      return new DjaLink[0];
    }

    DjaVector v = new DjaVector();
    for (Enumeration e = grid.enumerate(DjaLink.class);
            e.hasMoreElements();) {
      DjaLink l = (DjaLink) e.nextElement();
      if (l.getBeginObject() == this) {
        v.addElement(l);
      }
    }

    int t = v.size();
    DjaLink[] r = new DjaLink[t];
    for (int i = 0; i < t; i++) {
      r[i] = (DjaLink) v.elementAt(i);
    }
    return r;
  }

  public DjaLink[] getEndConnections() {
    DjaGrid grid = getGrid();
    if (grid == null) {
      return new DjaLink[0];
    }

    DjaVector v = new DjaVector();
    for (Enumeration e = grid.enumerate(DjaLink.class);
            e.hasMoreElements();) {
      DjaLink l = (DjaLink) e.nextElement();
      if (l.getEndObject() == this) {
        v.addElement(l);
      }
    }

    int t = v.size();
    DjaLink[] r = new DjaLink[t];
    for (int i = 0; i < t; i++) {
      r[i] = (DjaLink) v.elementAt(i);
    }
    return r;
  }

  public DjaText[] getTexts() {
    DjaText[] r = texts_;
    if (r == null) {
      r = new DjaText[0];
    }
    return r;
  }

  protected final DjaText[] getTextArray() {
    return texts_;
  }

  protected final void setTextArray(DjaText[] _texts) {
    texts_ = _texts;
  }

  public final synchronized void addText(DjaText _text) {
    if (texts_ == null) {
      _text.setNum(0);
      texts_ = new DjaText[]{_text};
    } else {
      int l = texts_.length;
      int n = _text.getNum();

      if ((n >= 0) && (n < l)) {
        texts_[n] = _text;
      } else {
        DjaText[] a = new DjaText[l + 1];
        for (int i = 0; i < l; i++) {
          a[i] = texts_[i];
        }
        _text.setNum(l);
        a[l] = _text;
        texts_ = a;
      }
    }
  }

  public DjaText getTextAt(int _i) {
    if (texts_ != null && texts_.length > _i) {
      return texts_[_i];
    }
    return null;
  }

  public void addText(String _text) {
    DjaText t = new DjaText(this, -1, _text);
    t.setPosition(ABSOLUTE);
    addText(t);
  }

  /**
   * Notify when a text was modified
   */
  public void textChanged(DjaText _text) {
  }

  // Simplify text
  /**
   * @deprecated use getText(0)
   */
  public /*final*/ String getText() {
    return getText(0);
  }

  public String getText(int _i) {
    String r = null;
    if (texts_.length > _i) {
      texts_[_i].getText();
    }
    return r;
  }

  /**
   * @deprecated use setText(0)
   */
  public /*final*/ void setText(String _text) {
    setText(_text, 0);
  }

  public void setText(String _text, int _i) {
    String txt = _text;
    if ("".equals(txt)) {
      txt = null;
    }
    if (texts_.length > _i) {
      texts_[_i].setText(txt);
    }
  }

  public Font getFont() {
    Font r = null;
    if (font_ != null) {
      r = font_.getFont();
    }
    if (r == null) {
      r = defaultPlainFont;
    }
    return r;
  }

  public void setFont(Font _font) {
    font_ = new DjaFont(_font);
  }

  public Color getColor() {
    Color r = null;
    if (color_ != null) {
      r = color_.getColor();
    }
    return r;
  }

  public void setColor(Color _color) {
    if (_color != null) {
      color_ = new DjaColor(_color);
    } else {
      color_ = null;
    }
  }

  /**
   * @deprecated
   */
  public Color getTextColor() {
    Color r = null;
    /*
     if(selected_) r=selectionForeground;
     else          r=textcolor_;
     */
    if (texts_.length > 0) {
      r = texts_[0].getColor();
    }
    return r;
  }

  /**
   * @deprecated
   */
  public void setTextColor(Color _color) {
    if (texts_.length > 0) {
      texts_[0].setColor(_color);
    }
  }

  /**
   * @deprecated
   */
  public int getTextPosition() {
    int r = CENTER;
    if (texts_.length > 0) {
      r = texts_[0].getPosition();
    }
    return r;
  }

  /**
   * @deprecated
   */
  public void setTextPosition(int _pos) {
    if (texts_.length > 0) {
      texts_[0].setPosition(_pos);
    }
  }

  /**
   * @deprecated
   */
  public boolean isTextMultiline() {
    boolean r = false;
    if (texts_.length > 0) {
      texts_[0].isMultiline();
    }
    return r;
  }

  /**
   * @deprecated
   */
  protected void setTextBounds(Rectangle _r) {
  }
  /*
   private Rectangle textBounds_=null;

   public boolean textContains(int _x, int _y)
   {
   Rectangle tb=getTextBounds();
   return (tb==null ? false : tb.contains(_x,_y));
   }

   protected Rectangle getTextBounds() { return textBounds_; }
   protected void setTextBounds(Rectangle _r) { textBounds_=_r; }
   */

  public static void setBestQuality(final Graphics2D _g) {
    _g.getRenderingHints().put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    _g.getRenderingHints().put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    _g.getRenderingHints().put(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    _g.getRenderingHints()
            .put(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
    _g.getRenderingHints().put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    _g.getRenderingHints().put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    _g.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
  }

  public void paint(Graphics _g) {
    setBestQuality((Graphics2D) _g);
    paintObject(_g);
    paintText(_g);
    paintTexts(_g);
  }

  public void paintObject(Graphics _g) {
  }

  /**
   * @deprecated
   */
  public void paintText(Graphics _g) {
  }

  public void paintInteractive(Graphics _g) {
    if (isSelected()) {
      DjaText[] texts = getTexts();
      for (int k = 0; k < texts.length; k++) {
        boolean moveable = texts[k].isMoveable();
        boolean selected = texts[k].isSelected();

        Rectangle r = texts[k].getBounds();

        int x = r.x;
        int y = r.y;
        int w = r.width;
        int h = r.height;

        _g.setColor(selected
                ? selectionZone
                : texts[k].getColor());

        for (int i = 1; i <= w; i += 4) {
          if (moveable) {
            _g.drawLine(x + i, y - 1, x + i + 1, y - 1);
          }
          _g.drawLine(x + i, y + h + 1, x + i + 1, y + h + 1);
        }

        if (moveable) {
          for (int j = 1; j <= h; j += 4) {
            _g.drawLine(x - 1, y + j, x - 1, y + j + 1);
            _g.drawLine(x + w + 1, y + j, x + w + 1, y + j + 1);
          }
        }

        if (selected) {
          _g.drawLine(x, y, x + 5, y);
          _g.drawLine(x + 5, y, x, y + 5);
          _g.drawLine(x, y + 5, x, y);
        }
      }
    }
  }

  public void paintAnchors(Graphics _g) {
    DjaAnchor[] anchors = getAnchors();
    for (int i = 0; i < anchors.length; i++) {
      anchors[i].paint(_g);
    }
  }

  public void paintAttachs(Graphics _g) {
    DjaAttach[] attachs = getAttachs();
    for (int i = 0; i < attachs.length; i++) {
      attachs[i].paint(_g);
    }
  }

  public void paintHandles(Graphics _g) {
    DjaHandle[] handles = getHandles();
    for (int i = 0; i < handles.length; i++) {
      handles[i].paint(_g);
    }
  }

  public void paintControls(Graphics _g) {
    DjaControl[] controls = getControls();
    for (int i = 0; i < controls.length; i++) {
      controls[i].paint(_g);
    }
  }

  public void paintTexts(Graphics _g) {
    DjaText[] texts = getTexts();
    for (int i = 0; i < texts.length; i++) {
      texts[i].paint(_g);
    }
  }

  public String toString() {
    String r = getClass().getName();
    int i = r.lastIndexOf('.');
    if (i >= 0) {
      r = r.substring(i + 1);
    }
    if (r.startsWith("Dja")) {
      r = r.substring(3);
    }
    return r;
  }
}
