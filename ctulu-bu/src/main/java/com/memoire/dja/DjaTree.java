/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaTree.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class DjaTree
       extends JTree
       // implements MouseListener // , PropertyChangeListener
{
  public DjaTree()
  {
    setShowsRootHandles(true);
    setBorder(null); // new EmptyBorder(5,5,5,5));
    setCellRenderer(new DjaTreeCellRenderer());
    setRootVisible(false);
    setGrid(null);
  }

  public void setGrid(DjaGrid _grid)
  {
    setModel(new DjaTreeModel(_grid));
  }

  // Mouse

  /*
  public void mouseDown(MouseEvent _evt)
  { }

  public void mouseEntered(MouseEvent _evt)
  { }

  public void mouseExited(MouseEvent _evt)
  { }

  public void mousePressed(MouseEvent _evt)
  {
    if(  _evt.isPopupTrigger()
       ||((_evt.getModifiers()&MouseEvent.BUTTON3_MASK)!=0))
      setModel(new DjaTreeModel());
  }

  public void mouseReleased(MouseEvent _evt)
  { }

  public void mouseUp(MouseEvent _evt)
  { }

  public void mouseClicked(MouseEvent _evt)
  { }
  */
}

class DjaNodeGrid extends DjaNode
{
  DjaGrid value_;

  DjaNodeGrid(DjaGrid _value)
  {
    value_=_value;
  }

  @Override
  int getChildCount()
  {
    return (value_==null ? 0 : value_.getObjects().size());
  }

  @Override
  boolean isLeaf()
    { return (getChildCount()==0); }

  @Override
  Object getChild(int _index)
  {
    Object r=null;

    if(value_!=null)
    {
      DjaObject o=(DjaObject)value_.getObjects().elementAt(_index);
      if(o instanceof DjaGroup) r=new DjaNodeGroup((DjaGroup)o);
      else                      r=new DjaNodeObject(o);
    }

    return r;
  }

  @Override
  int getIndexOfChild(Object _child)
    { return (value_==null ? -1 : value_.getObjects().indexOf(_child)); }

  public String toString()
    { return "Grid"; }

  @Override
  public Object getValue()
    { return value_; }

  @Override
  public Icon getIcon()
    { return null; } 
}

class DjaNodeObject extends DjaNode
{
  DjaObject value_;

  DjaNodeObject(DjaObject _value)
  {
    value_=_value;
  }

  @Override
  int getChildCount()
    { return 0; }

  @Override
  boolean isLeaf()
    { return true; }

  @Override
  Object  getChild(int _index)
    { return null; }

  @Override
  int getIndexOfChild(Object _child)
    { return -1; }

  public String toString()
  {
    String r=value_.getClass().getName();
    int    i=r.indexOf("Dja");
    if(i>=0) r=r.substring(i+3);

    String t=value_.getText(0);
    if(t!=null)
    {
      i=t.indexOf('\n');
      if(i>=0) t=t.substring(0,i);
      if(t.length()>13) t=t.substring(0,10)+"...";
      if(!t.equals("")) r=r+" ("+t+")";
    }

    return r;
  }

  @Override
  public Object getValue()
    { return value_; }

  @Override
  public Icon getIcon()
    { return value_.getIcon(); } 
}

class DjaNodeGroup extends DjaNode
{
  DjaGroup value_;

  DjaNodeGroup(DjaGroup _value)
  {
    value_=_value;
  }

  @Override
  int getChildCount()
    { return value_.getObjects().size(); }

  @Override
  boolean isLeaf()
    { return (getChildCount()==0); }

  @Override
  Object getChild(int _index)
  {
    DjaObject o=(DjaObject)value_.getObjects().elementAt(_index);
    Object    r;

    if(o instanceof DjaGroup) r=new DjaNodeGroup((DjaGroup)o);
    else                      r=new DjaNodeObject(o);

    return r;
  }

  @Override
  int getIndexOfChild(Object _child)
    { return value_.getObjects().indexOf(_child); }

  public String toString()
    { return "Group ["+getChildCount()+"]"; }

  @Override
  public Object getValue()
    { return value_; }

  @Override
  public Icon getIcon()
    { return value_.getIcon(); } 
}

class DjaTreeModel
      implements TreeModel
{
  DjaNodeGrid root_;
  DjaVector   listeners_;

  public DjaTreeModel(DjaGrid _grid)
  {
    root_     =new DjaNodeGrid(_grid);
    listeners_=new DjaVector();
  }

  @Override
  public int getChildCount(Object _parent)
  {
    int r=0;
    if(_parent instanceof DjaNode)
      r=((DjaNode)_parent).getChildCount();
    return r;
  }

  @Override
  public Object getChild(Object _parent, int _index)
  {
    Object r=null;
    if(_parent instanceof DjaNode)
      r=((DjaNode)_parent).getChild(_index);
    return r;
  }

  @Override
  public int getIndexOfChild(Object _parent, Object _child)
  {
    int r=-1;
    if(_parent instanceof DjaNode)
      r=((DjaNode)_parent).getIndexOfChild(_child);
    return r;
  }

  @Override
  public boolean isLeaf(Object _node)
  {
    boolean r=true;
    if(_node instanceof DjaNode)
      r=((DjaNode)_node).isLeaf();
    return r;
  }

  @Override
  public Object getRoot()
  { return root_; }

  @Override
  public void addTreeModelListener(TreeModelListener _l)
  { listeners_.addElement(_l); }

  @Override
  public void removeTreeModelListener(TreeModelListener _l)
  { listeners_.removeElement(_l); }

  @Override
  public void valueForPathChanged(TreePath _path, Object _newValue)
  {
  }
}

class DjaTreeCellRenderer
      implements TreeCellRenderer
{
  @Override
  public Component getTreeCellRendererComponent
    (JTree   tree,
     Object  value,
     boolean selected,
     boolean expanded,
     boolean leaf,
     int     row,
     boolean hasFocus)
  {
    JLabel r=new JLabel();

    if(value instanceof DjaNode)
    {
      DjaNode node=(DjaNode)value;
      r.setText(node.getTitle());
      r.setIcon(node.getIcon());
    }
    else
    {
      if(value!=null) r.setText(value.toString());
    }

    r.setOpaque(true);
    r.setFont(tree.getFont());

    if(r.getIcon()==null)
    {
           if(leaf)     r.setIcon(UIManager.getIcon("Tree.leafIcon"));
      else if(expanded) r.setIcon(UIManager.getIcon("Tree.openIcon"));
      else              r.setIcon(UIManager.getIcon("Tree.closedIcon"));
    }

    if(selected)
    {
      r.setBackground(UIManager.getColor("Tree.selectionBackground"));
      r.setForeground(UIManager.getColor("Tree.selectionForeground"));
      r.setBorder(UIManager.getBorder("Tree.selectedCellBorder"));
    }
    else
    {
      r.setBackground(UIManager.getColor("Tree.background"));
      r.setForeground(UIManager.getColor("Tree.foreground"));
      r.setBorder(UIManager.getBorder("Tree.cellBorder"));
    }

    return r;
  }
}
