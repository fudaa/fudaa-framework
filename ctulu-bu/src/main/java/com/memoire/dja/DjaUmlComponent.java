/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut       unstable
 * @file         DjaUmlComponent.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

public class DjaUmlComponent
       extends DjaForm
{
  private transient Dimension size_;

  public DjaUmlComponent()
  {
    this(null);
  }

  public DjaUmlComponent(String _text)
  {
    super(null);

    DjaText t0=new DjaText(this,0,_text,true,0,0,
			   RELATIVE_NW,0,MIDDLE,null,
			   null,false,false);
    setTextArray(new DjaText[] { t0 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (0);
    super.setHeight(0);
    textChanged(t0);
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();
    Dimension d=r[0].getSize();
    r[0].setW(getWidth()-24);
    r[0].setX(22);
    r[0].setY((getHeight()-d.height)/2);
    return r;
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    int l=11;
    if(h>=70) l++;

    DjaAnchor[] r=new DjaAnchor[l];

    x+=10; w-=10;
    r[0] =new DjaAnchor(this, 0,NORTH,x+w/4  ,y);
    r[1] =new DjaAnchor(this, 1,NORTH,x+w/2  ,y);
    r[2] =new DjaAnchor(this, 2,NORTH,x+w*3/4,y);
    r[3] =new DjaAnchor(this, 3,SOUTH,x+w/4  ,y+h);
    r[4] =new DjaAnchor(this, 4,SOUTH,x+w/2  ,y+h);
    r[5] =new DjaAnchor(this, 5,SOUTH,x+w*3/4,y+h);
    r[6] =new DjaAnchor(this, 6,EAST ,x+w    ,y+h/4);
    r[7] =new DjaAnchor(this, 7,EAST ,x+w    ,y+h/2);
    r[8] =new DjaAnchor(this, 8,EAST ,x+w    ,y+h*3/4);
    x-=10; w+=10;
    r[9] =new DjaAnchor(this, 9,WEST ,x      ,y+15);
    r[10]=new DjaAnchor(this,10,WEST ,x      ,y+35);

    if(h>=70)
    r[11]=new DjaAnchor(this,11,WEST ,x+10   ,y+h*3/4);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[3];
    r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
    Color bg=getBackground();
    Color fg=getForeground();

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x+10,y,w-10,h);
      DjaGraphics.fillRect(_g,x,y+10,20,10);
      DjaGraphics.fillRect(_g,x,y+30,20,10);
    }

    if(fg!=null)
    {
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawRect(_g,x,y+10,20,10,bp);
      DjaGraphics.drawRect(_g,x,y+30,20,10,bp);
      DjaGraphics.drawLine(_g,x+10,y+10,x+10,y,bp);
      DjaGraphics.drawLine(_g,x+10,y,x+w,y,bp);
      DjaGraphics.drawLine(_g,x+w,y,x+w,y+h,bp);
      DjaGraphics.drawLine(_g,x+w,y+h,x+10,y+h,bp);
      DjaGraphics.drawLine(_g,x+10,y+h,x+10,y+40,bp);
      DjaGraphics.drawLine(_g,x+10,y+30,x+10,y+20,bp);
    }

    super.paintObject(_g);
  }

  public Dimension optimalSize()
  {
    Dimension d=getTextArray()[0].getSize();
    int w=Math.max(32,d.width );
    int h=Math.max(51,d.height);
    return new Dimension(w+22,h+2);
  }
}
