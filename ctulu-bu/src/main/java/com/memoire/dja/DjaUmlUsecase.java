/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaUmlUsecase.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Dimension;
import java.awt.Font;

public class DjaUmlUsecase
       extends DjaEllipse
{
  private transient Dimension size_;

  public DjaUmlUsecase()
  {
    this(null);
  }

  public DjaUmlUsecase(String _text)
  {
    super(null);

    DjaText t0=new DjaText(this,0,_text,true,0,0,
			   CENTER,0,MIDDLE,null,
			   null,false,false);
    setTextArray(new DjaText[] { t0 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (0);
    super.setHeight(0);
    textChanged(t0);
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[3];
    r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);

    return r;
  }

  public Dimension optimalSize()
  {
    Dimension d0=getTextArray()[0].getSize();
    int w=(d0.width *11)/10;
    int h=(d0.height*11)/10;
    return new Dimension(Math.max(61,w+10),h+10);
  }
}
