/**
 * @modification $Date$
 * @statut       unstable
 * @file         DjaLoadSavePng.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

public class DjaLoadSavePng
  extends DjaLoadSaveImage
{
  public DjaLoadSavePng()
  {
    super("Png");
  }
}
