/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaRoundBox.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;


public class DjaRoundBox
       extends DjaBox
{
  public DjaRoundBox(String _texte)
  {
    super(_texte);
    putProperty("rayon","10");
  }

  public DjaRoundBox()
  {
    this(null);
  }

  private static class PC extends DjaControl
  {
    public PC(DjaObject _f,int _p,int _o,int _x,int _y)
    {
      super(_f,_p,_o,_x,_y);
    }

    @Override
    public void draggedTo(int _x, int _y)
    {
      DjaObject p=getParent();
      p.putProperty
	  ("rayon",
	   ""+Math.max(0,Math.min
		       (Math.min(p.getWidth(),
				 p.getHeight())/2,_x-p.getX())));
    }
  }

  @Override
  public DjaControl[] getControls()
  {
    int rayon=Integer.parseInt(getProperty("rayon"));

    DjaControl[] r=new DjaControl[1];
    r[0]=new PC(this,0,HORIZONTAL,getX()+rayon,getY());
    return r;
  }
}
