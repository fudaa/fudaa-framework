/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaUmlLifeline.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaUmlLifeline
       extends DjaLink
{
  public DjaUmlLifeline()
  {
    super();

    putProperty("debut","20");
    putProperty("fin"  ,"50");
    putProperty("destruction","vrai");

    xbegin_=2*deltaX;
    ybegin_=2*deltaY;
    yend_  =ybegin_+80;
  }

  @Override
  protected void updateXYO()
  {
    super.updateXYO();

    int fin=Integer.parseInt(getProperty("fin"));

    xend_  =xbegin_;
    if(yend_<ybegin_+fin) yend_=ybegin_+fin;
    obegin_=NORTH;
    oend_  =SOUTH;
  }

  private static class PC extends DjaControl
  {
      public PC(DjaObject _f,int _p,int _o,int _x,int _y)
      {
	super(_f,_p,_o,_x,_y);
      }

      @Override
      public void draggedTo(int _x, int _y)
      {
	DjaUmlLifeline p=(DjaUmlLifeline)getParent();
	int debut=Integer.parseInt(p.getProperty("debut"));
	int fin  =Integer.parseInt(p.getProperty("fin"));

	if(getP()==0)
	  p.putProperty("debut",
			""+Math.max(0,Math.min
				    (fin-deltaY,_y-p.getY())));
	else
	  p.putProperty("fin",
			""+Math.max(debut+deltaY,Math.min
					(p.getHeight()-deltaY,_y-p.getY())));
      }
  }

  @Override
  public DjaControl[] getControls()
  {
    updateXYO();

    int d=Integer.parseInt(getProperty("debut"));
    int f=Integer.parseInt(getProperty("fin"));

    DjaControl[] r=new DjaControl[2];
    r[0]=new PC(this,0,VERTICAL,xbegin_,ybegin_+d);
    r[1]=new PC(this,1,VERTICAL,xbegin_,ybegin_+f);
    return r;
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    updateXYO();

    int d=Integer.parseInt(getProperty("debut"));
    int f=Integer.parseInt(getProperty("fin"));

    return    new Rectangle(xbegin_-5,ybegin_+d,11,Math.max(0,f-d)).contains(_x,_y)
           || DjaLib.close(xbegin_,ybegin_,xend_,yend_,_x,_y);
  }

  @Override
  public void paintObject(Graphics _g)
  {
    Color fg=getForeground();
    Color bg=getBackground();

    if(fg!=null)
    {
      _g.setColor(fg);
      DjaGraphics.BresenhamParams bp=new DjaGraphics.BresenhamParams(4,4,1);
      DjaGraphics.drawLine(_g,xbegin_,ybegin_,xend_,yend_,bp);
    }

    int d=Integer.parseInt(getProperty("debut"));
    int f=Integer.parseInt(getProperty("fin"));

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,xbegin_-5,ybegin_+d,11,Math.max(0,f-d));
    }

    if(fg!=null)
    {
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawRect(_g,xbegin_-5,ybegin_+d,11,Math.max(0,f-d),bp);

      if("vrai".equals(getProperty("destruction")))
      {
	DjaGraphics.drawLine(_g,xend_-5,yend_-5,xend_+5,yend_+5,bp);
	DjaGraphics.drawLine(_g,xend_-5,yend_+5,xend_+5,yend_-5,bp);
      }
    }

    super.paintObject(_g);
  }

  /*
  public void paintText(Graphics _g)
  {
  }
  */
}
