/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaMatrixHermite.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Point;
import java.awt.Polygon;

public class DjaMatrixHermite
{
  public static final DjaMatrixHermite HERMITE=
    new DjaMatrixHermite(new double[][]
			 { { 2,-2, 1, 1 },
			   {-3, 3,-2,-1 },
			   { 0, 0, 1, 0 },
			   { 1, 0, 0, 0 } } );

  private double a[][];

  public DjaMatrixHermite(int ni, int nj)
  {
    a=new double[ni][nj];
  }
  
  public DjaMatrixHermite(double[][] _a)
  {
    a=_a;
  }

  public double a(int i, int j)
  {
    return a[i][j];
  }

  public void a(int i, int j, double ia)
  {
    a[i][j]=ia;
  }

  public DjaMatrixHermite multiplication(DjaMatrixHermite m)
  {
    int ni=a.length;
    int nj=m.a[0].length;
    DjaMatrixHermite r=new DjaMatrixHermite(ni,nj);

    for(int i=0; i<ni; i++)
      for(int j=0; j<nj; j++)
        for(int k=0; k<a[0].length; k++)
          r.a[i][j]+=a[i][k]*m.a[k][j];
    return r;
  }

  public static Point hermite
      (double t, int x1, int y1, int x2, int y2,
       double vx1, double vy1, double vx2, double vy2)
       //int vx1, int vy1, int vx2, int vy2)
  {
    DjaMatrixHermite H=DjaMatrixHermite.HERMITE;
    DjaMatrixHermite T=new DjaMatrixHermite
      (new double[][] { { t*t*t, t*t, t, 1 } } );
    DjaMatrixHermite X=new DjaMatrixHermite(new double[][] {
              { x1,   y1, 0, 1 },
              { x2,   y2, 0, 1 },
              { vx1, vy1, 0, 0 },
              { vx2, vy2, 0, 0 } } );
    DjaMatrixHermite Q=T.multiplication(H).multiplication(X);
    return new Point((int)Math.round(Q.a(0, 0)),
		     (int)Math.round(Q.a(0, 1)));
  }

  public static Polygon arc2polyline
    (int x1, int y1, int x2, int y2,
     double vx1, double vy1, double vx2, double vy2)
  {    
    //int nb=(Math.abs(x2-x1)+Math.abs(y2-y1))*3/2;///2;
    int nb=(int)Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))/2;
    //nb+=(nb&3);
    if(nb<2 ) nb=2;
    if(nb>48) nb=48;

    Point pf;
    int x[]=new int[nb+1];
    int y[]=new int[nb+1];
    for(int i=0; i<nb+1; i++)
    {
      pf=DjaMatrixHermite.hermite
	  ((double)i/(double)nb, x1, y1, x2, y2, vx1, vy1, vx2, vy2);
      x[i]=pf.x;
      y[i]=pf.y;
    }

    return new Polygon(x, y, x.length);
  }
}
