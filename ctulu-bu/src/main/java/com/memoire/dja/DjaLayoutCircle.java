/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaLayoutCircle.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.util.Enumeration;

public class DjaLayoutCircle
  implements DjaLayoutInterface
{
  public DjaLayoutCircle()
  {
  }

  @Override
  public void layout(DjaGrid _grid)
  {
    DjaVector v=new DjaVector();
    for(Enumeration e=_grid.enumerate(DjaForm.class);
	e.hasMoreElements(); )
      v.addElement(e.nextElement());

    int    n=v.size();
    double r=20.+10.*n;

    for(int i=0;i<n;i++)
    {
      DjaForm f=(DjaForm)v.elementAt(i);
      double  a=i*Math.PI*2D/n;
      int     x=10+40+(int)Math.round(1.2*r+1.2*r*Math.sin(a));
      int     y=10+20+(int)Math.round(0.8*r-0.8*r*Math.cos(a));
      f.setX(x);
      f.setY(y);
    }
  }
}
