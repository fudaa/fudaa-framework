/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut       unstable
 * @file         DjaText.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuLib;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.Serializable;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;

public class DjaText implements DjaOwner, DjaOptions, Cloneable, Serializable {
  private DjaObject parent_;
  private int num_;
  private String text_;
  private boolean multiline_;
  private int x_;
  private int y_;
  protected int w_; // minimal width
  protected int h_; // minimal height
  private int position_;
  private int reference_;
  private int alignment_;
  private DjaColor color_ = new DjaColor(Color.BLACK);
  private DjaFont font_;
  private boolean underlined_;
  private String before_;
  private String after_;
  private boolean selected_;
  private boolean moveable_;
  private boolean editable_;
  private boolean visible_;

  public DjaText() {
    this(null, 0, "", true, 0, 0, CENTER, 0, LEFT, null, null, false, true);
  }

  public DjaText(DjaObject _parent, int _num, String _text) {
    this(_parent, _num, _text, true, 0, 0, CENTER, 0, LEFT, null, null, false, true);
  }

  public DjaText(DjaObject _parent, int _num, String _text, boolean _multiline, int _x, int _y) {
    this(_parent, _num, _text, _multiline, _x, _y, CENTER, 0, LEFT, null, null, false, true);
  }

  public DjaText(DjaObject _parent, int _num, String _text, boolean _multiline, int _x, int _y, int _position,
      int _reference, int _alignment, Color _color, Font _font, boolean _underlined, boolean _moveable) {
    parent_ = _parent;
    num_ = _num;
    text_ = _text;
    multiline_ = _multiline;
    x_ = _x;
    y_ = _y;
    w_ = 0;
    h_ = 0;
    position_ = _position;
    reference_ = _reference;
    alignment_ = _alignment;
    underlined_ = _underlined;
    selected_ = true;
    moveable_ = _moveable;
    editable_ = true;
    visible_ = true;
    setColor(_color);
    setFont(_font);
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  // DjaOwner

  @Override
  public DjaGrid getGrid() {
    DjaGrid r = null;

    DjaOwner o = getOwner();
    while (o != null) {
      if (o instanceof DjaGrid) {
        r = (DjaGrid) o;
        break;
      }
      o = o.getOwner();
    }

    return r;
  }

  private void fireChange() {
    if (getParent() != null) {
      try {
        getParent().textChanged(this);
      } catch (Exception ex) {}
    }
    fireGridEvent(this, DjaGridEvent.MODIFIED);
  }

  public void fireGridEvent(DjaOwner _object, int _id) {
    DjaGrid grid = getGrid();
    if (grid != null) grid.fireGridEvent(_object, _id);
  }

  @Override
  public final DjaOwner getOwner() {
    return getParent();
  }

  // Properties

  public DjaObject getParent() {
    return parent_;
  }

  /* package */
  void setParent(DjaObject _parent) {
    parent_ = _parent;
  }

  public int getNum() {
    return num_;
  }

  void setNum(int _num) {
    num_ = _num;
  }

  public String getText() {
    return text_;
  }

  public String getDisplayText() {
    String r = getText();

    if (r != null) {
      if (before_ != null) r = before_ + r;
      if (after_ != null) r = r + after_;
    }

    return r;
  }

  public void setText(String _text) {
    view_ = null;
    String text = _text;
    if ("".equals(text)) text = null;
    text_ = text;
    fireChange();
  }

  public boolean isMultiline() {
    return multiline_;
  }

  public void setMultiline(boolean _multiline) {
    multiline_ = _multiline;
  }

  public boolean isEditable() {
    return editable_;
  }

  public void setEditable(boolean _editable) {
    editable_ = _editable;
  }

  public int getX() {
    return x_;
  }

  public void setX(int _x) {
    x_ = _x;
  }

  public int getY() {
    return y_;
  }

  public void setY(int _y) {
    y_ = _y;
  }

  public int getW() {
    return w_+2*marge_;
  }

  public void setW(int _w) {
    w_ = _w;
  }

  public int getH() {
    return h_+2*marge_;
  }

  public void setH(int _h) {
    h_ = _h;
  }

  public Color getColor() {
    Color r = null;
    if (color_ != null) r = color_.getColor();

    if (r == null) r = getParent().getColor();
    if ((r != null) && isSelected()) r = BuLib.mixColors(r, selectionTextColor);

    return r;
  }

  public void setColor(Color _color) {
    if (_color != null) color_ = new DjaColor(_color);
    else
      color_ = null;
  }

  public Font getFont() {
    Font r = null;
    if (font_ != null) r = font_.getFont();
    if (r == null) r = getParent().getFont();
    return r;
  }

  public void setFont(Font _font) {
    if (_font != null) font_ = new DjaFont(_font);
    else
      font_ = null;
    fireChange();
  }

  public int getPosition() {
    return position_;
  }

  public void setPosition(int _position) {
    position_ = _position;
  }

  public int getReference() {
    return reference_;
  }

  public void setReference(int _reference) {
    reference_ = _reference;
  }

  public int getAlignment() {
    return alignment_;
  }

  public void setAlignment(int _alignment) {
    alignment_ = _alignment;
  }

  public boolean isUnderlined() {
    return underlined_;
  }

  public void setUnderlined(boolean _underlined) {
    underlined_ = _underlined;
  }

  public boolean isSelected() {
    return selected_ && getParent().isSelected();
  }

  public void setSelected(boolean _selected) {
    selected_ = _selected;
  }

  public boolean isMoveable() {
    return moveable_ && getParent().isSelected();
  }

  public void setMoveable(boolean _moveable) {
    moveable_ = _moveable;
  }

  public String getBefore() {
    return before_;
  }

  public void setBefore(String _before) {
    before_ = _before;
  }

  public String getAfter() {
    return after_;
  }

  public void setAfter(String _after) {
    after_ = _after;
  }

  // Methods
  protected int marge_ ;

  public Point getLocation() {
    DjaObject parent = getParent();

    int xo = parent.getX() + marge_;
    int yo = parent.getY() + marge_;

    int xt = xo;
    int yt = yo;

    switch (getPosition()) {
    case NORTH: {
      Dimension d = getSize();
      xt += (parent.getWidth() - d.width) / 2 + x_;
      yt += -d.height + y_;
    }
      break;
    case SOUTH: {
      Dimension d = getSize();
      xt += (parent.getWidth() - d.width) / 2 + x_;
      yt += parent.getHeight() + 1 + y_;
      break;
    }
    case EAST: {
      Dimension d = getSize();
      xt += parent.getWidth() + 1 + x_;
      yt += (parent.getHeight() - d.height) / 2 + y_;
    }
      break;
    case WEST: {
      Dimension d = getSize();
      xt += -d.width - 2 + x_;
      yt += (parent.getHeight() - d.height) / 2 + y_;
    }
      break;
    case RELATIVE_NW: {
      xt += x_;
      yt += y_;
    }
      break;
    case RELATIVE_NE: {
      xt += parent.getWidth() + x_;
      yt += y_;
    }
      break;
    case RELATIVE_SW: {
      xt += x_;
      yt += parent.getHeight() + y_;
    }
      break;
    case RELATIVE_SE: {
      xt += parent.getWidth() + x_;
      yt += parent.getHeight() + y_;
    }
      break;
    /*
     * case RELATIVE_CENTER: { xt+=parent.getWidth()/2+x_; yt+=parent.getHeight()/2+y_; } break;
     */
    case RELATIVE_ANCHOR: {
      DjaAnchor anchor = parent.getAnchors()[reference_];
      xt = anchor.getX() + x_;
      yt = anchor.getY() + y_;
    }
      break;
    case RELATIVE_ATTACH: {
      DjaAttach attach = parent.getAttachs()[reference_];
      xt = attach.getX() + x_;
      yt = attach.getY() + y_;
    }
      break;
    case RELATIVE_CONTROL: {
      DjaControl control = parent.getControls()[reference_];
      xt = control.getX() + x_;
      yt = control.getY() + y_;
    }
      break;
    case RELATIVE_TEXT_NW: {
      DjaText t = parent.getTextArray()[reference_];
      if (t.getNum() >= getNum()) throw new RuntimeException("can not refer a higher text " + getParent() + " "
          + getNum());
      Point p = t.getLocation();
      xt = p.x + x_;
      yt = p.y + y_;
    }
      break;
    case RELATIVE_TEXT_NE: {
      DjaText t = parent.getTextArray()[reference_];
      if (t.getNum() >= getNum()) throw new RuntimeException("can not refer a higher text " + getParent() + " "
          + getNum());
      Point p = t.getLocation();
      Dimension d = t.getSize();
      xt = p.x + x_ + d.width;
      yt = p.y + y_;
    }
      break;
    case RELATIVE_TEXT_SW: {
      DjaText t = parent.getTextArray()[reference_];
      if (t.getNum() >= getNum()) throw new RuntimeException("can not refer a higher text " + getParent() + " "
          + getNum());
      Point p = t.getLocation();
      Dimension d = t.getSize();
      // System.err.println(""+getNum()+","+t.getNum()+" : "+p+" "+d);
      xt = p.x + x_;
      yt = p.y + y_ + d.height;
      // System.err.println("--> "+xt+","+yt);
    }
      break;
    case RELATIVE_TEXT_SE: {
      DjaText t = parent.getTextArray()[reference_];
      if (t.getNum() >= getNum()) throw new RuntimeException("can not refer a higher text " + getParent() + " "
          + getNum());
      Point p = t.getLocation();
      Dimension d = t.getSize();
      xt = p.x + x_ + d.width;
      yt = p.y + y_ + d.height;
    }
      break;
    case ABSOLUTE: {
      xt = x_;
      yt = y_;
    }
      break;
    default: {
      Dimension d = getSize();
      xt += (parent.getWidth() - d.width) / 2 + x_;
      yt += (parent.getHeight() - d.height) / 2 + y_;
    }
      break;
    }

    return new Point(xt, yt);
  }

  public void setLocation(Point _p) {
    Point q;

    if (getPosition() == ABSOLUTE) {
      q = _p;
    } else {
      q = getLocation();
      q.x = x_ + _p.x - q.x;
      q.y = y_ + _p.y - q.y;
    }

    DjaLib.snap(q);
    x_ = q.x;
    y_ = q.y;
  }

  public Dimension optimalSize() {
    String tx = getDisplayText();
    return DjaGraphics.getTextSize((tx == null ? "  " : tx), getFont());
  }

  public Dimension getSize() {
    Dimension r = optimalSize();
    r.width = Math.max(w_, r.width);
    r.height = Math.max(h_, r.height);
    return r;
  }

  public Rectangle getBounds() {
    Rectangle r = new Rectangle();
    Point p = getLocation();
    Dimension d = getSize();

    r.x = p.x;
    r.y = p.y;
    r.width = d.width;
    r.height = d.height;
    return r;
  }

  protected View view_;

  public void paint(Graphics _g) {
    if (!visible_) return;
    String tx = getDisplayText();
    if ((tx == null)) return;
    Color tc = Color.BLACK;
    if (color_ != null) tc = color_.getColor();

    _g.setColor(tc);
    Rectangle bd = getBounds();
    if (BasicHTML.isHTMLString(tx)) {
      _g.setColor(Color.BLACK);
      if (view_ == null) view_ = DjaGraphics.drawHtmlText(_g, getGrid(), tx, bd);
      else {
        view_.paint(_g, bd);
      }
      return;
    }
    _g.setFont(getFont());
    _g.setColor(tc);
    DjaGraphics.drawText(_g, tx, bd.x, bd.y, getAlignment(), bd.width, isUnderlined());
  }

  public boolean isVisible() {
    return visible_;
  }

  public void setVisible(boolean _visible) {
    visible_ = _visible;
  }
}
