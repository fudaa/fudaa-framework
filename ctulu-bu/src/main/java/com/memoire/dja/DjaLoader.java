/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaLoader.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.Hashtable;
import java.util.Stack;

public class DjaLoader
{
  /*
  public static DjaGrid loadAsSer(InputStream _in)
       throws IOException, ClassNotFoundException
  {
    return (DjaGrid)new ObjectInputStream(_in).readObject();
  }
  */

  private static final Color color(String _s)
  {
    Color r=null;
    if(!"null".equals(_s))
      r=new Color((int)Long.parseLong(_s,16));
    return r;
  }

  private static final Font font(String _p, int _line)
  {
    Font r=null;

    if(!"null".equals(_p))
    {
      try
      {
	String n=DjaCoder.decode(_p);
	int i=n.indexOf(",");
	int j=n.lastIndexOf(",");

	String ff=n.substring(0,i);
	int    ft=Integer.parseInt(n.substring(j+1));

	int fs=Font.PLAIN;
	if(n.indexOf(",gras,")    >=0) fs|=Font.BOLD;
	if(n.indexOf(",italique,")>=0) fs|=Font.ITALIC;

	r=new Font(ff,fs,ft);
      }
      catch(Exception ex)
      {
	System.err.println("Unreconized font: "+_p+" [line "+_line+"]");
	r=DjaOptions.defaultPlainFont;
      }
    }

    return r;
  }

  private static final String text(String _t)
  {
    String r=null;
    if(!"null".equals(_t)) r=DjaCoder.decode(_t);
    return r;
  }

  private static final DjaObject unid(Hashtable _tbl, String _id)
  {
    // if(_id==null) { System.err.println("id=null"); System.exit(0); }
    return ("null".equals(_id) ? null : (DjaObject)_tbl.get(_id));
  }

  /*
  public static DjaGrid loadGridAsText(InputStream _in)
  { return loadGridAsText(_in,false); }
  */

  /*
  public static DjaGrid loadGridAsText(InputStream _in, boolean _interactive)
  {
    DjaGrid r;

    if(_interactive) r=new DjaGridInteractive();
    else             r=new DjaGrid();

    r.setObjects(loadAsText(_in));
    return r;
  }
  */

  public static DjaVector loadAsText(InputStream _in)
  {
    DjaVector r=null;

    TextReader lin=new TextReader(_in);

    try
    {
      String t=lin.get();
      while(!t.equals(""))
      {
	if(t.equals("grille")) r=parseGrid(lin);
	else throw new Exception("Syntax Error: "+t);

	t=lin.get();
      }
    }
    catch(Throwable ex)
    {
      System.err.println("Unreconized format [line "+lin.line()+"]");
      System.err.println("DjaLoader: loadAsText(): "+ex.getMessage());
    }

    return r;
  }

  private static void checkToken(TextReader lin, String s)
  {
    String t=lin.get();
    if(!s.equals(t)) throw new RuntimeException("Syntax Error: "+t+" instead of "+s);
  }

  public static void parseBlock(TextReader lin)
  {
    int n=0;
    String t;

    while(true)
    {
      t=lin.get();
      if("" .equals(t)) break;

           if("{".equals(t)) n++;
      else if("}".equals(t)) { n--; if(n==0) break; }
      // else System.err.println("    "+t);
    }
  }

  public static void parseRegistry(TextReader lin)
  {
    checkToken(lin,"{");

    String t;

    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      String n=text(lin.get());
      // System.err.println("parse registry: "+t+" -> "+n);
      DjaRegistry.register(t,n);
    }
  }

  public static void parseForm(TextReader lin, DjaForm o, Hashtable tbl)
  {
    checkToken(lin,"{");

    String t;
    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("  "+t);
           if("id".equals(t)) tbl.put(lin.get(),o);
      else if("x"         .equals(t)) o.setX(Integer.parseInt(lin.get()));
      else if("y"         .equals(t)) o.setY(Integer.parseInt(lin.get()));
      else if("largeur"   .equals(t)) o.setWidth (Integer.parseInt(lin.get()));
      else if("hauteur"   .equals(t)) o.setHeight(Integer.parseInt(lin.get()));
      else if("contour"   .equals(t)) parseForeground(lin,o);
      else if("surface"   .equals(t)) parseBackground(lin,o);
      else if("texte"     .equals(t)) parseText(lin,o);
      else if("proprietes".equals(t)) parseProperties(lin,o);
      else parseBlock(lin);
    }
  }

  public static void parseArrow(TextReader lin, DjaLink o, Hashtable tbl)
  {
    checkToken(lin,"{");

    String t;
    int x=0,y=0;

    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("  "+t);
           if("id".equals(t)) tbl.put(lin.get(),o);
      else if("x"       .equals(t)) o.setBeginX(x=Integer.parseInt(lin.get()));
      else if("y"       .equals(t)) o.setBeginY(y=Integer.parseInt(lin.get()));
      else if("largeur"   .equals(t)) o.setEndX(x+Integer.parseInt(lin.get()));
      else if("hauteur"   .equals(t)) o.setEndY(y+Integer.parseInt(lin.get()));
      else if("contour"   .equals(t)) parseForeground(lin,o);
      else if("surface"   .equals(t)) parseBackground(lin,o);
      else if("texte"     .equals(t)) parseText(lin,o);
      else if("proprietes".equals(t)) parseProperties(lin,o);
      else parseBlock(lin);
    }
  }

  public static void parseForeground(TextReader lin, DjaObject o)
  {
    checkToken(lin,"{");

    String t;

    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("  "+t);
      if("couleur" .equals(t)) o.setForeground(color(lin.get()));
    }
  }

  public static void parseBackground(TextReader lin, DjaObject o)
  {
    checkToken(lin,"{");

    String t;

    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("  "+t);
      if("couleur" .equals(t)) o.setBackground(color(lin.get()));
    }
  }

  public static void parseText(TextReader lin, DjaObject o)
  {
    checkToken(lin,"{");

    String  t;
    DjaText r=new DjaText(o,0,null);

    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("  "+t);
           if("valeur"     .equals(t)) r.setText      (text(lin.get()));
      else if("avant"      .equals(t)) r.setBefore    (text(lin.get()));
      else if("apres"      .equals(t)) r.setAfter     (text(lin.get()));
      else if("numero"     .equals(t)) r.setNum       (Integer.parseInt(lin.get()));
      else if("x"          .equals(t)) r.setX         (Integer.parseInt(lin.get()));
      else if("y"          .equals(t)) r.setY         (Integer.parseInt(lin.get()));
      else if("largeur"    .equals(t)) r.setW         (Integer.parseInt(lin.get()));
      else if("hauteur"    .equals(t)) r.setH         (Integer.parseInt(lin.get()));
      else if("position"   .equals(t)) r.setPosition  (Integer.parseInt(lin.get()));
      else if("reference"  .equals(t)) r.setReference (Integer.parseInt(lin.get()));
      else if("alignement" .equals(t)) r.setAlignment (Integer.parseInt(lin.get()));
      else if("couleur"    .equals(t)) r.setColor     (color(lin.get()));
      else if("police"     .equals(t)) r.setFont      (font(lin.get(),lin.line()));
      else if("multiligne" .equals(t)) r.setMultiline ("vrai".equals(lin.get()));
      else if("souligne"   .equals(t)) r.setUnderlined("vrai".equals(lin.get()));
      else if("selectionne".equals(t)) r.setSelected  ("vrai".equals(lin.get()));
    }

    /*
    DjaText[] texts=o.getTextArray();
    int       n    =r.getNum();
    if((n>=0)&&(n<texts.length)) texts[n]=r;
    else
    */                         
    o.addText(r);
  }

  public static void parseProperties(TextReader lin, DjaObject o)
  {
    checkToken(lin,"{");

    String k,v;

    while(true)
    {
      k=lin.get();
      if("}".equals(k)) break;
      if("" .equals(k)) break;

      // System.err.println("  "+k);
      v=lin.get().replace('�','"');
      o.putProperty(k,v);
    }
  }

  public static void parseLink(TextReader lin, Hashtable tbl)
  {
    checkToken(lin,"{");

    String t;
    DjaLink a=null;

    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("  "+t);
           if("id"   .equals(t))            a=(DjaLink)unid(tbl,lin.get());
      else if("debut".equals(t)&&(a!=null)) parseBracket(lin,a,false,tbl);
      else if("fin"  .equals(t)&&(a!=null)) parseBracket(lin,a,true,tbl);
      else parseBlock(lin);
    }
  }

  public static void parseBracket(TextReader lin, DjaLink o,
				  boolean end, Hashtable tbl)
  {
    checkToken(lin,"{");

    String t;
    while(true)
    {
      t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;

      // System.err.println("    "+t);

      if(!end)
      {
             if("type"    .equals(t)) o.setBeginType(Integer.parseInt(lin.get()));
        else if("x"       .equals(t)) o.setBeginX(Integer.parseInt(lin.get()));
        else if("y"       .equals(t)) o.setBeginY(Integer.parseInt(lin.get()));
        else if("o"       .equals(t)) o.setBeginO(Integer.parseInt(lin.get()));
        else if("id"      .equals(t)) o.setBeginObject(unid(tbl,lin.get()));
        else if("position".equals(t)) o.setBeginPosition(Integer.parseInt(lin.get()));
      }
      else
      {
             if("type"    .equals(t)) o.setEndType(Integer.parseInt(lin.get()));
        else if("x"       .equals(t)) o.setEndX(Integer.parseInt(lin.get()));
        else if("y"       .equals(t)) o.setEndY(Integer.parseInt(lin.get()));
        else if("o"       .equals(t)) o.setEndO(Integer.parseInt(lin.get()));
        else if("id"      .equals(t)) o.setEndObject(unid(tbl,lin.get()));
        else if("position".equals(t)) o.setEndPosition(Integer.parseInt(lin.get()));
      }
    }
  }

  public static DjaVector parseGrid(TextReader lin)
  {
    DjaVector r=new DjaVector();

    checkToken(lin,"{");

    Hashtable tbl=new Hashtable();

    while(true)
    {
      String t=lin.get();
      if("}".equals(t)) break;
      if("" .equals(t)) break;
      // System.err.println(t);


      if("registre".equals(t))
	parseRegistry(lin);
      else
      if("lien".equals(t))
	parseLink(lin,tbl);
      else
      if("ordre".equals(t)||"position".equals(t))
	parseBlock(lin);
      else
      {
	DjaObject o=DjaRegistry.getInstance(t);

	if(o instanceof DjaForm)
	  { parseForm(lin,(DjaForm)o,tbl); r.addElement(o); }
	else
	if(o instanceof DjaLink)
	  { parseArrow(lin,(DjaLink)o,tbl); r.addElement(o); }
	else
	  parseBlock(lin);

	if(o!=null) o.afterLoading();
      }
    }

    return r;
  }

  public static final class TextReader
  {
    StreamTokenizer st;
    Stack           ss;

    public TextReader(InputStream is)
    {
      st=new StreamTokenizer
	(new InputStreamReader(new BufferedInputStream(is, 1024)));
      st.resetSyntax();
      st.eolIsSignificant(false);
      st.quoteChar('"');
      st.quoteChar('\'');
      st.slashSlashComments(true);
      st.slashStarComments(true);
      st.wordChars('a','z');
      st.wordChars('A','Z');
      st.wordChars('0','9');
      st.wordChars('_','_');
      st.wordChars('.','.');
      st.wordChars('+','+');
      st.wordChars('-','-');
      st.wordChars('[','[');
      st.wordChars(']',']');
      st.whitespaceChars(' ',' ');
      st.whitespaceChars('\t','\t');
      st.whitespaceChars('\n','\n');
      st.whitespaceChars('\r','\r');
      
      ss=new Stack();
    }

    String get()
    {
      String r="";

      if(ss.empty())
      {
	try
	{
	  st.nextToken();
	  // System.err.println(">>>"+st.ttype+" "+st.sval);

	  switch(st.ttype)
	  {
          case StreamTokenizer.TT_EOF   : r="";                break;
          case StreamTokenizer.TT_WORD  : r=st.sval;           break;
          case StreamTokenizer.TT_NUMBER: r=""+st.nval;        break;
          case '"'                      : r=st.sval;           break;
          case '\''                     : r=""+st.sval;        break;
          default                       : r=""+(char)st.ttype; break;
	  }
	}
	catch(Exception ex) { System.err.println("Error: "+ex); }
      }
      else r=(String)ss.pop();

      return r;
    }

    void put(String s)
    {
      ss.push(s);
    }

    int line()
    {
      return st.lineno();
    }
  }
}
