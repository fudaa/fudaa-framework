/**
 * @modification $Date: 2006-09-19 14:34:55 $
 * @statut       unstable
 * @file         DjaUmlState.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

public class DjaUmlState
       extends DjaBox
{
  private transient Dimension size_;

  public DjaUmlState()
  {
    this(null,0);
  }

  public DjaUmlState(String _text)
  {
    this(_text,0);
  }

  public DjaUmlState(String _text, int _type)
  {
    super(null);

    DjaText t0=new DjaText(this,0,_text,true,0,0,
			   CENTER,0,MIDDLE,null,
			   null,false,false);
    setTextArray(new DjaText[] { t0 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (0);
    super.setHeight(0);

    putProperty("rayon","10");
    putProperty("type",""+_type);

    textChanged(t0);
  }

  @Override
  public void putProperty(String _k, String _v)
  {
    super.putProperty(_k,_v);

    if("type".equals(_k))
    {
      int type=Integer.parseInt(_v);

      DjaText t=getTextArray()[0];

      switch(type)
      {
      case 0: t.setPosition(CENTER); break;
      case 1: t.setPosition(NORTH ); break;
      case 2: t.setPosition(SOUTH ); break;
      }
    }

    size_=optimalSize();
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[4];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[1]=new DjaAnchor(this,1,EAST ,x+w-1,y+h/2);
    r[2]=new DjaAnchor(this,2,SOUTH,x+w/2,y+h-1);
    r[3]=new DjaAnchor(this,3,WEST ,x    ,y+h/2);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    int type=Integer.parseInt(getProperty("type"));

    DjaHandle[] r;
    if(type==0)
    {
      r=new DjaHandle[3];
      r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
      r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
      r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);
    }
    else
    {
      r=new DjaHandle[1];
      r[0]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    }

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int type=Integer.parseInt(getProperty("type"));

    if(type==0)
    {
      super.paintObject(_g);
    }
    else
    {
      int x=getX();
      int y=getY();
      int w=getWidth();
      int h=getHeight();

      Color bg=getBackground();
      Color fg=getForeground();
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

      if(type==2)
      {
	if(bg!=null)
	{
	  DjaGraphics.setColor(_g,bg);
	  DjaGraphics.fillOval(_g,x,y,w,h);
	}
	if(fg!=null)
	{
	  DjaGraphics.setColor(_g,fg);
	  DjaGraphics.drawOval(_g,x,y,w-1,h-1,bp);
	}
      }

      if(fg!=null)
      {
	DjaGraphics.setColor(_g,fg);
	DjaGraphics.fillOval(_g,x+5,y+5,w-10,h-10);
      }
    }
  }

  private Dimension optimalSize()
  {
    if(getProperty("type")==null) return new Dimension(10,10);

    int type=Integer.parseInt(getProperty("type"));

    int w=0;
    int h=0;

    if(type==0)
    {
      Dimension d=getTextArray()[0].getSize();
      w=Math.max(50,d.width);
      h=d.height;
    }
    else
    {
      w=20;
      h=20;
    }

    return new Dimension(w+11,h+11);
  }
}

