/**
 * @modification $Date: 2006-09-19 14:34:55 $
 * @statut       unstable
 * @file         DjaSquare.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;


public class DjaSquare
       extends DjaBox
{
  public DjaSquare(String _texte)
  {
    super(_texte);
    setWidth(61);
  }

  public DjaSquare()
  {
    this(null);
  }

  @Override
  public void setWidth(int _w)
  {
    super.setHeight(_w);
    super.setWidth(_w);
  }

  @Override
  public void setHeight(int _h)
  {
    super.setWidth(_h);
    super.setHeight(_h);
  }
}
