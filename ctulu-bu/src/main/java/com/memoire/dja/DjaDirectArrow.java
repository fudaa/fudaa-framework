/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaDirectArrow.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;

public class DjaDirectArrow
       extends DjaLink
{
  private int xr0,yr0,xr1,yr1;

  public DjaDirectArrow(String _text)
  {
    super();

    xr0=xbegin_;
    yr0=ybegin_;
    xr1=xend_;
    yr1=yend_;

    if(_text!=null) addText(_text);
  }

  public DjaDirectArrow()
  {
    this(null);
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    return DjaLib.close(xr0,yr0,xr1,yr1,_x,_y);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    DjaAnchor[] r=new DjaAnchor[1];
    r[0]=new DjaAnchor(this,0,ANY,(xr0+xr1)/2,(yr0+yr1)/2);
    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    updateXYO();

    int x0=xbegin_;
    int y0=ybegin_;
    int o0=obegin_;
    int x1=xend_;
    int y1=yend_;
    int o1=oend_;

    //int x,y;

    xr0=x0;
    yr0=y0;
    xr1=x1;
    yr1=y1;

    if(tbegin_!=0)
    switch(o0)
    {
    case EAST:
      drawBracket(_g,tbegin_,o0,x0,y0-5,x0+10,y0+5);
      xr0+=11;
      break;
    case WEST:
      drawBracket(_g,tbegin_,o0,x0,y0-5,x0-10,y0+5);
      xr0-=11;
      break;
    case NORTH:
      drawBracket(_g,tbegin_,o0,x0-5,y0,x0+5,y0-10);
      yr0-=11;
      break;
    case SOUTH:
      drawBracket(_g,tbegin_,o0,x0-5,y0,x0+5,y0+10);
      yr0+=11;
      break;
    }
    
    if(tend_!=0)
    switch(o1)
    {
    case EAST:
      drawBracket(_g,tend_,o1,x1,y1-5,x1+10,y1+5);
      xr1+=11;
      break;
    case WEST:
      drawBracket(_g,tend_,o1,x1,y1-5,x1-10,y1+5);
      xr1-=11;
      break;
    case NORTH:
      drawBracket(_g,tend_,o1,x1-5,y1,x1+5,y1-10);
      yr1-=11;
      break;
    case SOUTH:
      drawBracket(_g,tend_,o1,x1-5,y1,x1+5,y1+10);
      yr1+=11;
      break;
    }

    if((tbegin_==1)||(tbegin_==4)) { xr0=xbegin_; yr0=ybegin_; }
    if((tend_  ==1)||(tend_  ==4)) { xr1=xend_;   yr1=yend_;   }

    _g.setColor(getForeground());
    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
    DjaGraphics.drawLine(_g,xr0,yr0,xr1,yr1,bp);

    super.paintObject(_g);
  }
}
