/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut       unstable
 * @file         DjaUmlClass.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

public class DjaUmlClass
       extends DjaBox
{
  private transient Dimension size_;

  public DjaUmlClass()
  {
    this(null);
  }

  public DjaUmlClass(String _text)
  {
    super(null);

    Font f0=new Font("SansSerif",Font.BOLD,18);

    DjaText t0=new DjaText(this,0,_text,false,2,2,
			   RELATIVE_NW,0,MIDDLE,null,f0,false,false);
    DjaText t1=new DjaText(this,1,_text,true,0,8,
			   RELATIVE_TEXT_SW,0,LEFT,null,null,false,false);
    DjaText t2=new DjaText(this,2,_text,true,0,8,
			   RELATIVE_TEXT_SW,1,LEFT,null,null,false,false);
    setTextArray(new DjaText[] { t0,t1,t2 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (61);
    super.setHeight(31);
    textChanged(t0);
    textChanged(t1);
    textChanged(t2);
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();

    /*
    r[1].setY(2);
    r[0].setY(r[1].getY()+r[1].getSize().height+2);
    r[2].setY(r[0].getY()+r[0].getSize().height+2);
    r[3].setY(r[2].getY()+r[2].getSize().height+8);
    */

    int w=getWidth()-5;
    r[0].setW(w);
    r[1].setW(w);
    r[2].setW(w);
    //r[3].setW(w);

    /*
    int h=super.getHeight()-r[3].getY();
    r[3].setH(h);
    */

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[3];
    r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Color bg=getBackground();
    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x,y,w,h);
    }

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    Color fg=getForeground();
    if(fg!=null)
    {
      DjaText[] texts=getTextArray();
      int y2=texts[1].getLocation().y-4;
      int y3=texts[2].getLocation().y-4;

      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawRect(_g,x,y,w-1,h-1,bp);
      DjaGraphics.drawLine(_g,x,y2,x+w-1,y2,bp);
      DjaGraphics.drawLine(_g,x,y3,x+w-1,y3,bp);
    }
  }

  private Dimension optimalSize()
  {
    DjaText[] texts=getTextArray();
    int w=0;
    int h=0;
    for(int i=0;i<3;i++)
    {
      Dimension d=texts[i].getSize();
      w=Math.max(w,d.width);
      h+=d.height;
    }
    w=Math.max(49,w);
    return new Dimension(w+2,h+20);
  }
}
