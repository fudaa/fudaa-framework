/**
 * @modification $Date: 2007-05-04 13:42:00 $
 * @statut       unstable
 * @file         DjaConvertDia.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.mst.MstHandlerBase;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class DjaConvertDia
       extends MstHandlerBase
       implements /*MstXmlHandler, */DjaOptions
{
  private static final int M2P(double x)
    { return (int)(x*20.); }

  private DjaVector          vector_   =new DjaVector();

  private DjaObject          object_   ;	
  private Hashtable          attr_     =new Hashtable();
  private String             atname_   ;
  private Object             atval_    ;
  private int                align_    =-1;

  private Properties options_=new Properties();
  private String     data_   ="";
  private String     file_   ="";

  public DjaConvertDia(String _file)
  {
    file_=_file;
  }

  public DjaVector getVector()
  {
    return vector_;
  }

  public void trace(String _s)
  {
    System.err.println(_s);
  }

  @Override
  public void error
  (String message, String systemId, int line, int column)
       throws Exception
  {
    super.error(message,systemId,line,column);
  }

  @Override
  public void charData(char ch[], int start, int length) throws Exception
  {
    for(int i=0; i<length; i++)
      data_+=ch[start+i];
    // trace("Data: "+data_+"\n");
  }

  @Override
  public void startElement(String _element) throws Exception
  {
    String element=_element;
    // trace(_element);
    if(element.startsWith("dia:")) element=element.substring(4);

    if("object".equals(element))
    {
      String type=(String)options_.get("type");
      if(type==null) return;

      object_=null;
      if(type.equals("Standard - Box"       )) object_=new DjaBox();
      if(type.equals("Standard - Ellipse"   )) object_=new DjaEllipse();
      if(type.equals("Standard - Text"      )) object_=new DjaLabel();
      if(type.equals("Standard - Image"     )) object_=new DjaImage();

      if(type.equals("Standard - Line"      )) object_=new DjaDirectArrow();
      if(type.equals("Standard - Arc"       )) object_=new DjaArcArrow();
      if(type.equals("Standard - ZigZagLine")) object_=new DjaZigZagArrow();
      if(type.equals("Standard - BezierLine")) object_=new DjaBezierArrow();

      if(object_ instanceof DjaLink) ((DjaLink)object_).setEndType(0);

      if(type.equals("UML - Class"       )) object_=new DjaUmlClass();
      if(type.equals("UML - Note"        )) object_=new DjaUmlNote();
      if(type.equals("UML - SmallPackage")) object_=new DjaUmlSmallPackage();
      if(type.equals("UML - LargePackage")) object_=new DjaUmlLargePackage();
      if(type.equals("UML - Actor"       )) object_=new DjaUmlActor();
      if(type.equals("UML - Usecase"     )) object_=new DjaUmlUsecase();
      if(type.equals("UML - Lifeline"    )) object_=new DjaUmlLifeline();
      if(type.equals("UML - Object"      )) object_=new DjaUmlObject();
      if(type.equals("UML - Component"   )) object_=new DjaUmlComponent();
      if(type.equals("UML - Node"        )) object_=new DjaUmlNode();
      if(type.equals("UML - Classicon"   )) object_=new DjaUmlClassIcon();
      if(type.equals("UML - State"       )) object_=new DjaUmlState();
      if(type.equals("UML - Branch"      )) object_=new DjaUmlBranch();

      if(type.equals("UML - Dependency"))
      {
	object_=new DjaZigZagArrow();
	((DjaLink)object_).setEndType(4);
	object_.putProperty("trace","10.10");
      }

      if(type.equals("UML - Realizes"))
      {
	object_=new DjaZigZagArrow();
	((DjaLink)object_).setBeginType(3);
	((DjaLink)object_).setEndType(0);
	object_.putProperty("trace","10.10");
      }

      if(type.equals("UML - Generalization"))
      {
	object_=new DjaZigZagArrow();
	((DjaLink)object_).setBeginType(3);
	((DjaLink)object_).setEndType(0);
      }

      if(type.equals("UML - Association"))
      {
	object_=new DjaZigZagArrow();
	((DjaLink)object_).setEndType(0);
      }

      if(type.equals("UML - Implements"))
      {
	object_=new DjaDirectArrow();
	((DjaLink)object_).setEndType(15);
      }

      if(type.equals("UML - Constraint"))
      {
	object_=new DjaDirectArrow();
	((DjaLink)object_).setEndType(4);
	object_.putProperty("trace","10.10");
      }

      if(type.equals("UML - Message"))
      {
	object_=new DjaDirectArrow();
	((DjaLink)object_).setEndType(5);
      }

      if(object_!=null)
      {
	object_.putProperty("epaisseur","2");
	vector_.addElement(object_);
      }
      attr_.clear();
    }

    if("attribute".equals(element))
      atname_=(String)options_.get("name");

    if("composite".equals(element))
    {
      String s=(String)options_.get("type");
      Hashtable t=new Hashtable();
      t.put("parent",attr_);
      if(s!=null) t.put("composite_type",s);
      attr_=t;
      atval_=t;
    }

    if("boolean".equals(element))
    {
      String s=(String)options_.get("val");
      atval_=Boolean.valueOf("true".equals(s));
    }

    if("int".equals(element))
    {
      String s=(String)options_.get("val");
      atval_=new Integer(s);
    }

    if("enum".equals(element))
    {
      String s=(String)options_.get("val");
      Integer i=new Integer(s);

      if(atval_==null)
	atval_=i;
      else
      if(atval_ instanceof Integer)
      {
	Vector v=new Vector(2);
	v.addElement(atval_);
	v.addElement(i);
      }
      else
      if(atval_ instanceof Vector)
	((Vector)atval_).addElement(i);
    }

    if("real".equals(element))
    {
      String s=(String)options_.get("val");
      atval_=new Double(s);
    }

    if("font".equals(element))
    {
      String  s=(String)options_.get("name");
      String  p="Serif";
      int     b=Font.PLAIN;
      int     t=12;
      if(object_!=null) t=object_.getFont().getSize();
      if(s.indexOf("Helvetica")>=0) p="SansSerif";
      if(s.indexOf("Courier"  )>=0) p="Monospaced";
      if(s.indexOf("Bold"     )>=0) b|=Font.BOLD;
      if(s.indexOf("Italic"   )>=0) b|=Font.ITALIC;
      atval_=new Font(p,b,t);
      // System.out.println("F="+atval_);
    }

    if("color".equals(element))
    {
      String s=(String)options_.get("val");
      atval_=new Color((int)Long.parseLong(s.substring(1),16));
    }

    if("point".equals(element))
    {
      String s=(String)options_.get("val");
      int i=s.indexOf(',');
      double x=new Double(s.substring(0,i)).doubleValue();
      s=s.substring(i+1);
      double y=new Double(s).doubleValue();
      Point  p=new Point(M2P(x),M2P(y));

      if(atval_==null)
	atval_=p;
      else
      if(atval_ instanceof Point)
      {
	Vector v=new Vector(2);
	v.addElement(atval_);
	v.addElement(p);
	atval_=v;
      }
      else
      if(atval_ instanceof Vector)
	((Vector)atval_).addElement(p);
    }

    if("rectangle".equals(element))
    {
      String s=(String)options_.get("val");
      int i=s.indexOf(',');
      double x=new Double(s.substring(0,i)).doubleValue();
      s=s.substring(i+1); i=s.indexOf(';');
      double y=new Double(s.substring(0,i)).doubleValue();
      s=s.substring(i+1); i=s.indexOf(',');
      double w=new Double(s.substring(0,i)).doubleValue()-x;
      s=s.substring(i+1);
      double h=new Double(s).doubleValue()-y;
      atval_=new Rectangle(M2P(x),M2P(y),
			   M2P(w),M2P(h));
    }
  }
    
  @Override
  public void endElement(String _element) throws Exception
  {
    // trace("/"+_element);
    String element=_element;
    if(element.startsWith("dia:")) element=element.substring(4);

    if("string".equals(element))
    {
      String s=data_.trim();
      if(s.startsWith("#")) s=s.substring(1);
      if(s.endsWith  ("#")) s=s.substring(0,s.length()-1);
      atval_=s; // .substring(1,s.length()-2);
    }

    if("attribute".equals(element))
    {
      if((atname_!=null)&&(atval_!=null))
        attr_.put(atname_,atval_);
      atname_=null;
      atval_=null;
    }

    if("composite".equals(element))
    {
      Hashtable t=attr_;
      attr_=(Hashtable)attr_.get("parent");

      if(object_!=null)
      {
	if(t.get("name")!=null)
	{
	  String s=(String)t.get("name");
	  if(t.get("value")==null) s+="()";
	  s+=": "+(String)t.get("composite_type");
	  if((t.get("value")!=null)&&(!"".equals(t.get("value"))))
	    s+=" = "+(String)t.get("value");

	  Integer v=(Integer)t.get("visibility");
	       if(v==null)         s=" "+s;
	  else if(v.intValue()==0) s="+"+s;
	  else if(v.intValue()==1) s="-"+s;
	  else if(v.intValue()==2) s="#"+s;
	  else if(v.intValue()==3) s=" "+s;

	  if(t.get("value")==null)
	  {
	    String p=object_.getProperty("methodes");
	    if(p==null) p=s; else p+="\n"+s;
	    object_.putProperty("methodes",p);
	    // System.err.println("operation: "+s);
	  }
	  else
	  {
	    String p=object_.getProperty("attributs");
	    if(p==null) p=s; else p+="\n"+s;
	    object_.putProperty("attributs",p);
	    // System.err.println("attribute: "+s);
	  }
	}
	else
	if(t.get("string")!=null)
	{
	  String s=(String)t.get("string");
	  if((object_ instanceof DjaUmlObject)&&(object_.getText(0)!=null))
	    ((DjaUmlObject)object_).setText(s,3);//.putProperty("attributs",s);
	  else
	    object_.setText(s,0);
	}

	if(t.get("height")!=null)
	{
	  Double d=(Double)t.get("height");
	  Font   f=object_.getFont();
	  object_.setFont(new Font(f.getFamily(),
				   f.getStyle(),
				   M2P(d.doubleValue()*0.9)));
	}

	if(t.get("font")!=null)
	{
	  Font f=(Font)t.get("font");
	  object_.setFont(new Font(f.getFamily(),
				   f.getStyle(),
				   object_.getFont().getSize()));
	}

	if(t.get("color")!=null)
	{
	  Color c=(Color)t.get("color");
	  object_.setColor(c);
	}

	if(t.get("alignment")!=null)
	  align_=((Integer)t.get("alignment")).intValue();
      }
    }

    if("object".equals(element))
    {
      if(object_!=null)
      {
	if(attr_.get("file")!=null)
	{
	  String s=(String)attr_.get("file");
	  if(object_ instanceof DjaImage)
	  {
	    if(!s.startsWith("/"))
	      s=new File(new File(file_).getParent(),s).getAbsolutePath();
	    // System.out.println("S="+s);
	    ((DjaImage)object_).setImage(new ImageIcon(s).getImage());
	  }
	}

	if(attr_.get("curve_distance")!=null)
	{
	  Double d=(Double)attr_.get("curve_distance");
	  object_.putProperty("courbure",""+M2P(d.doubleValue()));
	}

	if(attr_.get("border_width")!=null)
	{
	  Double d=(Double)attr_.get("border_width");
	  object_.putProperty("epaisseur",""+M2P(d.doubleValue()));
	}

	if(attr_.get("line_width")!=null)
	{
	  Double d=(Double)attr_.get("line_width");
	  object_.putProperty("epaisseur",""+M2P(d.doubleValue()));
	}

	if(attr_.get("line_style")!=null)
	{
	  int    i=((Integer)attr_.get("line_style")).intValue();
	  String t=null;
	  if(i==1) t=""+M2P(1  )+"."+M2P(0.9); // _ _ _ _ _
	  if(i==2) t=""+M2P(0.7)+"."+M2P(0.5); // _ . _ . _
	  if(i==3) t=""+M2P(0.7)+"."+M2P(0.5); // _ . . _ .
	  if(i==4) t=""+M2P(0.1)+"."+M2P(0.2); // .........
	  if(t!=null) object_.putProperty("trace",t);
	}

	/*
	if(attr_.get("height")!=null)
	{
	  Double d=(Double)attr_.get("height");
	  Font   f=object_.getFont();
	  object_.setFont(new Font(f.getFamily(),
				   f.getStyle(),
				   M2P(d.doubleValue()*0.9)));
	}

	if(attr_.get("font")!=null)
	{
	  Font f=(Font)attr_.get("font");
	  object_.setFont(new Font(f.getFamily(),
				   f.getStyle(),
				   object_.getFont().getSize()));
	}

	if(attr_.get("string")!=null)
	{
	  String s=(String)attr_.get("string");
	  object_.setText(s);
	}

	if(attr_.get("color")!=null)
	{
	  Color c=(Color)attr_.get("color");
	  object_.setTextColor(c);
	}
	*/

	if(attr_.get("obj_pos")!=null)
	{
	  Point p=(Point)attr_.get("obj_pos");
	  if(object_ instanceof DjaLabel)
	  {
	    p.x-=3;
	    p.y-=3+object_.getFont().getSize();
	  }
	  object_.setX(p.x);
	  object_.setY(p.y);
	}

	if(attr_.get("conn_endpoints")!=null)
	{
	  if(  (object_ instanceof DjaLink)
	     &&(attr_.get("conn_endpoints") instanceof Vector))
	    ((DjaLink)object_).setPoints
	      ((Vector)attr_.get("conn_endpoints"));
	}

	if(attr_.get("bez_points")!=null)
	{
	  if(  (object_ instanceof DjaBezierArrow)
	     &&(attr_.get("bez_points") instanceof Vector))
	    ((DjaLink)object_).setPoints
	      ((Vector)attr_.get("bez_points"));
	}

	if(attr_.get("orth_points")!=null)
	{
	  if(  (object_ instanceof DjaZigZagArrow)
	     &&(attr_.get("orth_points") instanceof Vector))
	    ((DjaZigZagArrow)object_).setPoints
	      ((Vector)attr_.get("orth_points"));
	}

	if(attr_.get("elem_corner")!=null)
	{
	  Point p=(Point)attr_.get("elem_corner");
	  object_.setX(p.x);
	  object_.setY(p.y);
	}

	if(attr_.get("elem_width")!=null)
	{
	  Double d=(Double)attr_.get("elem_width");
	  object_.setWidth(M2P(d.doubleValue()));
	}

	if(attr_.get("elem_height")!=null)
	{
	  Double d=(Double)attr_.get("elem_height");
	  object_.setHeight(M2P(d.doubleValue()));
	}

	if(align_>=0)
	{
	  int w=object_.getWidth()-8;
	  if(align_==1) object_.setX(object_.getX()-w/2);
	  if(align_==2) object_.setX(object_.getX()-w  );
	  align_=-1;
	}

	if(attr_.get("corner_radius")!=null)
	{
	  Double d=(Double)attr_.get("corner_radius");
	  object_.putProperty("rayon",""+M2P(d.doubleValue()));
	}
	
	if(attr_.get("inner_color")!=null)
	{
	  Color c=(Color)attr_.get("inner_color");
	  object_.setBackground(c);
	}

	if(attr_.get("border_color")!=null)
	{
	  Color c=(Color)attr_.get("border_color");
	  object_.setForeground(c);
	}

	if(attr_.get("start_arrow")!=null)
	{
	  int a=((Integer)attr_.get("start_arrow")).intValue();

	       if(a==0) a=0;
	  else if(a==1) a=4;
	  else if(a==2) a=6;
	  else if(a==3) a=5;
	  else if(a==4) a=7;
	  else if(a==5) a=8;
	  else if(a==6) a=16;  // --\
	  else if(a==7) a=18;  // --X 
          else if(a==8) a=14;
          else if(a==9) a=13;
	  else a=0;
	       
	  if(object_ instanceof DjaLink)
	    ((DjaLink)object_).setBeginType(a);
	}

	if(attr_.get("end_arrow")!=null)
	{
	  int a=((Integer)attr_.get("end_arrow")).intValue();

	       if(a==0) a=0;
	  else if(a==1) a=4;
	  else if(a==2) a=6;
	  else if(a==3) a=5;
	  else if(a==4) a=7;
	  else if(a==5) a=8;
	  else if(a==6) a=16;  // --\
	  else if(a==7) a=18;  // --X 
          else if(a==8) a=14;
          else if(a==9) a=13;
	  else a=0;
	       
	  if(object_ instanceof DjaLink)
	    ((DjaLink)object_).setEndType(a);
	}

	if(attr_.get("aggregate")!=null)
	{
	  int a=((Integer)attr_.get("aggregate")).intValue();
	  if(object_ instanceof DjaLink)
	  {
	         if(a==1) a=9;
	    else if(a==2) a=8;
	    else a=0;
	    ((DjaLink)object_).setEndType(a);
	  }
	}

	if(attr_.get("draw_border")!=null)
	{
	  Boolean b=(Boolean)attr_.get("draw_border");
	  if(!b.booleanValue())
	    object_.setForeground(null);
	  else
	  if(object_.getForeground()==null)
	    object_.setForeground(Color.black);
	}

	if(attr_.get("show_background")!=null)
	{
	  Boolean b=(Boolean)attr_.get("show_background");
	  if(!b.booleanValue())
	    object_.setBackground(null);
	  else
	  if(object_.getBackground()==null)
	    object_.setBackground(Color.white);
	}

	if(attr_.get("name")!=null)
	{
	  if(  (object_ instanceof DjaUmlClass)
             ||(object_ instanceof DjaUmlSmallPackage)
	     ||(object_ instanceof DjaUmlLargePackage))
	  object_.setText((String)attr_.get("name"),0);
	}

	if(attr_.get("template")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("template"));
	  if(object_ instanceof DjaUmlClass)
	    object_.putProperty("moule",b ? "vrai" : "faux");
        }

	if(attr_.get("visible_attributes")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("visible_attributes"));
	  if(object_ instanceof DjaUmlClass)
	    object_.putProperty("depliee",b ? "vrai" : "faux");
	}

	if(attr_.get("visible_operations")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("visible_operations"));
	  if(object_ instanceof DjaUmlClass)
	    object_.putProperty("depliee",b ? "vrai" : "faux");
	}

	if(attr_.get("rtop")!=null)
	{
	  double rtop=((Double)attr_.get("rtop")).doubleValue();
	  if(object_ instanceof DjaUmlLifeline)
	    object_.putProperty("debut",""+M2P(rtop));
	}

	if(attr_.get("rbot")!=null)
	{
	  double rbot=((Double)attr_.get("rbot")).doubleValue();
	  if(object_ instanceof DjaUmlLifeline)
	    object_.putProperty("fin",""+M2P(rbot));
	}

	if(attr_.get("draw_cross")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("draw_cross"));
	  if(object_ instanceof DjaUmlLifeline)
	    object_.putProperty("destruction",b ? "vrai" : "faux");
	}

	if(attr_.get("stereotype")!=null)
	{
	  if(object_ instanceof DjaUmlClassIcon)
	  {
	    int s=((Integer)attr_.get("stereotype")).intValue();
	    object_.putProperty("stereotype",""+s);
	    object_.setHeight(41);
	  }
	  if(object_ instanceof DjaUmlObject)
	  {
	    String s=(String)attr_.get("stereotype");
	    //object_.putProperty("stereotype",""+s.substring(1,s.length()-1));
	    //object_.setHeight(41);
	    ((DjaUmlObject)object_).setText(s.substring(1,s.length()-1),1);
	  }
	}

	if(attr_.get("exstate")!=null)
	{
	  String s=(String)attr_.get("exstate");
	  if(object_ instanceof DjaUmlObject)
	    //object_.putProperty("etat",s.substring(1,s.length()-1));
	    ((DjaUmlObject)object_).setText(s.substring(1,s.length()-1),2);
	}

	if(attr_.get("type")!=null)
	{
	  // System.err.println("TYPE="+attr_.get("type"));
	  int t=((Integer)attr_.get("type")).intValue();
	  if(object_ instanceof DjaUmlState)
	    object_.putProperty("type",""+t);
	}

	if(attr_.get("is_active")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("is_active"));
	  if(object_ instanceof DjaUmlObject)
	    object_.putProperty("actif",b ? "vrai" : "faux");
	}

	if(attr_.get("multiple")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("multiple"));
	  if(object_ instanceof DjaUmlObject)
	    object_.putProperty("multiple",b ? "vrai" : "faux");
	}

	if(attr_.get("textout")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("textout"));
	  if(b&&(object_ instanceof DjaUmlUsecase))
	  {
	    DjaText t0=object_.getTextArray()[0];
	    t0.setPosition(DjaOptions.SOUTH);
	    object_.setHeight(object_.getHeight()-t0.getSize().height);
          }
	}

	if(attr_.get("collaboration")!=null)
	{
	  boolean b=Boolean.TRUE.equals(attr_.get("collaboration"));
	  if(b&&(object_ instanceof DjaUmlUsecase))
	    object_.putProperty("trace","20.20");
	}

	/*
	if(object_ instanceof DjaUmlClass)
	{
	  String a=object_.getProperty("attributs");
	  String m=object_.getProperty("methodes");

	  int ho=object_.getHeight();
	  int hh=object_.getFont().getSize()+8;

	  object_.putProperty("depliee","faux");
	  object_.setHeight(0);

	  DjaObject ra=new DjaBox();
	  ra.putProperty("epaisseur",object_.getProperty("epaisseur"));
	  ra.putProperty("trace"    ,object_.getProperty("trace"    ));
	  grid_.add(ra);

	  DjaObject rm=new DjaBox();
	  rm.putProperty("epaisseur",object_.getProperty("epaisseur"));
	  rm.putProperty("trace"    ,object_.getProperty("trace"    ));
	  grid_.add(rm);

	  DjaObject na=new DjaLabel(a);
	  na.setFont(new Font("Serif",Font.PLAIN,12));
	  na.setX     (object_.getX());
	  na.setY     (object_.getY()+hh+1);
	  na.setWidth (object_.getWidth());
	  na.setHeight(0);
	  grid_.add(na);

	  ra.setX     (na.getX());
	  ra.setY     (na.getY()-1);
	  ra.setWidth (na.getWidth()+1);
	  ra.setHeight(na.getHeight());

	  DjaObject nm=new DjaLabel(m);
	  nm.setFont(new Font("Serif",Font.PLAIN,12));
	  nm.setX     (object_.getX());
	  nm.setY     (ra.getY()+ra.getHeight()-1);
	  nm.setWidth (object_.getWidth());
	  nm.setHeight(ho-hh-ra.getHeight());
	  grid_.add(nm);

	  rm.setX     (nm.getX());
	  rm.setY     (nm.getY()-1);
	  rm.setWidth (nm.getWidth()+1);
	  rm.setHeight(nm.getHeight());
	}
	*/
      }

      object_=null;    
    }

    data_="";
  }

  @Override
  public void attribute(String _attribut, String _valeur, boolean _specifie)
       throws Exception
  {
    if(_specifie)
      options_.put(_attribut,_valeur);
    // trace("  "+_attribut+"="+_valeur);
  }

  public static void main(String[] args)
  {
    for(int i=0;i<args.length;i++)
    {
      String file=args[i];

      try
      {
	DjaGrid   grid=new DjaGrid();
	DjaVector objs=new DjaLoadSaveDia().load(new File(file));

	grid.setObjects(objs);
	grid.recenter();

	JFrame f=new JFrame("Dja: "+file+" ("+i+")");
	f.getContentPane().add("Center",new JScrollPane(grid));
	f.pack();
	f.setSize(400,300);
	f.setLocation(200,100);
	f.setVisible(true);
	f.setLocation(200,100);
      }
      catch(Exception ex)
      {
	ex.printStackTrace();
	System.err.println(ex.getClass().getName()+": "+ex.getMessage());
      }
    }
  }  
}
