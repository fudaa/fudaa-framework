/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaPaletteForm.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuPanel;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.border.EmptyBorder;

public class DjaPaletteForm
       extends BuPanel
{
  BuButton[]   buttons_;
  BuGridLayout layout_;

  String[] names={ "Box",    "RoundBox", "Diamond", "Square",
		   "Circle", "Ellipse",  "HLine",   "VLine" };

  public DjaPaletteForm(ActionListener _al)
  {
    super();

    layout_=new BuGridLayout(4,2,2);
    layout_.setCfilled(false);
    setLayout(layout_);
    setBorder(new EmptyBorder(2,2,2,2));

    buttons_=new BuButton[names.length];

    for(int i=0; i<buttons_.length; i++)
    {
      BuIcon icon=DjaResource.DJA.getIcon("dja-"+names[i].toLowerCase(),16);

      buttons_[i]=new BuButton();
      buttons_[i].setIcon(icon);
      buttons_[i].setMargin(new Insets(1,1,1,1));
      buttons_[i].setRequestFocusEnabled(false);
      buttons_[i].setToolTipText( /*"Create a "+*/ names[i].toLowerCase());
      buttons_[i].setActionCommand("DJA_CREATE_FORM("+names[i]+")");

      add(buttons_[i]);
      buttons_[i].addActionListener(_al);
    }
  }
}
