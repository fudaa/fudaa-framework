/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaUmlClassIcon.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class DjaUmlClassIcon
       extends DjaForm
{
  public DjaUmlClassIcon()
  {
    this(null);
  }

  public DjaUmlClassIcon(String _text)
  {
    super(null);

    DjaText t0=new DjaText(this,0,_text,true,0,0,
			   SOUTH,0,MIDDLE,null,
			   null,false,false);
    setTextArray(new DjaText[] { t0 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (0);
    super.setHeight(0);

    putProperty("stereotype","0");

    textChanged(t0);
  }

  @Override
  public int getWidth()
  {
    int r=getHeight();
    if("1".equals(getProperty("stereotype")))
      r+=10;
    return r;
  }

  @Override
  public int getHeight()
  {
    return Math.max(21,super.getHeight());
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX()     -deltaX;
    int y=getY()     -deltaY;
    int w=getWidth() +2*deltaX;
    int h=getHeight()+2*deltaY;

    int ymax=y+h+getTextArray()[0].getSize().height-5;

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,EAST ,x+w-1,y+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w/2,ymax);
    r[6]=new DjaAnchor(this,6,WEST ,x    ,y+h/2);

    r[1]=new DjaAnchor(this,1,NORTH,x+w-1-268*w/2000,y+h/8);
    r[3]=new DjaAnchor(this,3,SOUTH,x+w-1           ,ymax);
    r[5]=new DjaAnchor(this,5,SOUTH,x               ,ymax);
    r[7]=new DjaAnchor(this,7,NORTH,x+    268*w/2000,y+h/8);

    if("1".equals(getProperty("stereotype")))
      r[7]=new DjaAnchor(this,7,NORTH,x,y+h/8);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    int ymax=y+h+getTextArray()[0].getSize().height-5;

    DjaHandle[] r=new DjaHandle[1];
    r[0]=new DjaHandle(this,SOUTH_EAST,x+w-1+2*deltaX,ymax); //y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    Color fg=getForeground();
    Color bg=getBackground();

    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillOval(_g,x+w-h,y,h,h);
    }

    if(fg!=null)
    {
      DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawOval(_g,x+w-h,y,h,h,bp);

      int t=Integer.parseInt(getProperty("stereotype"));
      switch(t)
      {
      case 1:
	DjaGraphics.drawLine(_g,x,y,x,y+h+1,bp);
	DjaGraphics.drawLine(_g,x,y+h/2,x+w-h,y+h/2,bp);
	break;
      case 2:
	DjaGraphics.drawLine(_g,x+w-h,y+h,x+w+1,y+h,bp);
	break;
      default:
	int a=x+w-h/2;
	DjaGraphics.drawLine(_g,a,y-5,a-5,y,bp);
	DjaGraphics.drawLine(_g,a,y+5,a-5,y,bp);
	break;
      }
    }

    super.paintObject(_g);
  }
}
