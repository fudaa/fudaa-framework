/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaCoder.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DjaCoder
{
  public static String encode(String s)
  {
    try {
      return URLEncoder.encode(s,FuLib.DEFAULT_ENCODING);
    } catch (UnsupportedEncodingException _evt) {
      FuLog.error(_evt);
      
    }
    return null;
  }

  public static String decode(String s)
  {
    String r=null;
    try { r=internal_decode(s); }
    catch(Exception ex) { }
    return r;
  }

  public static String internal_decode(String s) throws Exception {
    StringBuffer sb = new StringBuffer();
    for(int i=0; i<s.length(); i++) {
      char c = s.charAt(i);
      switch (c) {
      case '+':
	sb.append(' ');
	break;
      case '%':
	try {
	  sb.append((char)Integer.parseInt
		    (s.substring(i+1,i+3),16));
	}
	catch (NumberFormatException e) {
	  throw new IllegalArgumentException();
	}
	i += 2;
	break;
      default:
	sb.append(c);
	break;
      }
    }
    // Undo conversion to external encoding
    String result = sb.toString();
    byte[] inputBytes = result.getBytes("8859_1");
    return new String(inputBytes);
  }
}
