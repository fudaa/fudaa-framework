/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaRegistry.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.util.Enumeration;
import java.util.Hashtable;

public class DjaRegistry
{
  private static Hashtable keys_;
  private static Hashtable classes_;

  static
  {
    keys_   =new Hashtable();
    classes_=new Hashtable();
    System.getProperties().put("DjaRegistry",DjaRegistry.class);

    // System.err.println("REGISTER DJA OBJECTS");
    register("capsule"         ,DjaEmbed           .class);
    register("carre"           ,DjaSquare          .class);
    register("cercle"          ,DjaCircle          .class);
    register("ellipse"         ,DjaEllipse         .class);
    register("etiquette"       ,DjaLabel           .class);
    register("icone"           ,DjaIcon            .class);
    register("image"           ,DjaImage           .class);
    register("losange"         ,DjaDiamond         .class);
    register("pave"            ,DjaRoundBox        .class);
    register("point"           ,DjaPoint           .class);
    register("rectangle"       ,DjaBox             .class);

    register("fleche_arc"      ,DjaArcArrow        .class);
    register("fleche_bezier"   ,DjaBezierArrow     .class);
    register("fleche_brisee"   ,DjaBrokenArrow     .class);
    register("fleche_directe"  ,DjaDirectArrow     .class);
    register("fleche_zigzag"   ,DjaZigZagArrow     .class);

    register("uml-actor"       ,DjaUmlActor        .class);
    register("uml-branch"      ,DjaUmlBranch       .class);
    register("uml-class"       ,DjaUmlClass        .class);
    register("uml-classicon"   ,DjaUmlClassIcon    .class);
    register("uml-component"   ,DjaUmlComponent    .class);
    register("uml-join"        ,DjaUmlJoin         .class);
    register("uml-largepackage",DjaUmlLargePackage .class);
    register("uml-lifeline"    ,DjaUmlLifeline     .class);
    register("uml-node"        ,DjaUmlNode         .class);
    register("uml-note"        ,DjaUmlNote         .class);
    register("uml-object"      ,DjaUmlObject       .class);
    register("uml-smallpackage",DjaUmlSmallPackage .class);
    register("uml-state"       ,DjaUmlState        .class);
    register("uml-usecase"     ,DjaUmlUsecase      .class);
  }

  public static void init() { }

  @Override
  protected void finalize()
  {
    System.err.println("$$$ finalize registry");
  }

  public static synchronized void register(String _k, Class _c)
  {
    keys_   .put(_c,_k);
    classes_.put(_k,_c);
    // System.err.println("$$$ register "+_k);
  }

  public static synchronized void register(String _k, String _n)
  {
    if(getClass(_k)==null)
    {
      try
      {
	Class c=Class.forName(_n);
	keys_   .put(c,_k);
	classes_.put(_k,c);
	// System.err.println("$$$ register "+_k+": "+c);
      }
      catch(Exception ex)
      {
	// System.err.println("$$$ register: can not find "+_n);
      }
    }
  }

  public static synchronized Enumeration enumerate()
    { return keys_.elements(); }

  public static synchronized String getKeyword(DjaObject _o)
    { return (String)keys_.get(_o.getClass()); }

  public static synchronized Class getClass(String _k)
    { return (Class)classes_.get(_k); }

  public static synchronized DjaObject getInstance(String _k)
  {
    DjaObject r=null;
    Class c=getClass(_k);

    if(c==null)
    {
      System.err.println("### Not registred: "+_k);
    }
    else
    {
      try { r=(DjaObject)c.newInstance(); }
      catch(NoSuchMethodError ex1)
	{ System.err.println("### No default constructor for "+c.getName()); }
      catch(Exception ex2)
	{ System.err.println("### Can not instantiate "+c.getName()); }
    }
    return r;
  }
}
