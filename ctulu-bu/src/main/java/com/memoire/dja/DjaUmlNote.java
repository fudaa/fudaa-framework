/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaUmlNote.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

public class DjaUmlNote
       extends DjaForm
{
  private transient Dimension size_;

  public DjaUmlNote()
  {
    this(null);
  }

  public DjaUmlNote(String _text)
  {
    super(null);

    DjaText t=new DjaText(this,0,_text,true,2,2,
			  RELATIVE_NW,0,LEFT,null,null,false,false);
    setTextArray(new DjaText[] { t });

    super.setWidth (0);
    super.setHeight(0);
    setFont(new Font("Courier",Font.PLAIN,14));
    textChanged(t);
  }

  protected void resetSize()
  {
    size_=optimalSize();
  }

  @Override
  public int getWidth()
  {
    if(size_==null) resetSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) resetSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    resetSize();
  }

  @Override
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();
    r[0].setW(getWidth()-18);
    return r;
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x,y);
    r[1]=new DjaAnchor(this,1,NORTH,x+w/2,y);
    r[2]=new DjaAnchor(this,2,NORTH,x+w-7,y+7);
    r[3]=new DjaAnchor(this,3,EAST ,x+w,y+7+h/2);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w,y+h);
    r[5]=new DjaAnchor(this,5,SOUTH,x+w/2,y+h);
    r[6]=new DjaAnchor(this,6,SOUTH,x,y+h);
    r[7]=new DjaAnchor(this,7,WEST ,x,y+h/2);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[8];
    r[0]=new DjaHandle(this,NORTH     ,x+w/2       ,y    -deltaY);
    r[2]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[4]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);
    r[6]=new DjaHandle(this,WEST      ,x    -deltaX,y+h/2       );
    r[1]=new DjaHandle(this,NORTH_EAST,x+w-1+deltaX,y    -deltaY);
    r[3]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[5]=new DjaHandle(this,SOUTH_WEST,x    -deltaX,y+h-1+deltaY);
    r[7]=new DjaHandle(this,NORTH_WEST,x    -deltaX,y    -deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    int[] xp=new int[] { x,x+w-14,x+w ,x+w,x };
    int[] yp=new int[] { y,y     ,y+14,y+h,y+h };

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    Color bg=getBackground();
    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillPolygon(_g,xp,yp,5);
    }

    Color fg=getForeground();
    if(fg!=null)
    {
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawPolygon(_g,xp,yp,5,bp);
      DjaGraphics.drawLine(_g,x+w-14,y,x+w-14,y+14,bp);
      DjaGraphics.drawLine(_g,x+w-14,y+14,x+w,y+14,bp);
    }
  }

  private Dimension optimalSize()
  {
    Dimension d=getTextArray()[0].optimalSize();
    int w=Math.max(29,d.width+14);
    int h=d.height;
    return new Dimension(w+2,h+4);
  }
}

