/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaUmlObject.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class DjaUmlObject
       extends DjaBox
{
  public DjaUmlObject()
  {
    this(null);
  }

  public DjaUmlObject(String _text)
  {
    super(null);

    DjaText t0=new DjaText(this,0,null,false,2,0,
			   RELATIVE_NW,0,MIDDLE,Color.black,
			   null,true ,false);
    DjaText t1=new DjaText(this,1,null,false,2,0,
			   RELATIVE_NW,0,MIDDLE,Color.black,
			   null,false,false);
    DjaText t2=new DjaText(this,2,null,false,2,0,
			   RELATIVE_NW,0,MIDDLE,Color.black,
			   null,false,false);
    DjaText t3=new DjaText(this,3,null,true ,2,0,
			   RELATIVE_NW,0,LEFT  ,Color.black,
			   null,false,false);

    t1.setBefore("<<"); t1.setAfter(">>");
    t2.setBefore("[" ); t2.setAfter("]");
    setTextArray(new DjaText[] { t0,t1,t2,t3 });
  }

  @Override
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();

    r[1].setY(2);
    r[0].setY(r[1].getY()+r[1].getSize().height+2);
    r[2].setY(r[0].getY()+r[0].getSize().height+2);
    r[3].setY(r[2].getY()+r[2].getSize().height+8);

    int w=super.getWidth()-4;
    r[0].setW(w);
    r[1].setW(w);
    r[2].setW(w);
    r[3].setW(w);

    int h=super.getHeight()-r[3].getY();
    r[3].setH(h);

    return r;
  }

  @Override
  public void addText(String _text)
  {
    DjaText t=new DjaText(this,-1,_text);
    t.setPosition(RELATIVE_SW);
    addText(t);
  }

  @Override
  public int getWidth()
  {
    return Math.max(optimalSize().width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    return Math.max(optimalSize().height,super.getHeight());
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaText t0=getTexts()[0];
    int y0=y+t0.getY()+t0.getSize().height/2;

    DjaAnchor[] r=new DjaAnchor[8];
    r[0]=new DjaAnchor(this,0,NORTH,x+w/2,y);
    r[1]=new DjaAnchor(this,1,NORTH,x+3*w/4,y);
    r[2]=new DjaAnchor(this,2,EAST ,x+w-1,y0);
    r[3]=new DjaAnchor(this,3,SOUTH,x+3*w/4,y+h-1);
    r[4]=new DjaAnchor(this,4,SOUTH,x+w/2,y+h-1);
    r[5]=new DjaAnchor(this,5,SOUTH,x+w/4,y+h-1);
    r[6]=new DjaAnchor(this,6,WEST ,x    ,y0);
    r[7]=new DjaAnchor(this,7,NORTH,x+w/4,y);

    return r;
  }


  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[3];
    r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    if("vrai".equals(getProperty("multiple")))
      { y+=5; w-=5; h-=5; }

    Color bg=getBackground();
    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x,y,w,h);

      if("vrai".equals(getProperty("multiple")))
	DjaGraphics.fillRect(_g,x+5,y-5,w,h);
    }

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    Color fg=getForeground();
    if(fg!=null)
    {
      DjaText t3=getTexts()[3];
      int y3=y+t3.getY()-4;

      if("vrai".equals(getProperty("actif"))) bp.epaisseur_++;
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawRect(_g,x,y,w-1,h-1,bp);
      DjaGraphics.drawLine(_g,x,y3,x+w-1,y3,bp);

      if("vrai".equals(getProperty("multiple")))
      {
	DjaGraphics.drawLine(_g,x+5,y,x+5,y-5,bp);
	DjaGraphics.drawLine(_g,x+5,y-5,x+w+5,y-5,bp);
	DjaGraphics.drawLine(_g,x+w+5,y-5,x+w+5,y+h-5,bp);
	DjaGraphics.drawLine(_g,x+w+5,y+h-5,x+w,y+h-5,bp);
      }

      if("vrai".equals(getProperty("actif"))) bp.epaisseur_--;
    }
  }

  private Dimension optimalSize()
  {
    int w=0;
    int h=0;
    //Dimension r=new Dimension(0,0);

    DjaText[] t=getTexts();
    for(int i=0;i<4;i++)
    {
      Dimension d=t[i].getSize();
      w=Math.max(w,d.width);
      h+=d.height;
    }

    if("vrai".equals(getProperty("multiple")))
      { w+=5; h+=5; }

    w=Math.max(49,w);
    return new Dimension(w+2,h+21);
  }
}
