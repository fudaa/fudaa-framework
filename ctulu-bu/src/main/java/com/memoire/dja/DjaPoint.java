/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaPoint.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaPoint
       extends DjaForm
{
  public DjaPoint(String _texte)
  {
    super(_texte,SOUTH);
    super.setWidth (1);
    super.setHeight(1);
  }

  public DjaPoint()
  {
    this(null);
  }

  @Override
  public void setWidth(int _w)
  { }

  @Override
  public void setHeight(int _h)
  { }

  @Override
  public int getWidth()
  { return 1; }

  @Override
  public int getHeight()
  { return 1; }

  @Override
  public boolean contains(int _x, int _y)
  {
    Rectangle r=getBounds();
    r.grow(7,7);
    return r.contains(_x,_y);
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    DjaAnchor[] r=new DjaAnchor[1];
    r[0]=new DjaAnchor(this,0,NORTH,getX(),getY());
    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    DjaHandle[] r=new DjaHandle[0];
    return r;
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    int x=getX();
    int y=getY();

    Color bg=getBackground();
    if(bg!=null)
    {
      _g.setColor(bg);
      _g.fillOval(x-4,y-4,9,9);
    }

    Color fg=getForeground();
    if(fg!=null)
    {
      _g.setColor(fg);
      _g.drawOval(x-5,y-5,10,10);
    }

    super.paintInteractive(_g);
  }
}
