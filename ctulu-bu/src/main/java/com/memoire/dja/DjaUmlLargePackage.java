/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaUmlLargePackage.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaUmlLargePackage
       extends DjaForm
{
  private transient Dimension size_;

  public DjaUmlLargePackage()
  {
    this((String)null);
  }

  public DjaUmlLargePackage(String _text)
  {
    super(null);

    DjaText t0=new DjaText(this,0,_text,false,2,2,
			   RELATIVE_NW,0,LEFT,null,null,false,false);
    DjaText t1=new DjaText(this,1,null,true,0,4,
			   RELATIVE_TEXT_SW,0,LEFT,null,null,false,false);
    setTextArray(new DjaText[] { t0,t1 });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (0);
    super.setHeight(0);
    textChanged(t0);
    textChanged(t1);
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    Rectangle b=getBounds();
    boolean   r=b.contains(_x,_y);

    if(r)
    {
      Dimension d=getTextArray()[0].getSize();
      int hs=d.height+2;
      int ws=d.width+2;

      if(new Rectangle(b.x+ws,b.y,b.width-ws,hs).contains(_x,_y))
	r=false;
    }

    return r;
  }

  @Override
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();
    int       w=getWidth();
    int       h=getHeight();
    Dimension d0=r[0].getSize();
    Dimension d1=r[1].getSize();
    //r[0].setW(w-26);
    r[1].setW(w-4);
    r[1].setY((h-d0.height-d1.height-3)/2+2);
    return r;
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Dimension d=getTextArray()[0].getSize();
    int hs=d.height+2;
    int ws=d.width+2;

    DjaAnchor[] r=new DjaAnchor[7];
    r[0]=new DjaAnchor(this,0,NORTH,x+ws/2    ,y     );
    r[1]=new DjaAnchor(this,1,NORTH,x+(w+ws)/2,y+hs+2);
    r[2]=new DjaAnchor(this,2,SOUTH,x+w/4     ,y+h   );
    r[3]=new DjaAnchor(this,3,SOUTH,x+w/2     ,y+h   );
    r[4]=new DjaAnchor(this,4,SOUTH,x+3*w/4   ,y+h   );
    r[5]=new DjaAnchor(this,5,WEST,x  ,y+(h+hs)/2+1  );
    r[6]=new DjaAnchor(this,6,EAST,x+w,y+(h+hs)/2+1  );
    //r[2]=new DjaAnchor(this,2,NORTH,x+(w-10)  ,y+hs+8);

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[3];
    r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    Dimension d=getTextArray()[0].getSize();
    int hs=d.height+2;
    int ws=d.width+2;

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    Color bg=getBackground();
    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x,y,ws+3,hs+3);
    }

    Color fg=getForeground();
    if(fg!=null)
    {
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawLine(_g,x+ws+2,y+hs+2,x+ws+2,y,bp);
      DjaGraphics.drawLine(_g,x+ws+2,y,x,y,bp);
      DjaGraphics.drawLine(_g,x,y,x,y+hs+2,bp);
      DjaGraphics.drawRect(_g,x,y+hs+2,w,h-hs-2,bp);
    }

    super.paintObject(_g);
  }

  public Dimension optimalSize()
  {
    Dimension d0=getTextArray()[0].getSize();
    Dimension d1=getTextArray()[1].getSize();
    int w=Math.max(Math.max(d0.width,32)+20,d1.width);
    int h=d0.height+2+Math.max(30,d1.height);
    return new Dimension(w+4,h+4);
  }
}
