/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaUmlJoin.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;


public class DjaUmlJoin
       extends DjaBox
{
  public DjaUmlJoin()
  {
    super();
    setBackground(getForeground());
  }

  @Override
  public int getWidth()
  {
    return Math.max(4*getHeight()+1,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    return 5;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();

    DjaHandle[] r=new DjaHandle[1];
    r[0]=new DjaHandle(this,EAST,x+w-1+deltaX,y+3);

    return r;
  }
}
