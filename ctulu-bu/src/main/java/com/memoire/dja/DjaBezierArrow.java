/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaBezierArrow.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Vector;

public class DjaBezierArrow
       extends DjaLink
{
  private int xr0,yr0,xr1,yr1;
  
  public DjaBezierArrow(String _text)
  {
    super();

    xr0=xbegin_;
    yr0=ybegin_;
    xr1=xend_;
    yr1=yend_;

    putProperty("x2",""+(xbegin_+3*deltaX));
    putProperty("y2",""+ybegin_);
    putProperty("x3",""+(xend_-3*deltaX));
    putProperty("y3",""+yend_);

    if(_text!=null) addText(_text);
  }

  public DjaBezierArrow()
  {
    this(null);
  }

  @Override
  public void setX(int _x)
  {
    int dx=_x-getX();

    if(begin_==null)
    {
      xbegin_+=dx;
      int x2=Integer.parseInt(getProperty("x2"));
      putProperty("x2",""+(x2+dx));
    }

    if(end_==null)
    {
      xend_+=dx;
      int x3=Integer.parseInt(getProperty("x3"));
      putProperty("x3",""+(x3+dx));
    }

    updateXYO();
  }

  @Override
  public void setY(int _y)
  {
    int dy=_y-getY();

    if(begin_==null)
    {
      ybegin_+=dy;
      int y2=Integer.parseInt(getProperty("y2"));
      putProperty("y2",""+(y2+dy));
    }

    if(end_!=null)
    {
      yend_  +=dy;
      int y3=Integer.parseInt(getProperty("y3"));
      putProperty("y3",""+(y3+dy));
    }

    updateXYO();  
  }

  @Override
  public void setWidth(int _w)
  {
    int w=getWidth();
    if(w!=0)
    {
      int x=getX();
      xbegin_=x+(xbegin_-x)*_w/w;
      xend_  =x+(xend_  -x)*_w/w;

      /*
      int x2=Integer.parseInt(getProperty("x2"));
      int x3=Integer.parseInt(getProperty("x3"));
      putProperty("x2",""+(x+(x2-x)*_w/w));
      putProperty("x3",""+(x+(x3-x)*_w/w));
      */

      updateXYO();
    }
  }

  @Override
  public void setHeight(int _h)
  {
    int h=getHeight();
    if(h!=0)
    {
      int y=getY();
      ybegin_=y+(ybegin_-y)*_h/h;
      yend_  =y+(yend_  -y)*_h/h;

      /*
      int y2=Integer.parseInt(getProperty("y2"));
      int y3=Integer.parseInt(getProperty("y3"));
      putProperty("y2",""+(y+(y2-y)*_h/h));
      putProperty("y3",""+(y+(y3-y)*_h/h));
      */

      updateXYO();
    }
  }

  @Override
  public void setPoints(Vector _v)
  {
    super.setPoints(_v);

    // System.out.println("SET-POINTS: "+_v.size());

    if(_v.size()!=4) return;

    Point p2=(Point)_v.elementAt(1);
    Point p3=(Point)_v.elementAt(2);

    putProperty("x2",""+p2.x);
    putProperty("y2",""+p2.y);
    putProperty("x3",""+p3.x);
    putProperty("y3",""+p3.y);
  }

  private static class PC extends DjaControl
  {
      public PC(DjaObject _f,int _p,int _o,int _x,int _y)
      {
	super(_f,_p,_o,_x,_y);
      }

      @Override
      public void draggedTo(int _x, int _y)
      {
	  DjaObject p=getParent();
	  int n=2+getP();
	  p.putProperty("x"+n,""+_x);
	  p.putProperty("y"+n,""+_y);
      }
  }

  @Override
  public DjaControl[] getControls()
  {
    int x2=Integer.parseInt(getProperty("x2"));
    int y2=Integer.parseInt(getProperty("y2"));
    int x3=Integer.parseInt(getProperty("x3"));
    int y3=Integer.parseInt(getProperty("y3"));

    DjaControl[] r=new DjaControl[2];
    r[0]=new PC(this,0,BOTH,x2,y2);
    r[1]=new PC(this,1,BOTH,x3,y3);
    return r;
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    int x2=Integer.parseInt(getProperty("x2"));
    int y2=Integer.parseInt(getProperty("y2"));
    int x3=Integer.parseInt(getProperty("x3"));
    int y3=Integer.parseInt(getProperty("y3"));

    double c=Math.PI;
    int vx0=(int)((x2-xr0)*c);
    int vy0=(int)((y2-yr0)*c);
    int vx1=(int)((xr1-x3)*c);
    int vy1=(int)((yr1-y3)*c);

    Polygon p=DjaMatrixHermite.arc2polyline(xr0,yr0,xr1,yr1,vx0,vy0,vx1,vy1);
    boolean r=false;
    for(int i=0; i<p.npoints-1; i++)
      r|=DjaLib.close(p.xpoints[i],p.ypoints[i],p.xpoints[i+1],p.ypoints[i+1],_x,_y);

    return r;
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    DjaAnchor[] r=new DjaAnchor[1];
    Point       m=getMiddle();
    r[0]=new DjaAnchor(this,0,ANY,m.x,m.y);
    return r;
  }

  public Point getMiddle()
  {
    int x2=Integer.parseInt(getProperty("x2"));
    int y2=Integer.parseInt(getProperty("y2"));
    int x3=Integer.parseInt(getProperty("x3"));
    int y3=Integer.parseInt(getProperty("y3"));

    double c=Math.PI;
    int vx0=(int)((x2-xr0)*c);
    int vy0=(int)((y2-yr0)*c);
    int vx1=(int)((xr1-x3)*c);
    int vy1=(int)((yr1-y3)*c);

    Polygon p=DjaMatrixHermite.arc2polyline(xr0,yr0,xr1,yr1,vx0,vy0,vx1,vy1);

    return new Point(p.xpoints[p.npoints/2],p.ypoints[p.npoints/2]);
  }

  @Override
  public void paintObject(Graphics _g)
  {
    updateXYO();

    int x0=xbegin_;
    int y0=ybegin_;
    int o0=obegin_;
    int x1=xend_;
    int y1=yend_;
    int o1=oend_;

    //int x,y;

    xr0=x0;
    yr0=y0;
    xr1=x1;
    yr1=y1;

    if(tbegin_!=0)
    switch(o0)
    {
    case EAST:
      drawBracket(_g,tbegin_,o0,x0,y0-5,x0+10,y0+5);
      xr0+=11;
      break;
    case WEST:
      drawBracket(_g,tbegin_,o0,x0,y0-5,x0-10,y0+5);
      xr0-=11;
      break;
    case NORTH:
      drawBracket(_g,tbegin_,o0,x0-5,y0,x0+5,y0-10);
      yr0-=11;
      break;
    case SOUTH:
      drawBracket(_g,tbegin_,o0,x0-5,y0,x0+5,y0+10);
      yr0+=11;
      break;
    }
    
    if(tend_!=0)
    switch(o1)
    {
    case EAST:
      drawBracket(_g,tend_,o1,x1,y1-5,x1+10,y1+5);
      xr1+=11;
      break;
    case WEST:
      drawBracket(_g,tend_,o1,x1,y1-5,x1-10,y1+5);
      xr1-=11;
      break;
    case NORTH:
      drawBracket(_g,tend_,o1,x1-5,y1,x1+5,y1-10);
      yr1-=11;
      break;
    case SOUTH:
      drawBracket(_g,tend_,o1,x1-5,y1,x1+5,y1+10);
      yr1+=11;
      break;
    }

    if((tbegin_==1)||(tbegin_==4)) { xr0=xbegin_; yr0=ybegin_; }
    if((tend_  ==1)||(tend_  ==4)) { xr1=xend_;   yr1=yend_;   }

    _g.setColor(getForeground());
    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    int x2=Integer.parseInt(getProperty("x2"));
    int y2=Integer.parseInt(getProperty("y2"));
    int x3=Integer.parseInt(getProperty("x3"));
    int y3=Integer.parseInt(getProperty("y3"));

    double c=Math.PI;
    int vx0=(int)((x2-xr0)*c);
    int vy0=(int)((y2-yr0)*c);
    int vx1=(int)((xr1-x3)*c);
    int vy1=(int)((yr1-y3)*c);

    Polygon p=DjaMatrixHermite.arc2polyline(xr0,yr0,xr1,yr1,vx0,vy0,vx1,vy1);
    DjaGraphics.drawPolyline(_g,p,bp);

    super.paintObject(_g);
  }

  @Override
  public void paintInteractive(Graphics _g)
  {
    super.paintInteractive(_g);

    if(isSelected())
    {
      int x2=Integer.parseInt(getProperty("x2"));
      int y2=Integer.parseInt(getProperty("y2"));
      int x3=Integer.parseInt(getProperty("x3"));
      int y3=Integer.parseInt(getProperty("y3"));

      _g.setColor(controlsColor);
      DjaGraphics.BresenhamParams bp=new DjaGraphics.BresenhamParams(2,2);
      DjaGraphics.drawLine(_g,xbegin_,ybegin_,x2,y2,bp);
      DjaGraphics.drawLine(_g,x3,y3,xend_,yend_,bp);
    }
  }
}
