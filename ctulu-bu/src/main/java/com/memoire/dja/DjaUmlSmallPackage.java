/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         DjaUmlSmallPackage.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

public class DjaUmlSmallPackage
       extends DjaForm
{
  private transient Dimension size_;

  public DjaUmlSmallPackage()
  {
    this(null);
  }

  public DjaUmlSmallPackage(String _text)
  {
    super(_text);

    DjaText t=new DjaText(this,0,_text,false,2,17,
			   RELATIVE_NW,0,MIDDLE,null,null,false,false);
    setTextArray(new DjaText[] { t });

    super.setFont(new Font("Courier",Font.PLAIN,14));
    super.setWidth (0);
    super.setHeight(0);
    textChanged(t);
  }

  @Override
  public int getWidth()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.width,super.getWidth());
  }

  @Override
  public int getHeight()
  {
    if(size_==null) size_=optimalSize();
    return Math.max(size_.height,super.getHeight());
  }

  @Override
  public void textChanged(DjaText _text)
  {
    size_=optimalSize();
  }

  @Override
  public boolean contains(int _x, int _y)
  {
    Rectangle b=getBounds();
    boolean   r=b.contains(_x,_y);

    if(r)
    {
      if(new Rectangle(b.x+30,b.y,b.width-30,15).contains(_x,_y))
	r=false;
    }

    return r;
  }

  @Override
  public DjaText[] getTexts()
  {
    DjaText[] r=super.getTexts();
    Dimension d=r[0].getSize();
    r[0].setY((getHeight()-15-d.height)/2+16);
    r[0].setW(getWidth()-4);
    return r;
  }

  @Override
  public DjaAnchor[] getAnchors()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaAnchor[] r=new DjaAnchor[7];
    r[0]=new DjaAnchor(this,0,NORTH,x+15    ,y      );
    r[1]=new DjaAnchor(this,1,NORTH,x+w/2+15,y+15   );
    r[2]=new DjaAnchor(this,2,SOUTH,x+w/4   ,y+h    );
    r[3]=new DjaAnchor(this,3,SOUTH,x+w/2   ,y+h    );
    r[4]=new DjaAnchor(this,4,SOUTH,x+3*w/4 ,y+h    );
    r[5]=new DjaAnchor(this,5,WEST ,x       ,y+h/2+8);
    r[6]=new DjaAnchor(this,6,EAST ,x+w     ,y+h/2+8);
    //r[1.5]=new DjaAnchor(this,2,NORTH,x+(w-10),y+15   );

    return r;
  }

  @Override
  public DjaHandle[] getHandles()
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaHandle[] r=new DjaHandle[3];
    r[0]=new DjaHandle(this,EAST      ,x+w-1+deltaX,y+h/2       );
    r[1]=new DjaHandle(this,SOUTH_EAST,x+w-1+deltaX,y+h-1+deltaY);
    r[2]=new DjaHandle(this,SOUTH     ,x+w/2       ,y+h-1+deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g)
  {
    int x=getX();
    int y=getY();
    int w=getWidth();
    int h=getHeight();

    DjaGraphics.BresenhamParams bp=DjaGraphics.getBresenhamParams(this);

    Color bg=getBackground();
    if(bg!=null)
    {
      DjaGraphics.setColor(_g,bg);
      DjaGraphics.fillRect(_g,x,y,30,15);
      DjaGraphics.fillRect(_g,x,y+15,w,h-15);
    }

    Color fg=getForeground();
    if(fg!=null)
    {
      DjaGraphics.setColor(_g,fg);
      DjaGraphics.drawLine(_g,x+30,y+15,x+30,y,bp);
      DjaGraphics.drawLine(_g,x+30,y,x,y,bp);
      DjaGraphics.drawLine(_g,x,y,x,y+15,bp);
      DjaGraphics.drawRect(_g,x,y+15,w,h-15,bp);
    }

    super.paintObject(_g);
  }

  public Dimension optimalSize()
  {
    Dimension d=getTextArray()[0].getSize();
    int w=Math.max(42,d.width);
    int h=Math.max(16,d.height);
    return new Dimension(w+4,h+21);
  }
}
