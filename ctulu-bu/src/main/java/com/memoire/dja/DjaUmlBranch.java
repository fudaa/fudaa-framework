/**
 * @modification $Date: 2006-09-19 14:34:56 $
 * @statut       unstable
 * @file         DjaUmlBranch.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.dja;


public class DjaUmlBranch
       extends DjaDiamond
{
  public DjaUmlBranch()
  {
    super();
  }

  @Override
  public int getWidth()
    { return 21; }

  @Override
  public int getHeight()
    { return 21; }

  /*
  public String getText()
    { return null; }

  public void setText(String _t)
    { }
  */

  @Override
  public DjaHandle[] getHandles()
    { return new DjaHandle[0]; }

  /*
  public void paintText(Graphics _g)
    { }
  */
}
