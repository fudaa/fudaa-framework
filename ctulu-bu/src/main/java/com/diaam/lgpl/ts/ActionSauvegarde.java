/*
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 11:02:40  deniger
 * version initiale
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/25 12:28:22  Herve_AGNOUX
 * Initial revision
 *
 */
/*
-- TerminalStandard v111999

Fichier : TerminalStandard.java
Auteur : Herve AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/
package com.diaam.lgpl.ts;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Pour proposer a l'utilisateur une sauvegarde d'un document ShSh.
 * Pour l'utilisation, voir la doc de swing. Cette classe n'est pas 
 * tres elaboree, mais c'est pas grave. Cela viendra plus tard.
 * @author <a href="h_agnoux.html">Herve AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 */
public class ActionSauvegarde extends javax.swing.AbstractAction
{
    /**
     * Le fichier de sauvegarde par defaut est "log.txt".
     */
  public File fichierDeSauvegarde = new File("log.txt");

  private ShSh monShell;
  private Exception pasDeBol;

    /**
     * @param shell le ShSh dont il faudra sauvegarder le document.
     */
  public ActionSauvegarde(ShSh shell)
  {
    this(shell, "Sauvegarde");
  }

    /**
     * @param shell le ShSh dont il faudra sauvegarder le document.
     * @param name le nom de l'action que l'on veut voir a l'ecran.
     */
  public ActionSauvegarde(ShSh shell, String name)
  {
    super(name);
    monShell = shell;
  }

    /**
     * L'action se declenche ! Elle ne fait rien de mal : elle sauvegarde
     * le contenu du document dans un fichier texte, c'est tout.
     * @param ae RAS. (non utilise)
     */
  @Override
  public void actionPerformed(ActionEvent ae)
  {
    pasDeBol = null;
    try
      {
	FileWriter fw;

	fw = null;
	try
	  {
	    Document d;
	    String s;

	    d = monShell.getDocument();
	    s = d.getText(0, d.getLength());
	    fw = new FileWriter(fichierDeSauvegarde);
	    fw.write(s);
	  }
	finally
	  {
	    if (fw != null)
	      fw.close();
	  }
      }
    catch (IOException ioe)
      {
	pasDeBol = ioe;
      }
    catch (BadLocationException ble)
      {
	pasDeBol = ble;
      }
  }

    /**
     * En cas que actionPerformed se passe mal, l'appli pourra
     * trouver ici l'exception denonciatrice. (A mon avis �a ne sert a rien)
     * @return le bleme.
     */
  public Exception gargl()
  {
    return pasDeBol;
  }
}

