/*
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2006/03/09 10:14:00  deniger
 * Suppression des accents
 *
 * Revision 1.2  2005/08/16 12:56:56  deniger
 * correction noms de variables
 *
 * Revision 1.1  2003/01/29 11:02:40  deniger
 * version initiale
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/04 20:02:41  Herve_AGNOUX
 * Initial revision
 * License LGPL.
 * hgnoux@diaam.com
 */
/*
-- TerminalStandard v111999

Fichier : ShSh.java
Auteur : Herve AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/

package com.diaam.lgpl.ts;


import java.awt.AWTError;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.Writer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;

/**
 * <code>ShSh</code> est une classe qui a l'ambition de mettre n'importe
 * quel shell dans swing. <em>L'ambition</em> seulement... Ne me faites
 * pas dire ce que je n'ai pas dit... A cet effet, une interface generale
 * est a l'etude, visible dans les packages "script", "realscripts", et
 * "applications".<br>
 * ShSh constitue, avec les classes <code>MinReader</code> et
 * <code>SwingTextDocumentWriter</code>, le pivot du package.<br>
 * ShSh permet a l'utilisateur de faire... un copier/coller sur
 * les commandes ! N'est ce pas merveilleux !? Merci, SWING !
 * Pour la gestion interne, elle utilise la notion d'attribut. Cette
 * gestion est tres mal faite, mais elle fonctionne.<p>
 * Quelles sont les evolutions depuis la precedente version ? Pas grand
 * chose, vu de l'exterieur.
 * <ul>
 * <li> les methodes d'acces aux flux renvoient maintenant des MinReader et
 * autres SwingTextDocumentWriter, ceci pour acceder a leur methode
 * "asStream" pour quand on est obliger les flux d'octets. Avant, elles
 * renvoyaient simplement des Reader et des Writer.
 * <li> une grosse modif : au lieu de descendre d'un JTextArea, elle
 * descend maintenant d'un JTextPane. Cela permet d'utiliser la gestion
 * des attributs pour la cuisine interne, et d'esperer que dans de futures
 * versions cette cuisine soit mieux faite.
 * </ul>
 * Comment travaille cette classe ? Ce qu'elle re�oit du clavier, elle
 * le renvoit sur un MinReader ; et lorsque le document associe se modifie,
 * normalement par l'intermediaire d'un SwingTextDocumentWriter, �a
 * s'affiche automatiquement. Ce qui est moins automatique, c'est la gestion
 * du curseur dans ce cas. Voyez les sources pour constater les jongleries.
 * <p>Cette classe est en diffusion LGPL, et elle est telechargeable a
 * <a href="http://perso.club-internet.fr/hagnoux/java/TerminalStandard">
 * http://perso.club-internet.fr/hagnoux/java/TerminalStandard</a>.<br>
 * ... au fait ! Qu'est ce que <big>
 * <a href="http://www.diaam.com">diaam</a></big> ?
 * @author <a href="h_agnoux.html">Herve AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 * @see MinReader
 * @see SwingTextDocumentWriter
 */
public class ShSh extends javax.swing.JTextPane
{
  private MinReader tubeClavier;
  SwingTextDocumentWriter tubeSortieFenetre;
  private SimpleAttributeSet attrs;

  /**
   * Initialise les flux d'entree/sortie, et un listener pour le caret,
   * et un listener pour le clavier. Ecoute ce qui se passe sur le document,
   * et prepare les attributs caract�res de fa�on a pister ceux qui viennent
   * du clavier.
   */
  public ShSh()
    {
      this(null);
    }
  public ShSh(FileWriter _log)
    {
        tubeSortieFenetre = new SwingTextDocumentWriter(getDocument(), _log);
        tubeClavier = new MinReader();
 	addCaretListener(new DuCurseur());
	addKeyListener(new DuClavier());
  	getDocument().addDocumentListener(new DuDocument());
	attrs = new SimpleAttributeSet();
	attrs.addAttribute(this, "pas mal !");
	super.replaceSelection("\n");
    }

  /**
   * @return l'equivalent de l'entree standard.
   */
  public MinReader lecteur()
    {
        return tubeClavier;
    }

  /**
   * @return l'equivalent de la sortie standard.
   */
    public SwingTextDocumentWriter redacteur()
    {
        return tubeSortieFenetre;
    }

  /**
   * @return l'equivalent de la sortie erreur standard.
   */
    public SwingTextDocumentWriter gueulard()
    {
        return tubeSortieFenetre;
    }

  /**
   * En plus du comportement normal, elle envoit la cha�ne tapee au
   * clavier vers le lecteur (entree standard) a chaque retour chariot.
   * Normalement, encore une fois, tout ce qui vient du clavier passe par
   * ici.
   * @param content le contenu (-> le caract�re tape au clavier, ou le
   * "colle" si on a fait un "copie/colle").
   */
    @Override
    public void replaceSelection(String content)
    {
        try
        {
	  setCharacterAttributes(attrs, true);
        	super.replaceSelection(content);
		// on suppose abusivement que 'content' ne contient
		// qu'un caract�re...
		if (content.charAt(0) == '\n')
		{
			int s;
			int f;
			String str;
			int l;

			s = tubeSortieFenetre.offsetAtteint();
			f = getDocument().getLength();
			str = getDocument().getText(s, f - s);
			l = str.length();
			for (int i = 0; i < l;i++)
				tubeClavier.aj(str.charAt(i));
		}
	}
	catch (BadLocationException ble)
	{
		throw new AWTError(ble.toString());
	}
    }

  class DuCurseur implements CaretListener
  {
    @Override
    public void caretUpdate(CaretEvent e)
    {
      if (tubeSortieFenetre.offsetAtteint() <= e.getDot())
	{
	  if (!isEditable())
	    setEditable(true);
	}
      else
	{
	  if (isEditable())
	    setEditable(false);
	}
    }
  }

  class DuClavier extends KeyAdapter
  {
    @Override
    public void keyPressed(KeyEvent e)
    {
      if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
	{
	  if (tubeSortieFenetre.offsetAtteint() == getCaret().getDot())
	    {
		e.consume();
	    }
	}
    }
  }

    class DuDocument implements javax.swing.event.DocumentListener
    {
	@Override
  public void changedUpdate(DocumentEvent de)
	{
	}

	@Override
  public void insertUpdate(final DocumentEvent de)
	{
	    final Document d;
	    //Runnable r;

	    d = de.getDocument();
	    final int i1, i2;
	    final int l;
	    i1 = de.getOffset();
	    i2 = de.getLength();
	    l = d.getLength();
	    if (l == i1 + i2)
		{
		    Element e1 = d.getDefaultRootElement();
		    Element e2 = e1.getElement(e1.getElementIndex(i1));
		    while (!e2.isLeaf())
			e2 = e2.getElement(e2.getElementIndex(i1));
		    final Element e3 = e2;
		    AttributeSet as = e3.getAttributes();
		    if (as.isDefined(tubeSortieFenetre))
			if (getCaretPosition() != l)
			  {
				    setCaretPosition(l);
			  }
		}
	}

	@Override
  public void removeUpdate(DocumentEvent de)
	{
	}
    }

  /**
   * Juste pour le test...
   * @exception Exception R.A.S.
   */
    public static void main(String args[]) throws Exception
    {
		JFrame jf;
		JPanel jp;
		JScrollPane jsp;
		ShSh ss;
		Writer stdw;
        BufferedReader br;

		jf = new JFrame("ShSh");
		jp = new JPanel();
		jp.setLayout(new BorderLayout());
		jp.setPreferredSize(new Dimension(400, 250));
		ss = new ShSh();
		jsp = new JScrollPane(ss);
		jp.add("Center", jsp);
		jf.setContentPane(jp);
		jf.pack();
		jf.addWindowListener(new WindowAdapter()
				{
				@Override
        public void windowClosing(WindowEvent e)
				{System.exit(0);}
				});
		stdw = ss.redacteur();
		jf.setVisible(true);
		ss.requestFocus();
	        br = new BufferedReader(ss.lecteur());
	        stdw.write("GO !");
		while (true)
		{
	            String lu;

	            lu = br.readLine();
	            stdw.write(lu);
		}
	}
}
