/*
 * $Log: not supported by cvs2svn $
 * Revision 1.3  2006/03/09 10:14:00  deniger
 * Suppression des accents
 *
 * Revision 1.2  2005/08/16 12:56:56  deniger
 * correction noms de variables
 *
 * Revision 1.1  2003/01/29 11:02:40  deniger
 * version initiale
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/04 20:02:06  Herve_AGNOUX
 * Initial revision
 *
 */
/*
-- TerminalStandard v111999

Fichier : MinReader.java
Auteur : Herve AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/
package com.diaam.lgpl.ts;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/**
 * <code>MinReader</code> est une sorte de "workeuround" au fait que je ne
 * sois pas @?!\@@ parvenu a faire fonctionner des "pipe" java avec
 * Pnuts. Elle stoque les caract�res qu'on lui envoi avec <code>
 * aj</code> pour les delivrer lors d'un <code>read</code> : c'est
 * un <em>Reader</em>, tout de meme...
 * Quelles sont les modifs depuis la derni�re version ? Juste la methode
 * "asStream", o� je dois a
 * <a href="http://www.memoire.com/guillaume-desnoix/alma/">
 * l'equipe d'Alma</a> quelques judicieuses contributions.
 * <p>Cette classe est en diffusion LGPL, et elle est telechargeable a
 * <a href="http://perso.club-internet.fr/hagnoux/java/TerminalStandard">
 * http://perso.club-internet.fr/hagnoux/java/TerminalStandard</a>.<br>
 * @author <a href="h_agnoux.html">Herve AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 */
public class MinReader extends Reader
{
	private final static int TAILLE_BUF_IN = 1023;

	private boolean ouvert = true;
	char readerBuf[] = new char[TAILLE_BUF_IN];
	int debut = 0;
	int fin = 0;

  /**
   * R.A.S.
   */
	public MinReader()
	{
		lock = new Object();
	}

  /**
   * R.A.S.
   * @exception IOException R.A.S.
   */
	@Override
  public int read(char cbuf[], int off, int len) throws IOException
	{
		int nbLu = 0;

		if (!ouvert)
			throw new IOException("Lecteur ferme.");
		try
		{
			if (debut == fin)
			{
				synchronized (lock)
				{
					lock.wait();
				}
			}
			cbuf[off] = readerBuf[debut];
			debut++;
			if (debut == TAILLE_BUF_IN)
				debut = 0;
			nbLu = 1;
		}
		catch (Exception e)
		{
		  // rien... (du moins, il me semble...)
		}
		return nbLu;
	}

  /**
   * R.A.S.
   */
	@Override
  public void close()
	{
		ouvert = false;
	}

  /**
   * Ajoute des caract�res a lire par <code>read</code>.
   * @param clef  le caract�re.
   */
	public void aj(char clef)
	{
		int memoFin;
		boolean aDebloquer;

		memoFin = fin;
		aDebloquer = fin == debut;
		readerBuf[fin] = clef;
		fin++;
		if (fin == TAILLE_BUF_IN)
			fin = 0;
		if (fin == debut)
		{
			System.err.println
			  ("Buffer d'entree de MinReader trop petit !");
			fin = memoFin;
		}
		if (aDebloquer)
		{
			synchronized (lock)
			{
				lock.notifyAll();
			}
		}
	}

  /**
   * R.A.S.
   */
	@Override
  public boolean ready()
	{
		return debut != fin;
	}

  /**
   * Renvoit un InputStream intermediaire.
   * @return ... voir ligne au dessus.
   */
  public InputStream asStream()
  {
    return new Stream();
  }

  class Stream extends java.io.InputStream
  {
    char buf[] = new char[1];

    @Override
    public int read() throws IOException
    {
      int l;

      l = MinReader.this.read(buf, 0, 1);
      if (l == -1)
	return l;
      return buf[0];
    }

    @Override
    public int available()
    {
      int c;

      if (debut >= fin)
	c = debut - fin;
      else
	c = readerBuf.length - fin + debut;
      return c;
    }

    /**
     * Merci a l'equipe d'Alma ! Pour des raisons qui m'echappent, il faut
     * surcharger cette methode.
     */
    @Override
    public int read(byte[] _b, int _d, int _l) throws IOException
    {
      if(_l==0) return 0;
      _b[_d]=(byte)read();
      return 1;
    }
  }
}



