/*
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2006/03/09 10:14:00  deniger
 * Suppression des accents
 *
 * Revision 1.1  2003/01/29 11:02:40  deniger
 * version initiale
 *
 * Revision 1.2  2001/02/09 16:59:59  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/04 19:50:37  Herve_AGNOUX
 * Initial revision
 *
 */
/*
-- TerminalStandard v111999

Fichier : TerminalStandard.java
Auteur : Herve AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/

package com.diaam.lgpl.ts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * Cette classe, a l'origine du package, n'est plus qu'une vague
 * utilisation de la classe ShSh... Grandeur et decadence...
 * La classe <code>TerminalStandard</code> permet de placer
 * les flux standards (in, out, err) d'une appli dans une 
 * fenetre swing.<br>
 * La methode 'main' presente un exemple basique d'utilisation.
 * Certaine classes statiques facilitent sont integration dans des applis.
 * Cette classe est en diffusion LGPL, et elle est telechargeable a
 * <a href="http://perso.club-internet.fr/hagnoux/java/TerminalStandard">
 * http://perso.club-internet.fr/hagnoux/java/TerminalStandard</a><br>
 * @author <a href="h_agnoux.html">Herve AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
*/
public class TerminalStandard extends javax.swing.JPanel
{
	/**
	 * Pratique pour deboguer. Cet attribut conserve 
	 * le System.out original.
	 */
    public static final PrintStream ini = System.out;

    private ShSh monPays;
    private PrintStream oriOut;
    private PrintStream oriErr;
    private InputStream oriIn;
    private FileWriter log;
	
	/**
	* Le constructeur cree le ShSh, le place dans un
	* <code>JScrollPane</code>, et ajoute le tout au centre d'un
	* BorderLayout du panneau.
	*/
	public TerminalStandard()
	{
          try {
            log=new FileWriter(System.getProperty("user.dir")+File.separator+"ts.log");
          } catch( IOException e ) { System.err.println("TS: impossible de creer le fichier de log"); }
	    try
		{
		    Document d;
		    JScrollPane jsp;
		    
		    monPays = new ShSh(log);
                Style fs=monPays.getStyle(StyleContext.DEFAULT_STYLE);
                FontMetrics fm=monPays.getFontMetrics(
                  new Font(StyleConstants.getFontFamily(fs), Font.PLAIN, StyleConstants.getFontSize(fs)));
		    d = monPays.getDocument();
		    d.insertString
			(0,
			 "TerminalStandard v111999\n"+
			 "(c)1999 Herve AGNOUX <hagnoux@diaam.com>\n"+
			 "Distribue sous les termes de la LGPL\n"+
			 "\n", null);
		    jsp = new JScrollPane(monPays);
		    setLayout(new BorderLayout());
		    add("Center", jsp);
                setPreferredSize(
                  new Dimension(
                  fm.stringWidth(
                  "mmmmmmmmmmmmmmmmmmmm"+
                  "mmmmmmmmmmmmmmmmmmmm"+
                  "mmmmmmmmmmmmmmmmmmmm"+
                  "mmmmmmmmmmmmmmmmmmmmm"),
                  fm.getHeight()*25));
		}
	    catch (BadLocationException ble)
		{
		    throw new IllegalStateException(ble.toString());
		}
	}
	
	/**
	* Cette methode va chercher les flux de ShSh, et par 
	* l'intermediaire de leur methode "asStream", les assigne 
	* comme flux standard de l'appli.
	*/
	public void activeStandards()
	{
	    SwingTextDocumentWriter stdw1, stdw2;
	    MinReader mr;

	    if (oriIn == null)
		{
		    oriIn = System.in;
		    oriOut = System.out;
		    oriErr = System.err;
		    stdw1 = monPays.redacteur();
		    System.setOut(new PrintStream(stdw1.asStream()));
		    stdw2 = monPays.gueulard();
		    System.setErr(new PrintStream(stdw2.asStream()));
		    mr = monPays.lecteur();
		    System.setIn(mr.asStream());
		}
	}

  /**
   * Redirige le focus vers le ShSh interne.
   */
    @Override
    public void requestFocus()
    {
	if (isRequestFocusEnabled())
	    monPays.requestFocus();
    }

  /**
   * retablit les flux originaux de l'appli.
   */
    @Override
    public void removeNotify()
    {
	if (oriIn != null)
	    {
		System.setIn(oriIn);
		System.setOut(oriOut);
		System.setErr(oriErr);
	    }
	super.removeNotify();
    }

  /**
   * Pour utiliser TerminalStandard comme une appli (qui sert a quoi ? :-((
   * Je sais pas) (Mais si ! Allez voir la classe ScriptsStream !
   * @param lui le TerminalStandard qu'on veut utiliser.
   * @return une JFrame contenant "lui" (hum !), disposee par un
   * BorderLayout avec "lui" en son centre, une barre d'outil au nord,
   * le focus redirige vers elle, l'arret de l'appli si on clique sur la
   * croix...
   */ 
  public static JFrame enAppli(final TerminalStandard lui)
  {
    JFrame jf;
    Container c;
    Dimension d;
    JToolBar jtb;
    
    lui.activeStandards();
    jf = new JFrame();
    jf.addWindowListener(new WindowAdapter()
			 {
			   @Override
         public void windowClosing(WindowEvent e)
			     {System.exit(0);}
			   @Override
         public void windowActivated(WindowEvent e)
			     {lui.requestFocus();}
			 });
    c = jf.getContentPane();
    c.setLayout(new BorderLayout());
    c.add("Center", lui);
    jtb = new JToolBar();
    jtb.add(new ActionSauvegarde(lui.monPays));
    c.add("North", jtb);
    d = Toolkit.getDefaultToolkit().getScreenSize();
    jf.setBounds
      ((d.width * 10)/100, (d.height * 15)/100, 
       (d.width * 60)/100, (d.height * 50)/100);
    jf.setIconImage
	(Toolkit.getDefaultToolkit().getImage
	 (lui.getClass().getResource("tsico.gif")));
    return jf;
  }

    /**
     * Pour utiliser TerminalStandard en fenetre de log, a cote de l'appli
     * principale. Cette fenetre n'apparait QUE SI on ecrit quelque chose
     * dessus. Si l'utilisateur la referme, elle reste cachee, et
     * reapparrait des que l'on re-ecrit quelque chose dessus. Son
     * apparence et sa disposition generale sont la meme qu'avec
     * "enAppli".
     * @param lui le TerminalStandard a utiliser.
     * @return une JFrame de fenetre.
     * @see #enAppli
     */
    public static JFrame pourAfficherLog(TerminalStandard lui)
    {
      Dimension d;
      EcritureDocumentFaitJaillir edfj;
      JFrame jf;
      Container c;
      JToolBar jtb;

      lui.activeStandards();
      jf = new JFrame();
      edfj = lui.new EcritureDocumentFaitJaillir(jf, lui);
      lui.monPays.getDocument().addDocumentListener(edfj);
      c = jf.getContentPane();
      c.setLayout(new BorderLayout());
      c.add("Center", lui);
      jtb = new JToolBar();
      jtb.add(new ActionSauvegarde(lui.monPays));
      c.add("North", jtb);
      d = Toolkit.getDefaultToolkit().getScreenSize();
      jf.setBounds
	((d.width * 15)/100, (d.height * 20)/100, 
	 (d.width * 80)/100, (d.height * 60)/100);
      jf.setTitle("TerminalStandard : fenetre de log");
      return jf;
    }
	
	/**
	* Un exemple a suivre...
	* Voici le code mod�le :<pre>
	* TerminalStandard ts;
	* JFrame jf;
	*
	* ts = new TerminalStandard();
	* jf = TerminalStandard.enAppli(ts);
	* jf.show();
	* System.out.println("Hello bizou !"); 
	* // Hello bizou appara�t sur l'ecran swing !
	* </pre>
	* Amusez vous bien.
	*/
    public static void main(String args[]) throws IOException
    {
	JFrame jf;
	final TerminalStandard ts;
		
	ts = new TerminalStandard();
	jf = TerminalStandard.enAppli(ts);
	jf.setVisible(true);
	System.out.println
	    ("tapez 'q', pour sortir, \nou 'x' pour montrer "+
	     "une exception sur la sortie erreur.");
	while (true)
	    {
		int i;
		
		i = System.in.read();
		if ((char)i == 'q')
		    System.exit(0);
		else if ((char)i == 'x')
		    {
			Exception e = new Exception("La belle !");
			e.printStackTrace();
		    }
		System.out.print(""+(char)i);
	    }
    }

    private class EcritureDocumentFaitJaillir 
	implements javax.swing.event.DocumentListener
    {
	JFrame maFenetre;
	TerminalStandard maConsole;

	EcritureDocumentFaitJaillir
	    (JFrame fenetre, TerminalStandard console)
	{
	    maFenetre = fenetre;
	    maConsole = console;
	}

	@Override
  public void changedUpdate(DocumentEvent de)
	{
	}
	
	@Override
  public void insertUpdate(DocumentEvent de)
	{
	    if (maFenetre.isVisible())
		return;
	    maFenetre.setVisible(true);
	}

	@Override
  public void removeUpdate(DocumentEvent de)
	{
	}
    }
}
