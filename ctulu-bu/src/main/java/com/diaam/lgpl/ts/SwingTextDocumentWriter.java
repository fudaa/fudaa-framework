/*
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2006/03/09 10:14:00  deniger
 * Suppression des accents
 *
 * Revision 1.1  2003/01/29 11:02:40  deniger
 * version initiale
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/04 20:03:33  Herve_AGNOUX
 * Initial revision
 *
 */
/*
-- TerminalStandard v111999--

Fichier : SwingTextDocumentWriter.java
Decembre 1998
Auteur : Herve AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/
package com.diaam.lgpl.ts;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;

/**
 * <code>SwingTextDocumentWriter</code> est un writer pour ecrire dans
 * un document swing.<p>
 * Quelles sont les modifs par rapport a la precedente version ? L'ajout 
 * de la methode "asStream", et l'ajout de la gestion des attributs.
 * <p>Cette classe est en diffusion LGPL, et elle est telechargeable a
 * <a href="http://perso.club-internet.fr/hagnoux/java/TerminalStandard">
 * http://perso.club-internet.fr/hagnoux/java/TerminalStandard</a>.<br>
 * @author <a href="h_agnoux.html">Herve AGNOUX</a>, 
 * hagnoux@mail.club-internet.fr
 * @version 1.1 fevrier 1999.
 */
public class SwingTextDocumentWriter extends java.io.Writer
{
	private Document monDocument;
	private boolean ouvert = true;
	private int lOffsetAtteint;
    private AttributeSet mesAttributs;
    private Vector buffer;
    private Writer log;
	
  /**
   * Le constructeur enregistre le document dans lequel ecrire, et
   * initialise l'offset pour reperer la fin du dernier write, et initialise
   * les attributs des caract�res pour reperer ceux qui sont passes par
   * son entremise.
   * @param doc le document swing.
   */
	public SwingTextDocumentWriter(Document doc, Writer _log)
	{
          log=_log;
	    SimpleAttributeSet sas;

	    sas = new SimpleAttributeSet();
	    sas.addAttribute(this, "encore mieux !");
	    mesAttributs = sas;
		monDocument = doc;
		lOffsetAtteint = monDocument.getLength();
		buffer = new Vector();
      }
      
	public SwingTextDocumentWriter(Document doc)
	{
        this(doc, null);
	}
	
  /**
   * Pas grand chose a dire. A chaque <code>write</code>, l'instance
   * memorise l'offset du dernier caract�re ecrit, qui sera renvoye par
   * <code>offsetAtteint</code>.
   * @param cbuf Tableau de caract�res
   * @param off offset dans le tableau a partir duquel il faut ecrire les
   * caract�res.
   * @param len nombre de caract�res a ecrire.
   * @exception IOException la methode transforme les exceptions swing en
   * IOException.
   */
	@Override
  public void write(char cbuf[], int off, int len) throws IOException
	{
	    String s = new String(cbuf, off, len);
	    buffer.addElement(s);
          if( log!=null ) log.write(cbuf, off, len);
	}

  /**
   * Ne fait rien.
   * @exception IOException ...si l'on flush alors que le flux est ferme.
   */
	@Override
  public void flush() throws IOException
	{
	  if (!ouvert)
	    throw new IOException("Ecrivain ferme");
        if( log!=null ) log.flush();
	  try
	    {
	      Vector v;
	      String ss[];
	      final StringBuffer sb;

	      v = (Vector)buffer.clone();
	      buffer.removeAllElements();
	      ss = new String[v.size()];
	      v.copyInto(ss);
	      sb = new StringBuffer();
	      for (int i = 0; i < ss.length; i++)
		{
		  sb.append(ss[i]);
		}
	      monDocument.insertString
		(monDocument.getLength(), sb.toString(), mesAttributs);
	      lOffsetAtteint = monDocument.getLength();
	    }
	  catch (BadLocationException ble)
	    {
	      throw new IOException(ble.toString());
	    }
	  catch (Exception e){}
	}
	
  /**
   * Ne fait rien, a part renseigner un flag pour memoriser que le flux
   * et ferme.
   */
	@Override
  public void close() throws IOException
	{
        if( log!=null ) log.close();
		ouvert = false;
	}
	
  /**
   * Permet de savoir l'offset du dernier caract�re ecrit, pour divers
   * traitements (empecher son effacement, en l'occurence). Cette fa�on de
   * faire n'est pas geniale, et il y en aura probablement une autre plus tard.
   */
	public int offsetAtteint()
	{
	    return lOffsetAtteint;
	}
	
  /**
   * Pour les tests.
   * @exception Exception pour se debarasser.
   */
	public static void main(String args[]) throws Exception
	{
		JFrame jf;
		JPanel jp;
		JScrollPane jsp;
		JTextArea jta;
		SwingTextDocumentWriter stdw;
		
		jf = new JFrame("Test SwingTestDocumentWriter");
		jp = new JPanel();
		jp.setLayout(new BorderLayout());
		jp.setPreferredSize(new Dimension(400, 250));
		jta = new JTextArea();
		jta.setEditable(false);
		jsp = new JScrollPane(jta);
		jp.add("Center", jsp);
		jf.setContentPane(jp);
		jf.pack();
		stdw = new SwingTextDocumentWriter(jta.getDocument());
		stdw.write("Tapez 'q' pour sortir.\n");
		stdw.write
		  ("Tout ce que vous ecrivez sur l'entree standard "+
		   "sera ecrit ici : ");
		jf.setVisible(true);
		while (true)
		{
			char lu;
			
			lu = (char)System.in.read();
			if (lu == 'q')
				System.exit(0);
			stdw.write(lu);
		}
	}

  /**
   * @return un Stream intermediaire.
   */
  public OutputStream asStream()
  {
    return new Stream();
  }

  class Stream extends java.io.OutputStream
  {
    char buf[] = new char[1];

    @Override
    public void write(int b) throws IOException
    {
      buf[0] = (char)b;
      SwingTextDocumentWriter.this.write(buf, 0, 1);
      flush();
    }

    @Override
    public void write(byte b[], int off, int len) throws IOException
    {
      char c[];

      c = new char[b.length];
      for (int i = off; i < off + len; i++)
	c[i] = (char)b[i];
      SwingTextDocumentWriter.this.write(c, off, len);
      flush();
    }

	@Override
  public void flush() throws IOException
	{
	    SwingTextDocumentWriter.this.flush();
	}
	
  }
}
