/*
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/13 19:53:47  Herve_AGNOUX
 * Initial revision
 *
 */
/*
-- TerminalStandard v111999

Fichier : TerminalStandard.java
Auteur : Herv� AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/
package com.diaam.lgpl.script;

/**
 * Cet interface permet � une application d'exposer les objets
 * qu'elle veut � un interpreteur.
 * L'application lance d'abord un objet qui concr�tise cette interface,
 * puis lui expose les objets qu'elle souhaite, puis lance 
 * l'interpr�teur.
 * A partir de l�, l'interpreteur peut faire ce qu'il veut avec les objets
 * expos�s.
 * @author <a href="h_agnoux.html">Herv� AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 */
public interface Interpretable
{
    /**
     * Pour exposer un objet � l'interpreteur. Une fois que
     * l'application a exposer cet objet � l'interpreteur
     * le script est normalement libre de faire ce qu'il veut avec
     * cet objet.
     *
     * @param nom Un nom que l'appli sugg�re � l'interpr�teur pour
     * rep�rer cet objet.
     * @param lui L'objet que l'appli expose � l'interpreteur.
     */
    public void ajouteObjet(String nom, Object lui);


    /**
     * Une m�thode surtout pour la beaut� intellectuelle, je ne sais
     * pas si elle est utilisable, ni m�me s'il est pr�f�rable de
     * passer le nom de l'objet ou sa r�f�rence... Normalement,
     * son but est de dire � l'interpreteur que l'objet qui a tel nom
     * n'est plus disponible.
     *
     * @param nom le nom de l'objet.
     */
    public void effaceObjet(String nom);

  /**
   * Le script se lance en utilisant les ports standards de l'appli.
   */
  public void doWithStandardsStreams();

  /**
   * Le script se lance en utilisant les ports stipul�s dans ways.
   * @param ways les ports
   * @param withUser si 'true', le script est interactif.
   */
  public void doDirectly(TextWays ways, boolean withUser);
}
