/*
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/13 19:55:58  Herve_AGNOUX
 * Initial revision
 *
 */
/*
-- TerminalStandard v111999

Fichier : TerminalStandard.java
Auteur : Herv� AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/
package com.diaam.lgpl.script;

import java.io.Reader;
import java.io.Writer;

/**
 * Pas grand chose � dire de cette classe... sauf qu'elle m�rite sans doute
 * d'�tre d�velopp�e.
 * @author <a href="h_agnoux.html">Herv� AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 */
public class TextWays
{
  public Reader in;
  public Writer out;
  public Writer err;
}
