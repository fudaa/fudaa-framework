/*
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2006/03/09 10:53:04  deniger
 * maj
 *
 * Revision 1.3  2005/08/16 12:56:56  deniger
 * correction noms de variables
 *
 * Revision 1.2  2005/05/20 20:47:58  deniger
 * maj bu
 *
 * Revision 1.1  2003/01/29 11:02:46  deniger
 * version initiale
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.2  1999/11/20 20:34:56  Herve_AGNOUX
 * Pour changer le nom du fichier :
 * ScriptWays.java -> ScriptsWays.java,
 * pour faire comme 'Script_s_Streams.java'
 *
 * Revision 1.1  1999/11/20 16:02:15  Herve_AGNOUX
 * Initial revision
 *
 *
 */
/*
-- TerminalStandard v111999

Fichier : TerminalStandard.java
Auteur : Herv? AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/

package com.diaam.lgpl.applications;

import com.diaam.lgpl.script.Interpretable;
import com.diaam.lgpl.script.TextWays;
import com.diaam.lgpl.ts.ShSh;
import com.diaam.lgpl.ts.TerminalStandard;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.Introspector;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 * Cette classe est une simple application pour montrer l'utilisation
 * de scripts avec ShSh, et en utilisant des Reader et des Writer.
 * @author <a href="h_agnoux.html">Herv? AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 * @see com.diaam.lgpl.ts.ShSh
 */
public class ScriptsWays
{
  /**
   * Utilise la propri?t? syst?me "script" pour connaitre la classe
   * de script ? lancer. Utilise le BeanInfo correspondant pour le
   * descriptif. En cas que y'a des messages sur les flux stantards,
   * les affiche sur un TerminalStandard.
   * @param args non utilis?.
   * @see com.diaam.lgpl.ts.TerminalStandard
   * @exception Exception par ce que je suis feneant.
   */
  public static void main(String args[]) throws Exception
  {
    String s;
    TerminalStandard ts;
    JFrame jf1, jf2;
    BeanInfo bi;
    BeanDescriptor bd;
    final Class c;
    final Interpretable i;
    final ShSh t;
    Container co;
    Dimension dm;
    TextWays tw;
    JScrollPane jsp;

    ts = new TerminalStandard();
    jf1 = TerminalStandard.pourAfficherLog(ts);
    s = System.getProperty("script");
    if (s == null)
      {
	jf1.addWindowListener(new WindowAdapter()
			 {
			   @Override
         public void windowClosing(WindowEvent e)
			     {
				 System.exit(0);
			     }
			 });
	System.out.println
	  ("Veuillez indiquer avec une propriete le langage de script\n"+
	   "a utiliser SVP.\n"+
	   "Ex : java -Dscript=com.diaam.lgpl.realscripts.ForPnuts\n"+
	   "\tcom.diaam.lgpl.applications.ScriptStream\n");
	System.out.flush();
	return;
      }
    c = Class.forName(s);
    bi = Introspector.getBeanInfo(c);
    bd = bi.getBeanDescriptor();
    t = new ShSh();
    tw = new TextWays();
    tw.in = t.lecteur();
    tw.out = t.redacteur();
    tw.err = t.gueulard();
    jf2 = new JFrame();
    if (bd == null)
	jf2.setTitle("script avec '"+c.getName()+"'");
    else
	{
	    String s1, s2;

	    s1 = bd.getName();
	    if (s1 == null)
		jf2.setTitle("script avec '"+c.getName()+"'");
	    else
		jf2.setTitle("script avec '"+s1+"'");
	    s2 = bd.getShortDescription();
	    if (s2 != null)
		tw.out.write(s2+"\n");
	}
    i = (Interpretable)c.newInstance();
    i.ajouteObjet("interpretable", i);
    i.ajouteObjet("ts", ts);
    co = jf2.getContentPane();
    co.setLayout(new BorderLayout());
    jsp = new JScrollPane(t);
    co.add("Center", jsp);
    dm = Toolkit.getDefaultToolkit().getScreenSize();
    jf2.setBounds
      (0, (dm.height * 15)/100,
       dm.width, (dm.height * 60)/100);
    Thread u = new Thread(new RunScript(i, tw));
    u.start();
    jf2.addWindowListener(new WindowAdapter()
			  {
			      @Override
            public void windowActivated(WindowEvent e)
				  {t.requestFocus();}
			      @Override
            public void windowClosing(WindowEvent e)
				  {System.exit(0);}
			  });
    jf2.setVisible(true);
  }

  private static class RunScript implements java.lang.Runnable
  {
    Interpretable monScript;
    TextWays mesVoies;

    RunScript(Interpretable script, TextWays voies)
    {
      monScript = script;
      mesVoies = voies;
    }

    @Override
    public void run()
    {
	monScript.doDirectly(mesVoies, true);
    }
  }
}

