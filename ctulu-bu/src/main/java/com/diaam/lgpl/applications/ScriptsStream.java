/*
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 11:02:47  deniger
 * version initiale
 *
 * Revision 1.1  2000/11/10 15:31:30  vonarnim
 * - deplacement (et nouvelle version) de TerminalStandard dans ctulu
 *
 * Revision 1.1  1999/11/15 15:21:23  Herve_AGNOUX
 * Initial revision
 *
 *
 */
/*
-- TerminalStandard v111999

Fichier : TerminalStandard.java
Auteur : Herv� AGNOUX, hagnoux@mail.club-internet.fr
License LGPL.
*/

package com.diaam.lgpl.applications;

import com.diaam.lgpl.script.Interpretable;
import com.diaam.lgpl.ts.TerminalStandard;
import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.Introspector;
import javax.swing.JFrame;

/**
 * Cette classe est une simple application pour montrer l'utilisation
 * de scripts avec TerminalStandard.
 * @author <a href="h_agnoux.html">Herv� AGNOUX</a>,
 * hagnoux@mail.club-internet.fr
 * @version 111999
 * @see com.diaam.lgpl.ts.TerminalStandard
 */
public class ScriptsStream
{
  /**
   * Utilise la propri�t� syst�me "script" pour connaitre la classe
   * de script � lancer. Utilise le BeanInfo correspondant pour le
   * descriptif.
   * @param args non utilis�.
   * @exception Exception par ce que je suis feneant.
   */
  public static void main(String args[]) throws Exception
  {
    String s;
    TerminalStandard ts;
    JFrame jf;
    BeanInfo bi;
    BeanDescriptor bd;
    final Class c;
    final Interpretable i;

    s = System.getProperty("script");
    if (s == null)
      {
	System.out.println
	  ("Veuillez indiquer avec une propriete le langage de script\n"+
	   "a utiliser SVP.\n"+
	   "Ex : java -Dscript=com.diaam.lgpl.realscripts.ForPnuts\n"+
	   "\tcom.diaam.lgpl.applications.ScriptStream\n");
	System.exit(0);
      }
    c = Class.forName(s);
    ts = new TerminalStandard();
    bi = Introspector.getBeanInfo(c);
    bd = bi.getBeanDescriptor();
    jf = TerminalStandard.enAppli(ts);
    // en esperant qu'il n'y ait pas d'erreur entre la...
    if (bd == null)
	jf.setTitle("script avec '"+c.getName()+"'");
    else
	{
	    String s1, s2;

	    s1 = bd.getName();
	    if (s1 == null)
		jf.setTitle("script avec '"+c.getName()+"'");
	    else
		jf.setTitle("script avec '"+s1+"'");
	    s2 = bd.getShortDescription();
	    if (s2 != null)
		System.out.println(s2);
	}
    i = (Interpretable)c.newInstance();
    i.ajouteObjet("interpretable", i);
    i.ajouteObjet("ts", ts);
    Thread t = new Thread(new Runnable(){
      @Override
      public void run(){
	i.doWithStandardsStreams();}});
    t.start();
    System.out.println("Avec les streams System.in, System.out et System.err");
    // et la.
    jf.setVisible(true);
  }
}
