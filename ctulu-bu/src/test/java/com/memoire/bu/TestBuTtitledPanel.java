/*
 * @creation 2 mai 07
 * @modification $Date: 2007-05-04 13:43:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package com.memoire.bu;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.border.LineBorder;

/**
 * @author fred deniger
 * @version $Id: TestBuTtitledPanel.java,v 1.1 2007-05-04 13:43:21 deniger Exp $
 */
public final class TestBuTtitledPanel {

  private TestBuTtitledPanel() {
  }

  public static void main(String[] _args) {
    JFrame frame = new JFrame("Test BuTitledPanel");

    BuTitledPanel p = new BuTitledPanel(new BuButton("XXXX"));
    //BuCheckBox("A checkbox here!"));
    BuTextField tf = new BuTextField("test");
    tf.setSelectAllIsGainFocus(true);
    p.setLayout(new FlowLayout());
    p.add(tf);
    tf = new BuTextField("test");
    tf.setSelectAllIsGainFocus(true);
    p.add(tf);
    JComponent c = new JTree();
    c.setBorder(new BuCropBorder(new LineBorder(Color.red, 5), true, false, false, true));
    p.add(c);

    frame.getContentPane().add(p);

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });

    frame.pack();
    frame.setVisible(true);
  }
}
