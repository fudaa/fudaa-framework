/*
 *  @creation     23 janv. 2006
 *  @modification $Date: 2006-01-23 13:28:41 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package com.memoire.bu;

import java.awt.LayoutManager;
import javax.swing.JFrame;

/**
 * Test du layout BuFormLayout.
 *
 * @author Fred Deniger
 * @version $Id: TestFormLayout.java,v 1.1 2006-01-23 13:28:41 deniger Exp $
 */
public final class TestFormLayout {

  private TestFormLayout() {

  }

  public static BuPanel buildPanel(LayoutManager _lay, String _title,int _idx) {
    final BuPanel p1 = new BuTitledPanel(new BuLabel(_title),_lay);
    p1.add(new BuCheckBox("cb " + _title),BuFormLayout.constraint(0, _idx,true,0f));
    p1.add(new BuTextField("lb " + _title),BuFormLayout.constraint(1, _idx,1,1,true,true,1f,1f));
    return p1;
  }

  public static void main(String[] _args) {
    final BuPanel content = new BuPanel();
    final BuFormLayout formLayout = new BuMultiFormLayout(5, 5,1,1);
    content.setLayout(new BuVerticalLayout(5,true,false));
    final BuPanel p1 = buildPanel(formLayout, "1",0);
    final BuPanel p2 = buildPanel(formLayout,"2",1);
    content.add(p1);
    content.add(p2);
    final JFrame fr = new JFrame();
    fr.setContentPane(content);
    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.pack();
    fr.setVisible(true);
  }

}
