package org.fudaa.dodico.ef.operation.projection;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntIntHashMap;
import java.io.IOException;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.decorator.AbstractEfGridDataDecorator;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;
import org.fudaa.dodico.ef.operation.clean.EfOperationCleanGrid;

/**
 * Use to project a grid on a polygon.
 *
 */
public class EfOperationProjectionOrtho extends AbstractEfOperation {
  public class EfGridDataProjectionDecorator extends AbstractEfGridDataDecorator {

    private TIntIntHashMap eltNewIdxOldIdx;
    private boolean[] isPtModified;

    public EfGridDataProjectionDecorator(final EfGridData _src, EfGridInterface newGrid,
        InterpolationVectorContainer vectorContainer) {
      super(_src, newGrid);

    }

    @Override
    public EfGridInterface getGrid() {
      return getNewGrid();
    }

    @Override
    public EfData getData(final CtuluVariable _t, final int _idxTime) throws IOException {
      final EfData init = getInit().getData(_t, _idxTime);
      if (init == null) { return null; }

      final boolean isEle = init.isElementData();

      if (isEle) {
        if (this.eltNewIdxOldIdx != null) {
          // le type de la valeur initiale est conserv�e
          double[] newValues = new double[this.getNewGrid().getEltNb()];

          for (int i = 0; i < newValues.length; i++) {
            final int oldEltIdx = this.eltNewIdxOldIdx.get(i);
            newValues[i] = init.getValue(oldEltIdx);
          }

          return new EfDataElement(newValues);
        } else {
          return init;
        }
      }

      double[] newValues = new double[this.getNewGrid().getPtsNb()];
      for (int i = 0; i < newValues.length; i++) {
        if (this.isPtModified[i]) {
          // TODO Calculer la valeur par interpolation.
          newValues[i] = init.getValue(i);
        } else {
          newValues[i] = init.getValue(i);
        }
      }

      return new EfDataNode(newValues);
    }

    /**
     * @param eltNewIdxOldIdx the eltNewIdxOldIdx to set
     */
    public void setEltNewIdxOldIdxMap(TIntIntHashMap eltNewIdxOldIdx) {
      this.eltNewIdxOldIdx = eltNewIdxOldIdx;
    }

    /**
     * @return the eltNewIdxOldIdx
     */
    public TIntIntHashMap getEltNewIdxOldIdxMap() {
      return eltNewIdxOldIdx;
    }

    /**
     * @return the isPtModified
     */
    public boolean[] getIsPtModified() {
      return isPtModified;
    }

    /**
     * @param isPtModified the isPtModified to set
     */
    public void setIsPtModified(boolean[] isPtModified) {
      this.isPtModified = isPtModified;
    }

    /**
     * @return the newGrid_
     */
    public EfGridInterface getNewGrid() {
      return super.getGrid();
    }
  }

  private GISPolygone polygone;
  private InterpolationVectorContainer interpolator;

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    EfGridInterface grid = this.initGrid.getGrid();
    EfNode[] newNodes = grid.getNodes();
    EfElement[] elements = grid.getElts();
    boolean[] isModified = new boolean[newNodes.length];
    boolean[] isErased = new boolean[grid.getEltNb()];
    Coordinate[] polyCoord = this.polygone.getCoordinates();

    for (int i = 0; i < (polyCoord.length - 1); i++) {
      // R�cup�ration des points du segment en cours du polygone et calcul de dx et dy.
      double x1 = polyCoord[i].x;
      double y1 = polyCoord[i].y;
      double x2 = polyCoord[i + 1].x;
      double y2 = polyCoord[i + 1].y;

      double dx = x2 - x1;
      double dy = y2 - y1;

      int[] ptsIndicator = this.computePtsIndicator(newNodes, dx, dy, x1, y1);

      this.eraseExteriorElts(isErased, ptsIndicator);
      this.eraseOverstressedElts(elements.length, newNodes, isErased, ptsIndicator, dx, dy);
      this.projectPts(newNodes, isModified, ptsIndicator, dx, dy, x1, y1, x2, y2);
    }

    EfElement[] newElements = new EfElement[this.computeNbEltsNotErased(isErased)];
    TIntIntHashMap eltNewIdxOldIdx = new TIntIntHashMap(newElements.length);
    int currentIdx = 0;

    // Cr�ation du nouveau tableau d'�l�ment et de la correspondance des indices.
    for (int i = 0; i < isErased.length; i++) {
      if (!isErased[i]) {
        if (elements[i] == null) {
          log.addFatalError("ef.projection.failed");
          return null;
        }
        newElements[currentIdx] = new EfElement(elements[i]);
        eltNewIdxOldIdx.put(currentIdx, i);

        currentIdx++;
      }
    }

    EfGridInterface newGrid = new EfGrid(newNodes, newElements);// TODO new element est null null..

    EfGridDataProjectionDecorator gridData = new EfGridDataProjectionDecorator(this.initGrid, newGrid,
        this.interpolator);

    gridData.setEltNewIdxOldIdxMap(eltNewIdxOldIdx);
    gridData.setIsPtModified(isModified);

    // Nettoyage du maillage pour enlev� les points non utilis�.
    EfOperationCleanGrid cleanGrid = new EfOperationCleanGrid();

    cleanGrid.setInitGridData(gridData);
    cleanGrid.setMinDistance(0d);

    return cleanGrid.process(prog).getGridData();
  }

  /**
   * Donne le nombre d'�l�ment effac�.
   * 
   * @param isErased un tableau contenant pour chaque indice si l'�l�ment repr�sent� par cette indice a �t� effac�.
   * @return le nombre d'�l�ment effac�.
   */
  private int computeNbEltsNotErased(final boolean[] isErased) {
    int nbEltsErased = 0;

    for (int i = 0; i < isErased.length; i++) {
      if (!isErased[i]) {
        nbEltsErased++;
      }
    }

    return nbEltsErased;
  }

  /**
   * Projette les points fronti�res sur le polygone.
   * 
   * @param nodes un tableau contenant tout les noeuds.
   * @param isModified un tableau permettant de savoir si un noeud a �t� modifi� (d�plac� sur le polygone).
   * @param ptsIndicator un tableau contenant des indication pour chaque noeud (0 = ext�rieur, 1 = int�rieur, 2 =
   *          fronti�re).
   * @param dx le dx actuel.
   * @param dy le dy actuel.
   * @param x1 le x du premier point du segment actuel du polygone.
   * @param y1 le y du premier point du segment actuel du polygone.
   * @param x2 le x du deuxi�me point du segment actuel du polygone.
   * @param y2 le y du deuxi�me point du segment actuel du polygone.
   */
  private void projectPts(final EfNode[] nodes, final boolean[] isModified, final int[] ptsIndicator, double dx,
      double dy, double x1, double y1, double x2, double y2) {
    double a1 = 1d / (Math.pow(dx, 2) + Math.pow(dy, 2));
    double a2 = a1 * ((x1 * y2) - (y1 * x2));

    for (int i = 0; i < nodes.length; i++) {
      // Si le point en cours est sur la fronti�re, il est d�plac� sur le polygone.
      if (ptsIndicator[i] == 2) {
        double a3 = a1 * ((nodes[i].getX() * dx) + (nodes[i].getY() * dy));

        nodes[i] = new EfNode(new Coordinate((dx * a3) + (dy * a2), (dy * a3) - (dx * a2)));

        isModified[i] = true;
      }
    }
  }

  /**
   * Creer le tableau des indicateurs.
   * 
   * @param nodes un tableau contenant tout les noeuds.
   * @param dx le dx actuel.
   * @param dy le dy actuel.
   * @param x1 le x du premier point du segment actuel du polygone.
   * @param y1 le y du premier point du segment actuel du polygone.
   * @return un tableau contenant des indication pour chaque noeud (0 = ext�rieur, 1 = int�rieur, 2 = fronti�re mais pas
   *         setter dans cette m�thode).
   */
  private int[] computePtsIndicator(final EfNode[] nodes, double dx, double dy, double x1, double y1) {
    int[] ptsIndicator = new int[nodes.length];

    for (int i = 0; i < ptsIndicator.length; i++) {
      double nodeX = nodes[i].getX();
      double nodeY = nodes[i].getY();

      // Test si le point est � l'int�rieur du segment actuel du polygone.
      if (dx * (nodeY - y1) >= dy * (nodeX - x1)) {
        ptsIndicator[i] = 1;
      } else {
        ptsIndicator[i] = 0;
      }
    }

    return ptsIndicator;
  }

  /**
   * Supprime les �l�ments se trouvant � l'ext�rieur du polygone.
   * 
   * @param isErased un tableau contenant pour chaque indice si l'�l�ment repr�sent� par cette indice a �t� effac�.
   * @param ptsIndicator un tableau contenant des indication pour chaque noeud (0 = ext�rieur, 1 = int�rieur, 2 =
   *          fronti�re).
   */
  private void eraseExteriorElts(final boolean[] isErased, final int[] ptsIndicator) {
    EfElement[] elements = this.initGrid.getGrid().getElts();
    boolean mustBeErased;

    for (int i = 0; i < isErased.length; i++) {
      if (!isErased[i]) {
        mustBeErased = false;

        int[] idx = elements[i].getIndices();

        // Si un point de l'�l�ment est � l'ext�rieur, l'�l�ment est marqu� comme supprim�.
        for (int j = 0; j < idx.length; j++) {
          if (ptsIndicator[idx[j]] == 0) {
            mustBeErased = true;

            break;
          }
        }

        if (mustBeErased) {
          isErased[i] = true;

          // Tout les points de l'�l�ment marqu� comme supprim� qui ne sont pas � l'ext�rieur du polygone, deviennent
          // des points fronti�res.
          for (int j = 0; j < idx.length; j++) {
            if (ptsIndicator[idx[j]] != 0) {
              ptsIndicator[idx[j]] = 2;
            }
          }
        }
      }
    }
  }

  /**
   * Supprime les �l�ments surcontraint.
   * 
   * @param nbEltToTest le nombre d'�l�ment � tester en commen�ant par le premier (utilis� pour la r�cursivit�).
   * @param nodes un tableau contenant tout les noeuds.
   * @param isErased un tableau contenant pour chaque indice si l'�l�ment repr�sent� par cette indice a �t� effac�.
   * @param ptsIndicator un tableau contenant des indication pour chaque noeud (0 = ext�rieur, 1 = int�rieur, 2 =
   *          fronti�re).
   * @param dx le dx actuel.
   * @param dy le dy actuel.
   */
  private void eraseOverstressedElts(int nbEltToTest, final EfNode[] nodes, final boolean[] isErased,
      final int[] ptsIndicator, double dx, double dy) {
    EfElement[] elements = this.initGrid.getGrid().getElts();
    boolean mustBeErased;

    for (int i = 0; i < nbEltToTest; i++) {
      if (!isErased[i]) {
        mustBeErased = true;
        int[] idx = elements[i].getIndices();

        // Si tout les points de l'�lement sont des points fronti�res, il est marqu� comme supprim�.
        for (int j = 0; j < idx.length; j++) {
          if (ptsIndicator[idx[j]] != 2) {
            mustBeErased = false;

            break;
          }
        }

        if (!mustBeErased) {
          int segementNb = elements[i].getNbEdge();

          // Si un segment de l'�l�ment est compos� de 2 points fronti�res et qu'il est perpendiculaire au segment
          // actuel du polygone, l'�l�ment en cours doit �tre supprim�.
          for (int j = 0; j < segementNb; j++) {
            int pt1 = elements[i].getEdgePt1(j);
            int pt2 = elements[i].getEdgePt2(j);

            if ((ptsIndicator[pt1] == 2) && (ptsIndicator[pt2] == 2)) {
              if (((dx * (nodes[pt2].getX() - nodes[pt1].getX())) + (dy * (nodes[pt2].getY() - nodes[pt1].getY()))) <= 0d) {
                mustBeErased = true;

                break;
              }
            }
          }

          // Si l'�l�ment en cours doit �tre effac�, tout les �l�ments pr�c�dent sont retest�.
          if (mustBeErased) {
            for (int j = 0; j < idx.length; j++) {
              ptsIndicator[idx[j]] = 2;
            }

            this.eraseOverstressedElts(i, nodes, isErased, ptsIndicator, dx, dy);
          }
        }

        if (mustBeErased) {
          isErased[i] = false;
        }
      }
    }
  }

  /*
   * private boolean[] computeIsExteriorPts() { EfGridInterface grid = this.initGrid.getGrid(); boolean[] isExteriorPts
   * = new boolean[grid.getPtsNb()]; EfNode[] nodes = grid.getNodes(); SimplePointInRing tester = new
   * SimplePointInRing(this.polygone);
   * 
   * for (int i = 0; i < nodes.length; i++) { isExteriorPts[i] = !(tester.isInside(nodes[i].getCoordinate())); }
   * 
   * return isExteriorPts; }
   * 
   * private int[] computeOldIdxNewIdx(boolean[] isExteriorPts) { int[] oldIdxNewIdx = new int[isExteriorPts.length];
   * int newIdx = 0;
   * 
   * for (int i = 0; i < oldIdxNewIdx.length; i++) { if (isExteriorPts[i]) { oldIdxNewIdx[i] = -1; } else {
   * oldIdxNewIdx[i] = newIdx;
   * 
   * newIdx++; } }
   * 
   * return oldIdxNewIdx; }
   * 
   * private EfElement[] degenerateExteriorElts(boolean[] isExteriorPts) { EfGridInterface grid =
   * this.initGrid.getGrid(); EfElement[] newElements = new EfElement[grid.getEltNb()];
   * 
   * //TODO Degenerer les �l�ments ext�rieurs.
   * 
   * return newElements; }
   */
  public GISPolygone getPolygone() {
    return polygone;
  }

  public void setPolygone(GISPolygone polygone) {
    this.polygone = polygone;
  }

  /**
   * @return the interpolator
   */
  public InterpolationVectorContainer getInterpolator() {
    return interpolator;
  }

  /**
   * @param interpolator the interpolator to set
   */
  public void setInterpolator(InterpolationVectorContainer interpolator) {
    this.interpolator = interpolator;
  }
}
