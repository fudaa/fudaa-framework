/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

/**
 * @author deniger
 */
public class EfTrajectoireParametersMarqueur {

  /**
   * true si c'est un marqueur sur le temps
   */
  public boolean timeStep_;
  /**
   * Le delta max entre 2 marqueurs. si timeStep_ vaut true, il s'agit de l'un delta de temps sinon c'est de distance.
   */
  public double deltaMax_;

}
