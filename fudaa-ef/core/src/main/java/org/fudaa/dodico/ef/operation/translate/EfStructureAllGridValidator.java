package org.fudaa.dodico.ef.operation.translate;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.resource.EfResource;

import java.util.Arrays;

/**
 * Use to validate that a grid is valid
 */
public class EfStructureAllGridValidator {
  private final EfGridInterface grid;
  private final TranslatePointCleanValidator cleanValidator;

  public EfStructureAllGridValidator(EfGridInterface grid) {
    this.grid = grid;
    cleanValidator = new TranslatePointCleanValidator(grid);
  }

  public CtuluLogGroup isValid(ProgressionInterface prog) {
    TIntHashSet emptyMeshes = new TIntHashSet();
    TIntIntHashMap removedPoint = new TIntIntHashMap();
    for (int i = 0; i < grid.getPtsNb(); i++) {
      cleanValidator.shouldBeCleaned(i, prog, null, removedPoint, false);
    }
    for (int i = 0; i < grid.getEltNb(); i++) {
      final int trigoOriente = grid.getElement(i).isTrigoOriente(grid);
      if (trigoOriente == 0) {
        emptyMeshes.add(i);
      }
    }

    CtuluLogGroup res = new CtuluLogGroup(null);
    if (!emptyMeshes.isEmpty()) {
      final CtuluLog log = res.createNewLog(EfResource.getS("El�ment(s) avec une surface nulle"));
      final int[] emptyMeshesArray = emptyMeshes.toArray();
      Arrays.sort(emptyMeshesArray);
      for (int i = 0; i < emptyMeshesArray.length; i++) {
        log.addWarn(EfResource.getS("L'�l�ment {0} a une surface nulle", CtuluLibString.getString(emptyMeshesArray[i] + 1)));
      }
    }
    if (removedPoint != null) {
      TIntHashSet doneWarning = new TIntHashSet();
      final int[] removePointArray = removedPoint.keys();
      Arrays.sort(removePointArray);
      final CtuluLog log = res.createNewLog(EfResource.getS("Point(s) confondu(s)"));
      for (int i = 0; i < removePointArray.length; i++) {
        final int pointIdx = removePointArray[i];
        final int otherPtIdx = removedPoint.get(pointIdx);
        if (!doneWarning.contains(Math.min(pointIdx, otherPtIdx))) {
          doneWarning.add(Math.min(pointIdx, otherPtIdx));
          log.addWarn(EfResource.getS("Le point {0} est confondu avec le point {1}",
              CtuluLibString.getString(pointIdx + 1),
              CtuluLibString.getString(otherPtIdx + 1))
          );
        }
      }
    }
    return res;
  }
}
