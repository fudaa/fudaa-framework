/*
 * @creation 25 oct. 06
 * @modification $Date: 2007-06-11 13:04:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluVariable;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionI.java,v 1.3 2007-06-11 13:04:06 deniger Exp $
 */
public interface EfLineIntersectionI {

  double getX();

  double getY();

  /**
   * Appele cette m�thode que si l'intersection est r�elle et si isRealIntersection() renvoie true.
   * 
   * @param _data la valeur sous forme �l�mentaire ou nodal
   * @return la valeur pour cette intersection. Interpolation si n�cessaire
   */
  double getValue(CtuluVariable _var, int _tidx);

  boolean isNodeUsed(int _idxPt);

  /**
   * @return true si l'intersection est situee sur un noeud
   */
  boolean isNodeIntersection();

  /**
   * @return true si l'intersection est situee sur une arete
   */
  boolean isEdgeIntersection();

  /**
   * @return true si l'intersection est une intersection situ�e sur un point de la ligne utilis�e pour construire le
   *         profil
   */
  boolean isAPointProfileIntersection();

  /**
   * @return indice positif si l'intersection concerne le centre d'un element
   */
  int isMeshIntersection();

  double getDistanceFrom(double _x, double _y);

  double getD2From(double _x, double _y);

  /**
   * @return true si l'intersection est bien r�elle:certaines ne sont ajout�es que pour conserv�e les points de la ligne
   *         bris�e utils�e.
   */
  boolean isRealIntersection();

  boolean isOnSameEfObject(EfLineIntersectionI _other);

}