/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.ef.operation.type;

import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;

/**
 * @author deniger
 */
public class EfOperationToT3 extends AbstractEfOperation {

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    if (super.initGrid.getGrid().getEltType() == EfElementType.T3) { return super.initGrid; }

    TIntIntHashMap eltNewIdxOldIdx = new TIntIntHashMap(super.initGrid.getGrid().getEltNb());
    final EfGridDataRenumeroteDecorator res = new EfGridDataRenumeroteDecorator(initGrid, EfLibImpl.toT3(initGrid
        .getGrid(), prog, log, eltNewIdxOldIdx));
    res.setEltNewIdxOldIdxMap(eltNewIdxOldIdx);
    super.setFrontierChanged();
    return res;
  }
}
