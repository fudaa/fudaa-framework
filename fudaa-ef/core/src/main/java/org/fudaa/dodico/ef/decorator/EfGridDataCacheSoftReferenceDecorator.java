/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.decorator;

import gnu.trove.TIntObjectHashMap;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Un decorateur permettant de cacher des donnees. Utilise des SoftReference pour les datas pour eviter d'encombrer la
 * memoire
 * 
 * @author deniger
 */
public class EfGridDataCacheSoftReferenceDecorator implements EfGridData {

  final EfGridData init_;

  int maxSize_ = 30;

  TIntObjectHashMap timeMap_ = new TIntObjectHashMap((int) (maxSize_ / 0.75));

  public EfGridDataCacheSoftReferenceDecorator(EfGridData _init) {
    super();
    init_ = _init;
  }

  @SuppressWarnings("unchecked")
  protected EfData getCachedData(CtuluVariable _o, int _timeIdx) {
    Map<CtuluVariable, SoftReference<EfData>> res = (Map<CtuluVariable, SoftReference<EfData>>) timeMap_.get(_timeIdx);
    if (res == null) return null;
    SoftReference<EfData> reference = res.get(_o);
    EfData efData = reference == null ? null : reference.get();
    return efData;

  }

  @Override
  public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
    EfData data = getCachedData(_o, _timeIdx);
    // en cache: on renvoie
    if (data != null) return data;
    data = init_.getData(_o, _timeIdx);
    if (data != null) setCachedData(_o, _timeIdx, data);
    return data;
  }

  @Override
  public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
    EfData data = getCachedData(_o, _timeIdx);
    if (data == null) {
      data = getData(_o, _timeIdx);
    }
    if (data != null) return data.getValue(_idxObjet);
    return init_.getData(_o, _timeIdx, _idxObjet);
  }

  /**
   * @return
   * @see org.fudaa.dodico.ef.EfGridData#getGrid()
   */
  @Override
  public EfGridInterface getGrid() {
    return init_.getGrid();
  }

  /**
   * @return the maxSize
   */
  public int getMaxSize() {
    return maxSize_;
  }

  /**
   * @param _var
   * @return
   * @see org.fudaa.dodico.ef.EfGridData#isDefined(org.fudaa.ctulu.CtuluVariable)
   */
  @Override
  public boolean isDefined(CtuluVariable _var) {
    return init_.isDefined(_var);
  }

  /**
   * @param _idxVar
   * @return
   * @see org.fudaa.dodico.ef.EfGridData#isElementVar(org.fudaa.ctulu.CtuluVariable)
   */
  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return init_.isElementVar(_idxVar);
  }

  @SuppressWarnings("unchecked")
  protected void setCachedData(CtuluVariable _o, int _timeIdx, EfData _data) {
    Map<CtuluVariable, SoftReference<EfData>> res = (Map<CtuluVariable, SoftReference<EfData>>) timeMap_.get(_timeIdx);
    if (res == null) {
      res = new HashMap<CtuluVariable, SoftReference<EfData>>(10);
      timeMap_.put(_timeIdx, res);
    }
    res.put(_o, new SoftReference<EfData>(_data));

  }

  /**
   * La taille max du nombre de pas de pour lesquels on veut cacher les donnees
   * 
   * @param _maxSize the maxSize to set
   */
  public void setMaxSize(int _maxSize) {
    maxSize_ = _maxSize;
  }

}
