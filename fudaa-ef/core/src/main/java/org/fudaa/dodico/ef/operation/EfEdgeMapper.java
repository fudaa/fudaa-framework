package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.EfNodeComparator;
import org.fudaa.dodico.ef.EfSegment;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class EfEdgeMapper<T> {
  public static final double DEFAULT_SEGMENT_MAX_DISTANCE = 1e-3;
  private final TreeMap<EfNode, T> objectByMiddleNode;
  private final Map<EfSegment, T> objectBySegment;
  private final double segmentMaxDistance;
  private final EfGridInterface gridInterface;

  public EfEdgeMapper(EfGridInterface gridInterface) {
    this(gridInterface, EfNodeComparator.DEFAULT_EPSILON, DEFAULT_SEGMENT_MAX_DISTANCE);
  }

  public EfEdgeMapper(EfGridInterface gridInterface, double pointDistanceEpsilon, double segmentDistanceEpsilon) {
    this.gridInterface = gridInterface;
    objectByMiddleNode = new TreeMap<>(new EfNodeComparator(pointDistanceEpsilon));
    this.segmentMaxDistance = segmentDistanceEpsilon;
    objectBySegment = new HashMap<>();
  }

  public void addEdge(int pt1, int pt2, T object) {
    final EfNode efNode = gridInterface.getMilieu(pt1, pt2);
    objectByMiddleNode.put(efNode, object);
    EfSegment segment = new EfSegment(pt1, pt2);
    objectBySegment.put(segment, object);
  }

  public T getObject(double x, double y) {
    EfNode node = new EfNode(x, y, 0);
    T t = objectByMiddleNode.get(node);
    if (t == null) {
      t = findFromSegment(node);
    }
    return t;
  }

  private T findFromSegment(EfNode node) {
    T found = null;
    double minDistance = segmentMaxDistance;
    for (Map.Entry<EfSegment, T> efSegmentTEntry : objectBySegment.entrySet()) {
      final int pt1Idx = efSegmentTEntry.getKey().getPt1Idx();
      final int pt2Idx = efSegmentTEntry.getKey().getPt2Idx();
      double distance = CtuluLibGeometrie.distanceFromSegment(
          gridInterface.getPtX(pt1Idx), gridInterface.getPtY(pt1Idx),
          gridInterface.getPtX(pt2Idx), gridInterface.getPtY(pt2Idx),
          node.getX(), node.getY()
      );
      if (distance < minDistance) {
        found = efSegmentTEntry.getValue();
        minDistance = distance;
      }
    }
    return found;
  }
}
