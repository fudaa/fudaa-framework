package org.fudaa.dodico.ef.operation.clean;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;

import java.util.Arrays;

public class EfOperationCleanGrid extends AbstractEfOperation {
  /**
   * Contient la distance minimale.
   */
  private double minDistance;

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    int[] ptsOldIdxNewIdx = this.getPtsOldIdxNewIdx(prog);
    int[] eltsOldIdxNewIdx = this.getEltsOldIdxNewIdx(ptsOldIdxNewIdx, prog);
    TIntIntHashMap ptsNewIdxOldIdx = this.getNewIdxOldIdx(ptsOldIdxNewIdx);
    TIntIntHashMap eltsNewIdxOldIdx = this.getNewIdxOldIdx(eltsOldIdxNewIdx);
    EfGridInterface grid = this.initGrid.getGrid();
    EfNode[] nodes = grid.getNodes();
    EfElement[] elements = grid.getElts();
    EfNode[] newNodes = new EfNode[ptsNewIdxOldIdx.size()];
    EfElement[] newElements = new EfElement[eltsNewIdxOldIdx.size()];
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly(Messages.getString("ef.cleanGrid.msg"));
    updater.setValue(10, newNodes.length + newElements.length);

    for (int i = 0; i < newNodes.length; i++) {
      newNodes[i] = nodes[ptsNewIdxOldIdx.get(i)];

      updater.majAvancement();
    }

    for (int i = 0; i < newElements.length; i++) {
      newElements[i] = new EfElement(this.getPtsNewIdx(elements[eltsNewIdxOldIdx.get(i)].getIndices(), ptsOldIdxNewIdx));

      updater.majAvancement();
    }

    if (newNodes.length == 0) {
      log.addError("ef.cleanGrid.allNode.eliminated");
      return null;
    }
    EfGridInterface newGrid = new EfGrid(newNodes, newElements);

    EfGridDataRenumeroteDecorator renumeroteDecorator = new EfGridDataRenumeroteDecorator(super.initGrid, newGrid);
    renumeroteDecorator.setPtNewIdxOldIdxMap(ptsNewIdxOldIdx);
    renumeroteDecorator.setEltNewIdxOldIdxMap(eltsNewIdxOldIdx);

    if (log != null) {
      log.addInfo("ef.cleanGrid.node.elimine.log", nodes.length - newNodes.length);
      log.addInfo("ef.cleanGrid.mesh.elimine.log", elements.length - newElements.length);
      log.addInfo("ef.operation.log.newPt.nb", newNodes.length);
      log.addInfo("ef.operation.log.newEle.nb", newElements.length);
    }

    return renumeroteDecorator;
  }

  /**
   * @param ptsOldIdx les anciens indices dont on veut les nouveaux
   * @param ptsOldIdxNewIdx un tableau contenant pour chaque indice la valeur du nouvel indice ou -1 si le point est
   *     supprim�.
   * @return un tableau contenant les nouveaux indices pour ceux demand�s.
   */
  private int[] getPtsNewIdx(int[] ptsOldIdx, int[] ptsOldIdxNewIdx) {
    int[] ptsNewIdx = new int[ptsOldIdx.length];

    for (int i = 0; i < ptsNewIdx.length; i++) {
      ptsNewIdx[i] = ptsOldIdxNewIdx[ptsOldIdx[i]];
    }
    //in this case, an element with 4 or more points can be transformed to a smaller
    if (ptsNewIdx.length > 3 && isDegenerated(ptsNewIdx)) {
      TIntArrayList newIdx = new TIntArrayList(ptsNewIdx.length);
      int firstIdx = ptsNewIdx[0];
      int previousIdx = ptsNewIdx[0];
      newIdx.add(previousIdx);
      for (int i = 1; i < ptsNewIdx.length; i++) {
        int currentIdx = ptsNewIdx[i];
        //the index could be added if it's not the same than previous index
        boolean isSameThanLast = currentIdx == previousIdx;

        //for the last one, should be also different than the first one
        boolean isLastAndSameThanFilrst = (i == ptsNewIdx.length - 1) && currentIdx == firstIdx;

        if (!isSameThanLast && !isLastAndSameThanFilrst) {
          newIdx.add(currentIdx);
        }
        previousIdx = currentIdx;
      }
      ptsNewIdx = newIdx.toNativeArray();
    }

    return ptsNewIdx;
  }

  /**
   * @param prog le manager de progression.
   * @return un tableau contenant pour chaque indice la valeur du nouvel indice ou -1 si l'�l�ment est supprim�.
   */
  private int[] getEltsOldIdxNewIdx(int[] ptsOldIdxNewIdx, ProgressionInterface prog) {
    EfGridInterface grid = this.initGrid.getGrid();
    int[] eltOldIdxNewIdx = new int[grid.getEltNb()];
    EfElement[] elements = grid.getElts();
    ProgressionUpdater updater = new ProgressionUpdater(prog);
    int currentIdx = 0;

    updater.majProgessionStateOnly(Messages.getString("ef.cleanGrid.getEltsOldIdxNewIdx.msg"));
    updater.setValue(10, eltOldIdxNewIdx.length);

    Arrays.fill(eltOldIdxNewIdx, -1);

    for (int i = 0; i < eltOldIdxNewIdx.length; i++) {
      if (this.stop) {
        return new int[0];
      }

      int[] ptsNewIdx = this.getPtsNewIdx(elements[i].getIndices(), ptsOldIdxNewIdx);

      if (!this.isDegenerated(ptsNewIdx)) {
        eltOldIdxNewIdx[i] = currentIdx;

        for (int j = 0; j < i; j++) {
          if (eltOldIdxNewIdx[j] == -1) {
            if (this.isSuperposed(ptsNewIdx, this.getPtsNewIdx(elements[j].getIndices(), ptsOldIdxNewIdx))) {
              eltOldIdxNewIdx[i] = eltOldIdxNewIdx[j];
            }
          }
        }

        currentIdx++;
      } else {
        int[] ptsIdx = elements[i].getIndices();

        // Si l'�l�ment est d�g�ner�, on teste tout ces points pour �liminer sont qui ne sont utilis�s par aucun autre
        // �l�ment.
        for (int j = 0; j < ptsIdx.length; j++) {
          //we get the meshed using this idx in the old grid
          if (grid.getEltIdxContainsPtIdx(ptsIdx[j]).length < 2) {
            int newPtIdx = ptsOldIdxNewIdx[ptsIdx[j]];

            if (newPtIdx != -1) {
              //If this point replace another, we have to check that the other one is not used:
              boolean shouldClearPtIdx = true;
              for (int k = ptsIdx[j] + 1; k < ptsOldIdxNewIdx.length; k++) {
                if (ptsOldIdxNewIdx[k] == newPtIdx) {
                  if (grid.getEltIdxContainsPtIdx(k).length >= 2) {
                    shouldClearPtIdx = false;
                    break;
                  }
                }
              }
              if (shouldClearPtIdx) {
                for (int k = ptsIdx[j]; k < ptsOldIdxNewIdx.length; k++) {
                  if (ptsOldIdxNewIdx[k] == newPtIdx) {
                    ptsOldIdxNewIdx[k] = -1;
                  } else if (ptsOldIdxNewIdx[k] > newPtIdx) {
                    ptsOldIdxNewIdx[k]--;
                  }
                }
              }
            }
          }
        }
      }

      updater.majAvancement();
    }

    return eltOldIdxNewIdx;
  }

  /**
   * @param oldIdxNewIdx un tableau contenant pour chaque indice la valeur du nouvel indice ou -1 si le point/�l�ment
   *     est supprim�.
   * @return une map contant pour un cl� repr�sentant le nouvel indice, la valeur de l'ancien indice.
   */
  private TIntIntHashMap getNewIdxOldIdx(int[] oldIdxNewIdx) {
    TIntIntHashMap newIdxOldIdx = new TIntIntHashMap(oldIdxNewIdx.length);

    for (int i = 0; i < oldIdxNewIdx.length; i++) {
      if (oldIdxNewIdx[i] != -1) {
        if (!newIdxOldIdx.containsKey(oldIdxNewIdx[i])) {
          newIdxOldIdx.put(oldIdxNewIdx[i], i);
        }
      }
    }

    return newIdxOldIdx;
  }

  /**
   * @param prog le manager de progression.
   * @return un tableau contenant pour chaque indice la valeur du nouvel indice ou -1 si le point est supprim�.
   */
  private int[] getPtsOldIdxNewIdx(ProgressionInterface prog) {
    EfGridInterface grid = this.initGrid.getGrid();
    int[] oldIdxNewIdx = new int[grid.getPtsNb()];
    EfElement[] elements = grid.getElts();
    EfNode[] nodes = grid.getNodes();
    int currentIdx = 0;
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.setValue(10, elements.length + (oldIdxNewIdx.length * (oldIdxNewIdx.length - 1)) / 2);
    updater.majProgessionStateOnly(Messages.getString("ef.cleanGrid.getPtsOldIdxNewIdx.msg"));

    Arrays.fill(oldIdxNewIdx, -1);
    double minDistanceCarre = minDistance * minDistance;

    for (int i = 0; i < elements.length; i++) {
      if (this.stop) {
        return new int[0];
      }

      int nb = elements[i].getPtNb();
      for (int j = 0; j < nb; j++) {
        int idxPt = elements[i].getPtIndex(j);
        oldIdxNewIdx[idxPt] = idxPt;
      }
      updater.majAvancement();
    }

    for (int i = 0; i < oldIdxNewIdx.length; i++) {
      if (this.stop) {
        return new int[0];
      }

      if (oldIdxNewIdx[i] == i) {
        oldIdxNewIdx[i] = currentIdx;
        final double x = nodes[i].getX();
        final double y = nodes[i].getY();
        for (int j = (i + 1); j < oldIdxNewIdx.length; j++) {
          if (this.stop) {
            return new int[0];
          }

          if (oldIdxNewIdx[j] == j && Math.abs(x - nodes[j].getX()) <= minDistance) {
            double distance = CtuluLibGeometrie.getD2(x, y, nodes[j].getX(), nodes[j]
                .getY());

            if (distance < minDistanceCarre) {
              oldIdxNewIdx[j] = currentIdx;
            }
          }
          updater.majAvancement();
        }

        currentIdx++;
      }
    }

    return oldIdxNewIdx;
  }

  /**
   * @param idx1 les indices d'un �l�ment.
   * @param idx2 les indices d'un autre �l�ment.
   * @return true si les 2 �l�ments sont superpos�s.
   */
  private boolean isSuperposed(int[] idx1, int[] idx2) {
    boolean isSuperposed = true;
    int i = 0;

    while (isSuperposed && (i < idx1.length)) {
      for (int j = 0; j < idx2.length; j++) {
        isSuperposed = (idx1[i] == idx2[j]);

        if (isSuperposed) {
          break;
        }
      }

      i++;
    }

    return isSuperposed;
  }

  /**
   * @param idx les indices d'un �l�ment.
   * @return true si l'�l�ment est d�g�n�r�.
   */
  private boolean isDegenerated(int[] idx) {
    if (idx.length < 3) {
      return true;
    }
    for (int i = 0; i < (idx.length - 1); i++) {
      if (idx[i] < 0) {
        return true;
      }
      for (int j = (i + 1); j < idx.length; j++) {
        if (idx[i] == idx[j]) {
          return true;
        }
      }
    }

    return false;
  }

  public void setMinDistance(double minDistance) {
    this.minDistance = minDistance;
  }
}
