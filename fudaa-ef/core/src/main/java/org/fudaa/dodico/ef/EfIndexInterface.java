/*
 * @creation 17 janv. 07
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntHashSet;

/**
 * @author fred deniger
 * @version $Id: EfIndexInterface.java,v 1.1 2007-01-19 13:07:19 deniger Exp $
 */
public interface EfIndexInterface {

    int getNbEnv();

    Envelope getEnvelope(final int _i);

    Envelope[] getEnvs();

    /**
     *
     * @param _i indice of the envelope
     * @param _set the set that will contain all the indice of the meshes contained
     * by the envelope _i.
     */
    void fill(final int _i, final TIntHashSet _set);

    /**
     *
     * @param _searchEnv
     * @return index of meshes contained by this searchEnv.
     */
    int[] query(final Envelope _searchEnv);

    void query(final Envelope _searchEnv, final EfIndexVisitor _visitor);
}
