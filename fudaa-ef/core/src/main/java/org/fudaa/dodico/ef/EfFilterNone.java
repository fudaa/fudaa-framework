/*
 * @creation 1 f�vr. 07
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

/**
 * Un filtre vide: tous les elements/ noeuds sont selectionnes.
 * 
 * @author fred deniger
 * @version $Id: EfFilterNone.java,v 1.2 2007-05-04 13:45:58 deniger Exp $
 */
public class EfFilterNone implements EfFilter {

  @Override
  public boolean isActivated(final int _idxPt) {
    return true;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt) {
    return true;
  }

  @Override
  public boolean isActivatedElt(int idxElt, boolean strict) {
    return true;
  }

}
