/*
 * @creation 4 juin 07
 * @modification $Date: 2007-06-28 09:25:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateArrays;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.operation.linemerge.LineMerger;
import org.locationtech.jts.operation.polygonize.Polygonizer;
import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.GISGeometry;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygoneWithHole;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

/**
 * @author fred deniger
 * @version $Id: EfIsoActivitySearcher.java,v 1.2 2007-06-28 09:25:08 deniger Exp $
 */
public class EfIsoActivitySearcher implements CtuluActivity {

  class ArrayListWithIndex extends ArrayList {

    int idx_ = -1;

    public ArrayListWithIndex() {
      super();
    }

    public ArrayListWithIndex(Collection _c) {
      super(_c);
    }

    public ArrayListWithIndex(int _initialCapacity) {
      super(_initialCapacity);
    }
    
    @Override
    public boolean add(Object e) {
      return super.add(e);
    }

    public int getIdx() {
      return idx_;
    }

    public void setIdx(int _idx) {
      idx_ = _idx;
    }

  }
  class TempResult implements EfLineIntersectionParent {

    private final List availableStrings_ = new ArrayList(10);
    private final BitSet done_ = new BitSet(gridToUse_.getEltNb());
    private final boolean isInitDataOnElt_;
    private final EfData nodeData_;
    private final BitSet plat_;
    private final List strings_ = new ArrayList(50);
    private final double v_;

    // on part pour une pr�cision au centim�tre pr�s
    final double epsForAlign_ = 1E-2;

    Coordinate tmpA_ = new Coordinate();

    Coordinate tmpB_ = new Coordinate();

    Coordinate tmpPt_ = new Coordinate();

    public TempResult(BitSet _plat, double _v, EfData _node, boolean _isInitDataOnElt) {
      plat_ = _plat;
      v_ = _v;
      nodeData_ = _node;
      isInitDataOnElt_ = _isInitDataOnElt;
    }

    private void addGeomForSingleInter(List _l, Polygonizer _dest) {
      if (_l.size() == 1 && isInitDataOnElt_) {
        // que faire un point est situ� dans l'iso

      }
    }

    private void addInMerger(Collection _otherLines, LineMerger _merger) {
      if (_otherLines != null && _otherLines.size() > 0) {
        for (Iterator it = _otherLines.iterator(); it.hasNext();) {
          _merger.add((Geometry) it.next());
        }
      }
    }

    private GISGeometry createLine(List _line) {
      final int size = _line.size();
      List coordinatesList = new ArrayList(size);
      // le maillage transform�
      // boolean gridTransformed = isInitDataOnElt_;
      for (int i = 0; i < size; i++) {
        EfLineIntersection inter = (EfLineIntersection) _line.get(i);
        inter.setParent(this);
        coordinatesList.add(new Coordinate(inter.getX(), inter.getY(), 0));
      }
      Coordinate[] coordinates = CoordinateArrays.removeRepeatedPoints((Coordinate[]) coordinatesList
          .toArray(new Coordinate[coordinatesList.size()]));

      if (coordinates != null && coordinates.length >= 2) {
        return (GISGeometry) GISLib.createLineOrLinearStrings(coordinates);
      }
      return null;
    }

    private boolean isAligned(EfLineIntersection _newInter, List _dest) {
      boolean align = false;
//      if (_dest.size() >= 2) {
//        EfLineIntersection last = (EfLineIntersection) _dest.get(_dest.size() - 1);
//        // on initialise le parent qui est utilise pour r�cuperer les coordonn�es
//        _newInter.setParent(this);
//        last.setParent(this);
//        tmpPt_.x = last.getX();
//        tmpPt_.y = last.getY();
//        tmpB_.x = _newInter.getX();
//        tmpB_.y = _newInter.getY();
//        last = (EfLineIntersection) _dest.get(_dest.size() - 2);
//        last.setParent(this);
//        tmpA_.x = last.getX();
//        tmpA_.y = last.getY();
//        // le nouveau point est situ� sur la droite en cours:
//        align = (CGAlgorithms.distancePointLinePerpendicular(tmpPt_, tmpA_, tmpB_) <= epsForAlign_);
//      }
      return align;
    }

    /**
     * Utiliser cette m�thode pour enlever une ligne tempo: on veut r�ellement comparar des pointeurs.
     * 
     * @param _l la liste strings_ ou availables
     * @param _o l'objet a enlever
     */
    private void removeInList(List _l, ArrayListWithIndex _o) {
      int idx = CtuluLibArray.findObjectEgalEgal(_l, _o);
      if (idx >= 0) _l.remove(idx);
    }

    void addClosedLine(ArrayListWithIndex _l) {
      strings_.add(_l);
    }

    void addList(ArrayListWithIndex _list) {
      if (CtuluLibArray.findObjectEgalEgal(strings_, _list) < 0) {
        strings_.add(_list);
      } else {
        new Throwable().printStackTrace();
      }
      if (CtuluLibArray.findObjectEgalEgal(availableStrings_, _list) < 0) {
        availableStrings_.add(_list);
      } else {
        new Throwable().printStackTrace();
      }

    }

    ArrayListWithIndex addList(int _idxElt) {
      final ArrayListWithIndex res = new ArrayListWithIndex();
      res.setIdx(_idxElt);
      addList(res);
      return res;

    }

    boolean isAddedPoint(EfLineIntersection _inter) {
      return isInitDataOnElt_ && _inter.isNodeIntersection()
          && gridUsedToEltData_.getNewGrid().isPtAddedForEltCenter(getPtIdx(_inter));
    }

    boolean isOldPoint(EfLineIntersection _inter) {
      if (_inter.isNodeIntersection()) {
        return isInitDataOnElt_ && getPtIdx(_inter) < grid_.getPtsNb();
      }
      return false;
    }

    /**
     * Permet de tester si la nouvelle intersection est sur la droite en cours ou non. Si oui, le nouveau point remplace
     * l'avant dernier point: cela �vite d'ajouter des points pour rien. Dans le cas ou les donn�es sont d�finies sur
     * les �l�ments, on teste �galement l'alternance entre ancien point et nouveau point (centre d'un �l�ment).
     * 
     * @param _newInter la nouvelle intersection � ajouter
     * @param _dest la liste
     */
    protected void addPointInLine(EfLineIntersection _newInter, List _dest) {
      if (!isAligned(_newInter, _dest)) {
        _dest.add(_newInter);
      } else {
        _dest.set(_dest.size() - 1, _newInter);
      }

    }

    @Override
    public void buildNeighbor(ProgressionInterface _prog) {}

    public void finishAll(EfIsoResultInterface _res, ProgressionUpdater _up) {
      if (strings_.size() == 0) return;
      _up.setValue(4, 4);
      _up.majProgessionStateOnly(DodicoLib.getS("Construction des isolignes"));
      Polygonizer polygonizer = new Polygonizer();
      for (int i = strings_.size() - 1; i >= 0; i--) {
        List list = (List) strings_.get(i);
        int size = list.size();
        // une seule coordonn�e bizarre...
        if (size == 1) {
          addGeomForSingleInter(list, polygonizer);
          // ligne ferm�e: on l'ajoute en l'�tat
        } else if (size >= 2) {
          GISGeometry createLine = createLine(list);
          if (createLine != null) {
            if (size > 3
                && ((EfLineIntersection) list.get(0)).isOnSameEfObject((EfLineIntersection) list.get(size - 1))) {
              _res.geometryFound(createLine, v_);
            } else {
              polygonizer.add((Geometry) createLine);
            }
          }
        }
      }
      _up.majAvancement();
      // des iso compliqu�s ont �t� trouv�es
      // on recherches les lignes ferm�es
      for (Iterator it = polygonizer.getPolygons().iterator(); it.hasNext();) {
        GISPolygoneWithHole res = (GISPolygoneWithHole) it.next();
        // on ajoute la ligne fermee exterieure
        _res.geometryFound((GISGeometry) res.getExteriorRing(), v_);
        // on ajoute les lignes interieures
        for (int i = 0; i < res.getNumInteriorRing(); i++) {
          _res.geometryFound((GISGeometry) res.getInteriorRingN(i), v_);
        }
      }
      _up.majAvancement();
      // les segments qui ne forment pas de pol
      Collection otherLines = polygonizer.getInvalidRingLines();
      otherLines = polygonizer.getDangles();
      LineMerger merger = new LineMerger();
      addInMerger(otherLines, merger);
      otherLines = polygonizer.getCutEdges();
      addInMerger(otherLines, merger);
      for (Iterator it = merger.getMergedLineStrings().iterator(); it.hasNext();) {
        _res.geometryFound((GISGeometry) it.next(), v_);

      }
      _up.majAvancement();
    }

    public ArrayListWithIndex[] getAvailable() {
      return (ArrayListWithIndex[]) availableStrings_.toArray(new ArrayListWithIndex[availableStrings_.size()]);
    }

    @Override
    public EfGridInterface getGrid() {
      return EfIsoActivitySearcher.this.gridToUse_;
    }

    @Override
    public EfGridDataInterpolator getGridData() {
      throw new IllegalArgumentException("do not use");
    }

    public int getNbAvailables() {
      return availableStrings_.size();
    }

    public int getNbLinesAdded() {
      return strings_.size();
    }

    @Override
    public EfNeighborMesh getNeighbor() {
      return EfIsoActivitySearcher.this.neighbor_;
    }

    public EfData getNodeData() {
      return nodeData_;
    }

    public ArrayListWithIndex getSingleList(int _idxElt) {
      ArrayListWithIndex res = null;
      for (int i = 0; i < availableStrings_.size(); i++) {
        ArrayListWithIndex listi = (ArrayListWithIndex) availableStrings_.get(i);
        if (listi.getIdx() == _idxElt) {
          if (res == null) res = listi;
          else
            return null;
        }
      }
      return res;

    }

    public double getV() {
      return v_;
    }

    /**
     * Permet d'inverser de r�activer la premiere ligne.
     * 
     * @param _idxElt l'indice de l'�l�m�nt a utiliser pour la r�activation
     */
    public ArrayListWithIndex inverseFirst(ArrayListWithIndex _l, int _idxElt) {
      Collections.reverse(_l);
      updateList(_l, _idxElt);
      return _l;
    }

    public boolean isAvailable(List _l) {
      return availableStrings_.contains(_l);
    }

    /*
     * public boolean isEdgeDone(int _pt1, int _pt2) { tmp_.setMinMaxIdx(_pt1, _pt2); return edgeDone_.contains(tmp_); }
     */

    public boolean isDone(int _idxElt) {
      return done_.get(_idxElt);
    }

    public boolean isEltOk(int _idxElt) {
      return !isDone(_idxElt) && !isPlat(_idxElt);
    }

    public boolean isPlat(int _idxElt) {
      return plat_.get(_idxElt);
    }

    public Iterator iteratorOnAvail(int _idxElt) {
      ArrayList l = new ArrayList();
      for (int i = 0; i < availableStrings_.size(); i++) {
        ArrayListWithIndex listi = (ArrayListWithIndex) availableStrings_.get(i);
        if (listi.getIdx() == _idxElt) l.add(listi);
      }
      return l.iterator();
    }

    public void removeTmp(ArrayListWithIndex _line) {
      removeInList(strings_, _line);
      removeInList(availableStrings_, _line);
    }

    public void setClosed(ArrayListWithIndex _l) {
      removeInList(availableStrings_, _l);
      _l.setIdx(-1);
    }

    public void setDone(int _idxElt) {
      done_.set(_idxElt);
    }

    public void setFinished(ArrayListWithIndex _line, EfIsoResultInterface _res) {
      setClosed(_line);
      removeInList(strings_, _line);
      if (_line.size() > 1) {
        GISGeometry createLine = createLine(_line);
        if (createLine != null) _res.geometryFound(createLine, v_);
      }
    }

    /*
     * public void setEdgeDone(int _pt1, int _pt2) { edgeDone_.add(new EfSegment(Math.min(_pt1, _pt2), Math.max(_pt1,
     * _pt2))); }
     */

    public void updateList(ArrayListWithIndex _l, int _idxElt) {
      if (_l == null || _idxElt < 0) return;
      _l.setIdx(_idxElt);
    }
  }

  public EfIsoActivitySearcher(final EfData _data, final EfGridInterface _grid, final EfGridInterface _gridToUse,
      final EfIsoRestructuredGridResult _gridUsedToEltData, final EfNeighborMesh _neighbor, double _epsForValues,
      final CtuluListSelectionInterface _filterToUseToAvoid) {
    super();
    data_ = _data;
    grid_ = _grid;
    gridToUse_ = _gridToUse;
    gridUsedToEltData_ = _gridUsedToEltData;
    neighbor_ = _neighbor;
    epsForValues_ = _epsForValues;
    filterToUseToAvoid_ = _filterToUseToAvoid;
  }

  public static int getPtIdx(EfLineIntersection _inter) {
    return ((EfLineIntersection.ItemNode) _inter).ptIdx_;
  }

  double epsForValues_ = 1E-15;

  final CtuluListSelectionInterface filterToUseToAvoid_;

  final EfGridInterface grid_;
  final EfGridInterface gridToUse_;
  final EfIsoRestructuredGridResult gridUsedToEltData_;
  final EfNeighborMesh neighbor_;

  boolean stop_;

  private boolean closeClosedLine(TempResult _stockage, ArrayListWithIndex _current) {
    if (_current.size() > 3
        && ((EfLineIntersection) _current.get(0)).isOnSameEfObject((EfLineIntersection) _current
            .get(_current.size() - 1))) {
      _stockage.setClosed(_current);
      return true;
    }
    return false;
  }

  private BitSet createPlatBitSet(EfData _nodeData, final int _nbElt) {
    BitSet plat = new BitSet(_nbElt);
    if (filterToUseToAvoid_ != null) {
      for (int i = 0; i < _nbElt; i++) {
        if (filterToUseToAvoid_.isSelected(i)) {
          plat.set(i);
        }
      }
    }
    for (int i = 0; i < _nbElt; i++) {
      if (!plat.get(i) && EfLib.isPlat(gridToUse_.getElement(i), _nodeData, epsForValues_)) {
        plat.set(i);
      }
    }
    return plat;
  }

  private EfLineIntersection.ItemEdge findIntersectionOnEdge(double _value, int _edgePt1, int _edgePt2, double _v1,
      double _v2) throws IllegalAccessError {
    // ce cas ne doit pas arriver car cela voudrait dire que _value vaut v1 et _value vaut v2
    // et dans ce cas foundOnPt doit etre � true
    if (_v2 == _v1) {
      throw new IllegalAccessError("Error while finding intersection in mesh for value " + _value);
    }
    double alpha = (_value - _v1) / (_v2 - _v1);
    double x = gridToUse_.getPtX(_edgePt1) + alpha * (gridToUse_.getPtX(_edgePt2) - gridToUse_.getPtX(_edgePt1));
    double y = gridToUse_.getPtY(_edgePt1) + alpha * (gridToUse_.getPtY(_edgePt2) - gridToUse_.getPtY(_edgePt1));
    EfLineIntersection.ItemEdge edge = new EfLineIntersection.ItemEdge(_edgePt1, _edgePt2, x, y);
    return edge;
  }

  /**
   * Permet de rechercher les intersections.
   * 
   * @param _i
   * @param _node
   * @param _toAvoid
   * @param _stockage
   */
  private int findIntersectionsInElt(int _idxElt, ArrayListWithIndex _currentString, TempResult _stockage) {
    // on parcourt les aretes
    _stockage.setDone(_idxElt);
    EfElement elt = gridToUse_.getElement(_idxElt);
    // la derniere intersection pour ne pas repartir en arriere
    ArrayListWithIndex current = _currentString;
    boolean isFirstTime = _currentString == null;
    final EfData nodeData = _stockage.getNodeData();
    boolean isEltPlatForThisValue = isEltPlatForValue(nodeData, _idxElt, _stockage);
    EfLineIntersection toAvoid = CtuluLibArray.isEmpty(current) ? null : (EfLineIntersection) current.get(current
        .size() - 1);

    int nbFound = 0;
    final double value = _stockage.getV();
    int nbFoundMaxValue = isFirstTime ? 2 : 1;

    for (int i = 0; i < elt.getNbEdge(); i++) {
      int edgePt1 = elt.getEdgePt1(i);
      int edgePt2 = elt.getEdgePt2(i);

      // on teste le point 1 uniquement
      if (!isEltPlatForThisValue) {
        EfLineIntersection inter = isNodeEquals(nodeData, toAvoid, edgePt1, _stockage, current);
        if (inter != null) {
          if (current == null) current = _stockage.addList(_idxElt);
          _stockage.addPointInLine(inter, current);
          nbFound++;

          if (toAvoid != null && toAvoid.isNodeIntersection()) {
            int otherElt = neighbor_.getAdjacentMeshes(getPtIdx(inter), getPtIdx(toAvoid), _idxElt);
            if (otherElt >= 0) {
              _stockage.setDone(otherElt);
            }
          }
          // on arrete si nombre max trouve
          if (nbFound == nbFoundMaxValue) break;
        }
      }

      // si l'intersection n'est pas trouv�e sur les noeuds:
      // on va rechercher sur l'arete
      if (isEltPlatForThisValue || (!isSameValue(nodeData, value, edgePt1) && !isSameValue(nodeData, value, edgePt2))) {
        double v1 = nodeData.getValue(edgePt1);
        double v2 = nodeData.getValue(edgePt2);

        // si true la valeur est bien situ�e entre les points
        if ((value - v1) * (value - v2) < 0) {
          EfLineIntersection.ItemEdge edge = findIntersectionOnEdge(value, edgePt1, edgePt2, v1, v2);
          if (!edge.isOnSameEfObject(toAvoid)) {
            if (current == null) current = _stockage.addList(_idxElt);
            _stockage.addPointInLine(edge, current);
            nbFound++;
            // on arrete si nombre max trouve
            if (nbFound == nbFoundMaxValue) break;
          }

        }
      }

    }
    // buildResultFromIntersectionInMesh(_idxElt, _stockage, current, isFirstTime, nbFound, value);
    if (nbFound > 0) {
      _stockage.updateList(current, _idxElt);
      // pas d'intersection trouv�e
      if (!isFirstTime) {
        if (nbFound > 1) FuLog.error("DEF: several nodes found in a triangle T3");
        // on ferme si c'est une ligne ferm�e
        closeClosedLine(_stockage, current);
      }
    }
    return nbFound;
  }

  private boolean findNext(TempResult _stockage, ArrayListWithIndex _enCours) {
    EfLineIntersection lastInter = (EfLineIntersection) _enCours.get(_enCours.size() - 1);
    // l'intersection est sur une arete
    // dans ce cas, il faut trouver l'�l�ment adjacent
    if (lastInter.isEdgeIntersection()) {
      return findNextsForEdgeIntersection(_stockage, _enCours, lastInter);
      // dans le cadre d'un noeud, on le fait pour tous les �l�ments voisins
      // si les valeurs sont d�finies sur les �l�ments et que seules la valeurs
      // milieux corresponds
    } else if (lastInter.isNodeIntersection()) return findNextForNodeIntersection(_stockage, _enCours, lastInter);
    return false;
  }

  private boolean findNextForNodeIntersection(TempResult _stockage, ArrayListWithIndex _enCours,
      EfLineIntersection _lastInter) {
    // neighbors for this element
    int idxElt = _enCours.getIdx();
    int idxPt = getPtIdx(_lastInter);
    // dans le cas d'un r�sultat sur les �l�ments, on parcourt tous les �l�ments voisins.
    // le nombre d'intersection trouv�e
    int nbFoundTotal = 0;
    // pas de voisins ! bizarre
    int nbNeighbor = neighbor_.getNbNeighborMeshes(idxPt);

    // true si point pivot
    boolean onlyOne = _enCours.size() == 1;

    ArrayListWithIndex tmpString = new ArrayListWithIndex();
    tmpString.setIdx(idxElt);
    tmpString.add(_lastInter);
    List newBranches = new ArrayList(nbNeighbor);
    for (int i = 0; i < nbNeighbor; i++) {
      int nexElt = neighbor_.getNeighborMesh(idxPt, i);
      if (nexElt >= 0 && nexElt != idxElt && _stockage.isEltOk(nexElt)) {
        // tmpString.setIdx(nexElt);
        int nbFound = findIntersectionsInElt(nexElt, tmpString, _stockage);
        if (nbFound > 0) {
          nbFoundTotal += nbFound;
          _stockage.addList(tmpString);
          newBranches.add(tmpString);
          tmpString = new ArrayListWithIndex();
          tmpString.setIdx(idxElt);
          tmpString.add(_lastInter);

        }
      }
    }
    boolean ok = nbFoundTotal == 1 || (onlyOne && nbFoundTotal == 2);
    if (nbFoundTotal == 1) {
      ArrayListWithIndex first = (ArrayListWithIndex) newBranches.get(0);
      _stockage.removeTmp(first);
      _enCours.setIdx(first.getIdx());
      _stockage.addPointInLine((EfLineIntersection) first.get(1), _enCours);
    } else if (onlyOne && nbFoundTotal == 2) {
      ArrayListWithIndex first = (ArrayListWithIndex) newBranches.get(0);
      ArrayListWithIndex sec = (ArrayListWithIndex) newBranches.get(1);
      _stockage.removeTmp(first);
      _stockage.removeTmp(sec);
      _enCours.clear();
      _enCours.add(first.get(1));
      _enCours.add(_lastInter);
      _stockage.addPointInLine((EfLineIntersection) sec.get(1), _enCours);
      _enCours.setIdx(sec.getIdx());

    }

    // il y a plusieurs branches: on ferme la ligne courante et on va continuer avec les nouvelles
    if (!ok) {
      _stockage.setClosed(_enCours);
    }

    return ok;
  }
  
  private void findNexts(int _idxElt, TempResult _stockage) {
    if (_stockage.getNbAvailables() == 0) return;
    for (Iterator it = _stockage.iteratorOnAvail(_idxElt); it.hasNext();) {
      ArrayListWithIndex enCours = (ArrayListWithIndex) it.next();
      findNext(_stockage, enCours);
    }

  }

  private boolean findNextsForEdgeIntersection(TempResult _stockage, ArrayListWithIndex _enCours,
      EfLineIntersection _lastInter) {
    int idxElt = _enCours.getIdx();
    EfLineIntersection.ItemEdge edge = (EfLineIntersection.ItemEdge) _lastInter;
    int nexElt = neighbor_.getAdjacentMeshes(edge.getI1(), edge.getI2(), idxElt);
    // pas d'�l�ment voisins, on ferme la ligne
    if (nexElt < 0) {
      _stockage.setClosed(_enCours);
      return false;
    }
    if (_stockage.isEltOk(nexElt)) {
      _stockage.setDone(nexElt);
      int nbFound = findIntersectionsInElt(nexElt, _enCours, _stockage);
      // plus d'intersection dans l'�l�ment voisin: on le ferme.
      if (nbFound == 0) _stockage.setClosed(_enCours);
      // des intersections sont toujours en cours
      else {
        return true;
      }
    } else {

      boolean debut = false;
      // on revenu au d�but ?
      if (!CtuluLibArray.isEmpty(_enCours) && edge.isOnSameEfObject((EfLineIntersection) _enCours.get(0))) {
        debut = closeClosedLine(_stockage, _enCours);

      }
      // ben non.... cela peut arrive s'il y a un filtre
      if (!debut) {
        _stockage.setClosed(_enCours);
      }
    }
    return false;
  }

  private void finishAvailableLines(TempResult _interFound) {
    while (_interFound.getNbAvailables() > 0) {
      ArrayListWithIndex[] arrays = _interFound.getAvailable();
      TIntHashSet set = new TIntHashSet(arrays.length);
      for (int j = 0; j < arrays.length; j++) {
        int idx = arrays[j].getIdx();
        if (idx >= 0) set.add(idx);
        else
          _interFound.setClosed(arrays[j]);
      }
      int[] idxElt = set.toArray();
      for (int j = 0; j < idxElt.length; j++) {
        // FuLog.warning("DEF: isolines search restart for element " + idxElt[j]);
        findNexts(idxElt[j], _interFound);
      }
    }
  }

  /**
   * @param _node la valeur aux noeuds
   * @param _toAvoid l'intersection � ne pas rajouter: l'intersection en cours
   * @param _value la valeur de l'iso
   * @param _stockage la liste de stockage des intersections
   * @param _idxPt l'indice du point a tester
   * @return true si le noeuds d'indice _idxPt � la valeur _value et n'a pas d�j� �t� trait�e
   */
  private EfLineIntersection isNodeEquals(EfData _node, EfLineIntersection _toAvoid, int _idxPt, TempResult _res,
      List _current) {
    if (isSameValue(_node, _res.getV(), _idxPt)) {
      EfLineIntersection inter = new EfLineIntersection.ItemNode(_idxPt);
      if (!inter.isOnSameEfObject(_toAvoid)) {
        // dans le cadre d'une intersection sur un point, il faut �viter de revenir sur l'avant-dernier point
        // ce cas ne devrait jamais arriv� car si une arete d�fines par 2 points du maillage est trouv�e, on invalide
        // les 2 �l�ments
        // adjacents.
        if (!CtuluLibArray.isEmpty(_current) && _current.size() > 1
            && inter.isOnSameEfObject((EfLineIntersection) _current.get(_current.size() - 2))) {
          inter = null;
        }
        return inter;
      }
    }
    return null;
  }

  /**
   * Un element peut ne pas �tre plat mais �tre plat pour une valeur donn�e. par exemple -0.1, 0, 0.1 n'est pas plat �
   * 0.1 pres ( max delta =0.2) mais il le sera pour la valeur 0.
   * 
   * @param _node
   * @param _idxElt
   * @param _res
   * @return
   */
  private boolean isEltPlatForValue(EfData _node, int _idxElt, TempResult _res) {
    final EfElement elt = _res.getGrid().getElement(_idxElt);
    final double val = _res.getV();
    for (int i = elt.getPtNb() - 1; i >= 0; i--) {
      if (!isSameValue(_node, val, elt.getPtIndex(i))) return false;
    }
    return true;
  }

  private boolean isSameValue(EfData _node, double _value, int _idxPt) {
    return isEquals(_node.getValue(_idxPt), _value);
  }

  boolean isEquals(double _d1, double _d2) {
    return CtuluLib.isZero(_d1 - _d2, epsForValues_);
  }

  final EfData data_;

  /**
   * Permet de r�cup�rer les isos.<br>
   * TODO: on pourrait utiliser plusieurs threads pour accelerer le tout sur des bi-pro.
   * 
   * @param _values les valeurs � rechecher
   * @param _data les valeurs pouvant �tre d�finies aux noeuds ou sur les �l�ments
   * @param _dest le lieu de stockage des r�sultats
   * @param _prog la barre de progression
   * @return true si le calcul a ete mene au bout
   */
  public boolean search(final double _v, EfIsoResultInterface _dest, ProgressionInterface _prog, CtuluAnalyze _analyze) {
    if (data_ == null) return false;
    EfData nodeData = data_;
    if (nodeData == null) return false;
    // dans un premier temps, on recherches les �l�ments voisins.
    if (neighbor_ == null) return false;
    // on recherche les �l�ments plats: pour �viter de faire passer des isos par ces �l�ments
    final int nbElt = gridToUse_.getEltNb();
    // plat contiendra les elements plats et les elements non voulus par un �ventuel filtre
    BitSet plat = createPlatBitSet(nodeData, nbElt);
    if (stop_) return false;
    ProgressionUpdater up = new ProgressionUpdater(_prog);
    // la liste sert a stocker les intersections trouv�es par �l�ments
    // le 6 est bien sup�rieur aux intersections que l'ont trouvera en temps normal (2 max)

    // on initialise la barre de progression
    up.majProgessionStateOnly(DodicoLib.getS("Recherche des isolignes pour la valeur {0}", Double.toString(_v)));
    up.setValue(10, nbElt);
    // les �l�ments d�j� travers�s par une isolignes.
    TempResult interFound = new TempResult(plat, _v, nodeData, data_.isElementData());
    for (int idxElt = 0; idxElt < nbElt; idxElt++) {
      // on evite les �l�ments plats et les �l�ments d�j� travers�s
      if (interFound.isEltOk(idxElt)) {
        int nbLineBefore = interFound.getNbAvailables();
        int nbFound = findIntersectionsInElt(idxElt, null, interFound);
        // trouve !
        if (nbFound > 0) {
          ArrayListWithIndex list = interFound.getSingleList(idxElt);
          if (list != null) {
            boolean goOn = true;
            while (goOn) {
              goOn = findNext(interFound, list);
            }
            int nbLineAdded = interFound.getNbLinesAdded() - nbLineBefore;
            if (nbLineAdded == 1) {
              // on va recherche dans l'autre sens
              // on inverse la ligne
              ArrayListWithIndex first = interFound.inverseFirst(list, idxElt);
              goOn = true;
              while (goOn) {
                goOn = findNext(interFound, list);
              }
              nbLineAdded = interFound.getNbLinesAdded() - nbLineBefore;
              // c'est vraiment une ligne simple: pas de branches
              if (nbLineAdded == 1) {
                interFound.setFinished(first, _dest);

              }

            }
          } else
            FuLog.error("DEF: bad state");
          finishAvailableLines(interFound);
        }

      }

      up.majAvancement();
      if (stop_) return false;

    }
    if (stop_) return false;
    finishAvailableLines(interFound);
    // on finit la construction
    interFound.finishAll(_dest, up);
    if (stop_) return false;

    return true;
  }

  @Override
  public void stop() {
    stop_ = true;
  }

}
