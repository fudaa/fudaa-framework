/*
 * @creation 10 oct. 06
 * 
 * @modification $Date: 2007-04-20 16:21:08 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntHashSet;
import org.fudaa.dodico.ef.EfIndexInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;

/**
 * @author fred deniger
 * @version $Id: EfIndexRegular.java,v 1.1 2007-04-20 16:21:08 deniger Exp $
 */
public class EfIndexRegular implements EfIndexInterface {

  final Envelope[] env_;
  final int[][] idx_;

  @Override
  public int getNbEnv() {
    return env_.length;
  }

  @Override
  public Envelope getEnvelope(final int _i) {
    return env_[_i] == null ? null : new Envelope(env_[_i]);
  }

  @Override
  public Envelope[] getEnvs() {
    final Envelope[] res = new Envelope[getNbEnv()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getEnvelope(i);
    }
    return res;
  }

  @Override
  public void fill(final int _i, final TIntHashSet _set) {
    _set.addAll(idx_[_i]);
  }

  public EfIndexRegular(final Envelope[] _env, final int[][] _idx) {
    super();
    env_ = _env;
    idx_ = _idx;
    if (env_.length != idx_.length) { throw new IllegalArgumentException("arrays must have the same size !"); }
  }

  @Override
  public int[] query(final Envelope _searchEnv) {
    /**
     * the items that are matched are the items in quads which overlap the search envelope
     */
    final EfIndexVisitorHashSet visitor = new EfIndexVisitorHashSet();
    query(_searchEnv, visitor);
    return visitor.getResult();
  }

  @Override
  public void query(final Envelope _searchEnv, final EfIndexVisitor _visitor) {
    for (int i = env_.length - 1; i >= 0; i--) {
      if (env_[i].intersects(_searchEnv)) {
        final int[] idx = idx_[i];
        if (idx != null) {
          for (int k = idx.length - 1; k >= 0; k--) {
            _visitor.visitItem(idx[k]);
          }
        }

      }
    }
  }

}
