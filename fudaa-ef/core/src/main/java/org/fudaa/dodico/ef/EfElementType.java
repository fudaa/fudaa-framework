/*
 * @creation 19 ao�t 2003
 * @modification $Date: 2006-11-14 09:04:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.fudaa.dodico.commun.DodicoEnumType;

/**
 * @author deniger
 * @version $Id: EfElementType.java,v 1.9 2006-11-14 09:04:43 deniger Exp $
 */
public final class EfElementType extends DodicoEnumType {

  /**
   * le type Triangle a 3 points.
   */
  public static final EfElementType T3 = new EfElementType("T3", 3);
  public static final EfElementType T3_FOR_3D = new EfElementType("T3 3D", 6);
  /**
   * le type Triangle a 6 points (point milieu).
   */
  public static final EfElementType T6 = new EfElementType("T6", 6);

  /**
   * le type quadrilatere.
   */
  public static final EfElementType Q4 = new EfElementType("Q4", 4);
  /**
   * Le type pour rubar et refonde qui peuvent utiliser des quadrilatere et des triangles.
   */
  public static final EfElementType T3_Q4 = new EfElementType("T3_Q4", -1);
  /**
   * le type quadrilatere (avec points milieux).
   */
  public static final EfElementType Q8 = new EfElementType("Q8", 8);

  /**
   * @param _nbPoint le nombre de point de l'element a caracteriser
   * @return le type commun correspondant au nombre de point. null si le nombre n'est pas gere
   */
  public static EfElementType getCommunType(final int _nbPoint) {
    if (_nbPoint == T3.getNbPt()) {
      return T3;
    } else if (_nbPoint == T6.getNbPt()) {
      return T6;
    } else if (_nbPoint == Q4.getNbPt()) {
      return Q4;
    } else if (_nbPoint == Q8.getNbPt()) {
      return Q8;
    } else {
      return null;
    }
  }

  int nbPt_;

  /**
   * @param _id
   * @param _nom
   */
  private EfElementType(final String _nom, final int _nbPt) {
    super(_nom);
    nbPt_ = _nbPt;
  }

  public DodicoEnumType[] getArray() {
    return new EfElementType[] { Q4, Q8, T3, T3_Q4, T6 };
  }

  /**
   * @return le nombre de point attendu pour ce type
   */
  public int getNbPt() {
    return nbPt_;
  }
}