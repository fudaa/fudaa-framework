/*
 * @file EfGridSourcesAbstract.java @creation 5 f�vr. 2004 @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231
 * Compiegne @mail devel@fudaa.org
 */
package org.fudaa.dodico.ef.impl;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.EfGridSource;

/**
 * @author deniger
 * @version $Id: EfGridSourcesAbstract.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public abstract class EfGridSourcesAbstract implements EfGridSource {

  ConditionLimiteEnum[] boundaryConditions;
  Map options = new HashMap();

  @Override
  public Map getOptions() {
    return options;
  }

  @Override
  public ConditionLimiteEnum[] getBoundaryConditions() {
    return boundaryConditions;
  }

  public void setBoundaryConditions(ConditionLimiteEnum[] boundaryConditions) {
    this.boundaryConditions = boundaryConditions;
  }

  @Override
  public boolean containsNodeData() {
    return false;
  }

  @Override
  public boolean containsElementData() {
    return false;
  }

  @Override
  public int getTimeStepNb() {
    return 1;
  }

  @Override
  public boolean isElement(final int _idx) {
    return false;
  }

  @Override
  public double getTimeStep(final int _i) {
    return 0;
  }

  @Override
  public int getValueNb() {
    return 0;
  }

  @Override
  public String getValueId(final int _valueIdx) {
    return null;
  }

  @Override
  public double getValue(final int _valueIdx, final int _timeStep,
          final int _ptIdx) {
    return 0;
  }

  /**
   * @param _id l'identifiant
   * @param _s la source a parcourir
   * @return l'index trouve ou -1 si rien
   */
  public final static int getValueIndex(final String _id,
          final EfGridSource _s) {
    for (int i = _s.getValueNb() - 1; i >= 0; i--) {
      if (_id.equals(_s.getValueId(i))) {
        return i;
      }
    }
    return -1;
  }
}