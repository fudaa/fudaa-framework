/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.decorator.EfGridDataCacheOneTimeDecorator;
import org.fudaa.dodico.ef.decorator.EfGridDataCacheSoftReferenceDecorator;
import org.fudaa.dodico.ef.decorator.EfGridDataTimeDecorator;

/**
 * @author deniger
 */
public abstract class EfTrajectoireGridDataProvider {

  /**
   * Pour les lignes de courants, on utilise toujours le meme pas de temps.
   * 
   * @author deniger
   */
  public static class LigneDeCourant extends EfTrajectoireGridDataProvider {
    final EfGridData init_;

    public LigneDeCourant(int _t, EfGridData _init, CtuluVariable _vx, CtuluVariable _vy) {
      super();
      init_ = new EfGridDataCacheOneTimeDecorator(_init, _t, _vx, _vy);
    }

    @Override
    public EfGridData getDataFor(double _t) {
      return init_;
    }

    @Override
    public EfGridInterface getGrid() {
      return init_.getGrid();
    }

  }

  public static class Trajectoire extends EfTrajectoireGridDataProvider {

    final double[] initTimeStep_;
    final EfGridData init_;

    public Trajectoire(double[] _initTimeStep, EfGridData _init) {
      super();
      init_ = new EfGridDataCacheSoftReferenceDecorator(_init);
      initTimeStep_ = _initTimeStep;

    }

    @Override
    public EfGridData getDataFor(double _t) {
      return EfGridDataTimeDecorator.getTimeInterpolated(init_, initTimeStep_, _t);
    }

    @Override
    public EfGridInterface getGrid() {
      return init_.getGrid();
    }

  }

  public abstract EfGridInterface getGrid();

  public abstract EfGridData getDataFor(double _t);

}
