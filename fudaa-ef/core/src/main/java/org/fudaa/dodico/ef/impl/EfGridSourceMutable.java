/*
 * @creation 5 f�vr. 2004
 * @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;

/**
 * @author deniger
 * @version $Id: EfGridSourceMutable.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridSourceMutable implements EfGridSource {

  EfGrid g_;
  String[] valuesId_;
  String[] valuesEltId_;
  double[] timesStep_;
  double[][] values_;
  double[][] valuesElt_;
  ConditionLimiteEnum[] boundaryConditions;

  public EfGridSourceMutable() {
  }

  @Override
  public boolean isElement(final int _idx) {
    return false;
  }
  Map options = new HashMap();

  @Override
  public Map getOptions() {
    return options;
  }

  /**
   * @param _timeStep les nouveaux pas de temps
   */
  public void setTimeStep(final double[] _timeStep) {
    timesStep_ = _timeStep;
  }

  /**
   * @return true si maillage non null
   */
  public boolean isSet() {
    return g_ != null;
  }

  /**
   * Les valeurs doivent etre donnees par pas de temps. <br> double[0]=valeurs sur tous les points de la variable 0 au pas de temps 0 <br>
   * double[1]=valeurs sur tous les points de la variable 0 au pas de temps 1 <br> double[_ptId.length]=valeurs sur tous les points de la variable 1
   * au pas de temps 0 <br>
   *
   * @param _ptId les identifiants des variables
   * @param _values les valeurs
   */
  public void setPointValue(final String[] _ptId, final double[][] _values) {
    values_ = _values;
    valuesId_ = _ptId;
    if (valuesId_ == null) {
      values_ = null;
    }
    if (values_ == null) {
      valuesId_ = null;
    }
  }

  /**
   * @see #setPointValue(String[], double[][])
   * @param _eltId les identifiants des variables elementaires
   * @param _eltValues les valeurs
   */
  public void setEltValue(final String[] _eltId, final double[][] _eltValues) {
    valuesElt_ = _eltValues;
    valuesEltId_ = _eltId;
    if (valuesElt_ == null) {
      valuesEltId_ = null;
    }
    if (valuesEltId_ == null) {
      valuesElt_ = null;
    }
  }

  @Override
  public boolean containsElementData() {
    return valuesEltId_ != null;
  }

  @Override
  public boolean containsNodeData() {
    return valuesId_ != null;
  }

  public double getEltValue(final int _valueIdx, final int _eltIdx,
          final int _timeStep) {
    if (valuesElt_ != null) {
      return valuesElt_[_timeStep * valuesElt_.length + _valueIdx][_eltIdx];
    }
    return 0;
  }

  public String getEltValueId(final int _valueIdx) {
    return valuesEltId_ == null ? null : valuesEltId_[_valueIdx];
  }

  public FileFormat getFileFormat() {
    return null;
  }

  @Override
  public EfGridInterface getGrid() {
    return g_;
  }

  @Override
  public double getTimeStep(final int _i) {
    return timesStep_ == null ? 0 : timesStep_[_i];
  }

  @Override
  public int getTimeStepNb() {
    return timesStep_ == null ? 0 : timesStep_.length;
  }

  @Override
  public double getValue(final int _valueIdx, final int _timeStep,
          final int _ptIdx) {
    if (values_ != null) {
      return values_[_timeStep * values_.length + _valueIdx][_ptIdx];
    }
    return 0;
  }

  @Override
  public String getValueId(final int _valueIdx) {
    return valuesId_ == null ? null : valuesId_[_valueIdx];
  }

  @Override
  public int getValueNb() {
    return valuesId_ == null ? 0 : valuesId_.length;

  }

  public int getEltValueNb() {
    return valuesEltId_ == null ? 0 : valuesEltId_.length;

  }

  @Override
  public ConditionLimiteEnum[] getBoundaryConditions() {
    return boundaryConditions;
  }

  public void setBoundaryConditions(ConditionLimiteEnum[] boundaryConditions) {
    this.boundaryConditions = boundaryConditions;
  }
}