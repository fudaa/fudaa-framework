/*
 * @creation 13 mai 2005
 * 
 * @modification $Date: 2007-06-11 13:08:15 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.ef.decorator;

import gnu.trove.TIntIntHashMap;
import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Un decorateur pour les griddata dont la num�rotation des noeuds/�l�ments a
 * �t� modifiee. Il y a une map qui donne la correspondance entre l'indice des
 * nouveaux �l�ments/anciens �l�ments et une map pour les noeuds.
 * 
 * @author Fred Deniger
 * @version $Id: MvExportDecorator.java,v 1.6 2007-06-11 13:08:15 deniger Exp $
 */
public class EfGridDataRenumeroteDecorator extends AbstractEfGridDataDecorator {

    private TIntIntHashMap eltNewIdxOldIdx_;
    private TIntIntHashMap ptNewIdxOldIdx_;

    public EfGridDataRenumeroteDecorator(final EfGridData _src, EfGridInterface newGrid) {
        super(_src, newGrid);
    }

  @Override
    public EfData getData(final CtuluVariable _t, final int _idxTime) throws IOException {
        final EfData init = getInit().getData(_t, _idxTime);
        if (init == null) {
            return null;
        }
        final boolean isEle = init.isElementData();
        if (isEle && getEltNewIdxOldIdxMap() == null) {
            return init;
        }
        if (!isEle && getPtNewIdxOldIdxMap() == null) {
            return init;
        }
        // le type de la valeur initiale est conserv�e
        final double[] newValues = new double[isEle ? getNewGrid().getEltNb() : getNewGrid().getPtsNb()];
        for (int i = newValues.length - 1; i >= 0; i--) {
            final int oldEltIdx = getOldIdx(isEle, i);
            newValues[i] = init.getValue(oldEltIdx);
        }
        return isEle ? (EfData) new EfDataElement(newValues) : (EfData) new EfDataNode(newValues);
    }

    /**
     * @return the eltNewIdxOldIdx
     */
    public TIntIntHashMap getEltNewIdxOldIdxMap() {
        return eltNewIdxOldIdx_;
    }

  @Override
    public EfGridInterface getGrid() {
        return getNewGrid();
    }

    /**
     * @return the newGrid_
     */
    public EfGridInterface getNewGrid() {
        return super.getGrid();
    }

    /**
     * @param isEle true si on travaille sur les �l�ments
     * @return la map de correspondance
     */
    private TIntIntHashMap getNewIdxOldIdx(boolean isEle) {
        return isEle ? getEltNewIdxOldIdxMap() : getPtNewIdxOldIdxMap();
    }

    /**
     * @param isEle true si on travaille sur les �l�ments
     * @param newIdx le nouvelle indice
     * @return l'ancien indice.
     */
    private int getOldIdx(final boolean isEle, int newIdx) {
        TIntIntHashMap newOld = getNewIdxOldIdx(isEle);
        if (newOld == null || !newOld.contains(newIdx))
            return newIdx;
        return newOld.get(newIdx);
    }

    /**
     * @return the ptNewIdxOldIdx_
     */
    public TIntIntHashMap getPtNewIdxOldIdxMap() {
        return ptNewIdxOldIdx_;
    }

    /**
     * @param eltNewIdxOldIdx the eltNewIdxOldIdx_ to set
     */
    public void setEltNewIdxOldIdxMap(TIntIntHashMap eltNewIdxOldIdx) {
        this.eltNewIdxOldIdx_ = eltNewIdxOldIdx;
    }

    /**
     * si null, on suppose que nouveau indice = ancien. Si un indice n'est pas
     * dans la map, il est identique � l'ancien.
     * 
     * @param ptNewIdxOldIdx correpondance nouvel indice, ancien.
     */
    public void setPtNewIdxOldIdxMap(TIntIntHashMap ptNewIdxOldIdx) {
        this.ptNewIdxOldIdx_ = ptNewIdxOldIdx;
    }
}
