package org.fudaa.dodico.ef.operation.overstressed;

import java.text.DecimalFormat;
import java.util.Set;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;
import org.fudaa.dodico.ef.operation.refine.EfOperationRefine;
import org.fudaa.dodico.ef.operation.type.EfOperationToT3;
import org.fudaa.dodico.ef.operation.type.EfOperationToT6;

public class EfOperationOverstressed extends AbstractEfOperation {

  // EfOperationRefine operationRefine;

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    // R�cup�ration des index des �l�ments fronti�re.
    int[] overstressedIdx = super.initGrid.getGrid().getOverstressedElement(prog);
    if (CtuluLibArray.isEmpty(overstressedIdx)) {
      log.addInfo("ef.overstressed.notFoud");
      return initGrid;
    }
    log.addInfo("ef.overstressed.nbFound", overstressedIdx.length);
    // Cr�ation d'une liste de s�lection.
    // TODO simplifier
    CtuluListSelection selectedIdx = new CtuluListSelection(overstressedIdx);

    // Cr�ation d'une op�ration de rafinage.
    EfOperationRefine operationRefine = new EfOperationRefine();

    // Initialisation du GridData utilis� pour l'op�ration de raffinage.
    operationRefine.setInitGridData(this.initGrid);
    // Initialisation du CtuluListSelection utilis� pour l'op�ration de raffinage.
    operationRefine.setSelectedElt(selectedIdx);
    // Execution de l'op�ration de raffinage.
    setCurrentActivity(operationRefine);
    EfOperationResult opResultat = operationRefine.process(prog);
    if (opResultat.isFrontierChanged()) setFrontierChanged();
    EfFindEdgeToSwap findEdgeToSwap = new EfFindEdgeToSwap();
    setCurrentActivity(findEdgeToSwap);
    EfGridInterface grid = opResultat.getGridData().getGrid();
    Set<EfSegment> edgToSwap = findEdgeToSwap.process(grid, prog);
    log.addInfo("ef.overstressed.nbEdgesSwaped", edgToSwap.size());
    DecimalFormat fmt = CtuluLib.getDecimalFormat(3);
    if (edgToSwap.size() > 0) {
      for (EfSegment efSegment : edgToSwap) {
        log.addInfo("ef.overstressed.EdgeSwaped", "(" + fmt.format(grid.getPtX(efSegment.getPt1Idx())) + ", "
            + fmt.format(grid.getPtX(efSegment.getPt2Idx())) + ")");
      }
    }
    // on doit swapper des aretes
    if (CtuluLibArray.isNotEmpty(edgToSwap)) {
      boolean containsT6 = containsT6(grid);
      if (containsT6) {
        setFrontierChanged();
        opResultat = execOperation(new EfOperationToT3(), opResultat, prog);
      }
      EfOperationSwapSelectedEdge swapEdges = new EfOperationSwapSelectedEdge();
      swapEdges.setInitGridData(opResultat.getGridData());
      swapEdges.setSelectedEdge(edgToSwap);
      setCurrentActivity(swapEdges);
      opResultat = swapEdges.process(prog);
      if (containsT6) {
        opResultat = execOperation(new EfOperationToT6(), opResultat, prog);
      }

    }
    EfGridData gridData = opResultat.getGridData();
    grid = gridData.getGrid();
    log.addInfo("ef.operation.log.newPt.nb", grid.getPtsNb());
    log.addInfo("ef.operation.log.newEle.nb", grid.getEltNb());
    return gridData;
  }

}
