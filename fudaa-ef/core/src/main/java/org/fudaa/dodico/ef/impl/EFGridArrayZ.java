/*
 *  @creation     3 ao�t 2005
 *  @modification $Date: 2007-01-19 13:07:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;

public class EFGridArrayZ extends EfGridArray {

  /**
   * @param _pts
   * @param _elt
   */
  public EFGridArrayZ(final double[] _pts, final EfElement[] _elt) {
    super(_pts, _elt);
  }

  /**
   * @param _elt
   * @param _gridForPoint
   * @param _type
   */
  public EFGridArrayZ(final EfElement[] _elt, final EfGridInterface _gridForPoint, final EfElementType _type) {
    super(_elt, _gridForPoint, _type);
  }

  /**
   * @param _g
   */
  public EFGridArrayZ(final EfGrid _g) {
    super(_g);
  }

  /**
   * @param _g
   */
  public EFGridArrayZ(final EfGridAbstract _g) {
    super(_g);
  }
  public EFGridArrayZ(final EfGridInterface _g) {
    super(_g);
  }

  /**
   * @param _pts
   * @param _elt
   */
  public EFGridArrayZ(final EfNode[] _pts, final EfElement[] _elt) {
    super(_pts, _elt);
  }

  @Override
  public CtuluCommand setZ(final int _i,final double _newV){
    return super.setZ(_i, _newV);
  }

  @Override
  public CtuluCommand setZ(final int[] _i,final CtuluParser _newV){
    return super.setZ(_i, _newV);
  }

  @Override
  public CtuluCommand setZ(final int[] _i,final double _newV){
    return super.setZ(_i, _newV);
  }

  @Override
  public CtuluCommand setZ(final int[] _i,final double[] _newV){
    return super.setZ(_i, _newV);
  }

  @Override
  public boolean setZIntern(final int _i,final double _newV){
    return super.setZIntern(_i, _newV);
  }




}
