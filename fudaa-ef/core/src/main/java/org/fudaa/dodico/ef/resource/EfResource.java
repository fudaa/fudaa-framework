/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.resource;

import com.memoire.bu.BuResource;
import org.fudaa.dodico.commun.DodicoResource;

/**
 * @author deniger
 */
public class EfResource extends DodicoResource {

  public final static EfResource EF = new EfResource(DodicoResource.DODICO);

  public static String getS(final String _s) {
    return EF.getString(_s);
  }

  public static String getS(final String _s, final String _v0) {
    return EF.getString(_s, _v0);
  }

  public static String getS(final String _s, final String _v0, final String _v1) {
    return EF.getString(_s, _v0, _v1);
  }

  private EfResource(final BuResource _parent) {
    super(_parent);
  }
}
