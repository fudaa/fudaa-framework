/*
 *  @creation     2 mai 2005
 *  @modification $Date: 2007-01-19 13:07:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;





/**
 * @author Fred Deniger
 * @version $Id: EfFilterTime.java,v 1.3 2007-01-19 13:07:19 deniger Exp $
 */
public interface EfFilterTime extends EfFilter {
  
  /**
   * @param _t le pas de temps a prendre en compte
   * @param _src le fournisseur d'info
   */
  void updateTimeStep(int _t,EfGridData _src);
  

}
