/*
 * @creation 9 oct. 06
 * @modification $Date: 2007-06-11 13:04:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIterator;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitorHashSet.java,v 1.2 2007-06-11 13:04:06 deniger Exp $
 */
public class EfIndexVisitorHashSet implements EfIndexVisitor {

  private final TIntHashSet items_ = new TIntHashSet();

  public EfIndexVisitorHashSet() {}

  public void clear() {
    items_.clear();
  }

  public TIntIterator getIterator() {
    return items_.iterator();
  }

  public boolean isEmpty() {
    return items_.isEmpty();
  }

  public int getNbElt() {
    return items_.size();
  }

  public boolean containsElt() {
    return !items_.isEmpty();
  }

  public void visitItem(final Object _item) {
    visitItem(((Number) _item).intValue());
  }

  @Override
  public void visitItem(final int _item) {
    items_.add(_item);
  }

  public int[] getResult() {
    return items_.toArray();
  }

  public TIntHashSet getSelectedNodes(final EfGridInterface _grid) {
    final int nb = items_.size();
    final TIntHashSet res = new TIntHashSet(nb * 4);
    for (int i = 0; i < nb; i++) {
      _grid.getElement(i).fillList(res);
    }
    return res;
  }

  /**
   * @param _preselectMeshes
   */
  public void set(TIntHashSet _preselectMeshes) {
    items_.clear();
    for (TIntIterator it = _preselectMeshes.iterator(); it.hasNext();) {
      items_.add(it.next());
    }
  }

}
