/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.Messages;

/**
 * Classe abstraite permettant de g�rer la methode de stop avec une activit� courante.
 * 
 * @author deniger
 */
public abstract class AbstractEfOperation implements EfOperation {

  public static boolean containsT6(EfGridInterface grid) {
    if (grid == null) return false;
    for (int i = grid.getEltNb() - 1; i >= 0; i--) {
      if (grid.getElement(i).getDefaultType() == EfElementType.T6) return true;
    }
    return false;
  }

  CtuluActivity currentActivity;
  private boolean frontierChanged;

  protected EfGridData initGrid;
  protected boolean stop;

  /**
   * @return the currentActivity
   */
  protected CtuluActivity getCurrentActivity() {
    return currentActivity;
  }

  protected EfOperationResult execOperation(EfOperation op, EfOperationResult in, ProgressionInterface prog) {
    return execOperation(op, in.getGridData(), prog);
  }

  protected EfOperationResult execOperation(EfOperation op, EfGridData in, ProgressionInterface prog) {
    op.setInitGridData(in);
    setCurrentActivity(op);
    return op.process(prog);
  }

  /**
   * @return the frontierChanged
   */
  protected boolean getFrontierChanged() {
    return frontierChanged;
  }

  @Override
  public final EfOperationResult process(ProgressionInterface prog) {
    EfOperationResult res = new EfOperationResult();
    CtuluAnalyze analyze = new CtuluAnalyze();
    analyze.setDefaultResourceBundle(Messages.RESOURCE_BUNDLE);
    analyze.setDesc(getClass().getSimpleName() + ".title");
    res.setAnalyze(analyze);
    frontierChanged = false;
    res.setGridData(process(prog, analyze));
    res.setFrontierChanged(frontierChanged);
    return res;
  }

  protected abstract EfGridData process(ProgressionInterface prog, CtuluAnalyze log);

  /**
   * @param currentActivity the currentActivity to set
   */
  protected void setCurrentActivity(CtuluActivity currentActivity) {
    this.currentActivity = currentActivity;
  }

  /**
   * Indique que la frontiere a �t� modifiee
   * 
   * @param frontierChanged the frontierChanged to set
   */
  protected void setFrontierChanged() {
    this.frontierChanged = true;
  }

  @Override
  public void setInitGridData(EfGridData in) {
    initGrid = in;

  }

  @Override
  public void stop() {
    stop = true;
    if (currentActivity != null) currentActivity.stop();
  }

}
