/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.geom.Coordinate;
import java.util.List;

/**
 * Permet d'�viter les doublons dans les lignes
 *
 * @author Frederic Deniger
 */
public class TriangulationUniqueDataContent {

  private List<Coordinate> allCoordinate;
  private int[][] ptIndex;
  private int[] linearRingIdx;

  /**
   *
   * @param ptCoordinate les coordonn�es
   * @param linearRingIdx pour chaque coordonn�es indique la ligne contenante
   * @param idx contient l'indice dans ptCoordinate de tous les points des lignes.
   */
  TriangulationUniqueDataContent(List<Coordinate> ptCoordinate, int[] linearRingIdx, int[][] idx) {
    this.ptIndex = idx;
    this.allCoordinate = ptCoordinate;
    this.linearRingIdx = linearRingIdx;
  }

  /**
   *
   * @param ptIdx
   * @return l'indice du linearRing contenant ce point. -1 si aucun.
   */
  public int getLinearRingIdx(int ptIdx) {
    return linearRingIdx[ptIdx];
  }

  public int getNbPoints() {
    return allCoordinate.size();
  }

  public double getX(int idx) {
    return allCoordinate.get(idx).x;
  }

  public double getY(int idx) {
    return allCoordinate.get(idx).y;
  }

  public int getPtIdxFor(int idxPoly, int idxPtInPoly) {
    return ptIndex[idxPoly][idxPtInPoly];
  }
}
