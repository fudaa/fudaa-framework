/*
 * @creation 25 oct. 06
 * @modification $Date: 2007-06-13 12:55:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Comparator;
import java.util.List;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionsResultsMng.java,v 1.1 2007-06-13 12:55:42 deniger Exp $
 */
public final class EfLineIntersectionsResultsMng implements EfLineIntersectionParent {
  /**
   * Comparateur a utiliser sur les intersections d'un meme segment. Les intersections ayant la meme distance depuis
   * _x,_y (a 1E-4 pres) sont suppos�es �gales.
   *
   * @author fred deniger
   * @version $Id: EfLineIntersectionsResultsMng.java,v 1.1 2007-06-13 12:55:42 deniger Exp $
   */
  static class DistComparator implements Comparator<EfLineIntersection> {
    double x_;
    double y_;

    @Override
    public int compare(final EfLineIntersection _o1, EfLineIntersection  _o2) {
      if (_o1 == _o2 || ((EfLineIntersectionI) _o1).isOnSameEfObject(_o2)) {
        return 0;
      }
      final double res = (((EfLineIntersectionI) _o1).getDistanceFrom(x_, y_))
          - (((EfLineIntersectionI) _o2).getDistanceFrom(x_, y_));
      // inferieur au millimetre: meme point
      if (Math.abs(res) < 1E-3) {
        return 0;
      }
      // sinon on renvoie le resultat
      return res > 0 ? 1 : -1;
    }

    public double getX() {
      return x_;
    }

    public double getY() {
      return y_;
    }

    public void setX(final double _x) {
      x_ = _x;
    }

    public void setY(final double _y) {
      y_ = _y;
    }
  }

  public static class Initial {
    EfLineIntersection.ItemInitial[][] dataPerSeg_;
    EfGridInterface grid_;
    LineString string_;

    Initial(final EfGridInterface _grid, final EfLineIntersection.ItemInitial[][] _dataPerSeg, final LineString _line) {
      dataPerSeg_ = _dataPerSeg;
      grid_ = _grid;
      string_ = _line;
    }
  }

  public static final EfLineIntersection[] NO_DATA = new EfLineIntersection[0];

  protected static double[] computeDist(EfLineIntersectionsResultsI _res) {
    final double[] dist = new double[_res.getNbIntersect()];
    for (int i = 1; i < dist.length; i++) {
      EfLineIntersection i1 = _res.getIntersect(i - 1);
      EfLineIntersection i2 = _res.getIntersect(i);
      double distance = CtuluLibGeometrie.getDistance(i1.getX(), i1.getY(), i2.getX(), i2.getY());
      dist[i] = dist[i - 1] + distance;
    }
    return dist;
  }

  final EfLineIntersection[] datas_;
  EfLineIntersectionsResultsI defaultRes_;
  double[] dist_;
  final boolean forMesh_;
  final EfGridDataInterpolator grid_;
  /**
   * La ligne initialement utilis�e poru g�n�rer la courbe depuis le calque.
   */
  final LineString initLine_;
  final boolean isInit_;
  final BitSet isOut_;
  boolean isOutRemoved_;
  EfNeighborMesh neighbor_;

  EfLineIntersectionsResultsMng(final EfGridDataInterpolator _grid, final EfLineIntersection[] _dataPerSeg,
                                final BitSet _isOut, final LineString _string, final boolean _forMesh) {
    super();
    grid_ = _grid;
    datas_ = _dataPerSeg;
    isOut_ = _isOut;
    initSeg();
    forMesh_ = _forMesh;
    initLine_ = _string;
    isOutRemoved_ = false;
    isInit_ = true;
  }

  private EfLineIntersectionsResultsMng(final EfLineIntersectionsResultsMng _prev,
                                        final EfLineIntersection[] _dataPerSeg, final BitSet _isOut, final boolean _isOutRemoved) {
    grid_ = _prev.grid_;
    neighbor_ = _prev.neighbor_;
    datas_ = _dataPerSeg;
    isOut_ = _isOut;
    isOutRemoved_ = _isOutRemoved;
    isInit_ = false;
    initLine_ = _prev.initLine_;
    forMesh_ = _prev.forMesh_;
    initSeg();
  }

  @Override
  public void buildNeighbor(final ProgressionInterface _prog) {
    if (neighbor_ == null) {
      neighbor_ = EfNeighborMesh.compute(grid_.getData().getGrid(), _prog);
    }
  }

  public EfLineIntersectionsResultsBuilder createBuilder(EfLineIntersectionsCorrectionTester _tester) {
    return new EfLineIntersectionsResultsBuilder(initLine_, getDefaultRes(), _tester);
  }

  public EfLineIntersectionsResultsMng extract(final int _i1, final int _i2) {
    final int nb = _i2 - _i1 + 1;
    // on ne fait pas car c'est pas normal.
    if (nb <= 1 || nb == datas_.length || datas_.length <= 2) {
      return this;
    }
    final EfLineIntersection[] newRes = new EfLineIntersection[nb];
    final BitSet out = new BitSet(nb - 1);
    for (int i = _i1; i <= _i2; i++) {
      final int j = i - _i1;
      newRes[j] = datas_[i];
      out.set(j, isOut_.get(i));
    }
    return new EfLineIntersectionsResultsMng(this, newRes, out, isOutRemoved_);
  }

  public EfLineIntersectionsResultsI getDefaultRes() {
    if (defaultRes_ == null) {
      defaultRes_ = new EfLineIntersectionsResultsDefault(this);
    }
    return defaultRes_;
  }

  public double getFoundDistFromDeb(final int _idxInter) {
    if (dist_ == null) {
      dist_ = computeDist(getDefaultRes());
    }
    return dist_[_idxInter];
  }

  public EfLineIntersection getFoundIntersect(final int _i) {
    return datas_[_i];
  }

  public int getFoundNbIntersect() {
    return datas_ == null ? 0 : datas_.length;
  }

  @Override
  public EfGridInterface getGrid() {
    return grid_.getData().getGrid();
  }

  @Override
  public EfGridDataInterpolator getGridData() {
    return grid_;
  }

  /**
   * @return the initLine la ligne utilisee pour construire les intersections.
   */
  public LineString getInitLine() {
    return initLine_;
  }

  @Override
  public EfNeighborMesh getNeighbor() {
    return neighbor_;
  }

  private void initSeg() {
    for (int i = datas_.length - 1; i >= 0; i--) {
      datas_[i].setParent(this);
    }
  }

  public EfLineIntersectionsResultsMng inversion() {
    final EfLineIntersection[] newInter = new EfLineIntersection[datas_.length];
    int n = datas_.length - 1;
    final BitSet newOut = new BitSet(n);
    for (int i = n; i >= 0; i--) {
      newInter[i] = datas_[n - i];
      newOut.set(i, isOut_.get(n - i));
    }
    final EfLineIntersectionsResultsMng res = new EfLineIntersectionsResultsMng(this, newInter, newOut, true);
    return res;
  }

  public boolean isAllOut() {
    for (int i = getFoundNbIntersect() - 1; i >= 0; i--) {
      if (getFoundIntersect(i).isRealIntersection()) {
        return false;
      }
    }
    return true;
  }

  public boolean isEmpty() {
    return getFoundNbIntersect() == 0 || isAllOut();
  }

  public boolean isForMesh() {
    return forMesh_;
  }

  public boolean isInit() {
    return isInit_;
  }

  public boolean isOutRemoved() {
    return isOutRemoved_;
  }

  public boolean isSegmentIn(final int _idxInters) {
    return !isSegmentOut(_idxInters);
  }

  public boolean isSegmentOut(final int _idxInters) {
    return isOut_.get(_idxInters);
  }

  public EfLineIntersectionsResultsMng removeExt() {
    if (isOutRemoved_) {
      return this;
    }
    final List newInter = new ArrayList(datas_.length);
    final BitSet newOut = new BitSet(datas_.length - 1);
    int idx = 0;
    for (int i = 0; i < datas_.length; i++) {
      if (datas_[i].isRealIntersection()) {
        newInter.add(datas_[i]);
        newOut.set(idx, isOut_.get(i));
        idx++;
      }
    }
    if (idx == datas_.length) {
      isOutRemoved_ = true;
      return this;
    }
    final EfLineIntersectionsResultsMng res = new EfLineIntersectionsResultsMng(this, (EfLineIntersection[]) newInter
        .toArray(new EfLineIntersection[newInter.size()]), newOut, true);
    return res;
  }

  public EfLineIntersectionsResultsMng restoreInit(final ProgressionInterface _prog) {
    if (isInit_) {
      return this;
    }
    return new EfLineIntersectorActivity(grid_).computeFor(initLine_, _prog, forMesh_);
  }
}
