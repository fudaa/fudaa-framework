/*
 * @creation 30 mai 07
 * @modification $Date: 2007-06-11 13:04:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.interpolation;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataInterpolationAdapter;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author fred deniger
 * @version $Id: EfInterpolator.java,v 1.2 2007-06-11 13:04:06 deniger Exp $
 */
public class EfInterpolator {

  final InterpolationVectorContainer vect_;
  final EfGridData data_;

  public EfInterpolator(final EfGridData _data, final InterpolationVectorContainer _vect) {
    super();
    data_ = _data;
    vect_ = _vect;
  }

  public InterpolationVectorContainer getVect() {
    return vect_;
  }

  protected boolean isVect(CtuluVariable _o) {
    return vect_ != null && vect_.isVect(_o);
  }

  public static double interpolate(int _idxElt, double _x, double _y, EfData _vxData, EfData _vyData, boolean _isVx,
      EfGridInterface _grid) {
    EfData norme = new EfDataInterpolationAdapter.Norm(_vxData, _vyData);
    EfData theta = new EfDataInterpolationAdapter.Theta(_vxData, _vyData);
    double n = doInterpolate(_x, _y, norme, _idxElt, _grid);
    double t = doInterpolate(_x, _y, theta, _idxElt, _grid);
    return _isVx ? InterpolationVectorContainer.getVx(n, t) : InterpolationVectorContainer.getVy(n, t);
  }

  public static double interpolateDangerous(int _idxElt, double _x, double _y, EfData _value, EfGridInterface _grid) {
    return doInterpolate(_x, _y, _value, _idxElt, _grid);
  }

  public double getValue(int _pt1, int _pt2, double _coef1, double _coef2, CtuluVariable _v, int _tIdx)
  throws IOException {
	  //Interpolation sur les vecteurs - ca peut servir ulterieurement
	  // if (isVect(_v)) {

	  // CtuluVariable vx = vect_.getVxSafe(_v);
	  // CtuluVariable vy = vect_.getVySafe(_v);;
	  //  double vx1 = data_.getData(vx, _tIdx, _pt1);
	  //  double vx2 = data_.getData(vx, _tIdx, _pt2);
	  //  double vy1 = data_.getData(vy, _tIdx, _pt1);
	  //  double vy2 = data_.getData(vy, _tIdx, _pt2);
	  //  double n = getMid(InterpolationVectorContainer.getNorme(vx1, vy1), InterpolationVectorContainer
	  //     .getNorme(vx2, vy2), _coef1, _coef2);
	  // double t = getMid(InterpolationVectorContainer.getTheta(vx1, vy1), InterpolationVectorContainer
	  //     .getTheta(vx2, vy2), _coef1, _coef2);
	  // return vect_.isVx(_v) ? InterpolationVectorContainer.getVx(n, t) : InterpolationVectorContainer.getVy(n, t);
	  //}
	  return getMid(data_.getData(_v, _tIdx, _pt1), data_.getData(_v, _tIdx, _pt2), _coef1, _coef2);

  }

  public double getMidValue(int _pt1, int _pt2, CtuluVariable _v, int _tIdx) throws IOException {
    return getValue(_pt1, _pt2, 0.5, 0.5, _v, _tIdx);

  }

  private static double getMid(double _d1, double _d2, double _coef1, double _coef2) {
    return (_d1 * _coef1 + _d2 * _coef2);
  }

  public double interpolate(int _idxElt, double _x, double _y, CtuluVariable _value, int _tIdx) throws IOException {
    EfData data = data_.getData(_value, _tIdx);
    return interpolateFromValue(_idxElt, _x, _y, _value, data, _tIdx);

  }

  public double interpolateFromValue(int _idxElt, double _x, double _y, CtuluVariable _value, EfData _data, int _tIdx)
      throws IOException {
    EfData data = _data;
    if (!data.isElementData() && isVect(_value)) {
      CtuluVariable vx = null;
      CtuluVariable vy = null;
      EfData vxData = null;
      EfData vyData = null;
      boolean isVx = true;
      if (vect_.isVx(_value)) {
        vx = _value;
        vy = vect_.getVy(vx);
        vxData = data;
        vyData = data_.getData(vy, _tIdx);
      } else {
        isVx = false;
        vy = _value;
        vx = vect_.getVx(vy);
        vyData = data;
        vxData = data_.getData(vx, _tIdx);
      }
      return interpolate(_idxElt, _x, _y, vxData, vyData, isVx, data_.getGrid());

      // double norme=InterpolationVectorContainer.getNorme(, _vy)

    }
    return doInterpolate(_x, _y, data, _idxElt, data_.getGrid());

  }

  private static double interpolateT3(final double _xt, final double _yt, final EfData _data, final int _ielt,
      final EfGridInterface _g) {
    if (_data.isElementData()) {
      return _data.getValue(_ielt);
    }
    final EfElement elt = _g.getElement(_ielt);
    final int i1 = elt.getPtIndex(0);
    final int i2 = elt.getPtIndex(1);
    final int i3 = elt.getPtIndex(2);
    return doInterpolateTriangle(_xt, _yt, _data.getValue(i1), _data.getValue(i2), _data.getValue(i3), i1, i2, i3, _g);

  }

  private static double doInterpolateT6(final double _xt, final double _yt, final EfData _data, final int _ielt,
      final EfGridInterface _g) {
    if (_data.isElementData()) {
      return _data.getValue(_ielt);
    }
    final EfElement elt = _g.getElement(_ielt);
    final int i1 = elt.getPtIndex(0);
    final int i2 = elt.getPtIndex(2);
    final int i3 = elt.getPtIndex(4);
    return doInterpolateTriangle(_xt, _yt, _data.getValue(i1), _data.getValue(i2), _data.getValue(i3), i1, i2, i3, _g);
  }

  private static double doInterpolate(final double _xt, final double _yt, final EfData _data, final int _ielt,
      final EfGridInterface _g) {
    if (_data.isElementData()) {
      return _data.getValue(_ielt);
    }
    final EfElement elt = _g.getElement(_ielt);
    if (elt.getDefaultType() == EfElementType.T3) {
      return interpolateT3(_xt, _yt, _data, _ielt, _g);
    } else if (elt.getDefaultType() == EfElementType.T6) {
      return doInterpolateT6(_xt, _yt, _data, _ielt, _g);
    }
    return doInterpolateOther(_xt, _yt, _data, _ielt, _g);
  }

  private static double doInterpolateOther(final double _xt, final double _yt, final EfData _data, final int _ielt,
      final EfGridInterface _g) {
    if (_data.isElementData()) {
      return _data.getValue(_ielt);
    }
    final EfElement elt = _g.getElement(_ielt);
    double num = 0;
    double denom = 0;
    // esp est la distance min au dessous de laquelle 2 points sont consid�r�s comme identiques
    // on calcul les distances au carre, donc on met l'epsilon au carre
    final double eps = 1E-4 * 1E-4;
    for (int i = elt.getPtNb() - 1; i >= 0; i--) {
      final int idx = elt.getPtIndex(i);
      final double dist = CtuluLibGeometrie.getD2(_xt, _yt, _g.getPtX(idx), _g.getPtY(idx));
      // on suppose que si la distance est inf�rieur au dixieme de millim�tre, on prend le r�sultat tel quel.
      if (dist < eps) {
        return _data.getValue(idx);
      }
      num += _data.getValue(idx) / dist;
      denom += 1 / dist;
    }
    if (denom == 0) {
      throw new IllegalAccessError("ne devrait jamais arriv�");
    }
    return num / denom;
  }

  private static double doInterpolateTriangle(final double _xt, final double _yt, final double _v1, final double _v2,
      final double _v3, final int _pt1, final int _pt2, final int _pt3, final EfGridInterface _g) {
    final double a1 = (_g.getPtX(_pt3) - _g.getPtX(_pt2)) * (_yt - _g.getPtY(_pt2))
        - (_g.getPtY(_pt3) - _g.getPtY(_pt2)) * (_xt - _g.getPtX(_pt2));
    final double a2 = (_g.getPtX(_pt1) - _g.getPtX(_pt3)) * (_yt - _g.getPtY(_pt3))
        - (_g.getPtY(_pt1) - _g.getPtY(_pt3)) * (_xt - _g.getPtX(_pt3));
    final double a3 = (_g.getPtX(_pt2) - _g.getPtX(_pt1)) * (_yt - _g.getPtY(_pt1))
        - (_g.getPtY(_pt2) - _g.getPtY(_pt1)) * (_xt - _g.getPtX(_pt1));
    final double cmax = (_g.getPtX(_pt3) - _g.getPtX(_pt2)) * (_g.getPtY(_pt1) - _g.getPtY(_pt2))
        - (_g.getPtY(_pt3) - _g.getPtY(_pt2)) * (_g.getPtX(_pt1) - _g.getPtX(_pt2));
    return (a1 * _v1 + a2 * _v2 + a3 * _v3) / cmax;
  }

}
