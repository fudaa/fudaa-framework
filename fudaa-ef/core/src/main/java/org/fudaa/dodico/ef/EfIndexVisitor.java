/*
 * @creation 9 oct. 06
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitor.java,v 1.1 2007-01-19 13:07:19 deniger Exp $
 */
public interface EfIndexVisitor {

  void visitItem(final int _item);
}
