/*
 * @creation 9 juin 2004
 * @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.ef.EfGridVolumeInterface;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.EfSegment;

/**
 * Un maillage utilisant des elements avec des aretes.
 *
 * @author Fred Deniger
 * @version $Id: EfGridVolume.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridVolume extends EfGrid implements EfGridVolumeInterface {

  EfSegment[] aretes_;

  /**
   * @param _pts les points
   * @param _elts les elements
   * @param _aretes les aretes
   */
  public EfGridVolume(final EfNode[] _pts, final EfElementVolume[] _elts, final EfSegment[] _aretes) {
    super(_pts, _elts);
    aretes_ = _aretes;
  }

  /**
   * @param _x le x a tester
   * @param _y le y a tester
   * @param _eltIdx l'element a parcourir
   * @param _m un point mutable temporaire peut etre nul
   * @return l'indice globale de l'arete de l'element _eltIdx la plus proche du point (_x,_y).
   */
  @Override
  public int getNearestEdgesInElement(final double _x, final double _y, final int _eltIdx, final Coordinate _m) {
    return getEltVolume(_eltIdx).getNearestEdge(_x, _y, this, _m);
  }

  @Override
  public boolean isSameStructure(final EfGridVolumeInterface _v) {
    return EfLib.isSameStructure(this, _v);
  }
  /**
   * @return le nombre d'arete
   */
  @Override
  public int getNbAretes() {
    return aretes_ == null ? 0 : aretes_.length;
  }

  /**
   * @param _idx l'indice de l'arete
   * @return l'arete d'indice _idx
   */
  @Override
  public EfSegment getArete(final int _idx) {
    return aretes_[_idx];
  }

  /**
   * @param _idx l'indice de l'elt
   * @return l'element "volume" correspondant
   */
  @Override
  public EfElementVolume getEltVolume(final int _idx) {
    return (EfElementVolume) getElement(_idx);
  }

}
