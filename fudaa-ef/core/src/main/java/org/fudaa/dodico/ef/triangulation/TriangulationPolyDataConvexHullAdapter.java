/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;

/**
 * permet d'ajouter l'enveloppe convexe � une s�rie de donn�es.
 *
 * @author Frederic Deniger
 */
public class TriangulationPolyDataConvexHullAdapter extends AbstractTriangulationPolyData {

  private final TriangulationPolyDataInterface init;
  private final LinearRing convexHull;

  public TriangulationPolyDataConvexHullAdapter(TriangulationPolyDataInterface init, LinearRing convexHull) {
    this.init = init;
    this.convexHull = convexHull;
  }

  @Override
  public int getPtsNb() {
    return init.getPtsNb();
  }

  @Override
  public double getPtX(int idxPt) {
    return init.getPtX(idxPt);
  }

  @Override
  public double getPtY(int idxPt) {
    return init.getPtY(idxPt);
  }

  @Override
  public int getNbLines() {
    return init.getNbLines() + 1;
  }

  @Override
  public LineString getLine(int idxPoly) {
    if (idxPoly == 0) {
      return convexHull;
    }
    return init.getLine(idxPoly - 1);
  }

  @Override
  public boolean isClosedAndHole(int idxPoly) {
    if (idxPoly == 0) {
      return false;
    }
    return init.isClosedAndHole(idxPoly - 1);
  }
}
