/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Coordinate;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluVariable;

/**
 * @author deniger
 */
public class EfTrajectoireParameters {

  public EfTrajectoireParametersMarqueur marqueur_;

  /**
   * Les points de depart des trajectoires/ligne de courants
   */
  public List<Coordinate> points_;
  
  
  /**
   * Le segment initial
   */
  public List<Coordinate> segment_;
  
  public int nbPointsInitiaux_;
  /**
   * La dur�e de l'int�gration
   */
  public double dureeIntegration_;
  /**
   * L'indice du premier pas de temps a utiliser
   */
  public int firstTimeStepIdx_;
  public double firstTimeStep_;
  /**
   * la finesse: entier >=1 qui permet de diviser le temps de d�placement
   */
  public int finesse_;
  /**
   * la variable pour la vitesse selon x
   */
  public CtuluVariable vx;
  /**
   * la variable pour la vitesse selon y
   */
  public CtuluVariable vy;

  public boolean isLigneDeCourant;

  public boolean isTrajectoire() {
    return !isLigneDeCourant;
  }

  public List<CtuluVariable> varsASuivre_ = Collections.emptyList();

}
