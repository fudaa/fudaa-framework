/*
 * @creation 20 avr. 07
 * @modification $Date: 2007-04-20 16:21:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author fred deniger
 * @version $Id: EfGridRedispatched.java,v 1.1 2007-04-20 16:21:26 deniger Exp $
 */
public class EfGridRedispatched extends EfGridAbstract {

  final EfGridInterface old_;

  public EfGridRedispatched(final EfGridInterface _old, EfElement[] _elts, EfElementType _type) {
    super(_elts);
    typeElt_ = _type;
    old_ = _old;
  }

  @Override
  public double getPtX(int _i) {
    return old_.getPtX(_i);
  }

  @Override
  public double getPtY(int _i) {
    return old_.getPtY(_i);
  }

  @Override
  public double getPtZ(int _i) {
    return old_.getPtZ(_i);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y) {
    this.setPt(_i, _x, _y, 0.0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y, double _z) {
    old_.setPt(_i, _x, _y, _z);
  }

  @Override
  public int getPtsNb() {
    return old_.getPtsNb();
  }

  @Override
  protected boolean setZIntern(int _i, double _newV) {
    return false;
  }

}
