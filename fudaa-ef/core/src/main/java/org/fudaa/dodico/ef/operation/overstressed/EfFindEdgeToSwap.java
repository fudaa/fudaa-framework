/**
 * 
 */
package org.fudaa.dodico.ef.operation.overstressed;

import java.util.HashSet;
import java.util.Set;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.EfSegment;

/**
 * @author Christophe CANEL (Genesis)
 * 
 */
public class EfFindEdgeToSwap implements CtuluActivity {
/*
	public static class EdgeToSwapList
	{
		private TIntObjectHashMap<ElementCouple> element1List = new TIntObjectHashMap<ElementCouple>();
		private TIntObjectHashMap<ElementCouple> element2List = new TIntObjectHashMap<ElementCouple>();
		
		public EdgeToSwapList()
		{
		}
		
		public boolean add(ElementCouple couple)
		{
			if (this.element1List.containsValue(couple))
			{
				return false;
			}
			
			this.element1List.put(couple.getElement1(), couple);
			this.element2List.put(couple.getElement2(), couple);
			
			return true;
		}

		public ElementCouple getCouple(int idxElement1)
		{
			return this.element1List.get(idxElement1);
		}
		
		public boolean isSelectedElement1(int idx)
		{
			return this.element1List.contains(idx);
		}

		public boolean isSelectedElement2(int idx)
		{
			return this.element2List.contains(idx);
		}
	}
	
	/**
	 * Classe contenant un couple d'�l�ment.
	 * 
	 * @author Christophe CANEL (Genesis)
	 *         
	 *//*
	public static class ElementCouple {
		private final int element1;
		private final int element2;
		private final int pts1;
		private final int pts2;
		
		public ElementCouple(int element1, int element2, int pts1, int pts2) {
			super();
			this.element1 = Math.min(element1, element2);
			this.element2 = Math.max(element1, element2);
			this.pts1     = Math.min(pts1, pts2);
			this.pts2     = Math.max(pts1, pts2);
		}

		public int getElement1() {
			return element1;
		}

		public int getElement2() {
			return element2;
		}

		public int getPts1() {
			return pts1;
		}

		public int getPts2() {
			return pts2;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + element1;
			result = prime * result + element2;
			result = prime * result + pts1;
			result = prime * result + pts2;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ElementCouple other = (ElementCouple) obj;
			if (element1 != other.element1)
				return false;
			if (element2 != other.element2)
				return false;
			if (pts1 != other.pts1)
				return false;
			if (pts2 != other.pts2)
				return false;
			return true;
		}

	}
*/
	boolean stop;

	/**
	 * @param grid
	 * @return liste des segments a swapper ou liste vide si aucun
	 */
	public Set<EfSegment> process(EfGridInterface grid, final ProgressionInterface _prog)
	{
        stop = false;
		
		grid.computeBord(_prog, null);
		EfNeighborMesh neighborMesh = EfNeighborMesh.compute(grid, _prog);
		
		Set<EfSegment> list = new HashSet<EfSegment>();
		
		final int nbElt=grid.getEltNb();

		ProgressionUpdater updater = new ProgressionUpdater(_prog);
		updater.majProgessionStateOnly("ef.findEdgeToSwap.msg");
		updater.setValue(10,nbElt);
		// Boucle sur tout les elements.
		for (int i = 0; i < nbElt; i++) {
			
			if (this.stop)
				return null;
			
			EfElement elt=grid.getElement(i);
						
			for (int j = 0; j < elt.getNbEdge(); j++)
			{
				int edgePt1 = elt.getEdgePt1(j);
				int edgePt2 = elt.getEdgePt2(j);
				
				// Test si les 2 points en cours sont frontieres.
				if (grid.getFrontiers().isFrontierPoint(edgePt1) && grid.getFrontiers().isFrontierPoint(edgePt2))
				{
					// R�cup�ration de l'�l�ment adjacent a l'�l�ment en cours.
					int adjIdx = neighborMesh.getAdjacentMeshes(edgePt1, edgePt2, i);
					
					// Si il y a un �l�ment adjacent.
					if (adjIdx > -1)
					{
						list.add(new EfSegment(Math.min(edgePt1, edgePt2), Math.max(edgePt1, edgePt2)));
					}
				}
			}
			updater.majAvancement();
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
  @Override
	public void stop() {
		this.stop = true;
	}

}
