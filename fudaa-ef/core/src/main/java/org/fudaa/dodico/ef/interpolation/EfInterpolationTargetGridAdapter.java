/*
 *  @creation     1 juin 2005
 *  @modification $Date: 2007-06-05 08:59:14 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.interpolation;

import com.memoire.fu.FuEmptyArrays;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.interpolation.InterpolationTarget;
import org.fudaa.dodico.ef.EfGridInterface;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

/**
 * Une classe permettant d'utiliser un maillage comme cible d'une interpolation.
 *
 * @author Fred Deniger
 * @version $Id: EfInterpolationTargetGridAdapter.java,v 1.1 2007-06-05 08:59:14 deniger Exp $
 */
public final class EfInterpolationTargetGridAdapter implements InterpolationTarget {
  private final EfGridInterface efGridInterface;
  private final Envelope envelope;
  private final PointOnGeometryLocator[] testers;
  private final LinearRing[] externs;
  private final Envelope[] externEnvs;
  private final int[] selectedIdx;
  private final boolean isElt;

  /**
   * @param grid le maillage cible
   */
  public EfInterpolationTargetGridAdapter(final EfGridInterface grid, final boolean mesh) {
    this(grid, null, mesh);
  }

  /**
   * @param grid le maillage cible
   * @param selectedIdx non null si seulement une partie des points doivent etre interpolés.
   */
  public EfInterpolationTargetGridAdapter(final EfGridInterface grid, final int[] selectedIdx, final boolean mesh) {
    efGridInterface = grid;
    if (grid.getFrontiers() == null) {
      grid.computeBord(null, null);
    }
    isElt = mesh;
    envelope = new Envelope();
    if (grid.getFrontiers() == null) {
      externEnvs = new Envelope[0];
      externs = new LinearRing[0];
      this.selectedIdx = FuEmptyArrays.INT0;
      testers = new PointOnGeometryLocator[0];
      return;
    }
    externs = efGridInterface.getFrontiers().getExternRing(efGridInterface);
    externEnvs = GISLib.getInternalEnvelop(externs);
    this.selectedIdx = selectedIdx;

    efGridInterface.getEnvelope(envelope);
    testers = GISLib.getTester(externs);
  }

  private final Coordinate tmpCoordinate = new Coordinate();
  private final Coordinate tmpCoordinate1 = new Coordinate();
  private final Coordinate tmpCoordinate2 = new Coordinate();

  @Override
  public boolean isInBuffer(final double _xToTest, final double _yToTest, final double _maxDist) {
    // le point n'appartient pas a l'enveloppe du maillage
    if (!envelope.contains(_xToTest, _yToTest)) {
      if (GISLib.getDistance(_xToTest, _yToTest, envelope) > _maxDist) {
        return false;
      }

      return getDistanceFromFrontier(_xToTest, _yToTest) <= _maxDist;
    }
    tmpCoordinate.x = _xToTest;
    tmpCoordinate.y = _yToTest;
    // le point est contenu dans la forme: ok
    if (GISLib.isIn(tmpCoordinate, externEnvs, testers)) {
      return true;
    }
    // test distance
    return getDistanceFromFrontier(_xToTest, _yToTest) <= _maxDist;
  }

  private double getDistanceFromFrontier(final double _xref, final double _yref) {
    tmpCoordinate.x = _xref;
    tmpCoordinate.y = _yref;
    return GISLib.getDistanceFromFrontier(externs, tmpCoordinate, tmpCoordinate1, tmpCoordinate2);
  }

  @Override
  public int getPtsNb() {
    int nbPointOrMeshes = (isElt ? efGridInterface.getEltNb() : efGridInterface.getPtsNb());
    return (selectedIdx == null) ? nbPointOrMeshes : selectedIdx.length;
  }

  @Override
  public double getPtX(final int idx) {
    if (isElt) {
      return efGridInterface.getMoyCentreXElement(selectedIdx == null ? idx : selectedIdx[idx]);
    }
    return efGridInterface.getPtX(selectedIdx == null ? idx : selectedIdx[idx]);
  }

  @Override
  public double getPtY(final int idx) {
    if (isElt) {
      return efGridInterface.getMoyCentreYElement(selectedIdx == null ? idx : selectedIdx[idx]);
    }
    return efGridInterface.getPtY(selectedIdx == null ? idx : selectedIdx[idx]);
  }
}
