package org.fudaa.dodico.ef.frontier;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluResult;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Permet de donner la correspondance entre les indices des noeuds frontieres d'un nouveau maillage par rapport a un
 * ancien.
 * 
 * @author deniger
 */
public class EfFrontierMapperActivity {
  private final static double MAX_DISTANCE = 1e-2d;

  private EfGridInterface oldGrid;
  private EfGridInterface newGrid;

  // TODO Voir si ajouter progress et analyse.
  public CtuluResult<EfFrontierMapper> compute(ProgressionInterface prog) {
    CtuluResult<EfFrontierMapper> res = new CtuluResult<EfFrontierMapper>();
    CtuluAnalyze log = new CtuluAnalyze();
    res.setAnalyze(log);

    newGrid.computeBord(prog, log);
    oldGrid.computeBord(prog, log);

    EfFrontierInterface newFrontiers = newGrid.getFrontiers();
    EfFrontierInterface oldFrontiers = oldGrid.getFrontiers();
    TIntIntHashMap frontierNewPtsOldPts = new TIntIntHashMap(newFrontiers.getNbTotalPt());
    Coordinate currentCoord = new Coordinate();
    int nbFrontier = newFrontiers.getNbFrontier();
    for (int idxFrontier = 0; idxFrontier < nbFrontier; idxFrontier++) {
      int nbFrtPts = newFrontiers.getNbPt(idxFrontier);
      for (int idxPtOnFrontier = 0; idxPtOnFrontier < nbFrtPts; idxPtOnFrontier++) {
        int idxGlobalOnNew = newFrontiers.getIdxGlobal(idxFrontier, idxPtOnFrontier);
        this.newGrid.getCoord(idxGlobalOnNew, currentCoord);
        boolean isOldPtSupported = oldFrontiers.getFrontiereIndice(idxFrontier, idxPtOnFrontier) >= 0;
        if (isOldPtSupported) {
          int idxGlobalOnOld = oldFrontiers.getIdxGlobal(idxFrontier, idxPtOnFrontier);
          // test basique pour voir si le contour n'a pas bouge
          if (idxGlobalOnOld >= 0 && isSamePoint(oldGrid, idxGlobalOnOld, currentCoord)) {
            frontierNewPtsOldPts.put(newFrontiers.getFrontiereIndice(idxFrontier, idxPtOnFrontier), oldFrontiers
                .getIdxGlobal(idxFrontier, idxPtOnFrontier));
          }
        }
        frontierNewPtsOldPts.put(newFrontiers.getFrontiereIndice(idxFrontier, idxPtOnFrontier), findOldPtIdx(
            currentCoord, oldGrid, oldFrontiers));
      }
    }
    res.setResultat(new EfFrontierMapper(frontierNewPtsOldPts));
    return res;
  }

  /**
   * Donne l'indice dans l'ancien maillage du point ayant une certaine coordonn�e.
   * 
   * @param coord la coordonn�e � tester.
   * @return l'indice du point dans la numerotation frontiere -1 si le point n'existe pas.
   */
  private static int findOldPtIdx(Coordinate coord, EfGridInterface oldGrid, EfFrontierInterface oldFrontiers) {
    int nbFrontier = oldFrontiers.getNbFrontier();
    for (int i = 0; i < nbFrontier; i++) {
      int nbFrtPts = oldFrontiers.getNbPt(i);

      for (int j = 0; j < nbFrtPts; j++) {
        int idx = oldFrontiers.getIdxGlobal(i, j);
        if (isSamePoint(oldGrid, idx, coord)) { return oldFrontiers.getFrontiereIndice(i, j); }
      }
    }
    return -1;
  }

  private static boolean isSamePoint(EfGridInterface oldGrid, int idxPtInOldGrid, Coordinate coordToCompare) {
    return CtuluLibGeometrie.getDistance(coordToCompare.x, coordToCompare.y, oldGrid.getPtX(idxPtInOldGrid), oldGrid
        .getPtY(idxPtInOldGrid)) < MAX_DISTANCE;
  }

  public EfGridInterface getOldGrid() {
    return oldGrid;
  }

  public void setOldGrid(EfGridInterface oldGrid) {
    this.oldGrid = oldGrid;
  }

  public EfGridInterface getNewGrid() {
    return newGrid;
  }

  public void setNewGrid(EfGridInterface newGrid) {
    this.newGrid = newGrid;
  }
}
