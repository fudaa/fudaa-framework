/*
 * @creation 9 oct. 06
 * @modification $Date: 2007-04-20 16:21:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import gnu.trove.TIntHashSet;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitorHashSetNode.java,v 1.1 2007-04-20 16:21:09 deniger Exp $
 */
public class EfIndexVisitorHashSetNode implements EfIndexVisitor {

  private final TIntHashSet items_;
  private final EfGridInterface grid_;

  public EfIndexVisitorHashSetNode(final EfGridInterface _grid) {
    grid_ = _grid;
    items_ = new TIntHashSet(grid_.getPtsNb());
  }

  public void visitItem(final Object _item) {
    items_.add(((Number) _item).intValue());
  }

  @Override
  public void visitItem(final int _item) {
    grid_.getElement(_item).fillList(items_);
  }

  public int[] getResult() {
    return items_.toArray();
  }

}
