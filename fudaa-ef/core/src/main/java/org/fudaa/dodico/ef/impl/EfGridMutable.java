/*
 * @creation 27 juin 2003
 * @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfNode;

/**
 * Une version mutable du maillage.
 *
 * @author deniger
 * @version $Id: EfGridMutable.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridMutable extends EfGrid {

  /**
   * @param _pts les points
   * @param _elts les elements
   */
  public EfGridMutable(final EfNode[] _pts, final EfElement[] _elts) {
    super(_pts, _elts);
  }

  /**
   * @param _elements les nouveaux elements
   */
  public void setElts(final EfElement[] _elements) {
    elts_ = _elements;
  }

  /**
   * Remplace l'element a l'indice <code>_id</code> .Les tableaux ne sont pas redimensionnes.
   *
   * @param _element l'element
   * @param _id l'indice du nouvel elements
   */
  public void setElt(final EfElement _element, final int _id) {
    elts_[_id] = _element;
  }

  /**
   * @param _points les nouveaux points
   */
  public void setPts(final EfNode[] _points) {
    pts_ = _points;
  }

  /**
   * @param _is la nouvelle frontiere
   */
  public void setPtsFrontiere(final EfFrontier _is) {
    ptsFrontiere_ = _is;
  }

  /**
   * Attention : ne pas mettre n'importe quoi !
   *
   * @param _i le nouveau type commnu
   */
  public void setTypeElt(final EfElementType _i) {
    typeElt_ = _i;
  }

  @Override
  public boolean setZIntern(final int _i, final double _newV) {
    return super.setZIntern(_i, _newV);
  }

}