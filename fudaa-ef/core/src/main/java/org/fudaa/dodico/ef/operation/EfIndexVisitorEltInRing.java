/*
 * @creation 9 oct. 06
 *
 * @modification $Date: 2007-06-11 13:04:06 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitorHashSet.java,v 1.2 2007-06-11 13:04:06 deniger Exp $
 */
public class EfIndexVisitorEltInRing implements EfIndexVisitor {
  private CtuluListSelection eltSelection = new CtuluListSelection();
  private final PointOnGeometryLocator tester;
  private final EfGridInterface grid;
  private final Coordinate coord = new Coordinate();
  private boolean strict = true;

  /**
   * @param grid le maillage a parcourir
   * @param ring la ligne ferm�e dans laquelles les elements doivent etre
   * @param strict true si tous les noeuds doivent compris dans la ligne fermee
   * @return la selection d'elements contenus dans le polyligne fermee.
   */
  public static CtuluListSelectionInterface findContainedElt(EfGridInterface grid, LinearRing ring, boolean strict) {
    EfIndexVisitorEltInRing visitor = new EfIndexVisitorEltInRing(grid, ring);
    visitor.strict = strict;
    grid.getIndex().query(ring.getEnvelopeInternal(), visitor);
    return visitor.getResult();
  }

  private EfIndexVisitorEltInRing(EfGridInterface grid, LinearRing linearRing) {
    tester = new IndexedPointInAreaLocator(linearRing);
    this.grid = grid;
  }

  @Override
  public void visitItem(final int _item) {
    EfElement elt = grid.getElement(_item);
    boolean add = strict ? isAllNodeSelected(elt) : isAtLeastOneNodeSelected(elt);
    if (add) {
      eltSelection.add(_item);
    }
  }

  private boolean isAllNodeSelected(EfElement elt) {
    for (int i = 0; i < elt.getPtNb(); i++) {
      if (!isNodeSelected(elt.getPtIndex(i))) {
        return false;
      }
    }
    return true;
  }

  private boolean isAtLeastOneNodeSelected(EfElement elt) {
    for (int i = 0; i < elt.getPtNb(); i++) {
      if (isNodeSelected(elt.getPtIndex(i))) {
        return true;
      }
    }
    return false;
  }

  private boolean isNodeSelected(int ndxId) {
    coord.x = grid.getPtX(ndxId);
    coord.y = grid.getPtY(ndxId);
    return GISLib.isInside(tester, coord);
  }

  public CtuluListSelectionInterface getResult() {
    return eltSelection;
  }
}
