/*
 *  @creation     15 sept. 2005
 *  @modification $Date: 2007-06-11 13:04:06 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.interpolation;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesI;
import org.fudaa.ctulu.interpolation.SupportLocationI;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: EfInterpolationGridSupportAdapter.java,v 1.2 2007-06-11 13:04:06 deniger Exp $
 */
public abstract class EfInterpolationGridSupportAdapter implements SupportLocationI,
        InterpolationSupportValuesI {

  final EfGridInterface grid_;

  /**
   * @param _grid
   */
  public EfInterpolationGridSupportAdapter(final EfGridInterface _grid) {
    super();
    grid_ = _grid;
  }

  @Override
  public double getV(CtuluVariable _var, int _ptIdx) {
    return 0;
  }

  public int getNbValues() {
    return 0;
  }

  /**
   * @author Fred Deniger
   * @version $Id: EfInterpolationGridSupportAdapter.java,v 1.2 2007-06-11 13:04:06 deniger Exp $
   */
  public static class Node extends EfInterpolationGridSupportAdapter {

    /**
     * @param _grid
     */
    public Node(final EfGridInterface _grid) {
      super(_grid);
    }

    @Override
    public int getPtsNb() {
      return grid_.getPtsNb();
    }

    @Override
    public final double getPtX(final int _i) {
      return grid_.getPtX(_i);
    }

    @Override
    public final double getPtY(final int _i) {
      return grid_.getPtY(_i);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: EfInterpolationGridSupportAdapter.java,v 1.2 2007-06-11 13:04:06 deniger Exp $
   */
  public static class Mesh extends EfInterpolationGridSupportAdapter {

    /**
     * @param _grid
     */
    public Mesh(final EfGridInterface _grid) {
      super(_grid);
    }

    @Override
    public int getPtsNb() {
      return grid_.getEltNb();
    }

    @Override
    public final double getPtX(final int _i) {
      return grid_.getMoyCentreXElement(_i);
    }

    @Override
    public final double getPtY(final int _i) {
      return grid_.getMoyCentreYElement(_i);
    }
  }
}
