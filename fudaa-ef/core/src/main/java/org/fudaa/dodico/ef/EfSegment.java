/**
 * @modification $Date: 2007-05-22 13:11:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Definition d'un segment par 2 index. Le point milieu n'est pas pris en compte pour
 * 
 * @author deniger
 * @version $Id: EfSegment.java,v 1.15 2007-05-22 13:11:23 deniger Exp $
 */
public class EfSegment implements Comparable {

  private int pt1Idx_;
  private int pt2Idx_;

  /**
   * @param _pt1 l'indice du point 1
   * @param _pt2 l'indice du point 2
   */
  public EfSegment(final int _pt1, final int _pt2) {
    pt1Idx_ = _pt1;
    pt2Idx_ = _pt2;
  }

  /**
   * @param _s le segment utilise pour l'initialisation
   */
  public EfSegment(final EfSegment _s) {
    initFromSegment(_s);
  }

  protected final void initFromSegment(final EfSegment _s) {
    pt1Idx_ = _s.pt1Idx_;
    pt2Idx_ = _s.pt2Idx_;
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) {
      return true;
    }
    // normalement pour �tre en accord strict avec la norme Java
    // on devrait �crire _obj.GetClass()equals(getClass())
    // mais on ne pourrait pas comparer des EfSegment et des EfSegmentMutable
    if (!(_obj instanceof EfSegment)) {
      return false;
    }
    final EfSegment s = (EfSegment) _obj;
    return (s.pt1Idx_ == pt1Idx_) && (s.pt2Idx_ == pt2Idx_);
  }

  /**
   * @param _s le segment a tester
   * @return true si meme indice
   */
  public boolean isSameIdx(final EfSegment _s) {
    return ((_s.pt1Idx_ == pt1Idx_) && (_s.pt2Idx_ == pt2Idx_)) || ((_s.pt1Idx_ == pt2Idx_) && (_s.pt2Idx_ == pt1Idx_));
  }

  @Override
  public int hashCode() {
    return -pt1Idx_ * 23 + pt2Idx_;
  }

  /**
   * @param _support le maillage support
   * @param _m le point a initialiser avec les coordonnees du point milieu de ce segment
   */
  public void getCentre(final EfGridInterface _support, final Coordinate _m) {
    _m.x = (_support.getPtX(pt1Idx_) + _support.getPtX(pt2Idx_)) / 2;
    _m.y = (_support.getPtY(pt1Idx_) + _support.getPtY(pt2Idx_)) / 2;
    _m.z = (_support.getPtZ(pt1Idx_) + _support.getPtZ(pt2Idx_)) / 2;
  }

  public double getVx(final EfGridInterface _support) {
    return _support.getPtX(pt2Idx_) - _support.getPtX(pt1Idx_);
  }

  public double getVy(final EfGridInterface _support) {
    return _support.getPtY(pt2Idx_) - _support.getPtY(pt1Idx_);
  }

  public double getNorme(final EfGridInterface _support) {
    return Math.hypot(getVx(_support), getVy(_support));
  }

  public double getCentreX(final EfGridInterface _support) {
    return (_support.getPtX(pt1Idx_) + _support.getPtX(pt2Idx_)) / 2;
  }

  public double getCentreZ(final EfGridInterface _support) {
    return (_support.getPtZ(pt1Idx_) + _support.getPtZ(pt2Idx_)) / 2;
  }

  public double getCentreY(final EfGridInterface _support) {
    return (_support.getPtY(pt1Idx_) + _support.getPtY(pt2Idx_)) / 2;
  }

  /**
   * @return l'indice du point 1
   */
  public int getPt1Idx() {
    return pt1Idx_;
  }

  /**
   * @return l'indice du point 2
   */
  public int getPt2Idx() {
    return pt2Idx_;
  }

  /**
   * @param _i le nouvel indice pour le point 1
   */
  protected void setPt1Idx(final int _i) {
    pt1Idx_ = _i;
  }

  /**
   * @param _i le nouvel indice pour le point 2
   */
  protected void setPt2Idx(final int _i) {
    pt2Idx_ = _i;
  }

  protected void setMinMaxIdx(final int _i1, final int _i2) {
    if (_i1 < _i2) {
      pt1Idx_ = _i1;
      pt2Idx_ = _i2;
    } else {
      pt1Idx_ = _i2;
      pt2Idx_ = _i1;
    }
  }

  /**
   * @return "segment " + pt1Idx_ + "," + pt2Idx_
   */
  @Override
  public String toString() {
    return "segment " + pt1Idx_ + CtuluLibString.VIR + pt2Idx_;
  }

  /**
   * Les elements sont d'abord tri�s selon le point 1 puis selon le point 2.
   */
  @Override
  public int compareTo(final Object _seg) {
    final EfSegment s = (EfSegment) _seg;
    return pt1Idx_ > s.pt1Idx_ ? 1 : (pt1Idx_ < s.pt1Idx_ ? -1 : (pt2Idx_ > s.pt2Idx_ ? 1 : (pt2Idx_ < s.pt2Idx_ ? -1 : 0)));
  }

  /**
   * @param _l liste trie selon la methode EfSegment.compare
   * @param _pt1 l'indice a trouver
   * @return l'indice du segment dont le point 1 est _pt1. -1 si non trouve
   */
  /*public static int quickFindPt1(final List _l, final int _pt1) {
    if (_pt1 < 0) {
      return -1;
    }
    int lowIndex = 0;
    int highIndex = _l.size() - 1;
    int temp, tempMid;
    while (lowIndex <= highIndex) {
      tempMid = (lowIndex + highIndex) >> 1;
      temp = ((EfSegment) (_l.get(tempMid))).pt1Idx_ - _pt1;
      if (temp < 0) {
        lowIndex = tempMid + 1;
      } else if (temp > 0) {
        highIndex = tempMid - 1;
      } else {
        return tempMid;
      }
    }
    return -1;
  }

  *//**
   * @param _l le tableau trie selon EfSegment.compare
   * @param _pt1 l'indice a trouver
   * @return l'indice du segment dont le point 1 est _pt1. -1 si non trouve
   */
  /*
  public static int quickFindPt1(final EfSegment[] _l, final int _pt1) {
  if (_pt1 < 0) {
   return -1;
  }
  int lowIndex = 0;
  int highIndex = _l.length - 1;
  int temp, tempMid;
  while (lowIndex <= highIndex) {
   tempMid = (lowIndex + highIndex) >> 1;
   temp = ((_l[tempMid])).pt1Idx_ - _pt1;
   if (temp < 0) {
     lowIndex = tempMid + 1;
   } else if (temp > 0) {
     highIndex = tempMid - 1;
   } else {
     return tempMid;
   }
  }
  return -1;
  }*/
}
