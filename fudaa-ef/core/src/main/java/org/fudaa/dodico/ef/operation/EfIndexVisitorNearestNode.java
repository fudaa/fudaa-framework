/*
 * @creation 9 oct. 06
 * @modification $Date: 2007-04-20 16:21:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Envelope;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitorNearestNode.java,v 1.1 2007-04-20 16:21:10 deniger Exp $
 */
public class EfIndexVisitorNearestNode implements EfIndexVisitor {

  public static Envelope getEnvelope(final double _x, final double _y, final double _precision) {
    final Envelope res = new Envelope();
    res.expandToInclude(_x + _precision, _y + _precision);
    res.expandToInclude(_x - _precision, _y - _precision);
    return res;
  }

  private final EfGridInterface grid_;
  private int selected_ = -1;
  private double minDist_ = -1;
//  private final double maxSquareDistance_;
  private final double maxDistance_;

  private final double x_;
  private final double y_;

  /**
   * @param _grid
   * @param _x
   * @param _y
   * @param _maxDist distance maxi entre l'element a selectionner et le point. Si negatif, non pris en compte
   */
  public EfIndexVisitorNearestNode(final EfGridInterface _grid, final double _x, final double _y, final double _maxDist) {
    super();
    grid_ = _grid;
    maxDistance_ = _maxDist;
//    maxSquareDistance_ = _maxDist < 0 ? -1D : (_maxDist * _maxDist);
    x_ = _x;
    y_ = _y;
  }

  public Envelope buildSearchEnvelop() {

    return maxDistance_ < 0 ? null : EfIndexVisitorNodeSelect.buildSearchEnvelop(x_, y_, maxDistance_);

  }

  @Override
  public void visitItem(final int _idxElt) {
    final EfElement el = grid_.getElement(_idxElt);
    for (int i = el.getPtNb() - 1; i >= 0; i--) {
      final int idx = el.getPtIndex(i);
      final double dist = CtuluLibGeometrie.getDistance(x_, y_, grid_.getPtX(idx), grid_.getPtY(idx));
      if ((maxDistance_ < 0 || dist <= maxDistance_) && (selected_ < 0 || dist < minDist_)) {
        minDist_ = dist;
        selected_ = idx;
      }
    }

  }

  public double getMinDist() {
    return minDist_;
  }

  public int getNodeSelected() {
    return selected_;
  }

  /**
   * @return null si pas de selection
   */
  public CtuluListSelection getSelection() {
    if (selected_ >= 0) {
      final CtuluListSelection res = new CtuluListSelection();
      res.add(selected_);
      return res;
    }
    return null;
  }
}
