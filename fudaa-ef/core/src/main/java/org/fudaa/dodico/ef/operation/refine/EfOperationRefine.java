/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.ef.operation.refine;

import java.io.IOException;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfRefinedGridAddedPt;
import org.fudaa.dodico.ef.interpolation.EfInterpolator;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;

/**
 * Operation qui permet de raffiner un maillage en ajoutant un point au milieu.
 * 
 * @author deniger
 */
public class EfOperationRefine extends AbstractEfOperation {

  private static class EfGridDataFromRefine implements EfGridData {

    private final EfGridData init;
    private final EfRefinementMiddleResult res;
    EfInterpolator interpolator;

    /**
     * @param init les donnees initiales
     * @param res le resultat du rafinage
     * @param vectorContainer classe permettant d'interpoler des valeurs
     */
    public EfGridDataFromRefine(EfGridData init, EfRefinementMiddleResult res,
        InterpolationVectorContainer vectorContainer) {
      super();
      this.init = init;
      this.res = res;
      // permet d'interpoler des valeur a partir des donn�es sur l'ancien maillage
      this.interpolator = new EfInterpolator(init, vectorContainer);
    }

    @Override
    public double getData(CtuluVariable o, int timeIdx, int idxObjet) throws IOException {
      // il faudrait faire mieux pour les perfs...
      return getData(o, timeIdx).getValue(idxObjet);
    }

    @Override
    public EfData getData(CtuluVariable o, int timeIdx) throws IOException {
      EfData initData = init.getData(o, timeIdx);
      // les valeurs sont d�finies aux �l�ments:
      EfRefinedGridAddedPt newGrid = res.getNewGrid();
      if (initData.isElementData()) {
        int nbElt = newGrid.getEltNb();
        // les nouvelles valeurs.
        double[] newVal = new double[nbElt];
        // pour chaque nouvel element, on r�cup�re l'indice de l'ancien �l�ment, on lui affecte la valeur.
        for (int i = 0; i < nbElt; i++) {
          newVal[i] = initData.getValue(res.getOldMeshIdxForAddedMesh(i));
        }
        return new EfDataElement(newVal);
      }
      // le cas des nodes
      int nbNoeuds = newGrid.getPtsNb();
      double[] newVal = new double[nbNoeuds];
      for (int i = 0; i < nbNoeuds; i++) {
        if (res.isAddedPoint(i)) {
          // repr�sente l'indice de l'ancien �l�ment contenant ce nouveau noeud
          int oldMeshIdx = res.getOldMeshIdxForAddedPt(i);
          double x = newGrid.getPtX(i);
          double y = newGrid.getPtY(i);
          newVal[i] = interpolator.interpolate(oldMeshIdx, x, y, o, timeIdx);
          // c'est un point ajout�: il faudra interpoler sa valeur
        }
        // c'est un ancien point
        else {
          newVal[i] = initData.getValue(i);
        }
      }

      return new EfDataNode(newVal);
    }

    @Override
    public EfGridInterface getGrid() {
      return res.getNewGrid();
    }

    @Override
    public boolean isDefined(CtuluVariable var) {
      return init.isDefined(var);
    }

    @Override
    public boolean isElementVar(CtuluVariable idxVar) {
      return init.isDefined(idxVar);
    }

  }

  InterpolationVectorContainer interpolator;
  CtuluListSelectionInterface selectedElt;

  /**
   * @return the interpolator: le
   */
  public InterpolationVectorContainer getInterpolator() {
    return interpolator;
  }

  /**
   * @param interpolator the interpolator to set
   */
  public void setInterpolator(InterpolationVectorContainer interpolator) {
    this.interpolator = interpolator;
  }

  public CtuluListSelectionInterface getSelectedElt() {
    return this.selectedElt;
  }

  public void setSelectedElt(CtuluListSelectionInterface selectedElt) {
    this.selectedElt = selectedElt;
  }

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    EfRefinementMiddleActivity act = new EfRefinementMiddleActivity();
    EfRefinementMiddleResult transform = act.transform(initGrid.getGrid(), prog, this.selectedElt);
    EfOperationResult res = new EfOperationResult();
    res.setFrontierChanged(false);
    return new EfGridDataFromRefine(initGrid, transform, interpolator);
  }

}
