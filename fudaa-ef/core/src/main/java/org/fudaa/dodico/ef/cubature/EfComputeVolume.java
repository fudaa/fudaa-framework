/*
 * @creation 1 f�vr. 07
 * 
 * @modification $Date: 2007-06-11 13:04:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.cubature;

import com.memoire.fu.FuLog;
import java.io.IOException;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Permet de calculer le volume d'eau sur une s�lection d'�l�ment ou de noeuds.
 *
 * @author fred deniger
 * @version $Id: EfComputeVolume.java,v 1.4 2007-06-11 13:04:05 deniger Exp $
 */
public class EfComputeVolume implements CtuluActivity {

  final EfGridData data_;
  final EfFilter filter_;
  final CtuluVariable h_;
  ProgressionInterface prog_;
  EfComputeVolumeSeuil computer;
  boolean stop_;

  public static EfComputeVolume getVolumeComputer(final EfFilter _filter, final ProgressionInterface _prog,
          final EfGridData _data, final CtuluVariable _hVar, final CtuluAnalyze _analyze) {
    if (!_data.isDefined(_hVar)) {
      _analyze.addFatalError(DodicoLib.getS("La variable n'est pas d�finie"));
      return null;
    }
    EfComputeVolumeSeuil computer = EfComputeVolumeSeuil.getVolumeComputer(_filter, _prog, _data, _hVar, 0, _analyze);
    return new EfComputeVolume(computer);

  }

  EfComputeVolume(EfComputeVolumeSeuil computer) {
    this.computer = computer;
    prog_ = computer.prog_;
    data_ = computer.data_;
    h_ = computer.h_;
    filter_ = computer.filter_;
  }

  double computeVolume(EfData _h) {
    return computer.computeVolume(_h).getResultat();
  }

  public double[] getVolume(final int _nbTimeStep) {
    final ProgressionUpdater up = new ProgressionUpdater(prog_);
    up.setValue(10, _nbTimeStep);
    final double[] res = new double[_nbTimeStep];
    up.majProgessionStateOnly(DodicoLib.getS("Calcul du volume"));
    for (int i = 0; i < res.length; i++) {
      if (stop_) {
        return null;
      }
      EfData h = null;
      try {
        h = data_.getData(h_, i);
      } catch (final IOException _evt) {
        FuLog.error(_evt);

      }
      if (h == null) {
        return null;
      }
      res[i] = computeVolume(h);
      up.majAvancement();
    }
    return res;
  }

  public ProgressionInterface getProg() {
    return prog_;
  }

  public void setProg(final ProgressionInterface _prog) {
    prog_ = _prog;
  }

  @Override
  public void stop() {
    stop_ = true;
  }
}
