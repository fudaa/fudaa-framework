package org.fudaa.dodico.ef.impl;

import java.util.concurrent.Callable;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;

public class EfDataCallable implements Callable<EfData> {

  private final EfGridData src;
  private final CtuluVariable variable;
  private final int timeIdx;

  public EfDataCallable(EfGridData src, CtuluVariable variable, int timeIdx) {
    super();
    this.src = src;
    this.variable = variable;
    this.timeIdx = timeIdx;
  }

  @Override
  public EfData call() throws Exception {
    return src.getData(variable, timeIdx);
  }

}