/*
 *  @creation     16 mai 2005
 *  @modification $Date: 2007-06-11 13:04:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;

/**
 * Une interface pour une grille comportant un maillage et des data sur ce maillage.
 * @author Fred Deniger
 * @version $Id: EfGridData.java,v 1.8 2007-06-11 13:04:05 deniger Exp $
 */
public interface EfGridData {

  /**
   * @param _idxVar l'indice de la variable dans le model getNewVarListModel
   * @return true si variable definie sur les elements.
   */
  boolean isElementVar(CtuluVariable _idxVar);

  /**
   * @param _var La variable
   * @return true si des datas existent pour la variable
   */
  boolean isDefined(CtuluVariable _var);

  /**
   * @param _var l'indice de la variable
   * @param _timeIdx le pas de temps. Pour les sources atemporelle, cet indice est ignore
   * @return les valeurs (sur les noeuds ou sur les elements)
   * @throws IOException
   */
  EfData getData(CtuluVariable _var, int _timeIdx) throws IOException;

  /**
   * @param _var la variable demandee
   * @param _timeIdx le pas de temps
   * @param _idxObjet l'indice du noeuds/element
   * @return la valeurs au point demande
   * @throws IOException
   */
  double getData(CtuluVariable _var, int _timeIdx, int _idxObjet) throws IOException;

  /**
   * @return le maillage support
   */
  EfGridInterface getGrid();

}
