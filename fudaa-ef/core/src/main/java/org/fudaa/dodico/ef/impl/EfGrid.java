/*
 * @creation 25 juin 2003
 * 
 * @modification $Date: 2007-01-19 13:07:20 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;


import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;

/**
 * Un maillage contenant des elements donnes dans le meme sens. Ce maillage peut servir de reference pour une
 * interpolation (voir l'interface EfInteropationGridRefI). Dans ce l'interpolation se fait sur la hauteur en chaque
 * point (voir la methode getV(int). Ce maillage peut servir egalement comme destination d'une interpolation (voir
 * l'interface EfInterpolationTarget)
 * 
 * @author deniger
 * @version $Id: EfGrid.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 * @see org.fudaa.dodico.h2d.TestJMaillage
 * @see org.fudaa.dodico.telemac.TestJSerafin
 */
public class EfGrid extends EfGridAbstract {

  EfNode[] pts_;

  /**
   * @param _g le maillage utilise pour l'initialisation
   */
  public EfGrid(final EfGrid _g) {
    super(_g.elts_);
    final int n = _g.pts_.length;
    pts_ = new EfNode[n];
    typeElt_ = _g.typeElt_;
    for (int i = n - 1; i >= 0; i--) {
      pts_[i] = new EfNode(_g.pts_[i]);
    }
    if (_g.ptsFrontiere_ != null) {
      ptsFrontiere_ = new EfFrontier(_g.ptsFrontiere_);
    }
    super.initEnvelopFrom(_g);
  }

  /**
   * Attention : les tableaux sont repris et ne sont pas copies.
   * 
   * @param _pts les points a utiliser
   * @param _elts les elements a utiliser
   */
  public EfGrid(final EfNode[] _pts, final EfElement[] _elts) {
    super(_elts);
    pts_ = _pts;
  }

  /**
   * @param _i l'indice du point voulu
   * @return le point d'indice _i
   */
  public EfNode getPt(final int _i) {
    return pts_[_i];
  }

  @Override
  public int getPtsNb() {
    return pts_.length;
  }

  @Override
  public double getPtX(final int _i) {
    return pts_[_i].getX();
  }

  @Override
  public double getPtY(final int _i) {
    return pts_[_i].getY();
  }

  @Override
  public double getPtZ(final int _i) {
    return pts_[_i].getZ();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y) {
    this.setPt(_i, _x, _y, 0.0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y, double _z) {
    //TODO Verifier que �a ne pose pas de probl�me de changer l'instance dans le tableau.
    pts_[_i] = new EfNode(_x, _y, _z);
  }

  /**
   * @param _m le maillage a tester
   * @param _testZ true si le z doit etre aussi teste
   * @param _epsToCompareDouble la precision pour les comparaison de double
   * @return true si equivalent
   */
  public boolean isEquivalentGrid(final EfGrid _m, final boolean _testZ, final double _epsToCompareDouble) {
    if ((_m.pts_.length != pts_.length) || (_m.elts_.length != elts_.length)) {
      return false;
    }
    int n = elts_.length - 1;
    for (int i = n; i >= 0; i--) {
      if (!elts_[i].isPresentInArray(_m.elts_)) {
        return false;
      }
    }
    n = pts_.length - 1;
    if (_testZ) {
      for (int i = n; i >= 0; i--) {
        if (!pts_[i].isSame(_m.pts_[i])) {
          return false;
        }
      }
    } else {
      for (int i = n; i >= 0; i--) {
        if (!pts_[i].isSameXY(_m.pts_[i], _epsToCompareDouble)) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * @param _m le maillage a tester
   * @param _testZ true si le z doit etre aussi teste
   * @param _epsToCompareDouble la precision pour les comparaison de double
   * @return true si equivalent
   */
  @Override
  public boolean isEquivalent(final EfGridInterface _m, final boolean _testZ, final double _epsToCompareDouble) {
    if (_m instanceof EfGrid) {
      return isEquivalentGrid((EfGrid) _m, _testZ, _epsToCompareDouble);
    }
    return super.isEquivalent(_m, _testZ, _epsToCompareDouble);
  }

  /**
   * Comparaison de 2 maillages :les elements et les points doivent etre dans le meme ordre et exactement identiques.
   * 
   * @param _m le maillage a comparer
   * @return true si exactement le meme
   */
  public boolean isSameStrictGrid(final EfGrid _m, final ProgressionInterface _prog) {
    if ((_m.pts_.length != pts_.length) || (_m.elts_.length != elts_.length)) {
      return false;
    }
    int n = elts_.length - 1;
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(5, n, 0, 50);
    for (int i = n; i >= 0; i--) {
      if (!elts_[i].isEquivalent(_m.elts_[i])) {
        return false;
      }
      up.majAvancement();
    }
    n = pts_.length - 1;
    up.setValue(5, n, 50, 50);
    for (int i = n; i >= 0; i--) {
      if (!pts_[i].isSame(_m.pts_[i])) {
        return false;
      }
      up.majAvancement();
    }
    return true;
  }

  @Override
  public boolean isSameStrict(final EfGridInterface _m, final ProgressionInterface _prog, final double _eps) {
    if (_m instanceof EfGrid) {
      return isSameStrictGrid((EfGrid) _m, _prog);
    }
    return super.isSameStrict(_m, _prog, _eps);
  }

  /**
   * @param _i l'indice du point a modifier
   * @param _z la nouvelle altitude
   */
  @Override
  protected boolean setZIntern(final int _i, final double _newV) {
    if (pts_[_i].getZ() != _newV) {
      pts_[_i].setZ(_newV);
      return true;
    }
    return false;
  }
}