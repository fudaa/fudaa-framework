package org.fudaa.dodico.ef.impl;

import org.fudaa.dodico.ef.EfSegment;

/**
 * Classe utilitaire utilisee pour trouver les bords.
 * 
 * @author Fred Deniger
 * @version $Id: EfFoundSegment.java,v 1.2 2007-05-22 13:11:26 deniger Exp $
 */
public class EfFoundSegment extends EfSegment {

  boolean boundarySegment_;

  int index_;

  /**
   * @param _i1 l'indice 1 du segment
   * @param _i2 l'indice 2 du segment
   */
  public EfFoundSegment(final int _i1, final int _i2,boolean _setMinMax) {
    super(_i1, _i2);
    if(_setMinMax) super.setMinMaxIdx(_i1, _i2);
    
  }

  @Override
  public void setMinMaxIdx(int _i1, int _i2) {
    super.setMinMaxIdx(_i1, _i2);
  }

}