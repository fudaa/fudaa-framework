/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.ef.cubature;

import org.fudaa.ctulu.math.BilanHelper;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.operation.EfLineIntersection;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.ef.operation.EfVectorComputation;

/**
 * @author deniger
 */
public class EfBilanHelper extends BilanHelper {

  /**
   * Calcule l'aire du trapeze en utilisant un objet EfLineIntersectionsResultsI. EfLineIntersectionsResultsI fonctionne de la maniere suivante:
   * getNbIntersect():donne le nombre de points getDistFromDeb(i) donne le Xi: distance entre le debut et le point getIntersect(i)
   * getValueBadInterpolation(EfData _d) donne le y1 associ� au x1
   */
  public static EfDataIntegrale integrerMethodeTrapeze(EfLineIntersectionsResultsI _interfaceResults, EfData _crbdata,
          double seuil) {

    EfDataIntegrale data = new EfDataIntegrale();

    for (int i = 0; i < _interfaceResults.getNbIntersect() - 1; i++) {
      // on ne traite pas ce cas,
      if (_interfaceResults.isSegmentOut(i)) {
        continue;
      }
      double x1 = _interfaceResults.getDistFromDeb(i);
      double x2 = _interfaceResults.getDistFromDeb(i + 1);
      double y1 = _interfaceResults.getIntersect(i).getValueBadInterpolation(_crbdata);
      double y2 = _interfaceResults.getIntersect(i + 1).getValueBadInterpolation(_crbdata);
      computeBilan(x1, x2, y1, y2, seuil, data);
    }

    return data;
  }

  public static EfDataIntegrale integrerMethodeTrapezeVector(EfLineIntersectionsResultsI interfaceRes, EfData vxData, EfData vyData, double seuil) {
    EfDataIntegrale data = new EfDataIntegrale();
    EfVectorComputation vectorComputation = new EfVectorComputation();

    for (int i = 0; i < interfaceRes.getNbIntersect() - 1; i++) {
      // on ne traite pas ce cas,
      if (interfaceRes.isSegmentOut(i)) {
        continue;
      }
      final EfLineIntersection intersect1 = interfaceRes.getIntersect(i);
      final EfLineIntersection intersect2 = interfaceRes.getIntersect(i + 1);

      vectorComputation.vx1_ = intersect1.getValueBadInterpolation(vxData);
      vectorComputation.vy1_ = intersect1.getValueBadInterpolation(vyData);
      vectorComputation.vx2_ = intersect2.getValueBadInterpolation(vxData);
      vectorComputation.vy2_ = intersect2.getValueBadInterpolation(vyData);
      vectorComputation.x1_ = intersect1.getX();
      vectorComputation.y1_ = intersect1.getY();
      vectorComputation.x2_ = intersect2.getX();
      vectorComputation.y2_ = intersect2.getY();

      double x1 = interfaceRes.getDistFromDeb(i);
      double x2 = interfaceRes.getDistFromDeb(i + 1);
      double y1 = vectorComputation.getV1();
      double y2 = vectorComputation.getV2();
      computeBilan(x1, x2, y1, y2, seuil, data);
    }

    return data;
  }

  public static EfDataIntegrale integrerMethodeTrapezeFlowrate(EfLineIntersectionsResultsI interfaceRes, EfData vxData, EfData vyData, EfData h, double seuil) {
    EfDataIntegrale data = new EfDataIntegrale();
    EfVectorComputation vectorComputation = new EfVectorComputation();

    for (int i = 0; i < interfaceRes.getNbIntersect() - 1; i++) {
      // on ne traite pas ce cas,
      if (interfaceRes.isSegmentOut(i)) {
        continue;
      }
      final EfLineIntersection intersect1 = interfaceRes.getIntersect(i);
      final EfLineIntersection intersect2 = interfaceRes.getIntersect(i + 1);

      vectorComputation.vx1_ = intersect1.getValueBadInterpolation(vxData);
      vectorComputation.vy1_ = intersect1.getValueBadInterpolation(vyData);


      vectorComputation.vx2_ = intersect2.getValueBadInterpolation(vxData);
      vectorComputation.vy2_ = intersect2.getValueBadInterpolation(vyData);

      vectorComputation.x1_ = intersect1.getX();
      vectorComputation.y1_ = intersect1.getY();
      vectorComputation.x2_ = intersect2.getX();
      vectorComputation.y2_ = intersect2.getY();

      double h1 = intersect1.getValueBadInterpolation(h);
      double h2 = intersect2.getValueBadInterpolation(h);

      double x1 = interfaceRes.getDistFromDeb(i);
      double x2 = interfaceRes.getDistFromDeb(i + 1);
      double y1 = vectorComputation.getV1() * h1;
      double y2 = vectorComputation.getV2() * h2;
      computeBilan(x1, x2, y1, y2, seuil, data);
    }

    return data;
  }
}
