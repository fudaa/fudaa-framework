/*
 * @creation 11 f�vr. 2004
 * @modification $Date: 2006-09-19 14:42:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

/**
 * @author deniger
 * @version $Id: EfNodeMutable.java,v 1.10 2006-09-19 14:42:23 deniger Exp $
 */
public class EfNodeMutable extends EfNode {

  public EfNodeMutable() {
    super();
  }

  public EfNodeMutable(final double _x, final double _y, final double _z) {
    super(_x, _y, _z);
  }

  /**
   * @param _p point source pour x,y,z
   */
  public EfNodeMutable(final EfNode _p) {
    super(_p);
  }

  @Override
  public void setX(final double _x) {
    super.internSetX(_x);
  }

  @Override
  public void setXYZ(final double _x, final double _y, final double _z) {
    super.internSetXYZ(_x, _y, _z);
  }

  @Override
  public void setY(final double _y) {
    super.internSetY(_y);
  }


}