/**
 * @creation 5 f�vr. 2004
 * @modification $Date: 2007-03-02 13:00:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import java.io.IOException;
import java.util.Map;

/**
 * Une interface pour les sources de maillage. De plus, cette interface peut
 * donner des infos sur des proprietes nodales/elementaires eventuellement
 * inclues dans la source. Exemple :maillage serafin, maillage issu d'un fichier
 * inp,...
 * 
 * @author deniger
 * @version $Id: EfGridSource.java,v 1.8 2007-03-02 13:00:54 deniger Exp $
 */
public interface EfGridSource {
  
  
  public Map getOptions();

    /**
     * @return le maillage
     */
    EfGridInterface getGrid();

    /**
     * @return true si contient des donnees sup pour chaque point.
     */
    boolean containsNodeData();

    /**
     * @return true si contient des donnees sup pour chaque element.
     */
    boolean containsElementData();

    /**
     * @return le nombre de pas de temps present dans la source.
     */
    int getTimeStepNb();

    /**
     * @param _i
     *            l'indice du pas de temps
     * @return la valeur du pas de temps _i
     */
    double getTimeStep(int _i);

    /**
     * @return le nombre de variable concernant des proprietes nodales.
     */
    int getValueNb();

    /**
     * @param _valueIdx
     *            l'indice de la valeur
     * @return un identifiant pour la valeur nodal d'index
     *         <code>_valueIdx</code>.
     */
    String getValueId(int _valueIdx);

    boolean isElement(int _idx);

    /**
     * @param _valueIdx
     *            l'indice de la valeur
     * @param _timeStep
     *            le pas de temps voulu
     * @param _ptIdx
     *            l'indice du point
     * @return la valeur au point _ptIdx pour la valeur n�<code>_valueIdx</code>
     *         et pour le pas de temps <code>_timeStep</code>
     */
    double getValue(int _valueIdx, int _timeStep, int _ptIdx)
	    throws IOException;

    /**
     * Il y a autant d'occurrence que de noeuds.
     * 
     * @return renvoie les entiers d�crivant le type de conditions limites lues
     */
    ConditionLimiteEnum[] getBoundaryConditions();
}