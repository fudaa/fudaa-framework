/*
 * @creation 15 nov. 06
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;

/**
 * @author fred deniger
 * @version $Id: FileFormatGridVersionAbstract.java,v 1.2 2007-05-04 13:45:58 deniger Exp $
 */
public abstract class FileFormatGridVersionAbstract extends FileFormatUnique implements FileFormatGridVersion {

  public FileFormatGridVersionAbstract(final int _nbFile) {
    super(_nbFile);
  }


  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    return read(_f, _prog);
  }

  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
    return null;
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return write(_f, _m, _prog);
  }

}
