/*
 * @creation 17 janv. 07
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.fudaa.dodico.commun.DodicoIntIterator;

/**
 * @author fred deniger
 * @version $Id: AllFrontierIteratorInterface.java,v 1.1 2007-01-19 13:07:19 deniger Exp $
 */
public interface AllFrontierIteratorInterface extends DodicoIntIterator {

  int getBordIDx();

  /**
   * @return l'indice du point sur les frontieres
   */
  int getFrontierIdx();

  /**
   * Return the next index in the general numbering.
   */
  @Override
  int next();

}