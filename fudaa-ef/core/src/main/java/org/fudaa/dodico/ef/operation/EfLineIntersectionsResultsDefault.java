/*
 * @creation 12 juin 07
 * @modification $Date: 2007-06-13 12:55:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluVariable;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionsResultsDefault.java,v 1.1 2007-06-13 12:55:43 deniger Exp $
 */
public class EfLineIntersectionsResultsDefault implements EfLineIntersectionsResultsI {

  final EfLineIntersectionsResultsMng mng_;

  public EfLineIntersectionsResultsDefault(final EfLineIntersectionsResultsMng _mng) {
    super();
    mng_ = _mng;
  }

  @Override
  public double getDistFromDeb(int _idxInter) {
    return mng_.getFoundDistFromDeb(_idxInter);
  }

  @Override
  public EfLineIntersection getIntersect(int _i) {
    return mng_.getFoundIntersect(_i);
  }

  @Override
  public int getNbIntersect() {
    return mng_.getFoundNbIntersect();
  }

  @Override
  public boolean isAllOut() {
    return mng_.isAllOut();
  }

  @Override
  public boolean isEmpty() {
    return mng_.isEmpty();
  }

  @Override
  public boolean isForMesh() {
    return mng_.isForMesh();
  }

  @Override
  public boolean isSegmentIn(int _idxInters) {
    return mng_.isSegmentIn(_idxInters);
  }

  @Override
  public boolean isSegmentOut(int _idxInters) {
    return mng_.isSegmentOut(_idxInters);
  }

  @Override
  public boolean isDataAvailable(CtuluVariable _v) {
    return mng_.getGridData().getData().isDefined(_v);
  }

}
