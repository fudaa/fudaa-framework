/*
 * @creation 25 oct. 06
 *
 * @modification $Date: 2007-06-20 12:21:53 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIterator;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.EfLineIntersection.ItemEdge;
import org.fudaa.dodico.ef.operation.EfLineIntersection.ItemInitial;
import org.fudaa.dodico.ef.operation.EfLineIntersection.ItemNode;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsMng.DistComparator;
import org.locationtech.jts.algorithm.LineIntersector;
import org.locationtech.jts.algorithm.RobustLineIntersector;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.PrecisionModel;

import java.util.*;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectorActivity.java,v 1.5 2007-06-20 12:21:53 deniger Exp $
 */
public class EfLineIntersectorActivity implements CtuluActivity, EfLineIntersectionParent {
  static class TempResult extends EfLineIntersection.ItemInitial {
    public TempResult(final EfLineIntersectionParent parent) {
      super(parent);
    }

    void addArete(final int idx1, final int idx2, final double x, final double y) {
      if (!issetItem1()) {
        item1_ = new ItemEdge(idx1, idx2, x, y);
        item1_.setParent(parent_);
      } else {
        item2_ = new ItemEdge(idx1, idx2, x, y);
        item2_.setParent(parent_);
      }
    }

    void addPt(final int _idx) {
      if (isComplete()) {
        throw new IllegalAccessError("on doit jamais arriv� ici !");
      }
      if (!issetItem1()) {
        item1_ = new ItemNode(_idx);
        item1_.setParent(parent_);
      } else {
        item2_ = new ItemNode(_idx);
        item2_.setParent(parent_);
      }
    }

    void clear() {
      item1_ = null;
      item2_ = null;
    }

    boolean containsIntersection() {
      return issetItem1() || issetItem2();
    }

    public EfLineIntersection.ItemInitial getData(final int ielt, final Coordinate ptLine1) {
      ielt_ = ielt;
      return new EfLineIntersection.ItemInitial(this);
    }

    int getNbComplete() {
      int r = 0;
      if (issetItem1()) {
        r++;
      }
      if (issetItem2()) {
        r++;
      }
      return r;
    }

    boolean isComplete() {
      return getNbComplete() == 2;
    }

    boolean isNodeUsed(final int _idx) {
      return (issetItem1() && item1_.isNodeUsed(_idx)) || (issetItem2() && item2_.isNodeUsed(_idx));
    }
  }

  private EfIndexVisitorHashSet eltSelected = new EfIndexVisitorHashSet();
  private Envelope envelope = new Envelope();
  private EfLineIntersection firstIntersection;
  private final EfGridInterface gridInterface;
  private final EfGridDataInterpolator gridData;
  private LineIntersector intersector_;
  private double precision = 1E-3;
  private TIntHashSet preselectMeshes_;
  private Coordinate ptElt1 = new Coordinate();
  private Coordinate ptElt2 = new Coordinate();
  private Coordinate ptLine1 = new Coordinate();
  private Coordinate ptLine2 = new Coordinate();
  boolean stop;
  private final TempResult tempResult = new TempResult(this);

  public EfLineIntersectorActivity(final EfGridData efGridData) {
    this(new EfGridDataInterpolator(efGridData, null));
  }

  /**
   * @param gridData grid
   * @param vects peut etre null
   */
  public EfLineIntersectorActivity(final EfGridData gridData, final InterpolationVectorContainer vects) {
    this(new EfGridDataInterpolator(gridData, vects));
  }

  public EfLineIntersectorActivity(final EfGridDataInterpolator gridData) {
    super();
    this.gridData = gridData;
    gridInterface = this.gridData.getData().getGrid();
    intersector_ = new RobustLineIntersector();
    // mm
    intersector_.setPrecisionModel(new PrecisionModel(1E4));
  }

  protected void addIntersectFor(final int _idxElt, final List<EfLineIntersection.ItemInitial> results) {

    final EfElement elt = gridInterface.getElement(_idxElt);
    final int nbArete = elt.getNbEdge();
    tempResult.clear();
    for (int i = 0; i < nbArete && !tempResult.isComplete(); i++) {
      final int idx1 = elt.getEdgePt1(i);
      final int idx2 = elt.getEdgePt2(i);
      if (!tempResult.isOnePtUsed(idx1, idx2)) {
        gridInterface.getPt(idx1, ptElt1);
        gridInterface.getPt(idx2, ptElt2);
        // on recherche les intersections
        intersector_.computeIntersection(ptElt1, ptElt2, ptLine1, ptLine2);
        if (intersector_.hasIntersection()) {
          // Colineaire: on rajoute les deux points extremes
          final boolean isPt1AnIntersection = isIntersection(ptElt1);
          final boolean isPt2AnIntersection = isIntersection(ptElt2);
          if (intersector_.getIntersectionNum() == 2 || (isPt1AnIntersection && isPt2AnIntersection)) {
            if (!tempResult.isNodeUsed(idx1)) {
              tempResult.addPt(idx1);
            }
            if (!tempResult.isNodeUsed(idx2)) {
              tempResult.addPt(idx2);
            }
          } else if (isPt1AnIntersection) {
            if (!tempResult.isNodeUsed(idx1)) {
              tempResult.addPt(idx1);
            }
          } else if (isPt2AnIntersection) {
            if (!tempResult.isNodeUsed(idx2)) {
              tempResult.addPt(idx2);
            }
          } else {// un point interieur a l'arete
            final Coordinate c = intersector_.getIntersection(0);
            tempResult.addArete(idx1, idx2, c.x, c.y);
          }
        }
      }
    }

    if (tempResult.containsIntersection()) {
      results.add(tempResult.getData(_idxElt, ptLine1));
    }
  }

  private void addIntersectionForSegExtremite(final List<EfLineIntersection.ItemInitial> list, final double x, final double y,
                                              final EfIndexVisitorHashSet eltSelected) {
    final int startElt = getContainingElement(x, y, eltSelected.getIterator());
    final EfLineIntersection.ItemInitial init = new EfLineIntersection.ItemInitial(this);
    if (startElt >= 0) {
      final EfElement elt = gridInterface.getElement(startElt);
      final double tolerance = getTolerance();
      final int idxSelected = getSelectedNodeIdx(x, y, elt, tolerance);
      if (idxSelected >= 0) {
        init.item1_ = new EfLineIntersection.ItemNode(idxSelected);
        ((EfLineIntersection.ItemNode) init.item1_).setSurroundingMesh(startElt);
      } else {
        // sur une arete ?
        for (int i = 0; i < elt.getNbEdge(); i++) {
          final int i1 = elt.getEdgePt1(i);
          final int i2 = elt.getEdgePt2(i);
          if (CtuluLibGeometrie.distanceFromSegment(gridInterface.getPtX(i1), gridInterface.getPtY(i1), gridInterface.getPtX(i2), gridInterface
              .getPtY(i2), x, y) <= tolerance) {
            init.item1_ = new EfLineIntersection.ItemEdge(i1, i2, x, y);
            ((EfLineIntersection.ItemEdge) init.item1_).setSurroundingMesh(startElt);
            break;
          }
        }
        // pas une arete
        if (init.item1_ == null) {
          init.item1_ = new EfLineIntersection.ItemMesh(startElt, x, y);
        }
      }
      init.ielt_ = startElt;
    } else {
      init.item1_ = new EfLineIntersection.ItemNoIntersect(x, y);
    }

    init.item1_.setParent(this);
    init.item1_.extremiteIntersect_ = true;
    list.add(init);
  }

  @Override
  public void buildNeighbor(final ProgressionInterface progressionInterface) {
    gridInterface.getNeighbors();
  }

  /**
   * Calcul pour chaque segment, les intersections avec le maillage: attention le r�sultat n'est pas tri�.
   *
   * @param lineString line
   * @param prog progression
   * @return les resultats intiaux NON tri�s.
   */
  private EfLineIntersection.ItemInitial[][] compute(final LineString lineString, final ProgressionInterface prog) {
    // on construit l'index si necessaire.
    gridInterface.createIndexRegular(prog);
    final int nbseg = lineString.getNumPoints() - 1;
    final List res = new ArrayList(nbseg);
    final ProgressionUpdater up = new ProgressionUpdater(prog);
    up.setValue(10, nbseg);
    up.majProgessionStateOnly(DodicoLib.getS("Recherche intersections"));
    for (int i = 0; i < nbseg; i++) {
      if (stop) {
        return null;
      }
      final List<ItemInitial> itemInitials = intersect(i, lineString);
      // cas foireux: renvoie null si les deux points de la ligne sont �gaux
      if (itemInitials != null) {
        if (itemInitials.isEmpty()) {
          throw new IllegalAccessError("ne doit jamais arriver");
        }
        final ItemInitial[] objects = itemInitials.toArray(new ItemInitial[0]);
        res.add(objects);
        up.majAvancement();
      }
    }

    return (EfLineIntersection.ItemInitial[][]) res.toArray(new EfLineIntersection.ItemInitial[res.size()][]);
  }

  public final EfLineIntersectionsResultsMng computeFor(final LineString lineString, final ProgressionInterface prog,
                                                        final boolean forElement) {
    final LineString str = GISGeometryFactory.INSTANCE.createLineString(GISGeometryFactory.INSTANCE
        .getCoordinateSequenceFactory().create(lineString.getCoordinateSequence()));
    final EfLineIntersection.ItemInitial[][] res = compute(str, prog);
    stop = false;
    if (res == null) {
      return null;
    }

    final int nbItems = CtuluLibArray.getNbItems(res);
    final List list = new ArrayList(nbItems);
    double last = -1;
    double totalLength = 0;
    final int nbSegments = res.length;
    final DistComparator comp = new DistComparator();
    final SortedSet<EfLineIntersection> set = new TreeSet(comp);
    final ProgressionUpdater up = new ProgressionUpdater(prog);
    up.setValue(10, nbSegments);
    up.majProgessionStateOnly(DodicoLib.getS("Tri des intersections"));
    for (int i = 0; i < nbSegments; i++) {
      final EfLineIntersection.ItemInitial[] init = res[i];
      final double segXInit = str.getCoordinateSequence().getX(i);
      comp.setX(segXInit);
      final double segYInit = str.getCoordinateSequence().getY(i);
      comp.setY(segYInit);
      set.clear();
      final int nbi = init.length;
      if (nbi == 0) {
        throw new IllegalAccessError("ne devrait jamais arriv�");
      }
      // on commence par trier les resultats dans le segment i.
      for (int j = 0; j < nbi; j++) {
        if (stop) {
          return null;
        }
        if (forElement) {
          init[j].addToMeshSortedResult(set, gridInterface);
        } else {
          set.add(init[j].item1_);
          if (init[j].item2_ != null) {
            set.add(init[j].item2_);
          }
        }
      }
      for (final Iterator it = set.iterator(); it.hasNext(); ) {
        if (stop) {
          return null;
        }
        final EfLineIntersectionI intersect = (EfLineIntersection) it.next();
        final double dist = totalLength + intersect.getDistanceFrom(segXInit, segYInit);
        if (dist - last > precision) {
          last = dist;
          list.add(intersect);
          // distFromFirst.add(last);
        }
      }
      totalLength += CtuluLibGeometrie.getDistance(segXInit, segYInit, str.getCoordinateSequence().getX(i + 1), str
          .getCoordinateSequence().getY(i + 1));
      up.majAvancement();
    }

    final EfLineIntersection[] intersections = (EfLineIntersection[]) list.toArray(new EfLineIntersection[list.size()]);
    return new EfLineIntersectionsResultsMng(gridData, intersections, computeIsOut(intersections, prog), str,
        forElement);
  }

  public final EfLineIntersectionsResultsMng computeForMeshes(final LineString lineString, final ProgressionInterface progressionInterface) {
    return computeFor(lineString, progressionInterface, true);
  }

  public final EfLineIntersectionsResultsMng computeForNodes(final LineString lineString, final ProgressionInterface prog) {
    return computeFor(lineString, prog, false);
  }

  BitSet computeIsOut(final EfLineIntersectionI[] is, final ProgressionInterface prog) {
    if (is == null) {
      return null;
    }
    final BitSet isOut = new BitSet(is.length - 1);
    final ProgressionUpdater up = new ProgressionUpdater(prog);
    up.majProgessionStateOnly(DodicoLib.getS("Recherche des segments externes"));
    if (gridInterface.getFrontiers() == null) {
      gridInterface.computeBord(prog, null);
    }
    final GISPolygone[] ext = gridInterface.getExtRings();
    final GISPolygone[] intern = gridInterface.getFrontiers().getInternRing(gridInterface);
    final Envelope[] extEnv = GISLib.getInternalEnvelop(ext);
    final Envelope[] intEnv = GISLib.getInternalEnvelop(intern);
    final PointOnGeometryLocator[] extTester = GISLib.getTester(ext);
    final PointOnGeometryLocator[] intTester = GISLib.getTester(intern);
    final int nb = is.length - 1;
    final Coordinate c = new Coordinate();
    up.setValue(10, nb, 10, 90);
    up.majProgessionStateOnly();
    final double tolerance = getTolerance();
    for (int i = 0; i < nb; i++) {
      c.x = (is[i].getX() + is[i + 1].getX()) / 2;
      c.y = (is[i].getY() + is[i + 1].getY()) / 2;
      if (!GISLib.isIn(c, extEnv, extTester, ext, tolerance) || GISLib.isIn(c, intEnv, intTester, intern, tolerance)) {
        isOut.set(i);
      }
      up.majAvancement();
    }
    return isOut;
  }

  private int getContainingElement(final double x, final double y, final TIntIterator tIntIterator) {
    return EfLineSingleIntersectFinder.getContainingElement(gridInterface, x, y, tIntIterator, getTolerance());
  }

  @Override
  public EfGridInterface getGrid() {
    return gridInterface;
  }

  @Override
  public EfGridDataInterpolator getGridData() {
    return gridData;
  }

  @Override
  public EfNeighborMesh getNeighbor() {
    return null;
  }

  /**
   * @return the precision
   */
  protected double getPrecision() {
    return precision;
  }

  private int getSelectedNodeIdx(final double x, final double y, final EfElement elt, final double tolerance) {
    return EfLineSingleIntersectFinder.getSelectedNodeIdx(gridInterface, x, y, elt, tolerance);
  }

  private double getTolerance() {
    return 1E-3;
  }

  protected List<ItemInitial> intersect(final int _segIdx, final LineString _s) {
    final EfIndexInterface index = gridInterface.getIndex();
    // on recupere les extremites du segment
    _s.getCoordinateSequence().getCoordinate(_segIdx, ptLine1);
    _s.getCoordinateSequence().getCoordinate(_segIdx + 1, ptLine2);

    final List<ItemInitial> itemInitials = new ArrayList<>(eltSelected.getNbElt() + 2);
    // on construit l'enveloppe du segment

    // on recupere les �l�ments selectionn�s par l'enveloppe du segment
    eltSelected.clear();
    if (preselectMeshes_ != null && preselectMeshes_.size() > 0) {
      eltSelected.set(preselectMeshes_);
    } else {
      envelope.setToNull();
      envelope.expandToInclude(ptLine1);
      envelope.expandToInclude(ptLine2);
      index.query(envelope, eltSelected);
    }
    // le premier point du segment
    if (_segIdx == 0 && firstIntersection != null) {
      firstIntersection.setParent(this);
      firstIntersection.extremiteIntersect_ = true;
      final ItemInitial initial = new ItemInitial(this);
      initial.item1_ = firstIntersection;
      initial.ielt_ = firstIntersection.getSurroundingMesh();
      itemInitials.add(initial);
    } else {
      addIntersectionForSegExtremite(itemInitials, ptLine1.x, ptLine1.y, eltSelected);
    }
    // si les points sont diff�rents
    if (ptLine1.distance(ptLine2) > getTolerance()) {

      // si on a qeqchose
      if (eltSelected.containsElt()) {
        // on ajoute les donn�es aux resultats
        for (final TIntIterator it = eltSelected.getIterator(); it.hasNext(); ) {
          addIntersectFor(it.next(), itemInitials);
        }
      }
    }
    // Le dernier point du segment est traite si on est au dernier point
    if (_segIdx == _s.getNumPoints() - 2) {
      addIntersectionForSegExtremite(itemInitials, ptLine2.x, ptLine2.y, eltSelected);
    }
    return itemInitials;
  }

  private boolean isIntersection(final Coordinate pt) {
    final int nb = intersector_.getIntersectionNum();
    for (int i = 0; i < nb; i++) {
      if (intersector_.getIntersection(i).distance(pt) <= precision) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param firstIntersection the firstIntersection to set
   */
  void setKnownFirstIntersection(final EfLineIntersection firstIntersection) {
    this.firstIntersection = firstIntersection;
  }

  /**
   * @param precision the precision to set
   */
  protected void setPrecision(final double precision) {
    this.precision = precision;
  }

  void setPreselectedMeshes(final TIntHashSet _preselectMeshes) {
    preselectMeshes_ = _preselectMeshes;
  }

  @Override
  public void stop() {
    stop = true;
  }
}
