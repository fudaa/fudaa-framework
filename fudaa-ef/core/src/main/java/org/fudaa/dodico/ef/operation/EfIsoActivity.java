/*
 * @creation 17 avr. 07
 * @modification $Date: 2007-06-11 13:04:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import com.memoire.fu.FuLog;
import gnu.trove.TIntIntHashMap;
import java.io.IOException;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridDataInterpolationValuesAdapter;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.impl.EfLibImpl;

/**
 * @author fred deniger
 * @version $Id: EfIsoActivity.java,v 1.4 2007-06-11 13:04:06 deniger Exp $
 */
public class EfIsoActivity implements CtuluActivity {

  private static EfNeighborMesh computeNeighbor(EfGridInterface _g, ProgressionInterface _prog, CtuluAnalyze _analyze) {
    EfNeighborMesh neighbor = _g.getNeighbors();
    if (neighbor == null) {
      _g.computeNeighbord(_prog, _analyze);
      neighbor = _g.getNeighbors();
    }
    return neighbor;
  }

  public static int getPtIdx(EfLineIntersection _inter) {
    return ((EfLineIntersection.ItemNode) _inter).ptIdx_;
  }
  TIntIntHashMap eltNewIdxOldIdx_;

  CtuluListSelectionInterface eltToAvoid_;

  double epsForValues_ = 1E-15;

  CtuluListSelectionInterface filterToUseToAvoid_;

  CtuluListSelection finalFilterToEltData_;

  CtuluListSelection finalFilterToT3_;

  final EfGridInterface grid_;

  EfGridInterface gridEnT3_;

  EfGridInterface gridToUse_;

  EfIsoRestructuredGridResult gridUsedToEltData_;

  EfNeighborMesh neighbor_;

  boolean stop_;

  final InterpolationVectorContainer vects_;

  public EfIsoActivity(final EfGridInterface _grid, InterpolationVectorContainer _vects) {
    super();
    vects_ = _vects;
    grid_ = _grid;
  }

  /**
   * @param _data les donn�es initiales
   * @param _prog la barre de progression
   * @param _analyze l'analyze stockant les �ventuelles erreurs
   * @return les donn�es transform�es si n�cessaires pour avoir un maillage T3 avec des donn�es d�finies sur les noeuds.
   */
  private EfData initializeGrids(final CtuluVariable _var, EfData _data, InterpolationSupportValuesMultiI _values,
      ProgressionInterface _prog, CtuluAnalyze _analyze) {
    EfData nodeData = _data;
    gridToUse_ = grid_;
    filterToUseToAvoid_ = eltToAvoid_;
    if (_data.isElementData()) {
      // on stocke car est inchang� avec les valeurs
      if (gridUsedToEltData_ == null) {
        gridUsedToEltData_ = new EfIsoRestructuredGridActivity().restructure(gridToUse_, _prog, _analyze);
        if (gridUsedToEltData_ == null) return null;
      }
      if (gridUsedToEltData_ == null) return null;
      gridToUse_ = gridUsedToEltData_.getNewGrid();
      if (gridToUse_ == null) return null;
      filterToUseToAvoid_ = finalFilterToEltData_;
      nodeData = gridUsedToEltData_.getNewValues(_var, _data, _values, vects_, _prog, _analyze);
      // si on a un resultat sur les noeuds et que l'on n'a pas un maillage
    } else if (gridToUse_.getEltType() != EfElementType.T3) {
      if (gridEnT3_ == null) {
        if (gridToUse_.getEltType() == EfElementType.T6) {
          gridEnT3_ = EfLibImpl.maillageT6en4T3(grid_, _prog);
        } else {
          eltNewIdxOldIdx_ = new TIntIntHashMap();
          gridEnT3_ = EfLibImpl.toT3(gridToUse_, _prog, _analyze, eltNewIdxOldIdx_);
        }
      }
      gridToUse_ = gridEnT3_;

    }
    return nodeData;
  }

  private void updateFilter(boolean _elt) {
    filterToUseToAvoid_ = eltToAvoid_;
    if (filterToUseToAvoid_ == null) return;
    if (_elt) {
      if (finalFilterToEltData_ == null) {
        int nbElt = gridUsedToEltData_.getNewGrid().getEltNb();
        finalFilterToEltData_ = new CtuluListSelection(nbElt);
        for (int i = 0; i < nbElt; i++) {
          EfElement elt = gridUsedToEltData_.getNewGrid().getElement(i);
          for (int j = 0; j < elt.getPtNb(); j++) {
            int idxPt = elt.getPtIndex(j);
            if (gridUsedToEltData_.getNewGrid().isPtAddedForEltCenter(idxPt)) {
              if (eltToAvoid_.isSelected(gridUsedToEltData_.getNewGrid().getOltEltIdxForAddedPt(idxPt))) {
                finalFilterToEltData_.add(i);
                break;
              }
            }

          }
        }
      }
      filterToUseToAvoid_ = finalFilterToEltData_;

    } else if (grid_.getEltType() != EfElementType.T3) {
      if (finalFilterToT3_ == null) {
        // dans ce cas, chaque element a �t� remplace par 4 �l�ments
        if (grid_.getEltType() == EfElementType.T6) {
          int nbElt = grid_.getEltNb();
          finalFilterToT3_ = new CtuluListSelection(nbElt * 4);
          for (int i = 0; i < nbElt; i++) {
            if (eltToAvoid_.isSelected(i)) {
              finalFilterToT3_.add(4 * i);
              finalFilterToT3_.add(4 * i + 1);
              finalFilterToT3_.add(4 * i + 2);
              finalFilterToT3_.add(4 * i + 3);
            }
          }

        } else {
          int nbElt = gridToUse_.getEltNb();
          finalFilterToT3_ = new CtuluListSelection();
          for (int i = 0; i < nbElt; i++) {
            int old = eltNewIdxOldIdx_.get(i);
            if (eltToAvoid_.isSelected(old)) {
              finalFilterToT3_.add(old);
            }
          }
        }
      }
      filterToUseToAvoid_ = finalFilterToT3_;

    }
  }

  public CtuluListSelectionInterface getEltToAvoid() {
    return eltToAvoid_;
  }

  public double getEpsForValues() {
    return epsForValues_;
  }

  /**
   * Permet de r�cup�rer les isos.<br>
   * TODO: on pourrait utiliser plusieurs threads pour accelerer le tout sur des bi-pro.
   * 
   * @param _values les valeurs � rechecher
   * @param _data les valeurs pouvant �tre d�finies aux noeuds ou sur les �l�ments
   * @param _dest le lieu de stockage des r�sultats
   * @param _prog la barre de progression
   * @return true si le calcul a ete mene au bout
   */
  public EfIsoActivitySearcher search(CtuluVariable _var, EfData _data, ProgressionInterface _prog,
      CtuluAnalyze _analyze, InterpolationSupportValuesMultiI _values) {
    stop_ = false;
    if (_data == null) return null;
    EfData nodeData = initializeGrids(_var, _data, _values, _prog, _analyze);
    updateFilter(_data.isElementData());
    if (stop_) return null;
    if (nodeData == null) return null;
    neighbor_ = computeNeighbor(gridToUse_, _prog, _analyze);
    // dans un premier temps, on recherches les �l�ments voisins.
    if (neighbor_ == null) return null;
    return new EfIsoActivitySearcher(nodeData, grid_, gridToUse_, gridUsedToEltData_, neighbor_, epsForValues_,
        filterToUseToAvoid_);

  }

  public EfIsoActivitySearcher search(CtuluVariable _var, EfGridData _data, int _tIdx, ProgressionInterface _prog,
      CtuluAnalyze _analyze) {
    try {
      return search(_var, _data.getData(_var, _tIdx), _prog, _analyze, new EfGridDataInterpolationValuesAdapter(_data,
          _tIdx));
    } catch (IOException _evt) {
      FuLog.error(_evt);
    }
    return null;

  }

  public void setEltToAvoid(CtuluListSelectionInterface _eltToAvoid) {
    eltToAvoid_ = _eltToAvoid;
    finalFilterToEltData_ = null;
    finalFilterToT3_ = null;
  }

  public void setEpsForValues(double _epsForValues) {
    epsForValues_ = _epsForValues;
  }

  @Override
  public void stop() {
    stop_ = true;

  }

}
