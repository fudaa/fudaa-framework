/*
 GPL 2
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.gis.CtuluLibGeometrie;

/**
 *
 * @author Frederic Deniger
 */
public class EfVectorComputation {

  /**
   * utiliser pour calculer le d�bit
   */
  public double h1_;
  /**
   * utiliser pour calculer le d�bit
   */
  public double h2_;
  public double vx1_;
  public double vx2_;
  public double vy1_;
  public double vy2_;
  public double x1_;
  public double x2_;
  public double y1_;
  public double y2_;

  public EfVectorComputation() {
    super();
  }

  public double getQ() {
    double q1 = h1_ * getV1();
    double q2 = h2_ * getV2();
    return (q1 + q2) * CtuluLibGeometrie.getDistance(x1_, y1_, x2_, y2_) / 2;
  }

  public double getV1() {
    return getV(vx1_, vy1_);
  }

  public double getV2() {
    return getV(vx2_, vy2_);
  }

  double getV(double _vx, double _vy) {
    // le vecteur de x1 vers x2
    double vxs = x2_ - x1_;
    double vys = y2_ - y1_;
    double norme = Math.hypot(vxs, vys);
    // norme nulle ->pas de vitesse
    if (norme == 0) {
      return 0;
    }
    // le coupe v, vs doit etre dans le sens trigo
    // on normalise v
    double vx = vys / norme;
    double vy = -vxs / norme;
    return vx * _vx + vy * _vy; // /1
    // /1
    // /1
  }
}
