package org.fudaa.dodico.ef.operation.renum;

import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;

public class EfOperationRenumeration extends AbstractEfOperation {

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    // TODO Voir si il ne faut pas que �a plante si le nb de node = 0.
    EfGridInterface grid = this.initGrid.getGrid();
    grid.computeBord(prog, null);
    int nbPts = grid.getPtsNb();
    int nbElements = grid.getEltNb();
    int[] newIdxOldIdx = this.getNewIdxOldIdx(this.getPtsWeight(prog), prog);
    int[] oldIdxNewIdx = this.getOldIdxNewIdx(newIdxOldIdx, prog);
    EfElement[] elements = grid.getElts();
    EfElement[] newElements = new EfElement[nbElements];
    EfNode[] nodes = grid.getNodes();
    EfNode[] newNodes = new EfNode[nbPts];
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    if (this.stop) return null;

    updater.majProgessionStateOnly("ef.renumeration.msg");
    updater.setValue(10, nbElements + (2 * nbPts));

    // Cr�ation des �l�ments contenant les nouveaux indexes de leurs points.
    for (int i = 0; i < nbElements; i++) {
      if (this.stop) return null;

      int[] ptsIdx = new int[elements[i].getPtNb()];

      for (int j = 0; j < ptsIdx.length; j++) {
        ptsIdx[j] = oldIdxNewIdx[elements[i].getPtIndex(j)];
      }

      newElements[i] = new EfElement(ptsIdx);

      updater.majAvancement();
    }

    // D�placement des points pour les mettre dans le nouvel ordre.
    for (int i = 0; i < nbPts; i++) {
      if (this.stop) return null;

      newNodes[i] = nodes[newIdxOldIdx[i]];

      updater.majAvancement();
    }

    // TODO Voir si modifier comme dans EfOperationShufle.
    EfGridInterface newGrid = new EfGrid(newNodes, newElements);

    TIntIntHashMap ptNewIdxOldIdx = new TIntIntHashMap(newIdxOldIdx.length);

    for (int i = 0; i < newIdxOldIdx.length; i++) {
      if (this.stop) return null;

      ptNewIdxOldIdx.put(i, newIdxOldIdx[i]);

      updater.majAvancement();
    }

    EfGridDataRenumeroteDecorator renumeroteDecorator = new EfGridDataRenumeroteDecorator(super.initGrid, newGrid);
    renumeroteDecorator.setPtNewIdxOldIdxMap(ptNewIdxOldIdx);

    log.addInfo("ef.operation.log.newPt.nb", newGrid.getPtsNb());
    log.addInfo("ef.operation.log.newEle.nb", newGrid.getEltNb());

    return renumeroteDecorator;
  }

  /**
   * @param prog le manager de progression.
   * @return un tableau contenant pour chaque point sont poids.
   */
  private int[] getPtsWeight(ProgressionInterface prog) {
    EfGridInterface grid = this.initGrid.getGrid();
    int nbPts = grid.getPtsNb();
    int nbElements = grid.getEltNb();
    int[] ptsWeight = new int[nbPts];
    EfElement[] elements = grid.getElts();
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly("ef.renumeration.getPtsWeight.msg");
    updater.setValue(10, nbElements + nbPts);

    // A tout les points fronti�res, affectation d'un poids de 1, pour les
    // autres 0.
    for (int i = 0; i < nbPts; i++) {
      if (this.stop) return new int[0];

      ptsWeight[i] = grid.getFrontiers().isFrontierPoint(i) ? 1 : 0;

      updater.majAvancement();
    }

    // A tout les points de chaque �l�ments on ajoute 2 � leurs poids par
    // �l�ments auxquels ils appartiennent.
    for (int i = 0; i < nbElements; i++) {
      if (this.stop) return new int[0];

      int[] EltPts = elements[i].getIndices();

      for (int j = 0; j < EltPts.length; j++) {
        ptsWeight[EltPts[j]] = ptsWeight[EltPts[j]] + 2;
      }

      updater.majAvancement();
    }

    return ptsWeight;
  }

  /**
   * @param arrayInt un tableau de int.
   * @param prog le manager de progression.
   * @return le plus grand int du tableau.
   */
  private int max(int[] arrayInt, ProgressionInterface prog) {
    int max = arrayInt[0];
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly("ef.renumeration.max.msg");
    updater.setValue(10, arrayInt.length);

    for (int i = 1; i < arrayInt.length; i++) {
      if (this.stop) return 0;

      max = Math.max(max, arrayInt[i]);

      updater.majAvancement();
    }

    return max;
  }

  /**
   * @param ptsWeight un tableau contenant pour chaque point son poids.
   * @param prog le manager de progression.
   * @return un tableau contenant pour chaque nouvel indice sont ancien indice.
   */
  private int[] getNewIdxOldIdx(int[] ptsWeight, ProgressionInterface prog) {
    int[] temp = new int[max(ptsWeight, prog)];
    int[] newIdxOldIdx = new int[ptsWeight.length];
    int maxWeight = 0;
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    if (this.stop) return new int[0];

    updater.majProgessionStateOnly("ef.renumeration.getNewIdxOldIdx.msg");
    updater.setValue(10, ptsWeight.length);

    for (int i = 0; i < ptsWeight.length; i++) {
      if (this.stop) return new int[0];

      int actualPtWeight = ptsWeight[i];

      if (actualPtWeight > maxWeight) {
        for (int j = maxWeight; j < actualPtWeight; j++) {
          temp[j] = i - 1;
        }

        maxWeight = actualPtWeight;
      } else if (actualPtWeight < maxWeight) {
        for (int j = (maxWeight - 1); j > (actualPtWeight - 1); j--) {
          temp[j] = temp[j] + 1;

          newIdxOldIdx[temp[j]] = newIdxOldIdx[temp[j - 1] + 1];
        }
      }

      temp[actualPtWeight - 1] = temp[actualPtWeight - 1] + 1;

      newIdxOldIdx[temp[actualPtWeight - 1]] = i;

      updater.majAvancement();
    }

    return newIdxOldIdx;
  }

  /**
   * @param newIdxOldIdx un tableau contenant pour chaque nouvel indice sont ancien indice.
   * @param prog le manager de progression.
   * @return un tableau contenant pour chaque ancien indice sont nouvel indice.
   */
  private int[] getOldIdxNewIdx(int[] newIdxOldIdx, ProgressionInterface prog) {
    int[] oldIdxNewIdx = new int[newIdxOldIdx.length];
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly("ef.renumeration.getOldIdxNewIdx.msg");
    updater.setValue(10, oldIdxNewIdx.length);

    for (int i = 0; i < oldIdxNewIdx.length; i++) {
      if (this.stop) return new int[0];

      oldIdxNewIdx[newIdxOldIdx[i]] = i;

      updater.majAvancement();
    }

    return oldIdxNewIdx;
  }
}
