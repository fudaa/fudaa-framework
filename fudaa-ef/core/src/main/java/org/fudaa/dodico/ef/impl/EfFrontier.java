/**
 * @creation 30 juin 2003
 * @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import com.memoire.fu.FuLog;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.dodico.ef.AllFrontierIteratorInterface;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Les frontieres d'un maillage (plusieurs tableaux d'entier).
 *
 * @author deniger
 * @version $Id: EfFrontier.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfFrontier implements EfFrontierInterface {
  @Override
  public GISPolygone getSavedLinearRing(int _idxFr) {
    return ring_ == null ? null : ring_[_idxFr];
  }

  protected int[][] bords_;
  GISPolygone[] ring_;
  FastBitSet globalIdxInFrontier;

  /**
   * @param _l les tableaux d'entiers decrivant les frontiere
   */
  public EfFrontier(final List _l, final FastBitSet _isExterne) {
    final int n = _l.size();
    if (n > 0) {
      bords_ = new int[n][];
      _l.toArray(bords_);
    } else {
      bords_ = new int[0][];
    }
    isExterne_ = new CtuluListSelection(_isExterne);
  }

  @Override
  public GISPolygone getLinearRing(final EfGridInterface _grid, final int _idxFr) {
    if (ring_ == null) {
      ring_ = new GISPolygone[bords_.length];
    }
    GISPolygone r = ring_[_idxFr];
    if (r == null) {
      r = _grid.getLinearRing(bords_[_idxFr]);
      ring_[_idxFr] = r;
    }
    return r;
  }

  /**
   * @param _fr la frontiere a comparer
   * @return true si identique
   */
  @Override
  public boolean isSame(final EfFrontierInterface _fr) {
    if (_fr == null) {
      return false;
    }
    if (bords_.length != _fr.getNbFrontier()) {
      return false;
    }
    for (int i = bords_.length - 1; i >= 0; i--) {
      for (int j = _fr.getNbPt(i) - 1; j >= 0; j--) {
        if (_fr.getIdxGlobal(i, j) != getIdxGlobal(i, j)) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Permet de retrouver les frontieres externes. Cette procedure ne fait aucune supposition sur l'ordre des fronti�res donn�es. On pourrait optimiser
   * en prenant en compte que les frontieres sont donn�es dans l'ordre Telemac (point sud-ouest). Or il se peut que cela soit faux si un tableau ipobo
   * erron� est utilis�.
   *
   * @param _g le maillage
   * @param _idxFr liste de tableaux d'entier: les indices des frontieres
   * @param _prog la barre de progression
   * @return un bitset: si 1 c'est externe interne sinon
   */
  public static final FastBitSet getExterneFr(final EfGridInterface _g, final List _idxFr, final ProgressionInterface _prog) {
    if (_idxFr == null) {
      return null;
    }
    // le bitset contenant les frontieres externes
    final FastBitSet r = new FastBitSet(_idxFr.size());
    // la premiere frontiere est suppos�e externe
    r.set(0);
    // retiens les enveloppes et le PointInRing pour savoir si un point appartient
    // ou non a une autre frontiere
    // liste des SIRtreePointInRing
    final List<PointOnGeometryLocator> externTesters = new ArrayList(_idxFr.size());
    // liste des enveloppes
    final List externEnvs = new ArrayList(_idxFr.size());
    LinearRing ring = _g.getLinearRing((int[]) _idxFr.get(0));
    externEnvs.add(ring.getEnvelopeInternal());
    externTesters.add(new IndexedPointInAreaLocator(ring));
    // la coordonn�e tampon
    final Coordinate c = new Coordinate();
    // l'avancement
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    final int nb = _idxFr.size();
    up.setValue(3, nb, 0, 60);
    for (int i = 1; i < nb; i++) {
      final int[] idxs = (int[]) _idxFr.get(i);
      _g.getCoord(idxs[0], c);
      if (!GISLib.isIn(c, externEnvs, externTesters)) {
        r.set(i);
        ring = _g.getLinearRing(idxs);
        externEnvs.add(ring.getEnvelopeInternal());
        externTesters.add(new IndexedPointInAreaLocator(ring));
      }
      up.majAvancement();
    }
    // on va verifier que les frontieres sont bien externes
    // il se peut qu'une frontiere ajoutee � l'indice k contienne
    // une frontiere d'indice inf�rieure j. Dans ce cas la fronti�re j
    // doit etre declaree comme non externe
    // l'indice de l'env et du sir enregistre dans les listes externe
    int idxInList = 0;
    up.setValue(2, nb, 60, 40);
    up.majProgessionStateOnly();
    for (int i = 0; i < nb; i++) {
      if (r.get(i)) {
        _g.getCoord(((int[]) _idxFr.get(i))[0], c);
        // on ne teste qu'a partir de l'indice de cette frontiere
        // par construction il est impossible qu'une frontiere
        // d'indice sup soit contenu dans une frontiere inf
        if (GISLib.isIn(c, externEnvs, externTesters, ++idxInList)) {
          r.clear(i);
        }
      }
      up.majAvancement();
    }

    return r;
  }

  CtuluListSelection isExterne_;

  /**
   * Frontiere unique definit par le tableau d'entier.
   *
   * @param _l les indices des points frontieres
   */
  public EfFrontier(final int[] _l) {
    bords_ = new int[1][];
    bords_[0] = _l;
    isExterne_ = new CtuluListSelection(1);
    isExterne_.setSelectionInterval(0, 0);
  }

  @Override
  public boolean isExtern(final int _i) {
    return isExterne_.isSelected(_i);
  }

  @Override
  public int[] getIdxCopy(int _idxFr) {
    return CtuluLibArray.copy(bords_[_idxFr]);
  }

  /**
   * Renvoie l'index de frontiere a partir de l'index de la frontiere et de l'index du point sur la fronti�re. Par exemple si vous avez 2
   * frontieres,la premiere de taille 100 et la 2eme de taille 20, l'appel getIdxFrGlob(1,10) renvoie 110 (le point 10 de la 2eme frontiere).
   * getIdxFrGlob(0,10) renvoie (10). getIdxFrGlob(1,100) renvoie -1 (erreur).
   *
   * @param _idxFr l'indice de la frontier
   * @param _idxOnFr l'indice du point sur la frontier _idxFr
   * @return -1 si les donnees sont erron�es.
   */
  @Override
  public int getFrontiereIndice(final int _idxFr, final int _idxOnFr) {
    if ((_idxFr >= 0) && (_idxFr < bords_.length)) {
      int r = 0;
      for (int i = _idxFr - 1; i >= 0; i--) {
        r += bords_[i].length;
      }
      if ((_idxOnFr >= 0) && (_idxOnFr < bords_[_idxFr].length)) {
        r += _idxOnFr;
        return r;
      }
    }
    return -1;
  }

  @Override
  public int getFrontiereIndice(final int _idxGlob) {
    final int n = bords_.length;
    for (int i = 0; i < n; i++) {
      final int n2 = bords_[i].length;
      for (int j = 0; j < n2; j++) {
        if (bords_[i][j] == _idxGlob) {
          return i;
        }
      }
    }
    return -1;
  }

  @Override
  public double[] getAbsCurviligne(final EfGridInterface _grid, final int _frIdx, final int _firstIdxOnFr, final int _nb) {
    final double[] res = new double[_nb];
    final EfFrontier.FrontierIterator it = getFrontierIterator(_frIdx, _firstIdxOnFr);
    int ptLast = it.getGlobalIdx();
    int ptCurrent;
    for (int i = 1; i < res.length; i++) {
      it.next();
      ptCurrent = it.getGlobalIdx();
      res[i] = res[i - 1]
          + CtuluLibGeometrie.getDistance(_grid.getPtX(ptLast), _grid.getPtY(ptLast), _grid.getPtX(ptCurrent), _grid.getPtY(
          ptCurrent));
      ptLast = ptCurrent;
    }

    return res;
  }

  /**
   * @param _f la frontiere source
   */
  public EfFrontier(final EfFrontierInterface _f) {
    final int nbFr = _f.getNbFrontier();
    bords_ = new int[nbFr][];
    for (int i = nbFr - 1; i >= 0; i--) {
      bords_[i] = _f.getIdxCopy(i);
    }
    isExterne_ = new CtuluListSelection(nbFr);
    for (int i = 0; i < nbFr; i++) {
      if (_f.isExtern(i)) {
        isExterne_.add(i);
      }
      GISPolygone ringi = _f.getSavedLinearRing(i);
      if (ringi != null) {
        if (ring_ == null) {
          ring_ = new GISPolygone[nbFr];
        }
      }
      ring_[i] = ringi;
    }
  }

  @Override
  public GISPolygone[] getExternRing(final EfGridInterface _grid) {
    final int nb = getNbFrontier();
    final List r = new ArrayList(nb);
    for (int i = 0; i < nb; i++) {
      if (isExtern(i)) {
        r.add(getLinearRing(_grid, i));
      }
    }
    return (GISPolygone[]) r.toArray(new GISPolygone[r.size()]);
  }

  @Override
  public GISPolygone[] getInternRing(final EfGridInterface _grid) {
    final int nb = getNbFrontier();
    final List r = new ArrayList(nb);
    for (int i = 0; i < nb; i++) {
      if (!isExtern(i)) {
        r.add(getLinearRing(_grid, i));
      }
    }
    return (GISPolygone[]) r.toArray(new GISPolygone[r.size()]);
  }

  @Override
  public void clearSaveData() {
    if (ring_ != null) {
      Arrays.fill(ring_, null);
    }
  }

  @Override
  public int getNbFrontierExt() {
    return isExterne_.getNbSelectedIndex();
  }

  /**
   * @return l'iterateur sur les frontieres
   */
  @Override
  public AllFrontierIteratorInterface getAllFrontierIterator() {
    return new AllFrontiereIterator();
  }

  /**
   * @param _idxGlobal
   * @return [ frontier index, point index in the frontier] from the general index.
   */
  @Override
  public int[] getIdxFrontierIdxPtInFrontierFromGlobal(final int _idxGlobal) {
    final int[] r = new int[2];
    return (getIdxFrontierIdxPtInFrontierFromGlobal(_idxGlobal, r)) ? r : null;
  }

  /**
   * Initialyze
   * <code>temp</code> with [ frontier index, point index in the frontier] from the general index.
   *
   * @param _idxGlobal l'indice general du point
   * @param _temp le tableau a modifier [ frontier index, point index in the frontier]
   * @return true si cet indice est bien un indice frontiere
   */
  @Override
  public boolean getIdxFrontierIdxPtInFrontierFromGlobal(final int _idxGlobal, final int[] _temp) {
    for (int i = 0; i < bords_.length; i++) {
      final int n = bords_[i].length;
      for (int j = 0; j < n; j++) {
        if (bords_[i][j] == _idxGlobal) {
          _temp[0] = i;
          _temp[1] = j;
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public int getIdxFrontierFromGlobal(final int _idxGlobal) {
    for (int i = 0; i < bords_.length; i++) {
      final int n = bords_[i].length;
      for (int j = 0; j < n; j++) {
        if (bords_[i][j] == _idxGlobal) {
          return i;
        }
      }
    }
    return -1;
  }

  /**
   * Renvoie l'index du point d'indice global
   * <code>_idxGlobal</code> sur la frontiere.
   *
   * @param _idxGlobal l'indice globale
   * @return -1 si c'est n'est pas un point frontier. L'indice de frontiere sinon
   */
  @Override
  public int getIdxOnFrontier(final int _idxGlobal) {
    int index = 0;
    final int n = bords_.length;
    for (int i = 0; i < n; i++) {
      final int n2 = bords_[i].length;
      for (int j = 0; j < n2; j++) {
        if (bords_[i][j] == _idxGlobal) {
          return index;
        }
        index++;
      }
    }
    return -1;
  }

  /**
   * @param _idxGlobal l'indice global
   * @param _idxFr l'indice de la frontiere voulu
   * @return l'indice du point d'indice _idxGlobal sur la frontiere _idxFr
   */
  @Override
  public int getQuickIdxOnFrontier(final int _idxGlobal, final int _idxFr) {
    int index = 0;
    final int n2 = bords_[_idxFr].length;
    for (int j = 0; j < n2; j++) {
      if (bords_[_idxFr][j] == _idxGlobal) {
        return index;
      }
      index++;
    }
    return -1;
  }

  /**
   * Renvoie l'indice de frontiere a partir de l'indice global. Initialise egalement le tableau idxFrIdxOnFr (qui doit etre de taille 2) avec l'index
   * de frontiere et l'index du point sur la frontiere.
   *
   * @param _idxGlobal l'indice global du point
   * @param _idxFrIdxOnFr l'indice sur la frontiere
   * @return l'indice de frontiere a partir de l'indice global
   */
  @Override
  public int getIdxOnFrontier(final int _idxGlobal, final int[] _idxFrIdxOnFr) {
    int index = 0;
    final int n = bords_.length;
    for (int i = 0; i < n; i++) {
      final int n2 = bords_[i].length;
      for (int j = 0; j < n2; j++) {
        if (bords_[i][j] == _idxGlobal) {
          _idxFrIdxOnFr[0] = i;
          _idxFrIdxOnFr[1] = j;
          return index;
        }
        index++;
      }
    }
    return -1;
  }

  /**
   * @param _idxGlobal l'indice global a tester
   * @return true si le point d'indice globle <code>_idxGlobal</code> est un point frontiere
   */
  @Override
  public boolean isFrontierPoint(final int _idxGlobal) {
    if (this.globalIdxInFrontier == null) {
      globalIdxInFrontier = new FastBitSet();
      final int n = bords_.length;
      for (int i = 0; i < n; i++) {
        final int n2 = bords_[i].length;
        for (int j = 0; j < n2; j++) {
          globalIdxInFrontier.set(bords_[i][j]);
        }
      }
    }
    return globalIdxInFrontier.get(_idxGlobal);
  }

  /**
   * Initialyze the array _idxFrIdxPt with [frontiere idx,point idx on frontier] from the frontier index _idxOnFrontier.
   *
   * @param _idxOnFrontier l'indice frontiere du point
   * @param _idxFridxPt le tableau a initialise
   * @return true if found false otherwise. true si trouve
   */
  @Override
  public boolean getIdxFrontierIdxPtInFrontierFromFrontier(final int _idxOnFrontier, final int[] _idxFridxPt) {
    final int n = bords_.length;
    int shiftL = 0;
    for (int i = 0; i < n; i++) {
      if (_idxOnFrontier < (shiftL + bords_[i].length)) {
        _idxFridxPt[0] = i;
        _idxFridxPt[1] = _idxOnFrontier - shiftL;
        return true;
      }
      shiftL += bords_[i].length;
    }
    return false;
  }

  /**
   * @param _idxGlobalOnFrontiers l'indice frontiere du point
   * @return the general index from the index on the frontier. L'indice general du point dont l'indice frontiere est
   *     <code>_idxGlobalOnFrontiers</code>.
   */
  @Override
  public final int getIdxGlobalFrom(final int _idxGlobalOnFrontiers) {
    final int n = bords_.length;
    int shiftL = 0;
    for (int i = 0; i < n; i++) {
      if (_idxGlobalOnFrontiers < (shiftL + bords_[i].length)) {
        return bords_[i][_idxGlobalOnFrontiers - shiftL];
      }
      shiftL += bords_[i].length;
    }
    return -1;
  }

  /**
   * @param _idxFr l'indice de la frontiere
   * @return l'iterateur sur cette frontiere
   */
  @Override
  public FrontierIterator getFrontierIterator(final int _idxFr) {
    return new FrontierIterator(_idxFr);
  }

  /**
   * @param _idxFr l'indice de la frontiere
   * @param _firstIdx le premiere indice a prendre en compte
   * @return l'iterateur sur cette frontiere
   */
  @Override
  public FrontierIterator getFrontierIterator(final int _idxFr, final int _firstIdx) {
    return new FrontierIterator(_idxFr, _firstIdx);
  }

  /**
   * @return le nombre total de points des frontieres.
   */
  @Override
  public int getNbTotalPt() {
    int r = 0;
    for (int i = bords_.length - 1; i >= 0; i--) {
      r += bords_[i].length;
    }
    return r;
  }

  /**
   * Permet de verifier si les indices de bords sont dans le meme ordre que celui donne par _ipobo.
   *
   * @param _ipobo les indices des points frontieres a comparer
   * @return si meme ordre des points frontieres
   */
  @Override
  public boolean isSame(final int[] _ipobo) {
    if (_ipobo == null) {
      return false;
    }
    final int nBI = bords_.length;
    int indexIpobo = 0;
    int nbFI;
    for (int i = 0; i < nBI; i++) {
      nbFI = bords_[i].length;
      for (int j = 0; j < nbFI; j++) {
        if (_ipobo[indexIpobo + j] != bords_[i][j]) {
          FuLog.error("Error intern");
          return false;
        }
      }
      indexIpobo += nbFI;
    }
    return true;
  }

  /**
   * @return les bords sous forme d'un tableau ( conforme tableau ipobo de telemac/serafin).
   */
  @Override
  public int[] getArray() {
    final int[] r = new int[getNbTotalPt()];
    for (final AllFrontiereIterator it = new AllFrontiereIterator(); it.hasNext(); ) {
      final int i = it.next();
      r[it.getFrontierIdx()] = i;
    }
    return r;
  }

  @Override
  public int[] getIpobo(int _nbPt) {
    int[] res = new int[_nbPt];
    int idx = 1;
    for (final AllFrontiereIterator it = new AllFrontiereIterator(); it.hasNext(); ) {
      res[it.next()] = idx++;
    }
    return res;
  }

  /**
   * Attention peut etre tres long !
   *
   * @return une chaine de caractere decrivant les indices du bord principal
   * @see org.fudaa.ctulu.CtuluLibString#getObjectInString(Object, boolean)
   */
  @Override
  public String getDescBordPrinc() {
    return CtuluLibString.getObjectInString(bords_[0], true);
  }

  /**
   * @param _id la frontiere voulue
   * @return une chaine de caractere decrivant les indices du bord interne d'indice _id
   * @see org.fudaa.ctulu.CtuluLibString#getObjectInString(Object, boolean)
   */
  @Override
  public String getDescBordInterne(final int _id) {
    return CtuluLibString.getObjectInString(bords_[_id + 1], true);
  }

  @Override
  public String toString() {
    return "H2dMaillageFrontiere main frontier[" + bords_[0].length + "] intern frontier [" + getNbFrontierIntern()
        + "]";
  }

  /**
   * Fonction de debogage: ecrit tous les indices des frontiere.
   */
  @Override
  public void printFullString() {
    FuLog.all("extern " + CtuluLibString.getObjectInString(bords_[0], true));
    if (getNbFrontierIntern() > 0) {
      final int n = getNbFrontierIntern();
      for (int i = 0; i < n; i++) {
        FuLog.all("intern " + i + CtuluLibString.getObjectInString(bords_[i + 1], true));
      }
    }
  }

  /**
   * @param _mail le maillage contenant cette frontiere
   * @param _idxFrontier la frontiere a considerer
   * @param _dest l'envelope a modifier: ne l'initialise pas !
   */
  @Override
  public void getMinMax(final EfGridInterface _mail, final int _idxFrontier, final Envelope _dest) {
    final int[] bordIdx = bords_[_idxFrontier];
    final int nb = bordIdx.length - 1;
    for (int i = nb; i >= 0; i--) {
      final int idx = bordIdx[i];
      _dest.expandToInclude(_mail.getPtX(idx), _mail.getPtY(idx));
    }
  }

  /**
   * @return le nombre de frontiere interne (total -1)
   */
  @Override
  public int getNbFrontierIntern() {
    return bords_.length - 1;
  }

  /**
   * @return le nombre total de frontiere
   */
  @Override
  public int getNbFrontier() {
    return bords_.length;
  }

  /**
   * Pas de verification !
   *
   * @param _idxOnFirstFrontier l'indiece sur la premiere frontiere
   * @return l'index du point n� <code>_idx</code> sur la fronti�re principale (externe)
   */
  @Override
  public int getIdxGlobalPrinc(final int _idxOnFirstFrontier) {
    return bords_[0][_idxOnFirstFrontier];
  }

  /**
   * @param _idxFrontier l'indice de la frontiere
   * @param _idxPt l'indice du point sur la frontiere
   * @return l'indice global du point n� _idxPt sur la frontier _idxFrontier
   */
  @Override
  public int getIdxGlobal(final int _idxFrontier, final int _idxPt) {
    return bords_[_idxFrontier][_idxPt];
  }

  @Override
  public void fillWithGobalIdx(final int _idxFr, final TIntHashSet _s) {
    _s.addAll(bords_[_idxFr]);
  }

  /**
   * @param _idxFrontier l'indice de la frontiere
   * @return le nombre de point contenu par la frontiere _idxFrontier
   */
  @Override
  public int getNbPt(final int _idxFrontier) {
    return bords_[_idxFrontier].length;
  }

  /**
   * Un iterateur sur les frontiere.
   *
   * @author Fred Deniger
   * @version $Id: EfFrontier.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
   */
  public class AllFrontiereIterator implements AllFrontierIteratorInterface {
    int bordIdx_;
    int ptIdx_;
    int frontierIdx_ = -1;

    AllFrontiereIterator() {
    }

    @Override
    public boolean hasNext() {
      int nextBordIdx = bordIdx_;
      if (mustIncrementBord) {
        nextBordIdx++;
      }
      return (nextBordIdx < bords_.length - 1) ? true
          : ((nextBordIdx == bords_.length - 1) ? (ptIdx_ < bords_[nextBordIdx].length) : false);
    }

    @Override
    public int getBordIDx() {
      return bordIdx_;
    }

    /**
     * @return l'indice du point sur les frontieres
     */
    @Override
    public int getFrontierIdx() {
      return frontierIdx_;
    }

    boolean mustIncrementBord;

    /**
     * Return the next index in the general numbering.
     */
    @Override
    public int next() {
      if (mustIncrementBord) {
        bordIdx_++;
        mustIncrementBord = false;
      }
      final int r = bords_[bordIdx_][ptIdx_++];
      frontierIdx_++;
      if (ptIdx_ == bords_[bordIdx_].length) {
        mustIncrementBord = true;
        ptIdx_ = 0;
      }
      return r;
    }
  }

  /**
   * Un iterateur sur une fronti�re donn�e. N'a pas de de limite : il renvoie les indices en boucle.
   *
   * @author Fred Deniger
   * @version $Id: EfFrontier.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
   */
  public class FrontierIterator implements FrontierIteratorInterface {
    int bordIdx_;
    int idx_;
    int idxOnFr_;
    boolean reverse_;

    /**
     * @return true si parcourt dans le sens inverse des indices
     */
    @Override
    public final boolean isReverse() {
      return reverse_;
    }

    /**
     * @param _reverse true si on veut parcourir dans le sens inverse.
     */
    @Override
    public final void setReverse(final boolean _reverse) {
      reverse_ = _reverse;
    }

    /**
     * Commence l'iteration a partir du premier point.
     *
     * @param _bordIdx le bord a parcourir
     */
    public FrontierIterator(final int _bordIdx) {
      bordIdx_ = _bordIdx;
    }

    /**
     * @param _bordIdx l'indice du bord a parcourir
     * @param _idxOnFr l'indice initial sur la frontiere
     */
    public FrontierIterator(final int _bordIdx, final int _idxOnFr) {
      bordIdx_ = _bordIdx;
      idxOnFr_ = _idxOnFr;
      if ((idxOnFr_ < 0) || (idxOnFr_ >= getNbPoint())) {
        throw new IllegalArgumentException("First index is out of bounds");
      }
    }

    /**
     * @return le nombre de point de cette frontiere
     */
    @Override
    public final int getNbPoint() {
      return bords_[bordIdx_].length;
    }

    /**
     * @return le nombre de points parcourus.
     */
    @Override
    public int getNbPtParcouru() {
      return idx_;
    }

    /**
     * @return l'indice de frontieres.
     */
    @Override
    public int getGlobalFrIdx() {
      return getFrontiereIndice(bordIdx_, idxOnFr_);
    }

    /**
     * @return l'indice global du dernier point itere
     */
    @Override
    public int getGlobalIdx() {
      return bords_[bordIdx_][idxOnFr_];
    }

    @Override
    public boolean hasNext() {
      return true;
    }

    /**
     * return l'indice dans cette frontiere.
     */
    @Override
    public int next() {
      idx_++;
      if (reverse_) {
        idxOnFr_--;
      } else {
        idxOnFr_++;
      }
      if (idxOnFr_ >= getNbPoint()) {
        idxOnFr_ = 0;
      } else if (idxOnFr_ < 0) {
        idxOnFr_ = getNbPoint() - 1;
      }
      return idxOnFr_;
    }
  }

  public CtuluListSelectionInterface getExternState() {
    return isExterne_;
  }
}
