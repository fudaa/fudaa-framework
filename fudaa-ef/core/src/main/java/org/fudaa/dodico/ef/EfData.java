/**
 * @creation 18 mars 2004
 * @modification $Date: 2007-01-10 09:04:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * @author Fred Deniger
 * @version $Id: EfData.java,v 1.4 2007-01-10 09:04:17 deniger Exp $
 */
public interface EfData extends CtuluCollectionDouble {

  /**
   * @return true si c'est des donn�es par elements
   */
  boolean isElementData();

}