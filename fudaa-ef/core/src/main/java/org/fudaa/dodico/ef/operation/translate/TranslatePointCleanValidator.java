/**
 *
 */
package org.fudaa.dodico.ef.operation.translate;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;

/**
 * Use to validate that no meshes are empty and that points are not overlapping.
 */
public class TranslatePointCleanValidator {
  public static final double DEFAULT_MIN_DISTANCE = 0.01;
  private double minDistance = DEFAULT_MIN_DISTANCE;
  private EfNeighborMesh neighborMesh;
  private final EfGridInterface grid;

  /**
   * @param grid the grid to set
   */
  public TranslatePointCleanValidator(EfGridInterface grid) {
    this.grid = grid;
  }

  public void setMinDistance(double minDistance) {
    this.minDistance = minDistance;
  }

  /**
   * @param ptIdx
   * @param prog
   * @param emptyMeshes
   * @param removedPoint
   * @param checkMeshes true if meshes should be checked.
   */
  public boolean shouldBeCleaned(int ptIdx, ProgressionInterface prog, TIntHashSet emptyMeshes, TIntIntHashMap removedPoint, boolean checkMeshes) {
    final int[] neighbors = this.getNeighbors(ptIdx, prog);

    double ptX = grid.getPtX(ptIdx);
    double ptY = grid.getPtY(ptIdx);
    boolean shouldBeCleaned = false;
    for (int i = 0; i < neighbors.length; i++) {
      final EfElement element = grid.getElement(neighbors[i]);
      if (checkMeshes) {
        int trigoOriente = element.isTrigoOriente(grid);
        if (element.getPtNb() > 3 && trigoOriente != 0) {
          if (grid.getAire(neighbors[i]) < (minDistance * minDistance)) {
            trigoOriente = 0;
          }
        }
        if (trigoOriente == 0) {
          if (emptyMeshes != null) {
            emptyMeshes.add(neighbors[i]);
          }
          shouldBeCleaned = true;
        }
      }
      int nbPoint = element.getPtNb();
      for (int idxPoint = 0; idxPoint < nbPoint; idxPoint++) {
        int otherPointIdx = element.getPtIndex(idxPoint);
        if (otherPointIdx != ptIdx) {
          double otherPtX = grid.getPtX(otherPointIdx);
          double otherPtY = grid.getPtY(otherPointIdx);
          if (CtuluLibGeometrie.getDistance(ptX, ptY, otherPtX, otherPtY) <= minDistance) {
            if (removedPoint != null) {
              removedPoint.put(ptIdx, otherPointIdx);
            }
            shouldBeCleaned = true;
          }
        }
      }
    }
    return shouldBeCleaned;
  }

  protected EfNeighborMesh getNeighborMesh(ProgressionInterface progressionInterface) {
    if (neighborMesh == null) {
      neighborMesh = EfNeighborMesh.compute(grid, progressionInterface);
    }
    return neighborMesh;
  }

  protected int[] getNeighbors(int ptIdx, ProgressionInterface prog) {
    getNeighborMesh(prog);
    final int[] neighbors = new int[neighborMesh.getNbNeighborMeshes(ptIdx)];

    for (int i = 0; i < neighbors.length; i++) {
      neighbors[i] = neighborMesh.getNeighborMesh(ptIdx, i);
    }

    return neighbors;
  }
}
