/*
 * @creation 7 d�c. 2005
 * 
 * @modification $Date: 2007-06-11 13:04:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.interpolation.InterpolationCollectionAdapter;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

/**
 * @author Fred Deniger
 * @version $Id: EfNeighborMesh.java,v 1.13 2007-06-11 13:04:05 deniger Exp $
 */
public class EfNeighborMesh {

  public static EfNeighborMesh compute(final EfGridInterface _grid, final ProgressionInterface _prog) {
    if (_grid == null) {
      return null;
    }
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.majProgessionStateOnly(DodicoLib.getS("Recherche des mailles voisines"));
    final int nbPt = _grid.getPtsNb();
    final int nbElt = _grid.getEltNb();
    final boolean isQ4 = _grid.getEltType() == EfElementType.Q4;
    // pour chaque point, on donne les mailles voisines
    final TIntArrayList[] meshes = new TIntArrayList[nbPt];
    for (int i = 0; i < nbPt; i++) {
      meshes[i] = new TIntArrayList(isQ4 ? 4 : 10);
    }
    up.setValue(7, nbElt, 10, 80);
    up.majProgessionStateOnly();
    for (int i = 0; i < nbElt; i++) {
      final EfElement elt = _grid.getElement(i);
      for (int j = elt.getPtNb() - 1; j >= 0; j--) {
        meshes[elt.getPtIndex(j)].add(i);
      }
      up.majAvancement();
    }
    final int[][] res = new int[nbPt][];
    if (_prog != null) {
      _prog.setProgression(90);
    }
    for (int i = 0; i < nbPt; i++) {
      int[] nativeArray = meshes[i].toNativeArray();
      res[i] = nativeArray;
    }
    return new EfNeighborMesh(res);
  }

  final int[][] neighbor_;

  /**
   * @param _neighbor
   */
  public EfNeighborMesh(final int[][] _neighbor) {
    super();
    neighbor_ = _neighbor;
  }

  public int getNumberOfPoints() {
    return neighbor_.length;
  }

  public double getAverageForNodeDanger(final int _idxPt, final CtuluCollectionDouble _dataElement) {
    final int nb = getNbNeighborMeshes(_idxPt);
    double sum = 0;
    for (int i = 0; i < nb; i++) {
      int idxElt = getNeighborMesh(_idxPt, i);
      sum += _dataElement.getValue(idxElt);
    }
    return sum / nb;
  }

  private double getAverageForVect(final CtuluVariable _var, final int _idxPt, final CtuluCollectionDouble _dataElement,
      InterpolationSupportValuesMultiI _datas, InterpolationVectorContainer _vects) {
    CtuluCollectionDouble vx = _vects.getVxValue(_var, _datas);
    CtuluCollectionDouble vy = _vects.getVyValue(_var, _datas);
    CtuluCollectionDouble norme = new InterpolationCollectionAdapter.Norm(vx, vy);
    CtuluCollectionDouble theta = new InterpolationCollectionAdapter.Theta(vx, vy);
    double n = getAverageForNodeDanger(_idxPt, norme);
    double t = getAverageForNodeDanger(_idxPt, theta);
    return _vects.isVx(_var) ? InterpolationVectorContainer.getVx(n, t) : InterpolationVectorContainer.getVy(n, t);
  }

  private double getAverageForVect(final CtuluVariable _var, final int _idxPt, final int _tIdx, final CtuluCollectionDouble _dataElement,
      EfGridData _datas, InterpolationVectorContainer _vects) {
    return getAverageForVect(_var, _idxPt, _dataElement, new EfGridDataInterpolationValuesAdapter(_datas, _tIdx), _vects);
  }

  public EfDataNode getDataNodeSimple(final CtuluCollectionDouble _data) {
    final double[] res = new double[getNumberOfPoints()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getAverageForNodeDanger(i, _data);
    }
    return new EfDataNode(res);
  }

  private EfDataNode getDataNodeVecteur(final CtuluVariable _var, final CtuluCollectionDouble _data, InterpolationSupportValuesMultiI _datas,
      InterpolationVectorContainer _vects) {
    CtuluCollectionDouble vx = _vects.getVxValue(_var, _datas);
    CtuluCollectionDouble vy = _vects.getVyValue(_var, _datas);
    if (vx == null || vy == null)
      return null;
    CtuluCollectionDouble norme = new InterpolationCollectionAdapter.Norm(vx, vy);
    CtuluCollectionDouble theta = new InterpolationCollectionAdapter.Theta(vx, vy);
    return new EfDataNode(_vects.isVx(_var) ? InterpolationVectorContainer.getVx(norme, theta) : InterpolationVectorContainer.getVy(norme, theta));
  }

  public int[] getAdjacentMeshes(final int _idxPt1, final int _idxPt2) {
    final TIntHashSet set = new TIntHashSet(neighbor_[_idxPt1]);
    int[] res = new int[2];
    int idx = 0;
    Arrays.fill(res, -1);

    int[] toTest = neighbor_[_idxPt2];
    for (int i = 0; i < toTest.length; i++) {
      if (set.contains(toTest[i])) {
        res[idx++] = toTest[i];
        if (idx == 2) {
          return res;
        }
      }
    }
    return res;

  }

  public int getAdjacentMeshes(final int _idxPt1, final int _idxPt2, final int _idxEltToAvoid) {
    final TIntHashSet set = new TIntHashSet(neighbor_[_idxPt1]);
    int[] toTest = neighbor_[_idxPt2];
    for (int i = 0; i < toTest.length; i++) {
      if (toTest[i] != _idxEltToAvoid && set.contains(toTest[i])) {
        return toTest[i];
      }
    }
    return -1;

  }

  /**
   * @param _grid le maillage
   * @param _idxElt l'�l�ment pour lequel on recherche les �l�ments qui l'entoure
   * @return l'indice des �l�ments l'entourant
   */
  public int[] getAdjacentMeshesFromElt(final EfGridInterface _grid, final int _idxElt) {
    final TIntArrayList adj = new TIntArrayList(6);
    final EfElement elt = _grid.getElement(_idxElt);
    for (int i = elt.getNbEdge() - 1; i >= 0; i--) {
      final int res = getAdjacentMeshes(elt.getEdgePt1(i), elt.getEdgePt2(i), _idxElt);
      if (res >= 0) {
        adj.add(res);
      }

    }
    return adj.toNativeArray();

  }

  public double getAverageForNode(final CtuluVariable _var, final int _idxPt, final int _tIdx, final EfData _dataElement, EfGridData _datas,
      InterpolationVectorContainer _vects) {
    if (_vects.isVect(_var)) {
      return getAverageForVect(_var, _idxPt, _tIdx, _dataElement, _datas, _vects);

    }
    return getAverageForNodeDanger(_idxPt, _dataElement);
  }

  public double getAverageForNode(final CtuluVariable _var, final int _idxPt, final int _tIdx, final EfData _dataElement,
      EfGridDataInterpolator _datas) {
    if (_datas.getVect().isVect(_var)) {
      return getAverageForVect(_var, _idxPt, _tIdx, _dataElement, _datas.getData(), _datas.getVect());

    }
    return getAverageForNodeDanger(_idxPt, _dataElement);
  }

  public double getAverageForNode(final CtuluVariable _var, final int _idxPt, final int _tIdx, EfGridData _datas, InterpolationVectorContainer _vects) {
    return getAverageForNode(_var, _idxPt, new EfGridDataInterpolationValuesAdapter(_datas, _tIdx), _vects);
  }

  public double getAverageForNode(final CtuluVariable _var, final int _idxPt, final int _tIdx, EfGridDataInterpolator _dataI) {
    return getAverageForNode(_var, _idxPt, new EfGridDataInterpolationValuesAdapter(_dataI.getData(), _tIdx), _dataI.getVect());
  }

  public double getAverageForNode(final CtuluVariable _var, final int _idxPt, InterpolationSupportValuesMultiI _datas,
      InterpolationVectorContainer _vects) {
    if (_vects!=null && _vects.isVect(_var)) {
      return getAverageForVect(_var, _idxPt, _datas.getValues(_var), _datas, _vects);
    }
    return getAverageForNodeDanger(_idxPt, _datas.getValues(_var));
  }

  public double getAverageForNode(final CtuluVariable _var, final int _idxPt, CtuluCollectionDouble _data, InterpolationSupportValuesMultiI _datas,
      InterpolationVectorContainer _vects) {
    if (_vects!=null && _vects.isVect(_var)) {
      return getAverageForVect(_var, _idxPt, _data, _datas, _vects);
    }
    return getAverageForNodeDanger(_idxPt, _data);
  }

  public EfDataNode getDataNode(final CtuluVariable _var, final CtuluCollectionDouble _data, InterpolationSupportValuesMultiI _datas,
      InterpolationVectorContainer _vects) {
    if (_vects!=null && _vects.isVect(_var)) {
      return getDataNodeVecteur(_var, _data, _datas, _vects);
    }
    return getDataNodeSimple(_data);

  }

  public EfDataNode getDataNode(final CtuluVariable _var, final int _tIdx, final EfData _data, EfGridData _datas, InterpolationVectorContainer _vects) {
    return getDataNode(_var, _data, new EfGridDataInterpolationValuesAdapter(_datas, _tIdx), _vects);
  }

  public EfDataNode getDataNode(final CtuluVariable _var, final int _tIdx, EfGridData _datas, InterpolationVectorContainer _vects) {
    return getDataNode(_var, new EfGridDataInterpolationValuesAdapter(_datas, _tIdx), _vects);
  }

  public EfDataNode getDataNode(final CtuluVariable _var, InterpolationSupportValuesMultiI _datas, InterpolationVectorContainer _vects) {
    if (_vects.isVect(_var)) {
      return getDataNodeVecteur(_var, _datas.getValues(_var), _datas, _vects);
    }
    return getDataNodeSimple(_datas.getValues(_var));

  }

  public int getMeshIdx(final int _idxPt, final int _idxMeshInNeighbor) {
    return neighbor_[_idxPt][_idxMeshInNeighbor];
  }

  public int getNbNeighborMeshes(final int _idxPt) {
    return neighbor_[_idxPt].length;
  }

  public int getNeighborMesh(final int _idxPt, final int _idxMesh) {
    return neighbor_[_idxPt][_idxMesh];
  }

  /**
   * @param _idxPt l'indice du noeud
   * @param _idxMeshes la liste des indices des �l�ments a remplir
   */
  public void fillWithAdjacentMeshes(final int _idxPt, TIntHashSet _idxMeshes) {
    _idxMeshes.addAll(neighbor_[_idxPt]);

  }
}
