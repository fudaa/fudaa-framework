/*
 * 
 * @creation 11 juin 07
 * @modification $Date: 2007-06-13 12:55:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionsCorrectionActivity.java,v 1.1 2007-06-13 12:55:42 deniger Exp $
 */
public class EfLineIntersectionsCorrectionActivity implements CtuluActivity {

  private boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
  }

  public EfLineIntersectionsResultsI correct(EfLineIntersectionsResultsI _res,
      EfLineIntersectionsCorrectionTester _tester, int _tidx, ProgressionInterface _prog) {
    stop_ = false;
    if (_tester == null || _res == null || _res.isEmpty() || !_tester.isEnableFor(_res)) return _res;
    int nbNew = _res.getNbIntersect() + 15;
    BitSet isOut = new BitSet(nbNew);
    final List setIn = new ArrayList(2);
    boolean isSomethingAdded = false;
    int nbSeg = _res.getNbIntersect() - 1;
    ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(10, nbSeg);
    List destIntersection = new ArrayList(nbNew);
    destIntersection.add(_res.getIntersect(0));
    // on parcourt les segment
    for (int i = 0; i < nbSeg; i++) {
      if (stop_) return null;
      // le tester va ajouter des intersections si n�cessaire:
      // on ne fait le test que pour les segments internes.
      boolean isSegmentOut = _res.isSegmentOut(i);
      // c'est un segment externe: on se contente de mettre � jour le flag out pour les res
      if (isSegmentOut) {
        // on met a jour le flag out
        // 'destIntersection.size()-1' represente l'indice du segment dans le resultat
        isOut.set(destIntersection.size() - 1);
      }
      // sinon, on regarde si le tester va ajouter de nouvelles intersection
      else {
        boolean add = _tester.createNews(_tidx, _res.getIntersect(i), _res.getIntersect(i + 1), setIn);
        // si oui, on met a jour la liste de destination
        if (add) {
          isSomethingAdded = true;
          destIntersection.addAll(setIn);
          // on ne met pas a jour le bitset out car les segments ajout�s sont des seg internes
          // on efface la liste tempo
          setIn.clear();
        }
      }

      // on ajoute la derni�re intersection
      destIntersection.add(_res.getIntersect(i + 1));
      // on met � jour

      up.majAvancement();
    }
    if (!isSomethingAdded) return _res;
    return new AdapterResults(_res, (EfLineIntersection[]) destIntersection
        .toArray(new EfLineIntersection[destIntersection.size()]), isOut);

  }

  private static class AdapterResults implements EfLineIntersectionsResultsI {
    final EfLineIntersectionsResultsI res_;
    final EfLineIntersection[] inters_;
    final BitSet out_;
    final double[] dist_;

    public AdapterResults(final EfLineIntersectionsResultsI _res, final EfLineIntersection[] _inters, final BitSet _out) {
      super();
      res_ = _res;
      inters_ = _inters;
      out_ = _out;
      dist_ = EfLineIntersectionsResultsMng.computeDist(this);
    }

    @Override
    public double getDistFromDeb(int _idxInter) {
      return dist_[_idxInter];
    }

    @Override
    public EfLineIntersection getIntersect(int _i) {
      return inters_[_i];
    }

    @Override
    public int getNbIntersect() {
      return inters_.length;
    }

    /**
     * on n'est sur que non.
     */
    @Override
    public boolean isAllOut() {
      return false;
    }

    /**
     * on n'est sur que non.
     */
    @Override
    public boolean isEmpty() {
      return false;
    }

    @Override
    public boolean isForMesh() {
      return res_.isForMesh();
    }

    @Override
    public boolean isSegmentIn(int _idxInters) {
      return !out_.get(_idxInters);
    }

    @Override
    public boolean isSegmentOut(int _idxInters) {
      return out_.get(_idxInters);
    }

    @Override
    public boolean isDataAvailable(CtuluVariable _v) {
      return res_.isDataAvailable(_v);
    }

  }
}
