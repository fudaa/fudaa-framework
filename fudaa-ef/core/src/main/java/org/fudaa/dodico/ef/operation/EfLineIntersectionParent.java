/*
 * @creation 27 oct. 06
 * @modification $Date: 2007-06-05 08:59:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionParent.java,v 1.2 2007-06-05 08:59:13 deniger Exp $
 */
public interface EfLineIntersectionParent {

  EfGridDataInterpolator getGridData();

  /**
   * @return un raccourci vers getGridData.getGrid...
   */
  EfGridInterface getGrid();

  void buildNeighbor(ProgressionInterface _prog);

  EfNeighborMesh getNeighbor();
}
