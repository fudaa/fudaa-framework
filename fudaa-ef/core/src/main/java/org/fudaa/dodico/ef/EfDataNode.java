/**
 * @creation 24 mars 2004
 * @modification $Date: 2007-01-10 09:04:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.ef;

import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.collection.CtuluArrayDoubleImmutable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;


/**
 * @author Fred Deniger
 * @version $Id: EfDataNode.java,v 1.2 2007-01-10 09:04:17 deniger Exp $
 */
public  class EfDataNode extends CtuluArrayDoubleImmutable implements EfData {

  /**
   * @param _init
   */
  public EfDataNode(final CtuluArrayDoubleImmutable _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public EfDataNode(final CtuluCollectionDouble _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public EfDataNode(final double[] _init) {
    super(_init);
  }

  /**
   * @param _nb
   */
  public EfDataNode(final int _nb) {
    super(_nb);
  }

  /**
   * @param _init
   */
  public EfDataNode(final Object[] _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public EfDataNode(final TDoubleArrayList _init) {
    super(_init);
  }

  @Override
  public final boolean isElementData(){
    return false;
  }





}