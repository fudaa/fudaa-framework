/*
 *  @creation     2 mai 2005
 *  @modification $Date: 2006-04-07 09:23:10 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

/**
 * @author Fred Deniger
 * @version $Id: EfFilter.java,v 1.2 2006-04-07 09:23:10 deniger Exp $
 */
public interface EfFilter {

  /**
   * @param _idxPt l'indice du point
   * @return true si pris en compte
   */
  boolean isActivated(int _idxPt);

  /**
   * @param _idxElt l'indice l'element
   * @return true si pris en compte
   */
  boolean isActivatedElt(int _idxElt);
  
  
  /**
   * @param _idxElt l'indice l'element
   * @param strict if true and if the selection is a node selection, a element is selected only if all its nodes are selected
   * @return true si pris en compte
   */
  boolean isActivatedElt(int _idxElt,boolean strict);

}
