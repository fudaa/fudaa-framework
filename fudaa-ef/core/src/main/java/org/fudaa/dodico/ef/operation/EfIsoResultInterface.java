/*
 * @creation 17 avr. 07
 * @modification $Date: 2007-04-20 16:21:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.gis.GISGeometry;

/**
 * Une interface a utiliser pour r�cup�rer les isolignes ou isosurfaces calcul�es par l'operation.
 * 
 * @author fred deniger
 * @version $Id: EfIsoResultInterface.java,v 1.1 2007-04-20 16:21:10 deniger Exp $
 */
public interface EfIsoResultInterface {

  /**
   * M�thode appel�e d�s qu'une iso a �t� trouv�e.
   * 
   * @param _g la g�om�trie trouv�e
   * @param _value la valeur de l'iso
   */
  void geometryFound(GISGeometry _g, double _value);

}
