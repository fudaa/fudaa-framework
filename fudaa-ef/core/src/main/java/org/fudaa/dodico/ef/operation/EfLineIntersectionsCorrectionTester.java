/*
 * @creation 11 juin 07
 * @modification $Date: 2007-06-13 12:55:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import java.util.List;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionsCorrectionTester.java,v 1.1 2007-06-13 12:55:42 deniger Exp $
 */
public interface EfLineIntersectionsCorrectionTester {
  
  /**
   * @param _res le r�sultat qui sera corrig�
   * @return true si cette correction est active pour le resultat donne
   */
  boolean isEnableFor(EfLineIntersectionsResultsI _res);

  boolean createNews(int _tidx, EfLineIntersection _i1, EfLineIntersection _i2, List _setNewIn);

}
