/*
 * @creation 20 avr. 07
 * @modification $Date: 2007-06-11 13:04:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridDataInterpolationValuesAdapter;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.impl.EfRefinedGridAddedPt;

/**
 * @author fred deniger
 * @version $Id: EfIsoRestructuredGridResult.java,v 1.3 2007-06-11 13:04:06 deniger Exp $
 */
public class EfIsoRestructuredGridResult {

  private final EfRefinedGridAddedPt newGrid_;

  public EfIsoRestructuredGridResult(final EfRefinedGridAddedPt _newGrid) {
    super();
    newGrid_ = _newGrid;
  }

  public EfRefinedGridAddedPt getNewGrid() {
    return newGrid_;
  }

  public EfDataNode getNewValues(CtuluVariable _var, int _tidx, EfData _elt, EfGridData _datas,
      InterpolationVectorContainer _vects, ProgressionInterface _prog, CtuluAnalyze _analyze) {
    return getNewValues(_var, _elt, new EfGridDataInterpolationValuesAdapter(_datas, _tidx), _vects, _prog, _analyze);
  }

  public EfDataNode getNewValues(CtuluVariable _var, CtuluCollectionDouble _elt,
      InterpolationSupportValuesMultiI _datas, InterpolationVectorContainer _vects, ProgressionInterface _prog,
      CtuluAnalyze _analyze) {
    if (_elt == null) return null;
    if (_elt.getSize() != newGrid_.getOld().getEltNb()) {
      FuLog.error("DEF: bad size for _elt " + _elt.getSize() + " expected " + newGrid_.getOld().getEltNb());
      return null;
    }
    // on va calculer les valeurs sur le nouveau maillage
    EfNeighborMesh neig = newGrid_.computeNeighborForOld(_prog, _analyze);
    if (neig == null) return null;
    double[] newVal = new double[newGrid_.getPtsNb()];
    int oldPtsNb = newGrid_.getOldNbPt();
    for (int i = 0; i < oldPtsNb; i++) {
      newVal[i] = neig.getAverageForNode(_var, i, _elt, _datas, _vects);
    }
    // les centres
    for (int i = oldPtsNb; i < newGrid_.getPtsNb(); i++) {
      newVal[i] = _elt.getValue(i - oldPtsNb);
    }
    return new EfDataNode(newVal);
  }

}
