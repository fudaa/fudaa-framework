/*
 * @creation 18 avr. 07
 * 
 * @modification $Date: 2007-06-28 09:25:08 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TDoubleObjectHashMap;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometry;
import org.fudaa.ctulu.gis.GISPoint;

/**
 * @author fred deniger
 * @version $Id: EfIsoResultDefault.java,v 1.3 2007-06-28 09:25:08 deniger Exp $
 */
public class EfIsoResultDefault implements EfIsoResultInterface, GISDataModel {

  private final List<GISGeometry> res_ = new ArrayList<GISGeometry>();
  private List values_ = new ArrayList();
  TDoubleObjectHashMap pool_ = new TDoubleObjectHashMap();

  private Double createDouble(double _d) {
    Double val = (Double) pool_.get(_d);
    if (val == null) {
      val = CtuluLib.getDouble(_d);
      pool_.put(_d, val);
    }
    return val;

  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    EfIsoResultDefault res = new EfIsoResultDefault();
    res.values_ = values_;
    res.pool_ = pool_;
    for (GISGeometry geom : res_) {
      res.res_.add(geom.createTranslateGeometry(xyToAdd));
    }
    return res;
  }

  @Override
  public void geometryFound(GISGeometry _g, double _value) {
    res_.add(_g);
    values_.add(createDouble(_value));

  }

  GISAttributeInterface att_;

  @Override
  public GISAttributeInterface getAttribute(int _idxAtt) {
    return att_;
  }

  @Override
  public double getDoubleValue(int _idxAtt, int _idxGeom) {
    return ((Double) values_.get(_idxGeom)).doubleValue();
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return null;
  }

  @Override
  public Geometry getGeometry(int _idxGeom) {
    return (Geometry) res_.get(_idxGeom);
  }

  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    if(att_==null){
      return 0;
    }
    return att_.equals(_att)?0:-1;
  }

  @Override
  public int getNbAttributes() {
    return 1;
  }

  @Override
  public int getNumGeometries() {
    return res_.size();
  }

  @Override
  public Object getValue(int _idxAtt, int _idxGeom) {
    return values_.get(_idxGeom);
  }

  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
  }

  public GISAttributeInterface getAtt() {
    return att_;
  }

  public void setAtt(GISAttributeInterface _att) {
    att_ = _att;
  }

}
