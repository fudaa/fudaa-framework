/*
 * @creation 19 avr. 07
 * @modification $Date: 2007-04-20 16:21:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation.refine;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntIntHashMap;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author fred deniger
 * @version $Id: EfRefinementMiddleActivity.java,v 1.1 2007-04-20 16:21:12 deniger Exp $
 */
public class EfRefinementMiddleActivity implements CtuluActivity {

  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
  }

  public EfRefinementMiddleResult transform(EfGridInterface _g, ProgressionInterface _prog,
      CtuluListSelectionInterface _selectedElt) {
    final int nbElt = _g.getEltNb();
    final int nbPt = _g.getPtsNb();
    boolean isT6 = _g.getEltType() == EfElementType.T6;
    // le nombre de points cr�es: si T6, on recr�era des T6 au milieu
    int nbNewPt = isT6 ? 4 * nbElt : nbElt;
    // cette table permettra de faire la relation 'index point ajout�' -> 'indice de l'ancien �lement le contenant'
    final TIntIntHashMap newPtOldMesh = new TIntIntHashMap(nbNewPt);
    // dans le cadre d'un maillage T4 ( le pire que nous g�rons actuellement), il y aura 4 �l�ments cr��es � partir d'un
    // sinon c'est 3. Donc on prend le pire (*4)
    final List newElement = new ArrayList(nbElt * 4);
    // cette table permettra de faire la relation 'index nouveau element' -> 'indice de l'ancien �lement le contenant'
    final TIntIntHashMap newEltOldElt = new TIntIntHashMap(nbElt * 4);
    // un point sera ajout� par �l�ment (au pire)donc
    final TDoubleArrayList newXYZ = new TDoubleArrayList(nbNewPt * 3);
    // l'indice de l'�l�ment en cours
    // le nombre de points par element cr��
    final int nbPtByNewElt = isT6 ? 6 : 3;
    ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(10, nbElt);
    up.majProgessionStateOnly(DodicoLib.getS("Raffinement du maillage"));
    for (int i = 0; i < nbElt; i++) {

      if (stop_) return null;

      EfElement el = _g.getElement(i);
      
      // l'�l�ment en question ne doit pas �tre raffine
      if (_selectedElt != null && !_selectedElt.isSelected(i))
      {
          newEltOldElt.put(newElement.size(), i);
          newElement.add(el);
	  
    	  continue;
      }
      
      // l'indice du point milieu dans le futur maillage
      // on va commencer par enregistrer le point milieu
      final int idxMiddlePt = newXYZ.size() / 3 + nbPt;
      // ce point appartient � l'ancien elt i
      newPtOldMesh.put(idxMiddlePt, i);
      // on utilse la moyenne pour calcul le centre : impos� par rubar
      double xMiddle = _g.getMoyCentreXElement(i);
      double yMiddle = _g.getMoyCentreYElement(i);
      double zMiddle = _g.getMoyCentreZElement(i);

      newXYZ.add(xMiddle);
      newXYZ.add(yMiddle);
      newXYZ.add(zMiddle);
      // les ordres de parcours ci-dessous sont importants!
      // si T6 on cr�e les points milieux des aretes internes
      if (isT6) {
        for (int j = 0; j < el.getNbEdge(); j++) {
          final int idx = el.getEdgePt1(j);
          newXYZ.add((xMiddle + _g.getPtX(idx)) / 2);
          newXYZ.add((yMiddle + _g.getPtY(idx)) / 2);
          newXYZ.add((zMiddle + _g.getPtZ(idx)) / 2);
          // ce nouveau point appartient � l'�l�ment i.
          // son indice c'est celui du point milieu plus le numero de l'arete plus un
          // on pourrait faire aussi newXYZ.size() / 3 + nbPt
          newPtOldMesh.put(idxMiddlePt + j + 1, i);
        }
      }
      // on va cr�er les nouveaux �l�ments
      for (int j = 0; j < el.getNbEdge(); j++) {
        if (isT6) {
          // c'est la ....
          /**
           * l'ordre est
           * 
           * <pre>
           * [arete j pt 1] - [milieu arete j] - [arete j pt2] - [milieu entre pt 2 et nouveau centre] - [nouveau centre] - [milieu entre pt 1 et nouveau centre]
           * </pre>
           */
          int[] idx = new int[nbPtByNewElt];
          // on commence par les points extr�mit�s: les plus simple
          // les 2 points sur l'arete
          idx[0] = el.getEdgePt1(j);
          idx[2] = el.getEdgePt2(j);
          // le point milieu cr�e
          idx[4] = idxMiddlePt;

          // ensuite les points milieux du nouvel �l�ment
          // le premier est simple: c'est le point milieu de l'arete
          idx[1] = el.getEdgePtMiddle(j);
          // les 2 autres: plus compliqu�
          // par constuction, le point d'indice idxMiddlePt+1+j est le point situ�
          // entre le point 1 de l'arete j et le nouveau centre donc pour le 5 indice
          //
          idx[5] = idxMiddlePt + 1 + j;
          // pour le point 2 de l'arete j, il s'agit du point 1 de l'arete (j+1).
          // on utilise le reste de la division euclidienne pour rester dans les clous
          // si j vaut 2, on doit utiliser la premi�re arete interne
          idx[3] = idxMiddlePt + 1 + (j + 1) % 3;
          // on enregistre le nouvel �l�ment
          newEltOldElt.put(newElement.size(), i);
          newElement.add(new EfElement(idx));

        } else {
          int[] idx = new int[nbPtByNewElt];
          idx[0] = el.getEdgePt1(j);
          idx[1] = el.getEdgePt2(j);
          idx[2] = idxMiddlePt;
          // on enregiste l'indice
          newEltOldElt.put(newElement.size(), i);
          newElement.add(new EfElement(idx));

        }

      }
      up.majAvancement();
    }
    return new EfRefinementMiddleResult(_g, newXYZ.toNativeArray(), (EfElement[]) newElement
        .toArray(new EfElement[newElement.size()]), newPtOldMesh, newEltOldElt);
  }
}
