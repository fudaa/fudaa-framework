/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.geom.LineString;

/**
 * une implémentation par defaut avec les z comme attributs.
 *
 * @author Frederic Deniger
 */
public abstract class AbstractTriangulationPolyData implements TriangulationPolyDataInterface {

  LineString[] lines = new LineString[0];
  boolean[] holes = new boolean[0];
  boolean useZAsAttribute;

  public void setUseZAsAttribute(boolean useZAsAttribute) {
    this.useZAsAttribute = useZAsAttribute;
  }

  public AbstractTriangulationPolyData() {
  }

  public void setHoles(boolean[] holes) {
    this.holes = holes;
  }

  public void setLineString(LineString[] rings) {
    this.lines = rings;
  }

  @Override
  public boolean isClosed(int idxLine) {
    return lines[idxLine].isRing();
  }

  public int getNbAttributes() {
    return useZAsAttribute ? 1 : 0;
  }

  @Override
  public int getNbLines() {
    return lines.length;
  }

  @Override
  public LineString getLine(int idxPoly) {
    return lines[idxPoly];
  }

  @Override
  public boolean isClosedAndHole(int idxPoly) {
    return isClosed(idxPoly) && holes[idxPoly];
  }

  public double getPolyAttributes(int idxPoly, int idxPtInPoly, int idxAtt) {
    return getLine(idxPoly).getCoordinateSequence().getCoordinate(idxPtInPoly).z;
  }
}
