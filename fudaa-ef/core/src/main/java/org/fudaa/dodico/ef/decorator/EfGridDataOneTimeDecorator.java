/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.decorator;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

public class EfGridDataOneTimeDecorator implements EfGridData {

  final EfGridData init_;

  final int tIdx_;
  public EfGridDataOneTimeDecorator(EfGridData _init, int _idx) {
    super();
    init_ = _init;
    tIdx_ = _idx;
  }

  @Override
  public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
    return init_.getData(_o, tIdx_);
  }

  @Override
  public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
    return init_.getData(_o, tIdx_, _idxObjet);
  }

  @Override
  public EfGridInterface getGrid() {
    return init_.getGrid();
  }

  @Override
  public boolean isDefined(CtuluVariable _var) {
    return init_.isDefined(_var);
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return init_.isElementVar(_idxVar);
  }
}