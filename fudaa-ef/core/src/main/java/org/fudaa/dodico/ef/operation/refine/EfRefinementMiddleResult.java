/*
 * @creation 19 avr. 07
 * @modification $Date: 2007-06-11 13:04:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation.refine;

import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.impl.EfRefinedGridAddedPt;

/**
 * @author fred deniger
 * @version $Id: EfRefinementMiddleResult.java,v 1.3 2007-06-11 13:04:06 deniger
 *          Exp $
 */
public class EfRefinementMiddleResult {

    final EfRefinedGridAddedPt grid_;
    final TIntIntHashMap newPtOldElt_;
    final TIntIntHashMap newEltOldElt_;

  EfRefinementMiddleResult(EfGridInterface _old, double[] _ptAdded, EfElement[] _newElt, TIntIntHashMap _newPtOldElt,
            TIntIntHashMap _newEltOldElt) {
        grid_ = new EfRefinedGridAddedPt(_newElt, _ptAdded, EfElementType.T3, _old);
        newEltOldElt_ = _newEltOldElt;
        newPtOldElt_ = _newPtOldElt;
    }

    /**
     * @param _idxPt l'indice du point dans le nouveau maillage
     * @return true si c'est un point rajout� par le raffinage.
     */
  public boolean isAddedPoint(int _idxPt) {
        return _idxPt >= grid_.getOldNbPt();
    }

    /**
     * @param _idxPt l'indice du nouveau point
     * @return l'indice de l'ancien element contenant le nouveau point
     */
    public int getOldMeshIdxForAddedPt(int _idxPt) {
        return isAddedPoint(_idxPt) ? newPtOldElt_.get(_idxPt) : -1;
    }

    public int getOldMeshIdxForAddedMesh(int idxNewElt) {
        return newEltOldElt_.get(idxNewElt);
    }

    /**
     * @param _idxNewPt l'indice du nouveau point ajoute
     * @param _oldPtIdx l'ancien point
     * @return true si les 2 points appartiennent au ancien �l�ment
     */
  public boolean isOnSameOldMesh(int _idxNewPt, int _oldPtIdx) {
        int oldMesh = getOldMeshIdxForAddedPt(_idxNewPt);
        if (oldMesh >= 0) {
            return grid_.getOld().getElement(oldMesh).containsIndex(_oldPtIdx);
    }
        return false;
    }

  public EfRefinedGridAddedPt getNewGrid() {
        return grid_;
    }

    /**
     * @param _var la variable
     * @param _tidx le pas de temps
     * @param _eltData les donn��es d�finies sur l'element
     * @param _oldValues
     * @param _vect
     * @param _prog
     * @param _analyze
     * @return
     */
  public EfDataNode getNodeDataFromOldEltData(CtuluVariable _var, int _tidx, EfData _eltData, EfGridData _oldValues,
            InterpolationVectorContainer _vect, ProgressionInterface _prog, CtuluAnalyze _analyze) {
        if (_eltData == null)
            return null;
        double[] res = new double[grid_.getPtsNb()];
        EfNeighborMesh oldNeighbor = grid_.computeNeighborForOld(_prog, _analyze);
        if (oldNeighbor == null)
            return null;

        // on commence par affecter les anciens points avec la moyenne des �l�ments adjacents
        for (int i = 0; i < grid_.getOldNbPt(); i++) {
            res[i] = oldNeighbor.getAverageForNode(_var, i, _tidx, _eltData, _oldValues, _vect);
        }
        // les points ajout�s: ce sont des points milieux donc, on retrouve l'ancien elt et hop!
        for (int i = grid_.getOldNbPt(); i < res.length; i++) {
            int oldElt = newPtOldElt_.get(i);

      res[i] = _eltData.getValue(oldElt);
    }
        return new EfDataNode(res);
    }

}
