/**
 *
 */
package org.fudaa.dodico.ef.operation.translate;

import gnu.trove.TIntIntHashMap;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridDefaultAbstract;

/**
 * A grid with a serie of translated points.
 */
public class EfGridPointsTranslated extends EfGridDefaultAbstract {
  private final EfGridInterface initialGrid;
  private final TIntIntHashMap translatedIdx = new TIntIntHashMap();
  private double[] newX;
  private double[] newY;

  private  int indexToKeepXY=-1;


  public EfGridPointsTranslated(EfGridInterface initialGrid, int[] translatedPt, double[] newX, double[] newY) {
    this.initialGrid = initialGrid;
    this.newX = newX;
    this.newY = newY;
    for (int i = 0; i < translatedPt.length; i++) {
      translatedIdx.put(translatedPt[i], i);
    }
  }

  public EfGridInterface getInitialGrid() {
    return initialGrid;
  }

  public void setIndexToKeepXY(int indexToKeepXY) {
    this.indexToKeepXY = indexToKeepXY;
  }

  @Override
  public EfElement getElement(int _i) {
    return this.initialGrid.getElement(_i);
  }

  @Override
  public int getEltNb() {
    return this.initialGrid.getEltNb();
  }

  @Override
  public int getPtsNb() {
    return this.initialGrid.getPtsNb();
  }

  @Override
  public double getPtX(int _i) {
    if (_i!=indexToKeepXY && translatedIdx.containsKey(_i)) {
      return newX[translatedIdx.get(_i)];
    }
    return this.initialGrid.getPtX(_i);
  }

  @Override
  public double getPtY(int _i) {
    if (_i!=indexToKeepXY && translatedIdx.containsKey(_i)) {
      return newY[translatedIdx.get(_i)];
    }
    return this.initialGrid.getPtY(_i);
  }

  @Override
  public double getPtZ(int _i) {
    return this.initialGrid.getPtZ(_i);
  }

  @Override
  public void setPt(int _i, double _x, double _y) {
    this.setPt(_i, _x, _y, 0.0);
  }

  @Override
  public void setPt(int _i, double _x, double _y, double _z) {
  }

  @Override
  protected boolean setZIntern(int _i, double _newV) {
    return this.setZIntern(_i, _newV);
  }
}
