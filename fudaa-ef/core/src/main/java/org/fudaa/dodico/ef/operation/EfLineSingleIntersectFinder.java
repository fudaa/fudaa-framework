/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import gnu.trove.TIntIterator;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 */
public class EfLineSingleIntersectFinder {
  EfIndexVisitorHashSet _eltSelected = new EfIndexVisitorHashSet();

  final EfGridInterface grid_;

  public EfLineSingleIntersectFinder(EfGridInterface _grid) {
    super();
    grid_ = _grid;
  }

  double tolerance_ = 1E-3;

  public EfLineIntersection findIntersectionFor(double _x, double _y, ProgressionInterface _prog) {
    final int startElt = EfIndexHelper.getElementEnglobant(grid_, _x, _y, _prog);
    if (startElt >= 0) {
      EfElement elt = grid_.getElement(startElt);
      int idxSelected = getSelectedNodeIdx(_x, _y, elt);
      if (idxSelected >= 0) {
        return new EfLineIntersection.ItemNode(idxSelected);
      } else {
        // sur une arete ?
        double tolerance = getTolerance();
        for (int i = 0; i < elt.getNbEdge(); i++) {
          int i1 = elt.getEdgePt1(i);
          int i2 = elt.getEdgePt2(i);
          if (CtuluLibGeometrie.distanceFromSegment(grid_.getPtX(i1), grid_.getPtY(i1), grid_.getPtX(i2), grid_
              .getPtY(i2), _x, _y) <= tolerance) { return new EfLineIntersection.ItemEdge(i1, i2, _x, _y); }
        }
        // pas une arete
        return new EfLineIntersection.ItemMesh(startElt, _x, _y);
      }

    } else {
      return new EfLineIntersection.ItemNoIntersect(_x, _y);
    }

  }

  protected int getContainingElement(double _x, double _y, TIntIterator _it) {
    return getContainingElement(grid_, _x, _y, _it, getTolerance());

  }

  private int getSelectedNodeIdx(double _x, double _y, EfElement elt) {
    return getSelectedNodeIdx(grid_, _x, _y, elt, getTolerance());
  }

  /**
   * @return the tolerance
   */
  public double getTolerance() {
    return tolerance_;
  }

  /**
   * @param _tolerance the tolerance to set
   */
  public void setTolerance(double _tolerance) {
    tolerance_ = _tolerance;
  }

  protected static int getContainingElement(EfGridInterface _grid, double _x, double _y, TIntIterator _it,
      double _tolerance) {
    EfIndexVisitorNearestElt visitor = new EfIndexVisitorNearestElt(_grid, _x, _y, _tolerance / 10);
    while (_it.hasNext()) {
      visitor.visitItem(_it.next());
    }
    int idx = visitor.getSelected();
    // un element proche a �t� trouv� mais le point (_x,_y) n'appartient pas � ce point
    if (idx >= 0 && !visitor.isIn()) {
      // on tol�re une erreur de 1E-3: si la distance entre l'element et le point est inferieur a 1E-3 on renvoie quand
      // meme cet element
      idx = (_grid.getElement(idx).getDist(_x, _y, _grid) <= _tolerance) ? idx : -1;
    }
    return idx;

  }

  static int getSelectedNodeIdx(EfGridInterface _grid, double _x, double _y, EfElement elt, double tolerance) {
    int idxSelected = -1;
    for (int i = elt.getPtNb() - 1; i >= 0 && idxSelected < 0; i--) {
      final int idx = elt.getPtIndex(i);
      if (CtuluLibGeometrie.getDistance(_x, _y, _grid.getPtX(idx), _grid.getPtY(idx)) <= tolerance) {
        idxSelected = idx;
      }
    }
    return idxSelected;
  }

}
