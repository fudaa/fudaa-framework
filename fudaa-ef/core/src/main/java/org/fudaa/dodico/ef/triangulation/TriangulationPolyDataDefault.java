/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.geom.Coordinate;

/**
 * une impléementation par defaut avec les z comme attributs.
 *
 * @author Frederic Deniger
 */
public class TriangulationPolyDataDefault extends AbstractTriangulationPolyData {

  Coordinate[] coordinates = new Coordinate[0];

  public TriangulationPolyDataDefault() {
  }

  public void setCoordinates(Coordinate[] coordinates) {
    this.coordinates = coordinates;
  }

  @Override
  public int getPtsNb() {
    return coordinates.length;
  }

  @Override
  public double getPtX(int idxPt) {
    return coordinates[idxPt].x;
  }

  @Override
  public double getPtY(int idxPt) {
    return coordinates[idxPt].y;
  }

  public double getPtAttributes(int idxPt, int idxAtt) {
    return coordinates[idxPt].z;
  }
}
