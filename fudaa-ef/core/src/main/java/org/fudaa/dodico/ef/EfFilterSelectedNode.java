/*
 * @creation 13 mai 2005
 * 
 * @modification $Date: 2006-09-11 09:45:07 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;

/**
 * Un filtre sur des indices de selections: Attention le constructeur peut etre long.
 * 
 * @author Fred Deniger
 * @version $Id: EfFilterSelectedNode.java,v 1.3 2006-09-11 09:45:07 deniger Exp $
 */
public class EfFilterSelectedNode implements EfFilter {

  final CtuluListSelectionInterface selectedNode_;
  // -1 non selectione
  // 0 selectionne partiellement
  // 1 tout select
  int[] selectedElt_;

  /**
   * @param _selectedPt les elements selectionnées
   * @param _g
   */
  public EfFilterSelectedNode(final CtuluListSelectionInterface _selectedPt, final EfGridInterface _g) {
    super();
    selectedNode_ = _selectedPt;
    if (selectedNode_ != null && !selectedNode_.isEmpty()) {
      selectedElt_ = new int[_g.getEltNb()];
      // on parcourt les elements pour determiner leur etat
      for (int i = _g.getEltNb() - 1; i >= 0; i--) {
        final EfElement el = _g.getElement(i);
        boolean selected = false;
        boolean allSelected = true;
        for (int j = el.getPtNb() - 1; j >= 0; j--) {
          if (_selectedPt.isSelected(el.getPtIndex(j))) {
            selected = true;
          } else {
            allSelected = false;
          }
        }
        int res = -1;
        if (selected) {
          res++;
        }
        if (allSelected) {
          res++;
        }
        selectedElt_[i] = res;
      }
    }
  }

  @Override
  public boolean isActivated(final int _idxPt) {
    return selectedNode_ == null ? false : selectedNode_.isSelected(_idxPt);
  }

  public CtuluListSelectionInterface getSelectedElt() {
    CtuluListSelection res = new CtuluListSelection();
    if (selectedElt_ == null) return res;
    for (int i = selectedElt_.length - 1; i >= 0; i--) {
      if (isActivatedElt(i)) res.add(i);
    }
    return res;
  }

  @Override
  public boolean isActivatedElt(final int _idxElt) {
    return (selectedElt_ != null && selectedElt_[_idxElt] >= 0);
  }

  @Override
  public boolean isActivatedElt(int _idxElt, boolean strict) {
    return (selectedElt_ != null && selectedElt_[_idxElt] > 0);
  }

}
