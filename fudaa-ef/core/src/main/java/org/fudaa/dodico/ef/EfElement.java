/**
 * @creation 25 juin 2003
 * @modification $Date: 2007-06-20 12:21:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.interpolation.InterpolationCollectionAdapter;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;

import java.util.Arrays;

/**
 * Les elements d'un maillage.
 *
 * @author deniger
 * @version $Id: EfElement.java,v 1.28 2007-06-20 12:21:49 deniger Exp $
 */
public class EfElement {
  /**
   * Renvoie true, si un des elements <code>_elems</code> contient le segment <code>[_s1,_s2]</code> dans le sens
   * donne.
   *
   * @param _elems les elements a tester
   * @param _s1 l'indice 1
   * @param _s2 l'indice 2
   * @return true si contenu
   */
  public static boolean containsSegment(final EfElement[] _elems, final int _s1, final int _s2) {
    final int n = _elems.length - 1;
    for (int i = n; i >= 0; i--) {
      if (_elems[i].containsSegment(_s1, _s2)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _x le x a tester
   * @param _y le y a tester
   * @param _g la maillage
   * @return true si (_x,_y) contenu
   */
  public boolean contientXY(final double _x, final double _y, final EfGridInterface _g) {
    final int nb = getPtNb();
    if (nb <= 2) {
      return false;
    }
    int recu = 0;
    int idx = getPtIndex(nb - 1);
    double finx = _g.getPtX(idx);
    double finy = _g.getPtY(idx);
    double xElt, yElt;
    for (int i = 0; i < nb; finx = xElt, finy = yElt, i++) {
      idx = getPtIndex(i);
      xElt = _g.getPtX(idx);
      yElt = _g.getPtY(idx);
      if (yElt == finy) {
        continue;
      }
      double lx;
      if (xElt < finx) {
        if (_x >= finx) {
          continue;
        }
        lx = xElt;
      } else {
        if (_x >= xElt) {
          continue;
        }
        lx = finx;
      }
      double test1, test2;
      if (yElt < finy) {
        if (_y < yElt || _y >= finy) {
          continue;
        }
        if (_x < lx) {
          recu++;
          continue;
        }
        test1 = _x - xElt;
        test2 = _y - yElt;
      } else {
        if (_y < finy || _y >= yElt) {
          continue;
        }
        if (_x < lx) {
          recu++;
          continue;
        }
        test1 = _x - finx;
        test2 = _y - finy;
      }
      if (test1 < (test2 / (finy - yElt) * (finx - xElt))) {
        recu++;
      }
    }
    return ((recu & 1) != 0);
  }

  /**
   * @param _support le maillage support
   * @param _dest le point prenant les donn�es
   * @return true si op�ration r�ussie
   */
  public boolean getMilieuXY(final EfGridInterface _support, final Coordinate _dest) {
    if (_support == null || _dest == null) {
      return false;
    }
    final Coordinate n1 = _support.getCoor(ptIndex_[ptIndex_.length - 1]);
    final Coordinate n2 = _support.getCoor(ptIndex_[0]);
    final Coordinate n3 = _support.getCoor(ptIndex_[1]);
    double q = CtuluLibGeometrie.getDistanceXY(n1, n2) + CtuluLibGeometrie.getDistanceXY(n3, n2);
    double x = n2.x * q;
    double y = n2.y * q;
    double qt = q;
    for (int i = 1; i < ptIndex_.length; i++) {
      n1.setCoordinate(n2);
      n2.setCoordinate(n3);
      if (i == ptIndex_.length - 1) {
        _support.getPt(ptIndex_[0], n3);
      } else {
        _support.getPt(ptIndex_[i + 1], n3);
      }
      q = CtuluLibGeometrie.getDistanceXY(n1, n2) + CtuluLibGeometrie.getDistanceXY(n3, n2);
      x += q * n2.x;
      y += q * n2.y;
      qt += q;
    }
    _dest.x = x / qt;
    _dest.y = y / qt;
    _dest.z = getMilieuZ(_support);
    return true;
  }

  public double getMoyMilieuX(final EfGridInterface _support) {
    if (_support == null) {
      return 0;
    }
    double x = 0;
    final int nb = getPtNb();
    for (int i = nb - 1; i >= 0; i--) {
      x = x + _support.getPtX(getPtIndex(i));
    }
    return x / nb;
  }

  public double getMoyMilieuZ(final EfGridInterface _support) {
    if (_support == null) {
      return 0;
    }
    double z = 0;
    final int nb = getPtNb();
    for (int i = nb - 1; i >= 0; i--) {
      z = z + _support.getPtZ(getPtIndex(i));
    }
    return z / ((double)nb);
  }

  public double getMoyMilieuY(final EfGridInterface _support) {
    if (_support == null) {
      return 0;
    }
    double y = 0;
    final int nb = getPtNb();
    for (int i = nb - 1; i >= 0; i--) {
      y = y + _support.getPtY(getPtIndex(i));
    }
    return y / nb;
  }

  public double getMilieuX(final EfGridInterface _support) {
    if (_support == null) {
      return 0;
    }
    final Coordinate n1 = _support.getCoor(ptIndex_[ptIndex_.length - 1]);
    final Coordinate n2 = _support.getCoor(ptIndex_[0]);
    final Coordinate n3 = _support.getCoor(ptIndex_[1]);
    double q = CtuluLibGeometrie.getDistanceXY(n1, n2) + CtuluLibGeometrie.getDistanceXY(n3, n2);
    double x = n2.x * q;
    // double y = n2.getY() * q;
    double qt = q;
    for (int i = 1; i < ptIndex_.length; i++) {
      n1.setCoordinate(n2);
      n2.setCoordinate(n3);
      if (i == ptIndex_.length - 1) {
        _support.getPt(ptIndex_[0], n3);
      } else {
        _support.getPt(ptIndex_[i + 1], n3);
      }
      q = CtuluLibGeometrie.getDistanceXY(n1, n2) + CtuluLibGeometrie.getDistanceXY(n3, n2);
      x += q * n2.x;
      // y += q * n2.getY();
      qt += q;
    }
    return x / qt;
  }

  public double getMilieuZ(final EfGridInterface _support) {
    double z = 0;
    for (int i = 0; i < ptIndex_.length; i++) {
      z += _support.getPtZ(ptIndex_[i]);
    }
    return z / ptIndex_.length;
  }

  public double getMilieuY(final EfGridInterface _support) {
    if (_support == null) {
      return 0;
    }
    final Coordinate n1 = _support.getCoor(ptIndex_[ptIndex_.length - 1]);
    final Coordinate n2 = _support.getCoor(ptIndex_[0]);
    final Coordinate n3 = _support.getCoor(ptIndex_[1]);
    double q = CtuluLibGeometrie.getDistanceXY(n1, n2) + CtuluLibGeometrie.getDistanceXY(n3, n2);
    // double x = n2.getX() * q;
    double y = n2.y * q;
    double qt = q;
    for (int i = 1; i < ptIndex_.length; i++) {
      n1.setCoordinate(n2);
      n2.setCoordinate(n3);
      if (i == ptIndex_.length - 1) {
        _support.getPt(ptIndex_[0], n3);
      } else {
        _support.getPt(ptIndex_[i + 1], n3);
      }
      q = CtuluLibGeometrie.getDistanceXY(n1, n2) + CtuluLibGeometrie.getDistanceXY(n3, n2);
      // x += q * n2.getX();
      y += q * n2.y;
      qt += q;
    }
    return y / qt;
  }

  /**
   * Renvoie true, si un des elements <code>_elems</code> contient le segment <code>[_s1,_s2]</code> sans tenir
   * compte du sens.
   *
   * @param _elems les elements a tester
   * @param _s1 un indice
   * @param _s2 l'autre indice
   * @return true si contenu
   */
  public static boolean containsSegmentSensIndif(final EfElement[] _elems, final int _s1, final int _s2) {
    final int n = _elems.length - 1;
    for (int i = n; i >= 0; i--) {
      if (_elems[i].containsSegmentSensIndifferent(_s1, _s2)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Renvoie true, si un des elements du maillage contient le segment <code>[_s1,_s2]</code> sans tenir compte du
   * sens.
   *
   * @param _mail le maillage a tester
   * @param _s1 un indice du segment a chercher
   * @param _s2 l'autre indice du segment a chercher
   * @return true si contenu
   */
  public static boolean containsSegmentSensIndif(final EfGridInterface _mail, final int _s1, final int _s2) {
    for (int i = _mail.getEltNb() - 1; i >= 0; i--) {
      if (_mail.getElement(i).containsSegmentSensIndifferent(_s1, _s2)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _elts les elements a tester
   * @return le type commun ou null si heterogene
   */
  public static EfElementType getCommunType(final EfElement[] _elts) {
    final int n = _elts.length - 2;
    if (n < 0) {
      return null;
    }
    EfElement element;
    final int r = _elts[n + 1].getPtNb();
    for (int i = n; i >= 0; i--) {
      element = _elts[i];
      if (element.getPtNb() != r) {
        return null;
      }
    }
    return getDefaultType(r);
  }

  public static EfElementType getCommunType(final EfGridInterface _elts) {
    final int n = _elts.getEltNb() - 2;
    EfElement element;
    final int r = _elts.getElement(n + 1).getPtNb();
    for (int i = n; i >= 0; i--) {
      element = _elts.getElement(i);
      if (element.getPtNb() != r) {
        return null;
      }
    }
    return getDefaultType(r);
  }

  public static int getMaxPointInElements(final EfGridInterface _grid) {
    final int n = _grid.getEltNb() - 2;
    EfElement element;
    int r = _grid.getElement(n + 1).getPtNb();
    for (int i = n; i >= 0; i--) {
      element = _grid.getElement(i);
      if (element.getPtNb() > r) {
        r = element.getPtNb();
      }
    }
    return r;
  }

  /**
   * @param _nbPoint le nombre de point
   * @return le type pour le nombre de point donne
   */
  public static EfElementType getDefaultType(final int _nbPoint) {
    return EfElementType.getCommunType(_nbPoint);
  }

  /**
   * Renvoie l'index du segment contenant l'arete [s1,s2] dans l'ordre donne.
   *
   * @param _elems les elements a tester
   * @param _s1 l'indice 1
   * @param _s2 l'indice 2
   * @return -1 si non trouve
   */
  public static int getIdxSegmentContainsStrict(final EfElement[] _elems, final int _s1, final int _s2) {
    for (int i = _elems.length - 1; i >= 0; i--) {
      if (_elems[i].containsSegment(_s1, _s2)) {
        return i;
      }
    }
    return -1;
  }

  public static int getIdxSegmentContainsStrict(final EfGridInterface _elems, final int _s1, final int _s2) {
    for (int i = _elems.getEltNb() - 1; i >= 0; i--) {
      if (_elems.getElement(i).containsSegment(_s1, _s2)) {
        return i;
      }
    }
    return -1;
  }

  public static int getIdxSegmentContains(final EfElement[] _elems, final int _s1, final int _s2) {
    for (int i = _elems.length - 1; i >= 0; i--) {
      if (_elems[i].containsSegmentSensIndifferent(_s1, _s2)) {
        return i;
      }
    }
    return -1;
  }

  public static int getIdxSegmentContains(final EfGridInterface _elems, final int _s1, final int _s2) {
    for (int i = _elems.getEltNb() - 1; i >= 0; i--) {
      if (_elems.getElement(i).containsSegmentSensIndifferent(_s1, _s2)) {
        return i;
      }
    }
    return -1;
  }

  /**
   * @param _elem les elements a tester
   * @param _s1 l'indice recherche
   * @return les indices des elements contenu l'indice <code>_s1</code>.
   */
  public static int[] getIndexElementContainsPtIdx(final EfGridInterface _elem, final int _s1) {
    final int n = _elem.getEltNb() - 1;
    final int[] r = new int[n + 1];
    int index = 0;
    for (int i = n; i >= 0; i--) {
      if (_elem.getElement(i).containsIndex(_s1)) {
        r[index++] = i;
      }
    }
    final int[] temp = new int[index];
    System.arraycopy(r, 0, temp, 0, index);
    Arrays.sort(temp);
    return temp;
  }

  /**
   * @param _el l'element a modifier
   * @return un nouvel element avec les indices decales vers la gauche
   */
  public static EfElement shiftLeftElementIndex(final EfElement _el) {
    final int[] elIdx = _el.ptIndex_;
    final int[] n = new int[elIdx.length];
    System.arraycopy(elIdx, 1, n, 0, elIdx.length - 1);
    n[elIdx.length - 1] = elIdx[0];
    return new EfElement(n);
  }

  int[] ptIndex_;

  /**
   * @param _elem l'element utilise pour l'initialisation
   */
  public EfElement(final EfElement _elem) {
    ptIndex_ = new int[_elem.ptIndex_.length];
    System.arraycopy(_elem.ptIndex_, 0, ptIndex_, 0, _elem.ptIndex_.length);
  }

  /**
   * @param _ptIndex les indices a utiliser
   */
  public EfElement(final int[] _ptIndex) {
    ptIndex_ = _ptIndex;
  }

  /**
   * @return les indices des points pour cet element ( NE PAS MODIFIER LE TABLEAU).
   */
  protected int[] getPtIndex() {
    return ptIndex_;
  }

  /**
   * Oriente cet element dans le sens trigo.
   */
  protected boolean orienteDansSensTrigo(final EfGridInterface _maill) {
    return orienteDansSensTrigo(_maill, true);
  }

  /**
   * @param _support le maillage support
   * @return le max des abscisses
   */
  public double getMaxX(final EfGridInterface _support) {
    double r = _support.getPtX(ptIndex_[0]);
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      final double tmp = _support.getPtX(ptIndex_[i]);
      if (tmp > r) {
        r = tmp;
      }
    }
    return r;
  }

  /**
   * @param _support le maillage support
   * @return le max des abscisses
   */
  public double getMinX(final EfGridInterface _support) {
    double r = _support.getPtX(ptIndex_[0]);
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      final double tmp = _support.getPtX(ptIndex_[i]);
      if (tmp < r) {
        r = tmp;
      }
    }
    return r;
  }

  public Envelope getEnv(final EfGridInterface _grid) {
    return expandEnvelope(_grid, new Envelope());
  }

  public Envelope expandEnvelope(final EfGridInterface _grid, final Envelope _dest) {
    for (int i = ptIndex_.length - 1; i >= 0; i--) {
      _dest.expandToInclude(_grid.getPtX(ptIndex_[i]), _grid.getPtY(ptIndex_[i]));
    }
    return _dest;
  }

  /**
   * @param _support le maillage support
   * @return le max des abscisses
   */
  public double getMaxY(final EfGridInterface _support) {
    double r = _support.getPtY(ptIndex_[0]);
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      final double tmp = _support.getPtY(ptIndex_[i]);
      if (tmp > r) {
        r = tmp;
      }
    }
    return r;
  }

  /**
   * @param _support le maillage support
   * @return le max des abscisses
   */
  public double getMinY(final EfGridInterface _support) {
    double r = _support.getPtY(ptIndex_[0]);
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      final double tmp = _support.getPtY(ptIndex_[i]);
      if (tmp < r) {
        r = tmp;
      }
    }
    return r;
  }

  /**
   * Oriente cet element dans le sens trigo si _trigo ou dans le sens horaire.
   */
  protected boolean orienteDansSensTrigo(final EfGridInterface _maill, boolean _trigo) {
    final int sens = isTrigoOriente(_maill);
    if (sens == 0) {
      return false;
    }
    if (((sens < 0) && (_trigo)) || ((sens > 0) && (!_trigo))) {
      CtuluLibArray.invert(ptIndex_, 1);
      return true;
    }
    return false;
  }

  /**
   * Ajoute dans le vecteur _l, les segments de cet element contenus une seule fois dans le maillage _m (les segments de
   * bords). Il est important de bien specifier l'index (_indexThis) de l'element (this) sinon il ne sera jamais pris
   * comme une arete de bord.
   *
   * @param _m le maillage considere
   * @param _l la liste dans laquelle seront stockes les indices des segments de bords.
   * @param _indexThis l'indice de cet element dans le maillage _m.
   */
  public void addSingleSegment(final EfGridInterface _m, final TIntArrayList _l, final int _indexThis) {
    final int n = ptIndex_.length - 1;
    final int nElem = _m.getEltNb() - 1;
    int s1, s2;
    boolean contenu;
    for (int i = n; i >= 0; i--) {
      contenu = false;
      // Dans un maillage tous les elements sont dans le meme sens. Donc il suffit de
      // tester les segment (i,i-1) uniquement
      s1 = (i == 0 ? ptIndex_[n] : ptIndex_[i - 1]);
      s2 = ptIndex_[i];
      for (int j = nElem; ((j >= 0) && (!contenu)); j--) {
        if (j != _indexThis) {
          if (_m.getElement(j).containsSegment(s2, s1)) {
            contenu = true;
          }
        }
      }
      if (!contenu) {
        _l.add(s1);
        _l.add(s2);
      }
    }
  }

  /**
   * @param _s1 l'indice a tester
   * @return true si l'indice est contenu dans cet element
   */
  public boolean containsIndex(final int _s1) {
    final int n = ptIndex_.length - 1;
    for (int i = n; i >= 0; i--) {
      if (ptIndex_[i] == _s1) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _ptIdx1 l'indice 1
   * @param _ptIdx2 l'indice 2
   * @return true si cet element contient l arete (_ptIdx1,_ptIdx2) dans le sens donne
   */
  public boolean containsSegment(final int _ptIdx1, final int _ptIdx2) {
    final int n = ptIndex_.length - 1;
    for (int i = n - 1; i >= 0; i--) {
      if ((ptIndex_[i] == _ptIdx1) && (ptIndex_[i + 1] == _ptIdx2)) {
        return true;
      }
    }
    if ((ptIndex_[n] == _ptIdx1) && (ptIndex_[0] == _ptIdx2)) {
      return true;
    }
    return false;
  }

  /**
   * Recherche si cet element contient l'arete dans n'importe quel sens.
   *
   * @param _ptIdx1 un indice
   * @param _ptIdx2 l'autre indice
   * @return true si arete contenue (sens indifferent)
   */
  public boolean containsSegmentSensIndifferent(final int _ptIdx1, final int _ptIdx2) {
    return containsSegment(_ptIdx1, _ptIdx2) || containsSegment(_ptIdx2, _ptIdx1);
    /*
     * final int n = ptIndex_.length - 1; int rs1, rs2; for (int i = n - 1; i >= 0; i--) { rs1 = ptIndex_[i]; rs2 =
     * ptIndex_[i + 1]; if (((rs1 == _ptIdx1) && (rs2 == _ptIdx2)) || ((rs1 == _ptIdx2) && (rs2 == _ptIdx1))) { return
     * true; } } rs1 = ptIndex_[n]; rs2 = ptIndex_[0]; if (((rs1 == _ptIdx1) && (rs2 == _ptIdx2)) || ((rs1 == _ptIdx2) &&
     * (rs2 == _ptIdx1))) { return true; } return false;
     */
  }

  /**
   * Renvoie un nouvel elt oriente dans le sens trigo si _trigo=true et dans le sens horaire sinon. Si l'element est
   * plat, aucun changement n'est effectu�.
   *
   * @param _maill le maillage contenant cet element
   * @param _trigo true si le sens trigo est demande
   * @return un nouvel element oriente dans le sens trigo
   */
  public EfElement createElementTrigoOriente(final EfGridInterface _maill, final boolean _trigo) {
    final EfElement r = new EfElement(this);
    r.orienteDansSensTrigo(_maill, _trigo);
    return r;
  }

  /**
   * @param _s le tableau a remplir avec les indices des points utilises
   */
  public void fillList(final TIntHashSet _s) {
    final int n = ptIndex_.length;
    for (int i = 0; i < n; i++) {
      _s.add(ptIndex_[i]);
    }
  }

  /**
   * @return une copie du tableau des indices
   */
  public int[] getIndices() {
    return CtuluLibArray.copy(ptIndex_);
  }

  /**
   * Renvoie l'angle geometrique au point <code>_i</code>.
   *
   * @param _g le maillage contenant
   * @param _i l'indice du point en lequel l'angle est demande
   * @return angle en radian compris dans [0,Pi]
   */
  public double getAngle(final EfGridInterface _g, final int _i) {
    final int p0 = (_i == 0 ? ptIndex_[ptIndex_.length - 1] : ptIndex_[_i - 1]);
    final int p1 = (ptIndex_[_i]);
    final int p2 = (_i == ptIndex_.length - 1 ? ptIndex_[0] : ptIndex_[_i + 1]);
    final double d1 = CtuluLibGeometrie.getDistance(_g.getPtX(p0), _g.getPtY(p0), _g.getPtX(p1), _g.getPtY(p1));
    final double d2 = CtuluLibGeometrie.getDistance(_g.getPtX(p2), _g.getPtY(p2), _g.getPtX(p1), _g.getPtY(p1));
    final double x1 = (_g.getPtX(p0) - _g.getPtX(p1)) / d1;
    final double y1 = (_g.getPtY(p0) - _g.getPtY(p1)) / d1;
    final double x2 = (_g.getPtX(p2) - _g.getPtX(p1)) / d2;
    final double y2 = (_g.getPtY(p2) - _g.getPtY(p1)) / d2;
    double r = Math.asin(y2 * x1 - x2 * y1);
    if (r < 0) {
      r = -r;
    }
    if (x1 * x2 + y1 * y2 < 0) {
      r = Math.PI - r;
    }
    return r;
  }

  /**
   * @return pour les elements de type T6, on renvoie 3 aretes.
   */
  public int getNbEdge() {
    if (getDefaultType() == EfElementType.T6) {
      return 3;
    }
    return getPtNb();
  }

  public int getEdgePt1(int _areteIdx) {
    if (getDefaultType() == EfElementType.T6) {
      return getPtIndex(_areteIdx * 2);
    }
    return getPtIndex(_areteIdx);
  }

  /**
   * Attention aucun test n'est fait sur l'indice pass� en argument.
   *
   * @param _areteIdx l'indice de l'arete
   * @return l'indice du point milieu ou -1 si ce n'est pas un �l�ment T6
   */
  public int getEdgePtMiddle(int _areteIdx) {
    if (getDefaultType() == EfElementType.T6) {
      return getPtIndex(_areteIdx * 2 + 1);
    }
    return -1;
  }

  public int getEdgePt2(int _areteIdx) {
    if (getDefaultType() == EfElementType.T6) {
      return getPtIndex((_areteIdx * 2 + 2) % getPtNb());
    }
    return getPtIndex((_areteIdx + 1) % getPtNb());
  }

  /**
   * @return le type par defaut utilise pour cet element
   */
  public EfElementType getDefaultType() {
    return getDefaultType(ptIndex_.length);
  }

  /**
   * @param _g le maillage contenant
   * @return l'angle maxi de cet element
   */
  public double getMinAngle(final EfGridInterface _g) {
    double r = getAngle(_g, 0);
    double d;
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      d = getAngle(_g, i);
      if (d < r) {
        r = d;
      }
    }
    return r;
  }

  /**
   * @param _g le maillage contenant
   * @return l'angle maxi de cet element
   */
  public double getMaxAngle(final EfGridInterface _g) {
    double r = getAngle(_g, 0);
    double d;
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      d = getAngle(_g, i);
      if (d > r) {
        r = d;
      }
    }
    return r;
  }

  /**
   * Dangereux: il faut prendre en compte le fait que pour interpoler un vecteur, il faut interpoler la norme et l'angle
   * et non pas vx et vy.
   *
   * @param _data les donn�es sur les noeuds
   * @return la moyenne pour cette elements
   */
  public double getAverageDanger(final CtuluCollectionDouble _data) {
    final int nb = getPtNb();
    double sum = 0;
    for (int i = 0; i < nb; i++) {
      sum += _data.getValue(getPtIndex(i));
    }
    return sum / nb;
  }

  public double getAverage(final CtuluVariable _var, final int _tIdx, final CtuluCollectionDouble _data,
                           EfGridData _datas, InterpolationVectorContainer _vects) {
    return getAverage(_var, _data, new EfGridDataInterpolationValuesAdapter(_datas, _tIdx), _vects);
  }

  public double getAverage(final CtuluVariable _var, InterpolationSupportValuesMultiI _datas,
                           InterpolationVectorContainer _vects) {
    return getAverage(_var, _datas.getValues(_var), _datas, _vects);
  }

  public double getAverage(final CtuluVariable _var, final CtuluCollectionDouble _data,
                           InterpolationSupportValuesMultiI _datas, InterpolationVectorContainer _vects) {
    if (_vects.isVect(_var)) {
      CtuluCollectionDouble vx = _vects.getVxValue(_var, _datas);
      CtuluCollectionDouble vy = _vects.getVyValue(_var, _datas);
      if (vx == null || vy == null) {
        FuLog.error("vx or vy is null for " + _var);
        return 0D;
      }
      CtuluCollectionDouble norme = new InterpolationCollectionAdapter.Norm(vx, vy);
      CtuluCollectionDouble theta = new InterpolationCollectionAdapter.Theta(vx, vy);
      double n = getAverageDanger(norme);
      double t = getAverageDanger(theta);
      return _vects.isVx(_var) ? InterpolationVectorContainer.getVx(n, t) : InterpolationVectorContainer.getVy(n, t);
    }
    return getAverageDanger(_data);
  }

  /**
   * @param _g le maillage contenant
   * @return l'angle mini en degre
   */
  public double getMinAngleDegre(final EfGridInterface _g) {
    return getMinAngle(_g) * 180 / Math.PI;
  }

  /**
   * @param _g le maillage contenant
   * @return l'angle mini en degre
   */
  public double getMaxAngleDegre(final EfGridInterface _g) {
    return getMaxAngle(_g) * 180 / Math.PI;
  }

  /**
   * @param _i l'indice local du point pour lequel on veut l'indice global.
   * @return l'indice global du point.
   */
  public int getPtIndex(final int _i) {
    return ptIndex_[_i];
  }

  /**
   * @return le nombre de points de cet element
   */
  public final int getPtNb() {
    return ptIndex_.length;
  }

  /**
   * @param _g le maillage contenat
   * @param _minAngleInRadian l'angle en radian mini a ne pas depasser
   * @return true si contient un angle inferieur a <code>minAngleInRadian</code>
   */
  public boolean hasAngleLessThan(final EfGridInterface _g, final double _minAngleInRadian) {
    double d;
    for (int i = ptIndex_.length - 1; i >= 0; i--) {
      d = getAngle(_g, i);
      if (d < _minAngleInRadian) {
        return true;
      }
    }
    return false;
  }

  /**
   * OPtimisation: test un angle sur 2.
   *
   * @param _g le maillage contenat
   * @param _minAngleInRadian l'angle en radian mini a ne pas depasser
   * @return true si contient un angle inferieur a <code>minAngleInRadian</code>
   */
  public boolean hasAngleLessThanT6(final EfGridInterface _g, final double _minAngleInRadian) {
    double d;
    for (int i = ptIndex_.length - 2; i >= 0; i -= 2) {
      d = getAngle(_g, i);
      if (d < 0) {
        d = -d;
      }
      if (d < _minAngleInRadian) {
        return true;
      }
    }
    return false;
  }

  /**
   * Test si <code>_ele</code> est equivalent a cet element. Un element equivalent = un element qui poss�de les m�mes
   * indices dans le meme ordre (d�cal�s ou non). Exemple: 1,2,3 est �quivalent � 2,3,1 mais pas � 1,3,2
   *
   * @param _ele l'element a tester
   * @return true si equivalent
   */
  public boolean isEquivalent(final EfElement _ele) {
    if (ptIndex_.length != _ele.ptIndex_.length) {
      return false;
    }
    if (Arrays.equals(ptIndex_, _ele.ptIndex_)) {
      return true;
    }
    final int idx = CtuluLibArray.findInt(_ele.ptIndex_, ptIndex_[0]);
    if (idx > 0) {
      for (int i = 0; i < idx; i++) {
        if (ptIndex_[ptIndex_.length - idx + i] != _ele.ptIndex_[i]) {
          return false;
        }
      }
      for (int i = idx + 1; i < ptIndex_.length; i++) {
        if (ptIndex_[i - idx] != _ele.ptIndex_[i]) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  /**
   * @param _g le maillage contenant
   * @param _trigo true si le sens trigo est voulu
   * @return true si oriente dans le sens demande
   */
  public boolean isOrientedIn(final EfGridInterface _g, final boolean _trigo) {
    final int i = isTrigoOriente(_g);
    return _trigo ? i > 0 : i < 0;
  }

  /**
   * @param _fr la frontiere du maillage contenant
   * @return true si l'element est surcontraint : tous ces points sur la frontiere
   */
  public boolean isOverstressed(final EfFrontierInterface _fr) {
    for (int i = ptIndex_.length - 1; i >= 0; i--) {
      if (!_fr.isFrontierPoint(ptIndex_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _idx compris dans [0,getPtNb()[
   * @param _x
   * @param _y
   * @param _support
   */
  public double getAreteDistFrom(final int _idx, final double _x, final double _y, final EfGridInterface _support) {
    final int idx = ptIndex_[_idx];
    final int next = ptIndex_[(_idx + 1) % ptIndex_.length];
    return CtuluLibGeometrie.distanceFromSegment(_support.getPtX(idx), _support.getPtY(idx), _support.getPtX(next),
        _support.getPtY(next), _x, _y);
  }

//  public double getAreteDistAuCarreFrom(final int _idx, final double _x, final double _y, final EfGridInterface _support) {
//    final int idx = ptIndex_[_idx];
//    final int next = ptIndex_[(_idx + 1) % ptIndex_.length];
//    return CtuluLibGeometrie.distanceAuCarreFromSegment(_support.getPtX(idx), _support.getPtY(idx), _support
//        .getPtX(next), _support.getPtY(next), _x, _y);
//  }

  public double getDist(final double _x, final double _y, final EfGridInterface _support) {
    double d2 = getAreteDistFrom(0, _x, _y, _support);
    for (int i = ptIndex_.length - 1; i > 0; i--) {
      final double dTemp = getAreteDistFrom(i, _x, _y, _support);
      if (dTemp < d2) {
        d2 = dTemp;
      }
    }
    return d2;
  }

//  public double getDistAuCarre(final double _x, final double _y, final EfGridInterface _support) {
//    double d2 = getAreteDistAuCarreFrom(0, _x, _y, _support);
//    for (int i = ptIndex_.length - 1; i > 0; i--) {
//      final double dTemp = getAreteDistAuCarreFrom(i, _x, _y, _support);
//      if (dTemp < d2) {
//        d2 = dTemp;
//      }
//    }
//    return d2;
//  }

  /**
   * Optimisation maillage T6.
   *
   * @param _fr la frontiere du maillage contenant
   * @return true si l'element est surcontraint : tous ces points sur la frontiere
   */
  public boolean isOverstressedT6(final EfFrontierInterface _fr) {
    for (int i = ptIndex_.length - 2; i >= 0; i -= 2) {
      if (!_fr.isFrontierPoint(ptIndex_[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Test si un element equivalent est present de le tableau.
   *
   * @param _f le tableau a tester
   * @return true si element equivalent present dans le tableau
   * @see #isEquivalent(EfElement)
   */
  public boolean isPresentInArray(final EfElement[] _f) {
    return isPresentInArray(_f, -1);
  }

  public boolean isPresentInArray(final EfGridInterface _f) {
    return isPresentInArray(_f, -1);
  }

  /**
   * Test si un element equivalent est present de le tableau en commencant par un test sur l'index donne en parametre (
   * une petite optimisation).
   *
   * @param _f les elements a parcourir
   * @param _firstIndexToTest le premier indice a tester (si positif ou nul)
   * @return true si element equivalent dans le tableau
   * @see #isEquivalent(EfElement)
   */
  public boolean isPresentInArray(final EfElement[] _f, final int _firstIndexToTest) {
    if ((_firstIndexToTest >= 0) && (isEquivalent(_f[_firstIndexToTest]))) {
      return true;
    }

    for (int i = _f.length - 1; i >= 0; i--) {
      if (isEquivalent(_f[i])) {
        return true;
      }
    }
    return false;
  }

  public boolean isPresentInArray(final EfGridInterface _f, final int _firstIndexToTest) {
    if ((_firstIndexToTest >= 0) && (isEquivalent(_f.getElement(_firstIndexToTest)))) {
      return true;
    }

    for (int i = _f.getEltNb() - 1; i >= 0; i--) {
      if (isEquivalent(_f.getElement(i))) {
        return true;
      }
    }
    return false;
  }

  /**
   * Comparaison stricte.
   *
   * @param _ele l'element a tester
   * @return true si les 2 elements sont rigoureusement identique.
   */
  public boolean isSameStrict(final EfElement _ele) {
    if (ptIndex_.length != _ele.ptIndex_.length) {
      return false;
    }
    return (Arrays.equals(ptIndex_, _ele.ptIndex_));
  }

  /**
   * Test si le segment appartient a cet element et s'il est dans le meme sens que l'element. Exemple elt {0,1,2}
   * <br/>isSegmentInEltOrient(0,1)=1 <br/>isSegmentInEltOrient(1,0)=-1 <br/> isSegmentInEltOrient(1,3)=0 <br/>
   *
   * @param _ptIdx1 l'indice 1
   * @param _ptIdx2 l'indice 2
   * @return 1 si segment trouve et dans le bon sens 0 si segment non trouve -1 si segment trouve et dans le mauvais
   *     sens
   */
  public int isSegmentInEltOrient(final int _ptIdx1, final int _ptIdx2) {
    final int n = ptIndex_.length - 1;
    int rs1, rs2;
    for (int i = n - 1; i >= 0; i--) {
      rs2 = ptIndex_[i + 1];
      rs1 = ptIndex_[i];
      if ((rs1 == _ptIdx1) && (rs2 == _ptIdx2)) {
        return 1;
      } else if ((rs1 == _ptIdx2) && (rs2 == _ptIdx1)) {
        return -1;
      }
    }
    rs1 = ptIndex_[n];
    rs2 = ptIndex_[0];
    if ((rs1 == _ptIdx1) && (rs2 == _ptIdx2)) {
      return 1;
    } else if ((rs1 == _ptIdx2) && (rs2 == _ptIdx1)) {
      return -1;
    }
    return 0;
  }

  /**
   * Si cet element est oriente dans le sens trigo renvoie 1, renvoie 0 si element plat et -1 si oriente dans le sens
   * horaire. Calcul le produit vectoriel de l'element (soit l'aire). Si le produit est strictement positif l'element
   * est oriente dans le sens trigo
   *
   * @param _maill le maillage contenant cet element
   * @return 1 si sens trigo,-1 sens inverse et 0 si plat
   */
  public int isTrigoOriente(final EfGridInterface _maill) {
    if (ptIndex_.length < 3) {
      FuLog.error("not enough point for this element (nb point=" + ptIndex_.length + ")");
      return 0;
    }
    final Coordinate p0 = new Coordinate();
    final Coordinate p1 = new Coordinate();
    final Coordinate p2 = new Coordinate();
    _maill.getPt(ptIndex_[ptIndex_.length - 1], p0);
    _maill.getPt(ptIndex_[0], p1);
    _maill.getPt(ptIndex_[1], p2);
    double produitVec = ((p1.x - p0.x) * (p2.y - p1.y)) - ((p2.x - p1.x) * (p1.y - p0.y));
    for (int i = ptIndex_.length - 3; i >= 0; i--) {
      _maill.getPt(ptIndex_[i], p0);
      _maill.getPt(ptIndex_[i + 1], p1);
      _maill.getPt(ptIndex_[i + 2], p2);
      produitVec = produitVec + ((p1.x - p0.x) * (p2.y - p1.y)) - ((p2.x - p1.x) * (p1.y - p0.y));
    }
    if (produitVec != 0) {
      return produitVec > 0 ? 1 : -1;
    }
    return 0;
  }

  @Override
  public String toString() {
    return "H2dElement" + CtuluLibString.getObjectInString(ptIndex_, true);
  }
}
