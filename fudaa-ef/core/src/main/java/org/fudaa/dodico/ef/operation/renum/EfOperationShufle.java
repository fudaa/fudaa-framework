package org.fudaa.dodico.ef.operation.renum;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.decorator.EfSwapListManager;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;

public class EfOperationShufle extends AbstractEfOperation {

    /**
     *Cette op�ration ne change pas la num�rotation des noeuds de bords.
     */
    @Override
    protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {

        // TODO il faut mettre dans le log, les r�sultats des op�rations: a faire par la suite..en attente
        final EfGridInterface grid = super.initGrid.getGrid();
        EfSwapListManager<EfElement> swapedMeshes = new EfSwapListManager<EfElement>(grid.getElts());
        //fortran pour i allant de de i=1 � (nbElement-4)/2 par incr�ment de 2
        int nbElement = grid.getEltNb();

		ProgressionUpdater updater = new ProgressionUpdater(prog);
		updater.majProgessionStateOnly("ef.shufle.swap.msg");
		updater.setValue(10, (nbElement - 4) / 2);
        
        //attention: ici on reprend la numerotation fortran qui commence a 1:
        for (int i = 1; i <= (nbElement - 4) / 2; i += 2) {
			if (this.stop)
			{
				return null;
			}

			//les indices en fortran a swaper
            int idx1ToSwap = i;
            int idx2ToSwap = nbElement - i + 1;
            //on met -1 pour se retrouver dans la num�rotation java commencant � 0
            swapedMeshes.swap(idx1ToSwap - 1, idx2ToSwap - 1);

            updater.majAvancement();
        }
        EfElement[] elts = new EfElement[swapedMeshes.getList().size()];
        swapedMeshes.getList().toArray(elts);

        updater.majProgessionStateOnly("ef.shufle.renum.msg");
		updater.setValue(10, (nbElement - 4) / 2);
		
        //dans cette liste, on va chercher a modifier les numerotation locales pour partir du point xmax.
        for (int i = 0; i < nbElement; i++) {
			if (this.stop)
			{
				return null;
			}

			int idxLocalMax = getLocalIdxWithXmax(elts[i], grid);
            //on doit renumeroter si le x n'est pas maximal
            if (idxLocalMax != 0) {
                int ptNbDansElt = elts[i].getPtNb();
                //newIdxs contient le nouveau tableau d'indices globaux de l'�l�ment
                int[] newIdxs = new int[ptNbDansElt];
                //on renumerote en partant de idxLocalMax
                for (int j = 0; j < ptNbDansElt; j++) {
                    newIdxs[j] = elts[i].getPtIndex((j + idxLocalMax) % ptNbDansElt);
                }
                elts[i] = new EfElement(newIdxs);
            }

            updater.majAvancement();
        }
        //TODO pour Fred EfGrid a modifier...
        EfGridInterface newGrid = new EfGrid(grid.getNodes(), elts);

        EfGridDataRenumeroteDecorator renumeroteDecorator = new EfGridDataRenumeroteDecorator(super.initGrid, newGrid);
        renumeroteDecorator.setEltNewIdxOldIdxMap(swapedMeshes.getNewIdxOldIdx());

        log.addInfo("ef.operation.log.newPt.nb", newGrid.getPtsNb());
        log.addInfo("ef.operation.log.newEle.nb", newGrid.getEltNb());

        return renumeroteDecorator;
    }

    public static int getLocalIdxWithXmax(EfElement elt, EfGridInterface gridSupport) {
        int idx = 0;
        double max = gridSupport.getPtX(elt.getPtIndex(0));
        for (int i = 1; i < elt.getPtNb(); i++) {
            double x = gridSupport.getPtX(elt.getPtIndex(i));
            if (x > max) {
                max = x;
                idx = i;
            }
        }
        return idx;

    }
}
