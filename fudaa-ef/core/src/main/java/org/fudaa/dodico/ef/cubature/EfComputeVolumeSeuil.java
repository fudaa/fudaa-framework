package org.fudaa.dodico.ef.cubature;

import com.memoire.fu.FuLog;
import java.io.IOException;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfFilter;
import org.fudaa.dodico.ef.EfFilterNone;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Calcul du volume par rapport a un seuil et non plus 0.
 *
 * @author Adrien Hadoux
 */
public final class EfComputeVolumeSeuil implements CtuluActivity {

  public static abstract class ComputeDelegate {

    protected EfComputeVolumeSeuil volumeData;

    public ComputeDelegate(EfComputeVolumeSeuil data) {
      this.volumeData = data;
    }

    protected abstract EfDataIntegrale computeVolume(final EfData _h);
  }

  public static class ForMeshData extends ComputeDelegate {

    public ForMeshData(EfComputeVolumeSeuil data) {
      super(data);
    }

    @Override
    protected EfDataIntegrale computeVolume(final EfData _h) {
      EfDataIntegrale resi = new EfDataIntegrale();
      for (int ielt = 0; ielt < volumeData.data_.getGrid().getEltNb(); ielt++) {
        if (volumeData.filter_.isActivatedElt(ielt)) {
          double value = volumeData.data_.getGrid().getAire(ielt) * (_h.getValue(ielt) - volumeData.seuil_);// Todo je pense que c'est la diff
          resi.add(value);
        }
      }
      return resi;
    }
  }
  public static final boolean USE_STRICT_FILTER = true;

  public static class ForNodeData extends ComputeDelegate {

    public ForNodeData(EfComputeVolumeSeuil data) {
      super(data);
    }

    @Override
    protected EfDataIntegrale computeVolume(final EfData _h) {
      EfDataIntegrale resi = new EfDataIntegrale();
      for (int ielt = 0; ielt < volumeData.data_.getGrid().getEltNb(); ielt++) {
        // le calcul du volume se fait:
        // Volume=(surface des elements adjacents)* hauteur au point /3
        if (volumeData.filter_.isActivatedElt(ielt, USE_STRICT_FILTER)) {
          // liste des elements adjacents.
          // s est la surface des elements adjacents.
          EfElement elt = volumeData.data_.getGrid().getElement(ielt);
          int nbElt = elt.getPtNb();
          double s = 0D;
          for (int k = 0; k < nbElt; k++) {
            s = s + (_h.getValue(elt.getPtIndex(k)) - volumeData.seuil_);

          }
          resi.add(s * volumeData.data_.getGrid().getAire(ielt) / ((double) nbElt));
        }
      }
      return resi;
    }
  }
  final EfGridData data_;
  final EfFilter filter_;
  final CtuluVariable h_;
  // protected final int timeStep_;
  protected final double seuil_;
  ProgressionInterface prog_;
  boolean stop_;

  public static EfComputeVolumeSeuil getVolumeComputer(final EfFilter _filter, final ProgressionInterface _prog,
          final EfGridData _data, final CtuluVariable _hVar, final double seuil, final CtuluAnalyze _analyze) {
    if (!_data.isDefined(_hVar)) {
      _analyze.addFatalError(DodicoLib.getS("La variable n'est pas d�finie"));
      return null;
    }
    return new EfComputeVolumeSeuil(_filter, _prog, _data, _hVar, seuil);
  }

  EfComputeVolumeSeuil(final EfFilter _filter, final ProgressionInterface _prog, final EfGridData _data,
          final CtuluVariable _hVar, final double seuil) {
    super();
    prog_ = _prog;
    data_ = _data;
    h_ = _hVar;
    // timeStep_ = timestep;
    seuil_ = seuil;
    filter_ = _filter == null ? new EfFilterNone() : _filter;
  }

  protected EfDataIntegrale computeVolume(EfData _h) {
    if (_h.isElementData()) {
      return new ForMeshData(this).computeVolume(_h);
    }
    return new ForNodeData(this).computeVolume(_h);
  }

  public EfDataIntegrale getVolume(int choosenTimestep) {
    final ProgressionUpdater up = new ProgressionUpdater(prog_);
    up.setValue(10, 100);
    EfDataIntegrale res;
    up.majProgessionStateOnly(DodicoLib.getS("Calcul du volume"));

    if (stop_) {
      return null;
    }
    EfData h = null;
    try {
      h = data_.getData(h_, choosenTimestep);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    if (h == null) {
      return null;
    }
    res = computeVolume(h);
    up.majAvancement();

    return res;
  }

  public ProgressionInterface getProg() {
    return prog_;
  }

  public void setProg(final ProgressionInterface _prog) {
    prog_ = _prog;
  }

  @Override
  public void stop() {
    stop_ = true;
  }
}
