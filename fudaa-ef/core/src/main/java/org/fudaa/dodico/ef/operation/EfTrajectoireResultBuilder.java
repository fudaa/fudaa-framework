/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CDoubleArrayList;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

/**
 * Classe permet de populer les resultats de maniere incrementale. Cette classe est chargee d'ajouter les marqueurs si
 * nécessaire.
 * 
 * @author deniger
 */
public class EfTrajectoireResultBuilder {

  private final List<Coordinate> coords_ = new ArrayList<Coordinate>(50);

  final EfTrajectoireGridDataProvider gridDataProvider_;

  final TIntHashSet lastAdjacentMeshes_ = new TIntHashSet();

  // private EfLineIntersection lastIntersect_;
  private final EfTrajectoireMarqueurBuilder marqueurBuilder_;

  private final List<Boolean> marqueurs = new ArrayList<Boolean>(50);

  private final EfTrajectoireParameters parameters_;

  private CDoubleArrayList times_ = new CDoubleArrayList(50);
  private Map<CtuluVariable, CDoubleArrayList> varValues_;

  public EfTrajectoireResultBuilder(EfTrajectoireParameters _parameters, EfTrajectoireGridDataProvider _provider) {
    gridDataProvider_ = _provider;
    marqueurBuilder_ = EfTrajectoireMarqueurBuilder.createMarqueurBuilder(this, _parameters);
    varValues_ = new HashMap<CtuluVariable, CDoubleArrayList>(2 + _parameters.varsASuivre_.size());
    parameters_ = _parameters;
    varValues_.put(_parameters.vx, new CDoubleArrayList(100));
    varValues_.put(_parameters.vy, new CDoubleArrayList(100));
    for (CtuluVariable v : _parameters.varsASuivre_) {
      varValues_.put(v, new CDoubleArrayList(100));

    }
  }

  protected void addFirstIntersection(EfLineIntersection _intersect, double _t, EfGridDataInterpolator _data) {
    saveIntersection(_intersect, _t, _data);

  }

  protected void addIntersection(EfLineIntersection _intersect, double _t) {
    if (!coords_.isEmpty() && marqueurBuilder_ != null) {
      marqueurBuilder_.preCoordinateAdded(_intersect, _t);
    }
    saveIntersection(_intersect, _t, getDataFor(_t));

  }
  
  private EfLineIntersection lastIntersection;

  private void saveIntersection(EfLineIntersection _intersect, double _t, EfGridDataInterpolator _data) {
    setLastIntersection(_intersect);
    setParent(_intersect, _data);
    lastAdjacentMeshes_.clear();
    _intersect.fillWithAdjacentMeshes(lastAdjacentMeshes_);
    // EfGridDataInterpolator dataForT = getDataFor(_t);
    fillMaps(_intersect, _t, false, _data);
  }

  protected void addMarqueurIntersection(EfLineIntersection _intersect, double _t) {
    EfGridDataInterpolator dataFor = getDataFor(_t);
    setParent(_intersect, dataFor);
    fillMaps(_intersect, _t, true, dataFor);
  }

  private void fillMaps(EfLineIntersection _intersect, double _t, boolean _marqueur, EfGridDataInterpolator dataForT) {
    marqueurs.add(Boolean.valueOf(_marqueur || (coords_.isEmpty() && marqueurBuilder_ != null)));
    coords_.add(new Coordinate(_intersect.getX(), _intersect.getY()));
    times_.add(_t);
    // on ajoute un marqueur si demande ou si c'est le premier et si les marqueurs sont actives.
    for (Map.Entry<CtuluVariable, CDoubleArrayList> entry : varValues_.entrySet()) {
      CDoubleArrayList value = entry.getValue();
      double pointValue=0;
      if(_intersect.isRealIntersection()){
        pointValue=_intersect.getValue(entry.getKey(), 0);
      }
      value.add(pointValue);// on prend le pas de temps 0 car les donnees sont
      // construites ainsi
    }
  }

  private void setParent(EfLineIntersection _intersect, EfGridDataInterpolator dataForT) {
    _intersect.setParent(new EfLineIntersectionParentDefault(dataForT));
  }

  /**
   * @return the coords
   */
  public List<Coordinate> getCoords() {
    return coords_;
  }

  private EfGridDataInterpolator getDataFor(double _t) {
    return new EfGridDataInterpolator(gridDataProvider_.getDataFor(_t));
  }

  public Coordinate getLastCoordinate() {
    return coords_.isEmpty() ? null : coords_.get(coords_.size() - 1);
  }

  public double getLastTime() {
    return times_.get(times_.size() - 1);
  }

  public double getLastVx() {
    TDoubleArrayList list = varValues_.get(parameters_.vx);
    return list.getQuick(list.size() - 1);
  }

  public double getLastVy() {
    TDoubleArrayList list = varValues_.get(parameters_.vy);
    return list.getQuick(list.size() - 1);
  }

  /**
   * @return the marqueurs
   */
  public List<Boolean> getMarqueurs() {
    return marqueurs;
  }

  /**
   * @return the parameters
   */
  public EfTrajectoireParameters getParameters() {
    return parameters_;
  }

  /**
   * @return the times
   */
  public TDoubleArrayList getTimes() {
    return times_;
  }

  /**
   * @return the varValues: contient les donnees pour vx et vy
   */
  public Map<CtuluVariable, CDoubleArrayList> getVarValues() {
    return varValues_;
  }

  public void setLastIntersection(EfLineIntersection lastIntersection) {
    this.lastIntersection = lastIntersection;
  }

  public EfLineIntersection getLastIntersection() {
    return lastIntersection;
  }


}
