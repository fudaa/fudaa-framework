package org.fudaa.dodico.ef.operation.renum;

import gnu.trove.TIntHashSet;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.Messages;
import org.fudaa.dodico.ef.decorator.EfGridDataRenumeroteDecorator;
import org.fudaa.dodico.ef.decorator.EfSwapListManager;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;

public class EfOperationBackElimination extends AbstractEfOperation {

  private int vectorLength;

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    EfGridInterface grid = this.initGrid.getGrid();
    EfSwapListManager<EfElement> swaper = new EfSwapListManager<EfElement>(grid.getElts()); // on initialise avec le
                                                                                            // tableau des éléments du
                                                                                            // grid initiale
    List<EfElement> listElt = swaper.getList();
    int nbElement = listElt.size();

    // TODO il faut mettre dans le log, les résultats des opérations: a faire par la suite..en attente

    ProgressionUpdater updater = new ProgressionUpdater(prog);
    updater.majProgessionStateOnly(Messages.getString("ef.backElimination.msg"));
    updater.setValue(10, nbElement);

    int counter = 0;
    
    for (int i = 0; i < nbElement; i++) {
      if (this.stop) { return null; }

      int ielt2 = i;
      boolean goOn = true;

      while (goOn && this.isBackDependant(i, listElt)) {
        if (this.stop) { return null; }

        ielt2 = ((ielt2 + 1) % nbElement);

        if (ielt2 == i) {
          goOn = false;
        } else {
          swaper.swap(ielt2, i);
          counter++;
        }
      }

      updater.majAvancement();
    }

    EfElement[] elts = new EfElement[swaper.getList().size()];
    swaper.getList().toArray(elts);

    // TODO Voir si modifier comme dans EfOperationShufle.
    EfGridInterface newGrid = new EfGrid(grid.getNodes(), elts);

    EfGridDataRenumeroteDecorator renumeroteDecorator = new EfGridDataRenumeroteDecorator(super.initGrid, newGrid);
    renumeroteDecorator.setEltNewIdxOldIdxMap(swaper.getNewIdxOldIdx());

    log.addInfo("ef.backElimination.iteration", counter);
    log.addInfo("ef.operation.log.newPt.nb", newGrid.getPtsNb());
    log.addInfo("ef.operation.log.newEle.nb", newGrid.getEltNb());

    return renumeroteDecorator;
  }

  public boolean isDependent(EfElement elt1, EfElement elt2) {
    int[] element1Pts = elt1.getIndices();
    TIntHashSet element2Pts = new TIntHashSet(elt2.getIndices());

    for (int i = 0; i < element1Pts.length; i++) {
      if (element2Pts.contains(element1Pts[i])) { return true; }
    }

    return false;
  }

  public boolean isBackDependant(int idxElt, List<EfElement> elts) {
    EfElement toTest = elts.get(idxElt);
    EfGridInterface grid = this.initGrid.getGrid();
    int nbElement = grid.getEltNb();

    for (int i = 2; i <= this.vectorLength; i++) {
      int iEltToTest = (nbElement + idxElt + 1 - i) % nbElement;

      if (this.isDependent(toTest, elts.get(iEltToTest))) { return true; }
    }

    return false;
  }

  public void setVectorLength(int vectorLength) {
    this.vectorLength = vectorLength;
  }

}
