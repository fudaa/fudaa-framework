/*
 *  @creation     22 avr. 2005
 *  @modification $Date: 2007-05-22 13:11:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.TIntIntHashMap;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISCollectionPointInteface;
import org.fudaa.ctulu.gis.GISPolygone;

/**
 * @author Fred Deniger
 * @version $Id: EfGridInterface.java,v 1.17 2007-05-22 13:11:23 deniger Exp $
 */
public interface EfGridInterface {

  void computeBord(ProgressionInterface _inter, CtuluAnalyze _analyze);

  void computeNeighbord(ProgressionInterface _inter, CtuluAnalyze _analyze);

  EfNeighborMesh getNeighbors();

  void computeBordFast(int[] _ipobo, ProgressionInterface _pr, CtuluAnalyze _an);

  /**
   * @return true si la frontiere externe contient le point (x,y).
   */
  boolean contientPoint(double _x, double _y);

  GISPolygone getLinearRing(int[] _idx);

  GISPolygone[] getExtRings();

  void createIndexRegular(ProgressionInterface _prog);

  EfIndexInterface getIndex();

  public Envelope getEnvelopeForElement(int idxElt) ;

  /**
   * @return pour les maillages T6, les points entourant les point milieux. Il n'y a pas d'ordre dans les deux tables.
   */
  TIntIntHashMap[] getMiddleBounds();

  /**
   * Test sur x,y uniquement.
   * 
   * @param _p le point a tester
   * @return si le point x,y est contenu dans ce maillage
   */
  boolean contientPoint(EfNode _p);

  /**
   * Remplit la liste donnee en parametre avec les coordonnees de points.
   */
  boolean fillListWithPoint(GISCollectionPointInteface _listToFill);

  /**
   * @param _eltId l'element a considerer
   * @return l'aire de l'element d'indice _eltId
   */
  double getAire(int _eltId);

  void getCentreElement(int _idxElt, Coordinate _m);

  void getMoyCentreElement(int _idxElt, Coordinate _m);

  /**
   * @param _idxElt l'indice de l'element
   * @return le x du centre
   */
  double getCentreXElement(int _idxElt);

  double getMoyCentreXElement(int _idxElt);

  double getMoyCentreYElement(int _idxElt);

  double getMoyCentreZElement(int _idxElt);

  /**
   * @param _idxElt l'indice de l'element
   * @return le x du centre
   */
  double getCentreYElement(int _idxElt);

  /**
   * @param _idxElt l'indice de l'element
   * @return le centre z : moyenne des z
   */
  double getCentreZElement(int _idxElt);

  Coordinate getCoor(int _i);

  void getCoord(int _i, Coordinate _c);

  double getDistanceFromFrontier(double _x, double _y);

  /**
   * @param _i l'indice de l'element demande
   * @return l'element d'indice <code>_i</code>
   */
  EfElement getElement(int _i);

  /**
   * Recherche parmis les elements ayant leur indice compris dans le tableau _segmentIdxList celui contenant le point
   * d'indice _ptIdx. La recherche est effectuee depuis la fin du tableau.
   * 
   * @return -1 si aucun element contient le point d'indice _ptIdx.
   * @param _ptIdx l'indice recherche
   * @param _eltIdxList les indices des elements a parcourir
   */
  int getElementIdxContainingPtIdx(int _ptIdx, int[] _eltIdxList);

  /**
   * @param _i l'indice de l'element
   * @return l'element sous forme de polyligne fermee
   */
  LinearRing getElementRing(int _i);

  /**
   * Permet de retrouver l'element contenant le point (_x,_y) et dont le milieu est le plus pres.
   * 
   * @param _x le x a tester
   * @param _y le y a tester
   * @param _m un point mutable utilise pour calcul le centre de l'element: optimisation.
   * @return l'element contenant le point _x, _y. -1 si aucun
   */
  int getEltContainingXY(double _x, double _y, Coordinate _m);

  /**
   * @param _s l'indice recherche
   * @return Renvoie tous les elements contenant l'indice _s.
   */
  int[] getEltIdxContainsPtIdx(int _s);

  /**
   * @param _minAngleInDegre l'angle min en degre
   * @param _progress la barre de progression
   * @return les indices des elements ayant un angle inf a <code>_minAngleInDegre</code>
   */
  int[] getEltIdxWithAngleLessThan(double _minAngleInDegre, ProgressionInterface _progress);

  /**
   * @return le nombre d'element contenus dans ce maillage
   */
  int getEltNb();

  EfElement[] getElts();

  /**
   * @return le type commun. null si heterogene
   */
  EfElementType getEltType();

  /**
   * @param _dest l'envelope a modifier avec les valeurs de ce maillage
   */
  Envelope getEnvelope(Envelope _dest);

  /**
   * @param _frIdx l'indice de la frontiere
   * @return la fronti�re sous forme de polyligne fermee
   */
  GISPolygone getFrontierRing(int _frIdx);

  /**
   * @return la frontiere
   */
  EfFrontierInterface getFrontiers();

  /**
   * @return le type commun. null si heterogene
   */
  int getMaxPointInElements();

  double getMaxX();

  double getMaxY();

  double getMaxZ();

  /**
   * @param _ptIndex1 l'indice du premier point
   * @param _ptIndex2 l'indice du 2eme point
   * @return le point milieu
   */
  EfNode getMilieu(int _ptIndex1, int _ptIndex2);

  /**
   * @param _eltIdx l'indice de l'element
   * @return l'angle en radian min de cet elt
   */
  double getMinAngle(int _eltIdx);

  /**
   * @param _eltIdx l'indice de l'element
   * @return l'angle en DEGRE min de cet elt
   */
  double getMinAngleDegre(int _eltIdx);

  /**
   * @param _indexBord l'indice de la frontiere
   * @param _dest TODO
   */
  void getMinMaxPointFrontiere(int _indexBord, Envelope _dest);

  double getMinX();

  double getMinY();

  double getMinZ();

  EfNodeMutable[] getNodesMutable();

  EfNode[] getNodes();

  /**
   * @param _progress la barre de progression
   * @return les elements surcontraints ou null si aucun
   */
  int[] getOverstressedElement(ProgressionInterface _progress);

  void getPt(int _i, Coordinate _c);

  int getPtsNb();

  double getPtX(int _i);

  double getPtY(int _i);

  double getPtZ(int _i);

  void setPt(int _i, double _x, double _y);
  
  void setPt(int _i, double _x, double _y, double _z);
  
  /**
   * @param _progress la barre de progression
   * @param _aireMin l'aire mini
   * @return les indices des elements dont l'aire est inferieure a <code>_aireMin</code>.
   */
  int[] getSmallElement(ProgressionInterface _progress, double _aireMin);

  /**
   * Le point SO est le point avec x+y min et y min.
   * 
   * @param _l les indices a parcourir
   * @return l'indice du point SO contenu dans <code>_l</code>
   */
  int getSOPointIndex(EfFrontierInterface _l, int _idx);

  /**
   * @param _l liste de segement
   * @return l'indice du point SO contenu dans la liste <code>_l</code>.
   */
  int getSOPointIndexParmiSegment(List _l);

  /**
   * @param _eltIdx l'indice de l'element a tester
   * @return true si oriente dans le sens trigo
   */
  int isElementTrigoOriente(int _eltIdx);

  /**
   * @return true si les element sont different de nul.
   */
  boolean isEltDefini();

  /**
   * @param _el l'element a tester
   * @return true si l'element est present dans ce maillage
   */
  boolean isEltPresentInGrid(EfElement _el);

  /**
   * @param _m le maillage a tester
   * @param _testZ true si le z doit etre aussi teste
   * @param _epsToCompareDouble la precision pour les comparaison de double
   * @return true si equivalent
   */
  boolean isEquivalent(EfGridInterface _m, boolean _testZ, double _epsToCompareDouble);

  /**
   * Methode utile pour les maillage T6.
   * 
   * @param _idx l'indice du point
   * @return true si extremite
   */
  boolean isExtremePoint(int _idx);

  /**
   * Methode utile pour les maillage T6.
   * 
   * @param _idx l'indice du point
   * @return true si milieu
   */
  boolean isMiddlePoint(int _idx);

  /**
   * Comparaison de 2 maillages :les elements et les points doivent etre dans le meme ordre et exactement identiques.
   * 
   * @param _m le maillage a comparer
   * @param _prog TODO
   * @param _eps TODO
   * @return true si exactement le meme
   */
  boolean isSameStrict(EfGridInterface _m, ProgressionInterface _prog, double _eps);
}
