/*
 * @creation 12 oct. 06
 * @modification $Date: 2007-06-11 13:04:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author fred deniger
 * @version $Id: EfGridDataDefault.java,v 1.3 2007-06-11 13:04:05 deniger Exp $
 */
public class EfGridDataDefault implements EfGridData {

  final EfGridInterface grid_;

  Map varValues_;

  public EfGridDataDefault(final EfGridInterface _grid) {
    super();
    grid_ = _grid;
  }

  public void put(Object _o, EfData _var) {
    if (varValues_ == null) varValues_ = new HashMap();
    varValues_.put(_o, _var);
  }

  @Override
  public EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    return varValues_ == null ? null : (EfData) varValues_.get(_o);
  }

  @Override
  public double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    EfData data = getData(_o, _timeIdx);
    return data == null ? 0 : data.getValue(_idxObjet);
  }

  @Override
  public EfGridInterface getGrid() {
    return grid_;
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return varValues_ != null && varValues_.containsKey(_var);
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    try {
      return getData(_idxVar, 0) != null && getData(_idxVar, 0).isElementData();
    } catch (IOException _evt) {
      FuLog.error(_evt);
    }
    return false;
  }

}
