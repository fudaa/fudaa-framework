/*
 * @creation 22 ao�t 2003
 * @modification $Date: 2006-11-15 09:22:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;

/**
 * @author deniger
 * @version $Id: FileFormatGridVersion.java,v 1.6 2006-11-15 09:22:54 deniger Exp $
 */
public interface FileFormatGridVersion {

  /**
   * @return le nom de la version
   */
  String getName();

  /**
   * @return le format
   */
  FileFormat getFileFormat();

  /**
   * @param _f le fichier a ecrire
   * @param _m la source
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  CtuluIOOperationSynthese writeGrid(File _f, EfGridSource _m, ProgressionInterface _prog);

  /**
   * @param _f le fichier a lire
   * @param _prog la barre de progression
   * @return la synthese (contenant le maillage EfGridSource en tant que source)
   */
  CtuluIOOperationSynthese readGrid(File _f, ProgressionInterface _prog);

  /**
   * @return si le format de fichier contient des conditions limites.
   */
  boolean hasBoundaryConditons();

  /**
   * @return si le format de fichier peut lire des maillages.
   */
  boolean canReadGrid();

  /**
   * @return si le format de fichier peut �crire des maillages.
   */
  boolean canWriteGrid();
}