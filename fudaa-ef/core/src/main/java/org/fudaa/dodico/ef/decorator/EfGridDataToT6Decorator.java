package org.fudaa.dodico.ef.decorator;

import gnu.trove.TIntObjectHashMap;
import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfSegment;

/**
 * @author Fred Deniger
 * @version $Id: MvExportT6Activity.java,v 1.9 2007-06-11 13:08:16 deniger Exp $
 */
public class EfGridDataToT6Decorator implements EfGridData {

  final TIntObjectHashMap idxPt_;
  final EfGridInterface newGrid_;
  final EfGridData old_;

  public EfGridDataToT6Decorator(final EfGridData _old, final TIntObjectHashMap _idxPt, final EfGridInterface _grid) {
    super();
    idxPt_ = _idxPt;
    newGrid_ = _grid;
    old_ = _old;
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return old_.isDefined(_var);
  }

  @Override
  public double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    if (old_.isElementVar(_o) || _idxObjet < old_.getGrid().getPtsNb()) { return old_
        .getData(_o, _timeIdx, _idxObjet); }
    final EfSegment seg = (EfSegment) idxPt_.get(_idxObjet);
    return (old_.getData(_o, _timeIdx, seg.getPt1Idx()) + old_.getData(_o, _timeIdx, seg.getPt2Idx())) / 2;
  }

  @Override
  public EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    final EfData old = old_.getData(_o, _timeIdx);
    if (old == null || old_.isElementVar(_o)) { return old; }
    final double[] data = new double[newGrid_.getPtsNb()];

    for (int i = old.getSize() - 1; i >= 0; i--) {
      data[i] = old.getValue(i);
    }
    for (int i = old.getSize(); i < data.length; i++) {
      final EfSegment seg = (EfSegment) idxPt_.get(i);
      data[i] = (old.getValue(seg.getPt1Idx()) + old.getValue(seg.getPt2Idx())) / 2;

    }
    return new EfDataNode(data);
  }

  @Override
  public EfGridInterface getGrid() {
    return newGrid_;
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return old_.isElementVar(_idxVar);
  }

}