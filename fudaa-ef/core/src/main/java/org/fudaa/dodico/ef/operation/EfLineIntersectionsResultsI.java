/*
 * @creation 12 juin 07
 * @modification $Date: 2007-06-13 12:55:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluVariable;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionsResultsI.java,v 1.1 2007-06-13 12:55:42 deniger Exp $
 */
public interface EfLineIntersectionsResultsI {

  double getDistFromDeb(final int _idxInter);

  EfLineIntersection getIntersect(final int _i);

  int getNbIntersect();

  boolean isAllOut();

  boolean isEmpty();

  boolean isSegmentIn(final int _idxInters);

  boolean isSegmentOut(final int _idxInters);

  boolean isForMesh();
  
  /**
   * @param _v la variable a tester
   * @return true si des donnees sont disponibles pour la variable _v
   */
  boolean isDataAvailable(CtuluVariable _v);

}