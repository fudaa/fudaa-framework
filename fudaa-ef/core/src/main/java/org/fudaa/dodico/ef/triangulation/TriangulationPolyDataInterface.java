/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * Interface permettant de d�finir le contenu de donn�es d'entr�e
 *
 * @author Frederic Deniger
 */
public interface TriangulationPolyDataInterface extends SupportLocationI {

  /**
   *
   * @return le nombre de polygone
   */
  int getNbLines();

  /**
   * @param idxClosedLine entre 0 et getNbLines-1
   * @return le polygone
   */
  LineString getLine(int idxClosedLine);

  boolean isClosed(int idxLine);

  /**
   *
   * @param idxClosedLine entre 0 et getNbLines-1
   * @return true si le polygone i est un trou.
   */
  boolean isClosedAndHole(int idxClosedLine);
}
