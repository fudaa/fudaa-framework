/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-09-11 09:45:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;

/**
 * Classe d�crivant un element d'un maillage volumique.
 * @author Fred Deniger
 */
public class EfElementVolume extends EfElement {

  private final int[] areteIdx_;

  /**
   * Attention, les tableaux ne sont pas recopi�s. Il ne faut pas les modifier par la suite
   * @param _areteIdx les indices des aretes
   * @param _ndsIdx les indices des noeuds : issu des indices des noeuds
   */
  public EfElementVolume(final int[] _areteIdx, final int[] _ndsIdx) {
    super(_ndsIdx);
    areteIdx_ = _areteIdx;
  }

  public int[] getAreteIdxs(){
    return CtuluLibArray.copy(areteIdx_);
  }

  public void fillWithAreteIdx(final TIntHashSet _set){
    _set.addAll(areteIdx_);
  }

  /**
   * @param _idxArete l'indice de l'arete a tester
   * @return true si l'arete d'indice _idxArete est contenue dans cet element
   */
  public boolean containsAreteIdx(final int _idxArete){
    if (areteIdx_ != null) {
      for (int i = areteIdx_.length - 1; i >= 0; i--) {
        if (areteIdx_[i] == _idxArete) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param _i l'indice local de l'arete
   * @return l'indice global de l'arete _i
   */
  public int getIdxArete(final int _i){
    return areteIdx_[_i];
  }

  /**
   * @return le nombre d'arete
   */
  public int getNbAretes(){
    return areteIdx_.length;
  }

  /**
   * @param _x le x a tester
   * @param _y le y a tester
   * @param _grid le maillage support
   * @param _m un point mutable temporaire : peut etre null
   * @return l'indice global de l'arete la plus proche
   */
  public int getNearestEdge(final double _x,final double _y,final EfGridVolumeInterface _grid,final Coordinate _m){
    int r = getIdxArete(0);
    EfSegment s = _grid.getArete(r);
    Coordinate m = _m;
    if (m == null) {
      m = new Coordinate();
    }
    s.getCentre(_grid, m);
    double tmp;
    double d = CtuluLibGeometrie.getD2(_x, _y, m.x, m.y);
    for (int i = areteIdx_.length - 1; i > 0; i--) {
      final int idx = getIdxArete(i);
      s = _grid.getArete(idx);
      s.getCentre(_grid, m);
      tmp = CtuluLibGeometrie.getD2(_x, _y, m.x, m.y);
      if (tmp < d) {
        d = tmp;
        r = idx;
      }
    }
    return r;
  }

}
