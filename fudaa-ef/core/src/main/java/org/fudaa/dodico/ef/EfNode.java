/*
 * @creation 25 juin 2003
 * @modification $Date: 2006-09-19 14:42:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.gis.GISPoint;

/**
 * @author deniger
 * @version $Id: EfNode.java,v 1.9 2006-09-19 14:42:23 deniger Exp $
 */
public class EfNode extends GISPoint {

  /**
   * @param _n1 le premier point
   * @param _n2 le deuxieme point
   * @return le point milieu de [_n1, _n2]
   */
  public static EfNode createMilieu(final EfNode _n1, final EfNode _n2) {
    return new EfNode((_n1.getX() + _n2.getX()) / 2, (_n1.getY() + _n2.getY()) / 2, (_n1.getZ() + _n2.getZ()) / 2);
  }

  /**
   * @return le point milieu de [_n1, _n2]
   */
  public static EfNode createMilieu(final double _x1, final double _y1, final double _z1, final double _x2, final double _y2, final double _z2) {
    return new EfNode((_x1 + _x2) / 2, (_y1 + _y2) / 2, (_z1 + _z2) / 2);
  }

  /**
   * Initialisation par defaut (tout a 0).
   */
  public EfNode() {}

  public EfNode(final double _x, final double _y, final double _z) {
    super(_x, _y, _z);
  }

  /**
   * @param _c la coordonnees
   */
  public EfNode(final Coordinate _c) {
    super(_c.x, _c.y, _c.z);
  }

  /**
   * @param _p le point source pour recuperer les donnees
   */
  public EfNode(final EfNode _p) {
    super(_p);
  }

  protected void internSetXYZ(final double _x, final double _y, final double _z) {
    super.setXYZ(_x, _y, _z);
  }

  protected void internSetX(final double _x) {
    super.setX(_x);
  }

  protected void internSetY(final double _y) {
    super.setY(_y);
  }

  @Override
  protected void setX(final double _x) {
    new Throwable().printStackTrace();
    // super.setX(_x);
  }

  @Override
  protected void setXYZ(final double _x, final double _y, final double _z) {
    new Throwable().printStackTrace();
    // super.setXYZ(_x, _y, _z);
  }

  @Override
  protected void setY(final double _y) {
    new Throwable().printStackTrace();
    // super.setY(_y);
  }

  /*public void setZ(final double _z) {
    // new Throwable().printStackTrace();
    super.setZ(_z);
  }*/

}
