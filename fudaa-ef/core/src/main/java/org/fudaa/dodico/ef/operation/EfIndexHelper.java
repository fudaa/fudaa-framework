/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 */
public final class EfIndexHelper {

  private EfIndexHelper() {

  }

  /**
   * @param _grid
   * @param _x le x
   * @param _y le y
   * @param _prog progression
   * @return l'indice de l'element englobant ou -1 si non trouve
   */
  public static int getElementEnglobant(EfGridInterface _grid, double _x, double _y, ProgressionInterface _prog) {
    if (_grid.getIndex() == null) {
      _grid.createIndexRegular(_prog);
    }
    final EfIndexVisitorNearestElt visitor = new EfIndexVisitorNearestElt(_grid, _x, _y, 0);
    visitor.setOnlyInSearch(true);
    _grid.getIndex().query(EfIndexVisitorNearestNode.getEnvelope(_x, _y, 0), visitor);
    return visitor.isIn() ? visitor.getSelected() : -1;
  }

  /**
   * @param _grid
   * @param _x le x
   * @param _y le y
   * @param _tolerance la tolerance pour la recherche
   * @param _prog la barre de progression
   * @return la selection trouvee
   */
  public static CtuluListSelection getNearestNode(EfGridInterface _grid, double _x, double _y, double _tolerance,
      ProgressionInterface _prog) {
    if (_grid.getIndex() == null) {
      _grid.createIndexRegular(_prog);
    }
    final EfIndexVisitorNearestNode visitor = new EfIndexVisitorNearestNode(_grid, _x, _y, _tolerance);
    _grid.getIndex().query(EfIndexVisitorNearestNode.getEnvelope(_x, _y, _tolerance), visitor);
    return visitor.getSelection();
  }

  public static CtuluListSelection getNearestElement(EfGridInterface _grid, double _x, double _y, double _tolerance,
      ProgressionInterface _prog) {
    if (_grid.getIndex() == null) {
      _grid.createIndexRegular(_prog);
    }
    final EfIndexVisitorNearestElt visitor = new EfIndexVisitorNearestElt(_grid, _x, _y, _tolerance);
    _grid.getIndex().query(EfIndexVisitorNearestNode.getEnvelope(_x, _y, _tolerance), visitor);
    return visitor.getSelection();
  }
}
