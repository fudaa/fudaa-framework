/*
 *  @creation     22 avr. 2005
 *  @modification $Date: 2007-01-19 13:07:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;

/**
 * @author Fred Deniger
 * @version $Id: EfGridArray.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridArray extends EfGridAbstract {

  double[] xyz_;
  int nbPt_;

  public EfGridArray(final EfNode[] _pts, final EfElement[] _elt) {
    super(_elt);
    initPtFrom(_pts);
  }

  public EfGridArray(final EfNode[] _pts, final EfElement[] _elt, final EfElementType _type) {
    super(_elt);
    initPtFrom(_pts);
    typeElt_ = _type;
  }

  public EfGridArray(final EfElement[] _elt, final EfGridInterface _gridForPoint, final EfElementType _type) {
    super(_elt);
    typeElt_ = _type;
    initPtFrom(_gridForPoint);
  }

  public EfGridArray(final double[] _pts, final EfElement[] _elt) {
    super(_elt);
    xyz_ = CtuluLibArray.copy(_pts);
    nbPt_ = _pts.length / 3;
    if (_pts.length % 3 != 0) {
      throw new IllegalArgumentException();
    }
  }

  public EfGridArray(final EfGridAbstract _g) {
    super(_g.getElts());
    initPtFrom(_g);
    super.initEnvelopFrom(_g);
    super.ptsFrontiere_ = _g.ptsFrontiere_;
    typeElt_ = _g.typeElt_;
  }

  public EfGridArray(final EfGridInterface _g) {
    super(_g.getElts());
    initPtFrom(_g);
    super.ptsFrontiere_ = _g.getFrontiers();
    typeElt_ = _g.getEltType();
  }

  /**
   * @param _g le maillage servant � l'initialisation
   */
  public EfGridArray(final EfGrid _g) {
    super(_g.elts_);
    initPtFrom(_g);
  }

  protected final void initPtFrom(final EfGridInterface _g) {
    nbPt_ = _g.getPtsNb();
    xyz_ = new double[nbPt_ * 3];
    for (int i = 0; i < nbPt_; i++) {
      xyz_[i * 3] = _g.getPtX(i);
      xyz_[i * 3 + 1] = _g.getPtY(i);
      xyz_[i * 3 + 2] = _g.getPtZ(i);
    }

    clearIndex();
  }

  protected final void initPtFrom(final EfNode[] _g) {
    nbPt_ = _g.length;
    xyz_ = new double[nbPt_ * 3];
    for (int i = 0; i < nbPt_; i++) {
      xyz_[i * 3] = _g[i].getX();
      xyz_[i * 3 + 1] = _g[i].getY();
      xyz_[i * 3 + 2] = _g[i].getZ();
    }
    clearIndex();
  }

  @Override
  public int getPtsNb() {
    return nbPt_;
  }

  @Override
  public double getPtX(final int _i) {
    return xyz_[_i * 3];
  }

  @Override
  public double getPtY(final int _i) {
    return xyz_[_i * 3 + 1];
  }

  @Override
  public double getPtZ(final int _i) {
    return xyz_[_i * 3 + 2];
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y) {
    this.setPt(_i, _x, _y, 0.0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y, double _z) {
    xyz_[_i * 3] = _x;
    xyz_[_i * 3 + 1] = _y;
    xyz_[_i * 3 + 2] = _z;
    geomXYChanged();
  }

  @Override
  protected boolean setZIntern(final int _i, final double _newV) {
    final int i = _i * 3 + 2;
    if (xyz_[i] != _newV) {
      xyz_[i] = _newV;
      return true;
    }
    return false;
  }
}
