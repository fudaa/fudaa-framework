/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation.type;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.decorator.EfGridDataToT6Decorator;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;
import org.fudaa.dodico.ef.operation.EfOperationResult;

/**
 * @author deniger
 */
public class EfOperationToT6 extends AbstractEfOperation {

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    // si c'est deja un maillage T6
    if (initGrid.getGrid().getEltType() == EfElementType.T6) { return initGrid; }
    EfGridData last = initGrid;
    setFrontierChanged();
    if (initGrid.getGrid().getEltType() != EfElementType.T3) {

      EfOperationToT3 toT3 = new EfOperationToT3();
      toT3.setInitGridData(initGrid);
      EfOperationResult process = toT3.process(prog);
      setCurrentActivity(toT3);
      if (process == null) return null;
      log.merge(process.getAnalyze());
      last=process.getGridData();
    }
    final TObjectIntHashMap segInt = new TObjectIntHashMap();
    final EfGridInterface gridT6 = EfLibImpl.maillageT3enT6(last.getGrid(), prog, segInt);
    if (stop) { return null; }
    final TIntObjectHashMap newIdxSeg = new TIntObjectHashMap(segInt.size());
    final TObjectIntIterator iterator = segInt.iterator();
    for (int i = segInt.size(); i-- > 0;) {
      iterator.advance();
      newIdxSeg.put(iterator.value(), iterator.key());
    }
    return new EfGridDataToT6Decorator(last, newIdxSeg, gridT6);
  }
}
