/*
 * @creation 9 oct. 06
 * @modification $Date: 2007-04-20 16:21:10 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitorNodeSelect.java,v 1.1 2007-04-20 16:21:10 deniger Exp $
 */
public class EfIndexVisitorNodeSelect implements EfIndexVisitor {

  final TIntHashSet selection_;
  final double distAuCarre_;
  final double dist_;
  final EfGridInterface grid_;
  double x_;
  double y_;

  public EfIndexVisitorNodeSelect(final EfGridInterface _grid, final double _dist) {
    grid_ = _grid;
    dist_ = _dist;
    distAuCarre_ = dist_ * dist_;
    selection_ = new TIntHashSet(10);
  }

  public static Envelope buildSearchEnvelop(final double _x, final double _y, final double _err) {
    final Envelope res = new Envelope();
    res.expandToInclude(_x, _y);
    res.expandBy(_err, _err);
    return res;

  }

  public Envelope buildSearchEnvelop() {
    return buildSearchEnvelop(x_, y_, dist_);

  }

  public void initFor(final double _x, final double _y) {
    selection_.clear();
    x_ = _x;
    y_ = _y;
  }

  @Override
  public void visitItem(final int _item) {
    final EfElement el = grid_.getElement(_item);
    for (int i = el.getPtNb() - 1; i >= 0; i--) {
      final int idx = el.getPtIndex(i);
      if (CtuluLibGeometrie.getD2(x_, y_, grid_.getPtX(idx), grid_.getPtY(idx)) <= distAuCarre_) {
        selection_.add(idx);
      }
    }
  }

  public int[] getNodeIdx() {
    return selection_.toArray();
  }

}
