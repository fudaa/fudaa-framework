package org.fudaa.dodico.ef;

/**
 * Represente des enums qui peuvent lus dans les fichiers de maillage et qui
 * représentes des conditions limites.
 * 
 * @author deniger
 * 
 */
public enum ConditionLimiteEnum {

    // C PAR DEFAUT, ON SUPPOSE QUE LE POINT EST UN POINT FRONTIERE SOLIDE
    // C SANS FROTTEMENT. LA COULEUR 11, STANDARD POUR SUPERTAB, DONNE CE
    // C TYPE DE CARACTERISTIQUE.
    // LIHBOR(J)=ILOG
    // LIUBOR(J)=ILOG
    // LIVBOR(J)=ILOG
    // LITBOR(J)=ILOG
    SOLID(ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG,
	    ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG),

    // IF (NCOLFR(J).EQ.1) THEN
    // C H IMPOSEE , U ET V LIBRES
    // LIHBOR(J)=IENT
    // LIUBOR(J)=ISORT
    // LIVBOR(J)=ISORT
    // LITBOR(J)=ISORT
    H_IMPOSE_U_V_LIBRE(ConditionLimiteHelper.IENT, ConditionLimiteHelper.ISORT,
	    ConditionLimiteHelper.ISORT, ConditionLimiteHelper.ISORT),

    // ELSE IF (NCOLFR(J).EQ.2) THEN
    // C H IMPOSEE , DEBIT IMPOSE
    // LIHBOR(J)=IENT
    // LIUBOR(J)=IENT
    // LIVBOR(J)=IENT
    // LITBOR(J)=IENT
    H_Q_IMPOSE(ConditionLimiteHelper.IENT, ConditionLimiteHelper.IENT,
	    ConditionLimiteHelper.IENT, ConditionLimiteHelper.IENT),

    // ELSE IF (NCOLFR(J).EQ.3) THEN
    // C H , U ET V IMPOSEES
    // LIHBOR(J)=IENT
    // LIUBOR(J)=IENTU
    // LIVBOR(J)=IENTU
    // LITBOR(J)=IENT
    H_U_V_IMPOSE(ConditionLimiteHelper.IENT, ConditionLimiteHelper.IENTU,
	    ConditionLimiteHelper.IENTU, ConditionLimiteHelper.IENT),

    // ELSE IF (NCOLFR(J).EQ.4) THEN
    // C H IMPOSEE , U LIBRE , V NULLE
    // LIHBOR(J)=IENT
    // LIUBOR(J)=ISORT
    // LIVBOR(J)=IADH
    // LITBOR(J)=ISORT
    H_IMPOSE_U_LIBRE_V_NULL(ConditionLimiteHelper.IENT,
	    ConditionLimiteHelper.ISORT, ConditionLimiteHelper.IADH,
	    ConditionLimiteHelper.ISORT),

    // ELSE IF (NCOLFR(J).EQ.5) THEN
    // C CONDITION D'ONDE INCIDENTE
    // LIHBOR(J)=IINC
    // LIUBOR(J)=IINC
    // LIVBOR(J)=IINC
    // LITBOR(J)=ISORT
    ONDE_INCIDENCE(ConditionLimiteHelper.IINC, ConditionLimiteHelper.IINC,
	    ConditionLimiteHelper.IINC, ConditionLimiteHelper.ISORT),

    // RIEN EN 6 donc meme code que solid
    NONE_6(ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG,
	    ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG),

    // ELSE IF (NCOLFR(J).EQ.7) THEN
    // C H IMPOSEE , U NULLE , V LIBRE
    // LIHBOR(J)=IENT
    // LIUBOR(J)=IADH
    // LIVBOR(J)=ISORT
    // LITBOR(J)=ISORT
    H_IMPOSEE_U_NULLE_V_LIBRE(ConditionLimiteHelper.IENT,
	    ConditionLimiteHelper.IADH, ConditionLimiteHelper.ISORT,
	    ConditionLimiteHelper.ISORT),

    // ELSE IF (NCOLFR(J).EQ.8) THEN
    // C H LIBRE , U ET V IMPOSEES
    // LIHBOR(J)=ISORT
    // LIUBOR(J)=IENT
    // LIVBOR(J)=IENT
    // LITBOR(J)=IENT
    H_LIBRE_U_V_IMPOSE(ConditionLimiteHelper.ISORT, ConditionLimiteHelper.IENT,
	    ConditionLimiteHelper.IENT, ConditionLimiteHelper.IENT),

    // ELSE IF (NCOLFR(J).EQ.9) THEN
    // C H LIBRE , U ET V IMPOSEES
    // C
    // LIHBOR(J)=ISORT
    // LIUBOR(J)=IENTU
    // LIVBOR(J)=IENTU
    // LITBOR(J)=IENT
    H_LIBRE_U_V_IMPOSE_BIS(ConditionLimiteHelper.ISORT,
	    ConditionLimiteHelper.IENTU, ConditionLimiteHelper.IENTU,
	    ConditionLimiteHelper.IENT), // bizarre vitesse impose doit etre en
    // 6 et la c'est
    // en 5
    // RIEN EN 10 donc meme code que solid

    NONE_10(ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG,
	    ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG),

    // RIEN EN 11 donc meme code que solid
    NONE_11(ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG,
	    ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG),

    // ELSE IF (NCOLFR(J).EQ.12) THEN
    // C
    // C H LIBRE , U IMPOSEE , V NULLE
    // C
    // LIHBOR(J)=ISORT
    // LIUBOR(J)=IENT
    // LIVBOR(J)=IADH
    // LITBOR(J)=IENT
    H_LIBRE_U_IMPOSE_V_NULLE(ConditionLimiteHelper.ISORT,
	    ConditionLimiteHelper.IENT, ConditionLimiteHelper.IADH,
	    ConditionLimiteHelper.IENT),

    // ELSE IF (NCOLFR(J).EQ.13) THEN
    // C FRONTIERE SOLIDE AVEC V NULLE
    // LIHBOR(J)=ILOG
    // LIUBOR(J)=ILOG
    // LIVBOR(J)=IADH
    // LITBOR(J)=ILOG
    SOLIDE_V_NULLE(ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG,
	    ConditionLimiteHelper.IADH, ConditionLimiteHelper.ILOG),

    // ELSE IF (NCOLFR(J).EQ.14) THEN
    // C FRONTIERE SOLIDE AVEC U NULLE
    // LIHBOR(J)=ILOG
    // LIUBOR(J)=IADH
    // LIVBOR(J)=ILOG
    // LITBOR(J)=ILOG
    SOLIDE_U_NULLE(ConditionLimiteHelper.ILOG, ConditionLimiteHelper.IADH,
	    ConditionLimiteHelper.ILOG, ConditionLimiteHelper.ILOG),

    // ELSE IF (NCOLFR(J).EQ.15) THEN
    // C H LIBRE , U NULLE , V IMPOSEE
    // LIHBOR(J)=ISORT
    // LIUBOR(J)=IADH
    // LIVBOR(J)=IENT
    // LITBOR(J)=IENT
    H_LIBRE_U_NULLE_V_IMPOSEE(ConditionLimiteHelper.ISORT,
	    ConditionLimiteHelper.IADH, ConditionLimiteHelper.IENT,
	    ConditionLimiteHelper.IENT);

    private final int codeHauteur;
    private final int codeVitesseU;
    private final int codeVitesseV;
    private final int codeTraceur;

    /**
     * @param codeHauteur
     *            code pour la hauteur
     * @param codeVitesseU
     *            code pour la vitesse u
     * @param codeVitesseV
     *            code pour la vitesse v
     * @param codeTraceur
     *            code pour le traceur
     */
    private ConditionLimiteEnum(int codeHauteur, int codeVitesseU,
	    int codeVitesseV, int codeTraceur) {
	this.codeHauteur = codeHauteur;
	this.codeVitesseU = codeVitesseU;
	this.codeVitesseV = codeVitesseV;
	this.codeTraceur = codeTraceur;
    }

    public int getCodeHauteur() {
	return codeHauteur;
    }

    public int getCodeTraceur() {
	return codeTraceur;
    }

    public int getCodeVitesseU() {
	return codeVitesseU;
    }

    public int getCodeVitesseV() {
	return codeVitesseV;
    }

}
