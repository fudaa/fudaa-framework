/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.decorator;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Un decorateur permettant de cacher les donn�es EfData pour un pas de temps precis. Utilise des WeakReference pour les
 * EfData sauf pour les variables definies en tant que persistantes. Ce gridData ne renvoie que le donnees au pas de
 * temps passe dans le constructeur.
 * 
 * @author deniger
 */
public class EfGridDataCacheOneTimeDecorator implements EfGridData {

  final EfGridData init;

  Set<CtuluVariable> permanentVariable_;

  final Map<CtuluVariable, EfData> permCache_;
  final int timeCached_;
  final Map<CtuluVariable, WeakReference<EfData>> transCache_;

  /**
   * @param _init les donnees initiales
   * @param _timeCached le pas de temps a utilise
   * @param _permVariable
   */
  public EfGridDataCacheOneTimeDecorator(EfGridData _init, int _timeCached, CtuluVariable... _permVariable) {
    super();
    init = _init;
    timeCached_ = _timeCached;
    if (_permVariable != null) {
      permanentVariable_ = new HashSet<CtuluVariable>(Arrays.asList(_permVariable));
      permCache_ = new HashMap<CtuluVariable, EfData>(permanentVariable_.size());
    } else {
      permanentVariable_ = Collections.emptySet();
      permCache_ = Collections.emptyMap();
    }
    transCache_ = new HashMap<CtuluVariable, WeakReference<EfData>>(20);

  }

  protected EfData getCachedData(CtuluVariable _o) {
    if (permanentVariable_.contains(_o)) { return permCache_.get(_o); }
    WeakReference<EfData> res = transCache_.get(_o);
    if (res == null) return null;
    return res.get();

  }

  /**
   * {@inheritDoc}
   * 
   * @param _timeIdx ce pas de temps n'est pas pris en compte
   */
  @Override
  public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
    EfData initData = getCachedData(_o);
    if (initData != null) return initData;
    EfData data = init.getData(_o, timeCached_);
    setCachedData(_o, data);
    return data;
  }

  /**
   * {@inheritDoc}
   * 
   * @param _timeIdx ce pas de temps n'est pas pris en compte
   */
  @Override
  public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
    EfData data = getCachedData(_o);
    if (data != null) return data.getValue(_idxObjet);
    return init.getData(_o, timeCached_, _idxObjet);
  }

  /**
   * @return
   * @see org.fudaa.dodico.ef.EfGridData#getGrid()
   */
  @Override
  public EfGridInterface getGrid() {
    return init.getGrid();
  }

  /**
   * @return the timeCached
   */
  public int getTimeCached() {
    return timeCached_;
  }

  /**
   * @param _var
   * @return
   * @see org.fudaa.dodico.ef.EfGridData#isDefined(org.fudaa.ctulu.CtuluVariable)
   */
  @Override
  public boolean isDefined(CtuluVariable _var) {
    return init.isDefined(_var);
  }

  /**
   * @param _idxVar
   * @return
   * @see org.fudaa.dodico.ef.EfGridData#isElementVar(org.fudaa.ctulu.CtuluVariable)
   */
  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return init.isElementVar(_idxVar);
  }

  protected void setCachedData(CtuluVariable _o, EfData _data) {
    if (permanentVariable_.contains(_o)) {
      permCache_.put(_o, _data);
    } else transCache_.put(_o, new WeakReference<EfData>(_data));

  }

}
