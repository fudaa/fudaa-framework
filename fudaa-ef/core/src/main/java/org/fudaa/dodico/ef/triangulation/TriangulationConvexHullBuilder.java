/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.algorithm.ConvexHull;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;
import java.util.TreeSet;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.comparator.CoordinateComparator;

/**
 * Permet de d�terminer l'enveloppe convexe d'un ensemble de donn�es points/polylignes
 *
 * @author Frederic Deniger
 */
public class TriangulationConvexHullBuilder {

  public int getNbExternPolygon(TriangulationPolyDataInterface data) {
    int nbExtern = 0;
    int nbPoly = data.getNbLines();
    for (int i = 0; i < nbPoly; i++) {
      if (!data.isClosedAndHole(i)) {
        nbExtern++;
      }
    }
    return nbExtern;
  }

  public int getExternPolygonIdx(TriangulationPolyDataInterface data) {
    int nbPoly = data.getNbLines();
    for (int i = 0; i < nbPoly; i++) {
      if (!data.isClosedAndHole(i)) {
        return i;
      }
    }
    return -1;
  }

  public TriangulationPolyDataInterface addConvexHullIfNeeded(TriangulationPolyDataInterface data) {
    int nbExern = getNbExternPolygon(data);
    if (nbExern >= 1) {
      return data;
    }
    TreeSet<Coordinate> cs = new TreeSet<Coordinate>(new CoordinateComparator());
    for (int idxPoint = 0; idxPoint < data.getPtsNb(); idxPoint++) {
      cs.add(new Coordinate(data.getPtX(idxPoint), data.getPtY(idxPoint)));
    }
    for (int idxPoly = 0; idxPoly < data.getNbLines(); idxPoly++) {
      LineString poly = data.getLine(idxPoly);
      for (int idxPt = 0; idxPt < poly.getNumPoints(); idxPt++) {
        cs.add(new Coordinate(poly.getCoordinateN(idxPt)));
      }
    }
    Coordinate[] coordinates = (Coordinate[]) cs.toArray(new Coordinate[cs.size()]);
    ConvexHull convexHull = new ConvexHull(coordinates, GISGeometryFactory.INSTANCE);
    Object o = convexHull.getConvexHull();
    if (o instanceof Polygon) {
      o = ((Polygon) o).getExteriorRing();
    }
    return new TriangulationPolyDataConvexHullAdapter(data, (LinearRing) o);
  }
}
