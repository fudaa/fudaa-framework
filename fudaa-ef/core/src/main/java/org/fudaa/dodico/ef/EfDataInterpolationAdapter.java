/*
 * @creation 30 mai 07
 * @modification $Date: 2007-06-05 08:59:13 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.fudaa.ctulu.interpolation.InterpolationCollectionAdapter;

/**
 * @author fred deniger
 * @version $Id: EfDataInterpolationAdapter.java,v 1.1 2007-06-05 08:59:13 deniger Exp $
 */
public final class EfDataInterpolationAdapter {

  public static class Norm extends InterpolationCollectionAdapter.Norm implements EfData {

    public Norm(EfData _vx, EfData _vy) {
      super(_vx, _vy);
      vx_ = _vx;
    }

    final EfData vx_;

    @Override
    public boolean isElementData() {
      return vx_.isElementData();
    }

  }
  public static class Theta extends InterpolationCollectionAdapter.Theta implements EfData {

    public Theta(EfData _vx, EfData _vy) {
      super(_vx, _vy);
      vx_ = _vx;
    }

    final EfData vx_;

    @Override
    public boolean isElementData() {
      return vx_.isElementData();
    }
  }
}
