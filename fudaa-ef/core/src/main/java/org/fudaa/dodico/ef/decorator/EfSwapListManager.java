package org.fudaa.dodico.ef.decorator;

import gnu.trove.TIntIntHashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A utiliser lorsque l'on fait des �changes d'�l�ments dans shufle. Permet de
 * �changer les objets dans une liste et de conserver un tableau de
 * correspondance: nouvel indice->ancien indice.
 * 
 * @author deniger
 * @creation 7 oct. 2009
 * 
 * @param <T> les objets de la liste swapp�es
 */
public class EfSwapListManager<T> {
    private final List<T> list;
    private final TIntIntHashMap newIdxOldIdx = new TIntIntHashMap();

    public EfSwapListManager(List<T> list) {
        super();
        this.list = new ArrayList<T>(list);
    }

    public EfSwapListManager(T... list) {
        this(new ArrayList<T>(Arrays.asList(list)));
    }

    private void removeUnusedSwap(int i) {
        if (newIdxOldIdx.get(i) == i) {
            newIdxOldIdx.remove(i);
        }
    }

    protected int getOldPosition(int newIdx) {
        if (newIdxOldIdx.contains(newIdx))
            return newIdxOldIdx.get(newIdx);
        return newIdx;
    }

    /**
     * @param i l'indice dans la nouvelle liste
     * @return l'objet en position i dans la nouvelle liste.
     */
    public T get(int i) {
        return list.get(i);
    }

    /**
     * @return the list
     */
    public List<T> getList() {
        return list;
    }

    /**
     * @return the newIdxOldIdx: la correspondance nouvelle indice->ancien
     */
    public TIntIntHashMap getNewIdxOldIdx() {
        return newIdxOldIdx;
    }

    public void swap(int i, int j) {
        //on ne fait rien si meme indice.
        if (i == j)
            return;
        T oldValFori = list.get(i);
        list.set(i, list.get(j));
        list.set(j, oldValFori);
        //mise a jour de la table de correspondance
        int oldj = getOldPosition(j);
        int oldi = getOldPosition(i);
        newIdxOldIdx.put(i, oldj);
        newIdxOldIdx.put(j, oldi);
        removeUnusedSwap(i);
        removeUnusedSwap(j);
    }

}
