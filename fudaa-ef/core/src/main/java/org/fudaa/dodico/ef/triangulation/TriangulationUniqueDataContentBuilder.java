/*
 GPL 2
 */
package org.fudaa.dodico.ef.triangulation;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;
import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.comparator.CoordinateComparator;

/**
 * Permet de construire un TriangulationUniqueDataContent � partir d'un TriangulationPolyDataInterface. Ce contenu �vite les doublons au niveau des
 * points.
 *
 * @author Frederic Deniger
 */
public class TriangulationUniqueDataContentBuilder {

  TriangulationPolyDataInterface in;
  CoordinateComparator coordinateComparator;

  public TriangulationUniqueDataContentBuilder(TriangulationPolyDataInterface in) {
    this.in = in;
    coordinateComparator = new CoordinateComparator();
  }

  public TriangulationUniqueDataContentBuilder(TriangulationPolyDataInterface in, double eps) {
    this.in = in;
    coordinateComparator = new CoordinateComparator(eps);
  }

  public TriangulationUniqueDataContent build(ProgressionInterface prog) {
    List<Coordinate> ptCoordinate = new ArrayList<Coordinate>();
    TIntArrayList linearRingContainingPoint = new TIntArrayList();
    TreeMap<Coordinate, Integer> idxByCoordinate = new TreeMap<Coordinate, Integer>(coordinateComparator);
    int nb = in.getPtsNb();
    final int nbPoly = in.getNbLines();
    ProgressionUpdater updater = new ProgressionUpdater();
    updater.setValue(10, nb + nbPoly);
    int idxPtAdded = 0;
    for (int i = 0; i < nb; i++) {
      final Coordinate coordinate = new Coordinate(in.getPtX(i), in.getPtY(i));
      if (!idxByCoordinate.containsKey(coordinate)) {
        ptCoordinate.add(coordinate);
        //point interne:
        linearRingContainingPoint.add(-1);
        idxByCoordinate.put(coordinate, idxPtAdded++);
      }
      updater.majAvancement();
    }
    int[][] idx = new int[nbPoly][];
    for (int idxPoly = 0; idxPoly < nbPoly; idxPoly++) {
      final LineString line = in.getLine(idxPoly);
      CoordinateSequence coordinateSequence = line.getCoordinateSequence();
      int nbPt = in.isClosed(idxPoly) ? coordinateSequence.size() - 1 : coordinateSequence.size();//premier point = dernier point
      idx[idxPoly] = new int[nbPt];
      for (int idxPt = 0; idxPt < nbPt; idxPt++) {
        Coordinate coordinate = coordinateSequence.getCoordinate(idxPt);
        Integer setIdx = idxByCoordinate.get(coordinate);
        if (setIdx == null) {
          setIdx = ptCoordinate.size();
          ptCoordinate.add(coordinate);
          linearRingContainingPoint.add(idxPoly);
        } else {
          //le point appartient au linearring:
          linearRingContainingPoint.set(setIdx, idxPoly);
        }
        idx[idxPoly][idxPt] = setIdx;
      }
      updater.majAvancement();
    }
    return new TriangulationUniqueDataContent(ptCoordinate, linearRingContainingPoint.toNativeArray(), idx);
  }
}
