/*
 * @creation 10 oct. 06
 * @modification $Date: 2007-04-20 16:21:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexInterface;

/**
 * @author fred deniger
 * @version $Id: EfIndexerActivity.java,v 1.1 2007-04-20 16:21:06 deniger Exp $
 */
public class EfIndexerActivity implements CtuluActivity {

  final EfGridInterface grid_;
  int maxSize_ = 500;

  boolean stop_;

  public EfIndexerActivity(final EfGridInterface _grid) {
    super();
    grid_ = _grid;
  }

  /**
   * @return permet de recuperer le nombre de cellules horizontales selon le nombre d'�l�ments
   */
  private int getNbXCells() {
    final int nbElt = grid_.getEltNb();
    if (nbElt < 5000) {
      return 4;
    }
    if (nbElt < 100000) {
      return 7;
    }
    return 21;
  }

  public EfIndexInterface createRegularIndex(final ProgressionInterface _proj) {
    final int nbCells = getNbXCells();
    final int total = nbCells * nbCells;
    final List firstResEnv = new ArrayList(total);
    final List firstResInt = new ArrayList(total);
    final int[] elt = new int[grid_.getEltNb()];
    for (int i = elt.length - 1; i >= 0; i--) {
      elt[i] = i;
    }
    eltEnv_ = new Envelope[elt.length];
    for (int i = 0; i < elt.length; i++) {
      eltEnv_[i] = grid_.getElement(i).getEnv(grid_);
    }

    final ProgressionUpdater up = new ProgressionUpdater(_proj);
    up.majProgessionStateOnly(DodicoLib.getS("Indexation des �l�ments"));
    up.setValue(5, nbCells, 20, 60);
    up.majProgessionStateOnly();
    split(grid_.getEnvelope(null), elt, nbCells, firstResEnv, firstResInt, up);
    // si les elements sont mal r�partis, certaines cellules contiendront bien plus d'�l�ments
    // que d'autre. Pour �viter cela, on refait un tour et on divise
    // en 4 les cellules surpeupl�es.

    // le nombre accept�s par cellule : 10% acceptes
    final int nbMoy = (int) ((grid_.getEltNb() / total) * 1.1);
    // le 30 est purement arbritraire ...
    final List resEnv = new ArrayList(total + 30);
    final List resInt = new ArrayList(total + 30);
    final int nb = firstResEnv.size();
    up.setValue(4, nb, 60, 40);
    up.majProgessionStateOnly();

    for (int i = 0; i < nb; i++) {
      final int[] idx = (int[]) firstResInt.get(i);
      // surpeupl�e:
      if (idx.length > nbMoy) {
        split((Envelope) firstResEnv.get(i), idx, 2, resEnv, resInt, null);
      } else {
        resEnv.add(firstResEnv.get(i));
        resInt.add(idx);
      }
      up.majAvancement();
    }
    // on agrandit les enveloppe pour �viter les problemes dus aux arrondis et aux
    // repr�sentation des doubles en binaire.
    for (int i = resEnv.size() - 1; i >= 0; i--) {
      ((Envelope) resEnv.get(i)).expandBy(errForEnv_, errForEnv_);
    }

    return new EfIndexRegular((Envelope[]) resEnv.toArray(new Envelope[resEnv.size()]), CtuluLibArray.toArray(resInt));

  }

  Envelope[] eltEnv_;
  /**
   * Le nombre minimal d'�lements que devrait contenir une cellule. Si une cellule contient moins d'�l�ments que cette
   * limite, elle ne devrait pas etre divis�e.
   */
  final int minSize_ = 100;

  /**
   * Pour �viter les erreurs d'arrondi, on agrandi les enveloppe d'un mm afin que tous les points soient bien contenus.
   */
  final double errForEnv_ = 0.0005;

  private boolean split(final Envelope _initEnv, final int[] _nb, final int _nbXCells, final List _destEnv, final List _destInt,
      final ProgressionUpdater _up) {
    if (_nb.length < minSize_) {
      _destEnv.add(_initEnv);
      _destInt.add(_nb);
      return false;
    }
    final int nbXCell = _nbXCells;
    final int size = nbXCell * nbXCell;
    final double dx = _initEnv.getWidth() / nbXCell;
    final double dy = _initEnv.getHeight() / nbXCell;
    final Envelope[] env = new Envelope[size];
    final double[] xValues = new double[nbXCell + 1];
    final double[] yValues = new double[nbXCell + 1];
    xValues[0] = _initEnv.getMinX();
    yValues[0] = _initEnv.getMinY();
    xValues[nbXCell] = _initEnv.getMaxX();
    yValues[nbXCell] = _initEnv.getMaxY();
    for (int i = 1; i < nbXCell; i++) {
      xValues[i] = xValues[i - 1] + dx;
      yValues[i] = yValues[i - 1] + dy;
    }
    int idx = 0;
    for (int j = 0; j < nbXCell; j++) {
      for (int i = 0; i < nbXCell; i++) {
        env[idx] = new Envelope(xValues[i], xValues[i + 1], yValues[j], yValues[j + 1]);
        idx++;
      }
    }
    final int nbElt = _nb.length;
    final TIntArrayList[] tmplist = new TIntArrayList[size];
    final int defSize = (int) (1.5D * nbElt / size);
    for (int i = tmplist.length - 1; i >= 0; i--) {
      tmplist[i] = new TIntArrayList(defSize);
    }
    for (int i = 0; i < nbElt; i++) {
      final int integer = _nb[i];
      final Envelope ei = eltEnv_[integer];
      // Optimisation: on recupere les valeur min, max de l'element
      final double minx = ei.getMinX();
      final double maxx = ei.getMaxX();
      final double maxy = ei.getMaxY();
      final double miny = ei.getMinY();

      // on recherche avec quelles cellules x s'intersecte cette element
      int minXIdx = Arrays.binarySearch(xValues, minx);
      if (minXIdx < 0) {
        minXIdx = -minXIdx - 2;
      } else {
        minXIdx--;
      }
      int maxXIdx = Arrays.binarySearch(xValues, maxx);
      if (maxXIdx < 0) {
        maxXIdx = -maxXIdx - 2;
      } else {
        maxXIdx++;
      }

      // on recherche avec quelles cellules x s'intersecte cette element
      int minYIdx = Arrays.binarySearch(yValues, miny);
      if (minYIdx < 0) {
        minYIdx = -minYIdx - 2;
      } else {
        minYIdx--;
      }
      int maxYIdx = Arrays.binarySearch(yValues, maxy);
      if (maxYIdx < 0) {
        maxYIdx = -maxYIdx - 2;
      } else {
        maxYIdx++;
      }

      // on verifie que les indices min,max soient bien dans les limites
      minXIdx = Math.max(0, minXIdx);
      minXIdx = Math.min(nbXCell - 1, minXIdx);

      maxXIdx = Math.max(0, maxXIdx);
      maxXIdx = Math.min(nbXCell - 1, maxXIdx);

      minYIdx = Math.max(0, minYIdx);
      minYIdx = Math.min(nbXCell - 1, minYIdx);

      maxYIdx = Math.max(0, maxYIdx);
      maxYIdx = Math.min(nbXCell - 1, maxYIdx);

      // on parcourt les cellules selectionnees et on test s'il y intersection
      for (int ix = minXIdx; ix <= maxXIdx; ix++) {
        for (int iy = minYIdx; iy <= maxYIdx; iy++) {
          final int j = ix + iy * nbXCell;
          final Envelope envj = env[j];
          // pour �viter la m�thode intersects de Enveloppe qui fait des tests
          // couteux et inutiles dans notre cas.
          if (!(envj.getMinX() > maxx || envj.getMaxX() < minx || envj.getMinY() > maxy || envj.getMaxY() < miny)) {
            tmplist[j].add(integer);
          }
        }
      }
    }
    for (int i = 0; i < size; i++) {
      if (tmplist[i].size() > 0) {
        _destInt.add(tmplist[i].toNativeArray());
        _destEnv.add(env[i]);
        tmplist[i] = null;
      }
    }

    if (_up != null) {
      _up.majAvancement();
    }
    return true;
  }

  @Override
  public void stop() {
    stop_ = true;

  }

}
