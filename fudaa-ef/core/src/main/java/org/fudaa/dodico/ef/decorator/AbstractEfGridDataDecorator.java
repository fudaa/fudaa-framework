/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.decorator;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Une classe qui permet d'encapsuler les r�sultats d'une op�ration sur un maillage dans un conteneur EfGridData. Il est
 * suppos� que les types des valeurs ( valeurs aux noeuds ou aux �l�ments) ne sont pas modifi�es par l'op�rations
 * 
 * @author deniger
 */
public abstract class AbstractEfGridDataDecorator implements EfGridData {

  private final EfGridData init;
  private final EfGridInterface newGrid;

  /**
   * @param init les donn�es initiales avant l'op�ration sur le maillage
   * @param newGrid le nouveau maillage
   */
  public AbstractEfGridDataDecorator(EfGridData init, EfGridInterface newGrid) {
    super();
    this.init = init;
    this.newGrid = newGrid;
  }

  /**
   *
   */
  @Override
  public double getData(CtuluVariable o, int timeIdx, int idxObjet) throws IOException {
    return getData(o, timeIdx).getValue(idxObjet);
  }

  /**
   * @return le nouveau maillage obtenue par l'op�ration
   */
  @Override
  public EfGridInterface getGrid() {
    return newGrid;
  }

  /**
   * @return the init le gridData initial
   */
  protected EfGridData getInit() {
    return init;
  }

  @Override
  public boolean isDefined(CtuluVariable var) {
    return init.isDefined(var);
  }

  @Override
  public boolean isElementVar(CtuluVariable idxVar) {
    return init.isElementVar(idxVar);
  }

}
