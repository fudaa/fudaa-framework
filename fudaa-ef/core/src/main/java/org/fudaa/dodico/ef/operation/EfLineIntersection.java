/*
 * @creation 25 oct. 06
 * @modification $Date: 2007-06-13 12:55:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import com.memoire.fu.FuLog;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

import java.io.IOException;
import java.util.Collection;

public abstract class EfLineIntersection implements EfLineIntersectionI {
  /**
   * @return the surrounding mesh used to find this intersection. -1 if not computed.
   */
  protected abstract int getSurroundingMesh();

  /**
   * Definie une intersection sur une arete.
   *
   * @author fred deniger
   * @version $Id: EfLineIntersection.java,v 1.5 2007-06-13 12:55:42 deniger Exp $
   */
  public final static class ItemEdge extends EfLineIntersection {
    int[] adjacentMesh_;
    final int i1_;
    final int i2_;
    final double x_;
    final double y_;
    private int surroundingMesh;

    public ItemEdge(final int _i1, final int _i2, final double _x, double _y) {
      super();
      i1_ = _i1;
      i2_ = _i2;
      x_ = _x;
      y_ = _y;
    }

    @Override
    public void fillWithAdjacentMeshes(TIntHashSet _idxMeshes) {
      buildAdjacentMeshes();
      if (adjacentMesh_[0] >= 0) {
        _idxMeshes.add(adjacentMesh_[0]);
      }
      if (adjacentMesh_[1] >= 0) {
        _idxMeshes.add(adjacentMesh_[1]);
      }
    }

    private double getD1() {
      return 1 / CtuluLibGeometrie.getDistance(x_, y_, parent_.getGrid().getPtX(i1_), parent_.getGrid().getPtY(i1_));
    }

    private double getD2() {
      return 1 / CtuluLibGeometrie.getDistance(x_, y_, parent_.getGrid().getPtX(i2_), parent_.getGrid().getPtY(i2_));
    }

    public int getI1() {
      return i1_;
    }

    public int getI2() {
      return i2_;
    }

    @Override
    public double getValue(CtuluVariable _var, int _tIdx) {
      if (parent_ == null) {
        throw new IllegalAccessError("pas de parent");
      }
      EfGridDataInterpolator data = parent_.getGridData();
      try {
        if (data.getData().isElementVar(_var)) {

          buildAdjacentMeshes();
          if (adjacentMesh_[1] < 0) {
            return data.getData().getData(_var, _tIdx, adjacentMesh_[0]);
          }
          return data.getMidValue(adjacentMesh_[0], adjacentMesh_[1], _var, _tIdx);
        }

        double d1 = getD1();
        double d2 = getD2();
        return data.getValue(i1_, i2_, d1 / (d1 + d2), d2 / (d1 + d2), _var, _tIdx);
      } catch (IllegalAccessError _evt) {
        FuLog.error(_evt);
      } catch (IOException _evt) {
        FuLog.error(_evt);
      }
      return 0;
    }

    private void buildAdjacentMeshes() {
      if (adjacentMesh_ == null) {
        parent_.buildNeighbor(null);
        adjacentMesh_ = parent_.getNeighbor().getAdjacentMeshes(i1_, i2_);
      }
    }

    @Override
    public double getValueBadInterpolation(EfData _d) {
      if (_d.isElementData()) {

        buildAdjacentMeshes();
        if (adjacentMesh_[1] < 0) {
          return _d.getValue(adjacentMesh_[0]);
        }
        return (_d.getValue(adjacentMesh_[0]) + _d.getValue(adjacentMesh_[1])) / 2D;
      }

      double d1 = getD1();
      double d2 = getD2();
      return (d1 * _d.getValue(i1_) + d2 * _d.getValue(i2_)) / (d1 + d2);
    }

    @Override
    public double getX() {
      return x_;
    }

    @Override
    public double getY() {
      return y_;
    }

    @Override
    public boolean isEdgeIntersection() {
      return true;
    }

    @Override
    public int isMeshIntersection() {
      return -1;
    }

    @Override
    public boolean isOnSameEfObject(EfLineIntersectionI _other) {
      if (_other == null) {
        return false;
      }
      if (_other.isEdgeIntersection()) {
        ItemEdge edge = (ItemEdge) _other;
        return (edge.i1_ == i1_ && edge.i2_ == i2_) || (edge.i1_ == i2_ && edge.i2_ == i1_);
      }
      return false;
    }

    /**
     * @return the surroundingMesh
     */
    @Override
    protected int getSurroundingMesh() {
      return surroundingMesh;
    }

    /**
     * @param surroundingMesh the surroundingMesh to set
     */
    protected void setSurroundingMesh(int surroundingMesh) {
      this.surroundingMesh = surroundingMesh;
    }
  }

  static class ItemInitial {
    int ielt_ = -1;
    EfLineIntersection item1_;
    EfLineIntersection item2_;
    EfLineIntersectionParent parent_;

    protected ItemInitial(EfLineIntersection.ItemInitial _data) {
      ielt_ = _data.ielt_;
      item1_ = _data.item1_;
      item2_ = _data.item2_;
      parent_ = _data.parent_;
      if (item1_ == null) {
        throw new IllegalAccessError("one item must be defined item1==null");
      }
    }

    protected ItemInitial(EfLineIntersectionParent _parent) {
      parent_ = _parent;
    }

    /**
     * Pour des r�sultats sur les �l�ments, il faut r�cup�rer les intersections concernant les noeuds, il faut
     * transformer 2 intersections sur des aretes par le point milieu. CETTE METHODE EST SPECIFIQUE AU CALCULATEUR DE
     * PROFIL.
     *
     * @param _dest la collection a remplir
     */
    public void addToMeshSortedResult(Collection<EfLineIntersection> _dest, EfGridInterface _grid) {
      // cela arrive si les points extr�mit�s du profil sont aussi des points du maillage: dans ce cas, on donne la
      // priorit�
      // au point issu du profil

      if (item2_ == null && item1_.isNodeIntersection() && !item1_.isAPointProfileIntersection()) {
        return;
      }

      if (item2_ != null && item2_.isAPointProfileIntersection()) {
        if (_dest.contains(item2_)) {
          _dest.remove(item2_);
        }
        _dest.add(item2_);
      }
      if (item1_ != null && item1_.isAPointProfileIntersection()) {
        if (_dest.contains(item1_)) {
          _dest.remove(item1_);
        }
        _dest.add(item1_);
      }
      // un seul point d'intersection: non int�ressant

      // une seule intersection
      if (item2_ == null) {
        _dest.add(item1_);
      }
      // une des 2 intersections est la juste pour garder une extremit�
      // on ajoute les 2
      else if (item1_!=null && (!item1_.isRealIntersection()) || !item2_.isRealIntersection()) {
        _dest.add(item1_);
        _dest.add(item2_);
        // cas particulier ou la ligne coupe l'�l�m�nts sur 2 noeuds
        // et que l'on n'a pas � faire � un point du profil
      } else if (item1_!=null && item1_.isNodeIntersection() && item2_.isNodeIntersection()) {
        EfLineIntersection.ItemNode n1 = (EfLineIntersection.ItemNode) item1_;
        EfLineIntersection.ItemNode n2 = (EfLineIntersection.ItemNode) item2_;
        EfElement elt = _grid.getElement(getEltIdx());
        // si c'est une arete on ajoute le point milieu
        if (elt.containsSegmentSensIndifferent(n1.ptIdx_, n2.ptIdx_)) {
          EfLineIntersection.ItemEdge edge = new EfLineIntersection.ItemEdge(n1.ptIdx_, n2.ptIdx_, getX(), getY());
          edge.setParent(item1_.parent_);
          _dest.add(edge);
        } else {
          _dest.add(new ItemMesh(this));
        }
      } else {
        _dest.add(new ItemMesh(this));
      }
    }

    public int getEltIdx() {
      return ielt_;
    }

    public EfLineIntersectionParent getParent() {
      return parent_;
    }

    /**
     * Renvoie le centre des 2 points d'intersection ou l'unique point.
     */
    public double getX() {
      if (!issetItem2()) {
        return item1_.getX();
      }
      return (item1_.getX() + item2_.getX()) / 2;
    }

    /**
     * Renvoie le centre des 2 points d'intersection ou l'unique point.
     */
    public double getY() {
      if (!issetItem2()) {
        return item1_.getY();
      }
      return (item1_.getY() + item2_.getY()) / 2;
    }

    boolean isOnePtUsed(int idx, int idx2) {
      return isPtUsed(idx) || isPtUsed(idx2);
    }

    public boolean isPtUsed(int idx) {
      return (item1_ != null && item1_.isNodeUsed(idx)) || (item2_ != null && item2_.isNodeUsed(idx));
    }

    public boolean isRealIntersection() {
      return ielt_ >= 0;
    }

    boolean issetItem1() {
      return item1_ != null;
    }

    boolean issetItem2() {
      return item2_ != null;
    }
  }

  /**
   * Une intersection definie sur un �l�ment: utilise pour Rubar et pour un point a interpoler.
   *
   * @author fred deniger
   * @version $Id: EfLineIntersection.java,v 1.5 2007-06-13 12:55:42 deniger Exp $
   */
  public static class ItemMesh extends EfLineIntersection {
    final int ielt_;
    final double x_;
    final double y_;

    public ItemMesh(final int _ielt, double _x, double _y) {
      super();
      ielt_ = _ielt;
      x_ = _x;
      y_ = _y;
    }

    @Override
    public void fillWithAdjacentMeshes(TIntHashSet _idxMeshes) {
      _idxMeshes.add(ielt_);
    }

    public ItemMesh(final ItemInitial _init) {
      super();
      ielt_ = _init.ielt_;
      parent_ = _init.getParent();
      x_ = _init.getX();
      y_ = _init.getY();
    }

    public int getIelt() {
      return ielt_;
    }

    @Override
    public double getValue(CtuluVariable _var, int _tIdx) {
      if (!isRealIntersection()) {
        throw new IllegalAccessError("non accessible !");
      }
      EfGridDataInterpolator data = parent_.getGridData();
      try {
        if (data.getData().isElementVar(_var)) {
          EfData dataCache = data.getCache(_var, _tIdx);
          if (dataCache == null) {
            dataCache = data.getData().getData(_var, _tIdx);
            data.saveCache(_var, _tIdx, dataCache);
          }
          return dataCache.getValue(ielt_);
        }

        return data.interpolate(ielt_, x_, y_, _var, _tIdx);
      } catch (IOException _evt) {
        FuLog.error(_evt);
      }
      return 0;
    }

    @Override
    public double getValueBadInterpolation(EfData _d) {
      if (!isRealIntersection()) {
        throw new IllegalAccessError("non accessible !");
      }
      if (_d.isElementData()) {
        return _d.getValue(ielt_);
      }
      return EfGridDataInterpolator.interpolateDangerous(ielt_, x_, y_, _d, parent_.getGrid());
    }

    @Override
    public double getX() {
      return x_;
    }

    @Override
    public double getY() {
      return y_;
    }

    @Override
    public int isMeshIntersection() {
      return ielt_;
    }

    @Override
    public boolean isOnSameEfObject(EfLineIntersectionI _other) {
      if (_other != null && !_other.isRealIntersection() && !_other.isNodeIntersection()) {
        return isMeshIntersection() == _other
            .isMeshIntersection();
      }
      return false;
    }

    @Override
    public boolean isRealIntersection() {
      return ielt_ >= 0;
    }

    @Override
    protected int getSurroundingMesh() {
      return ielt_;
    }
  }

  /**
   * Definie une intersection sur un noeud.
   *
   * @author fred deniger
   * @version $Id: EfLineIntersection.java,v 1.5 2007-06-13 12:55:42 deniger Exp $
   */
  public final static class ItemNode extends EfLineIntersection {
    final int ptIdx_;
    private int surroundingMesh;

    public ItemNode(final int _ptIdx) {
      super();
      ptIdx_ = _ptIdx;
    }

    @Override
    public double getValue(CtuluVariable _var, int _tIdx) {
      EfGridDataInterpolator data = parent_.getGridData();
      try {
        if (data.getData().isElementVar(_var)) {
          parent_.buildNeighbor(null);
          return parent_.getNeighbor().getAverageForNode(_var, ptIdx_, _tIdx, parent_.getGridData());
        }
        return data.getData().getData(_var, _tIdx, ptIdx_);
      } catch (IllegalAccessError _evt) {
        FuLog.error(_evt);
      } catch (IOException _evt) {
        FuLog.error(_evt);
      }
      return 0;
    }

    @Override
    public void fillWithAdjacentMeshes(TIntHashSet _idxMeshes) {
      // on construit au cas ou...
      getParent().buildNeighbor(null);
      getParent().getNeighbor().fillWithAdjacentMeshes(ptIdx_, _idxMeshes);
    }

    @Override
    public double getValueBadInterpolation(EfData _d) {
      if (_d.isElementData()) {
        parent_.buildNeighbor(null);
        return parent_.getNeighbor().getAverageForNodeDanger(ptIdx_, _d);
      }
      return _d.getValue(ptIdx_);
    }

    @Override
    public double getX() {
      return parent_.getGrid().getPtX(ptIdx_);
    }

    @Override
    public double getY() {
      return parent_.getGrid().getPtY(ptIdx_);
    }

    @Override
    public boolean isEdgeIntersection() {
      return false;
    }

    @Override
    public int isMeshIntersection() {
      return -1;
    }

    @Override
    public boolean isNodeIntersection() {
      return true;
    }

    @Override
    public boolean isNodeUsed(int _ptIdx) {
      return ptIdx_ == _ptIdx;
    }

    @Override
    public boolean isOnSameEfObject(EfLineIntersectionI _other) {
      if (_other == null) {
        return false;
      }
      if (_other.isNodeIntersection()) {
        return ((ItemNode) _other).ptIdx_ == ptIdx_;
      }
      return false;
    }

    @Override
    public String toString() {
      return getClass().getName() + " " + ptIdx_;
    }

    /**
     * @return the surroundingMesh
     */
    @Override
    protected int getSurroundingMesh() {
      return surroundingMesh;
    }

    /**
     * @param surroundingMesh the surroundingMesh to set
     */
    protected void setSurroundingMesh(int surroundingMesh) {
      this.surroundingMesh = surroundingMesh;
    }
  }

  /**
   * Pas d'intersection pour ce point.
   *
   * @author fred deniger
   * @version $Id: EfLineIntersection.java,v 1.5 2007-06-13 12:55:42 deniger Exp $
   */
  public final static class ItemNoIntersect extends EfLineIntersection {
    final double x_;
    final double y_;

    public ItemNoIntersect(final double _x, final double _y) {
      super();
      x_ = _x;
      y_ = _y;
    }

    @Override
    public void fillWithAdjacentMeshes(TIntHashSet _idxMeshes) {
      // ne renvoie rien

    }

    @Override
    public double getValue(CtuluVariable _var, int _tIdx) {
      throw new IllegalAccessError("ne doit jamais etre appel�");
    }

    @Override
    public double getValueBadInterpolation(EfData _d) {
      throw new IllegalAccessError("ne doit jamais etre appel�");
    }

    @Override
    public double getX() {
      return x_;
    }

    @Override
    public double getY() {
      return y_;
    }

    @Override
    public int isMeshIntersection() {
      return -1;
    }

    @Override
    public boolean isOnSameEfObject(EfLineIntersectionI _other) {
      return false;
    }

    @Override
    public boolean isRealIntersection() {
      return false;
    }

    @Override
    protected int getSurroundingMesh() {
      return -1;
    }
  }

  boolean extremiteIntersect_;
  EfLineIntersectionParent parent_;

  protected EfLineIntersection() {
  }

  /**
   * Remplit la set avec les indices de tous les �l�mnents adjacents. Si l'intersection est dans un �l�ment seul
   * l'indice de l'�l�ment est renvoye.
   *
   * @param _idxMeshes ne doit pas etre vide et sera rempli avec les indices de tous les �l�ments adjacents � cette
   *     intersection.
   */
  public abstract void fillWithAdjacentMeshes(TIntHashSet _idxMeshes);

  @Override
  public double getD2From(double _x, double _y) {
    return CtuluLibGeometrie.getD2(getX(), getY(), _x, _y);
  }

  @Override
  public double getDistanceFrom(double _x, double _y) {
    return CtuluLibGeometrie.getDistance(getX(), getY(), _x, _y);
  }

  public EfLineIntersectionParent getParent() {
    return parent_;
  }

  /**
   * @param _d les donn�es en accord avec le maillage (meme nombre de point)
   * @return la valeur non interpol�e correction
   */
  public abstract double getValueBadInterpolation(EfData _d);

  @Override
  public boolean isAPointProfileIntersection() {
    return extremiteIntersect_;
  }

  @Override
  public boolean isEdgeIntersection() {
    return false;
  }

  @Override
  public boolean isNodeIntersection() {
    return false;
  }

  @Override
  public boolean isNodeUsed(int _idx) {
    return false;
  }

  @Override
  public abstract boolean isOnSameEfObject(EfLineIntersectionI _other);

  @Override
  public boolean isRealIntersection() {
    return true;
  }

  public void setParent(EfLineIntersectionParent _parent) {
    parent_ = _parent;
  }
}
