/**
 * @creation 27 janv. 2004
 * @modification $Date: 2007-06-28 09:25:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISPrecision;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.resource.EfResource;

/**
 * Contient des routines de bases sur les maillages.
 *
 * @author deniger
 * @version $Id: EfLib.java,v 1.29 2007-06-28 09:25:05 deniger Exp $
 */
public final class EfLib {

  public static EfDataNode getBathy(final EfGridInterface _grid) {
    final double[] bathy = new double[_grid.getPtsNb()];
    for (int i = bathy.length - 1; i >= 0; i--) {
      bathy[i] = _grid.getPtZ(i);
    }
    return new EfDataNode(bathy);
  }

  public static EfDataElement getElementData(final CtuluVariable _var, final EfData _nodeData, final EfGridInterface _grid,
          InterpolationSupportValuesMultiI _datas, InterpolationVectorContainer _vects) {
    if (_nodeData == null || _grid == null) {
      return null;
    }
    if (_nodeData.isElementData()) {
      return (EfDataElement) _nodeData;
    }
    final double[] res = new double[_grid.getEltNb()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = _grid.getElement(i).getAverage(_var, _nodeData, _datas, _vects);
    }
    return new EfDataElement(res);
  }

  public static EfDataElement getElementDataDanger(final EfData _nodeData, final EfGridInterface _grid) {
    if (_nodeData == null || _grid == null) {
      return null;
    }
    if (_nodeData.isElementData()) {
      return (EfDataElement) _nodeData;
    }
    final double[] res = new double[_grid.getEltNb()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = _grid.getElement(i).getAverageDanger(_nodeData);
    }
    return new EfDataElement(res);
  }

  /**
   * @param _elt l'�l�ment a tester
   * @param _nodeData les valeurs aux noeuds
   * @param _eps l'erreur accept�e
   * @return true si toutes les valeurs aux sommets sont �gales � _eps pr�s.
   */
  public static boolean isPlat(final EfElement _elt, final EfData _nodeData, final double _eps) {
    final double refValues = _nodeData.getValue(_elt.getPtIndex(0));
    for (int i = _elt.getPtNb() - 1; i > 0; i--) {
      if (!CtuluLib.isZero(refValues - _nodeData.getValue(_elt.getPtIndex(i)), _eps)) {
        return false;
      }

    }
    return true;
  }

  /**
   * @param _g le maillage concerne
   * @param _inter la barre de progression
   * @param _analyze les messages de l'operation
   * @return un tableau representant les connexions pour chaque point du maillage. Independant du type de maillage
   */
  public static int[][] getConnexionFor(final EfGridInterface _g, final ProgressionInterface _inter, final CtuluAnalyze _analyze) {
    final int nbEle = _g.getEltNb();
    final ProgressionUpdater prog = new ProgressionUpdater(_inter);
    prog.setValue(3, nbEle, 0, 60);
    final TIntHashSet[] r = new TIntHashSet[_g.getPtsNb()];
    for (int i = nbEle - 1; i >= 0; i--) {
      final EfElement elt = _g.getElement(i);
      final int nbIdxMax = elt.getPtNb() - 1;
      for (int j = nbIdxMax; j >= 0; j--) {
        TIntHashSet t = r[elt.getPtIndex(j)];
        if (t == null) {
          t = new TIntHashSet(10);
          r[elt.getPtIndex(j)] = t;
        }
        t.add((j == nbIdxMax) ? elt.getPtIndex(0) : elt.getPtIndex(j + 1));
        t.add((j == 0) ? elt.getPtIndex(nbIdxMax) : elt.getPtIndex(j - 1));
      }
      prog.majAvancement();
    }
    prog.setValue(2, nbEle, 60, 40);
    prog.majProgessionStateOnly();
    final int[][] rf = new int[r.length][];
    for (int i = r.length - 1; i >= 0; i--) {
      if (r[i] == null) {
        _analyze.addError(DodicoLib.getS("Pas de connexions trouv�es pour le point {0}", CtuluLibString.getString(i)), i);
      } else {
        rf[i] = r[i].toArray();
      }
      prog.majAvancement();
    }
    return rf;
  }

  /**
   * @param _mid l'indice milieu
   * @return the frontier idx from the frontier middle idx
   */
  public static int getIdxFromIdxMid(final int _mid) {
    return (_mid << 1) + 1;
  }

  public static int getIdxGlobalForPt(int idxElt, double x, double y, GISPrecision prec, EfGridInterface grid) {
    EfElement elt = grid.getElement(idxElt);
    int nb = elt.getPtNb();
    for (int i = 0; i < nb; i++) {
      int idxGlobal = elt.getPtIndex(i);
      if (prec.isSame(x, y, grid.getPtX(idxGlobal), grid.getPtY(idxGlobal))) {
        return idxGlobal;
      }
    }
    return -1;

  }

  /**
   * @param _idx l'index global
   * @return the frontier middle idx from the frontier idx
   */
  public static int getIdxMidFromIdx(final int _idx) {
    return (_idx - 1) >> 1;
  }

  /**
   * @param _grid le maillage a parcourir
   * @param _trigo true si on veut recuperer les elements dans le sens trigo
   * @param _progress la barre de progression
   * @return les indices des elements orientes dans le sens donne.
   */
  public static int[] getIdxWithOrientation(final EfGridInterface _grid, final boolean _trigo, final ProgressionInterface _progress) {
    if (_grid == null) {
      return null;
    }
    final int nbElt = _grid.getEltNb();
    final TIntArrayList r = new TIntArrayList(nbElt);
    // Mise en place de la progression
    final ProgressionUpdater prog = new ProgressionUpdater(_progress);
    prog.setValue(4, nbElt);
    prog.majProgessionStateOnly(DodicoLib.getS("Recherche des �l�ments mal orient�s"));
    for (int i = nbElt - 1; i >= 0; i--) {
      if (_grid.getElement(i).isOrientedIn(_grid, _trigo)) {
        r.add(i);
      }
      prog.majAvancement();
    }
    return r.toNativeArray();
  }

  public static CtuluRange getMinMax(final EfData _d, final CtuluRange _v) {
    return getMinMax(_d, _v, null);
  }

  /**
   * @param _f le filtre
   * @param _grid le maillage
   * @return les �l�ments non s�lectionn�s. null si param�tres null ou si vide.
   */
  public static CtuluListSelectionInterface getNonSelectedElt(final EfFilter _f, final EfGridInterface _grid) {
    if (_f == null || _grid == null) {
      return null;
    }
    final int eltNb = _grid.getEltNb();
    final CtuluListSelection res = new CtuluListSelection(eltNb);
    for (int i = 0; i < eltNb; i++) {
      if (!_f.isActivatedElt(i)) {
        res.add(i);
      }
    }
    return res.isEmpty() ? null : res;
  }

  /**
   * @param _f le filtre
   * @param _grid le maillage
   * @return les �l�ments s�lectionn�s. CtuluListModelSelectionEmpty.EMPTY si param�tres null ou si vide.
   */
  public static CtuluListSelectionInterface getSelectedElt(final EfFilter _f, final EfGridInterface _grid) {
    if (_f == null || _grid == null) {
      return CtuluListModelSelectionEmpty.EMPTY;
    }
    final int eltNb = _grid.getEltNb();
    final CtuluListSelection res = new CtuluListSelection(eltNb);
    for (int i = 0; i < eltNb; i++) {
      if (_f.isActivatedElt(i)) {
        res.add(i);
      }
    }
    return res.isEmpty() ? CtuluListModelSelectionEmpty.EMPTY : res;
  }

  /**
   * @param _d les donnees a parcourir
   * @param _v le tableau contenant le min,max
   * @param _c une condition a respecter pour le min,max
   * @return le min max des donnees _v si non null
   */
  public static CtuluRange getMinMax(final EfData _d, final CtuluRange _v, final EfFilter _c) {
    if (_d == null || _d.getSize() == 0) {
      return null;
    }
    final CtuluRange r = (_v == null) ? new CtuluRange() : _v;
    r.setToNill();
    boolean isInit = false;
    double t;
    final boolean elt = _d.isElementData();
    for (int i = _d.getSize() - 1; i >= 0; i--) {
      if (_c == null || (!elt && _c.isActivated(i) || (elt && _c.isActivatedElt(i)))) {
        isInit = true;
        t = _d.getValue(i);
        r.expandTo(t);
        isInit = true;
      }
    }
    if (!isInit) {
      r.min_ = 0;
      r.max_ = 0;
    }
    return r;
  }

  /**
   * @param _nbPt le nombre de point
   * @return the nb of middle point from the nb of points
   */
  public static int getNbMiddlePt(final int _nbPt) {
    return _nbPt >> 1;
  }

  /**
   * @param _idx l'indice a tester
   * @param _minIdx la borne min du bord
   * @param _maxIdx la borne max de l'index du bord
   * @param _nbPt le nombre de point sur la frontiere
   * @return true si contenue
   */
  public static boolean isContained(final int _idx, final int _minIdx, final int _maxIdx, final int _nbPt) {
    if (_minIdx <= _maxIdx) {
      return _idx >= _minIdx && _idx <= _maxIdx;
    }
    return (_idx >= _minIdx && _idx < _nbPt) || (_idx >= 0 && _idx <= _maxIdx);

  }

  public static String isDiffQuick(final EfGridInterface _g1, final EfGridInterface _g2) {
    if (_g1 == null || _g2 == null) {
      return DodicoLib.getS("Donn�es invalides");
    }
    final int nbNodeRead = _g1.getPtsNb();
    if (nbNodeRead != _g2.getPtsNb()) {
      return DodicoLib.getS("Le nombre de noeuds est diff�rent");
    }
    final int nbEltRead = _g1.getEltNb();
    if (nbEltRead != _g2.getEltNb()) {
      return DodicoLib.getS("Le nombre d'�l�ments est diff�rent");
    }
    int testedNode = (int) (Math.random() * (nbNodeRead - 1));
    if (testedNode > nbNodeRead - 1) {
      testedNode = nbNodeRead - 1;
    }
    if (testedNode < 0) {
      testedNode = 0;
    }
    final Coordinate c = _g1.getCoor(testedNode);
    final Coordinate cReprise = _g2.getCoor(testedNode);
    if (CtuluLibGeometrie.getDistanceXY(c, cReprise) > 1E-1) {
      return DodicoLib.getS("Un noeud diff�re");
    }
    if (_g1.getFrontiers() != null && _g2.getFrontiers() != null && !_g1.getFrontiers().isSame(_g2.getFrontiers())) {
      return DodicoLib.getS("Les points fronti�res sont diff�rents");

    }
    return null;

  }

  /**
   * Oriente tous les elements du maillage dans le sens trigo ou horaire.
   *
   * @param _grid le maillage a modifier
   * @param _progress la barre de progression
   * @param _trigo si true, orientation dans le sens trigo. Sens horaire sinon
   * @param _analyze l'analyse de l'operation
   */
  public static void orienteGrid(final EfGridInterface _grid, final ProgressionInterface _progress, final boolean _trigo, final CtuluAnalyze _analyze) {
    final int nbElt = _grid.getEltNb();
    // Mise en place de la progression
    final ProgressionUpdater prog = new ProgressionUpdater(_progress);
    prog.setValue(4, nbElt);
    prog.majProgessionStateOnly(DodicoLib.getS("Orientation des �l�ments"));
    // le nombre d'�l�ments mal orient�s
    int nbEltToOriente = 0;
    TIntArrayList list = new TIntArrayList();
    for (int i = nbElt - 1; i >= 0; i--) {
      if (_grid.getElement(i).orienteDansSensTrigo(_grid, _trigo)) {
        nbEltToOriente++;
        list.add(i + 1);
      }
      prog.majAvancement();
    }
    if ((_analyze != null) && (nbEltToOriente > 0) && nbEltToOriente != _grid.getEltNb()) {
      list.sort();
      _analyze.addWarn(DodicoLib.getS("Nombre d''�l�ments mal orient�s: {0}", CtuluLibString.getString(nbEltToOriente)), 0);
      int[] elts = list.toNativeArray();
      String eltsAsString = CtuluLibString.arrayToString(elts, "; ");
      eltsAsString = StringUtils.abbreviate(eltsAsString, 100);
      _analyze.addWarn(EfResource.getS("Les �l�ments mal orient�s: {0}", eltsAsString), 0);
    }
  }

  private EfLib() {

  }

  /**
   * @param _grid le maillage a parcourir
   * @param _minAngleInDegre l'angle min
   * @param _progress la barre de progression
   * @return les indices des elements ayant un angle inferieur a <code>_minAngleInDegre</code>
   */
  public int[] getEltIdxWithAngleLessThan(final EfGridInterface _grid, final double _minAngleInDegre, final ProgressionInterface _progress) {
    return _grid.getEltIdxWithAngleLessThan(_minAngleInDegre, _progress);
  }

  /**
   * @param _v le maillage a comparer
   * @return true si le nb de points et tous les indices sont identiques
   */
  public static boolean isSameStructure(final EfGridVolumeInterface _v1, final EfGridVolumeInterface _v) {
    if (_v == null) {
      return false;
    }
    // on compare le nombre de points
    int nV = _v.getPtsNb();
    int n = _v1.getPtsNb();
    if (n != nV) {
      return false;
    }
    // on compare les aretes
    // EfSegment[] areteV = _v.aretes_;
    // on compare les nombres d'aretes
    nV = _v.getNbAretes();
    n = _v1.getNbAretes();
    if (n != nV) {
      return false;
    }
    // on compare les indices des aretes
    for (int i = n - 1; i >= 0; i--) {
      if (!_v1.getArete(i).isSameIdx(_v.getArete(i))) {
        return false;
      }
    }
    nV = _v.getEltNb();
    n = _v1.getEltNb();
    if (n != nV) {
      return false;
    }
    for (int i = n - 1; i >= 0; i--) {
      if (!_v1.getEltVolume(i).isSameStrict(_v.getEltVolume(i))) {
        return false;
      }
    }
    return true;
  }

}
