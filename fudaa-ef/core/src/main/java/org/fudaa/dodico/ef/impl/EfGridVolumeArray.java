/*
 * @creation 22 avr. 2005
 *
 * @modification $Date: 2007-01-19 13:07:20 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.dodico.ef.*;

/**
 * @author Fred Deniger
 * @version $Id: EfGridVolumeArray.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridVolumeArray extends EfGridArray implements EfGridVolumeInterface {
  EfSegment[] aretes_;

  public EfGridVolumeArray(final EfNode[] _pts, final EfElementVolume[] _elts, final EfSegment[] _aretes) {
    super(_pts, _elts);
    aretes_ = _aretes;
  }

  /**
   * @param _g
   */
  public EfGridVolumeArray(final EfGridVolume _g) {
    super(_g);
    aretes_ = _g.aretes_;
  }

  /**
   * @param _x le x a tester
   * @param _y le y a tester
   * @param _eltIdx l'element a parcourir
   * @param _m un point mutable temporaire peut etre nul
   * @return l'indice globale de l'arete de l'element _eltIdx la plus proche du point (_x,_y).
   */
  @Override
  public int getNearestEdgesInElement(final double _x, final double _y, final int _eltIdx, final Coordinate _m) {
    return getEltVolume(_eltIdx).getNearestEdge(_x, _y, this, _m);
  }

  /**
   * @param _v le maillage a comparer
   * @return true si le nb de points et tous les indices sont identiques
   */
  @Override
  public boolean isSameStructure(final EfGridVolumeInterface _v) {
    return EfLib.isSameStructure(this, _v);
  }

  protected void set(final EfGridVolumeArray _v) {
    // a continuer pour mettre a jour correctement les valeurs dans rubar et utiliser
    // les tableaux de double
    super.elts_ = _v.elts_;
    aretes_ = _v.aretes_;
    super.initPtFrom(_v);
    super.initEnvelopFrom(_v);
    super.ptsFrontiere_ = _v.ptsFrontiere_;
    clearIndex();
  }

  protected void set(final EfNode[] _pts, final EfElementVolume[] _elts, final EfSegment[] _aretes) {
    aretes_ = _aretes;
    super.initPtFrom(_pts);
    elts_ = _elts;
    geomXYChanged();
    ptsFrontiere_ = null;
  }

  protected void setPt(final EfGrid _g) {
    if (_g.getPtsNb() != getPtsNb()) {
      new Throwable().printStackTrace();
      return;
    }
    super.initPtFrom(_g);
    super.initEnvelopFrom(_g);
    if (ptsFrontiere_ != null) {
      ptsFrontiere_.clearSaveData();
    }
  }

  /**
   * @return le nombre d'arete
   */
  @Override
  public int getNbAretes() {
    return aretes_ == null ? 0 : aretes_.length;
  }

  /**
   * @param _idx l'indice de l'arete
   * @return l'arete d'indice _idx
   */
  @Override
  public EfSegment getArete(final int _idx) {
    return aretes_[_idx];
  }

  /**
   * @param _idx l'indice de l'elt
   * @return l'element "volume" correspondant
   */
  @Override
  public EfElementVolume getEltVolume(final int _idx) {
    return (EfElementVolume) getElement(_idx);
  }
}
