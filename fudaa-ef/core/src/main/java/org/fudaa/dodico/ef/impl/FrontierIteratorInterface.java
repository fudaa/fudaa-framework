/*
 * @creation 17 janv. 07
 * @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.dodico.commun.DodicoIntIterator;

/**
 * @author fred deniger
 * @version $Id: FrontierIteratorInterface.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public interface FrontierIteratorInterface extends DodicoIntIterator {

  /**
   * @return true si parcourt dans le sens inverse des indices
   */
  boolean isReverse();

  /**
   * @param _reverse true si on veut parcourir dans le sens inverse.
   */
  void setReverse(final boolean _reverse);

  /**
   * @return le nombre de point de cette frontiere
   */
  int getNbPoint();

  /**
   * @return le nombre de points parcourus.
   */
  int getNbPtParcouru();

  /**
   * @return l'indice de frontieres.
   */
  int getGlobalFrIdx();

  /**
   * @return l'indice global du dernier point itere
   */
  int getGlobalIdx();

}