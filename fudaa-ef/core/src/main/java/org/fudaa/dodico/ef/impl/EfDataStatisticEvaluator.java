/*
 * @creation 1 f�vr. 07
 * 
 * @modification $Date: 2007-06-11 13:04:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Permet calculer des stats (min,max, moy) sur un jeu de donn�e. Utilise un pool de thread pour faire les calculs.
 *
 * @author fred deniger
 * @version $Id: EfDataStatisticEvaluator.java,v 1.3 2007-06-11 13:04:05 deniger Exp $
 */
public abstract class EfDataStatisticEvaluator {

  final String statId_;
  final String statTitle_;

  public final static EfDataStatisticEvaluator findById(List _l, String _id) {
    if (CtuluLibArray.isEmpty(_l)) {
      return null;
    }
    for (int i = _l.size() - 1; i >= 0; i--) {
      EfDataStatisticEvaluator li = (EfDataStatisticEvaluator) _l.get(i);
      if (li.statId_.equals(_id)) {
        return li;
      }
    }
    return null;
  }

  public EfDataStatisticEvaluator(final String _statId, final String _statTitle) {
    super();
    statId_ = _statId;
    statTitle_ = _statTitle;
  }

  public int getCorrectTsBegin(final int _ts, final int _nbTimeStep) {
    return _ts < 0 || _ts >= _nbTimeStep ? 0 : _ts;
  }

  int getTsBegin(final int _tsBegin, final int _tsEnd, final int _nbTimeStep) {
    int deb = getCorrectTsBegin(_tsBegin, _nbTimeStep);
    int end = getCorrectTsEnd(_tsBegin, _nbTimeStep);
    return deb <= end ? deb : end;
  }

  int getTsEnd(final int _tsBegin, final int _tsEnd, final int _nbTimeStep) {
    int deb = getCorrectTsBegin(_tsEnd, _nbTimeStep);
    int end = getCorrectTsEnd(_tsEnd, _nbTimeStep);
    return end >= deb ? end : deb;
  }

  public int getCorrectTsEnd(final int _ts, final int _nbTimeStep) {
    return _ts < 0 || _ts >= _nbTimeStep ? _nbTimeStep - 1 : _ts;
  }

  public double[] compute(EfGridData _src, CtuluVariable _var, int _nbTimeStep, double[] _dest, ProgressionInterface _prog) throws IOException {
    return compute(_src, _var, -1, -1, _nbTimeStep, _dest, _prog);
  }

  /**
   * Propose une implementation par d�faut du calcul. le tableau de resultat (_dest) est initalis�e avec les valeurs au premier pas de temps. Ensuite,
   * le m�thode {@link #updateValues(double[], EfData, int)} est appele pour les autres pas de temps.
   *
   * @param _src la source des donn�es
   * @param _var la variable
   * @param _tsBegin l'indice du temps de d�but. Il est remis dans les clous: entre 0 et nbTimeStep -1 et il sera inf�rieur ou �gal ou _tsEnd
   * @param _tsEnd l'indice du temps de fin. Il est remis dans les clous: entre 0 et nbTimeStep -1 et il sera sup�rieur ou �gal ou _tsBegin
   * @param _nbTimeStepInSrc le nombre de pas de temps dans les sources.
   * @param _dest le tableau de valeurs. si null, cr�� automatiquement en fonction du nombre de valeur dans les EfData
   * @return le tableau des va
   * @throws IOException
   */
  public double[] compute(final EfGridData _src, final CtuluVariable _var, final int _tsBegin, final int _tsEnd, final int _nbTimeStepInSrc,
          final double[] _dest, ProgressionInterface _prog) throws IOException {
    double[] res = _dest;

    int end = getTsEnd(_tsBegin, _tsEnd, _nbTimeStepInSrc);
    int begin = getTsBegin(_tsBegin, _tsEnd, _nbTimeStepInSrc);
    ProgressionUpdater prog = new ProgressionUpdater(_prog);
    prog.setValue(10, end - begin + 1);
    EfData data = _src.getData(_var, begin);
    if (res == null || res.length != data.getSize()) {
      res = new double[data.getSize()];
    }
    for (int i = 0; i < res.length; i++) {
      res[i] = data.getValue(i);
    }
    // le premier pas de temps est fait
    prog.majAvancement();

    if (end - begin < 10) {
      for (int itime = begin + 1; itime <= end; itime++) {
        data = _src.getData(_var, itime);
        for (int iPt = 0; iPt < res.length; iPt++) {
          updateValues(res, data, iPt);
        }
        prog.majAvancement();
      }
    } else {
      List<EfDataCallable> callables = new ArrayList<EfDataCallable>();
      for (int itime = begin + 1; itime <= end; itime++) {
        callables.add(new EfDataCallable(_src, _var, itime));
      }
      ExecutorService threadPool = Executors.newFixedThreadPool(Math.max(2, Runtime.getRuntime().availableProcessors() / 2));
      CompletionService<EfData> ecs = new ExecutorCompletionService<EfData>(threadPool);
      for (EfDataCallable s : callables) {
        ecs.submit(s);
      }
      for (int itime = begin + 1; itime <= end; itime++) {
        EfData newData;
        try {
          newData = ecs.take().get();
          if (newData != null) {
            for (int iPt = 0; iPt < res.length; iPt++) {
              updateValues(res, newData, iPt);
            }
          } else {
            FuLog.error("data is null");
          }
        } catch (Exception e) {
          FuLog.error(e);
        }

      }
      threadPool.shutdown();

    }
    finishCreation(res, begin, end);
    return res;
  }

  @Override
  public String toString() {
    return statTitle_;
  }

  /**
   * Appele � la fin de la m�thode compute. Permet de modifier les donn�es pour calculer une moyenne par exemple.
   *
   * @param _res le r�sultat renvoy�
   * @param _tsBegin l'indice du temps de d�but corrig�
   * @param _tsEnd l'indice du temps de fin corrig�
   */
  protected void finishCreation(double[] _res, int _tsBegin, int _tsEnd) {
  }

  /**
   * @param _res _res le tableau contenant les r�sultat
   * @param _datas le valeurs au pas de temps
   * @param _iPt l'indice du point dans
   */
  protected abstract void updateValues(double[] _res, EfData _data, int _iPt);

  public static class Max extends EfDataStatisticEvaluator {

    public Max() {
      super("MAX", DodicoLib.getS("Maximum"));
    }

    @Override
    protected void updateValues(double[] _res, EfData _data, int _iPt) {
      if (_data.getValue(_iPt) > _res[_iPt]) {
        _res[_iPt] = _data.getValue(_iPt);
      }
    }
  }

  public static class Min extends EfDataStatisticEvaluator {

    public Min() {
      super("MIN", DodicoLib.getS("Minimum"));
    }

    @Override
    protected void updateValues(double[] _res, EfData _data, int _iPt) {
      if (_data.getValue(_iPt) < _res[_iPt]) {
        _res[_iPt] = _data.getValue(_iPt);
      }
    }
  }

  public static class Moyenne extends EfDataStatisticEvaluator {

    public Moyenne() {
      super("AVERAGE", DodicoLib.getS("Moyenne"));
    }

    @Override
    protected void updateValues(double[] _res, EfData _data, int _iPt) {
      _res[_iPt] = _res[_iPt] + _data.getValue(_iPt);
    }

    @Override
    protected void finishCreation(double[] _res, int _tsBegin, int _tsEnd) {
      // on le transforme en double pour un calcul en double ...
      double nb = _tsEnd - _tsBegin + 1;
      for (int i = 0; i < _res.length; i++) {
        _res[i] = _res[i] / nb;
      }

    }
  }
  
  /**
   * Operator SUM
   * @author Adrien
   *
   */
  public static class Sum extends EfDataStatisticEvaluator {

	    public Sum() {
	      super("SUM", DodicoLib.getS("Somme"));
	    }

	    @Override
	    protected void updateValues(double[] _res, EfData _data, int _iPt) {
	     
	    	 _res[_iPt] =  _res[_iPt] +  _data.getValue(_iPt);
	    	
	    }
	  }

  public String getStatId() {
    return statId_;
  }

  public String getStatTitle() {
    return statTitle_;
  }
}
