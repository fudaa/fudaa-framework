/**
 *  @creation     17 d�c. 2004
 *  @modification $Date: 2007-01-10 09:04:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.collection.CtuluArrayDoubleImmutable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;


/**
 * @author Fred Deniger
 * @version $Id: EfDataElement.java,v 1.3 2007-01-10 09:04:17 deniger Exp $
 */
public class EfDataElement extends CtuluArrayDoubleImmutable implements EfData{

  /**
   * @param _init
   */
  public EfDataElement(final CtuluArrayDoubleImmutable _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public EfDataElement(final CtuluCollectionDouble _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public EfDataElement(final double[] _init) {
    super(_init);
  }

  /**
   * @param _nb
   */
  public EfDataElement(final int _nb) {
    super(_nb);
  }

  /**
   * @param _init
   */
  public EfDataElement(final Object[] _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public EfDataElement(final TDoubleArrayList _init) {
    super(_init);
  }

  @Override
  public final boolean isElementData(){
    return true;
  }



}
