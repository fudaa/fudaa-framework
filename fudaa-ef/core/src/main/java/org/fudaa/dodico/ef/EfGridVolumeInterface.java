/*
 *  @creation     22 avr. 2005
 *  @modification $Date: 2006-04-11 08:06:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;

/**
 * @author Fred Deniger
 * @version $Id: EfGridVolumeInterface.java,v 1.5 2006-04-11 08:06:16 deniger Exp $
 */
public interface EfGridVolumeInterface extends EfGridInterface {

  int getNearestEdgesInElement(double _x, double _y, int _eltIdx, Coordinate _m);

  int getNbAretes();

  EfSegment getArete(int _idx);

  EfElementVolume getEltVolume(int _idx);

  boolean isSameStructure(EfGridVolumeInterface _v);

}
