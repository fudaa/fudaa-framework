package org.fudaa.dodico.ef;

import com.memoire.fu.FuSortint;
import com.memoire.fu.FuSortint.Comparator;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluListSelection;

// TODO Trouver un autre nom (EfFilterValidator).
// TODO Pour l'instant le filtre contient les �l�ments a supprimer. G�rer les filtre qui contiennent les �l�ment �
// garder.
public class EfRemoveFilter {

  private class ElementsIdxComparator implements Comparator {

    private EfGridInterface grid;
    private EfFrontierInterface frontiers;

    public ElementsIdxComparator(EfGridInterface grid) {
      this.grid = grid;
      this.frontiers = this.grid.getFrontiers();
    }

    @Override
    public int compare(int a, int b) {
      if (this.isFrontierElt(a)) {
        if (this.isFrontierElt(b)) {
          return this.compareFrontierIdx(a, b);
        } else {
          return -1;
        }
      } else if (this.frontiers.isFrontierPoint(b)) {
        return 1;
      } else {
        return this.compareValue(a, b);
      }
    }

    private boolean isFrontierElt(int eltIdx) {
      int[] idxs = this.grid.getElement(eltIdx).getIndices();

      for (int i = 0; i < idxs.length; i++) {
        if (this.frontiers.isFrontierPoint(idxs[i])) {
          return true;
        }
      }

      return false;
    }

    private int compareFrontierIdx(int a, int b) {
      int frtA = this.getMaxFrontierIdx(a);
      int frtB = this.getMaxFrontierIdx(b);

      int result = this.compareValue(frtA, frtB);

      if (result == 0) {
        result = this.compareValue(a, b);
      }

      return result;
    }

    private int getMaxFrontierIdx(int eltIdx) {
      int[] idxs = this.grid.getElement(eltIdx).getIndices();
      int max = 0;

      for (int i = 0; i < idxs.length; i++) {
        if (this.frontiers.isFrontierPoint(idxs[i])) {
          max = Math.max(max, this.frontiers.getFrontiereIndice(idxs[i]));
        }
      }

      return max;
    }

    private int compareValue(int a, int b) {
      if (a < b) {
        return -1;
      } else if (a > b) {
        return 1;
      }

      return 0;
    }
  }
  private EfFilter filter;
  private EfGridInterface grid;
  private TIntArrayList eltsToRemove;
  private TIntArrayList selectedElts;
  private TIntHashSet selectedEltsSet;
  private TIntHashSet eltsInProcess;
  private boolean isSelectFilter;
  private boolean filterModified;

  public EfRemoveFilter() {
    this.isSelectFilter = true;
  }

  public EfFilter process() {
    this.eltsToRemove = new TIntArrayList();
    this.selectedElts = new TIntArrayList();
    this.eltsInProcess = new TIntHashSet();

    this.extractEltsToRemove();

    if (this.eltsToRemove.size() == 0) {
      this.filterModified = false;

      return this.filter;
    }

    this.grid.computeBord(null, null);

    EfFrontierInterface frontiers = this.grid.getFrontiers();
    EfNeighborMesh neighborMesh = EfNeighborMesh.compute(this.grid, null);

    this.sortEltsToRemove();
    TIntIntHashMap frontierIndiceByGlobalIdx = buildFrontierIndiceByGlobalIdx(frontiers);
    for (int i = 0; i < this.eltsToRemove.size(); i++) {
      this.selectElement(this.eltsToRemove.get(i), frontiers, neighborMesh, frontierIndiceByGlobalIdx);
    }

    this.filterModified = (this.eltsToRemove.size() != this.selectedElts.size());

    return new EfFilterSelectedElement(this.getFilterSelectedElts(), this.grid);
  }

  public void setFilter(EfFilter filter) {
    this.filter = filter;
  }

  public int getNbElementNotRemoved() {
    return this.eltsToRemove.size() - this.selectedElts.size();
  }

  public void setGrid(EfGridInterface grid) {
    this.grid = grid;
  }

  public boolean getFilterModified() {
    return this.filterModified;
  }

  /**
   * @param isSelectFilter true if the Filter contains the elements that will be use in the created grid.
   */
  public void setSelectFilter(boolean isSelectFilter) {
    this.isSelectFilter = isSelectFilter;
  }

  private boolean selectElement(int eltIdx, EfFrontierInterface frontiers, EfNeighborMesh neighborMesh,
                                TIntIntHashMap frontierIndiceByGlobalIdx) {
    if (this.isSelectedElement(eltIdx)) {
      return true;
    }

    int[] eltPtIdxs = this.grid.getElement(eltIdx).getIndices();
    TIntArrayList problematicPts = this.getProblematicPtsIdx(eltPtIdxs, frontiers, neighborMesh, frontierIndiceByGlobalIdx);
    boolean selectElement = true;

    this.eltsInProcess.add(eltIdx);

    for (int i = 0; i < problematicPts.size(); i++) {
      int ptIdx = problematicPts.get(i);
      int[] adjacentPtsIdx = this.getAdjacentPtsIdx(eltPtIdxs, ptIdx);
      int neighborElts[] = new int[]{neighborMesh.getAdjacentMeshes(ptIdx, adjacentPtsIdx[0], eltIdx),
                                     neighborMesh.getAdjacentMeshes(ptIdx, adjacentPtsIdx[1], eltIdx)};

      selectElement = false;

      for (int j = 0; j < neighborElts.length; j++) {
        if ((neighborElts[j] != -1) && (this.eltsToRemove.contains(neighborElts[j]))) {
          if (this.selectElement(neighborElts[j], frontiers, neighborMesh, frontierIndiceByGlobalIdx)) {
            selectElement = true;

            break;
          }
        }
      }

      if (!selectElement) {
        break;
      }
    }

    if (selectElement) {
      this.selectedElts.add(eltIdx);
    }

    this.eltsInProcess.remove(eltIdx);

    return selectElement;
  }

  private int[] getAdjacentPtsIdx(int[] eltPtIdxs, int ptIdx) {
    int nbPts = eltPtIdxs.length;

    for (int i = 0; i < eltPtIdxs.length; i++) {
      if (eltPtIdxs[i] == ptIdx) {
        return new int[]{eltPtIdxs[this.getPreviousIdx(nbPts, i)],
                         eltPtIdxs[this.getNextIdx(nbPts, i)]};
      }
    }

    return null;
  }

  private TIntArrayList getProblematicPtsIdx(int[] eltPtIdxs, EfFrontierInterface frontiers, EfNeighborMesh neighborMesh,
                                             TIntIntHashMap frontierIndiceByGlobalIdx) {
    TIntArrayList problematicPtsIdx = new TIntArrayList();
    int nbPts = eltPtIdxs.length;
    for (int i = 0; i < nbPts; i++) {
      int previousIdx = this.getPreviousIdx(nbPts, i);
      int nextIdx = this.getNextIdx(nbPts, i);

      if (this.isFrontierProblematicPt(eltPtIdxs[i], eltPtIdxs[previousIdx], eltPtIdxs[nextIdx], frontiers,
                                       frontierIndiceByGlobalIdx)) {
        problematicPtsIdx.add(eltPtIdxs[i]);

        continue;
      }

      if (this.isSelectedEltsProblematicPt(eltPtIdxs[i], eltPtIdxs[previousIdx], eltPtIdxs[nextIdx], neighborMesh)) {
        problematicPtsIdx.add(eltPtIdxs[i]);

        continue;
      }
    }

    return problematicPtsIdx;
  }

  private TIntIntHashMap buildFrontierIndiceByGlobalIdx(EfFrontierInterface frontiers) {
    TIntIntHashMap frontierIndiceByGlobalIdx = new TIntIntHashMap();
    AllFrontierIteratorInterface allFrontierIterator = frontiers.getAllFrontierIterator();
    while (allFrontierIterator.hasNext()) {
      int globalIdx = allFrontierIterator.next();
      frontierIndiceByGlobalIdx.put(globalIdx, allFrontierIterator.getBordIDx());
    }
    return frontierIndiceByGlobalIdx;
  }

  private boolean isFrontierProblematicPt(int ptIdx, int previousPtIdx, int nextPtIdx, EfFrontierInterface frontiers,
                                          TIntIntHashMap frontierIndiceByGlobalIdx) {
    if (frontiers.isFrontierPoint(ptIdx)) {
      int frtIdx = frontierIndiceByGlobalIdx.get(ptIdx);

      if (frontiers.isFrontierPoint(previousPtIdx)) {
        if (frtIdx == frontierIndiceByGlobalIdx.get(previousPtIdx)) {
          return false;
        }
      }

      if (frontiers.isFrontierPoint(nextPtIdx)) {
        if (frtIdx == frontierIndiceByGlobalIdx.get(nextPtIdx)) {
          return false;
        }
      }

      return true;
    }

    return false;
  }

  private boolean isSelectedEltsProblematicPt(int ptIdx, int previousPtIdx, int nextPtIdx, EfNeighborMesh neighbor) {
    int nbNeighborMeshes = neighbor.getNbNeighborMeshes(ptIdx);
    if (selectedEltsSet == null) {
      selectedEltsSet = new TIntHashSet();
      selectedEltsSet.addAll(selectedElts.toNativeArray());
    }
    for (int i = 0; i < nbNeighborMeshes; i++) {
      int idxElt = neighbor.getNeighborMesh(ptIdx, i);
      if (selectedEltsSet.contains(idxElt)) {
        EfElement element = this.grid.getElement(idxElt);
        if ((!element.containsIndex(previousPtIdx)) && (!element.containsIndex(nextPtIdx))) {
          return true;
        }
      }
    }

    return false;
  }

  private void extractEltsToRemove() {
    for (int i = 0; i < this.grid.getEltNb(); i++) {
      if (this.filter.isActivatedElt(i) != this.isSelectFilter) {
        this.eltsToRemove.add(i);
      }
    }
  }

  private void sortEltsToRemove() {
    int[] eltsToRemove = this.eltsToRemove.toNativeArray();

    FuSortint.sort(eltsToRemove, new ElementsIdxComparator(this.grid));

    this.eltsToRemove = new TIntArrayList(eltsToRemove);
  }

  private CtuluListSelection getFilterSelectedElts() {
    CtuluListSelection list = new CtuluListSelection();

    for (int i = 0; i < this.grid.getEltNb(); i++) {
      if (this.selectedElts.contains(i) != this.isSelectFilter) {
        list.add(i);
      }
    }

    return list;
  }

  private boolean isSelectedElement(int elementIdx) {
    return this.selectedElts.contains(elementIdx) || this.eltsInProcess.contains(elementIdx);
  }

  private int getPreviousIdx(int nbPts, int idx) {
    if (idx == 0) {
      return nbPts - 1;
    } else {
      return idx - 1;
    }
  }

  private int getNextIdx(int nbPts, int idx) {
    if (idx == (nbPts - 1)) {
      return 0;
    } else {
      return idx + 1;
    }
  }
  /*
   * public EfFilter process() { this.filterElements = new CtuluListSelection(); this.selectableElements = new
   * CtuluListSelection(); this.degenerateElements = new TIntObjectHashMap<TIntArrayList>();
   *
   * EfFrontierInterface frontiers = this.grid.getFrontiers(); EfNeighborMesh neighborMesh = EfNeighborMesh.compute(this.grid,
   * null);
   *
   * // Selection des �l�ments et traitment des �l�ments fronti�res for (int i = 0; i < this.grid.getEltNb(); i++) { if
   * (this.filter.isActivatedElt(i)) { EfElement element = this.grid.getElement(i); TIntArrayList[] frontiersPts = new
   * TIntArrayList[frontiers.getNbFrontier()];
   *
   * if (this.hasFrontierPts(element, frontiers, frontiersPts)) { TIntArrayList problematicPts = this.getProblematicPts(element,
   * frontiers, frontiersPts);
   *
   * if (problematicPts.isEmpty()) { this.selectableElements.add(i); } else { this.degenerateElements.put(i, problematicPts); } }
   * else { this.filterElements.add(i); } } }
   *
   * //Taitement des �l�ments d�g�n�r�s. TIntObjectIterator<TIntArrayList> iterator = this.degenerateElements.iterator();
   *
   * while (iterator.hasNext()) { if (!this.isSelectedElement(iterator.key())) {
   *
   * } }
   *
   * this.degenerateElements.clear();
   *
   * return new EfFilterSelectedElement(this.selectableElements, this.grid); }
   *
   * private boolean isSelectedElement(int elementIdx) { return this.selectableElements.isSelected(elementIdx) ||
   * this.elementsInProcess.isSelected(elementIdx); }
   *
   * private boolean hasFrontierPts(EfElement element, EfFrontierInterface frontiers, TIntArrayList[] frontiersPts) { int[] idxs =
   * element.getIndices(); boolean hasFrtPts = false;
   *
   * for (int i = 0; i < idxs.length; i++) { if (frontiers.isFrontierPoint(idxs[i])) { int frtIdx =
   * frontiers.getFrontiereIndice(idxs[i]);
   *
   * if (frontiersPts[frtIdx] == null) { frontiersPts[frtIdx] = new TIntArrayList(); }
   *
   * frontiersPts[frtIdx].add(idxs[i]);
   *
   * hasFrtPts = true; } }
   *
   * return hasFrtPts; }
   *
   * private TIntArrayList getProblematicPts(EfElement element, EfFrontierInterface frontiers, TIntArrayList[] frontiersPts) {
   * int[] idxs = element.getIndices(); int nbPts = idxs.length; TIntArrayList problematicPts = new TIntArrayList();
   *
   * for (int i = 0; i < frontiersPts.length; i++) { if (frontiersPts[i] != null) { // Jamais �gal � 0 (si �gal 0, frontiersPts[i]
   * �gal null, voir hasFrontierPts) if (frontiersPts[i].size() == 1) { problematicPts.add(frontiersPts[i].get(0));
   *
   * continue; }
   *
   * for (int j = 0; j < nbPts; j++) { if (frontiersPts[i].contains(idxs[j])) { if
   * ((!frontiersPts[i].contains(idxs[this.getPreviousIdx(nbPts, j)])) && (!frontiersPts[i].contains(idxs[this.getNextIdx(nbPts,
   * j)]))) { problematicPts.add(idxs[j]); } } } } }
   *
   * return problematicPts; }
   *
   * private boolean hasSelectedNeighbor(int elementIdx, TIntArrayList[] frontiersPts, EfNeighborMesh neighborMesh) { int[]
   * neighborIdxs = neighborMesh.getAdjacentMeshesFromElt(this.grid, elementIdx); EfElement[] neighbors = new
   * EfElement[neighborIdxs.length];
   *
   * for (int i = 0; i < neighborIdxs.length; i++) { neighbors[i] = this.grid.getElement(neighborIdxs[i]); }
   *
   * for (int i = 0; i < frontiersPts.length; i++) { if (frontiersPts[i] != null) { for (int j = 0; j < frontiersPts[i].size();
   * j++) { boolean hasSelectedNeighbor = false;
   *
   * for (int k = 0; k < neighbors.length; k++) { if (neighbors[k].containsIndex(frontiersPts[i].get(j))) { if
   * (this.selectElement(neighborIdxs[k], frontiers, neighborMesh)) { hasSelectedNeighbor = true;
   *
   * break; } } }
   *
   * if (!hasSelectedNeighbor) { return false; } } } }
   *
   * return false; }
   *
   * private int getPreviousIdx(int nbPts, int idx) { if (idx == 0) { return nbPts - 1; } else { return idx - 1; } }
   *
   * private int getNextIdx(int nbPts, int idx) { if (idx == (nbPts - 1)) { return 0; } else { return idx + 1; } } /* public
   * EfFilter process() { this.selectableElements = new CtuluListSelection(); this.unselectableElements = new
   * CtuluListSelection();
   *
   * EfFrontierInterface frontiers = this.grid.getFrontiers(); EfNeighborMesh neighborMesh = EfNeighborMesh.compute(this.grid,
   * null);
   *
   * for (int i = 0; i < this.grid.getEltNb(); i++) { this.selectElement(i, frontiers, neighborMesh); }
   *
   * return new EfFilterSelectedElement(this.selectableElements, this.grid); }
   *
   * private boolean selectElement(int elementIdx, EfFrontierInterface frontiers, EfNeighborMesh neighborMesh) { if
   * ((!this.filter.isActivatedElt(elementIdx)) || (this.unselectableElements.isSelected(elementIdx))) { return false; }
   *
   * if (this.selectableElements.isSelected(elementIdx)) { return true; }
   *
   * EfElement element = this.grid.getElement(elementIdx); TIntArrayList[] frontiersPts = new
   * TIntArrayList[frontiers.getNbFrontier()]; boolean canSelectElt = !this.hasFrontierPts(element, frontiers, frontiersPts);
   *
   * if (!canSelectElt) { canSelectElt = this.hasOnlyFrontierSegment(element, frontiers, frontiersPts); }
   *
   * if (!canSelectElt) { // On le met dans cette liste pour ne pas qu'il soit retest� par ses voisins (risque de StackOverflow).
   * this.unselectableElements.add(elementIdx);
   *
   * canSelectElt = this.hasSelectedNeighbor(elementIdx, frontiers, frontiersPts, neighborMesh); }
   *
   * if (canSelectElt) { this.unselectableElements.remove(elementIdx); this.selectableElements.add(elementIdx); } else {
   * this.unselectableElements.add(elementIdx); }
   *
   * return canSelectElt; }
   *
   * private boolean hasOnlyFrontierSegment(EfElement element, EfFrontierInterface frontiers, TIntArrayList[] frontiersPts) {
   * int[] idxs = element.getIndices(); int nbPts = idxs.length; boolean hasOnlyFrtSeg = true;
   *
   * for (int i = 0; i < frontiersPts.length; i++) { if (frontiersPts[i] != null) { if (frontiersPts[i].size() < 2) { return
   * false; }
   *
   * TIntArrayList ptsToRemove = new TIntArrayList();
   *
   * for (int j = 0; j < nbPts; j++) { if (frontiersPts[i].contains(idxs[j])) { if
   * ((!frontiersPts[i].contains(idxs[this.getPreviousIdx(nbPts, j)])) && (!frontiersPts[i].contains(idxs[this.getNextIdx(nbPts,
   * j)]))) { hasOnlyFrtSeg = false; } else { ptsToRemove.add(idxs[j]); } } }
   *
   * // On enl�ve le points faisant partie de segment fronti�re ppour garder ceux qui pose probl�me. for (int j = 0; j <
   * ptsToRemove.size(); j++) { frontiersPts[i].remove(frontiersPts[i].indexOf(ptsToRemove.get(j))); }
   *
   * if (frontiersPts[i].size() == 0) { frontiersPts[i] = null; } } }
   *
   * return hasOnlyFrtSeg; }
   *
   * private boolean hasSelectedNeighbor(int elementIdx, EfFrontierInterface frontiers, TIntArrayList[] frontiersPts,
   * EfNeighborMesh neighborMesh) { int[] neighborIdxs = neighborMesh.getAdjacentMeshesFromElt(this.grid, elementIdx); EfElement[]
   * neighbors = new EfElement[neighborIdxs.length];
   *
   * for (int i = 0; i < neighborIdxs.length; i++) { neighbors[i] = this.grid.getElement(neighborIdxs[i]); }
   *
   * for (int i = 0; i < frontiersPts.length; i++) { if (frontiersPts[i] != null) { for (int j = 0; j < frontiersPts[i].size();
   * j++) { boolean hasSelectedNeighbor = false;
   *
   * for (int k = 0; k < neighbors.length; k++) { if (neighbors[k].containsIndex(frontiersPts[i].get(j))) { if
   * (this.selectElement(neighborIdxs[k], frontiers, neighborMesh)) { hasSelectedNeighbor = true;
   *
   * break; } } }
   *
   * if (!hasSelectedNeighbor) { return false; } } } }
   *
   * return false; }
   *
   * private int getPreviousIdx(int nbPts, int idx) { if (idx == 0) { return nbPts - 1; } else { return idx - 1; } }
   *
   * private int getNextIdx(int nbPts, int idx) { if (idx == (nbPts - 1)) { return 0; } else { return idx + 1; } }
   */
}
