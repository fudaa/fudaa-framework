/*
 *  @creation     19 d�c. 2005
 *  @modification $Date: 2007-05-04 13:45:58 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluDoubleVisitor;

/**
 * @author Fred Deniger
 */
public class EfDataAdapter implements EfData {

  final boolean isElement_;
  final CtuluCollectionDouble model_;

  /**
   * @param _model
   */
  public EfDataAdapter(final CtuluCollectionDouble _model, final boolean _isElement) {
    super();
    model_ = _model;
    isElement_ = _isElement;
  }

  @Override
  public void expandTo(final CtuluRange _rangeToExpand) {
    model_.expandTo(_rangeToExpand);
  }

  @Override
  public void getRange(final CtuluRange _r) {
    model_.getRange(_r);

  }

  @Override
  public Double getCommonValue(final int[] _i) {
    return model_.getCommonValue(_i);
  }

  @Override
  public double getMax() {
    return model_.getMax();
  }

  @Override
  public double getMin() {
    return model_.getMin();
  }

  @Override
  public int getSize() {
    return model_.getSize();
  }

  @Override
  public double getValue(final int _i) {
    return model_ == null ? 0 : model_.getValue(_i);
  }

  @Override
  public Object getObjectValueAt(final int _i) {
    return CtuluLib.getDouble(getValue(_i));
  }

  @Override
  public double[] getValues() {
    return model_.getValues();
  }

  @Override
  public boolean isElementData() {
    return isElement_;
  }

  @Override
  public void iterate(final CtuluDoubleVisitor _visitor) {
    model_.iterate(_visitor);
  }

}
