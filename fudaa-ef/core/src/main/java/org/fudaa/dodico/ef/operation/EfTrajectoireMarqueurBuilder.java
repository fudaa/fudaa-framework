/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 */
public abstract class EfTrajectoireMarqueurBuilder {

  public static class Geo extends EfTrajectoireMarqueurBuilder {
    double deltaEnCours_;

    Geo(EfGridInterface _grid, double _maxDelta) {
      super(_grid);
      maxDelta = _maxDelta;
    }

    @Override
    public void preCoordinateAdded(Coordinate _c, double _t) {
      Coordinate lastCoordinate = getDest().getLastCoordinate();
      double distParcouru = CtuluLibGeometrie.getDistanceXY(_c, lastCoordinate);
      double oldDelta = deltaEnCours_;
      deltaEnCours_ = deltaEnCours_ + distParcouru;
      if (deltaEnCours_ > maxDelta) {
        double distanceAParcourir = maxDelta - oldDelta;
        double tPrec = getDest().getLastTime();
        double coeff = distanceAParcourir / distanceAParcourir;
        double x = lastCoordinate.x + coeff * (_c.x - lastCoordinate.x);
        double y = lastCoordinate.y + coeff * (_c.y - lastCoordinate.y);
        double t = tPrec + coeff * (_t - tPrec);
        deltaEnCours_ = 0;
        getDest().addMarqueurIntersection(getIntersection(x, y), t);
        // on relance l'operation au cas ou il y ait plusieurs marqueurs et pour recalculer le deltaEncours
        preCoordinateAdded(_c, _t);
      }
    }
  }

  public static class Time extends EfTrajectoireMarqueurBuilder {
    double lastMarqueurTime_;

    Time(EfGridInterface _grid, double _startTime, double _maxDelta) {
      super(_grid);
      maxDelta = _maxDelta;
      lastMarqueurTime_ = _startTime;
    }

    @Override
    public void preCoordinateAdded(Coordinate _c, double _t) {
      boolean neg = _t < lastMarqueurTime_;
      double delta = Math.abs(_t - lastMarqueurTime_);
      if (delta > maxDelta) {
        double time = neg ? lastMarqueurTime_ - maxDelta : lastMarqueurTime_ + maxDelta;
        double tPrec = getDest().getLastTime();
        Coordinate coordPrec = getDest().getLastCoordinate();
        double coeff = (time - tPrec) / (_t - tPrec);
        double x = coordPrec.x + coeff * (_c.x - coordPrec.x);
        double y = coordPrec.y + coeff * (_c.y - coordPrec.y);
        getDest().addMarqueurIntersection(getIntersection(x, y), time);
        lastMarqueurTime_ = time;
        // on relance l'operation au cas ou il y ait plusieurs marqueurs
        preCoordinateAdded(_c, _t);
      }
    }
  }

  public static EfTrajectoireMarqueurBuilder createMarqueurBuilder(EfTrajectoireResultBuilder _item,
      EfTrajectoireParameters _data) {
    if (_data == null || _data.marqueur_ == null) return null;
    EfTrajectoireMarqueurBuilder res = null;
    if (_data.marqueur_.timeStep_) {
      res = new Time(_item.gridDataProvider_.getGrid(), _data.firstTimeStep_, _data.marqueur_.deltaMax_);
    } else res = new Geo(_item.gridDataProvider_.getGrid(), _data.marqueur_.deltaMax_);
    res.dest_ = _item;
    return res;
  }

  private EfTrajectoireResultBuilder dest_;

  private final EfLineSingleIntersectFinder finder;
  double maxDelta;

  private EfTrajectoireMarqueurBuilder(EfGridInterface _grid) {
    finder = new EfLineSingleIntersectFinder(_grid);

  }

  /**
   * @return the dest
   */
  public EfTrajectoireResultBuilder getDest() {
    return dest_;
  }

  EfLineIntersection getIntersection(double _x, double _y) {
    return finder.findIntersectionFor(_x, _y, null);
  }

  public abstract void preCoordinateAdded(Coordinate _c, double _t);

  public void preCoordinateAdded(EfLineIntersection _c, double _t) {
    preCoordinateAdded(new Coordinate(_c.getX(), _c.getY()), _t);

  }
}
