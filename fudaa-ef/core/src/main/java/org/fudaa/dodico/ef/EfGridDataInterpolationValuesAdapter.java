/*
 * @creation 4 juin 07
 * @modification $Date: 2007-06-11 13:04:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import com.memoire.fu.FuLog;
import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * @author fred deniger
 * @version $Id: EfGridDataInterpolationValuesAdapter.java,v 1.2 2007-06-11 13:04:05 deniger Exp $
 */
public class EfGridDataInterpolationValuesAdapter implements InterpolationSupportValuesMultiI,
    SupportLocationI {
  final EfGridData data_;
  final int tIdx_;

  public EfGridDataInterpolationValuesAdapter(final EfGridData _data, final int _idx) {
    super();
    data_ = _data;
    tIdx_ = _idx;
  }

  @Override
  public int getPtsNb() {
    return data_.getGrid().getPtsNb();
  }

  @Override
  public double getPtX(int _i) {
    return data_.getGrid().getPtX(_i);
  }

  @Override
  public double getPtY(int _i) {
    return data_.getGrid().getPtY(_i);
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable _var) {
    try {
      return data_.getData(_var, tIdx_);
    } catch (IOException _evt) {
      FuLog.error(_evt);
    }
    return null;
  }

}
