package org.fudaa.dodico.ef.operation;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.decorator.EfGridDataCacheOneTimeDecorator;
import org.fudaa.dodico.ef.decorator.EfGridDataTimeDecorator;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

/**
 * Algorithme de Calcul les lignes de courants TODO mettre la classe dans EF il y a la classe qui est charge de faire le
 * calcul qui doit implementer CtulActivity et la classe qui implemente EfLineIntersectionParent.
 * 
 * @author Adrien Hadoux
 */
public class EfTrajectoireActivityNew implements CtuluActivity {

  final EfLineIntersectionsCorrectionTester lineIntersectCorrect_;

  /**
   * Liste des intersections r�sultats
   */
  ArrayList<EfLineIntersection> listeIntersection_;

  EfGridDataInterpolator gridDatas_;
  //  EfLineIntersectorActivity intersectFinder_;
  EfLineIntersectionsCorrectionActivity correctionActivity = new EfLineIntersectionsCorrectionActivity();
  EfLineIntersectionsCorrectionTester tester_;

  double[] timeStep_;

  /**
   * @param _interpolator ne doit pas etre null: contient les donn�es
   * @param _timeStep requis si un calcul en mode trajectoire est demande
   */
  public EfTrajectoireActivityNew(final EfGridData _data, final double[] _timeStep) {
    this(new EfGridDataInterpolator(_data), null, _timeStep);
  }

  /**
   * @param _interpolator ne doit pas etre null: contient les donn�es
   * @param _lineIntersectTester peut etre null: correction des intersections
   */
  public EfTrajectoireActivityNew(final EfGridDataInterpolator _interpolator, final EfLineIntersectionsCorrectionTester _lineIntersectTester,
      final double[] _timeStep) {
    gridDatas_ = _interpolator;
    tester_ = _lineIntersectTester;
    lineIntersectCorrect_ = _lineIntersectTester;
    timeStep_ = _timeStep;
    //    EfLineIntersectorActivity intersectFinder_;
    //    intersectFinder_ = new EfLineIntersectorActivity(gridDatas_);
    //    intersectFinder_.setPrecision(1E-3);
    if (CtuluLibArray.isEmpty(timeStep_))
      throw new IllegalArgumentException("on ne peut pas calculer un trajectoire sans les pas de temps");
  }

  protected boolean mustGoOn(final EfTrajectoireResultBuilder[] enCours) {
    for (int i = 0; i < enCours.length; i++) {
      if (enCours[i] != null) {
        return true;
      }

    }
    return false;

  }

  protected double getMaxDeltaFrom(final EfTrajectoireResultBuilder[] enCours, double _lastTime) {
    double res = 0;
    for (int i = 0; i < enCours.length; i++) {
      if (enCours[i] != null) {
        res = Math.max(res, Math.abs(enCours[i].getLastTime() - _lastTime));
      }

    }
    return res;

  }

  /**
   * @return the tester
   */
  public EfLineIntersectionsCorrectionTester getTester() {
    return tester_;
  }

  /**
   * @param _tester the tester to set
   */
  public void setTester(EfLineIntersectionsCorrectionTester _tester) {
    tester_ = _tester;
  }

  private volatile boolean warnAddForNullVelocities;

  /**
   * Methode appel�e par l'interface de calcul des lignes de courant.
   * 
   * @param init_ point de depart
   * @param dureeIntegration_ duree totale de l'integration en secondes
   * @param indicePdt indice du pas de temps
   * @param finesse coefficient de finesse double
   * @param vx la variable a utiliser
   * @param _prog l'interface de progression, peut etre null
   * @return
   */
  public List<EfTrajectoireResultBuilder> computeLigneCourant(final EfTrajectoireParameters _data, final ProgressionInterface _prog,
      final CtuluAnalyze log) {
    stop_ = false;
    final double first = timeStep_[0];
    final double last = timeStep_[timeStep_.length - 1];
    if (_data.firstTimeStep_ < first || _data.firstTimeStep_ > last) {
      log.addFatalError(DodicoLib.getS("Le premier pas de temps n'appartient pas � la simulation"));
      return null;
    }
    if (_data.isTrajectoire()) {
      final double endTime = _data.firstTimeStep_ + _data.dureeIntegration_;
      if (endTime < first || endTime > last) {
        log.addFatalError(DodicoLib.getS("La dur�e d'int�gration d�passe la dur�e de la simulation"));
        return null;
      }
    }
    if (_data.points_ == null)
      return Collections.emptyList();

    long to = System.nanoTime();

    EfTrajectoireGridDataProvider provider = createDataProvider(_data);

    // on initialise les resultats
    final EfGridDataInterpolator dataInterpolator = new EfGridDataInterpolator(new EfGridDataCacheOneTimeDecorator(gridDatas_.getData(),
        _data.firstTimeStepIdx_, _data.vx, _data.vy));
    final EfLineSingleIntersectFinder finder = new EfLineSingleIntersectFinder(dataInterpolator.getData().getGrid());
    finder.setTolerance(1E-3);

    final List<EfTrajectoireResultBuilder> res = new ArrayList<EfTrajectoireResultBuilder>(_data.points_.size());
    for (final Coordinate coord : _data.points_) {
      if (stop_)
        return res;
      final EfTrajectoireResultBuilder builder = new EfTrajectoireResultBuilder(_data, provider);
      final EfLineIntersection inter = finder.findIntersectionFor(coord.x, coord.y, _prog);
      builder.addFirstIntersection(inter, _data.firstTimeStep_, dataInterpolator);
      res.add(builder);
    }

    final EfTrajectoireResultBuilder[] enCours = res.toArray(new EfTrajectoireResultBuilder[res.size()]);
    double lastTime = _data.firstTimeStep_ + _data.dureeIntegration_;
    double absDuree = Math.abs(_data.dureeIntegration_);
    while (mustGoOn(enCours)) {
      if (stop_)
        return res;
      for (int i = 0; i < enCours.length; i++) {
        if (stop_)
          return res;
        final EfTrajectoireResultBuilder bi = enCours[i];
        if (bi != null && !doNext(bi, log)) {
          enCours[i] = null;
        }
        double maxDelta = getMaxDeltaFrom(enCours, lastTime);
        if (_prog != null) {
          _prog.setProgression((int) ((1D - maxDelta / absDuree) * 100));
        }

      }

    }
    System.err.println("normal=\t" + (System.nanoTime() - to) / 1000000d);
    to = System.nanoTime();
    List<EfTrajectoireResultBuilder> resBis = new ArrayList<EfTrajectoireResultBuilder>(2);
    if (_data.points_.size() == 1) {
      resBis.add(getRes(_data.points_.get(0), _data, dataInterpolator, log));
    } else {
      final List<EfTrajectoireResultBuilderCallable> callables = new ArrayList<EfTrajectoireResultBuilderCallable>(_data.points_.size());
      for (final Coordinate coord : _data.points_) {
        callables.add(new EfTrajectoireResultBuilderCallable(coord, _data, dataInterpolator, log, 0));
      }
      ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
      try {
        List<Future<EfTrajectoireResultBuilder>> results = newFixedThreadPool.invokeAll(callables);
        for (Future<EfTrajectoireResultBuilder> efTrajectoireResultBuilder : results) {
          resBis.add(efTrajectoireResultBuilder.get());
        }
      } catch (Exception e) {
        FuLog.error(e);
      }
    }
    System.err.println("optimise=\t" + (System.nanoTime() - to) / 1000000d);
    for (int i = res.size() - 1; i >= 0; i--) {
      EfTrajectoireResultBuilder init = res.get(i);
      EfTrajectoireResultBuilder newValue = resBis.get(i);
      List<Coordinate> initCoords = init.getCoords();
      List<Coordinate> newCoords = newValue.getCoords();
      if (initCoords.size() == newCoords.size()) {
        for (int j = initCoords.size() - 1; j >= 0; j--) {
          if (initCoords.get(j).distance(newCoords.get(j)) > 1e-4) {
            System.err.println("error");
          }
        }
      } else {
        System.err.println("error");
      }
    }

    return res;

  }

  private class EfTrajectoireResultBuilderCallable implements Callable<EfTrajectoireResultBuilder> {

    private final Coordinate coord;
    private final EfTrajectoireParameters _data;
    private final EfGridDataInterpolator interpolator;
    private CtuluAnalyze log;
    private final int idx;

    public EfTrajectoireResultBuilderCallable(Coordinate coord, EfTrajectoireParameters _data, EfGridDataInterpolator interpolator, CtuluAnalyze log,
        int idx) {
      super();
      this.coord = coord;
      this._data = _data;
      this.interpolator = interpolator;
      this.log = log;
      this.idx = idx;
    }

    @Override
    public EfTrajectoireResultBuilder call() throws Exception {
      try {
        return getRes(coord, _data, interpolator, log);
      } catch (Exception e) {
        e.printStackTrace();
      }
      return null;
    }

  }

  private EfTrajectoireResultBuilder getRes(Coordinate coord, final EfTrajectoireParameters _data, final EfGridDataInterpolator interpolator,
      CtuluAnalyze log) {
    //    final List<EfTrajectoireResultBuilder> res = new ArrayList<EfTrajectoireResultBuilder>(_data.points_.size());
    EfTrajectoireGridDataProvider provider = createDataProvider(_data);
    final EfTrajectoireResultBuilder builder = new EfTrajectoireResultBuilder(_data, provider);
    final EfLineSingleIntersectFinder finder = new EfLineSingleIntersectFinder(interpolator.getData().getGrid());
    final EfLineIntersection inter = finder.findIntersectionFor(coord.x, coord.y, null);
    finder.setTolerance(1E-3);
    builder.addFirstIntersection(inter, _data.firstTimeStep_, interpolator);
    while (!stop_) {
      if (!doNext(builder, log)) {
        return builder;
      }

    }
    return builder;
  }

  private EfTrajectoireGridDataProvider createDataProvider(final EfTrajectoireParameters _data) {
    EfTrajectoireGridDataProvider provider = null;
    if (_data.isLigneDeCourant) {
      provider = new EfTrajectoireGridDataProvider.LigneDeCourant(_data.firstTimeStepIdx_, gridDatas_.getData(), _data.vx, _data.vy);
    } else {
      if (CtuluLibArray.isEmpty(timeStep_))
        throw new IllegalArgumentException("on ne peut pas calculer un trajectoire sans les pas de temps");

      provider = new EfTrajectoireGridDataProvider.Trajectoire(timeStep_, gridDatas_.getData());
    }
    return provider;
  }

  /**
   * @param _enCours
   * @return true si on doit continuer, false si la trajectoire est arrive au bout.
   */
  boolean doNext(final EfTrajectoireResultBuilder _enCours, CtuluAnalyze log) {
    final Coordinate lastCoordinate = _enCours.getLastCoordinate();
    final TIntHashSet elementsAdjacent = _enCours.lastAdjacentMeshes_;
    final double lastTime = _enCours.getLastTime();
    // l'indice inf et sup des pas de temps englobant le dernier pas de temps
    final int[] minMaxLastTime = EfGridDataTimeDecorator.getTMinMax(timeStep_, lastTime);
    final EfTrajectoireParameters parameters = _enCours.getParameters();
    // dans le cas des trajectoires on doit rester dans les clous
    if (parameters.isTrajectoire() && minMaxLastTime == null) {
      FuLog.error("EfTrajectoireActivity: nous sommes en dehors de la simulation !");
      // on s'arrete et c'est bizarre
      return false;
    }
    final double maxDistance = getMaxDistance(lastCoordinate, elementsAdjacent);

    final double vx = _enCours.getLastVx();
    final double vy = _enCours.getLastVy();
    final double v = Math.hypot(vx, vy);
    // il s'agit du delta t en valu
    double absDeltaT = 0;
    int nextTimeStep = -1;
    // true si le pas de temps actuelle est sur un pas de temps du projet
    if (parameters.isTrajectoire()) {
      final boolean isOnTimeStep = (minMaxLastTime[0] == minMaxLastTime[1]);
      if (parameters.dureeIntegration_ < 0) {
        nextTimeStep = (isOnTimeStep) ? minMaxLastTime[0] - 1 : minMaxLastTime[0];
      } else {
        nextTimeStep = (isOnTimeStep) ? minMaxLastTime[1] + 1 : minMaxLastTime[1];
      }
      if (nextTimeStep < 0 || nextTimeStep >= timeStep_.length) {
        // ce ne devrait jamais arrive car l'iteration precedante aurait du arrete le tout
        FuLog.error("EfTrajectoireActivity: le pas de temps courant est en dehors de la simulation !");
        return false;
      }
    }
    // s'il y a une tres faible vitesse on va faire exploser le tout...
    final boolean isVitesseNulle = CtuluLib.isZero(v, 1E-4);
    if (isVitesseNulle) {
      // si c'est une ligne de courant on n'avancera plus
      if (parameters.isLigneDeCourant) {
        if (!warnAddForNullVelocities) {
          warnAddForNullVelocities = true;
          log.addWarn(DodicoLib.getS("Le calcul des lignes de courant a �t� arr�t� car une vitesse nulle a �t� rencontr�e"));
        }
        return false;
      }
      // sinon on va passer au temps suivant
      // on set le delta t pour qu'il aille au prochain pas de temps
      absDeltaT = Math.abs(timeStep_[nextTimeStep] - lastTime);
    } else {
      absDeltaT = maxDistance / v;

    }
    if (parameters.finesse_ > 1 && absDeltaT > 1E-2) {
      // la c'est bizarre: le coup de l'escargot
      absDeltaT = absDeltaT / parameters.finesse_;
    }
    absDeltaT = Math.max(absDeltaT, 1);// on se d�place au moins d'1 seconde pour avancer
    // pour les trajectoires, on essaie de ne pas sauter de pas de temps
    // dans le cas des vitesses null on ne fait rien
    if (!isVitesseNulle && parameters.isTrajectoire()) {
      absDeltaT = Math.min(Math.abs(timeStep_[nextTimeStep] - lastTime), absDeltaT);
    }
    if (parameters.dureeIntegration_ < 0)
      absDeltaT = -absDeltaT;

    // le nouveau pas de temps que l'on devrait avoir
    double newT = lastTime + absDeltaT;

    final double maxTimeAutorise = parameters.firstTimeStep_ + parameters.dureeIntegration_;
    if (Math.abs(newT - parameters.firstTimeStep_) > Math.abs(parameters.dureeIntegration_)) {
      newT = maxTimeAutorise;
    }

    // on remet a jour le delta t au cas il a ete modifie
    absDeltaT = newT - lastTime;
    final double newx = lastCoordinate.x + vx * absDeltaT;
    final double newy = lastCoordinate.y + vy * absDeltaT;
    EfLineIntersectorActivity intersectFinder = new EfLineIntersectorActivity(gridDatas_);
    intersectFinder.setPrecision(1E-3);
    intersectFinder.setPreselectedMeshes(elementsAdjacent);
    intersectFinder.setKnownFirstIntersection(_enCours.getLastIntersection());
    EfLineIntersectionsResultsI intersectResult = intersectFinder.computeFor(
        GISGeometryFactory.INSTANCE.createSegment(lastCoordinate.x, lastCoordinate.y, newx, newy), null,
        gridDatas_.getData().isElementVar(parameters.vx)).getDefaultRes();
    if (stop_)
      return false;
    if (tester_ != null) {
      intersectResult = correctionActivity.correct(intersectResult, tester_, 0, null);
      if (stop_)
        return false;
    }
    // on sort du maillage: on arrete
    if (intersectResult.isSegmentOut(0))
      return false;
    final EfLineIntersection foundIntersect = isVitesseNulle ? intersectResult.getIntersect(0) : intersectResult.getIntersect(1);
    // on a croise un objet du maillage, on doit recalculer le temps
    // si on une vitesse null, on n'a pas boug�...
    if (intersectResult.getNbIntersect() > 2 && !isVitesseNulle) {
      double deltaTFistInt = intersectResult.getDistFromDeb(1) / v;
      if (parameters.dureeIntegration_ < 0)
        deltaTFistInt = -deltaTFistInt;
      newT = lastTime + deltaTFistInt;
    }
    _enCours.addIntersection(foundIntersect, newT);
    if (FuLog.isDebug()) {
      FuLog.debug("Trajectoire compute on " + newT);
    }
    if (parameters.dureeIntegration_ > 0 && newT > maxTimeAutorise)
      return false;
    if (parameters.dureeIntegration_ < 0 && newT < maxTimeAutorise)
      return false;
    // on continue si diff�rent de 0
    return !CtuluLib.isZero(maxTimeAutorise - newT, 1E-2);
  }

  private double getMaxDistance(final Coordinate lastCoordinate, final TIntHashSet elementsAdjacent) {
    double maxDistance = 0;
    final EfGridInterface grid = gridDatas_.getData().getGrid();
    final Coordinate c = new Coordinate();
    for (final TIntIterator it = elementsAdjacent.iterator(); it.hasNext();) {
      final int idxElt = it.next();
      final EfElement elt = grid.getElement(idxElt);
      for (int i = 0; i < elt.getPtNb(); i++) {
        grid.getCoord(elt.getPtIndex(i), c);

        maxDistance = Math.max(maxDistance, CtuluLibGeometrie.getDistanceXY(c, lastCoordinate));
      }
    }
    return maxDistance;
  }

  public EfLineIntersectionsCorrectionTester getLineIntersectCorrect() {
    return lineIntersectCorrect_;
  }

  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
    correctionActivity.stop();
  }

}
