package org.fudaa.dodico.ef;

import org.fudaa.ctulu.CtuluLib;

import java.util.Comparator;

public class EfNodeComparator implements Comparator<EfNode> {
  public static final double DEFAULT_EPSILON = 1e-5;
  //La pr�cision dans le fichier �tant d'un dix�me, epsilon en vaut autant.
  private double epsilon = DEFAULT_EPSILON;

  public void setEpsilon(double epsilon) {
    epsilon = epsilon;
  }

  public EfNodeComparator() {
  }

  public EfNodeComparator(double epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  public int compare(EfNode o1, EfNode o2) {
    return this.compare(o1.getX(), o1.getY(), o2.getX(), o2.getY());
  }

  public int compare(double x1, double y1, double x2, double y2) {
    final int result = compare(x1, x2, epsilon);

    if (result == 0) {
      return compare(y1, y2, epsilon);
    }

    return result;
  }

  private static int compare(double d1, double d2, double epsilon) {
    if (CtuluLib.isEquals(d2, d1, epsilon)) {
      return 0;
    } else if (d1 < d2) {
      return -1;
    }

    return 1;
  }
}
