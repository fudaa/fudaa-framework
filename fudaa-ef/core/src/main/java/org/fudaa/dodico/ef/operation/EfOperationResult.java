/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Container du r�sultat d'une op�ration sur un maillages
 * 
 * @author deniger
 */
public class EfOperationResult {

  /**
   * R�sultat de l'op�ration. Si contient une FatalError les op�rations suivantes doivent s'arreter
   */
  CtuluAnalyze analyze;

  /**
   * true si la frontiere du maillage a �t� chang�e
   */
  boolean frontierChanged = true;

  /**
   * Le r�sultat final de l'op�ration
   */
  EfGridData gridData;

  public EfOperationResult() {
    super();
  }

  public EfOperationResult(CtuluAnalyze analyze, EfGridData gridData) {
    super();
    this.analyze = analyze;
    this.gridData = gridData;
  }

  /**
   * R�sultat de l'op�ration. Si contient une FatalError les op�rations suivantes doivent s'arreter
   * 
   * @return the analyze
   */
  public CtuluAnalyze getAnalyze() {
    return analyze;
  }

  /**
   * @return the gridData Le r�sultat final de l'op�ration
   */
  public EfGridData getGridData() {
    return gridData;
  }

  /**
   * @return true si la frontiere du maillage a �t� chang�e
   */
  public boolean isFrontierChanged() {
    return frontierChanged;
  }

  /**
   * R�sultat de l'op�ration. Si contient une FatalError les op�rations suivantes doivent s'arreter
   * 
   * @param analyze the analyze to set
   */
  public void setAnalyze(CtuluAnalyze analyze) {
    this.analyze = analyze;
  }

  /**
   * @param frontierChanged true si la frontiere du maillage a �t� chang�e
   */
  public void setFrontierChanged(boolean frontierChanged) {
    this.frontierChanged = frontierChanged;
  }

  /**
   * @param gridData the gridData Le r�sultat final de l'op�ration
   */
  public void setGridData(EfGridData gridData) {
    this.gridData = gridData;
  }

}
