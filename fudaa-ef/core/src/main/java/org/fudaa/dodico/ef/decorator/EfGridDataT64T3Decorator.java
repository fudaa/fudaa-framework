/*
 *  @creation     17 mai 2005
 *  @modification $Date: 2007-06-11 13:08:15 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.ef.decorator;

import com.memoire.fu.FuLog;
import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * Decoration pour les opérations qui transforme les T6 en 4 T3
 * 
 * @author Fred Deniger
 * @version $Id: MvExport4T3.java,v 1.5 2007-06-11 13:08:15 deniger Exp $
 */
public class EfGridDataT64T3Decorator extends EfGridDataRenumeroteDecorator {

  /**
   * @param _src
   */
  public EfGridDataT64T3Decorator(final EfGridData _src, EfGridInterface newGrid) {
    super(_src, newGrid);
  }

  @Override
  public EfData getData(final CtuluVariable _t, final int _idxTime) throws IOException {
    final EfData init = getInit().getData(_t, _idxTime);
    final boolean isEle = init.isElementData();
    if (!isEle) { return init; }
    if (FuLog.isTrace()) {
      FuLog.trace("TRE: 4T3 build for element");
    }
    final double[] newValues = new double[getNewGrid().getEltNb()];
    final int oldEltNb = getInit().getGrid().getEltNb();
    for (int i = oldEltNb - 1; i >= 0; i--) {
      final double d = init.getValue(i);
      newValues[i * 4] = d;
      newValues[i * 4 + 1] = d;
      newValues[i * 4 + 2] = d;
      newValues[i * 4 + 3] = d;
    }
    return new EfDataElement(newValues);
  }
}
