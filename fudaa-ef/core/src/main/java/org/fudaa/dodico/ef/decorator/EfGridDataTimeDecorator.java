/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.decorator;

import java.io.IOException;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataElement;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * un decorateur de grid data qui permet d'interpoler les valeurs sur un pas de temps donn�.
 * 
 * @author deniger
 */
public class EfGridDataTimeDecorator implements EfGridData {

  final EfGridData init_;
  final int tMaxIdx_;
  final int tMinIdx_;
  final double coeff_;

  /**
   * @param _init les donn�es initiales
   * @param _t le pas de temps sur lequel on veut interpoler
   * @param _minIdx le pas de temps inf a t
   * @param _min le temps inf
   * @param _maxIdx le pas de temps sup a t
   * @param _max le temps sup
   */
  public EfGridDataTimeDecorator(EfGridData _init, double _t, int _minIdx, double _min, int _maxIdx, double _max) {
    super();
    init_ = _init;
    tMaxIdx_ = _maxIdx;
    tMinIdx_ = _minIdx;
    coeff_ = (_t - _min) / (_max - _min);
  }

  /**
   * @param _timeIdx non utilise dans ce cas {@inheritDoc}
   */
  @Override
  public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
    final EfData data1 = init_.getData(_o, tMinIdx_);
    final EfData data2 = init_.getData(_o, tMaxIdx_);
    final double[] res = new double[data1.getSize()];
    for (int i = res.length - 1; i >= 0; i--) {
      final double v1 = data1.getValue(i);
      res[i] = coeff_ * (data2.getValue(i) - v1) + v1;
    }
    return data1.isElementData() ? (EfData) new EfDataElement(res) : new EfDataNode(res);
  }

  /**
   * @param _timeIdx non utilise dans ce cas {@inheritDoc}
   */
  @Override
  public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
    double data1 = init_.getData(_o, tMinIdx_, _idxObjet);
    double data2 = init_.getData(_o, tMaxIdx_, _idxObjet);
    return coeff_ * (data2 - data1) + data1;
  }

  @Override
  public EfGridInterface getGrid() {
    return init_.getGrid();
  }

  @Override
  public boolean isDefined(CtuluVariable _var) {
    return init_.isDefined(_var);
  }

  @Override
  public boolean isElementVar(CtuluVariable _idxVar) {
    return init_.isElementVar(_idxVar);
  }

  /**
   * @param srcTimeStep les pas te
   * @param time
   * @return null si en dehors ou tableau de 2 entier avec idxMin et idxMax des pas de temps englobant
   */
  public static int[] getTMinMax(double[] srcTimeStep, double time) {
    if (CtuluLib.isEquals(time, srcTimeStep[0], 1E-3)) { return new int[] { 0, 0 }; }
    if (CtuluLib.isEquals(time, srcTimeStep[srcTimeStep.length - 1], 1E-3)) { return new int[] {
        srcTimeStep.length - 1, srcTimeStep.length - 1 }; }
    if (time < srcTimeStep[0] || time > srcTimeStep[srcTimeStep.length - 1]) {

    return null; }
    int[] res = new int[2];
    final int idx = Arrays.binarySearch(srcTimeStep, time);
    if (idx >= 0) {
      res[0] = idx;
      res[1] = idx;
    } else {
      res[0] = -idx - 2;
      res[1] = res[0] + 1;
      if (CtuluLib.isEquals(time, srcTimeStep[res[0]], 1E-3)) {
        // en fait c'est egale a la borne inf, on prend celle-ci
        res[1] = res[0];
      }
      if (CtuluLib.isEquals(time, srcTimeStep[res[1]], 1E-3)) {
        // en fait c'est egale a la borne sup, on prend celle-ci
        res[0] = res[1];
      }
    }
    return res;
  }

  public static EfGridData getTimeInterpolated(EfGridData _init, double[] srcTimeStep, double time) {

    if (time < srcTimeStep[0] || time > srcTimeStep[srcTimeStep.length - 1]) { throw new IllegalArgumentException(
        "impossible to interpolate"); }
    final int idx = Arrays.binarySearch(srcTimeStep, time);
    if (idx >= 0) {
      return new EfGridDataOneTimeDecorator(_init, idx);
    } else {
      int tmin = -idx - 2;
      int tmax = tmin + 1;
      if (CtuluLib.isEquals(time, srcTimeStep[tmin], 1E-3)) { return new EfGridDataOneTimeDecorator(_init, tmin); }
      if (CtuluLib.isEquals(time, srcTimeStep[tmax], 1E-3)) { return new EfGridDataOneTimeDecorator(_init, tmax); }
      return new EfGridDataTimeDecorator(_init, time, tmin, srcTimeStep[tmin], tmax, srcTimeStep[tmax]);
    }

  }
}