package org.fudaa.dodico.ef.frontier;

import gnu.trove.TIntIntHashMap;
import org.fudaa.dodico.ef.impl.EfFrontier;

/**
 * 
 * Permet d'avoir la correspondance entre un numero frontiere et un ancien. On
 * manipule les indices sur la frontiere donnes par exemple par @see
 * {@link EfFrontier#getFrontiereIndice(int, int)} ou par
 * {@link EfFrontier#getArray()}
 * 
 */
public class EfFrontierMapper {
    private final TIntIntHashMap frontierNewPtsOldPts;

    public EfFrontierMapper(TIntIntHashMap frontierNewPtsOldPts) {
	this.frontierNewPtsOldPts = frontierNewPtsOldPts;
    }

    /**
     * Permet de savoir si un point fronti�re est nouveau.
     * 
     * @param idxFrtInNewGrid
     *            le nouveau point fronti�re � tester.
     * @return true si le nouveau point n'a pas de correspondance ou false si il
     *         en a une ou n'est pas un point fronti�re.
     */
    public boolean isNewFrontierNode(int idxFrtInNewGrid) {
	if (this.frontierNewPtsOldPts.containsKey(idxFrtInNewGrid)) {
	    if (this.frontierNewPtsOldPts.get(idxFrtInNewGrid) == -1) {
		return true;
	    }
	}

	return false;
    }

    /**
     * Donne la correspondance entre un noeuveau et un ancien point fronti�re.
     * 
     * @param idxFrtInNewGrid
     *            l'indice du nouveau point fronti�re.
     * @return l'indice de l'ancien point fronti�re ou -1 si le nouveau point
     *         n'a pas de correspondance ou n'est pas un point fronti�re.
     */
    public int getOldFrtIdx(int idxFrtInNewGrid) {
	if (this.frontierNewPtsOldPts.containsKey(idxFrtInNewGrid)) {
	    return this.frontierNewPtsOldPts.get(idxFrtInNewGrid);
	}

	return -1;
    }
}
