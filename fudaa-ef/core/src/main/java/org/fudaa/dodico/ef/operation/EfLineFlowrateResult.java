/*
 * @creation 30 janv. 07
 * @modification $Date: 2007-06-13 12:55:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import com.memoire.fu.FuLog;
import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Une classe permettant de calculer le d�bit � partir d'un profil.
 *
 * @author fred deniger
 * @version $Id: EfLineFlowrateResult.java,v 1.4 2007-06-13 12:55:42 deniger Exp $
 */
public class EfLineFlowrateResult {
  EfLineIntersectionsResultsBuilder res_;
  /**
   * L'identifiant pour la vitesse selon x.
   */
  final CtuluVariable vx_;
  /**
   * L'identifiant pour la vitesse selon y.
   */
  final CtuluVariable vy_;
  /**
   * L'identifiant pour la hauteur d'eau.
   */
  final CtuluVariable h_;
  final EfVectorComputation calculator_;

  public EfLineFlowrateResult(EfLineIntersectionsResultsBuilder _res, final CtuluVariable _vx, final CtuluVariable _vy,
          final CtuluVariable _h) {
    super();
    res_ = _res;
    vx_ = _vx;
    vy_ = _vy;
    h_ = _h;
    calculator_ = new EfVectorComputation();
  }

  /**
   * Calcul le d�bit entre le point (_idxPtOnProfil) et (_idxPtOnProfil+1).
   *
   * @param _idxPtOnProfil l'indice du point sur les intersections. Ne fait pas de calcul pour le dernier point =0
   * @param _vx les vitesses selon x
   * @param _vy les vitesses selon y
   * @param _h la hauteur d'eau
   * @return le d�bit
   */
  double getQ(int _idxPtOnProfil, EfData _vx, EfData _vy, EfData _h, EfLineIntersectionsResultsI _res) {
    if (_idxPtOnProfil >= _res.getNbIntersect() - 1) {
      return 0;
    }
    // le point 1
    int idx = _idxPtOnProfil;
    calculator_.x1_ = _res.getIntersect(idx).getX();
    calculator_.y1_ = _res.getIntersect(idx).getY();
    // double n = _res.getIntersect(idx).getValueBadInterpolation(_vx);
    // double t = _res.getIntersect(idx).getValueBadInterpolation(_vy);
    calculator_.vx1_ = _res.getIntersect(idx).getValueBadInterpolation(_vx);// InterpolationVectorContainer.getVx(n, t);
    calculator_.vy1_ = _res.getIntersect(idx).getValueBadInterpolation(_vy);
    calculator_.h1_ = _res.getIntersect(idx).getValueBadInterpolation(_h);
    idx++;
    calculator_.x2_ = _res.getIntersect(idx).getX();
    calculator_.y2_ = _res.getIntersect(idx).getY();
    // n = _res.getIntersect(idx).getValueBadInterpolation(_vx);
    // t = _res.getIntersect(idx).getValueBadInterpolation(_vy);
    calculator_.vx2_ = _res.getIntersect(idx).getValueBadInterpolation(_vx);
    calculator_.vy2_ = _res.getIntersect(idx).getValueBadInterpolation(_vy);
    calculator_.h2_ = _res.getIntersect(idx).getValueBadInterpolation(_h);
    return calculator_.getQ();
  }

  public double getQ(EfGridData _data, int _tidx, ProgressionInterface _prog) {
    EfData vx = null;
    EfData vy = null;
    EfData h = null;
    try {
      vx = _data.getData(vx_, _tidx);
      vy = _data.getData(vy_, _tidx);
      // norme = new EfDataInterpolationAdapter.Norm(vx, vy);
      // theta = new EfDataInterpolationAdapter.Theta(vx, vy);
      h = _data.getData(h_, _tidx);
    } catch (IOException _evt) {
      FuLog.error(_evt);

    }
    if (vx == null || vy == null || h == null) {
      return 0;
    }
    double q = 0;
    EfLineIntersectionsResultsI res = res_.createResults(_tidx, _prog);
    ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(10, res.getNbIntersect() - 1);
    for (int i = 0; i < res.getNbIntersect() - 1; i++) {
      if (res.isSegmentIn(i)) {
        q += getQ(i, vx, vy, h, res);
      }
      up.majAvancement();
    }
    return q;
  }
}
