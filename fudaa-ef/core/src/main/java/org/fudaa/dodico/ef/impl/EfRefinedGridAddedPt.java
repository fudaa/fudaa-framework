/*
 * @creation 20 avr. 07
 * 
 * @modification $Date: 2007-04-26 14:28:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;

/**
 * Un maillage issus d'un raffinement qui a consist� � ajout� des noeuds � un ancien maillage. Pour infos, certains
 * raffinements peuvent d�placer et/ou enlever des noeuds.
 * 
 * @author fred deniger
 * @version $Id: EfRefinedGridAddedPt.java,v 1.2 2007-04-26 14:28:47 deniger Exp $
 */
public class EfRefinedGridAddedPt extends EfGridAbstract {
  final int nbPt_;
  final double[] newPt_;
  final EfGridInterface old_;
  final int oldNbPt_;

  public EfRefinedGridAddedPt(EfElement[] _elts, double[] _newXyz, EfElementType _type, EfGridInterface _old) {
    super(_elts);
    typeElt_ = _type;
    newPt_ = _newXyz;
    old_ = _old;
    oldNbPt_ = old_.getPtsNb();
    nbPt_ = oldNbPt_ + newPt_.length / 3;
  }

  @Override
  protected boolean setZIntern(int _i, double _newV) {
    return false;
  }

  public EfNeighborMesh computeNeighborForOld(ProgressionInterface _prog, CtuluAnalyze _analyze) {
    if (old_.getNeighbors() == null) old_.computeNeighbord(_prog, _analyze);
    return old_.getNeighbors();
  }

  public EfGridInterface getOld() {
    return old_;
  }

  public boolean isOldPt(int _idx) {
    return _idx < oldNbPt_;
  }

  public int getOltEltIdxForAddedPt(int _idx) {
    return _idx - oldNbPt_;
  }

  public boolean isPtAddedForEltCenter(int _idx) {
    return _idx >= oldNbPt_;
  }

  public int getOldNbPt() {
    return oldNbPt_;
  }

  @Override
  public int getPtsNb() {
    return nbPt_;
  }

  @Override
  public double getPtX(int _i) {
    if (_i < oldNbPt_) return old_.getPtX(_i);
    return newPt_[(_i - oldNbPt_) * 3];
  }

  @Override
  public double getPtY(int _i) {
    if (_i < oldNbPt_) return old_.getPtY(_i);
    return newPt_[1 + (_i - oldNbPt_) * 3];
  }

  @Override
  public double getPtZ(int _i) {
    if (_i < oldNbPt_) return old_.getPtZ(_i);
    return newPt_[2 + (_i - oldNbPt_) * 3];
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y) {
    this.setPt(_i, _x, _y, 0.0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y, double _z) {

    if (_i < oldNbPt_) {
      old_.setPt(_i, _x, _y, _z);
    } else {
      newPt_[(_i - oldNbPt_) * 3] = _x;
      newPt_[1 + (_i - oldNbPt_) * 3] = _y;
      newPt_[2 + (_i - oldNbPt_) * 3] = _z;
    }
    geomXYChanged();
  }
}
