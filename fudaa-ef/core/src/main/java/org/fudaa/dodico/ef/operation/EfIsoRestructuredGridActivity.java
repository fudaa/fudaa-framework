/*
 * @creation 20 avr. 07
 * @modification $Date: 2007-04-26 14:28:59 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.ef.EfSegmentMutable;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.impl.EfRefinedGridAddedPt;

/**
 * Permet de transformer un maillage afin qu'il soit utilisable pour la recherche des isolignes sur les valeurs sont
 * d�finies sur les �l�ments. Donc � utiliser que dans ce cas: inutile dans les autres cas.
 * 
 * @author fred deniger
 * @version $Id: EfIsoRestructuredGridActivity.java,v 1.2 2007-04-26 14:28:59 deniger Exp $
 */
public class EfIsoRestructuredGridActivity implements CtuluActivity {

  boolean stop_;

 public  EfIsoRestructuredGridResult restructure(EfGridInterface _g, ProgressionInterface _prog, CtuluAnalyze _an) {
    EfGridInterface grid = _g;
    // si c'est un T6, les valeurs seront d�finies sur les elements, donc les points milieux sont inutiles et on les
    // vire.
    // TIntIntHashMap t6Modif = null;
    if (grid.getEltType() == EfElementType.T6) {
      // t6Modif = new TIntIntHashMap(grid.getPtsNb() / 2 + 1);
      grid = EfLibImpl.maillageT6enT3(grid, _prog, null);
    }
    return go(_prog, grid, _an);

  }

  private EfIsoRestructuredGridResult go(ProgressionInterface _prog, EfGridInterface _grid, CtuluAnalyze _an) {
    ProgressionUpdater up = new ProgressionUpdater(_prog);

    if (_grid.getNeighbors() == null) {
      _grid.computeNeighbord(_prog, _an);
      if (_grid.getNeighbors() == null) return null;
    }
    // non nul !
    EfNeighborMesh neighbors = _grid.getNeighbors();
    up.majProgessionStateOnly(DodicoLib.getS("Raffinement du maillage"));
    int eltNb = _grid.getEltNb();
    int ptNb = _grid.getPtsNb();
    // vir�!
    // la liste des aretes d�j� trait�es
    Set doneEdge = new HashSet(eltNb * 3);
    EfSegmentMutable seg = new EfSegmentMutable(0, 0);
    // un point sera ajout� par element
    double[] newXyz = new double[eltNb * 3];
    up.setValue(10, eltNb);
    for (int i = 0; i < eltNb; i++) {
      if (stop_) return null;
      // on utilise la moyenne : impos� par Rubar !
      newXyz[i * 3] = _grid.getMoyCentreXElement(i);
      newXyz[i * 3 + 1] = _grid.getMoyCentreYElement(i);
      newXyz[i * 3 + 2] = _grid.getMoyCentreZElement(i);
      up.majAvancement();
    }
    if (stop_) return null;
    // max absolu
    List newElt = new ArrayList(eltNb * 4);
    up.setValue(10, eltNb);
    for (int i = 0; i < eltNb; i++) {
      EfElement el = _grid.getElement(i);
      if (stop_) return null;
      for (int j = 0; j < el.getNbEdge(); j++) {
        int idx1 = el.getEdgePt1(j);
        int idx2 = el.getEdgePt2(j);
        int idxElt = neighbors.getAdjacentMeshes(idx1, idx2, i);
        // une arete interne
        if (idxElt >= 0) {
          seg.setMinMaxIdx(idx1, idx2);
          // s'il arete a d�ja ete effecut�e on oublie
          if (!doneEdge.contains(seg)) {
            doneEdge.add(new EfSegment(seg));
            // on va ajouter les 2 �l�ments qui relie les points de l'arete et les 2 centres
            int[] idx = new int[3];
            // pour l'indice 1 de l'arete
            // l'ordre est (peut �tre) important: le meme que les �l�ments initiaux
            idx[0] = idx1;
            // le centre de l'�l�ment adjacent
            idx[1] = ptNb + idxElt;
            // le centre de cet �l�ment en cours
            idx[2] = ptNb + i;
            newElt.add(new EfElement(idx));
            // l'indice 2
            idx = new int[3];
            idx[0] = idx2;
            // dans l'autre sen
            idx[1] = ptNb + i;
            idx[2] = ptNb + idxElt;
            newElt.add(new EfElement(idx));
          }
        }
        // arete frontiere: on l'ajoute
        else {
          // (ptNb+i) represente l'indice du point du centre de l'�l�ment
          newElt.add(new EfElement(new int[] { idx1, idx2, ptNb + i }));

        }
      }
      up.majAvancement();
    }
    if (stop_) return null;

    EfRefinedGridAddedPt newGrid = new EfRefinedGridAddedPt((EfElement[]) newElt.toArray(new EfElement[newElt.size()]),
        newXyz, EfElementType.T3, _grid);
    return new EfIsoRestructuredGridResult(newGrid);
  }

  @Override
  public void stop() {
    stop_ = true;

  }

}
