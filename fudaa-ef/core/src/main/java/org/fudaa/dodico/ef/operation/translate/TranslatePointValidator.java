package org.fudaa.dodico.ef.operation.translate;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluLogGroup;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.ef.resource.EfResource;
import org.locationtech.jts.algorithm.RobustLineIntersector;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.PrecisionModel;

import java.util.*;

/**
 */
public class TranslatePointValidator {
  private final int[] movedIdxArray;
  private final EfGridPointsTranslated translatedGrid;
  private final TranslatePointCleanValidator cleanValidator;
  private RobustLineIntersector lineIntersector = new RobustLineIntersector();
  private Coordinate edge1pt1 = new Coordinate();
  private Coordinate edge1pt2 = new Coordinate();
  private Coordinate edge2pt1 = new Coordinate();
  private Coordinate edge2pt2 = new Coordinate();

  /**
   * @param grid the grid to set
   */
  public TranslatePointValidator(EfGridInterface grid, int[] movedIdx, double[] newX, double[] newY) {
    this.movedIdxArray = movedIdx;
    this.translatedGrid = new EfGridPointsTranslated(grid, movedIdx, newX, newY);
    cleanValidator = new TranslatePointCleanValidator(translatedGrid);
    lineIntersector.setPrecisionModel(new PrecisionModel(1E2));
  }

  private boolean canTranslate(int ptIdx, ProgressionInterface prog) {
    final int[] neighbors = this.getNeighbors(ptIdx, prog);

    //no intersection in edges
    List<EfSegment> segments = getSegmentsNearPoint(neighbors);
    for (int i = 0; i < segments.size(); i++) {
      fillCoordinate(segments, edge1pt1, edge1pt2, i);
      for (int j = i + 1; j < segments.size(); j++) {
        fillCoordinate(segments, edge2pt1, edge2pt2, j);
        lineIntersector.computeIntersection(edge1pt1, edge1pt2, edge2pt1, edge2pt2);

        if (lineIntersector.hasIntersection() && lineIntersector.isProper() && lineIntersector.getIntersectionNum() != 2) {
          return false;
        }
      }
    }

    final double newX = translatedGrid.getPtX(ptIdx);
    final double newY = translatedGrid.getPtY(ptIdx);
    for (int neighbor : neighbors) {
      translatedGrid.setIndexToKeepXY(ptIdx);
      if (this.translatedGrid.getElement(neighbor).contientXY(newX, newY, translatedGrid)) {
        translatedGrid.setIndexToKeepXY(-1);
        return true;
      }
      translatedGrid.setIndexToKeepXY(-1);
    }

    if (cleanValidator.shouldBeCleaned(ptIdx, prog, null, null, true)) {
      return true;
    }
    EfFrontierInterface frontiers = this.translatedGrid.getInitialGrid().getFrontiers();
    if (frontiers == null) {
      this.translatedGrid.getInitialGrid().computeBord(prog, null);
      frontiers = this.translatedGrid.getInitialGrid().getFrontiers();
    }

    if (frontiers.isFrontierPoint(ptIdx)) {
      final int idxFrt = frontiers.getIdxFrontierIdxPtInFrontierFromGlobal(ptIdx)[0];
      final LinearRing frtRing = frontiers.getLinearRing(translatedGrid.getInitialGrid(), idxFrt);
      final PointOnGeometryLocator polygoneTester = GISLib.createPolygoneTester(frtRing);
      final Coordinate newPoint = new Coordinate(newX, newY);

      if (frontiers.isExtern(idxFrt)) {
        return (!GISLib.isInside(polygoneTester, newPoint));
      } else {
        return (GISLib.isInside(polygoneTester, newPoint));
      }
    }
    return false;
  }

  private void fillCoordinate(List<EfSegment> segments, Coordinate edge1pt1, Coordinate edge1pt2, int i) {
    edge1pt1.x = translatedGrid.getPtX(segments.get(i).getPt1Idx());
    edge1pt1.y = translatedGrid.getPtY(segments.get(i).getPt1Idx());
    edge1pt2.x = translatedGrid.getPtX(segments.get(i).getPt2Idx());
    edge1pt2.y = translatedGrid.getPtY(segments.get(i).getPt2Idx());
  }

  private List<EfSegment> getSegmentsNearPoint(int[] neighbors) {
    //we check that there are no intersection in Edges.
    Set<EfSegment> segmentsSet = new HashSet<>();
    for (int neighbor : neighbors) {
      final EfElement element = translatedGrid.getElement(neighbor);
      for (int j = 0; j < element.getNbEdge(); j++) {
        int idx1 = element.getEdgePt1(j);
        int idx2 = element.getEdgePt2(j);
        //to void duplicate
        segmentsSet.add(new EfSegment(Math.min(idx1, idx2), Math.max(idx1, idx2)));
      }
    }

    return new ArrayList<>(segmentsSet);
  }

  public CtuluLogGroup shouldBeCleaned(ProgressionInterface prog) {
    TIntHashSet emptyMeshes = new TIntHashSet();
    TIntIntHashMap removedPoint = new TIntIntHashMap();
    for (int movedIdx : movedIdxArray) {
      cleanValidator.shouldBeCleaned(movedIdx, prog, emptyMeshes, removedPoint, true);
    }
    CtuluLogGroup res = new CtuluLogGroup(null);
    if (!emptyMeshes.isEmpty()) {
      final CtuluLog log = res.createNewLog(EfResource.getS("El�ment(s) � supprimer"));
      final int[] emptyMeshesArray = emptyMeshes.toArray();
      Arrays.sort(emptyMeshesArray);
      for (int emptyArrayIdx : emptyMeshesArray) {
        log.addWarn(EfResource.getS("L'�l�ment {0} aura une surface nulle", CtuluLibString.getString(emptyArrayIdx + 1)));
      }
    }
    if (!removedPoint.isEmpty()) {
      TIntHashSet doneWarning = new TIntHashSet();
      final int[] removePointArray = removedPoint.keys();
      Arrays.sort(removePointArray);
      final CtuluLog log = res.createNewLog(EfResource.getS("Point(s) � supprimer"));
      for (final int pointIdx : removePointArray) {
        final int otherPtIdx = removedPoint.get(pointIdx);
        if (!doneWarning.contains(Math.min(pointIdx, otherPtIdx))) {
          doneWarning.add(Math.min(pointIdx, otherPtIdx));
          log.addWarn(EfResource.getS("Le point {0} sera supprim� car confondu avec le point {1}",
              CtuluLibString.getString(pointIdx + 1),
              CtuluLibString.getString(otherPtIdx + 1))
          );
        }
      }
    }
    return res;
  }

  public CtuluLog canTranslate(ProgressionInterface prog) {
    CtuluLog res = new CtuluLog();
    for (int idx : movedIdxArray) {
      if (!canTranslate(idx, prog)) {
        res.addError(EfResource.getS("Le d�placement du point {0} rend le maillage invalide", CtuluLibString.getString(idx + 1)));
      }
    }
    return res;
  }

  private int[] getNeighbors(int ptIdx, ProgressionInterface prog) {
    return cleanValidator.getNeighbors(ptIdx, prog);
  }
}
