/*
 *  @creation     19 d�c. 2005
 *  @modification $Date: 2007-01-19 13:07:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;


/**
 * Une classe permettant d'utilisant des donn�es differentes pour la bathy.
 * 
 * @author Fred Deniger
 * @version $Id: EfGridBathyAdapter.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridBathyAdapter extends EfGridDefaultAbstract {

  final EfData bathy_;

  final EfGridInterface grid_;

  /**
   * @param _bathy
   * @param _grid
   */
  public EfGridBathyAdapter(final EfData _bathy, final EfGridInterface _grid) {
    super();
    bathy_ = _bathy;
    grid_ = _grid;
  }

  @Override
  protected boolean setZIntern(final int _i, final double _newV) {
    return false;
  }

  @Override
  public EfElement getElement(final int _i) {
    return grid_.getElement(_i);
  }

  @Override
  public int getEltNb() {
    return grid_.getEltNb();
  }

  @Override
  public int getPtsNb() {
    return grid_.getPtsNb();
  }

  @Override
  public double getPtX(final int _i) {
    return grid_.getPtX(_i);
  }

  @Override
  public double getPtY(final int _i) {
    return grid_.getPtY(_i);
  }

  @Override
  public double getPtZ(final int _i) {
    return bathy_ == null ? 0 : bathy_.getValue(_i);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y) {
    this.setPt(_i, _x, _y, 0.0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPt(int _i, double _x, double _y, double _z) {
    grid_.setPt(_i, _x, _y);
    
    if (bathy_ != null)
    {
      //TODO Le _z devrait �tre setter mais bathy_ n'a pas de setValue().
    }
  }
}
