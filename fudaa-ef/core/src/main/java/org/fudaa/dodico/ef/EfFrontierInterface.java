/*
 * @creation 17 janv. 07
 * @modification $Date: 2007-01-19 13:07:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.dodico.ef.impl.EfFrontier.FrontierIterator;

/**
 * @author fred deniger
 * @version $Id: EfFrontierInterface.java,v 1.1 2007-01-19 13:07:18 deniger Exp $
 */
public interface EfFrontierInterface {

  GISPolygone getLinearRing(final EfGridInterface _grid, final int _idxFr);

  GISPolygone getSavedLinearRing(final int _idxFr);

  /**
   * Si des donn�es ont �t� modifi�es, permet d'effacer les �ventuels calcul interm�diaires.
   */
  void clearSaveData();

  /**
   * @param _fr la frontiere a comparer
   * @return true si identique
   */
  boolean isSame(final EfFrontierInterface _fr);

  /**
   * @param _idxFr l'indice de la frontiere
   * @return une copie des indices des noeuds pour la frontiere <code>_idxFr</code>
   */
  int[] getIdxCopy(int _idxFr);

  boolean isExtern(final int _i);

  /**
   * Renvoie l'index de frontiere a partir de l'index de la frontiere et de l'index du point sur la fronti�re. Par
   * exemple si vous avez 2 frontieres,la premiere de taille 100 et la 2eme de taille 20, l'appel getIdxFrGlob(1,10)
   * renvoie 110 (le point 10 de la 2eme frontiere). getIdxFrGlob(0,10) renvoie (10). getIdxFrGlob(1,100) renvoie -1
   * (erreur).
   * 
   * @param _idxFr l'indice de la frontier
   * @param _idxOnFr l'indice du point sur la frontier _idxFr
   * @return -1 si les donnees sont erron�es.
   */
  int getFrontiereIndice(final int _idxFr, final int _idxOnFr);

  int getFrontiereIndice(final int _idxGlob);
  
  double[] getAbsCurviligne(final EfGridInterface _grid, final int _frIdx, final int _firstIdxOnFr, final int _nb);

  GISPolygone[] getExternRing(final EfGridInterface _grid);

  GISPolygone[] getInternRing(final EfGridInterface _grid);

  int getNbFrontierExt();

  /**
   * @return l'iterateur sur les frontieres
   */
  AllFrontierIteratorInterface getAllFrontierIterator();

  /**
   * @param _idxGlobal
   * @return [ frontier index, point index in the frontier] from the general index.
   */
  int[] getIdxFrontierIdxPtInFrontierFromGlobal(final int _idxGlobal);

  /**
   * Initialyze <code>temp</code> with [ frontier index, point index in the frontier] from the general index.
   * 
   * @param _idxGlobal l'indice general du point
   * @param _temp le tableau a modifier [ frontier index, point index in the frontier]
   * @return true si cet indice est bien un indice frontiere
   */
  boolean getIdxFrontierIdxPtInFrontierFromGlobal(final int _idxGlobal, final int[] _temp);

  int getIdxFrontierFromGlobal(final int _idxGlobal);

  /**
   * Renvoie l'index du point d'indice global <code>_idxGlobal</code> sur la frontiere.
   * 
   * @param _idxGlobal l'indice globale
   * @return -1 si c'est n'est pas un point frontier. L'indice de frontiere sinon
   */
  int getIdxOnFrontier(final int _idxGlobal);

  /**
   * @param _idxGlobal l'indice global
   * @param _idxFr l'indice de la frontiere voulu
   * @return l'indice du point d'indice _idxGlobal sur la frontiere _idxFr
   */
  int getQuickIdxOnFrontier(final int _idxGlobal, final int _idxFr);

  /**
   * Renvoie l'indice de frontiere a partir de l'indice global. Initialise egalement le tableau idxFrIdxOnFr (qui doit
   * etre de taille 2) avec l'index de frontiere et l'index du point sur la frontiere.
   * 
   * @param _idxGlobal l'indice global du point
   * @param _idxFrIdxOnFr l'indice sur la frontiere
   * @return l'indice de frontiere a partir de l'indice global
   */
  int getIdxOnFrontier(final int _idxGlobal, final int[] _idxFrIdxOnFr);

  /**
   * @param _idxGlobal l'indice global a tester
   * @return true si le point d'indice globle <code>_idxGlobal</code> est un point frontiere
   */
  boolean isFrontierPoint(final int _idxGlobal);

  /**
   * Initialyze the array _idxFrIdxPt with [frontiere idx,point idx on frontier] from the frontier index _idxOnFrontier.
   * 
   * @param _idxOnFrontier l'indice frontiere du point
   * @param _idxFridxPt le tableau a initialise
   * @return true if found false otherwise. true si trouve
   */
  boolean getIdxFrontierIdxPtInFrontierFromFrontier(final int _idxOnFrontier, final int[] _idxFridxPt);

  /**
   * @param _idxGlobalOnFrontiers l'indice frontiere du point
   * @return the general index from the index on the frontier. L'indice general du point dont l'indice frontiere est
   *         <code>_idxGlobalOnFrontiers</code>.
   */
  int getIdxGlobalFrom(final int _idxGlobalOnFrontiers);

  /**
   * @param _idxFr l'indice de la frontiere
   * @return l'iterateur sur cette frontiere
   */
  FrontierIterator getFrontierIterator(final int _idxFr);

  /**
   * @param _idxFr l'indice de la frontiere
   * @return l'iterateur sur cette frontiere
   * @param _firstIdx le premiere indice a prendre en compte
   */
  FrontierIterator getFrontierIterator(final int _idxFr, final int _firstIdx);

  /**
   * @return le nombre total de points des frontieres.
   */
  int getNbTotalPt();

  /**
   * Permet de verifier si les indices de bords sont dans le meme ordre que celui donne par _ipobo.
   * 
   * @param _ipobo les indices des points frontieres a comparer
   * @return si meme ordre des points frontieres
   */
  boolean isSame(final int[] _ipobo);

  /**
   * @return les bords sous forme d'un tableau ( conforme tableau ipobo de telemac/serafin).
   */
  int[] getArray();

  int[] getIpobo(int _nbPt);

  /**
   * Attention peut etre tres long !
   * 
   * @return une chaine de caractere decrivant les indices du bord principal
   * @see org.fudaa.ctulu.CtuluLibString#getObjectInString(Object, boolean)
   */
  String getDescBordPrinc();

  /**
   * @param _id la frontiere voulue
   * @return une chaine de caractere decrivant les indices du bord interne d'indice _id
   * @see org.fudaa.ctulu.CtuluLibString#getObjectInString(Object, boolean)
   */
  String getDescBordInterne(final int _id);

  @Override
  String toString();

  /**
   * Fonction de debogage: ecrit tous les indices des frontiere.
   */
  void printFullString();

  /**
   * @param _mail le maillage contenant cette frontiere
   * @param _idxFrontier la frontiere a considerer
   * @param _dest l'envelope a modifier: ne l'initialise pas !
   */
  void getMinMax(final EfGridInterface _mail, final int _idxFrontier, final Envelope _dest);

  /**
   * @return le nombre de frontiere interne (total -1)
   */
  int getNbFrontierIntern();

  /**
   * @return le nombre total de frontiere
   */
  int getNbFrontier();

  /**
   * Pas de verification !
   * 
   * @param _idxOnFirstFrontier l'indiece sur la premiere frontiere
   * @return l'index du point n�<code>_idx</code> sur la fronti�re principale (externe)
   */
  int getIdxGlobalPrinc(final int _idxOnFirstFrontier);

  /**
   * @param _idxFrontier l'indice de la frontiere
   * @param _idxPt l'indice du point sur la frontiere
   * @return l'indice global du point n� _idxPt sur la frontier _idxFrontier
   */
  int getIdxGlobal(final int _idxFrontier, final int _idxPt);

  void fillWithGobalIdx(final int _idxFr, final TIntHashSet _s);

  /**
   * @param _idxFrontier l'indice de la frontiere
   * @return le nombre de point contenu par la frontiere _idxFrontier
   */
  int getNbPt(final int _idxFrontier);

}
