/*
 * @creation 22 janv. 07
 * @modification $Date: 2007-06-11 13:08:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.decorator;

import java.io.IOException;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridTranslate;

/**
 * @author fred deniger
 * @version $Id: MvExportDecoratorTranslation.java,v 1.3 2007-06-11 13:08:15 deniger Exp $
 */
public class EfGridDataTranslateDecorator implements EfGridData {

  final double tx_;
  final double ty_;
  final EfGridData init_;
  final EfGridInterface newTranslated_;

  public EfGridDataTranslateDecorator(final double _tx, final double _ty, final EfGridData _init) {
    super();
    tx_ = _tx;
    ty_ = _ty;
    init_ = _init;
    newTranslated_ = new EfGridTranslate(init_.getGrid(), tx_, ty_);
  }

  @Override
  public EfGridInterface getGrid() {
    return newTranslated_;
  }

  @Override
  public double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    return init_.getData(_o, _timeIdx, _idxObjet);
  }

  @Override
  public EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    return init_.getData(_o, _timeIdx);
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return init_.isDefined(_var);
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    return init_.isElementVar(_idxVar);
  }

}
