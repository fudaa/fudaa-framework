/*
 * @creation 25 juin 2003
 *
 * @modification $Date: 2007-05-22 13:11:26 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;
import gnu.trove.*;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.gis.*;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.operation.EfIndexerActivity;
import org.nfunk.jep.Variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

/**
 * Un maillage contenant des elements donnes dans le meme sens. Ce maillage peut servir de reference pour une interpolation (voir
 * l'interface EfInteropationGridRefI). Dans ce l'interpolation se fait sur la hauteur en chaque point (voir la methode getV(int).
 * Ce maillage peut servir egalement comme destination d'une interpolation (voir l'interface EfInterpolationTarget)
 *
 * @author deniger
 * @version $Id: EfGridDefaultAbstract.java,v 1.6 2007-05-22 13:11:26 deniger Exp $
 * @see org.fudaa.dodico.h2d.TestJMaillage
 * @see org.fudaa.dodico.telemac.TestJSerafin
 */
public abstract class EfGridDefaultAbstract implements GISPointContainerInterface, EfGridInterface {
  /**
   * @return
   */
  private static String getRechercheDesc() {
    return DodicoLib.getS("Recherche des fronti�res");
  }

  /**
   * Permet de cree un maillage a partir des points, elements et des indices des points frontiere arrang� selon l'ordre t�l�mac :
   * numerotation partant du point x+y min et x min et dans le sens trigo pour la frontiere externe et horaire pour les frontieres
   * internes.Il est suppose que le tableau _ipobo soit correct.
   *
   * @param _mail le maillage a considerer
   * @param _ipobo les indices ipobo ( convention telemac)
   * @param _pr la barre de progression
   */
  public static void computeBord(final EfGridDefaultAbstract _mail, final int[] _ipobo, final ProgressionInterface _pr) {
    if (_pr != null) {
      _pr.setDesc(getRechercheDesc());
      _pr.setProgression(0);
    }
    // Le vecteur stockant les bords.
    final List l = new ArrayList(5);
    final int n = _ipobo.length;
    final TIntArrayList bordEncours = new TIntArrayList(n);
    int s1, s2 = -1;
    final ProgressionUpdater up = new ProgressionUpdater(_pr);
    up.setValue(9, n, 0, 90);
    for (int i = 1; i < n; i++) {
      // le sens est important
      s1 = _ipobo[i - 1];
      s2 = _ipobo[i];
      bordEncours.add(s1);
      if (!EfElement.containsSegmentSensIndif(_mail, s1, s2)) {
        l.add(bordEncours.toNativeArray());
        bordEncours.clear();
      }
      up.majAvancement();
    }
    // finition
    if (s2 >= 0) {
      bordEncours.add(s2);
    }
    l.add(bordEncours.toNativeArray());
    if (_pr != null) {
      _pr.setProgression(90);
    }
    _mail.ptsFrontiere_ = new EfFrontier(l, EfFrontier.getExterneFr(_mail, l, _pr));
  }

  /**
   * @param _grid le maillage
   * @return modele pour le z.
   */
  public static CtuluCollectionDouble getBathyModel(final EfGridInterface _grid) {
    return new CtuluCollectionDoubleAbstract() {
      @Override
      public Double getCommonValue(final int[] _i) {
        return CtuluCollectionDoubleAbstract.getCommonValue(this, _i);
      }

      @Override
      public double getMax() {
        return _grid.getMaxZ();
      }

      @Override
      public double getMin() {
        return _grid.getMinZ();
      }

      @Override
      public int getSize() {
        return _grid.getPtsNb();
      }

      @Override
      public double getValue(final int _i) {
        return _grid.getPtZ(_i);
      }

      @Override
      public double[] getValues() {
        return CtuluCollectionDoubleAbstract.getArray(this);
      }
    };
  }

  /**
   * @param _grid le maillage
   * @return modele pour le z.
   */
  public static CtuluCollectionDouble getElementBathyModel(final EfGridInterface _grid) {
    return new CtuluCollectionDoubleAbstract() {
      @Override
      public double getMax() {
        return _grid.getMaxZ();
      }

      @Override
      public double getMin() {
        return _grid.getMinZ();
      }

      @Override
      public int getSize() {
        return _grid.getEltNb();
      }

      @Override
      public double getValue(final int _i) {
        return _grid.getCentreZElement(_i);
      }
    };
  }

  public static void verifFrontier(final EfGridInterface _g) {
    final EfFrontierInterface fr = _g.getFrontiers();
    if (fr == null) {
      return;
    }
    for (int i = 0; i < fr.getNbFrontier(); i++) {
      final int idx = _g.getSOPointIndex(fr, i);
      if (idx != fr.getIdxGlobal(i, 0)) {
        FuLog.error("erreur pour le bord " + (i + 1));
      }
      FuLog.error("point attendu " + (idx + 1) + " point eu " + (fr.getIdxGlobal(i, 0) + 1));
    }
  }

  EfIndexInterface index_;
  EfNeighborMesh neighbors_;

  @Override
  public void computeNeighbord(ProgressionInterface _inter, CtuluAnalyze _analyze) {
    neighbors_ = EfNeighborMesh.compute(this, _inter);
  }

  @Override
  public EfNeighborMesh getNeighbors() {
    if (neighbors_ == null) {
      computeNeighbord(null, null);
    }
    return neighbors_;
  }

  private Envelope envelopExtern_;
  protected BitSet isEltExtremite_;
  protected EfFrontierInterface ptsFrontiere_;
  protected EfElementType typeElt_;

  protected void initEnvelopFrom(EfGridDefaultAbstract other) {
    if (other != null && other.envelopExtern_ != null && other.elementsEnvelope != null) {
      this.envelopExtern_ = other.envelopExtern_;
      this.elementsEnvelope = other.elementsEnvelope;
    }
  }

  public EfGridDefaultAbstract() {
  }

  private int getSOPointIndex(final TIntIntHashMap _l) {
    if (_l.isEmpty()) {
      FuLog.error("no point");
      return -1;
    }
    final TIntIntIterator it = _l.iterator();
    int r = -1;
    double xPyMin = Double.MAX_VALUE;
    double yMin = Double.MAX_VALUE;
    final Coordinate pt = new Coordinate();
    for (int i = _l.size(); i-- > 0; ) {
      it.advance();
      // -1 car les indices ont ete augment� de 1 ...
      final int temp = it.key() - 1;
      getPt(temp, pt);
      final double xPyTemp = pt.x + pt.y;
      if (xPyTemp < xPyMin) {
        r = temp;
        xPyMin = xPyTemp;
        yMin = pt.y;
      } else if (xPyTemp == xPyMin) {
        if (pt.y < yMin) {
          r = temp;
          yMin = pt.y;
        }
      }
    }
    return r + 1;
  }

  protected void clearIndex() {
    index_ = null;
  }

  protected final void computeMiddle() {
    int idxDone = 0;
    final int nbPt = getPtsNb();
    final BitSet done = new BitSet(nbPt);
    isEltExtremite_ = new BitSet(nbPt);
    int temp;
    boolean isIdxExtrem;
    for (int i = getEltNb() - 1; (i >= 0) && (idxDone < nbPt); i--) {
      final EfElement elt = getElement(i);
      isIdxExtrem = false;
      for (int j = elt.getPtNb() - 1; j >= 0; j--) {
        temp = elt.getPtIndex(j);
        if (!done.get(temp)) {
          idxDone++;
          done.set(temp);
          isEltExtremite_.set(temp, isIdxExtrem);
        }
        isIdxExtrem = !isIdxExtrem;
      }
    }
  }

  /**
   * the XY data changed. The structure is the same.
   */
  protected final void geomXYChanged() {
    envelopExtern_ = null;
    if (ptsFrontiere_ != null) {
      ptsFrontiere_.clearSaveData();
    }
    clearIndex();
    elementsEnvelope = null;
  }

  protected final void restoreForm(final TIntDoubleHashMap _map) {
    final TIntDoubleIterator iterator = _map.iterator();
    for (int i = _map.size(); i-- > 0; ) {
      iterator.advance();
      setZIntern(iterator.key(), iterator.value());
    }
  }

  protected CtuluCommand setZ(final int _i, final double _newV) {
    final double old = getPtZ(_i);
    if (setZIntern(_i, _newV)) {
      return new CtuluCommand() {
        @Override
        public void redo() {
          setZIntern(_i, _newV);
        }

        @Override
        public void undo() {
          setZIntern(_i, old);
        }
      };
    }
    return null;
  }

  protected CtuluCommand setZ(final int[] _i, final CtuluParser _newV) {
    if (_i == null || _newV == null) {
      return null;
    }
    final Variable[] vs = _newV.findUsedVar();
    if (vs == null || vs.length == 0) {
      return setZ(_i, _newV.getValue());
    }
    final CtuluParser parser = new CtuluParser(_newV);
    final Variable oldVar = parser.getVar(CtuluParser.getOldVariable());
    final TIntDoubleHashMap map = new TIntDoubleHashMap();
    for (int i = _i.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      final double old = getPtZ(idx);
      if (oldVar != null) {
        oldVar.setValue(CtuluLib.getDouble(old));
      }
      if (setZIntern(idx, parser.getValue())) {
        map.put(idx, old);
      }
    }
    if (map.size() > 0) {
      return new CtuluCommand() {
        @Override
        public void redo() {
          setZ(_i, parser);
        }

        @Override
        public void undo() {
          restoreForm(map);
        }
      };
    }
    return null;
  }

  protected CtuluCommand setZ(final int[] _idxs, final double _newV) {
    if (_idxs == null) {
      return null;
    }
    final TIntDoubleHashMap map = new TIntDoubleHashMap();
    for (int i = _idxs.length - 1; i >= 0; i--) {
      final int idx = _idxs[i];
      final double old = getPtZ(idx);
      if (setZIntern(idx, _newV)) {
        map.put(idx, old);
      }
    }
    if (map.size() > 0) {
      return new CtuluCommand() {
        @Override
        public void redo() {
          setZ(_idxs, _newV);
        }

        @Override
        public void undo() {
          restoreForm(map);
        }
      };
    }
    return null;
  }

  protected CtuluCommand setZ(final int[] _i, final double[] _newV) {
    if (_i == null || _newV == null || _newV.length != _i.length) {
      return null;
    }
    final TIntDoubleHashMap map = new TIntDoubleHashMap();
    for (int i = _i.length - 1; i >= 0; i--) {
      final int idx = _i[i];
      final double old = getPtZ(idx);
      if (setZIntern(idx, _newV[i])) {
        map.put(idx, old);
      }
    }
    if (map.size() > 0) {
      return new CtuluCommand() {
        @Override
        public void redo() {
          setZ(_i, _newV);
        }

        @Override
        public void undo() {
          restoreForm(map);
        }
      };
    }
    return null;
  }

  protected abstract boolean setZIntern(int _i, double _newV);

  /**
   * Permet de trouver les bords du maillage en respectant les convention de Telemac. On commence par le bord exterieur par le
   * point SO (dans le sens trigo) puis on finit par les iles dans le sens horaire.La procedure est effectu�e que si les bords
   * n'ont pas ete deja determines ATTENTION : cette methode suppose que tous les �l�ments sont orient�s dans le meme sens
   *
   * @param _inter la barre de progression
   */
  @Override
  public final void computeBord(final ProgressionInterface _inter, final CtuluAnalyze _analyze) {
    if (ptsFrontiere_ != null) {
      return;
    }
    EfLib.orienteGrid(this, _inter, true, _analyze);
    final int nbPt = getPtsNb();
    final TIntObjectHashMap segments = new TIntObjectHashMap(nbPt);
    if (_inter != null) {
      _inter.setDesc(getRechercheDesc());
      _inter.setProgression(0);
    }
    int nbFrontiere = 0;
    if (getEltNb() == 0) {
      return;
    }
    for (int i = getEltNb() - 1; i >= 0; i--) {
      final EfElement efElement = getElement(i);
      final int ptNb = efElement.getPtNb();
      for (int j = ptNb; j > 0; j--) {
        final int i2 = efElement.getPtIndex(j - 1);
        final int i1 = efElement.getPtIndex(j == ptNb ? 0 : j);

        final TIntHashSet set = (TIntHashSet) segments.get(i2);
        // le segment n'est pas encore ajoute
        if (set == null || !set.contains(i1)) {
          TIntHashSet seti1 = (TIntHashSet) segments.get(i1);
          if (seti1 == null) {
            seti1 = new TIntHashSet(10);
            segments.put(i1, seti1);
          }

          seti1.add(i2);
          nbFrontiere++;
        } else {
          set.remove(i1);
          nbFrontiere--;
        }
      }
    }
    // maj avancement
    if (_inter != null) {
      _inter.setProgression(50);
      _inter.setDesc(DodicoLib.getS("Classement des fronti�res"));
    }
    final TIntIntHashMap seg = new TIntIntHashMap(nbFrontiere);
    final TIntObjectIterator it = segments.iterator();
    for (int i = segments.size(); i-- > 0; ) {
      it.advance();
      final TIntHashSet set = (TIntHashSet) it.value();
      if (set != null && set.size() == 1) {
        seg.put(it.key() + 1, set.iterator().next() + 1);
      }
    }
    int i1 = getSOPointIndex(seg);
    int i2 = i1;
    final TIntArrayList listMoinsUn = new TIntArrayList(nbFrontiere * 2);
    listMoinsUn.add(i1 - 1);
    final List bordFinal = new ArrayList(10);
    for (int i = seg.size(); i > 0; i--) {
      final int oldi2 = i2;
      i2 = seg.remove(i2);
      if (i2 == 0) {
        final String err = DodicoLib.getS("Erreur lors de la g�n�ration de la fronti�re. Le noeud {0} n'a pas de voisin ou il appartient � plusieurs fronti�res",
            CtuluLibString.getString(oldi2));
        if (_analyze == null) {
          FuLog.error("DEF: " + err);
        } else {
          _analyze.addError(err, oldi2);
        }
        return;
      }
      if (i2 == i1) {
        bordFinal.add(listMoinsUn.toNativeArray());
        listMoinsUn.resetQuick();
        if (seg.size() > 0) {
          i1 = getSOPointIndex(seg);
          listMoinsUn.add(i1 - 1);
          i2 = i1;
        }
      } else {
        listMoinsUn.add(i2 - 1);
      }
    }
    // maj avancement
    if (_inter != null) {
      _inter.setProgression(80);
      _inter.setDesc(DodicoLib.getS("Orientation des fronti�res"));
    }
    for (int i = bordFinal.size() - 1; i >= 0; i--) {
      // mise dans le bon sens.
      final int[] temp = (int[]) bordFinal.get(i);
      final int tempI = EfElement.getIdxSegmentContains(this, temp[0], temp[1]);
      if (tempI < 0) {
        throw new IllegalStateException("element for SW point not found");
      }
      final int tempInt = this.isElementTrigoOriente(tempI) * getElement(tempI).isSegmentInEltOrient(temp[0], temp[1]);

      if (tempInt == 0) {
        FuLog.warning("plane element or unknown points");
      }
      // Si le point est dans le sens horaire on inverse.
      // le bord externe doit etre dans le sens trigo
      // et les bord interne dans le sens horaire.
      if (tempInt < 0) {
        CtuluLibArray.invert(temp, 1);
      }
    }
    ptsFrontiere_ = new EfFrontier(bordFinal, EfFrontier.getExterneFr(this, bordFinal, _inter));
  }

  /**
   * Determine the boundary thanks to the ipobo array ( faster).
   *
   * @param _ipobo les indices de bords selon la convention telemac
   * @param _pr la barre de progression
   */
  @Override
  public final void computeBordFast(final int[] _ipobo, final ProgressionInterface _pr, CtuluAnalyze _an) {
    if (ptsFrontiere_ != null) {
      return;
    }
    if (_pr != null) {
      _pr.setDesc(getRechercheDesc());
      _pr.setProgression(0);
    }
    // Le vecteur stockant les bords.
    final int n = _ipobo.length;
    final int nseg = n - 1;
    final int maxSegIdx = nseg - 1;
    // We are looking for existing segments in the grid.
    // we build boundary segment from the ipobo array.
    // if a segment is found one time in the grid, it is a boundary
    // segment
    final EfFoundSegment[] seg = new EfFoundSegment[nseg];
    for (int i = maxSegIdx; i >= 0; i--) {
      seg[i] = new EfFoundSegment(_ipobo[i], _ipobo[i + 1], true);
      seg[i].index_ = i;
    }
    // on trie pour accelerer la recherche suivante.
    Arrays.sort(seg);
    final int pourc = 10;
    if (_pr != null) {
      _pr.setProgression(pourc);
    }
    final int nbEle = getEltNb();
    final ProgressionUpdater up = new ProgressionUpdater(_pr);
    up.setValue(4, nbEle, 0, 80);
    EfElement ele;
    EfFoundSegment sFound = new EfFoundSegment(0, 0, false);
    for (int i = nbEle - 1; i >= 0; i--) {
      ele = getElement(i);
      for (int j = ele.getNbEdge() - 1; j >= 0; j--) {
        sFound.setMinMaxIdx(ele.getEdgePt1(j), ele.getEdgePt2(j));
        int indexFound = Arrays.binarySearch(seg, sFound);
        if (indexFound >= 0) {
          // la premiere fois, il est marque comme segment frontiere: la deuxieme fois, il sera interne
          seg[indexFound].boundarySegment_ = !seg[indexFound].boundarySegment_;
        }
      }
      up.majAvancement();
    }
    final List l = new ArrayList(6);
    final TIntArrayList lint = new TIntArrayList(10);
    for (int i = maxSegIdx; i >= 0; i--) {
      if (!seg[i].boundarySegment_) {
        lint.add(seg[i].index_);
      }
    }
    if (lint.size() > 0) {
      final int[] idx = lint.toNativeArray();
      Arrays.sort(idx);
      int maxEleIdx = idx.length;
      int[] bord = new int[idx[0] + 1];
      System.arraycopy(_ipobo, 0, bord, 0, bord.length);
      l.add(bord);
      for (int i = 1; i < maxEleIdx; i++) {
        bord = new int[idx[i] - idx[i - 1]];
        System.arraycopy(_ipobo, idx[i - 1] + 1, bord, 0, bord.length);
        l.add(bord);
      }
      bord = new int[_ipobo.length - idx[maxEleIdx - 1] - 1];
      System.arraycopy(_ipobo, idx[maxEleIdx - 1] + 1, bord, 0, bord.length);
      l.add(bord);
      // un bord ne contient que 2 points: impossible
      if (bord.length < 3) {
        if (_an != null) {
          _an.addFatalError(DodicoLib.getS("La d�finition des noeuds fronti�res ou des conditions limites est fausse."));
        }
        return;
      }
      ptsFrontiere_ = new EfFrontier(l, EfFrontier.getExterneFr(this, l, _pr));
    } else {
      ptsFrontiere_ = new EfFrontier(_ipobo);
    }
  }

  private Envelope[] elementsEnvelope;

  /**
   * Recherche du point max (xmax,ymax,zmax).
   */
  public final void computeEnvelope() {

    if (envelopExtern_ != null && elementsEnvelope != null) {
      return;
    }
    envelopExtern_ = new Envelope();
    if (ptsFrontiere_ == null) {
      for (int i = getPtsNb() - 1; i >= 0; i--) {
        envelopExtern_.expandToInclude(getPtX(i), getPtY(i));
      }
    } else {
      for (int i = ptsFrontiere_.getNbFrontier() - 1; i >= 0; i--) {
        if (ptsFrontiere_.isExtern(i)) {
          ptsFrontiere_.getMinMax(this, i, envelopExtern_);
        }
      }
    }
    elementsEnvelope = new Envelope[getEltNb()];
    for (int i = 0; i < elementsEnvelope.length; i++) {
      Envelope envelope = new Envelope();
      EfElement elt = getElement(i);
      int nbPt = elt.getPtNb();
      for (int j = 0; j < nbPt; j++) {
        int idx = elt.getPtIndex(j);
        envelope.expandToInclude(getPtX(idx), getPtY(idx));
      }
      elementsEnvelope[i] = envelope;
    }
  }

  @Override
  public Envelope getEnvelopeForElement(int idxElt) {
    if (elementsEnvelope == null) {
      computeEnvelope();
    }
    return elementsEnvelope[idxElt];
  }

  /**
   * @return true si la frontiere externe contient le point (x,y).
   */
  @Override
  public final boolean contientPoint(final double _x, final double _y) {
    computeEnvelope();
    // on test si le point est en dehors de la "boite" du maillage
    // si les points extremites sont connus
    // test rapides
    if (!envelopExtern_.contains(_x, _y)) {
      return false;
    }
    if (ptsFrontiere_ == null) {
      computeBord(null, null);
      if (ptsFrontiere_ == null) {
        return false;
      }
    }
    final int nb = getFrontiers().getNbPt(0);

    if (nb <= 2) {
      return false;
    }
    int recu = 0;
    int p = getPointOnFrontierExtern(nb - 1);
    double finx = getPtX(p);
    double finy = getPtY(p);
    double xi, yi;
    for (int i = 0; i < nb; finx = xi, finy = yi, i++) {
      if ((finx == _x) && (finy == _y)) {
        return true;
      }
      p = getPointOnFrontierExtern(i);
      xi = getPtX(p);
      yi = getPtY(p);
      if (yi == finy) {
        continue;
      }
      double lx;
      if (xi < finx) {
        if (_x >= finx) {
          continue;
        }
        lx = xi;
      } else {
        if (_x >= xi) {
          continue;
        }
        lx = finx;
      }
      double test1, test2;
      if (yi < finy) {
        if (_y < yi || _y >= finy) {
          continue;
        }
        if (_x < lx) {
          recu++;
          continue;
        }
        test1 = _x - xi;
        test2 = _y - yi;
      } else {
        if (_y < finy || _y >= yi) {
          continue;
        }
        if (_x < lx) {
          recu++;
          continue;
        }
        test1 = _x - finx;
        test2 = _y - finy;
      }
      if (test1 < (test2 / (finy - yi) * (finx - xi))) {
        recu++;
      }
    }
    return ((recu & 1) != 0);
  }

  /**
   * Test sur x,y uniquement.
   *
   * @param _p le point a tester
   * @return si le point x,y est contenu dans ce maillage
   */
  @Override
  public final boolean contientPoint(final EfNode _p) {
    return contientPoint(_p.getX(), _p.getY());
  }

  @Override
  public void createIndexRegular(final ProgressionInterface _prog) {
    if (index_ == null) {
      index_ = new EfIndexerActivity(this).createRegularIndex(_prog);
    }
    computeEnvelope();
  }

  /**
   * Remplit la liste donnee en parametre avec les coordonnees de points.
   */
  @Override
  public final boolean fillListWithPoint(final GISCollectionPointInteface _listToFill) {
    if (_listToFill == null) {
      return false;
    }
    for (int i = 0; i < getPtsNb(); i++) {
      _listToFill.add(getPtX(i), getPtY(i), getPtZ(i));
    }
    return true;
  }

  /**
   * @param _eltId l'element a considerer
   * @return l'aire de l'element d'indice _eltId
   */
  @Override
  public final double getAire(final int _eltId) {
    double sum = 0.0;
    final EfElement elt = getElement(_eltId);
    final int ptNb = elt.getPtNb();
    for (int i = 0; i < ptNb; i++) {
      final int ptIndex = elt.getPtIndex(i);
      final double bx = getPtX(ptIndex);
      final double by = getPtY(ptIndex);
      final int ptIndex2 = elt.getPtIndex((i + 1) % ptNb);
      final double cx = getPtX(ptIndex2);
      final double cy = getPtY(ptIndex2);
      sum += (bx + cx) * (cy - by);
    }
    return Math.abs(sum / 2D);
  }

  @Override
  public final void getCentreElement(final int _idxElt, final Coordinate _m) {
    getElement(_idxElt).getMilieuXY(this, _m);
  }

  /**
   * @param _idxElt l'indice de l'element
   * @return le x du centre
   */
  @Override
  public final double getCentreXElement(final int _idxElt) {
    return getElement(_idxElt).getMilieuX(this);
  }

  /**
   * @param _idxElt l'indice de l'element
   * @return le x du centre
   */
  @Override
  public final double getCentreYElement(final int _idxElt) {
    return getElement(_idxElt).getMilieuY(this);
  }

  /**
   * @param _idxElt l'indice de l'element
   * @return le x du centre
   */
  @Override
  public final double getCentreZElement(final int _idxElt) {
    return getElement(_idxElt).getMilieuZ(this);
  }

  @Override
  public final Coordinate getCoor(final int _i) {
    final Coordinate r = new Coordinate();
    getPt(_i, r);
    return r;
  }

  @Override
  public final void getCoord(final int _i, final Coordinate _c) {
    _c.x = getPtX(_i);
    _c.y = getPtY(_i);
    _c.z = getPtZ(_i);
  }

  @Override
  public final double getDistanceFromFrontier(final double _x, final double _y) {
    if (ptsFrontiere_ == null) {
      computeBord(null, null);
      if (ptsFrontiere_ == null) {
        return Double.MAX_VALUE;
      }
    }
    final int n = ptsFrontiere_.getNbPt(0) - 1;
    int i1 = getPointOnFrontierExtern(0);
    int i2 = getPointOnFrontierExtern(n);
    double dmin = CtuluLibGeometrie.distanceFromSegment(getPtX(i1), getPtY(i1), getPtX(i2), getPtY(i2), _x, _y);
    if (dmin == 0) {
      return 0;
    }
    double d;
    for (int i = n; i > 0; i--) {
      i1 = getPointOnFrontierExtern(i - 1);
      i2 = getPointOnFrontierExtern(i);
      d = CtuluLibGeometrie.distanceFromSegment(getPtX(i1), getPtY(i1), getPtX(i2), getPtY(i2), _x, _y);
      if (d == 0) {
        return 0;
      }
      if (d < dmin) {
        dmin = d;
      }
    }
    return dmin;
  }

  /**
   * Recherche parmis les elements ayant leur indice compris dans le tableau _segmentIdxList celui contenant le point d'indice
   * _ptIdx. La recherche est effectuee depuis la fin du tableau.
   *
   * @param _ptIdx l'indice recherche
   * @param _eltIdxList les indices des elements a parcourir
   * @return -1 si aucun element contient le point d'indice _ptIdx.
   */
  @Override
  public final int getElementIdxContainingPtIdx(final int _ptIdx, final int[] _eltIdxList) {
    final int n = _eltIdxList.length - 1;
    int t;
    for (int i = n; i >= 0; i--) {
      t = _eltIdxList[i];
      if (getElement(t).containsIndex(_ptIdx)) {
        return t;
      }
    }
    return -1;
  }

  @Override
  public final LinearRing getElementRing(final int _i) {
    final EfElement el = getElement(_i);
    final Coordinate[] cs = new Coordinate[el.getPtNb() + 1];
    for (int i = cs.length - 2; i >= 0; i--) {
      final int idx = el.getPtIndex(i);
      cs[i] = new Coordinate(getPtX(idx), getPtY(idx), getPtZ(idx));
    }
    // meme point pour fermer le poygone.
    cs[cs.length - 1] = cs[0];
    return GISGeometryFactory.INSTANCE.createLinearRingImmutable(cs);
  }

  /**
   * Permet de retrouver l'element contenant le point (_x,_y) et dont le milieu est le plus pres.
   *
   * @param _x le x a tester
   * @param _y le y a tester
   * @param _m un point mutable utilise pour calcul le centre de l'element au cas ou le point soit contenu par plusieurs elements
   * @return l'element contenant le point _x, _y. -1 si aucun
   */
  @Override
  public final int getEltContainingXY(final double _x, final double _y, final Coordinate _m) {
    double d = -1;
    int idx = -1;
    final Coordinate m = _m == null ? new Coordinate() : _m;
    for (int i = getEltNb() - 1; i >= 0; i--) {
      // le point est contenu dans un element
      if (getEnvelopeForElement(i).contains(_x, _y) && getElement(i).contientXY(_x, _y, this)) {
        // premiere initialisation
        if (idx < 0) {
          idx = i;
        } // le point appartient a plusieurs elements: on va prendre l'element dont le milieu est
        // le plus pr�s
        else {
          // la distance min n'est pas encore calculee
          if (d < 0) {
            getElement(idx).getMilieuXY(this, m);
            d = CtuluLibGeometrie.getD2(_x, _y, m.x, m.y);
          }
          getElement(i).getMilieuXY(this, m);
          final double d2 = CtuluLibGeometrie.getD2(_x, _y, m.x, m.y);
          // si le milieu du nouvel element est plus pres
          if (d2 < d) {
            d = d2;
            idx = i;
          }
        }
      }
    }
    return idx;
  }

  /**
   * @param _s l'indice recherche
   * @return Renvoie tous les elements contenant l'indice _s.
   */
  @Override
  public final int[] getEltIdxContainsPtIdx(final int _s) {
    return EfElement.getIndexElementContainsPtIdx(this, _s);
  }

  /**
   * @param _minAngleInDegre l'angle min en degre
   * @param _progress la barre de progression
   * @return les indices des elements ayant un angle inf a
   *     <code>_minAngleInDegre</code>
   */
  @Override
  public final int[] getEltIdxWithAngleLessThan(final double _minAngleInDegre, final ProgressionInterface _progress) {
    final boolean t6 = (getEltType() == EfElementType.T6);
    final int nbElt = getEltNb();
    final TIntArrayList r = new TIntArrayList(nbElt);
    // Mise en place de la progression
    final ProgressionUpdater prog = new ProgressionUpdater(_progress);
    prog.setValue(4, nbElt);
    final double radianMin = _minAngleInDegre * Math.PI / 180;
    prog.majProgessionStateOnly(DodicoLib.getS("Recherche des �l�ments plats"));
    if (t6) {
      for (int i = nbElt - 1; i >= 0; i--) {
        if (getElement(i).hasAngleLessThanT6(this, radianMin)) {
          r.add(i);
        }
        prog.majAvancement();
      }
    } else {
      for (int i = nbElt - 1; i >= 0; i--) {
        if (getElement(i).hasAngleLessThan(this, radianMin)) {
          r.add(i);
        }
        prog.majAvancement();
      }
    }
    return r.isEmpty() ? null : r.toNativeArray();
  }

  @Override
  public EfElement[] getElts() {
    final EfElement[] rf = new EfElement[getEltNb()];
    for (int i = rf.length - 1; i >= 0; i--) {
      rf[i] = getElement(i);
    }
    return rf;
  }

  /**
   * @return le type commun. null si heterogene
   */
  @Override
  public EfElementType getEltType() {
    if (typeElt_ == null) {
      typeElt_ = EfElement.getCommunType(this);
    }
    return typeElt_;
  }

  @Override
  public final Envelope getEnvelope(final Envelope _dest) {
    computeEnvelope();
    if (_dest == null) {
      return new Envelope(envelopExtern_);
    }
    _dest.init(envelopExtern_);
    return _dest;
  }

  @Override
  public final GISPolygone[] getExtRings() {
    return ptsFrontiere_ == null ? null : ptsFrontiere_.getExternRing(this);
  }

  @Override
  public final GISPolygone getFrontierRing(final int _frIdx) {
    if (ptsFrontiere_ == null) {
      return null;
    }
    return ptsFrontiere_.getLinearRing(this, _frIdx);
  }

  /**
   * @return la frontiere
   */
  @Override
  public final EfFrontierInterface getFrontiers() {
    return ptsFrontiere_;
  }

  /**
   * Utilser lorque ce maillage sert de reference pour une interpolation sur un maillage.
   */
  public final EfGridInterface getGrid() {
    return this;
  }

  @Override
  public EfIndexInterface getIndex() {
    return index_;
  }

  @Override
  public final GISPolygone getLinearRing(final int[] _idx) {
    if (_idx == null) {
      return null;
    }
    final Coordinate[] cs = new Coordinate[_idx.length + 1];
    for (int i = cs.length - 2; i >= 0; i--) {
      final int idx = _idx[i];
      cs[i] = new Coordinate(getPtX(idx), getPtY(idx));
    }
    // meme point pour fermer le poygone.
    cs[cs.length - 1] = cs[0];
    return (GISPolygone) GISGeometryFactory.INSTANCE.createLinearRingImmutable(cs);
  }

  /**
   * @return le type commun. null si heterogene
   */
  @Override
  public int getMaxPointInElements() {
    return EfElement.getMaxPointInElements(this);
  }

  @Override
  public final double getMaxX() {
    computeEnvelope();
    return envelopExtern_.getMaxX();
  }

  @Override
  public final double getMaxY() {
    computeEnvelope();
    return envelopExtern_.getMaxY();
  }

  @Override
  public final double getMaxZ() {
    double r = getPtZ(0);
    for (int i = getPtsNb() - 1; i > 0; i--) {
      final double ri = getPtZ(i);
      if (ri > r) {
        r = ri;
      }
    }
    return r;
  }

  /**
   * @return pour les maillages T6, les points entourant les point milieux. Il n'y a pas d'ordre dans les deux tables.
   */
  @Override
  public TIntIntHashMap[] getMiddleBounds() {
    if (getEltType() != EfElementType.T6) {
      return null;
    }
    final TIntIntHashMap[] res = new TIntIntHashMap[2];
    final int nbPt = getPtsNb();
    res[0] = new TIntIntHashMap(nbPt / 2);
    res[1] = new TIntIntHashMap(nbPt / 2);

    // le nombre de noeuds traites. Evite de retester les meme noeuds.
    int idxDone = 0;
    // sauvegarde permettant de savoir quel noeud a ete traite.
    final BitSet done = new BitSet(nbPt);

    for (int i = getEltNb() - 1; (i >= 0) && (idxDone < nbPt); i--) {
      final EfElement elt = getElement(i);
      boolean isIdxExtrem = false;
      final int ptNb = elt.getPtNb();
      for (int j = ptNb - 1; j >= 0; j--) {
        final int temp = elt.getPtIndex(j);
        if (!done.get(temp)) {
          idxDone++;
          done.set(temp);
          if (!isIdxExtrem) {
            res[0].put(temp, elt.getPtIndex((j + 1) % ptNb));
            // j est strictement superieur a 0
            res[1].put(temp, elt.getPtIndex(j - 1));
          }
        }
        isIdxExtrem = !isIdxExtrem;
      }
    }
    return res;
  }

  /**
   * @param _ptIndex1 l'indice du premier point
   * @param _ptIndex2 l'indice du 2eme point
   * @return le point milieu
   */
  @Override
  public final EfNode getMilieu(final int _ptIndex1, final int _ptIndex2) {
    return EfNode.createMilieu(getPtX(_ptIndex1), getPtY(_ptIndex1), getPtZ(_ptIndex1), getPtX(_ptIndex2), getPtY(_ptIndex2),
        getPtZ(_ptIndex2));
  }

  /**
   * @param _eltIdx l'indice de l'element
   * @return l'angle en radian min de cet elt
   */
  @Override
  public final double getMinAngle(final int _eltIdx) {
    return getElement(_eltIdx).getMinAngle(this);
  }

  /**
   * @param _eltIdx l'indice de l'element
   * @return l'angle en DEGRE min de cet elt
   */
  @Override
  public final double getMinAngleDegre(final int _eltIdx) {
    return getElement(_eltIdx).getMinAngleDegre(this);
  }

  /**
   * @param _indexBord l'indice de la frontiere
   */
  @Override
  public final void getMinMaxPointFrontiere(final int _indexBord, final Envelope _dest) {
    ptsFrontiere_.getMinMax(this, _indexBord, _dest);
  }

  @Override
  public final double getMinX() {
    computeEnvelope();
    return envelopExtern_.getMinX();
  }

  @Override
  public final double getMinY() {
    computeEnvelope();
    return envelopExtern_.getMinY();
  }

  @Override
  public double getMinZ() {
    double r = getPtZ(0);
    for (int i = getPtsNb() - 1; i > 0; i--) {
      final double ri = getPtZ(i);
      if (ri < r) {
        r = ri;
      }
    }
    return r;
  }

  @Override
  public void getMoyCentreElement(int _idxElt, Coordinate _m) {
    _m.x = getMoyCentreXElement(_idxElt);
    _m.y = getMoyCentreYElement(_idxElt);
    _m.z = getMoyCentreZElement(_idxElt);
  }

  /**
   * @param _idxElt l'indice de l'�l�ment
   * @return somme des x/nb point
   */
  @Override
  public final double getMoyCentreXElement(final int _idxElt) {
    return getElement(_idxElt).getMoyMilieuX(this);
  }

  /**
   * @param _idxElt l'indice de l'element
   * @return somme des y/nb point
   */
  @Override
  public final double getMoyCentreYElement(final int _idxElt) {
    return getElement(_idxElt).getMoyMilieuY(this);
  }

  @Override
  public final double getMoyCentreZElement(final int _idxElt) {
    return getElement(_idxElt).getMoyMilieuZ(this);
  }

  @Override
  public EfNodeMutable[] getNodesMutable() {
    final EfNodeMutable[] res = new EfNodeMutable[getPtsNb()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new EfNodeMutable(getPtX(i), getPtY(i), getPtZ(i));
    }
    return res;
  }

  @Override
  public EfNode[] getNodes() {
    final EfNode[] res = new EfNode[getPtsNb()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new EfNode(getPtX(i), getPtY(i), getPtZ(i));
    }
    return res;
  }

  /**
   * @param _progress la barre de progression
   * @return les elements surcontraints ou null si aucun
   */
  @Override
  public final int[] getOverstressedElement(final ProgressionInterface _progress) {
    // final EfElementType t = getEltType();
    // if ((t != EfElementType.T3) && (t != EfElementType.T6)) {
    // FuLog.error("Only for triangles");
    // return null;
    // }
    final int nbElt = getEltNb();
    final TIntArrayList r = new TIntArrayList(nbElt);
    // Mise en place de la progression
    final ProgressionUpdater prog = new ProgressionUpdater(_progress);
    prog.setValue(4, nbElt);
    prog.majProgessionStateOnly(DodicoLib.getS("Recherche des �l�ments surcontraints"));
    EfFrontierInterface fr = getFrontiers();
    if (fr == null) {
      computeBord(_progress, null);
    }
    fr = getFrontiers();
    if (fr == null) {
      return null;
    }
    // final boolean t6 = (t == EfElementType.T6);
    // if (t6) {
    // for (int i = nbElt - 1; i >= 0; i--) {
    // if (getElement(i).isOverstressedT6(fr)) {
    // r.add(i);
    // }
    // prog.majAvancement();
    // }
    // } else {
    for (int i = nbElt - 1; i >= 0; i--) {
      if (getElement(i).getDefaultType() == EfElementType.T6) {
        if (getElement(i).isOverstressedT6(fr)) {
          r.add(i);
        }
      } else {
        if (getElement(i).isOverstressed(fr)) {
          r.add(i);
        }
      }
      prog.majAvancement();
    }
    // }
    return r.isEmpty() ? null : r.toNativeArray();
  }

  /**
   * Attention: aucun test n'est effectue et il faut que le bord externe soit defini.
   *
   * @param _idxOnFrontier l'indice sur la frontiere
   * @return le point dont l'indice sur la frontiere externe est
   *     <code>_idxOnFrontier</code>.
   */
  public final int getPointOnFrontierExtern(final int _idxOnFrontier) {
    return ptsFrontiere_.getIdxGlobalPrinc(_idxOnFrontier);
  }

  @Override
  public final void getPt(final int _i, final Coordinate _c) {
    _c.x = getPtX(_i);
    _c.y = getPtY(_i);
    _c.z = getPtZ(_i);
  }

  /**
   * @param _progress la barre de progression
   * @param _aireMin l'aire mini
   * @return les indices des elements dont l'aire est inferieure a
   *     <code>_aireMin</code>.
   */
  @Override
  public final int[] getSmallElement(final ProgressionInterface _progress, final double _aireMin) {
    final EfElementType t = getEltType();
    if ((t != EfElementType.T3) && (t != EfElementType.T6)) {
      FuLog.error("Only for triangles");
      return null;
    }
    final int nbElt = getEltNb();
    final TIntArrayList r = new TIntArrayList(nbElt);
    // Mise en place de la progression
    final ProgressionUpdater prog = new ProgressionUpdater(_progress);
    prog.setValue(4, nbElt);
    prog.majProgessionStateOnly(DodicoLib.getS("Recherche des �l�ments dont l'aire est inf�rieure � {0}"));

    final boolean t6 = (t == EfElementType.T6);
    if (t6) {
      for (int i = nbElt - 1; i >= 0; i--) {
        if (getAire(i) <= _aireMin) {
          r.add(i);
        }
        prog.majAvancement();
      }
    } else {
      for (int i = nbElt - 1; i >= 0; i--) {
        if (getAire(i) <= _aireMin) {
          r.add(i);
        }
        prog.majAvancement();
      }
    }
    return r.isEmpty() ? null : r.toNativeArray();
  }

  @Override
  public int getSOPointIndex(final EfFrontierInterface _l, final int _idx) {
    if (_l.getNbFrontier() == 0 || _l.getNbPt(_idx) == 0) {
      FuLog.error("no point");
      return -1;
    }
    int r = _l.getIdxGlobal(_idx, 0);
    final Coordinate pt = getCoor(r);
    double xPy = pt.x + pt.y;
    double yMin = pt.y;
    double xPyTemp;
    int temp;
    for (int i = _l.getNbPt(_idx) - 1; i > 0; i--) {
      temp = _l.getIdxGlobal(_idx, i);
      getPt(temp, pt);
      xPyTemp = pt.x + pt.y;
      if (xPyTemp < xPy) {
        r = temp;
        xPy = xPyTemp;
        yMin = pt.y;
      } else if (xPyTemp == xPy) {
        if (pt.y < yMin) {
          r = temp;
          yMin = pt.y;
        }
      }
    }
    return r;
  }

  /**
   * @param _l liste de segement
   * @return l'indice du point SO contenu dans la liste
   *     <code>_l</code>.
   */
  @Override
  public final int getSOPointIndexParmiSegment(final List _l) {
    final int n = _l.size();
    if (n == 0) {
      CtuluLibMessage.error("no point");
      return -1;
    }
    int r = ((EfSegment) _l.get(0)).getPt1Idx();
    final Coordinate pt = getCoor(r);
    double xPy = pt.x + pt.y;
    double yMin = pt.y;
    double xPyTemp;
    for (int i = n - 1; i > 0; i--) {
      final int tmp = ((EfSegment) _l.get(i)).getPt1Idx();
      getPt(tmp, pt);
      xPyTemp = pt.x + pt.y;
      if (xPyTemp < xPy) {
        r = tmp;
        xPy = xPyTemp;
        yMin = pt.y;
      } else if (xPyTemp == xPy) {
        if (pt.y < yMin) {
          r = tmp;
          yMin = pt.y;
        }
      }
    }
    return r;
  }

  /**
   * Utilser lorque ce maillage sert de reference pour une interpolation sur un maillage.
   *
   * @return z pour le point i
   */
  public final double getV(final int _i) {
    return getPtZ(_i);
  }

  /**
   * @param _eltIdx l'indice de l'element a tester
   * @return true si oriente dans le sens trigo
   */
  @Override
  public final int isElementTrigoOriente(final int _eltIdx) {
    return getElement(_eltIdx).isTrigoOriente(this);
  }

  /**
   * @return true si les element sont different de nul.
   */
  @Override
  public boolean isEltDefini() {
    return getEltNb() > 0;
  }

  /**
   * @param _el l'element a tester
   * @return true si l'element est present dans ce maillage
   */
  @Override
  public boolean isEltPresentInGrid(final EfElement _el) {
    return _el.isPresentInArray(this);
  }

  /**
   * @param _m le maillage a tester
   * @param _testZ true si le z doit etre aussi teste
   * @param _epsToCompareDouble la precision pour les comparaison de double
   * @return true si equivalent
   */
  @Override
  public boolean isEquivalent(final EfGridInterface _m, final boolean _testZ, final double _epsToCompareDouble) {
    if ((_m.getPtsNb() != getPtsNb()) || (_m.getEltNb() != getEltNb())) {
      return false;
    }
    int n = getEltNb() - 1;
    for (int i = n; i >= 0; i--) {
      if (!_m.getElement(i).isPresentInArray(this)) {
        return false;
      }
    }
    n = getPtsNb() - 1;
    final Coordinate c = new Coordinate();
    final Coordinate thisC = new Coordinate();
    for (int i = n; i >= 0; i--) {
      _m.getPt(i, c);
      getPt(i, thisC);
      if (thisC.distance(c) > _epsToCompareDouble) {
        return false;
      }
      // test sur z
      if (_testZ && Math.abs(thisC.z - c.z) > _epsToCompareDouble) {
        return false;
      }
    }
    return true;
  }

  /**
   * Methode utile pour les maillage T6.
   *
   * @param _idx l'indice du point
   * @return true si extremite
   */
  @Override
  public final boolean isExtremePoint(final int _idx) {
    if (typeElt_ != null && typeElt_ != EfElementType.T6) {
      return true;
    }
    if (isEltExtremite_ == null) {
      computeMiddle();
    }
    return isEltExtremite_.get(_idx);
  }

  /**
   * Methode utile pour les maillage T6.
   *
   * @param _idx l'indice du point
   * @return true si milieu
   */
  @Override
  public final boolean isMiddlePoint(final int _idx) {
    if (typeElt_ != null && typeElt_ != EfElementType.T6) {
      return false;
    }
    if (isEltExtremite_ == null) {
      computeMiddle();
    }
    return !isEltExtremite_.get(_idx);
  }

  @Override
  public boolean isSameStrict(final EfGridInterface _m, final ProgressionInterface _prog, final double _eps) {
    if ((_m.getPtsNb() != getPtsNb()) || (_m.getEltNb() != getEltNb())) {
      return false;
    }
    int n = getEltNb() - 1;
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(5, n, 0, 50);
    for (int i = n; i >= 0; i--) {
      if (!getElement(i).isEquivalent(_m.getElement(i))) {
        return false;
      }
      up.majAvancement();
    }

    n = getPtsNb() - 1;
    final Coordinate c1 = new Coordinate();
    final Coordinate cThis = new Coordinate();
    up.setValue(5, n, 50, 50);
    for (int i = n; i >= 0; i--) {
      _m.getPt(i, c1);
      getPt(i, cThis);
      if (!cThis.equals3D(c1)) {
        // -- on teste la difference par rapport a epsilon --//
        if (cThis.x - c1.x > _eps || cThis.y - c1.y > _eps || cThis.z - c1.z > _eps) {
          return false;
        }
      }
      up.majAvancement();
    }
    return true;
  }
}
