package org.fudaa.dodico.ef.operation.refine;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.*;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.decorator.AbstractEfGridDataDecorator;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.operation.AbstractEfOperation;
import org.fudaa.dodico.ef.resource.EfResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Raffine en decoupant les �l�ments en 4 selon les aretes.
 */
public class EfOperationRefineEdge extends AbstractEfOperation {
  public static class EfGridDataFromRefineEdge extends AbstractEfGridDataDecorator {
    private final int[][] newPtsSrc;
    private final int[] newElementsSrc;

    public EfGridDataFromRefineEdge(EfGridData init, EfGridInterface newGrid, int[][] newPtsSrc, int[] newElementsSrc) {
      super(init, newGrid);

      this.newPtsSrc = newPtsSrc;
      this.newElementsSrc = newElementsSrc;
    }

    @Override
    public EfData getData(CtuluVariable o, int timeIdx) throws IOException {
      EfData initData = this.getInit().getData(o, timeIdx);
      if (initData == null) {
        return null;
      }

      if (initData.isElementData()) {
        int nbElt = this.getGrid().getEltNb();

        double[] newVal = new double[nbElt];

        for (int i = 0; i < nbElt; i++) {
          // R�cup�ration de la valeur de l'�l�ment source de l'�l�ment en cours.
          newVal[i] = initData.getValue(this.newElementsSrc[i]);
        }
        return new EfDataElement(newVal);
      }
      int nbPts = this.getGrid().getPtsNb();

      double[] newVal = new double[nbPts];

      for (int i = 0; i < nbPts; i++) {
        // R�cup�ration des points sources du point en cours.
        int[] ptsSrc = this.newPtsSrc[i];
        double val = 0d;

        // Calcul de la moyenne des valeur des points sources.
        for (int j = 0; j < ptsSrc.length; j++) {
          val += initData.getValue(ptsSrc[j]);
        }

        // M�morisation dans la moyenne calcul�e comme valeur du point en cours.
        newVal[i] = val / ptsSrc.length;
      }
      return new EfDataNode(newVal);
    }
  }

  InterpolationVectorContainer interpolator;
  CtuluListSelectionInterface selectedElt;
  EfNeighborMesh neighborMesh;

  public InterpolationVectorContainer getInterpolator() {
    return interpolator;
  }

  public void setInterpolator(InterpolationVectorContainer interpolator) {
    this.interpolator = interpolator;
  }

  public CtuluListSelectionInterface getSelectedElt() {
    return this.selectedElt;
  }

  public void setSelectedElt(CtuluListSelectionInterface selectedElt) {
    this.selectedElt = selectedElt;
  }

  @Override
  protected EfGridData process(ProgressionInterface prog, CtuluAnalyze log) {
    EfGridInterface grid = this.initGrid.getGrid();
    EfNode[] nodes = grid.getNodes();
    EfElement[] elements = grid.getElts();
    ArrayList<EfNode> newNodes = new ArrayList<EfNode>(Arrays.asList(nodes));
    ArrayList<EfElement> newElements = new ArrayList<EfElement>(Arrays.asList(elements));

    this.neighborMesh = EfNeighborMesh.compute(grid, prog);

    this.addNeedElementToSelection(prog);

    TIntObjectHashMap<ArrayList<EfSegment>> neighborElements = this.findNeighborElements(prog);
    TObjectIntHashMap<EfSegment> segmentsNewNodes = this.computeSegmentsNewNodes(newNodes, prog);
    TIntIntHashMap elementsNewNodes = this.computeElementsNewNodes(newNodes, prog);
    int[][] newPtsSrc = this.computeNewPtsSrc(newNodes, segmentsNewNodes, elementsNewNodes, prog);
    int[] newElementsSrc = this.createNewElements(newElements, segmentsNewNodes, elementsNewNodes, neighborElements,
        prog);

    if (log != null) {
      int nbEltRefined = (this.selectedElt == null) ? grid.getEltNb() : this.selectedElt.getNbSelectedIndex();
      log.addInfo(EfResource.getS("{0} �l�ments ont �t� rafin�s", Integer.toString(nbEltRefined)));
      log.addInfo(EfResource.getS("{0} noeuds ajout�s", Integer.toString(newNodes.size() - grid.getPtsNb())));
      log.addInfo(EfResource.getS("{0} �l�ments ajout�s", Integer.toString(newElements.size() - grid.getEltNb())));
      log.addInfo(EfResource.getS("Le nouveau nombre de points est : {0}", Integer.toString(newNodes.size())));
      log.addInfo(EfResource.getS("Le nouveau nombre d'�l�ments est : {0}", Integer.toString(newElements.size())));
    }

    // Converti l'ArrayList de points en un tableau de points.
    EfNode[] newArrayNodes = new EfNode[0];
    newArrayNodes = newNodes.toArray(newArrayNodes);

    // Converti l'ArrayList de'�l�ments en un tableau d'�l�ments.
    EfElement[] newArrayElements = new EfElement[0];
    newArrayElements = newElements.toArray(newArrayElements);

    return new EfGridDataFromRefineEdge(this.initGrid, new EfGridArray(newArrayNodes, newArrayElements), newPtsSrc,
        newElementsSrc);
  }

  /**
   * Cr�ation des nouveaux �l�ments pour les �l�ments � rafiner.
   *
   * @param elementIdx l'�l�ment pour lequel les nouveaux �l�ments sont cr��s.
   * @param segmentsNewNodes la liste des nouveaux points cr��s par rapport � des segments.
   * @param elementsNewNodes la liste des nouveaux points cr��s par rapport � des �l�ments.
   * @return les nouveaux �l�ments.
   */
  private EfElement[] createNewSelectedElement(int elementIdx, TObjectIntHashMap<EfSegment> segmentsNewNodes,
                                               TIntIntHashMap elementsNewNodes) {
    EfElement[] newElements;
    EfElement element = this.initGrid.getGrid().getElement(elementIdx);

    if (element.getDefaultType() == EfElementType.T3) {
      newElements = new EfElement[4];

      int[] pts = element.getIndices();
      int newPt1 = segmentsNewNodes.get(this.createSegment(pts[0], pts[1]));
      int newPt2 = segmentsNewNodes.get(this.createSegment(pts[1], pts[2]));
      int newPt3 = segmentsNewNodes.get(this.createSegment(pts[2], pts[0]));

      newElements[0] = new EfElement(new int[]{pts[0], newPt1, newPt3});
      newElements[1] = new EfElement(new int[]{pts[1], newPt2, newPt1});
      newElements[2] = new EfElement(new int[]{pts[2], newPt3, newPt2});
      newElements[3] = new EfElement(new int[]{newPt1, newPt2, newPt3});
    } else if (element.getDefaultType() == EfElementType.Q4) {
      newElements = new EfElement[4];

      int[] pts = element.getIndices();
      int newPt1 = segmentsNewNodes.get(this.createSegment(pts[0], pts[1]));
      int newPt2 = segmentsNewNodes.get(this.createSegment(pts[1], pts[2]));
      int newPt3 = segmentsNewNodes.get(this.createSegment(pts[2], pts[3]));
      int newPt4 = segmentsNewNodes.get(this.createSegment(pts[3], pts[0]));
      int newPt5 = elementsNewNodes.get(elementIdx);

      newElements[0] = new EfElement(new int[]{pts[0], newPt1, newPt5, newPt4});
      newElements[1] = new EfElement(new int[]{pts[1], newPt2, newPt5, newPt1});
      newElements[2] = new EfElement(new int[]{pts[2], newPt3, newPt5, newPt2});
      newElements[3] = new EfElement(new int[]{pts[3], newPt4, newPt5, newPt3});
    } else {
      int nbPts = element.getPtNb();
      int[] pts = element.getIndices();
      int[] newPts = new int[nbPts * 2];
      int newPt = elementsNewNodes.get(elementIdx);

      newElements = new EfElement[nbPts * 2];

      for (int i = 0; i < (nbPts * 2); i++) {
        if ((i % 2) == 0) {
          newPts[i] = pts[i / 2];
        } else {
          newPts[i] = segmentsNewNodes.get(this.createSegment(pts[(i - 1) / 2], pts[(((i - 1) / 2) + 1) % nbPts]));
        }
      }

      for (int i = 0; i < newPts.length; i++) {
        newElements[i] = new EfElement(new int[]{newPts[i], newPts[(i + 1) % newPts.length], newPt});
      }
    }

    return newElements;
  }

  /**
   * Cr�ation des nouveaux �l�ments pour les �l�ments voisins des �l�ments � rafiner.
   *
   * @param elementIdx l'�l�ment pour lequel les nouveaux �l�ments sont cr��s.
   * @param segments la liste des segments par lesquels l'�l�ment est li� � un �l�ment rafiner.
   * @param segmentsNewNodes la liste des nouveaux points cr��s par rapport � des segments.
   * @return les nouveaux �l�ments.
   */
  private EfElement[] createNewElementForNeighborElement(int elementIdx, ArrayList<EfSegment> segments,
                                                         TObjectIntHashMap<EfSegment> segmentsNewNodes) {
    EfElement[] newElements;
    EfElement element = this.initGrid.getGrid().getElement(elementIdx);

    // Si l'�l�ment est un T3, il est coup� en 2.
    if (element.getDefaultType() == EfElementType.T3) {
      newElements = new EfElement[2];

      EfSegment segment = segments.get(0);
      int pt1 = segment.getPt1Idx();
      int pt2 = segment.getPt2Idx();
      int newPt = segmentsNewNodes.get(segment);
      int pt3 = element.getPtIndex(0);
      int idx = 0;
      int ptsNb = element.getPtNb();

      for (int i = 0; i < ptsNb; i++) {
        pt3 = element.getPtIndex(i);

        if ((pt3 != pt1) && (pt3 != pt2)) {
          break;
        }
      }

      for (idx = 0; idx < ptsNb; idx++) {
        if (element.getPtIndex(idx) == pt1) {
          break;
        }
      }

      if (element.getPtIndex((idx + 1) % ptsNb) == pt2) {
        newElements[0] = new EfElement(new int[]{pt1, newPt, pt3});
      } else {
        newElements[0] = new EfElement(new int[]{pt1, pt3, newPt});
      }

      for (idx = 0; idx < ptsNb; idx++) {
        if (element.getPtIndex(idx) == pt2) {
          break;
        }
      }

      if (element.getPtIndex((idx + 1) % ptsNb) == pt1) {
        newElements[1] = new EfElement(new int[]{pt2, newPt, pt3});
      } else {
        newElements[1] = new EfElement(new int[]{pt2, pt3, newPt});
      }
    }
    // Sinon l'�l�ment est un Q4
    else {
      EfSegment segment = segments.get(0);
      int pt1 = segment.getPt1Idx();
      int pt2 = segment.getPt2Idx();
      int newPt = segmentsNewNodes.get(segment);
      int nbEdge = element.getNbEdge();
      int ptsNb = element.getPtNb();
      int pt3 = element.getPtIndex(0);
      int pt4 = element.getPtIndex(0);
      int idx;

      // Si il est li� par un segment, il est coup� en 3.
      if (segments.size() == 1) {
        newElements = new EfElement[3];

        for (int i = 0; i < nbEdge; i++) {
          if (!segment.isSameIdx(this.createSegment(element.getEdgePt1(i), element.getEdgePt2(i)))) {
            if (element.getEdgePt1(i) == pt1) {
              pt3 = element.getEdgePt2(i);
            } else if (element.getEdgePt2(i) == pt1) {
              pt3 = element.getEdgePt1(i);
            } else if (element.getEdgePt1(i) == pt2) {
              pt4 = element.getEdgePt2(i);
            } else if (element.getEdgePt2(i) == pt2) {
              pt4 = element.getEdgePt1(i);
            }
          }
        }

        for (idx = 0; idx < ptsNb; idx++) {
          if (element.getPtIndex(idx) == pt1) {
            break;
          }
        }

        if (element.getPtIndex((idx + 1) % ptsNb) == pt2) {
          newElements[0] = new EfElement(new int[]{pt1, newPt, pt3});
          newElements[1] = new EfElement(new int[]{pt3, newPt, pt4});
          newElements[2] = new EfElement(new int[]{pt2, pt4, newPt});
        } else {
          newElements[0] = new EfElement(new int[]{pt1, pt3, newPt});
          newElements[1] = new EfElement(new int[]{pt3, pt4, newPt});
          newElements[2] = new EfElement(new int[]{pt2, newPt, pt4});
        }
      }
      // Si il est li� par 2 segments, il est coup� en 2.
      else {
        int newPt2 = segmentsNewNodes.get(segments.get(1));

        newElements = new EfElement[2];

        for (idx = 0; idx < ptsNb; idx++) {
          if (element.getPtIndex(idx) == pt1) {
            break;
          }
        }

        if (element.getPtIndex((idx + 1) % ptsNb) == pt2) {
          newElements[0] = new EfElement(new int[]{pt1, newPt, newPt2, pt3});
          newElements[1] = new EfElement(new int[]{pt2, pt4, newPt2, newPt});
        } else {
          newElements[0] = new EfElement(new int[]{pt1, pt3, newPt2, newPt});
          newElements[1] = new EfElement(new int[]{pt2, newPt, newPt2, pt4});
        }
      }
    }

    return newElements;
  }

  /**
   * Cr�ation des nouveaux �l�ments.
   *
   * @param elements la liste des �l�ments.
   * @param segmentsNewNodes la liste des nouveaux points cr��s par rapport � des segments.
   * @param elementsNewNodes la liste des nouveaux points cr��s par rapport � des �l�ments.
   * @param neighborElements la liste des �l�ments voisins aux �l�ments � rafiner.
   * @param prog le manager de progression.
   * @return un tableau qui, pour chaque �l�ments, donne l'�l�ments source.
   */
  private int[] createNewElements(ArrayList<EfElement> elements, TObjectIntHashMap<EfSegment> segmentsNewNodes,
                                  TIntIntHashMap elementsNewNodes, TIntObjectHashMap<ArrayList<EfSegment>> neighborElements,
                                  ProgressionInterface prog) {
    EfGridInterface grid = this.initGrid.getGrid();
    int nbElts = grid.getEltNb();
    TIntArrayList newElementsSrc = new TIntArrayList(nbElts);
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly(Messages.getString("ef.refineEdge.createNewElements.msg"));
    updater.setValue(10, nbElts);

    for (int i = 0; i < nbElts; i++) {
      newElementsSrc.add(i);
    }

    for (int i = 0; i < nbElts; i++) {
      // Cr�ation des nouveaux �l�ments pour les �l�ments � rafiner et leurs voisins.
      if ((this.selectedElt == null) || (this.selectedElt.isSelected(i)) || (neighborElements.containsKey(i))) {
        EfElement[] newElements;

        if ((this.selectedElt == null) || (this.selectedElt.isSelected(i))) {
          newElements = this.createNewSelectedElement(i, segmentsNewNodes, elementsNewNodes);
        } else {
          newElements = this.createNewElementForNeighborElement(i, neighborElements.get(i), segmentsNewNodes);
        }

        elements.set(i, newElements[0]);

        for (int j = 1; j < newElements.length; j++) {
          newElementsSrc.add(i);
          elements.add(newElements[j]);
        }
      } else {
        newElementsSrc.set(i, i);
      }

      updater.majAvancement();
    }

    return newElementsSrc.toNativeArray();
  }

  /**
   * Calculation des points sources des nouveaux points et ajout des nouveaux points.
   *
   * @param nodes la liste des points.
   * @param segmentsNewNodes la liste des nouveaux points cr��s par rapport � des segments.
   * @param elementsNewNodes la liste des nouveaux points cr��s par rapport � des �l�ments.
   * @param prog le manager de progression.
   */
  private int[][] computeNewPtsSrc(ArrayList<EfNode> nodes, TObjectIntHashMap<EfSegment> segmentsNewNodes,
                                   TIntIntHashMap elementsNewNodes, ProgressionInterface prog) {
    int[][] newPtsSrc = new int[nodes.size()][];
    EfGridInterface grid = this.initGrid.getGrid();
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly(Messages.getString("ef.refineEdge.computeNewPtsSrc.msg"));
    updater.setValue(10, grid.getPtsNb() + segmentsNewNodes.size() + elementsNewNodes.size());

    // R�cup�ration des points existant.
    for (int i = 0; i < grid.getPtsNb(); i++) {
      newPtsSrc[i] = new int[]{i};

      updater.majAvancement();
    }

    TObjectIntIterator<EfSegment> segmentIterator = segmentsNewNodes.iterator();

    // Cr�ation des nouveaux points pour les segments.
    while (segmentIterator.hasNext()) {
      segmentIterator.advance();

      EfSegment segment = segmentIterator.key();
      int idx = segmentIterator.value();

      newPtsSrc[idx] = new int[]{segment.getPt1Idx(), segment.getPt2Idx()};

      updater.majAvancement();
    }

    TIntIntIterator elementIterator = elementsNewNodes.iterator();

    // Cr�ation des nouveaux points pour les �l�ments.
    while (elementIterator.hasNext()) {
      elementIterator.advance();

      int element = elementIterator.key();
      int idx = elementIterator.value();

      newPtsSrc[idx] = grid.getElement(element).getIndices();

      updater.majAvancement();
    }

    return newPtsSrc;
  }

  /**
   * Permet de savoir si un �l�ment doit �tre rafiner.
   *
   * @param element l'�l�ment � tester.
   * @param segments la liste des segments par lesquels l'�l�ment est li� � un �l�ment � rafiner.
   * @return true si l'�l�ment doit �tre rafiner.
   */
  private boolean mustBeSelected(EfElement element, ArrayList<EfSegment> segments) {
    if (segments.size() > 0) {
      if (element.getDefaultType() == EfElementType.T3) {
        if (segments.size() > 1) {
          return true;
        }
      } else if (element.getDefaultType() == EfElementType.Q4) {
        if (segments.size() > 2) {
          return true;
        } else if (segments.size() == 2) {
          EfSegment segment1 = segments.get(0);
          EfSegment segment2 = segments.get(1);

          if ((segment1.getPt1Idx() == segment2.getPt1Idx()) || (segment1.getPt1Idx() == segment2.getPt2Idx())
              || (segment1.getPt2Idx() == segment2.getPt1Idx()) || (segment1.getPt2Idx() == segment2.getPt2Idx())) {
            return true;
          }
        }

        return false;
      } else {
        return true;
      }
    }

    return false;
  }

  /**
   * R�cup�ration des nouveaux points li�s � un �l�ments et ajout des ces nouveaux points.
   *
   * @param nodes la liste des points.
   * @param prog le manager de progression.
   * @return une map donne pour un �l�ment, l'indice du point cr�� en son centre.
   */
  private TIntIntHashMap computeElementsNewNodes(final ArrayList<EfNode> nodes, ProgressionInterface prog) {
    TIntIntHashMap elementsNewNodes = new TIntIntHashMap();
    EfGridInterface grid = this.initGrid.getGrid();
    EfElement[] elements = grid.getElts();
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly(Messages.getString("ef.refineEdge.computeElementsNewNodes.msg"));
    updater.setValue(10, elements.length);

    for (int i = 0; i < elements.length; i++) {
      if ((this.selectedElt == null) || this.selectedElt.isSelected(i)) {
        if (elements[i].getDefaultType() != EfElementType.T3) {
          Coordinate coordinate = new Coordinate();

          elements[i].getMilieuXY(grid, coordinate);

          elementsNewNodes.put(i, nodes.size());

          nodes.add(new EfNode(coordinate));
        }
      }

      updater.majAvancement();
    }

    return elementsNewNodes;
  }

  /**
   * R�cup�ration des nouveaux points li�s � un segment et ajout des ces nouveaux points.
   *
   * @param nodes la liste des points.
   * @param prog le manager de progression.
   * @return une map donne pour un segment, l'indice du point cr�� en son centre.
   */
  private TObjectIntHashMap<EfSegment> computeSegmentsNewNodes(final ArrayList<EfNode> nodes, ProgressionInterface prog) {
    TObjectIntHashMap<EfSegment> segmentsNewNodes = new TObjectIntHashMap<EfSegment>();
    EfGridInterface grid = this.initGrid.getGrid();
    EfElement[] elements = grid.getElts();
    ProgressionUpdater updater = new ProgressionUpdater(prog);

    updater.majProgessionStateOnly(Messages.getString("ef.refineEdge.computeSegmentsNewNodes.msg"));
    updater.setValue(10, elements.length);

    for (int i = 0; i < elements.length; i++) {
      if ((this.selectedElt == null) || this.selectedElt.isSelected(i)) {
        int segmentNb = elements[i].getNbEdge();

        for (int j = 0; j < segmentNb; j++) {
          EfSegment segment = this.createSegment(elements[i].getEdgePt1(j), elements[i].getEdgePt2(j));

          if (!segmentsNewNodes.containsKey(segment)) {
            Coordinate coordinate = new Coordinate();

            segment.getCentre(grid, coordinate);

            segmentsNewNodes.put(segment, nodes.size());

            nodes.add(new EfNode(coordinate));
          }
        }
      }

      updater.majAvancement();
    }

    return segmentsNewNodes;
  }

  /**
   * S�lectionne un nouvel �l�ment.
   *
   * @param eltIdx l'indice de l'�l�ment � s�lectionner.
   * @param selectedElt la liste des �l�ments s�lectionn�.
   * @param alreadyManaged la liste des �l�ments d�j� trait�.
   */
  private void manageSelection(int eltIdx, final CtuluListSelection selectedElt,
                               final TIntObjectHashMap<ArrayList<EfSegment>> alreadyManaged) {
    EfGridInterface grid = this.initGrid.getGrid();
    EfElement element = grid.getElement(eltIdx);

    selectedElt.add(eltIdx);

    int segmentsNb = element.getNbEdge();

    // Si les voisin de l'�l�ment ont d�j� �t� trait�, ils sont retrait�.
    for (int j = 0; j < segmentsNb; j++) {
      int neighborIdx = this.neighborMesh.getAdjacentMeshes(element.getEdgePt1(j), element.getEdgePt2(j), eltIdx);

      if (alreadyManaged.containsKey(neighborIdx)) {
        ArrayList<EfSegment> segments = alreadyManaged.get(neighborIdx);

        segments.add(this.createSegment(element.getEdgePt1(j), element.getEdgePt2(j)));

        if (this.mustBeSelected(grid.getElement(neighborIdx), segments)) {
          alreadyManaged.remove(neighborIdx);

          this.manageSelection(neighborIdx, selectedElt, alreadyManaged);
        }
      }
    }
  }

  /**
   * Ajout � la liste des �l�ments � rafiner les �l�ments qui doivent aussi �tre rafiner pour que le traitement
   * fonctionne.
   *
   * @param prog le manager de progression.
   */
  private void addNeedElementToSelection(ProgressionInterface prog) {
    if (this.selectedElt != null) {
      EfGridInterface grid = this.initGrid.getGrid();
      EfElement[] elements = grid.getElts();
      CtuluListSelection selectedElt = new CtuluListSelection(this.selectedElt.getSelectedIndex());
      TIntObjectHashMap<ArrayList<EfSegment>> alreadyManaged = new TIntObjectHashMap<ArrayList<EfSegment>>();
      ProgressionUpdater updater = new ProgressionUpdater(prog);

      updater.majProgessionStateOnly(Messages.getString("ef.refineEdge.addNeedElementToSelection.msg"));
      updater.setValue(10, elements.length);

      for (int i = 0; i < elements.length; i++) {
        // Si l'�l�ment en cours n'est pas s�lectionn�, il est tester pour savoir si il doit l'�tre.
        if (!selectedElt.isSelected(i)) {
          int segmentsNb = elements[i].getNbEdge();
          ArrayList<EfSegment> segments = new ArrayList<EfSegment>();

          for (int j = 0; j < segmentsNb; j++) {
            int neighborIdx = this.neighborMesh.getAdjacentMeshes(elements[i].getEdgePt1(j), elements[i].getEdgePt2(j),
                i);

            if (selectedElt.isSelected(neighborIdx)) {
              segments.add(this.createSegment(elements[i].getEdgePt1(j), elements[i].getEdgePt2(j)));
            }
          }

          if (this.mustBeSelected(elements[i], segments)) {
            this.manageSelection(i, selectedElt, alreadyManaged);
          } else {
            alreadyManaged.put(i, segments);
          }
        }

        updater.majAvancement();
      }

      this.selectedElt = selectedElt;
    }
  }

  /**
   * Permet de cr�er un segment dont le point 1 est inf�rieur au point 2.
   *
   * @param ptIdx1 le premier point du segment.
   * @param ptIdx2 le deuxi�me point du segment.
   * @return un segment ayant un point 1 inf�rieur au point 2.
   */
  private EfSegment createSegment(int ptIdx1, int ptIdx2) {
    return new EfSegment(Math.min(ptIdx1, ptIdx2), Math.max(ptIdx1, ptIdx2));
  }

  /**
   * Recherche les �l�ments voisins des �l�ments � rafiner.
   *
   * @param prog le manager de progression.
   * @return une liste contenant pour chaque �l�ment voisin d'un �l�ment � rafiner, les segment par lesquels il est li�
   *     � un �l�ment � rafiner.
   */
  private TIntObjectHashMap<ArrayList<EfSegment>> findNeighborElements(ProgressionInterface prog) {
    // TODO Voir pour changer ArrayList en Hashset.
    TIntObjectHashMap<ArrayList<EfSegment>> neighborElements = new TIntObjectHashMap<ArrayList<EfSegment>>();
    EfGridInterface grid = this.initGrid.getGrid();

    if (this.selectedElt != null) {
      int[] selectedIdx = this.selectedElt.getSelectedIndex();
      ProgressionUpdater updater = new ProgressionUpdater(prog);

      updater.majProgessionStateOnly(Messages.getString("ef.refineEdge.findNeighborElements.msg"));
      updater.setValue(10, selectedIdx.length);

      for (int i = 0; i < selectedIdx.length; i++) {
        EfElement currentElt = grid.getElement(selectedIdx[i]);
        int segmentsNb = currentElt.getNbEdge();

        for (int j = 0; j < segmentsNb; j++) {
          int neighborIdx = this.neighborMesh.getAdjacentMeshes(currentElt.getEdgePt1(j), currentElt.getEdgePt2(j),
              selectedIdx[i]);

          if ((neighborIdx != -1) && (!this.selectedElt.isSelected(neighborIdx))) {
            if (!neighborElements.containsKey(neighborIdx)) {
              neighborElements.put(neighborIdx, new ArrayList<EfSegment>());
            }

            ArrayList<EfSegment> segments = neighborElements.get(neighborIdx);

            EfSegment segment = this.createSegment(currentElt.getEdgePt1(j), currentElt.getEdgePt2(j));

            if (!segments.contains(segment)) {
              segments.add(segment);
            }
          }
        }

        updater.majAvancement();
      }
    }

    return neighborElements;
  }
}
