package org.fudaa.dodico.ef;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluResult;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author deniger
 */
public class ConditionLimiteHelper {

  public static final int ILOG = 2;
  public static final int IADH = 0;
  public static final int IENT = 5;
  public static final int IENTU = 6;
  public static final int ISORT = 4;
  public static final int IINC = 1;
  public static final ConditionLimiteEnum[] EMPTY_ARRAY = new ConditionLimiteEnum[0];

  /**
   * @param idx l'indice dans l'ordre des enums
   * @return la condition limite correspondante ou SOLID si indice en dehors des clous.
   */
  public static ConditionLimiteEnum getConditionsLimite(int idx) {
    ConditionLimiteEnum[] values = ConditionLimiteEnum.values();
    if (idx < 0 || idx >= values.length) return ConditionLimiteEnum.SOLID;
    return values[idx];
  }

  /**
   * @param clOnNode les cl donnees sur tous les noeuds du maillage
   * @param grid le maillage en question
   * @param prog la barre de progression
   * @return resultat avec les conditions donn�es sur les noeuds frontieres uniquement soit
   *         grid.getFrontiers().getArray()
   */
  public static CtuluResult<ConditionLimiteEnum[]> extractConditionLimiteOnFrontier(ConditionLimiteEnum[] clOnNode,
      EfGridInterface grid, ProgressionInterface prog) {
    CtuluAnalyze log = new CtuluAnalyze();
    grid.computeBord(prog, log);
    CtuluResult<ConditionLimiteEnum[]> res = new CtuluResult<ConditionLimiteEnum[]>();
    res.setAnalyze(log);
    int[] array = grid.getFrontiers().getArray();
    ConditionLimiteEnum[] cls = new ConditionLimiteEnum[array.length];
    for (int i = 0; i < cls.length; i++) {
      cls[i] = clOnNode[array[i]];
    }
    res.setResultat(cls);
    return res;
  }
}
