/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;

public class EfLineIntersectionParentDefault implements EfLineIntersectionParent {

  final EfGridDataInterpolator data_;

  protected EfLineIntersectionParentDefault(final EfGridDataInterpolator _interpolator) {
    data_ = _interpolator;

  }

  @Override
  public void buildNeighbor(ProgressionInterface _prog) {
    EfGridInterface grid = data_.getData().getGrid();
    EfNeighborMesh neighbor = grid.getNeighbors();
    if (neighbor == null) {
      grid.computeNeighbord(_prog, null);
    }

  }

  @Override
  public EfGridInterface getGrid() {
    return data_.getData().getGrid();
  }

  @Override
  public EfGridDataInterpolator getGridData() {
    return data_;
  }

  @Override
  public EfNeighborMesh getNeighbor() {
    return data_.getData().getGrid().getNeighbors();
  }

}