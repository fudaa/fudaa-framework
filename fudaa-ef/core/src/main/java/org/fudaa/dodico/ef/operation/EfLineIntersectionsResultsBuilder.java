/*
 * @creation 12 juin 07
 * @modification $Date: 2007-06-13 12:55:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.LineString;
import gnu.trove.TIntObjectHashMap;
import java.lang.ref.WeakReference;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author fred deniger
 * @version $Id: EfLineIntersectionsResultsBuilder.java,v 1.1 2007-06-13 12:55:43 deniger Exp $
 */
public class EfLineIntersectionsResultsBuilder {

  final EfLineIntersectionsResultsI res_;
  final EfLineIntersectionsCorrectionTester tester_;
  final TIntObjectHashMap pool_ = new TIntObjectHashMap(20);

  
  private  LineString initLine_;
  
  


public EfLineIntersectionsResultsBuilder(final LineString initLine,final EfLineIntersectionsResultsI _res,
      final EfLineIntersectionsCorrectionTester _tester) {
    super();
    res_ = _res;
    tester_ = _tester;
    initLine_=initLine;
    
  }

  public boolean isPresentInPool(int _tIdx) {
    return pool_.contains(_tIdx);
  }

  public boolean isSavedInPool(int _tIdx) {
    WeakReference ref = (WeakReference) pool_.get(_tIdx);
    return ref != null && ref.get() != null;

  }

  public EfLineIntersectionsResultsI createResults(int _tidx, ProgressionInterface _prog) {
    WeakReference ref = (WeakReference) pool_.get(_tidx);
    if (Fu.DEBUG && FuLog.isDebug()) FuLog.debug("DEF: res present in pool = " + (ref != null));
    if (ref != null && ref.get() != null) return (EfLineIntersectionsResultsI) ref.get();
    EfLineIntersectionsResultsI res = new EfLineIntersectionsCorrectionActivity().correct(res_, tester_, _tidx, _prog);
    // pas de correction
    if (res == res_) return res_;
    ref = new WeakReference(res);
    pool_.put(_tidx, ref);
    return res;

  }

  public EfLineIntersectionsCorrectionTester getTester() {
    return tester_;
  }

  public EfLineIntersectionsResultsI getInitRes() {
    return res_;
  }

public LineString getInitLine() {
	return initLine_;
}

public void setInitLine(LineString line) {
	 initLine_=line;
}

}
