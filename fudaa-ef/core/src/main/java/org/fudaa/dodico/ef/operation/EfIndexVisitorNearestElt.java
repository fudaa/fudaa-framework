/*
 * @creation 9 oct. 06
 * 
 * @modification $Date: 2007-04-20 16:21:04 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.operation;

import org.locationtech.jts.geom.Envelope;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfIndexVisitor;

/**
 * @author fred deniger
 * @version $Id: EfIndexVisitorNearestElt.java,v 1.1 2007-04-20 16:21:04 deniger Exp $
 */
public class EfIndexVisitorNearestElt implements EfIndexVisitor {

  private final EfGridInterface grid_;
  private int eltSelected_ = -1;
  private boolean isIn_;
  private boolean onlyInSearch_;
  private double minDist_ = -1;
  private final double maxDistance_;
  private final double x_;
  private final double y_;

  /**
   * @param _grid
   * @param _x
   * @param _y
   * @param _dist distance maxi entre l'element a selectionner et le point. Si negatif, non pris en compte: l'�l�ment le
   *          plus pres sera trouv� quelle que soit la distance
   */
  public EfIndexVisitorNearestElt(final EfGridInterface _grid, final double _x, final double _y, final double _dist) {
    super();
    grid_ = _grid;
    maxDistance_ = _dist;
    x_ = _x;
    y_ = _y;
  }

  public Envelope buildSearchEnvelop() {

    return maxDistance_ < 0 ? null : EfIndexVisitorNodeSelect.buildSearchEnvelop(x_, y_, maxDistance_);

  }

  @Override
  public void visitItem(final int _idxElt) {
    if (!isIn_) {
      final EfElement el = grid_.getElement(_idxElt);
      try {
        if (grid_.getEnvelopeForElement(_idxElt).contains(x_, y_) && el.contientXY(x_, y_, grid_)) {
          isIn_ = true;
          eltSelected_ = _idxElt;
        } else if(!onlyInSearch_){
          final double dist = el.getDist(x_, y_, grid_);
          // si maxDistance_ est n�gatif, l'utilisateur ne veut pas prendre en compte la distance max.
          if ((maxDistance_ < 0 || dist <= maxDistance_) && (eltSelected_ < 0 || dist < minDist_)) {
            minDist_ = dist;
            eltSelected_ = _idxElt;
          }
        }
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

  }

  /**
   * @return null si pas de selection
   */
  public CtuluListSelection getSelection() {
    if (eltSelected_ >= 0) {
      final CtuluListSelection res = new CtuluListSelection();
      res.add(eltSelected_);
      return res;
    }
    return null;
  }

  public boolean isIn() {
    return isIn_;
  }

  public double getMinDist() {
    return minDist_;
  }

  public int getSelected() {
    return eltSelected_;
  }

  /**
   * @return the onlyInSearch_
   */
  public boolean isOnlyInSearch() {
    return onlyInSearch_;
  }

  /**
   * @param onlyInSearch the onlyInSearch_ to set
   */
  public void setOnlyInSearch(boolean onlyInSearch) {
    onlyInSearch_ = onlyInSearch;
  }
}
