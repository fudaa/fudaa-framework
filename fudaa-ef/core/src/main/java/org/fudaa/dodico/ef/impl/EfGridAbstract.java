/*
 * @creation 25 juin 2003
 * @modification $Date: 2007-05-22 13:11:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;

/**
 * Un maillage contenant des elements donnes dans le meme sens. Ce maillage peut servir de reference pour une
 * interpolation (voir l'interface EfInteropationGridRefI). Dans ce l'interpolation se fait sur la hauteur en chaque
 * point (voir la methode getV(int). Ce maillage peut servir egalement comme destination d'une interpolation (voir
 * l'interface EfInterpolationTarget)
 * 
 * @author deniger
 * @version $Id: EfGridAbstract.java,v 1.3 2007-05-22 13:11:26 deniger Exp $
 */
public abstract class EfGridAbstract extends EfGridDefaultAbstract {

  protected EfElement[] elts_;
  
  

  /**
   * Attention : les tableaux sont repris et ne sont pas copies.
   * 
   * @param _elts les elements a utiliser
   */
  public EfGridAbstract(final EfElement[] _elts) {
    elts_ = _elts;
  }
  
  



  /**
   * @param _i l'indice de l'element demande
   * @return l'element d'indice <code>_i</code>
   */
  @Override
  public EfElement getElement(final int _i) {
    return elts_[_i];
  }

  /**
   * @return le nombre d'element contenus dans ce maillage
   */
  @Override
  public int getEltNb() {
    return elts_==null?0:elts_.length;
  }

  @Override
  public EfElement[] getElts() {
    final EfElement[] rf = new EfElement[getEltNb()];
    System.arraycopy(elts_, 0, rf, 0, rf.length);
    return rf;
  }

  /**
   * @return le type commun. null si heterogene
   */
  @Override
  public EfElementType getEltType() {
    if (typeElt_ == null) {
      typeElt_ = EfElement.getCommunType(elts_);
    }
    return typeElt_;
  }


  /**
   * @return true si les element sont different de nul.
   */
  @Override
  public boolean isEltDefini() {
    return elts_ != null;
  }

  /**
   * @param _el l'element a tester
   * @return true si l'element est present dans ce maillage
   */
  @Override
  public boolean isEltPresentInGrid(final EfElement _el) {
    return _el.isPresentInArray(elts_);
  }

}