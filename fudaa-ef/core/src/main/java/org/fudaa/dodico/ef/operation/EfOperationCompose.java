package org.fudaa.dodico.ef.operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;

public class EfOperationCompose implements EfOperation {
  private final List<EfOperation> ops;

  private EfOperation currentOp;
  private boolean stop;
  private EfGridData init;

  public EfOperationCompose(Collection<EfOperation> ops) {
    super();

    if (ops == null) {
      this.ops = Collections.emptyList();
    } else {
      this.ops = new ArrayList<EfOperation>(ops);
    }

  }

  public EfOperationCompose(EfOperation... ops) {
    this(Arrays.asList(ops));
  }

  @Override
  public EfOperationResult process(ProgressionInterface prog) {
    stop = false;
    CtuluAnalyze log = new CtuluAnalyze();
    EfGridData in = init;
    for (EfOperation it : ops) {
      if (stop) return null;
      it.setInitGridData(in);
      currentOp = it;
      EfOperationResult process = it.process(prog);
      in = process.getGridData();
      log.merge(process.getAnalyze());
    }
    return new EfOperationResult(log, in);
  }

  @Override
  public void setInitGridData(EfGridData in) {
    init = in;
  }

  @Override
  public void stop() {
    stop = true;
    if (currentOp != null) currentOp.stop();

  }

}
