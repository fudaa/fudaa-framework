/**
 * @creation 5 f�vr. 2004
 * @modification $Date: 2007-01-19 13:07:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 * @version $Id: EfGridSourceDefaut.java,v 1.1 2007-01-19 13:07:20 deniger Exp $
 */
public class EfGridSourceDefaut extends EfGridSourcesAbstract {

    EfGridInterface g_;
    FileFormat f_;

    /**
     * @param _g
     *            le maillage
     * @param _f
     *            le format
     */
    public EfGridSourceDefaut(final EfGridInterface _g, final FileFormat _f) {
	this(_g, _f, ConditionLimiteHelper.EMPTY_ARRAY);
    }

    public EfGridSourceDefaut(final EfGridInterface _g, final FileFormat _f,
	    ConditionLimiteEnum[] _b) {
	g_ = _g;
	f_ = _f;
	boundaryConditions = _b;
    }

    /**
   *
   */
  @Override
    public EfGridInterface getGrid() {
	return g_;
    }

    /**
   *
   */
    public FileFormat getFileFormat() {
	return f_;
    }

}