/*
 * @creation 7 juin 2005
 * 
 * @modification $Date: 2007-04-16 16:34:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.impl;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: EfGridGisAdapter.java,v 1.2 2007-04-16 16:34:22 deniger Exp $
 */
public class EfGridGisAdapter implements GISDataModel {

  final EfGridInterface grid_;
  final GISAttributeInterface z_;

  /**
   * @param _grid
   * @param _z
   */
  public EfGridGisAdapter(final EfGridInterface _grid, final GISAttributeInterface _z) {
    super();
    grid_ = _grid;
    z_ = _z;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new EfGridGisAdapter(new EfGridTranslate(grid_, xyToAdd.getX(), xyToAdd.getZ()), z_);
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return z_;
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return grid_.getEnvelope(null);
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return grid_.getPtZ(_idxGeom);
  }

  GISPoint[] pt_;

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    if (pt_ == null) {
      pt_ = new GISPoint[grid_.getPtsNb()];
      for (int i = pt_.length - 1; i >= 0; i--) {
        pt_[i] = new GISPoint(grid_.getPtX(i), grid_.getPtY(i), 0);
      }

    }
    return pt_[_idxGeom];
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    return _att == z_ ? 0 : -1;
  }

  @Override
  public int getNbAttributes() {
    return 1;
  }

  @Override
  public int getNumGeometries() {
    return grid_.getPtsNb();
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return CtuluLib.getDouble(getDoubleValue(_idxAtt, _idxGeom));
  }
}
