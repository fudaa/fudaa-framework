/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.operation;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.EfGridData;

/**
 * Activit� de transformation sur un maillage: non r�entrant.
 * 
 * @author deniger
 */
public interface EfOperation extends CtuluActivity {

  /**
   * @param in le maillage a transformer
   */
  void setInitGridData(EfGridData in);
  
  /**
   * @param _prog le receveur de progression
   * @return le r�sultat de l'op�ration
   */
  EfOperationResult process(ProgressionInterface _prog);
  
}
