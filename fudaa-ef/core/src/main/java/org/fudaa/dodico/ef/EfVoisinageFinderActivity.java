/*
 * @creation 1 mars 07
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;

/**
 * Permet de trouver pour chaque �l�ment les �l�ments voisins (ayant une arete en commun) et pour chaque arete les 1/2
 * �l�ments voisins. Cette classe est teste avec les classes de Rubar: {@link org.fudaa.dodico.rubar.TestJDAT}
 * 
 * @author fred deniger
 * @version $Id: EfVoisinageFinderActivity.java,v 1.2 2007-05-04 13:45:58 deniger Exp $
 */
public class EfVoisinageFinderActivity implements CtuluActivity {

  boolean stop_;
  /**
   * La taille est celle du nombre d'�l�ments.
   */
  int[][] elementVoisinsParElement_;
  int[][] elementVoisinsParArete_;

  @Override
  public void stop() {
    stop_ = true;

  }

  EfElementVolume[] elts_;
  EfSegment[] edges_;

  /**
   * @param _grid
   * @param _prog
   */
  public boolean process(final EfGridInterface _grid, final ProgressionInterface _prog, final CtuluAnalyze _analyse) {
    stop_ = false;
    final int eltNb = _grid.getEltNb();
    // les elements volumes: contenant les indices des aretes
    elts_ = new EfElementVolume[eltNb];
    // Dans un premier temps, on doit reconstruire les aretes
    // et pour ces aretes, on doit
    // cette liste permet de d�couvrir l'ordre de d�couverte des aretes : utile
    final int guessAreteNb = (int) (eltNb * 2.5);
    // stock la liaison arete->indice d'insertion
    // permettra de savoir si une arete a d�j� �t� rencontr�e ou non
    final TObjectIntHashMap edges = new TObjectIntHashMap(guessAreteNb);

    // pour chaque indice d'arete, cette map donnera un tableau de 2 entiers
    // indiquant les 2 �l�ments voisins.
    // le premier indice et l'�l�ment situ� � gauche
    // le deuxieme indice et l'�l�ment situ� � droite
    // le sens d'une arete est du plus petit indice au plus grand (norme Rubar)
    final TIntObjectHashMap areteVoisins = new TIntObjectHashMap(guessAreteNb);

    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(5, eltNb, 0, 50);
    // TODO: doit-on faire des tests de coh�rences pour savoir si le maillage est bien construit: au maximum 2 �l�ments
    // voisins pour chaque arete ?
    // coordonn�es temporaire pour calculer l'orientation
    final EfSegmentMutable tmp = new EfSegmentMutable(-1, -1);
    // indice permettant de connaitre l'arete en cours
    int compteurAretes = 0;
    for (int idxElt = 0; idxElt < eltNb; idxElt++) {
      if (stop_) {
        return false;
      }
      final EfElement vi = _grid.getElement(idxElt);
      // les indices des aretes associ�s � l'�l�ment
      final int[] arete = new int[vi.getNbEdge()];
      // on stocke le centre de l'�lement pour calculer l'orientation
      for (int idxAreteLocal = 0; idxAreteLocal < arete.length; idxAreteLocal++) {
        final int idx1 = vi.getEdgePt1(idxAreteLocal);
        final int idx2 = vi.getEdgePt2(idxAreteLocal);
        // cette methode permet de stocker le + petit indice en premier.
        tmp.setMinMaxIdx(idx1, idx2);
        // cette arete a d�j� �t� trouv�e ?
        int idxAreteGlobal = -1;
        if (edges.contains(tmp)) {
          idxAreteGlobal = edges.get(tmp);
        } else {
          // non on le cr�e
          idxAreteGlobal = compteurAretes;
          edges.put(new EfSegment(tmp), compteurAretes++);
        }
        // TODO si l'arete est infe � 0 on a un probleme
        arete[idxAreteLocal] = idxAreteGlobal;
        // on va mettre � jour les voisins de cette arete.
        int[] voisins = (int[]) areteVoisins.get(idxAreteGlobal);
        // c'est la premiere fois alors on l'initialise
        if (voisins == null) {
          voisins = new int[2];
          // on initialise avec la valeur -1 qui veut dire pas de voisins
          Arrays.fill(voisins, -1);
          areteVoisins.put(idxAreteGlobal, voisins);
        }
        int idxVoisins = 1;
        // element est dans le sens trigo
        boolean trigo = vi.isOrientedIn(_grid, true);
        // si gauche on met le tout � 0
        // l'�l�ment est a gauche de l'arete si on est dans le sens trigo et que l'arete est dans le sens de l'�l�ment
        // et inversement
        boolean isEdgeInMeshOrder = tmp.getPt1Idx() == idx1;
        if ((trigo && isEdgeInMeshOrder) || (!trigo && !isEdgeInMeshOrder)) {
          idxVoisins = 0;
        }
        // une erreur est pr�sente dans le maillage
        if (voisins[idxVoisins] >= 0) {
          _analyse.addFatalError(DodicoLib.getS("Le voisinage de l'ar�te {0} n'est pas valide", CtuluLibString
              .getString(idxAreteGlobal + 1)));
          return false;
        }
        voisins[idxVoisins] = idxElt;

      }
      elts_[idxElt] = new EfElementVolume(arete, vi.getPtIndex());
      up.majAvancement();
    }
    final int nbAretes = edges.size();
    // on stocke les aretes
    edges_ = new EfSegment[nbAretes];
    final TObjectIntIterator it = edges.iterator();
    for (int i = edges.size(); i-- > 0;) {
      it.advance();
      edges_[it.value()] = (EfSegment) it.key();
    }
    elementVoisinsParArete_ = new int[nbAretes][2];

    for (int i = 0; i < nbAretes; i++) {
      elementVoisinsParArete_[i] = (int[]) areteVoisins.get(i);
    }

    // pour les elements maintenants
    // pour chaque element, il y a autant de voisins potentiels que d'aretes
    elementVoisinsParElement_ = new int[eltNb][];

    // 2 elements sont voisins ( au sens volumique) s'ils ont une arete en commun.
    // Donc pour cr�er les listes les elements voisins, il suffit de parcourir le tableau des aretes et
    // d'enregister le fait que 2 elements ont une arete commune

    // on met a jour l'indicateur d'avancement.
    up.setValue(5, eltNb, 50, 50);
    up.majProgessionStateOnly();
    for (int idxElt = 0; idxElt < eltNb; idxElt++) {
      if (stop_) {
        return false;
      }
      final EfElementVolume vi = elts_[idxElt];
      final int nbAretesInElt = vi.getNbAretes();
      elementVoisinsParElement_[idxElt] = new int[nbAretesInElt];
      Arrays.fill(elementVoisinsParElement_[idxElt], -1);
      for (int k = 0; k < nbAretesInElt; k++) {
        final int idxArete = vi.getIdxArete(k);
        final int[] eltVoisinsPourArete = elementVoisinsParArete_[idxArete];
        int test = eltVoisinsPourArete[0];
        if (test >= 0 && test != idxElt) {
          elementVoisinsParElement_[idxElt][k] = test;
        } else {
          test = eltVoisinsPourArete[1];
          if (test >= 0 && test != idxElt) {
            elementVoisinsParElement_[idxElt][k] = test;
          }
        }
      }

      up.majAvancement();
    }
    return true;
  }

  /**
   * Pour les aretes possedant 2 voisins, les 2 indices seront >=0. Pour une arete frontiere, le premier indice vaut -1.
   * 
   * @return un tableau non null ( si process lance) de taille [ nbArete][2]
   */
  public int[][] getElementVoisinsParArete() {
    return elementVoisinsParArete_;
  }

  public int[][] getElementVoisinsParElement() {
    return elementVoisinsParElement_;
  }

  public EfElementVolume[] getElts() {
    return elts_;
  }

  public EfSegment[] getEdges() {
    return edges_;
  }

}
