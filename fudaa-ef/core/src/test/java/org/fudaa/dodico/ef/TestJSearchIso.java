/*
 * @creation 20 avr. 07
 * @modification $Date: 2007-06-11 13:07:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TIntIntHashMap;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.CtuluVariableDefault;
import org.fudaa.ctulu.gis.GISLigneBrisee;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.operation.EfIsoActivity;
import org.fudaa.dodico.ef.operation.EfIsoResultDefault;

/**
 * @author fred deniger
 * @version $Id: TestJSearchIso.java,v 1.4 2007-06-11 13:07:27 deniger Exp $
 */
public class TestJSearchIso extends TestCase {

  public static EfGridInterface getGrid(int _nbCol, int _nbRow) {
    EfNodeMutable[] nodes = new EfNodeMutable[_nbCol * _nbRow];
    double xFirst = 0;
    double yFirst = 0;
    double xDelta = 1;
    double yDelta = 1;
    for (int i = 0; i < nodes.length; i++) {
      int col = i % _nbCol;
      int ligne = (i - col) / _nbCol;
      nodes[i] = new EfNodeMutable(xFirst + col * xDelta, yFirst + ligne * yDelta, 0);
    }
    int nbColElt = _nbCol - 1;
    int nbRowElt = _nbRow - 1;
    EfElement[] elt = new EfElement[nbColElt * nbRowElt];
    for (int i = 0; i < elt.length; i++) {
      int[] idx = new int[4];
      int col = i % nbColElt;
      int ligne = (i - col) / nbColElt;
      // les deux indices du bas
      idx[0] = ligne * _nbCol + col;
      idx[1] = idx[0] + 1;
      // les deux du haut
      // l'ordre 1 puis 0 est normal
      idx[2] = idx[1] + _nbCol;
      idx[3] = idx[0] + _nbCol;
      elt[i] = new EfElement(idx);
    }
    return new EfGrid(nodes, elt);

  }

  protected static class DataAdapter implements EfGridData {

    final EfData values_;
    final EfGridInterface grid_;

    public DataAdapter(final EfGridInterface _grid, final EfData _values) {
      super();
      values_ = _values;
      grid_ = _grid;
    }

    @Override
    public double getData(CtuluVariable _o, int _timeIdx, int _idxObjet) throws IOException {
      return values_.getValue(_idxObjet);
    }

    @Override
    public EfData getData(CtuluVariable _o, int _timeIdx) throws IOException {
      return values_;
    }

    @Override
    public EfGridInterface getGrid() {
      return grid_;
    }

    @Override
    public boolean isDefined(CtuluVariable _var) {
      return true;
    }

    @Override
    public boolean isElementVar(CtuluVariable _idxVar) {
      return values_.isElementData();
    }

  }

  public void testForT4EltData() {
    EfGridInterface efGrid = getGrid(5, 5);
    double[] values = new double[efGrid.getEltNb()];
    double v = 5;
    // on initialise les 4 elements centraux.
    values[5] = v;
    values[6] = v;
    values[9] = v;
    values[10] = v;
    EfIsoResultDefault res = new EfIsoResultDefault();
    CtuluAnalyze an = new CtuluAnalyze();

    new EfIsoActivity(efGrid, new InterpolationVectorContainer()).search(new CtuluVariableDefault("toto"),
        new DataAdapter(efGrid, new EfDataElement(values)), 0, null, an).search(0, res, null, an);
    if (an.containsErrors()) an.printResume();
    assertFalse(an.containsErrors());
    assertEquals(1, res.getNumGeometries());
    assertTrue(res.getGeometry(0) instanceof GISLigneBrisee);
    // on va tester lorsque les valeurs sont d�finies sur les points
    values = new double[efGrid.getPtsNb()];
    initEltValues(values, v, efGrid, 5);
    initEltValues(values, v, efGrid, 6);
    initEltValues(values, v, efGrid, 9);
    initEltValues(values, v, efGrid, 10);
    res = new EfIsoResultDefault();
    new EfIsoActivity(efGrid, new InterpolationVectorContainer()).search(new CtuluVariableDefault("toto"),
        new DataAdapter(efGrid, new EfDataNode(values)), 0, null, an).search(0, res, null, an);;
    if (an.containsErrors()) an.printResume();
    assertFalse(an.containsErrors());
    assertEquals(1, res.getNumGeometries());
    assertTrue(res.getGeometry(0) instanceof GISLigneBrisee);
    testT3T6(v, 0, an, efGrid);

  }

  private void initEltValues(double[] _values, double _v, EfGridInterface _efGrid, int _idxElt) {
    EfElement elti = _efGrid.getElement(_idxElt);
    for (int i = elti.getPtNb() - 1; i >= 0; i--) {
      _values[elti.getPtIndex(i)] = _v;
    }
  }

  private void testT3T6(double _v, double _vs, CtuluAnalyze _an, EfGridInterface _efGrid) {
    double[] values;
    EfIsoResultDefault res;
    // dans un maillage T3
    res = new EfIsoResultDefault();
    TIntIntHashMap newEltOldElt = new TIntIntHashMap();
    EfGridInterface toT3 = EfLibImpl.toT3(_efGrid, null, _an, newEltOldElt);
    values = new double[toT3.getPtsNb()];
    for (int i = 0; i < toT3.getEltNb(); i++) {
      int old = newEltOldElt.get(i);
      if (old == 5 || old == 6 || old == 9 || old == 10) {
        for (int j = 0; j < toT3.getElement(i).getPtNb(); j++) {
          values[toT3.getElement(i).getPtIndex(j)] = _v;
        }
      }
    }
    new EfIsoActivity(toT3, new InterpolationVectorContainer()).search(new CtuluVariableDefault("toto"),
        new DataAdapter(toT3, new EfDataNode(values)), 0, null, _an).search(_vs, res, null, _an);
    if (_an.containsErrors()) _an.printResume();
    assertFalse(_an.containsErrors());
    assertEquals(1, res.getNumGeometries());
    assertTrue(res.getGeometry(0) instanceof GISLigneBrisee);
    values = new double[toT3.getEltNb()];
    for (int i = 0; i < toT3.getEltNb(); i++) {
      int old = newEltOldElt.get(i);
      if (old == 5 || old == 6 || old == 9 || old == 10) {
        values[i] = _v;
      }
    }
    res = new EfIsoResultDefault();
    new EfIsoActivity(toT3, new InterpolationVectorContainer()).search(new CtuluVariableDefault("toto"),
        new DataAdapter(toT3, new EfDataElement(values)), 0, null, _an).search(_vs, res, null, _an);
    if (_an.containsErrors()) _an.printResume();
    assertFalse(_an.containsErrors());
    assertEquals(1, res.getNumGeometries());
    assertTrue(res.getGeometry(0) instanceof GISLigneBrisee);
    Geometry g0 = res.getGeometry(0);

    // t3 en t6 et donn�e d�finie sur les �l�ments
    res = new EfIsoResultDefault();
    EfGridInterface maillageT3enT6 = EfLibImpl.maillageT3enT6(toT3, null, null);
    assertEquals(maillageT3enT6.getEltNb(), values.length);
    new EfIsoActivity(maillageT3enT6, new InterpolationVectorContainer()).search(new CtuluVariableDefault("toto"),
        new DataAdapter(maillageT3enT6, new EfDataElement(values)), 0, null, _an).search(_vs, res, null, _an);
    if (_an.containsErrors()) _an.printResume();
    assertFalse(_an.containsErrors());
    assertEquals(1, res.getNumGeometries());
    Geometry g2 = res.getGeometry(0);
    assertTrue(g2 instanceof GISLigneBrisee);
    // on doit avoir le meme r�sultat
    assertEquals(g2.getNumPoints(), g0.getNumPoints());
    Coordinate[] c0 = g0.getCoordinates();
    Coordinate[] c1 = g2.getCoordinates();
    for (int i = c0.length - 1; i >= 0; i--) {
      assertTrue(c0[i].equals2D(c1[i]));
    }
  }

//  public void testForZero() {
//    EfGridInterface g = getGrid(4, 4);
//    double[] v = new double[g.getEltNb()];
//    v[3] = 1;
//    v[4] = 1;
//    v[5] = 1;
//    v[6] = 1;
//    v[7] = 1;
//    v[8] = 1;
//
//    CtuluArrayDouble vs = new CtuluArrayDouble(1);
//    CtuluAnalyze an = new CtuluAnalyze();
//    EfIsoResultDefault res = new EfIsoResultDefault();
//    EfIsoActivitySearcher searcher = new EfIsoActivity(g, new InterpolationVectorContainer()).search(
//        new CtuluVariableDefault("toto"), new DataAdapter(g, new EfDataElement(v)), 0, null, an);
//    searcher.search(0, res, null, an);
//    if (an.containsErrors()) an.printResume();
//    assertFalse(an.containsErrors());
//    assertEquals(1, res.getNumGeometries());
//    assertEquals(4, res.getGeometry(0).getNumPoints());
//    res = new EfIsoResultDefault();
//    vs.set(0, 1);
//    searcher.search(1, res, null, an);
//    if (an.containsErrors()) an.printResume();
//    assertFalse(an.containsErrors());
//    assertEquals(1, res.getNumGeometries());
//    assertEquals(4, res.getGeometry(0).getNumPoints());
//  }

//  public void testForCroix() {
//    EfGridInterface g = getGrid(6, 6);
//    double[] v = new double[g.getEltNb()];
//    v[2] = 1;
//    v[7] = 1;
//    v[10] = 1;
//    v[11] = 1;
//    v[12] = 1;
//    v[13] = 1;
//    v[14] = 1;
//    v[17] = 1;
//    v[22] = 1;
//    CtuluAnalyze an = new CtuluAnalyze();
//    EfIsoResultDefault res = new EfIsoResultDefault();
//    new EfIsoActivity(g, new InterpolationVectorContainer()).search(new CtuluVariableDefault("toto"),
//        new DataAdapter(g, new EfDataElement(v)), 0, null, an).search(1, res, null, an);
//    if (an.containsErrors()) an.printResume();
//    assertEquals(4, res.getNumGeometries());
//    // du fait de la simplification des lignes, elles doivent comporter toutes 2 points.
//    Coordinate center = new Coordinate(2.5, 2.5);
//    Coordinate[] ext = new Coordinate[4];
//    ext[0] = new Coordinate(0.5, 2.5);
//    ext[1] = new Coordinate(2.5, 0.5);
//    ext[2] = new Coordinate(2.5, 4.5);
//    ext[3] = new Coordinate(4.5, 2.5);
//    for (int i = 0; i < res.getNumGeometries(); i++) {
//      assertEquals(2, res.getGeometry(i).getNumPoints());
//      Coordinate c1 = res.getGeometry(i).getCoordinates()[0];
//      Coordinate c2 = res.getGeometry(i).getCoordinates()[1];
//      if (c1.equals2D(center)) {
//        assertTrue(getIdx(c2, ext) >= 0);
//      } else {
//        assertTrue(c2.equals2D(center));
//        assertTrue(getIdx(c1, ext) >= 0);
//      }
//    }
//
//  }

  public static int getIdx(Coordinate _c, Coordinate[] _cs) {
    for (int i = _cs.length - 1; i >= 0; i--) {
      if (_cs[i].equals2D(_c)) return i;
    }
    return -1;
  }
}
