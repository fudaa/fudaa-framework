package org.fudaa.dodico.ef.frontier;

import org.locationtech.jts.geom.Coordinate;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluResult;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridArray;

public class TestEfFrontierMapperActivity extends TestCase {

  public void testMapper() {
    EfNode[] nodes1 = new EfNode[15];
    EfElement[] elements1 = new EfElement[11];
    EfNode[] nodes2 = new EfNode[16];
    EfElement[] elements2 = new EfElement[12];

    nodes1[0] = new EfNode(new Coordinate(0, 70));
    nodes1[1] = new EfNode(new Coordinate(20, 120));
    nodes1[2] = new EfNode(new Coordinate(60, 160));
    nodes1[3] = new EfNode(new Coordinate(120, 140));
    nodes1[4] = new EfNode(new Coordinate(180, 160));
    nodes1[5] = new EfNode(new Coordinate(260, 120));
    nodes1[6] = new EfNode(new Coordinate(280, 80));
    nodes1[7] = new EfNode(new Coordinate(240, 20));
    nodes1[8] = new EfNode(new Coordinate(180, 0));
    nodes1[9] = new EfNode(new Coordinate(100, 0));
    nodes1[10] = new EfNode(new Coordinate(40, 40));
    nodes1[11] = new EfNode(new Coordinate(100, 70));
    nodes1[12] = new EfNode(new Coordinate(140, 100));
    nodes1[13] = new EfNode(new Coordinate(150, 30));
    nodes1[14] = new EfNode(new Coordinate(180, 60));

    elements1[0] = new EfElement(new int[] { 0, 1, 11 });
    elements1[1] = new EfElement(new int[] { 1, 2, 11 });
    elements1[2] = new EfElement(new int[] { 2, 3, 12, 11 });
    elements1[3] = new EfElement(new int[] { 3, 4, 12 });
    elements1[4] = new EfElement(new int[] { 4, 5, 14, 12 });
    elements1[5] = new EfElement(new int[] { 5, 6, 14 });
    elements1[6] = new EfElement(new int[] { 6, 7, 14 });
    elements1[7] = new EfElement(new int[] { 7, 8, 13, 14 });
    elements1[8] = new EfElement(new int[] { 8, 9, 13 });
    elements1[9] = new EfElement(new int[] { 9, 10, 11, 13 });
    elements1[10] = new EfElement(new int[] { 10, 0, 11 });

    nodes2[0] = new EfNode(new Coordinate(60, 160));
    nodes2[1] = new EfNode(new Coordinate(120, 140));
    nodes2[2] = new EfNode(new Coordinate(180, 160));
    nodes2[3] = new EfNode(new Coordinate(160, 80));
    nodes2[4] = new EfNode(new Coordinate(180, 60));
    nodes2[5] = new EfNode(new Coordinate(150, 30));
    nodes2[6] = new EfNode(new Coordinate(180, 0));
    nodes2[7] = new EfNode(new Coordinate(260, 120));
    nodes2[8] = new EfNode(new Coordinate(0, 70));
    nodes2[9] = new EfNode(new Coordinate(20, 120));
    nodes2[10] = new EfNode(new Coordinate(140, 100));
    nodes2[11] = new EfNode(new Coordinate(240, 20));
    nodes2[12] = new EfNode(new Coordinate(280, 80));
    nodes2[13] = new EfNode(new Coordinate(40, 40));
    nodes2[14] = new EfNode(new Coordinate(60, 0));
    nodes2[15] = new EfNode(new Coordinate(110, 100));

    elements2[0] = new EfElement(new int[] { 8, 9, 15 });
    elements2[1] = new EfElement(new int[] { 9, 0, 15 });
    elements2[2] = new EfElement(new int[] { 0, 1, 10, 15 });
    elements2[3] = new EfElement(new int[] { 1, 2, 10 });
    elements2[4] = new EfElement(new int[] { 2, 7, 4, 3 });
    elements2[5] = new EfElement(new int[] { 7, 12, 4 });
    elements2[6] = new EfElement(new int[] { 12, 11, 4 });
    elements2[7] = new EfElement(new int[] { 11, 6, 5, 4 });
    elements2[8] = new EfElement(new int[] { 6, 14, 5 });
    elements2[9] = new EfElement(new int[] { 14, 13, 15, 5 });
    elements2[10] = new EfElement(new int[] { 13, 8, 15 });
    elements2[11] = new EfElement(new int[] { 2, 3, 10 });

    EfGridInterface grid1 = new EfGridArray(nodes1, elements1);
    EfGridInterface grid2 = new EfGridArray(nodes2, elements2);

    grid1.computeBord(null, null);
    grid2.computeBord(null, null);

    EfFrontierMapperActivity activity = new EfFrontierMapperActivity();

    activity.setOldGrid(grid1);
    activity.setNewGrid(grid2);

    CtuluResult<EfFrontierMapper> res = activity.compute(null);

    EfFrontierMapper mapper = res.getResultat();

    // Test si les anciens points ne sont pas not� comme des nouveaux.
    //le 0 est nouveau
    assertTrue(mapper.isNewFrontierNode(0));
    // Test si les nouveaux points sont correctement d�tect�s.
    assertTrue(mapper.isNewFrontierNode(12));
    assertTrue(mapper.isNewFrontierNode(14));
    assertEquals(-1,mapper.getOldFrtIdx(0));
    assertEquals(-1,mapper.getOldFrtIdx(12));
    assertEquals(-1,mapper.getOldFrtIdx(14));
    
    assertEquals(3,mapper.getOldFrtIdx(1));
    assertEquals(4,mapper.getOldFrtIdx(2));
    assertEquals(5,mapper.getOldFrtIdx(3));
    assertEquals(6,mapper.getOldFrtIdx(4));
    assertEquals(7,mapper.getOldFrtIdx(5));
    assertEquals(8,mapper.getOldFrtIdx(6));
    assertEquals(9,mapper.getOldFrtIdx(7));
    assertEquals(10,mapper.getOldFrtIdx(8));
    assertEquals(0,mapper.getOldFrtIdx(9));
    assertEquals(1,mapper.getOldFrtIdx(10));
    assertEquals(14,mapper.getOldFrtIdx(11));
    assertEquals(12,mapper.getOldFrtIdx(13));
    assertEquals(13,mapper.getOldFrtIdx(15));

  }
}
