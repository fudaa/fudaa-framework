package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.index.quadtree.Quadtree;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestElt;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestNode;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNodeSelect;
import org.fudaa.dodico.ef.operation.EfIndexerActivity;

public final class TestJSearchInGrid extends TestCase {

  public TestJSearchInGrid() {}

  static double getPrecision() {
    return 2;
  }

  public void testIndexer() {
    // construction d'un maillage simple
    final EfGridInterface grid = EfTestFactory.buildGrid(10, 10);
    assertNotNull(grid);
    assertEquals(100, grid.getPtsNb());
    final CtuluAnalyze analyze = new CtuluAnalyze();
    grid.computeBord(null, analyze);
    assertFalse(analyze.containsErrors());
    assertFalse(analyze.containsFatalError());
    // on commence les vrais tests
    testIndexer(grid);
  }

  private void testIndexer(final EfGridInterface _grid) {
    // test basique du spliter
    final EfIndexInterface index = new EfIndexerActivity(_grid).createRegularIndex(null);
    assertNotNull(index);
    // on verifie que tous les zones sont non nulles
    final TIntHashSet set = new TIntHashSet(_grid.getEltNb());
    for (int i = index.getNbEnv() - 1; i >= 0; i--) {
      assertNotNull(index.getEnvelope(i));
      index.fill(i, set);
    }
    // et que tous les elts soient bien compris dans les zones
    assertEquals(_grid.getEltNb(), set.size());
    final int[] idxs = set.toArray();
    Arrays.sort(idxs);
    for (int i = idxs.length - 1; i >= 0; i--) {
      assertEquals(i, idxs[i]);
    }

    // on test que tous les points du maillages soient bien compris dans les
    // envelopes
    final Envelope[] envelopes = index.getEnvs();
    for (int i = _grid.getPtsNb() - 1; i >= 0; i--) {
      assertTrue(isContained(envelopes, _grid.getPtX(i), _grid.getPtY(i)));
    }
    // on construit le quadtree
    int idx = _grid.getPtsNb() / 2;
    // on verifie les selectionneur de noeuds/elements
    final EfIndexVisitorNearestNode select = new EfIndexVisitorNearestNode(_grid, _grid.getPtX(idx), _grid.getPtY(idx), 1);
    index.query(select.buildSearchEnvelop(), select);
    assertEquals(idx, select.getNodeSelected());
    idx = _grid.getEltNb() / 2;
    final EfIndexVisitorNearestElt selectElt = new EfIndexVisitorNearestElt(_grid, _grid.getCentreXElement(idx), _grid
        .getCentreYElement(idx), 1);
    index.query(selectElt.buildSearchEnvelop(), selectElt);
    assertEquals(idx, selectElt.getSelected());
  }

  public void performance() {
    final EfGridInterface grid = EfTestFactory.buildGrid(1000, 500);
    assertNotNull(grid);
    // on test sur la recherche de 10 points
    final int[][] resIdx = doNaiveMethode(grid, 10, 20);
    doInitQuadTree(grid, 10, 20);
    final int[][] resIdxEF = doEfQuadTree(grid, 10, 20);
    doEfQuadTree(grid, 10, 20);
    System.out.println("\ncomparaison resultat");
    for (int i = resIdx.length - 1; i >= 0; i--) {
      Arrays.sort(resIdx[i]);
      Arrays.sort(resIdxEF[i]);

      if (!Arrays.equals(resIdx[i], resIdxEF[i])) {
        System.err.println("erreur entre les 2 m�thodes pour i=" + i);
        System.err.println(CtuluLibString.arrayToString(resIdx[i]));
        System.err.println(CtuluLibString.arrayToString(resIdxEF[i]));
      }
    }
    System.out.println("comparaison resultat FIN");
  }

  public boolean isContained(final Envelope[] _env, final double _x, final double _y) {
    for (int i = _env.length - 1; i >= 0; i--) {
      if (_env[i].contains(_x, _y)) {
        return true;
      }
    }
    return false;
  }

  private static int[][] doNaiveMethode(final EfGridInterface _grid, final int _middle, final int _max) {
    final long t0 = System.currentTimeMillis();
    int count = 0;
    final int[][] resIdx = new int[_max - _middle][];
    for (int i = _middle; i < _max; i++) {
      // on va rechercher le point
      final double x = _grid.getPtX(i) + 0.4;
      final double y = _grid.getPtY(i) + 0.2;
      resIdx[count++] = getPointsNear(x, y, getPrecision(), _grid).toNativeArray();
    }
    System.out.println("temps pris pour normal " + (System.currentTimeMillis() - t0));
    return resIdx;
  }

  private static int[][] doInitQuadTree(final EfGridInterface _grid, final int _middle, final int _max) {
    final Envelope env = new Envelope();
    long t0 = System.currentTimeMillis();
    final Quadtree tree = buildQuadTree(_grid);
    System.out.println("NORM: temps pris pour construction quadtree: " + (System.currentTimeMillis() - t0));
    t0 = System.currentTimeMillis();
    final int[][] resIdxQuad = new int[_max - _middle][];
    int count = 0;
    final double precision = getPrecision();
    final double d2 = precision * precision;
    for (int i = _middle; i < _max; i++) {
      // on va rechercher le point
      final double x = _grid.getPtX(i) + 0.4;
      final double y = _grid.getPtY(i) + 0.2;
      env.setToNull();

      env.expandToInclude(x + precision, y + precision);
      env.expandToInclude(x - precision, y - precision);
      final List l = tree.query(env);
      final TIntHashSet res = new TIntHashSet(l.size() + 3);
      for (int j = l.size() - 1; j >= 0; j--) {
        final EfElement el = (EfElement) l.get(j);
        for (int k = el.getPtNb() - 1; k >= 0; k--) {
          final int ptIndex = el.getPtIndex(k);
          if (CtuluLibGeometrie.getD2(_grid.getPtX(ptIndex), _grid.getPtY(ptIndex), x, y) <= d2) {
            res.add(ptIndex);
          }
        }
      }
      resIdxQuad[count++] = res.toArray();
    }
    System.out.println("NORM: temps pris pour recherche quad " + (System.currentTimeMillis() - t0));
    return resIdxQuad;
  }

  private static int[][] doEfQuadTree(final EfGridInterface _grid, final int _middle, final int _max) {
    final Envelope env = new Envelope();
    long t0 = System.currentTimeMillis();
    final EfIndexInterface tree = new EfIndexerActivity(_grid).createRegularIndex(null);
    System.out.println("EF: temps pris pour construction EF quadtree: " + (System.currentTimeMillis() - t0));
    t0 = System.currentTimeMillis();
    final int[][] resIdxQuadEF = new int[_max - _middle][];
    int count = 0;
    final double precision = getPrecision();
    final EfIndexVisitorNodeSelect selec = new EfIndexVisitorNodeSelect(_grid, precision);
    for (int i = _middle; i < _max; i++) {
      // on va rechercher le point
      final double x = _grid.getPtX(i) + 0.4;
      final double y = _grid.getPtY(i) + 0.2;
      selec.initFor(x, y);
      env.setToNull();
      env.expandToInclude(x, y);
      env.expandBy(precision, precision);
      tree.query(env, selec);
      resIdxQuadEF[count++] = selec.getNodeIdx();
    }
    System.out.println("EF: temps pris pour recherche quad " + (System.currentTimeMillis() - t0));
    return resIdxQuadEF;
  }

  public static Quadtree buildQuadTree(final EfGridInterface _grid) {
    final Quadtree res = new Quadtree();
    for (int i = _grid.getEltNb() - 1; i >= 0; i--) {
      final EfElement el = _grid.getElement(i);
      res.insert(el.getEnv(_grid), el);
    }
    return res;

  }

  public static TIntArrayList getPointsNear(final double _x, final double _y, final double _ecart, final EfGridInterface _g) {
    final TIntArrayList res = new TIntArrayList();
    final double e2 = _ecart * _ecart;
    for (int i = _g.getPtsNb() - 1; i >= 0; i--) {
      if (CtuluLibGeometrie.getD2(_x, _y, _g.getPtX(i), _g.getPtY(i)) <= e2) {
        res.add(i);
      }
    }
    return res;
  }

//  public static void main(final String[] _args) {
//    final EfGridInterface grid = EfTestFactory.buildGrid(510, 1000);
//    final File dest = new File("C:\\million.cor");
//    final EfGridInterface destGrid= EfLibImpl.maillageT3enT6(grid, null, null);
//    System.out.println("nombre de points "+destGrid.getPtsNb());
//    CorEleBthFileFormat.getInstance().writeGrid(dest,destGrid, null);
//    System.out.println("fin");
//  }
}
