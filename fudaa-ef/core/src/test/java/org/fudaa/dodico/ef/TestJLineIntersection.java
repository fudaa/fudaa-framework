/*
 * @creation 27 oct. 06
 * @modification $Date: 2007-06-13 12:57:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import java.util.List;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.CtuluVariableDefault;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.ef.TestJSearchIso.DataAdapter;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.interpolation.EfGridDataInterpolator;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestElt;
import org.fudaa.dodico.ef.operation.EfIndexVisitorNearestNode;
import org.fudaa.dodico.ef.operation.EfLineIntersection;
import org.fudaa.dodico.ef.operation.EfLineIntersectionI;
import org.fudaa.dodico.ef.operation.EfLineIntersectionParent;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsCorrectionTester;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsBuilder;
import org.fudaa.dodico.ef.operation.EfLineIntersectionsResultsI;
import org.fudaa.dodico.ef.operation.EfLineIntersectorActivity;

/**
 * @author fred deniger
 * @version $Id: TestJLineIntersection.java,v 1.10 2007-06-13 12:57:28 deniger Exp $
 */
public class TestJLineIntersection extends TestCase {

  public void testNodeIntersect() {
    final EfGridInterface grid = EfTestFactory.buildGrid(10, 10);
    assertNotNull(grid);
    /**
     * <code>
     *   45+-----+
     *     |   / |
     *     |  /  |
     *     | /   |
     *    5+-----+
     *     |   / |
     *     |  /  |
     *     | /   | 
     *     +-----+----+----+----+----+
     *     0     10   20   30 .....  90
     * </code>
     */
    final LineString str = GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] { new Coordinate(0, 0),
        new Coordinate(0, 5), new Coordinate(0, 45), new Coordinate(0, 50) // dernier point en dehors
        });
    CtuluVariable varNode = new CtuluVariableDefault("varNode");
    CtuluVariable varElt = new CtuluVariableDefault("varElt");
    EfGridDataDefault def = new EfGridDataDefault(grid);
    EfDataNode dataNode = createDataNodes(grid);
    InterpolationVectorContainer interpolationVectorContainer = new InterpolationVectorContainer();
    EfDataElement dataElt = EfLib.getElementData(null, dataNode, grid, null, interpolationVectorContainer);
    def.put(varNode, dataNode);
    def.put(varElt, dataElt);
    EfGridDataInterpolator support = new EfGridDataInterpolator(def, interpolationVectorContainer);
    EfLineIntersectorActivity act = new EfLineIntersectorActivity(support);
    EfLineIntersectionsResultsI res = act.computeForNodes(str, null).getDefaultRes();
    assertNotNull(res);
    // les 10 d'intersection + le point exterieur de (0,50)
    assertEquals(11, res.getNbIntersect());

    EfNeighborMesh neighbor = EfNeighborMesh.compute(grid, null);
    final double eps = 1E-5;

    for (int i = 0; i < res.getNbIntersect() - 1; i++) {
      EfLineIntersection inter = res.getIntersect(i);
      assertNotNull(inter);
      assertTrue(getAssertTitle(i, inter), inter.isRealIntersection());
      assertEquals(inter.getX(), 0, eps);
      assertEquals(getAssertTitle(i, inter), inter.getClass(), EfLineIntersection.ItemNode.class);
      assertTrue(inter.isNodeUsed(i * 10));
      assertEquals(dataNode.getValue(i * 10), inter.getValue(varNode, 0), eps);
      assertEquals(neighbor.getAverageForNode(null, i * 10, dataElt, null, interpolationVectorContainer), inter
          .getValue(varElt, 0), eps);
      if (i < 9) assertTrue("segment " + i, res.isSegmentIn(i));

    }
    assertFalse("segment 10", !res.isSegmentIn(10));
    assertEquals(0, res.getIntersect(10).getX(), eps);
    assertEquals(50, res.getIntersect(10).getY(), eps);
    testForCorrector(res);

    res = act.computeForMeshes(str, null).getDefaultRes();
    assertNotNull(res);
    // les 10 noeuds d'intersection + 9 centres et le point exterieur de (0,50)
    assertEquals(13, res.getNbIntersect());
    assertEquals(0, res.getIntersect(res.getNbIntersect() - 1).getX(), eps);
    assertEquals(50, res.getIntersect(res.getNbIntersect() - 1).getY(), eps);

  }

  private void testForCorrector(EfLineIntersectionsResultsI _mng) {

    EfLineIntersectionsResultsBuilder builder = new EfLineIntersectionsResultsBuilder(null, _mng, null);
    EfLineIntersectionsResultsI res = builder.createResults(0, null);
    assertNotNull(res);
    assertTrue(_mng == res);

    builder = new EfLineIntersectionsResultsBuilder(null, _mng, new DefaultTester());
    res = builder.createResults(0, null);
    assertNotNull(res);
    assertFalse(_mng == res);
    assertTrue(builder.isPresentInPool(0));
    assertTrue(builder.isSavedInPool(0));
    int nbOut = 0;
    for (int i = 0; i < _mng.getNbIntersect() - 1; i++) {
      if (_mng.isSegmentOut(i)) nbOut++;
    }
    assertEquals(_mng.getNbIntersect() * 2 - 1 - nbOut, res.getNbIntersect());
    for (int i = 0; i < res.getNbIntersect(); i += 2) {
      assertEquals(_mng.getIntersect(i / 2), res.getIntersect(i));
    }

  }

  protected class DefaultTester implements EfLineIntersectionsCorrectionTester {
    
    

    @Override
    public boolean createNews(int _tidx, EfLineIntersection _i1, EfLineIntersection _i2, List _setNewIn) {
      EfLineIntersectionParent parent = _i1.getParent();
      double newX = (_i1.getX() + _i2.getX()) / 2;
      double newY = (_i1.getY() + _i2.getY()) / 2;
      EfIndexVisitorNearestElt elt = new EfIndexVisitorNearestElt(parent.getGrid(), newX, newY, 1E-3);
      parent.getGrid().getIndex().query(EfIndexVisitorNearestNode.getEnvelope(newX, newY, 1E-2), elt);
      if (elt.isIn()) {
        _setNewIn.add(new EfLineIntersection.ItemMesh(elt.getSelected(), newX, newY));
        return true;
      }
      fail("must be in");
      return false;

    }

    @Override
    public boolean isEnableFor(EfLineIntersectionsResultsI _res) {
      return true;
    }
  }

  private EfDataNode createDataNodes(EfGridInterface _grid) {
    double[] vals = new double[_grid.getPtsNb()];
    for (int i = vals.length - 1; i >= 0; i--) {
      vals[i] = Math.random() * 100;
    }
    EfDataNode dataNode = new EfDataNode(vals);
    return dataNode;
  }

  public void testForNewBogue() {
    EfNode[] nds = new EfNode[8];
    int idx = 0;
    /* 0 */nds[idx++] = new EfNode(0, 0, 0);
    /* 1 */nds[idx++] = new EfNode(1, 0, 0);
    /* 2 */nds[idx++] = new EfNode(3, 0, 0);
    /* 3 */nds[idx++] = new EfNode(0, 1, 0);
    /* 4 */nds[idx++] = new EfNode(1, 2, 0);
    /* 5 */nds[idx++] = new EfNode(3, 3, 0);
    /* 6 */nds[idx++] = new EfNode(2, 4, 0);
    /* 7 */nds[idx++] = new EfNode(4, 1, 0);
    EfElement[] elts = new EfElement[5];
    idx = 0;
    /* 0 */elts[idx++] = new EfElement(new int[] { 0, 1, 4, 3 });
    /* 1 */elts[idx++] = new EfElement(new int[] { 1, 2, 4 });
    /* 2 */elts[idx++] = new EfElement(new int[] { 2, 7, 5, 4 });
    /* 3 */elts[idx++] = new EfElement(new int[] { 3, 4, 6 });
    /* 4 */elts[idx++] = new EfElement(new int[] { 4, 5, 6 });
    EfGrid grid = new EfGrid(nds, elts);
    final LineString str = GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] { new Coordinate(1, 0),
        new Coordinate(1, 2), new Coordinate(3, 3) });
    DataAdapter def = new DataAdapter(grid, null);
    EfGridDataInterpolator support = new EfGridDataInterpolator(def, new InterpolationVectorContainer());
    EfLineIntersectorActivity act = new EfLineIntersectorActivity(support);
    EfLineIntersectionsResultsI res = act.computeForMeshes(str, null).getDefaultRes();
    assertNotNull(res);
    assertEquals(5, res.getNbIntersect());
    res = act.computeForNodes(str, null).getDefaultRes();
    assertNotNull(res);
    assertEquals(3, res.getNbIntersect());

  }

  public void testLigneV2() {
    EfGridInterface grid = EfTestFactory.buildGrid(10, 10);
    assertNotNull(grid);
    double yDeb = -1;
    final LineString str = GISGeometryFactory.INSTANCE.createLineString(new Coordinate[] { new Coordinate(5, yDeb),// premier
        new Coordinate(5, 50) // dernier point en dehors
        });
    EfData dataNode = createDataNodes(grid);
    EfData dataElt = EfLib.getElementData(null, dataNode, grid, null, new InterpolationVectorContainer());
    EfNeighborMesh neighbor = EfNeighborMesh.compute(grid, null);
    EfGridDataDefault def = new EfGridDataDefault(grid);
    CtuluVariable varNode = new CtuluVariableDefault("varNode");
    CtuluVariable varElt = new CtuluVariableDefault("varElt");
    def.put(varNode, dataNode);
    def.put(varElt, dataElt);
    EfGridDataInterpolator support = new EfGridDataInterpolator(def, null);
    EfLineIntersectorActivity act = new EfLineIntersectorActivity(support);
    EfLineIntersectionsResultsI res = act.computeForNodes(str, null).getDefaultRes();
    assertNotNull(res);
    // 2 point exterieurs+ 10 aretes horizontales + 9 aretes obliques
    assertEquals(21, res.getNbIntersect());
    assertFalse(res.getIntersect(0).isRealIntersection());
    assertFalse(res.getIntersect(20).isRealIntersection());
    final double eps = 1E-5;
    assertEquals(5, res.getIntersect(0).getX(), eps);
    assertEquals(yDeb, res.getIntersect(0).getY(), eps);
    assertTrue(res.isSegmentOut(0));
    // le dernier point
    assertEquals(5, res.getIntersect(20).getX(), eps);
    assertEquals(50, res.getIntersect(20).getY(), eps);
    // le dernier segment le 20eme segment
    assertTrue(res.isSegmentOut(19));

    for (int i = 1; i < 20; i++) {
      if (i < 19) assertTrue(res.isSegmentIn(i));
      EfLineIntersectionI inter = res.getIntersect(i);
      assertTrue(inter.isRealIntersection());
      assertTrue(inter.isMeshIntersection() < 0);
      assertTrue(inter.isEdgeIntersection());
      assertEquals((i - 1) * 2.5, inter.getY(), eps);
      assertEquals(5, inter.getX(), eps);
      assertTrue(inter instanceof EfLineIntersection.ItemEdge);
      EfLineIntersection.ItemEdge resi = (EfLineIntersection.ItemEdge) inter;
      double v1 = dataNode.getValue(resi.getI1());
      double v2 = dataNode.getValue(resi.getI2());
      assertEquals((v1 + v2) * 0.5, inter.getValue(varNode, 0), eps);
      int[] elt = neighbor.getAdjacentMeshes(resi.getI1(), resi.getI2());
      assertNotNull(elt);
      assertEquals(2, elt.length);
      assertTrue(elt[0] >= 0);
      v1 = dataElt.getValue(elt[0]);

      v2 = elt[1] >= 0 ? dataElt.getValue(elt[1]) : v1;
      assertEquals((v1 + v2) * 0.5, inter.getValue(varElt, 0), eps);
    }
  }

  private String getAssertTitle(int _i, EfLineIntersection _inter) {
    return "indice " + _i + " class " + _inter.getClass().getName();
  }

}
