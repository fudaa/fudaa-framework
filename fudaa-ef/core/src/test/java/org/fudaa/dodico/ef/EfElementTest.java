package org.fudaa.dodico.ef;

import junit.framework.TestCase;
import org.fudaa.dodico.ef.impl.EfGrid;

import java.util.ArrayList;
import java.util.List;

public class EfElementTest extends TestCase {
  public void testOrientSensTrigo() {
    List<EfNode> nodes = new ArrayList<>();
    nodes.add(new EfNode(5, 0.125, 0));
    nodes.add(new EfNode(5.1, 0.125, 0));
    nodes.add(new EfNode(5.1, 0, 0));
    EfGrid grid = new EfGrid(nodes.toArray(new EfNode[nodes.size()]), new EfElement[]{new EfElement(new int[]{0, 1, 2})});
    EfElement element = grid.getElement(0);
    assertFalse(element.isOrientedIn(grid, true));
    element.orienteDansSensTrigo(grid);
    assertTrue(element.isOrientedIn(grid, true));
    assertEquals(0, element.getPtIndex(0));
    assertEquals(2, element.getPtIndex(1));
    assertEquals(1, element.getPtIndex(2));

    nodes.add(new EfNode(4.8, 0.25, 0));
    grid = new EfGrid(nodes.toArray(new EfNode[nodes.size()]), new EfElement[]{new EfElement(new int[]{0, 1, 2, 3})});
    element = grid.getElement(0);
    assertFalse(element.isOrientedIn(grid, true));
    element.orienteDansSensTrigo(grid);
    assertTrue(element.isOrientedIn(grid, true));
  }
}
