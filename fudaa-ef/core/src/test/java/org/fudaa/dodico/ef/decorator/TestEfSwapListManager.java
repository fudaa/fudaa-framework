package org.fudaa.dodico.ef.decorator;

import junit.framework.TestCase;

public class TestEfSwapListManager extends TestCase {

    public void testSwap() {
        EfSwapListManager<String> tested = createInitManager();
        tested.swap(0, 2);
        assertEquals("trois", tested.get(0));
        assertEquals("un", tested.get(2));
        assertEquals(2, tested.getOldPosition(0));
        assertEquals(1, tested.getOldPosition(1));
        assertEquals(0, tested.getOldPosition(2));
    }

    public void testSwapInutile() {

        //on reswap et dans ce cas, on doit se trouver dans le cas initial
        EfSwapListManager<String> tested = createInitManager();
        tested.swap(0, 2);
        tested.swap(0, 2);
        assertEquals("un", tested.get(0));
        assertEquals("deux", tested.get(1));
        assertEquals("trois", tested.get(2));
        assertEquals(0, tested.getOldPosition(0));
        assertEquals(1, tested.getOldPosition(1));
        assertEquals(2, tested.getOldPosition(2));
        assertEquals(0, tested.getNewIdxOldIdx().size());
        //2 swap consecutifs
    }

    public void test2Swaps() {
        EfSwapListManager<String> tested = createInitManager();
        tested.swap(0, 1);
        tested.swap(1, 2);
        assertEquals("deux", tested.get(0));
        assertEquals("trois", tested.get(1));
        assertEquals("un", tested.get(2));
        assertEquals(3, tested.getNewIdxOldIdx().size());
        assertEquals(1, tested.getOldPosition(0));
        assertEquals(2, tested.getOldPosition(1));
        assertEquals(0, tested.getOldPosition(2));
    }

    private EfSwapListManager<String> createInitManager() {
        return new EfSwapListManager<String>("un", "deux", "trois");
    }

}
