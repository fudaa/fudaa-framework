package org.fudaa.dodico.ef.operation.overstressed;

import gnu.trove.TIntHashSet;
import java.util.HashSet;
import java.util.Set;
import junit.framework.TestCase;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNeighborMesh;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.ef.EfTestFactory;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.operation.EfOperationResult;

public class TestEfOperationSwapSelectedEdge extends TestCase
{
	public void testFindEdgeToSwap()
	{
		final int nbLine =  5;
		final int nbPts  = 10;
		//TODO Tester les datas.
		
        //on cr�� un maillage simple de 50 point: 5 lignes contenant 10 point
        EfGridInterface defGrid = EfTestFactory.buildGrid(nbLine, nbPts);
        
        EfGridData defDataGrid = new EfGridDataDefault(defGrid);

        Set<EfSegment> list = new HashSet<EfSegment>();
        
        list.add(new EfSegment(nbPts - 2, (nbPts * 2) - 1));
        list.add(new EfSegment((nbLine - 2) * nbPts, ((nbLine - 1) * nbPts) + 1));
        
        EfOperationSwapSelectedEdge operation = new EfOperationSwapSelectedEdge();
        
        operation.setInitGridData(defDataGrid);
        operation.setSelectedEdge(list);
        
        EfOperationResult result = operation.process(null);
        
        EfGridInterface resGrid      = result.getGridData().getGrid();
		EfNeighborMesh  neighborMesh = EfNeighborMesh.compute(resGrid, null);

		TIntHashSet newElementsIdx = new TIntHashSet(neighborMesh.getAdjacentMeshes(nbPts - 1, (nbPts * 2) -2));
		
		// Test qu'il y a bien 2 �l�ments.
		assertEquals(2, newElementsIdx.size());
		// Test que les �l�ments sont bien ceux en bas � gauche.
		assertTrue(newElementsIdx.contains(((nbPts - 1) * 2) - 2));
		assertTrue(newElementsIdx.contains(((nbPts - 1) * 2) - 1));

		newElementsIdx = new TIntHashSet(neighborMesh.getAdjacentMeshes(((nbLine - 2) * nbPts) + 1, (nbLine - 1) * nbPts));
		
		// Test qu'il y a bien 2 �l�ments.
		assertEquals(2, newElementsIdx.size());
		// Test que les �l�ments sont bien ceux en haut � droite.
		assertTrue(newElementsIdx.contains(((nbPts - 1) * (nbLine - 2)) * 2));
		assertTrue(newElementsIdx.contains((((nbPts - 1) * (nbLine - 2)) * 2) + 1));
	}
}
