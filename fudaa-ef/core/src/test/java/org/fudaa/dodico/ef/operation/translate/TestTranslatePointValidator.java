package org.fudaa.dodico.ef.operation.translate;

import junit.framework.TestCase;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;

public class TestTranslatePointValidator extends TestCase {
  private TranslatePointValidator createValidator(int idx, double newX, double newY) {
    return new TranslatePointValidator(getGrid(), new int[]{idx}, new double[]{newX}, new double[]{newY});
  }

  public void testTranslatePointChecker() {

    // Point sur fronti�re interne dans �l�ment voisin.
    assertTrue(createValidator(2, 5.0, 13.0).canTranslate(null).isEmpty());
    assertTrue(createValidator(2, 10.0, 15.0).canTranslate(null).isEmpty());
    assertFalse(createValidator(2, 10.0, 19.0).canTranslate(null).isEmpty());
    assertFalse(createValidator(2, 1.0, 17.0).canTranslate(null).isEmpty());
    // Point sur fronti�re externe dans �l�ment voisin.
    assertFalse(createValidator(2, 16.0, 9.0).canTranslate(null).isEmpty());
    // Point sur fronti�re externe dans fronti�re externe.
    assertTrue(createValidator(6, 20.0, 5.0).canTranslate(null).isEmpty());
    // Point sur fronti�re externe dans �l�ment non-voisin.
    assertFalse(createValidator(6, 10.0, 7.0).canTranslate(null).isEmpty());
    // Point sur fronti�re externe dans fronti�re interne.
    assertFalse(createValidator(6, 10.0, 12.0).canTranslate(null).isEmpty());
    // El�ment devient plat.
    assertFalse(createValidator(6, 4.5, 14.0).canTranslate(null).isEmpty());
  }

  private static EfGridInterface getGrid() {
    EfNode[] nodes = new EfNode[]{new EfNode(2.0, 13.0, 0.0),
        new EfNode(5.0, 19.0, 0.0),
        new EfNode(7.0, 15.0, 0.0),
        new EfNode(10.0, 18.0, 0.0),
        new EfNode(15.0, 21.0, 0.0),
        new EfNode(15.0, 14.0, 0.0),
        new EfNode(18.0, 8.0, 0.0),
        new EfNode(13.0, 8.0, 0.0),
        new EfNode(10.0, 4.0, 0.0),
        new EfNode(7.0, 10.0, 0.0)};
    EfElement[] elements = new EfElement[]{new EfElement(new int[]{0, 1, 2}),
        new EfElement(new int[]{1, 3, 2}),
        new EfElement(new int[]{1, 4, 3}),
        new EfElement(new int[]{4, 5, 3}),
        new EfElement(new int[]{4, 6, 5}),
        new EfElement(new int[]{6, 7, 5}),
        new EfElement(new int[]{6, 8, 7}),
        new EfElement(new int[]{8, 9, 7}),
        new EfElement(new int[]{8, 0, 9}),
        new EfElement(new int[]{9, 0, 2})};
    return new EfGrid(nodes, elements);
  }
}
