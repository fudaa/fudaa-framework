package org.fudaa.dodico.ef.operation;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class EfEdgeMapperTest {
  @Test
  public void getObject() {
    Assert.assertEquals(1, 1);
    EfEdgeMapper<Integer> mapper = new EfEdgeMapper<>(createGrid());
    mapper.addEdge(0, 1, Integer.valueOf(1));
    mapper.addEdge(1, 2, Integer.valueOf(2));
    mapper.addEdge(2, 0, Integer.valueOf(3));
    //middle point
    Assert.assertEquals(1, mapper.getObject(5, 0).intValue());
    Assert.assertEquals(2, mapper.getObject(5, 5).intValue());
    Assert.assertEquals(3, mapper.getObject(0, 5).intValue());

    //sur segment
    Assert.assertEquals(1, mapper.getObject(5, 0.0001).intValue());
    Assert.assertEquals(2, mapper.getObject(5.0001, 5).intValue());
    Assert.assertEquals(2, mapper.getObject(8, 2).intValue());
    Assert.assertEquals(3, mapper.getObject(-0.0001, 6).intValue());

    Assert.assertNull(mapper.getObject(10, 10));
  }

  private EfGrid createGrid() {
    List<EfNode> nodes = new ArrayList<>();
    nodes.add(new EfNode(0, 0, 0));
    nodes.add(new EfNode(10, 0, 0));
    nodes.add(new EfNode(0, 10, 0));
    return new EfGrid(nodes.toArray(new EfNode[nodes.size()]), new EfElement[]{new EfElement(new int[]{0, 1, 2})});
  }
}
