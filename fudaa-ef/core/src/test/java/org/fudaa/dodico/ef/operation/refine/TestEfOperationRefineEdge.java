package org.fudaa.dodico.ef.operation.refine;

import org.locationtech.jts.geom.Coordinate;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;

public class TestEfOperationRefineEdge extends TestCase {
    
    private EfGridData createDataGrid()
    {
	EfNode[]    nodes    = new EfNode[14];
	EfElement[] elements = new EfElement[5];
	
	nodes[ 0] = new EfNode(new Coordinate( 20,   0));
	nodes[ 1] = new EfNode(new Coordinate( 60,   0));
	nodes[ 2] = new EfNode(new Coordinate( 80,  20));
	nodes[ 3] = new EfNode(new Coordinate( 80,  60));
	nodes[ 4] = new EfNode(new Coordinate( 60,  80));
	nodes[ 5] = new EfNode(new Coordinate( 20,  80));
	nodes[ 6] = new EfNode(new Coordinate(  0,  60));
	nodes[ 7] = new EfNode(new Coordinate(  0,  20));
	nodes[ 8] = new EfNode(new Coordinate( 20, 120));
	nodes[ 9] = new EfNode(new Coordinate( 60, 120));
	nodes[10] = new EfNode(new Coordinate( 40, 140));
	nodes[11] = new EfNode(new Coordinate(  0, 160));
	nodes[12] = new EfNode(new Coordinate(100, 120));
	nodes[13] = new EfNode(new Coordinate(100,  80));
	
	elements[0] = new EfElement(new int[]{0, 1, 2, 3, 4, 5, 6, 7});
	elements[1] = new EfElement(new int[]{4, 5, 8, 9});
	elements[2] = new EfElement(new int[]{8, 10, 9});
	elements[3] = new EfElement(new int[]{8, 11, 10});
	elements[4] = new EfElement(new int[]{4, 9, 12, 13});

	return new EfGridDataDefault(new EfGridArray(nodes, elements));
    }
    
    public void testRefineAll()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();
	
	/*   14 points d'origines
	 * +  9 pour E0
	 * +  4 (5 mais 1 commun avec E0) pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  2 (3 mais 1 commun avec E2) pour E3
	 * +  4 (5 mais 1 commun avec E1) pour E4
	 * = 35 points.
	 */
	assertEquals(35, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  3 pour E1
	 * +  3 pour E2
	 * +  3 pour E3
	 * +  3 pour E4
	 * = 32 �l�ments.
	 */
	assertEquals(32, resGrid.getEltNb());
    }
    
    public void testRefineElement0()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{0}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  0 (1 mais 1 commun avec E0) pour E1
	 * +  0 pour E2
	 * +  0 pour E3
	 * +  0 pour E4
	 * = 23 points.
	 */
	assertEquals(23, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  2 pour E1
	 * +  0 pour E2
	 * +  0 pour E3
	 * +  0 pour E4
	 * = 22 �l�ments.
	 */
	assertEquals(22, resGrid.getEltNb());
    }

    public void testRefineElement1()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{1}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  4 (5 mais 1 commun avec E0) pour E1
	 * +  0 (1 mais 1 commun avec E1) pour E2
	 * +  0 pour E3
	 * +  0 (1 mais 1 commun avec E1) pour E4
	 * = 27 points.
	 */
	assertEquals(27, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  3 pour E1
	 * +  1 pour E2
	 * +  0 pour E3
	 * +  2 pour E4
	 * = 26 �l�ments.
	 */
	assertEquals(26, resGrid.getEltNb());
    }

    public void testRefineElement2()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{2}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  0 pour E0
	 * +  1 pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  0 (1 mais 1 commun avec E2) pour E3
	 * +  0 pour E4
	 * = 17 points.
	 */
	assertEquals(17, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * +  0 pour E0
	 * +  2 pour E1
	 * +  3 pour E2
	 * +  1 pour E3
	 * +  0 pour E4
	 * = 11 �l�ments.
	 */
	assertEquals(11, resGrid.getEltNb());
    }

    public void testRefineElement3()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{3}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  0 pour E0
	 * +  0 pour E1
	 * +  1 pour E2
	 * +  2 (3 mais 1 commun avec E2) pour E3
	 * +  0 pour E4
	 * = 17 points.
	 */
	assertEquals(17, resGrid.getPtsNb());
	/*   5 �l�ments d'origines
	 * + 0 pour E0
	 * + 0 pour E1
	 * + 1 pour E2
	 * + 3 pour E3
	 * + 0 pour E4
	 * = 9 �l�ments.
	 */
	assertEquals(9, resGrid.getEltNb());
    }

    public void testRefineElement4()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{4}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  0 pour E0
	 * +  1 pour E1
	 * +  0 pour E2
	 * +  0 pour E3
	 * +  4 (5 mais 1 commun avec E1) pour E4
	 * = 19 points.
	 */
	assertEquals(19, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * +  0 pour E0
	 * +  2 pour E1
	 * +  0 pour E2
	 * +  0 pour E3
	 * +  3 pour E4
	 * = 10 �l�ments.
	 */
	assertEquals(10, resGrid.getEltNb());
    }

    public void testRefineElements0_2()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{0, 2}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  1 (2 mais 1 commun avec E0) pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  0 (1 mais 1 commun avec E2) pour E3
	 * +  0 pour E4
	 * = 26 points.
	 */
	assertEquals(26, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  1 pour E1
	 * +  3 pour E2
	 * +  1 pour E3
	 * +  0 pour E4
	 * = 25 �l�ments.
	 */
	assertEquals(25, resGrid.getEltNb());
    }

    public void testRefineElements1_3()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{1, 3}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  4 (5 mais 1 commun avec E0) pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  2 (3 mais 1 commun avec E2) pour E3
	 * +  0 (1 mais 1 commun avec E1) pour E4
	 * = 31 points.
	 */
	assertEquals(31, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  3 pour E1
	 * +  3 pour E2
	 * +  3 pour E3
	 * +  2 pour E4
	 * = 31 �l�ments.
	 */
	assertEquals(31, resGrid.getEltNb());
    }

    public void testRefineElements2_4()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{2, 4}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  4 (5 mais 1 commun avec E0) pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  0 (1 mais 1 commun avec E2) pour E3
	 * +  4 (5 mais 1 commun avec E1) pour E4
	 * = 33 points.
	 */
	assertEquals(33, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  3 pour E1
	 * +  3 pour E2
	 * +  1 pour E3
	 * +  3 pour E4
	 * = 30 �l�ments.
	 */
	assertEquals(30, resGrid.getEltNb());
    }

    public void testRefineElements0_2_4()
    {
	EfGridData defGridData = this.createDataGrid();
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{0, 2, 4}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  4 (5 mais 1 commun avec E0) pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  0 (1 mais 1 commun avec E2) pour E3
	 * +  4 (5 mais 1 commun avec E1) pour E4
	 * = 33 points.
	 */
	assertEquals(33, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  3 pour E1
	 * +  3 pour E2
	 * +  1 pour E3
	 * +  3 pour E4
	 * = 30 �l�ments.
	 */
	assertEquals(30, resGrid.getEltNb());
    }

    public void testRefineElements0_2WithE1Inversed()
    {
	EfNode[]    nodes    = new EfNode[14];
	EfElement[] elements = new EfElement[5];
	
	nodes[ 0] = new EfNode(new Coordinate( 20,   0));
	nodes[ 1] = new EfNode(new Coordinate( 60,   0));
	nodes[ 2] = new EfNode(new Coordinate( 80,  20));
	nodes[ 3] = new EfNode(new Coordinate( 80,  60));
	nodes[ 4] = new EfNode(new Coordinate( 60,  80));
	nodes[ 5] = new EfNode(new Coordinate( 20,  80));
	nodes[ 6] = new EfNode(new Coordinate(  0,  60));
	nodes[ 7] = new EfNode(new Coordinate(  0,  20));
	nodes[ 8] = new EfNode(new Coordinate( 20, 120));
	nodes[ 9] = new EfNode(new Coordinate( 60, 120));
	nodes[10] = new EfNode(new Coordinate( 40, 140));
	nodes[11] = new EfNode(new Coordinate(  0, 160));
	nodes[12] = new EfNode(new Coordinate(100, 120));
	nodes[13] = new EfNode(new Coordinate(100,  80));
	
	elements[0] = new EfElement(new int[]{0, 1, 2, 3, 4, 5, 6, 7});
	elements[1] = new EfElement(new int[]{4, 9, 8, 5});
	elements[2] = new EfElement(new int[]{8, 10, 9});
	elements[3] = new EfElement(new int[]{8, 11, 10});
	elements[4] = new EfElement(new int[]{4, 9, 12, 13});

	EfGridData defGridData = new EfGridDataDefault(new EfGridArray(nodes, elements));
	
	EfOperationRefineEdge refine = new EfOperationRefineEdge();
	
	refine.setInitGridData(defGridData);
	refine.setSelectedElt(new CtuluListSelection(new int[]{0, 2}));
	
	EfGridData      resGridData = refine.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();

	/*   14 points d'origines
	 * +  9 pour E0
	 * +  1 (2 mais 1 commun avec E0) pour E1
	 * +  2 (3 mais 1 commun avec E1) pour E2
	 * +  0 (1 mais 1 commun avec E2) pour E3
	 * +  0 pour E4
	 * = 26 points.
	 */
	assertEquals(26, resGrid.getPtsNb());
	/*    5 �l�ments d'origines
	 * + 15 pour E0
	 * +  1 pour E1
	 * +  3 pour E2
	 * +  1 pour E3
	 * +  0 pour E4
	 * = 25 �l�ments.
	 */
	assertEquals(25, resGrid.getEltNb());
    }
}
