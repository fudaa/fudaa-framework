package org.fudaa.dodico.ef;

import org.locationtech.jts.geom.Coordinate;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.dodico.ef.impl.EfGridArray;

public class TestEfRemoveFilter extends TestCase
{
  public void testEfRemoveFilter()
  {
    EfRemoveFilter  removeFilter = new EfRemoveFilter();
    EfGridInterface grid         = this.createGridA();
    
    removeFilter.setGrid(grid);
    removeFilter.setFilter(this.createFilter1(grid));
    removeFilter.setSelectFilter(false);
    
    this.testResult(removeFilter.process(), this.createList1(), grid.getEltNb());
    assertFalse("Le filtre ne doit pas �tre modifi�", removeFilter.getFilterModified());

    removeFilter.setFilter(this.createFilter2(grid));
    
    this.testResult(removeFilter.process(), this.createList2(), grid.getEltNb());
    assertTrue("Le filtre doit �tre modifi�", removeFilter.getFilterModified());

    removeFilter.setFilter(this.createFilter3(grid));
    
    this.testResult(removeFilter.process(), this.createList3(), grid.getEltNb());
    assertFalse("Le filtre ne doit pas �tre modifi�", removeFilter.getFilterModified());

    removeFilter.setFilter(this.createFilter4(grid));
    
    this.testResult(removeFilter.process(), this.createList4(), grid.getEltNb());
    assertTrue("Le filtre doit �tre modifi�", removeFilter.getFilterModified());

    removeFilter.setFilter(this.createFilter5(grid));
    
    this.testResult(removeFilter.process(), this.createList5(), grid.getEltNb());
    assertFalse("Le filtre ne doit pas �tre modifi�", removeFilter.getFilterModified());


    removeFilter.setFilter(this.createFilter7(grid));
    
    this.testResult(removeFilter.process(), this.createList7(), grid.getEltNb());
    assertFalse("Le filtre ne doit pas �tre modifi�", removeFilter.getFilterModified());


  }
  
  private EfFilter createFilter1(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{24, 25, 26, 31, 32}), grid);
  }
  
  private CtuluListSelectionInterface createList1()
  {
    return new CtuluListSelection(new int[]{24, 25, 26, 31, 32});
  }
  
  private EfFilter createFilter2(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{18, 26, 32}), grid);
  }
  
  private CtuluListSelectionInterface createList2()
  {
    return new CtuluListSelection(new int[]{26, 32});
  }
  
  private EfFilter createFilter3(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{17, 18, 19}), grid);
  }
  
  private CtuluListSelectionInterface createList3()
  {
    return new CtuluListSelection(new int[]{17, 18, 19});
  }
  
  private EfFilter createFilter4(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{17, 18}), grid);
  }
  
  private CtuluListSelectionInterface createList4()
  {
    return new CtuluListSelection(new int[]{17});
  }
  
  private EfFilter createFilter5(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{17, 18, 24, 25, 26}), grid);
  }
  
  private CtuluListSelectionInterface createList5()
  {
    return new CtuluListSelection(new int[]{17, 18, 24, 25, 26});
  }
  
  private EfFilter createFilter6(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{23, 29}), grid);
  }
  
  private CtuluListSelectionInterface createList6()
  {
    return new CtuluListSelection(new int[]{23});
  }
  
  private EfFilter createFilter7(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{23, 29, 30}), grid);
  }
  
  private CtuluListSelectionInterface createList7()
  {
    return new CtuluListSelection(new int[]{23, 29, 30});
  }
  
  private EfFilter createFilter8(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{23, 27, 28, 29, 33}), grid);
  }
  
  private CtuluListSelectionInterface createList8()
  {
    return new CtuluListSelection(new int[]{23, 27, 28, 33});
  }
  
  private EfFilter createFilter9(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{2, 3, 12}), grid);
  }
  
  private CtuluListSelectionInterface createList9()
  {
    return new CtuluListSelection(new int[]{2, 3});
  }
  
  private EfFilter createFilter10(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{2, 3, 6, 12}), grid);
  }
  
  private CtuluListSelectionInterface createList10()
  {
    return new CtuluListSelection(new int[]{2, 3, 6, 12});
  }

  private EfFilter createFilter11(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{22, 27}), grid);
  }
  
  private CtuluListSelectionInterface createList11()
  {
    return new CtuluListSelection(new int[]{22});
  }

  private EfFilter createFilter12(EfGridInterface grid)
  {
    return new EfFilterSelectedElement(new CtuluListSelection(new int[]{2, 3, 12}), grid);
  }
  
  private CtuluListSelectionInterface createList12()
  {
    return new CtuluListSelection(new int[]{3, 12});
  }
  
  private EfGridInterface createGridA()
  {
    return new EfGridArray(this.createNodes(), this.createElements());
  }
  
  private EfGridInterface createGridB()
  {
    EfElement[] elements = this.createElements();
    EfElement   tmp;
    
    tmp          = elements[2];
    elements[2]  = elements[12];
    elements[12] = tmp;
    
    tmp          = elements[23];
    elements[23]  = elements[29];
    elements[29] = tmp;
    
    return new EfGridArray(this.createNodes(), elements);
  }
  
  private void testResult(EfFilter filter, CtuluListSelectionInterface list, int nbElts)
  {
    for (int i = 0; i < nbElts; i++)
    {
      assertEquals("L'�l�ment : " + i + " n'est pas contenu dans les 2 listes", list.isSelected(i), filter.isActivatedElt(i));
    }
  }
  
  private EfNode[] createNodes()
  {
    return new EfNode[]{new EfNode(new Coordinate( 10,  60)),
                        new EfNode(new Coordinate( 10,  80)),
                        new EfNode(new Coordinate( 20,  70)),
                        new EfNode(new Coordinate( 35,  60)),
                        new EfNode(new Coordinate( 35,  80)),
                        new EfNode(new Coordinate( 50,  95)),
                        new EfNode(new Coordinate( 35, 110)),
                        new EfNode(new Coordinate( 60,  70)),
                        new EfNode(new Coordinate( 40,  50)),
                        new EfNode(new Coordinate( 35,  30)),
                        new EfNode(new Coordinate( 70,  20)),
                        new EfNode(new Coordinate( 80,  50)),
                        new EfNode(new Coordinate( 80,  20)),
                        new EfNode(new Coordinate( 80,  80)),
                        new EfNode(new Coordinate( 80, 115)),
                        new EfNode(new Coordinate( 90, 100)),
                        new EfNode(new Coordinate(110,  70)),
                        new EfNode(new Coordinate(110,  30)),
                        new EfNode(new Coordinate(115, 100)),
                        new EfNode(new Coordinate(120,  80)),
                        new EfNode(new Coordinate(130,  60)),
                        new EfNode(new Coordinate(130,  40)),
                        new EfNode(new Coordinate(130,  10)),
                        new EfNode(new Coordinate(120, 120)),
                        new EfNode(new Coordinate(140,  90)),
                        new EfNode(new Coordinate(150, 120)),
                        new EfNode(new Coordinate(150,  40))};
  }
  private EfElement[] createElements()
  {
    return new EfElement[]{new EfElement(new int[]{ 0,  1,  2}),
                           new EfElement(new int[]{ 2,  1,  4}),
                           new EfElement(new int[]{ 2,  4,  3}),
                           new EfElement(new int[]{ 3,  0,  2}),
                           new EfElement(new int[]{ 4,  1,  5}),
                           new EfElement(new int[]{ 1,  6,  5}),
                           new EfElement(new int[]{ 3,  4,  7}),
                           new EfElement(new int[]{ 8,  3,  7}),
                           new EfElement(new int[]{ 9,  3,  8}),
                           new EfElement(new int[]{10,  9,  8}),
                           new EfElement(new int[]{10,  8, 11}),
                           new EfElement(new int[]{10, 11, 12}),
                           new EfElement(new int[]{ 7,  4, 13}),
                           new EfElement(new int[]{ 4,  5, 13}),
                           new EfElement(new int[]{ 5, 14, 15}),
                           new EfElement(new int[]{13,  5, 15}),
                           new EfElement(new int[]{16, 13, 15}),
                           new EfElement(new int[]{11, 13, 16}),
                           new EfElement(new int[]{17, 11, 16}),
                           new EfElement(new int[]{12, 11, 17}),
                           new EfElement(new int[]{15, 14, 18}),
                           new EfElement(new int[]{16, 15, 18}),
                           new EfElement(new int[]{16, 18, 19}),
                           new EfElement(new int[]{20, 16, 19}),
                           new EfElement(new int[]{17, 16, 20}),
                           new EfElement(new int[]{17, 20, 21}),
                           new EfElement(new int[]{22, 17, 21}),
                           new EfElement(new int[]{18, 14, 23}),
                           new EfElement(new int[]{24, 18, 23}),
                           new EfElement(new int[]{19, 18, 24}),
                           new EfElement(new int[]{20, 19, 24}),
                           new EfElement(new int[]{21, 20, 26}),
                           new EfElement(new int[]{22, 21, 26}),
                           new EfElement(new int[]{24, 23, 25}),
                           new EfElement(new int[]{20, 24, 26}),
                           new EfElement(new int[]{24, 25, 26})};
  }
}
