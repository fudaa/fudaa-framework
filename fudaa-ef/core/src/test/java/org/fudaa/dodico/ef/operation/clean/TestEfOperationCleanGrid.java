package org.fudaa.dodico.ef.operation.clean;

import org.locationtech.jts.geom.Coordinate;
import junit.framework.TestCase;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;

public class TestEfOperationCleanGrid extends TestCase
{
    public void testCleanGrid()
    {
	EfNode[]    nodes    = new EfNode[17];
	EfElement[] elements = new EfElement[13];
	
	nodes[ 0] = new EfNode(new Coordinate( 40,   0));
	nodes[ 1] = new EfNode(new Coordinate(160,   0));
	nodes[ 2] = new EfNode(new Coordinate(100,  20));
	nodes[ 3] = new EfNode(new Coordinate( 80,  40));
	nodes[ 4] = new EfNode(new Coordinate(120,  40));
	nodes[ 5] = new EfNode(new Coordinate(180,  40));
	nodes[ 6] = new EfNode(new Coordinate(  0,  60));
	nodes[ 7] = new EfNode(new Coordinate(  0, 120));
	nodes[ 8] = new EfNode(new Coordinate( 60, 120));
	nodes[ 9] = new EfNode(new Coordinate( 40, 130));
	nodes[10] = new EfNode(new Coordinate(140, 120));
	nodes[11] = new EfNode(new Coordinate(200,  60));
	nodes[12] = new EfNode(new Coordinate(220,   0));
	nodes[13] = new EfNode(new Coordinate(220, 120));
	nodes[14] = new EfNode(new Coordinate(240,  60));
	nodes[15] = new EfNode(new Coordinate( 60,  80));
	nodes[16] = new EfNode(new Coordinate(100,  80));

	elements[ 0] = new EfElement(new int[]{0, 6, 3});
	elements[ 1] = new EfElement(new int[]{0, 3, 4, 1});
	elements[ 2] = new EfElement(new int[]{1, 4, 10});
	elements[ 3] = new EfElement(new int[]{3, 6, 8, 16});
	elements[ 4] = new EfElement(new int[]{3, 16, 4});
	elements[ 5] = new EfElement(new int[]{4, 16, 10});
	elements[ 6] = new EfElement(new int[]{16, 8, 10});
	elements[ 7] = new EfElement(new int[]{ 6, 7, 9, 8});
	elements[ 8] = new EfElement(new int[]{11, 13, 12});
	elements[ 9] = new EfElement(new int[]{12, 13, 14});
	elements[10] = new EfElement(new int[]{1, 11, 12});
	elements[11] = new EfElement(new int[]{11, 10, 13});
	elements[12] = new EfElement(new int[]{1, 10, 11});
	
	EfGridData defGridData = new EfGridDataDefault(new EfGridArray(nodes, elements));

	EfOperationCleanGrid cleanGrid = new EfOperationCleanGrid();
	
	cleanGrid.setInitGridData(defGridData);
	// 42 est la distance qui merge les points 3 et 4 et rien d'autre.
	cleanGrid.setMinDistance(42.5d);
	EfGridData      resGridData = cleanGrid.process(null, null);
	EfGridInterface resGrid     = resGridData.getGrid();
	
	/*   17 points d'origines
	 * -  3 non utilis�s
	 * -  3 ne respectant pas la distance minimale
	 * -  1 non utilis� apr�s �l�ment d�g�n�r� supprim�.
	 * = 10 points.
	 */
	assertEquals(11, resGrid.getPtsNb());

	/*   13 �l�ments d'origines
	 * -  2 d�g�n�r�s
	 * -  1 superpos�
	 * = 10 �l�ments.
	 */
	assertEquals(12, resGrid.getEltNb());
    }
}
