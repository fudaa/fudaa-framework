package org.fudaa.dodico.ef.operation.renum;

import gnu.trove.TIntHashSet;
import gnu.trove.TIntIntHashMap;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfTestFactory;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;

public class TestEfOperationShufle extends TestCase {
  /*
   * private static class CircularBuffer<T> { private class Element<T> { private T value; private Element<T> next;
   * 
   * public Element(T value) { this.value = value; }
   * 
   * public T getValue() { return this.value; }
   * 
   * public Element<T> getNext() { return next; }
   * 
   * public void setNext(Element<T> next) { this.next = next; } }
   * 
   * private Element<T> first; private Element<T> current; private int nbTurn;
   * 
   * public CircularBuffer(T[] values) { this.first = new Element<T>(values[0]); this.current = this.first;
   * 
   * for (int i = 1; i < values.length; i++) { Element<T> newElement = new Element<T>(values[i]);
   * 
   * this.current.setNext(newElement); this.current = newElement; }
   * 
   * this.first.setNext(this.current); this.Reset(); }
   * 
   * public void Reset() { this.current = this.first; this.nbTurn = 0; }
   * 
   * public T getValue() { return this.current.getValue(); }
   * 
   * public boolean isLast() { return (this.current.getNext() == this.first); }
   * 
   * public void Move() { this.current = this.current.getNext();
   * 
   * if (this.current == this.first) { this.nbTurn++; } }
   * 
   * public int getNbTurn() { return this.nbTurn; } }
   */

  private static class CircularIntBuffer {
    private class Element {
      private int value;
      private Element next;

      public Element(int value) {
        this.value = value;
      }

      public int getValue() {
        return this.value;
      }

      public Element getNext() {
        return next;
      }

      public void setNext(Element next) {
        this.next = next;
      }
    }

    private Element first;
    private Element current;
    private int nbTurn;

    public CircularIntBuffer(int[] values) {
      this.first = new Element(values[0]);
      this.current = this.first;

      for (int i = 1; i < values.length; i++) {
        Element newElement = new Element(values[i]);

        this.current.setNext(newElement);
        this.current = newElement;
      }

      this.current.setNext(this.first);
      this.Reset();
    }

    public void Reset() {
      this.current = this.first;
      this.nbTurn = 0;
    }

    public int getValue() {
      return this.current.getValue();
    }

    public boolean isLast() {
      return (this.current.getNext() == this.first);
    }

    public void Move() {
      this.current = this.current.getNext();

      if (this.current == this.first) {
        this.nbTurn++;
      }
    }

    public int getNbTurn() {
      return this.nbTurn;
    }
  }

  /**
   * L'eps utilise pour tester l'�galite entre les double
   */
  private double eps = 1E-10;

  public void testShuffle() {
    final int nbLine = 5;
    final int nbPts = 10;

    // on cr�� un maillage simple de 50 point: 5 lignes contenant 10 point
    EfGridInterface defGrid = EfTestFactory.buildGrid(nbLine, nbPts);
    EfGridDataDefault defGridData = new EfGridDataDefault(defGrid);
    EfElement[] elements = defGrid.getElts();

    EfOperationShufle operation = new EfOperationShufle();

    operation.setInitGridData(defGridData);
    EfGridData resGridData = operation.process(null, new CtuluAnalyze());
    EfGridInterface resGrid = resGridData.getGrid();
    EfElement[] newElements = resGrid.getElts();

    // Test qu'il y a tjs le meme nombre d'�l�ment.
    assertEquals(elements.length, newElements.length);

    TIntIntHashMap newIdxOldIdx = new TIntIntHashMap();

    for (int i = 0; i < elements.length; i++) {
      int newIdx = i;

      if (((i % 2) == 0) && (i < ((elements.length - 4) / 2))) {
        newIdx = elements.length - (i + 1);

        // M�morise le nouvel indice et l'ancien indice.
        newIdxOldIdx.put(newIdx, i);
      }
      // Si l'indice a �t� m�moris�, on r�cup�re le nouvel indice.
      else if (newIdxOldIdx.containsKey(i)) {
        newIdx = newIdxOldIdx.get(i);
      }

      int[] pts = elements[i].getIndices();
      int[] newPts = newElements[newIdx].getIndices();
      TIntHashSet newPtsSet = new TIntHashSet(newPts);

      // Test que les 2 �l�ments qui doivent �tre identiques ont le m�me nombre de point.
      assertEquals(pts.length, newPtsSet.size());

      for (int j = 0; j < pts.length; j++) {
        // Test que les 2 �l�ments qui doivent �tre identiques ont les m�me points.
        assertTrue(newPtsSet.contains(pts[j]));
      }

      CircularIntBuffer ptsBuffer = new CircularIntBuffer(pts);

      while (ptsBuffer.getNbTurn() == 0) {
        if (ptsBuffer.getValue() == newPts[0]) {
          break;
        }

        ptsBuffer.Move();
      }

      // Test qu'on a pas fais une tour complet du buffer.
      assertTrue(ptsBuffer.getNbTurn() == 0);

      double maxX = resGridData.getGrid().getPtX(newPts[0]);

      // Test la renumerotation des points.
      for (int j = 0; j < newPts.length; j++) {
        if (resGridData.getGrid().getPtX(newPts[j]) > maxX) {
          maxX = resGridData.getGrid().getPtX(newPts[j]);
        }

        assertEquals(ptsBuffer.getValue(), newPts[j]);

        ptsBuffer.Move();
      }

      // Test que le premier points est le max.
      assertEquals(maxX, resGridData.getGrid().getPtX(newPts[0]), this.eps);
    }
  }
}
