package org.fudaa.dodico.ef.operation.overstressed;

import java.util.Set;
import junit.framework.TestCase;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.ef.EfTestFactory;

public class TestEfFindEdgeToSwap extends TestCase
{
	public void testFindEdgeToSwap()
	{
		final int nbLine =  5;
		final int nbPts  = 10;
		
        //on cr�� un maillage simple de 50 point: 5 lignes contenant 10 point
        EfGridInterface defGrid = EfTestFactory.buildGrid(nbLine, nbPts);
        
        EfFindEdgeToSwap findEdgeToSwap = new EfFindEdgeToSwap();
        
        Set<EfSegment> list = findEdgeToSwap.process(defGrid, null);
        
        // Test qu'il y a bien 2 couples.
        assertEquals(2, list.size());

        EfSegment segment1 = new EfSegment(nbPts - 2, (nbPts * 2) - 1);
        EfSegment segment2 = new EfSegment((nbLine - 2) * nbPts, ((nbLine - 1) * nbPts) + 1);
        
        // Test que le segment reliant les 2 elements du coin en bas a droite sont contenu.
        assertTrue(list.contains(segment1));
        // Test que le segment reliant les 2 elements du coin en haut a gauche sont contenu.
        assertTrue(list.contains(segment2));
	}

}
