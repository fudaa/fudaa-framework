package org.fudaa.dodico.ef.operation.overstressed;

import java.util.Arrays;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfTestFactory;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.operation.EfOperationResult;

/**
 * @author Christophe CANEL (Genesis)
 *
 */
public class TestEfOperationOverstressed extends TestCase
{
	/**
	 * Test la classe EfOperationOverstressed
	 */
	public void testOverstressed()
	{
		final int nbLine =  5;
		final int nbPts  = 10;
		
        //on cr�� un maillage simple de 50 point: 5 lignes contenant 10 point
        EfGridInterface defGrid = EfTestFactory.buildGrid(nbLine, nbPts);
        EfGridDataDefault initGridData = new EfGridDataDefault(defGrid);
        
        // R�cup�ration des index des �l�ments fronti�re.
    	int[] overstressedIdx = defGrid.getOverstressedElement(null);

    	Arrays.sort(overstressedIdx);
    	
    	// Test que l'�l�ment en bas a droite est selectionn�.
    	assertEquals(((nbPts - 1) * 2) - 2, overstressedIdx[0]);
    	// Test que l'�l�ment en haut a gauche est selectionn�.
    	assertEquals((((nbPts - 1) * (nbLine - 2)) * 2) + 1, overstressedIdx[1]);
    	
        EfOperationOverstressed operationOverstressed = new EfOperationOverstressed();
        
        operationOverstressed.setInitGridData(initGridData);
        
        //on met null car on ne veut pas r�cup�rer l'avancement du process
        EfOperationResult resOperation = operationOverstressed.process(null);
        assertFalse(resOperation.isFrontierChanged());
        EfGridData resGrid = resOperation.getGridData();
        
        int defNbElements = defGrid.getEltNb();
        
        // Un element surcontraint est remplac� par 3 �lements : 2 surcontraint -> 4 �l�ments ajout�s.
        assertEquals(defNbElements + 4, resGrid.getGrid().getEltNb());

        // Test qu'il n'y plus d'�l�ment surcontraint.
        assertTrue(CtuluLibArray.isEmpty(resGrid.getGrid().getOverstressedElement(null)));
	}
}
