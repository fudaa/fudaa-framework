/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef;

import org.fudaa.dodico.ef.impl.EfGridArray;

/**
 * @author deniger
 *
 */
public class EfTestFactory {

  public static EfGridInterface buildGrid(final int _nbLine, final int _nbPtPerLine) {
    final double dx = 10;
    final double xInit = 0;
    final double dy = 5;
    final double yInit = 0;
  
    EfNode[] nds = new EfNode[_nbLine * _nbPtPerLine];
    int idx = 0;
    for (int i = 0; i < _nbLine; i++) {
      for (int j = 0; j < _nbPtPerLine; j++) {
        nds[idx++] = new EfNode(xInit + dx * j, yInit + i * dy, 0);
      }
    }
    EfElement[] elt = new EfElement[(_nbLine - 1) * (_nbPtPerLine - 1) * 2];
    idx = 0;
    for (int i = 0; i < _nbLine - 1; i++) {
      for (int j = 0; j < _nbPtPerLine - 1; j++) {
        int idxBase = (i * _nbPtPerLine) + j;
        elt[idx++] = new EfElement(new int[] { idxBase, idxBase + 1, idxBase + 1 + _nbPtPerLine });
        elt[idx++] = new EfElement(new int[] { idxBase, idxBase + 1 + _nbPtPerLine, idxBase + _nbPtPerLine });
  
      }
    }
  
    return new EfGridArray(nds, elt);
  }

}
