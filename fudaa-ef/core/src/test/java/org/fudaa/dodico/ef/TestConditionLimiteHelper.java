package org.fudaa.dodico.ef;

import junit.framework.TestCase;

public class TestConditionLimiteHelper extends TestCase {

    public void test() {
	assertEquals(ConditionLimiteEnum.SOLID, ConditionLimiteHelper
		.getConditionsLimite(0));
	assertEquals(ConditionLimiteEnum.SOLID, ConditionLimiteHelper
		.getConditionsLimite(-1));
	assertEquals(ConditionLimiteEnum.SOLID, ConditionLimiteHelper
		.getConditionsLimite(16));
	assertEquals(ConditionLimiteEnum.NONE_10, ConditionLimiteHelper
		.getConditionsLimite(10));
    }

}
