package org.fudaa.dodico.ef.operation.refine;

import org.locationtech.jts.geom.*;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluVariableDefault;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfTestFactory;
import org.fudaa.dodico.ef.impl.EfGridDataDefault;
import org.fudaa.dodico.ef.interpolation.EfInterpolator;
import org.fudaa.dodico.ef.operation.EfOperationResult;

/**
 *Test unitaire pour EfOperationRefline
 * 
 * @creation 2 oct. 2009
 * @version
 */
public class TestEfOperationRefine extends TestCase {

  /**
   * On teste que le maillage cr�� est bien correct
   */
  public void testRefineMeshOnly() {
    // on cr�� un maillage simple de 50 point: 5 lignes contenant 10 point
    EfGridInterface defGrid = EfTestFactory.buildGrid(5, 10);
    int nbPtsInitGrid = defGrid.getPtsNb();
    EfGridDataDefault initGridData = new EfGridDataDefault(defGrid);
    EfOperationRefine refineOperation = new EfOperationRefine();
    refineOperation.setInitGridData(initGridData);
    // on met null car on ne veut pas r�cup�rer l'avancement du process
    EfOperationResult resOperation = refineOperation.process(null);
    assertFalse(resOperation.isFrontierChanged());
    EfGridData resGrid = resOperation.getGridData();

    // Recup�ration du nb de points de la nouvelle grille.
    EfGridInterface grid = resGrid.getGrid();
    int nbPtsResGrid = grid.getPtsNb();

    // Boucle sur tout les nouveaux points.
    for (int i = nbPtsInitGrid; i < nbPtsResGrid; i++) {
      // R�cup�ration de l'index de l'ancien element.
      int idxElt = i - nbPtsInitGrid;

      // R�cup�ration des coordonn�es du centre de l'ancien �l�ment.
      Coordinate defPtCoor = new Coordinate(defGrid.getMoyCentreXElement(idxElt), defGrid.getMoyCentreYElement(idxElt),
          defGrid.getMoyCentreZElement(idxElt));
      // R�cup�ration des coordonn�es du point en cours.
      Coordinate resPtCoor = grid.getCoor(i);

      // Test si les coordonn�es X sont �gales.
      assertEquals(defPtCoor.x, resPtCoor.x, eps);
      // Test si les coordonn�es Y sont �gales.
      assertEquals(defPtCoor.y, resPtCoor.y, eps);
      // Test si la distance entre les coordonn�es est nulle.
      assertTrue(CtuluLibGeometrie.getDistance(defPtCoor.x, defPtCoor.y, resPtCoor.x, resPtCoor.y) <= eps);
    }
  }

  /**
   * L'eps utilise pour tester l'�galite entre les double
   */
  private double eps = 1E-10;

  /**
   * On teste que le maillage cr�� est bien correct
   */
  public void testRefineNodeDataOnly() {
    // on cr�� un maillage simple de 50 point: 5 lignes contenant 10 point
    EfGridInterface defGrid = EfTestFactory.buildGrid(5, 10);
    EfGridDataDefault initGridData = new EfGridDataDefault(defGrid);
    // on va cr��es des valeurs d�finies aux noeuds
    int initPtsNb = defGrid.getPtsNb();
    double[] nodesValues = generateRandomArray(initPtsNb);
    EfDataNode nd = new EfDataNode(nodesValues);
    CtuluVariableDefault var = new CtuluVariableDefault("vitesse");
    initGridData.put(var, nd);
    EfOperationRefine refineOperation = new EfOperationRefine();
    refineOperation.setInitGridData(initGridData);
    // on met null car on ne veut pas r�cup�rer l'avancement du process
    EfOperationResult resOperation = refineOperation.process(null);
    assertFalse(resOperation.isFrontierChanged());
    EfGridData resGrid = resOperation.getGridData();
    try {

      EfData newVitesseData = resGrid.getData(var, 0);

      // Test que les donn�es soyent bien des donn�es de noeud.
      assertTrue(newVitesseData instanceof EfDataNode);

      // R�cup�ration des valeurs des donn�es initiales.
      double[] defData = initGridData.getData(var, 0).getValues();
      // R�cup�ration des valuers des donn�es r�sultantes.
      double[] resData = resGrid.getData(var, 0).getValues();

      int resPtsNb = resGrid.getGrid().getPtsNb();

      // Boucle sur les valeurs initiales.
      for (int i = 0; i < initPtsNb; i++) {
        // Test si la valeur courante est �gale � la m�me valeur dans le r�sultat.
        assertEquals(defData[i], resData[i], eps);
      }

      int idxElt;
      EfInterpolator efInterpolator = new EfInterpolator(initGridData, null);

      // Boucle sur toute les nouvelles valeurs.
      for (int i = initPtsNb; i < resPtsNb; i++) {
        // R�cup�ration de l'index de l'ancien element.
        idxElt = i - initPtsNb;

        // Test si le nouvelle valeur en cours est correcte.
        assertEquals(resData[i], efInterpolator.interpolate(idxElt, resGrid.getGrid().getPtX(i), resGrid.getGrid()
            .getPtY(i), var, 0), eps);
      }
    } catch (IOException e) {
      fail(e.getMessage());
    }

  }

  private double[] generateRandomArray(int ptsNb) {
    double[] nodesValues = new double[ptsNb];
    for (int i = 0; i < nodesValues.length; i++) {
      // valeurs au hasard entre 0 et 100
      nodesValues[i] = Math.random() * 100;
    }
    return nodesValues;
  }

}
