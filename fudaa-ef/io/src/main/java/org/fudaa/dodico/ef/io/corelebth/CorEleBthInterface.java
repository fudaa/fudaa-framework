/**
 * @creation 10 mars 2003
 * @modification $Date: 2006-09-19 14:45:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.corelebth;

import org.fudaa.dodico.ef.EfGridSource;

/**
 * @author deniger
 * @version $Id: CorEleBthInterface.java,v 1.14 2006-09-19 14:45:48 deniger Exp $
 */
public interface CorEleBthInterface extends EfGridSource {

}