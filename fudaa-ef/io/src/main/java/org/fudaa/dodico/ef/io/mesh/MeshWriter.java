package org.fudaa.dodico.ef.io.mesh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridDefaultAbstract;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

public class MeshWriter extends FileOpWriterCharSimpleAbstract
{
    private FortranWriter writer;
    
    @Override
    protected void internalWrite(Object o)
    {
        if (o instanceof EfGridInterface)
        {
            this.writeGrid((EfGridDefaultAbstract)o);
        }
        else if (o instanceof EfGridSource)
        {
            this.writeGrid(((EfGridSource)o).getGrid());
        }
    }

    private void writeGrid(EfGridInterface grid)
    {
        this.writer = new FortranWriter(this.out_);
        
        EfNode[] nodes = grid.getNodes();
        EfElement[] elements = grid.getElts();
        HashMap<String, List<EfElement>> sortedElements = new HashMap<String, List<EfElement>>();
        
        for (int i = 0; i < MeshFormatHelper.ELEMENTS_HEADER.size(); i++)
        {
            sortedElements.put(MeshFormatHelper.ELEMENTS_HEADER.get(i), new ArrayList<EfElement>());
        }
        
        for (int i = 0; i < elements.length; i++)
        {
            String elementKind = MeshFormatHelper.getElementKind(elements[i]);
            
            if (MeshFormatHelper.isElementsHeader(elementKind))
            {
                sortedElements.get(elementKind).add(elements[i]);
            }
            else
            {
                // TODO Y a un probl�me.
            }
        }

        try
        {
            this.writeVersion();
            this.writeDimension();
            this.writeNodes(nodes);
            this.writeElements(sortedElements);
            this.writeEnd();
        }
        catch (IOException e)
        {
            analyze_.manageException(e);
        }
    }
    
    /**
     * Ecrit la version.
     * @throws IOException
     */
    private void writeVersion() throws IOException
    {
        this.writer.stringField(0, MeshFormatHelper.VERSION_HEADER);
        this.writer.writeFields();
        this.writer.intField(0, MeshFormatHelper.VERSION_VALUE);
        this.writer.writeFields();
    }
    
    /**
     * Ecrit la dimension.
     * @throws IOException
     */
    private void writeDimension() throws IOException
    {
        this.writer.stringField(0, MeshFormatHelper.DIMENSION_HEADER);
        this.writer.writeFields();
        this.writer.intField(0, MeshFormatHelper.DIMENSION_VALUE);
        this.writer.writeFields();
    }
    
    /**
     * Ecrit les noeuds.
     * @param nodes la liste de noeuds � �crire.
     * @throws IOException
     */
    private void writeNodes(EfNode[] nodes) throws IOException
    {
        this.writer.stringField(0, MeshFormatHelper.NODE_HEADER);
        this.writer.writeFields();
        this.writer.intField(0, nodes.length);
        this.writer.writeFields();
        
        for (int i = 0; i < nodes.length; i++)
        {
            this.writer.doubleField(0, nodes[i].getX());
            this.writer.doubleField(1, nodes[i].getY());
            this.writer.doubleField(2, nodes[i].getZ());
            this.writer.intField(3, MeshFormatHelper.NODE_REFERENCE);
            
            this.writer.writeFields();
        }
    }
    
    /**
     * Ecrit les �l�ments.
     * @param sortedElements la map contenant la liste des �l�ments pour chaque type d'�l�ments.
     * @throws IOException
     */
    private void writeElements(HashMap<String, List<EfElement>> sortedElements) throws IOException
    {
        for (int i = 0; i < MeshFormatHelper.ELEMENTS_HEADER.size(); i++)
        {
            List<EfElement> currentElements = sortedElements.get(MeshFormatHelper.ELEMENTS_HEADER.get(i));
            
            if (currentElements.size() > 0)
            {
                this.writer.stringField(0, MeshFormatHelper.ELEMENTS_HEADER.get(i));
                this.writer.writeFields();
                this.writer.intField(0, currentElements.size());
                this.writer.writeFields();
                
                for (int j = 0; j < currentElements.size(); j++)
                {
                    int[] pts = currentElements.get(j).getIndices();
                                        
                    for (int k = 0; k < pts.length; k++)
                    {
                        this.writer.intField(k, pts[k]);
                    }
                    
                    this.writer.intField(pts.length, MeshFormatHelper.ELEMENT_REFERENCE);
                    this.writer.writeFields();
                }
            }
        }
    }
    
    /**
     * Ecrit la fin.
     * @throws IOException
     */
    private void writeEnd() throws IOException
    {
        this.writer.stringField(0, MeshFormatHelper.END_HEADER);
        this.writer.writeFields();
    }
}
