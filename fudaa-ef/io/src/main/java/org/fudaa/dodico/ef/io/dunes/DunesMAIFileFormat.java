/*
 *  @creation     13 mars 2003
 *  @modification $Date: 2006-11-15 09:22:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.ef.io.dunes;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: DunesFileFormat.java,v 1.25 2006-11-15 09:22:54 deniger Exp $
 */
public final class DunesMAIFileFormat extends FileFormatUnique implements DunesVersionInterface, FileFormatGridVersion {

  public static final DunesMAIFileFormat INSTANCE = new DunesMAIFileFormat();

  /**
   * @return le singleton
   */
  public static DunesMAIFileFormat getInstance() {
    return INSTANCE;
  }

  private DunesMAIFileFormat() {
    super(1);
    extensions_ = new String[] { "mail", "dunes" };
    id_ = "DUNES";
    nom_ = "Dunes maillage";
    description_ = EfIOResource.getS("utilis� par le mailleur Dunes");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  public boolean containsGrid() {
    return true;
  }

  /**
   * Returns the aDAPTATIF.
   * 
   * @return int
   */
  @Override
  public int getAdaptatifCode() {
    return 2;
  }

  /**
   * Returns the eLEMENT_ID.
   * 
   * @return String
   */
  @Override
  public String getElementCode() {
    return "ELEMENT";
  }

  /**
   * Returns the nOEUD_ID.
   * 
   * @return String
   */
  @Override
  public String getNoeudCode() {
    return "NOEUD";
  }

  /**
   * Returns the nON_ADAPTATIF.
   * 
   * @return int
   */
  @Override
  public int getNonAdaptatifCode() {
    return 1;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new DunesMAIReader(this);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new DunesMAIWriter(this);
  }

  /**
   * @param _f le fichier cible a ecrire
   * @param _inter le source des donnees
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _f, final DunesInterface _inter, final ProgressionInterface _prog) {
    final DunesMAIWriter w = new DunesMAIWriter(this);
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  /**
   * @param _f le fichier cible
   * @param _m le maillage a ecrire
   * @param _prog la barre de progression
   * @return la synthese
   */
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridInterface _m, final ProgressionInterface _prog) {
    final DunesAdapter inter = new DunesAdapter();
    inter.setMaillage(_m);
    return write(_f, inter, _prog);
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return writeGrid(_f, _m.getGrid(), _prog);
  }

  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    return read(_f, _prog);
  }

  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese s = readGrid(_f, _prog);
    s.setSource(((EfGridSource) s.getSource()).getGrid());
    return s;
  }

  @Override
    public boolean canReadGrid()
    {
        return true;
    }
    
  @Override
    public boolean canWriteGrid()
    {
        return true;
    }
    
  @Override
    public boolean hasBoundaryConditons()
    {
        // TODO Voir si correct
        return false;
    }

}