/**
 * @creation 3 nov. 2004
 * @modification $Date: 2006-10-16 07:57:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import com.memoire.bu.BuFileFilter;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: SerafinFileFilter.java,v 1.9 2006-10-16 07:57:36 deniger Exp $
 */
public class SerafinFileFilter extends BuFileFilter {
  /**
   * @param _ft le format serafin
   */
  public SerafinFileFilter(final SerafinFileFormat _ft) {
    super(_ft.getExtensions(), _ft.getName());
  }

  boolean strict_;

  public SerafinFileFilter(final SerafinFileFormat _ft, final boolean _strict) {
    super(_strict ? new String[]{"ser"} : _ft.getExtensions(), _ft.getName());
    strict_ = _strict;
  }

  @Override
  public boolean accept(final File _d, final String _fn) {
    if (_fn == null) {
      return false;
    }
    boolean res = super.accept(_d, _fn);
    if (strict_) {
      return res;
    }
    if (!res) {
      res = isOk(_fn);
    }
    return res;
  }

  private boolean isOk(final String _name) {
    if ((_name.indexOf('.') < 0)) {
      return _name.startsWith("geo") || _name.startsWith("res") || _name.endsWith("RES");
    }
    return false;
  }

  @Override
  public boolean accept(final File _f) {
    if (_f == null) {
      return false;
    }
    final String name = _f.getName();
    if (name == null) {
      return false;
    }
    boolean res = super.accept(_f);
    if (!res) {
      res = isOk(name);
    }
    return res;
  }

  /**
   * @param _name le nom du fichier a tester
   * @return true si c'est un fichier r�sultat
   */
  private boolean isResFile(final String _name) {
    if (_name.endsWith(".slf")) {
      return _name.startsWith("r") && _name.indexOf('_') > 0;
    }
    return _name.endsWith(".resu") || _name.endsWith(".res3d") || _name.endsWith(".res2d") || _name.endsWith(".res") || ((_name.startsWith("res") || _name
        .endsWith("RES")) && (_name.indexOf('.') < 0));
  }

  /**
   * @param _f le fichier a tester
   * @return true si c'est un fichier r�sultat
   */
  public boolean isResFile(final File _f) {
    return _f.isFile() && isResFile(_f.getName());
  }
}
