/*
 *  @creation     19 f�vr. 2003
 *  @modification $Date: 2007-01-19 13:07:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.dunes;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * Lecteur de fichiers dunes (.mail).
 * 
 * @author deniger
 * @version $Id: DunesReader.java,v 1.28 2007-01-19 13:07:22 deniger Exp $
 */
public class DunesMAIReader extends FileOpReadCharSimpleAbstract {

  DunesVersionInterface version_;

  /**
   * Utilise la derniere version de DunesFileFormat.
   */
  public DunesMAIReader() {
    this(DunesMAIFileFormat.getInstance());
  }

  /**
   * @param _f le format a utiliser
   */
  public DunesMAIReader(final DunesVersionInterface _f) {
    version_ = _f;
  }

  @Override
  protected Object internalRead() {
    return readDunes();
  }

  private EfElement[] readElement(final ProgressionUpdater _up) throws IOException {
    final int nbElem = in_.intField(0);
    // Les donnees utiles pour l'etat d'avancement
    _up.setValue(2, nbElem, 50, 50);
    _up.majProgessionStateOnly();
    int[] index;

    final EfElement[] elems = new EfElement[nbElem];
    for (int i = 0; i < nbElem; i++) {
      in_.readFields();
      if (in_.getNumberOfFields() < 2) {
        analyze_.addInfo(EfIOResource.getS("Format attendu") + "NUM_ELEMENT NB_POINT P[1] ...P[NB_POINT]", in_
            .getLineNumber());
        analyze_.addError(EfIOResource.getS("La d�finition de l'�l�ment {0} n'est pas valide"), in_.getLineNumber());
        return null;
      }
      // le numero d'element
      int temp = in_.intField(0);
      if (temp != (i + 1)) {
        analyze_.addInfo(EfIOResource.getS("L'index de l'�l�ment ne correspond pas � l'index r�el"), in_
            .getLineNumber());
      }
      temp = in_.intField(1);
      if (temp <= 0) {
        analyze_.addError(EfIOResource.getS("Le nombre de points est n�gatif ou nul pour l'�l�ment d'indice {0}",
            CtuluLibString.getString(i + 1)), in_.getLineNumber());
        return null;
      }
      if (in_.getNumberOfFields() != (temp + 2)) {
        analyze_.addInfo(EfIOResource.getS("Nombre indiqu�") + ": " + temp + CtuluLibString.DOT
            + EfIOResource.getS("Nombre lu") + ": " + (in_.getNumberOfFields() - 2), in_.getLineNumber());
        analyze_.addFatalError(EfIOResource.getS("Le nombre d'�l�ments est incorrect"), in_.getLineNumber());
        return null;
      }
      index = new int[temp];
      for (int j = 0; j < temp; j++) {
        // on stocke les indexs en commencant a 0.
        index[j] = in_.intField(2 + j) - 1;
      }
      elems[i] = new EfElement(index);
      _up.majAvancement();
    }
    return elems;
  }

  private EfNode[] readNodes(final int _nbNoeuds, final ProgressionUpdater _up, final double[] _adaptatifs)
      throws IOException {
    final EfNode[] points = new EfNode[_nbNoeuds];
    _up.setValue(2, _nbNoeuds, 0, 50);
    _up.majProgessionStateOnly();
    int temp;
    // lecture des noeuds
    for (int i = 0; i < _nbNoeuds; i++) {
      in_.readFields();
      if (in_.getNumberOfFields() != 4) {
        analyze_.addInfo(EfIOResource.getS("Format attendu") + ": NUM_POINT X Y Z", in_.getLineNumber());
        analyze_.addError(EfIOResource.getS("La d�finition du noeud {0} n'est pas valide", CtuluLibString
            .getString(i + 1)), in_.getLineNumber());
        return null;
      }
      temp = in_.intField(0);
      if (temp != (i + 1)) {
        analyze_.addInfo(EfIOResource.getS("Le num�ro du point ne correspond pas au num�ro r�el"), in_.getLineNumber());
      }
      points[i] = new EfNode(in_.doubleField(1), in_.doubleField(2), in_.doubleField(3));
      if (_adaptatifs != null) {
        in_.readFields();
        if (in_.getNumberOfFields() != 1) {
          analyze_.addWarn(EfIOResource.getS("La ligne du coefficient adaptatif ne doit contenir qu'un champ"), in_
              .getLineNumber());
        }
        _adaptatifs[i] = in_.doubleField(0);
      }
      _up.majAvancement();
    }
    return points;

  }

  private DunesInterface readDunes() {
    DunesAdapter source = null;
    EfNode[] points = null;
    EfElement[] elems = null;
    double[] adaptatifs = null;
    in_.setJumpBlankLine(true);
    try {
      // lit jusqu'a la premiere ligne non vide.
      // Lecture de l'identifiant NOEUD
      in_.readFields();
      if ((in_.getNumberOfFields() != 1) || (!version_.getNoeudCode().equals(in_.stringField(0)))) {
        analyze_.addError(EfIOResource.getS("L'identifiant {0} est attendu", version_.getNoeudCode()), in_
            .getLineNumber());
        return null;

      }
      in_.readFields();
      // Lecture du nombre de noeud et format du maillage (apaptatif ou non)
      // NOMBRE_DE_NOEUD TYPE_DE_MAILLAGE

      // verification que l'on a bien 2 champs
      if (in_.getNumberOfFields() != 2) {
        analyze_.addFatalError(EfIOResource.getS("Nombre de noeuds non trouv�"), in_.getLineNumber());
        return null;
      }
      // le nombre de noeud
      final int nbNoeuds = in_.intField(0);
      // le type de maillage.
      // Si 2, des infos sur le maillage adaptatif seront presentes
      final int typeMaillage = in_.intField(1);
      // Si adaptatif, on initialise le tableau stockant les valeurs.
      if (typeMaillage == version_.getAdaptatifCode()) {
        adaptatifs = new double[nbNoeuds];
        // on affecte a source le tableau.
      }
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      points = readNodes(nbNoeuds, up, adaptatifs);
      if (points == null) { return null; }
      // Lecture du mot cle ELEMENT
      in_.readFields();
      if ((in_.getNumberOfFields() != 1) && (!version_.getElementCode().equals(in_.stringField(0)))) {
        analyze_.addError(EfIOResource.getS("L'identifiant {0} est attendu", version_.getElementCode()), in_
            .getLineNumber());
        return null;
      }
      // Lecture du nombre d'elements.
      in_.readFields();
      // Un seul champ attendu
      if (in_.getNumberOfFields() != 1) {
        analyze_.addFatalError(EfIOResource.getS("Cette ligne est invalide"), in_.getLineNumber());
        return null;
      }
      elems = readElement(up);
      if (elems == null) { return null; }
      in_.readFields();
      if (!"0 0 0 0".equals(in_.getLine().trim())) {
        if (in_.getLine().trim().length() > 0) {
          analyze_.addWarn(EfIOResource.getS("Cette ligne et les suivantes sont ignor�es"), in_.getLineNumber());
        }
      }
      in_.readFields();
      if (in_.getLine().trim().length() > 0) {
        analyze_.addWarn(EfIOResource.getS("Cette ligne et les suivantes sont ignor�es"), in_.getLineNumber());
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Dunes Read end");
      }
    } catch (final NumberFormatException fe) {
      analyze_.manageException(fe, in_.getLineNumber());
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    // Le mot clef noeud est trouve, on peut creer le maillage.
    final EfGrid maillage = new EfGrid(points, elems);
    EfLib.orienteGrid(maillage, progress_, true, analyze_);
    source = new DunesAdapter();
    source.setMaillage(maillage);
    source.setAdaptatif(adaptatifs);
    return source;
  }

  /**
   * @return le format
   */
  public FileFormat getFileFormat() {
    return getDunesFileFormat();
  }

  /**
   * @return le format utilise
   */
  public DunesMAIFileFormat getDunesFileFormat() {
    return (DunesMAIFileFormat) version_.getFileFormat();
  }

  /**
   * @return la version utilisee
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }
}