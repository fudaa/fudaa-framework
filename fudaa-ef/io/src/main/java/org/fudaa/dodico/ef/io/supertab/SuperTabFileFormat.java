package org.fudaa.dodico.ef.io.supertab;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

public class SuperTabFileFormat extends FileFormatUnique implements FileFormatGridVersion
{
    private static final SuperTabFileFormat INSTANCE = new SuperTabFileFormat();

    /**
     * @return singleton
     */
    public static SuperTabFileFormat getInstance() {
      return INSTANCE;
    }

    protected SuperTabFileFormat() {
      super(1);
      extensions_ = new String[] { "unv","fst","fastab","fast" };
      id_ = "SUPERTAB";
      nom_ = "SuperTab (unv)";
      
      //TODO Voir si correct.
      description_ = CtuluLibString.EMPTY_STRING;
      software_ = FileFormatSoftware.TELEMAC_IS;
    }

  @Override
    public CtuluIOOperationSynthese readGrid(File f, ProgressionInterface prog)
    {
        return read(f, prog);
    }

  @Override
    public CtuluIOOperationSynthese writeGrid(File f, EfGridSource m, ProgressionInterface prog)
    {
        return null;
    }

  @Override
    public FileReadOperationAbstract createReader()
    {
        return new SuperTabReader();
    }

  @Override
    public FileWriteOperationAbstract createWriter()
    {
        return null;
    }

  @Override
    public boolean canReadGrid()
    {
        return true;
    }

  @Override
    public boolean canWriteGrid()
    {
        return false;
    }

  @Override
    public boolean hasBoundaryConditons()
    {
        return true;
    }
}
