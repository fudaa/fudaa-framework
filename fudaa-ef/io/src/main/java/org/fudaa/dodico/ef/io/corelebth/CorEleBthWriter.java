/**
 * @creation 2003-03-12
 * @modification $Date: 2006-09-19 14:45:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.corelebth;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * @author deniger
 * @version $Id: CorEleBthWriter.java,v 1.28 2006-09-19 14:45:47 deniger Exp $
 */
public class CorEleBthWriter extends FileWriteOperationAbstract {

  private final int df_;
  private final int sf_;
  private final CorEleBthFileFormat version_;
  FortranWriter bthOut_;
  FortranWriter corOut_;
  FortranWriter eleOut_;
  /**
   * True si la premiere colonne est cod�e sur 5 caracateres.
   */
  boolean simpleColumn_;

  protected String lineSep_;

  /**
   * @param _f la version a utiliser
   */
  public CorEleBthWriter(final CorEleBthFileFormat _f) {
    lineSep_ = CtuluLibString.LINE_SEP;
    version_ = _f;
    sf_ = version_.getSimpleColumLength();
    df_ = version_.getDoubleColumLength();
  }

  private void setOut(final Writer _cor, final Writer _ele, final Writer _bth) {
    corOut_ = _cor == null ? null : new FortranWriter(_cor);
    eleOut_ = _ele == null ? null : new FortranWriter(_ele);
    bthOut_ = _bth == null ? null : new FortranWriter(_bth);
  }

  /**
   * @return <code>true</code> si la premiere colonne est cod�e sur 5 caracateres.
   */
  public boolean isSimpleColumn() {
    return simpleColumn_;
  }

  public void setSimpleColumn(final boolean _simpleColumn) {
    simpleColumn_ = _simpleColumn;
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (corOut_ != null) {
          corOut_.close();
          corOut_ = null;
        }
        if (eleOut_ != null) {
          eleOut_.close();
          eleOut_ = null;
        }
        if (bthOut_ != null) {
          bthOut_.close();
          bthOut_ = null;
        }
      }
    };
  }

  /**
   *
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof CorEleBthInterface) {
      writeCorEleBth((CorEleBthInterface) _o);
    } else {
      donneesInvalides(_o);
    }
  }

  /**
   * @return le format.
   */
  public FileFormat getFileFormat() {
    return version_.getFileFormat();
  }

  /**
   * Renvoie le separateur de ligne.
   * 
   * @return separateur de ligne
   */
  public final String getLineSeparator() {
    return lineSep_;
  }

  /**
   * @return la version utilisee.
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }

  @Override
  public final void setFile(final File _f) {
    setFile(version_.getFileFormat().getFile(_f));
  }

  /**
   * Aucun test sur les extension.
   * 
   * @param _cor fichier cor cible
   * @param _ele fichier ele cible
   * @param _bth fichier bth cible
   */
  public final void setFile(final File _cor, final File _ele, final File _bth) {
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_cor.getAbsolutePath());
    setOut(FileOperationAbstract.initFileWriter(_cor), FileOperationAbstract.initFileWriter(_ele),
        FileOperationAbstract.initFileWriter(_bth));
  }

  /**
   * Aucun test sur les extension.
   * 
   * @param _f cor,ele,bth
   */
  public final void setFile(final File[] _f) {
    setFile(_f[0], _f[1], _f[2]);
  }

  /**
   * Modifie le separateur de ligne.
   * 
   * @param _lineSep The lineSep to set
   */
  public final void setLineSeparator(final String _lineSep) {
    lineSep_ = _lineSep;
  }

  /**
   * @param _o la source a ecrire
   * @return la synthese
   */
  public final CtuluIOOperationSynthese write(final CorEleBthInterface _o) {
    writeCorEleBth(_o);
    return closeOperation(_o);
  }

  /**
   * @param _percBegin le pourcentage de debut
   * @param _percLength le longueur de la plage pourcentage
   * @param _maillage le maillge
   * @throws IOException io
   */
  public void writeBth(final int _percBegin, final int _percLength, final EfGridInterface _maillage) throws IOException {
    final int pointParLigne = version_.getBthPointByLine();
    if (bthOut_ == null) {
      analyze_.addFatalError(EfIOResource.getS("Le flux pour la bathym�trie est nul"));
      return;
    }
    bthOut_.setLineSeparator(getLineSeparator());
    bthOut_.setStringQuoted(false);
    // le format des lignes.
    final int[] fmt = new int[pointParLigne];
    for (int i = pointParLigne - 1; i >= 0; i--) {
      fmt[i] = version_.getBthColumnLength();
    }
    // les points concernes
    // H2dPoint[] pts= _maillage.getPts();
    final int l = _maillage.getPtsNb();
    // gestion de l'avancement
    final int nbLigne = l / pointParLigne;
    int index = 0;
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(nbLigne, 4, _percBegin, _percLength);
    up.majProgessionStateOnly();
    for (int numLigne = 0; numLigne < nbLigne; numLigne++) {
      for (int j = pointParLigne - 1; j >= 0; j--) {
        bthOut_.doubleField(j, _maillage.getPtZ(index + j));
      }
      index += pointParLigne;
      bthOut_.writeFields(fmt);
      // maj de l'avancement
      up.majAvancement();
    }
    final int coteRestante = l - nbLigne * pointParLigne;
    if (index != l - coteRestante) {
      FuLog.error("prob in cote restante");
    }
    if (coteRestante > 0) {
      for (int j = coteRestante - 1; j >= 0; j--) {
        bthOut_.doubleField(j, _maillage.getPtZ(index + j));
      }
      bthOut_.writeFields(fmt);
    }
    bthOut_.flush();
  }

  /**
   * @param _percBegin pourcentage de debut
   * @param _percLength longueur de la plage avancement
   * @param _format le format
   * @param _maillage le maillage soure
   * @throws IOException
   */
  public void writeCor(final int _percBegin, final int _percLength, final int _format, final EfGridInterface _maillage)
      throws IOException {
    if (corOut_ == null) {
      analyze_.addFatalError(EfIOResource.getS("Le flux de coordonn�es est nul"));
      return;
    }
    corOut_.setLineSeparator(getLineSeparator());
    corOut_.setStringQuoted(false);
    // H2dPoint[] pts= _maillage.getPts();
    final int nbPt = _maillage.getPtsNb();
    int[] fmt = new int[] { _format, sf_, sf_, df_, df_, df_ };
    // on fixe le dernier element
    corOut_.doubleField(5, 1D);
    corOut_.intField(0, nbPt);
    corOut_.intField(1, 2);
    corOut_.intField(2, 3);
    corOut_.doubleField(3, 1D);
    corOut_.doubleField(4, 1D);
    corOut_.writeFields(fmt);
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(nbPt, 4, _percBegin, _percLength);
    up.majProgessionStateOnly();
    fmt = new int[] { _format, df_, df_ };
    for (int i = 0; i < nbPt; i++) {
      corOut_.doubleField(2, _maillage.getPtY(i));
      corOut_.intField(0, i + 1);
      corOut_.doubleField(1, _maillage.getPtX(i));
      corOut_.writeFields(fmt);
      // maj de l'avancement
      up.majAvancement();
    }
    corOut_.flush();
  }

  /**
   * @param _inter la source a ecrire
   */
  public void writeCorEleBth(final CorEleBthInterface _inter) {
    final CorEleBthInterface source = _inter;
    if (source == null) {
      analyze_.addFatalError(DodicoLib.getS("Les donn�es sont nulles"));
      return;
    }
    final EfGridInterface maillage = source.getGrid();
    int format = version_.getDoubleColumLength();
    if (simpleColumn_) {
      format = format / 2;
    }

    // Si le nombre de points ne tiend pas dans le format simple
    // on suppose que nbElments<NbPoints ...
    final int l = maillage.getPtsNb();
    final int nbDigits = String.valueOf(l).length();
    // if (nbDigits > CorEleBthInterface.COLUMN_SIMPLE_LENGTH)
    // on ecrit toujours dans le format double.a voir
    if (nbDigits > version_.getDoubleColumLength()) {
      analyze_.addError(EfIOResource.getS("Le nombre de points est trop important"), 0);
      return;
    }
    final boolean afficheAvance = (progress_ == null ? false : true);
    if (afficheAvance) {
      progress_.setDesc(EfIOResource.getS("Ecriture fichiers cor,ele et bth"));
    }
    // pour le pourcentage d'avancement on suppose que la lecture
    // des points prend 40% du temps.
    int percBegin = 0;
    int percLength = 40;
    try {
      writeCor(percBegin, percLength, format, maillage);
    } catch (final IOException _io) {
      analyze_.manageException(_io);
      return;
    }
    // pour le pourcentage d'avancement on suppose que la lecture
    // des elements prend 40% du temps et part de 40.
    percBegin += percLength;
    try {
      writeEle(percBegin, percLength, format, maillage);
    } catch (final IOException _io) {
      analyze_.manageException(_io);
      return;
    }
    percBegin += percLength;
    percLength = 20;
    try {
      writeBth(percBegin, percLength, maillage);
    } catch (final IOException _io) {
      analyze_.manageException(_io);
      return;
    }
  }

  /**
   * @param _percBegin pourcentage debut
   * @param _percLength etendu pourcentage
   * @param _format le fomat
   * @param _maillage le maillage
   * @throws IOException io
   */
  public void writeEle(final int _percBegin, final int _percLength, final int _format, final EfGridInterface _maillage)
      throws IOException {
    if (eleOut_ == null) {
      analyze_.addFatalError(EfIOResource.getS("Le flux pour les �l�ments est nul"));
      return;
    }
    eleOut_.setLineSeparator(getLineSeparator());
    eleOut_.setStringQuoted(false);
    // H2dElement[] ele= _maillage.getElts();
    final int l = _maillage.getEltNb();
    // dans le cas double ??
    int[] fmt = new int[] { _format, sf_, sf_, sf_, sf_ };
    // utilite de ces 3 champs ?
    eleOut_.intField(4, 0);
    eleOut_.intField(3, 0);
    eleOut_.intField(2, 0);
    eleOut_.intField(1, _maillage.getEltType().getNbPt());
    eleOut_.intField(0, l);
    eleOut_.writeFields(fmt);
    fmt = new int[] { _format, // le num
        version_.getEleSecondColumnLength(), // ?
        _format, // ele 1
        _format, // ele 2
        _format, // ele 3
        _format, // ele 4
        _format, // ele 5
        _format, // ele 6
        _format // le 0 final
    };
    // Gestion de l'avancement
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(l, 4, _percBegin, _percLength);
    up.majProgessionStateOnly();
    EfElement elei;
    int taille;
    for (int i = 0; i < l; i++) {
      elei = _maillage.getElement(i);
      taille = elei.getPtNb();
      // le 0 final.
      eleOut_.intField(2 + taille, 0);
      eleOut_.intField(0, i + 1);
      eleOut_.stringField(1, " -1");
      for (int j = 0; j < taille; j++) {
        // les indexs de elei commencent a 0 !
        eleOut_.intField(2 + j, elei.getPtIndex(j) + 1);
      }
      eleOut_.writeFields(fmt);
      // maj de l'avancement
      up.majAvancement();
    }
    eleOut_.flush();
  }
}