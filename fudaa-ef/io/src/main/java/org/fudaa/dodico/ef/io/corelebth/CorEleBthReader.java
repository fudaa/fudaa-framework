/**
 * @creation 21 f�vr. 2003
 * @modification $Date: 2007-06-20 12:21:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.corelebth;

import gnu.trove.TIntArrayList;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileOperationAbstract;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridMutable;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * Lecture des fichiers cor,ele,bth.
 * 
 * @author Bertrand Marchand, Fred Deniger
 * @version $Id: CorEleBthReader.java,v 1.36 2007-06-20 12:21:44 deniger Exp $
 */
public class CorEleBthReader extends FileReadOperationAbstract {

  private final CorEleBthFileFormat version_;
  FortranReader corIn_;
  FortranReader eleIn_;
  FortranReader bthIn_;

  /**
   * Le constructeur par defaut. Les parametres <code>_cor</code> et <code>_ele</code> peuvent etre nuls.
   * 
   * @param _format version
   */
  public CorEleBthReader(final CorEleBthFileFormat _format) {
    version_ = _format;
  }

  private void setIn(final Reader _corIn, final Reader _eleIn, final Reader _bthIn) {
    setIn(_corIn == null ? null : new FortranReader(_corIn), _eleIn == null ? null : new FortranReader(_eleIn),
        _bthIn == null ? null : new FortranReader(_bthIn));
  }

  @Override
  public void setFile(final File _f) {
    setFile(version_.getFileFormat().getFile(_f));
  }

  /**
   * @param _f les fichiers cor,ele,bth
   */
  public void setFile(final File[] _f) {
    if (_f.length != CorEleBthFileFormat.NB_FILE) { throw new IllegalArgumentException("Number of file expected : 3"); }
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f[0].getAbsolutePath());
    setIn(FileOperationAbstract.initFileReader(_f[0]), FileOperationAbstract.initFileReader(_f[1]),
        FileOperationAbstract.initFileReader(_f[2]));
  }

  /**
   * @param _cor fichier cor
   * @param _ele fichier ele
   * @param _bth fichier bth
   */
  public void setIn(final File _cor, final File _ele, final File _bth) {
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_cor.getAbsolutePath());
    setIn(FileOperationAbstract.initFileReader(_cor), FileOperationAbstract.initFileReader(_ele), FileOperationAbstract
        .initFileReader(_bth));
  }

  private void setIn(final FortranReader _corIn, final FortranReader _eleIn, final FortranReader _bthIn) {
    if (_corIn == null) {
      analyze_.addFatalError(EfIOResource.getS("Le flux de coordonn�es est null"));
      return;
    }
    if (_eleIn == null) {
      analyze_.addFatalError(EfIOResource.getS("Le flux de �l�ments est null"));
      return;
    }

    corIn_ = _corIn;
    eleIn_ = _eleIn;
    bthIn_ = _bthIn;
  }

  /**
   * Lecture des fichiers. Le resultat stock� et obtenu avec <code>getMaillage()</code>. Si l'analyse (
   * <code>getAnalyse()</code>) poss�de une erreur, le maillage doit etre nul.
   * 
   * @return le resultat de l'ecriture
   */
  public CorEleBthAdapter readCorEleBth() {
    CorEleBthAdapter inter = null;
    EfGridMutable maillage;
    int percBegin = 0;
    int percLength = 40;
    EfNode[] pts;
    if (corIn_ == null) {
      analyze_.addFatalError(EfIOResource.getS("Le flux de coordonn�es est null"));
      return null;
    }
    try {
      pts = readCor(percBegin, percLength);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
      return null;
    }
    if (pts == null) { return null; }
    maillage = new EfGridMutable(pts, null);
    // lecture des elements
    // pour le pourcentage d'avancement on suppose que la lecture
    // des elements prend 40% du temps et part de 40.
    percBegin += percLength;
    // H2dElement[] elems= null;
    try {
      readEle(maillage, percBegin, percLength, pts.length);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    if (!maillage.isEltDefini()) { return null; }
    // gestion de l'avancement
    percBegin += percLength;
    if (bthIn_ == null) {
      analyze_.addWarn(EfIOResource.getS("Les donn�es concernant la bathym�trie sont introuvables"), -1);
      return null;
    }
    percLength = 20;
    // si fichier de bathy, on met a jour la bathy.
    // pour le pourcentage d'avancement on suppose que la lecture
    // de la bathy prend 20% du temps et part de 80.
    try {
      readBth(percBegin, percLength, maillage);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    inter = new CorEleBthAdapter();
    inter.setMaillage(maillage);
    EfLib.orienteGrid(maillage, progress_, true, analyze_);
    return inter;
    // isDone_ = true;
  }

  /**
   * Les fichiers elements donnent le nombre d'elements et les numeros des noeuds composant chaque element. Seuls les
   * elements T3 et T6 sont acceptes.
   */
  private void readEle(final EfGridMutable _maillage, final int _percBegin, final int _percLength, final int _nbMaxPoint)
      throws IOException {
    EfElement[] ele = null;
    final int doubleLength = version_.getDoubleColumLength();
    final int simpleLength = version_.getSimpleColumLength();
    final String analPref = "ELE ";
    eleIn_.setBlankZero(true);
    try {
      // Les formats de ce fichier peut varier. Il faut tester la deuxieme
      // ligne pour savoir si les champs sont de longueur double ou simple.
      // On marque le debut du re pour revenir en suite.
      // Au max, la premiere ligne contient 5champs de taille double.
      // la deuxieme ligne contient 13 champs de taille double.
      // pour etre sur, on prendra une marque pour 20 champs.
      eleIn_.mark(20 * doubleLength);
      // Lecture de la premiere ligne
      eleIn_.readLine();
      int format = doubleLength;
      // Lecture de la deuxieme ligne et test
      if (isSimpleFormat(eleIn_.readLine())) {
        format = simpleLength;
      }
      // on connait la largeur des colonnes on peut lire.
      eleIn_.reset();
      // Premiere ligne : <nombre d'�l�ments>,<nombre de noeuds/�l�ment>
      // (Les champs suivants importent peu)
      eleIn_.readFields();
      // Le format depend du nombre d'elements
      // Le nombre d'elements
      final int nbElements = eleIn_.intField(0);
      // le nombre max de points par elements
      final int nbMaxPts = eleIn_.intField(1);
      if (nbMaxPts == 3) {
        _maillage.setTypeElt(EfElementType.T3);
      } else if (nbMaxPts == 6) {
        _maillage.setTypeElt(EfElementType.T6);
      } else {
        // _maillage.setElts(null);
        analyze_.addWarn(analPref + EfIOResource.getS("Seuls les �l�ments T3 ou T6 sont correctement trait�s"), eleIn_
            .getLineNumber());
        // return;
      }
      // Connectivit�s
      // Le format par defaut pour toutes les lignes.
      final int[] fmt = new int[] { format, version_.getEleSecondColumnLength(), format, format, format, format,
          format, format, format };
      final int indexMax = _nbMaxPoint;
      int indexPt;
      ele = new EfElement[nbElements];
      TIntArrayList connect = new TIntArrayList(nbMaxPts);
      // Getion de l'etat d'avancement
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(nbElements, 4, _percBegin, _percLength);
      up.majProgessionStateOnly();
      boolean errorForIndiceDone = false;
      for (int i = 0; i < nbElements; i++) {
        // Le tableau qui contiendra temporaire les connexions.

        // on parse la ligne
        eleIn_.readFields(fmt);
        // if (nbPtInElem > 3) System.out.println(eleIn_.getNumberOfNotEmptyField());
        connect.resetQuick();
        if (eleIn_.intField(0) != (i + 1)) {
          final double nbValue = Math.log(i + 1) / Math.log(10);
          boolean ok = false;
          if (nbValue >= format) {
            fmt[0]++;
            eleIn_.analyzeCurrentLine(fmt);
            ok = eleIn_.intField(0) == (i + 1);
          }
          if (!ok && !errorForIndiceDone) {
            errorForIndiceDone = true;
            analyze_.addError(analPref + EfIOResource.getS("L'indice est invalide")
                + CtuluLibString.getEspaceString(eleIn_.getLineNumber()), eleIn_.getLineNumber());
            analyze_.addInfo(analPref + EfIOResource.getS("Indice attendu") + ": " + (i + 1), eleIn_.getLineNumber());
          }
        }

        int numberOfFields = eleIn_.getNumberOfFields();
        int idx = 0;
        for (int numChamp = 2; numChamp < numberOfFields && idx < nbMaxPts; numChamp++) {

          // Les 2 premiers champs sont ignores.
          indexPt = eleIn_.intField(numChamp);

          if ((indexPt > indexMax) || (indexPt < 0)) {
            analyze_.addError(analPref
                + EfIOResource.getS("L'indice {0} n'est pas valide", CtuluLibString.getString(indexPt)), eleIn_
                .getLineNumber());
            analyze_.addInfo(analPref + EfIOResource.getS("L'indice doit appartenir � l'intervalle") + " [1,"
                + indexMax + "]", eleIn_.getLineNumber());
            return;
          }
          if (indexPt > 0) {
            connect.add(indexPt - 1);
            idx++;
          } else break;
        }
        if (connect.size() < 3) {
          analyze_.addFatalError(DodicoLib.getS("L'�l�ment {0} est mal d�fini", CtuluLibString.getString(i + 1)));
          return;
        }
        ele[i] = new EfElement(connect.toNativeArray());
        // maj de l'avancement
        up.majAvancement();
      }
    } catch (final EOFException _e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Reading end");
      }
    } catch (final NumberFormatException fe) {
      analyze_.manageException(fe, eleIn_.getLineNumber());
      return;
    }
    // verif si le dernier element est bien defini.
    if (ele != null && ele[ele.length - 1] == null) {
      analyze_.addFatalError(analPref + EfIOResource.getS("Des �l�ments ne sont pas d�finis"), eleIn_.getLineNumber());
      return;
    }
    _maillage.setElts(ele);
  }

  /**
   * Renvoie true si le 4eme caractere est est 1 est le 3eme " " le format est de longueur simple.
   */
  private static boolean isSimpleFormat(final String _l) {
    if (((_l.charAt(4) == '1') && (_l.charAt(3) == ' '))) { return true; }
    return false;
  }

  /**
   * Lecture du fichier contenant les coordonnees. Le fichier cor est un fichier dans les champs ont des largeurs fixes.
   */
  private EfNode[] readCor(final int _percBegin, final int _percLength) throws IOException {
    EfNode[] pts = null;
    final int doubleFormat = version_.getDoubleColumLength();
    try {
      // Les formats des colonnes de ce fichier peut varier.
      // Il faut tester la deuxieme ligne pour savoir si les champs sont de longueur double ou
      // simple.
      // On marque le debut du buffer pour revenir en suite.
      // Au max, la premiere ligne contient 6 champs de taille double.
      // la deuxieme ligne contient 3 champs de taille double.
      // pour etre sur, on prendra une marque pour 10 champs.
      corIn_.mark(10 * doubleFormat);
      // Lecture de la premiere ligne
      corIn_.readLine();
      int format = doubleFormat;
      // Lecture de la deuxieme ligne et test
      if (isSimpleFormat(corIn_.readLine())) {
        format = version_.getSimpleColumLength();
      }
      // La premiere ligne contient le nombre de noeuds a lire.
      // <nombre de noeuds> (Les champs suivants importent peu)
      corIn_.reset();
      corIn_.readFields();
      // lecture du nombre de noeuds enfin...
      final int nbPts = corIn_.intField(0);
      pts = new EfNode[nbPts];
      int num;
      // Gestion de l'avancement
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(10, nbPts, _percBegin, _percLength);
      up.majProgessionStateOnly();
      final int[] fmt = new int[] { format, doubleFormat, doubleFormat };
      for (int i = 0; i < nbPts; i++) {
        // Coordonn�es <x>,<y>
        corIn_.readFields(fmt);
        num = corIn_.intField(0);
        // Si le numero du point ne correspond pas, on arrete la lecture
        // et on renvoie un message d'erreur.
        if (num != i + 1) {
          analyze_.addError(EfIOResource.getS("L'index du noeud {0} ne correspond pas", CtuluLibString.getString(num)),
              corIn_.getLineNumber());
          return null;
        }
        pts[i] = new EfNode(corIn_.doubleField(1), corIn_.doubleField(2), 0);
        // maj de l'avancement
        up.majAvancement();
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Reading end");
      }
    } catch (final NumberFormatException fe) {
      analyze_.manageException(fe, corIn_.getLineNumber());
      return null;
    }
    // la lecture est terminee....
    return pts;
  }

  /**
   * Lecture du fichier contenant la bathymetrie. Il y a <code>BTH_PT_BY_LINE</code> cotes par ligne.
   * 
   * @param _pts les points a modifier.
   */
  private void readBth(final int _percBegin, final int _percLength, final EfGridMutable _maillage) throws IOException {
    // L'index du point a modifier.
    int index = 0;
    // le nombre de point a modifier.
    final int l = _maillage.getPtsNb();
    try {
      final int pointParLigne = version_.getBthPointByLine();
      // initialisation du format.
      // Le nombre de point par ligne est stocke dans CorEleBthInterface.BTH_PT_BY_LINE
      final int[] fmt = new int[pointParLigne];
      // Gestion de l'avancement
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(l, 4, _percBegin, _percLength);
      up.majProgessionStateOnly();
      Arrays.fill(fmt, version_.getBthColumnLength());
      final int tailleDeLigne = fmt[0] * fmt.length;
      // Tant que l'index du point a modifier est inferieur au nb total.
      while (index < l) {

        // Lecture de la ligne
        bthIn_.readFields(fmt);
        if (index == 0 && bthIn_.getLine().length() != tailleDeLigne) {
          final int taille = bthIn_.getLine().length();
          if (taille % fmt.length == 0) {
            final int tailleCol = taille / fmt.length;
            analyze_.addWarn("BTH " + EfIOResource.getS("Nouveau format:") + CtuluLibString.ESPACE + tailleCol, bthIn_
                .getLineNumber());
            Arrays.fill(fmt, tailleCol);
            bthIn_.analyzeCurrentLine(fmt);

          }

        }
        // pour chaque cote, on initialise le point correspondant.
        for (int i = 0; i < pointParLigne; i++) {
          _maillage.setZIntern(index++, bthIn_.doubleField(i));
          // Si la limite est atteinte ...
          if (index >= l) {
            break;
          }
        }
        // maj de l'avancement
        up.majAvancement();
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Reading end");
      }
    } catch (final NumberFormatException fe) {
      analyze_.manageException(fe, bthIn_.getLineNumber());
      return;
    }
    // verifie si toutes les cotes sont bien initialisees.
    if (index < l) {
      // Ajout d'un avertissement.
      analyze_.addWarn(EfIOResource.getS(
          "La bathym�trie des derniers points n'a pas �t� initialis�e (nb pt initialis�s {0})", CtuluLibString
              .getString(index)), bthIn_.getLineNumber());
    }
  }

  /**
   * @return le format
   */
  public FileFormat getFileFormat() {
    return getCorEleBthFileFormat();
  }

  /**
   * @return le format
   */
  public CorEleBthFileFormat getCorEleBthFileFormat() {
    return (CorEleBthFileFormat) version_.getFileFormat();
  }

  @Override
  protected Object internalRead() {
    return readCorEleBth();
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (corIn_ != null) {
          corIn_.close();
          corIn_ = null;
        }
        if (eleIn_ != null) {
          eleIn_.close();
          eleIn_ = null;
        }
        if (bthIn_ != null) {
          bthIn_.close();
          bthIn_ = null;
        }
      }
    };
  }

  /**
   * @return la version
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }
}