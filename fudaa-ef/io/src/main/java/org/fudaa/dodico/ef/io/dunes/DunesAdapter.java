/*
 *  @creation     20 f�vr. 2003
 *  @modification $Date: 2007-01-19 13:07:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.dunes;

import org.fudaa.ctulu.fileformat.FileFormat;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;

/**
 * @author deniger
 * @version $Id: DunesAdapter.java,v 1.14 2007-01-19 13:07:22 deniger Exp $
 */
public class DunesAdapter extends EfGridSourcesAbstract implements DunesInterface {
  EfGridInterface maillage_;
  double[] adaptatif_;

  @Override
  public EfGridInterface getGrid() {
    return maillage_;
  }

  @Override
  public double[] getAdaptatifValeur() {
    return adaptatif_;
  }

  /**
   * Sets the adaptatif.
   *
   * @param _adaptatif The adaptatif to set
   */
  public void setAdaptatif(final double[] _adaptatif) {
    adaptatif_ = _adaptatif;
  }

  /**
   * Sets the maillage.
   *
   * @param _maillage The maillage to set
   */
  public void setMaillage(final EfGridInterface _maillage) {
    maillage_ = _maillage;
  }

  public FileFormat getFileFormat() {
    return DunesMAIFileFormat.getInstance();
  }

}
