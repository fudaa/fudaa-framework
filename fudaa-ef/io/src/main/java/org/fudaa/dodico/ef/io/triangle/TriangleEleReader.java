/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;

/**
 * Voir https://www.cs.cmu.edu/~quake/triangle.ele.html
 *
 * @author Frederic Deniger
 */
public class TriangleEleReader extends FileCharSimpleReaderAbstract<TriangleEleData> {

  @Override
  protected TriangleEleData internalRead() {
    in_.setJumpBlankLine(true);
    in_.setJumpCommentLine(true);
    in_.setCommentInOneField("#");
    TriangleEleData res = null;
    try {
      in_.readFields();
      // First line: <# of triangles> <nodes per triangle> <# of attributes> 
      if (in_.getNumberOfFields() != 3) {
        analyze_.addSevereError(EfIOResource.getS("Le format du fichier ele est incorrect:la premi�re ligne comporte {0} champs au lieu de 3", Integer.toString(in_.getNumberOfFields())), in_);
        return null;
      }
      int nbElt = in_.intField(0);
      ProgressionUpdater updater = new ProgressionUpdater(progress_);
      updater.setValue(20, nbElt);
      int nbNodePerElt = in_.intField(1);
      //la colonne 1 doit contenir 2 et n'est pas int�ressantes ici...
      int nbAttributes = in_.intField(2);
      res = new TriangleEleData(nbElt, nbNodePerElt, nbAttributes);
      //Remaining lines: <triangle #> <node> <node> <node> ... [attributes] 
      for (int idxElt = 0; idxElt < nbElt; idxElt++) {
        in_.readFields();
        final int numberOfFields = in_.getNumberOfFields();
        int nbExpected = 1 + nbNodePerElt + nbAttributes;
        if (numberOfFields != nbExpected) {
          analyze_.addSevereError(EfIOResource.getS("Le format du fichier ele est incorrect"), in_);
          return null;
        }
        for (int idxPt = 0; idxPt < nbNodePerElt; idxPt++) {
          res.setEltIdx(idxElt, idxPt, in_.intField(1 + idxPt) - 1);//les indices commencent � 0.
        }
        if (nbAttributes > 0) {
          for (int idxAttr = 0; idxAttr < nbAttributes; idxAttr++) {
            res.setAttributes(idxElt, idxAttr, in_.doubleField(1 + nbNodePerElt + idxAttr));
          }
        }
        updater.majAvancement();
      }
    } catch (IOException ex) {
      Logger.getLogger(TriangleEleReader.class.getName()).log(Level.SEVERE, null, ex);
    }
    return res;
  }
}
