/**
 * @creation 2002-11-20
 * @modification $Date: 2006-11-14 09:05:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import java.io.IOException;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;

/**
 * interface � implanter pour ecrire un fichier serafin. Les methodes sont calquees sur les donnees du fichier.
 *
 * @version $Id: SerafinInterface.java,v 1.16 2006-11-14 09:05:28 deniger Exp $
 * @author Fred Deniger
 */
public interface SerafinInterface extends EfGridSource {

  /**
   * @return les infos de la lecture utilises uniquement apres la lecture
   */
  SerafinNewReaderInfo getReadingInfo();

  /**
   * Correspond a getNbv1().
   */
  @Override
  int getValueNb();

  /**
   * @param _i [1...NBV1]
   */
  @Override
  String getValueId(int _i);

  /**
   * @param _i [1...NBV1]
   * @return l'unite de la variable
   */
  String getUnite(int _i);

  /**
   * @return le titre
   */
  String getTitre();

  /**
   * @return tableau des parametres
   */
  int[] getIparam();

  /**
   * @return l'option de discretisation
   */
  int getIdisc1();

  /**
   * @return tableau ipobo
   */
  int[] getIpoboInitial();

  /**
   * @return les indices frontieres uniquement
   */
  int[] getIpoboFr();

  /**
   * @return le temps en millisecondes
   */
  long getIdate();

  @Override
  EfGridInterface getGrid();

  /**
   * @param _pasTemps [1...getNbPasTemps]
   * @param _numVariable [1...NBV1]
   * @param i [1...NPOIN1]
   * @throws IOException
   */
  @Override
  double getValue(int _numVariable, int _pasTemps, int _ptIdx) throws IOException;

  @Override
  int getTimeStepNb();

  @Override
  double getTimeStep(int _i);

  /**
   * @return true si les r�sultats sont d�finies aux �l�ments
   */
  boolean isVolumique();

  /**
   *
   * @return true si les valeurs x,y sont en double pr�cisison
   */
  boolean isXYdoublePrecision();
}