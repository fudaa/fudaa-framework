package org.fudaa.dodico.ef.io.mesh;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.fudaa.dodico.ef.EfElement;


import gnu.trove.TObjectIntHashMap;

public class MeshFormatHelper
{
    public static final String VERSION_HEADER = "MeshVersionFormatted";
    public static final int VERSION_VALUE = 1;
    public static final String DIMENSION_HEADER = "Dimension";
    public static final int DIMENSION_VALUE = 3;
    public static final String NODE_HEADER = "Vertices";
    public static final int NODE_REFERENCE = 0;
    public static final String TRIANGLES_HEADER = "Triangles";
    public static final String QUADRILATERALS_HEADER = "Quadrilaterals";
    public static final String HEXAHEDRA_HEADER = "Hexahedra";
    public static final String TETRAHEDRA_HEADER = "Tetrahedra";
    public static final String PENTAHEDRA_HEADER = "Pentahedra";
    public static final String TRIANGLESP2_HEADER = "TrianglesP2";
    public static final String TETRAHEDRAP2_HEADER = "TetrahedraP2";
    public static final int ELEMENT_REFERENCE = 1;
    public static final String END_HEADER = "End";
    
    public static final List<String> ELEMENTS_HEADER = Collections.unmodifiableList(
            Arrays.asList(new String[]{MeshFormatHelper.TRIANGLES_HEADER,
                                       MeshFormatHelper.QUADRILATERALS_HEADER,
                                       MeshFormatHelper.HEXAHEDRA_HEADER,
                                       MeshFormatHelper.TETRAHEDRA_HEADER,
                                       MeshFormatHelper.PENTAHEDRA_HEADER,
                                       MeshFormatHelper.TRIANGLESP2_HEADER,
                                       MeshFormatHelper.TETRAHEDRAP2_HEADER}));
    private static TObjectIntHashMap<String> elementKindNbPoint = new TObjectIntHashMap<String>();;

    static
    {
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.TRIANGLES_HEADER, 3);
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.QUADRILATERALS_HEADER, 4);
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.HEXAHEDRA_HEADER, 8);
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.TETRAHEDRA_HEADER, 4);
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.PENTAHEDRA_HEADER, 6);
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.TRIANGLESP2_HEADER, 6);
        MeshFormatHelper.elementKindNbPoint.put(MeshFormatHelper.TETRAHEDRAP2_HEADER, 10);
    }

    public static boolean isHeader(final String header)
    {
        return (MeshFormatHelper.isNodeHeader(header) || MeshFormatHelper.isElementsHeader(header) || MeshFormatHelper.isEndHeader(header));
    }
    
    public static boolean isNodeHeader(final String header)
    {
        return (header.equals(MeshFormatHelper.NODE_HEADER));
    }
    
    public static boolean isEndHeader(final String header)
    {
        return (header.equals(MeshFormatHelper.END_HEADER));
    }
    
    public static boolean isElementsHeader(final String header)
    {
        for (int i = 0; i < MeshFormatHelper.ELEMENTS_HEADER.size(); i++)
        {
            if (header.equals(MeshFormatHelper.ELEMENTS_HEADER.get(i)))
            {
                return true;
            }
        }
        
        return false;
    }
    
    public static int getNbPointForElementKind(String kind)
    {
        if (MeshFormatHelper.elementKindNbPoint.containsKey(kind))
        {
            return MeshFormatHelper.elementKindNbPoint.get(kind);
        }
        
        return -1;
    }
    
    public static String getElementKind(EfElement element)
    {
        for (int i = 0; i < MeshFormatHelper.ELEMENTS_HEADER.size(); i++)
        {
            if (MeshFormatHelper.getNbPointForElementKind(MeshFormatHelper.ELEMENTS_HEADER.get(i)) == element.getPtNb())
            {
                return MeshFormatHelper.ELEMENTS_HEADER.get(i);
            }
        }
        
        return "";
    }
}
