package org.fudaa.dodico.ef.io.adcirc;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridMutable;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

public class AdcircReader extends FileOpReadCharSimpleAbstract
{

    @Override
    protected Object internalRead()
    {
        return this.readGrid();
    }

    private EfGridSource readGrid()
    {
        //TODO Voir si les initialisations sont correctes.
        in_.setJumpBlankLine(true);
        in_.setCommentInOneField("!");
        in_.setJumpCommentLine(true);

        EfNode[]    nodes    = null;
        EfElement[] elements = null;

        try
        {
            // Lecture du titre non utilis�.
            in_.readFields();

            // Lecture du nombre d'�l�ments et de points.
            in_.readFields();
            
            int nbElts = in_.intField(0);
            int nbPts  = in_.intField(1);
            
            nodes    = new EfNode[nbPts];
            elements = new EfElement[nbElts];
            
            // Lecture des points
            for (int i = 0; i < nbPts; i++)
            {
                in_.readFields();
                
                double x = in_.doubleField(1);
                double y = in_.doubleField(2);
                double z = in_.doubleField(3);
                
                nodes[i] = new EfNode(x, y, z);
            }
            
            // Lecture des �l�ments
            for (int i = 0; i < nbElts; i++)
            {
                in_.readFields();
                
                int[] pts = new int[3];
                
                for (int j = 0; j < pts.length; j++)
                {
                    pts[j] = (in_.intField(j + 2) - 1);
                }
                
                elements[i] = new EfElement(pts);
            }
        }
        catch (final EOFException eOFException)
        {
            analyze_.manageException(eOFException);
        }
        catch (final NumberFormatException nbFmtException)
        {
            analyze_.manageException(nbFmtException, in_.getLineNumber());
        }
        catch (IOException iOException)
        {
            analyze_.manageException(iOException);
        }
        
        if ((nodes.length > 0) && (elements.length > 0))
        {
            final EfGridMutable grid = new EfGridMutable(nodes, elements);

            grid.setTypeElt(EfElementType.T3);
//            EfLib.orienteGrid(grid, progress_, true, analyze_);

            return new EfGridSourceDefaut(grid, AdcircFileFormat.getInstance());
        }
        
        return null;
    }
}
