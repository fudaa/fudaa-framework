/*
 *  @creation     2003-02-20
 *  @modification $Date: 2006-10-16 07:55:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.dunes;

import java.io.IOException;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;

/**
 * @author deniger
 * @version $Id: DunesWriter.java,v 1.27 2006-10-16 07:55:47 deniger Exp $
 */
public class DunesMAIWriter extends FileOpWriterCharSimpleAbstract {

  DunesVersionInterface version_;

  /**
   * Construit avec une version par defaut.
   */
  public DunesMAIWriter() {
    this(DunesMAIFileFormat.getInstance());
  }

  /**
   * @param _f la version a utiliser
   */
  public DunesMAIWriter(final DunesVersionInterface _f) {
    version_ = _f;
  }

  /**
   * @return le format utilise
   */
  public FileFormat getFileFormat() {
    return version_.getFileFormat();
  }

  /**
   * Ecrire les donnees de l'interface <code>DunesInterface</code> sur la sortie <code>Writer</code>. L'ecriture
   * n'est effectu�e que si les points et elements du maillage sont non nuls. La m�thode se termine par un flush mais ne
   * ferme pas le flux.
   */
  protected void writeDunes(final DunesInterface _d) {
    final DunesInterface dunes = _d;
    // Test sur les objets a ecrire
    if (dunes == null) {
      analyze_.addFatalError(EfIOResource.getS("Les donn�es sont nulles"));
      return;
    }
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    final EfGridInterface maillage = dunes.getGrid();
    if (maillage == null) {
      analyze_.addFatalError("Le maillage est nul");
      return;
    }
    final int nPts = maillage.getPtsNb();
    if (nPts == 0) {
      analyze_.addFatalError(EfIOResource.getS("Le maillage ne contient pas de points"));
      return;
    }
    // H2dElement[] elems= maillage.getElts();
    // if (elems == null)
    // {
    // analyze_.addFatalError("Le maillage ne contient pas d'�l�ments !");
    // return;
    // }
    final int nElems = maillage.getEltNb();
    if (nElems == 0) {
      analyze_.addFatalError(EfIOResource.getS("Le maillage ne contient pas d'�l�ments"));
      return;
    }
    try {
      StringBuffer b = new StringBuffer(600);
      b.append(version_.getNoeudCode());
      b.append(' ');
      b.append(lineSep_);
      b.append(nPts);
      boolean printAdaptatifV = false;
      final double[] adValeur = dunes.getAdaptatifValeur();
      b.append(' ');
      if (adValeur == null) {
        b.append(version_.getNonAdaptatifCode());
      } else {
        b.append(version_.getAdaptatifCode());
        printAdaptatifV = true;
      }
      writelnToOut(b.toString());
      // Gestion de l'etat d'avancement
      // le pas pour afficher l'etat d'avancement
      final int pourc = 0;
      final boolean afficheAvance = (progress_ == null ? false : true);
      if (afficheAvance) {
        progress_.setProgression(pourc);
        progress_.setDesc(EfIOResource.getS("Ecriture fichier") + " dunes");
      }
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(2, nPts);

      for (int i = 0; i < nPts; i++) {
        b = new StringBuffer();
        b.append((i + 1));
        b.append(' ');
        b.append(maillage.getPtX(i));
        b.append(' ');
        b.append(maillage.getPtY(i));
        b.append(' ');
        b.append(maillage.getPtZ(i));
        b.append(lineSep_);
        if (printAdaptatifV&&adValeur!=null) {
          b.append(adValeur[i]);
          b.append(lineSep_);
        }
        writeToOut(b.toString());
        up.majAvancement();
      }
      b = new StringBuffer();
      // Cette ligne blanche est-elle utile ?
      b.append(lineSep_);
      // L'identifiant element
      b.append(version_.getElementCode());
      b.append(lineSep_);
      // le nb d'�l�ments
      b.append(nElems);
      writelnToOut(b.toString());
      // pour afficher l'etat d'avancement
      up.setValue(2, nElems, 50, 50);
      up.majAvancement();
      // les points par elements
      EfElement ppel;
      // le nombre de points par elements.
      int nppel;
      for (int i = 0; i < nElems; i++) {
        b = new StringBuffer();
        ppel = maillage.getElement(i);
        nppel = ppel.getPtNb();
        // le num d'elements
        b.append((i + 1));
        b.append(' ');
        // le nombre de point pour l'�l�ment i
        b.append(nppel);
        // les indices des points pour l'element i
        for (int j = 0; j < nppel; j++) {
          b.append(' ');
          // Les indices sont stock�s a partir de 0.
          b.append(ppel.getPtIndex(j) + 1);
        }
        writelnToOut(b.toString());
        // mis a jour de l'etat d'avancement au cas ou.
        up.majAvancement();
      }
      out_.flush();
    } catch (final IOException _ex) {
      analyze_.manageException(_ex);
    }
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof DunesInterface) {
      writeDunes((DunesInterface) _o);
    } else {
      analyze_.addFatalError(DodicoLib.getS("Donn�es invalides"));
    }
  }

  /**
   * @param _o la source a ecrire
   * @return la synthese de l'operation
   */
  public final CtuluIOOperationSynthese write(final DunesInterface _o) {
    writeDunes(_o);
    return closeOperation(_o);
  }

  /**
   * @return la version utilisee
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }
}
