/*
 *  @creation     7 juin 2005
 *  @modification $Date: 2007-04-16 16:34:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import java.io.IOException;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISPoint;

/**
 * @author Fred Deniger
 * @version $Id: SerafinGisAdapter.java,v 1.9 2007-04-16 16:34:22 deniger Exp $
 */
public class SerafinGisAdapter implements GISDataModel {

  final GISAttributeInterface[] att_;
  final SerafinInterface serafin_;
  final int timeStep_;
  GISPoint[] pt_;
  
  private double xOffset;
  private double yOffset;

  public SerafinGisAdapter(final SerafinInterface _serafin, final GISAttributeInterface[] _attributes) {
    att_ = _attributes;
    serafin_ = _serafin;
    timeStep_ = _serafin.getTimeStepNb() - 1;
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    SerafinGisAdapter res=new SerafinGisAdapter(serafin_, att_);
    res.xOffset=xyToAdd.getX();
    res.yOffset=xyToAdd.getY();
    return res;
  }
  
  

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {}



  public SerafinGisAdapter(final SerafinInterface _serafin, final GISAttributeInterface[] _attributes,
      final int _timeStep) {
    super();
    att_ = _attributes;
    serafin_ = _serafin;
    timeStep_ = _timeStep;
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt){
    return att_[_idxAtt];
  }




  @Override
  public Envelope getEnvelopeInternal() {
    return serafin_.getGrid().getEnvelope(null);
  }


  @Override
  public double getDoubleValue(final int _idxAtt,final int _idxGeom){

    try {
      return serafin_.getValue(_idxAtt, timeStep_, _idxGeom);
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
    return 0;
  }

  @Override
  public Geometry getGeometry(final int _idxGeom){
    if (pt_ == null) {
      pt_ = new GISPoint[serafin_.getGrid().getPtsNb()];
    }
    if(pt_[_idxGeom]==null){
        pt_[_idxGeom] = new GISPoint(serafin_.getGrid().getPtX(_idxGeom)+xOffset, serafin_.getGrid().getPtY(_idxGeom)+yOffset, 0);
    }
    return pt_[_idxGeom];
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att){
    return CtuluLibArray.findObject(att_, _att);
  }

  @Override
  public int getNbAttributes(){
    return att_.length;
  }

  @Override
  public int getNumGeometries(){
    return serafin_.getGrid().getPtsNb();
  }

  @Override
  public Object getValue(final int _idxAtt,final int _idxGeom){
    return CtuluLib.getDouble(getDoubleValue(_idxAtt, _idxGeom));
  }
}
