/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * Voir la documentation https://www.cs.cmu.edu/~quake/triangle.node.html
 *
 * @author Frederic Deniger
 */
public interface TriangleNodeDataInterface extends SupportLocationI {

  /**
   *
   * @return le nombre d'attribut attendu
   */
  int getNbAttributes();

  /**
   *
   * @param idxPt entre 0 et getNbPoint-1
   * @param idxAtt entre 0 et getNbAttributes-1
   * @return la valeur de l'attribut.
   */
  double getAttributes(int idxPt, int idxAtt);

  boolean containsBoundaryMarkers();

  /**
   * Voir https://www.cs.cmu.edu/~quake/triangle.markers.html
   *
   * @param idxPt entre 0 et getNbPoint-1
   * @return
   */
  int getMarker(int idxPt);
}
