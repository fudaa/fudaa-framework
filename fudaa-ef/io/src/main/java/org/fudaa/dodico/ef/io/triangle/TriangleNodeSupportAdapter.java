/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * Adapte une collection en un fournisseur de points.
 *
 * @author Frederic Deniger
 */
public class TriangleNodeSupportAdapter implements TriangleNodeDataInterface {

  private final SupportLocationI support;

  public TriangleNodeSupportAdapter(SupportLocationI support) {
    this.support = support;
  }

  @Override
  public int getPtsNb() {
    return support.getPtsNb();
  }

  @Override
  public double getPtX(int _i) {
    return support.getPtX(_i);
  }

  @Override
  public double getPtY(int _i) {
    return support.getPtY(_i);
  }

  @Override
  public int getNbAttributes() {
    return 0;
  }

  @Override
  public double getAttributes(int idxPt, int idxAtt) {
    return 0;
  }

  @Override
  public boolean containsBoundaryMarkers() {
    return false;
  }

  @Override
  public int getMarker(int idxPt) {
    return 0;
  }
}
