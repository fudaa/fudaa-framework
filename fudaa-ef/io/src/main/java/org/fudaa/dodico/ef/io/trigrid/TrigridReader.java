/**
 * @creation 27 janv. 2004
 * @modification $Date: 2007-01-19 13:07:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.trigrid;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridMutable;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.ef.resource.EfResource;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * Un lecteur de fichier trigrid ngh. Ce lecteur se base sur les sources du projet telemac. Le maillage lu, n'est pas
 * forc�ment supervalide : il faut v�rifier par exemple que les elements soient bien orientes,etc,...
 * 
 * @author deniger
 * @version $Id: TrigridReader.java,v 1.15 2007-01-19 13:07:23 deniger Exp $
 */
public class TrigridReader extends FileOpReadCharSimpleAbstract {

  // private TrigridFileFormat.TrigridVersion version_;

  /**
   */
  public TrigridReader() {}

  /**
   *
   */
  @Override
  protected Object internalRead() {
    return readGrid();
  }

  public static ConditionLimiteEnum getFromTrigridCode(int idx) {
    return ConditionLimiteEnum.SOLID;

  }

  private EfGridSource readGrid() {
    in_.setJumpBlankLine(true);
    in_.setCommentInOneField("!");
    in_.setJumpCommentLine(true);
    EfNode[] pts = null;
    EfElement[] elt = null;
    ConditionLimiteEnum[] boundaryCond = null;
    try {
      // Premiere ligne donne le nombre de points
      in_.readFields();
      final int nbPoint = in_.intField(0);
      if(nbPoint<0){
        analyze_.addFatalError(EfResource.getS("Fichier non compatible pour le format {0}","Trigrid"));
        return null;
      }
      pts = new EfNode[nbPoint];
      boundaryCond = new ConditionLimiteEnum[nbPoint];
      // 2 eme ligne : le nombre max de connexion par points
      in_.readFields();
      final int nbMaxVoisin = in_.intField(0);
      // 3 eme ligne : xmax,ymax,xmin,xmin
      // ignore pour l'instant
      in_.readFields();
      // Ensuite il y nbPoint ligne de la forme
      // Num_du_point X Y CODE Z V1 V2 ....V(nbMaxVoisin)
      // m stocke pour chaque points, les voisins
      final int[][] m = new int[nbPoint][nbMaxVoisin];
      for (int i = 0; i < nbPoint; i++) {
        in_.readFields();
        pts[i] = new EfNode(in_.doubleField(1), in_.doubleField(2), in_.doubleField(4));
        boundaryCond[i] = ConditionLimiteHelper.getConditionsLimite(this.transformTrigridCodeToUniversalCode(in_
            .intField(3)));
        for (int j = 0; j < nbMaxVoisin; j++) {
          m[i][j] = in_.intField(5 + j) - 1;
        }
      }
      // Nous allons retrouver les triangles, en suivant le fichier
      // fortran ngh2tri.f
      // NTR=0
      // DO N=1,NP1
      final List eltList = new ArrayList(nbPoint);
      for (int i = 0; i < nbPoint; i++) {
        // DO J=1,NF1
        for (int j = 0; j < nbMaxVoisin; j++) {
          // IF (M1(N,J).gt.N) THEN
          if (m[i][j] > i) {
            // DO K=J+1,NF1
            for (int k = j; k < nbMaxVoisin; k++) {
              // IF (M1(N,K).gt.N) THEN
              // marq2=.false.
              // marq1=.false.
              if (m[i][k] > i) {
                boolean marq1 = false;
                boolean marq2 = false;
                // DO i=1,NF1
                // IF (N.eq.M1(M1(N,J),i)) marq1=.true.
                // IF (M1(N,K).eq.M1(M1(N,J),i)) marq2=.true.
                // ENDDO
                for (int id = 0; id < nbMaxVoisin; id++) {
                  if (i == m[m[i][j]][id]) {
                    marq1 = true;
                  }
                  if (m[i][k] == m[m[i][j]][id]) {
                    marq2 = true;
                  }
                }
                // marqa=(marq1.and.marq2)
                // marq2=.false.
                // marq1=.false.
                final boolean marqa = marq1 && marq2;
                marq1 = false;
                marq2 = false;
                // DO i=1,NF1
                // IF (N.eq.M1(M1(N,K),i)) marq1=.true.
                // IF (M1(N,J).eq.M1(M1(N,K),i)) marq2=.true.
                // ENDDO
                for (int id = 0; id < nbMaxVoisin; id++) {
                  if (i == m[m[i][k]][id]) {
                    marq1 = true;
                  }
                  if (m[i][j] == m[m[i][k]][id]) {
                    marq2 = true;
                  }
                }
                final boolean marqb = marq1 && marq2;
                // IF (marqa.and.marqb) THEN
                // NTR=NTR+1
                // IKLE(NTR,1)=N
                // IKLE(NTR,2)=M1(N,J)
                // IKLE(NTR,3)=M1(N,K)
                if (marqa && marqb) {
                  eltList.add(new EfElement(new int[] { i, m[i][j], m[i][k] }));
                }
              }
            }
          }
        }
      }
      elt = new EfElement[eltList.size()];
      eltList.toArray(elt);

    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("NGH Read end");
      }
    } catch (final NumberFormatException fe) {
      analyze_.manageException(fe, in_.getLineNumber());
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    if (pts != null) {
      final EfGridMutable r = new EfGridMutable(pts, elt);
      r.setTypeElt(EfElementType.T3);
      EfLib.orienteGrid(r, progress_, true, analyze_);
      return new EfGridSourceDefaut(r, TrigridFileFormat.getInstance(), boundaryCond);
    }

    return null;

  }

  private int transformTrigridCodeToUniversalCode(int code) {
    switch (code) {
    // par d�faut renverra solid : le code 0 ou -1.
    case 1: {
      return 11;
    }
    case 2: {
      return 4;
    }
    case 3: {
      return 5;
    }
    case 4: {
      return 7;
    }
    case 5: {
      return 8;
    }
    case 6: {
      return 9;
    }
    case 7: {
      return 1;
    }
    case 8: {
      return 12;
    }
    case 9: {
      return 15;
    }
    case 10: {
      return 2;
    }
    case 11: {
      return 3;
    }
    case 12: {
      return 14;
    }
    case 13: {
      return 13;
    }
    }

    return -1;
  }
}