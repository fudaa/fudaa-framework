package org.fudaa.dodico.ef.io.mesh;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

public class MeshFileFormat extends FileFormatUnique implements FileFormatGridVersion
{
    private static final MeshFileFormat INSTANCE = new MeshFileFormat();

    /**
     * @return singleton
     */
    public static MeshFileFormat getInstance() {
      return INSTANCE;
    }

    protected MeshFileFormat() {
      super(1);
      extensions_ = new String[] { "mesh" };
      id_ = "MESH";
      nom_ = "Mesh (mesh)";
      
      //TODO Voir si correct.
      description_ = CtuluLibString.EMPTY_STRING;
      software_ = FileFormatSoftware.TELEMAC_IS;
    }

  @Override
    public boolean canReadGrid()
    {
        return true;
    }

  @Override
    public boolean canWriteGrid()
    {
        return true;
    }

  @Override
    public boolean hasBoundaryConditons()
    {
        return false;
    }

  @Override
    public CtuluIOOperationSynthese readGrid(File f, ProgressionInterface prog)
    {
        return read(f, prog);
    }

  @Override
    public CtuluIOOperationSynthese writeGrid(File f, EfGridSource m, ProgressionInterface prog)
    {
        return write(f, m, prog);
    }

  @Override
    public FileReadOperationAbstract createReader()
    {
        return new MeshReader();
    }

  @Override
    public FileWriteOperationAbstract createWriter()
    {
        return new MeshWriter();
    }
}
