package org.fudaa.dodico.ef.io.serafin;

import java.io.File;

import com.memoire.bu.BuFileFilter;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.dico.DicoLanguage;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * Gestion des fichiers serafin pour les volumes
 * @author Adrien Hadoux
 *
 */
public class SerafinVolumeFileFormat  extends FileFormatUnique implements SerafinFileFormatVersionInterface,FileFormatGridVersion {
	  
	private static final SerafinVolumeFileFormat INSTANCE = new SerafinVolumeFileFormat();

	  /**
	   * Nombre de param dans le tableau IParam.
	   */
	  public static final int IPARAM_NB = 10;

	  /**
	   * @return le nom de la variable utilisee pour designer le fond
	   */
	  public static String[] getCommonVariableFond() {
	    return new String[] { "FOND", "BOTTOM", "COTE_DU_FOND", "BOTTOM_LEVEL", "altimetrie" };
	  }

	  public static boolean is3DGrid(final EfGridInterface _grid) {
	    return _grid != null && (EfElementType.T3_FOR_3D == _grid.getEltType() || EfElementType.T6 == _grid.getEltType());
	  }

	  public static int getNbPlanIn3D(final int[] _iparams) {
	    if (_iparams != null && _iparams.length == IPARAM_NB) { return _iparams[6]; }
	    return 1;
	  }

	  /**
	   * @return BOTTOM
	   */
	  public static String getEnVariableFond() {
	    return "BOTTOM";
	  }

	  /**
	   * @return FOND
	   */
	  public static String getFrVariableFond() {
	    return "FOND";
	  }

	  /**
	   * @return le singleton
	   */
	  public static SerafinVolumeFileFormat getInstance() {
	    return INSTANCE;
	  }

	  /**
	   * @return le nom de la variable a utiliser pour la bathymetrie. dans le langage utilise (fr ou en)
	   */
	  public static String getStaticDefaultVariableFond() {
	    return SerafinFileFormat.getVariableFond(DicoLanguage.getCurrentID());
	  }

	  /**
	   * @param _dicoLanguageIdx l'identifiant du langage
	   * @return le nom de la variable a utiliser pour la bathymetrie.
	   * @see DicoLanguage#getCurrentID()
	   */
	  public static String getVariableFond(final int _dicoLanguageIdx) {
	    if (_dicoLanguageIdx == DicoLanguage.ENGLISH_ID) { return getEnVariableFond(); }
	    return getFrVariableFond();
	  }

	  /**
	   * Renvoie true si les donnees, pour chaque pas de temps, sont formattees en colonnees ( forme non compacte ou
	   * "unpackad form"). Teste en fait si IPARAM[1]==1.
	   * 
	   * @param _s le tableau iparam utilise pour savoir si colonne ou non.
	   * @return FormatEnColonne
	   */
	  public static boolean isFormatEnColonneCommon(final int[] _s) {
	    return _s[0] == 1 ? true : false;
	  }

	  /**
	   * A partir de <code>_s</code>, determine si la date (Idate) du projet est presente dans le fichier. Si la valeur du
	   * 10 element vaut 1, la date est definie.
	   * 
	   * @param _s talbeau iparam a tester
	   * @return true si Idate definie dans le projet.
	   */
	  public static boolean isIdateDefiniCommon(final int[] _s) {
	    if (_s[9] == 1) { return true; }
	    return false;
	  }

	  protected SerafinVolumeFileFormat() {
	    super(1);
	    extensions_ = new String[] { "vol", "volafin", "volfin", "vol3d", "vol2d", "volu" };
	    id_ = "SERAFIN VOLUME";
	    nom_ = "Volumes Serafin";
	    description_ = EfIOResource
	        .getS("Fichier binaire comportant le mod�le num�rique de terrain et des propri�t�s volumiques");
	    software_ = FileFormatSoftware.TELEMAC_IS;
	  }

	  /**
	   * @return true
	   */
	  public boolean canWriteOnlyGrid() {
	    return true;
	  }

	  public boolean containsGrid() {
	    return true;
	  }

  @Override
	  public BuFileFilter createFileFilter() {
	    return new SerafinVolumeFileFilter(this);
	  }

	  public BuFileFilter createFileFilterStrict() {
	    return new SerafinVolumeFileFilter(this, true);
	  }

	  /**
	   * @return le reader.
	   */
  @Override
	  public FileReadOperationAbstract createReader() {
	    return createSerafinReader();
	  }

	  /**
	   * @return le reader
	   */
	  public SerafinNewReader createSerafinReader() {
	    return new SerafinNewReader(true);
	  }

	  /**
	   * @return le writer
	   */
	  public SerafinWriter createSerafinWriter() {
	    return new SerafinWriter(this);
	  }

	  /**
	   * Lit le dernier pas de temps uniquement.
	   * 
	   * @param _file le fichier a lire
	   * @param _prog la progression
	   * @return synthese de l'operation
	   */
	  public CtuluIOOperationSynthese readLast(final File _file, final ProgressionInterface _prog) {
	    final SerafinNewReader r = createSerafinReader();
	    r.setOnlyReadLast(true);
	    return r.read(_file, _prog);
	  }

	  /**
	   * @return le writer.
	   */
  @Override
	  public FileWriteOperationAbstract createWriter() {
	    return createSerafinWriter();
	  }

  @Override
	  public String getDefaultVariableFond() {
	    return getStaticDefaultVariableFond();
	  }

  @Override
	  public int getParamNb() {
	    return IPARAM_NB;
	  }

	  /**
	   * @return le nom de la variable utilisee pour designer le fond
	   */
  @Override
	  public String[] getVariableFond() {
	    return getCommonVariableFond();
	  }

  @Override
	  public String getVersionName() {
	    return CtuluLibString.UN;
	  }

	  /**
	   * Renvoie true si les donnees, pour chaque pas de temps, sont formattees en colonnees ( forme non compacte ou
	   * "unpackad form"). Teste en fait si IPARAM[1]==1.
	   * 
	   * @param _s le tableau iparam utilise pour savoir si colonne ou non.
	   * @return FormatEnColonne
	   */
  @Override
	  public boolean isFormatEnColonne(final int[] _s) {
	    return _s[0] == 1 ? true : false;
	  }

	  /**
	   * A partir de <code>_s</code>, determine si la date (Idate) du projet est presente dans le fichier. Si la valeur du
	   * 10 element vaut 1, la date est definie.
	   * 
	   * @param _s talbeau iparam a tester
	   * @return true si Idate definie dans le projet.
	   */
  @Override
	  public boolean isIdateDefini(final int[] _s) {
	    if (_s[9] == 1) { return true; }
	    return false;
	  }

  @Override
	  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
	    final SerafinNewReader i = createSerafinReader();
	    i.setOnlyReadLast(true);
	    final CtuluIOOperationSynthese r = i.read(_f, _prog);
	    i.setProgressReceiver(null);
	    final SerafinAdapter adapter = (SerafinAdapter) r.getSource();
	    if (adapter == null) { return r; }
	    adapter.majGridFond();

	    return r;
	  }

	  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
	    final CtuluIOOperationSynthese s = read(_f, _prog);
	    s.setSource(((SerafinInterface) s.getSource()).getGrid());
	    return s;
	  }

	  /**
	   * @param _s tableau a modifier afin de prendre en compte la date.
	   */
  @Override
	  public void setDateDefini(final int[] _s) {
	    _s[9] = 1;
	  }

	  /**
	   * @param _s le tableau iparam a modifier pour le format colonne
	   */
  @Override
	  public void setFormatColonne(final int[] _s) {
	    _s[0] = 1;
	  }

	  /**
	   * @param _s tableau a modifier afin de ne pas prendre en compte la date.
	   */
  @Override
	  public void unsetDateDefini(final int[] _s) {
	    _s[9] = 0;
	  }

	  /**
	   * @param _s le tableau iparam a modifier pour ne pas etre dans le format colonne
	   */
  @Override
	  public void unsetFormatColonne(final int[] _s) {
	    _s[0] = 0;
	  }

	  /**
	   * @param _f le fichier a ecrire.
	   * @param _source la source pour ecrire.
	   * @param _prog la barre de progression
	   * @return la synthese de l'operation
	   */
	  public CtuluIOOperationSynthese write(final File _f, final SerafinInterface _source, final ProgressionInterface _prog) {
	    final SerafinWriter w = new SerafinWriter(this);
	    w.setFile(_f);
	    w.setProgressReceiver(_prog);
	    return w.write(_source);
	  }

	  /**
	   * @param _f le fichier a ecrire
	   * @param _m le maillage a ecrire
	   * @param _prog la barre de progression
	   * @return la synthese de l'operation
	   */
	  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridInterface _m, final ProgressionInterface _prog) {
	    return write(_f, new SerafinMaillageAdapter(this, _m), _prog);
	  }

  @Override
	  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
	    return write(_f, new SerafinGridSourceAdapter(_m, this, getDefaultVariableFond()), _prog);
	  }

	  /**
	   * Ecrit dans le format binaire adapte au systeme OS.
	   * 
	   * @param _f le fichier a ecrire.
	   * @param _source la source pour ecrire
	   * @param _prog la barre de progression
	   * @return la synthese de l'ecriture.
	   */
  @Override
	  public CtuluIOOperationSynthese writeSparc(final File _f, final SerafinInterface _source,
	      final ProgressionInterface _prog) {
	    final SerafinWriter w = new SerafinWriter(this);
	    w.setMachineSPARC();
	    w.setFile(_f);
	    w.setProgressReceiver(_prog);
	    return w.write(_source);
	  }

  @Override
    public boolean canReadGrid()
    {
        return true;
    }

  @Override
    public boolean canWriteGrid()
    {
        return true;
    }

  @Override
    public boolean hasBoundaryConditons()
    {
        // TODO Voir si correct
        return false;
    }

}
