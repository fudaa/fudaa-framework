/*
 * @creation 2002-11-20
 * 
 * @modification $Date: 2007-05-04 13:47:27 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.ef.io.serafin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fichiers.NativeBinarySystem;
import org.fudaa.dodico.fortran.FortranBinaryOutputStream;

/**
 * TODO a modifier pour prendre en compte l'aspect volumique. Classe permettant d'ecrire un fichier serafin (utilise un buffer pour l'ecriture).
 *
 * @version $Id: SerafinWriter.java,v 1.34 2007-05-04 13:47:27 deniger Exp $
 * @author Fred Deniger
 */
public class SerafinWriter extends FileWriteOperationAbstract implements CtuluActivity {

  private String machineID_;
  private long tailleTotale_;
  private SerafinFileFormatVersionInterface version_;
  OutputStream out_;
  boolean stop_;

  /**
   * Par default le format sparc est utilisee.
   *
   * @param _f la version utilisee
   */
  public SerafinWriter(final SerafinFileFormatVersionInterface _f) {
    version_ = _f;
    setMachineSPARC();
    // machineID_ = NativeBinaryInputStream.getLocalMachineId();
  }

  /**
   * Teste si le double n'appartient pas a la plage des float.
   *
   * @param _d
   * @return true si le double n'appartient pas a la plage des float
   */
  private boolean isDoubleToFloatError(final double _d) {
    final double d = _d > 0 ? _d : -_d;
    if (d > Float.MAX_VALUE) {
      return true;
    }
    if ((d > 0) && (d < Float.MIN_VALUE)) {
      return true;
    }
    return false;
  }

  private boolean isDoubleToDoubleError(final double _d) {
    final double d = _d > 0 ? _d : -_d;
    if (d > Double.MAX_VALUE) {
      return true;
    }
    if ((d > 0) && (d < Double.MIN_VALUE)) {
      return true;
    }
    return false;
  }

  public void setOut(final OutputStream _out) {
    out_ = _out;
    if (analyze_ == null) {
      analyze_ = new CtuluAnalyze();
    }
  }
  

  private void setProgression(final long _etat) {
    if (progress_ != null) {
      progress_.setProgression((int) ((100D * _etat) / tailleTotale_));
    }
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {
      @Override
      public void close() throws IOException {
        if (out_ != null) {
          out_.close();
        }
      }
    };
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof SerafinInterface) {
      writeSerafin((SerafinInterface) _o);
    } else {
      donneesInvalides(_o);
    }
  }

  /**
   * @return le format du lecteur
   */
  public FileFormat getFileFormat() {
    return version_.getFileFormat();
  }

  /**
   * @return la version utilisee
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileOperationAbstract#setFile(java.io.File)
   */
  @Override
  public void setFile(final File _f) {
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f.getAbsolutePath());
    FileOutputStream out = null;
    try {
      out = new FileOutputStream(_f);
    } catch (final FileNotFoundException _e) {
      FuLog.error(_e);
    }
    if (out != null) {
      setOut(out);
    }
  }

  /**
   * @param _f le fichier a ecrire
   */
  public void setFile(final File[] _f) {
    setFile(_f[0]);
  }

  /**
   * @param _id l'id de la machine
   */
  public void setMachineID(final String _id) {
    machineID_ = NativeBinarySystem.getMachineId(_id);
  }

  /**
   * Initialise le type de machine pour sparc.
   */
  public void setMachineSPARC() {
    machineID_ = NativeBinarySystem.SPARC;
  }

  /**
   * Initialise le type de machine pour X 86.
   */
  public void setMachineX86() {
    machineID_ = NativeBinarySystem.X86;
  }

  @Override
  public void stop() {
    stop_ = true;
  }

  /**
   * @param _o l'interface a ecrire
   * @return la synthese de l'ecriture
   */
  public final CtuluIOOperationSynthese write(final SerafinInterface _o) {
    writeSerafin(_o);
    return closeOperation(_o);
  }

  /**
   * Ecriture des donnees ( pas de buffer).
   *
   * @param _inter l'interface a ecrire
   */
  public void writeSerafin(final SerafinInterface _inter) {
    final SerafinInterface donnees = _inter;
    try {
      if (out_ == null) {
        analyze_.addFatalError(EfIOResource.getS("Le flux de sortie est nul"));
        return;
      }
      if (donnees == null) {
        analyze_.addFatalError(EfIOResource.getS("Les donn�es sont nulles"));
        return;
      }
      final FortranBinaryOutputStream out = new FortranBinaryOutputStream(out_, true, machineID_);
      // on verifie toutes les donn�es.
      // si elles sont valides on ecrit
      final int nbv1 = donnees.getValueNb();
      if (nbv1 > 100) {
        analyze_.addError(EfIOResource.getS("Le nombre de variables est sup�rieur � 100"), 0);
      }
      if (nbv1 < 0) {
        analyze_.addFatalError(EfIOResource.getS("Le nombre de variables est nul ou n�gatif"));
      }
      if (donnees.getGrid().getEltType() == null) {
        analyze_.addFatalError(EfIOResource.getS("Les �l�ments doivent �tre du m�me type"));
      }
      final int nbPasTemps = donnees.getTimeStepNb();
      if (nbPasTemps <= 0) {
        analyze_.addFatalError(EfIOResource.getS("Le nombre de pas de temps est n�gatif ou nul"), 0);
      }
      final int[] iparam = donnees.getIparam();
      if ((iparam == null) || (iparam.length != version_.getParamNb())) {
        analyze_.addFatalError(EfIOResource.getS("Le tableau des param�tres est invalide"), 0);
      }
      if (!analyze_.containsFatalError()) {
        writeAll(donnees, out);
      }

    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  protected double getCorrectDoubleValue(final double _init) {
    double min = 1E-15;
    return (_init > -min && _init < min) ? 0 : _init;
  }

  private void writeAll(final SerafinInterface _donnees, final FortranBinaryOutputStream _out) throws IOException {
    final EfGridInterface maillage = _donnees.getGrid();
    final int nbv1 = _donnees.getValueNb();
    final int nelem = maillage.getEltNb();
    final int npt = maillage.getPtsNb();
    final int nbValues = _donnees.isVolumique() ? nelem : npt;
    final int nppel = maillage.getElement(0).getPtNb();
    if (stop_) {
      return;
    }
    boolean xydoublePrecision = _donnees.isXYdoublePrecision();
    String titre = _donnees.getTitre();
    if (xydoublePrecision) {
      titre = CtuluLibString.adjustSize(79, titre) + "D";
      //si ce n'est pas un double pr�cision mais se termine par D on l'enleve
    } else if (titre.length() == 80 && titre.charAt(79) == 'D') {
      titre = titre.substring(0, 79).trim();
    }
    final int[] iparam = _donnees.getIparam();
    final boolean isIdateDefini = version_.isIdateDefini(iparam);
    final boolean isFormatColonne = version_.isFormatEnColonne(iparam);
    final int nbPasTemps = _donnees.getTimeStepNb();
    long tailleDebut = (8L + 80L) + (8L + 8L) + ((8L + 32L) * nbv1) + (8L + 40L) + (8L + 16L);
    if (isIdateDefini) {
      tailleDebut += 8L + 6L * 4L;
    }
    final long tailleIkle1 = 8L + 4L * nelem * nppel;
    long tailleXY = xydoublePrecision ? 8L : 4L;
    final long tailleIpobo = 8L + npt * 4L;
    final long tailleCoord = 8L + npt * tailleXY;
    long tailleTemps = 12L + 4L * nbv1 * nbValues;
    if (xydoublePrecision) {
      tailleTemps = 16 + 8 * nbv1 * nbValues;
    }
    if (isFormatColonne) {
      tailleTemps = 12L + 4L * nbv1 * nbValues + 8L * nbv1;
      if (xydoublePrecision) {
        tailleTemps = 16L + 8L * nbv1 * nbValues + 8L * nbv1;
      }
    }
    tailleTotale_ = tailleDebut + tailleIkle1 + tailleIpobo + tailleCoord * 2L + nbPasTemps * tailleTemps;
    // TITRE
    // tS chaine temporaire
    final String tS = CtuluLibString.adjustSize(80, titre);
    _out.writeCharacter(tS);
    _out.writeRecord();
    // NBV1
    _out.writeInteger(nbv1);
    // NBV2
    _out.writeInteger(0);
    _out.writeRecord();
    // TEXTi LUNITi
    writeVariables(_donnees, _out, nbv1);
    if (stop_) {
      return;
    }
    // IPARAM
    // tIA tableau d'entier tempo
    writeIparam(_out, iparam);
    if (isIdateDefini) {
      writeDate(_donnees, _out);
    }
    // NELEM1,NPOIN1,NPPEL1,IDISC1
    _out.writeInteger(nelem);
    _out.writeInteger(npt);
    _out.writeInteger(nppel);
    // DEBUG: IDISC A voir pour l'instant on met 1.
    _out.writeInteger(1);
    _out.writeRecord();
    long lu = tailleDebut;
    setProgression(lu);
    // IKLE1
    // H2dElement[] elems= maillage.getElts();
    EfElement eltIndex;
    for (int i = 0; i < nelem; i++) {
      if (stop_) {
        return;
      }
      eltIndex = maillage.getElement(i);
      for (int j = 0; j < nppel; j++) {
        _out.writeInteger(eltIndex.getPtIndex(j) + 1);
      }
    }
    _out.writeRecord();
    lu = updateProg(lu + tailleIkle1);
    writeIpobo(_donnees, _out, npt);
    if (stop_) {
      return;
    }
    lu = updateProg(lu + tailleIpobo);
    // H2dPoint[] pts = maillage.getPts();
    // X1
    writeNodesX(_out, maillage, npt, xydoublePrecision);
    if (stop_) {
      return;
    }
    lu = updateProg(lu + tailleCoord);
    // Y1
    writeNodesY(_out, maillage, npt, xydoublePrecision);
    if (stop_) {
      return;
    }
    lu = updateProg(lu + tailleCoord);
    // double[] tDA = donnees_.getPasDeTemps();
    for (int t = 0; t < nbPasTemps; t++) {

      if (xydoublePrecision) {
        _out.writeDoublePrecision(_donnees.getTimeStep(t));
      } else {
        double tD = getCorrectDoubleValue(_donnees.getTimeStep(t));
        if (isDoubleToFloatError(tD)) {
          analyze_.addError(EfIOResource.getS("Erreur de conversion lors de l'�criture des pas de temps"), t);
          analyze_.addInfo(" temps " + t + " = " + tD, t);
        }
        _out.writeReal((float) tD);
      }
      for (int i = 0; i < nbv1; i++) {
        if (stop_) {
          break;
        }
        if (isFormatColonne) {
          _out.writeRecord();
        }
        for (int j = 0; j < nbValues; j++) {
          if (xydoublePrecision) {
            _out.writeDoublePrecision(_donnees.getValue(i, t, j));
          } else {
            double tD = getCorrectDoubleValue(_donnees.getValue(i, t, j));
            if (isDoubleToFloatError(tD)) {
              analyze_.addError("Erreur de conversion lors de l'�criture des Donn�es", t);
            }
            _out.writeReal((float) tD);
          }
        }
      }
      _out.writeRecord();
      lu = updateProg(lu + tailleTemps);
    }
    out_.flush();
  }

  private long updateProg(final long _newVal) {
    setProgression(_newVal);
    return _newVal;
  }

  private void writeNodesY(final FortranBinaryOutputStream _out, final EfGridInterface _maillage, final int _npt, boolean xydoublePrecision) throws IOException {
    for (int i = 0; i < _npt; i++) {
      if (stop_) {
        return;
      }
      if (xydoublePrecision) {
        _out.writeDoublePrecision(_maillage.getPtY(i));
      } else {
        final double tD = getCorrectDoubleValue(_maillage.getPtY(i));
        if (isDoubleToFloatError(tD)) {
          analyze_.addError(EfIOResource.getS("Erreur de conversion lors de l'�criture des Y"), i);
          analyze_.addInfo("  Point " + i + " = " + tD, i);
        }
        float tD2 = (float) tD;
        _out.writeReal(tD2);
      }
    }
    _out.writeRecord();
  }

  private void writeNodesX(final FortranBinaryOutputStream _out, final EfGridInterface _maillage, final int _npt, boolean xydoublePrecision) throws IOException {
    for (int i = 0; i < _npt; i++) {
      if (stop_) {
        return;
      }
      if (xydoublePrecision) {
        _out.writeDoublePrecision(_maillage.getPtX(i));
      } else {
        final double tD = getCorrectDoubleValue(_maillage.getPtX(i));
        if (isDoubleToFloatError(tD)) {
          analyze_.addError(EfIOResource.getS("Erreur de conversion lors de l'�criture des X"), i);
          analyze_.addInfo("  Point " + i + " = " + tD, i);
        }
        float tD2 = (float) tD;
        _out.writeReal(tD2);
      }
    }
    _out.writeRecord();
  }

  private void writeIpobo(final SerafinInterface _inter, final FortranBinaryOutputStream _out, final int _npt) throws IOException {
    _inter.getGrid().computeBord(this.progress_, this.analyze_);
    // la tableau contenant les indice dans l'ordre
    final int[] ipobo = _inter.getIpoboInitial();
    if (ipobo == null) {
      return;
    }
    // IPOBO
    for (int i = 0; i < _npt; i++) {
      if (stop_) {
        return;
      }
      _out.writeInteger(ipobo[i]);
    }
    _out.writeRecord();
  }

  private void writeDate(final SerafinInterface _donnees, final FortranBinaryOutputStream _out) throws IOException {
    final Calendar c = Calendar.getInstance();
    c.setTime(new Date(_donnees.getIdate()));
    _out.writeInteger(c.get(Calendar.YEAR));
    _out.writeInteger(c.get(Calendar.MONTH));
    _out.writeInteger(c.get(Calendar.DAY_OF_MONTH));
    _out.writeInteger(c.get(Calendar.HOUR_OF_DAY));
    _out.writeInteger(c.get(Calendar.MINUTE));
    _out.writeInteger(c.get(Calendar.SECOND));
    _out.writeRecord();
  }

  private void writeIparam(final FortranBinaryOutputStream _out, final int[] _iparam) throws IOException {
    final int tI = _iparam.length;
    for (int i = 0; i < tI; i++) {
      _out.writeInteger(_iparam[i]);
    }
    _out.writeRecord();
  }

  private void writeVariables(final SerafinInterface _donnees, final FortranBinaryOutputStream _out, final int _nbv1) throws IOException {
    String tS;
    for (int i = 0; i < _nbv1; i++) {
      tS = CtuluLibString.adjustSize(16, _donnees.getValueId(i));
      _out.writeCharacter(tS);
      tS = CtuluLibString.adjustSize(16, _donnees.getUnite(i));
      _out.writeCharacter(tS);
      _out.writeRecord();
    }
  }
}