/**
 *  @creation     10 mars 2003
 *  @modification $Date: 2007-01-19 13:07:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.corelebth;

import org.fudaa.ctulu.fileformat.FileFormat;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;

/**
 * @author deniger
 * @version $Id: CorEleBthAdapter.java,v 1.13 2007-01-19 13:07:21 deniger Exp $
 */
public class CorEleBthAdapter extends EfGridSourcesAbstract implements CorEleBthInterface {
  private EfGridInterface maillage_;

  public CorEleBthAdapter() {
    super();
  }

  @Override
  public EfGridInterface getGrid() {
    return maillage_;
  }

  /**
   * @param _s le maillage a utiliser
   */
  public void setMaillage(final EfGridInterface _s) {
    maillage_ = _s;
  }

  public FileFormat getFileFormat() {
    return CorEleBthFileFormat.getInstance();
  }

}
