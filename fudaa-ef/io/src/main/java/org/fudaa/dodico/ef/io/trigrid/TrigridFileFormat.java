/**
 * @creation 13 mars 2003
 * @modification $Date: 2006-11-15 09:22:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.trigrid;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: TrigridFileFormat.java,v 1.14 2006-11-15 09:22:54 deniger Exp $
 */
public final class TrigridFileFormat extends FileFormatUnique implements FileFormatGridVersion {

  private static final TrigridFileFormat INSTANCE = new TrigridFileFormat();

  /**
   * @return singleton
   */
  public static TrigridFileFormat getInstance() {
    return INSTANCE;
  }

  protected TrigridFileFormat() {
    super(1);
    extensions_ = new String[] { "ngh" };
    id_ = "TRIGRID";
    nom_ = "Trigrid (ngh)";
    description_ = CtuluLibString.EMPTY_STRING;
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  public boolean canWriteOnlyGrid() {
    return true;
  }

  public boolean containsGrid() {
    return true;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return createTrigridReader();
  }

  public TrigridReader createTrigridReader() {
    return new TrigridReader();
  }

  public TrigridWriter createTrigridWriter() {
    return new TrigridWriter();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return createTrigridWriter();
  }

  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    return read(_f, _prog);
  }

  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese s = readGrid(_f, _prog);
    s.setSource(((EfGridSource) s.getSource()).getGrid());
    return s;
  }

  public CtuluIOOperationSynthese write(final File _f, final EfGridInterface _inter, final ProgressionInterface _prog) {
    final TrigridWriter w = createTrigridWriter();
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridInterface _m, final ProgressionInterface _prog) {
    return write(_f, _m, _prog);
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return write(_f, _m.getGrid(), _prog);
  }

  @Override
  public boolean canReadGrid() {
    return true;
  }

  @Override
  public boolean canWriteGrid() {
    return true;
  }

  @Override
  public boolean hasBoundaryConditons() {
    return true;
  }
}
