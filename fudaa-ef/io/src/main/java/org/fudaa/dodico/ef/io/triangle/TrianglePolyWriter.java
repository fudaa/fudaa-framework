/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.locationtech.jts.geom.LineString;
import org.fudaa.dodico.ef.triangulation.TriangulationPolyDataInterface;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.ef.triangulation.TriangulationUniqueDataContent;
import org.fudaa.dodico.ef.triangulation.TriangulationUniqueDataContentBuilder;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;

/**
 * Permet d'�crire les fichiers de type line: https://www.cs.cmu.edu/~quake/triangle.line.html
 *
 * @author Frederic Deniger
 */
public class TrianglePolyWriter extends FileOpWriterCharSimpleAbstract {

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof TriangulationPolyDataInterface) {
      try {
        writePoly((TriangulationPolyDataInterface) _o);
      } catch (IOException ex) {
        Logger.getLogger(TrianglePolyWriter.class.getName()).log(Level.SEVERE, null, ex);
        analyze_.manageException(ex);
      }
    } else {
      analyze_.addFatalError(DodicoLib.getS("Donn�es invalides"));
    }
  }
  private double eps = 1e-3;

  public void setEps(double eps) {
    this.eps = eps;
  }

  private void writePoly(TriangulationPolyDataInterface data) throws IOException {
    if (data == null) {
      analyze_.addFatalError(EfIOResource.getS("Les donn�es sont nulles"));
      return;
    }
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    TriangulationUniqueDataContentBuilder builder = new TriangulationUniqueDataContentBuilder(data, eps);
    TriangulationUniqueDataContent content = builder.build(progress_);

    int nbPoly = data.getNbLines();
    int nbPointsTotal = content.getNbPoints();
    //premiere ligne
//    First line: <# of vertices> <dimension (must be 2)> <# of attributes> <# of boundary markers (0 or 1)> 
    writeInt(nbPointsTotal);
    writeSpace();
    out_.write("2");
    writeSpace();
    writeInt(0);
    writeSpace();
    out_.write("1");
    writelnToOut();
    ProgressionUpdater updater = new ProgressionUpdater(progress_);
    updater.setValue(10, nbPointsTotal);
    updater.majProgessionStateOnly(EfIOResource.getS("Ecriture des points"));

    //ensuite 
    //<vertex #> <x> <y> [attributes] [boundary marker] 
    for (int i = 0; i < nbPointsTotal; i++) {
      writeInt(i + 1);
      writeSpace();
      writeDouble(content.getX(i));
      writeSpace();
      writeDouble(content.getY(i));
      writeSpace();
      //le boundary marker des points int�rieurs est a 0
      writeInt(content.getLinearRingIdx(i) + 1);
      writelnToOut();
      updater.majAvancement();
    }
    int nbSegment = 0;
    for (int idxPoly = 0; idxPoly < nbPoly; idxPoly++) {
      LineString line = data.getLine(idxPoly);
      //si ligne ferm�e: premier point = dernier point donc -1
      //si ligne ouverte -1 aussi.
      int numPoints = line.getNumPoints() - 1;
      nbSegment = nbSegment + numPoints;
    }
    //One line: <# of segments> <# of boundary markers (0 or 1)> 
    //calcul du nombre de segment.
    writeInt(nbSegment);
    writeSpace();
    writeString(CtuluLibString.UN);
    writelnToOut();
    updater.setValue(10, nbSegment);
    updater.majProgessionStateOnly(EfIOResource.getS("Ecriture des segments"));
    //Following lines: <segment #> <endpoint> <endpoint> [boundary marker] 
    int globalIdxSeg = 1;
    for (int idxLine = 0; idxLine < nbPoly; idxLine++) {
      LineString poly = data.getLine(idxLine);
      final boolean closed = data.isClosed(idxLine);
      int numPoints = closed ? poly.getNumPoints() - 2 : poly.getNumPoints() - 1;//le dernier segment est �crit apr�s
      for (int idxPt = 0; idxPt < numPoints; idxPt++) {
        writeInt(globalIdxSeg++);
        writeSpace();
        writeInt(content.getPtIdxFor(idxLine, idxPt) + 1);
        writeSpace();
        writeInt(content.getPtIdxFor(idxLine, idxPt + 1) + 1);
        writeSpace();
        //boundary marker
        writeInt(idxLine + 1);
        writelnToOut();
        updater.majAvancement();
      }
      if (closed) {
        //le segment fermant
        writeInt(globalIdxSeg++);
        writeSpace();
        writeInt(content.getPtIdxFor(idxLine, numPoints) + 1);
        writeSpace();
        writeInt(content.getPtIdxFor(idxLine, 0) + 1);
        writeSpace();
        //boundary marker
        writeInt(idxLine + 1);
        writelnToOut();
      }
      updater.majAvancement();
    }

    updater.majProgessionStateOnly(EfIOResource.getS("Ecriture des trous"));
    //les trous
    //One line: <# of holes> 
    //on calcul le nombre de trous en stockant un point int�rieur du trou
    List<Point> holes = new ArrayList<Point>();
    for (int idxPoly = 0; idxPoly < nbPoly; idxPoly++) {
      if (data.isClosedAndHole(idxPoly)) {
        LinearRing poly = (LinearRing) data.getLine(idxPoly);
        holes.add(poly.getCentroid());
      }
    }
    //One line: <# of holes> 
    writeInt(holes.size());
    updater.setValue(10, holes.size());
    writelnToOut();
    //<hole #> <x> <y> 
    for (int idxHole = 0; idxHole < holes.size(); idxHole++) {
      Point pt = holes.get(idxHole);
      writeInt(idxHole + 1);
      writeSpace();
      writeDouble(pt.getX());
      writeSpace();
      writeDouble(pt.getY());
      writelnToOut();
      updater.majAvancement();
    }






  }
}
