/*
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.dunes;

import java.io.IOException;
import java.util.ArrayList;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelPointToMultiPointAdapter;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Une classe pour ecrire des fichiers Dunes de mod�lisation (.geo)
 * @version $Id: SinusxWriter.java,v 1.24 2007/05/04 13:47:27 deniger Exp $
 * @author Bertrand Marchand
 */
public class DunesGEOWriter extends FileOpWriterCharSimpleAbstract implements CtuluActivity {

  final FileFormatVersionInterface v_;
  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
  }

  /**
   * @param _v la version utilisee.
   */
  public DunesGEOWriter(final FileFormatVersionInterface _v) {
    v_ = _v;
  }

  /**
   * Ecriture des g�om�tries contenues dans les mod�les. Les g�om�tries peuvent �tre 
   * des points, des polylignes/polygones ou des multipoints. Chaque modele est cens� contenir
   * le m�me type de g�om�tries.<p>
   * 
   * Les doublons de coordonn�es (si X et Y sont equivalents) sont supprim�s.
   * @param _geometries Les mod�les de g�om�tries
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (!(_o instanceof GISDataModel[])) {
      donneesInvalides(_o);
      return;
    }
      
    GISDataModel[] mdls=(GISDataModel[])_o;
   

    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    if (progress_ != null) {
      progress_.setProgression(0);
    }
    up.majProgessionStateOnly();
    
    Coordinate[] coords=null;
    
    // Les coordonn�es, d'abord la totalit�, puis sans les doublons.
    ArrayList<Coordinate> vcoords=new ArrayList<Coordinate>();
    int nblig=0;
    
    // Recuperation des coordonn�es : d'abord les lignes...
    GISDataModel[] mdlligs=filtreModels(mdls,1);
    if (mdlligs.length==0) {
      analyze_.addFatalError(EfIOResource.getS("Aucune ligne trouv�e"));
      return;
    }
    
    for (int i=0; i<mdlligs.length; i++) {
      GISDataModel mdl=mdlligs[i];
      int nbgeo=mdl.getNumGeometries();
      for (int j=0; j<nbgeo; j++) {
        LineString g=(LineString)mdl.getGeometry(j);
        int nbpt=g.getNumPoints();
        nblig+=nbpt-1;
        CoordinateSequence seq=g.getCoordinateSequence();
        for (int k=0; k<nbpt; k++) {
          vcoords.add(seq.getCoordinate(k));
        }
      }
    }
    if (stop_) return;
    up.setValue(1,1,10,10);
    up.majProgessionStateOnly();

    // ...Ensuite les points
    GISDataModel[] mdlpts=filtreModels(mdls,0);
    
    for (int i=0; i<mdlpts.length; i++) {
      GISDataModel mdl=mdlpts[i];
      int nbgeo=mdl.getNumGeometries();
      for (int j=0; j<nbgeo; j++) {
        GISMultiPoint g=(GISMultiPoint)mdl.getGeometry(j);
        int nbpt=g.getNumPoints();
        CoordinateSequence seq=g.getCoordinateSequence();
        for (int k=0; k<nbpt; k++) {
          vcoords.add(seq.getCoordinate(k));
        }
      }
    }
    if (stop_) return;

    coords=vcoords.toArray(new Coordinate[0]);
    // Les indexs de coordonn�es pour les lignes.
    int[] idxs=new int[coords.length];
    for (int i=0; i<idxs.length; i++) idxs[i]=i;
    
    // Suppression des doublons (pas optimal, mais fonctionnel).
    // Il serait plus efficace d'utiliser un tri suivant X pour supprimer les doublons.
    // La difficult� une fois le tri fait est de retrouver l'indice des points pour les lignes.
    up.setValue(100,coords.length,10,70);
    int nbsup=0;
    for (int i=0; i<coords.length; i++) {
      up.majAvancement();
      if (stop_) return;

      if (coords[i]==null) {
        nbsup++;
        continue;
      }
      // Reindexation au fur et a mesure des suppressions.
      idxs[i]-=nbsup;
      
      for (int j=i+1; j<coords.length; j++) {
        if (coords[j]==null) continue;
        if (coords[i].distance(coords[j])<1.e-6) {
          idxs[j]=idxs[i];
          coords[j]=null;
        }
      }
    }

    int nbtrous=0;
    int nbreg=0;
    up.setValue(20, coords.length-nbsup+nblig+nbtrous+nbreg, 80, 20);

    // Ecriture des infos. Repris de DParametresDunes.
    final FortranWriter fout = new FortranWriter(out_);
    try {
      int[] fmt;
      
      /** Ecriture des coordonnees des points * */
      fmt = new int[] { 8, 8 };
      fout.intField(1, 1);
      fout.intField(0, coords.length-nbsup);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 24, 24, 24 };
      for (int i = 0; i < coords.length; i++) {
        if (stop_) return;
        if (coords[i]==null) continue;
        fout.doubleField(3, coords[i].z);
        fout.intField(0, idxs[i]+1);
        fout.doubleField(1, coords[i].x);
        fout.doubleField(2, coords[i].y);
        fout.writeFields(fmt);
        up.majAvancement();
      }
      
      /** Ecriture des polylignes * */
      fmt = new int[] { 8, 8 };
      fout.intField(1, 0);
      fout.intField(0, nblig);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 8, 8 };
      int ilig=0;
      for (int i=0; i<mdlligs.length; i++) {
        if (stop_) return;
        GISDataModel mdl=mdlligs[i];
        int nbgeo=mdl.getNumGeometries();
        for (int j=0; j<nbgeo; j++) {
          LineString g=(LineString)mdl.getGeometry(j);
          int nbpt=g.getNumPoints();
          for (int k=1; k<nbpt; k++) {
            int index1=idxs[ilig]+1;
            int index2=idxs[ilig+1]+1;
            fout.intField(2, index2);
            fout.intField(0, ilig+1);
            fout.intField(1, index1);
            fout.writeFields(fmt);
            ilig++;
            up.majAvancement();
          }
        }
      }
      
      /** Ecriture des positions des trous * */
      fmt = new int[] { 8 };
      fout.intField(0, nbtrous);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 24, 24 };
      for (int i = 0; i < nbtrous; i++) {
        if (stop_) return;
        double x=0;
        double y=0;
        fout.intField(0, i + 1);
        fout.doubleField(1, x);
        fout.doubleField(2, y);
        fout.writeFields(fmt);
        up.majAvancement();
      }
      
      /** Ecriture des positions des regions * */
      fmt = new int[] { 8 };
      fout.intField(0, nbreg);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 24, 24, 24, 24 };
      for (int i = 0; i < nbreg; i++) {
        if (stop_) return;
        double x=0;
        double y=0;
        double z=0;
        final double aireMax = 0;
        fout.intField(0, i + 1);
        fout.doubleField(1, x);
        fout.doubleField(2, y);
        fout.doubleField(3, z);
        fout.doubleField(4, aireMax);
        fout.writeFields(fmt);
        up.majAvancement();
      }
    } catch (final IOException ex) {
      analyze_.manageException(ex);
    }
  }
  
  /**
   * R�cup�re les modeles d'un type particulier.
   * @param _mdls Les models
   * @param _type 0 : Points et multipoints. 1 : Lignes, polygones.
   * @return Les modeles, une fois filtr�s.
   */
  GISDataModel[] filtreModels(GISDataModel[] _mdls, int _type) {
    ArrayList<GISDataModel> mdls=new ArrayList<GISDataModel>();
    for (int i=0; i<_mdls.length; i++) {
      if (_mdls[i].getNumGeometries()==0) continue;
      
      Geometry g=_mdls[i].getGeometry(0);
      if (_type==0 && g instanceof GISMultiPoint)
        mdls.add(_mdls[i]);
      else if (_type==0 && g instanceof Point)
        mdls.add(new GISDataModelPointToMultiPointAdapter(_mdls[i]));
      else if (_type==1 && g instanceof LineString)
        mdls.add(_mdls[i]);
    }
    
    return mdls.toArray(new GISDataModel[0]);
  }
}
