/**
 * @creation 13 mars 2003
 * @modification $Date: 2007-01-19 13:07:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.corelebth;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: CorEleBthFileFormat.java,v 1.29 2007-01-19 13:07:21 deniger Exp $
 */
public class CorEleBthFileFormat extends FileFormatUnique implements FileFormatGridVersion{


  /**
   * LE nombre de fichier necessaire pour ce format.
   */
  public final static int NB_FILE = 3;
  private static final CorEleBthFileFormat INSTANCE = new CorEleBthFileFormat();

  /**
   * @return le singleton
   */
  public static final CorEleBthFileFormat getInstance() {
    return INSTANCE;
  }

  @Override
  public String[] getLinkedExtension() {
    return extensions_;
  }

  protected CorEleBthFileFormat() {
    super(NB_FILE);
    extensions_ = new String[] { "cor", "ele", "bth" };
    id_ = "CORELEBTH";
    nom_ = "Cor, ele, bth";
    description_ = EfIOResource.getS("Contient des donn�es concernant le mod�le num�rique de terrain uniquement."
        + " Compatible avec le format MEF/Mosaic v2.6");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  /**
   * @return reader
   */
  public CorEleBthReader createCorEleBthReader() {
    return new CorEleBthReader(this);
  }

  /**
   * @return writer
   */
  public CorEleBthWriter createCorEleBthWriter() {
    return new CorEleBthWriter(this);
  }

  /**
   * @param _f le fichier cible
   * @param _inter la source a utiliser pour l'ecriture
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _f, final CorEleBthInterface _inter, final ProgressionInterface _prog) {
    final CorEleBthWriter w = createCorEleBthWriter();
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  /**
   * @param _cor Fichier cible cor
   * @param _ele Fichier cible ele
   * @param _bth Fichier cible bth
   * @param _inter la source pour l'ecriture
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _cor, final File _ele, final File _bth,
      final CorEleBthInterface _inter, final ProgressionInterface _prog) {
    final CorEleBthWriter w = createCorEleBthWriter();
    w.setFile(_cor, _ele, _bth);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  /**
   * @return Largeur simple.
   */
  public int getSimpleColumLength() {
    return 5;
  }

  /**
   * @return Largeur double.
   */
  public int getDoubleColumLength() {
    return 10;
  }

  /**
   * @return Largeur de la 2eme colonne du fichier ele.
   */
  public int getEleSecondColumnLength() {
    return 25;
  }

  /**
   * @return Largeur d'une colonne du fichier bth.
   */
  public int getBthColumnLength() {
    return 12;
  }

  /**
   * @return Nombre de points par ligne dans le fichier bth.
   */
  public int getBthPointByLine() {
    return 6;
  }

  /**
   *
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return createCorEleBthReader();
  }

  /**
   *
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return createCorEleBthWriter();
  }

  /**
   * @param _f le fichier cible
   * @param _m source
   * @param _prog barre de progression
   * @return synthese de l'operation
   */
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridInterface _m, final ProgressionInterface _prog) {
    final CorEleBthAdapter inter = new CorEleBthAdapter();
    inter.setMaillage(_m);
    return write(_f, inter, _prog);
  }

  public CtuluIOOperationSynthese writeGridSimpleColumn(final File _f, final EfGridInterface _m,
      final ProgressionInterface _prog) {
    final CorEleBthAdapter inter = new CorEleBthAdapter();
    inter.setMaillage(_m);
    final CorEleBthWriter w = createCorEleBthWriter();
    w.setFile(_f);
    w.setSimpleColumn(true);
    w.setProgressReceiver(_prog);
    return w.write(inter);
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return writeGrid(_f, _m.getGrid(), _prog);
  }

  /**
   *
   */
  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    return read(_f, _prog);
  }

  /**
   * Lit le maillage, calcul le bord et oriente les �l�ments dans le sens trigo.
   * 
   * @param _f le fichier a lire
   * @param _prog la barre de progress
   * @return synthese de l'operation (EfGrid comme source)
   */
  public CtuluIOOperationSynthese readGridStrict(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese f = readGrid(_f, _prog);
    if (f == null || f.getSource() == null) {
      return f;
    }
    EfLib.orienteGrid((EfGrid) f.getSource(), _prog, true, f.getAnalyze());
    ((EfGrid) f.getSource()).computeBord(_prog, f.getAnalyze());
    return f;
  }

  /**
   *
   */
  public boolean canWriteOnlyGrid() {
    return true;
  }

  /**
   *
   */
  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese r = read(_f, _prog);
    r.setSource(((CorEleBthInterface) r.getSource()).getGrid());
    return null;
  }

  /**
   *
   */
  public boolean containsGrid() {
    return true;
  }

  @Override
    public boolean canReadGrid()
    {
        return true;
    }
    
  @Override
    public boolean canWriteGrid()
    {
        return true;
    }
    
  @Override
    public boolean hasBoundaryConditons()
    {
        // TODO Voir si correct
        return false;
    }

}