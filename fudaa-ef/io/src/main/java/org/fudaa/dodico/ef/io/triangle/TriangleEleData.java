/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

/**
 * Contient le r�sultat de lecture du fichier ele.
 *
 * @author Frederic Deniger
 */
public class TriangleEleData {

  private final int[][] elt;
  private final double[][] attributes;
  private final int nbNodesPerElt;
  private final int nbAttributes;

  public TriangleEleData(int nbEle, int nbNodesPerElt, int nbAttributes) {
    elt = new int[nbEle][nbNodesPerElt];
    attributes = new double[nbEle][nbAttributes];
    this.nbAttributes = nbAttributes;
    this.nbNodesPerElt = nbNodesPerElt;
  }

  public int getNbAttributes() {
    return nbAttributes;
  }

  public int getNbNodesPerElt() {
    return nbNodesPerElt;
  }

  public int getNbElt() {
    return elt.length;
  }

  public int getPtIdx(int idxElt, int idxPtInElt) {
    return elt[idxElt][idxPtInElt];
  }

  public double getAttribute(int idxElt, int idxAttribute) {
    return attributes[idxElt][idxAttribute];
  }

  void setEltIdx(int idxElt, int idxPt, int intField) {
    elt[idxElt][idxPt] = intField;
  }

  void setAttributes(int idxElt, int idxAttr, double doubleField) {
    attributes[idxElt][idxAttr] = doubleField;
  }
}
