/**
 *  @creation     24 sept. 2004
 *  @modification $Date: 2006-04-07 09:23:03 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;



/**
 * @author Fred Deniger
 * @version $Id: SerafinFileFormatVersionInterface.java,v 1.4 2006-04-07 09:23:03 deniger Exp $
 */
public interface SerafinFileFormatVersionInterface extends FileFormatVersionInterface{
  String getDefaultVariableFond();
  int getParamNb();
  
  String[] getVariableFond();
  boolean isFormatEnColonne(int[] _s);
  boolean isIdateDefini(int[] _s);
  void setDateDefini(int[] _s);
  void setFormatColonne(int[] _s);
  void unsetDateDefini(int[] _s);
  void unsetFormatColonne(int[] _s);
  CtuluIOOperationSynthese writeSparc(File _f,SerafinInterface _source,
    ProgressionInterface _prog);

}
