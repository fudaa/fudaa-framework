/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;

/**
 * *Permet d'�crire les fichiers de type node: https://www.cs.cmu.edu/~quake/triangle.node.html
 * @author Frederic Deniger
 */
public class TriangleNodeWriter extends FileOpWriterCharSimpleAbstract {

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof TriangleNodeDataInterface) {
      try {
        writeNode((TriangleNodeDataInterface) _o);
      } catch (IOException ex) {
        Logger.getLogger(TriangleNodeWriter.class.getName()).log(Level.SEVERE, null, ex);
        analyze_.manageException(ex);
      }
    } else {
      analyze_.addFatalError(DodicoLib.getS("Donn�es invalides"));
    }
  }

  private void writeNode(TriangleNodeDataInterface triangleNodeDataInterface) throws IOException {
    if (triangleNodeDataInterface == null) {
      analyze_.addFatalError(EfIOResource.getS("Les donn�es sont nulles"));
      return;
    }
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    final int nbPoints = triangleNodeDataInterface.getPtsNb();
    //premiere ligne
//    First line: <# of vertices> <dimension (must be 2)> <# of attributes> <# of boundary markers (0 or 1)> 
    out_.write(Integer.toString(nbPoints));
    ProgressionUpdater updater = new ProgressionUpdater(progress_);
    updater.setValue(20, nbPoints);
    writeSpace();
    out_.write("2");
    writeSpace();
    final int nbAttributes = triangleNodeDataInterface.getNbAttributes();
    out_.write(Integer.toString(nbAttributes));
    writeSpace();
    final boolean containsBoundaryMarkers = triangleNodeDataInterface.containsBoundaryMarkers();
    if (containsBoundaryMarkers) {
      out_.write("1");
    } else {
      out_.write("0");
    }
    writelnToOut();

    //ensuite 
    //<vertex #> <x> <y> [attributes] [boundary marker] 
    for (int i = 0; i < nbPoints; i++) {
      out_.write(Integer.toString(i + 1));
      writeSpace();
      out_.write(Double.toString(triangleNodeDataInterface.getPtX(i)));
      writeSpace();
      out_.write(Double.toString(triangleNodeDataInterface.getPtY(i)));
      if (nbAttributes > 0) {
        for (int k = 0; k < nbAttributes; k++) {
          writeSpace();
          out_.write(Double.toString(triangleNodeDataInterface.getAttributes(i, k)));
        }
      }
      if (containsBoundaryMarkers) {
        writeSpace();
        out_.write(Integer.toString(triangleNodeDataInterface.getMarker(i)));
      }
      writelnToOut();
      updater.majAvancement();
    }

  }
}
