/**
 *  @creation     24 sept. 2004
 *  @modification $Date: 2006-04-14 14:51:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.dunes;

import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

/**
 * @author Fred Deniger
 * @version $Id: DunesVersionInterface.java,v 1.6 2006-04-14 14:51:47 deniger Exp $
 */
public interface DunesVersionInterface extends FileFormatVersionInterface {

  /**
   * @return the ADAPTATIF.
   */
  int getAdaptatifCode();

  /**
   * @return code non adaptif
   */
  int getNonAdaptatifCode();

  /**
   * @return ELEMENT_ID
   */
  String getElementCode();

  /**
   * Returns the NOEUD_ID.
   *
   * @return String
   */
  String getNoeudCode();

}
