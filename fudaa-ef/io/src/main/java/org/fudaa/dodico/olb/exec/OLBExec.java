/**
 * @creation 10 d�c. 2003 @modification $Date: 2007-06-29 15:10:32 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.olb.exec;

import java.io.File;
import java.io.IOException;
import java.util.BitSet;

import com.memoire.fu.FuLib;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;

import org.fudaa.dodico.calcul.CalculExecBatch;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNodeMutable;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.impl.EfGridTranslate;
import org.fudaa.dodico.ef.io.dunes.DunesAdapter;
import org.fudaa.dodico.ef.io.dunes.DunesInterface;
import org.fudaa.dodico.ef.io.dunes.DunesMAIFileFormat;
import org.fudaa.dodico.objet.CExec;

/**
 * @author deniger
 * @version $Id: OLBExec.java,v 1.24 2007-06-29 15:10:32 deniger Exp $
 */
public class OLBExec extends CalculExecBatch {

  boolean stop_;

  public OLBExec() {
    super("olb");
  }

  /**
   * @param _init le maillage intiale
   * @param _prog la barre de progression
   * @param _ui le receveur des message
   * @return le maillage transforme ou null si erreur.
   */
  public EfGridInterface computeGrid(final EfGridInterface _init, final ProgressionInterface _prog, final CtuluUI _ui) {
    stop_ = false;
    final File exe = new File(getExecFile());
    if (!exe.exists()) {
      if (_ui != null) {
        _ui.error("OLB", DodicoLib.getS("Le fichier {0} est introuvable", exe.getAbsolutePath()), false);
      }
      return null;
    }
    File inFile = null;
    File outFile = null;
    File dir = exe.getParentFile();
    if (!dir.canWrite()) {
      dir = new File(FuLib.getJavaTmp());
      if (!dir.exists() || !dir.canWrite()) {
        return null;
      }
    }
    try {
      inFile = File.createTempFile("temp", ".olb_in", dir);
      outFile = File.createTempFile("temp", ".olb_out", dir);
    } catch (final IOException e) {
      deleteFiles(inFile, outFile);
      return null;
    }
    if (stop_) {
      deleteFiles(inFile, outFile);
      return null;
    }
    if ((inFile == null) || (outFile == null)) {
      deleteFiles(inFile, outFile);
      return null;
    }
    final DunesAdapter interfaceDunes = new DunesAdapter();
    // BUG du 12 octobre 2006
    // bogue de OLB arrondit les coordonn�es et rend des �l�meents T6 qui ne sont plus des T6:
    // les points milieux ne sont plus des points milieux du fait de l'arrondi.
    double offsetX = (_init.getMinX() + _init.getMaxX()) / 2;
    double offsetY = (_init.getMinY() + _init.getMaxY()) / 2;
    final EfGridInterface init = new EfGridTranslate(_init, -offsetX, -offsetY);
    interfaceDunes.setMaillage(init);
    CtuluIOOperationSynthese op = DunesMAIFileFormat.getInstance().getLastVersionInstance(null).write(inFile,
                                                                                                      interfaceDunes, _prog);
    if (stop_) {
      deleteFiles(inFile, outFile);
      return null;
    }
    if ((_ui != null) && (op.containsSevereError())) {
      _ui.error(null, op.getAnalyze().getFatalError().toString(), false);
      deleteFiles(inFile, outFile);
      return null;
    }

    final String[] cmd = new String[3];
    cmd[0] = exe.getAbsolutePath();
    cmd[1] = inFile.getName();
    cmd[2] = outFile.getName();
    final CExec exec = new CExec(cmd);
    exec.setExecDirectory(dir);
    exec.exec();
    if (!outFile.exists()) {
      deleteFiles(inFile, outFile);
      return null;
    }
    op = DunesMAIFileFormat.getInstance().getLastVersionInstance(outFile).read(outFile, _prog);
    if ((_ui != null) && (op.containsSevereError())) {
      _ui.error("OLB", op.getAnalyze().getFatalError(), false);
    }
    deleteFiles(inFile, outFile);
    final DunesInterface r = (DunesInterface) op.getSource();
    if (r == null || r.getGrid() == null) {
      return null;
    }
    final EfGridInterface resDunes = new EfGridTranslate(r.getGrid(), offsetX, offsetY);
    final EfGridInterface resFinal = resDunes.getEltType() == EfElementType.T6 ? checkT6(resDunes) : new EfGridArray(
            resDunes);
    return resFinal;
  }

  private EfGridInterface checkT6(final EfGridInterface _resDunes) {
    // pour les elements T6, on ne fait confiance � OLB et on recalcule les points milieux.
    final EfElement[] elt = _resDunes.getElts();
    final EfNodeMutable[] nds = _resDunes.getNodesMutable();
    final BitSet set = new BitSet(nds.length);
    final int nb = 6;
    for (int i = elt.length - 1; i >= 0; i--) {
      final EfElement elti = elt[i];
      for (int k = nb - 1; k >= 1; k -= 2) {
        final int idx = elti.getPtIndex(k);
        if (!set.get(idx)) {
          set.set(idx);
          final int next = elti.getPtIndex((k + 1) % nb);
          final int prec = elti.getPtIndex(k - 1);
          nds[idx].setX((nds[next].getX() + nds[prec].getX()) / 2);
          nds[idx].setY((nds[next].getY() + nds[prec].getY()) / 2);
          nds[idx].setZ((nds[next].getZ() + nds[prec].getZ()) / 2);
        }
      }
    }
    return new EfGridArray(nds, elt, EfElementType.T6);
  }

  private void deleteFiles(final File _f, final File _f1) {
    if (_f != null) {
      _f.delete();
    }
    if (_f1 != null) {
      _f1.delete();
    }
  }

  /**
   * Permet de stopper le processus entre les etapes ecriture maillage etlancement de olb.
   */
  public void stop() {
    stop_ = true;
  }
}