/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GisZoneCollectionAsListPointAdapter;

/**
 * Adapte une collection en un fournisseur de points.
 *
 * @author Frederic Deniger
 */
public class TriangleNodeAdapter extends GisZoneCollectionAsListPointAdapter implements TriangleNodeDataInterface {

  public TriangleNodeAdapter(GISZoneCollection points) {
    super(points);
  }

  @Override
  public int getNbAttributes() {
    return 0;
  }

  @Override
  public double getAttributes(int idxPt, int idxAtt) {
    return 0;
  }

  @Override
  public boolean containsBoundaryMarkers() {
    return false;
  }

  @Override
  public int getMarker(int idxPt) {
    return 0;
  }
}
