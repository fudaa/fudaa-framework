/**
 * @creation 2002-11-20
 * @modification $Date: 2007-01-19 13:07:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import java.io.IOException;

import com.memoire.fu.FuLog;
import java.util.Map;

import org.fudaa.ctulu.fileformat.FileFormat;

import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfDataNode;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridBathyAdapter;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;

/**
 * lecture d'un fichier serafin. Les donnees sont enregistrees en tant que double par simplicite. Les donnees de la deuxieme discretisation sont
 * ignorees. Attention : la numerotation des tableaux commencent a 0 (java!!!).
 *
 * @version $Id: SerafinAdapter.java,v 1.26 2007-01-19 13:07:22 deniger Exp $
 * @author Fred Deniger
 */
public class SerafinAdapter implements SerafinInterface {

  @Override
  public ConditionLimiteEnum[] getBoundaryConditions() {
    return ConditionLimiteHelper.EMPTY_ARRAY;
  }
  private boolean xYdoublePrecision;

  @Override
  public boolean isXYdoublePrecision() {
    return xYdoublePrecision;
  }

  @Override
  public Map getOptions() {
    return null;
  }

  public void setXYdoublePrecision(boolean xYdoublePrecision) {
    this.xYdoublePrecision = xYdoublePrecision;
  }

  /**
   * @return the isVolumique
   */
  @Override
  public boolean isVolumique() {
    return isVolumique_;
  }

  /**
   * @param _isVolumique the isVolumique to set
   */
  public void setVolumique(boolean _isVolumique) {
    isVolumique_ = _isVolumique;
  }
  /**
   * Les donnees sont stockees par pas de temps puis par variables. donneesParTemps_[0] pas de temps 1 variables 1 donneesParTemps_[1] pas de temps 1
   * variables 2 ..... donneesParTemps_[Nbv1-1] pas de temps 1 variables Nbv1 donneesParTemps_[Nbv1] pas de temps 2 variables 1
   */
  private double[][] donneesParTemps_;
  /**
   *
   */
  private long idateInMillis_;
  private int idisc1_;
  private int[] iparam_;
  private int[] ipoboInitial_;
  private EfGridInterface maillage_;
  /**
   * Le nombre de pas de temps reels.
   */
  private int nbPasTemps_;
  /**
   *
   */
  private int nbv1_;
  /**
   *
   */
  private int nbv2_;
  /**
   *
   */
  private String[] nomVariables_;
  /**
   * Les valeurs des pas de temps.
   */
  private double[] temps_;
  /**
   *
   */
  private String titre_;
  /**
   *
   */
  private String[] uniteVariables_;
  SerafinNewReaderInfo info_;
  int[] ipoboFr_;
  private boolean isVolumique_;

  /**
   * rien.
   */
  public SerafinAdapter() {
  }

  protected boolean majGridFond() {
    return majGridFond(getTimeStepNb() - 1);
  }

  protected boolean majGridFond(final int _timeStep) {
    if (_timeStep < 0) {
      return false;
    }
    final String[] fond = SerafinFileFormat.getCommonVariableFond();
    int numVariable = -1;
    for (int i = 0; i < fond.length; i++) {
      numVariable = EfGridSourcesAbstract.getValueIndex(fond[i], this);
      if (numVariable >= 0) {
        break;
      }
    }
    if (numVariable < 0) {
      return false;
    }
    try {
      maillage_ = new EfGridBathyAdapter(new EfDataNode(getDonnees(_timeStep, numVariable)), maillage_);
    } catch (final IOException _evt) {
      FuLog.error(_evt);
      return false;

    }

    return true;
  }

  @Override
  public boolean containsElementData() {
    return isVolumique_;
  }

  @Override
  public boolean containsNodeData() {
    return !isVolumique_;
  }

  /**
   * @param _pasTemps le pas de temps
   * @param _numVariable le numero de la variable
   * @return toutes les valeurs dans l'ordre donne des point.
   */
  public double[] getDonnees(final int _pasTemps, final int _numVariable) throws IOException {
    if (nbv1_ <= 0) {
      return null;
    }
    if (info_ != null) {
      return info_.getDouble(_numVariable, _pasTemps);
    }
    return donneesParTemps_[_pasTemps * nbv1_ + _numVariable];
  }

  // public double getEltValue(final int _valueIdx, final int _eltIdx, final int _timeStep) {
  // return 0;
  // }
  //
  // public String getEltValueId(final int _valueIdx) {
  // return null;
  // }
  //
  // public int getEltValueNb() {
  // return 0;
  // }
  public FileFormat getFileFormat() {
    return SerafinFileFormat.getInstance();
  }

  @Override
  public EfGridInterface getGrid() {
    return maillage_;
  }

  /**
   * @return Idate
   */
  @Override
  public long getIdate() {
    return idateInMillis_;
  }

  /**
   * Option non prise en compte.
   *
   * @return Idisc1 soit 1 (pout P1) en general.
   */
  @Override
  public int getIdisc1() {
    return idisc1_;
  }

  /**
   * ikle1 est un tableau represente sous forme de vecteur. Le tableau a nelem1_ lignes et nppel1_ colonnes. le point j de l'element i est obtenu par
   * (i,j)=i*nppel1_+j i [0,nelem1_[ j [0,nppel1_[
   *
   * @param _elem l'indice de l'element demande
   * @param _point l'indice du point demande
   * @return Ikle1 l'indice general du point appartenant a l'element _elem et ayant comme indice local _point
   */
  public int getIkle1(final int _elem, final int _point) {
    return maillage_.getElement(_elem).getPtIndex(_point);
  }

  /**
   * @return Iparam
   */
  @Override
  public int[] getIparam() {
    return iparam_;
  }

  @Override
  public int[] getIpoboFr() {
    return ipoboFr_;
  }

  @Override
  public int[] getIpoboInitial() {
    return ipoboInitial_;
  }

  /**
   * Returns the nbv2.
   *
   * @return int
   */
  public int getNbv2() {
    return nbv2_;
  }

  /**
   * Returns the temps.
   *
   * @return double[]
   */
  public double[] getPasDeTemps() {
    return temps_;
  }

  @Override
  public final SerafinNewReaderInfo getReadingInfo() {
    return info_;
  }

  @Override
  public double getTimeStep(final int _i) {
    if (temps_ == null) {
      return 0;
    }
    return temps_[_i];
  }

  /**
   * Renvoie le nombre de pas de temps.
   *
   * @return nbPasTemps_
   */
  @Override
  public int getTimeStepNb() {
    return nbPasTemps_;
  }

  /**
   * @return Titre
   */
  @Override
  public String getTitre() {
    return titre_;
  }

  /**
   * Renvoie l'unite des variables ( dans l'ordre).
   *
   * @return uniteVariables_
   */
  @Override
  public String getUnite(final int _i) {
    return uniteVariables_[_i];
  }

  /**
   * @return les unites des variables
   */
  public String[] getUnites() {
    return uniteVariables_;
  }

  /**
   * @param _pasTemps
   * @param _numVariable
   * @return Donnees
   */
  @Override
  public double getValue(final int _numVariable, final int _pasTemps, final int _i) throws IOException {
    if (nbv1_ <= 0) {
      return 0;
    }
    if (info_ != null) {
      return info_.getDouble(_numVariable, _pasTemps)[_i];
    }
    return donneesParTemps_[_pasTemps * nbv1_ + _numVariable][_i];
  }

  /**
   * Renvoie le nom des variables.
   *
   * @return nomVariables_
   */
  @Override
  public String getValueId(final int _i) {
    return nomVariables_[_i];
  }

  /**
   * Renvoie le nombre de variables de 1ere discretisation.
   *
   * @return nbv1_
   */
  @Override
  public int getValueNb() {
    return nbv1_;
  }

  /**
   * @return les variables dans l'ordre (important)
   */
  public String[] getVariables() {
    return nomVariables_;
  }

  /**
   * @param _i l'indice demande
   * @return X1
   */
  public double getX1(final int _i) {
    return maillage_.getPtX(_i);
  }

  /**
   * @param _i l'indice demande
   * @return Y1
   */
  public double getY1(final int _i) {
    return maillage_.getPtY(_i);
  }

  @Override
  public boolean isElement(final int _idx) {
    return isVolumique_;
  }

  /**
   * Sets the donneesParTemps.
   *
   * @param _donneesParTemps The donneesParTemps to set
   */
  public void setDonneesParTemps(final double[][] _donneesParTemps) {
    donneesParTemps_ = _donneesParTemps;
  }

  /**
   * Sets the idateInMillis.
   *
   * @param _idateInMillis The idateInMillis to set
   */
  public void setIdateInMillis(final long _idateInMillis) {
    idateInMillis_ = _idateInMillis;
  }

  /**
   * Sets the idisc1.
   *
   * @param _idisc1 The idisc1 to set
   */
  public void setIdisc1(final int _idisc1) {
    idisc1_ = _idisc1;
  }

  public final void setInfo(final SerafinNewReaderInfo _info) {
    info_ = _info;
  }

  /**
   * Sets the iparam.
   *
   * @param _iparam The iparam to set
   */
  public void setIparam(final int[] _iparam) {
    iparam_ = _iparam;
  }

  public void setIpoboFr(final int[] _ipobo) {
    ipoboFr_ = _ipobo;
  }

  /**
   * @param _ipobo le tableau ipobo
   */
  public void setIpoboInitial(final int[] _ipobo) {
    ipoboInitial_ = _ipobo;
  }

  /**
   * Affecte le maillage. npoin1, nelem1 et nppel1 sont egalament initialise.
   *
   * @param _maillage The maillage to set
   */
  public void setMaillage(final EfGridInterface _maillage) {
    maillage_ = _maillage;
  }

  /**
   * Sets the nbv1.
   *
   * @param _nbv1 The nbv1 to set
   */
  public void setNbv1(final int _nbv1) {
    nbv1_ = _nbv1;
  }

  /**
   * Sets the nbv2.
   *
   * @param _nbv2 The nbv2 to set
   */
  public void setNbv2(final int _nbv2) {
    nbv2_ = _nbv2;
  }

  /**
   * Sets the nomVariables.
   *
   * @param _nomVariables The nomVariables to set
   */
  public void setNomVariables(final String[] _nomVariables) {
    nomVariables_ = _nomVariables;
  }

  /**
   * Sets the temps.
   *
   * @param _temps The temps to set
   */
  public void setPasDeTemps(final double[] _temps) {
    temps_ = _temps;
    nbPasTemps_ = _temps.length;
  }

  /**
   * Sets the titre.
   *
   * @param _titre The titre to set
   */
  public void setTitre(final String _titre) {
    titre_ = _titre;
  }

  /**
   * Sets the uniteVariables.
   *
   * @param _uniteVariables The uniteVariables to set
   */
  public void setUniteVariables(final String[] _uniteVariables) {
    uniteVariables_ = _uniteVariables;
  }
}