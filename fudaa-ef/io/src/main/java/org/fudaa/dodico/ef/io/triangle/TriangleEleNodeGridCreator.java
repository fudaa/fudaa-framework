/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import gnu.trove.TIntIntHashMap;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EFGridArrayZ;
import org.fudaa.dodico.ef.io.EfIOResource;

/**
 * Permet de lire un couple de fichier node/ele afin de cr�er un maillage. V�rifie que le indices sont corrects et que tous les points du fichier node
 * sont utilis�s: sinon renumerote le maillage et enleve les noeuds non utilis�s.
 *
 * @author Frederic Deniger
 */
public class TriangleEleNodeGridCreator {

  private boolean useFirstAttributesAsZ = true;
  private ProgressionInterface progression;

  public ProgressionInterface getProgression() {
    return progression;
  }

  /**
   *
   * @param progression recevra l'avancement de l'op�ration.
   */
  public void setProgression(ProgressionInterface progression) {
    this.progression = progression;
  }

  /**
   *
   * @param useFirstAttributesAsZ true si on veut que l'attribut issu du fichier node soit trait� comme la valeur z.
   */
  public void setUseFirstAttributesAsZ(boolean useFirstAttributesAsZ) {
    this.useFirstAttributesAsZ = useFirstAttributesAsZ;
  }

  /**
   * Permet de lire les fichiers node/ele pour construire le maillage.
   *
   * @param nodeFile le fichier node
   * @param eltFile le fichier ele
   * @return r�sultat de l'op�ration de lecture.
   */
  public CtuluIOResult<EFGridArrayZ> readGrid(File nodeFile, File eltFile) {
    ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(2);
    EFGridArrayZ grid = null;
    CtuluIOResult<TriangleNodeDataInterface> nodeRes = null;
    CtuluIOResult<TriangleEleData> eltRes = null;
    try {
      Callable<CtuluIOResult> nodeReader = new NodeReaderCallable(nodeFile, progression);
      Callable<CtuluIOResult> eltReader = new EltReaderCallable(eltFile, progression);
      List<Callable<CtuluIOResult>> callables = Arrays.asList(nodeReader, eltReader);
      List<Future<CtuluIOResult>> invokeAll = newFixedThreadPool.invokeAll(callables);
      nodeRes = (CtuluIOResult<TriangleNodeDataInterface>) invokeAll.get(0).get();
      eltRes = (CtuluIOResult<TriangleEleData>) invokeAll.get(1).get();
    } catch (Exception e) {
      Logger.getLogger(TriangleEleNodeGridCreator.class.getName()).log(Level.INFO, "message {0}", e);
    } finally {
      newFixedThreadPool.shutdownNow();
    }
    if (nodeRes != null && nodeRes.getSource() == null) {
      return new CtuluIOResult<EFGridArrayZ>(nodeRes.getAnalyze());
    }
    if (eltRes != null && eltRes.getSource() == null) {
      return new CtuluIOResult<EFGridArrayZ>(eltRes.getAnalyze());
    }
    if (nodeRes != null && nodeRes.getSource() != null && eltRes != null && eltRes.getSource() != null) {
      return createGrid(nodeRes.getSource(), eltRes.getSource());
    }
    return new CtuluIOResult<EFGridArrayZ>();
  }

  private class NodeReaderCallable implements Callable<CtuluIOResult> {

    private final File nodeFile;
    ProgressionInterface progression;

    public NodeReaderCallable(File nodeFile, ProgressionInterface progression) {
      this.nodeFile = nodeFile;
      this.progression = progression;
    }

    @Override
    public CtuluIOResult call() throws Exception {
      return new TriangleNodeReader().read(nodeFile, progression);
    }
  }

  private class EltReaderCallable implements Callable<CtuluIOResult> {

    private final File eltFile;
    ProgressionInterface progression;

    public EltReaderCallable(File eltFile, ProgressionInterface progression) {
      this.eltFile = eltFile;
      this.progression = progression;
    }

    @Override
    public CtuluIOResult call() throws Exception {
      return new TriangleEleReader().read(eltFile, progression);
    }
  }

  CtuluIOResult<EFGridArrayZ> createGrid(TriangleNodeDataInterface nodes, TriangleEleData elts) {
    EfNode[] efNodes = new EfNode[nodes.getPtsNb()];
    boolean useZ = useFirstAttributesAsZ && nodes.getNbAttributes() >= 1;
    ProgressionUpdater updater = new ProgressionUpdater(progression);
    updater.setValue(10, efNodes.length);
    updater.majProgessionStateOnly(EfIOResource.getS("Cr�ation des noeuds"));
    for (int idxNode = 0; idxNode < efNodes.length; idxNode++) {
      efNodes[idxNode] = new EfNode(nodes.getPtX(idxNode), nodes.getPtY(idxNode), useZ ? nodes.getAttributes(idxNode, 0) : 0);
      updater.majAvancement();
    }
    BitSet usedNode = new BitSet();
    int nbElt = elts.getNbElt();
    updater.setValue(10, nbElt);
    updater.majProgessionStateOnly(EfIOResource.getS("Cr�ation des �l�ments"));
    int[][] eltsIdx = new int[nbElt][];
    CtuluLog log = new CtuluLog();
    for (int idxElt = 0; idxElt < nbElt; idxElt++) {
      int[] idx = new int[elts.getNbNodesPerElt()];
      for (int k = 0; k < idx.length; k++) {
        int pt = elts.getPtIdx(idxElt, k);
        idx[k] = pt;
        usedNode.set(pt);
        if (pt < 0 || pt >= efNodes.length) {
          log.addSevereError(EfIOResource.getS("L'�l�ment {0} contient des indices de noeuds erron�s", Integer.toString(idxElt + 1)));
          return new CtuluIOResult<EFGridArrayZ>(log);
        }
      }
      eltsIdx[idxElt] = idx;
      updater.majAvancement();
    }
    boolean correct = true;
    for (int idxNode = 0; idxNode < efNodes.length; idxNode++) {
      if (!usedNode.get(idxNode)) {
        correct = false;
      }
    }
    if (!correct) {
      List<EfNode> newNodes = new ArrayList<EfNode>();
      TIntIntHashMap oldIdxNewIdx = new TIntIntHashMap();
      for (int idxNode = 0; idxNode < efNodes.length; idxNode++) {
        if (usedNode.get(idxNode)) {
          oldIdxNewIdx.put(idxNode, newNodes.size());
          newNodes.add(efNodes[idxNode]);
        }
      }
      for (int idxElt = 0; idxElt < nbElt; idxElt++) {
        int nbPt = eltsIdx[idxElt].length;
        for (int idx = 0; idx < nbPt; idx++) {
          int oldIdx = eltsIdx[idxElt][idx];
          eltsIdx[idxElt][idx] = oldIdxNewIdx.get(oldIdx);
        }
      }
      efNodes = (EfNode[]) newNodes.toArray(new EfNode[newNodes.size()]);
    }

    EfElement[] efElt = new EfElement[nbElt];
    for (int idxElt = 0; idxElt < nbElt; idxElt++) {
      efElt[idxElt] = new EfElement(eltsIdx[idxElt]);
    }
    EFGridArrayZ grid = new EFGridArrayZ(efNodes, efElt);
    grid.computeBord(progression, null);
    return new CtuluIOResult<EFGridArrayZ>(log, grid);

  }
}
