package org.fudaa.dodico.ef.io.supertab;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridMutable;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

public class SuperTabReader extends FileOpReadCharSimpleAbstract {
  /*
   * public enum SuperTabFormat { SUPERTAB_V4, SUPERTAB_V6, }
   */
  // private final static int TITLE_SEC = 151;

  private final static int NODES_SIMPLE_PREC_SEC = 15;
  private final static int NODES_DOUBLE_PREC_SEC_V4 = 0;// A enlever je pense
  private final static int NODES_DOUBLE_PREC = 2411;// d'apr�s exemple donne
  private final static int NODES_DOUBLE_PREC_SEC_V6 = 781;
  private final static int ELEMENTS_SEC_DOUBLLE_PREC = 2412;// d'apr�s exemple donne
  private final static int ELEMENTS_SEC_V4 = 71;
  private final static int ELEMENTS_SEC_V6 = 780;

  public SuperTabReader() {
  }

  @Override
  protected Object internalRead() {
    return readGrid();
  }

  private EfGridSource readGrid() {
    // TODO Voir si les initialisations sont correctes.
    in_.setJumpBlankLine(true);
    in_.setCommentInOneField("!");
    in_.setJumpCommentLine(true);

    // String title = "";
    List<EfNode> nodes = new ArrayList<EfNode>();
    List<ConditionLimiteEnum> boundaryCond = new ArrayList<ConditionLimiteEnum>();
    List<EfElement> elements = new ArrayList<EfElement>();
    // boolean titleRead = false;
    boolean nodesRead = false;
    boolean elementsRead = false;
    EfElementType type = null;
    int sector;

    try {
      while (!(/* titleRead && */nodesRead && elementsRead)) {
        this.searchBeginSector();

        sector = this.searchNextSector();
        switch (sector) {
          /*
           * case TITLE_SEC : { title = this.readTitle();
           * 
           * titleRead = true;
           * 
           * break; }
           */

          case NODES_SIMPLE_PREC_SEC:
          case NODES_DOUBLE_PREC_SEC_V4:
          case NODES_DOUBLE_PREC_SEC_V6: {
            this.readNodes(nodes, boundaryCond);
            nodesRead = true;
            break;
          }
          case NODES_DOUBLE_PREC: {
            this.readNodesDoublePrec(nodes, boundaryCond);
            nodesRead = true;
            break;
          }
          case ELEMENTS_SEC_DOUBLLE_PREC:
          case ELEMENTS_SEC_V4:
          case ELEMENTS_SEC_V6: {
            type = this.readElements(elements);
            elementsRead = true;

            break;
          }
        }

      }
    } catch (final EOFException eOFException) {
      analyze_.manageException(eOFException);
    } catch (final NumberFormatException nbFmtException) {
      analyze_.manageException(nbFmtException, in_.getLineNumber());
    } catch (IOException iOException) {
      analyze_.manageException(iOException);
    }

    if ((nodes.size() > 0) && (elements.size() > 0)) {
      final EfGridMutable grid = new EfGridMutable(nodes.toArray(new EfNode[0]), elements.toArray(new EfElement[0]));

      grid.setTypeElt(type);
      // EfLib.orienteGrid(grid, progress_, true, analyze_);

      return new EfGridSourceDefaut(grid, SuperTabFileFormat.getInstance(), boundaryCond
              .toArray(new ConditionLimiteEnum[boundaryCond.size()]));
    }

    return null;
  }

  /**
   * Recherche le num�ro du prochain secteur.
   *
   * @return le num�ro du prochain secteur.
   * @throws IOException
   */
  private int searchNextSector() throws IOException {
    int sector = -1;

    // Recherche du prochain n� de secteur.
    while (sector == -1) {
      in_.readFields();

      sector = in_.intField(0);
    }

    return sector;
  }

  /**
   * Recherche le d�but du prochain secteur.
   *
   * @throws IOException
   */
  private void searchBeginSector() throws IOException {
    int sector = 0;
    boolean isIntField = false;

    // Recherche la prochaine ligne commen�ant par -1 (ligne avant celle contenant le n� de secteur).
    while (!(isIntField && (sector == -1))) {
      in_.readFields();

      try {
        sector = Integer.parseInt(in_.stringField(0));

        isIntField = true;
      } catch (Exception e) {
      }
    }
  }

  /**
   * Donne le type d'�l�ments pour la grille.
   *
   * @param currentType le type trouv� actuellement.
   * @param nbPts le nombre de points de l'�l�ment test�.
   * @return le type d'�l�ments pour la grille.
   */
  private EfElementType getType(EfElementType currentType, int nbPts) {
    EfElementType type = EfElementType.getCommunType(nbPts);

    if (currentType == null) {
      return null;
    } else if (currentType.equals(type)) {
      return type;
    } else if ((currentType.equals(EfElementType.T3_Q4) && (type.equals(EfElementType.T3) || type
            .equals(EfElementType.Q4)))
            || (currentType.equals(EfElementType.Q4) && type.equals(EfElementType.T3))
            || (currentType.equals(EfElementType.T3) && type.equals(EfElementType.Q4))) {
      return EfElementType.T3_Q4;
    }

    return null;
  }

  /**
   * Lit les �l�ments dans le fichier et retourne le type d'�l�ments pour la grille.
   *
   * @param elements la liste des �l�ments.
   * @return le type d'�l�ments pour la grille
   * @throws IOException
   */
  private EfElementType readElements(List<EfElement> elements) throws IOException {
    EfElementType type = null;
    boolean isFirstElt = true;

    in_.readFields();
    int sector = in_.intField(0);
    int typeEle = in_.intField(5);
    //on ignore les segments �ventuels
    while (typeEle == 2 && sector != -1) {
      in_.readFields();
      in_.readFields();
      in_.readFields();
      sector = in_.intField(0);
      typeEle = in_.intField(5);
    }

    while (sector != -1) {
      in_.readFields();
      int nbPts = in_.getNumberOfFields();

      int[] pts = new int[nbPts];

      for (int i = 0; i < nbPts; i++) {
        pts[i] = in_.intField(i) - 1;
      }

      elements.add(new EfElement(pts));

      if (isFirstElt) {
        type = EfElementType.getCommunType(nbPts);

        isFirstElt = false;
      } else {
        type = this.getType(type, nbPts);
      }

      in_.readFields();
      sector = in_.intField(0);
    }

    return type;
  }

  /**
   * Lit les points dans le fichier.
   *
   * @param nodes la liste des points.
   * @param boundaryCondtion la liste des conditions limites.
   * @throws IOException
   */
  private void readNodes(List<EfNode> nodes, List<ConditionLimiteEnum> boundaryCondtion) throws IOException {
    in_.readFields();
    int sector = in_.intField(0);

    while (sector != -1) {
      int color = in_.intField(3);
      double x = in_.doubleField(4);
      double y = in_.doubleField(5);
      double z = in_.doubleField(6);
      nodes.add(new EfNode(x, y, z));
      boundaryCondtion.add(ConditionLimiteHelper.getConditionsLimite(color));

      in_.readFields();
      sector = in_.intField(0);
    }
  }

  /**
   * En fortran, les doubles pr�cision sont ecrit avec un D a la place du E
   *
   * @param i l'indice de la colonne a lire
   * @return le double lu
   */
  private double readDoublePrec(int i) {
    return Double.parseDouble(in_.stringField(i).replace('D', 'E'));
  }

  /**
   * Lit les points dans le fichier.
   *
   * @param nodes la liste des points.
   * @param boundaryCondtion la liste des conditions limites.
   * @throws IOException
   */
  private void readNodesDoublePrec(List<EfNode> nodes, List<ConditionLimiteEnum> boundaryCondtion) throws IOException {
    in_.readFields();
    int sector = in_.intField(0);

    while (sector != -1) {
      int color = in_.intField(3);
      boundaryCondtion.add(ConditionLimiteHelper.getConditionsLimite(color));
      // les coordonn�es sont sur une autre ligne
      in_.readFields();
      double x = readDoublePrec(0);
      double y = readDoublePrec(1);
      double z = readDoublePrec(2);
      nodes.add(new EfNode(x, y, z));

      in_.readFields();
      sector = in_.intField(0);
    }
  }

  /**
   * Lit et retourne le titre du fichier.
   *
   * @return le titre du fichier.
   * @throws IOException
   */
  private String readTitle() throws IOException {
    in_.readFields();

    return in_.stringField(0);
  }
}