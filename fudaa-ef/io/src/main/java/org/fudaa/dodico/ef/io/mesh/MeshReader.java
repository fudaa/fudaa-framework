package org.fudaa.dodico.ef.io.mesh;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridMutable;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.ef.io.supertab.SuperTabFileFormat;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

public class MeshReader extends FileOpReadCharSimpleAbstract
{
    @Override
    protected Object internalRead()
    {
        // TODO Auto-generated method stub
        return this.readGrid();
    }

    private EfGridSource readGrid() {
        // TODO Voir si les initialisations sont correctes.
        in_.setJumpBlankLine(true);
        in_.setCommentInOneField("!");
        in_.setJumpCommentLine(true);

        List<EfNode> nodes = new ArrayList<EfNode>();
        List<EfElement> elements = new ArrayList<EfElement>();
        String currentHeader = "";
        EfElementType type = null;
        boolean isFirstElementHeader = true;
        
        try {
            while (!MeshFormatHelper.isEndHeader(currentHeader))
            {
                currentHeader = this.getNextHeader();
                
                if (MeshFormatHelper.isNodeHeader(currentHeader))
                {
                    this.readNodes(nodes);
                }
                else if (MeshFormatHelper.isElementsHeader(currentHeader))
                {
                    if (isFirstElementHeader)
                    {
                        type = this.readElements(elements);
                        
                        isFirstElementHeader = false;
                    }
                    else
                    {
                        EfElementType lastType = this.readElements(elements);
                    
                        type = this.getElementType(type, lastType);
                    }
                }
            }
        } catch (final EOFException eOFException) {
          analyze_.manageException(eOFException);
        } catch (final NumberFormatException nbFmtException) {
          analyze_.manageException(nbFmtException, in_.getLineNumber());
        } catch (IOException iOException) {
          analyze_.manageException(iOException);
        }

        if ((nodes.size() > 0) && (elements.size() > 0)) {
          final EfGridMutable grid = new EfGridMutable(nodes.toArray(new EfNode[0]), elements.toArray(new EfElement[0]));

            grid.setTypeElt(type);
            // EfLib.orienteGrid(grid, progress_, true, analyze_);

          return new EfGridSourceDefaut(grid, SuperTabFileFormat.getInstance());
        }

        return null;
    }

    /**
     * R�cup�re le type courant des �l�ments.
     * @param currentType le type current.
     * @param lastType le dernier type r�cup�r�.
     * @return le type courant des �l�ments.
     */
    private EfElementType getElementType(EfElementType currentType, EfElementType lastType)
    {
        if (currentType == null)
        {
            return null;
        }
        else if (lastType == currentType)
        {
            return currentType;
        }
        else if ((currentType.equals(EfElementType.T3_Q4) && (lastType.equals(EfElementType.T3) || lastType.equals(EfElementType.Q4))) || (currentType.equals(EfElementType.Q4) && lastType.equals(EfElementType.T3)) || (currentType.equals(EfElementType.T3) && lastType.equals(EfElementType.Q4)))
        {
            return EfElementType.T3_Q4;
        }
        
        return null;
    }
    
    /**
     * R�cup�re le type des �l�ments lus et les lit.
     * @param elements la liste des �l�ments.
     * @return Le type des �l�ments lus.
     * @throws IOException
     */
    private EfElementType readElements(List<EfElement> elements) throws IOException {
        EfElementType type = null;
        
        in_.readFields();

        int nbElt = in_.intField(0);
        
        for (int i = 0; i < nbElt; i++)
        {
            in_.readFields();
            
            int[] pts = new int[in_.getNumberOfFields() - 1];
            
            if (type == null)
            {
                type = EfElementType.getCommunType(pts.length);
            }
            
            for (int j = 0; j < pts.length; j++)
            {
                pts[j] = in_.intField(j);
            }
            
            elements.add(new EfElement(pts));
        }
        
        return type;
    }
    
    /**
     * Lit les noeuds.
     * @param nodes la liste des noeuds.
     * @throws IOException
     */
    private void readNodes(List<EfNode> nodes) throws IOException {
        in_.readFields();

        int nbNode = in_.intField(0);
        
        for (int i = 0; i < nbNode; i++)
        {
            double[] xyz = new double[]{0.0, 0.0, 0.0};
            
            in_.readFields();
            
            for (int j = 0; j < in_.getNumberOfFields() - 1; j++)
            {
                xyz[j] = in_.doubleField(j);
            }
            
            nodes.add(new EfNode(xyz[0], xyz[1], xyz[2]));
        }
    }
    
    /**
     * R�cup�re la prochaine en-t�te.
     * @return le prochaine en-t�te.
     * @throws IOException
     */
    private String getNextHeader() throws IOException
    {
        String header;
        
        do
        {
            in_.readFields();
            header = in_.stringField(0);
        } while (!MeshFormatHelper.isHeader(header));
        
        return header;
    }
}
