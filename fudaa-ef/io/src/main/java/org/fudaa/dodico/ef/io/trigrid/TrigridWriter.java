/*
 *  @creation     27 janv. 2004
 *  @modification $Date: 2006-11-14 09:05:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.ef.io.trigrid;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * @author deniger
 * @version $Id: TrigridWriter.java,v 1.16 2006-11-14 09:05:28 deniger Exp $
 */
public class TrigridWriter extends FileOpWriterCharSimpleAbstract {

  public TrigridWriter() {}

  /**
   * Ecriture de l'objet _o qui doit etre un maillage EfGridInterface.
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof EfGridSource) {
      writeEfGrid(((EfGridSource) _o).getGrid());
    }
    if (_o instanceof EfGridInterface) {
      writeEfGrid((EfGridInterface) _o);
    } else {
      analyze_.addFatalError(DodicoLib.getS("Donn�es invalides"));
    }
  }

  private void writeEfGrid(final EfGridInterface _f) {
    // Test sur les objets a ecrire
    // les donnees sont nulles
    if (_f == null) {
      analyze_.addFatalError(EfIOResource.getS("Les donn�es sont nulles"));
      return;
    }
    // le flux de sortie est nul
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    // recherche des connexion
    if (progress_ != null) {
      progress_.setDesc(DodicoLib.getS("Recherche des connexions"));
    }
    final int[][] connexion = EfLib.getConnexionFor(_f, progress_, analyze_);
    if (!analyze_.isEmpty()) {
      analyze_.addFatalError(DodicoLib.getS("Impossible de trouver les connexions"));
      return;
    }
    int max = 0;
    // recherche du nombre de connexion max
    for (int i = connexion.length - 1; i >= 0; i--) {
      if (connexion[i].length > max) {
        max = connexion[i].length;
      }
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("Trigrif nb connection max= " + max);
    }
    // Recherche des frontiere si necessaire
    if (_f.getFrontiers() == null) {
      if (progress_ != null) {
        progress_.setDesc(DodicoLib.getS("Recherche des fronti�res"));
      }
      _f.computeBord(progress_, analyze_);
    }
    final DecimalFormat doubleFormat = new DecimalFormat("0.000");
    doubleFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
    if (progress_ != null) {
      progress_.setDesc(DodicoLib.getS("Ecriture"));
    }
    try {
      writelnToOut(CtuluLibString.getString(_f.getPtsNb()));
      writelnToOut(CtuluLibString.getString(max));
      // 3 eme ligne : xmax,ymax,xmin,xmin
      writelnToOut(doubleFormat.format(_f.getMaxX()) + CtuluLibString.ESPACE + doubleFormat.format(_f.getMaxY())
          + CtuluLibString.ESPACE + doubleFormat.format(_f.getMinX()) + CtuluLibString.ESPACE
          + doubleFormat.format(_f.getMinY()));
      final int nbPt = _f.getPtsNb();
      final int indexMaxLength = CtuluLibString.getString(nbPt).length();
      final int doubleMaxLength = 12;
      final ProgressionUpdater pu = new ProgressionUpdater(progress_);
      pu.setValue(4, nbPt);
      pu.majProgessionStateOnly();
      String temp;

      final String zero = CtuluLibString.ESPACE + FortranWriter.addSpacesBefore(indexMaxLength, CtuluLibString.ZERO);
      for (int i = 0; i < nbPt; i++) {
        final StringBuffer b = new StringBuffer(100);
        b.append(FortranWriter.addSpacesBefore(indexMaxLength, CtuluLibString.getString(i + 1)));
        b.append(' ');
        temp = doubleFormat.format(_f.getPtX(i));
        b.append(FortranWriter.addSpacesBefore(doubleMaxLength, temp));
        b.append(' ');
        temp = doubleFormat.format(_f.getPtY(i));
        b.append(FortranWriter.addSpacesBefore(doubleMaxLength, temp)).append(' ');
        b.append(' ');
        b.append((_f.getFrontiers().isFrontierPoint(i) ? CtuluLibString.UN : CtuluLibString.ZERO)).append(' ');
        temp = doubleFormat.format(_f.getPtZ(i));
        b.append(CtuluLibString.ESPACE);
        b.append(FortranWriter.addSpacesBefore(doubleMaxLength, temp)).append(' ');
        for (int j = connexion[i].length - 1; j >= 0; j--) {
          b.append(CtuluLibString.ESPACE);
          b.append(FortranWriter.addSpacesBefore(indexMaxLength, CtuluLibString.getString(connexion[i][j] + 1)));
        }
        for (int j = max - connexion[i].length; j > 0; j--) {
          b.append(zero);
        }
        // Mise a jour du buffer
        writelnToOut(b.toString());
        pu.majAvancement();
      }

    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }

  }

}
