/**
 * @creation 8 ao�t 2003
 * @modification $Date: 2007-05-04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import java.util.Map;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.EfIOResource;

/**
 * @author deniger
 * @version $Id: SerafinMaillageBuilderAdapterAbstract.java,v 1.12 2007-05-04 13:47:27 deniger Exp $
 */
public abstract class SerafinMaillageBuilderAdapterAbstract implements SerafinInterface {

  long date_;
  int[] iparam_;
  EfGridInterface maill_;
  String titre_;
  String[] units_;
  SerafinFileFormatVersionInterface version_;
  protected String[] valuesName_;

  /**
   * Par defaut le format colonne est choisi.
   *
   * @param _ft la verison
   * @param _m le maillage support
   */
  public SerafinMaillageBuilderAdapterAbstract(final SerafinFileFormatVersionInterface _ft, final EfGridInterface _m) {
    maill_ = _m;
    version_ = _ft;
    titre_ = EfIOResource.getS("maillage");
    iparam_ = new int[version_.getParamNb()];
    setFormatColonne(true);
  }

  @Override
  public Map getOptions() {
    return null;
  }

  @Override
  public boolean containsElementData() {
    return isVolumique();
  }

  @Override
  public boolean containsNodeData() {
    return !isVolumique();
  }

  /**
   *
   */
  public final FileFormat getFileFormat() {
    return SerafinFileFormat.getInstance();
  }

  @Override
  public final EfGridInterface getGrid() {
    return maill_;
  }

  @Override
  public final long getIdate() {
    return date_;
  }

  @Override
  public final int getIdisc1() {
    return 1;
  }

  @Override
  public final int[] getIparam() {
    return iparam_;
  }

  @Override
  public int[] getIpoboInitial() {
    if (maill_.getFrontiers() == null) {
      maill_.computeBord(null, null);
      if (maill_.getFrontiers() == null) {
        return null;
      }
    }
    return maill_.getFrontiers().getIpobo(maill_.getPtsNb());
  }

  @Override
  public int[] getIpoboFr() {
    if (maill_.getFrontiers() == null) {
      maill_.computeBord(null, null);
      if (maill_.getFrontiers() == null) {
        return null;
      }
    }
    return maill_.getFrontiers().getArray();
  }

  @Override
  public final SerafinNewReaderInfo getReadingInfo() {
    return null;
  }

  @Override
  public final String getTitre() {
    return titre_;
  }

  /**
   * @see org.fudaa.dodico.ef.io.serafin.SerafinInterface#getUnite(int)
   * @return M
   */
  @Override
  public String getUnite(final int _i) {
    return units_[_i];
  }

  @Override
  public boolean isElement(final int _idx) {
    return isVolumique();
  }

  /**
   * @param _time nouvelle date
   */
  public final void setDate(final long _time) {
    version_.setDateDefini(iparam_);
    date_ = _time;
  }

//  /**
//   * Intialise la date avec la date courante.
//   */
//  public final void setDateCourante() {
//    setDate(System.currentTimeMillis());
//  }
  /**
   * @param _b true si format colonne demande
   */
  public final void setFormatColonne(final boolean _b) {
    if (_b) {
      version_.setFormatColonne(iparam_);
    } else {
      version_.unsetFormatColonne(iparam_);
    }
  }

  /**
   * @param _t le titre
   */
  public final void setTitre(final String _t) {
    if (_t != null) {
      titre_ = _t;
    }
  }

  /**
   * @param _units les unites
   */
  public final void setUnits(final String[] _units) {
    units_ = _units;
  }

  /**
   * La date ne sera pas ajoutee au fichier (modifie iparam).
   */
  public final void unsetDate() {
    version_.unsetDateDefini(iparam_);
  }

  public void setIparam(final int[] _iparam) {
    iparam_ = _iparam;
  }
}