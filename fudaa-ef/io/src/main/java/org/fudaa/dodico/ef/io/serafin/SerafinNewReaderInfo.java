/**
 * @creation 22 nov. 2004
 * @modification $Date: 2006-11-14 09:05:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import org.fudaa.dodico.ef.helper.LRUCache;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: SerafinNewReaderInfo.java,v 1.10 2006-11-14 09:05:28 deniger Exp $
 */
public class SerafinNewReaderInfo {
    final File file_;
    final int nbValues_;
    final boolean volumique_;
    private final long firstTimeStepPos_;
    int nbVar_;
    private boolean isColonne_;
    private boolean doublePrecision;
    private ByteOrder order_;
    private long timeEnrLength_;
    /**
     * Cache pour getDouble(final int _nbV, final int _timeStep)
     */
    private Map<Integer, Map<Integer, SoftReference<double[]>>> cacheGetDouble_;

    /**
     * @param _nbValue le nombre de valeur definies. Peut etre le nombre de noeud ou le nombre d'element si resultat volumique.
     */
    public SerafinNewReaderInfo(final int _nbValue, final long _pos, final File _file, boolean _volumique, boolean doublePrecision) {
        super();

        cacheGetDouble_ = Collections.synchronizedMap(new LRUCache<>(100));
        nbValues_ = _nbValue;
        file_ = _file;
        firstTimeStepPos_ = _pos;
        volumique_ = _volumique;
        this.doublePrecision = doublePrecision;
    }

    public void close() {
    }

    public int getSizeOfAValue() {
        return doublePrecision ? 8 : 4;
    }

    protected ByteBuffer createNewBuffer() {
        ByteBuffer buffer_ = ByteBuffer.allocateDirect(getSizeOfAValue() * nbValues_);
        if (order_ != null) {
            buffer_.order(order_);
        }
        return buffer_;
    }

    protected ByteBuffer createBufferOneDouble() {
        ByteBuffer oneDouble = ByteBuffer.allocateDirect(getSizeOfAValue());
        if (order_ != null) {
            oneDouble.order(order_);
        }
        return oneDouble;
    }

    private FileChannel createNewChannel() throws IOException {
        return new FileInputStream(file_).getChannel();
    }

    /**
     * @param _nbV      le nombre de variable
     * @param _timeStep le pas de temps
     * @return les valeurs
     */
    public double[] getDouble(final int _nbV, final int _timeStep) throws IOException {
        if (!cacheGetDouble_.containsKey(_nbV)) {
            cacheGetDouble_.put(_nbV, Collections.synchronizedMap(new LRUCache<Integer, SoftReference<double[]>>(100)));
        }
        Map<Integer, SoftReference<double[]>> hashMap = cacheGetDouble_.get(_nbV);
        SoftReference<double[]> softReference = hashMap.get(_timeStep);
        double[] res = null;
        if (softReference != null) {
            res = softReference.get();
        }
        if (res == null) {
            if (hashMap.size() > 5) {
                hashMap.clear();
            }
            final ByteBuffer createNewBuffer = createNewBuffer();
            final FileChannel newChannel = createNewChannel();
            try {
                createNewBuffer.rewind();
                // on saute les pas de temps pour avoir celui demande
                long pos = firstTimeStepPos_ + _timeStep * timeEnrLength_;
                if (isColonne_) {
                    // [Seq][t][Seq]+([Seq]pt0,pt1,...[Seq])*nbV1+ [Seq]
                    // les 12 correspondent a la sequence pour le pas de temps
                    // ensuite chaque variables demande 2 entier (seq) +nbPoint valeurs
                    if (doublePrecision) {
                        pos += 16L + _nbV * (8L + nbValues_ * 8L) + 4L;
                    } else {
                        pos += 12L + _nbV * (8L + nbValues_ * 4L) + 4L;
                    }
                } else {
                    // [Seq][t][v0 in x0][vo in x1][vo in x2]... [Seq]
                    if (doublePrecision) {
                        pos += 4L + 8L + (_nbV * nbValues_ * 8L);
                    } else {
                        pos += 4L + 4L + (_nbV * nbValues_ * 4L);
                    }
                }
                res = new double[nbValues_];
                newChannel.position(pos);
                newChannel.read(createNewBuffer);
                newChannel.close();
                createNewBuffer.rewind();
                for (int i = 0; i < nbValues_; i++) {
                    res[i] = doublePrecision ? createNewBuffer.getDouble() : createNewBuffer.getFloat();
                }
                hashMap.put(_timeStep, new SoftReference<double[]>(res));
            } finally {
                newChannel.close();
            }
        }
        return res;
    }

    public double getDouble(final int _nbV, final int _timeStep, final int _idxPt) throws IOException {
        final FileChannel channel = createNewChannel();
        ByteBuffer oneDouble = createBufferOneDouble();
        try {
            long pos = firstTimeStepPos_ + _timeStep * timeEnrLength_;
            if (isColonne_) {
                if (doublePrecision) {
                    pos += 16L + _nbV * (8L + nbValues_ * 8L) + 4L;
                } else {
                    pos += 12L + _nbV * (8L + nbValues_ * 4L) + 4L;
                }
                // [Seq][t][Seq]+([Seq]pt0,pt1,...[Seq])*nbV1+ [Seq]
                // les 12 correspondent a la sequence pour le pas de temps
                // ensuite chaque variables demande 2 entier (seq) +nbPoint valeurs
            } else {
                // [Seq][t][v0 in x0][vo in x1][vo in x2]... [Seq]
                if (doublePrecision) {
                    pos += 4L + 8L + (_nbV * nbValues_ * 8L);
                } else {
                    pos += 4L + 4L + (_nbV * nbValues_ * 4L);
                }
            }
            if (doublePrecision) {
                pos += _idxPt * 8;
            } else {
                pos += _idxPt * 4;
            }
            channel.position(pos);
            oneDouble.rewind();
            channel.read(oneDouble);
            oneDouble.rewind();
        } finally {
            channel.close();
        }
        return doublePrecision ? oneDouble.getDouble() : oneDouble.getFloat();
    }

    public final long getFirstTimeStepPos() {
        return firstTimeStepPos_;
    }

    public final ByteOrder getOrder() {
        return order_;
    }

    public final void setOrder(final ByteOrder _order) {
        order_ = _order;
    }

    public final long getTimeEnrLength() {
        return timeEnrLength_;
    }

    public final void setTimeEnrLength(final int _timeEnrLength) {
        timeEnrLength_ = _timeEnrLength;
    }

    public int getTimeStepAvailable() throws IOException {
        final long size = file_.length();
        return (int) ((size - firstTimeStepPos_) / timeEnrLength_);
    }

    /**
     * @return the volumique
     */
    public boolean isVolumique() {
        return volumique_;
    }

    public final void setColonne(final boolean _isColonne) {
        isColonne_ = _isColonne;
    }
}
