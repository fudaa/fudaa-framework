/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.fudaa.ctulu.interpolation.SupportLocationI;
import org.fudaa.dodico.ef.triangulation.AbstractTriangulationPolyData;

/**
 * une implémentation par defaut avec les z comme attributs.
 *
 * @author Frederic Deniger
 */
public class TriangulationPolyDataNodeDefault extends AbstractTriangulationPolyData {

  SupportLocationI nodeData;

  public TriangulationPolyDataNodeDefault() {
  }

  public void setNodeData(SupportLocationI nodeData) {
    this.nodeData = nodeData;
  }

  @Override
  public int getPtsNb() {
    return nodeData == null ? 0 : nodeData.getPtsNb();
  }

  @Override
  public double getPtX(int idxPt) {
    return nodeData.getPtX(idxPt);
  }

  @Override
  public double getPtY(int idxPt) {
    return nodeData.getPtY(idxPt);
  }

  /**
   *
   * @param idxPt
   * @param idxAtt
   * @return 0
   */
  public double getPtAttributes(int idxPt, int idxAtt) {
    return 0;
  }
}
