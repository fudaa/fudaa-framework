/**
 * @creation 5 f�vr. 2004
 * @modification $Date: 2006-10-27 10:23:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import java.io.IOException;
import java.util.Map;

import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.io.EfIOResource;

/**
 * @author deniger
 * @version $Id: SerafinGridSourceAdapter.java,v 1.17 2006-10-27 10:23:30 deniger Exp $
 */
public class SerafinGridSourceAdapter implements SerafinInterface {

  long date_;
  String fond_;
  boolean fondInValue_;
  SerafinFileFormatVersionInterface ft_;
  int[] iparam_;
  EfGridSource source_;
  String titre_;

  // //TODO Voir si mettre les conditions limites ici ou retourner celle de source_
  // int[] boundaryConditions;
  /**
   * @param _s la source
   * @param _f la version
   * @param _fondTitle l'identifiant pour le fond.
   */
  public SerafinGridSourceAdapter(final EfGridSource _s, final SerafinFileFormatVersionInterface _f,
          final String _fondTitle) {
    source_ = _s;
    ft_ = _f;
    fond_ = _fondTitle;
    titre_ = EfIOResource.getS("maillage");
    iparam_ = new int[ft_.getParamNb()];
    setFormatColonne(true);
  }

  @Override
  public Map getOptions() {
    return source_.getOptions();
  }

  /**
   * @param _b true si format colonne demande
   */
  public void setFormatColonne(final boolean _b) {
    if (_b) {
      ft_.setFormatColonne(iparam_);
    } else {
      ft_.unsetFormatColonne(iparam_);
    }
  }

  @Override
  public boolean containsElementData() {
    return isVolumique();
  }

  @Override
  public boolean containsNodeData() {
    return !isVolumique();
  }

  public double getEltValue(final int _valueIdx, final int _eltIdx, final int _timeStep) {
    return 0;
  }

  public String getEltValueId(final int _valueIdx) {
    return null;
  }

  public int getEltValueNb() {
    return 0;
  }

  public FileFormat getFileFormat() {
    return ft_.getFileFormat();
  }

  @Override
  public EfGridInterface getGrid() {
    return source_.getGrid();
  }

  @Override
  public long getIdate() {
    return date_;
  }

  @Override
  public int getIdisc1() {
    return 0;
  }

  @Override
  public int[] getIparam() {
    return iparam_;
  }

  @Override
  public int[] getIpoboInitial() {
    if (source_.getGrid().getFrontiers() == null) {
      source_.getGrid().computeBord(null, null);
      if (source_.getGrid().getFrontiers() == null) {
        return null;
      }
    }
    return source_.getGrid().getFrontiers().getIpobo(source_.getGrid().getPtsNb());
  }

  @Override
  public int[] getIpoboFr() {
    if (source_.getGrid().getFrontiers() == null) {
      source_.getGrid().computeBord(null, null);
      if (source_.getGrid().getFrontiers() == null) {
        return null;
      }
    }
    return source_.getGrid().getFrontiers().getArray();
  }

  @Override
  public SerafinNewReaderInfo getReadingInfo() {
    return null;
  }

  @Override
  public double getTimeStep(final int _i) {
    return source_.getTimeStep(_i);
  }

  @Override
  public int getTimeStepNb() {
    return source_.getTimeStepNb();
  }

  @Override
  public String getTitre() {
    return titre_;
  }

  @Override
  public String getUnite(final int _i) {
    return ((!fondInValue_) && (_i == 0)) ? "m" : "?";
  }

  @Override
  public double getValue(final int _numVariable, final int _pasTemps, final int _ptIdx) throws IOException {
    return fondInValue_ ? (source_.getValue(_numVariable, _pasTemps, _ptIdx)) : ((_numVariable == 0) ? source_
            .getGrid().getPtZ(_ptIdx) : source_.getValue(_numVariable - 1, _pasTemps, _ptIdx));
  }

  @Override
  public boolean isVolumique() {
    return false;
  }

  @Override
  public boolean isXYdoublePrecision() {
    return SerafinFileFormat.isDoubleFormatOption(source_.getOptions());
  }

  @Override
  public String getValueId(final int _i) {
    return fondInValue_ ? source_.getValueId(_i) : (_i == 0 ? fond_ : source_.getValueId(_i - 1));
  }

  @Override
  public int getValueNb() {
    return fondInValue_ ? source_.getValueNb() : (source_.getValueNb() + 1);
  }

  @Override
  public boolean isElement(final int _idx) {
    return isVolumique();
  }

  /**
   * @param _time la date en sec.
   */
  public void setDate(final long _time) {
    ft_.setDateDefini(iparam_);
    date_ = _time;
  }

  /**
   * Initialise la date avec la date courante.
   */
  public void setDateCourante() {
    setDate(System.currentTimeMillis());
  }

  /**
   * Permet de specifier si le fond est deja present dans les variables nodales. Si oui, aucune variable ne sera ajoutee. Sinon, les z des points
   * seront ajoutes et seront constant.
   *
   * @param _b true si le fond est deja present dans les variables nodales.
   */
  public void setFondInValue(final boolean _b) {
    fondInValue_ = _b;

  }

  /**
   * @param _t le nouveau titre
   */
  public void setTitre(final String _t) {
    if (_t != null) {
      titre_ = _t;
    }
  }

  /**
   * permet de specifier la variable a utiliser pour le fond. Cela ne sert pas s'il a ete declare que le fond est deja dans une variable.
   *
   * @param _t l'identifiant du fond
   * @see #setFondInValue(boolean)
   */
  public void setVariableFond(final String _t) {
    if (_t != null) {
      fond_ = _t;
    }
  }

  /**
   * Annule la date.
   */
  public void unsetDate() {
    ft_.unsetDateDefini(iparam_);
  }

  @Override
  public ConditionLimiteEnum[] getBoundaryConditions() {
    return ConditionLimiteHelper.EMPTY_ARRAY;
  }
  // public void setBoundaryConditions(int[] boundaryConditions)
  // {
  // this.boundaryConditions = boundaryConditions;
  // }
}
