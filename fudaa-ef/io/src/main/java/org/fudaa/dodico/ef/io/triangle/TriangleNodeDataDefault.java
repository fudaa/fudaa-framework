/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

/**
 *
 * @author Frederic Deniger
 */
public class TriangleNodeDataDefault implements TriangleNodeDataInterface {

  private double[] x;
  private double[] y;
  private int[] boundaries;
  private double[][] attributes;
  int nbAttributes;
  boolean containsBoundaries;

  public TriangleNodeDataDefault(int nbPoint, int nbAttributes, boolean containsBoundaries) {
    x = new double[nbPoint];
    y = new double[nbPoint];
    this.nbAttributes = nbAttributes;
    this.containsBoundaries = containsBoundaries;
    boundaries = new int[containsBoundaries ? nbPoint : 0];
    if (nbAttributes == 0) {
      attributes = new double[0][0];
    } else {
      attributes = new double[nbPoint][nbAttributes];
    }
  }

  @Override
  public int getPtsNb() {
    return x.length;
  }

  @Override
  public double getPtX(int idxPt) {
    return x[idxPt];
  }

  @Override
  public double getPtY(int idxPt) {
    return y[idxPt];
  }

  @Override
  public int getNbAttributes() {
    return nbAttributes;
  }

  @Override
  public double getAttributes(int idxPt, int idxAtt) {
    if (nbAttributes > 0) {
      return attributes[idxPt][idxAtt];
    }
    return 0;
  }

  @Override
  public boolean containsBoundaryMarkers() {
    return containsBoundaries;
  }

  @Override
  public int getMarker(int idxPt) {
    return containsBoundaries ? boundaries[idxPt] : 0;
  }

  void setX(int i, double doubleField) {
    x[i] = doubleField;
  }

  void setY(int i, double doubleField) {
    y[i] = doubleField;
  }

  void setAttributes(int i, int k, double doubleField) {
    attributes[i][k] = doubleField;
  }

  void setMarker(int i, int intField) {
    boundaries[i] = intField;
  }
}
