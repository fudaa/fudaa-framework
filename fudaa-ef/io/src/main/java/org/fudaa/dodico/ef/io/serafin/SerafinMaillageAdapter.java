/**
 * @creation 8 ao�t 2003
 * @modification $Date: 2007-01-19 13:07:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;
import org.fudaa.dodico.ef.io.EfIOResource;

/**
 * @author deniger
 * @version $Id: SerafinMaillageAdapter.java,v 1.24 2007-01-19 13:07:22 deniger Exp $
 */
public class SerafinMaillageAdapter extends EfGridSourcesAbstract implements SerafinInterface {

  EfGridInterface maill_;
  String titre_;
  int[] iparam_;
  SerafinFileFormatVersionInterface version_;
  String fond_;
  long date_;
  double[] bottom_;
  String bottomFriction_;
  private boolean xYdoublePrecision;

  @Override
  public SerafinNewReaderInfo getReadingInfo() {
    return null;
  }

  @Override
  public boolean isXYdoublePrecision() {
    return xYdoublePrecision;
  }

  public void setxYdoublePrecision(boolean xYdoublePrecision) {
    this.xYdoublePrecision = xYdoublePrecision;
  }

  /**
   * Par defaut le format colonne est choisi.
   *
   * @param _ft la verison
   * @param _m le maillage support
   */
  public SerafinMaillageAdapter(final SerafinFileFormatVersionInterface _ft, final EfGridInterface _m) {
    maill_ = _m;
    version_ = _ft;
    titre_ = EfIOResource.getS("maillage");
    fond_ = version_.getDefaultVariableFond();
    iparam_ = new int[version_.getParamNb()];
    setFormatColonne(true);
  }

  /**
   * @param _bottomFriction la variable a utiliser pour le fond
   * @param _b les valeurs en chaque point.
   */
  public void setBottomFriction(final String _bottomFriction, final double[] _b) {
    bottomFriction_ = _bottomFriction;
    bottom_ = _b;
    if ((bottom_ == null) || (bottomFriction_ == null)) {
      new Throwable().printStackTrace();
    }
  }

  /**
   * @param _time nouvelle date
   */
  public void setDate(final long _time) {
    version_.setDateDefini(iparam_);
    date_ = _time;
  }

  /**
   * @param _t le titre
   */
  public void setTitre(final String _t) {
    if (_t != null) {
      titre_ = _t;
    }
  }

  /**
   * @param _t id de la variable a utiliser pour le fond.
   */
  public void setVariableFond(final String _t) {
    if (_t != null) {
      fond_ = _t;
    }
  }

  /**
   * Intialise la date avec la date courante.
   */
  public void setDateCourante() {
    setDate(System.currentTimeMillis());
  }

  /**
   * La date ne sera pas ajoutee au fichier (modifie iparam).
   */
  public void unsetDate() {
    version_.unsetDateDefini(iparam_);
  }

  /**
   * @param _b true si format colonne demande
   */
  public void setFormatColonne(final boolean _b) {
    if (_b) {
      version_.setFormatColonne(iparam_);
    } else {
      version_.unsetFormatColonne(iparam_);
    }
  }

  @Override
  public int getValueNb() {
    return bottom_ == null ? 1 : 2;
  }

  @Override
  public String getValueId(final int _i) {
    if (_i == 0) {
      return fond_;
    } else if ((bottom_ != null) && (_i == 1)) {
      return bottomFriction_;
    } else {
      return null;
    }
  }

  /**
   * @see org.fudaa.dodico.ef.io.serafin.SerafinInterface#getUnite(int)
   * @return M
   */
  @Override
  public String getUnite(final int _i) {
    if (_i == 0) {
      return "M";
    }
    return null;
  }

  @Override
  public String getTitre() {
    return titre_;
  }

  @Override
  public int[] getIparam() {
    return iparam_;
  }

  @Override
  public int getIdisc1() {
    return 1;
  }

  @Override
  public int[] getIpoboInitial() {
    if (maill_.getFrontiers() == null) {
      maill_.computeBord(null, null);
    }

    return maill_.getFrontiers().getIpobo(maill_.getPtsNb());
  }

  @Override
  public int[] getIpoboFr() {
    if (maill_.getFrontiers() == null) {
      maill_.computeBord(null, null);
    }
    return maill_.getFrontiers().getArray();
  }

  @Override
  public long getIdate() {
    return date_;
  }

  @Override
  public EfGridInterface getGrid() {
    return maill_;
  }

  /**
   * Ne renvoie que les donnees pour le fond ou pour la friction si definie.
   *
   * @param _numVariable le numero de la variable
   * @param _i l'index du point
   * @param _pasTemps le pas de temps (doit etre = � 0)
   */
  @Override
  public double getValue(final int _numVariable, final int _pasTemps, final int _i) {
    if (_pasTemps != 0) {
      return 0;
    }
    if (_numVariable == 0) {
      return maill_.getPtZ(_i);
    } else if ((bottom_ != null) && (_numVariable == 1)) {
      return bottom_[_i];
    } else {
      return 0;
    }
  }

  @Override
  public boolean isVolumique() {
    return false;
  }

  /**
   *
   */
  @Override
  public int getTimeStepNb() {
    return 1;
  }

  /**
   *
   */
  @Override
  public double getTimeStep(final int _i) {
    return 0;
  }

  /**
   *
   */
  public FileFormat getFileFormat() {
    return SerafinFileFormat.getInstance();
  }
}