/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.gmsh;

import org.locationtech.jts.geom.LineString;
import org.fudaa.dodico.ef.io.triangle.*;
import org.fudaa.dodico.ef.triangulation.TriangulationPolyDataInterface;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.ef.triangulation.TriangulationConvexHullBuilder;
import org.fudaa.dodico.ef.triangulation.TriangulationUniqueDataContent;
import org.fudaa.dodico.ef.triangulation.TriangulationUniqueDataContentBuilder;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;

/**
 * Permet d'�crire un fichier d'entr�e pour GMSH. Par contre, le mailleur ne semble pas extr�menent robuste. Lors de l'�criture du fichier geo, on
 * teste qu'il y ait au plus un polygone externe. Si plus erreur. si 0, on cr�e l'enveloppe convexe. http://geuz.org/gmsh/doc/texinfo/gmsh.html La
 * pr�cision par d�faut est tres grande. Pour la modifier, il faut appeler setPrec(): setPrec(1) par exemple. Voir la doc de gmsh pour plus
 * d'information.
 *
 * @author Frederic Deniger
 */
public class GmshGeoWriter extends FileOpWriterCharSimpleAbstract {

  private double eps = 1e-3;

  public double getEps() {
    return eps;
  }

  public void setEps(double eps) {
    this.eps = eps;
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof TriangulationPolyDataInterface) {
      try {
        writePoly((TriangulationPolyDataInterface) _o);
      } catch (IOException ex) {
        Logger.getLogger(TrianglePolyWriter.class.getName()).log(Level.SEVERE, null, ex);
        analyze_.manageException(ex);
      }
    } else {
      analyze_.addFatalError(DodicoLib.getS("Donn�es invalides"));
    }
  }
  /**
   * Voir http://geuz.org/gmsh/doc/texinfo/gmsh.html.
   */
  private String prec = "4";//pr�cision trop grande pour les maillages...
//  private String prec = "1";

  public void setPrec(String prec) {
    this.prec = prec;
  }

  private void writePoly(TriangulationPolyDataInterface initData) throws IOException {
    TriangulationPolyDataInterface data = initData;
    if (data == null) {
      analyze_.addFatalError(EfIOResource.getS("Les donn�es sont nulles"));
      return;
    }
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    TriangulationConvexHullBuilder convexHullBuilder = new TriangulationConvexHullBuilder();
    int nbExtern = convexHullBuilder.getNbExternPolygon(data);
    if (nbExtern > 1) {
      analyze_.addFatalError(EfIOResource.getS("Une seule enveloppe externe doit �tre d�finie"));
    }
    if (nbExtern == 0) {
      data = convexHullBuilder.addConvexHullIfNeeded(data);
    }
    int nbPoly = data.getNbLines();
    TriangulationUniqueDataContentBuilder builder = new TriangulationUniqueDataContentBuilder(data, eps);
    TriangulationUniqueDataContent content = builder.build(progress_);

    int nbPointsTotal = content.getNbPoints();
    ProgressionUpdater updater = new ProgressionUpdater(progress_);
    updater.setValue(10, nbPointsTotal);
    updater.majProgessionStateOnly(EfIOResource.getS("Ecriture des points"));

    //ensuite 
    //Point(1) = {0.75, -0.36, 0, 1.0};
    //contient les points non inclus dans les lignes
    TIntHashSet pointInSurface = new TIntHashSet();
    DecimalFormat fmt = CtuluLib.getDecimalFormat(3);
    for (int i = 0; i < nbPointsTotal; i++) {
      writeString("Point(");
      writeInt(i + 1);
      pointInSurface.add(i + 1);
      writeString(") = {");
      writeString(fmt.format(content.getX(i)));
      writeString(",");
      writeString(fmt.format(content.getY(i)));
      writeString(", 0, ");
      writeString(prec);
      writeString("};");
      writelnToOut();
      updater.majAvancement();
    }
    int nbSegment = 0;
    for (int idxPoly = 0; idxPoly < nbPoly; idxPoly++) {
      LineString poly = data.getLine(idxPoly);
      //premier point = dernier point donc -1
      int numPoints = poly.getNumPoints() - 1;
      nbSegment = nbSegment + numPoints;
    }
    //One line: <# of segments> <# of boundary markers (0 or 1)> 
    //calcul du nombre de segment.
    updater.setValue(10, nbSegment);
    updater.majProgessionStateOnly(EfIOResource.getS("Ecriture des segments"));
    //Line(1) = {22, 21};
    int globalIdxSeg = 1;
    //contient les indices des lignes par surfaces
    TIntArrayList[] segmentBySurface = new TIntArrayList[nbPoly];
    for (int idxPoly = 0; idxPoly < nbPoly; idxPoly++) {
      segmentBySurface[idxPoly] = new TIntArrayList();
      LineString poly = data.getLine(idxPoly);
      int numPoints = data.isClosed(idxPoly) ? poly.getNumPoints() - 2 : poly.getNumPoints() - 1;//le dernier segment est �crit apr�s
      for (int idxPt = 0; idxPt < numPoints; idxPt++) {
        writeString("Line(");
        segmentBySurface[idxPoly].add(globalIdxSeg);
        writeInt(globalIdxSeg++);
        writeString(") = {");
        final int idxVertex = content.getPtIdxFor(idxPoly, idxPt) + 1;
        writeInt(idxVertex);
        writeString(",");
        final int idxVertexNext = content.getPtIdxFor(idxPoly, idxPt + 1) + 1;
        pointInSurface.remove(idxVertex);
        pointInSurface.remove(idxVertexNext);
        writeInt(idxVertexNext);
        writeString("};");
        writelnToOut();
        updater.majAvancement();
      }
      if (data.isClosed(idxPoly)) {
        //le segment fermant
        writeString("Line(");
        segmentBySurface[idxPoly].add(globalIdxSeg);
        writeInt(globalIdxSeg++);
        writeString(") = {");
        final int idxVertex = content.getPtIdxFor(idxPoly, numPoints) + 1;
        writeInt(idxVertex);
        writeString(",");
        final int idxVertexNext = content.getPtIdxFor(idxPoly, 0) + 1;
        writeInt(idxVertexNext);
        pointInSurface.remove(idxVertex);
        pointInSurface.remove(idxVertexNext);
        writeString("};");
        writelnToOut();
        updater.majAvancement();
      }
    }
    int idxLineExterne = -1;
//    Line Loop(19) = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 1};
    int firstLineLoopIdx = globalIdxSeg;
    for (int idxPoly = 0; idxPoly < nbPoly; idxPoly++) {
      if (!data.isClosed(idxPoly)) {
        continue;
      }
      if (!data.isClosedAndHole(idxPoly)) {
        idxLineExterne = globalIdxSeg;
      }
      final TIntArrayList lineLoop = segmentBySurface[idxPoly];
      writeString("Line Loop(");
      writeInt(globalIdxSeg++);
      writeString(") = {");
      for (int segIdx = 0; segIdx < lineLoop.size(); segIdx++) {
        if (segIdx > 0) {
          writeString(", ");
        }
        writeInt(lineLoop.get(segIdx));
      }
      writeString("};");
      writelnToOut();
    }
    //Plane Surface(21) = {19, 20};
    writeString("Plane Surface(");
    writeInt(globalIdxSeg++);
    writeString(") = {");
    //la surface externe
    writeInt(idxLineExterne);
    //les trous
    if (nbPoly > 1) {
      for (int idxLoop = firstLineLoopIdx; idxLoop < globalIdxSeg; idxLoop++) {
        if (idxLoop != idxLineExterne) {
          writeString(", ");
          writeInt(idxLoop);
        }
      }
    }
    writeString("};");
    writelnToOut();
//Point{2, 3...., 18} In Surface{21};
    writeString("Point{");
    int[] toArray = pointInSurface.toArray();
    int size = toArray.length;
    for (int idxPt = 0; idxPt < size; idxPt++) {
      if (idxPt > 0) {
        writeString(", ");
      }
      writeInt(toArray[idxPt]);
    }
    writeString("} In Surface{");
    writeInt(globalIdxSeg - 1);
    writeString("};");
  }
}
