/**
 * @creation 8 ao�t 2003
 * @modification $Date: 2007-01-10 09:04:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.serafin;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;

import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 * @version $Id: SerafinMaillageBuilderAdapter.java,v 1.11 2007-01-10 09:04:27 deniger Exp $
 */
public class SerafinMaillageBuilderAdapter extends SerafinMaillageBuilderAdapterAbstract {

  private double firstTimeStep_;
  private CtuluCollectionDouble[] values_;
  private boolean xYdoublePrecision = true;

  /**
   * Par defaut le format colonne est choisi.
   *
   * @param _ft la verison
   * @param _m le maillage support
   */
  public SerafinMaillageBuilderAdapter(final SerafinFileFormatVersionInterface _ft, final EfGridInterface _m) {
    super(_ft, _m);
  }

  public void setxYdoublePrecision(boolean xYdoublePrecision) {
    this.xYdoublePrecision = xYdoublePrecision;
  }

  public final double getFirstTimeStep() {
    return firstTimeStep_;
  }

  @Override
  public double getTimeStep(final int _i) {
    return firstTimeStep_;
  }

  /**
   *
   */
  @Override
  public int getTimeStepNb() {
    return 1;
  }

  /**
   * Ne renvoie que les donnees pour le fond ou pour la friction si definie.
   *
   * @param _numVariable le numero de la variable
   * @param _i l'index du point
   * @param _pasTemps le pas de temps (doit etre = � 0)
   */
  @Override
  public double getValue(final int _numVariable, final int _pasTemps, final int _i) {
    if (_pasTemps != 0) {
      return 0;
    }
    return values_[_numVariable].getValue(_i);
  }

  @Override
  public String getValueId(final int _i) {
    return valuesName_[_i];
  }

  @Override
  public int getValueNb() {
    return values_ == null ? 0 : values_.length;
  }

  @Override
  public boolean isVolumique() {
    return isVolumique_;
  }

  @Override
  public boolean isXYdoublePrecision() {
    return xYdoublePrecision;
  }

  public final void setFirstTimeStep(final double _firstTimeStep) {
    firstTimeStep_ = _firstTimeStep;
  }
  boolean isVolumique_;

  /**
   * @param _title les noms des valeurs a ajoutees
   * @param _values les valeurs en chaque point
   * @param _units les unites
   */
  public void setValues(final String[] _title, final CtuluCollectionDouble[] _values, final String[] _units) {
    valuesName_ = _title;
    values_ = _values;
    units_ = _units;
    isVolumique_ = _values != null && _values[0] != null && _values[0].getSize() == getGrid().getEltNb();
  }

  @Override
  public ConditionLimiteEnum[] getBoundaryConditions() {
    return ConditionLimiteHelper.EMPTY_ARRAY;
  }
}