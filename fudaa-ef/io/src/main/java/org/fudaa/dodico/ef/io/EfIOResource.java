/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.dodico.ef.io;

import com.memoire.bu.BuResource;

import org.fudaa.dodico.commun.DodicoResource;

/**
 * @author deniger
 */
public class EfIOResource extends DodicoResource {

  public final static EfIOResource EFIO = new EfIOResource(DodicoResource.DODICO);

  public static String getS(final String _s) {
    return EFIO.getString(_s);
  }

  public static String getS(final String _s, final String _v0) {
    return EFIO.getString(_s, _v0);
  }

  public static String getS(final String _s, final String _v0, final String _v1) {
    return EFIO.getString(_s, _v0, _v1);
  }

  private EfIOResource(final BuResource _parent) {
    super(_parent);
  }
}
