/*
 *  @creation     20 f�vr. 2003
 *  @modification $Date: 2006-04-14 14:51:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.ef.io.dunes;

import org.fudaa.dodico.ef.EfGridSource;

/**
 * @author deniger
 * @version $Id: DunesInterface.java,v 1.12 2006-04-14 14:51:47 deniger Exp $
 */
public interface DunesInterface extends EfGridSource {
  /**
   * Des valeurs sont ajoutes a chaque noeud pour un maillage adaptatif.
   *
   * @return null si non adaptatif
   */
  double[] getAdaptatifValeur();
}
