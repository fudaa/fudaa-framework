/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.ef.io.EfIOResource;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;

/**
 *Permet de lire les fichiers de type node: https://www.cs.cmu.edu/~quake/triangle.node.html
 * @author Frederic Deniger
 */
public class TriangleNodeReader extends FileCharSimpleReaderAbstract<TriangleNodeDataInterface> {

  @Override
  protected TriangleNodeDataInterface internalRead() {
    in_.setJumpBlankLine(true);
    in_.setJumpCommentLine(true);
    in_.setCommentInOneField("#");
    TriangleNodeDataDefault res = null;
    try {
      in_.readFields();
      // First line: <# of vertices> <dimension (must be 2)> <# of attributes> <# of boundary markers (0 or 1)> 
      if (in_.getNumberOfFields() != 4) {
        analyze_.addSevereError(EfIOResource.getS("Le format du fichier node est incorrect:la première ligne comporte {0} champs au lieu de 4", Integer.toString(in_.getNumberOfFields())), in_);
        return null;
      }
      int nbPts = in_.intField(0);
      ProgressionUpdater updater = new ProgressionUpdater(progress_);
      updater.setValue(20, nbPts);
      //la colonne 1 doit contenir 2 et n'est pas intéressantes ici...
      int nbAttributes = in_.intField(2);
      boolean containsMarkers = in_.intField(3) == 1;
      res = new TriangleNodeDataDefault(nbPts, nbAttributes, containsMarkers);
      //<vertex #> <x> <y> [attributes] [boundary marker] 
      for (int i = 0; i < nbPts; i++) {
        in_.readFields();
        final int numberOfFields = in_.getNumberOfFields();
        int nbExpected = 3 + nbAttributes + (containsMarkers ? 1 : 0);
        if (numberOfFields != nbExpected) {
          analyze_.addSevereError(EfIOResource.getS("Le format du fichier node est incorrect"), in_);
          return null;
        }
        res.setX(i, in_.doubleField(1));
        res.setY(i, in_.doubleField(2));
        if (nbAttributes > 0) {
          for (int k = 0; k < nbAttributes; k++) {
            res.setAttributes(i, k, in_.doubleField(3 + k));
          }
        }
        if (containsMarkers) {
          res.setMarker(i, in_.intField(3 + nbAttributes));
        }
        updater.majAvancement();
      }
    } catch (IOException ex) {
      Logger.getLogger(TriangleNodeReader.class.getName()).log(Level.SEVERE, null, ex);
    }
    return res;
  }
}
