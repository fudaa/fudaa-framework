package org.fudaa.dodico.ef.io.serafin;

import java.io.File;

import com.memoire.bu.BuFileFilter;

/**
 * Filter fichier volumes finis.
 * @author Adrien Hadoux
 *
 */
public class SerafinVolumeFileFilter extends BuFileFilter {

	  /**
	   * @param _ft le format serafin
	   */
	  public SerafinVolumeFileFilter(final SerafinVolumeFileFormat _ft) {
	    super(_ft.getExtensions(), _ft.getName());
	  }
	  boolean strict_;

	  public SerafinVolumeFileFilter(final SerafinVolumeFileFormat _ft, final boolean _strict) {
	    super( _ft.getExtensions(), _ft.getName());
	    strict_ = _strict;
	  }

  @Override
	  public boolean accept(final File _d, final String _fn) {
	    if (_fn == null) {
	      return false;
	    }
	    boolean res = super.accept(_d, _fn);
	    if (strict_) {
	      return res;
	    }
	    if (!res) {
	      res = isOk(_fn);
	    }
	    return res;
	  }

	  private boolean isOk(final String _name) {
	    if ((_name.indexOf('.') < 0)) {
	      return _name.startsWith("volafin") || _name.startsWith("volfin") || _name.endsWith("vol")|| _name.endsWith("volume");
	    }
	    return false;
	  }

  @Override
	  public boolean accept(final File _f) {
	    if (_f == null) {
	      return false;
	    }
	    final String name = _f.getName();
	    if (name == null) {
	      return false;
	    }
	    boolean res = super.accept(_f);
	    if (!res) {
	      res = isOk(name);
	    }
	    return res;
	  }

	  /**
	   * @param _name le nom du fichier a tester
	   * @return true si c'est un fichier r�sultat
	   */
	  public boolean isResFile(final String _name) {
	    return _name.toLowerCase().endsWith(".vol")|| _name.endsWith(".volfin")|| _name.toLowerCase().endsWith(".volafin")|| _name.toLowerCase().endsWith(".volume");
	  }

	  /**
	   * @param _f le fichier a tester
	   * @return true si c'est un fichier r�sultat
	   */
	  public boolean isResFile(final File _f) {
	    return _f.isFile() && isResFile(_f.getName());
	  }
	}