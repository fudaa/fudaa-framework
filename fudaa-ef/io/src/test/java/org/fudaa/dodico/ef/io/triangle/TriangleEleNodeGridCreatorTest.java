/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import java.io.File;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.impl.EFGridArrayZ;

/**
 *
 * @author Frederic Deniger
 */
public class TriangleEleNodeGridCreatorTest extends TestIO {

  public TriangleEleNodeGridCreatorTest() {
    super(null);
  }

  public void testReadGrid() {
    File eleFile = getFile("grid.ele");
    File nodeFile = getFile("grid.node");
    assertNotNull(eleFile);
    assertNotNull(nodeFile);
    TriangleEleNodeGridCreator creator = new TriangleEleNodeGridCreator();
    CtuluIOResult<EFGridArrayZ> readGrid = creator.readGrid(nodeFile, eleFile);
    final EFGridArrayZ grid = readGrid.getSource();
    assertNotNull(grid);
    assertEquals(1775, grid.getPtsNb());
    assertEquals(3386, grid.getEltNb());
    // 1    753  437  10    1
    assertDoubleEquals(753, grid.getPtX(0));
    assertDoubleEquals(437, grid.getPtY(0));
    assertDoubleEquals(10, grid.getPtZ(0));
    //1      82  1600    81
    EfElement element = grid.getElement(0);
    assertEquals(3, element.getPtNb());
    assertEquals(81, element.getPtIndex(0));
    assertEquals(1599, element.getPtIndex(1));
    assertEquals(80, element.getPtIndex(2));
  }
}
