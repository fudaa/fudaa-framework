package org.fudaa.dodico.ef.io.serafin;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfGridInterface;

import java.io.File;
import java.io.IOException;

public class TestSerafinReaderWriter extends TestIO {
  public TestSerafinReaderWriter() {
    super("init.slf");
    assertNotNull(fic_);
    assertTrue(fic_.exists());
  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();
  }

  public void testReadSerafinD() throws IOException {
    File target = createTempFile(".res");
    File serafinD = CtuluLibFile.getFileFromJar("/org/fudaa/dodico/ef/io/serafin/seraphinD.res", target);
    SerafinNewReader reader = new SerafinNewReader();
    reader.setFile(serafinD);
    CtuluIOOperationSynthese read = reader.read();
    assertFalse(read.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter adapter = (SerafinAdapter) read.getSource();
    assertTrue(adapter.isXYdoublePrecision());
    String titre = adapter.getTitre();
    assertEquals("TELEMAC 2D : GOUTTE D'EAU DANS UN BASSIN$                               SERAFIND", titre);
    EfGridInterface grid = adapter.getGrid();
    testGrid(grid);
  }

  public void testWriteSerafinD() throws IOException {
    File target = createTempFile(".res");
    File serafinD = CtuluLibFile.getFileFromJar("/org/fudaa/dodico/ef/io/serafin/seraphinD.res", target);
    SerafinNewReader reader = new SerafinNewReader();
    reader.setFile(serafinD);
    CtuluIOOperationSynthese initialIO = reader.read();
    assertFalse(initialIO.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter initialRes = (SerafinAdapter) initialIO.getSource();
    SerafinWriter writer = new SerafinWriter(SerafinFileFormat.getInstance());
    File writtenFile = createTempFile(".res");
    writer.setFile(writtenFile);
    writer.write(initialRes);
    reader.setFile(writtenFile);
    CtuluIOOperationSynthese transformedIO = reader.read();
    assertFalse(transformedIO.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter transformedRes = (SerafinAdapter) transformedIO.getSource();
    assertTrue(transformedRes.isXYdoublePrecision());
    assertEquals("TELEMAC 2D : GOUTTE D'EAU DANS UN BASSIN$                               SERAFIND", transformedRes.getTitre());
    testGrid(transformedRes.getGrid());
  }

  /**
   * test that the title is updated.
   */
  public void testWriteSerafinDTitle() throws IOException {
    File target = createTempFile(".res");
    File serafinD = CtuluLibFile.getFileFromJar("/org/fudaa/dodico/ef/io/serafin/seraphinD.res", target);
    SerafinNewReader reader = new SerafinNewReader();
    reader.setFile(serafinD);
    CtuluIOOperationSynthese initialIO = reader.read();
    assertFalse(initialIO.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter initialRes = (SerafinAdapter) initialIO.getSource();
    initialRes.setTitre("Without D");
    SerafinWriter writer = new SerafinWriter(SerafinFileFormat.getInstance());
    File writtenFile = createTempFile(".res");
    writer.setFile(writtenFile);
    writer.write(initialRes);
    reader.setFile(writtenFile);
    CtuluIOOperationSynthese transformedIO = reader.read();
    assertFalse(transformedIO.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter transformedRes = (SerafinAdapter) transformedIO.getSource();
    assertTrue(transformedRes.isXYdoublePrecision());
    assertEquals("Without D                                                                      D", transformedRes.getTitre());
    testGrid(transformedRes.getGrid());
  }

  public void testWriteSerafinDNotDoubleTitle() throws IOException {
    File target = createTempFile(".res");
    File serafinD = CtuluLibFile.getFileFromJar("/org/fudaa/dodico/ef/io/serafin/seraphinD.res", target);
    SerafinNewReader reader = new SerafinNewReader();
    reader.setFile(serafinD);
    CtuluIOOperationSynthese initialIO = reader.read();
    assertFalse(initialIO.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter initialRes = (SerafinAdapter) initialIO.getSource();
    //point important
    initialRes.setXYdoublePrecision(false);
    SerafinWriter writer = new SerafinWriter(SerafinFileFormat.getInstance());
    File writtenFile = createTempFile(".res");
    writer.setFile(writtenFile);
    writer.write(initialRes);
    reader.setFile(writtenFile);
    CtuluIOOperationSynthese transformedIO = reader.read();
    assertFalse(transformedIO.getAnalyze().containsErrorOrFatalError());
    SerafinAdapter transformedRes = (SerafinAdapter) transformedIO.getSource();
    assertFalse(transformedRes.isXYdoublePrecision());
    assertEquals("TELEMAC 2D : GOUTTE D'EAU DANS UN BASSIN$                               SERAFIN", transformedRes.getTitre());
    testGrid(transformedRes.getGrid());
  }

  public void testReadQuadri() {
    SerafinNewReader reader = new SerafinNewReader();
    reader.setFile(fic_);
    CtuluIOOperationSynthese read = reader.read();
    SerafinAdapter res = (SerafinAdapter) read.getSource();
    EfGridInterface grid = res.getGrid();
    assertDoubleEquals(473499.40625, grid.getPtX(10724));
    assertDoubleEquals(-95020, grid.getPtY(10724));
    int[] iparamInit = res.getIparam();
    int[] ipoboFrInit = res.getIpoboFr();
    int[] ipoboInit = res.getIpoboInitial();

    SerafinFileFormat fileFormat = SerafinFileFormat.getInstance();
    SerafinWriter writer = new SerafinWriter(fileFormat);
    File tmp = createTempFile();
    writer.setFile(tmp);
    writer.write(res);

    reader.setFile(tmp);
    CtuluIOOperationSynthese readAfterWrite = reader.read();
    SerafinAdapter resAfterWrite = (SerafinAdapter) readAfterWrite.getSource();

    compareTwoIntArray(iparamInit, resAfterWrite.getIparam());
    compareTwoIntArray(ipoboFrInit, resAfterWrite.getIpoboFr());
    compareTwoIntArray(ipoboInit, resAfterWrite.getIpoboInitial());

    EfGridInterface gridAfterWrite = resAfterWrite.getGrid();

    assertEquals(grid.getEltNb(), gridAfterWrite.getEltNb());
    for (int i = 0; i < grid.getEltNb(); i++) {
      assertTrue("Test for mesh " + i, grid.getElement(i).isSameStrict(gridAfterWrite.getElement(i)));
    }
    assertEquals(grid.getPtsNb(), gridAfterWrite.getPtsNb());
    for (int i = 0; i < grid.getPtsNb(); i++) {
      assertEquals("Test for x " + i, grid.getPtX(i), gridAfterWrite.getPtX(i), 1e-10);
    }
    for (int i = 0; i < grid.getPtsNb(); i++) {
      assertEquals("Test for y " + i, grid.getPtY(i), gridAfterWrite.getPtY(i), 1e-10);
    }
    //    assertDoubleEquals(473499.40625, gridAfterWrite.getPtX(10724));
    //    assertDoubleEquals(-95020, gridAfterWrite.getPtY(10724));
  }

  public void compareTwoIntArray(int[] a, int[] b) {
    assertEquals(a.length, b.length);
    for (int i = 0; i < b.length; i++) {
      assertEquals("test de pour i=" + i, a[i], b[i]);
    }
  }

  private void testGrid(EfGridInterface grid) {
    int nb = grid.getPtsNb();
    assertEquals(4624, nb);
    assertDoubleEquals(0, grid.getPtX(0));
    assertEquals(19.80000305, grid.getPtX(1), 1e-6);
    assertEquals(20.10000610, grid.getPtX(2), 1e-6);
    assertEquals(0.3, grid.getPtX(nb - 1), 1e-6);

    assertEquals(0, grid.getPtY(0), 1e-6);
    assertEquals(0, grid.getPtY(1), 1e-6);
    assertEquals(0, grid.getPtY(2), 1e-6);
    assertEquals(19.79998779, grid.getPtY(nb - 1), 1e-6);
  }

  public static void main(String[] args) {
    File file = new File(args[0]);
    System.out.println("Try to read " + file.getAbsolutePath());
    SerafinNewReader reader = new SerafinNewReader();
    reader.setFile(file);
    CtuluIOOperationSynthese initialIO = reader.read();
    initialIO.getAnalyze().printResume();
  }
}
