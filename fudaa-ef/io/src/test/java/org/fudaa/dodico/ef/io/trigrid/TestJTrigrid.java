/*
 *  @file         TestTrigrid.java
 *  @creation     27 janv. 2004
 *  @modification $Date: 2007-01-19 13:07:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.ef.io.trigrid;

import java.io.File;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfLib;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;

/**
 * @author deniger
 * @version $Id: TestJTrigrid.java,v 1.1 2007-01-19 13:07:17 deniger Exp $
 */
public class TestJTrigrid extends TestIO {

  EfGridInterface g_;

  /**
   * trigrid.ngh.
   */
  public TestJTrigrid() {
    super("trigrid.ngh");
    assertNotNull(fic_);
    assertTrue(fic_.exists());
    g_ = ((EfGridSource) TrigridFileFormat.getInstance().readGrid(fic_, null).getSource()).getGrid();
  }

  /**
   * Test Ecriture.
   */
  public void testEcriture() {
    final File f = createTempFile();
    TrigridFileFormat.getInstance().write(f, g_, null);
    final EfGridInterface g = ((EfGridSource) TrigridFileFormat.getInstance().readGrid(f, null).getSource()).getGrid();
    f.delete();
    internTestGridWriter(g);
  }

  /**
   * Un test assez long sur le lecture de fichier serafin. Les tests:<br>
   * 1- test de la conformite du maillage lu <br>
   * 2- Comparaison avec un maillage equivalent issu du fichier serafin "trigrid.geo" 3- Test pour retrouver les
   * connexions
   */
  public void testLecture() {
    //TODO Faire le test pour les conditions limites.
    assertNotNull(g_);
    internTestGrid(g_);
    final File f = getFile("trigrid.geo");
    final EfGridInterface g2 = ((EfGridSource) SerafinFileFormat.getInstance().readGrid(f, null).getSource()).getGrid();
    assertNotNull(g2);
    assertTrue(g2.isEquivalent(g_, false, 1e-3d));
    g_.computeBord(null,null);
    g2.computeBord(null,null);
    System.out.println("Nouvelles version de Trigrid ??????????????????");
    g_.getFrontiers().isSame(g2.getFrontiers().getArray());
    internTestConnectionFound(g_);
  }

  private void internTestConnectionFound(final EfGridInterface _g) {
    final CtuluAnalyze a = new CtuluAnalyze();
    final int[][] r = EfLib.getConnexionFor(_g, null, a);
    assertTrue(a.isEmpty());
    assertEquals(_g.getPtsNb(), r.length);
    // test de la ligne suivante
    // 1 6813.839 7844.116 1 1.078 1534 3684 1547 0 0 0 0 0
    int[] rToTest = r[0];
    assertEquals(3, rToTest.length);
    assertTrue(CtuluLibArray.findInt(rToTest, 1533) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 3683) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 1546) >= 0);
    // 3214 6567.800 6187.305 0 1.245 3084 2247 2443 3102 2990 2432 0 0
    rToTest = r[3213];
    assertEquals(6, rToTest.length);
    assertTrue(CtuluLibArray.findInt(rToTest, 3083) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 2246) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 2442) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 3101) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 2989) >= 0);
    assertTrue(CtuluLibArray.findInt(rToTest, 2431) >= 0);
  }

  private void internTestGrid(final EfGridInterface _g) {
    assertEquals(3724, _g.getPtsNb());
    // Attention les index sont decales
    assertDoubleEquals(6813.839, _g.getPtX(0));
    assertDoubleEquals(7844.116, _g.getPtY(0));
    assertDoubleEquals(1.078, _g.getPtZ(0));
    assertDoubleEquals(5104.207, _g.getPtX(1790));
    assertDoubleEquals(5580.120, _g.getPtY(1790));
    assertDoubleEquals(-0.372, _g.getPtZ(1790));
    // le dernier point
    assertDoubleEquals(4987.752, _g.getPtX(3723));
    assertDoubleEquals(5484.865, _g.getPtY(3723));
    assertDoubleEquals(-0.182, _g.getPtZ(3723));
    assertEquals(5788, _g.getEltNb());
    EfElement el = _g.getElement(0);
    assertTrue(el.containsIndex(0));
    assertTrue(el.containsIndex(1533));
    assertTrue(el.containsIndex(3683));
    el = _g.getElement(44);
    assertTrue(el.containsIndex(23));
    assertTrue(el.containsIndex(1718));
    assertTrue(el.containsIndex(1592));
    el = _g.getElement(132);
    assertTrue(el.containsIndex(64));
    assertTrue(el.containsIndex(904));
    assertTrue(el.containsIndex(1941));
    el = _g.getElement(5787);
    assertTrue(el.containsIndex(3633));
    assertTrue(el.containsIndex(3681));
    assertTrue(el.containsIndex(3625));
  }

  private void internTestGridWriter(final EfGridInterface _g) {
    assertEquals(3724, _g.getPtsNb());
    // Attention les index sont decales
    assertDoubleEquals(6813.839, _g.getPtX(0));
    assertDoubleEquals(7844.116, _g.getPtY(0));
    assertDoubleEquals(1.078, _g.getPtZ(0));
    assertDoubleEquals(5104.207, _g.getPtX(1790));
    assertDoubleEquals(5580.120, _g.getPtY(1790));
    assertDoubleEquals(-0.372, _g.getPtZ(1790));
    // le dernier point
    assertDoubleEquals(4987.752, _g.getPtX(3723));
    assertDoubleEquals(5484.865, _g.getPtY(3723));
    assertDoubleEquals(-0.182, _g.getPtZ(3723));
    assertEquals(5788, _g.getEltNb());
    EfElement el = new EfElement(new int[] { 0, 1533, 3683 });
    assertTrue(_g.isEltPresentInGrid(el));
    el = new EfElement(new int[] { 23, 1718, 1592 });
    assertTrue(_g.isEltPresentInGrid(el));
    el = new EfElement(new int[] { 64, 904, 1941 });
    assertTrue(_g.isEltPresentInGrid(el));
    el = new EfElement(new int[] { 3633, 3681, 3625 });
    assertTrue(_g.isEltPresentInGrid(el));
  }
}
