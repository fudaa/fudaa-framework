/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.fudaa.dodico.ef.triangulation.TriangulationPolyDataDefault;
import org.fudaa.dodico.ef.triangulation.TriangulationPolyDataInterface;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.dodico.common.TestIO;

/**
 *
 * @author Frederic Deniger
 */
public class TrianglePolyWriterTest extends TestIO {

  public TrianglePolyWriterTest() {
    super("exemple.poly");
  }

  private static TriangulationPolyDataInterface createDefault() {
    Coordinate[] cs = new Coordinate[3];
    cs[0] = new Coordinate(3.5, 4, 10);
    cs[1] = new Coordinate(4.5, 4, 10);
    cs[2] = new Coordinate(5.5, 4, 10);
    LineString[] rings = new LineString[3];
    rings[0] = GISGeometryFactory.INSTANCE.createLinearRing(2, 3, 4, 5);
    setZ(rings[0], 20);
    rings[1] = GISGeometryFactory.INSTANCE.createLinearRing(1, 10, 1, 20);
    setZ(rings[1], 30);
    rings[2] = GISGeometryFactory.INSTANCE.createLinearRing(7, 9, 6, 8);
    setZ(rings[2], 40);
    boolean[] holes = new boolean[3];
    holes[0] = true;
    holes[2] = true;
    TriangulationPolyDataDefault res = new TriangulationPolyDataDefault();
    res.setUseZAsAttribute(true);
    res.setCoordinates(cs);
    res.setHoles(holes);
    res.setLineString(rings);
    return res;
  }

  private static void setZ(LineString ring, double z) {
    for (int i = 0; i < ring.getNumPoints(); i++) {
      ring.getCoordinateSequence().setOrdinate(i, 2, z);
    }
  }

  public void testWrite() {
    File f = createTempFile();
    TrianglePolyWriter writer = new TrianglePolyWriter();
    writer.setFile(f);
    writer.write(createDefault());
    //on compare le fichier g�n�r� avec le fichier attendu
    List<String> expectedFile = CtuluLibFile.litFichierLineByLine(fic_);
    List<String> writtenFile = CtuluLibFile.litFichierLineByLine(f);
    assertEquals(expectedFile.size(), writtenFile.size());
    for (int i = 0; i < expectedFile.size(); i++) {
      assertEquals("line " + (i + 1), expectedFile.get(i).trim(), writtenFile.get(i).trim());
    }
  }
}
