package org.fudaa.dodico.ef.io.serafin;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.common.TestIO;

import java.io.File;
import java.io.IOException;

public class TestSerafinNewReaderInfo extends TestIO {
    public TestSerafinNewReaderInfo() {
        super("serafinWithResultat.ser");
        assertNotNull(fic_);
        assertTrue(fic_.exists());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void testReadSerafinD() throws IOException {
        File target = createTempFile(".res");
        File serafin = CtuluLibFile.getFileFromJar("/org/fudaa/dodico/ef/io/serafin/serafinWithResultat.ser", target);
        SerafinNewReader reader = new SerafinNewReader();
        reader.setFile(serafin);
        CtuluIOOperationSynthese read = reader.read();
        assertFalse(read.getAnalyze().containsErrorOrFatalError());
        SerafinAdapter adapter = (SerafinAdapter) read.getSource();
        final SerafinNewReaderInfo info_ = adapter.info_;

        assertEquals(11, info_.getTimeStepAvailable());

        double[] aDouble = info_.getDouble(1, 1);
        assertDoubleEquals(-0.10593309253454208, aDouble[10]);
        assertDoubleEquals(-0.18014433979988098, aDouble[1000]);
//pour le cache
        aDouble = info_.getDouble(1, 1);
        assertDoubleEquals(-0.10593309253454208, aDouble[10]);
        assertDoubleEquals(-0.18014433979988098, aDouble[1000]);
    }
}
