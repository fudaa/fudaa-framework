/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZoneCollectionMultiPoint;

/**
 *
 * @author Frederic Deniger
 */
public class TriangleNodeAdapterTest extends TestCase {

  public TriangleNodeAdapterTest() {
    super();
  }

  private static Coordinate[] create(int nb) {
    Coordinate[] res = new Coordinate[nb];
    for (int i = 0; i < res.length; i++) {
      res[i] = new Coordinate(Math.random() * 100, Math.random() * 100);
    }
    return res;
  }

  public void testAdapter() {
    GISZoneCollectionMultiPoint multiPoint = new GISZoneCollectionMultiPoint();
    Coordinate[] cs1 = create(5);
    Coordinate[] cs2 = create(4);
    List<Coordinate> all = new ArrayList<Coordinate>();
    all.addAll(Arrays.asList(cs1));
    all.addAll(Arrays.asList(cs2));
    CoordinateSequence seq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(cs1);
    multiPoint.addCoordinateSequence(seq, null, null);
    seq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(cs2);
    multiPoint.addCoordinateSequence(seq, null, null);
    TriangleNodeAdapter adapter = new TriangleNodeAdapter(multiPoint);
    assertEquals(cs1.length + cs2.length, adapter.getPtsNb());
    for (int i = 0; i < all.size(); i++) {
      assertEquals(all.get(i).x, adapter.getPtX(i), 1e-10);
      assertEquals(all.get(i).y, adapter.getPtY(i), 1e-10);
    }

  }
}
