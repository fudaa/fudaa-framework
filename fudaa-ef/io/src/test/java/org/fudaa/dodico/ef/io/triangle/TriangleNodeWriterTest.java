/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import java.io.File;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.dodico.common.TestIO;

/**
 *
 * @author Frederic Deniger
 */
public class TriangleNodeWriterTest extends TestIO {
  
  public TriangleNodeWriterTest() {
    super("carre.node");
  }
  
  public void testWrite() {
    TriangleNodeReader reader = new TriangleNodeReader();
    reader.setFile(super.fic_);
    CtuluIOResult<TriangleNodeDataInterface> read = reader.read();
    TriangleNodeDataInterface source = read.getSource();
    File createTempFile = super.createTempFile();
    TriangleNodeWriter writer = new TriangleNodeWriter();
    writer.setFile(createTempFile);
    CtuluIOOperationSynthese write = writer.write(source);
    CtuluAnalyze analyze = write.getAnalyze();
    assertFalse(analyze.containsErrorOrFatalError());
    
    reader.setFile(createTempFile);
    read = reader.read();
    source = read.getSource();
    assertNotNull(source);
    
    assertEquals(1775, source.getPtsNb());
    assertEquals(1, source.getNbAttributes());
    assertTrue(source.containsBoundaryMarkers());
    //les markers
    assertEquals(1, source.getMarker(0));
    assertEquals(1, source.getMarker(161));
    assertEquals(0, source.getMarker(162));
    //les attributs
    assertDoubleEquals(10, source.getAttributes(0, 0));
    assertDoubleEquals(10.26, source.getAttributes(1774, 0));
    //les X
    assertDoubleEquals(753, source.getPtX(0));
    assertDoubleEquals(322.29000000000002, source.getPtX(1774));
    //les Y
    assertDoubleEquals(437, source.getPtY(0));
    assertDoubleEquals(427, source.getPtY(1774));
  }
}
