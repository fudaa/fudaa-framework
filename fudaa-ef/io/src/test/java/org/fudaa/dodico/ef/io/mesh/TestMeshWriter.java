package org.fudaa.dodico.ef.io.mesh;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfNode;

public class TestMeshWriter extends TestIO
{
    private double eps = 1E-10;

    public TestMeshWriter() {
      super("exemple.mesh");
      assertNotNull(fic_);
      assertTrue(fic_.exists());
    }

    public void testWriter()
    {
        MeshReader reader = new MeshReader();

        reader.setFile(this.fic_);

        CtuluIOOperationSynthese synth = reader.read();

        EfGridInterface originalGrid = ((EfGridSource)synth.getSource()).getGrid();
        
        MeshWriter writer = new MeshWriter();
        final File file = createTempFile();

        writer.setFile(file);
        synth = writer.write(originalGrid);

        // Test que l'�criture se soit execut�e sans erreurs.
        assertFalse(synth.getAnalyze().containsErrors());
        assertFalse(synth.getAnalyze().containsFatalError());
        
        reader.setFile(file);
        synth = reader.read();

        EfGridInterface grid = ((EfGridSource)synth.getSource()).getGrid();

        EfNode[] nodes = grid.getNodes();
        EfNode[] originalNodes = originalGrid.getNodes();

        // Test que tout les points soyent corrects.
        for (int i = 0; i < nodes.length; i++) {
          assertTrue("Le noeud � l'indice " + i + " est incorrect", nodes[i].equalsExact(originalNodes[i], this.eps));
        }

        EfElement[] elements = grid.getElts();
        EfElement[] originalElements = originalGrid.getElts();

        // Test que tout les �l�ments soyent corrects.
        for (int i = 0; i < elements.length; i++) {
          assertTrue("L'�l�ment � l'indice " + i + " est incorrect", elements[i].isSameStrict(originalElements[i]));
        }
        
        file.delete();
    }
}
