/*
 GPL 2
 */
package org.fudaa.dodico.ef.io.triangle;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.dodico.common.TestIO;

/**
 *
 * @author Frederic Deniger
 */
public class TriangleEleReaderTest extends TestIO {

  public TriangleEleReaderTest() {
    super("grid.ele");
  }

  public void testRead() {
    TriangleEleReader reader = new TriangleEleReader();
    reader.setFile(fic_);
    CtuluIOResult<TriangleEleData> read = reader.read();
    TriangleEleData source = read.getSource();
    assertEquals(3386, source.getNbElt());
    assertEquals(3, source.getNbNodesPerElt());
    assertEquals(0, source.getNbAttributes());
    //1      82  1600    81
    assertEquals(81, source.getPtIdx(0, 0));
    assertEquals(1599, source.getPtIdx(0, 1));
    assertEquals(80, source.getPtIdx(0, 2));
    //3386     620  1578   619
    assertEquals(619, source.getPtIdx(3385, 0));
    assertEquals(1577, source.getPtIdx(3385, 1));
    assertEquals(618, source.getPtIdx(3385, 2));
  }
}
