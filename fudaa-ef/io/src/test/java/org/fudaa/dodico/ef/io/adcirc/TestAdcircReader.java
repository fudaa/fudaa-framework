package org.fudaa.dodico.ef.io.adcirc;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.EfNode;

public class TestAdcircReader extends TestIO
{
    private double eps = 1E-10;

    public TestAdcircReader()
    {
        super("adcirc.adc");
        assertNotNull(fic_);
        assertTrue(fic_.exists());
    }

    public void testReader()
    {
        AdcircReader reader = new AdcircReader();

        reader.setFile(this.fic_);
        
        CtuluIOOperationSynthese synth = reader.read();
        
        // Test que la lecture se soit execut�e sans erreurs.
        assertFalse(synth.getAnalyze().containsErrors());
        assertFalse(synth.getAnalyze().containsFatalError());
        
        EfGridSource    gridSrc = (EfGridSource)synth.getSource();
        EfGridInterface grid    = gridSrc.getGrid();
        
        // Test que la grille poss�de le bon nombre de points et d'�l�ments.
        assertEquals(63, grid.getPtsNb());
        assertEquals(96, grid.getEltNb());
        
        EfNode[]    nodes         = grid.getNodes();
        EfNode[]    originalNodes = this.getOriginalNodes();
        
        // Test que tout les points soyent corrects.
        for (int i = 0; i < nodes.length; i++)
        {
            assertTrue(nodes[i].equalsExact(originalNodes[i], this.eps));
        }

        EfElement[] elements         = grid.getElts();
        EfElement[] originalElements = this.getOriginalElements();

        // Test que tout les �l�ments soyent corrects.
        for (int i = 0; i < elements.length; i++)
        {
            assertTrue(elements[i].isSameStrict(originalElements[i]));
        }
    }

    /**
     * 
     * @return un tableau contenant les points se trouvant dans le fichier de test.
     */
    private EfNode[] getOriginalNodes()
    {
        EfNode[] nodes = new EfNode[63];
        
        nodes[0] = new EfNode(200000.000000, 0.000000, 10.000);
        nodes[1] = new EfNode(250000.000000, 0.000000, 15.625);
        nodes[2] = new EfNode(196160.000000, 39020.000000, 10.000);
        nodes[3] = new EfNode(245200.000000, 48770.000000, 15.625);
        nodes[4] = new EfNode(300000.000000, 0.000000, 22.500);
        nodes[5] = new EfNode(294240.000000, 58530.000000, 22.500);
        nodes[6] = new EfNode(350000.000000, 0.000000, 30.625);
        nodes[7] = new EfNode(343270.000000, 68280.000000, 30.625);
        nodes[8] = new EfNode(400000.000000, 0.000000, 40.000);
        nodes[9] = new EfNode(392310.000000, 78040.000000, 40.000);
        nodes[10] = new EfNode(450000.000000, 0.000000, 50.625);
        nodes[11] = new EfNode(441350.000000, 87790.000000, 50.625);
        nodes[12] = new EfNode(230970.000000, 95670.000000, 15.625);
        nodes[13] = new EfNode(184780.000000, 76540.000000, 10.000);
        nodes[14] = new EfNode(500000.000000, 0.000000, 62.500);
        nodes[15] = new EfNode(490390.000000, 97550.000000, 62.500);
        nodes[16] = new EfNode(277160.000000, 114810.000000, 22.500);
        nodes[17] = new EfNode(323360.000000, 133940.000000, 30.625);
        nodes[18] = new EfNode(207870.000000, 138890.000000, 15.625);
        nodes[19] = new EfNode(166290.000000, 111110.000000, 10.000);
        nodes[20] = new EfNode(369550.000000, 153070.000000, 40.000);
        nodes[21] = new EfNode(249440.000000, 166670.000000, 22.500);
        nodes[22] = new EfNode(415750.000000, 172210.000000, 50.625);
        nodes[23] = new EfNode(141420.000000, 141420.000000, 10.000);
        nodes[24] = new EfNode(176780.000000, 176780.000000, 15.625);
        nodes[25] = new EfNode(461940.000000, 191340.000000, 62.500);
        nodes[26] = new EfNode(291010.000000, 194450.000000, 30.625);
        nodes[27] = new EfNode(111110.000000, 166290.000000, 10.000);
        nodes[28] = new EfNode(138890.000000, 207870.000000, 15.625);
        nodes[29] = new EfNode(212130.000000, 212130.000000, 22.500);
        nodes[30] = new EfNode(332590.000000, 222230.000000, 40.000);
        nodes[31] = new EfNode(76540.000000, 184780.000000, 10.000);
        nodes[32] = new EfNode(95670.000000, 230970.000000, 15.625);
        nodes[33] = new EfNode(247490.000000, 247490.000000, 30.625);
        nodes[34] = new EfNode(39020.000000, 196160.000000, 10.000);
        nodes[35] = new EfNode(48770.000000, 245200.000000, 15.625);
        nodes[36] = new EfNode(374160.000000, 250010.000000, 50.625);
        nodes[37] = new EfNode(166670.000000, 249440.000000, 22.500);
        nodes[38] = new EfNode(0.000000, 200000.000000, 10.000);
        nodes[39] = new EfNode(0.000000, 250000.000000, 15.625);
        nodes[40] = new EfNode(415730.000000, 277790.000000, 62.500);
        nodes[41] = new EfNode(114810.000000, 277160.000000, 22.500);
        nodes[42] = new EfNode(282840.000000, 282840.000000, 40.000);
        nodes[43] = new EfNode(194450.000000, 291010.000000, 30.625);
        nodes[44] = new EfNode(58530.000000, 294240.000000, 22.500);
        nodes[45] = new EfNode(0.000000, 300000.000000, 22.500);
        nodes[46] = new EfNode(318200.000000, 318200.000000, 50.625);
        nodes[47] = new EfNode(133940.000000, 323360.000000, 30.625);
        nodes[48] = new EfNode(222230.000000, 332590.000000, 40.000);
        nodes[49] = new EfNode(68280.000000, 343270.000000, 30.625);
        nodes[50] = new EfNode(353550.000000, 353550.000000, 62.500);
        nodes[51] = new EfNode(0.000000, 350000.000000, 30.625);
        nodes[52] = new EfNode(153070.000000, 369550.000000, 40.000);
        nodes[53] = new EfNode(250010.000000, 374160.000000, 50.625);
        nodes[54] = new EfNode(78040.000000, 392310.000000, 40.000);
        nodes[55] = new EfNode(0.000000, 400000.000000, 40.000);
        nodes[56] = new EfNode(277790.000000, 415730.000000, 62.500);
        nodes[57] = new EfNode(172210.000000, 415750.000000, 50.625);
        nodes[58] = new EfNode(87790.000000, 441350.000000, 50.625);
        nodes[59] = new EfNode(0.000000, 450000.000000, 50.625);
        nodes[60] = new EfNode(191340.000000, 461940.000000, 62.500);
        nodes[61] = new EfNode(97550.000000, 490390.000000, 62.500);
        nodes[62] = new EfNode(0.000000, 500000.000000, 62.500);

        return nodes;
    }

    /**
     * 
     * @return un tableau contenant les �l�ments se trouvant dans le fichier de test.
     */
    private EfElement[] getOriginalElements()
    {
        EfElement[] elements = new EfElement[96];
        
        elements[0] = new EfElement(new int[]{0,1,2});
        elements[1] = new EfElement(new int[]{1,3,2});
        elements[2] = new EfElement(new int[]{3,1,5});
        elements[3] = new EfElement(new int[]{5,1,4});
        elements[4] = new EfElement(new int[]{6,5,4});
        elements[5] = new EfElement(new int[]{7,5,6});
        elements[6] = new EfElement(new int[]{7,6,9});
        elements[7] = new EfElement(new int[]{9,6,8});
        elements[8] = new EfElement(new int[]{10,9,8});
        elements[9] = new EfElement(new int[]{11,9,10});
        elements[10] = new EfElement(new int[]{2,12,13});
        elements[11] = new EfElement(new int[]{3,12,2});
        elements[12] = new EfElement(new int[]{11,10,15});
        elements[13] = new EfElement(new int[]{15,10,14});
        elements[14] = new EfElement(new int[]{3,5,12});
        elements[15] = new EfElement(new int[]{5,16,12});
        elements[16] = new EfElement(new int[]{5,17,16});
        elements[17] = new EfElement(new int[]{7,17,5});
        elements[18] = new EfElement(new int[]{12,19,13});
        elements[19] = new EfElement(new int[]{18,19,12});
        elements[20] = new EfElement(new int[]{7,9,17});
        elements[21] = new EfElement(new int[]{9,20,17});
        elements[22] = new EfElement(new int[]{18,12,21});
        elements[23] = new EfElement(new int[]{21,12,16});
        elements[24] = new EfElement(new int[]{11,22,9});
        elements[25] = new EfElement(new int[]{9,22,20});
        elements[26] = new EfElement(new int[]{18,24,19});
        elements[27] = new EfElement(new int[]{19,24,23});
        elements[28] = new EfElement(new int[]{11,15,22});
        elements[29] = new EfElement(new int[]{15,25,22});
        elements[30] = new EfElement(new int[]{17,21,16});
        elements[31] = new EfElement(new int[]{26,21,17});
        elements[32] = new EfElement(new int[]{28,27,24});
        elements[33] = new EfElement(new int[]{24,27,23});
        elements[34] = new EfElement(new int[]{18,21,24});
        elements[35] = new EfElement(new int[]{24,21,29});
        elements[36] = new EfElement(new int[]{26,17,30});
        elements[37] = new EfElement(new int[]{30,17,20});
        elements[38] = new EfElement(new int[]{27,32,31});
        elements[39] = new EfElement(new int[]{28,32,27});
        elements[40] = new EfElement(new int[]{21,33,29});
        elements[41] = new EfElement(new int[]{26,33,21});
        elements[42] = new EfElement(new int[]{34,32,35});
        elements[43] = new EfElement(new int[]{31,32,34});
        elements[44] = new EfElement(new int[]{20,22,30});
        elements[45] = new EfElement(new int[]{30,22,36});
        elements[46] = new EfElement(new int[]{37,24,29});
        elements[47] = new EfElement(new int[]{28,24,37});
        elements[48] = new EfElement(new int[]{35,39,34});
        elements[49] = new EfElement(new int[]{34,39,38});
        elements[50] = new EfElement(new int[]{40,22,25});
        elements[51] = new EfElement(new int[]{36,22,40});
        elements[52] = new EfElement(new int[]{41,32,37});
        elements[53] = new EfElement(new int[]{37,32,28});
        elements[54] = new EfElement(new int[]{42,33,30});
        elements[55] = new EfElement(new int[]{30,33,26});
        elements[56] = new EfElement(new int[]{43,37,33});
        elements[57] = new EfElement(new int[]{33,37,29});
        elements[58] = new EfElement(new int[]{44,32,41});
        elements[59] = new EfElement(new int[]{35,32,44});
        elements[60] = new EfElement(new int[]{45,39,44});
        elements[61] = new EfElement(new int[]{44,39,35});
        elements[62] = new EfElement(new int[]{36,46,30});
        elements[63] = new EfElement(new int[]{30,46,42});
        elements[64] = new EfElement(new int[]{37,47,41});
        elements[65] = new EfElement(new int[]{43,47,37});
        elements[66] = new EfElement(new int[]{43,33,48});
        elements[67] = new EfElement(new int[]{48,33,42});
        elements[68] = new EfElement(new int[]{41,47,44});
        elements[69] = new EfElement(new int[]{44,47,49});
        elements[70] = new EfElement(new int[]{36,40,46});
        elements[71] = new EfElement(new int[]{46,40,50});
        elements[72] = new EfElement(new int[]{44,51,45});
        elements[73] = new EfElement(new int[]{49,51,44});
        elements[74] = new EfElement(new int[]{43,48,47});
        elements[75] = new EfElement(new int[]{47,48,52});
        elements[76] = new EfElement(new int[]{42,46,48});
        elements[77] = new EfElement(new int[]{48,46,53});
        elements[78] = new EfElement(new int[]{49,47,54});
        elements[79] = new EfElement(new int[]{54,47,52});
        elements[80] = new EfElement(new int[]{55,51,54});
        elements[81] = new EfElement(new int[]{54,51,49});
        elements[82] = new EfElement(new int[]{56,46,50});
        elements[83] = new EfElement(new int[]{53,46,56});
        elements[84] = new EfElement(new int[]{53,57,48});
        elements[85] = new EfElement(new int[]{48,57,52});
        elements[86] = new EfElement(new int[]{58,54,57});
        elements[87] = new EfElement(new int[]{57,54,52});
        elements[88] = new EfElement(new int[]{54,59,55});
        elements[89] = new EfElement(new int[]{58,59,54});
        elements[90] = new EfElement(new int[]{53,56,57});
        elements[91] = new EfElement(new int[]{57,56,60});
        elements[92] = new EfElement(new int[]{61,57,60});
        elements[93] = new EfElement(new int[]{58,57,61});
        elements[94] = new EfElement(new int[]{62,59,61});
        elements[95] = new EfElement(new int[]{61,59,58});

        return elements;
    }
}
