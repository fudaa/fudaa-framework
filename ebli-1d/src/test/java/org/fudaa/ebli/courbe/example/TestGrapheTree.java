/*
 * @creation     25 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.courbe.example;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JTree;
import org.fudaa.ctulu.iterator.LogarithmicNumberIterator;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Tutorial sur la cr�ation de graphe avec un arbre de courbe.
 * 
 * L'objectif de ce code est d'afficher une fen�tre avec en son centre une 
 * zone avec un rep�re (axe x et y) et une ou plusieurs courbes affich�es, 
 * dans un menu une s�rie d'action r�alisables sur le graphe et les courbes, 
 * et enfin � la droite une s�rie de courbes repr�sent�es sous forme d'arbre
 * permettant de choisir la courbe selectionn�e.
 * Un diagramme de classe simplifi� du package src:org.fudaa.ebli.courbe est
 * disponible dans le package test:org.fudaa.ebli.all
 * 
 * Ce tutorial en accompagne un autre permettant de r�aliser un graphe
 * simple. C'est � dire ne poss�dant qu'une seule courbe, sans notion
 * d'arbre de courbe.
 * 
 * Ce tutorial reprend quasi int�gralement le code fourni par Fred Deniger
 * dans TestGraphe2.java
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class TestGrapheTree {
  
  private TestGrapheTree(){}
  
  public static void main(final String[] _args) {
    // Graphe model et graphe vue \\
    /*
     * Chaque notion est d�compos� en deux formes : le model et la vue.
     * Ainsi le graphe (le conteneur g�rant l'affichage des courbes dans
     * le rep�re) est d�compos� en graphe model et graphe vue.
     * Nous voulons pouvoir afficher plusieurs courbes. Le model choisi
     * pour le graphe est donc EGGrapheTreeModel.
     * Il existe plusieurs model de graphe (notamment EGGrapheSimpleModel)
     * chacun implementant l'interface EGGrapheModel. 
     */
    EGGrapheTreeModel grapheModel = new EGGrapheTreeModel();
    /*
     * La vue choisie pour ce model se nomme EGGraphe. Il n'y en a qu'une
     * quelque soit le model de graphe choisi.
     */
    EGGraphe grapheVue = new EGGraphe(grapheModel);
    
    // Axe x \\
    /*
     * Pour afficher les courbes, notre graphe vue a besoin d'un rep�re
     * (axe x et axe y). La premi�re difficult�e est ici.
     * Les deux axes ne sont pas r�f�renc� au m�me endroit. L'axe des x
     * est poss�d� par le graphe vue, et donc est commun � l'ensemble des
     * courbes. L'axe des y est poss�d� par un groupe de courbe de l'arbre.
     * Ainsi il n'y aura qu'un seul axe des x sur le rep�re, mais plusieurs
     * axes des y (un pour chaque groupe).
     */
    EGAxeHorizontal axeX = new EGAxeHorizontal(false);
    axeX.setTitre("temps");
    axeX.setUnite("sec");
    axeX.setBounds(0.0001, 1000000); // Minimum et maximum de l'axe
    /*
     * L'instruction suivante permet de choisir le type de graduation
     * de l'axe (lin�aire, logarithmique, etc...). Si cette fonction
     * n'est pas appel�e, le type par defaut est lin�aire.
     */
    axeX.setAxisIterator(new LogarithmicNumberIterator());
    axeX.setGraduations(true);
    grapheVue.setXAxe(axeX);
    
    // Groupes, courbes et axe y \\
    /*
     * Cr�ation d'un des deux groupes de courbes que nous allons placer dans 
     * l'arbre. Ces groupes poss�dent une s�rie de courbes, un axe y et un
     * titre.
     */
    EGGroup groupe = new EGGroup();
    groupe.setTitle("groupe 1");
    /*
     * Courbe : 
     * Comme pour le graphe, les courbes sont divis�es en courbe model et
     * courbe vue. 
     * Les deux tableaux de doubles pass�s en param�tres du model sont les
     * coordonn�es x et les coordonn�es y de la courbe. Le titre d'un
     * model correspond au nom qui sera affich� dans l'arbre pour cette
     * courbe.
     * La vue utilis�e ici est sp�cialement cr�e pour pouvoir
     * s'int�grer au groupe auquel elle apartient.
     */
    // Premi�re courbe de ce groupe
    EGCourbeModelDefault courbeModel=new EGCourbeModelDefault(new double[]{0.0001, 3, 4, 1000000}, new double[]{10000, 4, 5, 3});
    courbeModel.setTitle("Coubre bleue");
    EGCourbeChild courbeVue = new EGCourbeChild(groupe);
    courbeVue.setModel(courbeModel);
    courbeVue.setAspectContour(Color.cyan);
    groupe.addEGComponent(courbeVue);
    // Seconde courbe de ce groupe
    courbeModel = new EGCourbeModelDefault(new double[] { 0.0002, 5, 7, 900000 }, new double[] { 0.001, 1, 3, 4 });
    courbeModel.setTitle("Courbe rouge");
    courbeVue = new EGCourbeChild(groupe);
    courbeVue.setAspectContour(Color.RED);
    courbeVue.setModel(courbeModel);
    groupe.addEGComponent(courbeVue);
    /*
     * L'axe des y poss�de les m�mes propri�t�s que l'axe des x.
     */
    EGAxeVertical axeY = new EGAxeVertical();
    axeY.setGraduations(true);
    /*
     * La grille permet de dessiner des lignes horizontales (d�crites par
     * TraceLigneModel).Dans le cas o� c'est utilis� par l'axe des x, ces lignes
     * seront verticales formant ainsi une grille compl�te.
     */
    axeY.setTraceGraduations(new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    axeY.setAxisIterator(new LogarithmicNumberIterator());
    axeY.setBounds(0.0001, 10000);
    axeY.setLineColor(Color.blue);
    axeY.setTitre("Axe Y 1");
    groupe.setAxeY(axeY);
    grapheModel.add(groupe);
    // Construction du second groupe
    groupe = new EGGroup();
    groupe.setTitle("groupe 2");
    // Premi�re courbe de ce second groupe
    courbeModel = new EGCourbeModelDefault(new double[] { 1, 8, 9, 10 }, new double[] { 10, 4, 2, 24 });
    courbeModel.setTitle("Courbe jaune");
    courbeVue = new EGCourbeChild(groupe);
    courbeVue.setAspectContour(Color.yellow);
    courbeVue.setModel(courbeModel);
    groupe.addEGComponent(courbeVue);
    // Seconde courbe de ce second groupe
    courbeModel = new EGCourbeModelDefault(new double[] { 1, 3, 4, 5 }, new double[] { 14, 54, 25, 43 });
    courbeModel.setTitle("Courbe vert");
    courbeVue = new EGCourbeChild(groupe);
    courbeVue.setAspectContour(Color.green);
    courbeVue.setModel(courbeModel);
    groupe.addEGComponent(courbeVue);
    // Axe des y de ce second groupe
    axeY = new EGAxeVertical();
    axeY.setDroite(true); // Emplacement de l'axe � droite du graphe.
    axeY.setGraduations(true);
    axeY.setBounds(0, 55);
    axeY.setTitre("Axe Y 2");
    groupe.setAxeY(axeY);
    grapheModel.add(groupe);
    
    // La vue de l'arbre \\
    /*
     * C'est le graphe model qui contient les groupes. On peut donc le
     * consid�rer �galement comme le model de l'arbre.
     * Donc la vue de l'abre (ici un simple JTree) viendra utilise le
     * graphe model. 
     */
    JTree arbreVue = new JTree(grapheModel);
    arbreVue.setSelectionModel(grapheModel.getSelectionModel());
    arbreVue.setRootVisible(false);
    
    // EGFillePanel et menu\\
    /*
     * EGFillePanel est le 'manager' qui contient le graphe vue. Il permet
     * de g�n�rer des boutons pour interagir avec le graphe. Cette g�n�ration
     * peut prendre plusieurs forme, ici nous utiliserons la fonction 
     * fillSpecificMenu qui rempli le menu pass� en param�tre de tout les
     * boutons n�c�ssaires. Une autre utilisation de ces boutons est faite
     * dans le tutorial TestGrapheSimple.
     */
    EGFillePanel egPanel = new EGFillePanel(grapheVue);
    JMenu menu = new JMenu();
    menu.setName("Nom menu");
    menu.setText("Texte menu");
    egPanel.fillSpecificMenu(menu);
    JMenuBar menuBarre = new JMenuBar();
    menuBarre.add(menu);
    
    // Construction de la fen�tre \\
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setJMenuBar(menuBarre);
    
    BuPanel panel = new BuPanel();
    panel.setLayout(new BuBorderLayout());
    panel.add(egPanel, BuBorderLayout.CENTER);
    panel.add(new BuScrollPane(arbreVue), BuBorderLayout.EAST);

    grapheVue.setPreferredSize(new Dimension(200, 300));
    grapheVue.setSize(grapheVue.getPreferredSize());
    
    frame.getContentPane().add(panel);
    frame.pack();
    frame.setVisible(true);
    /*
     * Il est n�c�ssaire de redimmensionner la fen�tre pour que les
     * courbes se dessines.
     */
  }
}
