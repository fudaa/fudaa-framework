/*
 GPL 2
 */
package org.fudaa.ebli.courbe.convert;

import java.awt.Color;
import java.util.Collections;
import java.util.List;
import junit.framework.TestCase;
import org.fudaa.ebli.courbe.EGCourbeMarqueur;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class EGCourbePersistConverterTest extends TestCase {

  public EGCourbePersistConverterTest(String testName) {
    super(testName);
  }

  public void testToString() {
    try {
      EGCourbePersist persist = new EGCourbePersist();
      persist.setDisplayLabels(true);
      persist.setTitle("title");
      persist.setVerticalLabels(true);
      persist.setLineModel(new TraceLigneModel(1, 4, Color.GREEN));
      EGCourbePersistConverter converter = new EGCourbePersistConverter();
      String toXml = converter.toXml(persist);
      EGCourbePersist fromXml = converter.fromXml(toXml);
      assertNotNull(fromXml);
      assertEquals(persist.getTitle(), fromXml.getTitle());
      assertEquals(persist.getLineModel_(), fromXml.getLineModel_());
      assertEquals(persist.isVerticalLabels(), fromXml.isVerticalLabels());
      EGCourbeMarqueur mark = new EGCourbeMarqueur(20, true, new TraceLigneModel(7, 8, Color.BLACK), true);
      persist.setListeMarqueurs(Collections.singletonList(mark));
      toXml = converter.toXml(persist);
      fromXml = converter.fromXml(toXml);
      assertNotNull(fromXml);
      List<EGCourbeMarqueur> listeMarqueurs = fromXml.getListeMarqueurs();
      assertEquals(1, listeMarqueurs.size());
      EGCourbeMarqueur readFromXML = listeMarqueurs.get(0);
      assertEquals(mark.getValue(), readFromXML.getValue(), 1e-15);
      assertEquals(mark.getModel(), readFromXML.getModel());
    } catch (Exception e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }
}
