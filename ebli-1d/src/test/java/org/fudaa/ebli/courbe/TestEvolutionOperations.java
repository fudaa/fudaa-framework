/**
 * Licence GPL
 * Copyright Genesis
 */
package org.fudaa.ebli.courbe;

import junit.framework.TestCase;

/**
 * @author deniger
 */
public class TestEvolutionOperations extends TestCase {

  public void testSimplify() {
    double[] ts = new double[] { 1, 2, 3, 4, 5 };
    double[] vs = new double[] { 0, 0, 0, 0, 0 };
    EGCourbeModelDefault evol = new EGCourbeModelDefault(ts, vs);
    int[] res = EvolutionOperations.getIdxRemovable(evol);
    assertNotNull(res);
    assertEquals(3, res.length);
    assertEquals(1, res[0]);
    assertEquals(2, res[1]);
    assertEquals(3, res[2]);
    vs[2]=2;
    evol = new EGCourbeModelDefault(ts, vs);
    res = EvolutionOperations.getIdxRemovable(evol);
    assertNotNull(res);
    assertEquals(0, res.length);
    vs[1]=1;
    evol = new EGCourbeModelDefault(ts, vs);
    res = EvolutionOperations.getIdxRemovable(evol);
    assertNotNull(res);
    assertEquals(1, res.length);
    assertEquals(1, res[0]);
  }

}
