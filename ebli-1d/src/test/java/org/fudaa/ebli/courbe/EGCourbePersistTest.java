package org.fudaa.ebli.courbe;

import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.fudaa.ebli.courbe.convert.EGCourbePersistConverter;
import org.junit.Assert;

import java.io.IOException;

public class EGCourbePersistTest extends TestCase {

  public void testCopyEgCourbePersist() throws IOException {
    String xml = IOUtils.toString(getClass().getResource("courbePersist.xml"));
    Assert.assertNotNull(xml);
    EGCourbePersistConverter converter = new EGCourbePersistConverter();
    EGCourbePersist read = converter.fromXml(xml);

    EGCourbePersist copy = new EGCourbePersist(read);

    assertEquals(xml, converter.toXmlPretty(copy));
    assertNotSame(read.iconeModel, copy.iconeModel);
    assertNotSame(read.tracebox, copy.tracebox);
    assertNotSame(read.iconeModelSpecific, copy.iconeModelSpecific);
    assertNotSame(read.surfacePainter, copy.surfacePainter);
    assertNotSame(read.listeMarqueurs_, copy.listeMarqueurs_);

  }
}