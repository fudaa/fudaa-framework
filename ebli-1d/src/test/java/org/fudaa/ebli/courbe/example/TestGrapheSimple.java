/*
 * @creation     25 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.courbe.example;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JToolBar;

import org.fudaa.ctulu.editor.CtuluValueEditorDuration;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.courbe.EGTableGraphePanel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;

/**
 * Tutorial sur la cr�ation de graphe avec une seule courbe.
 * 
 * L'objectif de ce code est d'afficher une fen�tre avec en son centre une 
 * zone avec un rep�re (axe x et y) et une s�rie d'action disponible sous
 * forme de boutons � gauche du graphe.
 * Un diagramme de classe simplifi� du package src:org.fudaa.ebli.courbe est
 * disponible dans le package test:org.fudaa.ebli.all
 * 
 * Ce tutorial en accompagne un autre permettant de r�aliser un graphe
 * avec plusieurs courbes.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class TestGrapheSimple {

  private TestGrapheSimple(){}
  
  public static void main(final String[] _args) {
    // Graphe model et graphe vue \\
    /*
     * Chaque notion est d�compos� en deux formes : le model et la vue.
     * Ainsi le graphe (le conteneur g�rant l'affichage des courbes dans
     * le rep�re) est d�compos� en graphe model et graphe vue.
     * Nous ne voulons afficher qu'une courbe. Le model choisi pour le 
     * graphe est donc EGGrapheSimpleModel.
     * Il existe plusieurs model de graphe (notamment EGGrapheSimpleModel)
     * chacun implementant l'interface EGGrapheModel. 
     */
    EGGrapheSimpleModel grapheModel = new EGGrapheSimpleModel();
    /*
     * La vue choisie pour ce model se nomme EGGraphe. Il n'y en a qu'une
     * quelque soit le model de graphe choisi.
     */
    EGGraphe grapheVue = new EGGraphe(grapheModel);
    
    // Axe x et axe y\\
    /*
     * Pour afficher les courbes, notre graphe vue a besoin d'un rep�re
     * (axe x et axe y). La premi�re difficult�e est ici.
     * Les deux axes ne sont pas r�f�renc� au m�me endroit. L'axe des x
     * est poss�d� par le graphe vue, et donc est commun � l'ensemble des
     * courbes. L'axe des y est poss�d� par la vue de la courbe. Dans
     * notre cas cette distinction n'a pas d'int�ret. Dans le cas de
     * l'affichage de plusieurs courbe, cette architecture est utile.
     */
    EGAxeHorizontal axeX = new EGAxeHorizontal(false);
    axeX.setTitre("temps");
    axeX.setUnite("sec");
    // Definit que les valeurs sur l'axe des X sont des dur�es. Si on edite une valeur, cet �diteur sera utilis�
    // pour respecter un format particulier.
    CtuluValueEditorDuration ed = new CtuluValueEditorDuration();
    axeX.setValueEditor(ed);
    // D�finit que les donn�es en X seront affich�es suivant ce format.
    axeX.setSpecificFormat(ed.getDefaultFormatter());
    axeX.setBounds(0., 10000.); // Minimum et maximum de l'axe
    /*
     * L'instruction suivante permet de choisir le type de graduation
     * de l'axe (lin�aire, logarithmique, etc...). Si cette fonction
     * n'est pas appel�e, le type par defaut est lin�aire.
     */
//    axeX.setAxisIterator(new LogarithmicNumberIterator());
    axeX.setGraduations(true);
    grapheVue.setXAxe(axeX);
    /*
     * Nous cr�ons l'axe des y, mais l'utilisons lors de la cr�ation de la courbe vue.
     * L'axe des y poss�de les m�mes propri�t�s que l'axe des x.
     */
    EGAxeVertical axeY = new EGAxeVertical();
    axeY.setGraduations(true);
    /*
     * La grille permet de dessiner des lignes horizontales (d�crites par
     * TraceLigneModel).Dans le cas o� c'est utilis� par l'axe des x, ces lignes
     * seront verticales formant ainsi une grille compl�te.
     */
    axeY.setTraceGraduations(new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
//    axeY.setAxisIterator(new LogarithmicNumberIterator());
    axeY.setBounds(0., 10.);
    axeY.setLineColor(Color.blue);
    axeY.setTitre("Axe Y 1");
    
    // Courbe \\
    /*
     * Courbe : 
     * Comme pour le graphe, les courbes sont divis�es en courbe model et
     * courbe vue. 
     * Les deux tableaux de doubles pass�s en param�tres du model sont les
     * coordonn�es x et les coordonn�es y de la courbe.
     */
    EGCourbeModelDefault courbeModel=new EGCourbeModelDefault(new double[]{0, 300, 4000, 97200}, new double[]{2, 4, 5, 3});
    EGCourbeSimple courbeVue = new EGCourbeSimple(axeY, courbeModel);
    courbeVue.setTitle("Courbe 1");
    courbeVue.setAspectContour(Color.cyan);
    grapheModel.addCourbe(courbeVue, null);

    // EGFillePanel et boutons \\
    /*
     * EGFillePanel est le 'manager' qui contient le graphe vue. Il permet
     * de g�n�rer des boutons pour interagir avec le graphe. Cette g�n�ration
     * peut prendre plusieurs forme, ici nous utiliserons la fonction 
     * getSpecificActions qui retourne une s�rie d'action permettant de
     * cr�er les boutons � ajouter � l'interface.
     * Une autre utilisation de ces boutons est faite
     * dans le tutorial TestGrapheTree.
     */
    EGFillePanel egPanel = new EGFillePanel(grapheVue);
    // Construction des boutons
    EbliActionInterface[] actions = egPanel.getSpecificActions();
    JToolBar tbBoutons = new JToolBar();
    // Pour chaque action, on g�n�re un bouton correspondant
    for (int i=0; i<actions.length; i++) {
      if(actions[i] != null) {
        // Ajout du bouton correspondant � l'action dans le container
        tbBoutons.add(actions[i].buildToolButton(EbliComponentFactory.INSTANCE));
      }
      else {
        tbBoutons.addSeparator();
      }
    }
    
    /**
     *  On cr�e aussi le tableau des valeurs, li� au graphe.
     */
    EGTableGraphePanel tbGraphe = new EGTableGraphePanel(true);
    // Lie les donn�es du graphe au tableau.
    tbGraphe.setGraphe(grapheVue);
    // Lie les evenements de selection tableau/graphe.
    egPanel.majSelectionListener(tbGraphe);
    
    // Construction de la fen�tre \\
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    BuPanel panel = new BuPanel();
    panel.setLayout(new BuBorderLayout());
    panel.add(egPanel, BuBorderLayout.CENTER);
    panel.add(tbBoutons, BuBorderLayout.NORTH);
    panel.add(tbGraphe, BuBorderLayout.EAST);

    grapheVue.setPreferredSize(new Dimension(500, 500));
    grapheVue.setSize(grapheVue.getPreferredSize());
    
    frame.add(panel);
    frame.pack();
    frame.setVisible(true);
    /*
     * Il est n�c�ssaire de redimmensionner la fen�tre pour que les
     * courbes se dessines.
     */
  }
}

