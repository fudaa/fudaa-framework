package org.fudaa.ebli.courbe;

import com.memoire.bu.BuLib;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.courbe.convert.EGAxePersistConverter;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

import java.awt.*;

public class EGAxeVerticalPersistTest extends TestCase {

  public void testCopy() {

    EGAxePersistConverter converter = new EGAxePersistConverter();

    EGAxeVerticalPersist init = new EGAxeVerticalPersist();
    init.setDroite(true);
    init.setTitreVertical(true);
    init.setTitreVerticalDroite(true);
    init.setTitre("title");
    init.setTitreVisible(false);
    init.setUnite("unit");
    init.setVisible(true);
    init.setFont(new Font("test",1,2));
    init.setGraduations(true);
    init.setGrille(new TraceLigne(2,4, Color.RED));
    init.setIsIteratorUptodate(true);
    init.setLineColor(Color.WHITE);
    init.setRange(new CtuluRange(1, 2));
    init.setTraceGrille(true);
    init.setTraceSousGrille(true);
    init.setTraceGraduations(new TraceLigneModel(5,6, Color.RED));
    init.setTraceSousGraduations(new TraceLigneModel(0,1, Color.RED));
    init.setNbPas(222);
    init.setLongueurPas(1.223);
    init.setModeGraduations(100);
    init.setNbSousGraduations(8);
    init.setIsExtremiteDessinee(true);


    EGAxeVerticalPersist copy = init.copy();

    assertEquals(converter.toXml(init), converter.toXml(copy));
    assertNotSame(copy.getRange(), init.getRange());
    assertNotSame(copy.getTraceGraduations(), init.getTraceGraduations());
    assertNotSame(copy.getTraceSousGraduations(), init.getTraceSousGraduations());
    assertNotSame(copy.getGrille(), init.getGrille());
    assertSame(copy.getFont(), init.getFont());
  }
}