/**
 *  @creation     21 juin 2004
 *  @modification $Date: 2007-01-17 10:45:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe.example;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import org.fudaa.ctulu.iterator.LogarithmicNumberIterator;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbeChild;
import org.fudaa.ebli.courbe.EGCourbeModelDefault;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheTreeModel;
import org.fudaa.ebli.courbe.EGGroup;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Fred Deniger
 * @version $Id: TestGraphe2.java,v 1.8 2007-01-17 10:45:17 deniger Exp $
 */
public final class TestEGGraphe {

  private TestEGGraphe() {
    super();
  }

  /**
   * @param _args non utilise
   */
  public static void main(final String[] _args) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        launch();
      }
    });

  }

  private static void launch() {
    final EGGrapheTreeModel grapheModel = new EGGrapheTreeModel();
    final EGGraphe g = new EGGraphe(grapheModel);
    final EGAxeHorizontal x = new EGAxeHorizontal(false);
    x.setTitre("temps");
    x.setUnite("sec");
    x.setBounds(0.0001, 1000000);
    x.setAxisIterator(new LogarithmicNumberIterator());
    x.setGraduations(true);
    g.setXAxe(x);
    EGGroup gr = new EGGroup();
    gr.setTitle("gr 1");
    EGCourbeModelDefault m = new EGCourbeModelDefault(new double[] { 0.0001, 3, 4, 1000000 }, new double[] { 10000, 4,
        5, 3 });
    EGCourbeChild c = new EGCourbeChild(gr);
    m.setTitle("toto bleue");
    c.setModel(m);
    c.setAspectContour(Color.cyan);

    gr.addEGComponent(c);

    m = new EGCourbeModelDefault(new double[] { 0.0002, 5, 7, 900000 }, new double[] { 0.001, 1, 3, 4 });
    c = new EGCourbeChild(gr);
    c.setAspectContour(Color.RED);
    m.setTitle("toto rouge");
    c.setModel(m);

    gr.addEGComponent(c);

    EGAxeVertical y = new EGAxeVertical();
    y.setGraduations(true);
    y.setTraceGraduations(new TraceLigneModel(TraceLigne.LISSE, 1, Color.LIGHT_GRAY));
    y.setDroite(true);
    y.setAxisIterator(new LogarithmicNumberIterator());
    y.setBounds(0.0001, 10000);
    y.setLineColor(Color.blue);
    y.setTitre("essai 2");
    gr.setAxeY(y);
    grapheModel.add(gr);
    gr = new EGGroup();
    gr.setTitle("gr 2");
    m = new EGCourbeModelDefault(new double[] { 1, 8, 9, 10 }, new double[] { 10, 4, 2, 24 });
    c = new EGCourbeChild(gr);
    c.setAspectContour(Color.yellow);
    m.setTitle("toto jaune");
    c.setModel(m);
    gr.addEGComponent(c);
    y = new EGAxeVertical();
    y.setGraduations(true);
    y.setBounds(0, 55);
    y.setTitre("essai 1");
    gr.setAxeY(y);
    gr.addEGComponent(c);
    m = new EGCourbeModelDefault(new double[] { 1, 3, 4, 5 }, new double[] { 14, 54, 25, 43 });
    c = new EGCourbeChild(gr);
    m.setTitle("toto vert");
    c.setAspectContour(Color.green);
    c.setModel(m);
    gr.addEGComponent(c);
    grapheModel.add(gr);
    final JFrame f = new JFrame();
    f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    final BuPanel p = new BuPanel();
    p.setLayout(new BuBorderLayout());

    g.setPreferredSize(new Dimension(200, 300));
    g.setSize(g.getPreferredSize());
    final JTree t = new JTree(grapheModel);
    t.setSelectionModel(grapheModel.getSelectionModel());
    t.setRootVisible(false);
    p.add(new BuScrollPane(t), BuBorderLayout.EAST);
    p.doLayout();
    f.setContentPane(p);
    final EGFillePanel pn = new EGFillePanel(g);

    p.add(pn, BuBorderLayout.CENTER);
    final JMenu menu = new JMenu();
    menu.setName("essai");
    menu.setText("essai");
    pn.fillSpecificMenu(menu);
    final JMenuBar b = new JMenuBar();
    b.add(menu);
    f.setJMenuBar(b);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    f.pack();
    f.setVisible(true);
    g.axeUpdated();
    g.fullRepaint();

    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        g.axeUpdated();
        // g.restore();

      }
    });
  }
}