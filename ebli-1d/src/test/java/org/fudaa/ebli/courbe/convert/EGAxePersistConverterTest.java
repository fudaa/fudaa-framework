/*
 GPL 2
 */
package org.fudaa.ebli.courbe.convert;

import java.awt.*;

import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.courbe.EGAxeHorizontalPersist;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class EGAxePersistConverterTest extends TestCase {
  
  public EGAxePersistConverterTest(String testName) {
    super(testName);
  }
  
  public void testToStringVertical() {
    try {
      EGAxeVerticalPersist verticalPersist = new EGAxeVerticalPersist();
      verticalPersist.setDroite(true);
      verticalPersist.setRange(new CtuluRange(0, 10));
      verticalPersist.setFont(new Font("SansSerif", Font.PLAIN, 12));
      EGAxePersistConverter converter = new EGAxePersistConverter();
      String toXml = converter.toXml(verticalPersist);
      EGAxeVerticalPersist fromXml = converter.fromXmlVertical(toXml);
      assertNotNull(fromXml);
      assertEquals(verticalPersist.isDroite(), fromXml.isDroite());
      
      verticalPersist.setGrille(new TraceLigne(new TraceLigneModel(4, 4, Color.DARK_GRAY)));
      toXml = converter.toXml(verticalPersist);
      fromXml = converter.fromXmlVertical(toXml);
      TraceLigne grille = fromXml.getGrille();
      assertNotNull(grille);
      assertEquals(verticalPersist.getGrille().getModel(), fromXml.getGrille().getModel());
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
  
  public void testToStringHorizontal() {
    try {
      EGAxeHorizontalPersist hPersist = new EGAxeHorizontalPersist();
      hPersist.setLineColor(Color.YELLOW);
      hPersist.setRange(new CtuluRange(0, 10));
      EGAxePersistConverter converter = new EGAxePersistConverter();
      String toXml = converter.toXml(hPersist);
      EGAxeHorizontalPersist fromXml = converter.fromXmlHorizontal(toXml);
      assertNotNull(fromXml);
      CtuluRange range = fromXml.getRange();
      assertEquals(0, (int) range.getMin());
      assertEquals(10, (int) range.getMax());
      assertEquals(hPersist.getLineColor(), fromXml.getLineColor());
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
}
