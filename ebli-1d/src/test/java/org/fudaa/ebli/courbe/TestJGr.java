/*
 *  @file         TestGr.java
 *  @creation     26 ao�t 2003
 *  @modification $Date: 2007-03-09 08:38:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.courbe;

import gnu.trove.TIntArrayList;
import java.awt.geom.Point2D;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrSegment;

/**
 * @author deniger
 * @version $Id: TestJGr.java,v 1.7 2007-03-09 08:38:21 deniger Exp $
 */
public class TestJGr extends TestCase {

  private final double eps_;

  /**
   * constructeur par defaut.
   */
  public TestJGr() {
    super();
    eps_ = 1E-15;
  }


  public void testGrMorphisme() {
    final GrMorphisme m = GrMorphisme.translation(1, 2, 0);
    m.composition(GrMorphisme.rotation(1, 2, 0));
    final GrPoint p1 = new GrPoint(2, 4, 0);
    final GrPoint p2 = p1.applique(m);
    final Point2D p12d = new Point2D.Double(p1.x_, p1.y_);
    final Point2D p22d = new Point2D.Double(0, 0);
    m.creeAffineTransorm2D().transform(p12d, p22d);
    assertEquals(p2.x_, p22d.getX(), 1E-15);
    assertEquals(p2.y_, p22d.getY(), 1E-15);

  }

  /**
   * Test les GrSegment.
   */
  public void testGrSegment() {
    final GrSegment g = new GrSegment();
    g.o_ = new GrPoint(-1, 10, 100);
    g.e_ = new GrPoint(1, -5, 2);
    final GrBoite b = new GrBoite();
    g.boite(b);
    assertNotNull(b.o_);
    assertNotNull(b.e_);
    assertEquals(-1, b.o_.x_, eps_);
    assertEquals(-5, b.o_.y_, eps_);
    assertEquals(2, b.o_.z_, eps_);
    assertEquals(1, b.e_.x_, eps_);
    assertEquals(10, b.e_.y_, eps_);
    assertEquals(100, b.e_.z_, eps_);
    g.o_ = new GrPoint(4, -15, -2);
    g.e_ = new GrPoint(1, -5, 2);
    g.boite(b);
    assertEquals(1, b.o_.x_, eps_);
    assertEquals(-15, b.o_.y_, eps_);
    assertEquals(-2, b.o_.z_, eps_);
    assertEquals(4, b.e_.x_, eps_);
    assertEquals(-5, b.e_.y_, eps_);
    assertEquals(2, b.e_.z_, eps_);
  }

  /**
   * Test les Selection.
   */
  public void testSelection() {
    final CtuluListSelection l = new CtuluListSelection(20);
    l.setSelectionInterval(0, 15);
    int[] r = CtuluListSelection.isSelectionContiguous(l, 20);
    assertNotNull(r);
    assertEquals(r[0], 0);
    assertEquals(r[1], 15);
    l.remove(10);
    r = CtuluListSelection.isSelectionContiguous(l, 20);
    assertNull(r);
    l.addInterval(15, 19);
    r = CtuluListSelection.isSelectionContiguous(l, 20);
    assertNotNull(r);
    assertEquals(r[0], 11);
    assertEquals(r[1], 9);
    r = CtuluListSelection.isSelectionContiguous(l, 21);
    assertNull(r);
    l.remove(11);
    r = CtuluListSelection.isSelectionContiguous(l, 20);
    assertNotNull(r);
    assertEquals(r[0], 12);
    assertEquals(r[1], 9);
    l.remove(17);
    r = CtuluListSelection.isSelectionContiguous(l, 20);
    assertNull(r);
    l.setSelectionInterval(0, 0);
    r = CtuluListSelection.isSelectionContiguous(l, 20);
    assertNotNull(r);
    assertEquals(r[0], 0);
    assertEquals(r[1], 0);
    l.clear();
    l.add(163);
    l.add(164);
    l.add(165);
    l.add(166);
    r = CtuluListSelection.isSelectionContiguous(l, 200);
    assertNotNull(r);
    assertEquals(r[0], 163);
    assertEquals(r[1], 166);
    final int taille = 100;
    int[] selected = new int[] { 0, 50, 99 };
    CtuluListSelection sel = new CtuluListSelection(100);
    for (int i = selected.length - 1; i >= 0; i--) {
      sel.add(selected[i]);
    }
    for (int i = taille - 1; i >= 0; i--) {
      if (CtuluLibArray.findInt(selected, i) >= 0) {
        assertTrue(sel.isSelected(i));
      } else {
        assertFalse(sel.isSelected(i));
      }
    }
    sel.inverse(taille);
    for (int i = taille - 1; i >= 0; i--) {
      if (CtuluLibArray.findInt(selected, i) >= 0) {
        assertFalse(sel.isSelected(i));
      } else {
        assertTrue(sel.isSelected(i));
      }
    }
    selected = new int[] { 50, 51 };
    sel = new CtuluListSelection(taille);
    for (int i = selected.length - 1; i >= 0; i--) {
      sel.add(selected[i]);
    }
    for (int i = taille - 1; i >= 0; i--) {
      if (CtuluLibArray.findInt(selected, i) >= 0) {
        assertTrue(sel.isSelected(i));
      } else {
        assertFalse(sel.isSelected(i));
      }
    }
    sel.inverse(taille);
    for (int i = taille - 1; i >= 0; i--) {
      if (CtuluLibArray.findInt(selected, i) >= 0) {
        assertFalse(sel.isSelected(i));
      } else {
        assertTrue(sel.isSelected(i));
      }
    }

  }

  /**
   * Test les selection sous forme de tableau.
   */
  public void testSelectionArray() {
    int[] test = new int[] { 0, 1, 2, 3, 4 };
    assertEquals(0, EbliLib.isSelectionContiguous(test, 20));
    test = new int[] { 0, 1, 2, 3, 6 };
    assertEquals(-1, EbliLib.isSelectionContiguous(test, 20));
    assertEquals(4, EbliLib.isSelectionContiguous(test, 7));
    test = new int[] { 0, 1, 3, 6 };
    assertEquals(-1, EbliLib.isSelectionContiguous(test, 7));
  }

  public void testBoite() {
    final GrBoite b = new GrBoite();
    b.ajuste(new GrPoint(1, 1, 1));
    b.ajuste(new GrPoint(0, 2, 1));
    final double eps = 1E-15;
    assertEquals(b.o_.x_, 0, eps);
    assertEquals(b.o_.y_, 1, eps);
    assertEquals(b.e_.x_, 1, eps);
    assertEquals(b.e_.y_, 2, eps);
    try {
      final GrBoite b2 = (GrBoite) b.clone();
      assertEquals(b2.o_.x_, 0, eps);
      assertEquals(b2.o_.y_, 1, eps);
      assertEquals(b2.e_.x_, 1, eps);
      assertEquals(b2.e_.y_, 2, eps);
      b2.e_.y_ = 1.5;
      assertEquals(b.e_.y_, 2, eps);
      final GrBoite b3 = b.intersection(b2);
      assertEquals(b3.o_.x_, 0, eps);
      assertEquals(b3.o_.y_, 1, eps);
      assertEquals(b3.e_.x_, 1, eps);
      assertEquals(b3.e_.y_, 1.5, eps);

    } catch (final CloneNotSupportedException _evt) {
      fail(_evt.getMessage());

    }
  }

  /**
   * Norme.
   */
  public void testNorme() {
    final GrPoint p = new GrPoint(1, 1, 1);
    final GrPoint p1 = new GrPoint(101, 101, 1);
    assertEquals(Math.sqrt(20000), p.distance(p1), 1e-15);
    assertEquals(Math.sqrt(20000), p.distanceXY(p1), 1e-15);
  }

  public void testEGCourbe() {
    double[] x = new double[] { 0, 1, 2, 3, 4, 5 };
    double[] y = new double[] { 0, 10, 20, 30, 40, 50 };
    EGCourbe c = new EGCourbeSimple(null, new EGCourbeModelDefault(x, y));
    TIntArrayList list = new TIntArrayList();
    c.fillWithIdxStrictlyIncluded(-1, 5.3, list);
    assertEquals(x.length, list.size());
    for (int i = 0; i < x.length; i++) {
      assertEquals(i, list.get(i));
    }
    c.fillWithIdxStrictlyIncluded(-1, 0.3, list);
    assertEquals(1, list.size());
    assertEquals(0, list.get(0));
    c.fillWithIdxStrictlyIncluded(0, 0.3, list);
    assertEquals(0, list.size());
    c.fillWithIdxStrictlyIncluded(0, 1.3, list);
    assertEquals(1, list.size());
    assertEquals(1, list.get(0));

  }

  /**
   * Distance entre point et segment.
   */
  public void testDistanceSegment() {
    final GrPoint p = new GrPoint(0, 0, 0);
    final GrPoint p1 = new GrPoint(0, 5, 0);
    final GrPoint p2 = new GrPoint(5, 0, 0);
    final GrSegment s = new GrSegment(p1, p2);
    assertEquals(Math.sqrt(2) * 5d / 2d, s.distanceXY(p), 1e-15);
    GrPoint pTest = new GrPoint(-5, 10, 0);
    assertEquals(Math.sqrt(50), s.distanceXY(pTest), 1e-15);
    pTest = new GrPoint(10, -5, 0);
    assertEquals(Math.sqrt(50), s.distanceXY(pTest), 1e-15);
    final GrPolygone poly = new GrPolygone();
    poly.sommets_.ajoute(p);
    poly.sommets_.ajoute(p1);
    poly.sommets_.ajoute(p2);
    GrPoint p4 = new GrPoint(1, 1, 1);
    assertEquals(1d, poly.distanceXY(p4), 1e-15);
    p4 = new GrPoint(5, 5, 1);
    assertEquals(Math.sqrt(2) * 5d / 2d, poly.distanceXY(p4), 1e-15);
    p4 = new GrPoint(3, -1, 1);
    assertEquals(1, poly.distanceXY(p4), 1e-15);
    p4 = new GrPoint(-2, 3, 1);
    assertEquals(2, poly.distanceXY(p4), 1e-15);
    final GrPolyligne li = poly.toGrPolyligne();
    p4 = new GrPoint(5, 5, 1);
    assertEquals(Math.sqrt(2) * 5d / 2d, li.distanceXY(p4), 1e-15);
    p4 = new GrPoint(-2, 3, 1);
    assertEquals(2, li.distanceXY(p4), 1e-15);
    p4 = new GrPoint(3, -2, 1);
    assertEquals(Math.sqrt(8), li.distanceXY(p4), 1e-15);

  }
}
