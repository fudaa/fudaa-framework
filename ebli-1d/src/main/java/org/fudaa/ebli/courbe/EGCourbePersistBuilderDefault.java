package org.fudaa.ebli.courbe;

import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;

public class EGCourbePersistBuilderDefault extends EGCourbePersistBuilder<EGCourbeChild> {

  @Override
  protected EGCourbeChild createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    EGGroup parent = getGroup(params);
    EGCourbeChild egCourbeChild = new EGCourbeChild(parent);
    egCourbeChild.setModel(createModel(target, params));
    return egCourbeChild;
  }

  
}
