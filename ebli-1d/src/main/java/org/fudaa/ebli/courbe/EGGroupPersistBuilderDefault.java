package org.fudaa.ebli.courbe;

import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;

public class EGGroupPersistBuilderDefault extends EGGroupPersistBuilder<EGGroup> {

  @Override
  protected EGGroup createEGObject(EGGroupPersist target, Map params, CtuluAnalyze log) {
    return new EGGroup();
  }
}
