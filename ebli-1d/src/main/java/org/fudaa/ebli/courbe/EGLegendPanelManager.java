/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import com.itextpdf.text.Font;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuVerticalLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Frederic Deniger
 */
public class EGLegendPanelManager extends MouseAdapter implements EGGrapheModelListener {

  private class LegendLine {

    String txt;
    String tooltip;
    EGCourbe courbe;
    int padding;
  }

  JPanel panel;
  JPanel mainPanel;
  EGGraphe graphe;

  void createPanel() {
    panel = new JPanel(new BuVerticalLayout(1, false, false));
    panel.setOpaque(false);
    panel.setDoubleBuffered(false);
    mainPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    mainPanel.setDoubleBuffered(false);
    mainPanel.setOpaque(false);
    mainPanel.add(panel);
    rebuild();
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (graphe == null) {
      return;
    }
    if (e.getClickCount() >= 2) {
      JComponent jc = (JComponent) e.getComponent();
      Object clientProperty = jc.getClientProperty("COURBE");
      if (clientProperty != null) {
        EGPaletteLegendeGraphe pnLegend = new EGPaletteLegendeGraphe(graphe);
        pnLegend.selectCourbe((EGCourbe) clientProperty);
        pnLegend.afficheModale(panel);
      }

    }
  }

  public void setGraphe(EGGraphe graphe) {
    if (this.graphe != null) {
      this.graphe.getModel().removeModelListener(this);
    }
    this.graphe = graphe;
    if (graphe == null) {
      return;
    }
    this.graphe.getModel().addModelListener(this);
    if (panel != null) {
      rebuild();
    }
  }
  boolean editable = true;

  public boolean isEditable() {
    return editable;
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  @Override
  public void structureChanged() {
    rebuild();
  }

  @Override
  public void courbeContentChanged(EGObject _c, boolean _mustRestore) {
  }

  @Override
  public void courbeAspectChanged(EGObject _c, boolean _visibil) {
    rebuild();
  }

  @Override
  public void axeContentChanged(EGAxe _c) {
  }

  @Override
  public void axeAspectChanged(EGAxe _c) {
  }

  public EGGraphe getGraphe() {
    return graphe;
  }

  public JPanel getPanel() {
    if (mainPanel == null) {
      createPanel();
    }
    return mainPanel;
  }

  private String groupByLastSeparator;

  public String getGroupByLastSeparator() {
    return groupByLastSeparator;
  }

  /**
   * Si non null, sera utilis� pour afficher les courbes par groupe. Par exemple les courbes C1 - Toto / 1, C2 - Toto / 1, ... seront affich�es
   * <pre>
   * Toto / 1
   *    C1
   *    C2
   * </pre>
   *
   * @param groupByLastSeparator si null, pas de groupement par suffixe
   */
  public void setGroupByLastSeparator(String groupByLastSeparator) {
    this.groupByLastSeparator = groupByLastSeparator;
  }

  private void rebuild() {
    panel.removeAll();
    panel.setDoubleBuffered(false);
    EGCourbe[] courbes = graphe.getModel().getCourbes();
    List<LegendLine> lines = new ArrayList<>();
    for (EGCourbe courbe : courbes) {
      if (!courbe.isVisible_) {
        continue;
      }
      LegendLine line = new LegendLine();
      line.courbe = courbe;
      line.tooltip = courbe.getTitle();
      line.txt = courbe.getTitle();
      lines.add(line);
    }
    manageGroupByPrefix(lines);
    for (LegendLine line : lines) {
      addLabelToPanel(line);
    }
    panel.revalidate();
    panel.repaint(0);
  }

  /**
   * Si un groupement par pr�fixe est demand�, les lignes vont �tre r�agenc�es pour cela.
   *
   * @param lines
   */
  private void manageGroupByPrefix(List<LegendLine> lines) {
    if (lines.size() > 0 && StringUtils.isNotBlank(groupByLastSeparator)) {
      //contiend les lines par prefixes
      LinkedHashMap<String, List<LegendLine>> linesByPrefix = new LinkedHashMap<>();
      for (LegendLine line : lines) {
        String prefix = StringUtils.substringAfterLast(line.txt, groupByLastSeparator);
        List<LegendLine> foundList = linesByPrefix.get(prefix);
        if (foundList == null) {
          foundList = new ArrayList<>();
          linesByPrefix.put(prefix, foundList);
        }
        foundList.add(line);
      }
      lines.clear();
      for (Map.Entry<String, List<LegendLine>> entry : linesByPrefix.entrySet()) {
        String key = entry.getKey();
        List<LegendLine> linesByTitle = entry.getValue();
        //s'il y a plusieurs courbes pour le pr�fixe on fait un padding de 5 et on enl�ve le padding
        //si une seule line ou le pr�fixe est vide on ne fait rien
        if (StringUtils.isNotBlank(key) && linesByTitle.size() > 1) {
          LegendLine titleLine = new LegendLine();
          titleLine.tooltip = key;
          titleLine.txt = key;
          lines.add(titleLine);
          for (LegendLine legendLine : linesByTitle) {
            legendLine.txt = StringUtils.substringBeforeLast(legendLine.txt, groupByLastSeparator).trim();
            legendLine.padding = 15;
          }
        }
        lines.addAll(linesByTitle);
      }
    }
  }

  private void addLabelToPanel(LegendLine line) {
    JLabel label = new JLabel();

    label.setText(line.txt);
    label.setToolTipText(line.tooltip);
    if (line.courbe != null) {
      EGIconForCourbe icon = new EGIconForCourbe();
      label.setIcon(icon);
      icon.updateFromCourbe(line.courbe);
      label.putClientProperty("COURBE", line.courbe);
      label.addMouseListener(this);
    } else {
      label.setFont(BuLib.deriveFont(label.getFont(), Font.BOLD, 1));
    }
    label.setOpaque(false);
    label.setBorder(BorderFactory.createEmptyBorder(0, line.padding, 0, 0));
    panel.add(label);
  }
}
