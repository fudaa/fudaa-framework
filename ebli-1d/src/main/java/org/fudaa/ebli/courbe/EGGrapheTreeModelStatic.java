/*
 * @creation 18 sept. 06
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

/**
 * @author fred deniger
 * @version $Id: EGGrapheTreeModelStatic.java,v 1.1 2006-09-19 14:55:53 deniger Exp $
 */
public class EGGrapheTreeModelStatic extends EGGrapheTreeModel {

  @Override
  public boolean canAddCourbe() {
    return false;
  }

  @Override
  public boolean isContentModifiable() {
    return false;
  }

  @Override
  public boolean isStructureModifiable() {
    return false;
  }

}
