/*
 * @creation 8 mars 07
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.Map;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import org.fudaa.ebli.animation.EbliAnimationSourceAbstract;

public class EGFillePanelAnimAdapter extends EbliAnimationSourceAbstract {

  final EGFillePanel target_;

  public EGFillePanelAnimAdapter(final EGFillePanel _target) {
    target_ = _target;
  }

  @Override
  public Component getComponent() {
    return target_;
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return target_.getDefaultImageDimension();
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return target_.produceImage(_params);
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return target_.produceImage(_w, _h, _params);
  }

  @Override
  public void setVideoMode(final boolean _b) {
    // pour eviter qu'un thread soit cree par la vue calque
    Container f = SwingUtilities.getAncestorOfClass(JInternalFrame.class, target_);
    if (f == null) {
      f = SwingUtilities.getAncestorOfClass(Dialog.class, target_);
      if (f != null) {
        ((Dialog) f).setResizable(!_b);
      }
    } else {
      ((JInternalFrame) f).setResizable(!_b);
    }
  }
}