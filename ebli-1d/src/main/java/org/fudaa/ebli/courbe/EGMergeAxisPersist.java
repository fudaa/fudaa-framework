package org.fudaa.ebli.courbe;

public class EGMergeAxisPersist {
  
  private int[] groupeUsingTheAxis;
  private EGAxeVerticalPersist persist;
  /**
   * @param groupeUsingTheAxis
   * @param persist
   */
  public EGMergeAxisPersist(int[] groupeUsingTheAxis, EGAxeVertical persist) {
    super();
    this.groupeUsingTheAxis = groupeUsingTheAxis;
    this.persist = new EGAxeVerticalPersist(persist);
  }
  public int[] getGroupeUsingTheAxis() {
    return groupeUsingTheAxis;
  }
  public void setGroupeUsingTheAxis(int[] groupeUsingTheAxis) {
    this.groupeUsingTheAxis = groupeUsingTheAxis;
  }
  public EGAxeVerticalPersist getPersist() {
    return persist;
  }
  public void setPersist(EGAxeVerticalPersist persist) {
    this.persist = persist;
  }

}
