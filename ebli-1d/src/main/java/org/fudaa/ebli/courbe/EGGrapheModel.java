/*
 *  @creation     2 sept. 2004
 *  @modification $Date: 2007-02-01 16:12:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Component;
import java.util.List;
import java.util.Map;
import javax.swing.JComboBox;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.commun.EbliActionInterface;

/**
 * @author Fred Deniger
 * @version $Id: EGGrapheModel.java,v 1.8 2007-02-01 16:12:04 deniger Exp $
 */
public interface EGGrapheModel extends EGParent {

  /**
   * @return le nombre d'objet graphique utilisee par ce modele
   */
  int getNbEGObject();

  boolean isSomethingToDisplay();
  
  /**
   * @returnt true if the model is on update state.
   */
  boolean isUpdating();

  boolean contains(EGCourbe _c);

  /**
   * @param _i l'indice demande
   * @return l'objet d'indice _i ou null si hors limite
   */
  EGObject getEGObject(int _i);

  JComboBox createCbForSelectCourbe();
  JComboBox createCbForSelectCourbe(EGAxeVertical _vertical);

  /**
   * @return toutes les courbes du modele
   */
  EGCourbe[] getCourbes();

  boolean canDeleteCourbes();

  /**
   * Pour savoir, si les courbes peuvent etre dupliquees,supprimees, copiees ou renommees.
   * 
   * @return true si la structure de ce modele peut-etre modifiee
   */
  boolean isStructureModifiable();

  
  
  
  /**
   * @return true si le contenu de courbe peut etre modifie
   */
  boolean isContentModifiable();

  /**
   * @return true si des courbes peuvent etre ajout�es
   */
  boolean canAddCourbe();

  /**
   * Utiliser que si des courbes peuvent etre ajoutees.
   * 
   * @param _b l'action d'ajout de courbes a decorer
   */
  void decoreAddButton(EbliActionInterface _b);

  /**
   * @param _cmd le manager de commande
   * @param _parent le composant parent a utiliser pour afficher une boite de dialogue
   * @param _graphe TODO
   */
  void addNewCourbe(CtuluCommandManager _cmd, Component _parent, EGGraphe _graphe);

  /**
   * @param _l le listener d'evt pour la selection a ajouter
   */
  void addSelectionListener(EGSelectionListener _l);

  /**
   * @param _l le listener d'evt pour la selection a enlever
   */
  void removeSelectionListener(EGSelectionListener _l);

  /**
   * @param _l le listener de modele a ajouter
   */
  void addModelListener(EGGrapheModelListener _l);

  /**
   * @param _l le listener de modele a supprimer
   */
  void removeModelListener(EGGrapheModelListener _l);

  /**
   * @return la courbe selectionnee ou null si pas de courbe selectionnee
   */
  EGCourbe getSelectedComponent();

  void setSelectedComponent(EGCourbe _c);

  /**
   * @return l'objet selectionne: peut etre un groupe
   */
  EGObject getSelectedObject();

  /**
   * @return tous les objets selectionnes
   */
  EGObject[] getSelectedObjects();

  /**
   * @return le nombre d'objets selectionnes
   */
  int getNbSelectedObjects();

  @Override
  void fireAxeAspectChanged(EGAxe _a);

  void fireAxeContentChanged(EGAxe _a);

  void fireStructureChanged();

  void setAxeX(EGAxeHorizontal _h);

  /**
   * Enlever les courbes selectionnees.
   * 
   * @param _mng le manager de commande
   */
  void removeSelectedCurves(CtuluCommandManager _mng);

  /**
   * Dupliquer la courbe _c.
   * 
   * @param _mng le manager de commande
   * @param _c la courbe a dupliquer
   */
  void duplicateCourbe(CtuluCommandManager _mng, EGCourbe _c);
  
  
  /**
   * Dupliquer la courbe pour les sources choisis
   * 
   * @param _mng
   * @param _c
   */
  void duplicateCourbeForSrc(CtuluCommandManager _mng, EGCourbe _c);
  /**
   * methode qui permet de dupliquer les models de courbe
   * @param _duplicator TODO
   */
  EGGrapheModel duplicate(EGGrapheDuplicator _duplicator);
  
  
  
  /**
   * Retourne les donn�es persistantes sp�cifique du graphe model.
   *  Est utile pour les graphes de haut niveau m�tier
   * Ces donn�es pourront etre facilement serializee par un parser xml, bdd, ou texte
   * @return
   */
  Object getSpecificPersitDatas(Map Params);
  
  
  /**
   * Insere les donn�es sp�cifiques persistantes dans le modele.
   *Ces donn�es proviennent de fichier de sauvegardes (xml,texte,bdd...) 
   * @return
   */
  void setSpecificPersitDatas(Object sepcPersitData, Map Params);
  
  
  /**
   * Methode qui retournes les actions sp�cifiques AUX COURBES d'un graphe en fonction de son treemodel.
   * Ces actions seront situ�es dans le menu popup de la courbe (ie via clic droit sur la courbe dans l'arbre des courbes).
   * Par exemple dans tr, le TrPostCourbeTreeModel retourne des actions d'ajout de variables pour les �volutions temporelles.
   * @return
   */
  List<EbliActionInterface> getSpecificsActionsCurvesOnly(EGGraphe _target, CtuluUI ui);
  

}
