/**
 *  @creation     22 juin 2004
 *  @modification $Date: 2006-09-19 14:55:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.JComponent;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractiveComponent.java,v 1.5 2006-09-19 14:55:53 deniger Exp $
 */
public abstract class EGInteractiveComponent extends JComponent {

  private boolean active_;

  public void setActive(final boolean _b) {
    if (_b != active_) {
      active_ = _b;
      firePropertyChange("active",!active_,active_);
    }
  }
  public abstract String getDescription();
  public final boolean isActive() {
    return active_;
  }
  
  protected void internFireStateChanged(){
    if(isActive()) {
      firePropertyChange("state", null, getDescription());
    }
  }
  

}