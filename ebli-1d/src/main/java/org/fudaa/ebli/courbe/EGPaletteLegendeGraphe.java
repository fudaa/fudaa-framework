package org.fudaa.ebli.courbe;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import gnu.trove.TIntArrayList;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.gui.CtuluDecimalFormatEditPanel;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Palette de l�gende des graphes. Utilis�e pour �diter les l�gendes des graphes
 *
 * @author Adrien Hadoux
 */
public class EGPaletteLegendeGraphe extends CtuluDialogPanel implements ActionListener, ListSelectionListener,
        BuBorders {
  
  private final class EGGrapheModelListenerImplementation implements EGGrapheModelListener {
    
    @Override
    public void structureChanged() {
    }
    
    @Override
    public void courbeContentChanged(final EGObject c, final boolean mustRestore) {
    }
    
    @Override
    public void courbeAspectChanged(final EGObject c, final boolean visibil) {
      model_.fireContentsChanged(this, 0, model_.getSize() - 1);
      
    }
    
    @Override
    public void axeContentChanged(final EGAxe c) {
    }
    
    @Override
    public void axeAspectChanged(final EGAxe c) {
    }
  }
  
  class CustomDefaultListModel extends DefaultListModel {
    
    @Override
    public void fireContentsChanged(Object source, int index0, int index1) {
      super.fireContentsChanged(source, index0, index1);
    }
    
    @Override
    public void fireIntervalAdded(Object source, int index0, int index1) {
      super.fireIntervalAdded(source, index0, index1);
    }
    
    @Override
    public void fireIntervalRemoved(Object source, int index0, int index1) {
      super.fireIntervalRemoved(source, index0, index1);
    }
  }
  /**
   * Graphe associ� a la palette
   */
  private final EGGraphe graphe_;
  /**
   * Bouton qui applique les changements
   */
  private JButton btApply_;
  private BuButton btFormat_;
  private JButton btRefresh_;
  EGConfigureActionPalette paletteAction;
  /**
   * Format des nombres des l�gendes
   */
  private CtuluNumberFormatI format_ = CtuluNumberFormatDefault.DEFAULT_FMT;
  /**
   * S�parateur entre les formats
   */
  private String separatorFormat_ = "-";
  /**
   * La liste des courbes
   */
  private JList list_;
  CustomDefaultListModel model_;
  private EGGrapheModelListenerImplementation modelListener;
  
  public EGPaletteLegendeGraphe(final EGGraphe graphe) {
    super(false);
    graphe_ = graphe;
    buildContent();
  }
  
  @Override
  public boolean cancel() {
    graphe_.removeModelListener(modelListener);
    for (Map.Entry<EGCourbe, EGCourbePersist> entry : initCourbeConfig.entrySet()) {
      EGCourbe courbe = entry.getKey();
      courbe.applyPersitUiConfig(entry.getValue());
      courbe.fireCourbeAspectChanged(false);
    }
    for (Map.Entry<EGAxeVertical, EGAxeVerticalPersist> entry : initAxeConfig.entrySet()) {
      EGAxeVertical axe = entry.getKey();
      EGAxeVerticalPersist eGAxeVerticalPersist = entry.getValue();
      eGAxeVerticalPersist.apply(axe);
//      axe.fireCourbeAspectChanged(false);
    }
    if (initAxeHorizontal != null) {
      initAxeHorizontal.applyButRange(graphe_.getTransformer().getXAxe());
    }
    return super.cancel();
  }
  
  @Override
  public boolean ok() {
    graphe_.removeModelListener(modelListener);
    return super.ok();
  }
  
  @Override
  public void closeDialog() {
    graphe_.removeModelListener(modelListener);
    super.closeDialog();
  }

  /**
   * Appelee lorsqu'on appuie sur le bouton appliquer
   */
  private void actionApply() {
  }
  
  private void actionFormat() {
    final CtuluNumberFormatI ctuluNumberFormatI = format_;
    /*
     * CtuluNumberFormatI fmt = getDefaultFormat(); if (fmt == null) { fmt = CtuluLib.DEFAULT_NUMBER_FORMAT; }
     */
    final CtuluDecimalFormatEditPanel fmtSelect = new CtuluDecimalFormatEditPanel(ctuluNumberFormatI);
    fmtSelect.setErrorTextUnable();
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        return fmtSelect.isDataValid();
      }
    };
    pn.setLayout(new BuVerticalLayout(4));
    final JPanel pnSep = new BuPanel(new BuGridLayout(2, 2, 2));
    pnSep.add(new BuLabel(EbliLib.getS("S�parateur")));
    final BuTextField tf = new BuTextField();
    tf.setText(separatorFormat_);
    pnSep.add(tf);
    pn.add(pnSep);
    
    if (ctuluNumberFormatI == null || ctuluNumberFormatI.isDecimal()) {
      fmtSelect.setBorder(BorderFactory.createTitledBorder(EbliLib.getS("Format d�cimal")));
      pn.add(fmtSelect);
    }
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(this, EbliLib.getS("Format")))) {
      format_ = fmtSelect.getCurrentFmt();
      separatorFormat_ = tf.getText();
      ajusteAllLegendes();
      
    }
  }
  
  @Override
  public void actionPerformed(final ActionEvent _e) {
    
    final Object s = _e.getSource();
    if (s == btApply_) {
      actionApply();
    } else if (s == btRefresh_) {
      reinitPlages();
    } else if (btFormat_ == _e.getSource()) {
      actionFormat();
    }
    
  }

  /**
   * Methode qui ajuste toutes les l�gendes en fonction du format choisi et du separator.
   */
  protected void ajusteAllLegendes() {
  }
  
  public int getLine(EGCourbe c) {
    int nb = model_.getSize();
    for (int i = 0; i < nb; i++) {
      if (model_.get(i) == c) {
        return i;
      }
    }
    return -1;
  }
  
  public void selectCourbe(EGCourbe c) {
    int idx = getLine(c);
    if (idx >= 0) {
      list_.getSelectionModel().setSelectionInterval(idx, idx);
    }
  }
  final Map<EGCourbe, EGCourbePersist> initCourbeConfig = new HashMap<>();
  final Map<EGAxeVertical, EGAxeVerticalPersist> initAxeConfig = new HashMap<>();
  EGAxeHorizontalPersist initAxeHorizontal;
  
  private void buildContent() {
    final EGCourbe[] cs = graphe_.getModel().getCourbes();
    for (EGCourbe eGCourbe : cs) {
      initCourbeConfig.put(eGCourbe, eGCourbe.getPersitUiConfig());
    }
    List allAxeVertical = graphe_.getAllAxeVertical();
    for (Object object : allAxeVertical) {
      EGAxeVertical axe = (EGAxeVertical) object;
      initAxeConfig.put(axe, new EGAxeVerticalPersist(axe));
    }
    initAxeHorizontal = new EGAxeHorizontalPersist(graphe_.getTransformer().getXAxe());
    
    final BuBorderLayout lay = new BuBorderLayout(2, 2);
    setLayout(lay);

    // -- liste des courbes --//
    list_ = new JList();
    modelListener = new EGGrapheModelListenerImplementation();
    graphe_.getModel().addModelListener(modelListener);
    model_ = new CustomDefaultListModel();
    EGObject[] selectedObjects = graphe_.getModel().getSelectedObjects();
    TIntArrayList selection = new TIntArrayList();
    for (int i = 0; i < cs.length; i++) {
      model_.addElement(cs[i]);
      if (CtuluLibArray.containsObject(selectedObjects, cs[i])) {
        selection.add(i);
      }
    }
    list_.setModel(model_);
    list_.setCellRenderer(new EGCourbeCellRenderer());
    list_.getSelectionModel().addListSelectionListener(this);
    list_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    
    final BuScrollPane sp = new BuScrollPane(list_);
    sp.setPreferredSize(new Dimension(600, 100));
    sp.setBorder(BorderFactory.createTitledBorder(EbliResource.EBLI.getString("Liste des courbes")));
    add(sp, BuBorderLayout.NORTH);
    
    final BuPanel pnColor = new BuPanel();
    pnColor.setLayout(new BuButtonLayout(1, SwingConstants.LEFT));

    // -- update le panel des donn�es

    paletteAction = new EGConfigureActionPalette();
    final JComponent paletteComp = paletteAction.getPaletteContent();
    paletteComp.setPreferredSize(new Dimension(550, 550));
    updatePanelData();
    paletteComp.setBorder(BorderFactory.createTitledBorder(EbliResource.EBLI.getString("Param�trage")));
    
    add(paletteComp, BorderLayout.CENTER);
    if (selection.size() > 0) {
      list_.setSelectedIndices(selection.toNativeArray());
    }
    btApply_ = new BuButton(BuResource.BU.getIcon("appliquer"));
    btApply_.setText(BuResource.BU.getString("Appliquer"));
    btApply_.setToolTipText(EbliLib.getS("Appliquer les modifications"));
    btApply_.addActionListener(this);
    btRefresh_ = new BuButton(BuResource.BU.getIcon("rafraichir"));
    btRefresh_.setText(EbliLib.getS("R�initialiser les titres"));
    btRefresh_.setToolTipText(EbliLib.getS("R�initialiser les titres"));
    btRefresh_.addActionListener(this);
    btRefresh_.setVisible(btRefreshVisible);
    btRefresh_.setEnabled(true);
    final BuPanel btpn = new BuPanel();
    btpn.setLayout(new BuButtonLayout(1, SwingConstants.RIGHT));
    
    btpn.add(btRefresh_);
    add(btpn, BuBorderLayout.SOUTH);
    
    this.setPreferredSize(new Dimension(550, 850));
    this.setMinimumSize(new Dimension(550, 850));
  }
  private boolean btRefreshVisible = true;
  
  public void setBtRefreshTitleVisible(boolean visible) {
    btRefreshVisible = visible;
    if (btRefresh_ != null) {
      btRefresh_.setVisible(visible);
    }
  }
  
  private EGCourbe[] getSelectedCourbes() {
    final int[] select = this.list_.getSelectedIndices();
    if (select == null) {
      return null;
    }
    final EGCourbe[] res = new EGCourbe[select.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = (EGCourbe) list_.getModel().getElementAt(select[i]);
    }
    return res;
  }

  /**
   * Reinitialise les titres des courbes avec les donn�es initiales.
   */
  private void reinitPlages() {
    graphe_.reinitTitlesOrigins();
    list_.revalidate();
  }

  /**
   * Met a jour le panneau des donn�es.
   */
  private void updatePanelData() {
    paletteAction.setPaletteTarget(getSelectedCourbes());
  }
  
  @Override
  public void valueChanged(final ListSelectionEvent e) {
    updatePanelData();
  }
}
