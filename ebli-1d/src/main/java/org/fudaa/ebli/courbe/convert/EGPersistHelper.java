/*
 GPL 2
 */
package org.fudaa.ebli.courbe.convert;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import java.awt.Color;
import java.awt.Font;
import javax.swing.plaf.FontUIResource;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.converter.CtuluRangeToStringTransformer;
import org.fudaa.ebli.converter.ToStringTransfomerXstreamConverter;
import org.fudaa.ebli.converter.TraceLigneModelToStringTransformer;
import org.fudaa.ebli.converter.TraceLigneToStringXstreamConverter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class EGPersistHelper {

  public static void registerDefaultConverters(XStream xstream, TraceToStringConverter traceToStringConverter) {
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(traceToStringConverter.getColorToStringTransformer(), Color.class));
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(traceToStringConverter.getFontToStringTransformer(), Font.class));
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(traceToStringConverter.getFontToStringTransformer(), FontUIResource.class));
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(traceToStringConverter.getTraceBoxToStringTransformer(), TraceBox.class));
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(traceToStringConverter.getTraceIconModelToStringTransformer(), TraceIconModel.class));
    final TraceLigneModelToStringTransformer traceLigneModelToStringTransformer = traceToStringConverter.getTraceLigneModelToStringTransformer();
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(traceLigneModelToStringTransformer, TraceLigneModel.class));
    xstream.registerConverter(new ToStringTransfomerXstreamConverter(new CtuluRangeToStringTransformer(), CtuluRange.class));
    xstream.registerConverter(new TraceLigneToStringXstreamConverter(traceLigneModelToStringTransformer));
  }

  public static XStream createXstream() {
    final XmlFriendlyNameCoder replacer = new XmlFriendlyNameCoder("#", "_");
    final StaxDriver staxDriver = new StaxDriver(replacer);
    final XStream xstream = new XStream(staxDriver);
    XStream.setupDefaultSecurity(xstream);
    xstream.allowTypesByWildcard(new String[] {
            "org.fudaa.**"
    });
    return xstream;
  }
}
