/*
 * @creation 7 mai 07
 * @modification $Date: 2007-05-21 10:28:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.fu.FuEmptyArrays;
import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.collection.CourbeInterface;

/**
 * @author fred deniger
 * @version $Id: EvolutionOperations.java,v 1.1 2007-05-21 10:28:26 deniger Exp $
 */
public final class EvolutionOperations {

  private EvolutionOperations() {}

  public static int[] getIdxRemovable(CourbeInterface _evol) {
    return getIdxRemovable(_evol, 0.0001);
  }

  public static int[] getIdxRemovable(CourbeInterface _evol, final double _pourceDiff) {
    int nbVal = _evol.getNbValues();
    if (nbVal <= 2) return null;
    TIntArrayList res = new TIntArrayList(nbVal - 2);
    final Coordinate prec = new Coordinate(_evol.getX(0), _evol.getY(0));
    final Coordinate suiv = new Coordinate(0, 0);
    final Coordinate iCoord = new Coordinate(0, 0);
    for (int i = 1; i < nbVal - 1; i++) {
      iCoord.x = _evol.getX(i);
      iCoord.y = _evol.getY(i);
      suiv.x = _evol.getX(i + 1);
      suiv.y = _evol.getY(i + 1);
      double err = CGAlgorithms.distancePointLinePerpendicular(iCoord, prec, suiv);
      if ((iCoord.y == 0 && CtuluLib.isZero(err, _pourceDiff))
          || (iCoord.y != 0 && Math.abs(err / iCoord.y) < _pourceDiff)) {
        res.add(i);
      } else {
        prec.setCoordinate(iCoord);
      }

    }
    return res.size() == 0 ? FuEmptyArrays.INT0 : res.toNativeArray();
  }
}
