/*
 * @creation 9 ao�t 2004
 * @modification $Date: 2007-05-22 14:19:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.table.AbstractTableModel;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Un model pour pour les courbes.
 *
 * @author Fred Deniger
 * @version $Id: EGTableModel.java,v 1.16 2007-05-22 14:19:04 deniger Exp $
 */
public class EGTableModel extends AbstractTableModel {

  EGCourbe c_;
  EGGraphe a_;
  protected int xColIndex = 0;
  protected int yColIndex = 1;

  public EGTableModel() {
  }

  protected void setGraphe(final EGGraphe _a) {
    a_ = _a;
  }

  public int getXColIndex() {
    return xColIndex;
  }

  public int getYColIndex() {
    return yColIndex;
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  public EGCourbe getCourbe() {
    return c_;
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    if (c_.getModel().isModifiable() && c_.getModel().isPointDrawn(_rowIndex)) {
      if (_columnIndex == 1) {
        return true;
      }
      return c_.getModel().isXModifiable();
    }
    return false;
  }

  protected boolean isModelModifiable() {
    return c_ != null && c_.getModel() != null && c_.getModel().isModifiable();
  }

  protected boolean isModelXModifiable() {
    return isModelModifiable() && c_.getModel().isXModifiable();
  }

  @Override
  public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
    if (_rowIndex >= c_.getModel().getNbValues()) {
      return;
    }
    double x = c_.getModel().getX(_rowIndex);
    double y = c_.getModel().getY(_rowIndex);
    if (_columnIndex == xColIndex) {
      try {
        x = Double.parseDouble(_value.toString());
      } catch (final NumberFormatException _e) {
        return;
      }
    } else if (_columnIndex == yColIndex) {
      try {
        y = Double.parseDouble(_value.toString());
      } catch (final NumberFormatException _e) {
        return;
      }
    }
    c_.getModel().setValue(_rowIndex, x, y, a_.getCmd());
  }

  public boolean setValueAt(final double _val, final int _rowIndex, final int _columnIndex,
          final CtuluCommandContainer _cmd) {
    final double x = _columnIndex == xColIndex ? _val : c_.getModel().getX(_rowIndex);
    final double y = _columnIndex == yColIndex ? _val : c_.getModel().getY(_rowIndex);
    return c_.getModel().setValue(_rowIndex, x, y, _cmd);
  }

  public void addValueAt(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    c_.getModel().addValue(_x, _y, _cmd);
  }

  public final boolean addValueAt(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    return c_.getModel().addValue(_x, _y, _cmd);
  }

  protected void selectedCourbeChanged(final EGCourbe _a) {
    c_ = _a;
    fireTableDataChanged();
  }

  public EGAxeHorizontal getH() {
    return a_ == null ? null : a_.getTransformer().getXAxe();
  }

  @Override
  public String getColumnName(final int _column) {

    String res = null;
    if (getH() != null && _column == xColIndex) {
      res = getH().titre_;
    } else if (_column == yColIndex && c_ != null && c_.getAxeY() != null) {
      res = c_.getAxeY().titre_;
    }
    if (res == null && _column == 0) {
      return EbliLib.getS("Sel.");
    }
    return (res == null || res.length() == 0) ? CtuluLibString.ESPACE : res;
  }

  @Override
  public int getRowCount() {
    if (c_ == null) {
      return 3;
    }
    return c_.getModel().getNbValues();
  }

  public boolean isPointDrawn(final int _i) {
    return c_ != null && c_.getModel().isPointDrawn(_i);
  }

  protected String getX(final double _d) {
    final EGAxeHorizontal h = getH();
    if (h != null && h.isSpecificFormat()) {
      return h.getSpecificFormat().format(_d);
    }
    return Double.toString(_d);
  }

  protected String getXDetailAsString(int row) {
    return getXDetail(getInitX(row));
  }

  protected String getYDetailAsString(int row) {
    return getYDetail(getInitY(row));
  }

  protected String getXDetail(final double _d) {
    final EGAxeHorizontal h = getH();

    if (h != null) {
      if (h.getSpecificDetailFormat() == null) {
        return getX(_d);
      }
      return h.getSpecificDetailFormat().format(_d);
    }
    return Double.toString(_d);
  }

  protected String getY(final double _d) {
    final EGAxeVertical h = c_ == null ? null : c_.getAxeY();
    if (h != null && h.isSpecificFormat()) {
      return h.getSpecificFormat().format(_d);
    }
    return Double.toString(_d);
  }

  protected String getYDetail(final double _d) {
    final EGAxeVertical h = c_ == null ? null : c_.getAxeY();
    if (h != null) {
      if (h.getSpecificDetailFormat() == null) {
        return getY(_d);
      }
      return h.getSpecificDetailFormat().format(_d);
    }
    return Double.toString(_d);
  }

  public double getInitX(int rowIndex) {
    return c_.getModel().getX(rowIndex);
  }

  public double getInitY(int rowIndex) {
    return c_.getModel().getY(rowIndex);
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    final EGAxeHorizontal h = getH();
    // si pas de courbe, pas d'axe ou ligne invalide
    if (c_ == null || h == null || (_rowIndex >= c_.getModel().getNbValues())) {
      return CtuluLibString.EMPTY_STRING;
    }
    if (_columnIndex == yColIndex) {
      return getY(c_.getModel().getY(_rowIndex));
    }
    if (_columnIndex == xColIndex) {
      return getX(c_.getModel().getX(_rowIndex));
    }
    return CtuluLibString.EMPTY_STRING;
  }
}