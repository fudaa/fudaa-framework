/*
GPL 2
 */
package org.fudaa.ebli.courbe;

/**
 * Une factory pour customiser les exports de données d'un graphe.
 *
 * @author Frederic Deniger
 */
public interface EGExportDataBuilder {

  /**
   *
   * @param courbesInitiales les courbes initiales sans prétraitement. Utiliser éventuellement
   * org.fudaa.ebli.courbe.EGExportData.prepareCourbesForExport(EGCourbe[]) pour filtrer les courbes ( visible et ajouter les courbes liées)
   * @param _g
   * @param _isSameH
   * @param showLabel
   * @return
   */
  EGExportData createExportData(final EGCourbe[] courbesInitiales, final EGGraphe _g, final boolean _isSameH, boolean showLabel);

}
