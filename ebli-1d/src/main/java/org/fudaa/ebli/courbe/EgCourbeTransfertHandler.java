package org.fudaa.ebli.courbe;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.InputEvent;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

/**
 *
 * @author Frederic Deniger
 */
public class EgCourbeTransfertHandler extends TransferHandler {

  @Override
  protected void exportDone(final JComponent _source, final Transferable _data, final int _action) {
    if (_action == MOVE) {
      EGTableGraphePanel.EvolTable table = (EGTableGraphePanel.EvolTable) _source;
      table.delete();
    } else {
      super.exportDone(_source, _data, _action);
    }
  }

  @Override
  protected Transferable createTransferable(JComponent c) {
    EGTableGraphePanel.EvolTable table = (EGTableGraphePanel.EvolTable) c;
    return table.createTransferable();
  }

  @Override
  public boolean canImport(final JComponent _comp, final DataFlavor[] _transferFlavors) {
    return true;
  }

  @Override
  public void exportAsDrag(final JComponent _comp, final InputEvent _e, final int _action) {
    super.exportAsDrag(_comp, _e, _action);
  }

  @Override
  public void exportToClipboard(final JComponent _comp, final Clipboard _clip, final int _action) {
    super.exportToClipboard(_comp, _clip, _action);
  }

  @Override
  public int getSourceActions(final JComponent _c) {
    EGTableGraphePanel.EvolTable table = (EGTableGraphePanel.EvolTable) _c;
    if (table.getEGTableModel().isModelXModifiable()) {
      return COPY_OR_MOVE;
    }
    return COPY;
  }
}
