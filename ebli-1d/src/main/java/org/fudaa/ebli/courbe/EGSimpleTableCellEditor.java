/*
 * @creation 29 nov. 06
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class EGSimpleTableCellEditor extends AbstractCellEditor implements TableCellEditor {

  final EGTreeCellRenderer renderer_ = new EGTreeCellRenderer(true);
  int lastCol_;
  final EGListSimple list_;

  public EGSimpleTableCellEditor(final EGListSimple _list) {
    list_ = _list;
    renderer_.cbVisible_.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e) {
        stopCellEditing();
      }
    });
    renderer_.r1_.addKeyListener(new KeyListener() {

      @Override
      public void keyPressed(final KeyEvent _e) {}

      @Override
      public void keyReleased(final KeyEvent _e) {
        if (_e.getKeyCode() == KeyEvent.VK_ENTER) {
          stopCellEditing();
          _e.consume();
        }
      }

      @Override
      public void keyTyped(final KeyEvent _e) {}
    });
  }

  @Override
  public boolean isCellEditable(final EventObject _anEvent) {
    if (_anEvent instanceof MouseEvent) {
      final MouseEvent e = (MouseEvent) _anEvent;
      if (e.isConsumed()) {
        return false;
      }
      if (e.getButton() != MouseEvent.BUTTON1) {
        return false;
      }
      final int i = list_.convertColumnIndexToModel(list_.columnAtPoint(e.getPoint()));
      if (i == 2) {
        return e.getClickCount() >= 2;
      }

    }
    return true;
  }

  @Override
  public boolean shouldSelectCell(final EventObject _anEvent) {
    if (_anEvent instanceof MouseEvent) {
      final MouseEvent e = (MouseEvent) _anEvent;
      if (e.getButton() != MouseEvent.BUTTON1) {
        return true;
      }
      final int i = list_.convertColumnIndexToModel(list_.columnAtPoint(e.getPoint()));
      // seul la checkbox n'a pas besoin d'etre selectionnee.
      return i != 1;
    }
    return true;
  }

  @Override
  public Component getTableCellEditorComponent(final JTable _table, final Object _value, final boolean _isSelected,
      final int _row, final int _column) {
    final Component c = renderer_.getTableCellRendererComponent(_table, _value, _isSelected, true, _row, _column);
    lastCol_ = _column;
    renderer_.r1_.setEditable(lastCol_ == 2);
    return c;
  }

  @Override
  public Object getCellEditorValue() {
    if (lastCol_ == 1) {
      return Boolean.valueOf(renderer_.cbVisible_.isSelected());
    }
    return renderer_.r1_.getText();
  }
}