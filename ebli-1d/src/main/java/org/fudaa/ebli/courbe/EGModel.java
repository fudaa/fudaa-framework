/*
 *  @creation     21 juin 2004
 *  @modification $Date: 2007-05-22 14:19:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.collection.CourbeInterface;
import org.fudaa.ebli.palette.BPaletteInfo;

import java.awt.*;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: EGModel.java,v 1.12 2007-05-22 14:19:04 deniger Exp $
 */
public interface EGModel extends CourbeInterface {
  /**
   * @return true si ce modele peut-etre enleve
   */
  boolean isRemovable();

  /**
   * @return true si peut etre dupliqu�. En general, renvoie true.
   */
  boolean isDuplicatable();

  /**
   * @return le nombre de point du modele
   */
  @Override
  int getNbValues();

  /**
   * @param _i l'indice du segment compris entre 0 et getNbPoint()-2
   * @return true si le segment doit etre dessin�
   */
  boolean isSegmentDrawn(int _i);

  /**
   * @param _i l'indice du point compris entre 0 et getNbPoint()-1
   * @return true si le point doit etre dessine
   */
  boolean isPointDrawn(int _i);

  /**
   * @param _i l'indice du point compris entre 0 et getNbPoint()-1
   * @return le text � afficher concernant le point correspondant � _i. Null si
   *     il n'y en a pas.
   */
  String getPointLabel(int _i);

  /**
   * @param _idx l'indice voulu
   * @return la valeur de x en ce point
   */
  @Override
  double getX(int _idx);

  /**
   * @param _idx l'indice voulu
   * @return la valeur de y en ce point
   */
  @Override
  double getY(int _idx);

  /**
   * @return le x min
   */
  double getXMin();

  /**
   * @return le x max
   */
  double getXMax();

  /**
   * @return le y min
   */
  double getYMin();

  /**
   * @return le y max
   */
  double getYMax();

  /**
   * @return true si ce modele est modifiable
   */
  boolean isModifiable();

  /**
   * Utilise que si le modele est modifiable. Si x n'est pas modifiable aucune valeur ne pourra etre enleve ou ajoute
   *
   * @return true si les valeurs en x sont modifiables
   */
  boolean isXModifiable();

  /**
   * Si x n'est pas modifiable seule la valeur en _y sera prise en compte.
   *
   * @param _i l'indice du point a modifie
   * @param _x nouvelle valeur pour x
   * @param _y nouvelle valeur pour y
   * @param _cmd si non null la commande issue de l'operation peu-etre ajoutee a ce container
   * @return true si changement
   */
  boolean setValue(int _i, double _x, double _y, CtuluCommandContainer _cmd);

  /**
   * Ajoute une nouvelle valeur. Si la valeur en x est deja presente: ne fait rien. Si x n'est pas modifiable ne fait
   * rien non plus.
   *
   * @param _x nouvelle valeur de x.
   * @param _y nouvelle valeur de y.
   * @param _cmd si non null la commande issue de l'operation peu-etre ajoutee a ce container
   * @return true si changement
   */
  boolean addValue(double _x, double _y, CtuluCommandContainer _cmd);

  /**
   * Ajoute une nouvelle valeur. Si la valeur en x est deja presente: ne fait rien. Si x n'est pas modifiable ne fait
   * rien non plus.
   *
   * @param _x nouvelle valeur de x.
   * @param _y nouvelle valeur de y.
   * @param _cmd si non null la commande issue de l'operation peu-etre ajoutee a ce container
   * @return true si changement
   */
  boolean addValue(double[] _x, double[] _y, CtuluCommandContainer _cmd);

  /**
   * Ne fait si x n'est pas modifiable.
   *
   * @param _i l'indice a enlever
   * @param _cmd si non null la commande issue de l'operation peu-etre ajoutee a ce container
   * @return true si changement
   */
  boolean removeValue(int _i, CtuluCommandContainer _cmd);

  /**
   * Ne fait si x n'est pas modifiable.
   *
   * @param _i les indices a enlever
   * @param _cmd si non null la commande issue de l'operation peu-etre ajoutee a ce container
   * @return true si changement
   */
  boolean removeValue(int[] _i, CtuluCommandContainer _cmd);

  /**
   * @param _selectIdx l'indice du point a bouger
   * @param _deltaX le delta X
   * @param _deltaY le delta Y
   * @param _cmd si non null la commande issue de l'operation peu-etre ajoutee a ce container
   * @return true si modif
   */
  boolean deplace(int[] _selectIdx, double _deltaX, double _deltaY, CtuluCommandContainer _cmd);

  /**
   * @param _idx les indices a modifier
   * @param _x les nouveaux x. peuvent etre null
   * @param _y les nouveaux y
   * @param _cmd le container de commandes.
   * @return true si modifs.
   */
  boolean setValues(int[] _idx, double[] _x, double[] _y, CtuluCommandContainer _cmd);

  /**
   * @return le nom de la courbe
   */
  String getTitle();

  /**
   * @return true si le nom peut etre modifie
   */
  boolean isTitleModifiable();

  /**
   * @param _newName le nouveau nom
   * @return true si modification
   */
  boolean setTitle(String _newName);

  /**
   * Permet de completer les informations globales donn�es par la courbe.
   *
   * @param _table la table recoltant les donn�es
   * @param _selectedPt la s�lection
   */
  void fillWithInfo(final BPaletteInfo.InfoData _table, final CtuluListSelectionInterface _selectedPt);

  /**
   * methode qui permet de dupliquer le model
   *
   * @return un modele duplique
   */
  EGModel duplicate();

  /**
   * Methode qui une fois appel�e se charge de creer un module ou interface qui affiche l'origine de la cr�ation de la courbe.
   *
   * @param infos
   * @param impl l'implementation dans laquelle afficher les informations
   */
  void viewGenerationSource(Map infos, CtuluUI impl);

  boolean isGenerationSourceVisible();

  /**
   * Methode qui une fois appel�e se charge de creer un module ou interface qui permet de rejouer les donn�es de la courbe.
   *
   * @param infos
   * @param impl l'implementation dans laquelle afficher les informations
   */
  void replayData(EGGrapheModel model, Map infos, CtuluUI impl);

  boolean isReplayable();

  /**
   * retourner les infos persistantes de la courbe du mod�le.
   */
  Object savePersistSpecificDatas();

  /**
   * recuperer les infos persistantes de la courbe qui vont permettre de recr�er le mod�le.
   *
   * @param data
   * @param infos
   */
  void restoreFromSpecificDatas(Object data, Map infos);

  /**
   * @param idx
   * @return true if a specific icon must be used for this point
   */
  boolean useSpecificIcon(int idx);

  /**
   * @param idx l'indice du noeud/Segment
   * @return si non null, la couleur sera utilis� pour dessin� le point/segment i.
   */
  Color getSpecificColor(int idx);

  /**
   * Get init rows.
   */
  int[] getInitRows();
}
