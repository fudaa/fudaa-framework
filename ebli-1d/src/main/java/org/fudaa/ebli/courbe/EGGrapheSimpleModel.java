/*
 * @creation 2 sept. 2004
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuComboBox;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: EGGrapheSimpleModel.java,v 1.19 2007-05-04 13:49:42 deniger Exp $
 */
public class EGGrapheSimpleModel extends AbstractListModel implements EGGrapheModel {
  @Override
  public boolean isDisplayed() {
    return true;
  }

  public void removeCurves(final TIntObjectHashMap curvesRemoveOldIndice, final CtuluCommandManager _mng) {
    if (!curvesRemoveOldIndice.isEmpty()) {
      element_.removeAll(Arrays.asList(curvesRemoveOldIndice.getValues()));
      final int[] idx = curvesRemoveOldIndice.keys();
      Arrays.sort(idx);
      final EGCourbeSimple[] courbes = new EGCourbeSimple[idx.length];
      int minIdxRemove = idx[0];
      courbes[0] = (EGCourbeSimple) curvesRemoveOldIndice.get(minIdxRemove);
      int maxIdxRemove = minIdxRemove;
      for (int i = idx.length - 1; i > 0; i--) {
        final int k = idx[i];
        courbes[i] = (EGCourbeSimple) curvesRemoveOldIndice.get(k);
        if (k < minIdxRemove) {
          minIdxRemove = k;
        }
        if (k > maxIdxRemove) {
          maxIdxRemove = k;
        }
      }
      if (_mng != null) {
        _mng.addCmd(new CourbesRemovedCommand(idx, courbes));
      }
      fireIntervalRemoved(this, minIdxRemove, maxIdxRemove);
    }
  }

  public ComboBoxModel createComboBoxModel() {
    return new CbModel(null);
  }

  public class CbModel extends AbstractListModel implements ComboBoxModel {
    Object selected_;
    final EGAxeVertical vert_;

    public CbModel(final EGAxeVertical _vert) {
      super();
      vert_ = _vert;
    }

    @Override
    public Object getElementAt(final int _index) {
      return getCourbe(_index);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return getNbEGObject();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_ && (vert_ != null && vert_ != _anItem)) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  private class CourbeAddCommand implements CtuluCommand {
    final EGCourbeSimple courbeAdd_;
    final int idx_;

    public CourbeAddCommand(final EGCourbeSimple _s, final int _idx) {
      courbeAdd_ = _s;
      idx_ = _idx;
    }

    @Override
    public void redo() {
      if (idx_ >= 0) {
        internalAddElement(idx_, courbeAdd_);
        selection_.setSelectionInterval(idx_, idx_);
      } else {
        final int i = element_.size();
        internalAddElement(courbeAdd_);
        selection_.setSelectionInterval(i, i);
      }
    }

    @Override
    public void undo() {
      internalRemoveElement(courbeAdd_);
    }
  }

  private class CourbesAddCommand implements CtuluCommand {
    final EGCourbeSimple[] courbeAdd_;

    public CourbesAddCommand(final EGCourbeSimple[] _s) {
      courbeAdd_ = _s;
    }

    @Override
    public void redo() {
      internalAddElement(courbeAdd_);
    }

    @Override
    public void undo() {
      internalRemoveElement(courbeAdd_);
    }
  }

  private class CourbesRemovedCommand implements CtuluCommand {
    final EGCourbeSimple[] courbes_;
    final int[] idx_;

    /**
     * @param _idx les indices dans l'ordre croissant
     * @param _cs les courbes correspondantes
     */
    public CourbesRemovedCommand(final int[] _idx, final EGCourbeSimple[] _cs) {
      idx_ = _idx;
      courbes_ = _cs;
    }

    @Override
    public void redo() {
      selection_.clearSelection();
      for (int i = idx_.length - 1; i >= 0; i--) {
        element_.remove(courbes_[i]);
      }
      fireIntervalRemoved(this, idx_[0], idx_[idx_.length - 1]);
    }

    @Override
    public void undo() {
      final int n = idx_.length;
      selection_.clearSelection();
      for (int i = 0; i < n; i++) {
        if (idx_[i] == element_.size()) {
          element_.add(courbes_[i]);
        } else {
          element_.add(idx_[i], courbes_[i]);
        }
      }
      fireIntervalAdded(this, idx_[0], idx_[n - 1]);
      for (int i = 0; i < n; i++) {
        selection_.addSelectionInterval(idx_[i], idx_[i]);
      }
    }
  }

  class SimpleTableModel extends AbstractTableModel {
    @Override
    public Class getColumnClass(final int _columnIndex) {
      return EGObject.class;
    }

    @Override
    public int getColumnCount() {
      return 3;
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 2) {
        return EbliLib.getS("Nom");
      } else if (_column == 1) {
        return "v";
      }
      return CtuluLibString.ESPACE;
    }

    @Override
    public int getRowCount() {
      return getSize();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      return getElementAt(_rowIndex);
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      // la colonne 1 represente la case a cocher visible
      // la colonne 2 : le nom de la courbe: modifiable si le model l'autorise
      return _columnIndex == 1 || (_columnIndex == 2 && getCourbe(_rowIndex).isTitleModifiable());
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 1 && _value instanceof Boolean) {
        getCourbe(_rowIndex).setVisible(((Boolean) _value).booleanValue());
        fireCourbeAspectChanged(getCourbe(_rowIndex), true);
        fireTableRowsUpdated(_rowIndex, _rowIndex);
      } else if (_columnIndex == 2 && getCourbe(_rowIndex).setTitle(_value.toString())) {
        fireCourbeContentChanged(getCourbe(_rowIndex), false);
        fireTableRowsUpdated(_rowIndex, _rowIndex);
      }
    }
  }

  public static EGGrapheSimpleModel createSimpleModel(final EGCourbeSimple[] _cs) {
    return new EGGrapheSimpleModel(_cs) {
      @Override
      public boolean canAddCourbe() {
        return false;
      }

      @Override
      public boolean isContentModifiable() {
        return false;
      }

      @Override
      public boolean isStructureModifiable() {
        return false;
      }
    };
  }

  EGAxeHorizontal axeX_;
  protected List element_;
  boolean isUpdating;
  protected DefaultListSelectionModel selection_;
  SimpleTableModel tableModel_;

  public EGGrapheSimpleModel() {
    this(null);
  }

  public EGGrapheSimpleModel(final EGCourbeSimple[] _s) {
    if (_s != null) {
      element_ = new ArrayList(Arrays.asList(_s));
    } else {
      element_ = new ArrayList();
    }
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      getCourbe(i).setParent(this);
    }
    selection_ = new DefaultListSelectionModel();
  }

  /**
   * @param _obj la courbe a ajouter
   * @param _mng le manager de undo/redo
   */
  public void addCourbe(final EGCourbeSimple _obj, final CtuluCommandManager _mng) {
    addCourbe(_obj, _mng, -1);
  }

  /**
   * @param _obj la courbe a ajouter
   * @param _mng le manager de undo/redo
   * @param select if true, the new curve will be selected.
   */
  public void addCourbe(final EGCourbeSimple _obj, final CtuluCommandManager _mng, boolean select) {
    addCourbe(_obj, _mng, -1, select);
  }

  /**
   * @param _obj la courbe a ajouter
   * @param _mng le manager de undo/redo
   * @param _idx l'indice d'ajout
   */
  public void addCourbe(final EGCourbeSimple _obj, final CtuluCommandManager _mng, final int _idx) {
    addCourbe(_obj, _mng, _idx, true);
  }

  public void addCourbe(final EGCourbeSimple _obj, final CtuluCommandManager _mng, final int _idx, boolean select) {
    if (!isStructureModifiable()) {
      return;
    }
    final int idx = internalAddElement(_idx, _obj);
    if (select) {
      selection_.setSelectionInterval(idx, idx);
    }
    createCmdForCourbeAdd(_obj, _mng, _idx);
  }

  public void addCourbes(final EGCourbeSimple[] _obj, final CtuluCommandManager _mng) {
    if (!isStructureModifiable()) {
      return;
    }
    final int[] idx = internalAddElement(_obj);
    if (idx != null) {
      selection_.clearSelection();
      for (int i = idx.length - 1; i >= 0; i--) {
        selection_.addSelectionInterval(idx[i], idx[i]);
      }
    }
    createCmdForCourbeAdd(_obj, _mng);
  }

  @Override
  public void addModelListener(final EGGrapheModelListener _l) {
    listenerList.add(EGGrapheModelListener.class, _l);
  }

  @Override
  public void addNewCourbe(final CtuluCommandManager _cmd, final Component _p, final EGGraphe _graphe) {
  }

  @Override
  public void addSelectionListener(final EGSelectionListener _l) {
    selection_.addListSelectionListener(_l);
  }

  @Override
  public boolean canAddCourbe() {
    return true;
  }

  /**
   * Methode appelee avant l'ajout effectif d'une courbe.
   *
   * @param _s la courbe a enlever
   * @return true si la courbe peut etre ajoutee
   */
  protected boolean canAddCourbe(final EGCourbeSimple _s) {
    return true;
  }

  @Override
  public boolean canDeleteCourbes() {
    return isStructureModifiable();
  }

  @Override
  public boolean canDescendre(final EGObject _o) {
    if (element_.size() <= 1) {
      return false;
    }
    final int i = element_.indexOf(_o);
    return i >= 0 && i < element_.size() - 1;
  }

  @Override
  public boolean canMonter(final EGObject _o) {
    if (element_.size() <= 1) {
      return false;
    }
    return element_.indexOf(_o) > 0;
  }

  /**
   * Methode appelee avant la suppression d'une courbe.
   *
   * @param _s la courbe a enlever
   * @return true si la courbe peut etre ajoutee
   */
  public boolean canRemoveCourbe(final EGCourbeSimple _s) {
    return _s.getModel().isRemovable();
  }

  @Override
  public boolean contains(final EGCourbe _c) {
    return element_ == null ? false : element_.contains(_c);
  }

  @Override
  public JComboBox createCbForSelectCourbe() {
    return createCbForSelectCourbe(null);
  }

  @Override
  public JComboBox createCbForSelectCourbe(final EGAxeVertical _vertical) {
    final BuComboBox cb = new BuComboBox(new CbModel(null));
    cb.setRenderer(_vertical == null ? new EGTreeCellRenderer(true) : new EGTreeCellRenderer.AxeChooserRenderer(
        _vertical));
    return cb;
  }

  protected void createCmdForCourbeAdd(final EGCourbeSimple _courbeAdd, final CtuluCommandManager _cmd, final int _idx) {
    if (_cmd != null) {
      _cmd.addCmd(new CourbeAddCommand(_courbeAdd, _idx));
    }
  }

  protected void createCmdForCourbeAdd(final EGCourbeSimple[] _courbeAdd, final CtuluCommandManager _cmd) {
    if (_cmd != null) {
      _cmd.addCmd(new CourbesAddCommand(_courbeAdd));
    }
  }

  public TableModel createTableTreeModel() {
    if (tableModel_ == null) {
      tableModel_ = new SimpleTableModel();
    }
    return tableModel_;
  }

  @Override
  public void decoreAddButton(final EbliActionInterface _b) {
  }

  @Override
  public void descendre(final EGObject _o) {
    final int i = element_.indexOf(_o);
    final int lastIdx = element_.size() - 1;
    if (i >= 0 && i < lastIdx) {
      element_.remove(i);
      if (i == lastIdx - 1) {
        element_.add(_o);
      } else {
        element_.add(i + 1, _o);
      }
      fireContentsChanged(this, i, i + 1);
      selection_.setSelectionInterval(i + 1, i + 1);
    }
  }

  protected EGCourbeSimple doDuplicateCourbe(final EGCourbe _c) {
    return null;
  }

  //TODO a reprendre
  @Override
  public EGGrapheModel duplicate(EGGrapheDuplicator _duplicator) {

    //--duplication de la liste des courbes --//
    ArrayList<EGCourbeSimple> listeElements = new ArrayList<>(element_.size());

    for (Iterator<EGCourbeSimple> it = this.element_.iterator(); it.hasNext(); ) {
      EGCourbeSimple courbe = it.next();
      listeElements.add((EGCourbeSimple) courbe.duplicate(this, _duplicator));
    }

    EGGrapheSimpleModel duplic = new EGGrapheSimpleModel((EGCourbeSimple[]) listeElements.toArray());
    //a corriger
    duplic.axeX_ = this.axeX_.duplicate();

    // duplication du listselectionModel
//    try {
//      duplic.selection_ = (DefaultListSelectionModel) this.selection_.clone();
//    } catch (CloneNotSupportedException e) {
    //TODO cela suffit bien
    // -- si ce n'est pas duplicable, en cree un par defaut --//
    duplic.selection_ = new DefaultListSelectionModel();

//    }

    // -- pas besoin de dupliquer le tableModel, il ne fait que gerer la liste
    // elements --//
    //FRED ok
//    duplic.tableModel_ = new SimpleTableModel();

    return duplic;
  }

  @Override
  public void duplicateCourbe(final CtuluCommandManager _mng, final EGCourbe _c) {
    if (_c == null || (element_ == null) || element_.size() == 0) {
      return;
    }
    final int idx = element_.indexOf(_c);
    if (idx < 0) {
      return;
    }
    final EGCourbeSimple c = doDuplicateCourbe(_c);
    if (c != null) {
      addCourbe(c, _mng, idx + 1);
    }
  }

  @Override
  public void duplicateCourbeForSrc(CtuluCommandManager _mng, EGCourbe _c) {

    JOptionPane.showMessageDialog(null, "ne gere pas le multi source ");
    duplicateCourbe(_mng, _c);
  }

  @Override
  public void enDernier(final EGObject _o) {
    final int i = element_.indexOf(_o);
    final int lastIdx = element_.size() - 1;
    if (i >= 0 && i < lastIdx) {
      element_.remove(i);
      element_.add(_o);
      fireContentsChanged(this, i, lastIdx);
      selection_.setSelectionInterval(lastIdx, lastIdx);
    }
  }

  @Override
  public void enPremier(final EGObject _o) {
    final int i = element_.indexOf(_o);
    if (i > 0) {
      element_.remove(i);
      element_.add(0, _o);
      fireContentsChanged(this, 0, i);
      selection_.setSelectionInterval(0, 0);
    }
  }

  @Override
  public final void fireAxeAspectChanged(final EGAxe _a) {
    EGGrapheTreeModel.fireAxeAspectChanged(_a, listenerList);
  }

  @Override
  public final void fireAxeContentChanged(final EGAxe _a) {
    EGGrapheTreeModel.fireAxeContentChanged(_a, listenerList);
  }

  @Override
  protected final void fireContentsChanged(final Object _source, final int _index0, final int _index1) {
    super.fireContentsChanged(_source, _index0, _index1);
    if (tableModel_ != null) {
      tableModel_.fireTableRowsUpdated(_index0, _index1);
    }
  }

  @Override
  public final void fireCourbeAspectChanged(final EGObject _o, final boolean _visibility) {
    internFireCourbeChanged(_o);
    EGGrapheTreeModel.fireCourbeAspectChanged(_o, _visibility, listenerList);
  }

  public final void fireCourbeContentChanged(final EGObject _o) {
    fireCourbeContentChanged(_o, false);
  }

  @Override
  public final void fireCourbeContentChanged(final EGObject _o, final boolean _mustRestore) {
    internFireCourbeChanged(_o);

    EGGrapheTreeModel.fireCourbeContentChanged(_o, listenerList, _mustRestore);
  }

  @Override
  protected final void fireIntervalAdded(final Object _source, final int _index0, final int _index1) {
    super.fireIntervalAdded(_source, _index0, _index1);
    if (tableModel_ != null) {
      tableModel_.fireTableRowsInserted(_index0, _index1);
    }
    EGGrapheTreeModel.fireStructureChanged(listenerList);
    selection_.setSelectionInterval(_index0, _index1);
  }

  @Override
  protected final void fireIntervalRemoved(final Object _source, final int _index0, final int _index1) {
    super.fireIntervalRemoved(_source, _index0, _index1);
    if (tableModel_ != null) {
      tableModel_.fireTableRowsDeleted(_index0, _index1);
    }
    selection_.clearSelection();
    EGGrapheTreeModel.fireStructureChanged(listenerList);
  }

  public void fireObjectChanged(final EGObject _o) {
    if (element_ != null) {
      if (_o == null) {
        fireContentsChanged(this, 0, element_.size());
      } else {
        final int i = element_.indexOf(_o);
        if (i >= 0) {
          fireContentsChanged(this, i, i);
        }
      }
    }
  }

  @Override
  public final void fireStructureChanged() {
    fireIntervalAdded(this, 0, getNbEGObject());
    if (tableModel_ != null) {
      tableModel_.fireTableStructureChanged();
    }
  }

  @Override
  public EGAxeHorizontal getAxeX() {
    return axeX_;
  }

  public EGCourbeSimple getCourbe(final int _i) {
    if (element_ == null) {
      return null;
    }
    return (EGCourbeSimple) element_.get(_i);
  }

  @Override
  public EGCourbeSimple[] getCourbes() {
    final EGCourbeSimple[] r = new EGCourbeSimple[element_ == null ? 0 : element_.size()];
    if (element_ != null) {
      element_.toArray(r);
    }
    return r;
  }

  @Override
  public synchronized EGObject getEGObject(final int _i) {
    return element_ == null ? null : (EGObject) element_.get(_i);
  }

  @Override
  public Object getElementAt(final int _index) {
    return getEGObject(_index);
  }

  public ListSelectionModel getListSelection() {
    return selection_;
  }

  @Override
  public EGGrapheModel getMainModel() {
    return this;
  }

  @Override
  public int getNbEGObject() {
    return element_ == null ? 0 : element_.size();
  }

  @Override
  public int getNbSelectedObjects() {
    if (selection_.isSelectionEmpty()) {
      return 0;
    }
    final int max = selection_.getMaxSelectionIndex();
    int r = 0;
    for (int i = selection_.getMinSelectionIndex(); i <= max; i++) {
      if (selection_.isSelectedIndex(i)) {
        r++;
      }
    }
    return r;
  }

  @Override
  public EGCourbe getSelectedComponent() {
    if (selection_ == null || selection_.isSelectionEmpty()) {
      return null;
    }
    if (selection_.getMaxSelectionIndex() == selection_.getMinSelectionIndex()) {
      final int i = selection_.getMaxSelectionIndex();
      if (i < getNbEGObject()) {
        return getCourbe(i);
      }
    }
    return null;
  }

  @Override
  public EGObject getSelectedObject() {
    return getSelectedComponent();
  }

  @Override
  public EGObject[] getSelectedObjects() {
    if (selection_.isSelectionEmpty()) {
      return null;
    }
    final List r = new ArrayList();
    final int max = Math.min(selection_.getMaxSelectionIndex(), getSize() - 1);
    for (int i = selection_.getMinSelectionIndex(); i <= max; i++) {
      if (selection_.isSelectedIndex(i)) {
        r.add(getElementAt(i));
      }
    }
    if (r.isEmpty()) {
      return null;
    }
    final EGObject[] rf = new EGObject[r.size()];
    r.toArray(rf);
    return rf;
  }

  @Override
  public int getSize() {
    return getNbEGObject();
  }

  @Override
  public Object getSpecificPersitDatas(Map parameters) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<EbliActionInterface> getSpecificsActionsCurvesOnly(EGGraphe _target, CtuluUI ui) {
    // TODO Auto-generated method stub
    return null;
  }

  protected int internalAddElement(final EGCourbeSimple _obj) {
    if (!canAddCourbe(_obj)) {
      return -1;
    }
    if (element_ == null) {
      element_ = new ArrayList();
    }
    final int i = element_.size();
    element_.add(_obj);
    _obj.setParent(this);
    fireIntervalAdded(this, i, i);
    return i;
  }

  protected int[] internalAddElement(final EGCourbeSimple[] _obj) {
    if (_obj == null) {
      return null;
    }
    final TIntArrayList list = new TIntArrayList(_obj.length);
    for (int i = 0; i < _obj.length; i++) {
      final int idx = internalAddElement(_obj[i]);
      if (idx >= 0) {
        list.add(idx);
      }
    }
    return list.toNativeArray();
  }

  protected int internalAddElement(final int _index, final EGCourbeSimple _g) {
    if (_index < 0 || element_ == null || _index == element_.size()) {
      internalAddElement(_g);
      return element_.size() - 1;
    }
    /*    if (element_ == null) {
     element_ = new ArrayList();
     }*/
    element_.add(_index, _g);
    _g.setParent(this);
    fireIntervalAdded(this, _index, _index);
    return _index;
  }

  protected void internalRemoveElement(final EGCourbeSimple _o) {
    if (!canRemoveCourbe(_o)) {
      return;
    }
    if (element_ == null) {
      return;
    }
    final int i = element_.indexOf(_o);
    if (i >= 0) {
      element_.remove(i);
      fireIntervalRemoved(this, i, i);
    }
  }

  protected void internalRemoveElement(final EGCourbeSimple[] _o) {
    if (_o != null) {
      for (int i = _o.length - 1; i >= 0; i--) {
        internalRemoveElement(_o[i]);
      }
    }
  }

  private void internFireCourbeChanged(final EGObject _o) {
    if (_o == null) {

      fireContentsChanged(this, 0, getNbEGObject());
    } else {
      if (element_ == null) {
        return;
      }
      final int i = element_.indexOf(_o);
      fireContentsChanged(this, i, i);
    }
  }

  protected void internFireStructureChanged() {
    if (element_ != null) {
      super.fireIntervalAdded(this, 0, element_.size());
    }
  }

  @Override
  public boolean isContentModifiable() {
    return true;
  }

  @Override
  public boolean isSomethingToDisplay() {
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      if (getCourbe(i).isVisible_) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isStructureModifiable() {
    return true;
  }

  /**
   * @return the isUpdating
   */
  @Override
  public boolean isUpdating() {
    return isUpdating;
  }

  @Override
  public void monter(final EGObject _o) {
    final int i = element_.indexOf(_o);
    if (i > 0) {
      element_.remove(i);
      element_.add(i - 1, _o);
      fireContentsChanged(this, i - 1, i);
      selection_.setSelectionInterval(i - 1, i - 1);
    }
  }

  @Override
  public void removeModelListener(final EGGrapheModelListener _l) {
    listenerList.remove(EGGrapheModelListener.class, _l);
  }

  public void removeCurves(int[] idx, final CtuluCommandManager _mng) {
    if (idx == null) {
      return;
    }
    final TIntObjectHashMap curvesRemoveOldIndice = new TIntObjectHashMap();
    for (int i = 0; i < idx.length; i++) {
      int index = idx[i];
      if (getCourbe(index).getModel().isRemovable()) {
        curvesRemoveOldIndice.put(index, element_.get(index));
      }
    }
    removeCurves(curvesRemoveOldIndice, _mng);
  }

  @Override
  public void removeSelectedCurves(final CtuluCommandManager _mng) {
    if (selection_.isSelectionEmpty()) {
      return;
    }
    final int max = selection_.getMaxSelectionIndex();
    final TIntObjectHashMap curvesRemoveOldIndice = new TIntObjectHashMap();
    for (int i = selection_.getMinSelectionIndex(); i <= max; i++) {
      if ((selection_.isSelectedIndex(i)) && (getCourbe(i).getModel().isRemovable())) {
        curvesRemoveOldIndice.put(i, getCourbe(i));
      }
    }
    removeCurves(curvesRemoveOldIndice, _mng);
  }

  /**
   * Supprime les courbes du graphe, sans considération de leur caractère "removable"
   */
  public void removeAllCurves(final CtuluCommandManager _mng) {
    int[] idx = new int[getCourbes().length];
    if (idx.length == 0) {
      return;
    }
    CtuluLibArray.fillIncremental(idx, 0);

    if (_mng != null) {
      _mng.addCmd(new CourbesRemovedCommand(idx, getCourbes()));
    }
    fireIntervalRemoved(this, 0, idx.length - 1);
    internalRemoveElement(getCourbes());
  }

  @Override
  public void removeSelectionListener(final EGSelectionListener _l) {
    selection_.removeListSelectionListener(_l);
  }

  /**
   * @param _i l'index a selectionner
   */
  public void select(final int _i) {
    selection_.setSelectionInterval(_i, _i);
  }

  @Override
  public void setAxeX(final EGAxeHorizontal _h) {
    axeX_ = _h;
  }

  @Override
  public void setSelectedComponent(final EGCourbe _c) {
    final int idx = element_.indexOf(_c);
    if (idx >= 0) {
      select(idx);
    }
  }

  @Override
  public void setSpecificPersitDatas(Object sepcPersitData, Map parameters) {
    // TODO Auto-generated method stub
  }

  /**
   * @param isUpdating the isUpdating to set
   */
  public void setUpdating(boolean isUpdating) {
    this.isUpdating = isUpdating;
  }
}
