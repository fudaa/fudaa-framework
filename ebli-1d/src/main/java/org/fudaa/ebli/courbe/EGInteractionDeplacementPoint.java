/*
 * @creation 23 juin 2004
 * @modification $Date: 2007-05-22 14:19:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionDeplacementPoint.java,v 1.17 2007-05-22 14:19:04 deniger Exp $
 */
public class EGInteractionDeplacementPoint extends EGInteractiveComponent implements MouseListener, MouseMotionListener {

  private final EGGraphe target_;
  private int[] xEcranInit_;
  private int[] yEcranInit_;
  private int[] xEcranFin_;
  private int[] yEcranFin_;
  private int[] idxPt_;
  private boolean dragEnCours_;
  EGCourbe compModifie_;
  final CtuluListSelectionInterface select_;

  @Override
  public String getDescription() {
    return EbliLib.getS("Déplacement d'un point");
  }

  /**
   * @param _a la cible pour les modifications
   */
  public EGInteractionDeplacementPoint(final EGFillePanel _a) {
    target_ = _a.getGraphe();
    target_.addMouseListener(this);
    target_.addMouseMotionListener(this);
    select_ = _a.getSelection();
  }

  private TraceLigne l_;

  @Override
  protected void paintComponent(final Graphics _g) {
    if (isActive() && dragEnCours_ && (isCompSelectModifiable())) {
      if (l_ == null) {
        l_ = new TraceLigne();
        l_.setCouleur(Color.RED);
        l_.setTypeTrait(TraceLigne.TIRETE);
      }
      for (int i = xEcranInit_.length - 1; i >= 0; i--) {
        l_.dessineRectangle((Graphics2D) _g, xEcranInit_[i] - 2, yEcranInit_[i] - 2, 4, 4);
        l_.dessineFleche((Graphics2D) _g, xEcranInit_[i], yEcranInit_[i], xEcranFin_[i], yEcranFin_[i]);
      }
    }
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {}

  private boolean isCompSelectModifiable() {
    return (compModifie_ != null) && compModifie_.isVisible_
        && (compModifie_.getModel().isModifiable() && idxPt_ != null);
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (_e.isConsumed() || !isActive() || !isCompSelectModifiable()) {
      return;
    }
    final int xEnCours = _e.getX();
    final int yEnCours = _e.getY();
    if (CtuluLibGeometrie.getDistance(xEnCours, yEnCours, xEcranInit_[0], yEcranInit_[0]) > 3) {
      dragEnCours_ = true;
    }
    final int dx = compModifie_.getModel().isXModifiable() ? xEnCours - xEcranInit_[0] : 0;
    final int dy = yEnCours - yEcranInit_[0];
    for (int i = xEcranFin_.length - 1; i >= 0; i--) {
      xEcranFin_[i] = xEcranInit_[i] + dx;
      yEcranFin_[i] = yEcranInit_[i] + dy;
    }
    repaint();
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {

  }

  @Override
  public void mouseExited(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    dragEnCours_ = false;
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {}

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    if (!isActive()) {
      return;
    }
    compModifie_ = target_.getSelectedComponent();
    // on sauvegarde les coordonnees du point clique
    idxPt_ = null;
    if (compModifie_ != null && compModifie_.getModel() != null && compModifie_.getModel().isModifiable()) {
      final int idxSelected = compModifie_.select(_e.getX(), _e.getY(), target_.getTransformer(), EGInteractionSelection
          .getPrecisionPixel());
      if (idxSelected >= 0 && idxSelected < compModifie_.getModel().getNbValues()) {
        if (select_.getNbSelectedIndex() > 1 && select_.isSelected(idxSelected)) {
          idxPt_ = select_.getSelectedIndex();
          final int firstIdx = CtuluLibArray.findInt(idxPt_, idxSelected);
          final int oldValue = idxPt_[0];
          idxPt_[0] = idxSelected;
          idxPt_[firstIdx] = oldValue;

        } else {
          idxPt_ = new int[] { idxSelected };
        }
        xEcranFin_ = new int[idxPt_.length];
        yEcranFin_ = new int[idxPt_.length];
        xEcranInit_ = new int[idxPt_.length];
        yEcranInit_ = new int[idxPt_.length];
        for (int i = xEcranInit_.length - 1; i >= 0; i--) {
          xEcranInit_[i] = target_.getTransformer().getXEcran(compModifie_.getModel().getX(idxPt_[i]));
          yEcranInit_[i] = target_.getTransformer().getYEcran(compModifie_.getModel().getY(idxPt_[i]),
              compModifie_.getAxeY());
        }

      }

    }
    repaint();
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    if (isActive() && dragEnCours_ && isCompSelectModifiable()) {
      final double xmove = target_.getTransformer().getXReel(xEcranFin_[0])
          - target_.getTransformer().getXReel(xEcranInit_[0]);
      final double ymove = target_.getTransformer().getYReel(yEcranFin_[0], compModifie_.getAxeY())
          - target_.getTransformer().getYReel(yEcranInit_[0], compModifie_.getAxeY());
      compModifie_.getModel().deplace(idxPt_, xmove, ymove, target_.getCmd());
    }
    dragEnCours_ = false;
    repaint();
    idxPt_ = null;
  }

  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    if (!_b) {
      compModifie_ = null;
    }
  }

}