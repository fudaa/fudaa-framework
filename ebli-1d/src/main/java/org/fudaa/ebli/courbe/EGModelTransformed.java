package org.fudaa.ebli.courbe;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * Decorator pour le EGModel, permet de g�rer sans trop de d�gat des egmodel aux comportements diff�rents. Utilise le design pattern decorator.
 *
 * @author Adrien Hadoux
 */
public class EGModelTransformed extends EGModelDecorator {
  public EGModelTransformed(final EGModel modeleInitial) {
    super(modeleInitial);
  }

  @Override
  public boolean isRemovable() {
    return false;
  }

  @Override
  public boolean isDuplicatable() {
    return false;
  }

  @Override
  public int getNbValues() {
    return instanceModele_.getNbValues();
  }

  @Override
  public boolean isSegmentDrawn(int _i) {
    return instanceModele_.isSegmentDrawn(_i);
  }

  @Override
  public boolean isPointDrawn(int _i) {
    return instanceModele_.isPointDrawn(_i);
  }

  @Override
  public String getPointLabel(int _i) {
    return instanceModele_.getPointLabel(_i);
  }

  @Override
  public double getX(int _idx) {
    return instanceModele_.getX(_idx);
  }

  @Override
  public double getY(int _idx) {
    return instanceModele_.getY(_idx);
  }

  @Override
  public double getXMin() {
    return instanceModele_.getXMin();
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return instanceModele_.useSpecificIcon(idx);
  }

  @Override
  public Color getSpecificColor(int idx) {
    return instanceModele_.getSpecificColor(idx);
  }

  @Override
  public double getXMax() {
    return instanceModele_.getXMax();
  }

  @Override
  public double getYMin() {
    return instanceModele_.getYMin();
  }

  @Override
  public double getYMax() {
    return instanceModele_.getYMax();
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Override
  public boolean isXModifiable() {
    return false;
  }

  @Override
  public boolean setValue(int _i, double _x, double _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean addValue(double _x, double _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean addValue(double[] _x, double[] _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(int _i, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean removeValue(int[] _i, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean deplace(int[] _selectIdx, double _deltaX, double _deltaY, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public boolean setValues(int[] _idx, double[] _x, double[] _y, CtuluCommandContainer _cmd) {
    return false;
  }

  @Override
  public String getTitle() {
    return instanceModele_.getTitle();
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public boolean setTitle(String _newName) {
    return false;
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
  }

  @Override
  public EGModel duplicate() {
    return null;
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return false;
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
  }

  @Override
  public boolean isReplayable() {
    return false;
  }

  @Override
  public Object savePersistSpecificDatas() {
    return null;
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
  }

  @Override
  public int[] getInitRows() {
    // TODO Auto-generated method stub
    return null;
  }
}
