/*
 * @creation 5 ao�t 2004
 * @modification $Date: 2006-12-05 10:14:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGFilleSimple.java,v 1.16 2006-12-05 10:14:34 deniger Exp $
 */
public class EGFilleSimple extends EGFille implements CtuluFilleWithComponent {

  EGListSimple list_;

  EGSpecificActions listAction_;

  /**
   * @param _g ce panneau doit utilise un graphe simple.
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   */
  public EGFilleSimple(final EGFillePanel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id, null);
    // verif graphe simple.
    if (!(_g.vue_.graphe_.getModel() instanceof EGGrapheSimpleModel)) {
      throw new IllegalArgumentException();
    }
  }

  /**
   * @param _g ce panneau doit utilise un graphe simple.
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   */
  public EGFilleSimple(final EGFillePanel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _t) {
    super(_g, _titre, _appli, _id, _t);
    // verif graphe simple.
    if (!(_g.vue_.graphe_.getModel() instanceof EGGrapheSimpleModel)) {
      throw new IllegalArgumentException();
    }
  }

  /**
   * @param _g le graphe
   * @param _titre le titre
   */
  public EGFilleSimple(final EGGrapheSimpleModel _g, final String _titre) {
    super(new EGGraphe(_g), _titre);
  }

  /**
   * @param _g le graphe
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   */
  public EGFilleSimple(final EGGrapheSimpleModel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id) {
    super(new EGGraphe(_g), _titre, _appli, _id);
  }

  /**
   * @param _g le graphe
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   */
  public EGFilleSimple(final EGGrapheSimpleModel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _tablePanel) {
    super(new EGGraphe(_g), _titre, _appli, _id, _tablePanel);
  }

  protected final EGGrapheSimpleModel getGrapheSimple() {
    return (EGGrapheSimpleModel) getGraphe().getModel();
  }

  TableModel listModel_;

  @Override
  public JComponent createPanelComponent() {
    if (list_ == null) {
      list_ = new EGListSimple();
      list_.setColumnSelectionAllowed(false);
      listModel_ = getGrapheSimple().createTableTreeModel();
      list_.setModel(listModel_);
      list_.setName("LIST_SIMPLE");
      if (listAction_ == null) {
        listAction_ = new EGSpecificActions(getGraphe());
        listAction_.setCmd(getCmdMng());
      }
      list_.setIntercellSpacing(new Dimension(0, 0));
      list_.setShowHorizontalLines(false);
      list_.setRowHeight(35);
      list_.setShowVerticalLines(false);
      list_.setDefaultRenderer(EGObject.class, new EGTreeCellRenderer(true));
      list_.setDefaultEditor(EGObject.class, new EGSimpleTableCellEditor(list_));
    }
    return list_;
  }

  @Override
  public Class getComponentClass() {
    return EGListSimple.class;
  }

  @Override
  public String getComponentTitle() {
    return EbliLib.getS("courbes");
  }

  @Override
  public void majComponent(final Object _o) {
    final EGListSimple l = (EGListSimple) _o;
    l.setSpecificAction(listAction_);
    /*
     * int[] s = null; //recuperation des anciens indices selectionnees if
     * (!getGrapheSimple().getListSelection().isSelectionEmpty()) { ListSelectionModel selection =
     * getGrapheSimple().getListSelection(); int max = selection.getMaxSelectionIndex(); TIntArrayList array = new
     * TIntArrayList(max); for (int i = selection.getMinSelectionIndex(); i <= max; i++) { if
     * (selection.isSelectedIndex(i)) array.add(i); } s = array.toNativeArray(); }
     */
    l.setModel(listModel_);
    l.getColumnModel().getColumn(0).setMaxWidth(24);
    l.getColumnModel().getColumn(1).setMaxWidth(
        ((EGTreeCellRenderer) l.getDefaultRenderer(EGObject.class)).cbVisible_.getPreferredSize().width + 2);
    l.setSelectionModel(getGrapheSimple().getListSelection());
    /*
     * if (s != null){ }
     */
    l.repaint();
  }

  public final void setComponentListAction(final EGSpecificActions _listAction) {
    listAction_ = _listAction;
  }

}