/*
 * @creation 23 juin 2004
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import gnu.trove.TDoubleArrayList;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ebli.trace.TraceBox;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionSuivi.java,v 1.13 2006-09-19 14:55:53 deniger Exp $
 */
public class EGInteractionInfoOnCourbe extends EGInteractiveComponent implements MouseListener,
        MouseMotionListener {
  
  final EGGraphe target_;
  TraceBox tb_;
  String info;
  
  @Override
  public String getDescription() {
    return "";
  }
//  int selectedPtIdx_;

  /**
   * @param _a la cible pour le suivi.
   */
  public EGInteractionInfoOnCourbe(final EGGraphe _a) {
    target_ = _a;
    target_.addMouseListener(this);
    target_.addMouseMotionListener(this);
  }
  final int deltaInPixel = 5;
  
  private String updateWithMouse(final int _x, final int _y) {
    if (!isActive()) {
      return null;
    }
    StringBuilder names = new StringBuilder();
    double xreel = target_.getTransformer().getXReel(_x);
    if (target_.getTransformer().getXAxe().isDiscret()) {
      xreel = (int) xreel;
    }
    
    for (int i = 0; i < target_.getModel().getCourbes().length; i++) {
      final EGCourbe ci = target_.getModel().getCourbes()[i];
      if (ci.getAxeY() == null) {
        continue;
      }
      TDoubleArrayList yPressed = ci.interpolPointsOnEcran(xreel, target_.getTransformer());
      int nb = yPressed.size();
      for (int j = 0; j < nb; j++) {
        if (Math.abs(yPressed.get(j) - _y) <= deltaInPixel) {
          if (names.length() > 0) {
            names.append('\n');
          }
          names.append(ci.getTitle());
          
        }
      }
    }
    return names.toString();
  }
  
  @Override
  protected void paintComponent(final Graphics _g) {
    if (isActive() && StringUtils.isNotBlank(info)) {
      if (tb_ == null) {
        tb_ = new TraceBox();
        tb_.setVPosition(SwingConstants.BOTTOM);
        tb_.setHPosition(SwingConstants.CENTER);
        tb_.setColorFond(UIManager.getColor("ToolTip.background"));
        tb_.setColorText(UIManager.getColor("ToolTip.foreground"));
      }
      Point location = MouseInfo.getPointerInfo().getLocation();
      SwingUtilities.convertPointFromScreen(location, this);
      tb_.paintBox((Graphics2D) _g, location.x, location.y, info);
    }
  }
  
  @Override
  public void mouseDragged(final MouseEvent e) {
    updateWhenMouseMoved(e);
  }
  
  @Override
  public void mouseEntered(final MouseEvent e) {
    updateWhenMouseMoved(e);
  }
  
  @Override
  public void mouseClicked(MouseEvent e) {
    updateWhenMouseMoved(e);
  }
  
  @Override
  public void mouseExited(final MouseEvent e) {
    info = null;
    repaint();
  }
  
  @Override
  public void mouseMoved(final MouseEvent e) {
    updateWhenMouseMoved(e);
  }
  
  @Override
  public void mousePressed(final MouseEvent e) {
  }
  
  @Override
  public void mouseReleased(final MouseEvent e) {
  }

  /**
   * Methode appelee a chque activation
   */
  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    if (!_b) {
      if (target_ != null) {
        target_.repaint();
        
        
        
        
      }
      
    }
  }
  
  private void updateWhenMouseMoved(final MouseEvent e) {
    if (!isActive()) {
      return;
    }
    if (e.isConsumed()) {
      return;
    }
    info = updateWithMouse(e.getX(), e.getY());
    repaint();
  }
}
