/*
 * @creation 6 juil. 2004
 * @modification $Date: 2007-06-28 09:26:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.*;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGDialog.java,v 1.13 2007-06-28 09:26:47 deniger Exp $
 */
public class EGDialog extends JDialog implements ActionListener, CtuluPopupListener.PopupReceiver {

  final EGFillePanel panelDial_;
  int result_ = JOptionPane.CANCEL_OPTION;

  /**
   * @param _owner la fenetre parente
   * @param _g le graphe
   * @param _tableValue true si ajoute du tableau des valeurs
   * @param _okCancel true si on veut afficher des bouton ok, annuler
   */
  public EGDialog(final Frame _owner, final EGFillePanel _g, final boolean _tableValue, final boolean _okCancel) {
    super(_owner);
    final BuPanel ct = new BuPanel();
    ct.setLayout(new BuBorderLayout());
    final BuToolBar b = new BuToolBar();
    panelDial_ = _g;
    final EbliActionInterface[] r = panelDial_.buildActions();
    EbliLib.updateMapKeyStroke(this, r);
    for (int i = 0; i < r.length; i++) {
      if (r[i] == null) {
        b.addSeparator();
      } else {
        b.add(r[i].buildToolButton(EbliComponentFactory.INSTANCE));
      }
    }
    ct.add(b, BuBorderLayout.NORTH);
    CtuluPopupListener popup = new CtuluPopupListener(this, _g);
    _g.getGraphe().addMouseListener(popup);
    if (_tableValue) {
      final EGTableGraphePanel tablePanel = new EGTableGraphePanel();
      tablePanel.setGraphe(_g.getGraphe());
      final BuSplit2Pane spl = new BuSplit2Pane(tablePanel, panelDial_);
      spl.resetToPreferredSizes();
      ct.add(spl, BuBorderLayout.CENTER);
      _g.majSelectionListener(tablePanel);
    } else {
      ct.add(panelDial_, BuBorderLayout.CENTER);
    }
    if (_g.getModel().getNbEGObject() > 1) {
      JComponent c = null;
      final EGSpecificActions specificActions = new EGSpecificActions(_g.getGraphe());
      if (_g.getModel() instanceof EGGrapheSimpleModel) {
        final EGListSimple list = new EGListSimple();
        list.setColumnSelectionAllowed(false);
        final TableModel listModel = ((EGGrapheSimpleModel) _g.getModel()).createTableTreeModel();
        list.setModel(listModel);
        list.setName("LIST_SIMPLE");
        specificActions.setCmd(_g.getGraphe().getCmd());
        list.setIntercellSpacing(new Dimension(0, 0));
        list.setShowHorizontalLines(false);
        list.setRowHeight(35);
        list.setShowVerticalLines(false);
        list.setDefaultRenderer(EGObject.class, new EGTreeCellRenderer(true));
        list.setDefaultEditor(EGObject.class, new EGSimpleTableCellEditor(list));
        c = list;
      } else {
        final EGTree tree = new EGTree();
        tree.setExpandsSelectedPaths(true);
        tree.setActions(specificActions);
        tree.setModel((EGGrapheTreeModel) _g.getModel());
        tree.setSelectionModel(((EGGrapheTreeModel) _g.getModel()).getSelectionModel());
        tree.setMng(_g.getGraphe().getCmd());
        tree.setActions(specificActions);
        tree.setName("ARBRE_COURBES");
        c = tree;
      }

      ct.add(new BuScrollPane(c), BuBorderLayout.WEST);
    }
    if (_okCancel) {
      final BuPanel south = new BuPanel();
      south.setLayout(new BuButtonLayout(5, SwingConstants.RIGHT));
      final BuButton btOk = new BuButton();
      btOk.setText(EbliLib.getS("Continuer"));
      btOk.setActionCommand("OK");
      btOk.addActionListener(this);
      final BuButton btCancel = new BuButton();
      btCancel.setText(EbliLib.getS("Annuler"));
      btCancel.setActionCommand("CANCEL");
      btCancel.addActionListener(this);
      south.add(btOk);
      south.add(btCancel);
      ct.add(south, BuBorderLayout.SOUTH);
    }
    setContentPane(ct);
  }

  public EGDialog(final Frame _owner, final EGGraphe _g, final boolean _tableValue, final boolean _okCancel) {
    this(_owner, new EGFillePanelDialog(_g), _tableValue, _okCancel);

  }

  protected void popupMenu(final int _x, final int _y) {
    panelDial_.popupMenu(_x, _y);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("OK".equals(_e.getActionCommand())) {
      result_ = JOptionPane.OK_OPTION;
      dispose();
    } else if ("CANCEL".equals(_e.getActionCommand())) {
      dispose();
    }
  }

  /**
   * @return le resultat JOptionPane.CANCEL_OPTION ou JOptionPane.OK_OPTION
   */
  public final int getResult() {
    return result_;
  }

  /**
   * @return true si l'utilisateur a valide ses modif
   */
  public final boolean isResponseOk() {
    return result_ == JOptionPane.OK_OPTION;
  }

  @Override
  public void popup(MouseEvent _e) {
    popupMenu(_e.getX(), _e.getY());
    _e.isConsumed();
  }

}
