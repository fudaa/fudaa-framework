/*
 * @creation 5 ao�t 2004
 * @modification $Date: 2007-06-28 09:26:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.*;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JMenu;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;

/**
 * @author Fred Deniger
 * @version $Id: EGFille.java,v 1.26 2007-06-28 09:26:47 deniger Exp $
 */
public abstract class EGFille extends EbliFilleImprimable implements CtuluImageProducer, CtuluUndoRedoInterface,
    BuUndoRedoInterface, CtuluPopupListener.PopupReceiver, BuCutCopyPasteInterface, CtuluExportDataInterface,
    CtuluSelectionInterface {

  protected final EGFillePanel p_;

  protected EGTableGraphePanel tablePanel_;
  JMenu m_;

  JComponent[] specTools_;

  @Override
  public void inverseSelection() {
    p_.inverseSelection();
  }

  @Override
  public void clearSelection() {
    p_.clearSelection();
  }

  @Override
  public void find() {
    p_.find();
  }

  @Override
  public void replace() {
    p_.replace();

  }

  @Override
  public void select() {
    p_.select();
  }

  @Override
  public void startExport(final CtuluUI _impl) {
    EGExporter.startExport(getGraphe(), _impl, null);
  }

  /**
   * @param _g le graphe a visualiser
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   * @param _tableValues si non null, un split pane est ajoute : le composant de droite sera _p
   */
  public EGFille(final EGFillePanel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _tableValues) {
    super(_titre, true, true, true, true, _appli, _id);
    p_ = _g;
    new CtuluPopupListener(this, p_.vue_.graphe_);
    if (_tableValues == null) {
      setContentPane(p_);
    } else {
      final BuPanel centerPanel = new BuPanel();
      centerPanel.setLayout(new BuBorderLayout(0, 1, true, true));
      centerPanel.add(p_, BuBorderLayout.CENTER);
      final BuLabel lb = new BuLabel(CtuluLibString.ESPACE);
      lb.setFont(CtuluLibSwing.getMiniFont());
      centerPanel.add(lb, BuBorderLayout.SOUTH);
      tablePanel_ = _tableValues;
      tablePanel_.setGraphe(_g.vue_.graphe_);
      tablePanel_.addPanelAction(p_);
      tablePanel_.setLabel(lb);
      final BuSplit2Pane spl = new BuSplit2Pane(tablePanel_, centerPanel);
      spl.resetToPreferredSizes();
      setContentPane(spl);

    }
    final Runnable r = new Runnable() {

      @Override
      public void run() {
        p_.vue_.graphe_.restore();
      }
    };
    SwingUtilities.invokeLater(r);
  }

  /**
   * @param _g le graphe a visualiser
   * @param _titre le titre
   */
  public EGFille(final EGGraphe _g, final String _titre) {
    this(_g, _titre, null, null);
  }

  /**
   * @param _g le graphe a visualiser
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   */
  public EGFille(final EGGraphe _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id) {
    this(new EGFillePanel(_g), _titre, _appli, _id, new EGTableGraphePanel());
  }

  /**
   * @param _g le graphe a visualiser
   * @param _titre le titre
   * @param _appli l'appli parent pour recuperer les infos
   * @param _id pour les infos d'impression
   * @param _tablePanel si non null, un split pane est ajoute avec comme composant droit ce panneau.
   */
  public EGFille(final EGGraphe _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _tablePanel) {
    this(new EGFillePanel(_g), _titre, _appli, _id, _tablePanel);
  }

  private void majSelectionListener() {
    p_.majSelectionListener(tablePanel_);
  }

  protected void fillSpecificMenu(final JMenu _m) {
    p_.fillSpecificMenu(_m);
  }

  protected String getMenuTitle() {
    return getTitle();

  }

  protected void getSpecificComponent(final List _l) {
    final EbliActionInterface[] ac = p_.getSpecificActions();
    if (ac == null) { return; }
    EbliLib.updateMapKeyStroke(this, ac);
    final int n = ac.length;
    // JComponent[] r = new JComponent[n];
    final JDesktopPane j = getDesktopPane();
    BuDesktop buJ = null;
    // JComponent c;
    if (j instanceof BuDesktop) {
      buJ = (BuDesktop) j;
    }
    for (int i = 0; i < n; i++) {
      final EbliActionInterface a = ac[i];
      if (a == null) {
        _l.add(null);
      } else {
        if ((buJ != null) && (a instanceof EbliActionPaletteAbstract)) {
          ((EbliActionPaletteAbstract) a).setDesktop(buJ);
        }
        _l.add(a.buildToolButton(EbliComponentFactory.INSTANCE));
      }
    }
  }

  @Override
  public void popup(MouseEvent _evt) {
    p_.popupMenu(_evt.getX(), _evt.getY());
    requestFocusInWindow();
    _evt.consume();
  }

  @Override
  public void clearCmd(final CtuluCommandManager _source) {
    if (getCmdMng() != null && _source != getCmdMng()) {
      getCmdMng().clean();
    }
  }

  @Override
  public void copy() {
    if (tablePanel_ != null) {
      tablePanel_.tableCopy();
    }
  }

  @Override
  public void cut() {
    if (tablePanel_ != null) {
      tablePanel_.tableCut();
    }

  }

  @Override
  public void duplicate() {}

  @Override
  public CtuluCommandManager getCmdMng() {
    return getGraphe().getCmd();
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return p_.getDefaultImageDimension();
  }

  @Override
  public String[] getEnabledActions() {
    if (tablePanel_ == null) { return new String[] { "TOUTSELECTIONNER", "INVERSESELECTION", "CLEARSELECTION",
        "IMPRIMER", "MISEENPAGE", "PREVISUALISER", CtuluLibImage.SNAPSHOT_COMMAND,
        CtuluExportDataInterface.EXPORT_CMD }; }
    return new String[] { "TOUTSELECTIONNER", "INVERSESELECTION", "CLEARSELECTION", "IMPRIMER", "MISEENPAGE",
        "PREVISUALISER", CtuluLibImage.SNAPSHOT_COMMAND, "COLLER", "COPIER", "COUPER",
        CtuluExportDataInterface.EXPORT_CMD };
  }

  /**
   * @return le graphe en question
   */
  public EGGraphe getGraphe() {
    return p_.vue_.graphe_;
  }

  @Override
  public int getNumberOfPages() {
    return 1;
  }

  @Override
  public JMenu[] getSpecificMenus() {
    if (m_ == null) {
      m_ = new BuMenu();
      majSelectionListener();
      m_.setText(getMenuTitle());
      fillSpecificMenu(m_);
    }

    return new JMenu[] { m_ };
  }

  @Override
  public JComponent[] getSpecificTools() {
    if (specTools_ == null) {
      final List l = new ArrayList();
      getSpecificComponent(l);
      majSelectionListener();
      specTools_ = new JComponent[l.size()];
      l.toArray(specTools_);
    }

    return specTools_;
  }

  @Override
  public void paste() {
    if (tablePanel_ != null && getGraphe().getSelectedComponent() != null
        && getGraphe().getSelectedComponent().isCourbe()) {
      tablePanel_.tablePaste();
    }
  }

  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (_page != 0) { return Printable.NO_SUCH_PAGE; }
    EbliPrinter.printImage(_g, _format, getGraphe().cache_);
    return Printable.PAGE_EXISTS;

  }

  @Override
  public final BufferedImage produceImage(final Map _params) {
    return p_.produceImage(_params);
  }

  @Override
  public final BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return p_.produceImage(_w, _h, _params);
  }

  @Override
  public void redo() {
    final CtuluCommandManager c = getCmdMng();
    if (c != null) {
      c.redo();
    }
  }

  @Override
  public void setActive(final boolean _b) {}

  @Override
  public void undo() {
    final CtuluCommandManager c = getCmdMng();
    if (c != null) {
      c.undo();
    }
  }

}
