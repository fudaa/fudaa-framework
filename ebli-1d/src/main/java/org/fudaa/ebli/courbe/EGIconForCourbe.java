/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.Icon;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Un icone
 *
 * @author deniger
 */
public class EGIconForCourbe implements Icon {

  final TraceLigne traceLigne_ = new TraceLigne();
  final TraceIcon traceIcon_ = new TraceIcon();
  final int height = 20;
  final int width = 30;

  @Override
  public int getIconHeight() {
    return height;
  }

  @Override
  public int getIconWidth() {
    return width;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final Graphics2D g = (Graphics2D) _g;
    g.setColor(Color.white);
    g.fillRect(_x, _y, _x + getIconWidth(), _y + getIconHeight());
    final int middleH = _y + getIconHeight() / 2;
    traceLigne_.dessineTrait(g, _x, middleH, _x + getIconWidth(), middleH);
    traceIcon_.paintIconCentre(g, _x + getIconWidth() / 2, middleH);
  }

  public void updateFromCourbe(final EGCourbe _c) {
    traceLigne_.getModel().updateData(_c.getLigneModel());
    traceIcon_.getModel().updateData(_c.getIconModel());
    traceIcon_.setTaille(getIconHeight() / 4); // a voir
    // a voir
  }
}
