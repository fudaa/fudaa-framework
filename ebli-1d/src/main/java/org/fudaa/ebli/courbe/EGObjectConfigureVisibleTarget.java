/*
 * @creation 28 nov. 06
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.beans.PropertyChangeListener;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.controle.BSelecteurTextField;

/**
 * @author fred deniger
 * @version $Id: EGObjectConfigureVisibleTarget.java,v 1.2 2007-05-04 13:49:41 deniger Exp $
 */
public class EGObjectConfigureVisibleTarget implements BSelecteurTargetInterface {

  final EGObject object_;

  public EGObjectConfigureVisibleTarget(final EGObject _axe) {
    super();
    object_ = _axe;
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    object_.addListener(_key, _l);

  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == BSelecteurTextField.TITLE_PROPERTY) {
      return object_.getTitle();
    }
    return Boolean.valueOf(object_.isVisible());
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    object_.removeListener(_key, _l);

  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (_newProp == null) {
      return false;
    }
    if (_key == BSelecteurTextField.TITLE_PROPERTY) {
      return object_.setTitle((String) _newProp);
    }
    return object_.setVisible(((Boolean) _newProp).booleanValue());
  }

}
