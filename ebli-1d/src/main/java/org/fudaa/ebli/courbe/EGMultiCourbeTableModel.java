/*
 * @creation 24 oct. 06
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import jxl.write.WritableCell;
import org.fudaa.ctulu.table.CtuluTableModelInterface;

/**
 * @author fred deniger
 * @version $Id: EGMultiCourbeTableModel.java,v 1.4 2007-05-04 13:49:41 deniger Exp $
 */
public class EGMultiCourbeTableModel extends AbstractTableModel implements CtuluTableModelInterface {

  final EGCourbe[] allCourbes_;
  final EGCourbe[] selectedCourbes_;
  boolean showOnlySelected_;
  EGExportData current_;
  final EGGraphe g_;
  private List<String> comments;
  private final boolean showLabel;
//-- ahx --//
  private boolean isSelectRows = false;
  private int[] selectedRows = null;
  
  public EGMultiCourbeTableModel(final EGCourbe[] _allCourbes, final EGCourbe[] _selectedCourbes, final EGGraphe _g, final boolean showLabel) {
    super();
    allCourbes_ = _allCourbes == null ? new EGCourbe[0] : _allCourbes;
    selectedCourbes_ = _selectedCourbes == null ? new EGCourbe[0] : _selectedCourbes;
    g_ = _g;
    this.showLabel = showLabel;
    current_ = EGExportData.createExportData(allCourbes_, g_, false, false, showLabel, null);
    initSelectedRows();
    
  }
  

  private void initSelectedRows() {
	for(EGCourbe c: allCourbes_) {
		if(c != null && c.getModel() != null && c.getModel().getInitRows() != null)
			selectedRows = c.getModel().getInitRows() ;
	}
	
}


@Override
  public List<String> getComments() {
    return comments;
  }

  public void setComments(List<String> comments) {
    this.comments = comments;
    current_.setComments(comments);
  }

  @Override
  public WritableCell getExcelWritable(final int _rowInModel, final int _colInModel, final int _rowInXls, final int _colInXls) {
    return current_.getExcelWritable(_rowInModel, _colInModel, _rowInXls, _colInXls);
  }

  @Override
  public int getMaxCol() {
	  
    return current_.getMaxCol();
  }

  @Override
  public int getMaxRow() {
	  if(isSelectRows()) {
		  return selectedRows.length;
	  }
    return current_.getMaxRow();
  }

  @Override
  public Object getValue(final int _row, final int _col) {
	  if(isSelectRows()) {
		  return current_.getValue(selectedRows[_row], _col);
	  }
    return current_.getValue(_row, _col);
  }
  boolean showOneTime_;

  @Override
  public int getColumnCount() {
    return getMaxCol();
  }

  @Override
  public int getRowCount() {
	  if(isSelectRows()) {
		  return selectedRows.length;
	  }
    return getMaxRow();
  }

  @Override
  public Class getColumnClass(final int _columnIndex) {
    return current_.getColumnClass(_columnIndex);
  }

  @Override
  public String getColumnName(final int _column) {
    return current_.getColumnName(_column);
  }

  @Override
  public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
    return false;
  }

  @Override
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    return getValue(_rowIndex, _columnIndex);
  }

  protected boolean isShowOneTime() {
    return showOneTime_;
  }

  protected EGCourbe[] getCourbes() {
    return showOnlySelected_ ? selectedCourbes_ : allCourbes_;
  }

  protected void setShowOneTime(final boolean _showOneTime) {
    if (showOneTime_ != _showOneTime) {
      showOneTime_ = _showOneTime;
      upateCurrentModel();
    }
  }

  private void upateCurrentModel() {
    current_ = EGExportData.createExportData(getCourbes(), g_, showOneTime_, false, showLabel, null);
    fireTableStructureChanged();
  }

  protected boolean isShowOnlySelected() {
    return showOnlySelected_;
  }

  protected void setShowOnlySelected(final boolean _showOnlySelected) {
    if (showOnlySelected_ != _showOnlySelected) {
      showOnlySelected_ = _showOnlySelected;
      upateCurrentModel();
    }
  }

public boolean isSelectRows() {
	return isSelectRows;
}

public void setSelectRows(boolean isSelectRows) {
	this.isSelectRows = isSelectRows;
}


@Override
public int[] getSelectedRows() {
	return selectedRows;
}
}
