/*
 * @creation 8 mars 07
 * @modification $Date: 2007-05-22 14:19:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * @author fred deniger
 * @version $Id: EGComboBoxModelForX.java,v 1.3 2007-05-22 14:19:04 deniger Exp $
 */
public class EGComboBoxModelForX extends AbstractListModel implements ComboBoxModel {

  final EGCourbe courbe_;
  Object selected_;

  public EGComboBoxModelForX(final EGCourbe _courbe) {
    super();
    courbe_ = _courbe;
  }

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

  @Override
  public void setSelectedItem(final Object _anItem) {
    if (_anItem == selected_ || (_anItem != null && _anItem.equals(selected_))) {
      return;
    }
    selected_ = _anItem;
    fireContentsChanged(this, -1, -1);

  }

  @Override
  public Object getElementAt(final int _index) {
    return courbe_.getAxeX().getStringAffiche(courbe_.getModel().getX(_index));
  }

  @Override
  public int getSize() {
    return courbe_.getModel().getNbValues();
  }

}
