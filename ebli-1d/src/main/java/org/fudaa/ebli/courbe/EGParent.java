/*
 * @creation 28 nov. 06
 * @modification $Date: 2007-03-09 08:38:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.tree.TreeNode;

/**
 * @author fred deniger
 * @version $Id: EGParent.java,v 1.2 2007-03-09 08:38:20 deniger Exp $
 */
public interface EGParent {

  /**
   * A utiliser lorsque l'apparence d'un objet est modifie.
   * 
   * @param _o l'objet modifie
   */
  void fireCourbeContentChanged(EGObject _o,boolean _mustRestore);

  void fireCourbeAspectChanged(final EGObject _o, final boolean _visibility);

  void fireAxeAspectChanged(EGAxe _a);

  EGAxeHorizontal getAxeX();

  void monter(EGObject _o);
  
  boolean canMonter(EGObject _o);
  boolean canDescendre(EGObject _o);

  void descendre(EGObject _o);

  void enPremier(EGObject _o);

  void enDernier(EGObject _o);
  
  EGGrapheModel getMainModel();

  /**
   *
   * @return true if it will be display
   */
  boolean isDisplayed();

  interface Tree extends EGParent {

    void fireStructureChanged(EGGroup _o, EGCourbeChild _moved);
  }
  
  interface TreeNodes extends Tree, TreeNode {

  }

}
