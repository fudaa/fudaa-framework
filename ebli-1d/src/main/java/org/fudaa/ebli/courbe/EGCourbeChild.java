/*
 * @creation 11 d�c. 2003
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.courbe;

import com.memoire.fu.FuLog;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.tree.TreeNode;

/**
 * Cette Courbe suppose que tous les x du mod�le sont rang�es dans l'ordre croissant.
 *
 * @author deniger
 * @version $Id: EGCourbeChild.java,v 1.8 2007-05-04 13:49:41 deniger Exp $
 */
public class EGCourbeChild extends EGCourbe implements TreeNode {

  EGGroup parent_;

  public EGCourbeChild(final EGGroup _m) {
    this(_m, null);
  }

  public EGCourbeChild(final EGGroup _m, final EGModel _model) {
    super(_model);
    parent_ = _m;
  }

  protected EGCourbePersistBuilder<? extends EGCourbeChild> createPersistBuilder() {
    return new EGCourbePersistBuilderDefault();
  }

  @SuppressWarnings("unchecked")
  public void copyGraphicsConfiguration(EGCourbe to) {
    EGCourbePersistBuilder createPersistBuilder = createPersistBuilder();
    createPersistBuilder.initGraphicConfiguration(to, createPersistBuilder.persistGraphicsData(this));
  }

  @Override
  public EGCourbePersist getPersitUiConfig() {
    EGCourbePersistBuilder createPersistBuilder = createPersistBuilder();
    return createPersistBuilder.persistGraphicsData(this);
  }

  @Override
  public void applyPersitUiConfig(EGCourbePersist persist) {
    if (persist != null) {
      createPersistBuilder().initGraphicConfiguration(this, persist);
    }
  }

  @Override
  boolean setYaxe(final EGAxeVertical _v) {
    FuLog.warning(new Throwable());
    return false;
  }

  @Override
  public Enumeration children() {
    return null;
  }

  @Override
  protected EGParent getEGParent() {
    return parent_;
  }

  @Override
  public boolean isDisplayed() {
    return super.isDisplayed();
  }

  @Override
  public final boolean getAllowsChildren() {
    return false;
  }

  @Override
  public EGAxeVertical getAxeY() {
    return parent_ == null ? null : parent_.getAxeY();
  }

  @Override
  public TreeNode getChildAt(final int _childIndex) {
    return null;
  }

  @Override
  public int getChildCount() {
    return 0;
  }

  @Override
  public final int getIndex(final TreeNode _node) {
    return -1;
  }

  @Override
  public TreeNode getParent() {
    return parent_;
  }

  public EGGroup getParentGroup() {
    return parent_;
  }

  @Override
  public boolean isLeaf() {
    return true;
  }

  public void setParentGroup(final EGGroup _g) {
    parent_ = _g;
  }

  @Override
  public EGCourbe duplicate(EGParent newParent, EGGrapheDuplicator _duplicator) {

    EGCourbeChild duplic = null;

    if (this.getModel() != null) {
      duplic = new EGCourbeChild((EGGroup) newParent, this.getModel().duplicate());
    } else {
      duplic = new EGCourbeChild((EGGroup) newParent, null);
    }

    duplic.isVisible_ = this.isVisible_;

    duplic.displayTitleOnCurve_ = this.displayTitleOnCurve_;
    // if (this.font_ != null)
    // duplic.font_ = new Font(this.font_.getFamily(), this.font_.getStyle(),
    // this.font_.getSize());
    if (this.tbox_ != null) {
      duplic.tbox_ = this.tbox_.duplicate();
    }

    // -- duplication du egCourbeSurfacePainter --//
    if (this.surfacePainter_ != null) {
      duplic.surfacePainter_ = this.surfacePainter_.duplicate(duplic, _duplicator);
    }

    // -- aspect couleur contour+surface de la courbe --//
    duplic.setAspectContour(this.getAspectContour());
    duplic.setAlpha(this.getAlpha());
    if (this.getFont() != null) {
      duplic.setFont(new Font(this.getFont().getFamily(), this.getFont().getStyle(), this
              .getFont().getSize()));
    }
    if (this.getIconModel() != null) {
      duplic.setIconeModel(this.getIconModel().cloneData());
    }
    if (this.getMarkLigneModel() != null) {
      duplic.setLigneMark(this.getMarkLigneModel().buildCopy().getModel());
    }
    if (this.getLigneModel() != null) {
      duplic.setLigneType(this.getLigneModel());
    }
    duplic.setTitle(this.getTitle());

    // -- duplicate les marqueurs --//
    duplic.setMarqueurs(new ArrayList<EGCourbeMarqueur>());
    for (EGCourbeMarqueur mark : getMarqueurs()) {
      duplic.getMarqueurs().add(mark.duplique());
    }

    return duplic;
  }

  public void initGraphicConfigurationFrom(EGCourbeChild courbe) {
    EGCourbePersistBuilderDefault builder = new EGCourbePersistBuilderDefault();
    EGCourbePersist createPersistUnit = builder.persistGraphicsData(courbe);
    if (createPersistUnit != null) {
      builder.initGraphicConfiguration(this, createPersistUnit);
    }
  }
}
