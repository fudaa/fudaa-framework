/*
 * @creation 22 juin 2004
 * 
 * @modification $Date: 2007-03-23 17:25:14 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 * @author Fred Deniger
 * @version $Id: EGRepere.java,v 1.13 2007-03-23 17:25:14 deniger Exp $
 */
public class EGRepere {

  private EGAxeHorizontal xAxe_;
  private EGMarges marges_;
  private int w_;
  private int h_;

  public EGRepere() {
    super();
    marges_ = new EGMarges();
  }

  public int getMargesGauche() {
    return marges_.getGauche();
  }

  public int getMargesDroite() {
    return marges_.getDroite();
  }

  public int getMargesBas() {
    return marges_.getBas();
  }
  public int getMargesHaut() {
    return marges_.getHaut();
  }

  public void initFor(final Graphics2D _g2d, final EGGraphe _g) {
    w_ = _g.getWidth();
    h_ = _g.getHeight();
    marges_ = _g.computeMarges(_g2d);
    marges_.ajouteEmptyBorder(3);
    xAxe_ = _g.getModel().getAxeX();
  }

  public void setXAxe(final EGAxeHorizontal _axe) {
    xAxe_ = _axe;
  }

  public int getMaxEcranY() {
    return h_ - marges_.getBas();
  }

  public int getMinEcranY() {
    return marges_.getHaut();
  }

  public int getMaxEcranX() {
    return w_ - marges_.getDroite();
  }

  public int getMinEcranX() {
    return marges_.getGauche();
  }

  public EGAxeHorizontal getXAxe() {
    return xAxe_;
  }

  public int getWSansMarges() {
    return w_ - marges_.getHorizontalMarges();
  }

  public int getHSansMarges() {
    return h_ - marges_.getVerticalMarges();
  }

  public void versReel(final int _xEcran, final int _yEcran, final Point2D.Double _ptReel, final EGAxeVertical _yAxe) {
    _ptReel.setLocation(getXReel(_xEcran), getYReel(_yEcran, _yAxe));
  }

  public double getXReel(final int _xEcran) {
    return xAxe_.getValueFromPosition(xAxe_.getMinimum() + (_xEcran - marges_.getGauche()) * xAxe_.getEcart()
            / getWSansMarges());
  }

  public double getYReel(final int _yEcran, final EGAxeVertical _yAxe) {
    return _yAxe.getValueFromPosition(_yAxe.getMinimum() + ((-_yEcran + h_ - marges_.getBas()) * (_yAxe.getEcart()))
            / getHSansMarges());
  }

  public double getDistanceYReel(final int distance, final EGAxeVertical yAxe) {
    return yAxe.getValueFromPosition(distance * yAxe.getEcart() / getHSansMarges());
  }

  public double getDistanceXReel(final int distance) {
    return xAxe_.getValueFromPosition(distance * xAxe_.getEcart() / getHSansMarges());
  }

  public int getXEcran(final double _x) {
    final double x = xAxe_.getPositionFromValue(_x);
    return marges_.getGauche() + (int) (getWSansMarges() * ((x - xAxe_.getMinimum()) / xAxe_.getEcart()));
  }

  public int getYEcran(final double _y, final EGAxeVertical _axeY) {
    final double y = _axeY.getPositionFromValue(_y);
    return -marges_.getBas() + h_ - (int) (getHSansMarges() * ((y - _axeY.getMinimum()) / _axeY.getEcart()));
  }

  public int getH() {
    return h_;
  }

  public int getW() {
    return w_;
  }

  public EGRepere duplicate(EGGrapheDuplicator _duplicator) {

    EGRepere duplic = new EGRepere();
    duplic.h_ = this.h_;
    duplic.w_ = this.w_;
    duplic.xAxe_ = this.xAxe_.duplicate();
    duplic.marges_ = new EGMarges(marges_);
    return duplic;

  }
}