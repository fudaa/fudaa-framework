/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import java.awt.Graphics2D;

/**
 *
 * @author Frederic Deniger
 */
public interface EGHorizontalBanner {

  void dessine(final Graphics2D _g, final EGRepere _f);

  int getHeightNeeded(final Graphics2D _g);
}
