package org.fudaa.ebli.courbe;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

/**
 * Action qui ouvre une palette contenant un JXTreeTable pour l'affichage des
 * ordonnees pour un point selectionn� donn�
 * 
 * @author Adrien Hadoux
 * 
 */
public class EGActionPaletteAllCoordonnees extends EbliActionSimple implements EGGrapheModelListener {

  
  public class TreeTableModelGraphe extends DefaultTreeTableModel {
    final String[] titre_;

    // -- data correspondant au x donn� --//
    double[] dataY_ = new double[0];

    public TreeTableModelGraphe() {
      String[] val = { "Label", "Y" };
      titre_ = val;
    }

    @Override
    public int getColumnCount() {

      return titre_.length;
    }

    @Override
    public boolean isCellEditable(Object _node, int _column) {
      return false;
    }

    @Override
    public String getColumnName(int _columnIndex) {
      return titre_[_columnIndex];
    }

    public int getRowCount() {
      return graphe_.getModel().getCourbes().length;

    }

    @Override
    public Object getValueAt(Object node, int column) {
      Object res = "n/a";
      if (node instanceof DefaultMutableTreeTableNode) {
        DefaultMutableTreeTableNode defNode = (DefaultMutableTreeTableNode) node;
        if (defNode.getUserObject() instanceof EGGroup) {
          if (column == 0)
            return ((EGGroup) defNode.getUserObject()).getTitle();
          else
            return "";
        } else if (defNode.getUserObject() instanceof EGCourbeChild) {
          if (column == 0)
            return ((EGCourbeChild) defNode.getUserObject()).getTitle();
          else
            return getValueForSelectedX(((EGCourbeChild) defNode.getUserObject()));
        }
      }
      return res;
    }

  }
  
   final EGInteractionSuiviAllOrdonnees suiviInfos_;
  final EGGraphe graphe_;
  final EGFillePanel panelGraphe_;
  HashMap<EGCourbe, Double> mapCurvesY_ = new HashMap<>();
  final TreeTableModelGraphe modelGraphe_ = new TreeTableModelGraphe();
  JXTreeTable treeTableNodes_ = new JXTreeTable(modelGraphe_);
  
  
  public EGActionPaletteAllCoordonnees(EGFillePanel panel) {
    super(EbliLib.getS("Coordonn�es"), EbliResource.EBLI.getIcon("crystal_pointeuraide.png"), "SUIVIALL");
   
    panelGraphe_ = panel;
    EGVue vue = panel.vue_;
    graphe_ = vue.getGraphe();

     suiviInfos_ = new EGInteractionSuiviAllOrdonnees(graphe_, this);
    //
    // // -- creation des interactions --//
    vue.addInteractiveCmp(suiviInfos_);
    suiviInfos_.addPropertyChangeListener(panel);
     
     
     constructStructureModel();
    // final EGTreeCellRenderer treeCellRenderer = new EGTreeCellRenderer();
    // treeTableNodes_.setTreeCellRenderer(treeCellRenderer);

    // treeTableNodes_.setLeafIcon((EGObject) panel.getModel().getCourbes()[0]);
    // treeTableNodes_.setOpenIcon((EGObject) panel.getModel().getEGObject(0));
    // treeTableNodes_.setClosedIcon((EGObject)
    // panel.getModel().getEGObject(0));
    // treeTableNodes_.setRowHeight(30);
    // treeTableNodes_.setCollapsedIcon(_a.getModel().getEGObject(0));

    // panel.getModel().addModelListener(this);
     
  }
  

  /**
   * Met a jour a chaque clic dans le graphe
   */
  public void maj(double x, HashMap<EGCourbe, Double> mapCurvesY) {
    dialog_.setTitle(EbliResource.EBLI.getString("X s�lectionn�:") + x);
    
    mapCurvesY_ = mapCurvesY;
    
    treeTableNodes_.setTreeTableModel(constructStructureModel());
    treeTableNodes_.expandAll();
  }
  
  JDialog dialog_;
  @Override
  public void actionPerformed(final ActionEvent _e) {
    
    CtuluUIDialog ui = new CtuluUIDialog(graphe_);
    final Frame f = CtuluLibSwing.getFrameAncestorHelper(ui.getParentComponent());
   
    if (dialog_ == null) {
    dialog_ = new JDialog(f);

    dialog_.addWindowListener(new WindowAdapter() {

        

        @Override
        public void windowClosed(WindowEvent _e) {
          // on desactive l action de suivi sur le graphe
          suiviInfos_.setActive(false);
          dialog_ = null;
        }

        @Override
         public void windowClosing(WindowEvent _e) {
          suiviInfos_.setActive(false);
          dialog_ = null;
        }

       

      });
    }
    // dialog_.setModal(false);
    // dialog_.setContentPane(buildContentPane());
    // dialog_.setSize(200, 400);
    // dialog_.setVisible(true);

    dialog_.setModal(false);
    dialog_.pack();
    dialog_.setTitle(getTitle());
    // JPanel container=new JPanel(new BorderLayout());
    // JXTreeTable tree = new JXTreeTable(constructStructureModel());
    // tree.setLeafIcon((EGObject) panelGraphe_.getModel().getCourbes()[0]);
    // tree.setOpenIcon((EGObject) panelGraphe_.getModel().getEGObject(0));
    // tree.setClosedIcon((EGObject) panelGraphe_.getModel().getEGObject(0));
    // tree.setRowHeight(30);
    // tree.getColumn(0).setMinWidth(200);
    // container.add(tree, BorderLayout.CENTER);
    // container.add(tree.getTableHeader(), BorderLayout.NORTH);
    
    //container.add(buildTree());
    JPanel container=new JPanel(new BorderLayout());
    container.add(new JScrollPane(buildTree()),BorderLayout.CENTER);
    container
        .add(
            new JLabel(
               "<html> <body><b>"
            + EbliResource.EBLI.getString("Cliquez sur le graphe pour s�lectionner le x voulu.") + "</b><br />"
            + EbliResource.EBLI
               .getString("Fermer la palette pour quitter le mode s�lection du graphe")
            + " </body></html>"),
            BorderLayout.NORTH);
    dialog_.setContentPane(container);
    dialog_.setSize(400, 400);
    dialog_.setLocationRelativeTo(CtuluLibSwing.getFrameAncestor(ui.getParentComponent()));
    dialog_.setVisible(true);
    // dialog.dispose();
    // dialog = null;

    // --activation du suivi --//
    suiviInfos_.setActive(true);
    
  }

  
  public JComponent buildTree() {
   // return suiviInfos_.getTableInfos();

    // TreeTableModelGraphe model = constructStructureModel();
    // JXTreeTable tree = new JXTreeTable(model);
    // tree.setLeafIcon((EGObject) panelGraphe_.getModel().getCourbes()[0]);
    // tree.setOpenIcon((EGObject) panelGraphe_.getModel().getEGObject(0));
    // tree.setClosedIcon((EGObject) panelGraphe_.getModel().getEGObject(0));
    // tree.setRowHeight(30);
    //
    // JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    // panel.add(new JLabel(EbliResource.EBLI.getString("ordonn�es pour X=")));
    // // panel.add(labelX_);
    // JPanel tableInfo = new JPanel(new BorderLayout());
    // tableInfo.add(panel, BorderLayout.NORTH);
    // tableInfo.add(new JScrollPane(tree), BorderLayout.CENTER);
    // tableInfo.add(tree.getTableHeader(), BorderLayout.PAGE_START);
    JPanel container = new JPanel(new BorderLayout());
    treeTableNodes_ = new JXTreeTable(constructStructureModel());
   
    treeTableNodes_.setLeafIcon(panelGraphe_.getModel().getCourbes()[0]);
    treeTableNodes_.setOpenIcon(panelGraphe_.getModel().getEGObject(0));
    treeTableNodes_.setClosedIcon(panelGraphe_.getModel().getEGObject(0));
    treeTableNodes_.setRowHeight(30);
    treeTableNodes_.getColumn(0).setMinWidth(200);
    // treeTableNodes_.setMaximumSize(new Dimension(, 390));
    container.add(treeTableNodes_, BorderLayout.CENTER);
    container.add(treeTableNodes_.getTableHeader(), BorderLayout.NORTH);

    return container;
  }

  
  /** Methode qui construit la structure du model **/
  public TreeTableModelGraphe constructStructureModel() {
    DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode("Ordonn�es pour X donn�" );
    for (int i = 0; i < graphe_.getModel().getNbEGObject(); i++) {
      final EGObject ci = graphe_.getModel().getEGObject(i);
      if (ci instanceof EGGroup) {
        EGGroup groupe = (EGGroup) ci;
        DefaultMutableTreeTableNode group=new DefaultMutableTreeTableNode(groupe);
        root.add(group);
        
        for (int k = 0; k < groupe.getChildCount(); k++) {
         EGCourbeChild courbe = groupe.getCourbeAt(k);
         DefaultMutableTreeTableNode courb = new DefaultMutableTreeTableNode(courbe);
         group.add(courb);
        }
      }

      
    }
    TreeTableModelGraphe model=new TreeTableModelGraphe();
    model.setRoot(root);
    
    return model;
  }

  /**
   * Recupere la valeure associee a la courbe dans la map
   * 
   * @param child
   * @return
   */
  private Double getValueForSelectedX(EGCourbe child) {
    if (child != null && mapCurvesY_.containsKey(child))
      return mapCurvesY_.get(child);
    else
      return new Double(0);
  }
  

  JPanel tableInfo_;
  JLabel labelX_ = new JLabel("");
 

  @Override
  public void axeAspectChanged(EGAxe _c) {
    constructStructureModel();

  }

  @Override
  public void axeContentChanged(EGAxe _c) {
    constructStructureModel();

  }

  @Override
  public void courbeAspectChanged(EGObject _c, boolean _visibil) {
    constructStructureModel();

  }

  @Override
  public void courbeContentChanged(EGObject _c, boolean _mustRestore) {
    constructStructureModel();

  }

  @Override
  public void structureChanged() {
    constructStructureModel();

  }
  
  
}
