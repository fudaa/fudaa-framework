package org.fudaa.ebli.courbe;

import com.memoire.fu.FuLog;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.persistence.FileStreamStrategy;
import com.thoughtworks.xstream.persistence.XmlArrayList;
import gnu.trove.TIntArrayList;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.XstreamCustomizer;

/**
 * Classe qui se charge de rendre persistant les donn�es des graphes en xml.
 *
 * @author Adrien Hadoux
 */
public class EGGraphePersist {

  private static final String PANEL = "Panel:";
  /**
   * Path du repertoire dans lequel seront enregistr� toute les courbes, groupes et model...
   */
  final String directoryPath_;
  private XStream parser_;
  private static final String MAINFILE = "descriptorGraphe.xml";
  private static final String SPECIFIQUEFILE = "specificDatas.xml";
  private static final String COURBEDIRECTORY = "COURBES";
  final CtuluAnalyze log = new CtuluAnalyze();

  public EGGraphePersist(String directoryPath_) throws IOException {
    this.directoryPath_ = directoryPath_;
  }

  private File getBinFileForCurve(int i) {
    return new File(getDirectoryCurves(), i + ".bin");
  }

  /**
   * Recupere le parser adequat.
   *
   * @return
   */
  private XStream getParser() {
    if (parser_ == null) {
      parser_ = initXmlParser();
    }
    return parser_;
  }

  /**
   * Init le parser avec les alias adequats.
   *
   * @return
   */
  private XStream initXmlParser() {
    XStream xstream = new XStream(new DomDriver());
    // -- creation des alias pour que ce soit + parlant dans le xml file --//
    xstream.alias("Groupe", EGGroupPersist.class);
    xstream.alias("AxeX", EGAxeHorizontalPersist.class);
    xstream.alias("AxeY", EGAxeVerticalPersist.class);
    xstream.alias("Courbe", EGCourbePersist.class);
    XStream.setupDefaultSecurity(xstream);
    xstream.allowTypesByWildcard(new String[] {
            "org.fudaa.**"
    });
    return xstream;
  }

  /**
   * Retour,e le repertoire qui contient les courbes
   *
   * @return
   */
  private File getDirectoryCurves() {
    return new File(directoryPath_ + File.separator + COURBEDIRECTORY);
  }

  /**
   * Retourne le path vers le fiochier principal des courbes
   *
   * @return
   */
  private String getMainFilePath() {
    return directoryPath_ + File.separator + MAINFILE;
  }

  private String getPanelFilePath() {
    return directoryPath_ + File.separator + MAINFILE;
  }

  /**
   * Retourne le chemin vers le fichier de datats specifiques.
   *
   * @return
   */
  private String getSpecifiqueDataFilePath() {
    return directoryPath_ + File.separator + SPECIFIQUEFILE;
  }

  /**
   * Retourne la liste persistante des fichiers
   *
   * @return
   */
  List<EGCourbePersist> getPersitantCurvesList(boolean init) {
    List<EGCourbePersist> listPersitante = new XmlArrayList(new FileStreamStrategy(getDirectoryCurves()));
    if (init) {
      clearDatas(listPersitante);
    }
    return listPersitante;
  }

  /**
   * Methode qui nettoie le contenu des datas
   */
  private void clearDatas(List<EGCourbePersist> list) {
    if (list == null) {
      return;
    }
    while (list.size() != 0) {
      list.remove(0);
    }

  }

  /**
   * Enrtegistre le graphe au format persistant xml.
   *
   * @param parameters des parametres supplementaires utiles.
   * @throws IOException
   */
  public void savePersitGrapheXml(String graphePanelClass, EGGraphe grapheTopersist, Map parameters) {
    String mainfile = getMainFilePath();
    // -- outputstream du xstream pour enregistrement des diff�rents groupes--//
    ObjectOutputStream out = null;
    try {
      final XStream parser = getParser();
      XstreamCustomizer customizer = (XstreamCustomizer) parameters.get(XstreamCustomizer.PARAMETER_ID);
      if (customizer != null) {
        customizer.configureXstream(parser);
      }
      out = EbliLib.createObjectOutpuStream(new File(mainfile), parser);

      // -- enregistrement des courbes --//
      // strategy pour la liste xml: aun fichier par courbe.
      File fichierCourbes = getDirectoryCurves();
      if (fichierCourbes.mkdir() || fichierCourbes.isDirectory()) {
        // StreamStrategy strategy = new FileStreamStrategy(fichierCourbes);
        // creates the list of curves:
        List<EGCourbePersist> persistCurves = new ArrayList<>();
        List<EGCourbe> persistedCurves = new ArrayList<>();

        List<EGGroupPersist> listeGroupePersistance = new ArrayList<>(grapheTopersist.getModel().getNbEGObject());
        int nbEgObjects = grapheTopersist.getModel().getNbEGObject();
        int idxGroup = 0;
        int idxCurve = 0;
        Map<EGCourbe, Integer> idByCurve = new HashMap<>();
        Map<EGGroup, Integer> groupIdx = new HashMap<>();
        for (int egObjectIdx = 0; egObjectIdx < nbEgObjects; egObjectIdx++) {
          EGObject objet = grapheTopersist.getModel().getEGObject(egObjectIdx);
          if (objet instanceof EGGroup) {
            EGGroup groupe = (EGGroup) objet;
            groupIdx.put(groupe, idxGroup);
            // WARN les groupes enregistre l'axe initial.
            EGGroupPersistBuilder<EGGroup> persistBuilder = groupe.createPersistBuilder();
            EGGroupPersist persist = persistBuilder.persistObject(groupe, grapheTopersist, null);
            persist.setIdx(idxGroup++);
            int groupId = persist.getIdx();
            listeGroupePersistance.add(persist);
            EGObject[] childs = groupe.getEGChilds();
            for (int k = 0; k < childs.length; k++) {
              if (childs[k] instanceof EGCourbeChild) {
                // -- ajout de la courbe persitante dans un fichier avec pour groupee le i eme
                EGCourbeChild courbeChild = (EGCourbeChild) childs[k];
                persistCurves.add(createPersist(courbeChild, groupId, grapheTopersist, idxCurve));
                idByCurve.put(courbeChild, idxCurve);
                idxCurve++;
                persistedCurves.add(courbeChild);
              }
            }
          } else if (objet instanceof EGCourbeChild) {
            // le groupe d appartenance est -1
            persistCurves.add(createPersist((EGCourbeChild) objet, -1, grapheTopersist, idxCurve));
            idByCurve.put((EGCourbeChild) objet, idxCurve);
            idxCurve++;
            persistedCurves.add((EGCourbeChild) objet);
          }
        }
        int nbCourbe = persistCurves.size();
        EGCourbeSurfacePersistBuilder builder = new EGCourbeSurfacePersistBuilder();
        List<EGCourbePersist> savedCurves = getPersitantCurvesList(true);
        for (int i = 0; i < nbCourbe; i++) {
          EGCourbe egCourbe = persistedCurves.get(i);
          if (egCourbe.getSurfacePainter() != null) {
            persistCurves.get(i).surfacePainter = builder.build(egCourbe.getSurfacePainter(), idByCurve);
          }
          savedCurves.add(persistCurves.get(i));

        }
        // --on enregistre des infos bidons pour l'utilisateur --//
        out.writeObject(PANEL + graphePanelClass);
        String minMAx = "MinX: " + grapheTopersist.getXMin() + " MaxX: " + grapheTopersist.getXMax();
        out.writeObject(minMAx);
        // -- FIN BIDON --//

        // --on enregistre l'axe des x --//
        out.writeObject(new EGAxeHorizontalPersist(grapheTopersist.getModel().getAxeX()));

        // -- on enregistre le type de model du graphe --//
        String model = grapheTopersist.getModel().getClass().toString();
        out.writeObject(model);
        // --on ecrit le nombre de groupes --//
        int nbGroups = listeGroupePersistance.size();
        out.writeInt(nbGroups);
        // -- on ecrit la liste des groupes --//
        for (EGPersist gp : listeGroupePersistance) {
          out.writeObject(gp);
        }
        Map<EGAxeVertical, List<EGObject>> mergeAxis = grapheTopersist.getMergeAxis();
        List<EGMergeAxisPersist> merges = new ArrayList<>();
        if (mergeAxis.size() > 0) {
          for (Map.Entry<EGAxeVertical, List<EGObject>> entry : mergeAxis.entrySet()) {
            List<EGObject> list = entry.getValue();
            final TIntArrayList groupIdxInMap = getGroupIdxInMap(groupIdx, list);
            if (groupIdxInMap.size() > 0) {
              merges.add(new EGMergeAxisPersist(groupIdxInMap.toNativeArray(), entry.getKey()));
            }
          }
        }
        if (merges.size() > 0) {
          out.writeObject(merges);
        }
      }
    } catch (IOException e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(out);
    }

    // -- on enregistre les donn�es specifiques en fonction du type de grapheModel --//
    Object specifiquesDatas = grapheTopersist.getModel().getSpecificPersitDatas(parameters);
    if (specifiquesDatas == null) {
      return;
    }
    try {
      out = EbliLib.createObjectOutpuStream(new File(getSpecifiqueDataFilePath()), getParser());
      out.writeObject(specifiquesDatas);
    } catch (IOException e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(out);
    }

  }

  @SuppressWarnings("unchecked")
  private EGCourbePersist createPersist(EGCourbeChild egCourbeChild, int groupIdx, EGGraphe grapheTopersist, int courbeUniqueId) {
    EGCourbePersistBuilder<EGCourbeChild> builder = (EGCourbePersistBuilder<EGCourbeChild>) egCourbeChild.createPersistBuilder();
    EGCourbePersist courbePersist = builder.persistObject(egCourbeChild, grapheTopersist, getBinFileForCurve(courbeUniqueId));
    courbePersist.setId(courbeUniqueId);
    courbePersist.setIdgroup(groupIdx);
    return courbePersist;
  }

  private static TIntArrayList getGroupIdxInMap(Map<EGGroup, Integer> groupIdx, List<EGObject> list) {
    TIntArrayList idx = new TIntArrayList(list.size());
    for (EGObject o : list) {
      if (groupIdx.containsKey(o)) {
        idx.add(groupIdx.get(o).intValue());
      }
    }
    return idx;
  }

  /**
   * Genere une instance de model par rapport au model serializ�. Renvoie forc�ment une class implements EGGrapheTreeModel sinon null
   *
   * @param className
   * @return
   * @throws ClassNotFoundException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  public EGGrapheTreeModel generateModelInstance(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
    if (className.startsWith("class ")) {
      className = className.substring("class ".length());
    }
    Object myModel = null;
    try {
      Class myclass = Class.forName(className, true, Thread.currentThread().getContextClassLoader());
      myModel = myclass.newInstance();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    if (myModel == null) {
      myModel = new EGGrapheTreeModel();
    }

    if (myModel instanceof EGGrapheTreeModel) {
      return (EGGrapheTreeModel) myModel;
    }
    return null;

  }
  final Map<String, EGGroupPersistBuilder> cacheGroupBuilder = new HashMap<>();
  final Map<String, EGCourbePersistBuilder> cacheCurveBuilder = new HashMap<>();

  /**
   * Methode qui genere le eggraphe a partir des donn�es persistantes.
   *
   * @return
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ClassNotFoundException
   * @throws IllegalAccessException
   * @throws InstantiationException
   */
  @SuppressWarnings("unchecked")
  public EGFillePanel loadFillePanel(Map parameters) throws Exception {
    EGGraphe graphe = null;
    String mainfile = getMainFilePath();
    File fichierCourbes = getDirectoryCurves();
    XStream parser = getParser();
    XstreamCustomizer customizer = (XstreamCustomizer) parameters.get(XstreamCustomizer.PARAMETER_ID);
    if (customizer != null) {
      customizer.configureXstream(parser);
    }
    String graphePanelClass = null;
    EGGraphePersist.addEGAxeHorizontalPersistCompatibilityConfiguration(parser);
    if (fichierCourbes.isDirectory()) {
      // -- outputstream du xstream pour chargement des diff�rents groupes--//
      ObjectInputStream in = null;
      try {
        in = EbliLib.createObjectInpuStream(new File(mainfile), parser);
        // --lecture biddon --//
        graphePanelClass = (String) in.readObject();
        in.readObject();

        boolean doHorizontalRezoom = false;

        // - Etape 1: lecture de l'axe --//
        EGAxeHorizontalPersist axeHorizontalPersist = (EGAxeHorizontalPersist) in.readObject();

        if (axeHorizontalPersist.range == null) {
          doHorizontalRezoom = true;
        }

        EGAxeHorizontal axeX = axeHorizontalPersist.generateAxe();

        // - Etape 2: lecture du modele --//
        String modelName = (String) in.readObject();
        EGGrapheTreeModel model = generateModelInstance(modelName);
        model.setAxeX(axeX);

        // -- Etape 3: on ajoute tous nos amis les groupes --//
        int nbGroups = in.readInt();
        Map<Integer, EGGroup> groupIdx = new HashMap<>();
        Set<EGGroup> groupToRefresh = new HashSet<>();

        for (int i = 0; i < nbGroups; i++) {
          // -- lecture du groupe --//
          EGGroupPersist groupPersist = (EGGroupPersist) in.readObject();

          EGGroupPersistBuilder<EGGroup> builder = getGroupBuilder(groupPersist);
          Map groupParam = new HashMap();
          if (parameters != null) {
            groupParam.putAll(parameters);
          }
          EGGroup newGroup = builder == null ? null : builder.restoreObject(groupPersist, groupParam, log, null);
          if (newGroup != null) {
            groupIdx.put(groupPersist.getIdx(), newGroup);
            // ajout du groupe
            model.add(newGroup);
            if (!groupPersist.isRangeSet()) {
              groupToRefresh.add(newGroup);
            }
          }
        }
        List<EGMergeAxisPersist> mergeAxis = Collections.emptyList();

        // on essaie de lire si dispo: bizarre isAvailable n'est pas supporte.
        try {
          mergeAxis = (List<EGMergeAxisPersist>) in.readObject();
        } catch (EOFException eof) {
        } catch (Exception e) {
          FuLog.error(e);

        }

        // -- Etape 4: lecture des courbes --//
        // get the persitant list of curves:
        Map<Integer, EGCourbe> courbeById = new HashMap<>();
        List<EGCourbePersist> listeCourbesPersistantes = getPersitantCurvesList(false);
        List<EGCourbe> restoredCurves = new ArrayList<>();
        for (EGCourbePersist cbPersist : listeCourbesPersistantes) {
          EGGroup container = model.getGroup(cbPersist.Idgroup);

          if (container != null) {
            EGCourbePersistBuilder<? extends EGCourbeChild> builder = getCourbeBuilder(cbPersist);
            if (builder != null) {
              Map courbeParameters = new HashMap();
              if (parameters != null) {
                courbeParameters.putAll(parameters);
              }
              courbeParameters.put(EGCourbePersistBuilder.GROUPE_KEY, container);
              EGCourbeChild newCurve = builder.restoreObject(cbPersist, courbeParameters, log, getBinFileForCurve(cbPersist.getId()));
              if (newCurve == null) {
                continue;
              }
              if (cbPersist.getId() >= 0) {
                courbeById.put(cbPersist.getId(), newCurve);
              }
              restoredCurves.add(newCurve);
              container.addEGComponent(newCurve);
            } else {
              restoredCurves.add(null);
            }
          }
        }
        int nbCurves = restoredCurves.size();
        EGCourbeSurfacePersistBuilder builder = new EGCourbeSurfacePersistBuilder();
        for (int i = 0; i < nbCurves; i++) {
          if (listeCourbesPersistantes.get(i).surfacePainter != null && restoredCurves.get(i) != null) {
            builder.restore(listeCourbesPersistantes.get(i).surfacePainter, restoredCurves.get(i), courbeById);
          }
        }

        // -- on finalise les actions d'un graphe --//
        try {
          model.finalizePersistance();
        } catch (Exception e) {
          ((List<String>) parameters.get("errorMsg")).add("Erreur, la tentative de finalisation des donn�es du graphe a �chou� ");
          ((List<String>) parameters.get("errorMsg")).add("Cette erreur est peut �tre due � une incompatibilit� des variables pour un rejoue de donn�es.");

        }
        // -- Etape finale: creation du graphe --//
        graphe = new EGGraphe(model);
        graphe.transformer_.setXAxe(axeX);
        // ajustement des courbes en fonction du groupe 0
        for (EGGroup egGroup : groupToRefresh) {
          graphe.setYZoomAdaptedFor(egGroup);
        }
        if (doHorizontalRezoom) {
          graphe.restoreAxeX();
        }
        if (mergeAxis != null && mergeAxis.size() > 0) {
          for (EGMergeAxisPersist merge : mergeAxis) {
            int[] idxs = merge.getGroupeUsingTheAxis();
            List<EGAxeVertical> axeToMerge = new ArrayList<>();
            for (int i = 0; i < idxs.length; i++) {
              axeToMerge.add(groupIdx.get(Integer.valueOf(idxs[i])).getAxeY());
            }
            graphe.mergeAxis(axeToMerge.toArray(new EGAxeVertical[0]), merge.getPersist().generateAxe());

          }
        }
        // mise a jour des b�b�s
        model.fireStructureChanged();
      } catch (Exception e) {
        FuLog.error(e);
      } finally {
        CtuluLibFile.close(in);
      }
    }

    // -- on charge les donn�es specifiques en fonction du type de grapheModel --//
    ObjectInputStream in = null;
    try {
      // -- inputstream du xstream pour chargement des diff�rents groupes--//
      in = EbliLib.createObjectInpuStream(new File(getSpecifiqueDataFilePath()), parser);
      // -- lecture des donn�es specifiques --//
      Object specifiquesDatas = in.readObject();

      // -- remplissage du modele avec les donn�es persistantes --//
      graphe.getModel().setSpecificPersitDatas(specifiquesDatas, parameters);

    } catch (IOException e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(in);
    }
    if (!log.isEmpty()) {
      CtuluAnalyzeGUI.showDialogErrorFiltre(log, new CtuluUIDialog(CtuluLibSwing.getActiveWindow()), EbliLib.getS("Chargement du graphe"), false);
    }
    if (!CtuluLibString.isEmpty(graphePanelClass) && graphePanelClass.startsWith(PANEL)) {
      try {
        int indexOf = graphePanelClass.indexOf(':');
        String className = graphePanelClass.substring(indexOf + 1);
        if (!className.equals(EGFillePanel.class.getName())) {
          Class<?> forName = Class.forName(className);
          Constructor<?> constructor = forName.getConstructor(EGGraphe.class, CtuluUI.class);
          constructor.setAccessible(true);
          final CtuluUI ui = (CtuluUI) parameters.get("ui");
          if (constructor != null) {
            return (EGFillePanel) constructor.newInstance(graphe, ui);
          }
        }
      } catch (Exception e) {
        FuLog.error(e);
      }

    }

    return new EGFillePanel(graphe);
  }

  @SuppressWarnings("unchecked")
  private EGGroupPersistBuilder<EGGroup> getGroupBuilder(EGGroupPersist groupPersist) {
    String builderClass = groupPersist.builderClass;
    if (CtuluLibString.isEmpty(builderClass)) {
      builderClass = EGGroupPersistBuilderDefault.class.getName();
    }
    EGGroupPersistBuilder<EGGroup> builder = cacheGroupBuilder.get(builderClass);
    if (builder == null) {
      try {
        builder = (EGGroupPersistBuilder<EGGroup>) Class.forName(builderClass).newInstance();
        cacheGroupBuilder.put(builderClass, builder);
      } catch (Exception e) {
        FuLog.error(e);
        e.printStackTrace();
      }
    }
    if (builder == null) {
      log.addFatalError(EbliLib.getS("Le groupe {0} n'est pas reconnu, Il ne sera pas construit", builderClass));
    }
    return builder;
  }

  @SuppressWarnings("unchecked")
  private EGCourbePersistBuilder<? extends EGCourbeChild> getCourbeBuilder(EGCourbePersist courbePersist) {
    String builderClass = courbePersist.builderClass;
    if (CtuluLibString.isEmpty(builderClass)) {
      builderClass = EGCourbePersistBuilderDefault.class.getName();
    }
    EGCourbePersistBuilder<? extends EGCourbeChild> builder = cacheCurveBuilder.get(builderClass);
    if (builder == null) {
      try {
        builder = (EGCourbePersistBuilder<? extends EGCourbeChild>) Class.forName(builderClass).newInstance();
        cacheCurveBuilder.put(builderClass, builder);
      } catch (Exception e) {
        FuLog.error(e);
        e.printStackTrace();
      }
    }
    if (builder == null) {
      log.addFatalError(EbliLib.getS("Le groupe {0} n'est pas reconnu, Il ne sera pas construit", builderClass));
    }
    return builder;
  }

  public static void addEGAxeHorizontalPersistCompatibilityConfiguration(XStream parser) {
    parser.aliasAttribute(EGAxeHorizontalPersist.class, "range", "minMax");
  }
}
