/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author Frederic Deniger
 */
public class EGInteractionEditAxe extends EGInteractiveComponent implements MouseListener {

  private final EGActionAxeRepereConfigure configureAction;
  private final EGGraphe target;

  public EGInteractionEditAxe(final EGActionAxeRepereConfigure _a) {
    configureAction = _a;
    this.target = configureAction.getGraphe();
    this.target.addMouseListener(this);
  }

  @Override
  public String getDescription() {
    return "";
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (!configureAction.isEnabled()) {
      return;
    }
    if (e.getClickCount() >= 2) {
      final EGAxeHorizontal axeX = target.getModel().getAxeX();
      if (axeX.isInScreenBounds(e.getX(), e.getY())) {
        displayAxeH();
        return;
      }
      for (EGAxeVertical axeV : target.getAllAxeVertical()) {
        if (axeV.isInScreenBounds(e.getX(), e.getY())) {
          displayAxeV(axeV);
          return;
        }
      }

    }
  }

  protected void displayAxeH() {
    configureAction.buildContentPane();
    configureAction.actionPerformed(null);
  }

  protected void displayAxeV(EGAxeVertical v) {
    configureAction.buildContentPane();
    configureAction.showOnTargetAxis(new HashSet(Collections.singleton(v)));

  }

  @Override
  public void mousePressed(MouseEvent e) {
  }

  @Override
  public void mouseReleased(MouseEvent e) {
  }

  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }

}
