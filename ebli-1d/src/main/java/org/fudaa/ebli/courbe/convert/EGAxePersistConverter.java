/*
 GPL 2
 */
package org.fudaa.ebli.courbe.convert;

import com.thoughtworks.xstream.XStream;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.EGAxeHorizontalPersist;
import org.fudaa.ebli.courbe.EGAxeVerticalPersist;

/**
 *
 * @author Frederic Deniger
 */
public class EGAxePersistConverter {

  final TraceToStringConverter traceToStringConverter;

  public EGAxePersistConverter(TraceToStringConverter traceToStringConverter) {
    this.traceToStringConverter = traceToStringConverter;
  }

  public EGAxePersistConverter() {
    this(new TraceToStringConverter());
  }
  XStream xstream;

  public XStream getXstream() {
    return xstream;
  }

  public void setXstream(XStream xstream) {
    this.xstream = xstream;
  }

  public void init() {
    if (xstream == null) {
      xstream = EGPersistHelper.createXstream();
      initXstream(xstream);
    }
  }

  public void initXstream(XStream xstream) {
    EGPersistHelper.registerDefaultConverters(xstream, traceToStringConverter);
    xstream.processAnnotations(EGAxeHorizontalPersist.class);
    xstream.processAnnotations(EGAxeVerticalPersist.class);
    xstream.alias("range", CtuluRange.class);
  }

  public String toXml(EGAxeHorizontalPersist in) {
    if (xstream == null) {
      init();
    }
    return xstream.toXML(in);
  }

  public String toXml(EGAxeVerticalPersist in) {
    if (xstream == null) {
      init();
    }
    return xstream.toXML(in);
  }

  public EGAxeVerticalPersist fromXmlVertical(String in) {
    if (xstream == null) {
      init();
    }
    return (EGAxeVerticalPersist) xstream.fromXML(in);
  }

  public EGAxeHorizontalPersist fromXmlHorizontal(String in) {
    if (xstream == null) {
      init();
    }
    return (EGAxeHorizontalPersist) xstream.fromXML(in);
  }
}
