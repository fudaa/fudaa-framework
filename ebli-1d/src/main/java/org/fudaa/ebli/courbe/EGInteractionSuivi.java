/*
 * @creation 23 juin 2004
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import gnu.trove.TDoubleArrayList;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingConstants;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIcon;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionSuivi.java,v 1.13 2006-09-19 14:55:53 deniger Exp $
 */
public class EGInteractionSuivi extends EGInteractiveComponent implements MouseListener, MouseMotionListener {

  boolean dessine_;
  TraceIcon selected_;
  TraceIcon normal_;
  final EGGraphe target_;
  TraceBox tb_;
  Color traceColor_;
  EGAxeVertical v_;
  double xreel_;

  @Override
  public String getDescription() {
    return EbliLib.getS("suivi");
  }
  double[] yreel_;
  int selectedPtIdx_;

  /**
   * @param _a la cible pour le suivi.
   */
  public EGInteractionSuivi(final EGGraphe _a) {
    target_ = _a;
    target_.addMouseListener(this);
    target_.addMouseMotionListener(this);
  }
  TDoubleArrayList yPressed_;

  private void updateWithMouse(final int _x, final int _y) {
    if (!isActive()) {
      return;
    }
    final EGCourbe ci = target_.getSelectedComponent();
    if (ci == null) {
      dessine_ = false;
      v_ = null;
    } else {
      xreel_ = target_.getTransformer().getXReel(_x);
      if (target_.getTransformer().getXAxe().isDiscret()) {
        xreel_ = (int) xreel_;
      }
      selectedPtIdx_ = ci.select(_x, _y, target_.getTransformer(), 5);
      // on dessine si un point selectionne ou si le point appartient a la courbe
      dessine_ = selectedPtIdx_ >= 0 || ci.containsX(xreel_);
      if (dessine_) {
        v_ = ci.getAxeY();
        if (selectedPtIdx_ >= 0) {
          xreel_ = ci.getModel().getX(selectedPtIdx_);
          yreel_ = new double[]{ci.getModel().getY(selectedPtIdx_)};
        } else {
          TDoubleArrayList interpol = ci.interpolPoints(xreel_, target_.getTransformer());
          yreel_ = interpol.toNativeArray();
          if (target_.getTransformer().getXAxe().isLogarithmique()) {
            yPressed_ = ci.interpolPointsOnEcran(xreel_, target_.getTransformer());
          }
        }

      } else {
        v_ = null;
      }
    }
    repaint();
  }

  private boolean isExactPtSelect() {
    return selectedPtIdx_ >= 0;
  }

  @Override
  protected void paintComponent(final Graphics _g) {
    if (isActive() && dessine_ && (v_ != null)) {
      final Color old = _g.getColor();
      if (tb_ == null) {
        tb_ = new TraceBox();
      }
      final EGRepere r = target_.getTransformer();
      // si l'axe des x demande des points fixes (entier) et pas de point exact
      // selectionne on ne fait rien.
      if (r.getXAxe().isDiscret() && !isExactPtSelect()) {
        return;
      }
      final int x = r.getXEcran(xreel_);
      for (int i = 0; i < yreel_.length; i++) {
        final int y = r.getYEcran(yreel_[i], v_);
        if (isExactPtSelect()) {
          getSelectedIcone().paintIconCentre(_g, x, y);
        } else {
          getNormalIcone().paintIconCentre(_g, x, y);
        }
        boolean logAndEcart = false;
        if (r.getXAxe().isLogarithmique() && Math.abs(yPressed_.get(i) - y) > 3) {
          logAndEcart = true;
          getNormalIcone().paintIconCentre(_g, x, yPressed_.get(i));
        }
        _g.setColor(getTraceColor());
        final int mx = r.getMaxEcranY() + 2;
        _g.drawLine(x, r.getMinEcranY(), x, mx);

        final int xmin = v_.isDroite() ? r.getMinEcranX() : r.getMinEcranX() - target_.getOffSet(v_) - 3;
        final int xmax = v_.isDroite() ? r.getMaxEcranX() + target_.getOffSet(v_) + 3 : r.getMaxEcranX();
        _g.drawLine(xmin, y, xmax, y);
        if (logAndEcart) {
          _g.drawLine(xmin, (int) yPressed_.get(i), xmax, (int) yPressed_.get(i));
        }
        tb_.setColorText(r.getXAxe().getLineColor());
        tb_.getTraceLigne().setCouleur(r.getXAxe().getLineColor());
        tb_.setHPosition(SwingConstants.CENTER);
        tb_.setVPosition(SwingConstants.TOP);
        final String s = r.getXAxe().getStringInterpolatedAffiche(xreel_, true);
        tb_.paintBox((Graphics2D) _g, x, mx, s);
        tb_.setColorText(v_.getLineColor());
        tb_.getTraceLigne().setCouleur(v_.getLineColor());
        tb_.setHPosition(v_.isDroite() ? SwingConstants.LEFT : SwingConstants.RIGHT);
        tb_.setVPosition(SwingConstants.CENTER);
        tb_.paintBox((Graphics2D) _g, xmax, y, v_.getStringInterpolatedAffiche(yreel_[i], true));
        if (logAndEcart) {
          tb_.paintBox((Graphics2D) _g, x + 5, (int) yPressed_.get(i), v_.getStringInterpolatedAffiche(r.getYReel(
                  (int) yPressed_.get(i), v_), true));
        }
      }
      _g.setColor(old);

    }
  }

  public Color getTraceColor() {
    if (traceColor_ == null) {
      traceColor_ = Color.lightGray;
    }
    return traceColor_;
  }

  public TraceIcon getSelectedIcone() {
    if (selected_ == null) {
      selected_ = new TraceIcon(TraceIcon.CARRE_PLEIN, 4);
      selected_.setCouleur(Color.gray);
    }
    return selected_;
  }

  public TraceIcon getNormalIcone() {
    if (normal_ == null) {
      normal_ = new TraceIcon(TraceIcon.CROIX, 4);
      normal_.setCouleur(Color.gray);
    }
    return normal_;
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    if (!_e.isPopupTrigger()) {
      updateWithMouse(_e.getX(), _e.getY());
    }
    if (_e.getClickCount() == 2) {
      // dessine_ = !dessine_;
      dessine_ ^= true;
    }
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    dessine_ = true;
    updateWithMouse(_e.getX(), _e.getY());
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
  }

  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    if (!_b) {
      v_ = null;
      if (target_ != null) {
        target_.repaint();
      }
    }
  }
}
