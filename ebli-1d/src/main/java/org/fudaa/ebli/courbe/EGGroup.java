/*
 * @creation 21 juin 2004
 *
 * @modification $Date: 2007-05-04 13:49:41 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurTextField;
import org.fudaa.ebli.palette.BPaletteInfo;

import javax.swing.tree.TreeNode;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: EGGroup.java,v 1.24 2007-05-04 13:49:41 deniger Exp $
 */
public class EGGroup extends EGObject implements TreeNode, EGParent {
  private final List comp_;
  private String title_;
  private EGAxeVertical y_;
  protected EGParent.TreeNodes n_;
  /**
   * g�re le trac�  des courbes  et les couleurs par d�faut.  instancier la variable statique pour l'activer.
   */
  public static PaletteCourbes paletteTraceAuto;
  private int idTraceGroup = 0;

  public EGGroup() {
    super();
    comp_ = new ArrayList(10);
    idTraceGroup = updateDifferentTraceGroups(1);
  }

  public EGGroup(final int idGroup) {
    super();
    comp_ = new ArrayList(10);
    this.idTraceGroup = idGroup;
  }

  @Override
  public boolean isDisplayed() {
    if (getEgParent() == null) {
      return isVisible_;
    }
    return isVisible() && getEgParent().isDisplayed();
  }

  // public EGGroup(List l) {
  // super();
  // comp_ = l;
  // }
  public static int updateDifferentTraceGroups(int value) {
    return PaletteCourbes.updateTotalGroups(value);
  }

  public int getIdGroup() {
    return idTraceGroup;
  }

  public void setIdGroup(int idGroup) {
    this.idTraceGroup = idGroup;
  }

  public EGParent.TreeNodes getEgParent() {
    return n_;
  }

  protected EGGroupPersistBuilder<EGGroup> createPersistBuilder() {
    return new EGGroupPersistBuilderDefault();
  }

  public void setEgParent(EGParent.TreeNodes n) {
    n_ = n;
  }

  @Override
  void fillWithInfo(final BPaletteInfo.InfoData _table, final CtuluListSelectionInterface _selectedPt) {
    _table.setTitle(EbliLib.getS("Groupe:") + CtuluLibString.ESPACE + getTitle());
    _table.put(EbliLib.getS("Nombre de courbes"), CtuluLibString.getString(getChildCount()));
    CtuluRange r = getXRange();
    _table.put(EbliLib.getS("X min"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(r.min_));
    _table.put(EbliLib.getS("X max"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(r.max_));
    r = getYRange();
    _table.put(EbliLib.getS("Y min"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(r.min_));
    _table.put(EbliLib.getS("Y max"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(r.max_));
    final String pre = EbliLib.getS("Axe vertical:") + CtuluLibString.ESPACE;
    final EGAxeVertical axe = getAxeY();
    _table.put(pre + EbliLib.getS("Borne min"), axe.getStringAffiche(axe.getMinimum()));
    _table.put(pre + EbliLib.getS("Borne max"), axe.getStringAffiche(axe.getMaximum()));
  }

  @Override
  public boolean canDescendre(final EGObject _o) {
    if (comp_.size() <= 1) {
      return false;
    }
    final int idx = comp_.indexOf(_o);
    return idx >= 0 && idx < comp_.size() - 1;
  }

  @Override
  public boolean canMonter(final EGObject _o) {
    if (comp_.size() <= 1) {
      return false;
    }
    return comp_.indexOf(_o) > 0;
  }

  protected EGCourbeChild getCourbe(final int _i) {
    return (EGCourbeChild) comp_.get(_i);
  }

  /**
   * @param _i la courbe a ajouter
   */
  public void addEGComponent(final EGCourbeChild _i) {
    addEGComponent(_i, -1);
  }

  /**
   * @param _i la courbe a ajouter
   * @param _idx l'indice d'ajout
   */
  public void addEGComponent(final EGCourbeChild _i, final int _idx) {
    addEGComponent(_i, _idx, true);
  }

  public void addEGComponent(final EGCourbeChild _i, final int _idx, boolean setAutoTraceLigne) {
    if (comp_.contains(_i)) {
      return;
    }
    if (_i.getParentGroup() != null) {
      _i.getParentGroup().removeEGComponent(_i);
    }
    if ((_idx < 0) || (_idx >= comp_.size())) {
      comp_.add(_i);
    } else {
      comp_.add(_idx, _i);
    }
    // comp_.add(_i);
    _i.setParentGroup(this);
    if (setAutoTraceLigne) {
      setTraceLigne(_i);
    }
  }

  private void setTraceLigne(EGCourbeChild i) {
    int indiceCourbe = comp_.size() - 1;
    setTraceLigne(i, indiceCourbe);
  }

  private void setTraceLigne(EGCourbeChild i, int indiceCourbe) {
    if (i != null && paletteTraceAuto != null) {
      i.setLigneModel(paletteTraceAuto.getTraceLigneModelForGroupAndCurve(idTraceGroup, indiceCourbe));
      i.setAspectContour(i.getLigneModel().getCouleur());
    }
  }

  public void traceAllCourbeFromPalette(final int idGroup) {
    setIdGroup(idGroup);
    int cpt = 0;
    for (Object o : comp_) {
      if (o instanceof EGCourbeChild) {
        setTraceLigne((EGCourbeChild) o, cpt++);
      }
    }
  }

  public void addEGComponent(final List _l) {
    for (final Iterator it = _l.iterator(); it.hasNext(); ) {
      final EGCourbeChild i = (EGCourbeChild) it.next();
      if (!comp_.contains(i)) {
        comp_.add(i);
        i.setParentGroup(this);
        setTraceLigne(i);
      }
    }
  }

  @Override
  public boolean ajuste(final CtuluRange _x, final CtuluRange _y) {
    if (comp_.size() == 0) {
      return false;
    }
    boolean r = false;
    for (final Iterator it = comp_.iterator(); it.hasNext(); ) {
      final EGCourbeChild c = (EGCourbeChild) it.next();
      if (c.isVisible_) {
        r |= c.ajuste(_x, _y);
      }
    }
    return r;
  }

  @Override
  public boolean ajusteX(final CtuluRange _x) {
    if (comp_.size() == 0) {
      return false;
    }
    boolean r = false;
    for (final Iterator it = comp_.iterator(); it.hasNext(); ) {
      final EGCourbeChild c = (EGCourbeChild) it.next();
      if (c.isVisible_) {
        r |= c.ajusteX(_x);
      }
    }
    return r;
  }

  @Override
  public boolean ajusteY(final CtuluRange _y) {
    if (comp_.size() == 0) {
      return false;
    }
    boolean r = false;
    for (final Iterator it = comp_.iterator(); it.hasNext(); ) {
      final EGCourbeChild c = (EGCourbeChild) it.next();
      if (c.isVisible_) {
        r |= c.ajusteY(_y);
      }
    }
    return r;
  }

  @Override
  public Enumeration children() {
    return Collections.enumeration(comp_);
  }

  public List getChildren() {
    return comp_;
  }

  public boolean contains(final EGCourbe _i) {
    return comp_.contains(_i);
  }

  @Override
  public void descendre() {
    getEgParent().descendre(this);
  }

  @Override
  public void descendre(final EGObject _o) {
    final int i = comp_.indexOf(_o);
    if (i >= 0 && i < comp_.size() - 1) {
      comp_.remove(i);
      if (i == comp_.size() - 2) {
        comp_.add(_o);
      } else {
        comp_.add(i + 1, _o);
      }
      getEgParent().fireStructureChanged(this, (EGCourbeChild) _o);
    }
  }

  @Override
  public void dessine(final Graphics2D _g, final EGRepere _f) {
    if (!isVisible_ || !isAtLeastOneObjectVisible()) {
      return;
    }
    final Color old = _g.getColor();
    _g.setColor(Color.black);
    final int nbElement = this.comp_.size();
    for (int i = 0; i < nbElement; i++) {
      final EGCourbeChild o = (EGCourbeChild) comp_.get(i);
      o.dessine(_g, _f);
    }
    _g.setColor(old);
  }

  @Override
  public void enDernier() {
    getEgParent().enDernier(this);
  }

  @Override
  public void enDernier(final EGObject _o) {
    final int i = comp_.indexOf(_o);
    if (i >= 0 && i < comp_.size() - 1) {
      comp_.remove(i);
      comp_.add(_o);
      getEgParent().fireStructureChanged(this, (EGCourbeChild) _o);
    }
  }

  @Override
  public void enPremier() {
    getEgParent().enPremier(this);
  }

  @Override
  public void enPremier(final EGObject _o) {
    final int i = comp_.indexOf(_o);
    if (i > 0) {
      comp_.remove(i);
      comp_.add(0, _o);
      getEgParent().fireStructureChanged(this, (EGCourbeChild) _o);
    }
  }

  /**
   * @param _dest la liste a remplir avec les courbes de ce groupe.
   */
  @Override
  public void fillWithCurves(final List _dest) {
    final int nb = comp_.size();
    for (int i = 0; i < nb; i++) {
      _dest.add(getCourbe(i));
    }
  }

  @Override
  public void fireAxeAspectChanged(final EGAxe _a) {
    if (n_ != null) {
      getEgParent().fireAxeAspectChanged(_a);
    }
  }

  @Override
  public void fireCourbeAspectChanged(final boolean _visibility) {
    fireCourbeAspectChanged(this, _visibility);
  }

  @Override
  public void fireCourbeAspectChanged(final EGObject _o, final boolean _visibility) {
    if (n_ != null) {
      getEgParent().fireCourbeAspectChanged(_o, _visibility);
    }
  }

  @Override
  public void fireCourbeContentChanged() {
    fireCourbeContentChanged(this, false);
  }

  @Override
  public void fireCourbeContentChanged(final EGObject _o, final boolean _mustRestore) {
    if (n_ != null) {
      getEgParent().fireCourbeContentChanged(_o, _mustRestore);
    }
  }

  @Override
  public boolean getAllowsChildren() {
    return true;
  }

  @Override
  public EGAxeHorizontal getAxeX() {
    return getEgParent().getAxeX();
  }

  @Override
  public EGAxeVertical getAxeY() {
    return y_;
  }

  @Override
  public TreeNode getChildAt(final int _childIndex) {
    return (TreeNode) comp_.get(_childIndex);
  }

  @Override
  public int getChildCount() {
    return (comp_ == null ? 0 : comp_.size());
  }

  @Override
  public final BConfigurableInterface[] getConfigureInterfaces() {
    final BConfigurableInterface[] res = new BConfigurableInterface[getChildCount()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getCourbeAt(i).getSingleConfigureInterface();
    }
    return res;
  }

  public EGCourbeChild getCourbeAt(final int _i) {
    return (EGCourbeChild) comp_.get(_i);
  }

  public EGObject[] getEGChilds() {
    return (EGObject[]) comp_.toArray(new EGObject[0]);
  }

  @Override
  public int getIconHeight() {
    return 24;
  }

  @Override
  public int getIconWidth() {
    return 24;
  }

  @Override
  public int getIndex(final TreeNode _node) {
    return comp_.indexOf(_node);
  }

  @Override
  public EGGrapheModel getMainModel() {
    return getEgParent() == null ? null : getEgParent().getMainModel();
  }

  @Override
  public int getMargeGaucheNeeded(final Graphics2D _g) {
    return y_ == null ? 0 : y_.getWidthNeeded(_g);
  }

  @Override
  public int getMargeHautNeeded(final Graphics2D _g) {
    return y_ == null ? 0 : y_.getHeightNeeded(_g);
  }

  @Override
  public TreeNode getParent() {
    return n_;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public CtuluRange getXRange() {
    final CtuluRange x = new CtuluRange();
    x.min_ = Double.MAX_VALUE;
    x.max_ = -Double.MAX_VALUE;
    if (ajusteX(x)) {
      if (Double.isNaN(x.min_)) {
        x.min_ = 0;
      }
      if (Double.isNaN(x.max_)) {
        x.max_ = 0;
        /*
         * if (x.min == x.max) { if (x.min != 0) { x.min = x.min 0.9; x.max = x.max 1.1; } else { x.max = 1; } }
         */
      }
    } else {
      x.min_ = 0;
      x.max_ = 1;
    }
    return x;
  }

  @Override
  public CtuluRange getYRange() {
    final CtuluRange x = new CtuluRange();
    x.min_ = Double.MAX_VALUE;
    x.max_ = -Double.MAX_VALUE;
    if (!ajusteY(x)) {
      x.max_ = 0;
      x.min_ = 0;
      return x;
    }
    if (Double.isNaN(x.min_)) {
      x.min_ = 0;
    }
    if (Double.isNaN(x.max_)) {
      x.max_ = 0;
    }
    /*
     * if (x.min == x.max) { if (x.min != 0) { x.min = x.min 0.9; x.max = x.max 1.1; } else { x.max = 1; } }
     */
    return x;
  }

  public boolean isAtLeastOneObjectVisible() {
    for (int i = getChildCount() - 1; i >= 0; i--) {
      if (getCourbe(i).isVisible_) {
        return true;
      }
    }
    return false;
  }

  @Override
  public final boolean isCourbe() {
    return false;
  }

  @Override
  public boolean isLeaf() {
    return false;
  }

  @Override
  public boolean isSomethingToDisplay() {
    return isAtLeastOneObjectVisible();
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public void monter() {
    getEgParent().monter(this);
  }

  @Override
  public void monter(final EGObject _o) {
    final int i = comp_.indexOf(_o);
    if (i > 0) {
      comp_.remove(i);
      comp_.add(i - 1, _o);
      getEgParent().fireStructureChanged(this, (EGCourbeChild) _o);
    }
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    final int w = getIconWidth();
    final int h = getIconHeight();
    for (int i = 0; i <= 4; i += 2) {
      _g.setColor(Color.white);
      _g.fillRect(_x + i + 1, _y + i + 1, w - 6, h - 6);
      _g.setColor(isVisible_ ? Color.black : Color.lightGray);
      _g.drawRect(_x + i, _y + i, w - 4, h - 4);
    }
  }

  public void removeAll() {
    for (int i = comp_.size() - 1; i >= 0; i--) {
      final EGCourbeChild child = (EGCourbeChild) comp_.get(i);
      child.setParentGroup(null);
    }
    comp_.clear();
  }

  /**
   * @param _i la courbe a enelever
   */
  public void removeEGComponent(final EGCourbeChild _i) {
    if (!comp_.contains(_i)) {
      return;
    }
    comp_.remove(_i);
    _i.setParentGroup(null);
  }

  public void removeEGComponent(final List _list) {
    for (final Iterator it = _list.iterator(); it.hasNext(); ) {
      final EGCourbeChild i = (EGCourbeChild) it.next();
      if (!comp_.contains(i)) {
        return;
      }
      comp_.remove(i);
      i.setParentGroup(null);
    }
  }

  public void setAxeY(final EGAxeVertical _y) {
    y_ = _y;
    if (y_.font_ == null) {
      y_.font_ = EGGraphe.DEFAULT_FONT;
    }
  }

  public void setParent(final EGParent.TreeNodes _n) {
    n_ = _n;
  }

  @Override
  public boolean setTitle(final String _title) {
    if (!CtuluLibString.isEquals(_title, title_)) {
      title_ = _title;
      fireCourbeContentChanged();
      firePropertyChange(BSelecteurTextField.TITLE_PROPERTY, null, title_);
      return true;
    }
    return false;
  }

  @Override
  public boolean setYaxe(final EGAxeVertical _y) {
    if (_y != y_) {
      y_ = _y;
      return true;
    }
    return false;
  }

  public void sortMembers() {
    Collections.sort(comp_, new EGObjectComparator());
  }

  @Override
  public String toString() {
    return EbliLib.getS("Groupe") + CtuluLibString.ESPACE + title_;
  }

  public EGGroup duplicate(EGGrapheDuplicator _duplicator) {

    // -- duplication de l objet --//
    EGGroup duplic1 = new EGGroup(this.idTraceGroup);
    duplic1.isVisible_ = this.isVisible_;
    duplic1.title_ = this.title_;
    duplic1.n_ = this.n_;
    duplic1.y_ = this.y_.duplicate();
    EGGroup duplic = duplic1;
    for (Iterator it = this.comp_.iterator(); it.hasNext(); ) {
      Object item = it.next();
      if (item instanceof EGCourbeChild) {
        duplic.addEGComponent((EGCourbeChild) ((EGCourbeChild) item).duplicate(duplic, _duplicator), -1, false);
      }
    }

    return duplic;
  }
}
