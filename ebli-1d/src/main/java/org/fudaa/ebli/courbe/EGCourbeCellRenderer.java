/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

/**
 * Renderer qui affiche la courbe sous forme de sa courbe miniature et son libell� � cot�
 *
 * @author Adrien Hadoux
 */
@SuppressWarnings(value = "serial")
public class EGCourbeCellRenderer extends CtuluCellTextRenderer {
  // ne pas refaire le travail !!!!!!!
  private final EGIconForCourbe icon_ = new EGIconForCourbe();

  public EGCourbeCellRenderer() {
  }

  @Override
  protected void setValue(final Object _value) {
    final EGCourbe cb = (EGCourbe) _value;
    icon_.updateFromCourbe(cb);
    setIcon(icon_);
    setText(cb.getTitle());
    setOpaque(true);
  }
  
}
