package org.fudaa.ebli.courbe;

import java.util.HashMap;
import java.util.Map;

public class EGPersist {

  protected String builderClass = EGGroupPersistBuilder.class.getName();
  HashMap<String, Object> specificValues;

  public EGPersist() {
    super();
  }

  public EGPersist(EGPersist other) {
    if (other != null) {
      if (other.specificValues != null) {
        specificValues = (HashMap<String, Object>) other.specificValues.clone();
      }
    }
  }


  public String getSpecificStringValue(String key) {
    return (String) getSpecificValue(key);
  }

  public Object getSpecificValue(String key) {
    return specificValues == null ? null : specificValues.get(key);

  }

  public void saveSpecificData(String key, Object value) {
    if (specificValues == null) {
      specificValues = new HashMap<>();
    }
    specificValues.put(key, value);

  }

}
