package org.fudaa.ebli.courbe;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Color;
import java.awt.Font;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Classe persistante des axe horizontaux
 *
 * @author Adrien Hadoux
 */
@XStreamAlias("HorizontalAxis")
public class EGAxeHorizontalPersist {

  @XStreamAlias("Title")
  String titre;
  @XStreamAlias("VisibleTitle")
  boolean titreVisible = true;
  @XStreamAlias("Unit")
  String unite;
  @XStreamAlias("Grid")
  TraceLigne grille;
  @XStreamAlias("Visible")
  boolean visible;
  @XStreamAlias("Font")
  Font font;
  @XStreamAlias("Graduations")
  boolean graduations;
  @XStreamAlias("IsIteratorUptodate")
  boolean isIteratorUptodate;
  @XStreamAlias("LineColor")
  Color lineColor;
  @XStreamAlias("AxisRange")
  CtuluRange range;
  /**
   * Booleen qui indique si l'on trace ou non la graduation
   */
  @XStreamAlias("GridPainted")
  boolean traceGrille_ = false;
  /**
   * Booleen qui indique si l'on trcae
   */
  @XStreamAlias("MinorGridPainted")
  boolean traceSousGrille_ = false;
  @XStreamAlias("MarkLine")
  TraceLigneModel traceGraduations_ = null;
  @XStreamAlias("MinorMarkLine")
  TraceLigneModel traceSousGraduations_ = null;
  /**
   * Mode nb graduations
   */
  @XStreamAlias("Step")
  int nbPas_;
  /**
   * Mode on donne la longueur des pas
   */
  @XStreamAlias("StepLength")
  double longueurPas_;
  /**
   * le mode de graduation
   */
  @XStreamAlias("ModeGraduation")
  int modeGraduations_;
  @XStreamAlias("SizeMinorGraduation")
  int nbSousGraduations_;
  @XStreamAlias("ExtremDisplayed")
  private boolean isExtremiteDessinee_;

  public EGAxeHorizontalPersist(final EGAxeHorizontal axeX) {
    fillInfoWith(axeX);
  }

  public EGAxeHorizontalPersist() {
  }

  private void fillInfoWith(final EGAxeHorizontal axeX) {
    this.titre = axeX.titre_;
    this.titreVisible = axeX.titreVisible_;
    this.unite = axeX.unite_;
    this.visible = axeX.visible_;
    this.font = axeX.font_;
    this.graduations = axeX.graduations_;
    this.isExtremiteDessinee_ = axeX.isExtremiteDessinee_;
    this.isIteratorUptodate = axeX.isIteratorUptodate_;
    this.lineColor = axeX.lineColor_;

    // -- persistance grilles et sous grilles --//
    traceGrille_ = axeX.traceGrille_;
    traceSousGrille_ = axeX.traceSousGrille_;
    traceGraduations_ = axeX.traceGraduations_;

    traceSousGraduations_ = axeX.traceSousGraduations_;
    nbPas_ = axeX.nbPas_;
    longueurPas_ = axeX.longueurPas_;
    modeGraduations_ = axeX.modeGraduations_;
    nbSousGraduations_ = axeX.nbSousGraduations_;
    range = axeX.range_ == null ? null : new CtuluRange(axeX.range_);
  }

  public EGAxeHorizontal generateAxe() {
    final EGAxeHorizontal axeX = new EGAxeHorizontal();
    apply(axeX);
    return axeX;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public boolean isTitreVisible() {
    return titreVisible;
  }

  public void setTitreVisible(boolean titreVisible) {
    this.titreVisible = titreVisible;
  }

  public String getUnite() {
    return unite;
  }

  public void setUnite(String unite) {
    this.unite = unite;
  }

  public TraceLigne getGrille() {
    return grille;
  }

  public void setGrille(TraceLigne grille) {
    this.grille = grille;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public Font getFont() {
    return font;
  }

  public void setFont(Font font) {
    this.font = font;
  }

  public boolean isGraduations() {
    return graduations;
  }

  public void setGraduations(boolean graduations) {
    this.graduations = graduations;
  }

  public boolean isIsIteratorUptodate() {
    return isIteratorUptodate;
  }

  public void setIsIteratorUptodate(boolean isIteratorUptodate) {
    this.isIteratorUptodate = isIteratorUptodate;
  }

  public Color getLineColor() {
    return lineColor;
  }

  public void setLineColor(Color lineColor) {
    this.lineColor = lineColor;
  }

  public CtuluRange getRange() {
    return range;
  }

  public void setRange(CtuluRange range) {
    this.range = range;
  }

  public boolean isTraceGrille() {
    return traceGrille_;
  }

  public void setTraceGrille(boolean traceGrille_) {
    this.traceGrille_ = traceGrille_;
  }

  public boolean isTraceSousGrille() {
    return traceSousGrille_;
  }

  public void setTraceSousGrille(boolean traceSousGrille_) {
    this.traceSousGrille_ = traceSousGrille_;
  }

  public TraceLigneModel getTraceGraduations() {
    return traceGraduations_;
  }

  public void setTraceGraduations(TraceLigneModel traceGraduations_) {
    this.traceGraduations_ = traceGraduations_;
  }

  public TraceLigneModel getTraceSousGraduations() {
    return traceSousGraduations_;
  }

  public void setTraceSousGraduations(TraceLigneModel traceSousGraduations_) {
    this.traceSousGraduations_ = traceSousGraduations_;
  }

  public int getNbPas() {
    return nbPas_;
  }

  public void setNbPas(int nbPas_) {
    this.nbPas_ = nbPas_;
  }

  public double getLongueurPas() {
    return longueurPas_;
  }

  public void setLongueurPas(double longueurPas_) {
    this.longueurPas_ = longueurPas_;
  }

  public int getModeGraduations() {
    return modeGraduations_;
  }

  public void setModeGraduations(int modeGraduations_) {
    this.modeGraduations_ = modeGraduations_;
  }

  public int getNbSousGraduations() {
    return nbSousGraduations_;
  }

  public void setNbSousGraduations(int nbSousGraduations_) {
    this.nbSousGraduations_ = nbSousGraduations_;
  }

  public boolean isIsExtremiteDessinee() {
    return isExtremiteDessinee_;
  }

  public void setIsExtremiteDessinee(boolean isExtremiteDessinee_) {
    this.isExtremiteDessinee_ = isExtremiteDessinee_;
  }

  public void apply(final EGAxeHorizontal axeX) {
    applyButRange(axeX);
    if (range != null) {
      axeX.range_.initWith(range);
    }
  }

  /**
   * N'applique pas la modification du Range.
   *
   * @param axeX
   */
  public void applyButRange(final EGAxeHorizontal axeX) {
    axeX.titre_ = this.titre;
    axeX.titreVisible_ = this.titreVisible;
    axeX.unite_ = this.unite;
    axeX.visible_ = this.visible;
    axeX.font_ = this.font;
    axeX.graduations_ = this.graduations;
    axeX.isExtremiteDessinee_ = this.isExtremiteDessinee_;
    axeX.isIteratorUptodate_ = this.isIteratorUptodate;
    axeX.lineColor_ = this.lineColor;

    // -- persistance grilles et sous grilles --//
    axeX.traceGrille_ = traceGrille_;
    axeX.traceSousGrille_ = traceSousGrille_;
    axeX.traceGraduations_ = traceGraduations_;
    if (traceGraduations_ == null && grille != null) {
      axeX.traceSousGraduations_ = grille.getModel();
    }
    axeX.traceSousGraduations_ = traceSousGraduations_;
    axeX.nbPas_ = nbPas_;
    axeX.longueurPas_ = longueurPas_;
    axeX.modeGraduations_ = modeGraduations_;
    axeX.nbSousGraduations_ = nbSousGraduations_;
    axeX.initDisplayValues();
  }

  public EGAxeHorizontalPersist copy() {
    EGAxeHorizontalPersist copy = new EGAxeHorizontalPersist();
    copy.titre=titre;
    copy.titreVisible=titreVisible;
    copy.unite=unite;
    copy.visible=visible;
    copy.font=font;
    copy.graduations=graduations;
    copy.grille=grille==null?null:new TraceLigne(grille);
    copy.isIteratorUptodate=isIteratorUptodate;
    copy.lineColor=lineColor;
    copy.range=range==null?null:new CtuluRange(range);
    copy.traceGrille_=traceGrille_;
    copy.traceSousGrille_=traceSousGrille_;
    copy.traceGraduations_=traceGraduations_==null?null:new TraceLigneModel(traceGraduations_);
    copy.traceSousGraduations_=traceSousGraduations_==null?null:new TraceLigneModel(traceSousGraduations_);
    copy.nbPas_=nbPas_;
    copy.longueurPas_=longueurPas_;
    copy.modeGraduations_=modeGraduations_;
    copy.nbSousGraduations_=nbSousGraduations_;
    copy.isExtremiteDessinee_=isExtremiteDessinee_;
    return copy;
  }

}
