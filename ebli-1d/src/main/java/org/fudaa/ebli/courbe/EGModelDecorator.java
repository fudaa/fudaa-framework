package org.fudaa.ebli.courbe;


/**
 * Decorator pour le EGModel,
 * permet de g�rer sans trop de d�gat des egmodel aux comportements diff�rents.
 * Utilise le design pattern decorator.
 * @author Adrien Hadoux
 *
 */
public abstract class EGModelDecorator implements EGModel{

	/**
	 * l'instance du modele correct avant inversion.
	 *  C'est � partir  de ce modele que va se produire l'inversion.
	 */
	protected final EGModel instanceModele_;
	
	
	
	public EGModelDecorator(final EGModel modeleInitial){
		
		instanceModele_=modeleInitial;
	}
	
	/**
	 * retourme le egmodel initial
	 * @return
	 */
	public  EGModel getModeleInitial(){
		return instanceModele_;
	}
	
	@Override
  public int[] getInitRows() {
		return null;
	}
	
}
