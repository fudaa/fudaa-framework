/*
 * @creation 6 juil. 2004
 * @modification $Date: 2006-12-05 10:14:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EGFillePanelDialog.java,v 1.12 2006-12-05 10:14:34 deniger Exp $
 */
public class EGFillePanelDialog extends EGFillePanel {

  /**
   * @param _a le graphe correspondant
   */
  public EGFillePanelDialog(final EGGraphe _a) {
    super(_a);
  }

  @Override
  public EbliActionInterface[] buildActions() {
    final List r = new ArrayList(5);
    final List actionForGroup = new ArrayList(5);
    final EbliActionSimple s = new EbliActionSimple(EbliResource.EBLI.getString("Restaurer"), EbliResource.EBLI
        .getIcon("loupe-etendue"), "RESTORE") {

      @Override
      public void actionPerformed(ActionEvent _e) {
        vue_.getGraphe().restore();
      }
    };
    s.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Restaurer la vue globale"));
    s.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('r'));
    r.add(s);

    final EGInteractionSuivi suivi = new EGInteractionSuivi(vue_.getGraphe());
    vue_.addInteractiveCmp(suivi);
    EGActionInteraction a = new EGActionInteraction("suivi", EbliResource.EBLI.getIcon("pointeur"), "SUIVI", suivi);
    a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("suivre le pointeur"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('f'));
    r.add(a);
    actionForGroup.add(a);

    final EGInteractionZoom zoom = new EGInteractionZoom(vue_.graphe_);
    vue_.addInteractiveCmp(zoom);
    a = new EGActionInteraction(EbliResource.EBLI.getString("Zoom"), EbliResource.EBLI.getIcon("loupe"), "ZOOM", zoom);
    a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Zoom"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('z'));
    r.add(a);
    actionForGroup.add(a);

    // addKeyListener(selection_);
    vue_.graphe_.addKeyListener(selection_);
    vue_.addInteractiveCmp(selection_);
    a = new EGActionInteraction(EbliResource.EBLI.getString("Sélection ponctuelle"), EbliResource.EBLI
        .getIcon("fleche"), "SELECT", selection_);
    a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Sélectionner des points"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('s'));
    r.add(a);
    selection_.addPropertyChangeListener(this);
    actionForGroup.add(a);
    if (getModel().isContentModifiable()) {
      final EGInteractionDeplacementPoint pt = new EGInteractionDeplacementPoint(this);
      vue_.addInteractiveCmp(pt);
      a = new EGActionInteraction(EbliResource.EBLI.getString("Déplacer un point"), EbliResource.EBLI
          .getIcon("transform"), "MOVE_POINT", pt);
      a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Déplacer le point sélectionné"));
      a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('m'));
      r.add(a);
      pt.addPropertyChangeListener(this);
      actionForGroup.add(a);
    }
    final EbliActionInterface[] rf = new EbliActionInterface[r.size()];
    r.toArray(rf);
    EbliLib.updateMapKeyStroke(this, rf);

    return rf;
  }
}