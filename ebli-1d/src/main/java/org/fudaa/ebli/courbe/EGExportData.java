/*
 * @creation 10 f�vr. 2005
 * 
 * @modification $Date: 2007-06-14 12:00:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.table.CtuluTableCellDoubleValue;
import org.fudaa.ctulu.table.CtuluTableModelInterface;

/**
 * @author Fred Deniger
 * @version $Id: EGExportData.java,v 1.13 2007-06-14 12:00:22 deniger Exp $
 */
public abstract class EGExportData implements CtuluTableModelInterface {

  private List<String> comments;
  protected final boolean showLabel;

  public boolean isShowLabel() {
    return showLabel;
  }

  public String getStringXValueFor(final double xValue) {
    if (g_.getModel().getAxeX().getSpecificFormatExport() != null) {
      return g_.getModel().getAxeX().getSpecificFormatExport().format(xValue);
    }
    if (g_.getModel().getAxeX().getSpecificFormat() != null) {
      return g_.getModel().getAxeX().getSpecificFormat().format(xValue);
    }
    return Double.toString(xValue);
  }

  public String getStringYValueFor(final EGCourbe courbe, final double yValue) {
    if (courbe.getAxeY() != null && courbe.getAxeY().getSpecificFormatExport() != null) {
      return courbe.getAxeY().getSpecificFormatExport().format(yValue);
    }
    if (courbe.getAxeY() != null && courbe.getAxeY().getSpecificFormat() != null) {
      return courbe.getAxeY().getSpecificFormat().format(yValue);
    }
    return Double.toString(yValue);
  }

  public CtuluTableCellDoubleValue getXValueFor(final double xValue) {
    return new CtuluTableCellDoubleValue(getStringXValueFor(xValue), xValue);
  }

  public Object getYValueFor(final EGCourbe courbe, final double yValue) {
    return new CtuluTableCellDoubleValue(getStringYValueFor(courbe, yValue), yValue);
  }

  final static class AllData extends EGExportData {

    AllData(final EGCourbe[] _cs, final EGGraphe _g, boolean showLabel) {
      super(_cs, _g, showLabel);

      maxCol_ = showLabel ? cs_.length * 3 : cs_.length * 2;
      maxLine_ = 0;
      column_ = buildColName(cs_, _g.getTransformer().getXAxe().getTitre(), showLabel);
      for (int i = cs_.length - 1; i >= 0; i--) {
        final int r = cs_[i].getModel().getNbValues();
        if (r > maxLine_) {
          maxLine_ = r;
        }
      }
    }

    @Override
    public Class getColumnClass(int _col) {
      boolean label = showLabel && (_col % 3 == 0);
      return label ? String.class : CtuluTableCellDoubleValue.class;
    }

    @Override
    public Object getValue(final int _row, final int _col) {
      boolean label = showLabel && (_col % 3 == 0);
      final boolean x = showLabel ? (_col % 3 == 1) : (_col % 2 == 0);
      int courbeIdx = x ? _col / 2 : (_col - 1) / 2;
      if (showLabel) {
        if (label) {
          courbeIdx = _col / 3;
        } else if (x) {
          courbeIdx = (_col - 1) / 3;
        } else {
          courbeIdx = (_col - 2) / 3;
        }
      }
      final EGCourbe courbe = cs_[courbeIdx];
      if (_row >= courbe.getModel().getNbValues()) {
        return null;
      }
      if (label) {
        return courbe.getModel().getPointLabel(_row);
      }
      if (x) {
        final double xValue = courbe.getModel().getX(_row);
        return getXValueFor(xValue);
      }
      final double yValue = courbe.getModel().getY(_row);
      return getYValueFor(courbe, yValue);
    }

        @Override
        public int[] getSelectedRows() {
            return null;
        }
  }

  static String[] buildColName(final EGCourbe[] _c, final String _axeX, boolean showLabels) {
    final List<String> r = new ArrayList<>(showLabels ? _c.length * 3 : _c.length * 2);
    for (int i = 0; i < _c.length; i++) {
      if (showLabels) {
        if (_c[i].getModel() instanceof EgModelLabelNamed) {
          r.add(((EgModelLabelNamed) _c[i].getModel()).getLabelColumnName());
        } else {
          r.add("");
        }
      }
      r.add(_axeX + CtuluLibString.ESPACE + (i + 1));
      r.add(_c[i].getTitle());
    }
    return r.toArray(new String[0]);
  }

  private final static class HViewedOnly extends EGExportData {

    final int[][] idxs_;

    HViewedOnly(final EGCourbe[] _cs, final EGGraphe _g, boolean showLabel) {
      super(_cs, _g, showLabel);
      idxs_ = new int[cs_.length][];
      maxLine_ = 0;
      maxCol_ = cs_.length * 2;
      final EGAxeHorizontal h = g_.getTransformer().getXAxe();
      column_ = buildColName(cs_, h.getTitre(), showLabel);
      for (int i = cs_.length - 1; i >= 0; i--) {
        final EGCourbe c = cs_[i];
        final TIntArrayList l = new TIntArrayList(c.getModel().getNbValues());
        for (int j = c.getModel().getNbValues() - 1; j >= 0; j--) {
          if (h.containsPoint(c.getModel().getX(j))) {
            l.add(j);
          }
        }
        idxs_[i] = l.toNativeArray();
        Arrays.sort(idxs_[i]);
        if (idxs_[i].length > maxLine_) {
          maxLine_ = idxs_[i].length;
        }
      }
    }

    @Override
    public Class getColumnClass(int _col) {
      return CtuluTableCellDoubleValue.class;
    }

    @Override
        public int[] getSelectedRows() {
            return null;
        }
  
    
    @Override
    public Object getValue(final int _row, final int _col) {
      final boolean x = (_col % 2 == 0);
      final int courbeIdx = x ? _col / 2 : (_col - 1) / 2;
      if (_row >= idxs_[courbeIdx].length) {
        return null;
      }
      final EGCourbe courbe = cs_[courbeIdx];
      if (x) {
        final double xValue = courbe.getModel().getX(idxs_[courbeIdx][_row]);
        return getXValueFor(xValue);
      }
      final double y = courbe.getModel().getY(idxs_[courbeIdx][_row]);
      return getYValueFor(courbe, y);
    }
    
  }

  private final static class SameH extends EGExportData {

    final double[] time_;
    List<String> labels;

    SameH(final EGCourbe[] _cs, final EGGraphe _g, boolean _onlyH, boolean showLabels) {
      super(_cs, _g, showLabels);
      Set<EGTimeLabel> set = new HashSet<>();
      maxCol_ = cs_.length + 1;
      final EGAxeHorizontal h = g_.getTransformer().getXAxe();
      for (int i = cs_.length - 1; i >= 0; i--) {
        final EGCourbe c = cs_[i];
        for (int j = c.getModel().getNbValues() - 1; j >= 0; j--) {
          final double x = c.getModel().getX(j);
          if ((!_onlyH) || (h.containsPoint(x))) {
            EGTimeLabel value = new EGTimeLabel(x);
            set.add(value);
            value.setLabel(c.getModel().getPointLabel(j));
          }
        }
      }
      List<EGTimeLabel> values = new ArrayList<>(set);
      Collections.sort(values);
      time_ = new double[values.size()];
      for (int i = 0; i < time_.length; i++) {
        time_[i] = values.get(i).getX();
      }
      Arrays.sort(time_);
      maxLine_ = time_.length;
//      int nbCols = _cs.length + 1;
      if (showLabels) {
        maxCol_++;
      }
      column_ = new String[maxCol_];
      int offset = 0;
      if (showLabels) {
        offset = 1;
        column_[0] = "";
        if (cs_.length > 0 && cs_[0].getModel() instanceof EgModelLabelNamed) {
          column_[0] = ((EgModelLabelNamed) cs_[0].getModel()).getLabelColumnName();
        }
        labels = new ArrayList<>();
        for (EGTimeLabel value : values) {
          labels.add(value.getLabel());
        }
      }
      column_[offset] = h.getTitre();
      for (int i = 0; i < _cs.length; i++) {
        column_[i + 1 + offset] = _cs[i].getTitle();
      }
    }

    @Override
    public Class getColumnClass(int _col) {
      if (showLabel && _col == 0) {
        return String.class;
      }
      return CtuluTableCellDoubleValue.class;
    }

    @Override
    public Object getValue(final int _row, final int _col) {
      final double x = time_[_row];
      int offset = 0;
      if (showLabel) {
        offset = 1;
        if (_col == 0) {
          if (_row < labels.size()) {
            return labels.get(_row);
          }
          return "";

        }
      }
      if (_col == offset) {
        return getXValueFor(x);
      }
      return getYValueFor(cs_[_col - 1 - offset], cs_[_col - 1 - offset].interpol(x));
    }
       @Override
        public int[] getSelectedRows() {
            return null;
        }
  
        
  }

  public static EGExportData createExportData(final EGCourbe[] courbesInitiales, final EGGraphe _g, final boolean _isSameH,
          final boolean _onlyHiewed, boolean showLabel, final ProgressionInterface _prog) {
    final EGExportDataBuilder exportDataBuilder = _g.getExportDataBuilder();
    if (exportDataBuilder != null) {
      return exportDataBuilder.createExportData(courbesInitiales, _g, _isSameH, showLabel);
    }
    EGCourbe[] _cs = prepareCourbesForExport(courbesInitiales);
    if (_isSameH) {
      return new SameH(_cs, _g, _onlyHiewed, showLabel);
    } else if (_onlyHiewed) {
      return new HViewedOnly(_cs, _g, showLabel);
    }
    return new AllData(_cs, _g, showLabel);

  }

  public static EGCourbe[] prepareCourbesForExport(final EGCourbe[] courbesInitiales) {
    List<EGCourbe> courbes = new ArrayList<>();
    for (int i = 0; i < courbesInitiales.length; i++) {
      EGCourbe egCourbe = courbesInitiales[i];
      if (!egCourbe.isVisible()) {
        continue;
      }
      Collection<EGCourbe> associatesCourbesForExport = egCourbe.getAssociatesCourbesForExport();
      if (CtuluLibArray.isNotEmpty(associatesCourbesForExport)) {
        courbes.addAll(associatesCourbesForExport);
      }
      courbes.add(egCourbe);
    }
    EGCourbe[] _cs = courbes.toArray(new EGCourbe[0]);
    return _cs;
  }
  protected String[] column_;
  protected final EGCourbe[] cs_;
  protected final EGGraphe g_;
  protected int maxCol_;
  protected int maxLine_;

  protected EGExportData(final EGCourbe[] _cs, final EGGraphe _g, boolean showLabel) {
    cs_ = _cs;
    this.showLabel = showLabel;
    g_ = _g;
  }

  @Override
  public List<String> getComments() {
    return comments;
  }

  public void setComments(List<String> comments) {
    this.comments = comments;
  }

  public EGCourbe[] getCourbes() {
    return cs_;
  }

  public EGGraphe getGraphe() {
    return g_;
  }

  @Override
  public String getColumnName(final int _i) {
    return column_[_i];
  }

  public abstract Class getColumnClass(final int _i);

  public EGCourbe getCourbe(final int _idx) {
    return cs_[_idx];
  }

  @Override
  public WritableCell getExcelWritable(final int _row, final int _col, final int _rowInXls, final int _colInXls) {
    final Object value = getValue(_row, _col);
    if (value == null) {
      return null;
    }
    if (value instanceof Double) {
      final Double d = (Double) value;
      return new Number(_colInXls, _rowInXls, d.doubleValue());
    }
    if (value instanceof CtuluTableCellDoubleValue) {
      final CtuluTableCellDoubleValue d = (CtuluTableCellDoubleValue) value;
      return new Number(_colInXls, _rowInXls, d.getValue());
    }
    String asString = value.toString();
    try {
      double d = Double.parseDouble(asString.replace(',', '.'));
      return new Number(_colInXls, _rowInXls, d);
    } catch (NumberFormatException ex) {
    }
    return new Label(_colInXls, _rowInXls, asString);
  }

  @Override
  public final int getMaxCol() {
    return maxCol_;
  }

  @Override
  public final int getMaxRow() {
    return maxLine_;
  }
}
