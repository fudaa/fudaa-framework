package org.fudaa.ebli.courbe;

import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;

/**
 * Attention pas complètement implémenté.
 *
 * @author Frederic Deniger
 */
public class EGCourbeSimplePersistBuilderDefault extends EGCourbePersistBuilder<EGCourbeSimple> {

  @Override
  protected EGCourbeSimple createEGObject(EGCourbePersist target, Map params, CtuluAnalyze log) {
    return null;
  }

  @Override
  protected void postRestore(EGCourbeSimple egObject, EGCourbePersist persist, Map params, CtuluAnalyze log) {
    initGraphicConfiguration(egObject, persist);
    egObject.setTitle(persist.getTitle());
    if (persist.inverse) {
      egObject.inverserModele();
    }
  }
}
