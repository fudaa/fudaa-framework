/*
 * @creation 29 nov. 06
 * @modification $Date: 2007-05-22 14:19:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import gnu.trove.TIntArrayList;
import java.awt.Color;
import java.awt.Graphics2D;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: EGCourbeSurfacePainter.java,v 1.5 2007-05-22 14:19:05 deniger Exp $
 */
public class EGCourbeSurfacePainter {
  public interface Delegate {
    boolean isDataValid();

    void paint(EGCourbeSurfacePainter _parent, Graphics2D _g2d, EGRepere _r, int _idx);
  }
  public static class DelegateAxeX extends DelegateFixe {

    @Override
    public int getEcranValue(final EGRepere _r, final EGAxeVertical _y) {
      return _r.getMaxEcranY();
    }

  }

  public static class DelegateBottom extends DelegateFixe {

    @Override
    public int getEcranValue(final EGRepere _r, final EGAxeVertical _y) {
      return _r.getH();
    }

  }

  public static class DelegateCourbe implements Delegate, EGGrapheModelListener {
    EGCourbe cs_;
    final TIntArrayList list_ = new TIntArrayList();
    final EGGrapheModel model_;

    public DelegateCourbe(final EGGrapheModel _model) {
      super();
      model_ = _model;
      model_.addModelListener(this);
    }

    protected EGCourbe getCs() {
      return cs_;
    }

    protected boolean setCs(final EGCourbe _cs) {
      if (_cs == cs_) {
        return false;
      }
      cs_ = _cs;
      return true;
    }

    @Override
    public void axeAspectChanged(final EGAxe _c) {}

    @Override
    public void axeContentChanged(final EGAxe _c) {}

    @Override
    public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {}

    @Override
    public void courbeContentChanged(final EGObject _c, final boolean _mustRestore) {}

    @Override
    public boolean isDataValid() {
      return cs_ != null && model_.contains(cs_);
    }

    public boolean isIn(final double _x) {
      return _x >= cs_.getModel().getXMin() && _x <= cs_.getModel().getXMax();
    }

    @Override
    public void paint(final EGCourbeSurfacePainter _parent, final Graphics2D _g2d, final EGRepere _t, final int _i) {
      if (cs_ == null) {
        return;
      }
      final EGCourbe targetCourbe = _parent.targetCourbe_;
      final EGAxeVertical axeY = targetCourbe.getAxeY();
      final EGModel targetModel = targetCourbe.getModel();
      // pas la peine
      if (targetModel.getX(_i - 1) > cs_.getModel().getXMax()) {
        return;
      }
      if (targetModel.getX(_i) < cs_.getModel().getXMin()) {
        return;
      }
      final double xRealDeb = Math.max(targetModel.getX(_i - 1), cs_.getModel().getXMin());
      final double xRealFin = Math.min(targetModel.getX(_i), cs_.getModel().getXMax());
      cs_.fillWithIdxStrictlyIncluded(xRealDeb, xRealFin, list_);
      int xDeb = _t.getXEcran(xRealDeb);
      int yDeb = _t.getYEcran(targetCourbe.interpol(xRealDeb), axeY);
      final boolean logarithmique = _t.getXAxe().isLogarithmique();
      if (logarithmique) {
        yDeb = (int) targetCourbe.interpolOnEcran(xRealDeb, _t);
      }
      int y1 = _t.getYEcran(cs_.interpol(xRealDeb), axeY);
      if (logarithmique) {
        y1 = (int) cs_.interpolOnEcran(xRealDeb, _t);
      }
      int xEnd;
      int yEnd;
      int y2;
      final int nbIntern = list_.size();
      for (int i = 0; i < nbIntern; i++) {
        final double xIntern = cs_.getModel().getX(list_.getQuick(i));
        final double yIntern = cs_.getModel().getY(list_.getQuick(i));
        xEnd = _t.getXEcran(xIntern);
        y2 = _t.getYEcran(yIntern, axeY);
        yEnd = _t.getYEcran(targetCourbe.interpol(xIntern), axeY);
        if (logarithmique) {
          yEnd = (int) targetCourbe.interpolOnEcran(xIntern, _t);
        }
        _parent.fillSegment(_g2d, y1, y2, xDeb, yDeb, xEnd, yEnd);
        xDeb = xEnd;
        y1 = y2;
        yDeb = yEnd;
      }
      xEnd = _t.getXEcran(xRealFin);
      yEnd = _t.getYEcran(targetCourbe.interpol(xRealFin), axeY);
      if (logarithmique) {
        yEnd = (int) targetCourbe.interpolOnEcran(xRealFin, _t);
      }
      y2 = _t.getYEcran(cs_.interpol(xRealFin), axeY);
      if (logarithmique) {
        y2 = (int) cs_.interpolOnEcran(xRealFin, _t);
      }
      if (isIn(xRealFin)) {
        _parent.fillSegment(_g2d, y1, y2, xDeb, yDeb, xEnd, yEnd);
      }

    }

    @Override
    public void structureChanged() {
      if (!model_.contains(cs_)) {
        cs_ = null;
      }
    }
  }
  public abstract static class DelegateFixe implements Delegate {

    public abstract int getEcranValue(EGRepere _r, EGAxeVertical _y);

    @Override
    public boolean isDataValid() {
      return true;
    }

    @Override
    public void paint(final EGCourbeSurfacePainter _parent, final Graphics2D _g2d, final EGRepere _t, final int _i) {

      final EGCourbe c = _parent.targetCourbe_;
      final EGAxeVertical axeY = c.getAxeY();
      final int palier = getEcranValue(_t, axeY);
      final EGModel model = c.getModel();
      final int xie = _t.getXEcran(model.getX(_i - 1));
      final int yie = _t.getYEcran(model.getY(_i - 1), axeY);
      final int xi = _t.getXEcran(model.getX(_i));
      final int yi = _t.getYEcran(model.getY(_i), axeY);
      _parent.fillSegment(_g2d, palier, palier, xie, yie, xi, yi);

    }

  }

  public class DelegatePalier extends DelegateFixe {
    Double val_;

    public DelegatePalier(final double _val) {
      val_ = CtuluLib.getDouble(_val);
    }

    public DelegatePalier(final Double _val) {
      super();
      val_ = _val;
    }

    protected double getVal() {
      return val_.doubleValue();
    }

    protected boolean setVal(final Double _val) {
      if (_val == val_) {
        return false;
      }
      val_ = _val;
      return true;
    }

    @Override
    public int getEcranValue(final EGRepere _r, final EGAxeVertical _y) {
      return _r.getYEcran(val_.doubleValue(), _y);
    }

  }
  private static final String[] PAINTER_DEFINITION = new String[] { EbliLib.getS("Aucun"),
      EbliLib.getS("Remplir jusqu'� l'axe horizontal"), EbliLib.getS("Remplir toute la partie basse"),
      EbliLib.getS("Remplir jusqu'� la ligne horizontale"), EbliLib.getS("Remplir jusqu'� la courbe") };
  public static final DelegateAxeX TO_AXEX = new DelegateAxeX();
  public static final DelegateBottom TO_BOTTOM = new DelegateBottom();

  public static ComboBoxModel getTypeModel() {
    return new DefaultComboBoxModel(PAINTER_DEFINITION);
  }
  int alpha_ = 128;
  DelegateCourbe delegateCourbe_;
  DelegatePalier delegatePalier_;

  Color down_;

  public static abstract class SelectedIndexForX implements SelectedIndex {

    final int idx_;

    public SelectedIndexForX(final int _idx) {
      super();
      idx_ = _idx;
    }

    public int getIdx() {
      return idx_;
    }
  }

  public interface SelectedIndex {
    boolean isActive(int _idx, EGCourbe _target);
  }

  public static final SelectedIndex NONE = new SelectedIndex() {
    @Override
    public boolean isActive(int _idx, EGCourbe _target) {
      return true;
    }
  };
  public static final SelectedIndex TIME_FOR_MAX = new SelectedIndex() {
    @Override
    public boolean isActive(int _idx, EGCourbe _target) {
      return _idx <= _target.getActiveTimeIdx();
    }

  };
  public static final SelectedIndex TIME_FOR_MIN = new SelectedIndex() {
    @Override
    public boolean isActive(int _idx, EGCourbe _target) {
      return _idx >= _target.getActiveTimeIdx();
    }

  };

  public static final SelectedIndex createSelectedForMax(final int _idx) {
    return new SelectedIndexForX(_idx) {

      @Override
      public boolean isActive(final int _idxEnCours, final EGCourbe _target) {
        return _idxEnCours <= idx_;
      }

    };
  }

  public static final SelectedIndex createSelectedForMin(final int _idx) {
    return new SelectedIndexForX(_idx) {

      @Override
      public boolean isActive(final int _idxEnCours, final EGCourbe _target) {
        return _idxEnCours >= idx_;
      }

    };
  }

  public SelectedIndex maxIdx_ = NONE;

  SelectedIndex minIdx_ = NONE;
  final EGGrapheModel model_;

  Delegate painter_;

  int selectedPainter_;
  /**
   * La courbe en question.
   */
  final EGCourbe targetCourbe_;

  Color up_;

  /**
   * Tableau permettant de stockant les 4 x du polygone a remplir.
   */
  final int[] x_;

  /**
   * Tableau permettant de stockant les 4 y du polygone a remplir.
   */
  final int[] y_;
  public static final String PROPERTY_MIN_IDX = "surfaceMinIdx";
  public static final String PROPERTY_MAX_IDX = "surfaceMaxIdx";

  public EGCourbeSurfacePainter(final EGCourbe _target, final EGGrapheModel _model) {
    super();
    targetCourbe_ = _target;
    down_ = targetCourbe_.getAspectContour();
    if (down_ == null) {
      down_ = targetCourbe_.getLigneModel().getCouleur();
    }
    if (down_ == null) {
      down_ = Color.CYAN;
    }
    down_ = down_.brighter();
    up_ = down_;
    x_ = new int[4];
    y_ = new int[4];
    model_ = _model;
  }

  public EGCourbeSurfacePainter duplicate(EGCourbe cibleDuplique, EGGrapheDuplicator _duplicator) {
    //TODO: targetCourbe_.duplicate() n'est pas dans le graphe. Il faut une Map avec les anciennes/nouvelles valeurs
    //pas bon la courbe va etre duplique ailleurs

    // -- il ne faut pas oublier de changer la reference de la courvbe target
    // qui est duplic et non this, de meme pour le model pour eviter des boucles
    // infinies --//

    EGCourbeSurfacePainter duplic = new EGCourbeSurfacePainter(cibleDuplique, _duplicator.getModelDuplique());
    duplic.alpha_ = this.alpha_;
    if (this.delegateCourbe_ != null && duplic.model_ != null) {
      // if (duplic.model_ == null)
      // duplic.model_ = this.model_.duplicate(_duplicator);
    duplic.delegateCourbe_ = new DelegateCourbe(duplic.model_);
    }
    duplic.down_ = this.down_;
    duplic.maxIdx_ = this.maxIdx_;
    duplic.minIdx_ = this.minIdx_;
    if (this.delegatePalier_ != null)
    duplic.delegatePalier_ = new DelegatePalier(((int) Math.floor(targetCourbe_.getYMin())));
  
    duplic.selectedPainter_ = this.selectedPainter_;
    duplic.up_ = this.up_;
  
    // painter.x_ = CtuluLibArray.copy(this.x_);
    // painter.y_ = CtuluLibArray.copy(this.y_);
    
    
    
    return duplic;

  }
  
  
  protected int getAlpha() {
    return alpha_;
  }

  protected Color getDown() {
    return down_;
  }

  protected EGGrapheModel getModel() {
    return model_;
  }

  protected Delegate getPainter() {
    return painter_;
  }

  protected Color getUp() {
    return up_;
  }

  protected void fillDown(final Graphics2D _g) {
    fillPolygon(down_, _g);
  }

  protected void fillDown(final Graphics2D _g, final int _nb) {
    fillPolygon(down_, _g, _nb);
  }

  protected void fillPolygon(final Color _c, final Graphics2D _g) {
    fillPolygon(_c, _g, x_ == null ? 0 : x_.length);
  }

  protected void fillPolygon(final Color _c, final Graphics2D _g, final int _nb) {
    if (x_ != null && _c != null) {
      _g.setColor(EbliLib.getAlphaColor(_c, alpha_));
      _g.fillPolygon(x_, y_, _nb);
    }
  }

  /**
   * Les coordonnees sont des coordonn�es �cran. Elles sont pass�es sous forme de double pour �viter les erreurs
   * d'arrondis lors des calculs des intersections.
   * 
   * @param _g2d le graphics cible
   * @param _y1 le y de d�but de la courbe a comparer
   * @param _y2 le y de fin de la courbe a comparer
   * @param _xDeb le x de debut de la courbe
   * @param _yDeb le y de debut de la courbe
   * @param _xEnd le x de fin de la courbe
   * @param _yEnd le y de fin de la courbe
   */
  protected void fillSegment(final Graphics2D _g2d, final double _y1, final double _y2, final double _xDeb,
      final double _yDeb, final double _xEnd, final double _yEnd) {
    // inutile de faire des divisions par zero ....
    if (_xDeb == _xEnd || (_yDeb == _y1) && _yEnd == _y2) {
      return;
    }
    if ((_yDeb >= _y1 && _yEnd >= _y2) || (_yDeb <= _y1 && _yEnd <= _y2)) {
      x_[0] = (int) _xDeb;
      y_[0] = (int) _yDeb;
      x_[1] = (int) _xDeb;
      y_[1] = (int) _y1;
      x_[2] = (int) _xEnd;
      y_[2] = (int) _y2;
      x_[3] = (int) _xEnd;
      y_[3] = (int) _yEnd;
      if (_yDeb >= _y1) {
        paintUp(_g2d);
      } else {
        fillDown(_g2d);
      }
    } else {
      final double xInter = getIntersect(_xDeb, _yDeb, _xEnd, _yEnd, _y1, _y2);
      final double yInter = _yDeb + (xInter - _xDeb) * (_yEnd - _yDeb) / (_xEnd - _xDeb);
      x_[0] = (int) _xDeb;
      y_[0] = (int) _yDeb;
      x_[1] = (int) _xDeb;
      y_[1] = (int) _y1;
      x_[2] = (int) xInter;
      y_[2] = (int) yInter;
      if (_yDeb >= _y1) {
        paintUp(_g2d, 3);
      } else {
        fillDown(_g2d, 3);
      }
      x_[0] = (int) xInter;
      y_[0] = (int) yInter;
      x_[1] = (int) _xEnd;
      y_[1] = (int) _y2;
      x_[2] = (int) _xEnd;
      y_[2] = (int) _yEnd;
      if (_yEnd >= _y2) {
        paintUp(_g2d, 3);
      } else {
        fillDown(_g2d, 3);

      }
    }
  }

  protected void paintUp(final Graphics2D _g) {
    fillPolygon(up_, _g);
  }

  protected void paintUp(final Graphics2D _g, final int _nb) {
    fillPolygon(up_, _g, _nb);
  }

  protected boolean setDown(final Color _down) {
    if (down_ == _down) {
      return false;
    }
    down_ = _down;
    targetCourbe_.fireCourbeAspectChanged(false);
    targetCourbe_.firePropertyChange(EGCourbeSurfacePainterConfigure.BOTTOM_COLOR);
    targetCourbe_.firePropertyChange(EGCourbeSurfacePainterConfigure.COMMON_COLOR);
    return true;
  }

  protected boolean setUp(final Color _up) {
    if (_up == up_) {
      return false;
    }
    up_ = _up;
    targetCourbe_.fireCourbeAspectChanged(false);
    targetCourbe_.firePropertyChange(EGCourbeSurfacePainterConfigure.TOP_COLOR);
    targetCourbe_.firePropertyChange(EGCourbeSurfacePainterConfigure.COMMON_COLOR);
    return true;
  }

  protected boolean updateCourbe(final EGCourbe _cs) {
    final boolean res = delegateCourbe_ == null ? false : delegateCourbe_.setCs(_cs);
    if (res && delegateCourbe_ == painter_) {
      targetCourbe_.fireCourbeAspectChanged(false);
    }
    return res;

  }

  public EGCourbe getDelegateCourbe() {
    return delegateCourbe_ == null ? null : delegateCourbe_.getCs();
  }

  public double getIntersect(final double _xDeb, final double _yDeb, final double _xEnd, final double _yEnd,
      final double _y1, final double _y2) {
    return _xDeb + (_xEnd - _xDeb) / (1 + (_yEnd - _y2) / (_y1 - _yDeb));
  }

  public Double getPalier() {
    return delegatePalier_ == null ? null : delegatePalier_.val_;
  }

  public int getSelectedPainter() {
    return selectedPainter_;
  }

  /**
   * @param _c la courbe en question
   * @param _g2d l'objet graphics de destination
   * @param _idx l'indice >0 et inferieur au nombre de point
   */
  public void paint(final Graphics2D _g2d, final EGRepere _r, final int _idx) {
    // non dessine
    if (!minIdx_.isActive(_idx - 1, targetCourbe_) || !maxIdx_.isActive(_idx, targetCourbe_)) {
      return;
    }
    if (delegateCourbe_ != null) {

      if (!delegateCourbe_.isDataValid()) {
        delegateCourbe_.setCs(null);
      }
      // pas la peine ...
      if (painter_ == delegateCourbe_ && delegateCourbe_.getCs() == targetCourbe_) {
        return;
      }
    }
    final Color old = _g2d.getColor();
    if (painter_ != null && painter_.isDataValid()) {
      painter_.paint(this, _g2d, _r, _idx);
    }
    _g2d.setColor(old);
  }

  public boolean setAlpha(final int _alpha) {
    if (alpha_ == _alpha) {
      return false;
    }
    alpha_ = _alpha;
    targetCourbe_.fireCourbeAspectChanged(false);
    return true;
  }

  public boolean setPalier(final Double _palier) {
    final boolean res = delegatePalier_ == null ? false : delegatePalier_.setVal(_palier);
    if (res) {
      targetCourbe_.fireCourbeAspectChanged(false);
    }
    return res;
  }

  public boolean setSelectedPainter(final int _i) {
    if (_i == selectedPainter_) {
      return false;
    }
    selectedPainter_ = _i;
    if (selectedPainter_ == 0) {
      painter_ = null;
    } else if (selectedPainter_ == 1) {
      painter_ = TO_AXEX;
    } else if (selectedPainter_ == 2) {
      painter_ = TO_BOTTOM;
    } else if (selectedPainter_ == 3) {
      if (delegatePalier_ == null) {
        delegatePalier_ = new DelegatePalier(((int) Math.floor(targetCourbe_.getYMin())));
      }
      painter_ = delegatePalier_;
    } else if (selectedPainter_ == 4) {
      if (delegateCourbe_ == null) {
        delegateCourbe_ = new DelegateCourbe(model_);
      }
      painter_ = delegateCourbe_;
    }
    targetCourbe_.fireCourbeAspectChanged(false);
    // pour avertir que ces propri�t�s sont d�sormais �ditables
    targetCourbe_.firePropertyChange(PROPERTY_MAX_IDX);
    targetCourbe_.firePropertyChange(PROPERTY_MIN_IDX);
    return true;

  }
  
  public boolean isCourbePainter(){
    return selectedPainter_==4;
  }

  public void setSelectedPainterBottom() {
    setSelectedPainter(2);
  }

  public void setSelectedPainterCourbe(final EGCourbe _c) {
    setSelectedPainter(4);
    updateCourbe(_c);
  }
  
  public EGCourbe getSelectedPainterCourbe() {
    if(selectedPainter_==4){
      return delegateCourbe_.getCs();
    }
    return null;
    
  }

  public void setSelectedPainterToXaxe() {
    setSelectedPainter(1);
  }

  public SelectedIndex getMinIdx() {
    return minIdx_;
  }

  public boolean setMinIdx(final SelectedIndex _minIdx) {
    if (minIdx_ == _minIdx) {
      return false;
    }
    minIdx_ = _minIdx;
    targetCourbe_.fireCourbeAspectChanged(false);
    return true;
  }

  public SelectedIndex getMaxIdx() {
    return maxIdx_;
  }

  public boolean setMaxIdx(final SelectedIndex _maxIdx) {
    if (maxIdx_ == _maxIdx) {
      return false;
    }
    maxIdx_ = _maxIdx;
    targetCourbe_.fireCourbeAspectChanged(false);
    return true;
  }

}
