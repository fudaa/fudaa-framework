package org.fudaa.ebli.courbe;

import java.util.HashMap;
import java.util.Map;

/**
 * Permet de sauvegarder les axes deja duplique et de mettre � jour les
 * r�f�rences
 * 
 * @author deniger
 * 
 */
public class EGGrapheDuplicator {

  final Map<EGAxeVertical, EGAxeVertical> oldNew_ = new HashMap<>();
  EGAxeHorizontal oldH_;
  EGAxeHorizontal newH_;

  /**
   * instance du eggrapheModel tres importante cette ref: envoye des la
   * duplication du eggrapheModel a la base. Il est recupere notamment pour le
   * EGCourbeSurfacePainter afin d eviter des stackoverflow de duplication du
   * graphe model qui est deja duplique!!! .
   */
  EGGrapheModel modelDuplique_;
  
  public EGGrapheModel getModelDuplique() {
    return modelDuplique_;
  }

  public void setModelDuplique_(EGGrapheModel modelDuplique_) {
    this.modelDuplique_ = modelDuplique_;
  }

  /**
   * @param _vert l'axe a dupliquer
   * @return l'axe duplique en prenant soin de ne pas le recreer
   */
  public EGAxeVertical duplicateAxeV(EGAxeVertical _vert) {
    EGAxeVertical res = oldNew_.get(_vert);
    if (res == null) {
      res = _vert.duplicate();
      oldNew_.put(_vert, res);
    }
    return res;
  }

  /**
   * @param _vert l'axe horizontal a dupliquer. On suppose qu'il n'y a qu'un
   *          seul axe h.
   * @return l'axe duplique
   */
  public EGAxeHorizontal duplicateAxeH(EGAxeHorizontal _vert){
    if(oldH_!=null && _vert!=oldH_){
      //est-ce necessaire...
      throw new IllegalArgumentException(" un seul axe horizontal par graphe");
    }
    
    if(newH_==null) {
      oldH_=_vert;
      newH_=oldH_.duplicate();
    }
    return newH_;
    
  }
}
