/*
 * @creation 22 juin 2004
 * 
 * @modification $Date: 2007-05-22 14:19:04 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuLib;
import com.memoire.fu.FuLog;
import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Shape;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gui.ExportTableCommentSupplier;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.palette.BPaletteInfo;
import org.fudaa.ebli.repere.RepereMouseKeyController;
import org.fudaa.ebli.repere.RepereMouseKeyTarget;

/**
 * @author Fred Deniger
 * @version $Id: EGGraphe.java,v 1.26 2007-05-22 14:19:04 deniger Exp $
 */
public class EGGraphe extends JComponent implements ComponentListener, EGGrapheModelListener, RepereMouseKeyTarget,
        CtuluImageProducer {

  public static final Font DEFAULT_FONT = new Font("SansSerif", Font.PLAIN, 10);

  /**
   * Liste qui contient les parametres d'origines des titres, doit etre relanc� dans le cas ou l'on ajoute de nouveau une legende.
   */
  final HashMap<EGCourbe, String> listeTitlesOrigins_ = new HashMap<>();

  private static void zoomIn(final EGAxe _axe, final double _sCenter, final double _zoomFactor) {
    final double newEcart = _axe.getEcart() * _zoomFactor / 2;
    final double newMax = _sCenter + newEcart;
    final double newMin = _sCenter - newEcart;
    _axe.setBounds(newMin, newMax);
  }

  public static final String getAutorRestorePref() {
    return "ebli.courbe.auto.restore";
  }
//  private boolean autoRestore_ = false;
  private boolean autoRestore_ = EbliPreferences.EBLI.getBooleanProperty(getAutorRestorePref(), false);
  private final RepereMouseKeyController repereController_;
  TObjectIntHashMap<EGAxeVertical> axeOffset_;
  Image cache_;
  CtuluCommandManager cmd_;
  EGGrapheModel model_;
  Map<EGObject, EGAxeVertical> objectInitAxe_;
  CtuluRange xRangeSpecByUser_;
  protected double coeffLarger_ = 0.1;
  protected EGRepere transformer_;
  protected CtuluRange xRange_;
  EGHorizontalBanner horizontalBanner;
  private ExportTableCommentSupplier exportTableCommentSupplier;
  private EGExportDataBuilder exportDataBuilder;

  public EGGraphe(final EGGrapheModel _m) {
    addComponentListener(this);
    setOpaque(true);
    setBackground(Color.WHITE);
    setVisible(true);
    setFocusable(true);
    transformer_ = new EGRepere();
    model_ = _m;
    model_.addModelListener(this);
    cmd_ = new CtuluCommandManager();
    repereController_ = new RepereMouseKeyController(this);
    setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));

    initTitlesOrigins();
  }

  public EGExportDataBuilder getExportDataBuilder() {
    return exportDataBuilder;
  }

  public void setExportDataBuilder(EGExportDataBuilder exportDataBuilder) {
    this.exportDataBuilder = exportDataBuilder;
  }

  public void setExportTableCommentSupplier(ExportTableCommentSupplier exportTableCommentSupplier) {
    this.exportTableCommentSupplier = exportTableCommentSupplier;
  }

  public ExportTableCommentSupplier getExportTableCommentSupplier() {
    return exportTableCommentSupplier;
  }

  private void ajusteXAxe() {
    final EGAxeHorizontal hAxe = getTransformer().getXAxe();
    final double xmin = getXMin();
    final double xmax = getXMax();
    final double ecart = (xmax - xmin) * coeffLarger_;
    double r = getXMax() + ecart;
    if (hAxe.getMaximum() > r) {
      final double oldEcart = hAxe.getEcart();
      hAxe.setBounds(r - oldEcart, r);
    }
    r = getXMin() - ecart;
    if (hAxe.getMinimum() < r) {
      final double oldEcart = hAxe.getEcart();
      hAxe.setBounds(r, r + oldEcart);
    }
    maxXRangeCompute();
  }

  private void computeXRange() {
    if (xRange_ != null) {
      return;
    }
    xRange_ = new CtuluRange();
    final EGGrapheModel m = getModel();
    if (FuLog.isDebug()) {
      FuLog.debug("nb object " + m.getNbEGObject());
    }
    if (!m.isSomethingToDisplay()) {
      xRange_.max_ = 0;
      xRange_.min_ = 0;
      return;
    }
    xRange_.min_ = Double.MAX_VALUE;
    xRange_.max_ = -Double.MAX_VALUE;
    boolean done = false;
    for (int i = m.getNbEGObject() - 1; i >= 0; i--) {
      final EGObject g = m.getEGObject(i);
      if (g.isVisible_) {
        done |= g.ajusteX(xRange_);
      }
    }
    if (!done || Double.isNaN(xRange_.min_)) {
      xRange_.min_ = 0;
    }
    if (!done || Double.isNaN(xRange_.max_)) {
      xRange_.max_ = 0;
    }
  }

  private double getZoomCoef() {
    return 0.8;
  }

  public EGHorizontalBanner getHorizontalBanner() {
    return horizontalBanner;
  }

  public void setHorizontalBanner(EGHorizontalBanner horizontalBanner) {
    this.horizontalBanner = horizontalBanner;
  }

  private void maxXRangeCompute() {

    if (this.xRangeSpecByUser_ != null) {
      final EGAxeHorizontal h = transformer_.getXAxe();
      double min = h.getMinimum();
      double max = h.getMaximum();
      if (min < xRangeSpecByUser_.min_) {
        min = xRangeSpecByUser_.min_;
      }
      if (max > xRangeSpecByUser_.max_) {
        max = xRangeSpecByUser_.max_;
      }
      if (min != h.getMinimum() || max != h.getMaximum()) {
        h.setBounds(min, max);
      }
    }

  }

  /**
   * Restore l'axe des x.
   */
  public void restoreAxeX() {
    final EGAxeHorizontal axe = getTransformer().getXAxe();
    axe.setBounds(getXMin(), getXMax());
    maxXRangeCompute();
  }

  private void restoreAxeY(final EGAxeVertical _axe, final CtuluRange _r) {
    _axe.setBounds(_r.min_, _r.max_);
  }

  private void translateX(final double _factor) {
    translation((int) (transformer_.getWSansMarges() * _factor), 0);
  }

  private void translateY(final double _factor) {
    translation(0, (int) (transformer_.getHSansMarges() * _factor));
  }

  protected void setZoomAdaptedFor(final EGObject _g) {
    if (_g == null) {
      return;
    }
    CtuluRange x = _g.getXRange();
    final EGAxeHorizontal h = transformer_.getXAxe();
    h.setBounds(x.min_, x.max_);
    setYZoomAdaptedFor(_g);
    axeUpdated();

  }

  protected void setYZoomAdaptedFor(final EGObject _g) {
    CtuluRange x;
    final EGAxeVertical v = _g.getAxeY();
    x = _g.getYRange();
    v.setBounds(x.min_, x.max_);
  }

  protected void updateRepere() {
    transformer_.setXAxe(getModel().getAxeX());
  }

  public void addModelListener(final EGGrapheModelListener _l) {
    getModel().addModelListener(_l);
  }

  @Override
  public synchronized void addPropertyChangeListener(final String _propertyName, final PropertyChangeListener _listener) {
    super.addPropertyChangeListener(_propertyName, _listener);
  }

  @Override
  public void axeAspectChanged(final EGAxe _c) {
    fullRepaint();
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
    fullRepaint();
  }

  public void axeUpdated() {
    maxXRangeCompute();
    fullRepaint();
    fireAxeRepereChanged(null);
  }

  @Override
  public void componentHidden(final ComponentEvent _e) {
  }

  @Override
  public void componentMoved(final ComponentEvent _e) {
  }

  @Override
  public void componentResized(final ComponentEvent _e) {
    fullRepaint();
  }

  @Override
  public void componentShown(final ComponentEvent _e) {
  }

  public EGMarges computeMarges() {
    return computeMarges((Graphics2D) getGraphics());
  }

  public EGMarges computeMarges(Graphics2D _gr) {
    final EGMarges m = new EGMarges();
    final EGAxeHorizontal xaxe = transformer_.getXAxe();
    int bannerHeight = horizontalBanner == null ? 0 : horizontalBanner.getHeightNeeded(_gr);
    if (xaxe == null) {
      m.setBas(5 + bannerHeight);
      m.setDroite(5);
    } else {
      m.setBas(xaxe.getBottomHeightNeeded(_gr) + bannerHeight);
      m.setDroite(xaxe.getRightWidthNeeded(_gr));
    }
    m.setGauche(0);
    m.setHaut(0);
    int temp;
    final EGGrapheModel model = getModel();
    final Set axeSet = new HashSet();
    EGAxeVertical last = null;
    EGAxeVertical lastRight = null;
    int nbEGObject = model.getNbEGObject();
    for (int i = 0; i < nbEGObject; i++) {
      final EGObject o = model.getEGObject(i);
      if ((getObject(i).isVisible()) && (o.isSomethingToDisplay())) {
        final EGAxeVertical axe = getObject(i).getAxeY();
        if (axe == null) {
          continue;
        }
        if (axe.isVisible() && !axeSet.contains(axe)) {
          axeSet.add(axe);

          if (axe.isDroite()) {
            m.ajouteMargesDroite(axe.getWidthNeeded(_gr));
            // on recupere le dernier pour enlever la partie qui sera dessin� dans le graphe
            if (lastRight == null) {
              lastRight = axe;
            }
          } else {
            m.ajouteMargesGauche(axe.getWidthNeeded(_gr));
            // meme combat
            if (last == null) {
              last = axe;
            }
          }
          temp = o.getMargeHautNeeded(_gr);
          if (temp > m.getHaut()) {
            m.setHaut(temp);
          }
        }
      }
    }
    // on enleve les parties dessin�es dans le graphe
    if (last != null) {
      m.setGauche(m.getGauche() - last.getWidthDrawnInGraphe(_gr));
    }
    // on enleve les parties dessin�es dans le graphe
    if (lastRight != null) {
      m.setDroite(m.getDroite() - lastRight.getWidthDrawnInGraphe(_gr));
    }
    if (m.getGauche() <= 0) {
      m.setGauche(15);
    }
    if (m.getHaut() <= 0) {
      m.setHaut(15);
    }
    if (m.getDroite() <= 15) {
      m.setDroite(15);
    }
    return m;
  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
    if (_visibil) {
      xRange_ = null;
      if (autoRestore_) {
        restore();
      } else if (_c.isVisible_) {
        final EGAxeVertical vert = _c.getAxeY();
        // si l'axe n'a pas d�j� �t� initialis�.
        if (vert != null && !vert.isRangeInit()) {
          vert.ajusteFor(_c);
        }

      }
    }
    fullRepaint();
  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean _mustRestore) {
    // pour l'instant les deux methodes sont identiques
    structureChanged(_mustRestore);
  }

  public void dessine(final Graphics _g, final int _w, final int _h, final boolean _fill) {
    if (!isVisible()) {
      return;
    }
    final Graphics2D g2d = (Graphics2D) _g;
    BuLib.setAntialiasing(g2d);
    final Color old = _g.getColor();
    if (_fill) {
      g2d.setColor(Color.white);
      g2d.fillRect(0, 0, _w, _h);
    }
    Font f;
    final Font oldF = _g.getFont();

    f = getFont();
    g2d.setFont(f);
    g2d.setColor(Color.black);
    final EGGrapheModel model = getModel();
    final int nbElement = model.getNbEGObject();
    transformer_.initFor(g2d, this);
    // dessine axe horizontal
    final Shape oldClip = g2d.getClip();
    // g2d.clip(new Rectangle(xMarge, yMarge, wMarge, hMarge));
    int prec = 0;
    int precDroite = 0;
    if (axeOffset_ == null) {
      axeOffset_ = new TObjectIntHashMap<>(nbElement);
    } else {
      axeOffset_.clear();
      axeOffset_.ensureCapacity(nbElement);
    }
    for (int i = nbElement - 1; i >= 0; i--) {
      final EGObject g = model.getEGObject(i);
      if (g.isVisible_ && g.isSomethingToDisplay()) {
        g.dessine(g2d, transformer_);
      }
    }
    // on part dans l'autre sens pour les axe
    for (int i = 0; i < nbElement; i++) {
      final EGObject g = model.getEGObject(i);
      if (g.getAxeY() == null || !g.isVisible() || !g.isSomethingToDisplay()) {
        continue;
      }
      if (!axeOffset_.containsKey(g.getAxeY())) {
        final int rightAxe = g.getAxeY().getWidthDrawnInGraphe(g2d);
        final int leftAxe = g.getAxeY().getWidthNeeded(g2d) - rightAxe;
        if (g.getAxeY().isDroite()) {
          if (precDroite > 0) {
            precDroite += rightAxe;
          }
          axeOffset_.put(g.getAxeY(), precDroite);
          precDroite += leftAxe;
        } else {
          if (prec > 0) {
            prec += rightAxe;
          }
          axeOffset_.put(g.getAxeY(), prec);
          prec += leftAxe;
        }
      }
    }
    final TObjectIntIterator iterator = axeOffset_.iterator();
    for (int i = axeOffset_.size(); i-- > 0;) {
      iterator.advance();
      final EGAxeVertical v = (EGAxeVertical) iterator.key();
      v.dessine(g2d, transformer_, iterator.value());
    }
    if (transformer_.getXAxe() == null) {
      return;
    }
    transformer_.getXAxe().dessine(g2d, transformer_);
    if (horizontalBanner != null) {
      horizontalBanner.dessine(g2d, transformer_);
    }

    g2d.setColor(old);
    g2d.setClip(oldClip);
    g2d.setFont(oldF);
  }

  public void fillWithAxeXInfo(final BPaletteInfo.InfoData _d) {
    final EGAxeHorizontal axe = getTransformer().getXAxe();
    if (_d != null) {
      final String pre = EbliLib.getS("Axe horizontal:") + CtuluLibString.ESPACE;
      _d.put(pre + EbliLib.getS("Borne min"), axe.getStringAffiche(axe.getMinimum()));
      _d.put(pre + EbliLib.getS("Borne max"), axe.getStringAffiche(axe.getMaximum()));
    }
  }

  public void fireAxeRepereChanged(final EGAxe _a) {
    getModel().fireAxeContentChanged(_a);
  }

  public void fireColorChanged(final EGObject _i) {
    getModel().fireCourbeAspectChanged(_i, false);
  }

  public void fireVisibilityChanged(final EGObject _o) {
    xRange_ = null;
    getModel().fireCourbeAspectChanged(_o, false);
  }

  public void fullRepaint() {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          fullRepaint();
        }
      });
      return;
    }
    cache_ = null;
    if (isVisible()) {
      repaint();
    }
  }

  public List<EGAxeVertical> getAllAxeVertical() {
    if (axeOffset_ == null) {
      return Collections.emptyList();
    }
    return Arrays.asList(axeOffset_.keys(new EGAxeVertical[axeOffset_.size()]));
  }

  public final CtuluCommandManager getCmd() {
    return cmd_;
  }

  public EGGrapheModel getModel() {
    return model_;
  }

  public void setModel(EGGrapheModel model) {
    model_ = model;
  }

  public EGObject getObject(final int _i) {
    return getModel().getEGObject(_i);
  }

  public int getObjectNb() {
    return getModel().getNbEGObject();
  }

  public int getOffSet(final EGAxeVertical _axe) {
    if (axeOffset_ != null) {
      return axeOffset_.get(_axe);
    }
    return 0;
  }

  public RepereMouseKeyController getRepereController() {
    return repereController_;
  }

  public final EGCourbe getSelectedComponent() {
    return getModel().getSelectedComponent();
  }

  public EGRepere getTransformer() {
    return transformer_;
  }

  public double getTranslateFactor(final boolean _rapide) {
    return (_rapide) ? 0.25 : 0.125;
  }

  public final CtuluRange getUserXRange() {
    return xRangeSpecByUser_;
  }

  public double getXMax() {
    computeXRange();
    return xRange_.max_;
  }

  public double getXMin() {
    computeXRange();
    return xRange_.min_;
  }

  /*
   * public Iterator getGroupIterator(){ return components_.iterator(); }
   */
  public boolean isAutoRestore() {
    return autoRestore_;
  }

  public final boolean isUserXRangeSet() {
    return xRangeSpecByUser_ != null;
  }

  /**
   * @param _o les objets a tester
   * @return true si l'axe d'un de ces objets a ete mixe avec un autre.
   */
  public boolean isVerticalAxisConfigurationMod(final EGObject[] _o) {
    if (objectInitAxe_ != null && _o != null && _o.length > 0) {
      for (int i = _o.length - 1; i >= 0; i--) {
        if (objectInitAxe_.containsKey(_o[i])) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public void paintComponent(final Graphics _g) {
    // Rectangle clipPrec_ = g.getClipBounds();
    final Dimension size = getSize();
    Graphics cg;
    if (cache_ == null) {
      try {
        cache_ = createImage(size.width, size.height);
        cg = cache_.getGraphics();
      } catch (final NullPointerException e) {
        cache_ = null;
        cg = _g;
      }
      dessine(cg, size.width, size.height, true);
      if (cg != _g && cache_ != null) {
        _g.drawImage(cache_, 0, 0, this);
      }
    } else {
      _g.drawImage(cache_, 0, 0, this);
    }
  }

  @Override
  public void print(final Graphics _g) {
    dessine(_g, getWidth(), getHeight(), false);
  }

  public void removeModelListener(final EGGrapheModelListener _l) {
    getModel().removeModelListener(_l);
  }

  public void restore() {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          restore();
        }
      });
      return;
    }
    updateRepere();
    if (transformer_.getXAxe() == null) {
      return;
    }
    restoreAxeX();

    restoreAllYAxe();
    axeUpdated();
    requestFocus();
  }

  /**
   * restore uniquement les axes en y.
   */
  public Map restoreAllYAxe() {
    final Map axeYRange = new HashMap();
    final EGGrapheModel m = getModel();
    for (int i = m.getNbEGObject() - 1; i >= 0; i--) {
      final EGObject o = m.getEGObject(i);
      if (o.isVisible_) {
        final EGAxeVertical axe = o.getAxeY();
        if (axe == null) {
          continue;
        }
        CtuluRange r = (CtuluRange) axeYRange.get(axe);
        if (r == null) {
          r = new CtuluRange();
          r.max_ = -Double.MAX_VALUE;
          r.min_ = Double.MAX_VALUE;
          axeYRange.put(axe, r);
        }
        o.ajusteY(r);
      }
    }
    for (final Iterator it = axeYRange.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final EGAxeVertical axeY = (EGAxeVertical) e.getKey();
      if (axeY == null) {
        continue;
      }
      final CtuluRange r = (CtuluRange) e.getValue();
      if (Double.isNaN(r.max_) || Double.isInfinite(r.max_) || (r.max_ <= -Double.MAX_VALUE)) {
        r.max_ = 0;
      }
      if (Double.isNaN(r.min_) || Double.isInfinite(r.min_) || (r.min_ >= Double.MAX_VALUE)) {
        r.min_ = 0;
      }
      if (r.min_ == r.max_) {
        if (r.max_ == 0) {
          r.max_ = 1;
          r.min_ = -1;
        } else {
          r.max_ = r.min_ * 1.1;
          r.min_ = r.min_ * 0.9;
        }
        if (r.min_ == r.max_) {
          r.max_ = 1;
          r.min_ = -1;
        }
      } //on rajoute 5 pixel en haut et en bas
      else {
        double distanceYReel = Math.abs(transformer_.getDistanceYReel(8, axeY));
        r.min_ -= distanceYReel;
        r.max_ += distanceYReel;
      }
      restoreAxeY(axeY, r);

    }
    return axeYRange;

  }

  public void setAutoRestore(final boolean _autoRestore) {
    if (_autoRestore != autoRestore_) {
      autoRestore_ = _autoRestore;
      if (autoRestore_) {
        restore();
      }
      firePropertyChange("autorestore", !autoRestore_, autoRestore_);
      EbliPreferences.EBLI.putBooleanProperty(getAutorRestorePref(), autoRestore_);
    }
  }

  public final void setCmd(final CtuluCommandManager _cmd) {
    cmd_ = _cmd;
  }

  @Override
  public void setRapide(final boolean _b) {
    // on ne fait rien
  }

  public final void setUserXRange(final CtuluRange _rangeSpecByUser) {
    xRangeSpecByUser_ = _rangeSpecByUser;
  }

  public void setVisible(final EGObject _g, final boolean _b) {
    if (_g.setVisible(_b)) {

      fireVisibilityChanged(_g);
    }
  }

  public void setXAxe(final EGAxeHorizontal _axe) {
    transformer_.setXAxe(_axe);
    getModel().setAxeX(_axe);
  }

  public void setXRangeIsModified() {
    xRange_ = null;
  }

  public void setZoomOn(final Point _no, final Point _se) {
    final EGRepere repere = getTransformer();
    final EGAxeHorizontal hAxe = repere.getXAxe();
    double xReel = getTransformer().getXReel(_no.x);
    double x2Reel = getTransformer().getXReel(_se.x);
    if (xReel > x2Reel) {
      hAxe.setBounds(x2Reel, xReel);
    } else {
      hAxe.setBounds(xReel, x2Reel);
    }
    ajusteXAxe();
    final List l = getAllAxeVertical();
    if (l != null) {
      for (final Iterator it = l.iterator(); it.hasNext();) {
        final EGAxeVertical yaxe = (EGAxeVertical) it.next();
        xReel = repere.getYReel(_no.y, yaxe);
        x2Reel = repere.getYReel(_se.y, yaxe);
        if (xReel > x2Reel) {
          yaxe.setBounds(x2Reel, xReel);
        } else {
          yaxe.setBounds(xReel, x2Reel);
        }
      }
    }
    axeUpdated();
    requestFocus();

  }

  public void setZoomOnPoint(final int _xEcran, final int _yecran, final boolean _out, final double _zoomFactor) {
    final EGRepere repere = getTransformer();
    final EGAxeHorizontal hAxe = repere.getXAxe();
    double z = _out ? 1 / _zoomFactor : _zoomFactor;
    if (Double.isNaN(z)) {
      z = 1;
    }
    final double xReel = getTransformer().getXReel(_xEcran);
    zoomIn(hAxe, xReel, z);
    ajusteXAxe();
    final List l = getAllAxeVertical();
    if (l != null) {
      for (final Iterator it = l.iterator(); it.hasNext();) {
        final EGAxeVertical yaxe = (EGAxeVertical) it.next();
        zoomIn(yaxe, repere.getYReel(_yecran, yaxe), z);
      }
    }
    axeUpdated();
  }

  @Override
  public void structureChanged() {
    structureChanged(false);
  }
  boolean restoring;

  public void structureChanged(final boolean _restore) {
    if (!EventQueue.isDispatchThread()) {
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          structureChanged(_restore);
        }
      });
      return;
    }
    setXRangeIsModified();
    if (autoRestore_ || _restore) {
      if (restoring) {
        return;
      }
      restoring = true;
      restore();
      restoring = false;
    } else {
      fullRepaint();
    }
  }

  @Override
  public void translateXLeft(final boolean _largeTranslate, final boolean _rapide) {
    translateX(-getTranslateFactor(_rapide));

  }

  @Override
  public void translateXRight(final boolean _large, final boolean _rapide) {
    translateX(getTranslateFactor(_rapide));

  }

  @Override
  public void translateYDown(final boolean _largeTranslate, final boolean _rapide) {
    translateY(getTranslateFactor(_rapide));

  }

  @Override
  public void translateYUp(final boolean _largeTranslate, final boolean _rapide) {
    translateY(-getTranslateFactor(_rapide));

  }

  public void translation(final double _realDx, final double _realDy, final boolean _rapide) {
  }

  public void translation(final int _dx, final int _dy) {
    final EGRepere repere = getTransformer();
    if (_dx != 0) {
      final EGAxeHorizontal hAxe = repere.getXAxe();
      double distanceXReel = getTransformer().getDistanceXReel(_dx);
      hAxe.setBounds(hAxe.getMinimum() + distanceXReel, hAxe.getMaximum() + distanceXReel);
      ajusteXAxe();
    }
    if (_dy != 0) {
      final List l = getAllAxeVertical();
      if (l != null) {
        for (final Iterator it = l.iterator(); it.hasNext();) {
          final EGAxeVertical yaxe = (EGAxeVertical) it.next();
          //axe inverse...
          double distanceYReel = -getTransformer().getDistanceYReel(_dy, yaxe);
          yaxe.setBounds(yaxe.getMinimum() + distanceYReel, yaxe.getMaximum() + distanceYReel);
        }
      }
    }
    if (_dx != 0 || _dy != 0) {
      axeUpdated();
    }
    requestFocus();
//    final int width = (int) (0.5 * transformer_.getWSansMarges());
//    final int height = (int) (0.5 * transformer_.getHSansMarges());
//    final int x = (int) (0.5 * (transformer_.getMaxEcranX() + transformer_.getMinEcranX()) + _dx);
//    final int y = (int) (0.5 * (transformer_.getMaxEcranY() + transformer_.getMinEcranY()) + _dy);
//    setZoomOn(new Point(x + width, y + height), new Point(x - width, y - height));
  }

  /**
   * @param _axes les axes a merges
   * @param _s le nom de l'axe
   * @param _r le range
   */
  public void useOneAxeVertical(final EGAxeVertical[] _axes, final String _s, final CtuluRange _r) {
    if (_axes == null || _axes.length < 2) {
      return;
    }
    final EGAxeVertical newAxe = _axes[0].duplicate();
    String t = _s;
    if (CtuluLibString.isEmpty(_s)) {
      t = EbliLib.getS("Axe");
    }
    newAxe.setTitre(t);
    String unite = _axes[0].unite_;
    for (int i = 1; i < _axes.length; i++) {
      if ((unite != null) && !unite.equals(_axes[i].getUnite())) {
        unite = null;
      }
      if (_r == null) {
        newAxe.adjustBounds(_axes[i]);
      }
    }
    if (_r != null) {
      newAxe.setBounds(_r.min_, _r.max_);
    }
    /*
     * if (_pas > 0) { newAxe.pas = _pas; } else newAxe.pas = newAxe.getEcart() / 5;
     */
    newAxe.unite_ = unite;
    mergeAxis(_axes, newAxe);
  }

  public void mergeAxis(final EGAxeVertical[] _axes, final EGAxeVertical newAxe) {
    for (int i = getObjectNb() - 1; i >= 0; i--) {
      final EGObject o = getObject(i);
      if (o.getAxeY() != null && CtuluLibArray.findObject(_axes, o.getAxeY()) >= 0) {
        if (objectInitAxe_ == null) {
          objectInitAxe_ = new HashMap<>();
        }
        if (!objectInitAxe_.containsKey(o)) {
          objectInitAxe_.put(o, o.getAxeY());
        }
        o.setYaxe(newAxe);
      }
    }
  }

  /**
   * Permet de recuperer la configuration d'origine pour les axes des objets _o.
   *
   * @param _o les objets a parcourir
   * @return true si l'axe vertical d'un des obj a ete reinitialise
   */
  public boolean useVerticalAxisInitialConf(final EGObject[] _o) {
    if (objectInitAxe_ != null && _o != null && _o.length > 0) {
      boolean r = false;
      for (int i = _o.length - 1; i >= 0; i--) {
        if (objectInitAxe_.containsKey(_o[i])) {
          r = true;
          _o[i].setYaxe(objectInitAxe_.get(_o[i]));
          objectInitAxe_.remove(_o[i]);
        }
      }
      return r;
    }
    return false;
  }

  /**
   * @param o l'objedt
   * @return si axe vertical de l'objet o a ete fusionne, renvoie l'axe initiale
   */
  public EGAxeVertical getInitVerticalAxis(EGObject o) {
    if (objectInitAxe_ != null) {
      return objectInitAxe_.get(o);
    }
    return null;
  }

  /**
   * @return the merge axis-> the objects using the merges axis.
   */
  public Map<EGAxeVertical, List<EGObject>> getMergeAxis() {
    Map<EGAxeVertical, List<EGObject>> res = new HashMap<>();
    int nbEgObjects = getModel().getNbEGObject();
    for (int i = 0; i < nbEgObjects; i++) {
      EGObject egObject = getModel().getEGObject(i);
      // l'objet utilise un axe merge
      if (isVerticalAxisMerged(egObject)) {
        // si oui, on enregistre l'axe
        EGAxeVertical mergeAxis = egObject.getAxeY();
        List<EGObject> objects = res.get(mergeAxis);
        if (objects == null) {
          objects = new ArrayList<>();
          res.put(mergeAxis, objects);
        }
        objects.add(egObject);
      }
    }
    return res;
  }

  /**
   * @param o l'objedt
   * @return si axe vertical de l'objet o a ete fusionne, renvoie l'axe initiale
   */
  public boolean isVerticalAxisMerged(EGObject o) {
    return getInitVerticalAxis(o) != null;
  }

  public void zoom(final boolean _out) {

    final double factor = _out ? 1 / getZoomCoef() : getZoomCoef();
    final int width = (int) (factor * transformer_.getWSansMarges() / 2);
    final int height = (int) (factor * transformer_.getHSansMarges() / 2);
    PointerInfo info = MouseInfo.getPointerInfo();
    Point location = info.getLocation();
    SwingUtilities.convertPointFromScreen(location, this);
    final int x = location.x - transformer_.getMargesGauche();
    final int y = location.y - transformer_.getMargesHaut();
//    final int x = (int) (0.5 * transformer_.getMaxEcranX() + transformer_.getMinEcranX());
//    final int y = (int) (0.5 * transformer_.getMaxEcranY() + transformer_.getMinEcranY());
    setZoomOn(new Point(x + width, y + height), new Point(x - width, y - height));

  }

  @Override
  public void zoomIn() {
    zoom(false);

  }

  @Override
  public Dimension getDefaultImageDimension() {
    return getSize();
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    final BufferedImage i = CtuluLibImage.createImage(_w, _h, _params);
    final Graphics2D g2d = i.createGraphics();
    CtuluLibImage.setBestQuality(g2d);
    if (CtuluLibImage.mustFillBackground(_params)) {
      g2d.setColor(Color.WHITE);
      g2d.fillRect(0, 0, _w, _h);
    }

    if (getWidth() != _w || getHeight() != _h) {
      g2d.scale(CtuluLibImage.getRatio(_w, getWidth()), CtuluLibImage.getRatio(_h, getHeight()));
    }

    print(g2d);
    g2d.dispose();
    i.flush();
    return i;
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return produceImage(getWidth(), getHeight(), _params);

  }

  @Override
  public void zoomOnMouse(final MouseEvent _evt, final boolean _isOut) {
    final int x = _evt.getX();
    final int y = _evt.getY();
    final double factor = _isOut ? 1 / getZoomCoef() : getZoomCoef();
    final int width = (int) (transformer_.getWSansMarges() * factor / 2);
    final int height = (int) (transformer_.getHSansMarges() * factor / 2);
    /*
     * int width = (int) (factor getWidth() / 2); int height = (int) (factor getHeight() / 2);
     */ setZoomOn(new Point(x + width, y + height), new Point(x - width, y - height));

  }

  @Override
  public void zoomOut() {
    zoom(true);

  }

  public void initTitlesOrigins() {
    EGCourbe[] listeCourbes = getModel().getCourbes();
    for (int i = 0; i < listeCourbes.length; i++) {
      listeTitlesOrigins_.put(listeCourbes[i], listeCourbes[i].getTitle());
    }

  }

  public void reinitTitlesOrigins() {
    EGCourbe[] listeCourbes = getModel().getCourbes();
    for (int i = 0; i < listeCourbes.length; i++) {
      if (listeTitlesOrigins_.get(listeCourbes[i]) != null) {
        listeCourbes[i].initTitle(listeTitlesOrigins_.get(listeCourbes[i]));
      }
    }

  }

  public EGGraphe duplicate() {
    EGGrapheDuplicator duplicator = new EGGrapheDuplicator();
    EGGraphe duplic = new EGGraphe(this.getModel().duplicate(duplicator));
    duplic.autoRestore_ = this.autoRestore_;
    duplic.cache_ = null;
    // on ne partage pas les undo/redo
    duplic.cmd_ = new CtuluCommandManager();

    // TODO: il y a soucis: les axes verticaux sont dupliqu� de partout et on n'a plus les ref
    // -- copie des axes --//
    if (this.axeOffset_ != null) {
      duplic.axeOffset_ = new TObjectIntHashMap();
      int[] values = this.axeOffset_.getValues();
      Object[] liste = this.axeOffset_.keys();
      for (int i = 0; i < liste.length; i++) {
        int value = values[i];
        EGAxeVertical axe = (EGAxeVertical) liste[i];
        duplic.axeOffset_.put(duplicator.duplicateAxeV(axe), value);
      }
    }
    duplic.coeffLarger_ = this.coeffLarger_;

    if (this.objectInitAxe_ != null) {
      duplic.objectInitAxe_ = new HashMap<>();

      Set<EGObject> set = this.objectInitAxe_.keySet();
      Collection<EGAxeVertical> values = this.objectInitAxe_.values();
      EGAxeVertical[] vals = values.toArray(new EGAxeVertical[0]);
      int cpt = 0;
      for (Iterator<EGObject> it = set.iterator(); it.hasNext();) {
        Object objet = it.next();
        if (objet instanceof EGCourbeSimple) {
          EGCourbeSimple cs = (EGCourbeSimple) objet;
          duplic.objectInitAxe_.put(cs.duplicate(cs.getEGParent(), duplicator), vals[cpt++]);
        } else if (objet instanceof EGGroup) {
          EGGroup groupe = (EGGroup) objet;
          duplic.objectInitAxe_.put(groupe.duplicate(duplicator), vals[cpt++]);
        }
      }
    }
    if (this.xRange_ != null) {
      duplic.xRange_ = new CtuluRange(this.xRange_);
    }
    if (this.transformer_ != null) {
      duplic.transformer_ = this.transformer_.duplicate(duplicator);
    }
    if (this.xRangeSpecByUser_ != null) {
      duplic.xRangeSpecByUser_ = new CtuluRange(this.xRangeSpecByUser_);
    }
    return duplic;
  }

  void moveViewFromMouseDeltas(int dx, int dy) {
    translation(-dx, -dy);
  }
}
