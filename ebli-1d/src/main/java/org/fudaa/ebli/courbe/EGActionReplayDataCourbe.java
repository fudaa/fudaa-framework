package org.fudaa.ebli.courbe;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Classe qui permet de rejouer els actions pour une courbe s�lectionn�e. Cette action est automatiquement ajout�e dans le EGspecificAction qui liste
 * les actions des courbes (asscessible via clic droit). Permet de rejouer les donn�es des courbes. Il suffit de surcharger la methode replayData() de
 * EGModel dans le model de courbe d�sir�.
 *
 * @author Adrien Hadoux
 *
 */
public class EGActionReplayDataCourbe extends EbliActionSimple implements EGSelectionListener {

  /**
   * Action utilis�e uniquement pour les courbes unitaires (accessible via clic droit sur la courbe).
   *
   * @author Adrien Hadoux
   *
   */
  public static class CourbeOnly extends EGActionReplayDataCourbe {

    final EGModel model_;
    final EGGrapheTreeModel treemodel_;

    public CourbeOnly(EGGrapheTreeModel treemodel, EGModel model, CtuluUI _impl) {
      super();
      model_ = model;
      impl_ = _impl;
      treemodel_ = treemodel;
      setEnabled(model.isReplayable());
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      if (model_ != null) {
        model_.replayData(treemodel_, new HashMap(), impl_);
      }
    }

    @Override
    public void updateStateBeforeShow() {
    }

    @Override
    protected void updateEnableState() {
    }
  }
  EGFillePanel panelGraphe_;
  CtuluUI impl_;
  EGGrapheModel treemodel_;

  public EGActionReplayDataCourbe(EGFillePanel _panel, CtuluUI _impl) {
    super(EbliResource.EBLI.getString("Rejouer"), EbliResource.EBLI.getIcon("restore"), "REPLAYDATA");
    panelGraphe_ = _panel;
    impl_ = _impl;
    treemodel_ = _panel.getModel();
    treemodel_.addSelectionListener(this);
    setEnabled(false);
  }

  public EGActionReplayDataCourbe() {
    super(EbliResource.EBLI.getString("Rejouer"), EbliResource.EBLI.getIcon("restore"), "REPLAYDATA");
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    updateEnableState();
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    updateEnableState();
  }

  protected void updateEnableState() {
    final EGCourbe selectedComponent = panelGraphe_.getGraphe().getSelectedComponent();
    boolean enable = selectedComponent != null && selectedComponent.getModel() != null && selectedComponent.getModel().isReplayable();
    setEnabled(enable);
  }

  @Override
  public void updateStateBeforeShow() {
    updateEnableState();
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    updateEnableState();
    if (!isEnabled()) {
      impl_.error(EbliResource.EBLI.getString("Aucune courbe s�lectionn�e"), EbliResource.EBLI.getString("Aucune courbe s�lectionn�e"), true);
      return;
    }
    //-- on recupere la courbe selectionnee dans le graphe --//
    EGModel modelSelectionne = panelGraphe_.getGraphe().getSelectedComponent().getModel();

    modelSelectionne.replayData(treemodel_,
            new HashMap(), impl_);

    //-- on restore les donn�es --//
    panelGraphe_.getGraphe().restore();
    panelGraphe_.getGraphe().axeUpdated();

  }
}
