/*
 * @creation 26 janv. 2005
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatFixedFigure;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Fred Deniger
 * @version $Id: EGAxeRepereConfigurator.java,v 1.12 2007-05-04 13:49:41 deniger Exp $
 */
public class EGAxeRepereConfigurator extends BuPanel implements ActionListener, BuBorders, KeyListener,
        BSelecteurTargetInterface {

  private final static CtuluValueEditorI DEFAULT_EDITOR = CtuluValueEditorDefaults.DOUBLE_EDITOR;
  public static final String GRILLE_AXE_X = "GRILLE_AXE_X";
  public static final String GRILLE_AXE_Y = "GRILLE_AXE_Y";
  public static final String SOUS_GRILLE_AXE_X = "SOUS_GRILLE_AXE_X";
  public static final String SOUS_GRILLE_AXE_Y = "SOUS_GRILLE_AXE_Y";

  private static JPanel createPanel(final JComponent... components) {
    final JPanel pn = new JPanel(new BuGridLayout(components.length, 1, 1, true, true));
    for (final JComponent jComponent : components) {
      pn.add(jComponent);
    }
    return pn;
  }

  protected static void initPanel(final JPanel _p, final String _title) {
    _p.setLayout(new BuGridLayout(2, 5, 5));
    _p.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(
            BorderFactory.createEtchedBorder(), _title), EMPTY2222));
  }

  /**
   * Rend les largeurs des labels homog�nes.
   *
   * @param _p un panneau contenant des panneaux contenant des labels en premiere ligne
   */
  public static void updateLabelWidth(final JPanel _p) {
    final List label = new ArrayList();
    for (int i = _p.getComponentCount() - 1; i >= 0; i--) {
      final JComponent c = (JComponent) _p.getComponent(i);
      if (c.getComponentCount() > 0 && c.getComponent(0) instanceof JLabel) {
        label.add(c.getComponent(0));
      }
    }
    int maxWidth = 0;
    for (int i = label.size() - 1; i >= 0; i--) {
      final int w = ((JComponent) label.get(i)).getPreferredSize().width;
      if (w > maxWidth) {
        maxWidth = w;
      }
    }
    for (int i = label.size() - 1; i >= 0; i--) {
      final Dimension d = ((JComponent) label.get(i)).getPreferredSize();
      d.width = maxWidth;
      ((JComponent) label.get(i)).setPreferredSize(d);
    }

  }
  private EGAxeVertical[] axes_;
  private final EGAxeHorizontal axeX_;
  BuCheckBox axeXVisible;
  BuCheckBox axeYVisible;
  final BuCheckBox boxAfficheGraduationsX_ = new BuCheckBox(EbliLib.getS("Afficher la grille"));
  final BuCheckBox boxAfficheGraduationsY_ = new BuCheckBox(EbliLib.getS("Afficher la grille"));
  final BuCheckBox boxAfficheSousGraduationsX_ = new BuCheckBox(EbliLib.getS("Afficher la sous grille"));
  final BuCheckBox boxAfficheSousGraduationsY_ = new BuCheckBox(EbliLib.getS("Afficher la sous grille"));
  private BuCheckBox boxChooserUserLimitY;
  private BuCheckBox boxChooseUserLimitX_;
  private BuCheckBox cbInitialConf_;
  // private BuCheckBox cbUnblockBlockHView_;
  private BuCheckBox cbUseSameAxe_;
  BuComboBox comboHorizontal_ = null;
  BuComboBox comboVertical_ = null;
  private CtuluValueEditorI editorH_;
  private CtuluValueEditorI editorV_;
  private final CtuluNumberFormatFixedFigure formatter = new CtuluNumberFormatFixedFigure(6);
  private final EGGraphe graphe_;
  private Map mapRangesY_ = null;
  /**
   * 4 mode pour les graduations: 0: mode manuel graduations 1: mode manuel longueurs de pas 2: mode automatique graduations 3: mode automatique
   * longueurs de pas
   */
  public int modeGraduationX_ = EGAxe.AUTO_GRADUATIONS;
  /**
   * 4 mode pour les graduations: 0: mode manuel graduations 1: mode manuel longueurs de pas 2: mode automatique graduations 3: mode automatique
   * longueurs de pas
   */
  public int modeGraduationY_ = EGAxe.AUTO_GRADUATIONS;
  BSelecteurLineModel modelGraduationsX_;
  BSelecteurLineModel modelGraduationsY_;
  BSelecteurLineModel modelSousGraduationsX_;
  BSelecteurLineModel modelSousGraduationsY_;
  private final BuPanel pnAxeX;
  private final BuPanel pnAxeY_;
  EGObject[] targets_;
  private JComponent tfHorMax_;
  private JComponent tfHorMin_;
  private BuTextField tfHorPas_;
  private BuTextField tfHorSousPas_;
  private JComponent tfVertMax_;
  private JComponent tfVertMin_;
  private BuTextField tfVertPas_;
  private BuTextField tfVertSousPas_;
  private BuTextField titreAxeXNom;
  BuCheckBox titreAxeXVisible;
  private BuTextField titreAxeYNom;
  BuCheckBox titreAxeYVisible;
  private BuTextField uniteAxeXNom;
  BuCheckBox uniteAxeXVisible;
  private BuTextField uniteAxeY_;
  BuCheckBox uniteAxeYVisible;

  public EGAxeRepereConfigurator(final EGGraphe _m) {
    graphe_ = _m;
    // -- on recupere les valeurs des y --//
    mapRangesY_ = graphe_.restoreAllYAxe();

    // si pas configur� comme attendu
    if (graphe_.getTransformer().getXAxe() == null) {
      pnAxeY_ = null;
      pnAxeX = null;
      axeX_ = null;
      return;
    }
    axeX_ = _m.getTransformer().getXAxe();
    // initialisation des editeurs par defaut
    editorH_ = getEditorFor(axeX_);
    editorV_ = DEFAULT_EDITOR;
    setLayout(new BuVerticalLayout(5, true, true));
    // le panneau pour l'axe horizontal
    pnAxeX = new BuPanel();
    EGAxeRepereConfigurator.initPanel(pnAxeX, EbliLib.getS("Axe horizontal"));
    pnAxeX.setLayout(new BuGridLayout(2, 5, 5));
    buildXPanel(false);
    // vertical
    pnAxeY_ = new BuPanel();
    pnAxeY_.setLayout(new BuGridLayout(2, 5, 5));
    EGAxeRepereConfigurator.initPanel(pnAxeY_, EbliLib.getS("Axes verticaux"));
    buildYPanel(false);
    add(pnAxeX);
    add(pnAxeY_);
    final BuPanel bt = new BuPanel();
    bt.setLayout(new BuButtonLayout(2, SwingConstants.RIGHT));
    final BuButton btValid = new BuButton(BuResource.BU.getString("Appliquer"));
    btValid.setToolTipText(BuResource.BU.getString("Appliquer"));
    btValid.setIcon(BuResource.BU.getIcon("appliquer"));
    btValid.setActionCommand("APPLY");
    btValid.setMnemonic(KeyEvent.VK_A);
    btValid.addActionListener(this);
    add(bt);
    final EbliActionAbstract s = new EbliActionSimple(EbliResource.EBLI.getString("Restaurer"), EbliResource.EBLI.getIcon("restore"), "RESTORE") {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        // -- on reinitialise les mioin/max saisi par l'user --//
        _m.setUserXRange(null);
        _m.restore();
        updateAxeHorValue();
      }
    };
    s.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Restaurer la vue globale"));
    s.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('r'));
    s.putValue(EbliActionInterface.SECOND_KEYSTROKE, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    bt.add(s.buildButton(EbliComponentFactory.INSTANCE));
    bt.add(btValid);
    setTargets(null);
    EGAxeRepereConfigurator.updateLabelWidth(this);

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    apply();
  }

  protected void apply() {
    if ((axes_ == null && axeX_ == null)) {
      return;
    }
    // un seul | pour forcer l'appel aux deux methodes
    // normal ....
    final boolean axeHModified = applyHorizontal();
    final boolean axeVModified = applyVertical();
    if (axeHModified) {
      graphe_.getModel().fireAxeAspectChanged(axeX_);
    }
    if (axeVModified) {
      for (int i = 0; i < axes_.length; i++) {
        graphe_.getModel().fireAxeAspectChanged(axes_[i]);
      }
    }
    if (axeHModified | axeVModified) {
      graphe_.axeUpdated();
    }
    pnAxeX.requestFocus();
  }

  protected EGAxe getSelectedAxeY() {
    EGAxe axeChoisi = null;
    if (graphe_.getSelectedComponent() != null) {
      axeChoisi = graphe_.getSelectedComponent().getAxeY();
    }
    if (axeChoisi == null && axes_ != null && axes_.length > 0) {
      axeChoisi = axes_[0];
    }
    return axeChoisi;
  }

  private boolean applyHorizontal() {
    final String pas = tfHorPas_.getText().trim();
    final String Souspas = tfHorSousPas_.getText().trim();

    final boolean maxEdited = editorH_.isValueValidFromComponent(tfHorMax_) && boxChooseUserLimitX_.isSelected();
    final boolean minEdited = editorH_.isValueValidFromComponent(tfHorMin_) && boxChooseUserLimitX_.isSelected();
    boolean pasEdited = pas.length() > 0;
    if (!pasEdited) {
      pasEdited = Souspas.length() > 0;
    }

    double maxVal = maxEdited ? Double.parseDouble(editorH_.getValue(tfHorMax_).toString()) : axeX_.getMaximum();
    double minVal = minEdited ? Double.parseDouble(editorH_.getValue(tfHorMin_).toString()) : axeX_.getMinimum();
    final double pasVal = pasEdited ? Double.parseDouble(pas) : 0;
    final int souspasVal = (Souspas.length() > 0) ? Integer.parseInt(Souspas) : 0;

    boolean r = false;

    if (maxEdited || minEdited || pasEdited || !boxChooseUserLimitX_.isSelected()) {

      // -- si on d�passe le min et max on recadre --//
      final double maxValueX = graphe_.getXMax();
      final double minValueX = graphe_.getXMin();
      // -- si les limites ne sont plus selectionnes il faut de nouveau tout prendre --//
      if (!boxChooseUserLimitX_.isSelected()) {
        maxVal = maxValueX;
        minVal = minValueX;
        tfHorMax_.setEnabled(false);
        tfHorMin_.setEnabled(false);

      } else {
        // -- on libere la saisie des min et max --//
        tfHorMax_.setEnabled(true);
        tfHorMin_.setEnabled(true);
      }

      if (pasEdited) {
        if (modeGraduationX_ == EGAxe.MANUEL_GRADUATIONS || modeGraduationX_ == EGAxe.AUTO_GRADUATIONS
                || modeGraduationX_ == EGAxe.LOGARITHMIQUE) {
          r = axeX_.setBounds(minVal, maxVal, (int) pasVal, modeGraduationX_);
        } else if (modeGraduationX_ == EGAxe.MANUEL_LONGUEURPAS || modeGraduationX_ == EGAxe.AUTO_LONGUEURPAS) {
          r = axeX_.setBounds(minVal, maxVal, pasVal, modeGraduationX_);
        }
      } else {
        r = axeX_.setBounds(minVal, maxVal, -1, modeGraduationX_);
      }

      if (Souspas.length() > 0) {
        if (souspasVal != axeX_.nbSousGraduations_) {
          r = true;
        }
        axeX_.nbSousGraduations_ = souspasVal;

      }

    } else if (comboHorizontal_.getSelectedIndex() != -1) {
      axeX_.setModeGraduations(comboHorizontal_.getSelectedIndex());
    }
    TraceLigneModel model = modelGraduationsX_.getNewData();
    if (!axeX_.traceGraduations_.equals(model)) {
      axeX_.traceGraduations_ = model;
      r = true;
    }
    model = modelSousGraduationsX_.getNewData();
    if (!axeX_.traceSousGraduations_.equals(model)) {
      axeX_.traceSousGraduations_ = model;
      r = true;
    }

    if (axeX_.isTraceGrille() != boxAfficheGraduationsX_.isSelected()) {
      axeX_.setTraceGrille(boxAfficheGraduationsX_.isSelected());
      r = true;
    }
    if (axeX_.isTraceSousGrille() != boxAfficheSousGraduationsX_.isSelected()) {
      axeX_.setTraceSousGrille(boxAfficheSousGraduationsX_.isSelected());
      r = true;
    }

    if ((maxEdited && minEdited) || !boxChooseUserLimitX_.isSelected()) {
      final CtuluRange range = new CtuluRange();
      range.max_ = maxVal;
      range.min_ = minVal;
      graphe_.setUserXRange(range);
      r = true;
    }

    if (titreAxeXVisible.isSelected() != axeX_.isTitreVisible()) {
      axeX_.setTitreVisible(titreAxeXVisible.isSelected());
      r = true;
    }
    if (uniteAxeXVisible.isSelected() != axeX_.isUniteVisible()) {
      axeX_.setUniteVisible(uniteAxeXVisible.isSelected());
      r = true;
    }
    final String titre = titreAxeXNom.getText();
    if (titre != null) {
      if (axeX_.titre_ != null && !titre.equals(axeX_.titre_)) {
        axeX_.titre_ = titre;
        r = true;
      }
    }
    final String unite = uniteAxeXNom.getText();
    if (unite != null) {
      if (axeX_.unite_ != null && !unite.equals(axeX_.unite_)) {
        axeX_.unite_ = unite;
        r = true;
      }
    }
    if (axeXVisible.isSelected() != axeX_.isVisible()) {
      axeX_.setVisible(axeXVisible.isSelected());
      r = true;
    }

    return r;
  }

  private boolean applyVertical() {
    boolean r = false;
    if (CtuluLibArray.isEmpty(axes_)) {
      return false;
    }
    double maxVal = 0;
    double minVal = 0;
    boolean minEdited = false;
    boolean maxEdited = false;
    if (editorV_ != null) {
      minEdited = editorV_.isValueValidFromComponent(tfVertMin_) && boxChooserUserLimitY.isSelected();
      if (minEdited) {
        minVal = Double.parseDouble(editorV_.getStringValue(tfVertMin_));
      }
      maxEdited = editorV_.isValueValidFromComponent(tfVertMax_) && boxChooserUserLimitY.isSelected();
      if (maxEdited) {
        maxVal = Double.parseDouble(editorV_.getStringValue(tfVertMax_));
      }
    }
    final String pas = tfVertPas_.getText().trim();
    final String Souspas = tfVertSousPas_.getText().trim();

    boolean pasModif = pas.length() > 0;

    if (!pasModif) {
      pasModif = Souspas.length() > 0;
    }

    if (maxEdited || minEdited || pasModif) {

      // -- si le choix des max pas selectionn� il faut r�initialiser les y
      if (!boxChooserUserLimitY.isSelected()) {
        tfVertMax_.setEnabled(false);
        tfVertMin_.setEnabled(false);
        graphe_.restoreAllYAxe();
        return true;

      } else {
        tfVertMax_.setEnabled(true);
        tfVertMin_.setEnabled(true);
      }

      final double pasVal = pasModif ? Double.parseDouble(pas) : 0;
      for (int i = 0; i < axes_.length; i++) {

        final EGAxeVertical axe = axes_[i];
        if (comboVertical_.getSelectedIndex() != -1) {
          axe.setModeGraduations(comboVertical_.getSelectedIndex());
        }

        final int pasSousVal = (Souspas.length() > 0) ? Integer.parseInt(Souspas) : 0;

        double min = minVal;
        double max = maxVal;
        if (!minEdited || !boxChooserUserLimitY.isSelected()) {
          min = axe.getMinimum();
        } else {
        }
        if (!maxEdited || !boxChooserUserLimitY.isSelected()) {
          max = axe.getMaximum();
        }

        // -- on verifie avec le max que cela ne depasse pas--//
        if (mapRangesY_ != null) {
          final Object value = mapRangesY_.get(axe);
          if (value != null && value instanceof CtuluRange) {
            final double maxR = ((CtuluRange) value).max_;
            final double minR = ((CtuluRange) value).min_;

          }
        }

        if (pasModif) {
          if (modeGraduationY_ == EGAxe.MANUEL_GRADUATIONS || modeGraduationY_ == EGAxe.AUTO_GRADUATIONS
                  || modeGraduationY_ == EGAxe.LOGARITHMIQUE) {
            r |= axe.setBounds(min, max, (int) pasVal, modeGraduationY_);
          } else if (modeGraduationY_ == EGAxe.MANUEL_LONGUEURPAS || modeGraduationY_ == EGAxe.AUTO_LONGUEURPAS) {
            r |= axe.setBounds(min, max, pasVal, modeGraduationY_);
          }
        } else {
          r |= axe.setBounds(min, max, -1, modeGraduationY_);
        }

        TraceLigneModel model = modelGraduationsY_.getNewData();
        if (!axe.traceGraduations_.equals(model)) {
          axe.traceGraduations_ = model;
          r = true;
        }
        model = modelSousGraduationsY_.getNewData();
        if (!axe.traceSousGraduations_.equals(model)) {
          axe.traceSousGraduations_ = model;
          r = true;
        }

        if (axe.isTraceGrille() != boxAfficheGraduationsY_.isSelected()) {
          axe.setTraceGrille(boxAfficheGraduationsY_.isSelected());
          r = true;
        }
        if (axe.isTraceSousGrille() != boxAfficheSousGraduationsY_.isSelected()) {
          axe.setTraceSousGrille(boxAfficheSousGraduationsY_.isSelected());
          r = true;
        }
        final String titre = titreAxeYNom.getText();
        if (titre != null) {
          if (axe.titre_ != null && !titre.equals(axe.titre_)) {
            axe.titre_ = titre;
            r = true;
          }
        }
        final boolean titreVisible = titreAxeYVisible.isSelected();
        if (titreVisible != axe.isTitreVisible()) {
          axe.setTitreVisible(titreVisible);
          r = true;
        }
        if (axeYVisible.isSelected() != axe.isVisible()) {
          axe.setVisible(axeYVisible.isSelected());
          r = true;
        }
        final boolean uniteVisible = uniteAxeYVisible.isSelected();
        if (uniteVisible != axe.uniteVisible_) {
          axe.uniteVisible_ = uniteVisible;
          r = true;
        }
        final String unite = uniteAxeY_.getText();
        if (unite != null) {
          if (axe.unite_ != null && !unite.equals(axe.unite_)) {
            axe.unite_ = unite;

            r = true;
          }
        }
        if ((Souspas.length() > 0)) {
          if (axe.nbSousGraduations_ != pasSousVal) {
            r = true;
          }
          axe.nbSousGraduations_ = pasSousVal;

        }

      }
    }
    if (cbUseSameAxe_.isEnabled() && cbUseSameAxe_.isSelected()) {
      CtuluRange range = null;
      r = true;
      if (maxEdited && minEdited) {
        range = new CtuluRange();
        range.max_ = maxVal;
        range.min_ = minVal;
      }
      graphe_.useOneAxeVertical(axes_, titreAxeYNom.getText(), range);
    }
    if (cbInitialConf_.isEnabled() && cbInitialConf_.isSelected()) {
      if (graphe_.useVerticalAxisInitialConf(targets_)) {
        r = true;
      }
    }

    return r;
  }

  private void buildXPanel(final boolean _removeAll) {

    final double max = graphe_.getXMax();
    final double min = graphe_.getXMin();

    // Si l'axe poss�de un formatteur sp�cifique, on affiche le min et max suivant celui ci.
    final String minTxt = EbliLib.getS("Min:") + "(>" + (axeX_.isSpecificFormat() ? axeX_.getSpecificFormat().format(min) : formatter.format(min)) + ")";
    final String maxTxt = EbliLib.getS("Max:") + "(<" + (axeX_.isSpecificFormat() ? axeX_.getSpecificFormat().format(max) : formatter.format(max)) + ")";
    final String typePasTxt = EbliLib.getS("Type Graduation:");
    if (_removeAll) {
      pnAxeX.removeAll();
    }
    // tooltip commun
    final String tooltipTxt = EbliLib.getS("Utiliser une valeur vide pour garder la valeur initiale");
    final String tooltipPas = EbliLib.getS("Nombre maximal de graduations principales");
    // les text field
    tfHorMax_ = editorH_.createEditorComponent();
    tfHorMax_.setToolTipText(tooltipTxt);
    tfHorMin_ = editorH_.createEditorComponent();
    tfHorMin_.setToolTipText(tooltipTxt);

    if (tfHorPas_ == null) {
      tfHorPas_ = BuTextField.createDoubleField();
      tfHorPas_.setValueValidator(BuValueValidator.MIN(0));
      tfHorPas_.setColumns(10);
      tfHorPas_.setToolTipText(tooltipPas);
      tfHorPas_.addKeyListener(this);

      if (axeX_.getModeGraduations() == EGAxe.AUTO_GRADUATIONS || modeGraduationX_ == EGAxe.MANUEL_GRADUATIONS
              || modeGraduationX_ == EGAxe.LOGARITHMIQUE) {
        tfHorPas_.setText("" + axeX_.nbPas_);
      } else if (axeX_.getModeGraduations() == EGAxe.AUTO_LONGUEURPAS || modeGraduationX_ == EGAxe.MANUEL_LONGUEURPAS) {
        tfHorPas_.setText("" + axeX_.longueurPas_);
      }

    }

    if (tfHorSousPas_ == null) {
      tfHorSousPas_ = BuTextField.createIntegerField();
      tfHorSousPas_.setValueValidator(BuValueValidator.INTEGER);
      tfHorSousPas_.setColumns(10);
      tfHorSousPas_.setToolTipText(tooltipPas);
      tfHorSousPas_.addKeyListener(this);

    }

    tfHorMax_.addKeyListener(this);
    tfHorMin_.addKeyListener(this);
    if (axeXVisible == null) {
      axeXVisible = new BuCheckBox();
      axeXVisible.setSelected(axeX_.isVisible());
    }
    // -- titre axe horizontal --//
    createAxeXTitleComponents();
    createAxeXUnitComponents();

    pnAxeX.add(new BuLabel(EbliLib.getS("Afficher l'axe")));
    pnAxeX.add(axeXVisible);

    pnAxeX.add(createTitleNameLabel());
    pnAxeX.add(createPanel(titreAxeXVisible, titreAxeXNom));

    pnAxeX.add(createUnitNameLabel());
    pnAxeX.add(createPanel(uniteAxeXVisible, uniteAxeXNom));

    // borne inf
    BuLabel lb = new BuLabel(minTxt);
    lb.setToolTipText(EbliLib.getS("Borne inf�rieure de l'axe horizontal"));
    pnAxeX.add(lb);
    pnAxeX.add(tfHorMin_);
    // borne sup
    lb = new BuLabel(maxTxt);
    lb.setToolTipText(EbliLib.getS("Borne sup�rieure de l'axe horizontal"));
    pnAxeX.add(lb);
    pnAxeX.add(tfHorMax_);

    // --prendre en compte les limites max --//
    pnAxeX.add(new BuLabel());
    if (boxChooseUserLimitX_ == null) {
      boxChooseUserLimitX_ = new BuCheckBox(EbliLib.getS("Afficher uniquement jusqu'aux limites"));
      boxChooseUserLimitX_.setToolTipText(EbliLib.getS("Si s�lectionn�, les donn�es de la courbe ne pourront d�passer les valeurs pr�c�dentes"));
    }
    pnAxeX.add(boxChooseUserLimitX_);
    boxChooseUserLimitX_.setSelected(true);

    // -- selection du type de graduations
    if (comboHorizontal_ == null) {
      comboHorizontal_ = new BuComboBox(new String[]{EbliLib.getS("Manuel: Nombre de graduations"),
        EbliLib.getS("Manuel: longueur des pas"), EbliLib.getS("Automatique: Nombre de graduations"),
        EbliLib.getS("Automatique: Longueur des pas"), EbliLib.getS("Logarithmique")});
      lb = new BuLabel(typePasTxt);
      // -- listener de la combo --//
      comboHorizontal_.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          modeGraduationX_ = comboHorizontal_.getSelectedIndex();

          if (modeGraduationX_ == EGAxe.AUTO_GRADUATIONS || modeGraduationX_ == EGAxe.AUTO_LONGUEURPAS
                  || modeGraduationX_ == EGAxe.LOGARITHMIQUE) {
            tfHorSousPas_.setEnabled(false);
          } else {
            tfHorSousPas_.setEnabled(true);
          }

          if (modeGraduationX_ == EGAxe.AUTO_GRADUATIONS || modeGraduationX_ == EGAxe.MANUEL_GRADUATIONS) {
            tfHorPas_.setText("" + axeX_.nbPas_);
          } else if (modeGraduationX_ == EGAxe.AUTO_LONGUEURPAS || modeGraduationX_ == EGAxe.MANUEL_LONGUEURPAS) {
            tfHorPas_.setText("" + axeX_.longueurPas_);
            // int mode = modeGraduationX_;
            // axeH_.modeGraduations_ = modeGraduationX_;
          }

          // graphe_.axeUpdated();
        }
      });
    }

    pnAxeX.add(comboHorizontal_);
    // -- affichage ou non des checkbox --//

    boxAfficheGraduationsX_.setSelected(axeX_.traceGrille_);
    boxAfficheSousGraduationsX_.setSelected(axeX_.traceSousGrille_);
    pnAxeX.add(tfHorPas_);

    pnAxeX.add(new JLabel(EbliResource.EBLI.getString("Sous graduations:")));
    pnAxeX.add(tfHorSousPas_);

    // -- graduations --//
    modelGraduationsX_ = new BSelecteurLineModel(GRILLE_AXE_X, axeX_.traceGraduations_);
    pnAxeX.add(boxAfficheGraduationsX_);

    pnAxeX.add(modelGraduationsX_.buildPanel());

    // -- sous graduations --//
    modelSousGraduationsX_ = new BSelecteurLineModel(SOUS_GRILLE_AXE_X, axeX_.traceSousGraduations_);
    // modelSousGraduationsX_.setSelecteurTarget(this);
    pnAxeX.add(boxAfficheSousGraduationsX_);
    pnAxeX.add(modelSousGraduationsX_.buildPanel());

  }

  private void buildYPanel(final boolean _removeAll) {
    if (_removeAll) {
      pnAxeY_.removeAll();
    }

    final String minTxt = EbliLib.getS("Min:");
    final String maxTxt = EbliLib.getS("Max:");
    final String pasTxt = EbliLib.getS("Graduation:");
    final String tooltipPas = EbliLib.getS("Nombre maximal de graduations principales");
    final String tooltipTxt = "<html>"
            + EbliLib.getS("Si plusieurs axes sont s�lectionn�s, ce champ est vide si les valeurs ne sont pas identiques")
            + "<br>" + EbliLib.getS("Utiliser une valeur vide pour garder la valeur initiale") + "</html>";
    // les textes fields
    if (editorV_ == null) {
      tfVertMax_ = new BuLabel(EbliLib.getS("Non �ditable"));
      tfVertMin_ = new BuLabel(EbliLib.getS("Non �ditable"));
    } else {
      tfVertMax_ = editorV_.createEditorComponent();
      tfVertMin_ = editorV_.createEditorComponent();
      // tfVertMax_.addKeyListener(this);
      // tfVertMin_.addKeyListener(this);
    }
    tfVertMax_.setToolTipText(tooltipTxt);
    tfVertMin_.setToolTipText(tooltipTxt);
    if (tfVertPas_ == null) {
      tfVertPas_ = BuTextField.createDoubleField();
      tfVertPas_.setToolTipText(tooltipPas);
      tfVertPas_.setValueValidator(tfHorPas_.getValueValidator());
      tfVertPas_.setColumns(10);
      // tfVertPas_.addKeyListener(this);
    }

    if (tfVertSousPas_ == null) {
      tfVertSousPas_ = BuTextField.createIntegerField();
      tfVertSousPas_.setToolTipText(tooltipPas);
      tfVertSousPas_.setValueValidator(BuValueValidator.INTEGER);
      tfVertSousPas_.setColumns(10);
      // tfVertSousPas_.addKeyListener(this);
    }

    // -- titre axe horizontal --//
    EGAxe axeChoisi = null;
    if (axes_ != null && axes_.length == 1) {
      axeChoisi = axes_[0];
    }
    createAxeYTitleComponents();

    final boolean enabled = axeChoisi != null;
    if (enabled) {
      titreAxeYVisible.setSelected(axeChoisi.titreVisible_);
    }
    createAxeYUnitComponents();

    if (axeYVisible == null) {
      axeYVisible = new BuCheckBox();
    }

    titreAxeYNom.setEnabled(enabled);
    uniteAxeYVisible.setEnabled(enabled);
    axeYVisible.setEnabled(enabled);
    uniteAxeY_.setEnabled(enabled);
    if (enabled) {
      titreAxeYNom.setText(axeChoisi.titre_);
      uniteAxeYVisible.setSelected(axeChoisi.uniteVisible_);
      axeYVisible.setSelected(axeChoisi.isVisible());
      titreAxeYNom.setEnabled(titreAxeYVisible.isSelected() && axeChoisi.isTitleModifiable());
      uniteAxeY_.setEnabled(uniteAxeYVisible.isSelected() && axeChoisi.isUniteModifiable());
      uniteAxeY_.setText(axeChoisi.unite_);
    }

    pnAxeY_.add(new BuLabel(EbliLib.getS("Afficher l'axe")));
    pnAxeY_.add(axeYVisible);
    pnAxeY_.add(createTitleNameLabel());
    pnAxeY_.add(createPanel(titreAxeYVisible, titreAxeYNom));
    pnAxeY_.add(createUnitNameLabel());
    pnAxeY_.add(createPanel(uniteAxeYVisible, uniteAxeY_));

    // vertical min
    BuLabel lb = new BuLabel(minTxt);
    lb.setToolTipText(EbliLib.getS("Borne inf�rieure des axes verticaux"));
    pnAxeY_.add(lb);
    pnAxeY_.add(tfVertMin_);
    // vertical max
    lb = new BuLabel(maxTxt);
    lb.setToolTipText(EbliLib.getS("Borne sup�rieure des axes verticaux"));
    pnAxeY_.add(lb);
    pnAxeY_.add(tfVertMax_);

    // --prendre en compte les limites max --//
    pnAxeY_.add(new BuLabel());
    if (boxChooserUserLimitY == null) {
      boxChooserUserLimitY = new BuCheckBox(EbliLib.getS("Affiche uniquement jusqu'aux limites"));
      boxChooserUserLimitY.setToolTipText(EbliLib.getS("Si s�lectionn�, les donn�es de la courbe ne pourront d�passer les valeurs pr�c�dentes"));
    }
    pnAxeY_.add(boxChooserUserLimitY);
    boxChooserUserLimitY.setSelected(true);

    // vertical pas
    lb = new BuLabel(pasTxt);
    lb.setToolTipText(tooltipPas);
    // pnAxeV_.add(lb);

    // -- selection du type de graduations
    comboVertical_ = new BuComboBox(new String[]{EbliLib.getS("Manuel: Nombre de graduations"),
      EbliLib.getS("Manuel: longueur des pas"), EbliLib.getS("Automatique: Nombre de graduations"),
      EbliLib.getS("Automatique: Longueur des pas"), EbliLib.getS("Logarithmique")}) {
      @Override
      public void setSelectedIndex(final int anIndex) {
        super.setSelectedIndex(anIndex);

      }
    };

    pnAxeY_.add(comboVertical_);
    pnAxeY_.add(tfVertPas_);

    pnAxeY_.add(new JLabel(EbliResource.EBLI.getString("Sous graduations:")));
    pnAxeY_.add(tfVertSousPas_);
    // -- listener de la combo --//
    comboVertical_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        updateModeGraduationForY();
      }
    });
    updateModeGraduationForY();
    if (axeChoisi == null && axes_ != null && axes_.length > 0) {
      axeChoisi = axes_[0];
    }

    if (enabled) {
      final int mode = axeChoisi.getModeGraduations();
      if (mode == EGAxe.AUTO_GRADUATIONS || mode == EGAxe.MANUEL_GRADUATIONS || mode == EGAxe.LOGARITHMIQUE) {
        tfVertPas_.setText("" + axeChoisi.nbPas_);
      } else if (mode == EGAxe.AUTO_LONGUEURPAS || mode == EGAxe.MANUEL_LONGUEURPAS) {
        tfVertPas_.setText("" + axeChoisi.longueurPas_);
      }
    }

    // -- affichage ou non des checkbox --//
    if (enabled) {
      boxAfficheGraduationsY_.setSelected(axeChoisi.traceGrille_);
    }
    if (enabled) {
      boxAfficheSousGraduationsY_.setSelected(axeChoisi.traceSousGrille_);
    }
    if (enabled) {
      modelGraduationsY_ = new BSelecteurLineModel(GRILLE_AXE_Y, axeChoisi.traceGraduations_);
    } else {
      modelGraduationsY_ = new BSelecteurLineModel(GRILLE_AXE_Y);
    }
    modelGraduationsY_.setSelecteurTarget(this);
    pnAxeY_.add(boxAfficheGraduationsY_);
    pnAxeY_.add(modelGraduationsY_.buildPanel());

    // -- sous graduations --//
    if (enabled) {
      modelSousGraduationsY_ = new BSelecteurLineModel(SOUS_GRILLE_AXE_Y, axeChoisi.traceSousGraduations_);
    } else {
      modelSousGraduationsY_ = new BSelecteurLineModel(SOUS_GRILLE_AXE_Y);
    }

    // modelSousGraduationsY_.setSelecteurTarget(this);
    pnAxeY_.add(boxAfficheSousGraduationsY_);
    pnAxeY_.add(modelSousGraduationsY_.buildPanel());

    // same axis
    if (cbUseSameAxe_ == null) {
      cbUseSameAxe_ = new BuCheckBox(EbliLib.getS("Utiliser le m�me axe"));
      cbUseSameAxe_.setToolTipText(EbliLib.getS("Si s�lectionn�, toutes courbes s�lectionn�es utiliseront le m�me axe"));
      // cbUseSameAxe_.addItemListener(this);
    }
    pnAxeY_.add(new BuLabel());
    pnAxeY_.add(cbUseSameAxe_);
    // configuration initiale
    if (cbInitialConf_ == null) {
      cbInitialConf_ = new BuCheckBox(EbliLib.getS("Axes: configuration intiale"));
      cbInitialConf_.setToolTipText(EbliLib.getS("Si s�lectionn�, la configuration initiale des axes s�lectionn�s sera r�tablie"));
      // cbInitialConf_.addItemListener(this);
    }
    pnAxeY_.add(new BuLabel());
    pnAxeY_.add(cbInitialConf_);
  }

  private void createAxeXTitleComponents() {
    if (titreAxeXNom == null) {
      titreAxeXNom = new BuTextField();
    }
    if (titreAxeXVisible == null) {
      titreAxeXVisible = new BuCheckBox();
      titreAxeXVisible.setToolTipText(EbliLib.getS("Affichage ou non du titre de l'axe"));
      titreAxeXVisible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          titreAxeXNom.setEnabled(titreAxeXVisible.isSelected() && axeX_.isTitleModifiable());
        }
      });
    }
    titreAxeXVisible.setSelected(axeX_.titreVisible_);

    if (axeX_.titre_ != null) {
      titreAxeXNom.setText(axeX_.titre_);
    }
    titreAxeXNom.setEnabled(titreAxeXVisible.isSelected() && axeX_.isTitleModifiable());
  }

  private void createAxeXUnitComponents() {
    if (uniteAxeXNom == null) {
      uniteAxeXNom = new BuTextField();
    }
    if (uniteAxeXVisible == null) {
      uniteAxeXVisible = new BuCheckBox();
      uniteAxeXVisible.setToolTipText(EbliLib.getS("Affichage ou non de l'unit�"));
      uniteAxeXVisible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          uniteAxeXNom.setEnabled(uniteAxeXVisible.isSelected() && axeX_.isUniteModifiable());
        }
      });
    }

    uniteAxeXVisible.setSelected(axeX_.uniteVisible_);
    uniteAxeXNom.setEnabled(uniteAxeXVisible.isSelected() && axeX_.isUniteModifiable());
    if (axeX_.unite_ != null) {
      uniteAxeXNom.setText(axeX_.unite_);
    }
  }

  private void createAxeYTitleComponents() {
    if (titreAxeYNom == null) {
      titreAxeYNom = new BuTextField();
    }
    titreAxeYNom.setText(CtuluLibString.EMPTY_STRING);
    if (titreAxeYVisible == null) {
      titreAxeYVisible = new BuCheckBox();
      titreAxeYVisible.setToolTipText(EbliLib.getS("Affichage ou non du titre de l'axe"));
      titreAxeYVisible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          updateTitreAxeY();
        }
      });
    }
    updateTitreAxeY();
  }

  protected void updateTitreAxeY() {
    final EGAxe selectedAxeY = getSelectedAxeY();
    titreAxeYNom.setEnabled(titreAxeYVisible.isSelected() && selectedAxeY != null && selectedAxeY.isTitleModifiable());
  }

  private void createAxeYUnitComponents() {
    if (uniteAxeY_ == null) {
      uniteAxeY_ = new BuTextField();
    }
    uniteAxeY_.setText(CtuluLibString.EMPTY_STRING);
    if (uniteAxeYVisible == null) {
      uniteAxeYVisible = new BuCheckBox();
      uniteAxeYVisible.setToolTipText(EbliLib.getS("Affichage ou non de l'unit�"));
      uniteAxeYVisible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          updateUniteAxeY();
        }
      });
    }
    updateUniteAxeY();
  }

  protected void updateUniteAxeY() {
    final EGAxe selectedAxeY = getSelectedAxeY();
    uniteAxeY_.setEnabled(uniteAxeYVisible.isSelected() && selectedAxeY != null && selectedAxeY.isUniteModifiable());
  }

  private BuLabel createTitleNameLabel() {
    return new BuLabel(EbliResource.EBLI.getString("Nom du titre"));
  }

  private BuLabel createUnitNameLabel() {
    return new BuLabel(EbliResource.EBLI.getString("Nom de l'unit�"));
  }

  private CtuluValueEditorI getEditorFor(final EGAxe _a) {
    CtuluValueEditorI r = _a == null ? null : _a.getValueEditor();
    if (r == null) {
      r = DEFAULT_EDITOR;
    }
    return r;
  }

  @Override
  public Object getMin(final String _key) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Object getMoy(final String _key) {
    // TODO Auto-generated method stub
    return null;
  }

  private String getPas(final EGAxeVertical firstAxe) {
    if (firstAxe.getModeGraduations() == EGAxe.AUTO_GRADUATIONS
            || firstAxe.getModeGraduations() == EGAxe.MANUEL_GRADUATIONS) {
      return CtuluLibString.getString(firstAxe.getNbPas());
    } else {
      return Double.toString(firstAxe.longueurPas_);
    }
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key.equals(GRILLE_AXE_X)) {
      return axeX_.traceGraduations_;
    }
    if (_key.equals(SOUS_GRILLE_AXE_X)) {
      return axeX_.traceSousGraduations_;
    }
    final EGAxe axeChoisi = getSelectedAxeY();

    if (axeChoisi == null) {
      // if(axes_==null || axes_.length==0)
      return null;
    }
    if (_key.equals(GRILLE_AXE_Y)) {
      return axeChoisi.traceGraduations_;
    }
    if (_key.equals(SOUS_GRILLE_AXE_Y)) {
      return axeChoisi.traceSousGraduations_;
    }

    return null;
  }

  @Override
  public void keyPressed(final KeyEvent _e) {
  }

  @Override
  public void keyReleased(final KeyEvent _e) {
    if (_e.getKeyCode() == KeyEvent.VK_ENTER) {
      apply();
    }
  }

  @Override
  public void keyTyped(final KeyEvent _e) {
  }

  private void setAxeTargets() {
    this.updateAxesValue();
    final boolean enable = axes_ != null;
    for (int i = pnAxeY_.getComponentCount() - 1; i >= 0; i--) {
      final JComponent c = (JComponent) pnAxeY_.getComponent(i);
      for (int j = c.getComponentCount() - 1; j >= 0; j--) {
        c.getComponent(j).setEnabled(enable);
      }
    }
  }

  @Override
  public boolean setProperty(final String _key, final Object prop) {
    boolean ok = false;
    if (_key.equals(GRILLE_AXE_X)) {
      axeX_.traceGraduations_.updateData((TraceLigneModel) prop);
      ok = true;
    }
    if (_key.equals(SOUS_GRILLE_AXE_X)) {
      axeX_.traceSousGraduations_.updateData((TraceLigneModel) prop);
      ok = true;
    }
    if (ok) {
      graphe_.axeUpdated();
    }

    if (axes_ == null || axes_.length == 0) {
      return false;
    }
    if (_key.equals(GRILLE_AXE_Y)) {
      for (int i = 0; i < axes_.length; i++) {
        axes_[i].traceGraduations_.updateData((TraceLigneModel) prop);
      }
      ok = true;
    }
    if (_key.equals(SOUS_GRILLE_AXE_Y)) {
      for (int i = 0; i < axes_.length; i++) {
        axes_[i].traceSousGraduations_.updateData((TraceLigneModel) prop);
      }
      ok = true;
    }
    if (ok) {
      graphe_.axeUpdated();
    }

    return false;
  }

  public final void setTargets(final EGObject[] _a) {
    targets_ = _a;
    // updateAxesFromObject();
    setAxeTargets();
  }

  void updateAxeHorValue() {
    final double max = axeX_.getMaximum();
    final double min = axeX_.getMinimum();
    editorH_.setValue(CtuluLib.getDouble(max), tfHorMax_);
    editorH_.setValue(CtuluLib.getDouble(min), tfHorMin_);

    final int modeGraduations = axeX_.getModeGraduations();
    comboHorizontal_.setSelectedIndex(modeGraduations);
    if (modeGraduations == EGAxe.AUTO_GRADUATIONS || modeGraduations == EGAxe.MANUEL_GRADUATIONS
            || modeGraduations == EGAxe.LOGARITHMIQUE) {
      tfHorPas_.setText(CtuluLibString.getString(axeX_.getNbPas()));
    } else {
      tfHorPas_.setText("" + axeX_.longueurPas_);
    }

    tfHorSousPas_.setText("" + axeX_.nbSousGraduations_);

  }

  private void updateAxesFromObject() {
    axes_ = null;
    final Set s = new HashSet();
    if (targets_ != null) {
      for (int i = targets_.length - 1; i >= 0; i--) {
        final EGAxeVertical axeY = targets_[i].getAxeY();
        if (axeY != null) {
          s.add(axeY);
        }
      }
    }
    setTargetsAxis(s);
  }

  

  protected void setTargetsAxis(final Set s) {
    if (s.size() > 0) {
      axes_ = new EGAxeVertical[s.size()];
      s.toArray(axes_);
    }
    final int nb = axes_ == null ? 0 : axes_.length;
    // on cherche l'editor commun
    final CtuluValueEditorI oldEditor = editorV_;
    if (nb > 0) {
      // on initialise l'editeur avec une valeur non nulle.
      editorV_ = getEditorFor(axes_[0]);
      // si l'ancien editeur est compatible avec le nouveau on le garde
      if (oldEditor != null && oldEditor.getClass().equals(editorV_.getClass())) {
        editorV_ = oldEditor;
      }
      for (int i = 1; i < nb && editorV_ != null; i++) {
        final CtuluValueEditorI vi = getEditorFor(axes_[i]);
        if (!vi.getClass().equals(editorV_.getClass())) {
          editorV_ = null;
        }
      }
    }
    // if (oldEditor != editorV_) {
    CtuluLibMessage.debug("panel V rebuilt");
    buildYPanel(true);
    revalidate();
    // }
    pnAxeY_.setToolTipText(EbliLib.getS("Nombre d'axes: {0}", CtuluLibString.getString(nb)));
  }

  void updateAxesValue() {
    updateAxesFromObject();
    String min = CtuluLibString.EMPTY_STRING;
    String max = min;
    String pas = min;
    int mode = 0;
    if (axes_ != null && axes_.length > 0) {
      boolean minIsCommon = true;
      boolean maxIsCommon = true;
      boolean pasIsCommon = true;
      boolean modeIsCommon = true;

      final EGAxeVertical firstAxe = axes_[0];
      final double axeMin = firstAxe.getMinimum();
      min = Double.toString(axeMin);
      final double axeMax = firstAxe.getMaximum();
      max = Double.toString(axeMax);
      mode = firstAxe.getModeGraduations();
      pas = getPas(firstAxe);
      tfVertSousPas_.setText("" + firstAxe.nbSousGraduations_);

      for (int i = 1; i < axes_.length && (minIsCommon || maxIsCommon || pasIsCommon || modeIsCommon); i++) {
        if (minIsCommon && axes_[i].getMinimum() != axeMin) {
          min = CtuluLibString.EMPTY_STRING;
          minIsCommon = false;
        }
        if (maxIsCommon && axes_[i].getMaximum() != axeMax) {
          max = CtuluLibString.EMPTY_STRING;
          maxIsCommon = false;
        }
        if (pasIsCommon) {
          final String pasi = getPas(axes_[i]);
          if (!pasi.equals(pas)) {
            pasIsCommon = false;
            pas = CtuluLibString.EMPTY_STRING;
          }
        }
        modeIsCommon = axes_[i].getModeGraduations() == mode;
      }

    }
    this.comboVertical_.setSelectedIndex(mode);
    tfVertPas_.setText(pas);
    if (editorV_ != null) {
      editorV_.setValue(max, tfVertMax_);
      editorV_.setValue(min, tfVertMin_);
    }

    updateAxeHorValue();
    cbUseSameAxe_.setEnabled(axes_ != null && axes_.length > 1);
    cbUseSameAxe_.setSelected(false);
    cbInitialConf_.setEnabled(graphe_.isVerticalAxisConfigurationMod(targets_));
    cbInitialConf_.setSelected(false);
  }

  private void updateModeGraduationForY() {
    modeGraduationY_ = comboVertical_.getSelectedIndex();
    if (modeGraduationY_ == EGAxe.AUTO_GRADUATIONS || modeGraduationY_ == EGAxe.AUTO_LONGUEURPAS) {
      tfVertSousPas_.setEnabled(false);
    } else {
      tfVertSousPas_.setEnabled(true);
    }
    EGAxe axeChoisi = CtuluLibArray.isEmpty(axes_) ? null : axes_[0];
    if (graphe_.getSelectedComponent() != null) {
      axeChoisi = graphe_.getSelectedComponent().getAxeY();
    }
    if (axeChoisi == null && axes_ != null && axes_.length > 0) {
      axeChoisi = axes_[0];
    }
    if (axeChoisi != null) {
      final int modeGraduations = axeChoisi.getModeGraduations();
      if (modeGraduations == EGAxe.AUTO_GRADUATIONS || modeGraduations == EGAxe.MANUEL_GRADUATIONS
              || modeGraduations == EGAxe.LOGARITHMIQUE) {
        tfVertPas_.setText("" + axeChoisi.nbPas_);
      } else if (modeGraduations == EGAxe.AUTO_LONGUEURPAS || modeGraduations == EGAxe.MANUEL_LONGUEURPAS) {
        tfVertPas_.setText("" + axeChoisi.longueurPas_);
      }

    }
  }
}
