package org.fudaa.ebli.courbe;

import com.memoire.fu.FuLog;
import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.dodico.fortran.DodicoDoubleArrayBinaryFileSaver;

public abstract class EGPersistBuilder<T extends EGObject, P extends EGPersist> {
  
  public EGPersistBuilder() {
  }
  
  protected abstract T createEGObject(P target, Map params, CtuluAnalyze log);

  /**
   *
   * @param object
   * @param graphe
   * @param binFile can be used to store specific data. In general, it's the x,y values.
   * @return
   */
  public P persistObject(T object, EGGraphe graphe, File binFile) {
    P res = createPersistUnit(object);
    res.builderClass = getClass().getName();
    postCreatePersist(res, object, graphe, binFile);
    return res;
    
  }
  
  protected abstract P createPersistUnit(T object);
  
  protected void postCreatePersist(P res, T object, EGGraphe graphe, File binFile) {
  }
  
  protected void postRestore(T egObject, P persit, Map params, CtuluAnalyze log) {
  }
  
  protected final T restoreObject(P persist, Map params, CtuluAnalyze log, File binFile) {
    if (binFile != null && binFile.exists()) {
      DodicoDoubleArrayBinaryFileSaver saver = new DodicoDoubleArrayBinaryFileSaver(binFile);
      try {
        Map<String, double[]> load = saver.load();
        if (load != null) {
          params.putAll(load);
        }
      } catch (Exception e) {
        FuLog.error(e);
      }
    }
    T res = createEGObject(persist, params, log);
    postRestore(res, persist, params, log);
    return res;
  }
}
