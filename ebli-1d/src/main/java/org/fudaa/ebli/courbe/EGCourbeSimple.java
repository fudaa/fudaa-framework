/*
 * @creation 6 juil. 2004
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Font;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

/**
 * @author Fred Deniger
 * @version $Id: EGCourbeSimple.java,v 1.11 2007-05-04 13:49:41 deniger Exp $
 */
public class EGCourbeSimple extends EGCourbe {

  EGParent eventDispatcher_;
  EGAxeVertical v_;

  /**
   * @param _v l'axe vertical utilise pour l'afficage
   * @param _model le modele.
   */
  public EGCourbeSimple(final EGAxeVertical _v, final EGModel _model) {
    super(_model);
    v_ = _v;
  }

  @Override
  void fillWithInfo(final InfoData _table, final CtuluListSelectionInterface _selectedPt) {
    super.fillWithInfo(_table, _selectedPt);
    final String pre = EbliLib.getS("Axe vertical:") + CtuluLibString.ESPACE;
    final EGAxeVertical axe = getAxeY();
    if (_table != null && axe != null) {
      _table.put(pre + EbliLib.getS("Borne min"), axe.getStringAffiche(axe.getMinimum()));
      _table.put(pre + EbliLib.getS("Borne max"), axe.getStringAffiche(axe.getMaximum()));
    }
  }

  public void initGraphicConfigurationFrom(EGCourbeSimple courbe) {
    EGCourbeSimplePersistBuilderDefault builder = new EGCourbeSimplePersistBuilderDefault();
    EGCourbePersist createPersistUnit = builder.persistGraphicsData(courbe);
    if (createPersistUnit != null) {
      builder.initGraphicConfiguration(this, createPersistUnit);
    }
  }

  @Override
  public EGCourbePersist getPersitUiConfig() {
    EGCourbeSimplePersistBuilderDefault builder = new EGCourbeSimplePersistBuilderDefault();
    return builder.persistGraphicsData(this);
  }

  @Override
  public void applyPersitUiConfig(EGCourbePersist persist) {
    if (persist != null) {
      new EGCourbeSimplePersistBuilderDefault().initGraphicConfiguration(this, persist);
    }
  }

  @Override
  boolean setYaxe(final EGAxeVertical _v) {
    if (_v != v_) {
      v_ = _v;
      return true;
    }
    return false;
  }

  @Override
  protected EGParent getEGParent() {
    return eventDispatcher_;
  }

  @Override
  public EGAxeVertical getAxeY() {
    return v_;
  }

  public void setParent(final EGParent _eventDispatcher) {
    eventDispatcher_ = _eventDispatcher;
  }

  @Override
  public EGCourbe duplicate(EGParent newParent, EGGrapheDuplicator _duplicator) {
    EGCourbeSimple duplic = new EGCourbeSimple(this.v_.duplicate(), this.getModel().duplicate());

    duplic.isVisible_ = this.isVisible_;

    duplic.displayTitleOnCurve_ = this.displayTitleOnCurve_;
    duplic.font_ = new Font(this.font_.getFamily(), this.font_.getStyle(), this.font_.getSize());
    duplic.tbox_ = this.tbox_.duplicate();

    // -- duplication du egCourbeSurfacePainter --//
    duplic.surfacePainter_ = this.surfacePainter_.duplicate(duplic, _duplicator);

    return duplic;
  }
}
