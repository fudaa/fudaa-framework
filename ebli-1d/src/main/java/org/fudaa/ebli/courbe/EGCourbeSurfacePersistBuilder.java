package org.fudaa.ebli.courbe;

import java.util.Map;
import org.fudaa.ebli.courbe.EGCourbeSurfacePainter.SelectedIndex;

public class EGCourbeSurfacePersistBuilder {

  public EGCourbeSurfacePersist build(final EGCourbeSurfacePainter painter, Map<EGCourbe, Integer> idByCourbe) {
    final EGCourbeSurfacePersist res = new EGCourbeSurfacePersist();
    res.alpha = painter.alpha_;
    res.downColor = painter.down_;
    res.upColor = painter.up_;
    res.palier = painter.getPalier();
    res.surfaceType = painter.getSelectedPainter();
    res.minType = getSelectedIndex(painter.getMinIdx(), true);
    res.maxType = getSelectedIndex(painter.getMaxIdx(), true);
    if (painter.getDelegateCourbe() != null) {
      res.courbeId = idByCourbe.get(painter.getDelegateCourbe());
    }
    return res;
  }

  public EGCourbeSurfacePainter restore(final EGCourbeSurfacePersist saved, EGCourbe courbe,
      Map<Integer, EGCourbe> courbeById) {
    courbe.createSurface();
    EGCourbeSurfacePainter restored = courbe.getSurfacePainter();
    restored.setAlpha(saved.alpha);
    restored.setDown(saved.downColor);
    restored.setUp(saved.upColor);
    restored.setPalier(saved.palier);
    restored.setMaxIdx(getSelectedIndex(saved.maxType, false, saved.maxIdx));
    restored.setMinIdx(getSelectedIndex(saved.minType, true, saved.minIdx));
    restored.setSelectedPainter(saved.surfaceType);
    if (restored.isCourbePainter() && saved.courbeId != null) {
      restored.setSelectedPainterCourbe(courbeById.get(saved.courbeId));
    }
    return restored;
  }

  private static String getSelectedIndex(final SelectedIndex index, final boolean min) {
    if (index == EGCourbeSurfacePainter.NONE) { return "NONE"; }
    if (index == EGCourbeSurfacePainter.TIME_FOR_MAX) { return "TIME_FOR_MAX"; }
    if (index == EGCourbeSurfacePainter.TIME_FOR_MIN) { return "TIME_FOR_MIN"; }
    return min ? "MIN_CUSTOM" : "MAX_CUSTOM";
  }

  private static SelectedIndex getSelectedIndex(final String key, final boolean min, final int idx) {
    if ("NONE".equals(key)) { return EGCourbeSurfacePainter.NONE; }
    if ("TIME_FOR_MAX".equals(key)) { return EGCourbeSurfacePainter.TIME_FOR_MAX; }
    if ("TIME_FOR_MIN".equals(key)) { return EGCourbeSurfacePainter.TIME_FOR_MIN; }
    if (min) { return EGCourbeSurfacePainter.createSelectedForMin(idx); }
    return EGCourbeSurfacePainter.createSelectedForMax(idx);
  }
}
