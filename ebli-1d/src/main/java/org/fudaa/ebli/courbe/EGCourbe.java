/*
 * @creation 11 d�c. 2003
 * @modification $Date: 2007-05-22 14:19:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.courbe;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.*;
import org.fudaa.ebli.palette.BPaletteInfo;
import org.fudaa.ebli.trace.*;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.List;

/**
 * Cette Courbe suppose que tous les x du mod�le sont rang�es dans l'ordre croissant.
 *
 * @author deniger
 * @version $Id: EGCourbe.java,v 1.32 2007-05-22 14:19:05 deniger Exp $
 */
public abstract class EGCourbe extends EGObject {
  private int alpha_ = 255;
  private final TraceLigneModel lineModel_;
  // private double[] markH_;
  /**
   * la liste des valeurs marquees
   */
  private List<EGCourbeMarqueur> listeMarqueurs_ = null;
  private boolean markMaxLine_;
  private boolean markMinLine_;
  private EGModel model_;
  private TraceLigneModel tLigneMarqueur_;
  boolean displayTitleOnCurve_;
  boolean displayPointLabels_;
  Font font_ = CtuluLibSwing.getMiniFont();
  final TraceIconModel iconeModel_;
  TraceIconModel iconeModelSpecific_;
  private String specificIconTitle = EbliLib.getS("Icone pour points sp�cifiques");
  private boolean useSpecificIcon;
  EGCourbeSurfacePainter surfacePainter_;
  TraceBox tbox_;
  protected TraceBox tboxLabels_;

  /**
   * @param _model le model a prendre en compte
   */
  public EGCourbe(final EGModel _model) {
    iconeModel_ = new TraceIconModel(TraceIcon.RIEN, 1, Color.BLACK);
    iconeModelSpecific_ = new TraceIconModel(TraceIcon.RIEN, 1, Color.BLACK);
    lineModel_ = new TraceLigneModel();
    model_ = _model;
    initLabelsTraceBox(Color.BLACK);
  }

  public boolean isUseSpecificIcon() {
    return useSpecificIcon;
  }

  public String getSpecificIconTitle() {
    return specificIconTitle;
  }

  public void setSpecificIconTitle(String specificIconTitle) {
    this.specificIconTitle = specificIconTitle;
  }

  public void setUseSpecificIcon(boolean useSpecificIcon) {
    this.useSpecificIcon = useSpecificIcon;
  }

  public Collection<EGCourbe> getAssociatesCourbesForExport() {
    return Collections.emptyList();
  }

  private void buildLigneMarqueur() {
    if (tLigneMarqueur_ == null) {
      tLigneMarqueur_ = new TraceLigneModel();
      tLigneMarqueur_.setTypeTrait(TraceLigne.POINTILLE);
      tLigneMarqueur_.setCouleur(lineModel_.getCouleur().brighter());
    }
  }

  public abstract EGCourbePersist getPersitUiConfig();

  public abstract void applyPersitUiConfig(EGCourbePersist persist);

  private TraceLigne getLineMarkeur() {
    buildLigneMarqueur();
    final TraceLigne trMarqueur = new TraceLigne(new TraceLigneModel(tLigneMarqueur_));
    if (EbliLib.isAlphaChanged(alpha_)) {
      trMarqueur.setCouleur(EbliLib.getAlphaColor(tLigneMarqueur_.getCouleur(), alpha_));
    }
    return trMarqueur;
  }

  private void initTraceBox() {
    if (tbox_ == null) {
      tbox_ = new TraceBox();
      buildLigneMarqueur();
      tbox_.setColorBoite(tLigneMarqueur_.getCouleur());
      tbox_.setColorFond(Color.WHITE);
      tbox_.setHMargin(1);
      tbox_.setVMargin(1);
      tbox_.setHPosition(SwingConstants.CENTER);
      tbox_.setVPosition(SwingConstants.BOTTOM);
      tbox_.setColorText(Color.BLACK);
      tbox_.setDrawBox(true);
      tbox_.setDrawFond(true);
    }
  }

  protected void initLabelsTraceBox(Color _c) {
    if (tboxLabels_ == null) {
      tboxLabels_ = new TraceBox();
      tboxLabels_.setColorFond(Color.WHITE);
      tboxLabels_.setHMargin(2);
      tboxLabels_.setVMargin(2);
      tboxLabels_.setHPosition(SwingConstants.CENTER);
      tboxLabels_.setVPosition(SwingConstants.BOTTOM);
      tboxLabels_.setDrawBox(true);
      tboxLabels_.setDrawFond(true);
    }
    tboxLabels_.setColorBoite(_c);
    tboxLabels_.setColorText(_c);
  }

  protected void traceMarks(final Graphics2D _g, final EGRepere _t) {
    final int xmin = _t.getMinEcranX();
    final int xmax = _t.getMaxEcranX();
    final int ymax = _t.getMaxEcranY();

    final Font f = EGGraphe.DEFAULT_FONT;
    final Font old = _g.getFont();
    _g.setFont(f);

    CtuluRange r = null;
    TraceLigne trMarqueur = null;
    if (isMarkMaxLine() || isMarkMinLine()) {
      r = EGCourbeModelDefault.getLocalMinMax(_t.getXAxe(), getModel());
    }
    EGAxeVertical axeY = getAxeY();
    if (r != null && isMarkMinLine() && axeY.containsPoint(r.min_)) {
      trMarqueur = getLineMarkeur();
      final int yie = _t.getYEcran(r.min_, axeY);
      trMarqueur.dessineTrait(_g, xmin, yie, xmax, yie);
      initTraceBox();
      tbox_.setColorBoite(tLigneMarqueur_.getCouleur());
      int x = xmin + 3;
      final String str = axeY.getStringAffiche(r.min_);
      if (axeY.isDroite()) {
        x = xmax - 6 - _g.getFontMetrics().stringWidth(str);
      }
      tbox_.paintBox(_g, x, yie, str);
    }
    if (r != null && isMarkMaxLine() && axeY.containsPoint(r.max_)) {
      if (trMarqueur == null) {
        trMarqueur = getLineMarkeur();
      }
      final int yie = _t.getYEcran(r.max_, axeY);
      trMarqueur.dessineTrait(_g, xmin, yie, xmax, yie);
      initTraceBox();
      tbox_.setColorBoite(tLigneMarqueur_.getCouleur());
      int x = xmin + 3;
      final String str = axeY.getStringAffiche(r.max_);
      if (axeY.isDroite()) {
        x = xmax - 6 - _g.getFontMetrics().stringWidth(str);
      }
      tbox_.paintBox(_g, x, yie, str);
    }
    if (getMarqueurs() != null) {
      // if (trMarqueur == null) {
      // trMarqueur = getLineMarkeur();
      // }
      initTraceBox();

      for (EGCourbeMarqueur marqueur : getMarqueurs()) {
        tbox_.setColorBoite(marqueur.model_.getCouleur());

        // -- tracer marqueurs horitzontaux --//
        if (axeY != null && axeY.containsPoint(marqueur.getValue()) && marqueur.isVisible() && marqueur.traceHorizontal_) {
          final int yie = _t.getYEcran(marqueur.getValue(), axeY);
          int x = xmin + 3;
          final String str;
          if (marqueur.getTitle() != null) {
            str = marqueur.getTitle();
          } else {
            str = axeY.getStringAffiche(marqueur.getValue());
          }

          if (axeY.isDroite()) {
            x = xmax - 6 - _g.getFontMetrics().stringWidth(str);
          }
          new TraceLigne(marqueur.model_).dessineTrait(_g, xmin, yie, xmax, yie);
          tbox_.paintBox(_g, x, yie, str);
        } // -- tracer marqueurs verticaux --//
        else if (getAxeX() != null && getAxeX().containsPoint(marqueur.getValue()) && marqueur.isVisible()
            && !marqueur.traceHorizontal_) {
          final int xie = _t.getXEcran(marqueur.getValue());
          int y = 0 + 6;
          final String str;
          if (marqueur.getTitle() != null) {
            str = marqueur.getTitle();
          } else {
            str = getAxeX().getStringAffiche(marqueur.getValue());
          }
          tbox_.paintBox(_g, xie, y, str);
          new TraceLigne(marqueur.model_).dessineTrait(_g, xie/* +widthBox/2 */, y, xie/* +widthBox/2 */, ymax);
        }
      }
    }

    _g.setFont(old);
  }

  @Override
  void fillWithInfo(final BPaletteInfo.InfoData _table, final CtuluListSelectionInterface _selectedPt) {
    _table.setTitle(EbliLib.getS("Courbe:") + CtuluLibString.ESPACE + getTitle());
    getModel().fillWithInfo(_table, _selectedPt);
    _table.put(EbliLib.getS("Nombre de points"), CtuluLibString.getString(model_.getNbValues()));
    _table.put(EbliLib.getS("Nombre de points s�lectionn�s"), CtuluLibString.getString(_selectedPt == null ? 0
        : _selectedPt.getNbSelectedIndex()));
    _table.put(EbliLib.getS("X min"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(model_.getXMin()));
    _table.put(EbliLib.getS("X max"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(model_.getXMax()));
    _table.put(EbliLib.getS("Y min"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(model_.getYMin()));
    _table.put(EbliLib.getS("Y max"), CtuluLib.DEFAULT_NUMBER_FORMAT.format(model_.getYMax()));
  }

  final boolean isMarkMaxLine() {
    return markMaxLine_;
  }

  final boolean isMarkMinLine() {
    return markMinLine_;
  }

  boolean setLigneType(final TraceLigneModel _data) {
    final boolean res = lineModel_.updateData(_data);
    if (res) {
      fireCourbeAspectChanged(false);
      super.firePropertyChange(BSelecteurLineModel.PROPERTY, null, lineModel_);
    }
    return res;
  }

  final boolean setMarkMaxLine(final boolean _markMaxLine) {
    if (_markMaxLine != markMaxLine_) {
      markMaxLine_ = _markMaxLine;
      fireCourbeAspectChanged(false);
      firePropertyChange(EGCourbeConfigureTarget.PROP_MARK_MAX, markMaxLine_);
      return true;
    }
    return false;
  }

  final boolean setMarkMinLine(final boolean _markMinLine) {
    if (markMinLine_ != _markMinLine) {
      markMinLine_ = _markMinLine;
      fireCourbeAspectChanged(false);
      firePropertyChange(EGCourbeConfigureTarget.PROP_MARK_MIN, markMinLine_);
      return true;
    }
    return false;
  }

  protected int getAlpha() {
    return alpha_;
  }

  protected abstract EGParent getEGParent();

  public abstract EGCourbe duplicate(EGParent newParent, EGGrapheDuplicator _duplicator);

  protected Font getFont() {
    return font_;
  }

  @Override
  public boolean isDisplayed() {
    if (getEGParent() == null) {
      return isVisible_;
    }
    return isVisible_ && getEGParent().isDisplayed();
  }

  protected boolean isDisplayTitleOnCurve() {
    return displayTitleOnCurve_;
  }

  public boolean isDisplayPointLabels() {
    return displayPointLabels_;
  }

  protected boolean isSelected(final double _xToTest, final double _yToTest, final int _x, final int _y,
                               final EGRepere _t, final EGAxeVertical _yaxe, final int _precision) {
    return (CtuluLibGeometrie.getDistance(_x, _y, _t.getXEcran(_xToTest), _t.getYEcran(_yToTest, _yaxe)) <= _precision);
  }

  protected void paintSurface(final Graphics2D _g, final EGRepere _t, final int _i) {
    createSurface();
    if (surfacePainter_ != null) {
      surfacePainter_.paint(_g, _t, _i);
    }
  }

  protected boolean setAlpha(final int _alpha) {
    if (alpha_ != _alpha) {
      alpha_ = _alpha;
      firePropertyChange(BSelecteurAlpha.DEFAULT_PROPERTY);
      fireCourbeAspectChanged(false);
      return true;
    }
    return false;
  }

  public boolean setDisplayTitleOnCurve(final boolean _displayTitleOnCurve) {
    if (_displayTitleOnCurve == displayTitleOnCurve_) {
      return false;
    }
    displayTitleOnCurve_ = _displayTitleOnCurve;
    fireCourbeAspectChanged(false);
    firePropertyChange(EGCourbeConfigureTarget.PROP_DISPLAY_TITLE_CURVE, displayTitleOnCurve_);
    return true;
  }

  public boolean setDisplayPointLabels(final boolean _displayPointLabels) {
    if (_displayPointLabels == displayPointLabels_) {
      return false;
    }
    displayPointLabels_ = _displayPointLabels;
    fireCourbeAspectChanged(false);
    firePropertyChange(EGCourbeConfigureTarget.PROP_DISPLAY_POINT_LABELS, displayPointLabels_);
    return true;
  }

  public boolean isVerticalLabels() {
    return tboxLabels_.isVertical();
  }

  public boolean setVerticalLabels(final boolean vertical) {
    if (tboxLabels_.isVertical() == vertical) {
      return false;
    }
    tboxLabels_.setVertical(vertical);
    fireCourbeAspectChanged(false);
    firePropertyChange(EGCourbeConfigureTarget.PROP_DISPLAY_VERTICAL_LABELS, vertical);
    return true;
  }

  protected boolean setFont(final Font _font) {
    if (_font == font_) {
      return false;
    }
    font_ = _font;
    fireCourbeAspectChanged(false);
    firePropertyChange(BSelecteurReduitFonteNewVersion.PROPERTY);
    return true;
  }

  protected void traceTitle(final Graphics2D _g, final EGRepere _t, final Envelope _r) {
    if (_r == null) {
      return;
    }
    final double x = _r.getMinX();
    double y = interpol(x);
    final Font old = _g.getFont();
    _g.setFont(getFont());
    final FontMetrics fm = _g.getFontMetrics();
    final String title = getTitle();
    final int xie = _t.getXEcran(x);
    final int xlast = xie + fm.stringWidth(title);
    final double xLastReal = _t.getXReel(xlast);
    final TIntArrayList l = new TIntArrayList(getModel().getNbValues());
    fillWithIdxStrictlyIncluded(Math.floor(x), Math.ceil(xLastReal), l);
    for (int i = l.size() - 1; i >= 0; i--) {
      final double yi = getModel().getY(l.get(i));
      if (yi > y) {
        y = yi;
      }
    }
    final int yie = _t.getYEcran(y, getAxeY()) - fm.getHeight();
    _g.drawString(title, xie + 3, yie);
    _g.setFont(old);
  }

  @Override
  public boolean ajuste(final CtuluRange _x, final CtuluRange _y) {
    // un seul | pout ajuster les 2
    return ajusteX(_x) | ajusteY(_y);
  }

  @Override
  public boolean ajusteX(final CtuluRange _r) {
    double x = getXMin();
    boolean r = false;
    if (model_.getNbValues() > 0) {
      final boolean mustChange = _r.isNill();
      if (mustChange || x < _r.min_) {
        r = true;
        _r.min_ = x;
      }
      x = getXMax();
      if (mustChange || x > _r.max_) {
        r = true;
        _r.max_ = x;
      }
    }
    return r;
  }

  @Override
  public boolean ajusteY(final CtuluRange _r) {
    boolean r = false;
    double y = getYMin();
    final boolean mustChange = _r.isNill();
    if (mustChange || y < _r.min_) {
      r = true;
      _r.min_ = y;
    }
    y = getYMax();
    if (mustChange || y > _r.max_) {
      r = true;
      _r.max_ = y;
    }
    return r;
  }

  /**
   * @param _x le x a tester
   * @return true si afficher
   */
  public boolean containsX(final double _x) {
    return (_x <= getModel().getXMax()) && (_x >= getModel().getXMin());
  }

  public void createSurface() {
    if (surfacePainter_ == null && getEGParent() != null) {
      surfacePainter_ = new EGCourbeSurfacePainter(this, getEGParent().getMainModel());
    }
  }

  @Override
  public void descendre() {
    getEGParent().descendre(this);
  }

  /**
   * Boolean si oui ou non le graphe est un nuage de points
   */
  protected boolean nuagePoints_ = false;
  /**
   * Taille des points du nuages symbolis�s par des carr�s.
   */
  protected final int largeurPointsNuage_ = 5;

  protected TraceIcon getSpecificIcon() {
    if (useSpecificIcon && iconeModelSpecific_ != null) {
      return new TraceIcon(new TraceIconModel(iconeModelSpecific_));
    }
    return null;
  }

  protected boolean useSpecific(int idx) {
    return getModel().useSpecificIcon(idx);
  }

  @Override
  public void dessine(final Graphics2D _g, final EGRepere _t) {
    if (!isVisible_) {
      return;
    }
    if (model_.getNbValues() == 0) {
      return;
    }

    final Shape oldClip = _g.getClip();
    final int minX = _t.getMinEcranX();
    final int adjust = 3;
    if (getAxeY() == null) {
      _g.setClip(minX - adjust, 0, _t.getMaxEcranX() - minX + adjust * 2, _t.getMaxEcranY());
    } else {
      _g.setClip(minX - adjust, _t.getMinEcranY(), _t.getMaxEcranX() - minX + adjust * 2, _t.getMaxEcranY() - _t.getMinEcranY());
    }
    final int nbPt = model_.getNbValues();
    double xi, yi, xie, yie;
    boolean xiVisible;
    boolean yiVisible;
    double xpe = 0;
    double ype = 0;
    double ypeVal = 0;
    boolean xpVisible = false;
    Envelope rangeDisplayed = null;
    _g.setColor(EbliLib.getAlphaColor(getAspectContour(), alpha_));
    final boolean isAlphaChanged = EbliLib.isAlphaChanged(alpha_);
    final TraceIcon trIcon = new TraceIcon(new TraceIconModel(iconeModel_));
    final TraceIcon trIconSpecific = getSpecificIcon();
    final TraceLigne trLigne = new TraceLigne(new TraceLigneModel(lineModel_));
    if (isAlphaChanged) {
      trIcon.setCouleur(EbliLib.getAlphaColor(trIcon.getCouleur(), alpha_));
      trLigne.setCouleur(EbliLib.getAlphaColor(trLigne.getCouleur(), alpha_));
      if (trIconSpecific != null) {
        trIconSpecific.setCouleur(EbliLib.getAlphaColor(trIconSpecific.getCouleur(), alpha_));
      }
    }
    // Les labels ont m�me couleur que la courbe.
    initLabelsTraceBox(trLigne.getCouleur());
    Rectangle view = new Rectangle();
    view.x = _t.getMinEcranX();
    view.y = _t.getMinEcranY();
    view.height = _t.getHSansMarges();
    view.width = _t.getWSansMarges();
    Map<Point, StringBuilder> labels = new HashMap<>();
    final Color colorLine = trLigne.getCouleur();
    final Color colorIcon = trIcon.getCouleur();
    final Color colorIconSpecific = trIconSpecific == null ? null : trIconSpecific.getCouleur();
    for (int i = 0; i < nbPt; i++) {
      xi = model_.getX(i);
      yi = model_.getY(i);
      xie = _t.getXEcran(xi);
      if (getAxeY() == null) {
        yie = getYEcranForAxeYNull(yi, _t);
        yiVisible = true;
      } else {
        yie = _t.getYEcran(yi, getAxeY());
        yiVisible = true;
      }
      xiVisible = _t.getXAxe().containsPoint(xi);

      if (model_.isPointDrawn(i) && xiVisible && yiVisible) {
        // Paint point label
        if (displayPointLabels_) {
          String pLabel = model_.getPointLabel(i);
          if (pLabel != null) {
            // Dessin de la boite et du label
            // X
            int xBox = ((int) xie);
            // Y
            int yBox = (int) yie - 3;
            Point pt = new Point(xBox, yBox);
            StringBuilder lb = labels.get(pt);
            if (lb == null) {
              lb = new StringBuilder();
              lb.append(pLabel);
              labels.put(pt, lb);
            } else {
              lb.append('\n').append(pLabel);
            }
          }
        }
        final boolean useSpecific = trIconSpecific != null && useSpecific(i);
        // Paint point
        if (iconeModel_.getType() != TraceIcon.RIEN || (useSpecific && trIconSpecific.getType() != TraceIcon.RIEN)) {
          if (displayTitleOnCurve_) {
            if (rangeDisplayed == null) {
              rangeDisplayed = new Envelope();
            }
            rangeDisplayed.expandToInclude(xi, yi);
          }
          if (useSpecific) {
            trIconSpecific.setCouleur(colorIconSpecific);
            Color toOverride = model_.getSpecificColor(i);
            if (toOverride != null) {
              trIconSpecific.setCouleur(toOverride);
            }
            trIconSpecific.paintIconCentre(_g, xie, yie);
          } else {
            trIcon.setCouleur(colorIcon);
            Color toOverride = model_.getSpecificColor(i);
            if (toOverride != null) {
              trIcon.setCouleur(toOverride);
            }
            trIcon.paintIconCentre(_g, xie, yie);
          }
        } else if (nuagePoints_) {
          trLigne.setCouleur(colorLine);
          Color toOverride = model_.getSpecificColor(i);
          if (toOverride != null) {
            trLigne.setCouleur(toOverride);
          }
          // -- trace un + --//
          // -- ligne | --//
          trLigne.dessineTrait(_g, xie, yie - largeurPointsNuage_ / 2, xie, yie + largeurPointsNuage_ / 2);
          // --ligne - --//
          trLigne.dessineTrait(_g, xie - largeurPointsNuage_ / 2, yie, xie + largeurPointsNuage_ / 2, yie);
        }
      }
      _g.setColor(colorIcon);
      if ((i > 0) && model_.isSegmentDrawn(i - 1) && (xpVisible || xiVisible || _t.getXAxe().isContained(xpe, xie))) {
        if (displayTitleOnCurve_) {
          if (rangeDisplayed == null) {
            rangeDisplayed = new Envelope();
          }
          rangeDisplayed.expandToInclude(xi, yi);
        }
        paintSurface(_g, _t, i);

        if (!nuagePoints_) {
          boolean canDraw = true;
          if (getAxeY() != null) {
            CtuluRange range = getAxeY().getRange();
            canDraw = range.isValueContained(yi) || range.isValueContained(ypeVal);
          }
          if (canDraw) {
            double newyie = yie;
            double newyie2 = ype;

            trLigne.setCouleur(colorLine);
            Color toOverride = model_.getSpecificColor(i - 1);
            if (toOverride != null) {
              trLigne.setCouleur(toOverride);
            }
            trLigne.dessineTrait(_g, xie, newyie, xpe, newyie2);
          }
        }
      }
      xpVisible = xiVisible;
      xpe = xie;
      ype = yie;
      ypeVal = yi;
    }
    _g.setClip(oldClip);
    _g.setColor(colorIcon);
    for (Map.Entry<Point, StringBuilder> entry : labels.entrySet()) {
      Point point = entry.getKey();
      StringBuilder stringBuilder = entry.getValue();
      paintLabelBox(_g, point.x, point.y, stringBuilder.toString(), _t, view);
    }
    traceMarks(_g, _t);

    traceTitle(_g, _t, rangeDisplayed);
  }

  public boolean isNuagePoints() {
    return nuagePoints_;
  }

  public void setNuagePoints(boolean _nuagePoints) {
    this.nuagePoints_ = _nuagePoints;
  }

  public boolean setNuagePointEtApplique(boolean _nuagePOints) {
    setNuagePoints(_nuagePOints);
    fireCourbeAspectChanged(false);
    return true;
  }

  @Override
  public void enDernier() {
    getEGParent().enDernier(this);
  }

  @Override
  public void enPremier() {
    getEGParent().enPremier(this);
  }

  @Override
  public void fillWithCurves(final List _dest) {
    _dest.add(this);
  }

  /**
   * @param _xDeb l'abscisse de deb
   * @param _xEnd l'abscisse de deb
   * @param _dest la liste qui sera remplie avec les indices des points dont les abscisses sont strictement incluses dans ]_xDeb,_xEnd[
   */
  public void fillWithIdxStrictlyIncluded(final double _xDeb, final double _xEnd, final TIntArrayList _dest) {
    _dest.clear();
    if (getModel().getNbValues() == 0) {
      return;
    }
    int idx = getNearestIdx(_xDeb);
    if (idx < 0) {
      idx = -idx - 1;
    } else {
      idx++;
    }
    if (idx < 0 || idx >= getModel().getNbValues()) {
      return;
    }
    double x = getModel().getX(idx);
    while (x > _xDeb && x < _xEnd) {
      _dest.add(idx);
      idx++;
      if (idx >= getModel().getNbValues()) {
        break;
      }
      x = getModel().getX(idx);
    }
  }

  boolean silent;

  public void setSilent(boolean silent) {
    this.silent = silent;
  }

  @Override
  public final void fireCourbeAspectChanged(final boolean _visibility) {
    if (!silent && getEGParent() != null) {
      getEGParent().fireCourbeAspectChanged(this, _visibility);
    }
  }

  @Override
  public final void fireCourbeContentChanged() {
    if (!silent && getEGParent() != null) {
      getEGParent().fireCourbeContentChanged(this, false);
    }
  }

  /**
   * @return un indice entre 0 et le nombre de point indiquant l'indice active( un pas de temps par exemple)
   */
  public int getActiveTimeIdx() {
    return -1;
  }

  public boolean getAllowsChildren() {
    return false;
  }

  public Color getAspectContour() {
    final Color c = lineModel_.getCouleur();
    return c == iconeModel_.getCouleur() ? c : null;
  }

  public Color getAspectSurface() {
    return null;
  }

  public Color getAspectTexte() {
    return null;
  }

  @Override
  public final EGAxeHorizontal getAxeX() {
    return getEGParent().getAxeX();
  }

  @Override
  public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[]{getSingleConfigureInterface()};
  }

  public int getIconeType() {
    return iconeModel_.getType();
  }

  @Override
  public int getIconHeight() {
    return 24;
  }

  public TraceIconModel getIconModel() {
    return iconeModel_;
  }

  public TraceIconModel getIconModelSpecific() {
    return iconeModelSpecific_;
  }

  @Override
  public int getIconWidth() {
    return 24;
  }

  public int getIndex(final TreeNode _node) {
    return -1;
  }

  public TraceLigneModel getLigneModel() {
    return lineModel_;
  }

  /**
   * Definit le modele de ligne pour le trac� de la courbe. Conditionne aussi l'encadrement des labels.
   *
   * @param _ic Le modele.
   * @return True : Le mod�le a �t� accept�.
   */
  public boolean setLigneModel(final TraceLigneModel _ic) {
    final boolean r = lineModel_.updateData(_ic);
    if (r) {
      fireCourbeAspectChanged(false);
      super.firePropertyChange(BSelecteurIconModel.DEFAULT_PROPERTY, null, lineModel_);
    }
    return r;
  }

  public TraceLigneModel getMarkLigneModel() {
    return tLigneMarqueur_;
  }

  /**
   * @return les marques affichees par cette courbe
   */
  public List<EGCourbeMarqueur> getMarqueurs() {
    if (listeMarqueurs_ == null) {
      // -- init avec 3 marqueurs --//
      listeMarqueurs_ = new ArrayList<>(3);
      listeMarqueurs_.add(createMarqueur());
      listeMarqueurs_.add(createMarqueur());
      listeMarqueurs_.add(createMarqueur());
    }
    return listeMarqueurs_;
  }

  /**
   * A ne pas utiliser pour la persistance, il se peut que le eg model soit un decorator.
   */
  public EGModel getModel() {
    return model_;
  }

  /**
   * Retourne le modele initial, sans decorateur.
   */
  public EGModel getInitialModel() {
    if (getModel() instanceof EGModelDecorator) {
      return ((EGModelDecorator) getModel()).getModeleInitial();
    } else {
      return getModel();
    }
  }

  /**
   * Attention, cela suppose que les x sont strictement croissants:
   *
   * @param _x
   */
  public int getNearestIdx(final double _x) {
    int low = 0;
    int high = model_.getNbValues() - 1;
    while (low <= high) {
      final int mid = (low + high) >> 1;
      final double midVal = getModel().getX(mid);
      if (midVal < _x) {
        low = mid + 1;
      } else if (midVal > _x) {
        high = mid - 1;
      } else {
        return mid; // value found
      }
    }
    return -(low + 1); // value not found.
  }

  public TIntArrayList getSegmentIdxContaining(final double x) {
    TIntArrayList res = new TIntArrayList();
    for (int i = 0; i < model_.getNbValues() - 1; i++) {
      double min = getModel().getX(i);
      double max = getModel().getX(i + 1);
      if (min > max) {
        double tmp = max;
        max = min;
        min = tmp;
      }
      if (x >= min && x <= max) {
        res.add(i);
      }
    }
    return res;
  }

  public BConfigurableInterface getSingleConfigureInterface() {
    final BConfigurableComposite bConfigurableComposite = createConfigurableDisplay();
    BConfigurableInterface surface = null;
    if (surfacePainter_ != null) {
      surface = new EGCourbeSurfacePainterConfigure(surfacePainter_);
    }
    final BConfigurableComposite axeY = new BConfigurableComposite(new EGAxeConfigureTarget.AffichageConfigurator(
        getAxeY(), getEGParent()), new EGAxeConfigureTarget.TitleConfigure(getAxeY(), getEGParent()), EbliLib
        .getS("Axe vertical"));
    final BConfigurableComposite axeX = new BConfigurableComposite(new EGAxeConfigureTarget.AffichageConfigurator(
        getAxeX(), getEGParent()), new EGAxeConfigureTarget.TitleConfigure(getAxeX(), getEGParent()), EbliLib
        .getS("Axe horizontal"));
    if (surface == null) {
      return new BConfigurableComposite(bConfigurableComposite, new EGCourbeConfigureTarget.Marks(this), axeY, axeX,
          null);
    }
    return new BConfigurableComposite(null, bConfigurableComposite, surface, new EGCourbeConfigureTarget.Marks(this), axeY, axeX);
  }

  protected BConfigurableComposite createConfigurableDisplay() {
    return new BConfigurableComposite(new EGCourbeConfigureTarget.Display(this), EbliLib.getS("Courbe"));
  }

  public EGCourbeSurfacePainter getSurfacePainter() {
    return surfacePainter_;
  }

  @Override
  public String getTitle() {
    return model_.getTitle();
  }

  public double getXMax() {
    return model_.getXMax();
  }

  public double getXMin() {
    return model_.getXMin();
  }

  public double getYMax() {
    return model_.getYMax();
  }

  public double getYMin() {
    return model_.getYMin();
  }

  public double interpol(final double _x) {
    final int i = getNearestIdx(_x);
    if (i >= 0) {
      return model_.getY(i);
    }
    final int idx = -i - 2;
    if ((idx < 0) || (idx >= model_.getNbValues() - 1)) {
      return 0D;
    }
    final double xlow = model_.getX(idx);
    final double xup = model_.getX(idx + 1);
    final double ylow = model_.getY(idx);
    final double yup = model_.getY(idx + 1);
    return yup - (xup - _x) / (xup - xlow) * (yup - ylow);
  }

  public TDoubleArrayList interpolPointsOnEcran(final double _x, final EGRepere _rep) {
    TDoubleArrayList res = new TDoubleArrayList();
    if (model_.getNbValues() == 0) {
      return res;
    }
    final EGAxeVertical vert = getAxeY();
    final TIntArrayList idxs = getSegmentIdxContaining(_x);
    if (idxs.isEmpty()) {
      return res;
    }
    int nb = idxs.size();
    for (int i = 0; i < nb; i++) {
      int seg = idxs.get(i);
      double xmin = model_.getX(seg);
      double ymin = model_.getY(seg);
      double xmax = model_.getX(seg + 1);
      double ymax = model_.getY(seg + 1);
      double y = ymax - (xmax - _x) / (xmax - xmin) * (ymax - ymin);
      y = _rep.getYEcran(y, vert);
      res.add(y);
    }
    return res;
  }

  public TDoubleArrayList interpolPoints(final double _x, final EGRepere _rep) {
    TDoubleArrayList res = new TDoubleArrayList();
    if (model_.getNbValues() == 0) {
      return res;
    }
    final EGAxeVertical vert = getAxeY();
    final TIntArrayList idxs = getSegmentIdxContaining(_x);
    if (idxs.isEmpty()) {
      return res;
    }
    int nb = idxs.size();
    for (int i = 0; i < nb; i++) {
      int seg = idxs.get(i);
      double xmin = model_.getX(seg);
      double ymin = model_.getY(seg);
      double xmax = model_.getX(seg + 1);
      double ymax = model_.getY(seg + 1);
      double y = ymax - (xmax - _x) / (xmax - xmin) * (ymax - ymin);
      res.add(y);
    }
    return res;
  }

  /**
   * @param _x
   * @param _rep
   * @return attention, cela suppose que les x sont strictement croissants.
   */
  public double interpolOnEcran(final double _x, final EGRepere _rep) {
    if (model_.getNbValues() == 0) {
      return 0d;
    }
    final EGAxeVertical vert = getAxeY();
    final int i = getNearestIdx(_x);
    if (i >= 0) {
      return _rep.getYEcran(model_.getY(i), vert);
    }
    final int idx = -i - 2;
    if (model_.getNbValues() == 0) {
      return 0d;
    }
    if (idx < 0) {
      return _rep.getYEcran(model_.getY(0), vert);
    }
    if (idx >= model_.getNbValues() - 1) {
      return _rep.getYEcran(model_.getY(model_.getNbValues() - 1), vert);
    }
    final double xlow = _rep.getXEcran(model_.getX(idx));
    final double xup = _rep.getXEcran(model_.getX(idx + 1));
    final double ylow = _rep.getYEcran(model_.getY(idx), vert);
    final double yup = _rep.getYEcran(model_.getY(idx + 1), vert);
    return yup - (xup - _rep.getXEcran(_x)) / (xup - xlow) * (yup - ylow);
  }

  public boolean isActiveTimeEnable() {
    return false;
  }

  @Override
  public final boolean isCourbe() {
    return true;
  }

  /**
   * @return true
   */
  public final boolean isLegendAvailable() {
    return true;
  }

  @Override
  public boolean isSomethingToDisplay() {
    return true;
  }

  @Override
  public boolean isTitleModifiable() {
    return model_.isTitleModifiable();
  }

  @Override
  public void monter() {
    getEGParent().monter(this);
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    Color c = getAspectContour();
    if (!isVisible()) {
      c = FiltreAttenuation.attenueCouleur(c);
    }
    final Color o = _g.getColor();
    final Graphics2D g = (Graphics2D) _g;
    final Stroke oldS = g.getStroke();
    g.setStroke(new BasicStroke(1.5f));
    _g.setColor(c);
    int xinit = _x;
    int yinit = _y;
    final int w4 = getIconWidth() / 4;
    final int h = getIconWidth();
    int xfin = xinit + w4;
    int yfin = yinit + h / 4;
    _g.drawLine(xinit, yinit, xfin, yfin);
    xinit = xfin;
    yinit = yfin;
    xfin += w4;
    yfin = _y;
    _g.drawLine(xinit, yinit, xfin, yfin);
    xinit = xfin;
    yinit = yfin;
    xfin += w4;
    yfin = _y + h;
    _g.drawLine(xinit, yinit, xfin, yfin);
    xinit = xfin;
    yinit = yfin;
    xfin += w4;
    yfin = _y + h / 2;
    _g.drawLine(xinit, yinit, xfin, yfin);
    _g.setColor(o);
    g.setStroke(oldS);
  }

  public void paintSelection(final Graphics2D _g2d, final EGInteractionSelection _d) {
    final CtuluListSelectionInterface s = _d.getSelection();
    for (int i = s.getMaxIndex(); i >= s.getMinIndex(); i--) {
      if (s.isSelected(i) && i < model_.getNbValues()) {
        final int x = _d.getRepere().getXEcran(model_.getX(i));
        final int y = _d.getRepere().getYEcran(model_.getY(i), getAxeY());
        _d.getSelectionIcone().paintIconCentre(_g2d, x, y);
      }
    }
  }

  public void removePropertyChangeListener(final PropertyChangeListener _l) {
  }

  public int select(final int _xEcran, final int _yEcran, final EGRepere _repere, final int _prec) {
    if (model_.getNbValues() == 0) {
      return -1;
    }
    final double x = _repere.getXReel(_xEcran);
    int idx = getNearestIdx(x);
    if (idx >= 0) {
      final boolean r = isSelected(model_.getX(idx), model_.getY(idx), _xEcran, _yEcran, _repere, getAxeY(), _prec);
      if (r) {
        return idx;
      }
    }
    idx = -idx - 2;
    final int maxIdx = model_.getNbValues() - 1;
    if (idx < 0) {
      idx = 0;
    }
    if (idx > maxIdx) {
      idx = maxIdx;
    }
    boolean r = isSelected(model_.getX(idx), model_.getY(idx), _xEcran, _yEcran, _repere, getAxeY(), _prec);
    if (r) {
      return idx;
    }
    if (idx < maxIdx) {
      idx++;
      r = isSelected(model_.getX(idx), model_.getY(idx), _xEcran, _yEcran, _repere, getAxeY(), _prec);
      if (r) {
        return idx;
      }
    }
    return -1;
  }

  public boolean setAspectContour(final Color _c) {
    if (_c == null) {
      return false;
    }
    boolean r = false;
    if (!_c.equals(lineModel_.getCouleur())) {
      lineModel_.setCouleur(_c);
      firePropertyChange(BSelecteurLineModel.PROPERTY);
      r = true;
    }
    if (!iconeModel_.getCouleur().equals(_c)) {
      iconeModel_.setCouleur(_c);
      firePropertyChange(BSelecteurIconModel.DEFAULT_PROPERTY);
      r = true;
    }
    if (r) {
      fireCourbeAspectChanged(false);
      firePropertyChange(BSelecteurColorChooser.DEFAULT_PROPERTY, null, _c);
    }
    return r;
  }

  public boolean setIconeModel(final TraceIconModel _ic) {
    final boolean r = iconeModel_.updateData(_ic);
    if (r) {
      fireCourbeAspectChanged(false);
      super.firePropertyChange(BSelecteurIconModel.DEFAULT_PROPERTY, null, iconeModel_);
    }
    return r;
  }

  public boolean setIconeModelSpecific(TraceIconModel traceIconModel) {
    boolean r = false;
    if (iconeModelSpecific_ == null) {
      r = true;
      iconeModelSpecific_ = new TraceIconModel();
    }
    r = iconeModelSpecific_.updateData(traceIconModel);
    if (r) {
      fireCourbeAspectChanged(false);
      super.firePropertyChange(EGCourbeConfigureTarget.SPECIFIC_ICON, null, iconeModelSpecific_);
    }
    return r;
  }

  public boolean setLigneMark(final TraceLigneModel _data) {
    if (tLigneMarqueur_ == null) {
      buildLigneMarqueur();
    }
    final boolean res = tLigneMarqueur_.updateData(_data);
    if (res) {
      fireCourbeAspectChanged(false);
      super.firePropertyChange(EGCourbeConfigureTarget.PROP_MARK_LINE, null, iconeModel_);
    }
    return res;
  }

  /**
   * @param _marks les nouvelles marques
   * @return true si modife
   */
  // public boolean setMarks(final double[] _marks) {
  // if (Arrays.equals(markH_, _marks)) {
  // return false;
  // }
  // markH_ = CtuluLibArray.copy(_marks);
  // fireCourbeAspectChanged(false);
  // firePropertyChange(EGCourbeConfigureTarget.PROP_MARK_VALUES, null, _marks);
  // return true;
  // }

  /**
   * @param boolean affichent _marks
   * @return true si modife
   */
  public boolean addMarqueur(EGCourbeMarqueur marqueur) {
    getMarqueurs().add(marqueur);
    modifyMarqueur(marqueur);
    return true;
  }

  /**
   * Applique les modifs sur le graphe des la modif d'un marqueur.
   *
   * @param marqueur
   */
  public void modifyMarqueur(EGCourbeMarqueur marqueur) {
    fireCourbeAspectChanged(false);
    firePropertyChange(EGCourbeConfigureTarget.PROP_MARK_VALUES, null, marqueur);
  }

  /**
   * Ajoute un marqueur g�n�rique initialis� a visible=false.
   */
  public boolean addMarqueur() {
    EGCourbeMarqueur marqueur = createMarqueur();
    return addMarqueur(marqueur);
  }

  public boolean deleteLastMarqueur() {
    if (getMarqueurs().size() > 1) {
      getMarqueurs().remove(getMarqueurs().size() - 1);
      return true;
    }
    return false;
  }

  public boolean hideAllMarqueur() {
    for (EGCourbeMarqueur marqueur : getMarqueurs()) {
      marqueur.setVisible(false);
    }
    return true;
  }

  public EGCourbeMarqueur getMarqueur(int indice) {
    if (getMarqueurs() != null && getMarqueurs().size() > indice) {
      return getMarqueurs().get(indice);
    }
    return null;
  }

  public void setMarqueurs(List<EGCourbeMarqueur> liste) {
    listeMarqueurs_ = liste;
  }

  public void setModel(final EGModel _model) {
    model_ = _model;
  }

  @Override
  public boolean setTitle(final String _newTitle) {
    final boolean res = model_.setTitle(_newTitle);
    if (res) {
      fireCourbeContentChanged();
      firePropertyChange(BSelecteurTextField.TITLE_PROPERTY, null, model_.getTitle());
    }
    return res;
  }

  public boolean initTitle(final String _newTitle) {
    return setTitle(_newTitle);
  }

  @Override
  public String toString() {
    return getTitle();
  }

  public TraceBox getTbox() {
    return tbox_;
  }

  public void setTbox_(TraceBox tbox_) {
    this.tbox_ = tbox_;
  }

  @Override
  public BConfigurePalette getPalette() {
    return paletteEnCours_;
  }

  BConfigurePalette paletteEnCours_ = null;

  @Override
  public void setPalette(BConfigurePalette pal) {
    paletteEnCours_ = pal;
  }

  public static final String INDICE_PANEL_SHOW = "indicePtoshow";

  @Override
  public boolean reloadPalette(Map infos) {

    int panelToShow = -1;
    if (infos.get(INDICE_PANEL_SHOW) != null) {
      panelToShow = ((Integer) infos.get(INDICE_PANEL_SHOW)).intValue();
    }

    if (paletteEnCours_ != null) {
      return paletteEnCours_.setPalettePanelTarget(this, panelToShow);
    }

    return false;
  }

  public boolean isInverse = false;

  /**
   * Methode qui inverse le modele x par rapport � y
   */
  public void inverserModele() {

    if (!isInverse) {
      EGModelDecoratorInverse modelInverse = new EGModelDecoratorInverse(getModel());
      setModel(modelInverse);
    } else {
      // -- on lui re- set le modele --//
      setModel(getInitialModel());
    }
    isInverse = !isInverse;

    // -- on met a jour les axes --//
    ajusterAxesForCourbe();
    EGGrapheModel mainModel = getEGParent().getMainModel();
    if (mainModel != null) {
      mainModel.fireAxeContentChanged(getAxeX());
      mainModel.fireCourbeAspectChanged(this, true);
    }
  }

  public void inverserNuagePoints() {
    // -- on permutte entre nuage de points et classique --//
    nuagePoints_ = !nuagePoints_;
    EGGrapheModel mainModel = getEGParent().getMainModel();
    if (mainModel != null) {
      mainModel.fireCourbeAspectChanged(this, false);
    }
  }

  protected void ajusterAxesForCourbe() {
    this.getAxeY().ajusteFor(this);
    this.getAxeX().setBounds(getXMin(), getXMax());
  }

  public double getYEcran(double yi, final EGRepere _t) {
    if (getAxeY() == null) {
      return getYEcranForAxeYNull(yi, _t);
    }
    // -- attention si le point depasse de l'�cran il faut dessinner le trait jusqu'� la limite de l'axe--//
    double newyi = Math.min(yi, getAxeY().getMaximum());
    newyi = Math.max(newyi, getAxeY().getMinimum());
    double newyie = _t.getYEcran(newyi, getAxeY());
    return newyie;
  }

  public double getYEcranForAxeYNull(double yi, final EGRepere _t) {
    double yie;
    double ratio = Math.min(1, Math.max(0, yi));
    if (ratio >= 1) {
      yie = _t.getMaxEcranY();
    } else if (ratio <= 0) {
      yie = _t.getMinEcranY();
    } else {
      yie = _t.getMinEcranY() + (_t.getMaxEcranY() - _t.getMinEcranY()) * ratio;
    }
    return yie;
  }

  protected void paintLabelBox(final Graphics2D _g, int xBox, int yBox, String pLabel, final EGRepere _t, Rectangle view) {
    tboxLabels_.paintBox(_g, xBox, yBox, pLabel, view);
  }

  public EGCourbeMarqueur createMarqueur() {
    EGCourbeMarqueur marqueur = new EGCourbeMarqueur(0, false, new TraceLigneModel(), true);
    return marqueur;
  }
}
