/**
 * @creation 24 juin 2004
 * @modification $Date: 2006-07-13 13:35:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import org.fudaa.ebli.commun.EbliActionChangeState;

/**
 * @author Fred Deniger
 * @version $Id: EGActionInteraction.java,v 1.5 2006-07-13 13:35:42 deniger Exp $
 */
public class EGActionInteraction extends EbliActionChangeState implements PropertyChangeListener {
  
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    final boolean newVal = ((Boolean) _evt.getNewValue()).booleanValue();
    if (newVal != isSelected()) {
      setSelected(newVal);
    }
  }
  EGInteractiveComponent interComp_;
  
  public EGActionInteraction(final String _name, final Icon _ic, final String _action, final EGInteractiveComponent _inter) {
    super(_name, _ic, _action);
    setTarget(_inter);
  }
  
  public final void setTarget(final EGInteractiveComponent _inter) {
    if (interComp_ != null) {
      interComp_.removePropertyChangeListener("active", this);
    }
    interComp_ = _inter;
    interComp_.addPropertyChangeListener("active", this);
  }
  
  @Override
  public void changeAction() {
    interComp_.setActive(isSelected());
  }
}
