/*
 * @creation 5 ao�t 2004
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import javax.swing.JComponent;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Par defaut le dnd est active. Pour le desactive vous devez redefinir la methode majComponent.
 * @author Fred Deniger
 * @version $Id: EGFilleTree.java,v 1.13 2006-09-19 14:55:53 deniger Exp $
 */
public class EGFilleTree extends EGFille implements CtuluFilleWithComponent {

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public EGFilleTree(final EGFillePanel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id) {
    super(_g, _titre, _appli, _id, null);
    if (!(_g.vue_.graphe_.getModel() instanceof EGGrapheTreeModel)) {
      throw new IllegalArgumentException();
    }
  }

  public EGFilleTree(final EGFillePanel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _tablePanel) {
    super(_g, _titre, _appli, _id, _tablePanel);
    if (!(_g.vue_.graphe_.getModel() instanceof EGGrapheTreeModel)) {
      throw new IllegalArgumentException();
    }
  }

  /**
   * @param _g
   * @param _titre
   */
  public EGFilleTree(final EGGrapheTreeModel _g, final String _titre) {
    super(new EGGraphe(_g), _titre);
  }

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public EGFilleTree(final EGGrapheTreeModel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id) {
    super(new EGGraphe(_g), _titre, _appli, _id);
  }

  /**
   * @param _g
   * @param _titre
   * @param _appli
   * @param _id
   */
  public EGFilleTree(final EGGrapheTreeModel _g, final String _titre, final BuCommonInterface _appli,
      final BuInformationsDocument _id, final EGTableGraphePanel _p) {
    super(new EGGraphe(_g), _titre, _appli, _id, _p);
  }

  @Override
  public Class getComponentClass(){
    return EGTree.class;
  }

  @Override
  public String getComponentTitle(){
    return EbliLib.getS("courbes");
  }

  public final EGGrapheTreeModel getGrapheTree(){
    return (EGGrapheTreeModel) getGraphe().getModel();
  }

  EGSpecificActions actions_;
  EGTree tree_;

  @Override
  public JComponent createPanelComponent(){
    if (tree_ == null) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("init composant pour graphe fille");
      }
      tree_ = new EGTree();
      tree_.setExpandsSelectedPaths(true);
      actions_ = new EGSpecificActions(getGraphe());
      tree_.setActions(actions_);

      tree_.setName("ARBRE_COURBES");
    }
    return tree_;
  }

  @Override
  public void majComponent(final Object _o){
    final EGTree t = (EGTree) _o;
    final TreePath[] p = getGrapheTree().getSelectionModel().getSelectionPaths();
    t.setModel(getGrapheTree());
    t.setSelectionModel(getGrapheTree().getSelectionModel());
    t.setMng(getCmdMng());
    if (p != null) {
      t.setSelectionPaths(p);
    }
    t.setActions(actions_);
  }

}