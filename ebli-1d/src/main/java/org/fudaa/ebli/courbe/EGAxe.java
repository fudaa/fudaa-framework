/**
 * @creation 1999-07-29
 * @modification $Date: 2007-05-22 14:19:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.courbe;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Font;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.iterator.LogarithmicNumberIterator;
import org.fudaa.ctulu.iterator.NumberIterator;
import org.fudaa.ctulu.iterator.TickIterator;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurColorChooser;
import org.fudaa.ebli.controle.BSelecteurReduitFonteNewVersion;
import org.fudaa.ebli.controle.BSelecteurTextField;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @version $Id: EGAxe.java,v 1.25 2007-05-22 14:19:05 deniger Exp $
 * @author Guillaume Desnoix,Fred Deniger
 */
public abstract class EGAxe {

  public static final String PROP_EXTREMITE_VISIBLE = "axeExtremiteVisible";
  public static final String PROP_TITLE_VISIBLE = "axeTitleVisible";
  public static final String PROP_UNIT_VISIBLE = "axeUnitVisible";
  public static final String PROP_GRADUATIONS = "axeGraduations";
  public static final String PROP_TRACE_GRADUATIONS = "traceGraduation";
  public static final String PROP_TRACE_SOUS_GRADUATIONS = "traceSousGraduation";
  public static final String PROP_IS_GRID_GRADUATIONS_PAINTED = "displayGraduation";
  public static final String PROP_IS_GRID_SOUS_GRADUATIONS_PAINTED = "displaySousGraduation";
  public static final String PROP_TITLE_CENTERED = "axeTitleCentered";
  public static final String PROP_BOUNDS = "axeBounds";
  final CtuluRange range_ = new CtuluRange();
  TickIterator axisIterator_;
  Font font_;
  boolean graduations_;

  private CtuluRange xScreen;
  private CtuluRange yScreen;

  protected void clearXYScreen() {
    xScreen = null;
    yScreen = null;
  }

  protected void addScreenXY(double x, double y) {
    addScreenXY((int) x, (int) y);

  }

  protected void addScreenXY(int x, int y) {
    if (xScreen == null) {
      xScreen = new CtuluRange();
    }
    xScreen.expandTo(x);
    if (yScreen == null) {
      yScreen = new CtuluRange();
    }
    yScreen.expandTo(y);
  }

  public boolean isInScreenBounds(int x, int y) {
    if (xScreen != null && yScreen != null) {
      return xScreen.isValueContained(x) && yScreen.isValueContained(y);
    }
    return false;
  }
  /**
   * Booleen qui indique si l'on trace ou non la graduation
   */
  boolean traceGrille_ = false;
  /**
   * Booleen qui indique si l'on trcae
   */
  boolean traceSousGrille_ = false;
  TraceLigneModel traceGraduations_;
  TraceLigneModel traceSousGraduations_;
  boolean isIteratorUptodate_;
  Color lineColor_;
  public static final int MANUEL_GRADUATIONS = 0;
  public static final int MANUEL_LONGUEURPAS = 1;
  public static final int AUTO_GRADUATIONS = 2;
  public static final int AUTO_LONGUEURPAS = 3;
  public static final int LOGARITHMIQUE = 4;

  protected boolean isGridPainted() {
    return isTraceGrille() || isTraceSousGrille();
  }
  /**
   * Mode automatique ou manuel manuel graduations calcul le meme nombre de graduations . manuel calcul les graduations tous les longueur pas.
   * automatique graduations: calcul au mieux les graduations automatique longueur pas: calcul les graduations puis au mieux logarithmique.
   */
  protected int modeGraduations_ = AUTO_GRADUATIONS;
  /**
   * Mode nb graduations
   */
  int nbPas_;
  /**
   * Mode on donne la longueur des pas
   */
  double longueurPas_;
  CtuluNumberFormatI specificFormat_;
  CtuluNumberFormatI specificFormatExport_;
  CtuluNumberFormatI specificDetailFormat_;
  String titre_;
  boolean titreVisible_ = true;
  String unite_;
  boolean uniteVisible_ = true;
  CtuluValueEditorI valueEditor_;
  boolean visible_;
  protected boolean isExtremiteDessinee_;
  protected boolean isTitreCentre_ = true;
  protected TickIterator reuseIterator_;
  protected Object key;
  protected Object userObject;

  public EGAxe() {
    titre_ = "";
    visible_ = true;
    unite_ = "";
    graduations_ = true;
    nbPas_ = TickIterator.DEFAULT_STICK_NUMBER;
    lineColor_ = Color.black;
    initDisplayValues();
  }
  protected boolean isDiscret_;

  public final boolean isDiscret() {
    return isDiscret_;
  }

  public void setUserObject(Object userObject) {
    this.userObject = userObject;
  }

  public Object getUserObject() {
    return userObject;
  }

  public CtuluRange getRange() {
    return range_ == null ? new CtuluRange() : new CtuluRange(range_);
  }

  public void setIsDiscret(boolean isDiscret) {
    this.isDiscret_ = isDiscret;
  }

  public Object getKey() {
    return key;
  }

  public void setKey(Object key) {
    this.key = key;
  }

  public final void initDisplayValues() {
    if (traceGraduations_ == null) {
      traceGraduations_ = new TraceLigneModel(TraceLigne.TIRETE, 1, Color.black);
    }
    if (traceSousGraduations_ == null) {
      traceSousGraduations_ = new TraceLigneModel(TraceLigne.TIRETE, (float) 0.3, Color.gray);
    }
  }

  public EGAxe(final EGAxe _a) {
    this();
    if (_a != null) {
      range_.initWith(_a.range_);
      lineColor_ = _a.lineColor_;
      graduations_ = _a.graduations_;
      if (_a.axisIterator_ != null) {
        try {
          axisIterator_ = (TickIterator) (_a.axisIterator_.clone());
        } catch (final CloneNotSupportedException _ex) {
          // ne doit jamais arriver
          FuLog.warning(_ex.getMessage());
        }
      }
      visible_ = _a.visible_;
      titre_ = _a.titre_;
      unite_ = _a.unite_;
      titreVisible_ = _a.titreVisible_;
      uniteVisible_ = _a.uniteVisible_;
      isExtremiteDessinee_ = _a.isExtremiteDessinee_;
      isTitreCentre_ = _a.isTitreCentre_;
      font_ = _a.font_;
      nbPas_ = _a.nbPas_;
      modeGraduations_ = _a.modeGraduations_;
      longueurPas_ = _a.longueurPas_;
      if (_a.specificFormat_ != null) {
        specificFormat_ = _a.specificFormat_.getCopy();
      }
      if (_a.traceGraduations_ != null) {
        traceGraduations_ = new TraceLigneModel(_a.traceGraduations_);
      }
      if (_a.traceSousGraduations_ != null) {
        traceSousGraduations_ = new TraceLigneModel(_a.traceSousGraduations_);
      }
    }
  }

  /**
   * Peut retour l'iterateur principal: ne pas faire de init.
   *
   * @return un iterateur a jour.
   */
  private TickIterator getUpdatedTickIterator() {
    if (axisIterator_ == null) {
      return buildUpToDateMainTickIterator();
    }
    if (isIteratorUptodate_) {
      return axisIterator_;
    }
    try {
      TickIterator r = null;
      if (reuseIterator_ != null && reuseIterator_.getClass().equals(axisIterator_.getClass())) {
        r = reuseIterator_;
      } else {
        r = (TickIterator) axisIterator_.clone();
        reuseIterator_ = r;
      }
      r.init(getMinimum(), getMaximum(), nbPas_);
      return r;
    } catch (final CloneNotSupportedException _ex) {
      // ne doit jamais arriver
      FuLog.warning(_ex.getMessage());
    }
    return null;
  }
  /**
   * nombre de sous graduations
   */
  int nbSousGraduations_ = 3;

  final TickIterator buildUpToDateMainTickIterator() {
    if (userIterator != null) {
      axisIterator_ = userIterator;
    } else if (modeGraduations_ == LOGARITHMIQUE) {
      axisIterator_ = new LogarithmicNumberIterator();
      ((NumberIterator) axisIterator_).setMaxFractionDigits(2);
    } else {
      axisIterator_ = new NumberIterator();
      ((NumberIterator) axisIterator_).setMaxFractionDigits(2);
    }

    if (modeGraduations_ == MANUEL_GRADUATIONS) {
      axisIterator_.initExact(getMinimum(), getMaximum(), nbPas_, nbSousGraduations_ + 1);
    } else if (modeGraduations_ == MANUEL_LONGUEURPAS) {
      axisIterator_.initExactFromDist(getMinimum(), getMaximum(), longueurPas_, nbSousGraduations_ + 1);
    } else if (modeGraduations_ == AUTO_LONGUEURPAS) {

      int nbIteration = (int) ((getMaximum() - getMinimum()) / longueurPas_);
      axisIterator_.init(getMinimum(), getMaximum(), nbIteration);
    } else if (modeGraduations_ == AUTO_GRADUATIONS) {

      axisIterator_.init(getMinimum(), getMaximum(), nbPas_);
    } else if (modeGraduations_ == LOGARITHMIQUE) {

      axisIterator_.init(getMinimum(), getMaximum(), nbPas_);
    }

    if (FuLog.isDebug() && Fu.DEBUG) {
      FuLog.debug(" axis min=" + getMinimum() + " max= " + getMaximum() + " nbPas " + nbPas_);
    }

    isIteratorUptodate_ = true;
    return axisIterator_;
  }

  /**
   * @return true si l'extremite (la fleche) de l'axe est dessinee
   */
  final boolean isExtremiteDessinee() {
    return isExtremiteDessinee_;
  }

  /**
   * @return true si la legende est centree
   */
  final boolean isTitreCentre() {
    return isTitreCentre_;
  }

  /**
   * Modifie la propriete extremite dessinee.
   *
   * @param _isExtremiteDessinee
   */
  final boolean setExtremiteDessinee(final boolean _isExtremiteDessinee) {
    if (isExtremiteDessinee_ != _isExtremiteDessinee) {
      isExtremiteDessinee_ = _isExtremiteDessinee;
      firePropertyChange(PROP_EXTREMITE_VISIBLE, isExtremiteDessinee_);
      return true;
    }
    return false;
  }

  public CtuluNumberFormatI getSpecificFormatExport() {
    return specificFormatExport_;
  }

  public void setSpecificFormatExport(CtuluNumberFormatI specificFormatExport) {
    this.specificFormatExport_ = specificFormatExport;
  }

  /**
   * @param _isLegendeCentre true si legende centree sous l'axe
   */
  final boolean setTitreCentre(final boolean _isLegendeCentre) {
    if (isTitreCentre_ != _isLegendeCentre) {
      isTitreCentre_ = _isLegendeCentre;
      firePropertyChange(PROP_TITLE_CENTERED, isTitreCentre_);
      return true;
    }
    return false;
  }

  protected TickIterator createMainTickIterator() {
    return new NumberIterator();
  }

  protected void firePropertyChange(final String _prop) {
    /*
     * if (listener_ != null) { listener_.firePropertyChange(_prop, false, true); }
     */
  }

  protected void firePropertyChange(final String _prop, final Object _old, final Object _new) {
    /*
     * if (listener_ != null) { listener_.firePropertyChange(_prop, _old, _new); }
     */
  }

  protected void firePropertyChange(final String _prop, final boolean _new) {
    /*
     * if (listener_ != null) { listener_.firePropertyChange(_prop, !_new, _new); }
     */
  }

  /**
   * Met a jour l'iterateur reutilisable. Faire attention lorsque on parcourt cet iterateur: Acces concurrents non geres.
   */
  protected void updateReuseIterator() {
    // on cree si necessaire l'iterateur principal pour le cloner.
    if (axisIterator_ == null) {
      buildUpToDateMainTickIterator();
    }
    // prob on laisse tomber
    if (axisIterator_ == null) {
      return;
    }
    // le reuse iterateur n'est pas correct: on clone le principal
    if (reuseIterator_ == null || (!reuseIterator_.getClass().equals(axisIterator_.getClass()))) {
      try {
        reuseIterator_ = (TickIterator) axisIterator_.clone();
      } catch (final CloneNotSupportedException _ex) {
        // ne doit jamais arrive
        FuLog.warning(_ex.getMessage());
      }
    }
    if (reuseIterator_ != null) {
      reuseIterator_.init(getMinimum(), getMaximum(), nbPas_);
    }

  }

  /**
   * Permet d'ajuster les bornes de cet axe par rapport � _a : si le min de _a est inf au min de cet axe, le this.min est modifie.
   *
   * @param _a l'axe a ajuster
   */
  public void adjustBounds(final EGAxe _a) {
    if (_a != null) {
      if (_a.range_.max_ > range_.max_) {
        range_.max_ = _a.range_.max_;
      }
      if (_a.range_.min_ < range_.min_) {
        range_.min_ = _a.range_.min_;
      }
    }
  }

  public boolean containsPoint(final double _x) {
    return range_.isValueContained(_x);
  }

  public boolean isContained(final double _xmin, final double _xmax) {
    return range_.max_ <= _xmax && range_.min_ >= _xmin;
  }

  public final TickIterator getAxisIterator() {
    return axisIterator_;
  }

  public double getEcart() {
    final double r = getMaximum() - getMinimum();
    return r < 0 ? -r : r;
  }

  public Font getFont() {
    return font_;
  }

  public Color getLineColor() {
    return lineColor_;
  }

  public double getMaximum() {
    return range_.isNill() ? 10 : range_.getMax();
  }

  public boolean isRangeInit() {
    return !range_.isNill();
  }

  /*
   * public double getPas(){ return getInitializedTickIterator().getIncrement(); }
   */
  public double getMinimum() {
    return range_.isNill() ? 0 : range_.getMin();
  }

  /**
   * @return le nombre de pas max dessine par cet axe
   */
  public final int getNbPas() {
    return nbPas_;
  }

  public double getPositionFromValue(final double _v) {
    if (axisIterator_ == null) {
      return _v;
    }
    return axisIterator_.getPositionFromValue(_v);
  }

  public final CtuluNumberFormatI getSpecificFormat() {
    return specificFormat_;
  }

  /**
   * @param _val la valeur a considerer
   * @return la chaine a utiliser pour l'affichage
   */
  public String getStringAffiche(final double _val) {
    if (specificFormat_ != null) {
      return specificFormat_.format(_val);
    }
    return getUpdatedTickIterator().formatValue(_val);
  }

  /**
   * @param _val la valeur a considerer
   * @return la chaine a utiliser pour l'affichage de valeur interpolee
   */
  public String getStringInterpolatedAffiche(final double _val, boolean detail) {
    if (detail && specificDetailFormat_ != null) {
      return specificDetailFormat_.format(_val);
    }
    if (specificFormat_ != null) {
      return specificFormat_.format(_val);
    }
    return getUpdatedTickIterator().formatSubValue(_val);
  }

  public String getTitre() {
    return titre_;
  }

  public String getUnite() {
    return unite_;
  }

  /**
   * @return l'editeur utilise pour modifier les bornes de l'axe
   */
  public final CtuluValueEditorI getValueEditor() {
    if (valueEditor_ != null) {
      return valueEditor_;
    }
    if (specificFormat_ == null) {
      return CtuluValueEditorDefaults.DOUBLE_EDITOR;
    }
    CtuluValueEditorDouble res = new CtuluValueEditorDouble();
    res.setFormatter(specificFormat_);
    return res;
  }

  public String getAxeTexte() {
    final String t = titreVisible_ ? titre_ : CtuluLibString.EMPTY_STRING;
    if (uniteVisible_ && (unite_ != null) && unite_.length() > 0) {
      return t + " [" + unite_ + ']';
    }
    return t;
  }

  public double getValueFromPosition(final double _v) {
    if (axisIterator_ == null) {
      return _v;
    }
    return axisIterator_.getValueFromPosition(_v);
  }

  public boolean isGraduations() {
    return graduations_;
  }

  public boolean isLogarithmique() {
    return axisIterator_ != null
            && (axisIterator_ instanceof LogarithmicNumberIterator || modeGraduations_ == LOGARITHMIQUE);

  }

  public boolean isSpecificFormat() {
    return specificFormat_ != null;
  }

  public CtuluNumberFormatI getSpecificDetailFormat() {
    return specificDetailFormat_;
  }

  public boolean isTitreVisible() {
    return titreVisible_;
  }

  public boolean isUniteVisible() {
    return uniteVisible_;
  }

  public abstract boolean isVertical();

  public boolean isVisible() {
    return visible_;
  }
  private TickIterator userIterator;

  public final void setAxisIterator(final TickIterator _axisIterator) {
    this.userIterator = _axisIterator;
    axisIterator_ = _axisIterator;
    isIteratorUptodate_ = false;
  }

  /**
   * @param _min
   * @param _max
   * @return true si changement
   */
  public boolean setBounds(final double _min, final double _max) {
    return setBounds(_min, _max, -1);
  }

  /**
   * @param _min
   * @param _max
   * @param _nbPas
   * @return true si changement
   */
  public boolean setBounds(final double _min, final double _max, final int _nbPas) {
    return setBounds(_min, _max, _nbPas, 2);

  }

  /**
   * SetBounds versions nombre de graduations
   *
   * @param _min
   * @param _max
   * @param _nbPas
   * @param _mode
   * @return
   */
  public boolean setBounds(final double _min, final double _max, final int _nbPas, final int _mode) {
    final double oldMin = getMinimum();
    final double oldMax = getMaximum();
    final int oldPas = nbPas_;
    final int oldMode = modeGraduations_;

    if (_mode >= MANUEL_GRADUATIONS && _mode <= LOGARITHMIQUE) {
      modeGraduations_ = _mode;
    }

    range_.min_ = Math.min(_min, _max);
    range_.max_ = Math.max(_min, _max);
    if (range_.min_ >= range_.max_) {
      range_.min_ = (range_.min_ + range_.max_) * 0.5 - 0.5;
      range_.max_ = range_.min_ + 1;
    }
    if (isLogarithmique()) {
      range_.min_ = Math.max(0, range_.min_);
      range_.max_ = Math.max(0, range_.max_);
    }

    if (_nbPas > 0) {
      nbPas_ = _nbPas;
    }
    if (nbPas_ <= 0) {
      nbPas_ = TickIterator.DEFAULT_STICK_NUMBER;
    }
    if (oldPas != nbPas_ || Double.doubleToLongBits(oldMin) != Double.doubleToLongBits(range_.min_)
            || Double.doubleToLongBits(oldMax) != Double.doubleToLongBits(range_.max_) || oldMode != modeGraduations_) {
      isIteratorUptodate_ = false;
      firePropertyChange(PROP_BOUNDS);
      return true;
    }
    return false;

  }

  /**
   * SetBounds versions nombre de graduations
   *
   * @param _min
   * @param _max
   * @param _nbPas
   * @param _mode
   * @return
   */
  public boolean setBounds(final double _min, final double _max, final double _longueurPas, final int _mode) {
    final double oldMin = getMinimum();
    final double oldMax = getMaximum();
    final double oldLongPas = longueurPas_;
    final int oldMode = modeGraduations_;

    if (_mode >= MANUEL_GRADUATIONS && _mode <= LOGARITHMIQUE) {
      modeGraduations_ = _mode;
    }

    range_.min_ = Math.min(_min, _max);
    range_.max_ = Math.max(_min, _max);
    if (range_.min_ >= range_.max_) {
      range_.min_ = (range_.min_ + range_.max_) * 0.5 - 0.5;
      range_.max_ = range_.min_ + 1;
    }
    if (isLogarithmique()) {
      range_.min_ = Math.max(0, range_.min_);
      range_.max_ = Math.max(0, range_.max_);
    }

    if (_longueurPas < 0) {
      nbPas_ = TickIterator.DEFAULT_STICK_NUMBER;
      longueurPas_ = (int) ((_max - _min) / nbPas_);
    } else {
      longueurPas_ = _longueurPas;
      nbPas_ = (int) ((_max - _min) / longueurPas_);
    }

    if (nbPas_ <= 0) {
      nbPas_ = TickIterator.DEFAULT_STICK_NUMBER;
    }
    if (oldLongPas != longueurPas_ || Double.doubleToLongBits(oldMin) != Double.doubleToLongBits(range_.min_)
            || Double.doubleToLongBits(oldMax) != Double.doubleToLongBits(range_.max_) || oldMode != modeGraduations_) {
      isIteratorUptodate_ = false;
      firePropertyChange(PROP_BOUNDS);
      return true;
    }
    return false;

  }

  public boolean setFont(final Font _font) {
    if (!CtuluLib.isEquals(font_, _font)) {
      final Font old = font_;
      font_ = _font;
      firePropertyChange(BSelecteurReduitFonteNewVersion.PROPERTY, old, font_);
      return true;
    }
    return false;
  }

  public boolean setGraduations(final boolean _graduations) {
    if (graduations_ != _graduations) {
      graduations_ = _graduations;
      firePropertyChange(PROP_GRADUATIONS, graduations_);
      return true;
    }
    return false;
  }

  // }
  public boolean setLineColor(final Color _lineColor) {
    if (lineColor_ != _lineColor) {
      final Color old = lineColor_;
      lineColor_ = _lineColor;
      firePropertyChange(BSelecteurColorChooser.DEFAULT_PROPERTY, old, lineColor_);
      return true;
    }
    return false;
  }

  public final void setSpecificFormat(final CtuluNumberFormatI _specificFormat) {
    if (specificFormat_ != _specificFormat) {
      final CtuluNumberFormatI old = specificFormat_;
      specificFormat_ = _specificFormat;
      firePropertyChange("axeFormat", old, specificFormat_);
    }
  }

  /**
   *
   * @param _specificFormat format used for tooltips.
   */
  public final void setSpecificDetailFormat(final CtuluNumberFormatI _specificFormat) {
    if (specificDetailFormat_ != _specificFormat) {
      final CtuluNumberFormatI old = specificDetailFormat_;
      specificDetailFormat_ = _specificFormat;
      firePropertyChange("axeDetailFormat", old, specificDetailFormat_);
    }
  }

  public boolean setTitre(final String _titre) {
    if (!CtuluLibString.isEquals(titre_, _titre)) {
      final String old = titre_;
      titre_ = _titre;
      firePropertyChange(BSelecteurTextField.TITLE_PROPERTY, old, titre_);
      return true;
    }
    return false;
  }

  public boolean setTitreVisible(final boolean _new) {
    if (_new != titreVisible_) {
      titreVisible_ = _new;
      firePropertyChange(PROP_TITLE_VISIBLE, titreVisible_);
      return true;
    }
    return false;
  }

  public void setUnite(final Object _unite) {
    setUnite(_unite == null ? CtuluLibString.EMPTY_STRING : _unite.toString());
  }

  public void setUnite(final String _unite) {
    if (unite_ != _unite) {
      unite_ = _unite;
      firePropertyChange("axeUnit");
    }
  }

  public boolean setUniteVisible(final boolean _new) {
    if (_new != uniteVisible_) {
      uniteVisible_ = _new;
      firePropertyChange(PROP_UNIT_VISIBLE, uniteVisible_);
      return true;
    }
    return false;
  }
  boolean titleModifiable = true;
  boolean uniteModifiable = true;

  public void setTitleModifiable(boolean titleModifiable) {
    this.titleModifiable = titleModifiable;
  }

  public boolean isUniteModifiable() {
    return uniteModifiable;
  }

  public void setUniteModifiable(boolean uniteModifiable) {
    this.uniteModifiable = uniteModifiable;
  }

  public boolean isTitleModifiable() {
    return false;
  }

  /**
   * @param _valueEditor l'editeur � utiliser pour modifier les bornes de l'axe
   */
  public final void setValueEditor(final CtuluValueEditorI _valueEditor) {
    valueEditor_ = _valueEditor;
    firePropertyChange("axeValueEditor", uniteVisible_);
  }

  public boolean setVisible(final boolean _visible) {
    if (visible_ != _visible) {
      visible_ = _visible;
      firePropertyChange(BSelecteurCheckBox.PROP_VISIBLE, visible_);
      return true;
    }
    return false;
  }

  public boolean isTraceGrille() {
    return traceGrille_;
  }

  public boolean setTraceGrille(final boolean traceGrille) {
    if (this.traceGrille_ != traceGrille) {
      this.traceGrille_ = traceGrille;
      firePropertyChange(PROP_IS_GRID_GRADUATIONS_PAINTED, Boolean.valueOf(!traceGrille_), Boolean.valueOf(traceGrille_));
      return true;
    }
    return false;
  }

  public boolean isTraceSousGrille() {
    return traceSousGrille_;
  }

  public boolean setTraceSousGrille(final boolean traceSousGrille) {
    if (traceSousGrille != traceSousGrille_) {
      this.traceSousGrille_ = traceSousGrille;
      firePropertyChange(PROP_IS_GRID_SOUS_GRADUATIONS_PAINTED, Boolean.valueOf(!traceSousGrille_), Boolean.valueOf(traceSousGrille_));
      return true;
    }
    return false;

  }

  /**
   * @return the modeGraduations_
   */
  public int getModeGraduations() {
    return modeGraduations_;
  }

  /**
   * @param modeGraduations the modeGraduations_ to set
   */
  public void setModeGraduations(final int modeGraduations) {
    if (modeGraduations_ != modeGraduations) {
      isIteratorUptodate_ = false;
      firePropertyChange(PROP_BOUNDS);
      modeGraduations_ = modeGraduations;
    }
  }

  /**
   * @return the traceGraduations_
   */
  public TraceLigneModel getTraceGraduations() {
    return traceGraduations_;
  }

  /**
   * @param traceGraduations the traceGraduations_ to set
   */
  public boolean setTraceGraduations(final TraceLigneModel traceGraduations) {
    if (traceGraduations == null) {
      return false;
    }
    if (!traceGraduations.equals(traceGraduations_)) {
      traceGraduations_.updateData(traceGraduations);
      firePropertyChange(PROP_TRACE_GRADUATIONS, Boolean.TRUE, Boolean.FALSE);
      return true;
    }
    return false;
  }

  /**
   * @return the traceSousGraduations_
   */
  public TraceLigneModel getTraceSousGraduations() {
    return traceSousGraduations_;
  }

  /**
   * @param traceSousGraduations the traceSousGraduations_ to set
   */
  public boolean setTraceSousGraduations(final TraceLigneModel traceSousGraduations) {
    if (traceSousGraduations == null) {
      return false;
    }
    if (!traceSousGraduations.equals(traceSousGraduations_)) {
      traceSousGraduations_.updateData(traceSousGraduations);
      firePropertyChange(PROP_TRACE_SOUS_GRADUATIONS, Boolean.TRUE, Boolean.FALSE);
      return true;
    }
    return false;
  }
}
