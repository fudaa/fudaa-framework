/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

/**
 * Interface pour les courbes qui ne peuvent pas �tre export�es sous format tabulaire
 * @author Frederic Deniger
 */
public interface EGModelExportable {
  
  
  boolean isViewableinTable();
  
}
