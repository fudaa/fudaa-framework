/*
 *  @creation     5 ao�t 2004
 *  @modification $Date: 2007-06-28 09:26:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.event.MouseEvent;
import javax.swing.JTable;
import org.fudaa.ctulu.gui.CtuluPopupListener;

/**
 * @author Fred Deniger
 * @version $Id: EGListSimple.java,v 1.8 2007-06-28 09:26:47 deniger Exp $
 */
public class EGListSimple extends JTable implements CtuluPopupListener.PopupReceiver {

  EGSpecificActions actions_;

  public EGListSimple() {
    new CtuluPopupListener(this, this);
  }

  @Override
  public void popup(final MouseEvent _e) {
    if (actions_ != null) {
      actions_.buildPopupMenu(_e);
    }
  }

  /**
   * @param _newActions les nouvelles actions a considerer.
   */
  public void setSpecificAction(final EGSpecificActions _newActions) {
    actions_ = _newActions;
  }

}
