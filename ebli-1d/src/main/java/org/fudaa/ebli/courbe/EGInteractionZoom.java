/*
 * @creation 22 juin 2004
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionZoom.java,v 1.13 2007-05-04 13:49:41 deniger Exp $
 */
public class EGInteractionZoom extends EGInteractiveComponent implements MouseListener, KeyListener,
    MouseMotionListener {

  private boolean zoomMoins_;

  Point pt1_;

  Point pt2_;

  final EGGraphe target_;

  public EGInteractionZoom(final EGGraphe _target) {
    target_ = _target;
    target_.addMouseListener(this);
    target_.addMouseMotionListener(this);
  }

  private void paintZoomRect() {
    repaint();
  }

  private void setZoomMoins(final boolean _b) {
    if (_b != zoomMoins_) {
      zoomMoins_ = _b;
      internFireStateChanged();
    }
  }

  @Override
  public String getDescription() {
    return "zoom" + (zoomMoins_ ? " -" : " +");
  }

  public EGGraphe getTarget() {
    return target_;
  }

  @Override
  public void keyPressed(final KeyEvent _e) {
    if ((isActive()) && (_e.getKeyCode() == KeyEvent.VK_SHIFT)) {
      setZoomMoins(_e.isShiftDown());
    }
  }

  @Override
  public void keyReleased(final KeyEvent _e) {
    if ((isActive()) && (_e.getKeyCode() == KeyEvent.VK_SHIFT)) {
      setZoomMoins(false);
    }
  }

  @Override
  public void keyTyped(final KeyEvent _e) {}

  @Override
  public void mouseClicked(final MouseEvent _e) {}

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }

    if (!isActive() || (pt1_ == null)) {
      return;
    }
    final Point f = _e.getPoint();
    if (pt2_ != null) {
      paintZoomRect();
      pt2_ = _e.getPoint();
      paintZoomRect();
    }
    if (f.distance(pt1_) > 7) {
      pt2_ = _e.getPoint();
      paintZoomRect();
    }
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {}

  @Override
  public void mouseExited(final MouseEvent _e) {}

  @Override
  public void mouseMoved(final MouseEvent _e) {}

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (_e.isConsumed() || EbliLib.isPopupMouseEvent(_e)) {
      return;
    }
    pt1_ = _e.getPoint();
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (_e.isConsumed() || EbliLib.isPopupMouseEvent(_e)|| SwingUtilities.isMiddleMouseButton(_e)) {
      return;
    }

    if (isActive() && (target_ != null)) {
      if (pt2_ == null) {
        target_.setZoomOnPoint(_e.getX(), _e.getY(), _e.isShiftDown(), 0.75);
      } else {
        paintZoomRect();
        target_.setZoomOn(pt1_, pt2_);
        pt2_ = null;
      }
    }
  }

  @Override
  public void paintComponent(final Graphics _g) {
    if ((pt1_ != null) && (pt2_ != null)) {
      _g.drawLine(pt1_.x, pt1_.y, pt1_.x, pt2_.y);
      _g.drawLine(pt1_.x, pt1_.y, pt2_.x, pt1_.y);
      _g.drawLine(pt2_.x, pt2_.y, pt2_.x, pt1_.y);
      _g.drawLine(pt2_.x, pt2_.y, pt1_.x, pt2_.y);
    }
  }

  @Override
  public void setActive(final boolean _b) {
    target_.requestFocus();
    target_.requestFocusInWindow();
    super.setActive(_b);
  }

}