/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

/**
 *
 * @author Frederic Deniger
 */
public class EGTimeLabel implements Comparable<EGTimeLabel> {

  private final double x;
  private String label;
  private String subLabel;

  public EGTimeLabel(double x) {
    this.x = x;
  }

  public String getLabel() {
    return label;
  }

  public double getX() {
    return x;
  }

  public String getSubLabel() {
    return subLabel;
  }

  public void setSubLabel(String subLabel) {
    this.subLabel = subLabel;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public int compareTo(EGTimeLabel o) {
    if (o == this) {
      return 0;
    }
    if (o == null) {
      return 1;
    }
    if (equals(o)) {
      return 0;
    }
    return x - o.x > 0 ? 1 : -1;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 89 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final EGTimeLabel other = (EGTimeLabel) obj;
    if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
      return false;
    }
    return true;
  }

}
