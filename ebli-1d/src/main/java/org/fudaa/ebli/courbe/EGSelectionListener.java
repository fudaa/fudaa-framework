/*
 *  @creation     6 juil. 2004
 *  @modification $Date: 2006-04-12 15:28:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionListener;


/**
 * @author Fred Deniger
 * @version $Id: EGSelectionListener.java,v 1.3 2006-04-12 15:28:05 deniger Exp $
 */
public interface EGSelectionListener extends TreeSelectionListener, ListSelectionListener {

}
