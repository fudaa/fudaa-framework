package org.fudaa.ebli.courbe;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Classe persistante de la courbe
 *
 * @author Adrien Hadoux
 */
@XStreamAlias("Curve")
public class EGCourbePersist extends EGPersist {
  @XStreamAlias("Title")
  String title_;
  @XStreamAlias("IdGroup")
  int Idgroup;
  @XStreamAlias("Id")
  int id = -1;
  @XStreamAlias("NuagePoint")
  boolean nuagePoints = false;
  // /-- les min et max sont purement informatifs --//
  @XStreamOmitField
  double Xmin;
  @XStreamOmitField
  double Xmax;
  @XStreamOmitField
  double Ymin;
  @XStreamOmitField
  double Ymax;
  /**
   * @deprecated kept for old data
   */
  @XStreamOmitField
  @Deprecated
  double[] abscisses;
  /**
   * @deprecated kept for old data
   */
  @Deprecated
  @XStreamOmitField
  double[] ordonnees;
  // -- proprietes graphiques --//
  @XStreamAlias("TraceBox")
  TraceBox tracebox;
  @XStreamAlias("Line")
  TraceLigneModel lineModel_;
  @XStreamAlias("MarkLine")
  TraceLigneModel tLigneMarqueur_;
  @XStreamAlias("Icon")
  TraceIconModel iconeModel;
  @XStreamAlias("IconSpecific")
  TraceIconModel iconeModelSpecific;
  // -- data specifiques --//
  @XStreamOmitField
  Object dataSpecifiques;
  @XStreamOmitField
  String classeModel;
  // -- liste des marqueurs --//
  @XStreamImplicit(itemFieldName = "Marks")
  List<EGCourbeMarqueur> listeMarqueurs_;
  @XStreamAlias("Surface")
  EGCourbeSurfacePersist surfacePainter;
  // -- inverse ou non les x et y --//
  @XStreamAlias("Inverse")
  boolean inverse = false;
  @XStreamAlias("DisplayLabels")
  boolean displayLabels = false;
  @XStreamAlias("DisplayTitle")
  boolean displayTitle = false;
  @XStreamAlias("VerticalLabels")
  boolean verticalLabels = false;
  @XStreamAlias("Visible")
  private Boolean visible = true;
  @XStreamAlias("Colors")
  private HashMap<String, Color> colors = new HashMap<>();

  public EGCourbePersist() {
  }

  public EGCourbePersist(EGCourbePersist other) {
    super(other);
    if (other != null) {
      this.title_ = other.title_;
      this.id = other.id;
      this.Idgroup = other.Idgroup;
      this.nuagePoints = other.nuagePoints;
      this.displayLabels = other.displayLabels;
      this.displayTitle = other.displayTitle;
      this.inverse = other.inverse;
      this.visible=other.visible;
      this.verticalLabels=other.verticalLabels;
      if (other.tracebox != null) {
        this.tracebox = new TraceBox(other.tracebox);
      }
      if (other.iconeModel != null) {
        this.iconeModel = new TraceIconModel(other.iconeModel);
      }
      if (other.iconeModelSpecific != null) {
        this.iconeModelSpecific = new TraceIconModel(other.iconeModelSpecific);
      }
      if (other.lineModel_ != null) {
        this.lineModel_ = new TraceLigneModel(other.lineModel_);
      }
      if (other.tLigneMarqueur_ != null) {
        this.tLigneMarqueur_ = new TraceLigneModel(other.tLigneMarqueur_);
      }
      if(other.listeMarqueurs_!=null){
        this.listeMarqueurs_=new ArrayList<>();
        other.listeMarqueurs_.stream().forEach(i-> listeMarqueurs_.add(new EGCourbeMarqueur(i)));
      }
      if(other.colors!=null){
        this.colors= (HashMap<String, Color>) other.colors.clone();
      }else{
        this.colors=null;
      }
      if(other.surfacePainter!=null){
        this.surfacePainter= new EGCourbeSurfacePersist(other.surfacePainter);
      }
    }
  }

  public EGCourbePersist duplicate() {
    return new EGCourbePersist(this);
  }

  public void setColor(String key, Color color) {
    if (colors == null) {
      colors = new HashMap<>();
    }
    colors.put(key, color);
  }

  public Color getColor(String key) {
    if (colors == null) {
      return null;
    }
    return colors.get(key);
  }

  /**
   * @return
   */
  public boolean isVisible() {
    //test bizarre pour la compatibilité descendante: anciennes versions ne contenants pas cette valeur visible.
    return visible == null || Boolean.TRUE.equals(visible);
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public TraceLigneModel getLineModel_() {
    return lineModel_;
  }

  public String getTitle_() {
    return title_;
  }

  public void setTitle_(String title_) {
    this.title_ = title_;
  }

  public boolean isNuagePoints() {
    return nuagePoints;
  }

  public void setNuagePoints(boolean nuagePoints) {
    this.nuagePoints = nuagePoints;
  }

  public TraceBox getTracebox() {
    return tracebox;
  }

  public void setTracebox(TraceBox tracebox) {
    this.tracebox = tracebox;
  }

  public TraceLigneModel gettLigneMarqueur() {
    return tLigneMarqueur_;
  }

  public void settLigneMarqueur(TraceLigneModel tLigneMarqueur_) {
    this.tLigneMarqueur_ = tLigneMarqueur_;
  }

  public TraceIconModel getIconeModel() {
    return iconeModel;
  }

  public TraceIconModel getIconeModelSpecific() {
    return iconeModelSpecific;
  }

  public void setIconeModel(TraceIconModel iconeModel) {
    this.iconeModel = iconeModel;
  }

  public void setIconeModelSpecific(TraceIconModel iconeModelSpecific) {
    this.iconeModelSpecific = iconeModelSpecific;
  }

  public List<EGCourbeMarqueur> getListeMarqueurs() {
    return listeMarqueurs_;
  }

  public void setListeMarqueurs(List<EGCourbeMarqueur> listeMarqueurs_) {
    this.listeMarqueurs_ = listeMarqueurs_;
  }

  public EGCourbeSurfacePersist getSurfacePainter() {
    return surfacePainter;
  }

  public void setSurfacePainter(EGCourbeSurfacePersist surfacePainter) {
    this.surfacePainter = surfacePainter;
  }

  public void setLineModel(TraceLigneModel lineModel_) {
    this.lineModel_ = lineModel_;
  }

  public boolean isDisplayLabels() {
    return displayLabels;
  }

  public void setDisplayLabels(boolean displayLabels) {
    this.displayLabels = displayLabels;
  }

  public boolean isVerticalLabels() {
    return verticalLabels;
  }

  public void setVerticalLabels(boolean verticalLabels) {
    this.verticalLabels = verticalLabels;
  }

  public boolean isDisplayTitle() {
    return displayTitle;
  }

  public void setDisplayTitle(boolean displayTitle) {
    this.displayTitle = displayTitle;
  }

  public String getTitle() {
    return title_;
  }

  public void setTitle(String _title) {
    this.title_ = _title;
  }

  public int getIdgroup() {
    return Idgroup;
  }

  public void setIdgroup(int _idgroup) {
    Idgroup = _idgroup;
  }

  public int getSpecificIntValue(String string, int defaultValue) {
    Object o = getSpecificValue(string);
    if (o == null) {
      return defaultValue;
    }
    return ((Integer) o).intValue();
  }

  public boolean getSpecificBooleanValue(String string) {
    Object o = getSpecificValue(string);
    return Boolean.TRUE.equals(o);
  }

  /**
   * @return the id
   */
  protected int getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  protected void setId(int id) {
    this.id = id;
  }
}
