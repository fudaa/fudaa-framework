/*
 * @creation 9 ao�t 2004
 * @modification $Date: 2006-07-13 13:35:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.event.InputEvent;

/**
 * Permet de gerer les modes de selections selon les touches pressees.
 *
 * @author Fred Deniger
 * @version $Id: EGSelectionState.java,v 1.6 2006-07-13 13:35:43 deniger Exp $
 */
public class EGSelectionState {

  /**
   * La selection doit etre remplacee.
   */
  public final static int ACTION_REPLACE = 0;
  /**
   * Ajoute/Enleve a la selection.
   */
  public final static int ACTION_ADD_DEL = 1;

  /**
   * Selectionne entre 2 points.
   */
  public final static int ACTION_MAJ = 2;
  private String controleDesc_;
  private int modificateur_;

  public EGSelectionState() {
    super();
  }

  /**
   * @param _e l'evt support
   */
  public void majControleDesc(final InputEvent _e) {
    if (_e.isShiftDown()) {
      if (!_e.isControlDown()) {
        controleDesc_ = " (+[])";
        modificateur_ = EGSelectionState.ACTION_MAJ;
      }
    } else if (_e.isControlDown()) {
      if (!_e.isAltDown()) {
        controleDesc_ = " (+/-)";
        modificateur_ = EGSelectionState.ACTION_ADD_DEL;
      }
    } else {
      controleDesc_ = null;
      modificateur_ = EGSelectionState.ACTION_REPLACE;
    }
  }

  public final String getControleDesc() {
    return controleDesc_;
  }

  public final int getModificateur() {
    return modificateur_;
  }

  /**
   * @return true si mode ACTION_REPLACE
   */
  public boolean isReplaceMode() {
    return modificateur_ == ACTION_REPLACE;
  }

  /**
   * @return true si mode ACTION_ADD
   */
  public boolean isAddMode() {
    return modificateur_ == ACTION_ADD_DEL;
  }

  /**
   * @return true si mode ACTION_XOR
   */
  public boolean isMajMode() {
    return modificateur_ == ACTION_MAJ;
  }

  public final void setModificateur(final int _modificateur) {
    modificateur_ = _modificateur;
  }

}