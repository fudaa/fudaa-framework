/*
 * @creation 23 juin 2004
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import javax.swing.SwingConstants;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceIcon;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionSuivi.java,v 1.13 2006-09-19 14:55:53 deniger Exp $
 */
public class EGInteractionSuiviAllOrdonnees extends EGInteractiveComponent implements MouseListener,
        MouseMotionListener/* , EGGrapheModelListener */ {

  boolean dessine_;
  TraceIcon selected_;
  TraceIcon normal_;
  final EGGraphe target_;
  TraceBox tb_;
  Color traceColor_;
  EGAxeVertical v_;
  double xreel_;

  // TreeTableModelGraphe modelGraphe_ = new TreeTableModelGraphe();
  // JXTreeTable treeTableNodes_ = new JXTreeTable(modelGraphe_);
  // HashMap<EGCourbe, Double> mapCurvesY_ = new HashMap<EGCourbe, Double>();
  // JLabel labelX_ = new JLabel("");
  // public class TreeTableModelGraphe extends DefaultTreeTableModel {
  // String[] titre_;
  //
  // // -- data correspondant au x donn� --//
  // double[] dataY_ = new double[0];
  //    
  // public TreeTableModelGraphe() {
  // String[] val = { "Label", "Y" };
  // titre_ = val;
  // }
  //
  //   
  // public int getColumnCount() {
  //      
  //      
  // return titre_.length;
  // }
  //
  // @Override
  // public boolean isCellEditable(Object _node, int _column) {
  // return false;
  // }
  //
  //
  // public String getColumnName(int _columnIndex) {
  // return titre_[_columnIndex];
  // }
  //
  // public int getRowCount() {
  // return target_.getModel().getCourbes().length;
  //      
  // }
  //
  // public Object getValueAt(Object node, int column) {
  // Object res = "n/a";
  // if (node instanceof DefaultMutableTreeTableNode) {
  // DefaultMutableTreeTableNode defNode = (DefaultMutableTreeTableNode) node;
  // if (defNode.getUserObject() instanceof EGGroup) {
  // if (column == 0)
  // return ((EGGroup)defNode.getUserObject()).getTitle();
  // else
  // return "";
  // }else
  // if (defNode.getUserObject() instanceof EGCourbeChild) {
  // if (column == 0)
  // return ((EGCourbeChild) defNode.getUserObject()).getTitle();
  // else
  // return getValueForSelectedX(((EGCourbeChild) defNode.getUserObject()));
  // }
  // }
  // return res;
  // }
  //    
  //    
  //    
  // }
// /** Methode qui construit la structure du model **/
  // public void constructStructureModel() {
  //
  // DefaultMutableTreeTableNode root = new
  // DefaultMutableTreeTableNode("Ordonn�es pour X=" + xreel_);
  //    
  // for (int i = 0; i < target_.getModel().getNbEGObject(); i++) {
  // final EGObject ci = target_.getModel().getEGObject(i);
  // if (ci instanceof EGGroup) {
  // EGGroup groupe = (EGGroup) ci;
  // DefaultMutableTreeTableNode group=new DefaultMutableTreeTableNode(groupe);
  // root.add(group);
  //        
  // for (int k = 0; k < groupe.getChildCount(); k++) {
  // EGCourbeChild courbe = groupe.getCourbeAt(k);
  // DefaultMutableTreeTableNode courb = new
  // DefaultMutableTreeTableNode(courbe);
  // group.add(courb);
  // }
  // }
  //
  //      
  // }
  //    
  // modelGraphe_.setRoot(root);
  //    
  //   
  // }
  // /**
  // * Recupere la valeure associee a la courbe dans la map
  // *
  // * @param child
  // * @return
  // */
  // private Double getValueForSelectedX(EGCourbe child) {
  // if (child != null && mapCurvesY_.containsKey(child))
  // return mapCurvesY_.get(child);
  // else
  // return new Double(0);
  // }
  /**
   * Retourne le composant graphique qui se met a jour auto en fonction du clic utilisateur.
   *
   * @return
   */
  // JPanel tableInfo_;
  //  
  // public JComponent getTableInfos() {
  //    
  // if (tableInfo_ == null) {
  // JPanel panel=new JPanel(new FlowLayout(FlowLayout.CENTER));
  // panel.add(new JLabel(EbliResource.EBLI.getString("ordonn�es pour X=")));
  // panel.add(labelX_);
  // tableInfo_ = new JPanel(new BorderLayout());
  // tableInfo_.add(panel,BorderLayout.NORTH);
  // tableInfo_.add(new JScrollPane(treeTableNodes_), BorderLayout.CENTER);
  // tableInfo_.add(treeTableNodes_.getTableHeader(), BorderLayout.PAGE_START);
  // }
  //    
  // return tableInfo_;
  // }
  @Override
  public String getDescription() {
    return EbliLib.getS("suivi");
  }
  double yreel_;
  int selectedPtIdx_;
  final EGActionPaletteAllCoordonnees paelette_;

  /**
   * @param _a la cible pour le suivi.
   */
  public EGInteractionSuiviAllOrdonnees(final EGGraphe _a, EGActionPaletteAllCoordonnees palette) {
    target_ = _a;
    target_.addMouseListener(this);
    target_.addMouseMotionListener(this);
    paelette_ = palette;
    // constructStructureModel();
    //
    //   
    // // final EGTreeCellRenderer treeCellRenderer = new EGTreeCellRenderer();
    // // treeTableNodes_.setTreeCellRenderer(treeCellRenderer);
    //    
    // treeTableNodes_.setLeafIcon((EGObject) _a.getModel().getCourbes()[0]);
    // treeTableNodes_.setOpenIcon((EGObject) _a.getModel().getEGObject(0));
    // treeTableNodes_.setClosedIcon((EGObject) _a.getModel().getEGObject(0));
    // treeTableNodes_.setRowHeight(30);
    // // treeTableNodes_.setCollapsedIcon(_a.getModel().getEGObject(0));
    //    
    // _a.getModel().addModelListener(this);

    // palette_ = new DialogPalette();
    // palette_.setContentPane(getTableInfos());
    // palette_.setTitle("Ordonn�es pour le X s�lectionn�");
    // palette_.setResizable(true);
    // palette_.setSize(200, 200);
    //      
    // this.addPropertyChangeListener(palette_);


  }
  int yPressed_;
  final HashMap<EGCourbe, Double> mapCurvesY_ = new HashMap<>();

  private void updateWithMouse(final int _x, final int _y) {
    if (!isActive()) {
      return;
    }

    xreel_ = target_.getTransformer().getXReel(_x);
    if (target_.getTransformer().getXAxe().isDiscret()) {
      xreel_ = (int) xreel_;
    }

    for (int i = 0; i < target_.getModel().getCourbes().length; i++) {
      final EGCourbe ci = target_.getModel().getCourbes()[i]; // target_.
      // getSelectedComponent();

      selectedPtIdx_ = ci.select(_x, _y, target_.getTransformer(), 5);
      // on dessine si un point selectionne ou si le point appartient a la
      // courbe

      v_ = ci.getAxeY();
      if (selectedPtIdx_ >= 0) {
        xreel_ = ci.getModel().getX(selectedPtIdx_);
        yreel_ = ci.getModel().getY(selectedPtIdx_);
      } else {
        yreel_ = ci.interpol(xreel_);
        if (target_.getTransformer().getXAxe().isLogarithmique()) {
          yPressed_ = (int) ci.interpolOnEcran(xreel_, target_.getTransformer());
        }
      }

      // -- ajout dans la map
      mapCurvesY_.put(ci, yreel_);
      // labelX_.setText("" + xreel_);

    }
    paelette_.maj(xreel_, mapCurvesY_);

    repaint();



  }

  private boolean isExactPtSelect() {
    return selectedPtIdx_ >= 0;
  }

  @Override
  protected void paintComponent(final Graphics _g) {
    if (isActive() && dessine_ && (v_ != null)) {
      if (tb_ == null) {
        tb_ = new TraceBox();
      }
      final EGRepere r = target_.getTransformer();
      // si l'axe des x demande des points fixes (entier) et pas de point exact
      // selectionne on ne fait rien.
      if (r.getXAxe().isDiscret() && !isExactPtSelect()) {
        return;
      }
      final int x = r.getXEcran(xreel_);
      final int y = r.getYEcran(yreel_, v_);
      if (isExactPtSelect()) {
        getSelectedIcone().paintIconCentre(_g, x, y);
      } else {
        getNormalIcone().paintIconCentre(_g, x, y);
      }
      boolean logAndEcart = false;
      if (r.getXAxe().isLogarithmique() && Math.abs(yPressed_ - y) > 3) {
        logAndEcart = true;
        getNormalIcone().paintIconCentre(_g, x, yPressed_);
      }
      final Color old = _g.getColor();
      _g.setColor(getTraceColor());
      final int mx = r.getMaxEcranY() + 2;
      _g.drawLine(x, r.getMinEcranY(), x, mx);

      final int xmin = v_.isDroite() ? r.getMinEcranX() : r.getMinEcranX() - target_.getOffSet(v_) - 3;
      final int xmax = v_.isDroite() ? r.getMaxEcranX() + target_.getOffSet(v_) + 3 : r.getMaxEcranX();
      _g.drawLine(xmin, y, xmax, y);
      if (logAndEcart) {
        _g.drawLine(xmin, yPressed_, xmax, yPressed_);
      }
      tb_.setColorText(r.getXAxe().getLineColor());
      tb_.getTraceLigne().setCouleur(r.getXAxe().getLineColor());
      tb_.setHPosition(SwingConstants.CENTER);
      tb_.setVPosition(SwingConstants.TOP);
      final String s = r.getXAxe().getStringInterpolatedAffiche(xreel_, true);
      tb_.paintBox((Graphics2D) _g, x, mx, s);
      tb_.setColorText(v_.getLineColor());
      tb_.getTraceLigne().setCouleur(v_.getLineColor());
      tb_.setHPosition(v_.isDroite() ? SwingConstants.LEFT : SwingConstants.RIGHT);
      tb_.setVPosition(SwingConstants.CENTER);
      final Font oldF = _g.getFont();
      _g.setColor(old);
      _g.setFont(oldF);

    }
  }

  public Color getTraceColor() {
    if (traceColor_ == null) {
      traceColor_ = Color.lightGray;
    }
    return traceColor_;
  }

  public TraceIcon getSelectedIcone() {
    if (selected_ == null) {
      selected_ = new TraceIcon(TraceIcon.CARRE_PLEIN, 4);
      selected_.setCouleur(Color.gray);
    }
    return selected_;
  }

  public TraceIcon getNormalIcone() {
    if (normal_ == null) {
      normal_ = new TraceIcon(TraceIcon.CROIX, 4);
      normal_.setCouleur(Color.gray);
    }
    return normal_;
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    if (!_e.isPopupTrigger()) {
      updateWithMouse(_e.getX(), _e.getY());
    }
    if (_e.getClickCount() == 2) {
      // dessine_ = !dessine_;
      dessine_ ^= true;
    }
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    dessine_ = true;
    updateWithMouse(_e.getX(), _e.getY());
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
  }

  /**
   * Methode appelee a chque activation
   */
  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    if (!_b) {
      v_ = null;
      if (target_ != null) {
        target_.repaint();




      }
      // palette_.firePropertyChange("visible", 1, 0);

    }
    // palette_.firePropertyChange("visible", 0, 1);
  }
  // public void axeAspectChanged(EGAxe _c) {
  // constructStructureModel();
  //
  // }
  //
  // public void axeContentChanged(EGAxe _c) {
  // constructStructureModel();
  //
  // }
  //
  // public void courbeAspectChanged(EGObject _c, boolean _visibil) {
  // constructStructureModel();
  //
  // }
  //
  // public void courbeContentChanged(EGObject _c, boolean _mustRestore) {
  // constructStructureModel();
  //
  // }
  //
  // public void structureChanged() {
  // constructStructureModel();
  //
  //
  // }
}
