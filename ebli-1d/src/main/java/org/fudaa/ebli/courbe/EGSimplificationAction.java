/*
 * @creation 7 mai 07
 * @modification $Date: 2007-06-28 09:26:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Action permettant de simplifier des courbes.
 * 
 * @author fred deniger
 * @version $Id: EGSimplificationAction.java,v 1.2 2007-06-28 09:26:47 deniger Exp $
 */
public class EGSimplificationAction extends EbliActionSimple {

  final EGGraphe graphe_;

  public EGSimplificationAction(final EGGraphe _graphe) {
    super(EbliLib.getS("Simplifier les courbes"), EbliResource.EBLI.getToolIcon("clear"), "SIMPLIFY");
    graphe_ = _graphe;
  }

  @Override
  public void updateStateBeforeShow() {
    setEnabled(graphe_.getModel().isContentModifiable());
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    if (!graphe_.getModel().isSomethingToDisplay()) {
      CtuluLibDialog.showWarn(graphe_, getTitle(), EbliLib.getS("Aucune courbe n'est pr�sente"));
      return;
    }
    if (!graphe_.getModel().isContentModifiable()) {
      warnIfNoCurve();
      return;
    }
    EGObject[] obj = graphe_.getModel().getSelectedObjects();
    EGCourbe[] cs = null;
    if (!CtuluLibArray.isEmpty(obj)) {
      List curves = new ArrayList();
      for (int i = 0; i < obj.length; i++) {
        // non null
        obj[i].fillWithCurves(curves);
      }
      if (curves.size() > 0) cs = (EGCourbe[]) curves.toArray(new EGCourbe[0]);
    }
    if (cs == null) {
      EGCourbe[] all = graphe_.getModel().getCourbes();

      if (!canBeSimplified(all)) {
        warnIfNoCurve();
        return;
      }
      String yesText = EbliLib.getS("Simplifier toutes les courbes");
      String noText = BuResource.BU.getString("Non");
      if (CtuluLibDialog.showConfirmation(graphe_, getTitle(), EbliLib
          .getS("Voulez-vous simplifier toutes les courbes ?"), yesText, noText)) {
        cs = all;
      }
    } else if (cs.length > 0) {
      if (!canBeSimplified(cs)) {
        warnIfNoCurve();
        return;
      }
      String mes = null;
      if (cs.length == 1) {
        mes = EbliLib.getS("Voulez-vous simplifier la courbe s�lectionn�e ?");
      } else {
        mes = EbliLib.getS("Voulez-vous simplifier les {0} courbes s�lectionn�es ?", CtuluLibString
            .getString(cs.length));
      }
      String yesText = EbliLib.getS("Simplifier");
      String noText = BuResource.BU.getString("Ne pas simplifier");
      if (!CtuluLibDialog.showConfirmation(graphe_, getTitle(), mes, yesText, noText)) {
        return;
      }
    }
    if (!CtuluLibArray.isEmpty(cs)) {
      CtuluCommandComposite cmp = new CtuluCommandComposite();
      // non null;
      int nb = 0;
      for (int i = 0; i < cs.length; i++) {
        int[] idx = EvolutionOperations.getIdxRemovable(cs[i].getModel());
        if (!CtuluLibArray.isEmpty(idx)) {
          cs[i].getModel().removeValue(idx, cmp);
          nb++;
        }
      }

      graphe_.getCmd().addCmd(cmp.getSimplify());
      CtuluLibDialog.showConfirmation(graphe_, getTitle(), EbliLib.getS("{0} courbe(s) simplifi�e(s)", CtuluLibString
          .getString(nb)));

    } else {
      warnIfNoCurve();
    }

  }

  private boolean canBeSimplified(EGCourbe[] _cs) {
    for (int i = 0; i < _cs.length; i++) {
      if (_cs[i].getModel().isXModifiable()) return true;
    }
    return false;
  }

  private void warnIfNoCurve() {
    CtuluLibDialog.showWarn(graphe_, getTitle(), EbliLib.getS("Aucune courbe ne peut �tre modifi�e."));
  }
}
