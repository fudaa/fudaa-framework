/*
 * @creation 28 nov. 06
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.event.ListSelectionEvent;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurePaletteAction;

/**
 * @author fred deniger
 * @version $Id: EGConfigureActionPalette.java,v 1.2 2007-05-04 13:49:41 deniger Exp $
 */
public class EGConfigureActionPalette extends BConfigurePaletteAction implements EGSelectionListener {
  final EGGrapheModel model_;

  public EGConfigureActionPalette() {
    super(null);
    model_ = null;
    super.txtTitre=EbliLib.getS("Titre de la courbe");
    super.txtVisible=EbliLib.getS("Courbe visible");
  }

  public EGConfigureActionPalette(final EGGrapheTreeModel _model) {
    super(_model.getSelectionModel());
    model_ = null;
  }

  public EGConfigureActionPalette(final EGGrapheModel _model) {
    super(null);
    model_ = _model;
    _model.addSelectionListener(this);
    setTarget(_model.getSelectedObjects());
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    setTarget(model_.getSelectedObjects());
  }

}
