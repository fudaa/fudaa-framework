package org.fudaa.ebli.courbe;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Color;
import java.awt.Font;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Classe persistante des axe verticaux
 *
 * @author Adrien Hadoux
 *
 */
@XStreamAlias("VerticalAxis")
public class EGAxeVerticalPersist {

  @XStreamAlias("Right")
  boolean droite;
  @XStreamAlias("VerticalTitle")
  boolean titreVertical;
  @XStreamAlias("VerticalRightTitle")
  boolean titreVerticalDroite;
  @XStreamAlias("Title")
  String titre;
  @XStreamAlias("TitleIsVisible")
  boolean titreVisible = true;
  @XStreamAlias("Unit")
  String unite;
  @XStreamAlias("Visible")
  boolean visible;
  @XStreamAlias("Font")
  Font font;
  @XStreamAlias("Graduations")
  boolean graduations;
  @XStreamAlias("Grille")
  TraceLigne grille;
  @XStreamAlias("IsIteratorUptodate")
  boolean isIteratorUptodate;
  @XStreamAlias("LineColor")
  Color lineColor;
  @XStreamAlias("AxisRange")
  CtuluRange range;
  /**
   * Booleen qui indique si l'on trace ou non la graduation
   */
  @XStreamAlias("GridPainted")
  boolean traceGrille_ = false;
  /**
   * Booleen qui indique si l'on trcae
   */
  @XStreamAlias("MinorGridPainted")
  boolean traceSousGrille_ = false;
  @XStreamAlias("MarkLine")
  TraceLigneModel traceGraduations_ = null;
  @XStreamAlias("MinorMarkLine")
  TraceLigneModel traceSousGraduations_ = null;
  /**
   * Mode nb graduations
   */
  @XStreamAlias("Step")
  int nbPas_;
  /**
   * Mode on donne la longueur des pas
   */
  @XStreamAlias("StepLength")
  double longueurPas_;
  /**
   * le mode de graduation
   */
  @XStreamAlias("ModeGraduation")
  int modeGraduations_;
  @XStreamAlias("SizeMinorGraduation")
  int nbSousGraduations_;
  @XStreamAlias("ExtremDisplayed")
  private boolean isExtremiteDessinee_;

  public EGAxeVerticalPersist(EGAxeVertical axeY) {
    fillInfoWith(axeY);
  }

  public EGAxeVerticalPersist() {
  }

  public boolean isDroite() {
    return droite;
  }

  public void setDroite(boolean droite) {
    this.droite = droite;
  }

  public boolean isTitreVertical() {
    return titreVertical;
  }

  public void setTitreVertical(boolean titreVertical) {
    this.titreVertical = titreVertical;
  }

  public boolean isTitreVerticalDroite() {
    return titreVerticalDroite;
  }

  public void setTitreVerticalDroite(boolean titreVerticalDroite) {
    this.titreVerticalDroite = titreVerticalDroite;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public boolean isTitreVisible() {
    return titreVisible;
  }

  public void setTitreVisible(boolean titreVisible) {
    this.titreVisible = titreVisible;
  }

  public String getUnite() {
    return unite;
  }

  public void setUnite(String unite) {
    this.unite = unite;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public Font getFont() {
    return font;
  }

  public void setFont(Font font) {
    this.font = font;
  }

  public boolean isGraduations() {
    return graduations;
  }

  public void setGraduations(boolean graduations) {
    this.graduations = graduations;
  }

  public TraceLigne getGrille() {
    return grille;
  }

  public void setGrille(TraceLigne grille) {
    this.grille = grille;
  }

  public boolean isIsIteratorUptodate() {
    return isIteratorUptodate;
  }

  public void setIsIteratorUptodate(boolean isIteratorUptodate) {
    this.isIteratorUptodate = isIteratorUptodate;
  }

  public Color getLineColor() {
    return lineColor;
  }

  public void setLineColor(Color lineColor) {
    this.lineColor = lineColor;
  }

  public CtuluRange getRange() {
    return range;
  }

  public void setRange(CtuluRange range) {
    this.range = range;
  }

  public boolean isTraceGrille() {
    return traceGrille_;
  }

  public void setTraceGrille(boolean traceGrille_) {
    this.traceGrille_ = traceGrille_;
  }

  public boolean isTraceSousGrille() {
    return traceSousGrille_;
  }

  public void setTraceSousGrille(boolean traceSousGrille_) {
    this.traceSousGrille_ = traceSousGrille_;
  }

  public TraceLigneModel getTraceGraduations() {
    return traceGraduations_;
  }

  public void setTraceGraduations(TraceLigneModel traceGraduations_) {
    this.traceGraduations_ = traceGraduations_;
  }

  public TraceLigneModel getTraceSousGraduations() {
    return traceSousGraduations_;
  }

  public void setTraceSousGraduations(TraceLigneModel traceSousGraduations_) {
    this.traceSousGraduations_ = traceSousGraduations_;
  }

  public int getNbPas() {
    return nbPas_;
  }

  public void setNbPas(int nbPas_) {
    this.nbPas_ = nbPas_;
  }

  public double getLongueurPas() {
    return longueurPas_;
  }

  public void setLongueurPas(double longueurPas_) {
    this.longueurPas_ = longueurPas_;
  }

  public int getModeGraduations() {
    return modeGraduations_;
  }

  public void setModeGraduations(int modeGraduations_) {
    this.modeGraduations_ = modeGraduations_;
  }

  public int getNbSousGraduations() {
    return nbSousGraduations_;
  }

  public void setNbSousGraduations(int nbSousGraduations_) {
    this.nbSousGraduations_ = nbSousGraduations_;
  }

  public boolean isIsExtremiteDessinee() {
    return isExtremiteDessinee_;
  }

  public void setIsExtremiteDessinee(boolean isExtremiteDessinee_) {
    this.isExtremiteDessinee_ = isExtremiteDessinee_;
  }

  private void fillInfoWith(EGAxeVertical axeY) {
    this.droite = axeY.droite_;
    this.titreVertical = axeY.titreVertical_;
    this.titreVerticalDroite = axeY.titreVerticalDroite_;
    this.titre = axeY.titre_;
    this.titreVisible = axeY.titreVisible_;
    this.unite = axeY.unite_;
    this.visible = axeY.visible_;
    this.font = axeY.font_;
    this.graduations = axeY.graduations_;
//		this.grille = axeY.grille_;
    this.isIteratorUptodate = axeY.isIteratorUptodate_;
    this.lineColor = axeY.lineColor_;

    //-- persistance grilles et sous grilles --//
    traceGrille_ = axeY.traceGrille_;
    traceSousGrille_ = axeY.traceSousGrille_;
    traceGraduations_ = axeY.traceGraduations_;
    if (traceGraduations_ == null && grille != null) {
      axeY.traceSousGraduations_ = grille.getModel();
    }
    traceSousGraduations_ = axeY.traceSousGraduations_;
    nbPas_ = axeY.nbPas_;
    longueurPas_ = axeY.longueurPas_;
    modeGraduations_ = axeY.getModeGraduations();
    nbSousGraduations_ = axeY.nbSousGraduations_;
    range = axeY.range_;
    isExtremiteDessinee_ = axeY.isExtremiteDessinee_;
  }

  public void apply(EGAxeVertical axeY) {
    applyAllButRange(axeY);
    if (range != null) {
      axeY.range_.initWith(range);
    }
  }

  public EGAxeVertical generateAxe() {
    EGAxeVertical axeY = new EGAxeVertical();
    apply(axeY);
    return axeY;
  }

  public EGAxeVerticalPersist copy() {
    EGAxeVerticalPersist copy = new EGAxeVerticalPersist();
    copy.droite=droite;
    copy.titreVertical=titreVertical;
    copy.titreVerticalDroite=titreVerticalDroite;
    copy.titre=titre;
    copy.titreVisible=titreVisible;
    copy.unite=unite;
    copy.visible=visible;
    copy.font=font;
    copy.graduations=graduations;
    copy.grille=grille==null?null:new TraceLigne(grille);
    copy.isIteratorUptodate=isIteratorUptodate;
    copy.lineColor=lineColor;
    copy.range=range==null?null:new CtuluRange(range);
    copy.traceGrille_=traceGrille_;
    copy.traceSousGrille_=traceSousGrille_;
    copy.traceGraduations_=traceGraduations_==null?null:new TraceLigneModel(traceGraduations_);
    copy.traceSousGraduations_=traceSousGraduations_==null?null:new TraceLigneModel(traceSousGraduations_);
    copy.nbPas_=nbPas_;
    copy.longueurPas_=longueurPas_;
    copy.modeGraduations_=modeGraduations_;
    copy.nbSousGraduations_=nbSousGraduations_;
    copy.isExtremiteDessinee_=isExtremiteDessinee_;
    return copy;
  }

  public void applyAllButRange(EGAxeVertical axeY) {
    axeY.droite_ = this.droite;
    axeY.titreVertical_ = this.titreVertical;
    axeY.titreVerticalDroite_ = this.titreVerticalDroite;
    axeY.titre_ = this.titre;
    axeY.titreVisible_ = this.titreVisible;
    axeY.unite_ = this.unite;
    axeY.visible_ = this.visible;
    axeY.font_ = this.font;
    axeY.isExtremiteDessinee_ = this.isExtremiteDessinee_;
    axeY.graduations_ = this.graduations;
//		axeY.grille_ = this.grille;
    axeY.isIteratorUptodate_ = this.isIteratorUptodate;
    axeY.lineColor_ = this.lineColor;

    //-- persistance grilles et sous grilles --//
    axeY.traceGrille_ = traceGrille_;
    axeY.traceSousGrille_ = traceSousGrille_;
    axeY.traceGraduations_ = traceGraduations_;
    axeY.traceSousGraduations_ = traceSousGraduations_;
    axeY.nbPas_ = nbPas_;
    axeY.longueurPas_ = longueurPas_;
    axeY.setModeGraduations(modeGraduations_);
    axeY.nbSousGraduations_ = nbSousGraduations_;
    axeY.initDisplayValues();
  }
}
