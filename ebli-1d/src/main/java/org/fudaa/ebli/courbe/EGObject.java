/*
 * @creation 6 juil. 2004
 *
 * @modification $Date: 2007-05-04 13:49:41 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.courbe;

import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ebli.controle.BConfigurePalette;
import org.fudaa.ebli.controle.BConfigurePaletteTargetInterfaceReload;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.palette.BPaletteInfo;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: EGObject.java,v 1.17 2007-05-04 13:49:41 deniger Exp $
 */
public abstract class EGObject implements Icon, BConfigurePaletteTargetInterfaceReload {
  protected boolean isVisible_ = true;
  PropertyChangeSupport listener_;

  public void addListener(final String _property, final PropertyChangeListener _l) {
    if (listener_ == null) {
      listener_ = new PropertyChangeSupport(this);
    }
    listener_.addPropertyChangeListener(_property, _l);
  }

  public abstract boolean ajuste(CtuluRange _x, CtuluRange _y);

  public abstract boolean ajusteX(CtuluRange _x);

  public abstract boolean ajusteY(CtuluRange _y);

  public abstract void descendre();

  public abstract void dessine(Graphics2D _g, EGRepere _f);

  public abstract void enDernier();

  public abstract void enPremier();

  public abstract void fillWithCurves(final List _dest);

  abstract void fillWithInfo(BPaletteInfo.InfoData _table, CtuluListSelectionInterface _selectedPt);

  public abstract void fireCourbeAspectChanged(boolean _visibility);

  public abstract void fireCourbeContentChanged();

  protected void firePropertyChange(final String _prop) {
    if (listener_ != null) {
      listener_.firePropertyChange(_prop, false, true);
    }
  }

  protected void firePropertyChange(final String _prop, boolean _new) {
    if (listener_ != null) {
      listener_.firePropertyChange(_prop, !_new, _new);
    }
  }

  protected void firePropertyChange(final String _prop, final Object _old, final Object _new) {
    if (listener_ != null) {
      listener_.firePropertyChange(_prop, _old, _new);
    }
  }

  public abstract EGAxeHorizontal getAxeX();

  public abstract EGAxeVertical getAxeY();

  public int getMargeGaucheNeeded(final Graphics2D _g) {
    return getAxeY().getWidthNeeded(_g);
  }

  public int getMargeHautNeeded(final Graphics2D _g) {
    return getAxeY().isVisible() ? getAxeY().getHeightNeeded(_g) : 0;
  }

  @Override
  public BConfigurePalette getPalette() {
    return null;
  }

  public abstract String getTitle();

  @Override
  public BSelecteurTargetInterface getVisibleTitleTarget() {
    return new EGObjectConfigureVisibleTarget(this);
  }

  public CtuluRange getXRange() {
    final CtuluRange x = new CtuluRange();
    x.min_ = Double.MAX_VALUE;
    x.max_ = -Double.MAX_VALUE;
    ajusteX(x);
    if (Double.isNaN(x.min_)) {
      x.min_ = 0;
    }
    if (Double.isNaN(x.max_)) {
      x.max_ = 0;
    }
    if (x.min_ == x.max_) {
      if (x.min_ == 0) {
        x.max_ = 1;
      } else {
        x.min_ = x.min_ * 0.9;
        x.max_ = x.max_ * 1.1;
      }
    }
    return x;
  }

  public CtuluRange getYRange() {
    final CtuluRange y = new CtuluRange();
    y.min_ = Double.MAX_VALUE;
    y.max_ = -Double.MAX_VALUE;
    ajusteY(y);
    if (Double.isNaN(y.min_)) {
      y.min_ = 0;
    }
    if (Double.isNaN(y.max_)) {
      y.max_ = 0;
    }
    if (y.min_ == y.max_) {
      if (y.min_ == 0) {
        y.max_ = 1;
      } else {
        y.min_ = y.min_ * 0.9;
        y.max_ = y.max_ * 1.1;
      }
    }
    return y;
  }

  public abstract boolean isCourbe();

  public abstract boolean isSomethingToDisplay();

  /**
   * @return true si le nom peut etre modifie
   */
  @Override
  public abstract boolean isTitleModifiable();

  /**
   * @return Returns the isVisible.
   */
  public final boolean isVisible() {
    return isVisible_;
  }

  public  boolean isDisplayed() {
    return isVisible_;
  }

  public abstract void monter();

  @Override
  public boolean reloadPalette(Map infos) {
    return false;
  }

  public void removeListener(final String _prop, final PropertyChangeListener _l) {
    if (listener_ != null) {
      listener_.removePropertyChangeListener(_prop, _l);
    }
  }

  @Override
  public void setPalette(BConfigurePalette pal) {
  }

  public abstract boolean setTitle(String _newTitle);

  /**
   * @param _isVisible The isVisible to set.
   * @return true si modif
   */
  public boolean setVisible(final boolean _isVisible) {
    if (isVisible_ != _isVisible) {
      isVisible_ = _isVisible;
      fireCourbeAspectChanged(true);
      return true;
    }
    return false;
  }

  abstract boolean setYaxe(EGAxeVertical _v);
}
