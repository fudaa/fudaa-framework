/*
 * @creation 6 juil. 2004
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuResource;
import com.memoire.bu.BuSeparator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EGSpecificActions.java,v 1.16 2007-05-04 13:49:41 deniger Exp $
 */
public class EGSpecificActions implements ActionListener {

  CtuluPopupMenu menu_;

  final EGGraphe target_;

  // TreeSelectionModel selection_;

  CtuluCommandManager cmd_;

  public final CtuluCommandManager getCmd() {
    return cmd_;
  }

  public final void setCmd(final CtuluCommandManager _cmd) {
    cmd_ = _cmd;
  }

  public EGSpecificActions(final EGGraphe _target) {
    target_ = _target;
  }

  CtuluUI ui_;

  public EGSpecificActions(final EGGraphe _target, CtuluUI _ui) {
    this(_target);
    ui_ = _ui;
  }

  private static String getActLast() {
    return "DERNIER";
  }

  private static String getCopieSrc() {
    return "COPIESRC";
  }

  private static String getActDown() {
    return "DESCENDRE";
  }

  private static String getActUp() {
    return "MONTER";
  }

  private static String getActPrem() {
    return "PREMIER";
  }

  protected void buildPopupMenu(final MouseEvent _evt) {
    final EGGrapheModel m = target_.getModel();
    if (menu_ == null) {
      menu_ = new CtuluPopupMenu();
    } else {
      menu_.removeAll();
    }
    final int nbSelected = m.getNbSelectedObjects();
    if (nbSelected == 1) {
      final EGObject o = m.getSelectedObject();
      menu_.addCheckBox(EbliResource.EBLI.getString("Visible"), "VISIBLE", true, o.isVisible(), this);
      menu_.addMenuItem(EbliResource.EBLI.getString("Zoom"), "ZOOM_GROUP", EbliResource.EBLI.getIcon("loupe"), true,
          this);
      menu_.addMenuItem(EbliResource.EBLI.getString("Palette automatique"), "THEME_GROUP", EbliResource.EBLI.getIcon("theme"), true,
          this);
    } else if (nbSelected > 1) {
      menu_.addMenuItem(EbliResource.EBLI.getString("Visible"), "VISIBLE_OUI", BuResource.BU.getIcon("voir"), true,
          this);
      menu_.addMenuItem(EbliResource.EBLI.getString("Cach�"), "VISIBLE_NON", BuResource.BU.getIcon("cacher"), true,
          this);
    }
    if (m.isStructureModifiable()) {
      final boolean valideDuplicat = m.getNbSelectedObjects() >= 1 && (m.getNbSelectedObjects() == 1)
          && (m.getSelectedComponent() != null) && m.getSelectedComponent().getModel().isDuplicatable();
      menu_.add(new BuSeparator());
      menu_.addMenuItem(EbliResource.EBLI.getString("Dupliquer"), "DUPLICATE_SELECTED", BuResource.BU
          .getIcon("dupliquer"), valideDuplicat, this);
    }
    if (m.canDeleteCourbes()) {
      menu_.addMenuItem(EbliResource.EBLI.getString("Enlever les courbes s�lectionn�es"), "REMOVE_CURVES",
          BuResource.BU.getIcon("enlever"), m.getNbSelectedObjects() >= 1 && canRemoveCourbe(m), this);
    }
    if (nbSelected == 1 && m.getSelectedObject() instanceof EGCourbe) {
      final EGCourbe node = ((EGCourbe) m.getSelectedObject());
      final boolean canMonter = node.getEGParent().canMonter(node);
      final boolean canDescendre = node.getEGParent().canDescendre(node);

      menu_.addMenuItem(EbliResource.EBLI.getString("En premier"), getActPrem(),
          EbliResource.EBLI.getIcon("enpremier"), canMonter, this);
      menu_.addMenuItem(CtuluLib.getS("Monter"), getActUp(), CtuluResource.CTULU.getIcon("monter"), canMonter, this);
      menu_.addMenuItem(CtuluLib.getS("Descendre"), getActDown(), CtuluResource.CTULU.getIcon("descendre"),
          canDescendre, this);
      menu_.addMenuItem(EbliResource.EBLI.getString("En dernier"), getActLast(),
          EbliResource.EBLI.getIcon("endernier"), canDescendre, this);

      menu_.addMenuItem(EbliResource.EBLI.getString("Copier la courbe pour un autre jeu de donn�es"), getCopieSrc(),
          EbliResource.EBLI.getIcon("crystal_copier"), true, this);

      if (nbSelected == 1) {
        EGCourbe courbe = m.getSelectedComponent();
        if (!courbe.nuagePoints_) menu_.addMenuItem(EbliResource.EBLI.getString("Nuage de points"), "NUAGE_POINTS",
            EbliResource.EBLI.getIcon("curves"), true, this);
        else menu_.addMenuItem(EbliResource.EBLI.getString("Courbe classique"), "NUAGE_POINTS", BuResource.BU
            .getIcon("crystal_oscilloscope"), true, this);

        // -- action origine de la courbe. --//
        menu_.add(new EGActionAfficheOrigineCourbe.CourbeOnly(courbe.getModel(), ui_));

        menu_.addMenuItem(EbliResource.EBLI.getString("Inverser x et y"), "INVERSER", EbliResource.EBLI
            .getIcon("inverser"), true, this);

        // -- action replay data --//
        // menu_.add(new EGActionReplayDataCourbe.CourbeOnly(courbe.getModel(),ui_));

        if (target_.getModel().getSpecificsActionsCurvesOnly(target_, ui_) != null) for (EbliActionInterface action : target_
            .getModel().getSpecificsActionsCurvesOnly(target_, ui_))
          menu_.add(action);

      }

    }
    if (nbSelected > 1) {
      if (isAxeDifferent(m)) {
        menu_.addMenuItem(EbliResource.EBLI.getString("Fusionner les axes s�lectionn�s"), "AXE_FUSION", null, true, this);
      }
      menu_
          .addMenuItem(EbliResource.EBLI.getString("Utiliser des axes diff�rents"), "AXE_FUSION_NOT", null, true, this);

    }

    if (_evt != null) menu_.show(_evt.getComponent(), _evt.getX(), _evt.getY());
  }

  private boolean isAxeDifferent(final EGGrapheModel m) {
    EGObject[] selectedObject = m.getSelectedObjects();
    EGAxeVertical tmp = null;
    for (int i = 0; i < selectedObject.length; i++) {
      EGObject egObject = selectedObject[i];
      if (tmp == null) tmp = egObject.getAxeY();
      else if (tmp != egObject.getAxeY()) return true;
    }
    return false;
  }

  protected boolean canRemoveCourbe(final EGGrapheModel _m) {
    final EGObject[] p = _m.getSelectedObjects();
    if (p != null) {
      for (int i = p.length - 1; i >= 0; i--) {
        if (p[i] instanceof EGCourbe && ((EGCourbe) p[i]).getModel().isRemovable()) { return true; }
      }
    }
    return false;
  }

  private EGAxeVertical[] getSelectedAxes() {
    EGObject[] selectedObjects = target_.getModel().getSelectedObjects();
    if (selectedObjects == null) return null;
    Set<EGAxeVertical> res = new HashSet<>(selectedObjects.length);
    for (int i = 0; i < selectedObjects.length; i++) {
      res.add(selectedObjects[i].getAxeY());
    }
    return res.toArray(new EGAxeVertical[0]);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String com = _e.getActionCommand();
    final int n = target_.getModel().getNbSelectedObjects();
    if ("AXE_FUSION".equals(com)) {
      EGAxeVertical[] selectedAxes = getSelectedAxes();
      if (!CtuluLibArray.isEmpty(selectedAxes)) {
        this.target_.useOneAxeVertical(selectedAxes, selectedAxes[0].getTitre(), null);
        target_.axeUpdated();
      }

    } else if ("AXE_FUSION_NOT".equals(com)) {
      this.target_.useVerticalAxisInitialConf(target_.getModel().getSelectedObjects());
      target_.axeUpdated();

    } else if ("ZOOM_GROUP".equals(com)) {
      if (n == 1) {
        target_.setZoomAdaptedFor(target_.getModel().getSelectedObject());
      } 

    } else if ("THEME_GROUP".equals(com)) {
        if (n == 1 && target_.getModel().getSelectedObject() instanceof EGGroup) {
           EGGroup group = (EGGroup)target_.getModel().getSelectedObject();
           String idGroup = JOptionPane.showInputDialog(
                     BuResource.BU.getString("Veuillez choisir le groupe de la palette � appliquer:"),
                   ""+(group.getIdGroup()+1));
           try{
               int idGroupi = Integer.parseInt(idGroup);
               idGroupi = (idGroupi-1>=0)?idGroupi-1:0;
               group.traceAllCourbeFromPalette(idGroupi);
               target_.getModel().fireCourbeAspectChanged(null, false);
           }catch(NumberFormatException ex) {
               
           }
        }
    } else if ("REMOVE_CURVES".equals(com)) {
      target_.getModel().removeSelectedCurves(cmd_);

    } else if ("MODIFY_NAME".equals(com)) {
      changeName(target_, target_.getSelectedComponent(), cmd_);
    } else if ("DUPLICATE_SELECTED".equals(com)) {
      if (n == 1) {
        target_.getModel().duplicateCourbe(cmd_, target_.getSelectedComponent());
      }
    } else if ("NUAGE_POINTS".equals(com)) {
      // -- on permutte entre nuage de points et classique --//
      target_.getSelectedComponent().inverserNuagePoints();
    } else if ("INVERSER".equals(com)) {
      // -- on inverse le modele --//
      target_.getSelectedComponent().inverserModele();
    }

    else if ("COPIESRC".equals(com)) {
      target_.getModel().duplicateCourbeForSrc(cmd_, target_.getSelectedComponent());

    } else if ("VISIBLE".equals(com)) {
      if (n != 1) { return; }
      final EGObject o = target_.getModel().getSelectedObject();
      target_.setVisible(o, !o.isVisible_);
      target_.getModel().fireCourbeAspectChanged(o, false);
    } else if (com.startsWith("VISIBLE") && (n > 1)) {
      final EGObject[] ps = target_.getModel().getSelectedObjects();
      final boolean visible = com.endsWith("OUI");
      for (int i = ps.length - 1; i >= 0; i--) {
        ps[i].setVisible(visible);
      }
      // changement sur plusieurs
      target_.getModel().fireCourbeAspectChanged(null, false);

    } else if (n == 1) {
      final EGObject ps = target_.getModel().getSelectedObject();
      if (getActPrem().equals(com)) {
        ps.enPremier();
      } else if (getActDown().equals(com)) {
        ps.descendre();
      } else if (getActUp().equals(com)) {
        ps.monter();
      } else if (getActLast().equals(com)) {
        ps.enDernier();
      }
    }
  }

  static class ChangeNomPanel extends CtuluDialogPanel {

    final JTextField f_;

    /**
     * @param _old l'ancien nom
     */
    public ChangeNomPanel(final String _old) {
      f_ = addLabelStringText(EbliLib.getS("Nouveau nom"));
      f_.setText(_old);
    }

    /**
     * @return le nom entre par l'utilisateur
     */
    public String getNom() {
      return f_.getText();
    }

    @Override
    public boolean isDataValid() {
      if (f_.getText().trim().length() == 0) {
        setErrorText(EbliLib.getS("Le nom ne doit pas �tre vide"));
        return false;
      }
      return true;
    }
  }

  public static void changeNameAction(final EGGraphe _g, final EGObject _c, final String _newName,
      final CtuluCommandManager _cmd) {
    if (_c.setTitle(_newName)) {
      // final String old = _c.getModel().getTitle();
      _g.getModel().fireCourbeContentChanged(_c, false);
      /*
       * if (_cmd != null) { _cmd.addCmd(new CtuluCommand() { public void undo(){ _c.getModel().setTitle(old);
       * _g.getModel().fireCourbeAspectChanged(_c); } public void redo(){ _c.getModel().setTitle(_newName);
       * _g.getModel().fireCourbeAspectChanged(_c); } }); }
       */

    }
  }

  protected void changeNom(final EGObject _c, final String _newName) {
    changeNameAction(target_, _c, _newName, cmd_);
  }

  /**
   * Permet de modifier le nom d'une courbe.
   * 
   * @param _g le graphe cible
   * @param _c la courbe a modifier
   * @param _cmd le manager de commande
   */
  public static void changeName(final EGGraphe _g, final EGCourbe _c, final CtuluCommandManager _cmd) {
    if (_g == null || _c == null || !_c.getModel().isTitleModifiable()) { return; }
    final String old = _c.getModel().getTitle();
    final ChangeNomPanel p = new ChangeNomPanel(old);

    final int r = p.afficheModale(_g, EbliLib.getS("Modifer le nom de la courbe"));
    if (CtuluDialogPanel.isOkResponse(r)) {
      changeNameAction(_g, _c, p.getNom(), _cmd);
    }

  }

  public CtuluPopupMenu getMenu() {
    if (menu_ == null) buildPopupMenu(null);
    return menu_;
  }

}
