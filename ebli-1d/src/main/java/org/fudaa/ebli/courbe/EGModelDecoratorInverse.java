package org.fudaa.ebli.courbe;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;

import java.awt.*;
import java.util.Map;

/**
 * Decorator pour le EGModel, inverse les x et y de la courbe. utilise le design pattern decorator. permet de g�rer sans trop de d�gat l'inversion des
 * donn�es.
 *
 * @author Adrien Hadoux
 */
public class EGModelDecoratorInverse extends EGModelDecorator {
  public EGModelDecoratorInverse(final EGModel modeleInitial) {
    super(modeleInitial);
  }

  @Override
  public boolean addValue(double _x, double _y, CtuluCommandContainer _cmd) {

    return instanceModele_.addValue(_y, _x, _cmd);
  }

  @Override
  public boolean useSpecificIcon(int idx) {
    return instanceModele_.useSpecificIcon(idx);
  }

  @Override
  public boolean addValue(double[] _x, double[] _y, CtuluCommandContainer _cmd) {

    return instanceModele_.addValue(_y, _x, _cmd);
  }

  @Override
  public boolean deplace(int[] idx, double _deltax, double _deltay,
                         CtuluCommandContainer _cmd) {

    return instanceModele_.deplace(idx, _deltay, _deltax, _cmd);
  }

  @Override
  public EGModel duplicate() {

    return instanceModele_.duplicate();
  }

  @Override
  public void fillWithInfo(InfoData _table, CtuluListSelectionInterface pt) {

    instanceModele_.fillWithInfo(_table, pt);
  }

  @Override
  public int getNbValues() {

    return instanceModele_.getNbValues();
  }

  @Override
  public Object savePersistSpecificDatas() {

    return instanceModele_.savePersistSpecificDatas();
  }

  @Override
  public String getTitle() {

    return instanceModele_.getTitle();
  }

  @Override
  public double getX(int _idx) {

    return instanceModele_.getY(_idx);
  }

  @Override
  public double getXMax() {

    return instanceModele_.getYMax();
  }

  @Override
  public double getXMin() {

    return instanceModele_.getYMin();
  }

  @Override
  public double getY(int _idx) {

    return instanceModele_.getX(_idx);
  }

  @Override
  public double getYMax() {

    return instanceModele_.getXMax();
  }

  @Override
  public double getYMin() {

    return instanceModele_.getXMin();
  }

  @Override
  public boolean isDuplicatable() {

    return instanceModele_.isDuplicatable();
  }

  @Override
  public boolean isModifiable() {

    return instanceModele_.isModifiable();
  }

  @Override
  public boolean isPointDrawn(int _i) {

    return instanceModele_.isPointDrawn(_i);
  }

  @Override
  public boolean isRemovable() {

    return instanceModele_.isRemovable();
  }

  @Override
  public boolean isSegmentDrawn(int _i) {

    return instanceModele_.isSegmentDrawn(_i);
  }

  @Override
  public boolean isTitleModifiable() {

    return instanceModele_.isTitleModifiable();
  }

  @Override
  public boolean isXModifiable() {

    return instanceModele_.isXModifiable();
  }

  @Override
  public boolean removeValue(int _i, CtuluCommandContainer _cmd) {

    return instanceModele_.removeValue(_i, _cmd);
  }

  @Override
  public boolean removeValue(int[] _i, CtuluCommandContainer _cmd) {

    return instanceModele_.removeValue(_i, _cmd);
  }

  @Override
  public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    instanceModele_.replayData(model, infos, impl);
  }

  @Override
  public boolean isReplayable() {
    return instanceModele_.isReplayable();
  }

  @Override
  public void restoreFromSpecificDatas(Object data, Map infos) {
    instanceModele_.restoreFromSpecificDatas(data, infos);
  }

  @Override
  public boolean setTitle(String name) {

    return instanceModele_.setTitle(name);
  }

  @Override
  public boolean setValue(int _i, double _x, double _y,
                          CtuluCommandContainer _cmd) {

    return instanceModele_.setValue(_i, _y, _x, _cmd);
  }

  @Override
  public boolean setValues(int[] _idx, double[] _x, double[] _y,
                           CtuluCommandContainer _cmd) {

    return instanceModele_.setValues(_idx, _y, _x, _cmd);
  }

  @Override
  public void viewGenerationSource(Map infos, CtuluUI impl) {
    instanceModele_.viewGenerationSource(infos, impl);
  }

  @Override
  public boolean isGenerationSourceVisible() {
    return instanceModele_.isGenerationSourceVisible();
  }

  @Override
  public String getPointLabel(int _i) {
    return null;
  }

  @Override
  public Color getSpecificColor(int idx) {
    return instanceModele_.getSpecificColor(idx);
  }
}
