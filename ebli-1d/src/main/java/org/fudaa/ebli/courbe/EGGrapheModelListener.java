/*
 *  @creation     2 sept. 2004
 *  @modification $Date: 2006-12-05 10:14:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.util.EventListener;



/**
 * Un listener pour les graphes.
 * @author Fred Deniger
 * @version $Id: EGGrapheModelListener.java,v 1.5 2006-12-05 10:14:34 deniger Exp $
 */
public interface EGGrapheModelListener extends EventListener{

  /**
   * Envoye si l'organisation des courbes a �t� modifiee.
   */
  void structureChanged();

  /**
   * @param _c la courbe dont le contenu a ete modifie. null si toutes ou inconnue.
   * @param _mustRestore true si on doit restaurer la vue
   */
  void courbeContentChanged(EGObject _c, boolean _mustRestore);
  /**
   * @param _c la courbe dont l'aspect a ete modifie
   * @param _visibil true si la visibilit� de l'objet a �t� modifie
   * 
   */
  void courbeAspectChanged(EGObject _c, boolean _visibil);
  /**
   * @param _c l'axe dont les bornes ont ete modifiee
   */
  void axeContentChanged(EGAxe _c);
  /**
   * @param _c l'axe dont l'aspect a ete modifie
   */
  void axeAspectChanged(EGAxe _c);

}
