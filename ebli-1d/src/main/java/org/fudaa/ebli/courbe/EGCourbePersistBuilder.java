package org.fudaa.ebli.courbe;

import com.memoire.fu.FuEmptyArrays;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.dodico.fortran.DodicoDoubleArrayBinaryFileSaver;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class EGCourbePersistBuilder<T extends EGCourbe> extends EGPersistBuilder<T, EGCourbePersist> {
  public static final String GROUPE_KEY = "GROUPE";

  @Override
  protected final EGCourbePersist createPersistUnit(final T object) {
    return new EGCourbePersist();
  }

  protected EGGroup getGroup(final Map params) {
    return (EGGroup) params.get(GROUPE_KEY);
  }

  /**
   * Attention, il est important de garder cette signature pour la restauration des courbes des dossiers POST
   *
   * @param egObject
   * @param persist
   * @param params
   * @param log
   */
  @Override
  protected void postRestore(final EGCourbe egObject, final EGCourbePersist persist, final Map params, final CtuluAnalyze log) {
    initGraphicConfiguration(egObject, persist);
    egObject.setTitle(persist.getTitle());
    if (persist.inverse) {
      egObject.inverserModele();
    }
  }

  public boolean saveXY() {
    return true;
  }

  public EGCourbePersist persistGraphicsData(final T egCourbe) {
    final EGCourbePersist createPersistUnit = createPersistUnit(egCourbe);
    saveGraphicsData(createPersistUnit, egCourbe);
    return createPersistUnit;
  }

  public static EGModel forName(String className) {

    try {
      className = className.substring("class ".length());
      final Class myclass = Class.forName(className, true, Thread.currentThread().getContextClassLoader());
      final Object myModel = myclass.newInstance();

      if (myModel instanceof EGModel) {
        return (EGModel) myModel;
      } else {
        return null;
      }
    } catch (final Exception e) {
      return null;
    }
  }

  protected EGModel createModel(final EGCourbePersist persist, final Map _infos) {
    EGModel model = null;
    final String classeModel = persist.classeModel;
    if (classeModel != null) {
      model = forName(classeModel);
    }
    double[] savedX = null;
    double[] savedY = null;
    if (saveXY()) {
      savedX = (double[]) _infos.get(X_KEY);
      if (savedX == null) {
        savedX = persist.abscisses;
      }
      savedY = (double[]) _infos.get(Y_KEY);
      if (savedY == null) {
        savedY = persist.ordonnees;
      }
    }
    if (savedX == null) {
      savedX = FuEmptyArrays.DOUBLE0;
    }
    if (savedY == null) {
      savedY = FuEmptyArrays.DOUBLE0;
    }
    if (model == null) {
      model = new EGCourbeModelDefault(savedX, savedY);
    } else {
      // -- ajout des coordonnees --//
      model.addValue(savedX, savedY, null);
    }
    model.restoreFromSpecificDatas(persist.dataSpecifiques, _infos);
    return model;
  }

  public void initGraphicConfiguration(final EGCourbe courbeToModify, final EGCourbePersist persist) {
    // graphiques
    if (persist.tracebox != null) {
      courbeToModify.setTbox_(persist.tracebox);
    }
    if (persist.lineModel_ != null) {
      courbeToModify.setLigneType(persist.lineModel_);
    }
    if (persist.tLigneMarqueur_ != null) {
      courbeToModify.setLigneMark(persist.tLigneMarqueur_);
    }
    if (persist.iconeModel != null) {
      courbeToModify.setIconeModel(persist.iconeModel);
    }

    if (persist.iconeModelSpecific != null) {
      courbeToModify.setIconeModelSpecific(persist.iconeModelSpecific);
    }

    courbeToModify.setNuagePoints(persist.nuagePoints);

    if (persist.listeMarqueurs_ != null) {
      courbeToModify.setMarqueurs(persist.listeMarqueurs_);
    }
    courbeToModify.setDisplayPointLabels(persist.displayLabels);
    courbeToModify.setVerticalLabels(persist.verticalLabels);
    courbeToModify.setDisplayTitleOnCurve(persist.displayTitle);
    courbeToModify.setVisible(persist.isVisible());
  }

  public final static String X_KEY = "X";
  public final static String Y_KEY = "Y";

  @Override
  protected void postCreatePersist(final EGCourbePersist res, final T courbe, final EGGraphe graphe, final File binFile) {
    res.title_ = courbe.getTitle();
    res.nuagePoints = courbe.isNuagePoints();
    final EGModel initialModel = courbe.getInitialModel();
    if (saveXY()) {
      res.Xmin = initialModel.getXMin();
      res.Xmax = initialModel.getXMax();
      res.Ymin = initialModel.getYMin();
      res.Ymax = initialModel.getYMax();
      final Map<String, double[]> savedData = generateMapToPersistInBinFile(initialModel);
      final DodicoDoubleArrayBinaryFileSaver saver = new DodicoDoubleArrayBinaryFileSaver(binFile);
      saver.save(savedData);
    }
    // -- exemple pour evol: pdt, point,variable, fichier source --//
    res.dataSpecifiques = initialModel.savePersistSpecificDatas();
    res.classeModel = initialModel.getClass().toString();

    saveGraphicsData(res, courbe);
  }

  protected Map<String, double[]> generateMapToPersistInBinFile(final EGModel initialModel) {
    final double[] abscisses = new double[initialModel.getNbValues()];
    final double[] ordonnees = new double[initialModel.getNbValues()];
    for (int i = 0; i < initialModel.getNbValues(); i++) {
      abscisses[i] = initialModel.getX(i);
      ordonnees[i] = initialModel.getY(i);
    }
    final Map<String, double[]> savedData = new HashMap<>();
    savedData.put(X_KEY, abscisses);
    savedData.put(Y_KEY, ordonnees);
    return savedData;
  }

  protected void saveGraphicsData(final EGCourbePersist res, final T courbe) {
    // graphiques
    if (courbe.getTbox() != null) {
      res.tracebox = courbe.getTbox().duplicate();
    }
    if (courbe.getLigneModel() != null) {
      res.lineModel_ = new TraceLigneModel(courbe.getLigneModel());
    }
    if (courbe.getMarkLigneModel() != null) {
      res.tLigneMarqueur_ = new TraceLigneModel(courbe.getMarkLigneModel());
    }
    res.iconeModel = new TraceIconModel(courbe.getIconModel());
    if (courbe.isUseSpecificIcon() && courbe.getIconModelSpecific() != null) {
      res.iconeModelSpecific = new TraceIconModel(courbe.getIconModelSpecific());
    }
    final List<EGCourbeMarqueur> initMarqueurs = courbe.getMarqueurs();

    if (initMarqueurs != null) {
      res.listeMarqueurs_ = new ArrayList<>();
      for (final EGCourbeMarqueur eGCourbeMarqueur : initMarqueurs) {
        res.listeMarqueurs_.add(eGCourbeMarqueur.duplique());
      }
    }
    res.inverse = courbe.isInverse;
    res.displayLabels = courbe.displayPointLabels_;
    res.verticalLabels = courbe.isVerticalLabels();
    res.displayTitle = courbe.displayTitleOnCurve_;
    res.nuagePoints = courbe.nuagePoints_;
    res.setVisible(courbe.isVisible_);
  }
}
