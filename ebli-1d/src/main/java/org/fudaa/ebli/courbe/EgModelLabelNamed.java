/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

/**
 *
 * @author Frederic Deniger
 */
public interface EgModelLabelNamed {

  String getLabelColumnName();
}
