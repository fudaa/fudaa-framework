package org.fudaa.ebli.courbe;

import java.io.File;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluAnalyze;

public abstract class EGGroupPersistBuilder<T extends EGGroup> extends EGPersistBuilder<T, EGGroupPersist> {

  @Override
  protected final EGGroupPersist createPersistUnit(EGGroup object) {
    return new EGGroupPersist();
  }

  @Override
  protected void postCreatePersist(EGGroupPersist res, T groupe, EGGraphe graphe, File binFile) {
    res.builderClass = getClass().getName();
    res.title = groupe.getTitle();
    res.groupId = groupe.getIdGroup();
    EGAxeVertical axeVertical = graphe.getInitVerticalAxis(groupe);
    // axe non fusionne
    if (axeVertical == null) {
      axeVertical = groupe.getAxeY();
    }
    res.axeY = new EGAxeVerticalPersist(axeVertical);
    res.isVisible = groupe.isVisible_;

  }

  @Override
  protected void postRestore(T group, EGGroupPersist persist, Map params, CtuluAnalyze log) {
    EGAxeVertical axe = persist.axeY.generateAxe();
    group.setAxeY(axe);
    group.setTitle(persist.title);
    if (StringUtils.isBlank(axe.getTitre())) {
      axe.setTitre(group.getTitle());
    }
    group.setIdGroup(persist.groupId);
    group.isVisible_ = persist.isVisible;
  }
}
