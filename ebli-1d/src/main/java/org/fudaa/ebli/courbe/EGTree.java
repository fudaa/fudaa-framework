/*
 * @creation 6 juil. 2004
 * @modification $Date: 2007-06-28 09:26:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.*;
import java.io.IOException;
import java.util.EventObject;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.gui.CtuluPopupListener;

/**
 * @author Fred Deniger
 * @version $Id: EGTree.java,v 1.17 2007-06-28 09:26:47 deniger Exp $
 */
public class EGTree extends JTree implements Transferable, CtuluPopupListener.PopupReceiver {

  private class CustomTransfetHandler extends TransferHandler {

    /**
     * Fait rien.
     */
    public CustomTransfetHandler() {

    }

    @Override
    protected Transferable createTransferable(final JComponent _c) {
      return EGTree.this;
    }

    @Override
    protected void exportDone(final JComponent _source, final Transferable _data, final int _action) {
      super.exportDone(_source, _data, _action);
    }

    @Override
    public boolean canImport(final JComponent _comp, final DataFlavor[] _transferFlavors) {
      return true;
    }

    @Override
    public void exportAsDrag(final JComponent _comp, final InputEvent _e, final int _action) {
      super.exportAsDrag(_comp, _e, _action);
    }

    @Override
    public void exportToClipboard(final JComponent _comp, final Clipboard _clip, final int _action) {
      super.exportToClipboard(_comp, _clip, _action);
    }

    @Override
    public int getSourceActions(final JComponent _c) {
      return COPY_OR_MOVE;
    }

  }

  class ArbreCellEditor extends AbstractCellEditor implements TreeCellEditor {

    // la hauteur par defaut pour la case a cocher 'visible'
    final int cbHeight_;
    // la largeur par defaut pour la case a cocher 'visible'
    final int cbWidth_;
    final EGTreeCellRenderer editor_;
    protected EGObject cqEdite_;

    ArbreCellEditor() {
      editor_ = new EGTreeCellRenderer();
      editor_.cbVisible_.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent _e) {
          if (cqEdite_ != null) {
            cqEdite_.setVisible(editor_.cbVisible_.isSelected());
            ((EGGrapheTreeModel) getModel()).fireCourbeAspectChanged(cqEdite_, true);
            fireEditingStopped();
          }
        }
      });
      editor_.r1_.addKeyListener(new KeyListener() {

        @Override
        public void keyPressed(final KeyEvent _e) {}

        @Override
        public void keyReleased(final KeyEvent _e) {
          if (actions_ != null && _e.getKeyCode() == KeyEvent.VK_ENTER && editor_.r1_.getText() != null
              && editor_.r1_.getText().trim().length() > 0) {
            actions_.changeNom(cqEdite_, editor_.r1_.getText());
            fireEditingStopped();
          }
        }

        @Override
        public void keyTyped(final KeyEvent _e) {

        }
      });
      editor_.r1_.addFocusListener(new FocusListener() {

        @Override
        public void focusGained(final FocusEvent _e) {
          editor_.r1_.setBackground(UIManager.getColor("TextField.background"));
          editor_.r1_.setForeground(Color.BLACK);
        }

        @Override
        public void focusLost(final FocusEvent _e) {}
      });

      // on recupere les dimensions par defaut
      final Dimension d = editor_.cbVisible_.getPreferredSize();
      cbHeight_ = d.height;
      cbWidth_ = d.width;

    }

    @Override
    protected void fireEditingStopped() {
      super.fireEditingStopped();
    }

    @Override
    public Object getCellEditorValue() {
      return null;
    }

    @Override
    public Component getTreeCellEditorComponent(final JTree _tree, final Object _value, final boolean _isSelected,
        final boolean _expanded, final boolean _leaf, final int _row) {
      cqEdite_ = (EGObject) _value;
      editor_.getTreeCellRendererComponent(_tree, _value, _isSelected, _expanded, _leaf, _row, true);

      editor_.r1_.setEditable(cqEdite_.isTitleModifiable());
      editor_.cbVisible_.requestFocus();
      return editor_;
    }

    @Override
    public boolean isCellEditable(final EventObject _anEvent) {
      if (_anEvent instanceof MouseEvent) {
        final MouseEvent e = (MouseEvent) _anEvent;
        if (e.isConsumed()) {
          return false;
        }
        if (e.getButton() != MouseEvent.BUTTON1) {
          return false;
        }
        final int mouseX = e.getX();
        final TreePath p = getClosestPathForLocation(mouseX, e.getY());
        if (p != null && (p.getLastPathComponent() instanceof EGObject)) {
          final EGObject cq = (EGObject) p.getLastPathComponent();
          // on recupere le rectangle englobant de la cellule
          final Rectangle r = getRowBounds(getRowForPath(p));
          // calcul de l'enveloppe de la case a cocher
          // la case a cocher est a droite ....
          final int maxX = r.x + r.width;
          final int minX = maxX - cbWidth_;
          final int minY = r.y;
          final int maxY = minY + cbHeight_ + 1;
          if (mouseX >= minX && mouseX <= maxX && e.getY() >= minY && e.getY() <= maxY) {
            if (!isSelectionEmpty() && cq == getSelectionPath().getLastPathComponent()) {
              cq.setVisible(!cq.isVisible_);
              e.consume();
              ((EGGrapheTreeModel) getModel()).fireCourbeAspectChanged(cq, false);
              fireEditingStopped();
              return false;
            }
            return true;
          }
          // pour modifier le nom
          else if (cq.isTitleModifiable() && (mouseX < minX) && (mouseX > r.x + cq.getIconWidth())
              && e.getClickCount() == 2) {
            return true;
          }

        }
      }
      // le calque est mis a null s'il n'est pas edite

      return false;
    }

    @Override
    public boolean shouldSelectCell(final EventObject _anEvent) {
      return false;
    }

  }

  class CustomDropListener implements DropTargetListener {

    @Override
    public void dragEnter(final DropTargetDragEvent _dtde) {}

    @Override
    public void dragExit(final DropTargetEvent _dte) {}

    @Override
    public void dragOver(final DropTargetDragEvent _dtde) {}

    @Override
    public void drop(final DropTargetDropEvent _dtde) {
      boolean isOk = false;
      final DataFlavor[] fs = _dtde.getCurrentDataFlavors();
      for (int i = fs.length - 1; i >= 0; i--) {
        if (fs[i].getRepresentationClass() == EGTree.class) {
          isOk = true;
          break;
        }
      }
      if (isOk) {
        final Point p = _dtde.getLocation();
        final TreePath treePath = getClosestPathForLocation(p.x, p.y);

        if (treePath == null) {
          return;
        }
        final Object path = treePath.getLastPathComponent();
        if (path instanceof EGGroup) {
          final EGGrapheTreeModel m = (EGGrapheTreeModel) getModel();
          if (_dtde.getDropAction() == DnDConstants.ACTION_COPY) {
            m.dragCopyAction(EGTree.this, (EGGroup) path, mng_);
          }
          if (_dtde.getDropAction() == DnDConstants.ACTION_MOVE) {
            m.dragMoveAction(EGTree.this, (EGGroup) path, mng_);
          }
          _dtde.dropComplete(true);
        } else {
          _dtde.rejectDrop();
        }
      }
    }

    @Override
    public void dropActionChanged(final DropTargetDragEvent _dtde) {}
  }

  EGSpecificActions actions_;
  boolean isDragAlreadyInit_;

  DropTargetListener li_;

  CtuluCommandManager mng_;

  /**
   * Initialisation par defaut: pas de focus.
   */
  public EGTree() {
    setRootVisible(false);
    setShowsRootHandles(true);
    setExpandsSelectedPaths(true);
    setRequestFocusEnabled(false);
    setEditable(true);
    final EGTreeCellRenderer treeCellRenderer = new EGTreeCellRenderer();
    setCellRenderer(treeCellRenderer);
    setCellEditor(new ArbreCellEditor());
    setRowHeight(35);
    new CtuluPopupListener(this, this);
  }

  private DropTargetListener getDropListener() {
    if (li_ == null) {
      li_ = new CustomDropListener();
    }
    return li_;
  }

  private void initDragData(final TreeModel _m) {
    if (_m instanceof EGGrapheTreeModel) {
      final EGGrapheTreeModel m = (EGGrapheTreeModel) _m;
      setDragEnabled(m.isStructureModifiable());
      if (!isDragAlreadyInit_ && m.isStructureModifiable()) {
        setTransferHandler(new CustomTransfetHandler());
        setDropTarget(new DropTarget(this, getDropListener()));
        isDragAlreadyInit_ = true;
      }
    } else {
      setDragEnabled(false);
    }
  }

  public final EGSpecificActions getActions() {
    return actions_;
  }

  public final CtuluCommandManager getMng() {
    return mng_;
  }

  @Override
  public Object getTransferData(final DataFlavor _flavor) throws UnsupportedFlavorException, IOException {
    return this;
  }

  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return new DataFlavor[] { new DataFlavor(EGTree.class, "THIS") };
  }

  @Override
  public boolean isDataFlavorSupported(final DataFlavor _flavor) {
    return true;
  }

  @Override
  public void popup(MouseEvent _e) {
    if (actions_ != null) {
      actions_.buildPopupMenu(_e);
      _e.consume();
    }
  }

  public final void setActions(final EGSpecificActions _actions) {
    if (actions_ != _actions) {
      if (actions_ != null) {
        actions_.setCmd(null);
      }
      actions_ = _actions;
      if (actions_ != null) {
        actions_.setCmd(mng_);
      }
    }
  }

  public final void setMng(final CtuluCommandManager _mng) {
    mng_ = _mng;
  }

  @Override
  public void setModel(final TreeModel _newModel) {
    super.setModel(_newModel);
    initDragData(_newModel);
  }

  @Override
  public void updateUI() {
    setRowHeight(35);
    super.updateUI();
  }

}
