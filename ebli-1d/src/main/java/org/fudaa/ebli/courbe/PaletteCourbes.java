package org.fudaa.ebli.courbe;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTabbedPane;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;
import com.thoughtworks.xstream.io.xml.XppDriver;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Palette de couleurs et trait pour les courbes. Peut �tre configur�e est persist� en xml dans les user properties.
 *
 * @author Adrien Hadoux
 *
 */
public class PaletteCourbes {

    private static PaletteCourbes INSTANCE = null;
    private final String fileNameExtension;
    public  static int nbDifferentGroups =0;
    
    
    public synchronized static PaletteCourbes getInstance(String filename) {
        if (INSTANCE == null) {
            INSTANCE = new PaletteCourbes(filename);
        }
        return INSTANCE;
    }

    public synchronized static PaletteCourbes getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PaletteCourbes("");
        }
        return INSTANCE;
    }

    public synchronized static PaletteCourbes getInitSizedInstance(int maxGroup, int maxCurveByGroup, String filename) {
        INSTANCE = new PaletteCourbes(filename, maxGroup, maxCurveByGroup);
        return INSTANCE;

    }
    
    public synchronized static PaletteCourbes getLightInstance(String filename) {
        return getInitSizedInstance(5,10, filename);
    }

    public List<List<TraceLigneModel>> traceurs;
    public int maxGroup = 50;
    public int maxCurveByGroup = 100;

    public PaletteCourbes(final String filename, int maxGroup, int maxCurveByGroup) {
        this.maxGroup = maxGroup;
        this.maxCurveByGroup = maxCurveByGroup;
        fileNameExtension = filename;
        initTraceurs();
        // reinit number of different groups.
        nbDifferentGroups=0;
    }

    public PaletteCourbes(final String filename) {
        this(filename, 50, 100);

    }

    public static  int  updateTotalGroups(int value) {
    if(nbDifferentGroups + value>=0)
        nbDifferentGroups= nbDifferentGroups + value;
    return nbDifferentGroups;
}
    
    public void initTraceurs() {
        // -- try to load by getting the traceurs --//
        getTraceurs();
        File f = new File(fileName());
        if (!f.exists()) {
            savePalette();
        }
    }

    public TraceLigneModel getTraceLigneModelForGroupAndCurve(int group,
            int curve) {
        if (group >= maxGroup || curve >= maxCurveByGroup) {
            completeTraceurs(group >= maxGroup?(group+1):group, curve >= maxCurveByGroup?(curve+1):curve);
        }

        return new TraceLigneModel(getTraceurs().get(group).get(curve));
    }

    /**
     * complete size of generated traceurs to
     */
    private void completeTraceurs(int groups, int curves) {

        int nbGroupToGenerate = groups - traceurs.size();
        int nbCurvesToGenerate = curves - traceurs.get(0).size();
        if (nbGroupToGenerate > 0) {
            int sizeForACurve = traceurs.get(0).size();
            List<List<TraceLigneModel>> liste = generateTraceurs(
                    nbGroupToGenerate, sizeForACurve);
            traceurs.addAll(liste);
            maxGroup = groups;

        }
        if (nbCurvesToGenerate > 0) {
            int sizeForACurve = traceurs.get(0).size();
            int numberOfGroupToGenerate = traceurs.size();
            List<List<TraceLigneModel>> liste = generateTraceurs(
                    numberOfGroupToGenerate, nbCurvesToGenerate);
            //-- pour chaque groupe ajouter le nombre de curve model g�n�r�s --//
            for (int i = 0; i < traceurs.size(); i++) {
                traceurs.get(i).addAll(liste.get(i));
            }

            maxCurveByGroup = curves;
            savePalette();
        }
        savePalette();
    }

    private boolean isAlreadyGenerated(final List<Float> previousColor, final float randomColor) {
        for (Float p : previousColor) {
            if (Math.abs(randomColor - p) < 0.4f) {
                return true;
            }
        }

        return false;
    }

    private List<List<TraceLigneModel>> generateTraceurs(int group, int curves) {
        List<List<TraceLigneModel>> liste = new ArrayList<>();

        for (int i = 0; i < group; i++) {
            List<TraceLigneModel> listeGroupe = new ArrayList<>();
            List<Float> previousColor = new ArrayList<>();
            for (int c = 0; c < curves; c++) {
                int _typeTrait = 1;
                float _epaisseur = 1.0f;
                Color _c = Color.black;

                // -- generate random values --//
                Random r = new Random();
                float randomColor = r.nextFloat();
                int nbAttempts = 50;
                while (isAlreadyGenerated(previousColor, randomColor) && nbAttempts-- > 0) {
                    randomColor = r.nextFloat();
                }
                previousColor.add(randomColor);
                _c = Color.getHSBColor(randomColor,// random hue, color
                        1.0f,// full saturation, 1.0 for 'colorful' colors, 0.0
                        // for grey
                        1.0f // 1.0 for bright, 0.0 for black
                );
                // -- type trait 0 to 5 --//
                _typeTrait = r.nextInt(6);
                if (_typeTrait == 0 || _typeTrait == 2) {
                    _typeTrait = 1;
                }
                TraceLigneModel generated = new TraceLigneModel(_typeTrait,
                        _epaisseur, _c);
                listeGroupe.add(generated);

            }
            liste.add(listeGroupe);
        }

        return liste;
    }

    private void generateTraceurs() {
        setTraceurs(generateTraceurs(maxGroup, maxCurveByGroup));
        savePalette();
    }

    private void updateLineModels(final List<BSelecteurLineModel> listeSelectors, int selectedGroup) {
        List<TraceLigneModel> listeLigneModel = traceurs.get(selectedGroup);
        List<TraceLigneModel> listeLigneModelNew = new ArrayList<>();
        for (int i = 0; i < listeLigneModel.size(); i++) {
            if (i < listeSelectors.size()) {
                listeLigneModelNew.add(listeSelectors.get(i).getNewData());
            }
        }
        traceurs.set(selectedGroup, listeLigneModelNew);

    }
    int currentTab = 0;

     public void displayUiManager(final BuCommonImplementation impl) {
         displayUiManager(impl, null);
     }
     
         
    
    public void displayUiManager(final BuCommonImplementation impl, final ChangeListener lst) {
        initTraceurs();
        BuPanel mainPanel = new BuPanel(new BorderLayout());
        final BuTabbedPane panelTab = new BuTabbedPane();
        mainPanel.add(panelTab, BorderLayout.CENTER);
        BuPanel view = new BuPanel(new GridLayout(maxCurveByGroup, 2));
        final JScrollPane panescroll = new JScrollPane(view);
        final List<JLabel> labels = new ArrayList<>();
        final List<BSelecteurLineModel> listeSelectors = new ArrayList<>();
        currentTab = 0;
        for (int i = 0; i < traceurs.size(); i++) {
            BuPanel tab = new BuPanel(new BorderLayout());
            panelTab.addTab(EbliResource.EBLI.getString("Edition Groupe") + " " + (i + 1), tab);
            List<TraceLigneModel> listeLigneModel = traceurs.get(i);

            if (i == 0) {
                for (int j = 0; j < listeLigneModel.size(); j++) {
                    BSelecteurLineModel selecteur = new BSelecteurLineModel(
                            listeLigneModel.get(j));
                    BuLabel label = new BuLabel(EbliResource.EBLI.getString("Courbe " + (j + 1)
                            + " " + EbliResource.EBLI.getString("du Groupe") + (i + 1) + ":"));
                    labels.add(label);
                    view.add(label);
                    listeSelectors.add(selecteur);
                    view.add(selecteur.buildPanel());
                }
                tab.add(panescroll);
            }

        }

        panelTab.addChangeListener(new ChangeListener() { // add the Listener

            @Override
            public void stateChanged(ChangeEvent e) {
                updateLineModels(listeSelectors, currentTab);
                int groupeToLoad = panelTab.getSelectedIndex();
                List<TraceLigneModel> listeLigneModel = traceurs
                        .get(groupeToLoad);

                for (int j = 0; j < listeLigneModel.size(); j++) {
                    //listeSelectors.get(j)
                    //-- update selector with data --//
                    listeSelectors.get(j).updateFromModel(listeLigneModel.get(j));
                    labels.get(j).setText(EbliResource.EBLI.getString("Courbe" + " " + (j + 1)
                            + " " + EbliResource.EBLI.getString("du Groupe") + " " + (groupeToLoad + 1) + ":"));
                }
                ((JComponent) panelTab.getComponentAt(groupeToLoad))
                        .add(panescroll);
                currentTab = groupeToLoad;
            }
        });
      final JDialog  frame = new JDialog ();
      frame.setTitle(EbliResource.EBLI.getString("G�n�rateur des courbes de graphe"));
      frame.setModal(true);
      final BuInternalFrame internalFrame = new BuInternalFrame(
                EbliResource.EBLI.getString("G�n�rateur des courbes de graphe"), true, true, true, true);

        BuPanel buttonPanel = new BuPanel(new FlowLayout(FlowLayout.CENTER));
        BuButton ok = new BuButton(EbliResource.EBLI.getString("ok"),
                BuResource.BU.getIcon("valider_22"));
        buttonPanel.add(ok);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateLineModels(listeSelectors, currentTab);
                savePalette();
                if(impl!=null)
                    impl.removeInternalFrame(internalFrame);
                else 
                    frame.dispose();
                
                if(lst != null) {
                    lst.stateChanged(new ChangeEvent(e));
                }
            }
        });
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);
     
        Container comp = frame;
        if(impl!= null) {
             comp = internalFrame;
             comp.add(mainPanel);
        } else 
            frame.setContentPane(mainPanel);
        comp.setPreferredSize(new Dimension(400, 400));
        comp.setMinimumSize(new Dimension(400, 400));
        comp.setSize(new Dimension(400, 400));
       
   if(impl != null)
        impl.addInternalFrame(internalFrame);
        else {
            frame.setVisible(true);
        }
    }

    public String fileName() {
        String filename = System.getProperty("user.home") + File.separator
                + "palette_" + fileNameExtension;
        return filename;
    }

    public void loadPalette() {
        boolean isPaletteLoaded = false;
        ObjectInputStream in = null;

        final XmlFriendlyReplacer replacer = new XmlFriendlyReplacer("#", "_");
        //final XppDriver staxDriver = new XppCustomDriver(replacer);
        final XppDriver staxDriver = new XppDriver();
        final XStream xstream = new XStream(staxDriver);
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypesByWildcard(new String[] {
                "org.fudaa.**"
        });
        File file = new File(fileName());
        try {
            in = EbliLib.createObjectInpuStream(file, xstream);

            traceurs = (List<List<TraceLigneModel>>) in.readObject();
            if (traceurs != null && traceurs.size() > 0) {
                maxGroup = traceurs.size();
                maxCurveByGroup = traceurs.get(0).size();
                isPaletteLoaded = true;
            }
        } catch (Exception e) {
            System.out.println(EbliResource.EBLI.getString("Impossible de charger la palette, Fichier introuvable: ")
                    + file.getAbsolutePath());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (!isPaletteLoaded) {
            generateTraceurs();
        }
    }

    public void savePalette() {
        ObjectOutputStream out = null;
        final XmlFriendlyReplacer replacer = new XmlFriendlyReplacer("#", "_");
        //final XppDriver staxDriver = new XppCustomDriver(replacer);
        final XppDriver staxDriver = new XppDriver();
        final XStream xstream = new XStream(staxDriver);
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypesByWildcard(new String[] {
                "org.fudaa.**"
        });
        File file = new File(fileName());
        try {
            out = EbliLib.createObjectOutpuStream(file, xstream);

            out.writeObject(traceurs);

        } catch (Exception e) {

        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public List<List<TraceLigneModel>> getTraceurs() {
        if (traceurs == null) {
            loadPalette();
        }
        return traceurs;
    }

    public void setTraceurs(List<List<TraceLigneModel>> traceurs) {
        this.traceurs = traceurs;
    }

}
