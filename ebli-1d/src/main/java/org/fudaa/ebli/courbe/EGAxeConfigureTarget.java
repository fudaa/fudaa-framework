/*
 * @creation 28 nov. 06
 * 
 * @modification $Date: 2007-05-22 14:19:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JToggleButton;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.*;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author fred deniger
 * @version $Id: EGAxeConfigureTarget.java,v 1.3 2007-05-22 14:19:05 deniger Exp $
 */
public abstract class EGAxeConfigureTarget implements BSelecteurTargetInterface,
        BConfigurableInterface {

  protected static class AffichageConfigurator extends EGAxeConfigureTarget {

    private final class ItemLineUpdater implements ItemListener {

      private final JToggleButton cb;
      private final BSelecteurLineModel line;

      /**
       * @param cb
       * @param line
       */
      public ItemLineUpdater(JToggleButton cb, BSelecteurLineModel line) {
        super();
        this.cb = cb;
        this.line = line;
      }

      @Override
      public void itemStateChanged(ItemEvent e) {
        line.setEnabled(cb.isSelected());
      }
    }

    public AffichageConfigurator(final EGAxe _target, final EGParent _dispatcher) {
      super(_target, _dispatcher, EbliLib.getS("Affichage"));
    }

    @Override
    public BSelecteurInterface[] createSelecteurs() {
      final List res = new ArrayList();
      if (target_ == null) {
        return new BSelecteurInterface[0];
      }
      BSelecteurCheckBox cb;
      cb = new BSelecteurCheckBox(BSelecteurCheckBox.PROP_VISIBLE, EbliLib.getS("Axe Visible"));
      res.add(cb);
      final BSelecteurColorChooserBt selecteurColorChooser = new BSelecteurColorChooserBt();
      selecteurColorChooser.setTitle(EbliLib.getS("Couleur de l'axe"));
      res.add(selecteurColorChooser);
      // pour changer la police des axes.
      res.add(new BSelecteurFont());

      if (target_.isVertical()) {
        cb = new BSelecteurCheckBox(EGAxeVertical.PROP_DROITE);
        cb.setTitle(EbliLib.getS("Axe � droite"));
        res.add(cb);
      }
      cb = new BSelecteurCheckBox(EGAxe.PROP_GRADUATIONS);
      cb.setTitle(EbliLib.getS("Graduations"));
      res.add(cb);
      cb = new BSelecteurCheckBox(EGAxe.PROP_EXTREMITE_VISIBLE);
      cb.setTitle(EbliLib.getS("Extremit� de l'axe"));
      res.add(cb);
      cb = new BSelecteurCheckBox(EGAxe.PROP_IS_GRID_GRADUATIONS_PAINTED);
      cb.setTitle(EbliLib.getS("Afficher la grille"));
      res.add(cb);
      final BSelecteurLineModel line = new BSelecteurLineModel(EGAxe.PROP_TRACE_GRADUATIONS);
      line.setTitle(EbliLib.getS("Ligne de la grille"));
      line.setEnabled(cb.getCb().isSelected());
      cb.getCb().addItemListener(new ItemLineUpdater(cb.getCb(), line));
      res.add(line);
      cb = new BSelecteurCheckBox(EGAxe.PROP_IS_GRID_SOUS_GRADUATIONS_PAINTED);
      cb.setTitle(EbliLib.getS("Afficher la sous grille"));
      res.add(cb);
      final BSelecteurLineModel lineMinorGrid = new BSelecteurLineModel(EGAxe.PROP_TRACE_SOUS_GRADUATIONS);
      lineMinorGrid.setTitle(EbliLib.getS("Ligne de la sous grille"));
      res.add(lineMinorGrid);
      lineMinorGrid.setEnabled(cb.getCb().isSelected());
      cb.getCb().addItemListener(new ItemLineUpdater(cb.getCb(), lineMinorGrid));
      return (BSelecteurInterface[]) res.toArray(new BSelecteurInterface[0]);
    }

    @Override
    public Object getProperty(final String _key) {
      if (_key == BSelecteurColorChooser.DEFAULT_PROPERTY) {
        return target_.getLineColor();
      }
      if (_key == BSelecteurReduitFonteNewVersion.PROPERTY) {
        return target_.getFont();
      }
      if (_key == EGAxeVertical.PROP_DROITE) {
        return Boolean.valueOf(((EGAxeVertical) target_).isDroite());
      }
      if (_key == EGAxe.PROP_GRADUATIONS) {
        return Boolean.valueOf(target_.isGraduations());
      }
      if (_key == EGAxe.PROP_EXTREMITE_VISIBLE) {
        return Boolean.valueOf(target_.isExtremiteDessinee());
      }
      if (_key == BSelecteurCheckBox.PROP_VISIBLE) {
        return Boolean.valueOf(target_.isVisible());
      }
      if (_key == EGAxe.PROP_IS_GRID_GRADUATIONS_PAINTED) {
        return Boolean.valueOf(target_.isTraceGrille());
      }
      if (_key == EGAxe.PROP_IS_GRID_SOUS_GRADUATIONS_PAINTED) {
        return Boolean.valueOf(target_.isTraceSousGrille());
      }
      if (_key == EGAxe.PROP_TRACE_GRADUATIONS) {
        return target_.getTraceGraduations();
      }
      if (_key == EGAxe.PROP_TRACE_SOUS_GRADUATIONS) {
        return target_.getTraceSousGraduations();
      }

      return null;
    }

    @Override
    public boolean doSetProperty(final String _key, final Object _newProp) {
      if (_key == BSelecteurReduitFonteNewVersion.PROPERTY) {
        return target_.setFont((Font) _newProp);
      }
      if (_key == BSelecteurColorChooser.DEFAULT_PROPERTY) {
        return target_.setLineColor((Color) _newProp);
      }
      if (_key == EGAxe.PROP_TRACE_GRADUATIONS) {
        return target_.setTraceGraduations((TraceLigneModel) _newProp);
      }
      if (_key == EGAxe.PROP_TRACE_SOUS_GRADUATIONS) {
        return target_
                .setTraceSousGraduations((TraceLigneModel) _newProp);
      }
      final boolean booleanValue = ((Boolean) _newProp).booleanValue();
      if (_key == EGAxe.PROP_IS_GRID_GRADUATIONS_PAINTED) {
        return target_.setTraceGrille(booleanValue);
      }
      if (_key == EGAxe.PROP_IS_GRID_SOUS_GRADUATIONS_PAINTED) {
        return target_.setTraceSousGrille(booleanValue);
      }
      if (_key == EGAxe.PROP_IS_GRID_SOUS_GRADUATIONS_PAINTED) {
        return target_.setGraduations(booleanValue);
      }
      if (_key == BSelecteurCheckBox.PROP_VISIBLE) {
        return target_.setVisible(booleanValue);
      }

      if (_key == EGAxeVertical.PROP_DROITE) {
        return ((EGAxeVertical) target_).setDroite(booleanValue);
      }
      if (_key == EGAxe.PROP_GRADUATIONS) {
        return target_.setGraduations(booleanValue);
      }
      if (_key == EGAxe.PROP_EXTREMITE_VISIBLE) {
        return target_.setExtremiteDessinee(booleanValue);
      }

      return false;
    }
  }

  protected static class TitleConfigure extends EGAxeConfigureTarget {

    public TitleConfigure(final EGAxe _target, final EGParent _dispatcher) {
      super(_target, _dispatcher, EbliLib.getS("Titre"));
    }

    @Override
    public BSelecteurInterface[] createSelecteurs() {
      if (target_ == null) {
        return new BSelecteurInterface[0];
      }
      final List res = new ArrayList();
      BSelecteurCheckBox cb = new BSelecteurCheckBox(EGAxe.PROP_TITLE_VISIBLE);
      cb.setTitle(EbliLib.getS("Titre visible"));
      res.add(cb);
      cb = new BSelecteurCheckBox(EGAxe.PROP_UNIT_VISIBLE);
      cb.setTitle(EbliLib.getS("Unit� visible"));
      res.add(cb);
      BSelecteurCheckBox cbVertical = null;
      BSelecteurCheckBox cbVerticalDroite = null;
      if (target_.isVertical()) {
        cbVertical = new BSelecteurCheckBox(EGAxeVertical.PROP_TITLE_VERTICAL);
        cbVertical.setTitle(EbliLib.getS("Titre vertical"));
        res.add(cbVertical);
        cbVerticalDroite = new BSelecteurCheckBox(EGAxeVertical.PROP_TITLE_VERTICAL_DROITE);
        cbVerticalDroite.setTitle(EbliLib.getS("Titre � droite de l'axe"));
        res.add(cbVerticalDroite);
      }
      cb = new BSelecteurCheckBox(EGAxe.PROP_TITLE_CENTERED);
      cb.setTitle(EbliLib.getS("Titre centr�"));
      res.add(cb);
      if (cbVertical != null && cbVerticalDroite != null) {
        final JToggleButton cbVert = cbVertical.getCb();
        final JToggleButton cbCentered = cb.getCb();
        final JToggleButton cbDroite = cbVerticalDroite.getCb();
        cbVertical.getCb().addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(final ItemEvent _e) {
            cbCentered.setEnabled(cbVert.isSelected());
            cbDroite.setEnabled(cbVert.isSelected());

          }
        });
      }
      return (BSelecteurInterface[]) res.toArray(new BSelecteurInterface[0]);
    }

    @Override
    public Object getProperty(final String _key) {
      if (_key == EGAxe.PROP_TITLE_VISIBLE) {
        return Boolean.valueOf(target_.isTitreVisible());
      }
      if (_key == EGAxe.PROP_UNIT_VISIBLE) {
        return Boolean.valueOf(target_.isUniteVisible());
      }
      if (_key == EGAxe.PROP_TITLE_CENTERED) {
        return Boolean.valueOf(target_.isTitreCentre());
      }
      if (_key == EGAxeVertical.PROP_TITLE_VERTICAL) {
        return Boolean.valueOf(((EGAxeVertical) target_)
                .isTitreVertical());
      }
      if (_key == EGAxeVertical.PROP_TITLE_VERTICAL_DROITE) {
        return Boolean.valueOf(((EGAxeVertical) target_)
                .isTitreVerticalDroite());
      }
      return null;
    }

    @Override
    public boolean doSetProperty(final String _key, final Object _newProp) {
      final boolean b = ((Boolean) _newProp).booleanValue();
      if (_key == EGAxe.PROP_TITLE_VISIBLE) {
        return target_.setTitreVisible(b);
      }
      if (_key == EGAxe.PROP_UNIT_VISIBLE) {
        return target_.setUniteVisible(b);
      }
      if (_key == EGAxeVertical.PROP_TITLE_VERTICAL) {
        return ((EGAxeVertical) target_).setTitreVertical(b);
      }
      if (_key == EGAxeVertical.PROP_TITLE_VERTICAL_DROITE) {
        return ((EGAxeVertical) target_)
                .setTitreVerticalDroite(b);
      }
      if (_key == EGAxe.PROP_TITLE_CENTERED) {
        return target_.setTitreCentre(b);
      }

      return false;
    }
  }
  final EGParent dispatcher_;
  final EGAxe target_;
  final String title_;

  public EGAxeConfigureTarget(final EGAxe _target, final EGParent _dispatcher, final String _title) {
    super();
    target_ = _target;
    dispatcher_ = _dispatcher;
    title_ = _title;
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return this;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  protected abstract boolean doSetProperty(String _key, Object _newProp);

  @Override
  public final boolean setProperty(final String _key, final Object _newProp) {
    final boolean res = doSetProperty(_key, _newProp);
    if (res && dispatcher_ != null) {
      dispatcher_.fireAxeAspectChanged(target_);
    }
    return res;
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    // target_.removeListener(_key, _l);
  }

  @Override
  public void stopConfiguration() {
  }
}
