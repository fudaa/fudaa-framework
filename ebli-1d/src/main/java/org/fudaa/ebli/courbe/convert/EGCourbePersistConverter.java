/*
 GPL 2
 */
package org.fudaa.ebli.courbe.convert;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import org.fudaa.ebli.converter.TraceToStringConverter;
import org.fudaa.ebli.courbe.EGCourbeMarqueur;
import org.fudaa.ebli.courbe.EGCourbePersist;
import org.fudaa.ebli.courbe.EGPersist;

import java.io.StringWriter;
import java.io.Writer;

/**
 * @author Frederic Deniger
 */
public class EGCourbePersistConverter {

  final TraceToStringConverter traceToStringConverter;

  public EGCourbePersistConverter(TraceToStringConverter traceToStringConverter) {
    this.traceToStringConverter = traceToStringConverter;
  }

  public EGCourbePersistConverter() {
    this(new TraceToStringConverter());
  }

  XStream xstream;

  public XStream getXstream() {
    return xstream;
  }

  public void setXstream(XStream xstream) {
    this.xstream = xstream;
  }

  public void init() {
    if (xstream == null) {
      xstream = EGPersistHelper.createXstream();
      initXstream(xstream);
    }
  }

  public String toXml(EGCourbePersist in) {
    if (xstream == null) {
      init();
    }
    return xstream.toXML(in);
  }

  public String toXmlPretty(EGCourbePersist in) {
    if (xstream == null) {
      init();
    }
    StringWriter writer = new StringWriter();
    xstream.marshal(in, new PrettyPrintWriter(writer, new char[]{' ', ' '}));
    writer.flush();
    return writer.toString();
  }

  public EGCourbePersist fromXml(String in) {
    if (xstream == null) {
      init();
    }
    return (EGCourbePersist) xstream.fromXML(in);
  }

  public void initXstream(final XStream xstream) {
    EGPersistHelper.registerDefaultConverters(xstream, traceToStringConverter);
    xstream.omitField(EGPersist.class, "builderClass");
    xstream.omitField(EGCourbePersist.class, "id");
    xstream.omitField(EGCourbePersist.class, "Idgroup");
    xstream.omitField(EGCourbePersist.class, "specificValues");
    xstream.processAnnotations(EGCourbePersist.class);
    xstream.processAnnotations(EGCourbeMarqueur.class);
  }
}
