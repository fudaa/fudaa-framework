/*
 * @creation 24 juin 2004
 * 
 * @modification $Date: 2007-05-22 14:19:04 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPopupMenu;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuSeparator;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluListSelectionListener;
import org.fudaa.ctulu.CtuluSelectionInterface;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.*;
import org.fudaa.ebli.palette.BPaletteInfoAbstractAction;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EGFillePanel.java,v 1.32 2007-05-22 14:19:04 deniger Exp $
 */
public class EGFillePanel extends BuPanel implements PropertyChangeListener, ActionListener, CtuluSelectionInterface,
        CtuluImageProducer {

  protected final EGVue vue_;
  private boolean addSimplifyAction = true;
  final JLabel lbTools_;
  EbliActionInterface[] personnalAction_;
  final protected EGInteractionSelection selection_;
  EbliActionInterface[] specificTools_;

  /**
   * @param _a le graphe
   */
  public EGFillePanel(final EGGraphe _a) {
    this(_a, null);
  }

  public void setAddSimplifyAction(boolean addSimplifyAction) {
    this.addSimplifyAction = addSimplifyAction;
  }

  public EGFillePanel duplicate() {
    return new EGFillePanel(vue_.getGraphe().duplicate());
  }

  /**
   * doit etre appelee si le panneau est ferm� definitivement
   */
  public void cleanListeners() {
    EbliLib.cleanListener(personnalAction_);
    EbliLib.cleanListener(specificTools_);
  }

  public void addSelectionListener(final CtuluListSelectionListener _l) {
    selection_.addSelectionListener(_l);

  }

  public EGFillePanel(final EGGraphe _a, final EbliActionInterface[] _userAction) {
    setLayout(new BuBorderLayout(5, 0));
    personnalAction_ = _userAction;
    setBackground(Color.WHITE);
    addKeyListener(_a.getRepereController());
    addMouseListener(_a.getRepereController());
    addMouseWheelListener(_a.getRepereController());
    lbTools_ = new JLabel();
    // le nom est important et r�utilis� par d'autres
    lbTools_.setName("lbTools");
    lbTools_.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
    lbTools_.setPreferredSize(new Dimension(200, 20));
    lbTools_.setSize(new Dimension(100, 20));
    lbTools_.setOpaque(false);

    add(lbTools_, BuBorderLayout.SOUTH);
    vue_ = new EGVue(_a);
    add(vue_, BuBorderLayout.CENTER);
    selection_ = new EGInteractionSelection(vue_.graphe_);
    setDoubleBuffered(false);

    // suiviInfos_ = new EGInteractionSuiviAllOrdonnees(vue_.getGraphe());
  }

  protected void addListenerCourbePoint(final CtuluListSelectionListener _l) {
    selection_.getSelection().addListeSelectionListener(_l);
  }

  protected EbliActionInterface[] buildActions() {
    final List r = new ArrayList(8);
    final List actionForGroup = new ArrayList(5);
    // addKeyListener(selection_);
    vue_.graphe_.addKeyListener(selection_);
    vue_.addInteractiveCmp(selection_);
    EGActionInteraction a = new EGActionInteraction(EbliResource.EBLI.getString("S�lection ponctuelle"),
            EbliResource.EBLI.getIcon("fleche"), "SELECT", selection_);
    a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("S�lectionner des points"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('s'));
    r.add(a);
    selection_.addPropertyChangeListener(this);
    actionForGroup.add(a);

    EbliActionAbstract s = new EbliActionSimple(EbliResource.EBLI.getString("Restaurer"), EbliResource.EBLI
            .getIcon("restore"), "RESTORE") {
              @Override
              public void actionPerformed(ActionEvent _e) {
                vue_.getGraphe().restore();
              }
            };
    s.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Restaurer la vue globale"));
    s.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('r'));
    s.putValue(EbliActionInterface.SECOND_KEYSTROKE, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    r.add(s);
    s = new EbliActionChangeState(EbliLib.getS("Restaurer automatiquement"), EbliResource.EBLI.getIcon("restoreauto"),
            "AUTO_REST") {
              @Override
              public void changeAction() {
                getGraphe().setAutoRestore(isSelected());
              }
            };
    // EM:Si la fonctionnalit� 'Restaurer automatiquement' est d�j� activ�e, selectionner l'action.
    ((EbliActionChangeState) s).setSelected(getGraphe().isAutoRestore());

    getGraphe().addPropertyChangeListener("autorestore", this);
    //
    s.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Restaurer automatiquement la vue"));
    r.add(s);
    final EGActionAxeRepereConfigure actionAxeRepereConfigure = new EGActionAxeRepereConfigure(getGraphe());
    r.add(actionAxeRepereConfigure);
    new EGInteractionEditAxe(actionAxeRepereConfigure);
    final EGInteractionZoom zoom = new EGInteractionZoom(vue_.graphe_);
    // addKeyListener(zoom);
    vue_.graphe_.addKeyListener(zoom);
    vue_.addInteractiveCmp(zoom);
    a = new EGActionInteraction(EbliResource.EBLI.getString("Zoom"), EbliResource.EBLI.getIcon("loupe"), "ZOOM", zoom);
    a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Zoom"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('z'));
    r.add(a);
    actionForGroup.add(a);
    zoom.addPropertyChangeListener(this);

    final EGInteractionMove move = new EGInteractionMove(vue_.graphe_);
    move.setDefaultInteractiveComponent(selection_);
    vue_.addInteractiveCmp(move);
    a = new EGActionInteraction(EbliLib.getS("D�placer la vue"), EbliResource.EBLI.getIcon("main"), "MOVE_VIEW", move);
    a.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("D�placer la vue"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('m'));
    r.add(a);
    actionForGroup.add(a);

    final EGInteractionInfoOnCourbe infoOnCourbe = new EGInteractionInfoOnCourbe(vue_.graphe_);
    vue_.addInteractiveCmp(infoOnCourbe);
    a = new EGActionInteraction(EbliLib.getS("Afficher le nom de la courbe sous la souris"), EbliResource.EBLI.getIcon("tooltip"), "INFO",
            infoOnCourbe);
    a.putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Afficher le nom de la courbe sous la souris"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('n'));
    r.add(a);

    final EGInteractionSuivi suivi = new EGInteractionSuivi(vue_.getGraphe());
    vue_.addInteractiveCmp(suivi);
    a = new EGActionInteraction(EbliLib.getS("suivi"), EbliResource.EBLI.getIcon("pointeur"), "SUIVI", suivi);
    a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("suivre le pointeur"));
    a.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('f'));
    r.add(a);
    suivi.addPropertyChangeListener(this);
    actionForGroup.add(a);
    EbliActionSimple actionpal = new EGActionPaletteAllCoordonnees(this);
    r.add(actionpal);

    if (getModel().isContentModifiable()) {
      r.add(null);
      final EGInteractionAddPoint addPt = new EGInteractionAddPoint(this);
      vue_.addInteractiveCmp(addPt);
      addPt.addPropertyChangeListener(this);
      a = new EGActionInteraction(EbliResource.EBLI.getString("Ajouter des points"), BuResource.BU
              .getIcon("crystal_ajouter"), "ADD_POINT", addPt);
      a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Ajouter des points"));
      r.add(a);
      actionForGroup.add(a);
      final EGInteractionDeplacementPoint pt = new EGInteractionDeplacementPoint(this);
      vue_.addInteractiveCmp(pt);
      a = new EGActionInteraction(EbliResource.EBLI.getString("D�placer un point"), EbliResource.EBLI
              .getIcon("node-move-hand"), "MOVE_POINT", pt);
      a.putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("D�placer le point s�lectionn�"));
      r.add(a);
      pt.addPropertyChangeListener(this);
      actionForGroup.add(a);

      if (addSimplifyAction) {
        EbliActionSimple act = new EGSimplificationAction(vue_.graphe_);
        r.add(act);
      }
      r.add(null);

    }

    r.add(new EbliActionSimple(EbliLib.getS("Configurer les courbes"), BuResource.BU.getToolIcon("configurer"), "CONFIGURE") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        EGPaletteLegendeGraphe palette = new EGPaletteLegendeGraphe(getGraphe());
        palette.setBtRefreshTitleVisible(showRefreshButtonInConfigurePanel);
        palette.setPreferredSize(new Dimension(500, 800));
        palette.afficheModale(CtuluLibSwing.getFrameAncestor(EGFillePanel.this));
      }
    });

    final BPaletteInfoAbstractAction info = new BPaletteInfoAbstractAction() {
      @Override
      public JComponent buildContentPane() {
        return new EGPaletteInfo(EGFillePanel.this, null);
      }

      @Override
      protected boolean mustBeUpdated() {
        return getModel().getNbSelectedObjects() >= 0;
      }
    };
    r.add(info);
    r.add(new EGTableAction(getGraphe()));

    new EbliActionGroup(actionForGroup);

    if (personnalAction_ != null) {
      r.add(null);
      final int nb = personnalAction_.length;
      for (int i = 0; i < nb; i++) {
        r.add(personnalAction_[i]);
      }
    }

    // en dernier
    if (getModel().canAddCourbe()) {
      r.add(null);
      final EbliActionSimple addAction = new EbliActionSimple(EbliLib.getS("Ajouter une courbe"), BuResource.BU
              .getToolIcon("creer"), "NEW_CURVE") {
                @Override
                public void actionPerformed(ActionEvent _e) {
                  getModel().addNewCourbe(vue_.graphe_.getCmd(), EGFillePanel.this, EGFillePanel.this.getGraphe());
                  vue_.graphe_.restore();
                }
              };
      getModel().decoreAddButton(addAction);
      addAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_DOWN_MASK
              | InputEvent.SHIFT_DOWN_MASK));
      r.add(addAction);
    }

    final EbliActionInterface[] rf = new EbliActionInterface[r.size()];
    r.toArray(rf);
    EbliLib.updateMapKeyStroke(this, rf);

    return rf;
  }

  protected EGCourbe getCourbeSelected() {
    return vue_.graphe_.getSelectedComponent();
  }

  public EGGraphe getGraphe() {
    return vue_.graphe_;
  }

  protected EGGrapheModel getModel() {
    return vue_.graphe_.getModel();
  }

  public CtuluListSelection getSelection() {
    return selection_.getSelection();
  }

  public void majSelectionListener(final EGTableGraphePanel _tablePanel) {
    if ((selection_ != null) && (_tablePanel != null)) {
      _tablePanel.setListenerToSelection(selection_);
    }
  }

  protected void majLabelInfoAutoRestore() {
    String txt = (String) lbTools_.getClientProperty("actionNameSaved");
    if (getGraphe().isAutoRestore()) {
      txt = "<html><b>&lt;" + EbliLib.getS("Autom. zoom") + "&gt;</b> " + txt;
    }
    lbTools_.setText(txt);
  }

  protected void majLabelInfo(final String _s) {
    lbTools_.putClientProperty("actionNameSaved", _s);
    majLabelInfoAutoRestore();
  }

  protected void fillPopupMenu(final BuPopupMenu _menu) {
  }

  public void popupMenu(final int _x, final int _y) {
    final BuPopupMenu m = new BuPopupMenu();
    m.setInvoker(this);
    fillPopupMenu(m);
    fillMenuWithActions(m);
    m.add(new BuSeparator());
    fillSpecificMenu(m);
    m.show(this, _x, _y);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if (FuLog.isTrace()) {
      FuLog.trace(getClass().getName() + " action " + _evt.getActionCommand());
    }
    final String c = _evt.getActionCommand();
    if ("MODIFY_POINTS".equals(c)) {
      EGTransfoLib.modifier(vue_.graphe_, getCourbeSelected(), getSelection().getSelectedIndex(), vue_.getGraphe()
              .getCmd());
    } else if ("ALIGN_POINTS".equals(c)) {
      final EGCourbe courbe = getCourbeSelected();
      if (courbe != null) {
        final CtuluListSelectionInterface i = getSelection();
        if ((i != null) && (i.getNbSelectedIndex() >= 2)) {
          EGTransfoLib.aligner(vue_.graphe_, courbe, i.getMinIndex(), i.getMaxIndex(), vue_.getGraphe().getCmd());
        }
      }
    } else if ("ADD_POINTS".equals(c)) {
      final CtuluListSelectionInterface i = getSelection();
      if ((i != null) && (i.getNbSelectedIndex() >= 2)) {
        EGTransfoLib.ajouterPoint(vue_.graphe_, getCourbeSelected(), i.getMinIndex(), i.getMaxIndex(), vue_.getGraphe()
                .getCmd());
      }
    } else if ("REMOVE_POINTS".equals(c)) {
      final EGCourbe courbe = getCourbeSelected();
      if (courbe != null) {
        final CtuluListSelectionInterface i = getSelection();
        if (courbe.getModel().removeValue(i.getSelectedIndex(), vue_.getGraphe().getCmd())) {
          selection_.getSelection().clear();
        }
      }
    }
  }

  /**
   * @param _m le menu a remplir avec les actions de ce panel
   */
  public void fillSpecificMenu(final JMenu _m) {
    final EbliActionInterface[] ac = getSpecificActions();
    if (ac == null) {
      return;
    }
    final int n = ac.length;
    for (int i = 0; i < n; i++) {
      final EbliActionInterface a = ac[i];
      if (a == null) {
        _m.add(new BuSeparator());
      } else {
        _m.add(a.buildMenuItem(EbliComponentFactory.INSTANCE));
      }
    }
  }

  /**
   * @param _popup le menu a remplir avec les actions de ce panel
   */
  public void fillSpecificMenu(final JPopupMenu _popup) {
    final EbliActionInterface[] ac = getSpecificActions();
    if (ac == null) {
      return;
    }
    final int n = ac.length;
    for (int i = 0; i < n; i++) {
      final EbliActionInterface a = ac[i];
      if (a == null) {
        _popup.add(new BuSeparator());
      } else {
        _popup.add(a.buildMenuItem(EbliComponentFactory.INSTANCE));
      }
    }
  }

  @Override
  public void find() {
  }

  /**
   * @return la taille de l'image generee par defaut par ce composant
   */
  @Override
  public Dimension getDefaultImageDimension() {
    return vue_.getDefaultImageDimension();
  }

  public EGVue getView() {
    return vue_;
  }

  public EbliActionInterface[] getSpecificActions() {
    if (specificTools_ == null) {
      specificTools_ = buildActions();
    }
    return specificTools_;
  }
  private boolean showRefreshButtonInConfigurePanel = true;

  public boolean isShowRefreshButtonInConfigurePanel() {
    return showRefreshButtonInConfigurePanel;
  }

  public void setShowRefreshButtonInConfigurePanel(boolean showRefreshButtonInConfigurePanel) {
    this.showRefreshButtonInConfigurePanel = showRefreshButtonInConfigurePanel;
  }

  @Override
  public void clearSelection() {
    selection_.clearSelection();
  }

  @Override
  public void inverseSelection() {
    selection_.inverseSelection();
  }

  /**
   * @return l'image du graphe
   */
  @Override
  public BufferedImage produceImage(final Map _params) {

    return vue_.produceImage(_params);
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return vue_.produceImage(_w, _h, _params);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if ("autorestore".equals(_evt.getPropertyName())) {
      majLabelInfoAutoRestore();

    }
    if (_evt.getSource() instanceof EGInteractiveComponent) {
      final EGInteractiveComponent c = (EGInteractiveComponent) _evt.getSource();
      if (c.isActive()) {
        majLabelInfo(c.getDescription());
      } else {
        majLabelInfo(CtuluLibString.EMPTY_STRING);
      }
    }
  }

  @Override
  public void replace() {
  }

  @Override
  public void select() {
    selection_.selectAll();
  }

  /**
   * Les outils seront reconstruits avec les nouvelles actions suppl�mentaires.
   *
   * @param _a nouvelles actions a ajouter.
   */
  public final void setPersonnalAction(final EbliActionInterface[] _a) {
    if (specificTools_ != null) {
      specificTools_ = null;
    }
    personnalAction_ = _a;
  }
  BuMenuItem modifiyPointsMenuItem;
  BuMenuItem alignPointMenuItem;
  BuMenuItem refinePointMenuItem;
  BuMenuItem removePointMenuItem;

  public void fillMenuWithActions(final BuPopupMenu m) {
    modifiyPointsMenuItem = addModifiyPointsMenuItem(m);
    alignPointMenuItem = addAlignPointMenuItem(m);
    refinePointMenuItem = addRefinePointMenuItem(m);
    removePointMenuItem = addRemovePointMenuItem(m);
  }

  public boolean isSelectedCourbeModifiable() {
    final EGCourbe c = vue_.graphe_.getSelectedComponent();
    return (c != null) && (c.getModel().isModifiable()) && (getSelection() != null)
            && (!getSelection().isEmpty());
  }

  public boolean isSelectionAlignable() {
    return isSelectedCourbeModifiable() && (getSelection().getNbSelectedIndex() >= 2);
  }

  public boolean isSelectionRefinable() {
    return isSelectedCourbeModifiable()
            && (getSelection().getNbSelectedIndex() >= 2);
  }

  public boolean isSelectionRemovable() {
    return isSelectedCourbeModifiable();
  }

  public BuMenuItem addModifiyPointsMenuItem(final BuPopupMenu m) {
    return m.addMenuItem(EbliLib.getS("Modifier les points s�lectionn�s"), "MODIFY_POINTS", EbliResource.EBLI
            .getToolIcon("node-edit"), isSelectedCourbeModifiable());
  }

  public BuMenuItem addModifiyPointsMenuItem(final CtuluPopupMenu m) {
    return m.addMenuItem(EbliLib.getS("Modifier les points s�lectionn�s"), "MODIFY_POINTS", EbliResource.EBLI
            .getToolIcon("node-edit"), isSelectedCourbeModifiable());
  }

  public BuMenuItem addAlignPointMenuItem(final CtuluPopupMenu m) {
    return m.addMenuItem(EbliLib.getS("Aligner les points"), "ALIGN_POINTS", EbliResource.EBLI.getToolIcon("node-corner"), isSelectionAlignable());
  }

  public BuMenuItem addAlignPointMenuItem(final BuPopupMenu m) {
    return m.addMenuItem(EbliLib.getS("Aligner les points"), "ALIGN_POINTS", EbliResource.EBLI.getToolIcon("node-corner"), isSelectionAlignable());
  }

  public BuMenuItem addRefinePointMenuItem(final CtuluPopupMenu m) {
    return m.addMenuItem(EbliLib.getS("Raffiner"), "ADD_POINTS", EbliResource.EBLI.getToolIcon("node-add"), isSelectionRefinable());
  }

  public BuMenuItem addRefinePointMenuItem(final BuPopupMenu m) {
    return m.addMenuItem(EbliLib.getS("Raffiner"), "ADD_POINTS", EbliResource.EBLI.getToolIcon("node-add"), isSelectionRefinable());
  }

  public BuMenuItem addRemovePointMenuItem(final CtuluPopupMenu m) {
    final BuMenuItem addMenuItem = m.addMenuItem(EbliLib.getS("Supprimer un point"), "REMOVE_POINTS", EbliResource.EBLI.getToolIcon("node-delete"),
            isSelectionRemovable());
    addMenuItem.setAccelerator(EGTableGraphePanel.getDeleteKeyStroke());
    return addMenuItem;
  }

  public BuMenuItem addRemovePointMenuItem(final BuPopupMenu m) {
    final BuMenuItem addMenuItem = m.addMenuItem(EbliLib.getS("Supprimer un point"), "REMOVE_POINTS", EbliResource.EBLI.getToolIcon("node-delete"),
            isSelectionRemovable());
    addMenuItem.setAccelerator(EGTableGraphePanel.getDeleteKeyStroke());
    return addMenuItem;
  }
}
