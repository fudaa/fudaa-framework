/*
 * License GPL v2
 */
package org.fudaa.ebli.courbe;

import java.util.Set;
import javax.swing.JComponent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EGActionAxeRepereConfigure.java,v 1.10 2007-05-04 13:49:41 deniger Exp $
 */
@SuppressWarnings("serial")
public class EGActionAxeRepereConfigure extends EbliActionPaletteAbstract implements
        EGSelectionListener, EGGrapheModelListener {

  @Override
  public void axeAspectChanged(final EGAxe _c) {
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
    if (axeEditor_ != null && isSelected()) {
      axeEditor_.updateAxesValue();
    }
  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean _mustRestore) {
  }

  @Override
  public void structureChanged() {
  }

  final EGGraphe model_;
  EGAxeRepereConfigurator axeEditor_;

  /**
   * @param _g le model a considerer
   */
  public EGActionAxeRepereConfigure(final EGGraphe _g) {
    super(EbliLib.getS("Rep�re"), EbliResource.EBLI.getToolIcon("edit-axes"),
            "CONFIGURE_REPERE");
    model_ = _g;
    model_.getModel().addSelectionListener(this);
    model_.getModel().addModelListener(this);
    setResizable(true);
  }

  public EGGraphe getGraphe() {
    return model_;
  }

  public EGAxeRepereConfigurator getAxeEditor() {
    return axeEditor_;
  }

  @Override
  public JComponent buildContentPane() {
    if (axeEditor_ != null) {
      return axeEditor_;
    }
    axeEditor_ = new EGAxeRepereConfigurator(model_);
    axeEditor_.setTargets(model_.getModel().getSelectedObjects());
    return axeEditor_;
  }

  @Override
  public void updateBeforeShow() {
    if (!ignoreTarget) {
      updateTarget();
    }
  }

  boolean ignoreTarget;

  protected void showOnTargetAxis(final Set s) {
    buildContentPane();
    axeEditor_.setTargetsAxis(s);
    ignoreTarget = true;
    actionPerformed(null);
    ignoreTarget = false;

  }

  private void updateTarget() {
    if (axeEditor_ != null && isSelected()) {
      axeEditor_.setTargets(model_.getModel().getSelectedObjects());
    }
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    updateTarget();
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    updateTarget();
  }
}
