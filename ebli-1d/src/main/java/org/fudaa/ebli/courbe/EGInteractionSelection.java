/*
 * @creation 22 juin 2004
 * @modification $Date: 2007-05-22 14:19:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionEvent;
import org.fudaa.ctulu.CtuluListSelectionListener;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceIcon;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionSelection.java,v 1.19 2007-05-22 14:19:04 deniger Exp $
 */
public class EGInteractionSelection extends EGInteractiveComponent implements MouseListener, EGSelectionListener,
        KeyListener, CtuluListSelectionListener {

  /**
   * @return valeur par defaut pour la precision en pixel.
   */
  public final static int getPrecisionPixel() {
    return 5;
  }
  private CtuluListSelection s_;
  final EGGraphe a_;
  TraceIcon ic_;
  int lastIndex_ = -1;
  final EGSelectionState select_;

  /**
   * @param _a le graphe cible
   */
  public EGInteractionSelection(final EGGraphe _a) {
    a_ = _a;
    a_.addMouseListener(this);
    a_.getModel().addSelectionListener(this);
    select_ = new EGSelectionState();
  }
  
  private void updateSelection(final int _idxSelected) {
    repaint();
    boolean b = false;
    if (_idxSelected < 0) {
      if (select_.isReplaceMode()) {
        b = !s_.isEmpty();
        s_.clear();
      }
    } else {
      if (select_.isReplaceMode()) {
        b = s_.isEmpty() || !s_.isOnlyOnIndexSelected() || !s_.isSelected(_idxSelected);
        s_.setSelectionInterval(_idxSelected, _idxSelected);
      } else if (select_.isAddMode()) {
        if (s_.isSelected(_idxSelected)) {
          b = s_.remove(_idxSelected);
        } else {
          b = s_.add(_idxSelected);
        }
      } else if (select_.isMajMode()) {
        if (lastIndex_ >= 0) {
          b = s_.addInterval(lastIndex_, _idxSelected);
        } else {
          b = s_.add(_idxSelected);
        }
      }
    }
    if (b) {
      a_.repaint();
    }
    lastIndex_ = _idxSelected;
    a_.requestFocus();
    getParent().repaint();
  }
  
  public void inverseSelection() {
    if (isAllSelected()) {
      clearSelection();
    } else if (a_.getSelectedComponent() != null && !isSelectionEmpty()) {
      s_.inverse(a_.getSelectedComponent().getModel().getNbValues());
      a_.repaint();
    }
  }
  
  public boolean isAllSelected() {
    if (a_.getSelectedComponent() != null && !isSelectionEmpty()) {
      return s_.getMinIndex() == 0 && s_.getMaxIndex() == a_.getSelectedComponent().getModel().getNbValues() - 1;
    }
    return false;
  }
  
  public void clearSelection() {
    if (a_.getSelectedComponent() != null && !isSelectionEmpty()) {
      s_.clear();
      a_.repaint();
    }
  }
  
  public void selectAll() {
    if (a_.getSelectedComponent() != null) {
      getSelection();
      s_.setSelectionInterval(0, a_.getSelectedComponent().getModel().getNbValues() - 1);
      a_.repaint();
    }
  }
  
  @Override
  public String getDescription() {
    String r = EbliLib.getS("S�lection ponctuelle");
    if (select_.getControleDesc() != null) {
      r = r + CtuluLibString.ESPACE + select_.getControleDesc();
    }
    return r;
  }
  
  public EGRepere getRepere() {
    return a_.getTransformer();
  }
  
  public CtuluListSelection getSelection() {
    if (s_ == null) {
      s_ = new CtuluListSelection();
      s_.addListeSelectionListener(this);
    }
    return s_;
  }
  
  public void addSelectionListener(final CtuluListSelectionListener _l) {
    getSelection().addListeSelectionListener(_l);
  }
  
  public TraceIcon getSelectionIcone() {
    if (ic_ == null) {
      ic_ = new TraceIcon(TraceIcon.CARRE_SELECTION, 4);
      ic_.setCouleur(Color.blue);
    }
    return ic_;
  }

  /**
   * @return true si la selection est vide.
   */
  public boolean isSelectionEmpty() {
    return (s_ == null) || s_.isEmpty();
  }
  
  @Override
  public void keyPressed(final KeyEvent _e) {
    if (_e.getKeyCode() == KeyEvent.VK_ESCAPE) {
      setActive(true);
    }
    if (isActive()) {
      final String old = select_.getControleDesc();
      select_.majControleDesc(_e);
      firePropertyChange("state", old, select_.getControleDesc());
    }
  }
  
  @Override
  public void keyReleased(final KeyEvent _e) {
    keyPressed(_e);
  }
  
  @Override
  public void keyTyped(final KeyEvent _e) {
  }
  
  @Override
  public void listeSelectionChanged(final CtuluListSelectionEvent _e) {
    final EGCourbe c = a_.getSelectedComponent();
    if (c != null) {
      final EGModel model = c.getModel();
      CtuluListSelection toRemove = null;
      final int[] idx = s_.getSelectedIndex();
      if (idx != null) {
        for (int i = 0; i < idx.length; i++) {
          if (!model.isPointDrawn(idx[i])) {
            if (toRemove == null) {
              toRemove = new CtuluListSelection();
            }
            toRemove.add(idx[i]);
          }
        }
      }
      if (toRemove != null) {
        s_.remove(toRemove);
        
      }
      
    }
    repaint();
    
  }
  
  @Override
  public void mouseClicked(final MouseEvent _e) {
  }
  
  @Override
  public void mouseEntered(final MouseEvent _e) {
  }
  
  @Override
  public void mouseExited(final MouseEvent _e) {
  }
  
  @Override
  public void mousePressed(final MouseEvent _e) {
  }
  
  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    if ((!isActive()) || (a_.getSelectedComponent() == null) || (_e.isPopupTrigger())) {
      return;
    }
    if (!a_.getSelectedComponent().isVisible_) {
      return;
    }
    if (s_ == null) {
      getSelection();
      // s_ = new EbliListeSelection();
      // s_.clear();
    }
    
    final int i = a_.getSelectedComponent().select(_e.getX(), _e.getY(), getRepere(), getPrecisionPixel());
    if (i >= 0 && a_.getSelectedComponent().getModel().isPointDrawn(i)) {
      select_.majControleDesc(_e);
      updateSelection(i);
    }
  }
  
  @Override
  public void paintComponent(final Graphics _d) {
    if ((s_ == null) || (s_.isEmpty())) {
      return;
    }
    if (a_.getSelectedComponent() != null && a_.getSelectedComponent().isVisible_) {
      a_.getSelectedComponent().paintSelection((Graphics2D) _d, this);
    }
  }
  
  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    a_.requestFocus();
    a_.requestFocusInWindow();
  }
  
  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if ((s_ != null) && (!s_.isEmpty())) {
      s_.clear();
      repaint();
    }
  }
  
  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    if ((s_ != null) && (!s_.isEmpty())) {
      s_.clear();
      repaint();
    }
  }
}
