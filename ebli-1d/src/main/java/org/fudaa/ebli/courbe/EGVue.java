/*
 *  @creation     22 juin 2004
 *  @modification $Date: 2007-05-04 13:49:41 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.Map;
import javax.swing.JComponent;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.CalqueLayout;

/**
 * @author Fred Deniger
 * @version $Id: EGVue.java,v 1.7 2007-05-04 13:49:41 deniger Exp $
 */
public class EGVue extends JComponent implements CtuluImageProducer {

  final EGGraphe graphe_;

  public EGVue(final EGGraphe _g) {
    setLayout(new CalqueLayout());
    add(_g);
    setOpaque(true);
    setBackground(Color.WHITE);
    graphe_ = _g;
  }

  public void addInteractiveCmp(final EGInteractiveComponent _c) {
    add(_c, 0);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return graphe_.getDefaultImageDimension();
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return graphe_.produceImage(_w, _h, _params);
  }

  @Override
  public BufferedImage produceImage(final Map _params) {
    return graphe_.produceImage(_params);
  }

  public EGGraphe getGraphe() {
    return graphe_;
  }

}
