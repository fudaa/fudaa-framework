/*
 * @creation 28 nov. 06
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuTextField;
import java.awt.Color;
import java.awt.Font;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.*;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * FIXME a reprendre
 *
 * @author fred deniger
 * @version $Id: EGCourbeConfigureTarget.java,v 1.2 2007-05-04 13:49:41 deniger Exp $
 */
public abstract class EGCourbeConfigureTarget implements /*BConfigurableSectionInterface, */ BSelecteurTargetInterface,
        BConfigurableInterface {

  public static final String SPECIFIC_ICON = "SPECIFIC_ICON";

  public static class Display extends EGCourbeConfigureTarget {

    public Display(final EGCourbe _target) {
      super(_target, EbliLib.getS("Affichage"));
    }

    @Override
    public Object getProperty(final String _key) {
      if (BSelecteurIconModel.DEFAULT_PROPERTY.equals(_key)) {
        return target_.getIconModel();
      }
      if (SPECIFIC_ICON.equals(_key)) {
        return target_.getIconModelSpecific();
      }
      if (BSelecteurColorChooser.DEFAULT_PROPERTY.equals(_key)) {
        return target_.getAspectContour();
      }
      if (BSelecteurLineModel.PROPERTY.equals(_key)) {
        return target_.getLigneModel();
      }
      if (BSelecteurAlpha.DEFAULT_PROPERTY.equals(_key)) {
        return new Integer(target_.getAlpha());
      }
      if (PROP_DISPLAY_TITLE_CURVE.equals(_key)) {
        return Boolean.valueOf(target_.isDisplayTitleOnCurve());
      }
      if (PROP_DISPLAY_POINT_LABELS.equals(_key)) {
        return Boolean.valueOf(target_.isDisplayPointLabels());
      }
      if (PROP_DISPLAY_VERTICAL_LABELS.equals(_key)) {
        return Boolean.valueOf(target_.isVerticalLabels());
      }
      if (BSelecteurReduitFonteNewVersion.PROPERTY.equals(_key)) {
        return target_.getFont();
      }
      if (EGCourbeConfigureTarget.PROP_NUAGE_POINTS.equals(_key)) {
        return target_.isNuagePoints();
      }

      return null;
    }

    @Override
    public boolean setProperty(final String _key, final Object _newProp) {
      if (BSelecteurIconModel.DEFAULT_PROPERTY.equals(_key)) {
        return target_.setIconeModel((TraceIconModel) _newProp);
      }
      if (SPECIFIC_ICON.equals(_key)) {
        return target_.setIconeModelSpecific((TraceIconModel) _newProp);
      }
      if (BSelecteurColorChooser.DEFAULT_PROPERTY.equals(_key)) {
        return target_.setAspectContour((Color) _newProp);
      }
      if (BSelecteurLineModel.PROPERTY.equals(_key)) {
        return target_.setLigneType((TraceLigneModel) _newProp);
      }
      if (BSelecteurAlpha.DEFAULT_PROPERTY.equals(_key)) {
        return target_.setAlpha(((Integer) _newProp).intValue());
      }
      if (PROP_DISPLAY_TITLE_CURVE.equals(_key)) {
        return target_.setDisplayTitleOnCurve(((Boolean) _newProp).booleanValue());
      }
      if (PROP_DISPLAY_POINT_LABELS.equals(_key)) {
        return target_.setDisplayPointLabels(((Boolean) _newProp).booleanValue());
      }
      if (PROP_DISPLAY_VERTICAL_LABELS.equals(_key)) {
        return target_.setVerticalLabels(((Boolean) _newProp).booleanValue());
      }
      if (BSelecteurReduitFonteNewVersion.PROPERTY.equals(_key)) {
        return target_.setFont((Font) _newProp);
      }
      if (EGCourbeConfigureTarget.PROP_NUAGE_POINTS.equals(_key)) {

        final boolean value = ((Boolean) _newProp).booleanValue();
        return target_.setNuagePointEtApplique(value);
      }

      return false;
    }

    @Override
    public BSelecteurInterface[] createSelecteurs() {
      final boolean useSpecific = target_.isUseSpecificIcon();
      final BSelecteurInterface[] res = new BSelecteurInterface[useSpecific ? 10 : 9];
      int idx = 0;
      res[idx] = new BSelecteurColorChooserBt();
      ((BSelecteurColorChooserBt) res[idx]).setAddListenerToTarget(false);
      ((BSelecteurColorChooserBt) res[idx++]).setTitle(EbliLib.getS("Couleur du trac�"));
      res[idx++] = new BSelecteurAlpha();
      res[idx++] = new BSelecteurLineModel();
      res[idx++] = new BSelecteurIconModel();
      if (useSpecific) {
        final BSelecteurIconModel bSelecteurIconModel = new BSelecteurIconModel(SPECIFIC_ICON);
        bSelecteurIconModel.setTitle(target_.getSpecificIconTitle());
        res[idx++] = bSelecteurIconModel;
      }
      res[idx] = new BSelecteurCheckBox(PROP_NUAGE_POINTS);
      ((BSelecteurCheckBox) res[idx++]).setTitle(EbliLib.getS("Nuage de points"));
      res[idx] = new BSelecteurCheckBox(PROP_DISPLAY_TITLE_CURVE);
      ((BSelecteurCheckBox) res[idx++]).setTitle(EbliLib.getS("Afficher le titre sur la courbe"));
      res[idx++] = new BSelecteurFont();
      res[idx] = new BSelecteurCheckBox(PROP_DISPLAY_POINT_LABELS);
      ((BSelecteurCheckBox) res[idx++]).setTitle(EbliLib.getS("Afficher les labels des points"));
      res[idx] = new BSelecteurCheckBox(PROP_DISPLAY_VERTICAL_LABELS);
      ((BSelecteurCheckBox) res[idx++]).setTitle(EbliLib.getS("Labels verticaux"));
      return res;
    }
  }

  public static class Marks extends EGCourbeConfigureTarget {

    public Marks(final EGCourbe _target) {
      super(_target, EbliLib.getS("Marqueurs"));
    }

    @Override
    public Object getProperty(final String _key) {
      if (_key == EGCourbeConfigureTarget.PROP_MARK_LINE) {
        return target_.getMarkLigneModel();
      }
      if (_key == EGCourbeConfigureTarget.PROP_MARK_MAX) {
        return Boolean.valueOf(target_.isMarkMaxLine());
      }
      if (_key == EGCourbeConfigureTarget.PROP_MARK_MIN) {
        return Boolean.valueOf(target_.isMarkMinLine());
      }


      //-- gestion des marqueurs --//
      //-- nouvelle valeur marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_VALUES)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_VALUES);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          return "" + target_.getMarqueur(indice).getValue();
        }
      }
      //-- nouvelle visibilite marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_SHOW_VALUES)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_SHOW_VALUES);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          return  Boolean.valueOf(target_.getMarqueur(indice).isVisible());

        }
      }
      //-- nouvelle alignement marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_VALUES_HORIZONTAL)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_VALUES_HORIZONTAL);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          return Boolean.valueOf(target_.getMarqueur(indice).isTraceHorizontal());
        }
      }
      //-- nouveau linemodel marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_LINE_VALUES)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_LINE_VALUES);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          return target_.getMarqueur(indice).getModel();
        }
      }
      return null;
    }

    @Override
    public boolean setProperty(final String _key, final Object _newProp) {
      if (_key == EGCourbeConfigureTarget.PROP_MARK_LINE) {
        return target_.setLigneMark((TraceLigneModel) _newProp);
      }
      if (_key == EGCourbeConfigureTarget.PROP_MARK_MAX) {
        return target_.setMarkMaxLine(((Boolean) _newProp).booleanValue());
      }
      if (_key == EGCourbeConfigureTarget.PROP_MARK_MIN) {
        return target_.setMarkMinLine(((Boolean) _newProp).booleanValue());
      }

      //-- nouvelle valeur marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_VALUES)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_VALUES);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          final String val = (String) _newProp;
          final double newValue = Double.parseDouble(val);
          target_.getMarqueur(indice).setValue(newValue);
          target_.modifyMarqueur(target_.getMarqueur(indice));
          return true;
        }
      }
      //-- nouvelle visibilite marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_SHOW_VALUES)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_SHOW_VALUES);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          final boolean newValue = ((Boolean) _newProp).booleanValue();
          target_.getMarqueur(indice).setVisible(newValue);
          target_.modifyMarqueur(target_.getMarqueur(indice));
          return true;
        }
      }
      //-- nouvelle alignement marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_VALUES_HORIZONTAL)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_VALUES_HORIZONTAL);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          final boolean newValue = ((Boolean) _newProp).booleanValue();
          target_.getMarqueur(indice).setTraceHorizontal(newValue);
          target_.modifyMarqueur(target_.getMarqueur(indice));
          return true;
        }
      }
      //-- nouveau linemodel marqueur --//			
      if (_key.startsWith(EGCourbeConfigureTarget.PROP_MARK_LINE_VALUES)) {
        final int indice = extractIndiceMarqueur(_key, EGCourbeConfigureTarget.PROP_MARK_LINE_VALUES);
        if (indice != -1 && indice < target_.getMarqueurs().size()) {
          final TraceLigneModel newValue = ((TraceLigneModel) _newProp);
          target_.getMarqueur(indice).getModel().updateData(newValue);
          target_.modifyMarqueur(target_.getMarqueur(indice));
          return true;
        }
      }

      //-- creation marqueur --//
      if (_key == EGCourbeConfigureTarget.PROP_MARK_VALUES_ADD) {
        target_.addMarqueur();
        //-- on reload la palette --//
        final Map infos = new HashMap();
        infos.put(EGCourbe.INDICE_PANEL_SHOW, new Integer(1));
        return target_.reloadPalette(infos);

      }
      //-- supression --//
      if (_key == EGCourbeConfigureTarget.PROP_MARK_VALUES_DELETE_LAST) {
        target_.deleteLastMarqueur();
        //-- on reload la palette --//
        final Map infos = new HashMap();
        infos.put(EGCourbe.INDICE_PANEL_SHOW, new Integer(1));
        target_.modifyMarqueur(target_.getMarqueur(0));
        return target_.reloadPalette(infos);

      }
      //-- masquer --//
      if (_key == EGCourbeConfigureTarget.PROP_MARK_VALUES_HIDEALL) {
        target_.hideAllMarqueur();
        //-- on reload la palette --//
        final Map infos = new HashMap();
        infos.put(target_.INDICE_PANEL_SHOW, new Integer(1));
        return target_.reloadPalette(infos);

      }
      return false;
    }

    /**
     * Extrait l'indice de puis la clef a partir de la string totale prop.
     *
     * @param key
     * @param prop
     * @return
     */
    private int extractIndiceMarqueur(final String key, final String prop) {
      int indice = -1;
      final String valeur = key.substring(prop.length(), key.length());
      try {
        indice = Integer.parseInt(valeur);
      } catch (final NumberFormatException e) {
        indice = -1;
      }
      return indice;


    }

    @Override
    public BSelecteurInterface[] createSelecteurs() {
      final List<BSelecteurInterface> res = new ArrayList<>();
      BSelecteurInterface inter;

      //-- GESTION MIN ET MaX --//
      inter = new BSelecteurCheckBox(EGCourbeConfigureTarget.PROP_MARK_MAX);
      ((BSelecteurCheckBox) inter).setTitle(EbliLib.getS("Marquer valeur maximale"));
      res.add(inter);
      inter = new BSelecteurCheckBox(EGCourbeConfigureTarget.PROP_MARK_MIN);
      ((BSelecteurCheckBox) inter).setTitle(EbliLib.getS("Marquer valeur minimale"));
      res.add(inter);
      inter = new BSelecteurLineModel(EGCourbeConfigureTarget.PROP_MARK_LINE);
      res.add(inter);



      //---------------------------------------------------------------------------------------------------//
      //-- a partir de la on ajoute des composite: checkbox+jtxtfield+ linemodel pour tous les marqueurs --//
      BSelecteurComposite composition = null;
      //-- compteur indice marqueur --//
      int cpt = 0;
      for (final EGCourbeMarqueur marqueur : target_.getMarqueurs()) {
        final BSelecteurInterface[] listeTocompose = new BSelecteurInterface[4];
        //-- checkbox --//
        inter = new BSelecteurCheckBox(EGCourbeConfigureTarget.PROP_MARK_SHOW_VALUES + cpt, EbliLib.getS(" visible"));
        ((BSelecteurCheckBox) inter).setTitle(EbliLib.getS("Marqueur ") + (cpt + 1));
        listeTocompose[0] = inter;
        //-- textfield --//
        final BuTextField txt = new BuTextField();
        txt.setText("" + marqueur.getValue());
        inter = new BSelecteurTextField(EGCourbeConfigureTarget.PROP_MARK_VALUES + cpt, txt);
        listeTocompose[1] = inter;
        //-- lineModel --//
        inter = new BSelecteurLineModel(EGCourbeConfigureTarget.PROP_MARK_LINE_VALUES + cpt, marqueur.model_);
        listeTocompose[2] = inter;
        //-- affichage horizontal ou vertical
        inter = new BSelecteurCheckBox(EGCourbeConfigureTarget.PROP_MARK_VALUES_HORIZONTAL + cpt, EbliLib.getS("Horizontal"));
        ((BSelecteurCheckBox) inter).setTitle(EbliLib.getS("Horizontal"));
        ((BSelecteurCheckBox) inter).setTooltip(EbliLib.getS("coch� pour Horizontal, sinon marqueur vertical"));
        listeTocompose[3] = inter;

        //-- ajout de la composition
        composition = new BSelecteurComposite(listeTocompose, true);
        composition.setTitle(EbliLib.getS("Marqueur ") + (cpt + 1));
        res.add(composition);
        cpt++;
      }


      //-- ajout,suppression et masquage de marqueur --//
      final BSelecteurInterface[] listeTocompose = new BSelecteurInterface[3];

      inter = new BSelecteurButton(EGCourbeConfigureTarget.PROP_MARK_VALUES_HIDEALL, EbliLib.getS("Tout masquer"));
      listeTocompose[0] = inter;
      inter = new BSelecteurButton(EGCourbeConfigureTarget.PROP_MARK_VALUES_ADD, EbliLib.getS("Ajouter"));
      listeTocompose[1] = inter;
      inter = new BSelecteurButton(EGCourbeConfigureTarget.PROP_MARK_VALUES_DELETE_LAST, EbliLib.getS("Supprimer dernier marqueur"));
      listeTocompose[2] = inter;
      composition = new BSelecteurComposite(listeTocompose, true);
      res.add(composition);

      return res.toArray(new BSelecteurInterface[0]);
    }
  }
  protected final EGCourbe target_;
  final String title_;
  public final static String PROP_NUAGE_POINTS = "nuagePOints";
  public final static String PROP_MARK_MAX = "courbeMarkMax";
  public final static String PROP_DISPLAY_TITLE_CURVE = "courbeDisplayCurve";
  public final static String PROP_MARK_MIN = "courbeMarkMin";
  public final static String PROP_MARK_LINE = "courbeMarkLine";
  public final static String PROP_DISPLAY_POINT_LABELS = "courbeDisplayPointsLabels";
  public final static String PROP_DISPLAY_VERTICAL_LABELS = "verticalLabels";
  //-- proprietes propres aux liste des marqueurs --//
  public final static String PROP_MARK_VALUES = "courbeMarkValues";
  public final static String PROP_MARK_SHOW_VALUES = "courbeShowMarkValues";
  public final static String PROP_MARK_LINE_VALUES = "courbeMarkLineValues";
  public final static String PROP_MARK_VALUES_HORIZONTAL = "courbeMarkHorizontal";
  public final static String PROP_MARK_VALUES_ADD = "addMark";
  public final static String PROP_MARK_VALUES_HIDEALL = "hideAllMark";
  public final static String PROP_MARK_VALUES_DELETE_LAST = "deletelastMark";

  public EGCourbeConfigureTarget(final EGCourbe _target, final String _title) {
    super();
    target_ = _target;
    title_ = _title;
  }



  @Override
  public BSelecteurTargetInterface getTarget() {
    return this;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public void stopConfiguration() {
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.addListener(_key, _l);
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.removeListener(_key, _l);

  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }
}
