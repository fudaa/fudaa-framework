/*
 * @creation 22 juin 2004
 * @modification $Date: 2007-05-04 13:49:41 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionZoom.java,v 1.13 2007-05-04 13:49:41 deniger Exp $
 */
public class EGInteractionMove extends EGInteractiveComponent implements MouseListener, KeyListener,
        MouseMotionListener {

  Point pt1_;
  final EGGraphe target_;
  private boolean restoreDefaultInteractiveComponent;
  private EGInteractiveComponent defaultInteractiveComponent;

  public EGInteractionMove(final EGGraphe _target) {
    target_ = _target;
    target_.addMouseListener(this);
    target_.addMouseMotionListener(this);
  }

  public void setDefaultInteractiveComponent(EGInteractiveComponent defaultInteractiveComponent) {
    this.defaultInteractiveComponent = defaultInteractiveComponent;
  }

  @Override
  public String getDescription() {
    return "move";
  }

  public EGGraphe getTarget() {
    return target_;
  }

  @Override
  public void keyPressed(final KeyEvent _e) {
  }

  @Override
  public void keyReleased(final KeyEvent _e) {
  }

  @Override
  public void keyTyped(final KeyEvent _e) {
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (_e.isConsumed()) {
      return;
    }

    if (!isActive() || (pt1_ == null)) {
      return;
    }
    final Point f = _e.getPoint();
    target_.moveViewFromMouseDeltas(f.x - pt1_.x, f.y - pt1_.y);
    pt1_ = f;
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (_e.isConsumed() || EbliLib.isPopupMouseEvent(_e)) {
      return;
    }
    if (SwingUtilities.isMiddleMouseButton(_e)) {
      if (!isActive()) {
        restoreDefaultInteractiveComponent = true;
      }
      setActive(true);
    }
    pt1_ = _e.getPoint();
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    mouseDragged(_e);
    if (restoreDefaultInteractiveComponent) {
      setActive(false);
      if (defaultInteractiveComponent != null) {
        defaultInteractiveComponent.setActive(true);
      }
      restoreDefaultInteractiveComponent = false;
    }
  }

  @Override
  public void paintComponent(final Graphics _g) {
  }

  @Override
  public void setActive(final boolean _b) {
    target_.requestFocus();
    target_.requestFocusInWindow();
    super.setActive(_b);
  }
}
