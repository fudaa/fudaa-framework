package org.fudaa.ebli.courbe;

import java.awt.Color;

public class EGCourbeSurfacePersist {
  
  Color downColor;
  Color upColor;
  int alpha;
  int surfaceType;
  Double palier;
  Integer courbeId;
  String minType;
  int minIdx;
  int maxIdx;
  String maxType;

  public EGCourbeSurfacePersist(){

  }

  public EGCourbeSurfacePersist(EGCourbeSurfacePersist surfacePainter) {
    if(surfacePainter!=null){
      downColor=surfacePainter.downColor;
      upColor=surfacePainter.upColor;
      alpha=surfacePainter.alpha;
      surfaceType=surfacePainter.surfaceType;
      palier=surfacePainter.palier;
      courbeId=surfacePainter.courbeId;
      minType=surfacePainter.minType;
      minIdx=surfacePainter.minIdx;
      maxIdx=surfacePainter.maxIdx;
      maxType=surfacePainter.maxType;

    }
  }
}
