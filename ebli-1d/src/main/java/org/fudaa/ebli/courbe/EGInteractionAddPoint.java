/*
 * @creation 23 juin 2004
 * @modification $Date: 2007-05-22 14:19:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGInteractionDeplacementPoint.java,v 1.17 2007-05-22 14:19:04 deniger Exp $
 */
public class EGInteractionAddPoint extends EGInteractiveComponent implements MouseListener {

  private final EGGraphe target_;
  EGCourbe compModifie_;

  @Override
  public String getDescription() {
    return EbliLib.getS("Ajouter des points");
  }

  /**
   * @param _a la cible pour les modifications
   */
  public EGInteractionAddPoint(final EGFillePanel _a) {
    target_ = _a.getGraphe();
    target_.addMouseListener(this);
  }

  @Override
  protected void paintComponent(final Graphics _g) {
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
  }

  private boolean isCompSelectModifiable() {
    return (compModifie_ != null) && compModifie_.isVisible_
            && (compModifie_.getModel().isModifiable());
  }

  public void mouseDragged(final MouseEvent _e) {
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
  }

  public void mouseMoved(final MouseEvent _e) {
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
    compModifie_ = target_.getSelectedComponent();
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (!isActive() || !isCompSelectModifiable()) {
      return;
    }
    if (EbliLib.isPopupMouseEvent(_e)) {
      return;
    }
    compModifie_ = target_.getSelectedComponent();
    double x = target_.getTransformer().getXReel(_e.getX());
    double y = target_.getTransformer().getYReel(_e.getY(), compModifie_.getAxeY());
    compModifie_.getModel().addValue(x, y, target_.getCmd());
  }

  @Override
  public void setActive(final boolean _b) {
    super.setActive(_b);
    if (!_b) {
      compModifie_ = null;
    }
  }
}