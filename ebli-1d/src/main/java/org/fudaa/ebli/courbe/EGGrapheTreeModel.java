/*
 * @creation 2 sept. 2004
 *
 * @modification $Date: 2007-05-04 13:49:41 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuComboBox;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluTreeComboboxModel;
import org.fudaa.ctulu.gui.CtuluTreeComboboxRenderer;
import org.fudaa.ebli.commun.EbliActionInterface;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.tree.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: EGGrapheTreeModel.java,v 1.14 2007-05-04 13:49:41 deniger Exp $
 */
public class EGGrapheTreeModel extends DefaultTreeModel implements EGGrapheModel, EGParent.Tree {
  @Override
  public boolean isDisplayed() {
    return true;
  }

  @Override
  public EGGrapheModel getMainModel() {
    return this;
  }

  private class CommandRemoveCurves implements CtuluCommand {
    final Map courbeGroup_;

    /**
     * @param _l tableau courbes->Group
     */
    public CommandRemoveCurves(final Map _l) {
      courbeGroup_ = _l;
    }

    @Override
    public void redo() {
      for (final Iterator it = courbeGroup_.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        ((EGGroup) e.getValue()).removeEGComponent((EGCourbeChild) e.getKey());
      }
      getSelectionModel().clearSelection();
      structureChangedChangedForValues(courbeGroup_);
      expandAndSelectPathValues(courbeGroup_);
    }

    @Override
    public void undo() {
      for (final Iterator it = courbeGroup_.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        ((EGGroup) e.getValue()).addEGComponent((EGCourbeChild) e.getKey(), -1, false);
      }
      structureChangedChangedForValues(courbeGroup_);
      expandAndSelectPathKey(courbeGroup_);
    }
  }

  public class CommandAddCourbe implements CtuluCommand {
    final EGCourbeChild c_;
    final int idx_;
    final EGGroup parentGroup_;

    public CommandAddCourbe(final EGCourbeChild _c) {
      parentGroup_ = _c.getParentGroup();
      c_ = _c;
      idx_ = parentGroup_.getIndex(_c);
    }

    @Override
    public void redo() {
      parentGroup_.addEGComponent(c_, idx_, false);
      nodesWereInserted(parentGroup_, new int[]{parentGroup_.getChildCount() - 1});
      // fullRepaint();
      selectPath(c_);
    }

    @Override
    public void undo() {
      final int i = parentGroup_.getIndex(c_);
      parentGroup_.removeEGComponent(c_);
      nodesWereRemoved(parentGroup_, new int[]{i}, new Object[]{c_});
      // fullRepaint();
      selectPath(parentGroup_);
    }
  }

  public class CommandAddCourbes implements CtuluCommand {
    final EGCourbeChild[] c_;
    final EGGroup parentGroup_;

    public CommandAddCourbes(final EGCourbeChild[] _c) {
      parentGroup_ = _c[0].getParentGroup();
      c_ = _c;
    }

    @Override
    public void redo() {
      for (int i = 0; i < c_.length; i++) {
        parentGroup_.addEGComponent(c_[i], -1, false);
      }
      nodeStructureChanged(parentGroup_);
      // fullRepaint();
      expandAndSelectPath(Arrays.asList(c_));
    }

    @Override
    public void undo() {
      for (int i = 0; i < c_.length; i++) {
        parentGroup_.removeEGComponent(c_[i]);
      }
      nodeStructureChanged(parentGroup_);
      // fullRepaint();
      selectPath(parentGroup_);
    }
  }

  class CommandCourbeCopie implements CtuluCommand {
    final List curveCopied_;
    final List curveToCopy_;
    final EGGroup groupDest_;

    public CommandCourbeCopie(final EGGroup _dest, final List _courbeToCopy, final List _curveCopied) {
      groupDest_ = _dest;
      curveCopied_ = _curveCopied;
      curveToCopy_ = _courbeToCopy;
    }

    @Override
    public void redo() {
      groupDest_.addEGComponent(curveCopied_);
      nodeStructureChanged(groupDest_);
      expandAndSelectPath(curveCopied_);
      // fullRepaint();
    }

    @Override
    public void undo() {
      groupDest_.removeEGComponent(curveCopied_);
      nodeStructureChanged(groupDest_);
      expandAndSelectPath(curveToCopy_);
      // fullRepaint();
    }
  }

  class CommandCourbeMove implements CtuluCommand {
    final Map curveOldGroup_;
    final EGGroup groupDest_;

    public CommandCourbeMove(final EGGroup _dest, final Map _curveOldGroup) {
      groupDest_ = _dest;
      curveOldGroup_ = _curveOldGroup;
    }

    @Override
    public void redo() {
      moveCourbe(new ArrayList(curveOldGroup_.keySet()), groupDest_);
      nodeStructureChanged(groupDest_);
      structureChangedChangedForValues(curveOldGroup_);
      expandAndSelectPathKey(curveOldGroup_);
      // fullRepaint();
    }

    @Override
    public void undo() {
      EGCourbeChild lastCourbe;
      for (final Iterator it = curveOldGroup_.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry e = (Map.Entry) it.next();
        lastCourbe = (EGCourbeChild) e.getKey();
        ((EGGroup) e.getValue()).addEGComponent(lastCourbe, -1, false);
      }
      nodeStructureChanged(groupDest_);
      structureChangedChangedForValues(curveOldGroup_);
      expandAndSelectPathKey(curveOldGroup_);
      // fullRepaint();
    }
  }

  public static class GrapheTreeNode implements Icon, EGParent.TreeNodes {
    public final List components_ = new ArrayList();
    EGParent.Tree dispatcher_;

    public GrapheTreeNode() {
      super();
    }

    @Override
    public EGGrapheModel getMainModel() {
      return dispatcher_.getMainModel();
    }

    protected EGParent getDispatcher() {
      return dispatcher_;
    }

    public EGGroup getGroup(final int _i) {
      if (components_.size() > _i) {
        return (EGGroup) components_.get(_i);
      } else {
        return null;
      }
    }

    @Override
    public boolean isDisplayed() {
      return dispatcher_!=null && dispatcher_.isDisplayed();
    }

    protected void setDispatcher(final EGParent.Tree _dispatcher) {
      dispatcher_ = _dispatcher;
    }

    @Override
    public Enumeration children() {
      return Collections.enumeration(components_);
    }

    public boolean contains(final EGCourbe _c) {
      for (int i = components_.size() - 1; i >= 0; i--) {
        if (getGroup(i).contains(_c)) {
          return true;
        }
      }
      return false;
    }

    @Override
    public void descendre(final EGObject _o) {
      dispatcher_.descendre(_o);
    }

    @Override
    public boolean canDescendre(final EGObject _o) {
      return dispatcher_.canDescendre(_o);
    }

    @Override
    public boolean canMonter(final EGObject _o) {
      return dispatcher_.canMonter(_o);
    }

    @Override
    public void enDernier(final EGObject _o) {
      dispatcher_.enDernier(_o);
    }

    @Override
    public void enPremier(final EGObject _o) {
      dispatcher_.enPremier(_o);
    }

    @Override
    public void fireAxeAspectChanged(final EGAxe _a) {
      dispatcher_.fireAxeAspectChanged(_a);
    }

    @Override
    public void fireCourbeAspectChanged(final EGObject _o, final boolean _visibility) {
      dispatcher_.fireCourbeAspectChanged(_o, _visibility);
    }

    public void fireCourbeContentChanged(final EGObject _o) {
      fireCourbeContentChanged(_o, false);
    }

    @Override
    public void fireCourbeContentChanged(final EGObject _o, final boolean _mustRestore) {
      dispatcher_.fireCourbeContentChanged(_o, _mustRestore);
    }

    @Override
    public void fireStructureChanged(final EGGroup _o, final EGCourbeChild _moved) {
      dispatcher_.fireStructureChanged(_o, _moved);
    }

    @Override
    public boolean getAllowsChildren() {
      return true;
    }

    @Override
    public EGAxeHorizontal getAxeX() {
      return dispatcher_.getAxeX();
    }

    @Override
    public TreeNode getChildAt(final int _childIndex) {
      return (TreeNode) components_.get(_childIndex);
    }

    @Override
    public int getChildCount() {
      return components_ == null ? 0 : components_.size();
    }

    /**
     * @return les courbes contenues par ce model
     */
    public EGCourbe[] getCourbes() {
      final List r = new ArrayList();
      final int nb = components_.size();
      for (int i = 0; i < nb; i++) {
        getGroup(i).fillWithCurves(r);
      }
      final EGCourbe[] rf = new EGCourbe[r.size()];
      r.toArray(rf);
      return rf;
    }

    public List<EGCourbeChild> getAllCourbesChild() {
      final List r = new ArrayList();
      final int nb = components_.size();
      for (int i = 0; i < nb; i++) {
        getGroup(i).fillWithCurves(r);
      }

      return r;
    }

    @Override
    public int getIconHeight() {
      return 24;
    }

    @Override
    public int getIconWidth() {
      return 24;
    }

    @Override
    public int getIndex(final TreeNode _node) {
      return components_.indexOf(_node);
    }

    public boolean isEmpty() {
      for (final Iterator it = components_.iterator(); it.hasNext(); ) {
        if (((TreeNode) it.next()).getChildCount() >= 0) {
          return false;
        }
      }
      return true;
    }

    public int getNbCourbes() {
      int r = 0;

      for (final Iterator it = components_.iterator(); it.hasNext(); ) {
        r += ((TreeNode) it.next()).getChildCount();
      }
      return r;
    }

    @Override
    public TreeNode getParent() {
      return null;
    }

    @Override
    public boolean isLeaf() {
      return false;
    }

    @Override
    public void monter(final EGObject _o) {
      dispatcher_.monter(_o);
    }

    @Override
    public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    }
  }

  class GroupComboboxModel extends AbstractListModel implements ComboBoxModel {
    Object selected_;

    @Override
    public Object getElementAt(final int _index) {
      return getGrapheTreeNode().getChildAt(_index);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return getGrapheTreeNode().getChildCount();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  public class CommandAddCourbesMulti implements CtuluCommand {
    final EGCourbeChild[] c_;
    final EGGroup[] parentGroup_;

    public CommandAddCourbesMulti(final EGCourbeChild[] _c, final EGGroup[] _parentGroup) {
      parentGroup_ = _parentGroup;
      c_ = _c;
    }

    @Override
    public void redo() {
      for (int i = 0; i < c_.length; i++) {
        parentGroup_[i].addEGComponent(c_[i], -1, false);
      }
      fireStructureChanged();
      expandAndSelectPath(Arrays.asList(c_));
    }

    @Override
    public void undo() {
      for (int i = 0; i < c_.length; i++) {
        parentGroup_[i].removeEGComponent(c_[i]);
      }
      fireStructureChanged();
      // fullRepaint();
      expandAndSelectPath(Arrays.asList(parentGroup_));
    }
  }

  public final static void fireAxeAspectChanged(final EGAxe _c, final EventListenerList _listenerList) {
    // Guaranteed to return a non-null array
    final Object[] listeners = _listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == EGGrapheModelListener.class) {
        // Lazily create the event:
        ((EGGrapheModelListener) listeners[i + 1]).axeAspectChanged(_c);
      }
    }
  }

  public final static void fireAxeContentChanged(final EGAxe _c, final EventListenerList _listenerList) {
    // Guaranteed to return a non-null array
    final Object[] listeners = _listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == EGGrapheModelListener.class) {
        // Lazily create the event:
        ((EGGrapheModelListener) listeners[i + 1]).axeContentChanged(_c);
      }
    }
  }

  public final static void fireCourbeAspectChanged(final EGObject _c, final boolean _visible, final EventListenerList _listenerList) {
    // Guaranteed to return a non-null array
    final Object[] listeners = _listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == EGGrapheModelListener.class) {
        // Lazily create the event:
        ((EGGrapheModelListener) listeners[i + 1]).courbeAspectChanged(_c, _visible);
      }
    }
  }

  public final static void fireCourbeContentChanged(final EGObject _c, final EventListenerList _listenerList, final boolean _mustRestore) {
    // Guaranteed to return a non-null array
    final Object[] listeners = _listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == EGGrapheModelListener.class) {
        // Lazily create the event:
        ((EGGrapheModelListener) listeners[i + 1]).courbeContentChanged(_c, _mustRestore);
      }
    }
  }

  public final static void fireStructureChanged(final EventListenerList _listenerList) {
    // Guaranteed to return a non-null array
    final Object[] listeners = _listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == EGGrapheModelListener.class) {
        // Lazily create the event:
        ((EGGrapheModelListener) listeners[i + 1]).structureChanged();
      }
    }
  }

  EGAxeHorizontal axeX_;
  final TreeSelectionModel selectionModel_;

  public EGGrapheTreeModel() {
    super(new GrapheTreeNode());
    selectionModel_ = new DefaultTreeSelectionModel();
    getGrapheTreeNode().setDispatcher(this);
  }

  private List getSelectedCourbe(final EGTree _t) {
    final TreePath[] p = _t.getSelectionPaths();
    if (p == null) {
      return Collections.EMPTY_LIST;
    }
    final List c = new ArrayList(p.length);
    for (int i = p.length - 1; i >= 0; i--) {
      final Object o = p[i].getLastPathComponent();
      if (o instanceof EGCourbe) {
        c.add(o);
      }
    }
    return c;
  }

  private void groupeMoved(final EGObject _o) {
    final TreeNode n = (TreeNode) _o;
    fireStructureChanged();
    selectionModel_.setSelectionPath(new TreePath(getPathToRoot(n)));
  }

  void expandAndSelectPath(final List _nodes) {
    final TreePath[] ns = new TreePath[_nodes.size()];
    int idx = 0;
    for (final Iterator it = _nodes.iterator(); it.hasNext(); ) {
      ns[idx++] = new TreePath(getPathToRoot((TreeNode) it.next()));
    }
    selectionModel_.setSelectionPaths(ns);
  }

  void expandAndSelectPathKey(final Map _nodes) {
    final TreePath[] ns = new TreePath[_nodes.size()];
    int idx = 0;
    for (final Iterator it = _nodes.keySet().iterator(); it.hasNext(); ) {
      ns[idx++] = new TreePath(getPathToRoot((TreeNode) it.next()));
    }
    selectionModel_.setSelectionPaths(ns);
  }

  void expandAndSelectPathValues(final Map _nodes) {
    final TreePath[] ns = new TreePath[_nodes.size()];
    int idx = 0;
    for (final Iterator it = _nodes.values().iterator(); it.hasNext(); ) {
      ns[idx++] = new TreePath(getPathToRoot((TreeNode) it.next()));
    }
    selectionModel_.setSelectionPaths(ns);
  }

  protected List copyCourbe(final List _l, final EGGroup _dest) {
    return null;
  }

  protected CtuluCommand createCommandForAdd(final EGCourbeChild _c) {
    return new CommandAddCourbe(_c);
  }

  protected final void curvesAdded(final EGCourbeChild _c, final CtuluCommandContainer _mng) {
    if (_c == null) {
      return;
    }
    nodesWereInserted(_c.getParentGroup(), new int[]{_c.getParentGroup().getChildCount() - 1});
    // fullRepaint();
    selectPath(_c);
    if (_mng != null) {
      _mng.addCmd(new CommandAddCourbe(_c));
    }
  }

  /**
   * @param _c les courbes ajoute
   * @param _g le groupe de dest
   */
  protected final void curvesAdded(final EGCourbeChild[] _c, final EGGroup _g, final CtuluCommandContainer _mng) {
    if ((_c == null) || (_c.length == 0)) {
      return;
    }
    nodeStructureChanged(_g);
    // fullRepaint();
    selectPath(_g);
    if (_mng != null) {
      _mng.addCmd(new CommandAddCourbes(_c));
    }
  }

  protected EGCourbeChild duplicateCourbe(final EGCourbeChild _c) {
    return null;
  }

  @Override
  protected final void fireTreeNodesChanged(final Object _source, final Object[] _path, final int[] _childIndices, final Object[] _children) {
    super.fireTreeNodesChanged(_source, _path, _childIndices, _children);
  }

  @Override
  protected final void fireTreeNodesInserted(final Object _source, final Object[] _path, final int[] _childIndices, final Object[] _children) {
    super.fireTreeNodesInserted(_source, _path, _childIndices, _children);
    fireStructureChanged(listenerList);
  }

  @Override
  protected final void fireTreeNodesRemoved(final Object _source, final Object[] _path, final int[] _childIndices, final Object[] _children) {
    super.fireTreeNodesRemoved(_source, _path, _childIndices, _children);
    fireStructureChanged(listenerList);
  }

  @Override
  protected final void fireTreeStructureChanged(final Object _source, final Object[] _path, final int[] _childIndices, final Object[] _children) {
    super.fireTreeStructureChanged(_source, _path, _childIndices, _children);
    fireStructureChanged(listenerList);
  }

  protected GrapheTreeNode getGrapheTreeNode() {
    return (GrapheTreeNode) root;
  }

  protected EGGroup getGroup(final int _i) {
    return getGrapheTreeNode().getGroup(_i);
  }

  protected final void internFireCourbeChanged(final EGObject _o) {
    if (_o == null) {
      fireTreeNodesChanged(this, getPathToRoot(root), null, null);
    }
    if (_o instanceof TreeNode) {
      fireTreeNodesChanged(this, getPathToRoot((TreeNode) _o), null, null);
    }
  }

  /**
   * @param _l
   * @param _dest
   * @return courbe->AncienParent
   */
  protected Map moveCourbe(final List _l, final EGGroup _dest) {
    return null;
  }

  protected void removeAllGroups() {
    getGrapheTreeNode().components_.clear();
  }

  protected void removeAllTotal() {
    final GrapheTreeNode n = getGrapheTreeNode();
    for (int i = n.components_.size() - 1; i >= 0; i--) {
      getGroup(i).removeAll();
    }
    n.components_.clear();
  }

  protected void selectPath(final TreeNode _n) {
    selectionModel_.setSelectionPath(new TreePath(getPathToRoot(_n)));
  }

  protected void structureChangedChangedForValues(final Map _l) {
    for (final Iterator it = _l.values().iterator(); it.hasNext(); ) {
      nodeStructureChanged((TreeNode) it.next());
    }
  }

  /**
   * n'envoie pas d'evt.
   *
   * @param _g le groupe a ajouter
   */
  public void add(final EGGroup _g) {
    final GrapheTreeNode n = getGrapheTreeNode();
    _g.setParent(n);
    n.components_.add(_g);
  }

  protected void remove(final EGGroup _g) {
    final GrapheTreeNode n = getGrapheTreeNode();
    _g.setParent(n);
    if (n.components_.remove(_g)) {
      _g.setParent(null);
      fireTreeNodesRemoved(this, getPathToRoot(_g), null, null);
      fireStructureChanged();
    }
  }

  @Override
  public void addModelListener(final EGGrapheModelListener _l) {
    listenerList.add(EGGrapheModelListener.class, _l);
  }

  @Override
  public void addNewCourbe(final CtuluCommandManager _cmd, final Component _c, final EGGraphe _graphe) {
  }

  @Override
  public void addSelectionListener(final EGSelectionListener _l) {
    selectionModel_.addTreeSelectionListener(_l);
  }

  @Override
  public boolean canAddCourbe() {
    return false;
  }

  @Override
  public boolean canDeleteCourbes() {
    return isStructureModifiable();
  }

  @Override
  public boolean contains(final EGCourbe _c) {
    return getGrapheTreeNode().contains(_c);
  }

  /**
   * @param _g le groupe a tester
   * @return true si le groupe est deja present
   */
  public boolean contains(final EGGroup _g) {
    return getGrapheTreeNode().components_.contains(_g);
  }

  @Override
  public void decoreAddButton(final EbliActionInterface _b) {
  }

  protected void sortGroups() {
    final List r = getGrapheTreeNode().components_;
    Collections.sort(r, new EGObjectComparator());
    fireStructureChanged();
  }

  @Override
  public void descendre(final EGObject _o) {
    final List r = getGrapheTreeNode().components_;
    final int i = r.indexOf(_o);
    if (i >= 0 && i < r.size() - 1) {
      r.remove(i);
      if (i == r.size() - 1) {
        r.add(_o);
      } else {
        r.add(i + 1, _o);
      }
      groupeMoved(_o);
    }
  }

  public void dragCopyAction(final EGTree _t, final EGGroup _dest, final CtuluCommandManager _mng) {
    if (!isStructureModifiable()) {
      return;
    }
    final List c = getSelectedCourbe(_t);
    if (!c.isEmpty()) {
      final List l = copyCourbe(c, _dest);
      if (l != null) {
        // fullRepaint();
        nodeStructureChanged(_dest);
        if (_mng != null) {
          _mng.addCmd(new CommandCourbeCopie(_dest, c, l));
        }
        expandAndSelectPath(l);
      }
    }
  }

  public void dragMoveAction(final EGTree _t, final EGGroup _dest, final CtuluCommandManager _cmd) {
    if (!isStructureModifiable()) {
      return;
    }
    final List c = getSelectedCourbe(_t);
    if (!c.isEmpty()) {
      final Map l = moveCourbe(c, _dest);
      if (l != null) {
        nodeStructureChanged(_dest);
        structureChangedChangedForValues(l);
        expandAndSelectPathKey(l);
        if (_cmd != null) {
          _cmd.addCmd(new CommandCourbeMove(_dest, l));
        }
      }
    }
  }

  @Override
  public void duplicateCourbe(final CtuluCommandManager _mng, final EGCourbe _c) {
    if (!isStructureModifiable()) {
      return;
    }
    final EGCourbeChild child = (EGCourbeChild) _c;
    final EGGroup g = child.getParentGroup();
    final int i = g.getIndex(child);
    if (i >= 0) {
      final EGCourbeChild cDuplicate = duplicateCourbe(child);
      if (cDuplicate != null) {
        g.addEGComponent(cDuplicate, i + 1, false);
        curvesAdded(cDuplicate, _mng);
      }
    }
  }

  @Override
  public void duplicateCourbeForSrc(final CtuluCommandManager _mng, final EGCourbe _c) {

    final EGCourbeChild child = (EGCourbeChild) _c;
    final EGGroup g = child.getParentGroup();
    final int i = g.getIndex(child);
    if (i >= 0) {
      final EGCourbeChild cDuplicate = duplicateWithChooseSrc(child);
      if (cDuplicate != null) {
        g.addEGComponent(cDuplicate, -1, false);
        // g.addEGComponent(cDuplicate, i + 1);
        curvesAdded(cDuplicate, _mng);
      }
    }
  }

  /**
   * methode qui demande a l user de choisir sa courbe puis genere une copie avec la source en cours.
   *
   * @param _child
   */
  protected EGCourbeChild duplicateWithChooseSrc(EGCourbeChild _child) {

    return null;
  }

  @Override
  public void enDernier(final EGObject _o) {
    final List r = getGrapheTreeNode().components_;
    final int i = r.indexOf(_o);
    if (i >= 0 && i < r.size() - 1) {
      r.remove(i);
      r.add(_o);
      groupeMoved(_o);
    }
  }

  @Override
  public void enPremier(final EGObject _o) {
    final List r = getGrapheTreeNode().components_;
    final int i = r.indexOf(_o);
    if (i > 0) {
      r.remove(i);
      r.add(0, _o);
      groupeMoved(_o);
    }
  }

  @Override
  public final void fireAxeAspectChanged(final EGAxe _a) {
    fireAxeAspectChanged(_a, listenerList);
  }

  @Override
  public final void fireAxeContentChanged(final EGAxe _a) {
    fireAxeContentChanged(_a, listenerList);
  }

  @Override
  public final void fireCourbeAspectChanged(final EGObject _o, final boolean _visibility) {
    internFireCourbeChanged(_o);
    fireCourbeAspectChanged(_o, _visibility, listenerList);
  }

  protected boolean blockEvents_;

  @Override
  public final void fireCourbeContentChanged(final EGObject _o, final boolean _mustRestore) {
    if (blockEvents_) {
      return;
    }
    internFireCourbeChanged(_o);
    fireCourbeContentChanged(_o, listenerList, _mustRestore);
  }

  public final void fireCourbeContentChanged(final EGObject _o) {
    fireCourbeContentChanged(_o, false);
  }

  @Override
  public final void fireStructureChanged() {
    super.fireTreeStructureChanged(this, getPathToRoot(root), null, null);
    fireStructureChanged(listenerList);
  }

  @Override
  public void fireStructureChanged(final EGGroup _o, final EGCourbeChild _moved) {
    final int[] idx = new int[_o.getChildCount()];
    final Object[] childs = new Object[idx.length];
    for (int i = idx.length - 1; i >= 0; i--) {
      idx[i] = i;
      childs[i] = _o.getChildAt(i);
    }
    nodeStructureChanged(_o);
    this.fireTreeStructureChanged(this, getPathToRoot(_o), idx, childs);
    selectionModel_.setSelectionPath(new TreePath(getPathToRoot(_moved)));
  }

  @Override
  public EGAxeHorizontal getAxeX() {
    return axeX_;
  }

  @Override
  public EGCourbe[] getCourbes() {
    return getGrapheTreeNode().getCourbes();
  }

  public List<EGCourbeChild> getAllCourbesChild() {
    return getGrapheTreeNode().getAllCourbesChild();
  }

  @Override
  public EGObject getEGObject(final int _i) {
    return (EGObject) root.getChildAt(_i);
  }

  public ComboBoxModel getGroupComboboxModel() {
    return new GroupComboboxModel();
  }

  public int getNbCourbes() {
    return getGrapheTreeNode().getNbCourbes();
  }

  public boolean isEmpty() {
    return getGrapheTreeNode().isEmpty();
  }

  @Override
  public int getNbEGObject() {
    return root.getChildCount();
  }

  @Override
  public int getNbSelectedObjects() {
    return selectionModel_.getSelectionCount();
  }

  @Override
  public EGCourbe getSelectedComponent() {
    if (selectionModel_.getSelectionCount() == 0) {
      return null;
    }
    final TreePath p = selectionModel_.getSelectionPath();
    if (p != null) {
      final Object o = p.getLastPathComponent();
      if (o instanceof EGCourbeChild) {
        return (EGCourbeChild) o;
      }
    }
    return null;
  }

  @Override
  public EGObject getSelectedObject() {
    final TreePath p = selectionModel_.getSelectionPath();
    if (p != null) {
      return (EGObject) p.getLastPathComponent();
    }
    return null;
  }

  @Override
  public EGObject[] getSelectedObjects() {
    if (selectionModel_.isSelectionEmpty()) {
      return null;
    }
    final TreePath[] ps = selectionModel_.getSelectionPaths();
    final List r = new ArrayList();
    for (int i = 0; i < ps.length; i++) {
      r.add(ps[i].getLastPathComponent());
    }
    if (r.isEmpty()) {
      return null;
    }
    final EGObject[] rf = new EGObject[r.size()];
    r.toArray(rf);
    return rf;
  }

  public TreeSelectionModel getSelectionModel() {
    return selectionModel_;
  }

  @Override
  public void setSelectedComponent(final EGCourbe _c) {
    getSelectionModel().setSelectionPath(new TreePath(getPathToRoot((EGCourbeChild) _c)));
  }

  @Override
  public boolean isContentModifiable() {
    return true;
  }

  @Override
  public boolean isSomethingToDisplay() {
    for (int i = getNbEGObject() - 1; i >= 0; i--) {
      if (getEGObject(i).isSomethingToDisplay()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isStructureModifiable() {
    return true;
  }

  @Override
  public void monter(final EGObject _o) {
    final List r = getGrapheTreeNode().components_;
    final int i = r.indexOf(_o);
    if (i > 0) {
      r.remove(i);
      r.add(i - 1, _o);
      groupeMoved(_o);
    }
  }

  boolean isUpdating;

  @Override
  public boolean canDescendre(final EGObject _o) {
    final List r = getGrapheTreeNode().components_;
    if (r.size() <= 1) {
      return false;
    }
    final int i = r.indexOf(_o);
    return i >= 0 && i < r.size() - 1;
  }

  @Override
  public boolean canMonter(final EGObject _o) {
    final List r = getGrapheTreeNode().components_;
    if (r.size() <= 1) {
      return false;
    }
    return r.indexOf(_o) > 0;
  }

  public void removeCurves(final EGCourbeChild[] _cs, final CtuluCommandContainer _mng) {
    if (!canDeleteCourbes()) {
      return;
    }
    final Map l = new HashMap(_cs.length);
    for (int i = _cs.length - 1; i >= 0; i--) {
      if (_cs[i].getModel().isRemovable()) {
        final EGGroup parent = _cs[i].getParentGroup();
        l.put(_cs[i], parent);
        parent.removeEGComponent(_cs[i]);
      }
    }
    if (l.size() == 0) {
      return;
    }
    structureChangedChangedForValues(l);
    expandAndSelectPathValues(l);
    if (_mng != null) {
      _mng.addCmd(new CommandRemoveCurves(l));
    }
  }

  @Override
  public void removeModelListener(final EGGrapheModelListener _l) {
    listenerList.remove(EGGrapheModelListener.class, _l);
  }

  /**
   * Supprime les courbes selectionnees.
   */
  @Override
  public void removeSelectedCurves(final CtuluCommandManager _mng) {
    if (!canDeleteCourbes()) {
      return;
    }
    final EGCourbeChild s = (EGCourbeChild) getSelectedComponent();
    if (s == null) {
      return;
    }
    final TreePath[] p = getSelectionModel().getSelectionPaths();
    if (p == null) {
      return;
    }
    final List l = new ArrayList(p.length);
    for (int i = p.length - 1; i >= 0; i--) {
      final Object o = p[i].getLastPathComponent();
      if (o instanceof EGCourbeChild) {
        l.add(o);
      }
    }
    if (l.size() > 0) {
      getSelectionModel().clearSelection();
      removeCurves((EGCourbeChild[]) l.toArray(new EGCourbeChild[0]), _mng);
    }
  }

  @Override
  public void removeSelectionListener(final EGSelectionListener _l) {
    selectionModel_.removeTreeSelectionListener(_l);
  }

  @Override
  public void setAxeX(final EGAxeHorizontal _h) {
    axeX_ = _h;
  }

  @Override
  public JComboBox createCbForSelectCourbe(final EGAxeVertical _vert) {
    final CtuluTreeComboboxModel model = new CtuluTreeComboboxModel(this) {
      @Override
      public boolean isSelectable(Object _o) {
        return _o != null && ((EGObject) _o).isCourbe() && (_vert == null || _vert == ((EGCourbe) _o).getAxeY());
      }
    };
    final BuComboBox cb = new BuComboBox(model);
    cb.setRenderer(new CtuluTreeComboboxRenderer(this, new EGTreeCellRenderer.SimpleCellRender(_vert)));
    return cb;
  }

  @Override
  public JComboBox createCbForSelectCourbe() {
    return createCbForSelectCourbe(null);
  }

  /**
   * Trier les groupes de courbes.
   */
  public void sortAllGroup() {
    for (int i = getNbEGObject(); i >= 0; i--) {
      getGroup(i).sortMembers();
    }
  }

  @Override
  public void valueForPathChanged(final TreePath _path, final Object _newValue) {
    /*
     * MutableTreeNode aNode = (MutableTreeNode)path.getLastPathComponent(); aNode.setUserObject(newValue);
     * nodeChanged(aNode);
     */
    if (_path == null) {
      return;
    }
    internFireCourbeChanged((EGObject) _path.getLastPathComponent());
  }

  // TODO il faut dupliquer tous les groupes
  @Override
  public EGGrapheModel duplicate(EGGrapheDuplicator _duplicator) {
    EGGrapheTreeModel duplic = new EGGrapheTreeModel();
    duplic.axeX_ = this.axeX_.duplicate();
    duplic.selectionModel_.setSelectionMode(this.selectionModel_.getSelectionMode());

    GrapheTreeNode root = this.getGrapheTreeNode();
    GrapheTreeNode rootDuplique = duplic.getGrapheTreeNode();

    for (int i = 0; i < root.components_.size(); i++) {

      // -- ajout du groupe duplique --//
      duplic.add(root.getGroup(i).duplicate(_duplicator));
    }

    return duplic;
  }

  @Override
  public Object getSpecificPersitDatas(Map Params) {
    return "mod�le g�n�rique";
  }

  @Override
  public void setSpecificPersitDatas(Object sepcPersitData, Map Params) {
  }

  @Override
  public List<EbliActionInterface> getSpecificsActionsCurvesOnly(EGGraphe _target, CtuluUI ui) {
    return null;
  }

  /**
   * Methode qui est appelee apres avoir charg� un model de graphe persistant. Doit etre surcharg� a plus haut niveau pour effectuer des actions de
   * finalisation specifiques.
   */
  public void finalizePersistance() {
  }

  /**
   * @return the isUpdating
   */
  @Override
  public boolean isUpdating() {
    return isUpdating;
  }

  /**
   * @param isUpdating the isUpdating to set
   */
  public void setUpdating(boolean isUpdating) {
    this.isUpdating = isUpdating;
  }
}
