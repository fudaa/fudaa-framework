/*
 *  @creation     10 f�vr. 2005
 *  @modification $Date: 2007-06-14 12:00:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluRunnable;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserWriter;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.table.CtuluWriter;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGExporter.java,v 1.19 2007-06-14 12:00:22 deniger Exp $
 */
public class EGExporter extends CtuluDialogPanel implements BuBorders, ItemListener, ListSelectionListener {

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (courbeList_.isSelectionEmpty()) {
      setErrorText(EbliLib.getS("S�lectionner au moins une courbe"));
    } else {
      setErrorText(null);
    }
  }

  public static void startExport(final EGGraphe _target, final CtuluUI _impl, final List<CtuluWriter> _listWriter) {



    final CtuluFileChooserWriter ch = new CtuluFileChooserWriter(_impl, _listWriter);
    final Frame f = CtuluLibSwing.getFrameAncestorHelper(_impl.getParentComponent());
    final File destFile = ch.getDestFile();
    if (destFile == null) {
      return;
    }
    final EGExporter r = new EGExporter(_target);
    final int res = r.afficheModale(f, destFile.getName());
    if (CtuluDialogPanel.isOkResponse(res)) {
      new CtuluRunnable(CtuluResource.CTULU.getString("Ecriture fichier") + CtuluLibString.ESPACE + destFile.getName(),
              _impl) {
        @Override
        public boolean run(ProgressionInterface _proj) {
          final EGExportData d = EGExportData.createExportData(r.getSelectedCourbe(), _target, r.isSameH(), r
                  .isViewedOnlyExported(), false, _proj);
          if (d.getMaxCol() == 0 || d.getMaxRow() == 0) {
            CtuluLibDialog.showError(_impl.getParentComponent(), EbliLib.getS("Exporter de courbes"), EbliLib
                    .getS("Aucune donn�e � exporter"));
          }
          try {
            // -- recuperation du writer
            CtuluWriter writer = ch.getSelectedWriter();

            writer.setFile(destFile);
            writer.setModel(d);
            writer.write(_proj);


          } catch (final Exception _e) {
            FuLog.error(_e);
          }
          return true;
        }
      }.run();

    }
  }
  final BuCheckBox cbExportOnlyViewedH_;
  final BuCheckBox cbUseSameHValue_;
  final JList courbeList_;
  final JTable preview_;
  final EGGraphe target_;

  /**
   * @param _target le graphe cible.
   */
  public EGExporter(final EGGraphe _target) {
    super();
    target_ = _target;
    final EGCourbe[] c = _target.getModel().getCourbes();
    courbeList_ = new BuList();
    courbeList_.setCellRenderer(new CtuluCellTextRenderer() {
      @Override
      protected void setValue(final Object _value) {
        super.setIcon((Icon) _value);
        super.setText(((EGCourbe) _value).getTitle());
      }
    });
    courbeList_.setModel(new AbstractListModel() {
      @Override
      public Object getElementAt(final int _index) {
        return c[_index];
      }

      @Override
      public int getSize() {
        return c.length;
      }
    });
    courbeList_.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    selectDefaultCourbeToExport();
    courbeList_.getSelectionModel().addListSelectionListener(this);
    setLayout(new BuVerticalLayout(2, true, false));
    final BuPanel pnSelectedCurves = new BuPanel();
    pnSelectedCurves.add(new BuScrollPane(courbeList_));
    pnSelectedCurves.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(), EbliLib.getS("Courbes � exporter")), EMPTY1212));
    add(pnSelectedCurves);
    final BuPanel pnOptions = new BuPanel();
    pnOptions.setLayout(new BuVerticalLayout(2));
    pnOptions.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(), EbliLib.getS("Options")), EMPTY1212));
    add(pnOptions);
    cbExportOnlyViewedH_ = new BuCheckBox();
    cbExportOnlyViewedH_.setText(EbliLib
            .getS("Exporter uniquement les points compris entre les extr�mit�s courantes de l'axe horizontal"));
    final EGAxeHorizontal h = target_.getTransformer().getXAxe();
    cbExportOnlyViewedH_.setToolTipText(EbliLib.getS("De {0} � {1}", h.getStringAffiche(h.getMinimum()), h
            .getStringAffiche(h.getMaximum())));
    pnOptions.add(cbExportOnlyViewedH_);
    cbUseSameHValue_ = new BuCheckBox();
    cbUseSameHValue_.setText(EbliLib.getS("Utiliser les m�mes valeurs en abscisse pour toutes les courbes"));
    cbUseSameHValue_.setToolTipText("<html>"
            + EbliLib.getS("Si s�lectionn�, toutes les abscisses seront repr�sent�es dans la premi�re colonne."
            + "<br> Les ordonn�es inconnues seront interpol�es.") + "</html>");
    pnOptions.add(cbUseSameHValue_);
    final BuPanel pnPreview = new BuPanel();
    pnPreview.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(), EbliLib.getS("Exemple")), EMPTY1212));
    add(pnPreview);
    preview_ = new JTable();
    preview_.setModel(createTableModel());
    final BuScrollPane sp = new BuScrollPane(preview_);
    sp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    sp.setPreferredHeight(90);
    pnPreview.add(sp);
    cbUseSameHValue_.addItemListener(this);
    cbExportOnlyViewedH_.addItemListener(this);
  }

  private void selectDefaultCourbeToExport() {
    final EGObject[] o = target_.getModel().getSelectedObjects();
    if (o == null) {
      return;
    }
    final List r = new ArrayList(o.length);
    for (int i = 0; i < o.length; i++) {
      if (o[i] instanceof EGGroup) {
        final EGGroup g = (EGGroup) o[i];
        for (int j = 0; j < g.getChildCount(); j++) {
          r.add(g.getCourbe(j));
        }
      } else {
        r.add(o[i]);
      }
    }
    final ListSelectionModel m = courbeList_.getSelectionModel();
    final ListModel model = courbeList_.getModel();
    for (int i = model.getSize() - 1; i >= 0; i--) {
      final Object oi = model.getElementAt(i);
      if (CtuluLibArray.findObject(o, oi) >= 0) {
        m.addSelectionInterval(i, i);
      }
    }
  }

  public boolean isSameH() {
    return cbUseSameHValue_.isSelected();
  }

  public boolean isViewedOnlyExported() {
    return cbExportOnlyViewedH_.isSelected();
  }

  public EGCourbe[] getSelectedCourbe() {
    final Object[] o = courbeList_.getSelectedValues();
    final EGCourbe[] r = new EGCourbe[o.length];
    System.arraycopy(o, 0, r, 0, r.length);
    return r;
  }

  private TableModel createTableModel() {
    String[] column = null;
    String[][] values = null;
    final String crochet = "]";
    if (cbUseSameHValue_.isSelected()) {
      column = new String[]{target_.getTransformer().getXAxe().getTitre() + CtuluResource.CTULU.getString("Commun"),
        getStrOrdonnee() + CtuluLibString.ESPACE + EbliLib.getS(getStrCourbe(), CtuluLibString.UN),
        getStrOrdonnee() + CtuluLibString.ESPACE + EbliLib.getS(getStrCourbe(), CtuluLibString.DEUX)};
      values = new String[cbExportOnlyViewedH_.isSelected() ? 2 : 5][3];
      int ligne = 0;
      for (int i = 0; i < values.length; i++) {
        boolean add = true;
        if (cbExportOnlyViewedH_.isSelected() && (i == 0 || i == 5)) {
          add = false;
        }
        if (add) {
          values[ligne][0] = "X[" + (i + 1) + crochet;
          values[ligne][1] = "y1[" + (i + 1) + crochet;
          values[ligne][2] = "y2[" + (i + 1) + crochet;
          ligne++;
        }
      }
    } else {
      column = new String[]{
        target_.getTransformer().getXAxe().getTitre() + CtuluLibString.ESPACE + CtuluLibString.UN,
        getStrOrdonnee() + CtuluLibString.ESPACE + EbliLib.getS(getStrCourbe(), CtuluLibString.UN),
        target_.getTransformer().getXAxe().getTitre() + CtuluLibString.ESPACE + CtuluLibString.DEUX,
        getStrOrdonnee() + CtuluLibString.ESPACE + EbliLib.getS(getStrCourbe(), CtuluLibString.DEUX)};
      values = new String[cbExportOnlyViewedH_.isSelected() ? 2 : 5][column.length];
      int ligne = 0;
      for (int i = 0; i < values.length; i++) {
        boolean add = true;
        if (cbExportOnlyViewedH_.isSelected() && (i == 0 || i == 5)) {
          add = false;
        }
        if (add) {
          values[ligne][0] = "x1[" + (i + 1) + crochet;
          values[ligne][1] = "y1[" + (i + 1) + crochet;
          values[ligne][2] = "x2[" + (i + 1) + crochet;
          values[ligne][3] = "y2[" + (i + 1) + crochet;
          ligne++;
        }
      }
    }
    return new DefaultTableModel(values, column) {
      @Override
      public boolean isCellEditable(final int _row, final int _column) {
        return false;
      }
    };
  }

  private String getStrCourbe() {
    return "Courbe {0}";
  }

  private String getStrOrdonnee() {
    return CtuluResource.CTULU.getString("Ordonn�e");
  }

  /**
   * @return le graphe cible
   */
  public final EGGraphe getTarget() {
    return target_;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    preview_.setModel(createTableModel());
  }
}
