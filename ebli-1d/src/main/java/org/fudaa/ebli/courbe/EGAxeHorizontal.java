/*
 * @creation 21 juin 2004
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuLib;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorTime;
import org.fudaa.ctulu.iterator.DateIterator;
import org.fudaa.ctulu.iterator.NumberIntegerIterator;
import org.fudaa.ctulu.iterator.TickIterator;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Fred Deniger
 * @version $Id: EGAxeHorizontal.java,v 1.17 2007-05-04 13:49:41 deniger Exp $
 */
public class EGAxeHorizontal extends EGAxe {

  /**
   * Rien.
   */
  public EGAxeHorizontal() {
    super();
  }

  public EGAxeHorizontal(final boolean _isDiscret) {
    super.isDiscret_ = _isDiscret;
  }

  /**
   * @param _a l'axe servant pour l'initialisation
   */
  public EGAxeHorizontal(final EGAxe _a) {
    super(_a);
  }

  /**
   * @param _v l'axe horizontal servant pour l'initialisation
   */
  public EGAxeHorizontal(final EGAxeHorizontal _v) {
    super(_v);

    if (_v != null) {
      isDiscret_ = _v.isDiscret_;
    }

  }

  protected void afficheGraduationsEtGrille(final Graphics2D _g, final EGRepere _f) {
    // NumberFormat nf = isIntegerStep_ ? null : getNumberFormat();

    final FontMetrics fm = _g.getFontMetrics();
    final Color lightfg = getLightColor();
    // Color superlightfg = getSuperLightColor();
    final int y = _f.getMaxEcranY();
    final int yBasMajor = y + 6;
    final int yBas = y + 2;
    final int yMaxGrad = 0;// _f.getH();
    final TraceLigne traceGraduations = new TraceLigne(traceGraduations_);
    final TraceLigne traceSousGraduations = new TraceLigne(traceSousGraduations_);
    _g.setColor(lineColor_);

    String t;
    double lastMaxX = -10;
    final double ftHaut = fm.getMaxAscent() + 5;
    int idx = 0;
    final TickIterator iterator = buildUpToDateMainTickIterator();
    final ArrayList<Double> vals = new ArrayList<>();

    for (final TickIterator it = iterator; it.hasNext() && idx < 300; it.next()) {
      idx++;
      _g.setColor(lightfg);
      final double val = it.currentValue();
      final int xe = _f.getXEcran(val);

      vals.add(new Double(val));

      if (graduations_) {
        if (it.isMajorTick()) {
          if (specificFormat_ == null) {
            t = it.currentLabel();
          } else {
            t = specificFormat_.format(it.currentValue());
          }
          final double wordWidth = fm.stringWidth(t);
          final double xLeft = xe - wordWidth / 2;
          // pour eviter que les graduations se chevauchent
          if (xLeft > (lastMaxX + 2) && (xe + wordWidth / 2) < (_f.getMaxEcranX() + _f.getMargesDroite())) {
            if (traceGrille_) {
              traceGraduations.dessineTrait(_g, xe, y, xe, yMaxGrad);
            }
            _g.drawLine(xe, y, xe, yBasMajor);
            addScreenXY(xe, y);
            addScreenXY(xe, yBasMajor);
            _g.drawString(t, (int) xLeft, (int) (y + ftHaut));
            addScreenXY(xLeft, y + ftHaut);
            lastMaxX = xLeft + wordWidth;
          } else {
            _g.drawLine(xe, y, xe, yBas);
            addScreenXY(xe, y);
            addScreenXY(xe, yBas);
            if (traceGrille_) {
              traceGraduations.dessineTrait(_g, xe, y, xe, yMaxGrad);
            }
          }
        } else {
          _g.drawLine(xe, y, xe, yBas);
          addScreenXY(xe, y);
          addScreenXY(xe, yBas);
          if (traceSousGrille_) {
            traceSousGraduations.dessineTrait(_g, xe, y, xe, yMaxGrad);
          } else {
          }
        }
      }
    }

    vals.trimToSize();

  }

  /**
   * Appelee uniquement si isIntegerStep est true: permet de donner une représentation specifique pour la graduation passee en parametres. Par defaut,
   * l'entier est traduit en string.
   *
   * @param _i la graduation a dessiner
   * @return la chaine representant cette graduation
   */
  protected String getGraduationFor(final int _i) {
    return CtuluLibString.getString(_i);
  }

  protected Color getLightColor() {
    final int rc = lineColor_.getRed();
    final int gc = lineColor_.getGreen();
    final int bc = lineColor_.getBlue();
    final int incr = Math.max(Math.max((200 - rc), (200 - gc)), (200 - bc));
    return new Color(Math.min(255, rc + incr / 5), Math.min(255, gc + incr / 5), Math.min(255, bc + incr / 5));
  }

  protected Color getSuperLightColor() {
    final int rc = lineColor_.getRed();
    final int gc = lineColor_.getGreen();
    final int bc = lineColor_.getBlue();
    final int incr = Math.max(Math.max((200 - rc), (200 - gc)), (200 - bc));
    return new Color(Math.min(255, rc + incr), Math.min(255, gc + incr), Math.min(255, bc + incr));
  }

  @Override
  protected TickIterator createMainTickIterator() {
    if (isDiscret_ && axisIterator_ == null) {
      axisIterator_ = new NumberIntegerIterator();
    }
    return super.createMainTickIterator();
  }

  /**
   * Dessine l'axe horizontal.
   *
   * @param _g le graphics cible
   * @param _f le repere
   */
  public void dessine(final Graphics2D _g, final EGRepere _f) {
    clearXYScreen();
    if (!visible_) {
      return;
    }
    final Font old = _g.getFont();
    if (font_ != null) {
      _g.setFont(font_);
    }
    _g.setColor(lineColor_);
    final int y = _f.getMaxEcranY();
    if (graduations_ || isGridPainted()) {
      afficheGraduationsEtGrille(_g, _f);
    }
    int xf = _f.getMaxEcranX() + _f.getMargesDroite() - 1;
    final int xi = _f.getXEcran(getMinimum());
    paintTitre(_g, _f, y, xf, xi);
    if (!isExtremiteDessinee_) {
      xf = _f.getMaxEcranX();
    }
    _g.drawLine(xi, y, xf, y);
    addScreenXY(xi, y);
    addScreenXY(xf, y);
    if (isExtremiteDessinee_) {
      _g.drawLine(xf, y, xf - 3, y - 2);
      _g.drawLine(xf, y, xf - 3, y + 2);
      addScreenXY(xf - 3, y - 2);
      addScreenXY(xf - 3, y + 2);
    }
    _g.setFont(old);
    if (FuLog.isDebug() && Fu.DEBUG) {
      FuLog.debug(getClass().getName() + " paint END");
    }
  }

  private void paintTitre(final Graphics2D _g, final EGRepere _f, final int _y, final int _xf, final int _xi) {
    if (titreVisible_ || uniteVisible_) {
      final String t = titreVisible_ ? titre_ : null;
      String unit = null;
      if (uniteVisible_ && unite_ != null && unite_.length() > 0) {
        unit = '[' + unite_ + ']';
      }
      _g.setColor(lineColor_);

      int width = 0;
      Font font = getFont();
      if (font == null) {
        font = BuLib.DEFAULT_FONT;
      }
      final FontMetrics fm = _g.getFontMetrics(font);
      if (isTitreCentre_) {
        String txt = t;
        if (unit != null) {
          if (t == null) {
            txt = unit;
          } else {
            txt = txt + CtuluLibString.ESPACE + unit;
          }
        }
        final int xtext = _xi + (_f.getMaxEcranX() - _xi) / 2 - fm.stringWidth(txt) / 2;
        _g.drawString(txt, xtext, _y + getBottomHeightNeeded(_g) - fm.getHeight() - 1);
//        addScreenXY(xtext, _y + getBottomHeightNeeded(_g) - fm.getHeight() - 1);
//        addScreenXY(xtext + fm.stringWidth(t), _y + getBottomHeightNeeded(_g));
      } else {
        if (t != null) {
          width = fm.stringWidth(t);
        }
        if (unit != null) {
          final int unitWidth = fm.stringWidth(unit);
          if (unitWidth > width) {
            width = unitWidth;
          }
        }
        int yTitle = _y + fm.getAscent();
        if (t != null) {
          _g.drawString(t, _xf - width, yTitle);
          yTitle += fm.getHeight();
        }
        if (unit != null) {

          _g.drawString(unit, _xf - width, yTitle);
        }
      }
    }
  }

  /**
   * @param _g la cible
   * @return l'espace requis en bas de l'axe
   */
  public int getBottomHeightNeeded(final Graphics2D _g) {
    // epaisseur trait
    int r = 3;
    if (_g == null) {
      return r;
    }
    if (graduations_ || isGridPainted()) {
      if (font_ == null) {
        font_ = EGGraphe.DEFAULT_FONT;
      }
      // la graduation
      r += 10;
      r += _g.getFontMetrics(font_).getHeight();
    }
    // si la legende est centree elle est dessinee en-dessous de la graduation
    if (isTitreCentre_) {
      r += _g.getFontMetrics(font_).getHeight();
    }
    return r;
  }

  /**
   * @param _g le graphics dessine
   * @return l'espace requis a droite de l'axe
   */
  public int getRightWidthNeeded(final Graphics2D _g) {
    int r = 0;
    if (!isTitreCentre_) {
      if (titreVisible_ && titre_ != null) {
        r = _g.getFontMetrics(font_).stringWidth(titre_) + 1;
      }
      if (uniteVisible_ && unite_ != null) {
        final int t = _g.getFontMetrics(font_).stringWidth(unite_) + 1;
        if (t > r) {
          r = t;
        }
      }
    }
    if (isExtremiteDessinee_) {
      r += 3;
    }
    final FontMetrics fm = _g.getFontMetrics();
    int lastGraduation = fm.stringWidth(getStringAffiche(getMaximum())) / 2;
    return Math.max(r, lastGraduation);
  }

  /**
   * @param _val la valeur a considerer
   * @return la chaine a utiliser pour l'affichage
   */
  @Override
  public String getStringAffiche(final double _val) {
    if (specificFormat_ != null) {
      return specificFormat_.format(_val);
    }
    return super.getStringAffiche(_val);
  }

  /**
   * Les graduations peuvent sous forme d'entier. Dans ce cas, la graduation est effectuee en consequence: seul des entiers sont dessinés.
   *
   * @return true si la graduation est faite par palier.
   */
  /*
   * public final boolean isIntegerStep(){ return isIntegerStep_; }
   */
  @Override
  public final boolean isVertical() {
    return false;
  }

  public static EGAxeHorizontal buildDefautTimeAxe(final CtuluNumberFormatI _i) {
    final EGAxeHorizontal h = new EGAxeHorizontal();
    h.setTitre("t");
    h.setAxisIterator(new DateIterator());
    if (_i == null) {
      h.setUnite("s");
      h.setSpecificFormat(CtuluNumberFormatDefault.buildNoneFormatter());
    } else {
      h.setSpecificFormat(_i);
    }
    final CtuluValueEditorTime ed = new CtuluValueEditorTime();
    ed.setFmt(h.getSpecificFormat());
    h.setValueEditor(ed);

    return h;
  }

  /**
   * @param _isIntegerStep si la graduation est faite par palier.
   */
  /*
   * public final void setIntegerStep(boolean _isIntegerStep){ isIntegerStep_ = _isIntegerStep; if(axisIterator_!=null) axisIterator_=null; }
   */
  public EGAxeHorizontal duplicate() {

    final EGAxeHorizontal duplic = new EGAxeHorizontal(this);
    if (getFont() != null) {
      duplic.setFont(new Font(this.getFont().getFamily(), this.getFont().getStyle(), this.getFont().getSize()));
    }
    duplic.setTitre(this.getTitre());

    duplic.graduations_ = graduations_;
    duplic.longueurPas_ = longueurPas_;
    duplic.setModeGraduations(getModeGraduations());
    duplic.nbPas_ = nbPas_;
    duplic.nbSousGraduations_ = nbSousGraduations_;
    // duplic.range_=new CtuluRange(range_);
    duplic.traceGraduations_ = new TraceLigneModel(traceGraduations_);
    duplic.traceSousGraduations_ = new TraceLigneModel(traceSousGraduations_);
    duplic.traceGrille_ = traceGrille_;
    duplic.traceSousGrille_ = traceSousGrille_;
    duplic.nbSousGraduations_ = nbSousGraduations_;
    return duplic;
  }
}
