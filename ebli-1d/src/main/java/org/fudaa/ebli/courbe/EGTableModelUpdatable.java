/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import java.util.List;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluDoubleParser;

/**
 *
 * @author Frederic Deniger
 */
public interface EGTableModelUpdatable {

  int updateLines(final List _tab, final int _selectedColumm,
          final int _selectedRow, final CtuluDoubleParser _doubleParser, final CtuluCommandComposite _cmp);

  void addValuesInModel(List _tab, CtuluDoubleParser _doubleParser, CtuluCommandComposite _cmp, int _maxUpdate);

}
