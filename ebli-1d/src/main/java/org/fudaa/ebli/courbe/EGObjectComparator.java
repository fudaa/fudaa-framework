/*
 *  @creation     7 juil. 2004
 *  @modification $Date: 2006-09-19 14:55:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;



/**
 * @author Fred Deniger
 * @version $Id: EGObjectComparator.java,v 1.8 2006-09-19 14:55:53 deniger Exp $
 */
public class EGObjectComparator implements java.util.Comparator {

  @Override
  public int compare(final Object _a,final Object _b){
    if((_a instanceof EGObject) && (_b instanceof EGObject)) {
      return compare((EGObject)_a,(EGObject)_b);
    }
  throw new IllegalArgumentException();
  }

  /**
   * @param _a
   * @param _b
   * @return 0 si egaux et sinon comparaison des titres. si meme titre comparaison
   *    des hashcode
   */
  public int compare(final EGObject _a,final EGObject _b){
    if(_a==_b) {
      return 0;
    }
    final int r=_a.getTitle().compareTo(_b.getTitle());
    if(r!=0) {
      return r;
    }
    return _a.hashCode()-_b.hashCode();
  }

}
