/*
 * @creation 6 juil. 2004
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuLightBorder;
import com.memoire.bu.BuTextField;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.*;
import javax.swing.plaf.TextUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: EGTreeCellRenderer.java,v 1.15 2007-05-04 13:49:42 deniger Exp $
 */
public class EGTreeCellRenderer extends JPanel implements TreeCellRenderer, ListCellRenderer, TableCellRenderer {

  public static class SimpleCellRender extends CtuluCellTextRenderer {

    final EGAxeVertical vert_;

    public SimpleCellRender() {
      this(null);
    }

    public SimpleCellRender(final EGAxeVertical _vertical) {
      super();
      vert_ = _vertical;
      setOpaque(false);
    }

    @Override
    protected void setValue(final Object _value) {
      setEnabled(vert_ == null || (((EGObject) _value).isCourbe() && vert_ == ((EGCourbe) _value).getAxeY()));
      setText(((EGObject) _value).getTitle());
      setIcon(((EGObject) _value));
    }
  }

  public static class AxeChooserRenderer extends EGTreeCellRenderer {

    final EGAxeVertical vert_;

    public AxeChooserRenderer(final EGAxeVertical _vert) {
      super();
      vert_ = _vert;
    }

    @Override
    protected void startUpdateComponent() {
      setCpEnable(true);
    }

    private void setCpEnable(final boolean _r) {
      r1_.setEnabled(_r);
      r2_.setEnabled(_r);
      r3_.setEnabled(_r);
      cbVisible_.setEnabled(_r);
    }

    @Override
    protected void finishUpdateComponent(final Object _value) {
      boolean ok = true;
      if (_value instanceof EGGroup) {
        final EGGroup g = (EGGroup) _value;
        ok = g.getAxeY() == vert_;
      } else if (_value instanceof EGCourbe) {
        ok = ((EGCourbe) _value).getAxeY() == vert_;
        if (!ok) {
          setCpEnable(false);
        }
      }
    }
  }
  final JTextField r1_;
  final JLabel r2_;
  final JLabel r3_;
  JPanel pnEdit_;
  final JCheckBox cbVisible_;

  public EGTreeCellRenderer() {
    this(false);
  }

  public EGTreeCellRenderer(final boolean _isList) {
    r1_ = new BuTextField() {
      @Override
      public void setUI(final TextUI _ui) {
        super.setUI(_ui);
        setBorder(null);
      }
    };
    r1_.setEditable(false);
    r2_ = new JLabel(CtuluLibString.ESPACE);
    r2_.setPreferredSize(new Dimension(80, 12));
    r3_ = new JLabel(CtuluLibString.ESPACE);
    r3_.setPreferredSize(new Dimension(25, 25));
    final JPanel r4 = new JPanel(new BuBorderLayout(2, 1, true, false));
    r4.add(r1_, BuBorderLayout.CENTER);
    r4.add(r2_, BuBorderLayout.SOUTH);
    cbVisible_ = new BuCheckBox();
    cbVisible_.setVerticalAlignment(SwingConstants.CENTER);
    cbVisible_.setOpaque(false);
    cbVisible_.setRequestFocusEnabled(false);
    if (_isList) {
      pnEdit_ = r4;
      r4.setOpaque(true);
      r3_.setOpaque(true);
      cbVisible_.setOpaque(true);
    } else {
      r4.setOpaque(false);
      r4.add(cbVisible_, BuBorderLayout.EAST);
    }

    setLayout(new BuBorderLayout(2, 0, true, true));
    add(r3_, BuBorderLayout.WEST);
    add(r4, BuBorderLayout.CENTER);

    setOpaque(true);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
          final boolean _hasFocus, final int _row, final int _column) {
    updateComponent(_table.getFont(), UIManager.getColor("Table.foreground"), UIManager.getColor("Table.background"),
            UIManager.getColor("Table.selectionForeground"), UIManager.getColor("Table.selectionBackground"), _value,
            _isSelected);
    if (_column == 0) {
      return r3_;
    }
    if (_column == 1) {
      return cbVisible_;
    }
    return pnEdit_;
  }

  public void updateComponent(final Font _f, final Color _foregroundColor, final Color _backGroundColor,
          final Color _selectionColor, final Color _selectionBackGroungColor, final Object _value, final boolean _selected) {
    startUpdateComponent();
    if (_value == null) {
      r1_.setText(CtuluLibString.EMPTY_STRING);
      r1_.setToolTipText(CtuluLibString.EMPTY_STRING);
      return;
    }
    r1_.setText(_value.toString());
    r1_.setToolTipText(_value.toString());
    r1_.setFont(_f);
    r2_.setFont(BuLib.deriveFont(_f, -2));
    if (_value instanceof Icon) {
      r3_.setIcon((Icon) _value);
    }
    final StringBuilder s = new StringBuilder();
    boolean isVisible = true;
    if (_value instanceof EGGroup) {
      final EGGroup g = (EGGroup) _value;
      s.append("( ").append(g.getChildCount()).append(" )");
      isVisible = g.isVisible();
    } else if (_value instanceof EGCourbe) {
      isVisible = ((EGCourbe) _value).isVisible();
    }
    if (!isVisible) {
      s.append(' ').append(EbliResource.EBLI.getString("cach�"));
    }
    cbVisible_.setSelected(isVisible);
    r2_.setText(s.toString() + CtuluLibString.ESPACE);
    if (_selected) {
      final Color bg = _selectionBackGroungColor;
      final Color fg = _selectionColor;
      setBackground(bg);
      setForeground(fg);
      r1_.setBackground(bg);
      r1_.setForeground(fg);
      r2_.setBackground(bg);
      r2_.setForeground(fg);
      r3_.setBackground(bg);
      r3_.setForeground(fg);
      if (pnEdit_ != null) {
        pnEdit_.setBackground(bg);
        pnEdit_.setForeground(fg);
      }
      cbVisible_.setBackground(bg);
      cbVisible_.setForeground(fg);
      setBorder(new BuLightBorder(BuLightBorder.RAISED, 3));
    } else {
      final Color bg = _backGroundColor;
      final Color fg = _foregroundColor;
      setBackground(bg);
      setForeground(fg);
      r1_.setBackground(bg);
      r1_.setForeground(fg);
      r2_.setBackground(bg);
      r2_.setForeground(fg);
      r3_.setBackground(bg);
      r3_.setForeground(fg);
      if (pnEdit_ != null) {
        pnEdit_.setBackground(bg);
        pnEdit_.setForeground(fg);
      }
      cbVisible_.setBackground(bg);
      cbVisible_.setForeground(fg);
      setBorder(BuBorders.EMPTY3333);
    }
    r1_.setToolTipText(_value.toString());
    setToolTipText(_value.toString());
    r2_.setToolTipText(_value.toString());
    finishUpdateComponent(_value);
  }

  protected void startUpdateComponent() {
  }

  protected void finishUpdateComponent(final Object _value) {
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
          final boolean _isSelected, final boolean _cellHasFocus) {
    updateComponent(_list.getFont(), _list.getForeground(), _list.getBackground(), _list.getSelectionForeground(),
            _list.getSelectionBackground(), _value, _isSelected);
    return this;
  }
  Color treeSelectionForeground_;
  Color treeSelectionBackground_;

  private void updateTreeCellRenderer(final JTree _tree, final Object _value, final boolean _selected) {
    if (treeSelectionForeground_ == null) {
      treeSelectionForeground_ = UIManager.getColor("Tree.selectionForeground");
    }
    if (treeSelectionBackground_ == null) {
      treeSelectionBackground_ = UIManager.getColor("Tree.selectionBackground");
    }

    updateComponent(_tree.getFont(), _tree.getForeground(), _tree.getBackground(), treeSelectionForeground_,
            treeSelectionBackground_, _value, _selected);
  }

  @Override
  public Component getTreeCellRendererComponent(final JTree _tree, final Object _value, final boolean _selected,
          final boolean _expanded, final boolean _leaf, final int _row, final boolean _hasFocus) {
    updateTreeCellRenderer(_tree, _value, _selected);
    return this;
  }
}
