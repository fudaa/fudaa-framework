/*
 * @creation 1999-07-29
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.courbe;

/**
 * Marges.
 * 
 * @version $Revision: 1.8 $ $Date: 2007-05-04 13:49:41 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class EGMarges {
  @Override
  public String toString() {
    return getClass().getName() + ' ' + gauche_ + ", " + haut_ + ", " + droite_ + ", " + bas_;
  }

  private int gauche_;
  private int droite_;
  private int haut_;
  private int bas_;

  public EGMarges() {
    gauche_ = 60;
    droite_ = 20;
    haut_ = 40;
    bas_ = 30;
  }

  public void ajouteEmptyBorder(final int _x) {
    gauche_ += _x;
    droite_ += _x;
    haut_ += _x;
    bas_ += _x;
  }

  public int getHorizontalMarges() {
    return droite_ + gauche_;
  }

  public int getVerticalMarges() {
    return haut_ + bas_;
  }

  public EGMarges(EGMarges toCopy) {
    this(toCopy.gauche_, toCopy.haut_, toCopy.droite_, toCopy.bas_);
  }

  public EGMarges(final int _gauche, final int _haut, final int _droite, final int _bas) {
    gauche_ = _gauche;
    droite_ = _droite;
    haut_ = _haut;
    bas_ = _bas;
  }

  public int getBas() {
    return bas_;
  }

  public void setBas(final int _bas) {
    bas_ = _bas;
  }

  public int getDroite() {
    return droite_;
  }

  public void setDroite(final int _droite) {
    droite_ = _droite;
  }

  public int getGauche() {
    return gauche_;
  }

  public void setGauche(final int _gauche) {
    gauche_ = _gauche;
  }

  public void ajouteMargesGauche(final int _toAdd) {
    gauche_ += _toAdd;
  }

  public void ajouteMargesDroite(final int _toAdd) {
    droite_ += _toAdd;
  }

  public int getHaut() {
    return haut_;
  }

  public void setHaut(final int _haut) {
    haut_ = _haut;
  }
}
