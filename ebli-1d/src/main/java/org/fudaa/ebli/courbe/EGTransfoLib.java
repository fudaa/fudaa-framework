/*
 * @creation 9 ao�t 2004
 * @modification $Date: 2007-06-05 08:58:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.*;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import java.awt.Color;
import javax.swing.DefaultCellEditor;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EGTransfoLib.java,v 1.19 2007-06-05 08:58:39 deniger Exp $
 */
public final class EGTransfoLib {

  private EGTransfoLib() {
  }

  /**
   * Model pour des points selectionnes.
   *
   * @author Fred Deniger
   * @version $Id: EGTransfoLib.java,v 1.19 2007-06-05 08:58:39 deniger Exp $
   */
  public static class CourbeTableModel extends AbstractTableModel {

    final EGGraphe a_;
    final EGCourbe c_;
    final int[] idx_;
    TIntObjectHashMap xy_;

    /**
     * @param _a le graphe
     * @param _c la courbe en question
     * @param _idx les indices
     */
    public CourbeTableModel(final EGGraphe _a, final EGCourbe _c, final int[] _idx) {
      a_ = _a;
      c_ = _c;
      idx_ = _idx;
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      if (_columnIndex == 0) {
        return c_.getModel().isXModifiable();
      }
      return c_.getModel().isModifiable();
    }

    @Override
    public String getColumnName(final int _column) {
      if (_column == 1) {
        return c_.getAxeY().getAxeTexte();
      }
      return a_.getTransformer().getXAxe().getAxeTexte();
    }

    @Override
    public int getRowCount() {
      return idx_.length;
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      final int i = idx_[_rowIndex];
      if (_columnIndex == 0) {
        if ((xy_ != null) && xy_.contains(i)) {
          return ((String[]) xy_.get(i))[0];
        }
        return a_.getTransformer().getXAxe().getStringAffiche(c_.getModel().getX(i));
      }
      if ((xy_ != null) && xy_.contains(i)) {
        return ((String[]) xy_.get(i))[1];
      }
      return c_.getAxeY().getStringAffiche(c_.getModel().getY(i));
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      final int i = idx_[_rowIndex];
      if (_columnIndex == 0) {
        if (xy_ == null) {
          xy_ = new TIntObjectHashMap(idx_.length);
        }
        String[] v = (String[]) xy_.get(i);
        if (v == null) {
          v = new String[2];
          v[0] = (String) _value;
          v[1] = (String) getValueAt(_rowIndex, 1);
          xy_.put(i, v);
        } else {
          v[0] = (String) _value;
        }

      } else {
        if (xy_ == null) {
          xy_ = new TIntObjectHashMap(idx_.length);
        }
        String[] v = (String[]) xy_.get(i);
        if (v == null) {
          v = new String[2];
          v[1] = (String) _value;
          v[0] = (String) getValueAt(_rowIndex, 0);
          xy_.put(i, v);
        } else {
          v[1] = (String) _value;
        }
      }
    }
  }

  public static void modifier(final EGGraphe _a, final EGCourbe _c, final int[] _i, final CtuluCommandContainer _cmd) {
    if (_i == null) {
      return;
    }
    if (_i.length == 1) {
      modifier(_a, _c, _i[0], _cmd);
      return;
    }
    if (!_c.getModel().isModifiable()) {
      return;
    }
    final CourbeTableModel m = new CourbeTableModel(_a, _c, _i);
    final BuTextField createDoubleField = BuTextField.createDoubleField();
    TableCellEditor e = new DefaultCellEditor(createDoubleField);
    final JTable t = new CtuluTable(m);
    t.getColumnModel().getColumn(1).setCellEditor(e);
    if (_c.getModel().isXModifiable()) {
      if (_a.getTransformer().getXAxe().isDiscret()) {
        e = new DefaultCellEditor(BuTextField.createIntegerField());
      }
      t.getColumnModel().getColumn(0).setCellEditor(e);
    }
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.add(new BuScrollPane(t));
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_a)) && m.xy_.size() > 0) {
      final int[] idx = new int[m.xy_.size()];
      final double[] x = _c.getModel().isXModifiable() ? new double[idx.length] : null;
      final double[] y = new double[idx.length];
      final TIntObjectIterator iterator = m.xy_.iterator();
      for (int i = idx.length; i-- > 0;) {
        iterator.advance();
        idx[i] = iterator.key();
        final String[] s = (String[]) iterator.value();
        y[i] = Double.parseDouble(s[1]);
        if (x != null) {
          x[i] = Double.parseDouble(s[0]);
        }
      }
      _c.getModel().setValues(idx, x, y, _cmd);
    }
  }

  public static void ajouterPoint(final EGGraphe _a, final EGCourbe _c, final int _min, final int _max,
          final CtuluCommandContainer _cmd) {
    if (_c == null || !_c.getModel().isXModifiable()) {
      return;
    }
    final JSpinner nbPt = new JSpinner(new SpinnerNumberModel(1, 1, 10000, 1));
    final BuLabel lb = new BuLabel(EbliLib.getS("Nombre de points � ajouter"));
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {

        boolean r = ((Number) nbPt.getValue()).intValue() >= 1;
        if (!r) {
          lb.setForeground(Color.RED);
        }
        return r;
      }
    };
    pn.setLayout(new BuVerticalLayout(2, true, false));
    String x1;
    String y1;
    String x2;
    String y2;
    if (_c.getAxeY().isSpecificFormat()) {
      y1 = _c.getAxeY().getSpecificFormat().format(_c.getModel().getY(_min));
      y2 = _c.getAxeY().getSpecificFormat().format(_c.getModel().getY(_max));
    } else {
      y1 = Double.toString(_c.getModel().getY(_min));
      y2 = Double.toString(_c.getModel().getY(_max));
    }
    final EGAxeHorizontal h = _a.getTransformer().getXAxe();
    if (h.isSpecificFormat()) {
      x1 = h.getSpecificFormat().format(_c.getModel().getX(_min));
      x2 = h.getSpecificFormat().format(_c.getModel().getX(_max));

    } else {
      x1 = Double.toString(_c.getModel().getX(_min));
      x2 = Double.toString(_c.getModel().getX(_max));
    }
    pn.addLabel("<html>"
            + EbliLib.getS("Pr�ciser le nombre de points � ajouter entre")
            + "<br>"
            + EbliLib.getS("le point {0}", CtuluLibString.getString(_min + 1) + CtuluLibString.ESPACE + '(' + x1 + ';' + y1
            + ") ")
            + EbliLib.getS("et")
            + EbliLib.getS("le point {0}", CtuluLibString.getString(_max + 1) + CtuluLibString.ESPACE + '(' + x2 + ';' + y2
            + ')') + "</html>");
    final BuPanel pnNb = new BuPanel();
    pnNb.add(lb);
    pnNb.add(nbPt);
    pn.add(pnNb);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_a))) {
      int nb = ((Integer) nbPt.getValue()).intValue();
      double[] newX = new double[nb];
      double[] newY = new double[nb];
      final double min = _c.getModel().getX(_min);
      final CtuluNumberFormatFortran ft = new CtuluNumberFormatFortran(10);
      if (_max - _min == 1) {
        final double step = (_c.getModel().getX(_max) - min) / (nb + 1);
        for (int i = 1; i <= nb; i++) {
          newX[i - 1] = Double.parseDouble(ft.format(min + step * i));
        }
      } else {
        int idx = 0;
        final double dtotal = _c.getModel().getX(_max) - min;

        for (int i = _min; i < _max && idx < nb; i++) {
          final double minLocal = _c.getModel().getX(i);
          final double di = _c.getModel().getX(i + 1) - minLocal;
          int localPt = (int) Math.ceil(di / dtotal * nb);
          // au dernier point on va partager
          if (i == _max - 1) {
            localPt = nb - i;
          }
          final double step = di / (localPt + 1);
          for (int j = 0; j < localPt && idx < nb; j++) {
            newX[idx++] = Double.parseDouble(ft.format(minLocal + (j + 1) * step));
          }
        }
        if (idx != nb) {
          final double[] tmp = new double[idx];
          System.arraycopy(newX, 0, tmp, 0, tmp.length);
          newX = tmp;
          newY = new double[newX.length];
          nb = newX.length;
        }
      }
      for (int i = 0; i < nb; i++) {
        newY[i] = Double.parseDouble(ft.format(_c.interpol(newX[i])));
      }
      _c.getModel().addValue(newX, newY, _cmd);
    }
  }

  /**
   * @param _a le graphe
   * @param _c la courbe a modifier
   * @param _i l'indice
   * @param _cmd le receveur de commande
   */
  public static void modifier(final EGGraphe _a, final EGCourbe _c, final int _i, final CtuluCommandContainer _cmd) {
    if (!_c.getModel().isModifiable()) {
      return;
    }
    final double x1 = _c.getModel().getX(_i);
    final String si1 = _a.getTransformer().getXAxe().getStringAffiche(x1);
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5));
    BuTextField txtx = null;
    if (_a.getTransformer().getXAxe().isDiscret()) {
      txtx = pn.addLabelIntegerText("x: ");
    } else {
      txtx = pn.addLabelDoubleText("x: ");
    }
    txtx.setText(si1);
    txtx.setEditable(_c.getModel().isXModifiable());
    final BuTextField txty = pn.addLabelDoubleText("y: ");
    txty.setText(Double.toString(_c.getModel().getY(_i)));
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_a, EbliLib.getS("Modifier le point s�lectionn�")))) {
      _c.getModel().setValue(_i, Double.parseDouble(txtx.getText()), Double.parseDouble(txty.getText()), _cmd);
    }

  }

  /**
   * @param _a le graphe a considerer
   * @param _c la courbe a modifier
   * @param _i1 le premier indice
   * @param _i2 le dernier indice
   * @param _cmd la commande
   */
  public static void aligner(final EGGraphe _a, final EGCourbe _c, final int _i1, final int _i2,
          final CtuluCommandContainer _cmd) {
    if (!_c.getModel().isModifiable()) {
      return;
    }
    final int i1 = _i1 < _i2 ? _i1 : _i2;
    final int i2 = _i1 < _i2 ? _i2 : _i1;
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("align between " + i1 + " " + i2);
    }
    final double x1 = _c.getModel().getX(i1);
    final String si1 = _a.getTransformer().getXAxe().getStringAffiche(x1);
    final double x2 = _c.getModel().getX(i2);
    final String si2 = _a.getTransformer().getXAxe().getStringAffiche(x2);
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    final BuPanel content = new BuPanel();
    content.setLayout(new BuGridLayout(2, 5, 5));
    pn.addLabel(content, "");
    pn.addLabel(content, "y").setAlignmentX(0.5f);
    final String pt = EbliLib.getS("Point");
    final BuTextField f1 = pn.addLabelDoubleText(content, pt + CtuluLibString.ESPACE + "1 :" + si1);
    f1.setText(Double.toString(_c.getModel().getY(i1)));
    final BuTextField f2 = pn.addLabelDoubleText(content, pt + CtuluLibString.ESPACE + "2 :" + si2);
    f2.setText(Double.toString(_c.getModel().getY(i2)));
    final BuPanel help = new BuPanel();
    help.setLayout(new BuBorderLayout());
    final String h = EbliLib.getS("Tous les points entre le Point 1 et le Point 2 seront align�s");
    help.add(new BuLabel(h));
    pn.setLayout(new BuBorderLayout());
    pn.add(content, BuBorderLayout.CENTER);
    pn.add(help, BuBorderLayout.SOUTH);
    final int resp = pn.afficheModale(_a, EbliLib.getS("Aligner les points"));
    if (CtuluDialogPanel.isOkResponse(resp)) {
      final double y1 = Double.parseDouble(f1.getText());
      final double y2 = Double.parseDouble(f2.getText());
      final int nb = i2 - i1 + 1;
      final double[] y = new double[nb];
      final double[] x = new double[nb];
      final int[] idx = new int[nb];
      y[0] = y1;
      x[0] = x1;
      y[nb - 1] = y2;
      x[nb - 1] = x2;
      idx[0] = i1;
      idx[nb - 1] = i2;
      int idxEnCours = 1;
      for (int i = i1 + 1; i < i2; i++) {
        final double xi = _c.getModel().getX(i);
        x[idxEnCours] = xi;
        y[idxEnCours] = y2 + (y2 - y1) * (xi - x2) / (x2 - x1);
        idx[idxEnCours++] = i;
      }
      _c.getModel().setValues(idx, x, y, _cmd);

    }

  }
}
