/*
 * @creation 24 oct. 06
 * 
 * @modification $Date: 2007-05-04 13:49:42 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuMenu;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluCellTextDecimalRenderer;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ctulu.gui.CtuluTableExportPanel;
import org.fudaa.ctulu.gui.CtuluUIDialog;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ctulu.table.CtuluTableCellDoubleValue;
import org.fudaa.ctulu.table.CtuluTableColumnHeader;
import org.fudaa.ctulu.table.CtuluTableSortModel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliDateCellDoubleValueCellRenderer;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliTableInfoPanel;
import org.fudaa.ebli.commun.EbliTableInfoTarget;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author fred deniger
 * @version $Id: EGTableAction.java,v 1.3 2007-05-04 13:49:42 deniger Exp $
 */
public class EGTableAction extends EbliActionSimple implements EbliTableInfoTarget {

  final EGGraphe graphe_;
  EbliTableInfoPanel infoPanel = null;
  private CtuluUI ui;

  public EGTableAction(final EGGraphe _graphe) {
    super(EbliLib.getS("Tableau des valeurs"), BuResource.BU.getToolIcon("tableau"), "TABLE");
    graphe_ = _graphe;
  }

  public void setUi(CtuluUI ui) {
    this.ui = ui;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {

    infoPanel = new EbliTableInfoPanel(ui == null ? new CtuluUIDialog(graphe_) : ui, this, null);
    infoPanel.setExportTableCommentSupplier(graphe_.getExportTableCommentSupplier());
    infoPanel.showInDialog();

  }
  boolean displayAll;
  boolean addOptions = true;
  boolean addCheckbox = false;
  //boolean utilise pour afficher ou pas le dialogue permettant d'afficher les colonnes a exporter
  boolean showColumnToExport = false;
  boolean showLabel;

  public boolean isShowLabel() {
    return showLabel;
  }

  public boolean isShowColumnToExport() {
    return showColumnToExport;
  }

  public void setShowColumnToExport(boolean showColumnToExport) {
    this.showColumnToExport = showColumnToExport;
  }

  public boolean isAddCheckbox() {
    return addCheckbox;
  }

  public void setAddCheckbox(boolean addCheckbox) {
    this.addCheckbox = addCheckbox;
  }

  public void setShowLabel(boolean showLabel) {
    this.showLabel = showLabel;
  }

  public boolean isDisplayAll() {
    return displayAll;
  }

  public void setDisplayAll(boolean displayAll) {
    this.displayAll = displayAll;
  }

  public boolean isAddOptions() {
    return addOptions;
  }

  public void setAddOptions(boolean addOptions) {
    this.addOptions = addOptions;
  }

  protected boolean isExportable(EGCourbe cs) {
    if (cs == null) {
      return false;
    }
    EGModel model = cs.getModel();
    if (model instanceof EGModelExportable) {
      return ((EGModelExportable) model).isViewableinTable();
    }
    return true;
  }

  protected boolean isExportable(EGObject cs) {
    if (cs instanceof EGCourbe) {
      return isExportable((EGCourbe) cs);
    }
    return false;
  }

  @Override
  public BuTable createValuesTable() {
    EGCourbe[] cs = graphe_.getModel().getCourbes();
    List<EGCourbe> exportable = new ArrayList<>();
    for (EGCourbe courbe : cs) {
      if (isExportable(courbe)) {
        exportable.add(courbe);
      }
    }
    cs = exportable.toArray(new EGCourbe[0]);
    final EGObject[] selected = graphe_.getModel().getSelectedObjects();
    final int nb = selected == null ? 0 : selected.length;
    final List courbesSelected = new ArrayList(nb);
    if (displayAll) {
      for (int i = 0; i < cs.length; i++) {
        cs[i].fillWithCurves(courbesSelected);
      }
    } else if (selected != null) {
      for (int i = 0; i < nb; i++) {
        if (isExportable(selected[i])) {
          selected[i].fillWithCurves(courbesSelected);
        }
      }
    }
    final EGMultiCourbeTableModel tableModel = new EGMultiCourbeTableModel(cs, (EGCourbe[]) courbesSelected.toArray(new EGCourbe[0]), graphe_, showLabel);
    final CtuluTable table = new CtuluTable(tableModel);
    table.setDefaultRenderer(CtuluTableCellDoubleValue.class, new EbliDateCellDoubleValueCellRenderer());
    String title = null;
    final JInternalFrame iframe = (JInternalFrame) SwingUtilities.getAncestorOfClass(JInternalFrame.class, graphe_);
    if (iframe == null) {
      final JFrame f = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, graphe_);
      if (f != null) {
        title = f.getTitle();
      }
    } else {
      title = iframe.getTitle();
    }
    if (title == null) {
      title = EbliResource.EBLI.getString("Courbes");
    }
    EbliTableInfoPanel.setTitle(table, title);
    List<JComponent> components = new ArrayList<>();
    if (addOptions) {
      final BuMenu menu = new BuMenu(EbliLib.getS("Options"), "OPTIONS");
      menu.setIcon(BuResource.BU.getMenuIcon("configurer"));
      final ActionListener ac = new OptionActionListener(tableModel, table);
      final boolean isCourbeSelected = courbesSelected.size() > 0;
      menu.addCheckBox(EbliLib.getS("Afficher uniquement les courbes sélectionnées"), "SHOW_SELECTED_COURBES", null, isCourbeSelected,
              isCourbeSelected)
              .addActionListener(ac);
      menu.addCheckBox(EbliLib.getS("Afficher une seule colonne des abscisses"), "SHOW_ONE_TIME", null, true, false).addActionListener(ac);
      tableModel.setShowOnlySelected(isCourbeSelected);
      final CtuluPopupMenu popup = new CtuluPopupMenu();
      popup.addCheckBox(EbliLib.getS("Afficher uniquement les courbes sélectionnées"), "SHOW_SELECTED_COURBES", null, isCourbeSelected,
              isCourbeSelected).addActionListener(ac);
      popup.addCheckBox(EbliLib.getS("Afficher une seule colonne des abscisses"), "SHOW_ONE_TIME", null, true, false).addActionListener(ac);
      table.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseReleased(final MouseEvent _e) {
          if (EbliLib.isPopupMouseEvent(_e)) {
            popup.show(table, _e.getX(), _e.getY());
          }
        }
      });

      if (table.getModel() instanceof EGMultiCourbeTableModel) {
        EGMultiCourbeTableModel modelInit = ((EGMultiCourbeTableModel) table.getModel());
        int[] vals = modelInit.getSelectedRows();
        if (vals != null && vals.length > 0) {
          //TODO... ajout de la checkbox pour filtrer
          ActionListener actionL = new OptionActionListener(modelInit, table);
          menu.addCheckBox(EbliLib.getS("Afficher les points initiaux"), "SHOW_INITIAL_POINT", true, false)
                  .addActionListener(actionL);
        }
      }

      //EbliTableInfoPanel.setJMenuBarComponents(table, new JComponent[]{menu});
      components.add(menu);

    }
    if (addCheckbox) {
      JCheckBox ch = new JCheckBox(EbliLib.getS("Factorisation des abscisses"));
      ch.setActionCommand("SHOW_ONE_TIME");
      ch.addActionListener(new OptionActionListener(tableModel, table));
      components.add(ch);
    }

    if (!components.isEmpty()) {
      EbliTableInfoPanel.setJMenuBarComponents(table, components.toArray(new JComponent[0]));
    }

    EbliTableInfoPanel.setShowSelectedRow(table, false);
    CtuluTableExportPanel.setShowColumnsToExport(table, showColumnToExport);
    return table;
  }

  static class OptionActionListener implements ActionListener {

    final EGMultiCourbeTableModel target_;
    final CtuluTable table_;

    public OptionActionListener(final EGMultiCourbeTableModel _target, final CtuluTable _dest) {
      super();
      target_ = _target;
      table_ = _dest;
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      final String com = _e.getActionCommand();
      CtuluTableSortModel model = null;
      int idx = -1;
      if (table_.getModel() instanceof CtuluTableSortModel) {
        model = (CtuluTableSortModel) table_.getModel();
        idx = model.getSortingColumn();
      }

      if ("SHOW_SELECTED_COURBES".equals(com)) {
        Map<Object, TableCellRenderer> cellsRenderer = getCellsRenderer();
        target_.setShowOnlySelected(((AbstractButton) _e.getSource()).isSelected());
        putCellsRenderer(cellsRenderer);
      } else if ("SHOW_ONE_TIME".equals(com)) {
        Map<Object, TableCellRenderer> cellsRenderer = getCellsRenderer();
        target_.setShowOneTime(((AbstractButton) _e.getSource()).isSelected());
        for (int i = 0; i < table_.getColumnCount(); i++) {
          ((CtuluTableColumnHeader) table_.getTableHeader()).adjustWidth(table_.getColumnModel().getColumn(i));
        }
        putCellsRenderer(cellsRenderer);
      } else if ("SHOW_INITIAL_POINT".equals(com)) {
        //-- AHX - TODO filter table model --//
        boolean activateFilter = ((AbstractButton) _e.getSource()).isSelected();
        target_.setSelectRows(activateFilter);
        //-- refresh table --//
        target_.fireTableDataChanged();

      }
      if (idx > 0 && model != null) {
        model.removeSortingColumn(idx);
      }

    }

    private Map<Object, TableCellRenderer> getCellsRenderer() {
      TableColumnModel columnModel = table_.getColumnModel();
      Map<Object, TableCellRenderer> res = new HashMap<>();
      int columnCount = columnModel.getColumnCount();
      for (int colIdx = 0; colIdx < columnCount; colIdx++) {
        res.put(columnModel.getColumn(colIdx).getIdentifier(), columnModel.getColumn(colIdx).getCellRenderer());
      }
      return res;

    }

    private void putCellsRenderer(Map<Object, TableCellRenderer> renderer) {
      if (renderer == null) {
        return;
      }
      TableColumnModel columnModel = table_.getColumnModel();
      int columnCount = columnModel.getColumnCount();
      for (int colIdx = 0; colIdx < columnCount; colIdx++) {
        TableCellRenderer tableCellRenderer = renderer.get(columnModel.getColumn(colIdx).getIdentifier());
        if (tableCellRenderer == null) {
          tableCellRenderer = new CtuluCellTextDecimalRenderer();
        }
        columnModel.getColumn(colIdx).setCellRenderer(tableCellRenderer);
      }

    }
  }

  @Override
  public int[] getSelectedObjectInTable() {
    return null;
  }

  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }
}
