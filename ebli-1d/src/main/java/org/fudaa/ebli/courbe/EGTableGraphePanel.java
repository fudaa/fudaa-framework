/*
 * @creation 9 ao�t 2004
 * @modification $Date: 2007-06-05 08:58:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.*;
import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluSelectionListTableModelUpdater;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ctulu.table.CtuluTableExportInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.Timer;

/**
 * Un panneau qui affiche une table de la courbe selectionne par le graphe.
 *
 * @author Fred Deniger
 * @version $Id: EGTableGraphePanel.java,v 1.31 2007-06-05 08:58:39 deniger Exp $
 */
public class EGTableGraphePanel extends BuPanel implements EGSelectionListener, EGGrapheModelListener {
  EGGraphe a_;
  BuToolButton btAdd_;
  BuToolButton btCopy_;
  BuToolButton btImport_;
  BuToolButton btAlign_;
  BuToolButton btRemove_;
  final BuLabel lb_;
  final BuLabel lbCourbeInfo_;
  final BuLabel lbError_;
  BuLabel lbXyInfo_;
  int nbPoint_;
  EGInteractionSelection selection_;
  EvolTable t_;
  final CtuluCellTextRenderer renderer_ = new CtuluCellTextRenderer() {
    @Override
    public Component getTableCellRendererComponent(JTable _table, Object _value, boolean _isSelected,
                                                   boolean _hasFocus, int _row, int _column) {
      setToolTipText(CtuluLibString.EMPTY_STRING);
      Component res = super.getTableCellRendererComponent(_table, _value, _isSelected, _hasFocus, _row, _column);
      if (_value != null) {
        boolean ok = getSpecTableModel().isPointDrawn(_row);
        res.setEnabled(ok);
        if (!ok && _column == 1) {
          setText(CtuluLibString.EMPTY_STRING);
        }
        SpecTableModel model = (SpecTableModel) _table.getModel();
        //on modifie le tooltip que si non modifi� par le decorator eventuelle...
        if (!model.isAddingValuesRow(_row)) {
          if (CtuluLibString.EMPTY_STRING.equals(getToolTipText())) {
            if (_column == model.getXColIndex()) {
              setToolTipText(model.getXDetailAsString(_row));
            } else if (_column == model.getYColIndex()) {
              setToolTipText(model.getYDetailAsString(_row));
            }
          }
        }
      }
      return this;
    }

    @Override
    protected void setValue(final Object _value) {
      if (_value == null) {
        super.setValue(EbliLib.getS("Ajouter"));
      } else {
        super.setValue(_value);
      }
    }
  };
  boolean afficheNomCourbe_ = true;
  EbliActionSimple actionDelete;
  CtuluSelectionListTableModelUpdater updater_;
  private boolean addTopButtons = true;
  private boolean addPasteButtons = true;

  public EGTableGraphePanel() {
    this(true);
  }

  /**
   * Initialise tous les composants n�cessaires.
   */
  public EGTableGraphePanel(boolean afficheNomCourbe) {
    setLayout(new BuBorderLayout(5, 1, true, true));
    setBorder(BorderFactory.createEmptyBorder(2, 5, 5, 2));
    lb_ = new BuLabel();
    lb_.setHorizontalAlignment(SwingConstants.CENTER);
    lb_.setHorizontalTextPosition(SwingConstants.CENTER);
    lb_.setVerticalAlignment(SwingConstants.CENTER);
    lb_.setVerticalTextPosition(SwingConstants.CENTER);
    afficheNomCourbe_ = afficheNomCourbe;
    if (afficheNomCourbe) {
      add(lb_, BuBorderLayout.NORTH);
    }
    t_ = new EvolTable() {
      @Override
      public TableCellEditor getCellEditor(final int _row, final int _column) {
        if (!isCellEditable(_row, _column)) {
          return null;
        }
        CtuluValueEditorI r = null;
        SpecTableModel tableModel = (SpecTableModel) t_.getModel();
        if (_column == tableModel.getXColIndex() && a_ != null && a_.getTransformer().getXAxe() != null) {
          r = a_.getTransformer().getXAxe().getValueEditor();
        } else if (_column == tableModel.getYColIndex() && a_ != null && a_.getSelectedComponent() != null) {
          r = a_.getSelectedComponent().getAxeY().getValueEditor();
        }
        final TableCellEditor ed = r == null ? null : r.createTableEditorComponent();
        final TableCellEditor finalCellEditor = ed == null ? super.getCellEditor(_row, _column) : ed;
//la derniere ligne sert � ajouter des valeurs et donc pas de s�lection � avoir...
        if (finalCellEditor instanceof BuTableCellEditor && _row < getRowCount() - 1) {
          ((BuTableCellEditor) finalCellEditor).setSelectAll(true);
        }
        return finalCellEditor;
      }

      @Override
      public TableCellRenderer getDefaultRenderer(final Class _columnClass) {
        if (Double.class
            .equals(_columnClass)) {
          return renderer_;
        }
        return super.getDefaultRenderer(_columnClass);
      }
    };
    t_.setDeleteAction(getActionDelete());
    setDefaultEnterAction(t_);
    final BuTextField txt = createEditorDoubleTextField();
    txt.setColumns(10);
    t_
        .setDefaultEditor(Double.class, new BuTableCellEditor(txt) {
          @Override
          public boolean isCellEditable(final EventObject _evt) {
            if (_evt instanceof MouseEvent) {
              return ((MouseEvent) _evt).getClickCount() >= 2;
            }
            return true;
          }
        });

    t_.setModel(createValuesTableModel());
    t_.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    t_.setCellSelectionEnabled(true);
    add(new JScrollPane(t_), BuBorderLayout.CENTER);
    lbError_ = new BuLabel(CtuluLibString.ESPACE);
    lbError_.setForeground(Color.RED);
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuVerticalLayout());
    pn.add(lbError_);
    lbCourbeInfo_ = new BuLabel(CtuluLibString.ESPACE);
    lbCourbeInfo_.setBorder(BorderFactory.createEtchedBorder());
    lbError_.setFont(CtuluLibSwing.getMiniFont());
    lbCourbeInfo_.setFont(CtuluLibSwing.getMiniFont());
    pn.add(lbCourbeInfo_);
    add(pn, BuBorderLayout.SOUTH);
    setPreferredSize(new Dimension(150, 200));
  }

  private static Action getEnterAction() {
    return new AbstractAction() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        final JTable t = (JTable) _e.getSource();
        SpecTableModel tableModel = (SpecTableModel) t.getModel();
        int row = t.getSelectedRow();
        int col = t.getSelectedColumn();
        if (t.isEditing()) {
          t.getCellEditor().stopCellEditing();
        }

        if (col == tableModel.yColIndex) {
          col = tableModel.xColIndex;
          row++;
        } else {
          col = tableModel.yColIndex;
        }
        if (row >= t.getRowCount()) {
          row = t.getRowCount() - 1;
        }
        if ((row >= 0) && (col >= 0)) {
          t.getSelectionModel().setSelectionInterval(row, row);
          t.getColumnModel().getSelectionModel().setSelectionInterval(col, col);
          t.scrollRectToVisible(t.getCellRect(row, col, true));
        }
      }
    };
  }

  public static void setDefaultEnterAction(final JTable _t) {
    final ActionMap map = _t.getActionMap();
    map.put("theSuperActionOfTheyear", EGTableGraphePanel.getEnterAction());
    final InputMap inMap = _t.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    inMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "theSuperActionOfTheyear");
  }

  public static KeyStroke getDeleteKeyStroke() {
    return KeyStroke.getKeyStroke("control D");
  }

  public static void removeButton(JButton bt) {
    if (bt != null && bt.getParent() != null) {
      bt.getParent().remove(bt);
    }
  }

  public BuTextField createEditorDoubleTextField() {
    BuTextField r = BuTextField.createDoubleField();
    r.setSelectAllIsGainFocus(true);
    return r;
  }

  public CtuluCellTextRenderer getDefaultRenderer() {
    return renderer_;
  }

  public EvolTable getTable() {
    return t_;
  }

  public BuLabel getTitleLabel() {
    return lb_;
  }

  protected SpecTableModel createValuesTableModel() {
    return new SpecTableModel(this);
  }

  private void eventReceived() {
    if (t_.getCellEditor() != null) {
      t_.getCellEditor().cancelCellEditing();
    }
    if (a_.getSelectedComponent() == null) {
      lb_.setText(CtuluLibString.ESPACE);
      t_.repaint();
    } else {
      final String n = a_.getSelectedComponent().getTitle();
      if (n != null && !n.equals(lb_.getText())) {
        lb_.setText(n);
      }
      final int pt = a_.getSelectedComponent().getModel().getNbValues();
      if (pt == nbPoint_) {
        (getSpecTableModel()).fireTableRowsUpdated(0, pt);
      } else {
        nbPoint_ = pt;
        (getSpecTableModel()).fireTableRowsDeleted(0, nbPoint_);
        (getSpecTableModel()).fireTableRowsInserted(0, nbPoint_);
      }
    }
  }

  void updateButtons() {
    final EGCourbe c = a_.getSelectedComponent();
    final boolean modifiable = c != null && c.getModel().isModifiable();
    if (btCopy_ != null) {
      btCopy_.setEnabled(modifiable && c.getModel().isXModifiable());
    }
    if (btImport_ != null) {
      btImport_.setEnabled(modifiable && c.getModel().isXModifiable());
    }
    if (btAdd_ != null) {
      btAdd_.setEnabled(modifiable && c.getModel().isXModifiable()
          && selection_ != null && selection_.getSelection() != null && selection_.getSelection().getNbSelectedIndex() >= 2);
    }
    if (btRemove_ != null) {
      btRemove_.setEnabled(modifiable && c.getModel().isXModifiable()
          && selection_ != null && selection_.getSelection() != null && (!selection_.getSelection().isEmpty()));
    }
    if (btAlign_ != null) {
      btAlign_.setEnabled(modifiable && selection_ != null  && selection_.getSelection() != null
          && selection_.getSelection().getNbSelectedIndex() >= 2);
    }
    if (lbXyInfo_
        != null) {
      lbXyInfo_.setText(CtuluLibString.ESPACE);
    }
    if (lbXyInfo_ != null && c != null && selection_
        != null && selection_.getSelection()
        != null
        && selection_.getSelection().isOnlyOnIndexSelected()) {
      final int i = selection_.getSelection().getMaxIndex();
      if (i == c.getModel().getNbValues()) {
        lbXyInfo_.setText(EbliLib.getS("Ajouter un point"));
      } else {
        final String s = t_.getValueAt(i, 0).toString();
        if (s.length() == 0) {
          lbXyInfo_.setText(CtuluLibString.ESPACE);
        } else {
          lbXyInfo_.setText(s + " ; " + t_.getValueAt(i, 1));
        }
      }
    }
  }

  private void updateDatas(final EGCourbe _c) {
    if (t_.getCellEditor() != null) {
      t_.getCellEditor().cancelCellEditing();
    }
    getSpecTableModel().selectedCourbeChanged(_c);
    //we update to column names:
    int nbCol = Math.max(getSpecTableModel().getColumnCount(), t_.getColumnModel().getColumnCount());
    for (int i = 0; i < nbCol; i++) {
      TableColumn column = t_.getColumnModel().getColumn(i);
      column.setHeaderValue(getSpecTableModel().getColumnName(i));
    }

    if (_c == null) {
      lb_.setText(CtuluLibString.ESPACE);
      lbCourbeInfo_.setText(CtuluLibString.ESPACE);
    } else {
      lb_.setText(_c.getTitle());
      // utilise pour mettre a jour le tableau
      nbPoint_ = _c.getModel().getNbValues();
      String txt = null;
      if (_c.getModel().isModifiable()) {
        if (_c.getModel().isXModifiable()) {
          txt = EbliLib.getS("Courbe:") + EbliLib.getS("Modifiable");
        } else {
          txt = EbliLib.getS("Courbe:") + EbliLib.getS("Ordonn�es modifiables");
        }
      } else {
        txt = EbliLib.getS("Courbe:") + EbliLib.getS("Non modifiable");
      }
      lbCourbeInfo_.setText(txt);
    }
    t_.revalidate();
    revalidate();
    doLayout();
    repaint();
  }

  protected void tableCopy() {
    t_.copy();
  }

  protected void tableCut() {
    t_.cut();
  }

  protected void tablePaste() {
    t_.paste();
  }

  public void tableImport() {
    new EGCsvFileImporter(this).importTablerFile();
  }

  public EbliActionSimple getActionDelete() {
    if (actionDelete == null) {
      actionDelete = new DeleteAction();
    }
    return actionDelete;
  }

  public void setAddPasteButtons(boolean addCopyButtons) {
    this.addPasteButtons = addCopyButtons;
  }

  public BuToolButton getBtCopy() {
    return btCopy_;
  }

  public BuToolButton getBtImport() {
    return btImport_;
  }

  public boolean isAddTopButtons() {
    return addTopButtons;
  }

  public void setAddTopButtons(boolean addTopButtons) {
    this.addTopButtons = addTopButtons;
  }

  public void addPanelAction(final EGFillePanel _p) {
    // si le contenu n'est pas modifiable ou si les boutons ont deja ete
    // initialise on oublie ....
    if (!a_.getModel().isContentModifiable() || btAdd_ != null) {
      return;
    }
    btAdd_ = new BuToolButton();
    btAdd_.setIcon(EbliResource.EBLI.getToolIcon("node-add"));
    final Insets zeroInset = BuInsets.INSETS0000;
    btAdd_.setMargin(zeroInset);
    btAdd_.setActionCommand("ADD_POINTS");
    btAdd_.setToolTipText(EbliLib.getS("Raffiner") + " (" + EbliLib.getS("Ajouter des points") + ')');
    btAdd_.addActionListener(_p);
    btCopy_ = new BuToolButton();
    btCopy_.setIcon(EbliResource.EBLI.getToolIcon("crystal_coller"));
    btCopy_.setMargin(zeroInset);
    btCopy_.setActionCommand("COPY");
    btCopy_.setToolTipText(EbliLib.getS("Coller une s�rie de valeurs depuis le presse-papier"));
    btCopy_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        EGTableGraphePanel.this.tablePaste();
      }
    });

    btImport_ = new BuToolButton();
    btImport_.setIcon(EbliResource.EBLI.getToolIcon("crystal_importer"));
    btImport_.setMargin(zeroInset);
    btImport_.setActionCommand("IMPORT");
    btImport_.setToolTipText("<html>" + EbliLib.getS("Importer depuis un fichier Excel/csv") + "<br><b>" + EbliLib.getS(
        "Toutes les valeurs actuelles seront remplac�es"));
    btImport_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        EGTableGraphePanel.this.tableImport();
      }
    });

    btRemove_ = new BuToolButton();
    btRemove_.setMargin(zeroInset);
    btRemove_.setIcon(EbliResource.EBLI.getToolIcon("node-delete"));
    btRemove_.setActionCommand("REMOVE_POINTS");
    final KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK);
    btRemove_.setToolTipText(EbliLib.getS("Supprimer un point") + " ("
        + KeyEvent.getKeyModifiersText(ks.getModifiers()) + '+' + KeyEvent.getKeyText(ks.getKeyCode()) + ')');
    btRemove_.addActionListener(_p);
    btAlign_ = new BuToolButton();
    btAlign_.setMargin(zeroInset);
    btAlign_.setActionCommand("ALIGN_POINTS");
    btAlign_.setIcon(EbliResource.EBLI.getToolIcon("node-corner"));
    btAlign_.setToolTipText(EbliLib.getS("Aligner les points"));
    btAlign_.addActionListener(_p);
    updateButtons();
    final BuPanel north = new BuPanel();
    north.setLayout(new BuBorderLayout(2, 2, true, false));
    if (afficheNomCourbe_) {
      north.add(lb_, BuBorderLayout.NORTH);
    }
    if (addTopButtons || addPasteButtons) {
      final BuPanel pnBt = new BuPanel();
      pnBt.setLayout(new BuButtonLayout(1, SwingConstants.LEFT));
      if (addPasteButtons) {
        pnBt.add(btCopy_);
        pnBt.add(btImport_);
      }
      if (addTopButtons) {
        pnBt.add(btAdd_);
        pnBt.add(btAlign_);
        pnBt.add(btRemove_);
      }
      north.add(pnBt, BuBorderLayout.CENTER);
    }
    remove(lb_);
    add(north, BuBorderLayout.NORTH);
    final InputMap m = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    m.put(ks, "deleteLine");
    getActionMap().put("deleteLine", new AbstractAction() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        btRemove_.doClick();
      }
    });
  }

  public void removeEditButtons() {
    removeEditButtonsButCopy();
    removeButton(btCopy_);
    removeButton(btImport_);
  }

  public void removeEditButtonsButCopy() {
    removeButton(btAdd_);
    removeButton(btAlign_);
    removeButton(btRemove_);
  }

  public void setLabel(final BuLabel _lb) {
    lbXyInfo_ = _lb;
  }

  @Override
  public void axeAspectChanged(final EGAxe _c) {
  }

  @Override
  public void axeContentChanged(final EGAxe _c) {
    (getSpecTableModel()).fireTableChanged(new TableModelEvent(getSpecTableModel(), 0, getSpecTableModel().getRowCount() - 1,
        TableModelEvent.ALL_COLUMNS, TableModelEvent.UPDATE));
    repaint();
  }

  public SpecTableModel getSpecTableModel() {
    return (SpecTableModel) t_.getModel();
  }

  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil) {
  }

  @Override
  public void courbeContentChanged(final EGObject _c, final boolean _mustRestore) {
    eventReceived();
  }

  public void intervalAdded(final ListDataEvent _e) {
  }

  public void intervalRemoved(final ListDataEvent _e) {
  }

  /**
   * @param _a le nouveau graphe cible pour ce composant
   */
  public void setGraphe(final EGGraphe _a) {
    if (a_ == _a) {
      return;
    }
    getSpecTableModel().setGraphe(_a);
    if (a_ != null) {
      a_.getModel().removeSelectionListener(this);
      a_.getModel().removeModelListener(this);
    }
    a_ = _a;
    a_.getModel().addSelectionListener(this);
    a_.getModel().addModelListener(this);
    updateDatas(_a.getSelectedComponent());
    eventReceived();
  }

  public void updateState() {
    updateDatas(a_.getSelectedComponent());
    updateButtons();
    eventReceived();
  }

  /**
   * @param _s la selection a ecouter
   */
  public void setListenerToSelection(final EGInteractionSelection _s) {
    if (_s == selection_) {
      return;
    }
    if (selection_ != null && updater_ != null) {

      selection_.getSelection().removeListeSelectionListener(updater_);
      t_.getSelectionModel().removeListSelectionListener(updater_);
    }
    selection_ = _s;
    updater_ = new CtuluSelectionListTableModelUpdater(t_, selection_.getSelection()) {
      @Override
      protected void doAfterCtuluListSectionChanged() {
        updateButtons();
      }

      @Override
      protected void doAfterTableSectionChanged() {
        updateButtons();
        updateRowSelection();
      }
    };
  }

  protected void updateRowSelection() {
    final int[] rows = t_.getSelectedRows();
    if (rows != null) {
      for (int i = 0; i < rows.length; i++) {
        if (!getSpecTableModel().isPointDrawn(rows[i])) {
          t_.getSelectionModel().removeSelectionInterval(rows[i], rows[i]);
        }
      }
    }
  }

  /**
   * @param _t le nom de la colonne x du tableau
   */
  public void setNameForColX(final String _t) {
    t_.getColumnModel().getColumn(0).setHeaderValue(_t);
    revalidate();
  }

  /**
   * @param _t le nom de la colonne y du tableau
   */
  public void setNameForColY(final String _t) {
    t_.getColumnModel().getColumn(1).setHeaderValue(_t);
    revalidate();
  }

  @Override
  public void structureChanged() {
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    updateDatas(a_.getSelectedComponent());
    updateButtons();
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    final Object o = _e.getPath().getLastPathComponent();
    if (o instanceof EGCourbe) {
      updateDatas((EGCourbe) o);
    } else {
      updateDatas(null);
    }
    updateButtons();
  }

  public static class SpecTableModel extends EGTableModel {
    protected final EGTableGraphePanel graphePanel;
    Double newX_;
    Double newY_;
    boolean updating_;

    public SpecTableModel(EGTableGraphePanel graphePanel) {
      this.graphePanel = graphePanel;
    }

    @Override
    public void fireTableRowsDeleted(int firstRow, int lastRow) {
      newX_ = null;
      newY_ = null;
      super.fireTableRowsDeleted(firstRow, lastRow);
    }

    @Override
    protected void selectedCourbeChanged(final EGCourbe _a) {
      newX_ = null;
      newY_ = null;
      super.selectedCourbeChanged(_a);
    }

    @Override
    public Class getColumnClass(final int _columnIndex) {
      return Double.class;
    }

    @Override
    public int getRowCount() {
      final EGAxeHorizontal h = getH();
      if (c_ == null || h == null) {
        return 0;
      }
      if (c_.getModel().isXModifiable()) {
        return super.getRowCount() + 1;
      }
      return super.getRowCount();
    }

    public boolean isAddingValuesRow(int row) {
      return row == c_.getModel().getNbValues();
    }

    @Override
    public Object getValueAt(final int _rowIndex, final int _columnIndex) {
      if (c_ == null) {
        return CtuluLibString.ESPACE;
      }
      if (isAddingValuesRow(_rowIndex)) {
        if (_columnIndex == xColIndex) {
          return newX_ == null ? null : getX(newX_.doubleValue());
        }
        if (_columnIndex == yColIndex) {
          return newY_ == null ? null : getY(newY_.doubleValue());
        }
      }
      return super.getValueAt(_rowIndex, _columnIndex);
    }

    @Override
    public void setValueAt(final Object _value, final int _rowIndex, final int _columnIndex) {
      if (updating_ || c_ == null || _value == null || _value.toString().trim().length() == 0) {
        return;
      }
      if (_rowIndex == c_.getModel().getNbValues()) {
        try {

          final Double d = CtuluDoubleParser.parseValue(_value.toString());
          if (d == null) {
            return;
          }
          if (_columnIndex == xColIndex) {
            newX_ = d;
          } else if (_columnIndex == yColIndex) {
            newY_ = d;
          }
          if (newX_ != null && newY_ != null) {
            updating_ = true;
            final boolean r = c_.getModel().addValue(newX_.doubleValue(), newY_.doubleValue(), a_.getCmd());
            if (r) {
              fireTableRowsInserted(0, _rowIndex);
            }
            if (!r && graphePanel.lbXyInfo_ != null) {
              graphePanel.lbError_.setText(EbliLib.getS("Point non ajout� !"));
              new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                  graphePanel.lbError_.setText(CtuluLibString.ESPACE);
                }
              }, 3 * 1000);
            }
            newX_ = null;
            newY_ = null;
            updating_ = false;
          }
        } catch (final NumberFormatException e) {
          return;
        }
      } else {
        super.setValueAt(_value, _rowIndex, _columnIndex);
      }
      if (graphePanel.t_.getCellEditor() != null) {
        graphePanel.t_.getCellEditor().cancelCellEditing();
      }
    }
  }

  /**
   * Classe speciale pour les evolution.
   *
   * @author Fred Deniger
   * @version $Id: EGTableGraphePanel.java,v 1.31 2007-06-05 08:58:39 deniger Exp $
   */
  public static class EvolTable extends CtuluTable {
    Action deleteAction;
    private boolean canEdit_;

    public EvolTable() {
      super();
      setTransferHandler(new EgCourbeTransfertHandler());
    }

    /**
     * @param _values
     * @param _names
     */
    public EvolTable(final Object[][] _values, final Object[] _names) {
      super(_values, _names);
      setTransferHandler(new EgCourbeTransfertHandler());
    }

    /**
     * @param _model
     */
    public EvolTable(final TableModel _model) {
      super(_model);
      setTransferHandler(new EgCourbeTransfertHandler());
    }

    @Override
    protected String transformToString(Object value) {

      return super.transformToString(value);
    }

    void setDeleteAction(Action deleteAction) {
      this.deleteAction = deleteAction;
    }

    protected void delete() {
      deleteAction.actionPerformed(null);
    }

    @Override
    public TransferHandler getTransferHandler() {
      return super.getTransferHandler();
    }

    public EGTableModel getEGTableModel() {
      return (EGTableModel) super.getModel();
    }

    /**
     * @param _tab ArrayList
     */
    @Override
    protected void insertTableInTable(final ArrayList _tab) {
      final EGTableModel model = getEGTableModel();
      if (!model.isModelModifiable()) {
        return;
      }
      int selectedColumm = getSelectedColumn();
      int selectedRow = getSelectedRow();
      final CtuluDoubleParser doubleParser = new CtuluDoubleParser();
      if (selectedColumm <= 0 && getModel() instanceof CtuluTableExportInterface) {
        int modelIndex = getColumnModel().getColumn(0).getModelIndex();
        if (!((CtuluTableExportInterface) getModel()).isColumnExportable(modelIndex)) {
          selectedColumm = 1;
        }
      }

      final CtuluCommandComposite cmp = new CtuluCommandComposite();
      if (selectedRow >= 0) {
        int maxUpdate = updateLines(_tab, model, selectedColumm, selectedRow, doubleParser, cmp);
        if (model.isModelXModifiable()) {
          addValuesInModel(_tab, model, doubleParser, cmp, maxUpdate);
        }
      } else if (model.isModelXModifiable()) {
        addValuesInModel(_tab, model, doubleParser, cmp, 0);
      }
      model.a_.getCmd().addCmd(cmp.getSimplify());
    }

    public void cut() {
      // vide on fait rien
      if (getSelectionModel().isSelectionEmpty()) {
        return;
      }
      final EGTableModel model = (EGTableModel) getModel();
      if (!model.isModelXModifiable()) {
        return;
      }
      copy();
      final int[] row = getSelectedRows();
      // on ne peut pas tout enlever
      if (row.length < model.getRowCount()) {
        model.c_.getModel().removeValue(row, model.a_.getCmd());
      }
    }

    public void deleteAllValues() {
      final EGTableModel model = (EGTableModel) getModel();
      if (!model.isModelXModifiable()) {
        return;
      }
      final int[] row = new int[model.getRowCount() - 1];
      for (int i = 0; i < row.length; i++) {
        row[i] = i;
      }
      // on ne peut pas tout enlever
      model.c_.getModel().removeValue(row, model.a_.getCmd());
    }

    private void addValuesInModel(final List _tab, final EGTableModel _model, final CtuluDoubleParser _doubleParser,
                                  final CtuluCommandComposite _cmp, final int _maxUpdate) {
      if (_model instanceof EGTableModelUpdatable) {
        ((EGTableModelUpdatable) _model).addValuesInModel(_tab, _doubleParser, _cmp, _maxUpdate);
        return;
      }
      final int nbLine = _tab.size();
      final TDoubleArrayList newX = new TDoubleArrayList(nbLine - _maxUpdate + 1);
      final TDoubleArrayList newY = new TDoubleArrayList(nbLine - _maxUpdate + 1);
      for (int i = _maxUpdate; i < nbLine; i++) {
        final List listCell = (List) _tab.get(i);
        if (listCell.size() >= 2) {
          Object o = listCell.get(0);
          boolean ok = false;
          double x = 0;
          double y = 0;
          if (o instanceof Number) {
            x = ((Number) o).doubleValue();
            ok = true;
          } else if (o != null) {
            try {
              x = _doubleParser.parse(o.toString());
              ok = true;
            } catch (final NumberFormatException e) {
              ok = false;
            }
          }
          if (ok) {
            o = listCell.get(1);
            if (o instanceof Number) {
              y = ((Number) o).doubleValue();
              ok = true;
            } else if (o != null) {
              try {
                y = _doubleParser.parse(o.toString());
                ok = true;
              } catch (final NumberFormatException e) {
                ok = false;
              }
            }
          }
          if (ok) {
            newX.add(x);
            newY.add(y);
          }
        }
      }
      if (newX.size() > 0) {
        _model.addValueAt(newX.toNativeArray(), newY.toNativeArray(), _cmp);
      }
    }

    private int updateLines(final List _tab, final EGTableModel _model, final int _selectedColumm,
                            final int _selectedRow, final CtuluDoubleParser _doubleParser, final CtuluCommandComposite _cmp) {
      if (_model instanceof EGTableModelUpdatable) {
        return ((EGTableModelUpdatable) _model).updateLines(_tab, _selectedColumm, _selectedRow, _doubleParser, _cmp);
      }
      final int nbLine = _tab.size();
      final int maxUpdate = Math.min(nbLine, _model.c_.getModel().getNbValues() - _selectedRow);
      for (int i = 0; i < maxUpdate; i++) {
        final List listCell = (List) _tab.get(i);
        final int max = Math.min(2, listCell.size());
        for (int j = 0; j < max; j++) {
          final int iCellule = i + _selectedRow;
          final int jCellule = j + _selectedColumm;
          if (jCellule <= 1) {
            final Object o = listCell.get(j);
            boolean ok = false;
            double d = 0;
            if (o instanceof Number) {
              d = ((Number) o).doubleValue();
              ok = true;
            } else if (o != null) {
              try {
                d = _doubleParser.parse(o.toString());
                ok = true;
              } catch (final NumberFormatException e) {
                ok = false;
              }
            }
            if (ok) {
              _model.setValueAt(d, iCellule, jCellule, _cmp);
            }
          }
        }
      }
      return maxUpdate;
    }

    @Override
    protected boolean processKeyBinding(final KeyStroke _ks, final KeyEvent _e, final int _condition,
                                        final boolean _pressed) {
      Object key = getInputMap(_condition).get(_ks);
      if (key != null) {
        Action action = getActionMap().get(key);
        if (action != null) {
          return SwingUtilities.notifyAction(action, _ks, _e, this,
              _e.getModifiers());
        }
      }
      final int keyCode = _ks.getKeyCode();
      if (!isEditing() && (keyCode >= KeyEvent.VK_A && keyCode <= KeyEvent.VK_Z) && _pressed) {
        return false;
      }
      canEdit_ = !_e.isConsumed() && !isEditing()
          && ((keyCode == KeyEvent.VK_SPACE) || (keyCode >= KeyEvent.VK_0 && keyCode <= KeyEvent.VK_9));
      if (canEdit_) {
        if (keyCode == KeyEvent.VK_SPACE) {
          _e.consume();
        }
        final int anchorRow = getSelectionModel().getAnchorSelectionIndex();
        final int anchorColumn = getColumnModel().getSelectionModel().getAnchorSelectionIndex();
        if (anchorRow != -1 && anchorColumn != -1) {
          if (!editCellAt(anchorRow, anchorColumn)) {
            return false;
          }
          getEditorComponent().requestFocus();
        }
        return true;
      }
      return super.processKeyBinding(_ks, _e, _condition, _pressed);
    }

    @Override
    public boolean editCellAt(final int _row, final int _column) {
      if (canEdit_) {
        canEdit_ = false;
        return super.editCellAt(_row, _column, null);
      }
      return false;
    }
  }

  public class DeleteAction extends EbliActionSimple {
    public DeleteAction() {
      super(EbliLib.getS("Supprimer des points"), EbliResource.EBLI.getToolIcon("node-delete"), "REMOVE_POINTS");
      putValue(ACCELERATOR_KEY, getDeleteKeyStroke());
    }

    @Override
    public void updateStateBeforeShow() {
      setEnabled(btRemove_.isEnabled());
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      btRemove_.doClick();
    }
  }
}
