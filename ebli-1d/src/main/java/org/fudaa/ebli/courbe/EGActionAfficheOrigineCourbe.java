package org.fudaa.ebli.courbe;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Action qui utilise les methodes viewGenerationSource() des modeles de courbes. Permet pour les modeles qui surcharge la methode
 * d'afficher dans l'implementation fudaa voulue l'origine de la création de la courbe.
 *
 * @author Adrien Hadoux
 *
 */
@SuppressWarnings("serial")
public class EGActionAfficheOrigineCourbe extends EbliActionSimple implements EGSelectionListener {

  /**
   * Action utilisée uniquement pour les courbes unitaires (accessible via clic droit sur la courbe).
   *
   * @author Adrien Hadoux
   *
   */
  public static class CourbeOnly extends EGActionAfficheOrigineCourbe {

    final EGModel model_;

    public CourbeOnly(EGModel model, CtuluUI _impl) {
      super();
      model_ = model;
      impl_ = _impl;
      setEnabled(model != null && model.isGenerationSourceVisible());
    }

    @Override
    public void updateStateBeforeShow() {
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      if (model_ != null) {
        model_.viewGenerationSource(new HashMap(), impl_);
      }
    }
  }
  EGFillePanel panelGraphe_;
  CtuluUI impl_;

  public EGActionAfficheOrigineCourbe(EGFillePanel _panel, CtuluUI _impl) {
    this();
    panelGraphe_ = _panel;
    impl_ = _impl;
    panelGraphe_.getModel().addSelectionListener(this);
    setEnabled(false);
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    updateEnableState();
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    updateEnableState();
  }

  public EGActionAfficheOrigineCourbe() {
    super(EbliResource.EBLI.getString("Origine"), CtuluResource.CTULU.getIcon("crystal_visibilite"), "ORIGINE");
  }

  protected void updateEnableState() {
    final EGCourbe selectedComponent = panelGraphe_.getGraphe().getSelectedComponent();
    boolean enable = selectedComponent != null && selectedComponent.getModel() != null && selectedComponent.getModel().isGenerationSourceVisible();
    setEnabled(enable);
  }

  @Override
  public void updateStateBeforeShow() {
    updateEnableState();
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    updateEnableState();
    if (!isEnabled()) {
      impl_.error(EbliResource.EBLI.getString("Aucune courbe sélectionnée"), EbliResource.EBLI.getString("Aucune courbe sélectionnée"), true);
      return;
    }
    EGModel modelSelectionne = panelGraphe_.getGraphe().getSelectedComponent().getModel();
    modelSelectionne.viewGenerationSource(new HashMap(), impl_);


  }
}
