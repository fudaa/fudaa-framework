/*
 GPL 2
 */
package org.fudaa.ebli.courbe;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFileChooser;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFileChooserCsvExcel;
import org.fudaa.ctulu.table.CtuluExcelCsvFileReader;

/**
 *
 * @author Frederic Deniger
 */
public class EGCsvFileImporter {

  private final EGTableGraphePanel panel;

  EGCsvFileImporter(EGTableGraphePanel aThis) {
    this.panel = aThis;
  }

  void importTablerFile() {
    final CtuluFileChooserCsvExcel choose = CtuluFileChooserCsvExcel.createOpenFileChooser(panel);
    choose.setDialogType(JFileChooser.OPEN_DIALOG);
    choose.setTester(null);
    final File f = choose.getDestFile();
    if (f == null) {
      return;
    }
    final CtuluExcelCsvFileReader ctuluExcelCsvFileReader = new CtuluExcelCsvFileReader(f);
    final String[][] values = ctuluExcelCsvFileReader.readFile();
    if (values == null) {
      return;
    }
    boolean isCsv = !ctuluExcelCsvFileReader.isExcel();
    panel.t_.deleteAllValues();
    panel.t_.clearSelection();
    ArrayList tab = new ArrayList(values.length);
    for (int i = 0; i < values.length; i++) {
      String[] value = values[i];
      //ignore les commentaire
      if (isCsv && CtuluLibArray.isNotEmpty(value) && value[0] != null && value[0].startsWith("#")) {
        continue;
      }
      //la premier ligne peut �tre les entetes
      if (i == 0) {
        if (!CtuluLibString.isNumeric(value[0])) {
          continue;
        }
      }
      tab.add(Arrays.asList(value));
    }
    panel.t_.insertTableInTable(tab);
  }

}
