/*
 * @creation 29 nov. 06
 * @modification $Date: 2007-05-22 14:19:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPalette;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Color;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.*;
import org.fudaa.ebli.courbe.EGCourbeSurfacePainter.SelectedIndex;
import org.fudaa.ebli.courbe.EGCourbeSurfacePainter.SelectedIndexForX;

/**
 * @author fred deniger
 * @version $Id: EGCourbeSurfacePainterConfigure.java,v 1.6 2007-05-22 14:19:04 deniger Exp $
 */
public class EGCourbeSurfacePainterConfigure implements /*BConfigurableSectionInterface, */BSelecteurTargetInterface,
    BConfigurableInterface {

  public static class SelecteurType extends BSelecteurAbstract implements ItemListener {
    final BuComboBox cb_;
    JComponent cp_;
    final JPanel pn_;
    final EGCourbeSurfacePainterConfigure painter_;

    public SelecteurType(final EGCourbeSurfacePainterConfigure _painter) {
      super(SURFACE_TYPE);
      painter_ = _painter;
      pn_ = new BuPanel();
      setTitle(EbliLib.getS("Type"));
      cb_ = new BuComboBox(EGCourbeSurfacePainter.getTypeModel());
      cb_.addItemListener(this);
      cp_ = new JLabel(CtuluLibString.ESPACE);
      pn_.setLayout(new BuVerticalLayout());
      pn_.add(cb_);
      pn_.add(cp_);
    }

    private void updateInternComponents() {
      final int i = cb_.getSelectedIndex();
      pn_.remove(cp_);
      if (i <= 2) {
        cp_ = new BuLabel(CtuluLibString.ESPACE);
      }
      if (i == 3) {
        final BSelecteurTextField selecteur = new BSelecteurTextField(SURFACE_PALIER);
        // on suppose que seul ce composant modifie cette valeur
        selecteur.setAddListenerToTarget(false);
        selecteur.setSelecteurTarget(target_);
        cp_ = selecteur.getComponents()[0];
      }
      if (i == 4) {
        final JComboBox cb = getSurfaceTarget().getModel()
            .createCbForSelectCourbe(getSurfaceTarget().targetCourbe_.getAxeY());
        cb.setSelectedItem(getSurfaceTarget().getDelegateCourbe());
        cb.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(final ItemEvent _e) {
            if (_e.getStateChange() == ItemEvent.DESELECTED) {
              return;
            }
            final Object[] o = _e.getItemSelectable().getSelectedObjects();
            if (!CtuluLibArray.isEmpty(o) && o.length == 1) {
              getSurfaceTarget().updateCourbe((EGCourbe) o[0]);
            }

          }

        });
        cp_ = cb;

      }
      pn_.add(cp_);
      pn_.doLayout();
      final BuPalette parent = (BuPalette) SwingUtilities.getAncestorOfClass(BuPalette.class, pn_);
      if (parent == null) {
        final Window d = (Window) SwingUtilities.getAncestorOfClass(Window.class, pn_);
        if (d != null) {
          d.pack();
        }
      } else {
        parent.pack();
      }

    }

    @Override
    public JComponent[] getComponents() {
      return new JComponent[] { pn_ };
    }

    public EGCourbeSurfacePainter getSurfaceTarget() {
      return painter_.target_;
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      if (isUpdating_ || _e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      if (_e.getSource() == cb_) {
        firePropertyChange(getProperty(), new Integer(cb_.getSelectedIndex()));
      }
      updateInternComponents();
    }

    @Override
    public void updateFromTarget() {
      final int i = getTargetIntValue();
      cb_.setSelectedIndex(i);
      updateInternComponents();
    }
  }

  protected static abstract class SelecteurChooser {

    abstract SelectedIndex createSelecteur(EGCourbeSurfacePainter _target);

    public int isIdxChooserEnable(final EGCourbeSurfacePainter _target) {
      return -1;
    }

    public abstract boolean isSelected(EGCourbeSurfacePainter _target);

    @Override
    public abstract String toString();
  }

  protected static class MaxIdxSelecteurChooser extends SelecteurChooser {

    @Override
    SelectedIndex createSelecteur(final EGCourbeSurfacePainter _target) {
      return EGCourbeSurfacePainter.createSelectedForMax(_target.targetCourbe_.getModel().getNbValues() - 1);
    }

    @Override
    public boolean isSelected(final EGCourbeSurfacePainter _target) {
      final SelectedIndex idx = _target.getMaxIdx();
      return idx != EGCourbeSurfacePainter.NONE && idx != EGCourbeSurfacePainter.TIME_FOR_MAX;
    }

    @Override
    public int isIdxChooserEnable(final EGCourbeSurfacePainter _target) {
      if (isSelected(_target)) {
        return ((SelectedIndexForX) _target.getMaxIdx()).getIdx();
      }
      return _target.targetCourbe_.getModel().getNbValues() - 1;
    }

    @Override
    public String toString() {
      return EbliLib.getS("Remplir si inf�rieur �");
    }

  }
  protected static class MinIdxSelecteurChooser extends SelecteurChooser {

    @Override
    SelectedIndex createSelecteur(final EGCourbeSurfacePainter _target) {
      return EGCourbeSurfacePainter.createSelectedForMin(0);
    }

    @Override
    public int isIdxChooserEnable(final EGCourbeSurfacePainter _target) {
      if (isSelected(_target)) {
        return ((SelectedIndexForX) _target.getMinIdx()).getIdx();
      }
      return _target.targetCourbe_.getModel().getNbValues() - 1;
    }

    @Override
    public boolean isSelected(final EGCourbeSurfacePainter _target) {
      final SelectedIndex idx = _target.getMinIdx();
      return idx != EGCourbeSurfacePainter.NONE && idx != EGCourbeSurfacePainter.TIME_FOR_MIN;
    }

    @Override
    public String toString() {
      return EbliLib.getS("Remplir si sup�rieur �");
    }

  }

  protected static class DefaultSelecteurChooser extends SelecteurChooser {
    final String desc_;
    final SelectedIndex index_;
    final boolean isMax_;

    public DefaultSelecteurChooser(final String _desc, final SelectedIndex _index, final boolean _isMax) {
      super();
      desc_ = _desc;
      index_ = _index;
      isMax_ = _isMax;
    }

    @Override
    public boolean isSelected(final EGCourbeSurfacePainter _target) {
      final SelectedIndex selected = isMax_ ? _target.getMaxIdx() : _target.getMinIdx();
      return selected == index_;
    }

    @Override
    SelectedIndex createSelecteur(final EGCourbeSurfacePainter _target) {
      return index_;
    }

    @Override
    public String toString() {
      return desc_;
    }
  }

  protected final static SelecteurChooser NONE_SELECTEUR_MAX = new DefaultSelecteurChooser(EbliLib.getS("Non activ�"),
      EGCourbeSurfacePainter.NONE, true);
  protected final static SelecteurChooser NONE_SELECTEUR_MIN = new DefaultSelecteurChooser(EbliLib.getS("Non activ�"),
      EGCourbeSurfacePainter.NONE, false);

  protected final static SelecteurChooser TIME_FOR_MAX = new DefaultSelecteurChooser(EbliLib
      .getS("Remplir si inf�rieur au pas de temps actif"), EGCourbeSurfacePainter.TIME_FOR_MAX, true);
  protected final static SelecteurChooser TIME_FOR_MIN = new DefaultSelecteurChooser(EbliLib
      .getS("Remplir si sup�rieur au pas de temps actif"), EGCourbeSurfacePainter.TIME_FOR_MIN, false);

  public static class BorneSelecteur extends BSelecteurAbstract implements ItemListener {

    // final JPanel pn_;
    final BuComboBox cbType_;
    final BuComboBox cbIdx_;
    final boolean isMax_;
    final EGCourbeSurfacePainterConfigure painter_;
    final JPanel pn_;

    /**
     * 
     */
    public BorneSelecteur(final EGCourbeSurfacePainterConfigure _painter, final String _prop, final SelecteurChooser[] _selecteur,
        final EGCourbe _c, final boolean _max) {
      super(_prop);
      painter_ = _painter;
      setTitle(_max ? EbliLib.getS("Borne verticale max") : EbliLib.getS("Borne verticale min"));
      isMax_ = _max;
      // pn_ = new BuPanel();
      // pn_.setLayout(new BuGridLayout(2, 2, 2));
      cbType_ = new BuComboBox(_selecteur);
      // pn_.add(cbType_);
      cbIdx_ = new BuComboBox(new EGComboBoxModelForX(_c));
      // pn_.add(cbIdx_);
      cbType_.addItemListener(this);
      cbIdx_.addItemListener(this);
      pn_ = new BuPanel(new BuVerticalLayout(1));
      pn_.add(cbType_);
      pn_.add(cbIdx_);
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      if (isUpdating_) {
        return;
      }
      if (_e.getSource() == cbType_) {
        final SelecteurChooser selectedItem = (SelecteurChooser) cbType_.getSelectedItem();
        final SelectedIndex selected = (selectedItem).createSelecteur(getSurfaceTarget());
        if (isMax_) {
          getSurfaceTarget().setMaxIdx(selected);
        } else {
          getSurfaceTarget().setMinIdx(selected);
        }
        final int idx = selectedItem.isIdxChooserEnable(getSurfaceTarget());
        cbIdx_.setEnabled(idx >= 0);
        if (idx >= 0) {
          cbIdx_.setSelectedIndex(idx);
        }
      } else if (_e.getSource() == cbIdx_) {
        final int idx = cbIdx_.getSelectedIndex();
        if (isMax_) {
          getSurfaceTarget().setMaxIdx(EGCourbeSurfacePainter.createSelectedForMax(idx));
        } else {
          getSurfaceTarget().setMinIdx(EGCourbeSurfacePainter.createSelectedForMin(idx));
        }
      }

    }

    public EGCourbeSurfacePainter getSurfaceTarget() {
      return painter_.target_;
    }

    @Override
    public void updateFromTarget() {

      final EGCourbeSurfacePainter painter = getSurfaceTarget();
      final boolean enable = painter.getSelectedPainter() != 0;
      cbIdx_.setEnabled(enable);
      cbType_.setEnabled(enable);
      for (int i = 0; i < cbType_.getModel().getSize(); i++) {
        final SelecteurChooser selecteur = (SelecteurChooser) cbType_.getModel().getElementAt(i);
        if (selecteur.isSelected(painter)) {
          cbType_.setSelectedItem(selecteur);
          final int idx = selecteur.isIdxChooserEnable(painter);
          cbIdx_.setEnabled(enable && idx >= 0);
          if (idx >= 0) {
            cbIdx_.setSelectedIndex(idx);
          }
        }

      }

    }

    @Override
    public JComponent[] getComponents() {
      return new JComponent[] { pn_ };
    }

  }

  public static final String BOTTOM_COLOR = "surfaceBottomColor";

  public static final String COMMON_COLOR = "surfaceCommonColor";

  public static final String SURFACE_ALPHA = "surfaceAlpha";

  public static final String SURFACE_COURBE = "surfaceCourbe";
  public static final String SURFACE_PALIER = "surfacePalier";
  public static final String SURFACE_TYPE = "surfaceType";
  public static final String TOP_COLOR = "surfaceTopColor";
  final EGCourbeSurfacePainter target_;

  public EGCourbeSurfacePainterConfigure(final EGCourbeSurfacePainter _target) {
    super();
    target_ = _target;
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.targetCourbe_.addListener(_key, _l);

  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final List selecteur = new ArrayList();
    selecteur.add(new SelecteurType(this));
    final boolean isTime = target_.targetCourbe_.isActiveTimeEnable();
    final SelecteurChooser[] minSelecteurs = isTime ? new SelecteurChooser[] { NONE_SELECTEUR_MIN,
        new MinIdxSelecteurChooser(), TIME_FOR_MIN } : new SelecteurChooser[] { NONE_SELECTEUR_MIN,
        new MinIdxSelecteurChooser() };
    final SelecteurChooser[] maxSelecteurs = isTime ? new SelecteurChooser[] { NONE_SELECTEUR_MAX,
        new MaxIdxSelecteurChooser(), TIME_FOR_MAX } : new SelecteurChooser[] { NONE_SELECTEUR_MAX,
        new MaxIdxSelecteurChooser() };
    selecteur.add(new BorneSelecteur(this, EGCourbeSurfacePainter.PROPERTY_MIN_IDX, minSelecteurs,
        target_.targetCourbe_, false));
    selecteur.add(new BorneSelecteur(this, EGCourbeSurfacePainter.PROPERTY_MAX_IDX, maxSelecteurs,
        target_.targetCourbe_, true));
    selecteur.add(new BSelecteurColorChooserBt(COMMON_COLOR));
    BSelecteurColorChooserBt color = new BSelecteurColorChooserBt(BOTTOM_COLOR);
    color.setTitle(EbliLib.getS("Couleur si sup�rieur"));
    selecteur.add(color);
    color = new BSelecteurColorChooserBt(TOP_COLOR);
    color.setTitle(EbliLib.getS("Couleur si inf�rieur"));
    selecteur.add(color);
    selecteur.add(new BSelecteurAlpha(SURFACE_ALPHA));

    return (BSelecteurInterface[]) selecteur.toArray(new BSelecteurInterface[0]);
  }

  @Override
  public Object getMin(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(final String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getProperty(final String _key) {
    if (_key == COMMON_COLOR) {
      final Color c = target_.getUp();
      return (c == target_.getDown()) ? c : null;
    }
    if (_key == TOP_COLOR) {
      return target_.getUp();
    }
    if (_key == BOTTOM_COLOR) {
      return target_.getDown();
    }
    if (_key == SURFACE_ALPHA) {
      return new Integer(target_.getAlpha());
    }
    if (_key == SURFACE_TYPE) {
      return new Integer(target_.getSelectedPainter());
    }
    if (_key == SURFACE_COURBE) {
      return target_.getDelegateCourbe();
    }
    if (_key == SURFACE_PALIER) {
      return target_.getPalier();
    }
    if (_key == EGCourbeSurfacePainter.PROPERTY_MIN_IDX) {
      return target_;
    }
    if (_key == EGCourbeSurfacePainter.PROPERTY_MAX_IDX) {
      return target_;

    }
    return null;
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return this;
  }

  @Override
  public String getTitle() {
    return EbliLib.getS("Arri�re-plan");
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    target_.targetCourbe_.removeListener(_key, _l);

  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    if (_key == COMMON_COLOR) {
      //un seul  | normal !
      return target_.setUp((Color) _newProp) | target_.setDown((Color) _newProp);
    }
    if (_key == BOTTOM_COLOR) {
      return target_.setDown((Color) _newProp);
    }
    if (_key == TOP_COLOR) {
      return target_.setUp((Color) _newProp);
    }
    if (_key == SURFACE_ALPHA) {
      return target_.setAlpha(((Integer) _newProp).intValue());
    }
    if (_key == SURFACE_TYPE) {
      return target_.setSelectedPainter(((Integer) _newProp).intValue());
    }
    if (_key == SURFACE_COURBE) {

      return target_.updateCourbe((EGCourbe) _newProp);
    }
    if (_key == SURFACE_PALIER) {
      return target_.setPalier((Double) _newProp);
    }
    if (_key == EGCourbeSurfacePainter.PROPERTY_MIN_IDX) {
      return target_.setMinIdx((SelectedIndex) _newProp);
    }
    if (_key == EGCourbeSurfacePainter.PROPERTY_MAX_IDX) {
      return target_.setMaxIdx((SelectedIndex) _newProp);
    }
    return false;
  }

  @Override
  public void stopConfiguration() {}

}
