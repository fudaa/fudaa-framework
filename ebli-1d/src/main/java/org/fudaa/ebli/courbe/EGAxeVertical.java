/*
 * @creation 21 juin 2004
 * 
 * @modification $Date: 2007-05-04 13:49:41 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.iterator.TickIterator;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author Fred Deniger
 * @version $Id: EGAxeVertical.java,v 1.22 2007-05-04 13:49:41 deniger Exp $
 */
public class EGAxeVertical extends EGAxe {

  public static final String PROP_TITLE_VERTICAL_DROITE = "axeTitleVerticalDroite";
  public static final String PROP_DROITE = "axeTitleDroite";
  public static final String PROP_TITLE_VERTICAL = "axeTitleVertical";
  boolean droite_;
  boolean titreVertical_;
  boolean titreVerticalDroite_;

  public EGAxeVertical() {
    this(null, null, null);
  }

  public EGAxeVertical(final String _title, final String _unit) {
    this(null, _title, _unit);
  }

  public EGAxeVertical(final EGAxe _a, final String _title, final String _unit) {
    super(_a);
    setTitre(_title);
    setUnite(_unit);
    setSpecificFormat(CtuluLib.DEFAULT_NUMBER_FORMAT);
  }

  public EGAxeVertical(final EGAxe _a) {
    this(_a, null, null);
  }

  /**
   * Ajuste les bornes de cet axe pour prendre en compte les valeurs de l'objet _o.
   *
   * @param _o l'objet a prendre en compte
   */
  public void ajusteFor(final EGObject _o) {
    final CtuluRange r = new CtuluRange();
    _o.ajusteY(r);
    if (!range_.isNill()) {
      r.expandTo(range_);
    }
    setBounds(r.min_, r.max_);
  }

  public EGAxeVertical(final EGAxeVertical _v) {
    this(_v, null, null);
    if (_v != null) {
      droite_ = _v.droite_;
      titreVertical_ = _v.titreVertical_;
      titreVerticalDroite_ = _v.titreVerticalDroite_;
      setTitre(_v.getTitre());
      setUnite(_v.getUnite());
    }
  }

  private int dessineGraduation(final Graphics2D _g2d, final EGRepere _t, final FontMetrics _fm, final int _x,
          final boolean _grille) {

    int maxStringwidth = 0;
    if (graduations_ || _grille) {
      final double asc = _fm.getDescent();
      final double fontHeight = _fm.getDescent() + _fm.getAscent();
      final double xLeft = droite_ ? (_x + 3) : (_x - 3);
      final double xLeftMinor = droite_ ? (_x + 1) : (_x - 1);
      int xMaxGrad = _t.getMaxEcranX();
      if (droite_) {
        xMaxGrad = _t.getMinEcranX();
      }
      final TraceLigne traceGraduations = new TraceLigne(traceGraduations_);
//      traceGraduations.setCouleur(getLineColor());
      final TraceLigne traceSousGraduations = new TraceLigne(traceSousGraduations_);
      _g2d.setColor(lineColor_);
      boolean first = true;
      // stocke le max du dernier champ ecrit. Evite aux graduations de se chevaucher
      double maxLastIteration = Double.MAX_VALUE;
      _g2d.setColor(super.getLineColor());
      int tmp;
      final TickIterator iterator = buildUpToDateMainTickIterator();
      for (final TickIterator it = iterator; it.hasNext(); it.next()) {
        final double s = it.currentValue();
        String t;
        final int ye = _t.getYEcran(s, this);
        if (graduations_) {
          if (it.isMajorTick()) {
            if (traceGrille_) {
              traceGraduations.dessineTrait(_g2d, (int) xLeft, ye, xMaxGrad, ye);
            }
            _g2d.drawLine((int) xLeft, ye, _x, ye);
            addScreenXY(xLeft, ye);
            addScreenXY(_x, ye);
            if (specificFormat_ == null) {
              t = it.currentLabel();
            } else {
              t = specificFormat_.format(it.currentValue());
            }
            tmp = (int) (droite_ ? xLeft + 4 : (xLeft - _fm.stringWidth(t) - 1));
            if (first) {
              maxStringwidth = _fm.stringWidth(t);
              // le premier est dessine en haut pour eviter de chevaucher avec l'axe horizontal
              _g2d.drawString(t, tmp, ye);
              maxLastIteration = ye - fontHeight;
              first = false;
            } else if (maxLastIteration >= ye) {
              final int stringWidth = _fm.stringWidth(t);
              if (stringWidth > maxStringwidth) {
                maxStringwidth = stringWidth;
              }

              _g2d.drawString(t, tmp, (int) (ye + asc));
              addScreenXY(tmp, (int) (ye + asc));
              addScreenXY(tmp + _fm.stringWidth(t), (int) (ye + asc));
              maxLastIteration = ye - fontHeight;
            }
          } else {
            if (traceSousGrille_) {
              traceSousGraduations.dessineTrait(_g2d, (int) xLeftMinor, ye, xMaxGrad, ye);
            }
            _g2d.drawLine((int) xLeftMinor, ye, _x, ye);
            addScreenXY(xLeftMinor, ye);
            addScreenXY(_x, ye);
          }

        }
      }
    }
    return maxStringwidth;
  }

  final boolean isTitreVerticalDroite() {
    return titreVerticalDroite_;
  }

  final boolean setTitreVerticalDroite(final boolean _titreVerticalDroite) {
    if (titreVerticalDroite_ != _titreVerticalDroite) {
      titreVerticalDroite_ = _titreVerticalDroite;
      firePropertyChange(PROP_TITLE_VERTICAL_DROITE, titreVerticalDroite_);

      return true;
    }
    return false;
  }

  public void dessine(final Graphics2D _g2d, final EGRepere _t, final int _xOffset) {
    if (FuLog.isDebug() && Fu.DEBUG) {
      FuLog.debug(getClass().getName() + " axe vertical begin ...");
    }
    clearXYScreen();
    if (!visible_) {
      return;
    }
    final Font old = _g2d.getFont();
    if (font_ != null) {
      _g2d.setFont(font_);
    }
    final FontMetrics fm = _g2d.getFontMetrics();
    String t;
    _g2d.setColor(lineColor_);
    int x = 0;
    if (droite_) {
      x = _t.getMaxEcranX() + _xOffset;
    } else {
      x = _t.getMinEcranX() - _xOffset;
    }
    final int y = _t.getMaxEcranY();
    final boolean grille = isGridPainted();
    final int maxStringwidth = dessineGraduation(_g2d, _t, fm, x, grille);
    t = getAxeTexte();
    int yf = _t.getMinEcranY();
    if (isExtremiteDessinee_) {
      yf -= 2;
    }
    _g2d.setColor(lineColor_);

    _g2d.drawLine(x, y, x, yf);
    addScreenXY(x, y);
    addScreenXY(x, yf);
    if (isExtremiteDessinee_) {
      _g2d.drawLine(x, yf, x - 2, yf + 2);
      _g2d.drawLine(x, yf, x + 2, yf + 2);
      addScreenXY(x - 2, yf + 2);
      addScreenXY(x + 2, yf + 2);
    }
    if (t != null) {
      if (titreVertical_) {
        final AffineTransform save = _g2d.getTransform();
        _g2d.rotate(-Math.PI / 2, x, yf);
        int xTxt = x - fm.stringWidth(t);
        if (isTitreCentre_) {
          xTxt = xTxt - (_t.getMaxEcranY() - _t.getMinEcranY()) / 2 + fm.stringWidth(t) / 2;
        }
        if (titreVerticalDroite_) {
          if (droite_) {
            _g2d.drawString(t, xTxt, yf + fm.getAscent() + maxStringwidth + 4);
          } else {
            _g2d.drawString(t, xTxt, yf + fm.getAscent());
          }
        } else {
          if (droite_) {
            _g2d.drawString(t, xTxt, yf - fm.getDescent());
          } else {
            _g2d.drawString(t, xTxt, yf - fm.getDescent() - maxStringwidth - 1);
          }
        }

        _g2d.setTransform(save);
      } else {
        _g2d.drawString(t, x + 1 - fm.stringWidth(t) / 2, yf - fm.getAscent());
        addScreenXY(x + 1 - fm.stringWidth(t) / 2, yf - fm.getHeight() - fm.getAscent());
        addScreenXY(x + 1 + fm.stringWidth(t) / 2, yf);
      }
      _g2d.setFont(old);
    }
    if (FuLog.isDebug() && Fu.DEBUG) {
      FuLog.debug(getClass().getName() + " axe vertical END");
    }
  }

  public int getHeightNeeded(final Graphics2D _g) {
    int r = 0;
    if (isExtremiteDessinee_) {
      r += 3;
    }
    _g.setFont(getFont());
    if ((isTitreVisible() || isUniteVisible()) && !titreVertical_) {
      return r
              + _g.getFontMetrics(getFont()).getHeight() + _g.getFontMetrics(getFont()).getAscent();
    }
    return r;
  }

  public int getWidthNeeded(final Graphics2D _g2d) {
    if (!visible_) {
      return 0;
    }
    // epaisseur trait
    int r = 3;
    if (_g2d == null) {
      return r;
    }
    if (font_ == null) {
      font_ = EGGraphe.DEFAULT_FONT;
    }
    final FontMetrics fm = _g2d.getFontMetrics(this.font_);
    if (graduations_ || isGridPainted()) {
      // la graduation
      r += 4;
      int maxChaine = 0;
      final TickIterator iterator = buildUpToDateMainTickIterator();
      for (final TickIterator it = iterator; it.hasNext(); it.next()) {
        if (it.isMajorTick()) {
          String t = null;
          if (specificFormat_ == null) {
            t = it.currentLabel();
          } else {
            t = specificFormat_.format(it.currentValue());
          }
          final int wt = fm.stringWidth(t);
          if (wt > maxChaine) {
            maxChaine = wt;
          }
        }
      }
      r += maxChaine;
    }
    if (isUniteVisible() || isTitreVisible()) {
      if (titreVertical_) {
        r += fm.getHeight() + 6;
      } else {
        final String axeTexte = getAxeTexte();
        final int wT = (fm.stringWidth(axeTexte) + 4) / 2;
        r = wT + Math.max(r, wT);
      }
    }
    return r;
  }

  /**
   * @param _g2d le graphics
   * @return la dimension de la partie dessine dans le graphe: en general la moiti� de la largeur du titre
   */
  public int getWidthDrawnInGraphe(final Graphics2D _g2d) {
    if (!visible_) {
      return 0;
    }
    // epaisseur trait
    final int r = 0;
    if (_g2d == null) {
      return r;
    }
    if (font_ == null) {
      font_ = EGGraphe.DEFAULT_FONT;
    }
    final FontMetrics fm = _g2d.getFontMetrics(this.font_);
    if (isUniteVisible() || isTitreVisible()) {
      if (titreVertical_) {
        if ((isTitreVerticalDroite() && !isDroite()) || (!isTitreVerticalDroite() && isDroite())) {
          return fm
                  .getHeight() + 6;
        }
      } else {
        final String axeTexte = getAxeTexte();
        return (fm.stringWidth(axeTexte) + 4) / 2;

      }
    }
    return 0;
  }

  public final boolean isDroite() {
    return droite_;
  }

  @Override
  public final boolean isVertical() {
    return true;
  }

  public final boolean setDroite(final boolean _droite) {
    if (droite_ != _droite) {
      droite_ = _droite;
      firePropertyChange(PROP_DROITE, droite_);
      return true;
    }
    return false;
  }

  public final boolean isTitreVertical() {
    return titreVertical_;
  }

  public final boolean setTitreVertical(final boolean _droite) {
    if (titreVertical_ != _droite) {
      titreVertical_ = _droite;
      firePropertyChange(PROP_TITLE_VERTICAL, titreVertical_);
      return true;
    }
    return false;
  }

  public EGAxeVertical duplicate() {
    final EGAxeVertical duplic = new EGAxeVertical(this);
    duplic.setFont(new Font(this.getFont().getFamily(), this.getFont().getStyle(), this.getFont().getSize()));
    duplic.setTitre(this.getTitre());

    duplic.graduations_ = graduations_;
    duplic.longueurPas_ = longueurPas_;
    duplic.setModeGraduations(getModeGraduations());
    duplic.nbPas_ = nbPas_;
    duplic.nbSousGraduations_ = nbSousGraduations_;
    // duplic.range_=new CtuluRange(range_);
    duplic.traceGraduations_ = new TraceLigneModel(traceGraduations_);
    duplic.traceSousGraduations_ = new TraceLigneModel(traceSousGraduations_);
    duplic.traceGrille_ = traceGrille_;
    duplic.traceSousGrille_ = traceSousGrille_;
    duplic.nbSousGraduations_ = nbSousGraduations_;
    return duplic;
  }
}
