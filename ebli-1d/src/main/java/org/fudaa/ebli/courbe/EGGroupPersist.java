package org.fudaa.ebli.courbe;


/**
 * Classe persistante xml du eggroup
 * 
 * @author Adrien Hadoux
 */
public final class EGGroupPersist extends EGPersist {

  EGAxeVerticalPersist axeY;
  int idx;

  boolean isVisible;

  String title;
   int groupId;
   
   
           
  public int getIdx() {
    return idx;
  }

  public boolean isRangeSet() {
    return axeY != null && axeY.range != null;
  }

  /**
   * @param idx the idx to set
   */
  protected void setIdx(int idx) {
    this.idx = idx;
  }

}
