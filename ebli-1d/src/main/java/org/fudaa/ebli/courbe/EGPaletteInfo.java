/*
 *  @creation     26 janv. 2005
 *  @modification $Date: 2007-05-04 13:49:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.courbe;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionEvent;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListSelectionEvent;
import org.fudaa.ctulu.CtuluListSelectionListener;
import org.fudaa.ebli.palette.BPaletteInfo;

/**
 * @author Fred Deniger
 * @version $Id: EGPaletteInfo.java,v 1.9 2007-05-04 13:49:42 deniger Exp $
 */
public class EGPaletteInfo extends BPaletteInfo implements CtuluListSelectionListener,
    EGGrapheModelListener, EGSelectionListener {

  final EGFillePanel p_;

  public EGPaletteInfo(final EGFillePanel _p, CtuluCommandManager _cmd) {
    super(_cmd);
    _p.addListenerCourbePoint(this);
    _p.getModel().addModelListener(this);
    _p.getModel().addSelectionListener(this);
    p_=_p;
    
  }


  @Override
  public void axeAspectChanged(final EGAxe _c){
    updateState();
  }
  @Override
  public void axeContentChanged(final EGAxe _c){
    updateState();
  }
  @Override
  public void courbeAspectChanged(final EGObject _c, final boolean _visibil){}
  @Override
  public void courbeContentChanged(final EGObject _c, final boolean _mustRestore){
    updateState();
  }
  @Override
  public void structureChanged(){
    updateState();
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e){
    updateState();
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e){
    updateState();
  }

  @Override
  public void listeSelectionChanged(final CtuluListSelectionEvent _e){
    updateState();
  }

  

  
  @Override
  public void updateState(){
    if (!isAvailable()) {
      return;
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug(getClass().getName() + " maj info");
    }
    final EGObject newLa = p_.getModel().getSelectedObject();
    tableModel_.clear();
    txtTitle_.setText(CtuluLibString.EMPTY_STRING);
    if (newLa != null) {
      newLa.fillWithInfo(tableModel_,p_.getSelection());
      p_.getGraphe().fillWithAxeXInfo(tableModel_);
      tableModel_.fireTableDataChanged();
      updateSize();
    }
  }
}
