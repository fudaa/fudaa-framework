#  Donn�es temporelles des sources
#  Les rivi�res, except� la SOMME sont d�crites ici
#  Q(1) = Rivi�re de Valmont
#  Q(2) = Durdent
#  Q(3) = Veules
#  Q(4) = Dun
#  Q(5) = Saane
#  Q(6) = Scie
#  Q(7) = Arques
#  Q(8) = Yeres
#  Q(9) = Bresles
#  Q(10)= Ault
#  Q(11)= Ex_EP_Veulettes
#  Q(12)= STEP_Veules
#  Q(13)= PR_Plage_Veules
#  Q(14)= STEP_ST_Valery
#  Q(15)= EP_St_Valery1
#  Q(16)= EP_St_Valery2
#  Q(17)= EP_St_Valery3
#  Q(18)= EP_St_Valery4
#  Q(19)= EP_St_Valery_PR_B
#  Q(20)= STEP_Sotteville
#  Les colonnes TR repr�sentent les concentration du ou des traceurs au niveau de chaque source
#  Si il y a plusieurs traceurs, alors TR(2) repr�sente alors le traceur 2 de la source 1 ...
T       Q(1) Q(2) Q(3) Q(4) Q(5) Q(6) Q(7) Q(8) Q(9) Q(10) Q(11) Q(12) Q(13)  Q(14) Q(15) Q(16) Q(17) Q(18) Q(19) Q(20)  TR(1,1) TR(2,1) TR(3,1) TR(4,1) TR(5,1) TR(6,1) TR(7,1) TR(8,1) TR(9,1) TR(10,1) TR(11,1) TR(12,1) TR(13,1) TR(14,1)  TR(15,1) TR(16,1) TR(17,1) TR(18,1) TR(19,1) TR(20,1) 
s       m3/s m3/s m3/s m3/s m3/s m3/s m3/s m3/s m3/s m3/s  m3/s  m3/s  m3/s   m3/s  m3/s  m3/s  m3/s  m3/s  m3/s  m3/s   g/l     g/l     g/l     g/l     g/l     g/l     g/l     g/l     g/l     g/l      g/l      g/l      g/l      g/l       g/l      g/l      g/l      g/l      g/l      g/l    
0.      0.0  4.1  0.5  0.3  0.0  0.0  0.0  0.0  0.0  0.0   0.0   0.006 0.0    0.01  0.0   0.0   0.0   0.0   0.0   0.0005 0.D0    2.D3    2.D3    2.D4    0.D0    0.D0    0.D0    0.D0    0.D0    0.D0     0.D0     5.D5     0.D0     1875.D3   0.D0     0.D0     0.D0     0.D0     0.D0     24.D4
136500. 0.0  4.1  0.5  0.3  0.0  0.0  0.0  0.0  0.0  0.0   0.0   0.006 0.0    0.01  0.0   0.0   0.0   0.0   0.0   0.0005 0.D0    2.D3    2.D3    2.D4    0.D0    0.D0    0.D0    0.D0    0.D0    0.D0     0.D0     5.D5     0.D0     1875.D3   0.D0     0.D0     0.D0     0.D0     0.D0     24.D4
136800. 0.0  4.1  0.5  0.3  0.0  0.0  0.0  0.0  0.0  0.0   0.03  0.006 0.0045 0.01  0.1   0.1   0.1   0.1   0.1   0.0005 0.D0    2.D3    2.D3    2.D4    0.D0    0.D0    0.D0    0.D0    0.D0    0.D0     2.D3     5.D5     5.D7     1875.D3   2.D3     2.D3     2.D3     2.D3     2.D3     24.D4
147600. 0.0  4.1  0.5  0.3  0.0  0.0  0.0  0.0  0.0  0.0   0.03  0.006 0.0045 0.01  0.1   0.1   0.1   0.1   0.1   0.0005 0.D0    2.D3    2.D3    2.D4    0.D0    0.D0    0.D0    0.D0    0.D0    0.D0     2.D3     5.D5     5.D7     1875.D3   2.D3     2.D3     2.D3     2.D3     2.D3     24.D4
147900. 0.0  4.1  0.5  0.3  0.0  0.0  0.0  0.0  0.0  0.0   0.0   0.006 0.0    0.01  0.0   0.0   0.0   0.0   0.0   0.0005 0.D0    2.D3    2.D3    2.D4    0.D0    0.D0    0.D0    0.D0    0.D0    0.D0     0.D0     5.D5     0.D0     1875.D3   0.D0     0.D0     0.D0     0.D0     0.D0     24.D4
259200. 0.0  4.1  0.5  0.3  0.0  0.0  0.0  0.0  0.0  0.0   0.0   0.006 0.0    0.01  0.0   0.0   0.0   0.0   0.0   0.0005 0.D0    2.D3    2.D3    2.D4    0.D0    0.D0    0.D0    0.D0    0.D0    0.D0     0.D0     5.D5     0.D0     1875.D3   0.D0     0.D0     0.D0     0.D0     0.D0     24.D4