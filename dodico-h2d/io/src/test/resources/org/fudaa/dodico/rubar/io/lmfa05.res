 IT= 0
 RUPTURE PAR RENARD
avec contrainte reduite


  COTE EN CRETE =     22.00
  LARGEUR EN CRETE =      2.00
  COTE EN PIED =     19.00
  LARGEUR EN PIED =     10.00
  DIAMETRE DES GRAINS (D50) =0.00001000
  POROSITE =0.40
  MASSE VOLUMIQUE =2650.
  STRICKLER DONNE = 30.0
  STRICKLER CALCULE =143.1
 LARGEUR MAXIMALE DE BRECHE =  0.050000000000000266

 TEMPS DE DEBUT DE RUPTURE =  0.1  s

  COTE DU TERRAIN NATUREL =    0.01
  COTE DE LA BRECHE =   19.00

  RAYON DU RENARD =    0.10000


    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    2         0.0000    0.0000    0.0000    0.0000    0.0000


   HR   MN    S      COTE EAU  COTE BRECHE  LARG BRECHE             DEBIT LIQ   DEBIT SOL
    0    0    0         0.0823   19.0000    0.2000    0.0000    0.0000
    0    0    0        19.0000   19.0000    0.0500    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    0         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
    0    0    1         0.0000    0.0000    0.0000    0.0000    0.0000
 VOLUME ERODE =        0.
 QU IL FAUT COMPARER AVEC
 VOLUME PARTI =       -0.
