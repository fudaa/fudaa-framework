package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLibFile;

import java.io.File;
import java.util.List;

public class ResFileTester {
  public static void main(String[] arg) {
    File f = new File((arg[0]));
    if (!f.exists()) {
      System.err.println(f.getAbsolutePath() + " doesn't exist");
      System.exit(1);
    }
    final List<String> strings = CtuluLibFile.litFichierLineByLine(f);
    File fOut = new File(f.getAbsolutePath() + ".out");
    CtuluLibFile.copyFileLineByLine(f, fOut);
    System.err.println("nb line = " + strings.size());
    System.err.println("Times:");
    System.err.println(strings.get(0));
    System.err.println(strings.get(1));
    System.err.println(strings.get(94680));
    int sizeLineTime = strings.get(0).length();
    int sizeLineNormal = strings.get(1).length();
    int sizeLineEnd = strings.get(94680).length();
    System.err.println("time " + sizeLineTime);
    System.err.println("nomral " + sizeLineNormal);
    System.err.println("last " + sizeLineEnd);
    for (int i = 0; i < strings.size(); i += 94681) {

      System.err.println("time " + strings.get(i));
      if (strings.get(i).length() != sizeLineTime) {
        System.err.println("ERR TIME");
      }
      for (int j = i + 1; j < i + 94680; j++) {
        boolean isEnd = (j - i) % 31560 == 0;
        if (isEnd) {
          if (strings.get(j).length() != sizeLineEnd) {
            System.err.println("ERR NORMAL END" + j);
            System.err.println("ERR:");
            System.err.println(strings.get(j));
            System.exit(1);
          }
        } else if (strings.get(j).length() != sizeLineNormal) {
          System.err.println("ERR NORMAL " + j);
          System.err.println("ERR:");
          System.err.println(strings.get(j));

          System.exit(1);
        }
      }
      if (i > 0) {
        String last = strings.get(i - 1);
        if (last.length() != sizeLineEnd) {
          System.err.println("ERR END");
        }
      }
      if (strings.get(i + 1).length() != sizeLineNormal) {
        System.err.println("ERR NORMAL");
      }
    }
  }
}
