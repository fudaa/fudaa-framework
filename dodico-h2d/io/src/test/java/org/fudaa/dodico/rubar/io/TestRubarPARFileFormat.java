/**
 *
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoParams;
import org.fudaa.dodico.h2d.rubar.H2dRubarDicoModel;

import java.io.File;
import java.io.IOException;

public class TestRubarPARFileFormat extends TestIO {
  private static final int NB_NOEUDS = 1434;

  public TestRubarPARFileFormat() {
    super("parOld.par");
  }

  public void testLecture() {
    assertCorrect(read(this.fic_));
  }

  //2819
  public void testLectureNew() {
    assertNewCorrect(read(getFile("parNew.par")));
  }

  public void testEcritureNew() {
    assertNewCorrect(write(read(getFile("parNew.par")), true));
  }

  public void testEcriture() {
    assertCorrect(write(read(this.fic_), false));
    assertCorrect(write(read(this.fic_), true));
  }

  private void assertNewCorrect(H2DRubarDicoParams result) {
    final DicoEntite initalTime = H2dRubarDicoModel.getInitalTime(result.getDicoFileFormatVersion());
    final DicoEntite entitePasTemps = getEntitePasDeTemps(result);
    assertNotNull(entitePasTemps);
    assertEquals("0.2", result.getValue(initalTime));
    assertEquals("10800.0", result.getValue(entitePasTemps));
  }

  private DicoEntite getEntitePasDeTemps(H2DRubarDicoParams result) {
    return result.getDicoFileFormatVersion().getEntiteFor(
        new String[]{"Pas de temps de la sauvegarde du champ vitesse de l'eau (TPS)"
            , "Time step for saving water velocity field (TPS)"
        });
  }

  private DicoEntite getCoefficientB(H2DRubarDicoParams result) {
    return result.getDicoFileFormatVersion().getEntiteFor(
        new String[]{"Coefficient B de déviation du charriage"
            , "Coefficient B for bedload deviation"
        });
  }

  private void assertCorrect(H2DRubarDicoParams result) {
    final DicoEntite initalTime = H2dRubarDicoModel.getInitalTime(result.getDicoFileFormatVersion());
    assertNotNull(getEntitePasDeTemps(result));
    assertEquals("0.1", result.getValue(initalTime));
    assertEquals("600.0", result.getValue(getEntitePasDeTemps(result)));
    assertEquals("0.85", result.getValue(getCoefficientB(result)));
  }

  private static H2DRubarDicoParams read(File file) {
    final CtuluIOOperationSynthese read = RubarPARFileFormat.getInstance().getLastVersionInstance(file).read(file, null);
    assertFalse(read.containsSevereError());
    return RubarPARFileFormat.createParams((TIntObjectHashMap) read.getSource());
  }

  private H2DRubarDicoParams write(H2DRubarDicoParams dicoParams, boolean newFormat) {
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarPARFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }
    final CtuluIOOperationSynthese operation = RubarPARFileFormat.getInstance().getLastVersionImpl().write(file, RubarPARFileFormat.transform(dicoParams), null);
    assertFalse(operation.containsSevereError());
    H2DRubarDicoParams read = read(file);
    file.delete();
    return read;
  }
}
