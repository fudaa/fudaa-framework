/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;

import java.io.File;
import java.io.IOException;

public class TestRubarCINFileFormat extends TestIO {
  private static final int NB_NOEUDS = 1434;

  public TestRubarCINFileFormat() {
    super("cinOld.cin");
  }

  public void testLecture() {
    assertCorrect(read(this.fic_, 2181, 1));
  }

  //2819
  public void testLectureNew() {
    assertNewCorrect(read(getFile("cinNew.cin"), 2819, 3));
  }
  public void testEcritureNew() {
    assertNewCorrect(write(read(getFile("cinNew.cin"), 2819, 3)));
  }

  public void testEcriture() {
    assertCorrect(write(read(this.fic_, 2181, 1)));
  }

  private void assertNewCorrect(RubarCINResult result) {
    assertFalse(result.isConcentrationParHauteur_);
    assertDoubleEquals(0, result.getT0());
    assertEquals(13, result.getNbValues());
    assertDoubleEquals(0.17697, result.values_[0].getValue(4));
    assertDoubleEquals(13.6, result.values_[12].getValue(0));
    assertDoubleEquals(13.7, result.values_[12].getValue(2818));
  }

  private void assertCorrect(RubarCINResult result) {
    assertFalse(result.isConcentrationParHauteur_);
    assertDoubleEquals(0, result.getT0());
    assertEquals(7, result.getNbValues());
    assertDoubleEquals(0.6089, result.values_[0].getValue(8));
    assertDoubleEquals(0.1, result.values_[0].getValue(2180));
    assertDoubleEquals(-0.00185, result.values_[1].getValue(3));
    assertDoubleEquals(0.1, result.values_[6].getValue(2180));
  }

  private static RubarCINResult read(File file, int nbElement, int nbConcentrationBlock) {
    final CtuluIOOperationSynthese read = RubarCINFileFormat.getInstance().read(file, null, nbElement, nbConcentrationBlock);
    assertFalse(read.containsSevereError());
    return (RubarCINResult) read.getSource();
  }

  private RubarCINResult write(RubarCINResult grid) {
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarCINFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }
    final CtuluIOOperationSynthese operation = RubarCINFileFormat.getInstance().write(file, grid, null);
    assertFalse(operation.containsSevereError());
    final RubarCINResult readerResult = read(file, grid.getCommonValueSize(), (grid.values_.length - 4) / 3);
    file.delete();
    return readerResult;
  }
}
