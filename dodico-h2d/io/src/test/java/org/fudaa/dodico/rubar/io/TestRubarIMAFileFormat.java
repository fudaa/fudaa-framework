/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGrid;

/**
 * @author CANEL Christophe (Genesis)
 */
public class TestRubarIMAFileFormat extends TestIO {
  private static class Node {
    private double x;
    private double y;

    public Node(EfNode node) {
      x = node.getX();
      y = node.getY();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      long temp;
      temp = Double.doubleToLongBits(x);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = Double.doubleToLongBits(y);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
      if (this == obj) { return true; }
      if (obj == null) { return false; }
      if (!(obj instanceof Node)) { return false; }
      Node other = (Node) obj;
      return ((x < (other.x + epsilon)) && (x > (other.x - epsilon)) && (y < (other.y + epsilon)) && (y > (other.y - epsilon)));
    }
  }

  private static class Element {
    private Node[] nodes;

    public Element(EfElement element, EfGridInterface grid) {
      nodes = new Node[element.getPtNb()];

      for (int i = 0; i < nodes.length; i++) {
        nodes[i] = new Node(grid.getNodes()[element.getPtIndex(i)]);
      }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + Arrays.hashCode(nodes);
      return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
      if (this == obj) { return true; }
      if (obj == null) { return false; }
      if (!(obj instanceof Element)) { return false; }
      Element other = (Element) obj;

      final int nbNodes = nodes.length;

      if (nbNodes != other.nodes.length) { return false; }

      int startIdx = -1;

      for (int i = 0; i < nbNodes; i++) {
        if (nodes[0].equals(other.nodes[i])) {
          startIdx = i;

          break;
        }
      }

      if (startIdx == -1) { return false; }

      for (int i = 0; i < nbNodes; i++) {
        if (!nodes[i].equals(other.nodes[startIdx])) { return false; }

        startIdx = nextIdx(startIdx, nbNodes);
      }

      return true;
    }

    private int nextIdx(int currentIdx, int arrayLength) {
      int nextIdx = currentIdx + 1;

      if (nextIdx >= arrayLength) {
        nextIdx = 0;
      }

      return nextIdx;
    }
  }

  private final static double epsilon = 0.1;

  public TestRubarIMAFileFormat() {
    super("zarn75.ima");
  }

  public void testLecture() {
    assertEquals(getCorrectGrid(), read(this.fic_));
  }

  private static void assertEquals(EfGridInterface expected, EfGridInterface actual) {
    final EfNode[] expectedNodes = expected.getNodes();
    final EfNode[] actualNodes = actual.getNodes();

    assertEquals(expectedNodes.length, actualNodes.length);

    List<Node> nodes = new ArrayList<TestRubarIMAFileFormat.Node>();

    for (EfNode node : expectedNodes) {
      nodes.add(new Node(node));
    }

    for (EfNode node : actualNodes) {
      assertTrue(nodes.remove(new Node(node)));
    }
    
    final EfElement[] expectedElements = expected.getElts();
    final EfElement[] actualElements = actual.getElts();
    
    assertEquals(expectedElements.length, actualElements.length);

    List<Element> elements = new ArrayList<TestRubarIMAFileFormat.Element>();

    for (EfElement element : expectedElements) {
      elements.add(new Element(element, expected));
    }

    for (EfElement element : actualElements) {
      assertTrue(elements.remove(new Element(element, actual)));
    }
  }

  private static EfGridInterface read(File file) {
    final RubarIMAReader reader = (RubarIMAReader) new RubarIMAFileFormat().createReader();

    reader.setFile(file);

    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());

    return (EfGridInterface) result.getSource();
  }

  public void testEcriture() {
    final RubarIMAWriter writer = (RubarIMAWriter) new RubarIMAFileFormat().createWriter();
    File file = null;

    try {
      file = File.createTempFile("Test", "RubarIMAFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    final EfGridInterface correctGrid = getCorrectGrid();
    final CtuluIOOperationSynthese operation = writer.write(correctGrid);
    assertFalse(operation.containsSevereError());

    assertEquals(correctGrid, read(file));
  }

  private static EfGridInterface getCorrectGrid() {
    final EfNode[] nodes = new EfNode[25];

    nodes[0] = new EfNode(32.1, 257.4, 0.0);
    nodes[1] = new EfNode(28.5, 197.3, 0.0);
    nodes[2] = new EfNode(24.9, 137.2, 0.0);
    nodes[3] = new EfNode(114.7, 248.5, 0.0);
    nodes[4] = new EfNode(116.0, 175.8, 0.0);
    nodes[5] = new EfNode(117.3, 103.1, 0.0);
    nodes[6] = new EfNode(237.6, 293.7, 0.0);
    nodes[7] = new EfNode(241.3, 98.6, 0.0);
    nodes[8] = new EfNode(333.2, 286.5, 0.0);
    nodes[9] = new EfNode(345.3, 97.0, 0.0);
    nodes[10] = new EfNode(428.8, 279.3, 0.0);
    nodes[11] = new EfNode(449.3, 95.4, 0.0);
    nodes[12] = new EfNode(242.5, 71.5, 0.0);
    nodes[13] = new EfNode(243.7, 44.4, 0.0);
    nodes[14] = new EfNode(244.9, 17.3, 0.0);
    nodes[15] = new EfNode(347.0, 69.5, 0.0);
    nodes[16] = new EfNode(348.7, 42.0, 0.0);
    nodes[17] = new EfNode(350.4, 14.5, 0.0);
    nodes[18] = new EfNode(451.5, 67.5, 0.0);
    nodes[19] = new EfNode(453.7, 39.6, 0.0);
    nodes[20] = new EfNode(455.9, 11.7, 0.0);
    nodes[21] = new EfNode(553.3, 93.8, 0.0);
    nodes[22] = new EfNode(556.0, 65.5, 0.0);
    nodes[23] = new EfNode(558.7, 37.2, 0.0);
    nodes[24] = new EfNode(561.4, 8.9, 0.0);

    final EfElement[] elements = new EfElement[16];

    elements[0] = new EfElement(new int[] { 0, 1, 4, 3 });
    elements[1] = new EfElement(new int[] { 1, 2, 5, 4 });
    elements[2] = new EfElement(new int[] { 3, 4, 6 });
    elements[3] = new EfElement(new int[] { 4, 7, 6 });
    elements[4] = new EfElement(new int[] { 4, 5, 7 });
    elements[5] = new EfElement(new int[] { 6, 7, 9, 8 });
    elements[6] = new EfElement(new int[] { 8, 9, 11, 10 });
    elements[7] = new EfElement(new int[] { 7, 12, 15, 9 });
    elements[8] = new EfElement(new int[] { 12, 13, 16, 15 });
    elements[9] = new EfElement(new int[] { 13, 14, 17, 16 });
    elements[10] = new EfElement(new int[] { 9, 15, 18, 11 });
    elements[11] = new EfElement(new int[] { 15, 16, 19, 18 });
    elements[12] = new EfElement(new int[] { 16, 17, 20, 19 });
    elements[13] = new EfElement(new int[] { 11, 18, 22, 21 });
    elements[14] = new EfElement(new int[] { 18, 19, 23, 22 });
    elements[15] = new EfElement(new int[] { 19, 20, 24, 23 });

    return new EfGrid(nodes, elements);
  }
}
