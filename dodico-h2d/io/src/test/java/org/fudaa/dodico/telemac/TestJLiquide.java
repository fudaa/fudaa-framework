/**
 * @creation 15 avr. 2003
 * @modification $Date: 2007-01-19 13:07:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.telemac;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;

import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquide;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.telemac.io.TelemacLiquideFileFormat;
import org.fudaa.dodico.telemac.io.TelemacLiquideInterface;

/**
 * @author deniger
 * @version $Id: TestJLiquide.java,v 1.1 2007-01-19 13:07:18 deniger Exp $
 */
public class TestJLiquide extends TestIO {
  /**
   * 'liquide.liq'
   */
  public TestJLiquide() {
    super("liquide.liq");
  }

  protected void interfaceTest(final TelemacLiquideInterface _inter) {
    assertNotNull(_inter);
    assertEquals(2, _inter.getNbEvolutionsFrontiereLiquide());
    final H2dTelemacEvolutionFrontiere[] fs = _inter.getEvolutionsFrontieresLiquides();
    H2dEvolutionFrontiereLiquide fsi = fs[0].getTelemacEvolution();
    assertEquals(1, fsi.getIndexFrontiere());
    assertEquals("m3/s", fsi.getUnite());
    assertEquals(H2dVariableType.DEBIT, fsi.getVariableType());
    assertEquals(5, fsi.getPasTempNb());
    assertEquals(0, fsi.getPasDeTempsAt(0), eps_);
    assertEquals(1800, fsi.getPasDeTempsAt(1), eps_);
    assertEquals(50000, fsi.getPasDeTempsAt(2), eps_);
    assertEquals(50002, fsi.getPasDeTempsAt(3), eps_);
    assertEquals(50003, fsi.getPasDeTempsAt(4), eps_);
    assertEquals(5, fsi.getPasTempNb());
    assertEquals(0, fsi.getValueAt(0), eps_);
    assertEquals(500, fsi.getValueAt(1), eps_);
    assertEquals(501, fsi.getValueAt(2), eps_);
    assertEquals(503, fsi.getValueAt(4), eps_);
    fsi = fs[1].getTelemacEvolution();
    assertEquals(0, fsi.getIndexFrontiere());
    assertEquals("m", fsi.getUnite());
    assertEquals(H2dVariableType.COTE_EAU, fsi.getVariableType());
    assertEquals(5, fsi.getPasTempNb());
    assertEquals(0, fsi.getPasDeTempsAt(0), eps_);
    assertEquals(1800, fsi.getPasDeTempsAt(1), eps_);
    assertEquals(50000, fsi.getPasDeTempsAt(2), eps_);
    assertEquals(263, fsi.getValueAt(0), eps_);
    assertEquals(264, fsi.getValueAt(1), eps_);
    assertEquals(265, fsi.getValueAt(2), eps_);
    assertEquals(263, fsi.getValueAt(4), eps_);
  }

  /**
     *
     */
  public void testEcriture() {
    final TelemacLiquideInterface inter = getInter(fic_);
    final File tmpFile = createTempFile();
    final CtuluIOOperationSynthese syntheseR = TelemacLiquideFileFormat.getInstance().writeClLiquid(tmpFile, inter, null);
    assertFalse(syntheseR.containsMessages());
    interfaceTest(getInter(tmpFile));
  }

  /**
   * Ecrit les messages sur la sortie standard.
   * 
   * @param _f le fichier a lire
   * @return l'interface correspondate. si pas d'erreur.
   */
  public TelemacLiquideInterface getInter(final File _f) {
    final CtuluIOOperationSynthese syntheseR = TelemacLiquideFileFormat.getInstance().read(_f, null);
    final TelemacLiquideInterface r = (TelemacLiquideInterface) syntheseR.getSource();
    if (syntheseR.containsMessages()) {
      syntheseR.printAnalyze();
    }
    assertFalse(syntheseR.containsMessages());
    return r;
  }

  /**
   *
   */
  public void testLecture() {
    interfaceTest(getInter(fic_));
  }

  public void testLectureNewTracer() {
    File file = getFile("clLiquidesTracers.txt");
    TelemacLiquideInterface inter = getInter(file);
    testResultatNewFormat(inter);
  }

  public void testEcritureNewTracer() {
    File file = getFile("clLiquidesTracers.txt");
    TelemacLiquideInterface inter = getInter(file);
    File tmpFile = createTempFile();
    final CtuluIOOperationSynthese syntheseR = TelemacLiquideFileFormat.getInstance().writeClLiquid(tmpFile, inter, null);
    assertFalse(syntheseR.containsMessages());
    testResultatNewFormat(getInter(tmpFile));
  }

  private void testResultatNewFormat(TelemacLiquideInterface inter) {
    H2dTelemacEvolutionFrontiere[] evolutionsFrontieresLiquides = inter.getEvolutionsFrontieresLiquides();
    assertEquals(40, evolutionsFrontieresLiquides.length);
    H2dTelemacEvolutionFrontiere telemacEvolutionFrontiere = evolutionsFrontieresLiquides[39];
    assertEquals(0, telemacEvolutionFrontiere.getIdxVariable());
    H2dEvolutionFrontiereLiquide evolution = telemacEvolutionFrontiere.getTelemacEvolution();
    assertEquals(19, evolution.getIndexFrontiere());
    assertEquals(6, evolution.getEvolution().getNbValues());
    assertEquals(259200, (int) evolution.getEvolution().getX(5));
  }

  /**
   * Test la lecture d'un fichier contenant des tabulations.
   */
  public void testLectureTabulation() {
    interfaceTest(getInter(getFile("liquideTab.liq")));
  }
}
