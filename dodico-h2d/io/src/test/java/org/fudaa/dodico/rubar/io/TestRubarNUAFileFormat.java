/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.rubar.io.RubarNUAResult.RubarNUAResultBloc;
import org.fudaa.dodico.rubar.io.RubarNUAResult.RubarNUAResultLine;

import java.io.File;
import java.util.List;

/**
 * @author CANEL Christophe (Genesis)
 */
public class TestRubarNUAFileFormat extends TestIO {
  public TestRubarNUAFileFormat() {
    super("lmfa05.nua");
  }

  public void testLecture() {
    assertCorrect(read(this.fic_));
  }


  public void testLectureNew() {

    final RubarNUAResult nuaResult = read(getFile("nuaNew.nua"));
    final List<RubarNUAResultBloc> blocs = nuaResult.getBlocs();
    assertEquals(3, blocs.size());
    final RubarNUAResultBloc rubarNUAResultBloc = blocs.get(2);
    final RubarNUAResultLine rubarNUAResultLine = rubarNUAResultBloc.getLines().get(4566);
    assertEquals(4567,rubarNUAResultLine.getAreteIdx());
    assertDoubleEquals(2,rubarNUAResultLine.getCoeff());
    assertDoubleEquals(9.9750,rubarNUAResultLine.getPoint().getX());
    assertDoubleEquals(1.25,rubarNUAResultLine.getPoint().getY());
  }

  public void testLectureTutu() {
    final RubarNUAResult nuaResult = read(getFile("tututu.nua"));
    assertNotNull(nuaResult);
    final List<RubarNUAResultBloc> blocs = nuaResult.getBlocs();
    assertEquals(2, blocs.size());
    RubarNUAResultBloc firstBlock = blocs.get(0);
    assertEquals(4226, firstBlock.getLines().size());
    assertDoubleEquals(7719.0500, firstBlock.getLines().get(4225).getPoint().getX());
  }

  private void assertCorrect(RubarNUAResult result) {
    final List<RubarNUAResultBloc> blocs = result.getBlocs();
    assertEquals(2, blocs.size());

    RubarNUAResultBloc bloc = blocs.get(0);
    assertDoubleEquals(0.5029730472979922, bloc.getTime());
    assertCorrectBloc(bloc);

    bloc = blocs.get(1);
    assertDoubleEquals(1.0, bloc.getTime());
    assertCorrectBloc(bloc);
  }

  private void assertCorrectBloc(RubarNUAResultBloc bloc) {
    final List<RubarNUAResultLine> lines = bloc.getLines();

    assertEquals(19933, lines.size());

    RubarNUAResultLine line = lines.get(0);

    assertDoubleEquals(7.1375, line.getPoint().getX());
    assertDoubleEquals(0.0, line.getPoint().getY());
    assertDoubleEquals(0.0, line.getPoint().getZ());
    assertDoubleEquals(0.0, line.getCoeff());

    line = lines.get(1);

    assertDoubleEquals(7.1500, line.getPoint().getX());
    assertDoubleEquals(0.0050, line.getPoint().getY());
    assertDoubleEquals(0.0, line.getPoint().getZ());
    assertDoubleEquals(0.0, line.getCoeff());

    line = lines.get(2);

    assertDoubleEquals(7.1375, line.getPoint().getX());
    assertDoubleEquals(0.0100, line.getPoint().getY());
    assertDoubleEquals(0.0, line.getPoint().getZ());
    assertDoubleEquals(0.0004, line.getCoeff());

    line = lines.get(3);

    assertDoubleEquals(7.1250, line.getPoint().getX());
    assertDoubleEquals(0.0050, line.getPoint().getY());
    assertDoubleEquals(0.0, line.getPoint().getZ());
    assertDoubleEquals(0.0004, line.getCoeff());
  }

  private static RubarNUAResult read(File file) {
    final RubarNUAReader reader = (RubarNUAReader) new RubarNUAFileFormat().createReader();

    reader.setFile(file);

    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());

    return (RubarNUAResult) result.getSource();
  }
}
