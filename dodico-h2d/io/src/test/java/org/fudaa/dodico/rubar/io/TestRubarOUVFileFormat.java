/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageI;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageCompositeTypeControle;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CANEL Christophe (Genesis)
 */
public class TestRubarOUVFileFormat extends TestIO {
  public TestRubarOUVFileFormat() {
    super("22car5.ouv");
  }

  public void testLecture() {
    assertEqualsRubarOuvrageContainer(getExpectedRubarOuvrageContainer(), read(this.fic_, false));
  }

  public void testLectureNewFormat() {
    testNewFormatContent(read(getFile("ouvNewFormat.ouv"), true));
  }

  public void testLectureNewFormatWithTransport() {
    testNewFormatContentTransport(read(getFile("ouvNewTransport.ouv"), false));
  }

  public void testEcritureNewFormatWithTransport() {
    testNewFormatContentTransport(write(read(getFile("ouvNewTransport.ouv"), false), false));
  }

  private RubarOuvrageContainer write(RubarOuvrageContainer ouvrageContainer, boolean newFormat) {
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarOUVFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }
    final RubarOUVWriter writer = (RubarOUVWriter) new RubarOUVFileFormat().createWriter();
    writer.setNewFormat(newFormat);
    writer.setFile(file);
    final CtuluIOOperationSynthese operation = writer.write(ouvrageContainer);
    final RubarOuvrageContainer rubarOuvrageContainer = read(file, newFormat);
    file.delete();
    return rubarOuvrageContainer;
  }

  public void testEcritureNewFormat() {
    final RubarOuvrageContainer ouvrageContainer = read(getFile("ouvNewFormat.ouv"), true);
    testNewFormatContent(write(ouvrageContainer, true));
  }

  private void testNewFormatContent(RubarOuvrageContainer read) {
    assertEquals(1, read.getNbOuvrage());
    assertEquals(1, read.getOuvrage(0).getNbOuvrageElementaires());
    final RubarOuvrageElementaireComposite ouvrageElementaire = (RubarOuvrageElementaireComposite) read.getOuvrage(0).getOuvrageElementaire(0);
    assertEquals(H2dRubarOuvrageType.COMPOSITE, ouvrageElementaire.getType());
    assertDoubleEquals(8273.9, ouvrageElementaire.getXAmont());
    assertDoubleEquals(2998.075, ouvrageElementaire.getYAmont());
    assertDoubleEquals(8316.53333, ouvrageElementaire.getXAval());
    assertDoubleEquals(3191.4, ouvrageElementaire.getYAval());
  }

  private void testNewFormatContentTransport(RubarOuvrageContainer read) {
    assertEquals(1, read.getNbOuvrage());
    assertEquals(1, read.getOuvrage(0).getNbOuvrageElementaires());
    final RubarOuvrageElementaireApportDebit ouvrageElementaire = (RubarOuvrageElementaireApportDebit) read.getOuvrage(0).getOuvrageElementaire(0);
    assertEquals(H2dRubarOuvrageType.APPORT_DEBIT, ouvrageElementaire.getType());
    assertEquals(10, ouvrageElementaire.getEvols().size());

    EvolutionReguliereAbstract debit = ouvrageElementaire.getEvol(H2dVariableTransType.DEBIT_M3);
    assertEquals(482, debit.getNbValues());
    assertDoubleEquals(432000, debit.getX(481));
    assertDoubleEquals(39.0, debit.getY(481));

    EvolutionReguliereAbstract concentration = ouvrageElementaire.getEvol(H2dVariableTransType.CONCENTRATION);
    assertEquals(482, concentration.getNbValues());
    assertDoubleEquals(432000, concentration.getX(481));
    assertDoubleEquals(1, concentration.getY(481));

    concentration = ouvrageElementaire.getEvol(H2dVariableTransType.createIndexedVar(H2dVariableTransType.CONCENTRATION, 2));
    assertEquals(482, concentration.getNbValues());
    assertDoubleEquals(432000, concentration.getX(481));
    assertDoubleEquals(4, concentration.getY(481));

    concentration = ouvrageElementaire.getEvol(H2dVariableTransType.createIndexedVar(H2dVariableTransType.CONCENTRATION, 3));
    assertEquals(482, concentration.getNbValues());
    assertDoubleEquals(432000, concentration.getX(481));
    assertDoubleEquals(7, concentration.getY(481));
  }

  public void testLectureNewFormatErosionLineaire() {
    final RubarOuvrageContainer ouvrageContainer = read(getFile("debtsz.ouv"), false);
    assertEquals(118, ouvrageContainer.getNbOuvrage());
    final H2dRubarOuvrageI ouvrage = ouvrageContainer.getOuvrage(117);
    assertEquals(1, ouvrage.getNbOuvrageElementaires());
    final RubarOuvrageElementaireBreche ouvrageElementaire = (RubarOuvrageElementaireBreche) ouvrage.getOuvrageElementaire(0);
    assertDoubleEquals(0.000003, ouvrageElementaire.getCoefficientErosionLineaire());
    //on modifie une valeur
    ouvrageElementaire.coefficientErosionLineaire = 1;
    final RubarOUVWriter writer = (RubarOUVWriter) new RubarOUVFileFormat().createWriter();
    File file = null;

    try {
      file = File.createTempFile("Test", "RubarOUVFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    final CtuluIOOperationSynthese operation = writer.write(ouvrageContainer);
    assertFalse(operation.containsSevereError());

    //on relit
    final RubarOuvrageContainer readAfterModification = read(file, false);
    assertEquals(118, readAfterModification.getNbOuvrage());
    final H2dRubarOuvrageI ouvrageAfterModification = ouvrageContainer.getOuvrage(117);
    assertEquals(1, ouvrageAfterModification.getNbOuvrageElementaires());
    final RubarOuvrageElementaireBreche ouvrageElementaireAfterModification = (RubarOuvrageElementaireBreche) ouvrageAfterModification.getOuvrageElementaire(0);
    assertDoubleEquals(1, ouvrageElementaireAfterModification.getCoefficientErosionLineaire());
  }

  private static void assertEqualsRubarOuvrageContainer(RubarOuvrageContainer expected, RubarOuvrageContainer actual) {
    assertEquals(expected.getNbOuvrage(), actual.getNbOuvrage());

    for (int i = 0; i < expected.getNbOuvrage(); i++) {
      assertEqualsRubarOuvrage((RubarOuvrage) expected.getOuvrage(i), (RubarOuvrage) actual.getOuvrage(i));
    }
  }

  private static void assertEqualsRubarOuvrage(RubarOuvrage expected, RubarOuvrage actual) {
    assertEquals(expected.xa1_, actual.xa1_);
    assertEquals(expected.ya1_, actual.ya1_);
    assertEquals(expected.xe1_, actual.xe1_);
    assertEquals(expected.ye1_, actual.ye1_);
    assertEquals(expected.rubarRef_, actual.rubarRef_);

    assertEquals(expected.xEltIntern_.length, actual.xEltIntern_.length);

    for (int i = 0; i < expected.xEltIntern_.length; i++) {
      assertEquals(expected.xEltIntern_[i], actual.xEltIntern_[i]);
    }

    assertEquals(expected.yEltIntern_.length, actual.yEltIntern_.length);

    for (int i = 0; i < expected.yEltIntern_.length; i++) {
      assertEquals(expected.yEltIntern_[i], actual.yEltIntern_[i]);
    }

    assertEquals(expected.xa2_, actual.xa2_);
    assertEquals(expected.ya2_, actual.ya2_);
    assertEquals(expected.xe2_, actual.xe2_);
    assertEquals(expected.ye2_, actual.ye2_);

    assertEquals(expected.ouvElement_.length, actual.ouvElement_.length);

    for (int i = 0; i < expected.ouvElement_.length; i++) {
      assertEqualsH2dRubarOuvrageElementaireInterface(expected.ouvElement_[i], actual.ouvElement_[i]);
    }
  }

  private static void assertEqualsH2dRubarOuvrageElementaireInterface(H2dRubarOuvrageElementaireInterface expected,
                                                                      H2dRubarOuvrageElementaireInterface actual) {
    assertEquals(expected.getClass(), actual.getClass());

    if (expected instanceof RubarOuvrageElementaireBreche) {
      assertEqualsRubarOuvrageElementaireBreche((RubarOuvrageElementaireBreche) expected,
          (RubarOuvrageElementaireBreche) actual);
    } else if (expected instanceof RubarOuvrageElementaireDeversoir) {
      assertEqualsRubarOuvrageElementaireDeversoir((RubarOuvrageElementaireDeversoir) expected,
          (RubarOuvrageElementaireDeversoir) actual);
    } else if (expected instanceof RubarOuvrageElementaireDeversoirHydraulique) {
      assertEqualsRubarOuvrageElementaireDeversoirHydraulique((RubarOuvrageElementaireDeversoirHydraulique) expected,
          (RubarOuvrageElementaireDeversoirHydraulique) actual);
    } else if (expected instanceof RubarOuvrageElementaireOrificeCirculaire) {
      assertEqualsRubarOuvrageElementaireOrificeCirculaire((RubarOuvrageElementaireOrificeCirculaire) expected,
          (RubarOuvrageElementaireOrificeCirculaire) actual);
    } else if (expected instanceof RubarOuvrageElementaireComposite) {
      assertEqualsRubarOuvrageElementaireComposite((RubarOuvrageElementaireComposite) expected,
          (RubarOuvrageElementaireComposite) actual);
    } else {
      fail();
    }
  }

  private static void assertEqualsRubarOuvrageElementaireBreche(RubarOuvrageElementaireBreche expected,
                                                                RubarOuvrageElementaireBreche actual) {
    assertEquals(expected.brecheType, actual.brecheType);
    assertEquals(expected.debutTemps, actual.debutTemps);
    assertEquals(expected.coteCrete, actual.coteCrete);
    assertEquals(expected.cotePied, actual.cotePied);
    assertEquals(expected.largeurCrete, actual.largeurCrete);
    assertEquals(expected.largeurPied, actual.largeurPied);
    assertEquals(expected.diametreMedian, actual.diametreMedian);
    assertEquals(expected.coeffStrickler, actual.coeffStrickler);
    assertEquals(expected.masseVolumiqueGrain, actual.masseVolumiqueGrain);
    assertEquals(expected.porosite, actual.porosite);
    assertEquals(expected.coteFond, actual.coteFond);
    assertEquals(expected.dimensionInitiale, actual.dimensionInitiale);
    assertEquals(expected.pasDeTemps, actual.pasDeTemps);
    assertEquals(expected.coeffPerteDeCharge, actual.coeffPerteDeCharge);
    assertEquals(expected.indicateur, actual.indicateur);
    assertEquals(expected.nbPasDeTempsMax, actual.nbPasDeTempsMax);
  }

  private static void assertEqualsRubarOuvrageElementaireDeversoir(RubarOuvrageElementaireDeversoir expected,
                                                                   RubarOuvrageElementaireDeversoir actual) {
    assertEquals(expected.longDeversement_, actual.longDeversement_);
    assertEquals(expected.coteSeuilZd_, actual.coteSeuilZd_);
    assertEquals(expected.coteMisEnchargeZm_, actual.coteMisEnchargeZm_);
    assertEquals(expected.coefficientDebit_, actual.coefficientDebit_);
  }

  private static void assertEqualsRubarOuvrageElementaireDeversoirHydraulique(
      RubarOuvrageElementaireDeversoirHydraulique expected, RubarOuvrageElementaireDeversoirHydraulique actual) {
    assertEquals(expected.longDeversement_, actual.longDeversement_);
    assertEquals(expected.coteSeuilZd_, actual.coteSeuilZd_);
    assertEquals(expected.coteMisEnchargeZm_, actual.coteMisEnchargeZm_);
    assertEquals(expected.coefficientDebit_, actual.coefficientDebit_);
  }

  private static void assertEqualsRubarOuvrageElementaireOrificeCirculaire(
      RubarOuvrageElementaireOrificeCirculaire expected, RubarOuvrageElementaireOrificeCirculaire actual) {
    assertEquals(expected.longDeversement_, actual.longDeversement_);
    assertEquals(expected.coteSeuilZd_, actual.coteSeuilZd_);
    assertEquals(expected.diametre_, actual.diametre_);
    assertEquals(expected.coefficientDebit_, actual.coefficientDebit_);
  }

  private static void assertEqualsRubarOuvrageElementaireComposite(RubarOuvrageElementaireComposite expected,
                                                                   RubarOuvrageElementaireComposite actual) {
    assertEquals(expected.typeControle, actual.typeControle);
    assertEquals(expected.xAmont, actual.xAmont);
    assertEquals(expected.yAmont, actual.yAmont);
    assertEquals(expected.xAval, actual.xAval);
    assertEquals(expected.yAval, actual.yAval);

    assertEquals(expected.valeurs.length, actual.valeurs.length);

    for (int i = 0; i < expected.valeurs.length; i++) {
      assertEquals(expected.valeurs[i], actual.valeurs[i]);
    }

    assertEquals(expected.ouvrages.length, actual.ouvrages.length);

    for (int i = 0; i < expected.ouvrages.length; i++) {
      assertEqualsH2dRubarOuvrageElementaireInterface(expected.ouvrages[i], actual.ouvrages[i]);
    }
  }

  private static RubarOuvrageContainer read(File file, boolean newFormat) {
    final RubarOUVReader reader = (RubarOUVReader) new RubarOUVFileFormat().createReader();
    reader.setFile(file);

    final CtuluIOOperationSynthese result = reader.read();
    assertEquals(newFormat, reader.isNewFormat());
    assertFalse(result.containsSevereError());

    return (RubarOuvrageContainer) result.getSource();
  }

  public void testEcriture() {
    final RubarOUVWriter writer = (RubarOUVWriter) new RubarOUVFileFormat().createWriter();
    File file = null;

    try {
      file = File.createTempFile("Test", "RubarOUVFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    final CtuluIOOperationSynthese operation = writer.write(getExpectedRubarOuvrageContainer());
    assertFalse(operation.containsSevereError());

    assertEqualsRubarOuvrageContainer(getExpectedRubarOuvrageContainer(), read(file, false));
  }

  private static RubarOuvrageContainer getExpectedRubarOuvrageContainer() {
    final RubarOuvrageContainer container = new RubarOuvrageContainer();
    final List<RubarOuvrage> ouvrages = new ArrayList<RubarOuvrage>(6);

    ouvrages.add(getExpectedRubarOuvrage1());
    ouvrages.add(getExpectedRubarOuvrage2());
    ouvrages.add(getExpectedRubarOuvrage3());
    ouvrages.add(getExpectedRubarOuvrage4());
    ouvrages.add(getExpectedRubarOuvrage5());
    ouvrages.add(getExpectedRubarOuvrage6());

    container.ouvrage_ = ouvrages;

    return container;
  }

  private static RubarOuvrage getExpectedRubarOuvrage1() {
    RubarOuvrage ouvrage = new RubarOuvrage();

    ouvrage.xa1_ = 316.0;
    ouvrage.ya1_ = 1376.4;
    ouvrage.xe1_ = 313.5;
    ouvrage.ye1_ = 1376.4;

    ouvrage.rubarRef_ = -2;

    ouvrage.xEltIntern_ = new double[0];
    ouvrage.yEltIntern_ = new double[0];

    ouvrage.xa2_ = 316.0;
    ouvrage.ya2_ = 1376.4;
    ouvrage.xe2_ = 318.5;
    ouvrage.ye2_ = 1376.4;

    ouvrage.ouvElement_ = new H2dRubarOuvrageElementaireInterface[0];

    return ouvrage;
  }

  private static RubarOuvrage getExpectedRubarOuvrage2() {
    RubarOuvrage ouvrage = new RubarOuvrage();

    ouvrage.xa1_ = 146.0;
    ouvrage.ya1_ = 1061.05;
    ouvrage.xe1_ = 143.5;
    ouvrage.ye1_ = 1061.05;

    ouvrage.rubarRef_ = -2;

    ouvrage.xEltIntern_ = new double[2];
    ouvrage.yEltIntern_ = new double[2];

    ouvrage.xEltIntern_[0] = 153.5;
    ouvrage.yEltIntern_[0] = 1061.05;
    ouvrage.xEltIntern_[1] = 148.5;
    ouvrage.yEltIntern_[1] = 1061.05;

    ouvrage.xa2_ = 156.0;
    ouvrage.ya2_ = 1061.05;
    ouvrage.xe2_ = 158.5;
    ouvrage.ye2_ = 1061.1;

    ouvrage.ouvElement_ = new H2dRubarOuvrageElementaireInterface[1];

    ouvrage.ouvElement_[0] = getRubarOuvrageElementaireBreche(0, -1.0, 5.0, 0.0, 5.0, 15.0, 1.0, 20.0, 2650.0, 0.3,
        0.0, 2.0, 60.0, 0.5, 0, 5000);

    return ouvrage;
  }

  private static RubarOuvrage getExpectedRubarOuvrage3() {
    RubarOuvrage ouvrage = new RubarOuvrage();

    ouvrage.xa1_ = 146.0;
    ouvrage.ya1_ = 1056.3;
    ouvrage.xe1_ = 143.5;
    ouvrage.ye1_ = 1056.3;

    ouvrage.rubarRef_ = -2;

    ouvrage.xEltIntern_ = new double[0];
    ouvrage.yEltIntern_ = new double[0];

    ouvrage.xa2_ = 156.0;
    ouvrage.ya2_ = 1056.3;
    ouvrage.xe2_ = 158.5;
    ouvrage.ye2_ = 1056.3;

    ouvrage.ouvElement_ = new H2dRubarOuvrageElementaireInterface[1];

    ouvrage.ouvElement_[0] = getRubarOuvrageElementaireBreche(0, 154000.0, 5.0, 0.0, 5.0, 15.0, 1.0, 20.0, 2650.0, 0.3,
        0.0, 2.0, 60.0, 0.5, 0, 5000);

    return ouvrage;
  }

  private static RubarOuvrage getExpectedRubarOuvrage4() {
    RubarOuvrage ouvrage = new RubarOuvrage();

    ouvrage.xa1_ = 146.0;
    ouvrage.ya1_ = 1045.2;
    ouvrage.xe1_ = 143.5;
    ouvrage.ye1_ = 1045.2;

    ouvrage.rubarRef_ = -2;

    ouvrage.xEltIntern_ = new double[0];
    ouvrage.yEltIntern_ = new double[0];

    ouvrage.xa2_ = 156.0;
    ouvrage.ya2_ = 1045.2;
    ouvrage.xe2_ = 158.5;
    ouvrage.ye2_ = 1045.225;

    ouvrage.ouvElement_ = new H2dRubarOuvrageElementaireInterface[1];

    ouvrage.ouvElement_[0] = getRubarOuvrageElementaireBreche(3, -1, 5.0, 0.0, 5.0, 15.0, 1.0, 20.0, 2650.0, 0.3, 0.0,
        2.0, 60.0, 0.5, 0, 5000);

    return ouvrage;
  }

  private static RubarOuvrage getExpectedRubarOuvrage5() {
    RubarOuvrage ouvrage = new RubarOuvrage();

    ouvrage.xa1_ = 146.0;
    ouvrage.ya1_ = 1023.6;
    ouvrage.xe1_ = 143.5;
    ouvrage.ye1_ = 1023.6;

    ouvrage.rubarRef_ = -2;

    ouvrage.xEltIntern_ = new double[2];
    ouvrage.yEltIntern_ = new double[2];

    ouvrage.xEltIntern_[0] = 153.5;
    ouvrage.yEltIntern_[0] = 1023.6;
    ouvrage.xEltIntern_[1] = 148.5;
    ouvrage.yEltIntern_[1] = 1023.6;

    ouvrage.xa2_ = 156.0;
    ouvrage.ya2_ = 1023.6;
    ouvrage.xe2_ = 158.5;
    ouvrage.ye2_ = 1023.9;

    ouvrage.ouvElement_ = new H2dRubarOuvrageElementaireInterface[2];

    H2dRubarOuvrageElementaireInterface deversoir = getRubarOuvrageElementaireDeversoir(100.0, 9000.0, 9999.0, 0.2);
    H2dRubarOuvrageElementaireInterface orifice = getRubarOuvrageElementaireOrificeCirculaire(120.0, 9340.0, 9129.0,
        0.6);
    H2dRubarOuvrageElementaireInterface breche1 = getRubarOuvrageElementaireBreche(2, -1, 5.0, 0.0, 5.0, 15.0, 1.0,
        20.0, 2650.0, 0.3, 0.0, 2.0, 60.0, 0.5, 0, 5000);

    ouvrage.ouvElement_[0] = getRubarOuvrageElementaireComposite(H2dRubarOuvrageCompositeTypeControle.TIME, 143.5, 1023.6, 158.5, 1023.9, new double[]{
        153000.0, 163000.0}, new H2dRubarOuvrageElementaireInterface[]{deversoir, orifice, breche1});

    H2dRubarOuvrageElementaireInterface deversoirHydraulique = getRubarOuvrageElementaireDeversoirHydraulique(110.0,
        9450.0, 9579.0, 0.3);
    H2dRubarOuvrageElementaireInterface breche2 = getRubarOuvrageElementaireBreche(1, 158000.0, 5.0, 0.0, 5.0, 15.0,
        1.0, 20.0, 2650.0, 0.3, 0.0, 2.0, 60.0, 0.5, 0, 5000);

    ouvrage.ouvElement_[1] = getRubarOuvrageElementaireComposite(H2dRubarOuvrageCompositeTypeControle.TIME, 142.5, 1063.6, 128.5, 1123.9,
        new double[]{700.0}, new H2dRubarOuvrageElementaireInterface[]{deversoirHydraulique, breche2});

    return ouvrage;
  }

  private static RubarOuvrage getExpectedRubarOuvrage6() {
    RubarOuvrage ouvrage = new RubarOuvrage();

    ouvrage.xa1_ = 146.0;
    ouvrage.ya1_ = 1018.9;
    ouvrage.xe1_ = 143.5;
    ouvrage.ye1_ = 1018.9;

    ouvrage.rubarRef_ = -2;

    ouvrage.xEltIntern_ = new double[0];
    ouvrage.yEltIntern_ = new double[0];

    ouvrage.xa2_ = 156.0;
    ouvrage.ya2_ = 1018.9;
    ouvrage.xe2_ = 158.5;
    ouvrage.ye2_ = 1019.15;

    ouvrage.ouvElement_ = new H2dRubarOuvrageElementaireInterface[1];

    ouvrage.ouvElement_[0] = getRubarOuvrageElementaireBreche(0, 152000.0, 5.0, 0.0, 5.0, 15.0, 1.0, 20.0, 2650.0, 0.3,
        0.0, 2.0, 60.0, 0.5, 0, 5000);

    return ouvrage;
  }

  private static RubarOuvrageElementaireBreche getRubarOuvrageElementaireBreche(int brecheType, double debutTemps,
                                                                                double coteCrete, double cotePied, double largeurCrete, double largeurPied, double diametreMedian,
                                                                                double coeffStrickler, double masseVolumiqueGrain, double porosite, double coteFond,
                                                                                double dimensionInitiale,
                                                                                double pasDeTemps, double coeffPerteDeCharge, int indicateur, int nbPasDeTempsMax) {
    RubarOuvrageElementaireBreche ouvrage = new RubarOuvrageElementaireBreche();

    ouvrage.brecheType = brecheType;
    ouvrage.debutTemps = debutTemps;
    ouvrage.coteCrete = coteCrete;
    ouvrage.cotePied = cotePied;
    ouvrage.largeurCrete = largeurCrete;
    ouvrage.largeurPied = largeurPied;
    ouvrage.diametreMedian = diametreMedian;
    ouvrage.coeffStrickler = coeffStrickler;
    ouvrage.masseVolumiqueGrain = masseVolumiqueGrain;
    ouvrage.porosite = porosite;
    ouvrage.coteFond = coteFond;
    ouvrage.dimensionInitiale = dimensionInitiale;
    ouvrage.pasDeTemps = pasDeTemps;
    ouvrage.coeffPerteDeCharge = coeffPerteDeCharge;
    ouvrage.indicateur = indicateur;
    ouvrage.nbPasDeTempsMax = nbPasDeTempsMax;

    return ouvrage;
  }

  private static RubarOuvrageElementaireComposite getRubarOuvrageElementaireComposite(H2dRubarOuvrageCompositeTypeControle typeControle,
                                                                                      double xAmont, double yAmont, double xAval, double yAval, double[] valeurs,
                                                                                      H2dRubarOuvrageElementaireInterface[] ouvrages) {
    RubarOuvrageElementaireComposite ouvrage = new RubarOuvrageElementaireComposite();

    ouvrage.typeControle = typeControle;
    ouvrage.xAmont = xAmont;
    ouvrage.yAmont = yAmont;
    ouvrage.xAval = xAval;
    ouvrage.yAval = yAval;
    ouvrage.valeurs = valeurs;
    ouvrage.ouvrages = ouvrages;

    return ouvrage;
  }

  private static RubarOuvrageElementaireDeversoir getRubarOuvrageElementaireDeversoir(double longDeversement,
                                                                                      double coteSeuilZd, double coteMisEnchargeZm, double coefficientDebit) {
    RubarOuvrageElementaireDeversoir ouvrage = new RubarOuvrageElementaireDeversoir();

    ouvrage.longDeversement_ = longDeversement;
    ouvrage.coteSeuilZd_ = coteSeuilZd;
    ouvrage.coteMisEnchargeZm_ = coteMisEnchargeZm;
    ouvrage.coefficientDebit_ = coefficientDebit;

    return ouvrage;
  }

  private static RubarOuvrageElementaireDeversoirHydraulique getRubarOuvrageElementaireDeversoirHydraulique(
      double longDeversement, double coteSeuilZd, double coteMisEnchargeZm, double coefficientDebit) {
    RubarOuvrageElementaireDeversoirHydraulique ouvrage = new RubarOuvrageElementaireDeversoirHydraulique();

    ouvrage.longDeversement_ = longDeversement;
    ouvrage.coteSeuilZd_ = coteSeuilZd;
    ouvrage.coteMisEnchargeZm_ = coteMisEnchargeZm;
    ouvrage.coefficientDebit_ = coefficientDebit;

    return ouvrage;
  }

  private static RubarOuvrageElementaireOrificeCirculaire getRubarOuvrageElementaireOrificeCirculaire(
      double longDeversement, double coteSeuilZd, double diametre, double coefficientDebit) {
    RubarOuvrageElementaireOrificeCirculaire ouvrage = new RubarOuvrageElementaireOrificeCirculaire();

    ouvrage.longDeversement_ = longDeversement;
    ouvrage.coteSeuilZd_ = coteSeuilZd;
    ouvrage.diametre_ = diametre;
    ouvrage.coefficientDebit_ = coefficientDebit;

    return ouvrage;
  }
}
