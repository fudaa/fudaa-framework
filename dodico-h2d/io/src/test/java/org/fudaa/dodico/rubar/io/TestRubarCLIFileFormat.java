/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.h2d.rubar.H2dRubarTimeConditionMutable;
import org.fudaa.dodico.mesure.EvolutionReguliere;

import java.io.File;
import java.io.IOException;

/**
 * @author CANEL Christophe (Genesis)
 */
public class TestRubarCLIFileFormat extends TestIO {
  public TestRubarCLIFileFormat() {
    super("lmfa05.cli");
  }

  public void testLecture() {
    assertEquals(getConditions(), read("lmfa05.cli"));
    assertEquals(getConditionsWithTransOld(), read("lmfa05_trans_old.cli"));
    assertEquals(getConditionsWithTransNew(), read("lmfa05_trans_new.cli"));
  }

  public void testIsNewFormat() {
    assertFalse(RubarCLIReader.isNewFormat("     1  64.000   1.200   0.000"));
    assertFalse(RubarCLIReader.isNewFormat("     100640000000002000000.000"));
    assertFalse(RubarCLIReader.isNewFormat("     10064.0000000.2000000.00011111111111111111111111111"));
  }

  public void testGetNbConcentrations() {

    assertEquals(0, RubarCLIReader.getNbConcentrations(StringUtils.center("mock string", 39), 9));
    assertEquals(3, RubarCLIReader.getNbConcentrations(StringUtils.center("mock string", 129), 9));
  }

  public void testLectureNewFormatSeveralBlocks() {
    final H2dRubarTimeConditionMutable[] conditions = read(getFile("cliNewFormatSeveralBlocks.cli"), true);
    testContentFileFormatWithBlockTransport(conditions);
  }

  public void testLectureOldFormatSeveralBlocks() {
    final H2dRubarTimeConditionMutable[] conditions = read(getFile("cliOldFormatSeveralBlocks.cli"), false);
    testContentFileFormatWithBlockTransport(conditions);
  }

  public void testEcritureNewFormatSeveralBlocks() {
    H2dRubarTimeConditionMutable[] conditions = read(getFile("cliNewFormatSeveralBlocks.cli"), true);
    ;
    final RubarCLIWriter writer = (RubarCLIWriter) new RubarCLIFileFormat().createWriter();
    writer.setNewFormat(true);
    File file = null;

    try {
      file = File.createTempFile("Test", "RubarCLIFileFormat");
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }

    writer.setFile(file);
    CtuluIOOperationSynthese operation = writer.write(conditions);
    assertFalse(operation.containsSevereError());
    testContentFileFormatWithBlockTransport(read(file, true));
  }

  public void testEcritureOldFormatSeveralBlocks() {
    H2dRubarTimeConditionMutable[] conditions = read(getFile("cliOldFormatSeveralBlocks.cli"), false);
    ;
    final RubarCLIWriter writer = (RubarCLIWriter) new RubarCLIFileFormat().createWriter();
    writer.setNewFormat(false);
    File file = null;

    try {
      file = File.createTempFile("Test", "RubarCLIFileFormat");
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }

    writer.setFile(file);
    CtuluIOOperationSynthese operation = writer.write(conditions);
    assertFalse(operation.containsSevereError());
    testContentFileFormatWithBlockTransport(read(file, false));
  }

  private void testContentFileFormatWithBlockTransport(H2dRubarTimeConditionMutable[] conditions) {
    assertEquals(20, conditions.length);
    final int firstTimeStep = 0;
    final double firstTimeStepValue = 0;
    for (H2dRubarTimeConditionMutable conditionMutable : conditions) {
      assertEquals(3, conditionMutable.getNbConcentrationBlock());
      assertEquals(11, conditionMutable.getHEvol().getNbValues());
      for (int idxBlock = 0; idxBlock < 3; idxBlock++) {
        assertEquals(11, conditionMutable.getConcentrationEvolution(idxBlock).getNbValues());
        assertEquals(11, conditionMutable.getEtendueEvolution(idxBlock).getNbValues());
        assertEquals(11, conditionMutable.getDiametreEvolution(idxBlock).getNbValues());
      }
    }
    final H2dRubarTimeConditionMutable condition = conditions[9];

    assertDoubleEquals(firstTimeStepValue, condition.getHEvol().getX(firstTimeStep));
    assertDoubleEquals(0.5, condition.getHEvol().getY(firstTimeStep));
    assertDoubleEquals(firstTimeStepValue, condition.getQnEvol().getX(firstTimeStep));
    assertDoubleEquals(49, condition.getQnEvol().getY(firstTimeStep));
    assertDoubleEquals(firstTimeStepValue, condition.getQtEvol().getX(firstTimeStep));
    assertDoubleEquals(0.9, condition.getQtEvol().getY(firstTimeStep));

    //first time step
    int block = 0;
    assertDoubleEquals(firstTimeStepValue, condition.getConcentrationEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(0.1, condition.getConcentrationEvolution(block).getY(firstTimeStep));

    assertDoubleEquals(firstTimeStepValue, condition.getDiametreEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(0.021, condition.getDiametreEvolution(block).getY(firstTimeStep));

    assertDoubleEquals(firstTimeStepValue, condition.getEtendueEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(1, condition.getEtendueEvolution(block).getY(firstTimeStep));

    block = 1;
    assertDoubleEquals(firstTimeStepValue, condition.getConcentrationEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(1, condition.getConcentrationEvolution(block).getY(firstTimeStep));

    assertDoubleEquals(firstTimeStepValue, condition.getDiametreEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(0.01, condition.getDiametreEvolution(block).getY(firstTimeStep));

    assertDoubleEquals(firstTimeStepValue, condition.getEtendueEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(1, condition.getEtendueEvolution(block).getY(firstTimeStep));

    block = 2;
    assertDoubleEquals(firstTimeStepValue, condition.getConcentrationEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(0, condition.getConcentrationEvolution(block).getY(firstTimeStep));

    assertDoubleEquals(firstTimeStepValue, condition.getDiametreEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(0.01, condition.getDiametreEvolution(block).getY(firstTimeStep));

    assertDoubleEquals(firstTimeStep, condition.getEtendueEvolution(block).getX(firstTimeStep));
    assertDoubleEquals(13.7, condition.getEtendueEvolution(block).getY(firstTimeStep));

    //last time step
    block = 0;
    int lastTimeStep = 10;
    double lastTimeStepValue = 8100;

    assertDoubleEquals(lastTimeStepValue, condition.getHEvol().getX(lastTimeStep));
    assertDoubleEquals(3, condition.getHEvol().getY(lastTimeStep));
    assertDoubleEquals(lastTimeStepValue, condition.getQnEvol().getX(lastTimeStep));
    assertDoubleEquals(41, condition.getQnEvol().getY(lastTimeStep));
    assertDoubleEquals(lastTimeStepValue, condition.getQtEvol().getX(lastTimeStep));
    assertDoubleEquals(8, condition.getQtEvol().getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getConcentrationEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(0.2, condition.getConcentrationEvolution(block).getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getDiametreEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(0.022, condition.getDiametreEvolution(block).getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getEtendueEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(1.1, condition.getEtendueEvolution(block).getY(lastTimeStep));

    block = 1;
    assertDoubleEquals(lastTimeStepValue, condition.getConcentrationEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(1.1, condition.getConcentrationEvolution(block).getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getDiametreEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(0.03, condition.getDiametreEvolution(block).getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getEtendueEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(1.3, condition.getEtendueEvolution(block).getY(lastTimeStep));

    block = 2;
    assertDoubleEquals(lastTimeStepValue, condition.getConcentrationEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(0.1, condition.getConcentrationEvolution(block).getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getDiametreEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(0.51, condition.getDiametreEvolution(block).getY(lastTimeStep));

    assertDoubleEquals(lastTimeStepValue, condition.getEtendueEvolution(block).getX(lastTimeStep));
    assertDoubleEquals(13.8, condition.getEtendueEvolution(block).getY(lastTimeStep));
  }

  public void testLectureNim() {
    H2dRubarTimeConditionMutable[] read = read("nim_ns.cli");
    assertNotNull(read);
    assertEquals(16, read.length);
    for (H2dRubarTimeConditionMutable h2dRubarTimeConditionMutable : read) {
      assertEquals(56, h2dRubarTimeConditionMutable.getHEvol().getNbValues());
    }
  }

  private void assertEquals(H2dRubarTimeConditionMutable[] expected, H2dRubarTimeConditionMutable[] actual) {
    assertEquals(expected.length, actual.length);

    for (int i = 0; i < expected.length; i++) {
      assertEquals(expected[i], actual[i]);
    }
  }

  private void assertEquals(H2dRubarTimeConditionMutable expected, H2dRubarTimeConditionMutable actual) {
    assertEquals(expected.isTrans(), actual.isTrans());
    assertEquals(expected.getHEvol(), actual.getHEvol());
    assertEquals(expected.getQnEvol(), actual.getQnEvol());
    assertEquals(expected.getQtEvol(), actual.getQtEvol());

    if (expected.isTrans()) {
      assertEquals(1, expected.getNbConcentrationBlock());
      assertEquals(expected.getConcentrationEvolution(0), actual.getConcentrationEvolution(0));
      assertEquals(expected.getDiametreEvolution(0), actual.getDiametreEvolution(0));
      assertEquals(expected.getEtendueEvolution(0), actual.getEtendueEvolution(0));
    }
  }

  private void assertEquals(EvolutionReguliere expected, EvolutionReguliere actual) {
    assertEquals(expected.getNbValues(), actual.getNbValues());

    for (int i = 0; i < expected.getNbValues(); i++) {
      assertDoubleEquals(expected.getY(i), actual.getY(i));
      assertDoubleEquals(expected.getX(i), actual.getX(i));
    }
  }

  public H2dRubarTimeConditionMutable[] getConditionsWithTransNew() {
    H2dRubarTimeConditionMutable[] conditions = new H2dRubarTimeConditionMutable[3];

    conditions[0] = new H2dRubarTimeConditionMutable(2, 1);
    conditions[0].h_.add(0.0, 0.2);
    conditions[0].h_.add(0.436, 1.8);
    conditions[0].qn_.add(0.0, 0.004);
    conditions[0].qn_.add(0.436, 0.0184);
    conditions[0].qt_.add(0.0, 0.623);
    conditions[0].qt_.add(0.436, 14.7);
    conditions[0].addValueConcentration(0, 0.0, 0.5);
    conditions[0].addValueConcentration(0, 0.436, 0.0);
    conditions[0].addValueDiametre(0, 0.0, 0.1);
    conditions[0].addValueDiametre(0, 0.436, 0.9);
    conditions[0].addValueEtendue(0, 0.0, 1.0);
    conditions[0].addValueEtendue(0, 0.436, 2.3);

    conditions[1] = new H2dRubarTimeConditionMutable(2, 1);
    conditions[1].h_.add(0.0, 0.7);
    conditions[1].h_.add(0.436, 0.7);
    conditions[1].qn_.add(0.0, 0.0063);
    conditions[1].qn_.add(0.436, 0.0063);
    conditions[1].qt_.add(0.0, 2.3);
    conditions[1].qt_.add(0.436, 2.3);
    conditions[1].addValueConcentration(0, 0.0, 1.0);
    conditions[1].addValueConcentration(0, 0.436, 1.0);
    conditions[1].addValueDiametre(0, 0.0, 0.4);
    conditions[1].addValueDiametre(0, 0.436, 0.4);
    conditions[1].addValueEtendue(0, 0.0, 1.7);
    conditions[1].addValueEtendue(0, 0.436, 1.7);

    conditions[2] = new H2dRubarTimeConditionMutable(2, 1);
    conditions[2].h_.add(0.0, 1.8);
    conditions[2].h_.add(0.436, 0.2);
    conditions[2].qn_.add(0.0, 0.0184);
    conditions[2].qn_.add(0.436, 0.004);
    conditions[2].qt_.add(0.0, 14.7);
    conditions[2].qt_.add(0.436, 0.623);
    conditions[2].addValueConcentration(0, 0.0, 0.0);
    conditions[2].addValueConcentration(0, 0.436, 0.5);
    conditions[2].addValueDiametre(0, 0.0, 0.9);
    conditions[2].addValueDiametre(0, 0.436, 0.1);
    conditions[2].addValueEtendue(0, 0.0, 2.3);
    conditions[2].addValueEtendue(0, 0.436, 1.0);

    return conditions;
  }

  public H2dRubarTimeConditionMutable[] getConditionsWithTransOld() {
    H2dRubarTimeConditionMutable[] conditions = new H2dRubarTimeConditionMutable[3];

    conditions[0] = new H2dRubarTimeConditionMutable(2, 1);
    conditions[0].h_.add(0.0, 0.2);
    conditions[0].h_.add(0.436, 1.8);
    conditions[0].qn_.add(0.0, 0.004);
    conditions[0].qn_.add(0.436, 0.0184);
    conditions[0].qt_.add(0.0, 0.623);
    conditions[0].qt_.add(0.436, 14.7);
    conditions[0].addValueConcentration(0, 0.0, 0.5);
    conditions[0].addValueConcentration(0, 0.436, 0.0);
    conditions[0].addValueDiametre(0, 0.0, 0.0);
    conditions[0].addValueDiametre(0, 0.436, 0.0);
    conditions[0].addValueEtendue(0, 0.0, 0.0);
    conditions[0].addValueEtendue(0, 0.436, 0.0);

    conditions[1] = new H2dRubarTimeConditionMutable(2, 1);
    conditions[1].h_.add(0.0, 0.7);
    conditions[1].h_.add(0.436, 0.7);
    conditions[1].qn_.add(0.0, 0.0063);
    conditions[1].qn_.add(0.436, 0.0063);
    conditions[1].qt_.add(0.0, 2.3);
    conditions[1].qt_.add(0.436, 2.3);
    conditions[1].addValueConcentration(0, 0.0, 1.0);
    conditions[1].addValueConcentration(0, 0.436, 1.0);
    conditions[1].addValueDiametre(0, 0.0, 0.0);
    conditions[1].addValueDiametre(0, 0.436, 0.0);
    conditions[1].addValueEtendue(0, 0.0, 0.0);
    conditions[1].addValueEtendue(0, 0.436, 0.0);

    conditions[2] = new H2dRubarTimeConditionMutable(2, 1);
    conditions[2].h_.add(0.0, 1.8);
    conditions[2].h_.add(0.436, 0.2);
    conditions[2].qn_.add(0.0, 0.0184);
    conditions[2].qn_.add(0.436, 0.004);
    conditions[2].qt_.add(0.0, 14.7);
    conditions[2].qt_.add(0.436, 0.623);
    conditions[2].addValueConcentration(0, 0.0, 0.0);
    conditions[2].addValueConcentration(0, 0.436, 0.5);
    conditions[2].addValueDiametre(0, 0.0, 0.0);
    conditions[2].addValueDiametre(0, 0.436, 0.0);
    conditions[2].addValueEtendue(0, 0.0, 0.0);
    conditions[2].addValueEtendue(0, 0.436, 0.0);

    return conditions;
  }

  public H2dRubarTimeConditionMutable[] getConditions() {
    H2dRubarTimeConditionMutable[] conditions = new H2dRubarTimeConditionMutable[3];

    conditions[0] = new H2dRubarTimeConditionMutable(2, 0);
    conditions[0].h_.add(0.0, 0.2);
    conditions[0].h_.add(0.436, 1.8);
    conditions[0].qn_.add(0.0, 0.004);
    conditions[0].qn_.add(0.436, 0.0184);
    conditions[0].qt_.add(0.0, 0.623);
    conditions[0].qt_.add(0.436, 14.7);

    conditions[1] = new H2dRubarTimeConditionMutable(2, 0);
    conditions[1].h_.add(0.0, 0.7);
    conditions[1].h_.add(0.436, 0.7);
    conditions[1].qn_.add(0.0, 0.0063);
    conditions[1].qn_.add(0.436, 0.0063);
    conditions[1].qt_.add(0.0, 2.3);
    conditions[1].qt_.add(0.436, 2.3);

    conditions[2] = new H2dRubarTimeConditionMutable(2, 0);
    conditions[2].h_.add(0.0, 1.8);
    conditions[2].h_.add(0.436, 0.2);
    conditions[2].qn_.add(0.0, 0.0184);
    conditions[2].qn_.add(0.436, 0.004);
    conditions[2].qt_.add(0.0, 14.7);
    conditions[2].qt_.add(0.436, 0.623);

    return conditions;
  }

  private static H2dRubarTimeConditionMutable[] read(String file) {
    return read(getFile(TestRubarCLIFileFormat.class, file), false);
  }

  private static H2dRubarTimeConditionMutable[] read(File file, boolean newFormat) {
    final RubarCLIReader reader = (RubarCLIReader) new RubarCLIFileFormat().createReader();

    reader.setFile(file);

    final CtuluIOOperationSynthese result = reader.read();
    assertEquals(newFormat, reader.isNewFormat());
    assertFalse(result.containsSevereError());

    return (H2dRubarTimeConditionMutable[]) result.getSource();
  }

  public void testEcriture() {
    H2dRubarTimeConditionMutable[] conditions = getConditions();

    final RubarCLIWriter writer = (RubarCLIWriter) new RubarCLIFileFormat().createWriter();
    File file = null;

    try {
      file = File.createTempFile("Test", "RubarCLIFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    CtuluIOOperationSynthese operation = writer.write(conditions);
    assertFalse(operation.containsSevereError());
    assertEquals(getConditions(), read(file, false));

    conditions = getConditionsWithTransNew();

    try {
      file = File.createTempFile("Test", "RubarCLIFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    operation = writer.write(conditions);
    assertFalse(operation.containsSevereError());
    assertEquals(getConditionsWithTransNew(), read("lmfa05_trans_new.cli"));
  }
}
