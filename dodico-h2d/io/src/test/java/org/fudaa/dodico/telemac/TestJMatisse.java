/**
 * @creation 2 avr. 2004
 * @modification $Date: 2007-01-19 13:07:18 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import org.fudaa.dodico.common.TestIO;

/**
 * @author Fred Deniger
 * @version $Id: TestJMatisse.java,v 1.1 2007-01-19 13:07:18 deniger Exp $
 */
public class TestJMatisse extends TestIO {

  /**
   * BathygeoO1
   */
  public TestJMatisse() {
    super("Bathygeo01.mat");
  }

  /**
   * Test la lecture.
   */
  public void testReader(){
//    File f = fic_;
//    MatisseReader r = new MatisseReader();
//    r.setFile(f);
//    System.out.println("Bathygeo1.mat");
//    CtuluIOOperationSynthese s = r.read();
//    if (s.containsMessages()) s.getAnalyze().printResume("Message: ");
  //  MNT mnt = (MNT) s.getSource();
//    MNTZone z1 = mnt.zone(0);
    //MNTPolygone poly=z1.getPolygone(0);
  }

  /**
   * Lecture du fichier BATHYGEO.
   */
  public void testReaderBug(){
//    File f = getFile("BATHYGEO");
//    MatisseReader r = new MatisseReader();
//    r.setFile(f);
//    r.setMachineId(NativeBinaryInputStream.SPARC);
//    System.out.println("BATHYGEO");
//    CtuluIOOperationSynthese s = r.read();
//    if (s.containsMessages()) s.getAnalyze().printResume("Message: ");
  }

}