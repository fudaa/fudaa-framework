/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TDoubleArrayList;

import java.io.File;
import java.util.List;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.rubar.io.RubarDFZResult.RubarDZFResultBloc;

/**
 * @author CANEL Christophe (Genesis)
 *
 */
public class TestRubarDZFFileFormat extends TestIO {
  public TestRubarDZFFileFormat()
  {
    super("lmfa05.dzf");
  }
  
  public void testLecture()
  {
    assertCorrect(read(this.fic_));
  }

  private void assertCorrect(RubarDFZResult result)
  {
    final List<RubarDZFResultBloc> blocs = result.getBlocs();

    assertEquals(3, blocs.size());
  }

  private void assertCorrectBloc1(RubarDZFResultBloc bloc)
  {
    assertDoubleEquals(0.0, bloc.getTime());

    final TDoubleArrayList dzfs = bloc.getDzfs();
    
    assertDoubleEquals(0.0, dzfs.get(0));
    assertDoubleEquals(0.0, dzfs.get(1));
    assertDoubleEquals(0.0, dzfs.get(2));
    assertDoubleEquals(0.0, dzfs.get(3));
    assertDoubleEquals(0.0, dzfs.get(4));
    assertDoubleEquals(0.0, dzfs.get(5));
    assertDoubleEquals(0.0, dzfs.get(6));
    assertDoubleEquals(0.0, dzfs.get(7));
    assertDoubleEquals(0.0, dzfs.get(8));
    assertDoubleEquals(0.0, dzfs.get(9));
    assertDoubleEquals(0.0, dzfs.get(10));
    assertDoubleEquals(0.0, dzfs.get(11));
    assertDoubleEquals(0.0, dzfs.get(12));
    assertDoubleEquals(0.0, dzfs.get(13));
  }

  private void assertCorrectBloc2(RubarDZFResultBloc bloc)
  {
    assertDoubleEquals(0.503, bloc.getTime());

    final TDoubleArrayList dzfs = bloc.getDzfs();
    
    assertDoubleEquals(0.00885, dzfs.get(0));
    assertDoubleEquals(0.00885, dzfs.get(1));
    assertDoubleEquals(0.01768, dzfs.get(2));
    assertDoubleEquals(0.01767, dzfs.get(3));
    assertDoubleEquals(0.00885, dzfs.get(4));
    assertDoubleEquals(0.01767, dzfs.get(5));
    assertDoubleEquals(0.00885, dzfs.get(6));
    assertDoubleEquals(0.01767, dzfs.get(7));
    assertDoubleEquals(0.00885, dzfs.get(8));
    assertDoubleEquals(0.01767, dzfs.get(9));
    assertDoubleEquals(0.0, dzfs.get(10));
    assertDoubleEquals(0.0, dzfs.get(11));
    assertDoubleEquals(0.00009, dzfs.get(12));
    assertDoubleEquals(0.00017, dzfs.get(13));
  }

  private void assertCorrectBloc3(RubarDZFResultBloc bloc)
  {
    assertDoubleEquals(1.0, bloc.getTime());

    final TDoubleArrayList dzfs = bloc.getDzfs();
    
    assertDoubleEquals(0.01758, dzfs.get(0));
    assertDoubleEquals(0.01757, dzfs.get(1));
    assertDoubleEquals(0.03511, dzfs.get(2));
    assertDoubleEquals(0.03510, dzfs.get(3));
    assertDoubleEquals(0.01757, dzfs.get(4));
    assertDoubleEquals(0.03508, dzfs.get(5));
    assertDoubleEquals(0.01756, dzfs.get(6));
    assertDoubleEquals(0.03507, dzfs.get(7));
    assertDoubleEquals(0.01756, dzfs.get(8));
    assertDoubleEquals(0.03507, dzfs.get(9));
    assertDoubleEquals(0.0, dzfs.get(10));
    assertDoubleEquals(0.0, dzfs.get(11));
    assertDoubleEquals(0.00017, dzfs.get(12));
    assertDoubleEquals(0.00034, dzfs.get(13));
  }
  
  private static RubarDFZResult read(File file)
  {
    final RubarDZFReader reader = (RubarDZFReader)new RubarDZFFileFormat().createReader();

    reader.setFile(file);
    reader.setNbNoeuds(14);
    
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    
    return (RubarDFZResult)result.getSource();
  }
}
