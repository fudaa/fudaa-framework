/**
 * @creation 10 sept. 2003
 * @modification $Date: 2007-05-22 13:11:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.telemac;

import java.io.File;

import junit.framework.TestCase;

import org.fudaa.ctulu.CtuluIOOperationSynthese;

import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.telemac.io.TelemacCLFileFormat;

/**
 * @author deniger
 * @version $Id: TestJClMng.java,v 1.2 2007-05-22 13:11:26 deniger Exp $
 */
public class TestJClMng extends TestCase {

  EfGridInterface m_;

  H2dTelemacCLSourceInterface cl_;

  /**
   * Constructeur: lit les fichiers "testClMng....".
   */
  public TestJClMng() {
    final Class c = getClass();
    final File geoFile = TestIO.getFile(c, "testClMng.ser");
    final File cliFile = TestIO.getFile(c, "testClMng.cli");
    assertNotNull(geoFile);
    assertTrue(geoFile.exists());
    assertNotNull(cliFile);
    assertTrue(cliFile.exists());
    CtuluIOOperationSynthese op = SerafinFileFormat.getInstance().read(geoFile, null);
    assertFalse(op.containsSevereError());
    final SerafinInterface inter = (SerafinInterface) op.getSource();
    m_ = inter.getGrid();
    m_.computeBordFast(inter.getIpoboFr(), null, null);
    op = TelemacCLFileFormat.getInstance().getLastVersionImpl().read(cliFile, null);
    assertFalse(op.containsSevereError());
    cl_ = ((H2dTelemacCLSourceInterface) op.getSource());
  }

  /**
   * Test le maillage : frontiere,noeuds et elements.
   */
  public void testMaillage() {
    assertNotNull(m_);
    assertNotNull(m_.getFrontiers());
    final EfFrontierInterface frontiere = m_.getFrontiers();
    assertEquals(2, frontiere.getNbFrontier());
    //test extern frontier
    assertEquals(282, frontiere.getNbPt(0));
    assertEquals(13, frontiere.getIdxGlobalPrinc(0));
    assertEquals(28, frontiere.getIdxGlobalPrinc(281));
    //test intern frontier
    assertEquals(16, frontiere.getNbPt(1));
    assertEquals(232, frontiere.getIdxGlobal(1, 0));
    assertEquals(23, frontiere.getIdxGlobal(1, 15));
  }

  /**
   * Test les conditions lu.
   */
  public void testCl() {
    internalTestCl(cl_);
  }

  private static void internalTestCl(final H2dTelemacCLSourceInterface _cl) {
    assertEquals(298, _cl.getNbLines());
    final H2dTelemacCLElementSource s = new H2dTelemacCLElementSource();
    /** extern frontier* */
    //bord 0
    for (int i = 270; i < 282; i++) {
      _cl.getLine(i, s);
      assertEquals(H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE, s.bordType_);
    }
    _cl.getLine(0, s);
    assertEquals(H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE, s.bordType_);
    //  bord 1
    for (int i = 1; i < 138; i++) {
      _cl.getLine(i, s);
      assertEquals(H2dBoundaryTypeCommon.SOLIDE, s.bordType_);
    }
    //  bord 2
    for (int i = 138; i < 149; i++) {
      _cl.getLine(i, s);
      assertEquals(H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE, s.bordType_);
    }
    //  bord 3
    for (int i = 149; i < 270; i++) {
      _cl.getLine(i, s);
      assertEquals(H2dBoundaryTypeCommon.SOLIDE, s.bordType_);
    }
    /** intern frontier* */
    for (int i = 282; i < 298; i++) {
      _cl.getLine(i, s);
      assertEquals(H2dBoundaryTypeCommon.SOLIDE, s.bordType_);
    }
  }

  
}
