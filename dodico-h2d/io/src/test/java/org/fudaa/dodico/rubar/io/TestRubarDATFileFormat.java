/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource;

import java.io.File;
import java.io.IOException;

/**
 */
public class TestRubarDATFileFormat extends TestIO {
  public TestRubarDATFileFormat() {
    super("datOldFormat.dat");
  }

  public void testLecture() {
    final H2dRubarGridAreteSource read = read(this.fic_, false);
    testOldMaiContent(read);
  }

  public void testLectureNewFormat() {
    final H2dRubarGridAreteSource read = read(getFile("datNewFormat.dat"), true);
    testNewMaiContent(read);
  }

  public void testEcritureNewFormat() {
    final H2dRubarGridAreteSource read = read(getFile("datNewFormat.dat"), true);
    testNewMaiContent(write(read, false));
    testNewMaiContent(write(read, true));
  }

  public void testEcriture() {
    final H2dRubarGridAreteSource read = read(this.fic_, false);

    testOldMaiContent(write(read, false));
    testOldMaiContent(write(read, true));
  }

  private H2dRubarGridAreteSource write(H2dRubarGridAreteSource grid, boolean newFormat) {
    final RubarDATWriter writer = (RubarDATWriter) new RubarDATFileFormat().createWriter();
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarDATFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    writer.setNewFormat(newFormat);
    final CtuluIOOperationSynthese operation = writer.write(grid);
    assertFalse(operation.containsSevereError());
    final H2dRubarGridAreteSource h2dRubarGrid = read(file, newFormat);
    file.delete();
    return h2dRubarGrid;
  }

  private void testOldMaiContent(H2dRubarGridAreteSource src) {
    final H2dRubarGrid grid = src.getRubarGrid();
    assertEquals(4226, grid.getNbAretes());
    assertEquals(0, grid.getArete(0).getPt1Idx());
    assertEquals(2, grid.getArete(0).getPt2Idx());
    assertEquals(2044, grid.getArete(4225).getPt1Idx());
    assertEquals(2045, grid.getArete(4225).getPt2Idx());
    assertEquals(2181, grid.getEltNb());
    final EfElementVolume first = grid.getEltVolume(0);
    assertEquals(4, first.getPtNb());
    assertEquals(0, first.getPtIndex(0));
    assertEquals(2, first.getPtIndex(1));
    assertEquals(3, first.getPtIndex(2));
    assertEquals(1, first.getPtIndex(3));
    final EfElementVolume last = grid.getEltVolume(2180);
    assertEquals(4, last.getPtNb());
    assertEquals(2044, last.getPtIndex(0));
    assertEquals(2029, last.getPtIndex(1));
    assertEquals(2030, last.getPtIndex(2));
    assertEquals(2045, last.getPtIndex(3));

    assertEquals(2046, grid.getPtsNb());
    assertDoubleEquals(7444.0, grid.getPtX(0));
    assertDoubleEquals(33046.9, grid.getPtY(0));
    assertDoubleEquals(199.911, grid.getPtZ(0));

    assertDoubleEquals(7726.1, grid.getPtX(2045));
    assertDoubleEquals(33472.4, grid.getPtY(2045));
    assertDoubleEquals(200.179, grid.getPtZ(2045));
  }

  private void testNewMaiContent(H2dRubarGridAreteSource src) {
    final H2dRubarGrid grid = src.getRubarGrid();
    assertEquals(2819, grid.getEltNb());
    final EfElementVolume first = grid.getEltVolume(0);
    assertEquals(4, first.getPtNb());
    assertEquals(0, first.getPtIndex(0));
    assertEquals(2, first.getPtIndex(1));
    assertEquals(3, first.getPtIndex(2));
    assertEquals(1, first.getPtIndex(3));
    final EfElementVolume last = grid.getEltVolume(2818);
    assertEquals(4, last.getPtNb());
    assertEquals(3014, last.getPtIndex(0));
    assertEquals(3015, last.getPtIndex(1));
    assertEquals(3026, last.getPtIndex(2));
    assertEquals(3025, last.getPtIndex(3));

    assertEquals(3027, grid.getPtsNb());
    assertDoubleEquals(8793.1, grid.getPtX(0));
    assertDoubleEquals(-163.8, grid.getPtY(0));
    assertDoubleEquals(234.443, grid.getPtZ(0));

    assertDoubleEquals(9502.5, grid.getPtX(3026));
    assertDoubleEquals(4132.9, grid.getPtY(3026));
    assertDoubleEquals(230.233, grid.getPtZ(3026));
  }

  private static H2dRubarGridAreteSource read(File file, boolean newFormat) {
    final RubarDATReader reader = (RubarDATReader) new RubarDATFileFormat().createReader();
    reader.setFile(file);
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    assertEquals(newFormat, reader.isNewFormat());
    return (H2dRubarGridAreteSource) result.getSource();
  }
}
