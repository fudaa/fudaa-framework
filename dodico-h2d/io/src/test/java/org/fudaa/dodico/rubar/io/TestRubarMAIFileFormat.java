/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;

import java.io.File;
import java.io.IOException;

/**
 */
public class TestRubarMAIFileFormat extends TestIO {
  public TestRubarMAIFileFormat() {
    super("maiOld.mai");
  }

  public void testLecture() {
    final H2dRubarGrid read = read(this.fic_, false);
    testOldMaiContent(read);
  }

  public void testLectureNewFormat() {
    final H2dRubarGrid read = read(getFile("maiNew.mai"), true);
    testNewMaiContent(read);
  }

  public void testEcritureNewFormat() {
    final H2dRubarGrid read = read(getFile("maiNew.mai"), true);
    testNewMaiContent(write(read, false));
    testNewMaiContent(write(read, true));
  }

  public void testEcriture() {
    final H2dRubarGrid read = read(this.fic_, false);

    testOldMaiContent(write(read, false));
    testOldMaiContent(write(read, true));
  }

  private H2dRubarGrid write(H2dRubarGrid grid, boolean newFormat) {
    final RubarMAIWriter writer = (RubarMAIWriter) new RubarMAIFileFormat().createWriter();
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarMAIFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }

    writer.setFile(file);
    writer.setNewFormat(newFormat);
    final CtuluIOOperationSynthese operation = writer.write(grid);
    assertFalse(operation.containsSevereError());
    final H2dRubarGrid h2dRubarGrid = read(file, newFormat);
    file.delete();
    return h2dRubarGrid;
  }

  private void testOldMaiContent(H2dRubarGrid grid) {
    assertEquals(111464, grid.getEltNb());
    final EfElementVolume first = grid.getEltVolume(0);
    assertEquals(4, first.getPtNb());
    assertEquals(0, first.getPtIndex(0));
    assertEquals(2, first.getPtIndex(1));
    assertEquals(3, first.getPtIndex(2));
    assertEquals(1, first.getPtIndex(3));
    final EfElementVolume last = grid.getEltVolume(111463);
    assertEquals(4, last.getPtNb());
    assertEquals(115115, last.getPtIndex(0));
    assertEquals(115152, last.getPtIndex(1));
    assertEquals(115153, last.getPtIndex(2));
    assertEquals(115116, last.getPtIndex(3));

    assertEquals(115154, grid.getPtsNb());
    assertDoubleEquals(1578.72, grid.getPtX(0));
    assertDoubleEquals(2804.01, grid.getPtY(0));
    assertDoubleEquals(0, grid.getPtZ(0));

    assertDoubleEquals(2795.49, grid.getPtX(115153));
    assertDoubleEquals(2828.37, grid.getPtY(115153));
    assertDoubleEquals(0, grid.getPtZ(115153));
  }

  private void testNewMaiContent(H2dRubarGrid grid) {
    assertEquals(2819, grid.getEltNb());
    final EfElementVolume first = grid.getEltVolume(0);
    assertEquals(4, first.getPtNb());
    assertEquals(0, first.getPtIndex(0));
    assertEquals(2, first.getPtIndex(1));
    assertEquals(3, first.getPtIndex(2));
    assertEquals(1, first.getPtIndex(3));
    final EfElementVolume last = grid.getEltVolume(2818);
    assertEquals(4, last.getPtNb());
    assertEquals(3014, last.getPtIndex(0));
    assertEquals(3015, last.getPtIndex(1));
    assertEquals(3026, last.getPtIndex(2));
    assertEquals(3025, last.getPtIndex(3));

    assertEquals(3027, grid.getPtsNb());
    assertDoubleEquals(8793.1, grid.getPtX(0));
    assertDoubleEquals(-163.8, grid.getPtY(0));
    assertDoubleEquals(0, grid.getPtZ(0));

    assertDoubleEquals(9502.5, grid.getPtX(3026));
    assertDoubleEquals(4132.9, grid.getPtY(3026));
    assertDoubleEquals(0, grid.getPtZ(3026));
  }

  private static H2dRubarGrid read(File file, boolean newFormat) {
    final RubarMAIReader reader = (RubarMAIReader) new RubarMAIFileFormat().createReader();
    reader.setFile(file);
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    assertEquals(newFormat, reader.isNewFormat());
    return (H2dRubarGrid) result.getSource();
  }
}
