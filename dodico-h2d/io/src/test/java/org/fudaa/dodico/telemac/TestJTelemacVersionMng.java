/**
 * @creation 11 janv. 2005
 * @modification $Date: 2007-01-19 13:07:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import junit.framework.TestCase;
import org.fudaa.dodico.commun.DodicoPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: TestJTelemacVersionMng.java,v 1.1 2007-01-19 13:07:17 deniger Exp $
 */
public class TestJTelemacVersionMng extends TestCase {
  /**
   * Test simple du manager de version.
   */
  public void testProps() {

    final TelemacVersionManager mng = TelemacVersionManager.INSTANCE;
    assertNotNull(mng);
    mng.removeAll(false);
    DodicoPreferences.DODICO.putStringProperty("install.telemac.exe.dir", "C:\\telemac\\V5P4\\bin");
    mng.loadProperties();
    final String version = "V5P4";
    mng.removeAll(false);
    assertEquals(0, mng.getNbVersion());
    assertEquals("V5_4", TelemacVersionManager.cleanVersion("V5;4"));
    final Map test = new HashMap();
    test.put(version, "toto");
    test.put("V5P3", "toto2");
    mng.setVersion(test);
    internTest(mng);
    mng.saveVersions(true);
    mng.loadProperties();
    internTest(mng);
  }

  private void internTest(final TelemacVersionManager _mng) {
    assertEquals(2, _mng.getNbVersion());
    assertEquals("V5P3", _mng.getVersion(0));
    assertEquals("toto2", _mng.getVersionPath(0));
    assertEquals("V5P4", _mng.getVersion(1));
    assertEquals("toto", _mng.getVersionPath(1));
  }
}
