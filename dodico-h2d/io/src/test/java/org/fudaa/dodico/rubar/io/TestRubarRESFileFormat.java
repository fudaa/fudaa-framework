/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;
import java.util.List;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;

/**
 * @author CANEL Christophe (Genesis)
 *
 */
public class TestRubarRESFileFormat extends TestIO {
  public TestRubarRESFileFormat()
  {
    super("lmfa05.res");
  }
  
  public void testLecture()
  {
    assertCorrect(read(this.fic_));
  }

  private void assertCorrect(RubarRESResult result)
  {
    assertCorrectParameters(result.getParameters());
    assertCorrectResultsGlobaux(result.getResultatsGeneral());
    assertCorrectResultsOuvrageB(result.getResultatsOuvrageB());
  }
    
  private void assertCorrectParameters(RubarRESResultParameters parameters)
  {
    assertDoubleEquals(0.0, parameters.getIt());
    assertDoubleEquals(22.0, parameters.getCoteEnCrete());
    assertDoubleEquals(2.0, parameters.getLargeurEnCrete());
    assertDoubleEquals(19.0, parameters.getCoteEnPied());
    assertDoubleEquals(10.0, parameters.getLargeurEnPied());
    assertDoubleEquals(0.00001, parameters.getDiametreGrains());
    assertDoubleEquals(0.4, parameters.getPorosite());
    assertDoubleEquals(2650.0, parameters.getMasseVolumique());
    assertDoubleEquals(30.0, parameters.getStricklerDonne());
    assertDoubleEquals(143.1, parameters.getStricklerCalcule());
    assertDoubleEquals(0.050000000000000266, parameters.getLargeurMaxBreche());
    assertDoubleEquals(0.1, parameters.getTempsDebutRupture());
    assertDoubleEquals(0.01, parameters.getCoteTerrainNaturel());
    assertDoubleEquals(19.0, parameters.getCoteBreche());
    assertDoubleEquals(0.1, parameters.getRayonRenard());
    assertDoubleEquals(0.0, parameters.getVolumeErode());
    assertDoubleEquals(-0.0, parameters.getVolumeParti());
  }

  private void assertCorrectResultsGlobaux(List<RubarRESResultLine> lines)
  {
    assertEquals(2, lines.size());

    RubarRESResultLine line = lines.get(0);
    
    assertEquals(0, line.getHeure());
    assertEquals(0, line.getMinute());
    assertEquals(1, line.getSecond());
    assertDoubleEquals(0.0, line.getCoteEauAmont());
    assertDoubleEquals(0.0, line.getLargeurDiametreBreche());
    assertDoubleEquals(0.0, line.getCoteFondBreche());
    assertDoubleEquals(0.0, line.getDebitLiquide());
    assertDoubleEquals(0.0, line.getDebitSolide());

    line = lines.get(1);
    
    assertEquals(0, line.getHeure());
    assertEquals(0, line.getMinute());
    assertEquals(2, line.getSecond());
    assertDoubleEquals(0.0, line.getCoteEauAmont());
    assertDoubleEquals(0.0, line.getLargeurDiametreBreche());
    assertDoubleEquals(0.0, line.getCoteFondBreche());
    assertDoubleEquals(0.0, line.getDebitLiquide());
    assertDoubleEquals(0.0, line.getDebitSolide());
  }

  private void assertCorrectResultsOuvrageB(List<RubarRESResultLine> lines)
  {
    assertEquals(19, lines.size());

    RubarRESResultLine line = lines.get(0);
    
    assertEquals(0, line.getHeure());
    assertEquals(0, line.getMinute());
    assertEquals(0, line.getSecond());
    assertDoubleEquals(0.0823, line.getCoteEauAmont());
    assertDoubleEquals(19.0, line.getCoteFondBreche());
    assertDoubleEquals(0.2, line.getLargeurDiametreBreche());
    assertDoubleEquals(0.0, line.getDebitLiquide());
    assertDoubleEquals(0.0, line.getDebitSolide());

    line = lines.get(1);
    
    assertEquals(0, line.getHeure());
    assertEquals(0, line.getMinute());
    assertEquals(0, line.getSecond());
    assertDoubleEquals(19.0, line.getCoteEauAmont());
    assertDoubleEquals(19.0, line.getCoteFondBreche());
    assertDoubleEquals(0.05, line.getLargeurDiametreBreche());
    assertDoubleEquals(0.0, line.getDebitLiquide());
    assertDoubleEquals(0.0, line.getDebitSolide());
  }
  
  private static RubarRESResult read(File file)
  {
    final RubarRESReader reader = (RubarRESReader)new RubarRESFileFormat().createReader();

    reader.setFile(file);
    
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    
    return (RubarRESResult)result.getSource();
  }
}
