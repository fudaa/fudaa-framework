/**
 *
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

import java.io.File;

public class RubarOutReaderTest extends TestIO {
  public RubarOutReaderTest() {
    super("outOld.out");
  }

  public void testLecture() {
    testOldContent(read(this.fic_, false));
  }

  public void testLectureNew() {
    testNewContent(read(getFile("outNew.out"), false));
  }

  private void testOldContent(TIntObjectHashMap result) {
    assertEquals(65, result.size());
    RubarOutEdgeResult container = (RubarOutEdgeResult) result.get(10);
    assertDoubleEquals(252.1515, container.getZCentre());
    final EvolutionReguliere evolHeMod = container.getEvolutionModifiable(H2dVariableType.HAUTEUR_EAU);
    assertEquals(34, evolHeMod.getNbValues());

    assertDoubleEquals(600.0534645091877, evolHeMod.getX(0));
    assertDoubleEquals(0.0020, evolHeMod.getY(0));

    assertDoubleEquals(20000, evolHeMod.getX(33));
    assertDoubleEquals(0.0020, evolHeMod.getY(33));
  }

  private void testNewContent(TIntObjectHashMap result) {
    assertEquals(174, result.size());
    RubarOutEdgeResult container = (RubarOutEdgeResult) result.get(198217);
    assertDoubleEquals(16, container.getXCentre());
    final EvolutionReguliere evolHeMod = container.getEvolutionModifiable(H2dVariableType.HAUTEUR_EAU);
    assertEquals(4, evolHeMod.getNbValues());

    assertDoubleEquals(0.1000000000000001, evolHeMod.getX(0));
    assertDoubleEquals(0, evolHeMod.getY(0));

    assertDoubleEquals(20, evolHeMod.getX(3));
    assertDoubleEquals(0.0845, evolHeMod.getY(3));
  }

  private static TIntObjectHashMap read(File file, boolean concentration) {
    final RubarOutReader reader = new RubarOutReader();
    reader.setReadConcentration(concentration);
    reader.setFile(file);
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    return (TIntObjectHashMap) result.getSource();
  }
}
