package org.fudaa.dodico.telemac;

import java.io.File;

import junit.framework.TestCase;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.telemac.io.ScopeSFileFormat;
import org.fudaa.dodico.telemac.io.ScopeStructure;

public class TestJScopeSorT extends TestCase {
  /**
   * Le fichier de lecture.
   */
  String testLectureFic_;

 String[] listeExemplesAtester = { "scope.scopT", "scope.scopS", "exemple.scopT", "exemple.scopS" };

  /**
   * Constructeur par defaut.
   */
  public TestJScopeSorT() {}

  /**
   * @see junit.framework.TestCase#setUp()
   */
  @Override
  public void setUp() {
    testLectureFic_ = listeExemplesAtester[2];
    
  }

  /**
   * @param _f
   * @return Les zones lues
   * @throws NumberFormatException
   */
  private ScopeStructure lecture(final File _f) {
    
    final CtuluIOOperationSynthese op = ScopeSFileFormat.getInstance().getLastVersionInstance(null).read(_f, null);
    final ScopeStructure struct = (ScopeStructure) op.getSource();

    return struct;
  }

  private void ecriture(final File _f, ScopeStructure struct) {
    ScopeSFileFormat.getInstance().getLastVersionInstance(null).write(_f, struct, null);
  }
  
  public void lectureEcriture(int typeFichier) {
    File f = null;
    try {
      final File initFile = getFile(listeExemplesAtester[typeFichier]);
      assertNotNull("Le fichier " + listeExemplesAtester[typeFichier] + " est introuvable", initFile);
      assertTrue(initFile.getAbsolutePath() + " n'existe pas !", initFile.exists());
       ScopeStructure structureScope = lecture(initFile);
      assertNotNull(structureScope);
      
      // --tes tecriture contenu --//
      System.err.print("\n\n\n***contenu structure du fichier " + listeExemplesAtester[typeFichier] + "***");
      
      f = File.createTempFile("scopeEcriture", ".scopT");
      System.err.print("\n\nemplacement copie: " + f.getAbsolutePath());
      assertNotNull(f);
      ecriture(f, structureScope);
      structureScope = lecture(f);
      System.err.print("\n\n\n***contenu structure du fichier apres ecriture " + listeExemplesAtester[typeFichier]
          + "***");
      
      
    } catch (final Exception _e) {
      _e.printStackTrace();
    } finally {
       if (f != null) {
        f.delete();
      }
    }
  }
  
  public void testLectureEcritureScopeS1() {
    lectureEcriture(1);
  }

  public void testLectureEcritureScopeS2() {
    lectureEcriture(3);
  }

  public void testLectureEcritureScopeT1() {
    lectureEcriture(0);
  }

  public void testLectureEcritureScopeT2() {
    lectureEcriture(2);
  }

  
  /**
   * @param _f
   * @return File
   */
  public File getFile(final String _f) {
    return TestIO.getFile(getClass(), _f);
  }
  
  
}