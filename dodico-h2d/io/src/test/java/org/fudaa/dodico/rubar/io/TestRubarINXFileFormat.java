/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.fortran.FortranDoubleReaderResult;

import java.io.File;
import java.io.IOException;

public class TestRubarINXFileFormat extends TestIO {
  private static final int NB_NOEUDS = 1434;

  public TestRubarINXFileFormat() {
    super("inxOld.inx");
  }

  public void testLecture() {
    assertCorrect(read(this.fic_));
  }

  public void testLectureNewFormatNewTransport() {
    assertCorrectNewFormatNewTransport(read(getFile("inxNewTransportNewFormat.inx")));
  }

  public void testLectureOldFormatNewTransport() {
    assertCorrectNewFormatNewTransport(read(getFile("inxNewTransportOldFormat.inx")));
  }

  public void testEcritureNewFormatNewTransport() {
    assertCorrectNewFormatNewTransport(write(read(getFile("inxNewTransportNewFormat.inx")), true));
  }

  public void testEcritureOldFormatNewTransport() {
    assertCorrectNewFormatNewTransport(write(read(getFile("inxNewTransportOldFormat.inx")), false));
  }

  public void testEcriture() {
    assertCorrect(write(read(this.fic_), false));
  }

  private void assertCorrectNewFormatNewTransport(FortranDoubleReaderResult result) {
    assertEquals(2819, result.getNbLigne());
    assertEquals(15, result.getNbColonne());

    assertDoubleEquals(8803.775, result.getValue(0, 0));
    assertDoubleEquals(-173.7, result.getValue(0, 1));
    assertDoubleEquals(0, result.getValue(0, 2));
    assertDoubleEquals(0, result.getValue(0, 3));
    assertDoubleEquals(0, result.getValue(0, 4));
    assertDoubleEquals(9999.99, result.getValue(0, 5));
    assertDoubleEquals(0.00025, result.getValue(0, 13));
    assertDoubleEquals(5.366, result.getValue(0, 14));

    assertDoubleEquals(8997.2, result.getValue(29, 0));
    assertDoubleEquals(-119.45, result.getValue(29, 1));
    assertDoubleEquals(0, result.getValue(29, 2));
    assertDoubleEquals(0, result.getValue(29, 3));
    assertDoubleEquals(0, result.getValue(29, 4));
    assertDoubleEquals(9999.99, result.getValue(29, 5));
    assertDoubleEquals(0.00025, result.getValue(29, 13));
    assertDoubleEquals(5.367, result.getValue(29, 14));

    assertDoubleEquals(9490.2, result.getValue(2818, 0));
    assertDoubleEquals(4135.275, result.getValue(2818, 1));
    assertDoubleEquals(0, result.getValue(2818, 2));
    assertDoubleEquals(0, result.getValue(2818, 3));
    assertDoubleEquals(0, result.getValue(2818, 4));
    assertDoubleEquals(9999.99, result.getValue(2818, 5));
    assertDoubleEquals(0.00025, result.getValue(2818, 13));
    assertDoubleEquals(5.347, result.getValue(2818, 14));
  }

  private void assertCorrect(FortranDoubleReaderResult result) {
    assertEquals(2819, result.getNbLigne());
    assertEquals(6, result.getNbColonne());

    assertDoubleEquals(8803.775, result.getValue(0, 0));
    assertDoubleEquals(-173.7, result.getValue(0, 1));
    assertDoubleEquals(0, result.getValue(0, 2));
    assertDoubleEquals(0, result.getValue(0, 3));
    assertDoubleEquals(0, result.getValue(0, 4));
    assertDoubleEquals(9999.99, result.getValue(0, 5));

    assertDoubleEquals(8997.2, result.getValue(29, 0));
    assertDoubleEquals(-119.45, result.getValue(29, 1));
    assertDoubleEquals(0, result.getValue(29, 2));
    assertDoubleEquals(0, result.getValue(29, 3));
    assertDoubleEquals(0, result.getValue(29, 4));
    assertDoubleEquals(9999.99, result.getValue(29, 5));

    assertDoubleEquals(9490.2, result.getValue(2818, 0));
    assertDoubleEquals(4135.275, result.getValue(2818, 1));
    assertDoubleEquals(0, result.getValue(2818, 2));
    assertDoubleEquals(0, result.getValue(2818, 3));
    assertDoubleEquals(0, result.getValue(2818, 4));
    assertDoubleEquals(9999.99, result.getValue(2818, 5));
  }

  private static FortranDoubleReaderResult read(File file) {
    final CtuluIOOperationSynthese read = RubarINXFileFormat.getFormat(true).read(file, null);
    assertFalse(read.containsSevereError());
    return (FortranDoubleReaderResult) read.getSource();
  }

  private FortranDoubleReaderResult write(FortranDoubleReaderResult grid, boolean newFormat) {
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarINXFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }
    final CtuluIOOperationSynthese operation = RubarINXFileFormat.getFormat(newFormat).write(file, grid, null);
    assertFalse(operation.containsSevereError());
    final FortranDoubleReaderResult readerResult = read(file);
    file.delete();
    return readerResult;
  }
}
