/*
 *  @file         TestCas.java
 *  @creation     28 avr. 2003
 *  @modification $Date: 2007-01-19 13:07:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.telemac;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;

import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.dico.DicoCasInterface;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoLanguage;
import org.fudaa.dodico.dico.DicoModelAbstract;

/**
 * @author deniger
 * @version $Id: TestJCas.java,v 1.1 2007-01-19 13:07:17 deniger Exp $
 */
public class TestJCas extends TestIO {

  /**
   * fichier cas.txt.
   */
  public TestJCas() {
    super("cas.txt");
  }

  /**
   * @param _r interface a tester
   */
  protected void interfaceTest(final DicoCasInterface _r) {
    final DicoCasInterface inter = _r;
    assertEquals(inter.getNbInput(), 28);
    final DicoModelAbstract dico = Telemac2dFileFormat.getInstance().getLastVersionImpl(DicoLanguage.FRENCH_ID).getDico();
    DicoEntite ent = dico.getEntite("CONDITIONS INITIALES");
    String t = inter.getValue(ent);
    assertNotNull(ent);
    assertEquals(t, "HAUTEUR NULLE");
    ent = dico.getEntite("DEBITS IMPOSES");
    assertNotNull(ent);
    t = inter.getValue(ent);
    assertEquals(t, "0.;0.");
    ent = dico.getEntite("DUREE DU CALCUL");
    assertNotNull(ent);
    t = inter.getValue(ent);
    assertEquals(t, "1600");
    ent = dico.getEntite("FICHIER DES PARAMETRES");
    assertNotNull(ent);
    t = inter.getCommentaire(ent);
    assertNotNull(t);
    assertEquals("LE FICHIER QUI VA BIEN", t);
    ent = dico.getEntite("MASS-LUMPING SUR LA VITESSE");
    t = inter.getValue(ent);
    assertEquals("1", t);
    ent = dico.getEntite("DEBITS IMPOSES");
    t = inter.getValue(ent);
    assertEquals("0.;0.", t);
    ent = dico.getEntite("MAXIMUM D'ITERATIONS POUR LE SOLVEUR");
    t = inter.getValue(ent);
    assertEquals("100", t);
  }

  /**
   * Test de l'ecriture du fichier.
   */
  public void testEcriture() {
    final DicoCasInterface inter = getInter(fic_);
    final File tmpFile = createTempFile();
    final CtuluIOOperationSynthese syntheseR = Telemac2dFileFormat.getInstance().getLastVersionImpl(DicoLanguage.FRENCH_ID)
        .write(tmpFile, inter, null);
    assertFalse(syntheseR.containsMessages());
    interfaceTest(getInter(tmpFile));
  }

  /**
   * @param _f
   * @return l'interace correspodant a _f
   */
  public DicoCasInterface getInter(final File _f) {
    assertNotNull(_f);
    assertTrue(_f.exists());
    final CtuluIOOperationSynthese syntheseR = Telemac2dFileFormat.getInstance().getLastVersionImpl(DicoLanguage.FRENCH_ID)
        .read(_f, null);
    final DicoCasInterface r = (DicoCasInterface) syntheseR.getSource();
    assertNotNull(r);
    return r;
  }

  /**
   * Test la lecture de 'cas.txt'.
   */
  public void testLecture() {
    interfaceTest(getInter(fic_));
    assertTrue(Telemac2dFileFormat.getInstance().getLastVersionImpl(DicoLanguage.FRENCH_ID).createDicoCasReader()
        .testFile(fic_));
  }

}
