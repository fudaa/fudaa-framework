/**
 *
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

import java.io.File;

public class RubarMasReaderTest extends TestIO {
  public RubarMasReaderTest() {
    super("mas.mas");
  }

  public void testLecture() {
    testContent(read(this.fic_, true));
  }

  private void testContent(TIntObjectHashMap result) {
    assertEquals(174, result.size());
    RubarOutEdgeResult container = (RubarOutEdgeResult) result.get(198217);
    assertDoubleEquals(16, container.getXCentre());
    assertDoubleEquals(-1.38850, container.getYCentre());
    final EvolutionReguliere concentration = container.getEvolutionModifiable(H2dVariableTransType.CONCENTRATION);
    assertEquals(4, concentration.getNbValues());

    assertDoubleEquals(0.1000000000000001, concentration.getX(0));
    assertDoubleEquals(0, concentration.getY(0));

    assertDoubleEquals(20, concentration.getX(3));
    assertDoubleEquals(0.00173, concentration.getY(3));

    final EvolutionReguliere concentration1 = container.getEvolutionModifiable(H2dVariableTransType.createIndexedVar(H2dVariableTransType.CONCENTRATION, 2));
    assertEquals(4, concentration1.getNbValues());

    assertDoubleEquals(0.1000000000000001, concentration1.getX(0));
    assertDoubleEquals(0, concentration1.getY(0));

    assertDoubleEquals(20, concentration1.getX(3));
    assertDoubleEquals(0.00174, concentration1.getY(3));

    final EvolutionReguliere concentration3 = container.getEvolutionModifiable(H2dVariableTransType.createIndexedVar(H2dVariableTransType.CONCENTRATION, 3));
    assertDoubleEquals(0.00175, concentration3.getY(3));
    final EvolutionReguliere diametre3 = container.getEvolutionModifiable(H2dVariableTransType.createIndexedVar(H2dVariableTransType.DIAMETRE, 3));
    assertDoubleEquals(0.02041, diametre3.getY(3));
    final EvolutionReguliere etendue3 = container.getEvolutionModifiable(H2dVariableTransType.createIndexedVar(H2dVariableTransType.ETENDUE, 3));
    assertDoubleEquals(0.00001, etendue3.getY(3));
  }

  private static TIntObjectHashMap read(File file, boolean concentration) {
    final RubarOutReader reader = new RubarOutReader();
    reader.setReadConcentration(concentration);
    reader.setFile(file);
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    return (TIntObjectHashMap) result.getSource();
  }
}
