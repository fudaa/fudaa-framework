/**
 *
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.fortran.FortranDoubleReaderResult;

import java.io.File;
import java.io.IOException;

public class TestRubarCOXFileFormat extends TestIO {
  private static final int NB_NOEUDS = 1434;

  public TestRubarCOXFileFormat() {
    super("coxOld.cox");
  }

  public void testLecture() {
    assertCorrect(read(this.fic_));
  }

  public void testLectureNew() {
    assertCorrect(read(getFile("coxNew.cox")));
  }

  public void testEcritureNew() {
    assertCorrect(read(getFile("coxNew.cox")));
    assertCorrect(write(read(getFile("coxNew.cox")), true));
  }

  public void testEcriture() {
    assertCorrect(write(read(this.fic_), false));
    assertCorrect(write(read(this.fic_), true));
  }

  private void assertCorrect(FortranDoubleReaderResult result) {
    assertEquals(3027, result.getNbLigne());
    assertEquals(3, result.getNbColonne());

    assertDoubleEquals(8793.1, result.getValue(0, 0));
    assertDoubleEquals(-163.8, result.getValue(0, 1));
    assertDoubleEquals(234.443, result.getValue(0, 2));

    assertDoubleEquals(9394.0, result.getValue(2999, 0));
    assertDoubleEquals(4206.8, result.getValue(2999, 1));
    assertDoubleEquals(218.453, result.getValue(2999, 2));

    assertDoubleEquals(9502.5, result.getValue(3026, 0));
    assertDoubleEquals(4132.9, result.getValue(3026, 1));
    assertDoubleEquals(230.233, result.getValue(3026, 2));
  }

  private static FortranDoubleReaderResult read(File file) {
    final CtuluIOOperationSynthese read = RubarCOXFileFormat.getFormat(true).read(file, null);
    assertFalse(read.containsSevereError());
    return (FortranDoubleReaderResult) read.getSource();
  }

  private FortranDoubleReaderResult write(FortranDoubleReaderResult grid, boolean newFormat) {
    File file = null;
    try {
      file = File.createTempFile("Test", "RubarCOXFileFormat");
    } catch (IOException e) {
      e.printStackTrace();

      fail();
    }
    final CtuluIOOperationSynthese operation = RubarCOXFileFormat.getFormat(newFormat).write(file, grid, null);
    assertFalse(operation.containsSevereError());
    final FortranDoubleReaderResult readerResult = read(file);
    file.delete();
    return readerResult;
  }
}
