package org.fudaa.dodico.telemac;

import java.io.File;

import junit.framework.TestCase;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.telemac.io.ScopeGENEFileFormat;
import org.fudaa.dodico.telemac.io.ScopeStructure;

public class TestJScopeGENE extends TestCase {
  /**
   * Le fichier de lecture.
   */
  String testLectureFic_;

 String[] listeExemplesAtester = { "scope.scopGENE", "exemple.scopGENE" };

  /**
   * Constructeur par defaut.
   */
  public TestJScopeGENE() {}

  /**
   * @see junit.framework.TestCase#setUp()
   */
  @Override
  public void setUp() {
    
    
  }

  /**
   * @param _f
   * @return Les zones lues
   * @throws NumberFormatException
   */
  private ScopeStructure lecture(final File _f) {
    
    final CtuluIOOperationSynthese op = ScopeGENEFileFormat.getInstance().getLastVersionInstance(null).read(_f, null);
    final ScopeStructure struct = (ScopeStructure) op.getSource();

    return struct;
  }
  
  
 private void ecriture(final File _f,ScopeStructure struct) {
    
   ScopeGENEFileFormat.getInstance().getLastVersionInstance(null).write(_f, struct, null);

    
  }

  public void lectureEcriture(int typeFichier) {
    File f = null;
    try {
      final File initFile = getFile(listeExemplesAtester[typeFichier]);
      assertNotNull("Le fichier " + listeExemplesAtester[typeFichier] + " est introuvable", initFile);
      assertTrue(initFile.getAbsolutePath() + " n'existe pas !", initFile.exists());
       ScopeStructure structureScope = lecture(initFile);
      assertNotNull(structureScope);
      
      // --test tecriture contenu --//

       f = File.createTempFile("scopeGENE", ".scopGENE");
      assertNotNull(f);
      ecriture(f, structureScope);
      structureScope = lecture(f);

    } catch (final Exception _e) {
      _e.printStackTrace();
    } finally {
      // if (f != null) {
      // f.delete();
      // }
    }
  }
  
  // public void testLectureEcritureScopeGENE1() {
  // lectureEcriture(0);
  // }
  
  public void testLectureEcritureScopeGENE2() {
    lectureEcriture(1);
  }

 


  
  /**
   * @param _f
   * @return File
   */
  public File getFile(final String _f) {
    return TestIO.getFile(getClass(), _f);
  }
  
  
}
