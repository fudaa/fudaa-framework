/**
 *
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TDoubleObjectHashMap;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.dodico.common.TestIO;

import java.io.File;

public class RubarEnvReaderTest extends TestIO {
  public RubarEnvReaderTest() {
    super("envOld.env");
  }

  public void testLecture() {
    testOldContent(read(this.fic_, 17740));
  }

  public void testLectureNew() {
    testNewContent(read(getFile("envNew.env"), 2819));
  }

  private void testOldContent(TDoubleObjectHashMap result) {
    assertEquals(5969, result.size());
    TIntObjectHashMap container = (TIntObjectHashMap) result.get(688.330);
    final RubarMaxContainer o = (RubarMaxContainer) container.get(17739);
    assertDoubleEquals(0.126, o.getH());
  }

  private void testNewContent(TDoubleObjectHashMap result) {
    assertEquals(2162, result.size());
    TIntObjectHashMap container = (TIntObjectHashMap) result.get(7836.667);
    final RubarMaxContainer o = (RubarMaxContainer) container.get(2813);
    assertDoubleEquals(0.206, o.getH());
  }

  private static TDoubleObjectHashMap read(File file, int nbElement) {
    final RubarEnvReader reader = new RubarEnvReader();
    reader.setNbElt(nbElement);
    reader.setFile(file);
    final CtuluIOOperationSynthese result = reader.read();
    assertFalse(result.containsSevereError());
    return (TDoubleObjectHashMap) result.getSource();
  }
}
