/**
 * @creation 2002-11-20
 * @modification $Date: 2007-01-19 13:07:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;

import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.telemac.io.TelemacCLFileFormat;
/**
 * @version $Id: TestJCL.java,v 1.1 2007-01-19 13:07:17 deniger Exp $
 * @author Fred Deniger
 */
public class TestJCL extends TestIO {

  /**
   * test.cli.
   */
  public TestJCL() {
    super("test.cli");
  }

  protected H2dTelemacCLSourceInterface getInter(final File _f){
    final CtuluIOOperationSynthese syntheseR = TelemacCLFileFormat.getInstance().getLastVersionImpl().read(
        _f, null);
    final H2dTelemacCLSourceInterface r = (H2dTelemacCLSourceInterface) syntheseR.getSource();
    assertFalse(syntheseR.containsMessages());
    return r;
  }

  protected synchronized void interfaceTest(final H2dTelemacCLSourceInterface _inter){
    assertNotNull(_inter);
    int idx = 0;
    final H2dTelemacCLElementSource s = new H2dTelemacCLElementSource();
    _inter.getLine(idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_ONDE_INCIDENCE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.SOLIDE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.u_, 0, eps_);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.SOLIDE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.v_, 0, eps_);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.SOLIDE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.v_, 0, eps_);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.u_, 0, eps_);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    //assertEquals(s.hType_, H2dClType.PERMANENT);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES);
    //assertEquals(s.hType_, H2dClType.PERMANENT);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.v_, 0, eps_);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES);
    //assertEquals(s.hType_, H2dClType.PERMANENT);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.u_, 0, eps_);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE);
    //assertEquals(s.hType_, H2dClType.PERMANENT);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES);
    //assertEquals(s.hType_, H2dClType.PERMANENT);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.v_, 0, eps_);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.u_, 0, eps_);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.SOLIDE);
    //assertEquals(s.hType_, H2dClType.LIBRE);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.u_, 0, eps_);
    assertEquals(s.vType_, H2dBcType.PERMANENT);
    assertEquals(s.v_, 0, eps_);
    assertEquals(s.tracerType_, H2dBcType.LIBRE);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    //assertEquals(s.hType_, H2dClType.PERMANENT);
    assertEquals(s.uType_, H2dBcType.PERMANENT);
    assertEquals(s.u_, 0, eps_);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    _inter.getLine(++idx, s);
    assertEquals(s.bordType_, H2dBoundaryTypeCommon.LIQUIDE);
    assertEquals(s.uType_, H2dBcType.LIBRE);
    assertEquals(s.vType_, H2dBcType.LIBRE);
    assertEquals(s.tracerType_, H2dBcType.PERMANENT);
    final int l = _inter.getNbLines();
    assertEquals(18, l);
    for (int i = 0; i < l; i++) {
      _inter.getLine(i, s);
      //les valeurs
      assertEquals(s.h_, 100 + i, eps_);
      if ((i == 1) || (i == 6) || (i == 9) || (i == 14) || (i == 15) || (i == 16)) {
        assertEquals(s.u_, 0, eps_);
      }
      else {
        assertEquals(s.u_, 200 + i, eps_);
      }
      if ((i == 2) || (i == 5) || (i == 8) || (i == 13) || (i == 15)) {
        assertEquals(s.v_, 0, eps_);
      }
      else {
        assertEquals(s.v_, 300 + i, eps_);
      }
      assertEquals(s.friction_, 400 + i, eps_);
      assertEquals(s.tracer_, 500 + i, eps_);
      assertEquals(s.tracerCoefA_, 600 + i, eps_);
      assertEquals(s.tracerCoefB_, 700 + i, eps_);
      //les indices commencent a 0.
      assertEquals(s.ptGlobalIdx_, 799 + i, eps_);
    }
  }

  /**
   *
   */
  public void testEcriture(){
    final H2dTelemacCLSourceInterface inter = getInter(fic_);
    final File tmpFile = createTempFile();
    final CtuluIOOperationSynthese syntheseR = TelemacCLFileFormat.getInstance().getLastVersionImpl()
        .writeMaillage(tmpFile, inter, null);
    if (syntheseR.containsMessages()) {
      syntheseR.getAnalyze().printResume();
    }
    assertFalse(syntheseR.containsMessages());
    interfaceTest(getInter(tmpFile));
  }

  /**
   *
   */
  public void testLecture(){
    interfaceTest(getInter(fic_));
  }
}