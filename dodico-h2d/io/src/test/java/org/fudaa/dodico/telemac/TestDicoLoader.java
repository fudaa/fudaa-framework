package org.fudaa.dodico.telemac;

import junit.framework.TestCase;
import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoCasFileFormatVersion;

import java.util.List;

public class TestDicoLoader extends TestCase {
  /**
   * test les indices de bords.
   */
  public void testLoader() {
    final List<DicoCasFileFormat> formats = TelemacDicoManager.getINSTANCE().getFormats();
    assertEquals(10, formats.size());
    formats.forEach(f -> assertTrue(TelemacDicoManager.getINSTANCE().getVersions(f).size() > 0));
  }

  public void testDicoLoader() {
    final List<DicoCasFileFormat> formats = TelemacDicoManager.getINSTANCE().getFormats();
    formats.forEach(format -> {

      TelemacDicoManager.getINSTANCE().getVersions(format).forEach(version -> {
        String info = "format: " + format.getName() + ", version: " + version;
        try {
          final DicoCasFileFormatVersion dico = TelemacDicoManager.getINSTANCE().createVersionImpl(format, version);
          assertNotNull(info, dico);
        } catch (Exception ex) {
          ex.printStackTrace();
          fail(info + ", ex=" + ex.getMessage());
          throw ex;
        }
      });
    });
  }
}
