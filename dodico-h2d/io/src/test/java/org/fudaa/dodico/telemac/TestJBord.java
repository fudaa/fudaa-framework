/*
 *  @file         TestBord.java
 *  @creation     25 ao�t 2003
 *  @modification $Date: 2007-01-19 13:07:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.telemac;

import junit.framework.TestCase;

import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.H2dBoundaryMutable;
import org.fudaa.dodico.h2d.telemac.H2dTelemacBoundaryMutable;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * @author deniger
 * @version $Id: TestJBord.java,v 1.1 2007-01-19 13:07:18 deniger Exp $
 */
public class TestJBord extends TestCase {

  /**
   * Constructeur par defaut.
   */
  public TestJBord() {
    super();
  }

  /**
   * test les indices de bords.
   */
  public void testBordIdx() {
    final H2dBoundaryMutable b = new H2dBoundaryMutable(0);
    b.setIdxDeb(0);
    b.setIdxFin(5);
    final int nb = 10;
    assertEquals(6, b.getNPointInBord(nb));
    H2dBoundary.BordIndexIterator it = b.createIterator(nb);
    for (int i = 0; i < 6; i++) {
      assertTrue(it.hasNext());
      assertEquals(i, it.next());
    }
    assertFalse(it.hasNext());
    b.setIdxDeb(0);
    b.setIdxFin(0);
    it = b.createIterator(nb);
    for (int i = 0; i < 10; i++) {
      assertTrue(it.hasNext());
      assertEquals(i, it.next());
    }
    assertFalse(it.hasNext());
    b.setIdxDeb(8);
    b.setIdxFin(0);
    it = b.createIterator(nb);
    assertTrue(it.hasNext());
    assertEquals(8, it.next());
    assertTrue(it.hasNext());
    assertEquals(9, it.next());
    assertTrue(it.hasNext());
    assertEquals(0, it.next());
    assertFalse(it.hasNext());
    b.setIdxDeb(0);
    b.setIdxFin(5);
    assertTrue(b.containsIdx(0));
    assertTrue(b.containsIdx(1));
    assertTrue(b.containsIdx(2));
    assertTrue(b.containsIdx(5));
    assertFalse(b.containsIdx(6));
    assertFalse(b.containsIdx(-1));
    b.setIdxDeb(10);
    b.setIdxFin(1);
    assertTrue(b.containsIdx(1));
    assertTrue(b.containsIdx(0));
    assertTrue(b.containsIdx(10));
    assertTrue(b.containsIdx(1111111));
    assertFalse(b.containsIdx(2));
    assertFalse(b.containsIdx(3));
    assertEquals(3, b.getNPointInBord(11));
    b.setIdxDeb(0);
    b.setIdxFin(0);
    assertTrue(b.containsZeroIndex());
    b.setIdxFin(1);
    assertTrue(b.containsZeroIndex());
    b.setIdxDeb(100);
    assertTrue(b.containsZeroIndex());
    b.setIdxFin(0);
    assertTrue(b.containsZeroIndex());
    b.setIdxDeb(5);
    b.setIdxFin(6);
    assertFalse(b.containsZeroIndex());
  }

  /**
   * Test les interserctions des bords.
   */
  public void testTelemacIntersection() {
    final H2dTelemacBoundaryMutable b = new H2dTelemacBoundaryMutable(0);
    b.setIdxDeb(12);
    b.setIdxFin(16);
    assertTrue(b.isBeginIdxSelected(12, 12));
    assertTrue(b.isBeginIdxSelected(0, 14));
    assertTrue(b.isBeginIdxSelected(50, 13));
    assertFalse(b.isBeginIdxSelected(50, 10));
    assertFalse(b.isBeginIdxSelected(10, 11));
    assertFalse(b.isBeginIdxSelected(13, 130));
    final int nbP = 20;
    b.setBoundaryType(H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    assertEquals(-1, b.isIntersectBy(0, 11, nbP));
    assertEquals(0, b.isIntersectBy(0, 12, nbP));
    assertEquals(1, b.isIntersectBy(13, 14, nbP));
    assertEquals(2, b.isIntersectBy(12, 16, nbP));
    assertEquals(2, b.isIntersectBy(12, 17, nbP));
    assertEquals(2, b.isIntersectBy(19, 16, nbP));
    assertEquals(0, b.isIntersectBy(19, 15, nbP));
    assertEquals(-1, b.isIntersectBy(19, 11, nbP));
    // b.setBoundaryType(H2dBoundaryType.SOLIDE);
    // assertEquals(0, b.isIntersectBy(0, 12, nbP));
    // assertEquals(1, b.isIntersectBy(13, 14, nbP));
    // assertEquals(1, b.isIntersectBy(14, 14, nbP));
    // assertEquals(0, b.isIntersectBy(19, 15, nbP));
    // assertEquals(-1, b.isIntersectBy(19, 12, nbP));
    b.setIdxDeb(16);
    b.setIdxFin(5);
    b.setBoundaryType(H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE);
    assertEquals(-1, b.isIntersectBy(6, 15, nbP));
    assertEquals(0, b.isIntersectBy(4, 12, nbP));
    assertEquals(1, b.isIntersectBy(0, 4, nbP));
    assertEquals(1, b.isIntersectBy(17, 4, nbP));
    assertEquals(2, b.isIntersectBy(15, 6, nbP));
    // b.setBoundaryType(H2dBoundaryType.SOLIDE);
    // assertEquals(-1, b.isIntersectBy(5, 16, nbP));
    // assertEquals(0, b.isIntersectBy(4, 14, nbP));
    // assertEquals(0, b.isIntersectBy(18, 9, nbP));
    // assertEquals(1, b.isIntersectBy(19, 3, nbP));
    // assertEquals(2, b.isIntersectBy(16, 5, nbP));
  }
}
