/**
 * @creation 2002-11-20
 * @modification $Date: 2007-05-22 13:11:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.telemac;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.fudaa.ctulu.CtuluIOOperationSynthese;

import org.fudaa.dodico.common.TestIO;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.ef.io.serafin.SerafinInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinNewReader;
import org.fudaa.dodico.ef.io.serafin.SerafinNewReaderInfo;
import org.fudaa.dodico.ef.io.serafin.SerafinWriter;

/**
 * @version $Id: TestJSerafin.java,v 1.2 2007-05-22 13:11:26 deniger Exp $
 * @author Fred Deniger
 */
public class TestJSerafin extends TestIO {

  SerafinInterface inter_;

  /**
   * exemple.res.
   */
  public TestJSerafin() {
    super("exemple.res");
    inter_ = getInter(fic_);
  }

  /**
   * Test le nouveau lecteur utilisant java nio.
   */
  public void testNIO() {
    final SerafinNewReader r = new SerafinNewReader();
    r.setFile(fic_);
    final CtuluIOOperationSynthese s = r.read();
    assertFalse(s.containsSevereError());
    interfaceTest((SerafinInterface) s.getSource());
  }

  public void testVolfin() {
    File f = getFile("supg_column.volfin");
    final SerafinNewReader r = new SerafinNewReader();
    r.setFile(f);
    r.setVolumique(true);
    final CtuluIOOperationSynthese s = r.read();
    s.printAnalyze();
    assertFalse(s.containsSevereError());
    SerafinInterface t = (SerafinInterface) s.getSource();
    try {
      double[] values = t.getReadingInfo().getDouble(0, 0);
      assertEquals(t.getGrid().getEltNb(), values.length);
    } catch (IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
  }

  protected void interfaceTest(final SerafinInterface _t) {
    assertNotNull(_t);
    // TITRE
    assertEquals(_t.getTitre(), "MODELE MARITIME - CALCUL1                                               SERAPHIN");
    // NBV1
    assertEquals(_t.getValueNb(), 6);
    // TEXT
    String temp = _t.getValueId(0);
    assertEquals("VITESSE U", temp);
    temp = _t.getValueId(5);
    assertEquals("VITESSE SCALAIRE", temp);
    // LUNIT
    temp = _t.getUnite(1);
    assertEquals(temp, "M/S");
    temp = _t.getUnite(4);
    assertEquals(temp, "M");
    // IPARAM
    assertEquals(_t.getIparam()[0], 1);
    // IDATE
    assertEquals(_t.getIdate(), 0);
    // NELEM
    final EfGridInterface maillage = _t.getGrid();
    assertEquals(maillage.getEltNb(), 3087);
    // NPOIN1
    assertEquals(maillage.getPtsNb(), 1676);
    // NPPEL1
    assertEquals(maillage.getEltType(), EfElementType.T3);
    // IKLE1
    assertEquals(maillage.getElement(0).getPtIndex(1), 1545);
    assertEquals(maillage.getElement(maillage.getEltNb() - 1).getPtIndex(0), 219);
    // IPOBO1
    assertEquals(_t.getIpoboFr()[0], 4);
    assertEquals(_t.getIpoboFr()[1], 226);
    // X1
    assertEquals(maillage.getPtX(9), Float.intBitsToFloat(1160462115), eps_);
    // Y1
    assertEquals(maillage.getPtY(11), Float.intBitsToFloat(1140111573), eps_);
    // Nombre de pas temps
    final int tempInt = _t.getTimeStepNb();
    assertEquals(tempInt, 2);
    final double tempDouble = _t.getTimeStep(1);
    assertEquals(tempDouble, Float.intBitsToFloat(1153138688), eps_);
    // pas de temps 1, variable 3
    // premiere variable

    try {
      assertEquals(_t.getValue(2, 0, 0), Float.intBitsToFloat(1076258406), eps_);
      assertEquals(_t.getReadingInfo().getDouble(2, 0, 0), Float.intBitsToFloat(1076258406), eps_);
      assertEquals(_t.getValue(3, 1, maillage.getPtsNb() - 1), Float.intBitsToFloat(1093124747), eps_);
    } catch (final IOException e) {
      e.printStackTrace();
    }
    // pas de temps 2, variable 4
    // derniere variable

    assertEquals(2, _t.getTimeStepNb());
    assertDoubleEquals(0, _t.getTimeStep(0));
    assertDoubleEquals(1500, _t.getTimeStep(1));
  }

  /**
   * Test ecriture fichier serafin.
   */
  public void testEcriture() {
    final SerafinInterface inter = inter_;
    final File tmpFile = createTempFile();
    final SerafinWriter w = SerafinFileFormat.getInstance().createSerafinWriter();
    w.setMachineSPARC();
    w.setFile(tmpFile);
    final CtuluIOOperationSynthese syntheseR = w.write(inter);
    assertFalse(syntheseR.getAnalyze().containsErrors());
    interfaceTest(getInter(tmpFile));
  }

  protected SerafinInterface getInter(final File _f) {
    final CtuluIOOperationSynthese syntheseR = SerafinFileFormat.getInstance().read(_f, null);
    final SerafinInterface r = (SerafinInterface) syntheseR.getSource();
    assertFalse(syntheseR.getAnalyze().containsErrors());
    return r;
  }

  /**
   * Test la frontiere.
   */
  public void testRechercheFrontiereWithIpobo() {
    final EfGridArray g = new EfGridArray(inter_.getGrid());
    g.computeBordFast(inter_.getIpoboFr(), null, null);
    final EfFrontierInterface b = g.getFrontiers();
    assertEquals(b.getNbFrontierIntern(), 1);
  }

  /**
   * Test de 2 methodes pour la frontiere.
   */
  public void testRechercheFrontiereinterne() {
    final SerafinInterface inter = inter_;
    System.out.println("nb seg= " + inter.getIpoboFr().length);
    inter.getGrid().computeBord(null, null);
    assertTrue(inter.getGrid().getFrontiers().isSame(inter.getIpoboFr()));
  }

  /**
   * Lecture.
   */
  public void testLecture() {
    interfaceTest(inter_);
  }

  public static void main(String[] args) {
    File f = new File(TestJSerafin.class.getResource("exemple.res").getFile());
    final CtuluIOOperationSynthese syntheseR = SerafinFileFormat.getInstance().read(f, null);
    final SerafinInterface r = (SerafinInterface) syntheseR.getSource();
    final int initTimeStep = r.getTimeStepNb();
    final double maxTime = r.getTimeStep(initTimeStep - 1);
    final double delta = 20;

    final int nbTimeStep = initTimeStep + 100;
    SerafinInterface newInter = new SerafinInterface() {
      @Override
      public boolean isVolumique() {
        return r.isVolumique();
      }

      @Override
      public Map getOptions() {
        return null;
      }

      @Override
      public boolean isXYdoublePrecision() {
        return r.isXYdoublePrecision();
      }

      @Override
      public boolean isElement(int _idx) {
        return r.isElement(_idx);
      }

      @Override
      public boolean containsNodeData() {
        return r.containsNodeData();
      }

      @Override
      public boolean containsElementData() {
        return r.containsElementData();
      }

      @Override
      public int getValueNb() {
        return r.getValueNb();
      }

      @Override
      public String getValueId(int _i) {
        return r.getValueId(_i);
      }

      @Override
      public double getValue(int _numVariable, int _pasTemps, int _ptIdx) throws IOException {
        if (_pasTemps < initTimeStep) {
          return r.getValue(_numVariable, _pasTemps, _ptIdx);
        }
        return Math.random();
      }

      @Override
      public String getUnite(int _i) {
        return r.getUnite(_i);
      }

      @Override
      public String getTitre() {
        return r.getTitre();
      }

      @Override
      public int getTimeStepNb() {
        return nbTimeStep;
      }

      @Override
      public double getTimeStep(int _i) {
        if (_i < initTimeStep) {
          return r.getTimeStep(_i);
        }
        return maxTime + (1 + _i - initTimeStep) * delta;
      }

      @Override
      public SerafinNewReaderInfo getReadingInfo() {
        return r.getReadingInfo();
      }

      @Override
      public int[] getIpoboInitial() {
        return r.getIpoboInitial();
      }

      @Override
      public int[] getIpoboFr() {
        return r.getIpoboFr();
      }

      @Override
      public int[] getIparam() {
        return r.getIparam();
      }

      @Override
      public int getIdisc1() {
        return r.getIdisc1();
      }

      @Override
      public long getIdate() {
        return r.getIdate();
      }

      @Override
      public EfGridInterface getGrid() {
        return r.getGrid();
      }

      @Override
      public ConditionLimiteEnum[] getBoundaryConditions() {
        return ConditionLimiteHelper.EMPTY_ARRAY;
      }
    };

    final SerafinWriter w = SerafinFileFormat.getInstance().createSerafinWriter();
    w.setMachineX86();
    File f2 = new File(f.getAbsoluteFile() + ".res");
    w.setFile(f2);
    w.write(newInter);

  }
}
