/**
 * @creation 22 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.IOException;
import java.text.NumberFormat;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.rubar.H2dRubarDTRResult;

/**
 * @author Fred Deniger
 * @version $Id: RubarDTRWriter.java,v 1.7 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarDTRWriter extends FileOpWriterCharSimpleAbstract {

  protected void internalWrite(final H2dRubarDTRResult _o) {
    try {
      writelnToOut(Double.toString(_o.getTimeStep()));
      final int nb = _o.getNbPoint();
      writelnToOut(Integer.toString(nb));
      final NumberFormat fmt = _o.getXYFormatter();
      for (int i = 0; i < nb; i++) {
        out_.write(Integer.toString(i + 1));
        out_.write(CtuluLibString.ESPACE);
        out_.write(fmt.format(_o.getX(i)));
        out_.write(CtuluLibString.ESPACE);
        out_.write(fmt.format(_o.getY(i)));
        writelnToOut();

      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }

  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof H2dRubarDTRResult) {
      internalWrite((H2dRubarDTRResult) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}