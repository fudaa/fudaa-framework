/**
 * @creation 28 ao�t 2003
 * @modification $Date: 2007-05-22 13:11:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FortranBinaryOutputStream;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author deniger
 * @version $Id: CLVWriter.java,v 1.17 2007-05-22 13:11:25 deniger Exp $
 */
public class CLVWriter extends FileWriteOperationAbstract {

  FortranBinaryOutputStream out_;
  FileFormatVersionInterface version_;

  /**
   * @param _version la version utilise
   */
  public CLVWriter(final FileFormatVersionInterface _version) {
    version_ = _version;
  }

  private void writeClv(final EvolutionReguliereInterface[] _evol) {
    try {
      final EvolutionReguliereInterface base = _evol[0];
      final int nbPasTemps = base.getNbValues();
      final int nbEvol = _evol.length;
      // Verif que toutes les evolutions ont le bon nombre de pas de temps.
      for (int j = 1; j < nbEvol; j++) {
        if (_evol[j].getNbValues() > nbPasTemps) {
          analyze_
              .addError(DodicoLib.getS("La courbe {0} n'a pas le bon nombre de pas de temps", _evol[j].getNom()), j);
        } else if (_evol[j].getNbValues() < nbPasTemps) {
          analyze_.addFatalError(DodicoLib.getS("La courbe {0} n'a pas le bon nombre de pas de temps", _evol[j]
              .getNom()));
          return;
        }
      }
      out_.setRecordLength((nbEvol + 1) * 4);
      out_.writeInteger(nbPasTemps);
      out_.writeRecord();

      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.majProgessionStateOnly();
      up.setValue(nbPasTemps, 4);
      for (int i = 0; i < nbPasTemps; i++) {
        out_.writeDoublePrecision(base.getX(i));
        for (int j = 0; j < nbEvol; j++) {
          out_.writeDoublePrecision(_evol[j].getY(i));
        }
        out_.writeRecord();
        up.majAvancement();
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  /**
   *
   */
  @Override
  protected FortranInterface getFortranInterface() {
    return out_;
  }

  /**
   *
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof EvolutionReguliereInterface[]) {
      writeClv((EvolutionReguliereInterface[]) _o);
    } else {
      donneesInvalides(_o);
    }
  }

 

  /**
   * @return la version utilisee
   */
  public FileFormatVersionInterface getVersion() {
    return version_;
  }

  @Override
  public void setFile(final File _f) {
    analyze_ = new CtuluAnalyze();
    try {
      out_ = new FortranBinaryOutputStream(new FileOutputStream(_f), false);
    } catch (final FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  

  /**
   * @param _o les evolutions a ecrire
   * @return la synthese de l'operation
   */
  public final CtuluIOOperationSynthese write(final EvolutionReguliereInterface[] _o) {
    writeClv(_o);
    return closeOperation(_o);
  }

  /**
   * @param _o les evolutions a ecrire
   * @param _f le fichier cible
   * @param _inter la barre de progression
   * @return la synthese de l'operation
   */
  public final CtuluIOOperationSynthese write(final EvolutionReguliereInterface[] _o, final File _f,
      final ProgressionInterface _inter) {
    setFile(_f);
    setProgressReceiver(_inter);
    return write(_o);
  }
}
