/**
 * @creation 15 avr. 2003
 * @modification $Date: 2006-11-15 09:22:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.File;
import java.util.Map;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author deniger
 * @version $Id: TelemacLiquideFileFormat.java,v 1.27 2006-11-15 09:22:54 deniger Exp $
 */
public class TelemacLiquideFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {

  private static final TelemacLiquideFileFormat INSTANCE = new TelemacLiquideFileFormat();


  /**
   * @return singleton
   */
  public static final TelemacLiquideFileFormat getInstance() {
    return INSTANCE;
  }

  protected TelemacLiquideFileFormat() {
    super(1);
    extensions_ = new String[] { "liq", "txt" };
    id_ = "TELEMAC_FL";
    nom_ = H2dResource.getS("T�l�mac fronti�res liquides");
    description_ = H2dResource
        .getS("Ce fichier contient les valeurs d�pendantes du temps pour les fronti�res liquides");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  /**
   * @return #.
   */
  public String getCommentChar() {
    return "#";
  }

  /**
   * @param _s la chaine a tester
   * @return true si correspont a l'identifiant temps
   * @see #getTempsVariable()
   */
  public boolean isTempsVariable(final String _s) {
    return getTempsVariable().equals(_s);
  }

  /**
   * @return T
   */
  public String getTempsVariable() {
    return "T";
  }

  /**
   * @return reader
   */
  public TelemacLiquideReader createLiquideReader() {
    return new TelemacLiquideReader(this);
  }

  /**
   * @return writer
   */
  public TelemacLiquideWriter createLiquideWriter() {
    return new TelemacLiquideWriter(this);
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new TelemacLiquideReader(this);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new TelemacLiquideWriter(this);
  }

  /**
   * @param _f le fichier a ecrire
   * @param _source la source
   * @param _prog la barre de progression
   * @return la synthese de l'operation.
   */
  public CtuluIOOperationSynthese writeClLiquid(final File _f, final TelemacLiquideInterface _source,
      final ProgressionInterface _prog) {
    final TelemacLiquideWriter w = new TelemacLiquideWriter(this);
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_source);
  }

  /**
   *
   */
  @Override
  public CtuluIOOperationSynthese readEvolutions(final File _f, final ProgressionInterface _prog, final Map _options) {
    final CtuluIOOperationSynthese op = read(_f, _prog);
    final TelemacLiquideInterface source = (TelemacLiquideInterface) op.getSource();
    if (source == null) {
      return null;
    }
    final H2dTelemacEvolutionFrontiere[] evolLiq = source.getEvolutionsFrontieresLiquides();
    final int n = evolLiq.length;
    final EvolutionReguliereInterface[] evol = new EvolutionReguliereInterface[n];
    for (int i = 0; i < n; i++) {
      evol[i] = evolLiq[i].getTelemacEvolution().getEvolution();
    }
    op.setSource(evol);
    return op;
  }

}