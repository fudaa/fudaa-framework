package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.rubar.io.RubarNUAResult.RubarNUAResultBloc;
import org.fudaa.dodico.rubar.io.RubarNUAResult.RubarNUAResultLine;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarNUAReader extends FileOpReadCharSimpleAbstract {
  /**
   * if true, the format used to read/save is coded on 12 digits.
   */
  private boolean newFormat;

  @Override
  protected Object internalRead() {
    final RubarNUAResult r = new RubarNUAResult();

    try {

      int[] unexpectedFormat = new int[]{5, 15, 15, 12};
      int[] nromalFormat = new int[]{6, 15, 15, 12};
      int[] newFormat = new int[]{9, 15, 15, 12};
      int[] fmt = unexpectedFormat;
      int unexpectedSize = CtuluLibArray.getSum(unexpectedFormat);
      int expectedSize = CtuluLibArray.getSum(nromalFormat);
      int newSize = CtuluLibArray.getSum(newFormat);
      RubarNUAResultBloc currentBloc = null;

      while (true) {
        final String currentLine = in_.readLine();

        if (this.isTimeLine(currentLine)) {
          currentBloc = new RubarNUAResultBloc(this.getTime(currentLine));
          r.getBlocs().add(currentBloc);
          //we skip the line ARETE    X arete    Yarete     NU arete
          in_.readLine();
        } else {
          if (currentLine.length() == newSize) {
            fmt = newFormat;
          } else if (currentLine.length() == unexpectedSize) {
            fmt = unexpectedFormat;
          } else if (currentLine.length() == expectedSize) {
            fmt = nromalFormat;
          } else {
            analyze_.addError(H2dResource
                .getS("Ligne {0}: le format de la ligne est inconnu. La ligne doit comporter 4 blocs", Integer.toString(in_.getLineNumber())));
            break;
          }
          in_.analyzeCurrentLine(fmt);

          currentBloc.getLines().add(
              new RubarNUAResultLine(in_.intField(0), new GISPoint(in_.doubleField(1), in_.doubleField(2), 0.0), in_.doubleField(3)));
        }
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    return r;
  }

  private boolean isTimeLine(String line) {
    return line.contains("TEMPS=");
  }

  private double getTime(String timeLine) {
    return Double.parseDouble(timeLine.replace("TEMPS=", "").trim());
  }
}
