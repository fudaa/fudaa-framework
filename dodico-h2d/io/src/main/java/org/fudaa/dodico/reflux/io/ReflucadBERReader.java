/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TDoubleArrayList;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

import com.memoire.fu.FuLog;

/**
 * Un lecteur pour les fichiers Reflucad de berges.
 * 
 * @author Bertrand Marchand
 * @version $Id: SinusxReader.java,v 1.22 2007/05/04 13:47:27 deniger Exp $
 */
public class ReflucadBERReader extends FileOpReadCharSimpleAbstract implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  private String name_;
  /** */
  private int nbOctets_;

  FileFormatVersionInterface v_;

  /**
   * @param _v la version a utiliser
   */
  public ReflucadBERReader(final FileFormatVersionInterface _v) {
    v_ = _v;
  }

  /**
   * Retourne les donn�es sous forme de {@link GISZoneCollectionLigneBrisee}. Contient le Z en attribut.
   * @return La collection de lignes.
   */
  @Override
  protected Object internalRead() {
    return readBerges();
  }

  @Override
  protected void processFile(final File _f) {
    name_ = CtuluLibFile.getSansExtension(_f.getName());
    nbOctets_ = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized GISZoneCollectionLigneBrisee readBerges() {
    GISZoneCollectionLigneBrisee polys = new GISZoneCollectionLigneBrisee();
    polys.setAttributes(new GISAttribute[]{GISAttributeConstants.BATHY}, null);
    if(polys.getAttributeIsZ()==null)
      polys.setAttributeIsZ(GISAttributeConstants.BATHY);
    polys.setTitle(name_, null);
    
    TDoubleArrayList polyligneXyz=null;

    if (super.in_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu=0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets_ > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      // Entete
      in_.readFields();
      String entete=in_.stringField(0);
      if (!"ENTETE:".equalsIgnoreCase(entete)) {
        analyze_.addError(H2dResource.getS("Format de fichier invalide : Format RefluCAD attendu"), in_.getLineNumber());
        return null;
      }
      // Nom g�n�rique
      in_.readLine();
      // Date des lev�es
      in_.readLine();
      // Provenance
      in_.readLine();
      // Echelle
      in_.readFields();
//      double ech=in_.doubleField(0);
      FuLog.warning("Fichier Reflucad : Echelle non prise en compte");
      // Pr�cision
      in_.readLine();
      // Date de r�vision
      in_.readLine();
      // Num�ro de r�vision
      in_.readLine();
      // Nom de l'op�rateur
      in_.readLine();
      // Champ libre
      in_.readLine();
      // Ligne vide
      in_.readLine();
      lu+=183; // Si tous les champs remplis au maxi

      in_.setJumpBlankLine(true);
      
      // Boucle sur les berges (on ne tient pas compte de RD/RG), jusqu'� fin de fichier
      while (true) {
        if (bstop_) return null;
        
        in_.readFields();

        // Debut de berge (et fin de la pr�c�dente)
        if (in_.stringField(0).toUpperCase().startsWith("BERGE:")) {
          if (polyligneXyz!=null)
            polys.addPolyligne(new GISCoordinateSequence(polyligneXyz), null);
          polyligneXyz=null;
          lu+=10;
        }
        
        // La berge continue
        else {
          try {
            if (polyligneXyz==null) polyligneXyz= new TDoubleArrayList();
            polyligneXyz.add(in_.doubleField(0));
            polyligneXyz.add(in_.doubleField(1));
            polyligneXyz.add(in_.doubleField(2));
            lu+=35; // Approximatif
          }
          catch (NumberFormatException _exc) {
            analyze_.addError(H2dResource.getS("Format RefluCAD invalide"), in_.getLineNumber());
            return null;
          }
        }

        if ((afficheAvance) && ((lu * 100 / nbOctets_) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    } catch (final EOFException e) {} catch (final IOException e) {
      analyze_.manageException(e);
    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    if (polyligneXyz!=null)
      polys.addPolyligne(new GISCoordinateSequence(polyligneXyz), null);

    return polys;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  @Override
  public void stop() {
    bstop_ = true;
  }
}