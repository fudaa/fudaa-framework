/**
 * @creation 4 nov. 2004
 * @modification $Date: 2007-05-22 13:11:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleHashSet;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarTimeConditionInterface;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarCLIWriter.java,v 1.12 2007-05-22 13:11:24 deniger Exp $
 */
public class RubarCLIWriter extends FileOpWriterCharSimpleAbstract {
  boolean newFormat = false;

  public boolean isNewFormat() {
    return newFormat;
  }

  public void setNewFormat(boolean newFormat) {
    this.newFormat = newFormat;
  }

  protected void writeCLI(final H2dRubarTimeConditionInterface[] _o) {
    try {
      final int nas = _o.length;
      if (nas == 0) {
        return;
      }
      final FortranWriter w = new FortranWriter(out_);
      w.setSpaceBefore(true);
      TDoubleHashSet hashSet = new TDoubleHashSet(100);
      boolean mustResize = false;
      EvolutionReguliere first = null;
      boolean trans = false;
      int nbConcentrationBlocks = 0;
      for (int i = 0; i < nas; i++) {
        final H2dRubarTimeConditionInterface tc = _o[i];
        if (tc == null) {
          // vrai pour les courbes de tarage ...
          continue;
        }
        if (!trans && tc.isTrans()) {
          trans = true;
          nbConcentrationBlocks = tc.getNbConcentrationBlock();
        }
        if (first == null) {
          first = tc.getHEvol();
        } else if (tc.getHEvol() != null) {
          mustResize |= !(first.isEvolutionWithSameX(tc.getHEvol(), hashSet));
        }
        if (first == null) {
          first = tc.getQnEvol();
        } else if (tc.getQnEvol() != null) {
          mustResize |= !(first.isEvolutionWithSameX(tc.getQnEvol(), hashSet));
        }
        if (first == null) {
          first = tc.getQtEvol();
        } else if (tc.getQtEvol() != null) {
          mustResize |= !(first.isEvolutionWithSameX(tc.getQtEvol(), hashSet));
        }
        if (trans) {
          for (int blockIdx = 0; blockIdx < nbConcentrationBlocks; blockIdx++) {
            if (first == null) {
              first = tc.getConcentrationEvolution(blockIdx);
            } else if (tc.getConcentrationEvolution(blockIdx) != null) {
              mustResize |= !(first.isEvolutionWithSameX(tc.getConcentrationEvolution(blockIdx), hashSet));
            }
            if (first == null) {
              first = tc.getDiametreEvolution(blockIdx);
            } else if (tc.getDiametreEvolution(blockIdx) != null) {
              mustResize |= !(first.isEvolutionWithSameX(tc.getDiametreEvolution(blockIdx), hashSet));
            }
            if (first == null) {
              first = tc.getEtendueEvolution(blockIdx);
            } else if (tc.getEtendueEvolution(blockIdx) != null) {
              mustResize |= !(first.isEvolutionWithSameX(tc.getEtendueEvolution(blockIdx), hashSet));
            }
          }
        }
      }

      int nt = first == null ? 0 : first.getNbValues();
      if (mustResize) {
        FuLog.all("DRU: cli curves will be adadped to a common times step");
        nt = hashSet.size();
      }
      // NT
      final int[] fmt = new int[]{6};
      // 1000 est le max de pas de temps � utiliser
      if (nt > 1000) {
        analyze_.addWarn(H2dResource.getS("Le nombre de pas de temps maximum autoris� a �t� d�pass� (1000)"), -1);
      }
      w.intField(0, nt);
      w.writeFields(fmt);
      // NAS
      w.intField(0, nas);
      w.writeFields(fmt);
      if (nt == 0) {
        if (nas != 0) {
          new Throwable().printStackTrace();
        }
        return;
      }
      final CtuluNumberFormatI formatTime = FortranLib.getFortranFormat(15, 3);
      // on modifie le format
      fmt[0] = 15;
      final int[] formatLine = getFormatForValueLine(trans, nbConcentrationBlocks);
      final CtuluNumberFormatI fmtH = FortranLib.getFortranFormat(10, 3);
      final CtuluNumberFormatI formatConcentration = trans ? FortranLib.getFortranFormat(10, 7) : null;
      final CtuluNumberFormatI formatDiametre = trans ? FortranLib.getFortranFormat(10, 7) : null;
      final CtuluNumberFormatI formatEtendue = trans ? FortranLib.getFortranFormat(10, 7) : null;
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(nt, 4);
      // dans ce cas les valeurs seront interpol�es.
      if (mustResize) {
        analyze_.addWarn(H2dResource.getS("Les pas de temps ne sont pas identiques pour les courbes CLI et des valeurs ont �t� interpol�es"), -1);
        final double[] times = hashSet.toArray();
        Arrays.sort(times);
        hashSet = null; // ne doit plus etre utilise
        for (int i = 0; i < nt; i++) {
          final double t = times[i];
          w.stringField(0, formatTime.format(t));
          w.writeFields(fmt);
          for (int ias = 0; ias < nas; ias++) {
            // les indices du fichier commencent a 1
            w.intField(0, ias + 1);
            final H2dRubarTimeConditionInterface tci = _o[ias];
            EvolutionReguliereInterface evol = tci == null ? null : tci.getHEvol();
            w.stringField(1, fmtH.format(evol == null ? 0 : evol.getInterpolateYValueFor(t)));
            evol = tci == null ? null : tci.getQnEvol();
            w.stringField(2, fmtH.format(evol == null ? 0 : evol.getInterpolateYValueFor(t)));
            evol = tci == null ? null : tci.getQtEvol();
            w.stringField(3, fmtH.format(evol == null ? 0 : evol.getInterpolateYValueFor(t)));
            // Le cas transport
            if (trans) {
              int idxColumn = 4;
              for (int idxBlock = 0; idxBlock < nbConcentrationBlocks; idxBlock++) {
                evol = tci == null ? null : tci.getConcentrationEvolution(idxBlock);
                w.stringField(idxColumn++, formatConcentration.format(evol == null ? 0 : evol.getInterpolateYValueFor(t)));
                evol = tci == null ? null : tci.getDiametreEvolution(idxBlock);
                w.stringField(idxColumn++, formatDiametre.format(evol == null ? 0 : evol.getInterpolateYValueFor(t)));
                evol = tci == null ? null : tci.getEtendueEvolution(idxBlock);
                w.stringField(idxColumn++, formatEtendue.format(evol == null ? 0 : evol.getInterpolateYValueFor(t)));
              }
            }
            w.writeFields(formatLine);
          }
          up.majAvancement();
        }
      } else {
        // pas besoin de faire d'interpolation
        for (int i = 0; i < nt; i++) {
          final double t = first == null ? 0 : first.getX(i);
          w.stringField(0, formatTime.format(t));
          w.writeFields(fmt);
          for (int ias = 0; ias < nas; ias++) {
            w.intField(0, ias + 1);
            final H2dRubarTimeConditionInterface tci = _o[ias];
            EvolutionReguliereInterface evol = tci == null ? null : tci.getHEvol();
            w.stringField(1, fmtH.format(evol == null ? 0 : evol.getY(i)));
            evol = tci == null ? null : tci.getQnEvol();
            w.stringField(2, fmtH.format(evol == null ? 0 : evol.getY(i)));
            evol = tci == null ? null : tci.getQtEvol();
            w.stringField(3, fmtH.format(evol == null ? 0 : evol.getY(i)));
            // Le cas transport
            if (trans) {
              int idxColumn = 4;
              for (int idxBlock = 0; idxBlock < nbConcentrationBlocks; idxBlock++) {
                evol = tci == null ? null : tci.getConcentrationEvolution(idxBlock);
                w.stringField(idxColumn++, formatConcentration.format(evol == null ? 0 : evol.getY(i)));
                evol = tci == null ? null : tci.getDiametreEvolution(idxBlock);
                w.stringField(idxColumn++, formatDiametre.format(evol == null ? 0 : evol.getY(i)));
                evol = tci == null ? null : tci.getEtendueEvolution(idxBlock);
                w.stringField(idxColumn++, formatEtendue.format(evol == null ? 0 : evol.getY(i)));
              }
            }
            w.writeFields(formatLine);
          }
          up.majAvancement();
        }
      }
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  private int[] getFormatForValueLine(boolean trans, int nbConcentrationBlock) {
    return RubarCLIReader.getNewFormat(nbConcentrationBlock, RubarCLIReader.getColumnIndexLength(newFormat));
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof H2dRubarTimeConditionInterface[]) {
      writeCLI((H2dRubarTimeConditionInterface[]) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}
