/**
 * @creation 27 sept. 2004
 * @modification $Date: 2006-09-19 14:45:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.H2dParallelogrammeDataInterface;
import org.fudaa.dodico.h2d.H2dRegularGridDataInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarVF2MResultInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarVF2MResult.java,v 1.5 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarVF2MResult implements H2dRubarVF2MResultInterface {

  String firstLine_;
  RubarGridData gridData_;

  boolean isParall_;
  RubarParallelogrammeData[] parallDatas_;

  @Override
  public String getFirstLine() {
    return firstLine_;
  }

  @Override
  public H2dRegularGridDataInterface getGridData() {
    return gridData_;
  }

  @Override
  public int getNbParall() {
    return parallDatas_ == null ? 0 : parallDatas_.length;
  }

  @Override
  public H2dParallelogrammeDataInterface getParall(final int _i) {
    return parallDatas_ == null ? null : parallDatas_[_i];
  }

  @Override
  public boolean isParall() {
    return isParall_;
  }

}