/**
 * @creation 12 mars 2004
 * @modification $Date: 2007-06-20 12:21:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Locale;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RefluxSolutionWriter.java,v 1.7 2007-06-20 12:21:48 deniger Exp $
 */
public class RefluxSolutionWriter extends FileOpWriterCharSimpleAbstract implements CtuluActivity {

  RefluxRefondeSolutionFileFormat version_;

  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;

  }

  /**
   * @param _ft la version utilisee
   */
  public RefluxSolutionWriter(final RefluxRefondeSolutionFileFormat _ft) {
    version_ = _ft;
  }

  @Override
  protected void internalWrite(final Object _o) {
    stop_ = false;
    if (!(_o instanceof RefluxSolutionInterface)) {
      donneesInvalides(_o);
      return;
    }
    final RefluxSolutionInterface sol = (RefluxSolutionInterface) _o;
    if (out_ == null) {
      analyze_.addFatalError(H2dResource.getS("Le flux est null"));
      return;
    }
    final FortranWriter writer = new FortranWriter(super.out_);
    final DecimalFormat fmt = new DecimalFormat("0.00000E0");
    fmt.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
    fmt.setGroupingUsed(false);
    writer.setSpaceBefore(true);
    final int[] firstLine = new int[] { 5 };
    try {
      writer.intField(0, version_.getFirstLine());
      writer.writeFields(firstLine);
      final int[] fmtEtape = version_.getEtapeFormat();
      final int nbTimeStep = sol.getNbTimeStep();
      final int nbVar = sol.getNbValue();
      final int nbPt = sol.getNbPt();
      final int[] dataFmt = new int[nbVar + 1];
      Arrays.fill(dataFmt, 0, dataFmt.length, version_.getDataColLength());
      // on part sur 10
      dataFmt[0] = version_.getFirstDataColDoubleLength();
      // la derniere colonne de variable= entier
      dataFmt[dataFmt.length - 1] = 5;
      final ProgressionUpdater up = new ProgressionUpdater(super.progress_);
      up.setValue(10, nbTimeStep * nbPt);
      for (int i = 0; i < nbTimeStep; i++) {
        if (stop_) {
          return;
        }
        // le separateur -999 avant chaque nouveau pas de temps
        if (i > 0) {
          writer.intField(0, version_.getFirstLine());
          writer.writeFields(firstLine);
        }
        writer.stringField(0, " ===== PAS NUMERO :");
        writer.intField(1, i + 1);
        writer.stringField(2, " ITERATION NUMERO : ");
        writer.intField(3, i + 1);
        writer.stringField(4, "   TEMPS          ");
        final double t = sol.getTimeStep(i);
        writer.stringField(5, CtuluLib.isZero(t) ? CtuluLibString.ZERO : fmt.format(t));
        writer.writeFields(fmtEtape);
        if (i == 0) {
          writer.intField(0, -1);
          // va comprendre ...
          writer.intField(1, sol.getNbValue());
          writer.writeFields(version_.getNbVariableFormat());
        }
        final CtuluCollectionDouble[] data = new CtuluCollectionDouble[nbVar];
        for (int var = data.length - 1; var >= 0; var--) {
          data[var] = sol.getData(var, i);
        }
        for (int ipt = 0; ipt < nbPt; ipt++) {
          if (stop_) {
            return;
          }
          writer.intField(0, ipt + 1);
          writer.intField(dataFmt.length - 1, -1);
          for (int var = 0; var < data.length; var++) {
            final double value = data[var].getValue(ipt);
            writer.stringField(var + 1, CtuluLib.isZero(value) ? CtuluLibString.ZERO : fmt.format(value));
          }
          writer.writeFields(dataFmt);
          up.majAvancement();
        }

      }
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }
}