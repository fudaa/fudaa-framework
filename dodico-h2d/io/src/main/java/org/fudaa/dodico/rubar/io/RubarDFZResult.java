package org.fudaa.dodico.rubar.io;

import java.util.ArrayList;
import java.util.List;

import gnu.trove.TDoubleArrayList;

public class RubarDFZResult {

  public double[] getTimeSteps() {
    if (blocs == null) {
      return null;
    }
    double[] res = new double[blocs.size()];
    for (int i = 0; i < res.length; i++) {
      res[i] = blocs.get(i).getTime();

    }
    return res;
  }

  public static class RubarDZFResultBloc {

    private double time;
    private TDoubleArrayList dzfs = new TDoubleArrayList();

    public RubarDZFResultBloc(double time) {
      this.time = time;
    }

    /**
     * @return the time
     */
    public double getTime() {
      return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(double time) {
      this.time = time;
    }

    /**
     * @return the dzfs
     */
    public TDoubleArrayList getDzfs() {
      return dzfs;
    }
  }
  private List<RubarDZFResultBloc> blocs = new ArrayList<RubarDZFResultBloc>();

  /**
   * @return the blocs
   */
  public List<RubarDZFResultBloc> getBlocs() {
    return blocs;
  }
}
