/**
 * @creation 12 mars 2004
 * @modification $Date: 2007-06-20 12:21:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: RefluxRefondeSolutionFileFormat.java,v 1.1 2007-06-20 12:21:46 deniger Exp $
 */
public final class RefluxRefondeSolutionFileFormat extends FileFormatUnique {

  private final static RefluxRefondeSolutionFileFormat INSTANCE = new RefluxRefondeSolutionFileFormat();

  /**
   * @return singleton
   */
  public static RefluxRefondeSolutionFileFormat getInstance() {
    return INSTANCE;
  }

  /**
   * Constructeur prive.
   */
  private RefluxRefondeSolutionFileFormat() {
    super(1);
    description_ = DodicoLib.getS("Solutions Reflux");
    // sot pour les temperatures.
    // sol pour refonde
    extensions_ = new String[] { "sov", "sot", getRefondeExtension() };
    nom_ = description_;
  }

  public static String getRefondeExtension() {
    return "sol";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RefluxSolutionReader(this);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RefluxSolutionWriter(this);
  }

  /**
   * @return la largeur des colonnes des donnees
   */
  public int getDataColLength() {
    return 12;
  }

  /**
   * @return le format pour la ligne donnant les donnees sur l'etape
   */
  public int[] getEtapeFormat() {
    return new int[] { 19, 5, 20, 5, 18, 12 };
  }

  /**
   * @return la largeur de la premier colonne des donnees
   */
  public int getFirstDataColLength() {
    return 5;
  }

  public int getFirstDataColDoubleLength() {
    return 10;
  }

  /**
   * @return le contenu de la premiere ligne
   */
  public int getFirstLine() {
    return -999;
  }

  /**
   * @return le format pour la ligne donnant le nombre de variable par point
   */
  public int[] getNbVariableFormat() {
    return new int[] { 5, 5 };
  }


  public static boolean isOk(final EfGridData _data) {
    return isOk2D(_data) || isOkTemp(_data);
  }

  public static boolean isOk2D(final EfGridData _data) {
    return _data.isDefined(H2dVariableType.BATHYMETRIE) && _data.isDefined(H2dVariableType.VITESSE_U)
        && _data.isDefined(H2dVariableType.VITESSE_V) && _data.isDefined(H2dVariableType.COTE_EAU)
        && _data.isDefined(H2dVariableType.HAUTEUR_EAU);
  }

  public static boolean isOkTemp(final EfGridData _data) {
    return _data.isDefined(H2dVariableType.BATHYMETRIE) && _data.isDefined(H2dVariableType.TEMPERATURE);
  }
}