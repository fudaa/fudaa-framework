/**
 * @creation 27 sept. 2004
 * @modification $Date: 2007-01-10 09:04:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

import org.fudaa.dodico.h2d.H2dRegularGridDataInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarGridData.java,v 1.10 2007-01-10 09:04:25 deniger Exp $
 */
public class RubarGridData implements H2dRegularGridDataInterface {

  @Override
  public Object getValueIdentifier(final int _i) {
    return null;
  }

  double dx_;

  double dy_;

  int nbX_;
  int nbY_;
  CtuluArrayDouble[] v_;
  double x0_;
  double y0_;

  @Override
  public CtuluCollectionDoubleEdit getDoubleModel(final int _valIdx) {
    return v_[_valIdx];
  }

  @Override
  public CtuluCollectionDoubleEdit getDoubleValues(final int _i) {
    return getDoubleModel(_i);
  }

  @Override
  public double getDX() {
    return dx_;
  }

  @Override
  public double getDY() {
    return dy_;
  }

  @Override
  public int getNbPoint() {
    return nbX_ * nbY_;
  }

  @Override
  public int getNbPtOnX() {
    return nbX_;
  }

  @Override
  public int getNbPtOnY() {
    return nbY_;
  }

  @Override
  public int getNbValues() {
    return v_.length;
  }

  @Override
  public double getValues(final int _valIdx, final int _idx) {
    return getDoubleModel(_valIdx).getValue(_idx);
  }

  @Override
  public double getX0() {
    return x0_;
  }

  @Override
  public double getY0() {
    return y0_;
  }

}