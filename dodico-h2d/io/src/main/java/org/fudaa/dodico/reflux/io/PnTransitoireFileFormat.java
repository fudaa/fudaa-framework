/**
 * @creation 29 ao�t 2003
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author deniger
 * @version $Id: PnTransitoireFileFormat.java,v 1.15 2006-11-15 09:22:53 deniger Exp $
 */
public final class PnTransitoireFileFormat extends FileFormatUnique {

  private static final PnTransitoireFileFormat INSTANCE = new PnTransitoireFileFormat();

  /**
   * @return singleton
   */
  public static PnTransitoireFileFormat getInstance() {
    return INSTANCE;
  }

  /**
   * @param _nbFile
   */
  private PnTransitoireFileFormat() {
    super(1);
    nom_ = "pn transitoire";
    extensions_ = new String[] { "pnv" };
    id_ = "PN_TRANSITOIRE";
    description_ = H2dResource.getS("Fichier contenant les propri�t�s nodales transitoires");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  /**
   * @return reader
   */
  public CLVReader createCLVReader() {
    return new CLVReader(this);
  }

  /**
   * @return writer
   */
  public CLVWriter createCLVWriter() {
    return new CLVWriter(this);
  }

  /**
   * @param _f le fichier cible
   * @param _inter la source pour l'ecriture
   * @param _prog la barre de progression
   * @return synthese de l'ecriture
   */
  public CtuluIOOperationSynthese write(final File _f, final EvolutionReguliereInterface[] _inter,
      final ProgressionInterface _prog) {
    final CLVWriter w = createCLVWriter();
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  /**
   *
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new CLVReader(this);
  }

  /**
   *
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return new CLVWriter(this);
  }
}