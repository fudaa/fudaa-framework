package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * 
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarDZFFileFormat extends FileFormatUnique {
  /**
   * Initialise les donnees.
   */
  public RubarDZFFileFormat() {
    super(1);
    extensions_ = new String[] { "dzf"};
    super.description_ = H2dResource.getS("Différences de cotes de fond entre deux temps (version avec transport)");
    super.id_ = "RUBAR_DZF";
    super.nom_ = "Rubar dzf";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader(){
    return new RubarDZFReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter(){
    return null;
  }

  /**
   * @param f
   * @param prog
   * @param nbNoeuds
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese read(final File f, final ProgressionInterface prog, final int nbNoeuds){
    final RubarDZFReader i = new RubarDZFReader();
    i.setNbNoeuds(nbNoeuds);
    final CtuluIOOperationSynthese r = i.read(f, prog);
    i.setProgressReceiver(null);
    return r;
  }
}