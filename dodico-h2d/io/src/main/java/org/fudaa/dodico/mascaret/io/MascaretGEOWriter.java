/*
 * @creation     19 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.dodico.commun.DodicoLib;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import java.text.DecimalFormat;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.commun.FudaaPreferences;

/**
 * Ecrit au format mascaret 1d ou 2d.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MascaretGEOWriter extends FileWriteOperationAbstract {

  /** Le flux de sorti. */
  private Writer out_;
  /** Le bief au format Mascaret � �crire. */
  private MascaretBief bief_;
  /** Type du fichier mascaret de destination 1D ou 2D. */
  private boolean is1D_=true;

  /**
   * Cette classe permet � l'exporteur de selectionner les
   * profils � exporter. G�n�ralement les profiles invalides sont ignor�s.
   * @author Emmanuel MARTIN
   * @version $Id$
   */
  public interface FunctorSelectProfil {
    public boolean exportProfil(GISZoneCollection _zone, int _idxProfil);
  }
  
  public MascaretGEOWriter(boolean _is1D) {
    is1D_=_is1D;
  }
  
  /**
   * L'argument doit �tre un tableau contenant : 
   * 0 : string : le nom du bief
   * 1 : GISZoneCollectionLigneBrisee[] : les zones dans lesquelles trouver les g�om�tries
   * 2 : FunctorSelectProfil : pour selectionner les profils � exporter
   * 
   * On passe en param�tre les diff�rentes GISZoneCollectionLigneBrisee qui
   * contiennent les g�om�tries n�c�ssaires � l'exportation. Ces zone peuvent
   * contenir des g�om�tries incompatible avec l'export (comme des polygones),
   * elles seront simplement ignor�es. Dans le cas o� des g�om�tries manqueront,
   * une exception du type MascaretDataIncomplete est lev�e. Les profils invalides
   * sont �galements ignor�s. Si certaines donn�es sont incoh�rentes une exception 
   * du type MascaretDataError est lev�e. La version du fichier de destination ('1d'
   * ou '2d'). Si un des trois param�tres est null une exception de type
   * {@link IllegalArgumentException} est lev�e.
   */
  @Override
  protected void internalWrite(Object _o) {
    // Le format pour les coordonn�es, avec un nombre de d�cimales limit�
    int nbDigits=FudaaPreferences.FUDAA.getIntegerProperty(MascaretExportPreferencesComponent.MASCARET_EXPORT_NB_DIGITS, -1);
    DecimalFormat fmt=CtuluLib.getNoEffectDecimalFormat();
    if (nbDigits != -1) {
      fmt.setMaximumFractionDigits(nbDigits);
    }
    
    DecimalFormat fmtZ=CtuluLib.getNoEffectDecimalFormat();
    fmtZ.setMaximumFractionDigits(2);
    
    try {
      if(((Object[])_o).length!=3)
        throw new IllegalArgumentException("Il doit y avoir 3 param�tres dans _o.");
      String nomBief=(String)((Object[])_o)[0];
      GISZoneCollection[] zones=(GISZoneCollection[])((Object[])_o)[1];
      FunctorSelectProfil selectorProfil=(FunctorSelectProfil)((Object[])_o)[2];
      if (nomBief==null)
        throw new IllegalArgumentException("nomBief ne doit pas �tre null");
      if (zones==null)
        throw new IllegalArgumentException("zones mustn't be null");
      generateMascaretProfilAbstractRepresentations(nomBief, zones, selectorProfil);

      for (int i=0; i<bief_.getNbProfils(); i++) {
        MascaretProfilAbstractRepresentation profil=bief_.getProfil(i);
        // Premi�re ligne
        out_.write("Profil "+profil.nomBief+" "+profil.nomProfil+" "+profil.abscLong);
        if (is1D_)
          out_.write('\n');
        else {
          out_.write(' ');
          // Les coordonn�es X,Y de la trace de profil
          for (int j=0; j<profil.ptsTrace.size(); j++) {
            out_.write(fmt.format(profil.ptsTrace.get(j)[0])+" ");
            out_.write(fmt.format(profil.ptsTrace.get(j)[1])+" ");
          }
          // Les coordonn�es X,Y de l'axe hydrau
          out_.write("AXE "+fmt.format(profil.ptAxeHydrau[0])+" "+fmt.format(profil.ptAxeHydrau[1]));
          out_.write('\n');
        }
        // Les coordonn�es des points du profil
        for (int j=0; j<profil.pts.size(); j++) {
          // Abscisse en travers, coordonn�e Z, type de point.
          out_.write(fmt.format(profil.abscTravers.get(j))+" "+fmtZ.format(profil.coordZ.get(j))+" "+profil.typePts.get(j));
          if (is1D_)
            out_.write('\n');
          // Coordonnnes X,Y du point
          else
            out_.write(" "+fmt.format(profil.pts.get(j)[0])+" "+fmt.format(profil.pts.get(j)[1])+"\n");
        }
      }
    }
    catch (Exception _exp) {
      analyze_.manageException(_exp);
    }
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {
      @Override
      public void close() throws IOException {
        if (out_ != null) {
          out_.close();
        }
      }
    };
  }

  @Override
  public void setFile(File _f) {
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f.getAbsolutePath());
    FileWriter out = null;
    try {
      out = new FileWriter(_f);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    if (out != null) {
      out_ = out;
    }
  }

  
  boolean tolerateMultipleAxes=false;
  
  
  public void setTolerateMultipleAxe(boolean value) {
	  tolerateMultipleAxes = value;
  }
  public boolean isTolerateMultipleAxes() {
	  return tolerateMultipleAxes;
  }
  
  /**
   * G�n�re les repr�sentatino interm�diaires des profils.
   * @param zones_
   * @throws MascaretDataError 
   * @throws MascaretDataIncomplete 
   */
  private void generateMascaretProfilAbstractRepresentations(String _nomBief, GISZoneCollection[] _zones, FunctorSelectProfil _selectorProfil) throws MascaretDataError, MascaretDataIncomplete{
    // Recherche de l'axe hydraulique \\
    GISZoneCollection zoneAxeHydraulique=null;
    int indexAxeHydraulique=-1;
    for (int i=0; i<_zones.length; i++) {
      int idxAttNature=_zones[i].getIndiceOf(GISAttributeConstants.NATURE);
      if (idxAttNature!=-1)
        for (int j=0; j<_zones[i].getNbGeometries(); j++)
          if (_zones[i].getValue(idxAttNature, j)==GISAttributeConstants.ATT_NATURE_AH)
            if (zoneAxeHydraulique==null) {
              zoneAxeHydraulique=_zones[i];
              indexAxeHydraulique=j;
            }
            else 
               if(!tolerateMultipleAxes)
            	   throw new MascaretDataError("Il existe plusieurs axes hydrauliques.");
    }
    if(zoneAxeHydraulique==null)
      throw new MascaretDataIncomplete("Il n'y a pas d'axes hydrauliques.");
    
    // Recherche des profils \\
    List<GISZoneCollection> profils=new ArrayList<GISZoneCollection>();
    List<Integer> indexProfils=new ArrayList<Integer>();
    int nbProfilNotKeep=0;
    for (int i=0; i<_zones.length; i++) {
      int idxAttNature=_zones[i].getIndiceOf(GISAttributeConstants.NATURE);
      if (idxAttNature!=-1)
        for (int j=0; j<_zones[i].getNbGeometries(); j++)
          if (_zones[i].getValue(idxAttNature, j)==GISAttributeConstants.ATT_NATURE_PF) {
            if(_selectorProfil.exportProfil(_zones[i], j)) {
              profils.add(_zones[i]);
              indexProfils.add(j);
            }
            else
              nbProfilNotKeep++;
          }
    }
    if (nbProfilNotKeep>0)
      analyze_.addWarn(DodicoLib.getS("{0} profils sur {1} n'ont pas �t� export�s car ils ne sont pas conformes",""+nbProfilNotKeep,""+(indexProfils.size()+nbProfilNotKeep)), -1);
    else
      analyze_.addInfo(DodicoLib.getS("{0} profils ont �t� export�s",""+(indexProfils.size()+nbProfilNotKeep)));
      
    
    // G�n�ration des MascaretProfilAbstractRepresentations \\
    bief_=new MascaretBief();
    int countNoName=1;
    GISPolyligne axeHydraulique=(GISPolyligne)zoneAxeHydraulique.getGeometry(indexAxeHydraulique);
    for (int i=0;i<profils.size();i++) {
      GISZoneCollection zoneProfil=profils.get(i);
      int idxProfil=indexProfils.get(i);
      GISPolyligne profil=(GISPolyligne) zoneProfil.getGeometry(idxProfil);
      MascaretProfilAbstractRepresentation profilAbs=new MascaretProfilAbstractRepresentation();
      
      // Information globales sur le profil \\
      // Nom bief
      profilAbs.nomBief=_nomBief.replace(' ', '_');
      // Nom profil
      int idxTitle=zoneProfil.getIndiceOf(GISAttributeConstants.TITRE);
      if(idxTitle!=-1){
        String nomProfil=(String) zoneProfil.getValue(idxTitle, idxProfil);
        profilAbs.nomProfil=nomProfil.replace(' ', '_');
      }
      else
        profilAbs.nomProfil="No_Name_"+countNoName++;
      // Calcul de l'intersection avec l'axe hydraulique
      GrPoint pt=new GrPolyligne(axeHydraulique).intersectionXY(new GrPolyligne(profil),0.01);
      if (pt==null) {
    	  if(!tolerateMultipleAxes)
    		  throw new MascaretDataError("Un profil � plusieurs ou aucun point(s) d'intersection(s) avec l'axe hydraulique.");
      }else
        profilAbs.ptAxeHydrau=new double[]{pt.x_, pt.y_};

      // Abscisse curviligne axe hydraulique
      int idxAttrCommHydraulique=zoneProfil.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO);
      if(idxAttrCommHydraulique==-1)
        throw new MascaretDataError("L'attribut COMMENTAIRE_HYDRO doit �tre pr�sent.");
      String comm=(String) zoneProfil.getValue(idxAttrCommHydraulique, idxProfil);
      if(!GISLib.isHydroCommentValued(comm, GISAttributeConstants.ATT_COMM_HYDRO_PK))
        throw new MascaretDataError("La propri�t� PK de COMMENTAIRE_HYDRO doit �tre valu�e.");
      profilAbs.abscLong=GISLib.getHydroCommentDouble(comm, GISAttributeConstants.ATT_COMM_HYDRO_PK);
      CoordinateSequence coordSeq=axeHydraulique.getCoordinateSequence();
      // Trace profil
      coordSeq=profil.getCoordinateSequence();
      Coordinate coord[]=getPointsRuptures(coordSeq);  
      profilAbs.ptsTrace.add(new double[]{coordSeq.getX(0), coordSeq.getY(0)});
      for(int j=0;j<coord.length;j++)
        profilAbs.ptsTrace.add(new double[]{coord[j].x, coord[j].y});
      profilAbs.ptsTrace.add(new double[]{coordSeq.getX(coordSeq.size()-1), coordSeq.getY(coordSeq.size()-1)});
      
      // Information atomiques sur le profil \\
      coordSeq=profil.getCoordinateSequence();
      int idxZ=zoneProfil.getIndiceOf(zoneProfil.getAttributeIsZ());
      for(int j=0;j<profil.getNumPoints();j++){
        // Valeur curviligne
        profilAbs.abscTravers.add(absCurv(coordSeq, j));
        // Coordonn�es X Y
        profilAbs.pts.add(new double[]{coordSeq.getCoordinate(j).x, coordSeq.getCoordinate(j).y});
        // Coordinate Z
        if(idxZ!=-1)
          profilAbs.coordZ.add(((GISAttributeModelDoubleInterface)zoneProfil.getValue(idxZ, idxProfil)).getValue(j));
      }
      // Extraction des valeurs d'intersection
      ArrayList<Integer> seps=new ArrayList<Integer>();
      //INTERSECTION_RIVE_GAUCHE
      int idxInterRiveG=zoneProfil.getIndiceOf(GISAttributeConstants.INTERSECTION_RIVE_GAUCHE);
      Integer idxRG=-1;
      if(idxInterRiveG!=-1) {
        idxRG=(Integer) zoneProfil.getValue(idxInterRiveG, idxProfil);
        seps.add(idxRG);
      }
      //INTERSECTION_RIVE_DROITE
      int idxInterRiveD=zoneProfil.getIndiceOf(GISAttributeConstants.INTERSECTION_RIVE_DROITE);
      Integer idxRD=-1;
      if(idxInterRiveD!=-1) {
        idxRD=(Integer) zoneProfil.getValue(idxInterRiveD, idxProfil);
        seps.add(idxRD);
      }
      //INTERSECTION_LIMITE_STOCKAGE_GAUCHE
      int idxInterStockageG=zoneProfil.getIndiceOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_GAUCHE);
      Integer idxSG=-1;
      if(idxInterStockageG!=-1) {
        idxSG=(Integer) zoneProfil.getValue(idxInterStockageG, idxProfil);
        seps.add(idxSG);
      }
      //INTERSECTION_LIMITE_STOCKAGE_DROITE
      int idxInterStockageD=zoneProfil.getIndiceOf(GISAttributeConstants.INTERSECTION_LIMITE_STOCKAGE_DROITE);
      Integer idxSD=-1;
      if(idxInterStockageD!=-1) {
        idxSD=(Integer) zoneProfil.getValue(idxInterStockageD, idxProfil);
        seps.add(idxSD);
      }
      if(idxRG==-1&&idxRD!=-1||idxRG!=-1&&idxRD==-1)
        throw new MascaretDataIncomplete("Il y a une seule rive (il doit y en avoir soit 0 soit 2)");
      if(idxSG==-1&&idxSD!=-1||idxSG!=-1&&idxSD==-1)
        throw new MascaretDataIncomplete("Il y a une seule limite de stockage (il doit y en avoir soit 0 soit 2)");
      Integer[] sepsT=seps.toArray(new Integer[0]);
      Arrays.sort(sepsT);
      // Bathy Topo ou Stockage
      if(sepsT.length==4){
        int j;
        for(j=0;j<sepsT[0];j++)
          profilAbs.typePts.add('S');
        for(;j<sepsT[1];j++)
          profilAbs.typePts.add('T');
        for(;j<=sepsT[2];j++)
          profilAbs.typePts.add('B');
        for(;j<=sepsT[3];j++)
          profilAbs.typePts.add('T');
        for(;j<coordSeq.size();j++)
          profilAbs.typePts.add('S');
      }
      else if(sepsT.length==2) {
        if(sepsT[0]==idxRG||sepsT[0]==idxRD){
          int j;
          for(j=0;j<=sepsT[0];j++)
            profilAbs.typePts.add('T');
          for(;j<sepsT[1];j++)
            profilAbs.typePts.add('B');
          for(;j<coordSeq.size();j++)
            profilAbs.typePts.add('T');
        }
        else{
          int j;
          for(j=0;j<=sepsT[0];j++)
            profilAbs.typePts.add('S');
          for(;j<sepsT[1];j++)
            profilAbs.typePts.add('B');
          for(;j<coordSeq.size();j++)
            profilAbs.typePts.add('S');
        }
      }
      else
        for(int j=0;j<coordSeq.size();j++)
          profilAbs.typePts.add('B');
      
      // Verification de la coh�rence des r�sultats
      if(is1D_)
        if(!profilAbs.check1D())
          throw new MascaretDataError("Erreur de programmation : les donn�es g�n�r�es ne sont pas coh�rentes pour le 1d.");
      else
        if(!profilAbs.check2D())
          throw new MascaretDataError("Erreur de programmation : les donn�es g�n�r�es ne sont pas coh�rentes pour le 2d.");
      
      // Stockage avec les autres \\
      bief_.addProfil(profilAbs);
    }
  }
  
  /**
   * Retourne la liste des points de ruptures.
   */
  private Coordinate[] getPointsRuptures(CoordinateSequence _seq) {
    List<Coordinate> coord=new ArrayList<Coordinate>();
    int idx1=0;
    int idx2=1;
    int idx3=2;
    int idxPointRupturePrecedent=-1;
    while(idx3<_seq.size()) {
      if(idx1==idx2)
        idx2++;
      else if(idx2==idx3)
        idx3++;
      else if(_seq.getCoordinate(idx1).equals(_seq.getCoordinate(idx2)))
        idx2++;
      else if(_seq.getCoordinate(idx2).equals(_seq.getCoordinate(idx3)))
        idx3++;
      else {
        if(getAngle(_seq.getCoordinate(idx1), _seq.getCoordinate(idx2), _seq.getCoordinate(idx3))<(Math.PI-0.0001))
          if(idxPointRupturePrecedent<0||!_seq.getCoordinate(idxPointRupturePrecedent).equals(_seq.getCoordinate(idx2)))
            coord.add(_seq.getCoordinate(idx2));
        if(idx2+1<idx3)
          idx2++;
        else
          idx1=idx2;
      }
    }
    return coord.toArray(new Coordinate[0]);
  }
  
  /**
   * Retourne l'angle form� par les segments _a_b et _b_c. _a, _b et _c doivent
   * �tre non confondus sous peine d'avoir un {@link IllegalArgumentException}.
   * L'angle est toujours positif.
   * L'ordonn� z n'est pas prise en compte.
   * L'angle retourn� est en radian.
   */
  private double getAngle(Coordinate _a, Coordinate _b, Coordinate _c) {
    Coordinate a=new Coordinate(_a.x, _a.y, 0);
    Coordinate b=new Coordinate(_b.x, _b.y, 0);
    Coordinate c=new Coordinate(_c.x, _c.y, 0);
    if(egal(a, b)||egal(b, c)||egal(a, c))
      throw new IllegalArgumentException("Les trois points doivent �tre non confonus.");
    // Projection de b sur ac
    Coordinate b2=proj(b, a, c);
    // Calcul des angles interm�diaires
    double angleCBB2=Math.asin(b2.distance(c)/c.distance(b));
    double angleB2BA=Math.asin(b2.distance(a)/a.distance(b));
    if(angleCBB2==Double.NaN||angleB2BA==Double.NaN)
      return Math.PI;
    // Selon que B2 appartient ou non � ac, la d�termination de l'angle final diff�re.
    double angle;
    int position=getPositionXY(b2, a, c);
    if(position==-1)
      angle=angleCBB2-angleB2BA;
    else if(position==1)
      angle=angleB2BA-angleCBB2;
    else
      angle=angleCBB2+angleB2BA;
    return angle;
  }
  
  /**
   * Soit _a, _b, _c align�s et non confondus. Retourne -1 si _a avant _b_c, 0
   * si _a est entre _b_c (inclu), +1 sinon. Si _a, _b et _c ne sont pas
   * align�s, {@link IllegalArgumentException} est lev�.
   */
  private int getPositionXY(Coordinate _a, Coordinate _b, Coordinate _c) {
    if(!egal(proj(_a, _b, _c), _a)||egal(_a, _b)||egal(_b, _c)||egal(_a, _c))
      throw new IllegalArgumentException("Les trois points doivent �tre align�s et non confonus.");
    Coordinate vecBC=vec(_b, _c);
    double k;
    if(vecBC.x!=0)
      k=(_a.x-_b.x)/vecBC.x;
    else
      k=(_a.y-_b.y)/vecBC.y;
    if(k<0)
      return -1;
    else if(k>1)
      return 1;
    else
      return 0;
  }
  
  /**
   * Retourne la projection de _a sur la droite _b_c.
   * Attention : Il s'agit bien d'une projection sur une
   * droite et non un segment de droite : le r�sultat
   * peut �tre en dehors de [_b, _c]
   */
  private Coordinate proj(Coordinate _a, Coordinate _b, Coordinate _c){
    Coordinate vBC=vec(_b, _c);
    Coordinate vBA=vec(_b, _a);
    double normeBC=Math.sqrt(vBC.x*vBC.x+vBC.y*vBC.y+vBC.z*vBC.z);
    double produitScalaireBCBA=vBC.x*vBA.x+vBC.y*vBA.y+vBC.z*vBA.z;
    double rapportSurBC=produitScalaireBCBA/(normeBC*normeBC);
    return new Coordinate(vBC.x*rapportSurBC+_b.x, vBC.y*rapportSurBC+_b.y, vBC.z*rapportSurBC+_b.z);
  }
  
  /**
   * Compars les deux vecteurs avec la tol�rance donn�e. Le dernier param�tre
   * indique si un normalisation des deux vecteurs doit �tre faite avec la
   * comparaison. Les vecteurs ne sont pas modif�s dans l'op�ration. _testZ doit
   * �tre mit � vrai pour que la comparaison tienne compte du z.
   */
  private boolean egal(Coordinate _a, Coordinate _b) {
    Coordinate diff=vec(_a, _b);
    return Math.abs(diff.x)<0.0001&&Math.abs(diff.y)<0.0001;
  }
  
  /**
   * retourne le vecteur _a, _b sous forme de Coordinate.
   */
  private Coordinate vec(Coordinate _a, Coordinate _b){
    return new Coordinate(_b.x-_a.x, _b.y-_a.y, _b.z-_a.z);
  }
  
  /**
   * Calcul l'abscisse curviligne du point indiqu� en param�tre
   */
  private double absCurv(CoordinateSequence _coordSeq, int _idxPoint){
    if(_idxPoint==0)
      return 0;
    double valueCurviligne=0;
    for (int j=1; j<=_idxPoint; j++)
      valueCurviligne+=_coordSeq.getCoordinate(j).distance(_coordSeq.getCoordinate(j-1));
    return valueCurviligne;
  }
}
