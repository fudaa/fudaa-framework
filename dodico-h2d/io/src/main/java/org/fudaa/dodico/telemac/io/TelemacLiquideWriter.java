/**
 * @creation 15 avr. 2003
 * @modification $Date: 2006-12-20 16:07:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.telemac.io;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquide;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;

/**
 * @author deniger
 * @version $Id: TelemacLiquideWriter.java,v 1.22 2006-12-20 16:07:11 deniger Exp $
 */
public class TelemacLiquideWriter extends FileOpWriterCharSimpleAbstract {

  TelemacLiquideFileFormat version_;

  /**
   * @param _ft la version a utiliser.
   */
  public TelemacLiquideWriter(final TelemacLiquideFileFormat _ft) {
    version_ = _ft;
  }

  /**
   * @param _interface la source pour l'ecriture.
   */
  public void writeLiquide(final TelemacLiquideInterface _interface) {
    final TelemacLiquideInterface inter = _interface;
    if (out_ == null) {
      analyze_.addFatalError(H2dResource.getS("Le flux de sortie est nul"));
      return;
    }
    if ((inter == null) || (inter.getNbEvolutionsFrontiereLiquide() == 0)) {
      analyze_.addFatalError(H2dResource.getS("Les donn�es sont nulles"));
      return;
    }
    final H2dTelemacEvolutionFrontiere[] evols = inter.getEvolutionsFrontieresLiquides();
    final int nbEvol = evols.length;
    final int nbPasTemps = evols[0].getTelemacEvolution().getPasTempNb();
    for (int i = 1; i < nbEvol; i++) {
      if (evols[i - 1].getTelemacEvolution().getPasTempNb() != evols[i].getTelemacEvolution().getPasTempNb()) {
        analyze_.addError(H2dResource.getS("Les courbes n'ont pas les m�mes discr�tisations"), i);
        return;
      }
    }
    // on veut faire 4 mises a jour de l'avancement.
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbPasTemps);
    up.majProgessionStateOnly();
    try {
      final String comment = version_.getCommentChar();
      // on estime que les valeurs prennent 10 caracteres
      // Ecriture des commentaires
      writelnToOut(comment + " " + H2dResource.getS("Fichier des fronti�res liquides transitoires"));
      writelnToOut(comment + " " + H2dResource.getS("G�n�r� le") + CtuluLibString.ESPACE
          + DateFormat.getDateInstance().format(Calendar.getInstance().getTime()));
      // Ecritures des identifiants
      StringBuffer b = new StringBuffer();
      b.append(version_.getTempsVariable());
      for (int i = 0; i < nbEvol; i++) {
        H2dEvolutionFrontiereLiquide evolution = evols[i].getTelemacEvolution();
        b.append(CtuluLibString.ESPACE);
        b.append(evolution.getVariableType().getTelemacID());
        b.append('(');
        b.append(evolution.getIndexFrontiere() + 1);
        if (evols[i].isIdxVariableDefined()) {
          b.append(',');
          b.append(evols[i].getIdxVariable() + 1);
        }
        b.append(')');
      }
      writelnToOut(b.toString());
      b = new StringBuffer();
      // Ecritures des unites
      b.append('s');
      for (int i = 0; i < nbEvol; i++) {
        b.append(' ').append(evols[i].getTelemacEvolution().getUnite());
      }
      writelnToOut(b.toString());

      // Ecritures des valeurs
      final H2dEvolutionFrontiereLiquide ev0 = evols[0].getTelemacEvolution();
      for (int i = 0; i < nbPasTemps; i++) {
        b = new StringBuffer();
        b.append(ev0.getPasDeTempsAt(i));
        for (int j = 0; j < nbEvol; j++) {
          b.append(' ').append(evols[j].getTelemacEvolution().getValueAt(i));
        }
        writelnToOut(b.toString());
        up.majAvancement();
      }
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  /**
   *
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof TelemacLiquideInterface) {
      writeLiquide((TelemacLiquideInterface) _o);
    } else {
      donneesInvalides(_o);
    }
  }

  /**
   * @param _o la source pour l'ecriture
   * @return la synthese de l'ecriture
   */
  public CtuluIOOperationSynthese write(final TelemacLiquideInterface _o) {
    writeLiquide(_o);
    return closeOperation(_o);
  }

}
