/**
 * @creation 11 f�vr. 2004
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * Un format pour les fichiers Reflucad de berges.
 * @author Bertrand Marchand
 * @version $Id: SinusxFileFormat.java,v 1.15 2007/05/04 13:47:27 deniger Exp $
 */
public final class ReflucadBERFileFormat extends FileFormatUnique {

  static final ReflucadBERFileFormat INSTANCE = new ReflucadBERFileFormat();

  /**
   * @return singleton
   */
  public static ReflucadBERFileFormat getInstance() {
    return INSTANCE;
  }

  private ReflucadBERFileFormat() {
    super(1);
    extensions_ = new String[] { "ber" };
    id_ = "RCADBER";
    nom_ = "Reflucad berges";
    description_ = DodicoLib.getS("Comporte les d�finitions de berges");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new ReflucadBERReader(this);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return super.write(_f, _source, _prog);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    /** @todo */
    throw new RuntimeException("Method not implemented");
//    return new ReflucadBERWriter(this);
  }

}