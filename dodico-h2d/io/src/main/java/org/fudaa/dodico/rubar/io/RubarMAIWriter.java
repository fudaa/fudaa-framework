/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-09-19 14:45:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarNumberFormatter;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarMAIWriter.java,v 1.5 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarMAIWriter extends FileOpWriterCharSimpleAbstract {
  int nbDecimal_ = 1;
  /**
   * if true, the format used to read/save is coded on 12 digits.
   */
  private boolean newFormat;

  public final void setNbDecimal(final int _nbDecimal) {
    nbDecimal_ = _nbDecimal;
  }

  /**
   * @return le nombre de decimal lue.
   */
  public final int getNbDecimal() {
    return nbDecimal_;
  }

  public boolean isNewFormat() {
    return newFormat;
  }

  public void setNewFormat(boolean newFormat) {
    this.newFormat = newFormat;
  }

  /**
   * ne fait rien.
   */
  public RubarMAIWriter() {
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (!(_o instanceof EfGridInterface)) {
      donneesInvalides(_o);
      return;
    }
    final EfGridInterface grid = (EfGridInterface) _o;
    if (grid.getEltType() == EfElementType.T6) {
      analyze_.addFatalError(H2dResource.getS("Les maillages de type T6 ne sont pas support�s"));
    }
    try {
      final FortranWriter writer = new FortranWriter(out_);
      writer.setSpaceBefore(true);
      // Ecriture des �l�ments
      final int ne = grid.getEltNb();
      writer.intField(0, ne);
      writer.writeFields(new int[]{getNumberFormat()});

      // initialisation donnees
      int[] fmt = RubarMAIReader.getElementLinesFormat(newFormat);
      //int nbFieldByLine = fmt.length;
      int tmpOnLine = 0;

      // lecture de la ligne NNE(IE),INE(JN)
      for (int ie = 0; ie < ne; ie++) {
        final EfElement el = grid.getElement(ie);
        final int nne = el.getPtNb();
        if (nne > 4) {
          analyze_.addFatalError(H2dResource.getS("Maillage inadapt�"));
          return;
        }
        writer.intField(0, nne);
        for (int j = 0; j < nne; j++) {
          // +1 car dans java les tableau commencent a 0
          writer.intField(j + 1, el.getPtIndex(j) + 1);
        }
        writer.writeFields(fmt);
      }
      if (progress_ != null) {
        progress_.setProgression(40);
      }
      // LECTURE DES POINTS
      // nn
      final int nn = grid.getPtsNb();
      writer.intField(0, nn);
      writer.writeFields(new int[]{getNumberFormat()});
      fmt = new int[10];
      if (newFormat) {
        Arrays.fill(fmt, 13);
      } else {
        Arrays.fill(fmt, 8);
      }
      final int nbFieldByLine = fmt.length;
      tmpOnLine = 0;
      final CtuluNumberFormater formatter = H2dRubarNumberFormatter.createNumberFormateur(getXYFormatLength(), nbDecimal_);

      for (int i = 0; i < nn; i++) {
        writer.stringField(tmpOnLine++, formatter.format(grid.getPtX(i)));
        if (tmpOnLine == nbFieldByLine) {
          writer.writeFields(fmt);
          tmpOnLine = 0;
        }
      }
      if (tmpOnLine > 0) {
        writer.writeFields(fmt);
        tmpOnLine = 0;
      }
      for (int i = 0; i < nn; i++) {
        writer.stringField(tmpOnLine++, formatter.format(grid.getPtY(i)));
        if (tmpOnLine == nbFieldByLine) {
          writer.writeFields(fmt);
          tmpOnLine = 0;
        }
      }
      if (tmpOnLine > 0) {
        writer.writeFields(fmt);
        tmpOnLine = 0;
      }
    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  private int getXYFormatLength() {
    return newFormat ? 12 : 8;
  }

  private int getNumberFormat() {
    return newFormat ? 9 : 6;
  }
}
