@echo off
rem Calcul par reflux

set root=@project@

:calcul
if not exist %root%.inp goto rootNotFound
rem Suppression des fichiers resultats
if exist %root%.out del %root%.out
if exist %root%.sfv del %root%.sfv
if exist %root%.sov del %root%.sov
if exist %root%.sft del %root%.sft
if exist %root%.sot del %root%.sot

echo Launching reflux...
echo FICHIER: %root%.inp %root%.out > reflux.5

"@exe@" < reflux.5
del reflux.5
goto end

:syntaxe
echo Syntaxe : %0 {unite} {racine fichier param} {nom fichier param sans inp}
goto end

:rootNotFound

echo Erreur : Fichier %root%.inp inexistant
goto end

:end
