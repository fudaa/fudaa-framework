/**
 * @creation 22 d�c. 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

/**
 * @author Fred Deniger
 * @version $Id: RubarOUVFileFormat.java,v 1.6 2006-11-15 09:22:53 deniger Exp $
 */
public class RubarOUVFileFormat extends FileFormatUnique {

  /**
   * construit les extensions uniquement.
   */
  public RubarOUVFileFormat() {
    super(1);
    extensions_ = new String[] { "ouv" };
    super.id_ = "RUBAR_OUV";
    super.nom_ = "Rubar ouv";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarOUVReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarOUVWriter();
  }

}