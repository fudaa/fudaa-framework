/*
 * @creation 20 nov. 06
 * @modification $Date: 2006-11-20 14:23:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.fortran.FortranDoubleWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author fred deniger
 * @version $Id: RubarDIXFileFormat.java,v 1.1 2006-11-20 14:23:54 deniger Exp $
 */
public class RubarDIXFileFormat extends RubarDonneesBrutesFileFormat {
  private static RubarDIXFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarDIXFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarDIXFileFormat();
    }
    return instance_;
  }

  public RubarDIXFileFormat() {
    super();
    extensions_ = new String[]{"dix"};
    super.description_ = H2dResource.getS("Coefficient de diffusion");
    super.id_ = "RUBAR_DIX";
    super.nom_ = "Rubar dix";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarFortranDoubleReader(
        new int[][]{{13, 13, 13}, {8, 8, 8}}, 3
    );
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new FortranDoubleWriter(
        isNewFormat() ?
            new int[]{13, 13, 13}
            : new int[]{8, 8, 8}
    );
  }

  @Override
  public boolean isNuage() {
    return true;
  }
}
