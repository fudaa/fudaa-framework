/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarDURResult {
  private CtuluCollectionDouble zValues;

  public RubarDURResult(CtuluCollectionDouble zValues) {
    super();
    this.zValues = zValues;
  }

  public int getNbNoeuds() {
    return this.zValues == null ? 0 : this.zValues.getSize();
  }

  public double getZ(int idx) {
    return this.zValues.getValue(idx);
  }

  public CtuluCollectionDouble getzValues() {
    return zValues;
  }

  public void setzValues(CtuluCollectionDouble zValues) {
    this.zValues = zValues;
  }
}
