/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarCOTFileFormat.java,v 1.9 2006-11-15 09:22:53 deniger Exp $
 */
public final class RubarCOTFileFormat extends RubarDonneesBrutesFileFormat {

  private static RubarCOTFileFormat instance_;

  @Override
  public  boolean isNuage() {
    return false;
  }

  /**
   * @return l'instance a utiliser
   */
  public static RubarCOTFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarCOTFileFormat();
    }
    return instance_;
  }

  /**
   * 
   */
  public RubarCOTFileFormat() {
    super();
    extensions_ = new String[] { "cot" };
    super.description_ = H2dResource.getS("Cote du fond");
    super.id_ = "RUBAR_COT";
    super.nom_ = "Rubar cot";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarVF2MReader(false, true);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarVF2MWriter(true);
  }


}