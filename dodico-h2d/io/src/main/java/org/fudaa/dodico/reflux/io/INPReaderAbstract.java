/*
 * @creation 26 juin 07
 * @modification $Date: 2007-06-29 15:10:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntDoubleHashMap;
import gnu.trove.TIntObjectHashMap;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EfGridMutable;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.H2dTimeStepGroup;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryConditionMutable;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoModel;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;
import org.fudaa.dodico.h2d.type.H2dResolutionMethodType;
import org.fudaa.dodico.h2d.type.H2dResolutionSchemaType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author fred deniger
 * @version $Id: INPReaderAbstract.java,v 1.2 2007-06-29 15:10:25 deniger Exp $
 */
public abstract class INPReaderAbstract extends FileOpReadCharSimpleAbstract {

  /**
   * Classe utilitaire permettant de stocker les indices du groupe elementaire.
   */
  protected static class TemplateGroupeElement {

    private TIntDoubleHashMap fixe_;
    private TIntArrayList libre_;
    private final String nom_;
    private final int taille_;
    private TIntObjectHashMap transitoire_;

    // H2dVariableType type_;

    TemplateGroupeElement(final String _nom, final int _taille) {
      nom_ = _nom;
      taille_ = _taille;
    }

    /**
     * Ajoute en tant que prop permanente de valeur <code>_v</code> l'indice <code>_i</code> (si inferieur a la
     * taille max).
     */
    void ajouteFixe(final int _i, final double _v) {
      if (fixe_ == null) fixe_ = new TIntDoubleHashMap(taille_);
      if (_i < taille_) {
        fixe_.put(_i, _v);
      }
    }

    void ajouteFixe(final int[] _i, final double _k) {
      for (int k = 0; k < _i.length; k++) {
        ajouteFixe(_i[k], _k);
      }
    }

    /**
     * Si <code>_i</code> est inferieur a l'indice max (cf constructeur) l'ajoute en tant qu'indice libre.
     */
    void ajouteLibre(final int _i) {
      if (libre_ == null) libre_ = new TIntArrayList(taille_);
      if (_i < taille_) {
        libre_.add(_i);
      }
    }

    /**
     * Ajoute les <code>_nbIndices</code> premiers indices du tableau <code>_i</code> en tant que libre.
     * 
     * @see #ajouteLibre(int)
     */
    void ajouteLibre(final int[] _i) {
      if (libre_ == null) libre_ = new TIntArrayList(taille_);
      int nb = _i.length;
      for (int k = 0; k < nb; k++) {
        ajouteLibre(_i[k]);
      }
    }

    void ajouteTransitoire(final int _i, final EvolutionReguliereInterface _evol) {
      if (transitoire_ == null) transitoire_ = new TIntObjectHashMap(50);
      if (_i < taille_) {
        transitoire_.put(_i, _evol);
      }
    }

    /**
     * @return construit la structure correspondante.
     */
    public H2dNodalPropertyMixte getStructure() {
      final int nFixe = fixe_ == null ? 0 : fixe_.size();
      final int nLibre = libre_ == null ? 0 : libre_.size();
      final int nTrans = transitoire_ == null ? 0 : transitoire_.size();
      if (nFixe + nLibre + nTrans != taille_) {
        FuLog.warning("Des elements ne sont pas sp�cifi�s");
      }
      return new H2dNodalPropertyMixte(nom_, fixe_, transitoire_);
    }
  }

  public static H2dRefluxBoundaryConditionMutable createDefaultCL() {
    return new H2dRefluxBoundaryConditionMutable();
  }

  String baseFile_;

  List evolsCL_ = new ArrayList();
  List evolsPN_ = new ArrayList();
  String ficName_;

  /**
   * L'indice du premier element etant un bord.
   */
  int indexDebutBord_ = -1;
  /**
   * Le nombre d'�l�ments de bord (les �l�ments L3).
   */
  int nbElemBord_;
  /**
   * Le nombre d'�l�ments de fond (les �l�ments normaux).
   */
  int nbElemFond_;

  /**
   * Le nombre d'�l�ments d�finis ( lu avant les 2 autres).
   */
  // int nbElemTotalLu_;
  boolean readOnlyVersion_;

  ProgressionUpdater up_;

  int versionIn_;

  Double viscositeTransit_;

  protected INPAdapter projet_;

  /**
   * Les noeuds.
   */
  protected EfNode[] pts_;

  protected boolean readOnlyGrid_;

  protected boolean stop_;

  protected RefluxINPV5P0Version version_;

  protected int nbPropEle_;

  protected int nbGroupElem_;

  public INPReaderAbstract(RefluxINPV5P0Version _version) {
    version_ = _version;
  }

  protected abstract void analyzeElemMailEntete();

  protected abstract H2dRefluxBoundaryConditionMutable[] createConditionLimites();

  /**
   * @param _general
   */
  protected abstract boolean finishCreationInElemBloc(int _eltIdx, int _nbPtPerElement,
      H2dRefluxBordIndexGeneral _general);

  protected final void initReader() {
    stop_ = false;
    projet_ = new INPAdapter();
    evolsCL_ = new ArrayList();
    evolsPN_ = new ArrayList();
    up_ = new ProgressionUpdater(progress_);
    // On ignore les lignes blanches
    in_.setJumpBlankLine(true);
    in_.setBlankZero(true);
  }

  /**
   * Lecture difficile du format INP.
   */
  @Override
  protected final Object internalRead() {
    // Si pas de flux d'entree
    if (in_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Flux d'entr�e non trouv�"));
      return null;
    }
    initReader();
    // les groupes de pas de temps finaux
    // pour eviter de se faire avoir par l'exception levee
    // lorsque la fin du fichier est atteint.
    List gPT = null;

    try {
      // la structure du futur projet
      // BLOC ENTETE
      // on lit l'entete ....
      boolean ok = readEntete();
      if (!ok) {
        setProjectIsInvalid();
        return null;
      }
      if (stop_) return null;

      readImpressionParams();

      if (stop_) return null;

      // Bloc Data
      // Si le bloc DATA n'est pas trouve on peut s'arreter
      if (!in_.getLine().startsWith(version_.getDATA())) {
        analyze_.addFatalError(H2dResource.getS("Bloc de donn�es non trouv�"), in_.getLineNumber());
        addAnalyzeLigneLue();
        return null;
      }
      // BLOC TYPE PROJET
      final String refluxProjetNom = readProjectType();
      if (refluxProjetNom == null || stop_) {
        return null;
      }
      // on veut juste lire la version du fichier
      if (readOnlyVersion_) {
        return new Integer(versionIn_);
      }
      // Bloc COOR ou POIN
      pts_ = readCoord();
      if (pts_ == null) return null;

      // Bloc DLPN Typique maillage T6 Indique les degre de liberte de chaque point 3 extremite 2 milieu
      readDLPN();

      // Bloc COND Bloc definissant les differents groupes de condtions limites.
      final H2dRefluxBoundaryConditionMutable[] clTemp = readCond();
      if (clTemp == null || stop_) return null;
      // Bloc PRND Decrit pour chaque point frontiere la normale et le debit lineique.
      ok = readPRND(clTemp);
      saveCL(clTemp);
      if (stop_ || !ok) return null;
      // Bloc ELEM Definit les elements T6 puis T3
      ok = readElem();
      if (stop_ || !ok) return null;
      if (readOnlyGrid_) {
        return new EfGridSourceDefaut(projet_.getGrid(), getFileFormat());
      }

      // Bloc TPEL Definit pour chaque element son type. Les elements T6 ne sont pas interressant: toujours de type 5 :
      // on les ignore. Par contre, il faut connaitre le type des elements de bord.
      ok = readTPEL();
      if (stop_ || !ok) return null;
      // Bloc PREL Le bloc des proprietes elementaires. Donne pour chaque elements, le groupe de prop correspondant. Si
      // aucun, l'entier 0 est utilise.
      ok = readPREL();
      if (stop_) {
        return null;
      }
      // Bloc FICHIER Les fichiers entree,sortie, courbes transitoires.
      readFICHIER();
      if (stop_) {
        return null;
      }
      // Bloc SOLR
      readSOLR(refluxProjetNom);
      // Bloc CRAD Radiation
      readCRAD(refluxProjetNom);
      // Bloc VENT
      readVENT(refluxProjetNom);
      if (stop_) {
        return null;
      }
      // Bloc IMPRESSION inutile
      skipIMPRESSION();

      // Bloc TRANS
      skipTRANS();
      if (stop_) {
        return null;
      }
      // Bloc FLUT Lu tant que le bloc STOP n'est pas trouve.
      gPT = new ArrayList(20);
      gPT = readFLUT(gPT, refluxProjetNom);

    } catch (final EOFException _e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("End of file for inp file");
      }
      if (projet_.getDicoModel() == null || projet_.getEntiteValue() == null) {
        analyze_.addFatalError(H2dResource.getS("Fichier invalide"), in_.getLineNumber());
        return null;
      }
    } catch (final NumberFormatException _e) {
      analyze_.manageException(_e, in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    } catch (final IOException _e) {
      analyze_.manageException(_e);
      return null;
    }
    if (stop_ || projet_ == null) {
      return null;
    }
    if (gPT != null) projet_.setGroupePasTemps((H2dTimeStepGroup[]) gPT.toArray(new H2dTimeStepGroup[gPT.size()]));
    manageResult(projet_);
    // lecture des cl transitoires.
    readEvolsCL();
    if (stop_) {
      return null;
    }
    // lecture des pn transitoires.
    readEvolsPN();
    if (stop_) {
      return null;
    }
    return projet_;
  }

  private void saveCL(final H2dRefluxBoundaryConditionMutable[] _cl) {
    final TIntObjectHashMap clMap = new TIntObjectHashMap(_cl.length);
    for (int i = _cl.length - 1; i >= 0; i--) {
      final H2dRefluxBoundaryConditionMutable s = _cl[i];
      if (s != null) {
        clMap.put(s.getIndexPt(), s);
      }
    }
    projet_.setConditionLimite(clMap);
  }

  /**
   * @param _nbPtPerElement
   * @return
   */
  protected abstract boolean isElemBord(int _nbPtPerElement);

  private void manageResult(final INPAdapter _inter) {
    final EfGridMutable maillage = (EfGridMutable) _inter.getGrid();
    maillage.computeBord(progress_, analyze_);
  }

  private H2dRefluxBoundaryConditionMutable[] readCond() throws IOException {
    final H2dRefluxBoundaryConditionMutable[] clTemp = createConditionLimites();
    String bloc = in_.readLine();
    up_.majProgessionStateOnly(version_.getCOND());
    if (bloc == null || !bloc.startsWith(version_.getCOND())) {
      analyze_.addFatalError(H2dResource.getS("Le bloc COND n'a pas �t� trouv�"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }
    // Cet index indique quelle courbe transitoire utiliser
    int indexCL = evolsCL_.size() + 1;
    // les codes definissant une condition libre,permanent ou transitoire

    // Tableau temporaire contenant les conditions limites.
    // temp=nombre de point.
    // Ce tableau contiendra la condition limite a l'indice du point
    // correspondant.
    // Si le point d'indice 455 correspond a un bord, sa condition limite
    // se trouvera en clTemp[455]
    final int[] codeFormat = version_.getCONDCodeFormat();
    final int[] fmtCOND = version_.getCONDNoeudIdxFormat();
    // temp contient le nombre d'indices par ligne
    final int nbCol = fmtCOND.length;
    // Le code marquant la fin de l'enumeration des indices : 0
    int codeFin = version_.getCodeFin();
    final int[] troisDoubleFormat = version_.getTroisDoubleFormat();
    // Lecture de la 1ere ligne
    in_.readFields(codeFormat);
    String line = in_.getLine().trim();
    final String blocPRND = version_.getPRND();
    up_.setValue(10, clTemp.length);

    while (!line.startsWith(blocPRND)) {
      // Analyse des code
      // Format <code_u> <code_v> <code_h>
      final H2dBcType uCode = version_.getClType(in_.intField(0));
      if (uCode == null) {
        analyze_.addFatalError(H2dResource.getS("Le code pour la vitesse Un est inconnu"), in_.getLineNumber());
        addAnalyzeLigneLue();
        return null;
      }
      final H2dBcType vCode = version_.getClType(in_.intField(1));
      if (vCode == null) {
        analyze_.addFatalError(H2dResource.getS("Le code pour la vitesse ut est inconnu"), in_.getLineNumber());
        addAnalyzeLigneLue();
        return null;
      }
      final H2dBcType hCode = version_.getClType(in_.intField(2));
      if (hCode == null) {
        analyze_.addFatalError(H2dResource.getS("Le code pour la hauteur est inconnu"), in_.getLineNumber());
        addAnalyzeLigneLue();
        return null;
      }

      in_.readFields(troisDoubleFormat);
      // Si le code est permanent, on lit le champ et on affecte
      final double u = (uCode == H2dBcType.PERMANENT) ? in_.doubleField(0) : 0;
      final double v = (vCode == H2dBcType.PERMANENT) ? in_.doubleField(1) : 0;
      final double h = (hCode == H2dBcType.PERMANENT) ? in_.doubleField(2) : 0;

      // Devient false, si le code de fin (0) est lu
      boolean cont = true;
      // Format 16 colonnes de largeur 5 contenant les indices des points
      // concernes par cette caracteristique de bord.
      while (cont) {
        if (stop_) {
          return null;
        }
        in_.readFields(fmtCOND);
        // Lecture d'une ligne ( tant que nb lu inferieur au format et que
        // le code de fin n'est pas lu.
        for (int i = 0; i < nbCol && cont; i++) {
          // l'entier lu.
          int idxPt = in_.intField(i);

          // Si l'entier lu est le code de fin, stop
          if (idxPt == codeFin) {
            cont = false;
          } else {
            // les indices commencent a 0
            idxPt--;
            // il se peut que la condition limite pour ce point
            // soit deja intialisee. Il ne faut changer que ce qui
            // est demande.
            H2dRefluxBoundaryConditionMutable old = clTemp[idxPt];
            if (old == null) {
              up_.majAvancement();
              // Pas d'ancienne cl... c'est tout bon
              old = createDefaultCL();
              clTemp[idxPt] = old;
              old.setUType(uCode);
              old.setVType(vCode);
              old.setHType(hCode);
              old.setU(u);
              old.setV(v);
              old.setH(h);
              old.setIndexPt(idxPt);
            } else {
              // cl deja existante. ne changer que si permanent
              // le meme point est specifie de nouveau: le groupe
              // precedent et le courant imposent une valeur non libre.
              if (((uCode != H2dBcType.LIBRE) && (old.getUType() != H2dBcType.LIBRE))
                  || ((vCode != H2dBcType.LIBRE) && (old.getVType() != H2dBcType.LIBRE))
                  || ((hCode != H2dBcType.LIBRE) && (old.getHType() != H2dBcType.LIBRE))) {
                analyze_.addWarn(H2dResource.getS("Le point {0} est sp�cifi� plusieurs fois", CtuluLibString
                    .getString(idxPt + 1)), in_.getLineNumber());
                addAnalyzeLigneLue();
              }
              // on ne change les valeurs que si elles ne sont pas de type libre
              if (uCode != H2dBcType.LIBRE) {
                old.setUType(uCode);
                old.setU(u);
              }
              if (vCode != H2dBcType.LIBRE) {
                old.setVType(vCode);
                old.setV(v);
              }
              if (hCode != H2dBcType.LIBRE) {
                old.setHType(hCode);
                old.setH(h);
              }
            }
            // cas transitoire
            // Pour chaque point transitoire on incremente le compteur.
            if (uCode == H2dBcType.TRANSITOIRE) {
              EvolutionReguliere evol = new EvolutionReguliere("CL" + indexCL++);
              evolsCL_.add(evol);
              old.setUTransitoire(evol);
            }
            if (vCode == H2dBcType.TRANSITOIRE) {
              EvolutionReguliere evol = new EvolutionReguliere("CL" + indexCL++);
              evolsCL_.add(evol);
              old.setVTransitoire(evol);
            }
            if (hCode == H2dBcType.TRANSITOIRE) {
              EvolutionReguliere evol = new EvolutionReguliere("CL" + indexCL++);
              evolsCL_.add(evol);
              old.setHTransitoire(evol);
            }

          }

        }
      }
      in_.readFields(codeFormat);
      line = in_.getLine().trim();
    }
    return clTemp;
  }

  protected abstract EfNode[] readCoord() throws IOException;

  protected final void readCRAD(final String _refluxProjetNom) throws IOException {
    String line = in_.getLine();
    if (line.startsWith(version_.getCRAD())) {
      in_.readFields(version_.getCRADFormat());
      String sTemp = in_.stringField(0).trim();
      // on ne traite que les fichiers avec un seul bloc de donnees.
      if (_refluxProjetNom.equals(sTemp)) {
        projet_.setContientRadiations(true);
        if (in_.intField(1) > 0) {
          projet_.setRadiationsLecturePasDeTemps(true);
        }
      }
      sTemp = in_.readLine().trim();
    }
  }

  protected abstract void readDLPN() throws IOException;

  /**
   * Affecte le maillage.
   * 
   * @return true si ok
   * @throws IOException
   */
  protected final boolean readElem() throws IOException {
    readBlocElem();

    // Format ELEM PROJET <nb_elem> <nb_max_point_par_elem> <nombre point max par elem>
    // le nouveau format indique
    // Format ELEM PROJET <nb_elem> <nb_max_point_par_elem> <nombre de groupe elementaire> <nb_max_prop_par_group>
    if (!version_.getELEM().equals(in_.stringField(0))) {
      analyze_.addFatalError(H2dResource.getS("Le bloc ELEM n'a pas �t� trouv�"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return false;
    }
    analyzeElemMailEntete();
    up_.majProgessionStateOnly(version_.getELEM());
    // temp1 = le nombre d'elements
    final int nbElemTotalLu = in_.intField(2);
    // temp2 =le nombre max de pts par element
    int nbMaxPtPerElement = in_.intField(3);
    // ...
    if (nbMaxPtPerElement != version_.getELEMTailleFond()) {
      analyze_.addFatalError(H2dResource.getS("Seuls les �l�ments T6 sont autoris�s"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return false;
    }
    int[] fmtElt = version_.getELEMValeurFormat();
    // Les elements de bords sont de type L3 temp=3.
    int tempElt = version_.getELEMTailleBord();
    // maillage.typeElement = LTypeElement.T6;
    // les elements de fond ont un code 5.
    // Ce tableau comporte les elements T6 a la suite
    List elems = new ArrayList(nbElemTotalLu);
    // int[][] elem = new int[temp1][];
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("DRE: total element number " + nbElemTotalLu);
    }
    // Ce tableau contient aux indexs d'apparition les elements
    // de bords
    // H2dBordMutable[] elemBord = null;
    H2dRefluxBordIndexGeneral[] elemBordElt = null;
    // true si L3 rencontre
    boolean elemBordCommence = false;
    // l'index du premier element de bord.
    indexDebutBord_ = -1;
    // contiendra le nb d'element de fond.
    int indexEnCoursElt = 0;
    int[] tempArrayElt;
    // maillage.elements = elem;
    up_.setValue(10, nbElemTotalLu);
    final int offset = fmtElt == null ? 1 : 0;
    for (int i = 0; i < nbElemTotalLu; i++) {
      if (stop_) {
        return false;
      }
      // format libre
      // si format libre, la premiere colonne est l'indice de l'�l�ment
      if (fmtElt == null) in_.readFields();
      else
        in_.readFields(fmtElt);
      // Element de bord : sur 3 entier donc le dernier index est vide
      // Element de fond : le dernier index n'est pas une chaine vide.
      // ELEMENT DE FOND
      H2dRefluxBordIndexGeneral bordEnCours = null;
      if (!isElemBord(nbMaxPtPerElement)) {
        // T6 alors que L3 rencontre ....
        if (elemBordCommence) {
          analyze_.addFatalError(H2dResource.getS("Des �l�ments T6 se trouvent apr�s des �l�ments T3"), in_
              .getLineNumber());
          addAnalyzeLigneLue();
          return false;
        }
        tempArrayElt = new int[nbMaxPtPerElement];
        // elem[indexEnCoursElt] = new int[temp2];

        for (int j = 0; j < nbMaxPtPerElement; j++) {
          // les indexs commencent a 0
          tempArrayElt[j] = in_.intField(offset + j) - 1;
        }
        elems.add(new EfElement(tempArrayElt));
        indexEnCoursElt++;
      }
      // ELEMENT DE BORD
      else {
        // c'est le premier element de bord
        if (!elemBordCommence) {
          indexDebutBord_ = i;
          // le nombre d'elements de bord peut etre determiner: le nb total
          // d'element moins l'indice en cours
          elemBordElt = new H2dRefluxBordIndexGeneral[nbElemTotalLu - i];
          elemBordCommence = true;
        }
        // Les L3 sont a la suite. Donc index vaut 0 pour le premier, etc...
        int index = i - indexDebutBord_;
        if (elemBordElt == null) {
          return false;
        }
        bordEnCours = new H2dRefluxBordIndexGeneral();
        elemBordElt[index] = bordEnCours;
        tempArrayElt = new int[tempElt];
        for (int j = 0; j < tempElt; j++) {
          tempArrayElt[j] = in_.intField(offset + j) - 1;
        }
        elemBordElt[index].setIndex(tempArrayElt);
      }
      if (!finishCreationInElemBloc(i, nbMaxPtPerElement, bordEnCours)) return false;

      up_.majAvancement();
    }
    nbElemFond_ = indexEnCoursElt;
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("DRE : bottom element number =" + nbElemFond_);
    }
    nbElemBord_ = elemBordElt == null ? 0 : elemBordElt.length;
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("DRE: boundary element number =" + nbElemBord_);
    }
    EfGridMutable grid = new EfGridMutable(pts_, (EfElement[]) elems.toArray(new EfElement[elems.size()]));
    grid.setTypeElt(EfElement.getCommunType(grid));
    projet_.setMaillage(grid);
    projet_.setBords(elemBordElt);
    return true;
  }

  protected void readBlocElem() throws IOException {
    in_.readFields(version_.getELEMFormat());
  }

  protected final boolean readEntete() throws IOException {
    // l la ligne lue
    String currentLine = in_.readLine().trim();
    // bloc1 et bloc2 representent les prochains blocs a reconnaitre
    // le bloc1 IMPRESSION
    // bloc2 BLOC: DATA
    String bImpression = version_.getIMPRESSION();
    final String bData = version_.getDATA();
    // Pour reconnaitre, le num de version, on sait que la ligne commence
    // par Cree par
    String entete = version_.getENTETEVersionLineStart();
    // Juste avant la version le texte contient PREFLUX 5.0
    // un peu ...
    String entetePrefix = version_.getENTETEVersionPrefixe();
    // tant que les blocs impressions ou data ne sont pas trouve
    // on lit l'entete
    boolean foundEntete = false;
    while ((!currentLine.startsWith(bImpression)) && (!currentLine.startsWith(bData))) {
      // A la recherche de la version
      // si elle n'est pas encore trouvee on teste le debut de la ligne.
      if ((!foundEntete) && (currentLine.startsWith(entete))) {
        int temp = currentLine.lastIndexOf(entetePrefix);
        if (temp > 0) {
          projet_.setVersion(currentLine.substring(temp + entetePrefix.length() + 1, currentLine.length()));
          foundEntete = true;
        }
      }
      currentLine = in_.readLine().trim();
    }

    projet_.setDicoModel(version_.getRefluxDico());
    projet_.setEntitesValeurs(new HashMap());
    return foundEntete;
  }

  protected final void readFICHIER() throws IOException {
    String sTempFic = in_.readLine().trim();
    final List fic = new ArrayList(version_.getNbMaxFichiers());
    // sTemp= in_.readLine().trim();
    String sTemp1 = version_.getFICHIER();
    int index = sTemp1.length();
    while (sTempFic.startsWith(sTemp1)) {
      if (stop_) {
        return;
      }
      final String s = sTempFic.substring(index).trim();
      if (s.indexOf(CtuluLibString.ESPACE) < 0) {
        fic.add(s);
      } else {
        final StringTokenizer st = new StringTokenizer(s, CtuluLibString.ESPACE);
        while (st.hasMoreElements()) {
          fic.add(st.nextToken());
        }
      }
      sTempFic = in_.readLine().trim();
    }
    final String[] fics = new String[fic.size()];
    fic.toArray(fics);
    // System.out.println(getClass().getName() + " " + CtuluLibString.arrayToString(fics));
    projet_.setFics(fics);
  }

  protected final List readFLUT(final List _gPT, final String _refluxProjetNom) throws IOException {
    // on lit le prob a considerer
    if (!in_.readLine().trim().equals(_refluxProjetNom)) {
      analyze_.addFatalError("Un seul type de probl�me support�");
      return null;
    }
    up_.majProgessionStateOnly(version_.getFLUT());
    String line = in_.readLine().trim();
    String bloc1 = version_.getSTOP();
    boolean laxwenWarn = false;
    Map map = projet_.getEntiteValue();
    while (!line.startsWith(bloc1)) {
      if (stop_) {
        return null;
      }
      // Format: BLOC: FLUT
      // TYPE PROBLEME
      //
      // <nb_pas_temps> <t0>
      //
      // TYPE PROBLEME
      final H2dTimeStepGroup gt = new H2dTimeStepGroup();
      // le nombre de pas de temps

      in_.analyzeCurrentLine(version_.getFLUTNbPasTemps());
      final int nbTemps = in_.intField(0);
      if (!projet_.isBeginTimeSet()) {
        projet_.setBeginTime(in_.doubleField(1));
      }
      line = in_.readLine().trim();
      if (line.equals(_refluxProjetNom)) {
        _gPT.add(gt);
      }
      in_.readFields(version_.getFLUTSchemaResolutionFormat());
      // FORMAT : <schema_resolution> <valeur_pas_temps> <coef_schema>
      // on recupere le schema de resolution
      final H2dResolutionSchemaType schema = H2dResolutionSchemaType.getTypeForRefluxId(in_.stringField(0).trim());
      if (!laxwenWarn && schema == H2dResolutionSchemaType.LAXWEN) {
        analyze_.addError(H2dResource
            .getS("LE SCHEMA LAXWEN N'EST PAS COMPLETEMENT SUPPORTE: LES VARIABLES UTILISEES SONT qw,qy et H"), in_
            .getLineNumber());
        laxwenWarn = true;
      }
      // si schema connu
      if (schema != null) {
        gt.setSchema(schema);
        gt.setNbPasTemps(nbTemps);
        // si non stationnaire on peut lire les donnees
        if (schema != H2dResolutionSchemaType.STATIONNAIRE) {
          gt.setValeurPasTemps(in_.doubleField(1));
          gt.setCoefSchema(in_.doubleField(2));
        }
      } else {
        analyze_.addError(H2dResource.getS("Sch�ma non support�"), in_.getLineNumber());
        addAnalyzeLigneLue();
        gt.setSchema(H2dResolutionSchemaType.STATIONNAIRE);
      }
      in_.readFields(version_.getFLUTMethodeResolutionFormat());
      // FORMAT : <methode_resolution> <relaxation> <precision> <precision_bcd> <nb_max_iteration>
      if (schema != null) {
        H2dResolutionMethodType meth = H2dResolutionMethodType.getTypeForRefluxId(in_.stringField(0).trim());
        if (meth == null) {
          analyze_.addError(H2dResource.getS("M�thode de r�solution non trouv�e"), in_.getLineNumber());
          addAnalyzeLigneLue();
          meth = H2dResolutionMethodType.LINEAIRE;
        } else if (!schema.isSupported(meth)) {
          analyze_.addError(H2dResource.getS("M�thode de r�solution non valide pour le sch�ma") + " "
              + schema.getName(), in_.getLineNumber());
          addAnalyzeLigneLue();
          gt.setMethode(H2dResolutionMethodType.LINEAIRE);
        } else {
          gt.setMethode(meth);
        }
        if (meth != null) {
          if (meth.needViscosity()) {
            double d = 0;
            if (viscositeTransit_ == null) {
              FuLog.warning("prob with transit viscosity");
            } else {
              d = viscositeTransit_.doubleValue();
            }
            if (Fu.DEBUG && FuLog.isDebug()) {
              FuLog.debug("DEBUG: transit viscosity value= " + d);
            }
            gt.setMethodeValue(H2dVariableType.VISCOSITE, d);
          }
          gt.setRelaxation(in_.doubleField(1));
          gt.setPrecision(in_.doubleField(2));
          gt.setPrecisionBancCouvrantDecouvrant(in_.doubleField(3));
          gt.setNombreIterationMax(in_.intField(4));
        }
      }
      line = in_.readLine();
      // FORMAT COEFFICIENTS c1 c2 c3 c4 c5 c6
      // c7 ....
      if (!line.startsWith(version_.getFLUTCoefficientContribution())) {
        analyze_.addError(H2dResource.getS("Le bloc est invalide. Le bloc des coefficients est attendu"), in_
            .getLineNumber());
      }
      // en general, 12 coefs
      // TDoubleArrayList coef = new TDoubleArrayList(12);
      int[] fmt = version_.getCoefficientContributionFormat();
      int temp = fmt.length;
      boolean cont = true;
      // bloc2 = version_.getIMPRESSION();
      int nbCols = 0;
      // on lit tant que champ non vide et bloc stop non atteint
      while ((cont) && (!line.startsWith(bloc1))) {
        if (stop_) {
          return null;
        }
        in_.analyzeCurrentLine(fmt);
        for (int i = 1; (cont) && (i < temp); i++) {
          DicoEntite ent = null;
          if (CtuluLibString.isEmpty(in_.stringField(i))) {
            cont = false;
          } else {
            try {
              ent = version_.getCoefContribution(nbCols++);
            } catch (final IllegalArgumentException _ex) {
              _ex.printStackTrace();
            }
            double dTemp = in_.doubleField(i);
            // Seul les premiers coeff de contribution sont pris en compte.
            if ((ent != null) && (!map.containsKey(ent))) {
              map.put(ent, Double.toString(dTemp));
            } else {
              analyze_.addInfo(H2dResource.getS("Coefficient de contribution non pris en compte"), in_.getLineNumber());
            }
          }
        }
        line = in_.readLine().trim();
      }
      // affectation des coefficients.
      in_.analyzeCurrentLine(version_.getFLUTFreqImpressionFormat());
      gt.setFrequenceImpression(in_.intField(1));
      line = in_.readLine().trim();
    }
    return _gPT;
  }

  protected final void readImpressionParams() throws IOException {
    final Map map = projet_.getEntiteValue();
    final H2dRefluxDicoModel model = projet_.getDicoModel();
    // BLOC IMPRESSION
    final String bImpression = version_.getIMPRESSION();
    String currentLine = in_.getLine().trim();
    if (currentLine.startsWith(bImpression)) {
      if (stop_) {
        return;
      }
      final int taille = bImpression.length();
      // Tant que la ligne commence par impression, on traite les infos
      // H2dParametresCalculReflux param = new H2dParametresCalculReflux();
      // projet_.setParametresCalcul(param);
      DicoEntite ent;
      do {
        ent = model.getEntite(currentLine.substring(taille).trim());
        if (ent != null) {
          map.put(ent, "true");
        }
        currentLine = in_.readLine().trim();
      } while (currentLine.startsWith(bImpression));
    }
  }

  protected boolean readPREL() throws IOException {
    if (!readPRELentete()) return false;

    up_.majProgessionStateOnly(version_.getPREL());
    // le nombre de prop elementaire
    // le nb de groupe
    // on cree nbPropElementaire classes qui contiendra
    // toutes les donnees necessaires pour le groupe.
    final TemplateGroupeElement[] propElem = createPropElementaire();
    final int[][] elemParGroupArray = defineGroupe(propElem);
    if (stop_) return false;
    final int nbElemTotal = nbElemBord_ + nbElemFond_;
    viscositeTransit_ = null;
    up_.setValue(10, nbGroupElem_);
    final int clLibre = version_.getCodeLibre();
    final int clPermanent = version_.getCodePermanent();
    final int clTransitoire = version_.getCodeTransitoire();
    final H2dRefluxBordIndexGeneral[] elemBordElt = projet_.getBords();
    int[] fmtForCode = version_.getGroupElemCodeFormat();
    int[] fmtForFond = version_.getHuitDoubleFormat();
    for (int i = 0; i < nbGroupElem_; i++) {
      if (stop_) {
        return false;
      }
      // Attention: les prop elementaires normales sont donnees puis celle
      // concernant les bords (une donnee)
      // Lecture des codes
      // le meme format est garde
      // temp represente le nombre de prop elementaire
      // temp1 = le nombre de code par ligne
      // indexEnCours represente l'index de la prop lue.
      final int[] codes = new int[nbPropEle_];
      // pour les bords,seule une infos est donnee : le frottement.
      boolean bord = false;
      int indexEnCoursElt = 0;
      final int nbCol = fmtForCode.length;
      // on lit les codes pour les prop qui ne sont pas de bord
      // Format <code_viscosite> <code_rugosite> <code_alpha_longueur_melange> <code_perte_charge>
      // etc....
      // indexEnCours represente le nombre de props lues.
      // la variable bord vaut true si bord ( si le nombre de prop est
      // inferieur au nb donne).
      int indexPn = evolsPN_.size();
      while ((indexEnCoursElt < nbPropEle_) && (!bord)) {
        in_.readFields(fmtForCode);
        for (int j = 0; (j < nbCol) && (!bord) && (indexEnCoursElt < nbPropEle_); j++) {
          // ce cas represente une propriete de rugosite pour des elements
          // de bord.
          if (CtuluLibString.isEmpty(in_.stringField(j))) {
            bord = true;
          } else {
            codes[indexEnCoursElt++] = in_.intField(j);
          }
        }
      }
      // le nombre d'elements concernes par ce groupe
      final int[] elemsFori = elemParGroupArray[i];
      // Prop de bord
      if (bord) {
        // La rugosite est specifie pour les elements. Ils doivent
        // etre des elements de bords.
        if (indexEnCoursElt == 1) {
          // on recupere le type de prop ( permanent,libre ou transitoire)
          int indexElement;
          final int code = codes[0];
          H2dBcType codeFinal = null;
          double valeur = 0;
          if (code == clPermanent) {
            valeur = Double.parseDouble(in_.readLine().trim());
            codeFinal = H2dBcType.PERMANENT;
          } else {
            if (code == clTransitoire) {
              codeFinal = H2dBcType.TRANSITOIRE;
            } else {
              codeFinal = H2dBcType.LIBRE;
            }
            in_.readLine();
          }
          // Pour chaque elements du groupe, puis chaque point de l'element
          // on recupere la condition limite concernee et on affecte, le frottement.
          for (int k = 0; k < elemsFori.length; k++) {
            // l'element
            indexElement = elemsFori[k];
            // si element de bord
            if (indexElement >= nbElemFond_) {
              // les indices des points concernes
              final int itemp = indexElement - nbElemFond_;
              if (elemBordElt == null) {
                return false;
              }
              final H2dBoundaryType bordType = elemBordElt[itemp].getBordType();
              if (bordType != H2dRefluxBoundaryType.SOLIDE) {
                // erreur dans le fichier
                analyze_.addError(H2dResource.getS("Le bord {0} n'est pas un bord solide avec frottement",
                    CtuluLibString.getString(indexElement - nbElemFond_)), in_.getLineNumber());
              } else {
                // on affecte le bon type pour le bord.
                elemBordElt[itemp].setBord(H2dRefluxBoundaryType.SOLIDE_FROTTEMENT);
                elemBordElt[itemp].setRugositeType(codeFinal);
                elemBordElt[itemp].setRugosite(valeur);
                if (codeFinal == H2dBcType.TRANSITOIRE) {
                  final EvolutionReguliere evol = new EvolutionReguliere("PN" + (++indexPn));
                  evolsPN_.add(evol);
                  elemBordElt[itemp].setRugositeTransitoireCourbe(evol);
                }
              }
            } else {
              // Ce n'est pas un point frontiere et pourtant
              analyze_
                  .addError(
                      H2dResource.getS("Seule la rugosit� est sp�cifi�e alors que ce n'est pas un point de bord"), in_
                          .getLineNumber());
            }
          }
        } else {
          analyze_.addError(H2dResource.getS("Des propri�t�s ne sont pas renseign�es"), in_.getLineNumber());
        }
      } else {
        // Cas d'un groupe de fond.
        int nbCols = fmtForFond.length;
        // Cela ne devrait jamais arriver
        // le nb de prop annonce n'est pas egal au nb lu.
        // indexEnCours=nb prop lu.
        // temp=le nombre de prop elementaire
        if (nbElemTotal == 0) {
          // a voir !!
          in_.readFields(fmtForFond);
          // La viscosit� n'est qu'ici ?
          viscositeTransit_ = CtuluLib.getDouble(in_.doubleField(0));
        } else if (indexEnCoursElt != nbPropEle_) {
          analyze_.addWarn(H2dResource
              .getS("Le nombre de caract�ristiques pour les propri�t�s �l�mentaires est incorrect"), in_
              .getLineNumber());
          analyze_.addInfo(H2dResource.getS("les indexs correspondants seront suppos�s libres"), in_.getLineNumber());
          for (int j = 0; j < nbPropEle_; j++) {
            propElem[j].ajouteLibre(elemsFori);
          }
          final int index = indexEnCoursElt / nbCols;
          for (int j = 0; j < index; j++) {
            in_.readLine();
          }
        } else {
          // on lit les valeurs pour chaque prop. Les valeurs peuvent etre
          // sur plusieurs lignes...
          // indexEnCours represente la propriete traitee.
          indexEnCoursElt = 0;

          while (indexEnCoursElt < nbPropEle_) {
            in_.readFields(fmtForFond);
            // Pour chaque prop

            for (int j = 0; (j < nbCols) && (indexEnCoursElt < nbPropEle_); j++) {
              final TemplateGroupeElement tge = propElem[indexEnCoursElt];
              final int code = codes[indexEnCoursElt++];
              if (code == clLibre) {
                tge.ajouteLibre(elemsFori);
              } else if (code == clPermanent) {
                tge.ajouteFixe(elemsFori, in_.doubleField(j));
              } else if (code == clTransitoire) {
                for (int k = 0; k < elemsFori.length; k++) {
                  final EvolutionReguliere evol = new EvolutionReguliere("PN" + (++indexPn));
                  evolsPN_.add(evol);
                  tge.ajouteTransitoire(elemsFori[k], evol);
                }
              }
            }
          }

        }
      }
      up_.majAvancement();
    }
    // Toutes les prop sont definies.
    final H2dNodalPropertyMixte[] propElementaires = new H2dNodalPropertyMixte[nbPropEle_];
    for (int i = 0; i < nbPropEle_; i++) {
      if (stop_) {
        return false;
      }
      propElementaires[i] = propElem[i].getStructure();
    }
    projet_.setPropElementaires(propElementaires);
    // projet_.setNbPNTransitoires(courbeTransitoirePnIndex);
    // Maintenant, on peut achever le travail concernant les points de frontiere
    // et leur condition limite.

    return true;
  }

  protected int[][] defineGroupe(final TemplateGroupeElement[] _propElem) throws IOException {
    // Format: 16 colonnes de largeur 5 (entier)
    final int[] fmtElt = version_.getCONDNoeudIdxFormat();
    // temp=le nombre de prop elementaire
    // temp= propElem.length;
    // le nombre d'entier sur une ligne
    final int nbColsElt = fmtElt.length;
    // le nombre total d'element.
    final int nbEltTotal = nbElemBord_ + nbElemFond_;
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("DRE: " + nbPropEle_ + " elementary properties");
      FuLog.debug("DRE: " + nbGroupElem_ + " groups");
    }
    // contient le nombre d'element par groupe : des compteurs pour chaque groupe.
    // final int[] atemp = new int[nbGroupElem_];
    // contient les elements par groupe.
    final TIntArrayList[] elemParGroupe = initArrayListForGroup();
    // indexEnCours represente l'indice de l'element traite.
    int indexEnCours = 0;
    // on parcourt tous les elements
    up_.setValue(10, nbEltTotal);
    while (indexEnCours < nbEltTotal) {
      if (stop_) {
        return null;
      }
      in_.readFields(fmtElt);
      for (int i = 0; (i < nbColsElt) && (indexEnCours < nbEltTotal); i++) {
        // les indices commencent a 0
        final int index = in_.intField(i) - 1;
        // les indices negatif correspondent aux elements pour lesquels
        // rien n'est specifie super !
        if (index < 0) {
          // pour toutes les prop elementaires, c'est un element libre
          for (int j = 0; j < nbPropEle_; j++) {
            _propElem[j].ajouteLibre(indexEnCours);
          }
        }
        // verif utile ??
        else if (index < nbGroupElem_) {
          // ajoute au groupe d'indice "index", l'indice de l'element
          // a la place correspondante.
          // compteur= atemp[index] donc
          elemParGroupe[index].add(indexEnCours);
        } else {
          analyze_.addWarn(H2dResource.getS("L'�l�ment {0} poss�de un num�ro de groupe non valide", CtuluLibString
              .getString(indexEnCours)), in_.getLineNumber());
        }
        indexEnCours++;
        up_.majAvancement();
      }
    }

    final int[][] elemParGroupArray = CtuluLibArray.toArray(elemParGroupe);
    return elemParGroupArray;
  }

  protected TIntArrayList[] initArrayListForGroup() {
    final int nbEltTotal = nbElemBord_ + nbElemFond_;
    final TIntArrayList[] elemParGroupe = new TIntArrayList[nbGroupElem_];
    for (int i = 0; i < elemParGroupe.length; i++) {
      elemParGroupe[i] = new TIntArrayList(Math.max(10, nbEltTotal / 20));
    }
    return elemParGroupe;
  }

  private TemplateGroupeElement[] createPropElementaire() {
    final TemplateGroupeElement[] propElem = new TemplateGroupeElement[nbPropEle_];
    // on connait le nom des 4 premieres prop. Pour les autres, PROP+ indices.
    for (int i = nbPropEle_ - 1; i >= 0; i--) {
      propElem[i] = new TemplateGroupeElement("PROP " + (i + 1), nbElemFond_);
    }
    return propElem;
  }

  protected abstract boolean readPRELentete() throws IOException;

  protected abstract boolean readPRND(final H2dRefluxBoundaryConditionMutable[] _clTemp) throws IOException;

  protected final String readProjectType() throws IOException {
    // La premier ligne contient le type de projet
    in_.readFields(version_.getTypeProjetFormat());
    final String refluxProjetNom = in_.stringField(0).trim();
    // recupere l'objet correspondant. si inconnnu on sort
    final H2dProjetType typeProjet = H2dProjetType.getProjetFromRefluxId(refluxProjetNom);
    if (typeProjet == null) {
      setProjectIsInvalid();
      return null;
    }
    projet_.setTypeProjet(typeProjet);
    versionIn_ = in_.intField(1);
    return refluxProjetNom;
  }

  protected final void readSOLR(final String _refluxProjetNom) throws IOException {
    String line = in_.getLine();
    if (line.startsWith(version_.getSOLR())) {
      in_.readFields(version_.getSOLRFormat());
      String sTemp = in_.stringField(0).trim();
      // on ne traite que les fichiers avec un seul bloc de donnees.
      if (_refluxProjetNom.equals(sTemp)) {
        projet_.setContientSolReparties(true);
      }
      sTemp = in_.readLine().trim();
    }
  }

  protected abstract boolean readTPEL() throws IOException;

  protected final void readVENT(final String _refluxProjetNom) throws IOException {
    if (in_.getLine().startsWith(version_.getVENT())) {
      in_.readFields(version_.getVENTFormat());
      String sTemp = in_.stringField(0).trim();
      // on ne traite que les fichiers avec un seul bloc de donnees.
      if (_refluxProjetNom.equals(sTemp)) {
        projet_.setContientVent(true);
        if (in_.intField(1) > 0) {
          projet_.setVentLecturePasDeTemps(true);
        }
      }
      sTemp = in_.readLine().trim();
    }
  }

  protected final void setProjectIsInvalid() {
    analyze_.addFatalError(H2dResource.getS("Type de projet inconnu"), in_.getLineNumber());
    addAnalyzeLigneLue();
  }

  protected final void skipIMPRESSION() throws IOException {
    final String blocIMP = version_.getIMPRESSION();
    String line = in_.getLine();
    while (line.startsWith(blocIMP)) {
      line = in_.readLine().trim();
    }
  }

  protected final void skipTRANS() throws IOException {
    if (in_.getLine().startsWith(version_.getTRANS())) {
      analyze_.addWarn("Le bloc TRANS est ignor�", in_.getLineNumber());
      in_.readLine();
    }
    String line = in_.getLine();
    String flut = version_.getFLUT();
    while (!line.startsWith(flut)) {
      if (stop_) {
        return;
      }
      line = in_.readLine();
    }
  }

  /**
   * @return le format utilise.
   */
  public final FileFormat getFileFormat() {
    return getINPFileFormat();
  }

  /**
   * @return le format utilise.
   */
  public final INPFileFormat getINPFileFormat() {
    return (INPFileFormat) version_.getFileFormat();
  }

  /**
   * @return la version utilisee.
   */
  public final FileFormatVersionInterface getVersion() {
    return version_;
  }

  public int getVersionIn() {
    return versionIn_;
  }

  public final boolean isReadOnlyGrid() {
    return readOnlyGrid_;
  }

  public boolean isReadOnlyVersion() {
    return readOnlyVersion_;
  }

  @Override
  public final void setFile(final File _f) {
    super.setFile(_f);
    baseFile_ = _f.getAbsoluteFile().getParentFile().getAbsolutePath();
    ficName_ = CtuluLibFile.getSansExtension(_f.getName());
  }

  public final void setReadOnlyGrid(final boolean _readOnlyGrid) {
    readOnlyGrid_ = _readOnlyGrid;
  }

  public void setReadOnlyVersion(boolean _readOnlyVersion) {
    readOnlyVersion_ = _readOnlyVersion;
  }

  public final void stop() {
    stop_ = true;

  }

  protected void readEvolsCL() {
    // lecture des cl transitoires.
    if (evolsCL_.size() > 0) {

      String ficClv = version_.getCLTransitoireFichier(projet_.getFichiers());
      if (ficClv == null) {
        ficClv = ficName_ + CtuluLibString.DOT + version_.getClTransitoireFileExtension();
      }
      final File clFile = new File(baseFile_, ficClv);
      if (!clFile.exists()) {
        analyze_.addError(H2dResource.getS("Le fichier des conditions limites transitoires est introuvable")
            + CtuluLibString.ESPACE + clFile.getAbsolutePath(), 0);
      } else {
        final CLVReader clvr = version_.createCLVReader();
        clvr.setEvolutions((EvolutionReguliere[]) evolsCL_.toArray(new EvolutionReguliere[evolsCL_.size()]));
        clvr.setFile(clFile);
        clvr.setProgressReceiver(progress_);
        final CtuluIOOperationSynthese s = clvr.read();
        analyze_.merge(s.getAnalyze());
      }
    }
  }

  protected void readEvolsPN() {
    if (evolsPN_.size() > 0) {
      if (stop_ || projet_ == null) {
        return;
      }

      String ficPnv = version_.getPnTransitoireFichier(projet_.getFichiers());
      if (ficPnv == null) {
        ficPnv = ficName_ + version_.getClTransitoireFileExtension();
      }
      final File clFile = new File(baseFile_, ficPnv);
      if (!clFile.exists()) {
        analyze_.addError(H2dResource.getS("Le fichier des propri�t�s nodales transitoires est introuvable")
            + CtuluLibString.ESPACE + clFile.getAbsolutePath(), in_.getLineNumber());
      } else {
        final CLVReader pnvr = version_.createCLVReader();
        final EvolutionReguliere[] evols = new EvolutionReguliere[evolsPN_.size()];
        evolsPN_.toArray(evols);
        pnvr.setEvolutions(evols);
        pnvr.setFile(clFile);
        pnvr.setProgressReceiver(progress_);
        final CtuluIOOperationSynthese s = pnvr.read();
        analyze_.merge(s.getAnalyze());
      }
    }
  }

}