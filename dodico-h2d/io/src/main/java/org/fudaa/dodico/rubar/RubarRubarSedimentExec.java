/**
 * @creation 13 oct. 2004
 * @modification $Date: 2006-09-19 14:45:57 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar;

/**
 * @author Fred Deniger
 * @version $Id: RubarRubarSedimentExec.java,v 1.2 2006-09-19 14:45:57 deniger Exp $
 */
public class RubarRubarSedimentExec extends RubarExecAbstract {

  public RubarRubarSedimentExec() {
    super("rubar20ts");
  }
}
