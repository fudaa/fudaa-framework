/**
 * @creation 15 avr. 2003
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquideMutable;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;
import org.fudaa.dodico.h2d.telemac.H2dTelemacTransient;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: TelemacLiquideReader.java,v 1.22 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacLiquideReader extends FileOpReadCharSimpleAbstract {

  TelemacLiquideFileFormat version_;

  /**
   * Intialise avec la derniere version dispo.
   */
  public TelemacLiquideReader() {
    this(TelemacLiquideFileFormat.getInstance());
  }

  /**
   * @param _f la version a utiliser.
   */
  public TelemacLiquideReader(final TelemacLiquideFileFormat _f) {
    version_ = _f;
  }

  /**
   * @return les evolutions liquides lues. null si erreur fatale.
   */
  public TelemacLiquideInterface readLiquide() {
    TelemacLiquideAbstract inter = null;
    if (in_ == null) {
      analyze_.addFatalError(H2dResource.getS("Flux d'entr�e non trouv�"));
      return null;
    }
    inter = new TelemacLiquideAbstract();
    H2dTelemacEvolutionFrontiere[] evols = null;
    in_.setJumpBlankLine(true);
    in_.setCommentInOneField(version_.getCommentChar());
    String stemp;
    //    int itemp;
    double lastTemps = -1d;
    try {
      in_.readFields();
      while (in_.isCommentLine()) {
        in_.readFields();
      }
      // Plus de commentaires.
      // Une ligne avec au moins un champ + le premier ="T"
      final int nbValue = in_.getNumberOfFields() - 1;
      final int nbChamp = in_.getNumberOfFields();
      boolean ligneNonReconnue = false;
      if ((nbValue > 0) && (version_.isTempsVariable(in_.stringField(0)))) {
        evols = new H2dTelemacEvolutionFrontiere[nbValue];
        // values = new FuVectordouble[nbValue];
        for (int i = 0; i < nbValue; i++) {
          stemp = in_.stringField(i + 1);

          final int indexPar = stemp.indexOf('(');
          final int indexParFermante = stemp.indexOf(')');
          if ((indexPar < 0) || (indexParFermante < 0)) {
            ligneNonReconnue = true;
          } else {
            final H2dVariableType var = H2dTelemacTransient.getVariableForTelemacId(stemp.substring(0, indexPar));
            if (var == null) {
              ligneNonReconnue = true;
            } else {
              String indexes = stemp.substring(indexPar + 1, indexParFermante);
              int frontierIdx = -1;
              int variableIdx = -1;
              if (indexes.indexOf(',') > 0) {
                String[] parseString = CtuluLibString.parseString(indexes, ",");
                if (parseString.length == 2) {
                  try {
                    frontierIdx = Integer.parseInt(parseString[0]);
                    variableIdx = Integer.parseInt(parseString[1]);
                  } catch (Exception e) {
                    ligneNonReconnue = true;
                  }
                } else {
                  ligneNonReconnue = true;
                }

              } else {
                try {
                  frontierIdx = Integer.parseInt(indexes);
                } catch (Exception e) {
                  ligneNonReconnue = true;
                }

              }
              if (frontierIdx >= 0) {
                H2dEvolutionFrontiereLiquideMutable evolution = new H2dEvolutionFrontiereLiquideMutable(var, frontierIdx - 1);
                evolution.setNom(stemp);
                H2dTelemacEvolutionFrontiere telemacEvolution = new H2dTelemacEvolutionFrontiere();
                telemacEvolution.setEvolution(evolution);
                telemacEvolution.setIdxVariable(variableIdx - 1);
                evols[i] = telemacEvolution;
              }
            }
          }
        }
      } else {
        ligneNonReconnue = true;
      }
      if (ligneNonReconnue) {
        analyze_.addFatalError(H2dResource.getS("Ligne non valide"), in_.getLineNumber());
        analyze_.addInfo(H2dResource.getS("Ligne lue") + ": " + in_.getLine(), in_.getLineNumber());
        analyze_.addInfo(H2dResource.getS("Ligne attendue") + ": T Q ...", in_.getLineNumber());
        return null;
      }
      // on vire les commentaires
      in_.readFields();
      while (in_.isCommentLine()) {
        in_.readFields();
      }
      // lecture des unites
      if (in_.getNumberOfFields() != nbChamp) {
        analyze_.addError(H2dResource.getS("Nombre de champs invalide"), in_.getLineNumber());
        analyze_.addInfo(H2dResource.getS("Ligne lue") + ": " + in_.getLine(), in_.getLineNumber());
        return null;
      }
      stemp = in_.stringField(0);
      if (!"s".equals(stemp)) {
        analyze_.addInfo(H2dResource.getS("L'unit� du temps est trait�e en tant que seconde"), in_.getLineNumber());
      }
      if (evols != null) {
        for (int i = 1; i < nbChamp; i++) {
          ((H2dEvolutionFrontiereLiquideMutable) evols[i - 1].getTelemacEvolution()).setUnite(in_.stringField(i));
        }
      }
      while (true) {
        in_.readFields();
        if (!in_.isCommentLine()) {
          if (in_.getNumberOfFields() != nbChamp) {
            analyze_.addError(H2dResource.getS("nombre de champs invalide"), in_.getLineNumber());
            analyze_.addInfo(H2dResource.getS("Ligne lue") + ": " + in_.getLine(), in_.getLineNumber());
            return null;
          }
          final double ts = in_.doubleField(0);
          if (ts < lastTemps) {
            analyze_.addError(H2dResource.getS("Les valeurs du temps doivent �tre croissantes"), in_.getLineNumber());
            return null;
          }
          lastTemps = ts;
          if (evols != null) {
            for (int i = 1; i < nbChamp; i++) {
              String field = in_.stringField(i);
              double value = 0;
              if (field.indexOf('D') >= 0) {
                value = Double.parseDouble(field.replace('D', 'E'));
              } else {
                value = Double.parseDouble(field);
              }
              ((H2dEvolutionFrontiereLiquideMutable) evols[i - 1].getTelemacEvolution()).add(ts,value);
            }
          }
        }
      }
    } catch (final EOFException _e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("end of file for telemac liquid file");
      }
    } catch (final NumberFormatException _e) {
      analyze_.manageException(_e);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    if (evols == null) {
      analyze_.addError(H2dResource.getS("Fichier vide"), in_.getLineNumber());
      return null;
    }
    inter.setEvolutionsFrontieresLiquides(evols);
    return inter;
  }

  public TelemacLiquideFileFormat getTelemacLiquideFileFormat() {
    return (TelemacLiquideFileFormat) version_.getFileFormat();
  }

  @Override
  protected Object internalRead() {
    return readLiquide();
  }

}