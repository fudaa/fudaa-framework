/**
 * @creation 28 sept. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.IOException;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.H2dParallelogrammeDataInterface;
import org.fudaa.dodico.h2d.H2dRegularGridDataInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarVF2MResultInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarVF2MWriter.java,v 1.9 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarVF2MWriter extends FileOpWriterCharSimpleAbstract {

  /**
   * @param _isInverse
   */
  public RubarVF2MWriter(final boolean _isInverse) {
    super();
    isInverse_ = _isInverse;
  }

  /**
   * @param _r les parametres grid.
   */
  protected void writeGrid(final H2dRegularGridDataInterface _r) throws IOException {
    final String space = CtuluLibString.ESPACE;
    final int ny = _r.getNbPtOnY();
    final int nx = _r.getNbPtOnX();
    // NYMAX,NXMAX
    writelnToOut(ny + space + nx);
    // DX,DY
    writelnToOut(_r.getDX() + space + _r.getDY());
    // X0,Y0
    writelnToOut(_r.getX0() + space + _r.getY0());
    int reelx;
    // le nombre de valeur
    final int nbVal = _r.getNbValues();
    // l'avancement de la tache
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nx);
    // NXMAX fois
    for (int i = 0; i < nx; i++) {
      reelx = isInverse_ ? (nx - i - 1) : i;
      // I
      // writeln(CtuluLibString.getString(reelx + 1));
      // VALUE(I,J) pour chaque valeur
      for (int vali = 0; vali < nbVal; vali++) {
        final StringBuffer b = new StringBuffer();
        for (int j = 0; j < ny; j++) {
          if (j > 0) {
            b.append(space);
          }
          // attention bien utilise reelx: le vrai indice de x.
          b.append(_r.getValues(vali, reelx * ny + j));
        }
        writelnToOut(b.toString());
      }
      up.majAvancement();
    }

  }

  boolean isInverse_;

  @Override
  protected void internalWrite(final Object _o) {
    // donnees invalide : erreur +retour
    if (!(_o instanceof H2dRubarVF2MResultInterface)) {
      donneesInvalides(_o);
      return;
    }
    final H2dRubarVF2MResultInterface result = (H2dRubarVF2MResultInterface) _o;
    try {
      // on ecrit la premiere ligne: uniquement pour ini pour l'instant
      if (result.getFirstLine() != null) {
        writelnToOut(result.getFirstLine());
      }
      // si grid on appelle la methode speciale
      if (!result.isParall()) {
        writeGrid(result.getGridData());
        return;
      }
      // le nombre de parall
      final int np = result.getNbParall();
      final String space = CtuluLibString.ESPACE;
      // -1,NP
      writelnToOut("-1 " + np);
      // la progresssion
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(4, result.getNbParall());
      // on parcourt tous les parallelogrammes
      for (int i = 0; i < np; i++) {
        final H2dParallelogrammeDataInterface par = result.getParall(i);
        // les X XP(1,I),XP(2,I),XP(4,I)
        writelnToOut(par.getX1() + space + par.getX2() + space + par.getX4());
        // les Y YP(1,I),YP(2,I),YP(4,I)
        writelnToOut(par.getY1() + space + par.getY2() + space + par.getY4());
        // les valeurs Z1,Z2,Z3,Z4
        for (int vali = 0; vali < par.getNbValues(); vali++) {
          final StringBuffer b = new StringBuffer();
          for (int xi = 0; xi < 4; xi++) {
            if (xi > 0) {
              b.append(space);
            }
            b.append(par.getValue(vali, xi));
          }
          writelnToOut(b.toString());
        }
        up.majAvancement();
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }
}