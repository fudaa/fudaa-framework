/**
 * @creation 16 d�c. 2004
 * @modification $Date: 2007-06-20 12:21:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;

import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: RefluxRefondeSolutionSequentielResult.java,v 1.1 2007-06-20 12:21:45 deniger Exp $
 */
public class RefluxRefondeSolutionSequentielResult {
  long delimLength_;
  /**
   * Taille d'un ligne d'entete t=.
   */
  long enteteLength_;
  long firstPost_;

  RefluxRefondeSolutionFileFormat ft_;
  long lineLength_;
  int sizeFirstCol_;
  int nbPoint_;
  int nbVar_;
  double[] timeStep_;
  int typeProjet_ = -1;

  final boolean isRefonde_;

  final boolean isSot_;

  boolean isRefondeSeiche_;

  public RefluxRefondeSolutionSequentielResult(File _f) {
    if (isSot(_f)) {
      isRefonde_ = false;
      isSot_ = true;
    } else if (isRefonde(_f)) {
      isRefonde_ = true;
      isSot_ = false;
    } else {
      isRefonde_ = false;
      isSot_ = false;
    }
  }

  public static H2dVariableType[] getReflux2DOldVersionVars() {
    final H2dVariableType[] variable = new H2dVariableType[5];
    int idx = 0;
    variable[idx++] = H2dVariableType.BATHYMETRIE;
    variable[idx++] = H2dVariableType.VITESSE_U;
    variable[idx++] = H2dVariableType.VITESSE_V;
    variable[idx++] = H2dVariableType.COTE_EAU;
    variable[idx++] = H2dVariableType.HAUTEUR_EAU;
    return variable;
  }

  public static H2dVariableType[] getReflux2DVars() {
    final H2dVariableType[] variable = new H2dVariableType[5];
    int idx = 0;
    variable[idx++] = H2dVariableType.COTE_EAU;
    variable[idx++] = H2dVariableType.HAUTEUR_EAU;
    variable[idx++] = H2dVariableType.BATHYMETRIE;
    variable[idx++] = H2dVariableType.VITESSE_U;
    variable[idx++] = H2dVariableType.VITESSE_V;
    return variable;
  }

  public H2dVariableType[] getRefondeVars() {
    if (isRefondeSeiche_) return getRefondeSeicheVars();
    final H2dVariableType[] variable = new H2dVariableType[6];
    int idx = 0;
    variable[idx++] = refondeFirstVar_;
    variable[idx++] = H2dVariableType.HOULE;
    variable[idx++] = H2dVariableType.BATHYMETRIE;
    variable[idx++] = H2dVariableType.SXX;
    variable[idx++] = H2dVariableType.SXY;
    variable[idx++] = H2dVariableType.SYY;
    return variable;
  }

  public static H2dVariableType[] getRefondeSeicheVars() {
    final H2dVariableType[] variable = new H2dVariableType[2];
    int idx = 0;
    // variable[idx++] = H2dVariableType.;
    // ariable[idx++] = H2dVariableType.HOULE;
    variable[idx++] = H2dVariableType.BATHYMETRIE;
    variable[idx++] = H2dVariableType.COTE_EAU;
    return variable;
  }

  public static int[] getRefondeSeicheCols() {
    return new int[] { 2, 3 };
  }

  public static H2dVariableType[] getReflux2DSotVars() {
    return new H2dVariableType[] { H2dVariableType.BATHYMETRIE, H2dVariableType.TEMPERATURE };
  }

  public H2dVariableType[] getVars() {
    if (isRefonde_) {
      return getRefondeVars();
    } else if (isSot_) {
      return getReflux2DSotVars();
    }
    if (nbVar_ == 9) return getReflux2DOldVersionVars();
    return getReflux2DVars();

  }

  public int[] getPosition() {
    if (isRefonde_) {
      return getRefondeCols(isRefondeSeiche_);
    } else if (isSot_) {
      return getReflux2DSotCols();
    }
    // l'ancienne version de Reflux
    if (nbVar_ == 9) return getReflux2DOldVersionCols();
    // la nouvelle
    return getReflux2DCols();
  }

  public static int[] getReflux2DOldVersionCols() {
    return new int[] { 2, 3, 4, 6, 7 };
  }

  public static int[] getReflux2DCols() {
    return new int[] { 0, 1, 2, 3, 4 };
  }

  public static int[] getRefondeCols(boolean _isSeiche) {

    return _isSeiche ? getRefondeSeicheCols() : new int[] { 0, 1, 2, 3, 4, 5 };
  }

  public static int[] getReflux2DSotCols() {
    return new int[] { 2, 3 };
  }

  public static boolean isSot(final File _f) {
    return _f != null && _f.getName().toLowerCase().endsWith(".sot");
  }

  public static boolean isRefonde(final File _f) {
    return _f != null && _f.getName().toLowerCase().endsWith(".sol");
  }

  public static int[] getRefluxCols(final File _f) {
    return isSot(_f) ? getReflux2DSotCols() : getReflux2DCols();
  }

  /**
   * @return le nombre de point.
   */
  public final int getNbPoint() {
    return nbPoint_;
  }

  public int guessSizeForCol(String _l) {
    return _l.indexOf('1') + 1;

  }

  public int getLengthLastColForReflux() {
    return nbVar_ == 9 ? 5 : ft_.getDataColLength();
  }

  public int getLengthLastCol() {
    if (isRefonde_) return ft_.getDataColLength();
    return nbVar_ == 9 ? 5 : ft_.getDataColLength();
  }

  /**
   * @return le nombre de variable
   */
  public final int getNbVar() {
    return nbVar_;
  }

  /**
   * @param _idx l'indice du pas de temps
   * @return le pas de temps demande
   */
  public final double getTimeStep(final int _idx) {
    return timeStep_[_idx];
  }

  /**
   * @return le nombre de pas de temps
   */
  public final int getTimeStepNb() {
    return timeStep_.length;
  }

  /**
   * @return le type du projet lu dans le fichier dat
   */
  public final int getTypeProjet() {
    return typeProjet_;
  }

  protected boolean isRefondeSeiche() {
    return isRefondeSeiche_;
  }

  H2dVariableType refondeFirstVar_ = H2dVariableType.PHASE;

  public void setRefondeSeiche(boolean _isRefondeSeiche, H2dVariableType _firstVar) {
    isRefondeSeiche_ = _isRefondeSeiche;
    refondeFirstVar_ = _firstVar;
  }

  public boolean isRefonde() {
    return isRefonde_;
  }
}