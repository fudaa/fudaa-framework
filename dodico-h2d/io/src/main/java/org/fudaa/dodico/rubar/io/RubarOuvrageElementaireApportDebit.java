/**
 * @creation 22 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireApportI;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Map;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: RubarOuvrageElementaireApportDebit.java,v 1.3 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarOuvrageElementaireApportDebit extends RubarOuvrageElementaireTransfertDebit implements
    H2dRubarOuvrageElementaireApportI {
  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.APPORT_DEBIT;
  }

  @Override
  public void fillWithTransientCurves(String prefixe, H2dEvolutionVariableMap r) {
    if (evolutionsByType != null) {
      for (Map.Entry<H2dVariableType, EvolutionReguliere> entry : evolutionsByType.entrySet()) {
        entry.getValue().setNom(prefixe + "/" + getType() + ": " + entry.getKey().getLongName());
      }
    }
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    return false;
  }
}
