package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireCompositeI;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageCompositeTypeControle;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Set;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarOuvrageElementaireComposite implements H2dRubarOuvrageElementaireCompositeI {
  H2dRubarOuvrageCompositeTypeControle typeControle;
  double xAmont;
  double yAmont;
  double xAval;
  double yAval;
  double[] valeurs;
  H2dRubarOuvrageElementaireInterface[] ouvrages;

  @Override
  public H2dRubarOuvrageCompositeTypeControle getTypeControle() {
    return typeControle;
  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {

  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
    if (ouvrages != null) {
      int nb = ouvrages.length;
      for (int i = 0; i < nb; i++) {
        ouvrages[i].fillWithTransientCurves(pref + "/" + H2dResource.getS("Composite {0}", Integer.toString(i + 1)), r);
      }
    }
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {
    if (ouvrages != null) {
      int nb = ouvrages.length;
      for (int i = 0; i < nb; i++) {
        ouvrages[i].fillWithTarageEvolution(prefix + "/" + H2dResource.getS("Composite {0}", Integer.toString(i + 1)), allTarage);
      }
    }
  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    if (ouvrages != null) {
      int nb = ouvrages.length;
      for (int i = 0; i < nb; i++) {
        if (ouvrages[i].containsEvolution(tarage)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
  }

  @Override
  public double getXAmont() {
    return xAmont;
  }

  @Override
  public double getYAmont() {
    return yAmont;
  }

  @Override
  public double getXAval() {
    return xAval;
  }

  @Override
  public double getYAval() {
    return yAval;
  }

  @Override
  public double[] getValeurs() {
    return valeurs;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface[] getOuvrages() {
    return ouvrages;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return null;
  }

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.COMPOSITE;
  }
}
