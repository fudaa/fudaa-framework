/**
 * @creation 8 juin 2004
 * @modification $Date: 2007-06-05 08:59:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarTimeConditionMutable;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarCLIReader.java,v 1.16 2007-06-05 08:59:15 deniger Exp $
 */
public class RubarCLIReader extends FileOpReadCharSimpleAbstract {
  private static final double DEFAULT_DIAMETER = 0.0;
  private static final double DEFAULT_ETENDUE = 0.0;
  private static final int FORMAT_WITHOUT_TRANSPORT = 0;
  private static final int FORMAT_OLD_WITH_TRANSPORT = 1;
  private static final int FORMAT_NEW_WITH_TRANSPORT = 2;
  private static final int[][] FORMATS = new int[][]{{6, 10, 10, 10}, {6, 10, 10, 10, 10}, {6, 10, 10, 10, 10, 10, 10}};
  private static final int[][] FORMATS_8 = new int[][]{{6, 8, 8, 8}, {6, 8, 8, 8, 8}, {6, 8, 8, 8, 8, 8, 8}};
  public static final int LENGTH_COLUMN_OLD_INDEX = 6;
  public static final int LENGTH_COLUMN_INDEX = 9;
  public static final int LENGTH_COLUMN_VALUE = 10;
  private boolean newFormat;
  int nbConcentration;
  /**
   * the format to use to read lines
   */
  private int[] format;
  /**
   * For old format, contains the index of the format to use
   */
  int fmtIndexForOldFiles;

  /**
   * Ne fait rien.
   */
  public RubarCLIReader() {
  }

  public boolean isNewFormat() {
    return newFormat;
  }

  public static int getColumnIndexLength(boolean newFormat) {
    return newFormat ? LENGTH_COLUMN_INDEX : LENGTH_COLUMN_OLD_INDEX;
  }

  @Override
  protected Object internalRead() {
    H2dRubarTimeConditionMutable[] source = null;
    int nbTimeStep = 0;
    try {
      // LECTURE DES ELEMENTS
      in_.readFields();
      // lecture ne
      nbTimeStep = in_.intField(0);
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(4, nbTimeStep);
      in_.readFields();
      final int nas = in_.intField(0);
      // areteIdx = new int[nas];
      source = new H2dRubarTimeConditionMutable[nas];
      in_.setBlankZero(true);
      fmtIndexForOldFiles = 0;
      format = FORMATS[0];
      // on parcourt les pas de temps
      for (int i = 0; i < nbTimeStep; i++) {
        in_.readFields();
        final double t = in_.doubleField(0);
        // on parcourt les ar�tes
        for (int ias = 0; ias < nas; ias++) {
          if ((i == 0) && (ias == 0)) {//the first line of data
            //we have to find the format to use:
            in_.readLine();
            analyzeFirstLineToDetermineFormat();
            in_.analyzeCurrentLine(format);
          } else {
            in_.readFields(format);
          }

          H2dRubarTimeConditionMutable s = null;
          if (i == 0) {
            s = new H2dRubarTimeConditionMutable(nbTimeStep, nbConcentration);
            source[ias] = s;
          } else {
            s = source[ias];
          }

          this.setValues(s, t, fmtIndexForOldFiles);
        }
        up.majAvancement();
      }
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
    } catch (final EOFException _e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("End file for Rubar cli");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    if (source == null || source.length == 0) {
      return null;
    }
    if (source[source.length - 1] == null || source[0] == null) {
      analyze_.addError(H2dResource.getS("Erreur dans la lecture du fichier cli"));
    }
    // verif pas de temps
    else if (source.length > 0) {
      final int nbRead = source[0].h_.getNbValues();
      if (nbRead != nbTimeStep) {
        analyze_.addWarn(
            H2dResource.getS("Nombre de pas de temps attendu: {0}, lu: {1}", CtuluLibString.getString(nbTimeStep), CtuluLibString.getString(nbRead)),
            -1);
      }
    }
    return source;
  }

  private boolean isTransportFile() {
    return fmtIndexForOldFiles != FORMAT_WITHOUT_TRANSPORT;
  }

  private boolean canContainBlock;

  private void analyzeFirstLineToDetermineFormat() {
    //the new format use 9 columns for the first index
    newFormat = isNewFormat(in_.getLine());
    canContainBlock = false;
    if (!newFormat) {
      //some old format file should be supported here:
      fmtIndexForOldFiles = this.getFormatIndex(in_.getLine(), FORMATS);
      if (fmtIndexForOldFiles < 0) {
        fmtIndexForOldFiles = this.getFormatIndex(in_.getLine(), FORMATS_8);
        if (fmtIndexForOldFiles >= 0) {
          //in the old files, there is only one concentration block.
          nbConcentration = fmtIndexForOldFiles == FORMAT_WITHOUT_TRANSPORT ? 0 : 1;
          format = FORMATS_8[fmtIndexForOldFiles];
          return;
        }
      } else {
        //in the old files, there is only one concentration block.
        nbConcentration = fmtIndexForOldFiles == FORMAT_WITHOUT_TRANSPORT ? 0 : 1;
        format = FORMATS[fmtIndexForOldFiles];
        return;
      }
    }
    canContainBlock = true;
    final int lengthColumnIndex = getColumnIndexLength(newFormat);
    nbConcentration = getNbConcentrations(in_.getLine(), lengthColumnIndex);

    format = getNewFormat(nbConcentration, lengthColumnIndex);
  }

  /**
   * @param nbConcentration
   * @return fmt for new type of file.
   */
  public static int[] getNewFormat(int nbConcentration, int lengthColumnIndex) {
    //the format is 9 + 10 * nbvalues
    //3 values for normal cli + 3 by concentration blocks.
    int[] format = new int[1 + 3 + 3 * nbConcentration];
    Arrays.fill(format, LENGTH_COLUMN_VALUE);
    format[0] = lengthColumnIndex;
    return format;
  }

  private int getFormatIndex(String line, final int[][] formats) {
    for (int i = 0; i < formats.length; i++) {
      if (RubarFortranDoubleReader.isCorrectFormat(line, formats[i])) {
        return i;
      }
    }

    return -1;
  }

  /**
   * @param line
   * @return = if no transport. The number of concentration/diam/etendu otherwise
   */
  public static int getNbConcentrations(String line, int sizeIndex) {
    int lengthLine = line.length();
    //index + h + qn + qt
    int sizeNoTransport = sizeIndex + LENGTH_COLUMN_VALUE * 3;
    if (lengthLine == sizeNoTransport) {
      return 0;
    }
    return Math.max(1, (lengthLine - sizeNoTransport) / (3 * LENGTH_COLUMN_VALUE));
  }

  public static boolean isNewFormat(String line) {
    if (line == null || line.length() <= LENGTH_COLUMN_INDEX + LENGTH_COLUMN_VALUE) {
      return false;
    }
    //the last chars of the column should ne be empty
    if (line.charAt(LENGTH_COLUMN_INDEX - 1) == ' ') {
      return false;
    }
    String firtIndex = line.substring(0, LENGTH_COLUMN_INDEX).trim();
    try {
      Integer.parseInt(firtIndex);
    } catch (Exception ex) {
      return false;
      //do nothing as it means it's not a integer.
    }
    int[] fmt = new int[4];
    Arrays.fill(fmt, LENGTH_COLUMN_VALUE);
    fmt[0] = LENGTH_COLUMN_INDEX;

    final int nbMinChars = CtuluLibArray.getSum(fmt);
    if (line.length() < nbMinChars) {
      return false;
    }

    String cut = line.substring(0, nbMinChars);
    return RubarFortranDoubleReader.isCorrectFormat(cut, fmt, true);
  }

  private void setValues(H2dRubarTimeConditionMutable s, double t, int fmtIndex) {
    s.h_.add(t, in_.doubleField(1));
    s.qn_.add(t, in_.doubleField(2));
    s.qt_.add(t, in_.doubleField(3));
    if (canContainBlock) {
      if (nbConcentration > 0) {
        int firstColumnConcentration = 4;
        for (int i = 0; i < nbConcentration; i++) {
          s.addValueConcentration(i, t, in_.doubleField(firstColumnConcentration + i * 3));
          s.addValueDiametre(i, t, in_.doubleField(firstColumnConcentration + i * 3 + 1));
          s.addValueEtendue(i, t, in_.doubleField(firstColumnConcentration + i * 3 + 2));
        }
      }
    } else {
      if ((fmtIndex != FORMAT_WITHOUT_TRANSPORT)) {
        s.addValueConcentration(0, t, in_.doubleField(4));
        if (fmtIndex == FORMAT_NEW_WITH_TRANSPORT) {
          s.addValueDiametre(0, t, in_.doubleField(5));
          s.addValueEtendue(0, t, in_.doubleField(6));
        } else {
          s.addValueDiametre(0, t, DEFAULT_DIAMETER);
          s.addValueEtendue(0, t, DEFAULT_ETENDUE);
        }
      }
    }
  }
}
