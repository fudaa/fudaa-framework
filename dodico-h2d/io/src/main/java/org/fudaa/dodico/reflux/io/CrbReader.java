/**
 *  @creation     15 d�c. 2003
 *  @modification $Date: 2006-10-24 12:48:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * @author deniger
 * @version $Id: CrbReader.java,v 1.15 2006-10-24 12:48:36 deniger Exp $
 */
public class CrbReader extends FileOpReadCharSimpleAbstract {
  int nbLine_;

  public CrbReader() {

  }

  @Override
  protected Object internalRead() {
    final List r = new ArrayList();
    if (in_ == null) {
      return null;
    }
    try {
      final int[] fmt = new int[2];
      fmt[0] = 12;
      fmt[1] = 10;
      final int[] fmtEntete = new int[] { 20, 1, 5 };
      final int[] fmtCorps = new int[] { 10, 1, 10 };
      in_.readFields(fmt);
      if (in_.stringField(1).compareTo("4.0") < 0) {
        analyze_.addFatalError(DodicoLib.getS("Seules les versions > � {0} sont autoris�es", "4.0"));
        return null;
      }
      in_.setJumpBlankLine(true);
      int nb;
      while (true) {
        in_.readFields(fmtEntete);
        if (in_.getLine().startsWith("*")) {
          continue;
        }
        nb = in_.intField(2);
        final EvolutionReguliere evol = new EvolutionReguliere(nb);
        r.add(evol);
        evol.setNom(in_.stringField(0));
        if ((progress_ != null) && (nbLine_ > 0)) {
          progress_.setProgression((int) ((double) in_.getLineNumber() / (double) nbLine_ * 100d));
        }
        while (nb > 0) {
          in_.readFields(fmtCorps);
          if (!in_.getLine().startsWith("*")) {
            evol.add(in_.doubleField(0), in_.doubleField(2));
            nb--;
          }
        }
      }

    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("CRB end of file");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    final EvolutionReguliereAbstract[] rf = new EvolutionReguliereAbstract[r.size()];
    r.toArray(rf);
    return rf;
  }

  @Override
  public void setFile(final File _f) {
    super.setFile(_f);
    nbLine_ = (int) (_f.length() * 35d / 810d);
  }

}
