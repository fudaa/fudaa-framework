/*
 * @creation 7 juin 2005
 * 
 * @modification $Date: 2007-06-20 12:21:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntArrayList;

import java.io.IOException;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISPoint;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridTranslate;

/**
 * @author Fred Deniger
 * @version $Id: RefluxRefondeResultatGisAdapter.java,v 1.1 2007-06-20 12:21:47 deniger Exp $
 */
public class RefluxRefondeResultatGisAdapter implements GISDataModel {
  final int[] cols_;
  final GISAttributeInterface[] att_;
  GISPoint[] pt_;
  final RefluxSolutionSequentielReader reader_;
  double[][] preloadData_;
  final EfGridInterface res_;
  final int timeStep_;

  public RefluxRefondeResultatGisAdapter(final RefluxSolutionSequentielReader _reader, final EfGridInterface _res,
      final GISAttributeInterface[] _attributes, final int[] _cols, final int _timeStep) {
    super();
    att_ = _attributes;
    reader_ = _reader;
    cols_ = _cols;
    res_ = _res;
    timeStep_ = _timeStep;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new RefluxRefondeResultatGisAdapter(reader_, new EfGridTranslate(res_, xyToAdd.getX(), xyToAdd.getY()), att_, cols_, timeStep_);
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return att_[_idxAtt];
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    if (preloadData_ != null && preloadData_[_idxAtt] != null)
      return preloadData_[_idxAtt][_idxGeom];
    try {
      return reader_.read(cols_[_idxAtt], timeStep_, _idxGeom);
    } catch (final IOException e) {
      e.printStackTrace();
    }
    return 0;
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return res_.getEnvelope(null);
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    if (pt_ == null) {
      pt_ = new GISPoint[res_.getPtsNb()];
    }
    if (pt_[_idxGeom] == null) {
      pt_[_idxGeom] = new GISPoint(res_.getPtX(_idxGeom), res_.getPtY(_idxGeom), 0);
    }
    return pt_[_idxGeom];
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    return CtuluLibArray.findObject(att_, _att);
  }

  @Override
  public int getNbAttributes() {
    return att_.length;
  }

  @Override
  public int getNumGeometries() {
    return res_.getPtsNb();
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return CtuluLib.getDouble(getDoubleValue(_idxAtt, _idxGeom));
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
    if (_att == null)
      return;
    if (preloadData_ == null)
      preloadData_ = new double[att_.length][];
    // indices des colonnes a precharcher dans le tableau des attributs
    TIntArrayList colToPreload = new TIntArrayList(_att.length);
    // indices des colonnes dans le fichier res
    TIntArrayList colInReader = new TIntArrayList(_att.length);
    for (int i = 0; i < _att.length; i++) {
      // on recherche la place de l'attribut voulu
      int idx = CtuluLibArray.findObject(att_, _att[i]);
      if (idx >= 0) {
        colToPreload.add(idx);
        // on recup�re son indice dans le fichier
        colInReader.add(cols_[idx]);
      } else if (_att[i] != null) {
        FuLog.trace("DRE: attribute not found " + _att[i]);
      }

    }
    int size = colInReader.size();
    // tableau tempo utilise par le reader
    double[][] values = new double[size][res_.getPtsNb()];
    try {
      // on remplit le tableau values
      reader_.read(colInReader.toNativeArray(), timeStep_, values);
      // on remplit le tableau preload
      for (int i = 0; i < size; i++) {
        preloadData_[colToPreload.getQuick(i)] = values[i];
      }
    } catch (IOException _evt) {
      FuLog.error(_evt);

    }
  }
}
