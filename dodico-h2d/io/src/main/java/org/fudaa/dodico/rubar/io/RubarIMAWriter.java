/**
 */
package org.fudaa.dodico.rubar.io;

import java.io.IOException;

import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 *
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarIMAWriter extends FileOpWriterCharSimpleAbstract {

  @Override
  protected void internalWrite(final Object _o) {
    if (!(_o instanceof EfGridInterface)) {
      donneesInvalides(_o);
      return;
    }
    final EfGridInterface grid = (EfGridInterface) _o;

    try {
      final FortranWriter writer = new FortranWriter(out_);
      final int nbElem = grid.getEltNb();
      final int[] fmtLength = new int[]{6, 6};
      final int[] fmtNodes = new int[]{10, 10, 10, 10};

      for (int i = 0; i < nbElem; i++) {
        writer.intField(0, 1);
        writer.intField(1, 1);

        writer.writeFields(fmtLength);

        final EfElement element = grid.getElement(i);
        final double[] xs = new double[4];
        final double[] ys = new double[4];
        int nbPt = element.getPtNb();
        if (nbPt != 3 && nbPt != 4) {
          analyze_.addFatalError(H2dResource.getS(
                  "Seuls les maillages triangle (T3) ou quadrilat�re (Q4) peuvent �tre export�s au format IMA"));
          return;
        }

        for (int j = 0; j < 3; j++) {
          final int ptIdx = element.getPtIndex(j);

          xs[j] = grid.getPtX(ptIdx);
          ys[j] = grid.getPtY(ptIdx);
        }

        if (element.getPtNb() == 3) {
          xs[3] = xs[0];
          ys[3] = ys[0];
        } else {
          final int ptIdx = element.getPtIndex(3);

          xs[3] = grid.getPtX(ptIdx);
          ys[3] = grid.getPtY(ptIdx);
        }

        writer.doubleField(0, xs[0]);
        writer.doubleField(1, ys[0]);
        writer.doubleField(2, xs[1]);
        writer.doubleField(3, ys[1]);

        writer.writeFields(fmtNodes);

        writer.doubleField(0, xs[2]);
        writer.doubleField(1, ys[2]);
        writer.doubleField(2, xs[3]);
        writer.doubleField(3, ys[3]);

        writer.writeFields(fmtNodes);
      }
    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      analyze_.manageException(e);
    }

  }
}