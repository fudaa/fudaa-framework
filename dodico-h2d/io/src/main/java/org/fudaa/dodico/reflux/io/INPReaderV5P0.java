/*
 * @creation 21 mars 2003
 * @modification $Date: 2007-06-29 15:10:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.IOException;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryConditionMutable;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author deniger
 * @version $Id: INPReaderV5P0.java,v 1.1 2007-06-29 15:10:23 deniger Exp $
 */
public class INPReaderV5P0 extends INPReaderAbstract {

  /**
   * le reader : format par defaut (le dernier).
   */
  public INPReaderV5P0() {
    this(INPFileFormat.getInstance().getLastV5P0Impl());
  }

  /**
   * @param _f la version a utiliser.
   */
  public INPReaderV5P0(final RefluxINPV5P0Version _f) {
    super(_f);
  }

  @Override
  protected void analyzeElemMailEntete() {}

  @Override
  protected H2dRefluxBoundaryConditionMutable[] createConditionLimites() {
    final int nbPts = pts_.length;
    final H2dRefluxBoundaryConditionMutable[] clTemp = new H2dRefluxBoundaryConditionMutable[nbPts];
    return clTemp;
  }

  @Override
  protected boolean finishCreationInElemBloc(final int _idx, final int _nbPtPerElement,
      final H2dRefluxBordIndexGeneral _general) {
    return true;
  }

  @Override
  protected boolean isElemBord(final int _nbPtPerElement) {
    final String stringField = in_.stringField(_nbPtPerElement - 1);
    return CtuluLibString.EMPTY_STRING.equals(stringField) || CtuluLibString.ZERO.equals(stringField);
  }

  @Override
  protected EfNode[] readCoord() throws IOException {
    // Ce format est bizarre. Il impose 5 caract�res max pour
    // le nb de points. A ameliorer V 6.0 ?
    // Ligne de type
    // Format: COOR <type_projet> <nb_donnees_par_ligne> <nb_points>
    in_.readFields(version_.getCOORFormat());
    // Verif "pedantic"
    String bloc = in_.stringField(0);
    if (!bloc.equals(version_.getCOOR())) {
      analyze_.addFatalError(H2dResource.getS("Le bloc des coordonn�es est introuvable"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }
    if (stop_) {
      return null;
    }
    // Verif "pedantic" le projet defini par ces coordonnees doit
    // etre le meme que le principal.
    bloc = in_.stringField(1);
    // if (!sTemp1.equals(sTemp))
    // {
    // analyze_.addWarn(DodicoLib.geti18n("Le type de projet du bloc des coordonn�es est diff�rent
    // de celui annonc� par le bloc DATA"), in_);
    // }
    final int nbCoord = in_.intField(2);
    if (nbCoord != 3) {
      analyze_.addFatalError(H2dResource.getS("Le nombre de coordonn�es doit �tre �gal � 3"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }
    // temp contient le nombre de points
    final int nbPt = in_.intField(3);
    if (nbPt <= 0) {
      analyze_.addFatalError(H2dResource.getS("Le nombre de points est invalide"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }
    // construction du maillage
    // SMaillageIndex maillage = new SMaillageIndex();
    final EfNode[] pts = new EfNode[nbPt];
    // maillage.points = pts;
    // h2d.maillage = maillage;
    // l'etat d'avancement
    final int[] fmtCoord = version_.getTroisDoubleFormat();
    // Lecture des points
    // Format: <x> <y> <z> (donnees sur 10 colonnes)
    up_.setValue(10, nbPt);
    up_.majProgessionStateOnly(H2dResource.getS("Lecture des noeuds"));
    for (int i = 0; i < nbPt; i++) {
      if (stop_) {
        return null;
      }
      in_.readFields(fmtCoord);
      pts[i] = new EfNode(in_.doubleField(0), in_.doubleField(1), in_.doubleField(2));
      up_.majAvancement();
    }
    return pts;
  }

  /**
   * en fait ne sert a rien.
   */
  @Override
  protected void readDLPN() throws IOException {
    // Lecture du bloc d'entete
    // Format: DLPN <nb_max_degre_de_liberte>
    final int _nbPt = pts_.length;
    in_.readFields(version_.getDLPNFormat());
    up_.majProgessionStateOnly(version_.getDLPN());
    if (!version_.getDLPN().equals(in_.stringField(0))) {
      analyze_.addFatalError(H2dResource.getS("Le bloc degr�s de de libert� n'a pas �t� trouv�"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return;
    }
    // final TIntArrayList idxPts = new TIntArrayList(_nbPt / 2);
    int index = 0;
    final int[] fmt = version_.getCONDNoeudIdxFormat();
    int nbCols = fmt.length;
    // final int idxPtExtrem = version_.getDLPNDllPointExtremite();
    // Format : 16 colonnes de longueur 5.
    // les degre de liberte sont dans l'ordre des points bien sur
    up_.setValue(10, _nbPt);
    while (index < _nbPt) {
      if (stop_) {
        return;
      }
      in_.readFields(fmt);
      if (index + nbCols > _nbPt) {
        nbCols = _nbPt - index;
      }
      // ne sert a rien
      /*
       * for (int i = 0; i < nbCols; i++) { if (in_.intField(i) == idxPtExtrem) { idxPts.add(index + i); } }
       */
      index += nbCols;
      up_.majAvancement();
    }
    // ne sert a rien
    // return idxPts.toNativeArray();
  }

  @Override
  protected boolean readPRELentete() throws IOException {
    in_.readFields(version_.getPRELFormat());
    if (!version_.getPREL().equals(in_.stringField(0))) {
      analyze_.addFatalError(H2dResource.getS("Le bloc PREL est introuvable"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return false;
    }
    nbPropEle_ = in_.intField(3);
    nbGroupElem_ = in_.intField(2);
    return true;
  }

  @Override
  protected boolean readPRND(final H2dRefluxBoundaryConditionMutable[] _clTemp) throws IOException {
    // prop nodale.
    // les deux premieres sont la normales et le debit.
    // le reste sera ajoute dans une proprietes
    // Format: PRND <nb_prop_nodale>
    final String line = in_.getLine().trim();
    if (Integer.parseInt(line.substring(version_.getPRND().length() + 1).trim()) != 2) {
      analyze_.addFatalError(H2dResource
          .getS("Bloc PRND: Seuls 2 propri�t�s seront autoris�es (normales et d�bit lin��que)"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return false;
    }
    // temp1 represente le nombre de points
    final int[] fmt = version_.getCONDNoeudIdxFormat();
    // temp2 le nombre d'indice par ligne
    final int nbCols = fmt.length;
    // Ce chiffre indique le nombre de courbe que doit contenir
    // finalement le fichier pnv.
    // NOTE : les courbes sont stockes dans la liste evols
    // projet_.setNbCLTransitoires(courbeTransitoireCLIndex);
    int indexPn = evolsPN_.size() + 1;
    int indexPRNDEnCours = 0;
    H2dRefluxBoundaryConditionMutable tr = null;
    // Format 16 entiers par ligne
    // tant que tous les points ne sont pas lus...
    boolean errorPRNDAdded = false;
    up_.majProgessionStateOnly(version_.getPRND());
    final int nbPts = pts_.length;
    up_.setValue(nbPts, 10);
    while (indexPRNDEnCours < nbPts) {
      if (stop_) {
        return false;
      }
      in_.readFields(fmt);
      for (int i = 0; i < nbCols && indexPRNDEnCours < nbPts; i++) {
        // le code de la normale
        int index = in_.intField(i);
        // Un point frontiere ... pas clair pour l'instant
        // Affectation de la normale
        if (index != 0) {
          // la condition limite a deja ete initialisee
          tr = _clTemp[indexPRNDEnCours];
          // eh bien non...
          if (tr == null) {
            // alors initialisons
            tr = createDefaultCL();
            tr.setIndexPt(indexPRNDEnCours);
            _clTemp[indexPRNDEnCours] = tr;
          }
        }
        // ce n'est pas un point frontiere.
        else {
          tr = null;
        }
        // le debit lineique
        index = in_.intField(++i);
        // debit non libre
        if (index != 0) {
          // Un point frontiere pour lequel la normale est indefinie :ERREUR
          if (tr == null) {
            analyze_.addWarn(H2dResource.getS("La normale n'est pas d�finie pour le point {0}", CtuluLibString
                .getString(indexPRNDEnCours)), in_.getLineNumber());
            addAnalyzeLigneLue();
          }
          // ok
          else {
            final H2dBcType type = version_.getClType(index);
            tr.setQType(type);
            if (type == H2dBcType.TRANSITOIRE) {
              final EvolutionReguliere evol = new EvolutionReguliere("PN" + (indexPn++));
              evolsPN_.add(evol);
              tr.setQTransitoire(evol);
            } else if (type == null) {
              analyze_.addWarn(H2dResource.getS("Code inconnu") + "(" + index + ")", in_.getLineNumber());
              addAnalyzeLigneLue();
            }
            if (tr.getHType() != H2dBcType.LIBRE && type != H2dBcType.LIBRE) {

              tr.setHTypeFree();
              if (!errorPRNDAdded) {
                errorPRNDAdded = true;
                analyze_.addError(H2dResource
                    .getS("Des conditions limites sont mal sp�cifi�es: hauteur d'eau et d�bit impos�s en m�me temps"),
                    indexPRNDEnCours + 1);
              }
            }
          }
        }
        indexPRNDEnCours++;
        up_.majAvancement();
      }
    }

    return readPRNDDebit(nbPts, _clTemp);
  }

  protected boolean readPRNDDebit(final int _nbPts, final H2dRefluxBoundaryConditionMutable[] _clTemp)
      throws IOException {
    int indexPRNDEnCours = 0;
    // Format: huit colonnes de largeur 10
    final int[] fmt = version_.getHuitDoubleFormat();
    final int nbCols = fmt.length;
    while (indexPRNDEnCours < _nbPts) {
      if (stop_) {
        return false;
      }
      in_.readFields(fmt);
      for (int i = 0; i < nbCols && indexPRNDEnCours < _nbPts; i++) {
        // la normale
        double dTemp = in_.doubleField(i);
        H2dRefluxBoundaryConditionMutable tr = null;
        // Valeur non nulle: a traiter
        if (dTemp != 0) {
          // Une valeur donnee inutilement : erreur ?
          tr = _clTemp[indexPRNDEnCours];
          if (tr == null) {
            analyze_.addWarn(H2dResource.getS("La valeur de la normale ne sera pas prise en compte"), in_
                .getLineNumber());
            addAnalyzeLigneLue();
          } else {
            tr.setNormale(dTemp);
          }
        }
        // le debit
        dTemp = in_.doubleField(++i);
        if (dTemp != 0) {
          if (tr == null) {
            tr = _clTemp[indexPRNDEnCours];
          }
          if (tr == null) {
            analyze_.addWarn(H2dResource.getS("La valeur du d�bit lin��que ne sera pas prise en compte"), in_
                .getLineNumber());
            addAnalyzeLigneLue();
          } else {
            final H2dBcType type = tr.getQType();
            if (type == H2dBcType.TRANSITOIRE) {
              analyze_
                  .addWarn(H2dResource.getS("Le d�bit est de type transitoire. La valeur ne sera pas prise en compte"),
                      in_.getLineNumber());
              addAnalyzeLigneLue();
            } else if (type == H2dBcType.LIBRE) {
              analyze_.addWarn(H2dResource.getS("Le d�bit est de type libre. La valeur ne sera pas prise en compte"),
                  in_.getLineNumber());
              addAnalyzeLigneLue();
            } else {
              tr.setQ(dTemp);
            }
          }
        }
        indexPRNDEnCours++;
      }
    }
    return true;
  }

  @Override
  protected boolean readTPEL() throws IOException {
    final H2dRefluxBordIndexGeneral[] elemBordElt = projet_.getBords();
    in_.readLine();
    final String sTemp = in_.getLine().trim();
    if (!sTemp.startsWith(version_.getTPEL())) {
      analyze_.addFatalError(H2dResource.getS("Le bloc TPEL est introuvable"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return false;
    }
    up_.majProgessionStateOnly(version_.getTPEL());
    // Lecture des props des elements.
    // Format : 16 colonnes de largeur 5.
    final int[] fmt = version_.getCONDNoeudIdxFormat();
    final int nbCols = fmt.length;

    // Si que des elements de fond. On afecte le tableau des elements
    // et on ignore les lignes
    if (nbElemBord_ == 0) {
      // indexEnCours=le nombre de ligne a lire
      // astuce :La division est arrondie � l'entier inferieur
      final int indexEnCoursElt = ((nbElemFond_ - 1) / nbCols) + 1;
      up_.setValue(5, indexEnCoursElt);
      for (int i = 0; i < indexEnCoursElt; i++) {
        in_.readLine();
        up_.majAvancement();
      }
    } else {
      // des elements de bord sont presents...
      if (nbElemBord_ == 0) {
        analyze_.addFatalError(H2dResource.getS("Erreur de lecture des �l�ments. Bords non trouv�s"), in_
            .getLineNumber());
        return false;
      }
      // le nombre de ligne a ignorer: definition des elements de fond
      int indexEnCoursElt = indexDebutBord_ / nbCols;
      // on lit les premiers elements de fond
      up_.setValue(8, indexEnCoursElt, 0, 80);
      for (int i = 0; i < indexEnCoursElt; i++) {
        if (stop_) {
          return false;
        }
        in_.readLine();
        up_.majAvancement();
      }
      // l'index du premier chiffre concerne dans la ligne indexEnCours
      int indexElt = indexDebutBord_ - (indexEnCoursElt * nbCols);
      // l'index dans le tableau des elts de bords.
      indexEnCoursElt = 0;
      // Lecture des types pour les elements de bords.
      while (indexEnCoursElt < nbElemBord_) {
        if (stop_) {
          return false;
        }
        in_.readFields(fmt);
        for (int i = indexElt; i < nbCols && indexEnCoursElt < nbElemBord_; i++) {
          int bordCode = in_.intField(i);
          final H2dBoundaryType bordType = version_.getBordType(bordCode);
          if (bordType != null) {
            if (Fu.DEBUG && FuLog.isDebug()) {
              FuLog.debug("DRE: L3 n�= " + (indexEnCoursElt + 1) + bordType.getName());
            }
          } else {
            analyze_.addFatalError(H2dResource.getS("Code d'�l�ment inconnu") + "(" + bordCode + ")", in_
                .getLineNumber());
            addAnalyzeLigneLue();
            return false;
          }
          // H2dBordMutable bord = elemBord[indexEnCours++];
          // bord.setType(bordType);
          // le nb de points...
          if (elemBordElt == null) {
            return false;
          }
          elemBordElt[indexEnCoursElt++].setBord(bordType);

        }
        // l'indice du premier entier a lire pour les lignes suivantes.
        indexElt = 0;
      }
    }

    projet_.setBords(elemBordElt);
    return true;
  }

}