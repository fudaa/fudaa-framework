/**
 * @creation 21 mars 2003
 * @modification $Date: 2007-06-29 15:10:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.dico.DicoCasFileFormatAbstract;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: INPFileFormat.java,v 1.40 2007-06-29 15:10:23 deniger Exp $
 */
public final class INPFileFormat extends DicoCasFileFormatAbstract {

  private static final INPFileFormat INSTANCE = new INPFileFormat();

  /**
   * @return singleton
   */
  public static INPFileFormat getInstance() {
    return INSTANCE;
  }

  protected INPFileFormat() {
    super("reflux");
    extensions_ = new String[] { "inp" };
    id_ = "REFLUX";
    description_ = H2dResource.getS("Fichier d'entr�e par le logiciel Reflux");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  /**
   * @return inp
   */
  public static String getINPExtension() {
    return "inp";
  }

  public FileFormatVersionInterface getLastVersionInstance(String _id) {
    if (getLastVersionV5P1().equals(_id)) {
      return getLastV5P1Impl();
    }
    return getLastV5P0Impl();
  }

  /**
   * Pour l'instant on renvoie la V5P0 car je ne sais pas si le V5P1 est support�e/ voulue par tout le monde.
   */
  public FileFormatVersionInterface getLastVersionInstance() {
    return getLastV5P0Impl();
  }

  @Override
  public FileFormatVersionInterface getLastVersionInstance(File _f) {
    if (_f != null) {
      CtuluIOOperationSynthese op = getVersionFor(_f, null);
      FileFormatVersionInterface res = (FileFormatVersionInterface) op.getSource();
      if (res == null) return getLastV5P0Impl();
      return res;
    }
    return getLastV5P0Impl();
  }

  @Override
  public String getLastVersion() {
    return "5.0";
  }

  public String getLastVersionV5P1() {
    return "5.1";
  }

  public CtuluIOOperationSynthese getVersionFor(File _f, ProgressionInterface _proj) {
    INPReaderAbstract reader = new INPReaderV5P0();
    reader.setReadOnlyVersion(true);
    CtuluIOOperationSynthese op = reader.read(_f, _proj);
    Object o = op.getSource();
    if (o instanceof Integer) {
      op.setSource((((Integer) o).intValue() == 0) ? getLastV5P0Impl() : getLastV5P1Impl());
    } else
      op.setSource(null);
    return op;

  }

  /**
   * @return la derniere version inp a utiliser
   */
  public RefluxINPV5P1Version getLastV5P1Impl() {
    return new RefluxINPV5P1Version(getLastVersionV5P1());
  }

  public RefluxINPV5P0Version getLastV5P0Impl() {
    return getLastINPVersionImpl();
  }

  /**
   * @return la derniere version
   */
  public RefluxINPV5P0Version getLastINPVersionImpl() {
    return new RefluxINPV5P0Version(getLastVersion());
  }

  @Override
  public int getVersionNb() {
    return 2;
  }

  @Override
  public String[] getVersions() {
    return new String[] { getLastVersionV5P1(), getLastVersion() };
  }
}