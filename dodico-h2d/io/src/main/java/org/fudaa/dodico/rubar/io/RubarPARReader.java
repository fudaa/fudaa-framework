/**
 * @creation 11 juin 2004
 * @modification $Date: 2006-09-19 14:45:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoParams;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: RubarPARReader.java,v 1.10 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarPARReader extends FileOpReadCharSimpleAbstract {
  private static final int INDEX_VISCOSITY_ALONG_X = 25;
  private static final int INDEX_VISCOSITY_ALONG_Y = 26;
  private static final int INDEX_VITESSE_DEPOT = 33;

  public RubarPARReader(/* RubarPARFileFormat.PARVersion _v */) {
  }

  public boolean isIndex(int currentIndex, int searchIdx) {
    if (currentIndex < searchIdx) {
      return false;
    }
    if (currentIndex == searchIdx) {
      return true;
    }
    return (currentIndex - searchIdx) % H2DRubarDicoParams.NB_TRANSPORT_PARAMETERS == 0;
  }

  @Override
  protected Object internalRead() {
    final TIntObjectHashMap map = new TIntObjectHashMap(36);
    final int[] fmt = new int[]{35, 11};
    final int[] fmtViscosityAlongX = new int[]{35, 11, 7, 7, 11};
    final int[] fmtViscosity = new int[]{35, 11, 7, 7};
    final int[] fmtVitesseDepot = new int[]{35, 11, 8, 10};
    try {
      int idx = 0;
      while (true) {
        if (isIndex(idx, INDEX_VISCOSITY_ALONG_X)) {
          in_.readFields(fmtViscosityAlongX);
          String line = in_.getLine();
          String[] values = new String[]{in_.stringField(1).trim(), in_.stringField(2).trim(), in_.stringField(3).trim(), in_.stringField(4).trim()};
          map.put(idx++, values);
        } else if (isIndex(idx, INDEX_VISCOSITY_ALONG_Y)) {
          in_.readFields(fmtViscosity);
          map.put(idx++, new String[]{in_.stringField(1).trim(), in_.stringField(2).trim(), in_.stringField(3).trim()});
        } else if (isIndex(idx, INDEX_VITESSE_DEPOT)) {
          in_.readFields(fmtVitesseDepot);
          map.put(idx++, new String[]{in_.stringField(1).trim(), in_.stringField(2).trim(), in_.stringField(3).trim()});
        }else {
          in_.readFields(fmt);
          map.put(idx++, in_.stringField(1).trim());
        }
      }
    } catch (final EOFException _io) {
    } catch (final IOException _io) {
      analyze_.manageException(_io);
    } catch (final NumberFormatException _if) {
      analyze_.manageException(_if, in_.getLineNumber());
    }
    return map;
  }
}
