/*
 * @creation     19 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

import java.util.ArrayList;
import java.util.List;

/**
 * Container des informations relative � un profil.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MascaretProfilAbstractRepresentation {
  public String nomBief;
  public String nomProfil;
  public List<Double> coordZ=new ArrayList<Double>(); // Bathy ou Topo
  public List<Character> typePts=new ArrayList<Character>(); // B ou T ou S
  // Info sp�cifique 1D
  public double abscLong;
  public List<Double> abscTravers=new ArrayList<Double>();
  // Info sp�cifique 2D
  public List<double[]> ptsTrace=new ArrayList<double[]>();
  public double[] ptAxeHydrau;
  public List<double[]> pts=new ArrayList<double[]>();
  
  /** @return vrai si seul des informations 1d sont pr�sentes. */
  public boolean is1d(){
    return abscTravers.size()>0&&ptsTrace.size()==0&&ptAxeHydrau==null
        &&pts.size()==0;
  }
  
  /** @return vrai si les informations 2d sont pr�sentes. (les infos 1d peuvent �tre �galement pr�sentes) */
  public boolean is2d(){
    return (ptsTrace.size()!=0||ptAxeHydrau!=null&&ptAxeHydrau.length!=0||pts
        .size()!=0); 
  }
  
  /** Check les attributs communs � 1d et 2d. */
  private boolean checkCommon(){
    boolean check=nomBief.length()>0&&nomProfil.length()>0&&coordZ.size()>0&&typePts.size()==coordZ.size();
    int i=-1;
    // Verifi que bathyOuTopo ne comporte que B ou T
    while(check&&++i<typePts.size())
      check=typePts.get(i).equals('T')||typePts.get(i).equals('B')||typePts.get(i).equals('S');
    return check;
  }
  
  /** @return vrai si les informations utile 1D sont compl�tes et coh�rentes. */
  public boolean check1D(){
    boolean check=checkCommon()&&abscLong>=0&&abscTravers.size()==coordZ.size();
    int i=0;
    // Verifi que l'ordre des coordonn�es est croissant.
    while(check&&++i<abscTravers.size())
      check=abscTravers.get(i-1)<=abscTravers.get(i);
    return check;
  }
  
  /** @return vrai si les informations utile 2D sont compl�tes et coh�rentes. */
  public boolean check2D(){
    boolean check=checkCommon()&&ptsTrace.size()>=2&&ptsTrace.size()<=4
        &&ptAxeHydrau.length==2&&pts.size()==coordZ.size();
    // Verification de la taille des coordonn�es XY du profile
    int i=-1;
    while(check&&++i<pts.size())
      check=pts.get(i).length==2;
    return check;
  }
}
