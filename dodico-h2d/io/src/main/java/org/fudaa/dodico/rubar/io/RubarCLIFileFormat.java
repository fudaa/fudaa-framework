/**
 * @creation 8 juin 2004
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuComparator;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarTimeConditionInterface;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.mesure.EvolutionSameComparator;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Fred Deniger
 * @version $Id: RubarCLIFileFormat.java,v 1.13 2007-05-04 13:47:30 deniger Exp $
 */
public final class RubarCLIFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {
  private static RubarCLIFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarCLIFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarCLIFileFormat();
    }
    return instance_;
  }

  /**
   * Initialise toutes les donnees necessaires.
   */
  public RubarCLIFileFormat() {
    super(1);
    extensions_ = new String[]{"cli"};
    super.description_ = H2dResource.getS("Conditions limites");
    super.id_ = "RUBAR_CLI";
    super.nom_ = "Rubar cli";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarCLIReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarCLIWriter();
  }

  @Override
  public CtuluIOOperationSynthese readEvolutions(final File _f, final ProgressionInterface _prog, final Map _options) {
    final CtuluIOOperationSynthese io = read(_f, _prog);
    if (io == null || io.containsError()) {
      return io;
    }
    final H2dRubarTimeConditionInterface[] res = (H2dRubarTimeConditionInterface[]) io.getSource();
    EvolutionReguliereInterface[] cs = null;
    if (res != null) {
      final Set all = new TreeSet(new EvolutionSameComparator());
      int idxHcat = 1;
      int idxH = 1;
      int idxQn = 1;
      int idxQt = 1;
      for (int i = 0; i < res.length; i++) {
        final H2dRubarTimeConditionInterface ti = res[i];
        if (ti == null) {
          continue;
        }
        int nbBlocks = ti.getNbConcentrationBlock();
        for (int idxBlock = 0; idxBlock < nbBlocks; idxBlock++) {
          final EvolutionReguliere concentrationEvolution = ti.getConcentrationEvolution(idxBlock);
          if (concentrationEvolution != null && all.add(concentrationEvolution)) {
            concentrationEvolution.setNom(H2dVariableTransType.CONCENTRATION + CtuluLibString.getEspaceString(idxHcat++));
            concentrationEvolution.getInfos().put(EvolutionReguliere.INFO_TYPE, H2dVariableTransType.CONCENTRATION);
          }
          final EvolutionReguliere diametreEvolution = ti.getDiametreEvolution(idxBlock);
          if (diametreEvolution != null && all.add(diametreEvolution)) {
            diametreEvolution.setNom(H2dVariableTransType.DIAMETRE + CtuluLibString.getEspaceString(idxHcat++));
            diametreEvolution.getInfos().put(EvolutionReguliere.INFO_TYPE, H2dVariableTransType.DIAMETRE);
          }
          final EvolutionReguliere etendueEvolution = ti.getEtendueEvolution(idxBlock);
          if (etendueEvolution != null && all.add(etendueEvolution)) {
            etendueEvolution.setNom(H2dVariableTransType.ETENDUE + CtuluLibString.getEspaceString(idxHcat++));
            etendueEvolution.getInfos().put(EvolutionReguliere.INFO_TYPE, H2dVariableTransType.ETENDUE);
          }
        }
        final EvolutionReguliere h = ti.getHEvol();
        if (h != null && all.add(h)) {
          h.setNom(H2dVariableType.HAUTEUR_EAU + CtuluLibString.getEspaceString(idxH++));
          h.getInfos().put(EvolutionReguliere.INFO_TYPE, H2dVariableTransType.COTE_EAU);
        }
        final EvolutionReguliere qnEvol = ti.getQnEvol();
        if (qnEvol != null && all.add(qnEvol)) {
          qnEvol.setNom(H2dVariableType.DEBIT_NORMAL + CtuluLibString.getEspaceString(idxQn++));
          qnEvol.getInfos().put(EvolutionReguliere.INFO_TYPE, H2dVariableTransType.DEBIT_NORMAL);
        }
        final EvolutionReguliere qtEvol = ti.getQtEvol();
        if (qtEvol != null && all.add(qtEvol)) {
          qtEvol.setNom(H2dVariableType.DEBIT_TANGENTIEL + CtuluLibString.getEspaceString(idxQt++));
          qtEvol.getInfos().put(EvolutionReguliere.INFO_TYPE, H2dVariableTransType.DEBIT_TANGENTIEL);
        }
      }
      cs = (EvolutionReguliereInterface[]) all.toArray(new EvolutionReguliereInterface[all.size()]);
      if (cs != null) {
        Arrays.sort(cs, FuComparator.STRING_COMPARATOR);
      }
    }
    io.setSource(cs);
    return io;
  }
}
