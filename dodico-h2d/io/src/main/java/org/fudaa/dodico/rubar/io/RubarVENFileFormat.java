package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * 
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarVENFileFormat extends FileFormatUnique {
  /**
   * Initialise les donnees.
   */
  public RubarVENFileFormat() {
    super(1);
    extensions_ = new String[] { "ven"};
    super.description_ = H2dResource.getS("Chronique de vents par maille");
    super.id_ = "RUBAR_VEN";
    super.nom_ = "Rubar ven";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader(){
    return new RubarVENReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter(){
    return new RubarVENWriter();
  }

  /**
   * @param f
   * @param prog
   * @param nbElt
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese read(final File f,final ProgressionInterface prog,final int nbElt){
    final RubarVENReader i = new RubarVENReader();
    i.setNbElt(nbElt);
    final CtuluIOOperationSynthese r = i.read(f, prog);
    i.setProgressReceiver(null);
    return r;
  }
}