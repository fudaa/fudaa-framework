/**
 * @creation 16 nov. 2004
 * @modification $Date: 2007-03-27 16:09:59 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.fudaa.ctulu.CtuluLibArray;

import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: TelemacVariableMapper.java,v 1.12 2007-03-27 16:09:59 deniger Exp $
 */
public class TelemacVariableMapper {

  public static boolean isVitesse(final String _name) {
    return "VELOCITY U".equals(_name) || "VITESSE U".equals(_name) || "VITESSE W".equals(_name)
        || "VITESSE W".equals(_name) || "VELOCITY V".equals(_name) || "VITESSE V".equals(_name)
        || "CELERITY".equals(_name) || "CELERITE".equals(_name) || "SCALAR VELOCITY".equals(_name)
        || "VITESSE SCALAIRE".equals(_name) || "HIGHEST VELOCITY".equals(_name) || "VITESSE MAXIMUM".equals(_name);
  }

  public static boolean isWind(final String _name) {
    return ("WIND ALONG X".equals(_name) || "VENT X".equals(_name))
        || ("WIND ALONG Y".equals(_name) || "VENT Y".equals(_name));
  }

  public static boolean isFlowrate(final String _name) {
    return "FLOWRATE ALONG X".equals(_name) || "DEBIT SUIVANT X".equals(_name) || "FLOWRATE ALONG Y".equals(_name)
        || "DEBIT SUIVANT Y".equals(_name) || "SCALAR FLOWRATE".equals(_name) || "DEBIT SCALAIRE".equals(_name);
  }

  /**
   * @param _name le nom de la variable
   * @return l'unit� de la variable defini par telemac
   */
  public static String getUnits(final String _name) {
    if (isVitesse(_name) || isWind(_name)) {
      return "M/S";
    }
    if (isFlowrate(_name) || "VISCOSITY".equals(_name) || "VISCOSITE TURB.".equals(_name)) {
      return "M2/S";
    }
    if (isWaterDepth(_name) || isFreeSurface(_name) || isDrift(_name) || isBottom(_name) || isCoteMax(_name)) {
      return "M";
    }
    if (isFroud(_name)) {
      return null;
    }
    if (isTracer(_name)) {
      return null;
    }
    if (isTurb(_name)) {
      return "JOULE/KG";
    }
    if (isDissip(_name)) {
      return "WATT/KG";
    }

    if (isPress(_name)) {
      return "PASCAL";
    }
    if (isFric(_name)) {
      return null;
    }
    if (isCourant(_name)) {
      return null;
    }
    if (_name.startsWith("VARIABLE ")) {
      return "UNIT   ??";
    }
    if (isTime(_name)) {
      return "S";
    }
    return null;
  }

  private static boolean isCoteMax(final String _name) {
    return "HIGH WATER MARK".equals(_name) || "COTE MAXIMUM".equals(_name);
  }

  private static boolean isTime(final String _name) {
    return "HIGH WATER TIME".equals(_name) || "TEMPS COTE MAXI".equals(_name) || "TIME OF HIGH VEL".equals(_name)
        || "T VITESSE MAXI".equals(_name);
  }

  private static boolean isCourant(final String _name) {
    return "COURANT NUMBER".equals(_name) || "NBRE DE COURANT".equals(_name);
  }

  private static boolean isDrift(final String _name) {
    return "DRIFT ALONG X".equals(_name) || "DERIVE EN X".equals(_name) || "DRIFT ALONG Y".equals(_name)
        || "DERIVE EN Y".equals(_name);
  }

  private static boolean isFric(final String _name) {
    return "BOTTOM FRICTION".equals(_name) || "FROTTEMENT".equals(_name);
  }

  private static boolean isPress(final String _name) {
    return "AIR PRESSURE".equals(_name) || "PRESSION ATMOS.".equals(_name);
  }

  private static boolean isDissip(final String _name) {
    return "DISSIPATION".equals(_name) || "DISSIPATION".equals(_name);
  }

  private static boolean isTurb(final String _name) {
    return "TURBULENT ENERG.".equals(_name) || "ENERGIE TURBUL.".equals(_name);
  }

  private static boolean isTracer(final String _name) {
    return "TRACER".equals(_name) || "TRACEUR".equals(_name);
  }

  private static boolean isFroud(final String _name) {
    return "FROUDE NUMBER".equals(_name) || "FROUDE".equals(_name);
  }

  private static boolean isBottom(final String _name) {
    return "BOTTOM".equals(_name) || "FOND".equals(_name) || is3DBottom(_name);
  }

  public static boolean is3DBottom(final String _name) {
    return "COTE Z".equals(_name) || "ELEVATION Z".equals(_name);
  }

  private static boolean isFreeSurface(final String _name) {
    return "FREE SURFACE".equals(_name) || "SURFACE LIBRE".equals(_name);
  }

  private static boolean isWaterDepth(final String _name) {
    return "WATER DEPTH".equals(_name) || "HAUTEUR D''EAU".equals(_name);
  }

  /**
   * @return la table [var_fr,var_en]->H2dVariable
   */
  public static Map getVarNameFrEn() {
    final Map r = new HashMap();
    r.put(H2dVariableType.VITESSE_U, new String[] { "VITESSE U", "VELOCITY U" });
    r.put(H2dVariableType.VITESSE_V, new String[] { "VITESSE V", "VELOCITY V" });
    r.put(H2dVariableType.VITESSE_W, new String[] { "VITESSE W", "VELOCITY W" });
    r.put(H2dVariableType.HAUTEUR_EAU, new String[] { "HAUTEUR D'EAU", "WATER DEPTH" });
    r.put(H2dVariableType.COTE_EAU, new String[] { "SURFACE LIBRE", "FREE SURFACE" });
    r.put(H2dVariableType.BATHYMETRIE, new String[] { "FOND", "BOTTOM" });
    r.put(H2dVariableType.COEF_FROTTEMENT_FOND, new String[] { "FROTTEMENT", "BOTTOM FRICTION" });
    r.put(H2dVariableType.VISCOSITE, new String[] { "VISCOSITE TURB.", "VISCOSITY" });
    r.put(H2dVariableType.VITESSE, new String[] { "VITESSE SCALAIRE", "SCALAR VELOCITY" });
    r.put(H2dVariableType.DEBIT, new String[] { "DEBIT SCALAIRE", "SCALAR FLOWRATE" });
    r.put(H2dVariableType.DEBIT_X, new String[] { "DEBIT SUIVANT X", "FLOWRATE ALONG X" });
    r.put(H2dVariableType.DEBIT_Y, new String[] { "DEBIT SUIVANT Y", "FLOWRATE ALONG Y" });
    return r;
  }

  Map nameVar_;

  Map varName_;

  public TelemacVariableMapper() {
    varName_ = getVarNameFrEn();
    nameVar_ = new HashMap(varName_.size() * 2);
    for (final Iterator it = varName_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final String[] n = (String[]) e.getValue();
      nameVar_.put(n[0], e.getKey());
      nameVar_.put(n[1], e.getKey());
    }
  }

  /**
   * @param _t la variable
   * @param _lg l'indice du langage
   * @return le nom connu
   */
  public String getSerafinKnownVar(final Object _t, final int _lg) {
    final String[] r = (String[]) varName_.get(_t);
    if (r != null) {
      return r[_lg];
    }
    return null;
  }

  /**
   * @param _name le nom a chercher
   * @return la variable connue ou null si aucune
   */
  public H2dVariableType getUsedKnownVar(final String _name) {
    final Object r = nameVar_.get(_name);
    if (r != null) {
      return (H2dVariableType) r;
    }
    if (CtuluLibArray.findObject(SerafinFileFormat.getCommonVariableFond(), _name) >= 0) {
      return H2dVariableType.BATHYMETRIE;
    }
    return null;
  }

}