/**
 * @creation 22 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageI;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: RubarOuvrage.java,v 1.6 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarOuvrage implements H2dRubarOuvrageI {
  boolean noArete2_;
  H2dRubarOuvrageElementaireInterface[] ouvElement_;
  int rubarRef_;
  double xa1_, ya1_;
  double xa2_, ya2_;
  double xe1_, ye1_;
  double xe2_, ye2_;
  double[] xEltIntern_;
  double[] yEltIntern_;

  @Override
  public int getNbOuvrageElementaires() {
    return ouvElement_ == null ? 0 : ouvElement_.length;
  }

  @Override
  public boolean containsOuvrageElementaire() {
    return getNbOuvrageElementaires() > 0;
  }

  @Override
  public H2dRubarOuvrageRef getOuvrageRef() {
    if (rubarRef_ == -1) {
      return H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT;
    }
    return H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT;
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {
    if (ouvElement_ != null) {
      for (H2dRubarOuvrageElementaireInterface element : ouvElement_) {
        element.fillWithTarageEvolution("", allTarage);
      }
    }
  }

  @Override
  public boolean isElt2Set() {
    return !noArete2_;
  }

  @Override
  public final int getRubarRef() {
    return rubarRef_;
  }

  @Override
  public final double getXaAmont() {
    return xa1_;
  }

  @Override
  public final double getXaAval() {
    return xa2_;
  }

  @Override
  public final double getXElementAmont() {
    return xe1_;
  }

  @Override
  public final double getXElementAval() {
    return xe2_;
  }

  @Override
  public int getNbMailleIntern() {
    return xEltIntern_ == null ? 0 : xEltIntern_.length;
  }

  @Override
  public double getXMailleIntern(final int _i) {
    return xEltIntern_[_i];
  }

  @Override
  public final double getYAmont() {
    return ya1_;
  }

  @Override
  public final double getYAval() {
    return ya2_;
  }

  @Override
  public final double getYElementAmont() {
    return ye1_;
  }

  @Override
  public final double getYELementAval() {
    return ye2_;
  }

  @Override
  public double getYMailleIntern(final int _i) {
    return yEltIntern_[_i];
  }

  public final boolean isNoArete2() {
    return noArete2_;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getOuvrageElementaire(final int _i) {
    return ouvElement_[_i];
  }
}
