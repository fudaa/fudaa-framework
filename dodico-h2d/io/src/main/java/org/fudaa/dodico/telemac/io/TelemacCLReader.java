/**
 * @file TelemacCIReader.java
 * @creation 14 mars 2003
 * @modification $Date: 2006-11-14 09:05:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.telemac.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;

/**
 * @author deniger
 * @version $Id: TelemacCLReader.java,v 1.32 2006-11-14 09:05:28 deniger Exp $
 */
public class TelemacCLReader extends FileOpReadCharSimpleAbstract {

  int nbPoint_;
  TelemacCLVersion version_;

  /**
   * @param _f le version
   */
  public TelemacCLReader(final TelemacCLVersion _f) {
    version_ = _f;
  }

  /**
   * @param _i le nombre de point attendu.
   */
  public void setNBPointExpected(final int _i) {
    nbPoint_ = _i;
  }
  
  boolean readDataBrut;
  
  
  

  /**
   * @return true  si on lit les donnees brutes sans traitement. Le reader renverra des List<TelemacCLLine>
   */
  public boolean isReadDataBrut() {
    return readDataBrut;
  }

  public void setReadDataBrut(boolean readDataBrut) {
    this.readDataBrut = readDataBrut;
  }

  /**
   * @see org.fudaa.dodico.tr.TrReaderAbstract#read()
   */
  private List<TelemacCLLine> readCLBrutes() {
    if (in_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }
    in_.setJumpBlankLine(true);
    List<TelemacCLLine> l = new ArrayList<TelemacCLLine>(nbPoint_ == 0 ? 50 : nbPoint_);
    int index = 0;
    int nbField;

    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbPoint_);
    up.majProgessionStateOnly();
    try {
      while (true) {
        in_.readFields();
        final TelemacCLLine cl = new TelemacCLLine();
        nbField = in_.getNumberOfFields();
        if (nbField < 13) {
          analyze_.addFatalError(H2dResource.getS("Ligne non valide: elle doit contenir 13 champs"), in_
              .getLineNumber());
        }
        if (nbField == 14) {
          analyze_.addInfo(H2dResource.getS("Maillage cart�sien non support�"), in_.getLineNumber());
        }
        cl.lihbor_ = in_.intField(0);
        cl.liubor_ = in_.intField(1);
        cl.livbor_ = in_.intField(2);
        cl.hbor_ = in_.doubleField(3);
        cl.ubor_ = in_.doubleField(4);
        cl.vbor_ = in_.doubleField(5);
        cl.aubor_ = in_.doubleField(6);
        cl.litbor_ = in_.intField(7);
        cl.tbor_ = in_.doubleField(8);
        cl.atbor_ = in_.doubleField(9);
        cl.btbor_ = in_.doubleField(10);
        cl.n_ = in_.intField(11) - 1;
        cl.k_ = in_.intField(12) - 1;
        if (cl.k_ != index) {
          analyze_.addWarn(H2dResource.getS("L'index lu est invalide ( lu: {0} attendu: {1})", CtuluLibString
              .getString(cl.k_), CtuluLibString.getString(index)), in_.getLineNumber());
          // return null;
        }
        l.add(cl);
        index++;
        up.majAvancement();
      }
    } catch (final EOFException _oef) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Telemac boundary file read end");
      }
    } catch (final NumberFormatException _ex) {
      analyze_.manageException(_ex, in_.getLineNumber());
      return null;
    } catch (final IOException _ex) {
      analyze_.manageException(_ex);
      return null;
    }
    return l;
  }

  /**
   * @see org.fudaa.dodico.tr.TrReaderAbstract#read()
   */
  private H2dTelemacCLSourceInterface readCL() {
    TelemacCLAdapter inter = null;
    if (in_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }
    in_.setJumpBlankLine(true);
    List l = null;
    l = new ArrayList(nbPoint_ == 0 ? 50 : nbPoint_);
    int index = 0;
    int nbField;
    final TelemacCLLine cl = new TelemacCLLine();
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbPoint_);
    up.majProgessionStateOnly();
    try {
      while (true) {
        in_.readFields();
        nbField = in_.getNumberOfFields();
        if (nbField < 13) {
          analyze_.addFatalError(H2dResource.getS("Ligne non valide: elle doit contenir 13 champs"), in_
              .getLineNumber());
        }
        if (nbField == 14) {
          analyze_.addInfo(H2dResource.getS("Maillage cart�sien non support�"), in_.getLineNumber());
        }
        cl.lihbor_ = in_.intField(0);
        if (!version_.isCodeHauteur(cl.lihbor_)) {
          analyze_.addError(H2dResource.getS(H2dResource.getS("Code inconnu pour la hauteur")) + CtuluLibString.ESPACE
              + cl.lihbor_, in_.getLineNumber());
          return null;
        }
        cl.liubor_ = in_.intField(1);
        if (!version_.isCodeVitesse(cl.liubor_)) {
          analyze_.addError(H2dResource.getS("Code inconnu pour la vitesse u") + CtuluLibString.ESPACE + cl.liubor_,
              in_.getLineNumber());
          return null;
        }
        cl.livbor_ = in_.intField(2);
        if (!version_.isCodeVitesse(cl.livbor_)) {
          analyze_.addError(H2dResource.getS("Code inconnu pour la vitesse v") + CtuluLibString.ESPACE + cl.livbor_,
              in_.getLineNumber());
          return null;
        }
        cl.hbor_ = in_.doubleField(3);
        cl.ubor_ = in_.doubleField(4);
        cl.vbor_ = in_.doubleField(5);
        cl.aubor_ = in_.doubleField(6);
        cl.litbor_ = in_.intField(7);
        if (!version_.isCodeTraceur(cl.litbor_)) {
          analyze_.addWarn(H2dResource.getS("Code inconnu pour le traceur") + CtuluLibString.ESPACE + cl.litbor_, in_
              .getLineNumber());
          // return null;
        }
        cl.tbor_ = in_.doubleField(8);
        cl.atbor_ = in_.doubleField(9);
        cl.btbor_ = in_.doubleField(10);
        cl.n_ = in_.intField(11) - 1;
        cl.k_ = in_.intField(12) - 1;
        if (cl.k_ != index) {
          analyze_.addWarn(H2dResource.getS("L'index lu est invalide ( lu: {0} attendu: {1})", CtuluLibString
              .getString(cl.k_), CtuluLibString.getString(index)), in_.getLineNumber());
          // return null;
        }
        final H2dTelemacCLElementSource con = new H2dTelemacCLElementSource();
        boolean b = getStructure(analyze_, con, cl, index);
        if (!b) { return null; }
        l.add(con);
        index++;
        up.majAvancement();
      }
    } catch (final EOFException _oef) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Telemac boundary file read end");
      }
    } catch (final NumberFormatException _ex) {
      analyze_.manageException(_ex, in_.getLineNumber());
      return null;
    } catch (final IOException _ex) {
      analyze_.manageException(_ex);
      return null;
    }
    final int n = l.size();
    if (n > 0) {
      final H2dTelemacCLElementSource[] lines = new H2dTelemacCLElementSource[n];
      l.toArray(lines);
      inter = new TelemacCLAdapter(lines);
    }
    return inter;
  }

  /**
   * @param _r le receveur des messages
   * @param _target le structure a modifier
   * @param _l la source
   * @param _indicesFrontieres l'indice du point sur la frontiere
   * @return true si ok.
   */
  public boolean getStructure(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target, final TelemacCLLine _l,
      final int _indicesFrontieres) {
    return version_.getH2dDataFromFile(_r, _target, _l, _indicesFrontieres);
  }

  @Override
  protected Object internalRead() {
    if(readDataBrut) return readCLBrutes();
    return readCL();
  }

}