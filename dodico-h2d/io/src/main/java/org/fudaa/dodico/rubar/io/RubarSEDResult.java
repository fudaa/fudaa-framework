/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCoucheContainer;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarSEDResult implements H2dRubarSedimentInterface {
  double[] time;
  H2dRubarSedimentCoucheContainer[][] values;


  @Override
  public int getNbTimes() {
    return this.time == null ? 0 : this.time.length;
  }

  @Override
  public int getNbNoeuds() {
    return ((this.values == null) || (this.values.length == 0)) ? 0 : this.values[0].length;
  }

  @Override
  public double getTimes(int idx) {
    return this.time[idx];
  }

  @Override
  public H2dRubarSedimentCoucheContainer getCoucheContainer(int idxTime, int idxNoeud) {
    return this.values[idxTime][idxNoeud];
  }

//  public double getDiamn(int idxTime, int idxNoeud) {
//    return this.diamns[idxTime][idxNoeud];
//  }
//
//  public double getTaummn(int idxTime, int idxNoeud) {
//    return this.taummns[idxTime][idxNoeud];
//  }
//
//  public double getSigman(int idxTime, int idxNoeud) {
//    return this.sigmans[idxTime][idxNoeud];
//  }
}
