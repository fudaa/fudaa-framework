/*
 * @creation 18 juin 07
 * @modification $Date: 2007-06-20 12:21:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.refonde.io;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: RefondeINPResult.java,v 1.1 2007-06-20 12:21:50 deniger Exp $
 */
public interface RefondeINPResult {

  EfGridInterface getGrid();

  int getNombrePeriodeHoule();

  int getNombreAngleHoule();

  boolean isModuleSeiche();

  boolean isSpecUnknown();

  H2dVariableType getVariableForFirstCol();

}
