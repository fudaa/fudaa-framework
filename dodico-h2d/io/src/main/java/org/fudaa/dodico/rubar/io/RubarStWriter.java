/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelFilterAdapter;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;

/**
 * Une classe pour ecrire les donn�es Rubar St. Si le mod�le poss�de un attribut TITRE, alors il est �crit comme
 * nom du profil.
 * 
 * @author fred deniger, Bertrand Marchand
 * @version $Id$
 */
public class RubarStWriter extends FileOpWriterCharSimpleAbstract {

  /** Pour chaque profil, association entre l'indice de certains points et des indices de lignes directrices.*/
  HashMap<Integer,ArrayList<Integer>>[] pts2lds_=null;
  /** Le mod�le de profils, une fois r�ordonn�, ou identique si non r�ordonn�. */
  GISDataModel profs_=null;
  /** Le nom des lignes directrices, une fois control�es et modifi�es. */
  List<String> ldirnames_=new ArrayList<String>();
  /** Le fichier est de type ST */
  boolean isSt_;

  @Override
  public void setFile(final File _f) {
    isSt_ = !_f.getName().toLowerCase().endsWith(".cn");
    super.setFile(_f);
  }
  
  /**
   * Ecrit les profils et les lignes directrices.
   * param _o Un tableau GISDataModel[2]. 
   * [0] : le modele des profils (ou null si aucun profil), 
   * [1] : le modele de lignes directrices (ou null ou de longueur 0 si aucune ligne directrice).
   */
  @Override
  protected void internalWrite(final Object _o) {
    ProgressionUpdater up=new ProgressionUpdater(progress_);
    
    if (!(_o instanceof GISDataModel[]) || ((GISDataModel[])_o).length!=2) {
      donneesInvalides(_o);
      return;
    }
    
    profs_=((GISDataModel[])_o)[0];
    GISDataModel lines=((GISDataModel[])_o)[1];

    // Profils ou non ?
    boolean bprofs=profs_!=null && profs_.getNumGeometries()!=0;
    // Lignes directrices ou non ?
    boolean bldirs=lines!=null && lines.getNumGeometries()!=0;
    
    if (!bprofs) {
      if (bldirs) {
        analyze_.addInfo(DodicoLib.getS("Aucun profil : Les lignes directrices ne seront pas export�es"));
      }
      else {
        analyze_.addInfo(DodicoLib.getS("Aucun profil � exporter"));
      }
      return;
    }
    
    // Si pb avec les lignes directrices => Pas d'�criture de fichier.
    if (!prepareWrite((GISDataModel[])_o,up)) return;
        
    final int attName=profs_.getIndiceOf(GISAttributeConstants.TITRE);
    final int attPk=profs_.getIndiceOf(GISAttributeConstants.COMMENTAIRE_HYDRO);
    
    final int[] fmtEntete = new int[] { 6, 6, 6, 6, 1, 12, 1, 42 };
    final int[] fmt = new int[] { 1, 12, 1, 12, 1, 12, 1, 3 };
    final FortranWriter writer = new FortranWriter(out_);
    writer.setSpaceBefore(true);
    final CtuluNumberFormatI numbFmt = FortranLib.getImprovedFortranFormat(12, 4);
    final String sep = "999.999";

    try {
      int nb=profs_.getNumGeometries();
      up.setValue(50, nb, 50, 50);

      for (int i = 0; i <nb ; i++) {
        HashMap<Integer,ArrayList<Integer>> pts2ld=bldirs?pts2lds_[i]:null;
        String name;
        double pk=0;
        final CoordinateSequence str = ((LineString) profs_.getGeometry(i)).getCoordinateSequence();
        if (isSt_) {

          if (attName==-1||(name=(String)profs_.getValue(attName, i))==null) name="";
          name=name.substring(0,Math.min(name.length(),42));
          // PK, issu du commentaire hydrualique.
          if (attPk!=-1) {
            pk=GISLib.getHydroCommentDouble((String)profs_.getValue(attPk, i),GISAttributeConstants.ATT_COMM_HYDRO_PK);
          }
          // Nombre de points => On tient compte des points dupliqu�s car partag�s par plusieurs lignes directrices.
          int nbpt=0;
          for (int k=0; k<str.size(); k++) {
            ArrayList<Integer> idxline=bldirs ? pts2ld.get(k):null;
            if (idxline!=null)
              nbpt+=idxline.size();
            else
              nbpt+=1;
          }
          
          writer.setSpaceBefore(false);
          writer.stringField(7, name);
          writer.stringField(6, "");
          writer.stringField(5, numbFmt.format(pk));
          writer.stringField(4, "");
          writer.intField(3, nbpt);
          writer.intField(2, 0);
          writer.intField(1, 0);
          writer.intField(0, i+1);
          writer.writeFields(fmtEntete);
          writer.setSpaceBefore(true);
        }
        for (int k = 0; k < str.size(); k++) {
          // Un m�me point de profil peut �tre partag� par plusieurs lignes directrices => On duplique ce point.
          ArrayList<Integer> idxline=bldirs?pts2ld.get(k):null;
          if (idxline!=null) {
            for (int il=0; il<idxline.size(); il++) {
              name=ldirnames_.get(idxline.get(il));
              writer.setSpaceBefore(false);
              writer.stringField(7, name);
              writer.setSpaceBefore(true);
              writer.stringField(6, "");
              writer.stringField(5, numbFmt.format(str.getOrdinate(k, 2)));
              writer.stringField(4, "");
              writer.stringField(3, numbFmt.format(str.getOrdinate(k, 1)));
              writer.stringField(2, "");
              writer.stringField(1, numbFmt.format(str.getOrdinate(k, 0)));
              writer.stringField(0, "");
              writer.writeFields(fmt);
            }
          }
          else {
            writer.stringField(5, numbFmt.format(str.getOrdinate(k, 2)));
            writer.stringField(4, "");
            writer.stringField(3, numbFmt.format(str.getOrdinate(k, 1)));
            writer.stringField(2, "");
            writer.stringField(1, numbFmt.format(str.getOrdinate(k, 0)));
            writer.stringField(0, "");
            writer.writeFields(fmt);
          }
        }
        writer.stringField(5, sep);
        writer.stringField(4, "");
        writer.stringField(3, sep);
        writer.stringField(2, "");
        writer.stringField(1, sep);
        writer.stringField(0, "");
        writer.writeFields(fmt);
        
        up.majAvancement();
      }

      int nbprofs=profs_.getNumGeometries();
      if (isSt_) {
        analyze_.addInfo(DodicoLib.getS("{0} profils sur {1} ont �t� export�s", ""+nbprofs, ""+nbprofs), -1);
      }
      else {
        analyze_.addInfo(DodicoLib.getS("{0} lignes de niveaux sur {1} ont �t� export�es", ""+nbprofs, ""+nbprofs), -1);
      }
      if (bldirs) {
        int nbldirs=lines.getNumGeometries();
        analyze_.addInfo(DodicoLib.getS("{0} lignes directrices sur {1} ont �t� export�es", ""+nbldirs, ""+nbldirs), -1);
      }
    } 
    catch (final IOException _evt) {
      analyze_.manageException(_evt);
    }
  }
  
  /**
   * Controle que les lignes directrices et profils d�finissent bien une trame correcte. R�ordonne les profils et les 
   * lignes directrices. D�finit les liens entre les points des profils et des lignes directrices eventuelles.<p>
   * 
   * @param _mdls Les mod�les de lignes directrices et profils.
   * @param _up L'updater pour la progression
   * @return true si ok.
   */
  private boolean prepareWrite(GISDataModel[] _mdls, ProgressionUpdater _up) {
    GISDataModel ldirs=_mdls[1];
    
    pts2lds_=null;
    if (ldirs==null || ldirs.getNumGeometries()==0) return true; // On ne peut rien controler sans lignes directrices.
    
    // Les lignes directrices doivent avoir m�me nombre de points.
    int nbpts=-1;
    for (int i=0; i<ldirs.getNumGeometries(); i++) {
      if (nbpts==-1) {
        nbpts=ldirs.getGeometry(i).getNumPoints();
      }
      else if (ldirs.getGeometry(i).getNumPoints()!=nbpts) {
        analyze_.addError(CtuluLib.getS("Les lignes directrices doivent avoir le m�me nombre de points"), 0);
        break;
      }
    }
    // Le nombre de profils doit �tre �gal au nombre de points des lignes directrices.
    if (nbpts!=profs_.getNumGeometries()) {
      analyze_.addError(CtuluLib.getS("Le nombre de points sur les lignes directrices doit �tre �gal au nombre de profils"), 0);
    }
    if (analyze_.containsErrors()) return false;

    int attNameLines=ldirs.getIndiceOf(GISAttributeConstants.TITRE);
    
    List<Integer> ordProfs=new ArrayList<Integer>(profs_.getNumGeometries());
    CoordinateSequence firstLine=((LineString)ldirs.getGeometry(0)).getCoordinateSequence();
    _up.setValue(50, firstLine.size(), 0, 30);

    // On r�ordonne les profils suivant la 1ere ligne directrice.
    NEXT_LD_COORD:
    for (int i=0; i<firstLine.size(); i++) {
      double x1=firstLine.getOrdinate(i,CoordinateSequence.X);
      double y1=firstLine.getOrdinate(i,CoordinateSequence.Y);
      double z1=firstLine.getOrdinate(i,CoordinateSequence.Z);
      for (int j=0; j<profs_.getNumGeometries(); j++) {
        if (ordProfs.contains(j)) continue;
        CoordinateSequence seq=((LineString)profs_.getGeometry(j)).getCoordinateSequence();
        for (int k=0; k<seq.size(); k++) {
          double x2=seq.getOrdinate(k,CoordinateSequence.X);
          double y2=seq.getOrdinate(k,CoordinateSequence.Y);
          double z2=seq.getOrdinate(k,CoordinateSequence.Z);
          if (CtuluLibGeometrie.getDistance3D(x1,y1,z1,x2,y2,z2)<1.E-6) {
            ordProfs.add(j);
            _up.majAvancement();
            continue NEXT_LD_COORD;
          }
        }
      }
      String name;
      if (attNameLines==-1) name=""+0;
      else                  name=(String)ldirs.getValue(attNameLines,0);
      analyze_.addError(
          CtuluLib.getS("La ligne directrice {0} n'est pas connect�e � un profil au sommet {1}",name,""+(i+1)), 0);
      return false;
    }
    
    int[] iprofs=new int[ordProfs.size()];
    for (int i=0; i<iprofs.length; i++) iprofs[i]=ordProfs.get(i).intValue();
    profs_=GISDataModelFilterAdapter.buildAdapter(profs_, null, iprofs);
    
    _up.setValue(50, profs_.getNumGeometries(), 30, 20);

    // Les profils sont r�ordonn�s => On associe un index de ligne directrice aux points qui en contiennent.
    pts2lds_=new HashMap[profs_.getNumGeometries()];
    for (int i=0; i<profs_.getNumGeometries(); i++) {
      pts2lds_[i]=new HashMap<Integer,ArrayList<Integer>>();
      CoordinateSequence seqprof=((LineString)profs_.getGeometry(i)).getCoordinateSequence();
      
      NEXT_LD:
      for (int j=0; j<ldirs.getNumGeometries(); j++) {
        CoordinateSequence seqline=((LineString)ldirs.getGeometry(j)).getCoordinateSequence();
        double x1=seqline.getOrdinate(i,CoordinateSequence.X);
        double y1=seqline.getOrdinate(i,CoordinateSequence.Y);
        double z1=seqline.getOrdinate(i,CoordinateSequence.Z);

        for (int k=0; k<seqprof.size(); k++) {
          double x2=seqprof.getOrdinate(k,CoordinateSequence.X);
          double y2=seqprof.getOrdinate(k,CoordinateSequence.Y);
          double z2=seqprof.getOrdinate(k,CoordinateSequence.Z);
          if (CtuluLibGeometrie.getDistance3D(x1,y1,z1,x2,y2,z2)<1.E-6) {
            ArrayList<Integer> idxlines=pts2lds_[i].get(k);
            // Pas encore de ligne directrice associ�.
            if (idxlines==null) {
              idxlines=new ArrayList<Integer>();
              pts2lds_[i].put(k,idxlines);
            }
            // Si une ligne directrice au moins est d�j� associ�e au pt courant et que le pt d'apr�s a m�mes 
            // coordonn�es, on passe le pt courant. Ceci dans le but d'eviter la duplication de points.
            else if (k<seqprof.size()-1) {
              x2=seqprof.getOrdinate(k+1,CoordinateSequence.X);
              y2=seqprof.getOrdinate(k+1,CoordinateSequence.Y);
              z2=seqprof.getOrdinate(k+1,CoordinateSequence.Z);
              if (CtuluLibGeometrie.getDistance3D(x1,y1,z1,x2,y2,z2)<1.E-6) {
                continue;
              }
            }
            idxlines.add(j);
            _up.majAvancement();
            continue NEXT_LD;
          }
        }
        String name;
        if (attNameLines==-1) name=""+j;
        else                  name=(String)ldirs.getValue(attNameLines,j);
        analyze_.addError(
            CtuluLib.getS("La ligne directrice {0} n'est pas connect�e � un profil au sommet {1}",name,""+(i+1)), 0);
        return false;
      }
    }
    
    // Suppression des noms de lignes directrices dont le nom est sup�rieur � 3 caract�res ou redondant.
    String name;
    for (int j=0; j<ldirs.getNumGeometries(); j++) {
      if (attNameLines==-1 || 
          (name=(String)ldirs.getValue(attNameLines,j))==null || 
           name.length()>3 ||
           ldirnames_.indexOf(name)!=-1) {
        ldirnames_.add(null);
      }
      else {
        ldirnames_.add(name);
      }
    }

    // Ajout de noms de lignes conformes.
    int idir=0;
    char init='A';
    init--;
    
    for (int j=0; j<ldirs.getNumGeometries(); j++) {
      if (ldirnames_.get(j)==null) {
        do {
          if (idir%100==0)
            init++;
          name=""+init+(idir%100);
          idir++;
        }
        while (ldirnames_.indexOf(name)!=-1);
        ldirnames_.set(j,name);
      }
    }
    
    if (analyze_.containsErrors()) return false;
    return true;
  }
}
