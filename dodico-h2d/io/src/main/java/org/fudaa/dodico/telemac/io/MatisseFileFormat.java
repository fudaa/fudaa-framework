/*
 * @creation 12 mars 2003
 * @modification $Date: 2006-11-15 09:22:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.util.Arrays;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: MatisseFileFormat.java,v 1.15 2006-11-15 09:22:54 deniger Exp $
 */
public final class MatisseFileFormat extends FileFormatUnique {

  private static final MatisseFileFormat INSTANCE = new MatisseFileFormat();

  /**
   * @return le singleton
   */
  public static MatisseFileFormat getInstance() {
    return INSTANCE;
  }

  protected MatisseFileFormat() {
    super(1);
    extensions_ = new String[] {};
    Arrays.sort(extensions_);
    id_ = "MATISSE";
    nom_ = "matisse";
    description_ = H2dResource.getS("Fichier binaire BATHYGEO de matisse");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  public boolean containsGrid() {
    return false;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new MatisseReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return null;
  }

}