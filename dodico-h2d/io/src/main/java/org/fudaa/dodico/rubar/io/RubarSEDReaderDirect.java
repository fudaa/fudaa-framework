package org.fudaa.dodico.rubar.io;

import gnu.trove.TLongArrayList;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.Reader;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.dodico.fichiers.DodicoLineNumberReader;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * Lecteur de fichier SED plus econome en m�moire qui ne stocke pas les donn�es mais lit en fonction de la demande.
 *
 * @author Fred Deniger
 */
public class RubarSEDReaderDirect extends FileOpReadCharSimpleAbstract {

  private int nbNoeuds = -1;
  private DodicoLineNumberReader specReader_;
  private File file;

  @Override
  public void setFile(File _f) {
    super.setFile(_f);
    this.file = _f;
  }

  @Override
  protected void setFile(final Reader _r) {
    specReader_ = new DodicoLineNumberReader(_r);
    in_ = new FortranReader(specReader_);

  }

  @Override
  protected Object internalRead() {
    if (nbNoeuds <= 0) {
      analyze_.addFatalError("Le nombre de noeuds n'est pas pr�cis�");
    }
    in_.setBlankZero(true);
    //on se content de stocker les positions des blocs de pas de temps.
    final CtuluListDouble times = new CtuluListDouble();
    final TLongArrayList positions = new TLongArrayList();
    try {
      final int[] fmtTime = RubarSEDReader.getFormatTimeLine();
      //10 couches:
      final int[] fmtNoeud = RubarSEDReader.FMT_COUCHES;
      while (true) {
        positions.add(specReader_.getPositionInStream());
        in_.readFields(fmtTime);
        times.add(in_.doubleField(0));
        for (int i = 0; i < this.nbNoeuds; i++) {
          in_.readFields(fmtNoeud);
        }

      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    final RubarSEDSequentialResult r = new RubarSEDSequentialResult(nbNoeuds, times.getValues(), positions.toNativeArray(), file);
    return r;
  }

  /**
   * @return le nombre de noeuds � lire
   */
  public int getNbNoeuds() {
    return nbNoeuds;
  }

  /**
   * @param nbNoeuds le nombre de noeuds � lire
   */
  public void setNbNoeuds(final int nbNoeuds) {
    this.nbNoeuds = nbNoeuds;
  }
}
