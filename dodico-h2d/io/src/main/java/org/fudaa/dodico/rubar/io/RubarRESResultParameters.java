package org.fudaa.dodico.rubar.io;

public class RubarRESResultParameters
{
  private double it;
  private double coteEnCrete;
  private double largeurEnCrete;
  private double coteEnPied;
  private double largeurEnPied;
  private double diametreGrains;
  private double porosite;
  private double masseVolumique;
  private double stricklerDonne;
  private double stricklerCalcule;
  private double largeurMaxBreche;
  private double tempsDebutRupture;
  private double coteTerrainNaturel;
  private double coteBreche;
  private double rayonRenard;
  private double volumeErode;
  private double volumeParti;
  /**
   * @return the it
   */
  public double getIt() {
    return it;
  }
  /**
   * @param it the it to set
   */
  public void setIt(double it) {
    this.it = it;
  }
  /**
   * @return the coteEnCrete
   */
  public double getCoteEnCrete() {
    return coteEnCrete;
  }
  /**
   * @param coteEnCrete the coteEnCrete to set
   */
  public void setCoteEnCrete(double coteEnCrete) {
    this.coteEnCrete = coteEnCrete;
  }
  /**
   * @return the largeurEnCrete
   */
  public double getLargeurEnCrete() {
    return largeurEnCrete;
  }
  /**
   * @param largeurEnCrete the largeurEnCrete to set
   */
  public void setLargeurEnCrete(double largeurEnCrete) {
    this.largeurEnCrete = largeurEnCrete;
  }
  /**
   * @return the coteEnPied
   */
  public double getCoteEnPied() {
    return coteEnPied;
  }
  /**
   * @param coteEnPied the coteEnPied to set
   */
  public void setCoteEnPied(double coteEnPied) {
    this.coteEnPied = coteEnPied;
  }
  /**
   * @return the largeurEnPied
   */
  public double getLargeurEnPied() {
    return largeurEnPied;
  }
  /**
   * @param largeurEnPied the largeurEnPied to set
   */
  public void setLargeurEnPied(double largeurEnPied) {
    this.largeurEnPied = largeurEnPied;
  }
  /**
   * @return the diametreGrains
   */
  public double getDiametreGrains() {
    return diametreGrains;
  }
  /**
   * @param diametreGrains the diametreGrains to set
   */
  public void setDiametreGrains(double diametreGrains) {
    this.diametreGrains = diametreGrains;
  }
  /**
   * @return the porosite
   */
  public double getPorosite() {
    return porosite;
  }
  /**
   * @param porosite the porosite to set
   */
  public void setPorosite(double porosite) {
    this.porosite = porosite;
  }
  /**
   * @return the masseVolumique
   */
  public double getMasseVolumique() {
    return masseVolumique;
  }
  /**
   * @param masseVolumique the masseVolumique to set
   */
  public void setMasseVolumique(double masseVolumique) {
    this.masseVolumique = masseVolumique;
  }
  /**
   * @return the stricklerDonne
   */
  public double getStricklerDonne() {
    return stricklerDonne;
  }
  /**
   * @param stricklerDonne the stricklerDonne to set
   */
  public void setStricklerDonne(double stricklerDonne) {
    this.stricklerDonne = stricklerDonne;
  }
  /**
   * @return the stricklerCalcule
   */
  public double getStricklerCalcule() {
    return stricklerCalcule;
  }
  /**
   * @param stricklerCalcule the stricklerCalcule to set
   */
  public void setStricklerCalcule(double stricklerCalcule) {
    this.stricklerCalcule = stricklerCalcule;
  }
  /**
   * @return the largeurMaxBreche
   */
  public double getLargeurMaxBreche() {
    return largeurMaxBreche;
  }
  /**
   * @param largeurMaxBreche the largeurMaxBreche to set
   */
  public void setLargeurMaxBreche(double largeurMaxBreche) {
    this.largeurMaxBreche = largeurMaxBreche;
  }
  /**
   * @return the tempsDebutRupture
   */
  public double getTempsDebutRupture() {
    return tempsDebutRupture;
  }
  /**
   * @param tempsDebutRupture the tempsDebutRupture to set
   */
  public void setTempsDebutRupture(double tempsDebutRupture) {
    this.tempsDebutRupture = tempsDebutRupture;
  }
  /**
   * @return the coteTerrainNaturel
   */
  public double getCoteTerrainNaturel() {
    return coteTerrainNaturel;
  }
  /**
   * @param coteTerrainNaturel the coteTerrainNaturel to set
   */
  public void setCoteTerrainNaturel(double coteTerrainNaturel) {
    this.coteTerrainNaturel = coteTerrainNaturel;
  }
  /**
   * @return the coteBreche
   */
  public double getCoteBreche() {
    return coteBreche;
  }
  /**
   * @param coteBreche the coteBreche to set
   */
  public void setCoteBreche(double coteBreche) {
    this.coteBreche = coteBreche;
  }
  /**
   * @return the rayonRenard
   */
  public double getRayonRenard() {
    return rayonRenard;
  }
  /**
   * @param rayonRenard the rayonRenard to set
   */
  public void setRayonRenard(double rayonRenard) {
    this.rayonRenard = rayonRenard;
  }
  /**
   * @return the volumeErode
   */
  public double getVolumeErode() {
    return volumeErode;
  }
  /**
   * @param volumeErode the volumeErode to set
   */
  public void setVolumeErode(double volumeErode) {
    this.volumeErode = volumeErode;
  }
  /**
   * @return the volumeParti
   */
  public double getVolumeParti() {
    return volumeParti;
  }
  /**
   * @param volumeParti the volumeParti to set
   */
  public void setVolumeParti(double volumeParti) {
    this.volumeParti = volumeParti;
  }
}