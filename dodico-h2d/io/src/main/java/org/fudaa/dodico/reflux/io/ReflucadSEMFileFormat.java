/**
 * @creation 11 f�vr. 2004
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * Un format pour les fichiers Reflucad de semi de points.
 * @author Bertrand Marchand
 * @version $Id: SinusxFileFormat.java,v 1.15 2007/05/04 13:47:27 deniger Exp $
 */
public final class ReflucadSEMFileFormat extends FileFormatUnique {

  static final ReflucadSEMFileFormat INSTANCE = new ReflucadSEMFileFormat();

  /**
   * @return singleton
   */
  public static ReflucadSEMFileFormat getInstance() {
    return INSTANCE;
  }

  private ReflucadSEMFileFormat() {
    super(1);
    extensions_ = new String[] { "sem" };
    id_ = "RCADSEM";
    nom_ = "Reflucad semis";
    description_ = DodicoLib.getS("Comporte les d�finitions de semis toppographiques");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new ReflucadSEMReader(this);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return super.write(_f, _source, _prog);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    /** @todo */
    throw new RuntimeException("Method not implemented");
//    return new ReflucadSEMWriter(this);
  }

}