/**
 *  @creation     14 avr. 2003
 *  @modification $Date: 2007-05-04 13:47:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: Telemac2dFileFormat.java,v 1.13 2007-05-04 13:47:30 deniger Exp $
 */
public class Telemac2dFileFormat extends TelemacDicoFileFormat {
  /**
   * Initialise les variables de ce format. Par defaut, la version v5p2 est utilisee.
   */
  private static final Telemac2dFileFormat INSTANCE = new Telemac2dFileFormat();

  /**
   * @return singleton
   */
  public static final Telemac2dFileFormat getInstance() {
    return INSTANCE;
  }

  protected Telemac2dFileFormat() {
    super("telemac2d");
    extensions_ = new String[] { "" };
    id_ = nom_.toUpperCase();
    description_ = H2dResource.getS("fichier de param�tres pour T�l�mac 2D");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  /**
   * @param _dico le modeles des mot-cle
   * @return version utilisant ce modiel
   */
  public TelemacVersion createVersion(final DicoModelAbstract _dico) {
    return new TelemacVersion(_dico);
  }

  /**
   * @author Fred Deniger
   * @version $Id: Telemac2dFileFormat.java,v 1.13 2007-05-04 13:47:30 deniger Exp $
   */
  public static final class TelemacVersion extends TelemacDicoFileFormatVersion {
    TelemacVersion(final DicoModelAbstract _dico) {
      super(getInstance(), _dico);
    }

  }
}
