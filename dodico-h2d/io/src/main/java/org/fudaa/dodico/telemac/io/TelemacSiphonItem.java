/**
 * @creation 29 nov. 2004
 * @modification $Date: 2007-05-04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.collection.CtuluDoubleVisitor;

import org.fudaa.dodico.h2d.telemac.H2dTelemacSiphonInterface;

/**
 * @author Fred Deniger
 * @version $Id: TelemacSiphonItem.java,v 1.13 2007-05-04 13:47:27 deniger Exp $
 */
public class TelemacSiphonItem implements H2dTelemacSiphonInterface {

  double a1_, a2_;

  double ce1_, ce2_;

  double cs1_, cs2_;

  double d1_, d2_;

  int i1_, i2_;

  double l12_;

  double s12_;

  double[] values_;

  double z1_, z2_;

  @Override
  public double[] getValues() {
    return CtuluCollectionDoubleAbstract.getArray(this);
  }

  private void buildValue() {
    values_ = new double[getSize()];
    int idx = 0;
    values_[idx++] = d1_;
    values_[idx++] = d2_;
    values_[idx++] = ce1_;
    values_[idx++] = ce2_;
    values_[idx++] = cs1_;
    values_[idx++] = cs2_;
    values_[idx++] = s12_;
    values_[idx++] = l12_;
    values_[idx++] = z1_;
    values_[idx++] = z2_;
    values_[idx++] = a1_;
    values_[idx++] = a2_;

  }

  public final double getA1() {
    return a1_;
  }

  public final double getA2() {
    return a2_;
  }

  public final double getCe1() {
    return ce1_;
  }

  public final double getCe2() {
    return ce2_;
  }

  @Override
  public Double getCommonValue(final int[] _i) {
    return CtuluCollectionDoubleAbstract.getCommonValue(this, _i);
  }

  public final double getCs1() {
    return cs1_;
  }

  public final double getCs2() {
    return cs2_;
  }

  public final double getD1() {
    return d1_;
  }

  public final double getD2() {
    return d2_;
  }

  public final int getI1() {
    return i1_;
  }

  public final int getI2() {
    return i2_;
  }

  public final double getL12() {
    return l12_;
  }

  @Override
  public double getMax() {
    return 0;
  }

  @Override
  public void expandTo(final CtuluRange _rangeToExpand) {
    _rangeToExpand.expandTo(0);
  }

  @Override
  public void getRange(final CtuluRange _r) {
    if (_r != null) {
      _r.setToNill();
    }

  }

  @Override
  public double getMin() {
    return 0;
  }

  public final double getS12() {
    return s12_;
  }

  @Override
  public int getSource1() {
    return i1_;
  }

  @Override
  public int getSource2() {
    return i2_;
  }

  @Override
  public double getValue(final int _i) {
    if (values_ == null) {
      buildValue();
    }
    return values_[_i];
  }

  @Override
  public Object getObjectValueAt(final int _i) {
    return CtuluLib.getDouble(getValue(_i));
  }

  @Override
  public int getSize() {
    return 12;
  }

  @Override
  public void iterate(final CtuluDoubleVisitor _visitor) {

  }

  public final double getZ1() {
    return z1_;
  }

  public final double getZ2() {
    return z2_;
  }
}