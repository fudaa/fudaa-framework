package org.fudaa.dodico.mascaret.io;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * Une repr�sentation d'un bief Mascaret.
 * @author bmarchan
 */
public class MascaretBief {

  /** Profils ordonn�es du bief */
  List<MascaretProfilAbstractRepresentation> profils_=new ArrayList<MascaretProfilAbstractRepresentation>();

  public void addProfil(MascaretProfilAbstractRepresentation _prof) {
    profils_.add(_prof);
  }

  public int getNbProfils() {
    return profils_.size();
  }

  public MascaretProfilAbstractRepresentation getProfil(int _i) {
    return profils_.get(_i);
  }

  /**
   * @return Le nom du bief, ou null si aucun profil dans le bief.
   */
  public String getBiefName() {
    if (profils_.size()>0) return profils_.get(0).nomBief;
    return null;
  }

  /**
   * @param _analyze Pour suivi du traitement.
   * @param _progress La progression de lecture.
   * @return Les 5 zones correspondant aux traces, profils, axe, rives, stockage
   * dans cet ordre.
   */
  public GISZoneCollectionLigneBrisee[] buildZones(CtuluAnalyze _analyze, ProgressionInterface _progress) {
    if (profils_.size()==0)
      _analyze.addError(DodicoLib.getS("Aucun profil dans le bief."));


    GISZoneCollectionLigneBrisee traces=new GISZoneCollectionLigneBrisee();
    traces.setAttributes(new GISAttributeInterface[]{
      GISAttributeConstants.TITRE, GISAttributeConstants.NATURE}, null);

    GISZoneCollectionLigneBrisee profs=new GISZoneCollectionLigneBrisee();
    profs.setAttributes(new GISAttributeInterface[]{
      GISAttributeConstants.TITRE, GISAttributeConstants.NATURE,
      GISAttributeConstants.BATHY, GISAttributeConstants.COMMENTAIRE_HYDRO}, null);

    // G�n�ration des profiles et des traces de profiles \\
    for(int k=0;k<profils_.size();k++){
      if (_progress!=null)
        _progress.setProgression((int)((float)k/profils_.size()*100));
      
      MascaretProfilAbstractRepresentation profil=profils_.get(k);
      CoordinateArraySequence coordSeq=new CoordinateArraySequence(profil.ptsTrace.size());
      // Trace profil
      for(int i=0;i<profil.ptsTrace.size();i++){
        coordSeq.setOrdinate(i, 0, profil.ptsTrace.get(i)[0]);
        coordSeq.setOrdinate(i, 1, profil.ptsTrace.get(i)[1]);
        coordSeq.setOrdinate(i, 2, 0);
      }
      traces.addGeometry(new GISPolyligne(coordSeq), new Object[]{profil.nomProfil, GISAttributeConstants.ATT_NATURE_TP}, null);

      // Profil
      coordSeq=new CoordinateArraySequence(profil.pts.size());
      Object[] z=new Object[profil.pts.size()];
      for(int i=0;i<profil.pts.size();i++){
        coordSeq.setOrdinate(i, 0, profil.pts.get(i)[0]);
        coordSeq.setOrdinate(i, 1, profil.pts.get(i)[1]);
        z[i]=profil.coordZ.get(i);
        coordSeq.setOrdinate(i, 2, 0);
      }
      String comm=GISLib.setHydroCommentDouble("", profil.abscLong, GISAttributeConstants.ATT_COMM_HYDRO_PK);
      profs.addGeometry(new GISPolyligne(coordSeq), new Object[]{profil.nomProfil, GISAttributeConstants.ATT_NATURE_PF, z, comm}, null);
    }

    if (profils_.size()<2)
      _analyze.addInfo(DodicoLib.getS("Le bief ne comporte qu'un profil : Aucun axe hydraulique, aucune rive, aucune limite ne seront g�n�r�s."));

    GISZoneCollectionLigneBrisee[] zlims=buildLimites();
    GISZoneCollectionLigneBrisee zaxe=buildAxisWithPKOnProfiles();
    return new GISZoneCollectionLigneBrisee[]{traces,profs,zaxe,zlims[0],zlims[1]};
  }

  /**
   *
   * @return Les zones correspondant aux limites de biefs (rives + limites stockage
   * dans cet ordre)
   */
  private GISZoneCollectionLigneBrisee[] buildLimites() {
    GISZoneCollectionLigneBrisee rives=new GISZoneCollectionLigneBrisee();
    rives.setAttributes(new GISAttributeInterface[]{
      GISAttributeConstants.TITRE, GISAttributeConstants.NATURE}, null);

    GISZoneCollectionLigneBrisee stockages=new GISZoneCollectionLigneBrisee();
    stockages.setAttributes(new GISAttributeInterface[]{
      GISAttributeConstants.TITRE, GISAttributeConstants.NATURE}, null);

    // Les 4 lignes, dans l'ordre SG, SD, RG, RD
    CoordinateSequence[] seqs=new CoordinateSequence[4];
    for (int i=0; i<seqs.length; i++) {
      seqs[i]=GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(profils_.size(),3);
    }

    for (int j=0; j<profils_.size(); j++) {
      // Flag qui indique qu'on est pass� par le lit mineur
      boolean lm=false;

      Integer[] idx=new Integer[4];
      idx[0]=0;
      idx[1]=null;
      idx[2]=0;
      idx[3]=null;

      for (int i=0; i<profils_.get(j).typePts.size(); i++) {
        if (profils_.get(j).typePts.get(i)=='S') {
          if (!lm) {
            idx[0]=i+1;
            idx[2]=i+1;
          }
          else if (idx[1]==null) {
            idx[1]=i-1;
          }
        }
        if (profils_.get(j).typePts.get(i)=='T') {
          if (!lm) {
            idx[2]=i+1;
          }
          else if (idx[3]==null) {
            idx[3]=i-1;
          }
        }
        if (profils_.get(j).typePts.get(i)=='B') {
          lm=true;
        }
      }

      if (idx[3]==null)
        idx[3]=profils_.get(j).typePts.size()-1;
      if (idx[1]==null)
        idx[1]=profils_.get(j).typePts.size()-1;

      for (int i=0; i<seqs.length; i++) {
        seqs[i].setOrdinate(j, 0, profils_.get(j).pts.get(idx[i])[0]);
        seqs[i].setOrdinate(j, 1, profils_.get(j).pts.get(idx[i])[1]);
        seqs[i].setOrdinate(j, 2, 0);
      }
    }

    // Les limites de stockage
    stockages.addGeometry(new GISPolyligne(seqs[0]), new Object[]{"SG", GISAttributeConstants.ATT_NATURE_LS}, null);
    stockages.addGeometry(new GISPolyligne(seqs[1]), new Object[]{"SD", GISAttributeConstants.ATT_NATURE_LS}, null);
    // Les rives
    rives.addGeometry(new GISPolyligne(seqs[2]), new Object[]{"RG", GISAttributeConstants.ATT_NATURE_RV}, null);
    rives.addGeometry(new GISPolyligne(seqs[3]), new Object[]{"RD", GISAttributeConstants.ATT_NATURE_RV}, null);

    return new GISZoneCollectionLigneBrisee[]{rives,stockages};
  }

  /**
   * Cr�e l'axe hydraulique en respectant les PK des profils, et ajout de 2
   * points aux extr�mit�s pour �tre sur que les profils intersectent
   * bien l'axe. Les valeurs de profils en long sont modifi�s.
   *
   * @return L'axe hydraulique.
   */
  private GISZoneCollectionLigneBrisee buildAxisWithPKOnProfiles() {
    GISZoneCollectionLigneBrisee axe=new GISZoneCollectionLigneBrisee();
    axe.setAttributes(new GISAttributeInterface[]{
      GISAttributeConstants.NATURE, GISAttributeConstants.CURVILIGNE_DECALAGE}, null);

    // G�n�ration de l'axe hydraulique, si au moins 2 profils

    double pk=profils_.get(0).abscLong;
    FuLog.debug("Abs curv "+profils_.get(0).nomProfil+": "+pk);
    if (profils_.size()>1) {
      GrPoint p;
      int iProfFin=profils_.size()-1;
      double lgBief=profils_.get(iProfFin).abscLong-profils_.get(0).abscLong;

      List<Coordinate> lcoords=new ArrayList<Coordinate>();

      // Ajout du premier point extremit�, au dela du premier profil
      GrVecteur vext=new GrVecteur(
          profils_.get(1).ptAxeHydrau[0]-profils_.get(0).ptAxeHydrau[0],
          profils_.get(1).ptAxeHydrau[1]-profils_.get(0).ptAxeHydrau[1], 0).normaliseXY();
      double decal=lgBief/20;
      vext.autoMultiplication(decal);
      p=new GrPoint(profils_.get(0).ptAxeHydrau[0],profils_.get(0).ptAxeHydrau[1],0).soustraction(vext);
      lcoords.add(new Coordinate(p.x_, p.y_));

      for (int i=0; i<profils_.size()-1; i++) {
        // Longueur curviligne entre les 2 profils
        double lgCurv=profils_.get(i+1).abscLong-profils_.get(i).abscLong;
        // Longueur du segment entre les 2 profils
        Coordinate c1=new Coordinate(profils_.get(i).ptAxeHydrau[0], profils_.get(i).ptAxeHydrau[1]);
        Coordinate c2=new Coordinate(profils_.get(i+1).ptAxeHydrau[0], profils_.get(i+1).ptAxeHydrau[1]);
        double lgSeg=CtuluLibGeometrie.getDistanceXY(c1, c2);

        lcoords.add(c1);

        // Si la longueur du segment est inf�rieure � la longueur curviligne, on
        // ajoute 4 points interm�diaires.
        // Les 2 premiers points sont positionn�s sur le segment d�fini par les 2
        // coordonn�es de l'axe hydraulique � l'index i et i+1, sym�triquement
        // par rapport au milieu, � une distance de 5% de la longueur du segment.
        // Les 2 autres points interm�diaires sont sur la m�diatrice du segment
        // � �gale distance du segment d'axe.
        // La forme ortenue est un Z.
        if (lgSeg+0.0001<lgCurv) {

          // Vecteur directeur du segment
          GrVecteur vseg=new GrVecteur(c2.x-c1.x,c2.y-c1.y, 0).normaliseXY();
          // Vecteur directeur de la m�diatrice du segment d'axe.
          GrVecteur vmed=new GrVecteur(c2.y-c1.y,c1.x-c2.x, 0).normaliseXY();
          // Point milieu du segment
          GrPoint pmil=new GrPoint(GISLib.computeMiddle(c1, c2));
          // Distance de 5% sur le segment
          double dstSeg=lgSeg/20.;
          // Calcul des 2 points (cs1 et cs2) sur le segment
          vseg.autoMultiplication(dstSeg);
          p=pmil.soustraction(vseg);
          Coordinate cs1=new Coordinate(p.x_, p.y_);
          p=pmil.addition(vseg);
          Coordinate cs2=new Coordinate(p.x_, p.y_);
          // Distance curv et lineaire entre les points cs2 et cs1
          lgCurv=lgCurv-lgSeg+2*dstSeg;
          lgSeg=2*dstSeg;

          // Distance d'un point sur la m�diatrice au segment d'axe
          double dstMed=(lgCurv*lgCurv-lgSeg*lgSeg)/(4*lgCurv);
          // Calcul des 2 points (cm1 et cm2) sur la m�diatrice
          vmed.autoMultiplication(dstMed);
          p=pmil.addition(vmed);
          Coordinate cm1=new Coordinate(p.x_, p.y_);
          p=pmil.soustraction(vmed);
          Coordinate cm2=new Coordinate(p.x_, p.y_);

          // Ajout des points calcul�s
          lcoords.add(cs1);
          pk+=lcoords.get(lcoords.size()-2).distance(lcoords.get(lcoords.size()-1));
          lcoords.add(cm1);
          pk+=lcoords.get(lcoords.size()-2).distance(lcoords.get(lcoords.size()-1));
          lcoords.add(cm2);
          pk+=lcoords.get(lcoords.size()-2).distance(lcoords.get(lcoords.size()-1));
          lcoords.add(cs2);
          pk+=lcoords.get(lcoords.size()-2).distance(lcoords.get(lcoords.size()-1));
        }

        pk+=lcoords.get(lcoords.size()-1).distance(c2);
        FuLog.debug("Abs curv "+profils_.get(i+1).nomProfil+": "+pk);

        // On ajoute le dernier point de l'axe.
        if (i==profils_.size()-2)
          lcoords.add(c2);
      }

      // Ajout du dernier point extremit�, au dela du dernier profil
      GrVecteur vextFin=new GrVecteur(
          profils_.get(iProfFin).ptAxeHydrau[0] - profils_.get(iProfFin - 1).ptAxeHydrau[0],
          profils_.get(iProfFin).ptAxeHydrau[1] - profils_.get(iProfFin - 1).ptAxeHydrau[1], 0).normaliseXY();
      vextFin.autoMultiplication(lgBief / 20);
      p=new GrPoint(profils_.get(iProfFin).ptAxeHydrau[0], profils_.get(iProfFin).ptAxeHydrau[1], 0).addition(vextFin);
      lcoords.add(new Coordinate(p.x_, p.y_));

      CoordinateSequence seq=GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(lcoords.toArray(new Coordinate[0]));

      // Ajout du d�calage d'axe pour le respect des PK
      axe.addGeometry(new GISPolyligne(seq), new Object[]{GISAttributeConstants.ATT_NATURE_AH, profils_.get(0).abscLong-decal}, null);
    }

    return axe;
  }
}
