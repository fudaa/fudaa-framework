/**
 * @creation 29 ao�t 2003
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.util.Map;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author deniger
 * @version $Id: CLTransitoireFileFormat.java,v 1.19 2006-11-15 09:22:53 deniger Exp $
 */
public final class CLTransitoireFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {

  private static final CLTransitoireFileFormat INSTANCE = new CLTransitoireFileFormat();

  /**
   * @return le singleton
   */
  public static CLTransitoireFileFormat getInstance() {
    return INSTANCE;
  }

  private CLTransitoireFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Reflux courbes transitoires (format binaire)");
    extensions_ = new String[] { "clv" };
    id_ = "REFLUX_CLV";
    description_ = H2dResource.getS("Fichier contenant les conditions limites transitoire");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  /**
   * @return le reader
   */
  public CLVReader createCLVReader() {
    return new CLVReader(this);
  }

  /**
   * @return le writer
   */
  public CLVWriter createCLVWriter() {
    return new CLVWriter(this);
  }

  /**
   * @param _f le fichier cible
   * @param _inter les evolutions a ecrire
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _f, final EvolutionReguliere[] _inter,
      final ProgressionInterface _prog) {
    final CLVWriter w = createCLVWriter();
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new CLVReader(this);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new CLVWriter(this);
  }

  @Override
  public CtuluIOOperationSynthese readEvolutions(final File _f, final ProgressionInterface _prog, final Map _options) {
    return read(_f, _prog);
  }

}