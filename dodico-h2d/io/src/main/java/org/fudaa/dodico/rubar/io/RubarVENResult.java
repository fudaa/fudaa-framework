package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.rubar.H2dRubarVentInterface;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarVENResult implements H2dRubarVentInterface {
  EvolutionReguliere[] evolsX;
  EvolutionReguliere[] evolsY;
  int[] eltEvolIdx;

  /**
   * @return le nombre d'élément.
   */
  @Override
  public int getNbElt() {
    return eltEvolIdx == null ? 0 : eltEvolIdx.length;
  }

  /**
   * @return le nombre d'évolution X.
   */
  public int getNbEvolX() {
    return evolsX == null ? 0 : evolsX.length;
  }

  @Override
  public int getNbEvol() {
    return getNbEvolX();

  }

  /**
   * @return le nombre d'évolution Y.
   */
  public int getNbEvolY() {
    return evolsY == null ? 0 : evolsY.length;
  }

  @Override
  public EvolutionReguliere getEvolutionAlongX(int index) {
    return evolsX == null ? null : evolsX[index];
  }

  @Override
  public EvolutionReguliere getEvolutionAlongY(int index) {
    return evolsY == null ? null : evolsY[index];
  }

  /**
   * @param eltIdx l'element demande
   * @return l'indice de la courbe.
   */
  @Override
  public int getEvolIdx(final int eltIdx) {
    return eltEvolIdx[eltIdx];
  }

  @Override
  public EvolutionReguliere getEvolAlongXForElt(int eltIdx) {
    return getEvolutionAlongX(getEvolIdx(eltIdx));
  }

  @Override
  public EvolutionReguliere getEvolAlongYForElt(int idxElt) {
    return getEvolutionAlongY(getEvolIdx(idxElt));
  }
}