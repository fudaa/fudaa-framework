/**
 *  @creation     1 juin 2004
 *  @modification $Date: 2006-09-19 14:45:58 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoModelAbstract;


/**
 * @author Fred Deniger
 * @version $Id: TelemacSubief2dFileFormatVersion.java,v 1.3 2006-09-19 14:45:58 deniger Exp $
 */
public class TelemacSubief2dFileFormatVersion extends TelemacDicoFileFormatVersion {

  /**
   * @param _ft
   * @param _dico
   */
  public TelemacSubief2dFileFormatVersion(final DicoCasFileFormat _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico);
    DicoEntite e=getEntiteFor( new String[]{"FICHIER FORTRAN DE LECTURE DES DONNEES","FORTRAN FILE FOR READING DATA"});
    e.setRequired(true);
    e=getEntiteFor( new String[]{"FICHIER FORTRAN DE CHIMIE","CHIMIE FORTRAN FILE"});
    e.setRequired(true);
    e=getEntiteFor( new String[]{"DICTIONNAIRE DES PARAMETRES DE QE","QE PARAMETERS DICTIONARY"});
    e.setRequired(true);
    e=getEntiteFor( new String[]{"FICHIER DES PARAMETRES DE QE","WQ STEERING FILE"});
    e.setRequired(true);
    e=getEntiteFor( new String[]{"FICHIER FORTRAN","FORTRAN FILE"});
    e.setRequired(true);
    e=getEntiteFor( new String[]{"FICHIER DES RESULTATS","RESULTS FILE"});
    e.setRequired(true);
    e=getEntiteFor( new String[]{"FICHIER HYDRODYNAMIQUE","HYDRODYNAMIC FILE"});
    e.setRequired(true);
    
    
  }

}
