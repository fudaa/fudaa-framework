/**
 * @creation 13 oct. 2004
 * @modification $Date: 2006-09-19 14:45:57 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar;

/**
 * @author Fred Deniger
 * @version $Id: RubarRubarExec.java,v 1.4 2006-09-19 14:45:57 deniger Exp $
 */
public class RubarRubarExec extends RubarExecAbstract {

  public RubarRubarExec() {
    super("rubar20");
  }
}
