/**
 * @creation 04 f�vr. 2008
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * Un format pour les fichiers Reflucad de profils bathymetriques.
 * @author Bertrand Marchand
 * @version $Id:$
 */
public final class ReflucadPROFileFormat extends FileFormatUnique {

  static final ReflucadPROFileFormat INSTANCE = new ReflucadPROFileFormat();

  /**
   * @return singleton
   */
  public static ReflucadPROFileFormat getInstance() {
    return INSTANCE;
  }

  private ReflucadPROFileFormat() {
    super(1);
    extensions_ = new String[] { "pro" };
    id_ = "RCADPRO";
    nom_ = "Reflucad profils";
    description_ = DodicoLib.getS("Comporte les d�finitions de profils bathym�triques");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new ReflucadPROReader(this);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return super.write(_f, _source, _prog);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    /** @todo */
    throw new RuntimeException("Method not implemented");
//    return new ReflucadPROWriter(this);
  }

}