/**
 * @creation 28 mars 2003
 * @modification $Date: 2007-06-29 15:10:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntObjectHashMap;

import java.util.Map;

import org.fudaa.ctulu.fileformat.FileFormat;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.H2dTimeStepGroup;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoModel;
import org.fudaa.dodico.h2d.type.H2dProjetType;

/**
 * @author deniger
 * @version $Id: INPAdapter.java,v 1.26 2007-06-29 15:10:25 deniger Exp $
 */
public class INPAdapter extends EfGridSourcesAbstract implements INPInterface {

  private H2dRefluxBordIndexGeneral[] bords_;
  // private H2dRefluxBoundaryCondition[] conditionLimite_;
  private TIntObjectHashMap conditionLimite_;
  private boolean contientRadiations_;
  private boolean contientSolReparties_;
  private boolean contientVent_;
  private H2dRefluxDicoModel dicoModel_;
  private Map entitesValeurs_;

  private String[] fics_;
  private H2dTimeStepGroup[] groupePasTemps_;
  private boolean isRadiationsLecturePasDeTemps_;
  private boolean isVentLecturePasDeTemps_;
  private EfGrid maillage_;
  private H2dNodalPropertyMixte[] propElementaires_;
  H2dNodalPropertyMixte[] propNodales_;
  private final double timeBeginning_ = 0;
  // public H2dParametresCalculReflux parametresCalcul_;
  private H2dProjetType typeProjet_;
  // private int nbPNTransitoires_;
  // private int nbCLTransitoires_;
  private String version_;

  /**
   * Pour Reflux 5.1.
   */
  private int typeSortie_;

  FortranDoubleReaderResultInterface radiations_;
  FortranDoubleReaderResultInterface vent_;

  double beginTime_ = -1;

  @Override
  public double getTimeBeginInForInpFile() {
    return beginTime_;
  }

  public boolean isBeginTimeSet() {
    return beginTime_ >= 0;
  }

  /**
   * version inconnue.
   */
  public INPAdapter() {
    version_ = DodicoLib.getS("Inconnue");
  }

  @Override
  public boolean contientRadiations() {
    return contientRadiations_;
  }

  @Override
  public boolean contientSollicitationsReparties() {
    return contientSolReparties_;
  }

  @Override
  public boolean contientVent() {
    return contientVent_;
  }

  @Override
  public H2dRefluxBordIndexGeneral[] getBords() {
    return bords_;
  }

  @Override
  public H2dRefluxBoundaryCondition getConditionLimite(final int _idxGlobal) {
    return (H2dRefluxBoundaryCondition) conditionLimite_.get(_idxGlobal);
    // return H2dRefluxBoundaryCondition.getCLFastFromIdxGlobal(_idxGlobal, conditionLimite_);
  }

  /**
   * @return le modele des mot-cle
   */
  public H2dRefluxDicoModel getDicoModel() {
    return dicoModel_;
  }

  @Override
  public Map getEntiteValue() {
    return entitesValeurs_;
  }

  @Override
  public String[] getFichiers() {
    return fics_;
  }

  /**
   * utiliser par l'interface EfGridSource.
   */
  public FileFormat getFileFormat() {
    return INPFileFormat.getInstance();
  }

  @Override
  public EfGridInterface getGrid() {
    return maillage_;
  }

  @Override
  public H2dTimeStepGroup[] getGroupePasTemps() {
    return groupePasTemps_;
  }

  @Override
  public H2dNodalPropertyMixte[] getPropElementaires() {
    return propElementaires_;
  }

  @Override
  public double getTimeBeginningForTrans() {
    return timeBeginning_;
  }

  // /**
  // *
  // */
  // public H2dParametresCalculReflux getParametresCalcul()
  // {
  // return parametresCalcul_;
  // }
  //
  // /**
  // *
  // */
  // public void setParametresCalcul(H2dParametresCalculReflux _reflux)
  // {
  // parametresCalcul_ = _reflux;
  // }
  /**
   *
   */
  @Override
  public H2dProjetType getTypeProjet() {
    return typeProjet_;
  }

  @Override
  public String getVersion() {
    return version_;
  }

  @Override
  public boolean isRadiationsLecturePasDeTemps() {
    return isRadiationsLecturePasDeTemps_;
  }

  @Override
  public boolean isVentLecturePasDeTemps() {
    return isVentLecturePasDeTemps_;
  }

  /**
   * @param _bords les bords
   */
  public void setBords(final H2dRefluxBordIndexGeneral[] _bords) {
    bords_ = _bords;
  }

  /**
   * @param _refluxs les cl
   */
  public void setConditionLimite(final TIntObjectHashMap _refluxs) {
    conditionLimite_ = _refluxs;
  }

  /**
   * Sets the contientRadiations.
   * 
   * @param _contientRadiations The contientRadiations to set
   */
  public void setContientRadiations(final boolean _contientRadiations) {
    this.contientRadiations_ = _contientRadiations;
  }

  /**
   * Sets the contientSolReparties.
   * 
   * @param _contientSolReparties The contientSolReparties to set
   */
  public void setContientSolReparties(final boolean _contientSolReparties) {
    this.contientSolReparties_ = _contientSolReparties;
  }

  /**
   * Sets the contientVent.
   * 
   * @param _contientVent The contientVent to set
   */
  public void setContientVent(final boolean _contientVent) {
    this.contientVent_ = _contientVent;
  }

  /**
   * @param _model le nouveau model
   */
  public void setDicoModel(final H2dRefluxDicoModel _model) {
    dicoModel_ = _model;
  }

  /**
   * @param _map dicoEntite->String
   */
  public void setEntitesValeurs(final Map _map) {
    entitesValeurs_ = _map;
  }

  /**
   * Sets the fics.
   * 
   * @param _fics The fics to set
   */
  public void setFics(final String[] _fics) {
    fics_ = _fics;
  }

  /**
   * @param _tempses les groupes de pas de temps.
   */
  public void setGroupePasTemps(final H2dTimeStepGroup[] _tempses) {
    groupePasTemps_ = _tempses;
  }

  /**
   * @param _maillage le maillage
   */
  public void setMaillage(final EfGrid _maillage) {
    maillage_ = _maillage;
  }

  /**
   * @param _elementaires les prop elementaires
   */
  public void setPropElementaires(final H2dNodalPropertyMixte[] _elementaires) {
    propElementaires_ = _elementaires;
  }

  /**
   * Sets the isRadiationsLecturePasDeTemps.
   * 
   * @param _b true si radiation
   */
  public void setRadiationsLecturePasDeTemps(final boolean _b) {
    this.isRadiationsLecturePasDeTemps_ = _b;
  }

  /**
   * @param _type le type de projet
   */
  public void setTypeProjet(final H2dProjetType _type) {
    typeProjet_ = _type;
  }

  /**
   * Sets the isVentLecturePasDeTemps.
   * 
   * @param _b vent
   */
  public void setVentLecturePasDeTemps(final boolean _b) {
    this.isVentLecturePasDeTemps_ = _b;
  }

  /**
   * @param _string description de la version
   */
  public void setVersion(final String _string) {
    version_ = _string;
  }

  protected void setBeginTime(final double _beginTime) {
    beginTime_ = _beginTime;
  }

  @Override
  public FortranDoubleReaderResultInterface getRadiationResult() {
    return radiations_;
  }

  @Override
  public FortranDoubleReaderResultInterface getVentResult() {
    return vent_;
  }

  public void setRadiations(final FortranDoubleReaderResultInterface _radiations) {
    radiations_ = _radiations;
  }

  public void setVent(final FortranDoubleReaderResultInterface _vent) {
    vent_ = _vent;
  }

  @Override
  public int getTypeSortie() {
    return typeSortie_;
  }

  public void setTypeSortie(int _typeSortie) {
    typeSortie_ = _typeSortie;
  }

  @Override
  public H2dNodalPropertyMixte[] getPropNodales() {
    return propNodales_;
  }

  public void setPropNodales(H2dNodalPropertyMixte[] _propNodales) {
    propNodales_ = _propNodales;
  }

}