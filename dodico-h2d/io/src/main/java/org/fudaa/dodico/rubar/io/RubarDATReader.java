/**
 * @creation 8 juin 2004
 * @modification $Date: 2007-03-02 13:00:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteDefault;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarDATReader.java,v 1.17 2007-03-02 13:00:51 deniger Exp $
 */
public class RubarDATReader extends FileOpReadCharSimpleAbstract {
  RubarDATFileFormat v_;
  boolean newFormat;

  public boolean isNewFormat() {
    return newFormat;
  }

  /**
   * @param _v la version a utiliser.
   */
  public RubarDATReader(final RubarDATFileFormat _v) {
    v_ = _v;
  }

  @Override
  protected Object internalRead() {
    H2dRubarGridAreteDefault r = null;
    EfElementType typeElt = null;
    try {
      // LECTURE DES ELEMENTS
      in_.readFields();
      // lecture ne
      final int ne = in_.intField(0);
      newFormat = RubarFortranDoubleReader.isCorrectFormat(in_.getLine(), new int[]{9}, true);
      // initialisation donnees
      int tmpOnLine = 0;
      int nbFieldByLine;
      final EfElementVolume[] elt = new EfElementVolume[ne];
      final int[][] ieNeve = new int[ne][];
      int[] fmt = new int[5];
      Arrays.fill(fmt, newFormat ? 9 : 6);
      nbFieldByLine = fmt.length;
      TIntArrayList neWithErreur = null;
      for (int ie = 0; ie < ne; ie++) {
        // lecture de la ligne NAE(IE),IAE(JA)
        in_.readFields(fmt);
        final int nae = in_.intField(0);
        // utilise pour savoir ou on se trouve sur la ligne
        tmpOnLine = 1;
        final int[] iae = new int[nae];
        for (int ja = 0; ja < nae; ja++) {
          if (tmpOnLine == nbFieldByLine) {
            in_.readFields(fmt);
            tmpOnLine = 0;
          }
          // en java les index commencent a 0
          iae[ja] = in_.intField(tmpOnLine) - 1;
          tmpOnLine++;
        }
        // lecture de la ligne NNE(IE),INE(JN)
        in_.readFields(fmt);
        final int nne = in_.intField(0);
        if (ie == 0) {
          typeElt = EfElementType.getCommunType(nne);
        } else if (typeElt != null) {
          final EfElementType neType = EfElementType.getCommunType(nne);
          // le type est different
          if (neType != typeElt
              && !(typeElt == EfElementType.T3_Q4 && (neType == EfElementType.T3 || neType == EfElementType.Q4))) {
            // pour rubar, on peut avoir des T3 et Q4
            if ((typeElt == EfElementType.T3 && neType == EfElementType.Q4)
                || (neType == EfElementType.T3 && typeElt == EfElementType.Q4)) {
              typeElt = EfElementType.T3_Q4;
            } else {
              typeElt = null;
              FuLog.warning("grid type unknown");
            }
          }
        }
        tmpOnLine = 1;
        final int[] ine = new int[nne];
        for (int jn = 0; jn < nne; jn++) {
          if (tmpOnLine == nbFieldByLine) {
            in_.readFields(fmt);
            tmpOnLine = 0;
          }
          ine[jn] = in_.intField(tmpOnLine) - 1;
          if (ine[jn] < 0) {
            if (neWithErreur == null) {
              neWithErreur = new TIntArrayList();
            }
            neWithErreur.add(ie);
          }
          tmpOnLine++;
        }
        // lecture de la ligne NEVE(IE),IEVE(JE)
        // donnees stockes dans un tableau autre pour l'instant
        in_.readFields(fmt);
        final int neve = in_.intField(0);
        tmpOnLine = 1;
        ieNeve[ie] = new int[neve];
        for (int je = 0; je < neve; je++) {
          if (tmpOnLine == nbFieldByLine) {
            in_.readFields(fmt);
            tmpOnLine = 0;
          }

          ieNeve[ie][je] = in_.intField(tmpOnLine) - 1;
          tmpOnLine++;
        }
        // maj de l'element correspondant
        elt[ie] = new EfElementVolume(iae, ine);
      }
      if (progress_ != null) {
        progress_.setProgression(20);
      }
      // LECTURE DES ARETES
      // na
      in_.readFields();
      final int na = in_.intField(0);
      fmt = new int[6];
      Arrays.fill(fmt, newFormat ? 9 : 6);
      nbFieldByLine = fmt.length;
      final H2dRubarArete[] segment = new H2dRubarArete[na];
      final int[][] iaNeva = new int[na][];
      // permet de calculer le nombre d'aretes limites rentrantes
      // int nbNasCalculated = 0;
      TIntArrayList areteWithError = null;
      for (int ia = 0; ia < na; ia++) {
        // lecture nna(ia),ina(ia,jn)
        in_.readFields(fmt);
        final int nna = in_.intField(0);
        // une arete invalide on arrete la lecture
        if (nna != 2) {
          analyze_.addFatalError(H2dResource.getS("Le nombre de points pour une ar�te est incorrect") + " ("
              + CtuluLibString.getString(nna) + ")", in_.getLineNumber());
          return null;
        }
        // on affecte les indices
        final int seg1 = in_.intField(1) - 1;
        final int seg2 = in_.intField(2) - 1;
        // lecture de nrefa,neva,ieva
        in_.readFields(fmt);
        final int nrefa = in_.intField(0);
        H2dRubarBoundaryType type = null;
        if (v_.isExternIdx(nrefa)) {
          type = v_.getExternType(nrefa);
          if (type == null) {
            analyze_.addFatalError(H2dResource.getS("Code inconnu") + CtuluLibString.getEspaceString(nrefa));
            return null;
          }
        }
        if ((seg1 < 0) || (seg2 < 0)) {
          if (areteWithError == null) {
            areteWithError = new TIntArrayList(50);
          }
          areteWithError.add(ia + 1);
        }
        segment[ia] = new H2dRubarArete(seg1, seg2, type);
        final int neva = in_.intField(1);
        tmpOnLine = 2;
        iaNeva[ia] = new int[neva];
        for (int je = 0; je < neva; je++) {
          if (tmpOnLine == nbFieldByLine) {
            in_.readFields(fmt);
            tmpOnLine = 0;
          }
          iaNeva[ia][je] = in_.intField(tmpOnLine) - 1;
          tmpOnLine++;
        }
      }
      if (neWithErreur != null) {
        analyze_.addError(DodicoLib.getS("Des �l�ments sont mal d�finis: indice d'un noeud �gal � z�ro"), -1);
        CtuluLibMessage.info("El�ments mal d�finis" + CtuluLibString.LINE_SEP
            + CtuluLibString.arrayToString(neWithErreur.toNativeArray()));
      }
      if (areteWithError != null) {
        analyze_.addError(DodicoLib.getS("Des ar�tes sont mal d�finies: indice d'un noeud �gal � z�ro"), -1);
        CtuluLibMessage.info("Ar�tes mal d�finies" + CtuluLibString.LINE_SEP
            + CtuluLibString.arrayToString(areteWithError.toNativeArray()));
      }
      if (progress_ != null) {
        progress_.setProgression(50);
      }
      // LECTURE DES POINTS
      // nn
      in_.readFields();
      final int nn = in_.intField(0);
      fmt = new int[10];
      if (newFormat) {
        Arrays.fill(fmt, 13);
      } else {
        Arrays.fill(fmt, 8);
      }
      nbFieldByLine = fmt.length;
      int indiceToUseForZeroIdx = -1;
      int size = nn;
      if (areteWithError != null) {
        indiceToUseForZeroIdx = size;
        size++;
      }
      final EfNode[] nds = new EfNode[size];
      final double[] x = new double[size];
      final double[] y = new double[size];
      final double[] z = new double[size];
      if (indiceToUseForZeroIdx > 0) {
        nds[indiceToUseForZeroIdx] = new EfNode(0, 0, 0);
      }
      // xn
      in_.readFields(fmt);
      tmpOnLine = 0;
      // le nombre de decimales utilisees pour les x,y. Utile pour la r��criture
      int nbDecimal = -1;
      for (int i = 0; i < nn; i++) {
        if (tmpOnLine == nbFieldByLine) {
          in_.readFields(fmt);
          tmpOnLine = 0;
        }
        if (nbDecimal < 0) {
          final String l = in_.stringField(tmpOnLine);
          int idx = l.indexOf('.');
          // normalement inutile car le point est toujours utilise
          if (idx < 0) {
            idx = l.indexOf(',');
          }
          // les indices commencent a 0 ... d'ou le -1
          nbDecimal = l.length() - idx - 1;
        }
        x[i] = in_.doubleField(tmpOnLine);
        tmpOnLine++;
      }
      // yn
      in_.readFields(fmt);
      tmpOnLine = 0;
      for (int i = 0; i < nn; i++) {
        if (tmpOnLine == nbFieldByLine) {
          in_.readFields(fmt);
          tmpOnLine = 0;
        }
        y[i] = in_.doubleField(tmpOnLine);
        tmpOnLine++;
      }
      // zn
      //zn is always on 8 digits
      Arrays.fill(fmt, 8);
      in_.readFields(fmt);
      tmpOnLine = 0;
      for (int i = 0; i < nn; i++) {
        if (tmpOnLine == nbFieldByLine) {
          in_.readFields(fmt);
          tmpOnLine = 0;
        }
        z[i] = in_.doubleField(tmpOnLine);
        tmpOnLine++;
      }
      // affectation des valeurs dans les noeuds
      for (int i = 0; i < nn; i++) {
        nds[i] = new EfNode(x[i], y[i], z[i]);
      }
      if (areteWithError != null) {
        final int nb = areteWithError.size();
        for (int i = 0; i < nb; i++) {
          final int idx = areteWithError.get(i);
          final H2dRubarArete ari = segment[idx];
          int i1 = ari.getPt1Idx();
          int i2 = ari.getPt2Idx();
          if (i1 < 0) {
            i1 = indiceToUseForZeroIdx;
          }
          if (i2 < 0) {
            i2 = indiceToUseForZeroIdx;
          }
          segment[idx] = new H2dRubarArete(i1, i2, ari.getType());
        }
      }
      if (neWithErreur != null) {
        final int nb = neWithErreur.size();
        for (int i = 0; i < nb; i++) {
          final int idx = neWithErreur.get(i);
          final EfElementVolume v = elt[idx];
          final int[] newPt = new int[v.getPtNb()];
          for (int ptI = newPt.length - 1; ptI >= 0; ptI--) {
            int index = v.getPtIndex(ptI);
            if (index < 0) {
              index = indiceToUseForZeroIdx;
            }
            newPt[ptI] = index;
          }
          elt[idx] = new EfElementVolume(v.getAreteIdxs(), newPt);
        }
      }
      if (progress_ != null) {
        progress_.setProgression(70);
      }
      // LECTURE NAS
      in_.readFields();
      final int nas = in_.intField(0);

      final int[] idxAreteSortant = new int[nas];
      if (nas > 0) {
        // LECTURE DES IAC POUR VERIF UNIQUEMENT
        fmt = new int[13];
        Arrays.fill(fmt, newFormat ? 9 : 6);
        nbFieldByLine = fmt.length;
        in_.readFields(fmt);
        tmpOnLine = 0;
        for (int i = 0; i < nas; i++) {
          if (tmpOnLine == nbFieldByLine) {
            in_.readFields(fmt);
            tmpOnLine = 0;
          }
          final int idx = in_.intField(tmpOnLine) - 1;
          if (!segment[idx].isExternAndLiquid()) {
            analyze_.addFatalError(H2dResource.getS("L'ar�te {0} n'est pas une ar�te limite", CtuluLibString
                .getString(idx + 1)));
            return null;
          }
          idxAreteSortant[i] = idx;
          tmpOnLine++;
        }
      }
      r = new H2dRubarGridAreteDefault(new H2dRubarGrid(nds, elt, segment, typeElt), ieNeve, iaNeva);
      r.setNbDecimal(nbDecimal);
      r.setIdxAreteLimiteEntrante(idxAreteSortant);
    } catch (final NumberFormatException e) {
      e.printStackTrace();
      analyze_.manageException(e, in_.getLineNumber());
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return r;
  }
}
