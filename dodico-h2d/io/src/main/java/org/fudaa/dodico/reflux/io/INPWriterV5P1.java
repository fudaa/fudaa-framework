/*
 * @creation 21 mars 2003
 * @modification $Date: 2007-06-29 15:10:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntObjectHashMap;

import java.io.IOException;

import org.fudaa.ctulu.CtuluLibArray;

import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.type.H2dBcType;

/**
 * @author deniger
 * @version $Id: INPWriterV5P1.java,v 1.1 2007-06-29 15:10:27 deniger Exp $
 */
public class INPWriterV5P1 extends INPWriterAbstract {

  /**
   * version par defaut.
   */
  public INPWriterV5P1() {
    this(INPFileFormat.getInstance().getLastV5P1Impl());
  }

  /**
   * @param _f version a ajouter.
   */
  public INPWriterV5P1(final RefluxINPV5P1Version _f) {
    super(_f);
  }

  @Override
  protected void finishEltBordLine(int _idxBordEle, H2dRefluxBordIndexGeneral _bd, int _tailleElem) {
    int idxTotal = grid_.getEltNb() + _idxBordEle;
    out_.intField(0, idxTotal + 1);
    for (int i = _bd.getIndex().length + 1; i <= _tailleElem; i++) {
      out_.intField(i, 0);
    }
    out_.intField(_tailleElem + 1, version_.getBordTypeId(_bd.getBordType()));
    out_.intField(_tailleElem + 2, grpIdx_.get(idxTotal) + 1);

  }

  @Override
  protected void finishEltLine(int _idxEle, int _tailleElem) {
    out_.intField(0, _idxEle + 1);
    out_.intField(_tailleElem + 1, version_.getCodeFond());
    out_.intField(_tailleElem + 2, grpIdx_.get(_idxEle) + 1);

  }

  @Override
  protected void writeBlocCoor() throws IOException {
    up_.majProgessionStateOnly(version_.getCOOR());
    // 5=POIN
    out_.stringField(0, version_.getCOOR());
    // A15= Le type du probl�me de r�f�rence �ventuel, au format A15
    out_.stringField(1, inter_.getTypeProjet().getRefluxId());
    // I8 Le nombre de dimensions du probl�me NDIM, au format I8
    out_.intField(2, 3);
    // I8 Le nombre de noeuds total NNT, au format I8
    out_.intField(3, nbPts_);
    // I8 Le nombre maximum de degr�s de libert� par noeud, au format I8
    out_.intField(4, 3);
    // I8 Le nombre de propri�t�s par noeuds, au format I8
    // normale +debit+autre
    H2dNodalPropertyMixte[] nodals = inter_.getPropNodales();
    // il y a toujours les pr�cipitations
    if (nodals == null || nodals.length == 0) {
      nodals = new H2dNodalPropertyMixte[1];
      nodals[0] = new H2dNodalPropertyMixte("precipitation");
    }
    out_.intField(5, 2 + nodals.length);
    out_.writeFields(version_.getCOORFormat());

    up_.setValue(10, nbPts_);
    final int freeCode = version_.getCode(H2dBcType.LIBRE);
    final int permCode = version_.getCode(H2dBcType.PERMANENT);
    final int transCode = version_.getCode(H2dBcType.TRANSITOIRE);

    for (int i = 0; i < nbPts_; i++) {
      int idxDeb = 5;
      out_.intField(0, i + 1);
      out_.doubleField(1, grid_.getPtX(i));
      out_.doubleField(2, grid_.getPtY(i));
      out_.doubleField(3, grid_.getPtZ(i));
      out_.intField(4, grid_.isMiddlePoint(i) ? 2 : 3);
      H2dRefluxBoundaryCondition cl = inter_.getConditionLimite(i);
      // la normale
      out_.intField(idxDeb, cl == null ? freeCode : permCode);
      out_.doubleField(idxDeb + 1, cl == null ? 0 : cl.getNormale());
      // le debit
      int code = freeCode;
      double val = 0;
      if (cl != null) {
        if (cl.getQType() == H2dBcType.PERMANENT) {
          code = permCode;
          val = cl.getQ();
        } else if (cl.getQType() == H2dBcType.TRANSITOIRE) {
          super.pnCourbes_.add(cl.getQTransitoireCourbe());
          code = transCode;

        }
      }
      out_.intField(idxDeb + 2, code);
      out_.doubleField(idxDeb + 3, val);
      idxDeb = idxDeb + 4;
      for (int j = 0; j < nodals.length; j++) {
        final H2dNodalPropertyMixte prop = nodals[j];
        code = freeCode;
        val = 0;
        H2dBcType type = prop.getTypeForEltIdx(i);
        if (type == H2dBcType.PERMANENT) {
          code = permCode;
          val = prop.getPermanentValueFor(i);
        } else if (type == H2dBcType.TRANSITOIRE) {
          super.pnCourbes_.add(prop.getTransitoireEvol(i));
          code = transCode;
        }
        out_.intField(idxDeb + (j * 2), code);
        out_.doubleField(idxDeb + (j * 2 + 1), val);

      }

      out_.writeFields();
      up_.majAvancement();
    }
    out_.println();
  }

  @Override
  protected void writeBlocData() throws IOException {
    out_.writeln(version_.getDATA());
    // on force la version 1 pour le fichier de r�sultat.
    out_.intField(2, 1);
    out_.intField(1, 1);
    out_.stringField(0, inter_.getTypeProjet().getRefluxId());
    out_.writeFields(version_.getTypeProjetFormat());
    out_.println();
  }

  @Override
  protected void writeBlocDLPN() throws IOException {}

  @Override
  protected void writeBlocElemEntete() throws IOException {
    out_.stringField(0, version_.getELEM());
    out_.intField(2, grid_.getEltNb() + CtuluLibArray.getNbItem(inter_.getBords()));
    out_.intField(3, 6);
    out_.intField(4, getNbGroupWithViscosity());
    out_.intField(5, inter_.getPropElementaires().length);
    out_.writeFields(version_.getELEMFormat());
  }

  @Override
  protected void writeBlocPRELEntete() throws IOException {}

  @Override
  protected void writeBlocPRELIdxs() throws IOException {}

  @Override
  protected void writeBlocPRND(TIntObjectHashMap _globIdxBc) throws IOException {}

  @Override
  protected void writeBlocTPEL() throws IOException {}
}