/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarFROFileFormat.java,v 1.12 2006-11-15 09:22:53 deniger Exp $
 */
public final class RubarFROFileFormat extends RubarDonneesBrutesFileFormat {

  private static RubarFROFileFormat instance_;

  @Override
  public boolean isNuage() {
    return false;
  }

  /**
   * @return l'instance a utiliser
   */
  public static RubarFROFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarFROFileFormat();
    }
    return instance_;
  }

  /**
   * 
   */
  public RubarFROFileFormat() {
    super();
    extensions_ = new String[] { "fro" };
    super.description_ = H2dResource.getS("Frottement sur grille r�guli�re");
    super.id_ = "RUBAR_FRO";
    super.nom_ = "Rubar fro";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarVF2MReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarVF2MWriter(false);
  }


}