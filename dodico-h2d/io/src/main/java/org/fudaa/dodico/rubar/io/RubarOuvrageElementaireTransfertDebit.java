/**
 * @creation 22 d�c. 2004 @modification $Date: 2006-10-24 12:48:38 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireTransfertDebitI;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: RubarOuvrageElementaireTransfertDebit.java,v 1.4 2006-10-24 12:48:38 deniger Exp $
 */
public class RubarOuvrageElementaireTransfertDebit implements H2dRubarOuvrageElementaireTransfertDebitI {
  LinkedHashMap<H2dVariableType, EvolutionReguliere> evolutionsByType;

  @Override
  public EvolutionReguliereAbstract getEvol(H2dVariableType v) {
    if (evolutionsByType == null) {
      return null;
    }
    return evolutionsByType.get(v);
  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {
  }

  @Override
  public void fillWithTransientCurves(String prefixe, H2dEvolutionVariableMap r) {

  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {
    if (evolutionsByType != null) {
      for (Map.Entry<H2dVariableType, EvolutionReguliere> entry : evolutionsByType.entrySet()) {
        entry.getValue().setNom(prefix + " / " + getType());
        allTarage.add(entry.getValue());
      }
    }
  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    if (evolutionsByType != null) {
      for (EvolutionReguliere entry : evolutionsByType.values()) {
        if (entry == tarage) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return null;
  }

  @Override
  public LinkedHashMap<H2dVariableType, ? extends EvolutionReguliereAbstract> getEvols() {
    if (evolutionsByType == null) {
      return new LinkedHashMap<>();
    }
    return new LinkedHashMap<>(evolutionsByType);
  }

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.TRANSFERT_DEBIT_TARAGE;
  }
}
