/*
 * @creation     11 Mars 2009
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Un file format pour les fichier Rubar Casiers
 * @author Bertrand Marchand
 * @version $Id$
 */
public class RubarCasierFileFormat extends FileFormatUnique {

  public RubarCasierFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Cemagref casiers");
    description_ = H2dResource.getS("Fichier contenant des casiers");
    extensions_=new String[]{"mage"};
  }
  static final RubarCasierFileFormat INSTANCE = new RubarCasierFileFormat();

  /**
   * @return singleton
   */
  public static RubarCasierFileFormat getInstance() {
    return INSTANCE;
  }
  
  @Override
  public FileReadOperationAbstract createReader() {
    return null;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarCasierWriter();
  }

}
