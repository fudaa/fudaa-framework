/**
 * @creation 1 juin 2004
 * @modification $Date: 2006-11-14 09:05:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.ctulu.CtuluAnalyze;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryHouleType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * @author Fred Deniger
 * @version $Id: TelemacCLSubief2DVersion.java,v 1.13 2006-11-14 09:05:28 deniger Exp $
 */
public class TelemacCLSubief2DVersion extends TelemacCLTomawacVersion {

  /**
   * @param _v
   */
  public TelemacCLSubief2DVersion(final String _v) {
    super(_v);
  }

  @Override
  public void getFileLineFromH2dData(final CtuluAnalyze _r, final TelemacCLLine _target,
      final H2dTelemacCLElementSource _source, final int _indexFrontiere) {
    initDefaultData(_target, _source, _indexFrontiere);
    final H2dBoundaryType t = _source.bordType_;
    int r = -1;
    if (t.equals(H2dBoundaryHouleType.FRONTIERE_LIBRE)) {
      r = getCodeLibre();
    } else if (t.equals(H2dBoundaryHouleType.FRONTIERE_VALEURS_IMPOSEES)) {
      r = getCodeImpose();
    } else if (t.equals(H2dBoundaryTypeCommon.SOLIDE)) {
      r = getCodeSolide();
    } else {
      if (_r != null) {
        _r.addError(H2dResource.getS("Le type de bord est incompatible avec le projet"), _indexFrontiere);
      }
      r = getCodeLibre();
    }
    _target.litbor_ = r;

  }

  @Override
  public boolean getH2dDataFromFile(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target,
      final TelemacCLLine _l, final int _indicesFrontieres) {
    initH2dData(_target, _l);
    _target.tracerType_ = H2dBcType.LIBRE;
    final int id = _l.litbor_;
    if (id == getCodeLibre()) {
      _target.bordType_ = H2dBoundaryHouleType.FRONTIERE_LIBRE;
    } else if (id == getCodeImpose()) {
      _target.bordType_ = H2dBoundaryHouleType.FRONTIERE_VALEURS_IMPOSEES;
    } else if (id == getCodeSolide()) {
      _target.bordType_ = H2dBoundaryTypeCommon.SOLIDE;
    } else {
      if (_r != null) {
        _r.addFatalError(H2dResource.getS("Codes inconnus"));
      }
      return false;
    }
    return true;
  }

  @Override
  public boolean isCodeHauteur(final int _c) {
    return true;
  }

  @Override
  public boolean isCodeTraceur(final int _c) {
    return ((_c == 4) || (_c == 5) || (_c == 2));
  }
}