/**
 *  @creation     15 mars 2004
 *  @modification $Date: 2007-06-29 15:10:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TDoubleArrayList;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.fortran.FortranBinaryInputStream;

/**
 * @author Fred Deniger
 * @version $Id: RefluxSolutionInitReader.java,v 1.13 2007-06-29 15:10:25 deniger Exp $
 */
public class RefluxSolutionInitReader extends FileReadOperationAbstract {

  FortranBinaryInputStream in_;
  int nbDouble_;

  /**
   * Constructeur par defaut.
   */
  public RefluxSolutionInitReader() {}

  /**
   * @see org.fudaa.ctulu.fileformat.FileReadOperationAbstract#internalRead()
   */
  @Override
  protected Object internalRead() {
    return readList();
  }

  protected double[] readList() {
    if (in_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Input Stream not found"));
      return null;
    }
    final TDoubleArrayList r = new TDoubleArrayList(nbDouble_ + 1);
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbDouble_);
    try {
      in_.readRecord();
      final long n = in_.getEndOfRecord();
      while (n > in_.getCurrentPosition()) {
        r.add(in_.readDoublePrecision());
        up.majAvancement();
      }
    } catch (final EOFException _e) {

    } catch (final IOException _e) {
      analyze_.manageException(_e);
      return null;
    }
    return r.toNativeArray();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileOperationAbstract#setFile(java.io.File)
   */
  @Override
  public void setFile(final File _f) {
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f.getAbsolutePath());
    try {
      in_ = new FortranBinaryInputStream(new FileInputStream(_f), true);
    } catch (final FileNotFoundException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    nbDouble_ = (int) (_f.length() - 8) / 8;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileOperationAbstract#getFortranInterface()
   */
  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (in_ != null) {
          in_.close();
        }
      }
    };
  }

  /**
   * Lit le fichier si, attention : les niveaux d'eau sont gard�s tels quels.
   * 
   * @param _g le maillage associ�
   * @param _f le fichier siv a lire
   * @param _prog la barre de progression
   * @return la synthese contenant une instance de H2dRefluxSICourant2D
   */
  public static CtuluIOOperationSynthese loadSI(final EfGridInterface _g, final File _f,
      final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese init = RefluxSolutionInitFileFormat.getInstance().getLastVersionInstance(_f).read(
        _f, _prog);
    if (init.containsSevereError()) {
      init.setSource(null);
      return init;
    }
    final double[] vals = (double[]) init.getSource();
    final double[][] si = new double[3][_g.getPtsNb()];
    int idxDone = 0;
    final int nbPt = _g.getPtsNb();
    int temp;
    if (_prog != null) {
      _prog.setProgression(30);
    }
    int idxInVal = 0;
    idxDone = 0;
    temp = vals.length;
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(3, nbPt, 30, 60);
    up.majProgessionStateOnly();
    while (idxInVal < temp) {
      si[0][idxDone] = vals[idxInVal++];
      si[1][idxDone] = vals[idxInVal++];
      if (!_g.isMiddlePoint(idxDone)) {
        si[2][idxDone] = vals[idxInVal++];
      }
      up.majAvancement();
      idxDone++;
    }
    for (int i = _g.getEltNb() - 1; i >= 0; i--) {
      final EfElement el = _g.getElement(i);
      final int nbPtElt = el.getPtNb();
      for (int j = nbPtElt - 1; j >= 1; j -= 2) {
        int upIdx = j + 1;
        if (upIdx == nbPtElt) {
          upIdx = 0;
        }
        final int idxToMod = el.getPtIndex(j);
        si[2][idxToMod] = (si[2][el.getPtIndex(upIdx)] + si[2][el.getPtIndex(j - 1)]) / 2;
      }

    }
    if (_prog != null) {
      _prog.setProgression(95);
    }
    init.setSource(new RefluxSolutionInitAdapter(si[0], si[1], si[2]));
    return init;
  }
}
