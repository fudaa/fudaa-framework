/**
 * @creation 22 d�c. 2004 @modification $Date: 2007-05-04 13:47:30 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrage;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageCompositeTypeControle;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

import java.io.EOFException;
import java.io.IOException;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: RubarOUVReader.java,v 1.13 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarOUVReader extends FileOpReadCharSimpleAbstract {
  protected static final int[] FMT_BRECHE = new int[]{1, 1, 8, 10, 10, 10, 10};
  protected static final int[] FMT_BRECHE_LINE4 = new int[]{6, 6, 2, 4};
  protected static final int[] FMT_BRECHE_LINE3 = new int[]{10, 10, 10};
  protected static final int[] FMT_BRECHE_LINE2 = new int[]{10, 10, 10, 10};
  protected static final int[] FMT_COMPOSITE = new int[]{1, 1, 8, 10, 10, 10, 10};
  protected static final int[] FMT_COMPOSITE_NEW = new int[]{1, 1, 8, 13, 13, 13, 13};
  protected static final int[] FMT_COMPOSITE_VALEURS = new int[]{15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15};
  protected static final int[] FMT_DEVERSOIR = new int[]{1, 9, 10, 10, 10, 10};
  protected static final int[] FMT_DEVERSOIR_HYDRAULIQUE = new int[]{1, 9, 10, 10, 10, 10};
  protected static final int[] FMT_ORIFICE_CIRCULAIRE = new int[]{1, 9, 10, 10, 10, 10};
  private static final Map<Character, int[]> FORMATS = new HashMap<>();

  static {
    FORMATS.put('B', FMT_BRECHE);
    FORMATS.put('C', FMT_COMPOSITE);
    FORMATS.put('D', FMT_DEVERSOIR);
    FORMATS.put('H', FMT_DEVERSOIR_HYDRAULIQUE);
    FORMATS.put('O', FMT_ORIFICE_CIRCULAIRE);
    FORMATS.put('Q', new int[]{1, 9, 10, 10, 10, 10});
    FORMATS.put('Z', new int[]{1, 9, 10, 10, 10, 10});
  }

  private boolean newFormat;

  public boolean isNewFormat() {
    return newFormat;
  }

  @Override
  protected Object internalRead() {

    final List ouv = new ArrayList();
    in_.setBlankZero(true);
    in_.setJumpBlankLine(true);
    final RubarOuvrageContainer r = new RubarOuvrageContainer();
    r.ouvrage_ = ouv;
    try {
      while (true) {
        final RubarOuvrage o = new RubarOuvrage();
        in_.readFields();
        if (in_.getNumberOfFields() != 6) {
          analyze_.addWarn(H2dResource.getS("Le fichier {0} est corrompu � la ligne {1}", CtuluLibString.EMPTY_STRING, CtuluLibString.getString(in_.getLineNumber())),
              in_.getLineNumber());
          analyze_.addWarn(H2dResource.getS("{0} ouvrage(s) lu(s)", CtuluLibString.getString(ouv.size())),
              in_.getLineNumber());
          return r;
        }
        readE1(o);
        final int nbInternElt = in_.intField(5);
        if (nbInternElt >= 0) {
          o.xEltIntern_ = new double[nbInternElt];
          o.yEltIntern_ = new double[nbInternElt];
          for (int i = 0; i < nbInternElt; i++) {
            in_.readFields();
            o.xEltIntern_[i] = in_.doubleField(0);
            o.yEltIntern_[i] = in_.doubleField(1);
          }
        }
        in_.readFields();
        int nbOuvElement = 0;
        if (in_.getNumberOfFields() > 0) {
          if (in_.getNumberOfFields() == 5) {
            nbOuvElement = readE2(o);
          } else {
            o.noArete2_ = true;
            nbOuvElement = in_.intField(in_.getNumberOfFields() - 1);
          }
        }
        if (nbOuvElement >= 0) {
          o.ouvElement_ = new H2dRubarOuvrageElementaireInterface[nbOuvElement];
          boolean isUnknown = false;
          for (int i = 0; i < nbOuvElement; i++) {
            final String line = readOuvElementaire(o.ouvElement_, i);
            if (o.ouvElement_[i] == null) {
              final String type = (line == null || line.length() == 0) ? "?" : Character.toString(line.charAt(0));
              analyze_.addWarn(
                  H2dResource.getS("Ouvrage {0}", CtuluLibString.getString(ouv.size() + 1)) + ": "
                      + H2dResource.getS("Le type d'ouvrage '{0}' est inconnu", type), in_.getLineNumber());
              isUnknown = true;
              break;
            }
          }
          if (isUnknown) {
            continue;
          }
        }

        ouv.add(o);
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug(getClass().getName() + " end of file");
      }
    } catch (final NumberFormatException e) {
      analyze_.addFatalError(H2dResource.getS("Ouvrage {0}", CtuluLibString.getString(ouv.size() + 1)) + ": " + CtuluLib
          .getS("Erreur de lecture � la ligne {0}", Integer.toString(in_.getLineNumber())), in_.getLineNumber());
      return null;
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return r;
  }

  private String readOuvElementaire(final H2dRubarOuvrageElementaireInterface[] ouvElements, final int _i) throws IOException {
    final char type = in_.readLine().trim().toUpperCase().charAt(0);
    int[] format = FORMATS.get(type);

    if (format != null) {
      if (type == 'C') {
        if (in_.getLine().length() > CtuluLibArray.getSum(format)) {
          newFormat = true;
          format = FMT_COMPOSITE_NEW;
        }
      }
      in_.analyzeCurrentLine(format);
      switch (type) {
        case 'B': {
          ouvElements[_i] = readB();
          break;
        }
        case 'C': {
          ouvElements[_i] = readC();
          break;
        }
        case 'D': {
          ouvElements[_i] = readD();
          break;
        }
        case 'H': {
          ouvElements[_i] = readH();
          break;
        }
        case 'O': {
          ouvElements[_i] = readO();
          break;
        }
        case 'Q': {
          ouvElements[_i] = readQ();
          break;
        }
        case 'Z': {
          ouvElements[_i] = readZ();
          break;
        }
      }
    }

    return Character.toString(type);
  }

  private int readE2(final RubarOuvrage rubarOuvrage) {
    int nbOuvElement = 0;
    String first = in_.stringField(0);
    if (!CtuluLibString.isNumeric(first)) {
      return nbOuvElement;
    }
    rubarOuvrage.xa2_ = in_.doubleField(0);
    rubarOuvrage.ya2_ = in_.doubleField(1);
    rubarOuvrage.xe2_ = in_.doubleField(2);
    rubarOuvrage.ye2_ = in_.doubleField(3);
    nbOuvElement = in_.intField(4);
    return nbOuvElement;
  }

  private void readE1(final RubarOuvrage rubarOuvrage) {
    rubarOuvrage.xa1_ = in_.doubleField(0);
    rubarOuvrage.ya1_ = in_.doubleField(1);
    rubarOuvrage.xe1_ = in_.doubleField(2);
    rubarOuvrage.ye1_ = in_.doubleField(3);
    rubarOuvrage.rubarRef_ = in_.intField(4);
  }

  static int[] getFirstLineFmt() {
    return new int[]{1, 9, 10, 10, 10, 10};
  }

  private H2dRubarOuvrageElementaireInterface readD() {
    final RubarOuvrageElementaireDeversoir r = new RubarOuvrageElementaireDeversoir();
    r.longDeversement_ = in_.doubleField(2);
    r.coteSeuilZd_ = in_.doubleField(3);
    r.coteMisEnchargeZm_ = in_.doubleField(4);
    r.coefficientDebit_ = in_.doubleField(5);
    return r;
  }

  private H2dRubarOuvrageElementaireInterface readH() {
    final RubarOuvrageElementaireDeversoirHydraulique r = new RubarOuvrageElementaireDeversoirHydraulique();
    r.longDeversement_ = in_.doubleField(2);
    r.coteSeuilZd_ = in_.doubleField(3);
    r.coteMisEnchargeZm_ = in_.doubleField(4);
    r.coefficientDebit_ = in_.doubleField(5);
    return r;
  }

  private H2dRubarOuvrageElementaireInterface readC() throws IOException {
    final RubarOuvrageElementaireComposite r = new RubarOuvrageElementaireComposite();
    String readControl = in_.stringField(1);
    r.typeControle = H2dRubarOuvrageCompositeTypeControle.ELEVATION.getRubarId().equals(readControl) ? H2dRubarOuvrageCompositeTypeControle.ELEVATION
        : H2dRubarOuvrageCompositeTypeControle.TIME;
    r.xAmont = in_.doubleField(3);
    r.yAmont = in_.doubleField(4);
    r.xAval = in_.doubleField(5);
    r.yAval = in_.doubleField(6);

    int nbVal = in_.intField(2);
    r.valeurs = new double[nbVal];
    r.ouvrages = new H2dRubarOuvrageElementaireInterface[nbVal + 1];

    if (nbVal > 0) {
      in_.readFields(FMT_COMPOSITE_VALEURS);

      for (int i = 0; i < nbVal; i++) {
        r.valeurs[i] = in_.doubleField(i);
      }
    }

    for (int i = 0; i < nbVal + 1; i++) {
      readOuvElementaire(r.ouvrages, i);
    }

    return r;
  }

  private H2dRubarOuvrageElementaireInterface readO() {
    final RubarOuvrageElementaireOrificeCirculaire r = new RubarOuvrageElementaireOrificeCirculaire();
    r.longDeversement_ = in_.doubleField(2);
    r.coteSeuilZd_ = in_.doubleField(3);
    r.diametre_ = in_.doubleField(4);
    r.coefficientDebit_ = in_.doubleField(5);
    return r;
  }

  private H2dRubarOuvrageElementaireInterface readZ() throws IOException {
    final RubarOuvrageElementaireTransfertDebit r = new RubarOuvrageElementaireTransfertDebit();
    readApportTransfert(r, H2dRubarOuvrage.getTarageVariables(), false);
    return r;
  }

  private H2dRubarOuvrageElementaireInterface readApportTransfert(final RubarOuvrageElementaireTransfertDebit ouvrageElementaireTransfertDebit,
                                                                  List<H2dVariableType> vars, boolean useTransportBlock) throws IOException {
    final int nbCourbe = in_.intField(1);
    ouvrageElementaireTransfertDebit.evolutionsByType = new LinkedHashMap<>();
    EvolutionReguliere[] evolutions = null;
    List<H2dVariableType> varsToUse = vars;
    try {
      for (int i = 0; i < nbCourbe; i++) {
        in_.readFields();
        int nbVarRead = in_.getNumberOfFields() - 1;
        if (i == 0) {
          if (useTransportBlock) {
            varsToUse = getH2dVariableTypesTransportBlocks(nbVarRead);
          }
          int nbEvolution = Math.min(nbVarRead, varsToUse.size());
          evolutions = new EvolutionReguliere[nbEvolution];
          for (int j = 0; j < evolutions.length; j++) {
            evolutions[j] = new EvolutionReguliere(nbCourbe);
            final H2dVariableType variable = varsToUse.get(j);
            final EvolutionReguliere evolution = evolutions[j];
            ouvrageElementaireTransfertDebit.evolutionsByType.put(variable, evolution);
          }
        }
        double t = in_.doubleField(0);
        if (in_.getNumberOfFields() != 1 + evolutions.length) {
          analyze_.addWarn(H2dResource.getS("Le format de la ligne {0} n'est pas correct. {1} colonnes sont attendues",
              Integer.toString(in_.getLineNumber()),
              Integer.toString(1 + evolutions.length)), in_.getLineNumber());
          return ouvrageElementaireTransfertDebit;
        }
        for (int j = 0; j < evolutions.length; j++) {
          evolutions[j].add(t, in_.doubleField(j + 1));
        }
      }
    } catch (EOFException eofException) {
      analyze_.addWarn(H2dResource.getS("Le fichier a �t� lu mais le nombre de lignes du fichier ne correspond pas aux donn�es"));
    }
    return ouvrageElementaireTransfertDebit;
  }

  protected static List<H2dVariableType> getH2dVariableTypesTransportBlocks(int nbVarRead) {
    int nbTransportBlock = (nbVarRead - H2dRubarOuvrage.getNbVariableNonTransport()) / H2dRubarOuvrage.getNbVariableTransport();
    return H2dRubarOuvrage.getQVariables(nbTransportBlock);
  }

  private H2dRubarOuvrageElementaireInterface readQ() throws IOException {
    final RubarOuvrageElementaireApportDebit r = new RubarOuvrageElementaireApportDebit();
    //use null for variables because the variables will be recompute.
    readApportTransfert(r, null, true);
    return r;
  }

  private H2dRubarOuvrageElementaireInterface readB() throws IOException {
    final RubarOuvrageElementaireBreche r = new RubarOuvrageElementaireBreche();

    if (in_.stringField(1).trim().equals("")) {
      r.brecheType = 0;
    } else {
      r.brecheType = in_.intField(1);
    }
    if (in_.stringField(2).trim().equals("")) {
      r.debutTemps = -1;
    } else {
      r.debutTemps = in_.doubleField(2);
    }
    r.coteCrete = in_.doubleField(3);
    r.cotePied = in_.doubleField(4);
    r.largeurCrete = in_.doubleField(5);
    r.largeurPied = in_.doubleField(6);

    in_.readFields(FMT_BRECHE_LINE2);

    r.diametreMedian = in_.doubleField(0);
    r.coeffStrickler = in_.doubleField(1);
    r.masseVolumiqueGrain = in_.doubleField(2);
    r.porosite = in_.doubleField(3);

    in_.readFields(FMT_BRECHE_LINE3);

    r.coteFond = in_.doubleField(0);
    r.dimensionInitiale = in_.doubleField(1);
    if (in_.getNumberOfFields() > 2) {
      r.coefficientErosionLineaire = in_.doubleField(2);
    }

    in_.readFields(FMT_BRECHE_LINE4);

    r.pasDeTemps = in_.doubleField(0);
    r.coeffPerteDeCharge = in_.doubleField(1);
    r.indicateur = in_.intField(2);
    r.nbPasDeTempsMax = in_.intField(3);

    return r;
  }
}
