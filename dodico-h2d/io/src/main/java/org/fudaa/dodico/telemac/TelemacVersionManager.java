/**
 * @creation 11 janv. 2005
 * @modification $Date: 2006-09-19 14:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.commun.DodicoPreferences;
import org.fudaa.dodico.dico.DicoVersionManager;

import javax.swing.*;
import java.io.File;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: TelemacVersionManager.java,v 1.5 2006-09-19 14:45:58 deniger Exp $
 */
public final class TelemacVersionManager implements DicoVersionManager {
  private String[] versionId_;
  private String[] versionPath_;
  public static TelemacVersionManager INSTANCE = new TelemacVersionManager();

  private TelemacVersionManager() {
    loadProperties();
  }

  /**
   * @return le nombre de version definie
   */
  @Override
  public int getNbVersion() {
    return versionId_ == null ? 0 : versionId_.length;
  }

  /**
   * @param _i l'indice de la version
   * @return l'id de la version
   */
  @Override
  public String getVersion(final int _i) {
    return versionId_ == null ? null : versionId_[_i];
  }

  /**
   * @param _i l'indice de la version
   * @return le path de la version
   */
  @Override
  public String getVersionPath(final int _i) {
    return versionPath_ == null ? null : versionPath_[_i];
  }

  private String getVersionKey() {
    return "install.telemac.exe.version";
  }

  private String getPathKey(final String _version) {
    return "install.telemac." + _version + ".exe.dir";
  }

  protected void cleanProperties() {
    final List keys = new ArrayList();
    for (final Enumeration e = DodicoPreferences.DODICO.keys(); e.hasMoreElements(); ) {
      final String key = (String) e.nextElement();
      if (key.startsWith("install.telemac.")) {
        keys.add(key);
      }
    }
    if (keys.size() > 0) {
      for (int i = keys.size() - 1; i >= 0; i--) {
        DodicoPreferences.DODICO.removeProperty((String) keys.get(i));
      }
    }
  }

  /**
   * @return la liste des versions utilisees
   */
  @Override
  public String[] getVersionId() {
    return CtuluLibArray.copy(versionId_);
  }

  /**
   * @return la liste des path utilises
   */
  @Override
  public String[] getVersionPath() {
    return CtuluLibArray.copy(versionPath_);
  }

  /**
   * Ecrase les anciennes versions.
   * @param _m les nouvelles versions
   */
  public void setVersion(final Map _m) {
    versionId_ = null;
    versionPath_ = null;
    //pour avoir les cl�s dans l'ordre
    if (_m == null || _m.size() == 0) {
      return;
    }
    final TreeMap map = new TreeMap(_m);
    versionId_ = new String[map.size()];
    versionPath_ = new String[versionId_.length];
    int idx = 0;
    for (final Iterator it = map.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry e = (Map.Entry) it.next();
      versionId_[idx] = cleanVersion((String) e.getKey());
      versionPath_[idx++] = (String) e.getValue();
    }
  }

  @Override
  public ComboBoxModel getVersionModel() {
    return new VersionComboBoxModel(this);
  }

  private static class VersionComboBoxModel extends AbstractListModel implements ComboBoxModel {
    final TelemacVersionManager mng_;
    Object selected_;

    public VersionComboBoxModel(final TelemacVersionManager _mng) {
      super();
      mng_ = _mng;
    }

    @Override
    public Object getElementAt(final int _index) {
      return mng_.getVersion(_index);
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return mng_.getNbVersion();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

  /**
   * Permet de sauvegarder les versions.
   * @param _writeIniFile true si le fichier de pref doit etre ecrit
   */
  public void saveVersions(final boolean _writeIniFile) {
    cleanProperties();
    if (versionId_ != null && versionId_.length > 0) {
      final StringBuffer buf = new StringBuffer(versionId_[0]);
      DodicoPreferences.DODICO.putStringProperty(getPathKey(versionId_[0]), versionPath_[0]);
      for (int i = 1; i < versionId_.length; i++) {
        buf.append(getSepChar()).append(versionId_[i]);
        DodicoPreferences.DODICO.putStringProperty(getPathKey(versionId_[i]), versionPath_[i]);
      }
      DodicoPreferences.DODICO.putStringProperty(getVersionKey(), buf.toString());
    }
    if (_writeIniFile) {
      DodicoPreferences.DODICO.writeIniFile();
    }
  }

  /**
   * @return le separateur de version
   */
  public static String getSepChar() {
    return ";";
  }

  /**
   * @param _s la version intiale
   * @return la version sans virgule
   */
  public static String cleanVersion(final String _s) {
    return FuLib.replace(_s, getSepChar(), "_");
  }

  /**
   * @param _writeIniFile true si le fichier ini doit etre ecrit.
   */
  public void removeAll(final boolean _writeIniFile) {
    cleanProperties();
    versionId_ = null;
    versionPath_ = null;
    if (_writeIniFile) {
      DodicoPreferences.DODICO.writeIniFile();
    }
  }

  /**
   * recharge les donnees depuis le fichier de preferences.
   */
  public void loadProperties() {
    versionId_ = null;
    versionPath_ = null;
    final Map stringPath = new HashMap();
    //ancienne version
    final String oldPath = DodicoPreferences.DODICO.getStringProperty("install.telemac.exe.dir", null);
    if (oldPath == null) {
      final String versions = DodicoPreferences.DODICO.getStringProperty(getVersionKey(), null);
      boolean rewrite = false;
      if (versions != null) {
        final StringTokenizer tk = new StringTokenizer(versions, getSepChar());
        while (tk.hasMoreTokens()) {
          final String version = tk.nextToken();
          final String path = DodicoPreferences.DODICO.getStringProperty(getPathKey(version), null);
          if (path == null) {
            rewrite = true;
          } else {
            stringPath.put(version, path);
          }
        }
        setVersion(stringPath);
      }
      if (rewrite) {
        saveVersions(true);
      }
    } else {
      String curr = "curr";
      //on va essayer de trouver la version utilis�e.
      final File f = new File(oldPath);
      if (f.exists()) {
        final File parent = f.getParentFile();
        if (parent != null) {
          final String guessVersion = parent.getName();
          if (guessVersion.length() == 4 && guessVersion.charAt(0) == 'V') {
            curr = guessVersion;
          }
        }
      }
      stringPath.put(curr, oldPath);
      setVersion(stringPath);
      DodicoPreferences.DODICO.removeProperty("install.telemac.exe.dir");
    }
  }
}
