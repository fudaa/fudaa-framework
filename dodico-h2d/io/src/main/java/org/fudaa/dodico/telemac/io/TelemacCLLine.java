/**
 * @creation 14 mars 2003
 * @modification $Date: 2007-05-04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.ef.ConditionLimiteEnum;
import org.fudaa.dodico.ef.ConditionLimiteHelper;

/**
 * @author deniger
 * @version $Id: TelemacCLLine.java,v 1.11 2007-05-04 13:47:27 deniger Exp $
 */
public class TelemacCLLine {

  /**
   * Code pour la hauteur.
   */
  public int lihbor_;
  /**
   * Code pour u.
   */
  public int liubor_;
  /**
   * Code pour v.
   */
  public int livbor_;
  /**
   * valeur de la hauteur.
   */
  public double hbor_;
  /**
   * valeur de u.
   */
  public double ubor_;
  /**
   * valeur de v.
   */
  public double vbor_;
  /**
   * valeur du coeff de frottement.
   */
  public double aubor_;
  /**
   * Code pour le traceur.
   */
  public int litbor_;
  /**
   * valeur du traceur.
   */
  public double tbor_;
  /**
   * Coefficent a pour la loi de flux.
   */
  public double atbor_;
  /**
   * Coefficent b pour la loi de flux.
   */
  public double btbor_;
  /**
   * Numero global.
   */
  public int n_;
  /**
   * Numero dans la numerotation des points de bord.
   */
  public int k_;

  public TelemacCLLine() {
    super();
  }

  public TelemacCLLine(ConditionLimiteEnum cl) {
    super();
    initFrom(cl);
  }

  public boolean isSolid() {
    int solidCode = ConditionLimiteHelper.ILOG;
    if (lihbor_ != solidCode) return false;
    if (liubor_ == solidCode && livbor_ == solidCode) return true;
    if (liubor_ == solidCode && livbor_ == 0) return true;
    if (liubor_ == 0 && livbor_ == solidCode) return true;
    return false;
  }

  public boolean isLiquid() {
    return !isSolid();
  }

  public void initFrom(ConditionLimiteEnum clEnum) {
    if (clEnum == null) {
      initFrom(ConditionLimiteEnum.SOLID);
      return;
    }
    lihbor_ = clEnum.getCodeHauteur();
    liubor_ = clEnum.getCodeVitesseU();
    livbor_ = clEnum.getCodeVitesseV();
    litbor_ = clEnum.getCodeTraceur();
  }

  public boolean sameHUVCode(TelemacCLLine other) {
    return other != null && other.lihbor_ == lihbor_ && other.liubor_ == liubor_ && other.livbor_ == livbor_;
  }

  public void initCodesFrom(TelemacCLLine other) {
    if (other == null) {
      initFrom(ConditionLimiteEnum.SOLID);
      return;
    }
    lihbor_ = other.lihbor_;
    liubor_ = other.liubor_;
    livbor_ = other.livbor_;
    litbor_ = other.litbor_;
  }

  /**
   * @return description de toutes les variables.
   */
  public String getResume() {
    return lihbor_ + CtuluLibString.ESPACE + liubor_ + CtuluLibString.ESPACE + livbor_ + CtuluLibString.ESPACE + hbor_
        + CtuluLibString.ESPACE + ubor_ + CtuluLibString.ESPACE + vbor_ + CtuluLibString.ESPACE + aubor_
        + CtuluLibString.ESPACE + litbor_ + CtuluLibString.ESPACE + tbor_ + CtuluLibString.ESPACE + atbor_
        + CtuluLibString.ESPACE + btbor_ + CtuluLibString.ESPACE + n_ + 1 + CtuluLibString.ESPACE + k_ + 1;
  }

}