/*
 * @creation     19 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

/**
 * Exception utilis� par MascaretWriter indiquant que des g�om�tries manque pour
 * g�n�rer le fichier.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MascaretDataIncomplete extends Exception {

  public MascaretDataIncomplete(String _msg){
    super(_msg);
  }
}
