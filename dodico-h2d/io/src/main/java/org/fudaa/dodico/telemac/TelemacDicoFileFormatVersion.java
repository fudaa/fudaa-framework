/**
 * @creation 29 sept. 2003
 * @modification $Date: 2007-06-29 15:10:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import java.util.Set;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.ef.io.serafin.SerafinFileFormat;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoFileFormatVersion;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;
import org.fudaa.dodico.telemac.io.TelemacCLFileFormat;
import org.fudaa.dodico.telemac.io.TelemacCLVersion;
import org.fudaa.dodico.telemac.io.TelemacLiquideFileFormat;

/**
 * @author deniger
 * @version $Id: TelemacDicoFileFormatVersion.java,v 1.20 2007-06-29 15:10:29 deniger Exp $
 */
public class TelemacDicoFileFormatVersion extends H2dTelemacDicoFileFormatVersion {

  protected DicoEntite clEntite_;

  protected DicoEntite gridEntite_;

  /**
   * @param _ft
   * @param _dico
   */
  public TelemacDicoFileFormatVersion(final DicoCasFileFormat _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico);
    getCLEntiteFile();
    getMaillageEntiteFile();
    if (clEntite_ != null) {
      clEntite_.setRequired(true);
    }
    if (gridEntite_ != null && (!_ft.getID().toLowerCase().equals(H2dTelemacDicoParams.getWaqtelId()))) {
      gridEntite_.setRequired(true);
    }
  }

  public boolean isGridRequired() {
    return !getDicoFileFormat().getID().toLowerCase().equals(H2dTelemacDicoParams.getWaqtelId());
  }

  @Override
  public void fillListWithNotSetRequiredEntite(final DicoParamsInterface _i, final Set _l) {
    if ((clEntite_ != null) && clEntite_.isRequired() && (!_i.isValueSetFor(clEntite_))) {
      _l.add(clEntite_);
    }
    if (isGridRequired()) {
      if ((gridEntite_ != null) && (!_i.isValueSetFor(gridEntite_))) {
        _l.add(gridEntite_);
      }
    }
    final DicoEntite res = getEntiteFor("FICHIER DES RESULTATS", "RESULTS FILE");
    if (res != null && !_i.isValueSetFor(res)) {
      _l.add(res);
    }
  }

  @Override
  public String getBottomKey() {
    return SerafinFileFormat.getVariableFond(getDico().getLanguageIndex());
  }

  @Override
  public DicoEntite getCLEntiteFile() {
    if (clEntite_ == null) {
      clEntite_ = getEntiteFor(new String[]{"FICHIER DES CONDITIONS AUX LIMITES", "BOUNDARY CONDITIONS FILE"});
    }
    return clEntite_;
  }

  @Override
  public DicoEntite getCLLiquideEntiteFile() {
    return getEntiteFor(new String[]{"FICHIER DES FRONTIERES LIQUIDES", "LIQUID BOUNDARIES FILE"});
  }

  @Override
  public FileFormatVersionInterface getDefaultSerafinFormat() {
    return SerafinFileFormat.getInstance();
  }

  /**
   * @param _s le nom du code
   * @return la version a utiliser pour lire le fichier de conditions limites
   */
  public TelemacCLVersion getDefaultTelemacCLFormat(final String _s) {
    return (TelemacCLVersion) TelemacCLFileFormat.getInstance().getLastVersionInstanceForSystem(_s);
  }

  /**
   * @return la version a utiliser pour lire les courbes transitoire
   */
  public TelemacLiquideFileFormat getDefaultTelemacLiquideFormat() {
    return TelemacLiquideFileFormat.getInstance();
  }

  /**
   * @return le format parent
   */
  public DicoCasFileFormat getDicoFileFormat() {
    return (DicoCasFileFormat) super.getFileFormat();
  }


  /**
   *
   */
  @Override
  public DicoEntite getDureeDuCalcul() {
    return getEntiteFor(new String[]{"DUREE DU CALCUL", "DURATION"});
  }

  /**
   * @param _ent le mot-cl�
   * @return le format correspondant ou null si aucune
   */
  public FileFormat getFileFormat(final DicoEntite _ent) {
    if (_ent == null) {
      return null;
    }
    // entite du fichier cas
    if (_ent.equalsEntite(getMaillageEntiteFile())) {
      return SerafinFileFormat.getInstance();
    } else if (_ent.equalsEntite(getCLEntiteFile())) {
      return TelemacCLFileFormat.getInstance();
    } else if (_ent.equalsEntite(getCLLiquideEntiteFile())) {
      return TelemacLiquideFileFormat.getInstance();
    }
    return null;
  }

  /**
   * @return le mot-cl� decrivant le fichier de fond
   */
  public DicoEntite getFONDEntiteFile() {
    return getEntiteFor(new String[]{"FICHIER DES FONDS", "BOTTOM TOPOGRAPHY FILE"});
  }

  @Override
  public DicoEntite getLiquidBoundaryOption() {
    return getEntiteFor(new String[]{"OPTION POUR LES FRONTIERES LIQUIDES", "OPTION FOR LIQUID BOUNDARIES"});
  }

  @Override
  public DicoEntite getMaillageEntiteFile() {
    if (gridEntite_ == null) {
      gridEntite_ = getEntiteFor(new String[]{"FICHIER DE GEOMETRIE", "GEOMETRY FILE"});
    }
    return gridEntite_;
  }

  @Override
  public DicoEntite getNbPasTemps() {
    return getEntiteFor(new String[]{"NOMBRE DE PAS DE TEMPS", "NUMBER OF TIME STEPS"});
  }

  @Override
  public DicoEntite getPasTemps() {
    return getEntiteFor(new String[]{"PAS DE TEMPS", "TIME STEP"});
  }

  @Override
  public DicoEntite getPrescribedElevation() {
    return getEntiteFor(new String[]{"COTES IMPOSEES", "PRESCRIBED ELEVATIONS"});
  }

  @Override
  public DicoEntite getPrescribedFlowrate() {
    return getEntiteFor(new String[]{"DEBITS IMPOSES", "PRESCRIBED FLOWRATES"});
  }

  @Override
  public DicoEntite getPrescribedTracer() {
    return getEntiteFor(new String[]{"VALEURS IMPOSEES DES TRACEURS", "PRESCRIBED TRACERS VALUES"});
  }

  /**
   *
   */
  @Override
  public DicoEntite getPrescribedVelocity() {
    return getEntiteFor(new String[]{"VITESSES IMPOSEES", "PRESCRIBED VELOCITIES"});
  }

  /**
   * @return le mot-cl� a utiliser pour activer la reprise de calcul
   */
  @Override
  public DicoEntite getRepriseBooleanKeyword() {
    return getEntiteFor(getRepriseBooleanName());
  }

  @Override
  public DicoEntite getRepriseFileKeyword() {
    return getEntiteFor(getRepriseFileName());
  }

  @Override
  public DicoEntite getSeuilSiphonFile() {
    return getEntiteFor(new String[]{"FICHIER DE DONNEES FORMATE 1", "FORMATTED DATA FILE 1"});
  }

  /**
   * @return le mot-cle definissant le nombre de seuils.
   */
  public DicoEntite getSiphonNombreMotCle() {
    return getEntiteFor(new String[]{"NOMBRE DE SIPHONS", "NUMBER OF CULVERTS"});
  }

  @Override
  public FileFormatSoftware getSoftVersion() {
    final FileFormatSoftware r = new FileFormatSoftware(FileFormatSoftware.TELEMAC_IS);
    r.soft_ = getCodeName();
    r.language_ = super.getDico().getLanguage();
    r.version_ = getDico().getVersion();
    return r;
  }

  @Override
  public String[] getVariableFond() {
    return SerafinFileFormat.getInstance().getVariableFond();
  }

  @Override
  public DicoEntite getVelocityProfils() {
    return getEntiteFor(new String[]{"PROFILS DE VITESSE", "VELOCITY PROFILES"});
  }

  @Override
  public boolean isFlowrateComputeFromElevation(final String _v) {
    return CtuluLibString.QUATRE.equals(_v);
  }

  @Override
  public boolean isFlowrateComputeFromVelocities(final String _v) {
    return CtuluLibString.DEUX.equals(_v);
  }

  @Override
  public boolean isFlowrateComputeFromVelocityU(final String _v) {
    return CtuluLibString.TROIS.equals(_v);
  }

  @Override
  public boolean isSiphonAvailable() {
    return TelemacDicoManager.isTelemac2d((DicoCasFileFormat) getFileFormat());
  }

  @Override
  public boolean isStbtel() {
    return TelemacDicoManager.isStbtel(getDicoFileFormat());
  }

  @Override
  public boolean isTelemac2d() {
    return TelemacDicoManager.isTelemac2d(getDicoFileFormat());
  }

  @Override
  public boolean isTelemac3d() {
    return TelemacDicoManager.isTelemac3d(getDicoFileFormat());
  }

  /**
   * @return true si les courbes transitoires sont support�es par ce modele
   */
  @Override
  public boolean isTransientAvailable() {
    DicoCasFileFormat fileFormat = (DicoCasFileFormat) getFileFormat();
    return TelemacDicoManager.isTelemac2d(fileFormat) || TelemacDicoManager.isTelemac3d(fileFormat);
  }

}
