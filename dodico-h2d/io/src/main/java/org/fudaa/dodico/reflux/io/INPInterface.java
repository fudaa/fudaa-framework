/**
 *  @creation     28 mars 2003
 *  @modification $Date: 2007-06-29 15:10:23 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.reflux.io;

import org.fudaa.dodico.h2d.reflux.H2dRefluxSourceInterface;

/**
 * @author deniger
 * @version $Id: INPInterface.java,v 1.12 2007-06-29 15:10:23 deniger Exp $
 */
public interface INPInterface extends H2dRefluxSourceInterface {
  int getTypeSortie();

  

}
