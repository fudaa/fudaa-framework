/**
 * @creation 1 sept. 2004
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;

import java.io.File;
import java.util.Map;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionSameComparator;

/**
 * @author Fred Deniger
 * @version $Id: RubarTARFileFormat.java,v 1.12 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarTARFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {

  @Override
  public CtuluIOOperationSynthese readEvolutions(final File _f, final ProgressionInterface _prog, final Map _options) {
    final CtuluIOOperationSynthese io = read(_f, _prog);
    final TIntObjectHashMap o = (TIntObjectHashMap) io.getSource();
    io.setSource(o == null ? null : EvolutionSameComparator.getUniqueEvols(o.getValues()));
    return io;
  }

  private static RubarTARFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarTARFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarTARFileFormat();
    }
    return instance_;
  }

  public RubarTARFileFormat() {
    super(1);
    extensions_ = new String[] { "tar" };
    super.description_ = H2dResource.getS("Loi de tarage");
    super.id_ = "RUBAR_TAR";
    super.nom_ = "Rubar tar";
  }

  @Override
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog) {
    final RubarTARReader i = new RubarTARReader();
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarTARReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarTARWriter();
  }
}