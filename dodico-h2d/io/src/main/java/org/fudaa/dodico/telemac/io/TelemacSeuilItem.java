/**
 * @creation 29 nov. 2004
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilInterface;

/**
 * @author Fred Deniger
 * @version $Id: TelemacSeuilItem.java,v 1.3 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacSeuilItem implements H2dTelemacSeuilInterface {

  int[] idxPt1_;
  int[] idxPt2_;
  double[] cotes_;
  double[] debit_;

  @Override
  public int getNbPoint() {
    return idxPt1_ == null ? 0 : idxPt1_.length;
  }

  @Override
  public int getPtIdx1(final int _i) {
    return idxPt1_[_i];
  }

  @Override
  public int getPtIdx2(final int _i) {
    return idxPt2_[_i];
  }

  @Override
  public double getCote(final int _i) {
    return cotes_[_i];
  }

  @Override
  public double getCoefDebit(final int _i) {
    return debit_[_i];
  }

}