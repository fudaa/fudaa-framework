/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.fudaa.dodico.mesure.EvolutionSameComparator;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: RubarAPPFileFormat.java,v 1.6 2006-11-15 09:22:53 deniger Exp $
 */
public class RubarAPPFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {
  /**
   * Initialise les donnees.
   */
  public RubarAPPFileFormat() {
    super(1);
    extensions_ = new String[]{"app"};
    super.description_ = H2dResource.getS("Apport spatialisÚ par maille");
    super.id_ = "RUBAR_APP";
    super.nom_ = "Rubar app";
  }

  @Override
  public CtuluIOOperationSynthese readEvolutions(File _f, ProgressionInterface _prog, Map _options) {
    File dat = CtuluLibFile.changeExtension(_f, "dat");
    CtuluIOOperationSynthese io = null;
    CtuluAnalyze analyze = new CtuluAnalyze();
    if (!dat.exists()) {
      analyze.addFatalError(H2dResource.getS("Le fichier DAT {0} est nÚcessaire pour lire le fichier APP", dat.getName()));
      io = new CtuluIOOperationSynthese();
      io.setAnalyze(analyze);
      return io;
    }
    final List<String> lines = CtuluLibFile.litFichierLineByLine(dat, 1);
    if (lines.size() < 1) {
      analyze.addFatalError(H2dResource.getS("Le fichier DAT {0} est vide", dat.getName()));
      io = new CtuluIOOperationSynthese();
      io.setAnalyze(analyze);
      return io;
    }
    String firstLine = lines.get(0).trim();
    int nbElement = CtuluLibString.isNumeric(firstLine) ? Integer.parseInt(firstLine) : -1;
    if (nbElement < 0) {
      analyze.addFatalError(H2dResource.getS("Le fichier DAT {0} est vide", dat.getName()));
      io = new CtuluIOOperationSynthese();
      io.setAnalyze(analyze);
      return io;
    }
    io = read(_f, _prog, nbElement);
    final RubarAPPResult rubarAPPResult = (RubarAPPResult) io.getSource();
    final EvolutionReguliereInterface[] uniqueEvols = EvolutionSameComparator.getUniqueEvols(rubarAPPResult.evols_);
    int idx = 0;
    for (EvolutionReguliereInterface evol : uniqueEvols) {
      idx++;
      evol.setNom(CtuluLibFile.getSansExtension(_f.getName()) + " " + CtuluLibString.getString(idx));
    }
    io.setSource(rubarAPPResult == null ? null : uniqueEvols);
    return io;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarAPPReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarAPPWriter();
  }

  /**
   * @param _f
   * @param _prog
   * @param _nbElt
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog, final int _nbElt) {
    final RubarAPPReader i = new RubarAPPReader();
    i.setNbElt(_nbElt);
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }
}
