/**
 * @creation 21 mars 2003
 * @modification $Date: 2007-06-29 15:10:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntObjectHashMap;

import java.io.IOException;

import org.fudaa.ctulu.CtuluLibArray;

import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.type.H2dBcType;

/**
 * @author deniger
 * @version $Id: INPWriterV5P0.java,v 1.1 2007-06-29 15:10:24 deniger Exp $
 */
public class INPWriterV5P0 extends INPWriterAbstract {

  /**
   * version par defaut.
   */
  public INPWriterV5P0() {
    this(INPFileFormat.getInstance().getLastV5P0Impl());
  }

  /**
   * @param _f version a ajouter.
   */
  public INPWriterV5P0(final RefluxINPV5P0Version _f) {
    super(_f);
  }

  @Override
  protected void finishEltBordLine(int _idxBordEle, H2dRefluxBordIndexGeneral _bd, int _tailleElem) {}

  @Override
  protected void finishEltLine(int _idxEle, int _tailleElem) {}

  @Override
  protected void writeBlocCoor() throws IOException {
    up_.majProgessionStateOnly(version_.getCOOR());
    out_.intField(3, nbPts_);
    out_.intField(2, 3);
    out_.stringField(1, inter_.getTypeProjet().getRefluxId());
    out_.stringField(0, version_.getCOOR());
    out_.writeFields(version_.getCOORFormat());
    up_.setValue(10, nbPts_);
    int[] fmtCoord = version_.getTroisDoubleFormat();
    for (int i = 0; i < nbPts_; i++) {
      out_.doubleField(2, grid_.getPtZ(i));
      out_.doubleField(1, grid_.getPtY(i));
      out_.doubleField(0, grid_.getPtX(i));
      out_.writeFields(fmtCoord);
      up_.majAvancement();
    }
    out_.println();
  }

  @Override
  protected void writeBlocData() throws IOException {
    out_.writeln(version_.getDATA());
    out_.writeln(inter_.getTypeProjet().getRefluxId());
    out_.println();
  }
  
  @Override
  protected void writeBlocPRELIdxs() throws IOException {
    int idxCurrentD = 0;
    final int[] fmtD = version_.getGroupElemCodeFormat();
    final int maxD = fmtD.length;
    int maxEnCoursD = maxD;
    final H2dNodalPropertyMixte[] gpElD = inter_.getPropElementaires();
    while (idxCurrentD < grpIdx_.size()) {
    	if (idxCurrentD + maxD > grpIdx_.size())
    	{
    		maxEnCoursD = grpIdx_.size() - idxCurrentD;
    	}
      for (int i = maxEnCoursD - 1; i >= 0; i--) {
        out_.intField(i, grpIdx_.get(idxCurrentD + i) + 1);
      }
      idxCurrentD += maxEnCoursD;
      out_.writeFields(fmtD);
    }
  }

  @Override
  protected void writeBlocPRELEntete() throws IOException {
    out_.intField(3, inter_.getPropElementaires().length);
    out_.stringField(0, version_.getPREL());
    int nbGroupe = getNbGroupWithViscosity();

    out_.intField(2, nbGroupe);
    out_.writeFields(version_.getPRELFormat());
  }

  @Override
  protected void writeBlocDLPN() throws IOException {
    up_.majProgessionStateOnly(version_.getDLPN());
    out_.intField(2, 3);
    out_.stringField(0, version_.getDLPN());
    out_.writeFields(version_.getDLPNFormat());
    int[] fmt = version_.getCONDNoeudIdxFormat();
    int idxCurrent = 0;
    final int maxInit = fmt.length;
    int maxCurrent = maxInit;
    int codeExtremite = version_.getDLPNDllPointExtremite();
    int codeMilieu = version_.getDLPNDllPointMilieu();
    up_.setValue(10, nbPts_);
    while (idxCurrent < nbPts_) {
      if ((nbPts_ - idxCurrent) < maxInit) {
        maxCurrent = nbPts_ - idxCurrent;
      }
      for (int i = maxCurrent - 1; i >= 0; i--) {
        out_.intField(i, grid_.isMiddlePoint(idxCurrent + i) ? codeMilieu : codeExtremite);
      }
      out_.writeFields(fmt);
      idxCurrent += maxCurrent;
      up_.majAvancement();
    }
    out_.println();
  }

  @Override
  protected void writeBlocElemEntete() throws IOException {
    out_.intField(3, 6);
    out_.intField(2, grid_.getEltNb() + CtuluLibArray.getNbItem(inter_.getBords()));
    out_.stringField(0, version_.getELEM());
    out_.writeFields(version_.getELEMFormat());
  }

  @Override
  protected void writeBlocTPEL() throws IOException {
    final H2dRefluxBordIndexGeneral[] bdT3 = inter_.getBords();
    out_.stringField(0, version_.getTPEL());
    up_.majProgessionStateOnly(version_.getTPEL());
    out_.writeFields(version_.getBlocSimpleFormat());
    // sur 16 colonnes
    final int[] fmt = version_.getCONDNoeudIdxFormat();
    final int max = fmt.length;
    // Arrondi a l'entier inferieur.
    final int nbEle = grid_.getEltNb();
    int idxCurrent = nbEle / max;
    String sTPEL = FortranWriter.addSpacesBefore(fmt[0], Integer.toString(version_.getCodeFond()));
    final StringBuffer bTPEL = new StringBuffer(sTPEL.length() * max);
    for (int i = max; i > 0; i--) {
      bTPEL.append(sTPEL);
    }
    sTPEL = bTPEL.toString();
    for (int i = (idxCurrent - 1); i >= 0; i--) {
      out_.writeln(sTPEL);
    }
    // pour ecrire les derniers elt de fond.
    idxCurrent = nbEle - idxCurrent * max;
    final int codeFond = version_.getCodeFond();
    final int nbT3bord = bdT3 == null ? 0 : bdT3.length;
    up_.setValue(10, idxCurrent + nbT3bord);
    for (int i = idxCurrent - 1; i >= 0; i--) {
      out_.intField(i, codeFond);
      up_.majAvancement();
    }

    if (nbT3bord > 0) {
      for (int i = 0; i < nbT3bord; i++) {
        out_.intField(idxCurrent++, version_.getBordTypeId(bdT3[i].getBordType()));
        if (idxCurrent == max) {
          out_.writeFields(fmt);
          idxCurrent = 0;
        }
        up_.majAvancement();
      }
    }
    if (idxCurrent > 0) {
      out_.writeFields(fmt);
    }
    out_.println();
  }

  @Override
  protected void writeBlocPRND(final TIntObjectHashMap _globIdxBc) throws IOException {
    out_.intField(2, 2);
    out_.stringField(0, version_.getPRND());
    out_.writeFields(version_.getPRNDFormat());
    int[] fmtPRND = version_.getCONDNoeudIdxFormat();
    final int max16PRND = fmtPRND.length;
    int indexEnCoursPRND = 0;
    final int normalId = version_.getCodePermanent();
    for (int i = 0; i < nbPts_; i++) {
      final H2dRefluxBoundaryCondition cli = (H2dRefluxBoundaryCondition) _globIdxBc.get(i);

      int itemp1 = 0;
      int itemp2 = 0;
      if (cli != null) {
        itemp1 = normalId;
        itemp2 = version_.getCode(cli.getQType());
        if (cli.getQType() == H2dBcType.TRANSITOIRE) {
          pnCourbes_.add(cli.getQTransitoireCourbe());
        }
      }
      out_.intField(indexEnCoursPRND++, itemp1);
      if (indexEnCoursPRND == max16PRND) {
        out_.writeFields(fmtPRND);
        indexEnCoursPRND = 0;
      }
      out_.intField(indexEnCoursPRND++, itemp2);
      if (indexEnCoursPRND == max16PRND) {
        out_.writeFields(fmtPRND);
        indexEnCoursPRND = 0;
      }
    }
    // on ecrit la derniere ligne
    if (indexEnCoursPRND != 0) {
      out_.writeFields(fmtPRND);
    }
    // ecriture des valeurs.
    fmtPRND = version_.getHuitDoubleFormat();
    final int max = fmtPRND.length;
    indexEnCoursPRND = 0;

    for (int i = 0; i < nbPts_; i++) {
      final H2dRefluxBoundaryCondition cli = (H2dRefluxBoundaryCondition) _globIdxBc.get(i);
      double d1 = 0, d2 = 0;
      if (cli != null) {
        d1 = cli.getNormale();
        d2 = cli.getQ();
      }
      out_.doubleField(indexEnCoursPRND++, d1);
      if (indexEnCoursPRND == max) {
        out_.writeFields(fmtPRND);
        indexEnCoursPRND = 0;
      }
      out_.doubleField(indexEnCoursPRND++, d2);
      if (indexEnCoursPRND == max) {
        out_.writeFields(fmtPRND);
        indexEnCoursPRND = 0;
      }
    }
    if (indexEnCoursPRND != 0) {
      out_.writeFields(fmtPRND);
    }
    out_.println();
  }
}