/*
 * @creation 4 d�c. 06
 * @modification $Date: 2006-12-05 10:13:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.fortran.FortranDoubleReader;
import org.fudaa.dodico.fortran.FortranDoubleWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author fred deniger
 * @version $Id$
 */
public class RubarSEMFileFormat extends FileFormatUnique {

  public RubarSEMFileFormat() {
    super(1);
    extensions_ = new String[] { "sem" };
    super.description_ = H2dResource.getS("Fichier de semis de points");
    super.id_ = "RUBAR_SEM";
    super.nom_ = "Cemagref semis";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    FortranDoubleReader w=new FortranDoubleReader(getFmt());
    w.acceptCommentLines("#");
    return w;
  }

  private int[] getFmt() {
    return new int[] { 13, 13, 13 };
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new FortranDoubleWriter(getFmt());
  }

}
