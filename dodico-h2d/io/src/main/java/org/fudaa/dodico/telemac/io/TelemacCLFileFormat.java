/**
 * @creation 14 mars 2003
 * @modification $Date: 2007-06-29 15:10:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.File;

import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;

/**
 * Ce format de fichier est entierement decrit dans la documentation utilisateur de telemac.
 * 
 * @author deniger
 * @version $Id: TelemacCLFileFormat.java,v 1.24 2007-06-29 15:10:31 deniger Exp $
 */
public class TelemacCLFileFormat extends FileFormat {

  private static final TelemacCLFileFormat INSTANCE = new TelemacCLFileFormat();

  /**
   * @return le singleton
   */
  public static final TelemacCLFileFormat getInstance() {
    return INSTANCE;
  }

  protected TelemacCLFileFormat() {
    super(1);
    extensions_ = new String[] { "conlim" };
    id_ = "TELEMAC_CL";
    nom_ = H2dResource.getS("Conditions limites T�l�mac");
    description_ = H2dResource.getS("Fichier contenant les conditions limites pour T�l�mac");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  public FileFormatVersionInterface getLastVersionInstanceForSystem(final String _t) {
    if (H2dTelemacDicoParams.getArtemisId().equals(_t)) {
      return new TelemacCLArtemisVersion(getLastVersion());
    } else if (H2dTelemacDicoParams.getTomawacId().equals(_t)) {
      return new TelemacCLTomawacVersion(getLastVersion());
    } else if (H2dTelemacDicoParams.getSubief2dId().equals(_t)) {
      return new TelemacCLSubief2DVersion(getLastVersion());
    } else if (H2dTelemacDicoParams.getSisypheId().equals(_t)) {
      return new TelemacCLSisypheVersion(getLastVersion());
    }
    return getLastVersionImpl();
  }

  @Override
  public FileFormatVersionInterface getLastVersionInstance(File _f) {
    return getLastVersionImpl();
  }

  /**
   *
   */
  @Override
  public String getLastVersion() {
    return "1.0";
  }

  /**
   * @return derniere version
   */
  public TelemacCLVersion getLastVersionImpl() {
    return new TelemacCLVersion(getLastVersion());
  }
}