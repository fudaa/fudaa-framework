package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * 
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarRESFileFormat extends FileFormatUnique {
  /**
   * Initialise les donnees.
   */
  public RubarRESFileFormat() {
    super(1);
    extensions_ = new String[] { "res"};
    super.description_ = H2dResource.getS("R�sultats d��rosion de digue");
    super.id_ = "RUBAR_RES";
    super.nom_ = "Rubar res";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader(){
    return new RubarRESReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter(){
    return null;
  }
}