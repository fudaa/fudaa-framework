/**
 * @creation 2002-11-20
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

/**
 * interface � implanter pour ecrire un fichier serafin. Les methodes sont calquees sur les donnees du fichier.
 * 
 * @version $Id: SinusxKeyWord.java,v 1.8 2006-09-19 14:45:49 deniger Exp $
 * @author Fred Deniger
 */
public class SinusxKeyWord {

  private final String version_ = "2.0";
  private final String blocCommentaire_ = "C";
  private final String blocSep_ = "B";
  private final String blocNomEntite_ = "CN";
  private final String blocInfos_ = "CP";
  private final String typeSemis_ = "S";
  private final String typeSemisOld_ = "R";
  private final String typeProfil_ = "P";
  private final String typeCourbeNiveau_ = "N";
  private final String typeCourbe_ = "C";
  private final String courbeReliee_ = "1";
  private final String courbeNonReliee_ = "0";
  private final String courbeFermee_ = "1";
  private final String courbeNonFermee_ = "0";
  private final String idPlanXoy_ = "0";
  private final String idPlanYoz_ = "1";
  private final String idPlanZox_ = "2";
  /** Le nb d'indicateur pour un courbe. */
  private final int courbeNbIndic_ = 16;

  public SinusxKeyWord() {}

  /**
   * @return Version
   */
  public String getVersion() {
    return version_;
  }

  /**
   * @return CourbeNBIndic
   */
  public int getCourbeNBIndic() {
    return courbeNbIndic_;
  }

  /**
   * @return BlocCommentaire
   */
  public String getBlocCommentaire() {
    return blocCommentaire_;
  }

  /**
   * @param _t
   * @return BlocCommentaire
   */
  public boolean isBlocCommentaire(final String _t) {
    return blocCommentaire_.equals(_t);
  }

  /**
   * @return BlocSep
   */
  public String getBlocSep() {
    return blocSep_;
  }

  /**
   * @param _t
   * @return BlocSep
   */
  public boolean isBlocSep(final String _t) {
    return blocSep_.equals(_t);
  }

  /**
   * @return BlocNomEntite
   */
  public String getBlocNomEntite() {
    return blocNomEntite_;
  }

  /**
   * @param _t
   * @return BlocNomEntite
   */
  public boolean isBlocNomEntite(final String _t) {
    return blocNomEntite_.equals(_t);
  }

  /**
   * @return BlocInfos
   */
  public String getBlocInfos() {
    return blocInfos_;
  }

  /**
   * @param _t
   * @return BlocInfos
   */
  public boolean isBlocInfos(final String _t) {
    return blocInfos_.equals(_t);
  }

  /**
   * @return TypeSemis
   */
  public String getTypeSemis() {
    return typeSemis_;
  }

  /**
   * @param _t
   * @return TypeSemis
   */
  public boolean isTypeSemis(final String _t) {
    return ((typeSemis_.equals(_t)) || (typeSemisOld_.equals(_t)));
  }

  /**
   * @return TypeSemis_old
   */
  public String getTypeSemisOld() {
    return typeSemisOld_;
  }

  /**
   * @return TypeProfil
   */
  public String getTypeProfil() {
    return typeProfil_;
  }

  /**
   * @param _t
   * @return TypeProfil
   */
  public boolean isTypeProfil(final String _t) {
    return typeProfil_.equals(_t);
  }

  /**
   * @return TypeCourbeNiveau
   */
  public String getTypeCourbeNiveau() {
    return typeCourbeNiveau_;
  }

  /**
   * @param _t
   * @return TypeCourbeNiveau
   */
  public boolean isTypeCourbeNiveau(final String _t) {
    return typeCourbeNiveau_.equals(_t);
  }

  /**
   * @return TypeCourbe
   */
  public String getTypeCourbe() {
    return typeCourbe_;
  }

  /**
   * @param _t
   * @return TypeCourbe
   */
  public boolean isTypeCourbe(final String _t) {
    return typeCourbe_.equals(_t);
  }

  /**
   * @return CourbeReliee
   */
  public String getCourbeReliee() {
    return courbeReliee_;
  }

  /**
   * @param _t
   * @return CourbeReliee
   */
  public boolean isCourbeReliee(final String _t) {
    return courbeReliee_.equals(_t);
  }

  /**
   * @return CourbeNonReliee
   */
  public String getCourbeNonReliee() {
    return courbeNonReliee_;
  }

  /**
   * @param _t
   * @return CourbeNonReliee
   */
  public boolean isCourbeNonReliee(final String _t) {
    return courbeNonReliee_.equals(_t);
  }

  /**
   * @return CourbeFermee
   */
  public String getCourbeFermee() {
    return courbeFermee_;
  }

  /**
   * @param _t
   * @return CourbeFermee
   */
  public boolean isCourbeFermee(final String _t) {
    return courbeFermee_.equals(_t);
  }

  /**
   * @return CourbeNonFermee
   */
  public String getCourbeNonFermee() {
    return courbeNonFermee_;
  }

  /**
   * @param _t
   * @return CourbeNonFermee
   */
  public boolean isCourbeNonFermee(final String _t) {
    return courbeNonFermee_.equals(_t);
  }

  /**
   * @return IdPlanXoy
   */
  public String getIdPlanXoy() {
    return idPlanXoy_;
  }

  /**
   * @param _t
   * @return IdPlanXoy
   */
  public boolean isIdPlanXoy(final String _t) {
    return idPlanXoy_.equals(_t);
  }

  /**
   * @return IdPlanYoz
   */
  public String getIdPlanYoz() {
    return idPlanYoz_;
  }

  /**
   * @param _t
   * @return IdPlanYoz
   */
  public boolean isIdPlanYoz(final String _t) {
    return idPlanYoz_.equals(_t);
  }

  /**
   * @return IdPlanZox
   */
  public String getIdPlanZox() {
    return idPlanZox_;
  }

  /**
   * @param _t
   * @return IdPlanZox
   */
  public boolean isIdPlanZox(final String _t) {
    return idPlanZox_.equals(_t);
  }
}