package org.fudaa.dodico.telemac;

import com.memoire.fu.FuLog;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.Pair;
import org.fudaa.dodico.dico.DicoCasFileFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;

public class TelemacDicoManagerLoader {
  static URI getDicoZipUri() {
    try {
      return TelemacDicoManager.class.getResource("dicos/").toURI();
    } catch (URISyntaxException e) {
      FuLog.error(e);
    }
    return null;
  }

  public Map<DicoCasFileFormat, List<String>> load() {

    Map<String, Pair<DicoCasFileFormat, List<String>>> versionsByName = new HashMap<>();
    final URI dicoZipPath = getDicoZipUri();
    if (dicoZipPath == null) {
      return Collections.emptyMap();
    }
    if (dicoZipPath.toString().indexOf('!') >= 0) {
      final String[] array = dicoZipPath.toString().split("!");
      try (final FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), new HashMap<>())) {
        Path path = fs.getPath(array[1]);
        collectDicoPaths(versionsByName, path);
      } catch (IOException ioe) {
        FuLog.error(ioe);
      }
    } else {
      collectDicoPaths(versionsByName, Paths.get(dicoZipPath));
    }

    Map<DicoCasFileFormat, List<String>> versions = new HashMap<>();
    versionsByName.values().forEach(p -> p.secondValue.sort(null));
    versionsByName.values().forEach(p -> versions.put(p.firstValue, p.secondValue));
    return versions;
  }

  private void collectDicoPaths(Map<String, Pair<DicoCasFileFormat, List<String>>> versionsByName, Path dicoPath) {
    try (Stream<Path> paths = Files.find(dicoPath, 2, (p, b) -> b.isRegularFile(), FileVisitOption.FOLLOW_LINKS)) {
      paths.forEach(path -> {
        addDicoFile(path, versionsByName);
      });
    } catch (IOException ioe) {
      FuLog.error(ioe);
    }
  }

  private void addDicoFile(Path path, Map<String, Pair<DicoCasFileFormat, List<String>>> versionsByName) {
    Path fileName = path.getFileName();
    if (fileName.toString().endsWith(".dico")) {
      String version = path.getParent().getFileName().toString();
      String dico = StringUtils.substringBefore(fileName.toString(), ".");
      versionsByName.putIfAbsent(dico, new Pair<>(getDicoFileFormat(dico), new ArrayList<>()));
      versionsByName.get(dico).secondValue.add(version);
    }
  }

  private TelemacDicoFileFormat getDicoFileFormat(String codeId) {
    if (Telemac2dFileFormat.getInstance().getName().equals(codeId)) {
      return Telemac2dFileFormat.getInstance();
    }
    return new TelemacDicoFileFormat(codeId);
  }

  public static void main(String[] args) throws IOException {
    new TelemacDicoManagerLoader().load();
  }
}
