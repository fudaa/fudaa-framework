/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-20 14:23:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.fortran.FortranDoubleWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarFRXFileFormat.java,v 1.12 2006-11-20 14:23:54 deniger Exp $
 */
public final class RubarFRXFileFormat extends RubarDonneesBrutesFileFormat {
  private static RubarFRXFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarFRXFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarFRXFileFormat();
    }
    return instance_;
  }

  public static RubarFRXFileFormat getFormat(boolean newFormat) {
    RubarFRXFileFormat res = new RubarFRXFileFormat();
    res.setNewFormat(newFormat);
    return res;
  }

  /**
   *
   */
  public RubarFRXFileFormat() {
    super();
    extensions_ = new String[]{"frx"};
    super.description_ = H2dResource.getS("Frottement sur des noeuds");
    super.id_ = "RUBAR_FRX";
    super.nom_ = "Rubar frx";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarFortranDoubleReader(
        new int[][]{{13, 13, 13}, {8, 8, 8}}, 3
    );
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new FortranDoubleWriter(
        isNewFormat() ?
            new int[]{13, 13, 13}
            : new int[]{8, 8, 8}
    );
  }

  @Override
  public boolean isNuage() {
    return true;
  }
}
