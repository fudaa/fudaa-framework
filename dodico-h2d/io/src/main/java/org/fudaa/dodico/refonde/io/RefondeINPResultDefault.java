/*
 * @creation 18 juin 07
 * @modification $Date: 2007-06-29 15:10:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.refonde.io;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: RefondeINPResultDefault.java,v 1.3 2007-06-29 15:10:30 deniger Exp $
 */
public class RefondeINPResultDefault implements RefondeINPResult {

  EfGridInterface grid_;
  int nbDirection_;
  int nbPeriodeHoule_;
  boolean isModuleSeiche_;
  boolean isSpecUnknwon_;

  @Override
  public EfGridInterface getGrid() {
    return grid_;
  }

  @Override
  public int getNombreAngleHoule() {
    return nbDirection_;
  }

  @Override
  public int getNombrePeriodeHoule() {
    return nbPeriodeHoule_;
  }

  protected void setGrid(EfGridInterface _grid) {
    grid_ = _grid;
  }

  protected void setNbDirection(int _nbAngleHoule) {
    nbDirection_ = _nbAngleHoule;
  }

  @Override
  public H2dVariableType getVariableForFirstCol() {
    // NP=1 et ND=1
    if (nbDirection_ == 1 && nbPeriodeHoule_ == 1) {
      return H2dVariableType.PHASE;
    }
    // NP>1
    if (nbPeriodeHoule_ > 1) {
      return H2dVariableType.PERIODE;
    }
    // NP=1 et ND>1
    return H2dVariableType.DIRECTION;
  }

  protected void setNbPeriodeHoule(int _nbPeriodeHoule) {
    nbPeriodeHoule_ = _nbPeriodeHoule;
  }

  @Override
  public boolean isModuleSeiche() {
    return isModuleSeiche_;
  }

  protected void setModuleSeiche(boolean _isModuleSeiche) {
    isModuleSeiche_ = _isModuleSeiche;
  }

  @Override
  public boolean isSpecUnknown() {
    return isSpecUnknwon_;
  }

  protected void setSpecUnknwon(boolean _isSpecUnknwon) {
    isSpecUnknwon_ = _isSpecUnknwon;
  }

}
