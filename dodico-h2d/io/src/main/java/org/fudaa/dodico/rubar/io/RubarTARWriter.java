/**
 * @creation 8 nov. 2004
 * @modification $Date: 2007-05-22 13:11:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;

import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarTARWriter.java,v 1.8 2007-05-22 13:11:24 deniger Exp $
 */
public class RubarTARWriter extends FileOpWriterCharSimpleAbstract {

  private void writeTar(final TIntObjectHashMap _m) {
    try {
      final int nbCourbe = _m.size();
      final int[] idxs = _m.keys();
      Arrays.sort(idxs);
      for (int i = 0; i < nbCourbe; i++) {
        final EvolutionReguliereInterface e = (EvolutionReguliereInterface) _m.get(idxs[i]);
        final int nbValues = e.getNbValues();
        writelnToOut((idxs[i] + 1) + CtuluLibString.ESPACE + nbValues);
        for (int j = 0; j < nbValues; j++) {
          writelnToOut(e.getY(j) + CtuluLibString.ESPACE + e.getX(j));
        }
      }
    } catch (final IOException e) {
      e.printStackTrace();
    }

  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof TIntObjectHashMap) {
      writeTar((TIntObjectHashMap) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}