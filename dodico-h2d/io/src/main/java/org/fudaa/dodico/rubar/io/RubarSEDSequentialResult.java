/**
 *
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCouche;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCoucheContainer;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Permet de lire � la vol�e les r�sultats.
 *
 * @author Fred Deniger
 */
public class RubarSEDSequentialResult implements H2dRubarSedimentInterface {
  private final double[] times;
  private final long[] positions;
  private final int nbNoeud;
  private final File file;

  RubarSEDSequentialResult(int nbNoeuds, double[] times, long[] toNativeArray, File file) {
    this.nbNoeud = nbNoeuds;
    this.times = times;
    this.positions = toNativeArray;
    this.file = file;
  }

  @Override
  public int getNbTimes() {
    return this.times == null ? 0 : this.times.length;
  }

  @Override
  public int getNbNoeuds() {
    return nbNoeud;
  }

  @Override
  public double getTimes(int idx) {
    return this.times[idx];
  }

  private final TIntObjectHashMap<SoftReference<H2dRubarSedimentCoucheContainer[]>> cache = new TIntObjectHashMap<SoftReference<H2dRubarSedimentCoucheContainer[]>>();

  @Override
  public H2dRubarSedimentCoucheContainer getCoucheContainer(int idxTime, int idxNoeud) {
    SoftReference<H2dRubarSedimentCoucheContainer[]> softRefCouches = cache.get(idxTime);
    H2dRubarSedimentCoucheContainer[] couches = null;
    if (softRefCouches == null) {
      couches = readCouches(idxTime);
      cache.put(idxTime, new SoftReference<H2dRubarSedimentCoucheContainer[]>(couches));
    } else {
      couches = softRefCouches.get();
    }
    if (couches == null) {
      return new H2dRubarSedimentCoucheContainer();
    }
    return couches[idxNoeud];
  }

  public H2dRubarSedimentCoucheContainer[] readCouches(int idxTime) {
    H2dRubarSedimentCoucheContainer[] couches = new H2dRubarSedimentCoucheContainer[nbNoeud];
    FortranReader in = null;
    try (FileInputStream stream = new FileInputStream(file)) {
      long position = positions[idxTime];
      stream.getChannel().position(position);
      in = new FortranReader(new LineNumberReader(new InputStreamReader(stream)));
      in.setBlankZero(true);
      in.readFields(RubarSEDReader.getFormatTimeLine());
      for (int i = 0; i < nbNoeud; i++) {
        couches[i] = new H2dRubarSedimentCoucheContainer();
        in.readFields(RubarSEDReader.FMT_COUCHES);
        int k = 0;
        for (int j = 0; j < couches[i].getNbCouches(); j++) {
          H2dRubarSedimentCouche coucheItem = couches[i].getCoucheItem(j);
          coucheItem.setEpaisseur(in.doubleField(k++));
          coucheItem.setDiametre(in.doubleField(k++));
          coucheItem.setEtendue(in.doubleField(k++));
          coucheItem.setContrainte(in.doubleField(k++));
        }
      }
    } catch (Exception ex) {
      Logger.getLogger(RubarSEDSequentialResult.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      FortranLib.close(in);
    }
    return couches;
  }
}
