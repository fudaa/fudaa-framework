/**
 * @file TelemacDicoFileFormat.java
 * @creation 14 ao�t 2003
 * @modification $Date: 2007-06-29 15:10:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.telemac;

import java.io.File;

import com.memoire.bu.BuFileFilter;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoManager;
import org.fudaa.dodico.dico.DicoResource;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: TelemacDicoFileFormat.java,v 1.9 2007-06-29 15:10:29 deniger Exp $
 */
public class TelemacDicoFileFormat extends DicoCasFileFormat {

  private static TelemacDicoFileFormat empty;

  public static final  TelemacDicoFileFormat getEmptyFormat() {
    if (empty == null) {
      empty = new TelemacDicoFileFormat(CtuluLibString.EMPTY_STRING);
    }
    return empty;
  }

  /**
   * @param _nom
   */
  public TelemacDicoFileFormat(final String _nom) {
    super(_nom);
    description_ = DicoResource.getS("fichier cas");
    software_ = FileFormatSoftware.TELEMAC_IS;
    extensions_ = new String[] { "cas" };
  }

  @Override
  public BuFileFilter createFileFilter() {
    final BuFileFilter r = new BuFileFilter(extensions_, description_) {

      @Override
      public boolean accept(File _f) {
        if ((_f == null) || _f.getName() == null) {
          return false;
        }
        boolean res = super.accept(_f);
        if (!res) {
          String n = _f.getName();
          res = n.startsWith("cas") && (n.indexOf('.') < 0 || (n.endsWith(".txt")));
        }
        return res;
      }

      @Override
      public boolean accept(File _d, String _fn) {
        if (_fn == null) {
          return false;
        }
        boolean res = super.accept(_d, _fn);
        if (!res) {
          String n = _fn;
          res = n.startsWith("cas") && (n.indexOf('.') < 0 || (n.endsWith(".txt")));
        }
        return res;
      }

    };
    return r;
  }

  /**
   *
   */
  @Override
  public DicoManager getDicoManager() {
    return TelemacDicoManager.getINSTANCE();
  }
}
