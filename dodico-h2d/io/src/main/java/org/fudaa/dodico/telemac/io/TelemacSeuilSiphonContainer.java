/**
 * @creation 29 nov. 2004
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilSiphonContainer;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSiphonInterface;

/**
 * @author Fred Deniger
 * @version $Id: TelemacSeuilSiphonContainer.java,v 1.3 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacSeuilSiphonContainer implements H2dTelemacSeuilSiphonContainer {

  int optionTraitementVitesseT_;
  double relaxation_;
  TelemacSeuilItem[] seuils_;
  TelemacSiphonItem[] siphons_;

  @Override
  public int getSeuilVitesseTangentielle() {
    return optionTraitementVitesseT_;
  }

  @Override
  public final double getRelaxation() {
    return relaxation_;
  }

  public final int getOptionTraitementVitesseT() {
    return optionTraitementVitesseT_;
  }

  @Override
  public int getNbSeuil() {
    return seuils_ == null ? 0 : seuils_.length;
  }

  @Override
  public H2dTelemacSeuilInterface getSeuil(final int _i) {
    return seuils_[_i];
  }

  @Override
  public int getNbSiphon() {
    return siphons_ == null ? 0 : siphons_.length;
  }

  public TelemacSiphonItem getSiphonTest(final int _i) {
    return siphons_[_i];
  }

  @Override
  public H2dTelemacSiphonInterface getSiphon(final int _i) {
    return siphons_[_i];
  }
}