/**
 * @creation 17 d�c. 2004
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleObjectHashMap;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarEnvReader.java,v 1.8 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarEnvReader extends FileOpReadCharSimpleAbstract {
  double[] timesToAvoid_;
  int nbElt_;

  public final boolean isTrunc() {
    return isTrunc_;
  }

  public final double[] getTimesToAvoid() {
    return timesToAvoid_;
  }

  public final void setTimesToAvoid(final double[] _timesToAvoid) {
    timesToAvoid_ = _timesToAvoid;
  }

  /**
   * @return le nombre d'element
   */
  public final int getNbElt() {
    return nbElt_;
  }

  /**
   * @param _ie le nombre d'element
   */
  public final void setNbElt(final int _ie) {
    nbElt_ = _ie;
  }

  boolean isTrunc_;
  double tfinal_;
  boolean newFormat = false;

  @Override
  protected Object internalRead() {
    if (timesToAvoid_ == null) {
      timesToAvoid_ = new double[0];
    }
    Arrays.sort(timesToAvoid_);
    // La premiere ligne ne sert a rien
    final TDoubleObjectHashMap r = new TDoubleObjectHashMap();
    try {
      final String tFinal = in_.readLine();
      try {
        tfinal_ = Double.parseDouble(tFinal.trim());
      } catch (final RuntimeException _evt) {
        FuLog.error(_evt);
      }
      if (nbElt_ <= 0) {
        analyze_.addFatalError(H2dResource.getS("Le nombre d'�lements n'est pas pr�cis�"));
        return null;
      }
      in_.setBlankZero(false);
      final int[] fmtNew = new int[]{9, 15, 8, 15, 15, 15, 8, 15, 15};
      final int[] fmtOld = new int[]{6, 15, 8, 15, 15, 15, 8, 15, 15};
      int[] fmt = null;
      for (int i = 0; i < nbElt_; i++) {
        if (i == 0) {
          in_.readLine();
          if (CtuluLibArray.getSum(fmtNew) == in_.getLine().length()) {
            newFormat = true;
            fmt = fmtNew;
          } else {
            fmt = fmtOld;
          }
          in_.analyzeCurrentLine(fmt);
        } else {
          in_.readFields(fmt);
        }
        if (in_.getLine() == null || in_.getLine().trim().length() == 0 || in_.getNumberOfNotEmptyField() != fmt.length) {
          isTrunc_ = true;
          break;
        }
        double t = in_.doubleField(1);
        if (Arrays.binarySearch(timesToAvoid_, t) < 0) {
          TIntObjectHashMap m = (TIntObjectHashMap) r.get(t);
          if (m == null) {
            m = new TIntObjectHashMap();
            r.put(t, m);
          }
          RubarMaxContainer c = (RubarMaxContainer) m.get(i);
          if (c == null) {
            c = new RubarMaxContainer();
            m.put(i, c);
          }
          c.setH(in_.doubleField(2));
        }
        t = in_.doubleField(3);
        if (Arrays.binarySearch(timesToAvoid_, t) < 0) {
          TIntObjectHashMap m = (TIntObjectHashMap) r.get(t);
          if (m == null) {
            m = new TIntObjectHashMap();
            r.put(t, m);
          }
          RubarMaxContainer c = (RubarMaxContainer) m.get(i);
          if (c == null) {
            c = new RubarMaxContainer();
            m.put(i, c);
          }
          c.setU(in_.doubleField(4), in_.doubleField(5));
        }
      }
    } catch (final EOFException e) {

    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return r;
  }

  public double getTfinal() {
    return tfinal_;
  }
}
