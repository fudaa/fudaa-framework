/**
 * @creation 8 nov. 2004
 * @modification $Date: 2007-05-22 13:11:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarAppInterface;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarAPPWriter.java,v 1.10 2007-05-22 13:11:24 deniger Exp $
 */
public class RubarAPPWriter extends FileOpWriterCharSimpleAbstract {
  private void writeApp(final H2dRubarAppInterface _i) {
    try {
      final FortranWriter w = new FortranWriter(out_);
      w.setSpaceBefore(true);
      w.setStringQuoted(false);
      final int nbElt = _i.getNbElt();
      int tmp = 0;
      int[] fmt = new int[10];
      Arrays.fill(fmt, 8);
      for (int j = 0; j < nbElt; j++) {
        // retour a la ligne sur trop d'element
        if (tmp == fmt.length) {
          w.writeFields(fmt);
          tmp = 0;
        }
        w.intField(tmp++, _i.getEvolIdx(j) + 1);
      }
      w.writeFields(fmt);
      final int[] fmti = new int[]{8};
      final int nbEvol = _i.getNbEvol();
      final int maxAutorise = 20;
      final int maxTimeStep = 1000;
      if (nbEvol > maxAutorise) {
        analyze_.addWarn(H2dResource.getS("Le nombre maximum autoris� de chroniques est {0}", CtuluLibString
            .getString(maxAutorise)), -1);
      }

      w.intField(0, nbEvol);
      w.writeFields(fmti);
      boolean warningDone = false;
      final CtuluNumberFormatI numbFmt = FortranLib.getFortranFormat(15, 4);
      fmt = new int[]{15, 15};
      for (int i = 0; i < nbEvol; i++) {
        final EvolutionReguliereInterface evol = _i.getEvol(i);
        final int nbVal = evol.getNbValues();
        if (nbVal > maxTimeStep && !warningDone) {
          warningDone = true;
          analyze_.addWarn(H2dResource.getS("Le nombre maximum autoris� de pas de temps par chronique est {0}", CtuluLibString
              .getString(maxTimeStep)), -1);
        }
        w.intField(0, nbVal);
        w.writeFields(fmti);
        for (int j = 0; j < nbVal; j++) {
          w.stringField(0, numbFmt.format(evol.getY(j)));
          w.stringField(1, numbFmt.format(evol.getX(j)));
          w.writeFields(fmt);
        }
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof H2dRubarAppInterface) {
      writeApp((H2dRubarAppInterface) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}
