/**
 * @creation 8 juin 2004 @modification $Date: 2006-11-15 09:22:53 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersionAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: RubarDATFileFormat.java,v 1.16 2006-11-15 09:22:53 deniger Exp $
 */
public final class RubarDATFileFormat extends FileFormatGridVersionAbstract {
  private static RubarDATFileFormat instance_;

  @Override
  public String getVersionName() {
    return CtuluLibString.UN;
  }

  /**
   * @return l'instance a utiliser
   */
  public static RubarDATFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarDATFileFormat();
    }
    return instance_;
  }

  private boolean newFormat = true;

  /**
   * Initialise toutes les donnees.
   */
  public RubarDATFileFormat() {
    this(true);
  }

  /**
   * Initialise toutes les donnees.
   */
  public RubarDATFileFormat(boolean newFormat) {
    super(1);
    this.newFormat = newFormat;
    extensions_ = new String[]{"dat"};
    super.description_ = H2dResource.getS("Fichier des donn�es");
    super.id_ = "RUBAR_DAT";
    super.nom_ = newFormat ? "Rubar dat (9 digits)" : "Rubar dat (6 digits)";
  }

  /**
   * @param _idxRubar l'indice a tester
   * @return true s'il s'agit
   */
  public boolean isExternIdx(final int _idxRubar) {
    return _idxRubar != 0;
  }

  public static int getInternIdx() {
    return 0;
  }

  /**
   * Si renvoie null, il se peut ce soit un bord interne. A tester avec isExternIdx
   *
   * @param _idx l'indice a tester
   * @return le bord extern correspondant ou null si aucun.
   */
  public H2dRubarBoundaryType getExternType(final int _idx) {
    return H2dRubarBcTypeList.getExternType(_idx);
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return null;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    final RubarDATReader reader = new RubarDATReader(this);
    return reader;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    final RubarDATWriter rubarDATWriter = new RubarDATWriter();
    rubarDATWriter.setNewFormat(newFormat);
    return rubarDATWriter;
  }

  @Override
  public boolean canReadGrid() {
    return true;
  }

  @Override
  public boolean canWriteGrid() {
    return false;
  }

  @Override
  public boolean hasBoundaryConditons() {
    // TODO Voir si correct
    return false;
  }
}
