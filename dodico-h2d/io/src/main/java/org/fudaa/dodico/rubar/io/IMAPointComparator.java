package org.fudaa.dodico.rubar.io;

import java.util.Comparator;

import org.fudaa.ctulu.gis.CtuluLibGeometrie;

import org.locationtech.jts.geom.Point;

public class IMAPointComparator implements Comparator<Point> {

  private final double eps;

  public IMAPointComparator(double eps) {
    super();
    this.eps = eps;
  }

  @Override
  public int compare(Point o1, Point o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    double d = CtuluLibGeometrie.getDistance(o1.getX(), o1.getY(), o2.getX(), o2.getY());
    //they are equals if they are closer than eps.
    if (d <= eps) {
      return 0;
    }
    if (o1.getX() < o2.getX()) {
      return -1;
    } else if (o1.getX() > o2.getX()) {
      return 1;
    } else if (o1.getY() > o2.getY()) {
      return 1;
    }
    return -1;
  }

}
