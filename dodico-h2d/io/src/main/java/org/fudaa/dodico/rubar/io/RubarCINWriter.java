/**
 * @creation 8 nov. 2004
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.fileformat.FortranLib;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.rubar.H2DRubarSolutionsInitialesInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarCINWriter.java,v 1.10 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarCINWriter extends FileOpWriterCharSimpleAbstract {

  private void writeSI(final H2DRubarSolutionsInitialesInterface _i) {
    final int nbCourbe = _i.getNbValues();
    // true si transport
    final boolean trans = nbCourbe == 5;
    // le nombre d'elt
    final int nbElt = _i.getCommonValueSize();

    final FortranWriter w = new FortranWriter(out_);
    w.setSpaceBefore(true);
    CtuluNumberFormatI numbFmt = FortranLib.getFortranFormat(15, 3);
    w.stringField(0, numbFmt.format(_i.getT0()));
    // si transport, on a 3 colonnes ( deux lettres � la fin)
    int[] fmt = trans ? new int[] { 15, 1, 1 } : new int[] { 15, 1 };
    if (trans) {
      w.stringField(2, _i.isConcentrationParHauteur() ? "h" : CtuluLibString.ESPACE);
    }
    w.stringField(1, _i.isVitesse() ? "v" : CtuluLibString.ESPACE);
    try {
      w.writeFields(fmt);
      fmt = new int[8];
      Arrays.fill(fmt, 10);
      for (int i = 0; i < nbCourbe; i++) {
        final CtuluCollectionDoubleEdit a = _i.getValues(i);
        if (i == 0) {
          numbFmt = FortranLib.getFortranFormat(10, 4);
        } else if ((i == 1) || (i == 2)) {
          numbFmt = FortranLib.getFortranFormat(10, 5);
        } else if (i == 3) {
          numbFmt = FortranLib.getFortranFormat(10, 3);
        } else if (i >= 4) {
          numbFmt = FortranLib.getFortranFormat(10, 7);
        }
        // erreur
        else {
          new Throwable().printStackTrace();
        }
        int tmp = 0;
        for (int j = 0; j < nbElt; j++) {
          // retour a la ligne sur trop d'element
          if (tmp == fmt.length) {
            w.writeFields(fmt);
            tmp = 0;
          }
          w.stringField(tmp++, numbFmt.format(a.getValue(j)));
        }
        w.writeFields(fmt);
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }

  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof H2DRubarSolutionsInitialesInterface) {
      writeSI((H2DRubarSolutionsInitialesInterface) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}