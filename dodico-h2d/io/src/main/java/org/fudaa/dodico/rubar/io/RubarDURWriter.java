package org.fudaa.dodico.rubar.io;

import java.io.IOException;
import java.util.Arrays;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * 
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarDURWriter extends FileOpWriterCharSimpleAbstract {
  private void writeDURResult(final RubarDURResult result) {
    try {
      final FortranWriter w = new FortranWriter(out_);
      w.setSpaceBefore(true);
      w.setStringQuoted(false);
      final int nbNoeuds = result.getNbNoeuds();
      int tmp = 0;
      int[] fmt = new int[10];
      Arrays.fill(fmt, 8);
      for (int j = 0; j < nbNoeuds; j++) {
        // retour a la ligne sur trop d'element
        if (tmp == fmt.length) {
          w.writeFields(fmt);
          tmp = 0;
        }
        w.doubleField(tmp++, result.getZ(j));
      }
      w.writeFields(fmt);
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  @Override
  protected void internalWrite(final Object o) {
    if (o instanceof RubarDURResult) {
      writeDURResult((RubarDURResult)o);
    } else {
      donneesInvalides(o);
    }
  }
}