/*
 * @creation 4 mai 07
 * @modification $Date: 2007-06-29 15:10:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntObjectHashMap;

import java.util.Map;

import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.H2dTimeStepGroup;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryCondition;
import org.fudaa.dodico.h2d.reflux.H2dRefluxParameters;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

public class RefluxInpAdapter extends EfGridSourcesAbstract implements INPInterface {

  final H2dRefluxBordIndexGeneral[] bd_;
  // final H2dRefluxBoundaryCondition[] cls_;
  final TIntObjectHashMap cls_;
  String[] fics_;
  final boolean isSoll_;
  final H2dRefluxParameters params_;
  final H2dTimeStepGroup[] times_;

  @Override
  public H2dNodalPropertyMixte[] getPropNodales() {
    // REFLUX V5P1 a completer
    // return params_.getProprieteNodalTab();
    return null;
  }

  /**
   * @param _fic les fichiers a ajouter
   */
  public RefluxInpAdapter(H2dRefluxParameters _params, final String[] _fic) {
    fics_ = _fic;
    params_ = _params;
    bd_ = params_.getRefluxClManager().getRefluxBord();
    cls_ = saveCL(params_.getRefluxClManager().getRefluxCl());

    // sollicitations r�parties
    isSoll_ = H2dRefluxBordIndexGeneral.isSollicitation(bd_);
    times_ = params_.getGroupePasTempsTab();
    // nbDigits_ = _nbDigitForSoll;
  }

  private TIntObjectHashMap saveCL(final H2dRefluxBoundaryCondition[] _cl) {
    final TIntObjectHashMap clMap = new TIntObjectHashMap(_cl.length);
    for (int i = _cl.length - 1; i >= 0; i--) {
      final H2dRefluxBoundaryCondition s = _cl[i];
      if (s != null) {
        clMap.put(s.getIndexPt(), s);
      }
    }
    return clMap;
  }

  /**
   * @return true si contient Radiations
   */
  @Override
  public boolean contientRadiations() {
    return params_.getPropNodal().isRadiationEnable();
  }

  /**
   * @return true su sollications r�parties
   */
  @Override
  public boolean contientSollicitationsReparties() {
    return isSoll_;
  }

  /**
   * @return true si vent
   */
  @Override
  public boolean contientVent() {
    return params_.getPropNodal().isVentEnable();
  }

  /**
   * @return les bords dans le format "reflux"
   */
  @Override
  public H2dRefluxBordIndexGeneral[] getBords() {
    return bd_;
  }

  @Override
  public H2dRefluxBoundaryCondition getConditionLimite(final int _r) {
    return (H2dRefluxBoundaryCondition)cls_.get(_r);
  }

  @Override
  public Map getEntiteValue() {
    return params_.getDicoParams().getEntiteValues();
  }

  @Override
  public String[] getFichiers() {
    return fics_;
  }

  public FileFormat getFileFormat() {
    return INPFileFormat.getInstance();
  }

  @Override
  public EfGridInterface getGrid() {
    return params_.getMaillage();
  }

  @Override
  public H2dTimeStepGroup[] getGroupePasTemps() {
    return times_;
  }

  @Override
  public H2dNodalPropertyMixte[] getPropElementaires() {
    return params_.getPropElemArray();
  }

  @Override
  public FortranDoubleReaderResultInterface getRadiationResult() {
    return new RefluxSollicitationFortranAdapter(params_.getPropNodal().getForceData(H2dVariableType.RADIATION));
  }

  @Override
  public double getTimeBeginInForInpFile() {
    return params_.getBeginTimeForTrans();
  }

  @Override
  public double getTimeBeginningForTrans() {
    return params_.getBeginTimeForTrans();
  }

  @Override
  public H2dProjetType getTypeProjet() {
    return params_.getProjetType();
  }

  @Override
  public int getTypeSortie() {
    return 1;
  }

  @Override
  public FortranDoubleReaderResultInterface getVentResult() {
    return new RefluxSollicitationFortranAdapter(params_.getPropNodal().getForceData(H2dVariableType.VENT));
  }

  @Override
  public String getVersion() {
    return params_.getVersion().getVersionName();
  }

  @Override
  public boolean isRadiationsLecturePasDeTemps() {
    // Fred 2007-05-02 suite dicussion avec Zhang cette propri�t� n'est pas support�e
    return false;
    // return times_.length > 1 || times_[0].getNbPasTemps() > 1;
  }

  @Override
  public boolean isVentLecturePasDeTemps() {
    // Fred 2007-05-02 suite dicussion avec Zhang cette propri�t� n'est pas support�e
    return false;
    // return isRadiationsLecturePasDeTemps();
  }
}