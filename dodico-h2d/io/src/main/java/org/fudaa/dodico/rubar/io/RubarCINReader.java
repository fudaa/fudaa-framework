/**
 * @creation 14 juin 2004
 * @modification $Date: 2007-03-02 13:00:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluArrayDoubleUnique;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarCINReader.java,v 1.13 2007-03-02 13:00:51 deniger Exp $
 */
public class RubarCINReader extends FileOpReadCharSimpleAbstract {
  int nbElt_ = -1;
  int nbBlockTransport = -1;

  /**
   * @param nbBlockTransport the number of transport blocks.
   */
  public void setNbBlockTransport(int nbBlockTransport) {
    this.nbBlockTransport = nbBlockTransport;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileReadOperationAbstract#internalRead()
   */
  @Override
  protected Object internalRead() {
    if (nbElt_ <= 0) {
      analyze_.addFatalError("Le nombre d'�lements n'est pas pr�cis�");
    }
    final RubarCINResult r = new RubarCINResult();
    final int nbNonTransportVariables = 4;
    final int maxVal = nbBlockTransport > 0 ? (nbNonTransportVariables + 3 * nbBlockTransport) : nbNonTransportVariables;
    final CtuluCollectionDoubleEdit[] vValues = new CtuluCollectionDoubleEdit[maxVal];
    int ie = 0;
    try {
      // lecture de la premiere ligne
      int[] fmt = new int[]{15, 1, 1};
      in_.readFields(fmt);
      r.tO_ = in_.doubleField(0);
      r.isVitesse_ = "v".equals(in_.stringField(1));
      r.isConcentrationParHauteur_ = in_.getNumberOfFields() > 2 && "h".equals(in_.stringField(2));
      in_.setBlankZero(false);
      fmt = new int[8];
      final int nbFieldByLine = fmt.length;
      Arrays.fill(fmt, 10);
      int tmpOnLine = 0;
      boolean goOn = true;
      for (int varIdx = 0; varIdx < maxVal && goOn; varIdx++) {
        in_.readFields(fmt);
        vValues[varIdx] = new CtuluArrayDouble(nbElt_);
        int oldProgress = 0;
        tmpOnLine = 0;
        if (in_.getLine() == null) {
          goOn = false;
          break;
        }
        for (ie = 0; ie < nbElt_ && goOn; ie++) {
          if (tmpOnLine == nbFieldByLine) {
            in_.readFields(fmt);
            tmpOnLine = 0;
            if (in_.getLine() == null) {
              goOn = false;
              break;
            }
          }
          try {
            ((CtuluArrayDouble) vValues[varIdx]).set(ie, in_.doubleField(tmpOnLine));
          } catch (final NumberFormatException e2) {
            // les variable h,qu et qv doit etre lue.
            if (varIdx <= 2) {
              throw e2;
            }
            goOn = false;
            break;
          }
          tmpOnLine++;
        }
        if (progress_ != null) {
          oldProgress += 20;
          progress_.setProgression(oldProgress);
        }
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    // everything has been read
    if ((ie == nbElt_) && (vValues[maxVal - 1] != null)) {
      r.values_ = vValues;
    } else if (nbBlockTransport <= 0) {
      doNoTransport(r, vValues);
    } else {
      if (ie != nbElt_) {
        analyze_.addWarn(H2dResource.getS("Le fichier est tronqu� et ne contient pas toutes les valeurs"));
      }
      for (int i = nbNonTransportVariables; i < maxVal; i++) {
        if (vValues[i] == null) {
          vValues[i] = new CtuluArrayDoubleUnique(9999.999, nbElt_);
        }
      }
      analyze_.addWarn(H2dResource.getS("Des donn�es de concentrations sont manquantes et on �t� initialis�e par d�faut � 9999.999"));
      r.values_ = vValues;
    }
    if (r.values_ == null || CtuluLibArray.findObject(r.values_, null) >= 0) {
      if (nbBlockTransport > 0) {
        analyze_.addFatalError(H2dResource.getS("Le fichier doit contenir {0} variables car {1} concentrations sont attendues", CtuluLibString.getString(maxVal),
            CtuluLibString.getString(nbBlockTransport)));
      } else {
        analyze_.addFatalError(H2dResource.getS("Au moins 3 jeux de variables (h,qu et qv) doivent �tre d�finis"));
      }
      return null;
    }
    return r;
  }

  private void doNoTransport(final RubarCINResult _r, final CtuluCollectionDoubleEdit[] _vValues) {
    _r.values_ = new CtuluCollectionDoubleEdit[_vValues.length - 1];
    System.arraycopy(_vValues, 0, _r.values_, 0, _r.values_.length);
    // au cas ou la derniere variable n'a pas ete lue
    if (_r.values_[_r.values_.length - 1] == null) {
      if (FuLog.isTrace()) {
        FuLog.trace("H2D: cote eau non lue");
      }

      _r.values_[_r.values_.length - 1] = new CtuluArrayDoubleUnique(9999.999, nbElt_);
    }
  }

  /**
   * @return le nombre d'element a lire
   */
  public int getNbElt() {
    return nbElt_;
  }

  /**
   * @param _nbElt le nombre d'elements � lire
   */
  public void setNbElt(final int _nbElt) {
    nbElt_ = _nbElt;
  }
}
