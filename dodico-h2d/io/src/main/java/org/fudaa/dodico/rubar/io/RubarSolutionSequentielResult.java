/**
 * @creation 16 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

/**
 * @author Fred Deniger
 * @version $Id: RubarSolutionSequentielResult.java,v 1.6 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarSolutionSequentielResult {
  int nbVar_;
  int nbElt_;
  int nbLigne_;
  long resultLength_;
  long resultLastLineLength_;
  long timeLength_;

  double[] timeStep_;

  /**
   * @return le nombre de variable
   */
  public final int getNbVar() {
    return nbVar_;
  }

  /**
   * @param _idx l'indice du pas de temps
   * @return le pas de temps demande
   */
  public final double getTimeStep(final int _idx) {
    return timeStep_[_idx];
  }

  /**
   * @return le nombre de pas de temps
   */
  public final int getTimeStepNb() {
    return timeStep_ == null ? 0 : timeStep_.length;
  }
}