/**
 * @creation 15 mars 2004
 * @modification $Date: 2007-06-29 15:10:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.fortran.FortranBinaryOutputStream;
import org.fudaa.dodico.h2d.reflux.H2dRefluxSolutionInitAdapterInterface;

/**
 * @author Fred Deniger
 * @version $Id: RefluxSolutionInitWriter.java,v 1.15 2007-06-29 15:10:25 deniger Exp $
 */
public class RefluxSolutionInitWriter extends FileWriteOperationAbstract {

  FortranBinaryOutputStream out_;

  public RefluxSolutionInitWriter() {
    super();
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (out_ != null) {
          out_.close();
        }
      }
    };
  }

  @Override
  public void setFile(final File _f) {
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f.getAbsolutePath());
    FileOutputStream out = null;
    try {
      out = new FileOutputStream(_f);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
      _e.printStackTrace();
      return;
    }
    setOut(out);
  }

  private void setOut(final OutputStream _out) {
    out_ = new FortranBinaryOutputStream(_out, true);
  }

  @Override
  protected void internalWrite(final Object _o) {
    if ((_o instanceof double[])) {
      write((double[]) _o);
    } else {
      donneesInvalides(_o);
    }
  }

  private void write(final double[] _data) {
    if (out_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Le flux de sortie est nul"));
      return;
    }
    if (_data == null) {
      analyze_.addFatalError(DodicoLib.getS("Les donn�es sont nulles"));
      return;
    }
    try {
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(4, _data.length);
      for (int i = 0; i < _data.length; i++) {
        out_.writeDoublePrecision(_data[i]);
        up.majAvancement();
      }
      out_.writeRecord();
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  /**
   * @param _g le maillage associe
   * @param _f le fichier de dest
   * @param _init l'interface contenant les valeurs a ecrire
   * @param _prog la progression
   * @return la synthese de l'ecriture contenant le tableau de double generee comme source
   */
  public static CtuluIOOperationSynthese writeSI(final EfGridInterface _g,
      final H2dRefluxSolutionInitAdapterInterface _init, final File _f, final ProgressionInterface _prog) {
    final int nbPt = _g.getPtsNb();
    final double[] vals = new double[nbPt * 3];
    if (_prog != null) {
      _prog.setProgression(30);
    }
    int idxInVal = 0;
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(3, nbPt, 30, 70);
    up.majProgessionStateOnly();
    for (int i = 0; i < nbPt; i++) {
      vals[idxInVal++] = _init.getU(i);
      vals[idxInVal++] = _init.getV(i);
      if (_g.isExtremePoint(i)) {
        vals[idxInVal++] = _init.getCoteEau(i);
      }
      up.majAvancement();
    }
    final double[] fvals = new double[idxInVal];
    System.arraycopy(vals, 0, fvals, 0, fvals.length);
    return RefluxSolutionInitFileFormat.getInstance().getLastVersionInstance(null).write(_f, fvals, _prog);
  }
}