/**
 * @creation 1 juin 2004
 * @modification $Date: 2006-11-14 09:05:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.ctulu.CtuluAnalyze;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryHouleType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * @author Fred Deniger
 * @version $Id: TelemacCLArtemisVersion.java,v 1.13 2006-11-14 09:05:28 deniger Exp $
 */
public class TelemacCLArtemisVersion extends TelemacCLVersion {

  /**
   * @param _v
   */
  public TelemacCLArtemisVersion(final String _v) {
    super(_v);
  }

  /**
   * @return 2
   */
  public int getSolidId() {
    return 2;
  }

  /**
   * @return 1
   */
  public int getLiquidHouleIncidente() {
    return 1;
  }

  /**
   * @return 4
   */
  public int getLiquidSortieLibre() {
    return 4;
  }

  /**
   * @return 5
   */
  public int getLiquidHouleImposee() {
    return 5;
  }

  @Override
  public boolean getH2dDataFromFile(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target,
      final TelemacCLLine _l, final int _indicesFrontieres) {
    initH2dData(_target, _l);
    // le traceur ne sert a rien
    _target.tracerType_ = H2dBcType.LIBRE;
    final int id = _l.lihbor_;
    if (id == getSolidId()) {
      _target.bordType_ = H2dBoundaryTypeCommon.SOLIDE;
    } else if (id == getLiquidHouleIncidente()) {
      _target.bordType_ = H2dBoundaryHouleType.LIQUIDE_HOULE_INCIDENCE;
    } else if (id == getLiquidHouleImposee()) {
      _target.bordType_ = H2dBoundaryHouleType.LIQUIDE_HOULE_IMPOSEE;
    } else if (id == getLiquidSortieLibre()) {
      _target.bordType_ = H2dBoundaryHouleType.LIQUIDE_SORTIE_LIBRE;
    } else {
      if (_r != null) {
        _r.addFatalError(H2dResource.getS("Codes inconnus"));
      }
      return false;
    }
    return true;
  }

  @Override
  public void getFileLineFromH2dData(final CtuluAnalyze _r, final TelemacCLLine _target,
      final H2dTelemacCLElementSource _source, final int _indexFrontiere) {
    TelemacCLVersion.initDefaultData(_target, _source, _indexFrontiere);
    final H2dBoundaryType t = _source.bordType_;
    int r = -1;
    if (t.equals(H2dBoundaryHouleType.LIQUIDE_HOULE_IMPOSEE)) {
      r = getLiquidHouleImposee();
    } else if (t.equals(H2dBoundaryHouleType.LIQUIDE_HOULE_INCIDENCE)) {
      r = getLiquidHouleIncidente();
    } else if (t.equals(H2dBoundaryHouleType.LIQUIDE_SORTIE_LIBRE)) {
      r = getLiquidSortieLibre();
    } else if (t.equals(H2dBoundaryTypeCommon.SOLIDE)) {
      r = getSolidId();
    } else {
      if (_r != null) {
        _r.addError(H2dResource.getS("Le type de bord est incompatible avec le projet"), _indexFrontiere);
      }
      r = getSolidId();
    }
    _target.lihbor_ = r;
  }

  /**
   * @see org.fudaa.dodico.telemac.io.TelemacCLVersion#isCodeHauteur(int)
   */
  @Override
  public boolean isCodeHauteur(final int _c) {
    return (_c > 0) && (_c < 6) && (_c != 3);
  }

  /**
   * @see org.fudaa.dodico.telemac.io.TelemacCLVersion#isCodeTraceur(int)
   */
  @Override
  public boolean isCodeTraceur(final int _c) {
    return true;
  }

  /**
   * @see org.fudaa.dodico.telemac.io.TelemacCLVersion#isCodeVitesse(int)
   */
  @Override
  public boolean isCodeVitesse(final int _c) {
    return true;
  }

}