/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-12-20 16:46:00 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.fortran.FortranDoubleWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarCOXFileFormat.java,v 1.11 2006-12-20 16:46:00 deniger Exp $
 */
public final class RubarCOXFileFormat extends RubarDonneesBrutesFileFormat {
  private static RubarCOXFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarCOXFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarCOXFileFormat();
    }
    return instance_;
  }

  public static RubarCOXFileFormat getFormat(boolean newFormat) {
    RubarCOXFileFormat res = new RubarCOXFileFormat();
    res.setNewFormat(newFormat);
    return res;
  }

  @Override
  public boolean isNuage() {
    return true;
  }

  public RubarCOXFileFormat() {
    super();
    extensions_ = new String[]{"cox"};
    super.description_ = H2dResource.getS("Bathymétrie sur des noeuds");
    super.id_ = "RUBAR_COX";
    super.nom_ = "Rubar cox";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarFortranDoubleReader(
        new int[][]{{13, 13, 13}, {8, 8, 8}}, 3
    );
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new FortranDoubleWriter(
        isNewFormat() ?
            new int[]{13, 13, 13}
            : new int[]{8, 8, 8}
    );
  }
}
