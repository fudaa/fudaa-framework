/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarINXFileFormat.java,v 1.8 2006-11-15 09:22:53 deniger Exp $
 */
public final class RubarINXFileFormat extends RubarDonneesBrutesFileFormat {
  @Override
  public boolean isNuage() {
    return true;
  }

  private static RubarINXFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarINXFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarINXFileFormat();
    }
    return instance_;
  }

  public static RubarINXFileFormat getFormat(boolean newFormat) {
    RubarINXFileFormat res = new RubarINXFileFormat();
    res.setNewFormat(newFormat);
    return res;
  }

  /**
   *
   */
  public RubarINXFileFormat() {
    super();
    extensions_ = new String[]{"inx"};
    super.description_ = H2dResource.getS("Conditions intiales sur des noeuds");
    super.id_ = "RUBAR_INX";
    super.nom_ = "Rubar inx";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader() {
    final RubarFortranDoubleReader rubarFortranDoubleReader = new RubarFortranDoubleReader(
        new int[][]{
            {8, 8, 8, 8, 8, 8},
            {8, 8, 8, 8, 8, 8, 10},
            {13, 13, 13, 13, 13, 13}
        }, 7
    );
    //the order is important here.
    rubarFortranDoubleReader.addDynamicColumn(new int[]{13, 13, 13, 13, 13, 13}, new int[]{10, 10, 10});
    rubarFortranDoubleReader.addDynamicColumn(new int[]{8, 8, 8, 8, 8, 8}, new int[]{10, 10, 10});
    return rubarFortranDoubleReader;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    if (isNewFormat()) {
      return new RubarFortranDoubleWriter(new int[]{13, 13, 13, 13, 13, 13}, 10);
    }
    return new RubarFortranDoubleWriter(new int[]{8, 8, 8, 8, 8, 8}, 10);
  }
}
