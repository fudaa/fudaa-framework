/**
 * @creation 18 oct. 2004
 * @modification $Date: 2007-04-12 16:18:01 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

import org.fudaa.dodico.h2d.rubar.H2DRubarSolutionsInitialesInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarCINResult.java,v 1.10 2007-04-12 16:18:01 deniger Exp $
 */
public class RubarCINResult implements H2DRubarSolutionsInitialesInterface {

  boolean isConcentrationParHauteur_;

  boolean isVitesse_;
  double tO_;
  CtuluCollectionDoubleEdit[] values_;

  @Override
  public int getCommonValueSize() {
    if (values_ == null || values_.length == 0) {
      return -1;
    }
    final int r = values_[0].getSize();
    for (int i = values_.length - 1; i >= 0; i--) {
      if (r != values_[i].getSize()) {
        return -1;
      }
    }
    return r;
  }

  @Override
  public int getNbValues() {
    return values_ == null ? 0 : values_.length;
  }

  @Override
  public double getT0() {
    return tO_;
  }

  @Override
  public CtuluCollectionDoubleEdit getValues(final int _i) {
    return (values_ == null || _i >= values_.length) ? null : values_[_i];
  }

  @Override
  public boolean isConcentrationParHauteur() {
    return isConcentrationParHauteur_;
  }

  @Override
  public boolean isVitesse() {
    return isVitesse_;
  }

}