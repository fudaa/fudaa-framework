/**
 * @creation 15 avr. 2003
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;

/**
 * @author deniger
 * @version $Id: TelemacLiquideAbstract.java,v 1.10 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacLiquideAbstract implements TelemacLiquideInterface {

  H2dTelemacEvolutionFrontiere[] evol_;

  @Override
  public H2dTelemacEvolutionFrontiere[] getEvolutionsFrontieresLiquides() {
    return evol_;
  }

  @Override
  public int getNbEvolutionsFrontiereLiquide() {
    return evol_ == null ? 0 : evol_.length;
  }

  /**
   * @param _liquides
   */
  public void setEvolutionsFrontieresLiquides(final H2dTelemacEvolutionFrontiere[] _liquides) {
    evol_ = _liquides;
  }
}