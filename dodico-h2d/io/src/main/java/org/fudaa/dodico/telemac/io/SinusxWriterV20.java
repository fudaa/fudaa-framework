/*
 * @creation     10 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.telemac.io;

import java.io.IOException;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Une classe pour ecrire les fichiers Sinusx sous un format fixe. Version 2.0
 * @author Bertrand Marchand
 * @version $Id$
 */
public class SinusxWriterV20 extends SinusxWriterAbstract {
  
  /**
   * @param _v
   */
  public SinusxWriterV20(FileFormatVersionInterface _v) {
    super(_v);
  }

  @Override
  protected void writeCoordinateSequence(final CoordinateSequence _s, final ProgressionUpdater _up, final boolean _ferme)
      throws IOException {
    int n=_s.size();
    if (_ferme) {
      n--;
    }
    for (int k=0; k<n; k++) {
      writelnToOut(CtuluLibString.ESPACE+formate_.format(_s.getX(k))+CtuluLibString.ESPACE+formate_.format(_s.getY(k))
          +CtuluLibString.ESPACE+formate_.format(_s.getOrdinate(k, CoordinateSequence.Z))+" A");
      if (_up!=null) {
        _up.majAvancement();
      }
      if (stop_) {
        return;
      }
    }
  }

}
