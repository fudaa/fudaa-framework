/**
 *  @creation     21 d�c. 2004
 *  @modification $Date: 2006-09-19 14:45:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.text.NumberFormat;

import org.fudaa.dodico.h2d.rubar.H2dRubarDTRResult;
import org.fudaa.dodico.h2d.rubar.H2dRubarNumberFormatter;

/**
 * @author Fred Deniger
 * @version $Id: RubarDTRResult.java,v 1.4 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarDTRResult implements H2dRubarDTRResult {

  double time_;
  double[] x_;
  double[] y_;

  @Override
  public final double getTimeStep() {
    return time_;
  }

  @Override
  public NumberFormat getXYFormatter() {
    return H2dRubarNumberFormatter.DEFAULT_XY_FMT;
  }

  @Override
  public int getNbPoint() {
    return x_ == null ? 0 : x_.length;
  }

  @Override
  public double getX(final int _i) {
    return x_[_i];
  }

  @Override
  public double getY(final int _i) {
    return y_[_i];
  }
}
