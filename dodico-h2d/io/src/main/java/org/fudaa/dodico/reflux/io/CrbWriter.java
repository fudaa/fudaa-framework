/**
 *  @creation     15 d�c. 2003
 *  @modification $Date: 2007-05-22 13:11:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author deniger
 * @version $Id: CrbWriter.java,v 1.20 2007-05-22 13:11:25 deniger Exp $
 */
public class CrbWriter extends FileOpWriterCharSimpleAbstract {

  FileFormatVersionInterface v_;

  /**
   * @param _v la version a utiliser
   */
  public CrbWriter(final FileFormatVersionInterface _v) {
    v_ = _v;
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof EvolutionReguliereInterface[]) {
      writeEvol((EvolutionReguliereInterface[]) _o);
    } else {
      analyze_.addFatalError(DodicoLib.getS("Donn�es invalides"));
    }
  }

  private void writeEvol(final EvolutionReguliereInterface[] _evol) {
    if (_evol == null) {
      analyze_.addFatalError(DodicoLib.getS("Les donn�es sont nulles"));
      return;
    }
    try {
      final FortranWriter w = new FortranWriter(out_);
      w.setLineSeparator(lineSep_);
      w.stringField(1, v_.getVersionName());
      w.stringField(0, "*!$ Version");
      w.writeFields(new int[] { 12, 10 });
      final int n = _evol.length;
      int nbPoint;
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(4, n);
      up.majProgessionStateOnly();
      final int[] fmtEntete = new int[] { 20, 1, 5 };
      final int[] fmtCorps = new int[] { 10, 1, 10 };
      for (int i = 0; i < n; i++) {
        final EvolutionReguliereInterface e = _evol[i];
        w.intField(2, e.getNbValues());
        String nom = e.getNom();
        if ((nom == null) || (nom.length() == 0)) {
          nom = "C " + CtuluLibString.getString(i);
        }
        w.stringField(0, nom);
        w.writeFields(fmtEntete);
        nbPoint = e.getNbValues();
        for (int j = 0; j < nbPoint; j++) {
          w.doubleField(0, e.getX(j));
          w.doubleField(2, e.getY(j));
          w.writeFields(fmtCorps);
        }
        up.majAvancement();
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  /**
   * @return la version utilisee
   */
  public FileFormatVersionInterface getVersion() {
    return v_;
  }

  /**
   * @param _evol
   * @return l'operations
   */
  public CtuluIOOperationSynthese write(final EvolutionReguliereInterface[] _evol) {
    writeEvol(_evol);
    return closeOperation(_evol);
  }

  /**
   * @param _o les evolutions a enregistrer
   * @param _f le fichier
   * @param _inter la barre de progression
   * @return la synthese de l'operation
   */
  public final CtuluIOOperationSynthese write(final EvolutionReguliere[] _o, final File _f, final ProgressionInterface _inter) {
    if (progress_ != null) {
      progress_.setDesc(getOperationDescription(_f));
    }
    setFile(_f);
    setProgressReceiver(_inter);
    return write(_o);
  }
}
