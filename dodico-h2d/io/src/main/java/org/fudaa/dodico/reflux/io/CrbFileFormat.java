/**
 *  @creation     29 ao�t 2003
 *  @modification $Date: 2007-01-19 13:07:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.util.Map;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author deniger
 * @version $Id: CrbFileFormat.java,v 1.17 2007-01-19 13:07:21 deniger Exp $
 * @see org.fudaa.dodico.reflux.TestJCrb
 */
public final class CrbFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {
  private static final CrbFileFormat INSTANCE = new CrbFileFormat();


  @Override
  public String getVersionName() {
    return "5.11";
  }

  /**
   * @return instance
   */
  public static CrbFileFormat getInstance() {
    return INSTANCE;
  }

  /**
   * @param _nbFile
   */
  private CrbFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Reflux crb");
    extensions_ = new String[] { "crb" };
    id_ = "CRB";
    description_ = H2dResource.getS("Fichier contenant  plusieurs courbes");
    software_ = FileFormatSoftware.REFLUX_IS;
  }

  /**
   * @return le reader
   */
  public CrbReader createCrbReader() {
    return new CrbReader();
  }

  /**
   * @return le writer
   */
  public CrbWriter createCrbWriter() {
    return new CrbWriter(this);
  }

  /**
   * @param _f le fichier a ecrire
   * @param _inter la source pour l'ecriture
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _f, final EvolutionReguliere[] _inter, final ProgressionInterface _prog) {
    final CrbWriter w = createCrbWriter();
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  /**
   *
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new CrbReader();
  }

  /**
   *
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return new CrbWriter(this);
  }

  /**
   * the source is a H2DEvolution array.
   */
  @Override
  public CtuluIOOperationSynthese readEvolutions(final File _f, final ProgressionInterface _prog, final Map _options) {
    return read(_f, _prog);
  }

}
