/*
 * @creation     13 nov. 2008
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Un format pour Mascaret 1D (sans coordonnées géoréférencées).
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MascaretGEO1dFileFormat extends FileFormatUnique {

  private MascaretGEO1dFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Mascaret profils 1d");
    description_ = H2dResource.getS("Fichier contenant des profils 1D");
    extensions_=new String[]{"geo"};
  }
  
  static final MascaretGEO1dFileFormat INSTANCE = new MascaretGEO1dFileFormat();

  /**
   * @return singleton
   */
  public static MascaretGEO1dFileFormat getInstance() {
    return INSTANCE;
  }
  
  @Override
  public FileReadOperationAbstract createReader() {
    return null;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new MascaretGEOWriter(true);
  }

}
