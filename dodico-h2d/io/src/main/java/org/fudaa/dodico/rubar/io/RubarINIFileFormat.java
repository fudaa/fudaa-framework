/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarINIFileFormat.java,v 1.9 2006-11-15 09:22:53 deniger Exp $
 */
public final class RubarINIFileFormat extends RubarDonneesBrutesFileFormat {

  @Override
  public  boolean isNuage() {
    return false;
  }

  private static RubarINIFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarINIFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarINIFileFormat();
    }
    return instance_;
  }

  /**
   * 
   */
  public RubarINIFileFormat() {
    super();
    extensions_ = new String[] { "ini" };
    super.description_ = H2dResource.getS("Solutions initiales");
    super.id_ = "RUBAR_INI";
    super.nom_ = "Rubar ini";
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarVF2MReader(true, false);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarVF2MWriter(false);
  }


}