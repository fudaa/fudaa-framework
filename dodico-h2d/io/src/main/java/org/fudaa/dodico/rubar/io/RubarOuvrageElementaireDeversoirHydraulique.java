package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireDeversoirHydrauliqueI;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireDeversoirI;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Set;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarOuvrageElementaireDeversoirHydraulique implements H2dRubarOuvrageElementaireDeversoirI, H2dRubarOuvrageElementaireDeversoirHydrauliqueI {
  double longDeversement_;
  double coteSeuilZd_;
  double coteMisEnchargeZm_;
  double coefficientDebit_;

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.DEVERSOIR_HYDRAULIQUE;
  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {

  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return null;
  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    return false;
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
  }

  @Override
  public final double getCoefficientDebit() {
    return coefficientDebit_;
  }

  @Override
  public final double getCoteMisEnchargeZm() {
    return coteMisEnchargeZm_;
  }

  @Override
  public final double getCoteSeuilZd() {
    return coteSeuilZd_;
  }

  @Override
  public final double getLongDeversement() {
    return longDeversement_;
  }
}
