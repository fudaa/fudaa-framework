package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.fortran.FortranDoubleReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class RubarFortranDoubleReader extends FileReadOperationAbstract {
  private List<DynamicColumn> dynamicColumns = new ArrayList<>();
  private int[][] expectedFormats;
  private int nbFields;
  private FortranDoubleReader doubleReader;
  private File file;

  public RubarFortranDoubleReader(int[][] expectedFormats, int nbFields) {
    this.expectedFormats = expectedFormats;
    this.nbFields = nbFields;
  }

  public static boolean isCorrectFormat(String line, int[] format) {
    return isCorrectFormat(line, format, false);
  }

  public static boolean isCorrectFormat(String line, int[] format, boolean checkLastCharIsNotSpace) {
    int nbChar = CtuluLibArray.getSum(format);

    if (line.length() == nbChar) {
      int startIndex = 0;
      int endIndex = 0;

      for (int i = 0; i < format.length; i++) {
        startIndex = endIndex;
        endIndex += format[i];

        char charToTest = line.charAt(endIndex - 1);
        if (checkLastCharIsNotSpace && charToTest == ' ') {
          return false;
        }

        if (!Character.isDefined(charToTest)) {
          return false;
        }

        try {
          Double.parseDouble(line.substring(startIndex, endIndex));
        } catch (NumberFormatException e) {
          return false;
        }
      }

      return true;
    }

    return false;
  }

  @Override
  protected Object internalRead() {
    final List<String> lines = CtuluLibFile.litFichierLineByLine(file, 1);
    int[] fmtToUse = null;
    String line = null;
    if (CtuluLibArray.isNotEmpty(lines)) {
      line = lines.get(0);
      fmtToUse = getFormatToUse(line);
    }
    if (fmtToUse != null) {
      doubleReader = new FortranDoubleReader(fmtToUse);
    } else {
      fmtToUse = findDynamicFormat(line);
      if (fmtToUse != null) {
        doubleReader = new FortranDoubleReader(fmtToUse);
      }
    }
    if (doubleReader == null) {
      doubleReader = new FortranDoubleReader(nbFields);
    }
    doubleReader.setFile(file);
    doubleReader.setProgressReceiver(progress_);
    final Object read = doubleReader.internalRead();
    doubleReader.getAnalyze().setDesc(analyze_.getDesc());
    analyze_.merge(doubleReader.getAnalyze());
    return read;
  }

  private int[] getFormatToUse(String line) {

    int lineLength = line.length();
    for (int[] fmt : expectedFormats) {
      if (CtuluLibArray.getSum(fmt) == lineLength) {
        return fmt;
      }
    }
    return null;
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return doubleReader;
  }

  @Override
  public void setFile(File _f) {
    analyze_ = new CtuluAnalyze();
    analyze_.setDesc(getOperationDescription(_f));
    analyze_.setResource(_f.getAbsolutePath());
    this.file = _f;
  }

  public void addDynamicColumn(int[] dynamicBase, int[] dynamicToRepeat) {
    dynamicColumns.add(new DynamicColumn(dynamicBase, dynamicToRepeat));
  }

  private int[] findDynamicFormat(String line) {
    if (line == null) {
      return null;
    }
    for (DynamicColumn dynamicColumn : dynamicColumns) {
      int[] formatFound = dynamicColumn.getFormat(line);
      if (formatFound != null) {
        return formatFound;
      }
    }
    return null;
  }

  public static class DynamicColumn {
    private int[] dynamicBase;
    private int[] dynamicToRepeat;

    public DynamicColumn(int[] dynamicBase, int[] dynamicToRepeat) {
      this.dynamicBase = dynamicBase;
      this.dynamicToRepeat = dynamicToRepeat;
    }

    public int[] getFormat(String line) {
      //we test that the first column is on dynamicBase[0] chars
      String firstColumn = line.substring(0, dynamicBase[0]);
      if (firstColumn.endsWith(" ") || !CtuluLibString.isNumeric(firstColumn)) {
        return null;
      }
      int baseLength = CtuluLibArray.getSum(dynamicBase);
      int toRepeatLength = CtuluLibArray.getSum(dynamicToRepeat);
      int nbToRepeat = (line.length() - baseLength) / toRepeatLength;
      TIntArrayList fmtAsList = new TIntArrayList();
      fmtAsList.add(dynamicBase);
      for (int i = 0; i < nbToRepeat; i++) {
        fmtAsList.add(dynamicToRepeat);
      }
      int[] fmtToUse = fmtAsList.toNativeArray();
      if (isCorrectFormat(line, fmtToUse,true)) {
        return fmtToUse;
      }
      return null;
    }
  }
}
