/**
 * @creation 15 avr. 2003
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */

package org.fudaa.dodico.telemac.io;

import org.fudaa.dodico.h2d.telemac.H2dTelemacEvolutionFrontiere;


/**
 * @author deniger
 * @version $Id: TelemacLiquideInterface.java,v 1.11 2006-09-19 14:45:49 deniger Exp $
 */
public interface TelemacLiquideInterface {

  /**
   * @return le tableau des evolutions dans l'ordre.
   */
  H2dTelemacEvolutionFrontiere[] getEvolutionsFrontieresLiquides();

  /**
   * @return le nombre d'evolution definie.
   */
  int getNbEvolutionsFrontiereLiquide();

}