/*
 * @creation     13 nov. 2008
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoLib;

/**
 * Parser du format Mascaret. (versions 6.0 et pr�c�dentes).
 * Lit des fichiers Mascaret g�or�f�renc�s ou non.
 *
 * @author Emmanuel MARTIN, marchand@deltacad.fr
 * @version $Id$
 */
public class MascaretGEOReader extends FileReadOperationAbstract {

  /** Exception lev�e quand une probl�me est trouv� pendant le parsing. */
  private class ParseError extends Exception{
    public ParseError(String _message){
      super(_message);
    }
  }
  
  /** Le fichier � lire */
  private FileReader rawData_;
  private String fileName_;
  /** Les caract�res de 's�paration' */
  private char[] charSeparation_=new char[]{' ', '\t'};
  private char charSeparationNewLigne_='\n';
  /** Le bief */
  MascaretBief bief=new MascaretBief();
  
  public MascaretGEOReader() {
    rawData_=null;
  }
  
  /**
   * Retourne vrai si _element est dans _tabElements
   * @param _element
   * @param _tabElements
   * @return retourne vrai si _element est dans _tabElements
   */
  private boolean in(final char _element, final char[] _tabElements){
    boolean found=false;
    int i=-1;
    while(!found&&++i<_tabElements.length)
      found=_element==_tabElements[i];
    return found;
  }
  
  /**
   * @return token dont le caract�re de s�paration est le retour � la ligne. Null si rien � lire.
   */
  private StringBuffer nextLineToken(){
    StringBuffer token=new StringBuffer();
    int currentRawChar;
    try {
      currentRawChar=rawData_.read();
      // Lecture du token : lecture tant qu'un caract�re de s�paration n'est pas
      // trouv�.
      while (currentRawChar!=-1&&((char)currentRawChar)!=charSeparationNewLigne_) {
        token.append((char)currentRawChar);
        currentRawChar=rawData_.read();
      }
    }
    catch (IOException _exc) {
      // Erreur dans un des rawData_.read()
      _exc.printStackTrace();
    }
    // Retour du token
    return token.length()==0 ? null:token;
  }
  
  /**
   * Retourne le token suivant dans le flux (liste de caract�re jusqu'au
   * caract�re de s�paration suivant).
   * @return le token ou null si il n'y en a pas.
   */
  private String nextWordToken(StringBuffer _lineToken) {
    StringBuffer token=new StringBuffer();
    // Elimination des caract�res de s�paration avant le token
    while (_lineToken.length()>0&&in(_lineToken.charAt(0), charSeparation_))
      _lineToken.deleteCharAt(0);
    // Lecture du token : lecture tant qu'un caract�re de s�paration n'est pas
    // trouv�.
    while (_lineToken.length()>0&&!in(_lineToken.charAt(0), charSeparation_)) {
      token.append(_lineToken.charAt(0));
      _lineToken.deleteCharAt(0);
    }
    // Retour du token
    return token.toString().length()==0 ? null:token.toString();
  }
  
  /*
   * Lit les g�om�tries et les retourne sous forme d'un tableau d'objets contenant
   * le nom du bief +  5 collections distinctes.<p>
   * [0] : Nom du bief<br>
   * [1] : Les 5 collections : 0: Traces, 1: Profils, 2:Axe, 3:Rives, 4:Stockage
   */
  @Override
  protected Object[] internalRead() {
    try {
      StringBuffer lineToken=nextLineToken();
      if(lineToken==null)
        throw new ParseError(fileName_+DodicoLib.getS(" : le fichier est vide."));
      /*
       * Boucle principale : une it�ration par profil
       */
      String token=nextWordToken(lineToken);
      while (lineToken!=null&&token.equalsIgnoreCase("profil")) {
        MascaretProfilAbstractRepresentation profil=new MascaretProfilAbstractRepresentation();

        // Premi�re ligne \\
        // Extraction des informations commune � 1d et 2d
        profil.nomBief=nextWordToken(lineToken);
        profil.nomProfil=nextWordToken(lineToken);
        profil.abscLong=Double.parseDouble(nextWordToken(lineToken));
        // Coordonn�es de trace de profile : non pr�sent dans les fichiers 1d
        token=nextWordToken(lineToken);
        while (token!=null&&!token.equals("AXE")) {
          profil.ptsTrace.add(new double[]{Double.parseDouble(token), Double.parseDouble(nextWordToken(lineToken))});
          token=nextWordToken(lineToken);
        }
        // Coordonn�e d'intersection du profile et de l'axe hydraulique
        if (token!=null&&token.equals("AXE"))
          profil.ptAxeHydrau=new double[]{Double.parseDouble(nextWordToken(lineToken)),
              Double.parseDouble(nextWordToken(lineToken))};

        // Lignes suivantes : coordonn�es du profile \\
        lineToken=nextLineToken();
        while (lineToken!=null&&!(token=nextWordToken(lineToken)).equalsIgnoreCase("profil")) {
          profil.abscTravers.add(Double.parseDouble(token));
          profil.coordZ.add(Double.parseDouble(nextWordToken(lineToken)));
          profil.typePts.add(nextWordToken(lineToken).charAt(0));
          // Coordonn�es XY : non pr�sent dans les fichiers 1d
          token=nextWordToken(lineToken);
          if (token!=null)
            profil.pts.add(new double[]{Double.parseDouble(token), Double.parseDouble(nextWordToken(lineToken))});
          lineToken=nextLineToken();
        }
        // Verification de la coh�rance des informations
        if (profil.is1d())
          throw new ParseError(fileName_+DodicoLib.getS(" : les profils 1D ne sont pas g�r�s"));
        else if (!(profil.is2d()&&profil.check2D()))
          throw new ParseError(fileName_+DodicoLib.getS(" : le fichier source comporte des erreurs."));
        // Ajout du profile � la liste et traitement du profile suivant
        bief.addProfil(profil);
      }
      
      // G�n�ration des GISZoneCollectionLigneBrisee avec les informations extraites et
      // retour de celle-ci
      if (progress_!=null)
        progress_.setDesc(DodicoLib.getS("Lecture de ")+fileName_);
      return new Object[]{bief.getBiefName(),bief.buildZones(analyze_, progress_)};
    }
    catch (ParseError _exc) {
      analyze_.addFatalError(_exc.getMessage());
    }
    catch (NumberFormatException _exc){
      analyze_.addFatalError(fileName_+DodicoLib.getS(" : le fichier source comporte des erreurs."));
    }
    catch (NullPointerException _exc){
      analyze_.addFatalError(fileName_+DodicoLib.getS(" : le fichier source comporte des erreurs."));
    }
    return null;
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ctulu.fileformat.FileOperationAbstract#getFortranInterface()
   */
  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {
      @Override
      public void close() throws IOException{
        if (rawData_ != null)
          rawData_.close();
      }
    };
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.fileformat.FileOperationAbstract#setFile(java.io.File)
   */
  @Override
  public void setFile(File _f) {
    fileName_=_f.getName();
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f.getAbsolutePath());
    try {
      rawData_=new FileReader(_f);
    }
    catch (FileNotFoundException _exc) {
    }
  }

}
