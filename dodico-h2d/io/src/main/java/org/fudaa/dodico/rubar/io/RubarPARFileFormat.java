/**
 * @creation 8 juin 2004
 * @modification $Date: 2007-06-29 15:10:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.dico.*;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoCasFileFormatVersion;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoModelTransport;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoParams;
import org.fudaa.dodico.h2d.rubar.H2dRubarDicoModel;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

import java.io.File;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: RubarPARFileFormat.java,v 1.23 2007-06-29 15:10:22 deniger Exp $
 */
public class RubarPARFileFormat extends DicoCasFileFormat {
  /**
   * La version courante.
   *
   * @author Fred Deniger
   * @version $Id: RubarPARFileFormat.java,v 1.23 2007-06-29 15:10:22 deniger Exp $
   */
  public class PARVersion extends H2DRubarDicoCasFileFormatVersion {
    public PARVersion() {
      super(getInstance(), new H2dRubarDicoModel());
    }

    @Override
    public H2DRubarDicoCasFileFormatVersion createNewForProject(final H2dRubarProjetType _type, int nbTransportBlock) {
      if (_type == H2dRubarProjetType.TRANSPORT) {
        return new PARTransportVersion(nbTransportBlock);
      }
      return this;
    }

    @Override
    public int getNbTransportBlocks() {
      return 0;
    }

    @Override
    public H2dRubarProjetType getProjectType() {
      return H2dRubarProjetType.COURANTOLOGIE_2D;
    }

    @Override
    public FileFormatSoftware getSoftVersion() {
      final FileFormatSoftware r = new FileFormatSoftware(FileFormatSoftware.RUBAR_IS);
      r.soft_ = getCodeName();
      r.language_ = super.getDico().getLanguage();
      r.version_ = getDico().getVersion();
      return r;
    }

    @Override
    public FileReadOperationAbstract createReader() {
      return new RubarPARReader();
    }

    @Override
    public FileWriteOperationAbstract createWriter() {
      return new RubarPARWriter();
    }
  }

  private static RubarPARFileFormat instance;

  /**
   * @return l'instance a utiliser
   */
  public static RubarPARFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance == null) {
      instance = new RubarPARFileFormat();
    }
    return instance;
  }

  /**
   *
   */
  public RubarPARFileFormat() {
    super("rubar");
    extensions_ = new String[]{"par"};
    super.description_ = H2dResource.getS("Fichier des param�tres de simulation");
    super.id_ = "RUBAR_PAR";
    super.nom_ = "Rubar par";
  }

  @Override
  public DicoManager getDicoManager() {
    return null;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormat#getLastVersion()
   */
  @Override
  public String getLastVersion() {
    return "1.0";
  }

  @Override
  public FileFormatVersionInterface getLastVersionInstance() {
    return new PARVersion();
  }

  @Override
  public FileFormatVersionInterface getLastVersionInstance(File _f) {
    return getLastVersionInstance();
  }

  @Override
  public DicoCasFileFormatVersion getLastVersionImpl() {
    return (DicoCasFileFormatVersion) getLastVersionInstance();
  }

  public FileFormatVersionInterface getLastTransportVersionInstance(int nbTransportBlock) {
    return new PARTransportVersion(nbTransportBlock);
  }

  public H2DRubarDicoCasFileFormatVersion getLastTransportVersionImpl(int nbTransportBlock) {
    return (H2DRubarDicoCasFileFormatVersion) getLastTransportVersionInstance(nbTransportBlock);
  }

  /**
   * @param dicoParamsInterface le modele des mot-cl�s
   * @return l'association position dans le fichier par -> valeur
   */
  public static TIntObjectHashMap transform(final DicoParamsInterface dicoParamsInterface) {
    final DicoModelAbstract m = dicoParamsInterface.getDico();
    final List<DicoEntite> entites = new ArrayList<DicoEntite>(m.getEntiteNombre());
    entites.addAll(m.getEntites());
    Collections.sort(entites, new DicoEntiteComparator.Index());
    Map<DicoEntite, List<DicoEntite>> byLines = createParLines(entites);
    //kws ne contient plus les entites qui sont sur les autres lignes
    final TIntObjectHashMap r = new TIntObjectHashMap(entites.size());
    final CtuluNumberFormatFortran fmt = new CtuluNumberFormatFortran(11);
    final CtuluNumberFormatFortran fmtOtherFallVelocity = new CtuluNumberFormatFortran(11);
    final CtuluNumberFormatFortran fmtOther = new CtuluNumberFormatFortran(7);
    final CtuluNumberFormatFortran fmtLoadingDistance = new CtuluNumberFormatFortran(8);
    final CtuluNumberFormatFortran fmtLoadingVelocity = new CtuluNumberFormatFortran(10);
    for (int i = 0; i < entites.size(); i++) {
      DicoEntite e = entites.get(i);
      List<DicoEntite> otherOnLine = byLines.get(e);
      boolean useOneValue = H2DRubarDicoModelTransport.isDiameterSediment(e) >= 0
          || H2DRubarDicoModelTransport.isErosionCoefficient(e) >= 0;
      if (useOneValue) {
        if (!e.isModifiable()) {
          e = findModifiable(otherOnLine);
        }
      }
      String v = getParValues(dicoParamsInterface, fmt, e);
      if (!useOneValue && otherOnLine != null) {
        StringBuilder builder = new StringBuilder(30);
        builder.append(v);
        for (DicoEntite dicoEntite : otherOnLine) {
          CtuluNumberFormatFortran fmtToUse = fmtOther;
          if (H2DRubarDicoModelTransport.isFallVelocity(dicoEntite)) {
            fmtToUse = fmtOtherFallVelocity;
          }
          if (H2DRubarDicoModelTransport.isLoadingDistance(dicoEntite) >= 0) {
            fmtToUse = fmtLoadingDistance;
          }
          if (H2DRubarDicoModelTransport.isLoadingVelocity(dicoEntite) >= 0) {
            fmtToUse = fmtLoadingVelocity;
          }
          builder.append(getParValues(dicoParamsInterface, fmtToUse, dicoEntite));
        }
        v = builder.toString();
      }
      r.put(i, v);
    }
    return r;
  }

  private static DicoEntite findModifiable(List<DicoEntite> otherOnLine) {
    for (DicoEntite dicoEntite : otherOnLine) {
      if (dicoEntite.isModifiable()) {
        return dicoEntite;
      }
    }
    return null;
  }

  private static String getParValues(final DicoParamsInterface _i, final CtuluNumberFormatFortran fmt, final DicoEntite e) {
    String v = _i.getValue(e);

    final DicoDataType type = e.getType();
    if (type instanceof DicoDataType.Binaire) {
      if (DicoDataType.Binaire.getValue(v)) {
        v = CtuluLibString.UN;
      } else {
        v = CtuluLibString.ZERO;
      }
      // on transforme tous les r�elles
    } else if (type instanceof DicoDataType.Reel && v != null && v.length() > 0) {
      try {
        v = FortranWriter.addSpacesBefore(fmt.getMaxChar(), fmt.format(Double.parseDouble(v)));
      } catch (final NumberFormatException _evt) {

      }
    }
    return v;
  }

  public static H2DRubarDicoParams createParams(final TIntObjectHashMap readValues) {
    final boolean trans = H2DRubarDicoParams.isTransport(readValues);
    final int nbBlocks = H2DRubarDicoParams.getNbTransportBlock(readValues);
    final H2DRubarDicoCasFileFormatVersion v = trans ? getInstance().getLastTransportVersionImpl(nbBlocks) : (H2DRubarDicoCasFileFormatVersion) getInstance()
        .getLastVersionImpl();
    final DicoModelAbstract dico = v.getDico();
    final List<DicoEntite> entites = new ArrayList<DicoEntite>(dico.getEntites().size());
    entites.addAll(dico.getEntites());
    Collections.sort(entites, new DicoEntiteComparator.Index());
    Map<DicoEntite, List<DicoEntite>> byLines = createParLines(entites);

    final Map<DicoEntite, Object> valueByEntity = new HashMap<>(entites.size());
    List<DicoEntite> listToDisable = new ArrayList<>();
    for (int i = 0; i < entites.size(); i++) {
      final Object t = readValues.get(i);
      DicoEntite dicoEntite = entites.get(i);
      if (t != null) {
        List<DicoEntite> sameLine = byLines.get(dicoEntite);
        if (sameLine == null) {
          valueByEntity.put(dicoEntite, t);
        } else {
          //for some cases, a line contains the same value for several entity.
          if (t instanceof String) {
            valueByEntity.put(dicoEntite, t);
            listToDisable.add(dicoEntite);
          } else {
            Object[] values = (Object[]) t;
            valueByEntity.put(dicoEntite, values[0]);
            for (int valuesIdx = 1; valuesIdx < values.length; valuesIdx++) {
              DicoEntite key = sameLine.get(valuesIdx - 1);
              Object value = values[valuesIdx];
              if (CtuluLibString.isEmpty((String) value)) {
                value = key.getDefautValue();
              }
              valueByEntity.put(key, value);
            }
          }
        }
      }
    }
    for (DicoEntite dicoEntite : listToDisable) {
      boolean firstIsEnabled = H2DRubarDicoModelTransport.isModifiable(dicoEntite, valueByEntity, v);
      if (!firstIsEnabled) {
        dicoEntite.setModifiable(false);
        List<DicoEntite> inLines = byLines.get(dicoEntite);
        for (DicoEntite toModify : inLines) {
          toModify.setModifiable(false);
        }
        Object value = valueByEntity.get(dicoEntite);
        valueByEntity.put(dicoEntite, dicoEntite.getDefautValue());
        DicoEntite enabled = findEnabledEntite(byLines.get(dicoEntite), valueByEntity, v);
        if (enabled != null) {
          enabled.setModifiable(true);
          valueByEntity.put(enabled, value);
        }
      }
    }
    for (DicoEntite dicoEntite : valueByEntity.keySet()) {
      if (!H2DRubarDicoModelTransport.isModifiable(dicoEntite, valueByEntity, v)) {
        dicoEntite.setModifiable(false);
      }
    }

    return new H2DRubarDicoParams(valueByEntity, null, v);
  }

  private static DicoEntite findEnabledEntite(List<DicoEntite> list, Map<DicoEntite, Object> values, H2DRubarDicoCasFileFormatVersion version) {
    for (DicoEntite dicoEntite : list) {
      if (H2DRubarDicoModelTransport.isModifiable(dicoEntite, values, version)) {
        return dicoEntite;
      }
    }
    return null;
  }

  private static Map<DicoEntite, List<DicoEntite>> createParLines(final List<DicoEntite> entites) {
    Map<DicoEntite, List<DicoEntite>> byLines = new HashMap<DicoEntite, List<DicoEntite>>();
    int currentIndex = -1;
    DicoEntite lastEntite = null;
    for (Iterator iterator = entites.iterator(); iterator.hasNext(); ) {
      DicoEntite dicoEntite = (DicoEntite) iterator.next();
      int index = dicoEntite.getIndex();
      if (index == currentIndex) {
        iterator.remove();
        List<DicoEntite> onSameLine = byLines.get(lastEntite);
        if (onSameLine == null) {
          onSameLine = new ArrayList<DicoEntite>();
          byLines.put(lastEntite, onSameLine);
        }
        onSameLine.add(dicoEntite);
      } else {
        lastEntite = dicoEntite;
      }
      currentIndex = index;
    }
    return byLines;
  }

  /**
   * La version courante.
   *
   * @author Fred Deniger
   * @version $Id: RubarPARFileFormat.java,v 1.23 2007-06-29 15:10:22 deniger Exp $
   */
  public class PARTransportVersion extends H2DRubarDicoCasFileFormatVersion {
    int nbTransportBlocks;

    @Override
    public H2dRubarProjetType getProjectType() {
      return H2dRubarProjetType.TRANSPORT;
    }

    @Override
    public int getNbTransportBlocks() {
      return nbTransportBlocks;
    }

    /**
     * Constructeur par defaut.
     */
    public PARTransportVersion(int nbTransportBlocks) {
      super(getInstance(), new H2DRubarDicoModelTransport(nbTransportBlocks));
      this.nbTransportBlocks = nbTransportBlocks;
    }

    /**
     * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
     */
    @Override
    public FileReadOperationAbstract createReader() {
      return new RubarPARReader();
    }

    @Override
    public H2DRubarDicoCasFileFormatVersion createNewForProject(final H2dRubarProjetType _type, int nbTransportBlocks) {
      if (_type == H2dRubarProjetType.COURANTOLOGIE_2D) {
        return new PARVersion();
      }
      if (nbTransportBlocks == this.nbTransportBlocks) {
        return this;
      }
      return new PARTransportVersion(nbTransportBlocks);
    }

    /**
     * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
     */
    @Override
    public FileWriteOperationAbstract createWriter() {
      return new RubarPARWriter();
    }
  }
}
