/**
 * @creation 14 juin 2004 @modification $Date: 2007-01-10 09:04:25 $ @license GNU General Public License 2 @copyright (c)1998-2001
 * CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluArrayDouble;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarVF2MReader.java,v 1.15 2007-01-10 09:04:25 deniger Exp $
 */
public class RubarVF2MReader extends FileOpReadCharSimpleAbstract {

  boolean firstLine_;
  boolean isGridInverse_;

  /**
   * Constructeur par defaut.
   */
  public RubarVF2MReader() {
  }

  /**
   * @param _isFirstLine true si la premiere ligne doit etre lu
   * @param _isGridInverse true si le maillage commence par I=NXMAX
   */
  public RubarVF2MReader(final boolean _isFirstLine, final boolean _isGridInverse) {
    firstLine_ = _isFirstLine;
    isGridInverse_ = _isGridInverse;
  }

  @Override
  protected Object internalRead() {
    final RubarVF2MResult result = new RubarVF2MResult();
    try {
      if (firstLine_) {
        in_.readFields();
        result.firstLine_ = in_.getLine();
      }
      // NYMAX,NXMAX
      in_.readFields();
      // le nombre de points selon oy
      final int nbPtOy = in_.intField(0);
      // lecture des parallèlogramme
      if (nbPtOy < 0) {
        return internalReadParallel(in_.intField(1), result);
      }
      final RubarGridData grid = new RubarGridData();
      // result = new RubarVF2MResult();
      result.gridData_ = grid;
      // le nombre de points selon ox
      final int nbPtOx = in_.intField(1);
      grid.nbX_ = nbPtOx;
      grid.nbY_ = nbPtOy;
      // DX,DY
      in_.readFields();
      // pas d'espace sur Ox
      grid.dx_ = in_.doubleField(0);

      // pas d'espace sur Oy
      grid.dy_ = in_.doubleField(1);
      // X0,Y0
      in_.readFields();
      grid.x0_ = in_.doubleField(0);
      grid.y0_ = in_.doubleField(1);
      // 1+nb de var max (=5) soit 6
      final List r = new ArrayList(6 * nbPtOx);
      // on lit toutes les lignes et on les stocke dans des tableau de double
      // I (la ligne)
      // v1 de 1 a nbCol
      // v2 de 1 a nbCol
      // I+1 (la ligne)
      // v1 de 1 a nbCol
      // v2 de 1 a nbCol
      // Etc...
      try {
        while (true) {
          in_.readFields();//pour lire l'indice de la ligne
          in_.readFields();
          final double[] val = new double[in_.getNumberOfFields()];
          r.add(val);
          for (int i = val.length - 1; i >= 0; i--) {
            val[i] = in_.doubleField(i);
          }

        }
      } catch (final EOFException e) {
      }
      if (progress_ != null) {
        progress_.setProgression(60);
      }
      // calcul du nombre de variables
      final int nbValue = r.size() / nbPtOx;
      // on initialise le tableau
      grid.v_ = new CtuluArrayDouble[nbValue];
      // pour chaque ligne
      for (int i = 0; i < nbPtOx; i++) {
        // idx represente l'indice dans la liste global
        final int idx = i * nbValue;
        // si inverse on part de la fin.
        final int reeli = isGridInverse_ ? nbPtOx - 1 - i : i;
        // on parcourt toutes les valeurs
        for (int val = 0; val < nbValue; val++) {
          // vals est le tableau de retour
          CtuluArrayDouble vals = grid.v_[val];
          if (vals == null) {
            vals = new CtuluArrayDouble(nbPtOx * nbPtOy);
            grid.v_[val] = vals;
          }
          // lu est le tableau lu
          final double[] lu = (double[]) r.get(idx + val);
          for (int j = 0; j < nbPtOy; j++) {
            vals.set(reeli * nbPtOy + j, lu[j]);
          }
        }

      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    } catch (final Exception e) {
      analyze_.manageException(e);
      return null;
    }

    return result;
  }
  String name_ = CtuluLibString.EMPTY_STRING;

  @Override
  public void setFile(final File _f) {
    // TODO Auto-generated method stub
    super.setFile(_f);
    if (_f != null) {
      name_ = _f.getName();
    }
  }

  /**
   * @param _nb le nombre de champs lus
   * @param _first true si c'est la premiere ligne de valeurs lue. Dans ce cas, 4 champs doivent être obtenue
   * @return true si ok
   */
  private boolean testNbFieldsAndIsOk(final int _nb, final boolean _first) {
    if ((_first && _nb != 4) || (_nb != 4 && _nb != 3)) {
      analyze_.addFatalError(CtuluLib.getS("Le format du fichier {0} n'est pas correct ou le fichier est vide", name_)
                             + CtuluLibString.LINE_SEP
                             + H2dResource.getS("Une ligne doit contenir 3 champs pour les "
                                                + "coordonnées\n ou contenir 4 champs pour les valeurs.") + CtuluLibString.LINE_SEP
                             + H2dResource.getS("Nombre de champs lus: {0}", CtuluLibString.getString(_nb)),
                             super.in_.getLineNumber());
      return false;
    }
    return true;
  }

  protected Object internalReadParallel(final int _np, final RubarVF2MResult _res) {
    _res.isParall_ = true;
    final RubarParallelogrammeData[] datas = new RubarParallelogrammeData[_np];
    _res.parallDatas_ = datas;
    _res.gridData_ = null;
    try {
      int nbValue = -1;
      in_.readFields();
      for (int i = 0; i < _np; i++) {
        // on lit les coordonnees du premier parall
        final RubarParallelogrammeData di = new RubarParallelogrammeData();
        datas[i] = di;
        di.setX1(in_.doubleField(0));
        di.setX2(in_.doubleField(1));
        di.setX4(in_.doubleField(2));
        in_.readFields();
        di.setY1(in_.doubleField(0));
        di.setY2(in_.doubleField(1));
        di.setY4(in_.doubleField(2));
        // a ce stade on ne connait pas le nombre de variable definie.
        // une variable est definie sur 4 champs.
        // un debut de parall est def sur 3 champs
        // si on a un seul paralle
        if (_np == 1) {
          in_.readFields();
          int nbFormat = in_.getNumberOfFields();
          final List l = new ArrayList();
          // tant qu'on lit des des valeurs de 4
          // cette boucle lira une ligne en plus
          // ce qui est pris en compte
          if (!testNbFieldsAndIsOk(nbFormat, true)) {
            return null;
          }
          try {
            while (nbFormat == 4) {
              final double[] val = new double[4];
              for (int j = 3; j >= 0; j--) {
                val[j] = in_.doubleField(j);
              }
              l.add(val);
              in_.readFields();
              nbFormat = in_.getNumberOfFields();
              if (!testNbFieldsAndIsOk(nbFormat, false)) {
                return null;
              }
            }

          } catch (final EOFException e) {
            nbValue = l.size();
            di.values_ = new CtuluArrayDouble[nbValue];
            for (int k = di.values_.length - 1; k >= 0; k--) {
              di.values_[k] = new CtuluArrayDouble((double[]) l.get(k));
            }
          }
          return _res;
        }
        // si plusieurs parall
        if (nbValue < 0) {
          in_.readFields();
          int nbFormat = in_.getNumberOfFields();
          final List l = new ArrayList();
          // tant qu'on lit des des valeurs de 4
          // cette boucle lira une ligne en plus
          // ce qui est pris en compte
          if (!testNbFieldsAndIsOk(nbFormat, true)) {
            return null;
          }
          while (nbFormat == 4) {
            final double[] val = new double[4];
            for (int j = 3; j >= 0; j--) {
              val[j] = in_.doubleField(j);
            }
            l.add(val);
            in_.readFields();
            nbFormat = in_.getNumberOfFields();
            if (!testNbFieldsAndIsOk(nbFormat, false)) {
              return null;
            }
          }
          nbValue = l.size();
          di.values_ = new CtuluArrayDouble[nbValue];
          for (int k = di.values_.length - 1; k >= 0; k--) {
            di.values_[k] = new CtuluArrayDouble((double[]) l.get(k));
          }
        } else {
          di.values_ = new CtuluArrayDouble[nbValue];
          for (int j = 0; j < nbValue; j++) {
            in_.readFields();
            di.values_[j] = new CtuluArrayDouble(4);
            for (int k = 3; k >= 0; k--) {
              di.values_[j].set(k, in_.doubleField(k));
            }
          }
          // on avance la lecture d'un pas car la premiere boucle (while==4) lit
          // une ligne de trop
          in_.readFields();
        }

      }
    } catch (final EOFException e) {
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return _res;
  }
}