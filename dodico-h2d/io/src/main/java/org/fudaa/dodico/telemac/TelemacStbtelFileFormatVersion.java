/**
 *  @creation     9 nov. 2004
 *  @modification $Date: 2006-09-19 14:45:58 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac;

import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoModelAbstract;


/**
 * @author Fred Deniger
 * @version $Id: TelemacStbtelFileFormatVersion.java,v 1.5 2006-09-19 14:45:58 deniger Exp $
 */
public class TelemacStbtelFileFormatVersion extends TelemacDicoFileFormatVersion {

  /**
   * @param _ft
   * @param _dico
   */
  public TelemacStbtelFileFormatVersion(final DicoCasFileFormat _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico);
    if (clEntite_ != null){
      clEntite_.setRequired(false);
    }
  }
  @Override
  public DicoEntite getMaillageEntiteFile(){
    if (gridEntite_ == null) {
      gridEntite_ = getEntiteFor(new String[] { "FICHIER UNIVERSEL","UNIVERSAL FILE"});
    }
    return gridEntite_;
    
  }
  @Override
  public boolean isBcPointEditable(){
    return false;
  }
  
  @Override
  public boolean isRugosityModifiable(){
    return false;
  }
  
  
}
