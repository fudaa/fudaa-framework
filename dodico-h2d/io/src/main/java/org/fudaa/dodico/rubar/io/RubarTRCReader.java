/**
 * @creation 21 d�c. 2004
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoResource;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliere;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: RubarTRCReader.java,v 1.12 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarTRCReader extends FileOpReadCharSimpleAbstract {
  private final int nbPts;
  private final int nbValues;

  public final boolean isResultsTruncated() {
    return isResultsTruncated;
  }

  public RubarTRCReader(final int nbPt) {
    this(nbPt, 3);
  }

  public RubarTRCReader(int nbPts, int nbValues) {
    this.nbPts = nbPts;
    this.nbValues = nbValues;
  }

  private boolean isResultsTruncated;
  private double timeError;
  private int timeStepError;
  private boolean isErrorOnFirstLine;
  private String fileName = "TRC";

  @Override
  public void setFile(final File _f) {
    super.setFile(_f);
    if (_f != null) {
      fileName = _f.getName();
    }
  }

  @Override
  protected Object internalRead() {
    if (nbValues < 0) {
      return null;
    }
    if (in_ == null) {
      analyze_.addFatalError(DodicoResource.DODICO.getString("Flux d'entr�e non trouv�"));
      return null;
    }
    final EvolutionReguliere[][] r = new EvolutionReguliere[nbPts][nbValues];
    try {
      final int[] fmt = new int[]{15, 15, 15, 15, 15};
      final int nbValByLine = fmt.length;
      int tmpOnLine;
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      boolean goOne = true;
      int t = 0;
      while (goOne) {
        in_.readFields();
        final double ti = in_.doubleField(0);

        for (int val = 0; val < nbValues; val++) {
          in_.readFields(fmt);
          tmpOnLine = 0;
          for (int pt = 0; pt < nbPts; pt++) {
            if (r[pt][val] == null) {
              r[pt][val] = new EvolutionReguliere(30);
            }
            if (tmpOnLine == nbValByLine) {
              in_.readFields(fmt);
              tmpOnLine = 0;
            }
            if (in_.stringField(tmpOnLine).length() == 0 || tmpOnLine >= in_.getNumberOfFields()) {
              if (t == 0 && val == 0) {
                isErrorOnFirstLine = true;
              }
              isResultsTruncated = true;
              timeError = ti;
              timeStepError = t;
              goOne = false;
              break;
            }
            r[pt][val].add(ti, in_.doubleField(tmpOnLine));
            tmpOnLine++;
          }
        }
        up.majAvancement();
        t++;
      }
    } catch (final NumberFormatException e) {
      analyze_.addFatalError(CtuluLib.getS("Le format du fichier {0} n'est pas correct ou le fichier est vide", "'"
          + fileName + "'"), in_.getLineNumber());
      return null;
    } catch (final EOFException e) {

    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    }
    final RubarTRCResult resu = new RubarTRCResult();
    resu.evols_ = r;
    return resu;
  }

  public double getTimeError() {
    return timeError;
  }

  public int getTimeStepError() {
    return timeStepError;
  }

  public boolean isErrorOnFirstLine() {
    return isErrorOnFirstLine;
  }
}
