/**
 * @creation 11 f�vr. 2004
 * @modification $Date: 2007-05-04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: SinusxFileFormat.java,v 1.15 2007-05-04 13:47:27 deniger Exp $
 */
public final class SinusxFileFormat extends FileFormatUnique {

  static final SinusxFileFormat INSTANCE = new SinusxFileFormat();
  
  private String version_="2.1";

  /**
   * @return singleton
   */
  public static SinusxFileFormat getInstance() {
    return INSTANCE;
  }

  private SinusxFileFormat() {
    super(1);
    extensions_ = new String[] { "sx" };
    id_ = "SINUSX";
    nom_ = "Sinusx";
    description_ = DodicoLib.getS("Comporte les d�finitions de points,polylignes et polygones");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }
  
  /**
   * Change de version de fichier.
   * @param _version "2.0" : Le fichier est �crit en format fixe. "2.1" : Format libre.
   */
  public void setVersionName(String _version) {
    version_=_version;
  }
  
  @Override
  public String getVersionName() {
    return version_;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new SinusxReader(this);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return super.write(_f, _source, _prog);
  }

  @Override
  public SinusxWriterAbstract createWriter() {
    if ("2.0".equals(version_))
      return new SinusxWriterV20(this);
    else
      return new SinusxWriterV21(this);
  }

}