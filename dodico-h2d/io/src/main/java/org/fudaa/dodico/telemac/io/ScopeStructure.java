package org.fudaa.dodico.telemac.io;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * Structure de donn�es utilis�e pour la lecture des fichiers scope.
 * 
 * @author Adrien Hadoux
 */
public abstract class ScopeStructure {

  public final static String NOM_FICHIER = "Chemin absolu";

  public final static ScopeKeyWord KEY = new ScopeKeyWord();

  //  public abstract void printResults();

  public abstract EvolutionReguliere[] returnEvolReguliere(File fichier);

  public abstract String getTitleFormFile();

  /**
   * Structure pour les fichiers s ou t Par convention les x sont les premieres variables.
   * 
   * @author Adrien
   */
  public static class SorT extends ScopeStructure {

    private Map<String, List<Double>> data_ = new HashMap<String, List<Double>>();

    private List<String> listeVariales_ = new ArrayList<String>();

    private List<String> titles_ = new ArrayList<String>();

    public String type;

    public SorT() {
    }

    public void addTitle(String ligne) {
      ligne = ligne.replace(KEY.getBlocCommentaireSorT(), "");
      titles_.add(ligne);
    }

    public void addVariable(String ligne) {

      ligne = ligne.replace(KEY.getBlocCommentaireSorT(), "");

      listeVariales_.add(ligne);
      // on init la liste des valeurs de la variabel correspondante
      data_.put(ligne, new ArrayList<Double>());
    }

    public int getNbVariables() {
      return listeVariales_.size();
    }

    public String getVariable(int _i) {
      return listeVariales_.get(_i);
    }

    public List<Double> getListValueForVariable(int i) {
      return data_.get(getVariable(i));
    }

    public double getValueOfvariable(int indexVar, int indexValue) {
      return getListValueForVariable(indexVar).get(indexValue);
    }

    public void addValueForVariable(double value, int indexVariable) {
      data_.get(listeVariales_.get(indexVariable)).add(value);
    }

    /**
     * Recupere la liste des x. Par convention les x sont les premieres variables.
     * 
     * @return
     */
    public List<Double> getAllX() {
      return data_.get(listeVariales_.get(0));
    }

    /**
     * retourne un x sp�cifi�.
     * 
     * @param i
     * @return
     */
    public double getX(int i) {
      return getAllX().get(i);
    }

    private void insertX(double x) {
      getAllX().add(x);
    }

    public double getXMax() {
      double max = getX(0);
      for (int i = 1; i < getAllX().size(); i++)
        if (max < getX(i))
          max = getX(i);
      return max;
    }

    public double getXMin() {
      double min = getX(0);
      for (int i = 1; i < getAllX().size(); i++)
        if (min > getX(i))
          min = getX(i);
      return min;
    }

    /**
     * Methode qui insert le x dans les variables que si il n'existe pas deja.
     * 
     * @param x
     */
    public void insertNewX(double x) {

      for (int i = 0; i < getAllX().size(); i++)
        if (x == getX(i))
          return;
      insertX(x);
    }

    /**
     * Retourne le nombre de valeur pour chaque variables
     * 
     * @return
     */
    public int getNbValuePerVariables() {

      if (listeVariales_.size() == 0)
        return 0;
      return data_.get(listeVariales_.get(0)).size();
    }

    /**
     * retourne une string qui contient toutes les valeurs a l indice value pour toutes les variables
     * 
     * @param indice dans les tableaux des variables
     * @return
     */
    public String getListValuesofAllVariable(int indiceValue) {
      String brochetteVal = "";

      for (int i = 0; i < getNbVariables(); i++) {
        if (i > 0)
          brochetteVal += " ";
        if (data_.get(listeVariales_.get(i)).get(indiceValue) != KEY.VALUE_UNDEFINED)
          brochetteVal += data_.get(listeVariales_.get(i)).get(indiceValue);
        else brochetteVal += KEY.SYMBOL_VALUE_UNDEFINED;

      }
      return brochetteVal;
    }

    /**
     * Transforme la structure en evol regulieres. correction PREPRO-15
     * 
     * @author Adrien Hadoux
     * @return
     */
    @Override
    public EvolutionReguliere[] returnEvolReguliere(File fichier) {
      EvolutionReguliere[] evolReg = new EvolutionReguliere[getNbVariables() - 1];

      // -- on parcours toutes les variables --//
      for (int i = 1; i < getNbVariables(); i++) {
        String nomVar = getVariable(i);

        List<Double> listeX = getAllX();
        List<Double> listeY = getListValueForVariable(i);

        double[] tabX = new double[listeX.size()];
        double[] tabY = new double[listeY.size()];

        for (int j = 0; j < listeX.size(); j++) {
          tabX[j] = listeX.get(j);
          tabY[j] = listeY.get(j);

        }

        evolReg[i - 1] = new EvolutionReguliere(tabX, tabY, false);
        evolReg[i - 1].setNom("courbe de " + nomVar.replace("  ", ""));
        evolReg[i - 1].isScope_ = true;
        //-- ajout des infos specifiques --//
        Map infos = evolReg[i - 1].infos_;
        infos.put("Type", "Profil scop");
        infos.put("Fichier scop", fichier.getName());
        infos.put(NOM_FICHIER, fichier.getAbsolutePath());
        infos.put("Variable", nomVar.replace("  ", ""));

      }
      return evolReg;
    }

    /**
     * Methode qui restreint lq structure en ne prenant que les variqbles choisies et la plage de x.
     * 
     * @param selectedVariables
     * @param xMin
     * @param xMax
     */
    public void restreindreStructure(int[] selectedVariables, double xMin, double xMax) {

      //--on retire toutes les variables qui ne sont pas voulues --//
      Map<String, List<Double>> newdatas_ = new HashMap<String, List<Double>>();
      ArrayList<String> newVariables = new ArrayList<String>();

      //ajout des x
      newVariables.add(getVariable(0));
      newdatas_.put(getVariable(0), getAllX());

      for (int i = 0; i < selectedVariables.length; i++) {
        //la bonne variable est selectedVariables[i]+1 car dans le choix on avait enlev� les X
        newdatas_.put(getVariable(selectedVariables[i] + 1), getListValueForVariable(selectedVariables[i] + 1));
        newVariables.add(getVariable(selectedVariables[i] + 1));
      }

      //on retire les x qui ne sont pas les plages 
      for (int k = 0; k < getNbValuePerVariables(); k++) {
        if (getX(k) > xMax || getX(k) < xMin) {
          for (String var : newVariables) {
            newdatas_.get(var).remove(k);
          }
          k--;
        }
      }

      //-- on remplace les nouvelles valeurs --//
      data_ = newdatas_;
      listeVariales_ = newVariables;

      //test

    }

    @Override
    public String getTitleFormFile() {
      // TODO Auto-generated method stub
      String title = "";
      for (String partie : titles_)
        title += " " + partie;

      return title;
    }

  }

  /**
   * @author Adrien Hadoux. Classe les donnees par pas de temps. Chaque pas de temps contient une liste de variables.
   *         chaque variable contient des valeurs.
   */
  public static class Gene extends ScopeStructure {

    /**
     * Map qui donne a chaque separator sa liste de valeurs pour chaque variables.
     */
    private Map<String, ArrayList<ArrayList<Double>>> data_ = new HashMap<String, ArrayList<ArrayList<Double>>>();

    private List<String> listeSeparator = new ArrayList<String>();

    private List<String> listeVariales_ = new ArrayList<String>();

    private List<String> titles_ = new ArrayList<String>();

    public String type = ScopeKeyWord.TYPE_COURBE_MIXTE;

    public Gene() {
      // ajout par defaut des variables x
      listeVariales_.add("X");
    }

    public void addTitle(String ligne) {
      ligne = ligne.replace(KEY.getBlocCommentaireGENE(), "");
      titles_.add(ligne);
    }

    public void addVariable(String ligne) {
      ligne = ligne.replace(KEY.getBlocCommentaireGENE(), "");
      listeVariales_.add(ligne);
    }

    public ArrayList<Double> getAllX(String pdt) {
      return data_.get(pdt).get(0);
    }

    /**
     * retourne un x sp�cifi�.
     * 
     * @param i
     * @return
     */
    public double getX(int i, String pdt) {
      return getAllX(pdt).get(i);
    }

    private void insertX(double x, String pdt) {
      getAllX(pdt).add(x);
    }

    public double getXMax() {
      double max = getX(0, listeSeparator.get(0));

      for (int j = 0; j < listeSeparator.size(); j++)
        for (int i = 1; i < getAllX(listeSeparator.get(j)).size(); i++)
          if (max < getX(i, listeSeparator.get(j)))
            max = getX(i, listeSeparator.get(j));
      return max;
    }

    public double getXMin() {
      double min = getX(0, listeSeparator.get(0));

      for (int j = 0; j < listeSeparator.size(); j++)
        for (int i = 1; i < getAllX(listeSeparator.get(j)).size(); i++)
          if (min > getX(i, listeSeparator.get(j)))
            min = getX(i, listeSeparator.get(j));
      return min;
    }

    /**
     * Methode qui insert le x dans les variables que si il n'existe pas deja.
     * 
     * @param x
     */
    public void insertNewX(double x, String pdt) {

      for (int i = 0; i < getAllX(pdt).size(); i++)
        if (x == getX(i, pdt))
          return;
      insertX(x, pdt);
    }

    @Override
    public String getTitleFormFile() {
      // TODO Auto-generated method stub
      String title = "";
      for (String partie : titles_)
        title += " " + partie;

      return title;
    }

    public void addSeparator(String ligne) {
      ligne = ligne.replace(KEY.getBlocCommentaireGENE(), "");

      listeSeparator.add(ligne);

      // on ajoute un enregistrment dans data pour le pas de temps
      // correspondant
      ArrayList<ArrayList<Double>> listeDuPdt = new ArrayList<ArrayList<Double>>();
      data_.put(ligne, listeDuPdt);

      // --il faut initialiser cette liste du pas de temps en y ajoutant autant
      // de listes qu il y a de variables --//
      for (int i = 0; i < getNbVariables(); i++)
        listeDuPdt.add(new ArrayList<Double>());

    }

    // public void addVariableForPDt(String labelVariable, int pdt) {
    // labelVariable = labelVariable.replace(KEY.getBlocCommentaireGENE(), "");
    // listeVariales_.add(labelVariable);
    // getListeVarForPasDeTemps(pdt).add(new ArrayList<Double>());
    //
    // }

    public int getNbSeparator() {
      return listeSeparator.size();
    }

    public int getNbVariables() {
      return listeVariales_.size();
    }

    public ArrayList<ArrayList<Double>> getListeVarForSeparator(String pdt) {
      return data_.get(pdt);
    }

    public ArrayList<ArrayList<Double>> getListeVarForSeparator(int pdt) {
      return data_.get(listeSeparator.get(pdt));
    }

    public ArrayList<Double> getListValueForVariableForSeparator(String separator, int var) {

      return getListeVarForSeparator(separator).get(var);
    }

    public int getVariablePosition(String nomVar) {
      for (int i = 0; i < getNbVariables(); i++)
        if (getVariable(i).equals(nomVar))
          return i;
      return -1;
    }

    public void addValueForVariableAtSeparator(double value, int indexVariable, String nameSeparator) {
      ArrayList<Double> listeValueOfVar = getListValueForVariableForSeparator(nameSeparator, indexVariable);
      // if (listeValueOfVar == null)
      // listeValueOfVar = new ArrayList<Double>();

      listeValueOfVar.add(value);
    }

    /**
     * tailel par defaut des donnees pour une variale donn� pour un pas de temps donn�
     * 
     * @return
     */
    // public int getNbValuePerVariablesPerPdt() {
    //
    // return getListValueForVariableForPDT(listePasDeTemps.get(0), 0).size();
    // }

    /**
     * retourne l equivalent d une ligne du fichier qui contient les valeurs pour chaque variables pour un pas de temps
     * donn�.
     * 
     * @param separator
     * @return
     */
    public String getAllValuesForVariableAndSeparator(String separator, int indiceValue) {

      String ligneBrochette = "";
      for (int i = 0; i < listeVariales_.size(); i++) {
        ArrayList<Double> listeValueOfVar = getListValueForVariableForSeparator(separator, i);
        if (i != 0)
          ligneBrochette += " ";
        if (listeValueOfVar.size() <= indiceValue)
          ligneBrochette += KEY.SYMBOL_VALUE_UNDEFINED;
        else if (listeValueOfVar.get(indiceValue) != KEY.VALUE_UNDEFINED)
          ligneBrochette += listeValueOfVar.get(indiceValue);
        else ligneBrochette += KEY.SYMBOL_VALUE_UNDEFINED;
      }

      return ligneBrochette;
    }

    public String getSeparator(int i) {
      return listeSeparator.get(i);
    }

    public String getVariable(int i) {
      return listeVariales_.get(i);
    }

    public int getNbTitles() {
      return titles_.size();
    }

    public String getTitle(int i) {
      return titles_.get(i);
    }

    /**
     * Retourne les evolutions regulieres des profils spatiaux.
     */
    @Override
    public EvolutionReguliere[] returnEvolReguliere(File fichier) {
      EvolutionReguliere[] evolReg = new EvolutionReguliere[getNbSeparator() * (getNbVariables() - 1)];

      // -- on parcours tous les pas de temps --//
      for (int h = 0; h < getNbSeparator(); h++) {

        String pdt = getSeparator(h);

        // -- on parcours toutes les variables et non les x --//
        for (int i = 0; i < getNbVariables() - 1; i++) {
          String nomVar = getVariable(i + 1);

          List<Double> listeX = getAllX(pdt);
          List<Double> listeY = getListValueForVariableForSeparator(pdt, i + 1);

          double[] tabX = new double[listeX.size()];
          double[] tabY = new double[listeY.size()];

          for (int j = 0; j < listeX.size(); j++) {
            tabX[j] = listeX.get(j);
            tabY[j] = listeY.get(j);

          }

          evolReg[i] = new EvolutionReguliere(tabX, tabY, false);
          evolReg[i].setNom("courbe de " + nomVar.replace("  ", ""));
          evolReg[i].setUnite(nomVar);
          evolReg[i].isScope_ = true;
          //-- ajout des infos specifiques --//
          //       Map infos= evolReg[i].infos_;
          //       infos.put("Type", "Profil spatial scopGene");
          //       infos.put("Fichier scopGENE", fichier.getName());
          //       infos.put(NOM_FICHIER, fichier.getAbsolutePath());
          //       infos.put("Variable", nomVar.replace("  ", ""));
          //       infos.put("Plage de valeurs", pdt);
        }

      }
      return evolReg;
    }

    /**
     * Methode qui essaie d'extraire le nombre de la chaine de separator (utilis� pour les evol temporelles). Si n'y
     * arrive pas, retourne l'indice du separateur. Normalement les separateurs sont de la forme suivante: VAR = VALUE
     * c'est value qui nous interesse.
     * 
     * @param indiceSeparator
     * @return
     */
    public double getValueExtractOfSeparator(int indiceSeparator) {

      String separator = getSeparator(indiceSeparator);

      if (separator.lastIndexOf(' ') != -1) {
        String sub = separator.substring(separator.lastIndexOf(' '), separator.length());

        try {
          double value = Double.parseDouble(sub);

          return value;

        } catch (NumberFormatException e) {
          return indiceSeparator;
        }

      }
      return indiceSeparator;
    }

    /**
     * A utiliser poru transformer scopgen en evol temporelles. On cree une courbe par variables, et on prend les points
     * de tous les separaor pou les former.
     * 
     * @return
     */
    public EvolutionReguliere[] returnEvolRegulierePourTemporelles(double xVoulu, File fichier) {
      EvolutionReguliere[] evolReg = new EvolutionReguliere[getNbVariables() - 1];

      // -- on parcours toutes les variables et non les x, on a une seule variable --//
      for (int i = 0; i < getNbVariables() - 1; i++) {
        //-- on recupere les variables i+1 car la var 0 = les X --//  
        String nomVar = getVariable(i + 1);

        List<Double> listeTOTALX = new ArrayList<Double>();
        List<Double> listeTOTALY = new ArrayList<Double>();

        for (int k = 0; k < getNbSeparator(); k++) {

          //on recupere les x de ce separator
          List<Double> listX = getListValueForVariableForSeparator(getSeparator(k), 0);
          //on recupere tous les y de la variable de la courbe
          List<Double> listY = getListValueForVariableForSeparator(getSeparator(k), i + 1);

          //-- le x correspond � la valeur du caractere de separation--//
          double x = getValueExtractOfSeparator(k);

          //-- il faut recuperer le y le plus proche du x voulu en regardant les indices --//

          //-- recuperation de l'indice du x le plus proche --//
          int indiceXproche = 0;
          double min = Math.abs(xVoulu - listX.get(0));
          for (int g = 0; g < listX.size(); g++)
            if (min > Math.abs(xVoulu - listX.get(g))) {
              min = Math.abs(xVoulu - listX.get(g));
              indiceXproche = g;
            }

          double y = listY.get(indiceXproche);

          //on ajoute tout cela au x et y totaux
          listeTOTALX.add(x);
          listeTOTALY.add(y);

        }

        //creation de l evol reguliere
        double[] tabX = new double[listeTOTALX.size()];
        double[] tabY = new double[listeTOTALX.size()];
        for (int j = 0; j < listeTOTALX.size(); j++) {
          tabX[j] = listeTOTALX.get(j);
          tabY[j] = listeTOTALY.get(j);
        }
        evolReg[i] = new EvolutionReguliere(tabX, tabY, false);
        evolReg[i].setNom(nomVar.replace("  ", "") + " sur [" + getSeparator(0).replace("  ", "") + ";"
            + getSeparator(getNbSeparator() - 1).replace("  ", "") + "] pour X choisi=" + xVoulu);
        evolReg[i].setUnite(nomVar);
        evolReg[i].isScope_ = true;

        evolReg[i].titreAxeX_ = "T";
        evolReg[i].uniteAxeX_ = "secondes";
        evolReg[i].titreAxeY_ = nomVar.replace("  ", "");
        evolReg[i].uniteAxeY_ = null;
        evolReg[i].setxAxeIsTime(true);

        //-- ajout des infos specifiques --//
        Map infos = evolReg[i].infos_;
        infos.put("Type", "Profil temporel scopGene");
        infos.put("Fichier scopGENE", fichier.getName());
        infos.put(NOM_FICHIER, fichier.getAbsolutePath());
        infos.put("Variable", nomVar.replace("  ", ""));
        infos.put("X choisi", xVoulu);
        infos.put("Intervalle", "[" + getSeparator(0).replace("  ", "") + ";" + getSeparator(getNbSeparator() - 1).replace("  ", "") + "]");
      }

      return evolReg;
    }

    /**
     * Methode reservee pour les correlations, on genere une courbe a la fois. les x seront la variable du separator
     * specifi� les Y idem
     * 
     * @return
     */
    public EvolutionReguliere[] returnEvolReguliereCorrelation(int varX, int varY, int separatorX, int separatorY, File fichier) {
      EvolutionReguliere[] evolReg = new EvolutionReguliere[1];

      List<Double> listeX = getListValueForVariableForSeparator(getSeparator(separatorX), varX + 1);
      List<Double> listeY = getListValueForVariableForSeparator(getSeparator(separatorY), varY + 1);

      double[] tabX = new double[Math.min(listeX.size(), listeY.size())];
      double[] tabY = new double[Math.min(listeX.size(), listeY.size())];

      for (int j = 0; j < Math.min(listeX.size(), listeY.size()); j++) {
        tabX[j] = listeX.get(j);
        tabY[j] = listeY.get(j);

      }

      evolReg[0] = new EvolutionReguliere(tabX, tabY, false);
      evolReg[0].setNom("Correlation:(X=" + getVariable(varX + 1).replace("  ", "") + "//Y=" + getVariable(varY + 1).replace("  ", "") + ")");
      evolReg[0].setUnite(getVariable(varX + 1));
      evolReg[0].isScope_ = true;

      evolReg[0].titreAxeX_ = getVariable(varX + 1).replace("  ", "");
      evolReg[0].uniteAxeX_ = null;
      evolReg[0].titreAxeY_ = getVariable(varY + 1).replace("  ", "");
      evolReg[0].uniteAxeY_ = null;

      //-- ajout des infos specifiques --//
      Map infos = evolReg[0].infos_;
      infos.put("Type", "Correlation scopGene");
      infos.put("Fichier scopGENE", fichier.getName());
      infos.put(NOM_FICHIER, fichier.getAbsolutePath());
      infos.put("Variable X", getVariable(varX + 1).replace("  ", "") + " pour la plage " + getSeparator(separatorX));
      infos.put("Variable Y", getVariable(varY + 1).replace("  ", "") + " pour la plage " + getSeparator(separatorY));

      return evolReg;
    }

    /**
     * Methode qui reduit la structure en ne conservant que les separator de debut et de fin. la liste des variables
     * dont les indices doivents etre incr�ment�s car pas de x. et ne garder que le x choisi et interpoler les valeur de
     * facon lin�aire.
     * 
     * @param debSeparator
     * @param finSeparator
     * @param listVariables
     * @param xchoosen
     */
    public void restreindreStructure(int debSeparator, int finSeparator, int[] listVariables, double xchoosen) {

      //on reduit les separator a la liste voulue 
      restreindreStructure(debSeparator, finSeparator);

      //on reduit les variables
      restreindreStructure(listVariables);

      //on interpole pour le x choisi
      //restreindreStructure(xchoosen);

    }

    public void restreindreStructure(int debSeparator, int finSeparator) {
      Map<String, ArrayList<ArrayList<Double>>> newdata_ = new HashMap<String, ArrayList<ArrayList<Double>>>();
      List<String> newlisteSeparator = new ArrayList<String>();

      // on ajoute les separator de la plage
      for (int i = 0; i < getNbSeparator(); i++) {
        if (i >= debSeparator && i <= finSeparator) {
          String newSepa = getSeparator(i);
          newlisteSeparator.add(newSepa);
          newdata_.put(newSepa, getListeVarForSeparator(i));
        }
      }
      listeSeparator = newlisteSeparator;
      data_ = newdata_;

    }

    public void restreindreStructure(int[] listVariables) {
      Map<String, ArrayList<ArrayList<Double>>> newdata_ = new HashMap<String, ArrayList<ArrayList<Double>>>();
      List<String> newlisteVariables = new ArrayList<String>();

      //on ajoute par defaut les x
      newlisteVariables.add(getVariable(0));

      //on ajout les news variabels
      for (int j = 0; j < listVariables.length; j++)
        newlisteVariables.add(getVariable(listVariables[j] + 1));

      // on ajoute uniquement les datas de ces variables
      for (int i = 0; i < getNbSeparator(); i++) {

        ArrayList<ArrayList<Double>> newVariablePerValidator = new ArrayList<ArrayList<Double>>();

        //on ajoute tout dabord tous les x
        newVariablePerValidator.add(getAllX(getSeparator(i)));

        for (int j = 0; j < listVariables.length; j++) {
          //il faut incrementer de 1 car pas de X pris en compte pour les choix de var or ici c est l'elt 0
          int selectedVar = listVariables[j] + 1;
          newVariablePerValidator.add(getListValueForVariableForSeparator(getSeparator(i), selectedVar));

        }
        newdata_.put(getSeparator(i), newVariablePerValidator);

      }
      listeVariales_ = newlisteVariables;
      data_ = newdata_;

    }

    public void restreindreStructure(double xchoosen) {

      for (int i = 0; i < getNbSeparator(); i++) {

        ArrayList<Double> listeX = getAllX(getSeparator(i));

        int indiceProche = 0;
        double xProche = listeX.get(0);
        //etape 1 on recherche le x le plus proche du xchoosen
        for (int k = 1; k < listeX.size(); k++) {
          double test = listeX.get(k);
          if (Math.abs(test - xchoosen) < Math.abs(xProche - xchoosen)) {
            xProche = test;
            indiceProche = k;
          }
        }

        //on ne conserve dans chaque liste des variables que la valeur correspondante au xProche
        for (int j = 0; j < getNbVariables(); j++) {

          ArrayList<Double> listeValueVar = getListValueForVariableForSeparator(getSeparator(i), j);
          double conservVar = listeValueVar.get(indiceProche);
          for (int h = 0; h < listeValueVar.size();)
            listeValueVar.remove(h);
          listeValueVar.add(conservVar);

        }
      }

    }

  }

}
