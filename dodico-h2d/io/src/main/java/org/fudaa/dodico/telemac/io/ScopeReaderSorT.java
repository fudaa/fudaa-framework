package org.fudaa.dodico.telemac.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Classe reader de fichier au format scope. Utilise fortranReader
 * @author Adrien Hadoux
 * 
 */
public class ScopeReaderSorT extends FileOpReadCharSimpleAbstract implements CtuluActivity {

  
  FileFormatVersionInterface v_;
  private String name_;
  ScopeKeyWord key = new ScopeKeyWord();
  private int nbOctets_;
  private String extension_;
  public ScopeReaderSorT(final FileFormatVersionInterface _v) {
    v_ = _v;
  }
  
  
  @Override
  protected void processFile(final File _f) {
    name_ = CtuluLibFile.getSansExtension(_f.getName());
    nbOctets_ = (int) _f.length();
    extension_ = CtuluLibFile.getExtension(_f.getName());
  }
  
  @Override
  protected Object internalRead() {
    return readStructure();
  }

  @Override
  public void stop() {
  }

  
  public void setProgression(int val, String desc) {
    if (progress_ != null) {
      progress_.setProgression(val);
      progress_.setDesc(desc);
    }
  }
  
  /**
   * 
   * Retourne une map. La premiere clef indique le type de data (tempo,spatial
   * ou mixte, cf var static de ScopeKeyWord) les 3 clef suivantes contiennent
   * les titres ensuite chaque paire contient le titre de la variables et ses
   * valeurs associ�es sous arraylist de doubles.
   * 
   * @return
   */
  ScopeStructure.SorT readStructure() {

    
    if (super.in_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }
    
    ScopeStructure.SorT structure = new ScopeStructure.SorT();
   
    String ligne = null;
    
    try {
    if ((progress_ != null) && (nbOctets_ > 0)) {
      setProgression(0, "R�cup�ration des titres et nons des variables");
    }
    
    // -- creation du type de courbe contenu dans les datas --//
      structure.type = extension_;
   
      readHeader(structure);
    
      
      setProgression(30, "R�cup�ration des donn�es");
    
    
    // -- on lit toutes les valeurs pour une variable jusqu a l a fin de fichier--//
    try{
    readData(structure);
    
    
    }catch(EOFException Exc){
      
      
      return structure;
    }
    
      
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    return null;
  }
  
  
  
  public void readHeader(ScopeStructure.SorT structure) throws IOException {
    String ligne = null;
    int cpt = 0;
    do {
    in_.readFields();
    
    ligne = in_.getLine();
    
    if (key.isBlocCommentaireSorT(ligne) && cpt < 3)
        // titre
        structure.addTitle(ligne);
      else if (key.isBlocCommentaireSorT(ligne)) {
        // nom de variable
        structure.addVariable(ligne);
      }
      cpt++;
  } while (key.isBlocCommentaireSorT(ligne));
  
  }
  
  
  public void readData(ScopeStructure.SorT structure) throws IOException {
    
    while (true) {

      if (!key.isBlocCommentaireSorT(in_.getLine())) {
        // -- on recupere toutes les valeurs de la variable --//
        for (int i = 0; i < structure.getNbVariables(); i++) {
          double value = key.VALUE_UNDEFINED;
          if (!key.isUndefined(in_.stringField(i)))
          value = in_.doubleField(i);
          // ajout dans la map
          structure.addValueForVariable(value, i);
        }
      }
      in_.readFields();

    }
  }
    
  
  
}
