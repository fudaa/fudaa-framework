/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-09-19 14:45:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarMAIReader.java,v 1.8 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarMAIReader extends FileOpReadCharSimpleAbstract {
  int nbDecimal_ = -1;
  /**
   * if true, the format used to read/save is coded on 12 digits.
   */
  private boolean newFormat;

  /**
   * @return le nombre de decimal lue.
   */
  public final int getNbDecimal() {
    return nbDecimal_;
  }

  public RubarMAIReader() {
  }

  @Override
  protected Object internalRead() {
    nbDecimal_ = -1;
    H2dRubarGrid r = null;
    EfElementType type = null;
    try {
      // LECTURE DES ELEMENTS
      in_.readFields();
      // lecture ne
      final int ne = in_.intField(0);
      newFormat = in_.getLine().length() == 9;
      // initialisation donnees
      int tmpOnLineElt = 0;
      final EfElementVolume[] elt = new EfElementVolume[ne];
      int[] fmt = getElementLinesFormat(newFormat);
      for (int ie = 0; ie < ne; ie++) {
        in_.readFields(fmt);
        final int nne = in_.intField(0);
        if (ie == 0) {
          type = EfElementType.getCommunType(nne);
        } else if (type != null) {
          final EfElementType neType = EfElementType.getCommunType(nne);
          // le type est different
          if (neType != type
              && !(type == EfElementType.T3_Q4 && (neType == EfElementType.T3 || neType == EfElementType.Q4))) {
            // pour rubar, on peut avoir des T3 et Q4
            if ((type == EfElementType.T3 && neType == EfElementType.Q4)
                || (neType == EfElementType.T3 && type == EfElementType.Q4)) {
              type = EfElementType.T3_Q4;
            } else {
              type = null;
              FuLog.warning("grid type unknown");
            }
          }
        }
        final int[] ine = new int[nne];
        tmpOnLineElt = 1;
        for (int j = 0; j < nne; j++) {
          ine[j] = in_.intField(tmpOnLineElt++) - 1;
        }
        // maj de l'element correspondant
        elt[ie] = new EfElementVolume(null, ine);
      }
      if (progress_ != null) {
        progress_.setProgression(40);
      }
      // LECTURE DES POINTS
      // nn
      in_.readFields();
      final int nn = in_.intField(0);
      fmt = new int[10];
      if (newFormat) {
        Arrays.fill(fmt, 13);
      } else {
        Arrays.fill(fmt, 8);
      }

      final EfNode[] nds = new EfNode[nn];
      final double[] x = new double[nn];
      final double[] y = new double[nn];
      // double[] z = new double[nn];
      // xn
      readPt(fmt, /*nbFieldByLine,*/ nn, x, y);
      // affectation des valeurs dans les noeuds
      for (int i = 0; i < nn; i++) {
        nds[i] = new EfNode(x[i], y[i], 0);
      }

      r = new H2dRubarGrid(nds, elt, null, type);
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return r;
  }

  protected static int[] getElementLinesFormat(boolean newFormat) {
    return newFormat ? new int[]{9, 9, 9, 9, 9} : new int[]{6, 6, 6, 6, 6};
  }

  private void readPt(final int[] _fmt, /*int _nbFieldByLine,*/ final int _nn, final double[] _x, final double[] _y) throws IOException {
    in_.readFields(_fmt);
    int tmpOnLinePt = 0;
    final int nbFieldByLine = _fmt.length;
    for (int i = 0; i < _nn; i++) {
      if (tmpOnLinePt == nbFieldByLine) {
        in_.readFields(_fmt);
        tmpOnLinePt = 0;
      }
      if (nbDecimal_ < 0) {
        final String l = in_.stringField(tmpOnLinePt);
        int idx = l.indexOf('.');
        // normalement inutile car le point est toujours utilise
        if (idx < 0) {
          idx = l.indexOf(',');
        }
        // les indices commencent a 0 ... d'ou le -1
        nbDecimal_ = l.length() - idx - 1;
      }
      _x[i] = in_.doubleField(tmpOnLinePt);
      tmpOnLinePt++;
    }
    // yn
    in_.readFields(_fmt);
    tmpOnLinePt = 0;
    for (int i = 0; i < _nn; i++) {
      if (tmpOnLinePt == nbFieldByLine) {
        in_.readFields(_fmt);
        tmpOnLinePt = 0;
      }
      _y[i] = in_.doubleField(tmpOnLinePt);
      tmpOnLinePt++;
    }
  }

  public boolean isNewFormat() {
    return newFormat;
  }
}
