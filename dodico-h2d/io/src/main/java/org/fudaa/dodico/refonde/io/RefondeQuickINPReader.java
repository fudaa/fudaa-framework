/**
 * @creation 21 mars 2003
 * @modification $Date: 2007-06-29 15:10:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.refonde.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.impl.EFGridArrayZ;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * @author deniger
 * @version $Id: RefondeQuickINPReader.java,v 1.2 2007-06-29 15:10:31 deniger Exp $
 */
public class RefondeQuickINPReader extends FileOpReadCharSimpleAbstract implements CtuluActivity {

  String baseFile_;
  String ficName_;

  boolean stop_;
  boolean readOnlyGrid_;

  @Override
  public void stop() {
    stop_ = true;

  }

  /**
   * le reader : format par defaut (le dernier).
   */
  public RefondeQuickINPReader() {}

  /**
   * Lecture difficile du format INP.
   */
  private Object readINP() {
    stop_ = false;
    RefondeINPResultDefault projet = null;
    // Si pas de flux d'entree
    if (in_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Flux d'entr�e non trouv�"));
      return null;
    }
    // On ignore les lignes blanches
    in_.setJumpBlankLine(true);
    in_.setBlankZero(true);
    try {
      projet = new RefondeINPResultDefault();

      // COMT
      in_.readLine();
      // 4 ligne d'entete
      in_.readLine();
      String line = in_.readLine();
      boolean isSeiche = line.indexOf("SEICHE") > 0;
      projet.setModuleSeiche(isSeiche);
      in_.readLine();
      in_.readLine();
      // SPEC
      String bloc = in_.readLine().trim();
      // SPEC
      // 6 ! crit�re de d�ferlement
      // 10 ! Nbre de p�riode (NP)
      // 5.0 ! p�riode minimale
      // 15.0 ! p�riode maximale
      // 10 ! Nbre de direction (ND)
      // 165.0 ! direction minimale
      // 195.0 ! direction maximale
      if ("SPEC".equals(bloc)) {
        // premier ligne inconnue
        in_.readLine();
        int nbPeriode = Integer.parseInt(in_.readLine().trim());
        // nb periode
        projet.setNbPeriodeHoule(nbPeriode);
        // truc bidule
        // min periode
        in_.readLine();
        // max si plusieurs periodes
        if (nbPeriode > 1) in_.readLine();
        // le nombre d'angle
        int nbDirection = Integer.parseInt(in_.readLine().trim());
        projet.setNbDirection(nbDirection);
        // direction min
        in_.readLine();
        if (nbDirection > 1) in_.readLine();
        bloc = in_.readLine().trim();
      } else if (!isSeiche) {
        projet.setSpecUnknwon(true);
      }
      if (!"DATA".equals(bloc)) {
        analyze_.addFatalError("Fichier refonde inp corromu: bloc DATA non trouv�");
        return null;
      }
      int[] fmt = new int[] { 5, 5, 5, 10, 5 };
      in_.readLine();
      // le nombre de noeuds peut etre donn�e sur 5 caract�res: la m...
      // une remise � plat serait cool......
      if (in_.getLine().length() != CtuluLibArray.getSum(fmt)) {
        fmt[3] = 5;
      }
      in_.analyzeCurrentLine(fmt);
      if (!"COOR".equals(in_.stringField(1).trim())) {
        analyze_.addFatalError("Fichier refonde inp corromu: bloc COOR non trouv�");
        return null;
      }
      int nbPt = in_.intField(3);
      if (nbPt < 0) {
        analyze_.addFatalError("Fichier refonde inp corromu: nombre de points n�gatifs");
        return null;
      }
      fmt = new int[] { 10, 10, 10 };
      EfNode[] nds = new EfNode[nbPt];
      // on lit les facteurs d'echelle si necessaire.
      if (!projet.isModuleSeiche_) in_.readLine();
      for (int i = 0; i < nbPt; i++) {
        in_.readFields(fmt);
        nds[i] = new EfNode(in_.doubleField(0), in_.doubleField(1), in_.doubleField(2));
      }
      fmt = new int[] { 5, 5, 10, 5, 5, 5, 5, 5, 5, 5, 5 };
      if (isSeiche) {
        fmt = new int[] { 5, 5, 10, 5, 5, 5, 5, 5 };
      }
      in_.readLine();
      // meme bordel ...
      boolean is5 = false;
      if (in_.getLine().length() != CtuluLibArray.getSum(fmt)) {
        is5 = true;
        fmt[2] = 5;
      }
      in_.analyzeCurrentLine(fmt);

      if (!"ELEM".equals(in_.stringField(1))) {
        analyze_.addFatalError("Fichier refonde inp corromu: bloc ELEM non trouv�");
        return null;
      }
      int nbElt = in_.intField(2);
      int nbMaxPerElt = in_.intField(3);
      if (nbElt < 0 || nbMaxPerElt < 0) {
        analyze_.addFatalError("Fichier refonde inp corromu: nombre d'elts n�gatifs");
        return null;
      }
      List elt = new ArrayList(nbElt);
      // Connectivit� des �l�ments (super-�lement inclu)
      fmt = new int[8];
      Arrays.fill(fmt, is5 ? 5 : 10);
      for (int i = 0; i < nbElt; i++) {
        in_.readFields(fmt);
        int nbNds = in_.getNumberOfNotEmptyField();
        if (nbNds < 3) break;
        int[] ndsElt = new int[nbNds];
        for (int j = 0; j < nbNds; j++) {
          ndsElt[j] = in_.intField(j) - 1;
        }
        elt.add(new EfElement(ndsElt));
      }
      projet.grid_ = new EFGridArrayZ(nds, (EfElement[]) elt.toArray(new EfElement[elt.size()]));

    } catch (NumberFormatException _e) {
      analyze_.manageException(_e);
      return null;
    } catch (IOException _ioe) {
      analyze_.manageException(_ioe);
      return null;
    }

    return projet;
  }

  @Override
  protected Object internalRead() {
    return readINP();
  }

  @Override
  public void setFile(final File _f) {
    super.setFile(_f);
    baseFile_ = _f.getAbsoluteFile().getParentFile().getAbsolutePath();
    ficName_ = CtuluLibFile.getSansExtension(_f.getName());
  }

  public boolean isReadOnlyGrid() {
    return readOnlyGrid_;
  }

  public void setReadOnlyGrid(final boolean _readOnlyGrid) {
    readOnlyGrid_ = _readOnlyGrid;
  }
}