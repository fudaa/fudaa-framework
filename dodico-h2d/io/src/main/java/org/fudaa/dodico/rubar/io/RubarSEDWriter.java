package org.fudaa.dodico.rubar.io;

import java.io.IOException;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentInterface;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCouche;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCoucheContainer;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarSEDWriter extends FileOpWriterCharSimpleAbstract {
  private void writeSEDResult(final H2dRubarSedimentInterface result) {
    try {
      final FortranWriter w = new FortranWriter(out_);
      w.setSpaceBefore(true);
      w.setStringQuoted(false);

      final int nbTimes = result.getNbTimes();
      final int nbNoeuds = result.getNbNoeuds();
      final int[] fmtTime = new int[] { 15 };
      final int[] fmtNoeud = RubarSEDReader.FMT_COUCHES;

      for (int i = 0; i < nbTimes; i++) {
        w.doubleField(0, result.getTimes(i));
        w.writeFields(fmtTime);

        for (int j = 0; j < nbNoeuds; j++) {
          H2dRubarSedimentCoucheContainer coucheContainer = result.getCoucheContainer(i, j);
          int k = 0;
          for (int c = 0; c < coucheContainer.getNbCouches(); c++) {
            H2dRubarSedimentCouche coucheItem = coucheContainer.getCoucheItem(c);
            w.doubleField(k++, coucheItem.getEpaisseur());
            w.doubleField(k++, coucheItem.getDiametre());
            w.doubleField(k++, coucheItem.getEtendue());
            w.doubleField(k++, coucheItem.getContrainte());
          }
          w.writeFields(fmtNoeud);
        }
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  @Override
  protected void internalWrite(final Object o) {
    if (o instanceof H2dRubarSedimentInterface) {
      writeSEDResult((H2dRubarSedimentInterface) o);
    } else {
      donneesInvalides(o);
    }
  }
}