/**
 * @creation 27 sept. 2004
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

import org.fudaa.dodico.h2d.H2dParallelogrammeDataAbstract;

/**
 * @author Fred Deniger
 * @version $Id: RubarParallelogrammeData.java,v 1.14 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarParallelogrammeData extends H2dParallelogrammeDataAbstract {

  CtuluArrayDouble[] values_;

  @Override
  public CtuluCollectionDoubleEdit getValues(final int _valueIdx) {
    return values_[_valueIdx];
  }

  @Override
  public int getNbValues() {
    return values_ == null ? 0 : values_.length;
  }

  @Override
  public double getValue(final int _valueIdx, final int _x) {
    return values_[_valueIdx].getValue(_x);
  }

  @Override
  protected void setX1(final double _x1) {
    super.setX1(_x1);
  }

  @Override
  protected void setX2(final double _x2) {
    super.setX2(_x2);
  }

  @Override
  protected void setX4(final double _x4) {
    super.setX4(_x4);
  }

  @Override
  protected void setY1(final double _y1) {
    super.setY1(_y1);
  }

  /**
   * redéfinies pour des questions d'accessibilité.
   */
  @Override
  protected void setY2(final double _y2) {
    super.setY2(_y2);
  }

  @Override
  protected void setY4(final double _y4) {
    super.setY4(_y4);
  }

  @Override
  public boolean isValuesDefined() {
    return values_ != null;
  }

}