/**
 * @creation 19 mars 2003
 * @modification $Date: 2007-05-04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.IOException;
import java.util.List;


import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;

/**
 * @author deniger
 * @version $Id: TelemacCLWriter.java,v 1.28 2007-05-04 13:47:27 deniger Exp $
 */
public class TelemacCLWriter extends FileOpWriterCharSimpleAbstract {

  private final TelemacCLVersion version_;
  private boolean writeDataBrutes;

  /**
   * @return true si on ecrit a partir de donnees brutes soit des TelemacCLLine
   */
  public boolean isWriteDataBrutes() {
    return writeDataBrutes;
  }

  public void setWriteDataBrutes(boolean writeDataBrutes) {
    this.writeDataBrutes = writeDataBrutes;
  }

  /**
   * @param _f la version utilise (non null!)
   */
  public TelemacCLWriter(final TelemacCLVersion _f) {
    version_ = _f;
  }

  /**
   * @param _inter la source a ecrire.
   */
  public void writeCLBrutes(final List<TelemacCLLine> _inter) {
    final List<TelemacCLLine> inter = _inter;
    if (inter == null) {
      analyze_.addFatalError(H2dResource.getS("Les donn�es sont nulles"));
      return;
    }
    final int nbPt = inter.size();
    // Approximatif
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbPt);
    up.majProgessionStateOnly();
    final H2dTelemacCLElementSource source = new H2dTelemacCLElementSource();
    final char esp = ' ';
    try {
      for (int i = 0; i < nbPt; i++) {

        final StringBuffer buff = new StringBuffer(100);
        TelemacCLLine line = _inter.get(i);
        buff.append(line.lihbor_);
        buff.append(esp);
        buff.append(line.liubor_);
        buff.append(esp);
        buff.append(line.livbor_);
        buff.append(esp);
        buff.append(line.hbor_);
        buff.append(esp);
        buff.append(line.ubor_);
        buff.append(esp);
        buff.append(line.vbor_);
        buff.append(esp);
        buff.append(line.aubor_);
        buff.append(esp);
        buff.append(line.litbor_);
        buff.append(esp);
        buff.append(line.tbor_);
        buff.append(esp);
        buff.append(line.atbor_);
        buff.append(esp);
        buff.append(line.btbor_);
        buff.append(esp);
        buff.append(line.n_ + 1);
        if (line.k_ != i) {
          analyze_.addError(H2dResource.getS("L'index lu est invalide ( lu: {0} attendu: {1})", CtuluLibString
              .getString(i + 1), CtuluLibString.getString(line.k_ + 1)), i);
          return;
        }
        buff.append(esp);
        buff.append(line.k_ + 1);
        writelnToOut(buff.toString());
        up.majAvancement();
      }
      out_.flush();
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  /**
   * @param _inter la source a ecrire.
   */
  public void writeCL(final H2dTelemacCLSourceInterface _inter) {
    final H2dTelemacCLSourceInterface inter = _inter;
    if (inter == null) {
      analyze_.addFatalError(H2dResource.getS("Les donn�es sont nulles"));
      return;
    }
    final int nbPt = inter.getNbLines();
    // Approximatif
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbPt);
    up.majProgessionStateOnly();
    final TelemacCLLine line = new TelemacCLLine();
    final H2dTelemacCLElementSource source = new H2dTelemacCLElementSource();
    final char esp = ' ';
    try {
      for (int i = 0; i < nbPt; i++) {
        final StringBuffer buff = new StringBuffer(100);
        inter.getLine(i, source);
        getLine(analyze_, line, source, i);
        if (analyze_.containsErrors()) { return; }
        if (!version_.isCodeHauteur(line.lihbor_)) {
          analyze_.addError(H2dResource.getS("Le code concernant la hauteur est incorrect"), i);
          return;
        }
        buff.append(line.lihbor_);
        if (!version_.isCodeVitesse(line.liubor_)) {
          analyze_.addError(H2dResource.getS("Le code concernant la vitesse u est incorrect"), i);
          return;
        }
        buff.append(esp);
        buff.append(line.liubor_);
        if (!version_.isCodeVitesse(line.livbor_)) {
          analyze_.addError(H2dResource.getS("Le code concernant la vitesse v est incorrect"), i);
          return;
        }
        buff.append(esp);
        buff.append(line.livbor_);
        buff.append(esp);
        buff.append(line.hbor_);
        buff.append(esp);
        buff.append(line.ubor_);
        buff.append(esp);
        buff.append(line.vbor_);
        buff.append(esp);
        buff.append(line.aubor_);
        buff.append(esp);
        if (!version_.isCodeTraceur(line.litbor_)) {
          analyze_.addError(H2dResource.getS("Le code concernant le traceur est incorrect"), i);
          return;
        }
        buff.append(line.litbor_);
        buff.append(esp);
        buff.append(line.tbor_);
        buff.append(esp);
        buff.append(line.atbor_);
        buff.append(esp);
        buff.append(line.btbor_);
        buff.append(esp);
        buff.append(line.n_ + 1);
        if (line.k_ != i) {
          analyze_.addError(H2dResource.getS("L'index lu est invalide ( lu: {0} attendu: {1})", CtuluLibString
              .getString(i + 1), CtuluLibString.getString(line.k_ + 1)), i);
          return;
        }
        buff.append(esp);
        buff.append(line.k_ + 1);
        writelnToOut(buff.toString());
        up.majAvancement();
      }
      out_.flush();
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  /**
   * utilise la version pour faire la translation.
   * 
   * @param _r le receveur d'info/erreur
   * @param _target la cible a modifier
   * @param _source la source
   * @param _indexFrontiere l'indice sur la frontiere
   */
  public void getLine(final CtuluAnalyze _r, final TelemacCLLine _target, final H2dTelemacCLElementSource _source,
      final int _indexFrontiere) {
    version_.getFileLineFromH2dData(_r, _target, _source, _indexFrontiere);
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (isWriteDataBrutes() && (_o instanceof List)) {//Comment faire ?
      writeCLBrutes((List<TelemacCLLine>) _o);
    } else if (_o instanceof H2dTelemacCLSourceInterface) {
      writeCL((H2dTelemacCLSourceInterface) _o);
      return;
    } else donneesInvalides(_o);
  }

  /**
   * @param _o la source a ecrire
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final H2dTelemacCLSourceInterface _o) {
    writeCL(_o);
    return closeOperation(_o);
  }

}