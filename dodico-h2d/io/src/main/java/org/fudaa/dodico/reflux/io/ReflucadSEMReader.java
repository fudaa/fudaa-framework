/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

import com.memoire.fu.FuLog;

/**
 * Un lecteur pour les fichiers Reflucad de semis de points.
 * 
 * @author Bertrand Marchand
 * @version $Id: SinusxReader.java,v 1.22 2007/05/04 13:47:27 deniger Exp $
 */
public class ReflucadSEMReader extends FileOpReadCharSimpleAbstract implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  private String name_;
  /** */
  private int nbOctets_;

  FileFormatVersionInterface v_;

  /**
   * @param _v la version a utiliser
   */
  public ReflucadSEMReader(final FileFormatVersionInterface _v) {
    v_ = _v;
  }

  /**
   * Retourne les donn�es sous forme de {@link GISZoneCollectionPoint}. Contient le Z en attribut.
   * @return La collection de points
   */
  @Override
  protected Object internalRead() {
    return readSemis();
  }

  @Override
  protected void processFile(final File _f) {
    name_ = CtuluLibFile.getSansExtension(_f.getName());
    nbOctets_ = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized GISZoneCollectionPoint readSemis() {
    GISZoneCollectionPoint pts = new GISZoneCollectionPoint();
    pts.setAttributes(new GISAttribute[]{GISAttributeConstants.BATHY}, null);
    if(pts.getAttributeIsZ()==null)
      pts.setAttributeIsZ(GISAttributeConstants.BATHY);
    pts.setTitle(name_, null);

    if (super.in_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu=0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets_ > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      // Entete
      in_.readFields();
      String entete=in_.stringField(0);
      if (!"ENTETE:".equalsIgnoreCase(entete)) {
        analyze_.addError(H2dResource.getS("Format de fichier invalide : Format RefluCAD attendu"), 0);
        return null;
      }
      // Nom g�n�rique
      in_.readLine();
      // Date des lev�es
      in_.readLine();
      // Provenance
      in_.readLine();
      // Echelle
      in_.readFields();
//      double ech=in_.doubleField(0);
      FuLog.warning("Fichier Reflucad : Echelle non prise en compte");
      // Pr�cision
      in_.readLine();
      // Date de r�vision
      in_.readLine();
      // Num�ro de r�vision
      in_.readLine();
      // Nom de l'op�rateur
      in_.readLine();
      // Champ libre
      in_.readLine();
      // Ligne vide
      in_.readLine();
      lu+=183; // Si tous les champs remplis au maxi
      
      // Semi topo
      in_.readFields();
      if (!"SEM_TOPO:".equalsIgnoreCase(in_.stringField(0))) {
        analyze_.addError(H2dResource.getS("Format de fichier invalide : Semis RefluCAD attendu"), 0);
        return null;
      }
      lu+=10;
      
      // Les points, jusqu'� fin de fichier.
      while (true) {
        if (bstop_) return null;
        
        in_.setJumpBlankLine(true);
        in_.readFields();
        pts.add(in_.doubleField(0), in_.doubleField(1), in_.doubleField(2), Arrays.asList(new Double[]{new Double(in_.doubleField(2))}),null);
        lu+=35; // Approximatif

        if ((afficheAvance) && ((lu * 100 / nbOctets_) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    } catch (final EOFException e) {} catch (final IOException e) {
      analyze_.manageException(e);
    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    return pts;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  @Override
  public void stop() {
    bstop_ = true;
  }
}