/**
 * @creation 14 juin 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author Fred Deniger
 * @version $Id: RubarAPPReader.java,v 1.6 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarAPPReader extends FileOpReadCharSimpleAbstract {

  int nbElt_ = -1;

  public RubarAPPReader() {
    super();
  }

  @Override
  protected Object internalRead() {
    if (nbElt_ <= 0) {
      analyze_.addFatalError(H2dResource.getS("Le nombre d'�lements n'est pas pr�cis�"));
    }
    final RubarAPPResult rubarAPPResult = new RubarAPPResult();
    final int[] idxApp = new int[nbElt_];
    rubarAPPResult.eltEvolIdx_ = idxApp;
    // le max est la pour v�rifier que le NAPP sont correct
    int max = 0;
    int ie = 0;
    try {
      // lecture de NAPP(IE)
      int[] fmt = new int[10];
      Arrays.fill(fmt, 8);
      final int nbFieldByLine = fmt.length;
      int tmpOnLine = 0;
      in_.readFields(fmt);
      for (ie = 0; ie < nbElt_; ie++) {
        if (tmpOnLine == (nbFieldByLine)) {
          in_.readFields(fmt);
          tmpOnLine = 0;
        }
        idxApp[ie] = in_.intField(tmpOnLine) - 1;
        if (idxApp[ie] > max) {
          max = idxApp[ie];
        }
        tmpOnLine++;
      }
      final int nbCourbe = Integer.parseInt(in_.readLine().trim());
      if (nbCourbe <= max) {
        analyze_.addFatalError(H2dResource.getS("Des �l�ments utilisent une chronique d'apport non d�finie"));
        return null;
      }
      rubarAPPResult.evols_ = new EvolutionReguliere[nbCourbe];
      fmt = new int[] { 15, 15 };
      for (int i = 0; i < nbCourbe; i++) {
        final int nbPts = Integer.parseInt(in_.readLine().trim());
        rubarAPPResult.evols_[i] = new EvolutionReguliere(nbPts);
        for (int j = 0; j < nbPts; j++) {
          in_.readFields(fmt);
          rubarAPPResult.evols_[i].add(in_.doubleField(1), in_.doubleField(0));
        }

      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    return rubarAPPResult;
  }

  /**
   * @return le nombre d'element a lire
   */
  public int getNbElt() {
    return nbElt_;
  }

  /**
   * @param _nbElt le nombre d'elements � lire
   */
  public void setNbElt(final int _nbElt) {
    nbElt_ = _nbElt;
  }
}
