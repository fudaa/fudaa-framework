/**
 * @creation 14 juin 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author Fred Deniger
 * @version $Id: RubarTARReader.java,v 1.8 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarTARReader extends FileOpReadCharSimpleAbstract {

  @Override
  protected Object internalRead() {
    final TIntObjectHashMap r = new TIntObjectHashMap();
    try {
      while (true) {
        // IALT(I),NBLT(I)
        in_.readFields();

        if (in_.getNumberOfFields() == 2) {
          final int indiceGlobal = in_.intField(0);
          final int nbPoint = in_.intField(1);
          // s'il y a des points
          if (nbPoint > 0) {
            final EvolutionReguliere e = new EvolutionReguliere(nbPoint);
            // on lit nblt point q,z. Attention, on enregistre une evolution
            // avec x=z et y=q
            for (int i = nbPoint; i > 0; i--) {
              in_.readFields();
              // //ajout d'une ligne de test de conformit�
              if (in_.getNumberOfFields() != 2) {
                analyze_.addFatalError(H2dResource.getS("Le nombre de champs est invalide"), in_.getLineNumber());
              }
              e.add(in_.doubleField(1), in_.doubleField(0));
            }
            r.put(indiceGlobal - 1, e);
          }
        }
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    if (r.size() == 0) {
      analyze_.addError(H2dResource.getS("Pas de loi de tarage trouv�e"), -1);
      return null;
    }
    return r;
  }

}