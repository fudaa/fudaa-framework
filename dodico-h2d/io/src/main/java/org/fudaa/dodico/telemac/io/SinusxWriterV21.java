/*
 * @creation     10 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.telemac.io;

import java.io.IOException;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;

import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Une classe pour ecrire les fichiers Sinusx sous un format libre. Version 2.1.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class SinusxWriterV21 extends SinusxWriterAbstract {

  /**
   * @param _v
   */
  public SinusxWriterV21(FileFormatVersionInterface _v) {
    super(_v);
  }

  /**
   * Ecriture des coordonn�es. Elles sont �crites en format libre, pour ne plus avoir de pb avec les troncatures.
   */
  @Override
  protected void writeCoordinateSequence(final CoordinateSequence _s, final ProgressionUpdater _up, final boolean _ferme)
      throws IOException {
    int n=_s.size();
    if (_ferme) {
      n--;
    }
    for (int k=0; k<n; k++) {
      // Attention : La premi�re valeur est inscrite avec un '+' ou '-' obligatoire pour la relecture. C'est ce qui atteste qu'on est
      // sur une ligne de coordonn�es.
      String x=""+_s.getX(k);
      if (x.charAt(0)!='-' && x.charAt(0)!='+') x="+"+x;
      
      writelnToOut(CtuluLibString.ESPACE+x+CtuluLibString.ESPACE+_s.getY(k)
          +CtuluLibString.ESPACE+_s.getOrdinate(k, CoordinateSequence.Z)+" A");
      if (_up!=null) {
        _up.majAvancement();
      }
      if (stop_) {
        return;
      }
    }
  }

}
