/**
 * @creation 4 nov. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarArete;
import org.fudaa.dodico.h2d.rubar.H2dRubarGrid;
import org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarDATWriter.java,v 1.9 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarDATWriter extends FileOpWriterCharSimpleAbstract {
  boolean newFormat;

  public boolean isNewFormat() {
    return newFormat;
  }

  public void setNewFormat(boolean newFormat) {
    this.newFormat = newFormat;
  }

  protected void writeDat(final H2dRubarGridAreteSource _s) {
    try {
      final int[] fmtS = new int[]{getColumnFormatIdx()};
      final FortranWriter w = new FortranWriter(out_);
      final H2dRubarGrid g = _s.getRubarGrid();
      final int nbElt = g.getEltNb();
      if (!newFormat && (nbElt > 1E6 || g.getPtsNb() > 1E6)) {
        newFormat = true;
        analyze_.addWarn(H2dResource.getS("Le maillage est trop volumineux et le nouveau format a �t� utilis�."));
      }
      w.setSpaceBefore(true);
      // NE
      w.intField(0, nbElt);
      w.writeFields(fmtS);
      final int[] fmt5I = new int[5];
      Arrays.fill(fmt5I, getColumnFormatIdx());
      final int[][] eleVoisins = _s.elementsVoisinsParElement();
      for (int ie = 0; ie < nbElt; ie++) {
        // NAE,IAE
        final EfElementVolume v = g.getEltVolume(ie);
        final int nae = v.getNbAretes();
        w.intField(0, nae);
        int tmp = 1;
        for (int ja = 0; ja < nae; ja++) {
          // retour a la ligne sur trop d'element
          if (tmp == fmt5I.length) {
            w.writeFields(fmt5I);
            tmp = 0;
          }
          w.intField(tmp++, v.getIdxArete(ja) + 1);
        }
        // on ecrit la ligne en cours
        w.writeFields(fmt5I);
        // NNE,INE
        final int nne = v.getPtNb();
        w.intField(0, nne);
        tmp = 1;
        for (int jn = 0; jn < nne; jn++) {
          // retour a la ligne sur trop d'element
          if (tmp == fmt5I.length) {
            w.writeFields(fmt5I);
            tmp = 0;
          }
          w.intField(tmp++, v.getPtIndex(jn) + 1);
        }
        // on ecrit la ligne en cours
        w.writeFields(fmt5I);
        // NEVE IEVE
        final int neve = eleVoisins[ie].length;
        w.intField(0, neve);
        tmp = 1;
        for (int ieve = 0; ieve < neve; ieve++) {
          // retour a la ligne sur trop d'element
          if (tmp == fmt5I.length) {
            w.writeFields(fmt5I);
            tmp = 0;
          }
          w.intField(tmp++, eleVoisins[ie][ieve] + 1);
        }
        // on ecrit la ligne en cours
        w.writeFields(fmt5I);
      }
      if (progress_ != null) {
        progress_.setProgression(20);
      }
      final int na = g.getNbAretes();
      // NA
      w.intField(0, na);
      w.writeFields(fmtS);
      final int[] fmt6I = new int[6];
      Arrays.fill(fmt6I, getColumnFormatIdx());
      final int[][] nevaTableau = _s.elementsVoisinsParArete();
      for (int ia = 0; ia < na; ia++) {
        // NNA INA
        final H2dRubarArete s = g.getRubarArete(ia);
        /*
         * if (s.isExternAndLiquidAndTransient()) externAreteIdx.add(ia + 1);
         */
        w.intField(0, 2);
        w.intField(1, s.getPt1Idx() + 1);
        w.intField(2, s.getPt2Idx() + 1);
        w.writeFields(fmt5I);
        // NREFA NEVA IEVA
        final int neva = nevaTableau[ia].length;
        final H2dRubarBoundaryType t = s.getType();
        final int nrefa = t == null ? RubarDATFileFormat.getInternIdx() : t.getRubarIdx();
        w.intField(0, nrefa);
        w.intField(1, neva);
        int tmp = 2;
        for (int je = 0; je < neva; je++) {
          // retour a la ligne sur trop d'element
          if (tmp == fmt6I.length) {
            w.writeFields(fmt6I);
            tmp = 0;
          }
          w.intField(tmp++, nevaTableau[ia][je] + 1);
        }
        // on ecrit la ligne en cours
        w.writeFields(fmt6I);
      }
      // NN
      final int nn = g.getPtsNb();
      w.intField(0, nn);
      w.writeFields(fmtS);
      final int[] fmtX = new int[10];
      if (newFormat) {
        Arrays.fill(fmtX, 13);
      } else {
        Arrays.fill(fmtX, 8);
      }

      CtuluNumberFormatI fm = FortranLib.getFortranFormat(newFormat ? 12 : 8, _s.getNbDecimal());
      if (progress_ != null) {
        progress_.setProgression(60);
      }
      int tmp = 0;
      // XN
      for (int i = 0; i < nn; i++) {
        if (tmp == fmtX.length) {
          w.writeFields(fmtX);
          tmp = 0;
        }
        w.stringField(tmp++, fm.format(g.getPtX(i)));
      }
      w.writeFields(fmtX);
      tmp = 0;
      // YN
      for (int i = 0; i < nn; i++) {
        if (tmp == fmtX.length) {
          w.writeFields(fmtX);
          tmp = 0;
        }
        w.stringField(tmp++, fm.format(g.getPtY(i)));
      }
      w.writeFields(fmtX);
      if (progress_ != null) {
        progress_.setProgression(80);
      }
      // COTE
      // si x, y sont ecrit F8.4 alors z aussi. Sinon, z est en F8.3 C'est la fete !
      if (_s.getNbDecimal() != 4) {
        fm = FortranLib.getFortranFormat(8, 3);
      } else {
        fm = FortranLib.getFortranFormat(8, 4);
      }
      tmp = 0;
      //for z, always on 8 digits
      Arrays.fill(fmtX, 8);
      for (int i = 0; i < nn; i++) {
        if (tmp == fmtX.length) {
          w.writeFields(fmtX);
          tmp = 0;
        }
        w.stringField(tmp++, fm.format(g.getPtZ(i)));
      }
      w.writeFields(fmtX);
      // NAS
      final int[] extern = _s.getIdxAreteLimiteEntrante();
      final int nas = extern == null ? 0 : extern.length;
      w.intField(0, nas);
      w.writeFields(fmtS);
      // IAS
      if (extern != null && nas > 0) {
        // externAreteIdx.sort();
        final int[] fmt = new int[13];
        Arrays.fill(fmt, getColumnFormatIdx());
        tmp = 0;
        for (int i = 0; i < nas; i++) {
          if (tmp == fmt.length) {
            w.writeFields(fmt);
            tmp = 0;
          }
          w.intField(tmp++, extern[i] + 1);
        }
        w.writeFields(fmt);
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  private int getColumnFormatIdx() {
    return newFormat ? 9 : 6;
  }

  /**
   * Ecrire l'objet H2dRubarGridAreteSource.
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof H2dRubarGridAreteSource) {
      writeDat((H2dRubarGridAreteSource) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}
