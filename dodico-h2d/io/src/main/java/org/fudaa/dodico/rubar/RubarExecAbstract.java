/**
 * @creation 13 oct. 2004
 * @modification $Date: 2007-03-02 13:00:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar;

import java.io.File;

import org.fudaa.ctulu.CtuluUI;

import org.fudaa.dodico.calcul.CalculExecBatch;

/**
 * @author Fred Deniger
 * @version $Id: RubarExecAbstract.java,v 1.5 2007-03-02 13:00:54 deniger Exp $
 */
public abstract class RubarExecAbstract extends CalculExecBatch {

  /**
   * @param _exec
   */
  public RubarExecAbstract(final String _exec) {
    super(_exec);
    setStartCmdUse(true);
  }

  @Override
  public final String getTemplateDir() {
    return "serveurs/rubar/";
  }

  // TODO a d�commenter et � modifier pour que l'utilisateur n'est pas � rentrer le nom de l'�tude
  /*
   * protected String getCmdForGeneratedBatFile(final String _exe, final File _destFile) { String exe = "\"" + _exe +
   * "\""; if (_destFile != null) { if (FuLib.isWindows()) exe += " < " +
   * CtuluLibFile.getSansExtension(_destFile.getName()); else { exe += " << EOF" + CtuluLibString.LINE_SEP +
   * CtuluLibFile.getSansExtension(_destFile.getName()) + CtuluLibString.LINE_SEP + "EOF"; } } return exe; }
   */

  @Override
  public File createBatFile(final File _paramsFile) {
    return new File(_paramsFile.getParentFile(), exec_ + ".bat");
  }

  /*
   * protected boolean mustCreateBatFile(final String _exe) { return true; }
   */

  @Override
  public String[] getLaunchCmd(final File _paramsFile, final CtuluUI _ui) {
    return new String[] { super.getLaunchCmd(_paramsFile, _ui)[0] };
  }

}