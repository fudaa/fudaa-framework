/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-20 14:23:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarNumberFormatter;

/**
 * @author Fred Deniger
 * @version $Id: RubarDIFFileFormat.java,v 1.1 2006-11-20 14:23:54 deniger Exp $
 */
public final class RubarDIFFileFormat extends FileFormatUnique {

  private static RubarDIFFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public static RubarDIFFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarDIFFileFormat();
    }
    return instance_;
  }

  public RubarDIFFileFormat() {
    super(1);
    extensions_ = new String[] { "dif" };
    super.description_ = H2dResource.getS("Coefficient de diffusion par maille");
    super.id_ = "RUBAR_DIF";
    super.nom_ = "Rubar dif";
  }

  /**
   * @param _f
   * @param _prog
   * @param _nbElt
   * @return le resultats de l'operation
   */
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog, final int _nbElt) {
    final RubarFRTDIFReader i = new RubarFRTDIFReader();
    i.setNbElt(_nbElt);
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarFRTDIFReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarFRTDIFWriter(H2dRubarNumberFormatter.createNumberFormateur(8, 5));
  }

}