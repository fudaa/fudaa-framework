/**
 * @creation 17 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

/**
 * @author Fred Deniger
 * @version $Id: RubarMaxContainer.java,v 1.3 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarMaxContainer {

  double h_;
  double qu_;
  double qv_;
  boolean isHSet_;
  boolean isQSet_;

  /**
   * @return la hateur d'eau
   */
  public final double getH(){
    return h_;
  }

  /**
   * @param _h la hauteur
   */
  protected final void setH(final double _h){
    isHSet_=true;
    h_ = _h;
  }

  /**
   * @return le qu 
   */
  public final double getQu(){
    return qu_;
  }

  /**
   * @param _u le qu
   * @param _v le qv
   */
  protected final void setU(final double _u,final double _v){
    isQSet_=true;
    qu_ = _u;
    qv_=_v;
  }

  /**
   * @return qv
   */
  public final double getQv(){
    return qv_;
  }


  /**
   * @return true si hauteur initialiser
   */
  public final boolean isHSet(){
    return isHSet_;
  }

  /**
   * @return true si les debit on ete initialise
   */
  public final boolean isQSet(){
    return isQSet_;
  }
}