/*
 * @creation 13 juin 07
 * @modification $Date: 2007-06-20 12:21:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.fortran.FortranDoubleReader;
import org.fudaa.dodico.fortran.FortranDoubleReaderResult;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author fred deniger
 * @version $Id: RefluxSollicitationReader.java,v 1.2 2007-06-20 12:21:45 deniger Exp $
 */
public class RefluxSollicitationReader extends FileReadOperationAbstract {

  File file_;
  final int[] fmt_;
  FortranDoubleReader usedReader_;

  public RefluxSollicitationReader(final int[] _fmt) {
    super();
    fmt_ = _fmt;
  }

  @Override
  protected Object internalRead() {
    usedReader_ = null;
    try(LineNumberReader reader= new LineNumberReader(new FileReader(file_))) {
      // on lit la premiere ligne qui est -999
      reader.readLine();
      String first = reader.readLine();
      int sizeNorme = CtuluLibArray.getSum(fmt_);
      if (first.length() == sizeNorme) {
        usedReader_ = new FortranDoubleReader(fmt_);
        usedReader_.setSkipFirstLine(true);
        usedReader_.setFile(file_);
        return usedReader_.internalRead();
      } else if (first.length() == sizeNorme + 5) {
        analyze_
            .addError(
                H2dResource
                    .getS("Le fichier n'est pas correctement format�. Il a �t� corrig�.\nVeuillez sauvegarder votre projet pour prendre en compte la correction."),
                1);
        int[] newFmt = new int[fmt_.length + 1];
        System.arraycopy(fmt_, 0, newFmt, 1, fmt_.length);
        newFmt[0] = 5;
        usedReader_ = new FortranDoubleReader(newFmt);
        usedReader_.setSkipFirstLine(true);
        usedReader_.setFile(file_);
        return adapt((FortranDoubleReaderResult) usedReader_.internalRead());
      }

    } catch (IOException _evt) {
      analyze_.manageException(_evt);
      return null;

    }

    return null;
  }

  public static FortranDoubleReaderResult adapt(FortranDoubleReaderResult _init) {
    if (_init == null) return null;
    double[][] newRes = new double[_init.getNbColonne() - 1][_init.getNbLigne()];
    // on recopie les anciennes colonnes
    for (int i = 0; i < newRes.length; i++) {
      System.arraycopy(_init.r_[i + 1], 0, newRes[i], 0, newRes[i].length);
    }
    FortranDoubleReaderResult res = new FortranDoubleReaderResult();
    res.r_ = newRes;
    return res;
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return usedReader_;
  }

  @Override
  public void setFile(File _f) {
    analyze_ = new CtuluAnalyze();
    file_ = _f;

  }

}
