package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarVentInterface;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarVENWriter extends FileOpWriterCharSimpleAbstract {
  private void writeVENResult(final H2dRubarVentInterface result) {
    try {
      final FortranWriter w = new FortranWriter(out_);
      w.setSpaceBefore(true);
      w.setStringQuoted(false);
      final int nbElt = result.getNbElt();
      int tmp = 0;
      int[] fmt = new int[10];
      Arrays.fill(fmt, 8);
      for (int j = 0; j < nbElt; j++) {
        // retour a la ligne sur trop d'element
        if (tmp == fmt.length) {
          w.writeFields(fmt);
          tmp = 0;
        }
        w.intField(tmp++, result.getEvolIdx(j) + 1);
      }
      w.writeFields(fmt);
      final int[] fmti = new int[]{8};
      final int nbEvol = result.getNbEvol();
      final int maxAutorise = 20;
      final int maxTimeStep = 1000;
      if (nbEvol > maxAutorise) {
        analyze_.addWarn(H2dResource.getS("Le nombre maximum autoris� de chroniques est {0}", CtuluLibString.getString(maxAutorise)), -1);
      }

      w.intField(0, nbEvol);
      w.writeFields(fmti);
      boolean warningDone = false;
      final CtuluNumberFormatI numbFmt = FortranLib.getFortranFormat(15, 4);
      fmt = new int[]{15, 15, 15};
      for (int i = 0; i < nbEvol; i++) {
        final EvolutionReguliereInterface evolX = result.getEvolutionAlongX(i);
        final EvolutionReguliereInterface evolY = result.getEvolutionAlongY(i);
        final int nbVal = evolX.getNbValues();
        if (nbVal > maxTimeStep && !warningDone) {
          warningDone = true;
          analyze_.addWarn(
              H2dResource.getS("Le nombre maximum autoris� de pas de temps par chronique est {0}", CtuluLibString.getString(maxTimeStep)), -1);
        }
        w.intField(0, nbVal);
        w.writeFields(fmti);
        for (int j = 0; j < nbVal; j++) {
          w.stringField(0, numbFmt.format(evolX.getY(j)));
          w.stringField(1, numbFmt.format(evolY.getY(j)));
          w.stringField(2, numbFmt.format(evolX.getX(j)));
          w.writeFields(fmt);
        }
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  @Override
  protected void internalWrite(final Object o) {
    if (o instanceof H2dRubarVentInterface) {
      writeVENResult((H2dRubarVentInterface) o);
    } else {
      donneesInvalides(o);
    }
  }
}
