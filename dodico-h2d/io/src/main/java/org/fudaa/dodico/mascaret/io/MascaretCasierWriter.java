/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mascaret.io;

import java.io.PrintWriter;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;

import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Une classe pour ecrire des casiers au format Mascaret.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MascaretCasierWriter extends FileOpWriterCharSimpleAbstract {
  
  /**
   * Ecrit les casiers. Les casiers sont consid�r�s conformes, c'est a dire a plus de 4 points et un point semis.
   * param _o Un tableau GISDataModel[2]. 
   * [0] : le modele des contours de casiers  
   * [1] : le modele des multipoints internes aux casiers, dans le m�me ordre que les casiers.
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (!(_o instanceof GISDataModel[]) || ((GISDataModel[])_o).length!=2) {
      donneesInvalides(_o);
      return;
    }
    
    GISDataModel casiers=((GISDataModel[])_o)[0];
    GISDataModel semis=((GISDataModel[])_o)[1];
    if (casiers.getNumGeometries()!=semis.getNumGeometries()) {
      analyze_.addError(DodicoLib.getS("Chaque casier doit contenir au moins un semis"), -1);
      return;
    }
    
    final int attName=casiers.getIndiceOf(GISAttributeConstants.TITRE);
    
    final PrintWriter writer = new PrintWriter(out_);

    try {
      for (int i = 0; i < casiers.getNumGeometries(); i++) {
        CoordinateSequence seqCas=((GISCoordinateSequenceContainerInterface) casiers.getGeometry(i)).getCoordinateSequence();
        CoordinateSequence seqSemis=((GISCoordinateSequenceContainerInterface) semis.getGeometry(i)).getCoordinateSequence();

        String name;
        if (attName==-1||(name=(String)casiers.getValue(attName, i))==null) name="C"+(i+1);

        writer.println("Casier "+name);
        // Les points du contour.
        for (int k = 0; k < seqCas.size(); k++) {
          double x=seqCas.getOrdinate(k,0);
          double y=seqCas.getOrdinate(k,1);
          double z=seqCas.getOrdinate(k,2);
          writer.println(x+" "+y+" "+z+" F");
        }
        // Les points internes
        for (int k = 0; k < seqSemis.size(); k++) {
          double x=seqSemis.getOrdinate(k,0);
          double y=seqSemis.getOrdinate(k,1);
          double z=seqSemis.getOrdinate(k,2);
          writer.println(x+" "+y+" "+z+" I");
        }
      }

    } catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
    
  }
}
