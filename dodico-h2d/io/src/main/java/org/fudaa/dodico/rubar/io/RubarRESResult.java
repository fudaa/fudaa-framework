/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarRESResult {
  private RubarRESResultParameters parameters = new RubarRESResultParameters();
  private List<RubarRESResultLine> resultatsGeneral = new ArrayList<RubarRESResultLine>();
  private List<RubarRESResultLine> resultatsOuvrageB = new ArrayList<RubarRESResultLine>();

  public Map<H2dVariableType, EvolutionReguliereInterface> getEvolutions(String prefix) {
    Map<H2dVariableType, EvolutionReguliereInterface> res = new HashMap<H2dVariableType, EvolutionReguliereInterface>();
    if (resultatsOuvrageB.isEmpty()) {
      return res;
    }
    String end=prefix==null?"":prefix;
    double lastTime = 0;
    int nb = resultatsOuvrageB.size();
    List<H2dVariableType> variables = RubarRESResultLine.getVariables();
    for (H2dVariableType h2dVariableType : variables) {
      EvolutionReguliere value = new EvolutionReguliere(nb);
      res.put(h2dVariableType, value);
      value.setNom(h2dVariableType.getName()+end);
    }
    for (int i = 0; i < nb; i++) {
      RubarRESResultLine line = resultatsOuvrageB.get(i);
      if (i > 0 && CtuluLib.isEquals(lastTime, line.getTime(), 1e-3)) {
        continue;
      }
      lastTime = line.getTime();
      for (H2dVariableType h2dVariableType : variables) {
        ((EvolutionReguliere) res.get(h2dVariableType)).add(lastTime, line.getValue(h2dVariableType));
      }
    }
    return res;

  }

  /**
   * @return the parameters
   */
  public RubarRESResultParameters getParameters() {
    return parameters;
  }

  /**
   * @return the resultatsGeneral
   */
  public List<RubarRESResultLine> getResultatsGeneral() {
    return resultatsGeneral;
  }

  /**
   * @return the resultatsOuvrageB
   */
  public List<RubarRESResultLine> getResultatsOuvrageB() {
    return resultatsOuvrageB;
  }
}
