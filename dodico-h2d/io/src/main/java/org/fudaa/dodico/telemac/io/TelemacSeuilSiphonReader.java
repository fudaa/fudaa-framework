/**
 * @creation 29 nov. 2004
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: TelemacSeuilSiphonReader.java,v 1.8 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacSeuilSiphonReader extends FileOpReadCharSimpleAbstract {

  boolean containsSeuil_;

  public final boolean isContainsSeuil() {
    return containsSeuil_;
  }

  public final void setContainsSeuil(final boolean _containsSeuil) {
    containsSeuil_ = _containsSeuil;
  }

  @Override
  protected Object internalRead() {
    TelemacSeuilSiphonContainer cont = null;
    try {

      if (containsSeuil_) {
        cont = readSeuils();
      }

      in_.setJumpBlankLine(true);
      // Le commentaire du d�but des siphons
      in_.readLine();
      in_.setJumpBlankLine(false);
      in_.readFields();
      if (cont == null) {
        cont = new TelemacSeuilSiphonContainer();
      }
      cont.relaxation_ = in_.doubleField(0);
      final int nbSiphon = in_.intField(1);
      // les entetes
      in_.readLine();
      cont.siphons_ = new TelemacSiphonItem[nbSiphon];
      int count = 0;
      for (int i = 0; i < nbSiphon; i++) {
        in_.readFields();
        if (in_.getNumberOfFields() != 14) {
          analyze_.addError(H2dResource.getS("Le nombre de valeurs pour le siphon {0} n'est pas valide", CtuluLibString
              .getString(i + 1)), in_.getLineNumber());
          continue;

        }
        final TelemacSiphonItem it = new TelemacSiphonItem();
        cont.siphons_[count++] = it;
        int idx = 0;
        it.i1_ = in_.intField(idx++) - 1;
        it.i2_ = in_.intField(idx++) - 1;
        it.d1_ = in_.doubleField(idx++);
        it.d2_ = in_.doubleField(idx++);
        it.ce1_ = in_.doubleField(idx++);
        it.ce2_ = in_.doubleField(idx++);
        it.cs1_ = in_.doubleField(idx++);
        it.cs2_ = in_.doubleField(idx++);
        it.s12_ = in_.doubleField(idx++);
        it.l12_ = in_.doubleField(idx++);
        it.z1_ = in_.doubleField(idx++);
        it.z2_ = in_.doubleField(idx++);
        it.a1_ = in_.doubleField(idx++);
        it.a2_ = in_.doubleField(idx++);

      }
      if (count != nbSiphon) {
        final TelemacSiphonItem[] save = cont.siphons_;
        cont.siphons_ = new TelemacSiphonItem[count];
        System.arraycopy(save, 0, cont.siphons_, 0, count);
      }
    } catch (final EOFException e) {
      CtuluLibMessage.debug("Fin du fichier");
    } catch (final IOException _e) {
      // exception ajoutee a l'analyseur
      analyze_.manageException(_e);
    } catch (final NumberFormatException _fmt) {
      analyze_.addError(H2dResource.getS("Ce fichier n'est pas un fichier seuil/siphon"), -1);
      return null;
    }
    return cont;
  }

  private TelemacSeuilSiphonContainer readSeuils() throws IOException {
    TelemacSeuilSiphonContainer cont;
    // premiere ligne de commentaire lue
    in_.readLine();
    // la ligne doit comporter 2 valeurs
    // on ne sait pas encore ce que c'est : seuil ou syphon
    in_.readFields();
    final double v1 = in_.doubleField(0);
    final double v2 = in_.doubleField(1);
    cont = new TelemacSeuilSiphonContainer();
    final int nbSeuil = (int) v1;
    cont.optionTraitementVitesseT_ = (int) v2;
    cont.seuils_ = new TelemacSeuilItem[nbSeuil];
    int count = 0;
    for (int i = 0; i < nbSeuil; i++) {
      // ------- singularite 1
      in_.readLine();
      // nb de Points
      in_.readLine();
      in_.readFields();
      // Lecture du nombre de point
      final int nbPoint = in_.intField(0);
      // Points cotes 1
      in_.readLine();
      in_.readFields();
      boolean valide = true;
      if (in_.getNumberOfFields() != nbPoint) {
        analyze_.addError(H2dResource.getS("Le nombre de points annonc�s pour le seuil {0} n'est pas valide",
            CtuluLibString.getString(i + 1)), in_.getLineNumber());
        valide = false;
      }
      final TelemacSeuilItem it = valide ? new TelemacSeuilItem() : null;
      if (valide) {
        it.idxPt1_ = new int[nbPoint];
        it.idxPt2_ = new int[nbPoint];
        for (int j = 0; j < nbPoint; j++) {
          // les indices commencent a zero
          it.idxPt1_[j] = in_.intField(j) - 1;
        }
      }
      // Points cotes 2
      in_.readLine();
      in_.readFields();
      if (in_.getNumberOfFields() != nbPoint) {
        analyze_.addError(H2dResource.getS("Le nombre de points annonc�s pour le seuil {0} n'est pas valide",
            CtuluLibString.getString(i + 1)), in_.getLineNumber());
        valide = false;
      }
      if (valide) {
        for (int j = 0; j < nbPoint; j++) {
          // les indices commencent a zero
          it.idxPt2_[j] = in_.intField(j) - 1;
        }
        cont.seuils_[count++] = it;
      }
      // Cotes de digues
      in_.readLine();
      in_.readFields();
      if (in_.getNumberOfFields() != nbPoint) {
        analyze_.addError(H2dResource.getS("Le nombre de points utilis�s pour la c�te du seuil {0} n'est pas valide",
            CtuluLibString.getString(i + 1)), in_.getLineNumber());
      }
      it.cotes_ = new double[nbPoint];
      int max = Math.min(nbPoint, in_.getNumberOfFields());
      for (int j = 0; j < max; j++) {
        // les indices commencent a zero
        it.cotes_[j] = in_.doubleField(j);
      }
      // Coefficient de d�bit
      in_.readLine();
      in_.readFields();
      if (in_.getNumberOfFields() != nbPoint) {
        analyze_.addError(H2dResource.getS("Le nombre de points utilis�s pour le d�bit du seuil {0} n'est pas valide",
            CtuluLibString.getString(i + 1)), in_.getLineNumber());
      }
      it.debit_ = new double[nbPoint];
      max = Math.min(nbPoint, in_.getNumberOfFields());
      for (int j = 0; j < max; j++) {
        // les indices commencent a zero
        it.debit_[j] = in_.doubleField(j);
      }
    }
    if (count != cont.seuils_.length) {
      final TelemacSeuilItem[] save = cont.seuils_;
      cont.seuils_ = new TelemacSeuilItem[count];
      System.arraycopy(save, 0, cont.seuils_, 0, count);
    }
    return cont;
  }
}