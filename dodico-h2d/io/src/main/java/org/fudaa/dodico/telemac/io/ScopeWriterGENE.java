package org.fudaa.dodico.telemac.io;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Classe writer de fichier au format scope GENE. Utilise fortranWriter
 * 
 * @author Adrien Hadoux
 * 
 */
public class ScopeWriterGENE extends FileOpWriterCharSimpleAbstract implements CtuluActivity {

  boolean stop_;
  final FileFormatVersionInterface v_;

  ScopeKeyWord key_ = new ScopeKeyWord();

  public ScopeWriterGENE(final FileFormatVersionInterface _v) {
    v_ = _v;

  }

  @Override
  public void stop() {
    stop_ = true;
  }

  @Override
  protected void internalWrite(Object _o) {
    if (_o instanceof ScopeStructure) {
      writeStruct((ScopeStructure.Gene) _o);
    } else {
      donneesInvalides(_o);
    }
  }
  
  protected void writeStruct(ScopeStructure.Gene _struct) {

    if (super.out_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return;
    }

    // -- ecriture dans le fichier --//
    try {
      // -- ecriture des lignes titres --//
      writeHeader(_struct);

      // -- ecritures des variables --//
      writeVariable(_struct);

      // -- ecritures de toutes les valeurs des variables --//
      writeData(_struct);

    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }
  
  public void setProgression(int val, String desc) {
    if (progress_ != null) {
      progress_.setProgression(val);
      progress_.setDesc(desc);
    }
  }
  
  protected void writeHeader(ScopeStructure.Gene _struct) throws IOException {
   String bc = key_.getBlocCommentaireGENE();
   setProgression(0, H2dResource.getS("Cr�ation des en-t�tes") + bc);
   writelnToOut(bc + H2dResource.getS("Fichier g�n�r� par Fudaa") + bc);
   final String date = new SimpleDateFormat("yyyy-M-d  k:mm:ss").format(Calendar.getInstance().getTime());
   writelnToOut(bc + "Version " + key_.getVersion() + " - Date " + date + bc);
   writelnToOut(bc);
   
  }

  protected void writeVariable(ScopeStructure.Gene _struct) throws IOException {
   
    setProgression(10, H2dResource.getS("Cr�ation des variables"));
    // nb variabels - le x
    writelnToOut("" + (_struct.getNbVariables() - 1));
    for (int i = 1; i < _struct.getNbVariables(); i++) {
      writelnToOut(_struct.getVariable(i));
    }
    // tableau de val mode non eclatee
    String tableauVal = "0 " + key_.getScopeGENESeparator(_struct.getSeparator(0)) + " 0 0 0 0 0 0 0 0";
    writelnToOut(tableauVal);
  }

  protected void writeData(ScopeStructure.Gene _struct) throws IOException {
    
    setProgression(30, H2dResource.getS("Cr�ation des donn�es"));
    // pour chaque pas de temps
    for (int i = 0; i < _struct.getNbSeparator(); i++) {
      // separator pas de temps
      writelnToOut(_struct.getSeparator(i));

      // -- parcours la liste des donnees
      int nbValuesForThisPdt = _struct.getListeVarForSeparator(i).get(0).size();
      for (int k = 0; k < nbValuesForThisPdt; k++) {

        writelnToOut(_struct.getAllValuesForVariableAndSeparator(_struct.getSeparator(i), k));

      }
    
    }
  }
  
  
}
