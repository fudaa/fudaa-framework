package org.fudaa.dodico.reflux.io;


/**
 * Derniere version.
 * 
 * @author Fred Deniger
 * @version $Id: RefluxINPV5P1Version.java,v 1.5 2007-06-29 15:20:01 deniger Exp $
 */
public class RefluxINPV5P1Version extends RefluxINPV5P0Version {

  public RefluxINPV5P1Version(final String _version) {
    super(_version, true);
  }

  @Override
  public int[] getCOORFormat() {
    // le format d'apres le doc de Zhang
    // 5=le mot cl� POIN
    // 15= Le type du probl�me de r�f�rence �ventuel, au format A15
    // 8= Le nombre de dimensions du probl�me NDIM, au format I8
    // 8= Le nombre de noeuds total NNT, au format I8
    // 8=Le nombre maximum de degr�s de libert� par noeud, au format I8
    // 8=Le nombre de propri�t�s par noeuds, au format I8
    return new int[] { 5, 15, 8, 8, 8, 8 };
  }

  /**
   * @return format
   */
  @Override
  public int[] getCONDNoeudIdxFormat() {
    return new int[] { 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 };
  }

  @Override
  public String getPRND() {
    return "MAIL";
  }

  @Override
  public String getELEM() {
    return "MAIL";
  }

  /**
   * @return format
   */
  @Override
  public int[] getELEMFormat() {
    return new int[] { 5, 15, 8, 8, 8, 8 };
  }

  @Override
  public int[] getELEMValeurFormat() {
    return null;
  }

  @Override
  public INPReaderAbstract createINPReader() {
    return new INPReaderV5P1(this);
  }

  @Override
  public INPWriterAbstract createINPWriter() {
    return new INPWriterV5P1(this);
  }
}