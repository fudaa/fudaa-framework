/*
 * @creation     2 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CDoubleArrayList;
import org.fudaa.ctulu.collection.CollectionPointDataDoubleInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;

import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un adapteur pour ecrire sur un fichier Rubar SEM a partir d'un modele de multipoints.
 * @author Bertrand Marchand
 * @version $Id:$
 */
public class RubarSEMWriterGISAdapter implements FortranDoubleReaderResultInterface {
  CDoubleArrayList[] values_=new CDoubleArrayList[3];
  CtuluNumberFormatFortran doubleFormater_ = new CtuluNumberFormatFortran(13,4);

  /**
   * Le mod�le ne doit contenir que des multipoints. Le reste n'est pas trait�.
   * @param _mdl
   */
  public RubarSEMWriterGISAdapter(GISDataModel _mdl) {
    initVals(_mdl);
  }
  
  private void initVals(GISDataModel _mdl) {
    int nblines=0;
    int nb=_mdl.getNumGeometries();
    for (int i=0; i<nb; i++)
      nblines+=_mdl.getGeometry(i).getNumPoints();

    for (int i=0; i<3; i++) 
      values_[i]=new CDoubleArrayList(nblines);
    
    for (int i=0; i<nb; i++) {
      GISMultiPoint g=(GISMultiPoint)_mdl.getGeometry(i);
      CoordinateSequence seq=g.getCoordinateSequence();
      int nbpts=seq.size();
      for (int j=0; j<nbpts; j++) {
        for (int k=0; k<3; k++)
          values_[k].add(seq.getOrdinate(j,k));
      }
    }
  }
  
  @Override
  public CollectionPointDataDoubleInterface createXYValuesInterface() {
    throw new IllegalAccessError("This method shouldn't be accessed");
  }

  @Override
  public CtuluCollectionDouble getCollectionFor(int _col) {
    return values_[_col];
  }

  @Override
  public CtuluNumberFormatI getFormat(int _col) {
    return doubleFormater_;
  }

  @Override
  public int getNbColonne() {
    return 3; // X,Y,Z.
  }

  @Override
  public int getNbLigne() {
    return values_[0].size();
  }

  @Override
  public double getValue(int _ligne, int _col) {
    return getCollectionFor(_col).getValue(_ligne);
  }
}
