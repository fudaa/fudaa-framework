#!/bin/sh
OS=`uname`

#controle des arguments
if [ "$#" != "1" ]
then
  echo "Syntaxe : launcher.sh <fichier_sans_inp>"
  exit 1
fi

root=$1
echo $root
if [ ! -f $root.inp ]; then
  echo "Erreur : Fichier $root.inp inexistant"
  exit 1
fi

# Suppression des fichiers résultats
rm -f $root.out
rm -f $root.sfv
rm -f $root.sov
rm -f $root.sft
rm -f $root.sot

echo "Launching reflux..."

@exe@ << EOF

FICHIER: $root.inp $root.out
EOF
