/*
 * @creation     19 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

/**
 * Exception utilis� par MascaretWriter indiquant que des donn�es sont
 * incoh�rentes et rendent ainsi impossible l'export.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MascaretDataError extends Exception {

  public MascaretDataError(String _msg){
    super(_msg);
  }
}
