/*
 * @creation 2002-11-21
 * @modification $Date: 2007-05-04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISGeometry;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISMultiPoint;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISVisitorDefault;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;

/**
 * @version $Id: SinusxWriter.java,v 1.24 2007-05-04 13:47:27 deniger Exp $
 * @author Fred Deniger
 */
public abstract class SinusxWriterAbstract extends FileOpWriterCharSimpleAbstract implements CtuluActivity {

  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
  }

  class SinusxObjectIdentifier extends GISVisitorDefault {

    boolean isClosed_;
    boolean isLigne_;
    boolean isNiveau_;
    boolean isPt_;

    @Override
    public boolean visitPoint(final Point _p) {
      isPt_ = true;
      isLigne_ = false;
      isClosed_ = false;
      isNiveau_ = false;
      return true;
    }

    @Override
    public boolean visitPolygone(final LinearRing _p) {
      isPt_ = false;
      isLigne_ = true;
      isClosed_ = true;
      isNiveau_ = ((GISPolygone) _p).isNiveau() || GISLib.isNiveau(_p.getCoordinateSequence());
      return true;
    }

    @Override
    public boolean visitPolygoneWithHole(final Polygon _p) {
      isPt_ = false;
      isLigne_ = false;
      isClosed_ = false;
      isNiveau_ = false;
      return true;
    }

    @Override
    public boolean visitPolyligne(final LineString _p) {
      isPt_ = false;
      isLigne_ = true;
      isClosed_ = false;
      isNiveau_ = ((GISPolyligne) _p).isNiveau() || GISLib.isNiveau(_p.getCoordinateSequence());
      return true;
    }
  }
  private final String pointPlus_ = "+";

  /** 1 formate selon les spec de sinusx. */
  private final String unString_ = "+1.000000E+00";

  /** 0 formate selon les spec de sinusx. */
  private final String zeroString_ = "+0.000000E+00";

  final String bi_;

  final String bs_;

  final DecimalFormat formate_ = new DecimalFormat("0.000000E00", new DecimalFormatSymbols(Locale.US));

  final String idxoy_;
  final SinusxKeyWord key_ = new SinusxKeyWord();
  final FileFormatVersionInterface v_;

  /**
   * @param _v la version utilisee.
   */
  public SinusxWriterAbstract(final FileFormatVersionInterface _v) {
    v_ = _v;
    bs_ = key_.getBlocSep();
    bi_ = key_.getBlocInfos();
    idxoy_ = key_.getIdPlanXoy();
  }

  private String courbeDefaut(final int _nb) {
    final StringBuffer r = new StringBuffer(250);
    for (int i = 0; i < _nb; i++) {
      r.append(CtuluLibString.ESPACE).append(zeroString_);
    }
    return r.toString();
  }

  protected abstract void writeCoordinateSequence(final CoordinateSequence _s, final ProgressionUpdater _up, final boolean _ferme)
    throws IOException;
    
  private void writeEntete(final int _idx, final String _nom, final String _spec, final boolean _ferme)
      throws IOException {
    final StringBuffer buf = new StringBuffer();
    if (_idx == 0) {
      buf.append(repereDefaut());
    }
    buf.append(lineSep_);
    buf.append(key_.getBlocNomEntite()).append(CtuluLibString.ESPACE).append(_nom);
    buf.append(lineSep_);
    // DEBUG:A voir
    buf.append(bi_).append(CtuluLibString.ESPACE).append(_ferme ? key_.getCourbeFermee() : key_.getCourbeNonFermee())
        .append(CtuluLibString.ESPACE).append(key_.getCourbeReliee());
    buf.append(lineSep_);
    // if(ferme) buf_.append(COURBE_NON_RELIEE)
    if (_spec != null) {
      buf.append(bi_).append(CtuluLibString.ESPACE).append(_spec);
      buf.append(lineSep_);
    }
    buf.append(bi_).append(CtuluLibString.ESPACE).append(idxoy_);
    writelnToOut(buf.toString());
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof GISDataModel[][]) {
      writeZones((GISDataModel[][]) _o);
    } else {
      donneesInvalides((GISDataModel[][]) _o);
    }
  }

  protected String profilDefaut() {
    return CtuluLibString.ESPACE + zeroString_ + CtuluLibString.ESPACE + zeroString_ + CtuluLibString.ESPACE
        + unString_ + CtuluLibString.ESPACE + unString_;
  }

  protected String repereDefaut() {
    return CtuluLibString.ESPACE + zeroString_ + CtuluLibString.ESPACE + zeroString_ + CtuluLibString.ESPACE
        + zeroString_ + CtuluLibString.ESPACE + unString_ + CtuluLibString.ESPACE + unString_ + CtuluLibString.ESPACE
        + unString_ + " 1";
  }

  /**
   * @return la version
   */
  public FileFormatVersionInterface getVersion() {
    return v_;
  }

  /**
   * @param _zones les zones a ecrire.
   */
  public void writeZones(final GISDataModel[][] _zones) {
    if (_zones == null) {
      analyze_.addFatalError(H2dResource.getS("Les donn�es sont nulles"));
      return;
    }
    final SinusxObjectIdentifier identifieur = new SinusxObjectIdentifier();
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    if (progress_ != null) {
      progress_.setProgression(0);
    }
    up.majProgessionStateOnly();
    int nbPoints=0;
    for(int i=0;i<_zones.length;i++)
      for(int j=0;j<_zones[i].length;j++)
        for(int k=0;k<_zones[i][j].getNumGeometries();k++)
          nbPoints+=_zones[i][j].getGeometry(k).getNumPoints();
    up.setValue(5, nbPoints);

    final String bc = key_.getBlocCommentaire();
    try {
      writelnToOut(bc + "\t" + H2dResource.getS("Fichier g�n�r� par Fudaa"));
      final String date = new SimpleDateFormat("yyyy-M-d  k:mm:ss").format(Calendar.getInstance().getTime());
      writelnToOut(bc + "\tVersion " + v_.getVersionName() + " - Date " + date);
      writelnToOut(bc);
      writelnToOut(bc);
      writelnToOut(bc);
      String spec;
      formate_.setPositivePrefix(pointPlus_);

      // Points
      GISDataModel[] models=_zones[0];
      for (int j=0; j<models.length; j++) {
        if (stop_)
          return;
        final StringBuffer buf=new StringBuffer(50);
        buf.append(bs_).append(' ');
        buf.append(key_.getTypeSemis());
        writeToOut(buf.toString());
        writeEntete(j, "semis", null, false);
        Coordinate[] coord=new Coordinate[models[j].getNumGeometries()];
        for (int k=0; k<coord.length; k++)
          coord[k]=models[j].getGeometry(k).getCoordinate();
        writeCoordinateSequence(new GISCoordinateSequenceFactory().create(coord), up, false);
      }
      // Polylignes
      models=_zones[1];
      for (int j=0; j<models.length; j++) {
        final int nbPoly=models[j].getNumGeometries();
        int attName=models[j].getIndiceOf(GISAttributeConstants.TITRE);
        for (int polyIdx=0; polyIdx<nbPoly; polyIdx++) {
          if (stop_)
            return;
          final StringBuffer buf=new StringBuffer();
          buf.append(bs_).append(CtuluLibString.ESPACE);
          final LineString geom=(LineString)models[j].getGeometry(polyIdx);
          ((GISGeometry)geom).accept(identifieur);
          final boolean ferme=identifieur.isClosed_;

          final String nom;
          if (attName==-1)
            nom=((ferme) ? "polygone":"polyligne")+CtuluLibString.ESPACE+(j+1)+"-"+(polyIdx+1);
          else
            nom=(String)models[j].getValue(attName, polyIdx);

          if (identifieur.isNiveau_) {
            buf.append(key_.getTypeCourbeNiveau());
            spec=formate_.format(geom.getCoordinateSequence().getOrdinate(0, 2));
          }
          else {
            buf.append(key_.getTypeCourbe());
            spec=courbeDefaut(key_.getCourbeNBIndic());
          }
          writeToOut(buf.toString());
          writeEntete(j+polyIdx, nom, spec, ferme);
          writeCoordinateSequence(geom.getCoordinateSequence(), up, ferme);
        }
      }
      // Multipoint
      models=_zones[2];
      for (int j=0; j<models.length; j++) {
        int attName=models[j].getIndiceOf(GISAttributeConstants.TITRE);
        final int nbPoly=models[j].getNumGeometries();
        for (int polyIdx=0; polyIdx<nbPoly; polyIdx++) {
          if (stop_)
            return;
          final StringBuffer buf=new StringBuffer();
          buf.append(bs_).append(CtuluLibString.ESPACE);
          final GISMultiPoint geom=(GISMultiPoint)models[j].getGeometry(polyIdx);

          final String nom;
          if (attName==-1)
            nom="semis"+CtuluLibString.ESPACE+(j+1)+"-"+(polyIdx+1);
          else
            nom=(String)models[j].getValue(attName, polyIdx);

          buf.append(key_.getTypeSemis());
          spec=null;
          writeToOut(buf.toString());
          writeEntete(j+polyIdx, nom, spec, false);
          writeCoordinateSequence(geom.getCoordinateSequence(), up, false);
        }
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }
}
