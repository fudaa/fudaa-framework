/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersionAbstract;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarIMAFileFormat.java,v 1.7 2006-11-15 09:22:53 deniger Exp $
 */
public final class RubarIMAFileFormat extends FileFormatGridVersionAbstract {

  private static RubarIMAFileFormat instance_;

  @Override
  public String getVersionName() {
    return CtuluLibString.UN;
  }

  /**
   * @return l'instance a utiliser
   */
  public static RubarIMAFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarIMAFileFormat();
    }
    return instance_;
  }

  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese s = read(_f, _prog);
    if (s.getSource() != null) {
      final EfGridSourceDefaut r = new EfGridSourceDefaut((EfGridInterface) s.getSource(), this);
      s.setSource(r);
    }
    return s;
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return write(_f, _m == null ? null : _m.getGrid(), _prog);
  }

  /**
   * Initialise toutes les donnees.
   */
  public RubarIMAFileFormat() {
    super(1);
    extensions_ = new String[] { "ima" };
    super.description_ = H2dResource.getS("Maillage");
    super.id_ = "RUBAR_MAI";
    super.nom_ = H2dResource.getS("Rubar maillage ( format IMA)");
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarIMAReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarIMAWriter();
  }

  @Override
  public boolean canReadGrid() {
    return true;
  }

  @Override
  public boolean canWriteGrid() {
    return true;
  }

  @Override
  public boolean hasBoundaryConditons() {
    // TODO Voir si correct
    return false;
  }
}