package org.fudaa.dodico.mascaret.io;

import com.memoire.bu.BuAbstractPreferencesComponent;
import com.memoire.bu.BuTextField;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.fudaa.commun.FudaaPreferences;

/**
 * Un composant pour les pr�f�rences d'export de Mascaret
 * @author marchand@deltacad.fr
 */
public class MascaretExportPreferencesComponent extends BuAbstractPreferencesComponent {
  public static final String MASCARET_EXPORT_NB_DIGITS="file.mascaret.export.nbdigits";
  private BuTextField tfExport_;
  private JCheckBox cbLimit_;
  private int exportNbDec;

  public MascaretExportPreferencesComponent() {
    super(FudaaPreferences.FUDAA);
    
    tfExport_=BuTextField.createIntegerField();
    tfExport_.setPreferredSize(new Dimension(50,tfExport_.getPreferredSize().height));

    cbLimit_=new JCheckBox(DodicoLib.getS("Limite du nombre de d�cimales sur X,Y �"));
    setLayout(new FlowLayout(FlowLayout.LEFT,2,0));
    add(cbLimit_);
    add(tfExport_);
    
    updateComponent();
    
    tfExport_.addCaretListener(new CaretListener() {
      @Override
      public void caretUpdate(CaretEvent e) {
        setSavabled(tfExport_.getValue()!=null);
        setModified(true);
      }
    });
    cbLimit_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfExport_.setEnabled(cbLimit_.isSelected());
        setSavabled(!cbLimit_.isSelected() || tfExport_.getValue()!=null);
        setModified(true);
      }
    });
  }
  
  @Override
  public String getTitle() {
    return DodicoLib.getS("Export fichier Mascaret");
  }
  
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }
  
  /**
   * Mise a jour des composant � partir des info du fichier.
   */
  @Override
  protected void updateComponent() {
    exportNbDec=options_.getIntegerProperty(MASCARET_EXPORT_NB_DIGITS, -1);
    if (exportNbDec==-1) {
      tfExport_.setValue(3);
      tfExport_.setEnabled(false);
      cbLimit_.setSelected(false);
    }
    else {
      tfExport_.setValue(exportNbDec);
      tfExport_.setEnabled(true);
      cbLimit_.setSelected(true);
    }
  }
  
  /**
   * Remplit la table a partir des valeurs des combobox.
   */
  @Override
  protected void updateProperties() {
    // Nombre de decimales pour l'export des coordonn�es
    if (cbLimit_.isSelected()) {
      options_.putIntegerProperty(MASCARET_EXPORT_NB_DIGITS,(Integer)tfExport_.getValue());
    }
    else {
      options_.putIntegerProperty(MASCARET_EXPORT_NB_DIGITS,-1);
    }
  }
}
