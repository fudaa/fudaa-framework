/**
 * @creation 1 d�c. 2004
 * @modification $Date: 2006-11-15 09:22:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: TelemacSeuilSiphonFileFormat.java,v 1.8 2006-11-15 09:22:54 deniger Exp $
 */
public class TelemacSeuilSiphonFileFormat extends FileFormatUnique {

  /**
   * Parametres par defaut.
   */
  public TelemacSeuilSiphonFileFormat() {
    super(1);
    extensions_ = new String[] { "txt" };
    id_ = "TELEMAC_SEUIL_SIPHON";
    nom_ = H2dResource.getS("T�l�mac seuils / siphons");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return createReader(true);
  }

  /**
   * @param _containsSeuil true si le fichier est cense contenir des seuils
   * @return le resultat de l'operation
   */
  public FileReadOperationAbstract createReader(final boolean _containsSeuil) {
    final TelemacSeuilSiphonReader r = new TelemacSeuilSiphonReader();
    r.setContainsSeuil(_containsSeuil);
    return r;
  }

  /**
   * @param _f le fichier a lire
   * @param _containsSeuil true si contiens des seuils
   * @param _prog la progression
   * @return le resultat de la lecture
   */
  public CtuluIOOperationSynthese read(final File _f, final boolean _containsSeuil, final ProgressionInterface _prog) {
    final FileReadOperationAbstract i = createReader(_containsSeuil);
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new TelemacSeuilSiphonWriter();
  }

}