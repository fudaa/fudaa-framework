/**
 * @creation 3 janv. 2005 @modification $Date: 2007-05-22 13:11:24 $ @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd
 *     Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TDoubleHashSet;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.rubar.*;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: RubarOUVWriter.java,v 1.9 2007-05-22 13:11:24 deniger Exp $
 */
public class RubarOUVWriter extends FileOpWriterCharSimpleAbstract {
  FortranWriter w_;
  private boolean newFormat;

  public boolean isNewFormat() {
    return newFormat;
  }

  public void setNewFormat(boolean newFormat) {
    this.newFormat = newFormat;
  }

  protected void writeOuv(final H2dRubarOuvrageContainer _c) {

    final int nbOuv = _c.getNbOuvrage();
    H2dRubarOuvrageI o;

    final String esp = CtuluLibString.ESPACE;
    buildFirstLine();
    w_ = new FortranWriter(out_);
    w_.setSpaceBefore(true);
    final DecimalFormat fmt = CtuluLib.getDecimalFormat();
    fmt.setMinimumFractionDigits(4);
    fmt.setMaximumFractionDigits(4);
    try {
      for (int iOuv = 0; iOuv < nbOuv; iOuv++) {
        StringBuffer buf = new StringBuffer();
        o = _c.getOuvrage(iOuv);
        // XIA1, YIA1
        buf.append(fmt.format(o.getXaAmont())).append(esp).append(fmt.format(o.getYAmont())).append(esp);
        // YIE1, YIE1
        buf.append(fmt.format(o.getXElementAmont())).append(esp).append(fmt.format(o.getYElementAmont())).append(esp);
        final int nbMaille = o.getNbMailleIntern();
        // NREF, NBMAIL
        buf.append(o.getRubarRef()).append(esp).append(nbMaille);
        writelnToOut(buf.toString());
        // buf=writeln(buf);
        // XIEI YIEI
        for (int iMail = 0; iMail < nbMaille; iMail++) {
          writelnToOut(fmt.format(o.getXMailleIntern(iMail)) + esp + fmt.format(o.getYMailleIntern(iMail)));
        }
        buf = new StringBuffer();
        // XIA2, YIA2
        buf.append(fmt.format(o.getXaAval())).append(esp).append(fmt.format(o.getYAval())).append(esp);
        // YIE2, YIE2
        buf.append(fmt.format(o.getXElementAval())).append(esp).append(fmt.format(o.getYELementAval())).append(esp);
        // NOUV
        final int nbOuvEle = o.getNbOuvrageElementaires();
        buf.append(nbOuvEle);
        writelnToOut(buf.toString());
        for (int iOuvEle = 0; iOuvEle < nbOuvEle; iOuvEle++) {
          final H2dRubarOuvrageElementaireInterface ele = o.getOuvrageElementaire(iOuvEle);
          writeOuvElem(ele);
        }
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }

  protected void writeOuvElem(H2dRubarOuvrageElementaireInterface ele) throws IOException {
    if (ele.getType().equals(H2dRubarOuvrageType.BRECHE)) {
      writeOuvBreche((H2dRubarOuvrageElementaireBrecheI) ele);
    } else if (ele.getType().equals(H2dRubarOuvrageType.COMPOSITE)) {
      writeOuvComposite((H2dRubarOuvrageElementaireCompositeI) ele);
    } else if (ele.getType().equals(H2dRubarOuvrageType.DEVERSOIR)) {
      writeOuvDeversoir((H2dRubarOuvrageElementaireDeversoirI) ele);
    } else if (ele.getType().equals(H2dRubarOuvrageType.DEVERSOIR_HYDRAULIQUE)) {
      writeOuvDeversoirHydraulique((H2dRubarOuvrageElementaireDeversoirHydrauliqueI) ele);
    } else if (ele.getType().equals(H2dRubarOuvrageType.ORIFICE_CIRCULAIRE)) {
      writeOuvOrificeCirculaire((H2dRubarOuvrageElementaireOrificeCirculaireI) ele);
    } else if (ele.getType().equals(H2dRubarOuvrageType.APPORT_DEBIT)) {
      writeOuvApport((H2dRubarOuvrageElementaireApportI) ele);
    } else if (ele.getType().equals(H2dRubarOuvrageType.TRANSFERT_DEBIT_TARAGE)) {
      writeOuvTransfert((H2dRubarOuvrageElementaireTransfertDebitI) ele);
    }
  }

  int[] firstLine_;
  final CtuluNumberFormatI defaultFormat = FortranLib.getFortranFormat(10, 3);
  final CtuluNumberFormatI xyNewFormat = FortranLib.getFortranFormat(12, 3);

  protected void buildFirstLine() {
    if (firstLine_ == null) {
      firstLine_ = RubarOUVReader.getFirstLineFmt();
    }
  }

  protected void writeOuvComposite(final H2dRubarOuvrageElementaireCompositeI _d) throws IOException {
    w_.stringField(0, _d.getType().getRubarId());
    w_.stringField(1, _d.getTypeControle().getRubarId());
    final double[] valeurs = _d.getValeurs();
    w_.stringField(2, Integer.toString(valeurs.length));
    CtuluNumberFormatI xyFormat = this.defaultFormat;
    if (newFormat) {
      xyFormat = xyNewFormat;
    }
    w_.stringField(3, xyFormat.format(_d.getXAmont()));
    w_.stringField(4, xyFormat.format(_d.getYAmont()));
    w_.stringField(5, xyFormat.format(_d.getXAval()));
    w_.stringField(6, xyFormat.format(_d.getYAval()));
    w_.writeFields(newFormat ? RubarOUVReader.FMT_COMPOSITE_NEW : RubarOUVReader.FMT_COMPOSITE);
    if (valeurs.length > 0) {
      for (int i = 0; i < valeurs.length; i++) {
        w_.stringField(i, defaultFormat.format(valeurs[i]));
      }
      w_.writeFields(RubarOUVReader.FMT_COMPOSITE_VALEURS);
    }

    final H2dRubarOuvrageElementaireInterface[] ouvrages = _d.getOuvrages();

    for (int i = 0; i < ouvrages.length; i++) {
      writeOuvElem(ouvrages[i]);
    }
  }

  protected void writeOuvDeversoir(final H2dRubarOuvrageElementaireDeversoirI _d) throws IOException {
    w_.stringField(0, _d.getType().getRubarId());
    w_.stringField(2, defaultFormat.format(_d.getLongDeversement()));
    w_.stringField(3, defaultFormat.format(_d.getCoteSeuilZd()));
    w_.stringField(4, defaultFormat.format(_d.getCoteMisEnchargeZm()));
    w_.stringField(5, defaultFormat.format(_d.getCoefficientDebit()));
    w_.writeFields(RubarOUVReader.FMT_DEVERSOIR);
  }

  protected void writeOuvDeversoirHydraulique(final H2dRubarOuvrageElementaireDeversoirHydrauliqueI _d)
      throws IOException {
    w_.stringField(0, _d.getType().getRubarId());
    w_.stringField(2, defaultFormat.format(_d.getLongDeversement()));
    w_.stringField(3, defaultFormat.format(_d.getCoteSeuilZd()));
    w_.stringField(4, defaultFormat.format(_d.getCoteMisEnchargeZm()));
    w_.stringField(5, defaultFormat.format(_d.getCoefficientDebit()));
    w_.writeFields(RubarOUVReader.FMT_DEVERSOIR_HYDRAULIQUE);
  }

  protected void writeOuvOrificeCirculaire(final H2dRubarOuvrageElementaireOrificeCirculaireI _d) throws IOException {
    w_.stringField(0, _d.getType().getRubarId());
    w_.stringField(2, defaultFormat.format(_d.getLongDeversement()));
    w_.stringField(3, defaultFormat.format(_d.getCoteSeuilZd()));
    w_.stringField(4, defaultFormat.format(_d.getDiametre()));
    w_.stringField(5, defaultFormat.format(_d.getCoefficientDebit()));
    w_.writeFields(RubarOUVReader.FMT_ORIFICE_CIRCULAIRE);
  }

  protected void writeOuvDebit(final H2dRubarOuvrageElementaireTransfertDebitI _d, List<H2dVariableType> qVariables) throws IOException {
    w_.stringField(0, _d.getType().getRubarId());
    List<EvolutionReguliereAbstract> evols = new ArrayList<EvolutionReguliereAbstract>();
    TDoubleHashSet timesSet = new TDoubleHashSet();
    for (H2dVariableType qVariable : qVariables) {
      EvolutionReguliereAbstract evol = _d.getEvol(qVariable);
      if (evol != null) {
        timesSet.addAll(evol.getArrayX());
        evols.add(evol);
      } else {
        break;
      }
    }
    double[] times = timesSet.toArray();
    Arrays.sort(times);

    final int nbPt = times.length;
    w_.intField(1, nbPt);
    w_.stringField(2, CtuluLibString.ZERO);
    w_.stringField(3, CtuluLibString.ZERO);
    w_.stringField(4, CtuluLibString.ZERO);
    w_.stringField(5, CtuluLibString.ZERO);
    w_.writeFields(firstLine_);
    for (int i = 0; i < nbPt; i++) {
      out_.write(Double.toString(times[i]));
      for (int j = 0; j < evols.size(); j++) {
        out_.write(CtuluLibString.ESPACE);
        out_.write(Double.toString(evols.get(j).getInterpolateYValueFor(times[i])));
      }
      writelnToOut();
    }
  }

  private void writeOuvTransfert(final H2dRubarOuvrageElementaireTransfertDebitI _d) throws IOException {
    writeOuvDebit(_d, H2dRubarOuvrage.getTarageVariables());
  }

  private void writeOuvApport(final H2dRubarOuvrageElementaireApportI _d) throws IOException {
    int nbEvolution = _d.getEvols().size();
    List<H2dVariableType> qVariables = RubarOUVReader.getH2dVariableTypesTransportBlocks(nbEvolution);
    writeOuvDebit(_d, qVariables);
  }

  int[] fmtBrecheSec_;
  int[] fmtBrecheQuatre_;
  int[] fmtBrecheThird_;

  protected void writeOuvBreche(final H2dRubarOuvrageElementaireBrecheI _d) throws IOException {
    CtuluNumberFormatI formatteur6_2 = FortranLib.getFortranFormat(6, 2);
    CtuluNumberFormatI formatteur8_2 = FortranLib.getFortranFormat(8, 2);
    CtuluNumberFormatI formatteur10_2 = FortranLib.getFortranFormat(10, 2);

    w_.stringField(0, _d.getType().getRubarId());
    w_.stringField(1, Integer.toString(_d.getBrecheType()));

    if (_d.getDebutTemps() >= 0) {
      w_.stringField(2, formatteur8_2.format(_d.getDebutTemps()));
    }

    w_.stringField(3, formatteur10_2.format(_d.getCoteCrete()));
    w_.stringField(4, formatteur10_2.format(_d.getCotePied()));
    w_.stringField(5, formatteur10_2.format(_d.getLargeurCrete()));
    w_.stringField(6, formatteur10_2.format(_d.getLargeurPied()));

    w_.writeFields(RubarOUVReader.FMT_BRECHE);

    w_.stringField(0, formatteur10_2.format(_d.getDiametreMedian()));
    w_.stringField(1, formatteur10_2.format(_d.getCoeffStrickler()));
    w_.stringField(2, formatteur10_2.format(_d.getMasseVolumiqueGrain()));
    w_.stringField(3, formatteur10_2.format(_d.getPorosite()));

    w_.writeFields(RubarOUVReader.FMT_BRECHE_LINE2);

    w_.stringField(0, formatteur10_2.format(_d.getCoteFond()));
    w_.stringField(1, formatteur10_2.format(_d.getDimensionInitiale()));
    w_.stringField(2, formatteur10_2.format(_d.getCoefficientErosionLineaire()));

    w_.writeFields(RubarOUVReader.FMT_BRECHE_LINE3);

    w_.stringField(0, formatteur10_2.format(_d.getPasDeTemps()));
    w_.stringField(1, formatteur10_2.format(_d.getCoeffPerteDeCharge()));
    w_.stringField(2, Integer.toString(_d.getRenardSurverseId()));
    w_.stringField(3, Integer.toString(_d.getNbPasDeTempsMax()));

    w_.writeFields(RubarOUVReader.FMT_BRECHE_LINE4);
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof H2dRubarOuvrageContainer) {
      writeOuv((H2dRubarOuvrageContainer) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}
