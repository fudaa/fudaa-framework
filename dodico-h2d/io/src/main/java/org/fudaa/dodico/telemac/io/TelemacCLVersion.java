/**
 * @creation 1 juin 2004
 * @modification $Date: 2007-11-14 15:55:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.File;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatVersion;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * Version pour les fichiers condiditions limites.
 * 
 * @author Fred Deniger
 * @version $Id: TelemacCLVersion.java,v 1.20 2007-11-14 15:55:06 clavreul Exp $
 */
public class TelemacCLVersion extends FileFormatVersion {

  public static void initDefaultData(final TelemacCLLine _target, final H2dTelemacCLElementSource _source,
      final int _indexFrontiere) {
    _target.ubor_ = _source.u_;
    _target.vbor_ = _source.v_;
    _target.hbor_ = _source.h_;
    _target.tbor_ = _source.tracer_;
    _target.k_ = _indexFrontiere;
    _target.n_ = _source.ptGlobalIdx_;
    _target.aubor_ = _source.friction_;
    _target.atbor_ = _source.tracerCoefA_;
    _target.btbor_ = _source.tracerCoefB_;

  }

  private final int debitImpose_ = 5;

  private final int hauteurImposee_ = 5;
  private final int libre_ = 4;
  private final int ondeIncidence_ = 1;
  private final int solide_ = 2;
  private final int traceurImposee_ = 5;
  private final int vitesseImposee_ = 6;

  private final int vitesseNulle_ = 0;

  TelemacCLVersion(final String _v) {
    super(TelemacCLFileFormat.getInstance(), _v);
  }

  private void codeUIncorrect(final CtuluAnalyze _r, final int _indexFrontiere) {
    _r.addWarn(getUIncorrect(), _indexFrontiere);
  }

  private void doFromFileDebitImpose(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target,
      final TelemacCLLine _l, final int _indicesFrontieres) {
    int tempTest;
    final int vi = getCodeVitesseImposee();
    final int vn = getCodeVitesseNulle();
    _target.bordType_ = H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE;
    tempTest = _l.liubor_;
    if (tempTest == vn) {
      _target.uType_ = H2dBcType.PERMANENT;
      _target.u_ = 0;
    } else if (tempTest == vi) {
      _target.uType_ = H2dBcType.PERMANENT;
    } else if (_l.liubor_ == getCodeDebitImpose()) {
      _target.uType_ = H2dBcType.LIBRE;
    } else {
      _r.addError(getUIncorrect(), _indicesFrontieres);
      _r.addInfo(H2dResource.getS("La fronti�re est de type liquide d�bit impos�"), _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
    }
    tempTest = _l.livbor_;
    if (tempTest == vn) {
      _target.vType_ = H2dBcType.PERMANENT;
      _target.v_ = 0;
    } else if (tempTest == vi) {
      _target.vType_ = H2dBcType.PERMANENT;
    } else if (tempTest == getCodeDebitImpose()) {
      _target.vType_ = H2dBcType.LIBRE;
    } else {
      _r.addError(getCodeVIncorrect(), _indicesFrontieres);
      _r.addInfo(H2dResource.getS("La fronti�re est de type liquide d�bit impos�"), _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
      _target.vType_ = H2dBcType.LIBRE;
    }
  }

  private H2dBcType doFromFileHauteur(final CtuluAnalyze _r, final TelemacCLLine _l, final int _indicesFrontieres) {
    H2dBcType hType;
    if (_l.lihbor_ == getCodeLibre()) {
      hType = H2dBcType.LIBRE;
    } else if (_l.lihbor_ == getCodeHauteurImposee()) {
      hType = H2dBcType.PERMANENT;
    } else {
      _r.addError(H2dResource.getS("Le code concernant la hauteur est incorrect.") + CtuluLibString.ESPACE
          + H2dResource.getS("Codes accept�s") + getColon() + getCodeHauteurImposee() + getVir() + getCodeLibre(),
          _indicesFrontieres);
      hType = H2dBcType.LIBRE;
    }
    return hType;
  }

  private void doFromFileOnde(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target, final TelemacCLLine _l,
      final int _indicesFrontieres) {
    final int onde = getCodeOndeIncidence();
    if ((_l.liubor_ != onde) || (_l.livbor_ != onde)) {
      _r.addError(H2dResource.getS("Le code concernant l'onde incidence est incorrect.") + CtuluLibString.ESPACE
          + H2dResource.getS("Code accept�") + "1 1 1", _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
    }
    _target.bordType_ = H2dBoundaryTypeCommon.LIQUIDE_ONDE_INCIDENCE;
    _target.uType_ = H2dBcType.LIBRE;
    _target.vType_ = H2dBcType.LIBRE;
  }

  private void doFromFileSolid(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target, final TelemacCLLine _l,
      final int _indicesFrontieres) {
    final int solide = getCodeSolide();
    final int vn = getCodeVitesseNulle();
    _target.bordType_ = H2dBoundaryTypeCommon.SOLIDE;
    // le traceur doit etre solide egalement
    if (_l.litbor_ != solide) {
      _r.addWarn(H2dResource.getS("La condition limite concernant le traceur sera de type libre"), _indicesFrontieres);
    }
    _target.tracerType_ = H2dBcType.LIBRE;
    if (_l.liubor_ == vn) {
      _target.uType_ = H2dBcType.PERMANENT;
      _target.u_ = 0d;
    } else if (_l.liubor_ == solide) {
      _target.uType_ = H2dBcType.LIBRE;
    } else {
      _r.addError(getUIncorrect(), _indicesFrontieres);
      _r.addInfo(H2dResource.getS("Fronti�re solide"), _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
      _target.uType_ = H2dBcType.LIBRE;
    }
    if (_l.livbor_ == vn) {
      _target.vType_ = H2dBcType.PERMANENT;
      _target.v_ = 0;
    } else if (_l.livbor_ == solide) {
      _target.vType_ = H2dBcType.LIBRE;
    } else {
      _r.addError(getCodeVIncorrect(), _indicesFrontieres);
      _r.addInfo(H2dResource.getS("Fronti�re solide"), _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
      _target.vType_ = H2dBcType.LIBRE;
    }
  }

  private void doFromFileTracer(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target, final TelemacCLLine _l,
      final int _indicesFrontieres) {
    final int tempTest = _l.litbor_;
    final int libre = getCodeLibre();
    if (tempTest == getCodeTraceurImpose()) {
      _target.tracerType_ = H2dBcType.PERMANENT;
    } else if ((tempTest == libre) || (tempTest == getCodeSolide())) {
      _target.tracerType_ = H2dBcType.LIBRE;
      if (tempTest == getCodeSolide()) {
        _r.addWarn(H2dResource.getS("Le traceur est defini comme solide pour une fronti�re liquide."
            + "Il sera consid�r� comme libre"), _indicesFrontieres);
      }
    } else {
      _r.addError(H2dResource.getS("Le code concernant le traceur est incorrect.") + CtuluLibString.ESPACE
          + H2dResource.getS("Code lu") + getColon() + tempTest, _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
      _target.tracerType_ = H2dBcType.LIBRE;
    }
  }

  private void doFromH2dDebit(final CtuluAnalyze _r, final TelemacCLLine _target,
      final H2dTelemacCLElementSource _source, final int _indexFrontiere) {
    final int vi = getCodeVitesseImposee();
    final int vn = getCodeVitesseNulle();
    final int temp = getCodeDebitImpose();
    if (_source.uType_ == H2dBcType.LIBRE) {
      _target.liubor_ = temp;
      if (_source.vType_ == H2dBcType.PERMANENT) {
        _target.livbor_ = _source.v_ == 0 ? vn : vi;
      } else {
        if (_source.vType_ != H2dBcType.LIBRE) {
          _r.addWarn(getCodeVIncorrect(), _indexFrontiere);
          _r.addInfo(H2dResource.getS("Le code d�bit impos� sera utilis�"), _indexFrontiere);
        }
        _target.livbor_ = temp;
      }
    } else if (_source.vType_ == H2dBcType.LIBRE) {
      _target.livbor_ = temp;
      if (_source.uType_ == H2dBcType.PERMANENT) {
        _target.liubor_ = _source.u_ == 0 ? vn : vi;
      } else {
        _r.addWarn(getCodeVIncorrect(), _indexFrontiere);
        _r.addInfo(H2dResource.getS("Le code d�bit impos� sera utilis�"), _indexFrontiere);
        _target.liubor_ = temp;
      }
    } else {
      if ((_source.vType_ == H2dBcType.PERMANENT) && (_source.uType_ == H2dBcType.PERMANENT)) {
        _r.addWarn(H2dResource.getS("Au moins une composante de la vitesse doit �tre non impos�e"), _indexFrontiere);
      } else {
        _r.addWarn(H2dResource.getS("Codes inconnus"), _indexFrontiere);
      }
      _r.addInfo(H2dResource.getS("Les codes d�bit impos� seront utilis�s"), _indexFrontiere);
      _target.liubor_ = temp;
      _target.livbor_ = temp;
    }
  }

  private void doFromH2dOnde(final TelemacCLLine _target) {
    final int temp = getCodeOndeIncidence();
    _target.lihbor_ = temp;
    _target.liubor_ = temp;
    _target.livbor_ = temp;
  }

  private void doFromH2dSolid(final CtuluAnalyze _r, final TelemacCLLine _target,
      final H2dTelemacCLElementSource _source, final int _indexFrontiere) {
    final int vn = getCodeVitesseNulle();
    final int temp = getCodeSolide();
    if (_source.tracerType_ != H2dBcType.LIBRE) {
      _r.addWarn(H2dResource.getS("Le code concernant le traceur est incorrect"), _indexFrontiere);
      _r.addInfo(H2dResource.getS("Le code solide sera utilis�"), _indexFrontiere);
    }
    _target.lihbor_ = temp;
    _target.litbor_ = temp;
    if (_source.uType_ == H2dBcType.PERMANENT) {
      _target.liubor_ = vn;
      _target.ubor_ = 0d;
      if (_source.u_ != 0) {
        _r.addWarn(H2dResource.getS("Si impos�e, la vitesse u pour un bord solide doit �tre nulle"), _indexFrontiere);
      }
    } else {
      if (_source.uType_ != H2dBcType.LIBRE) {
        codeUIncorrect(_r, _indexFrontiere);
        _r.addInfo(H2dResource.getS("Le code solide sera utilis�"), _indexFrontiere);
      }
      _target.liubor_ = temp;
    }
    if (_source.vType_ == H2dBcType.PERMANENT) {
      _target.livbor_ = vn;
      _target.vbor_ = 0;
      if (_source.v_ != 0) {
        _r.addWarn(H2dResource.getS("Si impos�e, la vitesse v pour un bord solide doit �tre nulle"), _indexFrontiere);
      }
    } else {
      if (_source.vType_ != H2dBcType.LIBRE) {
        codeUIncorrect(_r, _indexFrontiere);
        _r.addInfo(H2dResource.getS("Le code solide sera utilis�"), _indexFrontiere);
      }
      _target.livbor_ = temp;
    }
  }

  private String getCodeVIncorrect() {
    return H2dResource.getS("Le code concernant la vitesse v est incorrect");
  }

  private String getColon() {
    return ": ";
  }

  private String getUIncorrect() {
    return H2dResource.getS("Le code concernant la vitesse u est incorrect");
  }

  private String getVir() {
    return ", ";
  }

  /**
   * @return le reader
   */
  public TelemacCLReader createCLReader() {
    return new TelemacCLReader(this);
  }

  /**
   * @return le writer
   */
  public TelemacCLWriter createCLWriter() {
    return new TelemacCLWriter(this);
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new TelemacCLReader(this);
  }

  /**
   * @param f le fichier a lire
   * @param prog la barre de progression
   * @return le resultat contenant List<TelemacCLLine>
   */
  public CtuluIOOperationSynthese readDataBrutes(final File f, final ProgressionInterface prog) {
    final TelemacCLReader reader = createCLReader();
    reader.setReadDataBrut(true);
    final CtuluIOOperationSynthese r = reader.read(f, prog);
    reader.setProgressReceiver(null);
    return r;

  }

  public CtuluIOOperationSynthese writeDataBrutes(final File _f, final Object _source, final ProgressionInterface _prog) {
    final TelemacCLWriter w = createCLWriter();
    w.setWriteDataBrutes(true);
    final CtuluIOOperationSynthese r = w.write(_source, _f, _prog);
    w.setProgressReceiver(null);
    return r;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new TelemacCLWriter(this);
  }

  /**
   * Comportement.
   * 
   * @return code du debit impose
   */
  public int getCodeDebitImpose() {
    return debitImpose_;
  }

  /**
   * Comportement.
   * 
   * @return code hauteur imposee
   */
  public int getCodeHauteurImposee() {
    return hauteurImposee_;
  }

  /**
   * Comportement.
   * 
   * @return code libre
   */
  public int getCodeLibre() {
    return libre_;
  }

  /**
   * type.
   * 
   * @return code onde incidente
   */
  public int getCodeOndeIncidence() {
    return ondeIncidence_;
  }

  /**
   * Type.
   * 
   * @return bord solide.
   */
  public int getCodeSolide() {
    return solide_;
  }

  /**
   * @return code traceur impose
   */
  public int getCodeTraceurImpose() {
    return traceurImposee_;
  }

  /**
   * @return vitesse imposee.
   */
  public int getCodeVitesseImposee() {
    return vitesseImposee_;
  }

  /**
   * @return code vitesse nulle.
   */
  public int getCodeVitesseNulle() {
    return vitesseNulle_;
  }

  /**
   * @param _r le receveur d'info
   * @param _target la ligne a modifier
   * @param _source la source utilisee pour modifier la cible
   * @param _indexFrontiere l'indice du point sur la frontiere.
   */
  public void getFileLineFromH2dData(final CtuluAnalyze _r, final TelemacCLLine _target,
      final H2dTelemacCLElementSource _source, final int _indexFrontiere) {
    initDefaultData(_target, _source, _indexFrontiere);
    // Bord solide
    if (_source.bordType_ == H2dBoundaryTypeCommon.SOLIDE) {
      doFromH2dSolid(_r, _target, _source, _indexFrontiere);
      return;
    }

    // On s'occupe du mode pour le traceur qui sera toujours le meme.
    if (_source.tracerType_ == H2dBcType.PERMANENT) {
      _target.litbor_ = getCodeTraceurImpose();
    } else {
      if (_source.tracerType_ != H2dBcType.LIBRE) {
        _r.addWarn(H2dResource.getS("Le code concernant le traceur est incorrect") + CtuluLibString.ESPACE
            + _source.tracerType_, _indexFrontiere);
        _r.addInfo(H2dResource.getS("Le code libre sera utilis�"), _indexFrontiere);
      }
      _target.litbor_ = getCodeLibre();
    }
    // onde incidente
    if (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_ONDE_INCIDENCE) {
      doFromH2dOnde(_target);
      return;
    }
    // le cas de h peut etre fixe.
    if ((_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE)
        || (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES)) {
      _target.lihbor_ = getCodeHauteurImposee();
    } else {
      _target.lihbor_ = getCodeLibre();
    }
    // debit impose
    if (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE) {
      doFromH2dDebit(_r, _target, _source, _indexFrontiere);
    }
    // frontiere liquide
    else {
      if (_source.uType_ == H2dBcType.PERMANENT) {
        if ((_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES)
            || (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES)) {
          _target.liubor_ = getCodeVitesseImposee();
        } else {
          if (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE) {
            _target.liubor_ = getCodeLibre();
          }
        }

        // _target.liubor_ = _source.u_ == 0 ? getCodeVitesseNulle() : getCodeVitesseImposee();
      } else {
        if (_source.uType_ != H2dBcType.LIBRE) {
          codeUIncorrect(_r, _indexFrontiere);
          _r.addInfo(H2dResource.getS("Le code libre sera utilis�"), _indexFrontiere);
        }
        _target.liubor_ = getCodeLibre();
      }
      if (_source.vType_ == H2dBcType.PERMANENT) {
        if ((_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES)
            || (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES)) {
          _target.livbor_ = getCodeVitesseImposee();
        } else {
          if (_source.bordType_ == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE) {
            _target.livbor_ = getCodeLibre();
          }
        }

        // _target.livbor_ = _source.v_ == 0 ? getCodeVitesseNulle() : getCodeVitesseImposee();
      } else {
        if (_source.vType_ != H2dBcType.LIBRE) {
          codeUIncorrect(_r, _indexFrontiere);
          _r.addInfo(H2dResource.getS("Le code libre sera utilis�"), _indexFrontiere);
        }
        _target.livbor_ = getCodeLibre();
      }
    }
  }

  /**
   * @param _r le receveur des infos/erreurs.
   * @param _target la cible a modifier.
   * @param _l la source utilisee pour modifier la cible.
   * @param _indicesFrontieres
   * @return true si ok.
   */
  public boolean getH2dDataFromFile(final CtuluAnalyze _r, final H2dTelemacCLElementSource _target,
      final TelemacCLLine _l, final int _indicesFrontieres) {
    initH2dData(_target, _l);
    // FRONTIERE SOLIDE
    // H2dBordType r= null;
    if (_l.lihbor_ == getCodeSolide()) {
      doFromFileSolid(_r, _target, _l, _indicesFrontieres);
      return true;
    }
    // LE TRACEUR
    doFromFileTracer(_r, _target, _l, _indicesFrontieres);
    // ONDE INCIDENTE
    if (_l.lihbor_ == getCodeOndeIncidence()) {
      doFromFileOnde(_r, _target, _l, _indicesFrontieres);
      return true;
    }
    // LA HAUTEUR
    final H2dBcType hType = doFromFileHauteur(_r, _l, _indicesFrontieres);

    // LIQUIDE DEBIT IMPOSE
    if ((_l.liubor_ == getCodeDebitImpose()) || (_l.livbor_ == getCodeDebitImpose())) {
      doFromFileDebitImpose(_r, _target, _l, _indicesFrontieres);
      return true;
    }
    _target.bordType_ = H2dBoundaryTypeCommon.LIQUIDE;
    int tempTest = _l.liubor_;
    // u
    // imposee
    if (tempTest == getCodeVitesseImposee()) {
      _target.uType_ = H2dBcType.PERMANENT;
    }
    // imposee nulle
    else if (tempTest == getCodeVitesseNulle()) {
      _target.uType_ = H2dBcType.PERMANENT;
      _target.u_ = 0;
    }
    // libre
    else if (tempTest == getCodeLibre()) {
      _target.uType_ = H2dBcType.LIBRE;
    }
    // erreur
    else {
      _r.addError(getUIncorrect() + CtuluLibString.DOT + CtuluLibString.ESPACE + H2dResource.getS("Codes accept�s")
          + getColon() + getCodeLibre() + getVir() + getCodeVitesseNulle() + getVir() + getCodeVitesseImposee(),
          _indicesFrontieres);
      _r.addInfo(H2dResource.getS("La fronti�re est de type liquide"), _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
      _target.uType_ = H2dBcType.LIBRE;
    }
    tempTest = _l.livbor_;
    if (tempTest == getCodeVitesseImposee()) {
      _target.vType_ = H2dBcType.PERMANENT;
    }
    // imposee nulle
    else if (tempTest == getCodeVitesseNulle()) {
      _target.vType_ = H2dBcType.PERMANENT;
      _target.v_ = 0;
    }
    // libre
    else if (tempTest == getCodeLibre()) {
      _target.vType_ = H2dBcType.LIBRE;
    }
    // erreur
    else {
      _r.addError(getCodeVIncorrect() + CtuluLibString.DOT + CtuluLibString.ESPACE + H2dResource.getS("Codes accept�s")
          + getColon() + getCodeLibre() + getVir() + getCodeVitesseNulle() + getVir() + getCodeVitesseImposee(),
          _indicesFrontieres);
      _r.addInfo(H2dResource.getS("La fronti�re est de type liquide"), _indicesFrontieres);
      _r.addInfo(_l.getResume(), _indicesFrontieres);
      _target.vType_ = H2dBcType.LIBRE;
    }
    // on precise le type de bord pour les cl liquides
    if (_target.bordType_.isLiquide()) {
      // hauteur imposee.
      if (hType == H2dBcType.PERMANENT) {
        if ((_target.uType_ == H2dBcType.PERMANENT) || (_target.vType_ == H2dBcType.PERMANENT)) {
          _target.bordType_ = H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES;
        } else {
          _target.bordType_ = H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE;
          // vitesse imposee.
        }
      } else if ((_target.uType_ == H2dBcType.PERMANENT) || (_target.vType_ == H2dBcType.PERMANENT)) {
        _target.bordType_ = H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES;
      }
    }
    return true;
  }

  public static void initH2dData(final H2dTelemacCLElementSource _target, final TelemacCLLine _l) {
    _target.h_ = _l.hbor_;
    _target.u_ = _l.ubor_;
    _target.v_ = _l.vbor_;
    _target.friction_ = _l.aubor_;
    _target.tracer_ = _l.tbor_;
    _target.tracerCoefA_ = _l.atbor_;
    _target.tracerCoefB_ = _l.btbor_;
    _target.ptGlobalIdx_ = _l.n_;
  }

  /**
   * @param _c le code a tester
   * @return true si <code>_c</code> est un code connu pour la hauteur (imposee,libre, solide ou onde incidente).
   */
  public boolean isCodeHauteur(final int _c) {
    return (_c == hauteurImposee_) || (_c == libre_) || (_c == solide_) || (_c == ondeIncidence_);

  }

  /**
   * @param _c le code a tester
   * @return true si <code>_c</code> est un code connu pour le traceur (imposee,libre, solide ou onde incidente).
   */
  public boolean isCodeTraceur(final int _c) {
    return (_c == traceurImposee_) || (_c == libre_) || (_c == solide_) || (_c == ondeIncidence_);
  }

  /**
   * @param _c le code a tester
   * @return true si <code>_c</code> est un code connu pour la vitesse (imposee,libre, solide ou onde incidente).
   */
  public boolean isCodeVitesse(final int _c) {
    return (_c == vitesseImposee_) || (_c == libre_) || (_c == solide_) || (_c == ondeIncidence_)
        || (_c == vitesseNulle_) || (_c == debitImpose_);
  }

  /**
   * @param _f le fichie a ecrire
   * @param _source la source
   * @param _prog la barre de progression
   * @return la synthese de l'operation.
   */
  // TODO le nom de la m�thode est bizarre a changer !
  public CtuluIOOperationSynthese writeMaillage(final File _f, final H2dTelemacCLSourceInterface _source,
      final ProgressionInterface _prog) {
    final TelemacCLWriter w = new TelemacCLWriter(this);
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_source);
  }

}