/**
 *  @creation     20 janv. 2005
 *  @modification $Date: 2006-11-15 09:22:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

/**
 * @author Fred Deniger
 * @version $Id: RubarFileFormatDefault.java,v 1.6 2006-11-15 09:22:53 deniger Exp $
 */
public class RubarFileFormatDefault extends FileFormatUnique {

  @Override
  public FileReadOperationAbstract createReader() {
    return null;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return null;
  }

  /**
   * @param _ext l'extension du fichier
   */
  public RubarFileFormatDefault(final String _ext) {
    super(1);
    extensions_ = new String[] { _ext.toLowerCase() };
    super.id_ = "RUBAR_" + _ext.toUpperCase();
    super.nom_ = "Rubar " + _ext.toLowerCase();

  }

  public RubarFileFormatDefault(final String _ext, final String _nom) {
    this(_ext);
    super.nom_ = "Rubar " + _nom;

  }

}
