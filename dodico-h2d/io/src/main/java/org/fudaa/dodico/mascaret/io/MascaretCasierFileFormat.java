/*
 * @creation     11 Mars 2009
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Un file format pour les fichier Mascaret Casiers
 * @author Bertrand Marchand
 * @version $Id$
 */
public class MascaretCasierFileFormat extends FileFormatUnique {

  public MascaretCasierFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Mascaret casiers");
    description_ = H2dResource.getS("Fichier contenant des casiers géoréférencés");
    extensions_=new String[]{"casieref"};
  }
  static final MascaretCasierFileFormat INSTANCE = new MascaretCasierFileFormat();

  /**
   * @return singleton
   */
  public static MascaretCasierFileFormat getInstance() {
    return INSTANCE;
  }
  
  @Override
  public FileReadOperationAbstract createReader() {
    return null;
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new MascaretCasierWriter();
  }

}
