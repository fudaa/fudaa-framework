/**
 * @creation 22 d�c. 2004 @modification $Date: 2006-09-19 14:45:51 $ @license GNU General Public License 2 @copyright (c)1998-2001
 *     CETMEF 2 bd Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireDeversoirI;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: RubarOuvrageElementaireDeversoir.java,v 1.3 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarOuvrageElementaireDeversoir implements H2dRubarOuvrageElementaireDeversoirI {
  double longDeversement_;
  double coteSeuilZd_;
  double coteMisEnchargeZm_;
  double coefficientDebit_;

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.DEVERSOIR;
  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    return false;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return null;
  }

  @Override
  public final double getCoefficientDebit() {
    return coefficientDebit_;
  }

  @Override
  public final double getCoteMisEnchargeZm() {
    return coteMisEnchargeZm_;
  }

  @Override
  public final double getCoteSeuilZd() {
    return coteSeuilZd_;
  }

  @Override
  public final double getLongDeversement() {
    return longDeversement_;
  }
}
